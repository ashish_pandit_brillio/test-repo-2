/**
 * RESTlet for Holiday List
 * 
 * Version Date Author Remarks 1.00 11 Apr 2016 Nitish Mishra
 * 
 */

function postRESTlet(dataIn) {
	var response = new Response();

	try {
		nlapiLogExecution('debug', 'dataIn', JSON.stringify(dataIn));
		var employeeId = getUserUsingEmailId(dataIn.EmailId);
		var requestType = dataIn.RequestType;

		switch (requestType) {

			case M_Constants.Request.Get:
				response.Data = getHolidayList(employeeId);
				response.Status = true;
			break;
		}

	} catch (err) {
		nlapiLogExecution('ERROR', 'postRESTlet', err);
		response.Data = err;
	}

	nlapiLogExecution('debug', 'response', JSON.stringify(response));
	return response;
}

function getHolidayList(employeeId) {
	try {
		nlapiLogExecution('debug', 'employee id', employeeId);

		var employeeLocation = nlapiLookupField('employee', employeeId,
		        'location');

		var locationArray = [ employeeLocation ];
		var currentLocation = employeeLocation;

		while (currentLocation) {
			var locationRec = nlapiLoadRecord('location', currentLocation);
			currentLocation = locationRec.getFieldValue('parent');

			if (currentLocation) {
				locationArray.push(currentLocation);
			}
		}

		nlapiLogExecution('debug', 'location array', JSON
		        .stringify(locationArray));

		return generateHolidayList(locationArray);
	} catch (err) {
		nlapiLogExecution('error', 'getHolidayList', err);
		throw err;
	}
}

function generateHolidayList(locationArray) {
	try {
		var searchHoliday = nlapiSearchRecord(
		        'customrecord_mobile_holiday_calendar', null, [
		                new nlobjSearchFilter('custrecord_mhc_location', null,
		                        'anyof', locationArray),
		                new nlobjSearchFilter('custrecord_mhc_date', null,
		                        'onorafter', 'startofthisyear'),
		                new nlobjSearchFilter('custrecord_mhc_date', null,
		                        'onorbefore', 'nextfiscalyear'),
		                new nlobjSearchFilter('isinactive', null, 'is', 'F') ],
		        [ new nlobjSearchColumn('custrecord_mhc_date').setSort(),
		                new nlobjSearchColumn('custrecord_mhc_title'),
		                new nlobjSearchColumn('custrecord_mhc_image') ]);

		var holidayList = [];
		var today = new Date();
		var nextHolidayFound = false;

		var urlPrefix = "";

		if (nlapiGetContext().getEnvironment() == "PRODUCTION") {
			urlPrefix = "https://system.na1.netsuite.com/";
		} else {
			urlPrefix = "https://system.na1.netsuite.com/";
		}

		if (searchHoliday) {

			searchHoliday.forEach(function(holiday) {
				var status = '';
				var holidayDate = nlapiStringToDate(holiday
				        .getValue('custrecord_mhc_date'));
				var imageFileId = holiday.getValue('custrecord_mhc_image');
				var imageUrl = "";

				if (imageFileId) {
					var imageFile = nlapiLoadFile(imageFileId);
					imageUrl = urlPrefix + imageFile.getURL();
				}

				if (holidayDate < today) {
					status = 3;
				} else {

					if (nextHolidayFound) {
						status = 1;
					} else {
						status = 2;
						nextHolidayFound = true;
					}
				}

				holidayList.push({
				    Date : holiday.getValue('custrecord_mhc_date'),
				    Name : holiday.getValue('custrecord_mhc_title'),
				    ImageUrl : imageUrl,
				    Status : status
				});
			});
		}

		return holidayList;
	} catch (err) {
		nlapiLogExecution('error', 'generateHolidayList', err);
		throw err;
	}
}

function generateCutomerHolidayList(customerId, subsidiary) {

	try {
		var searchHoliday = nlapiSearchRecord('customrecordcustomerholiday',
		        null, [
		                new nlobjSearchFilter('custrecordcustomersubsidiary',
		                        null, 'anyof', subsidiary),
		                new nlobjSearchFilter('custrecord13', null, 'anyof',
		                        customerId),
		                new nlobjSearchFilter('custrecordholidaydate', null,
		                        'onorafter', 'startofthisyear'),
		                new nlobjSearchFilter('custrecordholidaydate', null,
		                        'onorbefore', 'thisyear'),
		                new nlobjSearchFilter('isinactive', null, 'is', 'F') ],
		        [ new nlobjSearchColumn('custrecordholidaydate').setSort(),
		                new nlobjSearchColumn('custrecordholidaydescription') ]);

		var holidayList = [];
		var today = new Date();
		var nextHolidayFound = false;

		if (searchHoliday) {

			searchHoliday.forEach(function(holiday) {
				var status = '';
				var holidayDate = nlapiStringToDate(holiday
				        .getValue('custrecord_date'));

				if (holidayDate < today) {
					status = 3;
				} else {

					if (nextHolidayFound) {
						status = 1;
					} else {
						status = 2;
						nextHolidayFound = true;
					}
				}

				holidayList.push({
				    Date : holiday.getValue('custrecordholidaydate'),
				    Name : holiday.getValue('custrecordholidaydescription'),
				    Status : 1
				});
			});
		}

		return holidayList;
	} catch (err) {
		nlapiLogExecution('error', 'generateCutomerHolidayList', err);
		throw err;
	}
}