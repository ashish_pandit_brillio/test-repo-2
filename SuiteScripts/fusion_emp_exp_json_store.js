function postRESTlet(dataIn)
{
	try
	{
			nlapiLogExecution('Debug','dataIn',JSON.stringify(dataIn));
			var Data_Array; //To store the JSON value
			var request_Type = 'GET';
			var response = new Response();
			switch(request_Type)
			{
				case M_Constants.Request.Get:
				if(!dataIn instanceof Array)
				{
                    response.Data = "Some error with the data sent";
                    response.Status = false;
				}
			    else
				{
					Data_Array = new Array();
					for(var i = 0 ; i < dataIn.length ; i++)
					{
						Data_Array[i] = dataIn[i];
						nlapiLogExecution('debug','Data_Array',dataIn[i]);
					}
					//nlapiLogExecution('debug','dataIn',JSON.stringify(dataIn));

					response.Data = employeeExperience(dataIn);
                    response.Status = true;
                }
			break;
			}
	}

	catch(error)
	{
		nlapiLogExecution('debug','postRESTlet',error);
		response.Data = error;
		response.status = false;
	}

	nlapiLogExecution('debug', 'response', JSON.stringify(response));
    return response;
}

function employeeExperience(dataIn)
{
	try
	{
		var emp_List = []; //To keep the Count of employees
		var o_data_employeeData = dataIn.DATA_DS.G_1.G_2.FILE_FRAGMENT.EmploymentExtract.EmployeeData;
		if(_logValidation(o_data_employeeData))
		{
			var date_time_value = nlapiDateToString(new Date(), 'datetimetz');
			nlapiLogExecution('DEBUG', 'Checking Whether Object or Array');

		if (isArray(o_data_employeeData))
			{
				nlapiLogExecution('debug','Length is ',o_data_employeeData.length);
				//nlapiLogExecution('DEBUG', 'o_data_employeeData', JSON.stringify(o_data_employeeData));
				for (var count = 0 ; count < o_data_employeeData.length ; count++)
				{
					var rec = nlapiCreateRecord('customrecord_fu_employee_exp_json_master',{recordmode: 'dynamic'});
					var o_data_employeeFields = o_data_employeeData[count];
					nlapiLogExecution('debug','Data-Check',JSON.stringify(o_data_employeeFields));
					rec.setFieldValue('custrecord_fu_emp_exp_json',JSON.stringify(o_data_employeeFields));
					rec.setFieldValue('custrecord_fu_emp_exp_type',1);
					rec.setFieldValue('custrecord_fu_emp_exp_date_time',date_time_value);
					var id = nlapiSubmitRecord(rec , false , true);
					nlapiLogExecution('debug','XML data','Record id:' +id);
				}
			}
			else
			{
				//object
				var rec = nlapiCreateRecord('customrecord_fu_employee_exp_json_master',{recordmode: 'dynamic'});
				nlapiLogExecution('debug','Data-Check',JSON.stringify(o_data_employeeData));
				rec.setFieldValue('custrecord_fu_emp_exp_json',JSON.stringify(o_data_employeeData));
				rec.setFieldValue('custrecord_fu_emp_exp_type',1);
				rec.setFieldValue('custrecord_fu_emp_exp_date_time',date_time_value);
				var id = nlapiSubmitRecord(rec , false , true);
				nlapiLogExecution('debug','XML data','Record id:' +id);
			}
		}
	}

	catch(e)
	{
		nlapiLogExecution('Error','Process Error',e);
		//nlapiSetFieldValue('custrecord_fi_json_error_log', e);
		nlapiLogExecution('Debug','Catch Block');
		
	}

}

function _logValidation(value) {
    if (value != null && value != '' && value != undefined && value.toString() != undefined && value.toString() != 'NaN' && value != NaN && value != 'undefined' && value != ' ') {
        return true;
    } else {
        return false;
    }
}