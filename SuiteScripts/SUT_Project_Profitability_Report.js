/**
 * P/L report for managers
 * 
 * Version Date Author Remarks
 * 
 * 1.00 10 Mar 2016 amol.sahijwani
 * 
 * 2.00 16 May 2016 Nitish Mishra - Refactored and gave access to client
 * partners and regional heads
 */

function suitelet(request, response) {
	try {
		var str = request.getBody();

		var context = nlapiGetContext();

		var i_user_id = nlapiGetUser();

		var a_project_list = new Array();

		var s_selected_project_name = '';

		var a_selected_project_list = request.getParameterValues('project');
		nlapiLogExecution('debug', 'selected project', JSON
		        .stringify(a_selected_project_list));

		var showAll = false;

		var s_from = '';
		var s_to = '';

		if (a_selected_project_list == null
		        || a_selected_project_list.length == 0) {
			showAll = true;
		}

		if (request.getParameter('user') != null
		        && request.getParameter('user') != '' && i_user_id == '1582') {
			i_user_id = request.getParameter('user');
		}
		
		if(i_user_id == 39108)
			i_user_id = 1543;

		var search = nlapiLoadSearch('transaction', 1261);
		var filters = search.getFilters();
		
		//Search for exchange Rate
		var f_rev_curr = 0;
		var f_cost_curr = 0;
		var filters = [];
	
		//var filters = [];
		//filters.push(new nlobjSearchFilter('formulatext', null, 'is', s_mont).setFormula('{custrecord_pl_currency_exchange_month}')); //NOTE : Capital 'Y' in Year
		//filters.push(new nlobjSearchFilter('formulatext', null, 'is', s_year_).setFormula('{custrecord_pl_currency_exchange_year}'));
		var a_dataVal = {};
		var dataRows =[];
		
		var column = new Array();
		column[0] = new nlobjSearchColumn('custrecord_pl_currency_exchange_revnue_r'); //Sandbox 'custrecord_pl_currency_exchange_rev_ex_r'
		column[1] = new nlobjSearchColumn('custrecord_pl_currency_exchange_cost'); //Sandbox 'custrecord_pl_currency_exchange_cost_ex_'
		column[2] = new nlobjSearchColumn('custrecord_pl_currency_exchange_year');   
		column[3] = new nlobjSearchColumn('custrecord_pl_currency_exchange_month');
		
		var currencySearch = nlapiSearchRecord('customrecord_pl_currency_exchange_rates',null,null,column);
		if(currencySearch){
			for(var i_indx=0;i_indx < currencySearch.length;i_indx++){
				
				a_dataVal = {
						s_month : currencySearch[i_indx].getText('custrecord_pl_currency_exchange_month'),
						i_year : currencySearch[i_indx].getText('custrecord_pl_currency_exchange_year'),
						rev_rate : currencySearch[i_indx].getValue('custrecord_pl_currency_exchange_revnue_r'),
						cost_rate : currencySearch[i_indx].getValue('custrecord_pl_currency_exchange_cost')
						
				};
				dataRows.push(a_dataVal);
				
			}
		}

		// get the project list the user has access to
		if (nlapiGetUser() == 7905 || nlapiGetUser() == 41571 ||  nlapiGetUser() ==  42305 || nlapiGetUser() == 19967) {
			i_user_id = 1614;
		}

		nlapiLogExecution('debug', 'user', i_user_id);

		var project_search_results = getTaggedProjectList(i_user_id);

		// create html for select using the project data
		var s_project_options = '';

		for (var i = 0; project_search_results != null
		        && i < project_search_results.length; i++) {
			var i_project_id = project_search_results[i].getValue('internalid');
			var s_project_number = project_search_results[i]
			        .getValue('entityid');
			var s_project_name = project_search_results[i].getValue('jobname');

			var s_selected_project = request.getParameter('project');

			a_project_list.push(s_project_number);

			var s_selected = '';
			if ((a_selected_project_list != null && a_selected_project_list
			        .indexOf(s_project_number) != -1)
			        || showAll == true) {
				s_selected = ' selected="selected" ';

				if (s_selected_project_name != '') {
					s_selected_project_name += ', ';
				}

				s_selected_project_name += s_project_number + ' '
				        + s_project_name;
			}

			s_project_options += '<option value="' + s_project_number + '" '
			        + s_selected + '>' + s_project_number + ' '
			        + s_project_name + '</option>';
		}

		if (a_selected_project_list == null
		        || a_selected_project_list.length == 0) {
			if (a_project_list.length > 0) {
				a_selected_project_list = a_project_list;

				showAll = true;
			} else {
				a_selected_project_list = new Array();
			}
		}

		if (a_selected_project_list != null
		        && a_selected_project_list.length != 0) {
			var s_formula = '';

			for (var i = 0; i < a_selected_project_list.length; i++) {
				if (i != 0) {
					s_formula += " OR";
				}

				s_formula += " SUBSTR({custcolprj_name}, 0,9) = '"
				        + a_selected_project_list[i] + "' ";
			}

			var projectFilter = new nlobjSearchFilter('formulanumeric', null,
			        'equalto', 1);

			projectFilter.setFormula("CASE WHEN " + s_formula
			        + " THEN 1 ELSE 0 END");

			filters = filters.concat([ projectFilter ]);
		} else {
			var projectFilter = new nlobjSearchFilter('formulanumeric', null,
			        'equalto', 1);

			projectFilter.setFormula("0");

			filters = filters.concat([ projectFilter ]);
		}

		if (request.getParameter('from') != null
		        && request.getParameter('from') != '') {
			s_from = request.getParameter('from');

			// filters = filters.concat([new nlobjSearchFilter('trandate', null,
			// 'onorafter', s_from)]);
		}
		var s_exclude =  request.getParameter('efc') ;
		//var s_exclude ;
		//nlapiLogExecution('debug','Exclude Value Parameter',request.getParameter('efc'));
		//s_exclude =  request.getParameter('efc') ;
		nlapiLogExecution('debug','Exclude Value',s_exclude);
		if (request.getParameter('to') != null
		        && request.getParameter('to') != '') {
			s_to = request.getParameter('to');

			// filters = filters.concat([new nlobjSearchFilter('trandate', null,
			// 'onorbefore', s_to)]);
		}

		var columns = search.getColumns();

		columns[0].setSort(false);
		columns[3].setSort(true);
		columns[9].setSort(false);

		var search_results = searchRecord('transaction', null, filters, [
		        columns[2], columns[1], columns[0], columns[3], columns[4],
		        columns[5], columns[8], columns[9] ,columns[10]]);

		// Get the facility cost

		var s_facility_cost_filter = '';

		for (var i = 0; i < a_selected_project_list.length; i++) {
			s_facility_cost_filter += "'" + a_selected_project_list[i] + "'";

			if (i != a_selected_project_list.length - 1) {
				s_facility_cost_filter += ",";
			}
		}

		var facility_cost_filters = new Array();
		facility_cost_filters[0] = new nlobjSearchFilter('formulanumeric',
		        null, 'equalto', 1);
		if (s_facility_cost_filter != '') {
			facility_cost_filters[0]
			        .setFormula('CASE WHEN {custrecord_arpm_project.entityid} IN ('
			                + s_facility_cost_filter + ') THEN 1 ELSE 0 END');
		} else {
			facility_cost_filters[0].setFormula('0');
		}

		var facility_cost_columns = new Array();
		facility_cost_columns[0] = new nlobjSearchColumn(
		        'custrecord_arpm_period');
		facility_cost_columns[1] = new nlobjSearchColumn(
		        'custrecord_arpm_num_allocated_resources');
		facility_cost_columns[2] = new nlobjSearchColumn(
		        'custrecord_arpm_facility_cost_per_person');
		facility_cost_columns[3] = new nlobjSearchColumn(
		        'custrecord_arpm_location');

		var facility_cost_search_results = searchRecord(
		        'customrecord_allocated_resources_per_mon', null,
		        facility_cost_filters, facility_cost_columns);

		var o_json = new Object();

		var o_data = {
		    'Revenue' : [],
		    'Discounts' : [],
		    'People Cost' : [],
		    'Facility Cost' : [],
		    'Travel' : [],
		    'Expense' : []
		};

		var new_object = null;

		var s_period = '';

		var a_period_list = [];

		var a_category_list = [];

		var a_group = [];

		var a_income_group = [];

		for (var i = 0; search_results != null && i < search_results.length; i++) {
			var period = search_results[i].getText(columns[0]);

			//Code updated by Deepak, Dated - 21 Mar 17
			var s_month_year = period.split(' ');
			var s_mont = s_month_year[0];
			s_mont = getMonthCompleteName(s_mont);
			var s_year_ = s_month_year[1];
			var f_revRate = 66.0;
			var f_costRate = 66.0;
					
			//Fetch matching cost and rev rate convertion rate
			for(var data_indx = 0; data_indx < dataRows.length ; data_indx ++){
				if(dataRows[data_indx].s_month == s_mont && dataRows[data_indx].i_year == s_year_){
					f_revRate = dataRows[data_indx].rev_rate;
					f_costRate = dataRows[data_indx].cost_rate;
				}
			}
			/*//Search for exchange Rate
			var f_rev_curr = 0;
			var f_cost_curr = 0;
			var filters = [];
			filters.push(new nlobjSearchFilter('custrecord_pl_currency_exchange_month',null,'anyof',s_mont));
			filters.push(new nlobjSearchFilter('custrecord_pl_currency_exchange_year',null,'anyof',s_year_));
			//add search filters
			var filters = [];
			filters.push(new nlobjSearchFilter('formulatext', null, 'is', s_mont).setFormula('{custrecord_pl_currency_exchange_month}')); //NOTE : Capital 'Y' in Year
			filters.push(new nlobjSearchFilter('formulatext', null, 'is', s_year_).setFormula('{custrecord_pl_currency_exchange_year}'));
			
			var column = new Array();
			column[0] = new nlobjSearchColumn('custrecord_pl_currency_exchange_rev_ex_r');
			column[1] = new nlobjSearchColumn('custrecord_pl_currency_exchange_cost_ex_');
			
			var currencySearch = nlapiSearchRecord('customrecord_pl_currency_exchange_rates',null,filters,column);
			if(currencySearch){
				f_rev_curr =  currencySearch[0].getValue('custrecord_pl_currency_exchange_rev_ex_r');
				f_cost_curr =  currencySearch[0].getValue('custrecord_pl_currency_exchange_cost_ex_');
			}*/
			
			var transaction_type = search_results[i].getText(columns[8]);

			var transaction_date = search_results[i].getValue(columns[9]);
			
			var i_subsidiary = search_results[i].getValue(columns[10]);

			var amount = parseFloat(search_results[i].getValue(columns[1]));

			var category = search_results[i].getValue(columns[3]);
			
			//AMount Convertion Logic
			if(parseInt(i_subsidiary) != parseInt(2)){
			if (category != 'Revenue' && category != 'Other Income'
		        && category != 'Discounts' ) {
			//f_total_expense += o_data[s_category][j].amount;
				amount = parseFloat(amount) /parseFloat(f_costRate);	
		} else {
				amount = parseFloat(amount) /parseFloat(f_revRate);
			//f_total_revenue += o_data[s_category][j].amount;
			}
		}	//END
			

			if (!(category in o_data)) {
				o_data[category] = new Array();
			}

			var isWithinDateRange = true;

			var d_transaction_date = nlapiStringToDate(transaction_date,
			        'datetimetz');

			if (s_from != '') {
				var d_from = nlapiStringToDate(s_from, 'datetimetz');

				if (d_transaction_date < d_from) {
					isWithinDateRange = false;
				}
			}

			if (s_to != '') {
				var d_to = nlapiStringToDate(s_to, 'datetimetz');

				if (d_transaction_date > d_to) {
					isWithinDateRange = false;
				}
			}

			if (isWithinDateRange == true) {
				var i_index = a_period_list.indexOf(period);

				if (i_index == -1) {
					a_period_list.push(period);
				}

				i_index = a_period_list.indexOf(period);
			}

			o_data[category].push({
			    'period' : period,
			    'amount' : amount,
			    'num' : search_results[i].getValue(columns[4]),
			    'memo' : search_results[i].getValue(columns[5]),
			    'type' : transaction_type,
			    'dt' : transaction_date,
			    'include' : isWithinDateRange
			});
		}

		// Add facility cost
		o_data['Facility Cost'] = new Array();

		for (var i = 0; facility_cost_search_results != null
		        && i < facility_cost_search_results.length; i++) {
			var s_period = facility_cost_search_results[i]
			        .getValue(facility_cost_columns[0]);
			var f_allocated_resources = facility_cost_search_results[i]
			        .getValue(facility_cost_columns[1]);
			var f_cost_per_resource = facility_cost_search_results[i]
			        .getValue(facility_cost_columns[2]);
			var s_location = facility_cost_search_results[i]
			        .getText(facility_cost_columns[3]);

			if (f_cost_per_resource == '') {
				f_cost_per_resource = 0;
			}
			if(s_exclude == 'true'){
				o_data['Facility Cost'].push({
					'period' : s_period,
					'amount' : 0,
					'num' : '',
					'memo' : 'Facility cost for ' + f_allocated_resources
					+ ' resources at ' + s_location + '.',
					'type' : 'Facility Cost',
					'dt' : '',
					'include' : a_period_list.indexOf(s_period) != -1
				});
			}
			else{
			o_data['Facility Cost'].push({
			    'period' : s_period,
			    'amount' : f_allocated_resources * f_cost_per_resource,
			    'num' : '',
			    'memo' : 'Facility cost for ' + f_allocated_resources
			            + ' resources at ' + s_location + '.',
			    'type' : 'Facility Cost',
			    'dt' : '',
			    'include' : a_period_list.indexOf(s_period) != -1
			});
			}
		}

		var o_total = new Array();
		var o_period_total = new Array();

		var f_total_revenue = 0.0;
		var f_total_expense = 0.0;
		var f_profit_percentage = 0.0;

		var isDateRangeSelected = false;

		if (s_from != '' || s_to != '') {
			isDateRangeSelected = true;
		}

		var a_chart = new Array();

		// a_chart.push(['x'].concat(a_period_list));

		for ( var s_category in o_data) {
			var a_temp = [];

			o_total[s_category] = 0.0;
			o_period_total[s_category] = 0.0;

			for (var i = 0; i < a_period_list.length; i++) {
				a_temp.push(0.0);
			}

			for (var j = 0; j < o_data[s_category].length; j++) {
				var i_index = a_period_list
				        .indexOf(o_data[s_category][j].period);

				a_temp[i_index] += o_data[s_category][j].amount;

				if (o_data[s_category][j].include == true) {
					o_period_total[s_category] += o_data[s_category][j].amount;
				}

				o_total[s_category] += o_data[s_category][j].amount;

				if (s_category != 'Revenue' && s_category != 'Other Income'
				        && s_category != 'Discounts') {
					f_total_expense += o_data[s_category][j].amount;
				} else {
					f_total_revenue += o_data[s_category][j].amount;
				}
			}

			if (s_category != 'Revenue' && s_category != 'Other Income'
			        && s_category != 'Discounts') {
				a_group.push(s_category);
			} else {
				a_income_group.push(s_category);
			}

			a_chart.push([ s_category ].concat(a_temp));
		}

		var a_profit = [ 'Net Margin(%)', 0.0 ];

		var a_expense = [ '', 0.0 ];

		var a_revenue = [ '', 0.0 ];

		// Calculate Profit
		for (var i = 0; i < a_chart.length; i++) {

			if (i == 0) {
				for (j = 1; j < a_chart[i].length; j++) {
					a_profit.push(0.0);

					a_expense.push(0.0);

					a_revenue.push(0.0);
				}
			}

			for (var j = 1; j < a_chart[i].length; j++) {

				if (a_chart[i][0] == 'Revenue'
				        || a_chart[i][0] == 'Other Income'
				        || a_chart[i][0] == 'Discounts') {
					a_profit[j] = a_profit[j] + a_chart[i][j];

					a_revenue[j] = a_revenue[j] + a_chart[i][j];

					a_profit[a_profit.length - 1] += a_chart[i][j];
					a_revenue[a_revenue.length - 1] += a_chart[i][j];
				} else {
					a_profit[j] = a_profit[j] - a_chart[i][j];

					a_expense[j] = a_expense[j] + a_chart[i][j];

					a_profit[a_profit.length - 1] -= a_chart[i][j];
					a_expense[a_expense.length - 1] += a_chart[i][j];
				}
			}
		}

		for (var i = 1; i < a_profit.length; i++) {
			if (a_revenue[i] != 0.0) {
				a_profit[i] = (a_profit[i] / a_revenue[i] * 100).toFixed(2)
				        + '%';
			} else {
				a_profit[i] = ' - ';
			}
		}

		if (f_total_revenue != 0) {
			f_profit_percentage = (((f_total_revenue - f_total_expense) / f_total_revenue) * 100)
			        .toFixed(2)
			        + '%';
		} else {
			f_profit_percentage = ' - ';
		}

		// a_chart.push(a_profit);

		// o_json.push(new_object);

		// Display the table
		var s_table = '<table>';
		// Header
		s_table += '<thead><tr><th></th>';
		for (var i = 0; i < a_period_list.length; i++) {
			s_table += '<th>' + a_period_list[i] + '</th>';
		}
		if (isDateRangeSelected == true) {
			s_table += '<th>Total</th>';
		}

		s_table += '<th>Overall</th>';

		s_table += '</thead></tr>';
		// Table
		for (var i = 0; i < a_chart.length; i++) {
			if (a_chart[i][0] == 'People Cost') {
				s_table += '<tr style="background-color:#CCC;"><td>Net Revenue</td>';
				for (var j = 1; j < a_revenue.length; j++) {
					s_table += '<td style="text-align:right;">'
					        + accounting.formatNumber(a_revenue[j]) + '</td>';
				}
				if (isDateRangeSelected == true) {
					s_table += '<td style="text-align:right;">'
					        + accounting.formatNumber(f_total_revenue)
					        + '</td>';
				}
				s_table += '</tr>';
			}

			s_table += '<tr>';
			for (var j = 0; j < a_chart[i].length; j++) {
				var strValue = '';
				if (j > 0) {
					if (a_chart[i][0] != 'People Cost') {
						strValue += '<a href="javascript:displayPopup(\''
						        + a_chart[i][0] + '\',\''
						        + a_period_list[j - 1] + '\');">';
					}

					strValue += accounting.formatNumber(a_chart[i][j]);
					a_chart[i][j] = Math.round(a_chart[i][j]);

					if (a_chart[i][0] != 'People Cost') {
						strValue += '</a>';
					}

					s_table += '<td style="text-align:right;">' + strValue
					        + '</td>';
				} else {
					strValue += a_chart[i][j];

					s_table += '<td>' + strValue + '</td>';
				}
			}
			s_table += '<td style="text-align:right;">'
			        + accounting.formatNumber(o_period_total[a_chart[i][0]])
			        + '</td>';
			if (isDateRangeSelected == true) {
				s_table += '<td style="text-align:right;">'
				        + accounting.formatNumber(o_total[a_chart[i][0]])
				        + '</td>';
			}
			s_table += '</tr>';

			if (i == a_chart.length - 1) {
				s_table += '<tr style="background-color:#CCC;"><td>Total Cost</td>';
				for (var j = 1; j < a_expense.length; j++) {
					s_table += '<td style="text-align:right;">'
					        + accounting.formatNumber(a_expense[j]) + '</td>';
				}
				if (isDateRangeSelected == true) {
					s_table += '<td style="text-align:right;">'
					        + accounting.formatNumber(f_total_expense)
					        + '</td>';
				}
				s_table += '</tr>';

				s_table += '<tr style="font-weight:bold;"><td>Net Margin</td>';
				for (var j = 1; j < a_revenue.length; j++) {
					s_table += '<td style="text-align:right;">'
					        + accounting.formatNumber(a_revenue[j]
					                - a_expense[j]) + '</td>';
				}
				if (isDateRangeSelected == true) {
					s_table += '<td style="text-align:right;">'
					        + accounting.formatNumber(f_total_revenue
					                - f_total_expense) + '</td>';
				}
				s_table += '</tr>';
			}
		}

		s_table += '<tfoot><tr style="font-weight:bold;">';

		for (var i = 0; i < a_profit.length; i++) {
			s_table += '<td>' + a_profit[i] + '</td>';
		}
		if (isDateRangeSelected == true) {
			s_table += '<td>' + f_profit_percentage + '</td>';
		}
		s_table += '</tr></tfoot>';

		s_table += '</table>';

		// Define colors
		var a_colors = [ '#7293cb', '#808585', '#ab6857', '#84ba5b', '#ccc210',
		        '#d35e60', '#9067a7', '#e1974c' ];

		var o_bar_colors = new Object();
		var indx = 0;
		for ( var s_category in o_data) {
			if (indx < a_colors.length) {
				o_bar_colors[s_category] = a_colors[indx];

				indx++;
			}
		}

		// added by Nitish
		// generate the HTML content for the lead indicator table
		var lead_indicator_html = "";
		// if (nlapiGetUser() == 9673) {
		lead_indicator_html = generateLeadIndicatorTable(
		        a_selected_project_list, a_period_list, s_from, s_to);
		// }

		var objValues = new Object();
		objValues.a_chart = JSON.stringify(a_chart);
		objValues.a_group = JSON.stringify(a_group);
		objValues.a_income_group = JSON.stringify(a_income_group);
		objValues.a_period_list = JSON.stringify(a_period_list);
		objValues.s_project_options = s_project_options;
		objValues.s_table = s_table;
		objValues.s_cost_indicator_table = lead_indicator_html;
		objValues.s_table = s_table;
		objValues.s_from = s_from;
		objValues.s_to = s_to;
		objValues.s_exclude = s_exclude;
		objValues.s_title = s_selected_project_name;
		objValues.json_data = JSON.stringify(o_data);
		objValues.url = nlapiResolveURL('SUITELET', context.getScriptId(),
		        context.getDeploymentId());
		objValues.i_user_id = i_user_id;
		objValues.i_script_id = request.getParameter('script');
		objValues.i_deployment_id = request.getParameter('deploy');
		objValues.colors = JSON.stringify(o_bar_colors);

		var s_html = displayHTML(141181, objValues);

		response.write(s_html);

		nlapiLogExecution('AUDIT', 'Run By', '');

		return;

		// Instantiate a report definition to work with
		var reportDefinition = nlapiCreateReportDefinition();

		reportDefinition.addRowHierarchy('custrecord_type', 'Type', 'TEXT');
		reportDefinition.addRowHierarchy('custrecord_category', 'Category',
		        'TEXT');
		reportDefinition.addRowHierarchy('custrecord_sub_category',
		        'Sub Category', 'TEXT');

		reportDefinition.addColumn('custrecord_transaction_number', false,
		        'Transaction Number', null, 'TEXT', null);
		reportDefinition.addColumn('custrecord_remarks', false, 'Remarks',
		        null, 'TEXT', null);

		var columnQuarter = reportDefinition.addColumnHierarchy(
		        'custrecord_quarter', 'Quarter', null, 'TEXT');
		var columnPeriod = reportDefinition.addColumnHierarchy(
		        'custrecord_period', 'Period', columnQuarter, 'TEXT');

		reportDefinition.addColumn('custrecord_amount', true, 'Amount',
		        columnPeriod, 'CURRENCY', null);

		reportDefinition.addSearchDataSource('transaction', null, filters,
		        columns, {
		            'custrecord_type' : columns[16],
		            'custrecord_quarter' : columns[15],
		            'custrecord_period' : columns[1],
		            'custrecord_category' : columns[17],
		            'custrecord_sub_category' : columns[11],
		            'custrecord_amount' : columns[6],
		            'custrecord_transaction_number' : columns[3],
		            'custrecord_remarks' : columns[5]
		        });

		// Create a form to build the report on
		var form = nlapiCreateReportForm('Revenue Trend Report');

		// Build the form from the report definition
		var pvtTable = reportDefinition.executeReport(form);

		// Write the form to the browser
		response.writePage(form);
	} catch (err) {
		displayErrorForm(err);
	}
}

function displayHTML(i_file_id, objValues) {
	var file = nlapiLoadFile(i_file_id); // load the HTML file
	var contents = file.getValue(); // get the contents
	contents = replaceValues(contents, objValues);
	return contents;
}

// Used to display the html, by replacing the placeholders
function replaceValues(content, oValues) {
	for (param in oValues) {
		// Replace null values with blank
		var s_value = (oValues[param] == null) ? '' : oValues[param];

		// Replace content
		content = content.replace(new RegExp('{{' + param + '}}', 'gi'),
		        s_value);
	}

	return content;
}

function createPeriodList(startDate, endDate) {
	var d_startDate = nlapiStringToDate(startDate);
	var d_endDate = nlapiStringToDate(endDate);

	var arrPeriod = [];

	for (var i = 0;; i++) {
		var currentDate = nlapiAddMonths(d_startDate, i);
		arrPeriod.push({
		    Name : getMonthName(currentDate),
		    StartDate : getMonthStartDate(currentDate),
		    EndDate : getMonthEndDate(currentDate)
		});

		if (getMonthEndDate(currentDate) >= d_endDate) {
			break;
		}
	}

	var today = new Date();

	// remove the current month
	if (d_endDate.getMonth() >= today.getMonth()) {
		arrPeriod.pop();
	}

	return arrPeriod;
}

function getMonthEndDate(currentDate) {
	return nlapiAddDays(nlapiAddMonths(currentDate, 1), -1);
}

function getMonthStartDate(currentDate) {
	var month = currentDate.getMonth() + 1;
	var year = currentDate.getFullYear();
	var s_startDate = month + "/1/" + year;
	var d_startDate = nlapiStringToDate(s_startDate);
	return d_startDate;
}

function getMonthName(currentDate) {
	var monthNames = [ "JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JUL", "AUG",
	        "SEP", "OCT", "NOV", "DEC" ];
	return monthNames[currentDate.getMonth()] + " " + currentDate.getFullYear();
}

function generateLeadIndicatorTable(a_selected_project_list, a_period_list,
        startDate, endDate)
{
	try {
		if (!startDate) {
			startDate = '1/1/2015';
		}

		if (!endDate) {
			endDate = nlapiDateToString(new Date(), 'date');
		}

		nlapiLogExecution('debug', 'a_period_list', JSON
		        .stringify(a_period_list));

		var periodList = createPeriodList(startDate, endDate);

		// get all the project related utilization data
		var filters = [
		        new nlobjSearchFilter('isinactive', null, 'is', 'F'),
		        new nlobjSearchFilter('custrecord_urt_start_date', null,
		                'within', startDate, endDate) ];

		var s_formula = '';
		for (var i = 0; i < a_selected_project_list.length; i++) {
			if (i > 0) {
				s_formula += " OR";
			}
			s_formula += " SUBSTR({custrecord_urt_project}, 0,9) = '"
			        + a_selected_project_list[i] + "' ";
		}
		var projectFilter = new nlobjSearchFilter('formulanumeric', null,
		        'equalto', 1);
		projectFilter.setFormula("CASE WHEN " + s_formula
		        + " THEN 1 ELSE 0 END");
		filters.push(projectFilter);

		var utilizationSearch = nlapiSearchRecord(
		        'customrecord_utilization_report_trend',
		        null,
		        filters,
		        [
		                new nlobjSearchColumn('custrecord_urt_start_date',
		                        null, 'group').setSort(),
		                new nlobjSearchColumn('custrecord_urt_onsite_billed',
		                        null, 'sum'),
		                new nlobjSearchColumn('custrecord_urt_onsite_unbilled',
		                        null, 'sum'),
		                new nlobjSearchColumn('custrecord_urt_offsite_billed',
		                        null, 'sum'),
		                new nlobjSearchColumn(
		                        'custrecord_urt_offsite_unbilled', null, 'sum'),
		                new nlobjSearchColumn(
		                        'custrecord_urt_utilization_percent', null,
		                        'avg'),

		                new nlobjSearchColumn('custrecord_urt_salaried_count',
		                        null, 'sum'),
		                new nlobjSearchColumn('custrecord_urt_contract_count',
		                        null, 'sum'),
		                new nlobjSearchColumn('custrecord_urt_contract_cost',
		                        null, 'sum'),
		                new nlobjSearchColumn('custrecord_urt_salaried_cost',
		                        null, 'sum'),
		                new nlobjSearchColumn(
		                        'custrecord_urt_contractor_percent', null,
		                        'avg'),
		                new nlobjSearchColumn(
		                        'custrecord_urt_contractor_cost_percent', null,
		                        'avg') ]);

		// generate HTML for table
		var table_css = "";
		table_css += "<style>";

		table_css += "#cost_indicator tr:nth-child(even) {background: #e6e6ff}";
		table_css += "#cost_indicator tr:nth-child(odd) {background: white}";
		table_css += "#cost_indicator th {background: linear-gradient(#49708f, #293f50) !important}";
		table_css += "</style>";

		var s_table = '<table id="cost_indicator">';

		// Header
		s_table += '<thead><tr><th></th>';
		periodList.forEach(function(period) {
			s_table += '<th>' + period.Name + '</th>';
		});
		s_table += '</tr></thead>';

		// Data Rows
		var offSiteBilledRow = "<td>Offsite Billed (pm)</td>";
		var offSiteUnbilledRow = "<td>Offsite Unbilled (pm)</td>";
		var onSiteBilledRow = "<td>Onsite Billed (pm)</td>";
		var onSiteUnbilledRow = "<td>Onsite Unbilled (pm)</td>";
		var utilizationRow = "<td>% Utilization</td>";

		var salariedCount = "<td># Salaried (pm)</td>";
		var salariedCost = "<td>Salaried Cost ($)</td>";
		var contractCount = "<td># Contractor (pm)</td>";
		var contractCost = "<td>Contractor Cost ($)</td>";

		var salariedCountRatio = "<td>Contract Count Ratio</td>";
		var salariedCostRatio = "<td>Contract Cost Ratio</td>";

		periodList
		        .forEach(function(period) {

			        var columnFound = false;
			        for (var i = 0; i < utilizationSearch.length; i++) {
				        var searchDate = utilizationSearch[i].getValue(
				                'custrecord_urt_start_date', null, 'group');
				        var d_searchDate = nlapiStringToDate(searchDate);

				        if (period.StartDate <= d_searchDate
				                && d_searchDate <= period.EndDate) {
					        columnFound = true;
					        offSiteBilledRow += '<td style="text-align:right;">'
					                + parseFloat(
					                        utilizationSearch[i]
					                                .getValue(
					                                        'custrecord_urt_offsite_billed',
					                                        null, 'sum'))
					                        .toFixed(2) + '</td>';
					        offSiteUnbilledRow += '<td style="text-align:right;">'
					                + parseFloat(
					                        utilizationSearch[i]
					                                .getValue(
					                                        'custrecord_urt_offsite_unbilled',
					                                        null, 'sum'))
					                        .toFixed(2) + '</td>';
					        onSiteBilledRow += '<td style="text-align:right;">'
					                + parseFloat(
					                        utilizationSearch[i]
					                                .getValue(
					                                        'custrecord_urt_onsite_billed',
					                                        null, 'sum'))
					                        .toFixed(2) + '</td>';

					        onSiteUnbilledRow += '<td style="text-align:right;">'
					                + parseFloat(
					                        utilizationSearch[i]
					                                .getValue(
					                                        'custrecord_urt_onsite_unbilled',
					                                        null, 'sum'))
					                        .toFixed(2) + '</td>';
					        utilizationRow += '<td style="text-align:right;">'
					                + parseFloat(
					                        utilizationSearch[i]
					                                .getValue(
					                                        'custrecord_urt_utilization_percent',
					                                        null, 'avg'))
					                        .toFixed(2) + '%</td>';

					        salariedCountRatio += '<td style="text-align:right;">'
					                + parseFloat(
					                        utilizationSearch[i]
					                                .getValue(
					                                        'custrecord_urt_contractor_percent',
					                                        null, 'avg'))
					                        .toFixed(2) + '</td>';

					        salariedCostRatio += '<td style="text-align:right;">'
					                + parseFloat(
					                        utilizationSearch[i]
					                                .getValue(
					                                        'custrecord_urt_contractor_cost_percent',
					                                        null, 'avg'))
					                        .toFixed(2) + '</td>';

					        salariedCount += '<td style="text-align:right;">'
					                + parseFloat(
					                        utilizationSearch[i]
					                                .getValue(
					                                        'custrecord_urt_salaried_count',
					                                        null, 'sum'))
					                        .toFixed(2) + '</td>';

					        contractCount += '<td style="text-align:right;">'
					                + parseFloat(
					                        utilizationSearch[i]
					                                .getValue(
					                                        'custrecord_urt_contract_count',
					                                        null, 'sum'))
					                        .toFixed(2) + '</td>';

					        salariedCost += '<td style="text-align:right;">'
					                + Math.round(utilizationSearch[i].getValue(
					                        'custrecord_urt_salaried_cost',
					                        null, 'sum')) + '</td>';

					        contractCost += '<td style="text-align:right;">'
					                + Math.round(utilizationSearch[i].getValue(
					                        'custrecord_urt_contract_cost',
					                        null, 'sum')) + '</td>';

					        break;
				        }
			        }

			        // if column if not found, add 0
			        if (!columnFound) {
				        offSiteBilledRow += '<td style="text-align:right;">-</td>';
				        offSiteUnbilledRow += '<td style="text-align:right;">-</td>';
				        onSiteBilledRow += '<td style="text-align:right;">-</td>';
				        onSiteUnbilledRow += '<td style="text-align:right;">-</td>';
				        utilizationRow += '<td style="text-align:right;">-</td>';
				        salariedCount += '<td style="text-align:right;">-</td>';
				        salariedCost += '<td style="text-align:right;">-</td>';
				        contractCount += '<td style="text-align:right;">-</td>';
				        contractCost += '<td style="text-align:right;">-</td>';
				        salariedCountRatio += '<td style="text-align:right;">-</td>';
				        salariedCostRatio += '<td style="text-align:right;">-</td>';
			        }
		        });

		s_table += "<tr >" + onSiteBilledRow + "</tr>";
		s_table += "<tr>" + offSiteBilledRow + "</tr>";
		s_table += "<tr>" + onSiteUnbilledRow + "</tr>";
		s_table += "<tr>" + offSiteUnbilledRow + "</tr>";

		s_table += "<tr>" + utilizationRow + "</tr>";

		s_table += "<tr>" + salariedCount + "</tr>";
		s_table += "<tr>" + salariedCost + "</tr>";
		s_table += "<tr>" + contractCount + "</tr>";
		s_table += "<tr>" + contractCost + "</tr>";
		s_table += "<tr>" + salariedCostRatio + "</tr>";
		s_table += "<tr>" + salariedCountRatio + "</tr>";

		if (nlapiGetUser() == '9673') {
		}

		s_table += "</table>";

		return table_css + s_table;
	} catch (err) {
		nlapiLogExecution('ERROR', 'generateLeadIndicatorTable', err);
	}
}

//Get the complete month name
function getMonthCompleteName(month){
	var s_mont_complt_name = '';
	if(month == 'Jan')
		s_mont_complt_name = 'January';
	if(month == 'Feb')
		s_mont_complt_name = 'February';
	if(month == 'Mar')
		s_mont_complt_name = 'March';
	if(month == 'Apr')
		s_mont_complt_name = 'April';
	if(month == 'May')
		s_mont_complt_name = 'May';
	if(month == 'Jun')
		s_mont_complt_name = 'June';
	if(month == 'Jul')
		s_mont_complt_name = 'July';
	if(month == 'Aug')
		s_mont_complt_name = 'August';
	if(month == 'Sep')
		s_mont_complt_name = 'September';
	if(month == 'Oct')
		s_mont_complt_name = 'October';
	if(month == 'Nov')
		s_mont_complt_name = 'November';
	if(month == 'Dec')
		s_mont_complt_name = 'December';
	
	return s_mont_complt_name;
}