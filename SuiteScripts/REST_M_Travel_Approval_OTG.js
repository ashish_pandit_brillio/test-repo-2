/**
 * Travel
 * 
 * Version Date Author Remarks 1.00 27 Nov 2015 nitish.mishra
 * 
 */

function postRESTlet(dataIn) {
	var response = new Response();
	try {
		nlapiLogExecution('debug', 'dataIn', dataIn);
		var employeeId = getUserUsingEmailId(dataIn.EmailId);
		var requestType = dataIn.RequestType;
		var travelId = dataIn.Data.TravelId;
		var travelDetails = dataIn.Data.TravelDetails;
		var remarks = dataIn.Data.Remarks;
		/*var travelDetails = '';
		var remarks = '';
		var employeeId = 1646;
		var requestType = 'APPROVE';
		var travelId = 6351;*/
		switch (requestType) {

			case M_Constants.Request.Get:

				if (travelId) {
					response.Data = getTravelDetail(travelId, employeeId);
					response.Status = true;
				} else {
					response.Data = getTravelPendingApproval(employeeId);
					response.Status = true;
				}
			break;

			case M_Constants.Request.Approve:
				response.Data = approveTravel(travelId, employeeId,
				        travelDetails, remarks);
				response.Status = true;
			break;

			case M_Constants.Request.Reject:
				response.Data = rejectTravel(travelId, employeeId,
				        travelDetails, remarks);
				response.Status = true;
			break;
		}

	} catch (err) {
		nlapiLogExecution('ERROR', 'postRESTlet', err);
		response.Data = err;
	}
	nlapiLogExecution('debug', 'response', JSON.stringify(response));
	return response;
}

function getTravelPendingApproval(employeeId) {
	try {
		var filters = [
		        new nlobjSearchFilter('custrecord_tr_approving_manager_id',
		                null, 'is', employeeId),
		        new nlobjSearchFilter('custrecord_tr_status', null, 'anyof', 2) ];

		var columns = [ new nlobjSearchColumn('custrecord_tr_employee'),
		        new nlobjSearchColumn('name'),
		        new nlobjSearchColumn('custrecord_tr_travel_type'),
		        new nlobjSearchColumn('custrecord_tr_purpose_of_travel'),
		        new nlobjSearchColumn('custrecord_tr_departure_date'),
		        new nlobjSearchColumn('custrecord_tr_status'),
		        new nlobjSearchColumn('custrecord_tr_project'),
		        new nlobjSearchColumn('custrecord_tr_approving_manager_id') ];

		var pendingTravelSearch = nlapiSearchRecord(
		        'customrecord_travel_request', null, filters, columns);

		var travelList = [];

		if (pendingTravelSearch) {
			for (var i = 0; i < pendingTravelSearch.length && i < 50; i++) {
				var travelRequest = new TravelListItem();
				travelRequest.InternalId = pendingTravelSearch[i].getId();
				travelRequest.TravelId = pendingTravelSearch[i]
				        .getValue('name');
				travelRequest.EmployeeName = pendingTravelSearch[i]
				        .getText('custrecord_tr_employee');
				travelRequest.Purpose = pendingTravelSearch[i]
				        .getText('custrecord_tr_purpose_of_travel');
				travelRequest.DepartureDate = pendingTravelSearch[i]
				        .getValue('custrecord_tr_departure_date');
				travelRequest.Type = pendingTravelSearch[i]
				        .getText('custrecord_tr_travel_type');
				travelRequest.Project = pendingTravelSearch[i]
				        .getText('custrecord_tr_project');
				travelList.push(travelRequest);
			}
		}

		nlapiLogExecution('debug', 'travel list', JSON.stringify(travelList));
		return travelList;
	} catch (err) {
		nlapiLogExecution('error', 'getTravelPendingApproval', err);
		throw err;
	}
}

function getTravelDetail(travelId, approverId) {
	try {
	
	//Get Travel Created Date
	var createdDate_ = '';
	
	
	var filters = [];
		filters.push(new nlobjSearchFilter('internalid',null,'anyof',travelId));
		
		var cols = [];
		cols.push(new nlobjSearchColumn('date','systemnotes'));
		
		//var travelCreatedSearch = nlapiSearchRecord('customrecord_travel_request','customsearch1760',filters); //customsearch1885
		var travelCreatedSearch = nlapiSearchRecord('customrecord_travel_request','customsearch1885',filters); //PROD
		// nlapiLogExecution('debug','expensCreatedSearch',expensCreatedSearch.length);
		if(_logValidation(travelCreatedSearch)){
		var result1=travelCreatedSearch[0];
		var column1=result1.getAllColumns(); 
			 createdDate_ = new Date(result1.getValue(column1[0]));	
		     nlapiLogExecution('debug','Created Date',createdDate_);
		}
		var travelRecord = nlapiLoadRecord('customrecord_travel_request',
		        travelId);

		if (travelRecord.getFieldValue('custrecord_tr_approving_manager_id') == approverId) {
			var travelDetails = {
			    InternalId : travelRecord.getId(),
				CreatedDate: createdDate_,
			    EmployeeName : travelRecord
			            .getFieldText('custrecord_tr_employee'),
			    TravelId : travelRecord.getFieldValue('name'),
			    ProjectText : travelRecord
			            .getFieldText('custrecord_tr_project'),
			    ManagerText : travelRecord
			            .getFieldText('custrecord_tr_approving_manager_id'),
			    Description : travelRecord
			            .getFieldValue('custrecord_tr_short_description'),
			    Purpose : travelRecord
			            .getFieldText('custrecord_tr_purpose_of_travel'),
			    Mobile : travelRecord
			            .getFieldValue('custrecord_tr_onsite_contact_number'),
			    TypeText : travelRecord
			            .getFieldText('custrecord_tr_travel_type'),
			    BillableItems : [],
			    Itenary : []
			};

			for (var i = 1; i <= travelRecord
			        .getLineItemCount('recmachcustrecord_trmc_travel_request'); i++) {
				travelDetails.BillableItems.push({
				    Id : travelRecord.getLineItemValue(
				            'recmachcustrecord_trmc_travel_request', 'id', i),
				    Name : travelRecord.getLineItemText(
				            'recmachcustrecord_trmc_travel_request',
				            'custrecord_trmc_item', i),
				    Status : travelRecord.getLineItemValue(
				            'recmachcustrecord_trmc_travel_request',
				            'custrecord_trmc_value', i) == 'T'
				});
			}

			for (var i = 1; i <= travelRecord
			        .getLineItemCount('recmachcustrecord_trli_travel_request'); i++) {
				travelDetails.Itenary.push({
				    DepartureDate : travelRecord.getLineItemValue(
				            'recmachcustrecord_trli_travel_request',
				            'custrecord_trli_departure_date', i),
				    Source : travelRecord.getLineItemValue(
				            'recmachcustrecord_trli_travel_request',
				            'custrecord_trli_origin_city_display', i),
				    Destination : travelRecord.getLineItemValue(
				            'recmachcustrecord_trli_travel_request',
				            'custrecord_trli_destination_city_display', i)
				});
			}

			return travelDetails;
		} else {
			throw "You are not authorised to access this record";
		}
	} catch (err) {
		nlapiLogExecution('error', 'getTravelDetail', err);
		throw err;
	}
}

function approveTravel(travelId, approverId, travelDetails, remarks) {
	try {
		var travelRecord = nlapiLoadRecord('customrecord_travel_request',
		        travelId);

		if (travelRecord.getFieldValue('custrecord_tr_approving_manager_id') == approverId) {

			// status change
			if (travelRecord.getFieldValue('custrecord_tr_travel_type') == 1) {
				travelRecord.setFieldValue('custrecord_tr_status', '4');
			} else {
				travelRecord.setFieldValue('custrecord_tr_status', '3');
			}

			// checklist
			for (var i = 1; i <= travelRecord
			        .getLineItemCount('recmachcustrecord_trmc_travel_request'); i++) {
				var itemId = travelRecord.getLineItemValue(
				        'recmachcustrecord_trmc_travel_request', 'id', i);

				/*for (var j = 0; j <= travelDetails.BillableItems.length; j++) {

					if (itemId == travelDetails.BillableItems[j].Id) {
						travelRecord.setLineItemValue(
						        'recmachcustrecord_trmc_travel_request',
						        'custrecord_trmc_value', i,
						        travelDetails.BillableItems[j].Status ? 'T'
						                : 'F');
						break;
					}
				}*/
			}
			var travelId = nlapiSubmitRecord(travelRecord);
			nlapiLogExecution('AUDIT', 'Travel Approved', travelId);

			// creating approval log
			var recTRApproverAction = nlapiCreateRecord('customrecord_tr_approval_log');
			recTRApproverAction.setFieldValue('custrecord_tral_id', travelId);
			recTRApproverAction.setFieldValue('custrecord_tral_remarks',
			        remarks);
			recTRApproverAction.setFieldValue(
			        'custrecord_tral_approver_action', 1);
			recTRApproverAction.setFieldValue('custrecord_tral_approved_by',
			        approverId);
			recTRApproverAction.setFieldValue('custrecord_tral_approver_role',
			        1);
			var i_rec_id = nlapiSubmitRecord(recTRApproverAction);
			nlapiLogExecution('AUDIT', 'Travel Approval Log Created', i_rec_id);

			createLog('customrecord_travel_request', travelId, 'Update',
			        approverId, 'Approved');

			// send emails for travel approval
			sendEmail('TR_APPROVED', travelId, travelRecord, i_rec_id, null);
			
			return 'Approved Successfull';
		} else {
			throw "You are not authorised to access this record";
		}
	} catch (err) {
		nlapiLogExecution('error', 'approveTravel', err);
		throw err;
	}
}

function rejectTravel(travelId, approverId, travelDetails, remarks) {
	try {
		var travelRecord = nlapiLoadRecord('customrecord_travel_request',
		        travelId);

		if (travelRecord.getFieldValue('custrecord_tr_approving_manager_id') == approverId) {
			travelRecord.setFieldValue('custrecord_tr_status', 7);
			var travelId = nlapiSubmitRecord(travelRecord);
			nlapiLogExecution('AUDIT', 'Travel Rejected', travelId);

			// creating approval log
			var recTRApproverAction = nlapiCreateRecord('customrecord_tr_approval_log');
			recTRApproverAction.setFieldValue('custrecord_tral_id', travelId);
			recTRApproverAction.setFieldValue('custrecord_tral_remarks',
			        remarks);
			recTRApproverAction.setFieldValue(
			        'custrecord_tral_approver_action', 2);
			recTRApproverAction.setFieldValue('custrecord_tral_approved_by',
			        approverId);
			recTRApproverAction.setFieldValue('custrecord_tral_approver_role',
			        1);
			var i_rec_id = nlapiSubmitRecord(recTRApproverAction);
			nlapiLogExecution('AUDIT', 'Travel Rejection Log Created', i_rec_id);

			createLog('customrecord_travel_request', travelId, 'Update',
			        approverId, 'Rejected');

			sendEmail('TR_REJECTED', travelId, travelRecord, i_rec_id, null);
			return 'Reject Successfull';
		} else {
			throw "You are not authorised to access this record";
		}
	} catch (err) {
		nlapiLogExecution('error', 'rejectTravel', err);
		throw err;
	}
}

function sendEmail(mode, i_tr_id, recTR, i_log_rec_id, a_attachment_list) {
	var i_template = null; // Template to load

	var s_to = null;
	var a_cc = new Array();

	var values = new Object();

	var a_attachment_files = null;

	var i_domestic_international;

	var o_values = nlapiLookupField('customrecord_travel_request', i_tr_id, [
	        'custrecord_tr_employee', 'custrecord_tr_status',
	        'custrecord_tr_delivery_manager',
	        'custrecord_tr_approving_manager_id', 'custrecord_tr_visa_type',
	        'custrecord_tr_travel_type', 'custrecord_tr_gt_bill_rate' ]);

	var o_requestor = nlapiLookupField('employee',
	        o_values.custrecord_tr_employee, [ 'internalid', 'email' ]);

	var sendSeperateEmailToRequestor = false;
	var html_flag = false;
	try {
		switch (mode) {
			case 'TR_CREATED':
				s_to = recTR
				        .getFieldValue('custrecord_tr_approving_manager_id');

				i_domestic_international = recTR
				        .getFieldValue('custrecord_tr_travel_type');

				var o_approver = nlapiLookupField('employee', s_to, [
				        'firstname', 'lastname' ]);

				values['approver_firstname'] = o_approver.firstname;
				values['approver_lastname'] = o_approver.lastname;

				a_cc = a_cc.concat(o_requestor.email);
				a_cc = a_cc.concat(email_configuration.TRAVEL);

				if (i_domestic_international == K_DOMESTIC_INTERNATIONAL.INTERNATIONAL) {
					a_cc = a_cc.concat(email_configuration.IMMIGRATION);
					a_cc = a_cc.concat(email_configuration.HR);
				}

				i_template = K_EMAIL_TEMPLATES.SUBMITTED;
			break;
case 'TR_APPROVED':
				
				i_domestic_international	=	o_values.custrecord_tr_travel_type;
				
				var o_approval_log	=	nlapiLoadRecord('customrecord_tr_approval_log', i_log_rec_id);
				
				var o_approver	=	nlapiLookupField('employee', o_approval_log.getFieldValue('custrecord_tral_approved_by'), ['firstname', 'lastname', 'email']); // The one who approved
				
				var o_approving_manager	=	nlapiLookupField('employee', o_values.custrecord_tr_approving_manager_id, ['email']); // The approving manager
				
				//var o_requestor	=	nlapiLookupField('employee', o_values.custrecord_tr_employee, ['internalid', 'email']);
				
				values['role']		=	o_approval_log.getFieldText('custrecord_tral_approver_role');
				values['action']	=	o_approval_log.getFieldValue('custrecord_tral_approver_action');
				values['remarks']	=	o_approval_log.getFieldValue('custrecord_tral_remarks');
				values['remarks']	=	(values['remarks']	==	null)?'':'<br /><br /><b>' + values['role'] + ' Remarks</b><br />' + values['remarks'] + '<br />';
				
				var i_approver_role	=	o_approval_log.getFieldValue('custrecord_tral_approver_role'); // Role of the approver
				
				if(i_approver_role == K_TRAVEL_APPROVER_ROLE.APPROVER) // Approved by Approver
					{
						if(i_domestic_international == K_DOMESTIC_INTERNATIONAL.DOMESTIC)
							{
								s_to	=	email_configuration.TRAVEL;
								
								a_cc = a_cc.concat(o_requestor.email);
								a_cc = a_cc.concat(o_approver.email);
								a_cc = a_cc.concat(o_approving_manager.email);
							}
						else
							{
								s_to	=	email_configuration.IMMIGRATION;
								
								sendSeperateEmailToRequestor	=	true;
								
								//a_cc = a_cc.concat(o_requestor.email);
								a_cc = a_cc.concat(o_approver.email);
								a_cc = a_cc.concat(o_approving_manager.email);
								a_cc = a_cc.concat(email_configuration.TRAVEL);
								//a_cc = a_cc.concat(email_configuration.HR);
								a_cc = a_cc.concat(email_configuration.FINANCE);
								
								if(o_values.custrecord_tr_visa_type	==	K_VISA_TYPE.WORK)
									{
									
										// values['gt_bill_rate']	=	o_values.custrecord_tr_gt_bill_rate;
									
										a_cc	=	a_cc.concat(email_configuration.OPS);
										a_cc	=	a_cc.concat(email_configuration.HR_US);
										a_cc	=	a_cc.concat(email_configuration.TA);
										a_cc	=	a_cc.concat(email_configuration.INDIA_HR);
										//a_cc	=	a_cc.concat(email_configuration.INDIA_COMP_TEAM);
										a_cc	=	a_cc.concat(email_configuration.RMG_US);
										a_cc	=	a_cc.concat(email_configuration.RMG_INDIA);
									}
							}
					
						values['approver_firstname']	=	o_approver.firstname;
						values['approver_lastname']		=	o_approver.lastname;
						//a_cc.push('immigration@brillio.com');
						i_template	=	24;	
					}
				else if(i_approver_role	==	K_TRAVEL_APPROVER_ROLE.IMMIGRATION) // Approved by Immigration
					{
						if(i_domestic_international	==	K_DOMESTIC_INTERNATIONAL.INTERNATIONAL)
							{
								s_to	=	email_configuration.TRAVEL;
							}
						
						//var o_delivery_manager	=	nlapiLookupField('employee', o_approval_log.getFieldValue('custrecord_tral_approved_by'), ['firstname', 'lastname']);
						values['approver_firstname']	=	o_approver.firstname;
						values['approver_lastname']		=	o_approver.lastname;
						a_cc = a_cc.concat(email_configuration.IMMIGRATION);
						//a_cc.push(nlapiLookupField('employee', o_values.custrecord_tr_approving_manager_id, 'email'));
						//a_cc.push('team.indiahr@brillio.com');
						//a_cc.push('business.ops@brillio.com');
						//a_cc.push('traveldesk@brillio.com');
						i_template	=	K_EMAIL_TEMPLATES.IMMIGRATION_APPROVED;
					}
				else if(i_approver_role	==	K_TRAVEL_APPROVER_ROLE.TRAVEL) // Approved by Travel
					{
						s_to	=	o_requestor.internalid;
						values['approver_firstname']	=	o_approver.firstname;
						values['approver_lastname']		=	o_approver.lastname;
						
						if(i_domestic_international == K_DOMESTIC_INTERNATIONAL.INTERNATIONAL)
							{
								// Send seperate email to requestor, without the gt rate
								sendSeperateEmailToRequestor	=	true;
							
								a_cc = a_cc.concat(email_configuration.FINANCE);
								a_cc = a_cc.concat(email_configuration.IMMIGRATION);
								a_cc = a_cc.concat(email_configuration.FMG);
								
								// If Work Visa, send to HR
								if(o_values.custrecord_tr_visa_type	==	K_VISA_TYPE.WORK)
									{
										a_cc = a_cc.concat(email_configuration.INDIA_HR);
									}
							}
						
						if(a_attachment_list.length > 0)
							{
								a_attachment_files	=	new Array();
							}
						
						for(var i = 0; i < a_attachment_list.length; i++)
							{
								a_attachment_files.push(nlapiLoadFile(a_attachment_list[i]));
							}
						
						i_template	=	K_EMAIL_TEMPLATES.TRAVEL_APPROVED;
					}
           				try{
					var req_details	=	nlapiLookupField('customrecord_travel_request', i_tr_id, ['custrecord_tr_forex_or_advance']);
				var currency = req_details.custrecord_tr_forex_or_advance;
				nlapiLogExecution('AUDIT', 'currency value', currency);
				if(parseInt(currency) > 0){
					a_cc = a_cc.concat('santosh.banadawar@brillio.com');
				}
				}catch (e) {
					// TODO: handle exception
				}
				
				break;
			case 'TR_REJECTED':
								
				var o_approval_log	=	nlapiLoadRecord('customrecord_tr_approval_log', i_log_rec_id);
				
				s_to	=	o_requestor.internalid;
				a_cc = a_cc.concat(email_configuration.TRAVEL);
				var o_approver	=	nlapiLookupField('employee', o_approval_log.getFieldValue('custrecord_tral_approved_by'), ['firstname', 'lastname']);
				values['delivery_manager_firstname']	=	o_approver.firstname;
				values['delivery_manager_lastname']		=	o_approver.lastname;
				values['role']		=	o_approval_log.getFieldText('custrecord_tral_approver_role').toUpperCase();
				values['action']	=	o_approval_log.getFieldValue('custrecord_tral_approver_action');
				values['remarks']	=	(values['remarks']	==	null)?'':'<br /><br /><b>' + values['role'] + ' Remarks</b><br />' + values['remarks'] + '<br />';
				
				i_template	=	K_EMAIL_TEMPLATES.REJECTED;
				
				break;
		}
		
		
		var emailMerger = nlapiCreateEmailMerger(i_template); // Initiate Email Merger

		emailMerger.setCustomRecord('customrecord_travel_request', i_tr_id); // Set the ID of the transaction where you are going to fetch the values to populate the variables on the template

		var mergeResult = emailMerger.merge(); // Merge the template with the email

		var emailSubject = mergeResult.getSubject(); // Get the subject for the email

		var emailBody = mergeResult.getBody(); // Get the body for the email

		emailSubject	=	replaceValues(emailSubject, values);
		
		var emailBodyRequestor	=	'';
		
		if(sendSeperateEmailToRequestor == true)
			{
				values['gt_bill_rate']	=	'';
				emailBodyRequestor	=	replaceValues(emailBody, values);
			}
		
		values['gt_bill_rate']	=	o_values.custrecord_tr_gt_bill_rate;
		
		emailBody	=	replaceValues(emailBody, values);
		
		//s_to	=	9122;
		//a_cc	=	['amol.sahijwani@brillio.com'];
          //a_cc	=	a_cc.concat('waseem.pasha@brillio.com');
     
		try{
			var orh = recTR.getLineItemValue('recmachcustrecord_trli_travel_request', 'custrecord_trli_origin_city', 1);
			if(orh  == 429){
				 a_cc	=	a_cc.concat('gouri.v@brillio.com');
				 a_cc	=	a_cc.concat('lincy.n@brillio.com');
			}

		}catch (e) {
			// TODO: handle exception
			

		}
		nlapiSendEmail(442, s_to, emailSubject, emailBody, a_cc, null, null, a_attachment_files); // Send the email with attachment
        nlapiLogExecution('DEBUG','Mail sent to ->',s_to);
		
		if(sendSeperateEmailToRequestor == true)
			{
				nlapiSendEmail(442, o_requestor.email, emailSubject, emailBodyRequestor, null, null, null, a_attachment_files);
              nlapiLogExecution('DEBUG','Mail sent to ->',o_requestor.email);
			}
		
			
	} catch (ex) {
		nlapiLogExecution('ERROR', 'Error Send Email: ', ex);
		throw ex;
	}
}

function sendEmail_HTML(s_to,a_cc,values,recTR){
	try{
		var emp_id = recTR.getFieldValue('custrecord_tr_employee');
		var travel_name = recTR.getFieldValue('name');
		if(recTR){
			var emp_name = nlapiLookupField('employee',parseInt(emp_id),'firstname');
		}
		//Email Body
		var strVar = '';
		strVar += '<html>';
		strVar += '<body>';
		
		strVar += '<p>Dear '+ emp_name +',</p>';
		//strVar += '<br/>';
		strVar += '<p>Your Travel Request '+travel_name+' has been approved by '+values.approver_firstname+' '+values.approver_lastname+'';
		strVar += 'Please initiate the following actions</p>';
		//strVar += ' You are hereby requested to contact your reporting manager to learn about possible extension of your current allocation / assignment and other opportunities.</p>';
		
		strVar += '<p>1. TA, RMG, Manager - Please ensure that the information in the RRF is complete';
		strVar += '2. Brillio Ops - please ensure project allocation and client onboarding completion';
		strVar += '3. Comp team - Please initiate comp fitment';
		strVar += '4. India HR - Please initiate the relocation, separation and exit formalities from India. India HR to also initiate the offer letter and HR';
		strVar += '   onboarding in the Taleo';
		strVar += '5. Finance team - Please plan for Salary advance, forex etc';
		strVar += '6. Travel and Immigration - Please ensure timely document checks and travel arrangements are done';
		strVar += '7. HR US - Please ensure iBridge touchbase and US onboarding processes</p>';
		
		strVar += '<p>Thanks & Regards,';
		strVar += '   Information Systems</p>';
			
		strVar += '</body>';
		strVar += '</html>';
		
		//End of Email Body
		var emailSubject = 'Approved: Travel Request '+ travel_name +' raised by '+emp_name;
		var a_emp_attachment = new Array();
		a_emp_attachment['entity'] = parseInt(emp_id);
		//nlapiSendEmail(442, bcc, 'Notification: Allocation Ending In 30-Days dt '+today, strVar,bcc,bcc);
		//Approved: Travel Request '+ travel_name +' raised by '+emp_name
		nlapiSendEmail(442,30237, emailSubject, strVar,'deepak.srinivas@brillio.com','deepak.srinivas@brillio.com',a_emp_attachment);
		nlapiLogExecution('debug', 'Email Sent Resource:-- ',emp_name);
	}
	catch(err){
		nlapiLogExecution('DEBUG','International Approved Email Error',err);
		throw err;
	}
	
}
//Used to display the html, by replacing the placeholders
function replaceValues(content, oValues)
{
	for(param in oValues)
		{
			// Replace null values with blank
			var s_value	=	(oValues[param] == null)?'':oValues[param];
			
			// Replace content
			content = content.replace(new RegExp('{{' + param + '}}','gi'), s_value);
		}
	
	return content;
}

function _logValidation(value) 
{
 if(value != null && value.toString() != null && value != '' && value != undefined && value.toString() != undefined && value != 'undefined' && value.toString() != 'undefined'&& value.toString() != 'NaN' && value != NaN) 
 {
  return true;
 }
 else 
 { 
  return false;
 }
}