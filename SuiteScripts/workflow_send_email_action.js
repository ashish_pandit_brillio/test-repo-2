	/**
	 *@NApiVersion 2.x
	 *@NScriptType WorkflowActionScript
	 */
	define(["N/runtime","N/email","N/render","N/xml", "N/record","N/file",'N/search',"SuiteScripts/to_table"], function (runtime,email,render,xml,record,file,search,table) {

		function onAction(scriptContext) {
			try {
				const tpl_id = 29;
				const replyTo = "emails.3883006_SB1.2300.f11589132d@3883006-sb1.email.netsuite.com";
		
				log.debug({
					title: 'First Level Email Action Triggered'
				});
		
				var author = runtime.getCurrentUser();
		
				var newRecord = scriptContext.newRecord;
				var recordId = newRecord.id;
              var transactionId = recordId;
				var emailRecipients = newRecord.getValue({
					fieldId:"custbody_verticalhead"
				});

				var vertical_head = newRecord.getText({
					fieldId:"custbody_verticalhead"
				});

				var tranid = newRecord.getText({
					fieldId:"transactionnumber"
				});
				
				var trandate = newRecord.getText({
					fieldId:"trandate"
				});

				
		
				var entityId = newRecord.getValue({
					fieldId:"entity"
				});
				log.debug('entityId',entityId);
				var vendorId = record.load({
						type: 'vendor',
						id: entityId,
						isDynamic: true,
					});
				var vendorName = vendorId.getText({
					fieldId:"altname"
				});
				log.debug('vendorName',vendorName);
				
				
				var memo = newRecord.getText({
					fieldId:"memo"
				});
				
				var total = newRecord.getText({
					fieldId:"total"
				});
				

				var lines = table.getLines(newRecord,"item",["item","description","rate","quantity","amount"]);
				var expenses = table.getLines(newRecord,"expense",["account_display","taxrate","taxamt","amount"])
				log.debug("lines",lines);
				log.debug("expenses",expenses);

				var scope = {
					vendor:entityId,

					tranid:tranid,
					
					
					trandate:newRecord.getValue({
						fieldId:"trandate"
					}),
					
					memo:newRecord.getValue({
						fieldId:"memo"
					}),
					
					
					total:newRecord.getText({
						fieldId:"usertotal"
					}),
					
					items:table.render(lines ),

					expenses:table.render(expenses)
				};

				
				log.debug("scope",scope);
				
		var logourl =xml.escape("http://3883006-sb1.shop.netsuite.com/core/media/media.nl?id=6237&c=3883006_SB1&h=1bf2745c93c6c3315e42");


				/*var mergeResult = render.mergeEmail({
					templateId: tpl_id,
					entity:null,// entity_obj,
					recipient:null,//{ type:"employee",id: emailRecipients},
					supportCaseId: null,
					transactionId: transactionId,
					customRecord: null
				});
				*/
				var tpl_file = file.load({
					id:"SuiteScripts/vendor_bill.txt"
				})

				var tpl = ""

				var verticalHeader = newRecord.getText({
					fieldId:"custbody_verticalhead"
				});
				log.debug('verticalHeader',verticalHeader);
				var verticalHeaderString = "";
				if(verticalHeader[0].indexOf("-") != -1) 
					verticalHeaderString= verticalHeader[0].split("-")[1]; 
				else 
					verticalHeaderString=verticalHeader;
				log.debug(verticalHeaderString);
				
				// loading the file from the communications tab
				var vendorbillSearchObj = search.create({
					   type: "vendorbill",
					   filters:
					   [
						  ["type","anyof","VendBill"], 
						  "AND", 
						  ["mainline","is","T"], 
						  "AND", 
						  ["internalid","anyof",recordId]
					   ],
					   columns:
					   [
						  search.createColumn({
							 name: "internalid",
							 join: "file",
							 label: "Internal ID"
						  }),
						  search.createColumn({
							 name: "internalid",
							 sort: search.Sort.ASC,
							 label: "Internal ID"
						  })
					   ]
					});
					var filesearch=executeSearch(vendorbillSearchObj);
					var fileIds=[];
					if(filesearch && filesearch.length>0)
					{
						for(var i=0;i<filesearch.length;i++)
						{
							var fileId = filesearch[i].getValue({name: "internalid",join: "file"});
						fileIds.push(fileId);
						}
					}
					if(fileIds.length>0)
					{
						for(var j=0;j<fileIds.length;j++)
						{
						//var fileObj=file.load(fileIds[j]);
						}
					}
              var billNUmber = newRecord.getText({
					fieldId:"tranid"
				});
				//end the logic				
				tpl += "\nHello "+verticalHeaderString+"\n\n<br>"
				tpl+= "The Vendor Bill Number <b>"+billNUmber+"</b> for Vendor - <b>"+vendorName+"</b> of amount - <b>{usertotal}</b> is pending for approval."
				
				tpl += tpl_file.getContents();

			
				tpl = table.merge(tpl,scope);

				// tpl = tpl.replace(/{custbody_verticalhead}/g,vertical_head);
				// tpl = tpl.replace(/{tranId}/g,tranid);
				// tpl = tpl.replace(/{entityId}/g,entity)
				// tpl = tpl.replace(/{total}/g,total);
				
				tpl = tpl.replace(/{tranid}/g,tranid);
				tpl = tpl.replace(/{memo}/g,memo);
				tpl = tpl.replace(/{usertotal}/g,total);

				// tpl += "<br/><br/> Please reply with <b>Approved</b> or <b>Rejected</b> keywords"

				log.debug("tpl",tpl);
			 
				var emailSubject = "{tranid}:{id}:1:Vendor Bill For Approval"
				
				emailSubject = emailSubject.replace(/{tranid}/g,tranid);

				emailSubject = emailSubject.replace(/{id}/g,transactionId);

				var emailBody =  tpl;
				log.debug('emailBody',emailBody);				


				emailBody+='<table width="100%">'
                 emailBody+='<tr> ';

                 emailBody+='<td width="50%"> ';  
                 emailBody+='<table ><tr> ';
				 emailBody += "<td'><img src='"+logourl+"'style='float: left;width:150px;height:65px;'/></td>"
                emailBody+=' </tr>';    
                 emailBody+='</table>';  
                 emailBody+='</td>'; 

                 emailBody+=' <td width="50%">';     
                 emailBody+='<table >';   
                 emailBody+='<tr>';  
                 emailBody+='<td>Brillio Inc<br/>Brillio Technologies Private Limited<br/> 5th Floor, Primal Eco Space Campus , Priteck Park<br/>5th Floor, 4A Block<br/>Bellandur, Bangalore Karnataka 560037<br/>India';
                emailBody+=' </td>';       
                 emailBody+='  </tr>';     
                 emailBody+='  </table>';    
                 emailBody+='  </td> ';

                 emailBody+='  </tr> ';
                  emailBody+=' </table>';

                 emailBody+='  <p>';
                 emailBody+='  <b>Note: </b>Please reply with <b>Approved</b> or <b>Rejected</b> keyword.';
                 emailBody+='  </p>';
		
				email.send({
					author: emailRecipients,
					recipients: emailRecipients,
					subject: emailSubject,
					body: emailBody,
					//attachments: [fileObj],
					relatedRecords: {
						transactionId: transactionId,
						entityId: 41571
					},
					replyTo:replyTo
				});
				log.debug('emailRecipients',emailRecipients);

			}catch(e)
			{
				log.debug("error",e);
			}
		   
		}
		function executeSearch(srch) {
					var results = [];

					//var pagedData = srch.runPaged();
					var pagedData = srch.runPaged({
						pageSize: 1000
					});
					pagedData.pageRanges.forEach(function(pageRange) {
						var page = pagedData.fetch({
							index: pageRange.index
						});
						page.data.forEach(function(result) {
							results.push(result);
						});
					});

					return results;
				};
		return {
			onAction: onAction
		}
	});