/**
 * Update the Timesheet Pool Record when the TS is uploaded from CSV
 * 
 * Version    Date            Author           Remarks
 * 1.00       03 Sep 2015     nitish.mishra
 *
 */

function workflowAction() {
	var quickPoolSearch = nlapiSearchRecord(
	        'customrecord_ts_quick_approval_pool', null, new nlobjSearchFilter(
	                'custrecord_qap_timesheet', null, 'anyof',
	                nlapiGetRecordId()));

	if (quickPoolSearch) {
		quickApprovalRecord = nlapiSubmitField(
		        'customrecord_ts_quick_approval_pool', quickPoolSearch[0]
		                .getId(), 'custrecord_qap_ts_updated', 'T');

		nlapiLogExecution('debug', 'Quick Approval Pool TS Updated',
		        quickPoolSearch[0].getId());
	}
}
