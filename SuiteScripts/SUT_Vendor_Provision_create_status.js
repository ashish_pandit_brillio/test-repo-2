// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
/*
   	Script Name : SUT_Vendor_Provision_create_status.js
	Author      : Jayesh Dinde
	Date        : 7 April 2016
	Description : Create vendor provision record and display status for the same


	Script Modification Log:

	-- Date --			-- Modified By --				--Requested By--				-- Description --													  
                                                                                         																						  

Below is a summary of the process controls enforced by this script file.  The control logic is described
more fully, below, in the appropriate function headers and code blocks.


     SUITELET
		- suiteletFunction(request, response)


     SUB-FUNCTIONS
		- The following sub-functions are called by the above core functions in order to maintain code
            modularization:

               - NOT USED

*/
}
// END SCRIPT DESCRIPTION BLOCK  ====================================



// BEGIN GLOBAL VARIABLE BLOCK  =====================================
{
	//  Initialize any Global Variables, in particular, debugging variables...




}
// END GLOBAL VARIABLE BLOCK  =======================================





// BEGIN SUITELET ==================================================

function suiteletFunction_Vendor_Pro(request, response)
{
	try
	{	
		if (request.getMethod() == 'GET') 
	  	{
			var vendor_provision_month =  request.getParameter('custscript_vendor_provision_month');
			var vendor_provision_year =   request.getParameter('custscript_vendor_provision_year');
			var vendor_provision_debit_acnt =  request.getParameter('custscript_vendor_provision_debit_acnt');
			var vendor_provision_credit_acnt =  request.getParameter('custscript_vendor_provision_credit_acnt');
			var vendor_provision_criteria =  request.getParameter('custscript_vendor_provision_criteria');
			var vendor_provision_subsidiary =   request.getParameter('custscript_vendor_provision_subsidiary');
			var vendor_subsidiary_sub_id = request.getParameter('custscript_vendor_subsidiary_sub_id');
			var rate_type = request.getParameter('custscript_vendor_rate_type');
			
			var d_start_date = get_current_month_start_date(vendor_provision_month, vendor_provision_year);
			var d_end_date = get_current_month_end_date(vendor_provision_month, vendor_provision_year);
			
			var f_form = nlapiCreateForm("Vendor Provisioning Emp List");
			
			var result = get_emp_list_for_salary_upload(d_start_date,d_end_date,vendor_provision_subsidiary,vendor_subsidiary_sub_id,vendor_provision_month,vendor_provision_year,f_form,rate_type);
			
			var subsidiary_label = "<table>";
			subsidiary_label += "<tr><td Width=\"100%\" height=\"25\" style='text-align:left;font-weight:bold;' font-size=\"13\" \>Subsidiary: " + vendor_provision_subsidiary + "</td>";
			subsidiary_label += "</tr></table>";
			
			var result_text = f_form.addField('custpage_emp_list', 'inlinehtml', 'Subsidiary');
			result_text.setDefaultValue(subsidiary_label);
			
			if (_logValidation(result)) {
				var result_no_rslt_found = f_form.addField('custpage_emp_rcrd_nt_found', 'inlinehtml', 'No Record Found');
				result_no_rslt_found.setDefaultValue(result);
			}
			
			var month = f_form.addField('custpage_month', 'text', 'Month').setDisplayType('hidden');
			month.setDefaultValue(vendor_provision_month);
			
			var year = f_form.addField('custpage_year', 'text', 'Year').setDisplayType('hidden');
			year.setDefaultValue(vendor_provision_year);
			
			var start_date = f_form.addField('custpage_strt_date', 'text', 'Start Date').setDisplayType('hidden');
			start_date.setDefaultValue(d_start_date);
			
			var end_date = f_form.addField('custpage_end_date', 'text', 'End Date').setDisplayType('hidden');
			end_date.setDefaultValue(d_end_date);
			
			var rate_type_to_down = f_form.addField('custpage_rate_type', 'text', 'Rate Type').setDisplayType('hidden');
			rate_type_to_down.setDefaultValue(rate_type);
			
			var vendor_subsidairy = f_form.addField('custpage_ven_sub', 'text', 'Ven Sub').setDisplayType('hidden');
			vendor_subsidairy.setDefaultValue(vendor_subsidiary_sub_id);
			
			f_form.addSubmitButton('Export Excel');
			response.writePage(f_form);
		}
		else
		{
			
			var d_start_date =  request.getParameter('custpage_strt_date');
			var d_end_date =  request.getParameter('custpage_end_date');
			var vendor_subsidiary_sub_id =  request.getParameter('custpage_ven_sub');
			var vendor_provision_month =  request.getParameter('custpage_month');
			var vendor_provision_year =  request.getParameter('custpage_year');
			var rate_type = request.getParameter('custpage_rate_type');
			
			var filters = new Array();
			filters[0] = new nlobjSearchFilter('custrecord_month_sal_up_ven',null,'is',vendor_provision_month.trim());
			filters[1] = new nlobjSearchFilter('custrecord_year_sal_up_ven',null,'is',vendor_provision_year.trim());
			filters[2] = new nlobjSearchFilter('custrecord_is_processed', null, 'is', 'F');
			
			if (rate_type == 2)
			{
				filters[3] = new nlobjSearchFilter('custrecord_rate_type_sal_upload', null, 'is', 2);
			}
			else
			{
				filters[3] = new nlobjSearchFilter('custrecord_rate_type_sal_upload', null, 'is', 3);
			}
			filters[4] = new nlobjSearchFilter('custrecord_subsidiary_sal_up_ven', null,'is',parseInt(vendor_subsidiary_sub_id));
			
			var column = new Array();
			column[0]  = new nlobjSearchColumn('custrecord_employee_rec_id_sal_up_ven');
			column[1]  = new nlobjSearchColumn('custrecord_location_sla_up_ven');
			column[2]  = new nlobjSearchColumn('custrecord_cost_sal_up_ven');
			column[3]  = new nlobjSearchColumn('custrecord_project_assigned');
			column[4]  = new nlobjSearchColumn('custrecord_proj_hours_allocated');
			column[5]  = new nlobjSearchColumn('custrecord_percent_of_time_allocated');
			column[6]  = new nlobjSearchColumn('custrecord_no_of_leaves_taken');
			column[7]  = new nlobjSearchColumn('custrecord_no_of_sub_nt_approved_days');
			column[8]  = new nlobjSearchColumn('custrecord_no_of_approved_days');
			column[9]  = new nlobjSearchColumn('custrecord_no_of_nt_submitted_days');
			column[10]  = new nlobjSearchColumn('custrecord_allocated_time');
			column[11]  = new nlobjSearchColumn('custrecord_currency_ven_pro');
			column[12]  = new nlobjSearchColumn('custrecord_no_of_holidays');
					
			var down_excel = nlapiSearchRecord('customrecord_salary_upload_file', null, filters, column);
			if (_logValidation(down_excel)) //
			{
				down_excel_function(down_excel, vendor_provision_month, vendor_provision_year,vendor_subsidiary_sub_id);
			}
			else
			{
				var f_form = nlapiCreateForm("Vendor Provisioning Data");
				
				var result_no_rslt_found = f_form.addField('custpage_emp_rcrd_nt_found', 'inlinehtml', 'No Record Found');
				result_no_rslt_found.setDefaultValue('No Record Found');
				
				response.writePage(f_form);
			}
		}
		
		
	
	
	    /*var params=new Array();
	    params['custscript_current_date'] = i_current_date
		
		params['custscript_from_date'] = i_from_date
		params['custscript_to_date'] = i_to_date
		
		params['custscript_unbilled_type'] = i_unbilled_type
		params['custscript_unbilled_receivable_gl'] = i_unbilled_receivable_GL
		params['custscript_unbilled_revenue_gl'] = i_unbilled_revenue_GL
		params['custscript_reverse_date'] = i_reverse_date
		params['custscript_provision_creation_criteria'] = i_criteria 	
		params['custscript_user_role'] = i_role
		params['custscript_user_subsidiary'] = i_subsidiary 
        params['custscript_customer'] = i_customer 	
        params['custscript_count'] = i_search_count_f 
		params['custscript_month'] = i_month 	
		params['custscript_year'] = i_year 	 
		 
	//	response.write('https://system.sandbox.netsuite.com/app/common/search/searchresults.nl?searchid=138') 
	    if(i_unbilled_type == '2')
		{
		
		
				var params_1=new Array();
			// params['custscript_current_date_sch'] = i_current_date
			//----------------------------------------------------------------
			  params_1['custscript_sch_from_date'] = i_from_date
			  params_1['custscript_sch_to_date'] = i_to_date
			 //----------------------------------------------------------------
			
			 params_1['custscript_sch_subsidiary'] = i_subsidiary
			 params_1['custscript_criteria'] = i_criteria;
			 params_1['custscript_month_id'] = i_month;
			 params_1['custscript_year_id'] = i_year;
				
		var status=nlapiScheduleScript('customscript_sch_timebill_rec_create',null,params_1);	 
		nlapiSetRedirectURL('suitelet', 'customscript_sui_create_timebill_rec', 'customdeploy1', null,params);
		//nlapiLogExecution('DEBUG', 'suiteletFunction', ' Suitelet Called for criteria 3......... ' );
		}
		else
		{
		nlapiSetRedirectURL('suitelet', 'customscript_provision_unbilled_journal', 'customdeploy1', null,params);
		nlapiLogExecution('DEBUG', 'suiteletFunction', ' Suitelet Called ......... ' );
		}*/				
		
	}//TRY
	catch(exception)
	{
		nlapiLogExecution('DEBUG', 'ERROR',' Exception Caught -->' + exception);	
					
	}//CATCH
}

// END SUITELET ====================================================


// BEGIN OBJECT CALLED/INVOKING FUNCTION ===================================================
function get_todays_date()
{
	var today;
	// ============================= Todays Date ==========================================
		
	  var date1 = new Date();
      //nlapiLogExecution('DEBUG', 'schedulerFunction', 'Date ==' + date1);

      var offsetIST = 5.5;

    //To convert to UTC datetime by subtracting the current Timezone offset
     var utcdate =  new Date(date1.getTime() + (date1.getTimezoneOffset()*60000));
     //nlapiLogExecution('DEBUG', 'schedulerFunction', 'UTC Date' + utcdate);

    //Then cinver the UTS date to the required time zone offset like back to 5.5 for IST
     var istdate =  new Date(utcdate.getTime() - ((-offsetIST*60)*60000));
    // nlapiLogExecution('DEBUG', 'schedulerFunction', 'IST Date ==' + istdate);
    
     var day = istdate.getDate();
    // nlapiLogExecution('DEBUG', 'schedulerFunction', 'Day ==' + day);
	 
     var month = istdate.getMonth()+1;
     //nlapiLogExecution('DEBUG', 'schedulerFunction', 'Month ==' + month);

	 var year=istdate.getFullYear();
	// nlapiLogExecution('DEBUG', 'schedulerFunction', 'Year ==' +year);
	  
	 var date_format = checkDateFormat();
	// nlapiLogExecution('DEBUG', 'schedulerFunction', 'Date Format ==' +date_format);
	 
	 if (date_format == 'YYYY-MM-DD')
	 {
	 	today = year + '-' + month + '-' + day;		
	 } 
	 if (date_format == 'DD/MM/YYYY') 
	 {
	  	today = day + '/' + month + '/' + year;
	 }
	 if (date_format == 'MM/DD/YYYY')
	 {
	  	today = month + '/' + day + '/' + year;
	 }	 
	 return today;
}
function checkDateFormat()
{
    var context = nlapiGetContext();

	var dateFormatPref = context.getPreference('dateformat');

    return dateFormatPref;
}

function _logValidation(value) 
{
 if(value != null && value.toString() != null && value != '' && value != undefined && value.toString() != undefined && value != 'undefined' && value.toString() != 'undefined'&& value.toString() != 'NaN' && value != NaN) 
 {
  return true;
 }
 else 
 { 
  return false;
 }
}

function daysInMonth(i_month,year) {
	i_month = i_month.trim();
	
	if(i_month == 'Jan' || i_month == 'January' || i_month == 'JAN' || i_month == 'JANUARY')
	{	  	
		i_month = 1;		
	}	
	else if(i_month == 'Feb' || i_month == 'February' || i_month == 'FEB' || i_month == 'FEBRUARY')
	{
		i_month = 2;	
	}		
	else if(i_month == 'Mar' || i_month == 'March' || i_month == 'MAR' || i_month == 'MARCH')
	{	  
		i_month = 3;
	}	
	else if(i_month == 'Apr' || i_month == 'April' || i_month == 'APR' || i_month == 'APRIL')
	{
		i_month = 4;
	}	
	else if(i_month == 'May' || i_month == 'MAY')
	{	  
		i_month = 5;
	}	  
	else if(i_month == 'June' || i_month == 'Jun' || i_month == 'JUN' || i_month == 'JUNE')
	{	 
		i_month = 6;
	}	
	else if(i_month == 'Jul' || i_month == 'July' || i_month == 'JUL' || i_month == 'JULY')
	{	  
		i_month = 7;
	}	
	else if(i_month == 'August' || i_month == 'Aug' || i_month == 'AUG' || i_month == 'AUGUST')
	{	  
		i_month = 8;
	}  
	else if(i_month == 'Sep' || i_month == 'September' || i_month == 'SEP' || i_month == 'SEPTEMBER')
	{  	
		i_month = 9;
	}	
	else if(i_month == 'Oct' || i_month == 'October' || i_month == 'OCT' || i_month == 'OCTOBER')
	{	 	
		i_month = 10;
	}	
	else if(i_month == 'Nov' || i_month == 'November' || i_month == 'NOV' || i_month == 'NOVEMBER')
	{	  
		i_month = 11;
	}	
	else if(i_month == 'Dec' || i_month == 'December' || i_month == 'DEC' || i_month == 'DECEMBER')
	{	 
		i_month = 12;
	}
	
	year = Number(year).toFixed(0);
    return new Date(year, i_month, 0).getDate();
}

function get_emp_list_for_salary_upload(d_start_date,d_end_date,vendor_provision_subsidiary,vendor_subsidiary_sub_id,vendor_provision_month,vendor_provision_year,f_form,rate_type)
{
	var a_allocate_array = new Array();
	var flag_1 = 0;
	var flag_2 = 0;
	var flag_3 = 0;
	
	if (_logValidation(d_start_date) && _logValidation(d_end_date))
	{		
		d_start_date = nlapiStringToDate(d_start_date);
		d_end_date = nlapiStringToDate(d_end_date);
			
		var filters = new Array();
		filters[0] = new nlobjSearchFilter('custrecord_month_sal_up_ven',null,'is',vendor_provision_month.trim());
		filters[1] = new nlobjSearchFilter('custrecord_year_sal_up_ven',null,'is',vendor_provision_year.trim());
		filters[2] = new nlobjSearchFilter('custrecord_is_processed', null, 'is', 'F');
		if (rate_type == 2)
		{
			filters[3] = new nlobjSearchFilter('custrecord_rate_type_sal_upload', null, 'is', 2);
		}
		else
		{
			filters[3] = new nlobjSearchFilter('custrecord_rate_type_sal_upload', null, 'is', 3);
		}
		filters[4] = new nlobjSearchFilter('custrecord_subsidiary_sal_up_ven', null,'is',parseInt(vendor_subsidiary_sub_id));
		
		var column = new Array();
		column[0]  = new nlobjSearchColumn('custrecord_employee_rec_id_sal_up_ven');
		column[1]  = new nlobjSearchColumn('custrecord_location_sla_up_ven');
		column[2]  = new nlobjSearchColumn('custrecord_cost_sal_up_ven');
		column[3]  = new nlobjSearchColumn('custrecord_project_assigned');
		column[4]  = new nlobjSearchColumn('custrecord_proj_hours_allocated');
		column[5]  = new nlobjSearchColumn('custrecord_percent_of_time_allocated');
		column[6]  = new nlobjSearchColumn('custrecord_no_of_leaves_taken');
		column[7]  = new nlobjSearchColumn('custrecord_no_of_sub_nt_approved_days');
		column[8]  = new nlobjSearchColumn('custrecord_no_of_approved_days');
		column[9]  = new nlobjSearchColumn('custrecord_no_of_nt_submitted_days');
		column[10]  = new nlobjSearchColumn('custrecord_allocated_time');
		column[11]  = new nlobjSearchColumn('custrecord_currency_ven_pro');
		column[12]  = new nlobjSearchColumn('custrecord_no_of_holidays');
				
		var a_results = nlapiSearchRecord('customrecord_salary_upload_file', null, filters, column);
		if (_logValidation(a_results)) //
		{
			var vendor_pro_tab = f_form.addSubList('custpage_vendor_pro_list', 'list', 'Vendor Provision Entries', 'custpage_vendor_pro_tab');
			var i_S_No= vendor_pro_tab.addField('custpage_s_no', 'text', 'Sr. No');				
			var i_employee = vendor_pro_tab.addField('custpage_employee_name', 'select', 'Contractor','employee').setDisplayType('inline');	
			var i_location_f = vendor_pro_tab.addField('custpage_loc', 'text', 'Location').setDisplayType('inline');
			var i_currency = vendor_pro_tab.addField('custpage_emp_currency', 'text', 'Currency').setDisplayType('inline');
			var i_mnthly_rate = vendor_pro_tab.addField('custpage_mnthly_rate', 'text', 'Monthly Cost').setDisplayType('inline');
			var i_project = vendor_pro_tab.addField('custpage_emp_project', 'text', 'Project').setDisplayType('inline');
			var allocated_days = vendor_pro_tab.addField('custpage_proj_allocated_days', 'text', 'Allocated(Hours)').setDisplayType('inline');
			var i_proj_prcnt_hours = vendor_pro_tab.addField('custpage_proj_prcnt_hours', 'text', 'Project Percent of Hours').setDisplayType('inline');
			var i_proj_leave_days = vendor_pro_tab.addField('custpage_proj_leave_days', 'text', 'Leaves Taken(days)').setDisplayType('inline');
			var i_proj_not_apprvd_days = vendor_pro_tab.addField('custpage_proj_not_apprvd_days', 'text', 'Submitted / Not Approved(days)').setDisplayType('inline');
			var i_proj_apprvd_days = vendor_pro_tab.addField('custpage_proj_apprvd_days', 'text', 'Approved(days)').setDisplayType('inline');
			var not_submitted_days = vendor_pro_tab.addField('custpage_proj_nt_submitted_days', 'text', 'Not Submitted(days)').setDisplayType('inline');
			var no_of_holidays = vendor_pro_tab.addField('custpage_proj_holidays', 'text', 'Holidays(days)').setDisplayType('inline');
			
			for (var i = 0; i < a_results.length; i++) //
			{
			
				var Emp_id = a_results[i].getValue('custrecord_employee_rec_id_sal_up_ven');
				var Emp_location = a_results[i].getValue('custrecord_location_sla_up_ven');
				var Emp_currency = a_results[i].getValue('custrecord_currency_ven_pro');
				var Emp_monthly_rate = a_results[i].getValue('custrecord_cost_sal_up_ven');
				var Emp_Project = a_results[i].getValue('custrecord_project_assigned');
				var Emp_proj_hours = a_results[i].getValue('custrecord_proj_hours_allocated');
				var Emp_proj_percent_hours = a_results[i].getValue('custrecord_percent_of_time_allocated');
				var leave_duration_days = a_results[i].getValue('custrecord_no_of_leaves_taken');
				var not_approved_days = a_results[i].getValue('custrecord_no_of_sub_nt_approved_days');
				var approved_days = a_results[i].getValue('custrecord_no_of_approved_days');
				var not_submitted_days = a_results[i].getValue('custrecord_no_of_nt_submitted_days');
				var allocated_days = a_results[i].getValue('custrecord_allocated_time');
				var holidays = a_results[i].getValue('custrecord_no_of_holidays');
				
				vendor_pro_tab.setLineItemValue('custpage_s_no',i+1, Number(i+1).toFixed(0));		     
                vendor_pro_tab.setLineItemValue('custpage_employee_name',i+1,Emp_id);					
                vendor_pro_tab.setLineItemValue('custpage_loc',i+1,Emp_location);
				vendor_pro_tab.setLineItemValue('custpage_emp_currency',i+1,Emp_currency);
				vendor_pro_tab.setLineItemValue('custpage_mnthly_rate',i+1,Emp_monthly_rate);
			    vendor_pro_tab.setLineItemValue('custpage_emp_project',i+1, Emp_Project);
				//vendor_pro_tab.setLineItemValue('custpage_proj_hours',i+1,Emp_proj_hours);
				vendor_pro_tab.setLineItemValue('custpage_proj_prcnt_hours',i+1,Emp_proj_percent_hours);
				vendor_pro_tab.setLineItemValue('custpage_proj_leave_days',i+1,leave_duration_days);
				vendor_pro_tab.setLineItemValue('custpage_proj_holidays',i+1,holidays);
				vendor_pro_tab.setLineItemValue('custpage_proj_not_apprvd_days',i+1,not_approved_days);
				vendor_pro_tab.setLineItemValue('custpage_proj_apprvd_days',i+1,approved_days);
				vendor_pro_tab.setLineItemValue('custpage_proj_nt_submitted_days',i+1,not_submitted_days);
				vendor_pro_tab.setLineItemValue('custpage_proj_allocated_days',i+1,allocated_days);
				
			}
			
		}
		else
		{
			var result_text = "<table>";
			result_text += "<tr><td>";
			result_text += "NO RESULTS FOUND";
			result_text += "</td></tr>";
			result_text += "</table>";
			return result_text;
		}
		
	}
}


function get_current_month_start_date(i_month,i_year)
{
	i_month = i_month.toString().trim();
	i_year = i_year.toString().trim();
   var d_start_date = '';
   var d_end_date = '';
	
   var date_format = checkDateFormat();	 
	
 if (_logValidation(i_month) && _logValidation(i_year)) 
 {
 	  if(i_month == 'Jan' || i_month == 'January' || i_month == 'JAN' || i_month == 'JANUARY')
	  {	  	
		 if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 1 + '-' + 1;		
			d_end_date = i_year + '-' + 1 + '-' + 31;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 1 + '/' + i_year;
			d_end_date = 31 + '/' + 1 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 1 + '/' + 1 + '/' + i_year;
			d_end_date = 1 + '/' + 31 + '/' + i_year;
		 }	 
				
	  }	
	  else if(i_month == 'Feb' || i_month == 'February' || i_month == 'FEB' || i_month == 'FEBRUARY')
	  {
	  	if((i_year%4) == 0)
		{
			i_day = 29;
		}
		else
		{
			i_day = 28;
		}
			
		 if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 2 + '-' + 1;		
			d_end_date = i_year + '-' + 2 + '-' + i_day;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 2 + '/' + i_year;
			d_end_date = i_day + '/' + 2 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 2 + '/' + 1 + '/' + i_year;
			d_end_date = 2 + '/' + i_day + '/' + i_year;
		 }	 
			
	  }		
	  else if(i_month == 'Mar' || i_month == 'March' || i_month == 'MAR' || i_month == 'MARCH')
	  {	  
	     if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 3 + '-' + 1;		
			d_end_date = i_year + '-' + 3 + '-' + 31;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 3 + '/' + i_year;
			d_end_date = 31 + '/' + 3 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 3 + '/' + 1 + '/' + i_year;
			d_end_date = 3 + '/' + 31 + '/' + i_year;
		 }	 
	
	  }	
	  else if(i_month == 'Apr' || i_month == 'April' || i_month == 'APR' || i_month == 'APRIL')
	  {
	  	if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 4 + '-' + 1;		
			d_end_date = i_year + '-' + 4 + '-' + 30;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 4 + '/' + i_year;
			d_end_date = 30 + '/' + 4 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 4 + '/' + 1 + '/' + i_year;
			d_end_date = 4 + '/' + 30 + '/' + i_year;
	     }	 
	
	  }	
	  else if(i_month == 'May' || i_month == 'MAY')
	  {	  
		 if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 5 + '-' + 1;		
			d_end_date = i_year + '-' + 5 + '-' + 31;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 5 + '/' + i_year;
			d_end_date = 31 + '/' + 5 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 5 + '/' + 1 + '/' + i_year;
			d_end_date = 5 + '/' + 31 + '/' + i_year;
	     }	 
	
	  }	  
	  else if(i_month == 'June' || i_month == 'Jun' || i_month == 'JUN' || i_month == 'JUNE')
	  {	 
		 if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 6 + '-' + 1;		
			d_end_date = i_year + '-' + 6 + '-' + 30;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 6 + '/' + i_year;
			d_end_date = 30 + '/' + 6 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 6 + '/' + 1 + '/' + i_year;
			d_end_date = 6 + '/' + 30 + '/' + i_year;
	     }	 
			
	  }	
	  else if(i_month == 'Jul' || i_month == 'July' || i_month == 'JUL' || i_month == 'JULY')
	  {	  
		 if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 7 + '-' + 1;		
			d_end_date = i_year + '-' + 7 + '-' + 31;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 7 + '/' + i_year;
			d_end_date = 31 + '/' + 7 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 7 + '/' + 1 + '/' + i_year;
			d_end_date = 7 + '/' + 31 + '/' + i_year;
	     }	 
		
	  }	
	  else if(i_month == 'August' || i_month == 'Aug' || i_month == 'AUG' || i_month == 'AUGUST')
	  {	  
		 if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 8 + '-' + 1;		
			d_end_date = i_year + '-' + 8 + '-' + 31;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 8 + '/' + i_year;
			d_end_date = 31 + '/' + 8 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 8 + '/' + 1 + '/' + i_year;
			d_end_date = 8 + '/' + 31 + '/' + i_year;
	     }	 
		
	  }  
	  else if(i_month == 'Sep' || i_month == 'September' || i_month == 'SEP' || i_month == 'SEPTEMBER')
	  {  	
	     if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 9 + '-' + 1;		
			d_end_date = i_year + '-' + 9 + '-' + 30;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 9 + '/' + i_year;
			d_end_date = 30 + '/' + 9 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 9 + '/' + 1 + '/' + i_year;
			d_end_date = 9 + '/' + 30 + '/' + i_year;
	     }	 
	
	  }	
	  else if(i_month == 'Oct' || i_month == 'October' || i_month == 'OCT' || i_month == 'OCTOBER')
	  {	 	
		if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 10 + '-' + 1;		
			d_end_date = i_year + '-' + 10 + '-' + 31;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 10 + '/' + i_year;
			d_end_date = 31 + '/' + 10 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 10 + '/' + 1 + '/' + i_year;
			d_end_date = 10 + '/' + 31 + '/' + i_year;
	     }		
	  }	
	  else if(i_month == 'Nov' || i_month == 'November' || i_month == 'NOV' || i_month == 'NOVEMBER')
	  {	  
		if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 11 + '-' + 1;		
			d_end_date = i_year + '-' + 11 + '-' + 30;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 11 + '/' + i_year;
			d_end_date = 30 + '/' + 11 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 11 + '/' + 1 + '/' + i_year;
			d_end_date = 11 + '/' + 30 + '/' + i_year;
	     }	 
			
	  }	
	  else if(i_month == 'Dec' || i_month == 'December' || i_month == 'DEC' || i_month == 'DECEMBER')
	  {	 
		if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 12 + '-' + 1;		
			d_end_date = i_year + '-' + 12 + '-' + 31;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 12 + '/' + i_year;
			d_end_date = 31 + '/' + 12 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 12 + '/' + 1 + '/' + i_year;
			d_end_date = 12 + '/' + 31 + '/' + i_year;
	     }	 
			
	  }	
 }//Month & Year
 return d_start_date;
}


function get_current_month_end_date(i_month,i_year)
{
	i_month = i_month.toString().trim();
	i_year = i_year.toString().trim();
	var d_start_date = '';
	var d_end_date = '';
	
	var date_format = checkDateFormat();
	 	
 if (_logValidation(i_month) && _logValidation(i_year)) 
 {
 	  if(i_month == 'Jan' || i_month == 'January' || i_month == 'JAN' || i_month == 'JANUARY')
	  {	  	
		 if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 1 + '-' + 1;		
			d_end_date = i_year + '-' + 1 + '-' + 31;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 1 + '/' + i_year;
			d_end_date = 31 + '/' + 1 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 1 + '/' + 1 + '/' + i_year;
			d_end_date = 1 + '/' + 31 + '/' + i_year;
		 }	 
				
	  }	
	  else if(i_month == 'Feb' || i_month == 'February' || i_month == 'FEB' || i_month == 'FEBRUARY')
	  {
	  	if((i_year%4) == 0)
		{
			i_day = 29;
		}
		else
		{
			i_day = 28;
		}
			
		 if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 2 + '-' + 1;		
			d_end_date = i_year + '-' + 2 + '-' + i_day;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 2 + '/' + i_year;
			d_end_date = i_day + '/' + 2 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 2 + '/' + 1 + '/' + i_year;
			d_end_date = 2 + '/' + i_day + '/' + i_year;
		 }	 
			
	  }		
	  else if(i_month == 'Mar' || i_month == 'March' || i_month == 'MAR' || i_month == 'MARCH')
	  {	  
	     if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 3 + '-' + 1;		
			d_end_date = i_year + '-' + 3 + '-' + 31;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 3 + '/' + i_year;
			d_end_date = 31 + '/' + 3 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 3 + '/' + 1 + '/' + i_year;
			d_end_date = 3 + '/' + 31 + '/' + i_year;
		 }	 
	
	  }	
	  else if(i_month == 'Apr' || i_month == 'April' || i_month == 'APR' || i_month == 'APRIL')
	  {
	  	if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 4 + '-' + 1;		
			d_end_date = i_year + '-' + 4 + '-' + 30;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 4 + '/' + i_year;
			d_end_date = 30 + '/' + 4 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 4 + '/' + 1 + '/' + i_year;
			d_end_date = 4 + '/' + 30 + '/' + i_year;
	     }	 
	 
	
	  }	
	  else if(i_month == 'May' || i_month == 'MAY')
	  {	  
		 if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 5 + '-' + 1;		
			d_end_date = i_year + '-' + 5 + '-' + 31;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 5 + '/' + i_year;
			d_end_date = 31 + '/' + 5 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 5 + '/' + 1 + '/' + i_year;
			d_end_date = 5 + '/' + 31 + '/' + i_year;
	     }	 
	
	  }	  
	  else if(i_month == 'June' || i_month == 'Jun' || i_month == 'JUN' || i_month == 'JUNE')
	  {	 
		 if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 6 + '-' + 1;		
			d_end_date = i_year + '-' + 6 + '-' + 30;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 6 + '/' + i_year;
			d_end_date = 30 + '/' + 6 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 6 + '/' + 1 + '/' + i_year;
			d_end_date = 6 + '/' + 30 + '/' + i_year;
	     }	 
			
	  }	
	  else if(i_month == 'Jul' || i_month == 'July' || i_month == 'JUL' || i_month == 'JULY')
	  {	  
		 if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 7 + '-' + 1;		
			d_end_date = i_year + '-' + 7 + '-' + 31;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 7 + '/' + i_year;
			d_end_date = 31 + '/' + 7 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 7 + '/' + 1 + '/' + i_year;
			d_end_date = 7 + '/' + 31 + '/' + i_year;
	     }	 
		
	  }	
	  else if(i_month == 'August' || i_month == 'Aug' || i_month == 'AUG' || i_month == 'AUGUST')
	  {	  
		 if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 8 + '-' + 1;		
			d_end_date = i_year + '-' + 8 + '-' + 31;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 8 + '/' + i_year;
			d_end_date = 31 + '/' + 8 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 8 + '/' + 1 + '/' + i_year;
			d_end_date = 8 + '/' + 31 + '/' + i_year;
	     }	 
		
	  }  
	  else if(i_month == 'Sep' || i_month == 'September' || i_month == 'SEP' || i_month == 'SEPTEMBER')
	  {  	
	     if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 9 + '-' + 1;		
			d_end_date = i_year + '-' + 9 + '-' + 30;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 9 + '/' + i_year;
			d_end_date = 30 + '/' + 9 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 9 + '/' + 1 + '/' + i_year;
			d_end_date = 9 + '/' + 30 + '/' + i_year;
	     }	 
	
	  }	
	  else if(i_month == 'Oct' || i_month == 'October' || i_month == 'OCT' || i_month == 'OCTOBER')
	  {	 	
		if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 10 + '-' + 1;		
			d_end_date = i_year + '-' + 10 + '-' + 31;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 10 + '/' + i_year;
			d_end_date = 31 + '/' + 10 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 10 + '/' + 1 + '/' + i_year;
			d_end_date = 10 + '/' + 31 + '/' + i_year;
	     }	 
	
		
	  }	
	  else if(i_month == 'Nov' || i_month == 'November' || i_month == 'NOV' || i_month == 'NOVEMBER')
	  {	  
		if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 11 + '-' + 1;		
			d_end_date = i_year + '-' + 11 + '-' + 30;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 11 + '/' + i_year;
			d_end_date = 30 + '/' + 11 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 11 + '/' + 1 + '/' + i_year;
			d_end_date = 11 + '/' + 30 + '/' + i_year;
	     }	 
			
	  }	
	  else if(i_month == 'Dec' || i_month == 'December' || i_month == 'DEC' || i_month == 'DECEMBER')
	  {	 
		if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 12 + '-' + 1;		
			d_end_date = i_year + '-' + 12 + '-' + 31;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 12 + '/' + i_year;
			d_end_date = 31 + '/' + 12 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 12 + '/' + 1 + '/' + i_year;
			d_end_date = 12 + '/' + 31 + '/' + i_year;
	     }	 
			
	  }	
 }//Month & Year
 return d_end_date;
}	

function down_excel_function(down_excel,vendor_provision_month,vendor_provision_year,vendor_subsidiary_sub_id)
{
	try
	{
		var strVar1= '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:ss="urn:schemas-microsoft-com:office:spreadsheet" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40">'+
		'<head>'+
		'<meta http-equiv=Content-Type content="text/html; charset=windows-1252"/>'+
        '<meta name=ProgId content=Excel.Sheet/>'+
        '<meta name=Generator content="Microsoft Excel 11"/>'+
        '<!--[if gte mso 9]><xml>'+
        '<x:excelworkbook>'+
        '<x:excelworksheets>'+
        '<x:excelworksheet=sheet1>'+
        '<x:name>** ESTIMATE FILE**</x:name>'+
        '<x:worksheetoptions>'+
        '<x:selected></x:selected>'+
        '<x:freezepanes></x:freezepanes>'+
        '<x:frozennosplit></x:frozennosplit>'+
        '<x:splithorizontal>** FROZEN ROWS + 1 **</x:splithorizontal>'+
        '<x:toprowbottompane>** FROZEN ROWS + 1 **</x:toprowbottompane>'+
        '<x:splitvertical>** FROZEN COLUMNS + 1 **</x:splitvertical>'+
        '<x:leftcolumnrightpane>** FROZEN COLUMNS + 1**</x:leftcolumnrightpane>'+
		    '<x:activepane>0</x:activepane>'+// 0
		    '<x:panes>'+
			'<x:pane>'+
			'<x:number>3</x:number>'+
			'</x:pane>'+
			'<x:pane>'+
		    '<x:number>1</x:number>'+
		    '</x:pane>'+
		    '<x:pane>'+
			'<x:number>2</x:number>'+
        '</x:pane>'+
        '<x:pane>'+
        '<x:number>0</x:number>'+//1
        '</x:pane>'+
        '</x:panes>'+
        '<x:protectcontents>False</x:protectcontents>'+
        '<x:protectobjects>False</x:protectobjects>'+
        '<x:protectscenarios>False</x:protectscenarios>'+
        '</x:worksheetoptions>'+
        '</x:excelworksheet>'+
		'</x:excelworksheets>'+
        '<x:protectstructure>False</x:protectstructure>'+
        '<x:protectwindows>False</x:protectwindows>'+
		'</x:excelworkbook>'+
        
        // '<style>'+
			
			//-------------------------------------
        '</x:excelworkbook>'+
        '</xml><![endif]-->'+
         '<style>'+
		'p.MsoFooter, li.MsoFooter, div.MsoFooter'+
		'{ margin:0in; margin-bottom:.0001pt; mso-pagination:widow-orphan; tab-stops:center 3.0in right 6.0in; font-size:12.0pt;}'+
		'<style>'+

		'<!-- /* Style Definitions */'+

		//'@page Section1'+
		//'{ size:8.5in 11.0in; margin:1.9cm 1.27cm 1.9cm 1.27cm; mso-header-margin:.5in; mso-footer-margin:.5in; mso-title-page:yes; mso-header: h1; mso-footer: f1; mso-first-header: fh1; mso-first-footer: ff1; mso-paper-source:0;}'+

		'div.Section1'+
		'{ page:Section1;}'+

		'table#hrdftrtbl'+
		'{ margin:0in 0in 0in 900in; width:1px; height:1px; overflow:hidden; font-family:Arial;}-->'+

		'</style>'+
		
		
		'</head>'+
		

		'<body>'+
		
		'<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">'
		
		var strVar2 = '';  
		if(vendor_subsidiary_sub_id==parseInt(2))
		{
			strVar2 += "<table width=\"100%\">";
			strVar2 += "<tr>";
			strVar2 += "<td Width=\"10%\" style='text-align:left;font-weight:bold;' font-size=\"10\" \>Month - Year</td>";
			strVar2 += "<td Width=\"20%\" style='text-align:left;font-weight:bold;' font-size=\"10\" \>Employee Name</td>";
			strVar2 += "<td Width=\"5%\" style='text-align:left;font-weight:bold;' font-size=\"10\" \>Location</td>";
			strVar2 += "<td Width=\"5%\" style='text-align:left;font-weight:bold;' font-size=\"10\" \>Currency</td>";
			strVar2 += "<td Width=\"5%\" style='text-align:left;font-weight:bold;' font-size=\"10\" \>Hourly Rate</td>";
			strVar2 += "<td Width=\"20%\" style='text-align:left;font-weight:bold;' font-size=\"10\" \>Project</td>";
			strVar2 += "<td Width=\"5%\" style='text-align:left;font-weight:bold;' font-size=\"10\" \>Project Allocated Hours</td>";
			strVar2 += "<td Width=\"5%\" style='text-align:left;font-weight:bold;' font-size=\"10\" \>Percent of Time</td>";
			strVar2 += "<td Width=\"5%\" style='text-align:left;font-weight:bold;' font-size=\"10\" \>Total Leaves Taken (Hours)</td>";
			strVar2 += "<td Width=\"5%\" style='text-align:left;font-weight:bold;' font-size=\"10\" \>Total Holidays(Hours)</td>";
			strVar2 += "<td Width=\"5%\" style='text-align:left;font-weight:bold;' font-size=\"10\" \>Not Approved (Hours)</td>";
			strVar2 += "<td Width=\"5%\" style='text-align:left;font-weight:bold;' font-size=\"10\" \>Approved (Hours)</td>";
			strVar2 += "<td Width=\"5%\" style='text-align:left;font-weight:bold;' font-size=\"10\" \>Not Submitted (Hours)</td>";
			strVar2 += "<\/tr>";
		}
		else
		{
			strVar2 += "<table width=\"100%\">";
			strVar2 += "<tr>";
			strVar2 += "<td Width=\"10%\" style='text-align:left;font-weight:bold;' font-size=\"10\" \>Month - Year</td>";
			strVar2 += "<td Width=\"20%\" style='text-align:left;font-weight:bold;' font-size=\"10\" \>Employee Name</td>";
			strVar2 += "<td Width=\"5%\" style='text-align:left;font-weight:bold;' font-size=\"10\" \>Location</td>";
			strVar2 += "<td Width=\"5%\" style='text-align:left;font-weight:bold;' font-size=\"10\" \>Currency</td>";
			strVar2 += "<td Width=\"5%\" style='text-align:left;font-weight:bold;' font-size=\"10\" \>Monthly Rate</td>";
			strVar2 += "<td Width=\"20%\" style='text-align:left;font-weight:bold;' font-size=\"10\" \>Project</td>";
			strVar2 += "<td Width=\"5%\" style='text-align:left;font-weight:bold;' font-size=\"10\" \>Project Allocated Hours</td>";
			strVar2 += "<td Width=\"5%\" style='text-align:left;font-weight:bold;' font-size=\"10\" \>Percent of Time</td>";
			strVar2 += "<td Width=\"5%\" style='text-align:left;font-weight:bold;' font-size=\"10\" \>Total Leaves Taken (days)</td>";
			strVar2 += "<td Width=\"5%\" style='text-align:left;font-weight:bold;' font-size=\"10\" \>Total Holidays(days)</td>";
			strVar2 += "<td Width=\"5%\" style='text-align:left;font-weight:bold;' font-size=\"10\" \>Not Approved (days)</td>";
			strVar2 += "<td Width=\"5%\" style='text-align:left;font-weight:bold;' font-size=\"10\" \>Approved (days)</td>";
			strVar2 += "<td Width=\"5%\" style='text-align:left;font-weight:bold;' font-size=\"10\" \>Not Submitted (days)</td>";
			strVar2 += "<\/tr>";
		}
		if (_logValidation(down_excel)) //
		{
			for (var i = 0; i < down_excel.length; i++) //
			{
				var Emp_name = down_excel[i].getText('custrecord_employee_rec_id_sal_up_ven');
				var Emp_location = down_excel[i].getValue('custrecord_location_sla_up_ven');
				var Emp_currency = down_excel[i].getValue('custrecord_currency_ven_pro');
				var Emp_monthly_rate = down_excel[i].getValue('custrecord_cost_sal_up_ven');
				var Emp_Project = down_excel[i].getValue('custrecord_project_assigned');
				var Emp_proj_hours = down_excel[i].getValue('custrecord_proj_hours_allocated');
				var Emp_proj_percent_hours = down_excel[i].getValue('custrecord_percent_of_time_allocated');
				var leave_duration_days = down_excel[i].getValue('custrecord_no_of_leaves_taken');
				var not_approved_days = down_excel[i].getValue('custrecord_no_of_sub_nt_approved_days');
				var approved_days = down_excel[i].getValue('custrecord_no_of_approved_days');
				var not_submitted_days = down_excel[i].getValue('custrecord_no_of_nt_submitted_days');
				var allocated_days = down_excel[i].getValue('custrecord_allocated_time');
				var holidays = down_excel[i].getValue('custrecord_no_of_holidays');
				
				strVar2 += "<tr>";
				strVar2 += "<td Width=\"10%\" style='text-align:left;' font-size=\"10\" \>" + vendor_provision_month + "-" + vendor_provision_year + "</td>";
				strVar2 += "<td Width=\"20%\" style='text-align:left;' font-size=\"10\" \>" + Emp_name + "</td>";
				strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \>" + Emp_location + "</td>";
				strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \>" + Emp_currency + "</td>";
				strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \>" + Emp_monthly_rate + "</td>";
				strVar2 += "<td Width=\"20%\" style='text-align:left;' font-size=\"10\" \>" + Emp_Project + "</td>";
				strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \>" + allocated_days + "</td>";
				strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \>" + Emp_proj_percent_hours + "</td>";
				strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \>" + leave_duration_days + "</td>";
				strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \>" + holidays + "</td>";
				strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \>" + not_approved_days + "</td>";
				strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \>" + approved_days + "</td>";
				strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \>" + not_submitted_days + "</td>";
				strVar2 += "<\/tr>";
			}
		}
		else
		{
			//NO RESULTS FOUND
		}
					
		strVar2 += "<\/table>";			
		strVar1 = strVar1 + strVar2;
		var file = nlapiCreateFile('Vendor Provision Data.xls', 'XMLDOC', strVar1);
		nlapiLogExecution('debug','file:- ',file);
		response.setContentType('XMLDOC','Vendor Provision Data.xls');
		response.write( file.getValue() );	 
		
	}
	catch(err)
	{
		nlapiLogExecution('ERROR','ERROR MESSAGE:- ',err);
	}
}

//END OBJECT CALLED/INVOKING FUNCTION =====================================================