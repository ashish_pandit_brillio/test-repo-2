/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       29 Aug 2017     deepak.srinivas
 *
 */

/**
 * @param {Object} dataIn Parameter object
 * @returns {Object} Output object
 */
function postRESTlet(dataIn) {
	
	
	try{
		var response = new Response();
		nlapiLogExecution('debug', 'dataIn', JSON.stringify(dataIn));
		var current_date = nlapiDateToString(new Date());
		nlapiLogExecution('debug', 'current_date', current_date);
		
		var requestType = dataIn.RequestType;
		nlapiLogExecution('debug', 'requestType', requestType);
		switch (requestType) {

		case "REGION_MASTER":

				{
				response.Data = getRegionMasterData();
				response.Status = true;
				}
				break;
		
		case "LEADER_MASTER":

		{
		response.Data = getLeaderMasterData();
		response.Status = true;
		}
		break;
		
		case "CUSTOMER_CP":

			{
			response.Data = getCustomerCPdata();
			response.Status = true;
			}
			break;		
		}
		
	}
	catch (err) {
		nlapiLogExecution('ERROR', 'postRESTlet', err);
		response.Data = err;
	}
	return response;
}

function getRegionMasterData(){
	try{
		var JSON ={};
		var dataRow = [];
		var search = searchRecord('customrecord_region',null,[new nlobjSearchFilter('isinactive', null, 'is',
                                            						'F')], 
                                         				[
                                         				new nlobjSearchColumn('internalid'),
                                         				new nlobjSearchColumn('name'),
                                         				new nlobjSearchColumn('custrecord_region_head'),
                                         				new nlobjSearchColumn('custrecord_sub_region_of'),
                                         				new nlobjSearchColumn('custrecord_region_head_2'),
                                         				new nlobjSearchColumn('email','custrecord_region_head'),
                                         				new nlobjSearchColumn('email','custrecord_region_head_2')
                                         				]);
		if(search){
			for(var i=0;i<search.length;i++){
				JSON ={
					ID: search[i].getValue('internalid'),
					Name: search[i].getValue('name'),
					RegionHead: search[i].getText('custrecord_region_head'),
					RegionHeadEmail: search[i].getValue('email','custrecord_region_head'),
					SubRegion: search[i].getText('custrecord_sub_region_of'),
					RegionHead2: search[i].getText('custrecord_region_head_2'),
					RegionHead2Email: search[i].getValue('email','custrecord_region_head_2')
				};
				dataRow.push(JSON);
		
			}
		}
		return dataRow;
	}
	catch (err) {
		nlapiLogExecution('ERROR', 'region master error', err);
		response.Data = err;
	}
}

function getLeaderMasterData(){
	try{
		var JSON ={};
		var dataRow = [];
		var search = searchRecord('customrecord_leadership_master',null,[new nlobjSearchFilter('isinactive', null, 'is',
                                            						'F')], 
                                         				[
                                         				new nlobjSearchColumn('internalid'),
                                         				new nlobjSearchColumn('custrecord_employee_name_'),
                                         				new nlobjSearchColumn('custrecord_designation_'),
                                         				new nlobjSearchColumn('custrecord_leadership_master_email'),
                                         				
                                         				]);
		if(search){
			for(var i=0;i<search.length;i++){
				JSON ={
					ID: search[i].getValue('internalid'),
					Name: search[i].getText('custrecord_employee_name_'),
					Designation: search[i].getValue('custrecord_designation_'),
					Email: search[i].getValue('custrecord_leadership_master_email')
				};
				dataRow.push(JSON);
		
			}
		}
		return dataRow;
	}
	catch (err) {
		nlapiLogExecution('ERROR', 'leader master error', err);
		response.Data = err;
	}
}

function getCustomerCPdata(){
	try{
		var JSON ={};
		var dataRow = [];
		var search = searchRecord('customer',null,[new nlobjSearchFilter('isinactive', null, 'is',
                                            						'F')], 
                                         				[
                                         				new nlobjSearchColumn('internalid'),
                                         				new nlobjSearchColumn('entityid'),
                                         				new nlobjSearchColumn('altname'),
                                         				new nlobjSearchColumn('companyname'),
                                         				new nlobjSearchColumn('custentity_clientpartner'),
                                         				new nlobjSearchColumn('subsidiary'),
                                         				new nlobjSearchColumn('custentity_region'),
                                         				new nlobjSearchColumn('custentity_vertical'),
                                         				new nlobjSearchColumn('custentity_verticalhead'),
                                         				new nlobjSearchColumn('email','custentity_clientpartner'),
                                         				new nlobjSearchColumn('email','custentity_verticalhead')
                                         				]);
		if(search){
			for(var i=0;i<search.length;i++){
				JSON ={
					ID: search[i].getValue('internalid'),
					CustomerID: search[i].getValue('entityid'),
					CustomerName: search[i].getValue('altname'),
					CompanyName: search[i].getValue('companyname'),
					ClientPartner: search[i].getText('custentity_clientpartner'),
					Subsidiary: search[i].getText('subsidiary'),
					Region: search[i].getText('custentity_region'),
					Vertical: search[i].getText('custentity_vertical'),
					VerticalHead: search[i].getText('custentity_verticalhead'),
					ClientPartnerEmail: search[i].getValue('email','custentity_clientpartner'),
					VerticalHeadEmail: search[i].getValue('email','custentity_verticalhead')
				};
				dataRow.push(JSON);
		
			}
		}
		return dataRow;
	}
	catch (err) {
		nlapiLogExecution('ERROR', 'customer cp master error', err);
		response.Data = err;
	}
}