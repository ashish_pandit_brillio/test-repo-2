/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       22 Aug 2017     deepak.srinivas
 *
 */

/**
 * @param {Object} dataIn Parameter object
 * @returns {Object} Output object
 */
function postRESTlet(dataIn) {
	try{
		var response = new Response(); 
		var startDate = '';
		var endDate = '';
		var current_date = nlapiDateToString(new Date());
		//Log for current date
		nlapiLogExecution('DEBUG', 'Current Date', 'current_date...' + current_date);
		nlapiLogExecution('debug', 'dataIn', JSON.stringify(dataIn));
		
		//var s_Date =  '8/1/2017';
		var requestType = 'GET';
		var s_Date = dataIn.Date;
		var d_Date = s_Date.split('/');
		var year = d_Date[2];
		if(year){
			startDate = '1/1/'+year
			endDate = '12/31/'+year
		}
		nlapiLogExecution('DEBUG', 'Current Date', 'startDate...' + startDate);
		nlapiLogExecution('DEBUG', 'Current Date', 'endDate...' + endDate);
		
		switch(requestType) {
		
		case M_Constants.Request.Get:
		if (year) {
			response.Data = get_company_holidays(startDate,endDate);
			response.Status = true;
		} else {
			response.Data = "Some error with the data sent";
			response.Status = false;
	}
		break;
		
	}
}
	catch(err){
		nlapiLogExecution('error', 'Restlet Main Function', err);
		throw err;
	}
	nlapiLogExecution('debug', 'response', JSON.stringify(response));
	return response;

}
function get_company_holidays(start_date, end_date) {
	
	try{
	var holiday_list = [];
	var JSON ={};
	//start_date = nlapiDateToString(start_date, 'date');
	//end_date = nlapiDateToString(end_date, 'date');

	// nlapiLogExecution('debug', 'start_date', start_date);
	// nlapiLogExecution('debug', 'end_date', end_date);
	// nlapiLogExecution('debug', 'subsidiary', subsidiary);
	
	var search_company_holiday = nlapiSearchRecord('customrecord_mobile_holiday_calendar',
	        null, [new nlobjSearchFilter('custrecord_mhc_date', null, 'within',
	                        [start_date, end_date])], [ new nlobjSearchColumn('custrecord_mhc_date').setSort(false),
	                new nlobjSearchColumn('custrecord_mhc_title'),
	                new nlobjSearchColumn('custrecord_mhc_location')]);

	if (search_company_holiday) {

		for (var i = 0; i < search_company_holiday.length; i++) {
			
			JSON ={
				Date:	search_company_holiday[i].getValue('custrecord_mhc_date'),
				Description:	search_company_holiday[i].getValue('custrecord_mhc_title'),
				Location:	search_company_holiday[i].getText('custrecord_mhc_location')
					
			};
			holiday_list.push(JSON);
		}
	}

	return holiday_list;
}
	catch(err){
		nlapiLogExecution('error', 'Restlet Holiday Function', err);
		throw err;
	}
}

