							
// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
/*
   	Script Name : UES_SendEmailForTraining.js
	Author      : Ashish Pandit
	Date        : 08 May 2018
    Description : User Event to send email to Assign PM and DM  


	Script Modification Log:

	-- Date --			-- Modified By --				--Requested By--				-- Description --


Below is a summary of the process controls enforced by this script file.  The control logic is described
more fully, below, in the appropriate function headers and code blocks.


     BEFORE LOAD
		- beforeLoadRecord(type)



     BEFORE SUBMIT
		- beforeSubmitRecord(type)


     AFTER SUBMIT
		- afterSubmitRecord(type)



     SUB-FUNCTIONS
		- The following sub-functions are called by the above core functions in order to maintain code
            modularization:

               - NOT USED

*/
}
// END SCRIPT DESCRIPTION BLOCK  ====================================



// BEGIN GLOBAL VARIABLE BLOCK  =====================================
{
	//  Initialize any Global Variables, in particular, debugging variables...

}
// END GLOBAL VARIABLE BLOCK  =======================================



// BEGIN BEFORE LOAD ==================================================

function afterSubmitSendEmailTraining(type)
{
	if(type == 'create'||type == 'edit')
	{
		try
		{
			var recId = nlapiGetRecordId();
			var recType = nlapiGetRecordType();
			var recordObject = nlapiLoadRecord(recType,recId);
			var s_trainingId = recordObject.getFieldValue('custrecord_training_id');
			var s_skill = recordObject.getFieldText('custrecord_training_skill');
			var d_assignDate = recordObject.getFieldValue('custrecord_training_assigned_date');
			var s_assignBy = recordObject.getFieldText('custrecord_training_assigned_by');
			var i_employee = recordObject.getFieldValue('custrecord_training_employee');
			var s_employee = nlapiLookupField('employee',i_employee,'firstname');
			if(i_employee)
			{
				var records = new Array();
				var emailSubject = 'FUEL Notification: Training Assigned'
				var emailBody = 'Dear '+s_employee+'\n'+s_skill+' has been assigned to you by '+s_assignBy +'. Please plan to complete the same.\n\nBrillio-FUEL\nThis email was sent from a notification only address.\nIf you need further assistance, please write to fuel.support@brillio.com';
				records['entity'] = [94862,41571];
				nlapiSendEmail(154256, i_employee, emailSubject,emailBody, null, null, records);
			}
		}
		catch(error)
		{
			nlapiLogExecution('Debug','Error ',error);
		}
	}
}

// END BEFORE LOAD ====================================================


function _logValidation(value) 
{
 if(value != null && value!= 'null' && value != '' && value != undefined && value != undefined && value != 'undefined' && value.toString() != 'NaN' && value != NaN) 
 {
  return true;
 }
 else 
 { 
  return false;
 }
}
// END FUNCTION =====================================================
