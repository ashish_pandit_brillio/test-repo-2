// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
/*
   	Script Name : SUT_GetEmployeeAndSkills.js
	Author      : ASHISH PANDIT
	Date        : 30 JAN 2019
    Description : Suitelet to get employee and skill list 


	Script Modification Log:

	-- Date --			-- Modified By --				--Requested By--				-- Description --
 


Below is a summary of the process controls enforced by this script file.  The control logic is described
more fully, below, in the appropriate function headers and code blocks.


     SCHEDULED FUNCTION
		- scheduledFunction(type)


     SUB-FUNCTIONS
		- The following sub-functions are called by the above core functions in order to maintain code
            modularization:

               - NOT USED

*/
}
// END SCRIPT DESCRIPTION BLOCK  ====================================

function suiteletEmployeeSearch(request,response) {
	nlapiLogExecution('DEBUG','Start','Started');
	var i_mode = request.getParameter('mode');
	var a_data = request.getParameter('data');
    nlapiLogExecution('Debug','i_mode ',i_mode);
    nlapiLogExecution('Debug','a_data ',a_data);
	
/***************** Mode 1 for search by skills *********************/
	if(i_mode == 1) 
	{
		try
		{
			var searchResult = getSkillsList(a_data);
			nlapiLogExecution('debug','Search Result ',JSON.stringify(searchResult))
			response.write(JSON.stringify(searchResult));
		}
		catch(error)
		{
			nlapiLogExecution('Debug','error ',error);
		}
	}
	/*****************Mode 2 for search by employee *********************/ 
	if(i_mode == 2) 
	{
		try
		{
			var searchResult = getEmployeeDetails(a_data);
			nlapiLogExecution('Debug','searchResult ',JSON.stringify(searchResult));
			response.write(JSON.stringify(searchResult));
		}
		catch(error)
		{
			nlapiLogExecution('Debug','error ',error);
		}
	}
}


/******************************Search To Get Employee Data************************************/	
function getEmployeeDetails(data) 
{
	try 
	{
		var a_empid = getIds(data);
		nlapiLogExecution('Debug','a_empid ',a_empid.length);
		//var d_availableFrom = getAllocationEndDate(a_empid);
		if(a_empid.length>0)
		{
			var employeeSearch = nlapiSearchRecord("customrecord_employee_master_skill_data",null,
			[
			   ["custrecord_employee_skill_updated","anyof",a_empid], 
			   "AND", 
			   ["isinactive","is","F"]
			], 
			[
			   new nlobjSearchColumn("custrecord_employee_skill_updated"), 
			   new nlobjSearchColumn("custrecord_primary_updated"), 
			   new nlobjSearchColumn("custrecord_secondry_updated"), 
			   new nlobjSearchColumn("custrecord_family_selected"), 
			   new nlobjSearchColumn("title","CUSTRECORD_EMPLOYEE_SKILL_UPDATED",null), 
			   new nlobjSearchColumn("department","CUSTRECORD_EMPLOYEE_SKILL_UPDATED",null), 
			   new nlobjSearchColumn("location","CUSTRECORD_EMPLOYEE_SKILL_UPDATED",null), 
			   new nlobjSearchColumn("employeestatus","CUSTRECORD_EMPLOYEE_SKILL_UPDATED",null)
			]
			);
			if(employeeSearch)
			{
				nlapiLogExecution('Debug','employeeSearch length ',employeeSearch.length);
				var dataArray = new Array();
				for(var i=0; i<employeeSearch.length;i++)
				{
					var s_empName = employeeSearch[i].getText('custrecord_employee_skill_updated'); 
					temp = s_empName.indexOf("-");
					if(temp>0)
					{
						var resourceName = s_empName.split("-")[1];
					}
					else{
						var resourceName = s_empName;
					}
					var data = {};
					data.empname = {"name":resourceName,"id":employeeSearch[i].getValue('custrecord_employee_skill_updated')};
					data.jobtitle = {"name":employeeSearch[i].getValue("title","CUSTRECORD_EMPLOYEE_SKILL_UPDATED",null),"id":employeeSearch[i].getValue("title","CUSTRECORD_EMPLOYEE_SKILL_UPDATED",null)};
					data.location = {"name":employeeSearch[i].getText("location","CUSTRECORD_EMPLOYEE_SKILL_UPDATED",null),"id":employeeSearch[i].getValue("location","CUSTRECORD_EMPLOYEE_SKILL_UPDATED",null)};
					data.available = getAllocationEndDate(employeeSearch[i].getValue('custrecord_employee_skill_updated'));
					data.practice = {"name":employeeSearch[i].getText("department","CUSTRECORD_EMPLOYEE_SKILL_UPDATED",null),"id":employeeSearch[i].getValue("department","CUSTRECORD_EMPLOYEE_SKILL_UPDATED",null)};
					data.skillfamily = {"name":employeeSearch[i].getText('custrecord_family_selected'),"id":employeeSearch[i].getValue('custrecord_family_selected')};
					data.skills = {"name":employeeSearch[i].getText('custrecord_primary_updated'),"id":employeeSearch[i].getValue('custrecord_primary_updated')};
					data.emplevel = {"name":employeeSearch[i].getText("employeestatus","CUSTRECORD_EMPLOYEE_SKILL_UPDATED",null),"id":employeeSearch[i].getValue("employeestatus","CUSTRECORD_EMPLOYEE_SKILL_UPDATED",null)};
					dataArray.push(data);
				}
				return dataArray;
			}
			else
			{
				var data = {};
				data.empname = {"name": "","id":""};
				data.jobtitle = {"name":""};
				data.location = {"name":"","id":""};
				data.available = {"name":""};
				data.practice = {"name":"","id":""};
				data.skillfamily = {"name":"","id":""};
				data.skills = {"name":"","id":""};
				data.emplevel = {"name":"","id":""};
				return data;
			}
		}
		else
		{
			var data = {};
				data.empname = {"name": "","id":""};
				data.jobtitle = {"name":""};
				data.location = {"name":"","id":""};
				data.available = {"name":""};
				data.practice = {"name":"","id":""};
				data.skillfamily = {"name":"","id":""};
				data.skills = {"name":"","id":""};
				data.emplevel = {"name":"","id":""};
				return data;
		}
		
	} 
	catch (err) 
	{
		nlapiLogExecution('ERROR', 'getTaggedProjectList', err);
	}
}


/**********************************************Function to get employee by skills********************************************/
function getSkillsList(a_data)
{
	var a_skills = getIds(a_data);
	nlapiLogExecution('debug','a_skills ',a_skills.length);
	if(a_skills.length>0)
	{
		var customrecord_employee_master_skill_dataSearch = nlapiSearchRecord("customrecord_employee_master_skill_data",null,
		[
		   ["custrecord_primary_updated","anyof",a_skills]
		], 
		[
		   new nlobjSearchColumn("scriptid").setSort(false), 
		   new nlobjSearchColumn("custrecord_employee_skill_updated"), 
		   new nlobjSearchColumn("custrecord_primary_updated"), 
		   new nlobjSearchColumn("custrecord_secondry_updated"), 
		   new nlobjSearchColumn("custrecord_family_selected"), 
		   new nlobjSearchColumn("title","CUSTRECORD_EMPLOYEE_SKILL_UPDATED",null), 
		   new nlobjSearchColumn("employeestatus","CUSTRECORD_EMPLOYEE_SKILL_UPDATED",null), 
		   new nlobjSearchColumn("location","CUSTRECORD_EMPLOYEE_SKILL_UPDATED",null), 
		   new nlobjSearchColumn("department","CUSTRECORD_EMPLOYEE_SKILL_UPDATED",null),
		   new nlobjSearchColumn("formulatext").setFormula("{custrecord_primary_updated.id}"), 
		   new nlobjSearchColumn("formulanumeric").setFormula(createString(a_skills)).setSort(true)
		]
		);
		if(customrecord_employee_master_skill_dataSearch)
		{
			nlapiLogExecution('Debug','customrecord_employee_master_skill_dataSearch length ',customrecord_employee_master_skill_dataSearch.length)
			var dataArray = new Array();
			var searchLength = customrecord_employee_master_skill_dataSearch.length;
			if(customrecord_employee_master_skill_dataSearch.length>50)
			{
				searchLength = 50;
			}
				
			for(var i=0; i<searchLength; i++)
			{
				var s_empName = customrecord_employee_master_skill_dataSearch[i].getText('custrecord_employee_skill_updated'); 
				temp = s_empName.indexOf("-");
				if(temp>0)
				{
					var resourceName = s_empName.split("-")[1];
				}
				else{
					var resourceName = s_empName;
				}
				var data = {};
				data.empname = {"name":resourceName,"id":customrecord_employee_master_skill_dataSearch[i].getValue('custrecord_employee_skill_updated')};
				data.jobtitle = {"name":customrecord_employee_master_skill_dataSearch[i].getValue("title","CUSTRECORD_EMPLOYEE_SKILL_UPDATED",null)};
				data.location = {"name":customrecord_employee_master_skill_dataSearch[i].getText("location","CUSTRECORD_EMPLOYEE_SKILL_UPDATED",null),"id":customrecord_employee_master_skill_dataSearch[i].getValue("location","CUSTRECORD_EMPLOYEE_SKILL_UPDATED",null)};
				data.available = getAllocationEndDate(customrecord_employee_master_skill_dataSearch[i].getValue('custrecord_employee_skill_updated'));;
				data.practice = {"name":customrecord_employee_master_skill_dataSearch[i].getText("department","CUSTRECORD_EMPLOYEE_SKILL_UPDATED",null),"id":customrecord_employee_master_skill_dataSearch[i].getValue("department","CUSTRECORD_EMPLOYEE_SKILL_UPDATED",null)};
				data.skillfamily = {"name":customrecord_employee_master_skill_dataSearch[i].getText("custrecord_family_selected"),"id":customrecord_employee_master_skill_dataSearch[i].getValue("custrecord_family_selected")};
				data.skills = {"name":customrecord_employee_master_skill_dataSearch[i].getText("custrecord_primary_updated"),"id":customrecord_employee_master_skill_dataSearch[i].getValue("custrecord_primary_updated")};
				data.emplevel = {"name":customrecord_employee_master_skill_dataSearch[i].getText("employeestatus","CUSTRECORD_EMPLOYEE_SKILL_UPDATED",null),"id":customrecord_employee_master_skill_dataSearch[i].getValue("employeestatus","CUSTRECORD_EMPLOYEE_SKILL_UPDATED",null)};
				dataArray.push(data);
			}
			return dataArray;
		}
		else
		{
			var data = {};
			data.empname = {"name": "","id":""};
			data.jobtitle = {"name":""};
			data.location = {"name":"","id":""};
			data.available = {"name":""};
			data.practice = {"name":"","id":""};
			data.skillfamily = {"name":"","id":""};
			data.skills = {"name":"","id":""};
			data.emplevel = {"name":"","id":""};
			return data;
		}
	}
	else{
		var data = {};
			data.empname = {"name": "","id":""};
			data.jobtitle = {"name":""};
			data.location = {"name":"","id":""};
			data.available = {"name":""};
			data.practice = {"name":"","id":""};
			data.skillfamily = {"name":"","id":""};
			data.skills = {"name":"","id":""};
			data.emplevel = {"name":"","id":""};
			return data;
	}
	
}

/*********************************************Function To Get Skill Ids******************************************************/
function getIds(s_skills)
{
	var resultArray = new Array();
	if(s_skills)
	{
		nlapiLogExecution('Debug','s_skills in function',s_skills);
		var temp = s_skills.split(',');
		for(var i=0; i<temp.length;i++)
		{
			resultArray.push(temp[i]);
		}
	}
	return resultArray;
}
/*********************************************Function to get Resource Allocation details **********************************/
function getAllocationEndDate(empId)
{
	var resourceallocationSearch = nlapiSearchRecord("resourceallocation",null,
			[
			 ["startdate","onorbefore","today"], 
			 "AND", 
			 ["enddate","onorafter","today"],
			 "AND", 
			 ["resource","anyof",empId]
			 ], 
			 [
			  new nlobjSearchColumn("enddate")
			 ]
	);
	if(resourceallocationSearch)
	{
		return resourceallocationSearch[0].getValue('enddate');
	}
	else
	{
		return (new Date());
	}
}
/*******************************SEARCH RECORD CODE**********************************************/
function searchRecord(recordType, savedSearch, arrFilters, arrColumns,filterExpression)
{

	try {
		var search = null;

		// if a saved search is provided, load it and add the filters and
		// columns
		if (isNotEmpty(savedSearch)) {
			search = nlapiLoadSearch(recordType, savedSearch);

			if (isArrayNotEmpty(arrFilters)) {
				search.addFilters(arrFilters);
			}

			if (isArrayNotEmpty(arrColumns)) {
				search.addColumns(arrColumns);
			}

			if (isArrayNotEmpty(filterExpression)) {
				search.setFilterExpression(filterExpression);
			}
		}
		// create a new search
		else {
			search = nlapiCreateSearch(recordType, arrFilters, arrColumns);
		}

		// run search
		var resultSet = search.runSearch();

		// iterate through the search and get all data 1000 at a time
		var searchResultCount = 0;
		var resultSlice = null;
		var searchResult = [];

		do {
			resultSlice = resultSet.getResults(searchResultCount,
			        searchResultCount + 1000);

			if (resultSlice) {

				resultSlice.forEach(function(result) {

					searchResult.push(result);
					searchResultCount++;
				});
			}
		} while (isArrayNotEmpty(resultSlice) && resultSlice.length >= 1000);

		return searchResult;
	} catch (err) {
		nlapiLogExecution('ERROR', 'searchRecord', err);
		throw err;
	}
}

function isEmpty(value) {

	return value == null || value == "" || typeof (value) == undefined;
}

function isNotEmpty(value) {

	return !isEmpty(value);
}

function isArrayEmpty(argArray) {

	return !isArrayNotEmpty(argArray);
}

function isArrayNotEmpty(argArray) {

	return (isNotEmpty(argArray) && argArray.length > 0);
}



function createString(skillArray){
	var temp="";
	for (var int = 0; int < skillArray.length; int++) {
		temp += "CASE WHEN INSTR({custrecord_primary_updated.id},"+skillArray[int]+",1)>0 THEN 1 ELSE 0 END";
		if((skillArray.length - int)>1){
			temp += " +";
		}
	}
	return temp;
} 