/**
 * Module Description
 * 
 * Version Date Author Remarks 1.00 17 Feb 2015 nitish.mishra
 * 
 */

function clientLineInit(type) {

	if (type == "expense") {

		if (isTrue(nlapiGetFieldValue('usemulticurrency'))) {
			nlapiDisableLineItemField('expense', 'amount', true);
		}
	}

}

function pageInit() {

	if (isTrue(nlapiGetFieldValue('usemulticurrency'))) {
		nlapiDisableLineItemField('expense', 'amount', true);
	}
}

function postSourcing(type, name) {

	if (isTrue(nlapiGetFieldValue('usemulticurrency'))) {
		nlapiDisableLineItemField('expense', 'amount', true);
	}
}

function fieldChange(type, name, linenum) {

	if (type == 'expense') {

		if (name == 'foreignamount' || name == 'exchangerate') {
			var exchange_rate = nlapiGetCurrentLineItemValue('expense', 'exchangerate');
			var foreign_amount = nlapiGetCurrentLineItemValue('expense', 'foreignamount');
			var total_amount = parseFloat(exchange_rate) * parseFloat(foreign_amount);
			nlapiSetCurrentLineItemValue('expense', 'amount', total_amount, true, true);
		}
	}
}

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your
 * script deployment.
 * 
 * @appliedtorecord recordType
 * 
 * @param {String}
 *            type Sublist internal id
 * @returns {Boolean} True to save line item, false to abort save
 */
function clientValidateLine(type) {

	if (type == "expense") {

		if (!checkIfAllCurrencyAreSame()) {
			alert("Currency for all expenses must be same.\nPlease raise a seperate expenses for different currency.");
			return false;
		}
	}
	return true;
}

function checkIfAllCurrencyAreSame() {

	var currentCurrency = nlapiGetCurrentLineItemValue('expense', 'currency');

	if (currentCurrency) {
		var lineItemCount = nlapiGetLineItemCount('expense');
		nlapiLogExecution('debug', 'line number count', lineItemCount);

		if (lineItemCount == 1) {

			if (nlapiGetCurrentLineItemIndex('expense') == 1) {
				return true;
			}
		}

		for (var linenum = 1; linenum <= lineItemCount; linenum++) {

			if (currentCurrency != nlapiGetLineItemValue('expense', 'currency', linenum)) {
				return false;
			}
		}
	}
	return true;
}
