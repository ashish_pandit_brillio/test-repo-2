/**
 * @author Shweta
 */
// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
/*
   	Script Name : SUT_Salary_Upload_Refresh.js
	Author      : Shweta Chopde
	Date        : 26 Aug 2014
	Description :


	Script Modification Log:

	-- Date --			-- Modified By --				--Requested By--				-- Description --



Below is a summary of the process controls enforced by this script file.  The control logic is described
more fully, below, in the appropriate function headers and code blocks.


     SUITELET
		- suiteletFunction(request, response)


     SUB-FUNCTIONS
		- The following sub-functions are called by the above core functions in order to maintain code
            modularization:

               - NOT USED

*/
}
// END SCRIPT DESCRIPTION BLOCK  ====================================



// BEGIN GLOBAL VARIABLE BLOCK  =====================================
{
	//  Initialize any Global Variables, in particular, debugging variables...




}
// END GLOBAL VARIABLE BLOCK  =======================================





// BEGIN SUITELET ==================================================

function suiteletFunction(request, response)
{
  var i_percent_complete = '';	
  try 
  {
  	var i_click_status = request.getParameter('custscript_click_type_refresh');
	var i_recordID = request.getParameter('custscript_record_id_refresh');
		
	nlapiLogExecution('DEBUG', 'suiteletFunction', ' Click Status -->' + i_click_status);
	nlapiLogExecution('DEBUG', 'suiteletFunction', ' Record ID -->' + i_recordID);
	
	if(i_click_status == 'VALIDATE')
	{		
		var column = new Array();
		var filters = new Array();

	
		filters.push(new nlobjSearchFilter('status', null, 'anyof', ['PROCESSING', 'PENDING']));
		filters.push(new nlobjSearchFilter('internalid', 'script','is','242'));

		column.push(new nlobjSearchColumn('name', 'script'));
		column.push(new nlobjSearchColumn('internalid', 'scriptdeployment'));
		column.push(new nlobjSearchColumn('datecreated'));
		column.push(new nlobjSearchColumn('status'));
		column.push(new nlobjSearchColumn('startdate'));
		column.push(new nlobjSearchColumn('enddate'));
		column.push(new nlobjSearchColumn('queue'));
		column.push(new nlobjSearchColumn('percentcomplete'));
		column.push(new nlobjSearchColumn('queueposition'));
		column.push(new nlobjSearchColumn('percentcomplete'));

		var a_search_results =  nlapiSearchRecord('scheduledscriptinstance', null, filters, column);
	    
		if(_logValidation(a_search_results)) 
	    {
	    	nlapiLogExecution('DEBUG','suiteletFunction','Search Results Length -->'+a_search_results.length);
	    	
			for(var i=0;i<a_search_results.length;i++)
	    	{
	    		var s_script  = a_search_results[i].getValue('name', 'script');
				nlapiLogExecution('DEBUG','suiteletFunction','Script -->'+s_script);

				var d_date_created  = a_search_results[i].getValue('datecreated');
				nlapiLogExecution('DEBUG','suiteletFunction','Date Created -->'+d_date_created);

				var i_percent_complete  = a_search_results[i].getValue('percentcomplete');
				nlapiLogExecution('DEBUG','suiteletFunction','Percent Complete -->'+i_percent_complete);

				var s_script_status  = a_search_results[i].getValue('status');
				nlapiLogExecution('DEBUG','suiteletFunction','Script Status -->'+s_script_status);
			
			   i_percent_complete = i_percent_complete;
			
	    	}
	    }
	    else
	    {	    	
         i_percent_complete = '100%';            
	    }
		
	    if(_logValidation(i_recordID)) 
		{ 
			var o_recordOBJ = nlapiLoadRecord('customrecord_salary_upload_process',i_recordID)
			
			if(_logValidation(o_recordOBJ)) 
			{
				o_recordOBJ.setFieldValue('custrecord_percent_complete',i_percent_complete)
				
				var i_submitID = nlapiSubmitRecord(o_recordOBJ,true,true);
				nlapiLogExecution('DEBUG', 'suiteletFunction', ' Submit ID -->' + i_submitID);
	 
				
			}//Record OBJ				
		}//Record ID			
				
	}//Validate
	if(i_click_status == 'CREATE JE')
	{
		
	}//JE Creation
	if(i_click_status == 'DELETE')
	{
		
	}//JE Delete
	nlapiSetRedirectURL('RECORD', 'customrecord_salary_upload_process', i_recordID, null,null);
   nlapiLogExecution('DEBUG', 'suiteletFunction', ' Suitelet Called ......... ' );	
				  	
  }
  catch(exception)
  {
    nlapiLogExecution('DEBUG', 'ERROR',' Exception Caught -->' + exception);	
  }	
}

// END SUITELET ====================================================




// BEGIN OBJECT CALLED/INVOKING FUNCTION ===================================================
function _logValidation(value) 
{
 if(value!='null' && value != null && value != null && value != '' && value != undefined && value != undefined && value != 'undefined' && value!= 'undefined'&& value!= 'NaN' && value != NaN) 
 {
  return true;
 }
 else 
 { 
  return false;
 }
}
// END OBJECT CALLED/INVOKING FUNCTION =====================================================
