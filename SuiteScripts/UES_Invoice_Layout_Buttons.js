/**
 * @author Shweta
 */
// BEGIN SCRIPT DESCRIPTION BLOCK ==================================
{
	/*
	 * Script Name : UES_Invoice_Layout_Buttons.js Author : Shweta Chopde Date :
	 * 14 July 2014 Description : Create a button on Invoice for the generation
	 * of Invoice Layout
	 * 
	 * 
	 * Script Modification Log: -- Date -- -- Modified By -- --Requested By-- --
	 * Description -- 26 June 2014 Shweta Chopde Shekar Modified IDs as per the
	 * sandbox accounts
	 * 
	 * 
	 * 
	 * Below is a summary of the process controls enforced by this script file.
	 * The control logic is described more fully, below, in the appropriate
	 * function headers and code blocks.
	 * 
	 * 
	 * BEFORE LOAD - beforeLoadRecord(type)
	 * 
	 * 
	 * 
	 * BEFORE SUBMIT - beforeSubmitRecord(type)
	 * 
	 * 
	 * AFTER SUBMIT - afterSubmitRecord(type)
	 * 
	 * 
	 * 
	 * SUB-FUNCTIONS - The following sub-functions are called by the above core
	 * functions in order to maintain code modularization: - NOT USED
	 * 
	 */
}
// END SCRIPT DESCRIPTION BLOCK ====================================

// BEGIN GLOBAL VARIABLE BLOCK =====================================
{
	// Initialize any Global Variables, in particular, debugging variables...

}
// END GLOBAL VARIABLE BLOCK =======================================

// BEGIN BEFORE LOAD ==================================================

function beforeLoad_INV_buttons(type) {
	try {
		form.setScript('customscript_cli_invoice_layout_button_l');

		var i_layout_type = nlapiGetFieldValue('custbody_layout_type')

		if (i_layout_type == 1) {
			form.addButton('custpage_BLLC_T_M_Invoice_button', 'BLLC T&M',
					'open_BLLC_T_M_Invoice()');
		}// BLLC T&M
		if (i_layout_type == 2) {
			form.addButton('custpage_BLLC_T_M_Group_Invoice_button',
					'BLLC T&M Group', 'open_BLLC_T_M_Group_Invoice()');
		}// BLLC T&M Group
		if (i_layout_type == 3) {
			form.addButton('custpage_BLLC_FP_Invoice_button', 'BLLC FP',
					'open_BLLC_FP_Invoice()');
		}// BLLC FP
		if (i_layout_type == 4) {
			form.addButton('custpage_BLLC_Exp_Invoice_button', 'BLLC Exp',
					'open_BLLC_Exp_Invoice()');
		}// BLLC Exp
		if (i_layout_type == 5) {
			form.addButton('custpage_Exports_BLR_US_Onsite_button',
					'Exports - BLR US Onsite',
					'open_Exports_BLR_US_Onsite_Invoice()');
		}// Exports- BLR US Onsite
		if (i_layout_type == 6) {
			form.addButton('custpage_Exports_BLR UK_button',
					'Exports - BLR UK', 'open_Exports_BLR_UK_Invoice()');
		}// Exports- BLR UK
		if (i_layout_type == 7) {
			form.addButton('custpage_Domestic_BLR_button', 'Domestic - BLR',
					'open_Domestic_BLR_Invoice()');
		}// Domestic- BLR
		if (i_layout_type == 8) {
			form.addButton('custpage_Domestic_Tvm_T_M_button',
					'Domestic - Tvm T&M', 'open_Domestic_Tvm_T_M_Invoice()');
		}// Domestic- Tvm T&M
		if (i_layout_type == 9) {
			form.addButton('custpage_Dom_Tvm_License_button',
					'Dom - Tvm License', 'open_Dom_Tvm_License_Invoice()');
		}// Dom- Tvm License
		if (i_layout_type == 10) {
			form.addButton('custpage_Dom_Tvm_FP_Invoice_button',
					'Dom - Tvm FP', 'open_Dom_Tvm_FP_Invoice()');
		}// Dom- Tvm FP
		if (i_layout_type == 11) {
			form.addButton('custpage_Exports_BLR_US_Offshore_button',
					'Exports - BLR US Offshore',
					'open_Exports_BLR_US_Offshore_Invoice()');
		}// Exports - BLR US Offshore
		if (i_layout_type == 12) {
			form
					.addButton('custpage_Exports_BLR_US_Offshore_button',
							'BLLC T&M Group - Dis',
							'open_BLLC_T_M_Group_Dis_Invoice()');
		}// BLLC T&M Group Invoice
		if (i_layout_type == 13) {
			form.addButton('custpage_Exports_BLR_US_Offshore_button',
					'BLLC T&M Group - Move',
					'open_BLLC_T_M_Group_Move_Invoice()');
		}// BLLC T&M Move Invoice
		if (i_layout_type == 14) {
			form.addButton('custpage_llc_tm_discount', 'BLLC T&M - Discount',
					'open_BLLC_TM_Discount()');
		}// BLLC T&M Move Invoice
		if (i_layout_type == 15) {
			form.addButton('custpage_llc_comity_tm', 'BLLC Comity T&M',
					'open_BLLC_Comity_TM()');
		}// BLLC Comity T&M Invoice
		if (i_layout_type == 16) {
			form.addButton('custpage_llc_comity_fp', 'BLLC Comity FP',
					'open_BLLC_Comity_FP()');
		}// BLLC Comity FP Invoice
		if (i_layout_type == 17) {
			form.addButton('custpage_uk_fp', 'Brillio UK FP',
					'open_UK_FP()');
		}// UK FP Invoice
      if (i_layout_type == 19) {
			form.addButton('custpage_brillio_canada', 'Brillio CAD T&M',
				'open_Brillio_Canada_TM()');
		}// Brillio CAD T&M Invoice
		if (i_layout_type == 20) {
			form.addButton('custpage_brillio_canada', 'Brillio CAD FP',
				'open_Brillio_Canada_FP()');
		}// Brillio CAD T&M Invoice
		nlapiLogExecution('DEBUG', 'beforeLoadRecord',
				' Button Created Successfully  ..........');

	} catch (exception) {
		nlapiLogExecution('DEBUG', 'ERROR', ' Exception Caught -->' + exception);
	}
	return true;
}

// END BEFORE LOAD ====================================================

// BEGIN BEFORE SUBMIT ================================================

function beforeSubmitRecord(type) {
	/*
	 * On before submit: - PURPOSE -
	 * 
	 * FIELDS USED:
	 * 
	 * --Field Name-- --ID--
	 * 
	 * 
	 */

	// LOCAL VARIABLES
	// BEFORE SUBMIT CODE BODY
	return true;

}

// END BEFORE SUBMIT ==================================================

// BEGIN AFTER SUBMIT =============================================

function afterSubmitRecord(type) {
	/*
	 * On after submit: - PURPOSE -
	 * 
	 * FIELDS USED:
	 * 
	 * --Field Name-- --ID--
	 * 
	 * 
	 */

	// LOCAL VARIABLES
	// AFTER SUBMIT CODE BODY
	return true;

}

// END AFTER SUBMIT ===============================================

// BEGIN FUNCTION ===================================================
{

}
// END FUNCTION =====================================================
