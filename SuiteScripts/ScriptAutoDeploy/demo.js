
/**
 * @NApiVersion 2.1
 * test
 * test1
 * @NScriptType Suitelet
 * test deploy
 * test 
 * test
 * @NModuleScope SameAccount
 * test deploy
 */
define([], () => {
  function onRequest({ request, response }) {
    try {
      if (request.method === "GET") log.debug("get request", "received");
      else log.debug("post request", "received");
      
    } catch (err) {
      log.error("Error", err);
    }
  }
  return {
    onRequest,
  };
});
