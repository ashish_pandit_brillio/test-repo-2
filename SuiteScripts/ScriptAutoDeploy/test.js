// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
    /*
        Script Name:
        Author:Shamanth Kidiyoor
        Company:Brillio
        Date:
        Description:

        Script Modification Log:

        -- Date --			-- Modified By --				--Requested By--				-- Description --
        10-03-2020			Praveeena Madem					Deepak Ms						Change the code to update resource allocation master data and employee data instead of calling function in userevent, call function in suitelet.
        31-03-2020			Praveena Madem					Deepak Ms						Logic added to send notification to timesheet and opeartions team when termination failed due to filled timesheets
        08-02-2022          Raghav Gupta                                                    Added the logic to set vicarious email global permission to none.  

        Below is a summary of the process controls enforced by this script file.  The control logic is described more fully, below, in the appropriate function headers and code blocks.

         BEFORE LOAD 
            - NA

         BEFORE SUBMIT
            - BeforeSubmit(type)

         AFTER SUBMIT
            - NA

         SUB-FUNCTIONS
            - The following sub-functions are called by the above core functions in order to maintain code modularization:
            - NOT USED
    */
}
// END SCRIPT DESCRIPTION BLOCK  ====================================

// BEGIN GLOBAL VARIABLE BLOCK  =====================================
{
    // --

}
// END GLOBAL VARIABLE BLOCK  =======================================


// BEGIN BEFORE SUBMIT ================================================
function BeforeSubmit(type) {
    /*  On before submit:
        - PURPOSE
        -This function is used to call the suitelet to update the resource allocation records and update the employee record.and update the custom record.
        FIELDS USED:
        --Field Name--				--ID--
        FUSION JSON DATA			'custrecord_fi_fusion_json_data'
        TYPE						'custrecord_fi_fit_type'
    */
    //  LOCAL VARIABLES

    //  BEFORE SUBMIT CODE BODY
    try {
        var i_record_type = nlapiGetFieldValue('custrecord_fi_fit_type');
        if (i_record_type == 3) {
            var s_emp_term = nlapiGetFieldValue('custrecord_fi_fusion_json_data');
            var o_emp_term_data = JSON.parse(s_emp_term);
            var o_emp_data = o_emp_term_data.Assignment;
            var i_fusionNumber = o_emp_data.FusionID;
            var d_termDate = o_emp_data.Termination_Date;
            var termination_type = o_emp_data.Termination_Action
            //var result = FusionRecordSearch(i_fusionNumber, d_termDate);

            var today = new Date();
            var lwd = nlapiStringToDate(formatDate(d_termDate));
            var timeDiff = Math.abs((today.getTime()) - (lwd.getTime()))
            var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));

            if (diffDays >= 14) {
                var recieptients = ["shamanth.k@brillio.com", "business.ops@brillio.com"]; //157414
                var s_emailbody = "This is to inform you that employee termination is blocked as LWD is backdated to more than 14 days. LWD is " + formatDate(d_termDate) + ' for the employee ' + i_fusionNumber + " . Please check with HR team for the reason for late updation of LWD and proceed with terminating the resource manually from Netsuite.";
                nlapiSendEmail("442", ["shamanth.k@brillio.com", "information.systems@brillio.com"], 'Failed Employee Termination', s_emailbody);
                nlapiLogExecution('debug', 'ERROR WHILE UPDATING RESOURCE ALLCOATION DATA', resultedata);
            }

            nlapiLogExecution('debug', 'SUITLET START DATE', new Date());
            if (termination_type != 'Global Transfer' && diffDays < parseInt(14)) {
                var url = nlapiResolveURL('SUITELET', 'customscript_tm_terminateemp_updatera', 'customdeploy1', true);
                var response_data = nlapiRequestURL(url, s_emp_term, { "content-type": 'application/json' }, 'POST'); //Call Suitelet
                nlapiLogExecution('debug', 'SUITLET END DATE', new Date());
                var obj_responsebody = JSON.parse(response_data.getBody());
                if (response_data.getCode() == "200" && obj_responsebody.success == 'T') {
                    var i_emp_id = obj_responsebody.stuff[0]["Employee ID"]; //Fetch Employee internal id from suitelet
                    if (i_emp_id) {
                        nlapiSetFieldValue('custrecord_fu_emp_id', i_emp_id);
                        nlapiSetFieldValue('custrecord_fi_fusion_id', JSON.stringify(i_fusionNumber));
                        var o_employeeRecord = nlapiLoadRecord('employee', i_emp_id);
                        o_employeeRecord.setFieldValue('giveaccess', 'F'); //Removing access
                        o_employeeRecord.setFieldValue('custentity_employee_inactive', 'T'); //System Inactice
                        o_employeeRecord.setFieldValue('isjobresource', 'F'); //Project Resource Remove
                        o_employeeRecord.setFieldValue('custentity_lwd', formatDate(d_termDate)); //Update the termination date at employee master:

                        //added by raghav
                        var index = o_employeeRecord.findLineItemValue('empperms', 'permkey1', 'ADMI_VICARIOUS_EMAILS')
                        o_employeeRecord.setLineItemValue('empperms', 'permlevel1', index, 0);
                        //

                        var i_Id = nlapiSubmitRecord(o_employeeRecord);
                        nlapiLogExecution('debug', 'Employee Inactive Internal ID', i_Id);
                        nlapiSetFieldValue('custrecord_fi_json_error_log', ''); s
                    }
                }
                else {
                    nlapiSetFieldValue('custrecord_fi_fusion_id', JSON.stringify(i_fusionNumber));
                    nlapiLogExecution('debug', 'obj_responsebody', obj_responsebody);
                    var resultedata = "Code:" + obj_responsebody.Code + '  Details:' + obj_responsebody.Details + ' ' + 'Employee ID:- ' + ' ' + i_fusionNumber;
                    nlapiLogExecution('debug', 'ERROR WHILE UPDATING RESOURCE ALLCOATION DATA', response_data.getBody());
                    nlapiSetFieldValue('custrecord_fi_json_error_log', resultedata);
                    if (obj_responsebody.id) {
                        nlapiSetFieldValue('custrecord_fu_emp_id', obj_responsebody.id);
                    }
                    //Logic added to send notification to timesheet and opeartions team when termination failed due to filled timesheets
                    if (obj_responsebody.Details == "Employee has already filled timesheets for this period for this allocation") {
                        // _sendEmailTotimesheetTeam(obj_responsebody.id, obj_responsebody.s_emp_name, formatDate(d_termDate));//commented sending an email due to notification is sending on map reduce script.
                        nlapiLogExecution('debug', 'ERROR TIMESHEETS', resultedata);
                    }
                    else {
                        var recieptients = ["deepak.srinivas@brillio.com", "information.systems@brillio.com"]; //157414
                        var s_emailbody = "This is informed you that employee termination is failed due to" + resultedata;
                        nlapiSendEmail("200580", recieptients, 'Failed Employee Termination', s_emailbody);
                        nlapiLogExecution('debug', 'ERROR WHILE UPDATING RESOURCE ALLCOATION DATA', resultedata);
                    }
                }
            }
        }
    }
    catch (e) {
        nlapiLogExecution('debug', 'Error-1', e);
        var emp_term = nlapiGetFieldValue('custrecord_fi_fusion_json_data');
        nlapiLogExecution('debug', 'emp_term', emp_term);
        var emp_term_data = JSON.parse(emp_term);
        var emp_data = emp_term_data.Assignment;
        var i_fusionNumber = emp_data.FusionID;
        nlapiSetFieldValue('custrecord_fi_fusion_id', JSON.stringify(i_fusionNumber));
        e += ' ' + 'Employee ID:- ' + ' ' + i_fusionNumber;
        nlapiSetFieldValue('custrecord_fi_json_error_log', e);
        nlapiLogExecution('debug', 'We are in catch block', e);
        var recieptients = ["deepak.srinivas@brillio.com", "information.systems@brillio.com"]; //157414
        var s_emailbody = "This is informed you that employee termination is failed due to" + e;
        nlapiSendEmail("200580", recieptients, 'Failed Employee Termination', s_emailbody, null, null, null);
    }
    return true;
}

// END BEFORE SUBMIT ==================================================




/*
var TermList = [];

function FusionRecordSearch(fusionNumber, termDate) {
    try {
        if (fusionNumber != null) {
            var search_employee = nlapiSearchRecord('employee', null, ["formulatext: {custentity_fusion_empid}", "is", fusionNumber], [new nlobjSearchColumn(
                'internalid')]);
            if (_logValidation(search_employee)) {


                var id = search_employee[0].getId();
                nlapiLogExecution('DEBUG', 'EMP Internalid', id);
                nlapiSetFieldValue('custrecord_fu_emp_id', id);
                var employeeRecord = nlapiLoadRecord('employee', parseInt(id));
                //Update Last working date in Resource Allocation
                //nlapiSearchRecord(type, id, filters, columns)
                var resourceAllocationSearch = nlapiSearchRecord('resourceallocation', null, [
                    new nlobjSearchFilter('internalid', 'employee', 'anyof', id),
                    new nlobjSearchFilter('enddate', null, 'onorafter', formatDate(termDate))
                ]);
                if (isArrayNotEmpty(resourceAllocationSearch)) {
                    for (var index = 0; index < resourceAllocationSearch.length; index++) {
                        nlapiLogExecution('DEBUG', 'Allocation', resourceAllocationSearch.length);
                        var resAlloRec = nlapiLoadRecord('resourceallocation', resourceAllocationSearch[index].getId());
                        var s_startDate = resAlloRec.getFieldValue('startdate');
                        var d_startDate = nlapiStringToDate(s_startDate);
                        var s_termDate = formatDate(termDate);
                        var d_termDate = nlapiStringToDate(s_termDate);
                        nlapiLogExecution('DEBUG', 'Start Date', resAlloRec.getFieldValue('startdate'));
                        nlapiLogExecution('debug', 'd_startDate', d_startDate);
                        nlapiLogExecution('DEBUG', 'term date', d_termDate);
                        if (d_startDate > d_termDate) {
                            nlapiLogExecution('DEBUG', 'Termdate', formatDate(termDate));
                            nlapiLogExecution('debug', 'Resource ID', resourceAllocationSearch[index].getId());
                            nlapiDeleteRecord('resourceallocation', resourceAllocationSearch[index].getId());
                        } else {
                            resAlloRec.setFieldValue('enddate', formatDate(termDate)); // Allocation End date
                            resAlloRec.setFieldValue('custeventbenddate', formatDate(termDate)); // Billing End date
                            resAlloRec.setFieldValue('custevent_allocation_status', '22'); // Allocation status, Employee terminated 
                            resAlloRec.setFieldValue('notes', 'Employee terminated, hence allocation end date changed');
                            resAlloRec.setFieldValue('custevent_ra_last_modified_by', 442);
                            resAlloRec.setFieldValue('custevent_ra_last_modified_date', nlapiDateToString(new Date(), 'datetimetz'));
                            //Updated submit parameters
                            nlapiSubmitRecord(resAlloRec);
                            nlapiLogExecution('DEBUG', 'Allocation ended');
                            nlapiLogExecution('DEBUG', 'Allocation', resAlloRec);
                        }

                    }
                }

                // Update the subtier records
                // Get all subtier records that end after the LWD

                // Update the subtier records
                // Get all subtier records that end after the LWD
                var subtierSearch = nlapiSearchRecord('customrecord_subtier_vendor_data', null, [new nlobjSearchFilter('custrecord_stvd_contractor', null, 'anyof', id), new nlobjSearchFilter('custrecord_stvd_end_date', null,
                    'onorafter', formatDate(termDate))]);

                // change the end date of all the found subtier record
                if (subtierSearch) {

                    for (var index = 0; index < subtierSearch.length; index++) {
                        var subTierRec = nlapiLoadRecord(
                            'customrecord_subtier_vendor_data',
                            subtierSearch[index].getId());

                        // if start date is after the termination date,
                        // change
                        // the start date
                        if (nlapiStringToDate(subTierRec.getFieldValue('custrecord_stvd_start_date')) > nlapiStringToDate(formatDate(termDate))) {
                            subTierRec.setFieldValue('isinactive', 'T');
                            subTierRec.setFieldValue(
                                'custrecord_stvd_start_date', formatDate(termDate));
                        }

                        subTierRec.setFieldValue(
                            'custrecord_stvd_end_date', formatDate(termDate));

                        nlapiSubmitRecord(subTierRec, false, true);
                    }
                }

                //nlapiLogExecution('debug','Employee Inactive');
                employeeRecord.setFieldValue('giveaccess', 'F'); //Removing access
                employeeRecord.setFieldValue('custentity_employee_inactive', 'T'); //System Inactice
                employeeRecord.setFieldValue('isjobresource', 'F'); //Project Resource Remove
                employeeRecord.setFieldValue('custentity_lwd', formatDate(termDate)); //Update the termination date at employee master:
                var emp_term = nlapiGetFieldValue('custrecord_fi_fusion_json_data');
                var emp_term_data = JSON.parse(emp_term);
                var emp_data = emp_term_data.Assignment;
                var fusionNumber = emp_data.FusionID;
                nlapiSetFieldValue('custrecord_fi_fusion_id', JSON.stringify(fusionNumber));
                var i_Id = nlapiSubmitRecord(employeeRecord);
                nlapiLogExecution('debug', 'Employee Inactive Internal ID', i_Id);
                TermList.push({
                    'Employee ID': i_Id,
                    'FusionID': fusionNumber
                });
            }
        }
        return TermList;
    } catch (e) {
        var emp_term = nlapiGetFieldValue('custrecord_fi_fusion_json_data');
        var emp_term_data = JSON.parse(emp_term);
        var emp_data = emp_term_data.Assignment;
        var fusionNumber = emp_data.FusionID;
        nlapiSetFieldValue('custrecord_fi_fusion_id', JSON.stringify(fusionNumber));
        e += ' ' + 'Employee ID:- ' + ' ' + fusionNumber;
        nlapiSetFieldValue('custrecord_fi_json_error_log', e);
        nlapiLogExecution('debug', 'We are in catch block', e);
    }
}
*/


function _logValidation(value) {
    if (value != null && value != '' && value != undefined && value.toString() != 'NaN' && value != NaN) {
        return true;
    } else {
        return false;
    }
}

function dateFormat(date) {
    var date_return = '';
    if (date) {
        var dateObj = date.split('-');
        var month = dateObj[1];
        var year = dateObj[0];
        var date_d = dateObj[2].split('T');
        //var date_d = dateObj[2].split('T');
        nlapiLogExecution('DEBUG', 'dateObj[2]', dateObj[2]);
        var temp = new Array();
        temp = dateObj[2].substring(0, 2);
        //date_d = date_d.split(',')[0];
        date_return = month + '/' + temp + '/' + year;
    }
    return date_return;
}

function formatDate(fusionDate) {

    if (fusionDate) {
        // fusionDate = "2016-07-14T00:00:00.000Z";
        var datePart = fusionDate.split('T')[0];
        var dateSplit = datePart.split('-');
        // MM/DD/ YYYY
        var netsuiteDateString = dateSplit[1] + "/" + dateSplit[2] + "/" +
            dateSplit[0];
        return netsuiteDateString;
    }
    return "";
}

// FUnction Added by praveen on 31-03-2020
function _sendEmailTotimesheetTeam(i_employee, s_emp_name, s_termDate) {
    //var timesheetssearch = nlapiSearchRecord('timeentry', null, [
    var timesheetssearch = nlapiSearchRecord('timebill', null, [
        new nlobjSearchFilter('employee', null, 'anyof', i_employee),
        new nlobjSearchFilter('date', null, 'after', s_termDate)
        //,new nlobjSearchFilter('type', null, 'anyof', 'A'),
        //new nlobjSearchFilter('internalid', 'customer', 'anyof', i_project) //use in SB			
        //new nlobjSearchFilter('internalid','customerproject','anyof',project)// use in PROD
    ],
        [
            new nlobjSearchColumn("employee", "timeSheet", "GROUP").setSort(true),
            new nlobjSearchColumn("internalid", "timeSheet", "GROUP"),
            new nlobjSearchColumn("startdate", "timeSheet", "GROUP").setSort(false),
            new nlobjSearchColumn("approvalstatus", "timeSheet", "GROUP")
        ]);
    var strVar = "";
    strVar += "<p>Hi Operations\/Timesheet Team,<\/p>";
    strVar += "<p>Resource " + s_emp_name + " already filled the timesheet for the below week\/s. Please make sure the timesheets are deleted then further to that, kindly update allocation dates and employee last working date as per the termination date.<\/p>";
    strVar += "<p>Termination Date: " + s_termDate + " (MM/DD/YYYY)<\/p>";
    strVar += "<p>Timesheets &ndash;<\/p>";
    strVar += "<table border=\"1\" cellspacing=\"0\" style=\"table-layout: fixed;\">";
    strVar += "<tbody>";
    strVar += "<tr>";
    strVar += "<td style=\"width: 200px;\">";
    strVar += "<p><strong>Resource<\/strong><\/p>";
    strVar += "<\/td>";
    strVar += "<td style=\"width: 82px;\">";
    strVar += "<p><strong>Timesheet ID<\/strong><\/p>";
    strVar += "<\/td>";
    strVar += "<td style=\"width: 113px;\">";
    strVar += "<p><strong>Week Start Date<\/strong><\/p>";
    strVar += "<\/td>";
    strVar += "<td style=\"width: 112px;\">";
    strVar += "<p><strong>Timesheet Status<\/strong><\/p>";
    strVar += "<\/td>";
    strVar += "<\/tr>";
    if (timesheetssearch) {
        for (var i = 0; timesheetssearch.length > i; i++) {
            var cols = timesheetssearch[i].getAllColumns();
            if (timesheetssearch[i].getValue(cols[0])) {
                strVar += "<tr>";
                strVar += "<td style=\"width: 200px;\">" + timesheetssearch[i].getText(cols[0]) + "<\/td>";
                strVar += "<td style=\"width: 82px;\" align=\"center\">" + timesheetssearch[i].getValue(cols[1]) + "<\/td>";
                strVar += "<td style=\"width: 113px;\" align=\"center\">" + timesheetssearch[i].getValue(cols[2]) + "<\/td>";
                strVar += "<td style=\"width: 112px;\">" + timesheetssearch[i].getText(cols[3]) + "<\/td>";
                strVar += "<\/tr>";
            }
        }
    }
    strVar += "<\/tbody>";
    strVar += "<\/table>";
    strVar += "<p>&nbsp;<\/p>";
    strVar += "<p>If you need any additional support, please reach out to information systems team.<\/p>";
    strVar += "<p>&nbsp;<\/p>";
    strVar += "<p>Thanks,<\/p>";
    strVar += "<p>Information Systems.<\/p>";

    nlapiSendEmail('442', ['timesheet@brillio.com', 'business.ops@brillio.com'], 'Resource Termination Failed Notification', strVar, 'information.systems@brillio.com', null, null);
}