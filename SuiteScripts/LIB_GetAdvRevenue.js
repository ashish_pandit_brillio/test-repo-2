function data_flow(ProjectId){
try{
	var excel_data_res=[];
	
	var i_pro_internal_id = ProjectId;
	//Conversion rate search
		
	var o_PL_conversion_table=PL_curencyexchange_rate("T");
	
	
	var a_get_logged_in_user_exsiting_revenue_cap = '';
	var a_project_search_results='';
	
	var a_project_cap_filter = [[
      							['custrecord_revenue_share_approval_status','anyof',3],
	                            'or',
	                            ['custrecord_sow_value_change_status','is','Saved']
	                            ],
								'and',
								['custrecord_is_mnth_end_effort_created','is','T'],
								'and',
								['custrecord_revenue_share_project.custentity_exclude_rev_forecast','is','F']
								,'and',
                                ['custrecord_revenue_share_project.enddate','onorafter','startofthismonth']	
                                ,'and',
								['custrecord_revenue_share_project.internalid','noneof',[116023,102559,153636]]
								,'and',
								['custrecord_revenue_share_project.internalid','anyof', i_pro_internal_id]
	]; 
			   
	
	var a_pro_list = [];
	var a_columns_existing_cap_srch = new Array();
	a_columns_existing_cap_srch[0] = new nlobjSearchColumn('custrecord_revenue_share_project');		
	a_columns_existing_cap_srch[1] = new nlobjSearchColumn('custrecord_revenue_share_total');
	a_columns_existing_cap_srch[2] = new nlobjSearchColumn('created').setSort(true);
	a_columns_existing_cap_srch[3] = new nlobjSearchColumn('custrecord_revenue_share_approval_status'); //custrecord_sow_value_change_status
	a_columns_existing_cap_srch[4] = new nlobjSearchColumn('custrecord_sow_value_change_status');
	a_columns_existing_cap_srch[5] = new nlobjSearchColumn('custrecord_proj_strt_date_revenue_cap'); //custrecord_sow_value_change_status
	a_columns_existing_cap_srch[6] = new nlobjSearchColumn('custrecord_proj_end_date_revenue_cap');
	a_get_logged_in_user_exsiting_revenue_cap = searchRecord('customrecord_revenue_share', null, a_project_cap_filter, a_columns_existing_cap_srch);
	
	if(a_get_logged_in_user_exsiting_revenue_cap)
		{
		for(var i=0;i< a_get_logged_in_user_exsiting_revenue_cap.length;i++)
			{
				var pid=a_get_logged_in_user_exsiting_revenue_cap[i].getValue('custrecord_revenue_share_project');
				//Added the logic for Saved Status projects - 16th JAN 2020
				
				var i_revenue_share_status = a_get_logged_in_user_exsiting_revenue_cap[i].getValue('custrecord_revenue_share_approval_status');
				var i_saved_status = a_get_logged_in_user_exsiting_revenue_cap[i].getValue('custrecord_sow_value_change_status');
				var pro_revenue_share= a_get_logged_in_user_exsiting_revenue_cap[i].getValue('custrecord_revenue_share_total');
				var setup_st_date= a_get_logged_in_user_exsiting_revenue_cap[i].getValue('custrecord_proj_strt_date_revenue_cap');
				var setup_end_date= a_get_logged_in_user_exsiting_revenue_cap[i].getValue('custrecord_proj_end_date_revenue_cap');
			//	var pid = 121682;
				if(pid && a_pro_list.indexOf(pid) < 0)
				{
					var a_project_filter = [['status', 'noneof', 1], 'and',
							['jobtype', 'anyof', 2], 'and',
							['jobbillingtype', 'anyof', 'FBM'], 'and',
							['custentity_fp_rev_rec_type', 'anyof', 1],'and',
							['internalid','anyof',pid]];
					var a_columns_proj_srch = new Array();
					a_columns_proj_srch[0] = new nlobjSearchColumn('custentity_projectvalue');
					a_columns_proj_srch[1] = new nlobjSearchColumn('customer');
					a_columns_proj_srch[2] = new nlobjSearchColumn('custentity_fp_rev_rec_type');
					a_columns_proj_srch[3] = new nlobjSearchColumn('custentity_ytd_rev_recognized');
					a_columns_proj_srch[4]= new nlobjSearchColumn('startdate');
					a_columns_proj_srch[5]= new nlobjSearchColumn('custentity_rev_rec_new_strt_date');
					a_columns_proj_srch[6]= new nlobjSearchColumn('enddate');
					a_columns_proj_srch[7]= new nlobjSearchColumn('custentity_project_currency');
					a_columns_proj_srch[8]=new nlobjSearchColumn('internalid');
					a_columns_proj_srch[9]=new nlobjSearchColumn('companyname');
					a_columns_proj_srch[10]=new nlobjSearchColumn('entityid');
					
					a_project_search_results = searchRecord('job', null, a_project_filter, a_columns_proj_srch);
					
					
					for(var pm=0;pm < a_project_search_results.length;pm++)
					{
						f_current_month_actual_revenue_total_proj_level = 0;
						a_recognized_revenue_total_proj_level = new Array();
						var excel_dat=[];
						var data='';
						var internalid = a_project_search_results[pm].getValue('internalid');
						var entityid = a_project_search_results[pm].getValue('entityid');
						var proj_name= a_project_search_results[pm].getValue('companyname');
						var d_proj_start_date=a_project_search_results[pm].getValue('startdate');
						var d_proj_strt_date_old_proj = a_project_search_results[pm].getValue('custentity_rev_rec_new_strt_date');
						var d_proj_end_date = a_project_search_results[pm].getValue('enddate');
						var s_proj_currency = a_project_search_results[pm].getText('custentity_project_currency');
						
						//Added on 16th Jan
						var i_project_sow_value = a_project_search_results[pm].getValue('custentity_projectvalue');
						var i_proj_revenue_rec_type = a_project_search_results[pm].getValue('custentity_fp_rev_rec_type');
						var i_project_value_ytd = a_project_search_results[pm].getValue('custentity_ytd_rev_recognized');
						if(!i_project_value_ytd)
							i_project_value_ytd = 0;
						i_project_sow_value = parseFloat(i_project_sow_value) - parseFloat(i_project_value_ytd);
						i_project_sow_value = parseFloat(i_project_sow_value).toFixed(2);
						
						//Added logic to verify Saved or Approved
						
						 if((i_saved_status == 'Saved') )
							{
                              if(parseFloat(pro_revenue_share) <= parseFloat(i_project_sow_value))
                              {
								d_proj_start_date = setup_st_date;
								d_proj_end_date = setup_end_date;
								}
							}
						
						if(d_proj_strt_date_old_proj && i_saved_status != 'Saved')
						{
							d_proj_start_date = d_proj_strt_date_old_proj;
						}
						
						var d_pro_strtDate = nlapiStringToDate(d_proj_start_date);
						var i_year_project = d_pro_strtDate.getFullYear();
						var s_pro_endDate = nlapiStringToDate(d_proj_end_date);
						var i_year_project_end = s_pro_endDate.getFullYear();
						var i_project_mnth = d_pro_strtDate.getMonth();
						var i_prject_end_mnth = s_pro_endDate.getMonth();
						var s_currency_symbol_proj = getCurrency_Symbol(s_proj_currency);
						var s_currency_symbol_usd = getCurrency_Symbol('USD');
						var i_master_currency = 1;
						
						var i_master_currency=_Currency_exchangeRate(s_proj_currency, o_PL_conversion_table);//Fecth the Currency conversion rate 
						i_master_currency=i_master_currency?i_master_currency : 1;
						
						//nlapiLogExecution('DEBUG','i_master_currency Lib ',i_master_currency);		
						data = getMarginMilestone(internalid,i_year_project,s_currency_symbol_proj,d_pro_strtDate,s_pro_endDate,i_project_mnth,s_currency_symbol_usd,i_prject_end_mnth,i_year_project_end);
						for(var dataln=0;dataln<data.length;dataln++)
						{
							if(_logValidation(data[dataln].actual_rev)){
								var f_amt =  removeCommas(data[dataln].actual_rev);
							}
							else{
								var f_amt = 0;
							}
							excel_dat.push({
										'project_id':entityid,
										'project_name':proj_name,
										'practice':data[dataln].practice.replace(/[\r\n]/g, ''),
										'sub_practice': data[dataln].sub_practice,
										'Month':data[dataln].month,
										'cumm_rev':(parseFloat(i_master_currency) * parseFloat(f_amt)).toFixed(2),
									});
								
							
						}
						excel_data_res.push(excel_dat);
						
					}
					a_pro_list.push(pid);
				}
			}
			return excel_data_res; 
			
		}
	
	
}
catch (err) {
	nlapiLogExecution('ERROR', 'data Fetch Error', err);
}
}
function removearrayduplicate(array) {
    var newArray = new Array();
    label: for (var i = 0; i < array.length; i++) {
          for (var j = 0; j < array.length; j++) {
                if (newArray[j] == array[i])
                      continue label;
          }
          newArray[newArray.length] = array[i];
    }
    return newArray;
}

function getMarginMilestone(i_projectId,i_year_project,s_currency_symbol_proj,d_proj_start_date,d_proj_end_date,i_project_mnth,s_currency_symbol_usd,i_prject_end_mnth,i_year_project_end)
{
	//nlapiLogExecution('debug','i_projectId getmargin ',i_projectId)
	var sr_no=0;
	var bill_data_arr = new Array();
	try
	{
		var a_revenue_cap_filters = [['custrecord_revenue_share_project', 'anyof', parseInt(i_projectId)]];
					
		var a_column = new Array();
		a_column[0] = new nlobjSearchColumn('created').setSort(true);
		a_column[1] = new nlobjSearchColumn('custrecord_auto_number_counter');
		a_column[2] = new nlobjSearchColumn('custrecord_revenue_share_approval_status');
		
		var a_get_logged_in_user_exsiting_revenue_cap = nlapiSearchRecord('customrecord_revenue_share', null, a_revenue_cap_filters, a_column);
		if (a_get_logged_in_user_exsiting_revenue_cap)
		{
			var i_revenue_share_id = a_get_logged_in_user_exsiting_revenue_cap[0].getId();
			var i_revenue_share_stat = a_get_logged_in_user_exsiting_revenue_cap[0].getValue('custrecord_revenue_share_approval_status');
			var a_effort_activity_mnth_end_filter = [['custrecord_revenue_share_parent_json', 'anyof', parseInt(i_revenue_share_id)]
													 ,"AND",['custrecord_is_mnth_effrt_confirmed', 'is', 'T']];
			var a_columns_mnth_end_effort_activity_srch = new Array();
			a_columns_mnth_end_effort_activity_srch[0] = new nlobjSearchColumn('created').setSort(true);
			
			var a_get_mnth_end_effrt_activity = nlapiSearchRecord('customrecord_fp_rev_rec_mnth_end_json', null, a_effort_activity_mnth_end_filter, a_columns_mnth_end_effort_activity_srch);
			if (a_get_mnth_end_effrt_activity)
			{
				var i_mnth_end_json_id = a_get_mnth_end_effrt_activity[0].getId();
				//nlapiLogExecution('DEBUG','JSON Intetnal id',i_mnth_end_json_id);
			}			
			
			var a_recipient_mail_id = new Array();
			var a_practice_involved = new Array();
			var a_revenue_cap_filter = [['custrecord_revenue_share_per_practice_ca.internalid', 'anyof', parseInt(i_revenue_share_id)]];
			var a_columns_existing_cap_srch = new Array();
			a_columns_existing_cap_srch[0] = new nlobjSearchColumn('custrecord_revenue_share_per_practice_pr');
			a_columns_existing_cap_srch[1] = new nlobjSearchColumn('custrecord_revenue_share_per_practice_su');
			var a_exsiting_revenue_cap = nlapiSearchRecord('customrecord_revenue_share_per_practice', null, a_revenue_cap_filter, a_columns_existing_cap_srch);
			if (a_exsiting_revenue_cap)
			{
				//nlapiLogExecution('DEBUG','a_exsiting_revenue_cap Lib ',a_exsiting_revenue_cap.length);
				for(var i_revenue_index = 0; i_revenue_index < a_exsiting_revenue_cap.length; i_revenue_index++)
				{
					var i_parent_parctice_id = a_exsiting_revenue_cap[i_revenue_index].getValue('custrecord_revenue_share_per_practice_pr');
					var i_sub_parctice_id = a_exsiting_revenue_cap[i_revenue_index].getValue('custrecord_revenue_share_per_practice_su');
				
					if(a_practice_involved.indexOf(i_parent_parctice_id) < 0)
					{
						a_practice_involved.push(i_parent_parctice_id);
					}
					if(a_practice_involved.indexOf(i_sub_parctice_id) < 0)
					{
						a_practice_involved.push(i_sub_parctice_id);
					}
				}
			}	
		
			var a_filters_search_practice = [['internalid','anyof',a_practice_involved]
							  ];						  
			var a_columns_practice = new Array();
			a_columns_practice[0] = new nlobjSearchColumn('email', 'custrecord_practicehead');
			var a_parctice_srch = nlapiSearchRecord('department', null, a_filters_search_practice, a_columns_practice);
			if (a_parctice_srch)
			{
				for(var i_practice_index = 0; i_practice_index < a_parctice_srch.length; i_practice_index++)
				{
				a_recipient_mail_id.push(a_parctice_srch[i_practice_index].getValue('email', 'custrecord_practicehead'));
				}
			}			
		}			
		var projectWiseRevenue = [];
		var a_revenue_recognized_for_project = new Array();
		var a_filter_get_ytd_revenue_recognized = [['custrecord_project_to_recognize_amount', 'anyof', i_projectId]
													];
							
		var a_columns_get_ytd_revenue_recognized = new Array();
		a_columns_get_ytd_revenue_recognized[0] = new nlobjSearchColumn('custrecord_project_to_recognize_amount');
		a_columns_get_ytd_revenue_recognized[1] = new nlobjSearchColumn('custrecord_practice_to_recognize_amount');
		a_columns_get_ytd_revenue_recognized[2] = new nlobjSearchColumn('custrecord_subparctice_to_recognize_amnt');
		a_columns_get_ytd_revenue_recognized[3] = new nlobjSearchColumn('custrecord_role_to_recognize_amount');
		a_columns_get_ytd_revenue_recognized[4] = new nlobjSearchColumn('custrecord_level_to_recognize_amount');
		a_columns_get_ytd_revenue_recognized[5] = new nlobjSearchColumn('custrecord_revenue_recognized');
		a_columns_get_ytd_revenue_recognized[6] = new nlobjSearchColumn('custrecord_month_to_recognize_amount');
		a_columns_get_ytd_revenue_recognized[7] = new nlobjSearchColumn('custrecord_year_to_recognize_amount');
		
		var a_get_ytd_revenue = nlapiSearchRecord('customrecord_fp_rev_rec_recognized_amnt', null, a_filter_get_ytd_revenue_recognized, a_columns_get_ytd_revenue_recognized);
		if (a_get_ytd_revenue)
		{
			//nlapiLogExecution('audit','recognized revenue for project found');
			for(var i_revenue_index=0; i_revenue_index<a_get_ytd_revenue.length; i_revenue_index++)
			{
				//nlapiLogExecution('audit','recognized amnt:- ',a_get_ytd_revenue[i_revenue_index].getValue('custrecord_month_to_recognize_amount'));
				a_revenue_recognized_for_project.push({
												'practice_revenue_recognized': a_get_ytd_revenue[i_revenue_index].getValue('custrecord_practice_to_recognize_amount'),
												'subpractice_revenue_recognized': a_get_ytd_revenue[i_revenue_index].getValue('custrecord_subparctice_to_recognize_amnt'),
												'role_revenue_recognized': a_get_ytd_revenue[i_revenue_index].getValue('custrecord_role_to_recognize_amount'),
												'level_revenue_recognized': a_get_ytd_revenue[i_revenue_index].getValue('custrecord_level_to_recognize_amount'),
												'amount_revenue_recognized': a_get_ytd_revenue[i_revenue_index].getValue('custrecord_revenue_recognized'),
												'month_revenue_recognized': a_get_ytd_revenue[i_revenue_index].getValue('custrecord_month_to_recognize_amount'),
												'year_revenue_recognized': a_get_ytd_revenue[i_revenue_index].getValue('custrecord_year_to_recognize_amount')
												});
			}
		}
		
		
		var d_today_date = new Date();
		var i_current_mnth = d_today_date.getMonth();
		var i_current_year = d_today_date.getFullYear();
		var i_prev_month = parseFloat(i_current_mnth) - parseFloat(1);
	
		if(i_mnth_end_json_id){
			var a_total_effort_json = [];
			
			var o_mnth_end_effrt = nlapiLoadRecord('customrecord_fp_rev_rec_mnth_end_json',i_mnth_end_json_id);
			
			var s_effrt_json = o_mnth_end_effrt.getFieldValue('custrecord_json_mnth_end_effrt');
			//s_effrt_json = JSON.stringify(s_effrt_json);
			var s_effrt_json_2 = o_mnth_end_effrt.getFieldValue('custrecord_json_mnth_end_effrt2');
			//s_effrt_json_2 = JSON.stringify(s_effrt_json_2);
			var s_effrt_json_3 = o_mnth_end_effrt.getFieldValue('custrecord_json_mnth_end_effrt3');
			var s_effrt_json_4 = o_mnth_end_effrt.getFieldValue('custrecord_json_mnth_end_effrt4');
			
			var a_duplicate_sub_prac_count = new Array();
			var a_unique_list_practice_sublist = new Array();
			
			var a_subprac_searched_once = new Array();
			var a_subprac_searched_once_month = new Array();
			var a_subprac_searched_once_year = new Array();
			
			if(s_effrt_json)
			{
				a_total_effort_json.push(s_effrt_json);
				
				var i_practice_previous = 0;
				var f_total_revenue_for_tenure = 0;
				//for (var i_mnth_end_plan = 0; i_mnth_end_plan < a_get_mnth_end_effrt_plan.length; i_mnth_end_plan++)
				var s_entire_json_clubed = JSON.parse(s_effrt_json);
				for (var i_mnth_end_plan = 0; i_mnth_end_plan < s_entire_json_clubed.length; i_mnth_end_plan++)
				{
					var a_row_json_data = JSON.parse(JSON.stringify(s_entire_json_clubed[i_mnth_end_plan]));
					for (var i_row_json_index = 0; i_row_json_index < a_row_json_data.length; i_row_json_index++)
					{
						var i_practice = a_row_json_data[i_row_json_index].prac;
						var s_practice = a_row_json_data[i_row_json_index].prac_text;
						var i_sub_practice = a_row_json_data[i_row_json_index].subprac;
						var s_sub_practice = a_row_json_data[i_row_json_index].subprac_text;
						var i_role = a_row_json_data[i_row_json_index].role;
						var s_role = a_row_json_data[i_row_json_index].role_text;
						var i_level = a_row_json_data[i_row_json_index].level;
						var s_level = a_row_json_data[i_row_json_index].level_text;
						var i_location = a_row_json_data[i_row_json_index].loc;
						var s_location = a_row_json_data[i_row_json_index].loc_text;
						var f_revenue = a_row_json_data[i_row_json_index].cost;
						if (!f_revenue) 
							f_revenue = 0;
						
						var f_revenue_share = a_row_json_data[i_row_json_index].share;
						if (!f_revenue_share) 
							f_revenue_share = 0;
							
						var i_no_of_resources = a_row_json_data[i_row_json_index].allo;
						var s_mnth_strt_date = '1/31/2017';
						var s_mnth_end_date = '31/31/2017';
						var s_mnth = a_row_json_data[i_row_json_index].mnth;
						var s_year = a_row_json_data[i_row_json_index].year;
						
						var f_revenue_recognized = 0;
						
						if(a_unique_list_practice_sublist.indexOf(a_row_json_data[i_row_json_index].subprac)<0)
						{
							a_unique_list_practice_sublist.push(a_row_json_data[i_row_json_index].subprac);
							a_duplicate_sub_prac_count.push({
														'sub_prac': a_row_json_data[i_row_json_index].subprac,
														'sub_prac_name': a_row_json_data[i_row_json_index].subprac_text,
														'revenue_share': 50000,
														'no_count': 0
														});
														
						}
						
						f_revenue_recognized = find_recognized_revenue(a_revenue_recognized_for_project,i_practice,i_sub_practice,i_role,i_level,s_mnth,s_year,a_subprac_searched_once,a_subprac_searched_once_month,a_subprac_searched_once_year);
						
						if (!f_revenue_recognized) 
							f_revenue_recognized = 0;
							
					
						
						var s_month_name = s_mnth + '_' + s_year;
						
						var total_revenue = parseFloat(i_no_of_resources) * parseFloat(f_revenue);
						total_revenue = parseFloat(total_revenue).toFixed(2);
						
						
						
						if (i_practice_previous == 0) {
							i_practice_previous = a_row_json_data[i_row_json_index].prac_row;
						}
						
						if (i_practice_previous != a_row_json_data[i_row_json_index].prac_row) {
							f_total_revenue_for_tenure = 0;
							i_practice_previous = a_row_json_data[i_row_json_index].prac_row;
						}
					
						f_total_revenue_for_tenure = parseFloat(f_total_revenue_for_tenure) + parseFloat(total_revenue);
						f_total_revenue_for_tenure = parseFloat(f_total_revenue_for_tenure).toFixed(2);
						
						var i_prac_subPrac_role_level = i_practice + '_' + i_sub_practice + '_' + i_role + '_' + i_level + '_' + i_location;
						
						if (!projectWiseRevenue[i_prac_subPrac_role_level]) {
							projectWiseRevenue[i_prac_subPrac_role_level] = {
								practice_name: s_practice,
								sub_prac_name: s_sub_practice,
								sub_practice: i_sub_practice,
								role_name: s_role,
								level_name: s_level,
								location_name: s_location,
								total_revenue_for_tenure: f_total_revenue_for_tenure,
								revenue_share: f_revenue_share,
								RevenueData: {}
							};
							
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice] = new yearData(i_year_project);
							
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].Amount = total_revenue;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].prcnt_allocated = i_no_of_resources;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].recognized_revenue = f_revenue_recognized;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].mnth_strt = s_mnth_strt_date;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].mnth_end = s_mnth_end_date;
						}
						else {
							projectWiseRevenue[i_prac_subPrac_role_level].total_revenue_for_tenure = f_total_revenue_for_tenure;
							
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].Amount = total_revenue;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].prcnt_allocated = i_no_of_resources;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].recognized_revenue = f_revenue_recognized;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].mnth_strt = s_mnth_strt_date;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].mnth_end = s_mnth_end_date;
						}
					}
				}
			}
			
			if(s_effrt_json_2)
			{
				var i_practice_previous = 0;
				var f_total_revenue_for_tenure = 0;
				//for (var i_mnth_end_plan = 0; i_mnth_end_plan < a_get_mnth_end_effrt_plan.length; i_mnth_end_plan++)
				var s_entire_json_clubed = JSON.parse(s_effrt_json_2);
				for (var i_mnth_end_plan = 0; i_mnth_end_plan < s_entire_json_clubed.length; i_mnth_end_plan++)
				{
					var a_row_json_data = JSON.parse(JSON.stringify(s_entire_json_clubed[i_mnth_end_plan]));
					for (var i_row_json_index = 0; i_row_json_index < a_row_json_data.length; i_row_json_index++)
					{
						var i_practice = a_row_json_data[i_row_json_index].prac;
						var s_practice = a_row_json_data[i_row_json_index].prac_text;
						var i_sub_practice = a_row_json_data[i_row_json_index].subprac;
						var s_sub_practice = a_row_json_data[i_row_json_index].subprac_text;
						var i_role = a_row_json_data[i_row_json_index].role;
						var s_role = a_row_json_data[i_row_json_index].role_text;
						var i_level = a_row_json_data[i_row_json_index].level;
						var s_level = a_row_json_data[i_row_json_index].level_text;
						var i_location = a_row_json_data[i_row_json_index].loc;
						var s_location = a_row_json_data[i_row_json_index].loc_text;
						var f_revenue = a_row_json_data[i_row_json_index].cost;
						if (!f_revenue) 
							f_revenue = 0;
						
						var f_revenue_share = a_row_json_data[i_row_json_index].share;
						if (!f_revenue_share) 
							f_revenue_share = 0;
							
						var i_no_of_resources = a_row_json_data[i_row_json_index].allo;
						var s_mnth_strt_date = '1/31/2017';
						var s_mnth_end_date = '31/31/2017';
						var s_mnth = a_row_json_data[i_row_json_index].mnth;
						var s_year = a_row_json_data[i_row_json_index].year;
						
						var f_revenue_recognized = 0;
						
						if(a_unique_list_practice_sublist.indexOf(a_row_json_data[i_row_json_index].subprac)<0)
						{
							a_unique_list_practice_sublist.push(a_row_json_data[i_row_json_index].subprac);
							a_duplicate_sub_prac_count.push({
														'sub_prac': a_row_json_data[i_row_json_index].subprac,
														'sub_prac_name': a_row_json_data[i_row_json_index].subprac_text,
														'revenue_share': 50000,
														'no_count': 0
														});
														
						}
						
						f_revenue_recognized = find_recognized_revenue(a_revenue_recognized_for_project,i_practice,i_sub_practice,i_role,i_level,s_mnth,s_year,a_subprac_searched_once,a_subprac_searched_once_month,a_subprac_searched_once_year);
						
						if (!f_revenue_recognized) 
							f_revenue_recognized = 0;
						
						
						
						var s_month_name = s_mnth + '_' + s_year;
						
						var total_revenue = parseFloat(i_no_of_resources) * parseFloat(f_revenue);
						total_revenue = parseFloat(total_revenue).toFixed(2);
						
						
						
						if (i_practice_previous == 0) {
							i_practice_previous = a_row_json_data[i_row_json_index].prac_row;
						}
						
						if (i_practice_previous != a_row_json_data[i_row_json_index].prac_row) {
							f_total_revenue_for_tenure = 0;
							i_practice_previous = a_row_json_data[i_row_json_index].prac_row;
						}
					
						f_total_revenue_for_tenure = parseFloat(f_total_revenue_for_tenure) + parseFloat(total_revenue);
						f_total_revenue_for_tenure = parseFloat(f_total_revenue_for_tenure).toFixed(2);
						
						var i_prac_subPrac_role_level = i_practice + '_' + i_sub_practice + '_' + i_role + '_' + i_level + '_' + i_location;
						
						if (!projectWiseRevenue[i_prac_subPrac_role_level]) {
							projectWiseRevenue[i_prac_subPrac_role_level] = {
								practice_name: s_practice,
								sub_prac_name: s_sub_practice,
								sub_practice: i_sub_practice,
								role_name: s_role,
								level_name: s_level,
								location_name: s_location,
								total_revenue_for_tenure: f_total_revenue_for_tenure,
								revenue_share: f_revenue_share,
								RevenueData: {}
							};
							
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice] = new yearData(i_year_project);
							
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].Amount = total_revenue;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].prcnt_allocated = i_no_of_resources;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].recognized_revenue = f_revenue_recognized;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].mnth_strt = s_mnth_strt_date;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].mnth_end = s_mnth_end_date;
						}
						else {
							projectWiseRevenue[i_prac_subPrac_role_level].total_revenue_for_tenure = f_total_revenue_for_tenure;
							
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].Amount = total_revenue;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].prcnt_allocated = i_no_of_resources;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].recognized_revenue = f_revenue_recognized;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].mnth_strt = s_mnth_strt_date;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].mnth_end = s_mnth_end_date;
							
						}
							
					}
				}
				
			}
			
			if(s_effrt_json_3)
			{
				var i_practice_previous = 0;
				var f_total_revenue_for_tenure = 0;
				//for (var i_mnth_end_plan = 0; i_mnth_end_plan < a_get_mnth_end_effrt_plan.length; i_mnth_end_plan++)
				var s_entire_json_clubed = JSON.parse(s_effrt_json_3);
				for (var i_mnth_end_plan = 0; i_mnth_end_plan < s_entire_json_clubed.length; i_mnth_end_plan++)
				{
					var a_row_json_data = JSON.parse(JSON.stringify(s_entire_json_clubed[i_mnth_end_plan]));
					for (var i_row_json_index = 0; i_row_json_index < a_row_json_data.length; i_row_json_index++)
					{
						var i_practice = a_row_json_data[i_row_json_index].prac;
						var s_practice = a_row_json_data[i_row_json_index].prac_text;
						var i_sub_practice = a_row_json_data[i_row_json_index].subprac;
						var s_sub_practice = a_row_json_data[i_row_json_index].subprac_text;
						var i_role = a_row_json_data[i_row_json_index].role;
						var s_role = a_row_json_data[i_row_json_index].role_text;
						var i_level = a_row_json_data[i_row_json_index].level;
						var s_level = a_row_json_data[i_row_json_index].level_text;
						var i_location = a_row_json_data[i_row_json_index].loc;
						var s_location = a_row_json_data[i_row_json_index].loc_text;
						var f_revenue = a_row_json_data[i_row_json_index].cost;
						if (!f_revenue) 
							f_revenue = 0;
						
						var f_revenue_share = a_row_json_data[i_row_json_index].share;
						if (!f_revenue_share) 
							f_revenue_share = 0;
							
						var i_no_of_resources = a_row_json_data[i_row_json_index].allo;
						var s_mnth_strt_date = '1/31/2017';
						var s_mnth_end_date = '31/31/2017';
						var s_mnth = a_row_json_data[i_row_json_index].mnth;
						var s_year = a_row_json_data[i_row_json_index].year;
						
						var f_revenue_recognized = 0;
						
						if(a_unique_list_practice_sublist.indexOf(a_row_json_data[i_row_json_index].subprac)<0)
						{
							a_unique_list_practice_sublist.push(a_row_json_data[i_row_json_index].subprac);
							a_duplicate_sub_prac_count.push({
														'sub_prac': a_row_json_data[i_row_json_index].subprac,
														'sub_prac_name': a_row_json_data[i_row_json_index].subprac_text,
														'revenue_share': 50000,
														'no_count': 0
														});
											
						}
						
						f_revenue_recognized = find_recognized_revenue(a_revenue_recognized_for_project,i_practice,i_sub_practice,i_role,i_level,s_mnth,s_year,a_subprac_searched_once,a_subprac_searched_once_month,a_subprac_searched_once_year);
						
						if (!f_revenue_recognized) 
							f_revenue_recognized = 0;
						
						
						
						var s_month_name = s_mnth + '_' + s_year;
						
						var total_revenue = parseFloat(i_no_of_resources) * parseFloat(f_revenue);
						total_revenue = parseFloat(total_revenue).toFixed(2);
						
						
						
						if (i_practice_previous == 0) {
							i_practice_previous = a_row_json_data[i_row_json_index].prac_row;
						}
						
						if (i_practice_previous != a_row_json_data[i_row_json_index].prac_row) {
							f_total_revenue_for_tenure = 0;
							i_practice_previous = a_row_json_data[i_row_json_index].prac_row;
						}
					
						f_total_revenue_for_tenure = parseFloat(f_total_revenue_for_tenure) + parseFloat(total_revenue);
						f_total_revenue_for_tenure = parseFloat(f_total_revenue_for_tenure).toFixed(2);
						
						var i_prac_subPrac_role_level = i_practice + '_' + i_sub_practice + '_' + i_role + '_' + i_level + '_' + i_location;
						
						if (!projectWiseRevenue[i_prac_subPrac_role_level]) {
							projectWiseRevenue[i_prac_subPrac_role_level] = {
								practice_name: s_practice,
								sub_prac_name: s_sub_practice,
								sub_practice: i_sub_practice,
								role_name: s_role,
								level_name: s_level,
								location_name: s_location,
								total_revenue_for_tenure: f_total_revenue_for_tenure,
								revenue_share: f_revenue_share,
								RevenueData: {}
							};
							
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice] = new yearData(i_year_project);
							
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].Amount = total_revenue;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].prcnt_allocated = i_no_of_resources;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].recognized_revenue = f_revenue_recognized;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].mnth_strt = s_mnth_strt_date;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].mnth_end = s_mnth_end_date;
						}
						else {
							projectWiseRevenue[i_prac_subPrac_role_level].total_revenue_for_tenure = f_total_revenue_for_tenure;
							
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].Amount = total_revenue;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].prcnt_allocated = i_no_of_resources;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].recognized_revenue = f_revenue_recognized;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].mnth_strt = s_mnth_strt_date;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].mnth_end = s_mnth_end_date;
						}
					}
				}
			}
			
			if(s_effrt_json_4)
			{
				var i_practice_previous = 0;
				var f_total_revenue_for_tenure = 0;
				//for (var i_mnth_end_plan = 0; i_mnth_end_plan < a_get_mnth_end_effrt_plan.length; i_mnth_end_plan++)
				var s_entire_json_clubed = JSON.parse(s_effrt_json_4);
				for (var i_mnth_end_plan = 0; i_mnth_end_plan < s_entire_json_clubed.length; i_mnth_end_plan++)
				{
					var a_row_json_data = JSON.parse(JSON.stringify(s_entire_json_clubed[i_mnth_end_plan]));
					for (var i_row_json_index = 0; i_row_json_index < a_row_json_data.length; i_row_json_index++)
					{
						var i_practice = a_row_json_data[i_row_json_index].prac;
						var s_practice = a_row_json_data[i_row_json_index].prac_text;
						var i_sub_practice = a_row_json_data[i_row_json_index].subprac;
						var s_sub_practice = a_row_json_data[i_row_json_index].subprac_text;
						var i_role = a_row_json_data[i_row_json_index].role;
						var s_role = a_row_json_data[i_row_json_index].role_text;
						var i_level = a_row_json_data[i_row_json_index].level;
						var s_level = a_row_json_data[i_row_json_index].level_text;
						var i_location = a_row_json_data[i_row_json_index].loc;
						var s_location = a_row_json_data[i_row_json_index].loc_text;
						var f_revenue = a_row_json_data[i_row_json_index].cost;
						if (!f_revenue) 
							f_revenue = 0;
						
						var f_revenue_share = a_row_json_data[i_row_json_index].share;
						if (!f_revenue_share) 
							f_revenue_share = 0;
							
						var i_no_of_resources = a_row_json_data[i_row_json_index].allo;
						var s_mnth_strt_date = '1/31/2017';
						var s_mnth_end_date = '31/31/2017';
						var s_mnth = a_row_json_data[i_row_json_index].mnth;
						var s_year = a_row_json_data[i_row_json_index].year;
						
						var f_revenue_recognized = 0;
						
						if(a_unique_list_practice_sublist.indexOf(a_row_json_data[i_row_json_index].subprac)<0)
						{
							a_unique_list_practice_sublist.push(a_row_json_data[i_row_json_index].subprac);
							a_duplicate_sub_prac_count.push({
														'sub_prac': a_row_json_data[i_row_json_index].subprac,
														'sub_prac_name': a_row_json_data[i_row_json_index].subprac_text,
														'revenue_share': 50000,
														'no_count': 0
														});
									
						}
						
						f_revenue_recognized = find_recognized_revenue(a_revenue_recognized_for_project,i_practice,i_sub_practice,i_role,i_level,s_mnth,s_year,a_subprac_searched_once,a_subprac_searched_once_month,a_subprac_searched_once_year);
						
						if (!f_revenue_recognized) 
							f_revenue_recognized = 0;
						
						var s_month_name = s_mnth + '_' + s_year;
						
						var total_revenue = parseFloat(i_no_of_resources) * parseFloat(f_revenue);
						total_revenue = parseFloat(total_revenue).toFixed(2);
												
						if (i_practice_previous == 0) {
							i_practice_previous = a_row_json_data[i_row_json_index].prac_row;
						}
						
						if (i_practice_previous != a_row_json_data[i_row_json_index].prac_row) {
							f_total_revenue_for_tenure = 0;
							i_practice_previous = a_row_json_data[i_row_json_index].prac_row;
						}
					
						f_total_revenue_for_tenure = parseFloat(f_total_revenue_for_tenure) + parseFloat(total_revenue);
						f_total_revenue_for_tenure = parseFloat(f_total_revenue_for_tenure).toFixed(2);
						
						var i_prac_subPrac_role_level = i_practice + '_' + i_sub_practice + '_' + i_role + '_' + i_level + '_' + i_location;
						
						if (!projectWiseRevenue[i_prac_subPrac_role_level]) {
							projectWiseRevenue[i_prac_subPrac_role_level] = {
								practice_name: s_practice,
								sub_prac_name: s_sub_practice,
								sub_practice: i_sub_practice,
								role_name: s_role,
								level_name: s_level,
								location_name: s_location,
								total_revenue_for_tenure: f_total_revenue_for_tenure,
								revenue_share: f_revenue_share,
								RevenueData: {}
							};
							
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice] = new yearData(i_year_project);
							
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].Amount = total_revenue;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].prcnt_allocated = i_no_of_resources;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].recognized_revenue = f_revenue_recognized;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].mnth_strt = s_mnth_strt_date;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].mnth_end = s_mnth_end_date;
						}
						else {
							projectWiseRevenue[i_prac_subPrac_role_level].total_revenue_for_tenure = f_total_revenue_for_tenure;
							
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].Amount = total_revenue;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].prcnt_allocated = i_no_of_resources;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].recognized_revenue = f_revenue_recognized;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].mnth_strt = s_mnth_strt_date;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].mnth_end = s_mnth_end_date;
						}
					}
				}
			}
			var monthBreakUp = getMonthsBreakup(d_proj_start_date,d_proj_end_date);
			var excel_cost_data=[];		
			// Revenue share table
			//var h_tableHtml_revenue_share = generate_table_effort_view(projectWiseRevenue,monthBreakUp,i_year_project,i_project_mnth,5,s_currency_symbol_proj,s_currency_symbol_usd,i_year_project_end);					
			// Effort View table
			for (var i_dupli = 0; i_dupli < a_duplicate_sub_prac_count.length; i_dupli++)
			{
				//var f_prev_mnth_cost = generate_cost_view_prev_mnth(projectWiseRevenue_previous_effrt,monthBreakUp,i_year_project,i_project_mnth,2,a_duplicate_sub_prac_count[i_dupli].sub_prac,s_currency_symbol_proj,s_currency_symbol_usd,i_year_project_end,i_prject_end_mnth);
				//nlapiLogExecution('audit','cost for prev mnth:- '+f_prev_mnth_cost,'sub prac:- '+a_duplicate_sub_prac_count[i_dupli].sub_prac);
				var h_tableHtml_effort_view = generate_cost_view_milestone(projectWiseRevenue,monthBreakUp,i_year_project,i_project_mnth,2,a_duplicate_sub_prac_count[i_dupli].sub_prac,s_currency_symbol_proj,s_currency_symbol_usd,i_year_project_end,i_prject_end_mnth);
				excel_cost_data.push(h_tableHtml_effort_view);
			}
			// Total revenue table testing for now
		//	var h_tableHtml_total_view = generate_total_project_view(projectWiseRevenue,monthBreakUp,i_year_project,i_project_mnth,4,s_currency_symbol_proj,s_currency_symbol_usd,i_year_project_end,i_prject_end_mnth);
			//if(_logValidation(h_tableHtml_effort_view)&& )
			
			for(var excel_len=0;excel_len< excel_cost_data.length;excel_len++)
				{
						var rev_len=excel_cost_data[excel_len].month;
						var temp = excel_cost_data[excel_len];
						for(var dx=0;dx<temp.length;dx++){
						var temp_obj = temp[dx];
					//	for(var reven=0;reven<h_tableHtml_effort_view.length;reven++)
						if(excel_len!=excel_cost_data.length-1)
						{
							bill_data_arr[sr_no] = {
                                    'practice': temp_obj.practice,
                                    'sub_practice': temp_obj.subpractice,
									'month' :temp_obj.month,
									'actual_rev':temp_obj.revenue,
									'cumm_rev' :temp_obj.cummulative
								};
						}
						else
						{
								bill_data_arr[sr_no] = {
                                    'practice':temp_obj.practice,
                                    'sub_practice': temp_obj.subpractice,
									'month' :temp_obj.month,
									'actual_rev':temp_obj.revenue,
									'cumm_rev' :temp_obj.cummulative
								   };
						}
							sr_no++;
							//nlapiLogExecution('debug','Data',bill_data_arr);
						}
				}
			
		}
		
		nlapiLogExecution('audit','remaining usage:- ',nlapiGetContext().getRemainingUsage());
	}
	catch(err)
	{
        nlapiLogExecution('ERROR','suitelet_month_end_confirmation','ERROR MESSAGE:- '+err);
        throw err;
	}
	return bill_data_arr;
}

function generate_cost_view_prev_mnth(projectWiseRevenue_previous_effrt,monthBreakUp,i_year_project,i_project_mnth,mode,sub_prac,s_currency_symbol_proj,s_currency_symbol_usd,i_year_project_end,i_prject_end_mnth)
{
	
	
	var today = new Date();
	var currentMonthName = getMonthName(nlapiDateToString(today));
	
	
	var a_sub_prac_already_displayed = new Array();
	var i_subpractice_id = 0;
	var a_amount = new Array();
	var a_recognized_revenue = new Array();
	var i_frst_row = 0;
	var i_diplay_frst_column = 0;
	var i_total_row_revenue = 0;
	var i_total_per_row = 0;
	var total_prcnt_aloocated = new Array();
	var b_first_line_copied = 0;
	var f_total_allocated_row = 0;
	
	for (var emp_internal_id in projectWiseRevenue_previous_effrt)
	{
		if (projectWiseRevenue_previous_effrt[emp_internal_id].sub_practice == sub_prac)
		{
			f_total_allocated_row = 0;
			for (var project_internal_id in projectWiseRevenue_previous_effrt[emp_internal_id].RevenueData)
			{
				var i_index_mnth = 0;
				for (var month in projectWiseRevenue_previous_effrt[emp_internal_id].RevenueData[project_internal_id])
				{
					var a_MonthNames = MonthNames(i_year_project);
					var currentMonthPos = a_MonthNames.indexOf(month);
					var s_current_month = a_MonthNames[currentMonthPos];
					var s_current_mnth_yr = s_current_month.substr(4, 8);
					
					var i_nxt_yr_flg = 0;
					if(currentMonthPos >=12)
					{
						currentMonthPos = currentMonthPos - 12;
						i_nxt_yr_flg = 1;
					}
					
					var i_curr_yr_dis_flag = 0;
					if(parseInt(s_current_mnth_yr) < i_year_project_end && i_prject_end_mnth < currentMonthPos)
					{
						i_curr_yr_dis_flag = 1;
					}
					
					if (((i_project_mnth <= currentMonthPos && ((i_prject_end_mnth >= currentMonthPos || i_curr_yr_dis_flag == 1) && i_nxt_yr_flg == 0) && parseInt(s_current_mnth_yr) <= i_year_project) && parseInt(s_current_mnth_yr) <= i_year_project_end) || (i_prject_end_mnth >= currentMonthPos && parseInt(s_current_mnth_yr) <= i_year_project_end && i_nxt_yr_flg == 1))
					{
						//nlapiLogExecution('audit','currentMonthPos:- ',+currentMonthPos);
						if(b_first_line_copied == 1)
						{
							var f_allocated_prcnt = total_prcnt_aloocated[i_index_mnth];
							f_allocated_prcnt = parseFloat(f_allocated_prcnt) + parseFloat(projectWiseRevenue_previous_effrt[emp_internal_id].RevenueData[project_internal_id][month].prcnt_allocated);
							total_prcnt_aloocated[i_index_mnth] = f_allocated_prcnt;
							i_index_mnth++;							
						}
						else
						{
							total_prcnt_aloocated.push(projectWiseRevenue_previous_effrt[emp_internal_id].RevenueData[project_internal_id][month].prcnt_allocated);
						}
						
						f_total_allocated_row = parseFloat(projectWiseRevenue_previous_effrt[emp_internal_id].RevenueData[project_internal_id][month].prcnt_allocated) + parseFloat(f_total_allocated_row);
						
					}
				}
			}
			
			b_first_line_copied = 1;
			
			var s_sub_prac_name = projectWiseRevenue_previous_effrt[emp_internal_id].sub_prac_name;
			var s_practice_name = projectWiseRevenue_previous_effrt[emp_internal_id].practice_name;
			
			i_diplay_frst_column = 1;
		}
	}
	i_diplay_frst_column = 0;
	
	
	
	var f_total_row_allocation = 0;
	for(var i_index_total_allocated=0; i_index_total_allocated<total_prcnt_aloocated.length; i_index_total_allocated++)
	{
		f_total_row_allocation = parseFloat(f_total_row_allocation) + parseFloat(total_prcnt_aloocated[i_index_total_allocated]);
	}
	
	for ( var emp_internal_id in projectWiseRevenue_previous_effrt) {
		
		for ( var project_internal_id in projectWiseRevenue_previous_effrt[emp_internal_id].RevenueData) {
			
			if(projectWiseRevenue_previous_effrt[emp_internal_id].sub_practice == sub_prac)
			{
				
				var i_sub_practice_internal_id = 0;
				var i_amt = 0;
				for (var month in projectWiseRevenue_previous_effrt[emp_internal_id].RevenueData[project_internal_id])
				{
					var a_MonthNames = MonthNames(i_year_project);
					var currentMonthPos = a_MonthNames.indexOf(month);
					var s_current_month = a_MonthNames[currentMonthPos];
					var s_current_mnth_yr = s_current_month.substr(4, 8);
					
					var i_nxt_yr_flg = 0;
					if(currentMonthPos >=12)
					{
						currentMonthPos = currentMonthPos - 12;
						i_nxt_yr_flg = 1;
					}
					
					var i_curr_yr_dis_flag = 0;
					if(parseInt(s_current_mnth_yr) < i_year_project_end && i_prject_end_mnth < currentMonthPos)
					{
						i_curr_yr_dis_flag = 1;
					}
					
					if (((i_project_mnth <= currentMonthPos && ((i_prject_end_mnth >= currentMonthPos || i_curr_yr_dis_flag == 1) && i_nxt_yr_flg == 0) && parseInt(s_current_mnth_yr) <= i_year_project) && parseInt(s_current_mnth_yr) <= i_year_project_end) || (i_prject_end_mnth >= currentMonthPos && parseInt(s_current_mnth_yr) <= i_year_project_end && i_nxt_yr_flg == 1))
					{
						if (mode == 2)
						{
							
							
							var total_revenue_format = parseFloat(projectWiseRevenue_previous_effrt[emp_internal_id].RevenueData[project_internal_id][month].Amount).toFixed(1);
							
							
							if(i_frst_row == 0)
							{
								//nlapiLogExecution('audit','inside push amnt');
								a_amount.push(total_revenue_format);
								a_recognized_revenue.push(projectWiseRevenue_previous_effrt[emp_internal_id].RevenueData[project_internal_id][month].recognized_revenue);
							}
							else
							{
								var amnt_mnth = a_amount[i_amt];
								amnt_mnth = parseFloat(amnt_mnth) + parseFloat(projectWiseRevenue_previous_effrt[emp_internal_id].RevenueData[project_internal_id][month].Amount);
								
								a_amount[i_amt] = amnt_mnth;
								
								var i_existing_recognised_amount = a_recognized_revenue[i_amt];
								i_existing_recognised_amount = parseFloat(i_existing_recognised_amount) + parseFloat(projectWiseRevenue_previous_effrt[emp_internal_id].RevenueData[project_internal_id][month].recognized_revenue);
								
								a_recognized_revenue[i_amt] = i_existing_recognised_amount;
								
								i_amt++;
							}
							i_total_row_revenue = parseFloat(i_total_row_revenue) + parseFloat(projectWiseRevenue_previous_effrt[emp_internal_id].RevenueData[project_internal_id][month].Amount);
							i_total_per_row = parseFloat(i_total_per_row) + parseFloat(projectWiseRevenue_previous_effrt[emp_internal_id].RevenueData[project_internal_id][month].Amount);
						}
						
						
					}
				}
				
				if (mode == 2)
				{
					i_total_per_row = 0;
					//i_total_row_revenue = parseFloat(i_total_row_revenue) + parseFloat(projectWiseRevenue[emp_internal_id].total_revenue_for_tenure);
				}
				
				i_frst_row = 1;
	
				i_diplay_frst_column = 1;
				
				var f_revenue_share = projectWiseRevenue_previous_effrt[emp_internal_id].revenue_share;
			}			
		}
	}
	
	var s_sub_prac_name = '';
	for (var emp_internal_id in projectWiseRevenue_previous_effrt)
	{
		if (projectWiseRevenue_previous_effrt[emp_internal_id].sub_practice == sub_prac)
		{
			s_sub_prac_name = projectWiseRevenue_previous_effrt[emp_internal_id].sub_prac_name;
			s_practice_name = projectWiseRevenue_previous_effrt[emp_internal_id].practice_name;
			//projectWiseRevenue[emp_internal_id].revenue_share_revised = i_total_row_revenue;
			//projectWiseRevenue[emp_internal_id].total_revenue_for_tenure = i_total_row_revenue;
		}
	}
	
	
	for (var i_amt = 0; i_amt < a_amount.length; i_amt++)
	{
		
	}
	
	
	// Percent row
	
	var f_total_prcnt = 0;
	for (var i_amt = 0; i_amt < a_amount.length; i_amt++)
	{
		var f_prcnt_revenue = parseFloat(a_amount[i_amt]) / parseFloat(i_total_row_revenue);
		f_prcnt_revenue = f_prcnt_revenue * 100;
		if(!f_prcnt_revenue)
			f_prcnt_revenue = 0;
			
		f_total_prcnt = parseFloat(f_prcnt_revenue) + parseFloat(f_total_prcnt);
		
	}
	
	//Total reveue recognized per month row
	
	var d_today_date = new Date();
	var i_current_month = d_today_date.getMonth();
	var i_current_year = d_today_date.getFullYear();
	
	var f_total_revenue = 0;
	var f_revenue_amount_till_current_month = 0;
	for (var i_amt = 0; i_amt < a_amount.length; i_amt++)
	{
		var f_prcnt_revenue = parseFloat(a_amount[i_amt]) / parseFloat(i_total_row_revenue);
		f_prcnt_revenue = f_prcnt_revenue * 100;
		
		var f_revenue_amount = (parseFloat(f_prcnt_revenue) * parseFloat(f_revenue_share)) / 100;
		if(!f_revenue_amount)
			f_revenue_amount = 0;
		
		if(i_amt <= i_current_month)
		{
			//nlapiLogExecution('audit','i_current_month:- '+i_current_month,'a_amount.length:- '+a_amount.length);
			f_revenue_amount_till_current_month = parseFloat(f_revenue_amount) + parseFloat(f_revenue_amount_till_current_month);
			if(!f_revenue_amount_till_current_month)
				f_revenue_amount_till_current_month = 0;
		}
		
		f_total_revenue = parseFloat(f_revenue_amount) + parseFloat(f_total_revenue);
		
	}
	
	for (var emp_internal_id in projectWiseRevenue_previous_effrt)
	{
		for (var project_internal_id in projectWiseRevenue_previous_effrt[emp_internal_id].RevenueData)
		{
			var i_amount_map = 0;
			if (projectWiseRevenue_previous_effrt[emp_internal_id].sub_practice == sub_prac)
			{
				for (var month in projectWiseRevenue_previous_effrt[emp_internal_id].RevenueData[project_internal_id])
				{
					var a_MonthNames = MonthNames(i_year_project);
					var currentMonthPos = a_MonthNames.indexOf(month);
					var s_current_month = a_MonthNames[currentMonthPos];
					var s_current_mnth_yr = s_current_month.substr(4, 8);
					
					var i_nxt_yr_flg = 0;
					if(currentMonthPos >=12)
					{
						currentMonthPos = currentMonthPos - 12;
						i_nxt_yr_flg = 1;
					}
					
					var i_curr_yr_dis_flag = 0;
					if(parseInt(s_current_mnth_yr) < i_year_project_end && i_prject_end_mnth < currentMonthPos)
					{
						i_curr_yr_dis_flag = 1;
					}
					
					if (((i_project_mnth <= currentMonthPos && ((i_prject_end_mnth >= currentMonthPos || i_curr_yr_dis_flag == 1) && i_nxt_yr_flg == 0) && parseInt(s_current_mnth_yr) <= i_year_project) && parseInt(s_current_mnth_yr) <= i_year_project_end) || (i_prject_end_mnth >= currentMonthPos && parseInt(s_current_mnth_yr) <= i_year_project_end && i_nxt_yr_flg == 1))
					{
						var f_prcnt_revenue = parseFloat(a_amount[i_amount_map]) / parseFloat(i_total_row_revenue);
						f_prcnt_revenue = f_prcnt_revenue * 100;
						
						var f_revenue_amount = (parseFloat(f_prcnt_revenue) * parseFloat(f_revenue_share)) / 100;
		
						projectWiseRevenue_previous_effrt[emp_internal_id].RevenueData[project_internal_id][month].total_revenue = f_revenue_amount;
						projectWiseRevenue_previous_effrt[emp_internal_id].RevenueData[project_internal_id][month].prcent_complt = f_prcnt_revenue;
						i_amount_map++;
					}
				}	
			}
		}
	}
	
	var today = new Date();
	var currentMonthName = getMonthName(nlapiDateToString(today));
	
	var i_total_revenue_recognized_ytd = 0;
	for(var i_revenue_index=0; i_revenue_index<a_recognized_revenue.length; i_revenue_index++)
	{
		i_total_revenue_recognized_ytd = parseFloat(i_total_revenue_recognized_ytd) + parseFloat(a_recognized_revenue[i_revenue_index]);
		
	}
	
	//Actual revenue to be recognized 
	
	if(parseInt(i_year_project) != parseInt(i_current_year))
	{
		var i_total_project_tenure = 11 + parseInt(i_current_month);
		var f_total_prev_mnths = parseFloat(i_total_project_tenure) - parseFloat(i_project_mnth);
		i_current_month = parseFloat(f_total_prev_mnths) + parseFloat(1);
	}
	else
	{
		i_current_month = parseFloat(i_current_month) - parseFloat(i_project_mnth);
	}
	
	var f_current_month_actual_revenue = parseFloat(f_revenue_amount_till_current_month) - parseFloat(i_total_revenue_recognized_ytd);
	//var i_total_revenue_recognized_ytd = 0;
	
	//nlapiLogExecution('audit','i_current_month:- '+i_current_month);
	for(var i_revenue_index=0; i_revenue_index<=i_current_month-1; i_revenue_index++)
	{
		
		
	}
	
	var i_total_actual_revenue_recognized = parseFloat(i_total_revenue_recognized_ytd)+ parseFloat(f_current_month_actual_revenue);
		
	
	{
		var f_prcnt_revenue = parseFloat(a_amount[i_current_month]) / parseFloat(i_total_row_revenue);
		f_prcnt_revenue = f_prcnt_revenue * 100;
		
		var f_revenue_amount = (parseFloat(f_prcnt_revenue) * parseFloat(f_revenue_share)) / 100;
		if(!f_revenue_amount)
			f_revenue_amount = 0;
			
		return f_revenue_amount;
		
		if(i_amt < i_current_month)
		{
			//nlapiLogExecution('audit','i_current_month:- '+i_current_month,'a_amount.length:- '+a_amount.length);
			f_revenue_amount_till_current_month = parseFloat(f_revenue_amount) + parseFloat(f_revenue_amount_till_current_month);
			if(!f_revenue_amount_till_current_month)
				f_revenue_amount_till_current_month = 0;
		}
		
		f_total_revenue = parseFloat(f_revenue_amount) + parseFloat(f_total_revenue);
		
		i_total_actual_revenue_recognized = parseFloat(i_total_actual_revenue_recognized) + parseFloat(f_revenue_amount);
	}
	
	//Total revenue recognized for previous months
	
	
	for(var i_exist_index=0; i_exist_index<=a_recognized_revenue.length; i_exist_index++)
	{
		if(a_recognized_revenue_total_proj_level[i_exist_index])
		{
			var f_existing_cost_in_arr = parseFloat(a_recognized_revenue_total_proj_level[i_exist_index]);
		}
		else
		{
			var f_existing_cost_in_arr = 0;
		}
		
		var f_existing_cost = a_recognized_revenue[i_exist_index];
		f_existing_cost = parseFloat(f_existing_cost) + parseFloat(f_existing_cost_in_arr);
		
		a_recognized_revenue_total_proj_level[i_exist_index] = f_existing_cost;
	}
	//a_recognized_revenue_total_proj_level = a_recognized_revenue;
	
	//return html;
}

function generate_cost_view_milestone(projectWiseRevenue,monthBreakUp,i_year_project,i_project_mnth,mode,sub_prac,s_currency_symbol_proj,s_currency_symbol_usd,i_year_project_end,i_prject_end_mnth)
{
	var css_file_url = "";

	var context = nlapiGetContext();
	var html_data=[];
	var html_val={};

	var html ={
		'month':[],
		'practice' :[],
		'subpractice' :[],
		'revenue':[],
		'actual_rec' :[],
		'cummulative' :[],
		'html_amount':[],
		};
		
	var today = new Date();
	var currentMonthName = getMonthName(nlapiDateToString(today));
	
	for (var j = 0; j < monthBreakUp.length; j++)
	{
		var months = monthBreakUp[j];
		var s_month_name = getMonthName(months.Start); // get month name
		var s_year = nlapiStringToDate(months.Start)
		s_year = s_year.getFullYear();
		s_month_name = s_month_name+'_'+s_year;	
		html['month'].push(s_month_name);
	}
		
	
	
	var a_sub_prac_already_displayed = new Array();
	var i_subpractice_id = 0;
	var a_amount = new Array();
	var a_recognized_revenue = new Array();
	var i_frst_row = 0;
	var i_diplay_frst_column = 0;
	var i_total_row_revenue = 0;
	var i_total_per_row = 0;
	var total_prcnt_aloocated = new Array();
	var b_first_line_copied = 0;
	var f_total_allocated_row = 0;
	
	for (var emp_internal_id in projectWiseRevenue)
	{
		if (projectWiseRevenue[emp_internal_id].sub_practice == sub_prac)
		{
			f_total_allocated_row = 0;
			for (var project_internal_id in projectWiseRevenue[emp_internal_id].RevenueData)
			{
				
				
				if(i_diplay_frst_column == 0)
				{
				//	html += "<td class='label-name'>";
				//	html['revenue'].push('Effort View');
				//	html += "</td>";
				}
				else
				{
					//html += "<td class='label-name'>";
				//	html += '<b>';
					//html += "</td>";
				}
				
				
				projectWiseRevenue[emp_internal_id].practice_name;
				 projectWiseRevenue[emp_internal_id].sub_prac_name;	
				 projectWiseRevenue[emp_internal_id].role_name;
				 projectWiseRevenue[emp_internal_id].level_name;
				projectWiseRevenue[emp_internal_id].location_name;
				
				var i_index_mnth = 0;
				for (var month in projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id])
				{
					var a_MonthNames = MonthNames(i_year_project);
					var currentMonthPos = a_MonthNames.indexOf(month);
					var s_current_month = a_MonthNames[currentMonthPos];
					var s_current_mnth_yr = s_current_month.substr(4, 8);
					
					var i_nxt_yr_flg = 0;
					if(currentMonthPos >=12)
					{
						currentMonthPos = currentMonthPos - 12;
						i_nxt_yr_flg = 1;
					}
					
					var i_curr_yr_dis_flag = 0;
					if(parseInt(s_current_mnth_yr) < i_year_project_end && i_prject_end_mnth < currentMonthPos)
					{
						i_curr_yr_dis_flag = 1;
					}
					
					if (((i_project_mnth <= currentMonthPos && ((i_prject_end_mnth >= currentMonthPos || i_curr_yr_dis_flag == 1) && i_nxt_yr_flg == 0) && parseInt(s_current_mnth_yr) <= i_year_project) && parseInt(s_current_mnth_yr) <= i_year_project_end) || (i_prject_end_mnth >= currentMonthPos && parseInt(s_current_mnth_yr) <= i_year_project_end && i_nxt_yr_flg == 1))
					{
						//nlapiLogExecution('audit','currentMonthPos:- ',+currentMonthPos);
						if(b_first_line_copied == 1)
						{
							var f_allocated_prcnt = total_prcnt_aloocated[i_index_mnth];
							f_allocated_prcnt = parseFloat(f_allocated_prcnt) + parseFloat(projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id][month].prcnt_allocated);
							total_prcnt_aloocated[i_index_mnth] = f_allocated_prcnt;
							i_index_mnth++;							
						}
						else
						{
							total_prcnt_aloocated.push(projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id][month].prcnt_allocated);
						}
						
						f_total_allocated_row = parseFloat(projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id][month].prcnt_allocated) + parseFloat(f_total_allocated_row);
						
						
						parseFloat(projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id][month].prcnt_allocated).toFixed(2);
						
					}
				}
			}
			
			b_first_line_copied = 1;
			
			
			 parseFloat(f_total_allocated_row).toFixed(2);
					
			var s_sub_prac_name = projectWiseRevenue[emp_internal_id].sub_prac_name;
			var s_practice_name = projectWiseRevenue[emp_internal_id].practice_name;
			
			i_diplay_frst_column = 1;
		}
	}
	i_diplay_frst_column = 0;
	
	
	var f_total_row_allocation = 0;
	for(var i_index_total_allocated=0; i_index_total_allocated<total_prcnt_aloocated.length; i_index_total_allocated++)
	{
		
		
		f_total_row_allocation = parseFloat(f_total_row_allocation) + parseFloat(total_prcnt_aloocated[i_index_total_allocated]);
	}
	
	
	
	for ( var emp_internal_id in projectWiseRevenue) {
		
		for ( var project_internal_id in projectWiseRevenue[emp_internal_id].RevenueData) {
			
			if(projectWiseRevenue[emp_internal_id].sub_practice == sub_prac)
			{		
				 projectWiseRevenue[emp_internal_id].practice_name;
						
				 projectWiseRevenue[emp_internal_id].sub_prac_name;
				
				 projectWiseRevenue[emp_internal_id].role_name;
				 projectWiseRevenue[emp_internal_id].level_name;
				 projectWiseRevenue[emp_internal_id].location_name;	
				 var i_sub_practice_internal_id = 0;
				var i_amt = 0;
				for (var month in projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id])
				{
					var a_MonthNames = MonthNames(i_year_project);
					var currentMonthPos = a_MonthNames.indexOf(month);
					var s_current_month = a_MonthNames[currentMonthPos];
					var s_current_mnth_yr = s_current_month.substr(4, 8);
					
					var i_nxt_yr_flg = 0;
					if(currentMonthPos >=12)
					{
						currentMonthPos = currentMonthPos - 12;
						i_nxt_yr_flg = 1;
					}
					
					var i_curr_yr_dis_flag = 0;
					if(parseInt(s_current_mnth_yr) < i_year_project_end && i_prject_end_mnth < currentMonthPos)
					{
						i_curr_yr_dis_flag = 1;
					}
					
					if (((i_project_mnth <= currentMonthPos && ((i_prject_end_mnth >= currentMonthPos || i_curr_yr_dis_flag == 1) && i_nxt_yr_flg == 0) && parseInt(s_current_mnth_yr) <= i_year_project) && parseInt(s_current_mnth_yr) <= i_year_project_end) || (i_prject_end_mnth >= currentMonthPos && parseInt(s_current_mnth_yr) <= i_year_project_end && i_nxt_yr_flg == 1))
					{
						if (mode == 2)
						{
							var total_revenue_format = parseFloat(projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id][month].Amount).toFixed(1);
							if(i_frst_row == 0)
							{
								//nlapiLogExecution('audit','inside push recog amnt:- '+projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id][month].recognized_revenue);
								a_amount.push(total_revenue_format);
								a_recognized_revenue.push(projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id][month].recognized_revenue);
							}
							else
							{
								var amnt_mnth = a_amount[i_amt];
								amnt_mnth = parseFloat(amnt_mnth) + parseFloat(projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id][month].Amount);
								
								a_amount[i_amt] = amnt_mnth;
								
								var i_existing_recognised_amount = a_recognized_revenue[i_amt];
								//nlapiLogExecution('audit','month name:- '+month);
								//nlapiLogExecution('audit','index:- '+i_amt+'::existing rev:- '+i_existing_recognised_amount,'Rev REcg:- '+projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id][month].recognized_revenue);
								i_existing_recognised_amount = parseFloat(i_existing_recognised_amount) + parseFloat(projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id][month].recognized_revenue);
								
								a_recognized_revenue[i_amt] = i_existing_recognised_amount;
								
								i_amt++;
							}
							i_total_row_revenue = parseFloat(i_total_row_revenue) + parseFloat(projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id][month].Amount);
							i_total_per_row = parseFloat(i_total_per_row) + parseFloat(projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id][month].Amount);
						}
						
						//html += "</td>";
					}
				}
				
				if (mode == 2)
				{
				 i_total_per_row = 0;
				}
				
				i_frst_row = 1;
				
				i_diplay_frst_column = 1;
				
				var f_revenue_share = projectWiseRevenue[emp_internal_id].revenue_share;
			}			
		}
	}
	
	var s_sub_prac_name = '';
	for (var emp_internal_id in projectWiseRevenue)
	{
		if (projectWiseRevenue[emp_internal_id].sub_practice == sub_prac)
		{
			s_sub_prac_name = projectWiseRevenue[emp_internal_id].sub_prac_name;
			s_practice_name = projectWiseRevenue[emp_internal_id].practice_name;
			
		}
	}
	var f_total_prcnt = 0;
	for (var i_amt = 0; i_amt < a_amount.length; i_amt++)
	{
		var f_prcnt_revenue = parseFloat(a_amount[i_amt]) / parseFloat(i_total_row_revenue);
		f_prcnt_revenue = f_prcnt_revenue * 100;
		if(!f_prcnt_revenue)
			f_prcnt_revenue = 0;
			
		f_total_prcnt = parseFloat(f_prcnt_revenue) + parseFloat(f_total_prcnt);
		
	
	}
	
	var d_today_date = new Date();
	var i_current_month = d_today_date.getMonth();
	var i_current_year = d_today_date.getFullYear();
	
	var f_total_revenue = 0;
	var f_revenue_amount_till_current_month = 0;
	for (var i_amt = 0; i_amt < a_amount.length; i_amt++)
	{
		var f_prcnt_revenue = parseFloat(a_amount[i_amt]) / parseFloat(i_total_row_revenue);
		f_prcnt_revenue = f_prcnt_revenue * 100;
		
		var f_revenue_amount = (parseFloat(f_prcnt_revenue) * parseFloat(f_revenue_share)) / 100;
		if(!f_revenue_amount)
			f_revenue_amount = 0;
		
		if (parseInt(i_year_project) != parseInt(i_current_year))
		{
			var i_total_project_tenure = 11 + parseInt(i_current_month);
			var f_total_prev_mnths = parseFloat(i_total_project_tenure) - parseFloat(i_project_mnth);
			f_total_prev_mnths = parseFloat(f_total_prev_mnths) + parseFloat(1);
		}
		else
		{
			var f_total_prev_mnths = parseFloat(i_current_month) - parseFloat(i_project_mnth);
		}
		
		if(i_amt <= f_total_prev_mnths)
		{
			//nlapiLogExecution('audit','f_revenue_amount:- '+f_revenue_amount,'a_amount.length:- '+a_amount.length);
			f_revenue_amount_till_current_month = parseFloat(f_revenue_amount) + parseFloat(f_revenue_amount_till_current_month);
		}
		
		f_total_revenue = parseFloat(f_revenue_amount) + parseFloat(f_total_revenue);
		

	}
	
	for (var emp_internal_id in projectWiseRevenue)
	{
		for (var project_internal_id in projectWiseRevenue[emp_internal_id].RevenueData)
		{
			var i_amount_map = 0;
			if (projectWiseRevenue[emp_internal_id].sub_practice == sub_prac)
			{
				for (var month in projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id])
				{
					var a_MonthNames = MonthNames(i_year_project);
					var currentMonthPos = a_MonthNames.indexOf(month);
					var s_current_month = a_MonthNames[currentMonthPos];
					var s_current_mnth_yr = s_current_month.substr(4, 8);
					
					var i_nxt_yr_flg = 0;
					if(currentMonthPos >=12)
					{
						currentMonthPos = currentMonthPos - 12;
						i_nxt_yr_flg = 1;
					}
					
					var i_curr_yr_dis_flag = 0;
					if(parseInt(s_current_mnth_yr) < i_year_project_end && i_prject_end_mnth < currentMonthPos)
					{
						i_curr_yr_dis_flag = 1;
					}
					
					if (((i_project_mnth <= currentMonthPos && ((i_prject_end_mnth >= currentMonthPos || i_curr_yr_dis_flag == 1) && i_nxt_yr_flg == 0) && parseInt(s_current_mnth_yr) <= i_year_project) && parseInt(s_current_mnth_yr) <= i_year_project_end) || (i_prject_end_mnth >= currentMonthPos && parseInt(s_current_mnth_yr) <= i_year_project_end && i_nxt_yr_flg == 1))
					{
						var f_prcnt_revenue = parseFloat(a_amount[i_amount_map]) / parseFloat(i_total_row_revenue);
						f_prcnt_revenue = f_prcnt_revenue * 100;
						
						var f_revenue_amount = (parseFloat(f_prcnt_revenue) * parseFloat(f_revenue_share)) / 100;
		
						projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id][month].total_revenue = f_revenue_amount;
						projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id][month].prcent_complt = f_prcnt_revenue;
						i_amount_map++;
					}
				}	
			}
		}
	}
	
	
	var today = new Date();
	var currentMonthName = getMonthName(nlapiDateToString(today));
	
	for (var j = 0; j < monthBreakUp.length; j++)
	{
		var months = monthBreakUp[j];
		var s_month_name = getMonthName(months.Start); // get month name
		var s_year = nlapiStringToDate(months.Start)
		s_year = s_year.getFullYear();
		s_month_name = s_month_name+'_'+s_year;
		
	}

	
	var i_total_revenue_recognized_ytd = 0;
	for(var i_revenue_index=0; i_revenue_index<a_recognized_revenue.length; i_revenue_index++)
	{
		//nlapiLogExecution('audit','prev mnth recog rev:- '+a_recognized_revenue[i_revenue_index]);
		i_total_revenue_recognized_ytd = parseFloat(i_total_revenue_recognized_ytd) + parseFloat(a_recognized_revenue[i_revenue_index]);
		
	}
	
	//Actual revenue to be recognized 
	//html += "<tr>";
	
	html['revenue'].push('Actual revenue recognized/Revenue forecast');
	html['practice'].push(s_practice_name);
	html['subpractice'].push(s_sub_prac_name);
	
	if(parseInt(i_year_project) != parseInt(i_current_year))
	{
		var i_total_project_tenure = 11 + parseInt(i_current_month);
		var f_total_prev_mnths = parseFloat(i_total_project_tenure) - parseFloat(i_project_mnth);
		i_current_month = parseFloat(f_total_prev_mnths) + parseFloat(1);
			
		for(var i_revenue_index=0; i_revenue_index<i_current_month; i_revenue_index++)
		{
			
			html['revenue'].push(format2(a_recognized_revenue[i_revenue_index]));
			
		}
	}
	else
	{
		i_current_month = parseFloat(i_current_month) - parseFloat(i_project_mnth);
		//nlapiLogExecution('audit','i_current_month:- '+i_current_month,a_recognized_revenue[i_current_month-1]);
		for(var i_revenue_index=0; i_revenue_index<=i_current_month-1; i_revenue_index++)
		{
			html['revenue'].push(format2(a_recognized_revenue[i_revenue_index]));
		
		}
	}
	
	//nlapiLogExecution('audit','f_revenue_amount_till_current_month:- '+f_revenue_amount_till_current_month,'i_total_revenue_recognized_ytd:- '+i_total_revenue_recognized_ytd);
	var f_current_month_actual_revenue = parseFloat(f_revenue_amount_till_current_month) - parseFloat(i_total_revenue_recognized_ytd);
	//var i_total_revenue_recognized_ytd = 0;
	
	f_current_month_actual_revenue_total_proj_level = parseFloat(f_current_month_actual_revenue_total_proj_level) + parseFloat(f_current_month_actual_revenue);
	
	html['revenue'].push(format2(f_current_month_actual_revenue));
	
	var i_total_actual_revenue_recognized = parseFloat(i_total_revenue_recognized_ytd)+ parseFloat(f_current_month_actual_revenue);
	
	for (var i_amt = i_current_month+1; i_amt < a_amount.length; i_amt++)
	{
		var f_prcnt_revenue = parseFloat(a_amount[i_amt]) / parseFloat(i_total_row_revenue);
		f_prcnt_revenue = f_prcnt_revenue * 100;
		
		var f_revenue_amount = (parseFloat(f_prcnt_revenue) * parseFloat(f_revenue_share)) / 100;
		if(!f_revenue_amount)
			f_revenue_amount = 0;
		
		if(i_amt < i_current_month)
		{
			//nlapiLogExecution('audit','i_current_month:- '+i_current_month,'a_amount.length:- '+a_amount.length);
			f_revenue_amount_till_current_month = parseFloat(f_revenue_amount) + parseFloat(f_revenue_amount_till_current_month);
			if(!f_revenue_amount_till_current_month)
				f_revenue_amount_till_current_month = 0;
		}
		
		f_total_revenue = parseFloat(f_revenue_amount) + parseFloat(f_total_revenue);
		
		html['revenue'].push(format2(f_revenue_amount));
		
		
		i_total_actual_revenue_recognized = parseFloat(i_total_actual_revenue_recognized) + parseFloat(f_revenue_amount);
	}
	
	
	html['revenue'].push(format2(i_total_actual_revenue_recognized));
	
	html['cummulative'].push('Cumulative Revenue');
	

	var f_cumulative_rev = 0;
	for(var i_revenue_index=0; i_revenue_index<=i_current_month-1; i_revenue_index++)
	{
		var f_amount_rounded_off = parseFloat(a_recognized_revenue[i_revenue_index]).toFixed(1);
		
		f_cumulative_rev = parseFloat(f_cumulative_rev) + parseFloat(f_amount_rounded_off);
	//	html += "<td class='monthly-amount'>";
		html['cummulative'].push(format2(f_cumulative_rev));
		//html += "</td>";
	}
	
	f_amount_rounded_off = parseFloat(f_current_month_actual_revenue).toFixed(1);
	f_cumulative_rev = parseFloat(f_cumulative_rev) + parseFloat(f_amount_rounded_off);
	//html += "<td class='monthly-amount'>";
	//html['cummulative'].push(s_currency_symbol_proj+' '+format2(f_cumulative_rev));
	//html += "</td>";
	
	for(var i_future_index=i_current_month+1; i_future_index<a_amount.length; i_future_index++)
	{
		var f_prcnt_revenue = parseFloat(a_amount[i_future_index]) / parseFloat(i_total_row_revenue);
		f_prcnt_revenue = f_prcnt_revenue * 100;
		
		var f_revenue_amount = (parseFloat(f_prcnt_revenue) * parseFloat(f_revenue_share)) / 100;
		if(!f_revenue_amount)
			f_revenue_amount = 0;
			
		f_cumulative_rev = parseFloat(f_cumulative_rev) + parseFloat(f_revenue_amount);
	//	html += "<td class='monthly-amount'>";
		html['cummulative'].push(format2(f_cumulative_rev));
	//	html += "</td>";
	}
	
	//if(i_total_revenue_recognized_ytd == 0)
		i_total_revenue_recognized_ytd = f_revenue_share;
	
	//html += "<td class='monthly-amount'>";
	html['cummulative'].push(format2(i_total_revenue_recognized_ytd));
	//html += "</td>";
	
	//html += "</tr>";
	
	//html += "</table>";
	
	for(var i_exist_index=0; i_exist_index<=a_recognized_revenue.length; i_exist_index++)
	{
		if(a_recognized_revenue_total_proj_level[i_exist_index])
		{
			var f_existing_cost_in_arr = parseFloat(a_recognized_revenue_total_proj_level[i_exist_index]);
		}
		else
		{
			var f_existing_cost_in_arr = 0;
		}
		
		var f_existing_cost = a_recognized_revenue[i_exist_index];
		f_existing_cost = parseFloat(f_existing_cost) + parseFloat(f_existing_cost_in_arr);
		
		a_recognized_revenue_total_proj_level[i_exist_index] = f_existing_cost;
	}
	//a_recognized_revenue_total_proj_level = a_recognized_revenue;
	var data_month_All =html.month;
	var commulative_rev_month = html.cummulative; 
	var actual_rev_month = html.revenue;//revenue 
	var dataRow_filtered = [];
	for(var indxx=0;indxx<data_month_All.length;indxx++)	{
		
		if(indxx==9)
        {
			nlapiLogExecution("DEBUG",'actual_rev_month[indxx +1]',actual_rev_month[indxx +1]);
			//nlapiLogExecution("DEBUG",'data_month_All[indxx]',data_month_All[indxx]);
        }
		
		//Logic to restrict JSON only for future months
		var d_today = nlapiDateToString(new Date());
		// d_today = nlapiAddMonths(nlapiStringToDate(d_today), -1);
	 	//var d_today = '12/1/2017';
	 	d_today = nlapiStringToDate(d_today);
	 	
	 	var getCurrentMonth = d_today.getMonth() + 1;
	 	var getCurrentYear = d_today.getFullYear();
		
	 	//Report Months Capture
	 	
	 	
	 	var data_month_ind = data_month_All[indxx].split('_');
	 	
	 	var data_m_ind = data_month_ind[0];
	 	var d_month = getMonthCompleteIndex(data_m_ind);
	 //	d_month = parseInt(d_month);
	 	var data_year = data_month_ind[1];
	 	
	 	if(parseInt(d_month) >= parseInt(getCurrentMonth) && parseInt(data_year) >= parseInt(getCurrentYear)){
	 		
	 		dataRow_filtered.push({
	 			'month': data_month_All[indxx],
	 			'cummulative': commulative_rev_month[indxx +1],
	 			'revenue' : actual_rev_month[indxx +1],
	 			'practice':s_practice_name,
	 		'subpractice':s_sub_prac_name
	 		});
		
	 	}
	 	else if(parseInt(data_year) > parseInt(getCurrentYear)  ){
	 		dataRow_filtered.push({
	 			'month': data_month_All[indxx],
	 			'cummulative': commulative_rev_month[indxx +1],
	 			'revenue' : actual_rev_month[indxx +1],
	 			'practice':s_practice_name,
	 		'subpractice':s_sub_prac_name
	 		});
	 	}
	}
	html_val={
		pract : s_practice_name,
		sub_pract : s_sub_prac_name,
		};
	html_data.push(html);
	nlapiLogExecution('DEBUG','HTML',JSON.stringify(dataRow_filtered));	

	return dataRow_filtered;
}

function generate_total_project_view(projectWiseRevenue,monthBreakUp,i_year_project,i_project_mnth,mode,s_currency_symbol_proj,s_currency_symbol_usd,i_year_project_end,i_prject_end_mnth)
{
	var css_file_url = "";

	var context = nlapiGetContext();
	

	var html ={
		'month':[],
		'monthlyrevenue' :[],
		'rev_rec_permnth' :[],
		'act_rev_rec_mnth' :[],
		'cummulative_mnth':[],
		};

		
	var today = new Date();
	var currentMonthName = getMonthName(nlapiDateToString(today));
	
	for (var j = 0; j < monthBreakUp.length; j++)
	{
		var months = monthBreakUp[j];
		var s_month_name = getMonthName(months.Start); // get month name
		var s_year = nlapiStringToDate(months.Start)
		s_year = s_year.getFullYear();
		s_month_name = s_month_name+'_'+s_year;
				
		html['month'].push(s_month_name);
		
	}
		

	//html += "Total";
		
	var a_sub_prac_already_displayed = new Array();
	var i_subpractice_id = 0;
	var a_amount = new Array();
	var a_prcnt_arr = new Array();
	var i_frst_row = 0;
	var i_diplay_frst_column = 0;
	var i_total_row_revenue = 0;
	var total_prcnt_aloocated = new Array();
	var b_first_line_copied = 0;
	var f_total_allocated_row = 0;
	var i_total_row_revenue = 0;
	var f_total_prcnt_complt = 0;
	var a_sub_practice_arr = new Array();
	
	for ( var emp_internal_id in projectWiseRevenue)
	{
		for ( var project_internal_id in projectWiseRevenue[emp_internal_id].RevenueData)
		{
			var i_sub_practice_internal_id = 0;
			var i_amt = 0;
			
			if (a_sub_practice_arr.indexOf(projectWiseRevenue[emp_internal_id].sub_practice) < 0)
			{
				for (var month in projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id])
				{
					var a_MonthNames = MonthNames(i_year_project);
					var currentMonthPos = a_MonthNames.indexOf(month);
					var s_current_month = a_MonthNames[currentMonthPos];
					var s_current_mnth_yr = s_current_month.substr(4, 8);
					
					var i_nxt_yr_flg = 0;
					if(currentMonthPos >=12)
					{
						currentMonthPos = currentMonthPos - 12;
						i_nxt_yr_flg = 1;
					}
					
					var i_curr_yr_dis_flag = 0;
					if(parseInt(s_current_mnth_yr) < i_year_project_end && i_prject_end_mnth < currentMonthPos)
					{
						i_curr_yr_dis_flag = 1;
					}
					
					if (((i_project_mnth <= currentMonthPos && ((i_prject_end_mnth >= currentMonthPos || i_curr_yr_dis_flag == 1) && i_nxt_yr_flg == 0) && parseInt(s_current_mnth_yr) <= i_year_project) && parseInt(s_current_mnth_yr) <= i_year_project_end) || (i_prject_end_mnth >= currentMonthPos && parseInt(s_current_mnth_yr) <= i_year_project_end && i_nxt_yr_flg == 1))
					{
						var total_revenue_format = parseFloat(projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id][month].total_revenue);
						if(!total_revenue_format)
							total_revenue_format = 0;
							
						var f_perctn_complete = parseFloat(projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id][month].prcent_complt);
						if(!f_perctn_complete)
							f_perctn_complete = 0;
							
						if (i_frst_row == 0)
						{
							a_amount.push(total_revenue_format);
							a_prcnt_arr.push(f_perctn_complete);
						}
						else
						{
							var amnt_mnth = a_amount[i_amt];
							
							var f_revenue_amount = projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id][month].total_revenue;
							if(!f_revenue_amount)
								f_revenue_amount = 0;
								
							amnt_mnth = parseFloat(amnt_mnth) + parseFloat(f_revenue_amount);
							a_amount[i_amt] = amnt_mnth;
							//i_amt++;
							
							var f_prcnt_present = a_prcnt_arr[i_amt];
							
							var f_prcnt_updated = projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id][month].prcent_complt;
							if(!f_prcnt_updated)
								f_prcnt_updated = 0;
								
							f_prcnt_present = parseFloat(f_prcnt_present) + parseFloat(f_prcnt_updated);
							a_prcnt_arr[i_amt] = f_prcnt_present;
							i_amt++;
						}
						
						a_sub_practice_arr.push(projectWiseRevenue[emp_internal_id].sub_practice);
						
					}
				}
			}
			i_frst_row = 1;
	
			var f_revenue_share = projectWiseRevenue[emp_internal_id].revenue_share;			
		}
	}
	
	//prcnt completed for project
	
	html['monthlyrevenue'].push('Monthly Percent Complete');

	
	for (var i_amt = 0; i_amt < a_amount.length; i_amt++)
	{
		i_total_row_revenue = parseFloat(i_total_row_revenue) + parseFloat(a_amount[i_amt]);
	}
	
	for (var i_amt = 0; i_amt < a_amount.length; i_amt++)
	{
		var f_prcnt_to_diaplay = parseFloat(a_amount[i_amt]) / parseFloat(i_total_row_revenue);
		f_prcnt_to_diaplay = parseFloat(f_prcnt_to_diaplay) * 100;
		
		
		html['monthlyrevenue'].push(parseFloat(f_prcnt_to_diaplay).toFixed(1)+' %');
		
		
		f_total_prcnt_complt = parseFloat(f_total_prcnt_complt) + parseFloat(f_prcnt_to_diaplay);
		
	}
	
//	html += "<td class='projected-amount'>";
	html['monthlyrevenue'].push(parseFloat(f_total_prcnt_complt).toFixed(1)+' %');
	//html += "</td>";
	
	//html += "</tr>";
	
	// total revenue recognized project level

	html['rev_rec_permnth'].push('Revenue to be reconized/month'); //Total Project Revenue as per Plan
	
	
	for (var i_amt = 0; i_amt < a_amount.length; i_amt++)
	{
		
		html['rev_rec_permnth'].push(s_currency_symbol_proj+' '+format2(a_amount[i_amt]));
				
	}
	
	html['rev_rec_permnth'].push(s_currency_symbol_proj+' '+format2(i_total_row_revenue));
		
	// actual revenue that will be recognized

	html['act_rev_rec_mnth'].push('Actual revenue recognized/Revenue forecast'); //Actual Revenue to Recognize
	
	
	var d_today_date = new Date();
	var i_current_month = d_today_date.getMonth();
	var i_current_year = d_today_date.getFullYear();
	//nlapiLogExecution('audit','proj level len:- ',a_recognized_revenue_total_proj_level.length);
	
	var f_total_revenue_proj_level = 0;
	if(parseInt(i_year_project) != parseInt(i_current_year))
	{
		var i_total_project_tenure = 11 - parseInt(i_project_mnth);
		i_current_month = i_current_month + i_total_project_tenure + 1;
		
		for(var i_revenue_index=0; i_revenue_index<=i_current_month; i_revenue_index++)
		{
			//i_total_revenue_recognized_ytd = parseFloat(i_total_revenue_recognized_ytd) + parseFloat(a_recognized_revenue[i_revenue_index]);
			
			html['act_rev_rec_mnth'].push(s_currency_symbol_proj+' '+format2(a_recognized_revenue_total_proj_level[i_revenue_index]));
			f_total_revenue_proj_level = parseFloat(f_total_revenue_proj_level) + parseFloat(a_recognized_revenue_total_proj_level[i_revenue_index]);
		}
	}
	else
	{
		i_current_month = parseFloat(i_current_month) - parseFloat(i_project_mnth);
		for(var i_revenue_index=0; i_revenue_index<=i_current_month-1; i_revenue_index++)
		{
			//i_total_revenue_recognized_ytd = parseFloat(i_total_revenue_recognized_ytd) + parseFloat(a_recognized_revenue[i_revenue_index]);
			
			
			html['act_rev_rec_mnth'].push(s_currency_symbol_proj+' '+format2(a_recognized_revenue_total_proj_level[i_revenue_index]));
			
			f_total_revenue_proj_level = parseFloat(f_total_revenue_proj_level) + parseFloat(a_recognized_revenue_total_proj_level[i_revenue_index]);
		}
	}
	
	
	html['act_rev_rec_mnth'].push(s_currency_symbol_proj+' '+format2(f_current_month_actual_revenue_total_proj_level));
	
	
	f_total_revenue_proj_level = parseFloat(f_total_revenue_proj_level) + parseFloat(f_current_month_actual_revenue_total_proj_level);
	
	for (var i_amt = i_current_month+1; i_amt < a_amount.length; i_amt++)
	{
		
		html['act_rev_rec_mnth'].push(s_currency_symbol_proj+' '+format2(a_amount[i_amt]));
				
		f_total_revenue_proj_level = parseFloat(f_total_revenue_proj_level) + parseFloat(a_amount[i_amt]);
	}
	

	html['act_rev_rec_mnth'].push(s_currency_symbol_proj+' '+format2(f_total_revenue_proj_level));
	
	// cumulative total project level

	html['cummulative_mnth'].push('Cumulative Revenue');
	
	var f_project_level_cumulative = 0;
	for(var i_revenue_index=0; i_revenue_index<=i_current_month-1; i_revenue_index++)
	{
		//i_total_revenue_recognized_ytd = parseFloat(i_total_revenue_recognized_ytd) + parseFloat(a_recognized_revenue[i_revenue_index]);
		f_project_level_cumulative = parseFloat(f_project_level_cumulative) + parseFloat(a_recognized_revenue_total_proj_level[i_revenue_index]);
		html['cummulative_mnth'].push(s_currency_symbol_proj+' '+format2(f_project_level_cumulative));
		
	}
	
	f_project_level_cumulative = parseFloat(f_project_level_cumulative) + parseFloat(f_current_month_actual_revenue_total_proj_level);
	
	html['cummulative_mnth'].push(s_currency_symbol_proj+' '+format2(f_project_level_cumulative));
	
	for (var i_amt = i_current_month+1; i_amt < a_amount.length; i_amt++)
	{
		f_project_level_cumulative = parseFloat(f_project_level_cumulative) + parseFloat(a_amount[i_amt]);
		html['cummulative_mnth'].push(s_currency_symbol_proj+' '+format2(f_project_level_cumulative));
	}
	
	
	html['cummulative_mnth'].push('<b>'+s_currency_symbol_proj+' '+format2(f_total_revenue_proj_level));
	nlapiLogExecution('debug','Value',JSON.stringify(html));
	
	return html;
}

function generate_table_effort_view(projectWiseRevenue,monthBreakUp,i_year_project,i_project_mnth,mode,s_currency_symbol_proj,s_currency_symbol_usd,i_year_project_end)
{
	var css_file_url = "";

	var context = nlapiGetContext();
	if (context.getEnvironment() == "SANDBOX") {
		css_file_url = "https://system.sandbox.netsuite.com/core/media/media.nl?id=199133&c=3883006&h=64fa194295cd4ab64d58&mv=iq3kjr6g&_xt=.css&whence=";
	} else if (context.getEnvironment() == "PRODUCTION") {
		css_file_url = "https://system.na1.netsuite.com/core/media/media.nl?id=199133&c=3883006&h=64fa194295cd4ab64d58&mv=iq3kjr6g&_xt=.css&whence=";
	}

	var html = "";
	html += "<link href='" + css_file_url + "'  rel='stylesheet'>";

	html += "<table class='projection-table'>";

	// header
	html += "<tr class='header-row'>";
	
	html += "<td>";
	html += "Practice";
	html += "</td>";
	
	html += "<td>";
	html += "Sub Practice";
	html += "</td>";

	if(mode != 5)
	{	
		html += "<td>";
		html += "Role";
		html += "</td>";
	
		if(mode == 1 || mode == 2 || mode == 6)
		{
			html += "<td>";
			html += "Level";
			html += "</td>";
			
			if(mode == 6)
			{
				html += "<td>";
				html += "No. of resources";
				html += "</td>";
				
				html += "<td>";
				html += "Allocation start date";
				html += "</td>";
				
				html += "<td>";
				html += "Allocation end date";
				html += "</td>";
				
				html += "<td>";
				html += "Percent allocated";
				html += "</td>";
			}
			
			html += "<td>";
			html += "Location";
			html += "</td>";
		}
	}
	else
	{
		html += "<td>";
		html += "Revenue Share";
		html += "</td>";
	}
	
	if(mode != 5 && mode != 6)
	{
		var today = new Date();
		var currentMonthName = getMonthName(nlapiDateToString(today));
		
		for (var j = 0; j < monthBreakUp.length; j++)
		{
			var months = monthBreakUp[j];
			var s_month_name = getMonthName(months.Start); // get month name
			var s_year = nlapiStringToDate(months.Start)
			s_year = s_year.getFullYear();
			s_month_name = s_month_name+'_'+s_year;
					
			html += "<td>";
			html += s_month_name;
			html += "</td>";
		}
		
		if (mode == 2)
		{
			html += "<td>";
			html += "Total";
			html += "</td>";
		}
	}
		
	html += "</tr>";
	
	var a_sub_prac_already_displayed = new Array();
	var i_subpractice_id = 0;
	for ( var emp_internal_id in projectWiseRevenue) {
		
		for ( var project_internal_id in projectWiseRevenue[emp_internal_id].RevenueData) {
			
			var b_display_reveue_share = 'F';
			if(a_sub_prac_already_displayed.indexOf(projectWiseRevenue[emp_internal_id].sub_practice) < 0 && mode == 5)
			{
				html += "<tr>";
				html += "<td class='label-name'>";
				html += projectWiseRevenue[emp_internal_id].practice_name;
				html += "</td>";
					
				html += "<td class='label-name'>";
				html += projectWiseRevenue[emp_internal_id].sub_prac_name;
				html += "</td>";
				
				b_display_reveue_share = 'T';
				a_sub_prac_already_displayed.push(projectWiseRevenue[emp_internal_id].sub_practice);
			}
			
			if(mode != 5)
			{
				html += "<tr>";
				html += "<td class='label-name'>";
				html += projectWiseRevenue[emp_internal_id].practice_name;
				html += "</td>";
					
				html += "<td class='label-name'>";
				html += projectWiseRevenue[emp_internal_id].sub_prac_name;
				html += "</td>";
			}
			
			if (mode != 5)
			{
				html += "<td class='label-name'>";
				html += projectWiseRevenue[emp_internal_id].role_name;
				html += "</td>";
				
				if (mode == 1 || mode == 2 || mode == 6)
				{
					html += "<td class='label-name'>";
					html += projectWiseRevenue[emp_internal_id].level_name;
					html += "</td>";
					
					if(mode == 6)
					{
						html += "<td class='label-name'>";
						html += projectWiseRevenue[emp_internal_id].no_of_resources;
						html += "</td>";
						
						html += "<td class='label-name'>";
						html += projectWiseRevenue[emp_internal_id].allocation_strt_date;
						html += "</td>";
						
						html += "<td class='label-name'>";
						html += projectWiseRevenue[emp_internal_id].allocation_end_date;
						html += "</td>";
						
						html += "<td class='label-name'>";
						html += projectWiseRevenue[emp_internal_id].total_allocation;
						html += "</td>";
					}
					
					html += "<td class='label-name'>";
					html += projectWiseRevenue[emp_internal_id].location_name;
					html += "</td>";
				}
			}
			else
			{
				if (b_display_reveue_share == 'T')
				{
					html += "<td class='label-name'>";
					html += ''+s_currency_symbol_proj+' '+format2(projectWiseRevenue[emp_internal_id].revenue_share);
					html += "</td>";
				}
			}
			
			if (mode != 5 && mode != 6)
			{
				var i_sub_practice_internal_id = 0;
				for (var month in projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id])
				{
					var a_MonthNames = MonthNames(i_year_project);
					var currentMonthPos = a_MonthNames.indexOf(month);
					var s_current_month = a_MonthNames[currentMonthPos];
					var s_current_mnth_yr = s_current_month.substr(4, 8);
					
					var i_nxt_yr_flg = 0;
					if(currentMonthPos >=12)
					{
						currentMonthPos = currentMonthPos - 12;
						i_nxt_yr_flg = 1;
					}
					
					var i_curr_yr_dis_flag = 0;
					if(parseInt(s_current_mnth_yr) < i_year_project_end && i_prject_end_mnth < currentMonthPos)
					{
						i_curr_yr_dis_flag = 1;
					}
					
					if (((i_project_mnth <= currentMonthPos && ((i_prject_end_mnth >= currentMonthPos || i_curr_yr_dis_flag == 1)&& i_nxt_yr_flg == 0) && parseInt(s_current_mnth_yr) <= i_year_project) && parseInt(s_current_mnth_yr) <= i_year_project_end) || (i_prject_end_mnth >= currentMonthPos && parseInt(s_current_mnth_yr) <= i_year_project_end && i_nxt_yr_flg == 1))
					{
						
						if (mode == 2)
						{
							html += "<td class='monthly-amount'>";
							var total_revenue_format = parseFloat(projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id][month].Amount).toFixed(1);
							html += format2(total_revenue_format);
						}
						else if (mode == 1)
						{
							html += "<td class='monthly-amount'>";
							html += parseFloat(projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id][month].prcnt_allocated).toFixed(1);
							
							if(i_sub_practice_internal_id = 0)
							{
								i_sub_practice_internal_id = projectWiseRevenue[emp_internal_id].sub_practice;
							}
						}
						else if (mode == 3 || mode == 4)
						{
							var f_amount = projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id][month].Amount;
							var f_total_amount = projectWiseRevenue[emp_internal_id].total_revenue_for_tenure;
							
							var f_prcnt_cost = parseFloat(f_amount) / parseFloat(f_total_amount);
							var f_prcnt_cost_ = parseFloat(f_prcnt_cost) * parseFloat(100);
							
							if (mode == 3)
							{
								f_prcnt_cost_ = parseFloat(f_prcnt_cost_).toFixed(1);
								html += "<td class='monthly-amount'>";
								html += f_prcnt_cost_+' %';
							}
							else
							{
								html += "<td class='projected-amount'>";
								var f_revenue_share = projectWiseRevenue[emp_internal_id].revenue_share;
								if(projectWiseRevenue[emp_internal_id].sub_practice == 316)
									nlapiLogExecution('audit','prcnt:- '+f_prcnt_cost,f_revenue_share);
									
								f_revenue_share = (parseFloat(f_prcnt_cost_) * parseFloat(f_revenue_share)) / 100;
								html += format2(f_revenue_share);
							}
						}
						
						html += "</td>";
					}
				}
			}
			
			if (mode == 2)
			{
				html += "<td class='monthly-amount'>";
				html += format2(projectWiseRevenue[emp_internal_id].total_revenue_for_tenure);
				html += "</td>";
			}
			
			html += "</tr>";			
		}
	}

	html += "</table>";
	
	return html;
}

function format2(n) {
    n = parseFloat(n).toFixed(1).replace(/(\d)(?=(\d{3})+\.)/g, "$1,");
	n = n.toString().split('.');
	return n[0];
}

function getCurrency_Symbol(s_proj_currency)
{
	var s_currency_symbols = {
    'USD': '$', // US Dollar
    'EUR': '€', // Euro
    'GBP': '£', // British Pound Sterling
    'INR': '₹', // Indian Rupee
	};
	
	if(s_currency_symbols[s_proj_currency]!==undefined) {
	    return s_currency_symbols[s_proj_currency];
	}
}

function getMonthsBreakup(d_startDate, d_endDate) {
	
	try
	{
		var dateBreakUp = [];
		var new_start_date = new Date(d_startDate.getFullYear(), d_startDate
		        .getMonth(), 1);
				
		var nxt_mnth_strt_date = new Date(d_startDate.getFullYear(), d_startDate
		        .getMonth()+1, 1);
		
		var endDate = new Date(nxt_mnth_strt_date - 1);
				
		//var endDate = nlapiAddDays(nlapiAddMonths(d_startDate, 1), -1);
		endDate = nlapiDateToString(endDate, 'date');
	
		dateBreakUp.push({
		    Start : nlapiDateToString(new_start_date, 'date'),
		    End : endDate
		});
	
		for (var i = 1;; i++) {
			var new_date = nlapiAddMonths(new_start_date, i);
	
			if (new_date > d_endDate) {
				break;
			}
			
			endDate = nlapiAddDays(nlapiAddMonths(new_date, 1), -1);
			endDate = nlapiDateToString(endDate, 'date');
					
			dateBreakUp.push({
			    Start : nlapiDateToString(new_date, 'date'),
			    End : endDate
			});
		}
	}
	catch(err)
	{
		nlapiLogExecution('ERROR','getMonthsBreakup','ERROR MESSAGE :- '+err);
		throw err;
	}

	return dateBreakUp;
}

function getMonthName(currentDate) {
	currentDate = nlapiStringToDate(currentDate)
	var monthNames = [ "JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JUL", "AUG",
	        "SEP", "OCT", "NOV", "DEC" ];
	return monthNames[currentDate.getMonth()];
}

function MonthNames(yr) {
	var yr1 = Number(yr) + 1;
	
	var monthNames = [ "JAN_"+yr, "FEB_"+yr, "MAR_"+yr, "APR_"+yr, "MAY_"+yr, "JUN_"+yr, "JUL_"+yr, "AUG_"+yr,
	        "SEP_"+yr, "OCT_"+yr, "NOV_"+yr, "DEC_"+yr ];
	var month_nxt_yr = ["JAN_"+yr1, "FEB_"+yr1, "MAR_"+yr1, "APR_"+yr1, "MAY_"+yr1, "JUN_"+yr1, "JUL_"+yr1, "AUG_"+yr1,
	        "SEP_"+yr1, "OCT_"+yr1, "NOV_"+yr1, "DEC_"+yr1 ];
	monthNames = monthNames.concat(month_nxt_yr);
	
	return monthNames;
}

function yearData(yr) {
	var yr1 = Number(yr) + 1;
	
	var monthNames = [ "JAN_"+yr, "FEB_"+yr, "MAR_"+yr, "APR_"+yr, "MAY_"+yr, "JUN_"+yr, "JUL_"+yr, "AUG_"+yr,
	        "SEP_"+yr, "OCT_"+yr, "NOV_"+yr, "DEC_"+yr ];
	var month_nxt_yr = ["JAN_"+yr1, "FEB_"+yr1, "MAR_"+yr1, "APR_"+yr1, "MAY_"+yr1, "JUN_"+yr1, "JUL_"+yr1, "AUG_"+yr1,
	        "SEP_"+yr1, "OCT_"+yr1, "NOV_"+yr1, "DEC_"+yr1 ];
	monthNames = monthNames.concat(month_nxt_yr);
	
	for (var i = 0; i < monthNames.length; i++) {
		this[monthNames[i]] = {
		    Amount : 0,
			prcnt_allocated : 0,
			total_revenue : 0,
			recognized_revenue : 0,
			actual_revenue : 0,
			mnth_strt : 0,
			mnth_end : 0,
			prcent_complt : 0
		};
	}
}

function generate_previous_effrt_revenue(s_effrt_json,projectWiseRevenue_previous_effrt,a_revenue_recognized_for_project,i_year_project)
{
	if(s_effrt_json)
	{
		var i_practice_previous = 0;
		var f_total_revenue_for_tenure = 0;
		//for (var i_mnth_end_plan = 0; i_mnth_end_plan < a_get_mnth_end_effrt_plan.length; i_mnth_end_plan++)
		var s_entire_json_clubed = JSON.parse(s_effrt_json);
		for (var i_mnth_end_plan = 0; i_mnth_end_plan < s_entire_json_clubed.length; i_mnth_end_plan++)
		{
			var a_row_json_data = JSON.parse(JSON.stringify(s_entire_json_clubed[i_mnth_end_plan]));
			for (var i_row_json_index = 0; i_row_json_index < a_row_json_data.length; i_row_json_index++)
			{
				var i_practice = a_row_json_data[i_row_json_index].prac;
				var s_practice = a_row_json_data[i_row_json_index].prac_text;
				var i_sub_practice = a_row_json_data[i_row_json_index].subprac;
				var s_sub_practice = a_row_json_data[i_row_json_index].subprac_text;
				var i_role = a_row_json_data[i_row_json_index].role;
				var s_role = a_row_json_data[i_row_json_index].role_text;
				var i_level = a_row_json_data[i_row_json_index].level;
				var s_level = a_row_json_data[i_row_json_index].level_text;
				var i_location = a_row_json_data[i_row_json_index].loc;
				var s_location = a_row_json_data[i_row_json_index].loc_text;
				var f_revenue = a_row_json_data[i_row_json_index].cost;
				if (!f_revenue) 
					f_revenue = 0;
				
				var f_revenue_share = a_row_json_data[i_row_json_index].share;
				if (!f_revenue_share) 
					f_revenue_share = 0;
					
				var i_no_of_resources = a_row_json_data[i_row_json_index].allo;
				var s_mnth_strt_date = '1/31/2017';
				var s_mnth_end_date = '31/31/2017';
				var s_mnth = a_row_json_data[i_row_json_index].mnth;
				var s_year = a_row_json_data[i_row_json_index].year;
				
				var f_revenue_recognized = find_recognized_revenue_prev(a_revenue_recognized_for_project,i_practice,i_sub_practice,i_role,i_level,s_mnth,s_year,a_prev_subprac_searched_once);
				
				if (!f_revenue_recognized) 
					f_revenue_recognized = 0;
				
				//nlapiLogExecution('audit','f_revenue_recognized:- '+f_revenue_recognized);
				
				var s_month_name = s_mnth + '_' + s_year;
				
				var total_revenue = parseFloat(i_no_of_resources) * parseFloat(f_revenue);
				total_revenue = parseFloat(total_revenue).toFixed(2);
				
				if (i_practice_previous == 0) {
					i_practice_previous = a_row_json_data[i_row_json_index].prac_row;
				}
				
				if (i_practice_previous != a_row_json_data[i_row_json_index].prac_row) {
					f_total_revenue_for_tenure = 0;
					i_practice_previous = a_row_json_data[i_row_json_index].prac_row;
				}
			
				f_total_revenue_for_tenure = parseFloat(f_total_revenue_for_tenure) + parseFloat(total_revenue);
				f_total_revenue_for_tenure = parseFloat(f_total_revenue_for_tenure).toFixed(2);
				
				var i_prac_subPrac_role_level = i_practice + '_' + i_sub_practice + '_' + i_role + '_' + i_level + '_' + i_location;
				
				if (!projectWiseRevenue_previous_effrt[i_prac_subPrac_role_level]) {
					projectWiseRevenue_previous_effrt[i_prac_subPrac_role_level] = {
						practice_name: s_practice,
						sub_prac_name: s_sub_practice,
						sub_practice: i_sub_practice,
						role_name: s_role,
						level_name: s_level,
						location_name: s_location,
						total_revenue_for_tenure: f_total_revenue_for_tenure,
						revenue_share: f_revenue_share,
						RevenueData: {}
					};
					
					projectWiseRevenue_previous_effrt[i_prac_subPrac_role_level].RevenueData[i_practice] = new yearData(i_year_project);
					
					projectWiseRevenue_previous_effrt[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].Amount = total_revenue;
					projectWiseRevenue_previous_effrt[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].prcnt_allocated = i_no_of_resources;
					projectWiseRevenue_previous_effrt[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].recognized_revenue = f_revenue_recognized;
					projectWiseRevenue_previous_effrt[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].mnth_strt = s_mnth_strt_date;
					projectWiseRevenue_previous_effrt[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].mnth_end = s_mnth_end_date;
				}
				else {
					projectWiseRevenue_previous_effrt[i_prac_subPrac_role_level].total_revenue_for_tenure = f_total_revenue_for_tenure;
					
					projectWiseRevenue_previous_effrt[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].Amount = total_revenue;
					projectWiseRevenue_previous_effrt[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].prcnt_allocated = i_no_of_resources;
					projectWiseRevenue_previous_effrt[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].recognized_revenue = f_revenue_recognized;
					projectWiseRevenue_previous_effrt[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].mnth_strt = s_mnth_strt_date;
					projectWiseRevenue_previous_effrt[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].mnth_end = s_mnth_end_date;
				}
			}
		}
	}
	
	//return projectWiseRevenue_previous_effrt;
}

function find_recognized_revenue_prev(a_revenue_recognized_for_project,practice,subpractice,role,level,month,year,a_prev_subprac_searched_once)
{
	var f_recognized_amount = 0;
	var count =0;
	if(a_prev_subprac_searched_once.indexOf(subpractice) < 0)
	{
		for(var i_index_find=0; i_index_find<a_revenue_recognized_for_project.length; i_index_find++)
		{
			if(practice == a_revenue_recognized_for_project[i_index_find].practice_revenue_recognized)
			{
				if(subpractice == a_revenue_recognized_for_project[i_index_find].subpractice_revenue_recognized)
				{
					//if(role == a_revenue_recognized_for_project[i_index_find].role_revenue_recognized)
					{
						//if(level == a_revenue_recognized_for_project[i_index_find].level_revenue_recognized)
						{
							if(month == a_revenue_recognized_for_project[i_index_find].month_revenue_recognized)
							{
								if(year == a_revenue_recognized_for_project[i_index_find].year_revenue_recognized)
								{
                                  count = f_recognized_amount;
									f_recognized_amount = a_revenue_recognized_for_project[i_index_find].amount_revenue_recognized;
									//nlapiLogExecution('audit','f_recognized_amount:- '+f_recognized_amount);
						f_recognized_amount =parseFloat(f_recognized_amount) +parseFloat(count);
                                  a_prev_subprac_searched_once.push(subpractice);
								}
							}
						}
					}
				}
			}
		}
		
		return f_recognized_amount;
	}
	else
	{
		return 0;
	}
}

function find_recognized_revenue(a_revenue_recognized_for_project,practice,subpractice,role,level,month,year,a_subprac_searched_once,a_subprac_searched_once_month,a_subprac_searched_once_year)
{
	var f_recognized_amount = 0;
	var count =0;
	if(func_search_prac_processed_fr_mnth(a_subprac_searched_once,subpractice,month,year) < 0)
	{
		for(var i_index_find=0; i_index_find<a_revenue_recognized_for_project.length; i_index_find++)
		{				
			if(practice == a_revenue_recognized_for_project[i_index_find].practice_revenue_recognized)
			{
				if(subpractice ==  a_revenue_recognized_for_project[i_index_find].subpractice_revenue_recognized)
				{
					//if(role == a_revenue_recognized_for_project[i_index_find].role_revenue_recognized)
					{
						//if(level == a_revenue_recognized_for_project[i_index_find].level_revenue_recognized)
						{
							if(month == a_revenue_recognized_for_project[i_index_find].month_revenue_recognized)
							{	
								if(parseInt(year) == parseInt(a_revenue_recognized_for_project[i_index_find].year_revenue_recognized))
								{
									count = f_recognized_amount;
									f_recognized_amount = a_revenue_recognized_for_project[i_index_find].amount_revenue_recognized;
									//nlapiLogExecution('audit','f_recognized_amount inside look for prev mnth amnt once pr sub prac:- ',f_recognized_amount);
									a_subprac_searched_once.push(
																{
																	'subpractice': subpractice,
																	'month': month,
																	'year': year
																});
									//a_subprac_searched_once_month.push(month);
									//a_subprac_searched_once_year.push(year);
									f_recognized_amount =parseFloat(f_recognized_amount) +parseFloat(count);
									if(subpractice == 325)
									{
										//nlapiLogExecution('audit','month:- '+month,'year:- '+year);
										nlapiLogExecution('audit','f_recognized_amount:- '+f_recognized_amount);
									}
								}
							}
						}
					}
				}
			}
		}
		
		return f_recognized_amount;
	}
	else
	{
		return 0;
	}
}
function func_search_prac_processed_fr_mnth(a_subprac_searched_once,subpractice,month,year)
{
	var i_return_var = -1;
	for(var i_loop=0; i_loop<a_subprac_searched_once.length; i_loop++)
	{
		if(a_subprac_searched_once[i_loop].subpractice == subpractice)
		{
			if(a_subprac_searched_once[i_loop].month == month)
			{
				if(a_subprac_searched_once[i_loop].year == year)
				{
					i_return_var = i_loop;
					break;
				}
			}
		}
	}
	
	return i_return_var;
}
				
function _logValidation(value) 
{
 if(value != null && value.toString() != null && value != '' && value != undefined && value.toString() != undefined && value != 'undefined' && value.toString() != 'undefined'&& value.toString() != 'NaN' && value != NaN) 
 {
  return true;
 }
 else 
 { 
  return false;
 }
}


function getMonthCompleteName(month){
	var s_mont_complt_name = '';
	if(month == 'Jan')
		s_mont_complt_name = 'January';
	if(month == 'Feb')
		s_mont_complt_name = 'February';
	if(month == 'Mar')
		s_mont_complt_name = 'March';
	if(month == 'Apr')
		s_mont_complt_name = 'April';
	if(month == 'May')
		s_mont_complt_name = 'May';
	if(month == 'Jun')
		s_mont_complt_name = 'June';
	if(month == 'Jul')
		s_mont_complt_name = 'July';
	if(month == 'Aug')
		s_mont_complt_name = 'August';
	if(month == 'Sep')
		s_mont_complt_name = 'September';
	if(month == 'Oct')
		s_mont_complt_name = 'October';
	if(month == 'Nov')
		s_mont_complt_name = 'November';
	if(month == 'Dec')
		s_mont_complt_name = 'December';
	
	return s_mont_complt_name;
}

function getMonthCompleteIndex(month){
	var s_mont_indx = '';
	if(month == 'JAN')
		s_mont_indx = 1;
	if(month == 'FEB')
		s_mont_indx = 2;
	if(month == 'MAR')
		s_mont_indx = 3;
	if(month == 'APR')
		s_mont_indx = 4;
	if(month == 'MAY')
		s_mont_indx = 5;
	if(month == 'JUN')
		s_mont_indx = 6;
	if(month == 'JUL')
		s_mont_indx = 7;
	if(month == 'AUG')
		s_mont_indx = 8;
	if(month == 'SEP')
		s_mont_indx = 9;
	if(month == 'OCT')
		s_mont_indx = 10;
	if(month == 'NOV')
		s_mont_indx = 11;
	if(month == 'DEC')
		s_mont_indx = 12;
	
	return s_mont_indx;
}

function removeCommas(str) {
    while (str.search(",") >= 0) {
        str = (str + "").replace(',', '');
    }
    return str;
};

function getProject_internal_ID(pro_id){
	try{
		var id = '';
		var a_project_filtr = [['entityid', 'is', pro_id]];
		var cols = [];
			cols.push(new nlobjSearchColumn('internalid'));
			cols.push(new nlobjSearchColumn('companyname'));
		var a_project_search = searchRecord('job', null, a_project_filtr, cols);
		if(_logValidation(a_project_search)){
			id = a_project_search[0].getValue('internalid');
		}
		return id;
	}
	catch(e){
		nlapiLogExecution('ERROR','Get Project Internal ID Function',e);
	}
}