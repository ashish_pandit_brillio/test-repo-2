/**
 * @param {nlobjRequest}
 *            request Request object
 * @param {nlobjResponse}
 *            response Response object
 * @returns {Void} Any output is written via response object
 */
function scheduled(dataIn) {

	nlapiLogExecution('AUDIT', 'Report Run', '');

	try {
		

		 {
			 var context = nlapiGetContext();

				var d_today = new Date();
			 //	var d_today = '1/1/2018';
			 	//d_today = nlapiStringToDate(d_today);
			 
				var error = false;
				var dataRow = [];
				//Conversion Rates table
				var inr_to_usd = 0;
				var gbp_to_usd = 0;
				var eur_to_usd = 0;
				/*var a_conversion_rate_table = nlapiSearchRecord('customrecord_conversion_rate_sfdc',null,null,[new nlobjSearchColumn('custrecord_rate_inr_to_usd'),
				                                                              new nlobjSearchColumn('custrecord_rate_gbp_to_usd'),
				                                                              new nlobjSearchColumn('custrecord_rate_eur_to_usd'),
				                                                              new nlobjSearchColumn('internalid').setSort(true)]);
				if(a_conversion_rate_table)
					inr_to_usd = 1 / parseFloat(a_conversion_rate_table[0].getValue('custrecord_rate_inr_to_usd'));
					gbp_to_usd = a_conversion_rate_table[0].getValue('custrecord_rate_gbp_to_usd');
					eur_to_usd = a_conversion_rate_table[0].getValue('custrecord_rate_eur_to_usd');*/
				
				for (var i = 0; i < 12; i++) {
					var d_day = nlapiAddMonths(d_today, i);
					yieldScript(context);
					var d_month_start = nlapiAddDays(d_day, -1 * (d_day.getDate() - 1));
					var s_month_start = d_month_start.getMonth() + 1 + '/'
					        + d_month_start.getDate() + '/' + d_month_start.getFullYear();
					var d_month_end = nlapiAddDays(nlapiAddMonths(d_month_start, 1), -1);
					var s_month_end = d_month_end.getMonth() + 1 + '/'
					        + d_month_end.getDate() + '/' + d_month_end.getFullYear();
					try {
						var JSON_Obj = {};
						var o_total_array = [];
						var a_data = get_data(s_month_start, s_month_end,
						        d_month_start, d_month_end);
						
						for(var indx=0;indx<a_data.length;indx++){
							var s_local = a_data[indx];
							
							
							JSON_Obj = {
								//	'Employee': s_local.custpage_employee,	
									 'Vertical': s_local.custpage_vertical,
									 'Department': s_local.custpage_employee_department,
									 'ParentDepartment': s_local.custpage_employee_parent_department,
									 'ProjectID': s_local.custpage_project_id,
									 'ProjectName': s_local.custpage_project,
									// 'CustomerID': s_local.custpage_customer_id,
									 'CustomerName': s_local.custpage_customer,
									 'Amount': s_local.custpage_total_amount,
									 'ProjectCurrency': s_local.custpage_currency
							};
							o_total_array.push(JSON_Obj);
						}
						/*exportToXml(a_data, 'T', d_month_start.getMonth() + 1, d_month_end
						        .getFullYear());*/
						dataRow.push({
							'MonthStart': s_month_start,
							'MonthEnd': s_month_end,
							'Data': o_total_array
						});
					} catch (e) {
						nlapiLogExecution('ERROR', 'Error: ', e.message);
						error = true;
					}
				}
			/*var s_from_date = request.getParameter('custpage_from_date');
			var d_from_date = nlapiStringToDate(s_from_date, 'datetimetz');
			

			var s_to_date = request.getParameter('custpage_to_date');
			var d_to_date = nlapiStringToDate(s_to_date, 'datetimetz');

			var i_working_days = parseInt(calcBusinessDays(d_from_date,
			        d_to_date));


			var a_data = getRevenueData(s_from_date,
			        s_to_date, d_from_date, d_to_date);


			if (mode == 'excel') {
				exportToExcel(a_data, chk_show_ot_hours);
				return;// response.write('Test')
		}*/
		 }
		nlapiLogExecution('debug', 'response', JSON.stringify(dataRow));
      return dataRow;
		

	} catch (err) {
		nlapiLogExecution('ERROR', 'main', err);
		 throw err;
	}
}

function get_data(s_month_start, s_month_end,
        d_month_start, d_month_end){
	try{
	// Store resource allocations for project and employee
	var a_resource_allocations = new Array();
	var filter = [];
	filter.push(new nlobjSearchFilter('custrecord_forecast_month_startdate',null,'on',d_month_start));
	filter.push(new nlobjSearchFilter('custrecord_forecast_month_enddate',null,'on',d_month_end));
	
	var cols = [];
	cols.push(new nlobjSearchColumn('custrecord_forecast_month_startdate',null,'group'));
	cols.push(new nlobjSearchColumn('custrecord_forecast_month_enddate',null,'group'));
	cols.push(new nlobjSearchColumn('custrecord_forecast_vertical',null,'group'));
	cols.push(new nlobjSearchColumn('custrecord_forecast_department',null,'group'));
	cols.push(new nlobjSearchColumn('custrecord_forecast_parent_department',null,'group'));
	cols.push(new nlobjSearchColumn('custrecord_forecast_project_id',null,'group'));
	cols.push(new nlobjSearchColumn('custrecord_forecast_project_name',null,'group'));
	cols.push(new nlobjSearchColumn('custrecord_forecast_customer',null,'group'));
	cols.push(new nlobjSearchColumn('custrecord_forecast_amount',null,'sum'));
	cols.push(new nlobjSearchColumn('custrecord_forecast_project_currency',null,'group'));
	cols.push(new nlobjSearchColumn('custrecord_forecast_employee',null,'group'));
	
	var search_rec = searchRecord('customrecord_forecast_master_data',null,filter,cols);
	if(search_rec){
		for(var indx=0;indx<search_rec.length;indx++){
			a_resource_allocations[indx] = {
				   
				    'custpage_employee' : search_rec[indx].getValue('custrecord_forecast_employee',null,'group'),
				    'custpage_vertical' : search_rec[indx].getText('custrecord_forecast_vertical',null,'group'),
				    'custpage_employee_department' : search_rec[indx].getText('custrecord_forecast_department',null,'group'),
				    'custpage_employee_parent_department' : search_rec[indx].getValue('custrecord_forecast_parent_department',null,'group'),
				    'custpage_project_id' : search_rec[indx].getValue('custrecord_forecast_project_id',null,'group'),
				    'custpage_project' : search_rec[indx].getText('custrecord_forecast_project_name',null,'group'),
				    'custpage_customer' : search_rec[indx].getText('custrecord_forecast_customer',null,'group'),
				    'custpage_total_amount' : search_rec[indx].getValue('custrecord_forecast_amount',null,'sum'),
				    'custpage_currency' : search_rec[indx].getText('custrecord_forecast_project_currency',null,'group')
				    
				    
				};
		}
		
	}
	
	}
	catch(e){
		nlapiLogExecution('DEBUG','Data Search Error',e);
		throw e;
	}
	return a_resource_allocations;
}


function yieldScript(currentContext) {

	if (currentContext.getRemainingUsage() <= 100) {
		nlapiLogExecution('AUDIT', 'API Limit Exceeded');
		var state = nlapiYieldScript();

		if (state.status == "FAILURE") {
			nlapiLogExecution('ERROR', 'Yield Script Failed', 'Reason : '
			        + state.reason + ' / Size : ' + state.size);
			return false;
		} else if (state.status == "RESUME") {
			nlapiLogExecution('AUDIT', 'Script Resumed');
		}
	}
}
