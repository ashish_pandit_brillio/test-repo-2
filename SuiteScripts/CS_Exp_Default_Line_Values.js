/**
 * Set default value of the project and currency in lines
 * 
 * Version Date Author Remarks
 * 
 * 1.00 11 Jul 2016 Nitish Mishra
 * 
 */

function defaultValuesClientFieldChange(type, name, linenum) {
	var context = nlapiGetContext();
	if (context.getRoleCenter() == "EMPLOYEE") {

		if (type == 'expense' && name == 'amount') {

			var defaultProject = nlapiGetFieldValue('custpage_default_project');
			var defaultCurrency = nlapiGetFieldValue('custpage_default_currency');

			if (defaultProject) {
				nlapiSetCurrentLineItemValue('expense', 'customer',
				        defaultProject);
			}

			if (defaultCurrency) {
				nlapiSetCurrentLineItemValue('expense', 'currency',
				        defaultCurrency);
			}
		}
	}
}
