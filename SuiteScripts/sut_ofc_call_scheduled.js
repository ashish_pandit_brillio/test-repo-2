/**
 *@NApiVersion 2.x
 *@NScriptType Suitelet
 */
define(['N/ui/serverWidget', 'N/task','N/record','N/redirect'],
    function(ui, task,record,redirect) {
        function ofcCallScheduled(context) {
            log.debug({
                title: "Inside Get request",
                details: JSON.stringify(context.request.parameters)
            });
            try{

            
            var parameters = context.request.parameters;
            var i_mode = parameters['i_mode'];
            var i_recordID = parameters['i_rec_id'];
            var s_rec_type = parameters['s_rec_type'];
            log.debug('Params', i_mode + ', ' + i_recordID + ', ' + s_rec_type);

            var scheduledScriptTask = task.create({
                taskType: task.TaskType.SCHEDULED_SCRIPT
            });

            var data_parameters = {
                'custscript_ofc_mode': i_mode,
                'custscript_ofc_rec_id': i_recordID,
                'custscript_ofc_rec_type': s_rec_type
            };
            scheduledScriptTask.scriptId = 2248;//2331;
            scheduledScriptTask.params = data_parameters;
            scheduledScriptTask.deploymentId = 'customdeploy_sch_ofc_valid_create_delete';
            var scheduledId = scheduledScriptTask.submit();
            
            var taskStatus = task.checkStatus(scheduledId);
            log.debug('taskStatus',taskStatus);
            if (taskStatus.status === 'QUEUED' || taskStatus.status === 'PENDING') {
                var id = record.submitFields({
                    type: 'customrecord_ofc_cost_for_credit_amount',
                    id: i_recordID,
                    values: {
                    custrecord_ofc_current_status: 2
                    },
                    options: {
                    enableSourcing: false,
                    ignoreMandatoryFields : true
                    }
                   });
                   log.debug('id',id);
                
               // nlapiSubmitField('customrecord_je_upload',i_recordID,'custrecord_status_je_upload',2);
                // var message = '<html>';
                // message += '<head>';
                // message += "<meta http-equiv=\"refresh\" content=\"5\" \/>";
                // message += '<meta charset="utf-8" />';
                // message += '</head>';
                // message += '<body>';
                // var i_counter = '0%'
                // message += "<div id=\"my-progressbar-container\">";
                // message += "            ";
                // message += "<div class=\"ajax-content\" id=\"test14\" name=\"test14\" style='height:50%;width:50%;' align='left' valign='top'>"; //opacity: .75;
                // message += "<img src='https://system.na1.netsuite.com/core/media/media.nl?id=190162&c=3883006&h=1cf2a9f8bd8b922539c8' align='left' style='height:50%;width:50%;align=left;'/>";
                // message += "        <\/div>";
                // message += '</body>';
                // message += '</html>';

                // var loadRecord =  record.load({
                //     type: 'customrecord_ofc_cost_for_credit_amount',
                //     id : i_recordID,
                //     isDynamic: true
                //    }); 

                //    loadRecord.setValue({
                //     fieldId: 'custrecord_ofc_progress_bar',
                //     value: message,
                //     ignoreFieldChange: true
                //    });
              
                //    var recordId = loadRecord.save({
                //      enableSourcing: false,
                //      ignoreMandatoryFields: false
                //      });
              
                //     log.debug('savedRecord',recordId);

                   redirect.toRecord({
                    type: 'customrecord_ofc_cost_for_credit_amount',
                    id: i_recordID
                    });
               
               // nlapiSetFieldValue('custrecord_progress_bar_je_upload',message);



            }
        }catch(e){
            log.debug('error',e);
        }

        }

        return {
            onRequest: ofcCallScheduled
        };
    });