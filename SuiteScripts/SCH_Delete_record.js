//https://debugger.na1.netsuite.com/app/common/scripting/script.nl?id=1109
function deleteFunction(){
try{
var search = searchRecord('transaction','customsearch2601',null,null);
 //,'customsearch2196'customsearch2590 customsearch2589
nlapiLogExecution('debug','Record search',search.length);
if(search){
for(var i=0;i<search.length;i++){
var id = search[i].getId();
 var context = nlapiGetContext();
	if(context.getRemainingUsage() > 1000)
	{
			nlapiYieldScript();
	}
nlapiDeleteRecord('vendorbill',id);
nlapiLogExecution('debug','Record Deleted Count',i);

}
}
}
catch(e){
nlapiLogExecution('debug','Error in Deletion',e);
}
}

function helper()
{
    nlapiSetRecoveryPoint();
}

function yieldScript(currentContext) {

	if (currentContext.getRemainingUsage() <= 1000) {
		nlapiLogExecution('AUDIT', 'API Limit Exceeded');
		var state = nlapiYieldScript();

		if (state.status == "FAILURE") {
			nlapiLogExecution('ERROR', 'Yield Script Failed', 'Reason : '
			        + state.reason + ' / Size : ' + state.size);
			return false;
		} else if (state.status == "RESUME") {
			nlapiLogExecution('AUDIT', 'Script Resumed');
		}
	}
}