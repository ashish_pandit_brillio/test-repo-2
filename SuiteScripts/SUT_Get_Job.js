/**
 * @author Shweta
 */
// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
/*
   	Script Name : SUT_PR_Validations.js
	Author      :
	Company     :
	Date        :
	Description :


	Script Modification Log:

	-- Date --			-- Modified By --				--Requested By--				-- Description --



Below is a summary of the process controls enforced by this script file.  The control logic is described
more fully, below, in the appropriate function headers and code blocks.


     SUITELET
		- suiteletFunction(request, response)


     SUB-FUNCTIONS
		- The following sub-functions are called by the above core functions in order to maintain code
            modularization:

               - NOT USED

*/
}
// END SCRIPT DESCRIPTION BLOCK  ====================================



// BEGIN GLOBAL VARIABLE BLOCK  =====================================
{
	//  Initialize any Global Variables, in particular, debugging variables...




}
// END GLOBAL VARIABLE BLOCK  =======================================





// BEGIN SUITELET ==================================================
/**
 * 
 * @param {Object} request
 * @param {Object} response
 * 
 * Description --> For the employee record , get the 
 */
function suitelet_Get_Job(request, response)//
{
	var results = new Array();
	var i_employeeID;
	
	try //
	{
		if (request.getMethod() == 'GET') //
		{
			var project_ID = request.getParameter('custscript_project_id');
			nlapiLogExecution('DEBUG', ' suiteletFunction', ' project_ID -->' + project_ID);
			
			if (_logValidation(project_ID)) //
			{
				var o_projectOBJ = nlapiLoadRecord('job', project_ID)
				
				if (_logValidation(project_ID)) //
				{
					var vertical_ID = o_projectOBJ.getFieldValue('custentity_vertical')
					nlapiLogExecution('DEBUG', 'fieldChanged_PR_validations', ' vertical_ID -->' + vertical_ID);
                    
                    if (_logValidation(vertical_ID))//
                    {
                        
                    }
                    else
                    {
                        vertical_ID = "Vertical Not Selected on Project.";
                    }
					results.push(vertical_ID)
				}
			}
		}
	} 
	catch (exception) //
	{
		nlapiLogExecution('DEBUG', 'ERROR', ' Exception Caught -->' + exception);
	}//CATCH
	
	response.write(vertical_ID);
}

// END SUITELET ====================================================




// BEGIN OBJECT CALLED/INVOKING FUNCTION ===================================================

function _logValidation(value) //
{
 if (value != null && value.toString() != null && value != '' && value != undefined && value.toString() != undefined && value != 'undefined' && value.toString() != 'undefined' && value.toString() != 'NaN' && value != NaN) // 
	{
		return true;
	}
	else //
	{
		return false;
	}
}

// END OBJECT CALLED/INVOKING FUNCTION =====================================================
