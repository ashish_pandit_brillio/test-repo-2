/**
 * List view for the expenses for employees
 * 
 * Version Date Author Remarks 1.00 Mar 31 2015 Nitish Mishra
 * 
 */

/**
 * @param {nlobjRequest}
 *        request Request object
 * @param {nlobjResponse}
 *        response Response object
 * @returns {Void} Any output is written via response object
 */
function suitelet(request, response) {

	try {

		if (request.getParameter("mode") == "Export") {
			exportExpenseList(request);
		}
		else {
			createExpenseListForm(request);
		}

	}
	catch (err) {
		nlapiLogExecution('ERROR', 'suitelet', err);
	}
}

function createExpenseListForm(request) {

	try {
		var form = nlapiCreateForm('List Of Expenses', false);

		nlapiLogExecution('debug', 'expense', 'B');
		form.addButton('custpage_export_to_csv', 'Export To CSV', 'exportToCsv');
		form.setScript('customscript_cs_expense_list');

		var expenseDetails = getExpenseDetails();
		nlapiLogExecution('debug', 'expense', 'A');
		var expense_sublist = form.addSubList('custpage_expenses', 'list', 'Expenses');

		// expense_sublist.addField('edit_view', 'text', 'Edit|View');
		expense_sublist.addField('trandate', 'date', 'Date');
		// expense_sublist.addField('print', 'text', 'Print');
		expense_sublist.addField('tranid', 'text', 'Expense Number');
		expense_sublist.addField('project', 'text', 'Project');
		expense_sublist.addField('amount', 'currency', 'Amount');
		expense_sublist.addField('currency', 'text', 'Currency');
		expense_sublist.addField('status', 'text', 'Status');
		expense_sublist.addField('memo', 'text', 'Memo');
		expense_sublist.setLineItemValues(expenseDetails);

		nlapiLogExecution('debug', 'expense', 'B');

		response.writePage(form);
	}
	catch (err) {
		nlapiLogExecution('ERROR', 'createExpenseListForm', err);
		throw err;
	}
}

function getExpenseDetails() {

	try {

		var filters =
				[
					new nlobjSearchFilter('mainline', null, 'is', 'F'),
					new nlobjSearchFilter('type', null, 'anyof', 'ExpRept'),
					new nlobjSearchFilter('entity', null, 'anyof', nlapiGetUser())
				];

		var expenses = [];
		var expenseDetails = [];

		// var printUrl =
		// 'https://system.na1.netsuite.com/app/accounting/transactions/exprept.nl?print=T&id=';
		// var editUrl =
		// "https://system.na1.netsuite.com/app/accounting/transactions/exprept.nl?e=T&id=";
		// var viewUrl =
		// "https://system.na1.netsuite.com/app/accounting/transactions/exprept.nl?id=";

		searchRecord(
				'expensereport',
				null,
				filters,
				[
					new nlobjSearchColumn('fxamount', null, 'sum'),
					new nlobjSearchColumn('name', null, 'group'),
					new nlobjSearchColumn('currency', null, 'group'),
					new nlobjSearchColumn('mainname', null, 'group'),
					new nlobjSearchColumn('memomain', null, 'group'),
					new nlobjSearchColumn('transactionnumber', null, 'group'),
					new nlobjSearchColumn('statusref', null, 'group'),
					new nlobjSearchColumn('internalid', null, 'group').setSort(),
					new nlobjSearchColumn('custbody_transactiondate', null, 'group'),
				]).forEach(function(expense) {

			var internalId = expense.getValue('internalid', null, 'group');
			var memo = expense.getValue('memomain', null, 'group');
			var amount = expense.getValue('fxamount', null, 'sum');

			if (amount < 0) {
				return false;
			}

			// var currency = expense.getValue('currency', null,
			// 'group');
			// var currencySymbol =
			// (currency == "6") ? "₹" : (currency == "1" ? "$" :
			// currency);
			// var edit_view =
			// "<a style='color:#255599;text-decoration:none;' href='" +
			// editUrl
			// + internalId + "'>Edit</a> | "
			// + "<a style='color:#255599;text-decoration:none;' href='"
			// + viewUrl + internalId + "'>View</a>";

			expenses.push(internalId);
			expenseDetails.push({
				employee : expense.getText('mainname', null, 'group'),
				project : expense.getText('name', null, 'group'),
				tranid : expense.getValue('transactionnumber', null, 'group'),
				internalid : internalId,
				status : expense.getText('statusref', null, 'group'),
				amount : Math.abs(expense.getValue('fxamount', null, 'sum')),// + "
				// "
				// + currencySymbol,
				trandate : expense.getValue('custbody_transactiondate', null, 'group'),
				currency : expense.getText('currency', null, 'group'),
				// edit_view : edit_view,
				// print : "<a
				// style='color:#255599;text-decoration:none;' href='" +
				// printUrl
				// + internalId + "' target='_Blank'>Print</a>",
				memo : memo == "- None -" ? "" : memo
			});
		});

		nlapiLogExecution('debug', 'expense length A', expenseDetails.length);
		return expenseDetails;
	}
	catch (err) {
		nlapiLogExecution('ERROR', 'getExpenseDetails', err);
		throw err;
	}
}

function exportExpenseList(request) {

	try {

		var expenseDetails = getExpenseDetails();

		var csvString = "Date,Expense Number,Project,Amount,Currency,Status,Memo\n";

		expenseDetails.forEach(function(expense) {

			csvString +=
					expense.trandate + "," + expense.tranid + "," + expense.project + ","
							+ expense.amount + "," + expense.currency + "," + expense.status + ","
							+ expense.memo + "\n";
		});


		nlapiLogExecution('debug', 'file created');

		var userName = nlapiGetContext().getName();
		var filename = 'Expense List ( ' + userName + ").csv";
		var file = nlapiCreateFile(filename, 'CSV', csvString);
		response.setContentType('CSV', filename);
		response.write(file.getValue());
	}
	catch (err) {
		nlapiLogExecution('ERROR', 'exportExpenseList', err);
		throw err;
	}
}

function exportToCsv() {

	try {
		var link =
				nlapiResolveURL('SUITELET', 'customscript_sut_expense_list_employee',
						'customdeploy_sut_expense_list_emp')
						+ "&mode=Export";

		var win = window.open(link);
		win.focus();
	}
	catch (err) {
		alert(JSON.stringify(err));
	}
}