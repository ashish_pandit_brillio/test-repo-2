function suitelet_funct(request, response)
{
	try
	{
		var filters_emp_data = [[ 'custrecord_emp_practice_daily_ticker', 'anyof', parseInt(458) ], 'and',
												[ 'custrecord_date_of_dump', 'on', nlapiDateToString('11/15/2016')]];
												
		var columns = new Array();
		columns[0] = new nlobjSearchColumn('formulanumeric',null,'sum');
		columns[0].setFormula("TO_NUMBER({custrecord_prcnt_allocated_daily_ticker})");
		var emp_data_rcrd_srch = searchRecord('customrecord_daily_emp_head_count_detail', null, filters_emp_data, columns);
		if (emp_data_rcrd_srch)
		{
			var billed_count_last_day = emp_data_rcrd_srch[0].getValue('formulanumeric',null,'sum');
			nlapiLogExecution('audit','billed_count_last_day',billed_count_last_day);
		}
			
	}
	catch(err)
	{
		nlapiLogExecution('error','error message:--',err);
	}
}

function getDatediffIndays(startDate, endDate)
{
    var one_day=1000*60*60*24;
    var fromDate = startDate;
    var toDate = endDate;

	var date1 = nlapiStringToDate(fromDate);

    var date2 = nlapiStringToDate(toDate);

    var date3=Math.round((date2-date1)/one_day);

    return (date3+1);
}

function _logValidation(value) 
{
 if(value!='null' && value != null && value != null && value != '' && value != undefined && value != undefined && value != 'undefined' && value!= 'undefined'&& value!= 'NaN' && value != NaN) 
 {
  return true;
 }
 else 
 { 
  return false;
 }
}


function get_current_month_start_date(i_month,i_year)
{
   var d_start_date = '';
   var d_end_date = '';
	
   var date_format = checkDateFormat();	 
	
 if (_logValidation(i_month) && _logValidation(i_year)) 
 {
 	  if(i_month == 'Jan' || i_month == 'January' || i_month == 'JAN' || i_month == 'JANUARY')
	  {	  	
		 if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 1 + '-' + 1;		
			d_end_date = i_year + '-' + 1 + '-' + 31;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 1 + '/' + i_year;
			d_end_date = 31 + '/' + 1 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 1 + '/' + 1 + '/' + i_year;
			d_end_date = 1 + '/' + 31 + '/' + i_year;
		 }	 
				
	  }	
	  else if(i_month == 'Feb' || i_month == 'February' || i_month == 'FEB' || i_month == 'FEBRUARY')
	  {
	  	if((i_year%2) == 0)
		{
			i_day = 29;
		}
		else
		{
			i_day = 28;
		}
			
		 if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 2 + '-' + 1;		
			d_end_date = i_year + '-' + 2 + '-' + i_day;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 2 + '/' + i_year;
			d_end_date = i_day + '/' + 2 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 2 + '/' + 1 + '/' + i_year;
			d_end_date = 2 + '/' + i_day + '/' + i_year;
		 }	 
			
	  }		
	  else if(i_month == 'Mar' || i_month == 'March' || i_month == 'MAR' || i_month == 'MARCH')
	  {	  
	     if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 3 + '-' + 1;		
			d_end_date = i_year + '-' + 3 + '-' + 31;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 3 + '/' + i_year;
			d_end_date = 31 + '/' + 3 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 3 + '/' + 1 + '/' + i_year;
			d_end_date = 3 + '/' + 31 + '/' + i_year;
		 }	 
	
	  }	
	  else if(i_month == 'Apr' || i_month == 'April' || i_month == 'APR' || i_month == 'APRIL')
	  {
	  	if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 4 + '-' + 1;		
			d_end_date = i_year + '-' + 4 + '-' + 30;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 4 + '/' + i_year;
			d_end_date = 30 + '/' + 4 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 4 + '/' + 1 + '/' + i_year;
			d_end_date = 4 + '/' + 30 + '/' + i_year;
	     }	 
	 
	
	  }	
	  else if(i_month == 'May' || i_month == 'MAY')
	  {	  
		 if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 5 + '-' + 1;		
			d_end_date = i_year + '-' + 5 + '-' + 31;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 5 + '/' + i_year;
			d_end_date = 31 + '/' + 5 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 5 + '/' + 1 + '/' + i_year;
			d_end_date = 5 + '/' + 31 + '/' + i_year;
	     }	 
	
	  }	  
	  else if(i_month == 'June' || i_month == 'Jun' || i_month == 'JUN' || i_month == 'JUNE')
	  {	 
		 if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 6 + '-' + 1;		
			d_end_date = i_year + '-' + 6 + '-' + 30;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 6 + '/' + i_year;
			d_end_date = 30 + '/' + 6 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 6 + '/' + 1 + '/' + i_year;
			d_end_date = 6 + '/' + 30 + '/' + i_year;
	     }	 
			
	  }	
	  else if(i_month == 'Jul' || i_month == 'July' || i_month == 'JUL' || i_month == 'JULY')
	  {	  
		 if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 7 + '-' + 1;		
			d_end_date = i_year + '-' + 7 + '-' + 31;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 7 + '/' + i_year;
			d_end_date = 31 + '/' + 7 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 7 + '/' + 1 + '/' + i_year;
			d_end_date = 7 + '/' + 31 + '/' + i_year;
	     }	 
		
	  }	
	  else if(i_month == 'August' || i_month == 'Aug' || i_month == 'AUG' || i_month == 'AUGUST')
	  {	  
		 if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 8 + '-' + 1;		
			d_end_date = i_year + '-' + 8 + '-' + 31;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 8 + '/' + i_year;
			d_end_date = 31 + '/' + 8 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 8 + '/' + 1 + '/' + i_year;
			d_end_date = 8 + '/' + 31 + '/' + i_year;
	     }	 
		
	  }  
	  else if(i_month == 'Sep' || i_month == 'September' || i_month == 'SEP' || i_month == 'SEPTEMBER')
	  {  	
	     if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 9 + '-' + 1;		
			d_end_date = i_year + '-' + 9 + '-' + 30;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 9 + '/' + i_year;
			d_end_date = 30 + '/' + 9 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 9 + '/' + 1 + '/' + i_year;
			d_end_date = 9 + '/' + 30 + '/' + i_year;
	     }	 
	
	  }	
	  else if(i_month == 'Oct' || i_month == 'October' || i_month == 'OCT' || i_month == 'OCTOBER')
	  {	 	
		if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 10 + '-' + 1;		
			d_end_date = i_year + '-' + 10 + '-' + 31;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 10 + '/' + i_year;
			d_end_date = 31 + '/' + 10 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 10 + '/' + 1 + '/' + i_year;
			d_end_date = 10 + '/' + 31 + '/' + i_year;
	     }		
	  }	
	  else if(i_month == 'Nov' || i_month == 'November' || i_month == 'NOV' || i_month == 'NOVEMBER')
	  {	  
		if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 11 + '-' + 1;		
			d_end_date = i_year + '-' + 11 + '-' + 30;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 11 + '/' + i_year;
			d_end_date = 30 + '/' + 11 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 11 + '/' + 1 + '/' + i_year;
			d_end_date = 11 + '/' + 30 + '/' + i_year;
	     }	 
			
	  }	
	  else if(i_month == 'Dec' || i_month == 'December' || i_month == 'DEC' || i_month == 'DECEMBER')
	  {	 
		if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 12 + '-' + 1;		
			d_end_date = i_year + '-' + 12 + '-' + 31;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 12 + '/' + i_year;
			d_end_date = 31 + '/' + 12 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 12 + '/' + 1 + '/' + i_year;
			d_end_date = 12 + '/' + 31 + '/' + i_year;
	     }	 
			
	  }	
 }//Month & Year
 return d_start_date;
}


function get_current_month_end_date(i_month,i_year)
{
	var d_start_date = '';
	var d_end_date = '';
	
	var date_format = checkDateFormat();
	 	
 if (_logValidation(i_month) && _logValidation(i_year)) 
 {
 	  if(i_month == 'Jan' || i_month == 'January' || i_month == 'JAN' || i_month == 'JANUARY')
	  {	  	
		 if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 1 + '-' + 1;		
			d_end_date = i_year + '-' + 1 + '-' + 31;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 1 + '/' + i_year;
			d_end_date = 31 + '/' + 1 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 1 + '/' + 1 + '/' + i_year;
			d_end_date = 1 + '/' + 31 + '/' + i_year;
		 }	 
				
	  }	
	  else if(i_month == 'Feb' || i_month == 'February' || i_month == 'FEB' || i_month == 'FEBRUARY')
	  {
	  	if((i_year%2) == 0)
		{
			i_day = 29;
		}
		else
		{
			i_day = 28;
		}
			
		 if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 2 + '-' + 1;		
			d_end_date = i_year + '-' + 2 + '-' + i_day;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 2 + '/' + i_year;
			d_end_date = i_day + '/' + 2 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 2 + '/' + 1 + '/' + i_year;
			d_end_date = 2 + '/' + i_day + '/' + i_year;
		 }	 
			
	  }		
	  else if(i_month == 'Mar' || i_month == 'March' || i_month == 'MAR' || i_month == 'MARCH')
	  {	  
	     if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 3 + '-' + 1;		
			d_end_date = i_year + '-' + 3 + '-' + 31;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 3 + '/' + i_year;
			d_end_date = 31 + '/' + 3 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 3 + '/' + 1 + '/' + i_year;
			d_end_date = 3 + '/' + 31 + '/' + i_year;
		 }	 
	
	  }	
	  else if(i_month == 'Apr' || i_month == 'April' || i_month == 'APR' || i_month == 'APRIL')
	  {
	  	if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 4 + '-' + 1;		
			d_end_date = i_year + '-' + 4 + '-' + 30;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 4 + '/' + i_year;
			d_end_date = 30 + '/' + 4 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 4 + '/' + 1 + '/' + i_year;
			d_end_date = 4 + '/' + 30 + '/' + i_year;
	     }	 
	 
	
	  }	
	  else if(i_month == 'May' || i_month == 'MAY')
	  {	  
		 if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 5 + '-' + 1;		
			d_end_date = i_year + '-' + 5 + '-' + 31;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 5 + '/' + i_year;
			d_end_date = 31 + '/' + 5 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 5 + '/' + 1 + '/' + i_year;
			d_end_date = 5 + '/' + 31 + '/' + i_year;
	     }	 
	
	  }	  
	  else if(i_month == 'June' || i_month == 'Jun' || i_month == 'JUN' || i_month == 'JUNE')
	  {	 
		 if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 6 + '-' + 1;		
			d_end_date = i_year + '-' + 6 + '-' + 30;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 6 + '/' + i_year;
			d_end_date = 30 + '/' + 6 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 6 + '/' + 1 + '/' + i_year;
			d_end_date = 6 + '/' + 30 + '/' + i_year;
	     }	 
			
	  }	
	  else if(i_month == 'Jul' || i_month == 'July' || i_month == 'JUL' || i_month == 'JULY')
	  {	  
		 if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 7 + '-' + 1;		
			d_end_date = i_year + '-' + 7 + '-' + 31;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 7 + '/' + i_year;
			d_end_date = 31 + '/' + 7 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 7 + '/' + 1 + '/' + i_year;
			d_end_date = 7 + '/' + 31 + '/' + i_year;
	     }	 
		
	  }	
	  else if(i_month == 'August' || i_month == 'Aug' || i_month == 'AUG' || i_month == 'AUGUST')
	  {	  
		 if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 8 + '-' + 1;		
			d_end_date = i_year + '-' + 8 + '-' + 31;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 8 + '/' + i_year;
			d_end_date = 31 + '/' + 8 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 8 + '/' + 1 + '/' + i_year;
			d_end_date = 8 + '/' + 31 + '/' + i_year;
	     }	 
		
	  }  
	  else if(i_month == 'Sep' || i_month == 'September' || i_month == 'SEP' || i_month == 'SEPTEMBER')
	  {  	
	     if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 9 + '-' + 1;		
			d_end_date = i_year + '-' + 9 + '-' + 30;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 9 + '/' + i_year;
			d_end_date = 30 + '/' + 9 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 9 + '/' + 1 + '/' + i_year;
			d_end_date = 9 + '/' + 30 + '/' + i_year;
	     }	 
	
	  }	
	  else if(i_month == 'Oct' || i_month == 'October' || i_month == 'OCT' || i_month == 'OCTOBER')
	  {	 	
		if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 10 + '-' + 1;		
			d_end_date = i_year + '-' + 10 + '-' + 31;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 10 + '/' + i_year;
			d_end_date = 31 + '/' + 10 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 10 + '/' + 1 + '/' + i_year;
			d_end_date = 10 + '/' + 31 + '/' + i_year;
	     }	 
	
		
	  }	
	  else if(i_month == 'Nov' || i_month == 'November' || i_month == 'NOV' || i_month == 'NOVEMBER')
	  {	  
		if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 11 + '-' + 1;		
			d_end_date = i_year + '-' + 11 + '-' + 30;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 11 + '/' + i_year;
			d_end_date = 30 + '/' + 11 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 11 + '/' + 1 + '/' + i_year;
			d_end_date = 11 + '/' + 30 + '/' + i_year;
	     }	 
			
	  }	
	  else if(i_month == 'Dec' || i_month == 'December' || i_month == 'DEC' || i_month == 'DECEMBER')
	  {	 
		if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 12 + '-' + 1;		
			d_end_date = i_year + '-' + 12 + '-' + 31;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 12 + '/' + i_year;
			d_end_date = 31 + '/' + 12 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 12 + '/' + 1 + '/' + i_year;
			d_end_date = 12 + '/' + 31 + '/' + i_year;
	     }	 
			
	  }	
 }//Month & Year
 return d_end_date;
}

function checkDateFormat()
{
    var context = nlapiGetContext();

	var dateFormatPref = context.getPreference('dateformat');

    return dateFormatPref;
}

function send_mail(proj_data,billing_from_date,practice_arr_list)
{
	if (_logValidation(proj_data))
	{
		var today = new Date();
		var dd = today.getDate();
		var mm = today.getMonth()+1;
		var yyyy = today.getFullYear();
		var hours = today.getHours();
		var minutes = today.getMinutes();
		
		if(dd<10){
		    dd='0'+dd
		} 
		if(mm<10){
		    mm='0'+mm
		} 
		var today = dd+'/'+mm+'/'+yyyy;
		
		var total_billable_head = 0;
		var total_unbillable_head = 0;
		var Total_billable_head_onsite = 0;
		var Total_billabel_head_offsite = 0;
		var Total_unbillable_head_onsite = 0;
		var Total_unbillable_head_offsite = 0;
		var total_billable_head_allo = 0;
		
		nlapiLogExecution('audit','prac len:- ',practice_arr_list.length);
		for(var parent_index=0; parent_index<practice_arr_list.length; parent_index++)
		{
			var prent_practice_id = practice_arr_list[parent_index].internal_id;
			var parent_child_group = new Array();
			var group_unque_list = new Array();
			var sr_group = 0;
			var bill_onsite = 0;
			var bill_offsite = 0;
			var unbill_onsite = 0;
			var unbill_offsite = 0;
			
			for(var child_index=0; child_index<proj_data.length; child_index++)
			{
				var child_parent_practice_id = proj_data[child_index].parent_practice_id;
				var child_practice_id = proj_data[child_index].practice_id;
				
				if(parseInt(prent_practice_id) == parseInt(child_parent_practice_id))
				{
					nlapiLogExecution('audit','prent_practice_id :- '+prent_practice_id,child_parent_practice_id);
					if(group_unque_list.indexOf(child_practice_id)>=0)
					{
						var currnt_grp_index = group_unque_list.indexOf(child_practice_id);
						var proj_id = proj_data[currnt_grp_index].project_id;
						var proj_name = proj_data[currnt_grp_index].project_name;
						var proj_cust_name = proj_data[currnt_grp_index].cust_name;
						var proj_cust_id = proj_data[currnt_grp_index].cust_id;
						var bill_count_onsite = proj_data[currnt_grp_index].billable_count_onsite;
						var bill_count_offsite = proj_data[currnt_grp_index].billable_count_offsite;
						var unbill_count_onsite = proj_data[currnt_grp_index].unbillable_count_onsite;
						var unbill_count_offsite = proj_data[currnt_grp_index].unbillable_count_offsite;
						var bench_count = proj_data[currnt_grp_index].bench_count;
						var bill_type = proj_data[currnt_grp_index].billing_type;
						var clnt_part = proj_data[currnt_grp_index].client_part;
						var proj_mana = proj_data[currnt_grp_index].proj_manager;
						var delivery_mana = proj_data[currnt_grp_index].delivery_mana;
						var vertical_head = proj_data[currnt_grp_index].vertical_head;
						var cust_cp_name = proj_data[currnt_grp_index].customer_CP_name;
						var cuts_cp_id = proj_data[currnt_grp_index].customer_CP_id;
						bill_onsite = parseFloat(bill_onsite) + parseFloat(proj_data[currnt_grp_index].billable_allocation_onsite);
						bill_offsite = parseFloat(bill_offsite) + parseFloat(proj_data[currnt_grp_index].billable_allocation_offsite);
						unbill_onsite = parseFloat(unbill_onsite) + parseFloat(proj_data[currnt_grp_index].unbillabel_allo_onsite);
						unbill_offsite = parseFloat(unbill_offsite) + parseFloat(proj_data[currnt_grp_index].unbillable_allo_offsite);
						var practice_name = proj_data[currnt_grp_index].practice_name;
						
						parent_child_group[currnt_grp_index] = {
												'project_id':proj_id,
												'project_name':proj_name,
												'cust_name':proj_cust_name,
												'cust_id':proj_cust_id,
												'billable_count_onsite':bill_count_onsite,
												'billable_count_offsite':bill_count_offsite,
												'unbillable_count_onsite':unbill_count_onsite,
												'unbillable_count_offsite':unbill_count_offsite,
												'bench_count':bench_count,
												'billing_type':bill_type,
												'client_part':clnt_part,
												'proj_manager':proj_mana,
												'delivery_mana':delivery_mana,
												'vertical_head':vertical_head,
												'customer_CP_name':cust_cp_name,
												'customer_CP_id':cuts_cp_id,
												'billable_allocation_onsite':bill_onsite,
												'billable_allocation_offsite':bill_offsite,
												'unbillabel_allo_onsite':unbill_onsite,
												'unbillable_allo_offsite':unbill_offsite,
												'practice_id':child_practice_id,
												'parent_practice_id':child_parent_practice_id,
												'practice_name':practice_name
											};
						
					}
					else
					{
						var proj_id = proj_data[child_index].project_id;
						var proj_name = proj_data[child_index].project_name;
						var proj_cust_name = proj_data[child_index].cust_name;
						var proj_cust_id = proj_data[child_index].cust_id;
						var bill_count_onsite = proj_data[child_index].billable_count_onsite;
						var bill_count_offsite = proj_data[child_index].billable_count_offsite;
						var unbill_count_onsite = proj_data[child_index].unbillable_count_onsite;
						var unbill_count_offsite = proj_data[child_index].unbillable_count_offsite;
						var bench_count = proj_data[child_index].bench_count;
						var bill_type = proj_data[child_index].billing_type;
						var clnt_part = proj_data[child_index].client_part;
						var proj_mana = proj_data[child_index].proj_manager;
						var delivery_mana = proj_data[child_index].delivery_mana;
						var vertical_head = proj_data[child_index].vertical_head;
						var cust_cp_name = proj_data[child_index].customer_CP_name;
						var cuts_cp_id = proj_data[child_index].customer_CP_id;
						var bill_allo_onsite = proj_data[child_index].billable_allocation_onsite;
						var bill_allo_offsite = proj_data[child_index].billable_allocation_offsite;
						var unbill_allo_onsite = proj_data[child_index].unbillabel_allo_onsite;
						var unbill_allo_offsite = proj_data[child_index].unbillable_allo_offsite;
						var practice_name = proj_data[child_index].practice_name;
						
						parent_child_group[sr_group] = {
												'project_id':proj_id,
												'project_name':proj_name,
												'cust_name':proj_cust_name,
												'cust_id':proj_cust_id,
												'billable_count_onsite':bill_count_onsite,
												'billable_count_offsite':bill_count_offsite,
												'unbillable_count_onsite':unbill_count_onsite,
												'unbillable_count_offsite':unbill_count_offsite,
												'bench_count':bench_count,
												'billing_type':bill_type,
												'client_part':clnt_part,
												'proj_manager':proj_mana,
												'delivery_mana':delivery_mana,
												'vertical_head':vertical_head,
												'customer_CP_name':cust_cp_name,
												'customer_CP_id':cuts_cp_id,
												'billable_allocation_onsite':bill_allo_onsite,
												'billable_allocation_offsite':bill_allo_offsite,
												'unbillabel_allo_onsite':unbill_allo_onsite,
												'unbillable_allo_offsite':unbill_allo_offsite,
												'practice_id':child_practice_id,
												'parent_practice_id':child_parent_practice_id,
												'practice_name':practice_name
											};
						group_unque_list.push(child_practice_id);						
						sr_group++;
					}
				}
			}
			
			if(parent_child_group.length > 0)
			{
				var strVar = '';
				strVar += '<html>';
				strVar += '<body>';
				
				strVar += '<p>Daily Ticker dated '+today+' '+hours+':'+minutes+' (24 Hour Format)</p>';
					
				strVar += '<table>';
			
					strVar += '	<tr>';
					strVar += ' <td width="100%">';
					strVar += '<table width="100%" border="1">';
				
						strVar += '	<tr>';
						strVar += ' <td width="20%" font-size="11" align="center">Practice Name</td>';
						strVar += ' <td width="10%" font-size="11" align="center">Oniste Billed Count</td>';
						strVar += ' <td width="10%" font-size="11" align="center">Offiste Billed Count</td>';
						strVar += ' <td width="10%" font-size="11" align="center">Total Billed Count</td>';
						strVar += ' <td width="10%" font-size="11" align="center">Onsite Unbilled Count</td>';
						strVar += ' <td width="10%" font-size="11" align="center">Offsite Unbilled Count</td>';
						strVar += ' <td width="10%" font-size="11" align="center">Total Unbilled Count</td>';
						strVar += ' <td width="20%" font-size="11" align="center">Bench Count</td>';
						strVar += '	</tr>';
				
						for(var index=0; index<parent_child_group.length; index++)
						{
							var total_bill_count = parseFloat(parent_child_group[index].billable_allocation_onsite) + parseFloat(parent_child_group[index].billable_allocation_offsite);
							var total_unbill_count = parseFloat(parent_child_group[index].unbillabel_allo_onsite) + parseFloat(parent_child_group[index].unbillable_allo_offsite);
							
							strVar += '	<tr>';
							strVar += ' <td width="20%" font-size="11" align="center">'+parent_child_group[index].practice_name+'</td>';
							strVar += ' <td width="10%" font-size="11" align="center">'+parent_child_group[index].billable_allocation_onsite+'</td>';
							strVar += ' <td width="10%" font-size="11" align="center">'+parent_child_group[index].billable_allocation_offsite+'</td>';
							strVar += ' <td width="10%" font-size="11" align="center">'+total_bill_count+'</td>';
							strVar += ' <td width="10%" font-size="11" align="center">'+parent_child_group[index].unbillabel_allo_onsite+'</td>';
							strVar += ' <td width="10%" font-size="11" align="center">'+parent_child_group[index].unbillable_allo_offsite+'</td>';
							strVar += ' <td width="10%" font-size="11" align="center">'+total_unbill_count+'</td>';
							strVar += ' <td width="20%" font-size="11" align="center">'+parent_child_group[index].bench_count+'</td>';
							strVar += '	</tr>';
						}
				
					strVar += '</table>';
					strVar += ' </td>';	
					strVar += '	</tr>';
				
					strVar += '	<tr height="20px">';
					strVar += ' <td width="100%" font-size="11" align="center"></td>';
					strVar += '	</tr>';
						
					strVar += '	<tr>';
					strVar += ' <td width="100%">';
					strVar += '<table width="100%" border="1">';
					
						strVar += '	<tr>';
						strVar += ' <td width="20%" font-size="11" align="center">Project Name</td>';
						strVar += ' <td width="10%" font-size="11" align="center">Oniste Billed Count</td>';
						strVar += ' <td width="10%" font-size="11" align="center">Offiste Billed Count</td>';
						strVar += ' <td width="10%" font-size="11" align="center">Total Billed Count</td>';
						strVar += ' <td width="10%" font-size="11" align="center">Onsite Unbilled Count</td>';
						strVar += ' <td width="10%" font-size="11" align="center">Offsite Unbilled Count</td>';
						strVar += ' <td width="10%" font-size="11" align="center">Total Unbilled Count</td>';
						strVar += ' <td width="20%" font-size="11" align="center">Bench Count</td>';
						strVar += '	</tr>';
						
						for(var pro_index=0; pro_index<proj_data.length; pro_index++)
						{
							var total_bill_count = parseFloat(proj_data[pro_index].billable_allocation_onsite) + parseFloat(proj_data[pro_index].billable_allocation_offsite);
							var total_unbill_count = parseFloat(proj_data[pro_index].unbillabel_allo_onsite) + parseFloat(proj_data[pro_index].unbillable_allo_offsite);
							
							strVar += '	<tr>';
							strVar += ' <td width="20%" font-size="11" align="center">'+proj_data[pro_index].project_name+'</td>';
							strVar += ' <td width="10%" font-size="11" align="center">'+proj_data[pro_index].billable_allocation_onsite+'</td>';
							strVar += ' <td width="10%" font-size="11" align="center">'+proj_data[pro_index].billable_allocation_offsite+'</td>';
							strVar += ' <td width="10%" font-size="11" align="center">'+total_bill_count+'</td>';
							strVar += ' <td width="10%" font-size="11" align="center">'+proj_data[pro_index].unbillabel_allo_onsite+'</td>';
							strVar += ' <td width="10%" font-size="11" align="center">'+proj_data[pro_index].unbillable_allo_offsite+'</td>';
							strVar += ' <td width="10%" font-size="11" align="center">'+total_unbill_count+'</td>';
							strVar += ' <td width="20%" font-size="11" align="center">'+proj_data[pro_index].bench_count+'</td>';
							strVar += '	</tr>';
						}
						
					strVar += '</table>';
					strVar += ' </td>';	
					strVar += '	</tr>';
				
				strVar += '</table>';
				strVar += '</body>';
				strVar += '</html>';
		
				nlapiSendEmail(10730, 'jayesh@inspirria.com', 'Billed Count for today: ', strVar);
			}
		}
	}
}

function searchRecord(recordType, savedSearch, arrFilters, arrColumns,
        filterExpression)
{

	try {
		var search = null;

		// if a saved search is provided, load it and add the filters and
		// columns
		if (isNotEmpty(savedSearch)) {
			search = nlapiLoadSearch(recordType, savedSearch);

			if (isArrayNotEmpty(arrFilters)) {
				search.addFilters(arrFilters);
			}

			if (isArrayNotEmpty(arrColumns)) {
				search.addColumns(arrColumns);
			}

			if (isArrayNotEmpty(filterExpression)) {
				search.setFilterExpression(filterExpression);
			}
		}
		// create a new search
		else {
			search = nlapiCreateSearch(recordType, arrFilters, arrColumns);
		}

		// run search
		var resultSet = search.runSearch();

		// iterate through the search and get all data 1000 at a time
		var searchResultCount = 0;
		var resultSlice = null;
		var searchResult = [];

		do {
			resultSlice = resultSet.getResults(searchResultCount,
			        searchResultCount + 1000);

			if (resultSlice) {

				resultSlice.forEach(function(result) {

					searchResult.push(result);
					searchResultCount++;
				});
			}
		} while (isArrayNotEmpty(resultSlice) && resultSlice.length >= 1000);

		return searchResult;
	} catch (err) {
		nlapiLogExecution('ERROR', 'searchRecord', err);
		throw err;
	}
}

function isEmpty(value) {

	return value == null || value == "" || typeof (value) == undefined;
}

function isNotEmpty(value) {

	return !isEmpty(value);
}

function isArrayEmpty(argArray) {

	return !isArrayNotEmpty(argArray);
}

function isArrayNotEmpty(argArray) {

	return (isNotEmpty(argArray) && argArray.length > 0);
}