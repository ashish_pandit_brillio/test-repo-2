var a_recognized_revenue_total_proj_level = new Array();
var data_array = new Array();
 var monthNameArray = ["JAN_2018", "FEB_2018", "MAR_2018", "APR_2018", "MAY_2018", "JUN_2018", "JUL_2018", "AUG_2018",
        "SEP", "OCT", "NOV", "DEC"
    ];
function effort_test(request, response) {
    try {
		
		var excel_data_array = new Array();
        var o_context = nlapiGetContext();
        var i_user_logegdIn_id = o_context.getUser();
		var date = new Date();
        var year = date.getFullYear();
        var startDate = '1/1/2018';
        startDate = nlapiStringToDate(startDate);
        var o_total_curnt = [];   
        var dts='';
        var SkipMnt =false;
		var project_completed_arr = new Array();
	//	for (var i = 0; i <= 12; i++)
		{
			nlapiYieldScript();
			//var d_day = nlapiAddMonths(startDate, i);
			//var month_name= getMonthName_FromMonthNumber(d_day.getMonth());
			//var d_month_start = nlapiAddDays(d_day, -1 * (d_day.getDate() - 1));
			//var s_month_start = d_month_start.getMonth() + 1 + '/'
              //    + d_month_start.getDate() + '/' + d_month_start.getFullYear();
			//var d_month_end = nlapiAddDays(nlapiAddMonths(d_month_start, 1), -1);
			//var s_month_end = d_month_end.getMonth() + 1 + '/'
              //    + d_month_end.getDate() + '/' + d_month_end.getFullYear();
			var rev_rec_amntSearch = nlapiSearchRecord("customrecord_fp_rev_rec_recognized_amnt",null,
											[["custrecord_fp_rev_rec_type","anyof","1"], "AND", 
											["custrecord_year_to_recognize_amount","is","2018"], "AND", 
											["custrecord_revenue_recognized","notequalto","0.00"]
											//,"AND",["custrecord_month_to_recognize_amount",'is',month_name]
											],	 
											[new nlobjSearchColumn("custrecord_project_to_recognize_amount",null,"GROUP"), 
												new nlobjSearchColumn("custrecord_revenue_recognized",null,"SUM")
												//new nlobjSearchColumn("custrecord_month_to_recognize_amount",null,"GROUP")
												]);
			//if(_logValidation(rev_rec_amntSearch))
			//{
				for(var proj_indx=0;proj_indx< rev_rec_amntSearch.length ; proj_indx++)
            {
				var data_json_mn = [];
				var data_array =[];
				var i_projectId = rev_rec_amntSearch[proj_indx].getValue('custrecord_project_to_recognize_amount',null,"GROUP");
				
				if(project_completed_arr.indexOf(i_projectId) < 0)
				{
					var proj_final_ary = [];
					var res_amount = [];
					var revenue_shareSearch = nlapiSearchRecord("customrecord_revenue_share",null,
                                                [[["systemnotes.field","anyof","CUSTRECORD_REVENUE_SHARE_APPROVAL_STATUS"],"AND",
                                                ["custrecord_revenue_share_project","anyof",i_projectId],"AND",
                                                ["systemnotes.newvalue","is","Approved"]], "AND", 
												[["created","onorafter","12/01/2017 12:00 am"]]],
                                                 [new nlobjSearchColumn("id").setSort(false), 
                                                    new nlobjSearchColumn("custrecord_revenue_share_project"), 
                                                    new nlobjSearchColumn("custrecord_revenue_share_total"), 
                                                    new nlobjSearchColumn("custrecord_version_no"), 
                                                    new nlobjSearchColumn("custrecord_revenue_share_approval_status"), 
                                                    new nlobjSearchColumn("custrecord_is_mnth_end_effort_created"), 
                                                    new nlobjSearchColumn("custrecord_revenue_share_cust")
                                                ]);
					if(_logValidation(revenue_shareSearch))
					{
					for(cap_indx=0;cap_indx< revenue_shareSearch.length; cap_indx++)
					{
						var i_revenue_share_id= revenue_shareSearch[cap_indx].getId();
						var a_effort_plan_filter = [['custrecord_effort_plan_revenue_cap', 'anyof', parseInt(i_revenue_share_id)]];
						var a_columns_effort_plan = new Array();
						a_columns_effort_plan[0] = new nlobjSearchColumn('custrecord_effort_plan_practice');
						a_columns_effort_plan[1] = new nlobjSearchColumn('custrecord_effort_plan_sub_practice').setSort(false);
						a_columns_effort_plan[2] = new nlobjSearchColumn('custrecord_effort_plan_role');
						a_columns_effort_plan[3] = new nlobjSearchColumn('custrecord_effort_plan_level');
						a_columns_effort_plan[4] = new nlobjSearchColumn('custrecord_efort_plan_no_of_resources');
						a_columns_effort_plan[5] = new nlobjSearchColumn('custrecord_effort_plan_allo_strt_date');
						a_columns_effort_plan[6] = new nlobjSearchColumn('custrecord_effort_plan_allo_end_date');
						a_columns_effort_plan[7] = new nlobjSearchColumn('custrecord_effort_plan_percent_allocated');
						a_columns_effort_plan[8] = new nlobjSearchColumn('custrecord_cost_for_resource');
						a_columns_effort_plan[9] = new nlobjSearchColumn('custrecord_proj_strt_date_revenue_cap','custrecord_effort_plan_revenue_cap');
						a_columns_effort_plan[10] = new nlobjSearchColumn('custrecord_proj_end_date_revenue_cap','custrecord_effort_plan_revenue_cap');
						a_columns_effort_plan[11] = new nlobjSearchColumn('custrecord_effort_plan_location');
						a_columns_effort_plan[12] = new nlobjSearchColumn('custrecord_revenue_share_effort_plan');
						var a_get_proj_effort_plan = nlapiSearchRecord('customrecord_fp_rev_rec_effort_plan', null, a_effort_plan_filter, a_columns_effort_plan);
						if (a_get_proj_effort_plan)
						{
							var a_effort_plan_duplicate_filter = [['custrecord_effort_plan_revenue_cap', 'anyof', parseInt(i_revenue_share_id)]];
							var a_columns_effort_plan_duplicate = new Array();
							a_columns_effort_plan_duplicate[0] = new nlobjSearchColumn('custrecord_effort_plan_sub_practice',null,'group').setSort(false);
							a_columns_effort_plan_duplicate[1] = new nlobjSearchColumn('custrecord_revenue_share_effort_plan',null,'group');
							a_columns_effort_plan_duplicate[2] = new nlobjSearchColumn('internalid',null,'count');
							var a_srch_duplicate_sub_practice = nlapiSearchRecord('customrecord_fp_rev_rec_effort_plan', null, a_effort_plan_duplicate_filter, a_columns_effort_plan_duplicate);
							if(a_srch_duplicate_sub_practice)
							{
								var a_duplicate_sub_prac_count = new Array();
								var sr_no = 0;
								for(var i_dupli_index = 0; i_dupli_index < a_srch_duplicate_sub_practice.length; i_dupli_index++)
								{
									a_duplicate_sub_prac_count[sr_no] = {
																	'sub_prac': a_srch_duplicate_sub_practice[i_dupli_index].getValue('custrecord_effort_plan_sub_practice',null,'group'),
																	'sub_prac_name': a_srch_duplicate_sub_practice[i_dupli_index].getText('custrecord_effort_plan_sub_practice',null,'group'),
																	'revenue_share': a_srch_duplicate_sub_practice[i_dupli_index].getValue('custrecord_revenue_share_effort_plan',null,'group'),
																	'no_count': a_srch_duplicate_sub_practice[i_dupli_index].getValue('internalid',null,'count')
																	};
									sr_no++;
								}
							}
						}
						if(_logValidation(a_get_proj_effort_plan))
						{
						var previous_year = '';
						var projectWiseRevenue = {};
						var projectWiseEffortPlan = {};
						var pro_strtDate = a_get_proj_effort_plan[0].getValue('custrecord_proj_strt_date_revenue_cap','custrecord_effort_plan_revenue_cap')
						pro_strtDate = nlapiStringToDate(pro_strtDate);
						var i_year_project = pro_strtDate.getFullYear();
						var pro_endDate = a_get_proj_effort_plan[0].getValue('custrecord_proj_end_date_revenue_cap','custrecord_effort_plan_revenue_cap');
						pro_endDate = nlapiStringToDate(pro_endDate);
						var i_year_project_end = pro_endDate.getFullYear();
						var i_project_mnth = pro_strtDate.getMonth();
						var i_prject_end_mnth = pro_endDate.getMonth();
						//nlapiLogExecution('audit','i_prject_end_mnth:::: '+i_prject_end_mnth,pro_endDate);
						pro_strtDate = nlapiDateToString(pro_strtDate, 'date');
						var pro_endDate = a_get_proj_effort_plan[0].getValue('custrecord_proj_end_date_revenue_cap','custrecord_effort_plan_revenue_cap');
						pro_endDate = nlapiStringToDate(pro_endDate);
						pro_endDate = nlapiDateToString(pro_endDate, 'date');
						var monthBreakUp = getMonthsBreakup(nlapiStringToDate(pro_strtDate),nlapiStringToDate(pro_endDate));
						//nlapiLogExecution('audit','month brckup len:- '+monthBreakUp.length);
						var i_prev_subprac = '';
						var i_prev_role = '';
						var i_prev_level = '';
						for(var i_effort_index = 0; i_effort_index < a_get_proj_effort_plan.length; i_effort_index++)
						{
							var i_practice = a_get_proj_effort_plan[i_effort_index].getValue('custrecord_effort_plan_practice');
							var s_practice = a_get_proj_effort_plan[i_effort_index].getText('custrecord_effort_plan_practice');
							var i_sub_practice = a_get_proj_effort_plan[i_effort_index].getValue('custrecord_effort_plan_sub_practice');
							var s_sub_practice = a_get_proj_effort_plan[i_effort_index].getText('custrecord_effort_plan_sub_practice');
							var i_role = a_get_proj_effort_plan[i_effort_index].getValue('custrecord_effort_plan_role');
							var s_role = a_get_proj_effort_plan[i_effort_index].getText('custrecord_effort_plan_role');
							var i_level = a_get_proj_effort_plan[i_effort_index].getValue('custrecord_effort_plan_level');
							var s_level = a_get_proj_effort_plan[i_effort_index].getText('custrecord_effort_plan_level');
							var i_location = a_get_proj_effort_plan[i_effort_index].getValue('custrecord_effort_plan_location');
							var s_location = a_get_proj_effort_plan[i_effort_index].getText('custrecord_effort_plan_location');
							var f_revenue = a_get_proj_effort_plan[i_effort_index].getValue('custrecord_cost_for_resource');
							var i_total_allocation = a_get_proj_effort_plan[i_effort_index].getValue('custrecord_effort_plan_percent_allocated');
							var s_allocation_strt_date = a_get_proj_effort_plan[i_effort_index].getValue('custrecord_effort_plan_allo_strt_date');
							var s_allocation_end_date = a_get_proj_effort_plan[i_effort_index].getValue('custrecord_effort_plan_allo_end_date');
							var f_revenue_share = a_get_proj_effort_plan[i_effort_index].getValue('custrecord_revenue_share_effort_plan');
							var i_no_of_resources = a_get_proj_effort_plan[i_effort_index].getValue('custrecord_efort_plan_no_of_resources');
							if(i_location == 2)
							{
								s_location = 'UK/Europe/Nordics';
							}
							
							if(i_prev_subprac == '')
							{
								var f_total_revenue_for_tenure = 0;
								
								i_prev_subprac = i_sub_practice;
								i_prev_role = i_role;
								i_prev_level = i_level;
							}
							if(i_prev_subprac != i_sub_practice)
							{
								var f_total_revenue_for_tenure = 0;
								
								i_prev_subprac = i_sub_practice;
								i_prev_role = i_role;
								i_prev_level = i_level;
							}
							else
							{
								if(i_prev_role != i_role)
								{
									var f_total_revenue_for_tenure = 0;
									
									i_prev_subprac = i_sub_practice;
									i_prev_role = i_role;
									i_prev_level = i_level;
								}
								else if(i_prev_level != i_level)
								{
									var f_total_revenue_for_tenure = 0;
									
									i_prev_subprac = i_sub_practice;
									i_prev_role = i_role;
									i_prev_level = i_level;
								}
							}
							for (var j = 0; j < monthBreakUp.length; j++)
							{
								var months = monthBreakUp[j];
								
								var s_month_name = getMonthName(months.Start); // get month name
								
								var month_strt = nlapiStringToDate(months.Start);
								var month_end = nlapiStringToDate(months.End);
								var i_month = month_strt.getMonth();
								var i_year = month_strt.getFullYear();
								s_month_name = s_month_name+'_'+i_year;
								
								var i_total_days_in_mnth = new Date(i_year, i_month+1, 0).getDate();
								
								if(!f_revenue_share)
									f_revenue_share = 0;
									
								if(!f_revenue)
									f_revenue = 0;
									
								i_total_allocation = i_total_allocation.toString();
								i_total_allocation = i_total_allocation.split('%');
								var i_total_allo_resource_no = parseFloat(i_total_allocation[0]) * parseFloat(i_no_of_resources);
								
								var final_prcnt_allocation = parseFloat(i_total_allo_resource_no) / parseFloat(100);
								final_prcnt_allocation = parseFloat(final_prcnt_allocation);
								
								var d_allocation_strt_date = nlapiStringToDate(s_allocation_strt_date);
								var d_allocation_end_date = nlapiStringToDate(s_allocation_end_date);
								
								//nlapiLogExecution('audit','month_strt:- '+month_strt,month_end);
								
								var i_days_diff = 0;
								if(month_strt >= d_allocation_strt_date || month_end >= d_allocation_strt_date)
								{
									i_days_diff = 0;
									if(d_allocation_strt_date <= month_strt && d_allocation_end_date >= month_strt)
									{
										d_allocation_strt_date = month_strt;
									}
									else if(d_allocation_end_date < month_strt)
										d_allocation_strt_date = '';
									
									if(d_allocation_end_date >= month_end && d_allocation_end_date >= month_strt)
									{
										d_allocation_end_date = month_end;
									}
									else if(d_allocation_end_date < month_strt)
									{
										d_allocation_end_date = '';
									}
									
									
									if(d_allocation_strt_date != '')
									{
										if(d_allocation_end_date != '')
										{
											//nlapiLogExecution('audit','allo strt date inside:- '+d_allocation_strt_date,d_allocation_end_date);
											i_days_diff = getDatediffIndays(d_allocation_strt_date,d_allocation_end_date);
										}
									}
								}
								
								//nlapiLogExecution('audit','allo strt date:- '+d_allocation_strt_date,d_allocation_end_date);
								//nlapiLogExecution('audit','days diff:- '+i_days_diff,i_total_days_in_mnth);
								
								var f_prcnt_mnth_allo = parseFloat(i_days_diff)/parseFloat(i_total_days_in_mnth);
								f_prcnt_mnth_allo = parseFloat(f_prcnt_mnth_allo) * parseFloat(final_prcnt_allocation);
								f_prcnt_mnth_allo = parseFloat(f_prcnt_mnth_allo).toFixed(2);
								
								var total_revenue = parseFloat(f_prcnt_mnth_allo) * parseFloat(f_revenue);
								total_revenue = parseFloat(total_revenue).toFixed(2);
								f_total_revenue_for_tenure = parseFloat(f_total_revenue_for_tenure) + parseFloat(total_revenue);
								f_total_revenue_for_tenure = parseFloat(f_total_revenue_for_tenure).toFixed(2);
								
								var i_prac_subPrac_role_level = i_practice + '_' + i_sub_practice + '_' + i_role + '_' + i_level + '_' + i_location;
								
								projectWiseEffortPlan[i_effort_index] = {
										practice_name: s_practice,
										sub_prac_name: s_sub_practice,
										sub_practice: i_sub_practice,
										role_name: s_role,
										level_name: s_level,
										location_name: s_location,
										total_revenue_for_tenure: f_total_revenue_for_tenure,
										revenue_share: f_revenue_share,
										revenue_share_revised: 0,
										allocation_strt_date: s_allocation_strt_date,
										allocation_end_date: s_allocation_end_date,
										total_allocation: i_total_allocation[0],
										no_of_resources: i_no_of_resources,
										RevenueData: {}
									};
									
									projectWiseEffortPlan[i_effort_index].RevenueData[i_practice] = new yearData(i_year);
									
									projectWiseEffortPlan[i_effort_index].RevenueData[i_practice][s_month_name].Amount = total_revenue;
									projectWiseEffortPlan[i_effort_index].RevenueData[i_practice][s_month_name].prcnt_allocated = f_prcnt_mnth_allo;
								
								if (!projectWiseRevenue[i_prac_subPrac_role_level]) {
									projectWiseRevenue[i_prac_subPrac_role_level] = {
										practice_name: s_practice,
										sub_prac_name: s_sub_practice,
										sub_practice: i_sub_practice,
										role_name: s_role,
										level_name: s_level,
										location_name: s_location,
										total_revenue_for_tenure: f_total_revenue_for_tenure,
										revenue_share: f_revenue_share,
										revenue_share_revised: 0,
										allocation_strt_date: s_allocation_strt_date,
										allocation_end_date: s_allocation_end_date,
										total_allocation: i_total_allocation[0],
										no_of_resources: i_no_of_resources,
										RevenueData: {}
									};
									
									projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice] = new yearData(i_year);
									
									projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].Amount = total_revenue;
									projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].prcnt_allocated = f_prcnt_mnth_allo;
								//  nlapiLogExecution('DEBUG','Revenue data',JSON.stringify(projectWiseRevenue));	
								}
                        
								else {
									// nlapiLogExecution('DEBUG','Revenue data',JSON.stringify(projectWiseRevenue));
									var f_earlier_tenure_cost = projectWiseRevenue[i_prac_subPrac_role_level].total_revenue_for_tenure;
									//nlapiLogExecution('audit','i_prac_subPrac_role_level:- '+i_prac_subPrac_role_level,'f_earlier_tenure_cost:- '+f_earlier_tenure_cost);
									f_earlier_tenure_cost = parseFloat(f_earlier_tenure_cost) + parseFloat(f_total_revenue_for_tenure);
									
									var f_earlier_monthly_cost = projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].Amount;
									f_earlier_monthly_cost = parseFloat(f_earlier_monthly_cost) + parseFloat(total_revenue);
									
									var f_earlier_prcnt_allocated = projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].prcnt_allocated;
									f_earlier_prcnt_allocated = parseFloat(f_earlier_prcnt_allocated) + parseFloat(f_prcnt_mnth_allo);
									
									projectWiseRevenue[i_prac_subPrac_role_level].total_revenue_for_tenure = f_total_revenue_for_tenure;
									
									projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].Amount = f_earlier_monthly_cost;
									projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].prcnt_allocated = f_earlier_prcnt_allocated;
								}
							}
						}
						for(var i_dupli = 0; i_dupli<a_duplicate_sub_prac_count.length; i_dupli++)
						{
							//o_form_obj.addFieldGroup('custpage_cost_view_'+i_dupli, ''+a_duplicate_sub_prac_count[i_dupli].sub_prac_name);
							var h_tableHtml_cost_view = generate_cost_view(projectWiseRevenue,monthBreakUp,i_year_project,i_project_mnth,2,a_duplicate_sub_prac_count[i_dupli].sub_prac,i_year_project_end,i_prject_end_mnth);
							//o_form_obj.addField('cost_view_'+i_dupli, 'inlinehtml', null, null, 'custpage_cost_view_'+i_dupli).setDefaultValue(h_tableHtml_cost_view);
						}
						res_amount = generate_total_project_view(projectWiseRevenue,monthBreakUp,i_year_project,i_project_mnth,4,i_year_project_end,i_prject_end_mnth,data_json_mn,data_array);
						//o_form_obj.addFieldGroup('custpage_total_view', 'Total revenue recognized / month');
						//o_form_obj.addField('total_view', 'inlinehtml', null, null, 'custpage_total_view').setDefaultValue(h_tableHtml_total_view);
						}
					}
					
					for(var res_lp = 0; res_lp < res_amount.length; res_lp++)
					{
						proj_final_ary.push({
							'projectName':rev_rec_amntSearch[proj_indx].getText('custrecord_project_to_recognize_amount',null,"GROUP"),
							'project' :rev_rec_amntSearch[proj_indx].getValue('custrecord_project_to_recognize_amount',null,"GROUP"),
							'plan_month' : res_amount[res_lp].month,
							'plan_amount' : res_amount[res_lp].amount
							
						});
					}
					if(proj_final_ary.length>0)
					excel_data_array.push(proj_final_ary);
				project_completed_arr.push(i_projectId);

				}
			}
            }
			
      
        }    
           
        var excel_cost_data = [];
      
           /* for (var excel_len = 0; excel_len < excel_cost_data.length; excel_len++) {
                var rev_len = excel_cost_data[excel_len].month;
             
                    bill_data_arr[sr_no] = {
                        
                        'month': excel_cost_data[excel_len].month,
						'plan_effort_total' :excel_cost_data[excel_len].effort,
						'effort_total' : excel_cost_data[excel_len].effort_view,
						'sub_total_plan': excel_cost_data[excel_len].allocation_total_plan,
                        'sub_total': excel_cost_data[excel_len].allocation_total,
                        'actual_rev_plan': excel_cost_data[excel_len].revenue_plan,
						'actual_rev': excel_cost_data[excel_len].revenue,
						'currency':s_proj_currency,
						'plan_cost':excel_cost_data[excel_len].plan_cost,
						'current_cost':excel_cost_data[excel_len].currenr_cost,
						'plan_margin':excel_cost_data[excel_len].plan_margin,
						'crnt_margin':excel_cost_data[excel_len].crnt_margin

                    };
             //   }

                sr_no++;
                nlapiLogExecution('debug', 'Data', bill_data_arr);
            }*/
          

            
        
        down_excel_function(excel_data_array);
        nlapiLogExecution('audit', 'remaining usage:- ', nlapiGetContext().getRemainingUsage());
    } catch (err) {
        nlapiLogExecution('ERROR', 'suitelet_month_end_confirmation', 'ERROR MESSAGE:- ' + err);
    }
}

/*function generate_cost_view(projectWiseRevenue, monthBreakUp, i_year_project, i_project_mnth, mode, sub_prac, s_currency_symbol_proj, s_currency_symbol_usd, i_year_project_end, i_prject_end_mnth, projectAllocationPlan) {
    var css_file_url = "";

   


    var html_data = [];
    var html_val = {};

    var html = {
        'month': [],
        'effort': [],
		'effort_view' : [],
        'revenue': [],
		'revenue_plan' :[],
		'allocation_total_plan' :[],
        'allocation_total': [],
		'plan_cost':[],
		'currenr_cost':[],
		'plan_margin':[],
		'crnt_margin':[]
    };

    var today = new Date();
    var currentMonthName = getMonthName(nlapiDateToString(today));

    for (var j = 0; j < monthBreakUp.length; j++) {
        var months = monthBreakUp[j];
        var s_month_name = getMonthName(months.Start); // get month name
        var s_year = nlapiStringToDate(months.Start)
        s_year = s_year.getFullYear();
        s_month_name = s_month_name + '_' + s_year;
        html['month'].push(s_month_name);

    }

    var a_sub_prac_already_displayed = new Array();
    var i_subpractice_id = 0;
    var a_amount = new Array();
    var a_amount_plan = new Array();
    var a_recognized_revenue = new Array();
    var a_recognized_revenue_plan = new Array();
    var i_frst_row = 0;
    var i_frst_row_plan = 0;
    var i_diplay_frst_column = 0;
    var i_total_row_revenue = 0;
    var i_total_row_revenue_plan = 0;
    var i_total_per_row = 0;
    var total_prcnt_aloocated = new Array();
    var total_prcnt_aloocated_plan = new Array();
    var b_first_line_copied = 0;
    var b_first_line_copied_plan = 0;
    var f_total_allocated_row = 0;
    var i_diplay_frst_column_plan = 0;
	var margin_rev=new Array();
	var margin_rev_plan=new Array();
    for (var emp_internal_id in projectAllocationPlan) {
        if (projectAllocationPlan[emp_internal_id].sub_practice == sub_prac) {
            f_total_allocated_row = 0;
            for (var project_internal_id in projectAllocationPlan[emp_internal_id].RevenueDataPlan) {

                if (i_diplay_frst_column_plan == 0) {

                    html['effort'].push('Plan Effort View');

                }
				else
				{
										
					html['effort'].push('b');
				}
                html['effort'].push(projectAllocationPlan[emp_internal_id].practice_name);


                html['effort'].push(projectAllocationPlan[emp_internal_id].sub_prac_name);

                html['effort'].push(projectAllocationPlan[emp_internal_id].role_name);


                html['effort'].push(projectAllocationPlan[emp_internal_id].level_name);

                html['effort'].push(projectAllocationPlan[emp_internal_id].location_name);


                var i_index_mnth = 0;
                for (var month in projectAllocationPlan[emp_internal_id].RevenueDataPlan[project_internal_id]) {
                    var a_MonthNames = MonthNames(i_year_project);
                    var currentMonthPos = a_MonthNames.indexOf(month);
                    var s_current_month = a_MonthNames[currentMonthPos];
                    var s_current_mnth_yr = s_current_month.substr(4, 8);

                    var i_nxt_yr_flg = 0;
                    if (currentMonthPos >= 12) {
                        currentMonthPos = currentMonthPos - 12;
                        i_nxt_yr_flg = 1;
                    }

                    var i_curr_yr_dis_flag = 0;
                    if (parseInt(s_current_mnth_yr) < i_year_project_end && i_prject_end_mnth < currentMonthPos) {
                        i_curr_yr_dis_flag = 1;
                    }

                    if (((i_project_mnth <= currentMonthPos && ((i_prject_end_mnth >= currentMonthPos || i_curr_yr_dis_flag == 1) && i_nxt_yr_flg == 0) && parseInt(s_current_mnth_yr) <= i_year_project) && parseInt(s_current_mnth_yr) <= i_year_project_end) || (i_prject_end_mnth >= currentMonthPos && parseInt(s_current_mnth_yr) <= i_year_project_end && i_nxt_yr_flg == 1)) {
                        //nlapiLogExecution('audit','currentMonthPos:- ',+currentMonthPos);
                        if (b_first_line_copied_plan == 1) {
                            var f_allocated_prcnt = total_prcnt_aloocated_plan[i_index_mnth];
                            f_allocated_prcnt = parseFloat(f_allocated_prcnt) + parseFloat(projectAllocationPlan[emp_internal_id].RevenueDataPlan[project_internal_id][month].prcnt_allocated);
                            total_prcnt_aloocated_plan[i_index_mnth] = f_allocated_prcnt;
                            i_index_mnth++;
                        } else {
                            total_prcnt_aloocated_plan.push(projectAllocationPlan[emp_internal_id].RevenueDataPlan[project_internal_id][month].prcnt_allocated);
                        }

                        f_total_allocated_row = parseFloat(projectAllocationPlan[emp_internal_id].RevenueDataPlan[project_internal_id][month].prcnt_allocated) + parseFloat(f_total_allocated_row);


                        html['effort'].push(parseFloat(projectAllocationPlan[emp_internal_id].RevenueDataPlan[project_internal_id][month].prcnt_allocated).toFixed(2));

                    }
                }
            }

            b_first_line_copied_plan = 1;


            html['effort'].push(parseFloat(f_total_allocated_row).toFixed(2));




            var s_sub_prac_name = projectAllocationPlan[emp_internal_id].sub_prac_name;
            var s_practice_name = projectAllocationPlan[emp_internal_id].practice_name;

            i_diplay_frst_column_plan = 1;
        }
    }
    i_diplay_frst_column_plan = 0;
	if(total_prcnt_aloocated_plan.length>0)
	{
		html['allocation_total_plan'].push('Sub Total');
			
		html['allocation_total_plan'].push(s_practice_name);
		
		html['allocation_total_plan'].push(s_sub_prac_name);

		html['allocation_total_plan'].push('');

		html['allocation_total_plan'].push('');
		html['allocation_total_plan'].push('');
		

		var f_total_row_allocation = 0;
		for (var i_index_total_allocated = 0; i_index_total_allocated < total_prcnt_aloocated_plan.length; i_index_total_allocated++) {

			html['allocation_total_plan'].push(parseFloat(total_prcnt_aloocated_plan[i_index_total_allocated]).toFixed(2));


			f_total_row_allocation = parseFloat(f_total_row_allocation) + parseFloat(total_prcnt_aloocated_plan[i_index_total_allocated]);
		}

		html['allocation_total_plan'].push(parseFloat(f_total_row_allocation).toFixed(2));

	}
	else
	{
		html['effort'].push('Plan Effort View');

		html['effort'].push('NA');
		
		html['effort'].push('NA');
	
		html['effort'].push('');
	
		html['effort'].push('');
	
		html['effort'].push('');
		for (var mn = 0; mn <= monthBreakUp.length; mn++)
		{
			
				html['effort'].push('0');
		}
	}
    for (var emp_internal_id in projectWiseRevenue) {
        if (projectWiseRevenue[emp_internal_id].sub_practice == sub_prac) {
            f_total_allocated_row = 0;
            for (var project_internal_id in projectWiseRevenue[emp_internal_id].RevenueData) {

                if (i_diplay_frst_column == 0) {

                    html['effort_view'].push('Current Effort View');

                }
				else {

                    html['effort_view'].push('b');

                }

                html['effort_view'].push(projectWiseRevenue[emp_internal_id].practice_name);


                html['effort_view'].push(projectWiseRevenue[emp_internal_id].sub_prac_name);

                html['effort_view'].push(projectWiseRevenue[emp_internal_id].role_name);

                html['effort_view'].push(projectWiseRevenue[emp_internal_id].level_name);

                html['effort_view'].push(projectWiseRevenue[emp_internal_id].location_name);


                var i_index_mnth = 0;
                for (var month in projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id]) {
                    var a_MonthNames = MonthNames(i_year_project);
                    var currentMonthPos = a_MonthNames.indexOf(month);
                    var s_current_month = a_MonthNames[currentMonthPos];
                    var s_current_mnth_yr = s_current_month.substr(4, 8);

                    var i_nxt_yr_flg = 0;
                    if (currentMonthPos >= 12) {
                        currentMonthPos = currentMonthPos - 12;
                        i_nxt_yr_flg = 1;
                    }

                    var i_curr_yr_dis_flag = 0;
                    if (parseInt(s_current_mnth_yr) < i_year_project_end && i_prject_end_mnth < currentMonthPos) {
                        i_curr_yr_dis_flag = 1;
                    }

                    if (((i_project_mnth <= currentMonthPos && ((i_prject_end_mnth >= currentMonthPos || i_curr_yr_dis_flag == 1) && i_nxt_yr_flg == 0) && parseInt(s_current_mnth_yr) <= i_year_project) && parseInt(s_current_mnth_yr) <= i_year_project_end) || (i_prject_end_mnth >= currentMonthPos && parseInt(s_current_mnth_yr) <= i_year_project_end && i_nxt_yr_flg == 1)) {
                        //nlapiLogExecution('audit','currentMonthPos:- ',+currentMonthPos);
                        if (b_first_line_copied == 1) {
                            var f_allocated_prcnt = total_prcnt_aloocated[i_index_mnth];
                            f_allocated_prcnt = parseFloat(f_allocated_prcnt) + parseFloat(projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id][month].prcnt_allocated);
                            total_prcnt_aloocated[i_index_mnth] = f_allocated_prcnt;
                            i_index_mnth++;
                        } else {
                            total_prcnt_aloocated.push(projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id][month].prcnt_allocated);
                        }

                        f_total_allocated_row = parseFloat(projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id][month].prcnt_allocated) + parseFloat(f_total_allocated_row);

                        html['effort_view'].push(parseFloat(projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id][month].prcnt_allocated).toFixed(2));

                    }
                }
            }

            b_first_line_copied = 1;

            html['effort_view'].push(parseFloat(f_total_allocated_row).toFixed(2));


            var s_sub_prac_name = projectWiseRevenue[emp_internal_id].sub_prac_name;
            var s_practice_name = projectWiseRevenue[emp_internal_id].practice_name;

            i_diplay_frst_column = 1;
        }
    }
    i_diplay_frst_column = 0;
	if(total_prcnt_aloocated.length>0)
	{
		html['allocation_total'].push('Sub Total');
		html['allocation_total'].push(s_practice_name);
		
		html['allocation_total'].push(s_sub_prac_name);

		html['allocation_total'].push('');

		html['allocation_total'].push('');
		html['allocation_total'].push('');

		var f_total_row_allocation = 0;
		for (var i_index_total_allocated = 0; i_index_total_allocated < total_prcnt_aloocated.length; i_index_total_allocated++) {

			html['allocation_total'].push(parseFloat(total_prcnt_aloocated[i_index_total_allocated]).toFixed(2));


			f_total_row_allocation = parseFloat(f_total_row_allocation) + parseFloat(total_prcnt_aloocated[i_index_total_allocated]);
		}

		html['allocation_total'].push(parseFloat(f_total_row_allocation).toFixed(2));
	}
	else
	{
		html['effort_view'].push('Current Effort View');

		html['effort_view'].push('NA');

		html['effort_view'].push('NA');

		html['effort_view'].push('');

		html['effort_view'].push( '');
		
		html['effort_view'].push( '');
	
		for (var mn = 0; mn <= monthBreakUp.length; mn++)
		{
				
				html['effort_view'].push('0');
				
		}
	}

    //cost for plan
    for (var emp_internal_id in projectAllocationPlan) {

        for (var project_internal_id in projectAllocationPlan[emp_internal_id].RevenueDataPlan) {

            if (projectAllocationPlan[emp_internal_id].sub_practice == sub_prac) {
                if (i_diplay_frst_column_plan == 0) {
                    //html += "<td class='label-name'>";
                    //html += '<b>Cost View';
                    //html += "</td>";
                }

                var i_sub_practice_internal_id = 0;
                var i_amt = 0;
                for (var month in projectAllocationPlan[emp_internal_id].RevenueDataPlan[project_internal_id]) {
                    var a_MonthNames = MonthNames(i_year_project);
                    var currentMonthPos = a_MonthNames.indexOf(month);
                    var s_current_month = a_MonthNames[currentMonthPos];
                    var s_current_mnth_yr = s_current_month.substr(4, 8);

                    var i_nxt_yr_flg = 0;
                    if (currentMonthPos >= 12) {
                        currentMonthPos = currentMonthPos - 12;
                        i_nxt_yr_flg = 1;
                    }

                    var i_curr_yr_dis_flag = 0;
                    if (parseInt(s_current_mnth_yr) < i_year_project_end && i_prject_end_mnth < currentMonthPos) {
                        i_curr_yr_dis_flag = 1;
                    }

                    if (((i_project_mnth <= currentMonthPos && ((i_prject_end_mnth >= currentMonthPos || i_curr_yr_dis_flag == 1) && i_nxt_yr_flg == 0) && parseInt(s_current_mnth_yr) <= i_year_project) && parseInt(s_current_mnth_yr) <= i_year_project_end) || (i_prject_end_mnth >= currentMonthPos && parseInt(s_current_mnth_yr) <= i_year_project_end && i_nxt_yr_flg == 1)) {
                        if (mode == 2) {
                            var total_revenue_format = parseFloat(projectAllocationPlan[emp_internal_id].RevenueDataPlan[project_internal_id][month].Amount).toFixed(1);

                            if (i_frst_row_plan == 0) {
                                //nlapiLogExecution('audit','inside push recog amnt:- '+projectAllocationPlan[emp_internal_id].RevenueDataPlan[project_internal_id][month].recognized_revenue);
                                a_amount_plan.push(total_revenue_format);
                                a_recognized_revenue_plan.push(projectAllocationPlan[emp_internal_id].RevenueDataPlan[project_internal_id][month].recognized_revenue);
                            } else {
                                var amnt_mnth = a_amount_plan[i_amt];
                                amnt_mnth = parseFloat(amnt_mnth) + parseFloat(projectAllocationPlan[emp_internal_id].RevenueDataPlan[project_internal_id][month].Amount);

                                a_amount_plan[i_amt] = amnt_mnth;

                                var i_existing_recognised_amount = a_recognized_revenue_plan[i_amt];
                                //nlapiLogExecution('audit','month name:- '+month);
                                //nlapiLogExecution('audit','index:- '+i_amt+'::existing rev:- '+i_existing_recognised_amount,'Rev REcg:- '+projectAllocationPlan[emp_internal_id].RevenueDataPlan[project_internal_id][month].recognized_revenue);
                                i_existing_recognised_amount = parseFloat(i_existing_recognised_amount) + parseFloat(projectAllocationPlan[emp_internal_id].RevenueDataPlan[project_internal_id][month].recognized_revenue);

                                a_recognized_revenue_plan[i_amt] = i_existing_recognised_amount;

                                i_amt++;
                            }
                            i_total_row_revenue_plan = parseFloat(i_total_row_revenue_plan) + parseFloat(projectAllocationPlan[emp_internal_id].RevenueDataPlan[project_internal_id][month].Amount);
                            i_total_per_row = parseFloat(i_total_per_row) + parseFloat(projectAllocationPlan[emp_internal_id].RevenueDataPlan[project_internal_id][month].Amount);
                        }

                    }
                }

                if (mode == 2) {

                    //html += ''+s_currency_symbol_usd+' '+format2(projectAllocationPlan[emp_internal_id].total_revenue_for_tenure);
                    i_total_per_row = 0;
                    //i_total_row_revenue = parseFloat(i_total_row_revenue) + parseFloat(projectAllocationPlan[emp_internal_id].total_revenue_for_tenure);
                }

                i_frst_row_plan = 1;


                i_diplay_frst_column_plan = 1;

                var f_revenue_share = projectAllocationPlan[emp_internal_id].revenue_share;
            }
        }
    }

    var s_sub_prac_name = '';
    for (var emp_internal_id in projectAllocationPlan) {
        if (projectAllocationPlan[emp_internal_id].sub_practice == sub_prac) {
            s_sub_prac_name = projectAllocationPlan[emp_internal_id].sub_prac_name;
            s_practice_name = projectAllocationPlan[emp_internal_id].practice_name;
            //projectAllocationPlan[emp_internal_id].revenue_share_revised = i_total_row_revenue;
            //projectAllocationPlan[emp_internal_id].total_revenue_for_tenure = i_total_row_revenue;
        }
    }
	//** cost for Plan array
	html['plan_cost'].push('Plan Cost total');

	html['plan_cost'].push(s_practice_name);
	html['plan_cost'].push(s_sub_prac_name);
	html['plan_cost'].push('');
	html['plan_cost'].push('');
	html['plan_cost'].push('');
	if(a_amount_plan.length>0)
	{
		for (var i_amt = 0; i_amt < a_amount_plan.length; i_amt++)
		{
			html['plan_cost'].push(format2(a_amount_plan[i_amt]));
		
		}
		html['plan_cost'].push(format2(i_total_row_revenue_plan));
	}
	else
	{
		for (var mn = 0; mn <= monthBreakUp.length; mn++)
		{
	
			html['plan_cost'].push('0');
		}
	}
    //
    for (var emp_internal_id in projectWiseRevenue) {

        for (var project_internal_id in projectWiseRevenue[emp_internal_id].RevenueData) {

            if (projectWiseRevenue[emp_internal_id].sub_practice == sub_prac) {
                if (i_diplay_frst_column == 0) {
                    //html += "<td class='label-name'>";
                    //html += '<b>Cost View';
                    //html += "</td>";
                }

                var i_sub_practice_internal_id = 0;
                var i_amt = 0;
                for (var month in projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id]) {
                    var a_MonthNames = MonthNames(i_year_project);
                    var currentMonthPos = a_MonthNames.indexOf(month);
                    var s_current_month = a_MonthNames[currentMonthPos];
                    var s_current_mnth_yr = s_current_month.substr(4, 8);

                    var i_nxt_yr_flg = 0;
                    if (currentMonthPos >= 12) {
                        currentMonthPos = currentMonthPos - 12;
                        i_nxt_yr_flg = 1;
                    }

                    var i_curr_yr_dis_flag = 0;
                    if (parseInt(s_current_mnth_yr) < i_year_project_end && i_prject_end_mnth < currentMonthPos) {
                        i_curr_yr_dis_flag = 1;
                    }

                    if (((i_project_mnth <= currentMonthPos && ((i_prject_end_mnth >= currentMonthPos || i_curr_yr_dis_flag == 1) && i_nxt_yr_flg == 0) && parseInt(s_current_mnth_yr) <= i_year_project) && parseInt(s_current_mnth_yr) <= i_year_project_end) || (i_prject_end_mnth >= currentMonthPos && parseInt(s_current_mnth_yr) <= i_year_project_end && i_nxt_yr_flg == 1)) {
                        if (mode == 2) {
                            var total_revenue_format = parseFloat(projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id][month].Amount).toFixed(1);

                            if (i_frst_row == 0) {
                                //nlapiLogExecution('audit','inside push recog amnt:- '+projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id][month].recognized_revenue);
                                a_amount.push(total_revenue_format);
                                a_recognized_revenue.push(projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id][month].recognized_revenue);
                            } else {
                                var amnt_mnth = a_amount[i_amt];
                                amnt_mnth = parseFloat(amnt_mnth) + parseFloat(projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id][month].Amount);

                                a_amount[i_amt] = amnt_mnth;

                                var i_existing_recognised_amount = a_recognized_revenue[i_amt];
                                //nlapiLogExecution('audit','month name:- '+month);
                                //nlapiLogExecution('audit','index:- '+i_amt+'::existing rev:- '+i_existing_recognised_amount,'Rev REcg:- '+projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id][month].recognized_revenue);
                                i_existing_recognised_amount = parseFloat(i_existing_recognised_amount) + parseFloat(projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id][month].recognized_revenue);

                                a_recognized_revenue[i_amt] = i_existing_recognised_amount;

                                i_amt++;
                            }
                            i_total_row_revenue = parseFloat(i_total_row_revenue) + parseFloat(projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id][month].Amount);
                            i_total_per_row = parseFloat(i_total_per_row) + parseFloat(projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id][month].Amount);
                        }

                    }
                }

                if (mode == 2) {

                    //html += ''+s_currency_symbol_usd+' '+format2(projectWiseRevenue[emp_internal_id].total_revenue_for_tenure);
                    i_total_per_row = 0;
                    //i_total_row_revenue = parseFloat(i_total_row_revenue) + parseFloat(projectWiseRevenue[emp_internal_id].total_revenue_for_tenure);
                }

                i_frst_row = 1;


                i_diplay_frst_column = 1;

                var f_revenue_share = projectWiseRevenue[emp_internal_id].revenue_share;
            }
        }
    }

    var s_sub_prac_name = '';
    for (var emp_internal_id in projectWiseRevenue) {
        if (projectWiseRevenue[emp_internal_id].sub_practice == sub_prac) {
            s_sub_prac_name = projectWiseRevenue[emp_internal_id].sub_prac_name;
            s_practice_name = projectWiseRevenue[emp_internal_id].practice_name;
            //projectWiseRevenue[emp_internal_id].revenue_share_revised = i_total_row_revenue;
            //projectWiseRevenue[emp_internal_id].total_revenue_for_tenure = i_total_row_revenue;
        }
    }
		//** cost for current effort 

	html['currenr_cost'].push('Current Cost total');
	
	html['currenr_cost'].push(s_practice_name);

	html['currenr_cost'].push(s_sub_prac_name);

	html['currenr_cost'].push('');

	html['currenr_cost'].push('');

	html['currenr_cost'].push('');
	if(a_amount.length>0)
	{
		for (var i_amt = 0; i_amt < a_amount.length; i_amt++)
		{
			html['currenr_cost'].push(format2(a_amount[i_amt]));
		}

		html['currenr_cost'].push(format2(i_total_row_revenue));
	}
	else
	{
		for (var mn = 0; mn <= monthBreakUp.length; mn++)
		{
			html['currenr_cost'].push('0');
		
		}
	}
    // Total revenue recognized per plan
	if(a_amount_plan.length>0)
	{
		html['revenue_plan'].push('Revenue to be recognized/month Per Plan');
		
		html['revenue_plan'].push(s_practice_name);

		html['revenue_plan'].push(s_sub_prac_name);
		
		html['revenue_plan'].push('');		

		html['revenue_plan'].push( '');

		html['revenue_plan'].push( '');
		
		var d_today_date = new Date();
		var i_current_month = d_today_date.getMonth();
		var i_current_year = d_today_date.getFullYear();

		var f_total_revenue_plan = 0;
		var f_revenue_amount_till_current_month_plan = 0;
		for (var i_amt = 0; i_amt < a_amount_plan.length; i_amt++) {
			var f_prcnt_revenue = parseFloat(a_amount_plan[i_amt]) / parseFloat(i_total_row_revenue_plan);
			f_prcnt_revenue = f_prcnt_revenue * 100;

			var f_revenue_amount = (parseFloat(f_prcnt_revenue) * parseFloat(f_revenue_share)) / 100;
			if (!f_revenue_amount)
				f_revenue_amount = 0;
			margin_rev_plan[i_amt]=f_revenue_amount;
			if (parseInt(i_year_project) != parseInt(i_current_year)) {
				var i_total_project_tenure = 11 + parseInt(i_current_month);
				var f_total_prev_mnths = parseFloat(i_total_project_tenure) - parseFloat(i_project_mnth);
				f_total_prev_mnths = parseFloat(f_total_prev_mnths) + parseFloat(1);
			} else {
				var f_total_prev_mnths = parseFloat(i_current_month) - parseFloat(i_project_mnth);
			}

			if (i_amt <= f_total_prev_mnths) {
				//nlapiLogExecution('audit','f_revenue_amount:- '+f_revenue_amount,'a_amount_plan.length:- '+a_amount_plan.length);
				f_revenue_amount_till_current_month_plan = parseFloat(f_revenue_amount) + parseFloat(f_revenue_amount_till_current_month_plan);
			}

			f_total_revenue_plan = parseFloat(f_revenue_amount) + parseFloat(f_total_revenue_plan);


			html['revenue_plan'].push(format2(f_revenue_amount));

		}

		for (var emp_internal_id in projectAllocationPlan) {
			for (var project_internal_id in projectAllocationPlan[emp_internal_id].RevenueDataPlan) {
				var i_amount_map = 0;
				if (projectAllocationPlan[emp_internal_id].sub_practice == sub_prac) {
					for (var month in projectAllocationPlan[emp_internal_id].RevenueDataPlan[project_internal_id]) {
						var a_MonthNames = MonthNames(i_year_project);
						var currentMonthPos = a_MonthNames.indexOf(month);
						var s_current_month = a_MonthNames[currentMonthPos];
						var s_current_mnth_yr = s_current_month.substr(4, 8);

						var i_nxt_yr_flg = 0;
						if (currentMonthPos >= 12) {
							currentMonthPos = currentMonthPos - 12;
							i_nxt_yr_flg = 1;
						}

						var i_curr_yr_dis_flag = 0;
						if (parseInt(s_current_mnth_yr) < i_year_project_end && i_prject_end_mnth < currentMonthPos) {
							i_curr_yr_dis_flag = 1;
						}

						if (((i_project_mnth <= currentMonthPos && ((i_prject_end_mnth >= currentMonthPos || i_curr_yr_dis_flag == 1) && i_nxt_yr_flg == 0) && parseInt(s_current_mnth_yr) <= i_year_project) && parseInt(s_current_mnth_yr) <= i_year_project_end) || (i_prject_end_mnth >= currentMonthPos && parseInt(s_current_mnth_yr) <= i_year_project_end && i_nxt_yr_flg == 1)) {
							var f_prcnt_revenue = parseFloat(a_amount_plan[i_amount_map]) / parseFloat(i_total_row_revenue);
							f_prcnt_revenue = f_prcnt_revenue * 100;

							var f_revenue_amount = (parseFloat(f_prcnt_revenue) * parseFloat(f_revenue_share)) / 100;

							projectAllocationPlan[emp_internal_id].RevenueDataPlan[project_internal_id][month].total_revenue = f_revenue_amount;
							projectAllocationPlan[emp_internal_id].RevenueDataPlan[project_internal_id][month].prcent_complt = f_prcnt_revenue;
							i_amount_map++;
						}
					}
				}
			}
		}


		html['revenue_plan'].push(format2(f_total_revenue_plan));

	}
	else
	{
			
		html['revenue_plan'].push('Revenue to be recognized/month by plan ');
	
		html['revenue_plan'].push(s_practice_name);
	
		html['revenue_plan'].push(s_sub_prac_name);
	
		html['revenue_plan'].push('');
		html['revenue_plan'].push('');
		html['revenue_plan'].push('');
		for (var mn = 0; mn <= monthBreakUp.length; mn++)
		{
				html['revenue_plan'].push('0');
		}
	}
    var i_total_revenue_recognized_ytd_plan = 0;
    for (var i_revenue_index = 0; i_revenue_index < a_recognized_revenue.length; i_revenue_index++) {
        //nlapiLogExecution('audit','prev mnth recog rev:- '+a_recognized_revenue[i_revenue_index]);
        i_total_revenue_recognized_ytd_plan = parseFloat(i_total_revenue_recognized_ytd_plan) + parseFloat(a_recognized_revenue[i_revenue_index]);

    }


    //Total reveue recognized per month row
	if(a_amount.length>0)
	{
		html['revenue'].push('Revenue to be recognized/month');
		html['revenue'].push(s_practice_name);

		html['revenue'].push(s_sub_prac_name);
		
		html['revenue'].push('');		

		html['revenue'].push( '');

		html['revenue'].push( '');

		var d_today_date = new Date();
		var i_current_month = d_today_date.getMonth();
		var i_current_year = d_today_date.getFullYear();

		var f_total_revenue = 0;
		var f_revenue_amount_till_current_month = 0;
		for (var i_amt = 0; i_amt < a_amount.length; i_amt++) {
			var f_prcnt_revenue = parseFloat(a_amount[i_amt]) / parseFloat(i_total_row_revenue);
			f_prcnt_revenue = f_prcnt_revenue * 100;

			var f_revenue_amount = (parseFloat(f_prcnt_revenue) * parseFloat(f_revenue_share)) / 100;
			if (!f_revenue_amount)
				f_revenue_amount = 0;
			margin_rev[i_amt]=f_revenue_amount;
			if (parseInt(i_year_project) != parseInt(i_current_year)) {
				var i_total_project_tenure = 11 + parseInt(i_current_month);
				var f_total_prev_mnths = parseFloat(i_total_project_tenure) - parseFloat(i_project_mnth);
				f_total_prev_mnths = parseFloat(f_total_prev_mnths) + parseFloat(1);
			} else {
				var f_total_prev_mnths = parseFloat(i_current_month) - parseFloat(i_project_mnth);
			}

			if (i_amt <= f_total_prev_mnths) {
				//nlapiLogExecution('audit','f_revenue_amount:- '+f_revenue_amount,'a_amount.length:- '+a_amount.length);
				f_revenue_amount_till_current_month = parseFloat(f_revenue_amount) + parseFloat(f_revenue_amount_till_current_month);
			}

			f_total_revenue = parseFloat(f_revenue_amount) + parseFloat(f_total_revenue);


			html['revenue'].push(format2(f_revenue_amount));

		}

		for (var emp_internal_id in projectWiseRevenue) {
			for (var project_internal_id in projectWiseRevenue[emp_internal_id].RevenueData) {
				var i_amount_map = 0;
				if (projectWiseRevenue[emp_internal_id].sub_practice == sub_prac) {
					for (var month in projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id]) {
						var a_MonthNames = MonthNames(i_year_project);
						var currentMonthPos = a_MonthNames.indexOf(month);
						var s_current_month = a_MonthNames[currentMonthPos];
						var s_current_mnth_yr = s_current_month.substr(4, 8);

						var i_nxt_yr_flg = 0;
						if (currentMonthPos >= 12) {
							currentMonthPos = currentMonthPos - 12;
							i_nxt_yr_flg = 1;
						}

						var i_curr_yr_dis_flag = 0;
						if (parseInt(s_current_mnth_yr) < i_year_project_end && i_prject_end_mnth < currentMonthPos) {
							i_curr_yr_dis_flag = 1;
						}

						if (((i_project_mnth <= currentMonthPos && ((i_prject_end_mnth >= currentMonthPos || i_curr_yr_dis_flag == 1) && i_nxt_yr_flg == 0) && parseInt(s_current_mnth_yr) <= i_year_project) && parseInt(s_current_mnth_yr) <= i_year_project_end) || (i_prject_end_mnth >= currentMonthPos && parseInt(s_current_mnth_yr) <= i_year_project_end && i_nxt_yr_flg == 1)) {
							var f_prcnt_revenue = parseFloat(a_amount[i_amount_map]) / parseFloat(i_total_row_revenue);
							f_prcnt_revenue = f_prcnt_revenue * 100;

							var f_revenue_amount = (parseFloat(f_prcnt_revenue) * parseFloat(f_revenue_share)) / 100;

							projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id][month].total_revenue = f_revenue_amount;
							projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id][month].prcent_complt = f_prcnt_revenue;
							i_amount_map++;
						}
					}
				}
			}
		}


		html['revenue'].push(format2(f_total_revenue));
	}
	else
	{
	
		html['revenue'].push('Revenue to be recognized/month ');
	
		html['revenue'].push(s_practice_name);

		html['revenue'].push(s_sub_prac_name);

		html['revenue'].push('');
		html['revenue'].push('');
		html['revenue'].push( '');
		for (var mn = 0; mn <= monthBreakUp.length; mn++)
		{
				
				html['revenue'].push('0');
				
		}
	
	}

    var i_total_revenue_recognized_ytd = 0;
    for (var i_revenue_index = 0; i_revenue_index < a_recognized_revenue.length; i_revenue_index++) {
        //nlapiLogExecution('audit','prev mnth recog rev:- '+a_recognized_revenue[i_revenue_index]);
        i_total_revenue_recognized_ytd = parseFloat(i_total_revenue_recognized_ytd) + parseFloat(a_recognized_revenue[i_revenue_index]);

    }
	//Margin % as per Plan

	html['plan_margin'].push('Monthly Margin %  as per plan');

	html['plan_margin'].push(s_practice_name);

	html['plan_margin'].push(s_sub_prac_name);

	html['plan_margin'].push( '');

	html['plan_margin'].push( '');

	html['plan_margin'].push('');

	
	var f_total_mar_prcnt = 0;
	if(a_amount_plan.length>0)
	{
		for (var i_amt_mar = 0; i_amt_mar < a_amount_plan.length; i_amt_mar++)
		{
			var f_prcnt_mar_revenue = parseFloat(margin_rev_plan[i_amt_mar]-a_amount_plan[i_amt_mar]) / parseFloat(margin_rev_plan[i_amt_mar]);
			f_prcnt_mar_revenue = f_prcnt_mar_revenue * 100;
			if((!_logValidation(f_prcnt_mar_revenue))||(margin_rev_plan[i_amt_mar]<=0))
				f_prcnt_mar_revenue = 0;
				
			//f_total_mar_prcnt = parseFloat(f_prcnt_mar_revenue) + parseFloat(f_total_mar_prcnt);
			f_total_mar_prcnt=(parseFloat(f_total_revenue_plan)-parseFloat(i_total_row_revenue_plan))/parseFloat(f_total_revenue_plan);
			f_total_mar_prcnt=f_total_mar_prcnt*100;
			if(!_logValidation(f_total_mar_prcnt) || f_total_revenue_plan ==0)
				f_total_mar_prcnt=0;
			
			html['plan_margin'].push(parseFloat(f_prcnt_mar_revenue).toFixed(1)+' %');
		
		}
	}
	else
	{
		for (var mn = 0; mn < monthBreakUp.length; mn++)
		{
				
				html['plan_margin'].push('0');
		}
	}

	html['plan_margin'].push(parseFloat(f_total_mar_prcnt).toFixed(1)+' %');

	//
	//** Margin % per effort
	
	html['crnt_margin'].push( 'Monthly Margin % ');

	html['crnt_margin'].push(s_practice_name);

	html['crnt_margin'].push(s_sub_prac_name);

	html['crnt_margin'].push('');

	html['crnt_margin'].push( '');

	html['crnt_margin'].push( '');

	
	var f_total_mar_prcnt = 0;
	if(a_amount.length>0)
	{
		for (var i_amt_mar = 0; i_amt_mar < a_amount.length; i_amt_mar++)
		{
			var f_prcnt_mar_revenue = parseFloat(margin_rev[i_amt_mar]-a_amount[i_amt_mar]) / parseFloat(margin_rev[i_amt_mar]);
			f_prcnt_mar_revenue = f_prcnt_mar_revenue * 100;
			if((!_logValidation(f_prcnt_mar_revenue))||(margin_rev[i_amt_mar]<=0))
				f_prcnt_mar_revenue = 0;
				
			//f_total_mar_prcnt = parseFloat(f_prcnt_mar_revenue) + parseFloat(f_total_mar_prcnt);
			f_total_mar_prcnt=(parseFloat(f_total_revenue)-parseFloat(i_total_row_revenue))/parseFloat(f_total_revenue);
			f_total_mar_prcnt=f_total_mar_prcnt*100;
			if(!_logValidation(f_total_mar_prcnt) ||f_total_revenue== 0 )
				f_total_mar_prcnt=0;

			html['crnt_margin'].push(parseFloat(f_prcnt_mar_revenue).toFixed(1)+' %');
		}
	}
	else
	{
		for (var mn = 0; mn < monthBreakUp.length; mn++)
		{
				html['crnt_margin'].push('0');
		}
	}
	html['crnt_margin'].push(parseFloat(f_total_mar_prcnt).toFixed(1)+' %');

	
    //Actual revenue to be recognized 

    //Total revenue recognized for previous months
    html_data.push(html);
    nlapiLogExecution('DEBUG', 'HTML', JSON.stringify(html));
    return html;
}*/

function format2(n) {
    n = parseFloat(n).toFixed(1).replace(/(\d)(?=(\d{3})+\.)/g, "$1,");
    n = n.toString().split('.');
    return n[0];
}

function getCurrency_Symbol(s_proj_currency) {
    var s_currency_symbols = {
        'USD': '$', // US Dollar
        'EUR': '€', // Euro
        'GBP': '£', // British Pound Sterling
        'INR': '₹', // Indian Rupee
    };

    if (s_currency_symbols[s_proj_currency] !== undefined) {
        return s_currency_symbols[s_proj_currency];
    }
}

function getMonthsBreakup(d_startDate, d_endDate) {

    try {
        var dateBreakUp = [];
        var new_start_date = new Date(d_startDate.getFullYear(), d_startDate
            .getMonth(), 1);

        var nxt_mnth_strt_date = new Date(d_startDate.getFullYear(), d_startDate
            .getMonth() + 1, 1);

        var endDate = new Date(nxt_mnth_strt_date - 1);

        //var endDate = nlapiAddDays(nlapiAddMonths(d_startDate, 1), -1);
        endDate = nlapiDateToString(endDate, 'date');

        dateBreakUp.push({
            Start: nlapiDateToString(new_start_date, 'date'),
            End: endDate
        });

        for (var i = 1;; i++) {
            var new_date = nlapiAddMonths(new_start_date, i);

            if (new_date > d_endDate) {
                break;
            }

            endDate = nlapiAddDays(nlapiAddMonths(new_date, 1), -1);
            endDate = nlapiDateToString(endDate, 'date');

            dateBreakUp.push({
                Start: nlapiDateToString(new_date, 'date'),
                End: endDate
            });
        }
    } catch (err) {
        nlapiLogExecution('ERROR', 'getMonthsBreakup', 'ERROR MESSAGE :- ' + err);
    }

    return dateBreakUp;
}

function getMonthName(currentDate) {
    currentDate = nlapiStringToDate(currentDate)
    var monthNames = ["JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JUL", "AUG",
        "SEP", "OCT", "NOV", "DEC"
    ];
    return monthNames[currentDate.getMonth()];
}

function MonthNames(yr) {
    var yr1 = Number(yr) + 1;

    var monthNames = ["JAN_" + yr, "FEB_" + yr, "MAR_" + yr, "APR_" + yr, "MAY_" + yr, "JUN_" + yr, "JUL_" + yr, "AUG_" + yr,
        "SEP_" + yr, "OCT_" + yr, "NOV_" + yr, "DEC_" + yr
    ];
    var month_nxt_yr = ["JAN_" + yr1, "FEB_" + yr1, "MAR_" + yr1, "APR_" + yr1, "MAY_" + yr1, "JUN_" + yr1, "JUL_" + yr1, "AUG_" + yr1,
        "SEP_" + yr1, "OCT_" + yr1, "NOV_" + yr1, "DEC_" + yr1
    ];
    monthNames = monthNames.concat(month_nxt_yr);

    return monthNames;
}

function yearData(yr) {
    var yr1 = Number(yr) + 1;

    var monthNames = ["JAN_" + yr, "FEB_" + yr, "MAR_" + yr, "APR_" + yr, "MAY_" + yr, "JUN_" + yr, "JUL_" + yr, "AUG_" + yr,
        "SEP_" + yr, "OCT_" + yr, "NOV_" + yr, "DEC_" + yr
    ];
    var month_nxt_yr = ["JAN_" + yr1, "FEB_" + yr1, "MAR_" + yr1, "APR_" + yr1, "MAY_" + yr1, "JUN_" + yr1, "JUL_" + yr1, "AUG_" + yr1,
        "SEP_" + yr1, "OCT_" + yr1, "NOV_" + yr1, "DEC_" + yr1
    ];
    monthNames = monthNames.concat(month_nxt_yr);

    for (var i = 0; i < monthNames.length; i++) {
        this[monthNames[i]] = {
            Amount: 0,
            prcnt_allocated: 0,
            total_revenue: 0,
            recognized_revenue: 0,
            actual_revenue: 0,
            mnth_strt: 0,
            mnth_end: 0,
            prcent_complt: 0
        };
    }
}

function generate_previous_effrt_revenue(s_effrt_json, projectWiseRevenue_previous_effrt, a_revenue_recognized_for_project, i_year_project) {
    if (s_effrt_json) {
        var i_practice_previous = 0;
        var f_total_revenue_for_tenure = 0;
        //for (var i_mnth_end_plan = 0; i_mnth_end_plan < a_get_mnth_end_effrt_plan.length; i_mnth_end_plan++)
        var s_entire_json_clubed = JSON.parse(s_effrt_json);
        for (var i_mnth_end_plan = 0; i_mnth_end_plan < s_entire_json_clubed.length; i_mnth_end_plan++) {
            var a_row_json_data = JSON.parse(JSON.stringify(s_entire_json_clubed[i_mnth_end_plan]));
            for (var i_row_json_index = 0; i_row_json_index < a_row_json_data.length; i_row_json_index++) {
                var i_practice = a_row_json_data[i_row_json_index].prac;
                var s_practice = a_row_json_data[i_row_json_index].prac_text;
                var i_sub_practice = a_row_json_data[i_row_json_index].subprac;
                var s_sub_practice = a_row_json_data[i_row_json_index].subprac_text;
                var i_role = a_row_json_data[i_row_json_index].role;
                var s_role = a_row_json_data[i_row_json_index].role_text;
                var i_level = a_row_json_data[i_row_json_index].level;
                var s_level = a_row_json_data[i_row_json_index].level_text;
                var i_location = a_row_json_data[i_row_json_index].loc;
                var s_location = a_row_json_data[i_row_json_index].loc_text;
                var f_revenue = a_row_json_data[i_row_json_index].cost;
                if (!f_revenue)
                    f_revenue = 0;

                var f_revenue_share = a_row_json_data[i_row_json_index].share;
                if (!f_revenue_share)
                    f_revenue_share = 0;

                var i_no_of_resources = a_row_json_data[i_row_json_index].allo;
                var s_mnth_strt_date = '1/31/2017';
                var s_mnth_end_date = '31/31/2017';
                var s_mnth = a_row_json_data[i_row_json_index].mnth;
                var s_year = a_row_json_data[i_row_json_index].year;

                var f_revenue_recognized = find_recognized_revenue_prev(a_revenue_recognized_for_project, i_practice, i_sub_practice, i_role, i_level, s_mnth, s_year, a_prev_subprac_searched_once);

                if (!f_revenue_recognized)
                    f_revenue_recognized = 0;

                //nlapiLogExecution('audit','f_revenue_recognized:- '+f_revenue_recognized);

                var s_month_name = s_mnth + '_' + s_year;

                var total_revenue = parseFloat(i_no_of_resources) * parseFloat(f_revenue);
                total_revenue = parseFloat(total_revenue).toFixed(2);

                if (i_practice_previous == 0) {
                    i_practice_previous = a_row_json_data[i_row_json_index].prac_row;
                }

                if (i_practice_previous != a_row_json_data[i_row_json_index].prac_row) {
                    f_total_revenue_for_tenure = 0;
                    i_practice_previous = a_row_json_data[i_row_json_index].prac_row;
                }

                f_total_revenue_for_tenure = parseFloat(f_total_revenue_for_tenure) + parseFloat(total_revenue);
                f_total_revenue_for_tenure = parseFloat(f_total_revenue_for_tenure).toFixed(2);

                var i_prac_subPrac_role_level = i_practice + '_' + i_sub_practice + '_' + i_role + '_' + i_level + '_' + i_location;

                if (!projectWiseRevenue_previous_effrt[i_prac_subPrac_role_level]) {
                    projectWiseRevenue_previous_effrt[i_prac_subPrac_role_level] = {
                        practice_name: s_practice,
                        sub_prac_name: s_sub_practice,
                        sub_practice: i_sub_practice,
                        role_name: s_role,
                        level_name: s_level,
                        location_name: s_location,
                        total_revenue_for_tenure: f_total_revenue_for_tenure,
                        revenue_share: f_revenue_share,
                        RevenueData: {}
                    };

                    projectWiseRevenue_previous_effrt[i_prac_subPrac_role_level].RevenueData[i_practice] = new yearData(i_year_project);

                    projectWiseRevenue_previous_effrt[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].Amount = total_revenue;
                    projectWiseRevenue_previous_effrt[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].prcnt_allocated = i_no_of_resources;
                    projectWiseRevenue_previous_effrt[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].recognized_revenue = f_revenue_recognized;
                    projectWiseRevenue_previous_effrt[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].mnth_strt = s_mnth_strt_date;
                    projectWiseRevenue_previous_effrt[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].mnth_end = s_mnth_end_date;
                } else {
                    projectWiseRevenue_previous_effrt[i_prac_subPrac_role_level].total_revenue_for_tenure = f_total_revenue_for_tenure;

                    projectWiseRevenue_previous_effrt[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].Amount = total_revenue;
                    projectWiseRevenue_previous_effrt[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].prcnt_allocated = i_no_of_resources;
                    projectWiseRevenue_previous_effrt[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].recognized_revenue = f_revenue_recognized;
                    projectWiseRevenue_previous_effrt[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].mnth_strt = s_mnth_strt_date;
                    projectWiseRevenue_previous_effrt[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].mnth_end = s_mnth_end_date;
                }
            }
        }
    }

    //return projectWiseRevenue_previous_effrt;
}

function find_recognized_revenue_prev(a_revenue_recognized_for_project, practice, subpractice, role, level, month, year, a_prev_subprac_searched_once) {
    var f_recognized_amount = 0;

    if (a_prev_subprac_searched_once.indexOf(subpractice) < 0) {
        for (var i_index_find = 0; i_index_find < a_revenue_recognized_for_project.length; i_index_find++) {
            if (practice == a_revenue_recognized_for_project[i_index_find].practice_revenue_recognized) {
                if (subpractice == a_revenue_recognized_for_project[i_index_find].subpractice_revenue_recognized) {
                    //if(role == a_revenue_recognized_for_project[i_index_find].role_revenue_recognized)
                    {
                        //if(level == a_revenue_recognized_for_project[i_index_find].level_revenue_recognized)
                        {
                            if (month == a_revenue_recognized_for_project[i_index_find].month_revenue_recognized) {
                                if (year == a_revenue_recognized_for_project[i_index_find].year_revenue_recognized) {
                                    f_recognized_amount = a_revenue_recognized_for_project[i_index_find].amount_revenue_recognized;
                                    //nlapiLogExecution('audit','f_recognized_amount:- '+f_recognized_amount);
                                    a_prev_subprac_searched_once.push(subpractice);
                                }
                            }
                        }
                    }
                }
            }
        }

        return f_recognized_amount;
    } else {
        return 0;
    }
}

function find_recognized_revenue(a_revenue_recognized_for_project, practice, subpractice, role, level, month, year, a_subprac_searched_once, a_subprac_searched_once_month, a_subprac_searched_once_year) {
    var f_recognized_amount = 0;

    if (func_search_prac_processed_fr_mnth(a_subprac_searched_once, subpractice, month, year) < 0) {
        for (var i_index_find = 0; i_index_find < a_revenue_recognized_for_project.length; i_index_find++) {
            if (practice == a_revenue_recognized_for_project[i_index_find].practice_revenue_recognized) {
                if (subpractice == a_revenue_recognized_for_project[i_index_find].subpractice_revenue_recognized) {
                    //if(role == a_revenue_recognized_for_project[i_index_find].role_revenue_recognized)
                    {
                        //if(level == a_revenue_recognized_for_project[i_index_find].level_revenue_recognized)
                        {
                            if (month == a_revenue_recognized_for_project[i_index_find].month_revenue_recognized) {
                                if (parseInt(year) == parseInt(a_revenue_recognized_for_project[i_index_find].year_revenue_recognized)) {
                                    f_recognized_amount = a_revenue_recognized_for_project[i_index_find].amount_revenue_recognized;
                                    //nlapiLogExecution('audit','f_recognized_amount inside look for prev mnth amnt once pr sub prac:- ',f_recognized_amount);
                                    a_subprac_searched_once.push({
                                        'subpractice': subpractice,
                                        'month': month,
                                        'year': year
                                    });
                                    //a_subprac_searched_once_month.push(month);
                                    //a_subprac_searched_once_year.push(year);

                                    if (subpractice == 325) {
                                        nlapiLogExecution('audit', 'month:- ' + month, 'year:- ' + year);
                                        nlapiLogExecution('audit', 'f_recognized_amount:- ' + f_recognized_amount);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        return f_recognized_amount;
    } else {
        return 0;
    }
}

function func_search_prac_processed_fr_mnth(a_subprac_searched_once, subpractice, month, year) {
    var i_return_var = -1;
    for (var i_loop = 0; i_loop < a_subprac_searched_once.length; i_loop++) {
        if (a_subprac_searched_once[i_loop].subpractice == subpractice) {
            if (a_subprac_searched_once[i_loop].month == month) {
                if (a_subprac_searched_once[i_loop].year == year) {
                    i_return_var = i_loop;
                    break;
                }
            }
        }
    }

    return i_return_var;
}

function find_prac_exist(a_current_pract, i_prac_to_find, i_level_to_find) {
    var i_prac_present = 0;
    for (var i_prac_find = 0; i_prac_find < a_current_pract.length; i_prac_find++) {
        if (a_current_pract[i_prac_find].i_sub_practice == i_prac_to_find && a_current_pract[i_prac_find].i_level == i_level_to_find) {
            i_prac_present = 1;
        }
    }

    return i_prac_present;
}

function getMonthName_FromMonthNumber(month_number) {
    var a_month_name = ["JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JUL", "AUG",
        "SEP", "OCT", "NOV", "DEC"
    ];

    return a_month_name[month_number];
}

function down_excel_function(excel_data) {
    try {
        var strVar1 = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:ss="urn:schemas-microsoft-com:office:spreadsheet" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40">' +
            '<head>' +
            '<meta http-equiv=Content-Type content="text/html; charset=windows-1252"/>' +
            '<meta name=ProgId content=Excel.Sheet/>' +
            '<meta name=Generator content="Microsoft Excel 11"/>' +
            '<!--[if gte mso 9]><xml>' +
            '<x:excelworkbook>' +
            '<x:excelworksheets>' +
            '<x:excelworksheet=sheet1>' +
            '<x:name>** ESTIMATE FILE**</x:name>' +
            '<x:worksheetoptions>' +
            '<x:selected></x:selected>' +
            '<x:freezepanes></x:freezepanes>' +
            '<x:frozennosplit></x:frozennosplit>' +
            '<x:splithorizontal>** FROZEN ROWS + 1 **</x:splithorizontal>' +
            '<x:toprowbottompane>** FROZEN ROWS + 1 **</x:toprowbottompane>' +
            '<x:splitvertical>** FROZEN COLUMNS + 1 **</x:splitvertical>' +
            '<x:leftcolumnrightpane>** FROZEN COLUMNS + 1**</x:leftcolumnrightpane>' +
            '<x:activepane>0</x:activepane>' + // 0
            '<x:panes>' +
            '<x:pane>' +
            '<x:number>3</x:number>' +
            '</x:pane>' +
            '<x:pane>' +
            '<x:number>1</x:number>' +
            '</x:pane>' +
            '<x:pane>' +
            '<x:number>2</x:number>' +
            '</x:pane>' +
            '<x:pane>' +
            '<x:number>0</x:number>' + //1
            '</x:pane>' +
            '</x:panes>' +
            '<x:protectcontents>False</x:protectcontents>' +
            '<x:protectobjects>False</x:protectobjects>' +
            '<x:protectscenarios>False</x:protectscenarios>' +
            '</x:worksheetoptions>' +
            '</x:excelworksheet>' +
            '</x:excelworksheets>' +
            '<x:protectstructure>False</x:protectstructure>' +
            '<x:protectwindows>False</x:protectwindows>' +
            '</x:excelworkbook>' +

            // '<style>'+

            //-------------------------------------
            '</x:excelworkbook>' +
            '</xml><![endif]-->' +
            '<style>' +
            'p.MsoFooter, li.MsoFooter, div.MsoFooter' +
            '{ margin:0in; margin-bottom:.0001pt; mso-pagination:widow-orphan; tab-stops:center 3.0in right 6.0in; font-size:12.0pt;}' +
            '<style>' +

            '<!-- /* Style Definitions */' +

            //'@page Section1'+
            //'{ size:8.5in 11.0in; margin:1.9cm 1.27cm 1.9cm 1.27cm; mso-header-margin:.5in; mso-footer-margin:.5in; mso-title-page:yes; mso-header: h1; mso-footer: f1; mso-first-header: fh1; mso-first-footer: ff1; mso-paper-source:0;}'+

            'div.Section1' +
            '{ page:Section1;}' +

            'table#hrdftrtbl' +
            '{ margin:0in 0in 0in 900in; width:1px; height:1px; overflow:hidden; font-family:Arial;}-->' +

            '</style>' +


            '</head>' +


            '<body>' +

            '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">'

        var strVar2 = '';
        strVar2 += "<table width=\"100%\" >";
		strVar2+= '<table width="100%" border="1">';
        if (excel_data) //
        {
            strVar2 += "<tr>";
		//	strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" style='background-color:#999999'\> </td>";
            strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" style='background-color:#999999'\>Project</td>";
          //  strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" style='background-color:#999999'\>Month</td>";
            //strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" style='background-color:#999999'\>Amount</td>";
            //strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" style='background-color:#999999' \>Role</td>";
           // strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" style='background-color:#999999' \>Location</td>";
            //strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" style='background-color:#999999' \>   </td>";
            nlapiLogExecution('audit', 'down_excel_function transcation length:- ', excel_data.length);
			var months=excel_data[0].month;
			var startDate = '1/1/2018';
			startDate = nlapiStringToDate(startDate);
			for (var i = 0; i <= 12; i++)
			{
			//nlapiYieldScript();
			var d_day = nlapiAddMonths(startDate, i);
			var month_name= getMonthName_FromMonthNumber(d_day.getMonth());
			//month_name = monthNameArray.indexOf(month_name);
			strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" style='background-color:#999999' \>" + month_name + "</td>";
			}
		/*	for (mnth_ind = 0; mnth_ind < months.length; mnth_ind++)
			{
				strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" style='background-color:#999999' \>" + excel_data[0].month[mnth_ind] + "</td>";
			}
			strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" style='background-color:#999999' \>Total</td>";
			strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" style='background-color:#999999' \>Currency</td>";*/
			strVar2 += "<\/tr>";
		
			for(var ind=0;ind<excel_data.length;ind++)
			{
				strVar2 += "<tr>";
				var effort_months=excel_data[ind];
				
				for(var effor_mn = 0;effor_mn < effort_months.length; effor_mn++)
				{
					if(effor_mn==0)
					{
					strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \>" + excel_data[ind][effor_mn].projectName+ "</td>";
					var month_selected = excel_data[ind][effor_mn].plan_month;
					var month_index = monthNameArray.indexOf(month_selected);
					for(month_gap =0; month_gap < month_index;month_gap++)
					{
						strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \></td>";
					}
					}
					//strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \>" + excel_data[ind][effor_mn].plan_month + "</td>";
					strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \>" + excel_data[ind][effor_mn].plan_amount + "</td>";
					              
				}
				
				strVar2 += "</tr>";
				var sub_totl=excel_data[ind].sub_total_plan;
				
				/*strVar2 += "<tr>";
				var effort=excel_data[ind].effort_total;
				
				
				
					//
				strVar2 += "<tr>";
				var plan_cost=excel_data[ind].plan_cost;
				for (plan_cst_ind=0;plan_cst_ind <plan_cost.length;plan_cst_ind++)
				{
					
					strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \>" + excel_data[ind].plan_cost[plan_cst_ind] + "</td>";
					
					                  
				}
				strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \></td>";
				strVar2 += "</tr>";
				strVar2 += "<tr>";
				var current_cost=excel_data[ind].current_cost;
				for (cst_ind=0;cst_ind <current_cost.length;cst_ind++)
				{
					
					strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \>" + excel_data[ind].current_cost[cst_ind] + "</td>";
					
					                  
				}
				strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \></td>";
				strVar2 += "</tr>";		
				//
				strVar2 += "<tr>";
				var rev_plan=excel_data[ind].actual_rev_plan;
				for (rev_ind=0;rev_ind <rev_plan.length;rev_ind++)
				{
					strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \>" + excel_data[ind].actual_rev_plan[rev_ind] + "</td>";                
				}
				strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \>" + excel_data[ind].currency + "</td>";
				strVar2 += "</tr>";
				strVar2 += "<tr>";
				var rev=excel_data[ind].actual_rev;
				for (rev_indx=0;rev_indx <rev.length;rev_indx++)
				{
					strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \>" + excel_data[ind].actual_rev[rev_indx] + "</td>";                  
				}
				strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \>" + excel_data[ind].currency + "</td>"; 
				strVar2 += "</tr>";
				strVar2 += "<tr>";
				var plan_margin=excel_data[ind].plan_margin;
				for (plan_margin_ind=0;plan_margin_ind <plan_margin.length;plan_margin_ind++)
				{
					
					strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \>" + excel_data[ind].plan_margin[plan_margin_ind] + "</td>";
					
					                  
				}
				strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \></td>";
				strVar2 += "</tr>";
				strVar2 += "<tr>";
				var margin=excel_data[ind].crnt_margin;
				for (margin_ind=0;margin_ind <margin.length;margin_ind++)
				{
					
					strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \>" + excel_data[ind].crnt_margin[margin_ind] + "</td>";
					
					                  
				}
				strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \></td>";
				strVar2 += "</tr>";*/
			}
		
           
          

        } else {
            //NO RESULTS FOUND
        }

        strVar2 += "<\/table>";
        strVar1 = strVar1 + strVar2;
        var file = nlapiCreateFile('AuditReport.xls', 'XMLDOC', strVar1);
        nlapiLogExecution('debug', 'file:- ', file);
      // response.setContentType('XMLDOC', 'Auditreport.xls');
       //response.write(file.getValue());
		nlapiSendEmail(7905,'sai.vannamareddy@brillio.com' , 'Project Revenue Recognized Report', 'Please find the attachement of project rev rec report',null, 'sai.vannamareddy@brillio.com',null, file, true, null, null);

    } catch (err) {
        nlapiLogExecution('ERROR', 'ERROR MESSAGE:- ', err);
    }
}
function _logValidation(value) 
{
 if(value!='null' && value != null && value != null && value != '' && value != undefined && value != undefined && value != 'undefined' && value!= 'undefined'&& value!= 'NaN' && value != NaN) 
 {
  return true;
 }
 else 
 { 
  return false;
 }
 }
 function generate_total_project_view(projectWiseRevenue,monthBreakUp,i_year_project,i_project_mnth,mode,i_year_project_end,i_prject_end_mnth,data_json_mn,data_array)
{
	
	var context = nlapiGetContext();
	

	var html = "";
	
	
	
	var today = new Date();
	var currentMonthName = getMonthName(nlapiDateToString(today));
	
	for (var j = 0; j < monthBreakUp.length; j++)
	{
		var months = monthBreakUp[j];
		var s_month_name = getMonthName(months.Start); // get month name
		var s_year = nlapiStringToDate(months.Start)
		s_year = s_year.getFullYear();
		s_month_name = s_month_name+'_'+s_year;
				
	
	}
		

	
	var a_sub_prac_already_displayed = new Array();
	var i_subpractice_id = 0;
	var a_amount = new Array();
	var months_ref = new Array();
	var i_frst_row = 0;
	var i_diplay_frst_column = 0;
	var i_total_row_revenue = 0;
	var total_prcnt_aloocated = new Array();
	var b_first_line_copied = 0;
	var f_total_allocated_row = 0;
	var i_total_row_revenue = 0;
	var a_sub_practice_arr = new Array();
	
	for ( var emp_internal_id in projectWiseRevenue)
	{
		for ( var project_internal_id in projectWiseRevenue[emp_internal_id].RevenueData)
		{
			var i_sub_practice_internal_id = 0;
			var i_amt = 0;
			
			if (a_sub_practice_arr.indexOf(projectWiseRevenue[emp_internal_id].sub_practice) < 0)
			{
				for (var month in projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id])
				{
					var a_MonthNames = MonthNames(i_year_project);
					var currentMonthPos = a_MonthNames.indexOf(month);
					var s_current_month = a_MonthNames[currentMonthPos];
					var s_current_mnth_yr = s_current_month.substr(4, 8);
					var i_nxt_yr_flg = 0;
					
					if(currentMonthPos >=12)
					{
						i_nxt_yr_flg = 1;
						currentMonthPos = currentMonthPos - 12;
					}
					
					var i_curr_yr_dis_flag = 0;
					if(parseInt(s_current_mnth_yr) < i_year_project_end && i_prject_end_mnth < currentMonthPos)
					{
						i_curr_yr_dis_flag = 1;
					}
					
					if (((i_project_mnth <= currentMonthPos && ((i_prject_end_mnth >= currentMonthPos || i_curr_yr_dis_flag == 1) && i_nxt_yr_flg == 0) && parseInt(s_current_mnth_yr) <= i_year_project) && parseInt(s_current_mnth_yr) <= i_year_project_end) || (i_prject_end_mnth >= currentMonthPos && parseInt(s_current_mnth_yr) <= i_year_project_end && i_nxt_yr_flg == 1))
					{
						var total_revenue_format = parseFloat(projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id][month].total_revenue);
						
						if (i_frst_row == 0)
						{
							a_amount.push(total_revenue_format);
						}
						else
						{
							var amnt_mnth = a_amount[i_amt];
							amnt_mnth = parseFloat(amnt_mnth) + parseFloat(projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id][month].total_revenue);
							a_amount[i_amt] = amnt_mnth;
							i_amt++;
						}
						months_ref.push(month);
						a_sub_practice_arr.push(projectWiseRevenue[emp_internal_id].sub_practice);
						
					}
				}
			}
			i_frst_row = 1;
	
			var f_revenue_share = projectWiseRevenue[emp_internal_id].revenue_share;			
		}
	}
	

	for (var i_amt = 0; i_amt < a_amount.length; i_amt++)
	{
		
		if(monthIndex(months_ref[i_amt])>=0)
		{
			var mnth_indx = data_array.indexOf(months_ref[i_amt]);
			if(mnth_indx<0)
			{
				data_json_mn.push({
					month:months_ref[i_amt],
					amount:a_amount[i_amt]
				});
				data_array.push(months_ref[i_amt]);
			}
			else
			{
				data_json_mn[mnth_indx]={
				month:months_ref[i_amt],
				amount:a_amount[i_amt]
				};
			}
		}
		i_total_row_revenue = parseFloat(i_total_row_revenue) + parseFloat(a_amount[i_amt]);
		
	}
	

	
	return data_json_mn;
}
function getDatediffIndays(startDate, endDate)
{
    var one_day=1000*60*60*24;
    var fromDate = startDate;
    var toDate=endDate

	//var date1 = nlapiStringToDate(fromDate);

   //var date2 = nlapiStringToDate(toDate);
	
	var date1 = fromDate;
	var date2 = toDate;
	
    var date3=Math.round((date2-date1)/one_day);

    return (date3+1);
}
function generate_cost_view(projectWiseRevenue,monthBreakUp,i_year_project,i_project_mnth,mode,sub_prac,i_year_project_end,i_prject_end_mnth)
{
	

	var html = "";

		
	
	var today = new Date();
	var currentMonthName = getMonthName(nlapiDateToString(today));
	
	for (var j = 0; j < monthBreakUp.length; j++)
	{
		var months = monthBreakUp[j];
		var s_month_name = getMonthName(months.Start); // get month name
		var s_year = nlapiStringToDate(months.Start)
		s_year = s_year.getFullYear();
		s_month_name = s_month_name+'_'+s_year;
	
	}
		

	
	var a_sub_prac_already_displayed = new Array();
	var i_subpractice_id = 0;
	var a_amount = new Array();
	var i_frst_row = 0;
	var i_diplay_frst_column = 0;
	var i_total_row_revenue = 0;
	var total_prcnt_aloocated = new Array();
	var b_first_line_copied = 0;
	var f_total_allocated_row = 0;
	var f_total_per_row_cost = 0;
	
	for (var emp_internal_id in projectWiseRevenue)
	{
		if (projectWiseRevenue[emp_internal_id].sub_practice == sub_prac)
		{
			f_total_allocated_row = 0;
			for (var project_internal_id in projectWiseRevenue[emp_internal_id].RevenueData)
			{
				
				var i_index_mnth = 0;
				//nlapiLogExecution('audit','project year :- ',i_year_project);
				//nlapiLogExecution('audit','project month :- ',i_project_mnth);
				for (var month in projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id])
				{
					var a_MonthNames = MonthNames(i_year_project);
					var currentMonthPos = a_MonthNames.indexOf(month);
					var s_current_month = a_MonthNames[currentMonthPos];
					var s_current_mnth_yr = s_current_month.substr(4, 8);
					
					var i_nxt_yr_flg = 0;
					if(currentMonthPos >=12)
					{
						currentMonthPos = currentMonthPos - 12;
						i_nxt_yr_flg = 1;
					}
					
					var i_curr_yr_dis_flag = 0;
					if(parseInt(s_current_mnth_yr) < i_year_project_end && i_prject_end_mnth < currentMonthPos)
					{
						i_curr_yr_dis_flag = 1;
					}
					
					//nlapiLogExecution('audit','currentMonthPos::::'+currentMonthPos,'i_project_mnth:- '+i_project_mnth+':: i_prject_end_mnth:- '+i_prject_end_mnth);
					if (((i_project_mnth <= currentMonthPos && ((i_prject_end_mnth >= currentMonthPos || i_curr_yr_dis_flag == 1) && i_nxt_yr_flg == 0) && parseInt(s_current_mnth_yr) <= i_year_project) && parseInt(s_current_mnth_yr) <= i_year_project_end) || (i_prject_end_mnth >= currentMonthPos && parseInt(s_current_mnth_yr) <= i_year_project_end && i_nxt_yr_flg == 1))
					{
						//nlapiLogExecution('audit','i_project_mnth:- '+i_project_mnth+':::i_prject_end_mnth:- '+i_prject_end_mnth+'::::currentMonthPos:- '+currentMonthPos,s_current_mnth_yr);
						if(b_first_line_copied == 1)
						{
							var f_allocated_prcnt = total_prcnt_aloocated[i_index_mnth];
							f_allocated_prcnt = parseFloat(f_allocated_prcnt) + parseFloat(projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id][month].prcnt_allocated);
							total_prcnt_aloocated[i_index_mnth] = f_allocated_prcnt;
							i_index_mnth++;							
						}
						else
						{
							total_prcnt_aloocated.push(projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id][month].prcnt_allocated);
						}
						
						f_total_allocated_row = parseFloat(projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id][month].prcnt_allocated) + parseFloat(f_total_allocated_row);
						
						
					}
				}
			}
			
			b_first_line_copied = 1;
			
			
			
			var s_sub_prac_name = projectWiseRevenue[emp_internal_id].sub_prac_name;
			var s_practice_name = projectWiseRevenue[emp_internal_id].practice_name;
			
			i_diplay_frst_column = 1;
		}
	}
	i_diplay_frst_column = 0;
	
	
	var f_total_row_allocation = 0;
	for(var i_index_total_allocated=0; i_index_total_allocated<total_prcnt_aloocated.length; i_index_total_allocated++)
	{
		
		
		f_total_row_allocation = parseFloat(f_total_row_allocation) + parseFloat(total_prcnt_aloocated[i_index_total_allocated]);
	}
	
	

	
	for ( var emp_internal_id in projectWiseRevenue) {
		f_total_per_row_cost = 0;
		for ( var project_internal_id in projectWiseRevenue[emp_internal_id].RevenueData) {
			
			if(projectWiseRevenue[emp_internal_id].sub_practice == sub_prac)
			{
					
				var i_sub_practice_internal_id = 0;
				var i_amt = 0;
				for (var month in projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id])
				{
					var a_MonthNames = MonthNames(i_year_project);
					var currentMonthPos = a_MonthNames.indexOf(month);
					var s_current_month = a_MonthNames[currentMonthPos];
					var s_current_mnth_yr = s_current_month.substr(4, 8);
					var i_nxt_yr_flg = 0;
					
					if(currentMonthPos >=12)
					{
						i_nxt_yr_flg = 1;
						currentMonthPos = currentMonthPos - 12;
					}
					
					var i_curr_yr_dis_flag = 0;
					if(parseInt(s_current_mnth_yr) < i_year_project_end && i_prject_end_mnth < currentMonthPos)
					{
						i_curr_yr_dis_flag = 1;
					}
					
					if (((i_project_mnth <= currentMonthPos && ((i_prject_end_mnth >= currentMonthPos || i_curr_yr_dis_flag == 1)&& i_nxt_yr_flg == 0) && parseInt(s_current_mnth_yr) <= i_year_project) && parseInt(s_current_mnth_yr) <= i_year_project_end) || (i_prject_end_mnth >= currentMonthPos && parseInt(s_current_mnth_yr) <= i_year_project_end && i_nxt_yr_flg == 1))
					{
						
						if (mode == 2)
						{
							
							//html += "<td class='monthly-amount'>";
							var total_revenue_format = parseFloat(projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id][month].Amount).toFixed(1);
							//html += '$ '+format2(total_revenue_format);
							
							f_total_per_row_cost = parseFloat(projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id][month].Amount) + parseFloat(f_total_per_row_cost);
							
							if(i_frst_row == 0)
							{
								a_amount.push(total_revenue_format);
							}
							else
							{
								var amnt_mnth = a_amount[i_amt];
								amnt_mnth = parseFloat(amnt_mnth) + parseFloat(projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id][month].Amount);
								
								a_amount[i_amt] = amnt_mnth;
								i_amt++;
							}
						}
						
						//html += "</td>";
					}
				}
				
				if (mode == 2)
				{
					
					
					//i_total_row_revenue = parseFloat(i_total_row_revenue) + parseFloat(projectWiseRevenue[emp_internal_id].total_revenue_for_tenure);
					i_total_row_revenue = parseFloat(i_total_row_revenue) + parseFloat(f_total_per_row_cost);
				}
				
				i_frst_row = 1;
	
				
				
				i_diplay_frst_column = 1;
				
				var f_revenue_share = projectWiseRevenue[emp_internal_id].revenue_share;
			}			
		}
	}
	
	var s_sub_prac_name = '';
	for (var emp_internal_id in projectWiseRevenue)
	{
		if (projectWiseRevenue[emp_internal_id].sub_practice == sub_prac)
		{
			s_sub_prac_name = projectWiseRevenue[emp_internal_id].sub_prac_name;
			s_practice_name = projectWiseRevenue[emp_internal_id].practice_name;
			projectWiseRevenue[emp_internal_id].revenue_share_revised = i_total_row_revenue;
			projectWiseRevenue[emp_internal_id].total_revenue_for_tenure = i_total_row_revenue;
		}
	}
	

	
	// Percent row
	
	
	var f_total_prcnt = 0;
	for (var i_amt = 0; i_amt < a_amount.length; i_amt++)
	{
		var f_prcnt_revenue = parseFloat(a_amount[i_amt]) / parseFloat(i_total_row_revenue);
		f_prcnt_revenue = f_prcnt_revenue * 100;
		f_total_prcnt = parseFloat(f_prcnt_revenue) + parseFloat(f_total_prcnt);
	
	}
	


	var f_total_revenue = 0;
	for (var i_amt = 0; i_amt < a_amount.length; i_amt++)
	{
		var f_prcnt_revenue = parseFloat(a_amount[i_amt]) / parseFloat(i_total_row_revenue);
		f_prcnt_revenue = f_prcnt_revenue * 100;
		
		var f_revenue_amount = (parseFloat(f_prcnt_revenue) * parseFloat(f_revenue_share)) / 100;
		
		f_total_revenue = parseFloat(f_revenue_amount) + parseFloat(f_total_revenue);
		
		
	}
	
	for (var emp_internal_id in projectWiseRevenue)
	{
		for (var project_internal_id in projectWiseRevenue[emp_internal_id].RevenueData)
		{
			var i_amount_map = 0;
			if (projectWiseRevenue[emp_internal_id].sub_practice == sub_prac)
			{
				for (var month in projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id])
				{
					var a_MonthNames = MonthNames(i_year_project);
					var currentMonthPos = a_MonthNames.indexOf(month);
					var s_current_month = a_MonthNames[currentMonthPos];
					var s_current_mnth_yr = s_current_month.substr(4, 8);
					var i_nxt_yr_flg = 0;
					
					if(currentMonthPos >=12)
					{
						i_nxt_yr_flg = 1;
						currentMonthPos = currentMonthPos - 12;
					}
					
					var i_curr_yr_dis_flag = 0;
					if(parseInt(s_current_mnth_yr) < i_year_project_end && i_prject_end_mnth < currentMonthPos)
					{
						i_curr_yr_dis_flag = 1;
					}
					
					if (((i_project_mnth <= currentMonthPos && ((i_prject_end_mnth >= currentMonthPos || i_curr_yr_dis_flag == 1) && i_nxt_yr_flg == 0) && parseInt(s_current_mnth_yr) <= i_year_project) && parseInt(s_current_mnth_yr) <= i_year_project_end) || (i_prject_end_mnth >= currentMonthPos && parseInt(s_current_mnth_yr) <= i_year_project_end && i_nxt_yr_flg == 1))
					{
						var f_prcnt_revenue = parseFloat(a_amount[i_amount_map]) / parseFloat(i_total_row_revenue);
						f_prcnt_revenue = f_prcnt_revenue * 100;
						
						var f_revenue_amount = (parseFloat(f_prcnt_revenue) * parseFloat(f_revenue_share)) / 100;
		
						projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id][month].total_revenue = f_revenue_amount;
						i_amount_map++;
					}
				}	
			}
		}
	}
		
	return html;
}
function monthIndex(month)
{
	 var monthArray = ["JAN_2018", "FEB_2018", "MAR_2018", "APR_2018", "MAY_2018", "JUN_2018", "JUL_2018", "AUG_2018",
        "SEP", "OCT", "NOV", "DEC"
    ];
	return monthArray.indexOf(month);
   // return monthArray[month_number];

}