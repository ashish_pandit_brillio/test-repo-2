/**
 * Send an email if employee has already entered timesheets for the entered
 * period
 * 
 * Version Date Author Remarks 1.00 11 May 2016 Nitish Mishra
 * 
 */

function userEventAfterSubmit(type) {
	try {

		if (type == 'create') {

			var startDate = nlapiGetFieldValue('startdate');
			var endDate = nlapiGetFieldValue('enddate');
			var employee = nlapiGetFieldValue('allocationresource');
			var project = nlapiGetFieldValue('project');

			if (employee && project && startDate && endDate) {
				// get timesheet details for this allocation period
				var timesheetData = getTimesheets(employee, project, startDate,
				        endDate);

				// send mail only if timesheet found
				if (timesheetData) {
					var mailTemplate = getTimesheetExistMailTemplate(employee,
					        project, startDate, endDate, timesheetData);

					var emailId = "business.ops@brillio.com; information.systems@brillio.com;timesheet@brillio.com";

					nlapiSendEmail(442, emailId, mailTemplate.MailSubject,
					        mailTemplate.MailBody, null,
					        'nitish.mishra@brillio.com');
				}
			}
		}
		if (type == 'edit') {

		var oldRec = nlapiGetOldRecord();
		var newRec = nlapiGetNewRecord();
		var old_date = oldRec.getFieldValue('enddate');
		var new_date = newRec.getFieldValue('enddate');
			var startDate = nlapiGetFieldValue('startdate');
			var endDate = nlapiGetFieldValue('enddate');
			var employee = nlapiGetFieldValue('allocationresource');
			var project = nlapiGetFieldValue('project');
			var loadRec = nlapiLoadRecord(nlapiGetRecordType(),nlapiGetRecordId());
			var last_notify_date = nlapiGetFieldValue('custevent_30_days_prior_notification');
			var date = loadRec.getFieldValue('custevent_30_days_prior_notification');
			if(old_date != new_date){
			loadRec.setFieldValue('custevent_30_days_prior_notification','');
			nlapiSubmitRecord(loadRec);
			}
			
		}
	} catch (err) {
		nlapiLogExecution('ERROR', 'userEventAfterSubmit', err);
	}
}

function getTimesheets(employee, project, startDate, endDate) {
	try {
		var timeEntrySearch = searchRecord('timeentry', null, [
		        new nlobjSearchFilter('employee', null, 'anyof', employee),
		        new nlobjSearchFilter('customer', null, 'anyof', project),
		        new nlobjSearchFilter('type', null, 'anyof', 'A'),
		        new nlobjSearchFilter('date', null, 'within', startDate,
		                endDate) ], [
		        new nlobjSearchColumn('approvalstatus', 'timesheet', 'group'),
		        new nlobjSearchColumn('startdate', 'timesheet', 'group')
		                .setSort(),
		        new nlobjSearchColumn('enddate', 'timesheet', 'group') ]);

		var timesheetData = "";

		if (timeEntrySearch) {
			var rows = "";

			timeEntrySearch.forEach(function(timesheet) {
				rows += "<tr>";
				rows += "<td>";
				rows += timesheet.getValue('startdate', 'timesheet', 'group');
				rows += "</td>";
				rows += "<td>";
				rows += timesheet.getValue('enddate', 'timesheet', 'group');
				rows += "</td>";
				rows += "<td>";
				rows += timesheet.getText('approvalstatus', 'timesheet',
				        'group');
				rows += "</td>";
				rows += "</tr>";
			});

			if (rows) {
				timesheetData += "<table border=1>";
				timesheetData += "<tr style='background:#E3E1E0'>";
				timesheetData += "<td>";
				timesheetData += "Week Start Date";
				timesheetData += "</td>";
				timesheetData += "<td>";
				timesheetData += "Week End Date";
				timesheetData += "</td>";
				timesheetData += "<td>";
				timesheetData += "Status";
				timesheetData += "</td>";
				timesheetData += "</tr>";
				timesheetData += rows;
				timesheetData += "</table>";
			}
		}

		return timesheetData;
	} catch (err) {
		nlapiLogExecution('ERROR', 'getTimesheets', err);
		throw err;
	}
}

function getTimesheetExistMailTemplate(employee, project, startDate, endDate,
        timesheetData)
{
	try {
		var employeeName = nlapiLookupField('employee', employee, 'entityid');
		var projectDetails = nlapiLookupField('job', project, [ 'altname',
		        'entityid' ]);

		var htmltext = '';
		htmltext += '<p>Hi All,</p>';

		htmltext += '<p>This is inform you that '
		        + employeeName
		        + ' had already filled the timesheet for the period '
		        + startDate
		        + ' - '
		        + endDate
		        + ' and some changes have been made in his allocation of this period.</p>';

		htmltext += "<p><b>Resource : </b>" + employeeName + "<br/>";
		htmltext += "<b>Project : </b>" + projectDetails.entityid + " : "
		        + projectDetails.altname + "<br/>";
		htmltext += "<b>Start Date : </b>" + startDate + "<br/>";
		htmltext += "<b>End Date : </b>" + endDate + "<br/>";
		htmltext += "<b>By : </b>" + nlapiGetContext().getName() + "<br/>";
		htmltext += "<b>On : </b>"
		        + nlapiDateToString(new Date(), 'datetimetz') + "</p>";

		htmltext += timesheetData;

		htmltext += '<p>Regards,<br/>';
		htmltext += 'Information Systems</p>';

		nlapiLogExecution('debug', 'mail content', htmltext);

		return {
		    MailBody : addMailTemplate(htmltext),
		    MailSubject : "Allocation Change - Timesheet Exists - "
		            + employeeName + " : " + startDate + '-' + endDate
		};
	} catch (err) {
		nlapiLogExecution('ERROR', 'getTimesheetExistMailTemplate', err);
	}
}

function _logValidation(value) 
{
 if(value!='null' && value != null && value != null && value != '' && value != undefined && value != undefined && value != 'undefined' && value!= 'undefined'&& value!= 'NaN' && value != NaN) 
 {
  return true;
 }
 else 
 { 
  return false;
 }
}