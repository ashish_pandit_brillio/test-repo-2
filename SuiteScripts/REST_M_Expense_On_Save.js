/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       05 Aug 2016     deepak.srinivas
 *
 */

/**
 * @param {Object} dataIn Parameter object
 * @returns {Object} Output object
 */

function postRESTlet(dataIn) {
	
	try{
		var response = new Response();
		var current_date = nlapiDateToString(new Date());
		//Log for current date
		nlapiLogExecution('DEBUG', 'Current Date', 'current_date...' + current_date)
		nlapiLogExecution('debug', 'dataIn', JSON.stringify(dataIn));
		
		var employeeId =  getUserUsingEmailId(dataIn.EmailId);
		var requestType = dataIn.RequestType;
		var expense_ID = dataIn.Data.expenseId;
		
		nlapiLogExecution('debug', 'requestType', requestType);
		nlapiLogExecution('debug', 'employeeId requestType expense_ID', employeeId+requestType+expense_ID);

		
		//Added logic for expense delete .. 6th FEB 2016
		if(expense_ID)
		var i_expense_Id = getExpenseInternalID(expense_ID);
		/*var employeeId =  getUserUsingEmailId(Email_Id);

		var requestType = 'UPDATE';
		//var employeeId = getUserUsingEmailId(Email_Id);
		
		//var expense_ID = dataIn.expenseId;
		var expense_ID = 'EXP7089';*/
		//var project_ID = 'Information System';
		
		//nlapiLogExecution('DEBUG', 'Current Date', 'employeeId' + employeeId);
	//	nlapiLogExecution('DEBUG', 'Current Date', 'project_ID' + project_ID);
		//var requestType = dataIn.RequestType;
		
		/*for(var  i=0; i< obj.category.length;i++){
			var id_1 = obj.category[i].id;
			var category = obj.category[i].category;
			var category = obj.category[i].category;
			var amount = obj.category[i].amount;
			var project = obj.category[i].project;
			var currency = obj.category[i].currency;
			var Memo = obj.category[i].Memo;
			
		}*/
		switch (requestType) {

		case M_Constants.Request.Get:

			if (employeeId) {
				response.Data = onSaveEventExpense(dataIn,employeeId);
				response.Status = true;
			} else {
				response.Data = "Some error with the data sent";
				response.Status = false;
		}
			break;
		case M_Constants.Request.GetAll:
			if(employeeId){
				response.Data = getExpensesList(employeeId);
				response.Status = true;				
			} else {
				response.Data = "Some error with the data sent";
				response.Status = false;
		}
		break;
		case M_Constants.Request.GetOne:
			if(_logValidation(expense_ID) && _logValidation(i_expense_Id)){
				response.Data = getExpensesDetails(expense_ID);
				response.Status = true;				
			} else {
				response.Data = "Particular expense deleted !!";
				response.Status = false;
		}
		break;
		case M_Constants.Request.Update:
			if(expense_ID){
				response.Data = updateExpense(expense_ID,dataIn);
				response.Status = true;				
			} else {
				response.Data = "Cannot call without expenseID";
				response.Status = false;
		}
		break;
		case M_Constants.Request.Delete:
			if(expense_ID){
				response.Data = deleteExpenseReport(expense_ID);
				response.Status = true;				
			} else {
				response.Data = "Cannot call without expenseID";
				response.Status = false;
		}
		break;
	}
		
	}
	catch (err) {
		nlapiLogExecution('ERROR', 'postRESTlet', err);
		response.Data = err;
		response.Status = false;
	}
	nlapiLogExecution('debug', 'response', JSON.stringify(response));
	return response;
}

//Deleting The expense Report
function deleteExpenseReport(expense_ID){
	
	try{
		var getExpensID = getExpenseInternalID(expense_ID);
		var loadRec = nlapiLoadRecord('expensereport',getExpensID);
		var status = loadRec.getFieldValue('status');
		/*if(status =='Approved' || status == 'Approved (Overridden) by Accounting' || status == 'Approved by Accounting'){
			throw 'Cannot delete approved expenses';
		}*/
		
		nlapiDeleteRecord('expensereport', getExpensID);
		
		var msg = null;
		return msg;
	}
	catch(e){
		
		nlapiLogExecution('ERROR', 'Error in deleting the expense', e);
		throw e;
	}
}
//Updating the expense details
function updateExpense(exp_id,dataIn){
	try{
		nlapiLogExecution('DEBUG', 'JSON DATA in Update', JSON.stringify(dataIn));
		var expense_InternalID = getExpenseInternalID(exp_id);
		
		nlapiLogExecution('DEBUG', 'JSON DATA in Update expense_InternalID', expense_InternalID);
		var loadExpense = nlapiLoadRecord('expensereport',expense_InternalID);
		var check_status = loadExpense.getFieldValue('status'); //statusRef
		var check_status_ref = loadExpense.getFieldValue('statusRef');
		nlapiLogExecution('DEBUG', 'check_status_ref', check_status_ref);
		nlapiLogExecution('DEBUG', 'check_status', check_status); //supervisorapproval accountingapproval complete
		var check_supervisor_approval = loadExpense.getFieldValue('supervisorapproval');
		var check_accountingapproval = loadExpense.getFieldValue('accountingapproval'); 
		var check_complete = loadExpense.getFieldValue('complete'); 
		nlapiLogExecution('DEBUG', 'check_supervisor_approval', check_supervisor_approval);
		nlapiLogExecution('DEBUG', 'check_accountingapproval', check_accountingapproval);
			nlapiLogExecution('DEBUG', 'check_complete', check_complete);
		//if(check_status != 'In Progress' || check_status != 'inProgress' || check_status_ref != 'inProgress'){
		if(check_supervisor_approval == 'T' || check_accountingapproval == 'T' || check_complete == 'T'){
		throw 'Expense Already Submitted';
		}
		var empID = loadExpense.getFieldValue('entity');
		var employeeLookup_U = nlapiLookupField('employee',parseInt(empID),['entityid']);
		
		var employee_full_name_U = employeeLookup_U.entityid;
		//Get data
		var purpose = dataIn.Data.obj.purpose;
		loadExpense.setFieldValue('memo',purpose);
		var complete = dataIn.Data.complete;
		nlapiLogExecution('debug', 'complete', complete);
		
		if(complete == false){
			loadExpense.setFieldValue('complete','F');
			}
			else{
				loadExpense.setFieldValue('complete','T');
			}
		
		
		var obj ={};
		var obj = dataIn.Data.obj.category;
		nlapiLogExecution('debug', 'Inside Save obj', JSON.stringify(obj));
		nlapiLogExecution('DEBUG','Expenses Length',obj.length);
		
		//Removing Line Items
		var lineCount = loadExpense.getLineItemCount('expense');
		nlapiLogExecution('DEBUG','Update Expenses Line Length',lineCount);
		for(var n=lineCount ; n>=1; n--){
			loadExpense.removeLineItem('expense',n);
		}
		
		for(var j=0;j<obj.length;j++){
			loadExpense.selectNewLineItem('expense');
			
			loadExpense.setCurrentLineItemValue('expense','refnumber',j+1);
			nlapiLogExecution('DEBUG','ID',j+1);			
			loadExpense.setCurrentLineItemValue('expense','memo',obj[j].Memo); 
			nlapiLogExecution('DEBUG','Memo',obj[j].Memo);		
			
			nlapiLogExecution('DEBUG','catProject',obj[j].Project);
			
			var project = projectSplit(obj[j].Project);
			
			nlapiLogExecution('DEBUG','projectSplit',obj[j].Project);
			var projectInternalID = projectDetailSearch(project);
			nlapiLogExecution('DEBUG','projectInternalID',projectInternalID);
			
			loadExpense.setCurrentLineItemValue('expense','customer',projectInternalID); 
			loadExpense.setCurrentLineItemText('expense','category',obj[j].Category); 
			nlapiLogExecution('DEBUG','category',obj[j].Category);
			if((obj[j].imgData != '' || obj[j].imgData != null) && obj[j].imgData ){
			//Update Image
			var IMG_currentDate = nlapiDateToString(new Date(),'datetimetz');
			//Search For Employee Folder
			var folderID = null;
			var filters = Array();
			filters.push(new nlobjSearchFilter('name',null,'startswith',employee_full_name_U));
			
			var cols = Array();
			cols.push(new nlobjSearchColumn('internalid'));
			
			var searchFolder = nlapiSearchRecord('folder',null,filters,cols);
			if(searchFolder){
				folderID = searchFolder[0].getValue('internalid');
				nlapiLogExecution('DEBUG','Image Folder For Existing Employee ID',folderID);
			}
			if(!folderID){
				var folder = nlapiCreateRecord('folder');

			      if (folder) {

			        // folder.setFieldValue('parent', '10'); // create root level folder

			         folder.setFieldValue('name', employee_full_name_U);

			         folderID = nlapiSubmitRecord(folder);
			         nlapiLogExecution('DEBUG','Image Folder For New Employee ID',folderID);
			      }

			}
			
			var newImg = nlapiCreateFile(expense_InternalID+' '+'rImage'+' '+IMG_currentDate+'-'+j+'.png', 'PJPGIMAGE', obj[j].imgData);
			//newImg.setFolder(105120); //set the folder here
			newImg.setFolder(folderID); //set the folder here
			var id2=nlapiSubmitFile(newImg);
			nlapiLogExecution('debug', 'Image id', id2);
			loadExpense.setCurrentLineItemValue('expense','expmediaitem',id2);
			}
			//End Image Upload
			loadExpense.setCurrentLineItemValue('expense','foreignamount',obj[j].Amount);
			loadExpense.setCurrentLineItemText('expense','currency',obj[j].Currency);
			
			var date_ = obj[j].Date;
			var tranDate = formatDate(date_);
			nlapiLogExecution('DEBUG','tranDate',tranDate);	
			loadExpense.setCurrentLineItemValue('expense','expensedate',obj[j].Date);
			//Need to add more fields based on XML tag i,e record &xml=T
			loadExpense.commitLineItem('expense');
			
		}
		var id = nlapiSubmitRecord(loadExpense);
		nlapiLogExecution('DEBUG',' Updated the expense expense_ID','expense_ID:'+id);
		if(id){
			var lookUp_Expense = nlapiLookupField('expensereport',id,['tranid']);
			var expenseID_T = lookUp_Expense.tranid;
		}
		nlapiLogExecution('DEBUG',' Response the expense expense_ID','expense_ID:'+expenseID_T);
		
		var json_body = {};
		var JsonCategory = {};
		var data_Row = [];
		var dataRows = [];
		var expense_details = nlapiLoadRecord('expensereport',id);
		
		
		json_body = {
				Date: expense_details.getFieldValue('trandate'),
				Expense_Id: expense_details.getFieldValue('tranid'),
				Employee: expense_details.getFieldText('entity'),
				From : expense_details.getFieldValue('trandate'),
				To: expense_details.getFieldValue('duedate'),
				Purpose : expense_details.getFieldValue('memo'),
				Total : expense_details.getFieldValue('total'),
				Total_Non_reimbursable : expense_details.getFieldValue('nonreimbursable'),
				Total_Due : expense_details.getFieldValue('amount')
		};
		data_Row.push(json_body);
		//GEt Line Values
		var lineCount = expense_details.getLineItemCount('expense');
		for(var i=1;i<= lineCount;i++){
			
			JsonCategory ={
			 RefNo: expense_details.getLineItemValue('expense','refnumber',i),		
			 Category: expense_details.getLineItemValue('expense','category_display',i),
			 Currency: expense_details.getLineItemText('expense','currency',i),
			 Memo: expense_details.getLineItemValue('expense','memo',i),
			 Date : 	expense_details.getLineItemValue('expense','expensedate',i),
			 Project : 	expense_details.getLineItemValue('expense','custcolprj_name',i),
			 Department : 	expense_details.getLineItemValue('expense','department_display',i),
			 Vertical : 	expense_details.getLineItemValue('expense','class_display',i),
			 Amount: expense_details.getLineItemValue('expense','foreignamount',i) //customer_display
			
			
			}
			dataRows.push(JsonCategory);
		}
		
		
		//Response
		json_expenseID ={
				ExpenseID:expenseID_T,
				Body: data_Row,
				Lines: dataRows
		};
		
		return json_expenseID;
		
	}
	catch(e){
		nlapiLogExecution('ERROR', 'Update Expense Error', e);
		throw e;
	}
	
}
function getExpensesDetails(expenseid){
	try{
		var flag = true;
		var currency_1;
		var json ={},JsonCategory ={};
		var dataRow =[],dataRows=[];
		var main ={};
		nlapiLogExecution('DEBUG','Expense ID ON GETONE',expenseid);
		var expense_internal_id = getExpenseInternalID(expenseid);
				
		nlapiLogExecution('DEBUG','Expense Internal ID ON GETONE',expense_internal_id);
		
		var record_Obj = nlapiLoadRecord('expensereport',expense_internal_id);
		var emp_name = record_Obj.getFieldValue('entityname');
		var first_approver = record_Obj.getFieldText('custbody1stlevelapprover');
		var T_purpose = record_Obj.getFieldValue('memo'); //subsidiary
		var subsidiary_ = record_Obj.getFieldValue('subsidiary');
		var subsidiary_text = record_Obj.getFieldText('subsidiary');
		var totalAmt = record_Obj.getFieldValue('total');
		var T_Status = record_Obj.getFieldValue('statusRef');
		if(subsidiary_ == 3){
			currency_1 = 'INR';
			}
			else{
			currency_1 = 'USD';
			}
		json ={
			Employee: emp_name,
			Approver: first_approver,
			Subsidiary: subsidiary_text,
			Status: T_Status,
			Purpose: T_purpose,
			TotalAmt: totalAmt
				
		};
		dataRow.push(json);
		
		var lineCount = record_Obj.getLineItemCount('expense');
		for(var i=1;i<= lineCount;i++){
			var img ='';
			var data = '';
			var file = '';
			 img = record_Obj.getLineItemValue('expense','expmediaitem',i);
			if(img){
			 file  = nlapiLoadFile(img);
			 data = file.getValue();
			 nlapiLogExecution('debug','Img data','line:'+i+','+'data:'+data);
		}
			JsonCategory ={
			 RefNo: record_Obj.getLineItemValue('expense','refnumber',i),		
			 Category: record_Obj.getLineItemValue('expense','category_display',i),
			 Currency: record_Obj.getLineItemText('expense','currency',i),
			Memo: record_Obj.getLineItemValue('expense','memo',i),
			Date : 	record_Obj.getLineItemValue('expense','expensedate',i),
			Amount: record_Obj.getLineItemValue('expense','foreignamount',i), //customer_display
			Project: record_Obj.getLineItemValue('expense','customer_display',i),
			 imgData: data
			
			}
			dataRows.push(JsonCategory);
		}
		main = {
			Body: dataRow,
			Line:dataRows
				
		};
		//var loadRec =  nlapiLoadRecord('expensereport',expense_internal_id);
		//loadRec.setFieldValue('status','B');
		//nlapiSubmitRecord(loadRec);
		//nlapiSubmitField('expensereport', expense_internal_id, 'status', 'B');
		nlapiLogExecution('debug', 'dataIn', JSON.stringify(main));
		return main;
	}
	catch(e){
		nlapiLogExecution('DEBUG','Erron in getting the expense details',e);
		throw e;
		
	}
	
}
function getExpenseInternalID(expense_id) {
	try{
	var id = '';
	var filters = Array();
	filters.push(new nlobjSearchFilter('tranid', null, 'startswith', expense_id));

	var cols = Array();
	cols.push(new nlobjSearchColumn('internalid'));

	var searchObj = nlapiSearchRecord('expensereport',null,filters,cols);
	if(searchObj)
	id = searchObj[0].getValue('internalid');

	return id;
	}
	catch(e){
		nlapiLogExecution('DEBUG','Erron in searching expense',e);
		throw e.details;
	}
}
function onSaveEventExpense(dataIn,employeeID){
	try{
		var json_expenseID ={};
		nlapiLogExecution('debug', 'Inside Save DATAIN', JSON.stringify(dataIn));
		nlapiLogExecution('debug', 'Inside Save employeeID', employeeID);
		var purpose = dataIn.Data.obj.purpose;
		nlapiLogExecution('debug', 'purpose', purpose);
		var currentDate = nlapiDateToString(new Date());
		var date = new Date();
		nlapiLogExecution('DEBUG','currentDate obj',currentDate);
		
		var complete = dataIn.Data.complete;
		nlapiLogExecution('debug', 'complete', complete);
		//var imgData = dataIn.Data.imgData;
		//nlapiLogExecution('debug', 'imgData', imgData);
		
		//you just need to dynamically name your file
	/*	var newImg = nlapiCreateFile(employeeID+' '+'rImage'+' '+currentDate'.png', 'PJPGIMAGE', imgData);
		newImg.setFolder(105120); //set the folder here
		var id2=nlapiSubmitFile(newImg);
		nlapiLogExecution('debug', 'Image id2', id2);*/
		
		//currentDate = nlapiAddDays(date,1);
		//currentDate = nlapiDateToString(currentDate);
		nlapiLogExecution('DEBUG','currentDate add days',currentDate);
		var postingDate = currentDate;
		
	//	var project = dataIn.project;
		var secondLevelApprover ;
		var employeeLookup = nlapiLookupField('employee',parseInt(employeeID),[ 'approver','subsidiary','entityid']);
		var firstLevelApprover = employeeLookup.approver;
		var subsidiary_emp = employeeLookup.subsidiary;
		var employee_full_name = employeeLookup.entityid;
		if(subsidiary_emp == 3){
		secondLevelApprover =  7074; //108786-Santosh Banadawar
		}
		else{
		secondLevelApprover = 5764; //108984-Govind S Kharayat ID:5764
		}
		var obj ={};
		var obj = dataIn.Data.obj.category;
		nlapiLogExecution('debug', 'Inside Save obj', JSON.stringify(obj));
		
		
		//Test
		//var obj ={};
	//	obj = { "category":[
	//	{"id":"1","category":"Miles-Project","amount":"399","project":"RMG","currency":"USD","Memo":"TEST"}]}
		nlapiLogExecution('DEBUG','Expenses Length',obj.length);
		//var expenseId = dataIn.Data.ExpenseId;
	//	var id_= obj.category[0].id;
		//
		
		//create expense 
		var record = nlapiCreateRecord('expensereport');
		record.setFieldValue('memo',purpose);
		record.setFieldValue('entity',employeeID);
		//record.setFieldValue('custbody_transactiondate',postingDate);
		//record.setFieldValue('trandate',postingDate);
		record.setFieldValue('custbody1stlevelapprover',firstLevelApprover);
		record.setFieldValue('subsidiary',subsidiary_emp);
		record.setFieldValue('custbody_expenseapprover',secondLevelApprover);
		if(complete == false){
		record.setFieldValue('complete','F');
		}
		else{
			record.setFieldValue('complete','T');
		}
		for(var j=0;j<obj.length;j++){
			
			nlapiLogExecution('debug', 'obj[j].imgData', obj[j].imgData);
			//you just need to dynamically name your file
				/*var IMG_currentDate = nlapiDateToString(new Date(),'datetimetz');
				if(obj[j].imgData){
				var newImg = nlapiCreateFile(employeeID+' '+'rImage'+' '+IMG_currentDate+'.png', 'PJPGIMAGE', obj[j].imgData);
				newImg.setFolder(105120); //set the folder here
				var id2=nlapiSubmitFile(newImg);
				nlapiLogExecution('debug', 'Image id2', id2);
				}*/
				//Create Line Items From Here
			record.selectNewLineItem('expense');
			
			record.setCurrentLineItemValue('expense','refnumber',obj[j].RefNo);
			nlapiLogExecution('DEBUG','ID',obj[j].RefNo);			
			record.setCurrentLineItemValue('expense','memo',obj[j].Memo); 
			nlapiLogExecution('DEBUG','Memo',obj[j].Memo);		
			
			nlapiLogExecution('DEBUG','catProject',obj[j].Project);
			
			var project = projectSplit(obj[j].Project);
			
			nlapiLogExecution('DEBUG','projectSplit',obj[j].Project);
			var projectInternalID = projectDetailSearch(project);
			nlapiLogExecution('DEBUG','projectInternalID',projectInternalID);
			
			record.setCurrentLineItemValue('expense','customer',projectInternalID); 
			record.setCurrentLineItemText('expense','category',obj[j].Category); 
			nlapiLogExecution('DEBUG','category',obj[j].Category);
		//
			if((obj[j].imgData != '' || obj[j].imgData != null )&& obj[j].imgData){
			var IMG_currentDate = nlapiDateToString(new Date(),'datetimetz');
			nlapiLogExecution('DEBUG','Image at line'+j+'Data',obj[j].imgData);
			
			//Search For Employee Folder
			var folderID = null;
			var filters = Array();
			filters.push(new nlobjSearchFilter('name',null,'startswith',employee_full_name));
			
			var cols = Array();
			cols.push(new nlobjSearchColumn('internalid'));
			
			var searchFolder = nlapiSearchRecord('folder',null,filters,cols);
			if(searchFolder){
				folderID = searchFolder[0].getValue('internalid');
				nlapiLogExecution('DEBUG','Image Folder For Existing Employee ID',folderID);
			}
			if(!folderID){
				var folder = nlapiCreateRecord('folder');

			      if (folder) {

			        // folder.setFieldValue('parent', '10'); // create root level folder

			         folder.setFieldValue('name', employee_full_name);

			         folderID = nlapiSubmitRecord(folder);
			         nlapiLogExecution('DEBUG','Image Folder For Employee ID',folderID);
			      }

			}
			
			var newImg = nlapiCreateFile(employeeID+' '+'rImage'+' '+IMG_currentDate+'-'+j+'.png', 'PJPGIMAGE', obj[j].imgData);
			//newImg.setFolder(105120); //set the folder here
			newImg.setFolder(folderID); //set the folder here
			var id2=nlapiSubmitFile(newImg);
			nlapiLogExecution('debug', 'Image id2', id2);
			record.setCurrentLineItemValue('expense','expmediaitem',id2);
			}
			
			
			record.setCurrentLineItemValue('expense','foreignamount',obj[j].Amount);
			record.setCurrentLineItemText('expense','currency',obj[j].Currency);
			
			var date_ = obj[j].Date;
			var tranDate = formatDate(date_);
			nlapiLogExecution('DEBUG','tranDate',tranDate);	
			var tranDate = obj[j].Date;
			record.setCurrentLineItemValue('expense','expensedate',obj[j].Date);
			//Need to add more fields based on XML tag i,e record &xml=T
			record.commitLineItem('expense');
			
		}
		record.setFieldValue('custbody_transactiondate',tranDate);
		record.setFieldValue('trandate',tranDate);
		var expense_ID = nlapiSubmitRecord(record,true,true);
		nlapiLogExecution('DEBUG',' creating the expense expense_ID','expense_ID:'+expense_ID);
		if(expense_ID){
			var lookUp_Expense = nlapiLookupField('expensereport',expense_ID,['tranid']);
			var expenseID_T = lookUp_Expense.tranid;
		}
		
		//var url_print = 'https://system.sandbox.netsuite.com/app/accounting/transactions/exprept.nl?id='+expense_ID+'&print=T&whence=';
		//nlapiLogExecution('DEBUG',' creating the expense expense_ID url_print','url_print:'+url_print);
		
		var json_body = {};
		var JsonCategory = {};
		var data_Row = [];
		var dataRows = [];
		var expense_details = nlapiLoadRecord('expensereport',expense_ID);
		
		
		json_body = {
				Date: expense_details.getFieldValue('trandate'),
				Expense_Id: expense_details.getFieldValue('tranid'),
				Employee: expense_details.getFieldText('entity'),
				From : expense_details.getFieldValue('trandate'),
				To: expense_details.getFieldValue('duedate'),
				Purpose : expense_details.getFieldValue('memo'),
				Total : expense_details.getFieldValue('total'),
				Total_Non_reimbursable : expense_details.getFieldValue('nonreimbursable'),
				Total_Due : expense_details.getFieldValue('amount')
		};
		data_Row.push(json_body);
		//GEt Line Values
		var lineCount = expense_details.getLineItemCount('expense');
		for(var i=1;i<= lineCount;i++){
			
			JsonCategory ={
					 RefNo: expense_details.getLineItemValue('expense','refnumber',i),		
					 Category: expense_details.getLineItemValue('expense','category_display',i),
					 Currency: expense_details.getLineItemText('expense','currency',i),
					 Memo: expense_details.getLineItemValue('expense','memo',i),
					 Date : 	expense_details.getLineItemValue('expense','expensedate',i),
					 Project : 	expense_details.getLineItemValue('expense','custcolprj_name',i),
					 Department : 	expense_details.getLineItemValue('expense','department_display',i),
					 Vertical : 	expense_details.getLineItemValue('expense','class_display',i),
					 Amount: expense_details.getLineItemValue('expense','foreignamount',i) //customer_display
					
					
					}
			dataRows.push(JsonCategory);
		}
		
		
		//Response
		json_expenseID ={
				ExpenseID:expenseID_T,
				Body: data_Row,
				Lines: dataRows
		};
		
	//	nlapiSubmitField('expensereport', expense_ID, 'statusRef', 'pendingSupApproval');
		return json_expenseID;
	}
	catch(e){		
		nlapiLogExecution('DEBUG','Erron in creating the expense',e);
		throw e;
		
	}
}

function formatDate(Date) {

if(Date){
	// fusionDate = "2016-07-14T00:00:00.000Z";
	var datePart = Date.split('T')[0];
	var dateSplit = datePart.split('-');

	// MM/DD/ YYYY
	var netsuiteDateString = dateSplit[1] + "/" + dateSplit[2] + "/"
	        + dateSplit[0];
	return netsuiteDateString;
}
return "";
}

function projectSplit(project){

if(project){
 
 var projectSplit = project.split(' ');
 var projectID = projectSplit[0];

 return projectID;
}
return "";
}

function projectDetailSearch(project_id){
try{
	var id = '';
var filters = Array();
filters.push(new nlobjSearchFilter('entityid', null, 'is', project_id));

var cols = Array();
cols.push(new nlobjSearchColumn('internalid'));

var searchObj = nlapiSearchRecord('job',null,filters,cols);
if(searchObj)
id = searchObj[0].getValue('internalid');

return id;
}
catch(e){
nlapiLogExecution('DEBUG','Project Search Error',e);
throw e;
}
}

//Get Expenses from Employee ID
function getExpensesList(empID){
try{
var currency_1;
var filter = Array();
filter.push(new nlobjSearchFilter('entity', null, 'is', empID));
filter.push(new nlobjSearchFilter('mainline', null, 'is', 'T')) 

var columns = Array();
columns.push(new nlobjSearchColumn('tranid').setSort(true));
columns.push(new nlobjSearchColumn('memo'));
columns.push(new nlobjSearchColumn('status'));
columns.push(new nlobjSearchColumn('fxamount'));
columns.push(new nlobjSearchColumn('trandate'));
columns.push(new nlobjSearchColumn('subsidiary')); //

var expenseSearch = nlapiSearchRecord('expensereport',null,filter,columns);
if(expenseSearch){
var expenseID = expenseSearch[0].getId();
var expenseLoad = nlapiLoadRecord('expensereport',expenseID);
var lineCount = expenseLoad.getLineItemCount('expense');
for(var i=1;i<= 1;i++){
	
	var project =  expenseLoad.getLineItemValue('expense','customer_display',i);
	
	}
	
}

var dataRow = [];
var expenseList ={};

var employeeLookup_1 = nlapiLookupField('employee',parseInt(empID),['entityid']);
var employee_id = employeeLookup_1.entityid;
if(expenseSearch){
for(var i=0;i<expenseSearch.length;i++){
var subsidiary_1 = expenseSearch[i].getValue('subsidiary');
if(subsidiary_1 == 3){
currency_1 = 'INR';
}
else{
currency_1 = 'USD';
}
expenseList ={
Employee: employee_id,
ExpenseID : expenseSearch[i].getValue('tranid'),
//Date : expenseSearch[i].getValue('trandate'),
Purpose : expenseSearch[i].getValue('memo'),
Status : expenseSearch[i].getValue('status'),
Amount : expenseSearch[i].getValue('fxamount'),
Project: project,
Currency: currency_1

}
dataRow.push(expenseList);
}
}
return dataRow;

}

catch(e)
{
nlapiLogExecution('DEBUG','Expense Search Error',e);
throw e;
}
}

function _logValidation(value) 
{
 if(value != null && value.toString() != null && value != '' && value != undefined && value.toString() != undefined && value != 'undefined' && value.toString() != 'undefined'&& value.toString() != 'NaN' && value != NaN) 
 {
  return true;
 }
 else 
 { 
  return false;
 }
}