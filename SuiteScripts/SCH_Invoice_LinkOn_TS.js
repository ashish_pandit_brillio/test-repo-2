// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
    /*
     Script Name:	SCH_Invoice_LinkOn_TS
     Author:			Vikrant
     Company:		Aashna
     Date:			10-11-2014
     Script Modification Log:
     -- Date --			-- Modified By --				--Requested By--				-- Description --
     Below is a summary of the process controls enforced by this script file.  The control logic is described
     more fully, below, in the appropriate function headers and code blocks.
     SCHEDULED FUNCTION
     - SCH_Invoice_LinkOn_TS(type)
     SUB-FUNCTIONS
     - The following sub-functions are called by the above core functions in order to maintain code
     modularization:
     - NOT USED
     */
}
// END SCRIPT DESCRIPTION BLOCK  ====================================


// BEGIN SCHEDULED FUNCTION =============================================

function SCH_Invoice_LinkOn_TS(type) //
{
	/*  On scheduled function:
	 
	 - PURPOSE
	 
	 -
	 
	 FIELDS USED:
	 
	 --Field Name--				--ID--
	 
	 */
	//  LOCAL VARIABLES
	
	
	//  SCHEDULED FUNCTION CODE BODY
	try //
	{
		nlapiLogExecution('DEBUG', 'after_Sub_UES_Invoice_LinkOn_TS', '*** Execution Started ***');
		//do //
		{
			var initial_ID = 0;
			var initial_ID_2 = 0;
			
			nlapiLogExecution('DEBUG', 'after_Sub_UES_Invoice_LinkOn_TS', 'initial_ID : ' + initial_ID);
			nlapiLogExecution('DEBUG', 'after_Sub_UES_Invoice_LinkOn_TS', 'initial_ID_2 : ' + initial_ID_2);
			
			var parameter = nlapiGetContext().getSetting('SCRIPT', 'custscript_invoice_id');
			nlapiLogExecution('DEBUG', 'after_Sub_UES_Invoice_LinkOn_TS', 'parameter : ' + parameter);
			
			var parameter_2 = nlapiGetContext().getSetting('SCRIPT', 'custscript_counter_j_value');
			nlapiLogExecution('DEBUG', 'after_Sub_UES_Invoice_LinkOn_TS', 'parameter_2 : ' + parameter_2);
			
			var filters = new Array();
			
			if (_validate(parameter)) //
			{
				initial_ID = parameter;
				initial_ID_2 = parameter_2;
			}
			
			if (initial_ID_2 == 0) //
			{
				initial_ID_2 = 1;
			}
			
			//return;
			
			nlapiLogExecution('DEBUG', 'after_Sub_UES_Invoice_LinkOn_TS', 'initial_ID : ' + initial_ID);
			nlapiLogExecution('DEBUG', 'after_Sub_UES_Invoice_LinkOn_TS', 'initial_ID_2 : ' + initial_ID_2);
			
			filters[filters.length] = new nlobjSearchFilter('internalidnumber', null, 'greaterthan', initial_ID);
			
			var search_result = new Array(); //nlapiSearchRecord('invoice', 'customsearch_invoice_processed', filters, null);
			search_result = ['13660', '14276', '14443', '14992', '15783', '15791', '15812', '15815', '15817', '15818', '15819', '15994', '15997', '16002', '16012', '16014', '16016', '16018', '16019', '16020', '16026', '16051', '16058', '16168', '16169', '16171', '16172', '16173', '16174', '16175', '16176', '16177', '16178', '16179', '16180', '16314', '16348', '16557', '16578', '16598', '16611', '16624', '16627', '16649', '16708', '16718', '16737', '16738', '16739', '16741', '16742', '16904', '16915', '16922', '16924', '16938', '16948', '16957', '17081', '17086', '17100', '17104', '17106', '17109', '17116', '17122', '17128', '17130', '17136', '17137', '17138', '17139', '17141', '17142', '17434', '17476', '17477', '17478', '17479', '17480', '17507', '17508', '17509', '17510', '17511', '17512', '17513', '18281', '18283', '18497', '18498', '18499', '18711', '18712', '18713', '18717', '18718', '18719', '18720', '18721', '18722', '18723', '18724', '18725', '18863', '18870', '18885', '18899', '18902', '18903', '18905', '18907', '18908', '18909', '18911', '18912', '18913', '18927', '19161', '19177', '19184', '19185', '19194', '19197', '19198', '19209', '19211', '19214', '19217', '19220', '19230', '19231', '19232', '19233', '19234', '19238', '19239', '19240', '19241', '19242', '19243', '19303', '19321', '19327', '19329', '19333', '19337', '19341', '19355', '19358', '19360', '19363', '19364', '19368', '19369', '19372', '19373', '19376', '19377', '19379', '19380', '19381', '19382', '19383', '19384', '19385', '19386', '19387', '19388', '19389', '19390', '19391', '19392', '19393', '19394', '19395', '19396', '19397', '19398', '19400', '19401', '19402', '19403', '19404', '19405', '19406', '19407', '19408', '19409', '19410', '19411', '19412', '19413', '19414', '19415', '19416', '19417', '19419', '19420', '19424', '19425', '19426', '19427', '19428', '19429', '19430', '19431', '19446', '19447', '19461', '19462', '19465', '19466', '19467', '19470', '19472', '19473', '19474', '19475', '19478', '19480', '19481', '19482', '19484', '19485', '19487', '19489', '19491', '19510', '19526', '19529', '19533', '19535', '19536', '19537', '19571', '19572', '19575', '19578', '19582', '19583', '19586', '19587', '19595', '19600', '19606', '19610', '19614', '19615', '19617', '19795', '19816', '19828', '19829', '19830', '19831', '19851', '19852', '19887', '19888', '19895', '19896', '19897', '19898', '19899', '19900', '19901', '19902', '19903', '19904', '19905', '19906', '19907', '19908', '19909', '19910', '19911', '20002', '20017', '20028', '20035', '20039', '20042', '20051', '20052', '20064', '20102', '20110', '20116', '20120', '20131', '20149', '20158', '20160', '20161', '20166', '20170', '20171', '20174', '20336', '20337', '20338', '20339', '20340', '20341', '20342', '20343', '20344', '20345', '20346', '20347', '20348', '20349', '20350', '20351', '20352', '20353', '20354', '20355', '20356', '20357', '20358', '20359', '20360', '21573', '21574', '21575', '21576', '21577', '21578', '21579', '21580', '21581', '21582', '21583', '21584', '21586', '21587', '21589', '21590', '21591', '21592', '21593', '21594', '21595', '21597', '21598', '21600', '21601', '21603', '21604', '21605', '21606', '21607', '21608', '21610', '21611', '21612', '21613', '21614', '21615', '21617', '21618', '21621', '21622', '21623', '21624', '21625', '21626', '21627', '21628', '21629', '21630', '21631', '21632', '21633', '21634', '21635', '21636', '21637', '21638', '21639', '21640', '21641', '21642', '21643', '21644', '21645', '21646', '21647', '21648', '21649', '21650', '21651', '21652', '21653', '21654', '21655', '21657', '21658', '21659', '21660', '21661', '21662', '21664', '21666', '21667', '21668', '21669', '21671', '21672', '21673', '21674', '21675', '21676', '21677', '21678', '21679', '21680', '21681', '21682', '21683', '21684', '21685', '21687', '21688', '21689', '21690', '21691', '21692', '21697', '21698', '21699', '21700', '21701', '21702', '21703', '21704', '21705', '21706', '21707', '21708', '21709', '21710', '21711', '21712', '21713', '21714', '21715', '21716', '21718', '21719', '21720', '21721', '21722', '21723', '21724', '21725', '21726', '21727', '21728', '21729', '21730', '21731', '21732', '21733', '21734', '21735', '21736', '21737', '21738', '21739', '21740', '21741', '21742', '21743', '21744', '21745', '21771', '21772', '21773', '21774', '21775', '21776', '21777', '21778', '21779', '21782', '21783', '21900', '21912', '22182', '22183', '22206', '22208', '22212', '22214', '23973', '26210', '27555', ];
			
			if (_validate(search_result)) //
			{
				nlapiLogExecution('DEBUG', 'after_Sub_UES_Invoice_LinkOn_TS', 'search_result.length : ' + search_result.length);
				var result = '';
				//var all_columns = search_result[0].getAllColumns();
				
				for (var counter_i = initial_ID; counter_i < search_result.length; counter_i++) //
				{
					result = search_result[counter_i];
					
					var inv_id = result; //result.getValue(all_columns[1]);
					//nlapiLogExecution('ERROR', 'after_Sub_UES_Invoice_LinkOn_TS', 'inv_id : ' + inv_id);
					
					nlapiLogExecution('ERROR', 'after_Sub_UES_Invoice_LinkOn_TS', 'counter_i : ' + counter_i + ', inv_id : ' + inv_id);
					
					if (_validate(inv_id)) //
					{
						//inv_id = '9728';
						var inv_record = nlapiLoadRecord('invoice', inv_id);
						var i_ts_internal_ID = '';
						
						if (_validate(inv_record)) //
						{
							var line_count = inv_record.getLineItemCount('time');
							nlapiLogExecution('DEBUG', 'after_Sub_UES_Invoice_LinkOn_TS', 'Invoice line_count : ' + line_count);
							
							nlapiLogExecution('DEBUG', 'after_Sub_UES_Invoice_LinkOn_TS', 'initial_ID_2 : ' + initial_ID_2);
							
							
							
							for (var counter_j = initial_ID_2; counter_j <= line_count; counter_j++) //
							{
								initial_ID_2 = 1;
								try //
								{
									var apply_check = inv_record.getLineItemValue('time', 'apply', counter_j);
									////nlapiLogExecution('DEBUG', 'after_Sub_UES_Invoice_LinkOn_TS', 'apply_check : ' + apply_check);
									nlapiLogExecution('DEBUG', 'after_Sub_UES_Invoice_LinkOn_TS', 'counter_j : ' + counter_j);
									
									if (apply_check == 'T') //
									{
										i_ts_internal_ID = inv_record.getLineItemValue('time', 'doc', counter_j);
										//nlapiLogExecution('DEBUG', 'after_Sub_UES_Invoice_LinkOn_TS', 'i_ts_internal_ID : ' + i_ts_internal_ID);
										
										var rate = inv_record.getLineItemValue('time', 'rate', counter_j);
										//nlapiLogExecution('DEBUG', 'after_Sub_UES_Invoice_LinkOn_TS', 'rate : ' + rate);
										
										var r_TS_record = nlapiLoadRecord('timebill', i_ts_internal_ID);
										
										if (_validate(r_TS_record)) //
										{
											//var i_inv_id = nlapiGetRecordId();
											//nlapiLogExecution('DEBUG', 'after_Sub_UES_Invoice_LinkOn_TS', 'i_inv_id : ' + i_inv_id);
											
											r_TS_record.setFieldValue('custcol_invoice_link', inv_id); // 
											r_TS_record.setFieldValue('custcol_ratefrominvoice', rate); // 
											var submit = nlapiSubmitRecord(r_TS_record, false, true);
											nlapiLogExecution('DEBUG', 'after_Sub_UES_Invoice_LinkOn_TS', 'Record Submitted !!! : ' + i_ts_internal_ID + ', Remaining Usage : ' + nlapiGetContext().getRemainingUsage());
											
											initial_ID_2 = counter_j;
											
											//return;
											
											if (nlapiGetContext().getRemainingUsage() < 300) //
											{
												//nlapiYieldScript();
												//nlapiLogExecution('DEBUG', 'after_Sub_UES_Invoice_LinkOn_TS', 'usage : ' + nlapiGetContext().getRemainingUsage());
												
												try //
												{
													var yield = nlapiYieldScript();
													nlapiLogExecution('DEBUG', 'after_Sub_UES_Invoice_LinkOn_TS', '4 yield : ' + yield);
													
													if (yield == 'FAILURE') //
													{
														// Define SYSTEM parameters to schedule the script to re-schedule.
														var params = new Array();
														params['status'] = 'scheduled';
														params['runasadmin'] = 'T';
														var startDate = new Date();
														params['startdate'] = startDate.toUTCString();
														
														// Define CUSTOM parameters to schedule the script to re-schedule.
														params['custscript_invoice_id'] = initial_ID;
														params['custscript_counter_j_value'] = initial_ID_2;
														
														//nlapiLogExecution('DEBUG', 'TimeSheet_Notify ', ' Script Status --> Before schedule...');
														
														var status = nlapiScheduleScript(nlapiGetContext().getScriptId(), nlapiGetContext().getDeploymentId(), params);
														//nlapiLogExecution('DEBUG', 'TimeSheet_Notify ', ' Script Status -->' + status);
														
														//If script is scheduled then successfully then check for if status=queued
														if (status == 'QUEUED') //
														{
															nlapiLogExecution('DEBUG', ' TimeSheet_Notify', '4 Script is rescheduled ....................');
														}
														return;
													}
												} 
												catch (ex_y) //
												{
													nlapiLogExecution('ERROR', 'SCH_Invoice_LinkOn_TS', 'Exception : ' + ex_y);
													nlapiLogExecution('ERROR', 'SCH_Invoice_LinkOn_TS', 'Exception : ' + ex_y.message);
													
													/*
													
													 // Define SYSTEM parameters to schedule the script to re-schedule.
													
													 var params = new Array();
													
													 params['status'] = 'scheduled';
													
													 params['runasadmin'] = 'T';
													
													 var startDate = new Date();
													
													 params['startdate'] = startDate.toUTCString();
													
													 
													
													 // Define CUSTOM parameters to schedule the script to re-schedule.
													
													 params['custscript_invoice_id'] = initial_ID;
													
													 params['custscript_counter_j_value'] = initial_ID_2;
													
													 //nlapiLogExecution('DEBUG', 'TimeSheet_Notify ', ' Script Status --> Before schedule...');
													
													 
													
													 var status = nlapiScheduleScript(nlapiGetContext().getScriptId(), nlapiGetContext().getDeploymentId(), params);
													
													 //nlapiLogExecution('DEBUG', 'TimeSheet_Notify ', ' Script Status -->' + status);
													
													 
													
													 ////If script is scheduled then successfully then check for if status=queued
													
													 if (status == 'QUEUED') //
													
													 {
													
													 nlapiLogExecution('DEBUG', ' TimeSheet_Notify', '3 Script is rescheduled ....................');
													
													 }
													
													 return;
													
													 */
													
												}
												//return;
											}
										}
									}
									/*
									 else
									 if (apply_check == 'F') //
									 {
									 var i_ts_internal_ID = inv_record.getLineItemValue('time', 'doc', counter_j);
									 //nlapiLogExecution('DEBUG', 'after_Sub_UES_Invoice_LinkOn_TS', 'i_ts_internal_ID : ' + i_ts_internal_ID);
									 
									 var r_TS_record = nlapiLoadRecord('timebill', i_ts_internal_ID);
									 
									 if (_validate(r_TS_record)) //
									 {
									 //var i_inv_id = nlapiGetRecordId();
									 ////nlapiLogExecution('DEBUG', 'after_Sub_UES_Invoice_LinkOn_TS', 'i_inv_id : ' + i_inv_id);
									 
									 r_TS_record.setFieldValue('custcol_invoice_link', '');
									 
									 var submit = nlapiSubmitRecord(r_TS_record, false, true);
									 //nlapiLogExecution('DEBUG', 'after_Sub_UES_Invoice_LinkOn_TS', 'Record Submitted !!!');
									 }
									 }
									 */
								} 
								catch (ex_1) //
								{
									nlapiLogExecution('ERROR', 'SCH_Invoice_LinkOn_TS', 'Exception for time : ' + i_ts_internal_ID);
									nlapiLogExecution('ERROR', 'SCH_Invoice_LinkOn_TS', 'Exception : ' + ex_1);
									nlapiLogExecution('ERROR', 'SCH_Invoice_LinkOn_TS', 'Exception : ' + ex_1.message);
								}
							}
						}
					}
					
					//return;
					initial_ID = counter_i;
					
					var usageRemaining = nlapiGetContext().getRemainingUsage();
					//nlapiLogExecution('DEBUG', 'TimeSheet_Notify ', ' usageRemaining -->' + usageRemaining);
					
					if (usageRemaining < 1000) // use this at time of sending to all...
					//if (i == 10) // for testing purpose
					{
						try //
						{
							var yield = nlapiYieldScript();
							nlapiLogExecution('DEBUG', 'after_Sub_UES_Invoice_LinkOn_TS', '2 yield : ' + yield);
							
							if (yield == 'FAILURE') //
							{
								// Define SYSTEM parameters to schedule the script to re-schedule.
								var params = new Array();
								params['status'] = 'scheduled';
								params['runasadmin'] = 'T';
								var startDate = new Date();
								params['startdate'] = startDate.toUTCString();
								
								// Define CUSTOM parameters to schedule the script to re-schedule.
								params['custscript_invoice_id'] = initial_ID;
								params['custscript_counter_j_value'] = initial_ID_2;
								//nlapiLogExecution('DEBUG', 'TimeSheet_Notify ', ' Script Status --> Before schedule...');
								
								var status = nlapiScheduleScript(nlapiGetContext().getScriptId(), nlapiGetContext().getDeploymentId(), params);
								//nlapiLogExecution('DEBUG', 'TimeSheet_Notify ', ' Script Status -->' + status);
								
								////If script is scheduled then successfully then check for if status=queued
								if (status == 'QUEUED') //
								{
									nlapiLogExecution('DEBUG', ' TimeSheet_Notify', '2 Script is rescheduled ....................');
								}
							}
						} 
						catch (ex_y) //
						{
							// Define SYSTEM parameters to schedule the script to re-schedule.
							var params = new Array();
							params['status'] = 'scheduled';
							params['runasadmin'] = 'T';
							var startDate = new Date();
							params['startdate'] = startDate.toUTCString();
							
							// Define CUSTOM parameters to schedule the script to re-schedule.
							params['custscript_invoice_id'] = initial_ID;
							params['custscript_counter_j_value'] = initial_ID_2;
							//nlapiLogExecution('DEBUG', 'TimeSheet_Notify ', ' Script Status --> Before schedule...');
							
							var status = nlapiScheduleScript(nlapiGetContext().getScriptId(), nlapiGetContext().getDeploymentId(), params);
							//nlapiLogExecution('DEBUG', 'TimeSheet_Notify ', ' Script Status -->' + status);
							
							////If script is scheduled then successfully then check for if status=queued
							if (status == 'QUEUED') //
							{
								nlapiLogExecution('DEBUG', ' TimeSheet_Notify', '1 Script is rescheduled ....................');
							}
							return;
						}
						//return;
					}
				}
			}
		}
		//while (!_validate(search_result))
		{
		
		}
	} 
	catch (ex) //
	{
		nlapiLogExecution('ERROR', 'SCH_Invoice_LinkOn_TS', 'Exception : ' + ex);
		nlapiLogExecution('ERROR', 'SCH_Invoice_LinkOn_TS', 'Exception : ' + ex.message);
	}
	nlapiLogExecution('DEBUG', 'after_Sub_UES_Invoice_LinkOn_TS', '*** Execution Completed ***');
}

// END SCHEDULED FUNCTION ===============================================





// BEGIN FUNCTION ===================================================
{



}

function _validate(obj) //
{
    if (obj != null && obj != '' && obj != 'undefined')//
    {
        return true;
    }
    else //
    {
        return false;
    }
}

// END FUNCTION =====================================================
