/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       21 Nov 2016     deepak.srinivas
 *
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * Project Task recordType
 * 
 * @param {String} type Sublist internal id
 * @param {String} name Field internal id
 * @param {Number} linenum Optional line item number, starts from 1
 * @returns {Void}
 */
function task_default_set(type, name, linenum){
	try{
		if ((name == 'title') && (nlapiGetFieldValue('title') == 'Holiday' || (nlapiGetFieldValue('title') == 'Leave')) && nlapiGetFieldValue('nonbillabletask')== 'F'){
			alert('Your trying to create Task as Holiday/Leave, these two tasks are non-billable by default !!');
			nlapiSetFieldValue('nonbillabletask','T');
			return true;
		}
		else {
			if ((name == 'title') && (nlapiGetFieldValue('title') != 'Holiday' || (nlapiGetFieldValue('title') != 'Leave')))
			//alert('Your trying to create Task as Holiday/Leave, these two tasks are non-billable by default !!');
			nlapiSetFieldValue('nonbillabletask','F');
			return true;
		}
		//return true;
		}
		
	
	catch(e){
		nlapiLogExecution('DEBUG','Holiday Task non-billable error',e);
	}
 
}
