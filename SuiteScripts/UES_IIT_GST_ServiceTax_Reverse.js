// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
    /*
     Script Name:UES_ServiceTax_Reverse.js
     Author:		Sachin K
     Company:    Aashna Cloudtech Pvt Ltd.
     Date:
     Description:

     Script Modification Log:
     -- Date --			-- Modified By --				--Requested By--				-- Description --
      22 sep 2014         Supriya                         Kalpana                       Normal Account Validation
      28 may 2014         Nikhil jain                                                   Remove the hard coded service tax
      17 July 2015         Nikhil                      	  satish D                      Add an Service tax round up method
      30 July 2015         Nikhil                         sachin k                      add a reneweal process
      26 Feb 2016          Nikhil                         Vaibhav/Kalpana/sachin        add the expense account as Sbc purchase for reverse line   
      23 May 2016          Nikhil                                                       remove hardcodings of tax types  
      09 Mar 2017		   Onkar						  Sachin/Ashwinin				for GST tax code mapping              
     Below is a summary of the process controls enforced by this script file.  The control logic is described
     more fully, below, in the appropriate function headers and code blocks.

     BEFORE LOAD
     - beforeLoadRecord(type)
     NOT USED

     BEFORE SUBMIT
     - beforeSubmitRecord(type)
     NOT USED

     AFTER SUBMIT
     - afterSubmitRecord(type)
     beforeSubmitRecord(type)

     SUB-FUNCTIONS
     - The following sub-functions are called by the above core functions in order to maintain code
     modularization:
     getTaxaccount(taxcode)
     RoundUPServiceTAX(n)
     - NOT USED

     */
}
// END SCRIPT DESCRIPTION BLOCK  ====================================



// BEGIN GLOBAL VARIABLE BLOCK  =====================================
{
    //  Initialize any Global Variables, in particular, debugging variables...




}
// END GLOBAL VARIABLE BLOCK  =======================================





// BEGIN BEFORE LOAD ==================================================

function beforeLoadRecord(type)
{

    /*  On before load:
     - EXPLAIN THE PURPOSE OF THIS FUNCTION
     -
     FIELDS USED:
     --Field Name--				--ID--
     */
    //  LOCAL VARIABLES

    //  BEFORE LOAD CODE BODY


    return true;



}

// END BEFORE LOAD ====================================================





// BEGIN BEFORE SUBMIT ================================================

function beforeSubmitRecord(type){
	/*  On before submit:
	 - PURPOSE
	 -
	 FIELDS USED:
	 --Field Name--				--ID--
	 */
	//  LOCAL VARIABLES
	
	
	//  BEFORE SUBMIT CODE BODY
	
	//== GET GLOBAL SUBSIDIARY PARAMETERS ==
	
	try 
	{
		if (type != 'delete') 
		{
		
			var a_subisidiary = new Array()
			var s_pass_code;
			var i_AitGlobalRecId = SearchGlobalParameter();
			var i_Rcm_inputAcc;
			var i_Rcm_outputAcc;
			var gstApplyDate;
			var a_st_details = new Array();
			var st_details_obj = {};
			var st_details_obj_VNR = {};
			var st_serachRes;
			var st_class = '';
			var st_department = '';
			var st_location = '';
			var i_Default;
			var context = nlapiGetContext();
		    var dateFormatPref = context.getPreference('dateformat');
		    nlapiLogExecution('DEBUG', 'beforeLoadRecord_ShowImportButton', 'dateFormatPref ->'+ dateFormatPref);
			if (i_AitGlobalRecId != 0) 
			{
				var o_AitGloRec = nlapiLoadRecord('customrecord_indian_tax_glo_parameter', i_AitGlobalRecId);
				
				a_subisidiary = o_AitGloRec.getFieldValues('custrecord_ait_subsidiary');
				
				var i_vatCode = o_AitGloRec.getFieldValue('custrecord_ait_vat_code');
				
				var i_taxcode = o_AitGloRec.getFieldValue('custrecord_ait_taxcode');
				
				var i_servicetaxrate = o_AitGloRec.getFieldValue('custrecord_ait_servicetaxrate');
				
				var i_ecessrate = o_AitGloRec.getFieldValue('custrecord_ait_ecess_rate');
				
				var i_hecessrate = o_AitGloRec.getFieldValue('custrecord_ait_hecess_rate');
				
				var i_swachh_bharat_cess = o_AitGloRec.getFieldValue('custrecord_ait_swachh_cess');
				
				var i_total_service_tax = o_AitGloRec.getFieldValue('custrecord_ait_total_servicetax');
				
				var stRoundMethod = o_AitGloRec.getFieldValue('custrecord_ait_st_roundoff');
				
				s_pass_code = o_AitGloRec.getFieldValue('custrecord_encpwdstring');
				
				i_Rcm_inputAcc = o_AitGloRec.getFieldValue('custrecord_ait_rcm_st_input_acc');
				
				i_Rcm_outputAcc = o_AitGloRec.getFieldValue('custrecord_ait_rcm_st_output_acc');
				
				i_Default = o_AitGloRec.getFieldValue('custrecord_pay_servicetax');
				nlapiLogExecution('AUDIT', 'Bill After submit', '***********i_Default->' +i_Default);
				
				gstApplyDate = o_AitGloRec.getFieldValue('custrecord_ait_gst_apply_date');
				gstApplyDate = nlapiStringToDate(gstApplyDate, dateFormatPref);
				nlapiLogExecution('DEBUG', 'Bill ', "gstApplyDate->" + gstApplyDate);
				
				
				//================================Begin:- Code to Get the Service tax Details=============================//
				
				
				var st_filters = new Array();
				var st_columns = new Array();
				
				st_filters.push(new nlobjSearchFilter('custrecord_ait_service_tax_global_link', null, 'anyOf', i_AitGlobalRecId))
				st_filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'))
				
				st_columns.push(new nlobjSearchColumn('custrecord_ait_service_tax_type'))
				st_columns.push(new nlobjSearchColumn('custrecord_ait_service_tax_rate'))
				st_columns.push(new nlobjSearchColumn('custrecord_ait_service_expense_out'))
				st_columns.push(new nlobjSearchColumn('custrecord_ait_taxdetail_rcm_st_input_ac'))
				st_columns.push(new nlobjSearchColumn('custrecord_ait_taxdetail_rcm_st_out_acc'))
				st_columns.push(new nlobjSearchColumn('custrecord_ait_taxdetail_class'))
				st_columns.push(new nlobjSearchColumn('custrecord_ait_taxdetail_department'))
				st_columns.push(new nlobjSearchColumn('custrecord_ait_taxdetail_location'))
				
				var st_serachRes = nlapiSearchRecord('customrecord_ait_service_tax_details', null, st_filters, st_columns)
				
				if (st_serachRes != null && st_serachRes != '' && st_serachRes != undefined) 
				{
					for (var t = 0; t < st_serachRes.length; t++) 
					{
						var st_taxtype = st_serachRes[t].getValue('custrecord_ait_service_tax_type')
						var st_rate = st_serachRes[t].getValue('custrecord_ait_service_tax_rate')
						var st_expense = st_serachRes[t].getValue('custrecord_ait_service_expense_out')
						var st_inputAcc = st_serachRes[t].getValue('custrecord_ait_taxdetail_rcm_st_input_ac')
						var st_outputAcc = st_serachRes[t].getValue('custrecord_ait_taxdetail_rcm_st_out_acc')
						var st_class = st_serachRes[t].getValue('custrecord_ait_taxdetail_class')
						var st_department = st_serachRes[t].getValue('custrecord_ait_taxdetail_department')
						var st_location = st_serachRes[t].getValue('custrecord_ait_taxdetail_location')
						var st_taxtype_VNR = st_serachRes[t].getValue('custrecord_ait_service_tax_type')
						//Code added on 09/03/2017 For GST Tax Code Mapping
						st_taxtype = st_taxtype+'@'+st_rate;
						//EndOf Code added on 09/03/2017 For GST Tax Code Mapping
						st_details_obj_VNR[st_taxtype_VNR] = st_rate + '@' + st_expense + '@' + st_inputAcc + '@' + st_outputAcc + '@' + st_class + '@' + st_department + '@' + st_location;
						st_details_obj[st_taxtype] = st_rate + '@' + st_expense + '@' + st_inputAcc + '@' + st_outputAcc + '@' + st_class + '@' + st_department + '@' + st_location;
						nlapiLogExecution('DEBUG', 'Bill After submit', '***********st_details_obj->' + st_taxtype + ' = ' +st_rate + '@' + st_expense + '@' + st_inputAcc + '@' + st_outputAcc + '@' + st_class + '@' + st_department + '@' + st_location)
						nlapiLogExecution('DEBUG', 'Bill After submit', '***********st_details_obj->' +st_details_obj[st_taxtype])

						a_st_details.push(st_details_obj);
						nlapiLogExecution('DEBUG', 'Bill After submit', '***********a_st_details->'+ a_st_details)
					}
				}
				
				//===============================End:- Code to Get the Service tax Details=============================//
			
			}// END  if (i_AitGlobalRecId != 0)
			//== GET THE CONTEXT ==
			var currentContext = nlapiGetContext();
			if (currentContext.getExecutionContext() != 'workflow' && currentContext.getExecutionContext() != 'scheduled') 
			{
				//===== IF TYPE IS EDIT ======
				var recordType = nlapiGetRecordType();
				if (type == 'edit' || recordType == 'vendorcredit') 
				{
					// === GET THE RECORD ID , TYPE AND LOAD THE RECORD ====
					var Flag = 0;
					var i_subsidiary = nlapiGetFieldValue('subsidiary');
					var Flag = isindia_subsidiary(a_subisidiary, s_pass_code, i_subsidiary);
					var currentDate = nlapiGetFieldValue('trandate');
					currentDate = nlapiStringToDate(currentDate, dateFormatPref);
					nlapiLogExecution('DEBUG', 'customizeGlImpact', 'currentDate ->'+ currentDate);
					if (Flag == 1 && gstApplyDate <= currentDate) 
					{
						// ==== GET THE EXPESE LINE ITEM COUNT ====
						var exLinecount = nlapiGetLineItemCount('expense');
						nlapiLogExecution('DEBUG', 'Bill After submit', "exLinecount->" + exLinecount);
						//=== ITERATE THROUGH EVERY EXPENSE LINE ===
						for (var l = exLinecount; l >= 1; l--) 
						{
							var isReversetrue = nlapiGetLineItemValue('expense', 'custcol_st_reverseline', l)
							nlapiLogExecution('DEBUG', 'Bill After submit', "isReversetrue->" + isReversetrue)
							// ===== IF IS REVERES IS TRUE THEN REMOVE THE LINE ====
							if (isReversetrue == 'T') 
							{
								nlapiSelectLineItem('expense', l)
								nlapiRemoveLineItem('expense', l)
								nlapiLogExecution('DEBUG', 'Bill After submit', "Line Removed->" + l)
							} // END if(isReversetrue=='T')
						} // END for(var i=1;i<=exLinecount;i++)
					}// end if (a_subisidiary[y] == i_subsidiary)
				} // END if(type=='edit')
			}// END if (currentContext.getExecutionContext() != 'workflow')
			// ==== GET THE EXECUTION CONTEXT ====
			var currentContext = nlapiGetContext();
			
			// IF EXECUTION CONTEXT IS WORKFLOW ====
			if (currentContext.getExecutionContext() != 'workflow' && currentContext.getExecutionContext() != 'scheduled') 
			{
				var Flag = 0;
				var i_subsidiary = nlapiGetFieldValue('subsidiary');
				var Flag = isindia_subsidiary(a_subisidiary, s_pass_code, i_subsidiary);
				var currentDate = nlapiGetFieldValue('trandate');
				currentDate = nlapiStringToDate(currentDate, dateFormatPref);
				nlapiLogExecution('DEBUG', 'customizeGlImpact', 'currentDate ->'+ currentDate);
				if (Flag == 1 && gstApplyDate <= currentDate)
				{
					var calTaxrate = 0;
					var st_expense = 'F';
					
					var vid = nlapiGetFieldValue('entity');
					
					var o_Vendor_Record = nlapiLoadRecord('vendor', vid);
					
					var vendor_type = o_Vendor_Record.getFieldValue('isperson');
					//nlapiLookupField('vendor', vid, 'isperson')
					
					var vendor_gst_liable =  o_Vendor_Record.getFieldValue('custentity_iit_gst_liable');
					nlapiLogExecution('DEBUG', 'Bill After submit', 'vendor_gst_liable->' + vendor_gst_liable);
					
					var expense_count = nlapiGetLineItemCount('expense')
					var item_count = nlapiGetLineItemCount('item')
					
					var total_count = parseInt(expense_count) + parseInt(item_count)
					
					if (parseInt(total_count) <= parseInt(36)) 
					{
						
						if(vendor_gst_liable == 'T')
						{
							// === GET THE LINE COUNT =====
							var exLinecount = nlapiGetLineItemCount('expense')
							nlapiLogExecution('DEBUG', 'Bill After submit', '***********exLinecount->' + exLinecount)
							//=== ITERATE THROUGHT EVERY EXPENSE LINE ===
							for (var j = 1; j <= exLinecount; j++) 
							{
								nlapiLogExecution('DEBUG', 'Bill After submit', '***********j->' + j)
								var isReverseapply = nlapiGetLineItemValue('expense', 'custcol_st_reverseapply', j)
								
								var Isreciviabl = nlapiGetLineItemValue('expense', 'custcol_reciviableapply', j)
								
								var i_Expense_Account = nlapiGetLineItemValue('expense', 'account', j);
								nlapiLogExecution('DEBUG', 'Bill After submit Vendor Not Registered', 'i_Expense_Account->' + i_Expense_Account);
								
								var i_Expense = nlapiGetLineItemValue('expense', 'category', j);
								nlapiLogExecution('DEBUG', 'Bill After submit Vendor Not Registered', 'i_Expense->' + i_Expense);
								
								if(_logValidation(i_Expense))
								{
									var b_Expense_Out = nlapiLookupField('expensecategory', i_Expense, 'custrecord_rcm_expense_out');
									nlapiLogExecution('DEBUG', 'Bill After submit Vendor Not Registered', 'b_Expense_Out->' + b_Expense_Out);
									
									var i_Expense_GST_Not_CR = nlapiGetLineItemValue('expense', 'custcol_st_notcredit', j);
									nlapiLogExecution('DEBUG', 'Bill After submit Vendor Not Registered', 'i_Expense_GST_Not_CR->' + i_Expense_GST_Not_CR);
									//==== UPDATE THE BILL RECORD ====
									if (isReverseapply == 'T') 
									{
										nlapiLogExecution('DEBUG', 'Bill After submit', '***********isReverseapply->' + isReverseapply)
										var paccouttobeset = nlapiGetLineItemValue('expense', 'account', j) //if in case tax rate is 0.00
										if (currentContext.getExecutionContext() == 'csvimport' || currentContext.getExecutionContext() == 'webservices') 
										{
										
											/*
											 if (vendor_type == 'F')
											 {
											 throw "You cannot apply the  Reverse Service tax to this vendor, The vendor is of type Company";
											 }
											 */
											/*var csv_taxrate = nlapiGetLineItemValue('expense', 'taxrate1', j);
											
											var st_per = (parseFloat(csv_taxrate) / parseFloat(i_total_service_tax)) * 100;
											
											var stReverseper = parseInt(100) - parseInt(st_per);
											nlapiLogExecution('DEBUG', 'Bill After submit', '***********stReverseper->' + stReverseper)
											nlapiSelectLineItem('expense', j)
											var streverse_per_id = getSTReversePerID(stReverseper);
											nlapiSetCurrentLineItemValue('expense', 'custcol_st_recipientper', streverse_per_id);
											nlapiCommitLineItem('expense')
											nlapiLogExecution('DEBUG', 'Bill After submit', 'After commit');*/
											
											var i_TaxCode = nlapiGetLineItemValue('expense', 'taxcode', j);
											if(_logValidation(i_TaxCode))
											{
												var taxAccounts = getTaxaccount_Recipient(i_TaxCode);
												if(_logValidation(taxAccounts))
												{
													var st_recpient = taxAccounts[0];
													nlapiSelectLineItem('expense', j)
													nlapiSetCurrentLineItemValue('expense', 'custcol_st_recipientper', st_recpient);
													nlapiCommitLineItem('expense')
												}
											}
										}
										else 
										{
											var stReverseper = nlapiGetLineItemValue('expense', 'custcol_st_recipientper', j)
											nlapiLogExecution('DEBUG', 'Bill After submit', '***********stReverseper->' + stReverseper)
										}
										var amount = nlapiGetLineItemValue('expense', 'amount', j)
										
										if (parseFloat(stReverseper) > 0) 
										{
											var taxcode = nlapiGetLineItemValue('expense', 'taxcode', j)
											
											var location1 = nlapiGetLineItemValue('expense', 'location', j);
											nlapiLogExecution('DEBUG', 'Bill After submit', 'location1->' + location1)
											var department1 = nlapiGetLineItemValue('expense', 'department', j);
											nlapiLogExecution('DEBUG', 'Bill After submit', 'department1->' + department1)
											var class1 = nlapiGetLineItemValue('expense', 'class', j);
											nlapiLogExecution('DEBUG', 'Bill After submit', 'class1->' + class1)
											var projectID = nlapiGetLineItemValue('expense', 'custcol_project_entity_id', j);
											nlapiLogExecution('DEBUG', 'Bill After submit', 'projectID->' + projectID)
											//=======================Code to add swacchh cess expense account=====================//
											
											var sbc_expense_acc = nlapiGetLineItemValue('expense', 'account', j)
											
											var taxAccounts = getTaxaccount(taxcode)
											
											for (var i = 0; i < taxAccounts.length; i = 7 + parseInt(i)) 
											{
											
												nlapiLogExecution('DEBUG', 'Bill After submit', "taxAccounts->" + taxAccounts[i])
												nlapiLogExecution('DEBUG', 'Bill After submit', "tax Sales Accounts->" + taxAccounts[i + 1])
												nlapiLogExecution('DEBUG', 'Bill After submit', "taxRates->" + taxAccounts[i + 2])
												nlapiLogExecution('DEBUG', 'Bill After submit', "taxType->" + taxAccounts[i + 3])
												nlapiLogExecution('DEBUG', 'Bill After submit', "taxID->" + taxAccounts[i + 4])
												//Code added on 09/03/2017 For GST Tax Code Mapping
												nlapiLogExecution('DEBUG', 'Bill After submit', "i_RCM_Rate->" + taxAccounts[i + 6])
												//EndOf Code added on 09/03/2017 For GST Tax Code Mapping
												if (Isreciviabl == 'T') 
												{
													var purchaseAccounts = paccouttobeset;
													nlapiLogExecution('DEBUG', 'Bill After submit', "Newly setfor checking account->" + purchaseAccounts)
													
												} // END if(Isreciviabl == 'T')
												else 
												{
													var purchaseAccounts = taxAccounts[i] //comment for testing
												} //end ELSE if for setting purchase account
												var purchaseAccounts_Temp = taxAccounts[i];
												var salesAccount = taxAccounts[i + 1]
												var taxRate = taxAccounts[i + 2]
												var taxType = taxAccounts[i + 3]
												var taxID = taxAccounts[i + 4]
												var s_TaxType_Text = taxAccounts[i + 5]
												//Code added on 09/03/2017 For GST Tax Code Mapping
												var i_RCM_Rate = taxAccounts[i + 6]
												nlapiLogExecution('DEBUG', 'Bill After submit', "i_RCM_Rate->" + i_RCM_Rate)
												//EndOf Code added on 09/03/2017 For GST Tax Code Mapping
												//Setting for Tax rate if tax rate is 0.00%- Changed on 29-03-2013
												if (Isreciviabl == 'T') 
												{
													if (parseFloat(taxRate) == parseFloat(0)) 
													{
														//Code added on 09/03/2017 For GST Tax Code Mapping
														var taxType_Temp = taxType+'@'+i_RCM_Rate;
														nlapiLogExecution('DEBUG', 'Bill After submit', "taxType_Temp->" + taxType_Temp)
														//EndOf Code added on 09/03/2017 For GST Tax Code Mapping
														if (taxType_Temp in st_details_obj) 
														{
															st_value = st_details_obj[taxType_Temp]
															nlapiLogExecution('DEBUG', 'Bill After submit', "taxID->" + st_value)
															
															var i_st_taxrate = st_value.split('@')[0]
															nlapiLogExecution('DEBUG', 'Bill After submit', "i_st_taxrate->" + i_st_taxrate)
															
															var st_expense = st_value.split('@')[1]
															nlapiLogExecution('DEBUG', 'Bill After submit', "st_expense->" + st_expense)
															
															var st_inputAcc = st_value.split('@')[2]
															nlapiLogExecution('DEBUG', 'Bill After submit', "st_inputAcc->" + st_inputAcc)
															
															var st_outputAcc = st_value.split('@')[3]
															nlapiLogExecution('DEBUG', 'Bill After submit', "st_outputAcc->" + st_outputAcc)
															
															var st_class = st_value.split('@')[4]
															var st_department = st_value.split('@')[5]
															var st_location = st_value.split('@')[6]
															
															var calTaxrate = parseFloat(i_st_taxrate) - ((parseFloat(i_st_taxrate) * (100 - parseInt(stReverseper))) / 100)
															nlapiLogExecution('DEBUG', 'Bill After submit', "calTaxrat 1 !->" + calTaxrate)
		
														}
														
													}// end if
													else 
													{
														//Code added on 09/03/2017 For GST Tax Code Mapping
														var taxType_Temp = taxType+'@'+i_RCM_Rate;
														nlapiLogExecution('DEBUG', 'Bill After submit', "taxType_Temp->" + taxType_Temp)
														//EndOf Code added on 09/03/2017 For GST Tax Code Mapping
														if (taxType_Temp in st_details_obj) 
														{
															st_value = st_details_obj[taxType_Temp]
															nlapiLogExecution('DEBUG', 'Bill After submit', "taxID->" + st_value)
															
															var i_st_taxrate = st_value.split('@')[0]
															nlapiLogExecution('DEBUG', 'Bill After submit', "i_st_taxrate->" + i_st_taxrate)
															
															var st_expense = st_value.split('@')[1]
															nlapiLogExecution('DEBUG', 'Bill After submit', "st_expense->" + st_expense)
															
															var st_inputAcc = st_value.split('@')[2]
															nlapiLogExecution('DEBUG', 'Bill After submit', "st_inputAcc->" + st_inputAcc)
															
															var st_outputAcc = st_value.split('@')[3]
															nlapiLogExecution('DEBUG', 'Bill After submit', "st_outputAcc->" + st_outputAcc)
															
															var st_class = st_value.split('@')[4]
															var st_department = st_value.split('@')[5]
															var st_location = st_value.split('@')[6]
															
															var calTaxrate = parseFloat(i_st_taxrate) - parseFloat(taxRate)
															nlapiLogExecution('DEBUG', 'Bill After submit', "calTaxrate 2->" + calTaxrate)
														}
														
													}//end else- for txarate==0.00
												} // END if (Isreciviabl == 'T')
												else 
												{
													//Code added on 09/03/2017 For GST Tax Code Mapping
													var taxType_Temp = taxType+'@'+i_RCM_Rate;
													nlapiLogExecution('DEBUG', 'Bill After submit', "taxType_Temp->" + taxType_Temp)
													//EndOf Code added on 09/03/2017 For GST Tax Code Mapping
													if (taxType_Temp in st_details_obj) 
													{
								
														st_value = st_details_obj[taxType_Temp]
														nlapiLogExecution('DEBUG', 'Bill After submit', "taxID->" + st_value)
														
														var i_st_taxrate = st_value.split('@')[0]
														nlapiLogExecution('DEBUG', 'Bill After submit', "i_st_taxrate->" + i_st_taxrate)
														
														var st_expense = st_value.split('@')[1]
														nlapiLogExecution('DEBUG', 'Bill After submit', "st_expense->" + st_expense)
														
														var st_inputAcc = st_value.split('@')[2]
														nlapiLogExecution('DEBUG', 'Bill After submit', "st_inputAcc->" + st_inputAcc)
														
														var st_outputAcc = st_value.split('@')[3]
														nlapiLogExecution('DEBUG', 'Bill After submit', "st_outputAcc->" + st_outputAcc)
														
														var st_class = st_value.split('@')[4]
														var st_department = st_value.split('@')[5]
														var st_location = st_value.split('@')[6]
														
														var calTaxrate = parseFloat(i_st_taxrate) - parseFloat(taxRate)
														nlapiLogExecution('DEBUG', 'Bill After submit', "calTaxrate 3->" + calTaxrate)
													}
													
												} //End -else for IS reciviable
												//End Setting for Tax rate if tax rate is 0.00%- Changed on 29-03-2013
												
												var taxAmount = parseFloat(amount) * (parseFloat(calTaxrate) / 100)
												taxAmount = applySTRoundMethod(stRoundMethod, taxAmount)
												nlapiSelectNewLineItem('expense');
												if (st_expense == 'T') 
												{
													nlapiSetCurrentLineItemValue('expense', 'account', sbc_expense_acc);
												}
												else 
												{
													if (st_inputAcc != null && st_inputAcc != '' && st_inputAcc != undefined) 
													{
														nlapiSetCurrentLineItemValue('expense', 'account', st_inputAcc);
													}
													else 
													{
														nlapiSetCurrentLineItemValue('expense', 'account', purchaseAccounts);
													}
													nlapiSetCurrentLineItemValue('expense', 'custcol_st_code', taxID);
												}
												nlapiSetCurrentLineItemValue('expense', 'amount', taxAmount);
												nlapiSetCurrentLineItemValue('expense', 'taxcode', i_taxcode);
												nlapiSetCurrentLineItemValue('expense', 'custcol_st_reverseline', 'T');
												nlapiSetCurrentLineItemValue('expense', 'memo', 'RCM - ' + s_TaxType_Text);
												nlapiSetCurrentLineItemValue('expense', 'custcol_iit_rcm_input_account', purchaseAccounts_Temp);
												if (st_expense == 'T') 
												{
													
													nlapiSetCurrentLineItemValue('expense', 'location', location1);
													nlapiSetCurrentLineItemValue('expense', 'department', department1);
													nlapiSetCurrentLineItemValue('expense', 'class', class1);
													nlapiSetCurrentLineItemValue('expense', 'custcol_project_entity_id', projectID);
												}
												else 
												{
													if (nullvalidation(st_location))
													{
													
														nlapiSetCurrentLineItemValue('expense', 'location', st_location);	
													}
													else 
													{
														nlapiSetCurrentLineItemValue('expense', 'location', location1);
													}
													if (nullvalidation(st_department)) 
													{
													
														nlapiSetCurrentLineItemValue('expense', 'department', st_department);
													}
													else 
													{
														nlapiSetCurrentLineItemValue('expense', 'department', department1);
													}
													if (nullvalidation(st_class)) 
													{
													
														nlapiSetCurrentLineItemValue('expense', 'class', st_class);
													}
													else 
													{
														nlapiSetCurrentLineItemValue('expense', 'class', class1);
													}
													nlapiSetCurrentLineItemValue('expense', 'custcol_project_entity_id', projectID);
												}
												nlapiCommitLineItem('expense');
												
												nlapiSelectNewLineItem('expense');
												/*
												 if (st_expense == 'T')
												 {
												 nlapiSetCurrentLineItemValue('expense', 'account', salesAccount);
												 }
												 else
												 */
												{
													if (st_outputAcc != null && st_outputAcc != '' && st_outputAcc != undefined) 
													{
														nlapiSetCurrentLineItemValue('expense', 'account', st_outputAcc);
													}
													else 
													{
														nlapiSetCurrentLineItemValue('expense', 'account', salesAccount);
													}
												}
												nlapiSetCurrentLineItemValue('expense', 'amount', -taxAmount);
												nlapiSetCurrentLineItemValue('expense', 'taxcode', i_taxcode);
												nlapiSetCurrentLineItemValue('expense', 'custcol_st_reverseline', 'T');
												nlapiSetCurrentLineItemValue('expense', 'memo', 'RCM - ' + s_TaxType_Text);
												nlapiSetCurrentLineItemValue('expense', 'custcol_iit_rcm_input_account', purchaseAccounts_Temp);
												if (nullvalidation(st_location)) 
												{
												
													nlapiSetCurrentLineItemValue('expense', 'location', st_location);
													
												}
												else 
												{
													nlapiSetCurrentLineItemValue('expense', 'location', location1);
												}
												if (nullvalidation(st_department)) 
												{
												
													nlapiSetCurrentLineItemValue('expense', 'department', st_department);
												}
												else 
												{
													nlapiSetCurrentLineItemValue('expense', 'department', department1);
												}
												if (nullvalidation(st_class)) 
												{
												
													nlapiSetCurrentLineItemValue('expense', 'class', st_class);
												}
												else 
												{
													nlapiSetCurrentLineItemValue('expense', 'class', class1);
												}
												nlapiSetCurrentLineItemValue('expense', 'custcol_st_code', taxID);
												nlapiSetCurrentLineItemValue('expense', 'custcol_project_entity_id', projectID);
												nlapiCommitLineItem('expense');
												
											}// END for(i=0;i<taxAccounts.length;i=4+parseInt(i))
										} // END if(parseFloat(stReverseper)>0)
									} // END if(isReverseapply=='T')
									else if (isReverseapply == 'F' && (b_Expense_Out == 'T' || i_Expense_GST_Not_CR == 'T')) 
									{
										var i_Total_Debit_Expense = 0.00;
										
										var i_Expense = nlapiGetLineItemValue('expense', 'category', j);
										nlapiLogExecution('DEBUG', 'Bill After submit Vendor Not Registered', 'i_Expense->' + i_Expense);
								
										var i_Tax_Rate = parseInt(nlapiGetLineItemValue('expense', 'taxrate1', j));
										nlapiLogExecution('DEBUG', 'Bill After submit Vendor Not Registered', 'i_Tax_Rate->' + i_Tax_Rate);
										
										if(_logValidation(i_Expense) && i_Tax_Rate != 0)
										{
											var i_Expense_Account = nlapiGetLineItemValue('expense', 'account', j);
											nlapiLogExecution('DEBUG', 'Bill After submit Vendor Not Registered', 'i_Expense_Account->' + i_Expense_Account);
											
											var b_Expense_Out = nlapiLookupField('expensecategory', i_Expense, 'custrecord_rcm_expense_out');
											nlapiLogExecution('DEBUG', 'Bill After submit Vendor Not Registered', 'b_Expense_Out->' + b_Expense_Out);
			
											var taxcode = nlapiGetLineItemValue('expense', 'taxcode', j);
											nlapiLogExecution('DEBUG', 'Bill After submit Vendor Not Registered', 'taxcode->' + taxcode);
											
											var location1 = nlapiGetLineItemValue('expense', 'location', j);
											nlapiLogExecution('DEBUG', 'Bill After submit Vendor Not Registered', 'location1->' + location1);
											var department1 = nlapiGetLineItemValue('expense', 'department', j);
											nlapiLogExecution('DEBUG', 'Bill After submit Vendor Not Registered', 'department1->' + department1);
											var class1 = nlapiGetLineItemValue('expense', 'class', j);
											nlapiLogExecution('DEBUG', 'Bill After submit Vendor Not Registered', 'class1->' + class1);
											var amount = nlapiGetLineItemValue('expense', 'amount', j);
											nlapiLogExecution('DEBUG', 'Bill After submit Vendor Not Registered', 'amount->' + amount);
											var projectID = nlapiGetLineItemValue('expense', 'custcol_project_entity_id', j);
											nlapiLogExecution('DEBUG', 'Bill After submit Vendor Not Registered', 'projectID->' + projectID);
											
											var taxAccounts = getTaxaccount_VNR(taxcode);
											nlapiLogExecution('DEBUG', 'Bill After submit Vendor Not Registered', 'taxAccounts->' + taxAccounts);
											var purchaseAccounts;
											var salesAccount;
											var taxRate;
											var taxType;
											var taxID;
											var s_TaxType_Text;
											var i_RCM_Rate;
											var i_Not_Paid_Account;
											
											if(_logValidation(taxAccounts))
											{	
												for(var i = 0; i < taxAccounts.length; i = 8 + parseInt(i)) 
												{
													nlapiLogExecution('DEBUG', 'Bill After submit Vendor Not Registered', "taxAccounts->" + taxAccounts[i])
													nlapiLogExecution('DEBUG', 'Bill After submit Vendor Not Registered', "tax Sales Accounts->" + taxAccounts[i + 1])
													nlapiLogExecution('DEBUG', 'Bill After submit Vendor Not Registered', "taxRates->" + taxAccounts[i + 2])
													nlapiLogExecution('DEBUG', 'Bill After submit Vendor Not Registered', "taxType->" + taxAccounts[i + 3])
													nlapiLogExecution('DEBUG', 'Bill After submitVendor Not Registered', "taxID->" + taxAccounts[i + 4])
													//Code added on 09/03/2017 For GST Tax Code Mapping
													nlapiLogExecution('DEBUG', 'Bill After submit Vendor Not Registered', "i_RCM_Rate->" + taxAccounts[i + 6])
													
													purchaseAccounts = taxAccounts[i]; //comment for testing
													nlapiLogExecution('DEBUG', 'Bill After submit Vendor Not Registered', "purchaseAccounts->" + purchaseAccounts);
													
													salesAccount = taxAccounts[i + 1];
													nlapiLogExecution('DEBUG', 'Bill After submit Vendor Not Registered', "salesAccount->" + salesAccount);
													
													taxRate = taxAccounts[i + 2];
													nlapiLogExecution('DEBUG', 'Bill After submit Vendor Not Registered', "taxRate->" + taxRate);
													
													taxType = taxAccounts[i + 3];
													nlapiLogExecution('DEBUG', 'Bill After submit Vendor Not Registered', "taxType->" + taxType);
													
													taxID = taxAccounts[i + 4];
													nlapiLogExecution('DEBUG', 'Bill After submit Vendor Not Registered', "taxID->" + taxID);
													
													s_TaxType_Text = taxAccounts[i + 5];
													nlapiLogExecution('DEBUG', 'Bill After submit Vendor Not Registered', "taxType_Temp->" + s_TaxType_Text);
														
													i_RCM_Rate = taxAccounts[i + 6];
													nlapiLogExecution('DEBUG', 'Bill After submit Vendor Not Registered', "i_RCM_Rate->" + i_RCM_Rate);
													
													i_Not_Paid_Account = taxAccounts[i + 7];
													nlapiLogExecution('DEBUG', 'Bill After submit Vendor Not Registered', "i_Not_Paid_Account->" + i_Not_Paid_Account);
															
													if(_logValidation(salesAccount) && _logValidation(purchaseAccounts))
													{	
																					
														var taxAmount = parseFloat(amount) * (parseFloat(taxRate) / 100)
														taxAmount = applySTRoundMethod(stRoundMethod, taxAmount)
														taxAmount = parseFloat(taxAmount)
														taxAmount = taxAmount.toFixed(2);
														
														i_Total_Debit_Expense = i_Total_Debit_Expense + parseFloat(taxAmount);
														
														/*nlapiSelectNewLineItem('expense');
																			
														nlapiSetCurrentLineItemValue('expense', 'account', st_inputAcc);
														nlapiSetCurrentLineItemValue('expense', 'amount', taxAmount);
														nlapiSetCurrentLineItemValue('expense', 'taxcode', i_Default);
														nlapiSetCurrentLineItemValue('expense', 'custcol_st_reverseline', 'T');
														nlapiSetCurrentLineItemValue('expense', 'memo', 'VNR - ' + s_TaxType_Text);
														nlapiSetCurrentLineItemValue('expense', 'location', location1);
														nlapiSetCurrentLineItemValue('expense', 'department', department1);
														nlapiSetCurrentLineItemValue('expense', 'class', class1);
														
														nlapiCommitLineItem('expense');*/
														
														/*nlapiSelectNewLineItem('expense');
								
														nlapiSetCurrentLineItemValue('expense', 'account', salesAccount);
														nlapiSetCurrentLineItemValue('expense', 'amount', -taxAmount);
														nlapiSetCurrentLineItemValue('expense', 'taxcode', i_Default);
														nlapiSetCurrentLineItemValue('expense', 'custcol_st_reverseline', 'T');
														nlapiSetCurrentLineItemValue('expense', 'memo', 'VNR - ' + s_TaxType_Text);
														nlapiSetCurrentLineItemValue('expense', 'location', location1);
														nlapiSetCurrentLineItemValue('expense', 'department', department1);
														nlapiSetCurrentLineItemValue('expense', 'class', class1);
														
														nlapiCommitLineItem('expense');*/	
														
														//if(b_Expense_Out == 'T')
														{
															nlapiSelectNewLineItem('expense');
															
															nlapiSetCurrentLineItemValue('expense', 'account', purchaseAccounts);
															nlapiSetCurrentLineItemValue('expense', 'amount', -taxAmount);
															nlapiSetCurrentLineItemValue('expense', 'taxcode', i_Default);
															nlapiSetCurrentLineItemValue('expense', 'custcol_st_reverseline', 'T');
															//nlapiSetCurrentLineItemValue('expense', 'custcol_iit_vnr_input_entry', 'T');
															nlapiSetCurrentLineItemValue('expense', 'memo', 'Expense Out - ' + s_TaxType_Text);
															nlapiSetCurrentLineItemValue('expense', 'location', location1);
															nlapiSetCurrentLineItemValue('expense', 'department', department1);
															nlapiSetCurrentLineItemValue('expense', 'class', class1);
															nlapiSetCurrentLineItemValue('expense', 'custcol_iit_rcm_input_account', purchaseAccounts);
															nlapiSetCurrentLineItemValue('expense', 'custcol_project_entity_id', projectID);
															nlapiCommitLineItem('expense');	
														}
														/*if(b_Expense_Out == 'F')
														{
															if(_logValidation(i_Not_Paid_Account))
															{
																nlapiSelectNewLineItem('expense');
																
																nlapiSetCurrentLineItemValue('expense', 'account', i_Not_Paid_Account);
																nlapiSetCurrentLineItemValue('expense', 'amount', taxAmount);
																nlapiSetCurrentLineItemValue('expense', 'taxcode', i_Default);
																nlapiSetCurrentLineItemValue('expense', 'custcol_st_reverseline', 'T');
																nlapiSetCurrentLineItemValue('expense', 'memo', 'VNR - ' + s_TaxType_Text);
																nlapiSetCurrentLineItemValue('expense', 'location', location1);
																nlapiSetCurrentLineItemValue('expense', 'department', department1);
																nlapiSetCurrentLineItemValue('expense', 'class', class1);
																
																nlapiCommitLineItem('expense');
															}
														}*/
													}
												}
												
											
												nlapiSelectNewLineItem('expense');
												
												nlapiSetCurrentLineItemValue('expense', 'account', i_Expense_Account);
												nlapiSetCurrentLineItemValue('expense', 'amount', i_Total_Debit_Expense);
												nlapiSetCurrentLineItemValue('expense', 'taxcode', i_Default);
												nlapiSetCurrentLineItemValue('expense', 'custcol_st_reverseline', 'T');
												nlapiSetCurrentLineItemValue('expense', 'memo', 'Expense Out - ' + 'Expense Account');
												nlapiSetCurrentLineItemValue('expense', 'location', location1);
												nlapiSetCurrentLineItemValue('expense', 'department', department1);
												nlapiSetCurrentLineItemValue('expense', 'class', class1);
												nlapiSetCurrentLineItemValue('expense', 'custcol_iit_rcm_input_account', purchaseAccounts);
												nlapiSetCurrentLineItemValue('expense', 'custcol_project_entity_id', projectID);
												
												nlapiCommitLineItem('expense');			
											}
										}
									}
								}
							}// END for(var i=1;i<=exLinecount;i++)
							//-----------------------------------------------Begin:code for Item--------------------------------------------------------------------------
							// === GET THE LINE COUNT =====
							var exLinecount = nlapiGetLineItemCount('item')
							
							//=== ITERATE THROUGHT EVERY EXPENSE LINE ===
							for (var j = 1; j <= exLinecount; j++) 
							{
								
								var i_Item = nlapiGetLineItemValue('item', 'item', j);
								nlapiLogExecution('DEBUG', 'Bill After submit Vendor Not Registered', 'i_Item->' + i_Item);
								
								var i_Item_GST_Not_CR = nlapiGetLineItemValue('item', 'custcol_st_notcredit', j);
								nlapiLogExecution('DEBUG', 'Bill After submit Vendor Not Registered', 'i_Item_GST_Not_CR->' + i_Item_GST_Not_CR);
								
								var i_Item_Account;
								var b_Item_Out;
								var fields = ['assetaccount', 'custitem_iit_item_expense_out','expenseaccount'];
							    var i_Lookup_Value = nlapiLookupField('item', i_Item, fields);
							    if (_logValidation(i_Lookup_Value)) 
							    {
							    	i_Item_Account = i_Lookup_Value.assetaccount;
							    	nlapiLogExecution('DEBUG', 'Bill After submit Vendor Not Registered', 'i_Item_Account->' + i_Item_Account);
							    	
							    	if(!_logValidation(i_Item_Account))
							    	{
							    		i_Item_Account = i_Lookup_Value.expenseaccount;
										nlapiLogExecution('DEBUG', 'Bill After submit Vendor Not Registered', 'i_Item_Account->' + i_Item_Account);
							    	}
							    	
							    	b_Item_Out = i_Lookup_Value.custitem_iit_item_expense_out;
									nlapiLogExecution('DEBUG', 'Bill After submit Vendor Not Registered', 'b_Item_Out->' + b_Item_Out);
							    }
							    
								var isReverseapply = nlapiGetLineItemValue('item', 'custcol_st_reverseapply', j)
								
								var Isreciviabl = nlapiGetLineItemValue('item', 'custcol_reciviableapply', j)
								
								//==== UPDATE THE BILL RECORD ====
								if (isReverseapply == 'T') 
								{
									var paccouttobeset = ''
									// var paccouttobeset = nlapiGetLineItemValue('expense', 'account', i) //if in case tax rate is 0.00
									
									
									if (currentContext.getExecutionContext() == 'csvimport' || currentContext.getExecutionContext() == 'webservices') 
									{
									
										/*
										 if (vendor_type == 'F')
										 {
										 throw "You cannot apply the  Reverse Service tax to this vendor, The vendor is of type Company";
										 }
										 */
										/*var csv_taxrate = nlapiGetLineItemValue('item', 'taxrate1', j);
										
										var st_per = (parseFloat(csv_taxrate) / parseFloat(i_total_service_tax)) * 100;
										
										var stReverseper = parseInt(100) - parseInt(st_per);
										
										nlapiSelectLineItem('item', j)
										var streverse_per_id = getSTReversePerID(stReverseper);
										nlapiSetCurrentLineItemValue('item', 'custcol_st_recipientper', streverse_per_id);
										nlapiCommitLineItem('item');*/
										var i_TaxCode = nlapiGetLineItemValue('item', 'taxcode', j);
										if(_logValidation(i_TaxCode))
										{
											var taxAccounts = getTaxaccount_Recipient(i_TaxCode);
											if(_logValidation(taxAccounts))
											{
												var st_recpient = taxAccounts[0];
												nlapiSelectLineItem('item', j)
												nlapiSetCurrentLineItemValue('item', 'custcol_st_recipientper', st_recpient);
												nlapiCommitLineItem('item')
											}
										}
									}
									else 
									{
										var stReverseper = nlapiGetLineItemValue('item', 'custcol_st_recipientper', j)
										nlapiLogExecution('DEBUG', 'Bill After submit', '***********stReverseper->' + stReverseper)
									}
									
									var amount = nlapiGetLineItemValue('item', 'amount', j)
									
									if (parseFloat(stReverseper) > 0) 
									{
										var taxcode = nlapiGetLineItemValue('item', 'taxcode', j)
										
										var location1 = nlapiGetLineItemValue('item', 'location', j);
										nlapiLogExecution('DEBUG', 'Bill After submit', 'location1->' + location1)
										var department1 = nlapiGetLineItemValue('item', 'department', j);
										nlapiLogExecution('DEBUG', 'Bill After submit', 'department1->' + department1)
										var class1 = nlapiGetLineItemValue('item', 'class', j);
										nlapiLogExecution('DEBUG', 'Bill After submit', 'class1->' + class1)
										var projectID = nlapiGetLineItemValue('item', 'custcol_project_entity_id', j);
										nlapiLogExecution('DEBUG', 'Bill After submit', 'projectID->' + projectID)
										
										//================================Begin code to add SBC Expense account====================//
										
										var sbc_expense_acc = nlapiGetLineItemValue('item', 'custcol_ait_expense_acc', j);
										
										var taxAccounts = getTaxaccount(taxcode)
										
										
										for (var i = 0; i < taxAccounts.length; i = 7 + parseInt(i)) 
										{
										
											nlapiLogExecution('DEBUG', 'Bill After submit', "taxAccounts->" + taxAccounts[i])
											nlapiLogExecution('DEBUG', 'Bill After submit', "tax Sales Accounts->" + taxAccounts[i + 1])
											nlapiLogExecution('DEBUG', 'Bill After submit', "taxRates->" + taxAccounts[i + 2])
											nlapiLogExecution('DEBUG', 'Bill After submit', "taxType->" + taxAccounts[i + 3])
											nlapiLogExecution('DEBUG', 'Bill After submit', "taxID->" + taxAccounts[i + 4])
											//Code added on 09/03/2017 For GST Tax Code Mapping
											nlapiLogExecution('DEBUG', 'Bill After submit', "i_RCM_Rate->" + taxAccounts[i + 6])
											//EndOf Code added on 09/03/2017 For GST Tax Code Mapping
											if (Isreciviabl == 'T') 
											{
												var purchaseAccounts = paccouttobeset;
												nlapiLogExecution('DEBUG', 'Bill After submit', "Newly setfor checking account->" + purchaseAccounts)
												
											} // END if(Isreciviabl == 'T')
											else 
											{
												var purchaseAccounts = taxAccounts[i] //comment for testing
											} //end ELSE if for setting purchase account
											var purchaseAccounts_Temp = taxAccounts[i]
											var salesAccount = taxAccounts[i + 1]
											var taxRate = taxAccounts[i + 2]
											var taxType = taxAccounts[i + 3]
											var taxID = taxAccounts[i + 4]
											var s_TaxType_Text = taxAccounts[i + 5]
											//Code added on 09/03/2017 For GST Tax Code Mapping
											var i_RCM_Rate = taxAccounts[i + 6]
											nlapiLogExecution('DEBUG', 'Bill After submit', "i_RCM_Rate->" + i_RCM_Rate)
											//EndOf Code added on 09/03/2017 For GST Tax Code Mapping
											//Setting for Tax rate if tax rate is 0.00%- Changed on 29-03-2013
											if (Isreciviabl == 'T') 
											{
												if (parseFloat(taxRate) == parseFloat(0)) 
												{
													//Code added on 09/03/2017 For GST Tax Code Mapping
													var taxType_Temp = taxType+'@'+i_RCM_Rate;
													nlapiLogExecution('DEBUG', 'Bill After submit', "taxType_Temp->" + taxType_Temp)
													//Code added on 09/03/2017 For GST Tax Code Mapping
													if (taxType_Temp in st_details_obj) 
													{
														st_value = st_details_obj[taxType_Temp]
														nlapiLogExecution('DEBUG', 'Bill After submit', "taxID->" + st_value)
														
														var i_st_taxrate = st_value.split('@')[0]
														nlapiLogExecution('DEBUG', 'Bill After submit', "i_st_taxrate->" + i_st_taxrate)
														
														var st_expense = st_value.split('@')[1]
														nlapiLogExecution('DEBUG', 'Bill After submit', "st_expense->" + st_expense)
														
														var st_inputAcc = st_value.split('@')[2]
														nlapiLogExecution('DEBUG', 'Bill After submit', "st_inputAcc->" + st_inputAcc)
														
														var st_outputAcc = st_value.split('@')[3]
														nlapiLogExecution('DEBUG', 'Bill After submit', "st_outputAcc->" + st_outputAcc)
														
														var st_class = st_value.split('@')[4]
														var st_department = st_value.split('@')[5]
														var st_location = st_value.split('@')[6]
														
														var calTaxrate = parseFloat(i_st_taxrate) - ((parseFloat(i_st_taxrate) * (100 - parseInt(stReverseper))) / 100)
													}
													
												}// end if
												else 
												{
													//Code added on 09/03/2017 For GST Tax Code Mapping
													var taxType_Temp = taxType+'@'+i_RCM_Rate;
													nlapiLogExecution('DEBUG', 'Bill After submit', "taxType_Temp->" + taxType_Temp)
													//Code added on 09/03/2017 For GST Tax Code Mapping
													if (taxType_Temp in st_details_obj) 
													{
														st_value = st_details_obj[taxType_Temp]
														nlapiLogExecution('DEBUG', 'Bill After submit', "taxID->" + st_value)
														
														var i_st_taxrate = st_value.split('@')[0]
														nlapiLogExecution('DEBUG', 'Bill After submit', "i_st_taxrate->" + i_st_taxrate)
														
														var st_expense = st_value.split('@')[1]
														nlapiLogExecution('DEBUG', 'Bill After submit', "st_expense->" + st_expense)
														
														var st_inputAcc = st_value.split('@')[2]
														nlapiLogExecution('DEBUG', 'Bill After submit', "st_inputAcc->" + st_inputAcc)
														
														var st_outputAcc = st_value.split('@')[3]
														nlapiLogExecution('DEBUG', 'Bill After submit', "st_outputAcc->" + st_outputAcc)
														
														var st_class = st_value.split('@')[4]
														var st_department = st_value.split('@')[5]
														var st_location = st_value.split('@')[6]
														
														var calTaxrate = parseFloat(i_st_taxrate) - parseFloat(taxRate)
													}
													
												} //end else- for txarate==0.00
											} // END if (Isreciviabl == 'T')
											else 
											{
												//Code added on 09/03/2017 For GST Tax Code Mapping
												var taxType_Temp = taxType+'@'+i_RCM_Rate;
												nlapiLogExecution('DEBUG', 'Bill After submit', "taxType_Temp->" + taxType_Temp)
												//Code added on 09/03/2017 For GST Tax Code Mapping
												if (taxType_Temp in st_details_obj) 
												{
													st_value = st_details_obj[taxType_Temp]
													nlapiLogExecution('DEBUG', 'Bill After submit', "taxID->" + st_value)
													
													var i_st_taxrate = st_value.split('@')[0]
													nlapiLogExecution('DEBUG', 'Bill After submit', "i_st_taxrate->" + i_st_taxrate)
													
													var st_expense = st_value.split('@')[1]
													nlapiLogExecution('DEBUG', 'Bill After submit', "st_expense->" + st_expense)
													
													var st_inputAcc = st_value.split('@')[2]
													nlapiLogExecution('DEBUG', 'Bill After submit', "st_inputAcc->" + st_inputAcc)
													
													var st_outputAcc = st_value.split('@')[3]
													nlapiLogExecution('DEBUG', 'Bill After submit', "st_outputAcc->" + st_outputAcc)
													
													var st_class = st_value.split('@')[4]
													var st_department = st_value.split('@')[5]
													var st_location = st_value.split('@')[6]
													
													var calTaxrate = parseFloat(i_st_taxrate) - parseFloat(taxRate)
												}
												
											}//End -else for IS reciviable
											//End Setting for Tax rate if tax rate is 0.00%- Changed on 29-03-2013
											
											var taxAmount = parseFloat(amount) * (parseFloat(calTaxrate) / 100)
											taxAmount = applySTRoundMethod(stRoundMethod, taxAmount)
											taxAmount = parseFloat(taxAmount)
											taxAmount = taxAmount.toFixed(2);
											nlapiSelectNewLineItem('expense');
											if (st_expense == 'T') 
											{
												nlapiSetCurrentLineItemValue('expense', 'account', sbc_expense_acc);
											}
											else 
											{
												if (st_inputAcc != null && st_inputAcc != '' && st_inputAcc != undefined) 
												{
													nlapiSetCurrentLineItemValue('expense', 'account', st_inputAcc);
												}
												else 
												{
													nlapiSetCurrentLineItemValue('expense', 'account', purchaseAccounts);
												}
												nlapiSetCurrentLineItemValue('expense', 'custcol_st_code', taxID);
											}
											nlapiSetCurrentLineItemValue('expense', 'amount', taxAmount);
											nlapiSetCurrentLineItemValue('expense', 'taxcode', i_taxcode);
											nlapiSetCurrentLineItemValue('expense', 'custcol_st_reverseline', 'T');
											nlapiSetCurrentLineItemValue('expense', 'memo', 'RCM - ' + s_TaxType_Text);
											nlapiSetCurrentLineItemValue('expense', 'custcol_iit_rcm_input_account', purchaseAccounts_Temp);
											if (st_expense == 'T') 
											{
											
												nlapiSetCurrentLineItemValue('expense', 'location', location1);
												nlapiSetCurrentLineItemValue('expense', 'department', department1);
												nlapiSetCurrentLineItemValue('expense', 'class', class1);
												nlapiSetCurrentLineItemValue('expense', 'custcol_project_entity_id', projectID);
											}
											else 
											{
												if (nullvalidation(st_location)) 
												{
												
													nlapiSetCurrentLineItemValue('expense', 'location', st_location);
													
												}
												else 
												{
													nlapiSetCurrentLineItemValue('expense', 'location', location1);
												}
												if (nullvalidation(st_department)) 
												{
												
													nlapiSetCurrentLineItemValue('expense', 'department', st_department);
												}
												else 
												{
													nlapiSetCurrentLineItemValue('expense', 'department', department1);
												}
												if (nullvalidation(st_class)) 
												{
												
													nlapiSetCurrentLineItemValue('expense', 'class', st_class);
												}
												else 
												{
													nlapiSetCurrentLineItemValue('expense', 'class', class1);
												}
												nlapiSetCurrentLineItemValue('expense', 'custcol_project_entity_id', projectID);
											}
											nlapiCommitLineItem('expense');
											
											nlapiSelectNewLineItem('expense');
											/*
											 if (st_expense == 'T') {
											 nlapiSetCurrentLineItemValue('expense', 'account', salesAccount);
											 }
											 else
											 */
											{
												if (st_outputAcc != null && st_outputAcc != '' && st_outputAcc != undefined) 
												{
													nlapiSetCurrentLineItemValue('expense', 'account', st_outputAcc);
												}
												else 
												{
													nlapiSetCurrentLineItemValue('expense', 'account', salesAccount);
												}
											}
											nlapiSetCurrentLineItemValue('expense', 'amount', -taxAmount);
											nlapiSetCurrentLineItemValue('expense', 'taxcode', i_taxcode);
											nlapiSetCurrentLineItemValue('expense', 'custcol_st_reverseline', 'T');
											nlapiSetCurrentLineItemValue('expense', 'memo', 'RCM - ' + s_TaxType_Text);
											nlapiSetCurrentLineItemValue('expense', 'custcol_iit_rcm_input_account', purchaseAccounts_Temp);
											if (nullvalidation(st_location)) 
											{
												nlapiSetCurrentLineItemValue('expense', 'location', st_location);
											}
											else 
											{
												nlapiSetCurrentLineItemValue('expense', 'location', location1);
											}
											if (nullvalidation(st_department)) 
											{
											
												nlapiSetCurrentLineItemValue('expense', 'department', st_department);
											}
											else 
											{
												nlapiSetCurrentLineItemValue('expense', 'department', department1);
											}
											if (nullvalidation(st_class)) 
											{
											
												nlapiSetCurrentLineItemValue('expense', 'class', st_class);
											}
											else 
											{
												nlapiSetCurrentLineItemValue('expense', 'class', class1);
											}
											nlapiSetCurrentLineItemValue('expense', 'custcol_st_code', taxID);
											nlapiSetCurrentLineItemValue('expense', 'custcol_project_entity_id', projectID);
											nlapiCommitLineItem('expense');
											
										}// END for(i=0;i<taxAccounts.length;i=4+parseInt(i))
									} // END if(parseFloat(stReverseper)>0)
								} // END if(isReverseapply=='T')
								else if(isReverseapply == 'F' && (b_Item_Out == 'T' || i_Item_GST_Not_CR == 'T'))
								{

									nlapiLogExecution('DEBUG', 'Bill After submit Vendor Not Registered', '***********j_Temp->' + j_Temp);
									
									var i_Total_Debit_Item = 0.00;
									
									var i_Item = nlapiGetLineItemValue('item', 'item', j);
									nlapiLogExecution('DEBUG', 'Bill After submit Vendor Not Registered', 'i_Item->' + i_Item);
									
									var i_Tax_Rate = parseInt(nlapiGetLineItemValue('item', 'taxrate1', j));
									nlapiLogExecution('DEBUG', 'Bill After submit Vendor Not Registered', 'i_Tax_Rate->' + i_Tax_Rate);
									
									if(i_Tax_Rate != 0)
									{
										var i_Item_Account;
										var b_Item_Out;
										var fields = ['assetaccount', 'custitem_iit_item_expense_out','expenseaccount'];
									    var i_Lookup_Value = nlapiLookupField('item', i_Item, fields);
									    if (_logValidation(i_Lookup_Value)) 
									    {
									    	i_Item_Account = i_Lookup_Value.assetaccount;
									    	nlapiLogExecution('DEBUG', 'Bill After submit Vendor Not Registered', 'i_Item_Account->' + i_Item_Account);
									    	
									    	if(!_logValidation(i_Item_Account))
									    	{
									    		i_Item_Account = i_Lookup_Value.expenseaccount;
												nlapiLogExecution('DEBUG', 'Bill After submit Vendor Not Registered', 'i_Item_Account->' + i_Item_Account);
									    	}
									    	
									    	b_Item_Out = i_Lookup_Value.custitem_iit_item_expense_out;
											nlapiLogExecution('DEBUG', 'Bill After submit Vendor Not Registered', 'b_Item_Out->' + b_Item_Out);
									    }
										
										var taxcode = nlapiGetLineItemValue('item', 'taxcode', j);
										nlapiLogExecution('DEBUG', 'Bill After submit Vendor Not Registered', 'taxcode->' + taxcode);
										
										var location1 = nlapiGetLineItemValue('item', 'location', j);
										nlapiLogExecution('DEBUG', 'Bill After submit Vendor Not Registered', 'location1->' + location1);
										var department1 = nlapiGetLineItemValue('item', 'department', j);
										nlapiLogExecution('DEBUG', 'Bill After submit Vendor Not Registered', 'department1->' + department1);
										var class1 = nlapiGetLineItemValue('item', 'class', j);
										nlapiLogExecution('DEBUG', 'Bill After submit Vendor Not Registered', 'class1->' + class1);
										var amount = nlapiGetLineItemValue('item', 'amount', j);
										nlapiLogExecution('DEBUG', 'Bill After submit Vendor Not Registered', 'amount->' + amount);
										var projectID = nlapiGetLineItemValue('item', 'custcol_project_entity_id', j);
										nlapiLogExecution('DEBUG', 'Bill After submit Vendor Not Registered', 'projectID->' + projectID);
										
										
										var taxAccounts = getTaxaccount_VNR(taxcode);
										nlapiLogExecution('DEBUG', 'Bill After submit Vendor Not Registered', 'taxAccounts->' + taxAccounts);
										var purchaseAccounts;
										var salesAccount;
										var taxRate;
										var taxType;
										var taxID;
										var s_TaxType_Text;
										var i_RCM_Rate;
										var i_Not_Paid_Account;
										
										if(_logValidation(taxAccounts))
										{	
											for(var i_Temp = 0; i_Temp < taxAccounts.length; i_Temp = 8 + parseInt(i_Temp)) 
											{
												nlapiLogExecution('DEBUG', 'Bill After submit Vendor Not Registered', "taxAccounts->" + taxAccounts[i_Temp])
												nlapiLogExecution('DEBUG', 'Bill After submit Vendor Not Registered', "tax Sales Accounts->" + taxAccounts[i_Temp + 1])
												nlapiLogExecution('DEBUG', 'Bill After submit Vendor Not Registered', "taxRates->" + taxAccounts[i_Temp + 2])
												nlapiLogExecution('DEBUG', 'Bill After submit Vendor Not Registered', "taxType->" + taxAccounts[i_Temp + 3])
												nlapiLogExecution('DEBUG', 'Bill After submitVendor Not Registered', "taxID->" + taxAccounts[i_Temp + 4])
												//Code added on 09/03/2017 For GST Tax Code Mapping
												nlapiLogExecution('DEBUG', 'Bill After submit Vendor Not Registered', "i_RCM_Rate->" + taxAccounts[i_Temp + 6])
												
												purchaseAccounts = taxAccounts[i_Temp]; //comment for testing
												nlapiLogExecution('DEBUG', 'Bill After submit Vendor Not Registered', "purchaseAccounts->" + purchaseAccounts);
												
												salesAccount = taxAccounts[i_Temp + 1];
												nlapiLogExecution('DEBUG', 'Bill After submit Vendor Not Registered', "salesAccount->" + salesAccount);
												
												taxRate = taxAccounts[i_Temp + 2];
												nlapiLogExecution('DEBUG', 'Bill After submit Vendor Not Registered', "taxRate->" + taxRate);
												
												taxType = taxAccounts[i_Temp + 3];
												nlapiLogExecution('DEBUG', 'Bill After submit Vendor Not Registered', "taxType->" + taxType);
												
												taxID = taxAccounts[i_Temp + 4];
												nlapiLogExecution('DEBUG', 'Bill After submit Vendor Not Registered', "taxID->" + taxID);
												
												s_TaxType_Text = taxAccounts[i_Temp + 5];
												nlapiLogExecution('DEBUG', 'Bill After submit Vendor Not Registered', "taxType_Temp->" + s_TaxType_Text);
												
												i_RCM_Rate = taxAccounts[i_Temp + 6];
												nlapiLogExecution('DEBUG', 'Bill After submit Vendor Not Registered', "i_RCM_Rate->" + i_RCM_Rate);
												
												i_Not_Paid_Account = taxAccounts[i_Temp + 7];
												nlapiLogExecution('DEBUG', 'Bill After submit Vendor Not Registered', "i_Not_Paid_Account->" + i_Not_Paid_Account);
												
												if(_logValidation(salesAccount) && _logValidation(purchaseAccounts))
												{	
																				
													var taxAmount = parseFloat(amount) * (parseFloat(taxRate) / 100)
													taxAmount = applySTRoundMethod(stRoundMethod, taxAmount)
													taxAmount = parseFloat(taxAmount)
													taxAmount = taxAmount.toFixed(2);
													i_Total_Debit_Item = i_Total_Debit_Item + parseFloat(taxAmount);
													/*
													nlapiSelectNewLineItem('expense');
																		
													nlapiSetCurrentLineItemValue('expense', 'account', st_inputAcc);
													nlapiSetCurrentLineItemValue('expense', 'amount', taxAmount);
													nlapiSetCurrentLineItemValue('expense', 'taxcode', i_Default);
													nlapiSetCurrentLineItemValue('expense', 'custcol_st_reverseline', 'T');
													nlapiSetCurrentLineItemValue('expense', 'memo', 'VNR - ' + s_TaxType_Text);
													nlapiSetCurrentLineItemValue('expense', 'location', location1);
													nlapiSetCurrentLineItemValue('expense', 'department', department1);
													nlapiSetCurrentLineItemValue('expense', 'class', class1);
													
													nlapiCommitLineItem('expense');*/
													
													/*nlapiSelectNewLineItem('expense');
							
													nlapiSetCurrentLineItemValue('expense', 'account', salesAccount);
													nlapiSetCurrentLineItemValue('expense', 'amount', -taxAmount);
													nlapiSetCurrentLineItemValue('expense', 'taxcode', i_Default);
													nlapiSetCurrentLineItemValue('expense', 'custcol_st_reverseline', 'T');
													nlapiSetCurrentLineItemValue('expense', 'memo', 'VNR - ' + s_TaxType_Text);
													nlapiSetCurrentLineItemValue('expense', 'location', location1);
													nlapiSetCurrentLineItemValue('expense', 'department', department1);
													nlapiSetCurrentLineItemValue('expense', 'class', class1);
													
													nlapiCommitLineItem('expense');	*/
													
													//if(b_Item_Out == 'T')
													{
														nlapiSelectNewLineItem('expense');
														
														nlapiSetCurrentLineItemValue('expense', 'account', purchaseAccounts);
														nlapiSetCurrentLineItemValue('expense', 'amount', -taxAmount);
														nlapiSetCurrentLineItemValue('expense', 'taxcode', i_Default);
														nlapiSetCurrentLineItemValue('expense', 'custcol_st_reverseline', 'T');
														//nlapiSetCurrentLineItemValue('expense', 'custcol_iit_vnr_input_entry', 'T');
														nlapiSetCurrentLineItemValue('expense', 'memo', 'Expense Out - ' + s_TaxType_Text);
														nlapiSetCurrentLineItemValue('expense', 'location', location1);
														nlapiSetCurrentLineItemValue('expense', 'department', department1);
														nlapiSetCurrentLineItemValue('expense', 'class', class1);
														nlapiSetCurrentLineItemValue('expense', 'custcol_iit_rcm_input_account', purchaseAccounts);
														nlapiSetCurrentLineItemValue('expense', 'custcol_project_entity_id', projectID);
														
														nlapiCommitLineItem('expense');	
													}
													/*if(b_Item_Out == 'F')
													{
														if(_logValidation(i_Not_Paid_Account))
														{	
															nlapiSelectNewLineItem('expense');
															
															nlapiSetCurrentLineItemValue('expense', 'account', i_Not_Paid_Account);
															nlapiSetCurrentLineItemValue('expense', 'amount', taxAmount);
															nlapiSetCurrentLineItemValue('expense', 'taxcode', i_Default);
															nlapiSetCurrentLineItemValue('expense', 'custcol_st_reverseline', 'T');
															nlapiSetCurrentLineItemValue('expense', 'memo', 'VNR - ' + s_TaxType_Text);
															nlapiSetCurrentLineItemValue('expense', 'location', location1);
															nlapiSetCurrentLineItemValue('expense', 'department', department1);
															nlapiSetCurrentLineItemValue('expense', 'class', class1);
															
															nlapiCommitLineItem('expense');
														}
													}*/
												}
											}
											nlapiSelectNewLineItem('expense');
											
											nlapiSetCurrentLineItemValue('expense', 'account', i_Item_Account);
											nlapiSetCurrentLineItemValue('expense', 'amount', i_Total_Debit_Item);
											nlapiSetCurrentLineItemValue('expense', 'taxcode', i_Default);
											nlapiSetCurrentLineItemValue('expense', 'custcol_st_reverseline', 'T');
											nlapiSetCurrentLineItemValue('expense', 'memo', 'Expense Out - ' + 'Item Account');
											nlapiSetCurrentLineItemValue('expense', 'location', location1);
											nlapiSetCurrentLineItemValue('expense', 'department', department1);
											nlapiSetCurrentLineItemValue('expense', 'class', class1);
											nlapiSetCurrentLineItemValue('expense', 'custcol_iit_rcm_input_account', purchaseAccounts);
											nlapiSetCurrentLineItemValue('expense', 'custcol_project_entity_id', projectID);
											
											nlapiCommitLineItem('expense');	
										}
									}
								
								}
							} // END for(var i=1;i<=exLinecount;i++)
						//-----------------------------------------------End: code for Item--------------------------------------------------------------------------
						}
						else if(vendor_gst_liable == 'F')
						{
							
							// === GET THE LINE COUNT =====
							var exLinecount = nlapiGetLineItemCount('expense');
							nlapiLogExecution('DEBUG', 'Bill After submit Vendor Not Registered', '***********exLinecount->' + exLinecount);
							//=== ITERATE THROUGHT EVERY EXPENSE LINE ===
							for(var j = 1; j <= exLinecount; j++) 
							{
								nlapiLogExecution('DEBUG', 'Bill After submit Vendor Not Registered', '***********j->' + j);
								
								var i_Total_Debit_Expense = 0.00;
								
								var i_Expense = nlapiGetLineItemValue('expense', 'category', j);
								nlapiLogExecution('DEBUG', 'Bill After submit Vendor Not Registered', 'i_Expense->' + i_Expense);
						
								var i_Tax_Rate = parseInt(nlapiGetLineItemValue('expense', 'taxrate1', j));
								nlapiLogExecution('DEBUG', 'Bill After submit Vendor Not Registered', 'i_Tax_Rate->' + i_Tax_Rate);
								
								var i_Expense_GST_Not_CR = nlapiGetLineItemValue('expense', 'custcol_st_notcredit', j);
								nlapiLogExecution('DEBUG', 'Bill After submit Vendor Not Registered', 'i_Expense_GST_Not_CR->' + i_Expense_GST_Not_CR);
								
								if(_logValidation(i_Expense) && i_Tax_Rate != 0)
								{
									var i_Expense_Account = nlapiGetLineItemValue('expense', 'account', j);
									nlapiLogExecution('DEBUG', 'Bill After submit Vendor Not Registered', 'i_Expense_Account->' + i_Expense_Account);
									
									var b_Expense_Out = nlapiLookupField('expensecategory', i_Expense, 'custrecord_rcm_expense_out');
									nlapiLogExecution('DEBUG', 'Bill After submit Vendor Not Registered', 'b_Expense_Out->' + b_Expense_Out);
	
									var taxcode = nlapiGetLineItemValue('expense', 'taxcode', j);
									nlapiLogExecution('DEBUG', 'Bill After submit Vendor Not Registered', 'taxcode->' + taxcode);
									
									var location1 = nlapiGetLineItemValue('expense', 'location', j);
									nlapiLogExecution('DEBUG', 'Bill After submit Vendor Not Registered', 'location1->' + location1);
									var department1 = nlapiGetLineItemValue('expense', 'department', j);
									nlapiLogExecution('DEBUG', 'Bill After submit Vendor Not Registered', 'department1->' + department1);
									var class1 = nlapiGetLineItemValue('expense', 'class', j);
									nlapiLogExecution('DEBUG', 'Bill After submit Vendor Not Registered', 'class1->' + class1);
									var amount = nlapiGetLineItemValue('expense', 'amount', j);
									nlapiLogExecution('DEBUG', 'Bill After submit Vendor Not Registered', 'amount->' + amount);
									var projectID = nlapiGetLineItemValue('expense', 'custcol_project_entity_id', j);
									nlapiLogExecution('DEBUG', 'Bill After submit Vendor Not Registered', 'projectID->' + projectID);
									
									var taxAccounts = getTaxaccount_VNR(taxcode);
									nlapiLogExecution('DEBUG', 'Bill After submit Vendor Not Registered', 'taxAccounts->' + taxAccounts);
									var purchaseAccounts;
									var salesAccount;
									var taxRate;
									var taxType;
									var taxID;
									var s_TaxType_Text;
									var i_RCM_Rate;
									var i_Not_Paid_Account;
									
									if(_logValidation(taxAccounts))
									{	
										for(var i = 0; i < taxAccounts.length; i = 8 + parseInt(i)) 
										{
											nlapiLogExecution('DEBUG', 'Bill After submit Vendor Not Registered', "taxAccounts->" + taxAccounts[i])
											nlapiLogExecution('DEBUG', 'Bill After submit Vendor Not Registered', "tax Sales Accounts->" + taxAccounts[i + 1])
											nlapiLogExecution('DEBUG', 'Bill After submit Vendor Not Registered', "taxRates->" + taxAccounts[i + 2])
											nlapiLogExecution('DEBUG', 'Bill After submit Vendor Not Registered', "taxType->" + taxAccounts[i + 3])
											nlapiLogExecution('DEBUG', 'Bill After submitVendor Not Registered', "taxID->" + taxAccounts[i + 4])
											//Code added on 09/03/2017 For GST Tax Code Mapping
											nlapiLogExecution('DEBUG', 'Bill After submit Vendor Not Registered', "i_RCM_Rate->" + taxAccounts[i + 6])
											
											purchaseAccounts = taxAccounts[i]; //comment for testing
											nlapiLogExecution('DEBUG', 'Bill After submit Vendor Not Registered', "purchaseAccounts->" + purchaseAccounts);
											
											salesAccount = taxAccounts[i + 1];
											nlapiLogExecution('DEBUG', 'Bill After submit Vendor Not Registered', "salesAccount->" + salesAccount);
											
											taxRate = taxAccounts[i + 2];
											nlapiLogExecution('DEBUG', 'Bill After submit Vendor Not Registered', "taxRate->" + taxRate);
											
											taxType = taxAccounts[i + 3];
											nlapiLogExecution('DEBUG', 'Bill After submit Vendor Not Registered', "taxType->" + taxType);
											
											taxID = taxAccounts[i + 4];
											nlapiLogExecution('DEBUG', 'Bill After submit Vendor Not Registered', "taxID->" + taxID);
											
											s_TaxType_Text = taxAccounts[i + 5];
											nlapiLogExecution('DEBUG', 'Bill After submit Vendor Not Registered', "taxType_Temp->" + s_TaxType_Text);
												
											i_RCM_Rate = taxAccounts[i + 6];
											nlapiLogExecution('DEBUG', 'Bill After submit Vendor Not Registered', "i_RCM_Rate->" + i_RCM_Rate);
											
											i_Not_Paid_Account = taxAccounts[i + 7];
											nlapiLogExecution('DEBUG', 'Bill After submit Vendor Not Registered', "i_Not_Paid_Account->" + i_Not_Paid_Account);
													
											if(_logValidation(salesAccount) && _logValidation(purchaseAccounts))
											{	
																			
												var taxAmount = parseFloat(amount) * (parseFloat(taxRate) / 100)
												taxAmount = applySTRoundMethod(stRoundMethod, taxAmount)
												taxAmount = parseFloat(taxAmount)
												taxAmount = taxAmount.toFixed(2);
												
												i_Total_Debit_Expense = i_Total_Debit_Expense + parseFloat(taxAmount);
												
												var st_value_VNR;
												if (taxType in st_details_obj_VNR) 
												{
													st_value_VNR = st_details_obj_VNR[taxType]
													nlapiLogExecution('DEBUG', 'Bill After submit', "taxID->" + st_value_VNR)
													
													var i_st_taxrate = st_value_VNR.split('@')[0]
													nlapiLogExecution('DEBUG', 'Bill After submit', "i_st_taxrate->" + i_st_taxrate)
													
													var st_expense = st_value_VNR.split('@')[1]
													nlapiLogExecution('DEBUG', 'Bill After submit', "st_expense->" + st_expense)
													
													var st_inputAcc = st_value_VNR.split('@')[2]
													nlapiLogExecution('DEBUG', 'Bill After submit', "st_inputAcc->" + st_inputAcc)
													
													var st_outputAcc = st_value_VNR.split('@')[3]
													nlapiLogExecution('DEBUG', 'Bill After submit', "st_outputAcc->" + st_outputAcc)
													
													var st_class = st_value_VNR.split('@')[4]
													var st_department = st_value_VNR.split('@')[5]
													var st_location = st_value_VNR.split('@')[6]
													
													var calTaxrate = parseFloat(i_st_taxrate) - ((parseFloat(i_st_taxrate) * (100 - parseInt(stReverseper))) / 100)
													nlapiLogExecution('DEBUG', 'Bill After submit', "calTaxrat 1 !->" + calTaxrate)

												
													/*nlapiSelectNewLineItem('expense');
																		
													nlapiSetCurrentLineItemValue('expense', 'account', st_inputAcc);
													nlapiSetCurrentLineItemValue('expense', 'amount', taxAmount);
													nlapiSetCurrentLineItemValue('expense', 'taxcode', i_Default);
													nlapiSetCurrentLineItemValue('expense', 'custcol_st_reverseline', 'T');
													nlapiSetCurrentLineItemValue('expense', 'memo', 'VNR - ' + s_TaxType_Text);
													nlapiSetCurrentLineItemValue('expense', 'location', location1);
													nlapiSetCurrentLineItemValue('expense', 'department', department1);
													nlapiSetCurrentLineItemValue('expense', 'class', class1);
													
													nlapiCommitLineItem('expense');*/
													
													nlapiSelectNewLineItem('expense');
							
													nlapiSetCurrentLineItemValue('expense', 'account', st_outputAcc);
													nlapiSetCurrentLineItemValue('expense', 'amount', -taxAmount);
													nlapiSetCurrentLineItemValue('expense', 'taxcode', i_Default);
													nlapiSetCurrentLineItemValue('expense', 'custcol_st_reverseline', 'T');
													nlapiSetCurrentLineItemValue('expense', 'memo', 'VNR - ' + s_TaxType_Text);
													nlapiSetCurrentLineItemValue('expense', 'location', location1);
													nlapiSetCurrentLineItemValue('expense', 'department', department1);
													nlapiSetCurrentLineItemValue('expense', 'class', class1);
													nlapiSetCurrentLineItemValue('expense', 'custcol_iit_rcm_input_account', purchaseAccounts);
													nlapiSetCurrentLineItemValue('expense', 'custcol_project_entity_id', projectID);
													
													nlapiCommitLineItem('expense');	
													
													//if(b_Expense_Out == 'T')
													{
														nlapiSelectNewLineItem('expense');
														
														nlapiSetCurrentLineItemValue('expense', 'account', purchaseAccounts);
														nlapiSetCurrentLineItemValue('expense', 'amount', -taxAmount);
														nlapiSetCurrentLineItemValue('expense', 'taxcode', i_Default);
														nlapiSetCurrentLineItemValue('expense', 'custcol_st_reverseline', 'T');
														nlapiSetCurrentLineItemValue('expense', 'custcol_iit_vnr_input_entry', 'T');
														nlapiSetCurrentLineItemValue('expense', 'memo', 'VNR - ' + s_TaxType_Text);
														nlapiSetCurrentLineItemValue('expense', 'location', location1);
														nlapiSetCurrentLineItemValue('expense', 'department', department1);
														nlapiSetCurrentLineItemValue('expense', 'class', class1);
														nlapiSetCurrentLineItemValue('expense', 'custcol_iit_rcm_input_account', purchaseAccounts);
														nlapiSetCurrentLineItemValue('expense', 'custcol_project_entity_id', projectID);
														
														nlapiCommitLineItem('expense');	
													}
													if(b_Expense_Out == 'F' && i_Expense_GST_Not_CR == 'F')
													{
														if(_logValidation(st_inputAcc))
														{
															nlapiSelectNewLineItem('expense');
															
															nlapiSetCurrentLineItemValue('expense', 'account', st_inputAcc);
															nlapiSetCurrentLineItemValue('expense', 'amount', taxAmount);
															nlapiSetCurrentLineItemValue('expense', 'taxcode', i_Default);
															nlapiSetCurrentLineItemValue('expense', 'custcol_st_reverseline', 'T');
															nlapiSetCurrentLineItemValue('expense', 'memo', 'VNR - ' + s_TaxType_Text);
															nlapiSetCurrentLineItemValue('expense', 'location', location1);
															nlapiSetCurrentLineItemValue('expense', 'department', department1);
															nlapiSetCurrentLineItemValue('expense', 'class', class1);
															nlapiSetCurrentLineItemValue('expense', 'custcol_iit_rcm_input_account', purchaseAccounts);
															nlapiSetCurrentLineItemValue('expense', 'custcol_project_entity_id', projectID);
															
															nlapiCommitLineItem('expense');
														}
													}
												}
											}
										}
										
										if(b_Expense_Out == 'T' || i_Expense_GST_Not_CR == 'T')
										{
											nlapiSelectNewLineItem('expense');
											
											nlapiSetCurrentLineItemValue('expense', 'account', i_Expense_Account);
											nlapiSetCurrentLineItemValue('expense', 'amount', i_Total_Debit_Expense);
											nlapiSetCurrentLineItemValue('expense', 'taxcode', i_Default);
											nlapiSetCurrentLineItemValue('expense', 'custcol_st_reverseline', 'T');
											nlapiSetCurrentLineItemValue('expense', 'memo', 'VNR - Expense Out ' + 'Expense Account');
											nlapiSetCurrentLineItemValue('expense', 'location', location1);
											nlapiSetCurrentLineItemValue('expense', 'department', department1);
											nlapiSetCurrentLineItemValue('expense', 'class', class1);
											nlapiSetCurrentLineItemValue('expense', 'custcol_iit_rcm_input_account', purchaseAccounts);
											nlapiSetCurrentLineItemValue('expense', 'custcol_project_entity_id', projectID);
											
											nlapiCommitLineItem('expense');	
										}		
									}
								}
							}// END for(var i=1;i<=exLinecount;i++)
							//-----------------------------------------------Begin:code for Item--------------------------------------------------------------------------
							// === GET THE LINE COUNT =====
							// === GET THE LINE COUNT =====
							var itemLinecount = nlapiGetLineItemCount('item');
							nlapiLogExecution('DEBUG', 'Bill After submit Vendor Not Registered', '***********itemLinecount->' + itemLinecount);
							//=== ITERATE THROUGHT EVERY EXPENSE LINE ===
							for(var j_Temp = 1; j_Temp <= itemLinecount; j_Temp++) 
							{
								nlapiLogExecution('DEBUG', 'Bill After submit Vendor Not Registered', '***********j_Temp->' + j_Temp);
								
								var i_Total_Debit_Item = 0.00;
								
								var i_Item = nlapiGetLineItemValue('item', 'item', j_Temp);
								nlapiLogExecution('DEBUG', 'Bill After submit Vendor Not Registered', 'i_Item->' + i_Item);
								
								var i_Tax_Rate = parseInt(nlapiGetLineItemValue('item', 'taxrate1', j_Temp));
								nlapiLogExecution('DEBUG', 'Bill After submit Vendor Not Registered', 'i_Tax_Rate->' + i_Tax_Rate);
								
								var i_Item_GST_Not_CR = nlapiGetLineItemValue('item', 'custcol_st_notcredit', j_Temp);
								nlapiLogExecution('DEBUG', 'Bill After submit Vendor Not Registered', 'i_Item_GST_Not_CR->' + i_Item_GST_Not_CR);
								
								if(i_Tax_Rate != 0)
								{
									var i_Item_Account;
									var b_Item_Out;
									var fields = ['assetaccount', 'custitem_iit_item_expense_out','expenseaccount'];
								    var i_Lookup_Value = nlapiLookupField('item', i_Item, fields);
								    if (_logValidation(i_Lookup_Value)) 
								    {
								    	i_Item_Account = i_Lookup_Value.assetaccount;
								    	nlapiLogExecution('DEBUG', 'Bill After submit Vendor Not Registered', 'i_Item_Account->' + i_Item_Account);
								    	
								    	if(!_logValidation(i_Item_Account))
								    	{
								    		i_Item_Account = i_Lookup_Value.expenseaccount;
											nlapiLogExecution('DEBUG', 'Bill After submit Vendor Not Registered', 'i_Item_Account->' + i_Item_Account);
								    	}
								    	
								    	b_Item_Out = i_Lookup_Value.custitem_iit_item_expense_out;
										nlapiLogExecution('DEBUG', 'Bill After submit Vendor Not Registered', 'b_Item_Out->' + b_Item_Out);
								    }
									
									var taxcode = nlapiGetLineItemValue('item', 'taxcode', j_Temp);
									nlapiLogExecution('DEBUG', 'Bill After submit Vendor Not Registered', 'taxcode->' + taxcode);
									
									var location1 = nlapiGetLineItemValue('item', 'location', j_Temp);
									nlapiLogExecution('DEBUG', 'Bill After submit Vendor Not Registered', 'location1->' + location1);
									var department1 = nlapiGetLineItemValue('item', 'department', j_Temp);
									nlapiLogExecution('DEBUG', 'Bill After submit Vendor Not Registered', 'department1->' + department1);
									var class1 = nlapiGetLineItemValue('item', 'class', j_Temp);
									nlapiLogExecution('DEBUG', 'Bill After submit Vendor Not Registered', 'class1->' + class1);
									var amount = nlapiGetLineItemValue('item', 'amount', j_Temp);
									nlapiLogExecution('DEBUG', 'Bill After submit Vendor Not Registered', 'amount->' + amount);
									var projectID = nlapiGetLineItemValue('item', 'custcol_project_entity_id', j_Temp);
									nlapiLogExecution('DEBUG', 'Bill After submit Vendor Not Registered', 'projectID->' + projectID);
									
									var taxAccounts = getTaxaccount_VNR(taxcode);
									nlapiLogExecution('DEBUG', 'Bill After submit Vendor Not Registered', 'taxAccounts->' + taxAccounts);
									var purchaseAccounts;
									var salesAccount;
									var taxRate;
									var taxType;
									var taxID;
									var s_TaxType_Text;
									var i_RCM_Rate;
									var i_Not_Paid_Account;
									
									if(_logValidation(taxAccounts))
									{	
										for(var i_Temp = 0; i_Temp < taxAccounts.length; i_Temp = 8 + parseInt(i_Temp)) 
										{
											nlapiLogExecution('DEBUG', 'Bill After submit Vendor Not Registered', "taxAccounts->" + taxAccounts[i_Temp])
											nlapiLogExecution('DEBUG', 'Bill After submit Vendor Not Registered', "tax Sales Accounts->" + taxAccounts[i_Temp + 1])
											nlapiLogExecution('DEBUG', 'Bill After submit Vendor Not Registered', "taxRates->" + taxAccounts[i_Temp + 2])
											nlapiLogExecution('DEBUG', 'Bill After submit Vendor Not Registered', "taxType->" + taxAccounts[i_Temp + 3])
											nlapiLogExecution('DEBUG', 'Bill After submitVendor Not Registered', "taxID->" + taxAccounts[i_Temp + 4])
											//Code added on 09/03/2017 For GST Tax Code Mapping
											nlapiLogExecution('DEBUG', 'Bill After submit Vendor Not Registered', "i_RCM_Rate->" + taxAccounts[i_Temp + 6])
											
											purchaseAccounts = taxAccounts[i_Temp]; //comment for testing
											nlapiLogExecution('DEBUG', 'Bill After submit Vendor Not Registered', "purchaseAccounts->" + purchaseAccounts);
											
											salesAccount = taxAccounts[i_Temp + 1];
											nlapiLogExecution('DEBUG', 'Bill After submit Vendor Not Registered', "salesAccount->" + salesAccount);
											
											taxRate = taxAccounts[i_Temp + 2];
											nlapiLogExecution('DEBUG', 'Bill After submit Vendor Not Registered', "taxRate->" + taxRate);
											
											taxType = taxAccounts[i_Temp + 3];
											nlapiLogExecution('DEBUG', 'Bill After submit Vendor Not Registered', "taxType->" + taxType);
											
											taxID = taxAccounts[i_Temp + 4];
											nlapiLogExecution('DEBUG', 'Bill After submit Vendor Not Registered', "taxID->" + taxID);
											
											s_TaxType_Text = taxAccounts[i_Temp + 5];
											nlapiLogExecution('DEBUG', 'Bill After submit Vendor Not Registered', "taxType_Temp->" + s_TaxType_Text);
											
											i_RCM_Rate = taxAccounts[i_Temp + 6];
											nlapiLogExecution('DEBUG', 'Bill After submit Vendor Not Registered', "i_RCM_Rate->" + i_RCM_Rate);
											
											i_Not_Paid_Account = taxAccounts[i_Temp + 7];
											nlapiLogExecution('DEBUG', 'Bill After submit Vendor Not Registered', "i_Not_Paid_Account->" + i_Not_Paid_Account);
											
											if(_logValidation(salesAccount) && _logValidation(purchaseAccounts))
											{	
																			
												var taxAmount = parseFloat(amount) * (parseFloat(taxRate) / 100)
												taxAmount = applySTRoundMethod(stRoundMethod, taxAmount)
												taxAmount = parseFloat(taxAmount)
												taxAmount = taxAmount.toFixed(2);
												i_Total_Debit_Item = i_Total_Debit_Item + parseFloat(taxAmount);
												
												var st_value_VNR;
												if (taxType in st_details_obj_VNR) 
												{
													st_value_VNR = st_details_obj_VNR[taxType]
													nlapiLogExecution('DEBUG', 'Bill After submit', "taxID->" + st_value_VNR)
													
													var i_st_taxrate = st_value_VNR.split('@')[0]
													nlapiLogExecution('DEBUG', 'Bill After submit', "i_st_taxrate->" + i_st_taxrate)
													
													var st_expense = st_value_VNR.split('@')[1]
													nlapiLogExecution('DEBUG', 'Bill After submit', "st_expense->" + st_expense)
													
													var st_inputAcc = st_value_VNR.split('@')[2]
													nlapiLogExecution('DEBUG', 'Bill After submit', "st_inputAcc->" + st_inputAcc)
													
													var st_outputAcc = st_value_VNR.split('@')[3]
													nlapiLogExecution('DEBUG', 'Bill After submit', "st_outputAcc->" + st_outputAcc)
													
													var st_class = st_value_VNR.split('@')[4]
													var st_department = st_value_VNR.split('@')[5]
													var st_location = st_value_VNR.split('@')[6]
													
													var calTaxrate = parseFloat(i_st_taxrate) - ((parseFloat(i_st_taxrate) * (100 - parseInt(stReverseper))) / 100)
													nlapiLogExecution('DEBUG', 'Bill After submit', "calTaxrat 1 !->" + calTaxrate)
	
												
												
													/*
													nlapiSelectNewLineItem('expense');
																		
													nlapiSetCurrentLineItemValue('expense', 'account', st_inputAcc);
													nlapiSetCurrentLineItemValue('expense', 'amount', taxAmount);
													nlapiSetCurrentLineItemValue('expense', 'taxcode', i_Default);
													nlapiSetCurrentLineItemValue('expense', 'custcol_st_reverseline', 'T');
													nlapiSetCurrentLineItemValue('expense', 'memo', 'VNR - ' + s_TaxType_Text);
													nlapiSetCurrentLineItemValue('expense', 'location', location1);
													nlapiSetCurrentLineItemValue('expense', 'department', department1);
													nlapiSetCurrentLineItemValue('expense', 'class', class1);
													
													nlapiCommitLineItem('expense');*/
													
													nlapiSelectNewLineItem('expense');
							
													nlapiSetCurrentLineItemValue('expense', 'account', st_outputAcc);
													nlapiSetCurrentLineItemValue('expense', 'amount', -taxAmount);
													nlapiSetCurrentLineItemValue('expense', 'taxcode', i_Default);
													nlapiSetCurrentLineItemValue('expense', 'custcol_st_reverseline', 'T');
													nlapiSetCurrentLineItemValue('expense', 'memo', 'VNR - ' + s_TaxType_Text);
													nlapiSetCurrentLineItemValue('expense', 'location', location1);
													nlapiSetCurrentLineItemValue('expense', 'department', department1);
													nlapiSetCurrentLineItemValue('expense', 'class', class1);
													nlapiSetCurrentLineItemValue('expense', 'custcol_project_entity_id', projectID);
													
													nlapiCommitLineItem('expense');	
													
													//if(b_Item_Out == 'T')
													{
														nlapiSelectNewLineItem('expense');
														
														nlapiSetCurrentLineItemValue('expense', 'account', purchaseAccounts);
														nlapiSetCurrentLineItemValue('expense', 'amount', -taxAmount);
														nlapiSetCurrentLineItemValue('expense', 'taxcode', i_Default);
														nlapiSetCurrentLineItemValue('expense', 'custcol_st_reverseline', 'T');
														nlapiSetCurrentLineItemValue('expense', 'custcol_iit_vnr_input_entry', 'T');
														nlapiSetCurrentLineItemValue('expense', 'memo', 'VNR - ' + s_TaxType_Text);
														nlapiSetCurrentLineItemValue('expense', 'location', location1);
														nlapiSetCurrentLineItemValue('expense', 'department', department1);
														nlapiSetCurrentLineItemValue('expense', 'class', class1);
														nlapiSetCurrentLineItemValue('expense', 'custcol_iit_rcm_input_account', purchaseAccounts);
														nlapiSetCurrentLineItemValue('expense', 'custcol_project_entity_id', projectID);
														
														nlapiCommitLineItem('expense');	
													}
													if(b_Item_Out == 'F' && i_Item_GST_Not_CR == 'F')
													{
														if(_logValidation(st_inputAcc))
														{	
															nlapiSelectNewLineItem('expense');
															
															nlapiSetCurrentLineItemValue('expense', 'account', st_inputAcc);
															nlapiSetCurrentLineItemValue('expense', 'amount', taxAmount);
															nlapiSetCurrentLineItemValue('expense', 'taxcode', i_Default);
															nlapiSetCurrentLineItemValue('expense', 'custcol_st_reverseline', 'T');
															nlapiSetCurrentLineItemValue('expense', 'memo', 'VNR - ' + s_TaxType_Text);
															nlapiSetCurrentLineItemValue('expense', 'location', location1);
															nlapiSetCurrentLineItemValue('expense', 'department', department1);
															nlapiSetCurrentLineItemValue('expense', 'class', class1);
															nlapiSetCurrentLineItemValue('expense', 'custcol_iit_rcm_input_account', purchaseAccounts);
															nlapiSetCurrentLineItemValue('expense', 'custcol_project_entity_id', projectID);
															
															nlapiCommitLineItem('expense');
														}
													}
												}
											}
										}
										if(b_Item_Out == 'T' || i_Item_GST_Not_CR == 'T')
										{
											nlapiSelectNewLineItem('expense');
											
											nlapiSetCurrentLineItemValue('expense', 'account', i_Item_Account);
											nlapiSetCurrentLineItemValue('expense', 'amount', i_Total_Debit_Item);
											nlapiSetCurrentLineItemValue('expense', 'taxcode', i_Default);
											nlapiSetCurrentLineItemValue('expense', 'custcol_st_reverseline', 'T');
											nlapiSetCurrentLineItemValue('expense', 'memo', 'VNR - Expense Out ' + 'Item Account');
											nlapiSetCurrentLineItemValue('expense', 'location', location1);
											nlapiSetCurrentLineItemValue('expense', 'department', department1);
											nlapiSetCurrentLineItemValue('expense', 'class', class1);
											nlapiSetCurrentLineItemValue('expense', 'custcol_iit_rcm_input_account', purchaseAccounts);
											nlapiSetCurrentLineItemValue('expense', 'custcol_project_entity_id', projectID);
											
											nlapiCommitLineItem('expense');	
										}
									}
								}
							}// END for(var i=1;i<=exLinecount;i++)			
						}
					}// end if (a_subisidiary[y] == i_subsidiary)
				}
			} // END if(currentContext.getExecutionContext() != 'workflow')
		}
	} 
	catch (exception) {
		nlapiLogExecution('DEBUG', 'Bill After submit', "exception->" + exception)
	}
	return true;
}

// END BEFORE SUBMIT ==================================================





// BEGIN AFTER SUBMIT =============================================

function afterSubmitRecord(type)
{
	/*  On after submit:
	 - PURPOSE
	 -
	 FIELDS USED:
	 --Field Name--				--ID--
	 */
	//  LOCAL VARIABLES
	
	if (type != 'delete') 
	{
		var currentContext = nlapiGetContext();
		
		// IF EXECUTION CONTEXT IS WORKFLOW ====
		if (currentContext.getExecutionContext() != 'scheduled') 
		{
			var s_pass_code;
			var a_subisidiary = new Array();
			var gstApplyDate;
			var i_AitGlobalRecId = SearchGlobalParameter();
			nlapiLogExecution('DEBUG', 'Invoice ', "i_AitGlobalRecId" + i_AitGlobalRecId);
			var context = nlapiGetContext();
		    var dateFormatPref = context.getPreference('dateformat');
		    nlapiLogExecution('DEBUG', 'beforeLoadRecord_ShowImportButton', 'dateFormatPref ->'+ dateFormatPref);
			
			if (i_AitGlobalRecId != 0) 
			{
				var o_AitGloRec = nlapiLoadRecord('customrecord_indian_tax_glo_parameter', i_AitGlobalRecId);
				
				a_subisidiary = o_AitGloRec.getFieldValues('custrecord_ait_subsidiary');
				nlapiLogExecution('DEBUG', 'bill ', "a_subisidiary->" + a_subisidiary);
				
				i_vatCode = o_AitGloRec.getFieldValue('custrecord_ait_vat_code');
				nlapiLogExecution('DEBUG', 'bill', "i_vatCode->" + i_vatCode);
				
				i_taxcode = o_AitGloRec.getFieldValue('custrecord_ait_taxcode');
				nlapiLogExecution('DEBUG', 'bill ', "i_taxcode->" + i_taxcode);
				
				s_pass_code = o_AitGloRec.getFieldValue('custrecord_encpwdstring');
				nlapiLogExecution('DEBUG', 'Bill ', "s_pass_code->" + s_pass_code);
				
				gstApplyDate = o_AitGloRec.getFieldValue('custrecord_ait_gst_apply_date');
				gstApplyDate = nlapiStringToDate(gstApplyDate, dateFormatPref);
				nlapiLogExecution('DEBUG', 'Bill ', "gstApplyDate->" + gstApplyDate);
			}// end global record id
			
			billid = nlapiGetRecordId()
			currentRectype = nlapiGetRecordType();
			
			if (billid != null && billid != undefined && billid != '') 
			{
				var o_billobj = nlapiLoadRecord(currentRectype, billid);
				
				var currentDate = nlapiGetFieldValue('trandate');
				currentDate = nlapiStringToDate(currentDate, dateFormatPref);
				nlapiLogExecution('DEBUG', 'customizeGlImpact', 'currentDate ->'+ currentDate);
				
				if (o_billobj != null && o_billobj != undefined && o_billobj != '') 
				{
					var i_subsidiary = o_billobj.getFieldValue('subsidiary')
					//var a_subsidiary1 = a_subisidiary.toString();
					var Flag = 0;
					var Flag = isindia_subsidiary(a_subisidiary, s_pass_code, i_subsidiary)
					if (Flag == 1 && gstApplyDate <= currentDate) 
					{
						var expense_count = o_billobj.getLineItemCount('expense')
						var item_count = o_billobj.getLineItemCount('item')
						
						nlapiLogExecution('DEBUG', 'Bill After submit', "exLinecount->" + expense_count)
						
						//=== ITERATE THROUGH EVERY EXPENSE LINE ===
						
						var temp_flag = 'F';
						for (var k = 1; k <= expense_count; k++) 
						{
							var Reverse_true = nlapiGetLineItemValue('expense', 'custcol_st_reverseline', k)
							nlapiLogExecution('DEBUG', 'Bill After submit', "isReversetrue->" + Reverse_true)
							// ===== IF IS REVERES IS TRUE THEN REMOVE THE LINE ====
							if (Reverse_true == 'T') 
							{
								temp_flag = 'T';
								break;
								
							}
						}
						var total_count = parseInt(expense_count) + parseInt(item_count)
						nlapiLogExecution('DEBUG', 'Bill After submit', "temp_flag->" + temp_flag)
						
						if ((parseInt(total_count) > parseInt(36)) && (temp_flag == 'F')) 
						{
							var params = new Array();
							params['runasadmin'] = 'T';
							params['custscript_strecordid'] = billid;
							params['custscript_strecordtype'] = currentRectype;
							params['custscript_executionctx'] = currentContext.getExecutionContext();
							var status = nlapiScheduleScript('customscript_sch_iit_gst_reverse_service', null, params);
							nlapiLogExecution('DEBUG', 'Script Scheduled ', ' Script Status -->' + status);	
						}
					}
				}
			}
		}
	}
	
	return true;
}// END afterSubmitRecord(type)
// END AFTER SUBMIT ===============================================





// BEGIN FUNCTION ===================================================
{
    //FUNCTION FOR GETTING TAX ACCOUNT FOR THE RESPECTIVE TAX CODE
    function getTaxaccount(taxcode)
	{
        var taxAccountarray = new Array()

        var taxgrpobj = nlapiLoadRecord('taxgroup', taxcode) //Load Tax group
        var taxgrplineitemcount = taxgrpobj.getLineItemCount('taxitem'); //Get line item on tax group
        nlapiLogExecution('DEBUG', 'getTaxaccount', "taxgrplineitemcount" + taxgrplineitemcount)
        var p = 0

        for (var j = 1; j <= taxgrplineitemcount; j++)
		{
            var taxname = taxgrpobj.getLineItemValue('taxitem', 'taxname', j) //Get Tax name
            nlapiLogExecution('DEBUG', 'getTaxaccount ', "taxname->" + taxname)

            if (taxname != null)
			{
                var codeobj = nlapiLoadRecord('salestaxitem', taxname) //Get tax code object
                var accountcode = codeobj.getFieldValue('purchaseaccount') //Get account code
                nlapiLogExecution('DEBUG', 'getTaxaccount', "accountcode->" + accountcode)
                var salesaccount = codeobj.getFieldValue('saleaccount') //Get account code
                nlapiLogExecution('DEBUG', 'getTaxaccount', "salesaccount->" + salesaccount)
                var rate = codeobj.getFieldValue('rate') //Get account code
                nlapiLogExecution('DEBUG', 'getTaxaccount', "rate->" + rate)
                var taxtype = codeobj.getFieldValue('taxtype') //Get account code
                nlapiLogExecution('DEBUG', 'getTaxaccount', "taxtype->" + taxtype)
				 var taxtype_text = codeobj.getFieldText('taxtype') //Get account code
                nlapiLogExecution('DEBUG', 'getTaxaccount', "taxtype_text->" + taxtype_text)
                //Code added on 09/03/2017 For GST Tax Code Mapping
                var rcm_Rate = codeobj.getFieldValue('custrecord_tax_rate');
                nlapiLogExecution('DEBUG', 'getTaxaccount', "rcm_Rate->" + rcm_Rate);
                //End Of Code added on 09/03/2017 For GST Tax Code Mapping
                taxAccountarray[p++] = accountcode
                taxAccountarray[p++] = salesaccount
                taxAccountarray[p++] = rate
                taxAccountarray[p++] = taxtype
				taxAccountarray[p++] = taxname
				taxAccountarray[p++] = taxtype_text
				taxAccountarray[p++] = rcm_Rate

            } // END if (taxname != null)
        } // END for (var j = 1; j <= taxgrplineitemcount; j++)
        return taxAccountarray
    }// END getTaxaccount(taxcode)
    
    
    function getTaxaccount_VNR(taxcode)
	{
        var taxAccountarray = new Array()

        var taxgrpobj = nlapiLoadRecord('taxgroup', taxcode) //Load Tax group
        var taxgrplineitemcount = taxgrpobj.getLineItemCount('taxitem'); //Get line item on tax group
        nlapiLogExecution('DEBUG', 'getTaxaccount', "taxgrplineitemcount" + taxgrplineitemcount)
        var p = 0

        for (var j = 1; j <= taxgrplineitemcount; j++)
		{
            var taxname = taxgrpobj.getLineItemValue('taxitem', 'taxname', j) //Get Tax name
            nlapiLogExecution('DEBUG', 'getTaxaccount ', "taxname->" + taxname)

            if (taxname != null)
			{
                var codeobj = nlapiLoadRecord('salestaxitem', taxname) //Get tax code object
                var accountcode = codeobj.getFieldValue('purchaseaccount') //Get account code
                nlapiLogExecution('DEBUG', 'getTaxaccount', "accountcode->" + accountcode)
                var salesaccount = codeobj.getFieldValue('saleaccount') //Get account code
                nlapiLogExecution('DEBUG', 'getTaxaccount', "salesaccount->" + salesaccount)
                var rate = codeobj.getFieldValue('rate') //Get account code
                nlapiLogExecution('DEBUG', 'getTaxaccount', "rate->" + rate)
                var taxtype = codeobj.getFieldValue('taxtype') //Get account code
                nlapiLogExecution('DEBUG', 'getTaxaccount', "taxtype->" + taxtype)
				 var taxtype_text = codeobj.getFieldText('taxtype') //Get account code
                nlapiLogExecution('DEBUG', 'getTaxaccount', "taxtype_text->" + taxtype_text)
                //Code added on 09/03/2017 For GST Tax Code Mapping
                var rcm_Rate = codeobj.getFieldValue('custrecord_tax_rate');
                nlapiLogExecution('DEBUG', 'getTaxaccount', "rcm_Rate->" + rcm_Rate);
                var i_Not_Paid_Account = codeobj.getFieldValue('custrecord_iit_notpaid_acct');
                nlapiLogExecution('DEBUG', 'getTaxaccount', "i_Not_Paid_Account->" + i_Not_Paid_Account);
                //End Of Code added on 09/03/2017 For GST Tax Code Mapping
                taxAccountarray[p++] = accountcode
                taxAccountarray[p++] = salesaccount
                taxAccountarray[p++] = rate
                taxAccountarray[p++] = taxtype
				taxAccountarray[p++] = taxname
				taxAccountarray[p++] = taxtype_text
				taxAccountarray[p++] = rcm_Rate
				taxAccountarray[p++] = i_Not_Paid_Account

            } // END if (taxname != null)
        } // END for (var j = 1; j <= taxgrplineitemcount; j++)
        return taxAccountarray
    }// END getTaxaccount(taxcode)
    
    
    

    //FUNCTION FOR ROUDING AMOUNT
    function RoundUPServiceTAX(n)
	{
        var newnumb = parseFloat(n).toString().split('.')

        ans = n * 1000
        ans = Math.round(ans / 10) + ""
        while (ans.length < 2)
		{
            ans = "0" + ans
        }

        len = ans.length
        ans = ans.substring(0, len - 2);



        if (newnumb[1] != '' || newnumb[1] != null || newnumb[1] != 'undefined') {
            if (newnumb[1] > 00)
			{
                ans = parseInt(newnumb[0]) + 1
                nlapiLogExecution('DEBUG', 'RoundUPServiceTAX', "Rounded up amount" + ans)
                return ans
            }
            else
			{
                return n
            }

        } // END if (newnumb[1] != '' || newnumb[1] != null || newnumb[1] != 'undefined')

    }// END  RoundUPServiceTAX(n)

    // FUNCTION FOR GETTING GLOBAL SUBSIDIARY PARAMETER

    function SearchGlobalParameter()
	{

        var a_filters = new Array();
        var a_column = new Array();
        var i_globalRecId = 0;

        a_column.push(new nlobjSearchColumn('internalid'));

        var s_serchResult = nlapiSearchRecord('customrecord_indian_tax_glo_parameter', null, null, a_column)

        if (s_serchResult != null && s_serchResult != undefined && s_serchResult != '')
		{
            for (var i = 0; i < s_serchResult.length; i++)
			{
                i_globalRecId = s_serchResult[0].getValue('internalid');
                nlapiLogExecution('DEBUG', 'Bill ', "i_globalRecId" + i_globalRecId);

            }

        }


        return i_globalRecId;
    }

}
function getSTReversePerID(stReverseper)
{
	var a_filters = new Array();
	var a_column = new Array();
	var stper_internalID;
	
	a_column.push(new nlobjSearchColumn('internalid'));
	a_column.push(new nlobjSearchColumn('name'));
	
	var s_serchResult = nlapiSearchRecord('customlist_st_recipientper', null, null, a_column)
	
	if (s_serchResult != null && s_serchResult != undefined && s_serchResult != '') 
	{
		for (var k = 0; k < s_serchResult.length; k++) 
		{
			var stper_name = s_serchResult[k].getValue('name');
			//nlapiLogExecution('DEBUG', 'Bill ', "stper_internalID" + stper_internalID);
			if (stReverseper == stper_name) 
			{
				var stper_internalID = s_serchResult[k].getValue('internalid');
				nlapiLogExecution('DEBUG', 'Bill ', "stper_internalID" + stper_internalID);
				
				break;
			}
			
		}
		
	}
	return stper_internalID;
}
function applySTRoundMethod(stRoundMethod,taxamount)
{
	var roundedSTAmount = taxamount;
	
	if(stRoundMethod == 2)
	{
		roundedSTAmount = Math.round(taxamount)
	}
	if(stRoundMethod == 3)
	{
		roundedSTAmount = Math.round(taxamount/10)*10;
	}
	if(stRoundMethod == 4)
	{
		roundedSTAmount = Math.round(taxamount/100)*100;
	}
	
	return roundedSTAmount;
}
function nullvalidation(value)
{
	if(value != '' && value != null && value != undefined)
	{
		return true;
	} 
	else
	{
		return false;
	}
}

function _logValidation(value) 
{
	 if(value!='null' && value != null && value != null && value != '' && value != undefined && value != undefined && value != 'undefined' && value != 'undefined'&& value != 'NaN' && value != NaN) 
	 {
	  return true;
	 }
	 else 
	 { 
	  return false;
	 }
}
function getTaxaccount_Recipient(taxcode)
{
    var taxAccountarray = new Array();

    var taxgrpobj = nlapiLoadRecord('taxgroup', taxcode); //Load Tax group
    var taxgrplineitemcount = taxgrpobj.getLineItemCount('taxitem'); //Get line item on tax group
    nlapiLogExecution('DEBUG', 'getTaxaccount_Recipient', "taxgrplineitemcount" + taxgrplineitemcount);

    for (var j = 1; j <= taxgrplineitemcount; j++)
	{
        var taxname = taxgrpobj.getLineItemValue('taxitem', 'taxname', j); //Get Tax name
        nlapiLogExecution('DEBUG', 'getTaxaccount_Recipient ', "taxname->" + taxname);
        var p = 0;
        if(_logValidation(taxname))
		{
        	var i_GST_Recipient_Percentage = nlapiLookupField('salestaxitem', taxname, 'custrecord_iit_recipient_tax_percent');
           //alert('i_GST_Recipient_Percentage'+i_GST_Recipient_Percentage);
        	/* var codeobj = nlapiLoadRecord('salestaxitem', taxname); //Get tax code object
            var i_Tax_Name = codeobj.getFieldText('custrecord_iit_gst_sub_type'); //Get Sub Type*/
        	nlapiLogExecution('DEBUG', 'getTaxaccount_Recipient', "i_GST_Recipient_Percentage->" + i_GST_Recipient_Percentage);
            taxAccountarray[p++] = i_GST_Recipient_Percentage;

        } // END if (taxname != null)
    } // END for (var j = 1; j <= taxgrplineitemcount; j++)
    return taxAccountarray;
}// END getTaxaccount_Recipient(taxcode)
// END FUNCTION =====================================================