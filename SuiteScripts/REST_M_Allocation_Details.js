/**
 * RESTlet for Allocation Details
 * 
 * Version Date Author Remarks 1.00 11 Apr 2016 Nitish Mishra
 * 
 */

function postRESTlet(dataIn) {
	var response = new Response();
	try {
		nlapiLogExecution('debug', 'dataIn', JSON.stringify(dataIn));
		var employeeId = getUserUsingEmailId(dataIn.EmailId);
		var allocationId = dataIn.Data ? dataIn.Data.AllocationId : '';
		var requestType = dataIn.RequestType;

		switch (requestType) {

			case M_Constants.Request.Get:

				if (allocationId) {
					response.Data = getAllocationDetails(employeeId,
					        allocationId);
					response.Status = true;
				} else {
					response.Data = getAllAllocationList(employeeId);
					response.Status = true;
				}
			break;

		}

	} catch (err) {
		nlapiLogExecution('ERROR', 'postRESTlet', err);
		response.Data = err;
	}
	return response;
}

function getAllAllocationList(employeeId) {
	try {
		nlapiLogExecution('debug', 'employee id', employeeId);

		// get the current active allocation holiday list
		var allocationSearch = nlapiSearchRecord(
		        'resourceallocation',
		        null,
		        [ new nlobjSearchFilter('resource', null, 'anyof', employeeId) ],
		        [ new nlobjSearchColumn('company'),
		                new nlobjSearchColumn('enddate'),
		                new nlobjSearchColumn('startdate').setSort(),
		                new nlobjSearchColumn('percentoftime') ]);

		var allocationList = [];
		var today = new Date();

		if (allocationSearch) {

			allocationSearch
			        .forEach(function(allocation) {
				        var status = nlapiStringToDate(allocation
				                .getValue('enddate')) >= today;

				        allocationList.push({
				            Id : allocation.getId(),
				            Project : allocation.getText('company'),
				            StartDate : allocation.getValue('startdate'),
				            EndDate : allocation.getValue('enddate'),
				            Percent : allocation.getValue('percentoftime'),
				            Active : status
				        });
			        });
		}
		return allocationList;

	} catch (err) {
		nlapiLogExecution('error', 'getAllAllocationList', err);
		throw err;
	}
}

function getAllocationDetails(employeeId, allocationId) {
	try {
		var record = nlapiLoadRecord('resourceallocation', allocationId);

		if (record.getFieldValue('allocationresource') == employeeId) {
			var projectRec = nlapiLoadRecord('job', record
			        .getFieldValue('project'));

			return {
			    ResourceName : record.getFieldText('allocationresource'),
			    ProjectName : record.getFieldText('project'),
			    StartDate : record.getFieldValue('startdate'),
			    EndDate : record.getFieldValue('enddate'),
			    Site : record.getFieldText('custevent4'),
			    Percent : record.getFieldValue('percentoftime'),
			    Location : record.getFieldText('custeventwlocation'),
			    Practice : record.getFieldText('custevent_practice'),
			    CustomerName : projectRec.getFieldText('parent'),
			    BillingType : projectRec.getFieldText('jobbillingtype'),
			    Type : projectRec.getFieldText('jobtype'),
			    ProjectManager : projectRec
			            .getFieldText('custentity_projectmanager'),
			    DeliveryManager : projectRec
			            .getFieldText('custentity_deliverymanager'),
			    Subsidiary : projectRec.getFieldText('subsidiary'),
			    Vertical : projectRec.getFieldText('custentity_vertical'),
			    ExecutingPractice : projectRec
			            .getFieldText('custentity_practice')
			};
		} else {
			throw "You are not authorized to access this record";
		}
	} catch (err) {
		nlapiLogExecution('error', 'getAllocationDetails', err);
		throw err;
	}
}