/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       16 March 2020     Prabhat Gupta
 *
 */

/**
 * @param {Object} dataIn Parameter object
 * @returns {Object} Output object
 */
function getRESTlet(dataIn) {

	return {};
}

/**
 * @param {Object} dataIn Parameter object
 * @returns {Object} Output object
 */
function postRESTlet(dataIn) {

	try {
		
	//	dataIn = {"lastUpdatedTimestamp":""}
		var response = new Response();
		//nlapiLogExecution('debug', 'dataIn', JSON.stringify(dataIn));
		var current_date = nlapiDateToString(new Date());
		var currentDate = sysDate();
		var currentTime = timestamp(); // returns the time stamp in HH:MM:SS
		var currentDateAndTime = currentDate + ' ' + currentTime;
		//Log for current date



		var receivedDate = dataIn.lastUpdatedTimestamp;
		//nlapiLogExecution('DEBUG', 'Current Date', 'receivedDate...' + receivedDate);
		//nlapiLogExecution('DEBUG', 'Current Date', 'currentDateAndTime...' + currentDateAndTime);

		//var receivedDate = '7/12/2019 1:00 am';
		//Check if the timestamp is empty
		if (!_logValidation(receivedDate)) {          // for empty timestanp in this we will give whole data as a response
			var filters = new Array();
			filters = [
				
				 ["isinactive","is","F"]	// none of 1 => closed			 
				
				];
		//	nlapiLogExecution('debug', 'filters ', filters);
		} else {
			var filters = new Array();                             // this filter will provide the result within the current date and given date     
			filters = [
				
				["lastmodified", "within", receivedDate, currentDateAndTime]				
				];
		//	nlapiLogExecution('debug', 'filters ', filters);
		}
		
		// created search by grouping 
		var searchResults = searchRecord("customrecord_long_leave_data",null,
				filters, 
				[
				      
					   new nlobjSearchColumn("custrecord_leave_st_date"), 
					   new nlobjSearchColumn("custrecord_leave_end_date"), 
					   new nlobjSearchColumn("custrecord_leave_status"), 
					   new nlobjSearchColumn("custrecord_leave_emp_id"),
					   new nlobjSearchColumn("custrecord_leave_type"), //prabhat gupta NIS-1347 27/05/2020
					   new nlobjSearchColumn("isinactive")
					 
					
				]
				);	

      var resourceAllocationSearchResults = searchRecord("resourceallocation",null,
				[
				
				 ["employee.custentity_employee_inactive","is","F"], 
				 "AND", 
				 ["employee.custentity_implementationteam","is","F"],	
                  "AND", 
				 ["startdate","notafter","today"],				 
				  "AND", 
				 ["enddate","notbefore","today"]
				
				], 
				[
				   new nlobjSearchColumn("entityid","job",null),
				   new nlobjSearchColumn("altname","job",null),
				  new nlobjSearchColumn("internalid","employee",null), 
				   new nlobjSearchColumn("internalid","job",null), 
				   new nlobjSearchColumn("custentity_projectmanager","job",null), 
				   new nlobjSearchColumn("internalid","customer",null), 
				   new nlobjSearchColumn("location","employee",null), 
				   new nlobjSearchColumn("startdate"), 
				   new nlobjSearchColumn("enddate"), 
				   new nlobjSearchColumn("percentoftime"), 
					new nlobjSearchColumn("custeventrbillable"),  //prabhat gupta
					new nlobjSearchColumn("custentity_practice","job",null), //prabhat gupta 
					new nlobjSearchColumn("jobbillingtype","job",null)
				]
				);		
						
		
		
		var longLeaveData = [];
        
		if (searchResults) {
			
			
			for(var i=0; i<searchResults.length; i++){    
				
		    var longLeave = {};
                   
			var longLeaveInternalId = searchResults[i].getId();
			var employeeInternalId = searchResults[i].getValue("custrecord_leave_emp_id");
			var employeeName = searchResults[i].getText("custrecord_leave_emp_id");
			var startDate = searchResults[i].getValue("custrecord_leave_st_date");
			var endDate = searchResults[i].getValue("custrecord_leave_end_date");
			var leaveStatus = searchResults[i].getValue("custrecord_leave_status");
			var leaveType = searchResults[i].getValue("custrecord_leave_type");               //prabhat gupta NIS-1347 27/05/2020
			var projectDetail = getAllocation(resourceAllocationSearchResults,employeeInternalId);
			var isInactive = searchResults[i].getValue("isinactive");
			
			longLeave.longLeaveInternalId = longLeaveInternalId;
			longLeave.employeeInternalId = employeeInternalId;
			longLeave.employeeName = employeeName;
			longLeave.startDate = startDate;
			longLeave.endDate = endDate;
			longLeave.leaveStatus = leaveStatus;
			longLeave.leaveType = leaveType;              //prabhat gupta NIS-1347 27/05/2020
			longLeave.projectDetail = projectDetail;
			longLeave.isInactive = isInactive;
			
				
				
			longLeaveData.push(longLeave);
			
		    }
		}
		

		response.timeStamp = currentDateAndTime;
		response.data = longLeaveData;
		//response.data = dataRow;
		response.status = true;
	//	nlapiLogExecution('debug', 'response',JSON.stringify(response));
	} catch (err) {
		nlapiLogExecution('ERROR', 'postRESTlet', err);
		response.timeStamp = '';
		response.data = err;
		response.status = false;
	}
	return response;

}
//validate blank entries
function _logValidation(value) {
	if (value != null && value.toString() != null && value != '' && value != undefined && value.toString() != undefined && value != 'undefined' && value.toString() != 'undefined' && value.toString() != 'NaN' && value != NaN) {
		return true;
	} else {
		return false;
	}
}
//Get current date
function sysDate() {
	var date = new Date();
	var tdate = date.getDate();
	var month = date.getMonth() + 1; // jan = 0
	var year = date.getFullYear();
	return currentDate = month + '/' + tdate + '/' + year;
}

//Returns timestamp
function timestamp() {
	var str = "";

	var currentTime = new Date();
	var hours = currentTime.getHours();
	var minutes = currentTime.getMinutes();
	var seconds = currentTime.getSeconds();
	var meridian = "";
	if (hours > 12) {
		meridian += "pm";
	} else {
		meridian += "am";
	}
	if (hours > 12) {

		hours = hours - 12;
	}
	if (minutes < 10) {
		minutes = "0" + minutes;
	}
	if (seconds < 10) {
		seconds = "0" + seconds;
	}
	//str += hours + ":" + minutes + ":" + seconds + " ";
	str += hours + ":" + minutes + " ";
	return str + meridian;
}

function Response() {
	this.status = "";
	this.timeStamp = "";
	this.data = "";

}

//Search function for results more than 1000
function searchRecord(recordType, savedSearch, arrFilters, arrColumns,
		filterExpression) {

	try {
		var search = null;

		// if a saved search is provided, load it and add the filters and
		// columns
		if (savedSearch) {
			search = nlapiLoadSearch(recordType, savedSearch);

			if (isArrayNotEmpty(arrFilters)) {
				search.addFilters(arrFilters);
			}

			if (isArrayNotEmpty(arrColumns)) {
				search.addColumns(arrColumns);
			}

			if (isArrayNotEmpty(filterExpression)) {
				search.setFilterExpression(filterExpression);
			}
		}
		// create a new search
		else {
			search = nlapiCreateSearch(recordType, arrFilters, arrColumns);
		}

		// run search
		var resultSet = search.runSearch();

		// iterate through the search and get all data 1000 at a time
		var searchResultCount = 0;
		var resultSlice = null;
		var searchResult = [];

		do {
			resultSlice = resultSet.getResults(searchResultCount,
					searchResultCount + 1000);

			if (resultSlice) {

				resultSlice.forEach(function(result) {

					searchResult.push(result);
					searchResultCount++;
				});
			}
		} while (isArrayNotEmpty(resultSlice) && resultSlice.length >= 1000);

		return searchResult;
	} catch (err) {
		nlapiLogExecution('ERROR', 'searchRecord', err);
		throw err;
	}
}



function isEmpty(value) {

	return value == null || value == "" || typeof(value) == undefined;
}

function isNotEmpty(value) {

	return !isEmpty(value);
}

function isArrayEmpty(argArray) {

	return !isArrayNotEmpty(argArray);
}

function isArrayNotEmpty(argArray) {

	return (isNotEmpty(argArray) && argArray.length > 0);
}
//function to format date object into NetSuite's mm/dd/yyyy format.
function formatNSDate(dateObj) {
	if (dateObj) {
		var nsFormatDate = dateObj.getFullYear() + '-' + addZeros(dateObj.getMonth() + 1, 2) + '-' + addZeros(dateObj.getDate(), 2);
		return nsFormatDate;
	}
	return null;
}

//function to add leading zeros on date parts.
function addZeros(num, len) {
	var str = num.toString();
	while (str.length < len) {
		str = '0' + str;
	}
	return str;
}

function getAllocation(allocationSearch,empId){
	
	var allocation = [];
	if(allocationSearch){
	       for(var i=0; i<allocationSearch.length; i++){		
		
				var employeeInternalId = allocationSearch[i].getValue("internalid","employee",null);
				
				if(employeeInternalId == empId ){
					var resource = {};
					var projectInternalId = allocationSearch[i].getValue("internalid","job",null);
					var pmInternalId = allocationSearch[i].getValue("custentity_projectmanager","job",null);
					var accountInternalId = allocationSearch[i].getValue("internalid","customer",null);;
					var employeeLocation =  allocationSearch[i].getText("location","employee",null);;
					var employeeAllocationStartDate = allocationSearch[i].getValue('startdate');
					var employeeAllocationEndDate = allocationSearch[i].getValue('enddate');
					var allocationPercentage = allocationSearch[i].getValue('percentoftime');
					var billable = allocationSearch[i].getValue('custeventrbillable') ;    //prabhat gupta
					var billingTypeId = allocationSearch[i].getValue('jobbillingtype',"job",null) ; //prabhat gupta
					var billingType = allocationSearch[i].getText('jobbillingtype',"job",null) ; //prabhat gupta
					var projectPracticeInternalId = allocationSearch[i].getValue("custentity_practice","job",null) ;    //prabhat gupta
					var projectPracticeName = allocationSearch[i].getText("custentity_practice","job",null) ;    //prabhat gupta							
									   
				    var projectName = allocationSearch[i].getValue("entityid","job",null) + " " + allocationSearch[i].getValue("altname","job",null) //prabhat gupta
					resource.projectInternalId = projectInternalId; 
					resource.projectId = allocationSearch[i].getValue("entityid","job",null); //prabhat gupta
					resource.projectName = projectName;  //prabhat gupta
					resource.pmInternalId = pmInternalId;
					resource.accountInternalId = accountInternalId;
					resource.employeeLocation = employeeLocation;
					resource.employeeAllocationStartDate = employeeAllocationStartDate;
					resource.employeeAllocationEndDate = employeeAllocationEndDate;
					resource.allocationPercentage	= allocationPercentage
					
					resource.billable = billable;	
					resource.billingTypeId = billingTypeId;	
					resource.billingType = billingType;			
					resource.projectPracticeInternalId = projectPracticeInternalId;
					resource.projectPracticeName = projectPracticeName;
					allocation.push(resource);
				}
			}
	}
	
	return allocation;
	
}