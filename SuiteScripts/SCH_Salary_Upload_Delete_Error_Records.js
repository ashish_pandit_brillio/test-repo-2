/**
 * @author Shweta
 */
// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
/*
   	Script Name : SCH_Salary_Upload_Delete_Error_Records.js
	Author      : Shweta Chopde
	Date        : 1 Aug 2014
    Description : Delete the the Salary Error Records


	Script Modification Log:

	-- Date --			-- Modified By --				--Requested By--				-- Description --




Below is a summary of the process controls enforced by this script file.  The control logic is described
more fully, below, in the appropriate function headers and code blocks.


     SCHEDULED FUNCTION
		- scheduledFunction(type)


     SUB-FUNCTIONS
		- The following sub-functions are called by the above core functions in order to maintain code
            modularization:

               - NOT USED

*/
}
// END SCRIPT DESCRIPTION BLOCK  ====================================


// BEGIN SCHEDULED FUNCTION =============================================

function schedulerFunction(type)
{
   try
   {
   	var i_context = nlapiGetContext();  
	
	var i_CSV_File_ID =  i_context.getSetting('SCRIPT','custscript_csv_file_no_err');
	var i_recordID =  i_context.getSetting('SCRIPT','custscript_record_id_err');
		
	nlapiLogExecution('DEBUG', 'schedulerFunction',' Record ID -->' + i_recordID);
	nlapiLogExecution('DEBUG', 'schedulerFunction',' CSV File ID -->' + i_CSV_File_ID);	
					
	i_delete_record_status = delete_error_logs_salary_upload_records(i_recordID,i_CSV_File_ID)	
	nlapiLogExecution('DEBUG', 'schedulerFunction',' Delete Status  -->' + i_delete_record_status);	
	
	
   }//TRY
   catch(exception)
   {
	  nlapiLogExecution('DEBUG', 'ERROR',' Exception Caught -->' + exception);	
   }//CATCH

}

// END SCHEDULED FUNCTION ===============================================





// BEGIN FUNCTION ===================================================
/**
 * 
 * @param {Object} value
 * 
 * Description --> If the value is blank /null/undefined returns false else returns true
 */
function _logValidation(value) 
{
 if(value!='null' && value != null && value != null && value != '' && value != undefined && value != undefined && value != 'undefined' && value!= 'undefined'&& value!= 'NaN' && value != NaN) 
 {
  return true;
 }
 else 
 { 
  return false;
 }
}


function delete_error_logs_salary_upload_records(i_recordID,i_CSV_File_ID)
{
	var i_delete_record_status = '';
	if(_logValidation(i_recordID)&&_logValidation(i_CSV_File_ID))
	{
	var filter = new Array();
	filter[0] = new nlobjSearchFilter('custrecord_saluploadparent', null, 'is', i_recordID);
	filter[1] = new nlobjSearchFilter('custrecord_csv_file_id_no_e', null, 'is', i_CSV_File_ID);
		
	var columns = new Array();
	columns[0] = new nlobjSearchColumn('internalid');
		
	var a_search_results = nlapiSearchRecord('customrecord_error_logs_salary_process', null, filter, columns);
	
	if(_logValidation(a_search_results))
	{
		for (var i = 0; i < a_search_results.length; i++) {
		
			var i_recordID = a_search_results[i].getValue('internalid');
			nlapiLogExecution('DEBUG', 'schedulerFunction', ' Salary Upload Error Logs Record ID -->' + i_recordID);
			
			var i_submitID = nlapiDeleteRecord('customrecord_error_logs_salary_process', i_recordID)
			nlapiLogExecution('DEBUG', 'schedulerFunction', ' Salary Upload Error Logs Delete ID -->' + i_submitID);
			
			if (_logValidation(i_submitID)) {
				i_delete_record_status = 'DELETED'
			}
		}
	}//Search Results		
	}//Validation
	return i_delete_record_status;	
}//Delete Error Logs Salary Upload




// END FUNCTION =====================================================
