// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
/*
   	Script Name : UES_SetDeliveryAgent.js
	Author      : Ashish Pandit
	Date        : 26 April 2018
    Description : User Event to set delivery agent as delivery manager 


	Script Modification Log:

	-- Date --			-- Modified By --				--Requested By--				-- Description --


Below is a summary of the process controls enforced by this script file.  The control logic is described
more fully, below, in the appropriate function headers and code blocks.


     BEFORE LOAD
		- beforeLoadRecord(type)



     BEFORE SUBMIT
		- beforeSubmitRecord(type)


     AFTER SUBMIT
		- afterSubmitRecord(type)



     SUB-FUNCTIONS
		- The following sub-functions are called by the above core functions in order to maintain code
            modularization:

               - NOT USED

*/
}
// END SCRIPT DESCRIPTION BLOCK  ====================================



// BEGIN GLOBAL VARIABLE BLOCK  =====================================
{
	//  Initialize any Global Variables, in particular, debugging variables...

}
// END GLOBAL VARIABLE BLOCK  =======================================



// BEGIN BEFORE LOAD ==================================================

function afterSubmitSetDeliveryAgent(type)
{
	var context = nlapiGetContext();
	var contextType = context.getExecutionContext(); 
	
	if((type=='edit' && contextType != "suitelet") || type == 'create')
	{
		var recType = nlapiGetRecordType();
		var recId = nlapiGetRecordId();
		nlapiLogExecution('debug','recType ',recType);
		
		var recObj = nlapiLoadRecord(recType,recId);
		if(recType =='customrecord_sfdc_opportunity_record')
		{
			//var i_practice = recObj.getFieldValue('custrecord_taleo_location_country');
			//var i_account = recObj.getFieldValue('custrecord_taleo_location_state');
			
			var i_practice = recObj.getFieldValue('custrecord_practice_internal_id_sfdc');
			var i_account = recObj.getFieldValue('custrecord_customer_internal_id_sfdc');
			var i_project = recObj.getFieldValue('custrecord_project_internal_id_sfdc');
			if(!i_project)
			{
				if(_logValidation(i_practice) && _logValidation(i_account))
				{
					var customrecord_fuel_delivery_anchorSearch = nlapiSearchRecord("customrecord_fuel_delivery_anchor",null,
					[
					   ["custrecord_fuel_anchor_practice","anyof",i_practice], 
					   "AND", 
					   ["custrecord_fuel_anchor_customer","anyof",i_account]
					], 
					[
					   new nlobjSearchColumn("custrecord_fuel_anchor_employee")
					]
					);
					if(customrecord_fuel_delivery_anchorSearch)
					{
						var user = customrecord_fuel_delivery_anchorSearch[0].getValue('custrecord_fuel_anchor_employee');
						var a_userArray = new Array();
						a_userArray = getIds(user) 
						recObj.setFieldValues('custrecord_sfdc_opportunity_dm',a_userArray);
						var recId = nlapiSubmitRecord(recObj);
					}
					return true;
				}
			}
			else
			{
				var i_pm = nlapiLookupField('job',i_project,'custentity_projectmanager');
				nlapiLogExecution('Debug','i_pm ',i_pm);
				var i_dm = nlapiLookupField('job',i_project,'custentity_deliverymanager');
				recObj.setFieldValue('custrecord_sfdc_opp_pm',i_pm);
				recObj.setFieldValue('custrecord_sfdc_opportunity_dm',i_dm);
				var recId = nlapiSubmitRecord(recObj);
				return true;
			}
		}
		else if(recType =='customrecord_fulfillment_plan_sfdc')
		{
			var i_oppId = recObj.getFieldValue('custrecord_fulfill_plan_opp_id');
			var s_practice = recObj.getFieldValue('custrecord_fulfill_plan_practice');
			nlapiLogExecution('Debug','s_practice '+s_practice,'i_oppId '+i_oppId);
			if(s_practice)
			{
				var departmentSearch = nlapiSearchRecord("department",null,
				[
				   ["name","is",s_practice]
				], 
				[
				   new nlobjSearchColumn("internalid")
				]
				);
				if(departmentSearch)
				{
					var i_fulPractice = departmentSearch[0].getValue('internalid');
					nlapiLogExecution('Debug','i_fulPractice ',i_fulPractice);
				}
			}
			
			if(_logValidation(i_oppId) && _logValidation(i_fulPractice))
			{
				var recObject = nlapiLoadRecord('customrecord_sfdc_opportunity_record',i_oppId);
				var i_account = recObject.getFieldValue('custrecord_customer_internal_id_sfdc');
				
				var i_oppPractice = recObject.getFieldValue('custrecord_practice_internal_id_sfdc');
				var i_oppDM = recObject.getFieldValues('custrecord_sfdc_opportunity_dm');
				var i_project = recObject.getFieldValues('custrecord_project_internal_id_sfdc');
				if(_logValidation(i_fulPractice) && _logValidation(i_account) && !_logValidation(i_project))
				{
					var customrecord_fuel_delivery_anchorSearch = nlapiSearchRecord("customrecord_fuel_delivery_anchor",null,
					[
					   ["custrecord_fuel_anchor_practice","anyof",i_fulPractice], 
					   "AND", 
					   ["custrecord_fuel_anchor_customer","anyof",i_account]
					], 
					[
					   new nlobjSearchColumn("custrecord_fuel_anchor_employee")
					]
					);
					if(customrecord_fuel_delivery_anchorSearch)
					{
						nlapiLogExecution('Debug','customrecord_fuel_delivery_anchorSearch ',customrecord_fuel_delivery_anchorSearch.length);
						var user = customrecord_fuel_delivery_anchorSearch[0].getValue('custrecord_fuel_anchor_employee');
						var a_userArray = new Array();
						a_userArray = getIds(user) 
						if(i_oppDM)
						{
							for(var ii = 0;ii<i_oppDM.length;ii++)
							{
								a_userArray.push(i_oppDM[ii])
							}
						}
						nlapiLogExecution('Debug','a_userArray ',a_userArray);
						if(parseInt(i_oppPractice)!=parseInt(i_fulPractice))
						{
							recObject.setFieldValues('custrecord_sfdc_opportunity_dm',a_userArray);
							var recId = nlapiSubmitRecord(recObject);
							nlapiLogExecution('Debug','recId ',recId);
						}
					}
				}
				
				return true;
			}
		}
	}
}

// END BEFORE LOAD ====================================================

function getIds(user)
{
	var resultArray = new Array();
	if(_logValidation(user))
	{
		nlapiLogExecution('Debug','user in function',user);
		var temp = user.split(',');
		for(var i=0; i<temp.length;i++)
		{
			resultArray.push(temp[i]);
		}
	}
	nlapiLogExecution('Debug','resultArray ',resultArray);
	return resultArray;
}
function _logValidation(value) 
{
 if(value != null && value!= 'null' && value != '' && value != undefined && value != undefined && value != 'undefined' && value.toString() != 'NaN' && value != NaN) 
 {
  return true;
 }
 else 
 { 
  return false;
 }
}
// END FUNCTION =====================================================