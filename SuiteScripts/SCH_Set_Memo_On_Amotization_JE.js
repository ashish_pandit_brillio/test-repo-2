//https://system.na1.netsuite.com/app/common/scripting/script.nl?id=904
// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
/*
   	Script Name : SCH_Set_Memo_On_Amotization_JE.js
	Author      : Jayesh Dinde
	Date        : 20 May 2016
    Description : Set Memo based on schedule no, using Memo set on journal or bill attached on that particular
    			schedule for a particular line against JE created using Amortization option.


	Script Modification Log:

	-- Date --			-- Modified By --				--Requested By--				-- Description --




Below is a summary of the process controls enforced by this script file.  The control logic is described
more fully, below, in the appropriate function headers and code blocks.


     SCHEDULED FUNCTION
		- scheduledFunction(type)


     SUB-FUNCTIONS
		- The following sub-functions are called by the above core functions in order to maintain code
            modularization:

               - NOT USED

*/
}
// END SCRIPT DESCRIPTION BLOCK  ====================================


// BEGIN SCHEDULED FUNCTION =============================================

function Set_Memo_Amortization_JE()
{
	try
	{
		var i_context = nlapiGetContext();
		
		var search_amortization_je = nlapiSearchRecord(null, 'customsearch_amortization_je_withno_me_2', null, null);
		if (search_amortization_je)
		{
			for (var i = 0; i < search_amortization_je.length; i++)
			{
				var submit_je_flag = 0;
				var amortization_je_rcrdObj = nlapiLoadRecord('journalentry', search_amortization_je[i].getId());
				if (amortization_je_rcrdObj)
				{
					var is_je_already_processed = amortization_je_rcrdObj.getFieldValue('custbody_amortization_processed');
					nlapiLogExecution('audit','is je already processed:- '+is_je_already_processed,'JE ID:- '+search_amortization_je[i].getId());
					if(is_je_already_processed != 'T')
					{
						var line_count = amortization_je_rcrdObj.getLineItemCount('line');
						
						for (var j = 1; j <= line_count; j++)
						{
							if(i_context.getRemainingUsage() <= 2000)
							{
								nlapiYieldScript();
							}
				
							var schedule_ID = amortization_je_rcrdObj.getLineItemValue('line', 'schedulenum', j);
							
							if (_logValidation(schedule_ID))
							{
								var amortization_schedule_rcrd_obj = nlapiLoadRecord('amortizationschedule', schedule_ID);
								var created_from_id = amortization_schedule_rcrd_obj.getFieldValue('sourcetran');
								if (_logValidation(created_from_id))
								{
									var filters_search_schedule_attached_rcrd_type = new Array();
									filters_search_schedule_attached_rcrd_type[0] = new nlobjSearchFilter('internalid',null,'is',parseInt(created_from_id));
									var column_search_schedule_attached_rcrd_type = new Array();
									column_search_schedule_attached_rcrd_type[0] = new nlobjSearchColumn('recordtype');
					
									var search_schedule_attached_rcrd_type = nlapiSearchRecord('transaction', null, filters_search_schedule_attached_rcrd_type, column_search_schedule_attached_rcrd_type);
									if(search_schedule_attached_rcrd_type)
									{
										var schedule_attached_rcrd_type = search_schedule_attached_rcrd_type[0].getValue('recordtype');
										var schedule_attached_rcrd_id = search_schedule_attached_rcrd_type[0].getId();
										
										//nlapiLogExecution('audit','schedule_attached_rcrd_type:- '+schedule_attached_rcrd_type,'created_from_id:- '+created_from_id);
										
										var filters_search_schedule_attached_tran = new Array();
										filters_search_schedule_attached_tran[0] = new nlobjSearchFilter('recordtype',null,'is',schedule_attached_rcrd_type);
										filters_search_schedule_attached_tran[1] = new nlobjSearchFilter('internalid',null,'is',parseInt(created_from_id));
										
										var search_schedule_attached_rcrd_obj = nlapiSearchRecord('transaction', 'customsearch_find_attached_schedule', filters_search_schedule_attached_tran, null);
										if(search_schedule_attached_rcrd_obj)
										{
											//nlapiLogExecution('audit','trnsaction found for mapping');
											for (var s = 0; s < search_schedule_attached_rcrd_obj.length; s++)
											{
												if(i_context.getRemainingUsage() <= 2000)
												{
													nlapiYieldScript();
												}
							
												var result_C = search_schedule_attached_rcrd_obj[0];
												var columns = result_C.getAllColumns();
												var columnLen = columns.length;
												
												for (var z = 0; z < columnLen; z++)
												{
													var column = columns[z];
													var label = column.getLabel();
													
													if (label == 'Schedule Id')
													{
														var schedule_num = search_schedule_attached_rcrd_obj[s].getValue(column);
													}
													else if (label == 'Memo')
													{
														var memo = search_schedule_attached_rcrd_obj[s].getValue(column);
														if (!_logValidation(memo))
															memo = '';
													}
													else if (label == 'Proj Desc')
													{
														var proj_full_name = '';
														var proj_id = '';
														var proj_desc = search_schedule_attached_rcrd_obj[s].getValue(column);
														if (!_logValidation(proj_desc))
														{
															proj_desc = '';
														}
														else
														{
															proj_full_name = proj_desc.toString();
															proj_id_array = proj_full_name.split(' ');
															proj_id = proj_id_array[0];
														}
													}
													else if (label == 'Customer')
													{
														var proj_cust = search_schedule_attached_rcrd_obj[s].getValue(column);
														if (!_logValidation(proj_cust))
															proj_cust = '';
													}
													else if (label == 'Vendor Name')
													{
														var vendor_name = search_schedule_attached_rcrd_obj[s].getValue(column);
														if (!_logValidation(vendor_name))
															vendor_name = '';
													}
												}
												
												//nlapiLogExecution('audit','schedule_num:- '+schedule_num,'schedule_ID:- '+schedule_ID);
												if(parseInt(schedule_num) == parseInt(schedule_ID))
												{	
													//nlapiLogExecution('audit','schduled id matched');
													var i_cust_territory = '';		
													if (_logValidation(proj_id))
													{
														var filters_search_proj = new Array();
														filters_search_proj[0] = new nlobjSearchFilter('entityid',null,'is',proj_id);
														var column_search_proj = new Array();
														column_search_proj[0] = new nlobjSearchColumn('jobbillingtype');
														column_search_proj[1] = new nlobjSearchColumn('custentity_deliverymodel');
														column_search_proj[2] = new nlobjSearchColumn('territory','customer');
														var search_proj_results = nlapiSearchRecord('job', null, filters_search_proj, column_search_proj);
														if (_logValidation(search_proj_results))
														{
															var proj_billing_type = search_proj_results[0].getText('jobbillingtype');
															if (!_logValidation(proj_billing_type))
																proj_billing_type = '';
																
															var proj_onsite_offsite = search_proj_results[0].getText('custentity_deliverymodel');
															if (!_logValidation(proj_onsite_offsite))
																proj_onsite_offsite = '';
																
															i_cust_territory = search_proj_results[0].getValue('territory','customer');
														}
													}
																						
													amortization_je_rcrdObj.selectLineItem('line', j);
													amortization_je_rcrdObj.setCurrentLineItemValue('line','memo',memo);
													if(vendor_name != '')
													{
														amortization_je_rcrdObj.setCurrentLineItemValue('line','entity',vendor_name);
													}
													
													var is_je_already_processed = amortization_je_rcrdObj.getFieldValue('custbody_je_already_processed');
													if(is_je_already_processed == 'T')
													{
														
													}
													else
													{
														amortization_je_rcrdObj.setCurrentLineItemValue('line','custcol_billing_type',proj_billing_type);
														amortization_je_rcrdObj.setCurrentLineItemValue('line','custcol_onsite_offsite',proj_onsite_offsite);
													}
													
													amortization_je_rcrdObj.setCurrentLineItemValue('line','custcolprj_name',proj_desc);
													amortization_je_rcrdObj.setCurrentLineItemValue('line','custcolcustcol_temp_customer',proj_cust);
													amortization_je_rcrdObj.setCurrentLineItemValue('line','custcol_territory',i_cust_territory);
													amortization_je_rcrdObj.commitLineItem('line');
													submit_je_flag = 1;														
												}													
											}
										}
									}
								}
							}
						}
						
						if(submit_je_flag == 1)
						{
							amortization_je_rcrdObj.setFieldValue('custbody_je_already_processed','T');
							amortization_je_rcrdObj.setFieldValue('custbody_amortization_processed','T');
							amortization_je_rcrdObj.setFieldValue('custbody_is_je_updated_for_emp_type','F');
							var submitted_je_id = nlapiSubmitRecord(amortization_je_rcrdObj,true,true);
							nlapiLogExecution('audit','submitted je id:- ',submitted_je_id);
						}
					}
				}
				
				if(i_context.getRemainingUsage() <= 2000)
				{
					nlapiYieldScript();
				}
				
			}
			
			nlapiLogExecution('DEBUG','SCRIPT EXECUTION COMPLETED....');		
		}		
	}
	catch(err)
	{
		schedule_script_after_usage_exceeded(i);
		nlapiLogExecution('ERROR','ERROR MESSAGE:-- ',err);
	}
}

// END SCHEDULED FUNCTION =============================================

function _logValidation(value) 
{
 if(value!='null' && value != null && value != null && value != '' && value != undefined && value != undefined && value != 'undefined' && value!= 'undefined'&& value!= 'NaN' && value != NaN) 
 {
  return true;
 }
 else 
 { 
  return false;
 }
}

function schedule_script_after_usage_exceeded(i)
{
	 nlapiLogExecution('debug','inside scheduling function for i :- '+i);
	 var params=new Array();
	 params['status']='scheduled';
 	 params['runasadmin']='T';
	 params['custscript_counter_reschedule']=i;
	 		 
	 var startDate = new Date();
 	 params['startdate']=startDate.toUTCString();
	
	 var status=nlapiScheduleScript(nlapiGetContext().getScriptId(), nlapiGetContext().getDeploymentId(),params);
	 
}