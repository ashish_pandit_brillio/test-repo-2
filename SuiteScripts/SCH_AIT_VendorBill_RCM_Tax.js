// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
/*
   	Script Name:SCH_AIT_VendorBill_rcm_tax.js
	Author:		Nikhil jain
	Company:	Aashna Cloudtech Pvt. Ltd.
	Date:		07 March 2016
	Description:Craete the tax bill or invoice detail records for Rcm line


	Script Modification Log:

	-- Date --			-- Modified By --				--Requested By--				-- Description --




Below is a summary of the process controls enforced by this script file.  The control logic is described
more fully, below, in the appropriate function headers and code blocks.


     SCHEDULED FUNCTION
		- scheduledFunction(type)


     SUB-FUNCTIONS
		- The following sub-functions are called by the above core functions in order to maintain code
            modularization:

               - NOT USED

*/
}
// END SCRIPT DESCRIPTION BLOCK  ====================================


// BEGIN SCHEDULED FUNCTION =============================================

function schedulerFunction_create_rcmtax_rec(type)
{
	/*  On scheduled function:
	 - PURPOSE
	 -
	 FIELDS USED:
	 --Field Name--				--ID--
	 */
	//==== CODE FOR DESGNING POP UP XL ======
	
	var billid = nlapiGetContext().getSetting('SCRIPT', 'custscript_ait_venbillid')
	
	if (billid != null && billid != '' && billid != undefined) 
	{
		var o_billobj = nlapiLoadRecord('vendorbill', billid)
		
		if (o_billobj != null && o_billobj != '' && o_billobj != undefined) 
		{
			var i_exp_Linecount = o_billobj.getLineItemCount('expense')
			
			var vendor = o_billobj.getFieldValue('entity');
			var date = o_billobj.getFieldValue('trandate');
			var usertotal = o_billobj.getFieldValue('usertotal');
			var taxtotal = o_billobj.getFieldValue('taxtotal');
			var postingperiod = o_billobj.getFieldValue('postingperiod');
						
			for (var i = 1; i <= i_exp_Linecount; i++) 
			{
				var b_isReverse = o_billobj.getLineItemValue('expense', 'custcol_st_reverseline', i);
				var i_st_code = o_billobj.getLineItemValue('expense', 'custcol_st_code', i);
				var i_account = o_billobj.getLineItemValue('expense', 'account', i);
				var i_amount = o_billobj.getLineItemValue('expense', 'amount', i);
				
				if (b_isReverse == 'T' && i_st_code != null && i_st_code != '' && i_st_code != undefined) 
				{
					if (parseFloat(i_amount) > parseFloat(0)) 
					{
						record = nlapiCreateRecord('customrecord_vatbilldetail');
						record.setFieldValue('custrecord_vatbill_billno', billid)
						record.setFieldValue('custrecord_vatbill_tranno', billid)
						record.setFieldValue('custrecord_vatbill_account', i_account)
						record.setFieldValue('custrecord_vatbill_vendor', vendor);
						record.setFieldValue('custrecord_vatbill_billdate', date);
						record.setFieldValue('custrecord_vatbill_grossamount', usertotal);
						record.setFieldValue('custrecord_vatbill_taxamount', i_amount);
						record.setFieldValue('custrecord_vatbill_status', 'Open');
						record.setFieldValue('custrecord_vatbill_type', 'Bill');
						record.setFieldValue('custrecord_vatbill_taxid', i_st_code);
						record.setFieldValue('custrecord_vatbill_postingperiod', postingperiod);
						record.setFieldValue('custrecord_vatbill_rcm', 'T');
						record.setFieldValue('custrecord_vatbill_entity', vendor);
						
						var subtotal = parseFloat(usertotal) - parseFloat(taxtotal);
						record.setFieldValue('custrecord_vatbill_amount', parseFloat(subtotal));
						var id = nlapiSubmitRecord(record, true);
						nlapiLogExecution('DEBUG', 'Bill ', " New Vat Bill Detail Record ID " + id)
					}
					if (parseFloat(i_amount) < parseFloat(0)) 
					{
						record = nlapiCreateRecord('customrecord_vatinvoicedetail');
						record.setFieldValue('custrecord_vatinvoice_invoiceno', billid)
						record.setFieldValue('custrecord_vatinvoice_tranno', billid)
						record.setFieldValue('custrecord_vatinvoice_account', i_account)
						//record.setFieldValue('custrecord_vatinvoice_customer', customer);
						record.setFieldValue('custrecord_vatinvoice_date', date);
						record.setFieldValue('custrecord_vatinvoice_grossamount', usertotal);
						record.setFieldValue('custrecord_vatinvoice_taxamount', (-(i_amount)));
						record.setFieldValue('custrecord_vatinvoice_status', 'Open');
						record.setFieldValue('custrecord_vatinvoice_type', 'Bill');
						record.setFieldValue('custrecord_vatinvoice_taxid', i_st_code);
						record.setFieldValue('custrecord_vatinvoice_postingperiod', postingperiod);
						//record.setFieldValue('custrecord_vatbill_location', locationArray[k]);
						var subtotal = parseFloat(usertotal) - parseFloat(taxtotal);
						record.setFieldValue('custrecord_vatinvoice_amount', subtotal);
						record.setFieldValue('custrecord_vatinvoice_rcm', 'T');
						record.setFieldValue('custrecord_vatinvoice_entity', vendor);
						var id = nlapiSubmitRecord(record, true);
						nlapiLogExecution('DEBUG', 'Bill ', " New Tax Invoice Detail Record ID " + id)
					}
				}
			}
		}
	}
}


// END SCHEDULED FUNCTION ===============================================





// BEGIN FUNCTION ===================================================
{



}
// END FUNCTION =====================================================
