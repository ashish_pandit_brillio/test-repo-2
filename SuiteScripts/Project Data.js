/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       16 March 2020     Prabhat Gupta
 *
 */

/**
 * @param {Object} dataIn Parameter object
 * @returns {Object} Output object
 */
 function getRESTlet(dataIn) {

	return {};
}

/**
 * @param {Object} dataIn Parameter object
 * @returns {Object} Output object
 */
function postRESTlet(dataIn) {

	try {
		
		//dataIn = {"lastUpdatedTimestamp":""}
        var response = new Response();
        //nlapiLogExecution('debug', 'dataIn', JSON.stringify(dataIn));
        var currentDateAndTime = sysDate();



		var receivedDate = dataIn.lastUpdatedTimestamp;
		//nlapiLogExecution('DEBUG', 'Current Date', 'receivedDate...' + receivedDate);
		nlapiLogExecution('DEBUG', 'Current Date', 'currentDateAndTime...' + currentDateAndTime);

		//var receivedDate = '7/12/2019 1:00 am';
		//Check if the timestamp is empty
		if (!_logValidation(receivedDate)) {          // for empty timestanp in this we will give whole data as a response
			var filters = new Array();
			filters = [
				
				 ["status","noneof","1"]	// none of 1 => closed			 
				
				];
		//	nlapiLogExecution('debug', 'filters ', filters);
		} else {
			var filters = new Array();                             // this filter will provide the result within the current date and given date     
			filters = [
				
				["lastmodifieddate", "within", receivedDate, currentDateAndTime]				
				];
		//	nlapiLogExecution('debug', 'filters ', filters);
		}
		
		// created search by grouping 
		var searchResults = searchRecord("job",null,
				filters, 
				[
				       new nlobjSearchColumn("entityid"),
					   new nlobjSearchColumn("formulatext").setFormula("CONCAT({entityid},CONCAT(' ',{altname}))"), 
					   new nlobjSearchColumn("internalid","customer",null), 
					   new nlobjSearchColumn("email","CUSTENTITY_PROJECTMANAGER",null), 
					   new nlobjSearchColumn("email","CUSTENTITY_DELIVERYMANAGER",null), 
					   new nlobjSearchColumn("email","CUSTENTITY_CLIENTPARTNER",null), 
					   new nlobjSearchColumn("entitystatus"), 
					   new nlobjSearchColumn("jobtype"), 
					   new nlobjSearchColumn("custentity_project_allocation_category"), 
					   new nlobjSearchColumn("custrecord_parent_practice","CUSTENTITY_PRACTICE",null), 
					  new nlobjSearchColumn("CUSTENTITY_PRACTICE"), 
					   new nlobjSearchColumn("startdate"), 
					   new nlobjSearchColumn("custentity_location"), 
					   new nlobjSearchColumn("enddate"),
                       new nlobjSearchColumn("jobbillingtype"),
				]
				);	


						
		
		
		var projectData = [];
        
		if (searchResults) {
			
			
			for(var i=0; i<searchResults.length; i++){    
				
		    var project = {};
                   
			var projectInternalId = searchResults[i].getId();
			var projectId = searchResults[i].getValue("entityid");
			var projectName = searchResults[i].getValue("formulatext");
			var accountInternalId = searchResults[i].getValue("internalid","customer",null);
			var pmEmail = searchResults[i].getValue("email","CUSTENTITY_PROJECTMANAGER",null);
			var dmEmail = searchResults[i].getValue("email","CUSTENTITY_DELIVERYMANAGER",null);
			var clientEmail =  searchResults[i].getValue("email","CUSTENTITY_CLIENTPARTNER",null);
			var projectStatus = searchResults[i].getText("entitystatus");
			var projectType = searchResults[i].getText("jobtype");
			var allocationCategory = searchResults[i].getText("custentity_project_allocation_category");
			var practiceInternalId = searchResults[i].getValue("CUSTENTITY_PRACTICE");
			var practice = searchResults[i].getText("CUSTENTITY_PRACTICE");
            var parentPractice = searchResults[i].getText("custrecord_parent_practice","CUSTENTITY_PRACTICE",null);
			var parentPracticeId = searchResults[i].getValue("custrecord_parent_practice","CUSTENTITY_PRACTICE",null);
			var startDate = searchResults[i].getValue("startdate");
			var location = searchResults[i].getText("custentity_location");
            var endDate = searchResults[i].getValue("enddate");
            var jobType = searchResults[i].getText("jobbillingtype");
			var oppID = getOppId(projectId)
			if(oppID){
				oppID = oppID;
			}
			else{
				oppID = '';
			}
			nlapiLogExecution('DEBUG', 'oppID',oppID);

			project.projectInternalId = projectInternalId;
			project.projectId = projectId;
			project.projectName = projectName;
			project.accountInternalId = accountInternalId;
			project.pmEmail = pmEmail;
			project.dmEmail = dmEmail;
			project.clientEmail = clientEmail;
			project.projectStatus = projectStatus;
			project.projectType = projectType;
			project.allocationCategory = allocationCategory;
			project.practiceInternalId = practiceInternalId;
			project.practice = practice;
			project.parentPractice	= parentPractice;
			project.parentPracticeId = parentPracticeId;
			project.startDate = startDate;
			project.endDate = endDate;
			project.location = location;
			project.oppID = oppID;	
			project.jobType = jobType;	
			projectData.push(project);
			
		    }
		}
		

		response.timeStamp = currentDateAndTime;
		response.data = projectData;
		//response.data = dataRow;
		response.status = true;
	//	nlapiLogExecution('debug', 'response',JSON.stringify(response));
	} catch (err) {
		nlapiLogExecution('ERROR', 'postRESTlet', err);
		response.timeStamp = '';
		response.data = err;
		response.status = false;
	}
	return response;

}
//validate blank entries
function _logValidation(value) {
	if (value != null && value.toString() != null && value != '' && value != undefined && value.toString() != undefined && value != 'undefined' && value.toString() != 'undefined' && value.toString() != 'NaN' && value != NaN) {
		return true;
	} else {
		return false;
	}
}
//Get current date


//Returns timestamp
function timestamp() {
	var str = "";

	var currentTime = new Date();
	var hours = currentTime.getHours();
	var minutes = currentTime.getMinutes();
	var seconds = currentTime.getSeconds();
	var meridian = "";
	if (hours > 12) {
		meridian += "pm";
	} else {
		meridian += "am";
	}
	if (hours > 12) {

		hours = hours - 12;
	}
	if (minutes < 10) {
		minutes = "0" + minutes;
	}
	if (seconds < 10) {
		seconds = "0" + seconds;
	}
	//str += hours + ":" + minutes + ":" + seconds + " ";
	str += hours + ":" + minutes + " ";
	return str + meridian;
}

function Response() {
	this.status = "";
	this.timeStamp = "";
	this.data = "";

}

//Search function for results more than 1000
function searchRecord(recordType, savedSearch, arrFilters, arrColumns,
		filterExpression) {

	try {
		var search = null;

		// if a saved search is provided, load it and add the filters and
		// columns
		if (savedSearch) {
			search = nlapiLoadSearch(recordType, savedSearch);

			if (isArrayNotEmpty(arrFilters)) {
				search.addFilters(arrFilters);
			}

			if (isArrayNotEmpty(arrColumns)) {
				search.addColumns(arrColumns);
			}

			if (isArrayNotEmpty(filterExpression)) {
				search.setFilterExpression(filterExpression);
			}
		}
		// create a new search
		else {
			search = nlapiCreateSearch(recordType, arrFilters, arrColumns);
		}

		// run search
		var resultSet = search.runSearch();

		// iterate through the search and get all data 1000 at a time
		var searchResultCount = 0;
		var resultSlice = null;
		var searchResult = [];

		do {
			resultSlice = resultSet.getResults(searchResultCount,
					searchResultCount + 1000);

			if (resultSlice) {

				resultSlice.forEach(function(result) {

					searchResult.push(result);
					searchResultCount++;
				});
			}
		} while (isArrayNotEmpty(resultSlice) && resultSlice.length >= 1000);

		return searchResult;
	} catch (err) {
		nlapiLogExecution('ERROR', 'searchRecord', err);
		throw err;
	}
}



function isEmpty(value) {

	return value == null || value == "" || typeof(value) == undefined;
}

function isNotEmpty(value) {

	return !isEmpty(value);
}

function isArrayEmpty(argArray) {

	return !isArrayNotEmpty(argArray);
}

function isArrayNotEmpty(argArray) {

	return (isNotEmpty(argArray) && argArray.length > 0);
}
//function to format date object into NetSuite's mm/dd/yyyy format.
function formatNSDate(dateObj) {
	if (dateObj) {
		var nsFormatDate = dateObj.getFullYear() + '-' + addZeros(dateObj.getMonth() + 1, 2) + '-' + addZeros(dateObj.getDate(), 2);
		return nsFormatDate;
	}
	return null;
}

//function to add leading zeros on date parts.
function addZeros(num, len) {
	var str = num.toString();
	while (str.length < len) {
		str = '0' + str;
	}
	return str;
}
function getOppId(projectId){
	var searchResults = nlapiSearchRecord("customrecord_sfdc_opportunity_record",null,
[
   ["custrecord_project_sfdc","is",projectId]
], 
[
   new nlobjSearchColumn("custrecord_opportunity_id_sfdc")
]
);
if (searchResults) {	
	for(var i=0; i< searchResults.length; i++){    
		var oppId = searchResults[i].getValue("custrecord_opportunity_id_sfdc");
	}
}
	return oppId;
}