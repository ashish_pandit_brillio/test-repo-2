// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
    /*
           Script Name : UES_LOOKUP_SKILLS.js
        Author      : ASHISH PANDIT
        Date        : 25 OCT 2018
        Description : User Event to lookup internal ids  
    
    
        Script Modification Log:
    
        -- Date --			-- Modified By --				--Requested By--				-- Description --
     
    
    
    Below is a summary of the process controls enforced by this script file.  The control logic is described
    more fully, below, in the appropriate function headers and code blocks.
    
    
         SCHEDULED FUNCTION
            - scheduledFunction(type)
    
    
         SUB-FUNCTIONS
            - The following sub-functions are called by the above core functions in order to maintain code
                modularization:
    
                   - NOT USED
    
    */
    }
    // END SCRIPT DESCRIPTION BLOCK  ====================================
    
    
    // BEGIN SCHEDULED FUNCTION =============================================
    
    function afterSubmitLookup(type)
    {
          if(type == "delete"){
          return;
        }
        var i_rec_id = nlapiGetRecordId();
        var s_rec_type = nlapiGetRecordType();
        var recObj = nlapiLoadRecord(s_rec_type,i_rec_id);
        /*************Lookup Skills*********************/
        var s_skills = recObj.getFieldValue('custrecord_skills_sfdc');
        if(s_skills)
        {
            var a_skills = s_skills.split(",");
            var skillArray = new Array();
            for(var i=0;i<a_skills.length;i++)
            {
                var temp = a_skills[i];
                var customrecord_employee_skillsSearch = nlapiSearchRecord("customrecord_employee_skills",null,
                [
                   ["formulatext: {custrecord_emp_primary_skill}","is",temp]
                ], 
                [
                   new nlobjSearchColumn("custrecord_emp_primary_skill")
                ]
                );
                if(customrecord_employee_skillsSearch)
                {
                    skillArray.push(customrecord_employee_skillsSearch[0].getValue('custrecord_emp_primary_skill'));
                    
                }
            }
        }
        if(skillArray)
            recObj.setFieldValue('custrecord_skills_ids',skillArray.toString());
        /************* Lookup Project *********************/
        var s_projection_status = recObj.getFieldText('custrecord_projection_status_sfdc');
        var s_project = recObj.getFieldValue('custrecord_project_sfdc');
        nlapiLogExecution('Debug','s_project ',s_project);
        if(s_project)
        {
            var jobSearch = nlapiSearchRecord("job",null,
            [
               ["entityid","is",regExReplacer(s_project)]
            ], 
            [
               new nlobjSearchColumn("internalid")
            ]
            );
            if(jobSearch)
            {
                recObj.setFieldValue('custrecord_project_internal_id_sfdc',jobSearch[0].getValue('internalid'));
            }
        }
        /************* Lookup Customer *********************/
        var s_customer = recObj.getFieldValue('custrecord_customer_sfdc');
        nlapiLogExecution('Debug','s_customer ',s_customer);
        if(s_customer)
        {
            var customerSearch = nlapiSearchRecord("customer",null,
            [
               ["entityid","is",regExReplacer(s_customer)]
              // ["custentity_sfdc_account_id","is",s_customer]
            ], 
            [
               new nlobjSearchColumn("internalid"),
               new nlobjSearchColumn("stage")
            ]
            );
            if(customerSearch)
            {
                nlapiLogExecution('Debug','In customer Search');
                  recObj.setFieldValue('custrecord_customer_internal_id_sfdc',customerSearch[0].getValue('internalid'));
                  recObj.setFieldValue('custrecord_account_type_sfdc',customerSearch[0].getValue('stage'));
            }
            else
            {
                //recObj.setFieldValue('custrecord_account_name_sfdc',s_customer);
                recObj.setFieldValue('custrecord_account_type_sfdc','Prospect');
            }
        }
        
        /************* Lookup Region *********************/
        var s_region = recObj.getFieldValue('custrecord_opp_account_region');
        if(s_region)
        {
            var customrecord_regionSearch = nlapiSearchRecord("customrecord_region",null,
            [
               ["name","contains",regExReplacer(s_region)]
            ], 
            [
               new nlobjSearchColumn("internalid")
            ]
            );
            if(customrecord_regionSearch)
            {
                recObj.setFieldValue('custrecord_sfdc_opp_account_region',customrecord_regionSearch[0].getValue('internalid'));
            }
        }
        /************* Lookup Practice *********************/
        var s_practice = recObj.getFieldValue('custrecord_practice_sfdc');
        nlapiLogExecution('Debug','s_practice ',s_practice);
        if(s_practice)
        {
            var departmentSearch = nlapiSearchRecord("department",null,
            [
               ["name","is",regExReplacer(s_practice)]
            ], 
            [
               new nlobjSearchColumn("internalid")
            ]
            );
            if(departmentSearch)
            {
                recObj.setFieldValue('custrecord_practice_internal_id_sfdc',departmentSearch[0].getValue('internalid'));
            }
        }
        recObj.setFieldValue('custrecord_sales_act_sfd',s_projection_status);
        nlapiSubmitRecord(recObj);
    }
    
    // END AFTER SUBMIT ====================================================
    
    
    
    // BEGIN FUNCTION ===================================================
    
    function _logValidation(value) 
    {
     if(value != null && value!= 'null' && value != '' && value != undefined && value != undefined && value != 'undefined' && value.toString() != 'NaN' && value != NaN) 
     {
      return true;
     }
     else 
     { 
      return false;
     }
    }
    // END FUNCTION =====================================================
    function regExReplacer(str)
    {
        return str.replace(/\n|\r/g, "");
    }