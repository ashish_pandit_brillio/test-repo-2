//https://system.na1.netsuite.com/app/common/scripting/script.nl?id=368
/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       25 Dec 2014     amol.sahijwani
 *
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType
 * 
 * @param {String} type Operation types: create, edit, delete, xedit
 *                      approve, reject, cancel (SO, ER, Time Bill, PO & RMA only)
 *                      pack, ship (IF)
 *                      markcomplete (Call, Task)
 *                      reassign (Case)
 *                      editforecast (Opp, Estimate)
 * @returns {Void}
 */
function scheduled_update_expense(type){
	
	
	try{
	var current_date = nlapiDateToString(new Date());
		//Log for current date
	nlapiLogExecution('DEBUG', 'Execution Started Current Date', 'current_date...' + current_date);
	 var context = nlapiGetContext();
			// var invoice_id = context.getSetting('SCRIPT', 'custscript_ra_invoice_id');
	//if(!invoice_id)
//	return;
	var je_list = new Array();
		var filter = new Array();
		var a_results_je = searchRecord('transaction', 'customsearch2915', null, null); //customsearch1773
		if (_logValidation(a_results_je))
		{	
			for(var counter = 0; counter<a_results_je.length ; counter++)
			{
	if(je_list.indexOf(a_results_je[counter].getId())>=0)// && je_list.length > 50)
					{
						
					}
	else{					
				nlapiLogExecution('audit','j:-- ',counter);
				nlapiLogExecution('audit','a_results_je:-- ',a_results_je.length);
				var usageEnd = context.getRemainingUsage();
							if (usageEnd < 1000) 
							{
								yieldScript(context);
							}
	nlapiLogExecution('DEBUG','invoice_id',a_results_je[counter].getId());
	var record = nlapiLoadRecord('invoice',a_results_je[counter].getId());//332853
	var i_billable_time_count = record.getLineItemCount('time');	
	var update_status= record.getFieldValue('custbody_is_je_updated_for_emp_type');
	
	var customer = record.getFieldText('entity');
	var region_id_cust = nlapiLookupField('customer', record.getFieldValue('entity'),'custentity_region');
	
	var project = record.getFieldText('job');
	var proj_internal_id = record.getFieldValue('job');
	var proj_rcrd = nlapiLoadRecord('job',proj_internal_id);
	var billing_type = proj_rcrd.getFieldText('jobbillingtype');
	var region_id = proj_rcrd.getFieldValue('custentity_region');
	var project_region = proj_rcrd.getFieldText('custentity_region');
	var parent_practice = proj_rcrd.getFieldValue('custentity_practice');
	parent_practice = nlapiLookupField('department',parent_practice,'custrecord_parent_practice',true);
	if(!region_id)
		region_id = region_id_cust;
		
	var proj_name = proj_rcrd.getFieldValue('altname');
	var proj_category = proj_rcrd.getFieldText('custentity_project_allocation_category');
	var proj_category_val = proj_rcrd.getFieldValue('custentity_project_allocation_category');
	var billing_from_date = record.getFieldValue('custbody_billfrom');
	var billing_to_date = record.getFieldValue('custbody_billto');
	
	var territory	=	nlapiLookupField('job', record.getFieldValue('job'), 'customer.territory');
	
	//nlapiLogExecution('AUDIT', nlapiGetRecordId(), 'Project: ' + project + ', Customer: ' + customer);
	
	var a_employee_names = new Object();
	var a_resource_allocations = new Array();
	var emp_list = new Array();
	
	// Item Sublist
	var i_line_count = record.getLineItemCount('item')
				nlapiLogExecution('DEBUG', 'afterSubmit_cross_project', ' i_line_count-->' + i_line_count);
	if(_logValidation(i_line_count))
	{
		for(var i=1;i<=i_line_count;i++)
		{
						
			record.setLineItemValue('item', 'custcol_parent_executing_practice', i, parent_practice);
			record.setLineItemValue('item', 'custcol_project_region', i, project_region);		
						
																		
		}//Loop				
	}//Line Count
	
	
	
	var submitted_id = nlapiSubmitRecord(record, {disabletriggers : true, enablesourcing : true});
	nlapiLogExecution('DEBUG','submitted_id',submitted_id);
	je_list.push(submitted_id);
	yieldScript(context);
	}
}
}
			var timestp_E = timestamp();
			nlapiLogExecution('DEBUG', 'Execution Ended Current Date', 'timestp_E...' + timestp_E);
}

catch(e){
nlapiLogExecution('DEBUG','Process Error',e);
}
}
function _logValidation(value) 
{
 if(value!='null' && value != null && value != null && value != '' && value != undefined && value != undefined && value != 'undefined' && value!= 'undefined'&& value!= 'NaN' && value != NaN) 
 {
  return true;
 }
 else 
 { 
  return false;
 }
}

function timestamp() {
var str = "";

var currentTime = new Date();
var hours = currentTime.getHours();
var minutes = currentTime.getMinutes();
var seconds = currentTime.getSeconds();
var meridian = "";
if (hours > 12) {
    meridian += "pm";
} else {
    meridian += "am";
}
if (hours > 12) {

    hours = hours - 12;
}
if (minutes < 10) {
    minutes = "0" + minutes;
}
if (seconds < 10) {
    seconds = "0" + seconds;
}
str += hours + ":" + minutes + ":" + seconds + " ";

return str + meridian;
}
function yieldScript(currentContext) {

		nlapiLogExecution('AUDIT', 'API Limit Exceeded');
		var state = nlapiYieldScript();

		if (state.status == "FAILURE") {
			nlapiLogExecution('ERROR', 'Yield Script Failed', 'Reason : '
			        + state.reason + ' / Size : ' + state.size);
			return false;
		} else if (state.status == "RESUME") {
			nlapiLogExecution('AUDIT', 'Script Resumed');
		}
}