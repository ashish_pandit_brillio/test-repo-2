/**
 * Generate and attach the Work Order PDF to the Subtier record
 * 
 * Version Date Author Remarks
 * 
 * 1.00 18 May 2016 Nitish Mishra
 * 
 */

function userEventAfterSubmit(type) {
	try {
		var ccEmail = [ 'indiatalent.acq@brillio.com','srinivas.k@brillio.com','swetha.b@BRILLIO.COM','divya.manjunath@BRILLIO.COM','sandeep.bhuyan@brillio.com','deepashree.p@brillio.com' ];
		var bccEmail = [ 'govind.kharayat@brillio.com' ];

		nlapiLogExecution('debug', 'type', type);
		var subsidiary = nlapiGetFieldValue('custrecord_stvd_subsidiary');
		var isExtension = nlapiGetFieldValue('custrecord_stvd_is_extension');

		if ((type == 'edit'  && subsidiary == 3 && isExtension == 'T')) {
			var woNumber = nlapiGetFieldValue('custrecord_stvd_wo_number');

			if (woNumber) {
				// generate Work Order
				var xmlContent = getWorkOrderTemplate(nlapiGetRecordId());
				var pdfFile = nlapiXMLToPDF(xmlContent);

				// Save the file in file cabinet
				pdfFile.setFolder(constant.Folder.WorkOrderExtension);
				pdfFile.setName('WO#' + woNumber + ".pdf");
				var fileId = nlapiSubmitFile(pdfFile);
				nlapiLogExecution('DEBUG', 'emailfile',fileId);
				// set the file id in the current subtier record
				nlapiSubmitField(nlapiGetRecordType(), nlapiGetRecordId(),
				        'custrecord_stvd_wo_file', fileId);

				var employeeDetails = nlapiLookupField('employee',
				        nlapiGetFieldValue('custrecord_stvd_contractor'), [
				                'firstname', 'lastname' ]);

				var vendorDetails = nlapiLookupField('vendor',
				        nlapiGetFieldValue('custrecord_stvd_vendor'), [
				                'altname', 'email', 'internalid' ]);

				// get all the vendor contacts to send an email
				var contactSearch = nlapiSearchRecord('contact', null, [
				        new nlobjSearchFilter('company', null, 'anyof',
				                vendorDetails.internalid),
				        new nlobjSearchFilter('isinactive', null, 'is', 'F') ],
				        [ new nlobjSearchColumn('firstname') ]);

				if (contactSearch) {

					// send an email to the vendor contact
					for (var i = 0; i < contactSearch.length; i++) {
						var woEmailContent = getWOEmailTemplate(woNumber,
						        contactSearch[i].getValue('firstname'),
						        employeeDetails.firstname + " "
						                + employeeDetails.lastname);

						nlapiSendEmail(constant.Mail_Author.TalentAcquisition,
						        contactSearch[i].getId(),
						        woEmailContent.Subject, woEmailContent.Body,
						        ccEmail,bccEmail, {
						            'record' : nlapiGetRecordId(),
						            'recordtype' : nlapiGetRecordType()
						        }, pdfFile);
					}

					nlapiLogExecution('DEBUG', 'Email send to contacts');
				} else {
					// no contacts found, send email directly to the vendor
					// record
					var woEmailContent = getWOEmailTemplate(woNumber, '',
					        employeeDetails.firstname + " "
					                + employeeDetails.lastname);

					nlapiSendEmail(constant.Mail_Author.TalentAcquisition,
					        vendorDetails.internalid, woEmailContent.Subject,
					        woEmailContent.Body, ccEmail,bccEmail, {
					            'record' : nlapiGetRecordId(),
					            'recordtype' : nlapiGetRecordType()
					        }, pdfFile);

					nlapiLogExecution('DEBUG', 'Email send to vendor');
				}
			}
		}
	} catch (err) {
		nlapiLogExecution('ERROR', 'userEventAfterSubmit : '
		        + nlapiGetRecordId(), err);
		throw err;
	}
}

function getWOEmailTemplate(woNumber, vendorName, employeeName) {
	var htmltext = '<p>Hi ' + vendorName + ',</p>';

	htmltext += "<p>Please find the attached Extended Work Order for "
	        + employeeName + ".</p>";
	htmltext += "<p>Please review the same and share back with the signed copy.</p>";

	htmltext += '<p>Regards,<br/>';
	htmltext += 'Brillio Team </p>';

	return {
	    Subject : 'Work Order Extension #' + woNumber + " " + employeeName,
	    Body : addMailTemplate(htmltext)
	};
}