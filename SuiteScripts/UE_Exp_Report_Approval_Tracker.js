/**
 * Store the date of change in Status
 * 
 * Version Date Author Remarks 1.00 26 Feb 2016 Nitish Mishra
 * 
 */

function userEventBeforeSubmit(type) {
	nlapiLogExecution('debug', 'userEventBeforeSubmit type', type);
	trackDates();
}

function userEventAfterSubmit(type) {
	nlapiLogExecution('debug', 'userEventAfterSubmit type', type);
	trackDates();
}

function trackDates() {

	try {

		var oldRecord = nlapiGetOldRecord();
		var newStatus = nlapiLookupField('expensereport', nlapiGetRecordId(),
		        'statusRef');

		nlapiLogExecution('debug', 'old status', oldRecord
		        .getFieldValue('statusRef'));
		nlapiLogExecution('debug', 'new status', newStatus);

		var approverField = "";
		var approvalDateField = "";

		// check if the status has changed
		if (newStatus != oldRecord.getFieldValue('statusRef')) {

			switch (newStatus) {

				case "inProgress":
				break;

				case "pendingSupApproval":
				break;

				case "pendingAcctApproval":
					approverField = "custbody_exp_first_approver";
					approvalDateField = "custbody_exp_first_approval_date";
				break;

				case "rejectedBySup":
				break;

				case "approvedByAcct":
					approverField = "custbody_exp_accounting_approver";
					approvalDateField = "custbody_exp_accounts_approval_date";
				break;

				case "rejectedByAcct":
				break;

				case "rejectedByAcctOverride":
					approverField = "custbody_exp_accounting_approver";
					approvalDateField = "custbody_exp_accounts_approval_date";
				break;

				case "paidInFull":
					approverField = "custbody_exp_paid_in_full_emp";
					approvalDateField = "custbody_exp_paid_in_full_date";
				break;
			}

			if (approverField && approvalDateField) {
				var currentDate = nlapiDateToString(new Date(), 'datetimetz');
				nlapiSubmitField('expensereport', nlapiGetRecordId(), [
				        approverField, approvalDateField ], [ nlapiGetUser(),
				        currentDate ]);
				nlapiLogExecution('DEBUG', 'Approval Details Captured');
			}
		}
	} catch (err) {
		nlapiLogExecution('ERROR', 'sendMailsForReject', err);
	}
}