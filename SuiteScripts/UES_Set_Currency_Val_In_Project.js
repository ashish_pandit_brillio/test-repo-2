/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       26 Nov 2014     Swati
 *
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType
 * 
 * @param {String} type Operation types: create, edit, delete, xedit,
 *                      approve, cancel, reject (SO, ER, Time Bill, PO & RMA only)
 *                      pack, ship (IF only)
 *                      dropship, specialorder, orderitems (PO only) 
 *                      paybills (vendor payments)
 * @returns {Void}
 */
function userEventAfterSubmit_set_currency(type)
{//fun start
  
	var pro_type=nlapiGetRecordType();
 	nlapiLogExecution('Debug','pro_type============>',pro_type);
	var pro_id=nlapiGetRecordId();
 	nlapiLogExecution('Debug','pro_id============>',pro_id);
	
	var load_project=nlapiLoadRecord(pro_type,pro_id);
	if(load_project != null)
		{//if start
		
		 var i_currency=load_project.getFieldValue('currency');
		 nlapiLogExecution('Debug','i_currency============>',i_currency);		
		 load_project.setFieldValue('custentity_project_currency',i_currency);
		}//if close
	nlapiSubmitRecord(load_project);
}//fun close
//---------------------------------------------------------------------
function Schedule_update_currency()
{//fun start
    
       var search_project=nlapiSearchRecord('job',null,null,null);
       if(search_project != null)
	{//if start
		
		for(var kk=0 ; kk<search_project.length ; kk++)
		{//for start
                         var pro_id=search_project[kk].getId();
 			nlapiLogExecution('Debug','pro_id============>',pro_id);
                      
			 var load_project=nlapiLoadRecord('job',pro_id);
			if(load_project != null)
			{//if start
		
		 		var i_currency=load_project.getFieldValue('currency');
				 nlapiLogExecution('Debug','i_currency============>',i_currency);		
				 //load_project.setFieldValue('custentity_project_currency',i_currency);
 				 nlapiSubmitField('job',pro_id,'custentity_project_currency',i_currency);
			}//if close
			//nlapiSubmitRecord(load_project);

		}//for close
	}//if close
}//function
