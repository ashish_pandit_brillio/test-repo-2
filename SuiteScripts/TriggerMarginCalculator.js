/**
 *@NApiVersion 2.x
 *@NScriptType Suitelet
 */

// this creates a Suitelet form that lets you write and send an email

define(['N/ui/serverWidget', 'N/email', 'N/runtime','N/task'],
    function(ui, email, runtime,task) {
        function onRequest(context) {
            if (context.request.method === 'GET') {
                var form = ui.createForm({
                    title: 'Get P&L Form'
                });
                form.addSubmitButton({
                    label: 'P&L Report'
                });
                context.response.writePage(form);
            } else {
                var currentUser = runtime.getCurrentUser();
                var email = currentUser.email;
                var userId = currentUser.id;
                var html = "<h2 style=color: #5e9ca0;>Please check email for report!</h2></br><h2>Note : It take's time to process - please wait for some time.</h2>";
                var form = ui.createForm({
                    title: 'Get P&L Form'
                });
				var labelHTML = form.addField({
					id : "custpage_label",
					label : "message",
					type : "INLINEHTML"
				})
				labelHTML.defaultValue = html;
				context.response.writePage(form);
                var scheduleScriptTaskObj = task.create({
                    taskType: task.TaskType.SCHEDULED_SCRIPT,
                    scriptId: 1902,
                    params: {"custscript_login_email": email,
                    	"custscript1902_userid" : userId }
                });
                var mrTaskId = scheduleScriptTaskObj.submit();

                // Check the status of the task, and send an email if the
                // task has a status of FAILED.
                //
                // Update the authorId value with the internal ID of the user
                // who is the email sender. Update the recipientEmail value
                // with the email address of the recipient.
                var taskStatus = task.checkStatus(mrTaskId);
                log.debug("email : ",JSON.stringify(scheduleScriptTaskObj));
                log.debug("taskStatus : ",taskStatus);
                if(taskStatus == "FAILED"){
                	// send email to user 
                email.send({
				author: 442,
				recipients: email,
				subject: 'Margin Report',
				body: 'Hi'+"\n"+"margin report fails to generate - please contact Netsuite team for queries"+"\n"+"Thanks & Regards"+"\n"+"Information System"
				});
                }
            }
        }
        return {
            onRequest: onRequest
        };
    });