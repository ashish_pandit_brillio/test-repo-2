// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
    /*
     Script Name:	UES_auto_increament_ID
     Author:			Vikrant S. Adhav
     Company:		Aashna CloudTech
     Date:			19-03-2014
     Description:	This script is used to imply auto increament functionaity over Customer ID field.
     Script Modification Log:
     -- Date --			-- Modified By --				--Requested By--				-- Description --
     
     
     Below is a summary of the process controls enforced by this script file.  The control logic is described
     more fully, below, in the appropriate function headers and code blocks.
     
     BEFORE LOAD
     - beforeLoadRecord(type)
     BEFORE SUBMIT
     - beforeSubmitRecord(type)
     AFTER SUBMIT
     - afterSubmitRecord(type)
     SUB-FUNCTIONS
     - The following sub-functions are called by the above core functions in order to maintain code
     modularization:
     - NOT USED
     */
}
// END SCRIPT DESCRIPTION BLOCK  ====================================



// BEGIN GLOBAL VARIABLE BLOCK  =====================================
{
    //  Initialize any Global Variables, in particular, debugging variables...




}
// END GLOBAL VARIABLE BLOCK  =======================================





// BEGIN BEFORE LOAD ==================================================

function beforeLoadRecord(type) //
{

    /*  On before load:
     - EXPLAIN THE PURPOSE OF THIS FUNCTION
     -
     FIELDS USED:
     --Field Name--				--ID--
     */
    //  LOCAL VARIABLES
    
    //  BEFORE LOAD CODE BODY
    
    
    return true;
    
    
    
}

// END BEFORE LOAD ====================================================





// BEGIN BEFORE SUBMIT ================================================

function beforeSubmitRecord(type) //
{
    /*  On before submit:
     - PURPOSE
     -
     FIELDS USED:
     --Field Name--				--ID--
     */
    //  LOCAL VARIABLES
    
    
    //  BEFORE SUBMIT CODE BODY
    
    
    return true;
    
}

// END BEFORE SUBMIT ==================================================





// BEGIN AFTER SUBMIT =============================================

function afterSubmit_GenerateID(type) //
{
    /*  On after submit:
     - PURPOSE
     -
     FIELDS USED:
     --Field Name--				--ID--
     */
    //  LOCAL VARIABLES
    try//
    {
        if (type == 'create') //
        {
            nlapiLogExecution('DEBUG', 'afterSubmit_GenerateID', '*************** Execution Started ***************');
            
            var rec_ID = nlapiGetRecordId();
            nlapiLogExecution('DEBUG', 'afterSubmit_GenerateID', 'rec_ID : ' + rec_ID);
            
            var customer_rec = nlapiLoadRecord('customer', rec_ID); // Load customer record
            var customer_prefix = nlapiGetFieldValue('custentity_customeridprefix').toUpperCase();
            nlapiLogExecution('DEBUG', 'afterSubmit_GenerateID', 'customer_prefix : ' + customer_prefix.toUpperCase());
            
            var s_customer_ID = nlapiGetFieldValue('entityid');
            nlapiLogExecution('DEBUG', 'afterSubmit_GenerateID', 's_customer_ID : ' + s_customer_ID);
            
            var s_parent_ID = nlapiGetFieldValue('parent');
            nlapiLogExecution('DEBUG', 'afterSubmit_GenerateID', 's_parent_ID : ' + s_parent_ID);
            
            if (s_parent_ID != null && s_parent_ID != undefined && s_parent_ID != '') // if parent ID has value
            {
                nlapiLogExecution('DEBUG', 'afterSubmit_GenerateID', 'when parent field has value...');
                
                var parent_cust_rec = nlapiLoadRecord('customer', s_parent_ID);
                var s_parent_ID_T = parent_cust_rec.getFieldValue('entityid');
                nlapiLogExecution('DEBUG', 'afterSubmit_GenerateID', 's_parent_ID_T : ' + s_parent_ID_T);
                
                //s_parent_ID_T = s_parent_ID_T.slice(0, s_parent_ID_T.indexOf('-'));
                nlapiLogExecution('DEBUG', 'afterSubmit_GenerateID', 's_parent_ID_T after : ' + s_parent_ID_T);
                
                var filter = new Array();
                var column = new Array();
                var current_entity_ID = '';
                var alphabet = '';
                
                filter[0] = new nlobjSearchFilter('parent', null, 'is', s_parent_ID);
                
                column[0] = new nlobjSearchColumn('entityid');
                column[1] = new nlobjSearchColumn('internalid');
                column[0].setSort();
                
                var search_result = nlapiSearchRecord('customer', null, filter, column);
                nlapiLogExecution('DEBUG', 'afterSubmit_GenerateID', 'search_result : ' + search_result);
                
                if (search_result != null && search_result != undefined && search_result.length > 2) // if search result has records
                {
                    nlapiLogExecution('DEBUG', 'afterSubmit_GenerateID', 'search_result.length : ' + search_result.length);
                    
                    for (var i = 0; i < search_result.length - 1; i++)//
                    {
                        nlapiLogExecution('DEBUG', 'afterSubmit_GenerateID', 'entity ID : ' + search_result[i].getValue('entityid'));
                        nlapiLogExecution('DEBUG', 'afterSubmit_GenerateID', 'internal ID : ' + search_result[i].getValue('internalid'));
                    }
                    
                    current_entity_ID = search_result[search_result.length - 2].getValue('entityid');
                    nlapiLogExecution('DEBUG', 'afterSubmit_GenerateID', 'current_entity_ID : ' + current_entity_ID);
                    
                    if (current_entity_ID != null && current_entity_ID != undefined && current_entity_ID != '') //
                    {
                        var parent_cust = nlapiLoadRecord('customer', search_result[search_result.length - 2].getValue('internalid'));
                        
                        var s_parent_ID_text = parent_cust.getFieldValue('entityid');
                        nlapiLogExecution('DEBUG', 'afterSubmit_GenerateID', 's_parent_ID_text : ' + s_parent_ID_text);
                        nlapiLogExecution('DEBUG', 'afterSubmit_GenerateID', 'search_result : ' + search_result);
                        var arr = new Array();
                        arr = s_parent_ID_text.split('-');
                        parent_ID_text = arr[0];
                        alphabet = arr[1];
                        
                        nlapiLogExecution('DEBUG', 'afterSubmit_GenerateID', 'before alphabet : ' + alphabet);
                        //alphabet = parseInt(alphabet) + 1; //INCREAMENTED ALPHABET
                        alphabet = String.fromCharCode(alphabet.charCodeAt() + 1) //INCREAMENTED ALPHABET
                        nlapiLogExecution('DEBUG', 'afterSubmit_GenerateID', 'after alphabet : ' + alphabet);
                        
                        customer_rec.setFieldValue('entityid', s_parent_ID_T + '-' + alphabet); // set new entity ID
                        nlapiSubmitRecord(customer_rec, true);
                        nlapiLogExecution('DEBUG', 'afterSubmit_GenerateID', '*************** Execution Completed ***************');
                    }
                    else // 
                    {
                    
                    }
                }
                else // if records not found
                {
                    nlapiLogExecution('DEBUG', 'afterSubmit_GenerateID', 'While first child record !!!');
                    
                    var cust_rec = nlapiLoadRecord('customer', s_parent_ID);
                    var cust_ID = cust_rec.getFieldValue('entityid');
                    nlapiLogExecution('DEBUG', 'afterSubmit_GenerateID', 'cust_ID : ' + cust_ID);
                    var new_customer_ID = cust_ID + '-a';
                    nlapiLogExecution('DEBUG', 'afterSubmit_GenerateID', 'new_customer_ID : ' + new_customer_ID);
                    customer_rec.setFieldValue('entityid', new_customer_ID);
                    
                    nlapiSubmitRecord(customer_rec, true);
                    nlapiLogExecution('DEBUG', 'afterSubmit_GenerateID', '*************** Execution Completed ***************');
                }
            }
            else // if parent ID is blank
            {
                nlapiLogExecution('DEBUG', 'afterSubmit_GenerateID', 'when parent field is blank...');
                
                var columns = new Array();
                columns[0] = new nlobjSearchColumn('internalid');
                
                // search for custom record and take first record value
                var a_search_results = nlapiSearchRecord('customrecord_serial_number_generation', null, null, columns);
                var i_internalID = '';
                if (a_search_results != null && a_search_results != '' && a_search_results != undefined) //
                {
                    i_internalID = a_search_results[0].getValue('internalid');
                    
                    nlapiLogExecution('DEBUG', 'afterSubmit_GenerateID', 'i_internalID : ' + i_internalID);
                    
                    var r_custom_rec = nlapiLoadRecord('customrecord_serial_number_generation', i_internalID);
                    nlapiLogExecution('DEBUG', 'afterSubmit_GenerateID', 'r_custom_rec : ' + r_custom_rec);
                    
                    if (r_custom_rec != null && r_custom_rec != undefined)//
                    {
                        // Get values of custom record
                        var i_current_Number = (r_custom_rec.getFieldValue('custrecord_current_sequence_number'));
                        var s_current_ID = r_custom_rec.getFieldValue('custrecord_current_serial_number');
                        
                        nlapiLogExecution('DEBUG', 'afterSubmit_GenerateID', 'i_current_Number : ' + i_current_Number);
                        nlapiLogExecution('DEBUG', 'afterSubmit_GenerateID', 's_current_ID : ' + s_current_ID);
                        
                        var i_new_cust_number = parseInt(i_current_Number) + 1; // increment current customer id by 1
                        i_current_Number = i_new_cust_number;
                        if (i_new_cust_number <= 9) //	Add 0 if numbers are between 0 to 9
                        {
                            i_new_cust_number = '0' + i_new_cust_number.toString();
                        }
                        
                        nlapiLogExecution('DEBUG', 'afterSubmit_GenerateID', 'i_new_cust_number : ' + i_new_cust_number);
                        
                        nlapiLogExecution('DEBUG', 'afterSubmit_GenerateID', 'New Customer ID : ' + customer_prefix + i_new_cust_number);
                        
                        customer_rec.setFieldValue('entityid', customer_prefix + i_new_cust_number); // set new customer ID
                        //nlapiDisableField('custentity_customeridprefix', true); // make prefix field disable
                        
                        r_custom_rec.setFieldValue('custrecord_current_serial_number', customer_prefix + i_new_cust_number);
                        r_custom_rec.setFieldValue('custrecord_current_sequence_number', i_current_Number);
                        
                        nlapiSubmitRecord(r_custom_rec, true);
                        nlapiSubmitRecord(customer_rec, true);
                        nlapiLogExecution('DEBUG', 'afterSubmit_GenerateID', '*************** Execution Completed ***************');
                    }
                }
                else //
                {
                    nlapiLogExecution('DEBUG', 'afterSubmit_GenerateID', 'Search result is NULL !!');
                    nlapiLogExecution('DEBUG', 'afterSubmit_GenerateID', '*************** Execution Completed ***************');
                }
            }
        }
    } 
    catch (e)//
    {
        nlapiLogExecution('DEBUG', 'afterSubmit_GenerateID', 'Error : ' + e);
        nlapiLogExecution('DEBUG', 'afterSubmit_GenerateID', '*************** Execution Completed ***************');
    }
    
    //  AFTER SUBMIT CODE BODY
    
    return true;
    
}

// END AFTER SUBMIT ===============================================





// BEGIN FUNCTION ===================================================
{



}
// END FUNCTION =====================================================
