/**
 * @author Jayesh
 */

function not_submitted_hours(request, response)
{
	try
	{
		var o_form_obj = nlapiCreateForm('Not Submitted Time Entry');
		if(request.getMethod() == "GET")
		{
			o_form_obj.addFieldGroup('flg_grp_filters', 'Filters');
			o_form_obj.addField('start_date', 'date', 'Start Date', null,'flg_grp_filters').setMandatory(true);
			o_form_obj.addField('end_date', 'date', 'End Date', null,'flg_grp_filters').setMandatory(true);
			o_form_obj.addField('subsidiary', 'select', 'Subsidiary', 'subsidiary','flg_grp_filters').setMandatory(true);
			o_form_obj.addField('proj_type', 'select', 'Project Type', 'jobtype','flg_grp_filters').setMandatory(true);
			var fld_pro_billing_type = o_form_obj.addField('bill_type', 'select', 'Billing Type', null,'flg_grp_filters').setMandatory(true);
			fld_pro_billing_type.addSelectOption('','');
			fld_pro_billing_type.addSelectOption('TM','Time and Material');
			fld_pro_billing_type.addSelectOption('FBI','Fixed Bid, Interval');
			fld_pro_billing_type.addSelectOption('FBM','Fixed Bid, Milestone');

			var fld_emp_type = o_form_obj.addField('employee_type', 'select', 'Employee Type', null,'flg_grp_filters').setMandatory(true);
			fld_emp_type.addSelectOption('','');
			fld_emp_type.addSelectOption(1,'Hourly');
			fld_emp_type.addSelectOption(3,'Salaried');
			
			var fld_view_type = o_form_obj.addField('view_type', 'select', 'View Type', null,'flg_grp_filters').setMandatory(true);
			fld_view_type.addSelectOption('','');
			fld_view_type.addSelectOption(1,'Weekly');
			fld_view_type.addSelectOption(2,'Daily');
			
			var fld_emp_category = o_form_obj.addField('emp_category', 'select', 'Employee Category', null,'flg_grp_filters').setMandatory(true);
			fld_emp_category.addSelectOption('','');
			fld_emp_category.addSelectOption(1,'Contingent Worker');
			fld_emp_category.addSelectOption(2,'Employee ');
			
			o_form_obj.addSubmitButton('Generate Report');
			
			response.writePage(o_form_obj);
		}
		else
		{
			var d_startDate = request.getParameter('start_date');
			var d_endDate = request.getParameter('end_date');
			var i_subsidiary = request.getParameter('subsidiary');
			var i_pro_type = request.getParameter('proj_type');
			var i_emp_type = request.getParameter('employee_type');
			var s_pro_bill_type = request.getParameter('bill_type');
			var i_view_type = request.getParameter('view_type');
			var i_emp_category = request.getParameter('emp_category');
			
			var d_todays_date = nlapiStringToDate(nlapiDateToString(new Date()));
			var a_filters_allocation = [ ['startdate', 'onorbefore', d_todays_date ], 'and',
										['enddate', 'onorafter', d_todays_date ], 'and',
										['employee.custentity_employee_inactive', 'is', 'F'], 'and',
										['employee.subsidiary', 'is', parseInt(i_subsidiary)], 'and',
										['employee.employeetype', 'is', parseInt(i_emp_type)], 'and',
										['employee.custentity_persontype', 'is', parseInt(i_emp_category)], 'and',
										['job.jobtype', 'anyof', parseInt(i_pro_type)], 'and',
										['job.jobbillingtype', 'anyof', s_pro_bill_type]
									  ];
				
			//['resource', 'anyof', [2965,2966]], 'and',
						
			var a_columns = new Array();
			a_columns[0] = new nlobjSearchColumn('resource',null,'group');
			
			var a_project_allocation_result = searchRecord('resourceallocation', null, a_filters_allocation, a_columns);
			if (a_project_allocation_result)
			{
				var s_monthTimesheetTable = '';
				var a_active_emp_list = new Array();
				var a_emp_list = new Array();
				
				nlapiLogExecution('audit','resource count length:- '+a_project_allocation_result.length);
				
				for(var i_allocation_index=0; i_allocation_index<a_project_allocation_result.length; i_allocation_index++)
				{
					a_active_emp_list.push({
						Emp_id: a_project_allocation_result[i_allocation_index].getValue('resource',null,'group'),
						Emp_name: a_project_allocation_result[i_allocation_index].getText('resource',null,'group')
						});
						
					a_emp_list.push(a_project_allocation_result[i_allocation_index].getValue('resource',null,'group'));
				}
				
				s_monthTimesheetTable = createMonthTimesheetTable(a_emp_list, a_active_emp_list, d_startDate, d_endDate, i_view_type);
				
				o_form_obj.addField('html_content', 'inlinehtml', '').setDefaultValue(s_monthTimesheetTable);
				
				o_form_obj.setScript('customscript_cli_not_submitted_xportdata');
				o_form_obj.addButton('custpage_xport', 'Export Data', 'Xport_Data(\'  ' + d_startDate + '  \',\'  ' + d_endDate + '  \',\'  ' + i_subsidiary + '  \',\'  ' + i_pro_type + '  \',\'  ' + i_emp_type + '  \',\'  ' + s_pro_bill_type + '  \',\'  ' + i_view_type + '  \',\'  ' + i_emp_category + '  \');');
				
				response.writePage(o_form_obj);
			}
		}
	}
	catch(err)
	{
		nlapiLogExecution('ERROR','not_submitted_hours','ERROR MESSAGE:- '+err);
	}
}

function createMonthTimesheetTable(a_emp_list, a_active_emp_list, startDate, endDate, i_view_type) {
	try {
		// create a blank month table
		var table = {};
					
		var d_startDate = nlapiStringToDate(startDate);
		var d_endDate = nlapiStringToDate(endDate);
		var end_week = d_endDate.getWeekNumber();
		
		var i_days_diff = getDatediffIndays(startDate,endDate);
		var i_total_weeks = 0;
		
		if(i_view_type == 2)
		{
			for(var i_emp_index=0; i_emp_index<a_active_emp_list.length; i_emp_index++)
			{
				for (var i = 0; i<i_days_diff; i++)
				{
					var currentDate = nlapiAddDays(d_startDate, i);
					var s_currentDate = nlapiDateToString(currentDate, 'date');
					
					var i_unique_key = a_active_emp_list[i_emp_index].Emp_id + '_' + currentDate.getDate() + '_' + currentDate.getMonth() + '_' + currentDate.getFullYear();
					
					//var weekOfYear = currentDate.getWeekNumber();
					
					//nlapiLogExecution('audit','week entry:- '+weekOfYear+' ::strt date:-'+d_startDate,currentDate);
					
					if (currentDate > d_endDate) {
						break;
					}
					
					//if (!table[i_unique_key])
					{
						table[i_unique_key] = {
							EmpName: a_active_emp_list[i_emp_index].Emp_name,
						    Week : 'No',
						    Name : currentDate.getWeekName(2)
						};
					}
				}
			}
		}
		else
		{
			for(var i_emp_index=0; i_emp_index<a_active_emp_list.length; i_emp_index++)
			{
				for (var i = 0; i<i_days_diff; i++)
				{
					var currentDate = nlapiAddDays(d_startDate, i);
					var s_currentDate = nlapiDateToString(currentDate, 'date');
					var weekOfYear = currentDate.getWeekNumber();
					
					var i_unique_key = a_active_emp_list[i_emp_index].Emp_id + '_' + weekOfYear;
					
					//nlapiLogExecution('audit','week entry:- '+weekOfYear+' ::strt date:-'+d_startDate,currentDate);
					
					if (weekOfYear > end_week) {
						break;
					}
					
					if (!table[i_unique_key])
					{
						table[i_unique_key] = {
							EmpName: a_active_emp_list[i_emp_index].Emp_name,
						    Week : 'No',
						    Name : currentDate.getWeekName(1)
						};
						
						if(i_emp_index == 0)
						{
							i_total_weeks++;
						}
					}
				}
			}
		}
		
		// get all timesheets within the period
		var a_filters_time = new Array();
		a_filters_time[0] = new nlobjSearchFilter('employee', null, 'anyof', a_emp_list);
		a_filters_time[1] = new nlobjSearchFilter('type', null, 'anyof', 'A');
		a_filters_time[2] = new nlobjSearchFilter('date', null, 'within', startDate,endDate);
					
		var a_columns_time = new Array();
		a_columns_time[0] = new nlobjSearchColumn('date',null,'group');
		a_columns_time[1] = new nlobjSearchColumn('employee',null,'group');
		a_columns_time[2] = new nlobjSearchColumn('durationdecimal',null,'sum');
		a_columns_time[3] = new nlobjSearchColumn('totalhours','timesheet','max');
				
		var timeEntrySearch = searchRecord('timebill', null, a_filters_time, a_columns_time);//replaced searched record type as timebill on 10-04-2020

		if (timeEntrySearch) {
			
			nlapiLogExecution('audit','time entry len:- '+timeEntrySearch.length);
			timeEntrySearch.forEach(function(timeEntry) {
				
				var timeEntryDate = nlapiStringToDate(timeEntry.getValue(
				        'date', null, 'group'));
				var s_currentDate = nlapiDateToString(currentDate, 'date');
				var d_currentDate = new Date(s_currentDate);
				var weekOfYear = timeEntryDate.getWeekNumber();
				var dayOfWeek = timeEntryDate.getDay();
				
				var i_employee = timeEntry.getValue('employee', null, 'group');
				var f_duration = timeEntry.getValue('durationdecimal', null, 'sum');
				var f_timesheet_hours = timeEntry.getValue('totalhours','timesheet','max');
				
				//nlapiLogExecution('audit','week of year:- '+weekOfYear,'timeEntryDate:- '+timeEntryDate+'hours:- '+f_timesheet_hours);
				
				if(i_view_type == 2)
				{
					var i_unique_key = i_employee + '_' + timeEntryDate.getDate() + '_' + timeEntryDate.getMonth() + '_' + timeEntryDate.getFullYear();
					table[i_unique_key].Week = f_duration;
				}
				else
				{
					if(timeEntryDate.getDay() != 0)
					{
						var i_unique_key = i_employee + '_' + weekOfYear;
						table[i_unique_key].Week = f_timesheet_hours;
					}
				}
				
			});
		}

		var css = "<style>"
		        + ".month-timesheet-table { width: 600px !important; border : 1px solid black; background: #f2f2f2;} "
		        + ".month-timesheet-table td { border : 1px solid black; }"
		        + "</style>";

		var html = css;
		html += "<br/><table class='month-timesheet-table'>";
		
		html += "<tr style='background:#0066ff;color:white;font-weight:bold;'>";
		html += "<td>Employee</td>";
		
		var temp_counter = 1;
		for ( var week in table) {
			
			if(i_view_type == 2)
			{
				if(temp_counter <= i_days_diff)
				{
					temp_counter++;
					html += "<td style='background:#668cff;color:white;'>"
				        + table[week].Name + "</td>";
				}
			}
			else
			{
				if(temp_counter <= i_total_weeks)
				{
					html += "<td style='background:#668cff;color:white;'>"
				        + table[week].Name + "</td>";
						
					temp_counter++;
				}
			}
		}
		html += "</tr>";
		
		var i_previous_emp = 0;
		
		for (var week in table)
		{
			var i_emp_id = week.toString().split('_');
			i_emp_id = i_emp_id[0];
			
			if (i_previous_emp == 0)
			{
				i_previous_emp = i_emp_id;
				html += "<tr>";
				html += "<td>"+table[week].EmpName+"</td>";
			}
			
			if (i_previous_emp != i_emp_id)
			{
				html += "</tr>";
				html += "<tr>";
				html += "<td>"+table[week].EmpName+"</td>";
				i_previous_emp = i_emp_id;
			}
			
			if(table[week].Week == 'No')
			{
				html += "<td>N-0</td>";
			}
			else
			{
				html += "<td>" + table[week].Week + "</td>";
			}
		}
		html += "</tr>";
		 
		html += "</table><br/>";
		return html;
	} catch (err) {
		nlapiLogExecution('ERROR', 'createMonthTimesheetTable', err);
		throw err;
	}
}

function getDatediffIndays(startDate, endDate) {
	var one_day = 1000 * 60 * 60 * 24;
	var fromDate = startDate;
	var toDate = endDate;
	var date1 = nlapiStringToDate(fromDate);
	var date2 = nlapiStringToDate(toDate);
	var date3 = Math.round((date2 - date1) / one_day);
	return (date3 + 1);
}

Date.prototype.getWeekName = function(view) {
	
	var d = new Date(+this);
	var days = d.getDay();
	
	var startDate = nlapiAddDays(d, -days);
	var endDate = nlapiAddDays(startDate, 6);
	
	if(view == 2)
	{
		return nlapiDateToString(d, 'date');
	}
	else
	{
		return nlapiDateToString(startDate, 'date') + " - "
	        + nlapiDateToString(endDate, 'date');
	}
};

Date.prototype.getWeekNumber = function() {
	var d = new Date(+this);
	d.setHours(0, 0, 0);
	d.setDate(d.getDate() + 4 - (d.getDay() || 7));
	return Math
	        .ceil((((d - new Date(d.getFullYear(), 0, 1)) / 8.64e7) + 1) / 7);
};

// search code 
function searchRecord(recordType, savedSearch, arrFilters, arrColumns,  filterExpression){

	try {
		var search = null;

		// if a saved search is provided, load it and add the filters and
		// columns
		if (isNotEmpty(savedSearch)) {
			search = nlapiLoadSearch(recordType, savedSearch);

			if (isArrayNotEmpty(arrFilters)) {
				search.addFilters(arrFilters);
			}

			if (isArrayNotEmpty(arrColumns)) {
				search.addColumns(arrColumns);
			}

			if (isArrayNotEmpty(filterExpression)) {
				search.setFilterExpression(filterExpression);
			}
		}
		// create a new search
		else {
			search = nlapiCreateSearch(recordType, arrFilters, arrColumns);
		}

		// run search
		var resultSet = search.runSearch();
		
		// iterate through the search and get all data 1000 at a time
		var searchResultCount = 0;
		var resultSlice = null;
		var searchResult = [];

		do {
			resultSlice = resultSet.getResults(searchResultCount,
			        searchResultCount + 1000);

			if (resultSlice) {

				resultSlice.forEach(function(result) {

					searchResult.push(result);
					searchResultCount++;
				});
			}
		} while (isArrayNotEmpty(resultSlice) && resultSlice.length >= 1000);

		return searchResult;
	} catch (err) {
		nlapiLogExecution('ERROR', 'searchRecord', err);
		throw err;
	}
}

function isEmpty(value) {

	return value == null || value == "" || typeof (value) == undefined;
}

function isNotEmpty(value) {

	return !isEmpty(value);
}

function isArrayEmpty(argArray) {

	return !isArrayNotEmpty(argArray);
}

function isArrayNotEmpty(argArray) {

	return (isNotEmpty(argArray) && argArray.length > 0);
}