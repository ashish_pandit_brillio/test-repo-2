// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================

/*
		Script Name:	Restlet_Allocation_details_for_SFDC.js
		Author: 		Prabhat gupta
	    Date:           27 Oct 2020
		Description:	The script will get all the values from search and it return all the columns value into JSONObj.


		Script Modification Log:

		-- Date --		-- Modified By --			       --Requested By--				-- Description --

}*/
// END SCRIPT DESCRIPTION BLOCK  ====================================

/**
 * @NApiVersion 2.0
 * @NScriptType Restlet
 * @NModuleScope SameAccount
 */
define(['N/record','N/search' ],

function(record,search) 
{ 
   function doGet_AllocationDetails_data(context) 
	{
	 try{
		 log.debug ({ title: 'DEBUG',  details: 'Restlet POST'});
		var dataArray = []      		
		var responseObj = new Array();			  
			responseObj['message'] = '';
			responseObj['responseCode'] = '';
			responseObj['resource '] = '';
			responseObj['project_id'] = '';
			responseObj['project_name'] = '';
			responseObj['start_date'] = '';
			responseObj['end_date'] = '';	
			responseObj['percentage_allocation '] = '';
			responseObj['excuting_practice'] = '';
			responseObj['onsite_offsite'] = '';
			responseObj['resource_subsidiary'] = '';
			responseObj['employee_sub_practice'] = '';
			responseObj['employee_parent_department'] = '';
			responseObj['project_type'] = '';	
	
			//Getting Value in terms of columns 
			
			var filters =  [
							  ["enddate","onorafter","today"],
							  "AND",
							  ["job.jobtype","is","2"]							  
						   ]
		    
			var resourceallocationSearchObj = search.create({
				   type: "resourceallocation",
				   filters: filters,
				   columns:
				   [
					  search.createColumn({
						 name: "resource",
						 summary: "GROUP",
						 label: "Resource"
					  }),
					  search.createColumn({
						 name: "entityid",
						 join: "job",
						 summary: "MAX",
						 label: "Project ID"
					  }),
					  search.createColumn({
						 name: "altname",
						 join: "job",
						 summary: "MAX",
						 label: "Project Name"
					  }),
					  search.createColumn({
						 name: "startdate",
						 summary: "MAX",
						 label: "Start Date"
					  }),
					  search.createColumn({
						 name: "enddate",
						 summary: "MAX",
						 label: "End Date"
					  }),
					  search.createColumn({
						 name: "percentoftime",
						 summary: "SUM",
						 label: "Percentage of allocation"
					  }),
					  search.createColumn({
						 name: "custrecord_parent_practice",
						 join: "CUSTEVENT_PRACTICE",
						 summary: "GROUP",
						 label: "Executing Practice (Parent)"
					  }),
					  search.createColumn({
						 name: "custevent4",
						 summary: "GROUP",
						 label: "Onsite/Offsite"
					  }),
					  search.createColumn({
						 name: "subsidiary",
						 join: "employee",
						 summary: "GROUP",
						 label: "Subsidiary"
					  }),
					  search.createColumn({
						 name: "departmentnohierarchy",
						 join: "employee",
						 summary: "GROUP",
						 label: "Employee Sub Practice"
					  }),
					  search.createColumn({
						 name: "formulatext",
						 summary: "GROUP",
						 formula: "CASE WHEN INSTR({employee.department} , ' : ', 1) > 0 THEN SUBSTR({employee.department}, 1 , INSTR({employee.department} , ' : ', 1)) ELSE {employee.department} END",
						 label: "Employee Parent Department"
					  }),
					  search.createColumn({
						 name: "jobtype",
						 join: "job",
						 summary: "MAX",
						 label: "Project Type"
					  })
				   ]
				});
				
				//More than 1000+ records 
				var results = [];
				var searchid = 0;
				do {
					var resultslice = resourceallocationSearchObj.run().getRange({
						start: searchid,
						end: searchid + 1000
					});
					for (var rs in resultslice) {
						results.push(resultslice[rs]);
						searchid++;
					}
				} while (resultslice.length >= 1000);

				// if (_logValidation(searchResultCount)) 
					{
						var a_allCol =  resultslice[0].columns;
						//log.debug('a_allCol','a_allCol='+a_allCol ); 
						//log.debug('Record','Length='+resultslice.length )
					for(var i=0;i<results.length;i++)
						{
							 var s_resource = results[i].getText(a_allCol[0]);
								//log.debug('s_resource','s_resource='+s_resource);
							 var s_project_id = results[i].getValue(a_allCol[1]);
								//log.debug('s_project_id','s_project_id='+s_project_id);
							 var s_project_name = results[i].getValue(a_allCol[2]);
								//log.debug('s_project_name','s_project_name='+s_project_name);
							 var s_start_date = results[i].getValue(a_allCol[3]);
								//log.debug('s_start_date','s_start_date='+s_start_date);
							 var s_end_date = results[i].getValue(a_allCol[4]);
								//log.debug('s_end_date','s_end_date='+s_end_date);
							 var s_percentage_allocation = results[i].getValue(a_allCol[5]);
								//log.debug('s_percentage_allocation','s_percentage_allocation='+s_percentage_allocation);
							 var s_excuting_practice = results[i].getText(a_allCol[6]);
								//log.debug('s_excuting_practice','s_excuting_practice='+s_excuting_practice);
							 var s_onsite_offsite = results[i].getText(a_allCol[7]);
								//log.debug('s_onsite_offsite','s_onsite_offsite='+s_onsite_offsite);
							 var s_resource_subsidiary = results[i].getText(a_allCol[8]);
								//log.debug('s_resource_subsidiary','s_resource_subsidiary='+s_resource_subsidiary);
							 var s_employee_sub_practice = results[i].getValue(a_allCol[9]);
								//log.debug('s_employee_sub_practice','s_employee_sub_practice='+s_employee_sub_practice);
							 var s_employee_parent_department = results[i].getValue(a_allCol[10]);
								//log.debug('s_employee_parent_department','s_employee_parent_department='+s_employee_parent_department);
							 var s_project_type = results[i].getValue(a_allCol[11]);
								//log.debug('s_project_type','s_project_type='+s_project_type);

							responseObj['resource'] = s_resource;
							responseObj['project_id'] = s_project_id;
							responseObj['project_name'] = s_project_name;
							responseObj['start_date'] = s_start_date;
							responseObj['end_date'] = s_end_date;	
							responseObj['percentage_allocation'] = s_percentage_allocation.split(".")[0];
							responseObj['executing_practice'] = s_excuting_practice;
							responseObj['onsite_offsite'] = s_onsite_offsite;
							responseObj['resource_subsidiary'] = s_resource_subsidiary;
							responseObj['employee_sub_practice'] = s_employee_sub_practice;
							responseObj['employee_parent_department'] = s_employee_parent_department;
							responseObj['project_type'] = s_project_type;
							responseObj['message'] = "Get the record";
			                responseObj['responseCode'] = "1";


							var JSONObj = {
									
														"ResourceName": responseObj['resource'],
														"ProjectID": responseObj['project_id'], 
														"ProjName": responseObj['project_name'],
														"StartDate": responseObj['start_date'],
														"EndDate": responseObj['end_date'],
														"PercentageOfTime": responseObj['percentage_allocation'], 
														"EmpPractice": responseObj['executing_practice'],
														"Onsite_Offsite": responseObj['onsite_offsite'],
														"Subsidiary": responseObj['resource_subsidiary'],
														"EmployeeSubPractice": responseObj['employee_sub_practice'],
														"EmployeeParentDepartment": responseObj['employee_parent_department'], 
														"ProjectType": responseObj['project_type']
													
										   }
										dataArray.push(JSONObj)
						}//End of for loop
			          return dataArray;
				}//End of IF
		}//End of try
			catch (e)
			{
				 log.debug('Catch Log', ' e-====== **************** =' + e.toString());
			}//End of Catch
	}//End of Function


        function _logValidation(value) {
            if (value != null && value != '' && value != undefined && value.toString() != 'NaN' && value != NaN) {
                return true;
            } else {
                return false;
            }
        }
			
    return {
        'post': doGet_AllocationDetails_data        
    };
    
});
