/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       07 Aug 2015     nitish.mishra
 *
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your
 * script deployment.
 * 
 * @appliedtorecord recordType
 * 
 * @param {String}
 *            type Access mode: create, copy, edit
 * @returns {Void}
 */
function clientPageInit(type) {

}

function correctRate() {
	try {
		var itemCount = nlapiGetLineItemCount('item');

		for (var linenum = 1; linenum <= itemCount; linenum++) {
			nlapiSelectLineItem('item', linenum);
			nlapiSetCurrentLineItemValue('item', 'rate',
			        nlapiGetCurrentLineItemValue('item', 'amount'));
			nlapiCommitLineItem('item');
		}
	} catch (err) {
		alert(err.message);
	}
}

function correctDiscountLines() {
	try {

		var itemCount = nlapiGetLineItemCount('item');

		for (var linenum = 1; linenum <= itemCount; linenum++) {
			nlapiSelectLineItem('item', linenum);

			var lineDetails = {
			    item : nlapiGetCurrentLineItemValue('item', 'item'),
			    rate : nlapiGetCurrentLineItemValue('item', 'rate'),
			    amount : nlapiGetCurrentLineItemValue('item', 'amount'),
			    practice : nlapiGetCurrentLineItemValue('item', 'department'),
			    vertical : nlapiGetCurrentLineItemValue('item', 'class'),
			    pricedisplay : nlapiGetCurrentLineItemValue('item',
			            'price_display'),
			    employee : nlapiGetCurrentLineItemValue('item',
			            'custcol_employeenamecolumn'),
			    project : nlapiGetCurrentLineItemValue('item',
			            'custcolprj_name'),
			    customer : nlapiGetCurrentLineItemValue('item',
			            'custcolcustcol_temp_customer'),
			    territory : nlapiGetCurrentLineItemValue('item',
			            'custcol_territory')
			};

			var newItem = "";
			if (lineDetails.item == '1571') {
				newItem = 2523;
			} else if (lineDetails.item == '2454') {
				newItem = 2524;
			} else if (lineDetails.item == '2455') {
				newItem = 2522;
			}

			nlapiSelectNewLineItem('item');
			nlapiSetCurrentLineItemValue('item', 'item', newItem, true, true);
			nlapiSetCurrentLineItemValue('item', 'custcol_discouint_amt',
			        lineDetails.amount, true, true);
			nlapiSetCurrentLineItemValue('item', 'rate', lineDetails.amount,
			        true, true);
			nlapiSetCurrentLineItemValue('item', 'price_display', 'Custom',
			        true, true);
			nlapiSetCurrentLineItemValue('item', 'department',
			        lineDetails.practice);
			nlapiSetCurrentLineItemValue('item', 'custcol_employeenamecolumn',
			        lineDetails.employee);
			nlapiSetCurrentLineItemValue('item', 'custcolprj_name',
			        lineDetails.project);
			nlapiSetCurrentLineItemValue('item',
			        'custcolcustcol_temp_customer', lineDetails.customer);
			nlapiSetCurrentLineItemValue('item', 'custcol_territory',
			        lineDetails.territory);
			nlapiSetCurrentLineItemValue('item', 'class', lineDetails.vertical);
			nlapiCommitLineItem('item');
		}
	} catch (err) {
		alert(err.message);
	}
}
