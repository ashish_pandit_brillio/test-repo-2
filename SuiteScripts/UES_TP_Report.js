// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
/*
   	Script Name:
	Author:
	Company:
	Date:
	Description:


	Script Modification Log:

	-- Date --			-- Modified By --				--Requested By--				-- Description --



Below is a summary of the process controls enforced by this script file.  The control logic is described
more fully, below, in the appropriate function headers and code blocks.


     BEFORE LOAD
		- beforeLoadRecord(type)



     BEFORE SUBMIT
		- beforeSubmitRecord(type)


     AFTER SUBMIT
		- afterSubmitRecord(type)



     SUB-FUNCTIONS
		- The following sub-functions are called by the above core functions in order to maintain code
            modularization:

               - NOT USED

*/
}
// END SCRIPT DESCRIPTION BLOCK  ====================================



// BEGIN GLOBAL VARIABLE BLOCK  =====================================
{
	//  Initialize any Global Variables, in particular, debugging variables...




}
// END GLOBAL VARIABLE BLOCK  =======================================





// BEGIN BEFORE LOAD ==================================================

function beforeLoadRecord(type)
{

	/*  On before load:

          - EXPLAIN THE PURPOSE OF THIS FUNCTION

		-


		FIELDS USED:

          --Field Name--				--ID--


	*/



	//  LOCAL VARIABLES

    //  BEFORE LOAD CODE BODY


	return true;



}

// END BEFORE LOAD ====================================================





// BEGIN BEFORE SUBMIT ================================================

function beforeSubmitRecord(type)
{
    /*  On before submit:

          - PURPOSE
		-

          FIELDS USED:

          --Field Name--				--ID--


    */


    //  LOCAL VARIABLES


    //  BEFORE SUBMIT CODE BODY


	return true;

}

// END BEFORE SUBMIT ==================================================





// BEGIN AFTER SUBMIT =============================================

function afterSubmitRecord(type)
{
    /*  On after submit:

          - PURPOSE
		-

	FIELDS USED:

          --Field Name--				--ID--


    */


	//  LOCAL VARIABLES



	//  AFTER SUBMIT CODE BODY
		var id =nlapiGetRecordId()
		var type=nlapiGetRecordType()
		var o_cureentRecord=nlapiLoadRecord(type,id)
		var fromDate=o_cureentRecord.getFieldValue('custrecord_tp_fromdate')
		var toDate=o_cureentRecord.getFieldValue('custrecord_tp_todate')
		var fromSubsidiary=o_cureentRecord.getFieldValue('custrecord_tp_fromsubsidiary')
		var toSubsidiary=o_cureentRecord.getFieldValue('custrecord_tp_tosubsidary')

		var parameters = new Array();
		parameters['custscript_tpfromdate'] = fromDate;
		parameters['custscript_tptodate'] = toDate;
		parameters['custscript_tpfromsubsidiary'] = fromSubsidiary;
		parameters['custscript_tptosubsidiary'] = toSubsidiary;
		parameters['custscript_tprecordid'] = id;

	  	nlapiScheduleScript('customscript_sch_tp_price', null, parameters);
		nlapiLogExecution('DEBUG', 'afterSubmitRecord', " Script Schdule" );


	return true;

}

// END AFTER SUBMIT ===============================================





// BEGIN FUNCTION ===================================================
{



}
// END FUNCTION =====================================================
