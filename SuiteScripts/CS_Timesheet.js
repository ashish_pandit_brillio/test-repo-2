/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       22 May 2015     nitish.mishra
 *
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your
 * script deployment.
 * 
 * @appliedtorecord recordType
 * 
 * @param {String}
 *            type Access mode: create, copy, edit
 * @returns {Void}
 */
function showMessage(type) {
	var approval_status = nlapiGetFieldValue('approvalstatus');
	nlapiLogExecution('debug', 'approval_status', approval_status);

	if (approval_status == 3) {
		var main_div = document.getElementsByClassName("uir-record-status");
		main_div[0].innerHTML = "Approved -- <span style='color:red;'>You cannot edit this timesheet</span>";
	} else if (approval_status == 1) {
		var main_div = document.getElementsByClassName("uir-record-status");
		// main_div[0].setAttribute('style', 'color:red; border:1px solid
		// red;');
		main_div[0].innerHTML = "<span style='color:orange;'><b>Not Submitted</b> - Please click on Submit</span>";
	}
}
