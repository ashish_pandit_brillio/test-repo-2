/**
 * Module Description
 *
 * Version    Date            Author           Remarks
 * 1.00       03 May 2016     amol.sahijwani
 *
 */

/**
 * @returns {Void} Any or no return value
*/
function workflowAction(scriptContext)
{
	try
	{
		var context	=	nlapiGetContext();
		var type = nlapiGetContext().getSetting('SCRIPT', 'custscript_wa_payroll_diff_rep_event_typ');
		var oldRecord	=	nlapiGetOldRecord();
		var newRecord	=	nlapiGetNewRecord();

		//variable created for old values
		var f_old_ST_hours_total	=	0.0;
		var f_old_OT_hours_total	=	0.0;
		var f_old_leave_hours_total =	0.0;
		var f_old_holiday_hours_total = 0.0;

		//variable created for new values
		var f_new_ST_hours_total	=	0.0;
		var f_new_OT_hours_total	=	0.0;
		var f_new_leave_hours_total =	0.0;
		var f_new_holiday_hours_total = 0.0;

		// Array of Subrecord names
		var a_days = ['timebill0', 'timebill1', 'timebill2', 'timebill3', 'timebill4', 'timebill5', 'timebill6'];
		var a_days1 = ['hours0', 'hours1', 'hours2', 'hours3', 'hours4', 'hours5', 'hours6'];

		//type EDIT
		if((oldRecord != null) && ((type == 'EDIT') || (type == 'edit')))
		{
			// new Timesheet data
			var i_line_count_new = newRecord.getLineItemCount('timeitem');
			nlapiLogExecution('Debug','EDIT i_line_count_new',i_line_count_new);
			for(var i_line_indx = 1; i_line_indx <= i_line_count_new; i_line_indx++)
			{
				newRecord.selectLineItem('timeitem',i_line_indx);
				var emp_id = newRecord.getFieldValue('employee');
				var emp_type = nlapiLookupField('employee',emp_id,'employeetype');

				for(var i_day_indx = 0; i_day_indx < 7; i_day_indx++)
				{
					var sub_record_name = a_days[i_day_indx];
					var o_sub_record_view = newRecord.getCurrentLineItemValue('timeitem', sub_record_name);

					var i_item_id = newRecord.getCurrentLineItemValue('timeitem', 'item');
					var i_approval_status = newRecord.getCurrentLineItemValue('timeitem', 'statushours'+i_day_indx);
					var f_duration	=	hoursToDuration(newRecord.getCurrentLineItemValue('timeitem','hours'+i_day_indx));

					nlapiLogExecution('debug','EDIT, new iterations',"Row: "+i_line_indx+" Column: "+i_day_indx+" Duration: "+f_duration+" Approval_status: "+i_approval_status+" Emp_type: "+emp_type+" I_item_id: "+i_item_id);
					if(f_duration)
					{
						if(i_approval_status == 1 || i_approval_status == 2 || i_approval_status == 3 || i_approval_status =='') 
						{
							if(emp_type == 3)
							{
								if(i_item_id == '2222' || i_item_id == '2221' || i_item_id == '2224' || i_item_id == '2426')
								{
									f_new_ST_hours_total += f_duration;
								}
								else if(i_item_id == '2425')
								{
									f_new_OT_hours_total += f_duration;
								}
								else if(i_item_id == '2479')
								{
									f_new_leave_hours_total += f_duration;
								}
								else if(i_item_id == '2480')
								{
									f_new_holiday_hours_total += f_duration;
								}
							}
							else
							{
								if(i_item_id == '2222' || i_item_id == '2221')
								{
									f_new_ST_hours_total += f_duration;
								}
								else if(i_item_id == '2425')
								{
									f_new_OT_hours_total += f_duration;
								}
								else if(i_item_id == '2479')
								{
									f_new_leave_hours_total += f_duration;
								}
								else if(i_item_id == '2480')
								{
									f_new_holiday_hours_total += f_duration;
								}
							}
						}
					}
				}
			}
			nlapiLogExecution('debug','EDIT new durations',"New ST> "+f_new_ST_hours_total+" New OT> "+f_new_OT_hours_total+" New leave> "+f_new_leave_hours_total+" New holiday> "+f_new_holiday_hours_total);

			//Old Timesheet data
			var i_line_count_old = oldRecord.getLineItemCount('timeitem');
			nlapiLogExecution('Debug','EDIT i_line_count_old',i_line_count_old);
			for(var i_line_indx = 1; i_line_indx <= i_line_count_old; i_line_indx++)
			{
				oldRecord.selectLineItem('timeitem', i_line_indx);
				var emp_id = oldRecord.getFieldValue('employee');
				var emp_type = nlapiLookupField('employee',emp_id,'employeetype');

				for(var i_day_indx = 0; i_day_indx < 7; i_day_indx++)
				{
					var sub_record_name = a_days[i_day_indx];
					var o_sub_record_view = oldRecord.getCurrentLineItemValue('timeitem', sub_record_name);
					var i_item_id = oldRecord.getCurrentLineItemValue('timeitem','item');

					var i_approval_status = oldRecord.getCurrentLineItemValue('timeitem','statushours'+i_day_indx);

					var f_duration	=	hoursToDuration(oldRecord.getCurrentLineItemValue('timeitem','hours'+i_day_indx));
					nlapiLogExecution('debug','EDIT, old iterations',"Row: "+i_line_indx+" Column: "+i_day_indx+" Duration: "+f_duration+" Approval_status: "+i_approval_status+" Emp_type: "+emp_type+" I_item_id: "+i_item_id);
					if(f_duration)
					{
						if(i_approval_status == 1 || i_approval_status == 2 || i_approval_status == 3)
						{
							if(emp_type == 3)
							{
								if(i_item_id == '2222' || i_item_id == '2221' || i_item_id == '2224' || i_item_id == '2426')
								{
									f_old_ST_hours_total += f_duration;
								}
								else if(i_item_id == '2425')
								{
									f_old_OT_hours_total += f_duration;
								}
								else if(i_item_id == '2479')
								{
									f_old_leave_hours_total += f_duration;
								}
								else if(i_item_id == '2480')
								{
									f_old_holiday_hours_total += f_duration;
								}
							}
							else
							{
								if(i_item_id == '2222' || i_item_id == '2221')
								{
									f_old_ST_hours_total += f_duration;
								}
								else if(i_item_id == '2425')
								{
									f_old_OT_hours_total += f_duration;
								}
								else if(i_item_id == '2479')
								{
									f_old_leave_hours_total += f_duration;
								}
								else if(i_item_id == '2480')
								{
									f_old_holiday_hours_total += f_duration;
								}
							}
						}
					}
				}
			}
			nlapiLogExecution('debug','EDIT Old durations',"Old ST> "+f_old_ST_hours_total+" Old OT> "+f_old_OT_hours_total+" Old leave> "+f_old_leave_hours_total+" Old holiday> "+f_old_holiday_hours_total);
		}

		if(type == 'DELETE')
		{
			/*var i_line_count_new = oldRecord.getLineItemCount('timeitem');

			for(var i_line_indx = 1; i_line_indx <= i_line_count_new; i_line_indx++)
			{
				oldRecord.selectLineItem('timeitem', i_line_indx);

				var emp_id = oldRecord.getFieldValue('employee');
				var emp_type = nlapiLookupField('employee',emp_id,'employeetype');

				for(var i_day_indx = 0; i_day_indx < 7; i_day_indx++)
				{
					var sub_record_name = a_days[i_day_indx];
					var o_sub_record_view = oldRecord.getCurrentLineItemValue('timeitem', sub_record_name);
					if(o_sub_record_view)
					{
						var i_item_id = oldRecord.getCurrentLineItemValue('timeitem','item');

						var i_approval_status = oldRecord.getCurrentLineItemValue('timeitem','statushours'+i_day_indx);

						var f_duration	=	hoursToDuration(oldRecord.getCurrentLineItemValue('timeitem','hours'+i_day_indx));

						if(i_approval_status == 1 || i_approval_status == 2 || i_approval_status == 3)
						{
							if(emp_type == 3)
							{
								if(i_item_id == '2222' || i_item_id == '2221' || i_item_id == '2224' || i_item_id == '2426')
								{
									f_old_ST_hours_total += f_duration;
								}
								else if(i_item_id == '2425')
								{
									f_old_OT_hours_total += f_duration;
								}
								else if(i_item_id == '2479')
								{
									f_old_leave_hours_total += f_duration;
								}
								else if(i_item_id == '2480')
								{
									f_old_holiday_hours_total += f_duration;
								}
							}
							else
							{
								if(i_item_id == '2222' || i_item_id == '2221')
								{
									f_old_ST_hours_total += f_duration;
								}
								else if(i_item_id == '2425')
								{
									f_old_OT_hours_total += f_duration;
								}
								else if(i_item_id == '2479')
								{
									f_old_leave_hours_total += f_duration;
								}
								else if(i_item_id == '2480')
								{
									f_old_holiday_hours_total += f_duration;
								}
							}
						}
						//nlapiLogExecution('AUDIT', 'New Record', 'Day: ' + sub_record_name + ', Item ID: ' + i_item_id + ', Approval Status: ' + i_approval_status + ', Duration: ' + f_duration);
					}
				}
			}*/
			f_old_ST_hours_total = oldRecord.getFieldValue('custrecord_deleted_hours');
		}


		if((type == 'CREATE') || (type == 'create'))
		{
			var sysdate = new Date();

			var ts_date = newRecord.getFieldValue('trandate');
			ts_date = nlapiStringToDate(ts_date);

			var diffTime = Math.abs(sysdate - ts_date);
			var diffDays = Math.ceil(diffTime / (1000 * 60 * 60 * 24));

			if(diffDays > 14)
			{
				var i_line_count_new = newRecord.getLineItemCount('timeitem');
				nlapiLogExecution('Debug','CREATE i_line_count_new',i_line_count_new);

				for(var i_line_indx = 1; i_line_indx <= i_line_count_new; i_line_indx++)
				{
					newRecord.selectLineItem('timeitem', i_line_indx);

					var emp_id = newRecord.getFieldValue('employee');
					var emp_type = nlapiLookupField('employee',emp_id,'employeetype');

					for(var i_day_indx = 0; i_day_indx < 7; i_day_indx++)
					{
						var sub_record_name = a_days1[i_day_indx];
						var o_sub_record_view = newRecord.getCurrentLineItemValue('timeitem', sub_record_name);
						var i_item_id = newRecord.getCurrentLineItemValue('timeitem','item');
						var i_approval_status = newRecord.getCurrentLineItemValue('timeitem','statushours'+i_day_indx);

						//var f_duration	=	hoursToDuration(newRecord.getCurrentLineItemValue('timeitem','hours'+i_day_indx));
						var f_duration = hoursToDuration(o_sub_record_view);
						nlapiLogExecution('debug','CREATE, New iterations',"Row: "+i_line_indx+" Column: "+i_day_indx+" Duration: "+f_duration+" Approval_status: "+i_approval_status+" Emp_type: "+emp_type+" I_item_id: "+i_item_id);
						if(f_duration)
						{
							if(emp_type == 3)
							{
								if(i_item_id == '2222' || i_item_id == '2221' || i_item_id == '2224' || i_item_id == '2426')
								{
									f_new_ST_hours_total += f_duration;
								}	 
								else if(i_item_id == '2425')
								{
									f_new_OT_hours_total += f_duration;
								}
								else if(i_item_id == '2479')
								{
									f_new_leave_hours_total += f_duration;
								}
								else if(i_item_id == '2480')
								{
									f_new_holiday_hours_total += f_duration;
								}
							}
							else
							{
								if(i_item_id == '2222' || i_item_id == '2221')
								{
									f_new_ST_hours_total += f_duration;
								}
								else if(i_item_id == '2425')
								{
									f_new_OT_hours_total += f_duration;
								}
								else if(i_item_id == '2479')
								{
									f_new_leave_hours_total += f_duration;
								}
								else if(i_item_id == '2480')
								{
									f_new_holiday_hours_total += f_duration;
								}
							}
						}
					}
				}
				nlapiLogExecution('debug','CREATE durations',"New ST> "+f_new_ST_hours_total+" New ot> "+f_new_OT_hours_total+" New leave> "+f_new_leave_hours_total+" New holiday> "+f_new_holiday_hours_total);
			}
		}

		if(f_new_ST_hours_total != f_old_ST_hours_total || f_new_OT_hours_total != f_old_OT_hours_total || f_new_leave_hours_total != f_old_leave_hours_total || f_new_holiday_hours_total != f_old_holiday_hours_total)
		{
			var i_employee_id	=	newRecord.getFieldValue('employee');
			var s_end_date		=	newRecord.getFieldValue('enddate');

			var newLogRecord	=	nlapiCreateRecord('customrecord_payroll_ts_diff_rep_data');
			newLogRecord.setFieldValue('custrecord_ptdr_action', type);
			newLogRecord.setFieldValue('custrecord_ptdr_employee', i_employee_id);
			newLogRecord.setFieldValue('custrecord_ptdr_weekending', s_end_date);
			newLogRecord.setFieldValue('custrecord_ptdr_old_st_hours', f_old_ST_hours_total);
			newLogRecord.setFieldValue('custrecord_ptdr_old_ot_hours', f_old_OT_hours_total);
			newLogRecord.setFieldValue('custrecord_ptdr_old_leave_hours', f_old_leave_hours_total);
			newLogRecord.setFieldValue('custrecord_ptdr_old_holiday_hours', f_old_holiday_hours_total);
			newLogRecord.setFieldValue('custrecord_ptdr_new_st_hours', f_new_ST_hours_total);
			newLogRecord.setFieldValue('custrecord_ptdr_new_ot_hours', f_new_OT_hours_total);
			newLogRecord.setFieldValue('custrecord_ptdr_new_leave_hours', f_new_leave_hours_total);
			newLogRecord.setFieldValue('custrecord_ptdr_new_holiday_hours', f_new_holiday_hours_total);
			newLogRecord.setFieldValue('custrecord_ptdr_updated_by', i_employee_id);
			nlapiSubmitRecord(newLogRecord);
		}
	}
	catch(e)
	{
		nlapiLogExecution('AUDIT', 'Error', e.message);
		nlapiSendEmail(442, 'information.systems@brillio.com', 'Timesheet hours changed', e.message, null, null, null, null, null);	
	}
}

function hoursToDuration(s_hours)
{
	var a_duration = s_hours.split(":");

	return parseFloat(a_duration[0]) + parseFloat(a_duration[1])/60.0;
}