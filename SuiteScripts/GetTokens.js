/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       04 May 2019     Aazam Ali 	   Get Tokens for Taleo Integration 
 *
 */

function getTokens() {
	var url = "https://netsuite-taleo-transformer.azurewebsites.net/auth"; // url to get the authentication
    var postdata = {
        "authKey": "0df187e1-d435-41e7-b17d-71764c37909c"
    };
    var JSONBody = JSON.stringify(postdata, replacer);
    var headers = [];
    headers["Content-Type"] = "application/json";
    var method = "POST";
    var respones = nlapiRequestURL(url, JSONBody, headers,null, method);
    var jsonResoponse =  JSON.parse(respones.body);
    return jsonResoponse.token;
}
function replacer(key, value) {
    if (typeof value == "number" && !isFinite(value)) {
        return String(value);
    }
    return value;
}
function _logValidation(value) 
{
 if(value!='null' && value != null && value != null && value != '' && value != undefined && value != undefined && value != 'undefined' && value!= 'undefined'&& value!= 'NaN' && value != NaN) 
 {
  return true;
 }
 else 
 { 
  return false;
 }
}