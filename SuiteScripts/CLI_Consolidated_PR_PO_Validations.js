/**
 * @author Shweta
 */
// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
/*
   	Script Name : CLI_Consolidated_PR_PO_Validations.js
	Author      : Shweta Chopde
	Company     :
	Date        : 18 April 2014
    Description :


	Script Modification Log:

	-- Date --			-- Modified By --				--Requested By--				-- Description --




	Below is a summary of the process controls enforced by this script file.  The control logic is described
	more fully, below, in the appropriate function headers and code blocks.

     PAGE INIT
		- pageInit(type)


     SAVE RECORD
		- saveRecord()


     VALIDATE FIELD
		- validateField(type, name, linenum)


     FIELD CHANGED
		- fieldChanged(type, name, linenum)


     POST SOURCING
		- postSourcing(type, name)


	LINE INIT
		- lineInit(type)


     VALIDATE LINE
		- validateLine()


     RECALC
		- reCalc()


     SUB-FUNCTIONS
		- The following sub-functions are called by the above core functions in order to maintain code
            modularization:





*/
}
// END SCRIPT DESCRIPTION BLOCK  ====================================



// BEGIN SCRIPT UPDATION BLOCK  ====================================
/*


*/
// END SCRIPT UPDATION BLOCK  ====================================




// BEGIN GLOBAL VARIABLE BLOCK  =====================================
{

	//  Initialize any Global Variables, in particular, debugging variables...


}
// END GLOBAL VARIABLE BLOCK  =======================================





// BEGIN PAGE INIT ==================================================

function pageInit(type)
{
    /*  On page init:

	- PURPOSE

		FIELDS USED:

		--Field Name--				--ID--			--Line Item Name--

    */

    //  LOCAL VARIABLES


    //  PAGE INIT CODE BODY


}

// END PAGE INIT ====================================================





// BEGIN SAVE RECORD ================================================
var s_serial='';
var s_data_serial=''
var s_vendor =''
function saveRecord_PR()
{
  var a_PR_array = new Array();
  var a_PR_data_array = new Array();
  var a_vendor_array = new Array();
  
  var i_count = nlapiGetLineItemCount('custpage_pr_sublist')
  var i_cnt =0	
   for(var t=1;t<=i_count;t++)
   {
   	 var f_createPO = nlapiGetLineItemValue('custpage_pr_sublist','custpage_create_po_checkbox',t)
		 
	 if(f_createPO =='T')
	 {	 
	   var i_selected_POS = nlapiGetLineItemValue('custpage_pr_sublist','custpage_selected_pos',t)
	   
	   var i_quantity = nlapiGetLineItemValue('custpage_pr_sublist','custpage_quantity',t)
		
	   var i_item = nlapiGetLineItemValue('custpage_pr_sublist','custpage_item_id',t)
	   
	   var i_vendor = nlapiGetLineItemValue('custpage_pr_sublist','custpage_vendor',t)
	   
	   var i_delivery_location = nlapiGetLineItemValue('custpage_pr_sublist','custpage_delivery_location',t)		
		
	   a_vendor_array.push(i_vendor+'&&&'+i_delivery_location)	
		
	   var i_subsidiary = nlapiGetLineItemValue('custpage_pr_sublist','custpage_subsidiary',t)
		
	   var i_vertical = nlapiGetLineItemValue('custpage_pr_sublist','custpage_vertical',t)

	   var i_practice = nlapiGetLineItemValue('custpage_pr_sublist','custpage_practice',t)
		
	   var i_currency = nlapiGetLineItemValue('custpage_pr_sublist','custpage_currency',t)
	   
	   var i_description = nlapiGetLineItemValue('custpage_pr_sublist','custpage_description',t)
		
	   var i_S_NO = nlapiGetLineItemValue('custpage_pr_sublist','custpage_pr_s_no',t)
		  
	   var i_quan_rem = nlapiGetLineItemValue('custpage_pr_sublist','custpage_quantity_remaining',t)
	   
	   var i_PR_Nos = nlapiGetLineItemValue('custpage_pr_sublist','custpage_pr_nos',t)
	  	
	  	  var i_PR_Item_Rec_Id = nlapiGetLineItemValue('custpage_pr_sublist','custpage_pritem_id',t)
		  
	   if(s_data_serial =='')
	   {
	   	s_data_serial = i_selected_POS+'##'+i_item+'##'+i_quantity+'##'+i_vendor +'##'+i_subsidiary+'##'+i_vertical+'##'+i_practice+'##'+i_currency+'##'+i_description+'##'+i_S_NO+'##'+i_quan_rem+'##'+i_PR_Nos+'##'+i_PR_Item_Rec_Id
	   }
	   else
	   {
	   	s_data_serial = s_data_serial+'*,*'+i_selected_POS+'##'+i_item+'##'+i_quantity+'##'+i_vendor +'##'+i_subsidiary+'##'+i_vertical+'##'+i_practice+'##'+i_currency+'##'+i_description+'##'+i_S_NO+'##'+i_quan_rem+'##'+i_PR_Nos+'##'+i_PR_Item_Rec_Id
	   }	
		
	   if(s_serial =='')
	   {
	   	s_serial = i_selected_POS
	   }
	   else
	   {
	   	s_serial=s_serial+','+i_selected_POS
	   }	   
	   if(s_vendor =='')
	   {
	   	s_vendor = i_vendor
	   }
	   else
	   {
	   	s_vendor=s_vendor+','+i_vendor
	   }
	 	
	 }
	
   }  
   nlapiSetFieldValue('custpage_selected_created_po',s_serial)
   nlapiSetFieldValue('custpage_data_array',s_data_serial)
   nlapiSetFieldValue('custpage_vendor_array',s_vendor)
      
   a_vendor_array = removearrayduplicate(a_vendor_array)
   
  /*
 alert(' Vendor Array ->'+a_vendor_array)
   alert(' Vendor Array Length->'+a_vendor_array.length)
*/
   
    if(a_vendor_array.length!=1)
	{
		alert(' Please select only single vendor with a unique location. \n You are not allowed to select multiple vendors & mutiple locations while creating Purchase Order.')
	    location.reload();	
		return false;
	}
   
	return true;

}

// END SAVE RECORD ==================================================

// BEGIN VALIDATE FIELD =============================================

function validateField(type, name, linenum)
{

	/*  On validate field:

          - EXPLAIN THE PURPOSE OF THIS FUNCTION


          FIELDS USED:

          --Field Name--				--ID--


    */


	//  LOCAL VARIABLES



	//  VALIDATE FIELD CODE BODY


	return true;

}

// END VALIDATE FIELD ===============================================





// BEGIN FIELD CHANGED ==============================================

function fieldChanged_consolidated(type, name, linenum)
{	
  if(name =='custpage_quantity')
  {
  	var i_index = nlapiGetCurrentLineItemIndex('custpage_pr_sublist')	
	
  	var i_quantity = nlapiGetLineItemValue('custpage_pr_sublist','custpage_quantity',i_index)
	
 	var i_quantity_remaining = nlapiGetLineItemValue('custpage_pr_sublist','custpage_quantity_remaining',i_index)
	
	/*
alert(' Index -->'+i_index)		
	alert(' Quantity -->'+i_quantity)	
	alert(' Quantity Remaining -->'+i_quantity_remaining)
		
*/
	
	    if(i_quantity_remaining!=0 && (i_quantity!=null && i_quantity!='' && i_quantity!=undefined) &&(i_quantity_remaining!=null && i_quantity_remaining!=undefined && i_quantity_remaining!=''))
		{			
			if(parseInt(i_quantity) > parseInt(i_quantity_remaining))
			{
				nlapiSetLineItemValue('custpage_pr_sublist','custpage_quantity',i_index ,'')			
				alert(' Entered quanity is exceeding the quantity in stock \n Please enter quantity within remaining quantity limit .')
			    
			}
		}
		
		
	if((i_quantity!=null && i_quantity!='' && i_quantity!=undefined) &&(i_quantity_remaining!=null && i_quantity_remaining!=undefined))
	{
		if(parseInt(i_quantity) <= parseInt(i_quantity_remaining))
		{
		 var i_pending_quantity = parseInt(i_quantity_remaining)- parseInt(i_quantity)	
		/*
 alert('ddddddddd Index -->'+i_index)		
		 alert(' Pending Quantity -->'+i_pending_quantity)	
*/
		 nlapiSelectLineItem('custpage_pr_sublist',i_index)
		 nlapiSetLineItemValue('custpage_pr_sublist','custpage_quantity_remaining',i_index ,i_pending_quantity)
	  		
		}
	}
	
  }
  
  if(name == 'custpage_create_po_checkbox')
  {
  	var i_createPO = nlapiGetCurrentLineItemValue('custpage_pr_sublist','custpage_create_po_checkbox')
	
	if(i_createPO == 'T')
	{
		var i_index = nlapiGetCurrentLineItemIndex('custpage_pr_sublist')
	
		var i_PR_ID = nlapiGetLineItemValue('custpage_pr_sublist','custpage_pr_id',i_index)
		
		//var i_PR_No = nlapiGetLineItemValue('custpage_pr_sublist','custpage_pr_nos',i_index)
		
		nlapiSetLineItemValue('custpage_pr_sublist','custpage_selected_pos',i_index,i_PR_ID)
		
		var i_quantity = nlapiGetLineItemValue('custpage_pr_sublist','custpage_quantity',i_index)
				
		var i_quantity_remaining =  nlapiGetLineItemValue('custpage_pr_sublist','custpage_quantity_remaining',i_index)
		
		if(i_quantity_remaining!=0 && (i_quantity!=null && i_quantity!='' && i_quantity!=undefined) &&(i_quantity_remaining!=null && i_quantity_remaining!='' && i_quantity_remaining!=undefined))
		{
			if(parseInt(i_quantity) > parseInt(i_quantity_remaining))
			{
				alert(' Entered quanity is exceeding the quantity in stock \n Please enter quantity within remaining quantity limit .')
			    nlapiSetLineItemValue('custpage_pr_sublist','custpage_quantity',i_index,'')
			
			}
		}
		
		if(i_quantity ==null || i_quantity=='' || i_quantity==undefined)
		{
			alert(' Please enter Quantity.')
			nlapiSetLineItemValue('custpage_pr_sublist','custpage_create_po_checkbox',i_index,'F')
		
			return false;
		}
		
			
	}//Checked T
	
	if(i_createPO == 'F')
	{
		var i_index = nlapiGetCurrentLineItemIndex('custpage_pr_sublist')
		
		nlapiSetLineItemValue('custpage_pr_sublist','custpage_selected_pos',i_index,'')
			
	}//Checked T
	
  }//Create PO
}

// END FIELD CHANGED ================================================





// BEGIN POST SOURCING ==============================================

function postSourcing(type, name)
{

    /*  On post sourcing:

	- PURPOSE

          FIELDS USED:

		--Field Name--			--ID--		--Line Item Name--


    */

    //  LOCAL VARIABLES


    //  POST SOURCING CODE BODY



}

// END POST SOURCING ================================================



// BEGIN LINE INIT ==============================================

function lineInit(type)
{

    /*  On Line Init:

	- PURPOSE

          FIELDS USED:

		--Field Name--			--ID--		--Line Item Name--


    */

    //  LOCAL VARIABLES


    //  LINE INIT CODE BODY



}

// END LINE INIT ================================================


// BEGIN VALIDATE LINE ==============================================

function validateLine(type)
{

	/*  On validate line:

          - EXPLAIN THE PURPOSE OF THIS FUNCTION


          FIELDS USED:

          --Field Name--				--ID--


    */


	//  LOCAL VARIABLES


	//  VALIDATE LINE CODE BODY


	return true;

}

// END VALIDATE LINE ================================================




// BEGIN RECALC =====================================================

function recalc(type)
{

	/*  On recalc:

          - EXPLAIN THE PURPOSE OF THIS FUNCTION


          FIELDS USED:

          --Field Name--				--ID--


    */


	//  LOCAL VARIABLES


	//  RECALC CODE BODY


}

// END RECALC =======================================================


function removearrayduplicate(array)
{
   var newArray = new Array();
   label:for(var i=0; i<array.length;i++ )
     {  
     for(var j=0; j<array.length;j++ )
      {
      if(newArray[j]==array[i]) 
      continue label;
     }
     newArray[newArray.length] = array[i];
      }
   return newArray;
}


