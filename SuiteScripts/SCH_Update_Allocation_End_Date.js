// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
    /*
     Script Name: SCH_Update_Allocation_End_Date
     Author: Vikrant
     Company: Aashna CloudTech
     Date: 04-12-2014
     Script Modification Log:
     -- Date --			-- Modified By --				--Requested By--				-- Description --
     Below is a summary of the process controls enforced by this script file.  The control logic is described
     more fully, below, in the appropriate function headers and code blocks.
     SCHEDULED FUNCTION
     - scheduledFunction(type)
     SUB-FUNCTIONS
     - The following sub-functions are called by the above core functions in order to maintain code
     modularization:
     - NOT USED
     */
}
// END SCRIPT DESCRIPTION BLOCK  ====================================


// BEGIN SCHEDULED FUNCTION =============================================

function SCH_Update_Allocation_End_Date(type) //
{
    //  LOCAL VARIABLES
    
    
    
    //  SCHEDULED FUNCTION CODE BODY
    
    try //
    {
        var search_Result = new Array();
        
        //do // 
        {
            //var a_Filters = new Array();
            //a_Filters[a_Filters.length] = new nlobjSearchFilter('enddate', null, 'onorafter', '1/1/2030');
            
            //var a_Columns = new Array();
            //a_Columns[a_Columns.length] = new nlobjSearchColumn('internalid');
            
            //search_Result = nlapiSearchRecord('resourceallocation', null, a_Filters, a_Columns);  customsearch_over_allocated
            //var s = nlapiLoadSearch('resourceallocation', 'customsearch_over_allocated');
            //nlapiLogExecution('DEBUG', 'SCH_Update_Allocation_End_Date', 'before search');
            //search_Result = nlapiSearchRecord(null, 'customsearch_over_allocated');
            //nlapiLogExecution('DEBUG', 'SCH_Update_Allocation_End_Date', 'search_Result : ' + search_Result);
            //nlapiLogExecution('DEBUG', 'SCH_Update_Allocation_End_Date', 'After search');
            
            //return;
            
            search_Result = ['2104', '2183', '2211', '2569', '2658', '5619', '7543', '7545', '7547', '7558', '7570', '7652', '7658', '7663', '7670', '7671', '7711', '7730', '7731', '7748', '7752', '7834', '7843', '7851', '7856', '7894', '7897', '7905', '7958', '7977', '7978', '8092', '8098', '8099', '8100', '8102', '8103', '8104', '8105', '8106', '8107', '8108', '8109', '8110', '8111', '8112', '8113', '8114', '8208', '8210', '8213', '8214', '8215', '8216', '8325', '8331', '8333', '8334', '8339', '8340', '8341', '8342', '8343', '8440', '8442', '8443', '8444', '8445', '8446', '8447', '8448', '8449', '8450', '8451', '8452', '8453', '8454', '8455', '8456', '8457', '8458', '8459', '8460', '8462', '8463', '8464', '8466', '8467', '8468', '8469', '8470', '8471', '8472', '8473', '8474', '8476', '8477', '8542', '8544', '8545', '8546', '8547', '8548', '8549', '8550', '8551', '8552', '8553', '8554', '8555', '8557', '8558', '8559', '8561', '8562', '8563', '8566', '8567', '8568', '8569', '8570', '8571', '8572', '8573', '8574', '8575', '8576', '8577', '8642', '8643', '8644', '8645', '8646', '8647', '8648', '8649', '8651', '8652', '8653', '8654', '8655', '8656', '8657', '8658', '8659', '8660', '8661', '8664', '8665', '8666', '8667', '8668', '8669', '8670', '8671', '8672', '8673', '8674', '8675', '8742', '8743', '8744', '8745', '8746', '8747', '8748', '8749', '8750', '8751', '8752', '8753', '8754', '8755', '8756', '8757', '8758', '8759', '8760', '8761', '8762', '8763', '8764', '8765', '8766', '8767', '8768', '8770', '8771', '8772', '8773', '8775', '8778', '8779', '8780', '8848', '8849', '8850', '8852', '8853', '8854', '8855', '8856', '8962', '8963', '8964', '8965', '8966', '8968', '8969', '8970', '8983', '8991', '9003', '9006', '9008', '9009', '9010', '9026', '9033', '9036', '9037', '9038', '9054', '9059', '9070', '9072', '9077', '9078', '9120', '9125', '9137', '9142', '9144', '9290', '9303', '9320', '9325', '9384', '9551', '9567', '9679', '9726', '9727', '9728', '9729', '9730', '9731', '9732', '9733', '9734', '9735', '9736', '9737', '9738', '9739', '9740', '9741', '9742', '9743', '9744', '9745', '9746', '9747', '9748', '9749', '9750', '9751', '9752', '9753', '9836', '9837', '9838', '9839', '9840', '9841', '9842', '9843', '9845', '9846', '9847', '9848', '9849', '9850', '9851', '9852', '9853', '9854', '9855', '9856', '9857', '9858', '9859', '9860', '9861', '9862', '9863', '9864', '9865', '9950', '9951', '9952', '9953', '9954', '9955', '9956', '9957', '9958', '9959', '9960', '9961', '9962', '9963', '9964', '9965', '9966', '9967', '9968', '9969', '9970', '9971', '9972', '9973', '9974', '9975', '10066', '10067', '10068', '10073', '10074', '10079', '10121', '10125', '10130', '10134', '10137', '10183', '10195', '10197', '10204', '10208', '10212', '10213', '10217', '10218', '10219', '10220', '10254', '10255', '10256', '10262', '10276', '10277', '10278', '10279', '10280', '10281', '10282', '10283', '10284', '10290', '10293', '10301', '10302', '10328', '10329', '10331', '10333', '10334', '10335', '10336', '10340', '10371', '10377', '10386', '10387', '10388', '10389', '10390', '10391', '10392', '10393', '10408', '10410', '10414', '10415', '10416', '10417', '10418', '10419', '10420', '10421', '10422', '10423', '10425', '10426', '10443', '10446', '10447', '10519', '10522', '10525', '10526', '10527', '10529', '10530', '10552', '10553', '10554', '10565', '10572', '10573', '10656', '10657', '10658', '10659', '10660', '10661', '10662', '10663', '10751', '10789', '10791', '10792', '10809', '10811', '10812', '10824', '10848', '10849', '10860', '10861', '10881', '10882', '10888', '10936', '10940', '10941', '10955', '10960', '10961', '10962', '10995', '10998', '11002', '11003', '11005', '11067', '11068', '11069', '11073', '11089', '11090', '11091', '11092', '11093', '11094', '11095', '11096', '11103', '11104', '11105', '11106', '11107', '11108', '11208', '11212', '11214', '11215', '11216', '11222', '11247', '11248', '11263', '11269', '11274', '11299', '11300', '11330', '11333', '11340', '11342', '11343', '11344', '11361', '11542', '11543', '11544', '11545', '11549', '11550', '11551', '11552', '11553', '11554', '11555', '11556', '11557', '11558', '11559', '11562', '11564', '11565', '11566', '11568', '11570', '11571', '11573', '11574', '11575', '11576', '11577', '11578', '11579', '11580', '11582', '11583', '11584', '11585', '11586', '11587', '11588', '11612', '11613', '11616', '11617', '11648', '11649', '11685', '11688', '11702', '11703', '11710', '11711', '11712', '11716', '11746', '11751', '11752', '11794', '11797', '11798', '11799', '11800', '11803', '11804', '11806', '11808', '11809', '11811', '11817', '11818', '11819', '11820', '11822', '11828', '11829', '11830', '11831', '11836', '11837', '11839', '11840', '11867', '11868', '11891', '11909', '11911', '11918', '11961', '11996', '11998', '11999', '12000', '12001', '12002', '12003', '12033', '12039', '12040', '12041', '12043', '12044', '12045', '12046', '12047', '12048', '12049', '12050', '12051', '12053', '12054', '12055', '12056', '12057', '12058', '12059', '12060', '12063', '12074', '12075', '12076', '12077', '12078', '12079', '12081', '12082', '12083', '12107', '12112', '12134', '12138', '12144', '12145', '12154', '12166', '12168', '12184', '12186', '12231', '12232', '12233', '12234', '12235', '12237', '12238', '12254', '12259', '12261', '12262', '12263', '12269', '12270', '12271', '12282', '12283', '12286', '12287', '12288'];
            
            if (_is_Valid(search_Result)) //
            {
                //var all_Columns = search_Result[0].getAllColumns();
                nlapiLogExecution('DEBUG', 'SCH_Update_Allocation_End_Date', 'search_Result : ' + search_Result.length);
                
                for (var counter_I = 0; counter_I < search_Result.length; counter_I++) //
                {
                    nlapiLogExecution('DEBUG', 'SCH_Update_Allocation_End_Date', '*** ' + counter_I + ' ***' + ' *** ' +
                    search_Result[counter_I] +
                    ' ***');
                    
                    var i_allocation_ID = search_Result[counter_I]; //.getValue(all_Columns[0]);
                    nlapiLogExecution('DEBUG', 'SCH_Update_Allocation_End_Date', 'i_allocation_ID : ' + i_allocation_ID);
                    
                    if (_is_Valid(i_allocation_ID)) //
                    {
                        //var r_allocation_Rec = nlapiLoadRecord('resourceallocation', i_allocation_ID);
                        
                        //if (_is_Valid(r_allocation_Rec)) //
                        {
                            //r_allocation_Rec.setFieldValue('31/12/2017');
                            
                            try //
                            {
                                //var date = r_allocation_Rec.getFieldValue('enddate'); //'31-12-2017';
                                //nlapiLogExecution('DEBUG', 'SCH_Update_Allocation_End_Date', 'date : ' + date);
                                
                                var date = '12/31/2017';
                                
                                var d_End_Date = nlapiStringToDate(date);
                                //nlapiLogExecution('DEBUG', 'SCH_Update_Allocation_End_Date', 'd_End_Date : ' + d_End_Date);
                                
                                d_End_Date = nlapiDateToString(d_End_Date);
                                //nlapiLogExecution('DEBUG', 'SCH_Update_Allocation_End_Date', 'd_End_Date : ' + d_End_Date);
                                
                                //return;
                                
                                var a_Fields = ['enddate', 'custeventbenddate'];
                                var a_Fields_Values = [d_End_Date, d_End_Date];
                                
                                //var i_submitted = nlapiSubmitField('resourceallocation', i_allocation_ID, 'enddate', d_End_Date);
                                var i_submitted = nlapiSubmitField('resourceallocation', i_allocation_ID, a_Fields, a_Fields_Values);
                                nlapiLogExecution('DEBUG', 'SCH_Update_Allocation_End_Date', 'Record Submitted : ' + i_submitted + ' * Remianing Usage : ' + nlapiGetContext().getRemainingUsage());
                                
                                if (nlapiGetContext().getRemainingUsage() < 100) //
                                {
                                    nlapiYieldScript();
                                }
                            } 
                            catch (ex_1) //
                            {
                                nlapiLogExecution('ERROR', 'SCH_Update_Allocation_End_Date', 'Exception ex_1 : ' + ex_1);
                                nlapiLogExecution('ERROR', 'SCH_Update_Allocation_End_Date', 'Exception ex_1 : ' + ex_1.message);
                            }
                            
                            //return;
                        }
                    }
                }
            }
            else //
            {
                nlapiLogExecution('ERROR', 'SCH_Update_Allocation_End_Date', 'Invalid search_Result : ' + search_Result);
            }
        }
        //while (_is_Valid(search_Result))
        {
        
        }
    } 
    catch (ex) // 
    {
        nlapiLogExecution('ERROR', 'SCH_Update_Allocation_End_Date', 'Exception : ' + ex);
        nlapiLogExecution('ERROR', 'SCH_Update_Allocation_End_Date', 'Exception : ' + ex.message);
    }
}

// END SCHEDULED FUNCTION ===============================================





// BEGIN FUNCTION ===================================================
{

    function _is_Valid(obj) //
    {
        if (obj != null && obj != '' && obj != 'undefined' && obj != undefined)//
        {
            return true;
        }
        else //
        {
            return false;
        }
    }
    
}
// END FUNCTION =====================================================
