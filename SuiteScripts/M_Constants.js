/**
 * Module Description
 * 
 * Version Date Author Remarks 1.00 03 Nov 2015 nitish.mishra
 * 
 */

var M_Constants = {
	Request : {
	    Get : 'GET',
	    Post : 'POST',
	    Approve : 'APPROVE',
	    Reject : 'REJECT',
        GetOne: 'GETONE',
	    GetAll : 'GETALL',
	    Update: 'UPDATE',
      NextWeek: 'NEXTWEEK',
	    Delete: 'DELETE',
      NotSubmitted: 'NOTSUBMITTED',
      PastTimeSheet: 'PASTTIMESHEET',
      Submit: 'SUBMIT',
      Creation : 'CREATION',
      Create : 'CREATE'
          
	}
};