/**
 * @author Jayesh
 */


function afterSubmit()
{
	try
	{
		var projectId = nlapiGetFieldValue('custrecord_project_name_revenue');
		
		var projectData = nlapiLookupField('job', projectId, [
				        'jobbillingtype', 'custentity_t_and_m_monthly',
				        'customer', 'custentity_projectvalue',
				        'custentity_practice', 'startdate', 'enddate',
				        'subsidiary', 'entityid', 'custentity_project_currency' ]);
		
		var projectCurrencyValue = nlapiGetFieldValue('custrecord_currency_revenue');
		var projectCurrencyText = nlapiGetFieldText('custrecord_currency_revenue');
				
		var startDate = '1/1/2016';
		
		var totalRecognizedAmount = getTotalRecognizedAmount(projectId,
		        projectData.subsidiary, projectData.entityid, startDate,
		        projectCurrencyText);
		nlapiLogExecution('debug', 'total recognized value',totalRecognizedAmount);
		//nlapiSubmitField('custrecord_total_recognised_amount',totalRecognizedAmount);
		nlapiSubmitField('customrecord_practice_revenue_empwise',nlapiGetRecordId(),'custrecord_total_recognised_amount',totalRecognizedAmount);
	}
	catch(err)
	{
		nlapiLogExecution('ERROR','ERROR MESSAGE:- ',err);
	}
}


 function getTotalRecognizedAmount(projectId, subsidiary, projectName,
        monthStartDate, projectCurrency)
{
	try {
		var recognizedAmount = 0;

		nlapiLogExecution('debug', 'monthStartDate', monthStartDate);
		nlapiLogExecution('debug', 'projectName', projectName);

		var filters = [
		        new nlobjSearchFilter('formuladate', null, 'before',
		                monthStartDate).setFormula('{trandate}'),
		        new nlobjSearchFilter('custcolprj_name', null, 'startswith',
		                projectName) ];

		// if LLC
		/*if (subsidiary == 2) {
			filters.push(new nlobjSearchFilter('account', null, 'anyof',
			        [ '773' ]));
		} else { // BTPL*/
			filters.push(new nlobjSearchFilter('account', null, 'anyof', [
			        '773', '732' ]));
		//}
		
		if(parseInt(subsidiary) == parseInt(3))
		{
			filters.push(new nlobjSearchFilter('type', null, 'anyof',['Journal','CustInvc' ]));
		}
		else
		{
			filters.push(new nlobjSearchFilter('type', null, 'anyof','Journal'));
		}

		var jeSearch = nlapiSearchRecord('transaction', null, filters, [
		        new nlobjSearchColumn('fxamount', null, 'sum'),
		        new nlobjSearchColumn('currency', null, 'group') ]);

		nlapiLogExecution('debug', 'search data', JSON.stringify(jeSearch));

		if (jeSearch) {

			jeSearch.forEach(function(je) {
				var currency = je.getText('currency', null, 'group');
				var amount = je.getValue('fxamount', null, 'sum');
				
				if(currency == 'GBP')
				{
					amount = parseFloat(amount) * parseFloat(1.33); 
				}
				else if(currency == 'USD')
				{
					amount = parseFloat(amount) * parseFloat(1);
				}
				else if(currency == 'INR')
				{
					amount = parseFloat(amount) * parseFloat(0.0151515151515152);
				}
				
				recognizedAmount = parseFloat(recognizedAmount) + parseFloat(amount);
			});
		}

		return recognizedAmount;
	} catch (err) {
		nlapiLogExecution('ERROR', 'getTotalRecognizedAmount', err);
		throw err;
	}
}