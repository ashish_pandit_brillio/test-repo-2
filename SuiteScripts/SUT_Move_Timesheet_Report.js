/**
 * Timesheet Report For Move
 * 
 * Version Date Author Remarks 1.00 15 Mar 2016 Nitish Mishra
 * 
 */

function suitelet(request, response) {
	try {
		var mode = request.getParameter('mode');

		if (mode == 'CSV') {
			createCsvFile(request);
		} else {
			createGetForm(request);
		}
	} catch (err) {
		nlapiLogExecution('ERROR', 'suitelet', err);
	}
}

function createGetForm(request) {
	try {
		var form = nlapiCreateForm('Timesheet Report');
		form.addFieldGroup('custpage_grp_1', 'Select Details');

		var fromDate = request.getParameter('custpage_fromdate');
		var toDate = request.getParameter('custpage_todate');
		var project = request.getParameter('custpage_project');

		var fromDateField = form.addField('custpage_fromdate', 'date', 'From',
		        null, 'custpage_grp_1');
		fromDateField.setMandatory(true);
		fromDateField.setBreakType('startcol');
		if (fromDate) {
			fromDateField.setDefaultValue(fromDate);
		}

		var toDateField = form.addField('custpage_todate', 'date', 'To', null,
		        'custpage_grp_1');
		toDateField.setBreakType('startcol');
		toDateField.setMandatory(true);
		if (toDate) {
			toDateField.setDefaultValue(toDate);
		}

		var projectField = form.addField('custpage_project', 'select',
		        'Project', null, 'custpage_grp_1');
		projectField.setBreakType('startcol');

		// search all move projects
		var projectSearch = nlapiSearchRecord('job', null, [
		        new nlobjSearchFilter('customer', null, 'anyof', '8381'),
		        new nlobjSearchFilter('isinactive', null, 'is', 'F') ], [
		        new nlobjSearchColumn('entityid'),
		        new nlobjSearchColumn('altname') ]);

		if (projectSearch) {

			for (var i = 0; i < projectSearch.length; i++) {
				projectField.addSelectOption(projectSearch[i].getId(),
				        projectSearch[i].getValue('entityid') + " : "
				                + projectSearch[i].getValue('altname'));
			}
		}
		projectField.addSelectOption('', 'All Move Projects', true);

		// projectField.setMandatory(true);
		// projectField.addSelectOption('-all-', 'All Move Projects');
		if (project) {

			projectField.setDefaultValue(project);
		}

		var htmlText = "";
		if (fromDate && toDate) {
			var masterArray = getTimesheetDetails(fromDate, toDate, project);
			htmlText = generateHtml(masterArray);

			var csvLink = "<a href='/app/site/hosting/scriptlet.nl?script=807&deploy=1&"
			        + "custpage_fromdate="
			        + fromDate
			        + "&custpage_todate="
			        + toDate
			        + "&custpage_project="
			        + project
			        + "&mode=CSV' target='_blank'>"
			        + "<img style='height: 40px; margin-top: 20px;' src='https://system.na1.netsuite.com/core/media/media.nl?id=145877&c=3883006&h=db69121d869e7d0ba460'></a>";

			var csvLinkField = form.addField('custpage_download', 'inlinehtml',
			        '', null, 'custpage_grp_1');
			csvLinkField.setBreakType('startcol');
			csvLinkField.setDefaultValue(csvLink);

		} else {
			htmlText = "Please fill all fields and click refresh";
		}

		form.addFieldGroup('custpage_grp_2', 'Timesheet');
		form.addField('custpage_timesheet_html', 'inlinehtml', '', null,
		        'custpage_grp_2').setDefaultValue(htmlText);

		form.addSubmitButton('Refresh');
		response.writePage(form);
	} catch (err) {
		nlapiLogExecution('ERROR', 'createGetForm', err);
	}
}

function getTimesheetDetails(reportStartDateString, reportEndDateString,
        project)
{
	try {
		var reportStartDate = nlapiStringToDate(reportStartDateString);
		var reportEndDate = nlapiStringToDate(reportEndDateString);
		var filters = [ new nlobjSearchFilter('date', null, 'within',
		        reportStartDateString, reportEndDateString) ];

		var allMoveProjects = [ '@NONE@' ];
		var projectSearch = nlapiSearchRecord('job', null, [
		        new nlobjSearchFilter('customer', null, 'anyof', '8381'),
		        new nlobjSearchFilter('isinactive', null, 'is', 'F') ]);

		if (projectSearch) {

			for (var i = 0; i < projectSearch.length; i++) {
				allMoveProjects.push(projectSearch[i].getId());
			}
		}

		filters.push(new nlobjSearchFilter('customer', null, 'anyof',
		        allMoveProjects));//approvalstatus
		//filters.push(new nlobjSearchFilter('approvalstatus', null, 'anyof',
		        //[2,3]));

		if (project) {
			filters.push(new nlobjSearchFilter('customer', null, 'anyof',
			        project));
		}
		
		

		// creating array with the timesheet data
		var timeEntrySearch = searchRecord('timebill', 'customsearch3443', filters);

		var masterArray = [];
		var employeeList = [ '@NONE@' ];
		var projectList = [ '@NONE@' ];
		var weekDays = [ "Sunday", "Monday", "Tuesday", "Wednesday",
		        "Thursday", "Friday", "Saturday" ];

		if (timeEntrySearch) {

			for (var i = 0; i < timeEntrySearch.length; i++) {
				var timesheetId = timeEntrySearch[i].getValue('timesheet'), timesheetStartDate = timeEntrySearch[i]
				        .getValue('startdate', 'timesheet'), timesheetEndDate = timeEntrySearch[i]
				        .getValue('enddate', 'timesheet'), employeeId = timeEntrySearch[i]
				        .getValue('employee'), employeeName = timeEntrySearch[i]
				        .getText('employee'), taskId = timeEntrySearch[i]
				        .getValue('casetaskevent'), taskName = timeEntrySearch[i]
				        .getText('casetaskevent'), subTaskId = timeEntrySearch[i]
				        .getValue('custcol_te_move_sub_task_tb'), subTaskName = timeEntrySearch[i]
				        .getText('custcol_te_move_sub_task_tb'), durationDecimal = timeEntrySearch[i]
				        .getValue('durationdecimal'), duration = timeEntrySearch[i]
				        .getValue('hours'), timeEntryDateString = timeEntrySearch[i]
				        .getValue('date'), timeEntryDate = nlapiStringToDate(timeEntryDateString), timeEntryDay = weekDays[timeEntryDate
				        .getDay()], projectId = timeEntrySearch[i]
				        .getValue('customer'), projectName = timeEntrySearch[i]
				        .getValue('altname', 'job'), projectCode = timeEntrySearch[i]
				        .getValue('entityid', 'job'), billRate = timeEntrySearch[i]
				        .getValue('rate'), timesheetStatus = timeEntrySearch[i]
				        .getText('approvalstatus', 'timesheet'), approver = timeEntrySearch[i]
				        .getText('timeapprover', 'employee');

				if(projectName.toString() == ''){
						continue;
				}
				
				if(projectName.toString() != ''){
					employeeList.push(employeeId);
					projectList.push(projectId);
				}
				

				var timesheetIndex = -1;
				for (var j = 0; j < masterArray.length; j++) {

					if (masterArray[j].TimesheetId == timesheetId
					        && masterArray[j].TaskId == taskId
					        && masterArray[j].SubTaskId == subTaskId) {
						timesheetIndex = j;
						break;
					}
				}

				if (timesheetIndex == -1) {
					masterArray.push({
					    TimesheetId : timesheetId,
					    TimeEntryDate : timeEntryDate,
					    StartDateString : timesheetStartDate,
					    StartDate : nlapiStringToDate(timesheetStartDate),
					    EndDateString : timesheetEndDate,
					    EndDate : nlapiStringToDate(timesheetEndDate),
					    TaskId : taskId,
					    SubTaskId : subTaskId,
					    TaskName : taskName,
					    SubTaskName : subTaskName,
					    EmployeeId : employeeId,
					    EmployeeName : employeeName,
					    ProjectId : projectId,
					    ProjectCode : projectCode,
					    ProjectName : projectName,
					    BillRate : billRate,
					    Location : '',
					    Level : '',
					    WasAllocationBillable : '',
					    TimeApprover : approver,
					    Status : timesheetStatus,
					    Day : {
					        Sunday : '',
					        Monday : '',
					        Tuesday : '',
					        Wednesday : '',
					        Thursday : '',
					        Friday : '',
					        Saturday : '',
					        Total : 0
					    }
					});

					timesheetIndex = masterArray.length - 1;
				}

				masterArray[timesheetIndex].Day[timeEntryDay] = durationDecimal;
				masterArray[timesheetIndex].Day.Total += parseFloat(durationDecimal);
			}
		}

		nlapiLogExecution('debug', 'masterArray details', JSON
		        .stringify(masterArray));

		var allocationDetails = getAllocationDetails(reportStartDateString,
		        reportEndDateString, _.uniq(employeeList), _.uniq(projectList));

		for (var i = 0; i < masterArray.length; i++) {

			for (var j = 0; j < allocationDetails.length; j++) {

				if (masterArray[i].EmployeeId == allocationDetails[j].EmployeeId
				        && masterArray[i].ProjectId == allocationDetails[j].ProjectId
				        && masterArray[i].TimeEntryDate >= allocationDetails[j].StartDate
				        && masterArray[i].TimeEntryDate <= allocationDetails[j].EndDate) {
					masterArray[i].Level = allocationDetails[j].Role;
					masterArray[i].Location = allocationDetails[j].Location;
					masterArray[i].WasAllocationBillable = allocationDetails[j].WasBillable;
				}
			}
		}

		return masterArray;
	} catch (err) {
		nlapiLogExecution('ERROR', 'getTimesheetDetails', err);
		throw err;
	}
}

function getAllocationDetails(allocationStartDate, allocationEndDate,
        employeeList, projectList)
{
	try {
		var resourceAllocationSearch = nlapiSearchRecord(
		        'resourceallocation',
		        null,
		        [
		                // new nlobjSearchFilter('startdate', null, 'notafter',
		                // allocationEndDate),
		                // new nlobjSearchFilter('enddate', null, 'notbefore',
		                // allocationStartDate),
		                new nlobjSearchFilter('resource', null, 'anyof',
		                        employeeList),
		                new nlobjSearchFilter('project', null, 'anyof',
		                        projectList) ],
		        [ new nlobjSearchColumn('startdate'),
		                new nlobjSearchColumn('enddate'),
		                new nlobjSearchColumn('resource'),
		                new nlobjSearchColumn('company'),
		                new nlobjSearchColumn('custevent4'),
		                new nlobjSearchColumn('custeventrbillable'),
		                new nlobjSearchColumn('custevent_ra_move_billing_role') ]);

		var allocationDetails = [];
		if (resourceAllocationSearch) {

			for (var i = 0; i < resourceAllocationSearch.length; i++) {
				allocationDetails
				        .push({
				            EmployeeId : resourceAllocationSearch[i]
				                    .getValue('resource'),
				            ProjectId : resourceAllocationSearch[i]
				                    .getValue('company'),
				            Role : resourceAllocationSearch[i]
				                    .getText('custevent_ra_move_billing_role'),
				            Location : resourceAllocationSearch[i]
				                    .getText('custevent4'),
				            StartDate : nlapiStringToDate(resourceAllocationSearch[i]
				                    .getValue('startdate')),
				            EndDate : nlapiStringToDate(resourceAllocationSearch[i]
				                    .getValue('enddate')),
				            WasBillable : resourceAllocationSearch[i]
				                    .getValue('custeventrbillable') == 'T' ? 'Yes'
				                    : 'No'
				        });
			}
		}

		nlapiLogExecution('debug', 'allocation details', JSON
		        .stringify(allocationDetails));
		return allocationDetails;
	} catch (err) {
		nlapiLogExecution('ERROR', 'getAllocationDetails', err);
		throw err;
	}
}

function generateHtml(masterArray) {
	try {
		var context = nlapiGetContext();
		if (context.getEnvironment() == "SANDBOX") {
			css_file_url = "https://system.na1.netsuite.com/core/media/media.nl?id=63885&c=3883006&h=1ab6e33882afec4c1d2b&_xt=.css";
		} else if (context.getEnvironment() == "PRODUCTION") {
			css_file_url = "https://system.na1.netsuite.com/core/media/media.nl?id=63885&c=3883006&h=1ab6e33882afec4c1d2b&_xt=.css";
		}

		var htmlText = "";
		htmlText += "<link href='" + css_file_url + "'  rel='stylesheet'>";
		htmlText += "<style> .timesheet-master-table { margin-left: 0px; !important}</style>";

		htmlText += "<table class='timesheet-master-table'>";

		htmlText += "<tr class='header-row'>";
		htmlText += "<td>Start Date</td>";
		htmlText += "<td>End Date</td>";
		htmlText += "<td>Project Code</td>";
		htmlText += "<td>Project Name</td>";
		htmlText += "<td>Location</td>";
		htmlText += "<td>Level</td>";
		htmlText += "<td>Employee Name</td>";
		htmlText += "<td>Is Billable ?</td>";
		htmlText += "<td>Task</td>";
		htmlText += "<td>Sub Task</td>";
		htmlText += "<td>Bill Rate</td>";
		htmlText += "<td>Sun</td>";
		htmlText += "<td>Mon</td>";
		htmlText += "<td>Tue</td>";
		htmlText += "<td>Wed</td>";
		htmlText += "<td>Thu</td>";
		htmlText += "<td>Fri</td>";
		htmlText += "<td>Sat</td>";
		htmlText += "<td>Total</td>";
		htmlText += "<td>Status</td>";
		htmlText += "<td>Time Approver</td>";
		htmlText += "</tr>";

		for (var i = 0; i < masterArray.length; i++) {
			if(masterArray[i].Status != 'Rejected' && masterArray[i].Status != 'Open'){
			htmlText += "<tr class='ts-submitted'>";
			htmlText += "<td>" + masterArray[i].StartDateString + "</td>";
			htmlText += "<td>" + masterArray[i].EndDateString + "</td>";
			htmlText += "<td>" + masterArray[i].ProjectCode + "</td>";
			htmlText += "<td>" + masterArray[i].ProjectName + "</td>";
			htmlText += "<td>" + masterArray[i].Location + "</td>";
			htmlText += "<td>" + masterArray[i].Level + "</td>";
			htmlText += "<td>" + masterArray[i].EmployeeName + "</td>";
			htmlText += "<td>" + masterArray[i].WasAllocationBillable + "</td>";
			htmlText += "<td>" + masterArray[i].TaskName + "</td>";
			htmlText += "<td>" + masterArray[i].SubTaskName + "</td>";
			htmlText += "<td>" + masterArray[i].BillRate + "</td>";
			htmlText += "<td class='ts-approved'>" + masterArray[i].Day.Sunday
			        + "</td>";
			htmlText += "<td class='ts-approved'>" + masterArray[i].Day.Monday
			        + "</td>";
			htmlText += "<td class='ts-approved'>" + masterArray[i].Day.Tuesday
			        + "</td>";
			htmlText += "<td class='ts-approved'>"
			        + masterArray[i].Day.Wednesday + "</td>";
			htmlText += "<td class='ts-approved'>"
			        + masterArray[i].Day.Thursday + "</td>";
			htmlText += "<td class='ts-approved'>" + masterArray[i].Day.Friday
			        + "</td>";
			htmlText += "<td class='ts-approved'>"
			        + masterArray[i].Day.Saturday + "</td>";
			htmlText += "<td class='ts-not-submitted'>"
			        + masterArray[i].Day.Total + "</td>";
			htmlText += "<td>" + masterArray[i].Status + "</td>";
			htmlText += "<td>" + masterArray[i].TimeApprover + "</td>";
			htmlText += "</tr>";
		}
		}
		htmlText += "</table>";

		return htmlText;
	} catch (err) {
		nlapiLogExecution('ERROR', 'generateHtml', err);
		throw err;
	}
}

function createCsvFile(request) {
	try {
		var fromDate = request.getParameter('custpage_fromdate');
		var toDate = request.getParameter('custpage_todate');
		var project = request.getParameter('custpage_project');

		var csvText = "";

		if (fromDate && toDate) {
			var masterArray = getTimesheetDetails(fromDate, toDate, project);
			csvText = generateCsvContent(masterArray);
		} else {
			csvText = "Some fields missing";
		}

		var file = nlapiCreateFile('Timesheet Report.csv', 'CSV', csvText);
		response.setContentType('CSV', 'Timesheet Report.csv');
		response.write(file.getValue());
	} catch (err) {
		nlapiLogExecution('ERROR', 'createCsvFile', err);
	}
}

function generateCsvContent(masterArray) {
	try {
		var csvText = "";

		csvText += "Start Date,";
		csvText += "End Date,";
		csvText += "Project Code,";
		csvText += "Project Name,";
		csvText += "Location,";
		csvText += "Level,";
		csvText += "Employee Name,";
		csvText += "Is Billable,";
		csvText += "Task,";
		csvText += "Sub Task,";
		csvText += "Bill Rate,";
		csvText += "Sun,";
		csvText += "Mon,";
		csvText += "Tue,";
		csvText += "Wed,";
		csvText += "Thu,";
		csvText += "Fri,";
		csvText += "Sat,";
		csvText += "Total,";
		csvText += "Status,";
		csvText += "Approver";
		csvText += "\r\n";

		for (var i = 0; i < masterArray.length; i++) {
			if(masterArray[i].Status != 'Rejected' && masterArray[i].Status != 'Open'){
			csvText += "" + masterArray[i].StartDateString + ",";
			csvText += "" + masterArray[i].EndDateString + ",";
			csvText += "" + masterArray[i].ProjectCode + ",";
			csvText += "" + masterArray[i].ProjectName + ",";
			csvText += "" + masterArray[i].Location + ",";
			csvText += "" + masterArray[i].Level + ",";
			csvText += "" + masterArray[i].EmployeeName + ",";
			csvText += "" + masterArray[i].WasAllocationBillable + ",";
			csvText += "" + masterArray[i].TaskName + ",";
			csvText += "" + masterArray[i].SubTaskName + ",";
			csvText += "" + masterArray[i].BillRate + ",";
			csvText += "" + masterArray[i].Day.Sunday + ",";
			csvText += "" + masterArray[i].Day.Monday + ",";
			csvText += "" + masterArray[i].Day.Tuesday + ",";
			csvText += "" + masterArray[i].Day.Wednesday + ",";
			csvText += "" + masterArray[i].Day.Thursday + ",";
			csvText += "" + masterArray[i].Day.Friday + ",";
			csvText += "" + masterArray[i].Day.Saturday + ",";
			csvText += "" + masterArray[i].Day.Total + ",";
			csvText += "" + masterArray[i].Status + ",";
			csvText += "" + masterArray[i].TimeApprover + ",";
			csvText += "\r\n";
			}
			
		}

		return csvText;
	} catch (err) {
		nlapiLogExecution('ERROR', 'generateCsvContent', err);
		throw err;
	}
}