 /**
 * Module Description
 * User Event script to handle employee record creation and updation from the XML file generated from Fusion
 * 
 * Version                      Date                                                Author                           Remarks
 * 1.00                            18th November, 2015                Manikandan V          This script saves the form data into Survey key Response Master custom record from the Survey.html 
 *
 */


function feedbackSuitelet(request, response)
{
  var method	=request.getMethod();
  if (method == 'GET' )
  {
    var file = nlapiLoadFile(79804);   //load the HTML file
    var contents = file.getValue();    //get the contents
    response.write(contents);     //render it on  suitlet
  
  }
  else
  {
  //var form = nlapiCreateForm("Suitelet - POST call" );
  
  var feedback = new Object();
  feedback['Employee_ID'] = request.getParameter('TextBox1');
  feedback['Email_ID'] = request.getParameter('TextBox2');
  feedback['Employee_Name'] = request.getParameter('TextBox3');
 
  
  

  feedback['Response1'] = request.getParameter('OptionsGroup1');
  feedback['Response2'] = request.getParameter('OptionsGroup2');
  feedback['Response3'] = request.getParameter('OptionsGroup3');
  feedback['Response4'] = request.getParameter('OptionsGroup4');
  feedback['Comments'] = request.getParameter('txtComments');
  
 
  var status = saveRequest(feedback);
   //response.write("Thank You!");
   //response.write("Your survey responses have been recorded!");
    var thanks_note = nlapiLoadFile(50389);   //load the HTML file
    var thanks_contents = thanks_note.getValue();    //get the contents
    response.write(thanks_contents);     //render it on  suitlet
   }

function saveRequest(feedback)
{
	try
	{
               var feedback_form = nlapiCreateRecord('customrecord_employee_feedback_form');
               feedback_form.setFieldValue('custrecord_employee_id_us', feedback.Employee_ID);
	           feedback_form.setFieldValue('custrecord_email_id_us', feedback.Email_ID);
			   feedback_form.setFieldValue('custrecord_employee_name_us', feedback.Employee_Name);
               
			    
			   
			   feedback_form.setFieldValue('custrecord_quest_01',"How helpful was this orientation presentation for you?");
               feedback_form.setFieldValue('custrecord_quest_02', "How do you rate the content of the presentation?");
               feedback_form.setFieldValue('custrecord_quest_03', "How do you rate the knowledge of facilitator?");
			   feedback_form.setFieldValue('custrecord_quest_04', "Was the facilitator able to answer your questions?");
			   
             


               feedback_form.setFieldValue('custrecord_answ_1', feedback.Response1);
               feedback_form.setFieldValue('custrecord_answ_2', feedback.Response2);
               feedback_form.setFieldValue('custrecord_answ_3', feedback.Response3);
			   feedback_form.setFieldValue('custrecord_answ_4', feedback.Response4);
               feedback_form.setFieldValue('custrecord_notes1', feedback.Comments);
			
               
     
              
               var id = nlapiSubmitRecord(feedback_form, false,true);
               nlapiLogExecution('debug', 'Record Saved', id);
        }
catch(err)
{
nlapiLogExecution('error', 'Record not saved', err);
feedback_form.setFieldValue('custrecord_error_log_ops','');
}
}
 }