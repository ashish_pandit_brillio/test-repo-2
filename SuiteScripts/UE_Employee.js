/**
 * Contains functions to be executed for an employee record
 * 
 * Version Date Author Remarks 1.00 Apr 22 2015 Nitish Mishra
 * 
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your
 * script deployment.
 * 
 * @appliedtorecord recordType
 * 
 * @param {String}
 *        type Operation types: create, edit, view, copy, print, email
 * @param {nlobjForm}
 *        form Current form
 * @param {nlobjRequest}
 *        request Request object
 * @returns {Void}
 */
function userEventBeforeLoad(type, form, request) {

}

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your
 * script deployment.
 * 
 * @appliedtorecord recordType
 * 
 * @param {String}
 *        type Operation types: create, edit, delete, xedit approve, reject,
 *        cancel (SO, ER, Time Bill, PO & RMA only) pack, ship (IF) markcomplete
 *        (Call, Task) reassign (Case) editforecast (Opp, Estimate)
 * @returns {Void}
 */
function userEventBeforeSubmit(type) {

}

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your
 * script deployment.
 * 
 * @appliedtorecord recordType
 * 
 * @param {String}
 *        type Operation types: create, edit, delete, xedit, approve, cancel,
 *        reject (SO, ER, Time Bill, PO & RMA only) pack, ship (IF only)
 *        dropship, specialorder, orderitems (PO only) paybills (vendor
 *        payments)
 * @returns {Void}
 */
function userEventAfterSubmit(type) {

	try {

		if (type == 'edit') {

			var oldRecord = nlapiGetOldRecord();

			// notify the user about any changes in the email id
			notifyEmailIdChange(oldRecord);
		}
	}
	catch (err) {
		nlapiLogExecution('ERROR', 'userEventAfterSubmit', err);
	}
}

function notifyEmailIdChange(oldRecord) {

	try {
		var oldEmail = oldRecord.getFieldValue('email');
		var newEmail = nlapiGetFieldValue('email');

		if (oldEmail != newEmail) {

			// notify user about the email id change
			var mailContent =
					getEmailIdChangedMailTemplate(nlapiGetFieldValue('firstname'), oldEmail,
							newEmail);

			nlapiSendEmail(constant.Mail_Author.InformationSystems, newEmail, mailContent.Subject,
					mailContent.Body, oldEmail, null, {
						entity : oldRecord.getId()
					});
		}
	}
	catch (err) {
		nlapiLogExecution('ERROR', 'notifyEmailIdChange', err);
		throw err;
	}
}

function getEmailIdChangedMailTemplate(firstName, oldEmail, newEmail) {

	var htmlText = '<style> p,li {font-size : 11; font-family : "verdana"}</style>';

	htmlText += '<p>Hi ' + firstName + ',<p>';
	htmlText +=	'<p><b>This is to inform you that your email id has been changed in the NetSuite system.</b></p>';
	htmlText +=	"<p><u><b>Brillio Email ID Features</b></u></p>"
					+ "<p>Your Brillio email ID and password will be your single access key for all of Brillio Systems (Netsuite, Fusion, Yammer, etc.)."
					+ " It is also the only place where you will receive all official communication and announcements from now on."
					+ " Please keep checking your Brillio email a few times per week.</p>"
					+ "<p><b>NetSuite Access Instructions</b></p>"
					+ "<p>Please follow the below instructions to access your NetSuite</p>"
					+ "<p>Going forward use the below link to Login to NetSuite. We also recommend you use Google Chrome/Firefox to login into NetSuite.<p>"
					+ '<p>1. Login to Brillio SharePoint - <a href="https://brillioonline.sharepoint.com">https://brillioonline.sharepoint.com</a><br/>'
					+ '2. Proceed with windows credentials<br/>'
					+ '3. Click on <b>NetSuite</b> icon on the top-right corner of the screen.'
					+ '<br/><br/><i>Note:- If you are not able to login using the above link,'
					+ ' go to <a href="https:// sts.brillio.com/adfs/ls/IdpInitiatedSignOn.aspx">https:// sts.brillio.com/adfs/ls/IdpInitiatedSignOn.aspx</a> '
					+ 'and follow the instructions.</i><br/><br/>'
					+ '4. Select <b>NetSuite</b> from the drop-down list.<br/>'
					+ '5. Click on <b>Go</b> button and you will be redirected to your dashboard.<br/></p>'
					+ '<p>Login mail id : '
					+ "<a href='"
					+ newEmail
					+ "'>"
					+newEmail
					+ "</a>"
					+ "<br/>"
					+ 'Password : Your outlook mail password</p>'
					+ "<p><b>Further Assistance</b></p>"
					+ "<p>1)      For help with your NetSuite Access reach out to  <a href='mailto:information.systems@brillio.com'>information.systems@brillio.com</a><br/>"
					+ "2)      For help with email ID reach out to <a href='mailto:helpdeskit@brillio.com'>helpdeskit@brillio.com</a> or call 2014770984<br/>"
					+ '<p>Regards,<br/>' + 'Information Systems</p>';

	return {
		Subject : 'Your Email Id has been changed',
		Body : addMailTemplate(htmlText)
	};
}