/**
 * Contains button action to call a restlet to autofill the user timesheet for
 * that week and reload the page
 * 
 * Version Date Author Remarks 1.00 Apr 21 2015 Nitish Mishra
 * 
 */

// client script - button click event to call a restlet to generate the
// Time Sheet for the user for the current week
function autoFill() {

	try {
		// document.getElementById('img_autofill_progress').style.display =
		// 'inline';

		// call the RESTlet to create a Time Sheet record for that week
		var dataReceived = nlapiRequestURL("/app/site/hosting/scriptlet.nl?script=551&deploy=1&startdate="
				+ nlapiGetFieldValue('startdate')
				+ "&employee="
				+ nlapiGetFieldValue('employee'));

		dataReceived = dataReceived.getBody();
		dataReceived = JSON.parse(dataReceived);

		if (dataReceived.Status) {
			var url = nlapiResolveURL('RECORD', 'timesheet',
					dataReceived.Message, 'EDIT');
			window.location = url;
		} else {
			// document.getElementById('img_autofill_progess').style.display =
			// 'none';
			alert(dataReceived.Message);
		}
	} catch (err) {
		// document.getElementById('img_autofill_progess').style.display =
		// 'none';
		nlapiLogExecution('ERROR', 'autoFill', err);
		alert(JSON.stringify(err));
	}

}

function replaceAll(text, a, b) {

	var i = text.indexOf(a);
	do {
		i = text.indexOf(a);
		text = text.replace(a, b);
	} while (i != -1);

	return text;
}

// add the autofill button to the timesheet
function beforeLoad(type, form) {

	if (type == 'create') {
		form.addButton('custpage_autofill_ts', 'AutoFill', 'autoFill');
	}
}