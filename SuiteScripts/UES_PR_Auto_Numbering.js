/**
 * @author Shweta
 */
// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
/*
   	Script Name: UES_PR_Auto_Numbering.js
	Author     : Shweta Chopde
	Company    : Aashna Cloudtech
	Date       : 10 April 2014
	Description: Generate the auto - number ID for Purchase Request


	Script Modification Log:

	-- Date --			-- Modified By --				--Requested By--				-- Description --



Below is a summary of the process controls enforced by this script file.  The control logic is described
more fully, below, in the appropriate function headers and code blocks.


     BEFORE LOAD
		- beforeLoadRecord(type)



     BEFORE SUBMIT
		- beforeSubmitRecord(type)


     AFTER SUBMIT
		- afterSubmitRecord(type)



     SUB-FUNCTIONS
		- The following sub-functions are called by the above core functions in order to maintain code
            modularization:

               - NOT USED

*/
}
// END SCRIPT DESCRIPTION BLOCK  ====================================



// BEGIN GLOBAL VARIABLE BLOCK  =====================================
{
	//  Initialize any Global Variables, in particular, debugging variables...




}
// END GLOBAL VARIABLE BLOCK  =======================================





// BEGIN BEFORE LOAD ==================================================

function beforeLoadRecord(type)
{

	/*  On before load:

          - EXPLAIN THE PURPOSE OF THIS FUNCTION

		-


		FIELDS USED:

          --Field Name--				--ID--


	*/



	//  LOCAL VARIABLES

    //  BEFORE LOAD CODE BODY


	return true;



}

// END BEFORE LOAD ====================================================





// BEGIN BEFORE SUBMIT ================================================

function beforeSubmitRecord(type)
{
    /*  On before submit:

          - PURPOSE
		-

          FIELDS USED:

          --Field Name--				--ID--


    */


    //  LOCAL VARIABLES


    //  BEFORE SUBMIT CODE BODY


	return true;

}

// END BEFORE SUBMIT ==================================================





// BEGIN AFTER SUBMIT =============================================
/**
 * 
 * @param {Object} type
 * 
 * Description --> Get all the details as Record ID , Subsidiary & current sequence , 
 *                 depending on the subsidiary generates  the Auto Number 
 */
function afterSubmit_auto_numbering(type)
{
	var i_subsidiary;
	var s_prefix =''
	var i_sequence_number=''
	var i_zero_number =0
	var a_serial_number_array;
	var o_serial_numberOBJ;
	var i_serial_no_ID;
	
	if(type == 'create')
	{
		try
		{
			var i_recordID = nlapiGetRecordId()
			
			var s_record_type = nlapiGetRecordType()
			
			var o_recordOBJ = nlapiLoadRecord(s_record_type,i_recordID)
			
			if(_logValidation(o_recordOBJ))
			{
			  var i_line_item_count = o_recordOBJ.getLineItemCount('recmachcustrecord_purchaserequest')
			 			
			  if(_logValidation(i_line_item_count))
			  {
			  	i_subsidiary = o_recordOBJ.getLineItemValue('recmachcustrecord_purchaserequest','custrecord_subsidiaryforpr',1)
								
			  }//Line Count
			  
			  var a_split_array = new Array()
			 
			  if(i_subsidiary == 2)
			  {
			  	s_prefix = 25
				a_serial_number_array = search_serial_number_generation_US()
				
				if(_logValidation(a_serial_number_array))
				{
				  	a_split_array = a_serial_number_array[0].split('###')
					
					i_sequence_number = a_split_array[0]
					
					i_serial_no_ID = a_split_array[1]
				
				}//a_serial_number_array
							
			  }// US Subsidiary			  
			  if(i_subsidiary == 3 || i_subsidiary == 9)
			  {
			  	s_prefix = 15
				a_serial_number_array =   search_serial_number_generation_INDIA()
				
				if(_logValidation(a_serial_number_array))
				{
				  	a_split_array = a_serial_number_array[0].split('###')
					
					i_sequence_number = a_split_array[0]
					
					i_serial_no_ID = a_split_array[1]
				
				}//a_serial_number_array
			  }// INDIA Subsidiary	
			 //================= Code Added for CANADA ===============================
			 if(i_subsidiary == 10)
			  {
			  	s_prefix = 35
				a_serial_number_array =   search_serial_number_generation_CANADA()
				
				if(_logValidation(a_serial_number_array))
				{
				  	a_split_array = a_serial_number_array[0].split('###')
					
					i_sequence_number = a_split_array[0]
					
					i_serial_no_ID = a_split_array[1]
				
				}//a_serial_number_array
			  }// CANADA Subsidiary	
			 //================================================
			  if(_logValidation(i_serial_no_ID))
			  {
			  	o_serial_numberOBJ =  nlapiLoadRecord('customrecord_serial_number_generation',i_serial_no_ID)
			  }
			  var i_next_sequence_no = parseInt(i_sequence_number)+parseInt(1)
			 
			  if(i_next_sequence_no.toString().length == 1)
			  {
			  	i_zero_number = '0000'				
			  }
              if(i_next_sequence_no.toString().length == 2)
			  {
			  	i_zero_number = '000'				
			  }
			   if(i_next_sequence_no.toString().length == 3)
			  {
			  	i_zero_number = '00'				
			  }
			  if(i_next_sequence_no.toString().length == 4)
			  {
			  	i_zero_number = '0'				
			  }
			   if(i_next_sequence_no.toString().length == 5)
			  {
			  	i_zero_number = ''	
			  }
			  nlapiLogExecution('DEBUG', 'afterSubmit_auto_numbering',' i_zero_number -->' + i_zero_number);
			 
			  var i_auto_numberID = s_prefix+''+i_zero_number.toString()+i_next_sequence_no.toString()
			  nlapiLogExecution('DEBUG', 'afterSubmit_auto_numbering',' ****************** Auto Number ID ***************** -->' + i_auto_numberID);
			  	
               if(i_subsidiary == 3)
			  {
			   o_serial_numberOBJ.setFieldValue('custrecord_india_sequence_number',i_next_sequence_no.toString())
			   o_serial_numberOBJ.setFieldValue('custrecord_india_serial_number',i_auto_numberID)
			  }
			  if(i_subsidiary == 2)
			  {
			  	o_serial_numberOBJ.setFieldValue('custrecord_us_sequence_number',i_next_sequence_no.toString())
			    o_serial_numberOBJ.setFieldValue('custrecord_us_serial_number',i_auto_numberID)
			  }	
			  //==================Canada===========================
			  if(i_subsidiary == 10)
			  {
			  	o_serial_numberOBJ.setFieldValue('custrecord_canada_sequence_number',i_next_sequence_no.toString())
			    o_serial_numberOBJ.setFieldValue('custrecord_canada_serial_number',i_auto_numberID)
			  }				  
			  //============================================
			   o_recordOBJ.setFieldValue('custrecord_prno',i_auto_numberID)
			   
			   
			   // ========================== PR - Item ===============================
			   
			   
			   var i_PR_Line_count = o_recordOBJ.getLineItemCount('recmachcustrecord_purchaserequest') 
			   nlapiLogExecution('DEBUG', 'afterSubmit_auto_numbering',' PR Line Count -->' + i_PR_Line_count);
			  
			   	 if (_logValidation(i_PR_Line_count)) 
				 {
				 	for (var i = 1; i <= i_PR_Line_count; i++) 
					{
				 		var i_SR_No = o_recordOBJ.getLineItemValue('recmachcustrecord_purchaserequest', 'custrecord_prlineitemno', i)
				 		
				 		var i_item = o_recordOBJ.getLineItemValue('recmachcustrecord_purchaserequest', 'custrecord_prmatgrpcategory', i)
				 	 
					    o_recordOBJ.setLineItemValue('recmachcustrecord_purchaserequest', 'custrecord_prno1', i,i_auto_numberID)
				 	 
					 
					     var i_PR_ID = search_PR_Item_recordID_1(i_recordID,i_SR_No,i_item)
						 
						  if (_logValidation(i_PR_ID)) 
						 {
						 	var o_PR_OBJ_1 = nlapiLoadRecord('customrecord_pritem',i_PR_ID)
							
							 if(_logValidation(o_PR_OBJ_1)) 
							 {							 	
								o_PR_OBJ_1.setFieldValue('custrecord_prno1',i_auto_numberID);						
								
								var i_PR_submitID = nlapiSubmitRecord(o_PR_OBJ_1,true,true)
							    nlapiLogExecution('DEBUG', 'afterSubmit_Item_Data_Sourcing',' ************* PR Submit ID ********** -->' + i_PR_submitID);	
			  
							 }
							
						 }//PR ID	
					
					}
				 }
			  
			  
			   
			   
			   
			   
			   
			   
			   
			   
			   
			   
			   var i_submitID = nlapiSubmitRecord(o_recordOBJ, true,true)
			   var i_serial_no_submitID = nlapiSubmitRecord(o_serial_numberOBJ, true,true)
			   
			   nlapiLogExecution('DEBUG', 'afterSubmit_auto_numbering',' ****************** PR Submit ID ***************** -->' + i_submitID);
			  
		       nlapiLogExecution('DEBUG', 'afterSubmit_auto_numbering',' ****************** Serial No Submit ID  ***************** -->' + i_serial_no_submitID);
			  			  
			}//Record Object			
			
		}//TRY
	    catch(exception)
		{
			 nlapiLogExecution('DEBUG', 'ERROR',' Exception Caught -->' + exception);
		}//CATCH
		
	}//CREATE
	
	return true;
}

// END AFTER SUBMIT ===============================================


// BEGIN FUNCTION ===================================================
/**
 * 
 * @param {Object} value
 * 
 * Description --> If the value is blank /null/undefined returns false else returns true
 */
function _logValidation(value) 
{
 if(value != null && value.toString() != null && value != '' && value != undefined && value.toString() != undefined && value != 'undefined' && value.toString() != 'undefined'&& value.toString() != 'NaN' && value != NaN) 
 {
  return true;
 }
 else 
 { 
  return false;
 }
}
/**
 * Description --> Search for custom record Serial Number Generation  for the criteria as country India & 
 *                 Record Type as Purchase Request & returns the sequence number found
 * 
 */
function search_serial_number_generation_INDIA()
{ 
   var i_sequence_number;
   var o_serialOBJ;
   var a_return_array = new Array()
   var i_internal_id;
   var filter = new Array();
   filter[0] = new nlobjSearchFilter('custrecord_purchase_request_type', null, 'contains', 'PR - Purchase Request');
   filter[0] = new nlobjSearchFilter('custrecord_country', null, 'is',1);
	
   var columns = new Array();
   columns[0] = new nlobjSearchColumn('internalid');
   columns[1] = new nlobjSearchColumn('custrecord_india_sequence_number');
   
   var a_seq_searchresults = nlapiSearchRecord('customrecord_serial_number_generation',null,filter,columns);
   
   if(a_seq_searchresults!=null && a_seq_searchresults!='' && a_seq_searchresults!=undefined)
   {    	
		i_internal_id = a_seq_searchresults[0].getValue('internalid');
	   		
		if(_logValidation(i_internal_id))
		{
			o_serialOBJ = nlapiLoadRecord('customrecord_serial_number_generation',i_internal_id)
			
			if(_logValidation(o_serialOBJ))
			{					
			  i_sequence_number = o_serialOBJ.getFieldValue('custrecord_india_sequence_number');	             
			}
			
		}//Internal ID		
						 				
   }//Search Results	
	a_return_array[0] = i_sequence_number +'###'+ i_internal_id
		
	return a_return_array;	
}
/**
 * 
 * Description --> Search for custom record Serial Number Generation  for the criteria as country US & 
 *                 Record Type as Purchase Request & returns the sequence number found
 */
function search_serial_number_generation_US()
{ 
   var i_sequence_number;
   var a_return_array = new Array()
   var filter = new Array();
   var o_serialOBJ
   var i_internal_id;
   
   filter[0] = new nlobjSearchFilter('custrecord_purchase_request_type', null, 'contains', 'PR - Purchase Request');
   filter[0] = new nlobjSearchFilter('custrecord_country', null, 'is',2);
	
   var columns = new Array();
   columns[0] = new nlobjSearchColumn('internalid');
   columns[1] = new nlobjSearchColumn('custrecord_us_sequence_number');
   
   var a_seq_searchresults = nlapiSearchRecord('customrecord_serial_number_generation',null,filter,columns);
   
   if(a_seq_searchresults!=null && a_seq_searchresults!='' && a_seq_searchresults!=undefined)
   {					 
     	var i_internal_id = a_seq_searchresults[0].getValue('internalid');
	   		
		if(_logValidation(i_internal_id))
		{
			o_serialOBJ = nlapiLoadRecord('customrecord_serial_number_generation',i_internal_id)
			
				if(_logValidation(o_serialOBJ))
				{					
				  i_sequence_number = o_serialOBJ.getFieldValue('custrecord_us_sequence_number');	              
				}			
			
		}//Internal ID		
						 				
   }//Search Results	
	a_return_array[0] = i_sequence_number +'###'+ i_internal_id
		
	return a_return_array;	
					 				
}//Search Results	
		
	
/**
 * 
 * @param {Object} i_recordID
 * @param {Object} i_SR_No_QA
 * @param {Object} i_item_QA
 * 
 * Description --> For the criteria Item, Serial No & Purchase Request search the PR Ã¢â‚¬â€œ Item record 
 *                 & returns the PR Ã¢â‚¬â€œ Item internal ID
 */
function search_PR_Item_recordID_1(i_recordID,i_SR_No_QA,i_item_QA)
{  
   nlapiLogExecution('DEBUG', 'search_PR_Item_recordID', 'i_recordID ==' +i_recordID);
   nlapiLogExecution('DEBUG', 'search_PR_Item_recordID', 'i_SR_No_QA ==' +i_SR_No_QA);
   nlapiLogExecution('DEBUG', 'search_PR_Item_recordID', 'i_item_QA ==' +i_item_QA);
	   	
   var i_internal_id;
   
   var filter = new Array();
   filter[0] = new nlobjSearchFilter('custrecord_purchaserequest', null, 'is',i_recordID);
   filter[1] = new nlobjSearchFilter('custrecord_prlineitemno', null, 'is',parseInt(i_SR_No_QA));
   filter[2] = new nlobjSearchFilter('custrecord_prmatgrpcategory', null, 'is',i_item_QA);
	
   var columns = new Array();
   columns[0] = new nlobjSearchColumn('internalid');
   columns[1] = new nlobjSearchColumn('custrecord_purchaserequest');
   columns[2] = new nlobjSearchColumn('custrecord_prlineitemno');
   columns[3] = new nlobjSearchColumn('custrecord_prmatgrpcategory');
   
   
   var a_seq_searchresults = nlapiSearchRecord('customrecord_pritem',null,filter,columns);
	
	if (a_seq_searchresults != null && a_seq_searchresults != '' && a_seq_searchresults != undefined)
	{	
	 nlapiLogExecution('DEBUG', 'search_PR_Item_recordID', 'a_seq_searchresults ==' +a_seq_searchresults.length);
	 
	 for(var ty=0;ty<a_seq_searchresults.length;ty++)
	 {
	 	i_internal_id = a_seq_searchresults[ty].getValue('internalid');
		nlapiLogExecution('DEBUG', 'search_PR_Item_recordID', ' Internal ID -->' + i_internal_id);
		
		var i_PR = a_seq_searchresults[ty].getValue('custrecord_purchaserequest');
		nlapiLogExecution('DEBUG', 'search_PR_Item_recordID', ' i_PR ID -->' + i_PR);
		
		var i_line_no = a_seq_searchresults[ty].getValue('custrecord_prlineitemno');
		nlapiLogExecution('DEBUG', 'search_PR_Item_recordID', ' i_line_no -->' + i_line_no);
		
		var i_item = a_seq_searchresults[ty].getValue('custrecord_prmatgrpcategory');
		nlapiLogExecution('DEBUG', 'search_PR_Item_recordID', ' i_item-->' + i_item);
		
		if((i_PR == i_recordID)&&(i_line_no == i_SR_No_QA)&&(i_item == i_item_QA))
		{
			i_internal_id = i_internal_id
			break
		}
		
	 }//LOOP
	 
		
		
	}
	
	return i_internal_id;
}	
function search_serial_number_generation_CANADA()
{ 
   var i_sequence_number;
   var o_serialOBJ;
   var a_return_array = new Array()
   var i_internal_id;
   var filter = new Array();
   filter[0] = new nlobjSearchFilter('custrecord_purchase_request_type', null, 'contains', 'PR - Purchase Request');
   filter[0] = new nlobjSearchFilter('custrecord_country', null, 'is',34);
	
   var columns = new Array();
   columns[0] = new nlobjSearchColumn('internalid');
   columns[1] = new nlobjSearchColumn('custrecord_canada_sequence_number');
   
   var a_seq_searchresults = nlapiSearchRecord('customrecord_serial_number_generation',null,filter,columns);
   
   if(a_seq_searchresults!=null && a_seq_searchresults!='' && a_seq_searchresults!=undefined)
   {    	
		i_internal_id = a_seq_searchresults[0].getValue('internalid');
	   		
		if(_logValidation(i_internal_id))
		{
			o_serialOBJ = nlapiLoadRecord('customrecord_serial_number_generation',i_internal_id)
			
			if(_logValidation(o_serialOBJ))
			{					
			  i_sequence_number = o_serialOBJ.getFieldValue('custrecord_canada_sequence_number');	             
			}
			
		}//Internal ID		
						 				
   }//Search Results	
	a_return_array[0] = i_sequence_number +'###'+ i_internal_id
		
	return a_return_array;	
}			
// END FUNCTION =====================================================