// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
    /*
     Script Name:	CLI_ST_VALIDATE_40_WEEKLY
     Author:			Vikrant
     Company:		Aashna
     Date:			02-09-2014
     Description:	Validation for timesheet, should contain on 40 hrs for ST service items for a week
     Script Modification Log:
     -- Date --			-- Modified By --				--Requested By--				-- Description --
     Below is a summary of the process controls enforced by this script file.  The control logic is described
     more fully, below, in the appropriate function headers and code blocks.
     PAGE INIT
     - pageInit(type)
     SAVE RECORD
     - saveRecord()
     VALIDATE FIELD
     - validateField(type, name, linenum)
     FIELD CHANGED
     - fieldChanged(type, name, linenum)
     POST SOURCING
     - postSourcing(type, name)
     LINE INIT
     - lineInit(type)
     VALIDATE LINE
     - validateLine()
     RECALC
     - reCalc()
     SUB-FUNCTIONS
     - The following sub-functions are called by the above core functions in order to maintain code
     modularization:
     */
}
// END SCRIPT DESCRIPTION BLOCK  ====================================



// BEGIN SCRIPT UPDATION BLOCK  ====================================
/*

 */

// END SCRIPT UPDATION BLOCK  ====================================




// BEGIN GLOBAL VARIABLE BLOCK  =====================================
{

    //  Initialize any Global Variables, in particular, debugging variables...


}
// END GLOBAL VARIABLE BLOCK  =======================================





// BEGIN PAGE INIT ==================================================

function pageInit(type) //
{
    /*  On page init:
    
     
    
     - PURPOSE
    
     
    
     FIELDS USED:
    
     
    
     --Field Name--				--ID--			--Line Item Name--
    
     
    
     */
    
    //  LOCAL VARIABLES


    //  PAGE INIT CODE BODY


}

// END PAGE INIT ====================================================





// BEGIN SAVE RECORD ================================================

function saveRecord_val_st_hrs_weekly() //
{
    /*  On save record:
     - PURPOSE
     FIELDS USED:
     --Field Name--			--ID--		--Line Item Name--
     */
    //  LOCAL VARIABLES
    
    
    //  SAVE RECORD CODE BODY
    
    try //
    {
        // get line count for the Enter time tab	
		var a = new Array();
        a['User-Agent-x'] = 'SuiteScript-Call';
		var resposeObject='';
		
		if(g_hrs == false) //
		{
			//nlapiLogExecution('DEBUG', 'saveRecord_val_st_hrs_weekly', 'g_hrs : ' + g_hrs);
			return false;
		} 
        
        var emp_ID = nlapiGetFieldValue('employee');
        //nlapiLogExecution('DEBUG', 'saveRecord_val_st_hrs_weekly', 'emp_ID : ' + emp_ID);
        
        var weekly = nlapiGetFieldValue('weekly');
        nlapiLogExecution('DEBUG', 'saveRecord_val_st_hrs_weekly', 'weekly : ' + weekly);
        
        var F_date = nlapiGetFieldValue('trandate');
        //nlapiLogExecution('DEBUG', 'saveRecord_val_st_hrs_weekly', 'F_date : ' + F_date);
        
		F_date = nlapiStringToDate(F_date);
		
        var T_date = nlapiAddDays(new Date(F_date), 6);
        //nlapiLogExecution('DEBUG', 'saveRecord_val_st_hrs_weekly', 'after days addition T_date : ' + T_date);
        
        T_date = nlapiDateToString(T_date);
        //nlapiLogExecution('DEBUG', 'saveRecord_val_st_hrs_weekly', 'After parsing T_date : ' + T_date);
		
		F_date = nlapiDateToString(F_date);
        
        var line_count = nlapiGetLineItemCount('timeitem');
        //nlapiLogExecution('DEBUG', 'saveRecord_val_st_hrs_weekly', 'line_count : ' + line_count);
        
        if (_validate(weekly)) //
        {
            if (weekly == 'T') // 
            {
                for (var i = 1; i <= line_count; i++) //
                {
                    var item = nlapiGetLineItemValue('timeitem', 'item', i);
                    nlapiLogExecution('DEBUG', 'saveRecord_val_st_hrs_weekly', 'item : ' + item);
                    
                    var project = nlapiGetLineItemValue('timeitem', 'customer', i);
                    //nlapiLogExecution('DEBUG', 'saveRecord_val_st_hrs_weekly', 'project : ' + project);
                    
                    var casetaskevent = nlapiGetLineItemValue('timeitem', 'casetaskevent', i);
                    //nlapiLogExecution('DEBUG', 'saveRecord_val_st_hrs_weekly', 'casetaskevent : ' + casetaskevent);
                    
                    var project_name = '';
                    var custentity_otapplicable = false;
                    var limit_hrs_weekly = null;
                    
                    if (_validate(project)) //  
					{
						// fetch the values of the weekly limit and OT applicable or not.
						resposeObject = nlapiRequestURL('/app/site/hosting/scriptlet.nl?script=289&deploy=1&custscript_pro_id=' + project 
						+ '&custscript_req_type=project', null, a);
						var status = resposeObject.getBody();
						
						//var project_rec = nlapiLoadRecord('job', project);
						var project_rec = status;
						//nlapiLogExecution('DEBUG', 'saveRecord_val_st_hrs_weekly', 'project_rec : ' + project_rec);
						
						if (_validate(project_rec)) //
						{
							var resposeObject = nlapiRequestURL('/app/site/hosting/scriptlet.nl?script=289&deploy=1&custscript_pro_id=' + project +
							'&custscript_req_type=custentity_hoursperweek', null, a);
							var status = resposeObject.getBody();
							
							var limit = status; //project_rec.getFieldValue('custentity_hoursperweek');
							//nlapiLogExecution('DEBUG', 'saveRecord_val_st_hrs_weekly', 'limit : ' + limit);
							
							if (_validate(limit)) //
							{
								limit_hrs_weekly = limit;
							}
							
							var resposeObject = nlapiRequestURL('/app/site/hosting/scriptlet.nl?script=289&deploy=1&custscript_pro_id=' + project +
							'&custscript_req_type=custentity_otapplicable', null, a);
							var status = resposeObject.getBody();
							
							custentity_otapplicable = status; // project_rec.getFieldValue('custentity_otapplicable'); // get value of check box that determines OT hrs appowed or not.
							//nlapiLogExecution('DEBUG', 'saveRecord_val_st_hrs_weekly', 'custentity_otapplicable : ' + custentity_otapplicable);
							
							var resposeObject = nlapiRequestURL('/app/site/hosting/scriptlet.nl?script=289&deploy=1&custscript_pro_id=' + project +
							'&custscript_req_type=companyname', null, a);
							var status = resposeObject.getBody();
							
							project_name = status; //project_rec.getFieldValue('companyname');
							//nlapiLogExecution('DEBUG', 'saveRecord_val_st_hrs_weekly', 'project_name : ' + project_name);
							
						//nlapiLogExecution('DEBUG', 'saveRecord_val_st_hrs_weekly', 'limit_hrs_weekly : ' + limit_hrs_weekly);
						}
						else //
						{
						//nlapiLogExecution('DEBUG', 'saveRecord_val_st_hrs_weekly', 'project_rec is not valid : ' + project_rec);
						}
					}
					else //
					{
					//nlapiLogExecution('DEBUG', 'saveRecord_val_st_hrs_weekly', 'project_rec is not valid : ' + project_rec);
					}
                    //nlapiLogExecution('DEBUG', 'saveRecord_val_st_hrs_weekly', 'final project : ' + cust_ID);
                    
					var resposeObject = nlapiRequestURL('/app/site/hosting/scriptlet.nl?script=289&deploy=1&custscript_pro_id=' + project +
					'&custscript_req_type=st_task', null, a);
					var status = resposeObject.getBody();
                    var st_task_ID = status; //get_ST_Task_ID(project);
                    
                    if (_validate(st_task_ID)) // 
                    {
                        // if ST task is not present on project
                        //nlapiLogExecution('DEBUG', 'saveRecord_val_st_hrs_weekly', 'ST task is not present for the project ' + project_name);
                        
                        //if (item == '2222') // 2222 is for ST item
                        if (limit_hrs_weekly != null) // validate for ST Task only
                        {
                            var total_hrs = nlapiGetLineItemValue('timeitem', 'hourstotal', i); // this is total of current time entered on the form...
                            //nlapiLogExecution('DEBUG', 'saveRecord_val_st_hrs_weekly', 'total_hrs : ' + total_hrs);
                            
                            if (_validate(total_hrs) == false) //
                            {
                                total_hrs = 0;
                                //nlapiLogExecution('DEBUG', 'saveRecord_val_st_hrs_weekly', 'total_hrs : ' + total_hrs);
                            }
                            else //
                            {
                                total_hrs = _correct_time(total_hrs);
                            }
                            //nlapiLogExecution('DEBUG', 'saveRecord_val_st_hrs_weekly', 'Final total_hrs : ' + total_hrs);
                            
                            var filters = new Array();
                            filters[filters.length] = new nlobjSearchFilter('date', null, 'onorafter', F_date); // on selected field date only
                            filters[filters.length] = new nlobjSearchFilter('date', null, 'onorbefore', T_date); // on selected field date only
                            filters[filters.length] = new nlobjSearchFilter('employee', null, 'anyof', emp_ID); // for current employee only
                            filters[filters.length] = new nlobjSearchFilter('customer', null, 'anyof', project); // for current project only
                            filters[filters.length] = new nlobjSearchFilter('internalid', 'projecttask', 'anyof', st_task_ID); // for ST project task only
                            filters[filters.length] = new nlobjSearchFilter('type', null, 'anyof', 'A'); // Allow only Actual time...
                            //filters[filters.length] = new nlobjSearchFilter('item', null, 'anyof', '2222'); // time only for ST hrs
                            //filters[filters.length] = new nlobjSearchFilter('internalid', 'job', 'anyof', project); // for current project only
                            
                            var cols = new Array();
                            cols[cols.length] = new nlobjSearchColumn('durationdecimal', null, 'sum');
                            //cols[cols.length] = new nlobjSearchColumn('item', null, 'group');
                            
                            //nlapiLogExecution('DEBUG', 'fld_changed_8hrs_val', 'before search ...');
                            
                            var search_result = nlapiSearchRecord('timebill', null, filters, cols);
                            // above search will return only one line as sum of all times for the task on given week
                            
                            if (_validate(search_result)) //
                            {
                                //nlapiLogExecution('DEBUG', 'saveRecord_val_st_hrs_weekly', 'search_result.length : ' + search_result.length);
                                
                                for (var j = 0; j < search_result.length; j++) //
                                {
                                    var result = search_result[0];
                                    var all_columns = result.getAllColumns();
                                    
                                    var total_hrs_search = search_result[j].getValue(all_columns[0]); // get total of all time entered earlier for this task
                                    //nlapiLogExecution('DEBUG', 'saveRecord_val_st_hrs_weekly', 'total_hrs_search : ' + total_hrs_search);
                                    
                                    // add total hrs from search and current total hrs
                                    //var final_total = parseFloat(total_hrs_search) + parseFloat(total_hrs);
                                    
                                    if (_validate(total_hrs_search) == false) //
                                    {
                                        total_hrs_search = 0;
                                        //nlapiLogExecution('DEBUG', 'saveRecord_val_st_hrs_weekly', 'total_hrs_search : ' + total_hrs_search);
                                    }
                                    
                                    total_hrs = parseFloat(total_hrs_search) + parseFloat(total_hrs);
                                    //nlapiLogExecution('DEBUG', 'saveRecord_val_st_hrs_weekly', ' after addition total_hrs : ' + total_hrs);
                                }
                            }
                            else // if search is null
                            {
                                //nlapiLogExecution('DEBUG', 'saveRecord_val_st_hrs_weekly', 'search_result is null');
                            }
                            //nlapiLogExecution('DEBUG', 'saveRecord_val_st_hrs_weekly', ' total_hrs : ' + total_hrs);
                            //nlapiLogExecution('DEBUG', 'saveRecord_val_st_hrs_weekly', ' limit_hrs_weekly : ' + limit_hrs_weekly);
                            nlapiLogExecution('DEBUG', 'total_hrs', 'total_hrs: ' + total_hrs);
                            nlapiLogExecution('DEBUG', 'limit_hrs_weekly', 'limit_hrs_weekly: ' + limit_hrs_weekly);
                            
                            if (limit_hrs_weekly != null) // if Weekly limit is defined on Project
                            {
                                if (casetaskevent == st_task_ID) //
                                {
                                    if (total_hrs > limit_hrs_weekly) // if total hrs are exceeding limit of weekly
                                    {
                                        alert('Weekly hour limit "' + limit_hrs_weekly + '" exceeded  For project "' + project_name + '"');
                                        //nlapiLogExecution('DEBUG', 'saveRecord_val_st_hrs_weekly', 'Weekly hour limit "' + limit_hrs_weekly + '" exceeded, For project "' + project_name + '"');
                                        return false;
                                    }
                                    else //
                                    {
                                        //nlapiLogExecution('DEBUG', 'saveRecord_val_st_hrs_weekly', 'Hours entered "' + total_hrs + '" are fine...');
                                        return true;
                                    }
                                }
                            }
                            else //
                            {
                                nlapiLogExecution('ERROR', 'saveRecord_val_st_hrs_weekly', 'Weekly Limit for hrs is not defined');
                            }
                        }
                        else //
                        {
                            //nlapiLogExecution('DEBUG', 'saveRecord_val_st_hrs_weekly', 'item is not ST...');
                        }
                    }
                    else //
                    {
                        //nlapiLogExecution('DEBUG', 'saveRecord_val_st_hrs_weekly', 'ST Task not found for this project !!!');
                    }
                    
                    /*
                     }
                     
                     for (var k = 1; k <= line_count; k++) //
                     {
                     */
                    // if it is as OT task
                    if (custentity_otapplicable) //  if OT hrs are applicable to this project
                    {
                        if (item == '2425' && limit_hrs_weekly != null) // if it is an OT task (Service item is OT) and weekly limit is defined on Project
                        {
                            var filters = new Array();
                            filters[filters.length] = new nlobjSearchFilter('date', null, 'onorafter', F_date); // on selected field date only
                            filters[filters.length] = new nlobjSearchFilter('date', null, 'onorbefore', T_date); // on selected field date only
                            filters[filters.length] = new nlobjSearchFilter('employee', null, 'anyof', emp_ID); // for current employee only
                            filters[filters.length] = new nlobjSearchFilter('customer', null, 'anyof', project); // for current project only
                            filters[filters.length] = new nlobjSearchFilter('internalid', 'projecttask', 'anyof', st_task_ID); // for ST project task only
                            filters[filters.length] = new nlobjSearchFilter('type', null, 'anyof', 'A'); // Allow only Actual time...
							//filters[filters.length] = new nlobjSearchFilter('item', null, 'anyof', '2425'); // time only for OT hrs
                            //filters[filters.length] = new nlobjSearchFilter('internalid', 'job', 'anyof', project); // for current project only
                            
                            var cols = new Array();
                            cols[cols.length] = new nlobjSearchColumn('durationdecimal', null, 'sum');
                            //cols[cols.length] = new nlobjSearchColumn('item', null, 'group');
                            
                            nlapiLogExecution('DEBUG', 'saveRecord_val_st_hrs_weekly', 'before search ...');
                            
                            var search_result = nlapiSearchRecord('timebill', null, filters, cols);
                            // above search will return only one line as sum of all times for the task on given week
                            var total_ST_hrs = 0;
                            
                            if (_validate(search_result)) //
                            {
                                //nlapiLogExecution('DEBUG', 'saveRecord_val_st_hrs_weekly', 'search_result.length : ' + search_result.length);
                                
                                for (var j = 0; j < search_result.length; j++) //
                                {
                                    var result = search_result[0];
                                    var all_columns = result.getAllColumns();
                                    
                                    var total_hrs_search = search_result[j].getValue(all_columns[0]); // get total of all time entered earlier for this task
                                    //nlapiLogExecution('DEBUG', 'saveRecord_val_st_hrs_weekly', 'total_hrs_search : ' + total_hrs_search);
                                    
                                    // add total hrs from search and current total hrs
                                    //var final_total = parseFloat(total_hrs_search) + parseFloat(total_hrs);
                                    
                                    if (_validate(total_hrs_search) == false) //
                                    {
                                        total_hrs_search = 0;
                                        //nlapiLogExecution('DEBUG', 'saveRecord_val_st_hrs_weekly', 'total_hrs_search : ' + total_hrs_search);
                                    }
                                    
                                    total_ST_hrs = parseFloat(total_hrs_search) + parseFloat(total_ST_hrs);
                                    //nlapiLogExecution('DEBUG', 'saveRecord_val_st_hrs_weekly', ' after addition total_ST_hrs : ' + total_ST_hrs);
                                }
                            }
                            else // if search is null
                            {
                                //nlapiLogExecution('DEBUG', 'saveRecord_val_st_hrs_weekly', 'search_result is null');
                            }
                            nlapiLogExecution('DEBUG', 'saveRecord_val_st_hrs_weekly', ' after addition total_ST_hrs : ' + total_ST_hrs);
                            
                            if (total_ST_hrs < limit_hrs_weekly) // If ST hrs are matching weekly limit
                            {
                                alert('Total ST hours "' + total_ST_hrs + '" are below than limit of weekly hours "' + limit_hrs_weekly + '", So you cannot enter time in OT task !');
                                //nlapiLogExecution('DEBUG', 'saveRecord_val_st_hrs_weekly', 'OT hrs alert !!!');
                                return false;
                            }
                            else //
                            {
                                //nlapiLogExecution('DEBUG', 'saveRecord_val_st_hrs_weekly', 'OT hrs are fine !!!');
                            }
                        }
                        else //
                        {
                            //nlapiLogExecution('DEBUG', 'saveRecord_val_st_hrs_weekly', 'This is not OT Line !');
                        }
                    }
                    else //
                    {
                        alert('OT hrs are not applicable for project : ' + project_name);
                        //nlapiLogExecution('DEBUG', 'saveRecord_val_st_hrs_weekly', 'OT hrs are not applicable for "' + project_name + '" project !');
                    }
                }
            }
        }
        else //
        {
            return true;
        }
        
        //return false;
    
        // Fetch the details of hrs/week and hrs/day
        // go through all lines and add the hrs of ST tasks 
        // also get the earlier saved time entries for the same employee in given week
        // add these hrs into earlier total of hrs
        // if it exceeds than hrs/week
        // show alert and restrict user from saving
        // if day total exceeds than hrs/day then
        // show alert and restrict user from saving the record
    } 
    catch (ex) //
    {
        nlapiLogExecution('ERROR', 'saveRecord_val_st_hrs_weekly', 'Exception : ' + ex);
        nlapiLogExecution('ERROR', 'saveRecord_val_st_hrs_weekly', 'Exception : ' + ex.message);
        return false;
    }
    
    return true;
}

// END SAVE RECORD ==================================================



// BEGIN VALIDATE FIELD =============================================

function weekly_val_Field(type, name, linenum)//
{

    /*  On validate field:
     - EXPLAIN THE PURPOSE OF THIS FUNCTION
     FIELDS USED:
     --Field Name--				--ID--
     */
    //  LOCAL VARIABLES
    
    
    
    //  VALIDATE FIELD CODE BODY
    try //
    {
    
    } 
    catch (ex) //
    {
    
    }
    
    return true;
    
}

// END VALIDATE FIELD ===============================================





// BEGIN FIELD CHANGED ==============================================
var g_hrs = true;

			
function fld_changed_8hrs_val(type, name, linenum) //
{
    /*  On field changed:
     - PURPOSE
     FIELDS USED:
     --Field Name--				--ID--
     */
    //  LOCAL VARIABLES
    
    
    //  FIELD CHANGED CODE BODY
    try //
    {
        var a = new Array();
        a['User-Agent-x'] = 'SuiteScript-Call';
		var resposeObject='';
        // Floating Holiday validation
        if (name == 'casetaskevent') // if task is selected
        {
            //nlapiLogExecution('DEBUG', 'fld_changed_8hrs_val', '********** Execution Started ***********');
            
            var current_task_ID = nlapiGetCurrentLineItemValue('timeitem', name);
            //nlapiLogExecution('DEBUG', 'fld_changed_8hrs_val', 'current_task_ID : ' + current_task_ID);
            
            if (_validate(current_task_ID) == false)//
            {
                return true;
            }
						
            var project = nlapiGetCurrentLineItemValue('timeitem', 'customer');
			
            
            resposeObject = nlapiRequestURL('/app/site/hosting/scriptlet.nl?script=289&deploy=1&custscript_pro_id=' + project + '&custscript_req_type=fh_task', null, a);
            var status = resposeObject.getBody();
			
            var FH_task = status; //get_FH_Task_ID(project); // get FH (Floating Holiday task internal ID)
            //nlapiLogExecution('DEBUG', 'fld_changed_8hrs_val', 'FH_task : ' + FH_task);
            
            if (_validate(FH_task) == false) //
            {
				nlapiLogExecution('DEBUG', 'fld_changed_8hrs_val', 'Task not found FH_task : ' + FH_task);
                return true;
            }
            
            if (current_task_ID == FH_task) // if a Floating Holiday task selected
            {
                var filters = new Array();
                var emp_ID = nlapiGetFieldValue('employee');
                //nlapiLogExecution('DEBUG', 'fld_changed_8hrs_val', 'emp_ID : ' + emp_ID);
                
                //emp_ID = 3243;
                //nlapiLogExecution('DEBUG', 'fld_changed_8hrs_val', 'emp_ID : ' + emp_ID);
                
                filters[filters.length] = new nlobjSearchFilter('custrecord_empid', null, 'anyof', emp_ID);
                filters[filters.length] = new nlobjSearchFilter('custrecord_fh_date_availed', null, 'within', 'thisyear');
                
                var columns = new Array();
                columns[columns.length] = new nlobjSearchColumn('custrecord_empid');
                columns[columns.length] = new nlobjSearchColumn('created');
                
                var search_result = nlapiSearchRecord('customrecord_floating_holiday', null, filters, columns);
                
                if (_validate(search_result)) //
                {
                    if (search_result.length > 0) //
                    {
                        alert('You have already availed Floating Holiday for this year.');
                        nlapiSetCurrentLineItemValue('timeitem', name, '', false);
						//g_hrs = false;
                        return false;
                    }
                    else //
                    {
                        //nlapiLogExecution('DEBUG', 'fld_changed_8hrs_val', 'search_result.length is ' + search_result.length);
                    }
                }
                else //
                {
                    //nlapiLogExecution('DEBUG', 'fld_changed_8hrs_val', 'search_result is ' + search_result);
                }
            }
            else //
            {
                //nlapiLogExecution('DEBUG', 'fld_changed_8hrs_val', 'task is not floating holiday...');
            }
            
        }
        
        
        // Hours per day validation
        if (name == 'hours0' || name == 'hours1' || name == 'hours2' || name == 'hours3' || name == 'hours4' || name == 'hours5' || name == 'hours6') //
        {
            //nlapiLogExecution('DEBUG', 'fld_changed_8hrs_val', '********** Execution Started ***********');
            
            var weekly = nlapiGetFieldValue('weekly'); // weekly would be true when we are on weekly time sheet form.
            //nlapiLogExecution('DEBUG', 'fld_changed_8hrs_val', 'weekly : ' + weekly);
            
            if (weekly == 'T') //
            {
                //nlapiLogExecution('DEBUG', 'fld_changed_8hrs_val', 'Field Name=' + name + '\nLine Item Type=' + type + '\nField Value=' + nlapiGetFieldValue(name) + '\nLine Item Value=' + nlapiGetLineItemValue(type, name, 1));
                
                var current_hrs = nlapiGetCurrentLineItemValue('timeitem', name);
                //nlapiLogExecution('DEBUG', 'fld_changed_8hrs_val', 'current_hrs : ' + current_hrs);
				
                if (_validate(current_hrs)) //
                {
                    current_hrs = _correct_time(current_hrs);
                    //nlapiLogExecution('DEBUG', 'fld_changed_8hrs_val', 'after correcting time current_hrs : ' + current_hrs);
                }
                else // if hrs are not filled
                {
                    return true;
                }
                
                var emp_ID = nlapiGetFieldValue('employee');
                //nlapiLogExecution('DEBUG', 'fld_changed_8hrs_val', 'emp_ID : ' + emp_ID);
                
                //var durationdecimal = nlapiGetCurrentLineItemValue('timeitem', 'durationdecimal');
                //nlapiLogExecution('DEBUG', 'fld_changed_8hrs_val', 'durationdecimal : ' + durationdecimal);
                
                var current_item = nlapiGetCurrentLineItemValue('timeitem', 'item');
                //nlapiLogExecution('DEBUG', 'fld_changed_8hrs_val', 'current_item : ' + current_item);
                
                var casetaskevent = nlapiGetCurrentLineItemValue('timeitem', 'casetaskevent');
                //nlapiLogExecution('DEBUG', 'fld_changed_8hrs_val', 'casetaskevent : ' + casetaskevent);
                
                //if (current_item != '2222') // if item is not ST
                //{
                //	current_hrs = 0;
                //}
                
                var project = nlapiGetCurrentLineItemValue('timeitem', 'customer');
                //nlapiLogExecution('DEBUG', 'fld_changed_8hrs_val', 'project : ' + project);
                
                
                var st_task_ID = ''; // used to validate the task is ST or not.(excluding Leave, OT and Holiday tasks)
                var holiday_task = ''; // used to store holiday task id
                var leave_task = ''; // used to store leave task id
                //var FH_task = ''; // used to store floating holiday task id
                var jobtype = '2'; // 2 = External Project Type as per initial requirement
                var hrs_per_day = null; // 8 is default as per initial requirement.
                var custentity_otapplicable = 'F'; // used to validate if OT is applicable or not
                if (_validate(project)) // if project id is not blank
                {
					var resposeObject = nlapiRequestURL('/app/site/hosting/scriptlet.nl?script=289&deploy=1&custscript_pro_id=' + project 
					+ '&custscript_req_type=project', null, a);
            		var status = resposeObject.getBody();
			
                    var pro_rec = status; //nlapiLoadRecord('job', project);
                    
                    if (_validate(pro_rec)) // if project record is not blank
					{
						resposeObject = nlapiRequestURL('/app/site/hosting/scriptlet.nl?script=289&deploy=1&custscript_pro_id=' + project +
						'&custscript_req_type=st_task', null, a);
						var status = resposeObject.getBody();
						
						st_task_ID = status; //get_ST_Task_ID(project);
						//nlapiLogExecution('DEBUG', 'fld_changed_8hrs_val', 'From project st_task_ID : ' + st_task_ID);
						
						resposeObject = nlapiRequestURL('/app/site/hosting/scriptlet.nl?script=289&deploy=1&custscript_pro_id=' + project +
						'&custscript_req_type=hd_task', null, a);
						var status = resposeObject.getBody();
						
						holiday_task = status; //get_Holiday_Task_ID(project);
						//nlapiLogExecution('DEBUG', 'fld_changed_8hrs_val', 'From project holiday_task : ' + holiday_task);
						
						resposeObject = nlapiRequestURL('/app/site/hosting/scriptlet.nl?script=289&deploy=1&custscript_pro_id=' + project +
						'&custscript_req_type=lv_task', null, a);
						var status = resposeObject.getBody();
						
						leave_task = status; //get_Leave_Task_ID(project);
						//nlapiLogExecution('DEBUG', 'fld_changed_8hrs_val', 'From project leave_task : ' + leave_task);
						
						if (_validate(st_task_ID) == false) //
						{
							//nlapiLogExecution('DEBUG', 'fld_changed_8hrs_val', 'ST task not found on project skipping validation.');
							return true;
						}
						
						resposeObject = nlapiRequestURL('/app/site/hosting/scriptlet.nl?script=289&deploy=1&custscript_pro_id=' + project +
						'&custscript_req_type=custentity_otapplicable', null, a);
						var status = resposeObject.getBody();
						
						custentity_otapplicable = status; //pro_rec.getFieldValue('custentity_otapplicable');
						//nlapiLogExecution('DEBUG', 'fld_changed_8hrs_val', 'From project custentity_otapplicable : ' + custentity_otapplicable);
						
                        resposeObject = nlapiRequestURL('/app/site/hosting/scriptlet.nl?script=289&deploy=1&custscript_pro_id=' + project +
                        '&custscript_req_type=custentity_hoursperday', null, a);
                        var status = resposeObject.getBody();
						
						hrs_per_day = status; //pro_rec.getFieldValue('custentity_hoursperday');
						//nlapiLogExecution('DEBUG', 'fld_changed_8hrs_val', 'From project hrs_per_day : ' + hrs_per_day);
						
						if (_validate(hrs_per_day) == false) // if hrs per day is not defined on the project 
						{
							//alert('"Hours per Day" field is not defined for this Project, So by default, you can enter only 8 Standard Hours per day!');
							//hrs_per_day = 8;
							hrs_per_day = null;
						}
						
						resposeObject = nlapiRequestURL('/app/site/hosting/scriptlet.nl?script=289&deploy=1&custscript_pro_id=' + project +
                        '&custscript_req_type=jobtype', null, a);
                        var status = resposeObject.getBody();
						
						jobtype = status; //pro_rec.getFieldValue('jobtype');
					//nlapiLogExecution('DEBUG', 'fld_changed_8hrs_val', 'From project jobtype : ' + jobtype);
					}
					else // if project record not loaded
					{
						nlapiLogExecution('ERROR', 'fld_changed_8hrs_val', 'Project record not loaded : ' + pro_rec);
					}
                }
                else // if project id not found
                {
                    nlapiLogExecution('ERROR', 'fld_changed_8hrs_val', 'Project ID not found : ' + project);
                }
                
                //nlapiLogExecution('DEBUG', 'fld_changed_8hrs_val', 'final hrs_per_day : ' + hrs_per_day);
                //nlapiLogExecution('DEBUG', 'fld_changed_8hrs_val', 'final jobtype : ' + jobtype);
                
                if (casetaskevent != st_task_ID) // if project task is not ST
                {
                    current_hrs = 0;
                }
                
                var line_count = nlapiGetLineItemCount('timeitem'); // get currently entered lines count for calculation of Total ST hours on current day
                if (_validate(line_count)) //
                {
                    if (line_count > 0) //
                    {
                        for (var j = 1; j <= line_count; j++) //
                        {
                            var line_hrs = nlapiGetLineItemValue('timeitem', name, j);
                            //nlapiLogExecution('DEBUG', 'fld_changed_8hrs_val', 'line_hrs : ' + line_hrs);
                            
                            line_hrs = _correct_time(line_hrs);
                            //nlapiLogExecution('DEBUG', 'fld_changed_8hrs_val', 'after _correct_time line_hrs : ' + line_hrs);
                            
                            var casetaskevent_line = nlapiGetLineItemValue('timeitem', 'casetaskevent', j);
                            //nlapiLogExecution('DEBUG', 'fld_changed_8hrs_val', 'casetaskevent_line : ' + casetaskevent_line);
                            
                            if (casetaskevent_line == st_task_ID) // if project task is ST only
                            //if (casetaskevent_line != leave_task && casetaskevent_line != holiday_task) // if project task is not leave and Holiday and OT 
                            {
                                if (_validate(line_hrs)) //
                                {
                                    current_hrs = parseFloat(current_hrs) + parseFloat(line_hrs);
                                    //nlapiLogExecution('DEBUG', 'fld_changed_8hrs_val', 'after line_hrs addded current_hrs : ' + current_hrs);
                                }
                            }
                            else // task is not ST it may be leave or holiday or OT
                            {
                                //nlapiLogExecution('DEBUG', 'fld_changed_8hrs_val', 'project task is not ST so not taking hrs to total.');
                                //nlapiLogExecution('DEBUG', 'fld_changed_8hrs_val', 'casetaskevent_line : ' + casetaskevent_line);
                            }
                        }
                    }
                }
                
                //nlapiLogExecution('DEBUG', 'fld_changed_8hrs_val', 'current_hrs : ' + current_hrs);
                
                
                
                // Validation for Leave Hrs
                var leave_hrs = nlapiGetCurrentLineItemValue('timeitem', name);
                //nlapiLogExecution('DEBUG', 'fld_changed_8hrs_val', 'leave_hrs : ' + leave_hrs);
                
                leave_hrs = _correct_time(leave_hrs);
                //nlapiLogExecution('DEBUG', 'fld_changed_8hrs_val', 'time correction leave_hrs : ' + leave_hrs);
                
                if (leave_task == casetaskevent) // if task selected is holiday
                {
                    //var remaining_hrs = parseFloat(8) - parseFloat(current_hrs);
                    //nlapiLogExecution('DEBUG', 'fld_changed_8hrs_val', 'remaining_hrs : ' + remaining_hrs);
                    
                    if (leave_hrs > 8) // more than 8 hours are not allowed in Holiday task
                    {
                        alert('You cannot enter more than 8 hours in Leave task !');
                        nlapiSetCurrentLineItemValue('timeitem', name, '', false);
						g_hrs = false;
                        return false;
                    }
                    else //
                    {
                        //nlapiLogExecution('DEBUG', 'fld_changed_8hrs_val', 'Holiday hrs are OK');
                    }
                    
                    //if (leave_hrs > remaining_hrs) // more than 8 hours are not allowed in Holiday task
                    {
                        //alert('You can enter leave only for "' + remaining_hrs + '" hours as you worked for "' + current_hrs + '" hours already !');
                        //nlapiSetCurrentLineItemValue('timeitem', name, '', false);
                        //return false;
                    }
                    //else //
                    {
                        //nlapiLogExecution('DEBUG', 'fld_changed_8hrs_val', 'Leave hrs are OK !');
                    }
                }
                
                // Validation for ST Hrs per Day
                if (_validate(hrs_per_day)) // if service item is ST
                {
                    if (st_task_ID == casetaskevent) // if task selected is ST(project activities)
                    {
                        if (current_hrs > hrs_per_day) // if hours enter are more than max limit per day
                        {
                            alert('Hours entered are more than maximum limit ' + hrs_per_day + ', Please correct the same.');
                            nlapiSetCurrentLineItemValue('timeitem', name, '', false);
							g_hrs = false;
                            return false;
                        }
                        else //
                        {
                            //nlapiLogExecution('DEBUG', 'fld_changed_8hrs_val', 'ST hrs per day are OK');
                        }
                    }
                }
                else //
                {
                    //nlapiLogExecution('DEBUG', 'fld_changed_8hrs_val', 'ST hrs per day limit is not defined on project !');
                }
                
                
                if (jobtype == 2) // External projects are only allowed and Hour per day limit is defined on project
                {
                    var trandate = nlapiGetFieldValue('trandate');
                    //nlapiLogExecution('DEBUG', 'fld_changed_8hrs_val', 'trandate : ' + trandate);
                    
					trandate = nlapiStringToDate(trandate);
					//nlapiLogExecution('DEBUG', 'fld_changed_8hrs_val', 'nlapiStringToDate trandate : ' + trandate);
					
					//trandate = nlapiDateToString(trandate);
					//nlapiLogExecution('DEBUG', 'fld_changed_8hrs_val', 'nlapiDateToString trandate : ' + trandate);
					
                    /*
                     var d_date = Date.parse(trandate);
                     
                     var dd = d_date.getDate();
                     var mm = d_date.getMonth()+ 1;
                     var yy = d_date.getFullYear();
                     
                     nlapiLogExecution('DEBUG', 'fld_changed_8hrs_val', 'dd-mm-yy trandate : ' + dd + '-' + mm + '-' + yy);
                     */
					
					//trandate = _Correct_Date(trandate);
					//nlapiLogExecution('DEBUG', 'fld_changed_8hrs_val', '_Correct_Date trandate : ' + trandate);
					
					
					
                    var days_to_ADD = get_days_to_add(name);
                    //nlapiLogExecution('DEBUG', 'fld_changed_8hrs_val', 'days_to_ADD : ' + days_to_ADD);
                    
                    var d_date = nlapiAddDays(new Date(trandate), days_to_ADD);
                    //nlapiLogExecution('DEBUG', 'fld_changed_8hrs_val', 'after day add d_date : ' + d_date);
                    
                    d_date = nlapiDateToString(d_date);
                    nlapiLogExecution('DEBUG', 'fld_changed_8hrs_val', 'd_date : ' + d_date);
					
					//nlapiLogExecution('DEBUG', 'fld_changed_8hrs_val', 'st_task_ID : ' + st_task_ID);
                    
                    /*
                     d_date = new Date(d_date);
                     var today = d_date;
                     var dd = today.getDate();
                     var mm = today.getMonth() + 1; //January is 0! var yyyy = today.getFullYear(); if(dd<10){dd='0'+dd} if(mm<10){mm='0'+mm} var today = dd+'/'+mm+'/'+yyyy; document.getElementById("DATE").value = today;
                     var yyyy = today.getFullYear();
                     
                     d_date = mm + '/' + dd + '/' + yyyy;
                     //nlapiLogExecution('DEBUG', 'fld_changed_8hrs_val', 'before srch d_date : ' + d_date);
                     */
                    var filters = new Array();
                    filters[filters.length] = new nlobjSearchFilter('date', null, 'on', d_date); // on selected field date only
                    //filters[filters.length] = new nlobjSearchFilter('item', null, 'anyof', '2222'); // item only for ST hrs
                    filters[filters.length] = new nlobjSearchFilter('employee', null, 'anyof', emp_ID); // for current employee only
                    filters[filters.length] = new nlobjSearchFilter('internalid', 'projecttask', 'anyof', st_task_ID); // for ST/FP task ( Project task only ) filter only
                    filters[filters.length] = new nlobjSearchFilter('type', null, 'anyof', 'A'); // Allow only 
					
					var cols = new Array();
                    cols[cols.length] = new nlobjSearchColumn('durationdecimal');
                    cols[cols.length] = new nlobjSearchColumn('item');
					cols[cols.length] = new nlobjSearchColumn('type');
                    
                    nlapiLogExecution('DEBUG', 'fld_changed_8hrs_val', 'before search ...');
                    
                    var search_result = nlapiSearchRecord('timebill', null, filters, cols);
                    //nlapiLogExecution('DEBUG', 'fld_changed_8hrs_val', 'search_result : ' + search_result);
                    
                    if (_validate(search_result)) // check for search_result is not invalid
                    {
                        nlapiLogExecution('DEBUG', 'fld_changed_8hrs_val', 'search_result.length : ' + search_result.length);
                        var total_earlier_hrs = 0;
                        
                        for (var i = 0; i < search_result.length; i++) // take total in total_earlier_hrs for all earlier time enter in timesheet
                        {
                            //nlapiLogExecution('DEBUG', 'fld_changed_8hrs_val', 'into for loop');
							
                            var result = search_result[i];
                            //nlapiLogExecution('DEBUG', 'fld_changed_8hrs_val', 'result : ' + result);
							
                            var all_columns = result.getAllColumns();
                            //nlapiLogExecution('DEBUG', 'fld_changed_8hrs_val', 'all_columns : ' + all_columns);
                            
                            var hrs = result.getValue(all_columns[0]);
                            //nlapiLogExecution('DEBUG', 'fld_changed_8hrs_val', 'hrs : ' + hrs);
                            
                            var item = result.getValue(all_columns[1]);
                            //nlapiLogExecution('DEBUG', 'fld_changed_8hrs_val', 'item : ' + item);
                            
							var time_type = result.getValue(all_columns[2]);
							//nlapiLogExecution('DEBUG', 'fld_changed_8hrs_val', 'time_type : ' + time_type);
							
							if(time_type == 'P') //
							{
								continue;
							}
							
                            total_earlier_hrs = parseFloat(total_earlier_hrs) + parseFloat(hrs);
                        }
                        
                        //var total_hrs = parseFloat(current_hrs) + parseFloat(total_earlier_hrs);
                        current_hrs = parseFloat(current_hrs) + parseFloat(total_earlier_hrs);
                        //nlapiLogExecution('DEBUG', 'fld_changed_8hrs_val', 'Final current_hrs : ' + current_hrs);
                        
                        //if (total_hrs > hrs_per_day) // if total hrs are greater than limit for a day
                        if (hrs_per_day != null) //
                        {
                            if (casetaskevent == st_task_ID) //  validate only for ST task
                            {
                                if (current_hrs > hrs_per_day) // if total hrs are greater than limit for a day
                                {
                                    alert('You have already entered time in this task "' + total_earlier_hrs + '" in total.');
                                    nlapiSetCurrentLineItemValue('timeitem', name, '', false);
									g_hrs = false;
                                    return false;
                                }
                            }
                        }
                        
                        /*
                         if (current_item == '2425') // if it is an OT service item
                         {
                         //if (total_hrs != hrs_per_day) //
                         if (current_hrs != hrs_per_day) //
                         {
                         alert('You cannot enter OT time, as your standard time limit (' + hrs_per_day + ') for a day is not reached !');
                         nlapiSetCurrentLineItemValue('timeitem', name, '', false);
                         return false;
                         }
                         }
                         */
                    }
                    else //
                    {
                        nlapiLogExecution('ERROR', 'fld_changed_8hrs_val', 'No time found in day search : ' + search_result);
                    }
                }
                else // project type is not external
                {
                    nlapiLogExecution('ERROR', 'fld_changed_8hrs_val', 'project type is not external...not performing validation');
                }
                
                // Validation for Holiday Hrs
                var holiday_hrs = nlapiGetCurrentLineItemValue('timeitem', name);
                //nlapiLogExecution('DEBUG', 'fld_changed_8hrs_val', 'holiday_hrs : ' + holiday_hrs);
                
                holiday_hrs = _correct_time(holiday_hrs);
                //nlapiLogExecution('DEBUG', 'fld_changed_8hrs_val', 'time correction holiday_hrs : ' + holiday_hrs);
                
                if (holiday_task == casetaskevent) // if task selected is holiday
                {
                    //nlapiLogExecution('DEBUG', 'fld_changed_8hrs_val', 'Holiday current_hrs : ' + current_hrs);
                    if (current_hrs > 0) // if hours are entered in ST means no holiday is allowed
                    {
                        alert('Holiday hours are not allowed on working day. Please enter the hours in OT task');
                        nlapiSetCurrentLineItemValue('timeitem', name, '', false);
						g_hrs = false;
                        return false;
                    }
                    else //
                    {
                        //nlapiLogExecution('DEBUG', 'fld_changed_8hrs_val', 'Holiday hrs are OK !');
                    }
                    
                    if (holiday_hrs > 8) // more than 8 hours are not allowed in Holiday task
                    {
                        alert('You cannot enter more than 8 hours in Holiday task !');
                        nlapiSetCurrentLineItemValue('timeitem', name, '', false);
						g_hrs = false;
                        return false;
                    }
                    else //
                    {
                        //nlapiLogExecution('DEBUG', 'fld_changed_8hrs_val', 'Holiday hrs are OK');
                    }
                }
                
                // No ST hours found earlier
                if (current_item == '2425') // if it is an OT task (Service item is OT)
                {
                    //nlapiLogExecution('DEBUG', 'fld_changed_8hrs_val', 'Final custentity_otapplicable : ' + custentity_otapplicable);
                    
                    if (custentity_otapplicable == 'F') // If OT is not allowed on Project itself...
                    {
                        alert('OT Hours are not allowed for this project !!!');
                        nlapiLogExecution('ERROR', 'fld_changed_8hrs_val', 'OT Hours are not allowed for this project !!!');
                        nlapiSetCurrentLineItemValue('timeitem', name, '', false);
						g_hrs = false;
                        return false;
                    }
                    
                    //if (total_hrs != hrs_per_day) //
					//nlapiLogExecution('DEBUG', 'fld_changed_8hrs_val', 'hrs_per_day : ' + hrs_per_day);
					//nlapiLogExecution('DEBUG', 'fld_changed_8hrs_val', 'current_hrs : ' + current_hrs);
					
                    if (current_hrs != hrs_per_day && hrs_per_day != null) //
                    {
                        alert('You cannot enter OT time as your standard time limit (' + hrs_per_day + ') for a day is not reached !');
                        nlapiLogExecution('ERROR', 'fld_changed_8hrs_val', 'You cannot enter OT time as your standard time limit (' + hrs_per_day + ') for a day is not reached !');
                        nlapiSetCurrentLineItemValue('timeitem', name, '', false);
						g_hrs = false;
                        return false;
                    }
                }
                else //
                {
                    nlapiLogExecution('ERROR', 'fld_changed_8hrs_val', 'This is not OT task');
                }
            }
        }
        //nlapiLogExecution('ERROR', 'fld_changed_8hrs_val', 'returning 1');
		g_hrs = true;
        return true;
    } 
    catch (ex) //
    {
        log_error('fld_changed_8hrs_val', ex);
        log_error('fld_changed_8hrs_val', ex.message)
		g_hrs = false;
        return false;
    }
    
    nlapiLogExecution('ERROR', 'fld_changed_8hrs_val', 'returning 2');
	g_hrs = true;
    return true;
    //alert('Field Name='+name+'\nLine Item Type='+type+'\nField Value='+nlapiGetFieldValue(name)+'\nLine Item Value='+nlapiGetLineItemValue(type, name, 1));
}

// END FIELD CHANGED ================================================






// BEGIN POST SOURCING ==============================================

function postSourcing(type, name){

    /*  On post sourcing:
    
     
    
     - PURPOSE
    
     
    
     FIELDS USED:
    
     
    
     --Field Name--			--ID--		--Line Item Name--
    
     
    
     */
    
    //  LOCAL VARIABLES


    //  POST SOURCING CODE BODY



}

// END POST SOURCING ================================================



// BEGIN LINE INIT ==============================================

function weekly_lineInit(type) // 
{

    /*  On Line Init:
     - PURPOSE
     FIELDS USED:
     --Field Name--			--ID--		--Line Item Name--
     */
    //  LOCAL VARIABLES
    
    
    //  LINE INIT CODE BODY
    
    try //
    {
        var result_1 = document.getElementById("timeitem_copy").setAttribute("type", "hidden");
        //nlapiLogExecution('DEBUG', 'pageinit', 'button invisible : ' + result_1);
    } 
    catch (ex) //
    {
        nlapiLogExecution('ERROR', 'pageinit', 'ex : ' + ex);
        //nlapiLogExecution('DEBUG', 'pageinit', 'ex : ' + ex.message);
    }
    
}

// END LINE INIT ================================================


// BEGIN VALIDATE LINE ==============================================

function validateLine_weekly(type){

    /*  On validate line:
     - EXPLAIN THE PURPOSE OF THIS FUNCTION
     FIELDS USED:
     --Field Name--				--ID--
     */
    //  LOCAL VARIABLES
    
    
    //  VALIDATE LINE CODE BODY
    //nlapiLogExecution('DEBUG', 'validateLine_weekly', 'type : ' + type);
    //var current_line = nlapiGetCurrentLineItemValue('hourstotal');
    //var total_hrs = nlapiGetCurrentLineItemValue('hourstotal'); //nlapiGetFieldValue('hourstotal');
    //nlapiLogExecution('DEBUG', 'validateLine_weekly', 'total_hrs : ' + total_hrs);
    
    if (_validate(total_hrs)) //
    {
    
    }
    else {
        //nlapiLogExecution('DEBUG', 'validateLine_weekly', 'total_hrs is invalid : ' + total_hrs);
    }
    
    
    return true;
    
}

// END VALIDATE LINE ================================================



// BEGIN RECALC =====================================================

function recalc(type){

    /*  On recalc:
    
     
    
     - EXPLAIN THE PURPOSE OF THIS FUNCTION
    
     
    
     FIELDS USED:
    
     
    
     --Field Name--				--ID--
    
     
    
     */
    
    //  LOCAL VARIABLES


    //  RECALC CODE BODY


}

// END RECALC =======================================================



// BEGIN FUNCTION ===================================================

function _validate(obj) //
{
    if (obj != null && obj != '' && obj != 'undefined' && obj != undefined)//
    {
        return true;
    }
    else //
    {
        return false;
    }
}

function get_ST_Task_ID(projectID) // search for ST task in project task list
{
    var filters = new Array();
    filters[filters.length] = new nlobjSearchFilter('internalid', 'job', 'anyof', projectID);
    
    // In case of any issue, please take a look at saved search "customsearch_task_search_for_st_task"
    // And confirm that Task names of "OT", "Leave" and "Holiday" has not been change in NetSuite 
    // You can check this by analyzing result if the saved search
    
    var search_result = nlapiSearchRecord('projecttask', 'customsearch_task_search_for_st_task', filters, null);
    
    if (_validate(search_result)) //
    {
        //nlapiLogExecution('DEBUG', 'get_Task_ID', 'search_result.length : ' + search_result.length);
        
        for (var i = 0; i < search_result.length; i++) //
        {
            var result = search_result[i];
            var all_columns = result.getAllColumns();
            
            var st_task_ID = result.getValue(all_columns[0]);
            //nlapiLogExecution('DEBUG', 'get_Task_ID', 'st_task_ID : ' + st_task_ID);
            
            return st_task_ID;
        }
    }
    return null;
}

function get_Holiday_Task_ID(projectID) // search for Holiday task in project task list
{
    var filters = new Array();
    filters[filters.length] = new nlobjSearchFilter('internalid', 'job', 'anyof', projectID);
    
    // In case of any issue, please take a look at saved search "customsearch_task_search_holiday_task"
    // And confirm that Task names of "Holiday" has not been change in NetSuite 
    // You can check this by analyzing result if the saved search
    
    var search_result = nlapiSearchRecord('projecttask', 'customsearch_task_search_holiday_task', filters, null);
    
    if (_validate(search_result)) //
    {
        //nlapiLogExecution('DEBUG', 'get_Holiday_Task_ID', 'search_result.length : ' + search_result.length);
        
        for (var i = 0; i < search_result.length; i++) //
        {
            var result = search_result[i];
            var all_columns = result.getAllColumns();
            
            var hday_task_ID = result.getValue(all_columns[0]);
            //nlapiLogExecution('DEBUG', 'get_Holiday_Task_ID', 'hday_task_ID : ' + hday_task_ID);
            
            return hday_task_ID;
        }
    }
    return null;
}

function get_FH_Task_ID(projectID) // search for Holiday task in project task list
{
    var filters = new Array();
    filters[filters.length] = new nlobjSearchFilter('internalid', 'job', 'anyof', projectID);
    
    // In case of any issue, please take a look at saved search "customsearch_task_search_for_fh_task"
    // And confirm that Task names of "Floating Holiday" has not been change in NetSuite 
    // You can check this by analyzing result if the saved search
    
    var search_result = nlapiSearchRecord('projecttask', 'customsearch_task_search_for_fh_task', filters, null);
    
    if (_validate(search_result)) //
    {
        //nlapiLogExecution('DEBUG', 'get_FH_Task_ID', 'search_result.length : ' + search_result.length);
        
        for (var i = 0; i < search_result.length; i++) //
        {
            var result = search_result[i];
            var all_columns = result.getAllColumns();
            
            var fh_task_ID = result.getValue(all_columns[0]);
            //nlapiLogExecution('DEBUG', 'get_FH_Task_ID', 'fh_task_ID : ' + fh_task_ID);
            
            return fh_task_ID;
        }
    }
    return null;
}

function get_Leave_Task_ID(projectID) // search for Holiday task in project task list
{
    var filters = new Array();
    filters[filters.length] = new nlobjSearchFilter('internalid', 'job', 'anyof', projectID);
    
    // In case of any issue, please take a look at saved search "customsearch_task_search_for_leave_task"
    // And confirm that Task names of "Leave" has not been change in NetSuite 
    // You can check this by analyzing result if the saved search
    
    var search_result = nlapiSearchRecord('projecttask', 'customsearch_task_search_for_leave_task', filters, null);
    
    if (_validate(search_result)) //
    {
        //nlapiLogExecution('DEBUG', 'get_Leave_Task_ID', 'search_result.length : ' + search_result.length);
        
        for (var i = 0; i < search_result.length; i++) //
        {
            var result = search_result[i];
            var all_columns = result.getAllColumns();
            
            var leave_task_ID = result.getValue(all_columns[0]);
            //nlapiLogExecution('DEBUG', 'get_Leave_Task_ID', 'leave_task_ID : ' + leave_task_ID);
            
            return leave_task_ID;
        }
    }
    return null;
}

function _correct_time(t_time) //
{
    // function is used to correct the time 
    
    if (_validate(t_time)) {
        //nlapiLogExecution('DEBUG', '_correct_time', 't_time : ' + t_time);
        
        var hrs = t_time.split(':')[0]; //nlapiLogExecution('DEBUG', '_correct_time', 'hrs : ' + hrs);
        var mins = t_time.split(':')[1]; //nlapiLogExecution('DEBUG', '_correct_time', 'mins : ' + mins);
        mins = parseFloat(mins) / parseFloat('60'); //nlapiLogExecution('DEBUG', '_correct_time', 'after correct mins : ' + mins);
        hrs = parseFloat(hrs) + parseFloat(mins); //nlapiLogExecution('DEBUG', '_correct_time', 'after adding mins to hrs : ' + hrs);	
        return hrs;
    }
    else {
        //nlapiLogExecution('DEBUG', '_correct_time', 't_time is invalid : ' + t_time);
    }
    
}

function log_info(fun_name, msg) //
{
    //nlapiLogExecution('DEBUG', fun_name, msg);
}

function log_error(fun_name, msg) //
{
    nlapiLogExecution('ERROR', fun_name, msg);
}

function get_days_to_add(name) //
{
    if (name == 'hours0') //
    {
        return 0;
    }
    if (name == 'hours1') //
    {
        return 1;
    }
    if (name == 'hours2') //
    {
        return 2;
    }
    if (name == 'hours3') //
    {
        return 3;
    }
    if (name == 'hours4') //
    {
        return 4;
    }
    if (name == 'hours5') //
    {
        return 5;
    }
    if (name == 'hours6') //
    {
        return 6;
    }
}

function _Correct_Date(d_date)//
{
	var today;
	// ============================= Todays Date ==========================================
	
	//var date1 = new Date(d_date);
	var date1 = d_date;
	nlapiLogExecution('DEBUG', 'schedulerFunction', 'Date ==' + date1);
	
	var offsetIST = 5.5;
	
	//To convert to UTC datetime by subtracting the current Timezone offset
	var utcdate = new Date(date1.getTime() + (date1.getTimezoneOffset() * 60000));
	nlapiLogExecution('DEBUG', 'schedulerFunction', 'UTC Date' + utcdate);
	
	//Then cinver the UTS date to the required time zone offset like back to 5.5 for IST
	var istdate = new Date(utcdate.getTime() - ((-offsetIST * 60) * 60000));
	nlapiLogExecution('DEBUG', 'schedulerFunction', 'IST Date ==' + istdate);
	
	var day = istdate.getDate();
	nlapiLogExecution('DEBUG', 'schedulerFunction', 'Day ==' + day);
	
	var month = istdate.getMonth() + 1;
	nlapiLogExecution('DEBUG', 'schedulerFunction', 'Month ==' + month);
	
	var year = istdate.getFullYear();
	nlapiLogExecution('DEBUG', 'schedulerFunction', 'Year ==' + year);
	
	var date_format = checkDateFormat();
	nlapiLogExecution('DEBUG', 'schedulerFunction', 'Date Format ==' + date_format);
	
	if (date_format == 'YYYY-MM-DD') //
	{
		today = year + '-' + month + '-' + day;
	}
	if (date_format == 'DD/MM/YYYY') //
	{
		today = day + '/' + month + '/' + year;
	}
	if (date_format == 'MM/DD/YYYY') //
	{
		today = month + '/' + day + '/' + year;
	}
	var s_month = '';
	
	if (month == 1) //
	{
		s_month = 'Jan'
		
	}
	else 
		if (month == 2) //
		{
			s_month = 'Feb'
			
		}
		else 
			if (month == 3) //
			{
			
				s_month = 'Mar'
			}
			else 
				if (month == 4) //
				{
					s_month = 'Apr'
					
				}
				else 
					if (month == 5) //
					{
						s_month = 'May'
						
					}
					else 
						if (month == 6) //
						{
						
							s_month = 'Jun'
						}
						else 
							if (month == 7) //
							{
								s_month = 'Jul'
								
							}
							else 
								if (month == 8) //
								{
									s_month = 'Aug'
									
								}
								else 
									if (month == 9) //
									{
										s_month = 'Sep'
										
									}
									else 
										if (month == 10) //
										{
											s_month = 'Oct'
											
										}
										else 
											if (month == 11) //
											{
											
												s_month = 'Nov'
											}
											else 
												if (month == 12) //
												{
												
													s_month = 'Dec'
												}
	
	if (date_format == 'DD-Mon-YYYY') //
	{
		today = day + '-' + s_month + '-' + year;
	}
	
	
	return today;
}

function checkDateFormat() //
{
	var context = nlapiGetContext();
	
	var dateFormatPref = context.getPreference('dateformat');
	
	return dateFormatPref;
}

// END FUNCTION =====================================================