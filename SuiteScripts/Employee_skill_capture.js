/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       15 May 2017     bidhi.raj
 *
 */

/**
 * @param {nlobjRequest} request Request object
 * @param {nlobjResponse} response Response object
 * @returns {Void} Any output is written via response object
 */
function suitelet(request, response) {
	if (request.getMethod() == 'GET') {
		var columns = new Array();

		columns[0] = new nlobjSearchColumn('internalid');
		columns[1] = new nlobjSearchColumn('custentity_employee_primary_skill');
		columns[2] = new nlobjSearchColumn(
				'custentity_employee_secondary_skill');
		columns[3] = new nlobjSearchColumn('custentity_competency');
		columns[4] = new nlobjSearchColumn('entityid');
		var emp = nlapiSearchRecord('employee', 'customsearch_emplo_for_fm',
				null, columns);//loading employees,if reporting manager comes

		if (emp == null) {//loading employees,if project manager comes
			var em_array = [];
			var r_columns = new Array();
			r_columns[0] = new nlobjSearchColumn('resource');
			var emp1 = nlapiSearchRecord('resourceallocation',
					'customsearch2380', null, r_columns);
			for ( var k in emp1) {
				em_array.push(emp1[k].getValue('resource'));

			}
			var fil2 = new Array();
			if (em_array.length != 0) {
				fil2[0] = new nlobjSearchFilter('internalid', null, 'anyof',
						em_array);
				var emp = nlapiSearchRecord('employee', null, fil2, columns);
			}

		}
		//Create the form and add fields to it
		var form = nlapiCreateForm("Employee Skills Updation Page");
		var objUser = nlapiGetContext();
		var i_employee_id = objUser.getUser();
		//var i_employee_id =  8232;
		if (i_employee_id == 1605) {
			var filter = new Array();
			filter[0] = new nlobjSearchFilter('department', null, 'anyof', 497)
			var emp = nlapiSearchRecord('employee', null, filter, columns);
		}
		var i_role = nlapiGetRole();
		var department_t = nlapiLookupField('employee', i_employee_id,
				[ 'department' ]);

		var department_t_head = nlapiLookupField('department',
				department_t.department, [ 'custrecord_practicehead',
						'custrecord_parent_practice' ]);
		var dep = department_t_head.custrecord_parent_practice;
		nlapiLogExecution('Debug', 'dep',
				department_t_head.custrecord_practicehead);
		//custrecord_practicehead
		//i_role = 1104;
		if (parseInt(i_employee_id) == parseInt(37501) || parseInt(i_employee_id) == parseInt(41571) ||	parseInt(i_employee_id) == parseInt(8231) ||	
		i_employee_id == 30484	|| i_employee_id == 106232 || i_employee_id == 8231 ||i_employee_id == 5803) {
			//Calling Cleint Script Field Change
			form.setScript('customscript_cs_field_change_emp_list');
			form
					.addField('custpage_field0', 'select', 'Practice',
							'department');
			form.addField('custpage_field1', 'select', 'Employee')
					.setDisplayType('hidden');
			form.addField('custpage_field5', 'select', 'Employee', 'employee')
					.setMandatory(true);
			form.addField('custpage_field2', 'text', 'Primary Skills')
					.setMaxLength(230);
			form.addField('custpage_field3', 'text', 'Secondary skills')
					.setMaxLength(280);
			form.addField('custpage_field4', 'text', 'Course status');
			form.addField('custpage_field6', 'text', 'Competency');

			var sublist = form.addSubList('employee_', 'inlineeditor',
					'Employees Skill Details', null);
			sublist.addField('eid', 'text', 'Name').setDisplayType('disabled');
			sublist.addField('ps_id', 'text', 'Primary skill').setDisplayType(
					'disabled');
			sublist.addField('ss_id', 'text', 'Secondary skill')
					.setDisplayType('disabled');
			sublist.addField('cs_id', 'text', 'Course Status').setDisplayType(
					'disabled');
			sublist.addField('comp_id', 'text', 'Competency').setDisplayType(
					'disabled');
			//sublist.setLineItemValues(emp);
		}

		else {

			form.addField('custpage_field0', 'select', 'Select Field',
					'department').setMandatory(true).setDisplayType('disabled')
					.setDefaultValue(dep);
			var employ = form.addField('custpage_field1', 'select', 'Employee',
					null, null).setMandatory(true);
			employ.addSelectOption('', '');
			try {
				for (var i = 0; i < emp.length; ++i) {
					//employee.addSelectOption(30179, text, selected)
					var n = emp[i].getValue('internalid');
					employ.addSelectOption(n, emp[i].getValue('entityid'));
				}
			} catch (e) {
				throw 'Sorry!You dont have access to view this page';
				nlapiLogExecution('DEBUG', 'null reportees Error', e);
			}
			var sublist = form.addSubList('sublist_id', 'inlineeditor',
					'Employees Skill Details', null);
			sublist.addField("entityid", "text", "Name").setDisplayType(
					'disabled');
			sublist.addField("custentity_employee_primary_skill", "text",
					"Primary skill").setDisplayType('disabled');
			sublist.addField("custentity_employee_secondary_skill", "text",
					"Secondary skill").setDisplayType('disabled');
			sublist.addField("custentity_competency", "text", "Competency")
					.setDisplayType('disabled');
			sublist.setLineItemValues(emp);
			form.addField('custpage_field2', 'text', 'Primary Skills')
					.setMandatory(true).setMaxLength(20);
			form.addField('custpage_field3', 'text', 'Secondary skills')
					.setMaxLength(25);
			form.addField('custpage_field6', 'text', 'Competency');
		}
		//}

		form.addSubmitButton('Submit');
		response.writePage(form);
	}
	//POST call
	else {
		var objUser = nlapiGetContext();
		var i_employee_id = objUser.getUser();
		//var i_employee_id =  8232;
		var form = nlapiCreateForm("Employee record has been updated");
		//create the fields on the form and populate them with values from the previous screen
		if (parseInt(i_employee_id) == parseInt(37501) || parseInt(i_employee_id) == parseInt(41571)  ||	parseInt(i_employee_id) == parseInt(8231) || i_employee_id == 30484||i_employee_id == 5803) {
			var i_id_employee = request.getParameter('custpage_field5');
		} else {
			var i_id_employee = request.getParameter('custpage_field1');
		}

		var primary_skill = request.getParameter('custpage_field2');
		var secondary_skill = request.getParameter('custpage_field3');
		var course_details = request.getParameter('custpage_field4');
		var competency = request.getParameter('custpage_field6');
		var emp_search = nlapiLoadRecord('employee', i_id_employee, null);

		nlapiLogExecution('error', 'got updated employ', i_id_employee);

		var objUser = nlapiGetContext();
		var i_employee_id = objUser.getUser();
		var i_role = nlapiGetRole();
		nlapiLogExecution('error', 'i_employee_id of editor', i_employee_id);

		if (parseInt(i_employee_id) == parseInt(37501) || parseInt(i_employee_id) == parseInt(41571) || 	parseInt(i_employee_id) == parseInt(8231) || i_employee_id == 30484
				|| i_employee_id == 106232 || i_employee_id == 8231 ||i_employee_id == 5803) {
			if (primary_skill) {
				emp_search.setFieldValue('custentity_employee_primary_skill',
						primary_skill);
			}
			if (secondary_skill) {
				emp_search.setFieldValue('custentity_employee_secondary_skill',
						secondary_skill);
			}
			if (course_details) {
				emp_search.setFieldValue('custentity_course_status',
						course_details);
			}
			if (competency) {
				emp_search.setFieldValue('custentity_competency', competency);
			}

		} else {
			emp_search.setFieldValue('custentity_employee_primary_skill',
					primary_skill);
			if (secondary_skill) {
				emp_search.setFieldValue('custentity_employee_secondary_skill',
						secondary_skill);
			}
			if (competency) {
				emp_search.setFieldValue('custentity_competency', competency);
			}
		}

		try {

			nlapiSubmitRecord(emp_search);
			form.setScript('customscript_cs_field_change_emp_list');
			var url = 'https://system.na1.netsuite.com/app/site/hosting/scriptlet.nl?script=1201&deploy=1';
			form.addButton('cust_button', 'BACK', 'pageinit(\'  ' + url
					+ '  \');');

		} catch (error) {
			assistant.setError(error);
		}
		//custentity16
		//nlapiLogExecution('Debug', 'custom fields', i_id_employee+" "+primary_skill+" "+secondary_skill);

		response.writePage(form);
	}

}
