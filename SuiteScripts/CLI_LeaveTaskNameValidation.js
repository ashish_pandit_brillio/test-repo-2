/**
 * @author Jayesh
 */

function save_record_validate_taskname()
{
	try
	{
		var s_task_name_entered = nlapiGetFieldValue('title');
		//var b_is_taskBillable = nlapiGetFieldValue('nonbillabletask');
		
		//if(b_is_taskBillable == 'T')
		{
			if(s_task_name_entered.indexOf('Leave') >= 0 || s_task_name_entered.indexOf('leave') >= 0)
			{
				if(s_task_name_entered != 'Leave')
				{
					alert(s_task_name_entered+' project task name should be "Leave" that too case sensitive.');
					return false;
				}
			}
			
			if(s_task_name_entered.indexOf('Holiday') >= 0 || s_task_name_entered.indexOf('holiday') >= 0)
			{
				if(s_task_name_entered != 'Holiday')
				{
					alert(s_task_name_entered+' project task name should be "Holiday" that too case sensitive.');
					return false;
				}
			}
		}
		
		return true;
	}
	catch(err)
	{
		alert('ERROR MESSAGE:- '+err);
	}
}
