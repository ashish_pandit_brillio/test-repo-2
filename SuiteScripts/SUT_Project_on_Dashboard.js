// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
/*
   	Script Name: SUT_Project_on_Dashboard
	Author:		 Vikrant
	Company:	Aashna 
	Date:		27-Sep-2014
	Description: Script is developed for showing information of Current Projects assigned of Employee.


	Script Modification Log:

	-- Date --			-- Modified By --				--Requested By--				-- Description --



Below is a summary of the process controls enforced by this script file.  The control logic is described
more fully, below, in the appropriate function headers and code blocks.


     SUITELET
		- SUT_Project_on_Dashboard(request, response)


     SUB-FUNCTIONS
		- The following sub-functions are called by the above core functions in order to maintain code
            modularization:

               - NOT USED

*/
}
// END SCRIPT DESCRIPTION BLOCK  ====================================



// BEGIN GLOBAL VARIABLE BLOCK  =====================================
{
	//  Initialize any Global Variables, in particular, debugging variables...




}
// END GLOBAL VARIABLE BLOCK  =======================================





// BEGIN SUITELET ==================================================

function SUT_Project_on_Dashboard(request, response) //
{

	/*  Suitelet:
	 - EXPLAIN THE PURPOSE OF THIS FUNCTION
	 -
	 FIELDS USED:
	 --Field Name--				--ID--
	 */
	//  LOCAL VARIABLES
	
	
	
	//  SUITELET CODE BODY
	
	try //
	{
		nlapiLogExecution('DEBUG', 'SUT_Project_on_Dashboard', '******** Execution Started ********')
		
		var form = nlapiCreateForm('My Allocations');
		
		// Getting Past 5 Allocations.
		var sublist_past = form.addSubList('custpage_list_1', 'list', 'Past Allocations');
		
		var returncols = new Array();
		returncols[0] = new nlobjSearchColumn('company');
		returncols[1] = new nlobjSearchColumn('startdate');
		returncols[2] = new nlobjSearchColumn('enddate').setSort(true);
		//returncols[3] = new nlobjSearchColumn('salesrep');
		//returncols[4] = new nlobjSearchColumn('amount');
		
		var current_user = nlapiGetUser();
		nlapiLogExecution('DEBUG', 'SUT_Project_on_Dashboard', 'current_user...' + current_user)
		
		var filters = new Array();
		filters[filters.length] = new nlobjSearchFilter('internalid', 'employee', 'anyof', current_user);
		var current_date = nlapiDateToString(new Date());
		nlapiLogExecution('DEBUG', 'SUT_Project_on_Dashboard', 'current_date...' + current_date)
		filters[filters.length] = new nlobjSearchFilter('enddate', null, 'before', current_date);
		//filters[filters.length] = new nlobjSearchFilter('startdate', null, 'onorbefore', current_date);
		
		nlapiLogExecution('DEBUG', 'SUT_Project_on_Dashboard', '2 Before Search...')
		var results = nlapiSearchRecord('resourceallocation', null, filters, returncols);
		
        var s_project = sublist_past.addField('custevent_project_1', 'text', 'Project Name');
        var s_start_date = sublist_past.addField('custevent_startdate_1', 'text', 'Allocation Start Date');
        var s_end_date = sublist_past.addField('custevent_enddate_1', 'text', 'Allocation End Date');
		
		if (_validate(results)) //
		{
			nlapiLogExecution('DEBUG', 'SUT_Project_on_Dashboard', 'results.length... ' + results.length)
			for (var j = 0; j < results.length; j++) //
			{
				if(j == 5) // Show only last 5 allocations
				{
					break;
				}
				
				var result = results[j];
				var all_columns = result.getAllColumns();
				
				sublist_past.setLineItemValue('custevent_project_1', j + 1, '' + result.getText(all_columns[0]));
				sublist_past.setLineItemValue('custevent_startdate_1', j + 1, '' + result.getValue(all_columns[1]));
				sublist_past.setLineItemValue('custevent_enddate_1', j + 1, '' + result.getValue(all_columns[2]));
			}
		}
		//nlapiLogExecution('DEBUG', 'SUT_Project_on_Dashboard', 'After Search...')
		
		var sublist_current = form.addSubList('custpage_list_2', 'list', 'Current Allocations');
		
		var returncols = new Array();
		returncols[0] = new nlobjSearchColumn('company');
		returncols[1] = new nlobjSearchColumn('startdate');
		returncols[2] = new nlobjSearchColumn('enddate');
		//returncols[3] = new nlobjSearchColumn('salesrep');
		//returncols[4] = new nlobjSearchColumn('amount');
		
		var current_user = nlapiGetUser();
		nlapiLogExecution('DEBUG', 'SUT_Project_on_Dashboard', 'current_user...' + current_user)
		
		var filters = new Array();
		filters[filters.length] = new nlobjSearchFilter('internalid', 'employee', 'anyof', current_user);
		var current_date = nlapiDateToString(new Date());
		nlapiLogExecution('DEBUG', 'SUT_Project_on_Dashboard', 'current_date...' + current_date)
		filters[filters.length] = new nlobjSearchFilter('enddate', null, 'onorafter', current_date);
		filters[filters.length] = new nlobjSearchFilter('startdate', null, 'onorbefore', current_date);
		
		nlapiLogExecution('DEBUG', 'SUT_Project_on_Dashboard', '1 Before Search...')
		var results = nlapiSearchRecord('resourceallocation', null, filters, returncols);
		
        var s_project = sublist_current.addField('custevent_project', 'text', 'Project Name');
        var s_start_date = sublist_current.addField('custevent_startdate', 'text', 'Allocation Start Date');
        var s_end_date = sublist_current.addField('custevent_enddate', 'text', 'Allocation End Date');
				
		if (_validate(results)) //
		{
			nlapiLogExecution('DEBUG', 'SUT_Project_on_Dashboard', 'results.length... ' + results.length)
			for (var i = 0; i < results.length; i++) //
			{
				var result = results[i];
				var all_columns = result.getAllColumns();
				
				sublist_current.setLineItemValue('custevent_project', i + 1, '' + result.getText(all_columns[0]));
				sublist_current.setLineItemValue('custevent_startdate', i + 1, '' + result.getValue(all_columns[1]));
				sublist_current.setLineItemValue('custevent_enddate', i + 1, '' + result.getValue(all_columns[2]));
			}
		//response.writePage(form);
		}
		
		
		response.writePage(form);
	}
	catch (ex) //
	{
		nlapiLogExecution('ERROR', 'SUT_Project_on_Dashboard', 'ex...' + ex)
		nlapiLogExecution('ERROR', 'SUT_Project_on_Dashboard', 'ex...' + ex.message)
	}
}

// END SUITELET ====================================================




// BEGIN OBJECT CALLED/INVOKING FUNCTION ===================================================
{
function _validate(obj) //
{
    if (obj != null && obj != '' && obj != 'undefined' && obj != undefined)//
    {
        return true;
    }
    else //
    {
        return false;
    }
}


}
// END OBJECT CALLED/INVOKING FUNCTION =====================================================
