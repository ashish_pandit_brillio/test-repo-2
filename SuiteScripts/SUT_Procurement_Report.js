/**
 * Procurement Report
 * 
 * Version Date Author Remarks 1.00 Mar 30 2015 Nitish Mishra
 * 
 */

/**
 * @param {nlobjRequest}
 *        request Request object
 * @param {nlobjResponse}
 *        response Response object
 * @returns {Void} Any output is written via response object
 */
function suitelet(request, response) {

	try {

		if (request.getParameter("mode") == "Export") {
			exportProcurementReport(request);
		}
		else {
			createProcurementForm(request);
		}
	}
	catch (err) {
		nlapiLogExecution('ERROR', 'suitelet', err);
	}
}

function exportProcurementReport(request) {

	try {
		// get the values from the request
		var vertical = request.getParameter('custpage_vertical');
		var practice = request.getParameter('custpage_practice');
		var customer = request.getParameter('custpage_customer');
		var project = request.getParameter('custpage_project');
		var fromDate = request.getParameter('custpage_from_date');
		var toDate = request.getParameter('custpage_to_date');

		if (isEmpty(fromDate) || isEmpty(toDate)) {
			nlapiLogExecution('ERROR', 'Fields Missing');
			return;
		}

		var procurementDetails =
				getProcurementDetails(vertical, practice, customer, project, fromDate, toDate);

		var csvString =
				"REQUEST DATE,PR#,EMPLOYEE,CUSTOMER,END CUSTOMER,PROJECT,AMOUNT,CURRENCY,STATUS,PRACTICE,VERTICAL\n";

		procurementDetails.forEach(function(procurement) {

			csvString +=
					procurement.requestdate + "," + procurement.procurementid + ","
							+ procurement.employee + "," + procurement.customer + ","
							+ procurement.endcustomer + "," + procurement.project + ","
							+ procurement.amount + "," + procurement.currency + ","
							+ procurement.status + "," + procurement.practice + ","
							+ procurement.vertical + "\n";
		});


		nlapiLogExecution('debug', 'file created');

		var filename = 'Procurement Report - ' + new Date() + ".csv";
		var file = nlapiCreateFile(filename, 'CSV', csvString);
		response.setContentType('CSV', filename);
		response.write(file.getValue());
	}
	catch (err) {
		nlapiLogExecution('ERROR', 'exportProcurementReport', err);
		throw err;
	}
}

function createProcurementForm(request) {

	try {
		var form = nlapiCreateForm('Procurement Report', false);
		var report_access = getUserReportAccessList();

		// check deployment
		var context = nlapiGetContext();
		var deploymentId = context.getDeploymentId();
		form.addField('custpage_deployment', 'text', 'Deployement').setDisplayType('hidden')
				.setDefaultValue(deploymentId);

		// add from date
		var field_from_date = form.addField('custpage_from_date', 'date', 'From Date : ');
		field_from_date.setMandatory(true);

		// add to date
		var field_to_date = form.addField('custpage_to_date', 'date', 'To Date : ');
		field_to_date.setMandatory(true);

		// --
		var field_practice = null;
		var field_vertical = null;
		var field_customer = null;
		var field_project = null;

		switch (deploymentId) {

			case 'customdeploy_sut_procure_rep_vertical':
				field_practice =
						form.addField('custpage_practice', 'select', 'Practice', 'department');

				field_vertical = form.addField('custpage_vertical', 'select', 'Vertical');
				createDropdown(report_access.VerticalList.Values, report_access.VerticalList.Texts)
						.forEach(
								function(vertical) {

									field_vertical.addSelectOption(vertical.Value, vertical.Text,
											vertical.IsSelected);
								});

				field_customer =
						form.addField('custpage_customer', 'select', 'Customer', 'customer');
			break;

			case 'customdeploy_sut_procure_rep_practice':
				field_practice = form.addField('custpage_practice', 'select', 'Practice');
				createDropdown(report_access.PracticeList.Values, report_access.PracticeList.Texts)
						.forEach(
								function(department) {

									field_practice.addSelectOption(department.Value,
											department.Text, department.IsSelected);
								});

				field_vertical =
						form.addField('custpage_vertical', 'select', 'Vertical', 'classification');

				field_customer =
						form.addField('custpage_customer', 'select', 'Customer', 'customer');
			break;

			case 'customdeploy_sut_procure_rep_customer':
				if (isArrayEmpty(report_access.CustomerList)) {
					throw "No Customer Specified";
				}

				// add the filtered practice list
				field_practice = form.addField('custpage_practice', 'select', 'Practice');

				var practiceList = [];
				report_access.CustomerList.forEach(function(cust) {

					cust.Practice.forEach(function(prac) {

						practiceList.push(prac);
					});
				});

				nlapiSearchRecord('department', null,
						new nlobjSearchFilter('internalid', null, 'anyof', practiceList),
						new nlobjSearchColumn('name')).forEach(function(dept) {

					field_practice.addSelectOption(dept.getId(), dept.getValue('name'), false);
				});

				field_practice.addSelectOption('', '', true);
				field_practice.setMandatory(true);

				// add the vertical list
				field_vertical =
						form.addField('custpage_vertical', 'select', 'Vertical', 'classification');

				// add the customers list
				field_customer = form.addField('custpage_customer', 'select', 'Customer');

				report_access.CustomerList.forEach(function(cust) {

					field_customer.addSelectOption(cust.CustomerValue, cust.CustomerText, false);
				});

				field_customer.addSelectOption('', '', true);
				field_customer.setMandatory(true);
			break;
		}

		// add the project field
		field_project = form.addField('custpage_project', 'select', 'Project', 'job');
		// --
		/*
		 * // add the practice field var field_practice = null; if (deploymentId ==
		 * "customdeploy_sut_procure_rep_vertical") { field_practice =
		 * form.addField('custpage_practice', 'select', 'Practice',
		 * 'department'); } else if (deploymentId ==
		 * "customdeploy_sut_procure_rep_practice") { field_practice =
		 * form.addField('custpage_practice', 'select', 'Practice'); // get
		 * department list getDepartmentList().forEach( function(department) {
		 * 
		 * field_practice.addSelectOption(department.Value, department.Text,
		 * department.IsSelected); }); } // add the vertical field var
		 * field_vertical = null; if (deploymentId ==
		 * "customdeploy_sut_procure_rep_practice") { field_vertical =
		 * form.addField('custpage_vertical', 'select', 'Vertical',
		 * 'classification'); } else if (deploymentId ==
		 * "customdeploy_sut_procure_rep_vertical") { field_vertical =
		 * form.addField('custpage_vertical', 'select', 'Vertical'); // get
		 * vertical list getVerticalList().forEach(function(vertical) {
		 * 
		 * field_vertical.addSelectOption(vertical.Value, vertical.Text,
		 * vertical.IsSelected); }); }
		 */

		// get the values from the request
		var vertical = request.getParameter('custpage_vertical');
		var practice = request.getParameter('custpage_practice');
		var customer = request.getParameter('custpage_customer');
		var project = request.getParameter('custpage_project');
		var fromDate = request.getParameter('custpage_from_date');
		var toDate = request.getParameter('custpage_to_date');

		// set the default value in the drop down
		if (isNotEmpty(vertical)) {
			field_vertical.setDefaultValue(vertical);
		}

		if (isNotEmpty(practice)) {
			field_practice.setDefaultValue(practice);
		}

		if (isNotEmpty(customer)) {
			field_customer.setDefaultValue(customer);
		}

		if (isNotEmpty(project)) {
			field_project.setDefaultValue(project);
		}

		if (isNotEmpty(fromDate)) {
			field_from_date.setDefaultValue(fromDate);
		}

		if (isNotEmpty(toDate)) {
			field_to_date.setDefaultValue(toDate);
		}

		if (request.getMethod() == "POST") {
			var procurementDetails =
					getProcurementDetails(vertical, practice, customer, project, fromDate, toDate);
			var subList = form.addSubList('custpage_procurement', 'list', 'Purchase Request');

			subList.addField('requestdate', 'date', 'Request Date');
			subList.addField('procurementid', 'text', 'PR#');
			subList.addField('employee', 'text', 'Employee');
			subList.addField('project', 'text', 'Project');
			subList.addField('customer', 'text', 'Customer');
			subList.addField('endcustomer', 'text', 'End Customer');
			subList.addField('amount', 'text', 'Amount');
			subList.addField('currency', 'text', 'Currency');
			subList.addField('status', 'text', 'Status');
			subList.addField('practice', 'text', 'Practice');
			subList.addField('vertical', 'text', 'Vertical');
			subList.setLineItemValues(procurementDetails);

			// add the export to CSV button
			form.addButton('custpage_export_csv', 'Export To CSV', 'exportToCsv');
		}

		form.addSubmitButton('Get PR');
		form.setScript('customscript_cs_sut_procure_report');
		response.writePage(form);
	}
	catch (err) {
		nlapiLogExecution('ERROR', 'createProcurementForm', err);
		throw err;
	}
}

function getProcurementDetails(vertical, practice, customer, project, fromDate, toDate) {

	try {
		var customerName = null;
		var filters =
				[
					new nlobjSearchFilter('custrecord_prastatus', null, 'noneof', [
						constant.PurchareRequestStatus.Cancelled,
						constant.PurchareRequestStatus.Rejected, constant.None
					]),
					new nlobjSearchFilter('custrecord_requisitiondate',
							'custrecord_purchaserequest', 'within', fromDate, toDate)
				];

		// based on deployment and user
		var deploymentId = nlapiGetContext().getDeploymentId();

		var report_access = getUserReportAccessList();

		switch (deploymentId) {

			case "customdeploy_sut_procure_rep_vertical":
				filters.push(new nlobjSearchFilter('custrecord_verticals', null, 'anyof',
						report_access.VerticalList.Values));
			break;

			case "customdeploy__sut_procure_rep_practice":
				filters.push(new nlobjSearchFilter('custrecord_prpractices', null, 'anyof',
						report_access.PracticeList.Values));
			break;

			case "customdeploy_sut_procure_rep_customer":

				// add a check to see if the employee is
				// allowed to access the specified customer
				// with the specified practice
				var customerList = report_access.CustomerList;
				var allowAccess = false;

				for (var i = 0; i < customerList.length; i++) {

					if (customerList[i].CustomerValue == customer) {
						customerName = customerList[i].CustomerText;
						var practiceList = customerList[i].Practice;

						for (var j = 0; j < practiceList.length; j++) {

							if (practiceList[j] == practice) {
								allowAccess = true;
								break;
							}
						}
						break;
					}
				}

				if (!allowAccess) {
					throw "Access not allowed";
				}
			break;
		}


		// department filtering
		if (isNotEmpty(practice)) {
			filters.push(new nlobjSearchFilter('custrecord_prpractices', null, 'anyof',
					searchSubDepartments(practice)));
		}

		// project filtering
		// if (isNotEmpty(i_project_filter)) {
		// filters[filters.length] =
		// new nlobjSearchFilter('project', null, 'anyof', i_project_filter);
		// }

		// customer filtering
		if (isNotEmpty(customer)) {

			if (isEmpty(customerName)) {
				// get the customer name using search
				var customerDetails = nlapiLookupField('customer', customer, [
					'entityid', 'companyname'
				]);

				customerName = customerDetails.entityid + ' ' + customerDetails.companyname;
			}

			// add customer filter
			var customer_filter_2 = "case when {custrecord_customerforpritem} = any(";
			customer_filter_2 += "'" + customerName + "'";
			customer_filter_2 += ") then 'T' else 'F' end";
			nlapiLogExecution('debug', 'customer_filter_2', customer_filter_2);

			filters.push(new nlobjSearchFilter('formulatext', null, 'is', 'T')
					.setFormula(customer_filter_2));
		}

		// vertical filtering
		if (isNotEmpty(vertical)) {
			filters.push(new nlobjSearchFilter('custrecord_verticals', null, 'anyof', vertical));
		}

		var procurements = [];
		var procurementDetails = [];

		searchRecord(
				'customrecord_pritem',
				null,
				filters,
				[
					new nlobjSearchColumn('custrecord_prno', 'custrecord_purchaserequest', 'group'),
					new nlobjSearchColumn('custrecord_requisitiondate',
							'custrecord_purchaserequest', 'group'),
					new nlobjSearchColumn('custrecord_employee', null, 'group'),
					new nlobjSearchColumn('custrecord_prastatus', null, 'group'),
					new nlobjSearchColumn('custrecord_customerforpritem', null, 'group'),
					new nlobjSearchColumn('custrecord_verticals', null, 'group'),
					new nlobjSearchColumn('custrecord_prpractices', null, 'group'),
					new nlobjSearchColumn('custrecord_project', null, 'group'),
					new nlobjSearchColumn('custentity_endcustomer', 'custrecord_project', 'group'),
					new nlobjSearchColumn('custrecord_customerforpritem', null, 'group'),
					new nlobjSearchColumn('custrecord_totalvalue', null, 'sum'),
					new nlobjSearchColumn('custrecord_currencyforpr', null, 'group'),
					new nlobjSearchColumn('custrecord_purchaserequest', null, 'group').setSort()
				]).forEach(
				function(procurement) {

					procurements.push(procurement.getValue('custrecord_purchaserequest', null,
							'group'));
					procurementDetails.push({
						employee : procurement.getText('custrecord_employee', null, 'group'),
						project : procurement.getText('custrecord_project', null, 'group'),
						requestdate : procurement.getValue('custrecord_requisitiondate',
								'custrecord_purchaserequest', 'group'),
						procurementid : procurement.getValue('custrecord_prno',
								'custrecord_purchaserequest', 'group'),
						internalid : procurement.getValue('custrecord_purchaserequest', null,
								'group'),
						status : procurement.getText('custrecord_prastatus', null, 'group'),
						amount : procurement.getValue('custrecord_totalvalue', null, 'sum'),
						currency : procurement.getText('custrecord_currencyforpr', null, 'group'),
						vertical : procurement.getText('custrecord_verticals', null, 'group'),
						practice : procurement.getText('custrecord_prpractices', null, 'group'),
						customer : procurement.getValue('custrecord_customerforpritem', null,
								'group'),
						endcustomer : procurement.getText('custentity_endcustomer',
								'custrecord_project', 'group')
					});
				});

		nlapiLogExecution('debug', 'procurementDetails length A', procurementDetails.length);
		return procurementDetails;
	}
	catch (err) {
		nlapiLogExecution('ERROR', 'getProcurementDetails', err);
		throw err;
	}
}

function searchSubDepartments(i_department_id) {

	try {
		var s_department_name = nlapiLookupField('department', i_department_id, 'name');

		var filters = new Array();
		if (s_department_name.indexOf(' : ') == -1) {
			filters[0] = new nlobjSearchFilter('formulatext', null, 'is', s_department_name);
			filters[0]
					.setFormula('TRIM(CASE WHEN INSTR({name} , \' : \', 1) > 0 THEN SUBSTR({name}, 1 , INSTR({name} , \' : \', 1)) ELSE {name} END)');
		}
		else {
			filters[0] = new nlobjSearchFilter('internalid', null, 'anyof', i_department_id);
		}
		var columns = new Array();
		columns[0] = new nlobjSearchColumn('internalid');

		var search_sub_departments = nlapiSearchRecord('department', null, filters, columns);

		var a_sub_departments = new Array();

		for (var i = 0; i < search_sub_departments.length; i++) {
			a_sub_departments.push(search_sub_departments[i].getValue('internalid'));
		}

		return a_sub_departments;
	}
	catch (err) {
		nlapiLogExecution('ERROR', 'searchSubDepartments', err);
		throw err;
	}
}

function getDepartmentList() {

	try {
		var currentEmployeeId = nlapiGetUser();
		var permissionSearch =
				nlapiSearchRecord('customrecord_report_permission_list', null, [
					new nlobjSearchFilter('custrecord_rpl_employee', null, 'anyof', [
						currentEmployeeId
					]), new nlobjSearchFilter('isinactive', null, 'is', 'F'),
					new nlobjSearchFilter('custrecord_rpl_has_dept_access', null, 'is', 'T')
				]);

		if (isArrayNotEmpty(permissionSearch)) {

			var permission_record =
					nlapiLoadRecord('customrecord_report_permission_list', permissionSearch[0]
							.getId());
			var a_department_value_list =
					permission_record.getFieldValues('custrecord_rpl_department');
			var a_department_text_list =
					permission_record.getFieldTexts('custrecord_rpl_department');
			var a_department_list = [];

			if (isArrayEmpty(a_department_value_list)) {
				throw "No department specified";
			}

			var count_department = a_department_value_list.length;

			if (count_department > 1) {
				a_department_list.push({
					Value : '',
					Text : '',
					IsSelected : true
				});
			}

			for (var i = 0; i < count_department; i++) {
				a_department_list.push({
					Value : a_department_value_list[i],
					Text : a_department_text_list[i],
					IsSelected : false
				});
			}

			if (count_department == 1) {
				a_department_list[0].IsSelected = true;
			}

			return a_department_list;
		}
		else {
			throw "You are not authorized to access this page";
		}
	}
	catch (err) {
		nlapiLogExecution('ERROR', 'getDepartmentList', err);
		throw err;
	}
}

function getVerticalList() {

	try {
		var currentEmployeeId = nlapiGetUser();
		var permissionSearch =
				nlapiSearchRecord('customrecord_report_permission_list', null, [
					new nlobjSearchFilter('custrecord_rpl_employee', null, 'anyof', [
						currentEmployeeId
					]), new nlobjSearchFilter('isinactive', null, 'is', 'F'),
					new nlobjSearchFilter('custrecord__rpl_has_vertical_access', null, 'is', 'T')
				]);

		if (isArrayNotEmpty(permissionSearch)) {

			var permission_record =
					nlapiLoadRecord('customrecord_report_permission_list', permissionSearch[0]
							.getId());
			var a_vertical_value_list = permission_record.getFieldValues('custrecord_rpl_vertical');
			var a_vertical_text_list = permission_record.getFieldTexts('custrecord_rpl_vertical');
			var a_vertical_list = [];

			if (isArrayEmpty(a_vertical_value_list)) {
				throw "No Verticals Specified";
			}

			var count_vertical = a_vertical_value_list.length;

			if (count_vertical > 1) {
				a_vertical_list.push({
					Value : '',
					Text : '',
					IsSelected : true
				});
			}

			for (var i = 0; i < count_vertical; i++) {
				a_vertical_list.push({
					Value : a_vertical_value_list[i],
					Text : a_vertical_text_list[i],
					IsSelected : false
				});
			}

			if (count_vertical == 1) {
				a_vertical_list[0].IsSelected = true;
			}

			return a_vertical_list;
		}
		else {
			throw "You are not authorized to access this page";
		}
	}
	catch (err) {
		nlapiLogExecution('ERROR', 'getVerticalList', err);
		throw err;
	}
}

function exportToCsv() {

	try {
		// read the selections
		var vertical = nlapiGetFieldValue('custpage_vertical');
		var practice = nlapiGetFieldValue('custpage_practice');
		var customer = nlapiGetFieldValue('custpage_customer');
		var project = nlapiGetFieldValue('custpage_project');
		var from_date = nlapiGetFieldValue('custpage_from_date');
		var to_date = nlapiGetFieldValue('custpage_to_date');
		var deployment = nlapiGetFieldValue('custpage_deployment');

		var csvExportUrl =
				nlapiResolveURL('SUITELET', 'customscript_sut_procurement_report', deployment);
		var link =
				csvExportUrl + "&custpage_vertical=" + vertical + "&custpage_practice=" + practice
						+ "&custpage_customer=" + customer + "&custpage_project=" + project
						+ "&custpage_from_date=" + from_date + "&custpage_to_date=" + to_date
						+ "&mode=Export";

		var win = window.open(link);
		win.focus();
	}
	catch (err) {
		alert(JSON.stringify(err));
	}
}