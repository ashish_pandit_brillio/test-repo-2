/**
 * RESTlet for Allocation Details
 * 
 * Version Date Author Remarks 1.00 11 Apr 2016 Nitish Mishra
 * 
 */

function postRESTlet(dataIn) {
	var response = new Response();
	try {
		nlapiLogExecution('debug', 'dataIn', JSON.stringify(dataIn));
		var employeeId = getUserUsingEmailId(dataIn.EmailId);
		var allocationId = dataIn.Data ? dataIn.Data.AllocationId : '';
		nlapiLogExecution('debug', 'allocationId', allocationId);
		var requestType = dataIn.RequestType;
		var isPM = dataIn.Data ? dataIn.Data.IsProjectManager : '';
		//var isPM = dataIn.IsProjectManager;
		nlapiLogExecution('debug', 'isPM', isPM);
		switch (requestType) {

			case M_Constants.Request.Get:

				if(allocationId && isPM == 'No') {
					response.Data = getAllocationDetails(employeeId,allocationId);
					response.Status = true;
				}
					else if(allocationId && isPM == 'Yes') {
					response.Data = getPMsAllocationDetails(employeeId,allocationId,dataIn); 
					response.Status = true;	
					} else {
					response.Data = getActiveAllocationList(employeeId);
					response.Status = true;
				}
			break;

		}

	} catch (err) {
		nlapiLogExecution('ERROR', 'postRESTlet', err);
		response.Data = err;
	}
	return response;
}

function getAllAllocationList(employeeId) {
	try {
		nlapiLogExecution('debug', 'employee id', employeeId);

		// get the current active allocation holiday list
		var allocationSearch = nlapiSearchRecord(
		        'resourceallocation',
		        null,
		        [ new nlobjSearchFilter('resource', null, 'anyof', employeeId) ],
		        [ new nlobjSearchColumn('company'),
		                new nlobjSearchColumn('enddate'),
		                new nlobjSearchColumn('startdate').setSort(),
		                new nlobjSearchColumn('percentoftime') ]);

		var allocationList = []; 
		var today = new Date();

		if (allocationSearch) {

			allocationSearch
			        .forEach(function(allocation) {
				        var status = nlapiStringToDate(allocation
				                .getValue('enddate')) >= today;

				        allocationList.push({
				            Id : allocation.getId(),
				            Project : allocation.getText('company'),
				            StartDate : allocation.getValue('startdate'),
				            EndDate : allocation.getValue('enddate'),
				            Percent : allocation.getValue('percentoftime'),
				            Active : status
				        });
			        });
		}
		return allocationList;

	} catch (err) {
		nlapiLogExecution('error', 'getAllAllocationList', err);
		throw err;
	}
}

function getActiveAllocationList(employeeId) {
	try {
		nlapiLogExecution('debug', 'employee id', employeeId);
		var overall_alloc = [];
		var filters = [];
		filters.push(new nlobjSearchFilter('resource', null, 'anyof', employeeId));
		filters.push(new nlobjSearchFilter('enddate', null, 'notbefore', 'today'));
		filters.push(new nlobjSearchFilter('startdate', null, 'notafter', 'today'));
		// get the current active allocation holiday list
		var allocationSearch = nlapiSearchRecord(
		        'resourceallocation',
		        null,
		        filters,
		        [ new nlobjSearchColumn('company'),
		                new nlobjSearchColumn('enddate'),
		                new nlobjSearchColumn('startdate').setSort(),
		                new nlobjSearchColumn('percentoftime') ,
		                new nlobjSearchColumn('custentity_projectmanager','job'),
		                new nlobjSearchColumn('customer','job'),
		                new nlobjSearchColumn('jobbillingtype','job'),
		                new nlobjSearchColumn('jobtype','job'),
		                new nlobjSearchColumn('subsidiary','job'),
		                new nlobjSearchColumn('custentity_vertical','job'),
		                new nlobjSearchColumn('custentity_practice','job'),
		                new nlobjSearchColumn('startdate','job'),
		                new nlobjSearchColumn('enddate','job'),
		                new nlobjSearchColumn('custentity_worklocation','job'),
		        		new nlobjSearchColumn('entityid','job'),
		        		new nlobjSearchColumn('altname','job')]); 

		var active_allocationList = [];
		var today = new Date();

		if (allocationSearch) {
			
			for(var indx=0;indx<allocationSearch.length;indx++){
			//allocationSearch
			   //     .forEach(function(allocation) {
				        var status = nlapiStringToDate(allocationSearch[indx]
				                .getValue('enddate')) >= today;

				                active_allocationList.push({
				            SlNo:indx+1,    	
				            Id : allocationSearch[indx].getId(),
				            ProjectID: allocationSearch[indx].getValue('entityid','job'),
				            ProjectName : allocationSearch[indx].getValue('altname','job'),
				            AllocationStartDate : allocationSearch[indx].getValue('startdate'),
				            AllocationEndDate : allocationSearch[indx].getValue('enddate'),
				            Percent : allocationSearch[indx].getValue('percentoftime'),
				            Status : 'Active',
				            Customer : allocationSearch[indx].getText('customer','job'),
				            BillingType : allocationSearch[indx].getText('jobbillingtype','job'),
				            ProjectType : allocationSearch[indx].getText('jobtype','job'),
				            ProjectSubsidiary : allocationSearch[indx].getText('subsidiary','job'),
				            ProjectExecutingPractice : allocationSearch[indx].getText('custentity_practice','job'),
				            ProjectLocation : allocationSearch[indx].getText('custeventwlocation','job'),
				            ProjectStartDate : allocationSearch[indx].getValue('startdate','job'),
				            ProjectEndDate : allocationSearch[indx].getValue('enddate','job'),
				            ProjectLocation : allocationSearch[indx].getText('custentity_worklocation','job'),
				            IsProjectManager: allocationSearch[indx].getValue('custentity_projectmanager','job') == employeeId ? 'Yes' : 'No'
				        });
			        }
		}
		overall_alloc.push(active_allocationList);
		//Past Allocation
		var filters_p = [];
		filters_p.push(new nlobjSearchFilter('resource', null, 'anyof', employeeId));
		filters_p.push(new nlobjSearchFilter('enddate', null, 'before', 'today'));
		
		//filters.push(new nlobjSearchFilter('startdate', null, 'notafter', 'today'));
		// get the current active allocation holiday list
		var allocationSearch_p = nlapiSearchRecord(
		        'resourceallocation',
		        null,
		        filters_p,
		        [ new nlobjSearchColumn('company'),
		                new nlobjSearchColumn('enddate').setSort(true),
		                new nlobjSearchColumn('startdate'),
		                new nlobjSearchColumn('percentoftime') ,
		                new nlobjSearchColumn('custentity_projectmanager','job'),
		                new nlobjSearchColumn('custentity_deliverymanager','job'),
		                new nlobjSearchColumn('customer','job'),
		                new nlobjSearchColumn('jobbillingtype','job'),
		                new nlobjSearchColumn('jobtype','job'),
		                new nlobjSearchColumn('subsidiary','job'),
		                new nlobjSearchColumn('custentity_vertical','job'),
		                new nlobjSearchColumn('custentity_practice','job'),
		                new nlobjSearchColumn('startdate','job'),
		                new nlobjSearchColumn('enddate','job'),
		                new nlobjSearchColumn('custentity_worklocation','job'),
        				new nlobjSearchColumn('entityid','job'),
        				new nlobjSearchColumn('altname','job')]); 

		var past_allocationList = [];
		var today = new Date();
		var counter = 0;
		if (allocationSearch_p) {

			/*allocationSearch
			        .forEach(function(allocation) {*/
				        //var status = nlapiStringToDate(allocation
				            //    .getValue('enddate')) >= today;
			if(allocationSearch_p.length > 10)
				counter = 10;
			else
				counter = allocationSearch_p.length;

			for(var i=0; i<counter ;i++){
				past_allocationList.push({
							SlNo: i+1,
				            Id : allocationSearch_p[i].getId(),
				            ProjectID: allocationSearch_p[i].getValue('entityid','job'),
				            ProjectName : allocationSearch_p[i].getValue('altname','job'),
				            StartDate : allocationSearch_p[i].getValue('startdate'),
				            EndDate : allocationSearch_p[i].getValue('enddate'),
				            Percent : allocationSearch_p[i].getValue('percentoftime'),
				            Status : 'Past',
				            Customer : allocationSearch_p[i].getText('customer','job'),
				            BillingType : allocationSearch_p[i].getText('jobbillingtype','job'),
				            ProjectType : allocationSearch_p[i].getText('jobtype','job'),
				            ProjectSubsidiary : allocationSearch_p[i].getText('subsidiary','job'),
				            ProjectExecutingPractice : allocationSearch_p[i].getText('custentity_practice','job'),
				            ProjectLocation : allocationSearch_p[i].getText('custeventwlocation','job'),
				            ProjectStartDate : allocationSearch_p[i].getValue('startdate','job'),
				            ProjectEndDate : allocationSearch_p[i].getValue('enddate','job'),
				            ProjectLocation : allocationSearch_p[i].getText('custentity_worklocation','job'),
				            IsProjectManager: allocationSearch_p[i].getValue('custentity_projectmanager','job') == employeeId ? 'Yes' : 'No'
				        });
			        }
		}
		overall_alloc.push(past_allocationList);
		overall_alloc = removearrayduplicate(overall_alloc);
		return overall_alloc;

	} catch (err) {
		nlapiLogExecution('error', 'getAllAllocationList', err);
		throw err;
	}
}

function getAllocationDetails(employeeId, allocationId) {
	try {
		var record = nlapiLoadRecord('resourceallocation', allocationId);

		if (record.getFieldValue('allocationresource') == employeeId) {
			var projectRec = nlapiLoadRecord('job', record
			        .getFieldValue('project'));

			return {
			    ResourceName : record.getFieldText('allocationresource'),
			    ProjectName : record.getFieldText('project'),
			    StartDate : record.getFieldValue('startdate'),
			    EndDate : record.getFieldValue('enddate'),
			    Site : record.getFieldText('custevent4'),
			    Percent : record.getFieldValue('percentoftime'),
			    ProjectLocation : record.getFieldText('custeventwlocation'),
			    Practice : record.getFieldText('custevent_practice'),
			    CustomerName : projectRec.getFieldText('parent'),
			    BillingType : projectRec.getFieldText('jobbillingtype'),
			    Type : projectRec.getFieldText('jobtype'),
			    ProjectManager : projectRec
			            .getFieldText('custentity_projectmanager'),
			    DeliveryManager : projectRec
			            .getFieldText('custentity_deliverymanager'),
			    Subsidiary : projectRec.getFieldText('subsidiary'),
			    Vertical : projectRec.getFieldText('custentity_vertical'),
			    ExecutingPractice : projectRec 
			            .getFieldText('custentity_practice')
			};
		} else {
			throw "You are not authorized to access this record";
		}
	} catch (err) {
		nlapiLogExecution('error', 'getAllocationDetails', err);
		throw err;
	}
}

function getPMsAllocationDetails(employeeId, allocationId,dataIn){
try{
	var main = [];
	var allocationList = [];
	var status_allocation = dataIn.Data.Status;
	nlapiLogExecution('DEBUG','status_allocation',status_allocation);
	var record = nlapiLoadRecord('resourceallocation', allocationId);
	var project_V = record.getFieldValue('project');
	var ProjectARRAY = [];
	if(project_V){
	var projectRec = nlapiLoadRecord('job', record
	        .getFieldValue('project'));
	var ProjectID = projectRec.getFieldValue('entityid');
	var ProjectName = projectRec.getFieldValue('altname');
	var CustomerName = projectRec.getFieldText('parent');
	nlapiLogExecution('DEBUG','CustomerName',CustomerName);
	var BillingType = projectRec.getFieldText('jobbillingtype');
	var Type = projectRec.getFieldText('jobtype');
	var project_st_date = projectRec.getFieldValue('startdate');
	var project_end_date = projectRec.getFieldValue('enddate');
	var projectPM = projectRec.getFieldText('custentity_projectmanager');
	var projectDM = projectRec.getFieldText('custentity_deliverymanager');
	var projectLocation = projectRec.getFieldText('custentity_worklocation');
	
	var project_JSON = {
			ProjectID: ProjectID,
			IN:'',
			EMPID:'',
			ProjectName: ProjectName,
			Customer: CustomerName,
			Percent: record.getFieldValue('percentoftime'),
			ProjectStartDate: project_st_date,
			ProjectEndDate: project_end_date,
			BillingType: BillingType,
			ProjectType: Type,
			PM: projectPM,
			DM: projectDM,
			ProjectLocation: projectLocation
			
	};
	ProjectARRAY.push(project_JSON);
	}
	
	var filters = [];
	filters.push(new nlobjSearchFilter('custentity_projectmanager', 'job', 'anyof', employeeId));
	filters.push(new nlobjSearchFilter('project', null, 'anyof', project_V));
	if(status_allocation == 'Active')
		filters.push(new nlobjSearchFilter('enddate', null, 'onorafter', 'today'));
	else
		filters.push(new nlobjSearchFilter('enddate', null, 'before', 'today'));	
	
	
	var allocationSearch_p = nlapiSearchRecord(
	        'resourceallocation',
	        null,
	        filters,
	        [ new nlobjSearchColumn('company'),
	          new nlobjSearchColumn('resource'),
	                new nlobjSearchColumn('enddate'),
	                new nlobjSearchColumn('startdate').setSort(),
	                new nlobjSearchColumn('percentoftime'),
	                new nlobjSearchColumn('customer','job'),
	                new nlobjSearchColumn('jobbillingtype','job'),
	                new nlobjSearchColumn('jobtype','job'),
	                new nlobjSearchColumn('title','employee'),
	                new nlobjSearchColumn('subsidiary','job'),
	                new nlobjSearchColumn('custentity_vertical','job'),
	                new nlobjSearchColumn('custentity_practice','job'),
	                new nlobjSearchColumn('startdate','job'),
	                new nlobjSearchColumn('enddate','job'),
	                new nlobjSearchColumn('custentity_worklocation','job'),
	                new nlobjSearchColumn('custentity_projectmanager','job'),
	                new nlobjSearchColumn('custentity_deliverymanager','job'),
	                new nlobjSearchColumn('entityid','job'),
	                new nlobjSearchColumn('altname','job')]);
	
	if(allocationSearch_p){
	//	if(status_allocation == 'Active'){	
	for(var i=0;i<allocationSearch_p.length;i++){
		allocationList.push({
					SlNo: i+1,
		            Id : allocationSearch_p[i].getId(),
		            ProjectID: allocationSearch_p[i].getValue('entityid','job'),
		            ProjectName : allocationSearch_p[i].getValue('altname','job'),
		            Resource:allocationSearch_p[i].getText('resource'),
		            IN:'',
		            EMPID:'',
		            Designation : allocationSearch_p[i].getValue('title','employee'),
		            StartDate : allocationSearch_p[i].getValue('startdate'),
		            EndDate : allocationSearch_p[i].getValue('enddate'),
		            Percent : allocationSearch_p[i].getValue('percentoftime'),		            
		            Status: status_allocation,
		            Customer : allocationSearch_p[i].getText('customer','job'),
		            BillingType : allocationSearch_p[i].getText('jobbillingtype','job'),
		            ProjectType : allocationSearch_p[i].getText('jobtype','job'),
		            ProjectSubsidiary : allocationSearch_p[i].getText('subsidiary','job'),
		            ProjectExecutingPractice : allocationSearch_p[i].getText('custentity_practice','job'),
		            ProjectLocation : allocationSearch_p[i].getText('custeventwlocation','job'),
		            ProjectStartDate : allocationSearch_p[i].getValue('startdate','job'),
		            ProjectEndDate : allocationSearch_p[i].getValue('enddate','job'),
		            ProjectLocation : allocationSearch_p[i].getText('custentity_worklocation','job'),
		            Projectmanager : allocationSearch_p[i].getText('custentity_projectmanager','job'),
		            DelieveryManager : allocationSearch_p[i].getText('custentity_deliverymanager','job')
		        });
	        }
		//}
		/*else {
			for(var i=0;i<allocationSearch_p.length && i<10;i++){
				allocationList.push({
				            Id : allocationSearch_p[i].getId(),
				            Resource:allocationSearch_p[i].getText('resource'),
				            Project : allocationSearch_p[i].getText('company'),
				            StartDate : allocationSearch_p[i].getValue('startdate'),
				            EndDate : allocationSearch_p[i].getValue('enddate'),
				            Percent : allocationSearch_p[i].getValue('percentoftime'),		            
				            Status: status_allocation
				        });
			        }
			
		}*/	
	}
	main = {
		Project: ProjectARRAY,
		Resources: allocationList
			
	};
	return main;
}	
catch (err) {
	nlapiLogExecution('error', 'getAllocationDetails', err);
	throw err;
}
}

function removearrayduplicate(array) {
    var newArray = new Array();
    label: for (var i = 0; i < array.length; i++) {
          for (var j = 0; j < array.length; j++) {
                if (newArray[j] == array[i])
                      continue label;
          }
          newArray[newArray.length] = array[i];
    }
    return newArray;
}
