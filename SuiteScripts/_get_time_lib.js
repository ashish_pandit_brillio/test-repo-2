function sysDate() {
    var str = "";
    var d = new Date();
    var localTime = d.getTime();
    var localOffset = d.getTimezoneOffset() * 60000;
    var utc = localTime + localOffset;
    var target_offset = 5.5;
    var indian = utc+(3600000*target_offset);
    nd = new Date(indian);
    var hours = nd.getHours();
    var minutes = nd.getMinutes();
    var meridian = "";
    if (hours >= 12) {
        meridian += "pm";
    } else {
        meridian += "am";
    }
    if (hours > 12) {

        hours = hours - 12;
    }
    if (minutes < 10) {
        minutes = "0" + minutes;
    }
    //str += hours + ":" + minutes + ":" + seconds + " ";
    str += hours + ":" + minutes + " ";
    var tdate = nd.getDate();
    var month = nd.getMonth() + 1; // jan = 0
    var year = nd.getFullYear();
    if (month < 10) {
        month = "0" + month;
    }
    if (tdate < 10) {
        tdate = "0" + tdate;
    }
    var currentDate = month + '/' + tdate + '/' + year;
    //nlapiLogExecution('DEBUG', 'currentDate', currentDate);
    var currentTime = str + meridian;
    //nlapiLogExecution('DEBUG', 'currentTime', currentTime);
    var currentDateAndTime = currentDate + ' ' + currentTime;
    //nlapiLogExecution('DEBUG', 'Current Date in function ', currentDateAndTime);
    return currentDateAndTime;
}