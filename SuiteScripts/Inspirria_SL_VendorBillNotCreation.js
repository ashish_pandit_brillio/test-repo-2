/**
 * @NApiVersion 2.x
 * @NScriptType Suitelet
 */

var PAGE_SIZE = 1000;
var SEARCH_ID = 'customsearch_vendorbill_not_created';

define(['N/ui/serverWidget', 'N/search', 'N/redirect', 'N/file', 'N/encode'],
		function (serverWidget, search, redirect, file, encode) {
	function onRequest(context) {
		if (context.request.method == 'GET') {
			var form = serverWidget.createForm({
				title: 'Vendor Bill not created for invoices',
				hideNavBar: false
			});

			//	form.clientScriptFileId = CLIENT_SCRIPT_FILE_ID;

			// Get parameters
			var pageId = parseInt(context.request.parameters.page);
			var scriptId = context.request.parameters.script;
			var deploymentId = context.request.parameters.deploy;

			/*	var startDate = context.request.parameters.startdate;
			var endDate = context.request.parameters.endDate;
			var sub = context.request.parameters.subsidiary;
			var PrjStatus = context.request.parameters.jobstatus;
			var custoFilt = context.request.parameters.customer;
			var jobFilt = context.request.parameters.job;*/

			form.addSubmitButton({
				id: 'exportexcelreport',
				label: 'Export Excel'
			});


			// Add sublist that will show results
			var sublist = form.addSublist({
				id: 'sublist',
				type: serverWidget.SublistType.LIST,//,INLINEEDITOR
				label: 'Report'
			});

			// Add columns to be shown on Page

			sublist.addField({
				id: 'empid',
				label: 'Employee id',
				type: serverWidget.FieldType.TEXT
			});

			sublist.addField({
				id: 'empname',
				label: 'Employee Name',
				type: serverWidget.FieldType.TEXT
			});
			sublist.addField({
				id: 'venname',
				label: 'Vendor Name',
				type: serverWidget.FieldType.TEXT
			});
			sublist.addField({
				id: 'totalhrs',
				label: '# of hours',
				type: serverWidget.FieldType.TEXT
			});
			sublist.addField({
				id: 'payrate',
				label: 'Pay rate',
				type: serverWidget.FieldType.TEXT
			});
			sublist.addField({
				id: 'fromdate',
				label: 'Billing From Date',
				type: serverWidget.FieldType.TEXT
			});
			sublist.addField({
				id: 'todate',
				label: 'Billing To date',
				type: serverWidget.FieldType.TEXT
			});
			sublist.addField({
				id: 'docnumber',
				label: 'Document Number',
				type: serverWidget.FieldType.TEXT
			});
			sublist.addField({
				id: 'failureresson',
				label: 'Bill Failure Reason',
				type: serverWidget.FieldType.TEXT
			});
			// Run search and determine page count
			var PO_ID = '';
			var  ss= EmployeeSearch();
			var empSearch = ss.resultsofData;
			var IDArray = ss.resultsID;

			var groupResult_empSearch = empSearch.reduce(function (r, a) {
				r[a.Employee_ID] = r[a.Employee_ID] || [];
				r[a.Employee_ID].push(a);
				return r;
			}, Object.create(null));

			PO_ID = runSearch(SEARCH_ID,IDArray);
			//log.debug("empSearch",JSON.stringify(empSearch));
			var s=0;
			for (var j = 0; j < PO_ID.length; j++) {
				
				if(PO_ID[j].Employee_ID)
				{
					sublist.setSublistValue({
						id: 'empid',
						line: s,
						value: PO_ID[j].Employee_ID
					});
				}
				else{
					sublist.setSublistValue({
						id: 'empid',
						line: s,
						value: 'NA'
					});
				}

				sublist.setSublistValue({
					id: 'empname',
					line: s,
					value: PO_ID[j].employeename
				});

				
				for (var key in groupResult_empSearch) {
					if(key == PO_ID[j].Employee_ID)
					{
						for(var i=0;i<groupResult_empSearch[key].length;i++)
						{
							if(new Date(groupResult_empSearch[key][i].startdate) <= new Date(PO_ID[j].Billing_To) && new Date(groupResult_empSearch[key][i].enddate) >= new Date(PO_ID[j].Billing_From))
							{
								//log.debug("Vendor "+ empSearch[key][i].vendor+"Employee_ID "+Employee_ID);
								sublist.setSublistValue({
									id: 'venname',
									line: s,
									value: groupResult_empSearch[key][i].vendor
								});

							}
							else
							{
								//log.debug("Vendor NA Employee_ID ",Employee_ID);
								sublist.setSublistValue({
									id: 'venname',
									line: s,
									value: "NA"
								});

							}
						}    
					}
				}

				if(PO_ID[j].Quantity){
					sublist.setSublistValue({
						id: 'totalhrs',
						line: s,
						value: PO_ID[j].Quantity
					});	
				}else
				{
					sublist.setSublistValue({
						id: 'totalhrs',
						line: s,
						value: 'NA'
					});
				}

				if(PO_ID[j].Item_Rate)	{
					sublist.setSublistValue({
						id: 'payrate',
						line: s,
						value:PO_ID[j].Item_Rate
					});	
				}else{
					sublist.setSublistValue({
						id: 'payrate',
						line: s,
						value:'NA'
					});
				}
				if(PO_ID[j].Billing_From){
					sublist.setSublistValue({
						id: 'fromdate',
						line: s,
						value: PO_ID[j].Billing_From
					});
				}else
				{
					sublist.setSublistValue({
						id: 'fromdate',
						line: s,
						value: 'NA'
					});
				}
				if(PO_ID[j].Billing_To){
					sublist.setSublistValue({
						id: 'todate',
						line: s,
						value: PO_ID[j].Billing_To
					});
				}
				else{

					sublist.setSublistValue({
						id: 'todate',
						line: s,
						value: 'NA'
					});
				}
				if(PO_ID[j].Document_Number){
					sublist.setSublistValue({
						id: 'docnumber',
						line: s,
						value: PO_ID[j].Document_Number
					});
				}
				else{
					sublist.setSublistValue({
						id: 'docnumber',
						line: s,
						value: 'NA'
					});

				}
				if(PO_ID[j].bill_failure_reason){
					sublist.setSublistValue({
						id: 'failureresson',
						line: s,
						value: PO_ID[j].bill_failure_reason
					});
				}
				else
				{
					sublist.setSublistValue({
						id: 'failureresson',
						line: s,
						value: 'NA'
					});
				}
				s++;
				context.response.writePage(form);

			}

		}
		else {
			var delimiter1 = /\u0001/;
			var delimiter = /\u0002/;
			var SO_ID = [];
			var sublistData = context.request.parameters.sublistdata.split(delimiter);
			//log.debug('length', sublistData.length);
			var hardHeaders = ["EMPLOYEE ID", "EMPLOYEE NAME","VENDOR NAME" ,"# OF HOURS", "PAY RATE","BILLING FROM DATE" ,"BILLING TO DATE ","DOCUMENT NUMBER","BILL FAILURE REASON"]
			var CSVData = '<?xml version="1.0"?><?mso-application progid="Excel.Sheet"?>';
			CSVData += '<Workbook xmlns="urn:schemas-microsoft-com:office:spreadsheet" ';
			CSVData += 'xmlns:o="urn:schemas-microsoft-com:office:office" ';
			CSVData += 'xmlns:x="urn:schemas-microsoft-com:office:excel" ';
			CSVData += 'xmlns:ss="urn:schemas-microsoft-com:office:spreadsheet" ';
			CSVData += 'xmlns:html="http://www.w3.org/TR/REC-html40">';

			CSVData += '<Styles>';

			CSVData += '<Style ss:ID="s1">';
			CSVData += '<Font ss:Bold="1" ss:Underline="Single"/>';
			CSVData += '<Interior ss:Color="#CCFFFF" ss:Pattern="Solid"/>';
			CSVData += ' <Borders>';
			CSVData += ' <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>';
			CSVData += '</Borders>';
			CSVData += '</Style>';

			CSVData += '</Styles>';

			CSVData += '<Worksheet ss:Name="Report">';
			CSVData += '<Table><Row>';
			for (var k = 0; k < hardHeaders.length; k++) {
				CSVData += '<Cell ss:StyleID="s1"><Data ss:Type="String">' + hardHeaders[k] + '</Data></Cell>';
			}
			CSVData += '</Row>';
			for (var i = 0; i < sublistData.length; i++) {
				var sublistDataDetails = sublistData[i].split(delimiter1);
				CSVData += '<Row>';
				CSVData += '<Cell><Data ss:Type="String">' + sublistDataDetails[0] + '</Data></Cell>';
				CSVData += '<Cell><Data ss:Type="String">' + sublistDataDetails[1] + '</Data></Cell>';
				CSVData += '<Cell><Data ss:Type="String">' + sublistDataDetails[2] + '</Data></Cell>';
				CSVData += '<Cell><Data ss:Type="String">' + sublistDataDetails[3] + '</Data></Cell>';
				CSVData += '<Cell><Data ss:Type="String">' + sublistDataDetails[4] + '</Data></Cell>';
				CSVData += '<Cell><Data ss:Type="String">' + sublistDataDetails[5] + '</Data></Cell>';
				CSVData += '<Cell><Data ss:Type="String">' + sublistDataDetails[6] + '</Data></Cell>';
				CSVData += '<Cell><Data ss:Type="String">' + sublistDataDetails[7] + '</Data></Cell>';
				CSVData += '<Cell><Data ss:Type="String">' + sublistDataDetails[8] + '</Data></Cell>';
				CSVData += '</Row>';
			}
			CSVData += '</Table></Worksheet></Workbook>';
			var strXmlEncoded = encode.convert({
				string: CSVData,
				inputEncoding: encode.Encoding.UTF_8,
				outputEncoding: encode.Encoding.BASE_64
			});

			var objXlsFile = file.create({
				name: 'Vendor bill Report.xls',
				fileType: file.Type.EXCEL,
				contents: strXmlEncoded
			});
			context.response.writeFile({
				file: objXlsFile
			});
		}
	}

	return {
		onRequest: onRequest
	};

	function runSearch(searchId,IDArray) {
		var searchObj = search.load({
			id: searchId
		});

		if(IDArray)
		{
			//var temp =['101193-Naveen Ramesh Donthi','103175-Vishnu Bangar Raju C'];
		//	IDArray = IDArray.concat(temp);
			var uniq = IDArray.reduce(function(a,b){
				if (a.indexOf(b) < 0 ) a.push(b);
				return a;
			},[]);
			var st ='';
			for(var l=0;l<uniq.length;l++)
			{
				if(uniq.length-1 == l)
				{
					st+= "{custcol_employeenamecolumn} = '"+uniq[l]+"'";
				}
				else
				{
					st+= "{custcol_employeenamecolumn} = '"+uniq[l]+"' OR ";
				}
			}
			log.debug("st",st);
			//var str = uniq.toString();

			searchObj.filters.push(search.createFilter({
				name : 'formulatext',
				operator : search.Operator.IS,
				values : [1],
				formula : "CASE WHEN "+st+" THEN '1' ELSE '0' END" //"CASE WHEN {custcol_employeenamecolumn} = '"+str+"' THEN '1' ELSE '0' END"
			})); 

		}
		var results = [];
		var count = 0;
		var pageSize = 1000;
		var start = 0;
		do {
			var subresults = searchObj.run().getRange({
				start: start,
				end: start + pageSize
			});

			results = results.concat(subresults);
			count = subresults.length;
			start += pageSize;
		} while (count == pageSize);
		log.debug('resultsresultsresults Nihal', results.length);

		var resultsofData = new Array();
		for (var res = 0; res < results.length; res++) {


			var employeename = results[res].getValue({
				name: "custcol_employeenamecolumn",
				summary: "GROUP",
				label: "Employee Name"
			});
			var Employee_ID = results[res].getValue({
				name: "custcol_employee_entity_id",
				summary: "GROUP",
				sort: search.Sort.ASC,
				label: "Employee ID"
			});
			var Quantity = results[res].getValue({
				name: "quantity",
				summary: "SUM",
				label: "Quantity"
			});
			var Item_Rate = results[res].getValue({
				name: "rate",
				summary: "AVG",
				label: "Item Rate"
			});
			var Billing_From = results[res].getValue({
				name: "custbody_billfrom",
				summary: "GROUP",
				label: "Billing From"
			});
			var Billing_To = results[res].getValue({
				name: "custbody_billto",
				summary: "GROUP",
				label: "Billing To"
			});
			var Document_Number = results[res].getValue({
				name: "tranid",
				summary: "GROUP",
				label: "Document Number"
			});
			var bill_failure_reason = results[res].getValue({
				name: "custbody_bill_failure_reason",
				summary: "GROUP",
				label: "Vendor Bill Failure Reason"
			});
			resultsofData.push({
				"employeename": employeename,
				"Employee_ID": Employee_ID,
				"Quantity": Quantity,
				"Item_Rate": Item_Rate,
				"Billing_From": Billing_From,
				"Billing_To": Billing_To,
				"Document_Number": Document_Number,
				"bill_failure_reason":bill_failure_reason
			});
		}
			log.debug("resultsofData",JSON.stringify(resultsofData));
		return resultsofData;
	}
	function EmployeeSearch()
	{
		var searchObj = search.load({
			id: 'customsearch3323'
		});

		var results = [];
		var count = 0;
		var pageSize = 1000;
		var start = 0;
		do {
			var subresults = searchObj.run().getRange({
				start: start,
				end: start + pageSize
			});

			results = results.concat(subresults);
			count = subresults.length;
			start += pageSize;
		} while (count == pageSize);
		//log.debug('resultsresultsresults', results.length);
		var resultsID = new Array();
		var resultsofData = new Array();
		for (var res = 0; res < results.length; res++) {
			var internalid = results[res].getValue({
				name: "internalid",
				summary: "GROUP",
				sort: search.Sort.ASC,
				label: "Internal ID"
			});
			var Name = results[res].getValue({
				name: "entityid",
				summary: "GROUP",
				label: "Name"
			});

			var Vendor = results[res].getText({
				name: "custrecord_stvd_vendor",
				join: "CUSTRECORD_STVD_CONTRACTOR",
				summary: "GROUP",
				label: "Vendor"
			});
			var stDate =  results[res].getValue({
				name: "custrecord_stvd_start_date",
				join: "CUSTRECORD_STVD_CONTRACTOR",
				summary: "GROUP",
				label: "Start Date"
			});
			var edDate =  results[res].getValue({
				name: "custrecord_stvd_end_date",
				join: "CUSTRECORD_STVD_CONTRACTOR",
				summary: "GROUP",
				label: "End Date"
			});
			var Employee_ID =  results[res].getValue({
				name: "formulatext",
				summary: "GROUP",
				formula: "SUBSTR({entityid},0,INSTR({entityid},'-',1)-1)",
				label: "Formula (Text)"
			});
			resultsofData.push({
				"internalid": internalid,
				"vendor": Vendor,
				"startdate":stDate,
				"enddate":edDate,
				"Employee_ID":Employee_ID
			});
			resultsID.push(Name);
		}

		return {"resultsofData":resultsofData,"resultsID":resultsID}
	}
	function GetVendor(Employee_ID,Billing_From,Billing_To,empSearch)
	{
		var venName = "";
		for (var key in empSearch) {
			if(key == Employee_ID)
			{
				for(var i=0;i<empSearch[key].length;i++)
				{
					if(new Date(empSearch[key][i].startdate) <= new Date(Billing_To) && new Date(empSearch[key][i].enddate) >= new Date(Billing_From))
					{
						//log.debug("Vendor "+ empSearch[key][i].vendor+"Employee_ID "+Employee_ID);
						return empSearch[key][i].vendor;
					}
					else
					{
						//	log.debug("Vendor NA Employee_ID ",Employee_ID);
						return 'NA';
					}
				}    
			}
		}
	}
});