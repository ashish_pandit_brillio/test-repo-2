/**
 * @NApiVersion 2.0
 * @NScriptType MapReduceScript
 */
// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
    /*
       	Script Name: MRS_Delete Time sheets Send Notification
    	Author: Praveena Madem
    	Company: Inspirria Cloud Tech
    	Date: 04/30/2020
		Description : Script is used to send the email notifications of timesheet summary to operation team,timesheet team if required sent to billing team also in case employee has to terminate.
					  1.getInputData() : This Script will run every 12 hours to fetch failed Fusion- Employee JSON Master records while termiating due to timesheets are exists from the last 14 hours 
					  2. map : Group the Timebill Records with employee record.
					  3.Reduce : Fetch data from map and delete timebill,delete/update the timesheets and send notification timesheets summary

    	Script Modification Log:

    	-- Date --			-- Modified By --				--Requested By--				-- Description --
    	



    Below is a summary of the process controls enforced by this script file.  The control logic is described
    more fully, below, in the appropriate function headers and code blocks.


         MAP/REDUCE FUNCTION
    		- getInputData()
    		- map()
    		- reduce()
    		- summarize()


         SUB-FUNCTIONS
    		- The following sub-functions are called by the above core functions in order to maintain code
                modularization:

                   - formatDate()
    			   - search_TimeBill()
    			   - _search_employee()
    			   -_sendEmailTotimesheetTeam()
    			   

    */
}
// END SCRIPT DESCRIPTION BLOCK  ====================================



// BEGIN FUNCTION =============================================
define(['N/search', 'N/record', 'N/email', 'N/runtime', 'N/error', 'N/format'],
    function(search, record, email, runtime, error, format) {

        //Returns timestamp
        function timestamp(currentTime) {
            var str = "";
            var hours = currentTime.getHours();

            var minutes = currentTime.getMinutes();
            var seconds = currentTime.getSeconds();
            var meridian = "";
            if (hours > 12) {
                meridian += "pm";
            } else {
                meridian += "am";
            }
            if (hours > 12) {

                hours = hours - 12;
            }
            if (minutes < 10) {
                minutes = "0" + minutes;
            }
            if (seconds < 10) {
                seconds = "0" + seconds;
            }
            //str += hours + ":" + minutes + ":" + seconds + " ";
            str += hours + ":" + minutes + " ";
            return str + meridian;
        }


        //Get current date
        function sysDate(date) {

            var tdate = date.getDate();
            var month = date.getMonth() + 1; // jan = 0
            var year = date.getFullYear();
            return currentDate = month + '/' + tdate + '/' + year;
        }



		//Fetch failed Fusion- Employee JSON Master records while termiating due to timesheets are exists from the last 14 hours
        function _search_employee() {

            var fromtime = new Date();
            fromtime.setHours(fromtime.getHours() - 14);//SET HOURS as -14 hours 
            var todate = new Date(); //Current Date


            var fromDateAndTime = sysDate(fromtime) + ' ' + timestamp(fromtime); //FROM DATE/TIME
            var toDateAndTime = sysDate(todate) + ' ' + timestamp(todate); //TO DATE/TIME
            log.debug('fromDateAndTime', fromDateAndTime);
            log.debug('toDateAndTime', toDateAndTime);
			
			//Search on  Fusion- Employee JSON Master records 
            var customrecord_fi_employee_json_masterSearchObj = search.create({
                type: "customrecord_fi_employee_json_master",
                filters: [
                    [
                        ["custrecord_fi_json_error_log", "contains", "Employee has already filled timesheets for this period for this allocation"], "OR", ["custrecord_fi_json_error_log", "contains", "Timesheet(s) for this employee has been already"]
                    ],
                    "AND",
                    ["custrecord_fu_emp_id.custentity_employee_inactive", "is", "F"],
                    "AND",
                    ["custrecord_fi_fit_type", "anyof", "3"], 'AND',
                    ['lastmodified', 'within', fromDateAndTime, toDateAndTime] //Added by praveena
                ],
                columns: [
                    search.createColumn({
                        name: "internalid"
                    })
                    /*  search.createColumn({name: "custrecord_fi_fusion_json_data", label: "Fusion JSON Data"}),
      search.createColumn({name: "custrecord_fi_json_error_log", label: "Error Log"}),
      search.createColumn({name: "custrecord_fi_fit_type", label: "Type"}),
      search.createColumn({name: "custrecord_fu_emp_id", label: "Employee ID"}),
      search.createColumn({name: "custrecord_date_n_time", label: "Date and Time"}),
      search.createColumn({name: "custrecord_fi_fusion_id", label: "Fusion ID"})
	  */
                ]
            });
            /*var searchResultCount = customrecord_fi_employee_json_masterSearchObj.runPaged().count;
            log.debug("customrecord_fi_employee_json_masterSearchObj result count",searchResultCount);
            customrecord_fi_employee_json_masterSearchObj.run().each(function(result){
               // .run().each has a limit of 4,000 results
               return true;
            });
            */
            return customrecord_fi_employee_json_masterSearchObj;


        }


        //Function is used to search the resource allocation records 
        function _search_ResourceAllocation(i_emp_id, termDate) {
            var resourceAllocationSearch = search.create({
                type: "resourceallocation",
                filters: [
                    ["employee.internalid", "anyof", i_emp_id],
                    "AND",
                    ["enddate", "onorafter", termDate],
                ],
                columns: [
                    search.createColumn({
                        name: "internalid"
                    }),
                    search.createColumn({
                        name: "startdate"
                    }),
                ]
            });
            return resourceAllocationSearch;
        }



		 // Search Time Bill based on employee,type and date
        // And Get the columns Timesheet's Start date , Employee, Status ,Is billable, Approval Status ,Internal id ,approved , Timesheet's Internalid and Timesheet Approval status 
       
        function search_TimeBill(i_emp_id, termdate) {

            log.debug("search_TimeBill", "start");

            var o_Timebillsearch = search.create({
                type: "timebill",
                filters: [
                    ["employee", "anyof", i_emp_id],
                    "AND",
                    ["type", "anyof", "A"],
                    "AND",
                    ["date", "after", termdate]
                ],
                columns: [
                    search.createColumn({
                        name: "startdate",
                        join: "timeSheet",
                        sort: search.Sort.ASC
                    }), //,
                    search.createColumn({
                        name: "employee"
                    }),
                    search.createColumn({
                        name: "status"
                    }),
                    search.createColumn({
                        name: "billable"
                    }),
                    search.createColumn({
                        name: "approvalstatus"
                    }),
                    search.createColumn({
                        "name": 'internalid'
                    }),
                    search.createColumn({
                        "name": 'approved'
                    }),
                    search.createColumn({
                        "name": 'internalid',
                        join: "timeSheet"
                    }),
                    search.createColumn({
                        name: "approvalstatus",
                        join: "timeSheet"
                    }),
                ]
            });
            try {

            } catch (e) {
                log.debug("error", e);

            }
            return o_Timebillsearch;
        }//END search_TimeBill function

        function formatDate(fusionDate) {

            if (fusionDate) {
                // fusionDate = "2016-07-14T00:00:00.000Z";
                var datePart = fusionDate.split('T')[0];
                var dateSplit = datePart.split('-');
                // MM/DD/ YYYY
                var netsuiteDateString = dateSplit[1] + "/" + dateSplit[2] + "/" +
                    dateSplit[0];
                return netsuiteDateString;
            }
            return "";
        }//END formatDate function


        // Update the subtier records
        // Get all subtier records that end after the LWD

        function _search_subtier_vendor_data(i_emp_id, termDate) {
            var subtierSearch = search.create({
                type: "customrecord_subtier_vendor_data",
                filters: [
                    ["custrecord_stvd_contractor", "anyof", i_emp_id],
                    "AND",
                    ["custrecord_stvd_end_date", "onorafter", termDate],
                ],
                columns: [
                    search.createColumn({
                        name: "internalid"
                    })
                ]
            });
            return subtierSearch;
        }




        //Send Email notification with Timesheets Summary , Added by praveena on 31-03-2020
        function _sendEmailTotimesheetTeam(s_emp_name, s_termDate, data, recipientId, subject, startofbody) {

            try {
                var strVar = "";
                strVar += startofbody;
                strVar += "<p>Termination Date: " + s_termDate + " (MM/DD/YYYY)<\/p>";
                strVar += "<p>Timesheets &ndash;<\/p>";
                strVar += "<table border=\"1\" cellspacing=\"0\" style=\"table-layout: fixed;\">";
                strVar += "<tbody>";
                strVar += "<tr>";
                strVar += "<td style=\"width: 200px;\">";
                strVar += "<p><strong>Resource<\/strong><\/p>";
                strVar += "<\/td>";
                strVar += "<td style=\"width: 82px;\">";
                strVar += "<p><strong>Timesheet ID<\/strong><\/p>";
                strVar += "<\/td>";
                strVar += "<td style=\"width: 113px;\">";
                strVar += "<p><strong>Week Start Date<\/strong><\/p>";
                strVar += "<\/td>";
                strVar += "<td style=\"width: 112px;\">";
                strVar += "<p><strong>Timesheet Status<\/strong><\/p>";
                strVar += "<\/td>";
                strVar += "<td style=\"width: 112px;\">";
                strVar += "<p><strong>Billing Status<\/strong><\/p>";
                strVar += "<\/td>";
                strVar += "<\/tr>";
                strVar += data;
                strVar += "<\/tbody>";
                strVar += "<\/table>";
                strVar += "<p>&nbsp;<\/p>";
                strVar += "<p>If you need any additional support, please reach out to information systems team.<\/p>";
                strVar += "<p>&nbsp;<\/p>";
                strVar += "<p>Thanks,<\/p>";
                strVar += "<p>Information Systems.<\/p>";

                var senderId = 200580;
 //SEND EMAIL Notification
                email.send({
                    author: senderId,
                    recipients: recipientId,
                    subject: subject,
                    body: strVar,
                    cc: ['information.systems@brillio.com'],
                    attachments: null,
                    relatedRecords: {
                        entityId: recipientId
                    }
                });
                log.debug('sentemail');

            } catch (e) {

                log.debug('error while sending email', e);
            }
        }//end  _sendEmailTotimesheetTeam function


		//BEGIN getInputData
        function getInputData() {
            return _search_employee()//return the resulted data of Fusion- Employee JSON Master records
        }//END getInputData

		//BEGIN map
        function map(context) {
            log.debug('context', context.value);

            var o_values = JSON.parse(context.value);//Parse the OBJECT
            var i_termination_Id = o_values.id; // Fetch the Fusion- Employee JSON Master record id
			
			//Load The Fusion- Employee JSON Master record
		   var o_termination_record = record.load({
                type: 'customrecord_fi_employee_json_master',
                id: i_termination_Id,
            });

            log.debug('i_termination_Id', i_termination_Id);

            log.debug('lastmodified', o_termination_record.getValue({
                'fieldId': 'lastmodified'
            }));
            var i_employeeId = o_termination_record.getValue({//Get the Employee internalid
                'fieldId': 'custrecord_fu_emp_id'
            });
            var s_employee = o_termination_record.getText({//Get the Employee name
                'fieldId': 'custrecord_fu_emp_id'
            });
            var s_emp_term = o_termination_record.getValue({//Get the JSON data 
                'fieldId': 'custrecord_fi_fusion_json_data'
            });
            var o_emp_term_data = JSON.parse(s_emp_term);
            var o_emp_data = o_emp_term_data.Assignment;
            var i_fusionNumber = o_emp_data.FusionID;
            var d_termDate = o_emp_data.Termination_Date;
            d_termDate = d_termDate ? formatDate(d_termDate) : d_termDate;//Formatt the Termination_Date 
            log.debug('i_employeeId', i_employeeId);
            log.debug('d_termDate', d_termDate);



            var resourceAllocations_ids = []; //search_Resourceallocations(i_employeeId,s_terminationdate);


            var obj_ra_search = _search_ResourceAllocation(i_employeeId, d_termDate);

            var obj_ra_searchCount = obj_ra_search.runPaged().count;
            log.debug("obj_ra_searchCount result count", obj_ra_searchCount);

            if (obj_ra_searchCount > 0) {

                var obj_ra_searchResult = obj_ra_search.run().getRange({
                    start: 0,
                    end: obj_ra_searchCount
                });
                log.debug("obj_ra_searchResult result count", obj_ra_searchResult.length);
                for (var j = 0; obj_ra_searchResult.length > j; j++) {
                    resourceAllocations_ids.push({
                        'id': obj_ra_searchResult[j].getValue({
                            "name": 'internalid'
                        }),
                        'startdate': obj_ra_searchResult[j].getValue({
                            "name": 'startdate'
                        })
                    });

                }

            }

            //log.debug("obj_ra_searchCount result count",obj_ra_searchCount);
            resourceAllocations_ids = resourceAllocations_ids ? resourceAllocations_ids : [];

            var subteir_search = _search_subtier_vendor_data(i_employeeId, d_termDate);
            var obj_subteir_searchCount = subteir_search.runPaged().count;
            log.debug("obj_subteir_searchCount result count", obj_subteir_searchCount);
            var a_subteir_ids = [];
            if (obj_subteir_searchCount > 0) {

                var obj_subteir_searchResult = subteir_search.run().getRange({
                    start: 0,
                    end: obj_ra_searchCount
                });
                log.debug("obj_subteir_searchResult result count", obj_subteir_searchResult.length);
                for (var q = 0; obj_subteir_searchResult.length > q; q++) {

                    a_subteir_ids.push({
                        'id': obj_subteir_searchResult[q].getValue({
                            "name": 'internalid'
                        })


                    })

                }
            }
            a_subteir_ids = a_subteir_ids ? a_subteir_ids : [];




            var obj_pair = {};
            var o_timebill = {};

            var obj_search_data = search_TimeBill(i_employeeId, d_termDate);//Search the Timebill records based on employee and termiation date

            var searchResultCount = obj_search_data.runPaged().count;//Get the count of searched timebill results
            log.debug("searchResultCount result count", searchResultCount);
            var o_timebill = {};
            if (searchResultCount > 0) {

                //obj_search_data.run().each(function(result){

                var o_default_val_searchResult = obj_search_data.run().getRange({
                    start: 0,
                    end: searchResultCount
                });
                //	log.debug("o_default_val_searchResult result count",o_default_val_searchResult.length);
                for (var i = 0; o_default_val_searchResult.length - 1 >= i; i++) {


                    var i_tb_id = o_default_val_searchResult[i].getValue({
                        "name": 'internalid'
                    });
                    var s_timesheetdate = o_default_val_searchResult[i].getValue({
                        "name": "startdate",
                        "join": "timeSheet",
                        "sort": search.Sort.ASC
                    });

                    if (i == 0) {
                        o_timebill[s_timesheetdate] = [];

                        //		log.debug('o_timebill[s_timesheetdate]',o_timebill[s_timesheetdate]);

                    }
                    if (i != 0) {
                        var s_previous_s_timesheetdate = o_default_val_searchResult[i - 1].getValue({
                            "name": "startdate",
                            "join": "timeSheet",
                            "sort": search.Sort.ASC
                        });

                        if (s_timesheetdate != s_previous_s_timesheetdate) {
                            o_timebill[s_timesheetdate] = [];

                            //log.debug('i!==0',s_timesheetdate);
                        }
                    }


                     //Fetch the Timebill columns
                    var timebill_obj = {
                        "id": i_tb_id,//Timebill Internal id
                        "timesheetdate": s_timesheetdate,//Timesheet Start Date
                        "tb_approvalstatus": o_default_val_searchResult[i].getText({//Timebill approvalstatus
                            name: "approvalstatus"
                        }),
                        "ts_approvalstatus": o_default_val_searchResult[i].getText({//Timesheet approvalstatus
                            "name": 'approvalstatus',
                            join: "timeSheet"
                        }),
                        "ts_id": o_default_val_searchResult[i].getValue({//Timesheet Internalid
                            "name": 'internalid',
                            join: "timeSheet"
                        }),

                        "billable": o_default_val_searchResult[i].getValue({//timebill billable
                            "name": 'billable'
                        }),
                        "approved": o_default_val_searchResult[i].getValue({//timebill Approved
                            "name": 'approved'
                        }),
                        "tb_status": o_default_val_searchResult[i].getValue({//timebill Status
                            "name": 'status'
                        })
                    };

                    o_timebill[s_timesheetdate].push(timebill_obj); //Push the timebill values into array.

                    //log.debug("JSON result",JSON.stringify(o_timebill));
                } //ENd timebill for loop

            }



            //	log.debug('o_timebill',o_timebill);
            obj_pair.s_emp = s_employee;//Asign employee
            obj_pair.o_timebill = o_timebill;//Asign timebill object
            obj_pair.resourceAllocations_ids = resourceAllocations_ids;
            obj_pair.i_termination_Id = o_values.id;; //Asign Fusion- Employee JSON Master internalid
            obj_pair.s_termDate = d_termDate; //Asign termination date
            obj_pair.a_subteir_ids = a_subteir_ids;
            //log.debug('obj_pair',JSON.stringify(obj_pair));
			
			
			
            //This will do key pair values (key is employee id and value is related data of employee) and its writes the date to next stage 
            context.write({
                    key: i_employeeId,
                    value: obj_pair
                }

            );



        } //END map FUNCTION 

		//BEGIN reduce FUNCTION 
        function reduce(context) {
            var obj = JSON.parse(context.values[0]);//Parse the object

            var customerId = context.key;//Assign the EMployee id

            var o_timebill = obj.o_timebill;//Assign the Timebill Object
            var a_ra_ids = obj.resourceAllocations_ids;
            var i_termination_Id = obj.i_termination_Id;//Assign the Fusion- Employee JSON Master record internalid
            var s_termDate = obj.s_termDate;//Assign termination Date
            var d_termDate = format.parse({ //Convert the termiation date intlo Date format
                value: s_termDate,
                type: format.Type.DATE
            });


            var s_emp_name = obj.s_emp;//employee name
            log.debug('stage: reduce , s_termDate : d_termDate', s_termDate + " : " + d_termDate);
            log.debug('stage: a_ra_ids', a_ra_ids);

            var a_deleted_data = [];
            var previous_ts_startdate = '';
            var send_billing_team = false;
            var strVar = '';
            var delete_strVar1 = '';
            if (o_timebill) {
                //log.debug('o_timebill',o_timebill[]);
                for (x in o_timebill) {
                    log.debug('o_timebill====OBJ', o_timebill[x].length);

                    var o_tb = o_timebill[x];
                    var count = 0;

                    var is_tb_delted = false;
                    var ts_id = '';
                    //for (y in o_tb)
                    log.debug('o_timebill====OBJ', o_timebill[x].length);
				
				
					/*
                     *
                     * Delete the Timebill and Timesheet
                     * Here System will capture the timesheet summary while deletion and if any error occurs while deleting timesheets or if any billable timeentries are exists
                     */
				
                    for (var y = 0; o_tb.length > y; y++) {
                        ts_id = o_tb[y].ts_id;


                        if (o_tb[y].tb_status == 'Billed' && o_tb[y].approved == true) //SEND TIMESHEET DATA TO BILLING TEAM (Billed)
                        //if(send_billing_team==true)
                        {

                            log.debug('id========o_tb[y].billable', o_tb[y].billable);
                            log.debug('id========o_tb[y].approved', o_tb[y].approved);
                            log.debug('id========o_tb[y].tb_status', o_tb[y].tb_status);
                            log.debug('id========id', o_tb[y].id);

                            send_billing_team = true;

                            //Send the Data to billing Team
                            strVar += "<tr>";
                            strVar += "<td style=\"width: 200px;\">" + s_emp_name + "<\/td>";
                            strVar += "<td style=\"width: 82px;\" align=\"center\">" + o_tb[y].ts_id + "<\/td>";
                            strVar += "<td style=\"width: 113px;\" align=\"center\">" + o_tb[y].timesheetdate + "<\/td>";
                            strVar += "<td style=\"width: 112px;\">" + o_tb[y].ts_approvalstatus + "<\/td>";
                            strVar += "<td style=\"width: 112px;\" align=\"center\" >" + o_tb[y].tb_status + "<\/td>";
                            strVar += "<\/tr>";


                            log.debug('ts_approvalstatus========true true', o_tb[y].ts_approvalstatus);

                            break;


                        } else

                        {
                            //log.debug('id========else else', o_tb[y].id);

                            //Delete the timebill records and timesheets
                            //	log.debug('id',o_tb[y].id);
                            //	log.debug('timesheetdate',o_tb[y].timesheetdate);
                            //log.debug('s_emp_name',s_emp_name);
                            //log.debug('o_tb[y].ts_id',o_tb[y].ts_id);
                            //log.debug('o_tb',o_tb);
                            try {
								
								
								
								//DELETE the Timebill
                                record.delete({
                                    type: 'timebill',
                                    id: o_tb[y].id
                                }); //usage is 10 Units
                                log.debug('deleted' + count);
                                //Delete Timesheets.
                                is_tb_delted = true;

                                if (count == 0) {
									
									 //Capture the Deleted Timesheet summary
                                    delete_strVar1 += "<tr>";
                                    delete_strVar1 += "<td style=\"width: 200px;\">" + s_emp_name + "<\/td>";
                                    delete_strVar1 += "<td style=\"width: 82px;\" align=\"center\">" + o_tb[y].ts_id + "<\/td>";
                                    delete_strVar1 += "<td style=\"width: 113px;\" align=\"center\">" + o_tb[y].timesheetdate + "<\/td>";
                                    delete_strVar1 += "<td style=\"width: 112px;\">" + o_tb[y].ts_approvalstatus + "<\/td>";
                                    delete_strVar1 += "<td style=\"width: 112px;\" align=\"center\">" + o_tb[y].tb_status + "<\/td>";
                                    delete_strVar1 += "<\/tr>";

                                }

                            } catch (e) {
                                is_tb_delted = false;
								
								  //Capture the error Timesheet summary
                                strVar += "<tr>";
                                strVar += "<td style=\"width: 200px;\">" + s_emp_name + "<\/td>";
                                strVar += "<td style=\"width: 82px;\" align=\"center\">" + o_tb[y].ts_id + "<\/td>";
                                strVar += "<td style=\"width: 113px;\" align=\"center\">" + o_tb[y].timesheetdate + "<\/td>";
                                strVar += "<td style=\"width: 112px;\">" + o_tb[y].ts_approvalstatus + "<\/td>";
                                strVar += "<td style=\"width: 112px;\" align=\"center\">" + o_tb[y].tb_status + "<\/td>";
                                strVar += "<\/tr>";
                                log.debug('error while deleting timebill   ' + o_tb[y].id, e);
                                break;


                            }
                        }

                        //previous_ts_startdate=o_tb;//Assign Previous Value as cuurent one

                        count++;

                    }//End for loop  Timebill records per timesheet


                    if (is_tb_delted) {
                        try {

							//Use Look up to fetch the total hours
                            log.debug('Before delete TS ts_id', ts_id);
                            var fieldLookUp = search.lookupFields({
                                type: 'timesheet',
                                id: ts_id,
                                columns: ['totalhours']
                            });

                            log.debug('fieldLookUp.totalhours', fieldLookUp["totalhours"]);
                            log.debug('fieldLookUp.totalhours==0', fieldLookUp["totalhours"] == "0:00");
							
                            if (fieldLookUp["totalhours"] == "0:00") {//Check Timesheet hours
								
								//IF Related TIME BILL RECORDS are then delete timesheet
                                record.delete({
                                    type: 'timesheet',
                                    id: ts_id
                                });
                            }
                        } catch (e) {
                            log.debug('While DELETE TIMesheet Error', e);
                        }

                    }// End if (is_tb_delted)

                }
            }

//if any timesheet is billed Add billing team along with Timesheet and Oprations team else  recipient will be business operations team and timesheet team
                
            var recipientId = send_billing_team == true ? ['billing@brillio.com', 'timesheet@brillio.com', 'business.ops@brillio.com'] : ['timesheet@brillio.com', 'business.ops@brillio.com']


//Send Error Timesheet Summary 
            if (strVar) {
                var subject = "Termination Update : Resource Termination Failed Notification - " + s_emp_name;
                var startofbody = "";
                startofbody += "<p>Hi Team<\/p>";
                startofbody += "<p>Resource " + s_emp_name + " already filled the timesheet for the below week\/s.<\/p>";

                startofbody = send_billing_team == true ? startofbody + "<p>Billing Team :- The system was not able to delete the Timesheets due to Billed time entries and the LWD is falling between the dates.<\/p>" : startofbody;

                startofbody += "<p>Operation Team : Kindly work with timesheet and HR operations team to make the corrections on LWD if required. Post the rectification, kindly manually update allocation dates and employee last working date as per the termination date.<\/p>";

                _sendEmailTotimesheetTeam(s_emp_name, s_termDate, strVar, recipientId, subject, startofbody);

            }
			//Send Deleted Timesheet Summary
            if (delete_strVar1) {
                var subject = "Termination Update : Deleted Timesheets Notification - " + s_emp_name;


                var startofbody = "";
                startofbody += "<p>Hi Operations\/Timesheet Team,<\/p>";
                startofbody += "<p>Resource " + s_emp_name + " already filled the timesheet for the below week\/s. And system has deleted time entrys for below week\/s due to resource termination.<\/p>";

                _sendEmailTotimesheetTeam(s_emp_name, s_termDate, delete_strVar1, recipientId, subject, startofbody);
            }


            if (!strVar) { //Update RA and subtier when all timesheets are deleted by sumbitting Fusion- Employee JSON Master record so that user event script will trigger 


                var obj_termination_record = record.load({
                    type: 'customrecord_fi_employee_json_master',
                    id: i_termination_Id,
                });
                var i_reprocess_term_rec = obj_termination_record.save();
                log.debug("i_reprocess_term_rec : s_emp_name", i_reprocess_term_rec + " : " + s_emp_name);

                /*
				log.debug('!strVar',!strVar);
				
                if ((a_ra_ids.length > 0)
                    //   || (a_ra_ids.length>0 && o_timebill=={})
                ) {
                    for (i = 0; a_ra_ids.length > i; i++) {
                        log.debug('Allocation inside for loop', a_ra_ids.length);
                        log.debug('a_ra_ids[i]', a_ra_ids[i]);
                        //Load Resource allocation record
                        try {
                            log.debug('a_ra_ids[i].id', a_ra_ids[i].id);
                        } catch (e) {
                            log.debug('eeee', e);
                        }

                        var s_startDate = a_ra_ids[i].startdate; // resAlloRec.getValue({fieldId: 'startdate'});
                        log.debug('a_ra_ids[i].startdate', s_startDate);

                        var d_startDate = format.parse({
                            value: s_startDate,
                            type: format.Type.DATE
                        });

                        log.debug('Start Date', s_startDate);
                        log.debug('d_startDate', d_startDate);
                        log.debug('term date', d_termDate);

                        log.debug('comp', d_startDate > d_termDate);
                        if (d_startDate > d_termDate) {
                            log.debug('Termdate', d_termDate);
                            log.debug('Resource ID', a_ra_ids[i]);
                            var i_ra_deleted_id = record.delete({
                                type: 'resourceallocation',
                                id: a_ra_ids[i].id
                            });
                        } else {
                            try {

                                var resAlloRec = record.load({
                                    type: 'resourceallocation',
                                    id: a_ra_ids[i].id,
                                });

                                log.debug('s_termDate', s_termDate);

                                resAlloRec.setValue({
                                    fieldId: 'enddate',
                                    value: d_termDate
                                }); // Allocation End date

                                log.debug('s_termDate end date', s_termDate);

                                resAlloRec.setValue({
                                    fieldId: 'custeventbenddate',
                                    value: d_termDate
                                }); // Billing End date

                                log.debug('custeventbenddate end date', s_termDate);
                                resAlloRec.setValue({
                                    fieldId: 'custevent_allocation_status',
                                    value: '22'
                                }); // Allocation status, Employee terminated 
                                resAlloRec.setValue({
                                    fieldId: 'notes',
                                    value: 'Employee terminated, hence allocation end date changed'
                                });
                                resAlloRec.setValue({
                                    fieldId: 'custevent_ra_last_modified_by',
                                    value: '442'
                                });

                                var current_time = format.format({
                                    value: new Date(),
                                    type: format.Type.DATETIME
                                });
                                log.debug('current_time', current_time);
                                // resAlloRec.setValue({fieldId: 'custevent_ra_last_modified_date',value:current_time}); //nlapiDateToString(new Date(), 'datetimetz'));
                                //Updated submit parameters
                                resAlloRec.save();
                                log.debug('Allocation ended');
                                log.debug('Allocation', resAlloRec);
                            } catch (e) {
                                log.debug('error in allocations', e);
                            }

                        }



                    }

                }


                // Update the subtier records
                // Get all subtier records that end after the LWD


                // change the end date of all the found subtier record
                if (obj.a_subteir_ids.length > 0) {

                    for (var index = 0; index < obj.a_subteir_ids.length; index++) {
                        var subTierRec = record.load({
                            type: 'customrecord_subtier_vendor_data',
                            id: obj.a_subteir_ids[index].id
                        });
                        // if start date is after the termination date,
                        // change
                        // the start date
                        if (format.parse({
                                value: subTierRec.getValue({
                                    fieldId: 'custrecord_stvd_start_date'
                                }),
                                type: format.Type.DATE
                            }) > d_termDate) {

                            subTierRec.setValue({
                                fieldId: 'isinactive',
                                value: true
                            });
                            subTierRec.setValue({
                                fieldId: 'custrecord_stvd_start_date',
                                value: d_termDate
                            });
                        }

                        subTierRec.setValue({
                            fieldId: 'custrecord_stvd_end_date',
                            value: d_termDate
                        });
                        subTierRec.save({
                            enableSourcing: false,
                            ignoreMandatoryFields: true
                        });
                    }

                }

                log.debug('load employee-----', context.key);
                var employeeRecord = record.load({
                    type: 'employee',
                    id: context.key
                });
                log.debug('load employee-----', context.key);

                log.debug('giveaccess', employeeRecord.getValue({
                    fieldId: 'giveaccess'
                }));



                employeeRecord.setValue({
                    fieldId: 'giveaccess',
                    value: false
                }); //Removing access

                employeeRecord.setValue({
                    fieldId: 'custentity_employee_inactive',
                    value: true
                }); //System Inactice
                employeeRecord.setValue({
                    fieldId: 'isjobresource',
                    value: false
                }); //Project Resource Remove
                employeeRecord.setValue({
                    fieldId: 'custentity_lwd',
                    value: d_termDate
                }); //Update the termination date at employee master:
                var i_Id = employeeRecord.save(employeeRecord);
                nlapiLogExecution('debug', 'Employee Inactive Internal ID', i_Id);
                TermList.push({
                    'Employee ID': i_Id,
                    'FusionID': fusionNumber
                });

                log.debug('context.Values', context.values[0]);
				*/
                /*context.write({
                    key: custPaymentId 
                }); 
                */

            }

        }

        function summarize(summary) {
            //handleErrorIfAny(summary);
            //createSummaryRecord(summary);


            // Log only the name and description of each error thrown.

            /* summary.reduceSummary.errors.iterator().each(function (key, error){
				   var errorObject = JSON.parse(error);
				   log.error({
					   title:  'Reduce error for key: ' + key + ', execution no. ' + executionNo, 
					   details: errorObject.name + ': ' + errorObject.message
				   });
				   return true;
			   });
			   */
        }



        return {
            getInputData: getInputData,
            map: map,
            reduce: reduce,
            summarize: summarize
        };
    });