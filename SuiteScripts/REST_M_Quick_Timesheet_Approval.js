/**
 * RESTlet for Quick Time Approval Pool
 * 
 * Version Date Author Remarks 1.00 03 Nov 2015 Nitish Mishra
 * 
 */

function postRESTlet(dataIn) {
	var response = new Response();
	try {
		nlapiLogExecution('debug', 'dataIn', JSON.stringify(dataIn));
		var employeeId = getUserUsingEmailId(dataIn.EmailId);
		var requestType = dataIn.RequestType;

		//Test 
		//var requestType = 'GET';
		//var employeeId = '30237';
 		switch (requestType) {

			case M_Constants.Request.Get:
				response.Data = getPendingTimesheets(employeeId);
				response.Status = true;
			break;

			case M_Constants.Request.Approve:
				response.Data = approveTimesheets(dataIn.Data.TimesheetList,
				        employeeId);
				response.Status = true;
			break;

			case M_Constants.Request.Reject:
				response.Data = rejectTimesheets(dataIn.Data.TimesheetList,
				        employeeId);
				response.Status = true;
			break;
		}

	} catch (err) {
		nlapiLogExecution('ERROR', 'postRESTlet', err);
		response.Data = err;
		response.Status = false;
	}
	return response;
}

function getPendingTimesheets(employeeId) {
	try {
	var days = ['sunday','monday','tuesday','wednesday','thursday','friday','saturday'];
		var filters = [
		        [ 'custrecord_qap_approved', 'is', 'F' ],
		        'and',
		        [ 'custrecord_qap_rejected', 'is', 'F' ],
		        'and',
		        [ 'custrecord_qap_ts_updated', 'is', 'F' ],
		        'and',
		        [ 'custrecord_qap_employee.timeapprover', 'anyof', employeeId ],
		        'and', [ 'isinactive', 'is', 'F' ] ];

		var columns = [
		        new nlobjSearchColumn('custrecord_qap_employee'),
		        new nlobjSearchColumn('custrecord_qap_week_start_date')
		                .setSort(true),
		        new nlobjSearchColumn('custrecord_qap_week_end_date'),
		        new nlobjSearchColumn('custrecord_qap_timesheet'),
		        new nlobjSearchColumn('custrecord_qap_st_hours'),
		        new nlobjSearchColumn('custrecord_qap_ot_hours'),
		        new nlobjSearchColumn('custrecord_qap_leave_hours'),
		        new nlobjSearchColumn('custrecord_qap_holiday_hours'),
		        new nlobjSearchColumn('custrecord_qap_total_hours') ];

		var pendingTimesheetSearch = nlapiSearchRecord(
		        'customrecord_ts_quick_approval_pool', null, filters, columns);

		var timesheetList = [];
		var projectList = [];
		var dataRow = [];
		var json = {};
		var main = {};
		if (pendingTimesheetSearch) {
			for (var i = 0; i < pendingTimesheetSearch.length && i < 50; i++) {
			 var timesheet_ID = pendingTimesheetSearch[i].getValue('custrecord_qap_timesheet');
			 nlapiLogExecution('debug','TimeSheet ID',timesheet_ID);
				var loadRecord = nlapiLoadRecord('timesheet',timesheet_ID);
				var lineCount = loadRecord.getLineItemCount('timegrid');
				
				for(var k=1;k<= lineCount ;k++){
				loadRecord.selectLineItem('timegrid',k);
					
					
					for(var n=0;n<7;n++){
						
						var sub_day = days[n];
						var o_sub_record_view = loadRecord.viewCurrentLineItemSubrecord('timegrid', sub_day);
						if(o_sub_record_view){
						var i_item_id = o_sub_record_view.getFieldText('customer');
						
						if(i_item_id )
						{
						var project_id = i_item_id;
						break;
						//json ={
					//	projectID :i_item_id
				
						//}
						
						}
												
				
				
					projectList.push(json);
				}
				}
				if(project_id){
				break;
				}
				}
				 nlapiLogExecution('debug','projectList',projectList);
				var timesheet = new TimesheetQuickApprovalPool();
				timesheet.PoolId = pendingTimesheetSearch[i].getId();
				timesheet.TimesheetId = pendingTimesheetSearch[i]
				        .getValue('custrecord_qap_timesheet');
				timesheet.EmployeeName = pendingTimesheetSearch[i]
				        .getText('custrecord_qap_employee');
				timesheet.WeekStartDate = pendingTimesheetSearch[i]
				        .getValue('custrecord_qap_week_start_date');
				timesheet.WeekEndDate = pendingTimesheetSearch[i]
				        .getValue('custrecord_qap_week_end_date');
				timesheet.ST = pendingTimesheetSearch[i]
				        .getValue('custrecord_qap_st_hours');
				timesheet.OT = pendingTimesheetSearch[i]
				        .getValue('custrecord_qap_ot_hours');
				timesheet.Leave = pendingTimesheetSearch[i]
				        .getValue('custrecord_qap_leave_hours');
				timesheet.Holiday = pendingTimesheetSearch[i]
				        .getValue('custrecord_qap_holiday_hours');
				timesheet.TotalApprovable = pendingTimesheetSearch[i].getValue(
				        'custrecord_qap_total_hours').split(':')[0];
				timesheet.Checked = false;
				timesheet.Project = project_id;
				timesheetList.push(timesheet);
				
			}
		}
		//main = {
		//	TimeSheet : timesheetList,
		//	Project : projectList
		//};

		return timesheetList;
	} catch (err) {
		nlapiLogExecution('error', 'getPendingTimesheets', err);
		throw err;
	}
}

function approveTimesheets(timesheetList, approverId) {
	try {
		var resultantList = [];
		nlapiLogExecution('DEBUG', 'timesheetList', timesheetList.length);

		for (var i = 0; i < timesheetList.length && i < 50; i++) {
			var timesheet = timesheetList[i];

			nlapiLogExecution('DEBUG', 'timesheet.Checked', timesheet.Checked);
			if (timesheet.Checked) {
				var quickPoolRecordId = timesheetList[i].PoolId;

				try {
					nlapiSubmitField('customrecord_ts_quick_approval_pool',
					        quickPoolRecordId, 'custrecord_qap_approved', 'T');

					createLog('customrecord_ts_quick_approval_pool',
					        quickPoolRecordId, 'Update', approverId, 'Approved');

					nlapiLogExecution('DEBUG', 'approveTimesheets',
					        'RECORD UPDATED : ' + quickPoolRecordId);

					resultantList.push({
					    Id : quickPoolRecordId,
					    Message : ''
					});
				} catch (er) {
					nlapiLogExecution('ERROR', 'approveTimesheets',
					        'Failed To Approve : ' + quickPoolRecordId + ' : '
					                + er);
					resultantList.push({
					    Id : quickPoolRecordId,
					    Message : er
					});
				}
			}
		}

		return resultantList;
	} catch (err) {
		nlapiLogExecution('error', 'approveTimesheets', err);
		throw err;
	}
}

function rejectTimesheets(timesheetList, approverId) {
	try {
		var resultantList = [];

		for (var i = 0; i < timesheetList.length && i < 50; i++) {
			var timesheet = timesheetList[i];

			if (timesheet.Checked) {
				var quickPoolRecordId = timesheetList[i].PoolId;

				try {
					nlapiSubmitField('customrecord_ts_quick_approval_pool',
					        quickPoolRecordId, 'custrecord_qap_rejected', 'T');

					createLog('customrecord_ts_quick_approval_pool',
					        quickPoolRecordId, 'Update', approverId, 'Rejected');

					nlapiLogExecution('DEBUG', 'rejectTimesheets',
					        'RECORD UPDATED : ' + quickPoolRecordId);

					resultantList.push({
					    Id : quickPoolRecordId,
					    Message : ''
					});
				} catch (er) {
					nlapiLogExecution('ERROR', 'rejectTimesheets',
					        'Failed To Reject : ' + quickPoolRecordId + ' : '
					                + er);
					resultantList.push({
					    Id : quickPoolRecordId,
					    Message : er
					});
				}
			}
		}

		return resultantList;
	} catch (err) {
		nlapiLogExecution('error', 'rejectTimesheets', err);
		throw err;
	}
}