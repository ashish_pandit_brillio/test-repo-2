// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
/*
   	Script Name:UES Vendor Bill TDS CSV upload
	Author:		Nikhil jain 
	Company:	AashnaCloudtech Pvt. Ltd.
	Date:		25 June 2015
	Description:apply an tds on bill while uploading an csv


	Script Modification Log:

	-- Date --			-- Modified By --				--Requested By--				-- Description --
	20 july 2015         Nikhil jain                     satish doke                    Add an logic of tds rounding off
	30 July 2015         Nikhil jain                     sachin k                       add a reneweal process

Below is a summary of the process controls enforced by this script file.  The control logic is described
more fully, below, in the appropriate function headers and code blocks.


     BEFORE LOAD
		- beforeLoadRecord(type)



     BEFORE SUBMIT
		- beforeSubmitRecord(type)


     AFTER SUBMIT
		- afterSubmitRecord(type)



     SUB-FUNCTIONS
		- The following sub-functions are called by the above core functions in order to maintain code
            modularization:

               - NOT USED

*/
}
// END SCRIPT DESCRIPTION BLOCK  ====================================



// BEGIN GLOBAL VARIABLE BLOCK  =====================================
{
	//  Initialize any Global Variables, in particular, debugging variables...




}
// END GLOBAL VARIABLE BLOCK  =======================================





// BEGIN BEFORE LOAD ==================================================

function beforeLoadRecord(type)
{

	/*  On before load:

          - EXPLAIN THE PURPOSE OF THIS FUNCTION

		-


		FIELDS USED:

          --Field Name--				--ID--


	*/



	//  LOCAL VARIABLES

    //  BEFORE LOAD CODE BODY


	return true;



}

// END BEFORE LOAD ====================================================





// BEGIN BEFORE SUBMIT ================================================

function beforeSubmitRecord_tds_csvupload(type)
{
	/*  On before submit:
	 - PURPOSE
	 -
	 FIELDS USED:
	 --Field Name--				--ID--
	 */
	//  LOCAL VARIABLES
	
	
	//  BEFORE SUBMIT CODE BODY
	var a_subisidiary = new Array();
	var i_vatCode;
	var i_taxcode;
	var currentContext = nlapiGetContext();
	var s_pass_code;
		
	// IF EXECUTION CONTEXT IS WORKFLOW ====
	if (currentContext.getExecutionContext() == 'csvimport' || currentContext.getExecutionContext() == 'webservices') {
	
		var i_AitGlobalRecId = SearchGlobalParameter();
		nlapiLogExecution('DEBUG', 'Bill ', "i_AitGlobalRecId" + i_AitGlobalRecId);
		
		if (i_AitGlobalRecId != 0) {
			var o_AitGloRec = nlapiLoadRecord('customrecord_indian_tax_glo_parameter', i_AitGlobalRecId);
			
			a_subisidiary = o_AitGloRec.getFieldValues('custrecord_ait_subsidiary');
			nlapiLogExecution('DEBUG', 'Bill ', "a_subisidiary->" + a_subisidiary);
			
			var i_vatCode = o_AitGloRec.getFieldValue('custrecord_ait_vat_code');
			nlapiLogExecution('DEBUG', 'Bill ', "i_vatCode->" + i_vatCode);
			
			var i_taxcode = o_AitGloRec.getFieldValue('custrecord_ait_taxcode');
			nlapiLogExecution('DEBUG', 'Bill ', "i_taxcode->" + i_taxcode);
			
			var tdsRoundMethod = o_AitGloRec.getFieldValue('custrecord_ait_tds_roundoff');
			nlapiLogExecution('DEBUG','Bill ', "tdsRoundMethod->"+tdsRoundMethod);
			
			var s_pass_code = o_AitGloRec.getFieldValue('custrecord_encpwdstring');
		} // END   if (i_AitGlobalRecId != 0)
		var Flag = 0;
		var subsidiary = nlapiGetFieldValue('subsidiary')
		Flag = isindia_subsidiary(a_subisidiary, s_pass_code, subsidiary)
		if (Flag == 1) {
			var expense_line_count = nlapiGetLineItemCount('expense');
			nlapiLogExecution('DEBUG', 'Bill ', "new line count" + expense_line_count);
			var vid = nlapiGetFieldValue('entity');
			
			for (var l = 1; l <= expense_line_count; l++) {
				var result_flag = 0;
				var tdstype = '';
				
				var tdsapply = nlapiGetLineItemValue('expense', 'custcol_tdsapply', l);
				nlapiLogExecution('DEBUG', 'Bill ', "tdsapply" + tdsapply);
				
				var user_tds_type = nlapiGetLineItemValue('expense', 'custcol_tdstype', l);
				nlapiLogExecution('DEBUG', 'Bill ', "user_tds_type" + user_tds_type);
				
				
				if (tdsapply == 'T' && user_tds_type != null && user_tds_type != '' && user_tds_type != undefined) {
					//====================Function to check the valid tds type ===================================//
					result_flag = check_valid_tds_type(vid, user_tds_type)
					tdstype = user_tds_type;
					
				}
				else {
					if (tdsapply == 'T') {
					
						//===============Function to check ao certificate of vendor =================================//
						var ao_tds_type = Check_AO_certificate(vid)
						
						if (ao_tds_type != null && ao_tds_type != '' && ao_tds_type != undefined) {
							//nlapiSetLineItemValue('expense', 'custcol_tdstype', ao_tds_type, i);
							tdstype = ao_tds_type;
							result_flag = 1;
						}
						else {
							var filters = new Array()
							var column1 = new Array()
							
							filters.push(new nlobjSearchFilter('custrecord_vedname', null, 'is', vid));
							filters.push(new nlobjSearchFilter('custrecord_vedconecssion', null, 'is', 'F'));
							
							column1.push(new nlobjSearchColumn('custrecord_vedtdstype'));
							
							column1.push(new nlobjSearchColumn('custrecordvedtds_default'));
							
							var results1 = nlapiSearchRecord('customrecord_tdsrelation', null, filters, column1);
							
							if (results1 != null && results1 != undefined && results1 != '') {
								for (var i = 0; i < results1.length; i++) {
									var Default_tds_check = results1[i].getValue('custrecordvedtds_default')
									if (Default_tds_check == 'T') {
										var default_tds_type = results1[i].getValue('custrecord_vedtdstype')
										break;
									}
								}
								if (default_tds_type != null && default_tds_type != '' && default_tds_type != undefined) {
									nlapiLogExecution('DEBUG', 'Bill ', "default_tds_type->" + default_tds_type);
									//nlapiSetLineItemValue('expense', 'custcol_tdstype', default_tds_type,i)
									tdstype = default_tds_type;
									result_flag = 1;
								}
								else {
									//nlapiSetLineItemValue('expense', 'custcol_tdstype', results1[0].getValue('custrecord_vedtdstype'),i);
									tdstype = results1[0].getValue('custrecord_vedtdstype');
									result_flag = 1;
								}
							}// END if (results1 != null)
							else {
								throw 'Plesae Select TDS Type On Selected Vendor';
								
							}// END else if (results1 != null)
						}
						
					}
				}
				if ((result_flag == 1) && (tdstype != '' && tdstype != null && tdstype != undefined)) {
					nlapiLogExecution('DEBUG', 'Bill ', "result_flag->" + result_flag);
					
					var filters = new Array()
					var column = new Array()
					
					filters.push(new nlobjSearchFilter('internalid', null, 'is', tdstype));
					
					column.push(new nlobjSearchColumn('custrecord_netper'));
					column.push(new nlobjSearchColumn('custrecord_empty_pan_tdsper'));
					
					var results2 = nlapiSearchRecord('customrecord_tdsmaster', null, filters, column);
					
					if (results2 != null) {
						var vid = nlapiGetFieldValue('entity')
						
						var pan_no = nlapiLookupField('vendor', vid, 'custentity_vendor_panno')
						
						if (pan_no != null && pan_no != '' && pan_no != undefined) {
							var taxrate = results2[0].getValue('custrecord_netper');
						}
						else {
							var taxrate = results2[0].getValue('custrecord_empty_pan_tdsper');
						}
						
						
						nlapiSelectLineItem('expense', l)
						
						nlapiSetCurrentLineItemValue('expense', 'custcol_tdstype', tdstype);
						
						var amt = nlapiGetCurrentLineItemValue('expense', 'amount');
						nlapiSetCurrentLineItemValue('expense', 'custcol_baseamount', amt);
						
						var totaltds = (parseFloat(taxrate) * parseFloat(amt)) / 100
						
						//---------------------Begin function to round tds amount--------------------------//
						
						totaltds = applyTdsRoundMethod(tdsRoundMethod,totaltds)
						
						//---------------------End function to round tds amount--------------------------//
						nlapiSetCurrentLineItemValue('expense', 'custcol_tdsamount', parseFloat(totaltds))
						//nlapiSetCurrentLineItemValue('expense', 'custcol_total', parseFloat(grossAmt - totaltds))
						
						var tdsAmount = totaltds;
						tdsAmount = -tdsAmount
						
						var location = nlapiGetCurrentLineItemValue('expense', 'location')
						var department = nlapiGetCurrentLineItemValue('expense', 'department')
						var class1 = nlapiGetCurrentLineItemValue('expense', 'class')
						var Tdscheck = nlapiGetCurrentLineItemValue('expense', 'custcol_tdsline')
						
						var newline = parseInt(parseInt(l) + parseInt(1));
						nlapiLogExecution('DEBUG', 'Bill ', "new line newline" + newline);
						
						var Tdscheck1 = nlapiGetLineItemValue('expense', 'custcol_tdsline', newline);
						nlapiCommitLineItem('expense')
						
						var tdsAccount = getTDSaccount(tdstype)
						nlapiLogExecution('DEBUG', 'Bill ', "tdsAccount" + tdsAccount);
						
						//==============check Apply tds line is not at end===============================//
						if (expense_line_count != l) {
						
							if (Tdscheck == 'F' && Tdscheck1 == 'T') {
							
								//====================Select line On edit the existing Apply Tds line=================//
								nlapiSelectLineItem('expense', newline)
							}
							else {
								//=========Begin code to insert line after apply tds line==========================//
								nlapiSelectLineItem('expense', newline)
								nlapiInsertLineItem('expense', newline)
								expense_line_count = parseInt(expense_line_count) + parseInt(1);
								nlapiLogExecution('DEBUG', 'Bill ', "new line expense_line_count" + expense_line_count);
							}
						}//End if (expense_line_count != l) 
						else {
							//=========If last line of count then add new line ==========================//
							nlapiSelectNewLineItem('expense');
							
						}
						
						nlapiSetCurrentLineItemValue('expense', 'account', tdsAccount)
						nlapiSetCurrentLineItemValue('expense', 'amount', tdsAmount)
						nlapiSetCurrentLineItemValue('expense', 'taxcode', i_vatCode)
						nlapiSetCurrentLineItemValue('expense', 'grossamt', tdsAmount)
						nlapiSetCurrentLineItemValue('expense', 'custcol_tdsline', 'T')
						nlapiSetCurrentLineItemValue('expense', 'custcol_baseamount', 0)
						nlapiSetCurrentLineItemValue('expense', 'location', location)
						nlapiSetCurrentLineItemValue('expense', 'department', department)
						nlapiSetCurrentLineItemValue('expense', 'class', class1)
						nlapiCommitLineItem('expense')
						nlapiSetFieldValue('custbody_deducttds','T')
						nlapiLogExecution('DEBUG', 'Bill ', "result_flag-2>" + "after commit");
						
					}
				}
			// END if (tdapply == 'T') 
			
			}
			
			var item_line_count = nlapiGetLineItemCount('item');
			nlapiLogExecution('DEBUG', 'Bill ', "item_line_count" + item_line_count);
			
			for (var k = 1; k <= item_line_count; k++) {
				var result_flag = 0;
				var tdstype = '';
				
				var tdsapply = nlapiGetLineItemValue('item', 'custcol_tdsapply', k);
				nlapiLogExecution('DEBUG', 'Bill ', "tdsapply" + tdsapply);
				
				var user_tds_type = nlapiGetLineItemValue('item', 'custcol_tdstype', k);
				nlapiLogExecution('DEBUG', 'Bill ', "user_tds_type" + user_tds_type);
				
				
				if (tdsapply == 'T' && user_tds_type != null && user_tds_type != '' && user_tds_type != undefined) {
					//====================Function to check the valid tds type ===================================//
					result_flag = check_valid_tds_type(vid, user_tds_type)
					tdstype = user_tds_type;
					
				}
				else {
					if (tdsapply == 'T') {
						//===============Function to check ao certificate of vendor =================================//
						var ao_tds_type = Check_AO_certificate(vid)
						
						if (ao_tds_type != null && ao_tds_type != '' && ao_tds_type != undefined) {
							//nlapiSetLineItemValue('expense', 'custcol_tdstype', ao_tds_type, i);
							tdstype = ao_tds_type;
							result_flag = 1;
						}//if (ao_tds_type != null && ao_tds_type != '' && ao_tds_type != undefined) 
						else {
							var filters = new Array()
							var column1 = new Array()
							
							filters.push(new nlobjSearchFilter('custrecord_vedname', null, 'is', vid));
							filters.push(new nlobjSearchFilter('custrecord_vedconecssion', null, 'is', 'F'));
							
							column1.push(new nlobjSearchColumn('custrecord_vedtdstype'));
							
							column1.push(new nlobjSearchColumn('custrecordvedtds_default'));
							
							var results1 = nlapiSearchRecord('customrecord_tdsrelation', null, filters, column1);
							
							if (results1 != null && results1 != undefined && results1 != '') {
								for (var i = 0; i < results1.length; i++) {
									var Default_tds_check = results1[i].getValue('custrecordvedtds_default')
									if (Default_tds_check == 'T') {
										var default_tds_type = results1[i].getValue('custrecord_vedtdstype')
										break;
									}
								}
								if (default_tds_type != null && default_tds_type != '' && default_tds_type != undefined) {
									nlapiLogExecution('DEBUG', 'Bill ', "default_tds_type->" + default_tds_type);
									//nlapiSetLineItemValue('expense', 'custcol_tdstype', default_tds_type,i)
									tdstype = default_tds_type;
									result_flag = 1;
								}
								else {
									//nlapiSetLineItemValue('expense', 'custcol_tdstype', results1[0].getValue('custrecord_vedtdstype'),i);
									tdstype = results1[0].getValue('custrecord_vedtdstype');
									result_flag = 1;
								}
							}// END if (results1 != null)
							else {
								throw 'Plesae Select TDS Type On Selected Vendor';
								
							}// END else if (results1 != null)
						}
						
					}//END if (tdsapply == 'T') 
				}//end Else
				if ((result_flag == 1) && (tdstype != '' && tdstype != null && tdstype != undefined)) {
					nlapiLogExecution('DEBUG', 'Bill ', "result_flag->" + result_flag);
					
					var filters = new Array()
					var column = new Array()
					
					filters.push(new nlobjSearchFilter('internalid', null, 'is', tdstype));
					
					column.push(new nlobjSearchColumn('custrecord_netper'));
					column.push(new nlobjSearchColumn('custrecord_empty_pan_tdsper'));
					
					var results2 = nlapiSearchRecord('customrecord_tdsmaster', null, filters, column);
					
					if (results2 != null) {
						var vid = nlapiGetFieldValue('entity')
						
						var pan_no = nlapiLookupField('vendor', vid, 'custentity_vendor_panno')
						
						if (pan_no != null && pan_no != '' && pan_no != undefined) {
							var taxrate = results2[0].getValue('custrecord_netper');
						}
						else {
							var taxrate = results2[0].getValue('custrecord_empty_pan_tdsper');
						}
						
						
						nlapiSelectLineItem('item', k)
						
						nlapiSetCurrentLineItemValue('item', 'custcol_tdstype', tdstype);
						
						var amt = nlapiGetCurrentLineItemValue('item', 'amount')
						nlapiGetCurrentLineItemValue('item', 'custcol_baseamount', amt)
						var totaltds = (parseFloat(taxrate) * parseFloat(amt)) / 100
						
						//---------------------Begin function to round tds amount--------------------------//
						
						totaltds = applyTdsRoundMethod(tdsRoundMethod,totaltds)
						
						//---------------------End function to round tds amount--------------------------//
						
						nlapiSetCurrentLineItemValue('item', 'custcol_tdsamount', parseFloat(totaltds))
						//nlapiSetCurrentLineItemValue('expense', 'custcol_total', parseFloat(grossAmt - totaltds))
						
						var tdsAmount = totaltds;
						tdsAmount = -tdsAmount
						
						var location = nlapiGetCurrentLineItemValue('item', 'location')
						var department = nlapiGetCurrentLineItemValue('item', 'department')
						var class1 = nlapiGetCurrentLineItemValue('item', 'class')
						var Tdscheck = nlapiGetCurrentLineItemValue('item', 'custcol_tdsline')
						
						var newline = parseInt(parseInt(k) + parseInt(1));
						nlapiLogExecution('DEBUG', 'Bill ', "new line newline" + newline);
						
						var Tdscheck1 = nlapiGetLineItemValue('item', 'custcol_tdsline', newline);
						nlapiLogExecution('DEBUG', 'Bill ', "Tdscheck" + Tdscheck1);
						nlapiCommitLineItem('item')
						
						var tdsItem = getTDSitem(tdstype)
						nlapiLogExecution('DEBUG', 'Bill ', "tdsItem" + tdsItem);
						
						//==============check Apply tds line is not at end===============================//
						if (item_line_count != k) {
							if (Tdscheck == 'F' && Tdscheck1 == 'T') {
								//====================Select line on edit the existing Apply Tds line=================//
								nlapiSelectLineItem('item', newline)
							}
							else {
								//=========Begin code to insert line after apply tds line==========================//
								nlapiSelectLineItem('item', newline)
								nlapiInsertLineItem('item', newline)
								item_line_count = parseInt(item_line_count) + parseInt(1);
								nlapiLogExecution('DEBUG', 'Bill ', "new line item_line_count" + item_line_count);
							}
						}
						else {
							//=========If last line of count then add new line ==========================//
							nlapiSelectNewLineItem('item');
							
						}
						nlapiSetCurrentLineItemValue('item', 'item', tdsItem, true, true)
						nlapiSetCurrentLineItemValue('item', 'quantity', 1, true, true)
						nlapiSetCurrentLineItemValue('item', 'rate', parseFloat(tdsAmount), true, true)
						nlapiSetCurrentLineItemValue('item', 'taxcode', i_vatCode, true, true)
						nlapiSetCurrentLineItemValue('item', 'custcol_baseamount', 0)
						nlapiSetCurrentLineItemValue('item', 'custcol_tdsline', 'T', false, true)
						nlapiSetCurrentLineItemValue('item', 'location', location, true, true)
						nlapiSetCurrentLineItemValue('item', 'department', department, true, true)
						nlapiSetCurrentLineItemValue('item', 'class', class1, true, true)
						nlapiCommitLineItem('item')
						nlapiSetFieldValue('custbody_deducttds','T')
						nlapiLogExecution('DEBUG', 'Bill ', "result_flag-2>" + "after commit");
						
					}
				}//END if ((result_flag == 1) && (tdstype != '' && tdstype != null && tdstype != undefined)) 
			}
		}
		
	}
	return true;
	
}

// END BEFORE SUBMIT ==================================================





// BEGIN AFTER SUBMIT =============================================

function afterSubmitRecord(type)
{
    /*  On after submit:

          - PURPOSE
		-

	FIELDS USED:

          --Field Name--				--ID--


    */


	//  LOCAL VARIABLES



	//  AFTER SUBMIT CODE BODY

	return true;

}

// END AFTER SUBMIT ===============================================





// BEGIN FUNCTION ===================================================
function SearchGlobalParameter()			
	{
	
	    var a_filters = new Array();
	    var a_column = new Array();
	    var i_globalRecId = 0;
	    
	    a_column.push(new nlobjSearchColumn('internalid'));
	    
	    var s_serchResult = nlapiSearchRecord('customrecord_indian_tax_glo_parameter', null, null, a_column)
	    
	    if (s_serchResult != null && s_serchResult != undefined && s_serchResult != '') 	
		{
	        for (var i = 0; i < s_serchResult.length; i++) 	
			{
	            i_globalRecId = s_serchResult[0].getValue('internalid');
	            nlapiLogExecution('DEBUG', 'Bill ', "i_globalRecId" + i_globalRecId);
	            
	        }
	        
	    }
	    
	    
	    return i_globalRecId;
	}
function Check_AO_certificate(vid)
{
		var default_tds_type = '';
					
		var d_flag = 0;
					
		var trandate = nlapiGetFieldValue('trandate')
						
		var filter2= new Array()
		var column2=new Array()
		
	    filter2.push( new nlobjSearchFilter('internalid', null, 'is',vid));
	    filter2.push( new nlobjSearchFilter('custentity_apply_ao_certificate', null, 'is','T'));
	 	filter2.push( new nlobjSearchFilter('custentity_ao_cert_from_date', null, 'onorbefore',trandate));
		filter2.push( new nlobjSearchFilter('custentity_ao_cert_to_date', null, 'onorafter',trandate));
		
		column2.push( new nlobjSearchColumn('internalid'))
		
		 var results2 = nlapiSearchRecord('vendor', null, filter2, column2);
						    
		if (results2 != null && results2 != undefined && results2 != '') 
		{
			 var internal_id = results2[0].getValue('internalid')
			 d_flag = 1;
		}
		if (d_flag == 1) 
		{
			var filters = new Array()
			var column1 = new Array()
			
			filters.push(new nlobjSearchFilter('custrecord_vedname', null, 'is', vid));
			filters.push(new nlobjSearchFilter('custrecord_vedconecssion', null, 'is', 'T'));
			
			column1.push(new nlobjSearchColumn('custrecord_vedtdstype'))
			column1.push(new nlobjSearchColumn('custrecordvedtds_default'))
			
			var results1 = nlapiSearchRecord('customrecord_tdsrelation', null, filters, column1);
			
			if (results1 != null && results1 != undefined && results1 != '') 
			{
				for (var i = 0; i < results1.length; i++) 
				{
					var Default_tds_check = results1[i].getValue('custrecordvedtds_default')
					if (Default_tds_check == 'T') 
					{
						default_tds_type = results1[i].getValue('custrecord_vedtdstype')
						break;
					}
				}
				if (default_tds_type != null && default_tds_type != '' && default_tds_type != undefined) 
				{
				
				}
				else 
				{
					default_tds_type = results1[0].getValue('custrecord_vedtdstype');
					
				}
			}// END if (results1 != null)
		}
					 
	return default_tds_type;
}
function getTDSaccount(tdsType)
{
	//==== function to get Tds payable account. === 
	var tdsRec=nlapiLoadRecord('customrecord_tdsmaster',tdsType)
	var tdsAccount=tdsRec.getFieldValue('custrecord_tdsaccount')
	return tdsAccount
}
function getTDSitem(tdsType)
{
	//==== TO GET THE TDS TYPE ====
	var tdsRec=nlapiLoadRecord('customrecord_tdsmaster',tdsType)
	var tdsItem=tdsRec.getFieldValue('custrecord_tdsitem')
	return tdsItem
}
function check_valid_tds_type(vid, user_tds_type)
{
	var return_flag = 0;
	var tdstype_filters = new Array()
	var tdstype_column = new Array()
	
	tdstype_filters.push(new nlobjSearchFilter('custrecord_vedname', null, 'is', vid));
	tdstype_filters.push(new nlobjSearchFilter('custrecord_vedtdstype', null, 'is', user_tds_type));
	
	tdstype_column.push(new nlobjSearchColumn('custrecord_vedtdstype'));
	
	var tds_relation_results = nlapiSearchRecord('customrecord_tdsrelation', null, tdstype_filters, tdstype_column);
	
	if (tds_relation_results != null && tds_relation_results != undefined && tds_relation_results != '') 
	{
		
		return_flag = 1;
	}
	else 
	{
		throw 'The TDS type you entered is not belongs to the vendor';
		
	}
	return return_flag;
	
}
function applyTdsRoundMethod(tdsRoundMethod,tdsAmount)
{
	var roundedtdsAmount = tdsAmount;
	
	if(tdsRoundMethod == 2)
	{
		roundedtdsAmount = Math.round(tdsAmount)
	}
	if(tdsRoundMethod == 3)
	{
		roundedtdsAmount = Math.round(tdsAmount/10)*10;
	}
	if(tdsRoundMethod == 4)
	{
		roundedtdsAmount = Math.round(tdsAmount/100)*100;
	}
	
	return roundedtdsAmount;
}
// END FUNCTION =====================================================
