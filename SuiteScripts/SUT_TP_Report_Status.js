// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
/*
   	Script Name:SUT_TP_Report_Status.js
	Author:	Sachin K
	Company:Aashna
	Date:22-Aug-2014
	Description:Script is used for to give the detail to user for data process
	*


	Script Modification Log:

	-- Date --			-- Modified By --				--Requested By--				-- Description --



Below is a summary of the process controls enforced by this script file.  The control logic is described
more fully, below, in the appropriate function headers and code blocks.


     SUITELET
		- suiteletFunction(request, response)


     SUB-FUNCTIONS
		- The following sub-functions are called by the above core functions in order to maintain code
            modularization:

               - NOT USED

*/
}
// END SCRIPT DESCRIPTION BLOCK  ====================================



// BEGIN GLOBAL VARIABLE BLOCK  =====================================
{
	//  Initialize any Global Variables, in particular, debugging variables...




}
// END GLOBAL VARIABLE BLOCK  =======================================





// BEGIN SUITELET ==================================================

function createStatusForm(request, response)
{

	/*  Suitelet:

          - EXPLAIN THE PURPOSE OF THIS FUNCTION

		-


		FIELDS USED:

          --Field Name--				--ID--


	*/



	//  LOCAL VARIABLES



    //  SUITELET CODE BODY

	nlapiLogExecution('DEBUG','Request Method', " " + request.getMethod());
		if(request.getMethod() == 'GET')
		{
			var form = nlapiCreateForm("Transfer Price Data Processing Status");

		    var status = form.addField('status', 'text', 'Status');
			status.setDisplayType('inline');
			status.setDefaultValue('In process')
			form.setScript('customscript_cli_tp_report_processstatus');

			var Complete = form.addField('complete', 'text', '% Complete');
			Complete.setDisplayType('inline');

        	form.addButton('custpage_printdoc', 'Refresh','pageRefresh()');
             //----------------------------------------------------------------------------
             	var colArr = new Array();
				var filterArr = new Array();

				//filterArr.push(new nlobjSearchFilter('queue', null, 'is' , "2"));
				filterArr.push(new nlobjSearchFilter('status', null, 'anyof', ['PROCESSING', 'PENDING']));
				filterArr.push(new nlobjSearchFilter('internalid', 'script','is','232'));

				colArr.push(new nlobjSearchColumn('name', 'script'));
				colArr.push(new nlobjSearchColumn('internalid', 'scriptdeployment'));
				colArr.push(new nlobjSearchColumn('datecreated'));
				colArr.push(new nlobjSearchColumn('status'));
				colArr.push(new nlobjSearchColumn('startdate'));
				colArr.push(new nlobjSearchColumn('enddate'));
				colArr.push(new nlobjSearchColumn('queue'));
				colArr.push(new nlobjSearchColumn('percentcomplete'));
				colArr.push(new nlobjSearchColumn('queueposition'));
				colArr.push(new nlobjSearchColumn('percentcomplete'));

				var searchResults =  nlapiSearchRecord('scheduledscriptinstance', null, filterArr, colArr);
			    if(searchResults!=null)
			    {
			    	nlapiLogExecution('DEBUG','In script','searchResults.length=='+searchResults.length);
			    	for(var i=0;i<searchResults.length;i++ )
			    	{
			    		var script  = searchResults[i].getValue('name', 'script');
						nlapiLogExecution('DEBUG','In script','script=='+script);

						var datecreated  = searchResults[i].getValue('datecreated');
						nlapiLogExecution('DEBUG','In script','datecreated=='+datecreated);

						var percentcomplete  = searchResults[i].getValue('percentcomplete');
						nlapiLogExecution('DEBUG','In script','percentcomplete=='+percentcomplete);

						var scriptstatus  = searchResults[i].getValue('status');
						nlapiLogExecution('DEBUG','In script','scriptstatus=='+scriptstatus);

						status.setDefaultValue(scriptstatus)
			    	    Complete.setDefaultValue(percentcomplete)
			    	}
			    }
			    else
			    {
			    	status.setDefaultValue('Complete')
			    	Complete.setDefaultValue('100%')
			    	form.addButton('printreport', 'Print Report','reportprint()');
			    }
             //----------------------------------------------------------------------------

			//form.addSubmitButton('Data Process');
			response.writePage(form);


		}




}

// END SUITELET ====================================================




// BEGIN OBJECT CALLED/INVOKING FUNCTION ===================================================
{



}
// END OBJECT CALLED/INVOKING FUNCTION =====================================================
