/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       26 Oct 2017     Poobalan K
 *
 */

/**
 * @param {nlobjRequest} request Request object
 * @param {nlobjResponse} response Response object
 * @returns {Void} Any output is written via response object
 */
function unique(obj){
    var uniques=[];
    var stringify={};
    for(var i=0;i<obj.length;i++){
       var keys=Object.keys(obj[i]);
       keys.sort(function(a,b) {return a-b});
       var str='';
        for(var j=0;j<keys.length;j++){
           str+= JSON.stringify(keys[j]);
           str+= JSON.stringify(obj[i][keys[j]]);
        }
        if(!stringify.hasOwnProperty(str)){
            uniques.push(obj[i]);
            stringify[str]=true;
        }
    }
    return uniques;
}
function suitelet_Proj_NOT_Confirm(request, response){
	
try{
	if (request.getMethod() == 'GET' )
		{
		var form = nlapiCreateForm('Projects Not Yet Confirmed');
		form.addSubmitButton('Export');
		response.writePage(form);
		}
	else{
		var col = [],filter = [],Searchresults = [];
		//filter[0] = new nlobjSearchFilter("custrecord_is_mnth_effrt_confirmed",null,"is",'F');
		Searchresults = nlapiSearchRecord(null,'2186',null,null);
		nlapiLogExecution('Debug','DEBUG---->>>>', Searchresults);
		
	    var strName="<head>";
	    strName+="<style>";
	    strName+="th{background-color: #3c8dbc; color:white;}";
	    strName+=".BorderTopLeft{border-top:solid;border-left:solid}";
	    strName+=".BorderBottomLeft{border-bottom:solid;border-left:solid}";
	    strName+=".BorderTopRight{border-top:solid;border-right:solid}";
	    strName+=".BorderBottomRight{border-bottom:solid;border-right:solid}";
	    strName+=".BorderTop{border-top:solid;}";
	    strName+=".BorderLeft{border-left:solid}";
	    strName+=".BoderAll{border-bottom:solid;border-right:solid;border-Top:solid;border-left:solid}";
	    strName+=".BorderRight{border-right:solid;}";
	    strName+=".BorderBottom{border-bottom:solid;}";
	    strName+=".BorderTopLeftRight{border-Top:solid;border-Right:solid;border-Left:solid;}"; 
	    strName+="body{font-family:sans-serif;font-size:8px;margin-top:0px;}";
	    strName+="</style>";
	    strName+="<macrolist>";
	    strName+="<macro id='myfooter'>";
	    strName+="<table  style='width: 100%;'><tr>";
	    strName+="<td align='right' ><pagenumber/> of <totalpages/></td>";
	    strName+="</tr></table>";
	    strName+="</macro>";
	    strName+="<macro id='myHead'>";
	    strName+="</macro>";
	    strName+="</macrolist>";
	    strName+="</head>";
	    
	    strName += "<table width='1430px' border='1'>";
	    strName += "<tr>";
	    strName += "<td width='30px' style='align:left;' font-size='10px'><b>PROJECT</b></td>"; 
	    strName += "<td width='30px' style='align:left;' font-size='10px'><b></b></td>"; 
	    strName += "<td width='30px' style='align:left;' font-size='10px'><b></b></td>"; 
	    strName += "<td width='30px' style='align:left;' font-size='10px'><b></b></td>"; 
	    strName += "<td width='30px' style='align:left;' font-size='10px'><b></b></td>"; 
	    strName += "<td width='30px' style='align:left;' font-size='10px'><b></b></td>";
	    strName += "</tr>";
	    var MyArray = [];
	    for(var i=0; Searchresults!=null && i<Searchresults.length;i++)
		{
	    	var columns	= Searchresults[i].getAllColumns();
	    	var Proj_ID	= Searchresults[i].getText(columns[0]);
	    	var rec = {  "Proj_ID" : Proj_ID};
	    	MyArray.push(rec);
		}
	    var ArrayVal = unique(MyArray);
	    
	             for(var k in ArrayVal)
			    	{
				    	var Proj_ID = ArrayVal[k].Proj_ID;
				    	strName += "<tr>";
					    strName += "<td width='30px' style='align:left;' font-size='10px'>"+Proj_ID+"</td>"; 
					    strName += "<td width='30px' style='align:left;' font-size='10px'><b></b></td>"; 
					    strName += "<td width='30px' style='align:left;' font-size='10px'><b></b></td>"; 
					    strName += "<td width='30px' style='align:left;' font-size='10px'><b></b></td>"; 
					    strName += "<td width='30px' style='align:left;' font-size='10px'><b></b></td>"; 
					    strName += "<td width='30px' style='align:left;' font-size='10px'><b></b></td>";
					    strName += "</tr>";
			    	}
	    nlapiLogExecution('Debug','ArrayVal---->>>>',JSON.stringify(ArrayVal));
	    
	  /*  */
	    
	    strName += "</table>";
	    strName += "</body>";
	    var xlsFile = nlapiCreateFile('TEST.xls', 'EXCEL', nlapiEncrypt(strName, 'base64'));
	    response.setContentType(xlsFile.getType(),'Resource Allocation Details.xls');
		response.write(xlsFile.getValue());
}
}
catch(err)
{
	nlapiLogExecution('ERROR','Reports_Project_Not_confirmed','ERROR MESSAGE :- '+err);
}
}
