/**
 * @author Shweta
 */
// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
/*
   	Script Name : SUT_Call_Suitlet_Cross_Project_Sourcing.js
	Author      : Shweta Chopde
	Date        : 2 Sep 2014
	Description :


	Script Modification Log:

	-- Date --			-- Modified By --				--Requested By--				-- Description --



Below is a summary of the process controls enforced by this script file.  The control logic is described
more fully, below, in the appropriate function headers and code blocks.


     SUITELET
		- suiteletFunction(request, response)


     SUB-FUNCTIONS
		- The following sub-functions are called by the above core functions in order to maintain code
            modularization:

               - NOT USED

*/
}
// END SCRIPT DESCRIPTION BLOCK  ====================================



// BEGIN GLOBAL VARIABLE BLOCK  =====================================
{
	//  Initialize any Global Variables, in particular, debugging variables...




}
// END GLOBAL VARIABLE BLOCK  =======================================





// BEGIN SUITELET ==================================================

function suiteletFunction(request, response)
{
  	var a_return_array;
	var s_project_description = '';	
	var i_customer_name;
	var i_vertical_head='';
	
	try
	{
	 if(request.getMethod()=='GET')
	 {	 	
	   var i_projectID=request.getParameter('custscript_project_id_s');
	   // var i_projectID = (request.getParameter("custscript_project_id_s") == null) ? "" : request.getParameter("custscript_project_id_s");
       nlapiLogExecution('DEBUG', ' suiteletFunction',' Project ID -->' + i_projectID);
	   
	    //---------------added by swati-----------------------------------------------
		//var i_employeeID=request.getParameter('custscript_employee_id');
		//
		
		 var i_employeeID = (request.getParameter("custscript_employee_id") == null) ? "" : request.getParameter("custscript_employee_id");
		 nlapiLogExecution('DEBUG', ' suiteletFunction',' i_employeeID -->' + i_employeeID);
	   //--------------------------------------------------------------------------------
	 
	   if(_logValidation(i_projectID))
      {
		var o_projectOBJ = nlapiLoadRecord('job',i_projectID);
		i_vertical_head =  o_projectOBJ.getFieldValue('custentity_verticalhead');
		nlapiLogExecution('DEBUG', 'i_vertical_head','i_vertical_head-->' + i_vertical_head);
		
		i_customer_name =  o_projectOBJ.getFieldText('parent');
		nlapiLogExecution('DEBUG', 'suiteletFunction',' Customer-->' + i_customer_name);
		
		
		var i_project_name_ID =  o_projectOBJ.getFieldValue('entityid');
		nlapiLogExecution('DEBUG', 'suiteletFunction',' Project Name ID -->' + i_project_name_ID);
							
		var i_project_name =  o_projectOBJ.getFieldValue('companyname');
		nlapiLogExecution('DEBUG', 'suiteletFunction',' Project Name -->' + i_project_name);
		
		var i_territory	=	nlapiLookupField('job', i_projectID, 'customer.territory');
		
		s_project_description = i_project_name_ID+' '+i_project_name;
		nlapiLogExecution('DEBUG', 'suiteletFunction',' Project Description -->' + s_project_description);
		
	    if(_logValidation(i_employeeID))
			{
			     
				 //----------practice value get from employee master
				var o_employeeOBJ = nlapiLoadRecord('employee',i_employeeID);
				var i_practice=o_employeeOBJ.getFieldValue('department');
				nlapiLogExecution('DEBUG', 'suiteletFunction',' i_practice11111 -->' + i_practice);
			}	
			else{
					
					
					if(_logValidation(o_projectOBJ))
					{
						
						
						
						
						////-------------practice value get from project-------------------
						i_practice =  o_projectOBJ.getFieldValue('custentity_practice');
						nlapiLogExecution('DEBUG', 'suiteletFunction',' i_practice-->' + i_practice);
						
						//i_practice =  o_projectOBJ.getFieldValue('custentity_practice');
						//nlapiLogExecution('DEBUG', 'suiteletFunction',' i_practice-->' + i_practice);
						}
			
			/*
 if (_logValidation(i_customer)) 
			 {
			 	try
				{
					var o_custOBJ = nlapiLoadRecord('customer', i_customer);
				}
			    catch(ex)
				{
						var o_custOBJ = nlapiLoadRecord('job', i_customer);
				}
			 	
			 	if (_logValidation(o_custOBJ)) 
				{
			 		i_customer_name = o_custOBJ.getFieldValue('entitytitle');
			 	    nlapiLogExecution('DEBUG', 'suiteletFunction',' Customer Name -->' + i_customer_name);
			 	}
			 }	
*/		
						
		}//Project OBJ				
	}//Project ID
   else if(_logValidation(i_employeeID))
			{
			     
				 //----------practice value get from employee master
				var o_employeeOBJ = nlapiLoadRecord('employee',i_employeeID);
				var i_practice=o_employeeOBJ.getFieldValue('department');
				nlapiLogExecution('DEBUG', 'suiteletFunction',' i_practice11111 -->' + i_practice);
			}	

	a_return_array =s_project_description+'&&%%%**'+i_customer_name+'&&%%%**'+i_practice+'&&%%%**'+i_vertical_head+'&&%%%**'+i_territory;
	 }
		
	}
    catch(exception)
	{
	  nlapiLogExecution('DEBUG', 'ERROR',' Exception Caught -->' + exception);	
	}//CATCH
	
	response.write(a_return_array);

}

// END SUITELET ====================================================




// BEGIN OBJECT CALLED/INVOKING FUNCTION ===================================================

function _logValidation(value) 
{
 if(value != null && value.toString() != null && value != '' && value != undefined && value.toString() != undefined && value != 'undefined' && value.toString() != 'undefined'&& value.toString() != 'NaN' && value != NaN) 
 {
  return true;
 }
 else 
 { 
  return false;
 }
}
// END OBJECT CALLED/INVOKING FUNCTION =====================================================
