/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       10 Apr 2019     Aazamali Khan
 *
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType
 * 
 * @param {String} type Operation types: create, edit, delete, xedit,
 *                      approve, cancel, reject (SO, ER, Time Bill, PO & RMA only)
 *                      pack, ship (IF only)
 *                      dropship, specialorder, orderitems (PO only) 
 *                      paybills (vendor payments)
 * @returns {Void}
 */
function userEventBeforeSubmit(type){
  if(type == "edit"){
	  var allocatedResource = nlapiGetFieldValue("custrecord_frf_details_allocated_emp");
	  if(allocatedResource){
		  var oppId = nlapiGetFieldValue("custrecord_frf_details_opp_id"); // get the opportunity id from frf details record
		  var projectId = nlapiGetFieldValue("custrecord_frf_details_project"); // get the project id from frf details record
		  if(_logValidation(oppId)){
			  var searchResult = GetFulfillmentID(oppId);
			  var fulfillmenntDBId = searchResult[0].getId();
			  var fulfillmentDBRec = nlapiLoadRecord("customrecord_fulfillment_dashboard_data", fulfillmenntDBId);
			  var frfNumber = fulfillmentDBRec.getFieldValue("custrecord_fulfill_dashboard_frf");
			  if(frfNumber){
				  frfNumber = parseInt(frfNumber);
				  if(frfNumber > 0){
					 frfNumber = frfNumber - 1 ;
					 fulfillmentDBRec.setFieldValue("custrecord_fulfill_dashboard_frf", frfNumber)
				  }
				  nlapiSubmitRecord(fulfillmentDBRec);
			  }
		  }else{
			  var searchResult = GetFulfillmentIDForProject(projectId);
			  if(searchResult){
				  var fulfillmenntDBId = searchResult[0].getId();
			  var fulfillmentDBRec = nlapiLoadRecord("customrecord_fulfillment_dashboard_data", fulfillmenntDBId);
			  var frfNumber = fulfillmentDBRec.getFieldValue("custrecord_fulfill_dashboard_frf");
			  if(frfNumber){
				  frfNumber = parseInt(frfNumber);
				  if(frfNumber > 0){
					 frfNumber = frfNumber - 1 ;
					 fulfillmentDBRec.setFieldValue("custrecord_fulfill_dashboard_frf", frfNumber)
				  }
				  nlapiSubmitRecord(fulfillmentDBRec);
		  }	
			  }
	  }
	  }
	  }
	  }
function GetFulfillmentID(i_fulfillmentDB) {
	var customrecord_fulfillment_dashboard_dataSearch = nlapiSearchRecord("customrecord_fulfillment_dashboard_data",null,
			[
			 ["custrecord_fulfill_dashboard_opp_id","anyof",i_fulfillmentDB]
			 ], 
			 [

			  ]
	);
	return customrecord_fulfillment_dashboard_dataSearch;
}
function GetFulfillmentIDForProject(i_fulfillmentDB) {
	var customrecord_fulfillment_dashboard_dataSearch = nlapiSearchRecord("customrecord_fulfillment_dashboard_data",null,
			[
			 ["custrecord_fulfill_dashboard_project","anyof",i_fulfillmentDB]
			 ], 
			 [

			  ]
	);
	return customrecord_fulfillment_dashboard_dataSearch;
}
function _logValidation(value){
	if (value != null && value != '' && value != undefined && value != 'NaN') {
		return true;
	}
	else {
		return false;
	}
}