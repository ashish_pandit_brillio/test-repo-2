    /**
     * @NApiVersion 2.x
     * @NScriptType Restlet
     * @NModuleScope SameAccount
     */
     define(['N/record', 'N/search', 'N/runtime'],
     function (obj_Record, obj_Search, obj_Runtime) {
         function Send_Project_data(obj_Request) {
             try {
                 log.debug('requestBody ==', JSON.stringify(obj_Request));
                 var obj_Usage_Context = obj_Runtime.getCurrentScript();
                 var s_Request_Type = obj_Request.RequestType;
                 log.debug('s_Request_Type ==', s_Request_Type);
                 var s_Request_Data = obj_Request.RequestData;
                 log.debug('s_Request_Data ==', s_Request_Data);
                 var obj_Response_Return = {};
                 if (s_Request_Data == 'PROJECT') {
                     obj_Response_Return = get_Project_Data(obj_Search);
                 }
             } 
             catch (s_Exception) {
                 log.debug('s_Exception==', s_Exception);
             } //// 
             log.debug('obj_Response_Return==', JSON.stringify(obj_Response_Return));
             log.debug('Final Remaining Usage  ==', obj_Usage_Context.getRemainingUsage());
             return obj_Response_Return;
         } ///// Function Send_Project_data
 
 
         return {
             post: Send_Project_data
         };
 
     }); //// End of Main Function
     /****************** Supporting Functions ***********************************/
     function get_Project_Data(obj_Search) {
     try {
         var jobSearchObj = obj_Search.create({
             type: "job",
             filters:
             [
             ],
             columns:
             [
             obj_Search.createColumn({name: "internalid", label: "Internal ID"}),
             obj_Search.createColumn({
                 name: "internalid",
                 join: "customer",
                 label: "Customer Internal ID"
             }),
             obj_Search.createColumn({
                 name: "entityid",
                 sort: obj_Search.Sort.ASC,
                 label: "ID"
             }),
             obj_Search.createColumn({name: "altname", label: "Name"}),
             obj_Search.createColumn({
                 name: "internalid",
                 join: "CUSTENTITY_PRACTICE",
                 label: "Internal ID"
             }),
             obj_Search.createColumn({name: "custentity_project_services", label: "Project Services"}),
             obj_Search.createColumn({
                 name: "internalid",
                 join: "CUSTENTITY_PROJECTMANAGER",
                 label: "Internal ID"
             }),
             obj_Search.createColumn({name: "jobbillingtype", label: "Billing Type"}),
             obj_Search.createColumn({name: "jobtype", label: "Project Type"}),
             obj_Search.createColumn({name: "custentity_project_allocation_category", label: "Project Allocation Category"}),
             obj_Search.createColumn({name: "startdate", label: "Start Date"}),
             obj_Search.createColumn({name: "enddate", label: "Actual End Date"}),
             obj_Search.createColumn({name: "custentity_deliverymanager", label: "Delivery Manager"}),
             obj_Search.createColumn({
                name: "custentity_fusion_empid",
                join: "CUSTENTITY_PROJECTMANAGER",
                label: "Fusion Employee Id"
             }),
             obj_Search.createColumn({
                name: "custentity_fusion_empid",
                join: "CUSTENTITY_DELIVERYMANAGER",
                label: "Fusion Employee Id"
             })
             ]
         });
         var searchResultCount = jobSearchObj.runPaged().count;
         log.debug("jobSearchObj result count",searchResultCount);
         var myResults = getAllResults(jobSearchObj);
         var arr_Proj_json = [];
         myResults.forEach(function (result) {
             var obj_json_Container = {}
             obj_json_Container = {
                 'Internal_ID': result.getValue({ name: "internalid", label: "Internal ID" }),
                 'Customer_ID': result.getValue({ name: "internalid", join: "customer"}),
                 'Project_ID': result.getValue({ name: "entityid"}),
                 'Project_Name':result.getValue({name: 'altname'}),
                 'Project_Manager':result.getValue({name: 'altname'}),
                 'Practice':result.getValue({name: "internalid", join: "CUSTENTITY_PRACTICE"}),
                 'Project_Service': result.getValue({name: "custentity_project_services"}),
                 'Project_Manager_ID':result.getValue({name: "internalid",join: "CUSTENTITY_PROJECTMANAGER"}),
                 'Delivery_Manager_ID':result.getValue({name: "custentity_deliverymanager"}),
                 'Billing_Type':result.getText({name: "jobbillingtype"}),
                 'Project_Type':result.getText({name: "jobtype"}),
                 'Project_Allocation_Category':result.getText({name: "custentity_project_allocation_category"}),
                 'Start_Date':result.getValue({name: "startdate"}),
                 'End_Date':result.getValue({name: "enddate"}),
                 'Project_Manager_Employee_ID':result.getValue({name: "custentity_fusion_empid",join: "CUSTENTITY_PROJECTMANAGER"}),
                 'Delivery_Manager_Employee_ID':result.getValue({name: "custentity_fusion_empid",join: "CUSTENTITY_DELIVERYMANAGER"})
             };
             arr_Proj_json.push(obj_json_Container);
             return true;
         });
         return arr_Proj_json;
     } //// End of try
     catch (s_Exception) {
         log.debug('get_Project_Data s_Exception== ', s_Exception);
     } //// End of catch 
     } ///// End of function get_Project_Data()
 
 
     function getAllResults(s) {
     var result = s.run();
     var searchResults = [];
     var searchid = 0;
     do {
         var resultslice = result.getRange({ start: searchid, end: searchid + 1000 });
         resultslice.forEach(function (slice) {
             searchResults.push(slice);
             searchid++;
         }
         );
     } while (resultslice.length >= 1000);
     return searchResults;
     }