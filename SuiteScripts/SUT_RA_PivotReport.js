// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
/*
   	Script Name:
	Author:
	Company:
	Date:
	Description:


	Script Modification Log:

	-- Date --			-- Modified By --				--Requested By--				-- Description --



Below is a summary of the process controls enforced by this script file.  The control logic is described
more fully, below, in the appropriate function headers and code blocks.


     SUITELET
		- suiteletFunction(request, response)


     SUB-FUNCTIONS
		- The following sub-functions are called by the above core functions in order to maintain code
            modularization:

               - NOT USED

*/
}
// END SCRIPT DESCRIPTION BLOCK  ====================================



// BEGIN GLOBAL VARIABLE BLOCK  =====================================
{
	//  Initialize any Global Variables, in particular, debugging variables...




}
// END GLOBAL VARIABLE BLOCK  =======================================





// BEGIN SUITELET ==================================================

function createPivotReport(request, response)
{

	/*  Suitelet:

          - EXPLAIN THE PURPOSE OF THIS FUNCTION

		-


		FIELDS USED:

          --Field Name--				--ID--


	*/



	//  LOCAL VARIABLES



    //  SUITELET CODE BODY

	        var fromDate=request.getParameter('fromdate');
			var toDate=request.getParameter('todate');
			nlapiLogExecution('DEBUG','In Post','fromDate=='+fromDate);
			nlapiLogExecution('DEBUG','In Post','toDate=='+toDate);

			var no_of_days=getDatediffIndays(fromDate,toDate)
			var no_of_weekdays=getWeekend(fromDate,toDate)
			var totalWorkingDay=parseFloat(no_of_days)-parseFloat(no_of_weekdays)
			nlapiLogExecution('DEBUG','In Post','no_of_days=='+no_of_days+'-no_of_weekdays=='+no_of_weekdays+'-totalWorkingDay=='+totalWorkingDay);

			var totalNoofhrs=parseFloat(totalWorkingDay)*8
			nlapiLogExecution('DEBUG','In Post','totalNoofhrs=='+totalNoofhrs);

			//-----------------------------------------------------------------------------------------------------
			/*var report_Filters= new Array();
			var search = nlapiLoadSearch(null, 'customsearch_salaried_new_time_s_2_3_2');
			var columns = search.getColumns();
			var filters = search.getFilters();

			report_Filters[0]=new nlobjSearchFilter('date',null,'onorafter',fromDate)
			report_Filters[1]=new nlobjSearchFilter('date',null,'onorbefore',toDate)
			search.addFilters(report_Filters);
			nlapiLogExecution('DEBUG','In report','columns=='+columns);
			nlapiLogExecution('DEBUG','In report','filters=='+filters);
			if(search!=null)
			{
				nlapiLogExecution('DEBUG','In report','s=='+search.length);
				nlapiLogExecution('DEBUG','In report','s c=='+columns.length);
			}

			var resultSet = search.runSearch();
			nlapiLogExecution('DEBUG','In report','resultSet=='+resultSet);
			nlapiLogExecution('DEBUG','In report','resultSet legth=='+resultSet.length);

			var i=1
			resultSet.forEachResult(function(searchResult)
			{
			//sum += parseFloat(searchResult.getValue('total'));   // process the search result
			nlapiLogExecution('DEBUG','In report','Record Start==========================================='+i++)
			var employee=searchResult.getValue('employee',null,'group')
			var employee=searchResult.getText('employee',null,'group')
			//nlapiLogExecution('DEBUG','In report','employee=='+employee);
			var durationdecimal=searchResult.getValue('durationdecimal',null,'sum')
			//nlapiLogExecution('DEBUG','In report','durationdecimal=='+durationdecimal);

			var date=searchResult.getValue('date',null,'group')
			//nlapiLogExecution('DEBUG','In report','date=='+date);

			var releasedate=searchResult.getValue('releasedate','employee','group')
			//nlapiLogExecution('DEBUG','In report','releasedate=='+releasedate);
			var hiredate=searchResult.getValue('hiredate','employee','group')
			//nlapiLogExecution('DEBUG','In report','hiredate=='+hiredate);

			var subsidiary=searchResult.getValue('subsidiary','employee','group')
			var subsidiary=searchResult.getText('subsidiary','employee','group')

			var project=searchResult.getValue('altname','job','group')
			nlapiLogExecution('DEBUG','In report','employee=='+employee+'  durationdecimal=='+durationdecimal+ ' subsidiary=='+subsidiary+'-project-'+project+'---i---'+i++);
			nlapiLogExecution('DEBUG','In report','searchResult=='+searchResult.length)

			//------------------------------------------------------------------------------------
			    var fromDate=request.getParameter('fromdate');
				var toDate=request.getParameter('todate');
				nlapiLogExecution('DEBUG','In Post','fromDate=='+fromDate+'==toDate=='+toDate);
				nlapiLogExecution('DEBUG','In Post','hiredate=='+hiredate+'releasedate=='+releasedate);
				hiredateD=nlapiStringToDate(hiredate)
				releasedateD=nlapiStringToDate(releasedate)
				fromDateD=nlapiStringToDate(fromDate)
				toDateD=nlapiStringToDate(toDate)

				nlapiLogExecution('DEBUG','In Post','fromDate=='+fromDateD+'==toDate=='+toDateD);
				nlapiLogExecution('DEBUG','In Post','hiredate=='+hiredateD+'releasedate=='+releasedateD);
				if(hiredate!=null)
				{
					if(Date.parse(hiredateD)>=Date.parse(fromDateD)&&Date.parse(hiredateD)<=Date.parse(toDateD))
					{
						fromDate=hiredate
					}
					else
					{
						fromDate=fromDate
					}
				}
				if(releasedate!=null)
				{
					if(Date.parse(releasedateD)<=Date.parse(toDateD) && Date.parse(releasedateD)>=Date.parse(fromDateD))
					{
						toDate=releasedate
					}
					else
					{
						toDate=toDate
					}
				}
				nlapiLogExecution('DEBUG','In Post=====','fromDate=='+fromDate+'==toDate=='+toDate);
				var no_of_days=getDatediffIndays(fromDate,toDate)
				var no_of_weekdays=getWeekend(fromDate,toDate)
				var totalWorkingDay=parseFloat(no_of_days)-parseFloat(no_of_weekdays)
				nlapiLogExecution('DEBUG','In Post','no_of_days=='+no_of_days+'-no_of_weekdays=='+no_of_weekdays+'-totalWorkingDay=='+totalWorkingDay);

				var totalNoofhrs=parseFloat(totalWorkingDay)*8
				nlapiLogExecution('DEBUG','In Post','totalNoofhrs=='+totalNoofhrs);
			//------------------------------------------------------------------------------------




			return true;                // return true to keep iterating
			});*/

			     var reportDefinition = nlapiCreateReportDefinition();

			     //Define the rows hierarchy and the actual column data
			    var employee = reportDefinition.addRowHierarchy('employee', 'employee', 'TEXT');
				var Projet = reportDefinition.addRowHierarchy('altname', 'Projet', 'TEXT');

				var Department= reportDefinition.addRowHierarchy('date',  'Date', 'TEXT');
				var LopDays= reportDefinition.addRowHierarchy('releasedate',  'releasedate', 'TEXT');
				var paidDays= reportDefinition.addRowHierarchy('hiredate',  'hiredate',  'TEXT');

				//var Department= reportDefinition.addColumn('date', false, 'Date', null, 'TEXT', null);
				//var LopDays= reportDefinition.addColumn('releasedate', false, 'releasedate', null, 'TEXT', null);
				//var paidDays= reportDefinition.addColumn('hiredate', false, 'hiredate', null, 'TEXT', null);


				//Define the Column hierarchy and the actual column data
				//  var payrollComponent = reportDefinition.addColumnHierarchy('custrecord_payprocess_component', 'Payroll Component', null, 'TEXT');
				  var total = reportDefinition.addColumn('durationdecimal', true, 'Hrs.',null, 'FLOAT', null);//Value

				  //Create the search to feed the report
	    var columns = new Array();
	    columns[0] = new nlobjSearchColumn('employee', null, 'group');
	    columns[1] = new nlobjSearchColumn('altname','job','group');
	    columns[2] = new nlobjSearchColumn('date',null,'group');
	    columns[3] = new nlobjSearchColumn('releasedate','employee','group');
	    columns[4] = new nlobjSearchColumn('hiredate','employee','group');
	    columns[5] = new nlobjSearchColumn('durationdecimal',null,'sum');
	    //columns[6] = new nlobjSearchColumn('custrecord_payprocess_paymonth', null, 'group');
	   // columns[7] = new nlobjSearchColumn('custrecord_apm_pryprocess_lopdays', null, 'group');
	    //columns[8] = new nlobjSearchColumn('custrecord_apm_payprocess_paid_days', null, 'group');

	    //Add search to the report and map the search columns to the reports columns
	    var filters = new Array();
	    filters[0] = new nlobjSearchFilter('date',null,'onorafter',fromDate);
	    filters[1] = new nlobjSearchFilter('date',null,'onorbefore',toDate);



	    reportDefinition.addSearchDataSource('timebill', 'customsearch_salaried_new_ti_8', filters, columns,
	    {'employee':columns[0], 'altname':columns[1], 'date':columns[2], 'releasedate':columns[3],
	     'hiredate':columns[4],'durationdecimal':columns[5]});

	    reportDefinition.setTitle('RA Report')
	    //Create a form to build the report on
	    var form = nlapiCreateReportForm('RA Report');

	    //Build the form from the report definition
	    var pvtTable = reportDefinition.executeReport(form);

	    //Write the form to the browser
	    response.writePage(form);
		//alert('Sum: ' + sum)
	 //------------------------------------------------------------------------------------------------------------------------------------



}

// END SUITELET ====================================================




// BEGIN OBJECT CALLED/INVOKING FUNCTION ===================================================
{

function getDatediffIndays(startDate, endDate)
{
    var one_day=1000*60*60*24;
    var fromDate = startDate;
    var toDate=endDate

    var date1 = nlapiStringToDate(fromDate);

    var date2 = nlapiStringToDate(toDate);

    var date3=(date2-date1)/one_day;

    return (date3+1);
}

function getWeekend(startDate,endDate)
{
var i_no_of_sat = 0 ;
var i_no_of_sun = 0 ;

nlapiLogExecution('DEBUG','startDate',startDate);
nlapiLogExecution('DEBUG','endDate',endDate);
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 var date_format = checkDateFormat();

var startDate_1 = startDate
 var endDate_1 = endDate

 startDate = nlapiStringToDate(startDate);
 endDate = nlapiStringToDate(endDate);

 var i_count_day = startDate.getDate();

 var i_count_last_day = endDate.getDate();

 i_month = startDate.getMonth()+1;

 i_year = startDate.getFullYear();

var d_f = new Date();
var getTot = getDatediffIndays(startDate_1, endDate_1); //Get total days in a month

var sat = new Array();   //Declaring array for inserting Saturdays
var sun = new Array();   //Declaring array for inserting Sundays

for(var i=i_count_day;i<=i_count_last_day;i++)
{    //looping through days in month

  if (date_format == 'YYYY-MM-DD')
  {
   var newDate = i_year + '-' + i_month + '-' + i;
  }
  if (date_format == 'DD/MM/YYYY')
  {
    var newDate = i + '/' + i_month + '/' + i_year;
  }
  if (date_format == 'MM/DD/YYYY')
  {
    var newDate = i_month + '/' + i + '/' + i_year;
  }

    newDate = nlapiStringToDate(newDate);

    if(newDate.getDay()==0)
 {   //if Sunday
        sat.push(i);
  i_no_of_sat++;
    }
    if(newDate.getDay()==6)
 {   //if Saturday
        sun.push(i);
  i_no_of_sun++;
    }


}

 var i_total_days_sat_sun = parseInt(i_no_of_sat)+parseInt(i_no_of_sun);
 return i_total_days_sat_sun ;
}


function checkDateFormat()
{
    var context = nlapiGetContext();

	var dateFormatPref = context.getPreference('dateformat');

    return dateFormatPref;
}

}
// END OBJECT CALLED/INVOKING FUNCTION =====================================================
