/**
 * Operation Excellence Metrics
 * 
 * Version Date Author Remarks 1.00
 * 
 * 12 Aug 2016 Deepak MS
 * 
 */

function suitelet(request, response) {
	try {
	
	
	if(request.getMethod() == 'GET')
	{
	var current_date = nlapiDateToString(new Date());
	nlapiLogExecution('DEBUG', 'Current Date', 'current_date...' + current_date);
	//Create the form and add fields to it
	var form = nlapiCreateForm("Invoice Report");
	
	form.addFieldGroup('custpage_grp_0', 'Select Options');
	var customerField = form.addField('custpage_customer',
		        'select', 'Customer', 'customer', 'custpage_grp_0').setBreakType('startcol');
				
		customerField.setMandatory(true); 		
				
	var startDate = form.addField('custpage_startdate', 'date', 'Start Date', null,
		        'custpage_grp_0')
		startDate.setBreakType('startcol');	
startDate.setMandatory(true); 	
	var endDate = form.addField('custpage_enddate',
		        'date', 'End Date', null, 'custpage_grp_0').setBreakType('startcol');
		endDate.setMandatory(true); 	
				
	form.addSubmitButton('Load Projects');
	
		response.writePage(form);		
	}
	else
	{
	var form = nlapiCreateForm("Invoice Report");
	
	form.addFieldGroup('custpage_grp_01', 'Select Options');
	var cuID = request.getParameter('custpage_customer');
	
	var customerName  = nlapiLookupField('customer',parseInt(cuID),['entityid','altname']);
	var customerField1 = form.addField('custpage_customer_post',
		        'text', 'Customer', null, 'custpage_grp_01').setDefaultValue(customerName.altname);
	//customerField1.setBreakType('startcol');
	//customerField1.setDisplayType('inline');	
var st_date = request.getParameter('custpage_startdate');
var en_date = request.getParameter('custpage_enddate')	
	var startDate1 = form.addField('custpage_startdate_post', 'date', 'Start Date', null,
		        'custpage_grp_01').setDefaultValue(request.getParameter('custpage_startdate'));
	//startDate1.setBreakType('startcol');
	//startDate1.setDisplayType('inline');		
	var endDate1 = form.addField('custpage_enddate_post',
		        'date', 'End Date', null, 'custpage_grp_01').setDefaultValue(request.getParameter('custpage_enddate'));
	//endDate1.setBreakType('startcol');
	//endDate1.setDisplayType('inline');
				
	form.addFieldGroup('custpage_grp_1', 'Projects');			
	
	var projectsList  = searchProjects(cuID);
	var html  = createTableList(projectsList,st_date,en_date);
	var tableField = form.addField('custpage_html', 'inlinehtml', null,
			        null, 'custpage_grp_1')
			tableField.setDefaultValue(html);
			tableField.setBreakType('startcol');
			response.writePage(form);
	}
}	
catch(e){
nlapiLogExecution('DEBUG','Process Error',e);
}	
}
function searchProjects(customerID){
try{

var filters = Array();
filters.push(new nlobjSearchFilter('internalid', null, 'is', customerID));
filters.push(new nlobjSearchFilter('isinactive',
		                'job', 'is', 'F'));

var cols = Array();
cols.push(new nlobjSearchColumn('altname','job'));
cols.push(new nlobjSearchColumn('internalid','job'));

var projectSearchObj = nlapiSearchRecord('customer',null,filters,cols);
var dataRow  = [];
var jobList ={};
for(var i=0;i<projectSearchObj.length;i++){

jobList = {
	Name: projectSearchObj[i].getValue('altname','job'),
	Id: projectSearchObj[i].getValue('internalid','job')
	
	
	};
	dataRow.push(jobList);
}
return dataRow;
}
catch(e){
nlapiLogExecution('DEBUG',' Project Search Error',e);
}

}

function createTableList(project_List,stDate,end_date){
try{




	var html = "";
	// table css
	html += "<style>";
	html += ".ops-metrics-table td {border:1px solid black;}";
	html += ".ops-metrics-table tr:nth-child(even) {background: #ffffe6}";
	html += ".ops-metrics-table tr:nth-child(odd) {background: #e6ffe6}";
	html += ".ops-metrics-table tr:nth-child(even):hover {background: #ffff99}";
	html += ".ops-metrics-table tr:nth-child(odd):hover {background: #80ff80}";
	html += ".ops-metrics-table .header td {background-color: #001a33 !important; color : white !important;}";
	html += "</style>";

	// table
	html += "<table class='ops-metrics-table' width='200%' >";

	html += "<tr class='header'>";

	html += "<td  font-size='20'>";
	html += "Project";
	html += "</td>";

	html += "<td  font-size='20'>";
	html += "View Link";
	html += "</td>";
	
	html += "</tr>";	
	// table header
		for(var j=0;j<project_List.length;j++){
		var projectData = project_List[j];
		if(projectData.Name){
		html += "<tr>";
		html += "<td font-size='14'>";
				html += projectData.Name;
				html += "</td>";

				var linkURL = nlapiResolveURL('SUITELET', 'customscript_sut_invoice_employee_report','customdeploy_sut_invoice_employee_report', null)
				+ '&custpage_project_id=' + projectData.Id + '&custpage_start_date=' + stDate + '&custpage_end_date=' + end_date ;
				
				 var suiteletUrl = nlapiResolveURL('SUITELET', 'customscript_sut_invoice_employee_report', 'customdeploy_sut_invoice_employee_report', false);

				var param = '&custpage_project_id=' + projectData.Id + '&custpage_start_date=' + stDate + '&custpage_end_date=' + end_date;

				//var url = 'https://debugger.sandbox.netsuite.com'+suiteletUrl+param;
				var url = 'https://system.na1.netsuite.com'+suiteletUrl+param;
				var finalUrl = url + suiteletUrl + param;
				nlapiLogExecution('debug','url',url);
				nlapiLogExecution('debug','finalUrl',finalUrl);
				//var request  = nlapiRequestURL();
				
				html += "<td  font-size='14' >";
				html += "<a href="+url+">View</a>";
			   
				html += "</td>";
				//html += "<td width='100%' font-size='14'>";
				//Define the parameters of the Suitelet that will be executed.
			//html +="<p><a href="http://www.w3schools.com/html/">Visit our HTML tutorial</a></p>";
			
			//Create a link to launch the Suitelet.
			//createNewReqLink.setDefaultValue('<B>Click <A HREF="' + linkURL +'">here</A> to
			//create a new document signature request record.</B>');
				
			//	html +="<a href="http://www.w3schools.com">View</a>";
			//	html += "<p>View<a href  ="https://forms.na1.netsuite.com/app/site/hosting/scriptlet.nl?script=681&deploy=1&compid=3883006&h=889d72db68c85c6ca6a0" ></p>";

				html += "</td>";
		html += "</tr>";
		}
	}
	html += "</table>";

	return html;

}
catch(e){
nlapiLogExecution('DEBUG',' Project Search Error',e);
}

}