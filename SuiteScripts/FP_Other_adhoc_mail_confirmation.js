function Adhoc_trigger_fp_other()
{
	var i_revenue_share_id = nlapiGetFieldValue('custrecord_fp_others_mnth_end_fp_parent');
	var i_projectId = nlapiLookupField('customrecord_fp_rev_rec_others_parent',i_revenue_share_id,'custrecord_fp_rev_rec_others_projec');
	var i_revenue_share_stat = '2';
	var i_mnth_end_json_id = nlapiGetRecordId();
	// Get mail id for practice head
	var a_recipient_mail_id = new Array();
	var a_practice_involved = new Array();
	var a_revenue_cap_filter = [['custrecord_fp_rev_rec_others_parent_link.internalid', 'anyof', parseInt(i_revenue_share_id)]];
			nlapiLogExecution('DEBUG','trigger','triggered');
			var a_columns_existing_cap_srch = new Array();
			a_columns_existing_cap_srch[0] = new nlobjSearchColumn('custrecord_fp_rev_rec_others_practice',null,'group');
			a_columns_existing_cap_srch[1] = new nlobjSearchColumn('custrecord_fp_rev_rec_others_sub_practic',null,'group');
		
			var a_exsiting_revenue_cap = nlapiSearchRecord('customrecord_fp_rev_rec_others_eff_plan', null, a_revenue_cap_filter, a_columns_existing_cap_srch);
			if (a_exsiting_revenue_cap)
			{
				for(var i_revenue_index = 0; i_revenue_index < a_exsiting_revenue_cap.length; i_revenue_index++)
				{
					var i_parent_parctice_id = a_exsiting_revenue_cap[i_revenue_index].getValue('custrecord_fp_rev_rec_others_practice',null,'group');
					var i_sub_parctice_id = a_exsiting_revenue_cap[i_revenue_index].getValue('custrecord_fp_rev_rec_others_sub_practic',null,'group');
					
					if(a_practice_involved.indexOf(i_parent_parctice_id) < 0)
					{
						a_practice_involved.push(i_parent_parctice_id);
					}
					if(a_practice_involved.indexOf(i_sub_parctice_id) < 0)
					{
						a_practice_involved.push(i_sub_parctice_id);
					}
				}
			}
			
			var a_filters_search_practice = [['internalid','anyof',a_practice_involved]
									  ];
									  
			var a_columns_practice = new Array();
			a_columns_practice[0] = new nlobjSearchColumn('email', 'custrecord_practicehead');
			
			var a_parctice_srch = nlapiSearchRecord('department', null, a_filters_search_practice, a_columns_practice);
			if (a_parctice_srch)
			{
				for(var i_practice_index = 0; i_practice_index < a_parctice_srch.length; i_practice_index++)
				{
					a_recipient_mail_id.push(a_parctice_srch[i_practice_index].getValue('email', 'custrecord_practicehead'));
				}
			}
			
			var a_participating_mail_id = new Array();
			
			var a_projectData = nlapiLookupField('job', i_projectId, [
				        'custentity_projectmanager', 'custentity_deliverymanager',
				        'customer', 'custentity_clientpartner']);
						
			var s_recipient_mail_pm = nlapiLookupField('employee',a_projectData.custentity_projectmanager,'email');
			var s_recipient_mail_dm = nlapiLookupField('employee',a_projectData.custentity_deliverymanager,'email');
			var s_recipient_mail_cp = nlapiLookupField('employee',a_projectData.custentity_clientpartner,'email');
			var i_client_cp = nlapiLookupField('customer',a_projectData.customer,'custentity_clientpartner');
			var s_recipient_mail_acp = nlapiLookupField('employee',i_client_cp,'email');
			
			if(s_recipient_mail_pm)
			{
				a_participating_mail_id.push(s_recipient_mail_pm);
			}
			if(s_recipient_mail_dm)
			{
				a_participating_mail_id.push(s_recipient_mail_dm);
			}
			if(s_recipient_mail_cp)
			{
				a_participating_mail_id.push(s_recipient_mail_cp);
			}
			if(s_recipient_mail_acp)
			{
				a_participating_mail_id.push(s_recipient_mail_acp);
			}
			
			a_participating_mail_id.push('billing@brillio.com');
			a_participating_mail_id.push('vikash.garodia@brillio.com');
			a_participating_mail_id.push('bhavanishankar.t@brillio.com');
			a_participating_mail_id.push('team.fpa@brillio.com');
	
			var o_proj_rcrd = nlapiLoadRecord('job',i_projectId);
			var s_proj_id = o_proj_rcrd.getFieldValue('entityid');
			var s_proj_name = o_proj_rcrd.getFieldValue('companyname');
			var s_proj_full_name = s_proj_id +' '+ s_proj_name;
			
			var s_project_region = o_proj_rcrd.getFieldText('custentity_region');
			var s_project_cust = o_proj_rcrd.getFieldText('parent');
			var s_project_strt = o_proj_rcrd.getFieldValue('startdate');
			var s_project_end = o_proj_rcrd.getFieldValue('enddate');
			var s_project_prac = o_proj_rcrd.getFieldText('custentity_practice');
			
			// mail to BO team and participating practice head
			var strVar = '';
			strVar += '<html>';
			strVar += '<body>';
			
			strVar += '<p>Hello All,</p>';
			strVar += '<p>The effort for the following Fixed Price project has been confirmed in Netsuite by PM’.</p>';
			
			strVar += '<p>Region:- '+s_project_region+'<br>';
			strVar += 'Customer:- '+s_project_cust+'<br>';
			strVar += 'Project:- '+s_proj_full_name+'<br>';
			strVar += 'Project Start '+s_project_strt+' and End Date '+s_project_end+'<br>';
			strVar += 'Executing Practice:- '+s_project_prac+'</p>';
			
			strVar += '<a href=https://system.na1.netsuite.com/app/site/hosting/scriptlet.nl?script=1518&deploy=1&proj_id='+i_projectId+'&mode=Submit&mnth_end_activity_id='+i_mnth_end_json_id+'&revenue_share_id='+i_revenue_share_id+'&revenue_rcrd_status='+i_revenue_share_stat+'>Link to access the Effort and Revenue details screen in Netsuite</a>';
			
			strVar += '<p>Regards,</p>';
			strVar += '<p>Finance Team</p>';
				
			strVar += '</body>';
			strVar += '</html>';
			
			var a_emp_attachment = new Array();
			a_emp_attachment['entity'] = '39108';
			
			nlapiSendEmail(442, a_recipient_mail_id, 'FP Project Monthly effort confirmed: '+s_proj_full_name, strVar,a_participating_mail_id,'sai.vannamareddy@brillio.com',a_emp_attachment);
			//nlapiSendEmail(442, 'sai.vannamareddy@brillio.com', 'FP Project Monthly effort confirmed: '+s_proj_full_name, strVar,null,'sai.vannamareddy@brillio.com',a_emp_attachment);
}