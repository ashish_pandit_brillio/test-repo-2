/**
 * Screen for Department head to view all the allocations under him
 * 
 * Version    Date            Author           Remarks
 * 1.00       02 Jul 2015     Nitish Mishra
 *
 */

/**
 * @param {nlobjRequest}
 *            request Request object
 * @param {nlobjResponse}
 *            response Response object
 * @returns {Void} Any output is written via response object
 */
function suitelet(request, response) {
	try {
		var method = request.getMethod();
		var mode = request.getParameter('mode');

		if (method == 'GET') {

			if (mode == 'excel') {

				// check user display setting
				var ctx = nlapiGetContext();
				var employee_wise_display = ctx.getSetting('SCRIPT',
				        'custscript_alloc_employee_wise_view');

				if (employee_wise_display == 'T') {
					exportFormToExcel2(request);
				} else {
					exportFormToExcel(request);
				}
			} else {
				createGetForm(request);
			}
		} else {
			submitForm(request);
		}
	} catch (err) {
		nlapiLogExecution('error', 'suitelet', err);
		displayErrorForm(err, "Resource Allocations");
	}
}

function exportFormToExcel(request) {
	try {
		var user = nlapiGetUser();

		if (user == 9673) {
			user = 15585;
		}

		var filterProject = request.getParameter('project');
		var filterCustomer = request.getParameter('customer');
		var filterStartDate = request.getParameter('startdate');
		var filterEndDate = request.getParameter('enddate');

		var project_list = getRelatedAllocations(user, filterProject,
		        filterCustomer, filterStartDate, filterEndDate);

		var csvText = "";

		// add project wise sub tabs
		project_list.forEach(function(allocation) {
			csvText += getProjectCsvDetails(allocation);
		});

		var file = nlapiCreateFile('Allocation Details.csv', 'CSV', csvText);
		response.setContentType('CSV', 'Allocation Details.csv');
		response.write(file.getValue());
	} catch (err) {
		nlapiLogExecution('error', 'exportFormToExcel', err);
		throw err;
	}
}

function exportFormToExcel2(request) {
	try {
		var user = nlapiGetUser();

		if (user == 9673) {
			user = 15585;
		}

		var filterProject = request.getParameter('project');
		var filterCustomer = request.getParameter('customer');
		var filterStartDate = request.getParameter('startdate');
		var filterEndDate = request.getParameter('enddate');

		var project_list = getRelatedAllocations2(user, filterProject,
		        filterCustomer, filterStartDate, filterEndDate);

		var csvText = "";
		project_list.Labels.forEach(function(text) {
			csvText += text;
			csvText += ",";
		});

		csvText += "\r\n";

		project_list.Values.forEach(function(allocation) {

			allocation.forEach(function(text) {
				csvText += text;
				csvText += ",";
			});
			csvText += "\r\n";
		});

		var file = nlapiCreateFile('Allocation Details.csv', 'CSV', csvText);
		response.setContentType('CSV', 'Allocation Details.csv');
		response.write(file.getValue());
	} catch (err) {
		nlapiLogExecution('error', 'exportFormToExcel2', err);
		throw err;
	}
}

function getProjectCsvDetails(projectDetails) {
	var projectId = projectDetails.Id;

	// var projectData = nlapiLookupField('job', projectId, [ 'entityid',
	// 'altname' ]);
	var text = "";

	var projectRecord = nlapiLoadRecord('job', projectId);
	var billingType = projectRecord.getFieldText('jobbillingtype');
	var isMonthly = projectRecord.getFieldValue('custentity_t_and_m_monthly') == 'T' ? " Monthly"
	        : "";

	text += "Client," + projectRecord.getFieldText('parent') + "\n\n";

	text += "Project ID, Project Name, Project Type, Start Date, End Date, Project Manager, Delivery Manager,"
	        + " Client Partner\n";
	text += projectRecord.getFieldValue('entityid') + ","
	        + projectRecord.getFieldValue('altname') + "," + billingType
	        + isMonthly + "," + projectRecord.getFieldValue('startdate') + ","
	        + projectRecord.getFieldValue('enddate') + ","
	        + projectRecord.getFieldText('custentity_projectmanager') + ","
	        + projectRecord.getFieldText('custentity_deliverymanager') + ","
	        + projectRecord.getFieldText('custentity_clientpartner') + "\n";

	// text += "\n";

	var projectName = projectRecord.getFieldValue('entityid') + " : "
	        + projectRecord.getFieldValue('altname');

	text += "\n";
	text += getAllocationCsv(projectDetails.PresentAllocation, "Allocations",
	        projectName);
	text += "\n";
	text += "\n";

	// text += "\n";
	// text += getAllocationCsv(projectDetails.PresentAllocation,
	// "Allocations");

	// text += "\n";
	// text += getAllocationCsv(projectDetails.PastAllocation, "Past
	// Allocations");
	// text += "\n";

	return text;
}

function getAllocationCsv(allocationList, allocationLabel) {
	var text = "";

	var lineCount = allocationList.length;
	text += "Resource, Time Approver, Expense Approver, Start Date , End Date, Billable ,"
	        + " Bill Rate , %age Allocation, "
	        + "Billing Start Date, Billing End Date, Site, Location, Practice\n";

	for (var linenum = 0; linenum < lineCount; linenum++) {
		text += allocationList[linenum].resourcename + ",";
		text += allocationList[linenum].timeapprover_text + ",";
		text += allocationList[linenum].expenseapprover_text + ",";
		text += allocationList[linenum].startdate + ",";
		text += allocationList[linenum].enddate + ",";
		text += (allocationList[linenum].isbillable == 'T' ? 'Yes' : 'No')
		        + ",";
		text += allocationList[linenum].billrate + ",";
		text += allocationList[linenum].percent + ",";
		text += allocationList[linenum].billingstartdate + ",";
		text += allocationList[linenum].billingstartdate + ",";
		text += allocationList[linenum].site_text + ",";
		text += allocationList[linenum].location_text + ",";
		text += allocationList[linenum].practice_text + "\n";
	}

	return text;
}

function createGetForm(request) {
	try {
		var user = nlapiGetUser();

		if (user == 9673) {
			user = 15585;
		}

		var filterProject = request.getParameter('project');
		var filterCustomer = request.getParameter('customer');
		var filterStartDate = request.getParameter('startdate');
		var filterEndDate = request.getParameter('enddate');

		nlapiLogExecution('debug', 'filterProject', filterProject);
		nlapiLogExecution('debug', 'filterCustomer', filterCustomer);
		nlapiLogExecution('debug', 'filterStartDate', filterStartDate);
		nlapiLogExecution('debug', 'filterEndDate', filterEndDate);

		var form = nlapiCreateForm('Resource Allocations');
		form.addButton('custpage_refresh', 'Refresh', 'refreshAllocations');

		var customerField = form.addField('custpage_filter_customer', 'select',
		        'Customer', 'customer');
		if (filterCustomer) {
			customerField.setDefaultValue(filterCustomer);
		}

		var projectField = form.addField('custpage_filter_project', 'select',
		        'Project', 'job');
		if (filterProject) {
			projectField.setDefaultValue(filterProject);
		}

		var startDateField = form.addField('custpage_filter_start_date',
		        'date', 'From Date');
		if (filterStartDate) {
			startDateField.setDefaultValue(filterStartDate);
		}

		var endDateField = form.addField('custpage_filter_end_date', 'date',
		        'To Date');
		if (filterEndDate) {
			endDateField.setDefaultValue(filterEndDate);
		}

		var allocation_details = getRelatedAllocations(user, filterProject,
		        filterCustomer, filterStartDate, filterEndDate);

		nlapiLogExecution('debug', 'check', JSON.stringify(allocation_details));

		if (allocation_details.length > 0) {
			var i = 0;
			form.setScript('customscript_cs_sut_dept_wise_alloc_view');

			// add project wise sub tabs
			allocation_details.forEach(function(allocation) {
				i = i + 1;
				addProjectTab(form, allocation, i, filterCustomer,
				        filterStartDate, filterEndDate);
			});

			// form.addSubmitButton('Submit');
			form.addButton('custpage_export', 'Export All', 'exportToExcel');
		} else {
			form.addField('custpage_label', 'text', '')
			        .setDisplayType('inline').setDefaultValue(
			                'No Projects Available');
		}

		response.writePage(form);
	} catch (err) {
		nlapiLogExecution('error', 'createGetForm', err);
		throw err;
	}
}

function addProjectTab(form, allocation, jobIndex, customer, startDate, endDate)
{

	if (allocation.PresentAllocation.length > 0) {
		var projectId = allocation.Id;
		var tabId = 'custpage_tab_' + projectId; // id of the sub tab
		form.addTab(tabId, allocation.Name); // add a new tab

		addProjectSublist(form, projectId, tabId);

		// add present allocation sublist to the tab
		var present_allocation_list = form.addSubList('custpage_present_list_'
		        + projectId, 'list', 'Allocations', tabId);
		addSublistColumns(present_allocation_list);
		present_allocation_list.setLineItemValues(allocation.PresentAllocation);

		// add past allocation sublist to the tab
		// var past_allocation_list = form.addSubList('custpage_past_list_'
		// + projectId, 'list', 'Past Allocations', tabId);
		// addSublistColumns(past_allocation_list);
		// past_allocation_list.setLineItemValues(allocation.PastAllocation);

		// add a field to store the project
		form.addField('custpage_job_' + jobIndex, 'select', 'Project', 'job',
		        tabId).setDisplayType('hidden').setDefaultValue(projectId);

		if (allocation.PresentAllocation.length > 0) {
			var csvExportUrl = nlapiResolveURL('SUITELET',
			        'customscript_sut_dept_head_alloc_view',
			        'customdeploy_sut_dept_head_alloc_view')
			        + "&mode=excel&project="
			        + (projectId ? projectId : '')
			        + "&customer="
			        + (customer ? customer : '')
			        + "&startdate="
			        + (startDate ? startDate : '')
			        + "&enddate="
			        + (endDate ? endDate : '');

			form
			        .addField('custpage_export_' + jobIndex, 'url', '', null,
			                tabId).setDisplayType('inline').setLinkText(
			                'Export To Excel').setDefaultValue(csvExportUrl);
		}
	}
}

function addProjectSublist(form, projectId, tabId) {
	try {
		var projectRecord = nlapiLoadRecord('job', projectId);

		var projectUrl = nlapiResolveURL('SUITELET',
		        'customscript_sut_pm_project_details',
		        'customdeploy_sut_pm_project_details')
		        + "&pi=" + projectId;

		var projectList = form.addSubList('custpage_project_details'
		        + projectId, 'staticlist', 'Project Details', tabId);

		projectList.addField('text_a', 'text', '').setDisplayType('inline');
		projectList.addField('text_b', 'text', '').setDisplayType('inline');
		projectList.addField('text_c', 'text', '').setDisplayType('inline');
		projectList.addField('text_d', 'text', '').setDisplayType('inline');

		var billingType = projectRecord.getFieldText('jobbillingtype');
		var isMonthly = projectRecord
		        .getFieldValue('custentity_t_and_m_monthly') == 'T' ? " Monthly"
		        : "";

		var valueList = [
		        {
		            text_a : '<b>Project : </b>' + '<a target="_blank" href="'
		                    + projectUrl + '">'
		                    + projectRecord.getFieldValue('entityid') + " : "
		                    + projectRecord.getFieldValue('altname') + '</a>',
		            text_b : '<b>Customer : </b>'
		                    + projectRecord.getFieldText('parent'),
		            text_c : '<b>Status : </b>'
		                    + projectRecord.getFieldText('entitystatus'),
		            text_d : '<b>Billing Type : </b>' + billingType + isMonthly
		        },
		        {
		            text_a : '<b>Vertical : </b>'
		                    + projectRecord.getFieldText('custentity_vertical'),
		            text_b : '<b>Practice : </b>'
		                    + projectRecord.getFieldText('custentity_practice'),
		            text_c : '<b>Start Date : </b>'
		                    + projectRecord.getFieldValue('startdate'),
		            text_d : '<b>End Date : </b>'
		                    + projectRecord.getFieldValue('enddate')
		        },
		        {
		            text_a : '<b>Project Manager : </b>'
		                    + projectRecord
		                            .getFieldText('custentity_projectmanager'),
		            text_b : '<b>Delivery Manager : </b>'
		                    + projectRecord
		                            .getFieldText('custentity_deliverymanager'),
		            text_c : '<b>Client Partner : </b>'
		                    + projectRecord
		                            .getFieldText('custentity_clientpartner'),
		            text_d : '<b>Project Value : </b>'
		                    + projectRecord
		                            .getFieldValue('custentity_projectvalue')
		        } ];

		projectList.setLineItemValues(valueList);
	} catch (err) {
		nlapiLogExecution('ERROR', 'addProjectSublist', err);
		throw err;
	}
}

function addSublistColumns(sublist) {
	sublist.addField('resource', 'select', 'Resource', 'employee')
	        .setDisplayType('inline');
	sublist.addField('resourcename', 'text', 'Resource Name').setDisplayType(
	        'hidden');
	sublist.addField('startdate', 'date', 'Start Date')
	        .setDisplayType('inline');
	sublist.addField('enddate', 'date', 'End Date').setDisplayType('inline');
	sublist.addField('isbillable', 'checkbox', 'Is Billable?').setDisplayType(
	        'inline');
	sublist.addField('billrate', 'currency', 'Bill Rate').setDisplayType(
	        'inline');
	sublist.addField('percent', 'percent', '% Allocation').setDisplayType(
	        'inline');
	sublist.addField('billingstartdate', 'date', 'Billing Start Date')
	        .setDisplayType('inline');
	sublist.addField('billingenddate', 'date', 'Billing End Date')
	        .setDisplayType('inline');
	// sublist.addField('practice', 'select', 'Practice', 'department')
	// .setDisplayType('inline');
	sublist.addField('timeapprover', 'select', 'Time Approver', 'employee')
	        .setDisplayType('inline');
	sublist.addField('expenseapprover', 'select', 'Expense Approver',
	        'employee').setDisplayType('inline');
	sublist.addField('site', 'select', 'Site', 'customlistonsiteoffsitelist')
	        .setDisplayType('inline');
	// sublist.addField('location_text', 'text', 'Location').setDisplayType(
	// 'inline');
	sublist.addField('change', 'text', 'Change Made').setDisplayType('inline');
	sublist.addField('ismarked', 'checkbox', 'Modified').setDisplayType(
	        'hidden');
	// sublist.addField('remarks', 'textarea', 'Remarks').setDisplaySize(20, 2)
	// .setDisplayType('entry');
}

function getRelatedAllocations(user, projectId, customer, startDate, endDate) {
	try {

		var relatedPractices = [];

		// search for the related practices in the practice master
		var practiceSearch = nlapiSearchRecord('department', null, [
		        new nlobjSearchFilter('custrecord_practicehead', null, 'anyof',
		                user),
		        new nlobjSearchFilter('isinactive', null, 'is', 'F') ]);

		if (practiceSearch) {

			practiceSearch.forEach(function(prac) {
				relatedPractices.push(prac.getId());
			});
		}

		nlapiLogExecution('debug', 'related practices', relatedPractices.length);

		if (relatedPractices.length == 0) {
			throw "No Practices Linked To The User";
		}

		var filters = [ new nlobjSearchFilter('custevent_practice', null,
		        'anyof', relatedPractices) ];

		if (projectId) {
			filters.push(new nlobjSearchFilter('project', null, 'anyof',
			        projectId));
		}

		if (customer) {
			filters.push(new nlobjSearchFilter('customer', 'job', 'anyof',
			        customer));
		}

		if (startDate && endDate) {
			filters.push(new nlobjSearchFilter('enddate', null, 'notbefore',
			        startDate));
			filters.push(new nlobjSearchFilter('startdate', null, 'notafter',
			        endDate));
		} else if (startDate) {
			filters.push(new nlobjSearchFilter('enddate', null, 'notbefore',
			        startDate));
		} else if (endDate) {
			filters.push(new nlobjSearchFilter('startdate', null, 'notafter',
			        endDate));
		} else {
			filters.push(new nlobjSearchFilter('enddate', null, 'onorafter',
			        'today'));
		}

		var allocationSearch = searchRecord(
		        'resourceallocation',
		        null,
		        filters,
		        [
		                new nlobjSearchColumn('resource'),
		                new nlobjSearchColumn('project'),
		                new nlobjSearchColumn('startdate'),
		                new nlobjSearchColumn('enddate'),
		                new nlobjSearchColumn('custeventrbillable'),
		                new nlobjSearchColumn('percentoftime'),
		                new nlobjSearchColumn('custeventbstartdate'),
		                new nlobjSearchColumn('custeventbenddate'),
		                new nlobjSearchColumn('custeventwlocation'),
		                new nlobjSearchColumn('custevent3'),
		                new nlobjSearchColumn('custevent_practice'),
		                new nlobjSearchColumn('custevent4'),
		                new nlobjSearchColumn('custevent_allocation_status'),
		                new nlobjSearchColumn('approver', 'employee'),
		                new nlobjSearchColumn('custevent_ra_last_modified_date')
		                        .setSort(true),
		                new nlobjSearchColumn('timeapprover', 'employee') ]);

		if (allocationSearch) {
			nlapiLogExecution('debug', 'allocation count',
			        allocationSearch.length);
		}

		var presentAllocation = [];
		var pastAllocation = [];
		var futureAllocation = [];
		var projectList = [];

		if (allocationSearch) {
			var currentDate = new Date();
			var weekStartDate = getWeekStartDate();

			allocationSearch
			        .forEach(function(allocation) {
				        var endDate = nlapiStringToDate(allocation
				                .getValue('enddate'));
				        var startDate = nlapiStringToDate(allocation
				                .getValue('startdate'));

				        projectList.push(allocation.getValue('project'));

				        var currentAllocation = {
				            project : allocation.getValue('project'),
				            projectName : allocation.getText('project'),
				            resource : allocation.getValue('resource'),
				            resourcename : allocation.getText('resource'),
				            startdate : allocation.getValue('startdate'),
				            enddate : allocation.getValue('enddate'),
				            isbillable : allocation
				                    .getValue('custeventrbillable'),
				            billrate : allocation.getValue('custevent3'),
				            percent : allocation.getValue('percentoftime'),
				            location : allocation
				                    .getValue('custeventwlocation'),
				            billingstartdate : allocation
				                    .getValue('custeventbstartdate'),
				            billingenddate : allocation
				                    .getValue('custeventbenddate'),
				            practice : allocation
				                    .getValue('custevent_practice'),
				            practice_text : allocation
				                    .getText('custevent_practice'),
				            timeapprover : allocation.getValue('timeapprover',
				                    'employee'),
				            timeapprover_text : allocation.getText(
				                    'timeapprover', 'employee'),
				            expenseapprover : allocation.getValue('approver',
				                    'employee'),
				            expenseapprover_text : allocation.getText(
				                    'approver', 'employee'),
				            site : allocation.getValue('custevent4'),
				            site_text : allocation.getText('custevent4'),
				            location : allocation
				                    .getValue('custeventwlocation'),
				            location_text : allocation
				                    .getText('custeventwlocation'),
				            ismarked : 'F',
				            lastmodified : '',
				            change : ''
				        };

				        var last_modified_date = nlapiStringToDate(allocation
				                .getValue('custevent_ra_last_modified_date'));

				        if (last_modified_date) {
					        currentAllocation.lastmodified = nlapiDateToString(
					                last_modified_date, 'date');

					        if (weekStartDate <= last_modified_date) {
						        currentAllocation.ismarked = 'T';
						        currentAllocation.change = allocation
						                .getText('custevent_allocation_status');

					        }
				        }

				        if (currentDate > endDate) {
					        pastAllocation.push(currentAllocation);
					        // } else if (currentDate < startDate) {
					        // futureAllocation.push(currentAllocation);
				        } else {
					        presentAllocation.push(currentAllocation);
				        }
			        });
		}

		nlapiLogExecution('debug', 'presentAllocation',
		        presentAllocation.length);
		nlapiLogExecution('debug', 'futureAllocation', futureAllocation.length);
		nlapiLogExecution('debug', 'pastAllocation', pastAllocation.length);

		// remove duplicate projects
		nlapiLogExecution('debug', 'projectList A', projectList.length);
		projectList = _.uniq(projectList);
		nlapiLogExecution('debug', 'projectList B', projectList.length);

		// divide the allocations project wise
		var projectWiseAllocationList = [];

		projectList.forEach(function(currentProjectId) {
			var projectWiseEntry = {
			    Id : currentProjectId,
			    Name : nlapiLookupField('job', currentProjectId, 'altname'),
			    PresentAllocation : [],
			    PastAllocation : []
			};

			// loop through the allocation list and push the allocation (past /
			// present) to the respective project array
			presentAllocation.forEach(function(allocation) {

				if (allocation.project == currentProjectId) {
					projectWiseEntry.PresentAllocation.push(allocation);
				}
			});

			pastAllocation.forEach(function(allocation) {

				if (allocation.project == currentProjectId) {
					projectWiseEntry.PastAllocation.push(allocation);
				}
			});

			projectWiseAllocationList.push(projectWiseEntry);
		});

		return projectWiseAllocationList;
	} catch (err) {
		nlapiLogExecution('ERROR', 'getRelatedAllocations', err);
		throw err;
	}
}

function getRelatedAllocations2(user, projectId, customer, startDate, endDate) {
	try {

		var relatedPractices = [];

		// search for the related practices in the practice master
		var practiceSearch = nlapiSearchRecord('department', null, [
		        new nlobjSearchFilter('custrecord_practicehead', null, 'anyof',
		                user),
		        new nlobjSearchFilter('isinactive', null, 'is', 'F') ]);

		if (practiceSearch) {

			practiceSearch.forEach(function(prac) {
				relatedPractices.push(prac.getId());
			});
		}

		nlapiLogExecution('debug', 'related practices', relatedPractices.length);

		if (relatedPractices.length == 0) {
			throw "No Practices Linked To The User";
		}

		var filters = [ new nlobjSearchFilter('custevent_practice', null,
		        'anyof', relatedPractices) ];

		if (projectId) {
			filters.push(new nlobjSearchFilter('project', null, 'anyof',
			        projectId));
		}

		if (customer) {
			filters.push(new nlobjSearchFilter('customer', 'job', 'anyof',
			        customer));
		}

		if (startDate && endDate) {
			filters.push(new nlobjSearchFilter('enddate', null, 'notbefore',
			        startDate));
			filters.push(new nlobjSearchFilter('startdate', null, 'notafter',
			        endDate));
		} else if (startDate) {
			filters.push(new nlobjSearchFilter('enddate', null, 'notbefore',
			        startDate));
		} else if (endDate) {
			filters.push(new nlobjSearchFilter('startdate', null, 'notafter',
			        endDate));
		} else {
			filters.push(new nlobjSearchFilter('enddate', null, 'onorafter',
			        'today'));
		}

		var allocationSearch = nlapiSearchRecord('resourceallocation', 1071,
		        filters);

		if (allocationSearch) {
			nlapiLogExecution('debug', 'allocation count',
			        allocationSearch.length);
		}

		var allocationList = [], allColumns = [];

		if (allocationSearch) {
			var allColumns = allocationSearch[0].getAllColumns();
			var labels = [];

			for (var j = 0; j < allColumns.length; j++) {
				labels[j] = allColumns[j].getLabel();
			}

			for (var i = 0; i < allocationSearch.length; i++) {
				allocationList[i] = [];
				var allColumns = allocationSearch[i].getAllColumns();

				for (var j = 0; j < allColumns.length; j++) {
					var value = allocationSearch[i].getValue(allColumns[j]);
					var text = allocationSearch[i].getText(allColumns[j]);

					if (text) {
						text = text.replace(/,/g, " ");
					}

					if (value) {
						value = value.replace(/,/g, " ");
					}

					text = (text == "T" ? "Yes" : (text == "F" ? "No" : text));
					value = (value == "T" ? "Yes" : (value == "F" ? "No"
					        : value));
					allocationList[i][j] = text ? text : value;
				}
			}
		}

		// nlapiLogExecution('debug', 'allocation list', JSON
		// .stringify(allocationList));
		return {
		    Labels : labels,
		    Values : allocationList
		};
	} catch (err) {
		nlapiLogExecution('ERROR', 'getRelatedAllocations2', err);
		throw err;
	}
}

function getWeekStartDate() {
	var currentDate = new Date();
	var daysFromSunday = currentDate.getDay();
	var sundayOfWeek = nlapiAddDays(currentDate, -daysFromSunday);
	return sundayOfWeek;
}

function getRelatedProjects(currentUser) {
	try {

		var projectSearch = nlapiSearchRecord('job', null,
		        [
		                [
		                        [ 'custentity_projectmanager', 'anyof',
		                                currentUser ],
		                        'or',
		                        [ 'custentity_deliverymanager', 'anyof',
		                                currentUser ] ], 'and',
		                [ 'status', 'anyof', '2' ] ], [
		                new nlobjSearchColumn('entityid'),
		                new nlobjSearchColumn('altname'),
		                new nlobjSearchColumn('customer') ]);

		var project_id = [];
		var project_list = [];

		if (projectSearch) {

			projectSearch.forEach(function(project) {
				project_id.push(project.getId());

				project_list.push({
				    Id : project.getId(),
				    Name : project.getValue('altname')
				});
			});
		} else {
			throw "No Projects Found";
		}

		return {
		    IdList : project_id,
		    DropDownList : project_list
		};
	} catch (err) {
		nlapiLogExecution('ERROR', 'getRelatedProjects', err);
		throw err;
	}
}

// -- POST

function getPracticeRelatedProjects(practiceList) {
	try {
		var project_id = [], project_list = [];

		// search for the related practices in the practice master
		var projectSearch = nlapiSearchRecord('job', null, [
		        new nlobjSearchFilter('custentity_practice', null, 'anyof',
		                practiceList),
		        new nlobjSearchFilter('isinactive', null, 'is', 'F'),
		        new nlobjSearchFilter('status', null, 'anyof', '2') ],
		        [ new nlobjSearchColumn('altname') ]);

		if (projectSearch) {

			projectSearch.forEach(function(project) {
				project_id.push(project.getId());

				project_list.push({
				    Id : project.getId(),
				    Name : project.getValue('altname')
				});
			});
		}

		return {
		    IdList : project_id,
		    DropDownList : project_list
		};
	} catch (err) {
		nlapiLogExecution('ERROR', 'getPracticeRelatedProjects', err);
		throw err;
	}
}

function submitForm(request) {
	try {
		var project_count = request.getParameter('custpage_job_count');
		nlapiLogExecution('debug', 'project_count', project_count);

		var loggedUser = nlapiGetUser();
		// var userDetails = nlapiLookupField('employee', loggedUser, [
		// 'firstname', 'lastname', 'email' ]);
		// var requestorName = userDetails.firstname + " " +
		// userDetails.lastname;

		for (var i = 1; i <= project_count; i++) {
			var project_field_id = 'custpage_job_' + i;
			var project_id = request.getParameter(project_field_id);
			nlapiLogExecution('debug', '----',
			        '----------------------------------------');
			nlapiLogExecution('debug', 'project_id', project_id);
			var present_sublist_id = 'custpage_present_list_' + project_id;
			var past_sublist_id = 'custpage_past_list_' + project_id;
			// var future_sublist_id = 'custpage_future_list_' + project_id;
			var case_comments = [];

			var mailContent = "";
			mailContent += generateMailContentLineLevel(request,
			        present_sublist_id, "Present Allocations", case_comments);
			nlapiLogExecution('debug', '1', mailContent);
			if (mailContent) {
				mailContent += "\n";
			}

			mailContent += generateMailContentLineLevel(request,
			        past_sublist_id, "Past Allocations", case_comments);

			if (mailContent) {
				mailContent += "\n";
			}
			nlapiLogExecution('debug', '2', mailContent);

			// mailContent += generateMailContentLineLevel(request,
			// future_sublist_id, "Future Allocations", case_comments);

			nlapiLogExecution('debug', '3', mailContent);

			if (mailContent) {
				var projectDetails = nlapiLookupField('job', project_id, [
				        'entityid', 'altname' ]);
				mailContent = "Project : " + projectDetails.entityid + " "
				        + projectDetails.altname + "\n" + mailContent;

				var caseRecord = nlapiCreateRecord('supportcase', {
				    'customform' : '39',
				    'recordmode' : 'dynamic'
				});

				caseRecord.setFieldValue('title', 'Allocation Changes');
				caseRecord.setFieldValue('assigned', '10843');
				caseRecord.setFieldValue('custevent_case_project', project_id);
				caseRecord.setFieldValue('custevent_case_notes', JSON
				        .stringify(case_comments));
				caseRecord.setFieldValue('company', nlapiGetUser());
				caseRecord.setFieldValue('profile', '2');
				caseRecord.setFieldValue('item', '2512');
				caseRecord.setFieldValue('category', '4');
				caseRecord.setFieldValue('issue', '1');
				caseRecord.setFieldValue('origin', '3');
				caseRecord.setFieldValue('incomingmessage', mailContent);

				var caseId = nlapiSubmitRecord(caseRecord);
				nlapiLogExecution('audit', 'New Case Record Created',
				        'Internal Id' + caseId);
			}
		}

		nlapiLogExecution('debug', 'done');

		var form = nlapiCreateForm('Resource Allocation');
		form.setScript('customscript_cs_sut_pm_alloc');

		form.addField('custpage_msg', 'text', '').setDisplayType('inline')
		        .setDefaultValue('Your request has been submitted.');

		// form.addButton('custpage_btn_dashboard', 'Go To Dashboard',
		// 'goToDashboard');
		form.addButton('custpage_btn_back', 'Go Back', 'goBack');

		response.writePage(form);
	} catch (err) {
		nlapiLogExecution('ERROR', 'submitForm', err);
		throw err;
	}
}

function generateMailContentLineLevel(request, sublistId, allocationType,
        case_comments)
{
	try {
		var lineCount = request.getLineItemCount(sublistId);
		nlapiLogExecution('debug', sublistId + " " + allocationType, lineCount);

		var anyRemarks = false;
		var text = "\n" + allocationType + " :- \n";

		for (var linenum = 1; linenum <= lineCount; linenum++) {
			var remarks = request.getLineItemValue(sublistId, 'remarks',
			        linenum);

			if (isNotEmpty(remarks)) {
				nlapiLogExecution('debug', 'remarks', remarks);
				anyRemarks = true;
				text += request.getLineItemValue(sublistId, 'resourcename',
				        linenum);
				text += " : " + remarks;
				text += "\n";
				case_comments.push({
				    i : request
				            .getLineItemValue(sublistId, 'resource', linenum),
				    r : remarks
				});
			}
		}

		nlapiLogExecution('debug', 'A remarks ' + allocationType, text);
		if (!anyRemarks) {
			text = "";
		}
		nlapiLogExecution('debug', 'B remarks ' + allocationType, text);

		return text;
	} catch (err) {
		nlapiLogExecution('ERROR', 'generateMailContentLineLevel', err);
		throw err;
	}
}

function getAllocationCaseMailTemplate(tableData, userName, projectName,
        requestorName)
{
	var htmltext = '<table border="0" width="100%">';
	htmltext += '<tr>';
	htmltext += '<td colspan="4" valign="top">';
	htmltext += '<p>Hi,</p>';
	htmltext += '<p></p>';
	htmltext += '<p>This is to inform you that <b>' + userName
	        + '</b> has updated allocation details for <b>' + projectName
	        + '</b>.</p>';

	htmltext += tableData;

	htmltext += '<p></p>';
	htmltext += '</td></tr>';
	htmltext += '<tr></tr>';
	htmltext += '<tr></tr>';
	htmltext += '<p><br/>Regards,<br/>';
	htmltext += 'Information Systems</p>';
	htmltext += '</table>';
	htmltext += '<hr width="100%" size="1" noshade color="#CCCCCC">';
	htmltext += '<table width="100%" border="0" cellspacing="0" cellpadding="0">';
	htmltext += '<tr>';
	htmltext += '<td align="right">';
	htmltext += '<font style="font-size:9px; font-family:Verdana,Arial,Helvetica,sans-serif; color:#999999;">'
	        + 'Brillio is powered by <a href="http://www.netsuite.com/" style="color:#999999;">NetSuite</a>'
	        + ' &#151; One System. No Limits.</font>';
	htmltext += '</td>';
	htmltext += '</tr>';
	htmltext += '</table>';
	htmltext += '</body>';
	htmltext += '</html>';

	return {
	    Subject : 'Allocation Updation',
	    Body : htmltext
	};
}

function getPracticeHeadRelatedPractice() {
	try {
		var user = nlapiGetUser();
		var relatedPractices = [];

		// search for the related practices in the practice master
		var practiceSearch = nlapiSearchRecord('department', null, [
		        new nlobjSearchFilter('custrecord_practicehead', null, 'anyof',
		                user),
		        new nlobjSearchFilter('isinactive', null, 'is', 'F') ]);

		if (practiceSearch) {
			// var relatedPractices = [];

			practiceSearch.forEach(function(prac) {
				relatedPractices.push(prac.getId());
			});
		}

		return relatedPractices;
	} catch (err) {
		nlapiLogExecution('ERROR', 'getPracticeHeadRelatedPractice', err);
		throw err;
	}
}

// ----------Client Script

function refreshAllocations() {
	var customer = nlapiGetFieldValue('custpage_filter_customer');
	var project = nlapiGetFieldValue('custpage_filter_project');
	var startdate = nlapiGetFieldValue('custpage_filter_start_date');
	var enddate = nlapiGetFieldValue('custpage_filter_end_date');

	window.location = nlapiResolveURL('SUITELET',
	        'customscript_sut_dept_head_alloc_view',
	        'customdeploy_sut_dept_head_alloc_view')
	        + "&customer="
	        + customer
	        + "&project="
	        + project
	        + "&startdate="
	        + startdate + "&enddate=" + enddate;
}

function goBack() {
	window.location = nlapiResolveURL('SUITELET',
	        'customscript_sut_pm_weekly_allocation', 'customdeploy_sut_pm');
}

function goToDashboard() {
	var dashboardUrl = "/app/center/card.nl";
	window.location = dashboardUrl;
}

function getCSProjectDetails() {
	try {
		var selected_project = nlapiGetFieldValue('custpage_current_project');

		if (selected_project) {
			var script = 'customscript_sut_pm';
			var deployment = 'customdeploy_sut_pm';
			var url = "https://system.sandbox.netsuite.com/app/site/hosting/scriptlet.nl?script=585&deploy=1"
			        + "&project=" + selected_project;
			window.location = url;
		} else {
			alert("No Project Selected");
		}
	} catch (err) {
		nlapiLogExecution('ERROR', 'getCSProjectDetails', err);
		alert(err.message);
	}
}

function changeProject() {
	try {
		var url = "https://system.sandbox.netsuite.com/app/site/hosting/scriptlet.nl?script=585&deploy=1";
		window.location = url;
	} catch (err) {
		nlapiLogExecution('ERROR', 'getCSProjectDetails', err);
		alert(err.message);
	}
}

function exportToExcel() {
	try {
		var startdate = nlapiGetFieldValue('custpage_filter_start_date');
		var enddate = nlapiGetFieldValue('custpage_filter_end_date');
		var customer = nlapiGetFieldValue('custpage_filter_customer');
		var project = nlapiGetFieldValue('custpage_filter_project');

		var csvExportUrl = nlapiResolveURL('SUITELET',
		        'customscript_sut_dept_head_alloc_view',
		        'customdeploy_sut_dept_head_alloc_view')
		        + "&mode=excel&project="
		        + project
		        + "&customer="
		        + customer
		        + "&startdate=" + startdate + "&enddate=" + enddate;

		var win = window.open(csvExportUrl);
		win.focus();
	} catch (err) {
		nlapiLogExecution('ERROR', 'exportToExcel', err);
		alert(err.message);
	}
}

function readSublistDetails(allocation_details, projectId) {
	var projectName = nlapiGetFieldText(fieldId);
	var text = "";
	text += "Project Name" + projectName + "\n";

	return text;
}

function pageInit() {
	var cssElementContent = " .marked-row td { background-color : #E9FFE9 !important; } ";
	var headElement = document.getElementsByTagName('head')[0];
	var cssStyleElement = document.createElement('style');
	cssStyleElement.innerHTML = cssElementContent;
	headElement.appendChild(cssStyleElement);

	var projectCount = nlapiGetFieldValue('custpage_job_count');

	for (var i = 1; i <= projectCount; i++) {
		var projectId = nlapiGetFieldValue('custpage_job_' + i);
		var sublists = [ 'custpage_past_list_' + projectId,
		        'custpage_present_list_' + projectId ];

		for (var j = 0; j < 2; j++) {
			var linecount = nlapiGetLineItemCount(sublists[j]);

			for (var linenum = 1; linenum <= linecount; linenum++) {
				var isMarked = nlapiGetLineItemValue(sublists[j], 'ismarked',
				        linenum);

				if (isMarked == 'T') {
					document
					        .getElementById(sublists[j] + "row" + (linenum - 1)).style.fontWeight = "bold";
					document
					        .getElementById(sublists[j] + "row" + (linenum - 1)).className += " marked-row";

				}
			}
		}
	}
}