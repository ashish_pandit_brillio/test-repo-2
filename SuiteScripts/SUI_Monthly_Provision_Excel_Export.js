// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
/*
   	Script Name:SUI_Monthly_Provision_Excel_Export.js
	Author:Swati Kurariya
	Company:Aashna Cloud Tech Pvt. Ltd
	Date:04-02-2015
	Description:Export Suitelet Screen Data into excel file.


	Script Modification Log:

	-- Date --			-- Modified By --				--Requested By--				-- Description --



Below is a summary of the process controls enforced by this script file.  The control logic is described
more fully, below, in the appropriate function headers and code blocks.


     SUITELET
		- suiteletFunction(request, response)


     SUB-FUNCTIONS
		- The following sub-functions are called by the above core functions in order to maintain code
            modularization:

               - NOT USED

*/
}
// END SCRIPT DESCRIPTION BLOCK  ====================================



// BEGIN GLOBAL VARIABLE BLOCK  =====================================
{
	//  Initialize any Global Variables, in particular, debugging variables...




}
// END GLOBAL VARIABLE BLOCK  =======================================





// BEGIN SUITELET ==================================================

function suiteletFunction_Excel_Export(request, response)
{

	/*  Suitelet:

          - EXPLAIN THE PURPOSE OF THIS FUNCTION

		-Export Suitelet Screen Data into excel file.


		FIELDS USED:

          --Field Name--				--ID--


	*/



	//  LOCAL VARIABLES
      var d_From_Date;
	  var d_To_Date;
	  var o_Subsidairy;
	  var i_Unbilled_Type;
	  var i_Criteria;
	  var i_Customer;
	  var i;
	  var i_Month;
	  var i_Year;


    //  SUITELET CODE BODY
     //---------------------get papameter value---------------
     d_From_Date=request.getParameter('d_from_date');
	 d_To_Date=request.getParameter('d_to_date');
	 o_Subsidairy=request.getParameter('o_subsidairy');
	 i_Unbilled_Type=request.getParameter('i_unbilled_type');
	 i_Criteria=request.getParameter('i_criteria');
	 i_Customer=request.getParameter('i_customer');
	 i_Month=request.getParameter('i_month');
	 i_Year=request.getParameter('i_year');
	 
	 nlapiLogExecution('DEBUG','In Get','d_From_Date=='+d_From_Date);
	 nlapiLogExecution('DEBUG','In Get','d_To_Date=='+d_To_Date);
	 nlapiLogExecution('DEBUG','In Get','o_Subsidairy=='+o_Subsidairy);
	 nlapiLogExecution('DEBUG','In Get','i_Unbilled_Type=='+i_Unbilled_Type);
	 nlapiLogExecution('DEBUG','In Get','i_Criteria=='+i_Criteria);
	 nlapiLogExecution('DEBUG','In Get','i_Customer=='+i_Customer);
	 nlapiLogExecution('DEBUG','In Get','i_Month=='+i_Month);
	 nlapiLogExecution('DEBUG','In Get','i_Year=='+i_Year);
	 //--------------------------------------------------------
	 
	 if(i_Unbilled_Type == 1)
	 {//1 if start
	     nlapiLogExecution('DEBUG','If start for unbilled type 1============>');
		 
		   if(_logValidation(d_From_Date) && _logValidation(d_To_Date))
				{
					var filter = new Array();
					//filter[0] = new nlobjSearchFilter('trandate', null, 'onorbefore', i_current_date);
					filter[0] = new nlobjSearchFilter('trandate', null,'within',d_From_Date,d_To_Date);
					filter[1] = new nlobjSearchFilter('subsidiary', null, 'is', parseInt(o_Subsidairy));
					if(_logValidation(i_Customer))
						{
						filter[2] = new nlobjSearchFilter('customersubof', null, 'is',parseInt(i_Customer));
						}
						var i_search_results = searchRecord('expensereport','customsearch_unbilled_expense_search_2_4',filter,null);
						
					if(_logValidation (i_search_results))
					{
					
					  nlapiLogExecution('DEBUG', 'suiteletFunction',' i_search_results -->' + i_search_results.length);
					  var Fun_Status=Generate_Excel_Expense(i_search_results,o_Subsidairy);
					}
				}
					

				
	 
	 }//1 if close
	 else  if(i_Unbilled_Type == 2)
	 {//1 else close
	 
	 
	    //-----------------------------search the monthly provision allocation custom record-----------------------------------
		
		if(_logValidation (d_From_Date) && _logValidation(d_To_Date) &&  _logValidation(o_Subsidairy) && _logValidation(i_Criteria))
		{//2 if start
		    var filter = new Array();
				i=4;
                   // filter[0] = new nlobjSearchFilter('custrecord_from_date', null,'on',d_From_Date);	
   			    	//filter[1] = new nlobjSearchFilter('custrecord_to_date', null,'on',d_To_Date);
					 filter[0] = new nlobjSearchFilter('custrecord_month_val',null,'is',i_Month);
			    	  
	   			      filter[1] = new nlobjSearchFilter('custrecord_year_val',null,'is',i_Year);
					
   			    	filter[2] = new nlobjSearchFilter('custrecord_subsidiary',null,'is', parseInt(o_Subsidairy));
   			    	filter[3] = new nlobjSearchFilter('custrecord_criteria',null,'is', parseInt(i_Criteria));
   			    	if(i_Criteria == 3)
   			    	{
   			    		filter[i] = new nlobjSearchFilter('custrecord_billable',null,'is','T');
   			    		//i++;
						filter[filter.length] = new nlobjSearchFilter('custrecord_sum_of_not_submitted',null,'greaterthan',0);
						
   			    	}
   			    
					if(_logValidation(i_Customer))
						{
						filter[filter.length] = new nlobjSearchFilter('custrecord_customer',null,'is',parseInt(i_customer));
						}				   

					var i_search_results = searchRecord('customrecord_time_bill_rec','customsearch_time_bill_rec_search',filter,null);
					
					if(_logValidation (i_search_results))
					{
					
					  nlapiLogExecution('DEBUG', 'suiteletFunction',' i_search_results -->' + i_search_results.length);
					  var Fun_Status=Generate_Excel_Time(i_search_results,o_Subsidairy,i_Criteria);
					}
					
					
					
		}//2 if close
		
		//---------------------------------------------------------------------------------------------------------------------
	 
	 
	 }//1 else close




}

// END SUITELET ====================================================
//--------------------------------------------------------------------------------
function Generate_Excel_Time(i_search_results,o_Subsidairy,i_Criteria)
{//function start 
   var s_Subsi_Name;
                       
					   if(_logValidation(o_Subsidairy))
					   {
							o_Load_Subsidairy=nlapiLoadRecord('subsidiary',o_Subsidairy);
							s_Subsi_Name=o_Load_Subsidairy.getFieldValue('name');
							nlapiLogExecution('DEBUG','s_Subsi_Name',s_Subsi_Name);
					   }
					   else
					   {
							s_Subsi_Name='';
					   }
							
					//----------------------------------------------------------------------------
	                    var xmlString = '<?xml version="1.0"?><?mso-application progid="Excel.Sheet"?>'; 
						xmlString += '<Workbook xmlns="urn:schemas-microsoft-com:office:spreadsheet" ';
						xmlString += 'xmlns:o="urn:schemas-microsoft-com:office:office" ';
						xmlString += 'xmlns:x="urn:schemas-microsoft-com:office:excel" ';
						xmlString += 'xmlns:ss="urn:schemas-microsoft-com:office:spreadsheet" ';
						xmlString += 'xmlns:html="http://www.w3.org/TR/REC-html40">'; 

							
									   
									   if(i_Criteria == '3')
									   {
									   xmlString += '<Worksheet ss:Name="Sheet1">';
										xmlString += '<Table>' + 
									   '<Row>' +
									      '<Cell><Data ss:Type="String"> Record Id </Data></Cell>' +
										'<Cell><Data ss:Type="String"> S No </Data></Cell>' +
										'<Cell><Data ss:Type="String"> Employee </Data></Cell>' +
										'<Cell><Data ss:Type="String"> Project </Data></Cell>' +
										'<Cell><Data ss:Type="String"> Vertical </Data></Cell>' +
										'<Cell><Data ss:Type="String"> Practice </Data></Cell>' +
										'<Cell><Data ss:Type="String"> Date </Data></Cell>' +
										'<Cell><Data ss:Type="String"> Hour </Data></Cell>' +
										'<Cell><Data ss:Type="String"> Rate </Data></Cell>' +
										'<Cell><Data ss:Type="String"> Amount </Data></Cell>' +
										'<Cell><Data ss:Type="String"> Currency </Data></Cell>' +
										'<Cell><Data ss:Type="String"> Exchnage Rate </Data></Cell>' +
										'<Cell><Data ss:Type="String"> Subsidiary </Data></Cell>' +
										'<Cell><Data ss:Type="String"> Customer </Data></Cell>' +
										'<Cell><Data ss:Type="String"> Location </Data></Cell>' +
										'<Cell><Data ss:Type="String"> Billing Start Date </Data></Cell>' +
										'<Cell><Data ss:Type="String"> Billing End Date </Data></Cell>' +
										'<Cell><Data ss:Type="String"> Billed Days </Data></Cell>' +
										'<Cell><Data ss:Type="String"> Approved Days </Data></Cell>' +
										'<Cell><Data ss:Type="String"> Not Approved Days </Data></Cell>' +
										'<Cell><Data ss:Type="String"> Leave Days </Data></Cell>' +
										'<Cell><Data ss:Type="String"> Holiday Days </Data></Cell>' +
										'<Cell><Data ss:Type="String"> Floating Holiday Days </Data></Cell>' +
										'<Cell><Data ss:Type="String"> Not Submitted Days </Data></Cell>' +
										'<Cell><Data ss:Type="String"> Not Submitted Hours </Data></Cell>' +
										'<Cell><Data ss:Type="String"> Not Submitted Holiday </Data></Cell>' +
										'</Row>';
									   }
									   else{
									   
									    xmlString += '<Worksheet ss:Name="Sheet1">';
										xmlString += '<Table>' + 
									   '<Row>' +
										'<Cell><Data ss:Type="String"> Record Id </Data></Cell>' +
										'<Cell><Data ss:Type="String"> S No </Data></Cell>' +
										'<Cell><Data ss:Type="String"> Employee </Data></Cell>' +
										'<Cell><Data ss:Type="String"> Project </Data></Cell>' +
										'<Cell><Data ss:Type="String"> Vertical </Data></Cell>' +
										'<Cell><Data ss:Type="String"> Practice </Data></Cell>' +
										'<Cell><Data ss:Type="String"> Date </Data></Cell>' +
										'<Cell><Data ss:Type="String"> Hour </Data></Cell>' +
										'<Cell><Data ss:Type="String"> Rate </Data></Cell>' +
										'<Cell><Data ss:Type="String"> Amount </Data></Cell>' +
										'<Cell><Data ss:Type="String"> Currency </Data></Cell>' +
										'<Cell><Data ss:Type="String"> Exchnage Rate </Data></Cell>' +
										'<Cell><Data ss:Type="String"> Subsidiary </Data></Cell>' +
										'<Cell><Data ss:Type="String"> Customer </Data></Cell>' +
										'<Cell><Data ss:Type="String"> Location </Data></Cell>' +
										'</Row>';
                                         }
										
									for(var c=0;c<i_search_results.length;c++)	//for(var c=(rowCount - rowsPerPage);c<rowCount;c++)//for(var c=0;c<100;c++)	
									{ //for start
									
									       //---------------------------get all value from search----------------------
										   
											var rec_id=i_search_results[c].getId();
											//nlapiLogExecution('DEBUG', 'columnLen', 'rec_id'+rec_id);
											var a_search_transaction_result = i_search_results[c];
											var columns=a_search_transaction_result.getAllColumns();
											// nlapiLogExecution('DEBUG', 'columnLen', 'columns= '+columns.length);
											var columnLen = columns.length;
										   // nlapiLogExecution('DEBUG', 'columnLen', 'columnLen= '+columnLen);
											
											var i_date=a_search_transaction_result.getValue(columns[0]);
											//nlapiLogExecution('DEBUG', 'suiteletFunction',' i_date -->' + i_date);
											i_date1=i_date;
											
											var i_employee=a_search_transaction_result.getText(columns[1]);
										    //nlapiLogExecution('DEBUG', 'suiteletFunction',' i_employee -->' + i_employee);
												
											var i_project=a_search_transaction_result.getValue(columns[35]);
											//nlapiLogExecution('DEBUG', 'suiteletFunction',' i_employee -->' + i_employee);
												
											var i_practice=a_search_transaction_result.getText(columns[8]);
											//nlapiLogExecution('DEBUG', 'suiteletFunction',' i_practice -->' + i_practice);
											
											var i_subsidiary=o_Subsidairy;//a_search_transaction_result.getValue(columns[10]);
											//nlapiLogExecution('DEBUG', 'suiteletFunction',' i_subsidiary -->' + i_subsidiary);
											if(i_Criteria == 3)
											{
											   var i_hours=a_search_transaction_result.getValue(columns[33]);
											//nlapiLogExecution('DEBUG', 'suiteletFunction',' i_hours -->' + i_hours);
											}
											else
											{
											var i_hours=a_search_transaction_result.getValue(columns[18]);
											//nlapiLogExecution('DEBUG', 'suiteletFunction',' i_hours -->' + i_hours);
											}
												
											
											
											var i_customer=a_search_transaction_result.getText(columns[9]);
											//nlapiLogExecution('DEBUG', 'suiteletFunction',' i_customer -->' + i_customer);
											
											var i_rate=a_search_transaction_result.getValue(columns[12]);
											//nlapiLogExecution('DEBUG', 'suiteletFunction',' i_rate -->' + i_rate);
											
											var i_currency=a_search_transaction_result.getText(columns[13]);
											//nlapiLogExecution('DEBUG', 'suiteletFunction',' i_currency -->' + i_currency);
											
											var i_exchange_rate=a_search_transaction_result.getValue(columns[14]);
										    //nlapiLogExecution('DEBUG', 'suiteletFunction',' i_exchange_rate -->' + i_exchange_rate);
											
											var i_vertical=a_search_transaction_result.getText(columns[15]);
											//nlapiLogExecution('DEBUG', 'suiteletFunction',' i_vertical -->' + i_vertical);
											
											var i_location=a_search_transaction_result.getText(columns[16]);
											//nlapiLogExecution('DEBUG', 'suiteletFunction',' i_location -->' + i_location);
											
											//-------------added 10 feb 2015----------------------
											var d_billing_start_date=a_search_transaction_result.getValue(columns[22]);
											//nlapiLogExecution('DEBUG','d_billing_start_date',d_billing_start_date);
											
											var d_billing_end_date=a_search_transaction_result.getValue(columns[23]);
											//nlapiLogExecution('DEBUG','d_billing_end_date',d_billing_end_date);
											
											var i_billed_days=a_search_transaction_result.getValue(columns[24]);
											//nlapiLogExecution('DEBUG','i_billed_days',i_billed_days);
											
											var i_approved_days=a_search_transaction_result.getValue(columns[25]);
											//nlapiLogExecution('DEBUG','i_approved_days',i_approved_days);
											
											var i_not_approved_days=a_search_transaction_result.getValue(columns[26]);
											//nlapiLogExecution('DEBUG','i_not_approved_days',i_not_approved_days);
											
											var i_leave_days=a_search_transaction_result.getValue(columns[27]);
											//nlapiLogExecution('DEBUG','i_leave_days',i_leave_days);
											
											var i_holidays_days=a_search_transaction_result.getValue(columns[28]);
											//nlapiLogExecution('DEBUG','i_holidays_days',i_holidays_days);
											
											var i_floating_holidays_days=a_search_transaction_result.getValue(columns[29]);
											//nlapiLogExecution('DEBUG','i_floating_holidays_days',i_floating_holidays_days);
											
											var i_not_submitted_days=a_search_transaction_result.getValue(columns[30]);
											//nlapiLogExecution('DEBUG','i_not_submitted_days',i_not_submitted_days);
											
											var i_not_submitted_hour=a_search_transaction_result.getValue(columns[31]);
											//nlapiLogExecution('DEBUG','i_not_submitted_hour',i_not_submitted_hour);
											
											var i_not_submitted_holiday=a_search_transaction_result.getValue(columns[32]);
											//nlapiLogExecution('DEBUG','i_not_submitted_holiday',i_not_submitted_holiday);
							
							                
							   
										//----------------------------------------------------
											
											if(!_logValidation(i_currency))
											{
												i_currency = ''
											}
											if(!_logValidation(i_practice))
											{
												i_practice = ''
											}
											if(!_logValidation(i_vertical))
											{
												i_vertical = ''
											}			
											if(!_logValidation(i_rate))
											{
												i_rate = 0
											}
											if(!_logValidation(i_hours))
											{
												i_hours = 0
											}
												if(!_logValidation(i_employee))
											{
												i_employee = ''
											}	
											if(!_logValidation(i_project))
											{
												i_project = ''
											}	
											if(!_logValidation(i_date))
											{
												i_date = ''
											}	
											if(!_logValidation(i_amount))
											{
												i_amount = ''
											}	
											if(!_logValidation(i_customer))
											{
												i_customer = ''
											}	
											if(!_logValidation(i_exchange_rate))
											{
												i_exchange_rate = ''
											}	
											if(!_logValidation(i_subsidiary))
											{
												i_subsidiary = ''
											}
											if(!_logValidation(i_location))
											{
												i_location = ''
											}
											if(!_logValidation(d_billing_start_date))
											{
												d_billing_start_date = ''
											}
											if(!_logValidation(d_billing_end_date))
											{
												d_billing_end_date = ''
											}
											if(!_logValidation(i_billed_days))
											{
												i_billed_days = ''
											}
											if(!_logValidation(i_approved_days))
											{
												i_approved_days = ''
											}
											if(!_logValidation(i_not_approved_days))
											{
												i_not_approved_days = ''
											}
											if(!_logValidation(i_leave_days))
											{
												i_leave_days = ''
											}
											if(!_logValidation(i_holidays_days))
											{
												i_holidays_days = ''
											}
											if(!_logValidation(i_floating_holidays_days))
											{
												i_floating_holidays_days = ''
											}
											if(!_logValidation(i_not_submitted_holiday))
											{
												i_not_submitted_holiday = ''
											}
											

											var i_amount = parseFloat(i_rate)*parseFloat(i_hours);
											i_amount = parseFloat(i_amount)
											i_amount = i_amount.toFixed(2);
									
									    //---------------------------------------------------------------------------------
										if(i_Criteria == 3)
										{
										   xmlString += '<Row>' + 
											'<Cell><Data ss:Type="String">'+rec_id+'</Data></Cell>' + 
											'<Cell><Data ss:Type="String">'+(c+1)+'</Data></Cell>' + 
											'<Cell><Data ss:Type="String">'+i_employee+'</Data></Cell>' + 
											'<Cell><Data ss:Type="String">'+i_project+'</Data></Cell>' + 
											'<Cell><Data ss:Type="String">'+i_vertical+'</Data></Cell>' + 
											'<Cell><Data ss:Type="String">'+i_practice+'</Data></Cell>' + 
											'<Cell><Data ss:Type="String">'+i_date1+'</Data></Cell>' + 
											'<Cell><Data ss:Type="String">'+i_hours+'</Data></Cell>' + 
											'<Cell><Data ss:Type="String">'+i_rate+'</Data></Cell>' + 
											'<Cell><Data ss:Type="String">'+i_amount+'</Data></Cell>' + 
											'<Cell><Data ss:Type="String">'+i_currency+'</Data></Cell>' + 
											'<Cell><Data ss:Type="String">'+i_exchange_rate+'</Data></Cell>' + 
											'<Cell><Data ss:Type="String">'+s_Subsi_Name+'</Data></Cell>' + 
											'<Cell><Data ss:Type="String">'+i_customer+'</Data></Cell>' + 
											'<Cell><Data ss:Type="String">'+i_location+'</Data></Cell>' + 
											'<Cell><Data ss:Type="String">'+d_billing_start_date+'</Data></Cell>' + 
											'<Cell><Data ss:Type="String">'+d_billing_end_date+'</Data></Cell>' + 
											'<Cell><Data ss:Type="String">'+i_billed_days+'</Data></Cell>' + 
											'<Cell><Data ss:Type="String">'+i_approved_days+'</Data></Cell>' + 
											'<Cell><Data ss:Type="String">'+i_not_approved_days+'</Data></Cell>' + 
											'<Cell><Data ss:Type="String">'+i_leave_days+'</Data></Cell>' + 
											'<Cell><Data ss:Type="String">'+i_holidays_days+'</Data></Cell>' +
											'<Cell><Data ss:Type="String">'+i_floating_holidays_days+'</Data></Cell>' +
											
											'<Cell><Data ss:Type="String">'+i_not_submitted_days+'</Data></Cell>' +
											'<Cell><Data ss:Type="String">'+i_not_submitted_hour+'</Data></Cell>' +
											'<Cell><Data ss:Type="String">'+i_not_submitted_holiday+'</Data></Cell>' +
											'</Row>';
										
										}
									   else
										{
											xmlString += '<Row>' + 
											'<Cell><Data ss:Type="String">'+rec_id+'</Data></Cell>' + 
											'<Cell><Data ss:Type="String">'+(c+1)+'</Data></Cell>' + 
											'<Cell><Data ss:Type="String">'+i_employee+'</Data></Cell>' + 
											'<Cell><Data ss:Type="String">'+i_project+'</Data></Cell>' + 
											'<Cell><Data ss:Type="String">'+i_vertical+'</Data></Cell>' + 
											'<Cell><Data ss:Type="String">'+i_practice+'</Data></Cell>' + 
											'<Cell><Data ss:Type="String">'+i_date1+'</Data></Cell>' + 
											'<Cell><Data ss:Type="String">'+i_hours+'</Data></Cell>' + 
											'<Cell><Data ss:Type="String">'+i_rate+'</Data></Cell>' + 
											'<Cell><Data ss:Type="String">'+i_amount+'</Data></Cell>' + 
											'<Cell><Data ss:Type="String">'+i_currency+'</Data></Cell>' + 
											'<Cell><Data ss:Type="String">'+i_exchange_rate+'</Data></Cell>' + 
											'<Cell><Data ss:Type="String">'+s_Subsi_Name+'</Data></Cell>' + 
											'<Cell><Data ss:Type="String">'+i_customer+'</Data></Cell>' + 
											'<Cell><Data ss:Type="String">'+i_location+'</Data></Cell>' + 
											'</Row>';
										}
										//-------------------------------------------------------------------------------------
									}//for close
										

					        xmlString += '</Table></Worksheet></Workbook>';

	//create file
	 var Datetime = new Date();
	  var pdfName = 'Monthly_Provision_Time -' + Datetime+'.xls';
	  
	var xlsFile = nlapiCreateFile(pdfName, 'EXCEL', nlapiEncrypt(xmlString, 'base64'));

	xlsFile.setFolder(5570);

	//save file 
	var fileID = nlapiSubmitFile(xlsFile);
    nlapiLogExecution('DEBUG','fileID',fileID);
	  var fileobj=nlapiLoadFile(fileID); 
	  
	 
	   response.setContentType('WORD', pdfName,'inline');
	    response.write(fileobj.getValue());

}//function close
//---------------------------------------------------------------------------------------------------------------
function Generate_Excel_Expense(i_search_results,o_Subsidairy)
{//function start 
      
							var s_Subsi_Name;
							var i_ent = 0;
                       
					 /*  if(_logValidation(o_Subsidairy))
					   {
							o_Load_Subsidairy=nlapiLoadRecord('subsidiary',o_Subsidairy);
							s_Subsi_Name=o_Load_Subsidairy.getFieldValue('name');
							 nlapiLogExecution('DEBUG','s_Subsi_Name',s_Subsi_Name);
					   }
					   {
							s_Subsi_Name='';
					   }*/
					   
					   if(o_Subsidairy == '2')
					   {
					 //  llc
					    s_Subsi_Name='Brillio Inc : Brillio LLC';
					   }
					   else
					   {
					   
					     s_Subsi_Name='Brillio Inc : Brillio LLC : Brillio Technologies Private Limited';
					   }
					//----------------------------------------------------------------------------
	                    var xmlString = '<?xml version="1.0"?><?mso-application progid="Excel.Sheet"?>'; 
						xmlString += '<Workbook xmlns="urn:schemas-microsoft-com:office:spreadsheet" ';
						xmlString += 'xmlns:o="urn:schemas-microsoft-com:office:office" ';
						xmlString += 'xmlns:x="urn:schemas-microsoft-com:office:excel" ';
						xmlString += 'xmlns:ss="urn:schemas-microsoft-com:office:spreadsheet" ';
						xmlString += 'xmlns:html="http://www.w3.org/TR/REC-html40">'; 

							xmlString += '<Worksheet ss:Name="Sheet1">';
							xmlString += '<Table>' + 
									   '<Row>' +
										'<Cell><Data ss:Type="String"> Record Id </Data></Cell>' +
										'<Cell><Data ss:Type="String"> S No </Data></Cell>' +
										'<Cell><Data ss:Type="String"> Employee </Data></Cell>' +
										'<Cell><Data ss:Type="String"> Project </Data></Cell>' +
										'<Cell><Data ss:Type="String"> Vertical </Data></Cell>' +
										'<Cell><Data ss:Type="String"> Practice </Data></Cell>' +
										'<Cell><Data ss:Type="String"> Date </Data></Cell>' +
										
										'<Cell><Data ss:Type="String"> Amount </Data></Cell>' +
										'<Cell><Data ss:Type="String"> Currency </Data></Cell>' +
										
										'<Cell><Data ss:Type="String"> Subsidiary </Data></Cell>' +
										'<Cell><Data ss:Type="String"> Customer </Data></Cell>' +
										'<Cell><Data ss:Type="String"> Location </Data></Cell>' +
										'</Row>';

										
									for(var c=0;c<i_search_results.length;c++)	//for(var c=(rowCount - rowsPerPage);c<rowCount;c++)//for(var c=0;c<100;c++)	
									{ //for start
									
									       //---------------------------get all value from search----------------------
										   
											var a_search_transaction_result = i_search_results[c];
			        
					
											if(a_search_transaction_result ==null || a_search_transaction_result == '' || a_search_transaction_result == undefined)
											{
												break;
											}	
												var columns = a_search_transaction_result.getAllColumns();	
												var i_subsidiary_1=parseInt(o_Subsidairy);
												
											  var i_employee=a_search_transaction_result.getValue(columns[16]);	
											   
											   var i_practice=a_search_transaction_result.getText(columns[11]);
											   
											   var i_vertical=a_search_transaction_result.getText(columns[12]);
											   
											   var i_projectID=a_search_transaction_result.getValue(columns[9]);
											   
											   var i_currency=a_search_transaction_result.getText(columns[4]);
											   
											    var i_location=a_search_transaction_result.getText(columns[10]);
												
												var i_amount=a_search_transaction_result.getValue(columns[3]); 
												
												var i_customer=a_search_transaction_result.getText(columns[14]); 
				   
												//  var i_currency=a_search_transaction_result.getValue(columns[12]);
				   
												var columnLen = columns.length;
											
			      
											for(var hg= 0 ;hg<columnLen ; hg++ )
											{
												var column = columns[hg];
												var label = column.getLabel();
												var value = a_search_transaction_result.getValue(column)
												var text = a_search_transaction_result.getText(column)
													
											 if(label =='Internal ID')
											 {
												var i_internal_ID = value;
											 }
											 if(label =='Date')
											 {
												var i_date = value;
											 }
											 if(label =='Amount(Debit)')
											 {
												//var i_amount = value;
											 }
											 if(label =='Currency')
											 {
												//var i_currency = value;						
											 }
											 if(label =='Employee')
											 {
												//var i_employee = value;
											 }		
											 if(label =='Project ID')
											 {
												//var i_projectID = value;
											 }
											 if(label =='Project Name')
											 {
												i_project_name = value;
											 }
											 if(label =='Location')
											 {
												//i_location = value;
											 }
							
							 
											}//Column Loop				
										//nlapiLogExecution('DEBUG', 'suiteletFunction',' Expense ID  -->' + i_internal_ID);	
					
										var is_invoice_created = get_invoice_created_details(i_internal_ID);
										//nlapiLogExecution('DEBUG', 'suiteletFunction',' Is Invoice Created  -->' + is_invoice_created);	
					 
										if(is_invoice_created)
										{
										}
									else{
										
										i_ent++;					
										nlapiLogExecution('DEBUG', 'inside third if');	
																
										if(!_logValidation(i_practice))
										{
											i_practice = ''
										}
										if(!_logValidation(i_vertical))
										{
											i_vertical = ''
										}
										if(!_logValidation(i_employee))
										{
											i_employee = ''
										}	
										if(!_logValidation(i_projectID))
										{
											i_projectID = ''
										}	
										if(!_logValidation(i_date))
										{
											i_date = ''
										}	
										if(!_logValidation(i_amount))
										{
											i_amount = ''
										}	
										if(!_logValidation(i_currency))
										{
											i_currency = ''
										}	
										
										if(!_logValidation(i_subsidiary_1))
										{
											i_subsidiary_1 = ''
										}	
										 if(label =='Internal ID')
											 {
												var i_internal_ID = value;
											 }
										//---------------------------------------------------------------------------------
											xmlString += '<Row>' + 
											'<Cell><Data ss:Type="String">'+i_internal_ID+'</Data></Cell>' + 
											'<Cell><Data ss:Type="String">'+(c+1)+'</Data></Cell>' + 
											'<Cell><Data ss:Type="String">'+i_employee+'</Data></Cell>' + 
											'<Cell><Data ss:Type="String">'+i_projectID+'</Data></Cell>' + 
											'<Cell><Data ss:Type="String">'+i_vertical+'</Data></Cell>' + 
											'<Cell><Data ss:Type="String">'+i_practice+'</Data></Cell>' + 
											'<Cell><Data ss:Type="String">'+i_date+'</Data></Cell>' + 
											'<Cell><Data ss:Type="String">'+i_amount+'</Data></Cell>' + 
											'<Cell><Data ss:Type="String">'+i_currency+'</Data></Cell>' + 
											'<Cell><Data ss:Type="String">'+s_Subsi_Name+'</Data></Cell>' + 
												'<Cell><Data ss:Type="String">'+i_customer+'</Data></Cell>' + 
											'<Cell><Data ss:Type="String">'+i_location+'</Data></Cell>' + 
											'</Row>';
													
										//-------------------------------------------------------------------------------------
									}
									}//for close
										

					        xmlString += '</Table></Worksheet></Workbook>';

	//create file
	 var Datetime = new Date();
	  var pdfName = 'Monthly_Provision_Expense -' + Datetime+'.xls';
	  
	var xlsFile = nlapiCreateFile(pdfName, 'EXCEL', nlapiEncrypt(xmlString, 'base64'));

	xlsFile.setFolder(3549);

	//save file 
	var fileID = nlapiSubmitFile(xlsFile);
    nlapiLogExecution('DEBUG','fileID',fileID);
	  var fileobj=nlapiLoadFile(fileID); 
	  
	 
	   response.setContentType('WORD', pdfName,'inline');
	    response.write(fileobj.getValue());

}//function close

//-------------------------------------------------------------------------------
function _logValidation(value) 
{
 if(value != null && value.toString() != null && value != '' && value != undefined && value.toString() != undefined && value != 'undefined' && value.toString() != 'undefined'&& value.toString() != 'NaN' && value != NaN) 
 {
  return true;
 }
 else 
 { 
  return false;
 }
}



// BEGIN OBJECT CALLED/INVOKING FUNCTION ===================================================
{

function get_invoice_created_details(i_expenseID)
{
	var i_invoice_created = false;
	if(_logValidation(i_expenseID))
	{
		var o_expenseOBJ = nlapiLoadRecord('expensereport',i_expenseID)
		
		if(_logValidation(o_expenseOBJ))
		{
			var i_reimbursements_count = o_expenseOBJ.getLineItemCount('reimbursements');
			nlapiLogExecution('DEBUG', 'get_invoice_created_details', 'Reimbursements Count --> ' + i_reimbursements_count);
	 
			if(_logValidation(i_reimbursements_count))
			{
				for(var i = 1;i<=i_reimbursements_count;i++)
				{
					var s_linkurl = o_expenseOBJ.getLineItemValue('reimbursements','linkurl',i)
					nlapiLogExecution('DEBUG', 'get_invoice_created_details', 'Link URL --> ' + s_linkurl);
	     
					if(s_linkurl.indexOf('custinvc')>-1)
					{
						nlapiLogExecution('DEBUG', 'get_invoice_created_details', 'Invoice has been created .....');
						i_invoice_created = true;
						break;
	     
					}//Invoice Created					
				}//Loop				
			}//Reimbursements Count			
		}//Expense OBJ		
	}//Expense ID	
	return i_invoice_created;
}//Invoice Created Details

}
// END OBJECT CALLED/INVOKING FUNCTION =====================================================