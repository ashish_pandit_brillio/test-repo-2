/**
 *@NApiVersion 2.x
 *@NScriptType Restlet
 */
define(['N/record', 'N/search', 'N/format'], function (record, search, format) {
    function _post(datain) {
        try {
            log.debug({
                title: 'datain.length',
                details: datain.length
            });
            var data = [];
            for (var i = 0; i < datain.length; i++) {
                // mode: cancel
                // frfInternalid
                // loggedUser:
                // "cancelReason":
                // "cancelDate":
                var datainObj = datain[i];
                log.debug('datainObj Creation', datainObj);
                frfId = datainObj.frfInternalid;
                user = datainObj.loggedUser;
                var cancelReason = datainObj.cancelReason;
                var cancelReasonId = getReasonId(cancelReason)
                var d = new Date();
                log.debug({
                    title: 'd',
                    details: d
                })
                var formatDate = format.format({
                    value: d,
                    type: format.Type.DATE
                })
                log.debug({
                    title: 'formatDate',
                    details: formatDate
                })
                if (frfId) {
                    var employeeSearch = getEmployeeRec(user);
                    record.submitFields({
                        type: "customrecord_frf_details",
                        id: frfId,
                        values: {
                            'custrecord_frf_details_status': true,
                            "custrecord_frf_details_cancelled_date": formatDate,
                            "custrecord_frf_details_cancellation_reas": cancelReasonId,
                            "custrecord_frf_details_status_flag": 4,
                            "custrecord_frf_details_open_close_status": 4,
                            "custrecord_frf_details_rrf_cancelled_by": employeeSearch
                        }
                    })
                    data.push(frfId);
                }
                else {
                    return {
                        "code": "frf doesnot exist",
                        "message": "unsuccessful"
                    }
                }
            }
            return {
                "code": "frf has been processed",
                "message": data
            }
        } catch (error) {
            log.error({
                title: error.name,
                details: error.message
            })
        }
    }
    function getEmployeeRec(user) {
        var employeeSearchObj = search.create({
            type: "employee",
            filters:
                [
                    ["email", "is", user]
                ],
            columns:
                [

                ]
        });
        var searchResultCount = employeeSearchObj.runPaged().count;
        var empId;
        log.debug("employeeSearchObj result count", searchResultCount);
        employeeSearchObj.run().each(function (result) {
            empId = result.id;
            return true;
        });
        return empId
    }
    function getReasonId(cancelReason){
        try {
            var customlist_cancellation_reason_listSearchObj = search.create({
                type: "customlist_cancellation_reason_list",
                filters:
                [
                   ["name","is",cancelReason]
                ],
                columns:
                [
                   search.createColumn({name: "internalid", label: "Internal ID"})
                ]
             });
             var searchResultCount = customlist_cancellation_reason_listSearchObj.runPaged().count;
             log.debug("customlist_cancellation_reason_listSearchObj result count",searchResultCount);
             var listId;
             customlist_cancellation_reason_listSearchObj.run().each(function(result){
                listId = result.getValue({
                    name: 'internalid'
                })
                return true;
             });
             return listId;
        } catch (error) {
            log.error({
                title: error.name,
                details: error.message
            })
        }
    }
    return {
        post: _post
    }
});

