/**
 * Module Description
 * 
 * Version Date Author Remarks 1.00 Apr 01 2015 Nitish Mishra
 * 
 */

/**
 * @param {String}
 *        recType Record type internal id
 * @param {Number}
 *        recId Record internal id
 * @returns {Void}
 */
function massUpdate(recType, recId) {

	var record = nlapiLoadRecord(recType, recId);
	var lineItemCount = record.getLineItemCount('line');

	for (var linenum = 1; linenum <= lineItemCount; linenum++) {
		var projectId = record.getLineItemValue('line', 'custcol_sow_project', linenum);

		if (projectId) {
			var projectRecord = nlapiLoadRecord('job', projectId);
			var projectName = projectRecord.getFieldValue('altname');
			var id = projectRecord.getFieldValue('entityid');
			var customerName = nlapiLookupField('job', projectId, 'customer', true);
			var projectDesc = id + ' ' + projectName;

			record.setLineItemValue('line', 'custcol_project_name', linenum, id);
			record.setLineItemValue('line', 'custcolprj_name', linenum, projectDesc);

			if (customerName) {
				record.setLineItemValue('line', 'custcolcustcol_temp_customer', linenum,
						customerName);
			}

			record.commitLineItem('line');
		}
	}

	nlapiSubmitRecord(record);
}

function scheduled() {

	var recType = 'journalentry';
	var recId = null;
	var records = [
		73364, 73464, 72802, 72903, 72905
	];
	var context = nlapiGetContext();

	for (var i = 0; i < records.length; i++) {
		yieldScript(context);
		recId = records[i];
		var record = nlapiLoadRecord(recType, recId);
		var lineItemCount = record.getLineItemCount('line');

		for (var linenum = 1; linenum <= lineItemCount; linenum++) {

			var projectId = record.getLineItemValue('line', 'custcol_sow_project', linenum);

			if (projectId) {
				yieldScript(context);
				var projectRecord = nlapiLoadRecord('job', projectId);
				var projectName = projectRecord.getFieldValue('altname');
				var id = projectRecord.getFieldValue('entityid');
				var customerName = nlapiLookupField('job', projectId, 'customer', true);
				var projectDesc = id + ' ' + projectName;

				record.setLineItemValue('line', 'custcol_project_name', linenum, id);
				record.setLineItemValue('line', 'custcolprj_name', linenum, projectDesc);

				if (customerName) {
					record.setLineItemValue('line', 'custcolcustcol_temp_customer', linenum,
							customerName);
				}

				record.commitLineItem('line');
			}
		}

		yieldScript(context);
		nlapiSubmitRecord(record);
		nlapiLogExecution('debug', 'done', recId);
	}
}

function yieldScript(currentContext) {

	if (currentContext.getRemainingUsage() <= 100) {
		nlapiLogExecution('AUDIT', 'API Limit Exceeded');
		var state = nlapiYieldScript();

		if (state.status == "FAILURE") {
			nlapiLogExecution('ERROR', 'Yield Script Failed', 'Reason : ' + state.reason
					+ ' / Size : ' + state.size);
			return false;
		}
		else if (state.status == "RESUME") {
			nlapiLogExecution('AUDIT', 'Script Resumed');
		}
	}
}
