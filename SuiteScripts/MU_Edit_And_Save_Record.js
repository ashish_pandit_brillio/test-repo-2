/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       28 Apr 2016     amol.sahijwani
 *
 */

/**
 * @param {String} recType Record type internal id
 * @param {Number} recId Record internal id
 * @returns {Void}
 */
function massUpdate(recType, recId) {
	var rec	=	nlapiLoadRecord(recType, recId);
	try
	{
		nlapiSubmitRecord(rec);
		
		nlapiLogExecution('AUDIT', recType + ' ' + recId + ' Updated');
	}
	catch(e)
	{
		nlapiLogExecution('ERROR', 'Error in ' + recType + ': ' + recId, e.message);
	}
}
