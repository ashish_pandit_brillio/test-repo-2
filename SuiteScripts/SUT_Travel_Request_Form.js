/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       04 Aug 2015     amol.sahijwani
 *
 */

/**
 * @param {nlobjRequest} request Request object
 * @param {nlobjResponse} response Response object
 * @returns {Void} Any output is written via response object
 */
function suitelet(request, response){
	
	var method	=	request.getMethod();
	
	var strHtml	=	'<html><form></form></html>';
	
	if(method == 'GET')
		{
		var file = nlapiLoadFile(26307); //load the HTML file
		  var contents = file.getValue(); //get the contents
		  
		  // Get logged-in user
		  var objUser = nlapiGetContext();
		  
		  var i_employee_id = objUser.getUser();
		  
		  var a_project_data = getProjects(i_employee_id);
		  
		  var strProjectOptions = getOptions(a_project_data);
		  
		  contents = contents.replace(/{{username}}/gi, objUser.getName());
		  contents = contents.replace(/{{project_options}}/gi, strProjectOptions);
		  contents = contents.replace(/{{country_list}}/gi, getListValues('customlist_country_list'));
		  contents = contents.replace(/{{domestic_international}}/gi, getCheckBoxes('customlist_tr_travel_type', 'travel_type'));
		  contents = contents.replace(/{{request_type}}/gi, getListValues('customlist_tr_request_type'));
		  contents = contents.replace(/{{purpose_of_travel}}/gi, getListValues('customlist_tr_travel_purpose'));
		  
		  response.write(contents); //render it on the suitelet
		}
	else
		{
			var o_travel = new Object();
			o_travel['i_domestic_country'] = request.getParameter('domestic_country');
			o_travel['i_project'] = request.getParameter('project');
			o_travel['s_travel_description'] = request.getParameter('travel_description');
			o_travel['i_request_type']	=	request.getParameter('request_type');
			o_travel['i_purpose_of_travel'] = request.getParameter('purpose_of_travel');
			o_travel['i_domestic_international'] = request.getParameter('travel_type');
			
			var o_status = saveTravelRequest(o_travel);
			
			if(o_status.status == 1)
				{
					var i_rec_id = o_status.tr_id;
					var message = o_status.message;var s_url = nlapiResolveURL('SUITELET', 'customscript_sut_travel_req_details_form', 'customdeploy1');
					nlapiRequestURL(request.getHeader('Host') + '/' + s_url);
				}
			
			for ( param in params )
			{
				response.write('parameter: '+ param+ '<br />');
				response.write('value: '+params[param]+'<br />');
			} 
		
			var a_parameters = request.getAllParameters();
			
			response.write(JSON.stringify(a_parameters));
		}
}

function saveTravelRequest(o_travel)
{
	try
	{
		// Get logged-in user
		var objUser = nlapiGetContext();
		  
		var i_employee_id = objUser.getUser();
		
		var recTR	=	nlapiCreateRecord('customrecord_travel_request');
		
		recTR.setFieldValue('custrecord_tr_travel_type', o_travel.i_domestic_international);
		recTR.setFieldValue('custrecord_tr_country', o_travel.i_domestic_country);
		recTR.setFieldValue('custrecord_tr_request_type', o_travel.i_request_type);
		recTR.setFieldValue('custrecord_tr_employee', i_employee_id);
		recTR.setFieldValue('custrecord_tr_project', o_travel.i_project);
		recTR.setFieldValue('custrecord_tr_travel_purpose', o_travel.s_travel_description);
		
		var i_rec_id = nlapiSubmitRecord(recTR, true);	
		
		return ({'status': 1, 'message': 'TR Request Saved', 'tr_id': i_rec_id});
	}
	catch(e)
	{
		return ({'status': -1, 'message': 'Error: ' + e.message, 'tr_id': null});
	}
}
function getOptions(a_data)
{
	var strOptions = '';

	for(var i = 0; i < a_data.length; i++)
	{
		strOptions += '<option value = "'+a_data[i].value+'">' + a_data[i].display + '</option>';
	}

	return strOptions;
}
function getCheckBoxOptions(a_data, s_control_name)
{
	var strOptions = '';

	for(var i = 0; i < a_data.length; i++)
	{
		strOptions += '<label class="radio-inline"><input type="radio" id="' + s_control_name + '" name="' + s_control_name + '" value = "'+a_data[i].value+'">' + a_data[i].display + '</label>';
	}

	return strOptions;
}
function getProjects(i_employee_id)
{
	var columns = new Array();
	columns[0] = new nlobjSearchColumn('company');
	
	var resource_allocations = nlapiSearchRecord('resourceallocation', null, [new nlobjSearchFilter('resource', null, 'anyof', i_employee_id)], columns);
	
	var project_list = new Array();
	
	for(var i = 0; resource_allocations != null && i < resource_allocations.length; i++)
		{
			project_list.push({'display':resource_allocations[i].getText(columns[0]), 'value':resource_allocations[i].getValue(columns[0])});
		}
	
	return project_list;
}

function getListValues(s_list_name)
{
	var columns = new Array();
	columns[0]	=	new nlobjSearchColumn('name');
	
	var a_search_results = searchRecord(s_list_name, null, null, columns);
	
	var list_values = new Array();
	
	for(var i = 0; a_search_results != null && i < a_search_results.length; i++)
		{
			list_values.push({'display':a_search_results[i].getValue('name'), 'value':a_search_results[i].id});
		}
	
	return getOptions(list_values);
}

function getCheckBoxes(s_list_name, s_control_name)
{
	var columns = new Array();
	columns[0]	=	new nlobjSearchColumn('name');
	
	var a_search_results = searchRecord(s_list_name, null, null, columns);
	
	var list_values = new Array();
	
	for(var i = 0; a_search_results != null && i < a_search_results.length; i++)
		{
			list_values.push({'display':a_search_results[i].getValue('name'), 'value':a_search_results[i].id});
		}
	
	return getCheckBoxOptions(list_values, s_control_name);
}