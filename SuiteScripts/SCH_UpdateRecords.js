
//BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
	/*
	     Script Name	: SCH_CreateFrfDetails.js
	     Author			: Ashish Pandit
	     Company		: Inspirria CloudTech Pvt Ltd
	     Date			: 17 JAN 2019
	     Details        : Scheduled Script to create FRF Details custom records 

		 Script Modification Log:

		 -- Date --               -- Modified By --                  --Requested By--                   -- Description --


		 Below is a summary of the process controls enforced by this script file.  The control logic is described
	     more fully, below, in the appropriate function headers and code blocks.
	     SCHEDULED FUNCTION
	     - RFQbody_SCH_main()
	     SUB-FUNCTIONS
	     - The following sub-functions are called by the above core functions in order to maintain code
	     modularization:
	     - NOT USED
	 */
}

//END SCRIPT DESCRIPTION BLOCK  ====================================

function scheduled_updateRecords() 
{
	nlapiLogExecution('Debug','in script');
	try
	{
		var customrecord_frf_detailsSearch = nlapiSearchRecord(null,'customsearch2858',
		[
		], 
		[
		   new nlobjSearchColumn("internalid")
		]
		);
		if(customrecord_frf_detailsSearch)
		{
			//for(var i=0; i<customrecord_frf_detailsSearch.length;i++)
			nlapiLogExecution('Debug','customrecord_frf_detailsSearch length ',customrecord_frf_detailsSearch.length);
			for(var i=0; i<customrecord_frf_detailsSearch.length;i++)
			{
				var temp = customrecord_frf_detailsSearch[i].getId();
				nlapiLogExecution('Debug','temp ',temp);
				//var recObj = nlapiSubmitField('custrecord_taleo_ext_hire_frf_details','');
				var taleoId = nlapiDeleteRecord('customrecord_taleo_external_hire',temp);
				nlapiLogExecution('Debug','taleoId ',taleoId);
			}
		}
	}
	catch(e)
	{
		
	}
}

//========================================================================
function _logValidation(value) 
{
	if(value!='null' && value != null && value != null && value != '' && value != undefined && value != undefined && value != 'undefined' && value!= 'undefined'&& value!= 'NaN' && value != NaN) 
	{
		return true;
	}
	else 
	{ 
		return false;
	}
}

function getFRFNumber(recId) {
	var str = "" + recId;
	var pad = "00000000";
	var s_number = pad.substring(0, pad.length - str.length) + str;
	return "F"+s_number;
}

function createTaleoExternalHireRecord(i_osfRec){
	var taleoExternalHireRec = nlapiCreateRecord('customrecord_taleo_external_hire');
	taleoExternalHireRec.setFieldValue('custrecord_taleo_ext_hire_frf_details',i_osfRec);
	var talRec = nlapiSubmitRecord(taleoExternalHireRec);
	nlapiLogExecution('debug','talRec ',talRec);
	return talRec;
}