/**
 * @author Shweta
 */
// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
/*
   	Script Name: UES_QA_Preferred_Vendor.js
	Author     : Shweta Chopde
	Company    : Aashna Cloudtech
	Date       : 11 April 2014
	Description: Set the prefered Vendor on basis on AMount


	Script Modification Log:

	-- Date --			-- Modified By --				--Requested By--				-- Description --
    8 July 2014           Shweta Chopde                     Shekar                      1. Added logic for Email Reminder to Practice Head


Below is a summary of the process controls enforced by this script file.  The control logic is described
more fully, below, in the appropriate function headers and code blocks.


     BEFORE LOAD
		- beforeLoadRecord(type)



     BEFORE SUBMIT
		- beforeSubmitRecord(type)


     AFTER SUBMIT
		- afterSubmitRecord(type)



     SUB-FUNCTIONS
		- The following sub-functions are called by the above core functions in order to maintain code
            modularization:

               - NOT USED

*/
}
// END SCRIPT DESCRIPTION BLOCK  ====================================



// BEGIN GLOBAL VARIABLE BLOCK  =====================================
{
	//  Initialize any Global Variables, in particular, debugging variables...




}
// END GLOBAL VARIABLE BLOCK  =======================================





// BEGIN BEFORE LOAD ==================================================

function beforeLoad_create_button(type)
{
    var i_recordID = nlapiGetRecordId();
	
	var i_current_user = nlapiGetUser();
	
	form.setScript('customscript_cli_create_po')
	
		var i_recordID = nlapiGetRecordId()
		var s_record_type = nlapiGetRecordType()
		
		if(_logValidation(i_recordID)&& _logValidation(s_record_type))
		{
		 var o_recordOBJ = nlapiLoadRecord(s_record_type,i_recordID)
		 nlapiLogExecution('DEBUG', 'beforeLoad_create_button',' Record Object -->' + o_recordOBJ);	
		 
		 
		  
		if (_logValidation(o_recordOBJ)) 
		{
			var i_line_count_QA = o_recordOBJ.getLineItemCount('recmachcustrecord_purchaserequest');
			//nlapiLogExecution('DEBUG', 'beforeLoad_create_button',' Line Count QA -->' + i_line_count_QA);	
		 
			  if (_logValidation(i_line_count_QA)) 
			  {
			  	for (var a = 1; a <= i_line_count_QA; a++) 
				{
					var i_approval_status = o_recordOBJ.getLineItemValue('recmachcustrecord_purchaserequest','custrecord_prastatus',a)			    	
					if(i_approval_status == 27 && type == 'edit')
					{	
							form.addButton('custpage_create_PO','Create  PO','create_PO('+i_recordID+')');
						    break;
					}					
			  	}
			  }
			  
			  
			  var i_line_count_PR = o_recordOBJ.getLineItemCount('recmachcustrecord_purchaserequest')
			  var b_status = 0 ;
			  
			  if (_logValidation(i_line_count_PR)) 
			  {
			  
	              nlapiLogExecution('DEBUG', 'beforeLoad_create_button',' Line Count PR -->' + i_line_count_PR);	
					
			  	for(var kt=1;kt<=i_line_count_PR;kt++)
				{
					var i_pr_status = o_recordOBJ.getLineItemValue('recmachcustrecord_purchaserequest','custrecord_prastatus',kt)
					nlapiLogExecution('DEBUG', 'beforeLoad_create_button',' PR Status -->' + i_pr_status);	
					
					if(i_pr_status != 7)
					{
						form.setScript('customscript_cli_pr_cancellation')	 
					    form.addButton('custpage_cancelled','Cancel PR','cancel_PR('+i_recordID+')');
						break;
					}//PR Status 
					
				}//Loop

				
			//	if(b_status != 1)
				{
					
										
				}//Status
				
			  }//i_line_count_PR
			  
			  
			
			
		}//Record OBJ	
			
		}//Record ID & Record Type
	
		
	

	return true;



}

// END BEFORE LOAD ====================================================





// BEGIN BEFORE SUBMIT ================================================

function beforeSubmitRecord(type)
{
    /*  On before submit:

          - PURPOSE
		-

          FIELDS USED:

          --Field Name--				--ID--


    */


    //  LOCAL VARIABLES


    //  BEFORE SUBMIT CODE BODY


	return true;

}

// END BEFORE SUBMIT ==================================================





// BEGIN AFTER SUBMIT =============================================

function afterSubmit_preferred_vendor(type)
{
  var i_preferred_vendor = '';	
 //  if(type == 'create')
   {
   	try
	{
		var i_recordID = nlapiGetRecordId()
		var s_record_type = nlapiGetRecordType()
		var i_user = nlapiGetUser();
		
		var o_recordOBJ = nlapiLoadRecord(s_record_type,i_recordID)
		nlapiLogExecution('DEBUG', 'afterSubmit_preferred_vendor',' Record Object -->' + o_recordOBJ);	
		  
		if(_logValidation(o_recordOBJ))
		{
			var i_line_count_QA = o_recordOBJ.getLineItemCount('recmachcustrecord_prquote')
			nlapiLogExecution('DEBUG', 'afterSubmit_preferred_vendor',' Line Count QA -->' + i_line_count_QA);	
		 
		    if(_logValidation(i_line_count_QA))
			{
				for(var a=1;a<=i_line_count_QA;a++)
				{
					i_preferred_vendor =''
					
					var i_SR_No_QA = o_recordOBJ.getLineItemValue('recmachcustrecord_prquote','custrecord_srno',a)
			    	nlapiLogExecution('DEBUG', 'afterSubmit_preferred_vendor',' SR No. QA-->' + i_SR_No_QA);	
										
					var i_item_QA = o_recordOBJ.getLineItemValue('recmachcustrecord_prquote','custrecord_item',a)
			    	nlapiLogExecution('DEBUG', 'afterSubmit_preferred_vendor',' Item QA-->' + i_item_QA);	
					  
				    var i_Qty = o_recordOBJ.getLineItemValue('recmachcustrecord_prquote','custrecord_qty',a)
			    	nlapiLogExecution('DEBUG', 'afterSubmit_preferred_vendor',' i_Qty-->' + i_Qty);	
					  
					var i_vendor_1 = o_recordOBJ.getLineItemValue('recmachcustrecord_prquote','custrecord_vendor1name',a)
			    	nlapiLogExecution('DEBUG', 'afterSubmit_preferred_vendor',' i_vendor_1-->' + i_vendor_1);	
					  
					var i_rate_1 = o_recordOBJ.getLineItemValue('recmachcustrecord_prquote','custrecord_rate1',a)
			    	nlapiLogExecution('DEBUG', 'afterSubmit_preferred_vendor',' i_rate_1-->' + i_rate_1);	
					
					var i_discount_1 = o_recordOBJ.getLineItemValue('recmachcustrecord_prquote','custrecord_discount',a)
			    	nlapiLogExecution('DEBUG', 'afterSubmit_preferred_vendor',' i_discount_1-->' + i_discount_1);
					
					var i_other_charges_1 = o_recordOBJ.getLineItemValue('recmachcustrecord_prquote','custrecord_otherchargesv1',a)
			    	nlapiLogExecution('DEBUG', 'afterSubmit_preferred_vendor',' i_other_charges_1-->' + i_other_charges_1);
					
					var i_vendor_2 = o_recordOBJ.getLineItemValue('recmachcustrecord_prquote','custrecord_vendor2name',a)
			    	nlapiLogExecution('DEBUG', 'afterSubmit_preferred_vendor',' i_vendor_2-->' + i_vendor_2);	
					  
					var i_rate_2 = o_recordOBJ.getLineItemValue('recmachcustrecord_prquote','custrecord_rate2',a)
			    	nlapiLogExecution('DEBUG', 'afterSubmit_preferred_vendor',' i_rate_2-->' + i_rate_2);	
					
					var i_discount_2 = o_recordOBJ.getLineItemValue('recmachcustrecord_prquote','custrecord_disocuntven2',a)
			    	nlapiLogExecution('DEBUG', 'afterSubmit_preferred_vendor',' i_discount_2-->' + i_discount_2);
					
					var i_other_charges_2 = o_recordOBJ.getLineItemValue('recmachcustrecord_prquote','custrecord_otherchargesv2',a)
			    	nlapiLogExecution('DEBUG', 'afterSubmit_preferred_vendor',' i_other_charges_2-->' + i_other_charges_2);
					
					var i_vendor_3 = o_recordOBJ.getLineItemValue('recmachcustrecord_prquote','custrecord_vendor3name',a)
			    	nlapiLogExecution('DEBUG', 'afterSubmit_preferred_vendor',' i_vendor_3-->' + i_vendor_3);	
					  
					var i_rate_3 = o_recordOBJ.getLineItemValue('recmachcustrecord_prquote','custrecord_rate3',a)
			    	nlapiLogExecution('DEBUG', 'afterSubmit_preferred_vendor',' i_rate_3-->' + i_rate_3);	
					
					var i_discount_3 = o_recordOBJ.getLineItemValue('recmachcustrecord_prquote','custrecord_discountv3',a)
			    	nlapiLogExecution('DEBUG', 'afterSubmit_preferred_vendor',' i_discount_3-->' + i_discount_3);
					
					var i_other_charges_3 = o_recordOBJ.getLineItemValue('recmachcustrecord_prquote','custrecord_otherchargesv3',a)
			    	nlapiLogExecution('DEBUG', 'afterSubmit_preferred_vendor',' i_other_charges_3-->' + i_other_charges_3);
					  
					var i_vendor_PO = o_recordOBJ.getLineItemValue('recmachcustrecord_prquote','custrecord_vendorforpo',a)
			    	nlapiLogExecution('DEBUG', 'afterSubmit_preferred_vendor',' i_vendor_PO-->' + i_vendor_PO);
					 
					i_rate_1 = validate_value(i_rate_1) 
					i_rate_2 = validate_value(i_rate_2) 
					i_rate_3 = validate_value(i_rate_3) 
					
					i_discount_1 = validate_value(i_discount_1) 
					i_discount_2 = validate_value(i_discount_2) 
					i_discount_3 = validate_value(i_discount_3) 
										
					i_other_charges_1 = validate_value(i_other_charges_1) 
					i_other_charges_2 = validate_value(i_other_charges_2) 
					i_other_charges_3 = validate_value(i_other_charges_3) 
					
					i_Qty = validate_value(i_Qty) 
					
					i_discount_1 = parseFloat(i_discount_1)
					
					i_discount_2 = parseFloat(i_discount_2)
					
					i_discount_3 = parseFloat(i_discount_3)
					  
					var f_total_amt_V1 =    parseFloat(i_rate_1)*parseFloat(i_Qty)+parseFloat(i_other_charges_1)-parseFloat(i_discount_1)
					var f_total_amt_V2	=   parseFloat(i_rate_2)*parseFloat(i_Qty)+ parseFloat(i_other_charges_2)-parseFloat(i_discount_2)
					var f_total_amt_V3 =	parseFloat(i_rate_3)*parseFloat(i_Qty)+ parseFloat(i_other_charges_3)-parseFloat(i_discount_3)
 
				    nlapiLogExecution('DEBUG', 'afterSubmit_preferred_vendor',' f_total_amt_V1-->' + f_total_amt_V1);
					nlapiLogExecution('DEBUG', 'afterSubmit_preferred_vendor',' f_total_amt_V2-->' + f_total_amt_V2);
					nlapiLogExecution('DEBUG', 'afterSubmit_preferred_vendor',' f_total_amt_V3-->' + f_total_amt_V3);
					
					var f_MIN ; 	  
				  	if(f_total_amt_V1!=0 && f_total_amt_V2!=0  && f_total_amt_V3!=0 )	  
					{
						f_MIN = Math.min(f_total_amt_V1 ,f_total_amt_V2,f_total_amt_V3)
						nlapiLogExecution('DEBUG', 'afterSubmit_preferred_vendor',' f_MIN-->' + f_MIN);	
						
						if(f_MIN == f_total_amt_V1)
						{
							i_preferred_vendor = i_vendor_1
						}
						if(f_MIN == f_total_amt_V2)
						{
							i_preferred_vendor = i_vendor_2
						}
						if(f_MIN == f_total_amt_V3)
						{
							i_preferred_vendor = i_vendor_3
						}
					}
					if(f_total_amt_V1!=0 && f_total_amt_V2!=0  && f_total_amt_V3==0 )	  
					{
						f_MIN = Math.min(f_total_amt_V1 ,f_total_amt_V2)
						nlapiLogExecution('DEBUG', 'afterSubmit_preferred_vendor',' f_MIN-->' + f_MIN);	
						
						if(f_MIN == f_total_amt_V1)
						{
							i_preferred_vendor = i_vendor_1
						}
						if(f_MIN == f_total_amt_V2)
						{
							i_preferred_vendor = i_vendor_2
						}
						
					}
					if(f_total_amt_V1!=0 && f_total_amt_V2==0  && f_total_amt_V3==0 )	  
					{
						f_MIN = Math.min(f_total_amt_V1)
						nlapiLogExecution('DEBUG', 'afterSubmit_preferred_vendor',' f_MIN-->' + f_MIN);	
						
						if(f_MIN == f_total_amt_V1)
						{
							i_preferred_vendor = i_vendor_1
						}
						
					}	  
					
					//if(f_total_amt_V1!=0 && f_total_amt_V2!=0 && f_total_amt_V3!=0)
					{
						
						
					}
					
					nlapiLogExecution('DEBUG', 'afterSubmit_preferred_vendor',' i_preferred_vendor-->' + i_preferred_vendor);
					
					/*
                    o_recordOBJ.setLineItemValue('recmachcustrecord_prquote','custrecord_amount1', a,f_total_amt_V1  )
					o_recordOBJ.setLineItemValue('recmachcustrecord_prquote','custrecord_amount2', a,f_total_amt_V2 )
					o_recordOBJ.setLineItemValue('recmachcustrecord_prquote','custrecord_amount3', a,f_total_amt_V3 )
					o_recordOBJ.setLineItemValue('recmachcustrecord_prquote','custrecord_preferred_vendor',a ,i_preferred_vendor )
					
                   */
										
					
					  var i_QA_recordID = search_quotation_analysis_recordID(i_recordID,i_SR_No_QA,i_item_QA)
				       nlapiLogExecution('DEBUG', 'afterSubmit_Item_Data_Sourcing',' QA Record ID -->' + i_QA_recordID);	
	                 
				        if (_logValidation(i_QA_recordID)) 
						{
							var o_QA_OBJ = nlapiLoadRecord('customrecord_quotationanalysis', i_QA_recordID)
						
						 if (_logValidation(o_QA_OBJ))
						 {
						 	var i_status = o_QA_OBJ.getFieldValue('custrecord_approvalstatusforquotation');
							
							var i_item = o_QA_OBJ.getFieldValue('custrecord_item');
		
							var i_S_No = o_QA_OBJ.getFieldValue('custrecord_srno');
																					
							var i_PR_No =  o_QA_OBJ.getFieldValue('custrecord_prquote');
							
							var i_finance_head = o_QA_OBJ.getFieldValue('custrecord_financehead');
																					
							var i_vertical_head =  o_QA_OBJ.getFieldValue('custrecord_verticalheadforquotation');
												
							var i_PR_ID =  search_PR_Item_recordID(i_PR_No,i_S_No,i_item)
														
							nlapiLogExecution('DEBUG', 'afterSubmit_Item_Data_Sourcing',' i_status -->' + i_status);	
	                        nlapiLogExecution('DEBUG', 'afterSubmit_Item_Data_Sourcing',' i_vendor_PO -->' + i_vendor_PO);	
	                 
							
						 	o_QA_OBJ.setFieldValue('custrecord_amount1',f_total_amt_V1)
							o_QA_OBJ.setFieldValue('custrecord_amount2',f_total_amt_V2)
							o_QA_OBJ.setFieldValue('custrecord_amount3',f_total_amt_V3)
							o_QA_OBJ.setFieldValue('custrecord_preferred_vendor',i_preferred_vendor)
							
							var newPrStatus = '';
						 	var newQaStatus = '';
							if(i_preferred_vendor == o_QA_OBJ.getFieldValue('custrecord_vendorforpo')){
								newPrStatus = 27;
								newQaStatus = 7;
							}
							else{
								newPrStatus = 24;
								newQaStatus = 6;
							}
														 
							 if(i_status == 5)
							 {
							 	if(_logValidation(i_vendor_PO))
								{
									if (_logValidation(i_PR_ID)) 
									{
										var o_PR_Item_OBJ = nlapiLoadRecord('customrecord_pritem', i_PR_ID, {recordmode : 'dynamic'});
										
										if (_logValidation(o_PR_Item_OBJ)) 
										{											  
											  o_PR_Item_OBJ.setFieldValue('custrecord_vendor_pr_item',o_QA_OBJ.getFieldValue('custrecord_vendorforpo'));											  									
											  o_PR_Item_OBJ.setFieldValue('custrecord_prastatus',newPrStatus);
											  o_QA_OBJ.setFieldValue('custrecord_approvalstatusforquotation', newQaStatus);											
											  var i_submitID_QA = nlapiSubmitRecord(o_QA_OBJ,true,true);					
										      var i_PR_submitID = nlapiSubmitRecord(o_PR_Item_OBJ,true,true);
										      
										      //nlapiTriggerWorkflow('customrecord_pritem', i_PR_ID, 12);
										      if(newPrStatus == 24){
										    	  sendNotificationToFinanceHead(o_PR_Item_OBJ);
										    	  nlapiInitiateWorkflow('customrecord_pritem', i_PR_ID, 'customworkflow_prworkflow');
										      }
										      
										      sendFyiMailToPracticeHead(o_PR_Item_OBJ);
											
											  nlapiLogExecution('debug','PR mails to practice head triggered');											
											  
											  //sent_email(i_user,i_vertical_head)
											   
											  nlapiLogExecution('DEBUG', 'afterSubmit_Item_Data_Sourcing',' i_submitID_QA -->' + i_submitID_QA);	
					  		
							                  nlapiLogExecution('DEBUG', 'afterSubmit_Item_Data_Sourcing',' i_PR_submitID -->' + i_PR_submitID);	
							 
											
										}
									}
									
								}
							 	
							 }
													
							var i_QA_submitID = nlapiSubmitRecord(o_QA_OBJ,true,true)
							nlapiLogExecution('DEBUG', 'afterSubmit_Item_Data_Sourcing',' ********************** QA Submit ID ********************** -->' + i_QA_submitID);	
		  	                 
							 if((i_vendor_1!=null && i_vendor_1!=undefined && i_vendor_1!='') && ((i_vendor_2 ==null || i_vendor_2==undefined || i_vendor_2=='')  && (i_vendor_3==null || i_vendor_3==undefined || i_vendor_3=='')))
							 {
							 
                            var i_PR_Item_recordID = search_PR_Item_recordID(i_recordID,i_SR_No_QA,i_item_QA) 
							nlapiLogExecution('DEBUG', 'afterSubmit_Item_Data_Sourcing',' PR Item Record ID -->' + i_PR_Item_recordID);	
	                 
							  if (_logValidation(i_PR_Item_recordID)) 
							  {
							  	var o_PR_Item_OBJ = nlapiLoadRecord('customrecord_pritem', i_PR_Item_recordID)
							  	
							  	if (_logValidation(o_PR_Item_OBJ)) 
								{
									o_PR_Item_OBJ.setFieldValue('custrecord_vendor_pr_item',i_vendor_1)
									
									var i_PR_submitID = nlapiSubmitRecord(o_PR_Item_OBJ,true,true)
							        nlapiLogExecution('DEBUG', 'afterSubmit_Item_Data_Sourcing',' ********************** PR Item Submit ID ********************** -->' + i_PR_submitID);	
		  	                 
								
							  	}
							  }

							 	
							 }
							 
							 
							
							 
						 }
						
						
						}
		  
				}
								
			}//Line Count QA			
			
		}//Record Object
		
	}//TRY
	catch(exception)
	{
	  nlapiLogExecution('DEBUG', 'ERROR',' Exception Caught -->' + exception);	
	}//CATCH	
	
   }//CREATE
	return true;
}

function updatePrItemWithVendorDetails(prItemId, vendorForPo) {
	try {
		// get the QA for this PR item
		var qaSearch = nlapiSearchRecord('customrecord_quotationanalysis',
		        null, [
		                new nlobjSearchFilter('isinactive', null, 'is', 'F'),
		                new nlobjSearchFilter('custrecord_qa_pr_item', null,
		                        'anyof', prItemId) ], [ new nlobjSearchColumn(
		                'custrecord_vendorforpo') ]);

		if (qaSearch) {
			var qaId = qaSearch[0].getId();
			nlapiLogExecution('debug', 'quotation id', qaId);
			var vendorForPo = qaSearch[0].getValue('custrecord_vendorforpo');
			nlapiLogExecution('debug', 'vendorForPo', vendorForPo);

			nlapiSubmitField('customrecord_pritem', prItemId, [
			        'custrecord_pr_item_quote', 'custrecord_vendor_pr_item' ],
			        [ qaId, vendorForPo ]);
			nlapiLogExecution('debug', 'PR Item Updated with Vendor Details');
		} else {
			nlapiLogExecution('ERROR', 'No quotation associated with PR Item');
		}
	} catch (err) {
		nlapiLogExecution('ERROR', 'updatePrItemWithVendorDetails', err);
	}
}

function sendNotificationToFinanceHead(o_PR_Item_OBJ){
	var mailText = "";	
	mailText += "<p>Hi " + nlapiLookupField('employee', o_PR_Item_OBJ.getFieldValue('custrecord_pri_finance_approver'), 'firstname') + ", </p>";
	mailText += "<p>This is to inform you that a quotation is pending your approval.</p>";
	mailText += "<p>";
	mailText += "<b>Purchase Request : </b>" + o_PR_Item_OBJ.getFieldValue('custrecord_prno1') + "<br/>";
	mailText += "<b>Employee : </b>" 
		+ nlapiLookupField('employee', o_PR_Item_OBJ.getFieldValue('custrecord_employee'), 'entityid') + "<br/>";
	mailText += "<b>Item :</b>" + o_PR_Item_OBJ.getFieldText('custrecord_prmatgrpcategory') + "<br/>";
	mailText += "<b>Description : </b>" + o_PR_Item_OBJ.getFieldValue('custrecord_prmatldescription') + "<br/>";
	mailText += "<b>Quantity : </b>" + o_PR_Item_OBJ.getFieldValue('custrecord_quantity') + "<br/>";
	mailText += "<b>Unit Price : </b>" + o_PR_Item_OBJ.getFieldValue('custrecord_valueperunit') + "<br/>";
	mailText += "<b>Total Price : </b>" + o_PR_Item_OBJ.getFieldValue('custrecord_totalvalue');
	mailText += "</p>";
	mailText += "<p>Regards,<br/>Information Systems</p>";	
	
	nlapiSendEmail('442', nlapiLookupField('employee', o_PR_Item_OBJ.getFieldValue('custrecord_pri_finance_approver'), 'email'), 
			'Purchase Request #' + o_PR_Item_OBJ.getFieldValue('custrecord_prno1') +
			' - Pending Finance Approval', mailText, null, null);
	nlapiLogExecution('debug', 'mail has been send to finance head');
}

function sendFyiMailToPracticeHead(o_PR_Item_OBJ){
	var mailText = "";	
	mailText += "<p>Hi " + nlapiLookupField('employee', o_PR_Item_OBJ.getFieldValue('custrecord15'), 'firstname') + ", </p>";
	mailText += "<p><b>This mail is just for your information.</b></p>";
	mailText += "<p>This is to inform you that a purchase request has been approved under your practice.</p>";
	mailText += "<p>";
	mailText += "<b>Purchase Request : </b>" + o_PR_Item_OBJ.getFieldValue('custrecord_prno1') + "<br/>";
	mailText += "<b>Employee : </b>" 
		+ nlapiLookupField('employee', o_PR_Item_OBJ.getFieldValue('custrecord_employee'), 'entityid') + "<br/>";
	mailText += "<b>Item :</b>" + o_PR_Item_OBJ.getFieldText('custrecord_prmatgrpcategory') + "<br/>";
	mailText += "<b>Description : </b>" + o_PR_Item_OBJ.getFieldValue('custrecord_prmatldescription') + "<br/>";
	mailText += "<b>Quantity : </b>" + o_PR_Item_OBJ.getFieldValue('custrecord_quantity') + "<br/>";
	mailText += "<b>Unit Price : </b>" + o_PR_Item_OBJ.getFieldValue('custrecord_valueperunit') + "<br/>";
	mailText += "<b>Total Price : </b>" + o_PR_Item_OBJ.getFieldValue('custrecord_totalvalue');
	mailText += "</p>";
	mailText += "<p>Regards,<br/>Information Systems</p>";	
	
	nlapiSendEmail('442', nlapiLookupField('employee', o_PR_Item_OBJ.getFieldValue('custrecord15'), 'email'), 
			'FYI : Purchase Request #' + o_PR_Item_OBJ.getFieldValue('custrecord_prno1') +
			' - Approved By Procurement Team', mailText, null, null);
	nlapiLogExecution('debug', 'mail has been send to parent practice head');
}

// END AFTER SUBMIT ===============================================

// BEGIN FUNCTION ===================================================
function _logValidation(value) 
{
 if(value != null && value.toString() != null && value != '' && value != undefined && value.toString() != undefined && value != 'undefined' && value.toString() != 'undefined'&& value.toString() != 'NaN' && value != NaN) 
 {
  return true;
 }
 else 
 { 
  return false;
 }
}

function validate_value(value) 
{
  var return_value;	
 if(value != null && value.toString() != null && value != '' && value != undefined && value.toString() != undefined && value != 'undefined' && value.toString() != 'undefined'&& value.toString() != 'NaN' && value != NaN) 
 {
  return_value = value
 }
 else 
 { 
  return_value = 0
 }
 return return_value
}

function search_PR_Item_recordID(i_recordID,i_SR_No_QA,i_item_QA)
{
    nlapiLogExecution('DEBUG', 'search_PR_Item_recordID', 'i_recordID ==' +i_recordID);
   nlapiLogExecution('DEBUG', 'search_PR_Item_recordID', 'i_SR_No_QA ==' +i_SR_No_QA);
   nlapiLogExecution('DEBUG', 'search_PR_Item_recordID', 'i_item_QA ==' +i_item_QA);
	   	
   var i_internal_id;
   
   var filter = new Array();
   filter[0] = new nlobjSearchFilter('custrecord_purchaserequest', null, 'is',i_recordID);
   filter[1] = new nlobjSearchFilter('custrecord_prlineitemno', null, 'is',parseInt(i_SR_No_QA));
   filter[2] = new nlobjSearchFilter('custrecord_prmatgrpcategory', null, 'is',i_item_QA);
	
   var columns = new Array();
   columns[0] = new nlobjSearchColumn('internalid');
   columns[1] = new nlobjSearchColumn('custrecord_purchaserequest');
   columns[2] = new nlobjSearchColumn('custrecord_prlineitemno');
   columns[3] = new nlobjSearchColumn('custrecord_prmatgrpcategory');
   
   
   var a_seq_searchresults = nlapiSearchRecord('customrecord_pritem',null,filter,columns);
	
	if (a_seq_searchresults != null && a_seq_searchresults != '' && a_seq_searchresults != undefined)
	{	
	 nlapiLogExecution('DEBUG', 'search_PR_Item_recordID', 'a_seq_searchresults ==' +a_seq_searchresults.length);
	 
	 for(var ty=0;ty<a_seq_searchresults.length;ty++)
	 {
	 	i_internal_id = a_seq_searchresults[ty].getValue('internalid');
		nlapiLogExecution('DEBUG', 'search_PR_Item_recordID', ' Internal ID -->' + i_internal_id);
		
		var i_PR = a_seq_searchresults[ty].getValue('custrecord_purchaserequest');
		nlapiLogExecution('DEBUG', 'search_PR_Item_recordID', ' i_PR ID -->' + i_PR);
		
		var i_line_no = a_seq_searchresults[ty].getValue('custrecord_prlineitemno');
		nlapiLogExecution('DEBUG', 'search_PR_Item_recordID', ' i_line_no -->' + i_line_no);
		
		var i_item = a_seq_searchresults[ty].getValue('custrecord_prmatgrpcategory');
		nlapiLogExecution('DEBUG', 'search_PR_Item_recordID', ' i_item-->' + i_item);
		
		if((i_PR == i_recordID)&&(i_line_no == i_SR_No_QA)&&(i_item == i_item_QA))
		{
			i_internal_id = i_internal_id
			break
		}
		
	 }//LOOP
	 
		
		
	}
	
	return i_internal_id;
	
}


function search_quotation_analysis_recordID(i_recordID,i_SR_No_QA,i_item_QA)
{	
   var i_internal_id;
   
   var filter = new Array();
   filter[0] = new nlobjSearchFilter('custrecord_prquote', null, 'is',i_recordID);
   filter[1] = new nlobjSearchFilter('custrecord_srno', null, 'is',i_SR_No_QA);
   filter[2] = new nlobjSearchFilter('custrecord_item', null, 'is',i_item_QA);
	
   var columns = new Array();
   columns[0] = new nlobjSearchColumn('internalid');
   columns[1] = new nlobjSearchColumn('custrecord_prquote');
   columns[2] = new nlobjSearchColumn('custrecord_srno');
   columns[3] = new nlobjSearchColumn('custrecord_item');
   
   var a_seq_searchresults = nlapiSearchRecord('customrecord_quotationanalysis',null,filter,columns);
	
	if (a_seq_searchresults != null && a_seq_searchresults != '' && a_seq_searchresults != undefined)
	{	
		 for(var ty=0;ty<a_seq_searchresults.length;ty++)
		 {
		 	i_internal_id = a_seq_searchresults[ty].getValue('internalid');
			nlapiLogExecution('DEBUG', 'search_quotation_analysis_recordID', ' Internal ID -->' + i_internal_id);
			
			
           var i_PR = a_seq_searchresults[ty].getValue('custrecord_prquote');
			nlapiLogExecution('DEBUG', 'search_quotation_analysis_recordID', ' i_PR ID -->' + i_PR);
			
			var i_line_no = a_seq_searchresults[ty].getValue('custrecord_srno');
			nlapiLogExecution('DEBUG', 'search_quotation_analysis_recordID', ' i_line_no -->' + i_line_no);
			
			var i_item = a_seq_searchresults[ty].getValue('custrecord_item');
			nlapiLogExecution('DEBUG', 'search_quotation_analysis_recordID', ' i_item-->' + i_item);

			
			if((i_PR == i_recordID)&&(i_line_no == i_SR_No_QA)&&(i_item == i_item_QA))
			{
				i_internal_id = i_internal_id
				break
			}
			
		 }//LOOP
	}
	
	return i_internal_id;
	
}


function sent_email(i_author,i_recepientID)
{
	var i_recepient = get_employee_details(i_recepientID)
	
	var s_email_subject = ' Quotation analysis has been raised for your approval .'
			
	var s_email_body ='';	
	s_email_body+='Hi,\n\n';
	s_email_body+='Quotation analysis has been raised for your approval , please login to the Netsuite and go to reminder â€˜Quotation pending for practice head approvalâ€™ to approve / reject.\n\n';
	s_email_body+='Regards\n';
	s_email_body+='Information systems\n';
				
	var records = new Object();
    records['entity'] = i_author	

	if(_logValidation(442)&&_logValidation(i_recepient)) 
	{		
		 // commented by Nitish
		 //nlapiSendEmail(442, i_recepient, s_email_subject, s_email_body, null , null, records, null);
		 //nlapiLogExecution('DEBUG', 'sent_email', ' Email Sent ......');	
				
	}//Author & Recepient	
}//Email

function get_employee_details(i_employee)
{	
  var s_email;
  var s_name ;
  var a_return_array = new Array();
  
  if(_logValidation(i_employee))
  {
  	var o_employeeOBJ = nlapiLoadRecord('employee',i_employee)
	
	if(_logValidation(o_employeeOBJ))
	{
	  s_email = o_employeeOBJ.getFieldValue('email')  
	  nlapiLogExecution('DEBUG', 'get_employee_email',' Email -->' + s_email);
			
	}//Employee OBJ
		
  }//Employee
  
  return s_email ;
  	
}//Recipient Email
// END FUNCTION =====================================================
