/**
 * @NApiVersion 2.x
 * @NScriptType WorkflowActionScript
 * @NModuleScope SameAccount
 * @author Nihal Mulani, nihal.mulani@gmail.com
 * @description Error ReSync
 */
var url = 'https://brillio-redcarpet.silkroad.com:443/eprise/WebServices';
define(["N/record","N/search",'N/https','N/xml','N/format', 'N/cache','N/file','N/plugin','N/email'], function (record,search,https,xml,format,cache,file,plugin,email) {
	var email_Notification = ['nihal@inspirria.com','Purushottam.S@brillio.com','Surojit.Dhar@brillio.com','silkroad.support@brillio.com','mahesh.alavala@brillio.com','prabhat.gupta@brillio.com'];
	var flag = false;
	function onAction(context) {
		var errorRec = context.newRecord;
		var allocationresource = errorRec.getValue('custrecord_cross_employee_id');
		var project_id = errorRec.getValue('custrecord_cross_project_id');
		var allocation_id = Number(errorRec.getValue('custrecord_allocation_id'));
		var allo_startDate  = search.lookupFields({
			type: search.Type.RESOURCE_ALLOCATION,
			id: allocation_id,
			columns: ['startdate']
		});
		var allocation_start_date = allo_startDate.startdate;
		log.debug('allocation_start_date',allocation_start_date);
		var objload_project = record.load({
			type: record.Type.JOB,
			id: project_id
		});
		var client = objload_project.getValue({fieldId: 'parent'});
		var clientText = objload_project.getText({fieldId: 'parent'});
		var objload_EMPLOYEE = record.load({
			type: record.Type.EMPLOYEE,
			id: allocationresource
		});
		var emp_email_id = objload_EMPLOYEE.getValue({fieldId: 'email'});
		var hire_date = objload_EMPLOYEE.getValue({fieldId: 'custentity_actual_hire_date'});
		var token = CheckSilkroadConnection();
		var xml = generateXML(token,objload_EMPLOYEE,clientText,allocation_start_date);
		sendDataToSilkRoad(token,xml,emp_email_id,clientText,allocation_start_date,objload_EMPLOYEE,false,allocationresource,project_id,allocation_id);
	}
	function getAllocations(emp,excludeId)
	{
		var resourceallocationSearchObj = search.create({
			type: "resourceallocation",
			filters:
				[
					["resource","anyof",emp], 
					"AND", 
					["startdate","onorafter","05/11/2020"], 
					"AND", 
					["internalid","noneof",excludeId]
					],
					columns:
						[

							search.createColumn({
								name: "enddate",
								sort: search.Sort.DESC,

							}),
							search.createColumn({
								name: "percentoftime",
								sort: search.Sort.DESC,

							}),
							search.createColumn({
								name: "id",								
								label: "ID"
							}),
							search.createColumn({name: "resource", label: "Resource"}),
							search.createColumn({name: "customer", label: "Customer"}),
							search.createColumn({name: "company", label: "Project"})					

							]
		});
		var results = [];
		var count = 0;
		var pageSize = 1000;
		var start = 0;
		do {
			var subresults = resourceallocationSearchObj.run().getRange({
				start: start,
				end: start + pageSize
			});

			results = results.concat(subresults);
			count = subresults.length;
			start += pageSize;
		} while (count == pageSize);


		var data = new Array();
		for (var res = 0; res < results.length; res++)
		{
			var customer = results[res].getValue({
				name: "customer",
			});
			var percentage = results[res].getValue({
				name: "percentoftime",
			});

			percentage = percentage.split(".")[0];
			var endDate = results[res].getValue({
				name: "enddate",
			});
			data.push({"customer" : customer,
				"percentage" : percentage,
				"endDate" : endDate
			});
		}
		return data;
	}

	function getAllClients()
	{
		var customrecord_silkroad_clients_crossboardSearchObj = search.create({
			type: "customrecord_silkroad_clients_crossboard",
			filters:
				[
					["isinactive","is","F"]
					],
					columns:
						[
							search.createColumn({name: "custrecord_client_cross_boarding", label: "Client"})
							]
		});

		var results = [];
		var count = 0;
		var pageSize = 1000;
		var start = 0;
		do {
			var subresults = customrecord_silkroad_clients_crossboardSearchObj.run().getRange({
				start: start,
				end: start + pageSize
			});

			results = results.concat(subresults);
			count = subresults.length;
			start += pageSize;
		} while (count == pageSize);


		var custdata = new Array();
		for (var res = 0; res < results.length; res++)
		{
			var customer = results[res].getValue({
				name: "custrecord_client_cross_boarding",
			});
			custdata.push(customer);
		}
		return custdata;
	}
	function CheckSilkroadConnection()
	{
		var Login = getLoginString();
		var Login_headers = {
				"soapAction":url+'/LogIn',
				"Content-Type": "text/xml;charset=UTF-8"
		};
		var response = https.post({
			url: 'https://brillio-redcarpet.silkroad.com:443/eprise/WebServices',
			body: Login,
			headers: Login_headers
		});
		log.debug("response Token",response);
		var myresponse_body = response.body;

		var xmlDocument = xml.Parser.fromString({
			text: myresponse_body
		});
		var loginId = xmlDocument.getElementsByTagName({
			tagName: 'LogInResult'
		})[0].textContent;
		log.debug("loginId.body",loginId);
		return loginId;
	}
	function getLoginString()
	{
		var logStr = '';
		logStr+='<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:epr="http://Eprise">';
		logStr+='<soapenv:Header/>';
		logStr+='<soapenv:Body>';
		logStr+='<epr:LogIn>';
		logStr+='<epr:strLoginId>Brillio.Webservices</epr:strLoginId>';
		logStr+='<epr:strPassword>brillio123!</epr:strPassword>';
		logStr+='<epr:strRemoteIP></epr:strRemoteIP>';
		logStr+='	</epr:LogIn>';
		logStr+='	</soapenv:Body>';
		logStr+='</soapenv:Envelope>';
		return logStr;
	}
	function sendDataToSilkRoad(key,xml,res_name,clientText,res_start_date,objload_EMPLOYEE,flag,allocationresource,projectid,alloc_ID)
	{
		if(flag)
		{
			log.debug("ReLaunch","**************************Relaunch Event**************************");
		}
		var event_headers = {
				"soapAction":url+'/LaunchEvent',
				"Content-Type": "text/xml;charset=UTF-8"
		};
		var responseData = https.post({
			url: 'https://brillio-redcarpet.silkroad.com:443/eprise/WebServices',
			body: xml,
			headers: event_headers
		});
		log.debug("SEND response Event launching",responseData);
		var dataXML = responseData.body;
		var ErrorNum = getXmlValue(dataXML,'ErrorNum');
		log.debug("ErrorNum",ErrorNum);
		if(ErrorNum == 0 || ErrorNum == "0")
		{
			var Message = getXmlValue(dataXML,'Message');
			log.debug("Error Message",Message);
			var message_comp = Message.split(":");
			if(message_comp[0].trim() == "Brillio Onboarding event already exists for user")
			{
				//logOutService(key);
				//var cancelEventtoken = CheckSilkroadConnection(true);
				var cancel_xml = cancelCrossBoardingEvent(key,message_comp[1].trim());
				var cancel_event_headers = {
						"soapAction":url+'/XmlUserEdit',
						"Content-Type": "text/xml;charset=UTF-8"
				};
				var cancel_responseData = https.post({
					url: 'https://brillio-redcarpet.silkroad.com:443/eprise/WebServices',
					body: cancel_xml,
					headers: cancel_event_headers
				});
				log.debug("Event Cancellation",cancel_responseData);
				//	logOutService(cancelEventtoken);
				//var resend_Eventtoken = CheckSilkroadConnection(true);
				var resend_xml = generateXML(key,objload_EMPLOYEE,clientText,res_start_date);
				log.debug("resend_xml",resend_xml);
				sendDataToSilkRoad(key,resend_xml,res_name,clientText,res_start_date,objload_EMPLOYEE,true,allocationresource,projectid,alloc_ID);
			}
			else{
				log.debug("Error email.send.body");
				var fileObj_request = file.create({
					name: 'request.xml',
					fileType: file.Type.PLAINTEXT,
					contents: xml,
					description: 'Cross Boarding Failed',
					encoding: file.Encoding.UTF8							
				});

				var fileObj_response = file.create({
					name: 'response.xml',
					fileType: file.Type.PLAINTEXT,
					contents: dataXML,
					description: 'Cross Boarding Failed',
					encoding: file.Encoding.UTF8							
				});

				var body = "Hi All, </br> </br>Re-sync Resource Cross Boarding Event is Failed </br></br>"

					body += "<b>Resource:</b> "+res_name +"\n";
				body += "<b>Client Name:</b> "+ clientText +"\n";
				body += "<b>Resource Start Date:</b> "+ res_start_date +"\n";
				body += "<b>Error Message:</b> "+ Message +"\n\n";
				body += "Thanks,</br>";
				body += "Information Systems"

					email.send({
						author: 442, // System Information
						recipients:email_Notification,//email_Notification,
						subject: 'Re-sync Brillio - Cross Boarding Failed',
						body: body,
						attachments: [fileObj_request,fileObj_response]
					});
				createErrorRecord(alloc_ID,allocationresource,projectid,xml,dataXML,Message);
			}
		}else if(ErrorNum == 1 || ErrorNum == "1"){

			log.debug("success email.send.body");

			var fileObj_request = file.create({
				name: 'response.xml',
				fileType: file.Type.PLAINTEXT,
				contents: xml,
				description: 'Brillio - Successful Creation of Cross Boarding',
				encoding: file.Encoding.UTF8							
			});

			var fileObj_response = file.create({
				name: 'response.xml',
				fileType: file.Type.PLAINTEXT,
				contents: dataXML,
				description: 'Successful Creation of Cross Boarding',
				encoding: file.Encoding.UTF8							
			});

			var body = "Hi All, </br></br> This Resource Cross Boarding Event Re-sync Successfully Launched </br></br>"

				body += "<b>Resource:</b> "+res_name +"\n";
			body += "<b>Client Name:</b> "+ clientText +"\n";
			body += "<b>Resource Start Date:</b> "+ res_start_date +"\n \n";

			body += "Thanks,\n";
			body += "Information Systems"

				email.send({
					author: 442, // System Information
					recipients:email_Notification,//email_Notification,
					subject: 'Re-Sync Brillio - Successfully Launched The Cross Boarding',
					body: body,
					attachments: [fileObj_request,fileObj_response]
				});
		}
		logOutService(key);
	}
	function createErrorRecord(alloc_ID,allocationresource,projectid,xml,dataXML,Message)
	{
		try {
			var errRecObj = record.create({
				type: 'customrecord_silkroad_cross_boarding_err'
			});

			errRecObj.setValue({
				fieldId: 'custrecord_allocation_id',
				value: alloc_ID
			});
			errRecObj.setValue({
				fieldId: 'custrecord_cross_employee_id',
				value: allocationresource
			});
			errRecObj.setValue({
				fieldId: 'custrecord_cross_project_id',
				value: projectid
			});
			errRecObj.setValue({
				fieldId: 'custrecord_request_data',
				value: xml
			});
			errRecObj.setValue({
				fieldId: 'custrecord_response_data',
				value: dataXML
			});
			errRecObj.setValue({
				fieldId: 'custrecord_error_message',
				value: Message
			});
			errRecObj.save();
		} catch (e) {
			log.error('Error Catcher Rec Creation Failed', e);
		}
	}
	function cancelCrossBoardingEvent(tokenpara,fusionID)
	{
		var cXML = '';
		cXML+='<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:epr="http://Eprise">';
		cXML+='<soapenv:Header/>';
		cXML+='<soapenv:Body>';
		cXML+='<epr:XmlUserEdit>';
		cXML+='<epr:strSecurityToken>'+tokenpara+'</epr:strSecurityToken>';
		cXML+='<epr:strUsersXML>';
		cXML+='<![CDATA[<?xml version="1.0"?>';
		cXML+='<users>';
		cXML+='<user>';
		cXML+='<UserToEdit_EmployeeID>';
		cXML+='<EmployeeIDType>Employee_HRISID</EmployeeIDType>';
		cXML+='<ID>'+fusionID+'</ID>';
		cXML+='</UserToEdit_EmployeeID>';
		//cXML+='<UserToEdit_Email>'+generateEscapedXMLBody(emailId)+'</UserToEdit_Email>';
		cXML+='<TerminateEvent>';
		cXML+='<Name>Brillio Onboarding</Name>';
		cXML+='</TerminateEvent>';
		cXML+='</user>';
		cXML+='</users>';
		cXML+=']]>';
		cXML+='</epr:strUsersXML>';
		cXML+='</epr:XmlUserEdit>';
		cXML+='</soapenv:Body>';
		cXML+='</soapenv:Envelope>';
		return cXML;
	}
	function calculateDaysDiff(stDate,edDate)
	{
		// Date format will be MM/DD/YYYY
		var date1 = new Date(stDate); 
		var date2 = new Date(edDate); 
		var Difference_In_Time = date2.getTime() - date1.getTime(); 
		return Difference_In_Time / (1000 * 3600 * 24);
	}
	function getXmlValue(str, key) {
		return str.substring(
				str.lastIndexOf('<' + key + '>') + ('<' + key + '>').length,
				str.lastIndexOf('</' + key + '>')
		);
	}
	function generateXML(key,data,project,startdate)
	{
		xml= '';
		xml+='<?xml version="1.0" encoding="UTF-8"?>';
		xml+='<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:epr="http://Eprise">';
		xml+='<soapenv:Header />';
		xml+='<soapenv:Body>';
		xml+='<epr:LaunchEvent>';
		xml+='<epr:strSecurityToken>'+key+'</epr:strSecurityToken>';
		xml+='<epr:launchEventXml><![CDATA[<?xml version="1.0" encoding="UTF-8"?>';
		xml+='<Events>';
		xml+='<Event>';

		xml+='<ForWhom_EmployeeID>';
		xml+='<EmployeeIDType>Employee_HRISID</EmployeeIDType>';
		xml+='<ID>'+data.getValue({fieldId: 'custentity_fusion_empid'})+'</ID>';
		xml+='</ForWhom_EmployeeID>';
		var Man_email = '';
		if(data.getValue({fieldId: 'custentity_reportingmanager'}))
		{
			Man_email = search.lookupFields({
				type: search.Type.EMPLOYEE,
				id: data.getValue({fieldId: 'custentity_reportingmanager'}),
				columns: ['email']
			});
			Man_email = Man_email.email;
		}

		var hrbp_email = '';
		if(data.getValue({fieldId: 'custentity_emp_hrbp'}))
		{
			hrbp_email = search.lookupFields({
				type: search.Type.EMPLOYEE,
				id: data.getValue({fieldId: 'custentity_emp_hrbp'}),
				columns: ['email']
			});
			hrbp_email = hrbp_email.email;
		}
		//log.debug('Man_email',Man_email.email);
		xml+='<Name>Brillio Onboarding</Name>';
		xml+='<Person>';
		xml+='<Name>Manager</Name>';
		xml+='<Value>'+generateEscapedXMLBody(Man_email)+'</Value>';
		xml+='</Person>';
		xml+='<Person>';
		xml+='<Name>HR Coordinator</Name>'; // HRBP
		xml+='<Value>'+generateEscapedXMLBody(hrbp_email)+'</Value>';
		xml+='</Person>';
		xml+='<Person>';
		xml+='<Name>Recruiter</Name>'; // Need clarification from silkroad 
		xml+='<Value>Recruiter@brillio.com</Value>'; //aiyappa.kv@BRILLIO.COM
		xml+='</Person>';



		var formattedDateString = format.format({
			value: startdate,
			type: format.Type.DATE,
			timezone: format.Timezone.AMERICA_LOS_ANGELES
		});

		/*
		var formattedDateString = format.format({
			value: new Date(),
			type: format.Type.DATE,
			timezone: format.Timezone.AMERICA_LOS_ANGELES
		});
		 */
		/*	var parsedDateStringAsRawDateObject = format.parse({
			value: formattedDateString,
			type: format.Type.DATE
		});*/

		log.debug("formattedDateString", formattedDateString);

		/*var stampSplit = format.format({
			value: data.getValue({fieldId: 'hiredate'}),
			type: format.Type.DATE
		});
		log.debug("DATE stampSplit",stampSplit);*/
		var datesplit = formattedDateString.split("/");
		var datehire = datesplit[2]+"-"+datesplit[0]+"-"+datesplit[1];

		log.debug("datehire", datehire);

		xml+='<Date>';
		xml+='<Name>Date of Joining</Name>'; //
		xml+='<Value>'+datehire+'</Value>';//todays date Mahesh confirmed
		xml+='</Date>';

		xml+='<Category>';
		xml+='<Name>Base Location</Name>';
		xml+='<Value>'+ generateEscapedXMLBody(data.getValue({fieldId: 'custentity_list_brillio_location_e'}))+'</Value>';
		xml+='<Type>Name</Type>';
		xml+='</Category>';

		xml+='<Category>';
		xml+='<Name>Employee Type</Name>';
		xml+='<Value>'+ generateEscapedXMLBody(data.getText({fieldId: 'custentity_employeetype'}))+'</Value>';
		xml+='<Type>Name</Type>';
		xml+='</Category>';

		xml+='<Category>';
		xml+='<Name>H1B Transfer</Name>';// Purushootam says pass Hardcode value.
		xml+='<Value>No</Value>';
		xml+='<Type>Name</Type>';
		xml+='</Category>';

		xml+='<Category>';
		xml+='<Name>Legal Employer</Name>';
		xml+='<Value>'+ generateEscapedXMLBody(data.getValue({fieldId: 'custentity_legal_entity_fusion'}))+'</Value>';
		xml+='<Type>Name</Type>';
		xml+='</Category>';

		xml+='<Category>';
		xml+='<Name>Level</Name>';
		xml+='<Value>'+ data.getText({fieldId: 'employeestatus'})+'</Value>';
		xml+='<Type>Name</Type>';
		xml+='</Category>';

		xml+='<Category>';
		xml+='<Name>Relocation</Name>'; // Purushootam says pass Hardcode value.
		xml+='<Value>No</Value>';
		xml+='<Type>Name</Type>';
		xml+='</Category>';

		xml+='<Category>';
		xml+='<Name>All Roles</Name>'; // title
		xml+='<Value>'+generateEscapedXMLBody(data.getValue({fieldId: 'title'}))+'</Value>';
		xml+='<Type>Name</Type>';
		xml+='</Category>';

		var fieldLookUp = search.lookupFields({
			type: search.Type.DEPARTMENT,
			id: data.getValue({fieldId: 'department'}),
			columns: ['custrecord_parent_practice','name']
		});
		//	log.debug('fieldLookUp.custrecord_parent_practice',JSON.stringify(fieldLookUp))
		//	log.debug('fieldLookUp.name',fieldLookUp)
		xml+='<Category>';
		xml+='<Name>Practice</Name>';
		xml+='<Value>'+generateEscapedXMLBody(fieldLookUp.custrecord_parent_practice[0].text)+'</Value>';
		xml+='<Type>Name</Type>';
		xml+='</Category>';

		xml+='<Category>';
		xml+='<Name>Location City State</Name>';
		xml+='<Value>'+ generateEscapedXMLBody(data.getValue({fieldId: 'custentity_workcity'}))+'</Value>';
		xml+='<Type>Name</Type>';
		xml+='</Category>';

		xml+='<Category>';
		xml+='<Name>Employee Function</Name>';
		xml+='<Value>'+ generateEscapedXMLBody(data.getText({fieldId: 'custentity_emp_function'}))+'</Value>';
		xml+='<Type>Name</Type>';
		xml+='</Category>';
		var subsid = SubMapping(data.getValue({fieldId: 'subsidiary'}))
		xml+='<Category>';
		xml+='<Name>All Business Unit</Name>';
		xml+='<Value>'+generateEscapedXMLBody(subsid)+'</Value>';
		xml+='<Type>Name</Type>';
		xml+='</Category>';
		log.debug('fresh before: ',data.getValue({fieldId: 'custentity_fresher'}));
		var fresh = data.getValue({fieldId: 'custentity_fresher'}) ? "Fresher" : "Lateral";
		log.debug('fresh after: ',fresh)

		xml+='<Category>';
		xml+='<Name>Hire Type</Name>';
		xml+='<Value>'+generateEscapedXMLBody(fresh)+'</Value>';
		xml+='<Type>Name</Type>';
		xml+='</Category>';
		var dept = fieldLookUp.name;
		var r = dept.split(":");
		xml+='<Category>';
		xml+='<Name>Department</Name>';
		xml+='<Value>'+generateEscapedXMLBody(r[r.length-1].trim())+'</Value>';
		xml+='<Type>Name</Type>';
		xml+='</Category>';

		xml+='<Category>';
		xml+='<Name>Client</Name>';
		xml+='<Value>'+generateEscapedXMLBody(project)+'</Value>';//AMAW-01 Amazon Web Services, Inc.
		xml+='<Type>Name</Type>';
		xml+='</Category>';

		xml+='<Category>';
		xml+='<Name>Candidate Type</Name>';
		xml+='<Value>Employee</Value>';//'+ data.getText({fieldId: 'custentity_employeetype'})+'
		xml+='<Type>Name</Type>';
		xml+='</Category>';

		xml+='</Event>';
		xml+='</Events>]]>';
		xml+='</epr:launchEventXml>';
		xml+='</epr:LaunchEvent>';
		xml+='</soapenv:Body>';
		xml+='</soapenv:Envelope>';
		log.debug('XML: ',xml)
		return xml;
	}
	function logOutService(key)
	{
		var logout = '';
		logout+='<Envelope xmlns="http://schemas.xmlsoap.org/soap/envelope/">';
		logout+=' <Body>';
		logout+=' <LogOut xmlns="http://Eprise">';
		logout+=' <strSecurityToken>'+key+'</strSecurityToken>';
		logout+='</LogOut>';
		logout+=' </Body>';
		logout+='</Envelope>';

		var LogOut_headers = {
				"soapAction":url+'/LogOut',
				"Content-Type": "text/xml;charset=UTF-8"
		};
		var response = https.post({
			url: 'https://brillio-redcarpet.silkroad.com:443/eprise/WebServices',
			body: logout,
			headers: LogOut_headers
		});
		//	log.debug("response Logout",response);
		var myresponse_body = response.body;
		log.debug("myresponse_body Logout",myresponse_body);
	}
	function SubMapping(name)
	{
		if (name =='3')
		{
			return "Brillio Technologies India BU"
		}
		else if(name == '2')
		{
			return "Brillio LLC United States BU"
		}
		else if(name == '10')
		{
			return "Brillio Canada Inc BU"
		}
		else if(name == '7')
		{
			return "BRILLIO UK LIMITED BU"
		}
		else if(name == '21')
		{
			return "Cognetik Corp"
		}else
		{
			return null;
		}
	}

	function escapeXml(unsafeStr){
		if (unsafeStr) {
			return unsafeStr.replace(/[&<>'"]/g, function (c) {
				switch (c) {
				case '&': return '&amp;';
				case '<': return '&lt;';
				case '>': return '&gt;';
				case '\'': return '&apos;';
				case '"': return '&quot;';
				}
			});
		}
	};

	function generateEscapedXMLBody(body){
		var escapedBody = body
		if (body) {
			if (typeof body === "string") {
				escapedBody = escapeXml(body) || "";
			} else {
				escapedBody = body;
			}
		}
		return escapedBody;
	};
	return {
		onAction: onAction
	}
});