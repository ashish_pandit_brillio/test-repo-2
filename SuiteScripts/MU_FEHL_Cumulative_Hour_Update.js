/**
 * Update the comulative hours in the projects
 * 
 * Version Date Author Remarks 1.00 31 Mar 2016 Nitish Mishra
 * 
 */

function massUpdate(recType, recId) {
	try {
		var projectRec = nlapiLoadRecord(recType, recId);

		// get all the allocated employees to this project
		var resourceAllocationSearch = nlapiSearchRecord(
		        'resourceallocation',
		        null,
		        [
		                new nlobjSearchFilter('startdate', null, 'notafter',
		                        'today'),
		                new nlobjSearchFilter('enddate', null, 'notbefore',
		                        'today'),
		                new nlobjSearchFilter('project', null, 'anyof', recId) ],
		        [ new nlobjSearchColumn('resource') ]);

		if (resourceAllocationSearch) {

			for (var i = 0; i < resourceAllocationSearch.length; i++) {
				var rec = nlapiCreateRecord('customrecord_employee_cumulative_hour');
				rec.setFieldValue('custrecord_ech_project', recId);
				rec.setFieldValue('custrecord_ech_employee',
				        resourceAllocationSearch[i].getValue('resource'));
				var hr = projectRec
				        .getFieldValue('custentity_cummulativehours') ? projectRec
				        .getFieldValue('custentity_cummulativehours')
				        : 0;
				rec.setFieldValue('custrecord_ech_cumulative_hours', hr);
				rec.setFieldValue('custrecord_ech_last_invoiced_hours', 0);
				nlapiSubmitRecord(rec);
				nlapiLogExecution('DEBUG', 'ECH Record Created', recId);
			}
		}
	} catch (err) {
		nlapiLogExecution('ERROR', 'massUpdate', err);
	}
}
