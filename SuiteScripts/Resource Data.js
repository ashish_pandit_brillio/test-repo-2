/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       16 March 2020     Prabhat Gupta
 *
 */

/**
 * @param {Object} dataIn Parameter object
 * @returns {Object} Output object
 */
 function getRESTlet(dataIn) {

	return {};
}

/**
 * @param {Object} dataIn Parameter object
 * @returns {Object} Output object
 */
function postRESTlet(dataIn) {

	try {
		
		//dataIn = {"lastUpdatedTimestamp":""}
        var response = new Response();
        nlapiLogExecution('debug', 'dataIn', JSON.stringify(dataIn));
        var currentDateAndTime = sysDate();
		//Log for current date



		var receivedDate = dataIn.lastUpdatedTimestamp;
		//nlapiLogExecution('DEBUG', 'Current Date', 'receivedDate...' + receivedDate);
		//nlapiLogExecution('DEBUG', 'Current Date', 'currentDateAndTime...' + currentDateAndTime);

		//var receivedDate = '7/12/2019 1:00 am';
		//Check if the timestamp is empty
		if (_logValidation(receivedDate)) {          
			var filters = new Array();
			filters = [				
				 
				 ["employee.custentity_implementationteam","is","F"],				 
				  "AND", 
				 ["enddate","notbefore",receivedDate], 
				 "AND", 
				 ["systemnotes.type","is","T"]
				
				];
		nlapiLogExecution('debug', 'filters ', filters);
		}else{
			
			throw new Error("date format is mm/dd/yyyy and it should  be not before end date of allocation");
		}
		
		// created search by grouping 
		var searchResults = searchRecord("resourceallocation",null,
				filters, 
				[
				  new nlobjSearchColumn("internalid","employee",null), 
				   new nlobjSearchColumn("internalid","job",null), 
				   new nlobjSearchColumn("custentity_projectmanager","job",null), 
           new nlobjSearchColumn("custentity_deliverymanager","job",null), 
				   new nlobjSearchColumn("internalid","customer",null), 
				   new nlobjSearchColumn("location","employee",null), 
				   new nlobjSearchColumn("startdate"), 
				   new nlobjSearchColumn("enddate"), 
				   new nlobjSearchColumn("percentoftime"), 
					new nlobjSearchColumn("custeventrbillable"),  //prabhat gupta
					new nlobjSearchColumn("custentity_practice","job",null), //prabhat gupta 
					new nlobjSearchColumn("jobbillingtype","job",null),
					new nlobjSearchColumn("custevent_practice"), 
				   new nlobjSearchColumn("custrecord_parent_practice","CUSTEVENT_PRACTICE",null),
                   new nlobjSearchColumn("isinactive","employee",null),
				   new nlobjSearchColumn("date","systemNotes",null),
				  // new nlobjSearchColumn("custevent3")
				]
				);		
		
		
		var ResourceData = [];
      nlapiLogExecution('debug', 'searchResults', searchResults.length);
		if (searchResults) {
         nlapiLogExecution('debug', 'searchResults', searchResults.length);


			for(var i=0; i<searchResults.length; i++){    
				
		    var resource = {};
                   
			var internalId = searchResults[i].getId();
			var employeeInternalId = searchResults[i].getValue("internalid","employee",null);
			var projectInternalId = searchResults[i].getValue("internalid","job",null);
			var pmInternalId = searchResults[i].getValue("custentity_projectmanager","job",null);
			var dmInternalId = searchResults[i].getValue("custentity_deliverymanager","job",null);
			var accountInternalId = searchResults[i].getValue("internalid","customer",null);;
			var employeeLocation =  searchResults[i].getText("location","employee",null);;
			var employeeAllocationStartDate = searchResults[i].getValue('startdate');
			var employeeAllocationEndDate = searchResults[i].getValue('enddate');
			var allocationPercentage = searchResults[i].getValue('percentoftime');
			var billable = searchResults[i].getValue('custeventrbillable') ;    //prabhat gupta
			var billingTypeId = searchResults[i].getValue('jobbillingtype',"job",null) ; //prabhat gupta
			var billingType = searchResults[i].getText('jobbillingtype',"job",null) ; //prabhat gupta
			var projectPracticeInternalId = searchResults[i].getValue("custentity_practice","job",null) ;    //prabhat gupta
			var projectPracticeName = searchResults[i].getText("custentity_practice","job",null) ;    //prabhat gupta
			var allocationPractice = searchResults[i].getText("custevent_practice") ;  //prabhat gupta
			var allocationPracticeId = searchResults[i].getValue("custevent_practice") ;//prabhat gupta
              var isInactive = searchResults[i].getValue("isinactive","employee",null); //prabhat gupta
              var allocatedDate = searchResults[i].getValue("date","systemNotes",null);
              //var billRate = searchResults[i].getValue("custevent3"); //prabhat gupta 
              
			resource.internalId = internalId;
			resource.employeeInternalId = employeeInternalId;
			resource.projectInternalId = projectInternalId;
			resource.pmInternalId = pmInternalId;
			resource.dmInternalId = dmInternalId;
			resource.accountInternalId = accountInternalId;
			resource.employeeLocation = employeeLocation;
			resource.employeeAllocationStartDate = employeeAllocationStartDate;
			resource.employeeAllocationEndDate = employeeAllocationEndDate;
			resource.allocationPercentage	= allocationPercentage
			//resource.billRate = billRate;
            resource.billable = billable;	
			resource.billingTypeId = billingTypeId;	
            resource.billingType = billingType;			
			resource.projectPracticeInternalId = projectPracticeInternalId;
			resource.projectPracticeName = projectPracticeName;
			resource.allocationPractice = allocationPractice;
			resource.allocationPracticeId = allocationPracticeId;
              resource.isInactive = isInactive;
              resource.allocatedDate = allocatedDate
			ResourceData.push(resource);
			
		    }
		}
		

		response.timeStamp = receivedDate;
		response.data = ResourceData;
		//response.data = dataRow;
		response.status = true;
	//	nlapiLogExecution('debug', 'response',JSON.stringify(response));
	} catch (err) {
		nlapiLogExecution('ERROR', 'postRESTlet', err);
		response.timeStamp = '';
		response.data = err;
		response.status = false;
	}
	return response;

}
//validate blank entries
function _logValidation(value) {
	if (value != null && value.toString() != null && value != '' && value != undefined && value.toString() != undefined && value != 'undefined' && value.toString() != 'undefined' && value.toString() != 'NaN' && value != NaN) {
		return true;
	} else {
		return false;
	}
}
//Get current date


//Returns timestamp
function timestamp() {
	var str = "";

	var currentTime = new Date();
	var hours = currentTime.getHours();
	var minutes = currentTime.getMinutes();
	var seconds = currentTime.getSeconds();
	var meridian = "";
	if (hours > 12) {
		meridian += "pm";
	} else {
		meridian += "am";
	}
	if (hours > 12) {

		hours = hours - 12;
	}
	if (minutes < 10) {
		minutes = "0" + minutes;
	}
	if (seconds < 10) {
		seconds = "0" + seconds;
	}
	//str += hours + ":" + minutes + ":" + seconds + " ";
	str += hours + ":" + minutes + " ";
	return str + meridian;
}

function Response() {
	this.status = "";
	this.timeStamp = "";
	this.data = "";

}

//Search function for results more than 1000
function searchRecord(recordType, savedSearch, arrFilters, arrColumns,
		filterExpression) {

	try {
		var search = null;

		// if a saved search is provided, load it and add the filters and
		// columns
		if (savedSearch) {
			search = nlapiLoadSearch(recordType, savedSearch);

			if (isArrayNotEmpty(arrFilters)) {
				search.addFilters(arrFilters);
			}

			if (isArrayNotEmpty(arrColumns)) {
				search.addColumns(arrColumns);
			}

			if (isArrayNotEmpty(filterExpression)) {
				search.setFilterExpression(filterExpression);
			}
		}
		// create a new search
		else {
			search = nlapiCreateSearch(recordType, arrFilters, arrColumns);
		}

		// run search
		var resultSet = search.runSearch();

		// iterate through the search and get all data 1000 at a time
		var searchResultCount = 0;
		var resultSlice = null;
		var searchResult = [];

		do {
			resultSlice = resultSet.getResults(searchResultCount,
					searchResultCount + 1000);

			if (resultSlice) {

				resultSlice.forEach(function(result) {

					searchResult.push(result);
					searchResultCount++;
				});
			}
		} while (isArrayNotEmpty(resultSlice) && resultSlice.length >= 1000);

		return searchResult;
	} catch (err) {
		nlapiLogExecution('ERROR', 'searchRecord', err);
		throw err;
	}
}



function isEmpty(value) {

	return value == null || value == "" || typeof(value) == undefined;
}

function isNotEmpty(value) {

	return !isEmpty(value);
}

function isArrayEmpty(argArray) {

	return !isArrayNotEmpty(argArray);
}

function isArrayNotEmpty(argArray) {

	return (isNotEmpty(argArray) && argArray.length > 0);
}
//function to format date object into NetSuite's mm/dd/yyyy format.
function formatNSDate(dateObj) {
	if (dateObj) {
		var nsFormatDate = dateObj.getFullYear() + '-' + addZeros(dateObj.getMonth() + 1, 2) + '-' + addZeros(dateObj.getDate(), 2);
		return nsFormatDate;
	}
	return null;
}

//function to add leading zeros on date parts.
function addZeros(num, len) {
	var str = num.toString();
	while (str.length < len) {
		str = '0' + str;
	}
	return str;
}