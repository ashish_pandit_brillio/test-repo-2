function beforeSubmit() {
	var record_type = nlapiGetFieldValue('custrecord_fu_emp_exp_type');
	if(record_type == 1 || record_type == 2)
	{	
    try {
        nlapiLogExecution('debug', 'Running');
        var empList = [];
        nlapiLogExecution('debug', 'Fusion JSON record');
        var get_emp_data = nlapiGetFieldValue('custrecord_fu_emp_exp_json');
        nlapiLogExecution('debug', 'Data', get_emp_data);

        var emp_data = JSON.parse(get_emp_data);
        nlapiLogExecution('debug', 'Parse Data', emp_data);
        var o_emp_datafields = emp_data.Employee;
        nlapiLogExecution('debug', 'Employee Datafields', o_emp_datafields);
		
		if(o_emp_datafields){
        var s_personNumber = o_emp_datafields.PersonNumber;
        nlapiLogExecution('debug', 'Person Number', s_personNumber);
        var searchemployee = searchEmployeeRec(s_personNumber);
		}
		
		if(searchemployee){
			
			var emp_exp_detail = nlapiSearchRecord("customrecord_fu_emp_experience_data",null,
					[
					   ["isinactive","is","F"],"AND",["custrecord_fu_emp_name","is",searchemployee]
					], 
					[					  
					 new nlobjSearchColumn("internalid"), 
					  
					]
					);
					
					if(emp_exp_detail){
						
						for(var i=0; i<emp_exp_detail.length; i++){
							
							nlapiDeleteRecord ('customrecord_fu_emp_experience_data', emp_exp_detail[i].getId() );
						}					
						
					}
			
			var o_data_employeeProfile = emp_data.Profile.Profile;
			
			if(o_data_employeeProfile == undefined){                //for few record we are getting first profile as array so, it's coming as undefined
				o_data_employeeProfile = emp_data.Profile
			}
		
					if (isArray(o_data_employeeProfile))
						{
							nlapiLogExecution('debug','Length is ',o_data_employeeProfile.length);
							//nlapiLogExecution('DEBUG', 'o_data_employeeProfile', JSON.stringify(o_data_employeeProfile));
							for (var count = 0 ; count < o_data_employeeProfile.length ; count++)
							{
								var rec = nlapiCreateRecord('customrecord_fu_emp_experience_data',{recordmode: 'dynamic'});
								var o_data_employeeFields = o_data_employeeProfile[count];
								
								var start_date = formatDate(o_data_employeeProfile[count].Profile.Start_Date);
								var end_date = formatDate(o_data_employeeProfile[count].Profile.End_Date);
								var employer = o_data_employeeProfile[count].Profile.Employer_Name;
								nlapiLogExecution('debug','Data-Check',JSON.stringify(o_data_employeeFields));
								rec.setFieldValue('custrecord_fu_emp_name',searchemployee);
								rec.setFieldValue('custrecord_fu_exp_start_date',start_date);
								rec.setFieldValue('custrecord_fu_exp_end_date',end_date);
								rec.setFieldValue('custrecord_fu_exp_employer_name',employer);
								var id = nlapiSubmitRecord(rec , false , true);
								nlapiLogExecution('debug','XML data','Record id:' +id);
							}
						}else{
							
							var rec = nlapiCreateRecord('customrecord_fu_emp_experience_data',{recordmode: 'dynamic'});
							nlapiLogExecution('debug','Data-Check',JSON.stringify(o_data_employeeProfile));
							var start_date = formatDate(o_data_employeeProfile.Profile.Start_Date);
								var end_date = formatDate(o_data_employeeProfile.Profile.End_Date);
								var employer = o_data_employeeProfile.Profile.Employer_Name;
							rec.setFieldValue('custrecord_fu_emp_name',searchemployee);
								rec.setFieldValue('custrecord_fu_exp_start_date',start_date);
								rec.setFieldValue('custrecord_fu_exp_end_date',end_date);
								rec.setFieldValue('custrecord_fu_exp_employer_name',employer);
							var id = nlapiSubmitRecord(rec , false , true);
							nlapiLogExecution('debug','XML data','Record id:' +id);
						}
		
		
		}  				
       
        
    } catch (err) {
        nlapiLogExecution('debug', 'We are in catch block', err);
		var get_emp_data = nlapiGetFieldValue('custrecord_fu_emp_exp_json');
		var emp_data = JSON.parse(get_emp_data);
	    var personNumber = emp_data.Employee.PersonNumber;
		var pNumber = JSON.stringify(personNumber);
		pNumber.split('.');
		pNumber = pNumber.replace(/[^0-9 ]/g, "");
		nlapiSetFieldValue('custrecord_fi_fusion_id',pNumber);
		err += ' ' + 'Employee ID:-' + personNumber;
        nlapiSetFieldValue('custrecord_fi_json_error_log', err);
        return err;
    }
}
}

function formatDate(fusionDate) {

    if (fusionDate) {
        var datePart = fusionDate.split('T')[0];
        var dateSplit = datePart.split('-');


        // MM/DD/ YYYY
        var netsuiteDateString = dateSplit[1] + "/" + dateSplit[2] + "/" +
            dateSplit[0];
        return netsuiteDateString;
    }
    return "";
}

function searchEmployeeRec(empid) {

    var emp_internalID = '';
     if (empid) {
        var empSearch = nlapiSearchRecord('employee', null, [
            new nlobjSearchFilter('custentity_fusion_empid', null,
                'is', empid),
            new nlobjSearchFilter('custentity_employee_inactive',
                null, 'is', 'F')
        ], [new nlobjSearchColumn(
            'internalid')]);
        if (_logValidation(empSearch))
            emp_internalID = empSearch[0].getValue('internalid');
    }

    return emp_internalID;
}

function getAllFusionFieldMapping(type) {

    var filters = Array();
    filters.push(new nlobjSearchFilter('custrecord_ns_fusion_type', null, 'is', type));
    return nlapiSearchRecord(
        'customrecord519',
        null, filters, [new nlobjSearchColumn('custrecord_ns_fusion_fldtype'),
            new nlobjSearchColumn('custrecord42'),
            new nlobjSearchColumn('custrecord_ns_fusion_listtype'),
            new nlobjSearchColumn('custrecord_ns_fusion_fldname')
        ]);
}

function getDropDownData() {
    return searchRecord('customrecord_fi_drop_down_mapping', null, [new nlobjSearchFilter('isinactive', null, 'is', 'F')], [
        new nlobjSearchColumn('custrecord_fi_fusion_value'),
        new nlobjSearchColumn('custrecord_fi_dd_list_type'),
        new nlobjSearchColumn('custrecord_fi_netsuite_id')
    ]);
}


function _logValidation(value) {
    if (value != null && value != '' && value != undefined && value.toString() != undefined && value.toString() != 'NaN' && value != NaN && value != 'undefined' && value != ' ') {
        return true;
    } else {
        return false;
    }
}