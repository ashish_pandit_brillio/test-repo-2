//BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
    /*
     Script Name:SUT_Move_Invoice_draft_report.js
     Author:Sai Saranya
     Date:10-10-2018
     Description:
     Script Modification Log:
     -- Date --			-- Modified By --				--Requested By--				-- Description --
     10-04-2020			Praveena 						Deepak							Changed internalid and id of searches and logic added to avoid amount is zero when there is no bill rate
     Below is a summary of the process controls enforced by this script file.  The control logic is described
     more fully, below, in the appropriate function headers and code blocks.
     SUITELET
     - suiteletFunction(request, response )
     SUB-FUNCTIONS
     - The following sub-functions are called by the above core functions in order to maintain code
     modularization:
     - NOT USED
     */
}
//END SCRIPT DESCRIPTION BLOCK  ====================================



//END GLOBAL VARIABLE BLOCK  =======================================


//BEGIN SUITELET ==================================================
var d_start_wk_1 = null;
var d_start_wk_2 = null;
var d_start_wk_3 = null;
var d_start_wk_4 = null;
var d_start_wk_5 = null;

var d_end_wk_1 = null;
var d_end_wk_2 = null;
var d_end_wk_3 = null;
var d_end_wk_4 = null;
var d_end_wk_5 = null;

function GenerateInvoiceDraftReport(request, response) //
{
    try //
    {
        var a_employeearray = new Array();

        if (request.getMethod() == 'GET') //
        {
            var s_period = '';
            var form = nlapiCreateForm('Move Invoice Draft Report');
            var oldvalues = false;
            var flag = 0;
            form.setScript('customscript_move_draft_fr_invoice');
			var contextObj = nlapiGetContext();
			var beginUsage = contextObj.getRemainingUsage();
			var fromdate = form.addField('custpage_fromdate', 'Date', 'Start Date :');
            var Todate = form.addField('custpage_todate', 'Date', 'End Date :');

            var s_startdate = request.getParameter('custpage_fromdate');
			var s_enddate = request.getParameter('custpage_todate');
            //nlapiLogExecution('DEBUG', 'GET Search results ', ' s_enddate =' + s_enddate)
            var sublist1 = form.addSubList('record', 'list', 'Draft Report');
            var id = request.getParameter('id');
            if (id == 'Export') //
            {
                var param = request.getParameter('param');
                var datesplit = param.toString().split('@')
                s_startdate = datesplit[0]
               
            }

            if (s_startdate == null) //
            {
                oldvalues = false;
            } else //
            {
                oldvalues = true;
            }

            if (oldvalues) // Means start date is selected
            {
                // Add Export Button to form and set on click function to it
                form.addButton('custombutton', 'Export As CSV', 'fxn_generatePDF_For_managers_Move(\'' + s_startdate + '\',\' ' + s_enddate + ' \');');

                var id = request.getParameter('id');
                if (id == 'Export') //
                {
                    var param = request.getParameter('param');
                    var datesplit = param.toString().split('@')
                    s_startdate = datesplit[0]
					var param_enddate = request.getParameter('enddate');
					var enddatesplit = param_enddate.toString().split('@')
					s_enddate = enddatesplit[0]
                }

                var s_startdate1 = s_startdate
                s_startdate1 = nlapiStringToDate(s_startdate1)
                var enddate1 = nlapiStringToDate(s_enddate)
				s_startdate1 = nlapiDateToString(s_startdate1)
				enddate1 = nlapiDateToString(enddate1)
				d_start_wk_1 =s_startdate1 ;
				d_end_wk_1 = enddate1;
				var calculateSundays_new = calculateSundays(s_startdate1,enddate1)
				var enddate_new = calculateSundays_new[0]
                //nlapiLogExecution('DEBUG', 'GET results ', ' enddate_new=' + enddate_new)
				var SearchEmployeeData1 = new Array()
                SearchEmployeeData1 = SearchEmployeeData(s_startdate1,enddate1)
                //nlapiLogExecution('DEBUG', 'GET results ', ' enddate1=' + enddate1 + '===' + s_startdate1)
                nlapiLogExecution('DEBUG', 'GET results ', ' SearchEmployeeData1=' + SearchEmployeeData1.length)
			//	var timesheetdata_WK_1 = SearchTimeData_New(d_start_wk_1, d_end_wk_1);
            //    nlapiLogExecution('DEBUG', 'GET results ', ' timesheetdata_WK_1=' + timesheetdata_WK_1.length)
				var setvalue = setValuesublist_New(sublist1, SearchEmployeeData1, enddate_new, s_startdate1, calculateSundays_new, enddate1);
            }

            if (oldvalues) //
            {
                fromdate.setDefaultValue(s_startdate);
				Todate.setDefaultValue(s_enddate);
                
            }
            //nlapiLogExecution('DEBUG', 'GET results ', 'id=' + id)

            if (id != 'Export') //
            {
                form.addSubmitButton('Submit');
                response.writePage(form);
            } else //
            {
                down_excel_function(sublist1, s_startdate1,enddate1);
            }
        } else // If method of request is Post
        {
            var date = request.getParameter('custpage_fromdate');
			var to_date = request.getParameter('custpage_todate');
			var params = new Array();
            params['custpage_fromdate'] = date;
			params['custpage_todate']= to_date;
			nlapiSetRedirectURL('SUITELET', 'customscript_sut_move_draft_report', 'customdeploy1', false, params);
        }
    } catch (e) //
    {
        nlapiLogExecution('ERROR', 'Try Catch Error ', 'Exception = ' + e)
    }
}

function setValuesublist_New(sublist1, SearchEmployeeData,s_enddate, s_startdate, calculate, enddate1)
{
	//var s_FusionId = sublist1.addField('custevent_fusid', 'text', 'Fusion Id');
	var s_firstName = sublist1.addField('custevent_firstname', 'text', 'Employee Name');
  	var s_project = sublist1.addField('custevent_project', 'text', 'Project');
	var s_bill_rte = sublist1.addField('custevent_billrate','float','Bill Rate');
	//var getdates= getdates(s_startdate,s_enddate);
	var new_enddate = '';
	var linenumber =1;
	var emp_unique_array= new Array();
  var new_enddate= nlapiStringToDate(s_startdate); 
   var new_enddate_date= nlapiStringToDate(enddate1); 
	for( new_enddate ; new_enddate <= new_enddate_date; )
	{
		var s_new_enddate = (new_enddate);
		var s_month_name = s_new_enddate.getDate();
		var s_mnth_name_indx = s_new_enddate.getMonth();
		s_month_name = s_month_name+'_'+s_mnth_name_indx;
		var day = s_new_enddate.getDay();
		if(parseInt(day) != parseInt(0) && parseInt(day) != parseInt(6))
		{
			sublist1.addField(''+s_month_name, 'float', ''+nlapiDateToString(new_enddate));
		}
		new_enddate = nlapiAddDays(s_new_enddate,1);
		//new_enddate = nlapiDateToString(new_enddate);
	}	
	var total = sublist1.addField('custevent_total_hr','float','Total');
	var amount = sublist1.addField('custevent_total_amount','float','Amount');
	
	for(var emp_indx= 0; emp_indx < SearchEmployeeData.length; emp_indx++)
	{
		var emp_columns= SearchEmployeeData[emp_indx].getAllColumns();
		var emp_id = SearchEmployeeData[emp_indx].getValue(emp_columns[0]);
		var emp_name = SearchEmployeeData[emp_indx].getValue(emp_columns[19]);
		var emp_name = SearchEmployeeData[emp_indx].getText(emp_columns[19]);
		var emp_billrate = SearchEmployeeData[emp_indx].getValue(emp_columns[20])
	//	var emp_id =1816;
		
		if(emp_unique_array.indexOf(emp_id)<0)
		{	
			//var project_array = new Array();
			var timesheetdata_WK_1= SearchTimeData_New(s_startdate,enddate1,emp_id);
			emp_unique_array.push(emp_id);
			var total_hrs= 0;
			if(_logValidation(timesheetdata_WK_1))
			{
				var hrs_duration =0;
				var date_ts_flag = '';
				var bill_rate = 0;
				for(var time_indx = 0; time_indx < timesheetdata_WK_1.length; time_indx++)
				{
					var columns= timesheetdata_WK_1[time_indx].getAllColumns();//
					var projectid = timesheetdata_WK_1[time_indx].getText(columns[3]);
					//project_array.push(projectid);
					bill_rate = timesheetdata_WK_1[time_indx].getValue(columns[10]);
					sublist1.setLineItemValue('custevent_firstname', linenumber, timesheetdata_WK_1[time_indx].getText(columns[2]));
					sublist1.setLineItemValue('custevent_project', linenumber, timesheetdata_WK_1[time_indx].getText(columns[3]));
					sublist1.setLineItemValue('custevent_billrate', linenumber, timesheetdata_WK_1[time_indx].getValue(columns[10]));
					var item_selected = timesheetdata_WK_1[time_indx].getValue(columns[4]);
					
					
					var date_ts = timesheetdata_WK_1[time_indx].getValue(columns[0]);
					
					if(date_ts == date_ts_flag)
					{
						total_hrs = total_hrs - parseInt(hrs_duration);
						hrs_duration = parseInt(hrs_duration) + parseInt(timesheetdata_WK_1[time_indx].getValue(columns[6]));
						
					}
					else
					{
						hrs_duration = (timesheetdata_WK_1[time_indx].getValue(columns[6]));
					}
					if(item_selected == 'Holiday'|| item_selected == 'Leave')
					{
						hrs_duration = 0;
					}
					var new_day = nlapiStringToDate(date_ts);
					date_ts = nlapiStringToDate(date_ts);
					var get_mnth= date_ts.getMonth();
					date_ts = date_ts.getDate();
					
					date_ts = date_ts+'_'+get_mnth;
					var day = new_day.getDay();
					if(parseInt(day) != parseInt(0) && parseInt(day) != parseInt(6))
					{
						total_hrs = parseInt(total_hrs) + parseInt(hrs_duration);
						sublist1.setLineItemValue(''+date_ts, linenumber,parseInt(hrs_duration));
					}
					date_ts_flag =  timesheetdata_WK_1[time_indx].getValue(columns[0]);
			
				}
				sublist1.setLineItemValue('custevent_total_hr', linenumber,total_hrs);
				var amount= parseFloat(bill_rate) * parseFloat(total_hrs);
				amount=amount? amount : 0;//Added by Praveena to avoid blank total amount
				sublist1.setLineItemValue('custevent_total_amount', linenumber,amount);
				linenumber++;
			}
			else
			{
				
				sublist1.setLineItemValue('custevent_firstname', linenumber,  emp_name);
              
             //sublist1.setLineItemValue('custevent_project', linenumber,  SearchEmployeeData[emp_indx].getText(columns[7]));
				sublist1.setLineItemValue('custevent_project', linenumber,  SearchEmployeeData[emp_indx].getText("company",null,"GROUP"));
				sublist1.setLineItemValue('custevent_billrate', linenumber, emp_billrate);
				sublist1.setLineItemValue('custevent_total_hr', linenumber,total_hrs);
				sublist1.setLineItemValue('custevent_total_amount', linenumber,'0');
				
				linenumber++;
			}
		}
	}	//
	
}


function SearchEmployeeData(s_startdate, s_enddate) //
{
    var e_column = new Array();
    var e_filters = new Array();
    var LastfetchedInternalid_T = 0;
    var searchresultFinal_T = new Array();
    var searchresult_T = new Array();
	var search = nlapiLoadSearch('resourceallocation', 'customsearch_non_submitted_ts_by_alloc_5');
//	var columns =search.getColumns();
	e_filters[0] = new nlobjSearchFilter('startdate', null, 'notafter', s_enddate); ///is not after 9/28.14
	e_filters[1] = new nlobjSearchFilter('enddate', null, 'notbefore', s_startdate); ////is not before 1sept
	e_filters[2] = new nlobjSearchFilter('internalidnumber', 'employee', 'greaterthan', LastfetchedInternalid_T); ////is not before 1sept
	searchresult_T = searchRecord('resourceallocation', 'customsearch_non_submitted_ts_by_alloc_5', e_filters, null);
	if (searchresult_T != null && searchresult_T != undefined && searchresult_T != '') //
	{
        return searchresult_T;
	}
     
}

function SearchTimeData(s_startdate, s_enddate) //
{
    var LastfetchedInternalid_T = 0;
    var searchresultFinal_T = new Array();
    var searchresult_T = new Array();

    do //
    {
        var a_filters = new Array();
        a_filters[0] = new nlobjSearchFilter('date', null, 'onorbefore', s_enddate);
        a_filters[1] = new nlobjSearchFilter('date', null, 'onorafter', s_startdate);
        a_filters[2] = new nlobjSearchFilter('internalidnumber', 'employee', 'greaterthan', LastfetchedInternalid_T); ////is not before 1sept
        var searchresult_T = nlapiSearchRecord('timebill', 'customsearch_salnonstdrptts_3_all_2', a_filters, null);

        if (searchresult_T != null && searchresult_T != undefined && searchresult_T != '') //
        {
            //nlapiLogExecution('DEBUG', 'researchcodes', 'searchresult_T == ' + searchresult_T.length);
            var temp_emp = '';
            var emp = '';

            for (var i = 0; i < searchresult_T.length; i++) //
            {
                var a_search_empdata = searchresult_T[i];
                var all_columns = a_search_empdata.getAllColumns();
                emp = a_search_empdata.getValue(all_columns[2]);
                //nlapiLogExecution('DEBUG', 'SearchTimeData', 'i : ' + i + ' emp ==' + emp);

                if (emp != null) //
                {
                    if (i == 0) //
                    {
                        temp_emp = emp;
                    }

                    LastfetchedInternalid_T = temp_emp;
                    //nlapiLogExecution('DEBUG', 'SearchData', 'i : ' + i + ' temp_emp ==' + temp_emp);
                    if (emp == temp_emp) //if employee id is same then add to final array
                    {
                        searchresultFinal_T.push(searchresult_T[i]);
                    } else //else employee id does nt match
                    {
                        //nlapiLogExecution('DEBUG', 'SearchData', 'Emp change row number : ' + i);
                        if (i > 950) //
                        {
                            //nlapiLogExecution('DEBUG', 'SearchData', 'i==' + i);
                            //nlapiLogExecution('DEBUG', 'SearchData', 'LastfetchedInternalid_T==' + LastfetchedInternalid_T);
                            break;
                        } else //
                        {
                            searchresultFinal_T.push(searchresult_T[i]);
                            temp_emp = emp;
                            LastfetchedInternalid_T = temp_emp;
                        }
                    }
                }
                //searchresultFinal_T.push(searchresult_T[i]);
                //nlapiLogExecution('DEBUG', 'researchcodes', 'lastid==' + a_search_empdata.getValue('internalid', null, 'group'));
            }
            //nlapiLogExecution('DEBUG', 'researchcodes', 'lastid==' + a_search_empdata.getValue('internalid', null, 'group'));
            //LastfetchedInternalid_T = searchresult_T[searchresult_T.length - 1].getValue('internalid', 'employee', 'group');
            //nlapiLogExecution('DEBUG', 'researchcodes', 'LastfetchedInternalid_T==' + LastfetchedInternalid_T);
        }
        //var searchresult_T = nlapiSearchRecord('timebill', 'customsearch_salnonstdrptts_3_all', a_filters, null);
        //return searchresult_T
    }
    while (searchresult_T != null && searchresult_T != undefined && searchresult_T != '') {

    }
    return searchresultFinal_T;

}

function SearchTimeData_New(s_startdate, s_enddate, empid) //
{
    var LastfetchedInternalid_T = 0;
    var searchresultFinal_T = new Array();
    var searchresult_T = new Array();
	var a_filters = new Array();
	a_filters[a_filters.length] = new nlobjSearchFilter('date', null, 'onorafter', s_startdate);
    a_filters[a_filters.length] = new nlobjSearchFilter('date', null, 'onorbefore', s_enddate);
    a_filters[a_filters.length] = new nlobjSearchFilter('approvalstatus', null, 'anyof', ['2','3']);
	a_filters[a_filters.length] = new nlobjSearchFilter('employee', null, 'anyof', empid);
	
    // a_filters[a_filters.length] = new nlobjSearchFilter('internalidnumber', 'employee', 'greaterthan', LastfetchedInternalid_T); ////is not before 1sept
    var searchresult_T = nlapiSearchRecord('timebill', 'customsearch_move_invoice_re', a_filters, null);
	
		return searchresult_T;
}

var manager_email_Array = new Array();

function get_Manager_Email() //
{
    var search_result = nlapiSearchRecord('job', 'customsearch_manager_with_email_3', null, null);

    if (_logValidation(search_result)) //
    {
        for (var i = 0; i < search_result.length; i++) //
        {
            var result = search_result[i];
            var all_columns = result.getAllColumns();

            manager_email_Array[i] = result.getValue(all_columns[1]) + "##" + result.getValue(all_columns[2]);
        }
        nlapiLogExecution('DEBUG', 'get_Manager_Email', 'manager_email_Array count : ' + manager_email_Array.length);
    }
}

/*
 Function name :-getWeekNumber()
 Parameter :- date
 return type :- returns week number of given year
 */
function getWeekNumber(d) {
    //Copy date so don't modify original
    d = new Date(+d);
    d.setHours(0, 0, 0);
    //Set to nearest Thursday: current date + 4 - current day number
    //Make Sunday's day number 7
    d.setDate(d.getDate() + 4 - (d.getDay() || 7));
    //Get first day of year
    var yearStart = new Date(d.getFullYear(), 0, 1);
    //Calculate full weeks to nearest Thursday
    var weekNo = Math.ceil((((d - yearStart) / 86400000) + 1) / 7)
    //nlapiLogExecution('DEBUG', 'beforeLoad_CalculateWeek', 'weekNo=' + weekNo);
    //Return array of year and week number
    return weekNo;
}


//
function down_excel_function(sublist1, s_startdate,enddate1)
{
	try
	{
		var strVar1= '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:ss="urn:schemas-microsoft-com:office:spreadsheet" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40">'+
		'<head>'+
		'<meta http-equiv=Content-Type content="text/html; charset=windows-1252"/>'+
        '<meta name=ProgId content=Excel.Sheet/>'+
        '<meta name=Generator content="Microsoft Excel 11"/>'+
        '<!--[if gte mso 9]><xml>'+
        '<x:excelworkbook>'+
        '<x:excelworksheets>'+
        '<x:excelworksheet=sheet1>'+
        '<x:name>** ESTIMATE FILE**</x:name>'+
        '<x:worksheetoptions>'+
        '<x:selected></x:selected>'+
        '<x:freezepanes></x:freezepanes>'+
        '<x:frozennosplit></x:frozennosplit>'+
        '<x:splithorizontal>** FROZEN ROWS + 1 **</x:splithorizontal>'+
        '<x:toprowbottompane>** FROZEN ROWS + 1 **</x:toprowbottompane>'+
        '<x:splitvertical>** FROZEN COLUMNS + 1 **</x:splitvertical>'+
        '<x:leftcolumnrightpane>** FROZEN COLUMNS + 1**</x:leftcolumnrightpane>'+
		    '<x:activepane>0</x:activepane>'+// 0
		    '<x:panes>'+
			'<x:pane>'+
			'<x:number>3</x:number>'+
			'</x:pane>'+
			'<x:pane>'+
		    '<x:number>1</x:number>'+
		    '</x:pane>'+
		    '<x:pane>'+
			'<x:number>2</x:number>'+
        '</x:pane>'+
        '<x:pane>'+
        '<x:number>0</x:number>'+//1
        '</x:pane>'+
        '</x:panes>'+
        '<x:protectcontents>False</x:protectcontents>'+
        '<x:protectobjects>False</x:protectobjects>'+
        '<x:protectscenarios>False</x:protectscenarios>'+
        '</x:worksheetoptions>'+
        '</x:excelworksheet>'+
		'</x:excelworksheets>'+
        '<x:protectstructure>False</x:protectstructure>'+
        '<x:protectwindows>False</x:protectwindows>'+
		'</x:excelworkbook>'+
        
        // '<style>'+
			
			//-------------------------------------
        '</x:excelworkbook>'+
        '</xml><![endif]-->'+
         '<style>'+
		'p.MsoFooter, li.MsoFooter, div.MsoFooter'+
		'{ margin:0in; margin-bottom:.0001pt; mso-pagination:widow-orphan; tab-stops:center 3.0in right 6.0in; font-size:12.0pt;}'+
		'<style>'+

		'<!-- /* Style Definitions */'+

		//'@page Section1'+
		//'{ size:8.5in 11.0in; margin:1.9cm 1.27cm 1.9cm 1.27cm; mso-header-margin:.5in; mso-footer-margin:.5in; mso-title-page:yes; mso-header: h1; mso-footer: f1; mso-first-header: fh1; mso-first-footer: ff1; mso-paper-source:0;}'+

		'div.Section1'+
		'{ page:Section1;}'+

		'table#hrdftrtbl'+
		'{ margin:0in 0in 0in 900in; width:1px; height:1px; overflow:hidden; font-family:Arial;}-->'+

		'</style>'+
		
		
		'</head>'+
		

		'<body>'+
		
		'<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">'
		
		var strVar2 = '';    
		strVar2 += "<table width=\"100%\" border='1'>";
		strVar2 += "<tr>";
		strVar2 += "<td Width=\"10%\" style='text-align:left;font-weight:bold;' font-size=\"10\" \>Employee Name</td>";
		strVar2 += "<td Width=\"10%\" style='text-align:left;font-weight:bold;' font-size=\"10\" \>Project</td>";
		strVar2 += "<td Width=\"10%\" style='text-align:left;font-weight:bold;' font-size=\"10\" \>Bill Rate</td>";
		
		for(var new_enddate = nlapiStringToDate(s_startdate); new_enddate <= nlapiStringToDate(enddate1); )
		{
			var s_new_enddate = (new_enddate);
			var s_month_name = s_new_enddate.getDate();
			var day = s_new_enddate.getDay();
			if(parseInt(day) != parseInt(0) && parseInt(day) != parseInt(6))
			{
				strVar2 += "<td Width=\"10%\" style='text-align:left;font-weight:bold;' font-size=\"10\" \>"+nlapiDateToString(new_enddate)+"</td>"
			}
			new_enddate = nlapiAddDays(s_new_enddate,1);
			//new_enddate = nlapiDateToString(new_enddate);
		}	
		strVar2 += "<td Width=\"10%\" style='text-align:left;font-weight:bold;' font-size=\"10\" \>Total</td>";
		strVar2 += "<td Width=\"10%\" style='text-align:left;font-weight:bold;' font-size=\"10\" \>Amount</td>";
		strVar2 += "<\/tr>";
		
		if (sublist1) //
		{
			var list_length = sublist1.getLineItemCount();
			for (var i = 1; i <= list_length; i++) //
			{
				var s_employee = sublist1.getLineItemValue('custevent_firstname', i);
				var s_project = sublist1.getLineItemValue('custevent_project', i);
				var s_total = sublist1.getLineItemValue('custevent_total_hr', i);
				var s_amount = sublist1.getLineItemValue('custevent_total_amount', i);
				var s_billrate = sublist1.getLineItemValue('custevent_billrate', i);
				
				nlapiLogExecution('audit','s_employee:-'+s_employee,'s_project:-'+s_project);
				strVar2 += "<tr>";
				strVar2 += "<td Width=\"10%\" style='text-align:left;' font-size=\"10\" \>" + s_employee + "</td>";
				strVar2 += "<td Width=\"10%\" style='text-align:left;' font-size=\"10\" \>" + s_project +  "</td>";
				strVar2 += "<td Width=\"10%\" style='text-align:left;' font-size=\"10\" \>" + s_billrate +  "</td>";
				for(var new_enddate_excel = nlapiStringToDate(s_startdate); new_enddate_excel <= nlapiStringToDate(enddate1); )
				{
					var s_excel_new_enddate = (new_enddate_excel);
					var s_month_name = s_excel_new_enddate.getDate();
					var day = s_excel_new_enddate.getDay();
					var s_mnth_name_indx = s_excel_new_enddate.getMonth();
					s_month_name = s_month_name+'_'+s_mnth_name_indx;
					var s_hrs = sublist1.getLineItemValue(''+s_month_name,i);
					if(!(_logValidation(s_hrs)))
					{
						s_hrs = '';
					}
					if(parseInt(day) != parseInt(0) && parseInt(day) != parseInt(6))
					{
						strVar2 += "<td Width=\"10%\" style='text-align:left;' font-size=\"10\" \>" + s_hrs + "</td>";
					}
					
					new_enddate_excel = nlapiAddDays(s_excel_new_enddate,1);
					//new_enddate_excel = nlapiDateToString(new_enddate_excel);
					
				}
				strVar2 += "<td Width=\"10%\" style='text-align:left;' font-size=\"10\" \>" + s_total + "</td>";
				strVar2 += "<td Width=\"10%\" style='text-align:left;' font-size=\"10\" \>" + s_amount + "</td>";
				strVar2 += "<\/tr>";
			}
		}
		else
		{
			//NO RESULTS FOUND
		}
					
		strVar2 += "<\/table>";			
		strVar1 = strVar1 + strVar2;
		var file = nlapiCreateFile('Move InvoiceDraft.xls', 'XMLDOC', strVar1);
		nlapiLogExecution('debug','file:- ',file);
		response.setContentType('XMLDOC','Move Invoice Draft.xls');
		response.write( file.getValue() );	 
		
	}
	catch(err)
	{
		nlapiLogExecution('ERROR','ERROR MESSAGE:- ',err);
	}
}
//
function calculateSundays(s_startdate, s_enddate) {
    var sundayarray = new Array();
    //nlapiLogExecution('DEBUG', 'suitlet_Po_ClosedStatus', ' s_startdate-->' + s_startdate);
    //////s_startdate= s_enddate
    var date = nlapiStringToDate(s_startdate)
    //nlapiLogExecution('DEBUG', 'suitlet_Po_ClosedStatus', ' date-->' + date);
    var day = date.getDay()
    //nlapiLogExecution('DEBUG', 'suitlet_Po_ClosedStatus', ' day-->' + day);
    var addday = getdays(day)
    //nlapiLogExecution('DEBUG', 'suitlet_Po_ClosedStatus', ' addday-->' + addday);

    for (var i = 0; i < 5; i++) {
        s_startdate = nlapiStringToDate(s_startdate)
        if (i == 0) {

            s_startdate = nlapiAddDays(s_startdate, addday)
            s_startdate = nlapiDateToString(s_startdate)
            sundayarray.push(s_startdate)
        } else {
            s_startdate = nlapiAddDays(s_startdate, 7)
            s_startdate = nlapiDateToString(s_startdate)
            sundayarray.push(s_startdate)
        }
    }

    //nlapiLogExecution('DEBUG', 'suitlet_Po_ClosedStatus', ' sundayarray-->' + sundayarray);
    return sundayarray;
}

function Cal_Timesheetdate(s_startdate, i_calenderweek) {
    //nlapiLogExecution('DEBUG', 'suitlet_Po_ClosedStatus', ' s_startdate-->' + s_startdate);
    //nlapiLogExecution('DEBUG', 'suitlet_Po_ClosedStatus', ' i_calenderweek-->' + i_calenderweek);
    var i_timsheetdate = ''
    i_calenderweek = nlapiStringToDate(i_calenderweek)
    i_timsheetdate = nlapiAddDays(i_calenderweek, 7)
    i_timsheetdate = nlapiDateToString(i_timsheetdate)
    return i_timsheetdate;


}

function Sundayswithweekposition(s_startdate, s_enddate) {
    //nlapiLogExecution('DEBUG', 'suitlet_Po_ClosedStatus', ' s_startdate-->' + s_startdate);
    var position = new Array();

    var date = nlapiStringToDate(s_startdate)
    //nlapiLogExecution('DEBUG', 'suitlet_Po_ClosedStatus', ' date-->' + date);
    var day = date.getDay()
    //nlapiLogExecution('DEBUG', 'suitlet_Po_ClosedStatus', ' day-->' + day);
    var addday = getdays(day)
    //nlapiLogExecution('DEBUG', 'suitlet_Po_ClosedStatus', ' addday-->' + addday);
    for (var i = 0; i < 4; i++) {
        s_startdate = nlapiStringToDate(s_startdate)
        if (i == 0) {

            s_startdate = nlapiAddDays(s_startdate, addday)
            s_startdate = nlapiDateToString(s_startdate)

            position.push(s_startdate + "##" + (i + 1))
            //position.push(s_startdate + "##" + (4 - i))
        } else {
            s_startdate = nlapiAddDays(s_startdate, 7)
            s_startdate = nlapiDateToString(s_startdate)
            position.push(s_startdate + "##" + (i + 1))
            //position.push(s_startdate + "##" + (4 - i))
        }
    }

    //nlapiLogExecution('DEBUG', 'suitlet_Po_ClosedStatus', ' position-->' + position);
    return position;
}

function getdays(day) {
    //nlapiLogExecution('DEBUG', 'suitlet_Po_ClosedStatus', ' day-->' + day);
    var x = '';
    switch (day) {
        case 0:
            x = "7";
            break;
        case 1:
            x = "6";
            break;
        case 2:
            x = "5";
            break;
        case 3:
            x = "4";
            break;
        case 4:
            x = "3";
            break;
        case 5:
            x = "2";
            break;
        case 6:
            x = "1";
            break;

    }
    //nlapiLogExecution('DEBUG', 'suitlet_Po_ClosedStatus', ' x-->' + x);
    return x;
}

function EmployeeSearch() {


    var a_filters = new Array();



    var searchresult = nlapiSearchRecord('employee', 'customsearch2381', a_filters, null);
    return searchresult

}

function _logValidation(value) {
    if (value != null && value != '' && value != undefined && value.toString() != 'NaN' && value != NaN) {
        return true;
    } else {
        return false;
    }
}