// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
/*
   	Script Name : UES_SetTotalPosition.js
	Author      : Ashish Pandit
	Date        : 29 April 2018
    Description : User Event to set Total Position  


	Script Modification Log:

	-- Date --			-- Modified By --				--Requested By--				-- Description --


Below is a summary of the process controls enforced by this script file.  The control logic is described
more fully, below, in the appropriate function headers and code blocks.


     BEFORE LOAD
		- beforeLoadRecord(type)



     BEFORE SUBMIT
		- beforeSubmitRecord(type)


     AFTER SUBMIT
		- afterSubmitRecord(type)



     SUB-FUNCTIONS
		- The following sub-functions are called by the above core functions in order to maintain code
            modularization:

               - NOT USED

*/
}
// END SCRIPT DESCRIPTION BLOCK  ====================================



// BEGIN GLOBAL VARIABLE BLOCK  =====================================
{
	//  Initialize any Global Variables, in particular, debugging variables...

}
// END GLOBAL VARIABLE BLOCK  =======================================



// BEGIN BEFORE LOAD ==================================================

function afterSubmitSetTotalPosition(type)
{
	if(type == 'create')
	{
		try
		{
			var recId = nlapiGetRecordId();
			var recType = nlapiGetRecordType();
			var recordObject = nlapiLoadRecord(recType,recId);
			
			var i_position = recordObject.getFieldValue('custrecord_fulfill_plan_position');
			var i_oppId = recordObject.getFieldValue('custrecord_fulfill_plan_opp_id');
			nlapiLogExecution('Debug','i_position ',i_position);
			if(i_oppId)
			{
				var customrecord_fulfillment_dashboard_dataSearch = nlapiSearchRecord("customrecord_fulfillment_dashboard_data",null,
				[
				   ["custrecord_fulfill_dashboard_opp_id","anyof",i_oppId]
				], 
				[
				   new nlobjSearchColumn("internalid")
				]
				);
				if(customrecord_fulfillment_dashboard_dataSearch)
				{
					nlapiLogExecution('Debug','customrecord_fulfillment_dashboard_dataSearch length ',customrecord_fulfillment_dashboard_dataSearch.length);
					var i_sfdcRecId = customrecord_fulfillment_dashboard_dataSearch[0].getValue('internalid');
					nlapiLogExecution('Debug','i_sfdcRecId ',i_sfdcRecId);
					var recObj = nlapiLoadRecord('customrecord_fulfillment_dashboard_data',parseInt(i_sfdcRecId));
					var i_positionDash = recObj.getFieldValue('custrecord_fulfill_dashboard_total_pos');
					if(!i_positionDash)
					{
						i_positionDash = 0;
					}
					nlapiLogExecution('Debug','i_positionDash  ',i_positionDash);
					var totalPositions = parseInt(i_positionDash) + parseInt(i_position);
					nlapiLogExecution('Debug','totalPositions  ',parseInt(totalPositions));
					var submitId = nlapiSubmitField('customrecord_fulfillment_dashboard_data',parseInt(i_sfdcRecId),'custrecord_fulfill_dashboard_total_pos',totalPositions);
				}
			}
		}
		catch(error)
		{
			nlapiLogExecution('Debug','Error ',error);
		}
		
	}
	if(type=='edit')
	{
		try
		{
			var recId = nlapiGetRecordId();
			var recType = nlapiGetRecordType();
			var recordObject = nlapiLoadRecord(recType,recId);
			
			var i_position = recordObject.getFieldValue('custrecord_fulfill_plan_position');
			var i_oppId = recordObject.getFieldValue('custrecord_fulfill_plan_opp_id');
			var fulfil_status = recordObject.getFieldValue('custrecord_fulfill_plan_status');
			nlapiLogExecution('Debug','i_position ',i_position);
			
			/**************Cancelled Positions ************************/
			if(i_oppId && fulfil_status =='Cancelled')
			{
				var customrecord_fulfillment_dashboard_dataSearch = nlapiSearchRecord("customrecord_fulfillment_dashboard_data",null,
				[
				   ["custrecord_fulfill_dashboard_opp_id","anyof",i_oppId]
				], 
				[
				   new nlobjSearchColumn("internalid")
				]
				);
				if(customrecord_fulfillment_dashboard_dataSearch)
				{
					nlapiLogExecution('Debug','customrecord_fulfillment_dashboard_dataSearch length ',customrecord_fulfillment_dashboard_dataSearch.length);
					var i_sfdcRecId = customrecord_fulfillment_dashboard_dataSearch[0].getValue('internalid');
					nlapiLogExecution('Debug','i_sfdcRecId ',i_sfdcRecId);
					var recObj = nlapiLoadRecord('customrecord_fulfillment_dashboard_data',parseInt(i_sfdcRecId));
					var i_positionDash = recObj.getFieldValue('custrecord_fulfill_dashboard_total_pos');
					if(!i_positionDash)
					{
						i_positionDash = 0;
					}
					nlapiLogExecution('Debug','i_positionDash  ',i_positionDash);
					var totalPositions = parseInt(i_positionDash) -  parseInt(i_position);
					totalPositions = parseInt(totalPositions)>0?totalPositions:0;
					nlapiLogExecution('Debug','totalPositions  ',parseInt(totalPositions));
					var submitId = nlapiSubmitField('customrecord_fulfillment_dashboard_data',parseInt(i_sfdcRecId),'custrecord_fulfill_dashboard_total_pos',totalPositions);
				}
			}
			/**************Modifiying Positions ************************/
			if(i_oppId && fulfil_status =='Modified')
			{
				var oldRecord = nlapiGetOldRecord();
			    var i_OldPositions = oldRecord.getFieldValue('custrecord_fulfill_plan_position');
				var customrecord_fulfillment_dashboard_dataSearch = nlapiSearchRecord("customrecord_fulfillment_dashboard_data",null,
				[
				   ["custrecord_fulfill_dashboard_opp_id","anyof",i_oppId]
				], 
				[
				   new nlobjSearchColumn("internalid")
				]
				);
				if(customrecord_fulfillment_dashboard_dataSearch)
				{
					var i_sfdcRecId = customrecord_fulfillment_dashboard_dataSearch[0].getValue('internalid');
					var recObj = nlapiLoadRecord('customrecord_fulfillment_dashboard_data',parseInt(i_sfdcRecId));
					var i_positionDash = recObj.getFieldValue('custrecord_fulfill_dashboard_total_pos');
					if(!i_positionDash)
					{
						i_positionDash = 0;
					}
					nlapiLogExecution('Debug','i_positionDash  ',i_positionDash);
					if(i_position != i_OldPositions && _logValidation(i_OldPositions))
					{
						var i_differnce = parseInt(i_OldPositions) - parseInt(i_position);
						nlapiLogExecution('debug','i_differnce ',i_differnce);
						var totalPositions = parseInt(i_positionDash) -  parseInt(i_differnce);			
						nlapiLogExecution('debug','totalPositions ',totalPositions);

						/*if(i_differnce>0)
						{
							var totalPositions = parseInt(i_positionDash) -  parseInt(i_differnce);
						}
						else
						{
							var totalPositions = parseInt(i_positionDash) -  parseInt(i_differnce);
						}*/
					}
					else{
						totalPositions = i_positionDash;
					}
					totalPositions = parseInt(totalPositions)>0?totalPositions:0;
					nlapiLogExecution('Debug','totalPositions  ',parseInt(totalPositions));
					var submitId = nlapiSubmitField('customrecord_fulfillment_dashboard_data',parseInt(i_sfdcRecId),'custrecord_fulfill_dashboard_total_pos',totalPositions);
				}
			}
		}
		catch(error)
		{
			nlapiLogExecution('Debug','Error ',error);
		}
	}
}

// END BEFORE LOAD ====================================================


function _logValidation(value) 
{
 if(value != null && value!= 'null' && value != '' && value != undefined && value != undefined && value != 'undefined' && value.toString() != 'NaN' && value != NaN) 
 {
  return true;
 }
 else 
 { 
  return false;
 }
}
// END FUNCTION =====================================================
