// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
/*
   	Script Name:SUT_Vat_Statement_Screen.js
	Author:		Nikhil jain
	Company:	Aashna Cloudtech Pvt. Ltd.
	Date:		24 Nov 2015
	Description:display an vat statement in suitlet form


	Script Modification Log:

	-- Date --			-- Modified By --				--Requested By--				-- Description --



Below is a summary of the process controls enforced by this script file.  The control logic is described
more fully, below, in the appropriate function headers and code blocks.


     SUITELET
		- suiteletFunction(request, response)


     SUB-FUNCTIONS
		- The following sub-functions are called by the above core functions in order to maintain code
            modularization:

               - NOT USED

*/
}
// END SCRIPT DESCRIPTION BLOCK  ====================================



// BEGIN GLOBAL VARIABLE BLOCK  =====================================
{
	//  Initialize any Global Variables, in particular, debugging variables...




}
// END GLOBAL VARIABLE BLOCK  =======================================





// BEGIN SUITELET ==================================================

function suiteletFunction_vat_statement_screen(request, response)
{

	/*  Suitelet:

          - EXPLAIN THE PURPOSE OF THIS FUNCTION

		-

		FIELDS USED:

          --Field Name--				--ID--


	*/
	
	 if (request.getMethod() == 'GET') 
	 {
	 	var s_purchase_tax_name;
		var s_sales_tax_name;
		
	 	var fromDate = request.getParameter('fromDate')
    	var toDate = request.getParameter('toDate')
    	var taxcode = request.getParameter('taxcode')
		var i_taxTypeID = request.getParameter('i_taxTypeID')
		
		var i_AitGlobalRecId = SearchGlobalParameter();
		
		if (i_AitGlobalRecId != 0) 
		{
			var o_AitGloRec = nlapiLoadRecord('customrecord_indian_tax_glo_parameter', i_AitGlobalRecId);
			
			var s_purchase_tax_name = o_AitGloRec.getFieldValue('custrecord_ait_purchase_tax_name');
			
			var s_sales_tax_name = o_AitGloRec.getFieldValue('custrecord_sales_tax_name');
		
		}
		
		var form = nlapiCreateForm('Tax Statement')
		
		form.setScript('customscript_vatpaymentlistclient');
		
		form.addButton('custpage_importstatement', 'Export-PDF','import_tax_statement()');
		
		form.addButton('custpage_closestatment', 'Close','close_tax_statement()');
		
		 var o_taxType = form.addField('custpage_taxtype', 'select', 'Tax Type');
		 o_taxType.addSelectOption('1','Service Tax')
		 o_taxType.addSelectOption('2','Swachh Cess')
		 o_taxType.addSelectOption('6','RCM - Service Tax')
		 o_taxType.setDefaultValue(i_taxTypeID);
		 o_taxType.setDisplayType('hidden');
		
		var taxcodeobj = form.addField('custpage_taxcode','text','Tax Code')
		taxcodeobj.setDisplayType('hidden')
		taxcodeobj.setDefaultValue(taxcode)
		
		var fromdateObj = form.addField('custpage_fromdate','date','From Date')
		fromdateObj.setDefaultValue(fromDate)
		fromdateObj.setDisplayType('inline')
		
		var toDateobj = form.addField('custpage_todate','date','To Date')
		toDateobj.setDefaultValue(toDate)
		toDateobj.setDisplayType('inline')
		
		var purchasesublistObj = form.addSubList('purchaselist','list','Purchase','tab1') 
		var purchasetax = setPurchaseSubList(purchasesublistObj,form,request,response,fromDate,toDate,taxcode,i_taxTypeID);
		
		var salessublistObj = form.addSubList('saleslist','list','Sales','tab1')
		var salestax = setSalesSubList(salessublistObj,form,request,response,fromDate,toDate,taxcode,i_taxTypeID);
		
		if(s_purchase_tax_name != null && s_purchase_tax_name != '' && s_purchase_tax_name != undefined)
		{
			
		}
		else
		{
			s_purchase_tax_name = "Purchase Tax"
		}
		
		var purchaseObj = form.addField('custpage_purchasetax','currency',s_purchase_tax_name)
		if (purchasetax != null && purchasetax != '' && purchasetax != undefined) {
			purchaseObj.setDefaultValue(purchasetax)
		}
		else
		{
			purchasetax = parseInt(0)
		}
		purchaseObj.setDisplayType('inline')
		
		if(s_sales_tax_name != null && s_sales_tax_name != '' && s_sales_tax_name != undefined)
		{
			
		}
		else
		{
			s_sales_tax_name = "Sales Tax"
		}
		var salesObj = form.addField('custpage_salestax','currency',s_sales_tax_name)
		salesObj.setDefaultValue(salestax)
		salesObj.setDisplayType('inline')
		
		var taxPayObj = form.addField('custpage_taxpayable','currency','Tax Payable')
		taxpayable = (parseFloat(salestax) - parseFloat(purchasetax) )
		taxPayObj.setDefaultValue(taxpayable)
		taxPayObj.setDisplayType('inline')
		
		response.writePage(form)

	 }
	 else 
	 	if (request.getMethod() == 'POST') 
		{
			
	 	}


	//  LOCAL VARIABLES


    //  SUITELET CODE BODY
	
		
}

// END SUITELET ====================================================




// BEGIN OBJECT CALLED/INVOKING FUNCTION ===================================================

function setPurchaseSubList(purchasesublistObj, form, request, response, fromdate, todate, taxcode,i_taxTypeID){

	purchasesublistObj.addField('bill_date', 'text', 'Bill Date');
	
	purchasesublistObj.addField('bill_no', 'text', 'Bill No');
	
	purchasesublistObj.addField('vendor', 'text', 'Vendor');
	
	purchasesublistObj.addField('bill_amount', 'text', 'Bill Amount');
	
	purchasesublistObj.addField('tax_amount', 'text', 'Tax Amount');
	
	purchasesublistObj.addField('taxid', 'text', 'Tax ID');
	
	nlapiLogExecution('DEBUG', 'user', 'tax code ' + taxcode)
	
	var purchasedetail = new Array()
	var purchasetaxtotal = 0
	var purchaseamount = 0
	var purchasegrossamount = 0
	
	if (i_taxTypeID != null && i_taxTypeID != '' && i_taxTypeID != undefined) 
	{
		if (parseInt(i_taxTypeID) != parseInt(6) && parseInt(i_taxTypeID) != parseInt(2)) 
		{
			var jvIdAndType = new Array();
			jvIdAndType = taxcode.split(',');
			nlapiLogExecution('DEBUG', 'journalVouchersPrint', " JV ID & Type : " + jvIdAndType);
			nlapiLogExecution('DEBUG', 'journalVouchersPrint', " JV ID & Type Length : " + jvIdAndType.length);
			var taxIDPara = new Array
			
			for (i = 0; jvIdAndType.length != 0 && i < jvIdAndType.length; i++) 
			{
				var recordId = jvIdAndType[i];
				nlapiLogExecution('DEBUG', 'journalVouchersPrint', " Record ID : " + recordId);
				nlapiLogExecution('DEBUG', 'journalVouchersPrint', " 1 : " + i);
				taxIDPara[i] = recordId
			} // end for(i=0;jvIdAndType.length != 0 && i< jvIdAndType.length; i++)
			var filters = new Array();
			filters.push(new nlobjSearchFilter('custrecord_vatbill_taxid', null, 'anyof', taxIDPara))
			filters.push(new nlobjSearchFilter('custrecord_vatbill_billdate', null, 'onorafter', fromdate))
			filters.push(new nlobjSearchFilter('custrecord_vatbill_billdate', null, 'onorbefore', todate))
			filters.push(new nlobjSearchFilter('custrecord_vatbill_status', null, 'is', 'Open'))
			//filters.push( new nlobjSearchFilter('approvalstatus', 'custrecord_vatbill_tranno', 'is',2))
			filters.push(new nlobjSearchFilter('mainline', 'custrecord_vatbill_tranno', 'is', 'T'))
			filters.push(new nlobjSearchFilter('voided', 'custrecord_vatbill_tranno', 'is', 'F'))
			filters.push(new nlobjSearchFilter('recordtype', 'custrecord_vatbill_tranno', 'is', 'vendorcredit'))
			if (parseInt(i_taxTypeID) == parseInt(1)) {
				filters.push(new nlobjSearchFilter('custrecord_vatbill_rcm', null, 'is', 'F'))
			}
			
			var bill_filters = new Array();
			bill_filters.push(new nlobjSearchFilter('custrecord_vatbill_taxid', null, 'anyof', taxIDPara))
			bill_filters.push(new nlobjSearchFilter('custrecord_vatbill_billdate', null, 'onorafter', fromdate))
			bill_filters.push(new nlobjSearchFilter('custrecord_vatbill_billdate', null, 'onorbefore', todate))
			bill_filters.push(new nlobjSearchFilter('custrecord_vatbill_status', null, 'is', 'Open'))
			bill_filters.push(new nlobjSearchFilter('approvalstatus', 'custrecord_vatbill_tranno', 'is', 2))
			bill_filters.push(new nlobjSearchFilter('mainline', 'custrecord_vatbill_tranno', 'is', 'T'))
			bill_filters.push(new nlobjSearchFilter('voided', 'custrecord_vatbill_tranno', 'is', 'F'))
			bill_filters.push(new nlobjSearchFilter('recordtype', 'custrecord_vatbill_tranno', 'is', 'vendorbill'))
			if (parseInt(i_taxTypeID) == parseInt(1)) 
			{
				bill_filters.push(new nlobjSearchFilter('custrecord_vatbill_rcm', null, 'is', 'F'))
				//bill_filters.push(new nlobjSearchFilter('custrecord_ait_tax_type', 'custrecord_vatbill_taxid', 'is', 1))
			}
			
			var column = new Array();
			column.push(new nlobjSearchColumn('custrecord_vatbill_taxamount'))
			column.push(new nlobjSearchColumn('custrecord_vatbill_amount'))
			column.push(new nlobjSearchColumn('custrecord_vatbill_grossamount'))
			column.push(new nlobjSearchColumn('custrecord_vatbill_billno'))
			column.push(new nlobjSearchColumn('custrecord_vatbill_vendor'))
			column.push(new nlobjSearchColumn('custrecord_vatbill_billdate'))
			column.push(new nlobjSearchColumn('custrecord_vatbill_taxid'))
			column.push(new nlobjSearchColumn('custrecord_vatbill_tranno'))
			
			var results = nlapiSearchRecord('customrecord_vatbilldetail', null, bill_filters, column);
			nlapiLogExecution('DEBUG', 'getPurchasetaxtotal', 'results---->' + results)
			
			var credit_results = nlapiSearchRecord('customrecord_vatbilldetail', null, filters, column);
			nlapiLogExecution('DEBUG', 'getPurchasetaxtotal', 'results---->' + credit_results)
			
			if (credit_results != null) {
				if (results != null) {
					results = credit_results.concat(results);
				}
				else {
					results = credit_results
				}
			}
			var pomessage = ""
			if (results != null && results != '' && results != undefined) {
				nlapiLogExecution('DEBUG', 'Vat Payment', 'results.length = ' + results.length);
				
				var k = 0;
				
				for (i = 0; results != null && i < results.length; i++) {
					k = k + 1;
					var temptotal = results[i].getValue('custrecord_vatbill_taxamount')
					var tempamount = results[i].getValue('custrecord_vatbill_amount')
					var tempgrossamount = results[i].getValue('custrecord_vatbill_grossamount')
					
					var billNo = results[i].getValue('custrecord_vatbill_billno')
					var vendor = results[i].getText('custrecord_vatbill_vendor')
					var billDate = results[i].getValue('custrecord_vatbill_billdate')
					var taxId = results[i].getText('custrecord_vatbill_taxid')
					var s_BillNoText = results[i].getText('custrecord_vatbill_tranno')
					
					purchasesublistObj.setLineItemValue('bill_date', k, billDate);
					purchasesublistObj.setLineItemValue('bill_no', k, s_BillNoText);
					purchasesublistObj.setLineItemValue('vendor', k, vendor);
					purchasesublistObj.setLineItemValue('bill_amount', k, tempgrossamount);
					purchasesublistObj.setLineItemValue('tax_amount', k, temptotal);
					purchasesublistObj.setLineItemValue('taxid', k, taxId);
					purchasetaxtotal = purchasetaxtotal + parseFloat(temptotal)
					purchaseamount = purchaseamount + parseFloat(tempamount)
					purchasegrossamount = purchasegrossamount + parseFloat(tempgrossamount)
					
				} // end  for(i=0; i < results != null && i < results.length; i++)
			} // end  if( results != null)
			purchasedetail = purchasetaxtotal
			
			return purchasedetail
		}
	}
	else {
		var jvIdAndType = new Array();
		jvIdAndType = taxcode.split(',');
		nlapiLogExecution('DEBUG', 'journalVouchersPrint', " JV ID & Type : " + jvIdAndType);
		nlapiLogExecution('DEBUG', 'journalVouchersPrint', " JV ID & Type Length : " + jvIdAndType.length);
		var taxIDPara = new Array
		
		for (i = 0; jvIdAndType.length != 0 && i < jvIdAndType.length; i++) {
			var recordId = jvIdAndType[i];
			nlapiLogExecution('DEBUG', 'journalVouchersPrint', " Record ID : " + recordId);
			nlapiLogExecution('DEBUG', 'journalVouchersPrint', " 1 : " + i);
			taxIDPara[i] = recordId
		} // end for(i=0;jvIdAndType.length != 0 && i< jvIdAndType.length; i++)
		var filters = new Array();
		filters.push(new nlobjSearchFilter('custrecord_vatbill_taxid', null, 'anyof', taxIDPara))
		filters.push(new nlobjSearchFilter('custrecord_vatbill_billdate', null, 'onorafter', fromdate))
		filters.push(new nlobjSearchFilter('custrecord_vatbill_billdate', null, 'onorbefore', todate))
		filters.push(new nlobjSearchFilter('custrecord_vatbill_status', null, 'is', 'Open'))
		//filters.push( new nlobjSearchFilter('approvalstatus', 'custrecord_vatbill_tranno', 'is',2))
		filters.push(new nlobjSearchFilter('mainline', 'custrecord_vatbill_tranno', 'is', 'T'))
		filters.push(new nlobjSearchFilter('voided', 'custrecord_vatbill_tranno', 'is', 'F'))
		filters.push(new nlobjSearchFilter('recordtype', 'custrecord_vatbill_tranno', 'is', 'vendorcredit'))
		if (parseInt(i_taxTypeID) == parseInt(1)) {
			filters.push(new nlobjSearchFilter('custrecord_vatbill_rcm', null, 'is', 'F'))
		}
		
		var bill_filters = new Array();
		bill_filters.push(new nlobjSearchFilter('custrecord_vatbill_taxid', null, 'anyof', taxIDPara))
		bill_filters.push(new nlobjSearchFilter('custrecord_vatbill_billdate', null, 'onorafter', fromdate))
		bill_filters.push(new nlobjSearchFilter('custrecord_vatbill_billdate', null, 'onorbefore', todate))
		bill_filters.push(new nlobjSearchFilter('custrecord_vatbill_status', null, 'is', 'Open'))
		bill_filters.push(new nlobjSearchFilter('approvalstatus', 'custrecord_vatbill_tranno', 'is', 2))
		bill_filters.push(new nlobjSearchFilter('mainline', 'custrecord_vatbill_tranno', 'is', 'T'))
		bill_filters.push(new nlobjSearchFilter('voided', 'custrecord_vatbill_tranno', 'is', 'F'))
		bill_filters.push(new nlobjSearchFilter('recordtype', 'custrecord_vatbill_tranno', 'is', 'vendorbill'))
		if (parseInt(i_taxTypeID) == parseInt(1)) {
			bill_filters.push(new nlobjSearchFilter('custrecord_vatbill_rcm', null, 'is', 'F'))
		}
		
		var column = new Array();
		column.push(new nlobjSearchColumn('custrecord_vatbill_taxamount'))
		column.push(new nlobjSearchColumn('custrecord_vatbill_amount'))
		column.push(new nlobjSearchColumn('custrecord_vatbill_grossamount'))
		column.push(new nlobjSearchColumn('custrecord_vatbill_billno'))
		column.push(new nlobjSearchColumn('custrecord_vatbill_vendor'))
		column.push(new nlobjSearchColumn('custrecord_vatbill_billdate'))
		column.push(new nlobjSearchColumn('custrecord_vatbill_taxid'))
		column.push(new nlobjSearchColumn('custrecord_vatbill_tranno'))
		
		var results = nlapiSearchRecord('customrecord_vatbilldetail', null, bill_filters, column);
		nlapiLogExecution('DEBUG', 'getPurchasetaxtotal', 'results---->' + results)
		
		var credit_results = nlapiSearchRecord('customrecord_vatbilldetail', null, filters, column);
		nlapiLogExecution('DEBUG', 'getPurchasetaxtotal', 'results---->' + credit_results)
		
		if (credit_results != null) {
			if (results != null) {
				results = credit_results.concat(results);
			}
			else {
				results = credit_results
			}
		}
		var pomessage = ""
		if (results != null && results != '' && results != undefined) {
			nlapiLogExecution('DEBUG', 'Vat Payment', 'results.length = ' + results.length);
			
			var k = 0;
			
			for (i = 0; results != null && i < results.length; i++) {
				k = k + 1;
				var temptotal = results[i].getValue('custrecord_vatbill_taxamount')
				var tempamount = results[i].getValue('custrecord_vatbill_amount')
				var tempgrossamount = results[i].getValue('custrecord_vatbill_grossamount')
				
				var billNo = results[i].getValue('custrecord_vatbill_billno')
				var vendor = results[i].getText('custrecord_vatbill_vendor')
				var billDate = results[i].getValue('custrecord_vatbill_billdate')
				var taxId = results[i].getText('custrecord_vatbill_taxid')
				var s_BillNoText = results[i].getText('custrecord_vatbill_tranno')
				
				purchasesublistObj.setLineItemValue('bill_date', k, billDate);
				purchasesublistObj.setLineItemValue('bill_no', k, s_BillNoText);
				purchasesublistObj.setLineItemValue('vendor', k, vendor);
				purchasesublistObj.setLineItemValue('bill_amount', k, tempgrossamount);
				purchasesublistObj.setLineItemValue('tax_amount', k, temptotal);
				purchasesublistObj.setLineItemValue('taxid', k, taxId);
				purchasetaxtotal = purchasetaxtotal + parseFloat(temptotal)
				purchaseamount = purchaseamount + parseFloat(tempamount)
				purchasegrossamount = purchasegrossamount + parseFloat(tempgrossamount)
				
			} // end  for(i=0; i < results != null && i < results.length; i++)
		} // end  if( results != null)
		purchasedetail = purchasetaxtotal
		
		return purchasedetail
	}
}

function setSalesSubList(salessublistObj, form, request, response, fromdate, todate, taxcode, i_taxTypeID)
{

	salessublistObj.addField('invoicedate', 'text', 'Invoice Date');
	
	salessublistObj.addField('invoice_no', 'text', 'Invoice No');
	
	salessublistObj.addField('customer', 'text', 'Customer');
	
	salessublistObj.addField('invoice_amount', 'text', 'Invoice Amount');
	
	salessublistObj.addField('inv_tax_amount', 'text', 'Tax Amount');
	
	salessublistObj.addField('inv_taxid', 'text', 'Tax ID');
	
	var saledetail = new Array()
	var saletaxtotal = 0.00
	var saleamount = 0.00
	var salegrosstotal = 0.00
	
	var jvIdAndType = new Array();
	jvIdAndType = taxcode.split(',');
	nlapiLogExecution('DEBUG', 'journalVouchersPrint', " JV ID & Type : " + jvIdAndType);
	nlapiLogExecution('DEBUG', 'journalVouchersPrint', " JV ID & Type Length : " + jvIdAndType.length);
	var taxIDPara = new Array
	for (var i = 0; jvIdAndType.length != 0 && i < jvIdAndType.length; i++) 
	{
		var recordId = jvIdAndType[i];
		nlapiLogExecution('DEBUG', 'journalVouchersPrint', " Record ID : " + recordId);
		nlapiLogExecution('DEBUG', 'journalVouchersPrint', " 1 : " + i);
		taxIDPara[i] = recordId
	} // end for(var i=0;jvIdAndType.length != 0 && i< jvIdAndType.length; i++)
	nlapiLogExecution('DEBUG', 'user', 'taxIDPara ' + taxIDPara)
	
	var filters = new Array();
	filters.push(new nlobjSearchFilter('custrecord_vatinvoice_taxid', null, 'anyof', taxIDPara))
	filters.push(new nlobjSearchFilter('custrecord_vatinvoice_date', null, 'onorafter', fromdate))
	filters.push(new nlobjSearchFilter('custrecord_vatinvoice_date', null, 'onorbefore', todate))
	filters.push(new nlobjSearchFilter('custrecord_vatinvoice_status', null, 'is', 'Open'))
	filters.push(new nlobjSearchFilter('mainline', 'custrecord_vatinvoice_tranno', 'is', 'T'))
	filters.push(new nlobjSearchFilter('voided', 'custrecord_vatinvoice_tranno', 'is', 'F'))
	if (i_taxTypeID != null && i_taxTypeID != '' && i_taxTypeID != undefined) {
		if (parseInt(i_taxTypeID) == parseInt(6)) {
			filters.push(new nlobjSearchFilter('custrecord_vatinvoice_rcm', null, 'is', 'T'))
		//filters.push(new nlobjSearchFilter('custrecord_ait_tax_type', 'custrecord_vatinvoice_taxid', 'is', 1))
		}
		else {
			filters.push(new nlobjSearchFilter('custrecord_vatinvoice_rcm', null, 'is', 'F'))
		//filters.push(new nlobjSearchFilter('custrecord_ait_tax_type', 'custrecord_vatinvoice_taxid', 'is', 1))
		}
	}
	var column = new Array();
	column.push(new nlobjSearchColumn('custrecord_vatinvoice_taxamount'))
	column.push(new nlobjSearchColumn('custrecord_vatinvoice_amount'))
	column.push(new nlobjSearchColumn('custrecord_vatinvoice_grossamount'))
	column.push(new nlobjSearchColumn('custrecord_vatinvoice_invoiceno'))
	column.push(new nlobjSearchColumn('custrecord_vatinvoice_customer'))
	column.push(new nlobjSearchColumn('custrecord_vatinvoice_date'))
	column.push(new nlobjSearchColumn('custrecord_vatinvoice_taxid'))
	column.push(new nlobjSearchColumn('custrecord_vatinvoice_tranno'))
	column.push(new nlobjSearchColumn('custrecord_vatinvoice_entity'))
	column.push(new nlobjSearchColumn('recordtype', 'custrecord_vatinvoice_tranno', null))
	column.push(new nlobjSearchColumn('approvalstatus', 'custrecord_vatinvoice_tranno', null))
	
	var somessage = ''
	var results = nlapiSearchRecord('customrecord_vatinvoicedetail', null, filters, column);
	if (results != null && results != '' && results != undefined) 
	{
		var k = 0;
		
		for (i = 0; results != null && i < results.length; i++) 
		{
		
			var i_rectype = results[i].getValue('recordtype', 'custrecord_vatinvoice_tranno', null)
			nlapiLogExecution('DEBUG', 'getSaletaxtotal', ' i_rectype-->' + i_rectype)
			
			var i_status = results[i].getValue('approvalstatus', 'custrecord_vatinvoice_tranno', null)
			nlapiLogExecution('DEBUG', 'getSaletaxtotal', ' i_status-->' + i_status)
			
			var temptotal = results[i].getValue('custrecord_vatinvoice_taxamount')
			var tempamout = results[i].getValue('custrecord_vatinvoice_amount')
			var tempgrossamount = results[i].getValue('custrecord_vatinvoice_grossamount')
			
			var invoiceno = results[i].getValue('custrecord_vatinvoice_invoiceno')
			var customer = results[i].getText('custrecord_vatinvoice_customer')
			if (customer != null && customer != '' && customer != undefined) {
			
			}
			else {
				var customer = results[i].getText('custrecord_vatinvoice_entity')
			}
			var invoicedate = results[i].getValue('custrecord_vatinvoice_date')
			var taxid = results[i].getText('custrecord_vatinvoice_taxid')
			var s_InvoiceNoText = results[i].getText('custrecord_vatinvoice_tranno')
			
			
			if (i_rectype == 'vendorbill') 
			{
				if (i_status == '2') 
				{
					saletaxtotal = saletaxtotal + parseFloat(temptotal)
					nlapiLogExecution('DEBUG', 'getSaletaxtotal', 'Post saletaxtotal-->' + saletaxtotal)
					k = k + 1;
					salessublistObj.setLineItemValue('invoicedate', k, invoicedate);
					salessublistObj.setLineItemValue('invoice_no', k, s_InvoiceNoText);
					salessublistObj.setLineItemValue('customer', k, customer);
					salessublistObj.setLineItemValue('invoice_amount', k, tempgrossamount);
					salessublistObj.setLineItemValue('inv_tax_amount', k, temptotal);
					salessublistObj.setLineItemValue('inv_taxid', k, taxid);
						
				}
			}
			else 
			{
				saletaxtotal = saletaxtotal + parseFloat(temptotal)
				nlapiLogExecution('DEBUG', 'getSaletaxtotal', 'Post saletaxtotal-->' + saletaxtotal)
				
				k = k + 1;
				
				salessublistObj.setLineItemValue('invoicedate', k, invoicedate);
				salessublistObj.setLineItemValue('invoice_no', k, s_InvoiceNoText);
				salessublistObj.setLineItemValue('customer', k, customer);
				salessublistObj.setLineItemValue('invoice_amount', k, tempgrossamount);
				salessublistObj.setLineItemValue('inv_tax_amount', k, temptotal);
				salessublistObj.setLineItemValue('inv_taxid', k, taxid);
			}
			
			//saletaxtotal = saletaxtotal + parseFloat(temptotal)
			saleamount = saleamount + parseFloat(tempamout)
			salegrosstotal = salegrosstotal + parseFloat(tempgrossamount)
		}// end  for(i=0; i < results != null && i < results.length; i++)
	} // end  if( results != null)
	saledetail = saletaxtotal
	
	return saledetail
}
function SearchGlobalParameter()
	{

        var a_filters = new Array();
        var a_column = new Array();
        var i_globalRecId = 0;

        a_column.push(new nlobjSearchColumn('internalid'));

        var s_serchResult = nlapiSearchRecord('customrecord_indian_tax_glo_parameter', null, null, a_column)

        if (s_serchResult != null && s_serchResult != undefined && s_serchResult != '')
		{
            for (var i = 0; i < s_serchResult.length; i++)
			{
                i_globalRecId = s_serchResult[0].getValue('internalid');
                nlapiLogExecution('DEBUG', 'Bill ', "i_globalRecId" + i_globalRecId);

            }

        }


        return i_globalRecId;
    }

// END OBJECT CALLED/INVOKING FUNCTION =====================================================
