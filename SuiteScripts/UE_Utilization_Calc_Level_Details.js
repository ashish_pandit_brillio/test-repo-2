/**
 * This script calculates how many employees of various levels were allocated on
 * the entered project during the entered time period
 * 
 * Version Date Author Remarks 1.00 02 Jun 2016 Nitish Mishra
 * 
 */

function userEventBeforeSubmit(type) {
	try {

		if (type == 'create' || type == 'edit' || type == 'xedit') {
			var startDate = nlapiGetFieldValue('custrecord_urt_start_date');
			var endDate = nlapiGetFieldValue('custrecord_urt_end_date');
			var project = nlapiGetFieldValue('custrecord_urt_project');

			if (startDate && endDate && project) {
				var d_startDate = nlapiStringToDate(startDate);
				var d_endDate = nlapiStringToDate(endDate);
				var totalDays = getDayDiff(startDate, endDate);
				nlapiLogExecution('debug', 'totalDays', totalDays);

				// get all the allocated employees for this period
				var allocationSearch = nlapiSearchRecord('resourceallocation',
				        null, [
				                new nlobjSearchFilter('formuladate', null,
				                        'notafter', endDate)
				                        .setFormula('{startdate}'),
				                new nlobjSearchFilter('formuladate', null,
				                        'notbefore', startDate)
				                        .setFormula('{enddate}'),
				                new nlobjSearchFilter('project', null, 'anyof',
				                        project) ], [
				                new nlobjSearchColumn('resource'),
				                new nlobjSearchColumn('startdate'),
				                new nlobjSearchColumn('enddate'),
				                new nlobjSearchColumn('custeventrbillable'),
				                new nlobjSearchColumn('custevent4'),
				                new nlobjSearchColumn('percentoftime') ]);

				// create a list of allocated employees
				var allocatedEmployees = [];

				if (allocationSearch) {

					allocationSearch.forEach(function(allocation) {
						allocatedEmployees
						        .push(allocation.getValue('resource'));
					});
				}

				if (allocatedEmployees.length > 0) {

					// get the level change history of all the allocated
					// employees
					var employeeHistorySearch = searchRecord(
					        'customrecord_level_change_history', null, [
					                new nlobjSearchFilter(
					                        'custrecord_lch_employee', null,
					                        'anyof', allocatedEmployees),
					                new nlobjSearchFilter('isinactive', null,
					                        'is', 'F'),
					                new nlobjSearchFilter(
					                        'custrecord_lch_from_date', null,
					                        'notafter', endDate),
					                new nlobjSearchFilter(
					                        'custrecord_lch_to_date', null,
					                        'notbefore', startDate) ], [
					                new nlobjSearchColumn(
					                        'custrecord_lch_employee'),
					                new nlobjSearchColumn(
					                        'custrecord_lch_level'),
					                new nlobjSearchColumn(
					                        'custrecord_lch_from_date'),
					                new nlobjSearchColumn(
					                        'custrecord_lch_to_date') ]);

					if (employeeHistorySearch) {
						nlapiLogExecution('debug', 'history count',
						        employeeHistorySearch.length);
						var mainObject = {};

						allocationSearch
						        .forEach(function(allocation) {
							        var s_AllocationStartDate = allocation
							                .getValue('startdate');
							        var s_AllocationEndDate = allocation
							                .getValue('enddate');
							        var allocationEmployee = allocation
							                .getValue('resource');
							        var d_AllocationStartDate = nlapiStringToDate(s_AllocationStartDate);
							        var d_AllocationEndDate = nlapiStringToDate(s_AllocationEndDate);
							        var percentageAllocation = allocation
							                .getValue('percentoftime');

							        employeeHistorySearch
							                .forEach(function(history) {
								                var historyEmployee = history
								                        .getValue('custrecord_lch_employee');

								                if (historyEmployee == allocationEmployee) {
									                var historyLevel = history
									                        .getValue('custrecord_lch_level');
									                var s_historyStartDate = history
									                        .getValue('custrecord_lch_from_date');
									                var s_historyEndDate = history
									                        .getValue('custrecord_lch_to_date');

									                var d_historyStartDate = nlapiStringToDate(s_historyStartDate);
									                var d_historyEndDate = null;
									                if (s_historyEndDate) {
										                d_historyEndDate = nlapiStringToDate(s_historyEndDate);
									                } else {
										                d_historyEndDate = new Date(
										                        "1/1/4022");
									                }

									                var newStartDate = MaximumDate(
									                        d_historyStartDate,
									                        d_AllocationStartDate,
									                        d_startDate);

									                var newEndDate = MinimumDate(
									                        d_historyEndDate,
									                        d_AllocationEndDate,
									                        d_endDate);

									                var personMonth = getPersonUtilization(
									                        nlapiDateToString(
									                                newStartDate,
									                                'date'),
									                        nlapiDateToString(
									                                newEndDate,
									                                'date'),
									                        percentageAllocation,
									                        startDate, endDate,
									                        totalDays, 8);

									                if (!mainObject[historyLevel]) {
										                mainObject[historyLevel] = 0;
									                }

									                mainObject[historyLevel] += personMonth;
								                }
							                });
						        });

						nlapiLogExecution('debug', 'main Array', JSON
						        .stringify(mainObject));

						// push all level details in the custom record
						for ( var status in mainObject) {
							nlapiSelectNewLineItem('recmachcustrecord_lud_parent');
							nlapiSetCurrentLineItemText(
							        'recmachcustrecord_lud_parent',
							        'custrecord_lud_employee_status', status);
							nlapiSetCurrentLineItemValue(
							        'recmachcustrecord_lud_parent',
							        'custrecord_lud_utilization',
							        mainObject[status]);
							nlapiCommitLineItem('recmachcustrecord_lud_parent');
						}
					} else {
						throw "No employee level change history found";
					}
				} else {
					throw "No allocation found";
				}
			} else {
				throw 'Mandatory fields missing';
			}
		}
	} catch (err) {
		nlapiLogExecution('ERROR', 'userEventBeforeSubmit', err);
	}
}