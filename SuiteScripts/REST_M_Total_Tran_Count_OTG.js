/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       05 May 2017     deepak.srinivas
 *
 */

/**
 * @param {Object} dataIn Parameter object
 * @returns {Object} Output object
 */
			
function postRESTlet(dataIn) {
	try{
		var response = new Response();
		var current_date = nlapiDateToString(new Date());
		//Log for current date
		nlapiLogExecution('DEBUG', 'Current Date', 'current_date...' + current_date)
		nlapiLogExecution('debug', 'dataIn', JSON.stringify(dataIn));
		
		var employeeId =  getUserUsingEmailId(dataIn.EmailId);
		var requestType = dataIn.RequestType;
		/*var email = 'rahul.murali@brillio.com';
		var employeeId =  getUserUsingEmailId(email);
		var requestType = 'GET';*/
		
		
		
		switch (requestType) {

		case M_Constants.Request.Get:

			if (employeeId) 
			{
				response.Data = total_Tran_Count();
				response.Status = true;
			} else {
				response.Data = "Some error with the data sent";
				response.Status = false;
		}
			break;
		}
	
	}
	catch (err) {
		nlapiLogExecution('ERROR', 'postRESTlet', err);
		response.Data = err;
		response.Status = false;
	}
	nlapiLogExecution('debug', 'response', JSON.stringify(response));
	return response;
	
}
function total_Tran_Count(){
try{
	//variables
	var i_gross_amt = 0;
	var i_total_exp_submitted = 0;
	var i_total_exp_approved = 0;
	var i_total_TS_approved = 0;
	var i_total_TS_approved_Hr = 0;
	var i_total_TR_approved = 0;
	var timeHours_mins = 0;
	var JSON ={};
	var dataRow = [];
	
		{//Get Total Amount in $ approved from APP
		//var searchResults = nlapiSearchRecord('transaction','customsearch1768');	//Sandbox
		var searchResults = nlapiSearchRecord('transaction','customsearch_expenses_approv_from_mobile'); //PROD	
		if(searchResults){
			var results = searchResults[0];
			var columns = results.getAllColumns();
			
			i_gross_amt = results.getValue(columns[0]);
		}
	//GET Total Expense submitted from APP
		//var searchResults_submitted = nlapiSearchRecord('transaction','customsearch_expenses_raised_from_mobile'); //SANDBOX
		var searchResults_submitted = nlapiSearchRecord('transaction','customsearch_expenses_raised_from_mobile'); //PROD
		if(searchResults_submitted){
			var results_sub = searchResults_submitted[0];
			var columns_sub = results_sub.getAllColumns();
			
			i_total_exp_submitted = results_sub.getValue(columns_sub[0]);
		}
		
	//GET Total Expense Approved/Rejected from APP
		//var searchResults_approved = nlapiSearchRecord('expensereport','customsearch1769'); //Sandbox
		var searchResults_approved = nlapiSearchRecord('expensereport','customsearch_expense_approved_from_app'); //PROD
		if(searchResults_approved){
			var results_apr = searchResults_approved[0];
			var columns_apr = results_apr.getAllColumns();
			
			i_total_exp_approved = results_apr.getValue(columns_apr[0]);
		}
		
		//GET Total Timesheet Approved/Rejected from APP
		//var searchResults_approved_TS = nlapiSearchRecord('customrecord_ts_quick_approval_pool','customsearch1770');	//Sandbox
		var searchResults_approved_TS = nlapiSearchRecord('customrecord_ts_quick_approval_pool','customsearch_ts_approved_rej_from_app'); //Prod
		if(searchResults_approved_TS){
			var results_apr_TS = searchResults_approved_TS[0];
			var columns_apr_TS = results_apr_TS.getAllColumns();
			
			i_total_TS_approved = results_apr_TS.getValue(columns_apr_TS[0]);
		}	
		
		//GET Total Timesheet Hours from APP
		//var searchResults_approved_TS_Hr = nlapiSearchRecord('customrecord_ts_quick_approval_pool','customsearch1771');	//sandbox
		var searchResults_approved_TS_Hr = nlapiSearchRecord('customrecord_ts_quick_approval_pool','customsearch_ts_hrs_from_app');	//PROD
		if(searchResults_approved_TS_Hr){
			for(var i=0;i<searchResults_approved_TS_Hr.length;i++){
			var results_apr_TS_Hr = searchResults_approved_TS_Hr[i];
			var columns_apr_TS_Hr = results_apr_TS_Hr.getAllColumns();
			var timeHours = results_apr_TS_Hr.getValue(columns_apr_TS_Hr[0]);
			timeHours = timeHours.split(':');
			
			var time_Hour =timeHours[0];
			var time_Hr_Min = timeHours[1];
			
			if(parseFloat(time_Hr_Min) > 0 ){
				timeHours_mins = time_Hr_Min;
			}
			//TOtal
			i_total_TS_approved_Hr = parseFloat(time_Hour) + parseFloat(i_total_TS_approved_Hr) + parseFloat(timeHours_mins);
		
			}
		}
		
		//GET Total Travel Approved/Rejected from APP
		//var searchResults_approved_TR = nlapiSearchRecord('customrecord_travel_request','customsearch1772');	//Sandbox
		var searchResults_approved_TR = nlapiSearchRecord('customrecord_travel_request','customsearch1835');	//PROD
		if(searchResults_approved_TR){
			var results_apr_TR = searchResults_approved_TR[0];
			var columns_apr_TR = results_apr_TR.getAllColumns();
			
			i_total_TR_approved = results_apr_TR.getValue(columns_apr_TR[0]);
		}
		JSON = {
				TotalExpAmtApproved: i_gross_amt,
				TotalExpSubmitted: i_total_exp_submitted,
				TotalExpApprOrRej: i_total_exp_approved,
				TotalTSApprOrRej: i_total_TS_approved,
				TotalTSHours: parseFloat(i_total_TS_approved_Hr),
				TotalTravelApprOrRej: i_total_TR_approved
				
				
		};
		dataRow.push(JSON);
	}
		return dataRow;
}
	catch(e){		
		nlapiLogExecution('DEBUG','Erron in getting count',e);
		throw e;
		
	}
}