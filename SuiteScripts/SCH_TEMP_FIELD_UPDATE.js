//BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
	/*
     Script Name	:
     Author			: Ashish Pandit
     Company		:
     Date			: 
     
	 
	 Script Modification Log:
     
	 -- Date --               -- Modified By --                  --Requested By--                   -- Description --
     

	 Below is a summary of the process controls enforced by this script file.  The control logic is described
     more fully, below, in the appropriate function headers and code blocks.
     SCHEDULED FUNCTION
     - RFQbody_SCH_main()
     SUB-FUNCTIONS
     - The following sub-functions are called by the above core functions in order to maintain code
     modularization:
     - NOT USED
	 */
}

//END SCRIPT DESCRIPTION BLOCK  ====================================

function scheduled_DeleteRecord() 
{
	try
	{
		
		var customrecord_ts_extra_attributesSearch = nlapiSearchRecord("customrecord_ts_extra_attributes",'customsearch2525',null,null);
		
		// Call Scheduled script for JE creation
		if(customrecord_ts_extra_attributesSearch)
		{
			nlapiLogExecution('Debug','customrecord_ts_extra_attributesSearch.length',customrecord_ts_extra_attributesSearch.length);
			for(var i=0;i<customrecord_ts_extra_attributesSearch.length;i++)
			{
				var id = customrecord_ts_extra_attributesSearch[i].getId();
				nlapiLogExecution('Debug','id  ',id);
				var subId = nlapiDeleteRecord('customrecord_ts_extra_attributes',id);
				nlapiLogExecution('Debug','subId  ',subId);
			}
		}
	
	}
	catch(e)
	{
		nlapiLogExecution('Debug','Exception ',e.message);
	}
}