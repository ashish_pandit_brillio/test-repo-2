//https://system.na1.netsuite.com/app/common/scripting/script.nl?id=940
// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
/*
   	Description: This script will update SBC lines MEMO field as per brillio standard memo on vendor bill.
				 These are populated through AIT script. Need for this script was, we can't edit AIT script hence new script created.


	Script Modification Log:

	-- Date --			-- Modified By --				--Requested By--				-- Description --



Below is a summary of the process controls enforced by this script file.  The control logic is described
more fully, below, in the appropriate function headers and code blocks.


     BEFORE LOAD
		- beforeLoadRecord(type)



     BEFORE SUBMIT
		- beforeSubmitRecord(type)


     AFTER SUBMIT
		- afterSubmit_UpdateMemo(type)



     SUB-FUNCTIONS
		- The following sub-functions are called by the above core functions in order to maintain code
            modularization:

               - NOT USED

*/
}
// END SCRIPT DESCRIPTION BLOCK  ====================================



// BEGIN GLOBAL VARIABLE BLOCK  =====================================
{
	//  Initialize any Global Variables, in particular, debugging variables...




}
// END GLOBAL VARIABLE BLOCK  =======================================





// BEGIN BEFORE LOAD ==================================================

function beforeLoadRecord(type)
{

	/*  On before load:

          - EXPLAIN THE PURPOSE OF THIS FUNCTION

		-


		FIELDS USED:

          --Field Name--				--ID--


	*/



	//  LOCAL VARIABLES

    //  BEFORE LOAD CODE BODY


	return true;



}

// END BEFORE LOAD ====================================================


// BEGIN BEFORE SUBMIT ================================================

function beforeSubmitRecord(type)
{
    /*  On before submit:

          - PURPOSE
		-

          FIELDS USED:

          --Field Name--				--ID--

    */

    //  LOCAL VARIABLES
	
	


    //  BEFORE SUBMIT CODE BODY


	return true;
}

// END BEFORE SUBMIT ==================================================


// BEGIN AFTER SUBMIT =============================================

function afterSubmit_UpdateMemo(type)
{
    /*  On after submit:

          - PURPOSE
		-

	FIELDS USED:

          --Field Name--				--ID--

    */

	//  LOCAL VARIABLES
	
	var submit_record_flag = 0;

	//  AFTER SUBMIT CODE BODY
		
	try
	{
		if(type == 'create' || type == 'edit')
		{
			var vendor_bill_rcrd = nlapiLoadRecord(nlapiGetRecordType(),nlapiGetRecordId());
			
			var item_line_count = vendor_bill_rcrd.getLineItemCount('item');
			for (var d = 1; d <= item_line_count; d++)
			{
				var emp_type = '';
				var person_type = '';
				var onsite_offsite = '';
				var cust_full_name_with_id = '';
				var core_practice = '';
				var emp_fusion_id = '';
				var emp_frst_name = '';
				var emp_middl_name = '';
				var emp_lst_name = '';
				var emp_full_name = '';
				var emp_practice = '';
              var emp_practice_text = '';
				var s_proj_desc_split = '';
				var s_employee_name_split = '';
				var proj_name_selected = '';
				var s_employee_name = '';
				var s_employee_id = '';
				var s_employee_name_id = '';
				var proj_name_selected = '';
				var project_entity_id = '';
				var s_proj_desc_id = '';
				var existing_practice = '';
				var proj_full_name_with_id = '';
				var employee_with_id = '';
				var proj_category_val = '';
				var misc_practice = '';
				var parent_practice = '';
				var project_region = '';
				
				existing_practice = vendor_bill_rcrd.getLineItemValue('item', 'department',d);
				s_employee_name = vendor_bill_rcrd.getLineItemValue('item', 'custcol_employeenamecolumn', d);
				s_employee_id = vendor_bill_rcrd.getLineItemValue('item','custcol_employee_entity_id',d);	
							if(_logValidation(s_employee_id)){
							 s_employee_name_split = s_employee_id;
							 nlapiLogExecution('audit','s_employee_name_split item',s_employee_name_split);
							}
							else if(_logValidation(s_employee_name)){
							s_employee_name_id = s_employee_name.split('-');
							 s_employee_name_split = s_employee_name_id[0];
							 nlapiLogExecution('audit','s_employee_name',s_employee_name);
							}
				if(_logValidation(s_employee_name) || _logValidation(s_employee_id))
				{
					//var s_employee_name_id = emp_name_selected.split('-');
					//var s_employee_name_split = s_employee_name_id[0];
					
					var filters_emp = new Array();
					filters_emp[0] = new nlobjSearchFilter('entityid',null,'contains',s_employee_name_split);
					var column_emp = new Array();	
    				column_emp[0] = new nlobjSearchColumn('custentity_persontype');
					column_emp[1] = new nlobjSearchColumn('employeetype');
					column_emp[2] = new nlobjSearchColumn('subsidiary');
					column_emp[3] = new nlobjSearchColumn('custentity_fusion_empid');
					column_emp[4] = new nlobjSearchColumn('firstname');
					column_emp[5] = new nlobjSearchColumn('middlename');
					column_emp[6] = new nlobjSearchColumn('lastname');
					column_emp[7] = new nlobjSearchColumn('department');
					column_emp[8] = new nlobjSearchColumn('location');
					
					var a_results_emp = nlapiSearchRecord('employee', null, filters_emp, column_emp);
					nlapiLogExecution('audit','a_results_emp:-- ',a_results_emp);
					if (_logValidation(a_results_emp))
					{
						var emp_id = a_results_emp[0].getId();
						var emp_type = a_results_emp[0].getText('employeetype');
						nlapiLogExecution('audit','emp_type:-- ',emp_type);
						if(!_logValidation(emp_type))
						{
							emp_type = '';
						}
						var person_type = a_results_emp[0].getText('custentity_persontype');
						nlapiLogExecution('audit','person_type:-- ',person_type);
						if(!_logValidation(person_type))
						{
							person_type = '';
						}
						
						nlapiLogExecution('audit','person_type:-- ',person_type);
						var emp_subsidiary = a_results_emp[0].getValue('subsidiary');
						person_type = a_results_emp[0].getText('custentity_persontype');
						emp_fusion_id = a_results_emp[0].getValue('custentity_fusion_empid');
						emp_practice = a_results_emp[0].getValue('department');
                      emp_practice_text =  a_results_emp[0].getText('department');
						emp_frst_name = a_results_emp[0].getValue('firstname');
						emp_middl_name = a_results_emp[0].getValue('middlename');
						emp_lst_name = a_results_emp[0].getValue('lastname');
						if(!_logValidation(emp_fusion_id))
						{
							emp_fusion_id = '';
						}
						nlapiLogExecution('audit','emp_fusion_id:-- ',emp_fusion_id);
						if(emp_frst_name)
							emp_full_name = emp_frst_name;
											
							if(emp_middl_name)
								emp_full_name = emp_full_name + ' ' + emp_middl_name;
											
							if(emp_lst_name)
								emp_full_name = emp_full_name + ' ' + emp_lst_name;
						if (_logValidation(emp_subsidiary))
						{
							if(emp_subsidiary == 3)
							{
								onsite_offsite = 'Offsite';
							}
							else
							{
								onsite_offsite = 'Onsite';
							}	
						}
						if(emp_practice)
						{
							var is_practice_active_e = nlapiLookupField('department',parseInt(emp_practice),['isinactive','custrecord_is_delivery_practice']);
							var isinactive_Practice_e = is_practice_active_e.isinactive;
							nlapiLogExecution('debug','isinactive_Practice_e',isinactive_Practice_e);
							core_practice=is_practice_active_e.custrecord_is_delivery_practice;
							nlapiLogExecution('debug','core_practice',core_practice);
						}
						if(emp_fusion_id)
								employee_with_id = emp_fusion_id +'-'+ emp_full_name;
								
							
						vendor_bill_rcrd.selectLineItem('item', d);
						vendor_bill_rcrd.setCurrentLineItemValue('item', 'custcol_employeenamecolumn', employee_with_id);
						
						vendor_bill_rcrd.setCurrentLineItemValue('item', 'custcol_employee_type', emp_type);
						vendor_bill_rcrd.setCurrentLineItemValue('item', 'custcol_person_type', person_type);
						vendor_bill_rcrd.setCurrentLineItemValue('item', 'custcol_onsite_offsite', onsite_offsite);
						vendor_bill_rcrd.setCurrentLineItemValue('item', 'custcol_employee_entity_id', emp_fusion_id);
						vendor_bill_rcrd.setCurrentLineItemValue('item', 'custcol_emp_name_on_a_click_report', emp_full_name);
						if(existing_practice){
						}
						else{
						vendor_bill_rcrd.setCurrentLineItemValue('item', 'department', emp_practice);
						}
						vendor_bill_rcrd.commitLineItem('item');
						
						submit_record_flag = 1;
					}
					
				}
				
				proj_name_selected = vendor_bill_rcrd.getLineItemValue('item', 'custcolprj_name', d);
				project_entity_id = vendor_bill_rcrd.getLineItemValue('item','custcol_project_entity_id',d);
				if(project_entity_id){
							s_proj_desc_split = project_entity_id;
							nlapiLogExecution('audit','s_proj_desc_split',s_proj_desc_split);
							}
							else if(proj_name_selected){
								s_proj_desc_id = proj_name_selected.substr(0,9);
								s_proj_desc_split = s_proj_desc_id;
								nlapiLogExecution('audit','proj_name_selected',proj_name_selected);
							}
				//if (_logValidation(proj_name_selected))
				//{
				//	proj_full_name = proj_name_selected.toString();
				//	proj_id_array = proj_full_name.split(' ');
				//	proj_id = proj_id_array[0];
					if (_logValidation(s_proj_desc_split))
					{
						var filters_search_proj = new Array();
						filters_search_proj[0] = new nlobjSearchFilter('entityid',null,'is',s_proj_desc_split);
						var column_search_proj = new Array();
						column_search_proj[0] = new nlobjSearchColumn('jobbillingtype');
						column_search_proj[1] = new nlobjSearchColumn('customer');
						column_search_proj[2] = new nlobjSearchColumn('custentity_region','customer');
						column_search_proj[3] = new nlobjSearchColumn('entityid');
						column_search_proj[4] = new nlobjSearchColumn('altname');
						column_search_proj[5] = new nlobjSearchColumn('custentity_project_allocation_category');
						column_search_proj[6] = new nlobjSearchColumn('companyname','customer');
						column_search_proj[7] = new nlobjSearchColumn('entityid','customer');
						column_search_proj[8] = new nlobjSearchColumn('custentity_vertical');
						column_search_proj[9] = new nlobjSearchColumn('territory','customer');
						column_search_proj[10] = new nlobjSearchColumn('custentity_practice');
						column_search_proj[11] = new nlobjSearchColumn('custentity_region');
						column_search_proj[12] = new nlobjSearchColumn('custrecord_parent_practice','custentity_practice');
						
						var search_proj_results = nlapiSearchRecord('job', null, filters_search_proj, column_search_proj);
						if (_logValidation(search_proj_results))
						{
							var proj_billing_type = search_proj_results[0].getText('jobbillingtype');
							var i_region_id = search_proj_results[0].getText('custentity_region');
							var i_proj_rcrd = nlapiLoadRecord('job',search_proj_results[0].getId());
							var i_proj_name = i_proj_rcrd.getFieldValue('altname');
							var i_proj_entity_id = search_proj_results[0].getValue('entityid');
							var i_proj_category = search_proj_results[0].getText('custentity_project_allocation_category');
							var i_proj_vertical = search_proj_results[0].getValue('custentity_vertical');
							var i_proj_practice = search_proj_results[0].getValue('custentity_practice');
							proj_category_val = search_proj_results[0].getValue('custentity_project_allocation_category');												
									
							var i_cust_name = search_proj_results[0].getValue('companyname','customer');
							var i_cust_entity_id = search_proj_results[0].getValue('entityid','customer');
							var i_cust_territory = search_proj_results[0].getValue('territory','customer');
							parent_practice = search_proj_results[0].getText('custrecord_parent_practice','custentity_practice');
							if (!_logValidation(proj_billing_type))
								proj_billing_type = '';
							
							vendor_bill_rcrd.selectLineItem('item', d);
							vendor_bill_rcrd.setCurrentLineItemValue('item', 'custcol_billing_type', proj_billing_type);
							vendor_bill_rcrd.setCurrentLineItemValue('item', 'custcol_project_entity_id', s_proj_desc_split);
							
							var proj_cust = search_proj_results[0].getText('customer');
							if (_logValidation(proj_cust))
							{
								var cust_entity_id = proj_cust.split(' ');
								cust_entity_id = cust_entity_id[0];
								vendor_bill_rcrd.setCurrentLineItemValue('item', 'custcol_customer_entityid', cust_entity_id);
							}
							if(s_employee_name_split || existing_practice){
							}
							else{
							vendor_bill_rcrd.setCurrentLineItemValue('item', 'department', i_proj_practice);
							}
							if(i_proj_entity_id)
								proj_full_name_with_id = i_proj_entity_id +' '+ i_proj_name;
							if(proj_name_selected){
							}
							else{							
							vendor_bill_rcrd.setCurrentLineItemValue('item', 'custcolprj_name', proj_full_name_with_id);
							}
							if(cust_entity_id)
							cust_full_name_with_id = cust_entity_id +' '+ i_cust_name;
							
							//
							if(proj_category_val)
							{
								if((parseInt(proj_category_val)==parseInt(1)) && (core_practice =='T'))
								{
									misc_practice=emp_practice;
                                  misc_practice = emp_practice_text;
								}
								else 
								{
									misc_practice=search_proj_results[0].getValue('custentity_practice');
									var is_practice_active = nlapiLookupField('department',parseInt(misc_practice),['isinactive']);
									isinactive_Practice_e = is_practice_active.isinactive;
                                  misc_practice=search_proj_results[0].getText('custentity_practice');
								}
							}
							nlapiLogExecution('audit','Pratice','misc_practice:'+misc_practice);
							if(misc_practice && isinactive_Practice_e == 'F'){
							vendor_bill_rcrd.setCurrentLineItemValue('item','custcol_mis_practice', misc_practice);
							}
							//
							var cust_region = search_proj_results[0].getValue('custentity_region','customer');
							
							vendor_bill_rcrd.setCurrentLineItemValue('item', 'custcolcustcol_temp_customer', cust_full_name_with_id);
							vendor_bill_rcrd.setCurrentLineItemValue('item', 'custcol_region_master_setup', cust_region);
							vendor_bill_rcrd.setCurrentLineItemValue('item', 'custcol_cust_name_on_a_click_report', i_cust_name);
							vendor_bill_rcrd.setCurrentLineItemValue('item', 'custcol_proj_name_on_a_click_report', i_proj_name);
							vendor_bill_rcrd.setCurrentLineItemValue('item', 'custcol_project_region', i_region_id);
							vendor_bill_rcrd.setCurrentLineItemValue('item', 'custcol_proj_category_on_a_click', i_proj_category);
							vendor_bill_rcrd.setCurrentLineItemValue('item', 'custcol_territory', i_cust_territory);
							vendor_bill_rcrd.setCurrentLineItemValue('item', 'custcol_parent_executing_practice', parent_practice);
							vendor_bill_rcrd.commitLineItem('item');
							
							submit_record_flag = 1;	
						}
					}									
				//}
				
					
			}
			
			if(submit_record_flag == 1)
			{
				var vendor_bill_submitted_id = nlapiSubmitRecord(vendor_bill_rcrd,true,true);
				nlapiLogExecution('debug','submitted bill id:-- ',vendor_bill_submitted_id);
			}
		}
	}
	catch(e)
	{
		nlapiLogExecution('ERROR','ERROR MESSAGE:-- ',e);
	}
	
	return true;

}

// END AFTER SUBMIT ===============================================



// BEGIN FUNCTION ===================================================
{

function _logValidation(value) 
{
 if(value != null && value.toString() != null && value != '' && value != undefined && value.toString() != undefined && value != 'undefined' && value.toString() != 'undefined'&& value.toString() != 'NaN' && value != NaN) 
 {
  return true;
 }
 else 
 { 
  return false;
 }
}

}
// END FUNCTION =====================================================
