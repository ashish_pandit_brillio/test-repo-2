/**
 * Change the client partner of a project
 * 
 * Version Date Author Remarks 1.00 07 Apr 2016 Nitish Mishra
 * 
 */

function suitelet(request, response) {

	try {

		var mode = request.getMethod();

		if (mode == 'GET') {
			getScreen(request);
		} else {
			postScreen(request);
		}
	} catch (err) {
		nlapiLogExecution('ERROR', 'suitelet', err);
		displayErrorForm(err, "Client Partner Change");
	}
}

function getScreen(request) {
	try {

		var customerId = request.getParameter('customer');

		if (customerId) {
			var customerDetails = nlapiLoadRecord('customer', customerId);

			var form = nlapiCreateForm('Client Partner Change - '
			        + customerDetails.getFieldValue('entityid') + " : "
			        + customerDetails.getFieldValue('altname'));
			form.setScript('customscript_cs_sut_cp_change_screen');
			form.addButton('custpage_btn_go_back', 'Change Customer',
			        'changeCustomer()');
			form.addSubmitButton('Submit Changes');

			// Customer Section
			form.addFieldGroup('custpage_customer_group', 'Customer Section');

			var customerField = form.addField('custpage_customer', 'select',
			        'Please select the customer', 'customer',
			        'custpage_customer_group');
			customerField.setDefaultValue(customerId);
			customerField.setDisplayType('inline');

			var customerStatus = form.addField('custpage_customer_status',
			        'text', 'MSA Status', null, 'custpage_customer_group');
			customerStatus.setDefaultValue(customerDetails
			        .getFieldText('custentity_msastatus'));
			customerStatus.setDisplayType('inline');

			var oldClientPartner = form.addField('custpage_old_client_partner',
			        'select', 'Old Client Partner', 'employee',
			        'custpage_customer_group');
			oldClientPartner.setDefaultValue(customerDetails
			        .getFieldValue('custentity_clientpartner'));
			oldClientPartner.setDisplayType('inline');

			var newClientPartner = form.addField('custpage_new_client_partner',
			        'select', 'New Client Partner', 'employee',
			        'custpage_customer_group');

			// Project Section
			var projectSublist = form.addSubList('custpage_list_job', 'list',
			        'Related Projects');
			projectSublist.addButton('custpage_copy_customer',
			        'Copy From Customer', 'copyFromCustomer');
			projectSublist.addField('project', 'select', 'Project', 'job')
			        .setDisplayType('inline');
			projectSublist.addField('status', 'text', 'Status');
			projectSublist.addField('type', 'text', 'Type');
			projectSublist.addField('old_cp', 'select', 'Old Client Partner',
			        'employee').setDisplayType('inline');
			projectSublist.addField('new_cp', 'select', 'New Client Partner',
			        'employee');

			var projectSearch = nlapiSearchRecord(
			        'job',
			        null,
			        [
			                new nlobjSearchFilter('customer', null, 'anyof',
			                        customerId),
			                new nlobjSearchFilter('status', null, 'anyof', [ 2,
			                        4 ]),
			                new nlobjSearchFilter('isinactive', null, 'is', 'F') ],
			        [ new nlobjSearchColumn('entitystatus'),
			                new nlobjSearchColumn('jobtype'),
			                new nlobjSearchColumn('custentity_clientpartner') ]);

			var projectData = [];
			if (projectSearch) {

				projectSearch.forEach(function(project) {
					projectData.push({
					    project : project.getId(),
					    old_cp : project.getValue('custentity_clientpartner'),
					    status : project.getText('entitystatus'),
					    type : project.getText('jobtype')
					});
				});
			}

			projectSublist.setLineItemValues(projectData);

			response.writePage(form);
		} else {
			var form = nlapiCreateForm('Client Partner Change');
			form.setScript('customscript_cs_sut_cp_change_screen');
			form.addButton('custpage_btn_change_customer',
			        'Get Client Partners', 'getCustomerDetails()');

			form.addField('custpage_customer', 'select',
			        'Please select the customer', 'customer');
			response.writePage(form);
		}
	} catch (err) {
		nlapiLogExecution('ERROR', 'getScreen', err);
		throw err;
	}
}

function postScreen(request) {
	try {

		var customerId = request.getParameter('custpage_customer');

		if (customerId) {
			var result = [];
			var customerMessage = "";

			var newClientPartner = request
			        .getParameter('custpage_new_client_partner');
			var customerUpdated = false;

			if (newClientPartner) {
				try {
					nlapiSubmitField('customer', customerId,
					        'custentity_clientpartner', newClientPartner);
					nlapiLogExecution('debug', 'customer : ' + customerId,
					        'Client Partner Updated');
					customerMessage = "Successfully Updated";
					customerUpdated = true;
				} catch (exc) {
					customerMessage = exc;
				}
			} else {
				customerMessage = "No Change";
				customerUpdated = true;
			}

			if (customerUpdated) {
				var projectCount = request
				        .getLineItemCount('custpage_list_job');

				for (var line = 1; line <= projectCount; line++) {
					var projectId = request.getLineItemValue(
					        'custpage_list_job', 'project', line);
					var newClientPartner = request.getLineItemValue(
					        'custpage_list_job', 'new_cp', line);

					if (newClientPartner) {
						try {

							nlapiSubmitField('job', projectId,
							        'custentity_clientpartner',
							        newClientPartner);
							nlapiLogExecution('debug',
							        'project : ' + projectId,
							        'Client Partner Updated');

							result.push({
							    type : 'job',
							    id : projectId,
							    error : 'Successfully Updated'
							});
						} catch (ex) {
							result.push({
							    type : 'job',
							    id : projectId,
							    error : ex
							});
						}
					} else {
						result.push({
						    type : 'job',
						    id : projectId,
						    error : 'No Change'
						});
					}
				}
			} else {
				result
				        .push({
				            type : 'job',
				            id : '',
				            error : 'Failed to update at customer, not updating the project'
				        });
			}

			// Resultant form
			var customerDetails = nlapiLoadRecord('customer', customerId);

			var form = nlapiCreateForm('Client Partner Change - '
			        + customerDetails.getFieldValue('entityid') + " : "
			        + customerDetails.getFieldValue('altname'));
			form.setScript('customscript_cs_sut_cp_change_screen');
			form.addButton('custpage_btn_go_back', 'Go Back',
			        'changeCustomer()');

			form.addFieldGroup('custpage_customer_group', 'Customer Section');
			var customerField = form.addField('custpage_customer', 'select',
			        'Customer', 'customer', 'custpage_customer_group');
			customerField.setDefaultValue(customerId);
			customerField.setDisplayType('inline');

			var customerStatus = form.addField('custpage_customer_message',
			        'text', 'Message', null, 'custpage_customer_group');
			customerStatus.setDefaultValue(customerMessage);
			customerStatus.setDisplayType('inline');

			var projectSublist = form.addSubList('custpage_list_job_result',
			        'staticlist', 'Related Projects');
			projectSublist.addField('id', 'select', 'Project', 'job')
			        .setDisplayType('inline');
			projectSublist.addField('error', 'text', 'Message');
			projectSublist.setLineItemValues(result);

			response.writePage(form);
		} else {
			throw "No customer selected";
		}
	} catch (err) {
		nlapiLogExecution('ERROR', 'getScreen', err);
		throw err;
	}
}

// Client Script Part
function getCustomerDetails() {
	var customer = nlapiGetFieldValue('custpage_customer');

	if (customer) {
		window.location = nlapiResolveURL('SUITELET',
		        'customscript_sut_client_partner_change',
		        'customdeploy_sut_client_partner_change')
		        + "&customer=" + customer;
	} else {
		alert("Please select a customer");
	}
}

function changeCustomer() {
	window.location = nlapiResolveURL('SUITELET',
	        'customscript_sut_client_partner_change',
	        'customdeploy_sut_client_partner_change');
}

function copyFromCustomer() {
	var newCp = nlapiGetFieldValue('custpage_new_client_partner');
	console.log("Client Partner : " + newCp);

	if (newCp) {

		var projectCount = nlapiGetLineItemCount('custpage_list_job');

		for (var line = 1; line <= projectCount; line++) {
			nlapiSelectLineItem('custpage_list_job', line)
			nlapiSetCurrentLineItemValue('custpage_list_job', 'new_cp', newCp);
			nlapiCommitLineItem('custpage_list_job');
		}
	}
}