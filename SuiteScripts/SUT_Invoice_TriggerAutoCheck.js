/**
 * @author Jayesh
 */

function suiteletFunction(request, response)
{
	try
	{
		var i_recordID = request.getParameter('custscript_record_id');
		i_recordID = i_recordID.trim();
		
		var params=new Array();
		params['custscript_invoice_id_to_update'] = i_recordID;
	 
		var status = nlapiScheduleScript('customscript_sch_invoice_autocheck',null,params);
		nlapiLogExecution('audit','invoice id:- '+i_recordID);
		//nlapiSubmitField('invoice',parseInt(i_recordID),'custbody_invoice_auto_check_unabled','T');
		
		nlapiSetRedirectURL('RECORD', 'invoice', parseInt(i_recordID), false);
	}
	catch(exception)
	{
	  nlapiLogExecution('DEBUG', 'ERROR',' Exception Caught -->' + exception);	
	}
}

// END SUITELET ====================================================
