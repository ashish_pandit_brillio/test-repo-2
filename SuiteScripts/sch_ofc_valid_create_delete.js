var SEARCHMODULE;
var FORMAT;
var RECORD;
var RUNTIME;
var REDIRECT;
var FILE;
/**
 *@NApiVersion 2.x
 *@NScriptType ScheduledScript
 *@NModuleScope Public
 Author - Sitaram Upadhya
 Dated - 08'July,2021
 */
define(["N/search", "N/format", "N/record", 'N/runtime', 'N/redirect', 'N/file'], runScheduledScript);

//********************** MAIN FUNCTION **********************
function runScheduledScript(search, format, record, runtime, redirect, file) {
    SEARCHMODULE = search;
    FORMAT = format;
    RECORD = record;
    RUNTIME = runtime;
    REDIRECT = redirect;
    FILE = file;

    var returnObj = {};
    returnObj.execute = execute;
    return returnObj;
}

function execute(context) {
    
    var script = RUNTIME.getCurrentScript();
    var ofc_mode = script.getParameter('custscript_ofc_mode');
    var ofc_recID = script.getParameter('custscript_ofc_rec_id');
    var ofc_recType = script.getParameter('custscript_ofc_rec_type');



    //***  Validate - Logic - Used for validation of CSV file uploaded by the User 

    if (_logValidation(ofc_recID) && _logValidation(ofc_mode) && _logValidation(ofc_recType) && (ofc_mode == 'validate')) {
        var o_RecObj = RECORD.load({
            type: ofc_recType,
            id: ofc_recID
        });
        var fileId = o_RecObj.getValue({
            fieldId: 'custrecord_ofc_csv_file'
        });
         var month_v = o_RecObj.getValue({
            fieldId: 'custrecord_ofc_month'
        });
        var year_v = o_RecObj.getValue({
            fieldId: 'custrecord_ofc_year'
        });

        if (_logValidation(fileId)) {
            var emp_list = validate_JE(fileId, ofc_recType, ofc_recID);
            deleteOFCRecord(month_v,year_v)
            var status = createOfcCostTable(emp_list, ofc_recID);
            log.debug({
                title: 'Debug',
                details: JSON.stringify(status)
            });
            if (status == true) {
                RECORD.submitFields({
                    type: 'customrecord_ofc_cost_for_credit_amount',
                    id: parseInt(ofc_recID),
                    values: {
                        custrecord_ofc_current_status: 4
                    },
                    options: {
                        enableSourcing: true,
                        ignoreMandatoryFields: true
                    }
                }); // Validation Completed

            } else {
                RECORD.submitFields({
                    type: 'customrecord_ofc_cost_for_credit_amount',
                    id: parseInt(ofc_recID),
                    values: {
                        custrecord_ofc_current_status: 3
                    },
                    options: {
                        enableSourcing: true,
                        ignoreMandatoryFields: true
                    }
                }); // Failed

            }

        }


    }

    // ***  Delete - Logic - Used for deleting the file if the validation fails -
    // deleteErrorLog() - This will delete the error logs from the system
    // Sets the status to Not Started

    if (_logValidation(ofc_recID) && _logValidation(ofc_mode) && _logValidation(ofc_recType) && (ofc_mode == 'delete')) {
        var o_RecObj = RECORD.load({
            type: ofc_recType,
            id: parseInt(ofc_recID)
        });

        var fileId = o_RecObj.getValue({
            fieldId: 'custrecord_ofc_csv_file'
        });


        if (_logValidation(fileId)) {
            FILE.delete({
                id: parseInt(fileId)
            });
            deleteErrorLog(ofc_recID);
            RECORD.submitFields({
                type: ofc_recType,
                id: parseInt(ofc_recID),
                values: {
                    custrecord_ofc_current_status: 1
                },
                options: {
                    enableSourcing: true,
                    ignoreMandatoryFields: true
                }
            }); // Not Started
        }


    }
  
     //***************Re-run module */

    if (_logValidation(ofc_recID) && _logValidation(ofc_mode) && _logValidation(ofc_recType) && (ofc_mode == 're-run')) {
        var o_RecObj = RECORD.load({
            type: ofc_recType,
            id: parseInt(ofc_recID)
        });

        var fileId = o_RecObj.getValue({
            fieldId: 'custrecord_ofc_csv_file'
        });

        var journalId = o_RecObj.getValue({
            fieldId: 'custrecord_ofc_journal_entry'
        });


        if (_logValidation(fileId)) {
            FILE.delete({
                id: parseInt(fileId)
            });
            if(_logValidation(journalId)){
                var deleted_Record = RECORD.delete({
                    type: RECORD.Type.JOURNAL_ENTRY, 
                    id: journalId,
                });

                log.debug("deleted_Record",deleted_Record);

            }
            deleteErrorLog(ofc_recID);
            RECORD.submitFields({
                type: ofc_recType,
                id: parseInt(ofc_recID),
                values: {
                    custrecord_ofc_current_status: 1
                },
                options: {
                    enableSourcing: true,
                    ignoreMandatoryFields: true
                }
            }); // Not Started
        }


    }
    
    // ***  Create JE - JE creation logic by passing in month and year 

    if (_logValidation(ofc_recID) && _logValidation(ofc_mode) && _logValidation(ofc_recType) && (ofc_mode == 'create')) {
        var o_RecObj = RECORD.load({
            type: ofc_recType,
            id: parseInt(ofc_recID)
        });

         var month_v = o_RecObj.getValue({
            fieldId: 'custrecord_ofc_month'
        });
        var year_v = o_RecObj.getValue({
            fieldId: 'custrecord_ofc_year'
        });
        var subsidiary_v_ofc = o_RecObj.getValue({
            fieldId: 'custrecord_ofc_subsidiary'
        });
        try{
           createJE(month_v,year_v,ofc_recID,ofc_recType,subsidiary_v_ofc);
        } catch(e){
          log.debug('error on create JE',e);
            RECORD.submitFields({
                    type: ofc_recType,
                    id: parseInt(ofc_recID),
                    values: {
                        custrecord_ofc_log_cost: e.message
                    },
                    options: {
                        enableSourcing: true,
                        ignoreMandatoryFields: true
                    }
                });

	RECORD.submitFields({
                    type: ofc_recType,
                    id: parseInt(ofc_recID),
                    values: {
                        custrecord_ofc_current_status: 3
                    },
                    options: {
                        enableSourcing: true,
                        ignoreMandatoryFields: true
                    }
                });
	
        }
       
        

       


    }



}

// Below function "validate_JE" is used to delete the ErrorLog records, parse the CSV file

function validate_JE(fileId, ofc_recType, ofc_recID) {

    try {
        deleteErrorLog(ofc_recID);
        if (_logValidation(ofc_recID)) {
            var primaryFile = FILE.load({
                id: fileId
            });
            var fileContent = primaryFile.getContents();
            var fileType = primaryFile.fileType;

            var recObj = RECORD.load({
                type: ofc_recType,
                id: ofc_recID
            });
            var i_month = recObj.getText({
                fieldId: 'custrecord_ofc_month'
            });
            var i_month_v = recObj.getValue({
                fieldId: 'custrecord_ofc_month'
            });

            var i_year = recObj.getText({
                fieldId: 'custrecord_ofc_year'
            });
            var i_year_v = recObj.getValue({
                fieldId: 'custrecord_ofc_year'
            });

           // var cost_given = recObj.getValue({
           //      fieldId: 'custrecord_ofc_additional_cost'
           // });

            var subsidiary_name = recObj.getValue({
                fieldId: 'custrecord_ofc_subsidiary'
            });
          
           log.debug('subsidiary_name', JSON.stringify(subsidiary_name)); //added

            var d_start_date1 = get_current_month_start_date(i_month, i_year);
            var d_start_date = new Date(d_start_date1);

            var d_end_date1 = get_current_month_end_date(i_month, i_year);
            var d_end_date = new Date(d_end_date1);


            var facilityCost = getFacilityCost(ofc_recID);
            var facility_obj = facility_Exchange_cost(ofc_recID);
           log.debug('facilityCost', facilityCost);
            log.debug('facilityCost', JSON.stringify(facilityCost));

            var fusionIDs = [];
            var seatCountFusionID = {};

            if (fileType == 'CSV') {
                if (_logValidation(fileContent)) {
                    var splitLine = fileContent.split("\n");//replace("\r", "").split(",");
                    var updated_length = splitLine.length - 1;
                    var csv_length = splitLine.length;
					
					var fusionIDList = getCurentFusionIDs();
					log.debug('fusionIDList',fusionIDList);
                    log.debug('updated_length',updated_length);
                    //chnaged - i<=updated_length
                    for (var i = 1; i < updated_length; i++) {
                        var errMessage = '';

                        var word = (splitLine[i]).replace("\r", "").split(",");
                        log.debug("i "+i,word);

                        var fusionID = '';
                        var seatCount = '';
                        fusionID = word[0];
                        seatCount = word[1];
                        if(i==1){
                          log.debug('fusionID',fusionID+ ', '+ typeof(fusionID));
                          log.debug('seatCount',seatCount+ ', '+ typeof(seatCount));
                        }
                        if(fusionID == '' || seatCount == ''){
                          log.debug('entered');
                          errMessage = 'Row is empty at line no - ' + i;
                        } else {
                          if (word.length != 2 && word.length != 0) {
                            errMessage = 'Numbers of columns are not 2 in CSV file ';
                        }
						}
						
						if(fusionIDList.indexOf(fusionID) == -1){
							errMessage = 'Fusion ID '+fusionID+ ' is not present in the system';
						}
                        
                        fusionID = parseInt(word[0]);
                        

                        

                        if (errMessage != '') {

                            //Create Custom Record
                            var errorRec = RECORD.create({
                                type: 'customrecord_je_upload_error',
                                isDynamic: true
                            });

                            errorRec.setValue({
                                fieldId: 'custrecorderror_detail_le_upload',
                                value: errMessage
                            });
                            errorRec.setValue({
                                fieldId: 'custrecord_ofc_cost_credit_amount_record',
                                value: parseInt(ofc_recID)
                            });


                            RECORD.submitFields({
                                type: ofc_recType,
                                id: parseInt(ofc_recID),
                                values: {
                                    custrecord_ofc_current_status: 2
                                },
                                options: {
                                    enableSourcing: true,
                                    ignoreMandatoryFields: true
                                }
                            });


                            //log.debug({title: 'Row is empty or columns are greater than 3',details: 'File is empty'});
                            //Save the Record
                            var recId = errorRec.save({
                                enableSourcing: false,
                                ignoreMandatoryFields: false
                            });
                            
                            return

                        } else {
                          if(fusionID!='' && seatCount!=''){
                            fusionIDs.push(fusionID);
                            seatCountFusionID[fusionID] = seatCount;
                          }
                            

                        }
                    }
                    
                  //Checks whether fusion IDs are unique or not 
                   var arrDuplicates =  hasDuplicates(fusionIDs)
                   if(arrDuplicates){
                     errMessage = 'CSV file contains duplicate fusion IDs'
                      var errorRec = RECORD.create({
                                type: 'customrecord_je_upload_error',
                                isDynamic: true
                            });

                            errorRec.setValue({
                                fieldId: 'custrecorderror_detail_le_upload',
                                value: errMessage
                            });
                            errorRec.setValue({
                                fieldId: 'custrecord_ofc_cost_credit_amount_record',
                                value: parseInt(ofc_recID)
                            });


                            RECORD.submitFields({
                                type: ofc_recType,
                                id: parseInt(ofc_recID),
                                values: {
                                    custrecord_ofc_current_status: 2
                                },
                                options: {
                                    enableSourcing: true,
                                    ignoreMandatoryFields: true
                                }
                            });


                            //log.debug({title: 'Row is empty or columns are greater than 3',details: 'File is empty'});
                            //Save the Record
                            var recId = errorRec.save({
                                enableSourcing: false,
                                ignoreMandatoryFields: false
                            });
                      return
                     
                     
                     
                   }


                    if (fusionIDs.length > 0) {
                        log.debug('fusionIDs',fusionIDs.length);
                        var finalFilter = [];
                        

                        for (var fusionIndex = 0; fusionIndex < fusionIDs.length; fusionIndex++) {
                            //var filterTemp1 = [];
                            var filtertemp = ["custentity_fusion_empid", "is"];
                            var fusionIDtoStr = fusionIDs[fusionIndex].toString();
                            filtertemp.push(fusionIDtoStr);//fusionIDtoStr
                          
                           
                           // filterTemp1.push(filtertemp);
                            finalFilter.push(filtertemp);
                            if (fusionIndex != fusionIDs.length - 1) {
                                finalFilter.push("OR");
                            }

                        }

                         log.debug('finalFilter',finalFilter);

                        var employeeSearchObj = SEARCHMODULE.create({
                            type: "employee",
                            filters: finalFilter,
                            columns: [
                                SEARCHMODULE.createColumn({
                                    name: "internalid",
                                    label: "Internal ID"
                                }),
                                 SEARCHMODULE.createColumn({
                                   name: "custentity_fusion_empid",
                                   label: "Fusion Employee Id"
                                 })
                            ]
                        });
                        var searchResultCount = employeeSearchObj.runPaged().count;
                        
                         log.debug("employeeSearchObj result count",searchResultCount);
                        var employeeIDs = new Array();
                       
                        employeeSearchObj.run().each(function(result) {
                            employeeIDs.push(result.getValue({
                                name: "internalid"
                            }));
                          
                            return true;
                        });
                      
                      
                      
                      
                      log.debug("employeeIDs",employeeIDs);
                      log.debug("d_start_date1",d_start_date1);
                      log.debug("d_end_date1",d_end_date1);
                      if(employeeIDs.length == 1){
                        employeeIDs = employeeIDs[0]
                      }
                      log.debug("employeeIDs",employeeIDs);
                      
                        var resourceallocationSearchObj = SEARCHMODULE.create({
                            type: "resourceallocation",
                            filters: [
                                ["resource", "anyof", employeeIDs],
                                "AND",
                                ["enddate", "onorafter", d_start_date1],
                                "AND",
                                ["startdate", "onorbefore", d_end_date1],
                                "AND",
                                ["job.custentity_project_allocation_category","anyof","1"]

                            ],
                            columns: [
                                SEARCHMODULE.createColumn({
                                    name: "company",
                                    label: "Project"
                                }),
                                SEARCHMODULE.createColumn({
                                    name: "customer",
                                    label: "Customer"
                                }),
                                SEARCHMODULE.createColumn({
                                    name: "percentoftime",
                                    label: "Percentage of Time"
                                }),
                                SEARCHMODULE.createColumn({
                                    name: "location",
                                    join: "employee",
                                    label: "Location"
                                }),
                                SEARCHMODULE.createColumn({
                                    name: "startdate",
                                    label: "Billing Start Date"
                                }),
                                SEARCHMODULE.createColumn({
                                    name: "enddate",
                                    label: "Billing End Date"
                                }),
                                SEARCHMODULE.createColumn({
                                    name: "custevent_practice",
                                    label: "Practice"
                                }),
                                SEARCHMODULE.createColumn({
                                    name: "resource",
                                    label: "Resource"
                                }),
                                SEARCHMODULE.createColumn({
                                    name: "subsidiary",
                                    join: "employee",
                                    label: "Subsidiary"
                                }),
                                SEARCHMODULE.createColumn({
                                    name: "custevent_practice",
                                    label: "Practice"
                                }),
                                SEARCHMODULE.createColumn({
                                    name: "custentity_vertical",
                                    join: "job",
                                    label: "Vertical"
                                }),
                                SEARCHMODULE.createColumn({
                                    name: "territory",
                                    join: "customer",
                                    label: "Territory"
                                }),
                                SEARCHMODULE.createColumn({
                                    name: "subsidiary",
                                    join: "customer",
                                    label: "Primary Subsidiary"
                                }),
                                SEARCHMODULE.createColumn({
                                    name: "custeventwlocation",
                                    label: "Work Location"
                                }),
                                SEARCHMODULE.createColumn({
                                    name: "custevent_workcityra",
                                    label: "Work City"
                                }),
                                SEARCHMODULE.createColumn({
                                    name: "custevent_workstatera",
                                    label: "Work State"
                                }),
                                SEARCHMODULE.createColumn({
                                    name: "customer",
                                    label: "Customer"
                                }),
                                SEARCHMODULE.createColumn({
                                    name: "custentity_fusion_empid",
                                    join: "employee",
                                    label: "Fusion Employee Id"
                                })
                            ]
                        });
                        var allocationCount = resourceallocationSearchObj.runPaged().count;
                        log.debug('allocationCount',allocationCount);

                        var allocationResults_ = [];
                        var searchid_ = 0;

                        do {
                            var resultslice_ = resourceallocationSearchObj.run().getRange({
                                start: searchid_,
                                end: searchid_ + 1000
                            })

                            for (var re in resultslice_) {

                                allocationResults_.push(resultslice_[re]);
                                searchid_++;
                            }
                        } while (resultslice_.length >= 1000);

                        var counterIndex = 0;
                        var emp_detail_arr = {};

                        for (var index = 0; index < allocationResults_.length; index++) {
                            log.debug('Allocation Debug', JSON.stringify(allocationResults_[index]));
                            var project = allocationResults_[index].getValue({
                                name: "company"
                            });
                             log.debug('1 project', "1");
                            var customer = allocationResults_[index].getValue({
                                name: "customer"
                            });
                            log.debug('1 customer', "1");
                            var percentoftime = allocationResults_[index].getValue({
                                name: "percentoftime"
                            });
                            log.debug('1 percentoftime', "1");
                            var emp_location = allocationResults_[index].getValue({
                                name: "location",
                                join: "employee"
                            });
                            log.debug('1 emp_location', "1");
                            var billingStartDate1 = allocationResults_[index].getValue({
                                name: "startdate"
                            }); //need to convert to date
                            log.debug('1 billingStartDate1', "1");
                            var billingStartDate = new Date(billingStartDate1);
                            log.debug('1 billingStartDate', "1");



                            var billingEndDate1 = allocationResults_[index].getValue({
                                name: "enddate"
                            }); //need to convert to date
                            log.debug('1 billingEndDate1', "1");
                            var billingEndDate = new Date(billingEndDate1);
                            log.debug('1 billingEndDate', "1");

                            var practice = allocationResults_[index].getValue({
                                name: "custevent_practice"
                            });
                            log.debug('1 practice', "1");
                            var resource = allocationResults_[index].getValue({
                                name: "resource"
                            });
                            log.debug('1 resource', "1");
                            var resource_name = allocationResults_[index].getText({
                                name: "resource"
                            });
                            log.debug('1 resource_name', "1");
                            var resourceSubsidiary = allocationResults_[index].getValue({
                                name: "subsidiary",
                                join: "employee"
                            });
                           log.debug('1 resourceSubsidiary', "1");
                            var projectVertical = allocationResults_[index].getValue({
                                name: "custentity_vertical",
                                join: "job"
                            });
                           log.debug('1 projectVertical', "1");
                            var customerTerittory = allocationResults_[index].getValue({
                                name: "territory",
                                join: "customer"
                            });
                           log.debug('1 customerTerittory', "1");
                            var customerSubsidiary = allocationResults_[index].getValue({
                                name: "subsidiary",
                                join: "customer"
                            });
                           log.debug('1 customerSubsidiary', "1");
                            var workLocation = allocationResults_[index].getValue({
                                name: "custeventwlocation"
                            });
                           log.debug('1 workLocation', "1");
                            var workCity = allocationResults_[index].getValue({
                                name: "custevent_workcityra"
                            });
                           log.debug('1 workCity', "1");
                            var workState = allocationResults_[index].getValue({
                                name: "custevent_workstatera"
                            });
                            log.debug('1 workState', "1");
                            var fusionID = allocationResults_[index].getValue({
                                name: "custentity_fusion_empid",
                                join: "employee"
                            });
                            log.debug('1 fusionID', "1");

                            var subsidiary_employee = allocationResults_[index].getValue({
                                name: "subsidiary",
                                join: "employee"
                            });  //added

                            var final_credit_amount = 0;

                           



                            percentoftime = percentoftime.toString();
                            percentoftime = percentoftime.split('.');
                            percentoftime = percentoftime[0].toString().split('%');
                            var final_prcnt_allocation = parseFloat(percentoftime) / parseFloat(100);
                            final_prcnt_allocation = parseFloat(final_prcnt_allocation); 
                           log.debug('resourceSubsidiary', resourceSubsidiary+ ', '+subsidiary_name);
                           log.debug('facilityCost[emp_location]',facilityCost[emp_location]);
                            if (subsidiary_name.indexOf(resourceSubsidiary)!=-1) {
                              log.debug('passed1');
                                if (facilityCost[emp_location]) {
                                  log.debug('passed2');
                                    var ofc_cost_india = parseFloat(facilityCost[emp_location]);

                                    var cost_given = facility_obj[emp_location].custrecord_ofc_addition_cost;
                                    var exchange_rate = facility_obj[emp_location].custrecord_exchnage_rate_ofc;




                                    if (billingStartDate <= d_start_date && billingEndDate >= d_end_date) {
                                        var daysBtwMonth = getDatediffIndays(d_start_date, d_end_date);
                                       

                                    } else if (billingStartDate <= d_start_date && billingEndDate <= d_end_date) {
                                        var daysBtwMonth = getDatediffIndays(d_start_date, billingEndDate);
                                        

                                    } else if (billingStartDate >= d_start_date && billingEndDate >= d_end_date) {
                                        var daysBtwMonth = getDatediffIndays(billingStartDate, d_end_date);
                                        

                                    } else if (billingStartDate >= d_start_date && billingEndDate <= d_end_date) {
                                        var daysBtwMonth = getDatediffIndays(billingStartDate, billingEndDate);
                                       

                                    }

                                   

                                    var numberDays = getDatediffIndays(d_start_date, d_end_date);
                                  
                                    var projectAllocationdays = daysBtwMonth / numberDays;
                                   
                                    var projectAllocationPercent = parseFloat(projectAllocationdays) * final_prcnt_allocation;
                                    var seatCount = seatCountFusionID[fusionID];
                                   
                                    var seatAllocationPercent = '';
                                    if (_logValidation(seatCount)) {
                                        seatAllocationPercent = parseFloat(projectAllocationPercent) * parseFloat(seatCount);

                                    }
                                    

                                    var cost_as_per_allocation = parseFloat(projectAllocationPercent) * ofc_cost_india;
                                  
                                    var cost_as_per_seat_count = ofc_cost_india * parseFloat(seatAllocationPercent) * parseFloat(cost_given);
                                    

                                    final_credit_amount = cost_as_per_allocation - cost_as_per_seat_count;
                                     log.debug('FINAL CREDIT AMOUNT ',final_credit_amount+', '+resource_name+', '+seatCount + typeof(seatCount));
                                     //log.debug('SEAT COUNT',seatCount!= 0 +', '+seatCount != 1 + ", "+seatCount!= "0"+ ', '+seatCount!= "1"  );
                                  
                                   //&& ((seatCount!== 0) || (seatCount !== 1) || (seatCount!== "0") || (seatCount!== "1") )
                                    if(seatCount== 0 || seatCount== 1){
                                      continue;
                                    }

                                    if (cost_as_per_allocation > cost_as_per_seat_count) {
                                     
                                        log.debug('SEat count',seatCount+ ", "+ seatCount!= 0);
                                     
                                        emp_detail_arr[counterIndex] = {
                                            'resource': resource,
                                            'resource_name': resource_name,
                                            'project': project,
                                            'customer': customer,
                                            'percent_of_time': final_prcnt_allocation,
                                            'emp_location': emp_location,
                                            'billingStartDate': billingStartDate,
                                            'billingEndDate': billingEndDate,
                                            'practice': practice,
                                            'month': i_month_v,
                                            'year': i_year_v,
                                            'final_credit_amount': parseFloat(final_credit_amount),
                                            'customer_subsidiary': customerSubsidiary,
                                            'custrecord_ofc_cost': cost_as_per_allocation,
                                            'custrecord_ofc_days_of_allocation': daysBtwMonth,
                                            'custrecord_ofc_days_between_months': numberDays,
                                            'custrecord_ofc_project_allocation_percen': projectAllocationPercent,
                                            'custrecord_ofc_seat_alloc_percent': seatAllocationPercent,
                                            'custrecord_cost_for_seat_count': cost_as_per_seat_count,
                                            'custrecord_ofc_seat_count': seatCount,
                                            'custrecord_ofc_resource_subidiary_': subsidiary_employee,
                                            'custrecord_exchnage_rate_': exchange_rate,
                                            'custrecord_ofc_is_credit_cost': true,
                                            

                                        };
                                        counterIndex++;
                                        


                                    
                                    }


                                }


                            }


                        }

                        return emp_detail_arr;




                    }



                } else {
               

                    //Create Custom Record
                    var errorRec = RECORD.create({
                        type: 'customrecord_je_upload_error',
                        isDynamic: true
                    });

                    errorRec.setValue({
                        fieldId: 'custrecorderror_detail_le_upload',
                        value: 'Uploaded file is empty'
                    });
                    errorRec.setValue({
                        fieldId: 'custrecord_ofc_cost_credit_amount_record',
                        value: parseInt(ofc_recID)
                    });


                    RECORD.submitFields({
                        type: ofc_recType,
                        id: parseInt(ofc_recID),
                        values: {
                            custrecord_ofc_current_status: 2
                        },
                        options: {
                            enableSourcing: true,
                            ignoreMandatoryFields: true
                        }
                    });


                    //log.debug({title: 'File is empty',details: 'File is empty'});
                    //Save the Record
                    var recId = errorRec.save({
                        enableSourcing: false,
                        ignoreMandatoryFields: false
                    });
                     log.debug('Error-Record',recId);


                }




            } else {

                //Create Custom Record
                var errorRec = RECORD.create({
                    type: 'customrecord_je_upload_error',
                    isDynamic: true
                });

                errorRec.setValue({
                    fieldId: 'custrecorderror_detail_le_upload',
                    value: 'Uploaded file is not CSV file'
                });
                errorRec.setValue({
                    fieldId: 'custrecord_ofc_cost_credit_amount_record',
                    value: parseInt(ofc_recID)
                });


                RECORD.submitFields({
                    type: ofc_recType,
                    id: parseInt(ofc_recID),
                    values: {
                        custrecord_ofc_current_status: 2
                    },
                    options: {
                        enableSourcing: true,
                        ignoreMandatoryFields: true
                    }
                });


                log.debug({title: 'Not CSV File Exception',details: 'Not CSV File Exception'});
                //Save the Record
                var recId = errorRec.save({
                    enableSourcing: false,
                    ignoreMandatoryFields: false
                });
                /// log.debug('Error-Record',recId);

            }

            REDIRECT.toRecord({
                type: 'custrecord_ofc_cost_credit_amount_record',
                id: ofc_recID
            });




        }


    } catch (e) {
        log.debug('exception', e);
      RECORD.submitFields({
            type: ofc_recType,
            id: parseInt(ofc_recID),
            values: {
                custrecord_ofc_log_cost: e.message
            },
            options: {
                enableSourcing: true,
                ignoreMandatoryFields: true
            }
        });
        
     RECORD.submitFields({
            type: ofc_recType,
            id: parseInt(ofc_recID),
            values: {
                custrecord_ofc_current_status: 3
            },
            options: {
                enableSourcing: true,
                ignoreMandatoryFields: true
            }
        });
        


    }




}


// deleteErrorLog - It is used to delete all the error logs taking in record ID as the parameter

function deleteErrorLog(Rec_Id) {
    //Delete error logs
    var customrecord_je_upload_errorSearchObj = SEARCHMODULE.create({
        type: "customrecord_je_upload_error",
        filters: [
            ["custrecord_ofc_cost_credit_amount_record", "anyof", Rec_Id]
        ],
        columns: [
            SEARCHMODULE.createColumn({
                name: "internalid",
                label: "Internal ID"
            })
        ]
    });
    var searchResultCount = customrecord_je_upload_errorSearchObj.runPaged().count;


    var a_results_search = customrecord_je_upload_errorSearchObj.run().getRange({
        start: 0,
        end: 1000
    });
    if (_logValidation(a_results_search)) {
       
        for (var k = 0; k < a_results_search.length; k++) {
            
            var errorId = a_results_search[k].getValue({
                name: "internalid",
                label: "Internal ID"
            });

           

            var deleted_Record = RECORD.delete({
                type: 'customrecord_je_upload_error',
                id: errorId,
            });
           
        }
    }
}

function _logValidation(value) {
    if (value != 'null' && value != null && value != null && value != '' && value != undefined && value != undefined && value != 'undefined' && value != 'undefined' && value != 'NaN' && value != NaN) {
        return true;
    } else {
        return false;
    }
}

//getFacilityCost - Used to get an object consisting of Location and their corresponding cost 

function getFacilityCost(ofc_recID) {

    var customrecord_location_facility_costSearchObj = SEARCHMODULE.create({
        type: "customrecord_location_facility_cost",
        filters: [
            ["custrecord_ofc_ref_facility_cost_credit", "anyof", parseInt(ofc_recID)]
        ],
        columns: [
            SEARCHMODULE.createColumn({
                name: "custrecord_location_facility_cost",
                label: "Location For Facility Cost"
            }),
            SEARCHMODULE.createColumn({
                name: "custrecord_ofc_cost_india",
                label: "OFC COST INDIA"
            })
        ]
    });
    var searchResultCount = customrecord_location_facility_costSearchObj.runPaged().count;
    var facilityCost = {};
    customrecord_location_facility_costSearchObj.run().each(function(result) {
        var location_facility = result.getValue({
            name: "custrecord_location_facility_cost"
        });
        var ofc_cost = result.getValue({
            name: "custrecord_ofc_cost_india"
        });
        facilityCost[location_facility] = ofc_cost;
        return true;
    });

    return facilityCost;


}

function facility_Exchange_cost(ofc_recID){

    var customrecord_location_facility_costSearchObj = SEARCHMODULE.create({
        type: "customrecord_location_facility_cost",
        filters:
        [
            ["custrecord_ofc_ref_facility_cost_credit", "anyof", parseInt(ofc_recID)]
        ],
        columns:
        [
           SEARCHMODULE.createColumn({name: "custrecord_location_facility_cost", label: "Location For Facility Cost"}),
           SEARCHMODULE.createColumn({name: "custrecord_ofc_cost_india", label: "OFC COST INDIA"}),
           SEARCHMODULE.createColumn({name: "custrecord_exchnage_rate_ofc", label: "Exchange Rate"}),
           SEARCHMODULE.createColumn({name: "custrecord_ofc_addition_cost", label: "Addition Cost "})
        ]
     });
     var searchResultCount = customrecord_location_facility_costSearchObj.runPaged().count;
     log.debug("customrecord_location_facility_costSearchObj result count",searchResultCount);
     
     var facilityCost = {};
     
     customrecord_location_facility_costSearchObj.run().each(function(result){
       
       var location_facility = result.getValue({
            name: "custrecord_location_facility_cost"
        });

        var rate_exchange = result.getValue({
            name: "custrecord_exchnage_rate_ofc"
        });
        var additional_cost = result.getValue({
            name: "custrecord_ofc_addition_cost"
        });
        facilityCost[location_facility] = {
            'custrecord_exchnage_rate_ofc':rate_exchange,
            'custrecord_ofc_addition_cost': additional_cost

        }
        return true;
        
     });

     return facilityCost;

}






function createOfcCostTable(emp_list, ofc_recID) {
   log.debug("EMP_LIST",JSON.stringify(emp_list));

    try {
        var flag = false;
      


        if (_logValidation(emp_list)) {

            var size = Object.keys(emp_list).length;
            log.debug('size ', size);
            for (var j = 0; j < size; j++) {
            log.debug("JSON_stringify ",JSON.stringify(emp_list[j]))

                try {
                    var createRecord = RECORD.create({
                        type: 'customrecord_ofc_cost_custom_table',
                        isDynamic: true
                    });
                   
                    createRecord.setValue({
                        fieldId: 'custrecord_ofc_resource',
                        value: emp_list[j].resource
                    });
                    

                    createRecord.setValue({
                        fieldId: 'custrecord_ofc_cost_project',
                        value: emp_list[j].project
                    });
                    

                    createRecord.setValue({
                        fieldId: 'custrecord_ofc_cost_customer',
                        value: emp_list[j].customer
                    });
                   

                    createRecord.setValue({
                        fieldId: 'custrecord_ofc_percent_allocation',
                        value: emp_list[j].percent_of_time * 100
                    });
                    

                    createRecord.setValue({
                        fieldId: 'custrecord_ofc_cost_location',
                        value: emp_list[j].emp_location
                    });
                    

                    createRecord.setValue({
                        fieldId: 'custrecord_ofc_cost_billing_start_date',
                        value: emp_list[j].billingStartDate
                    });
                   

                    createRecord.setValue({
                        fieldId: 'custrecord_ofc_billion_end_date',
                        value: emp_list[j].billingEndDate
                    });
                    

                    if (emp_list[j].practice != 320) //Added on 01/09/2020  because 320 is practice is inactive insystem
                    {
                        createRecord.setValue({
                            fieldId: 'custrecord_ofc_cost_practice',
                            value: emp_list[j].practice
                        });
                    }
                    

                    createRecord.setValue({
                        fieldId: 'custrecord_ofc_cost_month',
                        value: emp_list[j].month
                    });
                  

                    createRecord.setValue({
                        fieldId: 'custrecord_ofc_cost_year',
                        value: emp_list[j].year
                    });
                   

                    createRecord.setValue({
                        fieldId: 'custrecord_ofc_cost_for_credit',
                        value: emp_list[j].final_credit_amount
                    });
                 

                    createRecord.setValue({
                        fieldId: 'custrecord_ofc_days_of_allocation',
                        value: emp_list[j].custrecord_ofc_days_of_allocation
                    });
                 

                    createRecord.setValue({
                        fieldId: 'custrecord_ofc_days_between_months',
                        value: emp_list[j].custrecord_ofc_days_between_months
                    });
                   

                    createRecord.setValue({
                        fieldId: 'custrecord_ofc_seat_alloc_percent',
                        value: emp_list[j].custrecord_ofc_seat_alloc_percent
                    });
                  

                    createRecord.setValue({
                        fieldId: 'custrecord_cost_for_seat_count',
                        value: parseFloat(emp_list[j].custrecord_cost_for_seat_count)
                    });
                 

                    createRecord.setValue({
                        fieldId: 'custrecord_ofc_seat_count',
                        value: parseFloat(emp_list[j].custrecord_ofc_seat_count)
                    });
                 

                    createRecord.setValue({
                        fieldId: 'custrecord_ofc_is_credit_cost',
                        value: emp_list[j].custrecord_ofc_is_credit_cost
					});
                 
					
					createRecord.setValue({
                        fieldId: 'custrecord_ofc_project_allocation_percen',
                        value: emp_list[j].custrecord_ofc_project_allocation_percen
                    });
                    
                    createRecord.setValue({
                        fieldId: 'custrecord_ofc_resource_subidiary_',
                        value: emp_list[j].custrecord_ofc_resource_subidiary_
                    });
                    
                    createRecord.setValue({
                        fieldId: 'custrecord_exchnage_rate_',
                        value: emp_list[j].custrecord_exchnage_rate_
					});
                  
					
					createRecord.setValue({
                        fieldId: 'custrecord_ofc_cost',
                        value: emp_list[j].custrecord_ofc_cost
                    });

               

                    var recordId = createRecord.save();
                    log.debug('recordId', recordId);

                } catch (e) {
                    log.debug('Catch Block', 'Inside Catch');
                   
                    RECORD.submitFields({
                        type: 'customrecord_ofc_cost_for_credit_amount',
                        id: parseInt(ofc_recID),
                        values: {
                            custrecord_ofc_log_cost: e.message
                        },
                        options: {
                            enableSourcing: true,
                            ignoreMandatoryFields: true
                        }
                    });
                    flag = false;
                    return flag;
                }

            }
            flag = true;
            return flag;
        }
    } catch (e) {
        log.debug('err', e);
        log.debug('err', 'Error in creating the custom record for OFC');
        
        RECORD.submitFields({
            type: 'customrecord_ofc_cost_for_credit_amount',
            id: parseInt(ofc_recID),
            values: {
                custrecord_ofc_log_cost: e.message
            },
            options: {
                enableSourcing: true,
                ignoreMandatoryFields: true
            }
        });
        
RECORD.submitFields({
            type: 'customrecord_ofc_cost_for_credit_amount',
            id: parseInt(ofc_recID),
            values: {
                custrecord_ofc_current_status: 3
            },
            options: {
                enableSourcing: true,
                ignoreMandatoryFields: true
            }
        });
      
        return false;
    }


}


function deleteOFCRecord(month,year){

var customrecord_ofc_cost_custom_tableSearchObj = SEARCHMODULE.create({
   type: "customrecord_ofc_cost_custom_table",
   filters:
   [
      ["custrecord_ofc_cost_month","anyof",month], 
      "AND", 
      ["custrecord_ofc_cost_year","anyof",year], 
      "AND", 
      ["custrecord_ofc_is_credit_cost","is","T"] 
   ],
   columns:
   [
      SEARCHMODULE.createColumn({name: "internalid", label: "Internal ID"})
   ]
});
var searchResultCount = customrecord_ofc_cost_custom_tableSearchObj.runPaged().count;

var ofcResults_ = [];
                        var searchid_ = 0;

                        do {
                            var resultslice_ = customrecord_ofc_cost_custom_tableSearchObj.run().getRange({
                                start: searchid_,
                                end: searchid_ + 1000
                            })

                            for (var re in resultslice_) {

                                ofcResults_.push(resultslice_[re]);
                                searchid_++;
                            }
                        } while (resultslice_.length >= 1000);

                        var counterIndex = 0;
                        var emp_detail_arr = {};

	for (var index = 0; index < ofcResults_.length; index++) {
                            var internalID = ofcResults_[index].getValue({
                                name: "internalid"
                            });
							
			 var deleted_Record = RECORD.delete({
                type: 'customrecord_ofc_cost_custom_table',
                id: internalID,
            });
							
	}
	
}

//changed the duplicate element finding logic
function hasDuplicates(array) {
    for (var i = 0; i < array.length; i++) {
    if (array.indexOf(array[i]) !== array.lastIndexOf(array[i])) {
      return true
    }
  }
  return false
}


function getCurentFusionIDs(){
 
	var employeeSearchObj = SEARCHMODULE.create({
		type: "employee",
		filters:
		[
		],
		columns:
		[
		   SEARCHMODULE.createColumn({name: "custentity_fusion_empid", label: "Fusion Employee Id"})
		]
	 });
	 var searchResultCount = employeeSearchObj.runPaged().count;
	 var fusionIDforEmp = new Array();


	 var fuionResults_ = [];
                        var searchid_ = 0;

                        do {
                            var resultslice_ = employeeSearchObj.run().getRange({
                                start: searchid_,
                                end: searchid_ + 1000
                            })

                            for (var re in resultslice_) {

                                fuionResults_.push(resultslice_[re].getValue({name: "custentity_fusion_empid"}));
                                searchid_++;
                            }
						} while (resultslice_.length >= 1000);
	
	return fuionResults_;

}




function createJE(i_month,i_year,ofc_recID,ofc_recType,subsidiary_v_ofc){

    try{
      
      var vendorList = { };
    var customrecord_default_vendor_mappingSearchObj = SEARCHMODULE.create({
       type: "customrecord_default_vendor_mapping",
       filters:
       [
       ],
       columns:
       [
          SEARCHMODULE.createColumn({name: "custrecord_subsidiary_je", label: "Subsidiary"}),
          SEARCHMODULE.createColumn({name: "custrecord_default_vendor", label: "Vendor"})
       ]
    });
    var searchResultCount = customrecord_default_vendor_mappingSearchObj.runPaged().count;
    log.debug("customrecord_default_vendor_mappingSearchObj result count",searchResultCount);
    
    
    var defaultVendorResults_ = [];
    var searchid_ = 0;
    
    do {
        var resultslice_ = customrecord_default_vendor_mappingSearchObj.run().getRange({
            start: searchid_,
            end: searchid_ + 1000
        })
    
        for (var re in resultslice_) {
    
            defaultVendorResults_.push(resultslice_[re]);
            searchid_++;
        }
    } while (resultslice_.length >= 1000);
    
    for (var index = 0; index < defaultVendorResults_.length; index++) {
                                
    var subsidiary_vendor = defaultVendorResults_[index].getValue({
        name: "custrecord_subsidiary_je"
    });
     
    var vendor_default = defaultVendorResults_[index].getValue({
        name: "custrecord_default_vendor"
    });
    
    vendorList[subsidiary_vendor] = vendor_default
    
    }  

    var JEIDS = new Array();

    for(var ofc_index =0;ofc_index<subsidiary_v_ofc.length;ofc_index++){
    
    
    var customrecord_ofc_cost_custom_tableSearchObj = SEARCHMODULE.create({
    type: "customrecord_ofc_cost_custom_table",
    filters:
    [
      ["custrecord_ofc_cost_month","anyof", i_month], 
      "AND", 
      ["custrecord_ofc_cost_year","anyof", i_year], 
      "AND", 
      ["custrecord_ofc_is_credit_cost","is","T"],
      "AND",
      ["custrecord_ofc_resource_subidiary_","anyof",subsidiary_v_ofc[ofc_index]]
    ],
    columns:
    [
      SEARCHMODULE.createColumn({name: "scriptid", label: "Script ID"}),
      SEARCHMODULE.createColumn({name: "custrecord_ofc_resource", label: "Resource"}),
      SEARCHMODULE.createColumn({name: "custrecord_ofc_cost_project", label: "Project"}),
      SEARCHMODULE.createColumn({name: "custrecord_ofc_cost_billing_start_date", label: "Start Date"}),
      SEARCHMODULE.createColumn({name: "custrecord_ofc_billion_end_date", label: "End Date"}),
      SEARCHMODULE.createColumn({name: "custrecord_ofc_cost", label: "OFC Cost"}),
      SEARCHMODULE.createColumn({name: "custrecord_ofc_cost_month", label: "Month"}),
      SEARCHMODULE.createColumn({name: "custrecord_ofc_cost_year", label: "Year"}),
      SEARCHMODULE.createColumn({name: "custrecord_ofc_cost_location", label: "Location"}),
      SEARCHMODULE.createColumn({name: "custrecord_ofc_cost_for_credit", label: "OFC Cost For Credit"}),
      SEARCHMODULE.createColumn({name: "custrecord_ofc_days_of_allocation", label: "Days of Allocation"}),
      SEARCHMODULE.createColumn({name: "custrecord_ofc_days_between_months", label: "Days between Months"}),
      SEARCHMODULE.createColumn({name: "custrecord_ofc_project_allocation_percen", label: "Project Allocation Percent"}),
      SEARCHMODULE.createColumn({name: "custrecord_ofc_seat_alloc_percent", label: "Seat Allocation Percent"}),
      SEARCHMODULE.createColumn({name: "custrecord_cost_for_seat_count", label: "Cost for Seat Count"}),
      SEARCHMODULE.createColumn({name: "custrecord_ofc_seat_count", label: "Seat Count"}),
      SEARCHMODULE.createColumn({name: "custrecord_ofc_is_credit_cost", label: "Is Credit Cost"}),
      SEARCHMODULE.createColumn({name: "custrecord_ofc_cost_customer", label: "Customer"}),
      SEARCHMODULE.createColumn({name: "custrecord_exchnage_rate_", label: "Exchange Rate"})

    ]
    });
    
    
    var customrecord_ofc = customrecord_ofc_cost_custom_tableSearchObj.runPaged().count;
    
    var ofc_Cost_Results_ = [];
    var searchid_ = 0;
    
    do {
    var resultslice_ = customrecord_ofc_cost_custom_tableSearchObj.run().getRange({
        start: searchid_,
        end: searchid_ + 1000
    })
    
    for (var re in resultslice_) {
    
        ofc_Cost_Results_.push(resultslice_[re]);
        searchid_++;
    }
    } while (resultslice_.length >= 1000);
    
    var obj_JERecord = RECORD.create({
    type: RECORD.Type.JOURNAL_ENTRY, 
    isDynamic: false});
    
    var o_RecObj = RECORD.load({
    type: ofc_recType,
    id: ofc_recID,
    isDynamic: true
    });
    var fileId = o_RecObj.getValue({
    fieldId: 'custrecord_ofc_csv_file'
    });
    var month_v = o_RecObj.getText({
    fieldId: 'custrecord_ofc_month'
    });
    var year_v = o_RecObj.getText({
    fieldId: 'custrecord_ofc_year'
    }); 
    
    var credit_account = o_RecObj.getValue({
    fieldId: 'custrecord_ofc_account_credit'
    }); 
    
    /*
    var C_exchangeRate = o_RecObj.getValue({
    fieldId: 'custrecord_ofc_currency_exchange_rate'
    }); 
    
    C_exchangeRate = parseFloat(C_exchangeRate);
    */
    var debit_account = o_RecObj.getValue({
    fieldId: 'custrecord_ofc_debit_account'
    }); 
    
    var additional_cost = o_RecObj.getValue({
    fieldId: 'custrecord_ofc_additional_cost'
    });
    
    var d_start_date_S = get_current_month_start_date(month_v, year_v);
    d_start_date_S = new Date(d_start_date_S);
    
    
    obj_JERecord.setValue({fieldId : 'subsidiary', value : subsidiary_v_ofc[ofc_index]});
      
    obj_JERecord.setValue({fieldId : 'trandate', value : d_start_date_S});
    
    
    
    var sr_no_journal = 1;
    var total_credit_to_be_tagged = 0;
    
    try{
        
    if(ofc_Cost_Results_){
    
    for(var index=0; index<ofc_Cost_Results_.length;index++){
    log.debug('index',index);
    var proj_rcrd = RECORD.load({
            type: 'job',
            id: ofc_Cost_Results_[index].getValue({name: 'custrecord_ofc_cost_project'}),
            isDynamic: true
        });
    
    var entity_id = proj_rcrd.getValue({fieldId: 'entityid'});
                var alt_name = proj_rcrd.getValue({fieldId: 'altname'});
                var custentity_vertical = proj_rcrd.getValue({fieldId: 'custentity_vertical'});
                var proj_desrcption = entity_id +' '+alt_name;
    
    
     var customerDetails = SEARCHMODULE.lookupFields({
                             type: 'customer',
                             id: ofc_Cost_Results_[index].getValue({name: 'custrecord_ofc_cost_customer'}),
                             columns: ['territory']
                            });
     var cust_territory = customerDetails.territory[0].value;
     var C_exchangeRate = ofc_Cost_Results_[index].getValue({name: 'custrecord_exchnage_rate_'});
        
        C_exchangeRate = parseFloat(C_exchangeRate);
     var emp_amount_to_be_tagged = parseFloat(C_exchangeRate) * ofc_Cost_Results_[index].getValue({name: 'custrecord_ofc_cost_for_credit'});
                emp_amount_to_be_tagged = emp_amount_to_be_tagged.toString();
                if(emp_amount_to_be_tagged.indexOf(".")>0)
                {
                    var temp_index = emp_amount_to_be_tagged.indexOf(".")+3;
                    emp_amount_to_be_tagged = emp_amount_to_be_tagged.substr(0,temp_index);
                }
                emp_amount_to_be_tagged = parseFloat(emp_amount_to_be_tagged);
                
        total_credit_to_be_tagged = parseFloat(total_credit_to_be_tagged) + parseFloat(emp_amount_to_be_tagged);
        total_credit_to_be_tagged = total_credit_to_be_tagged.toFixed(2);
    
           
        
            obj_JERecord.setSublistValue({
                    sublistId: 'line',
                    fieldId: 'account',
                    line: index,
                    value: credit_account
                });
        
                obj_JERecord.setSublistValue({
                    sublistId: 'line',
                    fieldId: 'custcol_srnumber',
                    value: parseInt(index+1),
                    line: index
                });
        sr_no_journal++; 
        
        obj_JERecord.setSublistValue({
                    sublistId: 'line',
                    fieldId: 'credit',
                    value: emp_amount_to_be_tagged,
                    line: index
                });
        
        //if logic to be added for the department
        obj_JERecord.setSublistValue({
                    sublistId: 'line',
                    fieldId: 'department',
                    value: ofc_Cost_Results_[index].getValue({name: 'custrecord_ofc_cost_practice'}),
                    line: index
                });
                
                obj_JERecord.setSublistValue({
                    sublistId: 'line',
                    fieldId: 'class',
                    value: custentity_vertical,
                    line: index
                });
                obj_JERecord.setSublistValue({
                    sublistId: 'line',
                    fieldId: 'custcol_project_name',
                    value: proj_desrcption,
                    line: index
                });
                obj_JERecord.setSublistValue({
                    sublistId: 'line',
                    fieldId: 'custcolprj_name',
                    value: proj_desrcption,
                    line: index
                });
                obj_JERecord.setSublistValue({
                    sublistId: 'line',
                    fieldId: 'custcol_sow_project',
                    value: ofc_Cost_Results_[index].getValue({name: 'custrecord_ofc_cost_project'}),
                    line: index
                });
                obj_JERecord.setSublistValue({
                    sublistId: 'line',
                    fieldId: 'custcol_territory',
                    value: cust_territory,
                    line: index
                });
                obj_JERecord.setSublistValue({
                    sublistId: 'line',
                    fieldId: 'custcol_employeenamecolumn',
                    value: ofc_Cost_Results_[index].getText({name: 'custrecord_ofc_resource'}),
                    line: index
                }); //
                obj_JERecord.setSublistValue({
                    sublistId: 'line',
                    fieldId: 'custcolcustcol_temp_customer',
                    value: ofc_Cost_Results_[index].getText({name: 'custrecord_ofc_cost_customer'}),
                    line: index
                }); 
                obj_JERecord.setSublistValue({
                    sublistId: 'line',
                    fieldId: 'memo',
                    value: 'OFC Credit Cost JE',
                    line: index
                });
      
           /*
               obj_JERecord.setSublistValue({
                    sublistId: 'line',
                    fieldId: 'entity',
                    line: index,
                    value: 296842
                });*/
      
        if(_logValidation(vendorList[subsidiary_v_ofc[ofc_index]])){
              obj_JERecord.setSublistValue({
                    sublistId: 'line',
                    fieldId: 'entity',
                    line: index,
                    value: vendorList[subsidiary_v_ofc[ofc_index]]
                });
          }
    
    
    }
      
    
        
        obj_JERecord.setSublistValue({
                    sublistId: 'line',
                    fieldId: 'custcol_srnumber',
                    value: parseInt(sr_no_journal),
                    line: index
                    
                });
        
        obj_JERecord.setSublistValue({
                    sublistId: 'line',
                    fieldId: 'debit',
                    value: total_credit_to_be_tagged,
                    line: index
                }); 
      
       obj_JERecord.setSublistValue({
                    sublistId: 'line',
                    fieldId: 'account',
                    line: index,
                    value: debit_account
                });
      
      /*obj_JERecord.setSublistValue({
                   sublistId: 'line',
                    fieldId: 'entity',
                    line: index,
                   value: 296842
               });*/
      
      if(_logValidation(vendorList[subsidiary_v_ofc[ofc_index]])){
              obj_JERecord.setSublistValue({
                    sublistId: 'line',
                    fieldId: 'entity',
                    line: index,
                    value: vendorList[subsidiary_v_ofc[ofc_index]]
                });
          }
      
              
        var journalEntryID = obj_JERecord.save({
        ignoreMandatoryFields: true,
        enableSourcing : true
        });

        JEIDS.push(journalEntryID);
            
         log.debug('journalEntryID',journalEntryID);
         
    } 
     
    
    }catch(e){
        log.debug('Exception',e);
        RECORD.submitFields({
                        type: ofc_recType,
                        id: parseInt(ofc_recID),
                        values: {
                            custrecord_ofc_log_cost: e.message
                        },
                        options: {
                            enableSourcing: true,
                            ignoreMandatoryFields: true
                        }
                    });
                    
        RECORD.submitFields({
                        type: ofc_recType,
                        id: parseInt(ofc_recID),
                        values: {
                            custrecord_ofc_current_status: 3
                        },
                        options: {
                            enableSourcing: true,
                            ignoreMandatoryFields: true
                        }
                    });
        
        
        return;
    }

 }
    
    RECORD.submitFields({
                        type: ofc_recType,
                        id: parseInt(ofc_recID),
                        values: { //JEIDS  journalEntryID
                            custrecord_ofc_journal_entry: JEIDS
                        },
                        options: {
                            enableSourcing: true,
                            ignoreMandatoryFields: true
                        }
                    });
    
    RECORD.submitFields({
                        type: ofc_recType,
                        id: parseInt(ofc_recID),
                        values: {
                            custrecord_ofc_current_status: 5
                        },
                        options: {
                            enableSourcing: true,
                            ignoreMandatoryFields: true
                        }
                    });
      
    RECORD.submitFields({
                        type: ofc_recType,
                        id: parseInt(ofc_recID),
                        values: {
                            custrecord_ofc_log_cost: "JE creation completed"
                        },
                        options: {
                            enableSourcing: true,
                            ignoreMandatoryFields: true
                        }
                    });
      
    }catch(e){
        log.debug('Exception',e);
        RECORD.submitFields({
                        type: ofc_recType,
                        id: parseInt(ofc_recID),
                        values: {
                            custrecord_ofc_log_cost: e.message
                        },
                        options: {
                            enableSourcing: true,
                            ignoreMandatoryFields: true
                        }
                    });
    
        RECORD.submitFields({
                        type: ofc_recType,
                        id: parseInt(ofc_recID),
                        values: {
                            custrecord_ofc_current_status: 3
                        },
                        options: {
                            enableSourcing: true,
                            ignoreMandatoryFields: true
                        }
                    });
        return false;
    }
    
    
    
    
    
    }



 function get_current_month_start_date(i_month, i_year) {
    var d_start_date = '';
    var d_end_date = '';

    var date_format = checkDateFormat();

    if (_logValidation(i_month) && _logValidation(i_year)) {
        if (i_month == 'Jan' || i_month == 'January' || i_month == 'JAN' || i_month == 'JANUARY') {
            if (date_format == 'YYYY-MM-DD') {
                d_start_date = i_year + '-' + 1 + '-' + 1;
                d_end_date = i_year + '-' + 1 + '-' + 31;
            }
            if (date_format == 'DD/MM/YYYY') {
                d_start_date = 1 + '/' + 1 + '/' + i_year;
                d_end_date = 31 + '/' + 1 + '/' + i_year;
            }
            if (date_format == 'MM/DD/YYYY') {
                d_start_date = 1 + '/' + 1 + '/' + i_year;
                d_end_date = 1 + '/' + 31 + '/' + i_year;
            }

        } else if (i_month == 'Feb' || i_month == 'February' || i_month == 'FEB' || i_month == 'FEBRUARY') {
            if ((i_year % 2) == 0) {
                i_day = 29;
            } else {
                i_day = 28;
            }

            if (date_format == 'YYYY-MM-DD') {
                d_start_date = i_year + '-' + 2 + '-' + 1;
                d_end_date = i_year + '-' + 2 + '-' + i_day;
            }
            if (date_format == 'DD/MM/YYYY') {
                d_start_date = 1 + '/' + 2 + '/' + i_year;
                d_end_date = i_day + '/' + 2 + '/' + i_year;
            }
            if (date_format == 'MM/DD/YYYY') {
                d_start_date = 2 + '/' + 1 + '/' + i_year;
                d_end_date = 2 + '/' + i_day + '/' + i_year;
            }

        } else if (i_month == 'Mar' || i_month == 'March' || i_month == 'MAR' || i_month == 'MARCH') {
            if (date_format == 'YYYY-MM-DD') {
                d_start_date = i_year + '-' + 3 + '-' + 1;
                d_end_date = i_year + '-' + 3 + '-' + 31;
            }
            if (date_format == 'DD/MM/YYYY') {
                d_start_date = 1 + '/' + 3 + '/' + i_year;
                d_end_date = 31 + '/' + 3 + '/' + i_year;
            }
            if (date_format == 'MM/DD/YYYY') {
                d_start_date = 3 + '/' + 1 + '/' + i_year;
                d_end_date = 3 + '/' + 31 + '/' + i_year;
            }

        } else if (i_month == 'Apr' || i_month == 'April' || i_month == 'APR' || i_month == 'APRIL') {
            if (date_format == 'YYYY-MM-DD') {
                d_start_date = i_year + '-' + 4 + '-' + 1;
                d_end_date = i_year + '-' + 4 + '-' + 30;
            }
            if (date_format == 'DD/MM/YYYY') {
                d_start_date = 1 + '/' + 4 + '/' + i_year;
                d_end_date = 30 + '/' + 4 + '/' + i_year;
            }
            if (date_format == 'MM/DD/YYYY') {
                d_start_date = 4 + '/' + 1 + '/' + i_year;
                d_end_date = 4 + '/' + 30 + '/' + i_year;
            }


        } else if (i_month == 'May' || i_month == 'MAY') {
            if (date_format == 'YYYY-MM-DD') {
                d_start_date = i_year + '-' + 5 + '-' + 1;
                d_end_date = i_year + '-' + 5 + '-' + 31;
            }
            if (date_format == 'DD/MM/YYYY') {
                d_start_date = 1 + '/' + 5 + '/' + i_year;
                d_end_date = 31 + '/' + 5 + '/' + i_year;
            }
            if (date_format == 'MM/DD/YYYY') {
                d_start_date = 5 + '/' + 1 + '/' + i_year;
                d_end_date = 5 + '/' + 31 + '/' + i_year;
            }

        } else if (i_month == 'June' || i_month == 'Jun' || i_month == 'JUN' || i_month == 'JUNE') {
            if (date_format == 'YYYY-MM-DD') {
                d_start_date = i_year + '-' + 6 + '-' + 1;
                d_end_date = i_year + '-' + 6 + '-' + 30;
            }
            if (date_format == 'DD/MM/YYYY') {
                d_start_date = 1 + '/' + 6 + '/' + i_year;
                d_end_date = 30 + '/' + 6 + '/' + i_year;
            }
            if (date_format == 'MM/DD/YYYY') {
                d_start_date = 6 + '/' + 1 + '/' + i_year;
                d_end_date = 6 + '/' + 30 + '/' + i_year;
            }

        } else if (i_month == 'Jul' || i_month == 'July' || i_month == 'JUL' || i_month == 'JULY') {
            if (date_format == 'YYYY-MM-DD') {
                d_start_date = i_year + '-' + 7 + '-' + 1;
                d_end_date = i_year + '-' + 7 + '-' + 31;
            }
            if (date_format == 'DD/MM/YYYY') {
                d_start_date = 1 + '/' + 7 + '/' + i_year;
                d_end_date = 31 + '/' + 7 + '/' + i_year;
            }
            if (date_format == 'MM/DD/YYYY') {
                d_start_date = 7 + '/' + 1 + '/' + i_year;
                d_end_date = 7 + '/' + 31 + '/' + i_year;
            }

        } else if (i_month == 'August' || i_month == 'Aug' || i_month == 'AUG' || i_month == 'AUGUST') {
            if (date_format == 'YYYY-MM-DD') {
                d_start_date = i_year + '-' + 8 + '-' + 1;
                d_end_date = i_year + '-' + 8 + '-' + 31;
            }
            if (date_format == 'DD/MM/YYYY') {
                d_start_date = 1 + '/' + 8 + '/' + i_year;
                d_end_date = 31 + '/' + 8 + '/' + i_year;
            }
            if (date_format == 'MM/DD/YYYY') {
                d_start_date = 8 + '/' + 1 + '/' + i_year;
                d_end_date = 8 + '/' + 31 + '/' + i_year;
            }

        } else if (i_month == 'Sep' || i_month == 'September' || i_month == 'SEP' || i_month == 'SEPTEMBER') {
            if (date_format == 'YYYY-MM-DD') {
                d_start_date = i_year + '-' + 9 + '-' + 1;
                d_end_date = i_year + '-' + 9 + '-' + 30;
            }
            if (date_format == 'DD/MM/YYYY') {
                d_start_date = 1 + '/' + 9 + '/' + i_year;
                d_end_date = 30 + '/' + 9 + '/' + i_year;
            }
            if (date_format == 'MM/DD/YYYY') {
                d_start_date = 9 + '/' + 1 + '/' + i_year;
                d_end_date = 9 + '/' + 30 + '/' + i_year;
            }

        } else if (i_month == 'Oct' || i_month == 'October' || i_month == 'OCT' || i_month == 'OCTOBER') {
            if (date_format == 'YYYY-MM-DD') {
                d_start_date = i_year + '-' + 10 + '-' + 1;
                d_end_date = i_year + '-' + 10 + '-' + 31;
            }
            if (date_format == 'DD/MM/YYYY') {
                d_start_date = 1 + '/' + 10 + '/' + i_year;
                d_end_date = 31 + '/' + 10 + '/' + i_year;
            }
            if (date_format == 'MM/DD/YYYY') {
                d_start_date = 10 + '/' + 1 + '/' + i_year;
                d_end_date = 10 + '/' + 31 + '/' + i_year;
            }
        } else if (i_month == 'Nov' || i_month == 'November' || i_month == 'NOV' || i_month == 'NOVEMBER') {
            if (date_format == 'YYYY-MM-DD') {
                d_start_date = i_year + '-' + 11 + '-' + 1;
                d_end_date = i_year + '-' + 11 + '-' + 30;
            }
            if (date_format == 'DD/MM/YYYY') {
                d_start_date = 1 + '/' + 11 + '/' + i_year;
                d_end_date = 30 + '/' + 11 + '/' + i_year;
            }
            if (date_format == 'MM/DD/YYYY') {
                d_start_date = 11 + '/' + 1 + '/' + i_year;
                d_end_date = 11 + '/' + 30 + '/' + i_year;
            }

        } else if (i_month == 'Dec' || i_month == 'December' || i_month == 'DEC' || i_month == 'DECEMBER') {
            if (date_format == 'YYYY-MM-DD') {
                d_start_date = i_year + '-' + 12 + '-' + 1;
                d_end_date = i_year + '-' + 12 + '-' + 31;
            }
            if (date_format == 'DD/MM/YYYY') {
                d_start_date = 1 + '/' + 12 + '/' + i_year;
                d_end_date = 31 + '/' + 12 + '/' + i_year;
            }
            if (date_format == 'MM/DD/YYYY') {
                d_start_date = 12 + '/' + 1 + '/' + i_year;
                d_end_date = 12 + '/' + 31 + '/' + i_year;
            }

        }
    } 
    return d_start_date;
 }

  //Get End date of the month
 function get_current_month_end_date(i_month, i_year) {
    var d_start_date = '';
    var d_end_date = '';

    var date_format = checkDateFormat();

    if (_logValidation(i_month) && _logValidation(i_year)) {
        if (i_month == 'Jan' || i_month == 'January' || i_month == 'JAN' || i_month == 'JANUARY') {
            if (date_format == 'YYYY-MM-DD') {
                d_start_date = i_year + '-' + 1 + '-' + 1;
                d_end_date = i_year + '-' + 1 + '-' + 31;
            }
            if (date_format == 'DD/MM/YYYY') {
                d_start_date = 1 + '/' + 1 + '/' + i_year;
                d_end_date = 31 + '/' + 1 + '/' + i_year;
            }
            if (date_format == 'MM/DD/YYYY') {
                d_start_date = 1 + '/' + 1 + '/' + i_year;
                d_end_date = 1 + '/' + 31 + '/' + i_year;
            }

        } else if (i_month == 'Feb' || i_month == 'February' || i_month == 'FEB' || i_month == 'FEBRUARY') {
            if ((i_year % 2) == 0) {
                i_day = 29;
            } else {
                i_day = 28;
            }

            if (date_format == 'YYYY-MM-DD') {
                d_start_date = i_year + '-' + 2 + '-' + 1;
                d_end_date = i_year + '-' + 2 + '-' + i_day;
            }
            if (date_format == 'DD/MM/YYYY') {
                d_start_date = 1 + '/' + 2 + '/' + i_year;
                d_end_date = i_day + '/' + 2 + '/' + i_year;
            }
            if (date_format == 'MM/DD/YYYY') {
                d_start_date = 2 + '/' + 1 + '/' + i_year;
                d_end_date = 2 + '/' + i_day + '/' + i_year;
            }

        } else if (i_month == 'Mar' || i_month == 'March' || i_month == 'MAR' || i_month == 'MARCH') {
            if (date_format == 'YYYY-MM-DD') {
                d_start_date = i_year + '-' + 3 + '-' + 1;
                d_end_date = i_year + '-' + 3 + '-' + 31;
            }
            if (date_format == 'DD/MM/YYYY') {
                d_start_date = 1 + '/' + 3 + '/' + i_year;
                d_end_date = 31 + '/' + 3 + '/' + i_year;
            }
            if (date_format == 'MM/DD/YYYY') {
                d_start_date = 3 + '/' + 1 + '/' + i_year;
                d_end_date = 3 + '/' + 31 + '/' + i_year;
            }

        } else if (i_month == 'Apr' || i_month == 'April' || i_month == 'APR' || i_month == 'APRIL') {
            if (date_format == 'YYYY-MM-DD') {
                d_start_date = i_year + '-' + 4 + '-' + 1;
                d_end_date = i_year + '-' + 4 + '-' + 30;
            }
            if (date_format == 'DD/MM/YYYY') {
                d_start_date = 1 + '/' + 4 + '/' + i_year;
                d_end_date = 30 + '/' + 4 + '/' + i_year;
            }
            if (date_format == 'MM/DD/YYYY') {
                d_start_date = 4 + '/' + 1 + '/' + i_year;
                d_end_date = 4 + '/' + 30 + '/' + i_year;
            }


        } else if (i_month == 'May' || i_month == 'MAY') {
            if (date_format == 'YYYY-MM-DD') {
                d_start_date = i_year + '-' + 5 + '-' + 1;
                d_end_date = i_year + '-' + 5 + '-' + 31;
            }
            if (date_format == 'DD/MM/YYYY') {
                d_start_date = 1 + '/' + 5 + '/' + i_year;
                d_end_date = 31 + '/' + 5 + '/' + i_year;
            }
            if (date_format == 'MM/DD/YYYY') {
                d_start_date = 5 + '/' + 1 + '/' + i_year;
                d_end_date = 5 + '/' + 31 + '/' + i_year;
            }

        } else if (i_month == 'June' || i_month == 'Jun' || i_month == 'JUN' || i_month == 'JUNE') {
            if (date_format == 'YYYY-MM-DD') {
                d_start_date = i_year + '-' + 6 + '-' + 1;
                d_end_date = i_year + '-' + 6 + '-' + 30;
            }
            if (date_format == 'DD/MM/YYYY') {
                d_start_date = 1 + '/' + 6 + '/' + i_year;
                d_end_date = 30 + '/' + 6 + '/' + i_year;
            }
            if (date_format == 'MM/DD/YYYY') {
                d_start_date = 6 + '/' + 1 + '/' + i_year;
                d_end_date = 6 + '/' + 30 + '/' + i_year;
            }

        } else if (i_month == 'Jul' || i_month == 'July' || i_month == 'JUL' || i_month == 'JULY') {
            if (date_format == 'YYYY-MM-DD') {
                d_start_date = i_year + '-' + 7 + '-' + 1;
                d_end_date = i_year + '-' + 7 + '-' + 31;
            }
            if (date_format == 'DD/MM/YYYY') {
                d_start_date = 1 + '/' + 7 + '/' + i_year;
                d_end_date = 31 + '/' + 7 + '/' + i_year;
            }
            if (date_format == 'MM/DD/YYYY') {
                d_start_date = 7 + '/' + 1 + '/' + i_year;
                d_end_date = 7 + '/' + 31 + '/' + i_year;
            }

        } else if (i_month == 'August' || i_month == 'Aug' || i_month == 'AUG' || i_month == 'AUGUST') {
            if (date_format == 'YYYY-MM-DD') {
                d_start_date = i_year + '-' + 8 + '-' + 1;
                d_end_date = i_year + '-' + 8 + '-' + 31;
            }
            if (date_format == 'DD/MM/YYYY') {
                d_start_date = 1 + '/' + 8 + '/' + i_year;
                d_end_date = 31 + '/' + 8 + '/' + i_year;
            }
            if (date_format == 'MM/DD/YYYY') {
                d_start_date = 8 + '/' + 1 + '/' + i_year;
                d_end_date = 8 + '/' + 31 + '/' + i_year;
            }

        } else if (i_month == 'Sep' || i_month == 'September' || i_month == 'SEP' || i_month == 'SEPTEMBER') {
            if (date_format == 'YYYY-MM-DD') {
                d_start_date = i_year + '-' + 9 + '-' + 1;
                d_end_date = i_year + '-' + 9 + '-' + 30;
            }
            if (date_format == 'DD/MM/YYYY') {
                d_start_date = 1 + '/' + 9 + '/' + i_year;
                d_end_date = 30 + '/' + 9 + '/' + i_year;
            }
            if (date_format == 'MM/DD/YYYY') {
                d_start_date = 9 + '/' + 1 + '/' + i_year;
                d_end_date = 9 + '/' + 30 + '/' + i_year;
            }

        } else if (i_month == 'Oct' || i_month == 'October' || i_month == 'OCT' || i_month == 'OCTOBER') {
            if (date_format == 'YYYY-MM-DD') {
                d_start_date = i_year + '-' + 10 + '-' + 1;
                d_end_date = i_year + '-' + 10 + '-' + 31;
            }
            if (date_format == 'DD/MM/YYYY') {
                d_start_date = 1 + '/' + 10 + '/' + i_year;
                d_end_date = 31 + '/' + 10 + '/' + i_year;
            }
            if (date_format == 'MM/DD/YYYY') {
                d_start_date = 10 + '/' + 1 + '/' + i_year;
                d_end_date = 10 + '/' + 31 + '/' + i_year;
            }


        } else if (i_month == 'Nov' || i_month == 'November' || i_month == 'NOV' || i_month == 'NOVEMBER') {
            if (date_format == 'YYYY-MM-DD') {
                d_start_date = i_year + '-' + 11 + '-' + 1;
                d_end_date = i_year + '-' + 11 + '-' + 30;
            }
            if (date_format == 'DD/MM/YYYY') {
                d_start_date = 1 + '/' + 11 + '/' + i_year;
                d_end_date = 30 + '/' + 11 + '/' + i_year;
            }
            if (date_format == 'MM/DD/YYYY') {
                d_start_date = 11 + '/' + 1 + '/' + i_year;
                d_end_date = 11 + '/' + 30 + '/' + i_year;
            }

        } else if (i_month == 'Dec' || i_month == 'December' || i_month == 'DEC' || i_month == 'DECEMBER') {
            if (date_format == 'YYYY-MM-DD') {
                d_start_date = i_year + '-' + 12 + '-' + 1;
                d_end_date = i_year + '-' + 12 + '-' + 31;
            }
            if (date_format == 'DD/MM/YYYY') {
                d_start_date = 1 + '/' + 12 + '/' + i_year;
                d_end_date = 31 + '/' + 12 + '/' + i_year;
            }
            if (date_format == 'MM/DD/YYYY') {
                d_start_date = 12 + '/' + 1 + '/' + i_year;
                d_end_date = 12 + '/' + 31 + '/' + i_year;
            }

        }
    } //Month & Year
    return d_end_date;
}

function checkDateFormat() {
    var userObj = RUNTIME.getCurrentUser();
    var dateFormatPref = userObj.getPreference({
        name: 'DATEFORMAT'
    });

    return dateFormatPref;
}

function getDatediffIndays(startDate, endDate) {
    var fromDate = startDate;
    var toDate = endDate;
    fromDate = new Date(fromDate);
    toDate = new Date(toDate);
    var diff = toDate.getDate() - fromDate.getDate();
    return Math.round(diff + 1);
}