/**
 * Send email trigger when a new allocation is done, to the practice head
 * 
 * Version Date Author Remarks 1.00 02 Mar 2016 Nitish Mishra
 * 
 */

function userEventAfterSubmit(type) {
	try {

		if (type == 'create') {
			var projectId = nlapiGetFieldValue('project');
			var projectDetails = nlapiLookupField('job', projectId, [
			        'custentity_practice', 'custentity_projectmanager',
			        'custentity_deliverymanager', 'custentity_clientpartner','jobbillingtype']);
			var executingPracticeId = projectDetails.custentity_practice;
			var practiceHeadId = nlapiLookupField('department',
			        executingPracticeId, 'custrecord_practicehead');
			var mailRecipientName = nlapiLookupField('employee',
			        projectDetails.custentity_projectmanager, 'firstname');
			mailTemplate = allocationCreationNotification(mailRecipientName,projectId);

			nlapiSendEmail(constant.Mail_Author.InformationSystems,
			        projectDetails.custentity_projectmanager,
			        mailTemplate.Subject, mailTemplate.Body, [
			                nlapiLookupField('employee',
			                        projectDetails.custentity_deliverymanager,
			                        'email'),
			                nlapiLookupField('employee', practiceHeadId,
			                        'email') ], null, {
				        'entity' : projectId
			        });

			// if the allocation is not billable, send a mail to the client
			// partner
			var isResourceNotBillable = nlapiGetFieldValue('custeventrbillable') != 'T';

			if (isResourceNotBillable) {

				if (projectDetails.custentity_clientpartner) {
					var clientPartner = nlapiLookupField('employee',
					        projectDetails.custentity_clientpartner,
					        'firstname');
					mailTemplate = clientPartnerEmailTemplate(clientPartner,projectId);

					nlapiSendEmail(constant.Mail_Author.InformationSystems,
					        projectDetails.custentity_clientpartner,
					        mailTemplate.Subject, mailTemplate.Body, null,
					        null, {
						        'entity' : projectId
					        });
					nlapiLogExecution('DEBUG', 'Email Send To Client Partner',
					        'Project : ' + projectId);
				}
			}
		}
	} catch (err) {
		nlapiLogExecution('ERROR', 'userEventAfterSubmit', err);
	}
}

function allocationCreationNotification(practiceHeadName, projectId){
	var htmltext = '<p>Hi ' + practiceHeadName + ',</p>';

	htmltext += "<p>This is to inform you that a new allocation has"
	        + " been created for a project :</p>";

	htmltext += "<b>Employee : </b>" + nlapiGetFieldText('allocationresource')
	        + "<br/>";
	htmltext += "<b>Project : </b>" + nlapiGetFieldText('project') + "<br/>";
	htmltext += "<b>Start Date : </b>" + nlapiGetFieldValue('startdate')
	        + "<br/>";
	htmltext += "<b>End Date : </b>" + nlapiGetFieldValue('enddate') + "<br/>";
	htmltext += "<b>Billable : </b>"
	        + (nlapiGetFieldValue('custeventrbillable') == 'T' ? 'Yes' : 'No')
	        + "<br/>";
	htmltext += "<b>Site : </b>" + nlapiGetFieldText('custevent4') + "<br/>";
	htmltext += "<b>Shadow : </b>"
	        + (nlapiGetFieldValue('custevent_ra_is_shadow') == 'T' ? 'Yes'
	                : 'No') + "<br/>";
					
   htmltext += "<b>Billing Type : </b>" + nlapiLookupField('job', projectId, 'jobbillingtype', true) + "<br/>";
   htmltext += "<b>Bill Rate : </b>" + nlapiGetFieldValue('custevent3') + "<br/>";
   htmltext += "<b>Allocation Percentage : </b>" + nlapiGetFieldValue('allocationamount') + "<br/>";//mani
   

	htmltext += '<p>Regards,<br/>';
	htmltext += 'Information Systems</p>';

	return {
	    Subject : 'New Allocation Created',
	    Body : addMailTemplate(htmltext)
	};
}

function clientPartnerEmailTemplate(firstName,projectId) {
	var htmltext = '<p>Hi ' + firstName + ',</p>';

	htmltext += "<p>This is to inform you that a new allocation has"
	        + " been created for a project :</p>";

	htmltext += "<b>Employee : </b>" + nlapiGetFieldText('allocationresource')
	        + "<br/>";
	htmltext += "<b>Project : </b>" + nlapiGetFieldText('project') + "<br/>";
	htmltext += "<b>Start Date : </b>" + nlapiGetFieldValue('startdate')
	        + "<br/>";
	htmltext += "<b>End Date : </b>" + nlapiGetFieldValue('enddate') + "<br/>";
	htmltext += "<b>Billable : </b>"
	        + (nlapiGetFieldValue('custeventrbillable') == 'T' ? 'Yes' : 'No')
	        + "<br/>";
	htmltext += "<b>Site : </b>" + nlapiGetFieldText('custevent4') + "<br/>";
	htmltext += "<b>Shadow : </b>"
	        + (nlapiGetFieldValue('custevent_ra_is_shadow') == 'T' ? 'Yes'
	                : 'No') + "<br/>";
					
   htmltext += "<b>Billing Type : </b>" + nlapiLookupField('job', projectId, 'jobbillingtype', true) + "<br/>";
   htmltext += "<b>Bill Rate : </b>" + nlapiGetFieldValue('custevent3') + "<br/>";
   htmltext += "<b>Allocation Percentage: </b>" + nlapiGetFieldValue('allocationamount') + "<br/>";//mani
   

	htmltext += '<p>Regards,<br/>';
	htmltext += 'Information Systems</p>';

	return {
	    Subject : 'New Non-Billable Allocation Created',
	    Body : addMailTemplate(htmltext)
	};
}