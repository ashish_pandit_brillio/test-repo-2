/**
 * @author Jayesh
 */

function suitelet(request, response) {
	try {
		var userId = nlapiGetUser();
		
		var requestedProject = request.getParameterValues('custpage_project');
		var requestedCustomer = request.getParameterValues('custpage_customer');
		//var requestedRegion = request.getParameterValues('custpage_region');
		
		var requestedInrExchangeRate = request.getParameter('custpage_exchange_rate_inr');
		var requestedGbpExchangeRate = request.getParameter('custpage_exchange_rate_gbp');
		
		var formTitle = 'Revenue Projection';

		// Form
		var form = nlapiCreateForm(formTitle);
		form.addFieldGroup('custpage_1', 'Select Options');

		// Customer Field
		var customerField = form.addField('custpage_customer', 'select','Customer', null, 'custpage_1');
		customerField.addSelectOption('', ' ');
		var taggedProjects = getTaggedProjectList();
		taggedProjects.forEach(function(project) {
			var Customer_Name = project.getText('customer');
			var cust_id = project.getValue('customer');
			customerField.addSelectOption(cust_id, Customer_Name);
		});
		if (requestedCustomer) {
			customerField.setDefaultValue('');
		}
		
		// Project Field
		var projectField = form.addField('custpage_project', 'select','Project', null, 'custpage_1');
		projectField.addSelectOption('', ' ');
		var taggedProjects = getTaggedProjectList();
		taggedProjects.forEach(function(project) {
			var projectName = project.getValue('entityid') + " : "
			        + project.getValue('altname');
			projectField.addSelectOption(project.getId(), projectName);
		});
		if (requestedProject) {
			projectField.setDefaultValue('');
		}
		
		// Currency Field for INR
		var inrField = form.addField('custpage_exchange_rate_inr', 'currency',
		        'USD to INR ( 1 USD = x INR )', null, 'custpage_1');
		inrField.setBreakType('startcol');
		inrField.setDefaultValue('');
		
		if (requestedInrExchangeRate) {
			inrField.setDefaultValue(requestedInrExchangeRate);
		}
		
		// Currency Field For GBP
		var gbpField = form.addField('custpage_exchange_rate_gbp', 'currency',
		        'USD to GBP ( 1 USD = x GBP )', null, 'custpage_1');
		//gbpField.setBreakType('startcol');
		gbpField.setDefaultValue('');
		
		if (requestedGbpExchangeRate) {
			gbpField.setDefaultValue(requestedGbpExchangeRate);
		}
		
		//nlapiLogExecution('audit','gbp:- ',requestedGbpExchangeRate);
		var requestedExchangeRateTable = {
		    USD : 1,
		    INR : parseFloat(requestedInrExchangeRate),
		    GBP : parseFloat(requestedGbpExchangeRate)
		};
		
		//Region Field
		/*var regionField = form.addField('custpage_region', 'select','Region', null, 'custpage_1');
		regionField.addSelectOption(1, ' ');
		regionField.addSelectOption(2, 'UK');
		regionField.addSelectOption(3, 'US-East');
		regionField.addSelectOption(4, 'US-West');
		regionField.addSelectOption(5, 'India');
		regionField.addSelectOption(6, 'US-T&NW');
		regionField.addSelectOption(7, 'US-Central');
		regionField.addSelectOption(8, 'Europe');
		regionField.addSelectOption(9, 'Brillio');*/
		
		// Select Options field group end
		
		form.addSubmitButton('Load Data');
			
		nlapiLogExecution('audit','cust selctd:- ',JSON.stringify(requestedCustomer));
		nlapiLogExecution('audit','pro selctd:- ',JSON.stringify(requestedProject));
		var tableHtml = createProjectionTable(requestedProject,requestedCustomer,requestedExchangeRateTable);
			
		form.addFieldGroup('custpage_2', 'Revenue Data');
		form.addField('custpage_table', 'inlinehtml', null, null, 'custpage_2').setDefaultValue(tableHtml);
		response.writePage(form);
		
	} catch (err) {
		nlapiLogExecution('ERROR', 'suitelet', err);
		displayErrorForm(err, "Revenue Projection");
	}
}

function getTaggedProjectList() {
	try {
		
		var project_search_results = searchRecord('job', '1517', null,
		        [ new nlobjSearchColumn("entityid"),
		                new nlobjSearchColumn("altname"),
						new nlobjSearchColumn("customer") ]);

		nlapiLogExecution('debug', 'project count',
		        project_search_results.length);

		if (project_search_results.length == 0) {
			throw "You don't have any projects under you.";
		}

		return project_search_results;
	} catch (err) {
		nlapiLogExecution('ERROR', 'getTaggedProjectList', err);
		throw err;
	}
}

function getTaggedCustList() {
	try {
		
		var project_search_results = searchRecord('job', '1517', null, new nlobjSearchColumn('customer',null,'group'));

		nlapiLogExecution('debug', 'project count',
		        project_search_results.length);

		if (project_search_results.length == 0) {
			throw "You don't have any projects under you.";
		}

		return project_search_results;
	} catch (err) {
		nlapiLogExecution('ERROR', 'getTaggedProjectList', err);
		throw err;
	}
}

function getProjectDetails(projectList, requestedProject) {
	try {
		var projectData = {};

		var filters = [ new nlobjSearchFilter('internalid', null, 'anyof',
		        projectList) ];

		if (requestedProject) {
			filters.push(new nlobjSearchFilter('internalid', null, 'anyof',
			        requestedProject));
		}

		searchRecord(
		        'job',
		        null,
		        filters,
		        [ new nlobjSearchColumn('startdate'),
		                new nlobjSearchColumn('enddate'),
		                new nlobjSearchColumn('jobbillingtype'),
		                new nlobjSearchColumn('entityid'),
		                new nlobjSearchColumn('altname'),
		                new nlobjSearchColumn('custentity_t_and_m_monthly') ])
		        .forEach(
		                function(project) {
			                projectData[project.getId()] = {
			                    EntityId : project.getValue('entityid'),
			                    AltName : project.getValue('altname'),
			                    StartDate : project.getValue('startdate'),
			                    EndDate : project.getValue('enddate'),
			                    BillingType : project.getText('jobbillingtype')
			                            + (project
			                                    .getValue('custentity_t_and_m_monthly') == 'T' ? ' Monthly'
			                                    : '')
			                };
		                });

		return projectData;
	} catch (err) {
		nlapiLogExecution('ERROR', 'getProjectDetails', err);
		throw err;
	}
}

function getProjectDetails_practiceWise(prac_list) {
	try {
		var projectData = {};

		var filters = [ new nlobjSearchFilter('custentity_practice', null, 'anyof',
		        prac_list) ];


		searchRecord(
		        'job',
		        null,
		        filters,
		        [ new nlobjSearchColumn('startdate'),
		                new nlobjSearchColumn('enddate'),
		                new nlobjSearchColumn('jobbillingtype'),
		                new nlobjSearchColumn('entityid'),
		                new nlobjSearchColumn('altname'),
		                new nlobjSearchColumn('custentity_t_and_m_monthly') ])
		        .forEach(
		                function(project) {
			                projectData[project.getId()] = {
			                    EntityId : project.getValue('entityid'),
			                    AltName : project.getValue('altname'),
			                    StartDate : project.getValue('startdate'),
			                    EndDate : project.getValue('enddate'),
			                    BillingType : project.getText('jobbillingtype')
			                            + (project
			                                    .getValue('custentity_t_and_m_monthly') == 'T' ? ' Monthly'
			                                    : '')
			                };
		                });

		return projectData;
	} catch (err) {
		nlapiLogExecution('ERROR', 'getProjectDetails', err);
		throw err;
	}
}

function getProjectRevenueData(projectList, startDate, endDate,
        requestedProject)
{
	try {
		var filters = [
		        new nlobjSearchFilter('custrecord_prp_project',
		                'custrecord_eprp_project_rev_projection', 'anyof',
		                projectList),
		        new nlobjSearchFilter('custrecord_prp_start_date',
		                'custrecord_eprp_project_rev_projection', 'within',
		                startDate, endDate),
		        new nlobjSearchFilter('isinactive',
		                'custrecord_eprp_project_rev_projection', 'is', 'F'),
		        new nlobjSearchFilter('isinactive', null, 'is', 'F'),
		        new nlobjSearchFilter('custrecord_prp_is_past',
		                'custrecord_eprp_project_rev_projection', 'is', 'F') ];

		if (requestedProject) {
			filters.push(new nlobjSearchFilter('custrecord_prp_project',
			        'custrecord_eprp_project_rev_projection', 'anyof',
			        requestedProject));
		}

		var revenueSearch = nlapiSearchRecord(
		        'customrecord_emp_practice_rev_projection', null, filters, [
		                new nlobjSearchColumn('custrecord_prp_project',
		                        'custrecord_eprp_project_rev_projection'),
		                new nlobjSearchColumn('custrecord_prp_start_date',
		                        'custrecord_eprp_project_rev_projection'),
		                new nlobjSearchColumn('custrecord_prp_month_name',
		                        'custrecord_eprp_project_rev_projection'),
		                new nlobjSearchColumn('custrecord_eprp_practice'),
		                new nlobjSearchColumn('custrecord_eprp_revenue'),
		                new nlobjSearchColumn('custrecord_eprp_usd_revenue'),
		                new nlobjSearchColumn('custrecord_eprp_currency') ]);

		if (revenueSearch) {
			nlapiLogExecution('debug', 'revenueSearch', revenueSearch.length);
		}

		return revenueSearch;
	} catch (err) {
		nlapiLogExecution('ERROR', 'getProjectRevenueData', err);
		throw err;
	}
}

function getProjectRevenueData_ByPractice(prac_list, startDate, endDate,
        requestedProject)
{
	try {
		var filters = [
		        new nlobjSearchFilter('custrecord_eprp_practice',
		                null, 'anyof',
		                prac_list),
		        new nlobjSearchFilter('custrecord_prp_start_date',
		                'custrecord_eprp_project_rev_projection', 'within',
		                startDate, endDate),
		        new nlobjSearchFilter('isinactive',
		                'custrecord_eprp_project_rev_projection', 'is', 'F'),
		        new nlobjSearchFilter('isinactive', null, 'is', 'F'),
		        new nlobjSearchFilter('custrecord_prp_is_past',
		                'custrecord_eprp_project_rev_projection', 'is', 'F') ];

		var revenueSearch = nlapiSearchRecord(
		        'customrecord_emp_practice_rev_projection', null, filters, [
		                new nlobjSearchColumn('custrecord_prp_project',
		                        'custrecord_eprp_project_rev_projection'),
		                new nlobjSearchColumn('custrecord_prp_start_date',
		                        'custrecord_eprp_project_rev_projection'),
		                new nlobjSearchColumn('custrecord_prp_month_name',
		                        'custrecord_eprp_project_rev_projection'),
		                new nlobjSearchColumn('custrecord_eprp_practice'),
		                new nlobjSearchColumn('custrecord_eprp_revenue'),
		                new nlobjSearchColumn('custrecord_eprp_usd_revenue'),
		                new nlobjSearchColumn('custrecord_eprp_currency') ]);

		if (revenueSearch) {
			nlapiLogExecution('debug', 'revenueSearch', revenueSearch.length);
		}

		return revenueSearch;
	} catch (err) {
		nlapiLogExecution('ERROR', 'getProjectRevenueData', err);
		throw err;
	}
}

function createProjectionTable(requestedProject,requestedCustomer,requestedExchangeRateTable)
{
	try {
		
		var projectWiseRevenue = {};
		var sr_no = 0;
		var fltr_counter = 0;
		var emp_unique_proj = new Array();
		
		var arrFilters = new Array();
		
		if(requestedProject)
		{
			arrFilters[fltr_counter] = new nlobjSearchFilter('custrecord_project_name_revenue',null,'anyof',requestedProject);
			fltr_counter++;
		}
		
		if(requestedCustomer)
		{
			arrFilters[fltr_counter] = new nlobjSearchFilter('custrecord_cust_revenue',null,'anyof',requestedCustomer);
			fltr_counter++;
		}
		var startDate_revenue = "1/1/2016";
		var endDate_revenue = "12/31/2016";
		arrFilters[fltr_counter] = new nlobjSearchFilter('custrecord_strt_date_revenue',null,'within',startDate_revenue,endDate_revenue);
		fltr_counter++;
		arrFilters[fltr_counter] = new nlobjSearchFilter('custrecord_allocation_end_date',null,'after',startDate_revenue);
		fltr_counter++;
		arrFilters[fltr_counter] = new nlobjSearchFilter('custrecord_emp_bill_rate',null,'isnotempty');
		fltr_counter++;
		arrFilters[fltr_counter] = new nlobjSearchFilter('custrecord_emp_bill_rate',null,'notequalto',0);
		fltr_counter++;
		arrFilters[fltr_counter] = new nlobjSearchFilter('custrecord_fb_project',null,'is','F');
		fltr_counter++;
		
		var arrColumns = new Array();	
		arrColumns[0] = new nlobjSearchColumn('custrecord_project_name_revenue');
		arrColumns[1] = new nlobjSearchColumn('custrecord_region_revenue');
		arrColumns[2] = new nlobjSearchColumn('custrecord_practice_revenue');
		arrColumns[3] = new nlobjSearchColumn('custrecord_cust_revenue');
		arrColumns[4] = new nlobjSearchColumn('custrecord_resource_revenue');
		arrColumns[5] = new nlobjSearchColumn('custrecord_usd_cost_revenue');
		arrColumns[6] = new nlobjSearchColumn('custrecord_emp_practice_revenue');
		arrColumns[7] = new nlobjSearchColumn('custrecord_month_revenue');
		arrColumns[8] = new nlobjSearchColumn('custrecord_recognised');
		arrColumns[9] = new nlobjSearchColumn('internalid');
		arrColumns[10] = new nlobjSearchColumn('custrecord_currency_revenue');
		arrColumns[11] = new nlobjSearchColumn('custrecord_pro_strt_date');
		arrColumns[12] = new nlobjSearchColumn('custrecord_pro_end_date');
		arrColumns[13] = new nlobjSearchColumn('custrecord_allocation_strt_date');
		arrColumns[14] = new nlobjSearchColumn('custrecord_allocation_end_date');
		arrColumns[15] = new nlobjSearchColumn('custrecord_emp_bill_rate');
		arrColumns[16] = new nlobjSearchColumn('custrecord_monthly_project');
		arrColumns[17] = new nlobjSearchColumn('custrecord_emp_onsiite_offsite_revenue');
		arrColumns[18] = new nlobjSearchColumn('custrecord_prcnt_of_time_allocated');
		
		var revenueSearchData = searchRecord('customrecord_practice_revenue_empwise', null, arrFilters, arrColumns);

		if (revenueSearchData)
		{
			nlapiLogExecution('audit','srch len:- ',revenueSearchData.length);
			for (var i = 0; i < revenueSearchData.length; i++)
			{
				var projectId = revenueSearchData[i].getValue('custrecord_project_name_revenue');
				var projectName = revenueSearchData[i].getText('custrecord_project_name_revenue');
				var proj_region = revenueSearchData[i].getText('custrecord_region_revenue');
				var proj_prac = revenueSearchData[i].getValue('custrecord_practice_revenue');
				var cust = revenueSearchData[i].getText('custrecord_cust_revenue');
				var emp_id = revenueSearchData[i].getValue('custrecord_resource_revenue');
				var emp_name = revenueSearchData[i].getText('custrecord_resource_revenue');
				var revenue = revenueSearchData[i].getValue('custrecord_usd_cost_revenue');
				var emp_prac = revenueSearchData[i].getValue('custrecord_emp_practice_revenue');
				var isRecognised = revenueSearchData[i].getValue('custrecord_recognised');
				var monthName = revenueSearchData[i].getValue('custrecord_month_revenue');
				var proj_currency = revenueSearchData[i].getValue('custrecord_currency_revenue');
				var pro_currency_name = revenueSearchData[i].getText('custrecord_currency_revenue');
				var pro_actual_strt_date = revenueSearchData[i].getValue('custrecord_pro_strt_date');
				var pro_actual_end_date = revenueSearchData[i].getValue('custrecord_pro_end_date');
				var allocation_strt_date = revenueSearchData[i].getValue('custrecord_allocation_strt_date');
				var allocation_end_date = revenueSearchData[i].getValue('custrecord_allocation_end_date');
				var emp_bill_rate = revenueSearchData[i].getValue('custrecord_emp_bill_rate');
				var is_monthly_proj = revenueSearchData[i].getValue('custrecord_monthly_project');
				var emp_onsite_offsite = revenueSearchData[i].getValue('custrecord_emp_onsiite_offsite_revenue');
				var emp_prcnt_allocation = revenueSearchData[i].getValue('custrecord_prcnt_of_time_allocated');
				var internal_id = revenueSearchData[i].getId();
				
				if(_logValidation(revenue))
				{
					if (requestedExchangeRateTable[pro_currency_name])
					{
						//nlapiLogExecution('audit','rate:- ',requestedExchangeRateTable[proj_currency]);
						var exchangeRate = requestedExchangeRateTable[pro_currency_name];
						//revenue = Math.round(revenue * exchangeRate);
						revenue = Math.round(revenue * (1 / exchangeRate),2);
					}
					revenue = parseFloat(revenue).toFixed(2);
				}
				else
				{
					revenue = parseFloat(0);
				}
				
				var is_monthly_hourly = 'Hourly';
				if(is_monthly_proj == 'T')
				{
					is_monthly_hourly = 'Monthly';
				}
				
				//if (!projectWiseRevenue[emp_id])
				//{
					/*var emp_exist = searchEmp(emp_id,projectId,emp_unique_proj);
					//nlapiLogExecution('audit','emp_exist:- ',emp_exist);
					if(emp_exist)
					{
						nlapiLogExecution('audit','emp_exist:- true');
						projectWiseRevenue[emp_id].RevenueData[projectId][monthName].Amount = revenue;
						projectWiseRevenue[emp_id].RevenueData[projectId][monthName].IsRecognised = isRecognised;
					}
					else
					{
						nlapiLogExecution('audit','emp_exist:- false');
						emp_unique_proj[sr_no] = {
											employee_id:emp_id,
											project_id:projectId
										};
						sr_no++;*/
					var emp_id_prj_id = emp_id +'_'+ projectId+'_'+allocation_end_date;
					if (!projectWiseRevenue[emp_id_prj_id])
					{
						//nlapiLogExecution('audit','inside create emp rcrd');
						
						projectWiseRevenue[emp_id_prj_id] = {
							project_id: projectId,
							projectName: projectName,
							proj_region: proj_region,
							proj_prac: proj_prac,
							cust: cust,
							emp_id: emp_id,
							emp_name: emp_name,
							revenue: revenue,
							emp_prac: emp_prac,
							IsRecognised: isRecognised,
							pro_actual_strt_date: pro_actual_strt_date,
							pro_actual_end_date: pro_actual_end_date,
							allo_strt_date: allocation_strt_date,
							allo_end_date: allocation_end_date,
							emp_bill_rate: emp_bill_rate,
							is_monthly_hourly: is_monthly_hourly,
							emp_onsite_offsite: emp_onsite_offsite,
							projectCurrency: pro_currency_name,
							emp_prcnt_allocation: emp_prcnt_allocation,
							RevenueData: {}
						};
						
						projectWiseRevenue[emp_id_prj_id].RevenueData[allocation_end_date] = new yearData();
						
						projectWiseRevenue[emp_id_prj_id].RevenueData[allocation_end_date][monthName].Amount = revenue;
						projectWiseRevenue[emp_id_prj_id].RevenueData[allocation_end_date][monthName].IsRecognised = isRecognised;
					
						//unique_proj.push(projectId);
						
						/*if (!projectWiseRevenue[emp_id].RevenueData[projectId]) {
							projectWiseRevenue[emp_id].RevenueData[projectId] = new yearData();
						}
						
						projectWiseRevenue[emp_id].RevenueData[projectId][monthName].Amount = revenue;
						projectWiseRevenue[emp_id].RevenueData[projectId][monthName].IsRecognised = isRecognised;*/
						
						
					}
					else
					{
						if (!projectWiseRevenue[emp_id_prj_id].RevenueData[allocation_end_date])
						{
							//emp_id = emp_id +'_'+sr_no;
						
							//nlapiLogExecution('audit','inside create proj rcrd');
							projectWiseRevenue[emp_id_prj_id] = {
								project_id: projectId,
								projectName: projectName,
								proj_region: proj_region,
								proj_prac: proj_prac,
								cust: cust,
								emp_id: emp_id,
								emp_name: emp_name,
								revenue: revenue,
								emp_prac: emp_prac,
								IsRecognised: isRecognised,
								pro_actual_strt_date: pro_actual_strt_date,
								pro_actual_end_date: pro_actual_end_date,
								allo_strt_date: allocation_strt_date,
								allo_end_date: allocation_end_date,
								emp_bill_rate: emp_bill_rate,
								is_monthly_hourly: is_monthly_hourly,
								emp_onsite_offsite: emp_onsite_offsite,
								projectCurrency: pro_currency_name,
								emp_prcnt_allocation: emp_prcnt_allocation,
								RevenueData: {}
							};
							
							projectWiseRevenue[emp_id_prj_id].RevenueData[allocation_end_date] = new yearData();
							
							projectWiseRevenue[emp_id_prj_id].RevenueData[allocation_end_date][monthName].Amount = revenue;
							projectWiseRevenue[emp_id_prj_id].RevenueData[allocation_end_date][monthName].IsRecognised = isRecognised;
						}
						else
						{
							projectWiseRevenue[emp_id_prj_id].RevenueData[allocation_end_date][monthName].Amount = revenue;
							projectWiseRevenue[emp_id_prj_id].RevenueData[allocation_end_date][monthName].IsRecognised = isRecognised;
						}
					}
					
				//}
				
				/*if (!projectWiseRevenue[emp_id].RevenueData[emp_prac]) {
					projectWiseRevenue[internal_id].RevenueData[internal_id] = new yearData();
				}

				projectWiseRevenue[internal_id].RevenueData[internal_id][monthName].Amount = revenue;
				projectWiseRevenue[internal_id].RevenueData[internal_id][monthName].IsRecognised = isRecognised;*/
				
			}
		}
		
		var css_file_url = "";

		var context = nlapiGetContext();
		if (context.getEnvironment() == "SANDBOX") {
			css_file_url = "https://system.na1.netsuite.com/core/media/media.nl?id=199133&c=3883006&h=64fa194295cd4ab64d58&mv=iq3kjr6g&_xt=.css&whence=";
		} else if (context.getEnvironment() == "PRODUCTION") {
			css_file_url = "https://system.na1.netsuite.com/core/media/media.nl?id=199133&c=3883006&h=64fa194295cd4ab64d58&mv=iq3kjr6g&_xt=.css&whence=";
		}

		var html = "";
		html += "<link href='" + css_file_url + "'  rel='stylesheet'>";

		html += "<table class='projection-table'>";

		// header
		html += "<tr class='header-row'>";
		html += "<td>";
		html += "Customer Name";
		html += "</td>";

		html += "<td>";
		html += "Region";
		html += "</td>";
		
		html += "<td>";
		html += "Project Name";
		html += "</td>";
		
		html += "<td>";
		html += "Project Curreny";
		html += "</td>";
		
		html += "<td>";
		html += "Project Start Date";
		html += "</td>";
		
		html += "<td>";
		html += "Project End Date";
		html += "</td>";

		html += "<td>";
		html += "Executing Practice";
		html += "</td>";

		html += "<td>";
		html += "Employee Name";
		html += "</td>";

		html += "<td>";
		html += "Employee Practice";
		html += "</td>";
		
		html += "<td>";
		html += "Allocation Start Date";
		html += "</td>";
		
		html += "<td>";
		html += "Allocation End Date";
		html += "</td>";
		
		html += "<td>";
		html += "Percent Allocated";
		html += "</td>";
		
		html += "<td>";
		html += "Bill Rate";
		html += "</td>";
		
		html += "<td>";
		html += "Monthly/Hourly";
		html += "</td>";
		
		html += "<td>";
		html += "Onsite/Offsite";
		html += "</td>";

		var monthNames = [ "JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JUL",
		        "AUG", "SEP", "OCT", "NOV", "DEC" ];

		var today = new Date();
		var currentMonthName = getMonthName(nlapiDateToString(today));
		
		for (var i = 0; i < monthNames.length; i++) {
			html += "<td>";
			html += monthNames[i];
			html += "</td>";
		}

		html += "</tr>";

		for ( var emp_internal_id in projectWiseRevenue) {
			//nlapiLogExecution('audit','emp_internal_id');
			for ( var project_internal_id in projectWiseRevenue[emp_internal_id].RevenueData) {
				//nlapiLogExecution('audit','projectData[project].EntityId',projectData[project].EntityId);
				html += "<tr>";
				html += "<td class='label-name'>";
				html += projectWiseRevenue[emp_internal_id].cust;
				html += "</td>";

				html += "<td class='label-name'>";
				html += projectWiseRevenue[emp_internal_id].proj_region;
				html += "</td>";
				
				html += "<td class='label-name'>";
				html += projectWiseRevenue[emp_internal_id].projectName;
				html += "</td>";
				
				html += "<td class='label-name'>";
				html += projectWiseRevenue[emp_internal_id].projectCurrency;
				html += "</td>";
				
				html += "<td class='label-name'>";
				html += projectWiseRevenue[emp_internal_id].pro_actual_strt_date;
				html += "</td>";
				
				html += "<td class='label-name'>";
				html += projectWiseRevenue[emp_internal_id].pro_actual_end_date;
				html += "</td>";
				
				html += "<td class='label-name'>";
				html += projectWiseRevenue[emp_internal_id].proj_prac;
				html += "</td>";

				html += "<td class='label-name'>";
				html += projectWiseRevenue[emp_internal_id].emp_name;
				html += "</td>";

				html += "<td class='label-name'>";
				html += projectWiseRevenue[emp_internal_id].emp_prac;
				html += "</td>";
				
				html += "<td class='label-name'>";
				html += projectWiseRevenue[emp_internal_id].allo_strt_date;
				html += "</td>";
				
				html += "<td class='label-name'>";
				html += projectWiseRevenue[emp_internal_id].allo_end_date;
				html += "</td>";
				
				html += "<td class='label-name'>";
				html += projectWiseRevenue[emp_internal_id].emp_prcnt_allocation;
				html += "</td>";
				
				html += "<td class='label-name'>";
				html += projectWiseRevenue[emp_internal_id].emp_bill_rate;
				html += "</td>";
				
				html += "<td class='label-name'>";
				html += projectWiseRevenue[emp_internal_id].is_monthly_hourly;
				html += "</td>";
				
				html += "<td class='label-name'>";
				html += projectWiseRevenue[emp_internal_id].emp_onsite_offsite;
				html += "</td>";

				for ( var month in projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id]) {
					// check if past month
					//nlapiLogExecution('audit','month in arr: '+projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id][month].Amount);
					var currentMonthPos = monthNames.indexOf(month);
					var currentMonthDate = nlapiStringToDate((currentMonthPos + 1)
					        + '/31/2016');

					if (currentMonthDate < new Date()) {
						// if
						// (projectWiseRevenue[project].RevenueData[practice][month].IsRecognised)
						// {
						html += "<td class='monthly-amount'>";
					} else {
						html += "<td class='projected-amount'>";
					}
					//month = 'AUG';
					html += projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id][month].Amount;
					html += "</td>";

				}
				html += "</tr>";
			}
		}

		html += "</table>";

		return html;
	} catch (err) {
		nlapiLogExecution('ERROR', 'createProjectionTable', err);
		throw err;
	}
}

function searchRecord(recordType, savedSearch, arrFilters, arrColumns,filterExpression)
{
	try {
		var search = null;

		// if a saved search is provided, load it and add the filters and
		// columns
		if (isNotEmpty(savedSearch)) {
			search = nlapiLoadSearch(recordType, savedSearch);

			if (isArrayNotEmpty(arrFilters)) {
				search.addFilters(arrFilters);
			}

			if (isArrayNotEmpty(arrColumns)) {
				search.addColumns(arrColumns);
			}

			if (isArrayNotEmpty(filterExpression)) {
				search.setFilterExpression(filterExpression);
			}
		}
		// create a new search
		else {
			search = nlapiCreateSearch(recordType, arrFilters, arrColumns);
		}

		// run search
		var resultSet = search.runSearch();

		// iterate through the search and get all data 1000 at a time
		var searchResultCount = 0;
		var resultSlice = null;
		var searchResult = [];

		do {
			resultSlice = resultSet.getResults(searchResultCount,
			        searchResultCount + 1000);

			if (resultSlice) {

				resultSlice.forEach(function(result) {

					searchResult.push(result);
					searchResultCount++;
				});
			}
		} while (isArrayNotEmpty(resultSlice) && resultSlice.length >= 1000);

		return searchResult;
	} catch (err) {
		nlapiLogExecution('ERROR', 'searchRecord', err);
		throw err;
	}
}

function searchEmp(emp_id,projectId,emp_unique_proj)
{
	for(var jk=0; jk<emp_unique_proj.length ; jk++)
	{
		var proj_ID = emp_unique_proj[jk].project_id;
		var Emp_ID = emp_unique_proj[jk].employee_id;
		
		if(parseInt(Emp_ID) == parseInt(emp_id) && parseInt(proj_ID)== parseInt(projectId))
		{
			return true;
		}
		else
		{
			return false;
		}
	}
}

function isEmpty(value) {

	return value == null || value == "" || typeof (value) == undefined;
}

function isNotEmpty(value) {

	return !isEmpty(value);
}

function isArrayEmpty(argArray) {

	return !isArrayNotEmpty(argArray);
}

function isArrayNotEmpty(argArray) {

	return (isNotEmpty(argArray) && argArray.length > 0);
}

function yearData() {
	var monthNames = [ "JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JUL", "AUG",
	        "SEP", "OCT", "NOV", "DEC" ];

	for (var i = 0; i < monthNames.length; i++) {
		this[monthNames[i]] = {
		    Amount : 0,
		    IsRecognised : true
		};
	}
}

function legendsTable() {
	var htmlText = "";
	htmlText += "<table class='legend-table'>";
	htmlText += "<tr><td class=''></td><td>Legends</td></tr>";
	htmlText += "<tr><td class='recognised-circle'></td><td>Recognised</td></tr>";
	htmlText += "<tr><td class='projected-circle'></td><td>Projected</td></tr>";
	htmlText += "</table>";
	return htmlText;
}

function getMonthName(currentDate) {
	currentDate = nlapiStringToDate(currentDate)
	var monthNames = [ "JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JUL", "AUG",
	        "SEP", "OCT", "NOV", "DEC" ];
	return monthNames[currentDate.getMonth()];
}

function getDataRefreshDate() {
	try {
		var refreshDate = '';

		var dateSearch = nlapiSearchRecord(
		        'customrecord_emp_practice_rev_projection', null, null,
		        new nlobjSearchColumn('formuladate', null, 'max')
		                .setFormula('{created}'));

		if (dateSearch) {
			refreshDate = dateSearch[0].getValue('formuladate', null, 'max');
		}

		return refreshDate;
	} catch (err) {
		nlapiLogExecution('ERROR', 'getDataRefreshDate', err);
		throw err;
	}
}

function isRefreshOngoing() {
	try {
		var isRefreshOnGoing = false;

		var search = nlapiSearchRecord('scheduledscriptinstance', null,
		        [
		                new nlobjSearchFilter('internalid', 'script', 'anyof',
		                        [ 938 ]),
		                new nlobjSearchFilter('status', null, 'anyof', [
		                        'RETRY', 'PROCESSING', 'PENDING' ]) ])

		if (search) {
			isRefreshOnGoing = true;
		}

		return isRefreshOnGoing;
	} catch (err) {
		nlapiLogExecution('ERROR', 'isRefreshOngoing', err);
		throw err;
	}
}

function _logValidation(value){
	if (value != 'null' && value != null && value != null && value != '' && value != undefined && value != undefined && value != 'undefined' && value != 'undefined' && value != 'NaN' && value != NaN) {
		return true;
	}
	else {
		return false;
	}
}

