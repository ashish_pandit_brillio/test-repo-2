/**
 * Module Description
 * 
 * Version Date Author Remarks 1.00 Apr 17 2015 Nitish Mishra
 * 
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your
 * script deployment.
 * 
 * @appliedtorecord recordType
 * 
 * @param {String}
 *        type Operation types: create, edit, delete, xedit, approve, cancel,
 *        reject (SO, ER, Time Bill, PO & RMA only) pack, ship (IF only)
 *        dropship, specialorder, orderitems (PO only) paybills (vendor
 *        payments)
 * @returns {Void}
 */
function userEventAfterSubmit(type) {

try {

		// check if the joining mail is to be send or not
		var sendJoiningMail = isTrue(nlapiGetFieldValue('custentity_emp_send_joining_email'));
		//sendJoiningMail = true;
		nlapiLogExecution('debug', 'type', type);
		nlapiLogExecution('debug', 'sendJoiningMail', sendJoiningMail);

		// if false, no need to send email
		if (sendJoiningMail) {

			// if type is create, send as per the field value
			if (type == 'create') {

			}
			// if edit, check if the value has been changed and is marked now
			else if (type == 'edit') {
				var oldRecord = nlapiGetOldRecord();

				if (isTrue(oldRecord.getFieldValue('custentity_emp_send_joining_email'))) {
					sendJoiningMail = false;
				}
			}

			nlapiLogExecution('debug', 'sendJoiningMail', sendJoiningMail);
			if (sendJoiningMail) {

				// get the person type
				var employeeId = nlapiGetRecordId();
				var mailContent = null;
				var employeeDetails = nlapiLookupField('employee', employeeId, [
					'firstname', 'email', 'custentity_persontype','department'
				]);
              var emp_dep_for_parent=employeeDetails.department;
				var parent_dep= nlapiLookupField('department',emp_dep_for_parent,'custrecord_parent_practice');
				// if email id contains "@brillio.com" send the salaried mail,
				// else send the contigent worker mail
				if(employeeDetails.department == '191' || employeeDetails.department == '485'|| parent_dep == '492' || parent_dep == '527')
				{
				// get mail content as per the person type
				if (doesEmailContainsBrillio(employeeDetails.email)) {
					mailContent = getSalariedMailTemplate(employeeDetails);
				}
				
				// send email to the new joinee
				if (isNotEmpty(mailContent)) {
					var file_obj = nlapiLoadFile('898070');
					nlapiSendEmail(129799, employeeDetails.email,
							mailContent.Subject, mailContent.Body, null, 'sai.vannamareddy@brillio.com', {
								entity : employeeId
							},file_obj);
                                        //sendNewJoineeEmail(employeeId);
				}
				else {
					throw "Invalid Person Type";
				}
				}
			}
		}
	}
	catch (err) {
		nlapiLogExecution('error', 'userEventAfterSubmit', err);
		throw err;
	}
}

function doesEmailContainsBrillio(email) {

	email = email.toLowerCase();
	return email.indexOf("@brillio.com") > -1;
}

function getSalariedMailTemplate(employeeDetails) {

	var htmlText = '<style> p,li {font-size : 11; font-family : "verdana"}</style>';
	htmlText += '<p>Hi ' + employeeDetails.firstname + ',<p>';
	htmlText +=
			'<p>Welcome Onboard!!</p>'
					+ '<p>Please visit the below link to update your Skills and certification.</p>'
					+ '<a href=https://system.na1.netsuite.com/app/site/hosting/scriptlet.nl?script=1818&deploy=1>Skills and certification</a>'
					+ '<br/><p>Thanks and All The Best,<br/>' + 'Fulfillment Team</p>';

	return {
		Subject : 'Skill Update',
		Body : addMailTemplate(htmlText)
	};
}
