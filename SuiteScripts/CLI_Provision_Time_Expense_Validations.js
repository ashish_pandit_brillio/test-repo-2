/**
 * @author Shweta
 */
// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
/*
   	Script Name : CLI_Provision_Time_Expense_Validations.js
	Author      : Shweta Chopde
	Date        : 12 May 2014
	Description : Validation on  Unbilled Expense / Time form


	Script Modification Log:

	-- Date --			-- Modified By --				--Requested By--				-- Description --




	Below is a summary of the process controls enforced by this script file.  The control logic is described
	more fully, below, in the appropriate function headers and code blocks.

     PAGE INIT
		- pageInit(type)


     SAVE RECORD
		- saveRecord()


     VALIDATE FIELD
		- validateField(type, name, linenum)


     FIELD CHANGED
		- fieldChanged(type, name, linenum)


     POST SOURCING
		- postSourcing(type, name)


	LINE INIT
		- lineInit(type)


     VALIDATE LINE
		- validateLine()


     RECALC
		- reCalc()


     SUB-FUNCTIONS
		- The following sub-functions are called by the above core functions in order to maintain code
            modularization:





*/
}
// END SCRIPT DESCRIPTION BLOCK  ====================================



// BEGIN SCRIPT UPDATION BLOCK  ====================================
/*


*/
// END SCRIPT UPDATION BLOCK  ====================================




// BEGIN GLOBAL VARIABLE BLOCK  =====================================
{

	//  Initialize any Global Variables, in particular, debugging variables...


}
// END GLOBAL VARIABLE BLOCK  =======================================





// BEGIN PAGE INIT ==================================================

function pageInit(type)
{
    /*  On page init:

	- PURPOSE

		FIELDS USED:

		--Field Name--				--ID--			--Line Item Name--

    */

    //  LOCAL VARIABLES


    //  PAGE INIT CODE BODY


}

// END PAGE INIT ====================================================





// BEGIN SAVE RECORD ================================================
var a_data_array = ''
var a_data_process_array = ''
var a_data_criteria_array = ''
//----------------------------------------
var a_data_array_1 = '';
var a_data_array_2 = '';
var a_data_array_3 = '';
var a_data_array_4 = '';
var a_data_array_5 = '';
var a_data_array_6 = '';
//----------------------------------------
function saveRecord_provision()
{
	var i_current_user = nlapiGetUser();
    var i_line_count = nlapiGetLineItemCount('custpage_time_expense_sublist')
	var i_unbilled_type = nlapiGetFieldValue('custpage_unbilled_type')
	var i_cnt = 0;		
	var i_pnt = 0;
	if(_logValidation(i_line_count))
	{
	  for(var i=1;i<=i_line_count;i++)
	  {	  
		if(i_unbilled_type == 1)
		{		
			var i_select = nlapiGetLineItemValue('custpage_time_expense_sublist','custpage_select',i)
			var i_employee = nlapiGetLineItemValue('custpage_time_expense_sublist','custpage_employee',i)
			var i_project = nlapiGetLineItemValue('custpage_time_expense_sublist','custpage_project',i)
			var i_vertical = nlapiGetLineItemValue('custpage_time_expense_sublist','custpage_vertical',i)
			var i_practice = nlapiGetLineItemValue('custpage_time_expense_sublist','custpage_practice',i)	
			var i_date = nlapiGetLineItemValue('custpage_time_expense_sublist','custpage_date',i)
			var i_amount = nlapiGetLineItemValue('custpage_time_expense_sublist','custpage_amount',i)
			var i_currency = nlapiGetLineItemValue('custpage_time_expense_sublist','custpage_currency',i)
			var i_exchange_rate = nlapiGetLineItemValue('custpage_time_expense_sublist','custpage_exchange_rate',i)
			var i_internalID = nlapiGetLineItemValue('custpage_time_expense_sublist','custpage_internal_id',i)
			var i_subsidiary = nlapiGetLineItemValue('custpage_time_expense_sublist','custpage_subsidiary',i)
			var i_location = nlapiGetLineItemValue('custpage_time_expense_sublist','custpage_location',i)
			if(i_date == null || i_date == '')
			{
			}
			else
			{
				var i_date='11/26/2014';
			}				
			if(i_select == 'T')
			{	
                //--------------------------------------------------------------------------------------
					if(i_currency == '1' || i_currency == 'USD')
					{//if start
					   //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
						if(a_data_array_1 =='')
						{
							a_data_array_1 = i_employee+'#####'+i_project+'#####'+i_vertical+'#####'+i_practice+'#####'+i_date+'#####'+i_amount+'#####'+i_currency+'#####'+i_exchange_rate+'#####'+i_internalID+'#####'+i_subsidiary+'#####'+i_location
						}
						else
						{
							a_data_array_1=a_data_array_1+','+i_employee+'#####'+i_project+'#####'+i_vertical+'#####'+i_practice+'#####'+i_date+'#####'+i_amount+'#####'+i_currency+'#####'+i_exchange_rate+'#####'+i_internalID+'#####'+i_subsidiary+'#####'+i_location
						}
					   //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
					}//if close
					else if(i_currency == '2' || i_currency == 'GBP')
					{
					
					  //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
						if(a_data_array_2 =='')
						{
							a_data_array_2 = i_employee+'#####'+i_project+'#####'+i_vertical+'#####'+i_practice+'#####'+i_date+'#####'+i_amount+'#####'+i_currency+'#####'+i_exchange_rate+'#####'+i_internalID+'#####'+i_subsidiary+'#####'+i_location
						}
						else
						{
							a_data_array_2=a_data_array_2+','+i_employee+'#####'+i_project+'#####'+i_vertical+'#####'+i_practice+'#####'+i_date+'#####'+i_amount+'#####'+i_currency+'#####'+i_exchange_rate+'#####'+i_internalID+'#####'+i_subsidiary+'#####'+i_location
						}
					   //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
					}
					else if(i_currency == '4' || i_currency == 'EUR')
					{
					
					      //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
						if(a_data_array_3 =='')
						{
							a_data_array_3 = i_employee+'#####'+i_project+'#####'+i_vertical+'#####'+i_practice+'#####'+i_date+'#####'+i_amount+'#####'+i_currency+'#####'+i_exchange_rate+'#####'+i_internalID+'#####'+i_subsidiary+'#####'+i_location
						}
						else
						{
							a_data_array_3=a_data_array_3+','+i_employee+'#####'+i_project+'#####'+i_vertical+'#####'+i_practice+'#####'+i_date+'#####'+i_amount+'#####'+i_currency+'#####'+i_exchange_rate+'#####'+i_internalID+'#####'+i_subsidiary+'#####'+i_location
						}
					   //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
					}
					else if(i_currency == '5' || i_currency == 'PHP' || i_currency == 'Peso')
					{
					    //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
						if(a_data_array_4 =='')
						{
							a_data_array_4 = i_employee+'#####'+i_project+'#####'+i_vertical+'#####'+i_practice+'#####'+i_date+'#####'+i_amount+'#####'+i_currency+'#####'+i_exchange_rate+'#####'+i_internalID+'#####'+i_subsidiary+'#####'+i_location
						}
						else
						{
							a_data_array_4=a_data_array_4+','+i_employee+'#####'+i_project+'#####'+i_vertical+'#####'+i_practice+'#####'+i_date+'#####'+i_amount+'#####'+i_currency+'#####'+i_exchange_rate+'#####'+i_internalID+'#####'+i_subsidiary+'#####'+i_location
						}
					   //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
					}
					else if(i_currency == '6' || i_currency == 'INR')
					{
					   //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
						if(a_data_array_5 =='')
						{
							a_data_array_5 = i_employee+'#####'+i_project+'#####'+i_vertical+'#####'+i_practice+'#####'+i_date+'#####'+i_amount+'#####'+i_currency+'#####'+i_exchange_rate+'#####'+i_internalID+'#####'+i_subsidiary+'#####'+i_location
						}
						else
						{
							a_data_array_5=a_data_array_5+','+i_employee+'#####'+i_project+'#####'+i_vertical+'#####'+i_practice+'#####'+i_date+'#####'+i_amount+'#####'+i_currency+'#####'+i_exchange_rate+'#####'+i_internalID+'#####'+i_subsidiary+'#####'+i_location
						}
					   //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
					}
					else if(i_currency == '7' || i_currency == 'SGD')
					{
					   //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
						if(a_data_array_6 =='')
						{
							a_data_array_6 = i_employee+'#####'+i_project+'#####'+i_vertical+'#####'+i_practice+'#####'+i_date+'#####'+i_amount+'#####'+i_currency+'#####'+i_exchange_rate+'#####'+i_internalID+'#####'+i_subsidiary+'#####'+i_location
						}
						else
						{
							a_data_array_6=a_data_array_6+','+i_employee+'#####'+i_project+'#####'+i_vertical+'#####'+i_practice+'#####'+i_date+'#####'+i_amount+'#####'+i_currency+'#####'+i_exchange_rate+'#####'+i_internalID+'#####'+i_subsidiary+'#####'+i_location
						}
					   //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
					}
                			
              /* if(a_data_array =='')
			   {
			   	a_data_array = i_employee+'#####'+i_project+'#####'+i_vertical+'#####'+i_practice+'#####'+i_date+'#####'+i_amount+'#####'+i_currency+'#####'+i_exchange_rate+'#####'+i_internalID+'#####'+i_subsidiary+'#####'+i_location
			   }
			   else
			   {
			   	a_data_array=a_data_array+','+i_employee+'#####'+i_project+'#####'+i_vertical+'#####'+i_practice+'#####'+i_date+'#####'+i_amount+'#####'+i_currency+'#####'+i_exchange_rate+'#####'+i_internalID+'#####'+i_subsidiary+'#####'+i_location
			   }*/
			   if(a_data_process_array =='')
			   {
			   	a_data_process_array = i_internalID
			   }
			   else
			   {
			   	a_data_process_array=a_data_process_array+','+i_internalID
			   }
			   if(a_data_criteria_array =='')
			   {
			   	a_data_criteria_array = i_employee+'#####'+i_project+'#####'+i_subsidiary
			   }
			   else
			   {
			   	a_data_criteria_array=a_data_criteria_array+','+i_employee+'#####'+i_project+'#####'+i_subsidiary
			   }
			
			   
            }//Select T				
          
		}//Expense
		if(i_unbilled_type == 2)
		{
			var i_select = nlapiGetLineItemValue('custpage_time_expense_sublist','custpage_select',i)
			var i_employee = nlapiGetLineItemValue('custpage_time_expense_sublist','custpage_employee',i)
			//alert('i_employee'+i_employee);
			var i_project = nlapiGetLineItemValue('custpage_time_expense_sublist','custpage_project',i)
			var i_vertical = nlapiGetLineItemValue('custpage_time_expense_sublist','custpage_vertical',i)
			//alert('i_project'+i_project);
			var i_practice = nlapiGetLineItemValue('custpage_time_expense_sublist','custpage_practice',i)	
			var i_date = nlapiGetLineItemValue('custpage_time_expense_sublist','custpage_date',i)
			var i_hours = nlapiGetLineItemValue('custpage_time_expense_sublist','custpage_hours',i)
			var i_rate = nlapiGetLineItemValue('custpage_time_expense_sublist','custpage_rate',i)
			var i_amount = nlapiGetLineItemValue('custpage_time_expense_sublist','custpage_amount',i)
			var i_currency = nlapiGetLineItemValue('custpage_time_expense_sublist','custpage_currency',i)
		    var i_exchange_rate = nlapiGetLineItemValue('custpage_time_expense_sublist','custpage_exchange_rate',i)
			var i_internalID = nlapiGetLineItemValue('custpage_time_expense_sublist','custpage_internal_id',i)
		    var i_subsidiary = nlapiGetLineItemValue('custpage_time_expense_sublist','custpage_subsidiary',i)
			var i_customer = nlapiGetLineItemValue('custpage_time_expense_sublist','custpage_customer',i)
			var i_location = nlapiGetLineItemValue('custpage_time_expense_sublist','custpage_location',i)
			var i_item = nlapiGetLineItemValue('custpage_time_expense_sublist','custpage_item',i)
			var i_month_of_date = nlapiGetLineItemValue('custpage_time_expense_sublist','custpage_month_of_date',i)
			
			if(i_date == null || i_date == '' || i_date == 'null' || i_date == 'undefined')
			{
				var i_date='03/12/2014';
			}				
			if(i_select == 'T')
			{
			   //--------------------------------------------------------------------------------------
					if(i_currency == '1' || i_currency == 'USD')
					{//if start
					   //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
							if(a_data_array_1 =='')
						   {
							a_data_array_1 = i_employee+'#####'+i_project+'#####'+i_vertical+'#####'+i_practice+'#####'+i_amount+'#####'+i_currency+'#####'+i_hours+'#####'+i_rate+'#####'+i_exchange_rate+'#####'+i_internalID+'#####'+i_subsidiary+'#####'+i_customer+'#####'+i_location+'#####'+i_item+'#####'+i_month_of_date
						   }
						   else
						   {
							a_data_array_1=a_data_array_1+','+i_employee+'#####'+i_project+'#####'+i_vertical+'#####'+i_practice+'#####'+i_amount+'#####'+i_currency+'#####'+i_hours+'#####'+i_rate+'#####'+i_exchange_rate+'#####'+i_internalID+'#####'+i_subsidiary+'#####'+i_customer+'#####'+i_location+'#####'+i_item+'#####'+i_month_of_date
						   }
					   //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
					}//if close
					else if(i_currency == '2' || i_currency == 'GBP')
					{
					
					    //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
							if(a_data_array_2 =='')
						   {
							a_data_array_2 = i_employee+'#####'+i_project+'#####'+i_vertical+'#####'+i_practice+'#####'+i_amount+'#####'+i_currency+'#####'+i_hours+'#####'+i_rate+'#####'+i_exchange_rate+'#####'+i_internalID+'#####'+i_subsidiary+'#####'+i_customer+'#####'+i_location+'#####'+i_item+'#####'+i_month_of_date
						   }
						   else
						   {
							a_data_array_2=a_data_array_2+','+i_employee+'#####'+i_project+'#####'+i_vertical+'#####'+i_practice+'#####'+i_amount+'#####'+i_currency+'#####'+i_hours+'#####'+i_rate+'#####'+i_exchange_rate+'#####'+i_internalID+'#####'+i_subsidiary+'#####'+i_customer+'#####'+i_location+'#####'+i_item+'#####'+i_month_of_date
						   }
					   //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
					}
					else if(i_currency == '4' || i_currency == 'EUR')
					{
					
					      //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
							if(a_data_array_3 =='')
						   {
							a_data_array_3 = i_employee+'#####'+i_project+'#####'+i_vertical+'#####'+i_practice+'#####'+i_amount+'#####'+i_currency+'#####'+i_hours+'#####'+i_rate+'#####'+i_exchange_rate+'#####'+i_internalID+'#####'+i_subsidiary+'#####'+i_customer+'#####'+i_location+'#####'+i_item+'#####'+i_month_of_date
						   }
						   else
						   {
							a_data_array_3=a_data_array_3+','+i_employee+'#####'+i_project+'#####'+i_vertical+'#####'+i_practice+'#####'+i_amount+'#####'+i_currency+'#####'+i_hours+'#####'+i_rate+'#####'+i_exchange_rate+'#####'+i_internalID+'#####'+i_subsidiary+'#####'+i_customer+'#####'+i_location+'#####'+i_item+'#####'+i_month_of_date
						   }
						  
					   //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
					}
					else if(i_currency == '5' || i_currency == 'PHP' || i_currency == 'Peso')
					{
					     //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
							if(a_data_array_4 =='')
						   {
							a_data_array_4 = i_employee+'#####'+i_project+'#####'+i_vertical+'#####'+i_practice+'#####'+i_amount+'#####'+i_currency+'#####'+i_hours+'#####'+i_rate+'#####'+i_exchange_rate+'#####'+i_internalID+'#####'+i_subsidiary+'#####'+i_customer+'#####'+i_location+'#####'+i_item+'#####'+i_month_of_date
						   }
						   else
						   {
							a_data_array_4=a_data_array_4+','+i_employee+'#####'+i_project+'#####'+i_vertical+'#####'+i_practice+'#####'+i_amount+'#####'+i_currency+'#####'+i_hours+'#####'+i_rate+'#####'+i_exchange_rate+'#####'+i_internalID+'#####'+i_subsidiary+'#####'+i_customer+'#####'+i_location+'#####'+i_item+'#####'+i_month_of_date
						   }
					   //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
					}
					else if(i_currency == '6' || i_currency == 'INR')
					{
					     //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
							if(a_data_array_5 =='')
						   {
							a_data_array_5 = i_employee+'#####'+i_project+'#####'+i_vertical+'#####'+i_practice+'#####'+i_amount+'#####'+i_currency+'#####'+i_hours+'#####'+i_rate+'#####'+i_exchange_rate+'#####'+i_internalID+'#####'+i_subsidiary+'#####'+i_customer+'#####'+i_location+'#####'+i_item+'#####'+i_month_of_date
						   }
						   else
						   {
							a_data_array_5=a_data_array_5+','+i_employee+'#####'+i_project+'#####'+i_vertical+'#####'+i_practice+'#####'+i_amount+'#####'+i_currency+'#####'+i_hours+'#####'+i_rate+'#####'+i_exchange_rate+'#####'+i_internalID+'#####'+i_subsidiary+'#####'+i_customer+'#####'+i_location+'#####'+i_item+'#####'+i_month_of_date
						   }
					   //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
					}
					else if(i_currency == '7' || i_currency == 'SGD')
					{
					     //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
							if(a_data_array_6 =='')
						   {
							a_data_array_6 = i_employee+'#####'+i_project+'#####'+i_vertical+'#####'+i_practice+'#####'+i_amount+'#####'+i_currency+'#####'+i_hours+'#####'+i_rate+'#####'+i_exchange_rate+'#####'+i_internalID+'#####'+i_subsidiary+'#####'+i_customer+'#####'+i_location+'#####'+i_item+'#####'+i_month_of_date
						   }
						   else
						   {
							a_data_array_6=a_data_array_6+','+i_employee+'#####'+i_project+'#####'+i_vertical+'#####'+i_practice+'#####'+i_amount+'#####'+i_currency+'#####'+i_hours+'#####'+i_rate+'#####'+i_exchange_rate+'#####'+i_internalID+'#####'+i_subsidiary+'#####'+i_customer+'#####'+i_location+'#####'+i_item+'#####'+i_month_of_date
						   }
					   //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
					}
					
					
			   //---------------------------------------------------------------------------------------
			   /*if(a_data_array =='')
			   {
			   	a_data_array = i_employee+'#####'+i_project+'#####'+i_vertical+'#####'+i_practice+'#####'+i_date+'#####'+i_amount+'#####'+i_currency+'#####'+i_hours+'#####'+i_rate+'#####'+i_exchange_rate+'#####'+i_internalID+'#####'+i_subsidiary+'#####'+i_customer
			   }
			   else
			   {
			   	a_data_array=a_data_array+','+i_employee+'#####'+i_project+'#####'+i_vertical+'#####'+i_practice+'#####'+i_date+'#####'+i_amount+'#####'+i_currency+'#####'+i_hours+'#####'+i_rate+'#####'+i_exchange_rate+'#####'+i_internalID+'#####'+i_subsidiary+'#####'+i_customer
			   }*/
			   
			   if(a_data_process_array =='')
			   {
			   	a_data_process_array = i_internalID
			   }
			   else
			   {
			   	a_data_process_array=a_data_process_array+','+i_internalID
			   }
			    if(a_data_criteria_array =='')
			   {
			   	a_data_criteria_array = i_employee+'#####'+i_project+'#####'+i_subsidiary
			   }
			   else
			   {
			   	a_data_criteria_array=a_data_criteria_array+','+i_employee+'#####'+i_project+'#####'+i_subsidiary
			   }
				
			}				
		}//Time Tracking
		
		
				
	  }//Loop	
	   if(a_data_array =='')
		{
			a_data_array=a_data_array_1+'curr'+a_data_array_2+'curr'+a_data_array_3+'curr'+a_data_array_4+'curr'+a_data_array_5+'curr'+a_data_array_6;
		}
	    nlapiSetFieldValue('custpage_data',a_data_array)
	 	  						
	}//Line Count
	
	//alert('a_data_array===>'+a_data_array);
	
    if(!_logValidation(a_data_process_array))
	{
	  alert(' Please select atleast a single entry from the list .')
	  return false;	
	}
    if(_logValidation(a_data_array))
	{	
	if(_logValidation(a_data_process_array))
	{
	 var a = new Array();
      a['User-Agent-x'] = 'SuiteScript-Call';
	  
	  var resposeObject = nlapiRequestURL('/app/site/hosting/scriptlet.nl?script=295&deploy=1&custscript_data_array_provision=' + a_data_process_array+'&custscript_current_user=' + i_current_user, null, a);//275 for sandbox
  	                    
	  var i_submitID = resposeObject.getBody();
	//  alert('i_submitID '+i_submitID)
	
		
		
	/*
  var o_processOBJ = nlapiCreateRecord('customrecord_provision_processed_records');
	  	
	  o_processOBJ.setFieldValue('custrecord_processed_records_provision',a_data_process_array)
	  o_processOBJ.setFieldValue('custrecord_employee_id_provision',i_current_user)	
	  
	  i_submitID = nlapiSubmitRecord(o_processOBJ,true,true)	
	  nlapiLogExecution('DEBUG', 'ERROR',' Custom Record ID -->' + i_submitID);	
*/
				
	  nlapiSetFieldValue('custpage_custom_id',i_submitID)
	  nlapiSetFieldValue('custpage_data_process',a_data_process_array)
	  nlapiSetFieldValue('custpage_data_criteria',a_data_criteria_array)
			
	}//Data Process Array 		
		
	//  window.open('https://system.sandbox.netsuite.com/app/common/scripting/scriptstatus.nl?&scripttype=250')
	  
	  if(i_unbilled_type == 1)
	  {
	  	 //window.open('https://system.sandbox.netsuite.com/app/common/search/searchresults.nl?searchid=106')
	//  window.open('https://system.sandbox.netsuite.com/app/common/custom/custrecordentrylist.nl?rectype=155');
	 
	  }
	  if(i_unbilled_type == 2)
	  {
	  	//window.open('https://system.sandbox.netsuite.com/app/common/search/searchresults.nl?searchid=104');
	 
	// window.open('https://system.sandbox.netsuite.com/app/common/custom/custrecordentrylist.nl?rectype=155');
	 
	 
      }	  
	}
	return true;
}

// END SAVE RECORD ==================================================

// BEGIN VALIDATE FIELD =============================================

function validateField(type, name, linenum)
{

	/*  On validate field:

          - EXPLAIN THE PURPOSE OF THIS FUNCTION


          FIELDS USED:

          --Field Name--				--ID--


    */


	//  LOCAL VARIABLES



	//  VALIDATE FIELD CODE BODY


	return true;

}

// END VALIDATE FIELD ===============================================





// BEGIN FIELD CHANGED ==============================================

function fieldChanged(type, name, linenum)
{
    /*  On field changed:

          - PURPOSE


          FIELDS USED:

			--Field Name--				--ID--

    */


    //  LOCAL VARIABLES


    //  FIELD CHANGED CODE BODY

	//alert('Field Name='+name+'\nLine Item Type='+type+'\nField Value='+nlapiGetFieldValue(name)+'\nLine Item Value='+nlapiGetLineItemValue(type, name, 1));
	//------------------------added by swati------------------------------------
	 if (name == 'custpage_rows') {
	  var rowCount = nlapiGetFieldValue('custpage_rows');
	  nlapiSetFieldValue('custpage_rows',rowCount,false);
	  var url = window.location.search;
	    var customer = nlapiGetFieldValue('custpage_customer');
	  if (url.indexOf('&param_rowCount') > 0) {
	   // Remove the previous value of the parameter: param_rowCount
	   url = url.substring(0, url.indexOf('&param_rowCount'));
	  }
	  // The code below refreshes the page and passes the value of the dropdown
	  // in the URL as a parameter
	  window.location.search = url + '&param_rowCount=' + rowCount + '&test=1'+'&customer' + customer;
	 }
	  if (name == 'custpage_customer') 
	  {
	  var customer = nlapiGetFieldValue('custpage_customer');
	  nlapiSetFieldValue('custpage_customer',customer,false);
	  var url = window.location.search;
	  if (url.indexOf('&custpage_customer') > 0) {
	   // Remove the previous value of the parameter: param_rowCount
	   url = url.substring(0, url.indexOf('&custpage_customer'));
	  }
	  // The code below refreshes the page and passes the value of the dropdown
	  // in the URL as a parameter
	  window.location.search = url + '&custpage_customer=' + customer;
	 }
	//--------------------------------------------------------------------------

}

// END FIELD CHANGED ================================================





// BEGIN POST SOURCING ==============================================

function postSourcing(type, name)
{

    /*  On post sourcing:

	- PURPOSE

          FIELDS USED:

		--Field Name--			--ID--		--Line Item Name--


    */

    //  LOCAL VARIABLES


    //  POST SOURCING CODE BODY



}

// END POST SOURCING ================================================



// BEGIN LINE INIT ==============================================

function lineInit(type)
{

    /*  On Line Init:

	- PURPOSE

          FIELDS USED:

		--Field Name--			--ID--		--Line Item Name--


    */

    //  LOCAL VARIABLES


    //  LINE INIT CODE BODY



}

// END LINE INIT ================================================


// BEGIN VALIDATE LINE ==============================================

function validateLine(type)
{

	/*  On validate line:

          - EXPLAIN THE PURPOSE OF THIS FUNCTION


          FIELDS USED:

          --Field Name--				--ID--


    */


	//  LOCAL VARIABLES


	//  VALIDATE LINE CODE BODY


	return true;

}

// END VALIDATE LINE ================================================




// BEGIN RECALC =====================================================

function recalc(type)
{

	/*  On recalc:

          - EXPLAIN THE PURPOSE OF THIS FUNCTION


          FIELDS USED:

          --Field Name--				--ID--


    */


	//  LOCAL VARIABLES


	//  RECALC CODE BODY


}

// END RECALC =======================================================





// BEGIN FUNCTION ===================================================
function mark_all()
{	
   var i_line_count = nlapiGetLineItemCount('custpage_time_expense_sublist')
		
	if(i_line_count!=null && i_line_count!='' && i_line_count!=0 && i_line_count!= undefined)
	{		
		for(var a=1;a<=i_line_count;a++)
		{		  
		   nlapiSelectLineItem('custpage_time_expense_sublist',a)
		   nlapiSetCurrentLineItemValue('custpage_time_expense_sublist','custpage_select','T')
		   nlapiCommitLineItem('custpage_time_expense_sublist');
		}
				
	}

}
function unmark_all()
{
	var i_line_count = nlapiGetLineItemCount('custpage_time_expense_sublist')
		
	if(i_line_count!=null && i_line_count!='' && i_line_count!=0 && i_line_count!= undefined)
	{		
		for(var a=1;a<=i_line_count;a++)
		{		  
		   nlapiSelectLineItem('custpage_time_expense_sublist',a)
		   nlapiSetCurrentLineItemValue('custpage_time_expense_sublist','custpage_select','F')
		   nlapiCommitLineItem('custpage_time_expense_sublist');
		}
				
	}
}


function refresh()
{
	location.reload();
}


function redirect_to_home_page()
{
	window.open('/app/site/hosting/scriptlet.nl?script=233&deploy=1','_self',null)
 }


//---------------------------Export Excel Code-----------------------------------------
/*
Code Added By Swati 
date:-04-02-2014
This code ,to export suitelet screen data into excel file.

*/

function export_excel()
{//fun start

    alert('Excel File Download Wait For Some Time !!');
	var d_from_date=nlapiGetFieldValue('custpage_from_date');
	//alert('d_from_date====>'+d_from_date);
	nlapiLogExecution('DEBUG', 'ClinetFunction', 'd_from_date= '+d_from_date);
	
	var d_to_date=nlapiGetFieldValue('custpage_to_date');
	//alert('d_to_date====>'+d_to_date);
	nlapiLogExecution('DEBUG', 'ClinetFunction', 'd_to_date= '+d_to_date);
	
	var o_subsidairy=nlapiGetFieldValue('custpage_user_subsidiary');
	//alert('o_subsidairy====>'+o_subsidairy);
	nlapiLogExecution('DEBUG', 'ClinetFunction', 'd_from_date= '+d_from_date);
	
	var i_unbilled_type=nlapiGetFieldValue('custpage_unbilled_type');
	//alert('i_unbilled_type====>'+i_unbilled_type);
	nlapiLogExecution('DEBUG', 'ClinetFunction', 'i_unbilled_type= '+i_unbilled_type);
	
	var i_criteria=nlapiGetFieldValue('custpage_criteria_f');
	//alert('i_criteria====>'+i_criteria);
	nlapiLogExecution('DEBUG', 'ClinetFunction', 'i_criteria= '+i_criteria);
	
	var i_customer=nlapiGetFieldValue('custpage_customer_sec');
	//alert('i_customer====>'+i_customer);
	nlapiLogExecution('DEBUG', 'ClinetFunction', 'i_customer= '+i_customer);
	
	var i_month=nlapiGetFieldValue('custpage_d_month');
	//alert('i_month====>'+i_month);
	nlapiLogExecution('DEBUG', 'ClinetFunction', 'i_month= '+i_month);
	
	var i_year=nlapiGetFieldValue('custpage_d_year');
	//alert('i_year====>'+i_year);
	nlapiLogExecution('DEBUG', 'ClinetFunction', 'i_year= '+i_year);
	
	
	if(_logValidation(d_from_date)  &&  _logValidation(d_to_date) &&  _logValidation(o_subsidairy))
	{//if start
	
	     var createExcelURL = nlapiResolveURL("SUITELET", 'customscript_monthly_provision_excel_exp', 'customdeploy1', false);
		
		//pass the internal id of the current record
		createExcelURL += '&d_from_date=' + d_from_date + "&d_to_date=" + d_to_date + "&o_subsidairy=" + parseInt(o_subsidairy) + "&i_unbilled_type=" + i_unbilled_type + "&i_criteria=" + i_criteria + "&i_customer=" + i_customer + "&i_month=" + i_month + "&i_year=" + i_year;
		
		// alert(createPDFURL);
    
		//show the Excel file 
		 newWindow = window.open(createExcelURL);
	
	}//if close

}//fun close
//-------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------
function Show_Journal_Entry()
{
    var d_from_date=nlapiGetFieldValue('custpage_from_date');
	//alert('d_from_date====>'+d_from_date);
	nlapiLogExecution('DEBUG', 'ClinetFunction', 'd_from_date= '+d_from_date);
	
	var d_to_date=nlapiGetFieldValue('custpage_to_date');
	//alert('d_to_date====>'+d_to_date);
	nlapiLogExecution('DEBUG', 'ClinetFunction', 'd_to_date= '+d_to_date);
	
	var o_subsidairy=nlapiGetFieldValue('custpage_user_subsidiary');
	//alert('o_subsidairy====>'+o_subsidairy);
	nlapiLogExecution('DEBUG', 'ClinetFunction', 'd_from_date= '+d_from_date);
	
	var i_unbilled_type=nlapiGetFieldValue('custpage_unbilled_type');
	//alert('i_unbilled_type====>'+i_unbilled_type);
	nlapiLogExecution('DEBUG', 'ClinetFunction', 'i_unbilled_type= '+i_unbilled_type);
	
	var i_criteria=nlapiGetFieldValue('custpage_criteria_f');
	//alert('i_criteria====>'+i_criteria);
	nlapiLogExecution('DEBUG', 'ClinetFunction', 'i_criteria= '+i_criteria);
	
	var i_customer=nlapiGetFieldValue('custpage_customer_sec');
	//alert('i_customer====>'+i_customer);
	nlapiLogExecution('DEBUG', 'ClinetFunction', 'i_customer= '+i_customer);
	
	var i_month=nlapiGetFieldValue('custpage_d_month');
	//alert('i_month====>'+i_month);
	nlapiLogExecution('DEBUG', 'ClinetFunction', 'i_month= '+i_month);
	
	var i_year=nlapiGetFieldValue('custpage_d_year');
	//alert('i_year====>'+i_year);
	nlapiLogExecution('DEBUG', 'ClinetFunction', 'i_year= '+i_year);
	
	
	var createExcelURL = nlapiResolveURL("SUITELET", 'customscript_sut_provision_created_jv_li','customdeploy1', false);
	createExcelURL += '&d_from_date=' + d_from_date + "&d_to_date=" + d_to_date + "&o_subsidairy=" + parseInt(o_subsidairy) + "&i_unbilled_type=" + i_unbilled_type + "&i_criteria=" + i_criteria + "&i_customer=" + i_customer + "&i_month=" + i_month + "&i_year=" + i_year;
    newWindow = window.open(createExcelURL);
}

//-----------------------------------------------------------------------------------


function _logValidation(value) 
{
 if(value != null && value.toString() != null && value != '' && value != undefined && value.toString() != undefined && value != 'undefined' && value.toString() != 'undefined'&& value.toString() != 'NaN' && value != NaN) 
 {
  return true;
 }
 else 
 { 
  return false;
 }
}
// END FUNCTION =====================================================






