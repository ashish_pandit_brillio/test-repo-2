/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       03 Dec 2014     amol.sahijwani
 *
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType
 * 
 * @param {String} type Operation types: create, edit, delete, xedit
 *                      approve, reject, cancel (SO, ER, Time Bill, PO & RMA only)
 *                      pack, ship (IF)
 *                      markcomplete (Call, Task)
 *                      reassign (Case)
 *                      editforecast (Opp, Estimate)
 * @returns {Void}
 */
function userEventBeforeSubmit(type){
	if(type != 'delete')
		{
			
			
			var i_item = nlapiGetFieldValue('item');
			
			
			
			var i_employee_id = nlapiGetFieldValue('employee');
			var i_project_id = nlapiGetFieldValue('customer');
			var d_date	=	nlapiGetFieldValue('trandate');
			nlapiLogExecution('AUDIT','Id: ' + nlapiGetRecordId(),'Item: ' + i_item + ', Employee: ' + i_employee_id + ', Project: ' + i_project_id + ', date: ' + d_date.toString());
			
			if(i_item == '2479' || i_item == '2480')
				return;
			
			if(i_project_id != null)
				{
					// Search for project allocations
					var filters = new Array();
					filters[0]	=	new nlobjSearchFilter('resource', null, 'anyof', i_employee_id);
					filters[1]	=	new nlobjSearchFilter('project', null, 'anyof', i_project_id);
					filters[2]	=	new nlobjSearchFilter('custeventbstartdate', null, 'onorbefore', d_date);
					filters[3]	=	new nlobjSearchFilter('custeventbenddate', null, 'onorafter', d_date);
					filters[4]	=	new nlobjSearchFilter('custeventrbillable', null, 'is', 'T');
					
					try
					{
						var search_results = nlapiSearchRecord('resourceallocation', null, filters, null);
						
						if(search_results != null && search_results.length > 0)
							{
								
								nlapiSubmitField('timebill', nlapiGetRecordId(), 'isbillable', 'T');
								
								nlapiLogExecution('AUDIT','Id: ' + nlapiGetRecordId(),'Billable: True');
							}
						else
							{
								
								nlapiSubmitField('timebill', nlapiGetRecordId(), 'isbillable', 'F');
								
								nlapiLogExecution('AUDIT','Id: ' + nlapiGetRecordId(),'Billable: False');
							}
					}
					catch(e)
					{
						nlapiLogExecution('ERROR', 'Error', e.message);
					}				
				}
		}
}
