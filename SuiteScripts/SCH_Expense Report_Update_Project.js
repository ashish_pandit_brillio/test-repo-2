// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
    /*
       	Script Name : SCH_Expense Report_Update_Project.js
    	Author      : Jayesh Dinde
    	Date        : 4 Sep 2016
    	Description : Expense Report_Update_Project


    	Script Modification Log:

    	-- Date --			-- Modified By --				--Requested By--				-- Description --


    */
}
// END SCRIPT DESCRIPTION BLOCK  ====================================



// BEGIN GLOBAL VARIABLE BLOCK  =====================================
{
    //  Initialize any Global Variables, in particular, debugging variables...




}
// END GLOBAL VARIABLE BLOCK  =======================================


// BEGIN Function =============================================

function scheduled_update_expense() {
	var context = nlapiGetContext();
    try {
        var i_customer = '';
        var s_project_description = '';
        var i_vertical = '';
        var i_territory = '';
        var emp_practice = '';
        var emp_practice_text = '';
        var emp_type = '';
        var person_type = '';
        var onsite_offsite = '';
        var core_practice = '';
        var region_id = '';
        var cust_name = '';
        var s_line;
        var i_practice;
        var i_location;
        var s_employee_name;
        var emp_full_name = '';
        var i_category = '';
        var i_project_service = '';
        var proj_category_val = '';
        var misc_practice = '';
        var project_region = '';
        var parent_practice = '';
		var excel_file_obj = '';	
        var err_row_excel = '';	
        var strVar_excel = '';	
        /*----------Added by Koushalya 28/12/2021---------*/
        var filters_customrecord_revenue_location_subsidiarySearch=["custrecord_offsite_onsite","anyof","2"];
        var obj_rev_loc_subSrch= Search_revenue_Location_subsdidary(filters_customrecord_revenue_location_subsidiarySearch);
        var obj_offsite_onsite=obj_rev_loc_subSrch["Onsite/Offsite"];
         /*-------------------------------------------------*/

        strVar_excel += '<table>';	
        strVar_excel += '	<tr>';	
        strVar_excel += ' <td width="100%">';	
        strVar_excel += '<table width="100%" border="1">';	
        strVar_excel += '	<tr>';	
        strVar_excel += ' <td width="6%" font-size="11" align="center">Transaction Type</td>';	
        strVar_excel += ' <td width="6%" font-size="11" align="center">Transaction Number</td>';	
        strVar_excel += ' <td width="6%" font-size="11" align="center">Transaction Internal ID</td>';	
        strVar_excel += ' <td width="6%" font-size="11" align="center">Error Message</td>';	
        strVar_excel += '	</tr>';
        //customsearch2655
        var a_results_expense = searchRecord('transaction', 'customsearch1548', null, null);
        //var a_results_expense = searchRecord('transaction', 'customsearch2655', null, null);
        if (a_results_expense) {
            for (var counter = 0; counter < a_results_expense.length; counter++) {
                //nlapiLogExecution('audit','i:- '+counter,a_results_expense[counter].getId());
                var o_recordOBJ = nlapiLoadRecord('expensereport', a_results_expense[counter].getId());

                 try {	
                    var transactionNum = o_recordOBJ.getFieldValue('tranid');	
                    var transaction_internal = a_results_expense[counter].getId();
				
				if (_logValidation(o_recordOBJ)) {
                    s_line = 'expense';

                    var i_employee = o_recordOBJ.getFieldValue('entity');

                    if (_logValidation(i_employee)) {
                        var o_employeeOBJ = nlapiLoadRecord('employee', i_employee);

                        if (_logValidation(o_employeeOBJ)) {
                            i_practice = o_employeeOBJ.getFieldValue('department');
                            //nlapiLogExecution('DEBUG', 'afterSubmit_cross_project', ' Practice-->' + i_practice);

                            i_location = o_employeeOBJ.getFieldValue('location');
                            //nlapiLogExecution('DEBUG', 'afterSubmit_cross_project', ' Location-->' + i_location);

                            s_employee_name = o_employeeOBJ.getFieldValue('entityid');
                            //nlapiLogExecution('DEBUG', 'afterSubmit_cross_project', ' Employee Name-->' + s_employee_name);

                            emp_type = o_employeeOBJ.getFieldText('employeetype');

                            person_type = o_employeeOBJ.getFieldText('custentity_persontype');

                            if (!_logValidation(person_type)) {
                                person_type = '';
                            }
                            if (!_logValidation(emp_type)) {
                                emp_type = '';
                            }


                            var emp_subsidiary = o_employeeOBJ.getFieldValue('subsidiary');
                            var emp_frst_name = o_employeeOBJ.getFieldValue('firstname');
                            var emp_middl_name = o_employeeOBJ.getFieldValue('middlename');
                            var emp_lst_name = o_employeeOBJ.getFieldValue('lastname');

                            if (emp_frst_name)
                                emp_full_name = emp_frst_name;

                            if (emp_middl_name)
                                emp_full_name = emp_full_name + ' ' + emp_middl_name;

                            if (emp_lst_name)
                                emp_full_name = emp_full_name + ' ' + emp_lst_name;
                            //
                            emp_practice = o_employeeOBJ.getFieldValue('department');
                            emp_practice_text = o_employeeOBJ.getFieldText('department');
                            if (emp_practice) {
                                var is_practice_active_e = nlapiLookupField('department', parseInt(emp_practice), ['isinactive', 'custrecord_is_delivery_practice']);
                                var isinactive_Practice_e = is_practice_active_e.isinactive;
                                nlapiLogExecution('debug', 'isinactive_Practice_e', isinactive_Practice_e);
                                core_practice = is_practice_active_e.custrecord_is_delivery_practice;
                                nlapiLogExecution('debug', 'core_practice', core_practice);
                            }
                            //
                            if (_logValidation(emp_subsidiary)) {
                                if (emp_subsidiary == 3 && o_employeeOBJ.getFieldValue('custentity_legal_entity_fusion') == 'Brillio Technologies Private Limited UK') {
                                    onsite_offsite = 'Onsite';
                                }
                                else{
                                    onsite_offsite = obj_offsite_onsite[emp_subsidiary]?'Offshore':'Onsite';    
                                }
                                /* else if (emp_subsidiary == 3 || emp_subsidiary == 9 || emp_subsidiary == 12) {
                                    onsite_offsite = 'Offsite';
                                } else {
                                    onsite_offsite = 'Onsite';
                                }*/
                            }
                        }

                    } //Employee	

                    var i_line_count = o_recordOBJ.getLineItemCount(s_line)
                    //nlapiLogExecution('audit','count:- ',i_line_count);
                    if (_logValidation(i_line_count)) {
                        for (var i = 1; i <= i_line_count; i++) {
                            var i_projectID = o_recordOBJ.getLineItemValue(s_line, 'customer', i);
                            //nlapiLogExecution('DEBUG', 'afterSubmit_cross_project', ' Project ID -->' + i_projectID);

                            if (_logValidation(i_projectID)) {
                                var o_projectOBJ = nlapiLoadRecord('job', i_projectID);

                                if (_logValidation(o_projectOBJ)) {
                                    var i_project_name_ID = o_projectOBJ.getFieldValue('entityid');
                                    //nlapiLogExecution('DEBUG', 'afterSubmit_cross_project', ' Project Name ID -->' + i_project_name_ID);

                                    var i_project_name = o_projectOBJ.getFieldValue('companyname');
                                    //nlapiLogExecution('DEBUG', 'afterSubmit_cross_project', ' Project Name -->' + i_project_name);

                                    s_project_description = i_project_name_ID + ' ' + i_project_name;
                                    //nlapiLogExecution('DEBUG', 'afterSubmit_cross_project', ' Project Description -->' + s_project_description);

                                    i_category = o_projectOBJ.getFieldText('custentity_project_allocation_category');
                                    nlapiLogExecution('DEBUG', 'afterSubmit_cross_project', ' Pro Category-->' + i_category);

                                    i_customer = o_projectOBJ.getFieldText('parent');
                                    //nlapiLogExecution('DEBUG', 'afterSubmit_cross_project', ' Customer-->' + i_customer);	
                                    //Added for project region and Practice--
                                    var practice_id = o_projectOBJ.getFieldValue('custentity_practice');
                                    parent_practice = nlapiLookupField('department', practice_id, 'custrecord_parent_practice', true);
                                    project_region = o_projectOBJ.getFieldText('custentity_region');
                                    //
                                    i_vertical = o_projectOBJ.getFieldValue('custentity_vertical');
                                    //nlapiLogExecution('DEBUG', 'afterSubmit_cross_project', ' Vertical-->' + i_vertical);

                                    i_project_service = o_projectOBJ.getFieldValue('custentity_project_services');

                                    var i_customer_id = o_projectOBJ.getFieldValue('parent');
                                    if (_logValidation(i_customer_id)) {
                                        i_territory = nlapiLookupField('customer', i_customer_id, 'territory');
                                        region_id = nlapiLookupField('customer', i_customer_id, 'custentity_region');
                                        cust_name = nlapiLookupField('customer', i_customer_id, 'companyname');
                                    }

                                    var billing_type = o_projectOBJ.getFieldText('jobbillingtype');
                                    //nlapiLogExecution('audit','billing_type:-- ',billing_type);
                                    if (!_logValidation(billing_type)) {
                                        billing_type = '';
                                    }

                                } //Project OBJ
                            } //Project ID				

                            if (!_logValidation(i_customer)) {
                                i_customer = '';
                            }
                            if (!_logValidation(s_project_description)) {
                                s_project_description = '';
                            }
                            if (!_logValidation(i_vertical)) {
                                i_vertical = '';
                            }
                            if (!_logValidation(i_practice)) {
                                i_practice = '';
                            }
                            if (!_logValidation(i_location)) {
                                i_location = '';
                            }
                            //
                            proj_category_val = o_projectOBJ.getFieldValue('custentity_project_allocation_category');
                            //
                            if (proj_category_val) {
                                if ((parseInt(proj_category_val) == parseInt(1)) && (core_practice == 'T')) {
                                    misc_practice = emp_practice;
                                    misc_practice = emp_practice_text;
                                } else {
                                    misc_practice = o_projectOBJ.getFieldValue('custentity_practice');
                                    var is_practice_active = nlapiLookupField('department', parseInt(misc_practice), ['isinactive']);
                                    isinactive_Practice_e = is_practice_active.isinactive;
                                    misc_practice = o_projectOBJ.getFieldText('custentity_practice');
                                }
                            }
                            nlapiLogExecution('audit', 'Pratice', 'misc_practice:' + misc_practice);
                            if (misc_practice && isinactive_Practice_e == 'F') {
                                o_recordOBJ.setLineItemValue(s_line, 'custcol_mis_practice', i, misc_practice);
                            }
							
							
                            //
                            o_recordOBJ.setLineItemValue(s_line, 'custcolprj_name', i, s_project_description);
                            o_recordOBJ.setLineItemValue(s_line, 'custcol_proj_name_on_a_click_report', i, i_project_name);
                            o_recordOBJ.setLineItemValue(s_line, 'custcol_proj_category_on_a_click', i, i_category);
                            o_recordOBJ.setLineItemValue(s_line, 'class', i, i_vertical);
                            o_recordOBJ.setLineItemValue(s_line, 'department', i, i_practice);
                            o_recordOBJ.setLineItemValue(s_line, 'location', i, i_location);
                            o_recordOBJ.setLineItemValue(s_line, 'custcol_employeenamecolumn', i, s_employee_name);
                            o_recordOBJ.setLineItemValue(s_line, 'custcol_territory', i, i_territory);
                            o_recordOBJ.setLineItemValue(s_line, 'custcol_onsite_offsite', i, onsite_offsite);
                            o_recordOBJ.setLineItemValue(s_line, 'custcol_billing_type', i, billing_type);
                            o_recordOBJ.setLineItemValue(s_line, 'custcol_employee_type', i, emp_type);
                            o_recordOBJ.setLineItemValue(s_line, 'custcol_person_type', i, person_type);
                            o_recordOBJ.setLineItemValue(s_line, 'custcol_transaction_project_services', i, i_project_service);
                            o_recordOBJ.setLineItemValue(s_line, 'custcol_parent_executing_practice', i, parent_practice);
                            o_recordOBJ.setLineItemValue(s_line, 'custcol_project_region', i, project_region);

                            if (!_logValidation(i_project_name_ID)) {
                                i_project_name_ID = '';
                            }

                            o_recordOBJ.setLineItemValue(s_line, 'custcol_project_entity_id', i, i_project_name_ID);
                            var cust_entity_id = '';
                            if (_logValidation(i_customer)) {
                                cust_entity_id = i_customer.split(' ');
                                cust_entity_id = cust_entity_id[0];
                            }
                            o_recordOBJ.setLineItemValue(s_line, 'custcolcustcol_temp_customer', i, i_customer);
                            o_recordOBJ.setLineItemValue(s_line, 'custcol_cust_name_on_a_click_report', i, cust_name);
                            o_recordOBJ.setLineItemValue(s_line, 'custcol_region_master_setup', i, region_id);
                            o_recordOBJ.setLineItemValue(s_line, 'custcol_customer_entityid', i, cust_entity_id);
                            var s_employee_name_split = s_employee_name.split('-');
                            s_employee_name_split = s_employee_name_split[0];
                            o_recordOBJ.setLineItemValue(s_line, 'custcol_employee_entity_id', i, s_employee_name_split);
                            o_recordOBJ.setLineItemValue(s_line, 'custcol_emp_name_on_a_click_report', i, emp_full_name);

                        } //Loop				
                    } //Line Count
					
                    o_recordOBJ.setFieldValue('custbody_is_je_updated_for_emp_type', 'T');
                    // nlapiSetFieldValue('custbody_is_je_updated_for_emp_type','F');
                    var i_submitID = nlapiSubmitRecord(o_recordOBJ, true, true);
                    nlapiLogExecution('DEBUG', 'afterSubmit_cross_project', ' ***************** Submit ID ****************-->' + i_submitID);

                } //Record OBJ	

                var context = nlapiGetContext();

                var usageEnd = context.getRemainingUsage();
                if (usageEnd < 2000) {
                    yieldScript(context);
                    //break;
                }
				} catch (err) {	
                    //Added try-catch logic by Sitaram 06/08/2021	
                    nlapiLogExecution('audit', 'err:-- ', err);	
                    err_row_excel += '	<tr>';	
                    err_row_excel += ' <td width="6%" font-size="11" align="center">' + 'expensereport' + '</td>';	
                    err_row_excel += ' <td width="6%" font-size="11" align="center">' + transactionNum + '</td>';	
                    err_row_excel += ' <td width="6%" font-size="11" align="center">' + transaction_internal + '</td>';	
                    err_row_excel += ' <td width="6%" font-size="11" align="center">' + err.message + '</td>';	
                    err_row_excel += '	</tr>';	
                    //continue;	
                }

            } //for loop
			 if (_logValidation(err_row_excel)) {	
                var tailMail = '';	
                tailMail += '</table>';	
                tailMail += ' </td>';	
                tailMail += '</tr>';	
                tailMail += '</table>';	
                strVar_excel = strVar_excel + err_row_excel + tailMail	
                //excel_file_obj = generate_excel(strVar_excel);	
                var mailTemplate = "";	
                mailTemplate += '<html>';	
                mailTemplate += '<body>';	
                mailTemplate += "<p> This is to inform that following transactions are having Errors and therefore, system is not able to set the project/MIS mandatory fields</p>";	
                mailTemplate += "<p> Please make sure, transaction is updated with proper details else may not able to see the project/customer/employee details at Income Statement level.</p>";	
                mailTemplate += "<p><b> Script Id:- " + context.getScriptId() + "</b></p>";	
                mailTemplate += "<p><b> Script Deployment Id:- " + context.getDeploymentId() + "</b></p>";	
                mailTemplate += "<br/>"	
                mailTemplate += "<br/>"	
                mailTemplate += strVar_excel	
                mailTemplate += "<br/>"	
                mailTemplate += "<p>Regards, <br/> Information Systems</p>";	
                mailTemplate += '</body>';	
                mailTemplate += '</html>';	
                nlapiSendEmail(442, 'billing@brillio.com', 'Issue with updating the MIS data for the transactions', mailTemplate, 'netsuite.support@brillio.com', null, null, null);	
            }
        }
    } catch (exception) {
        nlapiLogExecution('DEBUG', 'ERROR', '  Exception Caught -->' + exception);
    }
}

// END Function ===============================================


// BEGIN FUNCTION ===================================================

function yieldScript(currentContext) {

    nlapiLogExecution('AUDIT', 'API Limit Exceeded');
    var state = nlapiYieldScript();

    if (state.status == "FAILURE") {
        nlapiLogExecution('ERROR', 'Yield Script Failed', 'Reason : ' +
            state.reason + ' / Size : ' + state.size);
        return false;
    } else if (state.status == "RESUME") {
        nlapiLogExecution('AUDIT', 'Script Resumed');
    }
}

function _logValidation(value) {
    if (value != null && value.toString() != null && value != '' && value != undefined && value.toString() != undefined && value != 'undefined' && value.toString() != 'undefined' && value.toString() != 'NaN' && value != NaN) {
        return true;
    } else {
        return false;
    }
}

function searchRecord(recordType, savedSearch, arrFilters, arrColumns,
    filterExpression) {

    try {
        var search = null;

        // if a saved search is provided, load it and add the filters and
        // columns
        if (isNotEmpty(savedSearch)) {
            search = nlapiLoadSearch(recordType, savedSearch);

            if (isArrayNotEmpty(arrFilters)) {
                search.addFilters(arrFilters);
            }

            if (isArrayNotEmpty(arrColumns)) {
                search.addColumns(arrColumns);
            }

            if (isArrayNotEmpty(filterExpression)) {
                search.setFilterExpression(filterExpression);
            }
        }
        // create a new search
        else {
            search = nlapiCreateSearch(recordType, arrFilters, arrColumns);
        }

        // run search
        var resultSet = search.runSearch();

        // iterate through the search and get all data 1000 at a time
        var searchResultCount = 0;
        var resultSlice = null;
        var searchResult = [];

        do {
            resultSlice = resultSet.getResults(searchResultCount,
                searchResultCount + 1000);

            if (resultSlice) {

                resultSlice.forEach(function(result) {

                    searchResult.push(result);
                    searchResultCount++;
                });
            }
        } while (isArrayNotEmpty(resultSlice) && resultSlice.length >= 1000);

        return searchResult;
    } catch (err) {
        nlapiLogExecution('ERROR', 'searchRecord', err);
        throw err;
    }
}

function isEmpty(value) {

    return value == null || value == "" || typeof(value) == undefined;
}

function isNotEmpty(value) {

    return !isEmpty(value);
}

function isArrayEmpty(argArray) {

    return !isArrayNotEmpty(argArray);
}

function isArrayNotEmpty(argArray) {

    return (isNotEmpty(argArray) && argArray.length > 0);
}

// END FUNCTION =====================================================