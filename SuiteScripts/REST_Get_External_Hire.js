/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       06 Mar 2019     Aazamali Khan
 *
 */

/**
 * @param {Object} dataIn Parameter object
 * @returns {Object} Output object
 */
function getRESTlet(dataIn) {
	var body = dataIn;
	//var jsonBody = JSON.parse(body);
	var n_frf = body.frf;
	nlapiLogExecution('ERROR','n_frf',n_frf);
	var dataOut = GetExternalHireDetails(n_frf);
	nlapiLogExecution('ERROR','dataOut',dataOut);
	return {data : dataOut};
}
function GetExternalHireDetails(frf) {
	var searchResult = nlapiSearchRecord("customrecord_taleo_external_hire",null,
			[
			 ["custrecord_taleo_ext_hire_frf_details","anyof",frf],"AND",
      	     ["custrecord_taleo_ext_hire_frf_details.custrecord_frf_details_status","is","F"]
			 ], 
			 [
			  new nlobjSearchColumn("custrecord_taleo_screening_stage"), 
			  new nlobjSearchColumn("custrecord_taleo_external_stage_cleared"), 
			  new nlobjSearchColumn("custrecord_taleo_inter_stg_first_level_c"), 
			  new nlobjSearchColumn("custrecord_interview_stage_second_lvl_c"), 
			  new nlobjSearchColumn("custrecord_taleo_interview_conducted"), 
			  new nlobjSearchColumn("custrecord_taleo_ext_back_check"), 
			  new nlobjSearchColumn("custrecord_taleo_offer_made_conducted"), 
			  new nlobjSearchColumn("custrecord_taleo_offer_accepted"), 
			  new nlobjSearchColumn("custrecord_taleo_inter_stg_first_lvl_cl"), 
			  new nlobjSearchColumn("custrecord_interview_stage_second_lvl_cl"), 
			  new nlobjSearchColumn("custrecord_taleo_customer_inter_clear"), 
			  new nlobjSearchColumn("custrecord_taleo_back_check_clear"), 
			  new nlobjSearchColumn("custrecord_taleo_offer_made_clear"), 
			  new nlobjSearchColumn("custrecord_taleo_ext_hire_frf_details"), 
			  new nlobjSearchColumn("custrecord_interviewer_statge_first"), 
			  new nlobjSearchColumn("custrecord_interviewer_second_stage"), 
			  new nlobjSearchColumn("custrecord_offer_made_email"),
			  new nlobjSearchColumn("custrecord_customer_interviewer")
			  ]
	);
	var dataOut = new Array();
	var jsonObj1 = {};
	if (searchResult) {
		jsonObj1 = {"mode":"external"};
		dataOut.push(jsonObj1);
		var childId = searchResult[0].getId();
        var jsonObj = {};
		jsonObj.screeningstage = {"conducted":searchResult[0].getValue("custrecord_taleo_screening_stage"),
				"cleared":searchResult[0].getValue("custrecord_taleo_external_stage_cleared"),"email":"NA","total":parseInt(searchResult[0].getValue("custrecord_taleo_screening_stage"))+parseInt(searchResult[0].getValue("custrecord_taleo_external_stage_cleared"))};
		jsonObj.Interviewfirst = {"conducted":searchResult[0].getValue("custrecord_taleo_inter_stg_first_level_c"),
				"cleared":searchResult[0].getValue("custrecord_taleo_inter_stg_first_lvl_cl"),
				"email":searchResult[0].getValue("custrecord_interviewer_statge_first"),
				"total":parseInt(searchResult[0].getValue("custrecord_taleo_inter_stg_first_level_c"))+parseInt(searchResult[0].getValue("custrecord_taleo_inter_stg_first_lvl_cl"))};
		jsonObj.InterviewSecond ={"conducted":searchResult[0].getValue("custrecord_interview_stage_second_lvl_c"),
				"cleared":searchResult[0].getValue("custrecord_interview_stage_second_lvl_cl"),
				"email":searchResult[0].getValue("custrecord_interviewer_second_stage"),
				"total":parseInt(searchResult[0].getValue("custrecord_interview_stage_second_lvl_c"))+parseInt(searchResult[0].getValue("custrecord_interview_stage_second_lvl_cl"))};
		jsonObj.customerInterview = {"conducted":searchResult[0].getValue("custrecord_taleo_interview_conducted"),
				"cleared":searchResult[0].getValue("custrecord_taleo_customer_inter_clear"),
				"email":searchResult[0].getValue("custrecord_customer_interviewer"),
				"total":parseInt(searchResult[0].getValue("custrecord_taleo_interview_conducted"))+parseInt(searchResult[0].getValue("custrecord_taleo_customer_inter_clear"))};
		jsonObj.offermade = {"conducted":"NA",
				"cleared":searchResult[0].getValue("custrecord_taleo_offer_made_conducted"),
				"email":searchResult[0].getValue("custrecord_customer_interviewer"),
				"total":searchResult[0].getValue("custrecord_taleo_offer_made_conducted")};
		jsonObj.offeraccepted = {"conducted":"NA",
				"cleared":searchResult[0].getValue("custrecord_taleo_offer_accepted"),
				"email":searchResult[0].getValue("custrecord_offer_made_email"),
				"total":searchResult[0].getValue("custrecord_taleo_offer_accepted")};
		var searchResultChild = GetChildRecords(childId);
		var result = [];
		for (var int = 0; int < searchResultChild.length; int++) {
			result.push({"candidate":searchResultChild[int].getValue("custrecord_tale_res_selected_can"),"CurrentOrganisation":searchResultChild[int].getValue("custrecord_tale_res_curr_org"),"OfferDate":searchResultChild[int].getValue("custrecord_taleo_res_offer_date"),"JoiningDate":searchResultChild[int].getValue("custrecord_taleo_res_joining_date"),"email":searchResultChild[int].getValue("custrecord_taleo_res_email")})	
		}
		jsonObj.selectedcandidate = result;
		dataOut.push(jsonObj);
	
	}else{
		var frfFlag = nlapiLookupField("customrecord_frf_details",frf,"custrecord_frf_details_external_hire");
		if(frfFlag = "T"){
			jsonObj1.mode = {"mode":"external"};
		}else{
			jsonObj1.mode = {"mode":"internal"};
		}
		
		dataOut.push(jsonObj1);
	}
	return dataOut;
}
function GetChildRecords(childId) {
	var searchResult = nlapiSearchRecord("customrecord_taleo_resources",null,
			[
			   ["custrecord_taleo_res_external_hire_fr","anyof",childId]
			], 
			[
			   new nlobjSearchColumn("scriptid").setSort(false), 
			   new nlobjSearchColumn("custrecord_taleo_res_external_hire_fr"), 
			   new nlobjSearchColumn("custrecord_tale_res_selected_can"), 
			   new nlobjSearchColumn("custrecord_tale_res_curr_org"), 
			   new nlobjSearchColumn("custrecord_taleo_res_offer_date"), 
			   new nlobjSearchColumn("custrecord_taleo_res_joining_date"),
			   new nlobjSearchColumn("custrecord_taleo_res_email")
			]
			);
	return searchResult;
}