/**
 * Screen to display all the provision entries grouped as they will be added in
 * the JE
 * 
 * Version Date Author Remarks 1.00 22 Apr 2016 Nitish Mishra
 * 
 */

function suitelet(request, response) {
	try {
		var provisionMasterId = request.getParameter('master');

		if (!provisionMasterId) {
			provisionMasterId = getRunningProvisionMasterId();
		}

		if (provisionMasterId) {
			var provisionMaster = nlapiLoadRecord(
			        'customrecord_provision_master', provisionMasterId);
			var tranDate = nlapiDateToString(new Date(), 'date');
			var arrExchangeRate = getExchangeRate(tranDate, provisionMaster
			        .getFieldText('custrecord_pm_currency'));

			if (request.getMethod() == "GET") {
				var mode = request.getParameter('mode');
				var module = request.getParameter('module');
				if (mode == "CSV") {
					generateProvisionCsv(provisionMasterId, provisionMaster,
					        arrExchangeRate, module);
				} else {
					generateProvisionScreen(provisionMasterId, provisionMaster,
					        arrExchangeRate);
				}
			} else {
				redirectToCreateJeProgress();
			}
		} else {
			throw "No Provision Active";
		}
	} catch (err) {
		nlapiLogExecution('ERROR', 'suitelet', err);
		displayErrorForm(err, "Monthly Provision - Provisioned Entries");
	}
}

function generateProvisionCsv(provisionMasterId, provisionMasterRec,
        arrExchangeRate, module)
{
	try {
		var form = nlapiCreateForm('Monthly Provision - Provisioned Entries');
		var file = null;

		switch (module) {
			// Hourly Approved
			case "HA":
				var file = generateCsvFile(getHourlyApprovedData(
				        provisionMasterId, arrExchangeRate),
				        'Hourly - Approved', 'Bill Rate', 'Hours');
				response.setContentType('CSV', 'Hourly - Approved.csv');
			break;

			// Hourly Submitted
			case "HS":
				var file = generateCsvFile(getHourlySubmittedData(
				        provisionMasterId, arrExchangeRate),
				        'Hourly - Submitted, Not Approved', 'Bill Rate',
				        'Hours');
				response.setContentType('CSV',
				        'Hourly - Submitted, Not Approved.csv');
			break;

			// Hourly Not Submitted
			case "HN":
				var file = generateCsvFile(getHourlyNotSubmittedData(
				        provisionMasterId, arrExchangeRate),
				        'Hourly - Not Submitted', 'Bill Rate', 'Hours');
				response.setContentType('CSV', 'Hourly - Not Submitted.csv');
			break;

			// Monthly Approved
			case "MA":
				var file = generateCsvFile(getMonthlyApprovedData(
				        provisionMasterId, arrExchangeRate),
				        'Monthly - Approved', 'Daily Rate', 'Days', true);
				response.setContentType('CSV', 'Monthly - Approved.csv');
			break;

			// Monthly Submitted
			case "MS":
				var file = generateCsvFile(getMonthlySubmittedData(
				        provisionMasterId, arrExchangeRate),
				        'Monthly - Submitted, Not Approved', 'Daily Rate',
				        'Days', true);
				response.setContentType('CSV',
				        'Monthly - Submitted, Not Approved.csv');
			break;

			// Monthly Not Submitted
			case "MN":
				var file = generateCsvFile(getMonthlyNotSubmittedData(
				        provisionMasterId, arrExchangeRate),
				        'Monthly - Not Submitted', 'Daily Rate', 'Days', true);
				response.setContentType('CSV', 'Monthly - Not Submitted.csv');
			break;
		}

		if (file) {
			response.write(file.getValue());
		} else {
			response.writeLine("Incorrect Module");
		}
	} catch (err) {
		nlapiLogExecution('ERROR', 'generateProvisionCsv', err);
		throw err;
	}
}

function generateCsvFile(data, fileName, unitCostLabel, quantityLabel,
        isMonthly)
{

	var header = "From Date,To Date,Employee,Customer,Project,Vertical,Practice,Location,"
	        + quantityLabel + "," + unitCostLabel;

	if (isMonthly) {
		header += ",Monthly Rate";
	}
	header += ",Amount,Currency,Foreign Amount,Foreign Currency\r\n";

	var csvText = "";
	data.forEach(function(line) {
		csvText += line.fromdate + ",";
		csvText += line.todate + ",";
		csvText += line.employee + ",";
		csvText += line.customer + ",";
		csvText += line.project + ",";
		csvText += line.vertical + ",";
		csvText += line.practice + ",";
		csvText += line.location + ",";
		csvText += line.hours + ",";
		csvText += line.billrate + ",";

		if (isMonthly) {
			csvText += line.monthlyrate + ",";
		}
		csvText += line.amountconverted + ",";
		csvText += line.newcurrency + ",";
		csvText += line.amount + ",";
		csvText += line.currency + ",";
		csvText += "\r\n";
	});

	var file = nlapiCreateFile(fileName + ".csv", "CSV", header + csvText);
	return file;
}

function generateProvisionScreen(provisionMasterId, provisionMasterRec,
        arrExchangeRate)
{
	try {
		var form = nlapiCreateForm('Provisioned Entries');

		// Hourly
		form.addTab('custpage_hourly', 'Hourly');

		// Hourly Approved
		var list = addSubTab(form, 'custpage_hourly_approved', 'Approved',
		        'custpage_hourly', 'Bill Rate', 'Hours');
		list.setLineItemValues(getHourlyApprovedData(provisionMasterId,
		        arrExchangeRate));

		// Hourly Submitted
		var list = addSubTab(form, 'custpage_hourly_submitted',
		        'Submitted, Not Approved', 'custpage_hourly', 'Bill Rate',
		        'Hours');
		list.setLineItemValues(getHourlySubmittedData(provisionMasterId,
		        arrExchangeRate));

		// Hourly Not Submitted
		var list = addSubTab(form, 'custpage_hourly_notsubmitted',
		        'Not Submitted', 'custpage_hourly', 'Bill Rate', 'Hours');
		list.setLineItemValues(getHourlyNotSubmittedData(provisionMasterId,
		        arrExchangeRate));

		// Monthly
		form.addTab('custpage_monthly', 'Monthly');

		// Monthly Approved
		var list = addSubTab(form, 'custpage_monthly_approved', 'Approved',
		        'custpage_monthly', 'Daily Rate', 'Days', true);
		list.setLineItemValues(getMonthlyApprovedData(provisionMasterId,
		        arrExchangeRate));

		// Monthly Submitted
		var list = addSubTab(form, 'custpage_monthly_submitted',
		        'Submitted, Not Approved', 'custpage_monthly', 'Daily Rate',
		        'Days', true);
		list.setLineItemValues(getMonthlySubmittedData(provisionMasterId,
		        arrExchangeRate));

		// Monthly Not Submitted
		var list = addSubTab(form, 'custpage_monthly_notsubmitted',
		        'Not Submitted', 'custpage_monthly', 'Daily Rate', 'Days', true);
		list.setLineItemValues(getMonthlyNotSubmittedData(provisionMasterId,
		        arrExchangeRate));

		form.addTab("custpage_files", "Files");
		var list = form.addSubList("custpage_files_list", "staticlist",
		        "Files", "custpage_files");
		list.addField("url", "text", "File");
		list.setLineItemValues(generateFileLinks(provisionMasterId));

		// create a sublist and pull all the error logs from the custom
		// table related to this master provision record
		form.addTab("custpage_error", "Error Logs");
		var errorList = form.addSubList('custpage_error_logs', 'staticlist',
		        'Error Logs', 'custpage_error');
		var errorLogs = getErrorLogs(provisionMasterId);
		errorList.addField('comments', 'text', 'Comments');
		errorList.setLineItemValues(errorLogs);

		if (provisionMasterRec
		        .getFieldValue('custrecord_pm_validation_completed') == 'T'
		        && provisionMasterRec
		                .getFieldValue('custrecord_pm_provision_creation_done') == 'T'
		        && provisionMasterRec
		                .getFieldValue('custrecord_pm_je_creation_done') == 'F') {

			if (errorLogs.length == 0) {
				form.addSubmitButton("Create JE");
			}
		}

		response.writePage(form);
	} catch (err) {
		nlapiLogExecution('ERROR', 'generateProvisionScreen', err);
		throw err;
	}
}

function getErrorLogs(masterProvisionId) {
	try {
		var logSearch = searchRecord(
		        'customrecord_provision_error_log',
		        null,
		        [
		                new nlobjSearchFilter('isinactive', null, 'is', 'F'),
		                new nlobjSearchFilter(
		                        'custrecord_pel_provision_master', null,
		                        'anyof', masterProvisionId) ],
		        [
		                new nlobjSearchColumn('custrecord_pel_error_comment'),
		                new nlobjSearchColumn('custrecord_pel_provision_master') ]);

		var logList = [];
		if (logSearch) {

			for (var i = 0; i < logSearch.length; i++) {
				logList.push({
					comments : logSearch[i]
					        .getValue('custrecord_pel_error_comment')
				});
			}
		}

		return logList;
	} catch (err) {
		nlapiLogExecution('ERROR', 'getErrorLogs', err);
		throw err;
	}
}

function addSubTab(form, subTabId, label, masterTabId, unitCostLabel,
        quantityLabel, isMonthly)
{
	var list = form.addSubList(subTabId, 'list', label, masterTabId);
	list.addField('fromdate', 'text', 'From Date');
	list.addField('todate', 'text', 'To Date');
	list.addField('employee', 'text', 'Employee');
	list.addField('customer', 'text', 'Customer');
	list.addField('project', 'text', 'Project');
	list.addField('vertical', 'text', 'Vertical');
	list.addField('practice', 'text', 'Practice');
	list.addField('location', 'text', 'Location');
	list.addField('hours', 'text', quantityLabel);
	list.addField('billrate', 'text', unitCostLabel);

	if (isMonthly) {
		list.addField('monthlyrate', 'text', 'Monthly Rate');
	}
	list.addField('amountconverted', 'text', 'Amount');
	list.addField('newcurrency', 'text', 'Currency');
	list.addField('amount', 'text', 'Foreign Amount');
	list.addField('currency', 'text', 'Currency');
	return list;
}

function generateFileLinks(provisionMasterId) {
	try {
		var context = nlapiGetContext();
		var url = nlapiResolveURL("SUITELET", context.getScriptId(), context
		        .getDeploymentId());
		url += '&master=' + provisionMasterId;
		url += '&mode=CSV&module=';

		var arrLinks = [];
		arrLinks.push(generateFileDownloadUrl(url, "Hourly - Approved", "HA"));
		arrLinks.push(generateFileDownloadUrl(url,
		        "Hourly - Submitted, Not Approved", "HS"));
		arrLinks.push(generateFileDownloadUrl(url, "Hourly - Not Submitted",
		        "HN"));
		arrLinks.push(generateFileDownloadUrl(url, "Monthly - Approved", "MA"));
		arrLinks.push(generateFileDownloadUrl(url,
		        "Monthly - Submitted, Not Approved", "MS"));
		arrLinks.push(generateFileDownloadUrl(url, "Monthly - Not Submitted",
		        "MN"));

		return arrLinks;
	} catch (err) {
		nlapiLogExecution('ERROR', 'generateFileLinks', err);
		throw err;
	}
}

function generateFileDownloadUrl(primaryUrl, label, module) {
	return {
		url : "<a href='" + primaryUrl + module + "' target='_blank'>" + label
		        + "</a>"
	};
}

function redirectToCreateJeProgress() {
	try {
		var masterProvisionId = request.getParameter('custpage_master_id');

		// check if the provision entry creation script is not already running

		// if not running, call the schedule script
		nlapiScheduleScript('customscript_sch_mp_create_je',
		        'customdeploy_sch_mp_create_je');

		// redirect to provision entry creation progress screen
		response.sendRedirect('SUITELET',
		        'customscript_sut_mp_process_progress',
		        'customdeploy_sut_mp_je_creation_progress');
	} catch (err) {
		nlapiLogExecution('ERROR', 'redirectToCreateJeProgress', err);
		throw err;
	}
}