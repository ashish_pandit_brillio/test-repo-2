/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       14 Jun 2019     Aazamali Khan
 *
 */

/**
 * @param {Object} dataIn Parameter object
 * @returns {Object} Output object
 */
function postRESTlet(dataIn) {
    var data = dataIn;
    if (data) {
        for (var i = 0; i < data.length; i++) {
            var dataElement = data[i];
            var requestionNumber = dataElement.RequisitionNumber;
            // Find External Hire Record for requisition ID.
            var rrfRecId = getRequisitionNumber(requestionNumber);
            if (rrfRecId) {
                var taleoRecId = nlapiLookupField("customrecord_frf_details", rrfRecId, "custrecord_frf_details_rrf_number");
                var taleoRecObj = nlapiLoadRecord("customrecord_taleo_external_hire", taleoRecId);
                taleoRecObj.setFieldValue("custrecord_taleo_screening_stage", dataElement.ScreeningTotal);
                taleoRecObj.setFieldValue("custrecord_taleo_external_hire_conducted", dataElement.ScreeningConducted);
                taleoRecObj.setFieldValue("custrecord_taleo_interview_conducted", dataElement.ClientInterviewConducted);
                taleoRecObj.setFieldValue("custrecord_taleo_customer_inter_clear", dataElement.ClientInterviewCleared);
                taleoRecObj.setFieldValue("custrecord_taleo_external_selected_can", dataElement.CandidateName);
                taleoRecObj.setFieldValue("custrecord_taleo_ext_hr_round_eligible", dataElement.HRRoundEligible);
				taleoRecObj.setFieldValue("custrecord_taleo_extenal_hr_round", dataElement.HRRoundCleared);
				taleoRecObj.setFieldValue("custrecord_taleo_ext_hr_round_conducted", dataElement.HRRoundConducted);
                taleoRecObj.setFieldValue("custrecord_tal_offer_accepted_cleared", dataElement.OfferAccepted);
                taleoRecObj.setFieldValue("custrecord_taleo_external_offer_rejected", dataElement.OfferRefused);
				taleoRecObj.setFieldValue("custrecord_taleo_ext_hire_status", dataElement.RequisitionStatus);
				taleoRecObj.setFieldValue("custrecord_taleo_inter_stg_first_level_c", dataElement.Tech1Eligible);
				taleoRecObj.setFieldValue("custrecord_taleo_inter_stg_first_lvl_cl", dataElement.Tech1Cleared);
				taleoRecObj.setFieldValue("custrecord_taleo_ext_hire_tech1_cond", dataElement.Tech1Conducted);
				taleoRecObj.setFieldValue("custrecord_interview_stage_second_lvl_c", dataElement.Tech2Eligible);
				taleoRecObj.setFieldValue("custrecord_interview_stage_second_lvl_cl", dataElement.Tech2Cleared);
				taleoRecObj.setFieldValue("custrecord_taleo_ext_tech2_conducted", dataElement.Tech2Conducted);
				taleoRecObj.setFieldValue("custrecord_taleo_ext_offer_released", dataElement.OfferReleased);
				taleoRecObj.setFieldValue("custrecord_taleo_ext_cust_interview_eli", dataElement.ClientInterviewEligible);
				
                nlapiSubmitRecord(taleoRecObj, false, true);
				
            }/*  else {
                return {
                    "code": "error",
                    "message": "requisition number is not found!"
                }
            } */
        }
    }
    return {
		"code" : "success",
		"message" : "Requisition updated!" 
	};
}

function getRequisitionNumber(rrfNumber) {
    // to do
    var customrecord_frf_detailsSearch = nlapiSearchRecord("customrecord_frf_details", null,
        [
            ["custrecord_frf_details_ns_rrf_number", "is", rrfNumber]
        ],
        []
    );
    if (customrecord_frf_detailsSearch) {
        return customrecord_frf_detailsSearch[0].getId();
    } else {
        return null;
    }
}