function scheduled(type) {
    nlapiLogExecution('DEBUG', 'START');
    try {
        var context = nlapiGetContext();
        var provisionMasterId = getRunningProvisionMasterId();

        // search all the entries related to the master
        var provisionEntrySearch = searchRecord('customrecord_provision_entry', null, [
            new nlobjSearchFilter('custrecord_pe_provision_master', null, 'anyof', provisionMasterId)
        ]);
	
        if (provisionEntrySearch) {
            var count = provisionEntrySearch.length;
			 nlapiLogExecution('DEBUG', 'count', count);
          
            for (var i = 0; i < count ; i++) {
                var provisionEntry = nlapiDeleteRecord('customrecord_provision_entry', provisionEntrySearch[i].getId());
              //  context.setPercentComplete(i * 100 / count);
             //   yieldScript(context);
            }
        }
    }
    catch (err) {
        nlapiLogExecution('ERROR', 'scheduled', err);
    }

    nlapiLogExecution('DEBUG', 'END');
}

function yieldScript(currentContext) {

    if (currentContext.getRemainingUsage() <= 100) {
        nlapiLogExecution('AUDIT', 'API Limit Exceeded');
        var state = nlapiYieldScript();

        if (state.status == "FAILURE") {
            nlapiLogExecution('ERROR', 'Yield Script Failed', 'Reason : '
			        + state.reason + ' / Size : ' + state.size);
            return false;
        } else if (state.status == "RESUME") {
            nlapiLogExecution('AUDIT', 'Script Resumed');
        }
    }
}

function getRunningProvisionMasterId() {
    try {
        // get the currently active monthly provision
        var provisionMasterSearch = nlapiSearchRecord('customrecord_provision_master', null, [
            new nlobjSearchFilter('isinactive', null, 'is', 'F'),
            new nlobjSearchFilter('custrecord_pm_is_currently_active', null, 'is', 'T')
        ]);

        var provisionMasterId = null;
        if (provisionMasterSearch) {
            provisionMasterId = provisionMasterSearch[0].getId();
        }

        return provisionMasterId;
    }
    catch (err) {
        nlapiLogExecution('ERROR', 'getRunningProvisionMasterId', err);
        throw err;
    }
}