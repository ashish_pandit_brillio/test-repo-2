// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
/*
   	Script Name:SUT Search Quarterly return detail record
	Author:Nikhil jain
	Company:Aashnacloudtech Pvt. Ltd
	Date:	3 Aprial 2015
	Description:Search the quarterly detail return record.


	Script Modification Log:

	-- Date --			-- Modified By --				--Requested By--				-- Description --



Below is a summary of the process controls enforced by this script file.  The control logic is described
more fully, below, in the appropriate function headers and code blocks.


     SUITELET
		- suiteletFunction(request, response)


     SUB-FUNCTIONS
		- The following sub-functions are called by the above core functions in order to maintain code
            modularization:

               - NOT USED

*/
}
// END SCRIPT DESCRIPTION BLOCK  ====================================



// BEGIN GLOBAL VARIABLE BLOCK  =====================================
{
	//  Initialize any Global Variables, in particular, debugging variables...




}
// END GLOBAL VARIABLE BLOCK  =======================================





// BEGIN SUITELET ==================================================

function suiteletFunction_search_quarterly_record(request, response)
{

	/*  Suitelet:

          - EXPLAIN THE PURPOSE OF THIS FUNCTION

		-

		FIELDS USED:

          --Field Name--				--ID--


	*/


	//  LOCAL VARIABLES


    //  SUITELET CODE BODY
	
		if (request.getMethod() == 'GET') 
		{
			var form = nlapiCreateForm("Search Quarterly eTDS Return");
			
			form.setScript('customscript_cli_search_qty_return');
			
			var year = form.addField('custpage_year', 'select', 'Financial Year','customrecord_financial_year');
			year.setMandatory(true)
			
			var quarter = form.addField('custpage_quarter', 'select', 'Quarter', 'customlist_quater');
       		quarter.setDisplayType('entry');
			quarter.setMandatory(true)
				
			var context = nlapiGetContext();
			var i_subcontext = context.getFeature('SUBSIDIARIES');
		
			if (i_subcontext != false) 
			{
				var i_AitGlobalRecId =  SearchGlobalParameter();
			
	
				if(i_AitGlobalRecId != 0 )
			{
				var o_AitGloRec = nlapiLoadRecord('customrecord_indian_tax_glo_parameter',i_AitGlobalRecId);
				
				var a_subisidiary = new Array();
				
				a_subisidiary = o_AitGloRec.getFieldValues('custrecord_ait_subsidiary');
				
		
			}
			var subsidiary = form.addField('subsidiary', 'select', 'Subsidiary');
			subsidiary.addSelectOption('', '');
        	popluatsubsidiaries(request, response, subsidiary,a_subisidiary);
			subsidiary.setMandatory(true)
			
			form.addSubmitButton('Search');
			
			response.writePage(form);
		}
	
		}
		else
        if (request.getMethod() == 'POST')
		{
           var internal_id =  search_quartely_detailrecord(request,response);
		   
		   if(internal_id != null && internal_id != '' && internal_id != undefined)
		   {
		   		nlapiSetRedirectURL('RECORD', 'customrecord_quarterly_return_details',internal_id , false);
		   }
		   else
		   {
		   	    nlapiSetRedirectURL('SUITELET', 'customscript_sut_search_quarterly_detail', '1', false);
		   }
		
        }
		

}

// END SUITELET ====================================================




// BEGIN OBJECT CALLED/INVOKING FUNCTION ===================================================

function SearchGlobalParameter()
{
	var a_filters = new Array();
	var a_column = new Array();
	var i_globalRecId = 0;
	
    a_column.push(new nlobjSearchColumn('internalid'));
	
	var s_serchResult = nlapiSearchRecord('customrecord_indian_tax_glo_parameter',null,null,a_column)
	
	if(s_serchResult != null && s_serchResult != undefined && s_serchResult != '')
	{
		for (var i=0; i<s_serchResult.length && s_serchResult != null; i++) 	
		{
			i_globalRecId = s_serchResult[0].getValue('internalid');
			nlapiLogExecution('DEBUG','Bill ', "i_globalRecId"+i_globalRecId);
		
		}
		
	}
	return i_globalRecId;
}
function popluatsubsidiaries(request, response, taxagency,subsidiary_id)
	{
	   // var subsidiary = request.getParameter('subsidiary')
	    var ved_filchk1 = new Array();
	    var ved_columnsCheck1 = new Array();

	   ved_filchk1.push( new nlobjSearchFilter('internalid', null, 'anyof', subsidiary_id));

	    ved_columnsCheck1.push( new nlobjSearchColumn('internalid'));
	    ved_columnsCheck1.push( new nlobjSearchColumn('name'));

	    var ved_searchResultsCheck1 = nlapiSearchRecord('subsidiary', null, ved_filchk1, ved_columnsCheck1);
	    if (ved_searchResultsCheck1 != null)
		{
	        for (i = 0; ved_searchResultsCheck1 != null && i < ved_searchResultsCheck1.length; i++)
			{
	            taxagency.addSelectOption(ved_searchResultsCheck1[i].getValue('internalid'),ved_searchResultsCheck1[i].getValue('name'), false);

	        }
	    }

	}
 function search_quartely_detailrecord(request, response)
 {
 
 	var i_internaid = '';
 	var subsidiary = request.getParameter('subsidiary')
 	nlapiLogExecution('DEBUG', 'create_quartely_detailrecord', 'subsidiary->' + subsidiary)
 	
 	var financial_yr = request.getParameter('custpage_year')
 	nlapiLogExecution('DEBUG', 'create_quartely_detailrecord', 'financial yr->' + financial_yr)
 	
 	var quarter = request.getParameter('custpage_quarter')
 	nlapiLogExecution('DEBUG', 'create_quartely_detailrecord', 'quarter->' + quarter)
 	
 	var filters = new Array();
 	filters.push(new nlobjSearchFilter('custrecord_financial_year', null, 'is', financial_yr));
 	filters.push(new nlobjSearchFilter('custrecord_quater', null, 'is', quarter));
 	
 	var context = nlapiGetContext();
 	var i_subcontext = context.getFeature('SUBSIDIARIES');
 	
 	if (i_subcontext != false) 
	{
 		filters.push(new nlobjSearchFilter('custrecord_quarterlyreturn_subsidiary', null, 'is', subsidiary));
 	}
 	var column = new Array();
 	column.push(new nlobjSearchColumn('internalid'));
 	
 	var results = nlapiSearchRecord('customrecord_quarterly_return_details', null, filters, column);
 	if (results != null && results != '' && results != undefined) 
	{
 		nlapiLogExecution('DEBUG', 'create_quartely_detailrecord', 'inside search')
 		i_internaid = results[0].getValue('internalid')
 		nlapiLogExecution('DEBUG', 'create_quartely_detailrecord', 'i_internaid->' + i_internaid)
 		
 	}
	return i_internaid;
 }
// END OBJECT CALLED/INVOKING FUNCTION =====================================================
