/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       14 Jul 2016     deepak.srinivas
 *
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType
 *   
 * @param {String} type Operation types: create, edit, view, copy, print, email
 * @param {nlobjForm} form Current form
 * @param {nlobjRequest} request Request object
 * @returns {Void}
 */
function create_BTPL_Invoice(type,form){
//Creates Button on Invoice Screen to generate PDF For BTPL Customers
try{
if(type=='view'){
var rcrd_id = nlapiGetRecordId();
//var loadRec = nlapiLoadRecord(nlapiGetRecordType(),nlapiGetRecordId());
var i_subsidiary = nlapiGetFieldValue('subsidiary');
if(i_subsidiary == 3){
form.addButton('custpage_addbutton_BTPL','Generate BTPL PDF','fxn_generatePDF(\''+rcrd_id+'\');');
form.setScript('customscript_btpl_invoice_pdf_cs');
}
}
}
catch(e){
nlapiLogExecution('ERROR','Processing PDF Error',e);
throw e;
}
}
