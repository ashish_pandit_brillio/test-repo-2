//BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
	/*
     Script Name	: SCH_UpdateDashboardProjectData
     Author			: Ashish Pandit
     Company		: Inspirria CloudTech Pvt Ltd
     Date			: 19/12/2018
     
	 
	 Script Modification Log:
     
	 -- Date --               -- Modified By --                  --Requested By--                   -- Description --
     

	 Below is a summary of the process controls enforced by this script file.  The control logic is described
     more fully, below, in the appropriate function headers and code blocks.
     SCHEDULED FUNCTION
     - RFQbody_SCH_main()
     SUB-FUNCTIONS
     - The following sub-functions are called by the above core functions in order to maintain code
     modularization:
     - NOT USED
	 */
}

//END SCRIPT DESCRIPTION BLOCK  ====================================

function scheduled_UpdateData() 
{
	try
	{
		/*Array to push Dashboard data*/
		var a_DBdataArray = new Array();
		
		/*Search to get Dashboard data records*/
		var customrecord_fulfillment_dashboard_dataSearch = nlapiSearchRecord("customrecord_fulfillment_dashboard_data",null,
		[
			//["custrecord_fulfill_dashboard_project","anyof","118768"]
		], 
		[
		   new nlobjSearchColumn("custrecord_fulfill_dashboard_project"), 
		   new nlobjSearchColumn("custrecord_fulfill_dashboard_account"), 
		   new nlobjSearchColumn("custrecord_fulfill_dashboard_opp_id"), 
		   new nlobjSearchColumn("custrecord_fulfill_dashboard_start_date"), 
		   new nlobjSearchColumn("custrecord_fulfill_dashboard_practice")
		]
		);
		if(customrecord_fulfillment_dashboard_dataSearch)
		{
			for(var i = 0;i<customrecord_fulfillment_dashboard_dataSearch.length;i++)
			{
				CheckMetering();
				var i_project = customrecord_fulfillment_dashboard_dataSearch[i].getValue('custrecord_fulfill_dashboard_project');
				var i_oppId = customrecord_fulfillment_dashboard_dataSearch[i].getValue('custrecord_fulfill_dashboard_opp_id');
				nlapiLogExecution('Debug','i_oppId ',i_oppId);
				if(i_project)
				{
					var i_team_size = GetTeamSize(i_project);
					nlapiLogExecution('Debug','i_team_size :  ',i_team_size);
					var search = getFRF_RRFCount(i_project);
					nlapiLogExecution('Debug','search ',search);
					if(search)
					{
						var rrfNumber = search.rrf;
						var frfNumber = search.frf;
						nlapiLogExecution('Debug','rrfNumber '+rrfNumber,'frfNumber '+frfNumber);
					}
					
					if(i_team_size)
						i_team_size = i_team_size.toString();
					var i_emp_exiting = GetEmployeeExist(i_project);
					if(i_emp_exiting)
						i_emp_exiting = i_emp_exiting.toString();
					
					/*Create Dashboard custom records*/
					try
					{
						nlapiLogExecution("AUDIT","fulfillment ids",customrecord_fulfillment_dashboard_dataSearch[i].getId());
						var recordObj = nlapiLoadRecord('customrecord_fulfillment_dashboard_data',customrecord_fulfillment_dashboard_dataSearch[i].getId());
						//recordObj.setFieldValue('custrecord_fulfill_dashboard_status',);
						if(i_team_size)
							recordObj.setFieldValue('custrecord_fulfill_dashboard_pro_team',i_team_size);
						if(i_emp_exiting)
							recordObj.setFieldValue('custrecord_fulfill_dashboard_exiting',i_emp_exiting);
						if(frfNumber)
							recordObj.setFieldValue('custrecord_fulfill_dashboard_frf',frfNumber);
                      	else
                           recordObj.setFieldValue('custrecord_fulfill_dashboard_frf',"0");
						if(rrfNumber)
							recordObj.setFieldValue('custrecord_fulfill_dashboard_rrf',rrfNumber);
                        else
                          	recordObj.setFieldValue('custrecord_fulfill_dashboard_rrf',"0");
						var rec = nlapiSubmitRecord(recordObj);
						nlapiLogExecution('Debug','rec ',rec);
					}
					catch(error)
					{
						nlapiLogExecution('debug','Exception Occured du',error)
					}
				}
				else if(i_oppId)
				{
					try
					{
						var search = getFRF_RRFCountOpp(i_oppId);
						if(search)
						{
							var rrfNumber = search.rrf;
							var frfNumber = search.frf;
							nlapiLogExecution('Debug','rrfNumber '+rrfNumber,'frfNumber '+frfNumber);
						
							var recordObj = nlapiLoadRecord('customrecord_fulfillment_dashboard_data',customrecord_fulfillment_dashboard_dataSearch[i].getId());
							//recordObj.setFieldValue('custrecord_fulfill_dashboard_status',);
							if(frfNumber)
								recordObj.setFieldValue('custrecord_fulfill_dashboard_frf',frfNumber);
                            else
                              	recordObj.setFieldValue('custrecord_fulfill_dashboard_frf',"0");
							if(rrfNumber)
								recordObj.setFieldValue('custrecord_fulfill_dashboard_rrf',rrfNumber);
                            else
                              	recordObj.setFieldValue('custrecord_fulfill_dashboard_rrf',"0");
							var rec = nlapiSubmitRecord(recordObj);
							nlapiLogExecution('Debug','rec ',rec);
						}
					}
					catch(error)
					{
						nlapiLogExecution('debug','Exception Occured du',error)
					}
				}
			}
		}
	}
	catch(e)
	{
		nlapiLogExecution('Debug','Exception ',e.message);
	}
}

//===================================== END OF FUNCTION ================================================

function _logValidation(value) 
{
 if(value!='null' && value != null && value != null && value != '' && value != undefined && value != undefined && value != 'undefined' && value!= 'undefined'&& value!= 'NaN' && value != NaN) 
 {
  return true;
 }
 else 
 { 
  return false;
 }
}

/*----Function to count Team Size-----*/
function GetTeamSize(i_project) {
	var jobSearch = nlapiSearchRecord("resourceallocation",null,
			[
			 ["project","anyof",i_project],"AND",["enddate","notbefore","today"], 
    "AND", 
    ["startdate","notafter","today"]
		
			 ], 
			[
			 // new nlobjSearchColumn("resource","resourceAllocation","group")
			  new nlobjSearchColumn("resource",null,"GROUP")
			]
	);
	if (jobSearch) {
	//var resourceCheck = jobSearch[0].getValue("resource",null,"GROUP");
	
		return jobSearch.length;
	}else{
		return "0";
	}
}
/*----Function to count employee exiting in 30 days-----*/
function GetEmployeeExist(i_project) {
	Date.prototype.addDays = function(days) {
		var date = new Date(this.valueOf());
		date.setDate(date.getDate() + days);
		return date;
	}
	var date = new Date();
	date.addDays(30);
	var resourceallocationSearch = nlapiSearchRecord("resourceallocation",null,
			[
			 ["job.internalid","anyof",i_project], //5515 project internal ID
			 "AND", 
			 ["employee.custentity_future_term_date","onorafter","today"] // 01/05/2019 calculated dynamically using current date and adding 30 days to it. 
			 ], 
			 [
			   new nlobjSearchColumn("resource",null,"GROUP")
			 ]
	);
	if (resourceallocationSearch) {
		return resourceallocationSearch.length;
	}else{
      return "";
    }
}
/*----Function to get FRF and RRF count-----*/
/*Function to get FRF details*/
function GetFRF(i_oppID) {
	nlapiLogExecution('AUDIT', 'project ID ', i_oppID);
	var customrecord_frf_detailsSearch = nlapiSearchRecord("customrecord_frf_details",null,
	[
	   ["custrecord_frf_details_opp_id","anyof",i_oppID]
	], 
	[
		new nlobjSearchColumn("custrecord_frf_details_external_hire")
	]
	);
	return customrecord_frf_detailsSearch;
}


//Get current date
function sysDate() {
	var date = new Date();
	var tdate = date.getDate();
	var month = date.getMonth() + 1; // jan = 0
	var year = date.getFullYear();
	return currentDate = month + '/' + tdate + '/' + year;
	}
//Returns timestamp
function timestamp() {
	var str = "";

	var currentTime = new Date();
	var hours = currentTime.getHours();
	var minutes = currentTime.getMinutes();
	var seconds = currentTime.getSeconds();
	var meridian = "";
	if (hours > 12) {
	    meridian += "pm";
	} else {
	    meridian += "am";
	}
	if (hours > 12) {

	    hours = hours - 12;
	}
	if (minutes < 10) {
	    minutes = "0" + minutes;
	}
	if (seconds < 10) {
	    seconds = "0" + seconds;
	}
	//str += hours + ":" + minutes + ":" + seconds + " ";
	str += hours + ":" + minutes +" ";
	return str + meridian;
	}
	function GetManagerName(tempString) {
	var s_manager = "";
	//var tempString = searchResult[int].getText("custentity_projectmanager","custrecord_project_internal_id_sfdc",null);
	temp = tempString.indexOf("-");
	if(temp>0)
	{
		var s_manager = tempString.split("-")[1];
	}
	else{
		var s_manager = tempString;
	}
	return s_manager;
}

function getFRF_RRFCount(i_project)
{
	var customrecord_frf_detailsSearch = nlapiSearchRecord("customrecord_frf_details",null,
	[
	   ["custrecord_frf_details_project","anyof",i_project], 
	   "AND", 
	   ["isinactive","is","F"], 
	   "AND", 
	   ["custrecord_frf_details_opp_id.custrecord_stage_sfdc","noneof","8","9"],
	   "AND",
	   ["custrecord_frf_details_status","is","F"],
	   "AND",
	   ["custrecord_frf_details_status_flag","noneof","2"]
	], 
	[
	   new nlobjSearchColumn("custrecord_frf_details_external_hire",null,"GROUP"), 
	   new nlobjSearchColumn("internalid",null,"COUNT")
	]
	);
	if(customrecord_frf_detailsSearch)
	{
		for(var i=0; i<customrecord_frf_detailsSearch.length;i++)
		{
			//nlapiLogExecution('Debug','customrecord_frf_detailsSearch.length ',customrecord_frf_detailsSearch.length);
			var isExternalHire = customrecord_frf_detailsSearch[i].getValue("custrecord_frf_details_external_hire",null,"GROUP");
			var count = customrecord_frf_detailsSearch[i].getValue("internalid",null,"COUNT"); 
			
			var i_rrf = 0;
			var i_frf = 0;
			if(isExternalHire =='T')
			{
				i_rrf = count;
			}
			else
			{
				i_frf = count;
			}
		}
		return { "rrf":i_rrf,
				 "frf":i_frf
			   }
	}
}
function getFRF_RRFCountOpp(i_opp)
{
	var customrecord_frf_detailsSearch = nlapiSearchRecord("customrecord_frf_details",null,
	[
	   ["custrecord_frf_details_opp_id","anyof",i_opp], 
	   "AND", 
	   ["isinactive","is","F"], 
	   "AND", 
	   ["custrecord_frf_details_opp_id.custrecord_stage_sfdc","noneof","8","9"],
	   "AND",
	   ["custrecord_frf_details_status","is","F"],
	   "AND",
	   ["custrecord_frf_details_status_flag","noneof","2"]
	], 
	[
	   new nlobjSearchColumn("custrecord_frf_details_external_hire",null,"GROUP"), 
	   new nlobjSearchColumn("internalid",null,"COUNT")
	]
	);
	if(customrecord_frf_detailsSearch)
	{
		for(var i=0; i<customrecord_frf_detailsSearch.length;i++)
		{
			//nlapiLogExecution('Debug','customrecord_frf_detailsSearch.length ',customrecord_frf_detailsSearch.length);
			var isExternalHire = customrecord_frf_detailsSearch[i].getValue("custrecord_frf_details_external_hire",null,"GROUP");
			var count = customrecord_frf_detailsSearch[i].getValue("internalid",null,"COUNT"); 
			
			var i_rrf = 0;
			var i_frf = 0;
			if(isExternalHire =='T')
			{
				i_rrf = count;
			}
			else
			{
				i_frf = count;
			}
		}
		return { "rrf":i_rrf,
				 "frf":i_frf
			   }
	}
}
function CheckMetering() {
    var CTX = nlapiGetContext();
    var START_TIME = new Date().getTime();
    //	want to try to only check metering every 15 seconds
    var remainingUsage = CTX.getRemainingUsage();
    var currentTime = new Date().getTime();
    var timeDifference = currentTime - START_TIME;
    //	changing to 15 minutes, should cause little if any impact, but willmake runaway scripts faster to kill
    if (remainingUsage < 800 || timeDifference > 900000) {
        START_TIME = new Date().getTime();
        var status = nlapiYieldScript();
        nlapiLogExecution('AUDIT', 'STATUS = ', JSON.stringify(status));
    }
}