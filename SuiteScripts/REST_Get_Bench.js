/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       01 Apr 2019     Aazamali Khan
 *
 */

/**
 * @param {Object} dataIn Parameter object
 * @returns {Object} Output object
 */
function postRESTlet(dataIn) {
	var searchResult = GetBenchProjection();
	var dataOut = [];
	var s_certificate = [];
	if (searchResult) {
		for (var i = 0; i < 10 ; i++) {
			var empId = searchResult[i].getValue("resource",null,"GROUP");
			var status = GetFRFSoftLock(empId);
			var jsonObj = {};
			jsonObj.avaliable = searchResult[i].getValue("percentoftime",null,"GROUP");
			jsonObj.employeename = searchResult[i].getText("resource",null,"GROUP");
			jsonObj.designation = nlapiLookupField("employee", empId, "title");
			jsonObj.practice = nlapiLookupField("employee", empId, "department",true);
			var skillData = getFamilySkill(empId);
			
			if (skillData) {
				jsonObj.skills = skillData[0].getText("custrecord_primary_updated") +","+skillData[0].getValue("custrecord_secondry_updated") ;
				for (var j = 0; j < skillData.length; j++) {
					var array_element = skillData[j].getValue("custrecord_certifcate_det","CUSTRECORD_EMP_SKILL_PARENT",null);
					if(array_element){
					 s_certificate.push(array_element);
					}
				}
				jsonObj.certificates = s_certificate;
			}else{
				jsonObj.skills = "";
				jsonObj.certificates = [];
			}
			jsonObj.tranings = [];
			nlapiLogExecution("AUDIT", "Allocation category : ", searchResult[i].getValue("custentity_project_allocation_category","job","GROUP"));
			if (searchResult[i].getValue("custentity_project_allocation_category","job","GROUP") == "4") {
				var enddate = searchResult[i].getValue("enddate",null,"MAX");
				enddate = nlapiStringToDate(enddate);
				var date = new Date();
				var milliseconds = date - enddate;
				var days = Math.round(milliseconds / (1000*60*60*24));
				nlapiLogExecution("ERROR", "bench days : ", days);
				jsonObj.bench = enddate;
				jsonObj.days = Math.abs(days);
				
			}else{
				jsonObj.bench = searchResult[i].getValue("enddate",null,"MAX");
				jsonObj.days = "NA";
			}
			jsonObj.status = status;
			jsonObj.utilization = "";
			nlapiLogExecution("ERROR", "bench date : ", searchResult[i].getValue("enddate",null,"MAX"));
			dataOut.push(jsonObj);
		}
	}
	return {data : dataOut};
}
function GetBenchResource() {

	var resourceallocationSearch = nlapiSearchRecord("resourceallocation",null,
			[
			 ["job.custentity_project_allocation_category","anyof","4"], 
			 "AND", 
			 ["job.status","anyof","2"], 
			 "AND", 
			 ["employee.isinactive","is","F"], 
			 "AND", 
			 ["startdate","notafter","today"], //+ 11 month
			 "AND", 
			 ["enddate","notbefore","today"]/*, // start date of month
			 "AND",
			 ["custevent_practice","anyof",i_practice]*/
			 ], 
			 [
			  new nlobjSearchColumn("custentity_emp_function","employee","GROUP"), 
			  new nlobjSearchColumn("subsidiarynohierarchy","employee","GROUP"), 
			  new nlobjSearchColumn("formulatext",null,"GROUP").setFormula("CASE WHEN INSTR({employee.department} , ' : ', 1) > 0 THEN SUBSTR({employee.department}, 1 , INSTR({employee.department} , ' : ', 1)) ELSE {employee.department} END"), 
			  new nlobjSearchColumn("departmentnohierarchy","employee","GROUP"), 
			  new nlobjSearchColumn("resource",null,"GROUP"), 
			  new nlobjSearchColumn("employeestatus","employee","GROUP"), 
			  new nlobjSearchColumn("title","employee","GROUP"), 
			  new nlobjSearchColumn("altname","customer","GROUP"), 
			  new nlobjSearchColumn("entityid","job","GROUP"), 
			  new nlobjSearchColumn("companyname","job","GROUP"), 
			  new nlobjSearchColumn("jobtype","job","GROUP"), 
			  new nlobjSearchColumn("custentity_practice","job","GROUP"), 
			  new nlobjSearchColumn("formulatext",null,"GROUP").setFormula("case when {custevent_ra_is_shadow} = 'T' then 'Shadow' else ' ' end"), 
			  new nlobjSearchColumn("custevent4",null,"GROUP"), 
			  new nlobjSearchColumn("startdate",null,"GROUP"), 
			  new nlobjSearchColumn("enddate",null,"GROUP"), 
			  new nlobjSearchColumn("jobbillingtype","job","GROUP"), 
			  new nlobjSearchColumn("custentity_t_and_m_monthly","job","GROUP"), 
			  new nlobjSearchColumn("custeventwlocation",null,"GROUP"), 
			  new nlobjSearchColumn("custevent_workcityra",null,"GROUP"), 
			  new nlobjSearchColumn("location","employee","GROUP"), 
			  new nlobjSearchColumn("custentity_projectmanager","job","GROUP"), 
			  new nlobjSearchColumn("custentity_deliverymanager","job","GROUP"), 
			  new nlobjSearchColumn("custentity_reportingmanager","employee","GROUP"), 
			  new nlobjSearchColumn("employeetype","employee","GROUP"), 
			  new nlobjSearchColumn("hiredate","employee","GROUP"), 
			  new nlobjSearchColumn("percentoftime",null,"GROUP")
			  ]
	);
	return resourceallocationSearch;
}
function getFamilySkill(employeeId){

	var customrecord_employee_master_skill_dataSearch = nlapiSearchRecord("customrecord_employee_master_skill_data",null,

			[

			 ["custrecord_employee_skill_updated","anyof",employeeId]

			 ],
			 [
			  new nlobjSearchColumn("internalid"),
			  new nlobjSearchColumn("custrecord_primary_updated"),
			  new nlobjSearchColumn("custrecord_secondry_updated"),
			  new nlobjSearchColumn("custrecord_family_selected"),
			  new nlobjSearchColumn("custrecord_certifcate_det","CUSTRECORD_EMP_SKILL_PARENT",null),
			  new nlobjSearchColumn("created").setSort(true)
			  ]

	);

	var searchData = customrecord_employee_master_skill_dataSearch;
	nlapiLogExecution('DEBUG', 'searchData:',JSON.stringify(searchData));
	if (searchData) {
		return searchData;
	}else{
		return "";
	}
}
function GetBenchProjection() {
	var resourceallocationSearch = nlapiSearchRecord("resourceallocation",null,
			[
			 ["enddate","within","04/01/2019","07/01/2019"]/*,
			   "AND", 
			   ["job.custentity_project_allocation_category","anyof","4"]*//*, 
			   "AND", 
			   ["resource","anyof","1670"]*/
			 ], 
			 [
			  new nlobjSearchColumn("resource",null,"GROUP"), 
			  new nlobjSearchColumn("enddate",null,"MAX"), 
			  new nlobjSearchColumn("percentoftime",null,"GROUP"),
			  new nlobjSearchColumn("custentity_project_allocation_category","job","GROUP")
			  ]
	);
	return resourceallocationSearch;
}
function GetFRFSoftLock(empId) {
	var customrecord_frf_detailsSearch = nlapiSearchRecord("customrecord_frf_details",null,
			[
			   ["custrecord_frf_details_selected_emp","anyof",empId]
			], 
			[
			   new nlobjSearchColumn("scriptid").setSort(false), 
			   new nlobjSearchColumn("custrecord_frf_details_opp_id"), 
			   new nlobjSearchColumn("custrecord_frf_details_res_location"), 
			   new nlobjSearchColumn("custrecord_frf_details_res_practice"), 
			   new nlobjSearchColumn("custrecord_frf_details_emp_level"), 
			   new nlobjSearchColumn("custrecord_frf_details_external_hire"), 
			   new nlobjSearchColumn("custrecord_frf_details_role"), 
			   new nlobjSearchColumn("custrecord_frf_details_bill_rate"), 
			   new nlobjSearchColumn("custrecord_frf_details_critical_role"), 
			   new nlobjSearchColumn("custrecord_frf_details_skill_family"), 
			   new nlobjSearchColumn("custrecord_frf_details_start_date"), 
			   new nlobjSearchColumn("custrecord_frf_details_end_date"), 
			   new nlobjSearchColumn("custrecord_frf_details_selected_emp"), 
			   new nlobjSearchColumn("custrecord_frf_details_internal_fulfill"), 
			   new nlobjSearchColumn("custrecord_frf_details_primary_skills"), 
			   new nlobjSearchColumn("custrecord_frf_details_status"), 
			   new nlobjSearchColumn("custrecord_frf_details_frf_number"), 
			   new nlobjSearchColumn("custrecord_frf_details_project"), 
			   new nlobjSearchColumn("custrecord_frf_details_suggestion"), 
			   new nlobjSearchColumn("custrecord_frf_type"), 
			   new nlobjSearchColumn("custrecord_frf_emp_notice_rotation"), 
			   new nlobjSearchColumn("custrecord_frf_details_billiable"), 
			   new nlobjSearchColumn("custrecord_frf_details_source"), 
			   new nlobjSearchColumn("custrecord_frf_details_secondary_skills"), 
			   new nlobjSearchColumn("custrecord_frf_details_allocation"), 
			   new nlobjSearchColumn("custrecord_frf_details_dollar_loss"), 
			   new nlobjSearchColumn("custrecord_frf_details_suggest_pref_mat"), 
			   new nlobjSearchColumn("custrecord_frf_details_special_req"), 
			   new nlobjSearchColumn("custrecord_frf_details_personal_email"), 
			   new nlobjSearchColumn("custrecord_frf_details_created_by"), 
			   new nlobjSearchColumn("custrecord_frf_details_parent"), 
			   new nlobjSearchColumn("custrecord_frf_details_backup_require"), 
			   new nlobjSearchColumn("custrecord_frf_details_job_title"), 
			   new nlobjSearchColumn("custrecord_frf_details_edu_lvl"), 
			   new nlobjSearchColumn("custrecord_frf_details_edu_program"), 
			   new nlobjSearchColumn("custrecord_frf_details_emp_status"), 
			   new nlobjSearchColumn("custrecord_frf_details_first_interviewer"), 
			   new nlobjSearchColumn("custrecord_frf_details_sec_interviewer"), 
			   new nlobjSearchColumn("custrecord_frf_details_cust_interview"), 
			   new nlobjSearchColumn("custrecord_frf_details_cust_inter_email"), 
			   new nlobjSearchColumn("custrecord_frf_details_ta_manager"), 
			   new nlobjSearchColumn("custrecord_frf_details_req_type"), 
			   new nlobjSearchColumn("custrecord_frf_details_reason_external"), 
			   new nlobjSearchColumn("custrecord_frf_details_open_close_status"), 
			   new nlobjSearchColumn("custrecord_frf_details_allocated_emp"), 
			   new nlobjSearchColumn("custrecord_frf_details_availabledate"), 
			   new nlobjSearchColumn("custrecord_frf_details_region")
			]
			);
	if(customrecord_frf_detailsSearch){
		return customrecord_frf_detailsSearch[0].getValue("custrecord_frf_details_frf_number");
	}else{
		return "";
	}
}

