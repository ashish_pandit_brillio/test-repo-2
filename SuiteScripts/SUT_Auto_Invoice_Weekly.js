/**
 * Suitelet form for invoicing T&M weekly projects
 * 
 * Version    Date            Author           Remarks
 * 1.00       29 Jul 2015     Nitish Mishra
 * 2.00		  10 Apr 2020	  Praveena 			Changed searched internal id and searched record type
 */

/**
 * @param {nlobjRequest}
 *            request Request object
 * @param {nlobjResponse}
 *            response Response object
 * @returns {Void} Any output is written via response object
 */
function suitelet(request, response) {
	try {
	
		if (request.getMethod() == 'GET') {
			createGetForm(request);
		} else {
          nlapiLogExecution('ERROR', 'post beginig', response);
			postDetails(request);
		}
	} catch (err) {
		nlapiLogExecution('ERROR', 'suitelet', err);
      
	}
}

function postDetails(request) {
	try {
	//	nlapiLogExecution('debug', 'postDetails', 'started');
		var lineCount = request.getLineItemCount('custpage_timesheet');
		nlapiLogExecution('debug', 'lineCount', lineCount);
		var data = [];

		for (var linenum = 1; linenum <= lineCount; linenum++) {

			if (request
			        .getLineItemValue('custpage_timesheet', 'apply', linenum) == 'T') {
				data.push({
				    c : request.getLineItemValue('custpage_timesheet',
				            'customer', linenum),
				    p : request.getLineItemValue('custpage_timesheet',
				            'project', linenum),
				    e : request.getLineItemValue('custpage_timesheet',
				            'employee', linenum),
				    f : request.getLineItemValue('custpage_timesheet',
				            'd_startdate', linenum),
				    t : request.getLineItemValue('custpage_timesheet',
				            'd_enddate', linenum)
				});
			}
		}

		nlapiLogExecution('debug', 'invoice data length', data.length);

		var params = {
			custscript_invoice_details : JSON.stringify(data)
		};

		nlapiScheduleScript('customscript_sch_generate_invoice',
		        'customdeploy_sch_generate_invoice', params);
		nlapiLogExecution('debug', 'data passed to scheduled script');

	//	nlapiLogExecution('debug', 'postDetails', 'ended');

		response.sendRedirect('SUITELET',
		        'customscript_sut_auto_invoice_status',
		        'customdeploy_sut_auto_invoice_status', null, {
			        t : data.length
		        });
	} catch (err) {
		nlapiLogExecution('ERROR', 'postDetails', err);
		throw err;
	}
}

function createGetForm(request) {
	try {
		
		var form = nlapiCreateForm('Auto Invoice');
		var selectedCustomer = request.getParameter('custid');
     //var selectedCustomer =65865;
		nlapiLogExecution('debug', 'Customer', selectedCustomer);
       
		if (selectedCustomer) {
			form.setScript('customscript_cs_auto_billing_weekly');

			var billingCustomer = form.addField('custpage_billing_customer',
			        'select', 'Customer', 'customer');
			billingCustomer.setDefaultValue(selectedCustomer);
			billingCustomer.setDisplayType('inline');

			var billingCycle = form.addField('custpage_billing_cycle',
			        'select', 'Billing Cycle',
			        'customrecord_customer_billing_cycle');
			billingCycle.setDisplayType('inline');

			// get customer data
			var customerDetails = nlapiLookupField('customer',
			        selectedCustomer, [ 'custentity_cust',
			                'custentity_cust.custrecord_cbc_start_date',
			                'custentity_cust.custrecord_cbc_interval',
			                'custentity_cust.custrecord_cbc_days_from_sunday',
			                'custentity_cust.custrecord_cbc_is_weekly',
			                'custentity_cust.custrecord_cbc_start_date' ]);

			billingCycle.setDefaultValue(customerDetails.custentity_cust);

			// list of projects under above customers
			var projects = getRelatedProjects(selectedCustomer);
			var project_list = [];
			projects.forEach(function(project) {
				project_list.push(project.getId());
			});
			nlapiLogExecution('debug', 'Checkpoint', 'Project List Created : '
			        + project_list.length);

			// list of time-sheets under above projects
			var timesheets = [];

			if (customerDetails['custentity_cust.custrecord_cbc_is_weekly'] == 'T' && parseInt(selectedCustomer) ==parseInt(4297)) {
				timesheets = getTimeEntryWeekWise_FEHL(
				        project_list,
				        customerDetails['custentity_cust.custrecord_cbc_days_from_sunday'],
				        customerDetails['custentity_cust.custrecord_cbc_interval'],selectedCustomer);
				/*timesheets = getTimeEntryWeekWise2(
				        project_list,
				        customerDetails['custentity_cust.custrecord_cbc_days_from_sunday'],
				        customerDetails['custentity_cust.custrecord_cbc_interval']);*/
			}
			else if (customerDetails['custentity_cust.custrecord_cbc_is_weekly'] == 'T' ) {
				timesheets = getTimeEntryWeekWise(
				        project_list,
				        customerDetails['custentity_cust.custrecord_cbc_days_from_sunday'],
				        customerDetails['custentity_cust.custrecord_cbc_interval']);
			}
				
			
			else {
				timesheets = getTimeEntryMonthWise(
				        project_list,
				        customerDetails['custentity_cust.custrecord_cbc_start_date']);
			}

			nlapiLogExecution('debug', 'Checkpoint',
			        'All Timesheets Fetched : ' + timesheets.length);

			timesheets.forEach(function(time) {
				time.customer = selectedCustomer;
				time.totalamount = parseFloat(time.st_hours)
				        * parseFloat(time.st_rate) + parseFloat(time.ot_hours)
				        * parseFloat(time.ot_rate);
			});

			// creating time-sheet sublist
			var timesheet_sublist = form.addSubList('custpage_timesheet',
			        'list', 'Timesheet');
			timesheet_sublist.addMarkAllButtons();
			timesheet_sublist.addRefreshButton();

			timesheet_sublist.addField('apply', 'checkbox', 'Apply');
			timesheet_sublist.addField('customer', 'select', 'Customer',
			        'customer').setDisplayType('inline');
			timesheet_sublist.addField('project', 'select', 'Project', 'job')
			        .setDisplayType('inline');
			timesheet_sublist.addField('employee', 'select', 'Employee',
			        'employee').setDisplayType('inline');
			timesheet_sublist.addField('st_hours', 'float', 'ST Hours')
			        .setDisplayType('inline');
			timesheet_sublist.addField('st_rate', 'currency', 'ST Rate')
			        .setDisplayType('inline');
			timesheet_sublist.addField('ot_hours', 'float', 'OT Hours')
			        .setDisplayType('inline');
			timesheet_sublist.addField('ot_rate', 'currency', 'OT Rate')
			        .setDisplayType('inline');
			timesheet_sublist.addField('leave_hours', 'float', 'Leave Hours')
			        .setDisplayType('inline');
			timesheet_sublist.addField('holiday_hours', 'float',
			        'Holiday Hours').setDisplayType('inline');
			timesheet_sublist.addField('totalamount', 'currency', 'Total')
			        .setDisplayType('inline');
			timesheet_sublist.addField('d_startdate', 'date', 'Start Date')
			        .setDisplayType('inline');
			timesheet_sublist.addField('d_enddate', 'date', 'End Date')
			        .setDisplayType('inline');
			timesheet_sublist.setLineItemValues(timesheets);

			form.addButton('custpage_go_back', 'Go Back', 'goBack');
			form.addSubmitButton('Generate Invoices');
		} else {
			//nlapiLogExecution('debug', 'create customer selection form');

			var customerSearch = nlapiSearchRecord('customer', '1046');
			var sublist = form.addSubList('custpage_customer_list', 'list',
			        'Please select a customer');
			sublist.addField('customer', 'text', 'Customer').setDisplayType(
			        'inline');
			sublist.addField('billingcycle', 'text', 'Billing Cycle')
			        .setDisplayType('inline');

			var sublist_data = [];
			var url = nlapiResolveURL('SUITELET',
			        'customscript_sut_auto_invoicing_weekly',
			        'customdeploy_sut_auto_invoicing_weekly')
			        + "&custid=";

			for (var i = 0; i < customerSearch.length; i++) {
				var customer_url = url + customerSearch[i].getId();

				sublist_data.push({
				    customer : "<a href='" + customer_url + "'>"
				            + customerSearch[i].getValue('companyname')
				            + "</a>",
				    billingcycle : customerSearch[i].getText('custentity_cust')
				});
				nlapiLogExecution('Debug', 'createGetForm', sublist_data[0].customer);
			}

			sublist.setLineItemValues(sublist_data);
		}
		response.writePage(form);

	} catch (err) {
		nlapiLogExecution('ERROR', 'createGetForm', err);
		throw err;
	}
}

function getAutoEnabledCustomers() {
	try {

	} catch (err) {
		nlapiLogExecution('ERROR', 'getAutoEnabledCustomers', err);
		throw err;
	}
}

function getRelatedProjects(customerId) {
	try {
	nlapiLogExecution('audit','cust id:- '+customerId);
		return searchRecord('job', null, [
		        new nlobjSearchFilter('customer', null, 'anyof', customerId),
		        new nlobjSearchFilter('jobbillingtype', null, 'anyof', 'TM'),
				new nlobjSearchFilter('custentity_exclude_auto_invoice', null, 'is', 'T')]);
	} catch (err) {
		nlapiLogExecution('ERROR', 'getRelatedProjects', err);
		throw err;
	}
}

function getUnbilledTimesheets(projectId) {
	try {
		return searchRecord('timeentry', 892, [ new nlobjSearchFilter(
		        'customer', null, 'anyof', projectId) ]);
	} catch (err) {
		nlapiLogExecution('ERROR', 'getRelatedProjects', err);
		throw err;
	}
}
function getTimeEntryWeekWise2(projectId, dayDiff, daysInterval) {
	try {
		var timeEntryList = [];

		dayDiff = parseInt(dayDiff);
		nlapiLogExecution('debug', 'day diff', dayDiff);

		// daysInterval = 7;// ??
		daysInterval = parseInt(daysInterval);
		nlapiLogExecution('debug', 'daysInterval', daysInterval);

		var timeEntrySearch = searchRecord('timebill', 3749,
		        [ new nlobjSearchFilter('customer', null, 'anyof', projectId) ]);

		if (timeEntrySearch) {

			var ot_hours = 0, ot_rate = 0, st_hours = 0, st_rate = 0, holiday_hours = 0, holiday_rate = 0, leave_hours = 0, leave_rate = 0;

			for (var i = 0; i < timeEntrySearch.length; i++) {
				ot_hours = 0, ot_rate = 0, st_hours = 0, st_rate = 0,
				        holiday_hours = 0, holiday_rate = 0, leave_hours = 0,
				        leave_rate = 0;
				var weekExists = false;
				var employee = timeEntrySearch[i].getValue('employee');
				var project = timeEntrySearch[i].getValue('customer');
				var date = nlapiStringToDate(timeEntrySearch[i]
				        .getValue('date'));
				var hours = parseFloat(timeEntrySearch[i]
				        .getValue('durationdecimal'));
				var rate = parseFloat(timeEntrySearch[i].getValue('rate'));
				var item = timeEntrySearch[i].getValue('item');

				if (item == 'OT') { // OT
					ot_hours = hours;
					ot_rate = rate;
				} else if (item == 'Holiday') { // holiday
					holiday_hours = hours;
					holiday_rate = rate;
				} else if (item == 'Leave') { // leave
					leave_hours = hours;
					leave_rate = rate;
				} else { // ST
					st_hours = hours;
					st_rate = rate;
				}

				// week start and week end date calculation
				var weekStartDate = getWeekStartDate(date, dayDiff);
				var weekEndDate = nlapiAddDays(weekStartDate, 6);
				
				// nlapiLogExecution('debug', 'weekStartDate', weekStartDate);
				// nlapiLogExecution('debug', 'weekEndDate', weekEndDate);

				var s_weekStartDate = nlapiDateToString(weekStartDate, 'date');
				var s_weekEndDate = nlapiDateToString(weekEndDate, 'date');

				for (var j = 0; j < timeEntryList.length; j++) {

					if (timeEntryList[j].employee == employee
					        && timeEntryList[j].project == project
					        && timeEntryList[j].d_startdate == s_weekStartDate ) {

						if (item == 'OT') { // OT
							timeEntryList[j].ot_hours += hours;
							timeEntryList[j].ot_rate = rate;
						} else if (item == 'Holiday') { // holiday
							timeEntryList[j].holiday_hours += hours;
							timeEntryList[j].holiday_rate = rate;
						} else if (item == 'Leave') { // leave
							timeEntryList[j].leave_hours += hours;
							timeEntryList[j].leave_rate = rate;
						} else { // ST
							timeEntryList[j].st_hours += hours;
							timeEntryList[j].st_rate = rate;
						}
						weekExists = true;
						break;
					}
				}

				if (!weekExists) {
					timeEntryList.push({
					    employee : employee,
					    project : project,
					    st_hours : st_hours,
					    ot_hours : ot_hours,
					    leave_hours : leave_hours,
					    holiday_hours : holiday_hours,
					    st_rate : st_rate,
					    ot_rate : ot_rate,
					    leave_rate : leave_rate,
					    holiday_rate : holiday_rate,
					    startdate : weekStartDate,
					    enddate : weekEndDate,
					    d_startdate : nlapiDateToString(weekStartDate, 'date'),
					    d_enddate : nlapiDateToString(weekEndDate, 'date')
					});
				}
			}
		}

		var updatedTimeEntryList = [];

		// remove entries where ST and OT hours both are zero
		timeEntryList.forEach(function(time) {

			if (time.st_hours > 0 || time.ot_hours > 0) {
				updatedTimeEntryList.push(time);
			}
		});

		return updatedTimeEntryList;
	} catch (err) {
		nlapiLogExecution('ERROR', 'getTimeEntryWeekWise', err);
		throw err;
	}
}
function getTimeEntryWeekWise(projectId, dayDiff, daysInterval) {
	try {
		var timeEntryList = [];

		dayDiff = parseInt(dayDiff);
		nlapiLogExecution('debug', 'day diff', dayDiff);

		// daysInterval = 7;// ??
		daysInterval = parseInt(daysInterval);
		nlapiLogExecution('debug', 'daysInterval', daysInterval);

		//var timeEntrySearch = searchRecord('timebill', 3373,
		var timeEntrySearch = searchRecord('timebill', 'customsearch1047_2',
		        [ new nlobjSearchFilter('customer', null, 'anyof', projectId) ]);

		if (timeEntrySearch) {

			var ot_hours = 0, ot_rate = 0, st_hours = 0, st_rate = 0, holiday_hours = 0, holiday_rate = 0, leave_hours = 0, leave_rate = 0;

			for (var i = 0; i < timeEntrySearch.length; i++) {
				ot_hours = 0, ot_rate = 0, st_hours = 0, st_rate = 0,
				        holiday_hours = 0, holiday_rate = 0, leave_hours = 0,
				        leave_rate = 0;
				var weekExists = false;
				var employee = timeEntrySearch[i].getValue('employee');
				var project = timeEntrySearch[i].getValue('customer');
				var date = nlapiStringToDate(timeEntrySearch[i]
				        .getValue('date'));
				var hours = parseFloat(timeEntrySearch[i]
				        .getValue('durationdecimal'));
				var rate = parseFloat(timeEntrySearch[i].getValue('rate'));
				var item = timeEntrySearch[i].getValue('item');

				if (item == 'OT') { // OT
					ot_hours = hours;
					ot_rate = rate;
				} else if (item == 'Holiday') { // holiday
					holiday_hours = hours;
					holiday_rate = rate;
				} else if (item == 'Leave') { // leave
					leave_hours = hours;
					leave_rate = rate;
				} else { // ST
					st_hours = hours;
					st_rate = rate;
				}

				// week start and week end date calculation
				var weekStartDate = getWeekStartDate(date, dayDiff);
				var weekEndDate = nlapiAddDays(weekStartDate, 6);
				
				// nlapiLogExecution('debug', 'weekStartDate', weekStartDate);
				// nlapiLogExecution('debug', 'weekEndDate', weekEndDate);

				var s_weekStartDate = nlapiDateToString(weekStartDate, 'date');
				var s_weekEndDate = nlapiDateToString(weekEndDate, 'date');

				for (var j = 0; j < timeEntryList.length; j++) {

					if (timeEntryList[j].employee == employee
					        && timeEntryList[j].project == project
					        && timeEntryList[j].d_startdate == s_weekStartDate) {

						if (item == 'OT') { // OT
							timeEntryList[j].ot_hours += hours;
							timeEntryList[j].ot_rate = rate;
						} else if (item == 'Holiday') { // holiday
							timeEntryList[j].holiday_hours += hours;
							timeEntryList[j].holiday_rate = rate;
						} else if (item == 'Leave') { // leave
							timeEntryList[j].leave_hours += hours;
							timeEntryList[j].leave_rate = rate;
						} else { // ST
							timeEntryList[j].st_hours += hours;
							timeEntryList[j].st_rate = rate;
						}
						weekExists = true;
						break;
					}
				}

				if (!weekExists) {
					timeEntryList.push({
					    employee : employee,
					    project : project,
					    st_hours : st_hours,
					    ot_hours : ot_hours,
					    leave_hours : leave_hours,
					    holiday_hours : holiday_hours,
					    st_rate : st_rate,
					    ot_rate : ot_rate,
					    leave_rate : leave_rate,
					    holiday_rate : holiday_rate,
					    startdate : weekStartDate,
					    enddate : weekEndDate,
					    d_startdate : nlapiDateToString(weekStartDate, 'date'),
					    d_enddate : nlapiDateToString(weekEndDate, 'date')
					});
				}
			}
		}

		var updatedTimeEntryList = [];

		// remove entries where ST and OT hours both are zero
		timeEntryList.forEach(function(time) {

			if (time.st_hours > 0 || time.ot_hours > 0) {
				updatedTimeEntryList.push(time);
			}
		});

		return updatedTimeEntryList;
	} catch (err) {
		nlapiLogExecution('ERROR', 'getTimeEntryWeekWise', err);
		throw err;
	}
}
//For Only FEHL Customer
function getTimeEntryWeekWise_FEHL(projectId, dayDiff, daysInterval,i_customer) {
	try {
		var timeEntryList = [];

		dayDiff = parseInt(dayDiff); //Days from Sunday = 1
		nlapiLogExecution('debug', 'day diff', dayDiff);

		// daysInterval = 7;// ??
		daysInterval = parseInt(daysInterval); //Week interval = 7
		nlapiLogExecution('debug', 'daysInterval', daysInterval);

		
		var timeEntrySearch = searchRecord('timebill', 3749,
		        [ new nlobjSearchFilter('customer', null, 'anyof', projectId)//45499 // 72256 //280194
		         // ,new nlobjSearchFilter('employee', null, 'anyof', 72985)  
		        //  ,new nlobjSearchFilter('internalid', 'timesheet', 'anyof', 349512)
		        ]);
		if (timeEntrySearch) {

			var ot_hours = 0, ot_rate = 0, st_hours = 0, st_rate = 0, holiday_hours = 0, holiday_rate = 0, leave_hours = 0, leave_rate = 0;

			for (var i = 0; i < timeEntrySearch.length; i++) {
				ot_hours = 0, ot_rate = 0, st_hours = 0, st_rate = 0,
				        holiday_hours = 0, holiday_rate = 0, leave_hours = 0,
				        leave_rate = 0;
				var weekExists = false;
				var employee = timeEntrySearch[i].getValue('employee');
				var project = timeEntrySearch[i].getValue('customer');
				var date = nlapiStringToDate(timeEntrySearch[i]
				        .getValue('date'));
				var hours = parseFloat(timeEntrySearch[i]
				        .getValue('durationdecimal'));
				var rate = parseFloat(timeEntrySearch[i].getValue('rate'));
				var item = timeEntrySearch[i].getValue('item');

				if (item == 'OT') { // OT
					ot_hours = hours;
					ot_rate = rate;
				} else if (item == 'Holiday') { // holiday
					holiday_hours = hours;
					holiday_rate = rate;
				} else if (item == 'Leave') { // leave
					leave_hours = hours;
					leave_rate = rate;
				} else { // ST
					st_hours = hours;
					st_rate = rate;
				}

				// week start and week end date calculation
				var weekStartDate_Mid = getWeekStartDate(date, dayDiff);
				var weekStartDate = getWeekStartDate(date, dayDiff);
				var weekEndDate = nlapiAddDays(weekStartDate, 6);
				//nlapiLogExecution('Debug','556weekEndDate',weekEndDate);
				//Function to get check if month weekend date is in next month
				weekEndDate = getWeekEndDate(weekStartDate,weekEndDate);
				//nlapiLogExecution('Debug','after funtion call weekEndDate',weekEndDate);
				var day = weekEndDate.getDay();
				var start_day = weekStartDate.getDay();
				var date_dif = '';
				var flag = '';
				var s_weekStartDate = '';
				var s_weekEndDate = '';
				 nlapiLogExecution('debug', 'start_day', start_day);
				 nlapiLogExecution('debug', 'day', day);
				 nlapiLogExecution('debug', 'date', date);
				 nlapiLogExecution('debug', '568weekEndDate', weekEndDate);
				if(day == 0){
					flag = false;
					s_weekStartDate = nlapiDateToString(weekStartDate, 'date');
					s_weekEndDate = nlapiDateToString(weekEndDate, 'date');
				}
				//else if(start_day == 1 && (nlapiDateToString(date) <= nlapiDateToString(weekEndDate))){
				else if(start_day == 1 && (date <= weekEndDate)){
					flag = true;
					s_weekStartDate = nlapiDateToString(weekStartDate, 'date');
					s_weekEndDate = nlapiDateToString(weekEndDate, 'date');
				}
				else if(date > s_weekEndDate){
					flag = true;
					date_dif = 6 - parseInt(day);
					 weekStartDate = nlapiAddDays(weekEndDate,1);
					 //nlapiLogExecution('debug', 'weekStartDate in else', weekStartDate);
					 weekEndDate =nlapiAddDays(weekStartDate_Mid, 6);
					// nlapiLogExecution('debug', 'weekEndDate in else', weekEndDate);
					s_weekStartDate = nlapiDateToString(weekStartDate, 'date');
					s_weekEndDate = nlapiDateToString(weekEndDate, 'date');
				}
				 nlapiLogExecution('debug', 'weekStartDate', s_weekStartDate);
				 nlapiLogExecution('debug', 'weekEndDate', s_weekEndDate);

				//var s_weekStartDate = nlapiDateToString(weekStartDate, 'date');
				//var s_weekEndDate = nlapiDateToString(weekEndDate, 'date');
				var s_date = nlapiDateToString(date, 'date');
				for (var j = 0; j < timeEntryList.length; j++) {

					if (timeEntryList[j].employee == employee
					        && timeEntryList[j].project == project
					        && timeEntryList[j].d_startdate == s_weekStartDate /*&&  s_date <= s_weekEndDate*/ ) { //&&  s_date <= s_weekEndDate

						if (item == 'OT') { // OT
							timeEntryList[j].ot_hours += hours;
							timeEntryList[j].ot_rate = rate;
						} else if (item == 'Holiday') { // holiday
							timeEntryList[j].holiday_hours += hours;
							timeEntryList[j].holiday_rate = rate;
						} else if (item == 'Leave') { // leave
							timeEntryList[j].leave_hours += hours;
							timeEntryList[j].leave_rate = rate;
						} else { // ST
							timeEntryList[j].st_hours += hours;
							timeEntryList[j].st_rate = rate;
						}
						weekExists = true;
						break;
					}
				}

				if (!weekExists) {
					timeEntryList.push({
					    employee : employee,
					    project : project,
					    st_hours : st_hours,
					    ot_hours : ot_hours,
					    leave_hours : leave_hours,
					    holiday_hours : holiday_hours,
					    st_rate : st_rate,
					    ot_rate : ot_rate,
					    leave_rate : leave_rate,
					    holiday_rate : holiday_rate,
					    startdate : weekStartDate,
					    enddate : weekEndDate,
					    d_startdate : nlapiDateToString(weekStartDate, 'date'),
					    d_enddate : nlapiDateToString(weekEndDate, 'date')
					});
					//var end_last = weekEndDate ;
				}
			}
		}

		var updatedTimeEntryList = [];

		// remove entries where ST and OT hours both are zero
		timeEntryList.forEach(function(time) {

			if (time.st_hours > 0 || time.ot_hours > 0) {
				updatedTimeEntryList.push(time);
			}
		});

		return updatedTimeEntryList;
	} catch (err) {
		nlapiLogExecution('ERROR', 'getTimeEntryWeekWise', err);
		throw err;
	}
}

function getWeekEndDate(startdate,enddate){
	try{
	//	var n_st_date = nlapiStringToDate(startdate);
		//var n_end_date = nlapiStringToDate(enddate);
		nlapiLogExecution('Debug','getWeekEndDate startdate',startdate);
		nlapiLogExecution('Debug','getWeekEndDate enddate',enddate);
		
		var st_date_month = startdate.getMonth() + 1;
		var end_date_month = enddate.getMonth() + 1;
		nlapiLogExecution('Debug','getWeekEndDate st_date_month',st_date_month);
		nlapiLogExecution('Debug','getWeekEndDate end_date_month',end_date_month);
		var lastDay = '';
		if(parseInt(st_date_month) != parseInt(end_date_month)){
			lastDay = new Date(startdate.getFullYear(), startdate.getMonth()+1, 0);
			nlapiLogExecution('Debug','if block lastDay',lastDay);
		}
		else{
			lastDay = enddate;
			nlapiLogExecution('Debug','else block lastDay',lastDay);
		}
		return lastDay;
	}
	catch (err) {
		nlapiLogExecution('ERROR', 'getWeekEndDate', err);
		throw err;
	}
}
function getTimeEntryMonthWise(projectId, monthDay) {
	try {
		var timeEntryList = [];

		var timeEntrySearch = searchRecord('timebill', 3749,
		        [ new nlobjSearchFilter('customer', null, 'anyof', projectId) ]);//repalced record type and searched id by praveena on 09-04-2020

		if (timeEntrySearch) {

			var ot_hours = 0, ot_rate = 0, st_hours = 0, st_rate = 0, holiday_hours = 0, holiday_rate = 0, leave_hours = 0, leave_rate = 0;

			for (var i = 0; i < timeEntrySearch.length; i++) {
				ot_hours = 0, ot_rate = 0, st_hours = 0, st_rate = 0,
				        holiday_hours = 0, holiday_rate = 0, leave_hours = 0,
				        leave_rate = 0;

				var monthExist = false;
				var employee = timeEntrySearch[i].getValue('employee');
				var project = timeEntrySearch[i].getValue('customer');
				var date = nlapiStringToDate(timeEntrySearch[i]
				        .getValue('date'));
				var hours = parseFloat(timeEntrySearch[i]
				        .getValue('durationdecimal'));
				var rate = parseFloat(timeEntrySearch[i].getValue('rate'));
				var item = timeEntrySearch[i].getValue('item');

				if (item == 'OT') { // OT
					ot_hours = hours;
					ot_rate = rate;
				} else if (item == 'Holiday') { // holiday
					holiday_hours = hours;
					holiday_rate = rate;
				} else if (item == 'Leave') { // leave
					leave_hours = hours;
					leave_rate = rate;
				} else { // ST
					st_hours = hours;
					st_rate = rate;
				}

				nlapiLogExecution('debug', 'date', date);

				// start and end date calculation
				var monthStartDate = getMonthStartDate(date, monthDay);
				var monthEndDate = getMonthEndDate(monthStartDate);

				nlapiLogExecution('debug', 'monthStartDate', monthStartDate);
				nlapiLogExecution('debug', 'monthEndDate', monthEndDate);

				var s_monthStartDate = nlapiDateToString(monthStartDate, 'date');
				var s_monthEndDate = nlapiDateToString(monthEndDate, 'date');

				// loop and insert it in the time sheet list
				for (var j = 0; j < timeEntryList.length; j++) {

					if (timeEntryList[j].employee == employee
					        && timeEntryList[j].project == project
					        && timeEntryList[j].d_startdate == s_monthStartDate) {

						if (item == 'OT') { // OT
							timeEntryList[j].ot_hours += hours;
							timeEntryList[j].ot_rate = rate;
						} else if (item == 'Holiday') { // holiday
							timeEntryList[j].holiday_hours += hours;
							timeEntryList[j].holiday_rate = rate;
						} else if (item == 'Leave') { // leave
							timeEntryList[j].leave_hours += hours;
							timeEntryList[j].leave_rate = rate;
						} else { // ST
							timeEntryList[j].st_hours += hours;
							timeEntryList[j].st_rate = rate;
						}

						monthExist = true;
						break;
					}
				}

				if (!monthExist) {
					timeEntryList
					        .push({
					            employee : employee,
					            project : project,
					            st_hours : st_hours,
					            ot_hours : ot_hours,
					            leave_hours : leave_hours,
					            holiday_hours : holiday_hours,
					            st_rate : st_rate,
					            ot_rate : ot_rate,
					            leave_rate : leave_rate,
					            holiday_rate : holiday_rate,
					            startdate : s_monthStartDate,
					            enddate : s_monthEndDate,
					            d_startdate : nlapiDateToString(monthStartDate,
					                    'date'),
					            d_enddate : nlapiDateToString(monthEndDate,
					                    'date')
					        });
				}
			}
		}

		var updatedTimeEntryList = [];

		// remove entries where ST and OT hours both are zero
		timeEntryList.forEach(function(time) {

			if (time.st_hours > 0 || time.ot_hours > 0) {
				updatedTimeEntryList.push(time);
			}
		});

		return updatedTimeEntryList;
	} catch (err) {
		nlapiLogExecution('ERROR', 'getTimeEntryMonthWise', err);
		throw err;
	}
}

function getMonthEndDate(currentDate, monthDay) {
	return nlapiAddDays(nlapiAddMonths(currentDate, 1), -1);
}

function getMonthStartDate(currentDate, monthDay) {
	var month = currentDate.getMonth() + 1;
	var year = currentDate.getFullYear();
	var s_startDate = month + "/" + monthDay + "/" + year;
	var d_startDate = nlapiStringToDate(s_startDate);
	return d_startDate;
}

function getWeekStartDate(currentDate, dayDiff) {
	//nlapiLogExecution('Debug','currentDate',currentDate);
	var daysFromSunday = currentDate.getDay();
	//nlapiLogExecution('Debug','inFunctiondaysFromSunday',daysFromSunday);
	var sundayOfWeek = nlapiAddDays(currentDate, -daysFromSunday);
	//nlapiLogExecution('Debug','inFunction_sundayOfWeek',sundayOfWeek);
	var cycleWeekStartDate = nlapiAddDays(sundayOfWeek, dayDiff);
	//nlapiLogExecution('Debug','cycleWeekStartDate',cycleWeekStartDate);
	if (cycleWeekStartDate > currentDate) {
		cycleWeekStartDate = nlapiAddDays(cycleWeekStartDate, -7);
	}
	//nlapiLogExecution('Debug','afterifcycleWeekStartDate',cycleWeekStartDate);
	return cycleWeekStartDate;
}

function refreshCustomer() {
	var customer = nlapiGetFieldValue('custpage_billing_customer');

	if (customer) {
		var url = nlapiResolveURL('SUITELET',
		        'customscript_sut_auto_invoicing_weekly',
		        'customdeploy_sut_auto_invoicing_weekly')
		        + "&customer=" + customer;
		window.location = url;
	} else {
		alert('Select All Mandatory Fields');
	}
}

function goBack() {
	var url = nlapiResolveURL('SUITELET',
	        'customscript_sut_auto_invoicing_weekly',
	        'customdeploy_sut_auto_invoicing_weekly');
	window.location = url;
}