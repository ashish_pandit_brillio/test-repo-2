/**
 * Create a new level change record and end the last one
 * 
 * Version Date Author Remarks 1.00 02 Jun 2016 Nitish Mishra
 * 
 */

function userEventAfterSubmit(type) {
	try {
		var employeeId = nlapiGetRecordId();
		var newLevel = nlapiGetFieldValue('employeestatus');
		var hireDate = nlapiGetFieldValue('hiredate');
		var newLwd = nlapiGetFieldValue('custentity_lwd');

		if (type == 'edit' || type == 'xedit') {
			var oldRecord = nlapiGetOldRecord();
			var oldLevel = oldRecord.getFieldValue('employeestatus');

			if (newLevel != oldLevel) {
				// get the running level change record
				var changeSearch = nlapiSearchRecord(
				        'customrecord_level_change_history', null, [
				                new nlobjSearchFilter(
				                        'custrecord_lch_employee', null,
				                        'anyof', employeeId),
				                new nlobjSearchFilter('custrecord_lch_to_date',
				                        null, 'isempty') ]);

				var currentDate = new Date();

				if (changeSearch) {
					// update the running record to date as yesterday
					var yesterday = nlapiAddDays(currentDate, -1);
					nlapiSubmitField('customrecord_level_change_history',
					        changeSearch[0].getId(), 'custrecord_lch_to_date',
					        nlapiDateToString(yesterday, 'date'));
					nlapiLogExecution('debug', 'updated running entry');
				} else {
					nlapiLogExecution('debug', 'no previous entry');
				}

				// create a new entry
				var rec = nlapiCreateRecord('customrecord_level_change_history');
				rec.setFieldValue('custrecord_lch_employee', employeeId);
				rec.setFieldValue('custrecord_lch_level', newLevel);
				rec.setFieldValue('custrecord_lch_from_date',
				        nlapiDateToString(currentDate, 'date'));
				nlapiSubmitRecord(rec);
				nlapiLogExecution('debug', 'new record created');
			} else if (newLwd != oldRecord.getFieldValue('custentity_lwd')) {

				// get the running level change record
				var changeSearch = nlapiSearchRecord(
				        'customrecord_level_change_history', null, [
				                new nlobjSearchFilter(
				                        'custrecord_lch_employee', null,
				                        'anyof', employeeId),
				                new nlobjSearchFilter('custrecord_lch_to_date',
				                        null, 'isempty') ]);
				if (changeSearch) {
					// update the running record to date as yesterday
					nlapiSubmitField('customrecord_level_change_history',
					        changeSearch[0].getId(), 'custrecord_lch_to_date',
					        newLwd);
					nlapiLogExecution('debug', 'updated running entry');
				} else {
					nlapiLogExecution('debug', 'no previous entry');
				}
			}
		} else if (type == 'create') {
			var rec = nlapiCreateRecord('customrecord_level_change_history');
			rec.setFieldValue('custrecord_lch_employee', employeeId);
			rec.setFieldValue('custrecord_lch_level', newLevel);
			rec.setFieldValue('custrecord_lch_from_date', hireDate);
			nlapiSubmitRecord(rec);
			nlapiLogExecution('debug', 'new record created');
		}
	} catch (err) {
		nlapiLogExecution('ERROR', 'userEventAfterSubmit', err);
		throw err;
	}
}
