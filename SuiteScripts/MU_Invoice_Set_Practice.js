/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       27 Nov 2014     amol.sahijwani
 *
 */

/**
 * @param {String} recType Record type internal id
 * @param {Number} recId Record internal id
 * @returns {Void}
 */
function massUpdate(recType, recId) {
	var start = new Date().getTime();
	
	var record = nlapiLoadRecord(recType, recId);
	
	var customerId = record.getFieldValue('entity');
	
	var vertical = nlapiLookupField('customer', customerId, 'custentity_vertical');
	
	var i_billable_time_count = record.getLineItemCount('time');
	
	var recordChanged = false;
	
	var context = nlapiGetContext();
	
	var usage = 100;
	
	var PracticeUpdated = 0;
	
	var VerticalUpdated = 0;
	
	var currentEmployeeId = null;
	
	var strStatus = '';
	
	var practiceUpdatedForEmployee = 0;
	
	var verticalsUpdatedForEmployee = 0;
	
	var employeeData = new Array();
	
	for (var i = 1; i <= i_billable_time_count; i++)
		{
			var isApply = record.getLineItemValue('time','apply',i);
			
			var item	=	record.getLineItemValue('time','item',i);
			
			if(isApply == 'T' && item == '2425')
				{
					var employeeId = record.getLineItemValue('time', 'employee', i);
					
					var employeeIndx = getEmployeeIndx(employeeData, employeeId);
					
					if(employeeIndx == null && employeeId != null)
						{
							var employeeDepartment = nlapiLookupField('employee',employeeId,'department');
							
							var objEmployee = {mEmpId:employeeId,mEmpDept:employeeDepartment,mP:0,mV:0};
							
							employeeIndx = employeeData.length;
							
							employeeData[employeeIndx] = objEmployee;
						}
					
					if(record.getLineItemValue('time', 'class', i) == null)
						{
							record.setLineItemValue('time', 'class', i, vertical);
							
							recordChanged = true;
							
							if(employeeIndx != null)
								{
									employeeData[employeeIndx].mV++;
								}
						}
					
					if(record.getLineItemValue('time', 'department', i) == null)
						{
							if(employeeIndx != null)
								{
									
									
									record.setLineItemValue('time', 'department', i, employeeData[employeeIndx].mEmpDept);
							
									recordChanged = true;
									
									employeeData[employeeIndx].mP++;
								}
						}
				}
			
			var remainingUsage = context.getRemainingUsage();
			
			if(remainingUsage < 50)
				{
					nlapiLogExecution('ERROR', 'remaining usage', context.getRemainingUsage());
					return;
				}
		}
	
	for(var i = 0; i < employeeData.length; i++)
		{
			strStatus += 'Employee: ' + employeeData[i].mEmpId + ',Dept:'+employeeData[i].mEmpDept+' (P:'+employeeData[i].mP+', V:'+employeeData[i].mV+')<br/>';
		}
	
	if(recordChanged)
		{
			nlapiSubmitRecord(record);
		}
	
	var end = new Date().getTime();
	var time = end - start;
	
	time = Math.round(time/1000);
	
	var minutes = Math.floor(time / 60);
	
	var seconds = time - minutes * 60;
	
	usage = 1000 - context.getRemainingUsage();
	
	nlapiLogExecution('DEBUG','Invoice done: ' + recId + ', Usage: ' + usage + ' (' + minutes + 'm ' + seconds + 's), Vertical: ' + vertical, strStatus);
}

function getEmployeeIndx(a_Employee, i_employee_id)
{
	for(var i = 0; i < a_Employee.length; i++)
		{
			if(a_Employee[i].mEmpId == i_employee_id)
				{
					return i;
				}
		}
	
	return null;
	
}