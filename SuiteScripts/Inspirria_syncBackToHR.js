/**
 * @NApiVersion 2.0
 * @NScriptType Restlet
 * @NModuleScope SameAccount
 */
define(['N/record','N/search','N/format' ],function(record,search,format){
	function SyncBacktoHR(datain) 
	{
		log.debug('Start');
		var date = datain.lastmodifieddate;
		log.debug('date',date);
		if(date)
		{
			var Timestamp = format.format({
				value: new Date(),
				type: format.Type.DATETIME,
				//timezone: format.Timezone.ASIA_MUSCAT
			});
			var stampSplit = Timestamp.split(" ");
			var datesplit = stampSplit[0].split("/");
			var Time = stampSplit[1].split(":");
			var timeStamp = datesplit[0]+"/"+datesplit[1]+"/"+datesplit[2]+" "+Time[0]+":"+Time[1]+" "+stampSplit[2]
			log.debug('timeStamp',timeStamp);

			var loadSearch = getSyncBacktoHR(date,timeStamp);

			return {
				"status":"success",
				"lastmodifieddate":timeStamp,
				"data":loadSearch
			}
		}
		else
		{
			return {
				"status":"error",
				"message":"Last modified date required."
			}
		}
	}
	function getSyncBacktoHR(fromDate , toDate)
	{
		var ActualSearch = search.load({
			id: 'customsearch_synchbacktosr'
		});

		ActualSearch.filters.push(search.createFilter({
			name : 'lastmodifieddate',
			operator : search.Operator.WITHIN,
			values : [fromDate,toDate]
		}));
		/*ActualSearch.filters.push(search.createFilter({
			name : 'date',
			join : 'systemnotes',
			operator : search.Operator.WITHIN,
			values : [fromDate,toDate]
		}));*/

		var results = [];
		var count = 0;
		var pageSize = 1000;
		var start = 0;
		do {
			var subresults = ActualSearch.run().getRange({
				start: start,
				end: start + pageSize
			});

			results = results.concat(subresults);
			count = subresults.length;
			start += pageSize;
		} while (count == pageSize);
		log.debug('loadActualSearch:', results.length);

		var ActualData = new Array();
		for (var res = 0; res < results.length; res++)
		{
			var email = results[res].getValue({
				name: "email",
				summary: "GROUP",
				label: "Email"
			});
			email = (email == "- None -") ? null : email;
			var AssignmentNumber = results[res].getValue({
				name: "custentity_assignment_num",
				summary: "GROUP",
				label: "AssignmentNumber"
			});
			AssignmentNumber = (AssignmentNumber == "- None -") ? null : AssignmentNumber;
			var Legal_Entity_Fusion = results[res].getValue({
				name: "custentity_legal_entity_fusion",
				summary: "GROUP",
				label: "Legal Entity Fusion"
			});
			Legal_Entity_Fusion = (Legal_Entity_Fusion == "- None -") ? null : Legal_Entity_Fusion;
			var Title = results[res].getValue({
				name: "custentity_job_code",
				summary: "GROUP",
				label: "Job Code"
			});
			Title = (Title == "- None -") ? null : Title;
			var Employee_Status = results[res].getText({
				name: "employeestatus",
				summary: "GROUP",
				label: "Employee Status"
			});
			Employee_Status = (Employee_Status == "- None -") ? null : Employee_Status;
			var Parent_Practice = results[res].getText({
				name: "custrecord_parent_practice",
				join: "department",
				summary: "GROUP",
				label: "Parent Practice"
			});
			Parent_Practice = (Parent_Practice == "- None -") ? null : Parent_Practice;
			var departmentnohierarchy = results[res].getValue({
				name: "departmentnohierarchy",
				summary: "GROUP",
				label: "Practice (no hierarchy)"
			});
			//  	departmentnohierarchy = (departmentnohierarchy == "- None -") ? null : departmentnohierarchy;
			var Brillio_Location = results[res].getValue({
				name: "custentity_list_brillio_location_e",
				summary: "GROUP",
				label: "Brillio Location"
			});
			Brillio_Location = (Brillio_Location == "- None -") ? null : Brillio_Location;
			var Person_Type = results[res].getText({
				name: "custentity_employeetype",
				summary: "GROUP",
				label: "Person Type"
			});
			Person_Type = (Person_Type == "- None -") ? null : Person_Type;
			var Reporting_Manager = results[res].getValue({
				 name: "custentity_fusion_empid",
				 join: "CUSTENTITY_REPORTINGMANAGER",
				 summary: "GROUP",
				 label: "Reporting Manager Fusion Id"
			});
			Reporting_Manager = (Reporting_Manager == "- None -") ? null : Reporting_Manager;
			
			var Reporting_Manager_mail = results[res].getValue({
				  name: "email",
				 join: "CUSTENTITY_REPORTINGMANAGER",
				 summary: "GROUP",
				 label: "Reporting Manager Mail"
			});
			Reporting_Manager_mail = (Reporting_Manager_mail == "- None -") ? null : Reporting_Manager_mail;
			
			var Fusion_Employee_Id = results[res].getValue({
				name: "custentity_fusion_empid",
				summary: "GROUP",
				label: "Fusion Employee Id"
			});
			Fusion_Employee_Id = (Fusion_Employee_Id == "- None -") ? null : Fusion_Employee_Id;
			var Function = results[res].getText({
				name: "custentity_emp_function",
				summary: "GROUP",
				label: "Function"
			});

			Function = (Function == "- None -") ? null : Function;
			var HRBP_Id = results[res].getValue({
				 name: "custentity_fusion_empid",
				 join: "CUSTENTITY_EMP_HRBP",
				 summary: "GROUP",
				 label: "HRBP Fusion Id"
			});
			HRBP_Id = (HRBP_Id == "- None -") ? null : HRBP_Id;
			
			var HRBP_Mail = results[res].getValue({
				 name: "email",
				 join: "CUSTENTITY_EMP_HRBP",
				 summary: "GROUP",
				 label: "HRBP Mail"
			});
			HRBP_Mail = (HRBP_Mail == "- None -") ? null : HRBP_Mail;
			
			var Subsidiary = results[res].getValue({
				 name: "subsidiary",
				 summary: "GROUP",
				 label: "Subsidiary"
			});
			Subsidiary = (Subsidiary == "- None -") ? null : Subsidiary;
			
			if(_logValidation(Subsidiary)){
				
				Subsidiary = SubMapping(Subsidiary);
			}
			
			ActualData.push({
				"Assignment_Number": AssignmentNumber,
				"Business_Unit": Subsidiary,
				"Designation": Title,
				"Level": Employee_Status,
				"practice": Parent_Practice,
				"Department": departmentnohierarchy,
				"Base_Location": Brillio_Location,
				"EmploymentType": Person_Type,
				"Manager_Id": Reporting_Manager,
				"Manager_mail": Reporting_Manager_mail,
				"HRBP_Id": HRBP_Id,
				"HRBP_Mail": HRBP_Mail,
				"Employee_Mail": email,
				"Employee_ID": Fusion_Employee_Id,
				"Function": Function,
				"Legal_Entity": Legal_Entity_Fusion,
				"Employement_Stage": "Employee"
			});
		}
		return ActualData;

	}
	function _logValidation(value) {
		if (value != null && value != '' && value != undefined && value.toString() != 'NaN' && value != NaN) {
			return true;
		} else {
			return false;
		}
	}
	
	function SubMapping(name)
	{
		if (name =='3')
		{
			return "Brillio Technologies India BU"
		}
		else if(name == '2')
		{
			return "Brillio LLC United States BU"
		}
		else if(name == '10')
		{
			return "Brillio Canada Inc BU"
		}
		else if(name == '7')
		{
			return "BRILLIO UK LIMITED BU"
		}
		else if(name == '21')
		{
			return "Cognetik Corp"
		}else
		{
			return null;
		}
	}
	

	return {
		'post': SyncBacktoHR        
	};
});