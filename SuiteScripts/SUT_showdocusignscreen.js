// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
/*
   	Script Name: SUT_showdocusignscreen.js
	Author:		 Rakesh K 
	Company:	 Brillio 
	Date:		 29-02-2017
	Version: 	 1.0
	Description: When user will click on edit button at that time all records will come.


	Script Modification Log:

	-- Date --			-- Modified By --				--Requested By--				-- Description --



Below is a summary of the process controls enforced by this script file.  The control logic is described
more fully, below, in the appropriate function headers and code blocks.


     SUITELET
		- suiteletFunction(request, response)


     SUB-FUNCTIONS
		- The following sub-functions are called by the above core functions in order to maintain code
            modularization:

               - NOT USED

*/
}
// END SCRIPT DESCRIPTION BLOCK  ====================================



// BEGIN GLOBAL VARIABLE BLOCK  =====================================
{
	//  Initialize any Global Variables, in particular, debugging variables...


}
// END GLOBAL VARIABLE BLOCK  =======================================




var strVar= '';
var s_tablestring = '';
var l_link_URL = '';
// BEGIN SUITELET ==================================================

function suiteletFunction_edit_mode_showdocu(request, response){
	
	
	try{
		if (request.getMethod() == 'GET'){
		
			var objUser = nlapiGetContext();
			var o_values  = new Object();
			var i_employee_id = objUser.getUser();
			var i_roleID = objUser.getRole();
			var s_role	=	getRoleName(i_employee_id, objUser.getRole());
			var o_employee_data	=	nlapiLookupField('employee', i_employee_id, ['firstname', 'lastname', 'employeestatus']);
			//var o_json = getrequiterdata(i_employee_id);
			o_values['username']	=	o_employee_data.firstname + ' ' + o_employee_data.lastname;
			o_values['role']		=	s_role;
			
			
			var lastNextID = request.getParameter('lastnextrecordID');
			var pid = request.getParameter('pid');
			var edit = request.getParameter('edit');
			var email = request.getParameter('email');
			
			if(pid && edit == 1){
				
				var reqid = nlapiDecrypt(pid, 'base64');
				nlapiLogExecution('debug', 'reqid', reqid);
				
				var o_customRecord = nlapiLoadRecord('customrecord_recruiters_details', reqid);
				if(_logValidation(o_customRecord)){
					
					var ObjectVendor = nlapiCreateRecord('vendor');
					var v_name = o_customRecord.getFieldValue('custrecord_vendor_name');
					if(_logValidation(v_name)){
						
						ObjectVendor.setFieldValue('companyname',v_name); // vendor name 
					}
					
					var f_id = o_customRecord.getFieldValue('custrecord_federal_id');
					if(_logValidation(f_id)){
						
						ObjectVendor.setFieldValue('taxidnum',f_id); // FEDERAL ID
					}
					
					ObjectVendor.setFieldValue('category',6); // Vendor
					ObjectVendor.setFieldValue('custentity_subcategory',3); //Subcontractor
					ObjectVendor.setFieldValue('subsidiary',2); // subsidiary
					ObjectVendor.setFieldValue('custentity_w9status',3); // other 
					
					var s_phone = o_customRecord.getFieldValue('custrecord_phone_number');
						
					if(_logValidation(s_phone)){
							ObjectVendor.setFieldValue('phone',s_phone); // Vendor
					}
					
					var admin_email =o_customRecord.getFieldValue('custrecord_email_for_administration'); 
					if(_logValidation(admin_email)){
						
						ObjectVendor.setFieldValue('email',admin_email); // Vendor
					}
				}
				o_customRecord.setFieldValue('custrecord_approval_status_recruiters',1);
				
				
				var s_adminEmail = o_customRecord.getFieldValue('custrecord_email_for_administration');
				var i_recruiter = o_customRecord.getFieldValue('custrecord_netsuite_role');
				var s_reqEmail = nlapiLookupField('employee',i_employee_id,'email');
				var s_vendor_name = o_customRecord.getFieldValue('custrecord_vendor_name');
				var s_contract_type = o_customRecord.getFieldText('custrecord_contract_type');
				
				var subject = "Your request for ("+s_contract_type+") has been approved for ("+s_vendor_name+").";

				var strVar="";
				strVar += "<html>";
				strVar += "<body>";
				strVar += "<p style=font:small-caption>";
				strVar += "";
				strVar += "Hi,<br>";
				strVar += "Your request has been approved<br>";
				
				strVar += "Thank you.";
				strVar += "</p>";
				strVar += '<p><b>NOTE:</b> This is a system generated email. Please do not reply. In case of any queries or concerns, please reach out to contracts@brillio.com</p>';
				strVar += "<\/body>";
				strVar += "<\/html>";
				
				var ccList = [];
				ccList.push(s_adminEmail);
				ccList.push('contracts@brillio.com');
				
				
		        nlapiSendEmail(442, s_reqEmail, subject, strVar, ccList);
				
				var vendorID = nlapiSubmitRecord(ObjectVendor);
				if(vendorID){
					//nlapiSubmitField('customrecord_recruiters_details', reqid, 'custrecord_approval_status_recruiters', 1);
				}
				var customRecordID = nlapiSubmitRecord(o_customRecord);
				nlapiLogExecution('debug', 'vendorID', vendorID);
				nlapiLogExecution('debug', 'customRecordID', customRecordID);
				response.sendRedirect('SUITELET','customscript_sut_showdocusignscreen','customdeploy1',null,null);
			}
			if(pid && email == 'T'){
				
				var reqid = nlapiDecrypt(pid, 'base64');
				nlapiLogExecution('debug', 'reqid', reqid);
								
				var o_customRecord = nlapiLoadRecord('customrecord_recruiters_details', reqid);
				if(o_customRecord){
					
					var s_adminEmail = o_customRecord.getFieldValue('custrecord_email_for_administration');
					var s_reqEmail = o_customRecord.getFieldValue('custrecord_email_for_requirements');
					var i_Requester_inetenal_id = o_customRecord.getFieldValue('custrecord_netsuite_role');
					
					var email_link = "https://forms.na1.netsuite.com/app/site/hosting/scriptlet.nl?script=1193&deploy=1&compid=3883006&h=eff4ebc8b4246c802237&pid="+reqid;
					var v_name= o_customRecord.getFieldValue('custrecord_vendor_name');
					
					var ccList = [];
					ccList.push(s_reqEmail);
					ccList.push('contracts@brillio.com');
					
					var strVar = '';
					   strVar += '<html>';
					   strVar += '<body>';
					   strVar += '<p>Hello, </p>'+v_name;
					   strVar += '<p>Kindly upload the required document for the Vendor using link given below.</p>';
					   
					   strVar += '<p>Certificate of Insurance listing Brillio, LLC. 3rd Floor, 110 Allen Road, Liberty Corner, Basking Ridge, NJ 07920 as "Certificate Holder" as well as "Additional Insured". (Required levels are found in the Consulting Services Agreement and Client Addendum)</p>';
					   strVar += '<p>Page 1 of your company’s Articles of Incorporation / Certificate of Formation.</p>';

					   
					   strVar += '<a href='+email_link+'>Documents Upload</a>';
					   
					   strVar += '<p>Thanks & Regards,</p>';
					   strVar += '<p>Team IS</p>';
					  
					
					   strVar += '<p><b>NOTE:</b> This is a system generated email. Please do not reply. In case of any queries or concerns, please reach out to contracts@brillio.com</p>';
					   					   
					   strVar += '</body>';
					   strVar += '</html>';
					   
					 
					   var a_emp_attachment = new Array();
						a_emp_attachment['entity'] = i_Requester_inetenal_id;
					 
					//var email_link = "https://forms.sandbox.netsuite.com/app/site/hosting/scriptlet.nl?script=1190&deploy=1&compid=3883006&h=547e10f56d6c23491ccd&pid="+recid;
				}
				
				nlapiSendEmail(442, s_adminEmail, 'Please upload below documents to complete the vendor empanelment.', strVar, ccList, null, a_emp_attachment);
			}
			
			var last_pre_rec_id = request.getParameter('last_prev_id');
			var mode = request.getParameter('mode');
			var counter = request.getParameter('counter');
			
			if(lastNextID != null ){
				var o_json = getrequiterdata(i_employee_id,lastNextID,last_pre_rec_id,'next');
			}
			else if(last_pre_rec_id){
				var o_json = getrequiterdata(i_employee_id,lastNextID,last_pre_rec_id,'prev');
			}
			else{
				var o_json = getrequiterdata(i_employee_id,lastNextID,last_pre_rec_id,'diff');
			}
			
			 var recordid = '';
			 var s_vendor_name = '';
			 var  s_federal_id= '';
			 var s_sr_no = '';
			 var s_vendor_contracttype = '';
			 var s_vendor_emailaddress = '';
			 var s_vendor_contactperson = '';
			 var s_vendor_status = '';
			
			if(mode == '' || mode == null){
				var finalvalue = parseInt(1) + parseInt(counter);
				o_values['counter'] = finalvalue;
				
			}
			if(mode == 'next'){
				var finalvalue = parseInt(1) + parseInt(counter);
				o_values['counter'] = finalvalue;
			}
			if(mode == 'prev' || counter =='' ||counter == null){
				var finalvalue = parseInt(1);
				o_values['counter'] = finalvalue;
			}
			if(mode == 'prev' && counter !='' && counter != null){
				var finalvalue = parseInt(1);
				o_values['counter'] = finalvalue;
			}
			o_values['record_found'] = o_json.length;
			
			 	var s_tablestring="";
				s_tablestring += "<html>";
				
				s_tablestring += "<table class=\"table\">";
				s_tablestring += "	<thead>";
				s_tablestring += "	<tr>";
				s_tablestring += "	<th>SR.NO<\/th>";
				s_tablestring += "	<th>Vendor Name<\/th>";
				s_tablestring += "	<th>Federal ID<\/th>";
				s_tablestring += "	<th style=\"display:none\">Record ID<\/th>";
				s_tablestring += "	<th>Contract Type<\/th>";
				s_tablestring += "	<th>Contact Person<\/th>";
				s_tablestring += "	<th>Email Address<\/th>";
				s_tablestring += "	<th>Sent Link<\/th>";
				s_tablestring += "	<th>Status<\/th>";
				s_tablestring += "<th><\/th>";
				s_tablestring += "	<\/tr>";
				s_tablestring += "	<\/thead>";
			
				
			 for(var key in o_json){
				
				 if(key <= 3){
					 if(mode == 'prev'){
						 var newid = o_json[0].record_id;
						 nlapiLogExecution('debug','mode newid' , newid);
					 }else{
						   var newid = o_json[key].record_id;
						   nlapiLogExecution('debug','else newid' , newid);
						 } 
				 
					 var find_prev_id = o_json[0].record_id;
				 //	var newid = o_json[key].record_id;
					//var recordid = o_json[key].record_id ;
					 var s_vendor_name = o_json[key].vendor_name ;
					var s_federal_id = o_json[key].federal_id;
					var s_sr_no = o_json[key].sr_no ;
					var s_vendor_contracttype = o_json[key].vendor_contracttype ;
					var s_vendor_emailaddress = o_json[key].vendor_emailaddress ;
					var s_vendor_contactperson = o_json[key].vendor_contactperson ;
					var s_vendor_status = o_json[key].vendor_status ;
					var s_send_docu =  o_json[key]._senddocument;
					var recid = nlapiEncrypt(newid, 'base64');
					//l_link_URL = "<a href=https://system.sandbox.netsuite.com/app/site/hosting/scriptlet.nl?script=1156&deploy=1&pid="+recid+"&edit=1>Edit<\/a><br>";
					// approve link 
					
					s_tablestring += "<script type='text/javascript'>";
					s_tablestring += "function confirm_delete() {";
					s_tablestring += "  return confirm('Do you really wish to Approve Vendor?');";
					//s_tablestring += "  alert('yes/no:- '+yes_no+')";
					s_tablestring += "}";
					s_tablestring += "</script>";
					
					l_link_URL= "<a href='https://3883006.app.netsuite.com/app/site/hosting/scriptlet.nl?script=1196&deploy=1&pid="+recid+"&edit=1'><img src='https://3883006.app.netsuite.com/core/media/media.nl?id=2046241&c=3883006&h=7cadf2851d8fd7436eef' alt='Smiley face' onclick='return confirm_delete()'></a>";
				 	l_open_url = "<a href='https://3883006.app.netsuite.com/app/common/custom/custrecordentry.nl?rectype=427&id="+newid+"&edit'><img src='https://3883006.app.netsuite.com/core/media/media.nl?id=2046242&c=3883006&h=2c130e477011d5bbfee8' alt='Smiley face' ></a>";
					var email_Link = "<a href='https://3883006.app.netsuite.com/app/site/hosting/scriptlet.nl?script=1196&deploy=1&pid="+recid+"&email=T'><img src='https://3883006.app.netsuite.com/core/media/media.nl?id=2046243&c=3883006&h=6de016837bbefa58b784' alt='Smiley face' ></a>";
				 	var edit_link_URL = "<a href=https://3883006.app.netsuite.com/app/site/hosting/scriptlet.nl?script=1190&deploy=1&pid="+recid+"&edit=1>Edit<\/a><br>";
					s_tablestring += "<tbody>";
					s_tablestring += "<tr>";
					s_tablestring += "<td>"+s_sr_no+"<\/td>";
					s_tablestring += "<td>"+s_vendor_name+"<\/td>";
					s_tablestring += "<td>"+s_federal_id+"<\/td>";
					s_tablestring += "<td style=\"display:none\">"+recordid+"<\/td>";
					s_tablestring += "<td>"+s_vendor_contracttype+"<\/td>";
					s_tablestring += "<td>"+s_vendor_contactperson+"<\/td>";
					s_tablestring += "<td>"+s_vendor_emailaddress+"<\/td>";
					s_tablestring += "<td>"+s_send_docu+"<\/td>";
					s_tablestring += "<td>"+s_vendor_status+"<\/td>";
					s_tablestring += "<td>"+l_link_URL+"<\/td>";
					s_tablestring += "<td>"+l_open_url+"<\/td>";
					s_tablestring += "<td>"+email_Link+"<\/td>";
					s_tablestring += "<td>"+edit_link_URL+"<\/td>";
					
					s_tablestring += "<\/tr>";
					s_tablestring += "	<\/tbody>";
				
				 }
			 }
			 s_tablestring += "<\/table>";
			// l_link_URL+="<a href=https://system.sandbox.netsuite.com/app/site/hosting/scriptlet.nl?script=1156&deploy=1&pid=&edit=1>Edit<\/a><br>";
			
			 
			 if(o_json.length <=1 || o_json.length == null ){
					
					o_values['btn_disable_next'] = 'disabled';
					o_values['s_href_next'] = '#';
					o_values['btn_disable_prev'] = 'disabled';
				
				}
				if(o_json.length <3){
					
					o_values['btn_disable_next'] = 'disabled';
					o_values['s_href_next'] = '#';
				}
				
				if(newid && o_json.length >=3 ){
					//nlapiLogExecution('debug', 'newid', newid);
				 o_values['s_href_next'] = "https://3883006.app.netsuite.com/app/site/hosting/scriptlet.nl?script=1196&deploy=1&lastnextrecordID="+newid+"&mode=next"+"&counter="+finalvalue;	
					
				}
				if(counter == 2){
					o_values['btn_disable_prev'] = 'disabled';
					o_values['s_href_prev'] = '#';
				}
				//&& mode == 'prev'
				// && mode
				if(find_prev_id  && mode && counter !=2){
					
					o_values['s_href_prev'] = "https://3883006.app.netsuite.com/app/site/hosting/scriptlet.nl?script=1196&deploy=1&last_prev_id="+find_prev_id+"&mode=prev"+"&counter="+finalvalue;
				}	
				if(mode == '' || mode == null){
					o_values['btn_disable_prev'] = 'disabled';
					o_values['s_href_prev'] = '#';
					
				}
					o_values['last_id'] = newid; 
					o_values['last_prev_id'] = newid; 
					o_values['s_table_content'] = strVar; 
					o_values['is_selected'] = 'selected';
					
					
			 nlapiLogExecution('debug', 'l_link_URL', l_link_URL);
			 o_values['employee-name'] = recordid;//o_json[key].record_id;
			 o_values['vendor-name'] = s_vendor_name;// o_json[key].vendor_name;
			 o_values['federal-id'] = s_federal_id;//o_json[key].federal_id;
			 o_values['sr-no'] = s_sr_no; //o_json[key].sr_no;
			 o_values['vendor-contracttype'] = s_vendor_contracttype;
			 o_values['vendor-emailaddress'] =s_vendor_emailaddress;
			 o_values['vendor-contactperson'] =s_vendor_contactperson;
			 o_values['vendor-status'] =s_vendor_status;
			 o_values['s_edit_button'] = strVar; 
			 o_values['is_selected'] = 'selected';
			 o_values['record_found'] = o_json.length;
			 
			 //var view_all = getrequiterdataAllEmployee();
			 //o_values['table_content'] = view_all;
			 
			 var s_new_request_url	=	nlapiResolveURL('SUITELET', 'customscript_sut_recruiters_screen', 'customdeploy1');
			 var s_my_requests_url	=	nlapiResolveURL('SUITELET', 'customscript_sut_my_request_form', 'customdeploy1');
			 var s_new_home_url	=	nlapiResolveURL('SUITELET', 'customscript_vendor_contract_screen', 'customdeploy1');
			 var s_search_URL = nlapiResolveURL('SUITELET', 'customscript_sut_searchvendorinformation', 'customdeploy1');
			 o_values['new_request_url']	=	s_new_request_url;
			 o_values['my_requests_url']	=	s_my_requests_url;
			 o_values['s_search_URL']	=	s_search_URL;
			 o_values['s_new_home_url']	=	s_new_home_url;
			 o_values['table_content'] = s_tablestring;
			 var file = nlapiLoadFile(2046231); 
			 var contents = file.getValue();    //get the contents
			 nlapiLogExecution('debug', 's_tablestring', s_tablestring);
			 contents = replaceValues(contents, o_values);
			 // nlapiLogExecution('debug', 'contents', contents);
			  response.write(contents);
		
		}
		
	}catch(e){
		
		nlapiLogExecution('debug', 'ERROR'+e);
	}




}
// get role name 
function getRoleName(i_user_id, i_role_id)
{
	var a_search_results	=	nlapiSearchRecord('employee', null, [new nlobjSearchFilter('internalid', null, 'anyof', i_user_id), new nlobjSearchFilter('role', null, 'anyof', i_role_id)], new nlobjSearchColumn('role'));
	
	if(a_search_results != null && a_search_results.length == 1)
		{
			return a_search_results[0].getText('role');
		}
	else
		{
			return '';
		}
}
// END SUITELET ====================================================

function replaceValues(content, oValues)
{
	for(param in oValues)
		{
			// Replace null values with blank
			var s_value	=	(oValues[param] == null)?'':oValues[param];
			
			// Replace content
			content = content.replace(new RegExp('{{' + param + '}}','gi'), s_value);
		}
	
	return content;
}


// BEGIN OBJECT CALLED/INVOKING FUNCTION ===================================================
{

	
		

	// get custom record data for particular user 
	function getrequiterdata(i_employee_id,lastNextID,last_pre_rec_id,mode)	{
		
		var temp = 0;
			//nlapiLogExecution('debug', 'getrequiterdata', i_employee_id);
			//nlapiLogExecution('debug', 'lastNextID', lastNextID);
		 	var filters =	new Array();
		 	var columns	=	new Array();
		 	
		 	//if(i_employee_id != null && i_employee_id != '')
		 	{
		 		filters[temp++]	=	new nlobjSearchFilter('custrecord_docusign_process',null,'is','T');
		 		filters[temp++]	=	new nlobjSearchFilter('custrecord_approval_status_recruiters',null,'anyof',2);
		 		//filters[temp++]	=	new nlobjSearchFilter('custrecord_netsuite_role',null,'anyof',i_employee_id);
		 	}
		 	if(_logValidation(lastNextID)){
		 		filters[temp++]	=	new nlobjSearchFilter('internalidnumber',null,'greaterthan',parseInt(lastNextID));
		 	}
		 	if(_logValidation(last_pre_rec_id)){
		 		filters[temp++]	=	new nlobjSearchFilter('internalidnumber',null,'lessthan',parseInt(last_pre_rec_id));
		 	}
		 	if(mode == 'next' || mode == 'diff'){
				//nlapiLogExecution('audit','inside if sort');
				columns[0]	=	new nlobjSearchColumn('internalid').setSort(false);
			}else if(mode == 'prev'){
				
				//nlapiLogExecution('audit','inside else sort');
				columns[0]	=	new nlobjSearchColumn('internalid').setSort(true);
			}
			
			
			columns[1]	=	new nlobjSearchColumn('custrecord_vendor_name');
			columns[2]	=	new nlobjSearchColumn('custrecord_federal_id');
			columns[3]	=	new nlobjSearchColumn('custrecord_email_for_administration'); // email for admin 
			columns[4]	=	new nlobjSearchColumn('custrecord_contract_type'); // contract type
			columns[5]	=	new nlobjSearchColumn('custrecord_contact_person'); // contact person 
			columns[6]	=	new nlobjSearchColumn('custrecord_approval_status_recruiters'); // status 
			columns[7]	=	new nlobjSearchColumn('custrecord_send_email_for_docu_upload'); // status 
			
			
			var searchResults	=	searchRecord('customrecord_recruiters_details', null, filters, columns);
			//nlapiLogExecution('debug', 'searchResults', searchResults.length);
			var a_data	=	new Array();
			//va s_tablestring="";
			var k=1;
			if(searchResults){
				
				for(var i = 0; i < searchResults.length; i++){
					
					var s_record_id	=	searchResults[i].getValue(columns[0]);
					var s_vendor_name	=	searchResults[i].getValue(columns[1]);
					var s_federal_id	=	searchResults[i].getValue(columns[2]);
					var s_emailaddress = searchResults[i].getValue(columns[3]);
					var s_contracttype = searchResults[i].getText(columns[4]);
					var s_contactperson =searchResults[i].getValue(columns[5]);
					var s_status = searchResults[i].getText(columns[6]);
					var s_senddocument = searchResults[i].getValue(columns[7]);
					if(s_senddocument == 'T'){
						s_senddocument = 'Doument Link Send to vendor';
						
					}else{
						s_senddocument = 'NO';
					}
					a_data.push({ 'record_id': s_record_id, '_senddocument':s_senddocument,'vendor_name': s_vendor_name, 'federal_id': s_federal_id, 'sr_no': k,'vendor_contracttype':s_contracttype,'vendor_emailaddress':s_emailaddress,'vendor_contactperson':s_contactperson,'vendor_status':s_status});
					k++;
					}
			}
			
			
			

			return a_data;
		}
	
/*function getrequiterdata(i_employee_id)	{
	 	
		//nlapiLogExecution('debug', 'getrequiterdata', i_employee_id);
	 	var filters =	new Array();
	 	var columns	=	new Array();
	 	//if(i_employee_id != null && i_employee_id != '')
	 	{
	 		
	 	}
	 	filters[0]	=	new nlobjSearchFilter('custrecord_approval_status_recruiters',null,'anyof',2);
		columns[0]	=	new nlobjSearchColumn('internalid');
		columns[1]	=	new nlobjSearchColumn('custrecord_vendor_name');
		columns[2]	=	new nlobjSearchColumn('custrecord_federal_id');
		columns[3]	=	new nlobjSearchColumn('custrecord_email_for_administration'); // email for admin 
		columns[4]	=	new nlobjSearchColumn('custrecord_contract_type'); // contract type
		columns[5]	=	new nlobjSearchColumn('custrecord_contact_person'); // contact person 
		columns[6]	=	new nlobjSearchColumn('custrecord_approval_status_recruiters'); // status 
		
		var searchResults	=	searchRecord('customrecord_recruiters_details', null, filters, columns);
		//nlapiLogExecution('debug', 'searchResults', searchResults);
		var a_data	=	new Array();
		
		var k =1;
		for(var i = 0; i < searchResults.length; i++){
				
				var s_record_id	=	searchResults[i].getValue(columns[0]);
				var s_vendor_name	=	searchResults[i].getValue(columns[1]);
				var s_federal_id	=	searchResults[i].getValue(columns[2]);
				var s_emailaddress = searchResults[i].getValue(columns[3]);
				var s_contracttype = searchResults[i].getText(columns[4]);
				var s_contactperson =searchResults[i].getValue(columns[5]);
				var s_status = searchResults[i].getText(columns[6]);
						
				a_data.push({ 'record_id': s_record_id, 'vendor_name': s_vendor_name, 'federal_id': s_federal_id, 'sr_no': k,'vendor_contracttype':s_contracttype,'vendor_emailaddress':s_emailaddress,'vendor_contactperson':s_contactperson,'vendor_status':s_status});
					
				
				k++;

			}
			

		return a_data;
	}*/


}


// search code 
function searchRecord(recordType, savedSearch, arrFilters, arrColumns,  filterExpression){

	try {
		var search = null;

		// if a saved search is provided, load it and add the filters and
		// columns
		if (isNotEmpty(savedSearch)) {
			search = nlapiLoadSearch(recordType, savedSearch);

			if (isArrayNotEmpty(arrFilters)) {
				search.addFilters(arrFilters);
			}

			if (isArrayNotEmpty(arrColumns)) {
				search.addColumns(arrColumns);
			}

			if (isArrayNotEmpty(filterExpression)) {
				search.setFilterExpression(filterExpression);
			}
		}
		// create a new search
		else {
			search = nlapiCreateSearch(recordType, arrFilters, arrColumns);
		}

		// run search
		var resultSet = search.runSearch();

		// iterate through the search and get all data 1000 at a time
		var searchResultCount = 0;
		var resultSlice = null;
		var searchResult = [];

		do {
			resultSlice = resultSet.getResults(searchResultCount,
			        searchResultCount + 1000);

			if (resultSlice) {

				resultSlice.forEach(function(result) {

					searchResult.push(result);
					searchResultCount++;
				});
			}
		} while (isArrayNotEmpty(resultSlice) && resultSlice.length >= 1000);

		return searchResult;
	} catch (err) {
		nlapiLogExecution('ERROR', 'searchRecord', err);
		throw err;
	}
}

function isEmpty(value) {

	return value == null || value == "" || typeof (value) == undefined;
}

function isNotEmpty(value) {

	return !isEmpty(value);
}

function isArrayEmpty(argArray) {

	return !isArrayNotEmpty(argArray);
}

function isArrayNotEmpty(argArray) {

	return (isNotEmpty(argArray) && argArray.length > 0);
}
function _logValidation(value)  
{
	if(value != null && value != '' && value != undefined && value.toString() != 'NaN' && value != NaN)
	{
		return true;
	}
	else
	{
		return false;
	}
}
// END OBJECT CALLED/INVOKING FUNCTION =====================================================