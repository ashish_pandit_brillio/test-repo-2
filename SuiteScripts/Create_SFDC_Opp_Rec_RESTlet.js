/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       31 Jan 2019     Aazamali Khan	   Restlet to create SFDC 
 * 1.01 	  25 Sep 2019 	  Aazamali Khan	   Updated the code to have the revised startDate and endDate
 * 1.02		  09 Oct 2019	  Aazamali Khan    added the code to have data for SET fields
 */
/**
 * @param {Object} dataIn Parameter object
 * @returns {Object} Output object
 */
 function getRESTlet(dataIn) {
    try {
        nlapiLogExecution("DEBUG", "dataIn", JSON.stringify(dataIn));
        if (_logValidation(dataIn)) {
            nlapiLogExecution("AUDIT", "dataIn", "validate");
            var Data = dataIn.Data;
			
			var allOpp = AllOpportunity();
			
            for (var int = 0; int < Data.length; int++) {
                var loadJSON = Data[int];
                //nlapiLogExecution("AUDIT", "loadJSON.Previous_Project_Start_Date__c", loadJSON.Previous_Project_Start_Date__c); 
                //if(!(_logValidation(loadJSON.Previous_Project_Start_Date__c))){
                if (_logValidation(loadJSON.Opportunity__r)) {
                    var Opportunity__r = loadJSON.Opportunity__r;
                    var Account = Opportunity__r.Account;
                    var region = Account.Account_Region__c;
                    // Account/ Customer Details
                    var type = Account.Type;
                    var customerId = Account.NetSuite_Customer_ID__c;
                    // Opportunity Base fields 
                    var opp_name = Opportunity__r.Name;
                    var opp_Id = Opportunity__r.Opportunity_ID__c;
                    var projectStatus = Opportunity__r.Projection_Status__c;
                    var stageName = Opportunity__r.StageName;
                    var winibility = Opportunity__r.Confidence__c;
                    var startDate = dateFormat(Opportunity__r.Project_Start_Date__c);
                    var endDate = dateFormat(Opportunity__r.Project_End_Date__c);
                    var practice = Opportunity__r.Organization_Practice__c;
                    var closeDate = dateFormat(Opportunity__r.CloseDate);
                    var projectId = Opportunity__r.Project_Id__c;
                    var fulfilmentNumber = Opportunity__r.Count_Fulfillment_Plan__c;
                    var mostLikelyDate = dateFormat(Opportunity__r.MostLikelyDate__c); // prabhat gupta
                    var childCreatedDate = dateFormat(Opportunity__r.OppCreatedDate__c); // prabhat gupta


                    var amount = Opportunity__r.Amount;
                    var oppType = Opportunity__r.Type;
                    // Get client partner 
                    var clientPartnerEmail = Account.Owner.Email;

                    var clientPartnerId = getUserUsingEmailId(clientPartnerEmail, practice); //NIS-1691 prabhat gupta 21/8/2020 for comparing the Cognetik practice  

                    var parent_opp_obj = Opportunity__r.Child_Opportunity__r;  
                    // Fulfillment fields
                    var fulfillmentName = loadJSON.Name;
                    var fulfillmentId = loadJSON.Id;
                    var fulfillmentStatus = loadJSON.FulFillment_Status__c;
                    if(fulfillmentStatus == "Initiate"|| fulfillmentStatus == "Modified"){
                        var fulfilmentRaised = "T"
                    }
                    else if(fulfillmentStatus == "Cancelled"){
                        if(fulfilmentNumber>1){
                            var fulfilmentRaised = "T"
                        }
                        else{
                            var fulfilmentRaised = "F"
                        }
                    }
                    else{
                        var fulfilmentRaised = "F"
                    }
                    var fulfillmentPractice = loadJSON.practices__c;
                    var fulfillmentLocation = loadJSON.Location__c;
                    var fulfillmentResourceCount = loadJSON.Resource_Count__c;
                    var fulfillmentSkills = loadJSON.Skill__c;
                    // SET FIELDS
                    var setCategory = loadJSON.SET_Category__c;
                    var setWatchList = loadJSON.SET_Watchlist__c;
                    var setOwner = null;
                    if (_logValidation(loadJSON.SET_Owner__c)) {
                        var owerObj = loadJSON.SET_Owner__r;
                        var s_setOwner = owerObj.Email;
                        setOwner = getUserUsingEmailId(s_setOwner, practice); //NIS-1691 prabhat gupta 21/8/2020 for comparing the Cognetik practice
                    }
                    // Create SFDC Opportunity Record 
                    var searchResult = searchOpportunity(opp_Id);
                    if (!searchResult) {
                        nlapiLogExecution("AUDIT", "opp present", "if");
                        var sfdcRec = nlapiCreateRecord("customrecord_sfdc_opportunity_record");
                        sfdcRec.setFieldValue("custrecord_opportunity_name_sfdc", opp_name);
                        sfdcRec.setFieldValue("custrecord_opportunity_id_sfdc", opp_Id);
                        sfdcRec.setFieldValue("custrecord_practice_sfdc", practice);
                        sfdcRec.setFieldText("custrecord_projection_status_sfdc", projectStatus);
                        sfdcRec.setFieldValue("custrecord_confidence_sfdc", winibility);
                        sfdcRec.setFieldValue("custrecord_start_date_sfdc", startDate);
                        sfdcRec.setFieldValue("custrecord_end_date_sfdc", endDate);
                        sfdcRec.setFieldValue("custrecord_close_date_sfdc", closeDate);
                        sfdcRec.setFieldValue("custrecord_account_type_sfdc", type);
                        sfdcRec.setFieldValue("custrecord_customer_sfdc", customerId);
                        sfdcRec.setFieldText("custrecord_stage_sfdc", stageName);
                        sfdcRec.setFieldValue("custrecord_project_sfdc", projectId);
                        sfdcRec.setFieldValue("custrecord_opp_account_region", region);
                        sfdcRec.setFieldValue("custrecord_tcv_sfdc", amount);
                        sfdcRec.setFieldValue("custrecord_sfdc_client_partner", clientPartnerId);
                        sfdcRec.setFieldValue("custrecord_opp_type_sfdc", oppType);
                        sfdcRec.setFieldValue("custrecord_set_category", setCategory);
                        if (setWatchList) {
                            sfdcRec.setFieldValue("custrecord_set_watchlist", "T");
                        }
                        if (_logValidation(setOwner)) {
                            sfdcRec.setFieldValue("custrecord_set_owner", setOwner);
                        }

                        //-----------------------------------------------------------------------------------------------------------------				//prabhat Gupta NIS-1326 18/05/2020


                        if (projectStatus == "Most Likely") {
                            if (!_logValidation(projectId)) {
                                sfdcRec.setFieldValue("custrecord_most_likely_date_sfdc", mostLikelyDate);

                            } else {

                                if (_logValidation(mostLikelyDate)) {

                                    sfdcRec.setFieldValue("custrecord_most_likely_date_sfdc", mostLikelyDate);

                                } else {

                                    sfdcRec.setFieldValue("custrecord_most_likely_date_sfdc", childCreatedDate);
                                }
                            }
                        }
                        //---------------------------------------------------------------------------------------------------------------	

                         

                         if(_logValidation(parent_opp_obj)){
							 var parent_opp = parent_opp_obj.Opportunity_ID__c;
                                
                              if(_logValidation(parent_opp)){
								  
								  var parent_opp_s = allOpp[parent_opp];
								  
								  if(_logValidation(parent_opp_s)){
									  
									  var parent_opp_id = parent_opp_s;
									  sfdcRec.setFieldValue("custrecord_sfdc_parent_opp_id", parent_opp_id);
									  
								  }
							  }								
						 }						 

                         sfdcRec.setFieldValue("custrecord_fulfilment_plan_sfdc",fulfilmentRaised)
                        var sfdcId = nlapiSubmitRecord(sfdcRec);
                        nlapiLogExecution("AUDIT", "sfdcId", sfdcId);
                        // Create Fulfillment Plan - SFDC/Child Record 
                        var childRec = nlapiCreateRecord("customrecord_fulfillment_plan_sfdc");
                        childRec.setFieldValue("custrecord_fulfill_plan_opp_id", sfdcId);
                        childRec.setFieldValue("custrecord_fulfill_plan_fulfil_name", fulfillmentName);
                        childRec.setFieldValue("custrecord_fulfill_plan_fulfill_id", fulfillmentId);
                        childRec.setFieldValue("custrecord_fulfill_plan_status", fulfillmentStatus);
                        childRec.setFieldValue("custrecord_fulfill_plan_practice", fulfillmentPractice);
                        childRec.setFieldValue("custrecord_fulfill_plan_opp_loc", fulfillmentLocation);
                        childRec.setFieldValue("custrecord_fulfill_plan_position", fulfillmentResourceCount);
                        childRec.setFieldValue("custrecord_fulfill_plan_skill", fulfillmentSkills);




                        var chlidId = nlapiSubmitRecord(childRec);
                    } else {
                        nlapiLogExecution("AUDIT", "opp is not present", "else");
                        var oppRecId = searchResult[0].getId();
                        var sfdcRec = nlapiLoadRecord("customrecord_sfdc_opportunity_record", oppRecId);
                        sfdcRec.setFieldValue("custrecord_opportunity_name_sfdc", opp_name);
                        sfdcRec.setFieldValue("custrecord_opportunity_id_sfdc", opp_Id);
                        sfdcRec.setFieldValue("custrecord_practice_sfdc", practice);
                        sfdcRec.setFieldText("custrecord_projection_status_sfdc", projectStatus);
                        sfdcRec.setFieldValue("custrecord_winnability_sfdc", winibility);
                        sfdcRec.setFieldValue("custrecord_start_date_sfdc", startDate);
                        sfdcRec.setFieldValue("custrecord_end_date_sfdc", endDate);
                        sfdcRec.setFieldValue("custrecord_close_date_sfdc", closeDate);
                        sfdcRec.setFieldValue("custrecord_account_type_sfdc", type);
                        sfdcRec.setFieldValue("custrecord_customer_sfdc", customerId);
                        sfdcRec.setFieldText("custrecord_stage_sfdc", stageName);
                        sfdcRec.setFieldValue("custrecord_project_sfdc", projectId);
                        sfdcRec.setFieldValue("custrecord_opp_account_region", region);
                        sfdcRec.setFieldValue("custrecord_tcv_sfdc", amount);
                        sfdcRec.setFieldValue("custrecord_sfdc_client_partner", clientPartnerId);
                        sfdcRec.setFieldValue("custrecord_opp_type_sfdc", oppType);
                        sfdcRec.setFieldValue("custrecord_fulfilment_plan_sfdc",fulfilmentRaised)
                        //-----------------------------------------------------------------------------------------------------------------				//prabhat Gupta NIS-1326 18/05/2020


                        if (projectStatus == "Most Likely") {

                            if (!_logValidation(projectId)) {
                                sfdcRec.setFieldValue("custrecord_most_likely_date_sfdc", mostLikelyDate);

                            } else {

                                if (_logValidation(mostLikelyDate)) {

                                    sfdcRec.setFieldValue("custrecord_most_likely_date_sfdc", mostLikelyDate);

                                } else {

                                    sfdcRec.setFieldValue("custrecord_most_likely_date_sfdc", childCreatedDate);
                                }
                            }
                        }
                        //---------------------------------------------------------------------------------------------------------------	
						
						

                         if(_logValidation(parent_opp_obj)){
                            
							 var parent_opp = parent_opp_obj.Opportunity_ID__c;
                            
                              if(_logValidation(parent_opp)){
                                
								  var parent_opp_s = allOpp[parent_opp];
								
								  if(_logValidation(parent_opp_s)){
                                    
									  var parent_opp_id = parent_opp_s;
									  sfdcRec.setFieldValue("custrecord_sfdc_parent_opp_id", parent_opp_id);
								  }
							  }								
						 }
                         else{
                            sfdcRec.setFieldValue("custrecord_sfdc_parent_opp_id", '');
                         }	
						
						
						

                        var sfdcId = nlapiSubmitRecord(sfdcRec);
                        nlapiLogExecution("AUDIT", "sfdcId", sfdcId);
                        // Create Fulfillment Plan - SFDC/Child Record 
                        if (fulfillmentStatus == "Cancelled" || fulfillmentStatus == "Modified") {
                            var searchFulfillmentRes = searchFulfillmentRecords(fulfillmentName);
                            if (searchFulfillmentRes) {
                                var childRec = nlapiLoadRecord("customrecord_fulfillment_plan_sfdc", searchFulfillmentRes[0].getId()); //nlapiCreateRecord("customrecord_fulfillment_plan_sfdc");
                                childRec.setFieldValue("custrecord_fulfill_plan_opp_id", sfdcId);
                                childRec.setFieldValue("custrecord_fulfill_plan_fulfil_name", fulfillmentName);
                                childRec.setFieldValue("custrecord_fulfill_plan_fulfill_id", fulfillmentId);
                                childRec.setFieldValue("custrecord_fulfill_plan_status", fulfillmentStatus);
                                childRec.setFieldValue("custrecord_fulfill_plan_practice", fulfillmentPractice);
                                childRec.setFieldValue("custrecord_fulfill_plan_opp_loc", fulfillmentLocation);
                                childRec.setFieldValue("custrecord_fulfill_plan_position", fulfillmentResourceCount);
                                childRec.setFieldValue("custrecord_fulfill_plan_skill", fulfillmentSkills);
                                var chlidId = nlapiSubmitRecord(childRec);
                                nlapiLogExecution("AUDIT", "chlidId after status is cancelled or modified", chlidId);
                            }
                        } else {
                            var childRec = nlapiCreateRecord("customrecord_fulfillment_plan_sfdc");
                            childRec.setFieldValue("custrecord_fulfill_plan_opp_id", sfdcId);
                            childRec.setFieldValue("custrecord_fulfill_plan_fulfil_name", fulfillmentName);
                            childRec.setFieldValue("custrecord_fulfill_plan_fulfill_id", fulfillmentId);
                            childRec.setFieldValue("custrecord_fulfill_plan_status", fulfillmentStatus);
                            childRec.setFieldValue("custrecord_fulfill_plan_practice", fulfillmentPractice);
                            childRec.setFieldValue("custrecord_fulfill_plan_opp_loc", fulfillmentLocation);
                            childRec.setFieldValue("custrecord_fulfill_plan_position", fulfillmentResourceCount);
                            childRec.setFieldValue("custrecord_fulfill_plan_skill", fulfillmentSkills);
                            var chlidId = nlapiSubmitRecord(childRec);
                        }
                    }
                } else {
                    var oppId = loadJSON.Opportunity_ID__c;
                    var newStartDate = dateFormat(loadJSON.Project_Start_Date__c);
                    var newEndDate = dateFormat(loadJSON.Project_End_Date__c);
                    var stageName = loadJSON.StageName;
                    var projectionStatus = loadJSON.Projection_Status__c;
                    var projectId = loadJSON.Project_Id__c;
                    var searchRes = searchOpportunity(oppId);
                    var practiceName = loadJSON.Organization_Practice__c;
                    var confidence = loadJSON.Confidence__c;
                    // SET FIELDS
                    var setCategory = loadJSON.SET_Category__c;
                    var setWatchList = loadJSON.SET_Watchlist__c;
                    var setOwner = null;
                    if (_logValidation(loadJSON.SET_Owner__c)) {
                        var owerObj = loadJSON.SET_Owner__r;
                        var s_setOwner = owerObj.Email;
                        setOwner = getUserUsingEmailId(s_setOwner, practiceName); //NIS-1691 prabhat gupta 21/8/2020 for comparing the Cognetik practice
                    }

                    var mostLikelyDate = dateFormat(loadJSON.MostLikelyDate__c); // prabhat gupta
                    var childCreatedDate = dateFormat(loadJSON.OppCreatedDate__c); // prabhat gupta
					
					var parent_opp_obj = loadJSON.Child_Opportunity__r; 
                    var oppType = loadJSON.Type;
                    var fulfilmentNumber = loadJSON.Count_Fulfillment_Plan__c;
                    if(fulfilmentNumber == 0){
                        var fulfilmentRaised = "F"
                    }
                    else if(fulfilmentNumber > 0){
                        var fulfilmentRaised = "T"
                    }
					
					
					

                    if (searchRes) {
                        var i_oppId = searchRes[0].getId();
                        var oppRec = nlapiLoadRecord("customrecord_sfdc_opportunity_record", i_oppId);
                        var existingStartDate = oppRec.getFieldValue("custrecord_start_date_sfdc");
                        var existingEndDate = oppRec.getFieldValue("custrecord_end_date_sfdc");
                        if (stageName != "Closed Shelved" && stageName != "Closed Lost") {
                            if (newStartDate) {
                                //oppRec.setFieldValue("custrecord_start_date_sfdc", newStartDate);
                                if (Date.parse(newStartDate) != Date.parse(existingStartDate) || Date.parse(newEndDate) != Date.parse(existingEndDate)) {
                                    oppRec.setFieldValue("custrecord_sfdc_opp_rec_rev_startdate", newStartDate);
                                }
                            }
                            if (newEndDate) {
                                //oppRec.setFieldValue("custrecord_end_date_sfdc", newEndDate);
                                if (Date.parse(newStartDate) != Date.parse(existingStartDate) || Date.parse(newEndDate) != Date.parse(existingEndDate)) {
                                    oppRec.setFieldValue("custrecord_sfdc_opp_rec_rev_enddate", newEndDate);
                                }
                            }

                        }
                        // Added on 08 May 2019 to update projection status and opportunity stage
                        if (projectionStatus) oppRec.setFieldText("custrecord_projection_status_sfdc", projectionStatus);
                        if (stageName) oppRec.setFieldText("custrecord_stage_sfdc", stageName);
                        if (projectId) oppRec.setFieldValue("custrecord_project_sfdc", projectId);
                        if (practiceName) oppRec.setFieldValue("custrecord_practice_sfdc", practiceName);
                        if (confidence) oppRec.setFieldValue("custrecord_confidence_sfdc", confidence);
                        if (oppType) oppRec.setFieldValue("custrecord_opp_type_sfdc", oppType);
                        //End
                        //set Fields start
                        oppRec.setFieldValue("custrecord_set_category", setCategory);
                        if (setWatchList) {
                            oppRec.setFieldValue("custrecord_set_watchlist", "T");
                        } else {
                            oppRec.setFieldValue("custrecord_set_watchlist", "F");
                        }
                        if (_logValidation(setOwner)) {
                            oppRec.setFieldValue("custrecord_set_owner", setOwner);
                        } else {
                            oppRec.setFieldValue("custrecord_set_owner", "");
                        }
                        //-----------------------------------------------------------------------------------------------------------------				//prabhat Gupta NIS-1326 18/05/2020


                        if (projectionStatus == "Most Likely") {

                            if (!_logValidation(projectId)) {
                                oppRec.setFieldValue("custrecord_most_likely_date_sfdc", mostLikelyDate);

                            } else {

                                if (_logValidation(mostLikelyDate)) {

                                    oppRec.setFieldValue("custrecord_most_likely_date_sfdc", mostLikelyDate);

                                } else {

                                    oppRec.setFieldValue("custrecord_most_likely_date_sfdc", childCreatedDate);
                                }
                            }
                        }
                        //---------------------------------------------------------------------------------------------------------------			

                           if(_logValidation(parent_opp_obj)){
                           
							 var parent_opp = parent_opp_obj.Opportunity_ID__c;
                               
                              if(_logValidation(parent_opp)){
								  
								  var parent_opp_s = allOpp[parent_opp];
								  
								  if(_logValidation(parent_opp_s)){
									  
									  var parent_opp_id = parent_opp_s;
                                     
									  oppRec.setFieldValue("custrecord_sfdc_parent_opp_id", parent_opp_id);
									  
								  }
							  }								
						 }
                         else{
                            oppRec.setFieldValue("custrecord_sfdc_parent_opp_id", '');
                         }
           					if(fulfilmentRaised!=''||fulfilmentRaised!=null){
                                oppRec.setFieldValue("custrecord_fulfilment_plan_sfdc", fulfilmentRaised);
                               }
                        // set fields end 
                        nlapiSubmitRecord(oppRec);
                    } else {
                        return {
                            "status": "incorrect data",
                            "message": "Given Opportunity #" + loadJSON.Opportunity_ID__c + " does not present in NetSuite"
                        }
                    }
                }
            }
        }
        return {
            "status": "success"
        };
    } catch (e) {
        nlapiLogExecution("DEBUG", "Error in script :", e);
        return {
            "status": "fail",
            "message": e
        };
    }
}
/**
 * @param {Object} dataIn Parameter object
 * @returns {Object} Output object
 */
function postRESTlet(dataIn) {
    return {};
}
//validate blank entries
function _logValidation(value) {
    if (value != null && value.toString() != null && value != '' && value != undefined && value.toString() != undefined && value != 'undefined' && value.toString() != 'undefined' && value.toString() != 'NaN' && value != NaN) {
        return true;
    } else {
        return false;
    }
}

function dateFormat(date) {
    var date_return = '';
    if (date) {
        var dateObj = date.split('-');
        var month = dateObj[1];
        var year = dateObj[0];
        var date_d = dateObj[2];
        date_return = month + '/' + date_d + '/' + year;
    }
    return date_return;
}

function searchCustomer(account_id) {
    try {
        var customer_id = '';
        var filters = [];
        var s_account_customer = '"' + account_id + '"';
        nlapiLogExecution('DEBUG', 'Customer Filter ', s_account_customer);
        //filters.push(new nlobjSearchFilter('custentity22',null,'is',account_id.toString())); //custentity_sfdc_account_id
        filters.push(new nlobjSearchFilter('custentity_sfdc_account_id', null, 'is', account_id.toString()));
        var cols = [];
        cols.push(new nlobjSearchColumn('internalid').setSort(true));
        //cols.push(new nlobjSearchColumn('name'));
        var searchRecord = nlapiSearchRecord('customer', null, filters, cols);
        if (searchRecord) customer_id = searchRecord[0].getValue('internalid');
        nlapiLogExecution('DEBUG', 'Customer Search ', customer_id);
        return customer_id;
    } catch (e) {
        nlapiLogExecution('DEBUG', 'Customer Search Error', e);
        throw e;
    }
}

function searchProject(pro_id) {
    try {
        var proj_id = '';
        var filters = [];
        var s_account_customer = '"' + pro_id + '"';
        nlapiLogExecution('DEBUG', 'Project Filter ', pro_id);
        filters.push(new nlobjSearchFilter('entityid', null, 'startswith', pro_id));
        var cols = [];
        cols.push(new nlobjSearchColumn('internalid').setSort(true));
        //cols.push(new nlobjSearchColumn('name'));
        var searchRecord = nlapiSearchRecord('job', null, filters, cols);
        if (searchRecord) proj_id = searchRecord[0].getValue('internalid');
        nlapiLogExecution('DEBUG', 'Project Search ', proj_id);
        return proj_id;
    } catch (e) {
        nlapiLogExecution('DEBUG', 'Customer Search Error', e);
        throw e;
    }
}

function searchOpportunity(opp_Id) {
    var customrecord_sfdc_opportunity_recordSearch = nlapiSearchRecord("customrecord_sfdc_opportunity_record", null,
        [
            ["custrecord_opportunity_id_sfdc", "is", opp_Id]
        ],
        [
            new nlobjSearchColumn("scriptid").setSort(false),
            new nlobjSearchColumn("custrecord_opportunity_id_sfdc"),
            new nlobjSearchColumn("custrecord_opportunity_name_sfdc"),
            new nlobjSearchColumn("custrecord_project_sfdc"),
            new nlobjSearchColumn("custrecord_customer_sfdc"),
            new nlobjSearchColumn("custrecord_start_date_sfdc"),
            new nlobjSearchColumn("custrecord_end_date_sfdc"),
            new nlobjSearchColumn("custrecord_practice_internal_id_sfdc"),
            new nlobjSearchColumn("custrecord_location_sfdc"),
            new nlobjSearchColumn("custrecord_sales_act_sfd"),
            new nlobjSearchColumn("custrecord_account_type_sfdc"),
            new nlobjSearchColumn("custrecord_opp_account_region"),
            new nlobjSearchColumn("custrecord_opp_service_line"),
            new nlobjSearchColumn("custrecord_account_name_sfdc"),
            new nlobjSearchColumn("custrecord_opp_fulfilment_status_sfdc"),
            new nlobjSearchColumn("custrecord_status_sfdc")
        ]);
    return customrecord_sfdc_opportunity_recordSearch;
}

function searchFulfillmentRecords(fulfillId) {
    var customrecord_fulfillment_plan_sfdcSearch = nlapiSearchRecord("customrecord_fulfillment_plan_sfdc", null,
        [
            ["custrecord_fulfill_plan_fulfil_name", "is", fulfillId]
        ],
        []);
    return customrecord_fulfillment_plan_sfdcSearch;
}


//--------------------------------------------------------------------------------------------------------------------

//NIS-1691 prabhat gupta 21/8/2020 for getting the employee id of the person but if we get the practice as Cognetik we will check the prefix of mail id then we will get the emp. id of person 

function getUserUsingEmailId(emailId, practice) {
    try {



        if (practice == "Cognetik") {

            var prefix_email = emailId.split("@")[0];
            var employeeSearch = nlapiSearchRecord('employee', null, [
                new nlobjSearchFilter('email', null, 'contains', prefix_email),
                new nlobjSearchFilter('custentity_employee_inactive', null, 'is', 'F')
            ]);
        } else {
            var employeeSearch = nlapiSearchRecord('employee', null, [
                new nlobjSearchFilter('email', null, 'is', emailId),
                new nlobjSearchFilter('custentity_employee_inactive', null, 'is', 'F')
            ]);
        }

        if (employeeSearch) {
            return employeeSearch[0].getId();
        } else {
            throw "User does not exist";
        }
    } catch (err) {
        nlapiLogExecution('ERROR', 'getUserUsingEmailId', err);
        throw err;
    }
}


function AllOpportunity() {
	
    var customrecord_sfdc_opportunity_recordSearch = searchRecord("customrecord_sfdc_opportunity_record", null,
        [
        ["custrecord_sfdc_parent_opp_id","anyof","@NONE@"]
],
        [           
            new nlobjSearchColumn("custrecord_opportunity_id_sfdc")
        ]);
	var oppData = {};	
	
	if(_logValidation(customrecord_sfdc_opportunity_recordSearch)){
		
		for(var i=0; i<customrecord_sfdc_opportunity_recordSearch.length; i++){
			
			var opp = customrecord_sfdc_opportunity_recordSearch[i].getValue("custrecord_opportunity_id_sfdc");
			var opp_id = customrecord_sfdc_opportunity_recordSearch[i].getId();
			
			oppData[opp] = opp_id;
			
		}	
		
	}	
	
    return oppData;
}

function searchRecord(recordType, savedSearch, arrFilters, arrColumns,
		filterExpression) {

	try {
		var search = null;

		// if a saved search is provided, load it and add the filters and
		// columns
		if (savedSearch) {
			search = nlapiLoadSearch(recordType, savedSearch);

			if (isArrayNotEmpty(arrFilters)) {
				search.addFilters(arrFilters);
			}

			if (isArrayNotEmpty(arrColumns)) {
				search.addColumns(arrColumns);
			}

			if (isArrayNotEmpty(filterExpression)) {
				search.setFilterExpression(filterExpression);
			}
		}
		// create a new search
		else {
			search = nlapiCreateSearch(recordType, arrFilters, arrColumns);
		}

		// run search
		var resultSet = search.runSearch();

		// iterate through the search and get all data 1000 at a time
		var searchResultCount = 0;
		var resultSlice = null;
		var searchResult = [];

		do {
			resultSlice = resultSet.getResults(searchResultCount,
					searchResultCount + 1000);

			if (resultSlice) {

				resultSlice.forEach(function(result) {

					searchResult.push(result);
					searchResultCount++;
				});
			}
		} while (isArrayNotEmpty(resultSlice) && resultSlice.length >= 1000);

		return searchResult;
	} catch (err) {
		nlapiLogExecution('ERROR', 'searchRecord', err);
		throw err;
	}
}

function isEmpty(value) {

	return value == null || value == "" || typeof(value) == undefined;
}

function isNotEmpty(value) {

	return !isEmpty(value);
}

function isArrayEmpty(argArray) {

	return !isArrayNotEmpty(argArray);
}

function isArrayNotEmpty(argArray) {

	return (isNotEmpty(argArray) && argArray.length > 0);
}

//---------------------------------------------------------------------------------------------------------------