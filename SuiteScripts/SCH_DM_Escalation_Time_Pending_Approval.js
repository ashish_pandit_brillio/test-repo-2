// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
    /*
     Script Name:	SCH_DM_Escalation_Time_Pending_Approval
     Author:			Vikrant Adhav
     Company:		Aashna CloudTech
     Date:			01-11-2014
     Script Modification Log:
     -- Date --			-- Modified By --				--Requested By--				-- Description --
     Below is a summary of the process controls enforced by this script file.  The control logic is described
     more fully, below, in the appropriate function headers and code blocks.
     SCHEDULED FUNCTION
     - SCH_DM_Escalation_Time_Pending_Approval(type)
     SUB-FUNCTIONS
     - The following sub-functions are called by the above core functions in order to maintain code
     modularization:
     - NOT USED
     */
}
// END SCRIPT DESCRIPTION BLOCK  ====================================


// BEGIN SCHEDULED FUNCTION =============================================

function SCH_DM_Escalation_Time_Pending_Approval(type) //
{
    /*  On scheduled function:
     
     - PURPOSE
     
     -
     
     FIELDS USED:
     
     --Field Name--				--ID--
     
     */
    //  LOCAL VARIABLES
    
    
    
    //  SCHEDULED FUNCTION CODE BODY
    
    try //
    {
        var i_initial_ID = 0;
        var search_Result = '';
        var filters = new Array();
        
        //do //
        //{
        // Fetch data from saved search
        filters = new Array();
        //filters[filters.length] = new nlobjSearchFilter('custentity_deliverymanager', 'customer', 'greaterthan', i_initial_ID)
        
        search_Result = nlapiSearchRecord('timebill', 'customsearch_escalation_to_dm', filters, null); // this will get the data from saved search
        if (_validate(search_Result)) //
        {
            nlapiLogExecution('DEBUG', 'SCH_DM_Escalation_Time_Pending_Approval', 'search_Result.length : ' + search_Result.length);
            
            var all_columns = search_Result[0].getAllColumns();
            
            var text = '';
            var value = '';
            var label = '';
            var email_text = '';
            var email_text_final = '';
            var i_delivery_manager_temp = '';
            
            for (var counter_i = 0; counter_i < search_Result.length; counter_i++) //
            {
                nlapiLogExecution('DEBUG', 'SCH_DM_Escalation_Time_Pending_Approval', 'counter_i : ' + counter_i);
                
                var result = search_Result[counter_i];
                
                var i_delivery_manager = '';
                var s_delivery_manager = '';
                
                var s_project_manager = '';
                var s_employee = '';
                var s_project = '';
                var i_duration = '';
                var s_vertical = '';
                var s_vertical_head = '';
                
                for (var counter_j = 0; counter_j < all_columns.length; counter_j++) //
                {
                    var column = all_columns[counter_j];
                    text = result.getText(column);
                    value = result.getValue(column);
                    label = column.getLabel();
                    
                    if (label == 'delivery_manager') //
                    {
                        i_delivery_manager = value;
                        s_delivery_manager = text;
                    }
                    
                    if (label == 'project_manager') //
                    {
                        s_project_manager = text;
                    }
                    
                    if (label == 'employee') //
                    {
                        s_employee = text;
                    }
                    
                    if (label == 'project') //
                    {
                        s_project = value;
                        ;
                    }
                    //nlapiLogExecution('DEBUG', 'SCH_DM_Escalation_Time_Pending_Approval', 'label : ' + label);
                    if (label == 'total_hours') //
                    {
                        i_duration = value;
                        //nlapiLogExecution('DEBUG', 'SCH_DM_Escalation_Time_Pending_Approval', 'i_duration : ' + text);
                        //nlapiLogExecution('DEBUG', 'SCH_DM_Escalation_Time_Pending_Approval', 'i_duration : ' + value);
                    }
                    
                    if (label == 'vertical') //
                    {
                        s_vertical = text;
                    }
                    
                    if (label == 'vertical_head') //
                    {
                        s_vertical_head = text;
                    }
                }
                
                if (counter_i == 0) //
                {
                    i_delivery_manager_temp = i_delivery_manager;
                    //nlapiLogExecution('DEBUG', 'SCH_DM_Escalation_Time_Pending_Approval', 'counter_i : ' + counter_i);
                }
                
				var last_row = parseInt(search_Result.length - 1);
				nlapiLogExecution('DEBUG', 'SCH_DM_Escalation_Time_Pending_Approval', 'last_row : ' + last_row);
               
			    if (counter_i == last_row) // if it is a last row
                {
                    i_delivery_manager = '';
                    nlapiLogExecution('DEBUG', 'SCH_DM_Escalation_Time_Pending_Approval', 'i_delivery_manager : ' + i_delivery_manager);
					nlapiLogExecution('DEBUG', 'SCH_DM_Escalation_Time_Pending_Approval', 'i_delivery_manager_temp : ' + i_delivery_manager_temp);
                }
                
                
                if (i_delivery_manager != i_delivery_manager_temp) // if delivery manager is not matching send mail 
                {
                    //nlapiLogExecution('DEBUG', 'SCH_DM_Escalation_Time_Pending_Approval', 'i_delivery_manager : ' + i_delivery_manager);
                    //nlapiLogExecution('DEBUG', 'SCH_DM_Escalation_Time_Pending_Approval', 'i_delivery_manager_temp : ' + i_delivery_manager_temp);
					
                    // Adding final mail content
                    email_text_final += '<html>';
                    email_text_final += '<p>Hi,<br/>Following is the list of Employee time sheets pending for Project Manager approval(Under your Projects) for last month</p>';
                    email_text_final += '<p><br/>Kindly followup with Project Managers to process the same ASAP.</p>';
                    email_text_final += '<br/><br/><br/><table width=100% align="left">';
                    email_text_final += '<tr align="left">';
                    email_text_final += '<td align=left><strong>Employee</strong>';
                    email_text_final += '</td>';
                    email_text_final += '<td align="left"><strong>Project</strong>';
                    email_text_final += '</td>';
                    email_text_final += '<td align="left"><strong>Project Manager</strong>';
                    email_text_final += '</td>';
                    email_text_final += '<td align="left"><strong>Hours</strong>';
                    email_text_final += '</td>';
                    email_text_final += '<td align="left"><strong>Vertical</strong>';
                    email_text_final += '</td>';
                    email_text_final += '<td align="left"><strong>Vertical Head</strong>';
                    email_text_final += '</td>';
                    email_text_final += '</tr>';
                    
                    email_text_final = email_text_final + email_text; // adding earlier added mail content to final content.
                    email_text_final += '</table>';
                    
                    // Adding Signature
                    email_text_final += '<table width="100%" border="0" cellspacing="0" cellpadding="0">';
                    
                    email_text_final += '<tr>';
                    email_text_final += '<td align="left">';
                    email_text_final += 'Regards,<br/>Information Systems.';
                    email_text_final += '</td>';
                    email_text_final += '</tr>';
                    
                    email_text_final += '<tr>';
                    email_text_final += '<td align="right">';
                    email_text_final += '<font style="font-size:9px; font-family:Verdana,Arial,Helvetica,sans-serif; color:#999999;">Brillio is powered by <a href="http://www.netsuite.com/" style="color:#999999;">NetSuite</a> &#151; One System. No Limits.</font>';
                    email_text_final += '</td>';
                    email_text_final += '</tr>';
                    email_text_final += '</table>';
                    
                    email_text_final += '</html>';
                    
                    nlapiLogExecution('DEBUG', 'SCH_DM_Escalation_Time_Pending_Approval', 'email_text_final : ' + email_text_final);
                    
                    var sender = '442'; // Information Systems will send this mail
                    var recepeint = i_delivery_manager_temp; // to delivery manager
                    var CC = 'lisaj@brillio.com'; // CCing Lisa	
                    var subject = 'Escalation Mail - Timesheets Pending for PM Approval (For October) !!!';
                    
                    nlapiSendEmail(sender, recepeint, subject, email_text_final, CC)
                    //nlapiSendEmail(sender, 'vikrant@aashnacloudtech.com', subject, email_text_final);
                    nlapiLogExecution('DEBUG', 'SCH_DM_Escalation_Time_Pending_Approval', 'Email Sent !!! ' + i_delivery_manager_temp);
                    
                    i_initial_ID = i_delivery_manager_temp;
                    i_delivery_manager_temp = i_delivery_manager;
                    email_text = '';
                    email_text_final = '';
                    
                    if (counter_i > 900) //
                    {
                        //return;
                        //break;
                    }
                    
                    //return;
                    
                    email_text += '<tr>';
                    email_text += '<td>' + s_employee;
                    email_text += '</td>';
                    email_text += '<td>' + s_project;
                    email_text += '</td>';
                    email_text += '<td>' + s_project_manager;
                    email_text += '</td>';
                    email_text += '<td>' + i_duration;
                    email_text += '</td>';
                    email_text += '<td>' + s_vertical;
                    email_text += '</td>';
                    email_text += '<td>' + s_vertical_head;
                    email_text += '</td>';
                    email_text += '</tr>';
                    
                }
                else // if matching then create mail body
                {
                    email_text += '<tr>';
                    email_text += '<td>' + s_employee;
                    email_text += '</td>';
                    email_text += '<td>' + s_project;
                    email_text += '</td>';
                    email_text += '<td>' + s_project_manager;
                    email_text += '</td>';
                    email_text += '<td>' + i_duration;
                    email_text += '</td>';
                    email_text += '<td>' + s_vertical;
                    email_text += '</td>';
                    email_text += '<td>' + s_vertical_head;
                    email_text += '</td>';
                    email_text += '</tr>';
                }
            }
        }
        
        // categories all data as per Delivery manager
        // send mail to each delivery manager
        //}
        //while (search_Result != null) //
        //{
    
        //}
    } 
    catch (ex) //
    {
        nlapiLogExecution('ERROR', 'SCH_DM_Escalation_Time_Pending_Approval', 'ex' + ex);
        nlapiLogExecution('ERROR', 'SCH_DM_Escalation_Time_Pending_Approval', 'ex' + ex.message);
    }
    
}

// END SCHEDULED FUNCTION ===============================================





// BEGIN FUNCTION ===================================================
{
    function _validate(obj) //
    {
        if (obj != null && obj != '' && obj != 'undefined')//
        {
            return true;
        }
        else //
        {
            return false;
        }
    }
    
    
}
// END FUNCTION =====================================================
