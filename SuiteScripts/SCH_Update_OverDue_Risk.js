/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       09 Apr 2019     Aazamali Khan	   update the overdue and risk.
 * 1.01       13  Aug 2019    Aazamali Khan    Jira FUEL-169
 *
 */
/**
 * @param {String} type Context Types: scheduled, ondemand, userinterface, aborted, skipped
 * @returns {Void}
 */
function updateOverDueRisk(type) {
	var fulfillmentArray = [];
	var searchResult = getFulfillmentRecords();
	if (searchResult) {
		for (var i = 0; i < searchResult.length; i++) {
			CheckMetering();
			//fulfillmentArray.push(searchResult[i].getId());
			var recId = searchResult[i].getId();
			getFRFDetailsForFulfillment(recId);
		}
	}
	/*for (var j = 0; j < fulfillmentArray.length; j++) {
		CheckMetering();
		getFRFDetailsForFulfillment(fulfillmentArray[j]);
	}*/
}

function getFulfillmentRecords() {
	var filters = [];
	var columns = [];
	//filters.push(["custrecord_fulfill_dashboard_opp_id","anyof","3800"]);
	var dashboard_dataSearch = searchRecord("customrecord_fulfillment_dashboard_data", null, filters, columns);
	return dashboard_dataSearch;
}

function CheckMetering() {
	var CTX = nlapiGetContext();
	var START_TIME = new Date().getTime();
	//	want to try to only check metering every 15 seconds
	var remainingUsage = CTX.getRemainingUsage();
	var currentTime = new Date().getTime();
	var timeDifference = currentTime - START_TIME;
	//	changing to 15 minutes, should cause little if any impact, but willmake runaway scripts faster to kill
	if (remainingUsage < 800 || timeDifference > 900000) {
		START_TIME = new Date().getTime();
		var status = nlapiYieldScript();
		//nlapiLogExecution('AUDIT', 'STATUS = ', JSON.stringify(status));
	}
}

function getFRFDetailsForFulfillment(recId) {
	var oppId = nlapiLookupField("customrecord_fulfillment_dashboard_data", recId, "custrecord_fulfill_dashboard_opp_id", false);
	var projectId = nlapiLookupField("customrecord_fulfillment_dashboard_data", recId, "custrecord_fulfill_dashboard_project", false);
	//var oldRiskValue = nlapiLookupField("customrecord_fulfillment_dashboard_data", recId, "custrecord_fulfill_dashboard_risk", false);
	var overDue = 0;
	var risk = 0;
	var riskIST = 0;
	var overDueIST = 0;
	/*if(isNotEmpty(oldRiskValue))
	{
		risk = parseInt(oldRiskValue);
		
	}*/
	if (oppId) {
		// search for frf details based on opportunity ids
		////nlapiLogExecution("DEBUG", "oppId : ", oppId);
		var customrecord_frf_detailsSearch = nlapiSearchRecord("customrecord_frf_details", null,
			[
				["custrecord_frf_details_opp_id", "anyof", oppId], "AND",
				["custrecord_frf_details_open_close_status", "anyof", "1", "3"], "AND", ["custrecord_frf_details_status", "is", "F"]
			],
			[]);
		if (customrecord_frf_detailsSearch) {
			for (var int = 0; int < customrecord_frf_detailsSearch.length; int++) {
				var frfDetailsId = customrecord_frf_detailsSearch[int].getId();
                nlapiLogExecution("DEBUG", "frfDetailsId : ", frfDetailsId);
				var frfDetailsRec = nlapiLoadRecord("customrecord_frf_details", frfDetailsId);
				var startDate = frfDetailsRec.getFieldValue("custrecord_frf_details_start_date");
				var externalHire = frfDetailsRec.getFieldValue("custrecord_frf_details_external_hire");
				var avaliableDate = frfDetailsRec.getFieldValue("custrecord_frf_details_availabledate");
				var frfStatus = frfDetailsRec.getFieldValue("custrecord_frf_details_open_close_status");
				//nlapiLogExecution("DEBUG", "frfStatus : ", frfStatus);
				nlapiLogExecution("DEBUG", "avaliableDate : ", avaliableDate);
				nlapiLogExecution("DEBUG", "startDate : ", startDate);
				var currentDate = GetTodaysDate();
				var indiaTime = toTimeZone(currentDate, "Asia/Kolkata")
				currentDate = nlapiStringToDate(currentDate);
				var riskCurrentDate = nlapiStringToDate(indiaTime);
				if (externalHire == "T" && frfStatus == "1") {
					if (avaliableDate) {
						if (avaliableDate > startDate && startDate > currentDate) {
							++risk;
						}
					} else {
						if (startDate > currentDate) {
							++risk;
						}
					}
					if (avaliableDate) {
						if (avaliableDate > startDate && nlapiStringToDate(startDate) > riskCurrentDate) {
							++riskIST;
						}
					} else {
						if (nlapiStringToDate(startDate) > riskCurrentDate) {
							++riskIST;
						}
					}
				}
				startDate = nlapiStringToDate(startDate);
				//nlapiLogExecution("DEBUG", "startDate : "+startDate, "currentDate : "+currentDate);
				if (startDate < currentDate) {
					++overDue;
				}
				if (startDate < riskCurrentDate) {
					++overDueIST;
				}
				var days = startDate - currentDate;
				var diff1days = Math.round(days / (1000 * 60 * 60 * 24));
				//nlapiLogExecution("DEBUG", "diff1days", diff1days);
				var indiaDays = startDate - riskCurrentDate;
				var indiaDiffdays = Math.round(indiaDays / (1000 * 60 * 60 * 24));
				if (externalHire == "F" && frfStatus == "1") {
					if (diff1days <= 20 && diff1days >= 0) {
						++risk;
					}
				}
				if (externalHire == "F" && frfStatus == "1") {
					if (indiaDiffdays <= 20 && indiaDiffdays >= 0) {
						++riskIST;
					}
				}
			}
		}
		nlapiLogExecution("ERROR", "Risk : ", risk);
		nlapiLogExecution("ERROR", "Over Due : ", overDue);
		//nlapiLogExecution("ERROR", "recordId : ", recId);
		nlapiSubmitField("customrecord_fulfillment_dashboard_data", recId, "custrecord_fulfill_dashboard_overdue", overDue);
		nlapiSubmitField("customrecord_fulfillment_dashboard_data", recId, "custrecord_fulfill_dashboard_risk", risk);
		nlapiSubmitField("customrecord_fulfillment_dashboard_data", recId, "custrecord_fulfillment_dash_data_riskist", riskIST);
		nlapiSubmitField("customrecord_fulfillment_dashboard_data", recId, "custrecord_fulfillment_dash_data_overist", overDueIST);
	} else if (projectId) {
		var searchResult = nlapiSearchRecord("customrecord_frf_details", null,
			[
				["custrecord_frf_details_project", "anyof", projectId], "AND",
				["custrecord_frf_details_opp_id", "noneof", "@NONE@"], "AND",
				["custrecord_frf_details_open_close_status", "anyof", "1", "3"], "AND", ["custrecord_frf_details_status", "is", "F"]
			],
			[]);
		if (searchResult) {
			for (var int = 0; int < searchResult.length; int++) {
				var frfDetailsId = searchResult[int].getId();
				var frfDetailsRec = nlapiLoadRecord("customrecord_frf_details", frfDetailsId);
				var startDate = frfDetailsRec.getFieldValue("custrecord_frf_details_start_date");
				var externalHire = frfDetailsRec.getFieldValue("custrecord_frf_details_external_hire");
				var avaliableDate = frfDetailsRec.getFieldValue("custrecord_frf_details_availabledate");
				var frfStatus = frfDetailsRec.getFieldValue("custrecord_frf_details_open_close_status");
				var currentDate = GetTodaysDate();
				var indiaTime = toTimeZone(currentDate, "Asia/Kolkata")
				currentDate = nlapiStringToDate(currentDate);
				var riskCurrentDate = nlapiStringToDate(indiaTime);
				if (externalHire == "T" && frfStatus == "1") {
					if (avaliableDate) {
						if (avaliableDate > startDate && nlapiStringToDate(startDate) > currentDate) {
							++risk;
						}
					} else {
						if (nlapiStringToDate(startDate) > currentDate) {
							++risk;
						}
					}
					if (avaliableDate) {
						if (avaliableDate > startDate && nlapiStringToDate(startDate) > riskCurrentDate) {
							++riskIST;
						}
					} else {
						if (nlapiStringToDate(startDate) > riskCurrentDate) {
							++riskIST;
						}
					}
				}
				startDate = nlapiStringToDate(startDate);
				if (startDate < currentDate) {
					++overDue;
				}
				if (startDate < riskCurrentDate) {
					++overDueIST;
				}
				var days = startDate - currentDate;
				var indiaDays = startDate - riskCurrentDate;
				var indiaDiffdays = Math.round(indiaDays / (1000 * 60 * 60 * 24));
				var diff1days = Math.round(days / (1000 * 60 * 60 * 24));
				if (externalHire == "F" && frfStatus == "1") {
					if (diff1days <= 20 && diff1days >= 0) {
						++risk;
					}
				}
				if (externalHire == "F" && frfStatus == "1") {
					if (indiaDiffdays <= 20 && indiaDiffdays >= 0) {
						++riskIST;
					}
				}
			}
		}
		nlapiSubmitField("customrecord_fulfillment_dashboard_data", recId, "custrecord_fulfill_dashboard_overdue", overDue);
		nlapiSubmitField("customrecord_fulfillment_dashboard_data", recId, "custrecord_fulfill_dashboard_risk", risk);
		nlapiSubmitField("customrecord_fulfillment_dashboard_data", recId, "custrecord_fulfillment_dash_data_riskist", riskIST);
		nlapiSubmitField("customrecord_fulfillment_dashboard_data", recId, "custrecord_fulfillment_dash_data_overist", overDueIST);
	}
}

function GetTodaysDate() {
	var today = new Date();
	var dd = today.getDate();
	var mm = today.getMonth() + 1;
	var yyyy = today.getFullYear();
	return mm + "/" + dd + "/" + yyyy;
}
function toTimeZone(time, zone) {
    var format = 'MM/DD/YYYY';
    return moment(time, format).tz(zone).format(format);
}