function PageInitValidateSkill()
{
	try
	{
		//confirm("Have you acquired new skills?");
		if(confirm("Have you acquired new skills?"))
		{
			window.open('/app/site/hosting/scriptlet.nl?script=1657&deploy=1','_self')
		}
		else
		{
			var User_ID = nlapiGetUser();
			var rec = nlapiCreateRecord('customrecord_employee_master_skill_data');
			//var rec = nlapiLoadRecord('customrecord_employee_master_skill_data',record);
			rec.setFieldValue('custrecord_new_skill_acquired',2);
			rec.setFieldValue('custrecord_employee_skill_updated',User_ID);
			nlapiSubmitRecord(rec,true,true);
			window.close();
		}
	}
	
	catch(err)
	{
		nlapiLogExecution('Debug','Error in Client Script', err);
	}
}
