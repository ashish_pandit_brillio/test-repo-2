function scheduled(type)
{
	try
	{
		var current_date = new Date();
        var a_data = searchEmployee();
		
		var excel_result = createExcel(a_data);
		
		var newAttachment = nlapiLoadFile(excel_result);
        var a_emp_attachment = new Array();
		
		var emp_email = '';
		
		emp_email = ['japnit.sethi@BRILLIO.COM'];
		
		nlapiSendEmail(442, emp_email , 'Employee Data Kwench as on ' + nlapiDateToString(new Date(), 'date'),'PFA the employee data for qwench',null,["prabhat.gupta@brillio.com","sridhar.v@brillio.com"],null, newAttachment);
        nlapiLogExecution('DEBUG', 'Mail Sent', emp_email);
	}
	catch (err)
	{
		nlapiLogExecution('Debug','Process Error - Main',err);
	}
}

function createExcel(a_results) {
    try {
		
		var current_date = nlapiDateToString(new Date());
        var strVar_excel = '';
        var html = "";

        html += "Employee Id,";
        html += "First Name,";
        html += "Last Name,";
 
       
        html += "Email ID,";
        
        html += "RM Employee ID,";
        html += "BU,";
        html += "Department,";
        html += "Designation,";
		html += "Unit Head Id,";
        html += "Unit Head Name,";
        html += "Employee Grade,";
		html += "Location,";
        html += "Gender,";
        html += "Date of birth,";
		html += "Date of Joining,";
        html += "\r\n";
		
		for (var k = 0; k < a_results.length; k++) {
            var temp = a_results[k];
            html += _logValidation(temp.EmployeeId) == false ? ' ' : temp.EmployeeId;
            html += ",";
            html += _logValidation(temp.Firstname) == false ? ' ' : temp.Firstname;
            html += ",";
            html += _logValidation(temp.Lastname) == false ? ' ' : temp.Lastname;
            html += ",";
			html += _logValidation(temp.EmailId) == false ? ' ' : temp.EmailId;
            html += ",";
            html += _logValidation(temp.RMID) == false ? ' ' : temp.RMID;
            html += ",";
            html += _logValidation(temp.BU) == false ? ' ' : temp.BU;
            html += ",";
			html += _logValidation(temp.Department) == false ? ' ' : temp.Department;
			html += ",";
            html += _logValidation(temp.Designation) == false ? ' ' : temp.Designation;
            html += ",";
            html += _logValidation(temp.UnitHeadId) == false ? ' ' : temp.UnitHeadId;
            html += ",";
            html += _logValidation(temp.UnitHeadName) == false ? ' ' : temp.UnitHeadName;
            html += ",";
            html += _logValidation(temp.Employeelevel) == false ? ' ' : temp.Employeelevel;
            html += ",";
            html += _logValidation(temp.Location) == false ? ' ' : temp.Location;
            html += ",";
            html += _logValidation(temp.Gender) == false ? ' ' : temp.Gender;
            html += ",";
            html += _logValidation(temp.Birthdate) == false ? ' ' : temp.Birthdate;
            html += ",";
            html += _logValidation(temp.DOJ) == false ? ' ' : temp.DOJ;
            html += "\r\n";
        }
		
		var current_date = nlapiDateToString(new Date());
		//var _date = nlapiStringToDate(current_date);
        var fileName = 'Employee Data Qwench '+ nlapiDateToString(new Date())  +' .csv';

        var file = nlapiCreateFile(fileName, 'CSV', html);
        file.setFolder('354237'); //Prod
        var fileID = nlapiSubmitFile(file);
        nlapiLogExecution('debug', 'File ID', fileID);

        return fileID;
    } catch (err) {
        nlapiLogExecution('DEBUG', 'Excel creation error', err);
    }
}

function searchEmployee()
{
	try
	{
		var search_ = nlapiLoadSearch('employee','customsearch2898');
		var filters = search_.getFilters();
		var columns = search_.getColumns();
		
		var o_data = new Array();
		
		var search_results = searchRecord('employee', null, filters,[
			columns[0], columns[1], columns[2], columns[3], columns[4], columns[5],
			columns[6], columns[7], columns[8], columns[9], columns[10], columns[11],
			columns[12],columns[13]
		]);
		
		if (search_results)
		{
			var unit_name = '';
			var unit_head = '';
			var unit_head_name = '';
			
			
			for(var i = 0; i < search_results.length; i++)
			{
				var emp_subsidiary = search_results[i].getValue(columns[13]);
				var emp_parent_practice = search_results[i].getValue(columns[12]);
				var emp_practice = search_results[i].getValue(columns[5]);
			
					if(emp_parent_practice == 510)
					{
						unit_name = 'Advanced Technology Group';
						unit_head = 102436;
						unit_head_name = 'Chander Damodaran';
					}
						
					if(emp_parent_practice == 322)
					{	
						if(emp_subsidiary == 3)
						{
							unit_name = 'Brillio Analytics (IN)';
							unit_head = 116376;
							unit_head_name = 'Yogesh Jain';
						}
						else
						{
							unit_name = 'Brillio Analytics (US)';
							unit_head = 116425;
							unit_head_name = 'Shailen Salvi';
						}
					}
						
					if(emp_parent_practice == 422)
					{
						unit_name = 'Finance(IN)';
						unit_head = 108386;
						unit_head_name = 'Maneesh Agarwal';
					}
						
					if(emp_parent_practice == 455)
					{
						unit_name = 'CRO & Strategic Alliance';
						unit_head = 116425;
						unit_head_name = 'Shailen Salvi';
					}
						
					if(emp_parent_practice == 454)
					{
						unit_name = 'India';
						unit_head = 102365;
						unit_head_name = 'Jayanth Selvappullai';
					}
						
					if(emp_parent_practice == 169)
					{
						unit_name = 'IS';
						unit_head = 102916;
						unit_head_name = 'Moses R';
					}	
					
					if(emp_parent_practice == 530)
					{
						if(emp_subsidiary == 3)
						{
							unit_name = 'CRM and Commerce';
							unit_head = 114902;
							unit_head_name = 'Suraj Gupta';
						}
						else
						{
							unit_name = 'CRM and Commerce(US)';
							unit_head = 116019;
							unit_head_name = 'Piyush Pandya';
						}
					}

					if(emp_parent_practice == 191)
					{
						if(emp_subsidiary == 3)
						{
							unit_name = 'Digital Infrastructure (IN)';
							unit_head = 112608;
							unit_head_name = 'Sivaramakrishna Perubotla';
						}
						else
						{
							unit_name = 'Digital Infrastructure (US)';
							unit_head = 114927;
							unit_head_name = 'Vijay Somanchi';
						}
					}

					if(emp_parent_practice == 70)
					{
					unit_name = 'Executive Office';
					unit_head = 108944;
					unit_head_name = 'Rajendra Mamodia';
					}
					
					if(emp_parent_practice == 72)
					{
						unit_name = 'Finance(IN)';
						unit_head = 108386;
						unit_head_name = 'Maneesh Agarwal';
					}
					
					if(emp_parent_practice == 415)
					{
					unit_name = 'Finance(IN)';
					unit_head = 108386;
					unit_head_name = 'Maneesh Agarwal';
					}
					
					if(emp_parent_practice == 520)
					{
					if(emp_subsidiary == 3)
					{
						unit_name = 'GDS (IN)';
						unit_head = 113586;
						unit_head_name = 'Benjamin Rowland Antonio Lourenco';
					}
					else
					{
						unit_name = 'GDS(US)';
						unit_head = 117809;
						unit_head_name = 'Hung Vu';
					}
					}
					
					if(emp_parent_practice == 410)
					{
					if(emp_subsidiary == 3)
					{
						unit_name = 'Human Resource (IN)';
						unit_head = 102365;
						unit_head_name = 'Jayanth Selvappullai';
					}
					else
					{
						unit_name = 'Human Resource(US)';
						unit_head = 114065;
						unit_head_name = 'Chaitali Mallick';
					}
					}
					
					if(emp_parent_practice == 363)
					{
						unit_name = 'India';
						unit_head = 102365;
						unit_head_name = 'Jayanth Selvappullai';
					}
						
					if(emp_parent_practice == 270)
					{
					unit_name = 'IS';
					unit_head = 102916;
					unit_head_name = 'Moses R';
					}
					
					if(emp_parent_practice == 271)
					{
					unit_name = 'IS';
					unit_head = 102916;
					unit_head_name = 'Moses R';
					}
					
					if(emp_parent_practice == 527)
					{
						if(emp_subsidiary == 3)
						{
						unit_name = 'PE(IN)';
						unit_head = 102531;
						unit_head_name = 'Sridhar Krishnamurthy';
						}
						else
						{
							unit_name = 'PE(US)';
							unit_head = 107637;
							unit_head_name = 'Vinod Subramanyam';
						}
					}
					if(emp_parent_practice == 518)
					{
					unit_name = 'Solution Management Group';
					unit_head = 116425;
					unit_head_name = 'Shailen Salvi';
					}
					
					if(emp_parent_practice == 504 || emp_practice == 542)
					{
					unit_name = 'Sales (IN)';
					unit_head = 108298;
					unit_head_name = 'Shishir Saxena';
					}
					
					if(emp_parent_practice == 424)
					{
					unit_name = 'Sales (IN)';
					unit_head = 108298;
					unit_head_name = 'Shishir Saxena';
					}
				
				
				o_data.push({
					'EmployeeId': search_results[i].getValue(columns[0]),
					'Firstname' : search_results[i].getValue(columns[1]),
					'Lastname'	: search_results[i].getValue(columns[2]),
					'EmailId'	: search_results[i].getValue(columns[3]),
					'RMID'		: search_results[i].getValue(columns[4]),
					'BU'		: unit_name,
					'Department' :	search_results[i].getText(columns[5]),
					'Designation' : search_results[i].getValue(columns[6]),
					'UnitHeadId' : unit_head,
					'UnitHeadName': unit_head_name,
					'Employeelevel': search_results[i].getText(columns[7]),
					'Location' : search_results[i].getText(columns[8]),
					'Gender' : search_results[i].getValue(columns[9]),
					'Birthdate' : search_results[i].getValue(columns[10]),
					'DOJ' : search_results[i].getValue(columns[11]),
				});
				
			}
		}
		
		return o_data;
	}
	catch (err)
	{
		
	}
}

//Blank Values
function _logValidation(value) {
    if (value != 'null' && value != null && value != '- None -' && value != null && value != '' && value != undefined && value != undefined && value != 'undefined' && value != 'undefined' && value != 'NaN' && value != NaN) {
        return true;
    } else {
        return false;
    }
}