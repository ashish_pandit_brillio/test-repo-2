/**
 * @author Sai
 */

function suiteletFunction_FP_RevRec_Dashboard(request,response)
{
	try
	{
		var o_context = nlapiGetContext();
		var i_user_logegdIn_id = o_context.getUser();
		var a_project_list = new Array();
		var a_project_search_results = '';
		
    
     
	if(i_user_logegdIn_id == 262780) //Added user Ashish to check the functionality 
		   i_user_logegdIn_id =7905;
      if(i_user_logegdIn_id == 242409) //Added user Sitaram  to check the functionality 
		   i_user_logegdIn_id =7905;
      
      if(i_user_logegdIn_id == 62082) //Added user Shamanth to check the functionality 
		   i_user_logegdIn_id =204539;
      if(i_user_logegdIn_id == 283840) //Added user Sitaram  to check the functionality 
		   i_user_logegdIn_id =7905;
		// design form which will be displayed to user
		var o_form_obj = nlapiCreateForm("FP Project Details Page");
		var a_project_filter = [];
		var a_currentuserFilter='';
		if(o_context.getDeploymentId()=="customdeploy_sut_fp_rev_rec_ado_details"){
			a_currentuserFilter=['customer.custentity_account_delivery_owner', 'anyof', i_user_logegdIn_id];
		}else if(o_context.getDeploymentId()=="customdeploy_sut_fp_rev_rec_pm_details"){
			a_currentuserFilter=['custentity_projectmanager', 'anyof', i_user_logegdIn_id];
		}
		else if(o_context.getDeploymentId()=="customdeploy_sut_fp_rev_rec_dm_details"){
			a_currentuserFilter=['custentity_deliverymanager', 'anyof', i_user_logegdIn_id];
		}
		else
		{
			a_currentuserFilter=[['custentity_projectmanager', 'anyof', i_user_logegdIn_id], 'or',
								['custentity_deliverymanager', 'anyof', i_user_logegdIn_id], 'or',
								['custentity_clientpartner', 'anyof', i_user_logegdIn_id], 'or',
								['custentity_practice.custrecord_practicehead','anyOf', i_user_logegdIn_id],'or',
								['customer.custentity_clientpartner', 'anyof', i_user_logegdIn_id]];
		}
		a_project_filter=[a_currentuserFilter, 'and',
								['status', 'noneof', 1], 'and',
								['jobtype', 'anyof', 2], 'and',
								['jobbillingtype', 'anyof', 'FBM'], 'and',
								['custentity_fp_rev_rec_type', 'anyof', 1]];
								
								
								
		var a_columns_proj_srch = new Array();
		a_columns_proj_srch[0] = new nlobjSearchColumn('custentity_projectvalue');
		a_columns_proj_srch[1] = new nlobjSearchColumn('customer');
		a_columns_proj_srch[2] = new nlobjSearchColumn('custentity_fp_rev_rec_type');
		a_columns_proj_srch[3] = new nlobjSearchColumn('custentity_ytd_rev_recognized');
		
		if(parseInt(i_user_logegdIn_id) == parseInt(41571)|| parseInt(i_user_logegdIn_id) == parseInt(7905)		||
			parseInt(i_user_logegdIn_id) == parseInt(258172)|| parseInt(i_user_logegdIn_id) == parseInt(7981) 	||  
			parseInt(i_user_logegdIn_id) == parseInt(112319)|| parseInt(i_user_logegdIn_id) == parseInt(5748) 	||
			parseInt(i_user_logegdIn_id) == parseInt(133724) || parseInt(i_user_logegdIn_id) == parseInt(204539) || 
			parseInt(i_user_logegdIn_id) == parseInt(271123)|| parseInt(i_user_logegdIn_id) == parseInt(294103)  || 
			parseInt(i_user_logegdIn_id) == parseInt(262780)
		) 
		{
			var a_project_filter = [['status', 'noneof', 1], 'and',
								['jobtype', 'anyof', 2], 'and',
								['jobbillingtype', 'anyof', 'FBM'], 'and',
								['custentity_fp_rev_rec_type', 'anyof', 1]];
								
			a_project_search_results = searchRecord('job', null, a_project_filter, a_columns_proj_srch);
		}
		else
		{
			a_project_search_results = searchRecord('job', null, a_project_filter, a_columns_proj_srch);
		}
		 
		if(a_project_search_results)
		{
		var a_get_logged_in_user_exsiting_revenue_cap = '';
			var a_revenue_cap_filter = [];
			if(o_context.getDeploymentId()=="customdeploy_sut_fp_rev_rec_ado_details"){
			a_revenue_cap_filter=[['custrecord_revenue_share_cust.custentity_account_delivery_owner', 'anyof', i_user_logegdIn_id]];
			}
			else if(o_context.getDeploymentId()=="customdeploy_sut_fp_rev_rec_pm_details"){
			a_currentuserFilter=['custrecord_revenue_share_project.custentity_projectmanager', 'anyof', i_user_logegdIn_id];
			}
			else if(o_context.getDeploymentId()=="customdeploy_sut_fp_rev_rec_dm_details"){
			a_currentuserFilter=['custrecord_revenue_share_project.custentity_deliverymanager', 'anyof', i_user_logegdIn_id];
			}
			else{
					a_revenue_cap_filter=[['custrecord_revenue_share_project.custentity_projectmanager', 'anyof', i_user_logegdIn_id], 'or',
					['custrecord_revenue_share_project.custentity_deliverymanager', 'anyof', i_user_logegdIn_id], 'or',
					['custrecord_revenue_share_project.custentity_clientpartner', 'anyof', i_user_logegdIn_id], 'or',
					['custrecord_revenue_share_cust.custentity_clientpartner', 'anyof', i_user_logegdIn_id]];
			}
			
			var a_columns_existing_cap_srch = new Array();
			a_columns_existing_cap_srch[0] = new nlobjSearchColumn('custrecord_revenue_share_project');
			a_columns_existing_cap_srch[1] = new nlobjSearchColumn('custrecord_revenue_share_total');
			a_columns_existing_cap_srch[2] = new nlobjSearchColumn('created').setSort(true);
			a_columns_existing_cap_srch[3] = new nlobjSearchColumn('custrecord_revenue_share_approval_status');
			if(parseInt(i_user_logegdIn_id) == parseInt(41571)|| parseInt(i_user_logegdIn_id) == parseInt(7905)		||
				parseInt(i_user_logegdIn_id) == parseInt(258172)|| parseInt(i_user_logegdIn_id) == parseInt(7981) 	||  
				parseInt(i_user_logegdIn_id) == parseInt(112319)|| parseInt(i_user_logegdIn_id) == parseInt(5748) 	||
				parseInt(i_user_logegdIn_id) == parseInt(133724) || parseInt(i_user_logegdIn_id) == parseInt(204539) || 
				parseInt(i_user_logegdIn_id) == parseInt(271123)|| parseInt(i_user_logegdIn_id) == parseInt(294103) ||
				parseInt(i_user_logegdIn_id) == parseInt(262780)
			) 
			a_get_logged_in_user_exsiting_revenue_cap = searchRecord('customrecord_revenue_share', null, null, a_columns_existing_cap_srch);
			else
			 a_get_logged_in_user_exsiting_revenue_cap = searchRecord('customrecord_revenue_share', null, a_revenue_cap_filter, a_columns_existing_cap_srch);
			if(a_get_logged_in_user_exsiting_revenue_cap)
			{
				var a_existig_cap_projects = new Array();
				var a_existing_proj_cap_details = new Array();
				var i_index_existing_pro = 0;
				for(var i_existing_cap = 0; i_existing_cap < a_get_logged_in_user_exsiting_revenue_cap.length; i_existing_cap++)
				{
					if(a_existig_cap_projects.indexOf(a_get_logged_in_user_exsiting_revenue_cap[i_existing_cap].getValue('custrecord_revenue_share_project')) < 0)
					{
						//nlapiLogExecution('audit','existing proj index:- ',a_get_logged_in_user_exsiting_revenue_cap[i_existing_cap].getValue('custrecord_revenue_share_project'));
						
						a_existig_cap_projects.push(a_get_logged_in_user_exsiting_revenue_cap[i_existing_cap].getValue('custrecord_revenue_share_project'));
						a_existing_proj_cap_details[i_index_existing_pro] = {
																				'proj_intenal_id': a_get_logged_in_user_exsiting_revenue_cap[i_existing_cap].getValue('custrecord_revenue_share_project'),
																				'pro_revenue_share': a_get_logged_in_user_exsiting_revenue_cap[i_existing_cap].getValue('custrecord_revenue_share_total'),
																				'existing_rcrd_id': a_get_logged_in_user_exsiting_revenue_cap[i_existing_cap].getId(),
																				'existing_rcrd_status': a_get_logged_in_user_exsiting_revenue_cap[i_existing_cap].getValue('custrecord_revenue_share_approval_status'),
																				's_existing_rcrd_status': a_get_logged_in_user_exsiting_revenue_cap[i_existing_cap].getText('custrecord_revenue_share_approval_status')
																				
																			};
						i_index_existing_pro++;
					}					
				}	
			}
		
			var a_JSON = {};
			var s_create_update_mode = '';
			var linkUrl	=	nlapiResolveURL('SUITELET', '1139', 'customdeploy_sut_fp_rev_rec_proj_details');
			
			for(var i_pro_index = 0; i_pro_index < a_project_search_results.length; i_pro_index++)
			{
				var i_project_internal_id = a_project_search_results[i_pro_index].getId();
				var f_project_existing_revenue = 0;
				var i_existing_revenue_share_rcrd_id = 0;
				var b_is_proj_value_chngd_flag = 'F';
				var s_existing_revenue_share_status = '';
				if(a_existig_cap_projects.indexOf(a_project_search_results[i_pro_index].getId()) >= 0)
				{
					//nlapiLogExecution('audit','a_existig_cap_projects '+a_project_search_results[i_pro_index].getId,a_existig_cap_projects.length);
					s_create_update_mode = 'Update';
					
					s_revenue_share_status = a_existing_proj_cap_details[a_existig_cap_projects.indexOf(a_project_search_results[i_pro_index].getId())].s_existing_rcrd_status;
					if(!s_revenue_share_status)
					{
						s_revenue_share_status = 'Saved';
					}
					
					f_project_existing_revenue = a_existing_proj_cap_details[a_existig_cap_projects.indexOf(a_project_search_results[i_pro_index].getId())].pro_revenue_share;
					i_existing_revenue_share_rcrd_id = a_existing_proj_cap_details[a_existig_cap_projects.indexOf(a_project_search_results[i_pro_index].getId())].existing_rcrd_id;
					s_existing_revenue_share_status = a_existing_proj_cap_details[a_existig_cap_projects.indexOf(a_project_search_results[i_pro_index].getId())].existing_rcrd_status;
				}
				else
				{
					s_create_update_mode = 'Create';
					s_revenue_share_status = 'Not Submitted';
				}
				
				var i_project_value_ytd = a_project_search_results[i_pro_index].getValue('custentity_ytd_rev_recognized');
				
				if(!i_project_value_ytd)
					i_project_value_ytd = 0;
				
				var f_proj_total_value = a_project_search_results[i_pro_index].getValue('custentity_projectvalue');
				f_proj_total_value = parseFloat(f_proj_total_value) - parseFloat(i_project_value_ytd);
				f_proj_total_value = parseFloat(f_proj_total_value).toFixed(2);
				if((parseFloat(f_project_existing_revenue) != parseFloat(f_proj_total_value)) && s_create_update_mode != 'Create')
				{
					s_existing_revenue_share_status = '';
					s_revenue_share_status = 'Saved';
					b_is_proj_value_chngd_flag = 'T';
					nlapiSubmitField('customrecord_revenue_share',i_existing_revenue_share_rcrd_id,'custrecord_revenue_share_approval_status','');
					nlapiSubmitField('customrecord_revenue_share',i_existing_revenue_share_rcrd_id,'custrecord_sow_value_change_status','Saved');
				}
				
				var i_project_sow_value = a_project_search_results[i_pro_index].getValue('custentity_projectvalue');
				i_project_sow_value = parseFloat(i_project_sow_value) - parseFloat(i_project_value_ytd);
				i_project_sow_value = parseFloat(i_project_sow_value).toFixed(2);
				
				a_JSON = {
							project_cutomer: a_project_search_results[i_pro_index].getValue('customer'),
							project_id: a_project_search_results[i_pro_index].getId(),
							project_value: i_project_sow_value,
							revenue_share_status: s_revenue_share_status,
							project_existing_value: f_project_existing_revenue,
							project_rev_rec_type: a_project_search_results[i_pro_index].getValue('custentity_fp_rev_rec_type'),
							suitelet_url: linkUrl + '&proj_id=' + a_project_search_results[i_pro_index].getId() +'&mode=' + s_create_update_mode +'&existing_rcrd_id=' +i_existing_revenue_share_rcrd_id+'&revenue_share_status='+s_existing_revenue_share_status+'&proj_val_chngd='+b_is_proj_value_chngd_flag
						};
				
				a_project_list.push(a_JSON);
			}
			
			//create sublist for project
			var f_form_sublist = o_form_obj.addSubList('project_sublist','list','Project List');
			f_form_sublist.addField('project_cutomer','select','Customer','customer').setDisplayType('inline');
			f_form_sublist.addField('project_id','select','Project','job').setDisplayType('inline');
			f_form_sublist.addField('project_value','currency','Project Value');
			f_form_sublist.addField('revenue_share_status','text','Project Setup Status');
			f_form_sublist.addField('project_existing_value','currency','Captured Revenue Share');
			f_form_sublist.addField('project_rev_rec_type','select','Rev Rec Type','customlist_fp_rev_rec_project_type').setDisplayType('inline');
			f_form_sublist.addField('suitelet_url', 'url','').setLinkText('Update');
			f_form_sublist.setLineItemValues(a_project_list);
		
		}
		else
		{
			var s_result_not_found = "<table>";
			s_result_not_found += "<tr><td>";
			s_result_not_found += "NO RESULTS FOUND";
			s_result_not_found += "</td></tr>";
			s_result_not_found += "</table>";
			
			var f_no_rslt_found = o_form_obj.addField('rcrd_nt_found', 'inlinehtml', 'No Record Found');
			f_no_rslt_found.setDefaultValue(s_result_not_found);
		}
		
		response.writePage(o_form_obj);
	}
	catch(err)
	{
		nlapiLogExecution('ERROR','function suiteletFunction_FP_RevRec_Dashboard','ERROR MESSAGE :- '+err);
	}
}

function searchRecord(recordType, savedSearch, arrFilters, arrColumns,
        filterExpression)
{

	try {
		var search = null;

		// if a saved search is provided, load it and add the filters and
		// columns
		if (isNotEmpty(savedSearch)) {
			search = nlapiLoadSearch(recordType, savedSearch);

			if (isArrayNotEmpty(arrFilters)) {
				search.addFilters(arrFilters);
			}

			if (isArrayNotEmpty(arrColumns)) {
				search.addColumns(arrColumns);
			}

			if (isArrayNotEmpty(filterExpression)) {
				search.setFilterExpression(filterExpression);
			}
		}
		// create a new search
		else {
			search = nlapiCreateSearch(recordType, arrFilters, arrColumns);
		}

		// run search
		var resultSet = search.runSearch();

		// iterate through the search and get all data 1000 at a time
		var searchResultCount = 0;
		var resultSlice = null;
		var searchResult = [];

		do {
			resultSlice = resultSet.getResults(searchResultCount,
			        searchResultCount + 1000);

			if (resultSlice) {

				resultSlice.forEach(function(result) {

					searchResult.push(result);
					searchResultCount++;
				});
			}
		} while (isArrayNotEmpty(resultSlice) && resultSlice.length >= 1000);

		return searchResult;
	} catch (err) {
		nlapiLogExecution('ERROR', 'searchRecord', err);
		throw err;
	}
}

function isEmpty(value) {

	return value == null || value == "" || typeof (value) == undefined;
}

function isNotEmpty(value) {

	return !isEmpty(value);
}

function isArrayEmpty(argArray) {

	return !isArrayNotEmpty(argArray);
}

function isArrayNotEmpty(argArray) {

	return (isNotEmpty(argArray) && argArray.length > 0);
}