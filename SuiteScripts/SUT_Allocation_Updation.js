/**
 * Update the allocation details for any project
 * 
 * Version    Date            Author           Remarks
 * 1.00       22 Jun 2015     Nitish Mishra
 *-- Date --			-- Modified By --				--Requested By--				-- Description --
 *  07 FEB 2020			Praveena Madem						Deepak						Updated logic to fetch subtier vendor data and resource allocation details display and processing
 *
 *
 */

/**
 * @param {nlobjRequest}
 *            request Request object
 * @param {nlobjResponse}
 *            response Response object
 * @returns {Void} Any output is written via response object
 */
function suitelet(request, response) {
    try {

        var mode = request.getMethod();

        if (mode == 'POST') {
            submitAllocationDetails(request);
			nlapiLogExecution("DEBUG","nlapiGetContext().getRemainingUsage()",nlapiGetContext().getRemainingUsage());
        } else {

            var projectId = request.getParameter('project');

            if (projectId) {
                generateAllocationUpdateForm(projectId, request.getParameter('enddate'));
            } else {
                generateProjectSelectionForm();
            }
        }
    } catch (err) {
        nlapiLogExecution('ERROR', 'suitelet', err);
    }
}

function generateProjectSelectionForm() {
    try {

        var form = nlapiCreateForm('Allocation Updation');
        form.setScript('customscript_cs_sut_alloc_update');

        form
            .addField('custpage_project', 'select', 'Select any project',
                'job');

        form
            .addButton('btn_submit', 'Get Project Allocations',
                'selectProject');
        response.writePage(form);
    } catch (err) {
        nlapiLogExecution('ERROR', 'generateProjectSelectionForm', err);
        throw err;
    }
}

function selectProject() {
    try {

        var projectId = nlapiGetFieldValue('custpage_project');

        if (projectId) {
            var url = nlapiResolveURL('SUITELET',
                    'customscript_sut_alloc_update',
                    'customdeploy_sut_alloc_update') +
                "&project=" + projectId;
            window.location = url;
        } else {
            alert('Please select any project');
        }
    } catch (err) {
        alert(err.message);
    }
}

function generateAllocationUpdateForm(projectId, endDate) {
    try {

        if (projectId) {
            var projectDetails = nlapiLookupField('job', projectId, [
                'entityid', 'companyname', 'enddate'
            ]);

            var form = nlapiCreateForm('Allocation Updation - ' +
                projectDetails.entityid + " : " +
                projectDetails.companyname);

            form.setScript('customscript_cs_sut_alloc_update');
            form.addField('custpage_project', 'select', 'Selected Project',
                'job').setDisplayType('hidden').setDefaultValue(projectId);

            // project end date (current)
            form.addField('custpage_old_job_end_date', 'date',
                    'Project End Date (Current)').setDisplayType('inline')
                .setDefaultValue(projectDetails.enddate);

            var endDateField = form.addField('custpage_end_date', 'date',
                'To Allocation Date - Click Change Date');

            // project end date (new)
            form.addField('custpage_new_job_end_date', 'date',
                'Project End Date (New)');

            var newEndDateField = form
                .addField('custpage_new_end_date', 'date',
                    'New End Date (To Be Updated In All Allocation - Click Update End Dates)');

            //nlapiLogExecution('debug', 'enddate', endDate);

            if (endDate)
                endDateField.setDefaultValue(endDate);

            var allocationList = form.addSubList('custpage_allocation_list',
                'list', 'Allocations');

            allocationList.addField('id', 'select', 'allocation',
                'resourceallocation').setDisplayType('hidden');
            allocationList.addField('resource', 'select', 'Resource',
                'employee').setDisplayType('inline');

            allocationList.addField('employeetype', 'select', 'Employee Type', 'customlist_persontype').setDisplayType('hidden');
            allocationList.addField('employeesubsidiary', 'select', 'Subsidiary', 'subsidiary').setDisplayType('hidden');
            allocationList.addField('startdate', 'date', 'Start Date');
            allocationList.addField('enddate', 'date', 'Current End Date');
            allocationList.addField('newenddate', 'date', 'New End Date')
                .setDisplayType('entry');

            allocationList.addField('refvendorname', 'text', 'Refferal Vendor Name').setDisplayType('inline');
            allocationList.addField('refenddate', 'date', 'Current Ref End Date');
            allocationList.addField('newrefenddate', 'date', 'New Ref End Date').setDisplayType('entry');

            //allocationList.addField('subrefvencheck', 'checkbox', 'Extend Subtier Referral Vendor');
			allocationList.addField('resourcename', 'text', 'resourcename').setDisplayType('hidden');//TO display on UI when data is more
            allocationList.setLineItemValues(getAllocationForSelectedProject(
                projectId, endDate, allocationList));

            form.addButton('custpage_btn_update', 'Update End Dates',
                'updateEndDates');
            form.addButton('custpage_btn_refresh', 'Change Date', 'refresh');
            form.addSubmitButton('Submit');
            response.writePage(form);
        } else {
            generateProjectSelectionForm();
        }
    } catch (err) {
        nlapiLogExecution('ERROR', 'generateAllocationUpdateForm', err);
        throw err;
    }
}

function refresh() {
    window.location = nlapiResolveURL('SUITELET',
            'customscript_sut_alloc_update', 'customdeploy_sut_alloc_update') +
        '&project=' +
        nlapiGetFieldValue('custpage_project') +
        '&enddate=' + nlapiGetFieldValue('custpage_end_date');
}

function updateEndDates() {
    var newEndDate = nlapiGetFieldValue('custpage_new_end_date');

    if (newEndDate) {
        var count = nlapiGetLineItemCount('custpage_allocation_list');

        for (var i = 1; i <= count; i++) {
            nlapiSetLineItemValue('custpage_allocation_list', 'newenddate', i,
                newEndDate);
        }
    }
}
function getAllocationForSelectedProject(projectId, endDate,form) {
	try {
		var allocationSublistDetails = [];var a_Vendor_details = [];
		if (!endDate)
			endDate = 'today';
		//var internla_limit = '35000';
		
		
		//FETCH Subtier Vendor Data and store into an array
		searchRecord('customrecord_subtier_vendor_data', null, [new nlobjSearchFilter('custrecord_stvd_vendor_type', null, 'anyof', 2),
        new nlobjSearchFilter('custentity_employee_inactive', "custrecord_stvd_contractor", "is", "F"),
        new nlobjSearchFilter("custentity_implementationteam", "custrecord_stvd_contractor", "is", "F"),
        new nlobjSearchFilter('isjobresource', 'custrecord_stvd_contractor', 'is', 'T')
    ],
    [
         new nlobjSearchColumn('custrecord_stvd_contractor').setSort(true),
         new nlobjSearchColumn('custrecord_stvd_end_date').setSort(false),
		 new nlobjSearchColumn('custrecord_stvd_vendor')
    ]).forEach(function(results) {
    var contractor = results.getValue('custrecord_stvd_contractor');
    if (contractor && a_Vendor_details.indexOf(contractor) == -1) {
        a_Vendor_details[contractor] = {
            vendorname: results.getText('custrecord_stvd_vendor'),
            refendDate: results.getValue('custrecord_stvd_end_date'),
            contractor: results.getValue('custrecord_stvd_contractor')
        };
    }
});

//FETCH RESOURCE ALLOCATION DETAILS
searchRecord(
    'resourceallocation',
    null,
    [
        new nlobjSearchFilter('project', null, 'anyof',
            projectId),
        new nlobjSearchFilter('custentity_employee_inactive',
            'employee', 'is', 'F'),
        new nlobjSearchFilter('custentity_implementationteam',
            'employee', 'is', 'F'),
        new nlobjSearchFilter('enddate', null, 'notbefore', endDate),
        new nlobjSearchFilter('isjobresource', 'employee', 'is', 'T')
        //    ,
        // new nlobjSearchFilter('formulanumeric', 'employee', 'greaterthanorequalto',
        //	40000).setFormula('{internalid}')
    ],

    [new nlobjSearchColumn('startdate'),
        new nlobjSearchColumn('enddate').setSort(true),
        new nlobjSearchColumn('resource'),
        new nlobjSearchColumn('subsidiary', 'resource'),
        new nlobjSearchColumn('custentity_persontype', 'resource')
    ]).forEach(
    function(allocation) {
        var resource = allocation.getValue('resource');
        var refendDate = a_Vendor_details[resource] ? a_Vendor_details[resource].refendDate : '';
        var vendorname = a_Vendor_details[resource] ? a_Vendor_details[resource].vendorname : '';

        allocationSublistDetails.push({
            id: allocation.getId(),
            refvendorname: vendorname,
            resource: allocation.getValue('resource'),
            employeetype: allocation.getValue('custentity_persontype', 'resource'),
            startdate: allocation.getValue('startdate'),
            enddate: allocation.getValue('enddate'),

            employeesubsidiary: allocation.getValue('subsidiary', 'resource'),
            refenddate: refendDate,
            resourcename: allocation.getText('resource')
        });
    });
a_Vendor_details = [];
return allocationSublistDetails; 
	}
	catch(e)
	{
		nlapiLogExecution('ERROR', 'getActiveAllocationForSelectedProject', err);
		throw err;
	}
}
/*
function getAllocationForSelectedProject(projectId, endDate,form) {
	try {
		var allocationSublistDetails = [];
		var internla_limit = '35000';
		if (!endDate)
			endDate = 'today';

		searchRecord(
		        'resourceallocation',
		        null,
		        [
		                new nlobjSearchFilter('project', null, 'anyof',
		                        projectId),
		                new nlobjSearchFilter('custentity_employee_inactive',
		                        'employee', 'is', 'F'),
		                new nlobjSearchFilter('custentity_implementationteam',
		                        'employee', 'is', 'F'),
		               new nlobjSearchFilter('enddate', null, 'notbefore',endDate),
		                new nlobjSearchFilter('isjobresource', 'employee','is', 'T')
						    //    ,
		               // new nlobjSearchFilter('formulanumeric', 'employee', 'greaterthanorequalto',
		    	       	//	40000).setFormula('{internalid}')
		    	        		],
		                
		                [ new nlobjSearchColumn('startdate'),
		                new nlobjSearchColumn('enddate').setSort(true),
		                new nlobjSearchColumn('resource'),
						new nlobjSearchColumn('subsidiary','resource'),
		                new nlobjSearchColumn('custentity_persontype','resource')]).forEach(
		        function(allocation) {
		        	
		        	var contractor = allocation.getValue('resource');
		        	
		        	var vendorname = '';
		        	var refendDate = '';
		        	var results = searchRecord('customrecord_subtier_vendor_data',null,[
		        	                      new nlobjSearchFilter('custrecord_stvd_contractor', null, 'anyof',contractor),
		        	                      new nlobjSearchFilter('custrecord_stvd_vendor_type', null, 'anyof',2)],
		        	                      [
		        	                       new nlobjSearchColumn('custrecord_stvd_vendor'),
		        	                       new nlobjSearchColumn('custrecord_stvd_end_date').setSort(true)
		        	                       ]);
		        	
		        	if(results!=''){
		        				        		
		        		vendorname = results[0].getText('custrecord_stvd_vendor');
		        		nlapiLogExecution('Debug', 'vendorname', results[0].getText('custrecord_stvd_vendor'));
		        		refendDate = results[0].getValue('custrecord_stvd_end_date');
		        	}
					
			        allocationSublistDetails.push({
			            id : allocation.getId(),
			            resource : allocation.getValue('resource'),
			            employeetype : allocation.getValue('custentity_persontype','resource'),
			            startdate : allocation.getValue('startdate'),
			            enddate : allocation.getValue('enddate'),
			            refvendorname : vendorname,
						employeesubsidiary : allocation.getValue('subsidiary','resource'),
			            refenddate : refendDate,
						resourcename:allocation.getText('resource')
			        });
		        });
nlapiLogExecution('DEBUG', 'allocationSublistDetails', JSON.stringify(allocationSublistDetails));
		return allocationSublistDetails;
	} catch (err) {
		nlapiLogExecution('ERROR', 'getActiveAllocationForSelectedProject', err);
		throw err;
	}
}

*/




function submitAllocationDetails(request) {
    try {
        var projectId = request.getParameter('custpage_project');
        var selectedDate = request.getParameter('custpage_end_date');
        var lineItemCount = request
            .getLineItemCount('custpage_allocation_list');
        //nlapiLogExecution('debug', 'lineItemCount', lineItemCount);

        var new_project_end_date = request
            .getParameter('custpage_new_job_end_date');
        //nlapiLogExecution('debug', 'new project end date', new_project_end_date);
        var old_project_end_date = request.getParameter('custpage_old_job_end_date');

        if (new_project_end_date) {
            nlapiSubmitField('job', projectId, 'enddate', new_project_end_date,
                true);
            //	nlapiLogExecution('debug', 'project end date updated', projectId);
        }

        var tableData = "";
        var employeeSubRefFlag = false;



        /*	var extendSubRefV = false;
        	if(employeeSubRefFlag){
        		 var r = window.confirm("One or more Subtier Vendor extensions are found. Do you want to extend? ");
        		    if (r == true) {
        		    	extendSubRefV = true;
        		    }
        	}*/
        
		
		var flag_check=0;
		//FOr Loop is used to check the entered New End Date or Referral End Date count to process through back end schedule or suitelet 
		for (var linenum = 1; linenum <= lineItemCount; linenum++) {
			
			var newEndDate = request.getLineItemValue(
			        'custpage_allocation_list', 'newenddate', linenum);
			var newrefenddate = request.getLineItemValue(
			        'custpage_allocation_list', 'newrefenddate', linenum);
					if(newEndDate || newrefenddate)
					{
						flag_check++;
					}
					
		}
		
		var projectDetails = nlapiLookupField('job', projectId, [
            'entityid', 'companyname'
        ]);
        var projectName = projectDetails.entityid + " : " +
            projectDetails.companyname;
        var userName = nlapiGetContext().getName();
		
		var form = nlapiCreateForm('Allocation Updation - ' +
            projectDetails.entityid + " : " +
            projectDetails.companyname);
			
			
		if(flag_check>6){
	     tableData=_update_bulk_date(lineItemCount,request,tableData,projectName,userName,new_project_end_date,old_project_end_date)//Call SCH script to process records
		 form.addField('bulkshowmessage', 'text', 'Message').setDisplayType('inline').setDefaultValue('Allocation Extension details are bulk and the Email will be sent to business.ops@brillio.com; shruthi.l@brillio.com once the Allocation Extension has been done');
		}
		else{
			tableData=update_few_ra_Data(lineItemCount,request,tableData,new_project_end_date,old_project_end_date);//Calling function to process data.
			// send email
				if (tableData) {
					 var tableDate2 = "<table><tr><td>Resource</td><td>Old End Date</td><td>New End Date</td><td>Old Referral End Date</td><td>New Referral End Date</td></tr>" +
                tableData + "</table>";
					var mailContent = getAllocationUpdateMailTemplate(tableDate2,
						userName, projectName);
					// get the email id from the context
					var emails = "business.ops@brillio.com; shruthi.l@brillio.com";

					if (emails) {
						nlapiSendEmail(constant.Mail_Author.InformationSystems, emails,
							mailContent.Subject, mailContent.Body);
					}
				}
				
		 var tableDate1 = "<table><tr><td>Resource</td><td>Old End Date</td><td>New End Date</td><td>Old Referral End Date</td><td>New Referral End Date</td></tr>" +
            tableData + "</table>";
        //nlapiLogExecution('debug', 'tableDate1', tableData);

        
        var htmltext = '<style>td{padding-right: 20px;}</style><p>This is to inform you that <b>' + userName +
            '</b> has updated allocation details for <b>' + projectName +
            '</b>.</p>';

        htmltext += tableDate1;

        
        form.addField('custpage_html', 'inlinehtml', 'html',
            'job').setDefaultValue(htmltext);
		}
		
        

       

        response.writePage(form);
        //generateAllocationUpdateForm(projectId, selectedDate);
    } catch (err) {
        nlapiLogExecution('ERROR', 'submitAllocationDetails', err);
        throw err;
    }
}

function getAllocationUpdateMailTemplate(tableData, userName, projectName) {
    var htmltext = '<table border="0" width="100%">';
    htmltext += '<tr>';
    htmltext += '<td colspan="4" valign="top">';
    htmltext += '<p>Hi,</p>';
    htmltext += '<p></p>';
    htmltext += '<p>This is to inform you that <b>' + userName +
        '</b> has updated allocation details for <b>' + projectName +
        '</b>.</p>';

    htmltext += tableData;

    htmltext += '<p></p>';
    htmltext += '</td></tr>';
    htmltext += '<tr></tr>';
    htmltext += '<tr></tr>';
    htmltext += '<p><br/>Regards,<br/>';
    htmltext += 'Information Systems</p>';
    htmltext += '</table>';
    htmltext += '<hr width="100%" size="1" noshade color="#CCCCCC">';
    htmltext += '<table width="100%" border="0" cellspacing="0" cellpadding="0">';
    htmltext += '<tr>';
    htmltext += '<td align="right">';
    htmltext += '<font style="font-size:9px; font-family:Verdana,Arial,Helvetica,sans-serif; color:#999999;">Brillio is powered by <a href="http://www.netsuite.com/" style="color:#999999;">NetSuite</a> &#151; One System. No Limits.</font>';
    htmltext += '</td>';
    htmltext += '</tr>';
    htmltext += '</table>';
    htmltext += '</body>';
    htmltext += '</html>';

    return {
        Subject: 'Allocation Updation',
        Body: htmltext
    };
};

function _update_bulk_date(lineItemCount,request,tableData,projectname,username,new_project_end_date,old_project_end_date)
{
	var a_update_allocationDetails=[];
	
        for (var linenum = 1; linenum <= lineItemCount; linenum++) {
            var newEndDate = request.getLineItemValue(
                'custpage_allocation_list', 'newenddate', linenum);
            var empsubsidiary = request.getLineItemValue('custpage_allocation_list', 'employeesubsidiary', linenum);
            nlapiLogExecution('debug', 'Subsidiary', empsubsidiary);
            //nlapiLogExecution('debug', 'new allocation end date', newEndDate);
            var newrefenddate = request.getLineItemValue('custpage_allocation_list', 'newrefenddate', linenum);
			var i_resource=request.getLineItemValue('custpage_allocation_list', 'resource', linenum);
			var s_resource=request.getLineItemValue('custpage_allocation_list', 'resourcename', linenum);
			var enddate=request.getLineItemValue('custpage_allocation_list', 'enddate', linenum);
			var refenddate=request.getLineItemValue('custpage_allocation_list', 'refenddate', linenum);
			//nlapiLogExecution('DEBUG','i_resource',i_resource);
			//nlapiLogExecution('DEBUG','s_resource',s_resource);
            if (newEndDate || newrefenddate) {
                try {
					
					/*var a_update_allocationDetails=[];
					a_update_allocationDetails[0]={"14":12};
					a_update_allocationDetails[0][14];*/
					var id=request.getLineItemValue('custpage_allocation_list', 'id', linenum);
					//nlapiLogExecution('DEBUG','id',id);
					var obj_allocation_data="";
					obj_allocation_data={
							"id" : request.getLineItemValue('custpage_allocation_list', 'id', linenum),
							"newEndDate" : newEndDate,
							"newrefenddate" : newrefenddate,
							"empsubsidiary" : empsubsidiary,
							"enddate":enddate,
							"refenddate":refenddate
							};
					if (empsubsidiary == '2') {
						obj_allocation_data["isvendor"]=request.getLineItemValue('custpage_allocation_list', 'refvendorname', linenum);
						obj_allocation_data["isrefendDate"]=request.getLineItemValue('custpage_allocation_list', 'newrefenddate', linenum);
					}						
					   a_update_allocationDetails.push(obj_allocation_data);
					   
						//nlapiLogExecution('DEBUG','a_update_allocationDetails[id]',JSON.stringify(a_update_allocationDetails[id]));
						//nlapiLogExecution('debug', 'allocation updated',allocationId);
                    //nlapiLogExecution('debug', 'Resource updated', allocationResource);
                    // generating content for email
                    tableData += "<tr>";
                    tableData += "<td>" +
                        s_resource +
                        "</td>";
                    tableData += "<td>" +
                        request.getLineItemValue(
                            'custpage_allocation_list', 'enddate',
                            linenum) + "</td>";
                    tableData += "<td>" +
                        request.getLineItemValue(
                            'custpage_allocation_list', 'newenddate',
                            linenum) + "</td>";
                    tableData += "<td>" +
                        request.getLineItemValue(
                            'custpage_allocation_list', 'refenddate',
                            linenum) + "</td>";
                    tableData += "<td>" +
                        request.getLineItemValue(
                            'custpage_allocation_list', 'newrefenddate',
                            linenum) + "</td>";
                    tableData += "</tr>";
                } catch (e) {

                    nlapiLogExecution('error',
                        'Failed while updating the record', e);
                    tableData += "<tr>";
                    tableData += "<td>" +
                        s_resource +
                        "</td>";
                    tableData += "<td>" +
                        request.getLineItemValue(
                            'custpage_allocation_list', 'enddate',
                            linenum) + "</td>";
                    if (e.name == '404') {
                        tableData += "<td>" +
                            request.getLineItemValue(
                                'custpage_allocation_list', 'newenddate',
                                linenum) + "</td>";

                    } else {

                        tableData += "<td>" + e + "</td>";

                    }
                    tableData += "<td>" +
                        request.getLineItemValue(
                            'custpage_allocation_list', 'refenddate',
                            linenum) + "</td>";
                    if (e.name != '404') {
                        tableData += "<td>" +
                            request.getLineItemValue(
                                'custpage_allocation_list', 'refenddate',
                                linenum) + "</td>";
                    } else {
                        tableData += "<td>" + e + "</td>";
                    }
                    tableData += "</tr>";
                }
            }
        }

				if(a_update_allocationDetails.length>0)
				{
					var param=new Array();
					param['custscript_update_allocation_details']=JSON.stringify(a_update_allocationDetails);
					param['custscript_projectname']=projectname;
					param['custscript_username']=username;
					param['custscript_new_project_end_date']=new_project_end_date;
					param['custscript_old_project_enddate']=old_project_end_date;
					 nlapiScheduleScript ('customscript_sch_ra_extension',null,param);
					 nlapiLogExecution('DEBUG','a_update_allocationDetails',JSON.stringify(a_update_allocationDetails));
				}
				
				return tableData;
}

function update_few_ra_Data(lineItemCount,request,tableData,new_project_end_date,old_project_end_date)
{
	for (var linenum = 1; linenum <= lineItemCount; linenum++) {

			var newEndDate = request.getLineItemValue(
			        'custpage_allocation_list', 'newenddate', linenum);
			var empsubsidiary = request.getLineItemValue(
					'custpage_allocation_list', 'employeesubsidiary', linenum);
				//nlapiLogExecution('debug', 'Subsidiary', empsubsidiary);
			//nlapiLogExecution('debug', 'new allocation end date', newEndDate);
			var newrefenddate = request.getLineItemValue(
			        'custpage_allocation_list', 'newrefenddate', linenum);
			
			if (newEndDate || newrefenddate) {
				var allocationId = request.getLineItemValue(
				        'custpage_allocation_list', 'id', linenum);
						

				var allocationRecord = nlapiLoadRecord('resourceallocation',
				        allocationId);
				
				var allocationResource =allocationRecord.getFieldValue('allocationresource');
				
				
				try {
					
					allocationRecord.setFieldValue('enddate', newEndDate);
					allocationRecord.setFieldValue('custeventbenddate',
					        newEndDate);
					allocationRecord.setFieldValue('notes', 'Project Extended');
					allocationRecord.setFieldValue(
					        'custevent_allocation_status', '18');
					nlapiSubmitRecord(allocationRecord, false, true);
					
					//var employeeType = nlapiLookupField('employee', allocationResource , 'custentity_persontype');
					// No need to check for employee type	
					
					//if(employeeType == '1')
					if(empsubsidiary == '2')
					{
						var oldEndDate_res = allocationRecord.getFieldValue('enddate');
						var oldStartDate_res = allocationRecord.getFieldValue('startdate');
						//var isSubRefchecked = request.getLineItemValue('custpage_allocation_list', 'subrefvencheck', linenum);
						var isvendor = request.getLineItemValue('custpage_allocation_list', 'refvendorname', linenum);
						var isrefendDate = request.getLineItemValue('custpage_allocation_list', 'newrefenddate', linenum);
						
						var filters = new Array();
						filters[0] = new nlobjSearchFilter('custrecord_stvd_contractor',null,'anyof',parseInt(allocationResource));
						filters[1] = new nlobjSearchFilter('isinactive',null,'is','F');
						
						var columns = new Array();
						columns[0] = new nlobjSearchColumn('custrecord_stvd_start_date');
						columns[1] = new nlobjSearchColumn('custrecord_stvd_end_date').setSort(true);
						columns[2] = new nlobjSearchColumn('custrecord_stvd_vendor_type');
						
						var search_results	=	searchRecord('customrecord_subtier_vendor_data', null, filters, columns);
						
						nlapiLogExecution('DEBUG', 'refvendor', isrefendDate);
						if(isArrayNotEmpty(search_results))
						{
							//flag it if it is updated for first vendor which is the latest vendor
							var flag= true;
							var flag2= true;
							nlapiLogExecution('Debug', 'flag',flag);
							
							for(var k= 0 ; k<search_results.length; k++)
							{
								//
								if(isrefendDate =='' || isrefendDate == null )
								{
									nlapiLogExecution('Debug', 'vendor type1  dd', search_results[k].getValue('custrecord_stvd_vendor_type')+" "+isrefendDate);
									if(flag==true)
										{
									if(search_results[k].getValue('custrecord_stvd_vendor_type') == '1')
										{ 
											if(nlapiStringToDate(newEndDate) > nlapiStringToDate(search_results[k].getValue('custrecord_stvd_end_date')))
											{
												if(new_project_end_date)
												{
													if(nlapiStringToDate(new_project_end_date)>= nlapiStringToDate(newEndDate))
													{
														var a = nlapiSubmitField('customrecord_subtier_vendor_data', search_results[k].getId(), 'custrecord_stvd_end_date', newEndDate);
														flag =false;
													}
												}
												else
												{
													if(nlapiStringToDate(old_project_end_date)>= nlapiStringToDate(newEndDate))
													{
														var a = nlapiSubmitField('customrecord_subtier_vendor_data', search_results[k].getId(), 'custrecord_stvd_end_date', newEndDate);
														flag= false;
													}
												}
												
											}
											else
											{
												if(new_project_end_date)
												{
													if(nlapiStringToDate(new_project_end_date)>= nlapiStringToDate(newEndDate))
													{
														var a = nlapiSubmitField('customrecord_subtier_vendor_data', search_results[k].getId(), 'custrecord_stvd_end_date', newEndDate);
														flag =false;
													}
												}
												else
												{
													if(nlapiStringToDate(old_project_end_date)>= nlapiStringToDate(newEndDate))
													{
														var a = nlapiSubmitField('customrecord_subtier_vendor_data', search_results[k].getId(), 'custrecord_stvd_end_date', newEndDate);
														flag= false;
													}
												}
												
											}
										}
										}
								}
								else
								{	
									if(isvendor != '' || isvendor != null){
										
										if(search_results[k].getValue('custrecord_stvd_vendor_type') == '2')
											{
											if(flag2==true)
												{
												if(nlapiStringToDate(newrefenddate) > nlapiStringToDate(search_results[k].getValue('custrecord_stvd_end_date')))
												{
													if(new_project_end_date)
													{
														if(nlapiStringToDate(new_project_end_date)>= nlapiStringToDate(newrefenddate))
															{
															var a = nlapiSubmitField('customrecord_subtier_vendor_data', search_results[k].getId(), 'custrecord_stvd_end_date', newrefenddate);
															flag2 = false;
															}
														else{
															var e =nlapiCreateError('404', 'Referral End Date should be Less or equal to Project end Date');
															throw e;
														}
													}
													else
														{
														if(nlapiStringToDate(old_project_end_date)>= nlapiStringToDate(newrefenddate))
														{
															var a = nlapiSubmitField('customrecord_subtier_vendor_data', search_results[k].getId(), 'custrecord_stvd_end_date', newrefenddate);
															flag2 = false;
														}
														else{
															var e =nlapiCreateError('404', 'Referral End Date should be Less or equal to Project end Date');
															throw e;
														}
														}
												}
											}
											}
										/*else{
											if(nlapiStringToDate(newrefenddate) > nlapiStringToDate(search_results[k].getValue('custrecord_stvd_end_date')))
											{
												if(new_project_end_date)
												{
													if(nlapiStringToDate(new_project_end_date)>= nlapiStringToDate(newrefenddate))
														{
														var a = nlapiSubmitField('customrecord_subtier_vendor_data', search_results[k].getId(), 'custrecord_stvd_end_date', newrefenddate);
														}
												}
												else
													{
													if(nlapiStringToDate(old_project_end_date)>= nlapiStringToDate(newEndDate))
													var a = nlapiSubmitField('customrecord_subtier_vendor_data', search_results[k].getId(), 'custrecord_stvd_end_date', newrefenddate);
													}
											}
										}*/
										else
											{
											nlapiLogExecution('Debug', 'vendor type', search_results[k].getText('custrecord_stvd_vendor_type'));
											if(flag==true)
											{
											 if(search_results[k].getValue('custrecord_stvd_vendor_type') == '1')
											 { 
												if(nlapiStringToDate(newEndDate) > nlapiStringToDate(search_results[k].getValue('custrecord_stvd_end_date')))
												{
													if(new_project_end_date)
													{
														if(nlapiStringToDate(new_project_end_date)>= nlapiStringToDate(newEndDate))
														{
															var a = nlapiSubmitField('customrecord_subtier_vendor_data', search_results[k].getId(), 'custrecord_stvd_end_date', newEndDate);
															nlapiLogExecution('Debug', 'Submitted first', flag);
															flag =false;
														}
													}
													else
													{
														if(nlapiStringToDate(old_project_end_date)>= nlapiStringToDate(newEndDate))
														{
															var a = nlapiSubmitField('customrecord_subtier_vendor_data', search_results[k].getId(), 'custrecord_stvd_end_date', newEndDate);
															nlapiLogExecution('Debug', 'Submitted in else', flag);
															flag= false;
														}
													}
												  }
											 	}
											  }
											}
									}
								}
							}
						}
					}
					
				//	nlapiLogExecution('debug', 'allocation updated',allocationId);
					nlapiLogExecution('debug', 'Resource updated',allocationResource);
					// generating content for email
					tableData += "<tr>";
					tableData += "<td>"
					        + allocationRecord
					                .getFieldText('allocationresource')
					        + "</td>";
					tableData += "<td>"
					        + request.getLineItemValue(
					                'custpage_allocation_list', 'enddate',
					                linenum) + "</td>";
					tableData += "<td>"
					        + request.getLineItemValue(
					                'custpage_allocation_list', 'newenddate',
					                linenum) + "</td>";
					tableData += "<td>"
				        + request.getLineItemValue(
				                'custpage_allocation_list', 'refenddate',
				                linenum) + "</td>";
					tableData += "<td>"
				        + request.getLineItemValue(
				                'custpage_allocation_list', 'newrefenddate',
				                linenum) + "</td>";
					tableData += "</tr>";
				} catch (e) {
					
					nlapiLogExecution('error',
					        'Failed while updating the record', e);
					tableData += "<tr>";
					tableData += "<td>"
					        + allocationRecord
					                .getFieldText('allocationresource')
					        + "</td>";
					tableData += "<td>"
					        + request.getLineItemValue(
					                'custpage_allocation_list', 'enddate',
					                linenum) + "</td>";
					if(e.name=='404')
						{
						tableData += "<td>"
					        + request.getLineItemValue(
					                'custpage_allocation_list', 'newenddate',
					                linenum) + "</td>";
							
						}
					else
							{
					
						tableData += "<td>" + e + "</td>";
							
							}
					tableData += "<td>"
				        + request.getLineItemValue(
				                'custpage_allocation_list', 'refenddate',
				                linenum) + "</td>";
					if(e.name!='404')
						{
					tableData += "<td>"
				        + request.getLineItemValue(
				                'custpage_allocation_list', 'refenddate',
				                linenum) + "</td>";
						}
					else
					{
						tableData += "<td>" + e + "</td>";
					}
					tableData += "</tr>";
				}
			}
		}
		
		return tableData;
}