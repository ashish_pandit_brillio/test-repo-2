// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
/*
   	Script Name:    SUT TDS Payment Section wise 
	Author:         Nikhil jain
	Company:		Aashna Cloudtech Pvt. Ltd.
	Date:			
	Version:		
	Description:	Vendor Tax Liability Form section wise 


	Script Modification Log:

	-- Date --			-- Modified By --				--Requested By--				-- Description --
     



Below is a summary of the process controls enforced by this script file.  The control logic is described
more fully, below, in the appropriate function headers and code blocks.


     SUITELET
		- suiteletFunction(request, response)
        createCriteriaForm(request, response)

     SUB-FUNCTIONS
		- The following sub-functions are called by the above core functions in order to maintain code
            modularization:

               - NOT USED

*/
}
// END SCRIPT DESCRIPTION BLOCK  ====================================



// BEGIN GLOBAL VARIABLE BLOCK  =====================================
{
	//  Initialize any Global Variables, in particular, debugging variables...




}
// END GLOBAL VARIABLE BLOCK  =======================================





// BEGIN SUITELET ==================================================

function suiteletFunction(request, response)
{

	/*  Suitelet:

          - EXPLAIN THE PURPOSE OF THIS FUNCTION

		-


		FIELDS USED:

          --Field Name--				--ID--


	*/



	//  LOCAL VARIABLES



    //  SUITELET CODE BODY






}

// END SUITELET ====================================================

function createCriteriaForm(request, response)
{
	nlapiLogExecution('DEBUG','Request Method', " " + request.getMethod());
	if(request.getMethod() == 'GET')
	{
		//===== FOR SINGLE SUBSIDARY ACCOUNT  =======
		
		
		// === CREATE FORM ====
		var form = nlapiCreateForm("TDS Payment Criteria Form");
	    var sysdate=new Date
		var date1=nlapiDateToString(sysdate)

	    // == ADD THE FROM DATE FIELD===
		var fromdate = form.addField('fromdate', 'date', ' From Date');
		fromdate.setMandatory(true);
		fromdate.setDefaultValue(date1)

		 // == ADD THE TODATE DATE FIELD===
		var todate = form.addField('todate', 'date', ' To Date');
		todate.setMandatory(true);
		todate.setDefaultValue(date1)
        
		var context = nlapiGetContext();
		var i_subcontext = context.getFeature('SUBSIDIARIES');
		
		if (i_subcontext != false) 
		{
			var i_AitGlobalRecId =  SearchGlobalParameter();
			nlapiLogExecution('DEBUG','Bill ', "i_AitGlobalRecId"+i_AitGlobalRecId);
	
			if(i_AitGlobalRecId != 0 )
			{
				var o_AitGloRec = nlapiLoadRecord('customrecord_indian_tax_glo_parameter',i_AitGlobalRecId);
				
				var a_subisidiary = new Array();
				
				a_subisidiary = o_AitGloRec.getFieldValues('custrecord_ait_subsidiary');
				nlapiLogExecution('DEBUG','Bill ', "a_subisidiary->"+a_subisidiary);
		
			}
			var subsidiary = form.addField('subsidiary', 'select', 'Subsidiary');
			subsidiary.addSelectOption('', '');
        	popluatsubsidiaries(request, response, subsidiary,a_subisidiary);
			subsidiary.setMandatory(true)
		}
		
	    // == ADD  THE SUBSIDIARY FIELD ===
		var tds_section = form.addField('tds_section', 'select', 'TDS Section','customlist_tds_section');
		tds_section.setMandatory(true);
		
		var assessee_type = form.addField('assessee_type', 'select', 'Assessee Type','customlist_assessee_code');
		assessee_type.setMandatory(true);

		// === ADD SUBMIT BUTTON ===
		form.addSubmitButton('Search');
		response.writePage(form);
		
	}//END if(request.getMethod() == 'GET')
	else if(request.getMethod() == 'POST')
	{

		//===== FOR SINGLE SUBSIDARY ACCOUNT  =======
		var context = nlapiGetContext();
	    var i_subcontext = context.getFeature('SUBSIDIARIES');
		if(i_subcontext == false)
		{
			var subsidiary = null;
		}
		else
		{
			var subsidiary = request.getParameter('subsidiary');
		}
		
		var params = new Array();
		params['fromdate'] = request.getParameter('fromdate');
		params['todate']=request.getParameter('todate')
		params['subsidiary'] = subsidiary;
		params['tds_section']=request.getParameter('tds_section')
		params['assessee_type']=request.getParameter('assessee_type')
		nlapiSetRedirectURL('SUITELET','customscript_tds_paylist_sectionwise', '1', false, params);


	}//END else if(request.getMethod() == 'POST')
}
function SearchGlobalParameter()
{
	var a_filters = new Array();
	var a_column = new Array();
	var i_globalRecId = 0;
	
    a_column.push(new nlobjSearchColumn('internalid'));
	
	var s_serchResult = nlapiSearchRecord('customrecord_indian_tax_glo_parameter',null,null,a_column)
	
	if(s_serchResult != null && s_serchResult != undefined && s_serchResult != '')
	{
		for (var i=0; i<s_serchResult.length && s_serchResult != null; i++) 	
		{
			i_globalRecId = s_serchResult[0].getValue('internalid');
			nlapiLogExecution('DEBUG','Bill ', "i_globalRecId"+i_globalRecId);
		
		}
		
	}
	return i_globalRecId;
}
function popluatsubsidiaries(request, response, taxagency,subsidiary_id)
	{
	   // var subsidiary = request.getParameter('subsidiary')
	    var ved_filchk1 = new Array();
	    var ved_columnsCheck1 = new Array();

	   ved_filchk1.push( new nlobjSearchFilter('internalid', null, 'anyof', subsidiary_id));

	    ved_columnsCheck1.push( new nlobjSearchColumn('internalid'));
	    ved_columnsCheck1.push( new nlobjSearchColumn('name'));

	    var ved_searchResultsCheck1 = nlapiSearchRecord('subsidiary', null, ved_filchk1, ved_columnsCheck1);
	    if (ved_searchResultsCheck1 != null)
		{
	        for (i = 0; ved_searchResultsCheck1 != null && i < ved_searchResultsCheck1.length; i++)
			{
	            taxagency.addSelectOption(ved_searchResultsCheck1[i].getValue('internalid'),ved_searchResultsCheck1[i].getValue('name'), false);

	        }
	    }

	}
