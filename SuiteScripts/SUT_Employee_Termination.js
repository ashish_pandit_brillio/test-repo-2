/**
 *
 * Script Name: SUT | Manual EMP Termination Update RA
 * Author:Praveena Madem
 * Company:INSPIRRIA CLOUDTECH
 * Date: 14 - Mar - 2020
 * Description:1.Fetch resource allocations data and update the Billing ENd date
				
			
 			   
 * Script Modification Log:
 *  -- Date --			-- Modified By --				--Requested By--				-- Description --
*/


function _callSuitelet_Updatethe_Data(request,response)
{
try{
var body=request.getBody();
if(body)
body=JSON.parse(body);
nlapiLogExecution('DEBUG','body',body)
var i_employee_id=body.i_employee_id;
var newTerminationDate=body.newTerminationDate;
var resourceAllocationSearch = nlapiSearchRecord(
				        'resourceallocation', null, [
				                new nlobjSearchFilter('internalid', 'employee','anyof', i_employee_id),
				                new nlobjSearchFilter('enddate', null,'onorafter', newTerminationDate) ]);

				if (isArrayNotEmpty(resourceAllocationSearch)) {
nlapiLogExecution('debug','i : isArrayNotEmpty(resourceAllocationSearch)',isArrayNotEmpty(resourceAllocationSearch));
					for (var index = 0; index < resourceAllocationSearch.length; index++) {
						var rescAllocRec = nlapiLoadRecord(
						        'resourceallocation',
						        resourceAllocationSearch[index].getId(), {
							        recordmode : 'dynamic'
						        });
						rescAllocRec.setFieldValue('enddate',
						        newTerminationDate);
						rescAllocRec.setFieldValue('custeventbenddate',
						        newTerminationDate);
						rescAllocRec.setFieldValue(
						        'custevent_allocation_status', '22');
						rescAllocRec
						        .setFieldValue('notes',
						                'Employee terminated, hence allocation end date changed');
						if(nlapiGetUser()==-4)
						{
							rescAllocRec.setFieldValue('custevent_ra_last_modified_by',"442");
					   }
                      else
                        {
                          rescAllocRec.setFieldValue('custevent_ra_last_modified_by',nlapiGetUser());
                        }
						rescAllocRec.setFieldValue(
						        'custevent_ra_last_modified_date',
						        nlapiDateToString(new Date(), 'datetimetz'));
						var i_ra_rec_id=nlapiSubmitRecord(rescAllocRec, false, true);
						nlapiLogExecution('debug','index : i_ra_rec_id',index+" : "+i_ra_rec_id);
					}
				}

				// Get all subtier records that end after the LWD
				var subtierSearch = nlapiSearchRecord(
				        'customrecord_subtier_vendor_data', null, [
				                new nlobjSearchFilter(
				                        'custrecord_stvd_contractor', null,
				                        'anyof', i_employee_id),
				                new nlobjSearchFilter(
				                        'custrecord_stvd_end_date', null,
				                        'onorafter', newTerminationDate) ]);

				// change the end date of all the found subtier record
				if (subtierSearch) {

					for (var index = 0; index < subtierSearch.length; index++) {
						var subTierRec = nlapiLoadRecord(
						        'customrecord_subtier_vendor_data',
						        subtierSearch[index].getId(), {
							        recordmode : 'dynamic'
						        });

						// if start date is after the termination date, change
						// the start date
						if (nlapiStringToDate(subTierRec
						        .getFieldValue('custrecord_stvd_start_date')) > nlapiStringToDate(newTerminationDate)) {
							subTierRec.setFieldValue('isinactive', 'T');
							subTierRec.setFieldValue(
							        'custrecord_stvd_start_date',
							        newTerminationDate);
						}

						subTierRec.setFieldValue('custrecord_stvd_end_date',
						        newTerminationDate);

						nlapiSubmitRecord(subTierRec, false, true);
					}
				}
				var result={"success":'T'};
  nlapiLogExecution('DEBUG','success',"employee :"+i_employee_id);
				response.write(JSON.stringify(result));
				}
				catch (e) {
   
        var obj = {
            "Code": "",
            "Details": ""
        }
        obj.Code = e.getCode();
        obj.Details = e.getDetails();
      	//obj.id=id;
        response.write(JSON.stringify(obj));
    
    }
}