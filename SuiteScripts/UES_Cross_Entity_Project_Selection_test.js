//https://system.na1.netsuite.com/app/common/scripting/script.nl?id=278
/**
 * @author Shweta
 */
// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
/*
   	Script Name : UES_Cross_Entity_Project_Selection.js
	Author      : Shweta Chopde
	Date        : 1 Sep 2014
	Description : 


	Script Modification Log:

	-- Date --			-- Modified By --				--Requested By--				-- Description --



Below is a summary of the process controls enforced by this script file.  The control logic is described
more fully, below, in the appropriate function headers and code blocks.


     BEFORE LOAD
		- beforeLoadRecord(type)



     BEFORE SUBMIT
		- beforeSubmitRecord(type)


     AFTER SUBMIT
		- afterSubmitRecord(type)



     SUB-FUNCTIONS
		- The following sub-functions are called by the above core functions in order to maintain code
            modularization:

               - NOT USED

*/
}
// END SCRIPT DESCRIPTION BLOCK  ====================================



// BEGIN GLOBAL VARIABLE BLOCK  =====================================
{
	//  Initialize any Global Variables, in particular, debugging variables...




}
// END GLOBAL VARIABLE BLOCK  =======================================





// BEGIN BEFORE LOAD ==================================================

function beforeLoadRecord(type)
{

	/*  On before load:

          - EXPLAIN THE PURPOSE OF THIS FUNCTION

		-


		FIELDS USED:

          --Field Name--				--ID--


	*/



	//  LOCAL VARIABLES

    //  BEFORE LOAD CODE BODY


	return true;



}

// END BEFORE LOAD ====================================================





// BEGIN BEFORE SUBMIT ================================================

function beforeSubmitRecord(type)
{
    /*  On before submit:

          - PURPOSE
		-

          FIELDS USED:

          --Field Name--				--ID--


    */


    //  LOCAL VARIABLES


    //  BEFORE SUBMIT CODE BODY


	return true;

}

// END BEFORE SUBMIT ==================================================





// BEGIN AFTER SUBMIT =============================================

function afterSubmit_cross_project(type)
{
  if(type == 'create' || type == 'edit')
  {
  	
   try
   {   		
	var i_customer = '';
	var s_project_description = '';
   	var i_vertical = '';
	var i_recordID = nlapiGetRecordId();	
	var s_record_type = nlapiGetRecordType();
	var i_territory	=	'';
	var i_category = '';
	var i_project_services = '';
	var proj_category_val = '';
	var parent_practice ='';
	var project_region ='';
	 /*----------Added by Koushalya 28/12/2021---------*/
	 var filters_customrecord_revenue_location_subsidiarySearch=["custrecord_offsite_onsite","anyof","2"];
	 var obj_rev_loc_subSrch= Search_revenue_Location_subsdidary(filters_customrecord_revenue_location_subsidiarySearch);
	 var obj_offsite_onsite=obj_rev_loc_subSrch["Onsite/Offsite"];
	  /*-------------------------------------------------*/

	nlapiLogExecution('DEBUG', 'afterSubmit_cross_project', ' Record ID -->' + i_recordID);
	nlapiLogExecution('DEBUG', 'afterSubmit_cross_project', ' Record Type -->' + s_record_type);
				
	if(s_record_type == 'salesorder' || s_record_type == 'invoice')
	{
		if(_logValidation(i_recordID)&&_logValidation(s_record_type))
		{
			var o_recordOBJ = nlapiLoadRecord(s_record_type,i_recordID);
			
			if(_logValidation(o_recordOBJ))
			{					
			//	if(_logValidation(i_projectID))
				{				
					
					if((s_record_type == 'salesorder')||(s_record_type == 'invoice'))
					{
						var i_projectID = o_recordOBJ.getFieldValue('job');
				        nlapiLogExecution('DEBUG', 'afterSubmit_cross_project', ' Project ID -->' + i_projectID);
						
					}//Sales Order / Invoice
					
					
					var o_projectOBJ = nlapiLoadRecord('job',i_projectID);
					
					if(_logValidation(o_projectOBJ))
					{
						var i_project_name_ID =  o_projectOBJ.getFieldValue('entityid');
						nlapiLogExecution('DEBUG', 'afterSubmit_cross_project', ' Project Name ID -->' + i_project_name_ID);
							
						var i_project_name =  o_projectOBJ.getFieldValue('altname');
						nlapiLogExecution('DEBUG', 'afterSubmit_cross_project', ' Project Name -->' + i_project_name);
						
						s_project_description = i_project_name_ID+' '+i_project_name;
						nlapiLogExecution('DEBUG', 'afterSubmit_cross_project', ' Project Description -->' + s_project_description);
						
						i_customer =  o_projectOBJ.getFieldText('parent');
					    nlapiLogExecution('DEBUG', 'afterSubmit_cross_project', ' Customer-->' + i_customer);
						
						var practice_id = o_projectOBJ.getFieldValue('custentity_practice');
						parent_practice = nlapiLookupField('department',practice_id,'custrecord_parent_practice',true);
						project_region = o_projectOBJ.getFieldText('custentity_region');
						i_category =  o_projectOBJ.getFieldText('custentity_project_allocation_category');
					    nlapiLogExecution('DEBUG', 'afterSubmit_cross_project', ' Pro Category-->' + i_category);
						
						proj_category_val = o_projectOBJ.getFieldValue('custentity_project_allocation_category');
					    i_vertical =  o_projectOBJ.getFieldValue('custentity_vertical');
					    nlapiLogExecution('DEBUG', 'afterSubmit_cross_project', ' Vertical-->' + i_vertical);
						
						i_project_services =  o_projectOBJ.getFieldValue('custentity_project_services'); //Project Services
					    
					    var i_customer_id	=	o_projectOBJ.getFieldValue('parent');
						var customer_Obj = nlapiLoadRecord('customer',i_customer_id);
					    if(_logValidation(i_customer_id))
						{
					    	i_territory	= customer_Obj.getFieldValue('territory');
							var cust_ID_V	= customer_Obj.getFieldValue('entityid');
							var region_id = customer_Obj.getFieldValue('custentity_region');
							var cust_name = customer_Obj.getFieldValue('companyname');
						}
						
						var billing_type = o_projectOBJ.getFieldText('jobbillingtype');
						if(!_logValidation(billing_type))
						{
					    	billing_type	=	'';
						}
											
					}//Project OBJ				
				}//Project ID		
			
				    
					if((s_record_type == 'salesorder')||(s_record_type == 'invoice'))
					{
						var s_line = 'item';
					}//Sales Order / Invoice
				
					
				var i_line_count = o_recordOBJ.getLineItemCount(s_line)
				nlapiLogExecution('DEBUG', 'afterSubmit_cross_project', ' i_line_count-->' + i_line_count);
				if(_logValidation(i_line_count))
				{
					for(var i=1;i<=i_line_count;i++)
					{
						if(!_logValidation(i_customer))
						{
							i_customer = '';
						}
						if(!_logValidation(s_project_description))
						{
							s_project_description = '';
						}
						
						var emp_type = '';
						var emp_frst_name = '';
						var emp_full_name = '';
						var emp_middl_name = '';
						var emp_lst_name = '';
						var person_type = '';
						var onsite_offsite = '';
						var s_employee_name_split = '';
						var core_practice = '';
						
						var misc_practice = '';
						var emp_practice = '';
						var emp_practice_text = '';
						var s_employee_name = o_recordOBJ.getLineItemValue(s_line,'custcol_employeenamecolumn',i);
						nlapiLogExecution('DEBUG', 'afterSubmit_cross_project', ' s_employee_name-->' + s_employee_name);
						if(!_logValidation(s_employee_name))
						{
							s_employee_name = '';
						}
						else
						{
							var s_employee_name_id = s_employee_name.split('-');
							var s_employee_name_split = s_employee_name_id[0];
							nlapiLogExecution('audit','s_employee_name split:-- '+s_employee_name_split,s_employee_name);
							var filters_emp = new Array();
							filters_emp[0] = new nlobjSearchFilter('entityid',null,'contains',s_employee_name_split);
							var column_emp = new Array();	
		    				column_emp[0] = new nlobjSearchColumn('custentity_persontype');
							column_emp[1] = new nlobjSearchColumn('employeetype');
							column_emp[2] = new nlobjSearchColumn('subsidiary');
							column_emp[3] = new nlobjSearchColumn('custentity_fusion_empid');
							column_emp[4] = new nlobjSearchColumn('firstname');
							column_emp[5] = new nlobjSearchColumn('middlename');
							column_emp[6] = new nlobjSearchColumn('lastname');
							column_emp[7] = new nlobjSearchColumn('department');
							column_emp[8] = new nlobjSearchColumn('location');
							column_emp[9] = new nlobjSearchColumn('custentity_legal_entity_fusion');
							var a_results_emp = nlapiSearchRecord('employee', null, filters_emp, column_emp);
							nlapiLogExecution('audit','a_results_emp:-- ',a_results_emp);
							if (_logValidation(a_results_emp))
							{
								var emp_id = a_results_emp[0].getId();
								var emp_type = a_results_emp[0].getText('employeetype');
								var emp_fusion_id = a_results_emp[0].getValue('custentity_fusion_empid');
								emp_frst_name = a_results_emp[0].getValue('firstname');
								emp_middl_name = a_results_emp[0].getValue('middlename');
								emp_lst_name = a_results_emp[0].getValue('lastname');
								//nlapiLogExecution('audit','emp_type:-- ',emp_type);
								if(!_logValidation(emp_type))
								{
									emp_type = '';
								}
								if(emp_frst_name)
										emp_full_name = emp_frst_name;
											
								if(emp_middl_name)
										emp_full_name = emp_full_name + ' ' + emp_middl_name;
											
								if(emp_lst_name)
										emp_full_name = emp_full_name + ' ' + emp_lst_name;
										
								var person_type = a_results_emp[0].getText('custentity_persontype');
								//nlapiLogExecution('audit','person_type:-- ',person_type);
								if(!_logValidation(person_type))
								{
									person_type = '';
								}
								var emp_subsidiary = a_results_emp[0].getValue('subsidiary');
								if (_logValidation(emp_subsidiary))
								{
								/*	if(emp_subsidiary == 3 || emp_subsidiary == 12)
									{
										onsite_offsite = 'Offsite';
									}*/
									if (emp_subsidiary == 3 && a_results_emp[0].getValue('custentity_legal_entity_fusion', 'employee') == 'Brillio Technologies Private Limited UK') {
										onsite_offsite_dump = 'Onsite';
									}
									else{
										onsite_offsite = obj_offsite_onsite[emp_subsidiary]?'Offshore':'Onsite';    
									}
									/*else if(emp_subsidiary == 3 && a_results_emp[0].getValue('custentity_legal_entity_fusion') == 'Brillio Technologies Private Limited UK')
									{
										onsite_offsite = 'Onsite';
									}
									else
									{
										onsite_offsite = 'Onsite';
									}	*/
								}
								emp_practice = a_results_emp[0].getFieldValue('department');
								emp_practice_text = a_results_emp[0].getText('department');
								if(emp_practice)
								{
									var is_practice_active_e = nlapiLookupField('department',parseInt(emp_practice),['isinactive','custrecord_is_delivery_practice']);
									var isinactive_Practice_e = is_practice_active_e.isinactive;
									nlapiLogExecution('debug','isinactive_Practice_e',isinactive_Practice_e);
									core_practice=is_practice_active_e.custrecord_is_delivery_practice;
									nlapiLogExecution('debug','core_practice',core_practice);
								}
							}
						
						}
						if(s_record_type == 'invoice')
						{
							if(proj_category_val)
							{
								if((parseInt(proj_category_val)==parseInt(1)) && (core_practice =='T'))
								{
									misc_practice=emp_practice;
									var is_practice_active = nlapiLookupField('department',parseInt(misc_practice),['isinactive']);
									isinactive_Practice_e = is_practice_active.isinactive;
									misc_practice = emp_practice_text;
								}
								else 
								{
									misc_practice=o_projectOBJ.getFieldValue('custentity_practice');
									var is_practice_active = nlapiLookupField('department',parseInt(misc_practice),['isinactive']);
									isinactive_Practice_e = is_practice_active.isinactive;
									misc_practice=o_projectOBJ.getFieldText('custentity_practice');
								}
							}
							nlapiLogExecution('audit','Pratice','misc_practice:'+misc_practice);
							if(misc_practice && isinactive_Practice_e == 'F'){
							o_recordOBJ.setLineItemValue(s_line,'custcol_mis_practice',i, misc_practice);	
							}
						}						
						o_recordOBJ.setLineItemValue(s_line,'custcolcustcol_temp_customer',i,i_customer);
						o_recordOBJ.setLineItemValue(s_line,'custcolprj_name',i,s_project_description);
						o_recordOBJ.setLineItemValue(s_line,'custcol_territory', i, i_territory);
						o_recordOBJ.setLineItemValue(s_line,'custcol_onsite_offsite', i, onsite_offsite);
						o_recordOBJ.setLineItemValue(s_line,'custcol_billing_type', i, billing_type);
						o_recordOBJ.setLineItemValue(s_line,'custcol_employee_type', i, emp_type);
						o_recordOBJ.setLineItemValue(s_line,'custcol_person_type', i, person_type);
						o_recordOBJ.setLineItemValue(s_line,'custcol_project_entity_id', i, i_project_name_ID);
						o_recordOBJ.setLineItemValue(s_line,'custcol_transaction_project_services', i, i_project_services);
						o_recordOBJ.setLineItemValue(s_line,'custcol_parent_executing_practice', i, parent_practice);
						o_recordOBJ.setLineItemValue(s_line,'custcol_project_region', i, project_region);
						var cust_entity_id = i_customer.split(' ');
						cust_entity_id = cust_entity_id[0];
						o_recordOBJ.setLineItemValue(s_line,'custcol_customer_entityid', i, cust_entity_id);
						o_recordOBJ.setLineItemValue(s_line,'custcol_employee_entity_id', i, s_employee_name_split);
						o_recordOBJ.setLineItemValue(s_line,'custcol_region_master_setup', i, region_id);
						o_recordOBJ.setLineItemValue(s_line,'custcol_cust_name_on_a_click_report',i,cust_name);
						o_recordOBJ.setLineItemValue(s_line,'custcol_proj_name_on_a_click_report', i, i_project_name);
						o_recordOBJ.setLineItemValue(s_line,'custcol_proj_category_on_a_click', i, i_category); 
						o_recordOBJ.setLineItemValue(s_line,'custcol_emp_name_on_a_click_report', i, emp_full_name);
						
						
						
																		
					}//Loop				
				}//Line Count
				
				var i_submitID = nlapiSubmitRecord(o_recordOBJ,true,true);
				nlapiLogExecution('DEBUG', 'afterSubmit_cross_project', ' ***************** Submit ID ****************-->' + i_submitID);
						
							
			}//Record OBJ		
		}//Record ID & Record Type 
	
	}//Record Type - Sales Order / Invoice 		
		
	if(s_record_type == 'expensereport')
	{
		var s_line ;
		var i_practice;
		var i_location;
		var s_employee_name;
		var emp_full_name = '';
		var i_category = '';
		var i_project_services = '';
		var core_practice = '';
		var emp_type = '';
		var person_type = '';
		var onsite_offsite = '';
		var region_id = '';
		var cust_name = '';
		var proj_category_val = '';
		var misc_practice = '';
		var emp_practice = '';
		var emp_practice_text = '';
		var parent_practice='';
		var project_region = '';
		if(_logValidation(i_recordID)&&_logValidation(s_record_type))
		{
			var o_recordOBJ = nlapiLoadRecord(s_record_type,i_recordID);
			
			if(_logValidation(o_recordOBJ))
			{			
			    if(s_record_type == 'expensereport')
				{
					s_line = 'expense';
					
				}//Expense Report
					
				var i_employee = o_recordOBJ.getFieldValue('entity'); 
				
				if(_logValidation(i_employee))
				{
				   var o_employeeOBJ = nlapiLoadRecord('employee',i_employee);
				
					if (_logValidation(o_employeeOBJ)) 
					{
						i_practice = o_employeeOBJ.getFieldValue('department');
						nlapiLogExecution('DEBUG', 'afterSubmit_cross_project', ' Practice-->' + i_practice);
						
						i_location = o_employeeOBJ.getFieldValue('location');
						nlapiLogExecution('DEBUG', 'afterSubmit_cross_project', ' Location-->' + i_location);
						
						
						
						s_employee_name = o_employeeOBJ.getFieldValue('entityid');
						nlapiLogExecution('DEBUG', 'afterSubmit_cross_project', ' Employee Name-->' + s_employee_name);
					
						emp_type = o_employeeOBJ.getFieldText('employeetype');
						
						person_type = o_employeeOBJ.getFieldText('custentity_persontype');
						
						var emp_subsidiary = o_employeeOBJ.getFieldValue('subsidiary');
						var emp_frst_name = o_employeeOBJ.getFieldValue('firstname');
						var emp_middl_name = o_employeeOBJ.getFieldValue('middlename');
						var emp_lst_name = o_employeeOBJ.getFieldValue('lastname');
						
						if(emp_frst_name)
								emp_full_name = emp_frst_name;
											
						if(emp_middl_name)
								emp_full_name = emp_full_name + ' ' + emp_middl_name;
											
						if(emp_lst_name)
								emp_full_name = emp_full_name + ' ' + emp_lst_name;
						//
						emp_practice = o_employeeOBJ.getFieldValue('department');
						emp_practice_text = o_employeeOBJ.getFieldText('department');
						if(emp_practice)
						{
							var is_practice_active_e = nlapiLookupField('department',parseInt(emp_practice),['isinactive','custrecord_is_delivery_practice']);
							var isinactive_Practice_e = is_practice_active_e.isinactive;
							nlapiLogExecution('debug','isinactive_Practice_e',isinactive_Practice_e);
							core_practice=is_practice_active_e.custrecord_is_delivery_practice;
							nlapiLogExecution('debug','core_practice',core_practice);
						}
						//
						if (_logValidation(emp_subsidiary))
						{
							/*if(emp_subsidiary == 3 || emp_subsidiary == 12)
							{
								onsite_offsite = 'Offsite';
							}*/
							if (emp_subsidiary == 3 && o_employeeOBJ.getFieldValue('custentity_legal_entity_fusion', 'employee') == 'Brillio Technologies Private Limited UK') {
								onsite_offsite_dump = 'Onsite';
							}
							else{
								onsite_offsite = obj_offsite_onsite[emp_subsidiary]?'Offshore':'Onsite';    
							}
							/*else if(emp_subsidiary == 3 && o_employeeOBJ.getFieldValue('custentity_legal_entity_fusion') == 'Brillio Technologies Private Limited UK')
									{
										onsite_offsite = 'Onsite';
									}
							else
							{
								onsite_offsite = 'Onsite';
							}	*/
						}
					}				
					
				}//Employee	
					
				var i_line_count = o_recordOBJ.getLineItemCount(s_line)
				
				if(i_line_count >= 30) // to avoid script execution limit exceed issue update expense through schduled script
				{
					nlapiSubmitField('expensereport',i_recordID,'custbody_update_expense_using_sch','T');
				}
				
				if(_logValidation(i_line_count))
				{
					for(var i=1;i<=i_line_count;i++)
					{				
					var i_projectID = o_recordOBJ.getLineItemValue(s_line,'customer',i);
				    nlapiLogExecution('DEBUG', 'afterSubmit_cross_project', ' Project ID -->' + i_projectID);
										
					if(_logValidation(i_projectID))
					{							
					    var o_projectOBJ = nlapiLoadRecord('job',i_projectID);
					
						if(_logValidation(o_projectOBJ))
						{
							var i_project_name_ID =  o_projectOBJ.getFieldValue('entityid');
							nlapiLogExecution('DEBUG', 'afterSubmit_cross_project', ' Project Name ID -->' + i_project_name_ID);
								
							var i_project_name =  o_projectOBJ.getFieldValue('companyname');
							nlapiLogExecution('DEBUG', 'afterSubmit_cross_project', ' Project Name -->' + i_project_name);
							
							s_project_description = i_project_name_ID+' '+i_project_name;
							nlapiLogExecution('DEBUG', 'afterSubmit_cross_project', ' Project Description -->' + s_project_description);
							
							i_category =  o_projectOBJ.getFieldText('custentity_project_allocation_category');
							nlapiLogExecution('DEBUG', 'afterSubmit_cross_project', ' Pro Category-->' + i_category);
							
							i_customer =  o_projectOBJ.getFieldText('parent');
						    nlapiLogExecution('DEBUG', 'afterSubmit_cross_project', ' Customer-->' + i_customer);	
							//Added for Project Region and parent executing practice
							
							var practice_id = o_projectOBJ.getFieldValue('custentity_practice');
							parent_practice = nlapiLookupField('department',practice_id,'custrecord_parent_practice',true);
							project_region = o_projectOBJ.getFieldText('custentity_region');
							
							//
							 i_vertical =  o_projectOBJ.getFieldValue('custentity_vertical');
					         nlapiLogExecution('DEBUG', 'afterSubmit_cross_project', ' Vertical-->' + i_vertical);
							 
							 i_project_services =  o_projectOBJ.getFieldValue('custentity_project_services'); //Project Services
					         
					         var i_customer_id	=	o_projectOBJ.getFieldValue('parent');
							    if(_logValidation(i_customer_id))
								{
							    	i_territory	=	nlapiLookupField('customer', i_customer_id, 'territory');
									region_id = nlapiLookupField('customer', i_customer_id, 'custentity_region');
									cust_name = nlapiLookupField('customer', i_customer_id, 'companyname');
								}
									
							var billing_type = o_projectOBJ.getFieldText('jobbillingtype');
							nlapiLogExecution('audit','billing_type:-- ',billing_type);
							if(!_logValidation(billing_type))
							{
						    	billing_type	=	'';
							}
										
						}//Project OBJ
					}//Project ID				
												
						if(!_logValidation(i_customer))
						{
							i_customer = '';
						}
						if(!_logValidation(s_project_description))
						{
							s_project_description = '';
						}
						if(!_logValidation(i_vertical))
						{
							i_vertical = '';
						}
						if(!_logValidation(i_practice))
						{
							i_practice = '';
						}
						if(!_logValidation(i_location))
						{
							i_location = '';
						}
						//
						proj_category_val = o_projectOBJ.getFieldValue('custentity_project_allocation_category');
						
						if(proj_category_val)
						{
							if((parseInt(proj_category_val)==parseInt(1)) && (core_practice =='T'))
							{
								misc_practice=emp_practice;
								//var is_practice_active = nlapiLookupField('department',parseInt(misc_practice),['isinactive']);
								//isinactive_Practice_e = is_practice_active.isinactive;
								misc_practice = emp_practice_text;
							}
							else 
							{
								misc_practice=o_projectOBJ.getFieldValue('custentity_practice');
								var is_practice_active = nlapiLookupField('department',parseInt(misc_practice),['isinactive']);
								isinactive_Practice_e = is_practice_active.isinactive;
								misc_practice=o_projectOBJ.getFieldText('custentity_practice');
							}
						}
						nlapiLogExecution('audit','Pratice','misc_practice:'+misc_practice);
						if(misc_practice && isinactive_Practice_e == 'F'){
						o_recordOBJ.setLineItemValue(s_line,'custcol_mis_practice',i, misc_practice);
						}
						//
						o_recordOBJ.setLineItemValue(s_line,'custcolcustcol_temp_customer',i,i_customer);
						o_recordOBJ.setLineItemValue(s_line,'custcolprj_name',i,s_project_description);
						o_recordOBJ.setLineItemValue(s_line,'class',i,i_vertical);
						o_recordOBJ.setLineItemValue(s_line,'department',i,i_practice);
						o_recordOBJ.setLineItemValue(s_line,'location',i,i_location);
						o_recordOBJ.setLineItemValue(s_line,'custcol_employeenamecolumn',i,s_employee_name);
						o_recordOBJ.setLineItemValue(s_line,'custcol_territory', i, i_territory);
						o_recordOBJ.setLineItemValue(s_line,'custcol_onsite_offsite', i, onsite_offsite);
						o_recordOBJ.setLineItemValue(s_line,'custcol_billing_type', i, billing_type);
						o_recordOBJ.setLineItemValue(s_line,'custcol_employee_type', i, emp_type);
						o_recordOBJ.setLineItemValue(s_line,'custcol_person_type', i, person_type);
						o_recordOBJ.setLineItemValue(s_line,'custcol_parent_executing_practice', i, parent_practice);
						o_recordOBJ.setLineItemValue(s_line,'custcol_project_region', i, project_region);
						o_recordOBJ.setLineItemValue(s_line,'taxcode',i,2901);
						
						
						if(!_logValidation(i_project_name_ID))
						{
							i_project_name_ID = '';
						}
						
						o_recordOBJ.setLineItemValue(s_line,'custcol_project_entity_id', i, i_project_name_ID);
						var cust_entity_id = '';
						if(_logValidation(i_customer))
						{
							cust_entity_id = i_customer.split(' ');
							cust_entity_id = cust_entity_id[0];
						}
						o_recordOBJ.setLineItemValue(s_line,'custcol_customer_entityid', i, cust_entity_id);
						var s_employee_name_split = s_employee_name.split('-');
						s_employee_name_split = s_employee_name_split[0];
						o_recordOBJ.setLineItemValue(s_line,'custcol_employee_entity_id', i, s_employee_name_split);
						
						//Income St Lines
						o_recordOBJ.setLineItemValue(s_line,'custcol_cust_name_on_a_click_report',i,cust_name);
						o_recordOBJ.setLineItemValue(s_line,'custcol_proj_name_on_a_click_report', i, i_project_name);
						o_recordOBJ.setLineItemValue(s_line,'custcol_proj_category_on_a_click', i, i_category); 
						o_recordOBJ.setLineItemValue(s_line,'custcol_emp_name_on_a_click_report', i, emp_full_name);
						o_recordOBJ.setLineItemValue(s_line,'custcol_region_master_setup', i, region_id);
						o_recordOBJ.setLineItemValue(s_line,'custcol_transaction_project_services', i, i_project_services);
						nlapiLogExecution('audit','cust_name:-- ',cust_name);
						nlapiLogExecution('audit','i_project_name:-- ',i_project_name);
						nlapiLogExecution('audit','i_category:-- ',i_category);
						nlapiLogExecution('audit','emp_full_name:-- ',emp_full_name);
						nlapiLogExecution('audit','region_id:-- ',region_id);
											
					}//Loop				
				}//Line Count
				
				var i_submitID = nlapiSubmitRecord(o_recordOBJ,true,true);
				nlapiLogExecution('DEBUG', 'afterSubmit_cross_project', ' ***************** Submit ID ****************-->' + i_submitID);
						
							
			}//Record OBJ		
		}//Record ID & Record Type 
	
	}//Record Type - Expense Report		
		
		
				
	if(s_record_type == 'timebill')
	{
		var i_vertical ='';
		var s_Employee_Name = '';
		
		if (_logValidation(i_recordID) && _logValidation(s_record_type)) 
		{
			var o_recordOBJ = nlapiLoadRecord(s_record_type, i_recordID);
			
			if(_logValidation(o_recordOBJ))
			{			
				var i_employee = o_recordOBJ.getFieldValue('employee'); 
				
				if(_logValidation(i_employee))
				{
				   var o_employeeOBJ = nlapiLoadRecord('employee',i_employee);
				
					if (_logValidation(o_employeeOBJ)) 
					{
						i_practice = o_employeeOBJ.getFieldValue('department');
						nlapiLogExecution('DEBUG', 'afterSubmit_cross_project', ' Practice-->' + i_practice);
						
						s_Employee_Name = o_employeeOBJ.getFieldValue('entityid');
						nlapiLogExecution('DEBUG', 'afterSubmit_cross_project', ' s_Employee_Name-->' + s_Employee_Name);
					}				
					
				}//Employee	
				
				var i_projectID = o_recordOBJ.getFieldValue('customer');
				nlapiLogExecution('DEBUG', 'afterSubmit_cross_project', ' Project ID -->' + i_projectID);
				
				if(_logValidation(i_projectID))
				{						
			    var o_projectOBJ = nlapiLoadRecord('job',i_projectID);
				
				if(_logValidation(o_projectOBJ))
				{
					var i_project_name_ID =  o_projectOBJ.getFieldValue('entityid');
					nlapiLogExecution('DEBUG', 'afterSubmit_cross_project', ' Project Name ID -->' + i_project_name_ID);
						
					var i_project_name =  o_projectOBJ.getFieldValue('companyname');
					nlapiLogExecution('DEBUG', 'afterSubmit_cross_project', ' Project Name -->' + i_project_name);
					
					var s_project_description = i_project_name_ID+' '+i_project_name;
					nlapiLogExecution('DEBUG', 'afterSubmit_cross_project', ' Project Description -->' + s_project_description);
					
					var i_customer =  o_projectOBJ.getFieldText('parent');
					nlapiLogExecution('DEBUG', 'afterSubmit_cross_project', ' Customer-->' + i_customer);
					
					 i_vertical =  o_projectOBJ.getFieldValue('custentity_vertical');
					 nlapiLogExecution('DEBUG', 'afterSubmit_cross_project', ' Vertical-->' + i_vertical);
							
						if(!_logValidation(i_customer))
						{
							i_customer = '';
						}
						if(!_logValidation(s_project_description))
						{
							s_project_description = '';
						}
						if(!_logValidation(i_vertical))
						{
							i_vertical = '';
						}
						if(!_logValidation(i_practice))
						{
							i_practice = '';
						}
						
						if(!_logValidation(s_Employee_Name))
						{
							s_Employee_Name = '';
						}
					o_recordOBJ.setFieldValue('custcolprj_name',s_project_description);
					o_recordOBJ.setFieldValue('custcolcustcol_temp_customer',i_customer);
					o_recordOBJ.setFieldValue('class',i_vertical);					
					o_recordOBJ.setFieldValue('department',i_practice);
					o_recordOBJ.setFieldValue('custcol_employeenamecolumn',s_Employee_Name);
					
					
					var i_submitID = nlapiSubmitRecord(o_recordOBJ,true,true);
				    nlapiLogExecution('DEBUG', 'afterSubmit_cross_project', ' ***************** Submit ID ****************-->' + i_submitID);
									
				}//Project OBJ					
				}//Project ID						
				
			}//Record OBJ			
		}//Record ID & Record Type		
	}//Timesheet			  	
   }
   catch(exception)
   {
	  nlapiLogExecution('DEBUG','ERROR','  Exception Caught -->'+exception);	
   }
  	
  }//Create	
 	

	return true;

}

// END AFTER SUBMIT ===============================================





// BEGIN FUNCTION ===================================================
function _logValidation(value) 
{
 if(value != null && value.toString() != null && value != '' && value != undefined && value.toString() != undefined && value != 'undefined' && value.toString() != 'undefined'&& value.toString() != 'NaN' && value != NaN) 
 {
  return true;
 }
 else 
 { 
  return false;
 }
}

// END FUNCTION =====================================================