/**
 *
 * Script Name: SCH | Resource Allocation Extension
 * Author:Praveena Madem
 * Company:INSPIRRIA CLOUDTECH
 * Date: 17 - Feb - 2020
 * Description:1. This script will update the enddate of resource allocations (extend the allocations)
						   
 * Script Modification Log:
 *  -- Date --			-- Modified By --				--Requested By--				-- Description --
*/

function SCH_AllocationExtension(a_update_allocationDetails)
{
	//get script parameter values
var context = nlapiGetContext();
var a_update_allocationDetails =[];
var s_projectname=context.getSetting('SCRIPT', 'custscript_projectname');
var s_username=context.getSetting('SCRIPT', 'custscript_username');
var new_project_end_date=context.getSetting('SCRIPT', 'custscript_new_project_end_date');
a_update_allocationDetails=context.getSetting('SCRIPT', 'custscript_update_allocation_details');
a_update_allocationDetails=JSON.parse(a_update_allocationDetails);
//nlapiLogExecution('debug','a_update_allocationDetails',a_update_allocationDetails);
var tableData="";
	 for (var linenum = 0; a_update_allocationDetails!=null && a_update_allocationDetails.length-1>=linenum; linenum++) {
       
       if(context.getRemainingUsage() < 300){
         nlapiLogExecution('debug','context.getRemainingUsage()',context.getRemainingUsage());
         nlapiYieldScript()}
           var newEndDate = a_update_allocationDetails[linenum]["newEndDate"];
            var empsubsidiary = a_update_allocationDetails[linenum]["empsubsidiary"];
         //   nlapiLogExecution('debug', 'Subsidiary : newEndDate' , empsubsidiary+" :"+newEndDate);
            //nlapiLogExecution('debug', 'new allocation end date', newEndDate);
            var newrefenddate = a_update_allocationDetails[linenum]["newrefenddate"]
            if (newEndDate || newrefenddate) {
                var allocationId =a_update_allocationDetails[linenum]["id"];
				var enddate = a_update_allocationDetails[linenum]["enddate"];
				var refenddate=a_update_allocationDetails[linenum]["refenddate"];
                var allocationRecord = nlapiLoadRecord('resourceallocation',allocationId);
                var allocationResource = allocationRecord.getFieldValue('allocationresource');
				var s_resource = allocationRecord.getFieldText('allocationresource');
				
                try {
                    allocationRecord.setFieldValue('enddate', newEndDate);
                    allocationRecord.setFieldValue('custeventbenddate',newEndDate);
                    allocationRecord.setFieldValue('notes', 'Project Extended');
                    allocationRecord.setFieldValue('custevent_allocation_status', '18');
                    nlapiSubmitRecord(allocationRecord, false, true);
                       //nlapiLogExecution('debug', 'allocationRecord', allocationRecord);
                   // var employeeType = nlapiLookupField('employee', allocationResource, 'custentity_persontype');
                    // No need to check for employee type	

                    //if(employeeType == '1')
                    if (empsubsidiary == '2') {
                        var oldEndDate_res = allocationRecord.getFieldValue('enddate');
                        var oldStartDate_res = allocationRecord.getFieldValue('startdate');
                       var isvendor=a_update_allocationDetails[linenum]["isvendor"];
						var isrefendDate=a_update_allocationDetails[linenum]["isrefendDate"];
						
                        var filters = new Array();
                        filters[0] = new nlobjSearchFilter('custrecord_stvd_contractor', null, 'anyof', parseInt(allocationResource));
                        filters[1] = new nlobjSearchFilter('isinactive', null, 'is', 'F');

                        var columns = new Array();
                        columns[0] = new nlobjSearchColumn('custrecord_stvd_start_date');
                        columns[1] = new nlobjSearchColumn('custrecord_stvd_end_date').setSort(true);
                        columns[2] = new nlobjSearchColumn('custrecord_stvd_vendor_type');

                        var search_results = searchRecord('customrecord_subtier_vendor_data', null, filters, columns);

                       // nlapiLogExecution('DEBUG', 'refvendor', isrefendDate);
                        if (isArrayNotEmpty(search_results)) {
                            //flag it if it is updated for first vendor which is the latest vendor
                            var flag = true;
                            var flag2 = true;
                            nlapiLogExecution('Debug', 'flag', flag);

                            for (var k = 0; k < search_results.length; k++) {
                                //
                                if (isrefendDate == '' || isrefendDate == null) {
                                    nlapiLogExecution('Debug', 'vendor type1  dd', search_results[k].getValue('custrecord_stvd_vendor_type') + " " + isrefendDate);
                                    if (flag == true) {
                                        if (search_results[k].getValue('custrecord_stvd_vendor_type') == '1') {
                                            if (nlapiStringToDate(newEndDate) > nlapiStringToDate(search_results[k].getValue('custrecord_stvd_end_date'))) {
                                                if (new_project_end_date) {
                                                    if (nlapiStringToDate(new_project_end_date) >= nlapiStringToDate(newEndDate)) {
                                                        var a = nlapiSubmitField('customrecord_subtier_vendor_data', search_results[k].getId(), 'custrecord_stvd_end_date', newEndDate);
                                                        flag = false;
                                                    }
                                                } else {
                                                    if (nlapiStringToDate(old_project_end_date) >= nlapiStringToDate(newEndDate)) {
                                                        var a = nlapiSubmitField('customrecord_subtier_vendor_data', search_results[k].getId(), 'custrecord_stvd_end_date', newEndDate);
                                                        flag = false;
                                                    }
                                                }

                                            } else {
                                                if (new_project_end_date) {
                                                    if (nlapiStringToDate(new_project_end_date) >= nlapiStringToDate(newEndDate)) {
                                                        var a = nlapiSubmitField('customrecord_subtier_vendor_data', search_results[k].getId(), 'custrecord_stvd_end_date', newEndDate);
                                                        flag = false;
                                                    }
                                                } else {
                                                    if (nlapiStringToDate(old_project_end_date) >= nlapiStringToDate(newEndDate)) {
                                                        var a = nlapiSubmitField('customrecord_subtier_vendor_data', search_results[k].getId(), 'custrecord_stvd_end_date', newEndDate);
                                                        flag = false;
                                                    }
                                                }

                                            }
                                        }
                                    }
                                } else {
                                    if (isvendor != '' || isvendor != null) {

                                        if (search_results[k].getValue('custrecord_stvd_vendor_type') == '2') {
                                            if (flag2 == true) {
                                                if (nlapiStringToDate(newrefenddate) > nlapiStringToDate(search_results[k].getValue('custrecord_stvd_end_date'))) {
                                                    if (new_project_end_date) {
                                                        if (nlapiStringToDate(new_project_end_date) >= nlapiStringToDate(newrefenddate)) {
                                                            var a = nlapiSubmitField('customrecord_subtier_vendor_data', search_results[k].getId(), 'custrecord_stvd_end_date', newrefenddate);
                                                            flag2 = false;
                                                        } else {
                                                            var e = nlapiCreateError('404', 'Referral End Date should be Less or equal to Project end Date');
                                                            throw e;
                                                        }
                                                    } else {
                                                        if (nlapiStringToDate(old_project_end_date) >= nlapiStringToDate(newrefenddate)) {
                                                            var a = nlapiSubmitField('customrecord_subtier_vendor_data', search_results[k].getId(), 'custrecord_stvd_end_date', newrefenddate);
                                                            flag2 = false;
                                                        } else {
                                                            var e = nlapiCreateError('404', 'Referral End Date should be Less or equal to Project end Date');
                                                            throw e;
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        /*else{
                                        	if(nlapiStringToDate(newrefenddate) > nlapiStringToDate(search_results[k].getValue('custrecord_stvd_end_date')))
                                        	{
                                        		if(new_project_end_date)
                                        		{
                                        			if(nlapiStringToDate(new_project_end_date)>= nlapiStringToDate(newrefenddate))
                                        				{
                                        				var a = nlapiSubmitField('customrecord_subtier_vendor_data', search_results[k].getId(), 'custrecord_stvd_end_date', newrefenddate);
                                        				}
                                        		}
                                        		else
                                        			{
                                        			if(nlapiStringToDate(old_project_end_date)>= nlapiStringToDate(newEndDate))
                                        			var a = nlapiSubmitField('customrecord_subtier_vendor_data', search_results[k].getId(), 'custrecord_stvd_end_date', newrefenddate);
                                        			}
                                        	}
                                        }*/
                                        else {
                                            nlapiLogExecution('Debug', 'vendor type', search_results[k].getText('custrecord_stvd_vendor_type'));
                                            if (flag == true) {
                                                if (search_results[k].getValue('custrecord_stvd_vendor_type') == '1') {
                                                    if (nlapiStringToDate(newEndDate) > nlapiStringToDate(search_results[k].getValue('custrecord_stvd_end_date'))) {
                                                        if (new_project_end_date) {
                                                            if (nlapiStringToDate(new_project_end_date) >= nlapiStringToDate(newEndDate)) {
                                                                var a = nlapiSubmitField('customrecord_subtier_vendor_data', search_results[k].getId(), 'custrecord_stvd_end_date', newEndDate);
                                                                nlapiLogExecution('Debug', 'Submitted first', flag);
                                                                flag = false;
                                                            }
                                                        } else {
                                                            if (nlapiStringToDate(old_project_end_date) >= nlapiStringToDate(newEndDate)) {
                                                                var a = nlapiSubmitField('customrecord_subtier_vendor_data', search_results[k].getId(), 'custrecord_stvd_end_date', newEndDate);
                                                                nlapiLogExecution('Debug', 'Submitted in else', flag);
                                                                flag = false;
                                                            }
                                                        }
                                                    }
                                                }
                                           }
                                        }
                                    }
                                }
                            }
                        }
                    }

                    //	nlapiLogExecution('debug', 'allocation updated',allocationId);
                    nlapiLogExecution('debug', 'Resource updated', allocationResource);
					tableData += "<tr>";
                    tableData += "<td>" +
                        s_resource +
                        "</td>";
                    tableData += "<td>" +
                        enddate + "</td>";
                    tableData += "<td>" +
                        newEndDate + "</td>";
                    tableData += "<td>" +
                        refenddate + "</td>";
                    tableData += "<td>" +
                       newrefenddate + "</td>";
                    tableData += "</tr>";
                } catch (e) {

                   nlapiLogExecution('error',
                        'Failed while updating the record', e);
                    tableData += "<tr>";
                    tableData += "<td>" +
                        s_resource +
                        "</td>";
                    tableData += "<td>" +
                        enddate + "</td>";
                    if (e.name == '404') {
                        tableData += "<td>" +
                            newenddate + "</td>";

                    } else {

                        tableData += "<td>" + e + "</td>";

                    }
                  
					tableData += "<td>" +
                        refenddate + "</td>";
                    if (e.name != '404') {
                        tableData += "<td>" +
                           refenddate + "</td>";
                    } else {
                        tableData += "<td>" + e + "</td>";
                    }
                    tableData += "</tr>";
                    
                }
            }
        }
		
		 var tableDate1 = "<table><tr><td>Resource</td><td>Old End Date</td><td>New End Date</td><td>Old Referral End Date</td><td>New Referral End Date</td></tr>" +
            tableData + "</table>";
        //nlapiLogExecution('debug', 'tableDate1', tableData);

        // send email
        if (tableData) {

            var mailContent = getAllocationUpdateMailTemplate(tableDate1,
                s_username, s_projectname);
            // get the email id from the context
            var emails = "business.ops@brillio.com; shruthi.l@brillio.com";

            if (emails) {
                nlapiSendEmail(constant.Mail_Author.InformationSystems, emails,mailContent.Subject, mailContent.Body);
            }
        }
		 nlapiLogExecution('error','Remaining Usage', context.getRemainingUsage());
		}


function getAllocationUpdateMailTemplate(tableData, userName, projectName) {
    var htmltext = '<table border="0" width="100%">';
    htmltext += '<tr>';
    htmltext += '<td colspan="4" valign="top">';
    htmltext += '<p>Hi,</p>';
    htmltext += '<p></p>';
    htmltext += '<p>This is to inform you that <b>' + userName +
        '</b> has updated allocation details for <b>' + projectName +
        '</b>.</p>';
    htmltext += tableData;
    htmltext += '<p></p>';
    htmltext += '</td></tr>';
    htmltext += '<tr></tr>';
    htmltext += '<tr></tr>';
    htmltext += '<p><br/>Regards,<br/>';
    htmltext += 'Information Systems</p>';
    htmltext += '</table>';
    htmltext += '<hr width="100%" size="1" noshade color="#CCCCCC">';
    htmltext += '<table width="100%" border="0" cellspacing="0" cellpadding="0">';
    htmltext += '<tr>';
    htmltext += '<td align="right">';
    htmltext += '<font style="font-size:9px; font-family:Verdana,Arial,Helvetica,sans-serif; color:#999999;">Brillio is powered by <a href="http://www.netsuite.com/" style="color:#999999;">NetSuite</a> &#151; One System. No Limits.</font>';
    htmltext += '</td>';
    htmltext += '</tr>';
    htmltext += '</table>';
    htmltext += '</body>';
    htmltext += '</html>';

    return {
        Subject: 'Allocation Updation',
        Body: htmltext
    };
};
