function Main() {
    try {
        var currentContext_C = nlapiGetContext();
        var todayDate = new Date();
        nlapiLogExecution('debug', 'todayDate', todayDate);
        var currentTimeStamp = moment(todayDate).format("DD-MMM-YYYY HH:mm:ss")
        var current_day = todayDate.getDay();
        var end_day = '';
        if(current_day == 0){
            end_day = 8;
        }
        else if(current_day == 1){
            end_day = 9;
        } else if(current_day == 2){
            end_day = 10;
        } else if(current_day == 3){
            end_day = 11;
        } else {
            end_day = current_day+1;
        }

        var d_end_date = nlapiAddDays(todayDate, -end_day);
        nlapiLogExecution('debug', 'd_end_date', d_end_date);

       // var request_from_date = nlapiAddDays(d_end_date, -69); commented for testing
        var request_from_date = nlapiAddDays(d_end_date, -97);
        nlapiLogExecution('debug', 'request_from_date', request_from_date);
        var sunday_of_end_week = nlapiAddDays(request_from_date, -current_day);
        nlapiLogExecution('debug', 'sunday_of_end_week', sunday_of_end_week);

        var filters = new Array();
        filters[0] = new nlobjSearchFilter('jobbillingtype', null,
            'anyof', "TM");
        filters[1] = new nlobjSearchFilter('type', null, 'anyof',
        "2");
        

        var columns = new Array();

        columns[0] = new nlobjSearchColumn('internalid', null, 'group').setSort(true);
        

        try {
            var search_results = searchRecord('job', null, filters,
                columns);
        } catch (e) {
            nlapiLogExecution('ERROR', 'Error', e.message);
        }

        var projectsList = new Array();
        
        nlapiLogExecution('DEBUG', 'search_results', JSON.stringify(search_results));

        if (search_results != null && search_results.length > 0) {
            for (var i_search_indx = 0; i_search_indx < search_results.length; i_search_indx++) {

                var projectId = search_results[i_search_indx].getValue(columns[0]);
                projectsList.push(projectId);

            }
        }

        nlapiLogExecution('DEBUG', 'projectsList', JSON.stringify(projectsList));
      
      
      ////////////    --------Time Approver ---------------/////
      var timeApproverList = new Array();

var filtersEmp = new Array();
filtersEmp[0] = new nlobjSearchFilter('custentity_implementationteam', null,'is', "F");     
			
var columnsEmp = new Array();
columnsEmp[0] = new nlobjSearchColumn('timeapprover', null, 'group');
		
try {
            var search_results_EMP = searchRecord('employee', null, filtersEmp,columnsEmp);
        } catch (e) {
            nlapiLogExecution('ERROR', 'Error', e.message);
        }

 if (search_results_EMP != null && search_results_EMP.length > 0) {
            for (var i_search_indx = 0; i_search_indx < search_results_EMP.length; i_search_indx++) {

                var timeApprover = search_results_EMP[i_search_indx].getValue(columnsEmp[0]);
				
				if(_logValidation(timeApprover) && timeApproverList.indexOf(timeApprover)==-1){
					timeApproverList.push(timeApprover);
				}
				
				
			}
 }
 
  nlapiLogExecution('debug', 'timeApproverList', JSON.stringify(timeApproverList));
 
 
 
 //var timeApproverListTA = new Array();
/*
var filtersEmpTA = new Array();
filtersEmpTA[0] = new nlobjSearchFilter('internalid', null,'anyof', timeApproverList);   
filtersEmpTA[1] = new nlobjSearchFilter('subsidiary', null,'anyof', ['3','27','23']);  
			
var columnsEmpTA = new Array();
columnsEmpTA[0] = new nlobjSearchColumn('timeapprover', null, 'group');
		
try {
            var search_results_EMP_TA = searchRecord('employee', null, filtersEmpTA,columnsEmpTA);
        } catch (e) {
            nlapiLogExecution('ERROR', 'Error', e.message);
        }

 if (search_results_EMP_TA != null && search_results_EMP_TA.length > 0) {
            for (var i_search_indx = 0; i_search_indx < search_results_EMP_TA.length; i_search_indx++) {

                var timeApproverTA = search_results_EMP_TA[i_search_indx].getValue(columnsEmpTA[0]);
				
				if(_logValidation(timeApproverTA) && timeApproverListTA.indexOf(timeApproverTA)==-1){
					timeApproverListTA.push(timeApproverTA);
				}
				
				
			}
 }
 
 nlapiLogExecution('debug', 'timeApproverListTA', JSON.stringify(timeApproverListTA));
 */
		
		
		
		
		var filtersTS = new Array();
        filtersTS[0] = new nlobjSearchFilter('approvalstatus', null,
            'anyof', "2");
        filtersTS[1] = new nlobjSearchFilter('timesheetdate', null, 'within', request_from_date,
            d_end_date);
	    filtersTS[2] = new nlobjSearchFilter('customer', 'timebill', 'anyof', projectsList);  //filtersTS[2]
	    filtersTS[3] = new nlobjSearchFilter('custrecord_ts_time_approver', null, 'anyof', timeApproverList);
	    //filtersTS[1] = new nlobjSearchFilter('custrecord_ts_time_approver', null, 'anyof', '1618'); //test Line
        

        var columnsTS = new Array();

        columnsTS[0] =   new nlobjSearchColumn("employee",null,"GROUP"), 
        columnsTS[1] =   new nlobjSearchColumn("startdate",null,"GROUP"), 
        columnsTS[2] =   new nlobjSearchColumn("enddate",null,"GROUP"), 
        columnsTS[3] =   new nlobjSearchColumn("approvalstatus",null,"GROUP"), 
        columnsTS[4] =   new nlobjSearchColumn("submittedhoursdecimal",null,"GROUP"), 
        columnsTS[5] =   new nlobjSearchColumn("customer","timeBill","GROUP"), 
        columnsTS[6] =   new nlobjSearchColumn("custrecord_ts_time_approver",null,"GROUP").setSort(true)
        

        try {
            var search_resultsTS = searchRecord('timesheet', null, filtersTS,
                columnsTS);
        } catch (e) {
            nlapiLogExecution('ERROR', 'Error', e.message);
        }
		
		var TAPendingList = new Object();
		var TimeApproverList = new Object();
		var ProjectNames = new Object();
		var employeeList = new Object();
		
		if (search_resultsTS != null && search_resultsTS.length > 0) {
            for (var i_indx = 0; i_indx < search_resultsTS.length; i_indx++) {

                if(currentContext_C.getRemainingUsage() <= 2000){
                    yieldScript(currentContext_C);
                    }

                var s_employee_name = search_resultsTS[i_indx].getValue(columnsTS[0]);
                var employeeName =  search_resultsTS[i_indx].getText(columnsTS[0]);
                var startDate = search_resultsTS[i_indx].getValue(columnsTS[1]);
                var endDate = search_resultsTS[i_indx].getValue(columnsTS[2]);
                var approvalStatus = search_resultsTS[i_indx].getText(columnsTS[3]);
                var durationDecimal = search_resultsTS[i_indx].getValue(columnsTS[4]);
                var project = search_resultsTS[i_indx].getValue(columnsTS[5]);
                var projectName = search_resultsTS[i_indx].getText(columnsTS[5]);
                var timeApprover = search_resultsTS[i_indx].getValue(columnsTS[6]);
                var timeApprover_name = search_resultsTS[i_indx].getText(columnsTS[6]);
				
				if(!_logValidation(TimeApproverList[timeApprover])){
                    TimeApproverList[timeApprover] = timeApprover_name;
                }
				
				if(!_logValidation(ProjectNames[project])){
                    ProjectNames[project] = projectName;
                }
				
				if(!_logValidation(employeeList[s_employee_name])){
                    employeeList[s_employee_name] = employeeName;
                }
				
				
		if(!_logValidation(TAPendingList[timeApprover])){

			 TAPendingList[timeApprover] = new Object();
			 TAPendingList[timeApprover][project] = new Object();
			 TAPendingList[timeApprover][project][s_employee_name] = new Array();		 
			 TAPendingList[timeApprover][project][s_employee_name].push(durationDecimal);
			 
		} else{
			if(!_logValidation(TAPendingList[timeApprover][project])){
			 TAPendingList[timeApprover][project] = new Object();
			 TAPendingList[timeApprover][project][s_employee_name] = new Array();		 
			 TAPendingList[timeApprover][project][s_employee_name].push(durationDecimal);
				
				
			} else {
				if(!_logValidation(TAPendingList[timeApprover][project][s_employee_name])){
			 TAPendingList[timeApprover][project][s_employee_name] = new Array();		 
			 TAPendingList[timeApprover][project][s_employee_name].push(durationDecimal);
				
				
			  } else {
				  TAPendingList[timeApprover][project][s_employee_name].push(durationDecimal);
			  }
				
			}
			
		}

			 

	
				
				
				
				
				
				
				
				
				
				
				
			}
			
		}
      
       //nlapiLogExecution('DEBUG', 'TAPendingList', JSON.stringify(TAPendingList));
		
		//var pendingTimesheets = new Array();
        //var timeApproversPendingTS = new Object();
        
		
		for(var ta in TAPendingList){
            nlapiLogExecution('DEBUG', 'Usage--', currentContext_C.getRemainingUsage());
            if(currentContext_C.getRemainingUsage() <= 2000){
                yieldScript(currentContext_C);
                }
			var htmlBody = '';
			
            htmlBody +='<!DOCTYPE html>';
            htmlBody +='<html lang="en" xmlns="http://www.w3.org/1999/xhtml" xmlns:o="urn:schemas-microsoft-com:office:office">'
            htmlBody +='<head>';
            htmlBody +='<meta charset="utf-8">';
            htmlBody +='<meta name="viewport" content="width=device-width,initial-scale=1">';
            htmlBody +='<meta name="x-apple-disable-message-reformatting">';
            htmlBody +='<title></title>';
            htmlBody +='<!--[if mso]>';
            htmlBody +='  <style>';
            htmlBody +='    table {border-collapse:collapse;border-spacing:0;border:none;margin:0;}';
            htmlBody +='    div, td {padding:0;}';
            htmlBody +='    div {margin:0 !important;}';
            htmlBody +='	</style>';
            htmlBody +='  <noscript>';
            htmlBody +='    <xml>';
            htmlBody +='      <o:OfficeDocumentSettings>';
            htmlBody +='        <o:PixelsPerInch>96</o:PixelsPerInch>';
            htmlBody +='      </o:OfficeDocumentSettings>';
            htmlBody +='    </xml>';
            htmlBody +='  </noscript>';
            htmlBody +='  <![endif]-->';
            htmlBody +='  <style>';
            htmlBody +='table, td, div, h1, p {';
            htmlBody +='font-family: Calibri;';
            htmlBody +='    }';
            htmlBody +='    @media screen and (max-width: 530px) {';
            htmlBody +='      .unsub {';
            htmlBody +='        display: block;';   
            htmlBody +='        padding: 8px;';
            htmlBody +='        margin-top: 14px;';
            htmlBody +='        border-radius: 6px;';
            htmlBody +='        background-color: #555555;';
            htmlBody +='       text-decoration: none !important;';
            htmlBody +='        font-weight: bold;';
            htmlBody +='      }';
            htmlBody +='      .col-lge {';
            htmlBody +='        max-width: 100% !important;';
            htmlBody +='      }';
            htmlBody +='    }';                                     
            htmlBody +='    @media screen and (min-width: 531px) {';
            htmlBody +='      .col-sml {';                  
            htmlBody +='       max-width: 27% !important;';
            htmlBody +='     }';
            htmlBody +='      .col-lge {';
            htmlBody +='        max-width: 73% !important;';
            htmlBody +='      }';
            htmlBody +='    }';
            htmlBody +='  </style>';
            htmlBody +='</head>';
            htmlBody +='<body style="margin:0;padding:0;word-spacing:normal;">';                                                                                                    
            htmlBody +='  <div role="article" aria-roledescription="email" lang="en" style="text-size-adjust:100%;-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;">';
            htmlBody +='    <table role="presentation" style="width:100%;border:none;border-spacing:0;">';
            htmlBody +='      <tr>';                                    
            htmlBody +='        <td align="center" style="padding:0;">';
            htmlBody +='          <!--[if mso]>';                                                   
            htmlBody +='          <table role="presentation" align="center" style="width:600px;">';
            htmlBody +='          <tr>';
            htmlBody +='          <td>';
            htmlBody +='          <![endif]-->';                                                                                                                                                                            
            htmlBody +='          <table role="presentation" style="width:94%;max-width:600px;border:none;border-spacing:0;text-align:left;font-family:Calibri;font-size:16px;line-height:22px;color:#363636;">';
            htmlBody +='            <tr>';                                                                                          
            htmlBody +='              <td style="padding:40px 30px 30px 30px;text-align:center;font-size:24px;font-weight:bold;">';
            htmlBody +='              </td>';
            htmlBody +='           </tr>';
            htmlBody +='            <tr>';                                                  
            htmlBody +='              <td style="padding:30px;background-color:#ffffff;">';  
      //nlapiLogExecution('debug', "TAName = ", ta);
      //console.log("TA = " + ta);
      var totalHours = 0;
      var noTS = 0;
      var tCounter = 0;
      
      var EmployeeName = TimeApproverList[ta].split("-");
        if(EmployeeName.length>1){
            EmployeeName = EmployeeName[1];
        }
        htmlBody +=' <img src="https://3883006-sb1.app.netsuite.com/core/media/media.nl?id=4758119&c=3883006_SB1&h=peNTKgeVIkN5vrAokPXC5b11W9HM4CQL6cBRYsRaul8mCJTe" width="100%" alt="Logo" style="max-width:100%;height:auto;border:none;text-decoration:none;color:#ffffff;">';

        htmlBody +='                 <p><span style="font-family:"calibri";font-size:11pt">Dear <strong>'+EmployeeName+'</strong>,</span></p>';  
        htmlBody +='   <p><span style="font-family:"calibri";font-size:11pt"> Ensuring weekly timesheets are approved on schedule is critical for our financial operations, including billing and revenue recognition, in addition to maintaining compliance with client contractual requirements.&nbsp;As a reminder, timesheet approvers are responsible for approving all timesheets by the end-of-day Thursday each week. Timesheet approvals are considered late if not completed by end-of-day (PST) Thursdays.&nbsp;</span></p>';
        htmlBody +='  <p><span style="font-family:"calibri";font-size:11pt"><strong>Please find below the list of timesheet approvals that are showing late as per our records.</strong>&nbsp;We request you review the list and, if appropriate, approve the missing timesheets immediately. If you need any assistance,<a href="https://brillioonline.sharepoint.com/sites/Digitaloffice/Shared Documents/Forms/AllItems.aspx?id=%2Fsites%2FDigitaloffice%2FShared%20Documents%2FOnTheGo%20Support%2FTimesheet%20Management%2Epdf&parent=%2Fsites%2FDigitaloffice%2FShared%20Documents%2FOnTheGo%20Support&p=true">click here for instructions on approving the timesheets</a> or reach out to timesheet@brillio.com</span></p> ';
        htmlBody +='<p><span style="font-family:"calibri";font-size:11pt"><a href="https://onthego.brillio.com/dashboard/timesheet-approval">Click here to approve your timesheet(s) in OnTheGo</a></span></p> ';
		htmlBody +='   <div style="overflow-x:auto;">';
        htmlBody +='<table style="border:1px solid black"><tbody><tr><th style="border:1px solid black; text-align:left;"><b>Project</b></th><th style="border:1px solid black;text-align:left;" ><b>Employee</b></th><th style="border:1px solid black"><b>No.of Timesheets</b></th><th style="border:1px solid black"><b>Pending Hours</b></th>'

      
    for(var projectNAME in TAPendingList[ta]){
        //console.log('Project',projectNAME);
       // nlapiLogExecution('debug', "projectNAME = ", projectNAME);
        tCounter++;
        var proj_NoTS = 0;
        var pro_Totalhrs = 0;
      for(var employee in TAPendingList[ta][projectNAME]){
        //nlapiLogExecution('debug', "employee = ", employee);
        //nlapiLogExecution('debug', "Pending TS  = ", json_object[ta][projectNAME][employee]);
		
		var PendingTS = TAPendingList[ta][projectNAME][employee];
        var hoursPending = sumOfPending(PendingTS);
        
        totalHours+=parseFloat(hoursPending);
        noTS+=parseInt(PendingTS.length);

        pro_Totalhrs+=parseFloat(hoursPending);
        proj_NoTS+=parseInt(PendingTS.length);
		
		htmlBody +='</tr><tr><td style="border:1px solid black">'+ProjectNames[projectNAME]+'</td><td style="border:1px solid black">'+employeeList[employee]+'</td><td style="border:1px solid black;text-align:center">'+PendingTS.length+'</td><td style="border:1px solid black;text-align:center">'+parseFloat(hoursPending).toFixed(2)+'</td></tr>';

		
        		
		
		
		
        //console.log('employee ',employee);
        //console.log('Pending TS ',json_object[ta][projectNAME][employee]);
        
      }
      htmlBody +='<tr><td style="border:1px solid black"><b>Project Total</b></td><td style="border:1px solid black"></td><td style="border:1px solid black;text-align:center"><b>'+proj_NoTS+'</b></td><td style="border:1px solid black;text-align:center"><b>'+pro_Totalhrs.toFixed(2)+'</b></td></tr>';
        
    }
    
    htmlBody +='<tr><td style="border:1px solid black"><b> Overall Total</b></td><td style="border:1px solid black"></td><td style="border:1px solid black;text-align:center"><b>'+noTS+'</b></td><td style="border:1px solid black;text-align:center"><b>'+totalHours.toFixed(2)+'</b></td></tr>';

    
    htmlBody +='</tbody></table>';
    //htmlBody +='<p><span style="font-family:"calibri";font-size:11pt"><a href="https://brillioonline.sharepoint.com/sites/Digitaloffice/Shared Documents/Forms/AllItems.aspx?id=%2Fsites%2FDigitaloffice%2FShared%20Documents%2FOnTheGo%20Support%2FTimesheet%20Management%2Epdf&parent=%2Fsites%2FDigitaloffice%2FShared%20Documents%2FOnTheGo%20Support&p=true">click here for instructions on submitting your timesheet</a></span></p> ';
   // htmlBody +='<p><span style="font-family:"calibri";font-size:11pt"><a href="https://brillioonline.sharepoint.com/sites/Digitaloffice/Shared Documents/Forms/AllItems.aspx?id=%2Fsites%2FDigitaloffice%2FShared%20Documents%2FOnTheGo%20Support%2FTimesheet%20Management%2Epdf&parent=%2Fsites%2FDigitaloffice%2FShared%20Documents%2FOnTheGo%20Support&p=true">Click here for instructions on submitting your timesheet</a></span></p>';
    htmlBody +='</div>';
   // htmlBody +='   <p><span style="font-family:"calibri";font-size:11pt">&nbsp;</span></p> ';
//htmlBody +='   <p><span style="font-family:"calibri";font-size:11pt">We request you review and, if appropriate, approve the missing timesheets immediately. If you need any assistance or believe you have approved all required timesheets, please reach out to <em>timesheet@brillio.com</em> for assistance. &nbsp;</span></p>';
//htmlBody +='   <p><span style="font-family:"calibri";font-size:11pt"></span></p>';

//htmlBody +='   <p><span style="font-family:"calibri";font-size:11pt"></span></p> ';
htmlBody +='   <p><span style="font-family:"calibri";font-size:11pt">Thank you in advance for your cooperation.</span></p>';
htmlBody +='   <p><span style="font-family:"calibri";font-size:11pt">&nbsp;</span></p>';
//htmlBody +='   <p><span style="font-family:"calibri";font-size:11pt">Dated: '+currentTimeStamp+' PST</span></p>';
htmlBody +='   <p><span style="font-family:"calibri";font-size:11pt">Regards,</span></p>';
htmlBody +='  <p><strong><span style="font-family:"calibri";font-size:11pt">Brillio HR</span></strong></p>';
htmlBody +='  <img src="https://3883006-sb1.app.netsuite.com/core/media/media.nl?id=4776380&c=3883006_SB1&h=7VyKjfer-59HR7fdi1j-6Zm7AGXJ0yHs-OAhl3yEE74tv_pg" alt="Policy." width="100%">';
             
htmlBody +='              </td>';
htmlBody +='            </tr>';           
htmlBody +='            <!--tr>';
htmlBody +='              <td style="padding:35px 30px 11px 30px;font-size:0;background-color:#ffffff;border-bottom:1px solid #f0f0f5;border-color:rgba(201,201,207,.35);">';
htmlBody +='                <!--[if mso]>';
htmlBody +='                <table role="presentation" width="100%">';
htmlBody +='                <tr>';
htmlBody +='                <td style="width:145px;" align="left" valign="top">';
htmlBody +='               <![endif]-->';                
htmlBody +='                <!--[if mso]>';
htmlBody +='                </td>';
htmlBody +='                <td style="width:395px;padding-bottom:20px;" valign="top">';
htmlBody +='                <![endif]-->';
             
htmlBody +='               <!--[if mso]>';
htmlBody +='                </td>';
htmlBody +='                </tr>';
htmlBody +='                </table>';
htmlBody +='                <![endif]-->';
htmlBody +='              <!--/td>';
htmlBody +='            <!--/tr>';
htmlBody +='          </table>';
htmlBody +='          <!--[if mso]>';
htmlBody +='          </td>';
htmlBody +='          </tr>';
htmlBody +='          </table>';
htmlBody +='          <![endif]-->';
htmlBody +='        </td>';
htmlBody +='      </tr>';
htmlBody +='    </table>';
htmlBody +='  </div>';
htmlBody +='</body>';
htmlBody +='</html>';
	
	
	if(_logValidation(ta)){
      nlapiLogExecution('DEBUG', 'mail sent', ta);
      // nlapiSendEmail(442, ta, 'Timesheets Pending for Approval',htmlBody, null, null, null, null);
      try{
        nlapiSendEmail(442, ta, 'Timesheets Pending for Approval',htmlBody, null, null, null, null);
      } catch(e){
        nlapiLogExecution('DEBUG', 'e', e.message);
      }
      
       }
	

	
	
	
    }
		
		
		
		
		
		
		



       
        //nlapiLogExecution('DEBUG', 'TAPendingList', JSON.stringify(TAPendingList));

        //nlapiSendEmail(442,320475, 'TS - notification - emp -Not Approved- test', JSON.stringify(TAPendingList), null, null, null, null);














     

          


    }catch(e){
       nlapiLogExecution('DEBUG', 'Error', JSON.stringify(e));
    }
}


function searchRecord(recordType, savedSearch, arrFilters, arrColumns,
    filterExpression) {

    try {
        var search = null;

        // if a saved search is provided, load it and add the filters and
        // columns
        if (isNotEmpty(savedSearch)) {
            search = nlapiLoadSearch(recordType, savedSearch);

            if (isArrayNotEmpty(arrFilters)) {
                search.addFilters(arrFilters);
            }

            if (isArrayNotEmpty(arrColumns)) {
                search.addColumns(arrColumns);
            }

            if (isArrayNotEmpty(filterExpression)) {
                search.setFilterExpression(filterExpression);
            }
        }
        // create a new search
        else {
            search = nlapiCreateSearch(recordType, arrFilters, arrColumns);
        }

        // run search
        var resultSet = search.runSearch();

        // iterate through the search and get all data 1000 at a time
        var searchResultCount = 0;
        var resultSlice = null;
        var searchResult = [];

        do {
            resultSlice = resultSet.getResults(searchResultCount,
                searchResultCount + 1000);

            if (resultSlice) {

                resultSlice.forEach(function(result) {

                    searchResult.push(result);
                    searchResultCount++;
                });
            }
        } while (isArrayNotEmpty(resultSlice) && resultSlice.length >= 1000);

        return searchResult;
    } catch (err) {
        nlapiLogExecution('ERROR', 'searchRecord', err);
        throw err;
    }
}

function isArrayEmpty(argArray) {

    return !isArrayNotEmpty(argArray);
}

function isArrayNotEmpty(argArray) {

    return (isNotEmpty(argArray) && argArray.length > 0);
}

function isEmpty(value) {

    return value == null || value == "" || typeof(value) == undefined;
}

function isNotEmpty(value) {

    return !isEmpty(value);
}

function isTrue(value) {

    return value == 'T';
}

function isFalse(value) {

    return value != 'T';
}


function _logValidation(value) {
    if (value != '- None -' && value != null && value.toString() != null && value != '' && value != undefined && value.toString() != undefined && value != 'undefined' && value.toString() != 'undefined' && value.toString() != 'NaN' && value != NaN) {
        return true;
    } else {
        return false;
    }
}

function sumOfPending(PendingTS){
	var sum = 0.0
	for (var i = 0; i < PendingTS.length; i++) {
    sum += parseFloat(PendingTS[i]);
    }
	
	return sum;
	
}

function yieldScript(currentContext) {

	if (currentContext.getRemainingUsage() <= 2000) {
		nlapiLogExecution('AUDIT', 'API Limit Exceeded');
		var state = nlapiYieldScript();

		if (state.status == "FAILURE") {
			nlapiLogExecution('ERROR', 'Yield Script Failed', 'Reason : '
			        + state.reason + ' / Size : ' + state.size);
			return false;
		} else if (state.status == "RESUME") {
			nlapiLogExecution('AUDIT', 'Script Resumed');
		}
	}
}