/**
 * @author Jayesh
 */

function scheduled(type) {
	try {
		var context = nlapiGetContext();
		
		/*var filters = new Array();
		filters[0] = new nlobjSearchFilter('custrecord_monthly_project', null, 'is', 'T');
		var searchResult = searchRecord('customrecord_practice_revenue_empwise', null, filters, null);
		nlapiLogExecution('audit','len total:- ',searchResult.length);
		
		for (var i = 0; i < searchResult.length; i++) {
			var rcrd = nlapiLoadRecord('customrecord_practice_revenue_empwise',searchResult[i].getId());
			nlapiSubmitRecord(rcrd,true,true);
			yieldScript(context);
		}
		return;*/
		
		/*var filters = new Array();
		filters[0] = new nlobjSearchFilter('custrecord_monthly_project', null, 'is', 'T');
		var searchResult = searchRecord('customrecord_practice_revenue_empwise', null, filters, null);
		for (var i = 0; i < searchResult.length; i++) {
			nlapiDeleteRecord('customrecord_practice_revenue_empwise',searchResult[i].getId());
			yieldScript(context);
		}
		
		delete searchResult;
		
		return;*/

		var newYear = nlapiStringToDate('1/1/2016');
		var projectList = [];
		
		searchRecord('job', '1524').forEach(function(project) {
			
			var pro_id = project.getId();
			
			var filters_search_resource = new Array();
			filters_search_resource[0] = new nlobjSearchFilter('project', null, 'anyof', parseInt(pro_id));
			filters_search_resource[1] = new nlobjSearchFilter('isinactive', 'employee', 'is', 'F');
			
			var columns_resource = new Array();
			columns_resource[0] = new nlobjSearchColumn('startdate');
			columns_resource[1] = new nlobjSearchColumn('enddate');
			columns_resource[2] = new nlobjSearchColumn('company');
			columns_resource[3] = new nlobjSearchColumn('custentity_region','job');
			columns_resource[4] = new nlobjSearchColumn('custentity_practice','job');
			columns_resource[5] = new nlobjSearchColumn('customer');
			columns_resource[6] = new nlobjSearchColumn('resource');
			columns_resource[7] = new nlobjSearchColumn('department','employee');
			columns_resource[8] = new nlobjSearchColumn('startdate','job');
			columns_resource[9] = new nlobjSearchColumn('enddate','job');
			columns_resource[10] = new nlobjSearchColumn('custevent_monthly_rate');
			columns_resource[11] = new nlobjSearchColumn('percentoftime');
			
			var resource_srch = nlapiSearchRecord('resourceallocation', null, filters_search_resource, columns_resource); 
			if(_logValidation(resource_srch))
			{
				for(var index=0; index<resource_srch.length ; index++)
				{
					var startDate = nlapiStringToDate(resource_srch[index].getValue('startdate'));
					
					if (startDate < newYear) {
						startDate = newYear;
					}

					projectList.push({
					    proj_id : resource_srch[index].getValue('company'),
						proj_name : resource_srch[index].getText('company'),
						pro_region : resource_srch[index].getValue('custentity_region','job'),
						pro_practice : resource_srch[index].getText('custentity_practice','job'),
						pro_cust : resource_srch[index].getValue('customer'),
						resource : resource_srch[index].getValue('resource'),
						resource_prac : resource_srch[index].getText('department','employee'),
					    StartDate : nlapiDateToString(startDate, 'date'),
					    EndDate : resource_srch[index].getValue('enddate'),
						proj_actual_strt_date: resource_srch[index].getValue('startdate','job'),
						proj_actual_end_date: resource_srch[index].getValue('enddate','job'),
						allocation_strt_date: resource_srch[index].getValue('startdate'),
						allocation_end_date: resource_srch[index].getValue('enddate'),
						emp_bill_rate: resource_srch[index].getValue('custevent_monthly_rate'),
						prcnt_of_time: resource_srch[index].getValue('percentoftime'),
					});
				}
			}
						
		});

		//nlapiLogExecution('debug', 'project count', projectList.length);

		for (var i = 0; i < projectList.length; i++) {
			var project = projectList[i];
			var monthBreakUp = getMonthsBreakup(
			        nlapiStringToDate(project.StartDate),
			        nlapiStringToDate(project.EndDate));

			for (var j = 0; j < monthBreakUp.length; j++) {
				var months = monthBreakUp[j];
				try {
					var rec = nlapiCreateRecord(
					        'customrecord_practice_revenue_empwise', {
						        recordmode : 'dynamic'
					        });
					rec.setFieldValue('custrecord_project_name_revenue', project.proj_id);
					rec.setFieldValue('custrecord_strt_date_revenue',months.Start);
					//rec.setFieldValue('custrecord_region_revenue',project.pro_region);
					rec.setFieldValue('custrecord_practice_revenue',project.pro_practice);
					rec.setFieldValue('custrecord_cust_revenue',project.pro_cust);
					rec.setFieldValue('custrecord_resource_revenue',project.resource);
					rec.setFieldValue('custrecord_emp_practice_revenue',project.resource_prac);
					rec.setFieldValue('custrecord_monthly_project','T');
					rec.setFieldValue('custrecord_pro_strt_date',project.proj_actual_strt_date);
					rec.setFieldValue('custrecord_pro_end_date',project.proj_actual_end_date);
					rec.setFieldValue('custrecord_allocation_strt_date',project.allocation_strt_date);
					rec.setFieldValue('custrecord_allocation_end_date',project.allocation_end_date);
					rec.setFieldValue('custrecord_emp_bill_rate',project.emp_bill_rate);
					rec.setFieldValue('custrecord_prcnt_of_time_allocated',project.prcnt_of_time);
					
					var recId = nlapiSubmitRecord(rec, true, true);
					nlapiLogExecution('debug', 'record created', recId);
					yieldScript(context);
				} catch (err) {
					nlapiLogExecution('ERROR', 'Failed To Create Break-Up For '
					        + project.proj_id + " for " + months.Start, err);
				}
			}
			nlapiLogExecution('debug', 'pro id:- ', project.proj_id);
		}
		
	} catch (err) {
		nlapiLogExecution('ERROR', 'scheduled', err);
	}
}

function getMonthsBreakup(d_startDate, d_endDate) {
	var dateBreakUp = [];
	var new_start_date = new Date(d_startDate.getFullYear(), d_startDate
	        .getMonth(), 1);

	dateBreakUp.push({
	    Start : nlapiDateToString(new_start_date, 'date'),
	    End : ''
	});

	for (var i = 1;; i++) {
		var new_date = nlapiAddMonths(new_start_date, i);

		if (new_date > d_endDate) {
			break;
		}

		dateBreakUp.push({
		    Start : nlapiDateToString(new_date, 'date'),
		    End : ''
		});
	}

	return dateBreakUp;
}

function yieldScript(currentContext) {

	if (currentContext.getRemainingUsage() <= 1000) {
		nlapiLogExecution('AUDIT', 'API Limit Exceeded');
		var state = nlapiYieldScript();

		if (state.status == "FAILURE") {
			nlapiLogExecution('ERROR', 'Yield Script Failed', 'Reason : '
			        + state.reason + ' / Size : ' + state.size);
			return false;
		} else if (state.status == "RESUME") {
			nlapiLogExecution('AUDIT', 'Script Resumed');
		}
	}
}

function _logValidation(value) 
{
 if(value!='null' && value != null && value != null && value != '' && value != undefined && value != undefined && value != 'undefined' && value!= 'undefined'&& value!= 'NaN' && value != NaN) 
 {
  return true;
 }
 else 
 { 
  return false;
 }
}

function searchRecord(recordType, savedSearch, arrFilters, arrColumns,
        filterExpression)
{

	try {
		var search = null;

		// if a saved search is provided, load it and add the filters and
		// columns
		if (isNotEmpty(savedSearch)) {
			search = nlapiLoadSearch(recordType, savedSearch);

			if (isArrayNotEmpty(arrFilters)) {
				search.addFilters(arrFilters);
			}

			if (isArrayNotEmpty(arrColumns)) {
				search.addColumns(arrColumns);
			}

			if (isArrayNotEmpty(filterExpression)) {
				search.setFilterExpression(filterExpression);
			}
		}
		// create a new search
		else {
			search = nlapiCreateSearch(recordType, arrFilters, arrColumns);
		}

		// run search
		var resultSet = search.runSearch();

		// iterate through the search and get all data 1000 at a time
		var searchResultCount = 0;
		var resultSlice = null;
		var searchResult = [];

		do {
			resultSlice = resultSet.getResults(searchResultCount,
			        searchResultCount + 1000);

			if (resultSlice) {

				resultSlice.forEach(function(result) {

					searchResult.push(result);
					searchResultCount++;
				});
			}
		} while (isArrayNotEmpty(resultSlice) && resultSlice.length >= 1000);

		return searchResult;
	} catch (err) {
		nlapiLogExecution('ERROR', 'searchRecord', err);
		throw err;
	}
}

function isEmpty(value) {

	return value == null || value == "" || typeof (value) == undefined;
}

function isNotEmpty(value) {

	return !isEmpty(value);
}

function isArrayEmpty(argArray) {

	return !isArrayNotEmpty(argArray);
}

function isArrayNotEmpty(argArray) {

	return (isNotEmpty(argArray) && argArray.length > 0);
}
