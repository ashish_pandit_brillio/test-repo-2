/**
 * @author Shweta
 */
// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
/*
   	Script Name : SUT_PR_Validations.js
	Author      : Shweta Chopde
	Company:
	Date:
	Description:


	Script Modification Log:

	-- Date --			-- Modified By --				--Requested By--				-- Description --



Below is a summary of the process controls enforced by this script file.  The control logic is described
more fully, below, in the appropriate function headers and code blocks.


     SUITELET
		- suiteletFunction(request, response)


     SUB-FUNCTIONS
		- The following sub-functions are called by the above core functions in order to maintain code
            modularization:

               - NOT USED

*/
}
// END SCRIPT DESCRIPTION BLOCK  ====================================



// BEGIN GLOBAL VARIABLE BLOCK  =====================================
{
	//  Initialize any Global Variables, in particular, debugging variables...




}
// END GLOBAL VARIABLE BLOCK  =======================================





// BEGIN SUITELET ==================================================

function suiteletFunction(request, response)
{
	var results=new Array();
	var i_employeeID ;
	try
	{
	 if(request.getMethod()=='GET')
	 {
	 	var i_employee=request.getParameter('custscript_employee');
       nlapiLogExecution('DEBUG', ' suiteletFunction',' Employee -->' + i_employee);
	   
	    if (_logValidation(i_employee))
		{
			var o_employeeOBJ = nlapiLoadRecord('employee', i_employee)
			
			if (_logValidation(o_employeeOBJ)) 
			{
				i_employeeID = o_employeeOBJ.getFieldValue('entityid')
				nlapiLogExecution('DEBUG', 'fieldChanged_PR_validations', ' Employee ID-->' + i_employeeID);
				results.push(i_employeeID)
			}
		}
	 	
		
	 }
		
	}
    catch(exception)
	{
	  nlapiLogExecution('DEBUG', 'ERROR',' Exception Caught -->' + exception);	
	}//CATCH
	
	 response.write(i_employeeID);	
}

// END SUITELET ====================================================




// BEGIN OBJECT CALLED/INVOKING FUNCTION ===================================================
function _logValidation(value) 
{
 if(value != null && value.toString() != null && value != '' && value != undefined && value.toString() != undefined && value != 'undefined' && value.toString() != 'undefined'&& value.toString() != 'NaN' && value != NaN) 
 {
  return true;
 }
 else 
 { 
  return false;
 }
}

// END OBJECT CALLED/INVOKING FUNCTION =====================================================
