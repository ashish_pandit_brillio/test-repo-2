/**
 * @author Shweta
 */
// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
/*
   	Script Name : SCH_Salary_Upload_Delete_Non_Error_Records.js
	Author      : Shweta Chopde
	Date        : 1 Aug 2014
    Description : Delete the the Salary Records


	Script Modification Log:

	-- Date --			-- Modified By --				--Requested By--				-- Description --




Below is a summary of the process controls enforced by this script file.  The control logic is described
more fully, below, in the appropriate function headers and code blocks.


     SCHEDULED FUNCTION
		- scheduledFunction(type)


     SUB-FUNCTIONS
		- The following sub-functions are called by the above core functions in order to maintain code
            modularization:

               - NOT USED

*/
}
// END SCRIPT DESCRIPTION BLOCK  ====================================


// BEGIN SCHEDULED FUNCTION =============================================

function schedulerFunction(type)
{
   var i_employee_type ;
   var i_delete_record_status ;	
   try
   {
	var i_context = nlapiGetContext();  
	
	var i_CSV_File_ID =  i_context.getSetting('SCRIPT','custscript_file_id_delete');
	var i_recordID =  i_context.getSetting('SCRIPT','custscript_record_id_delete');
	var i_hourly_employee =  i_context.getSetting('SCRIPT','custscript_hourly_employee_delete');
	var i_monthly_employee =  i_context.getSetting('SCRIPT','custscript_salaried_employee_delete');
	
	nlapiLogExecution('DEBUG', 'schedulerFunction',' CSV File ID -->' + i_CSV_File_ID);	
	nlapiLogExecution('DEBUG', 'schedulerFunction',' Record ID -->' + i_recordID);	
	nlapiLogExecution('DEBUG', 'schedulerFunction',' Monthly Employee -->' + i_monthly_employee);
    nlapiLogExecution('DEBUG', 'schedulerFunction',' Hourly Employee -->' + i_hourly_employee);
	
	if(i_monthly_employee == 'T')
	{			
	  i_delete_record_status = delete_monthly_salary_upload(i_recordID,i_CSV_File_ID)	
	}//Monthly
	if(i_hourly_employee == 'T')
	{			
	  i_delete_record_status = delete_hourly_salary_upload(i_recordID,i_CSV_File_ID)
	}//Hourly	
	
	 if(i_delete_record_status == 'DELETED')
	 { 			
	/*
	var s_notes = 'File has been removed from the system.<br> You need to upload it again after removing errors from files.';
		if(_logValidation(i_recordID))
		{
		var o_recordOBJ = nlapiLoadRecord('customrecord_salary_upload_process',i_recordID);
		
		if(_logValidation(o_recordOBJ))
		{		
			o_recordOBJ.setFieldValue('custrecord_file_status',2)
			o_recordOBJ.setFieldValue('custrecord_notes',s_notes)
			o_recordOBJ.setFieldValue('custrecord_csv_file_id_no',i_CSV_File_ID)			
			
			var i_submitID = nlapiSubmitRecord(o_recordOBJ,true,true);
			nlapiLogExecution('DEBUG', 'schedulerFunction',' ------------ Submit ID ----------->' + i_submitID);
		
			if(_logValidation(i_submitID))
			{
				var i_submit_fileID = nlapiDeleteFile(i_CSV_File_ID);
				nlapiLogExecution('DEBUG', 'schedulerFunction',' ------------ File Submit ID ----------->' + i_submit_fileID);
			}			
		}//Record OBJ				
		}//Record ID	
*/	
	 }//DELETE	
   }//TRY
   catch(exception)
   {
	  nlapiLogExecution('DEBUG', 'ERROR',' Exception Caught -->' + exception);	
   }//CATCH
}

// END SCHEDULED FUNCTION ===============================================





// BEGIN FUNCTION ===================================================
/**
 * 
 * @param {Object} value
 * 
 * Description --> If the value is blank /null/undefined returns false else returns true
 */
function _logValidation(value) 
{
 if(value!='null' && value != null && value != null && value != '' && value != undefined && value != undefined && value != 'undefined' && value!= 'undefined'&& value!= 'NaN' && value != NaN) 
 {
  return true;
 }
 else 
 { 
  return false;
 }
}

function delete_monthly_salary_upload(i_recordID,i_CSV_File_ID)
{
	var i_delete_record_status ;
	if(_logValidation(i_recordID)&&_logValidation(i_CSV_File_ID))
	{
	var filter = new Array();
	filter[0] = new nlobjSearchFilter('custrecord_salary_upload_id_m', null, 'is', i_recordID);
	filter[1] = new nlobjSearchFilter('custrecord_csv_file_m_s_p_u', null, 'is', i_CSV_File_ID);
	filter[2] = new nlobjSearchFilter('custrecord_emp_type_m_s_p_u', null, 'is', 1);
	
	var columns = new Array();
	columns[0] = new nlobjSearchColumn('internalid');
		
	  var a_search_results = nlapiSearchRecord('customrecord_salary_upload_monthly_file', null, filter, columns);
	
	    if (a_search_results != null && a_search_results != '' && a_search_results != undefined) 
		{
			for (var i = 0; i < a_search_results.length; i++) 
			{
				var i_recordID = a_search_results[i].getValue('internalid');
				nlapiLogExecution('DEBUG', 'schedulerFunction', ' Salary Upload Monthly File Record ID -->' + i_recordID);
				
				var i_submitID = nlapiDeleteRecord('customrecord_salary_upload_monthly_file',i_recordID)
				nlapiLogExecution('DEBUG', 'schedulerFunction', ' --------------------- Salary Upload Monthly File Delete ID ------------------->' + i_submitID);
				
				if(_logValidation(i_submitID))
				{
					i_delete_record_status = 'DELETED'
				}			
			}
		}		
	}//Check	
	return i_delete_record_status;
}//Delete Monthly Salary Upload



function delete_hourly_salary_upload(i_recordID,i_CSV_File_ID)
{
	var i_delete_record_status;
	if(_logValidation(i_recordID)&&_logValidation(i_CSV_File_ID))
	{
	var filter = new Array();
	filter[0] = new nlobjSearchFilter('custrecord_salary_upload_id_h', null, 'is', i_recordID);
	filter[1] = new nlobjSearchFilter('custrecord_csv_file_process_s_u', null, 'is', i_CSV_File_ID);
	filter[2] = new nlobjSearchFilter('custrecord_employee_type_s_u', null, 'is', 2);
	
	var columns = new Array();
	columns[0] = new nlobjSearchColumn('internalid');
		
	var a_search_results = nlapiSearchRecord('customrecord_salary_upload_hourly_file', null, filter, columns);
	   
	    if (a_search_results != null && a_search_results != '' && a_search_results != undefined) 
		{
			for (var i = 0; i < a_search_results.length; i++) 
			{
				var i_recordID = a_search_results[i].getValue('internalid');
				nlapiLogExecution('DEBUG', 'schedulerFunction', ' Salary Upload Hourly File Record ID -->' + i_recordID);
				
				var i_submitID = nlapiDeleteRecord('customrecord_salary_upload_hourly_file',i_recordID)
				nlapiLogExecution('DEBUG', 'schedulerFunction', ' --------------------- Salary Upload Hourly File Delete ID ------------------->' + i_submitID);
				
				if(_logValidation(i_submitID))
				{
					i_delete_record_status = 'DELETED'
				}
				
			}
		}		
		
	}//Check	
	return i_delete_record_status;
}//Delete Hourly Salary Upload


// END FUNCTION =====================================================
