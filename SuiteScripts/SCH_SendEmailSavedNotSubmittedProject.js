// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
/*
   	Script SCH_SendEmailSavedNotSubmittedProject.js
	Author: Ashish Pandit	
	Date:17-April-2018	
    Description: Scheduled script to send Email to PM for Project confirmation for Saved and Not Submitted Projects  


	Script Modification Log:

	-- Date --			-- Modified By --				--Requested By--				-- Description --
	
	
	Below is a summary of the process controls enforced by this script file.  The control logic is described
	more fully, below, in the appropriate function headers and code blocks.

   
     SUB-FUNCTIONS
		- The following sub-functions are called by the above core functions in order to maintain code
            modularization:

		scheduled_SendEmailForApproval()



*/
}
// END SCRIPT DESCRIPTION BLOCK  ====================================


function scheduled_SendEmailSavedProject(type) {
	var o_freeze_date = nlapiLoadRecord('customrecord_fp_rev_rec_freeze_date',1);
		if(o_freeze_date)
		{
			var s_freeze_date = o_freeze_date.getFieldValue('custrecord_freeze_date');
			var i_freeze_day = nlapiStringToDate(s_freeze_date).getDate();
			var i_freeze_month = nlapiStringToDate(s_freeze_date).getMonth();
			
			var d_today_date = new Date();
			var i_today_date = d_today_date.getDate();
			var i_today_month = d_today_date.getMonth();
			var i_today_year = d_today_date.getFullYear();
			
			if(parseInt(i_freeze_month) != parseInt(i_today_month))
				i_freeze_day = 25;
			
			i_today_month = parseInt(i_today_month) + parseInt(1);
			
			nlapiLogExecution('audit','i_freeze_day:- '+i_freeze_day,'i_today_date:- '+i_today_date);
			if(i_today_date < 20)
			{
				if(i_today_date == 5 || i_today_date == 10 || i_today_date == 15)
				{
					
				}
				else
				{
					nlapiLogExecution('audit','freeze date trigger date yet to come');
					return;
				}		
			}
			
			if(i_today_date >= i_freeze_day)
			{
				nlapiLogExecution('audit','freeze date trigger crossed');
				return;
			}
		}
	//Getting Current month 
	var d_today_date = new Date();
	//nlapiLogExecution('DEBUG', 'd_today_date', d_today_date);
	var i_month = d_today_date.getMonth();
	//nlapiLogExecution('DEBUG', 'i_month', i_month);
	var a_projectArray = new Array();
	
	// Search to get all Approved Projects for FP Rev Rec Others 
	
	var customrecord_fp_rev_rec_others_parentSearch = nlapiSearchRecord("customrecord_fp_rev_rec_others_parent",null,
			[
			  	  
			   ["custrecord_revenue_other_approval_status","anyof","2"],
			    "AND", 
			   ["custrecord_fp_rev_rec_others_projec.custentity_fp_rev_rec_type","anyof","2","4"]   //Added Straight Line type on 10/26/2018
			   
			], 
			[
			   new nlobjSearchColumn("id",null,null).setSort(false), 
			   new nlobjSearchColumn("custrecord_fp_rev_rec_others_projec",null,null)
			]
			);
	
		  if(_logValidation(customrecord_fp_rev_rec_others_parentSearch)) 
		  {
				for(var i=0; i<customrecord_fp_rev_rec_others_parentSearch.length;i++)
				{
				var i_project = customrecord_fp_rev_rec_others_parentSearch[i].getValue('custrecord_fp_rev_rec_others_projec');
				a_projectArray.push(i_project);
				}			
		  }
	
		var jobSearch = nlapiSearchRecord("job",null,
			[
			   ["enddate","onorafter","startofthismonth"], 
			   "AND", 
			   ["custentity_fp_rev_rec_type","anyof","4"], 
			   "AND", 
			   ["internalid","noneof",a_projectArray],
               "AND", 
			   ["subsidiary", "noneof", "9","8"]
			  
			], 
			[
			   new nlobjSearchColumn("internalid",null,null).setSort(false), 
			   new nlobjSearchColumn("entityid",null,null),
			   new nlobjSearchColumn("custentity_projectmanager",null,null),
			   new nlobjSearchColumn("custentity_deliverymanager",null,null),
			   new nlobjSearchColumn("custentity_clientpartner",null,null),
			   new nlobjSearchColumn("custentity_project_currency",null,null),
			   new nlobjSearchColumn("custentity_discount_project",null,null)
			]
			);
    if(_logValidation(jobSearch)) 
    	{
	    	
			nlapiLogExecution('DEBUG','jobSearch.length=',jobSearch.length);
			var i_emailCount = 0;
    		for(var i=0; i<jobSearch.length;i++)  
			{
				var i_project = jobSearch[i].getValue('internalid');
				
				//var i_project = jobSearch[i].getValue('entityid');
				var s_projectName = jobSearch[i].getValue('entityid');
				nlapiLogExecution('DEBUG','i_project==',i_project);
				if(_logValidation(i_project))
				{
				flag_counter_arr=0;
				flag_counter=0;
				//var recObj = nlapiLoadRecord('job',i_project);
				
				var b_discProject = jobSearch[i].getValue('custentity_discount_project');
				nlapiLogExecution('Debug', 'b_discProject ='+b_discProject,s_projectName);
				// To skip Discount projects
				if(b_discProject == 'T')
				{
					nlapiLogExecution('Debug','in break');	
					continue;
				}
				
				var i_projManager = jobSearch[i].getValue('custentity_projectmanager');
				var s_projectManager = jobSearch[i].getText('custentity_projectmanager');
				var i_deliveryManager = jobSearch[i].getValue('custentity_deliverymanager');
				var i_clientPartner = jobSearch[i].getValue('custentity_clientpartner');
				if(_logValidation(i_projManager))
				var i_projManagerEmail = nlapiLookupField('employee',i_projManager,'email');
				if(_logValidation(i_deliveryManager))
				var i_deliveryManagerEmail = nlapiLookupField('employee',i_deliveryManager,'email');
				if(_logValidation(i_clientPartner))
				var i_clientPartnerEmail = nlapiLookupField('employee',i_clientPartner,'email');
				var s_proj_currency = jobSearch[i].getValue('custentity_project_currency');
				var d_today = new Date();	
				var getCurrentMonth = d_today.getMonth() + 1;
				var getCurrentYear = d_today.getFullYear();
				var s_month_name = getMonthName(nlapiDateToString(d_today));
				var s_emailSub = 'Monthly revenue confirmation for the Project '+ s_projectName +  ' Forecast Setup / Approval Reminder';
				s_projectManager = s_projectManager.split('-');
				nlapiLogExecution('DEBUG','s_projectManager '+s_projectManager[1]);
				var s_emailBody = 'Dear '+s_projectManager[1] +'<br> Please create an effort / revenue plan for your project '+s_projectName+' and get it approved by the practice head. <br>Confirm the recognition plan for the month  '+s_month_name +'  '+ getCurrentYear +' before the freeze date '+ i_freeze_day +'  '+ s_month_name +'. <br> Should you fail to confirm before freeze date, system will not recognize the revenue for this month and same is true for future months unless you confirm the plan at least once.<br>';
				s_emailBody +=  '<br>Note: If there is any help required in setting this up or get it approved, please work with prashanth.s@brillio.com and sapan.shah@brillio.com immediately, before the freeze date.<br><br>Thanks & Regards,<br>Team IS'; 
				var recordarray = new Array();
				recordarray['entity'] = 94862; // Employee Ashish for testing 
				
				var a_participating_mail_id = new Array();
					a_participating_mail_id.push(i_deliveryManagerEmail);
					a_participating_mail_id.push(i_clientPartnerEmail);
					a_participating_mail_id.push('billing@brillio.com');
					a_participating_mail_id.push('team.fpa@brillio.com');
				
				//Sending Email to PM  
				nlapiSendEmail(442,i_projManagerEmail,s_emailSub,s_emailBody,a_participating_mail_id,['information.systems@brillio.com','deepak.srinivas@brillio.com'],null,null,false,false,null);
				
				//nlapiSendEmail(94862,'ashish.p@inspirria.com',s_emailSub,s_emailBody,null,null,recordarray,null,false,false,null);
				i_emailCount++;
				var context = nlapiGetContext();
				if(context.getRemainingUsage() < 200)
					{
						var state = nlapiYieldScript();
					}
				}
						
			}
			nlapiLogExecution('Debug','i_emailCount = ',i_emailCount);
		}
			
	}
	
	function getMonthName(currentDate) {
	currentDate = nlapiStringToDate(currentDate)
	var monthNames = [ "JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JUL", "AUG",
	        "SEP", "OCT", "NOV", "DEC" ];
	return monthNames[currentDate.getMonth()];
	}
function _logValidation(value) 
{
 if(value!='null' && value != null && value != null && value != '' && value != undefined && value != undefined && value != 'undefined' && value!= 'undefined'&& value!= 'NaN' && value != NaN) 
 {
  return true;
 }
 else 
 { 
  return false;
 }
 }