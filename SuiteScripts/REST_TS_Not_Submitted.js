function main(datain) {
	var response = {
	    Status : false,
	    Message : ''
	};

	try {
		var startDate = '10/1/2015', endDate = '10/31/2015', employeeList = [ 2742 ];
		response.Message = getNotSubmitted(startDate, endDate, employeeList);
		response.Status = true;
	} catch (err) {
		nlapiLogExecution('ERROR', 'main', err);
		response.Message = err;
	}

	return JSON.stringify(response);
}

function getNotSubmitted(startDate, endDate, employeeList, getDayWise) {
	try {
		nlapiLogExecution('debug', 'rest', 'start');
		nlapiLogExecution('debug', 'start date', startDate);
		nlapiLogExecution('debug', 'endDate', endDate);
		nlapiLogExecution('debug', 'employeeList', employeeList);
		nlapiLogExecution('debug', 'employee count', employeeList.length);

		var s_startDate = startDate, d_startDate = nlapiStringToDate(startDate);
		var s_endDate = endDate, d_endDate = nlapiStringToDate(endDate);

		// array of days common for all employees
		var noOfWorkingDays = getWorkingDaysArray(d_startDate, d_endDate);
		var allocationWiseArray = getEmployeeAllocationDetails(d_startDate,
		        d_endDate, employeeList, noOfWorkingDays);

		nlapiLogExecution('debug', 'allocationWiseArray', JSON
		        .stringify(allocationWiseArray));

		var holidayArray = getHolidayArray(s_startDate, s_endDate);
		nlapiLogExecution('debug', 'holidayArray', JSON.stringify(holidayArray));

		markHolidaysAsSubmitted(d_startDate, holidayArray, allocationWiseArray);
		nlapiLogExecution('debug', 'allocationWiseArray', JSON
		        .stringify(allocationWiseArray));

		var timesheetWiseArray = checkSubmittedTimesheets(d_startDate,
		        d_endDate, employeeList, allocationWiseArray);
		nlapiLogExecution('debug', 'timesheetWiseArray', JSON
		        .stringify(timesheetWiseArray));

		if (getDayWise) {
			return timesheetWiseArray;
		} else {
			var responseArray = {};

			for ( var emp in timesheetWiseArray) {
				responseArray[emp] = {};

				for ( var project in timesheetWiseArray[emp]) {
					var dayArray = timesheetWiseArray[emp][project].DaysArray;
					var cntNotSubmitted = 0;

					for (var i = 0; i < dayArray.length; i++) {

						if (dayArray[i] == 2) {
							cntNotSubmitted += 1;
						}
					}

					responseArray[emp][project] = cntNotSubmitted;
				}
			}

			nlapiLogExecution('debug', 'rest', 'end');
			return responseArray;
		}
	} catch (err) {
		nlapiLogExecution('ERROR', 'getNotSubmitted', err);
	}
}

function getSubmittedTimesheets(startDate, endDate, employeeList) {
	try {
		return searchRecord('timesheet', null,
		        [
		               // new nlobjSearchFilter('timesheetdate', null, 'within', //Modified filter if the month start date in the middle 
		                //        startDate, endDate),
						new nlobjSearchFilter('date', 'timebill', 'within',
								startDate, endDate),
		                new nlobjSearchFilter('employee', null, 'anyof',
		                        employeeList) ], [
		                new nlobjSearchColumn('employee'),
		                new nlobjSearchColumn('startdate'),
		                new nlobjSearchColumn('enddate') ]);
	} catch (err) {
		nlapiLogExecution('ERROR', 'getSubmittedTimesheets', err);
		throw err;
	}
}

function getWorkingDaysArray(startDate, endDate) {
	try {
		// 1. create a array containing all days from start to end
		var mainArray = [];

		// 2. loop from start to end date and mark saturday sunday as submitted
		for (var i = 0;; i++) {
			var currentDate = nlapiAddDays(startDate, i);

			// if the date exceeds the end date, exit the loop
			if (currentDate > endDate) {
				break;
			}

			// check if saturday / sunday
			var day = currentDate.getDay();
			if (day == 0 || day == 6) {
				mainArray[i] = 3;
			} else {
				mainArray[i] = 1;
			}
		}

		// nlapiLogExecution('debug', 'getWorkingDaysArray', JSON
		// .stringify(mainArray));
		return mainArray;
	} catch (err) {
		nlapiLogExecution('ERROR', 'getWorkingDaysArray', err);
		throw err;
	}
}

function getEmployeeAllocationDetails(d_start_date, d_end_date, employeeList,
        workingDaysArray)
{
	try {
		var mainArray = {};
		var search_allocation = searchRecord('resourceallocation', null,

		        [
		                new nlobjSearchFilter('resource', null, 'anyof',
		                        employeeList),
		                new nlobjSearchFilter('startdate', null, 'notafter',
		                        d_end_date),
		                new nlobjSearchFilter('enddate', null, 'notbefore',
		                        d_start_date) ], [
		                new nlobjSearchColumn('resource'),
		                new nlobjSearchColumn('startdate'),
		                new nlobjSearchColumn('enddate'),
		                new nlobjSearchColumn('customer'),
		                new nlobjSearchColumn('subsidiary', 'employee'),
		                new nlobjSearchColumn('custentityproject_holiday',
		                        'job'), new nlobjSearchColumn('project') ]);

		nlapiLogExecution('debug', 'no. of allocations',
		        search_allocation.length);

		for (var i = 0; i < search_allocation.length; i++) {
			var temp_holiday_list = [];

			for (var m = 0; m < workingDaysArray.length; m++) {
				temp_holiday_list.push(workingDaysArray[m]);
			}

			//nlapiLogExecution('debug', 'workingDaysArray in loop', JSON
			       // .stringify(workingDaysArray))

			var employeeId = search_allocation[i].getValue('resource');
			var projectId = search_allocation[i].getValue('project');
			var allocation_start_date = nlapiStringToDate(search_allocation[i]
			        .getValue('startdate'));
			var allocation_end_date = nlapiStringToDate(search_allocation[i]
			        .getValue('enddate'));
			var projectHoliday = search_allocation[i].getValue(
			        'custentityproject_holiday', 'job');
			var customer = search_allocation[i].getValue('customer');
			var subsidiary = search_allocation[i].getValue('subsidiary',
			        'employee');

			var holidayType = "";

			if (projectHoliday == 1) {
				holidayType = "Brillio";
			} else {
				holidayType = customer;
			}

			var start_date_point = allocation_start_date > d_start_date ? allocation_start_date
			        : d_start_date;

			for (var d = 0;; d++) {
				var d_current_date = nlapiAddDays(start_date_point, d);

				if (d_current_date > allocation_end_date
				        || d_current_date > d_end_date) {
					break;
				}

				var n = getDatediffIndays(d_current_date, d_start_date) - 1;

				if (temp_holiday_list[n] == 1) {
					temp_holiday_list[n] = 2;
				}
			}

			// nlapiLogExecution('debug', 'temp list', JSON
			// .stringify(temp_holiday_list));

			// add the holiday list, employee and project to a main array
			var empTemp = mainArray['E' + employeeId];

			if (!empTemp) {
				mainArray['E' + employeeId] = {};
				mainArray['E' + employeeId]['P' + projectId] = {
				    HolidayType : holidayType,
				    DaysArray : [],
				    Customer : customer,
				    Subsidiary : subsidiary
				};

				temp_holiday_list.forEach(function(a) {
					mainArray['E' + employeeId]['P' + projectId].DaysArray
					        .push(a);
				});
			} else {
				var projectTemp = mainArray['E' + employeeId]['P' + projectId];

				if (!projectTemp) {
					mainArray['E' + employeeId]['P' + projectId] = {
					    HolidayType : holidayType,
					    DaysArray : [],
					    Customer : customer,
					    Subsidiary : subsidiary
					};

					temp_holiday_list.forEach(function(a) {
						mainArray['E' + employeeId]['P' + projectId].DaysArray
						        .push(a);
					});
				} else {
					// todo : multiple allocation to same project
					// add the allocation to the same array
				}
			}

			// delete temp_holiday_list;
		}

		return mainArray;
	} catch (err) {
		nlapiLogExecution('ERROR', 'getEmployeeAllocationDetails', err);
		throw err;
	}
}

function markHolidaysAsSubmitted(startdate, holidayArray, allocationWiseArray) {
	try {
		// nlapiLogExecution('debug', 'Holiday Start');

		for ( var emp in allocationWiseArray) {

			for ( var project in allocationWiseArray[emp]) {
				var holidayType = allocationWiseArray[emp][project].HolidayType;
				var subsidiary = allocationWiseArray[emp][project].Subsidiary;
				var daysArray = allocationWiseArray[emp][project].DaysArray;

				// nlapiLogExecution('debug', 'emp pro holidaytype sub array',
				// emp
				// + " " + project + " " + holidayType + " " + subsidiary
				// + " " + daysArray);

				for (var i = 0; i < daysArray.length; i++) {
					var currentDate = nlapiDateToString(nlapiAddDays(startdate,
					        i), 'date');

					if (holidayArray[currentDate]) {
						// nlapiLogExecution('debug', 'holiday on date',
						// currentDate);
						var subHolidayList = holidayArray[currentDate][subsidiary];

						if (subHolidayList) {
							// nlapiLogExecution('debug',
							// 'subsidiary holiday on date list exists',
							// currentDate);

							for (var j = 0; j < subHolidayList.length; j++) {

								if (subHolidayList[j] == holidayType) {

									if (allocationWiseArray[emp][project].DaysArray[i] == 2) {
										allocationWiseArray[emp][project].DaysArray[i] = 3;
										// nlapiLogExecution('debug',
										// 'holiday marked', currentDate);
									}
								}
							}
						}
					}
				}
			}
		}

		// nlapiLogExecution('debug', 'allocationWiseArray', JSON
		// .stringify(allocationWiseArray));
		nlapiLogExecution('debug', 'Holiday End');
	} catch (err) {
		nlapiLogExecution('ERROR', 'markHolidaysAsSubmitted', err);
		throw err;
	}
}

function checkSubmittedTimesheets(d_start_date, d_end_date, employeeList,
        mainArray)
{
	try {
		var submittedTimesheetSearchResult = getSubmittedTimesheets(
		        d_start_date, d_end_date, employeeList);

		if (submittedTimesheetSearchResult) {
			nlapiLogExecution('debug', 'no. of timesheets',
			        submittedTimesheetSearchResult.length);

			for (var i = 0; i < submittedTimesheetSearchResult.length; i++) {
				var employeeId = submittedTimesheetSearchResult[i]
				        .getValue('employee'), ts_startdate = submittedTimesheetSearchResult[i]
				        .getValue('startdate'), ts_enddate = submittedTimesheetSearchResult[i]
				        .getValue('enddate');

				// for the employee, mark timesheets as submitted for all the
				// projects if allocation exists
				var n = getDatediffIndays(nlapiStringToDate(ts_startdate),
				        d_start_date) - 1;
				var startIndex = n >= 0 ? n : 0;
				// nlapiLogExecution('debug', 'start index', startIndex);

				var eIndex = 'E' + employeeId;

				// nlapiLogExecution('debug', 'emp', JSON
				// .stringify(mainArray[eIndex]));

				for ( var project in mainArray[eIndex]) {
					// nlapiLogExecution('debug', 'project', JSON
					// .stringify(project));

					for (var j = startIndex; j < n + 7; j++) {

						if (mainArray[eIndex][project].DaysArray[j] == 2) {
							mainArray[eIndex][project].DaysArray[j] = 3;
						}
					}
				}
			}
		}

		return mainArray;
	} catch (err) {
		nlapiLogExecution('ERROR', 'checkSubmittedTimesheets', err);
		throw err;
	}
}

function getDatediffIndays(startDate, endDate) {
	var one_day = 1000 * 60 * 60 * 24;
	var daysDiff = Math.round((startDate - endDate) / one_day);
	return (daysDiff + 1);
}

function getHolidayArray(start_date, end_date) {

	// Brillio Holiday
	var search_company_holiday = searchRecord('customrecord_holiday', null,
	        [ new nlobjSearchFilter('custrecord_date', null, 'within',
	                start_date, end_date) ], [
	                new nlobjSearchColumn('custrecord_date'),
	                new nlobjSearchColumn('custrecordsubsidiary') ]);

	var brillio_holidays = {};

	if (search_company_holiday) {

		for (var i = 0; i < search_company_holiday.length; i++) {
			var tempA = brillio_holidays[search_company_holiday[i]
			        .getValue('custrecord_date')];

			if (!tempA) {
				brillio_holidays[search_company_holiday[i]
				        .getValue('custrecord_date')] = {};

				brillio_holidays[search_company_holiday[i]
				        .getValue('custrecord_date')][search_company_holiday[i]
				        .getValue('custrecordsubsidiary')] = [ 'Brillio' ];
			} else {
				var tempB = brillio_holidays[search_company_holiday[i]
				        .getValue('custrecord_date')][search_company_holiday[i]
				        .getValue('custrecordsubsidiary')];

				if (!tempB) {
					brillio_holidays[search_company_holiday[i]
					        .getValue('custrecord_date')][search_company_holiday[i]
					        .getValue('custrecordsubsidiary')] = [ 'Brillio' ];
				} else {
					brillio_holidays[search_company_holiday[i]
					        .getValue('custrecord_date')][search_company_holiday[i]
					        .getValue('custrecordsubsidiary')].push('Brillio');
				}
			}
		}
	}

	nlapiLogExecution('debug', 'Brillio Holiday Done');

	// customer holiday
	var search_customer_holiday = searchRecord('customrecordcustomerholiday',
	        null, [ new nlobjSearchFilter('custrecordholidaydate', null,
	                'within', start_date, end_date) ], [
	                new nlobjSearchColumn('custrecordholidaydate'),
	                new nlobjSearchColumn('custrecordcustomersubsidiary'),
	                new nlobjSearchColumn('custrecord13') ]);

	if (search_customer_holiday) {

		for (var i = 0; i < search_customer_holiday.length; i++) {
			var tempA = brillio_holidays[search_customer_holiday[i]
			        .getValue('custrecordholidaydate')];

			if (!tempA) {
				brillio_holidays[search_customer_holiday[i]
				        .getValue('custrecordholidaydate')] = {};

				brillio_holidays[search_customer_holiday[i]
				        .getValue('custrecordholidaydate')][search_customer_holiday[i]
				        .getValue('custrecordcustomersubsidiary')] = [ search_customer_holiday[i]
				        .getValue('custrecord13') ];
			} else {
				var tempB = brillio_holidays[search_customer_holiday[i]
				        .getValue('custrecordholidaydate')][search_customer_holiday[i]
				        .getValue('custrecordcustomersubsidiary')];

				if (!tempB) {
					brillio_holidays[search_customer_holiday[i]
					        .getValue('custrecordholidaydate')][search_customer_holiday[i]
					        .getValue('custrecordcustomersubsidiary')] = [ search_customer_holiday[i]
					        .getValue('custrecord13') ];
				} else {
					brillio_holidays[search_customer_holiday[i]
					        .getValue('custrecordholidaydate')][search_customer_holiday[i]
					        .getValue('custrecordcustomersubsidiary')]
					        .push(search_customer_holiday[i]
					                .getValue('custrecord13'));
				}
			}
		}
	}

	return brillio_holidays;
}
