/**
 * @author Shweta
 */
// BEGIN SCRIPT DESCRIPTION BLOCK ==================================
{
	/*
	 * Script Name: SUT_Consolidated_PR_PO_Report.js Author : Shweta Chopde Date :
	 * 17 April 2014 Description:
	 * 
	 * 
	 * Script Modification Log:
	 *  -- Date -- -- Modified By -- --Requested By-- -- Description --
	 * 
	 * 
	 * 
	 * Below is a summary of the process controls enforced by this script file.
	 * The control logic is described more fully, below, in the appropriate
	 * function headers and code blocks.
	 * 
	 * 
	 * SUITELET - suiteletFunction(request, response)
	 * 
	 * 
	 * SUB-FUNCTIONS - The following sub-functions are called by the above core
	 * functions in order to maintain code modularization:
	 *  - NOT USED
	 * 
	 */
}
// END SCRIPT DESCRIPTION BLOCK ====================================

// BEGIN GLOBAL VARIABLE BLOCK =====================================
{
	// Initialize any Global Variables, in particular, debugging variables...

}
// END GLOBAL VARIABLE BLOCK =======================================

// BEGIN SUITELET ==================================================

var a_PR_ID_Array = new Array();
var a_PR_Item_Array = new Array()

function suiteletFunction(request, response) {
	var i_employee_ID;
	var i_PR_Date;
	var i_PR_ID;
	var i_customer;
	var i_PR_quantity;
	var i_PR_item;
	var i_QA_status;
	var i_PR_status;
	var i_procurement_team;
	var i_practice_head;
	var i_item_category;
	var i_IT_team;
	var i_PR_vendor;
	var i_PR_description;
	var i_PR_practice;
	var i_PR_vertical;
	var i_PR_currency;
	var i_PR_subsidiary;
	var i_PR_SNO;
	var i_preferred_vendor;

	var i_vendor_PO;

	var i_rate_1;

	var i_payment_1;

	var i_rate_2;

	var i_payment_2;

	var i_rate_3;

	var i_payment_3;

	var i_vendor_1;

	var i_vendor_2;

	var i_vendor_3;

	var i_serial_no;

	var i_PR_remaining_quant;

	var i_is_Consolidated;

	var i_delivery_location;

	var i_PR_No;

	try {
		if (request.getMethod() == 'GET') {

			var f_form = nlapiCreateForm("CONSOLIDATED PR - PO REPORT");

			f_form.setScript('customscript_cli_consolidated_pr_po_vali')

			var i_create_PO = f_form.addField('custpage_selected_created_po',
			        'text', 'Created PO');
			i_create_PO.setDisplayType('hidden')

			var i_array_PO = f_form.addField('custpage_data_array', 'text',
			        'PO Array');
			i_array_PO.setDisplayType('hidden')

			var i_array_vendor = f_form.addField('custpage_vendor_array',
			        'text', 'Vendor Array');
			i_array_vendor.setDisplayType('hidden')

			var PR_list = f_form.addSubList('custpage_pr_sublist', 'list',
			        ' PR List', 'custpage_pr_tab');

			PR_list.addField('custpage_create_po_checkbox', 'checkbox',
			        'Create PO');

			PR_list.addField('custpage_serial_no', 'text', 'S.NO');

			PR_list.addField('custpage_selected_pos', 'text', 'Selected PO#')
			        .setDisplayType('hidden');

			PR_list.addField('custpage_pritem_poref', 'text', 'PR Ref.');// .setDisplayType('hidden');

			PR_list.addField('custpage_pr_id', 'text', 'PR#').setDisplayType(
			        'inline');

			PR_list.addField('custpage_vendor', 'select', 'Vendor', 'vendor')
			        .setDisplayType('inline');

			PR_list.addField('custpage_delivery_location', 'select',
			        'Delivery Location', 'location').setDisplayType('inline');

			PR_list.addField('custpage_item_id', 'select', 'Item', 'item')
			        .setDisplayType('inline');

			PR_list.addField('custpage_quantity', 'text', 'Quantity')
			        .setDisplayType('entry');

			PR_list.addField('custpage_quantity_remaining', 'text',
			        'Quantity Remaining').setDisplayType('inline');

			PR_list.addField('custpage_item_category', 'select',
			        'Item Category', 'customlist_itemcategorylist')
			        .setDisplayType('inline');

			PR_list.addField('custpage_practice_head', 'select',
			        'Practice Head', 'employee').setDisplayType('inline');

			PR_list.addField('custpage_it_team', 'select', 'IT Team',
			        'employee').setDisplayType('inline');

			PR_list.addField('custpage_procurement_team', 'select',
			        'Procurement Team', 'employee').setDisplayType('inline');

			PR_list.addField('custpage_customer', 'text', 'Customer')
			        .setDisplayType('inline');

			PR_list.addField('custpage_pr_date', 'date', 'PR Date');

			PR_list.addField('custpage_pr_status', 'select', 'PR Status',
			        'customlist_prstatus').setDisplayType('inline');

			PR_list.addField('custpage_subsidiary', 'text', 'Subsidiary',
			        'subsidiary').setDisplayType('hidden');

			PR_list.addField('custpage_vertical', 'select', 'Vertical',
			        'classification').setDisplayType('hidden');

			PR_list.addField('custpage_practice', 'date', 'Practice',
			        'department').setDisplayType('hidden');

			PR_list.addField('custpage_currency', 'select', 'Currency',
			        'currency').setDisplayType('hidden');

			PR_list.addField('custpage_description', 'textarea', 'Desc')
			        .setDisplayType('hidden');

			PR_list.addField('custpage_pr_s_no', 'text', 'S NO')
			        .setDisplayType('hidden');

			PR_list.addField('custpage_pr_nos', 'text', 'PR NO')
			        .setDisplayType('hidden');

			PR_list.addField('custpage_pritem_id', 'text', 'PR Item Rec ID')
			        .setDisplayType('hidden');

			// ================ Search Purchase Request Records
			// ====================

			var a_search_results = nlapiSearchRecord(
			        'customrecord__prpurchaserequest',
			        'customsearch_pr_consolidated_search', null, null);

			if (_logValidation(a_search_results)) {
				for (var i = 0; i < a_search_results.length; i++) {
					var a_search_transaction_result = a_search_results[i];
					var columns = a_search_transaction_result.getAllColumns();
					var columnLen = columns.length;
					var pr_item_rec_id = a_search_transaction_result
					        .getValue(columns[19]);
					var pr_item_POREF = a_search_transaction_result
					        .getValue(columns[20]);
					nlapiLogExecution('DEBUG', 'suiteletFunction',
					        ' pr_item_POREF -->' + pr_item_POREF);
					nlapiLogExecution('DEBUG', 'suiteletFunction',
					        ' pr_item_rec_id -->' + pr_item_rec_id);
					for (var t = 0; t < columnLen; t++) {
						var column = columns[t];
						var label = column.getLabel();
						var value = a_search_transaction_result
						        .getValue(column)
						var text = a_search_transaction_result.getText(column)

						if (label == 'PR ID') {
							i_PR_ID = value;
						}
						if (label == 'PR No') {
							i_PR_No = value;
						}

						if (label == 'PR Date') {
							i_PR_Date = value;
						}
						if (label == 'Customer') {
							i_customer = value;
						}
						if (label == 'IT Team') {
							i_IT_team = value;
						}
						if (label == 'Item Category') {
							i_item_category = value;
						}
						if (label == 'Practice Head') {
							i_practice_head = value;
						}
						if (label == 'Procurement Team') {
							i_procurement_team = value;
						}
						if (label == 'PR Status') {
							i_PR_status = value;
						}
						if (label == 'PR Quantity') {
							i_PR_quantity = value;
						}
						if (label == 'PR Item') {
							i_PR_item = value;
						}
						if (label == 'QA Status') {
							i_QA_status = value;
						}
						if (label == 'PR Vendor') {
							i_PR_vendor = value;
						}
						if (label == 'PR Subsidiary') {
							i_PR_subsidiary = value;
						}
						if (label == 'PR Currency') {
							i_PR_currency = value;
						}
						if (label == 'PR Vertical') {
							i_PR_vertical = value;
						}
						if (label == 'PR Practice') {
							i_PR_practice = value;
						}
						if (label == 'PR Description') {
							i_PR_description = value;
						}
						if (label == 'PR SNO') {
							i_PR_SNO = value;
						}
						if (label == 'PR Remain Quant') {
							i_PR_remaining_quant = value;
						}
						if (label == 'Is Consolidated') {
							i_is_Consolidated = value;
						}
						if (label == 'Delivery Location') {
							i_delivery_location = value;
						}

					}

					if (!_logValidation(i_PR_No)) {
						i_PR_No = ''
					}
					if (!_logValidation(i_delivery_location)) {
						i_delivery_location = ''
					}
					if (!_logValidation(i_is_Consolidated)) {
						i_is_Consolidated = ''
					}
					if (!_logValidation(i_PR_remaining_quant)) {
						i_PR_remaining_quant = ''
					}
					if (!_logValidation(i_PR_SNO)) {
						i_PR_SNO = ''
					}
					if (!_logValidation(i_PR_description)) {
						i_PR_description = ''
					}
					if (!_logValidation(i_PR_practice)) {
						i_PR_practice = ''
					}
					if (!_logValidation(i_PR_vertical)) {
						i_PR_vertical = ''
					}
					if (!_logValidation(i_PR_currency)) {
						i_PR_currency = ''
					}
					if (!_logValidation(i_PR_subsidiary)) {
						i_PR_subsidiary = ''
					}
					if (!_logValidation(i_PR_vendor)) {
						i_PR_vendor = ''
					}
					if (!_logValidation(i_QA_status)) {
						i_QA_status = ''
					}
					if (!_logValidation(i_PR_item)) {
						i_PR_item = ''
					}
					if (!_logValidation(i_PR_quantity)) {
						i_PR_quantity = ''
					}
					if (!_logValidation(i_PR_status)) {
						i_PR_status = ''
					}
					if (!_logValidation(i_procurement_team)) {
						i_procurement_team = ''
					}
					if (!_logValidation(i_practice_head)) {
						i_practice_head = ''
					}
					if (!_logValidation(i_item_category)) {
						i_item_category = ''
					}
					if (!_logValidation(i_IT_team)) {
						i_IT_team = ''
					}
					if (!_logValidation(i_customer)) {
						i_customer = ''
					}

					nlapiLogExecution('DEBUG', 'suiteletFunction',
					        ' i_delivery_location -->' + i_delivery_location)
					nlapiLogExecution('DEBUG', 'suiteletFunction',
					        ' i_PR_remaining_quant -->' + i_PR_remaining_quant)
					nlapiLogExecution('DEBUG', 'suiteletFunction',
					        ' i_PR_vendor -->' + i_PR_vendor)
					nlapiLogExecution('DEBUG', 'suiteletFunction',
					        ' i_PR_ID -->' + i_PR_ID)
					nlapiLogExecution('DEBUG', 'suiteletFunction',
					        ' i_PR_Date -->' + i_PR_Date)
					nlapiLogExecution('DEBUG', 'suiteletFunction',
					        ' i_customer -->' + i_customer)
					nlapiLogExecution('DEBUG', 'suiteletFunction',
					        ' i_QA_status -->' + i_QA_status)
					nlapiLogExecution('DEBUG', 'suiteletFunction',
					        ' i_PR_item -->' + i_PR_item)
					nlapiLogExecution('DEBUG', 'suiteletFunction',
					        ' i_PR_quantity -->' + i_PR_quantity)
					nlapiLogExecution('DEBUG', 'suiteletFunction',
					        ' i_PR_status -->' + i_PR_status)
					nlapiLogExecution('DEBUG', 'suiteletFunction',
					        ' i_procurement_team -->' + i_procurement_team)
					nlapiLogExecution('DEBUG', 'suiteletFunction',
					        ' i_practice_head -->' + i_practice_head)
					nlapiLogExecution('DEBUG', 'suiteletFunction',
					        ' i_item_category -->' + i_item_category)
					nlapiLogExecution('DEBUG', 'suiteletFunction',
					        ' i_IT_team -->' + i_IT_team)
					nlapiLogExecution('DEBUG', 'suiteletFunction',
					        ' i_PR_subsidiary -->' + i_PR_subsidiary)
					nlapiLogExecution('DEBUG', 'suiteletFunction',
					        ' i_PR_currency -->' + i_PR_currency)
					nlapiLogExecution('DEBUG', 'suiteletFunction',
					        ' i_PR_vertical -->' + i_PR_vertical)
					nlapiLogExecution('DEBUG', 'suiteletFunction',
					        ' i_PR_practice -->' + i_PR_practice)
					nlapiLogExecution('DEBUG', 'suiteletFunction',
					        ' i_PR_description -->' + i_PR_description)

					PR_list.setLineItemValue('custpage_serial_no', (i + 1),
					        parseInt(i + 1).toFixed(0))
					PR_list
					        .setLineItemValue('custpage_pr_id', (i + 1),
					                i_PR_ID)
					PR_list.setLineItemValue('custpage_pr_date', (i + 1),
					        i_PR_Date)
					PR_list.setLineItemValue('custpage_customer', (i + 1),
					        i_customer)
					PR_list.setLineItemValue('custpage_item_id', (i + 1),
					        i_PR_item)
					// PR_list.setLineItemValue('custpage_quantity',(i+1),i_PR_quantity)
					PR_list.setLineItemValue('custpage_pr_status', (i + 1),
					        i_PR_status)
					PR_list.setLineItemValue('custpage_procurement_team',
					        (i + 1), i_procurement_team)
					PR_list.setLineItemValue('custpage_practice_head', (i + 1),
					        i_practice_head)
					PR_list.setLineItemValue('custpage_item_category', (i + 1),
					        i_item_category)
					PR_list.setLineItemValue('custpage_pr_nos', (i + 1),
					        i_PR_No)
					PR_list.setLineItemValue('custpage_pritem_id', (i + 1),
					        pr_item_rec_id)// added by swati
					PR_list.setLineItemValue('custpage_pritem_poref', (i + 1),
					        pr_item_POREF)// added by swati

					if (i_is_Consolidated == 'F' || i_is_Consolidated == null
					        || i_is_Consolidated == undefined
					        || i_is_Consolidated == '') {
						PR_list.setLineItemValue('custpage_quantity_remaining',
						        (i + 1), i_PR_quantity)
					} else {
						PR_list.setLineItemValue('custpage_quantity_remaining',
						        (i + 1), i_PR_remaining_quant)
					}
					PR_list.setLineItemValue('custpage_it_team', (i + 1),
					        i_IT_team)
					PR_list.setLineItemValue('custpage_vendor', (i + 1),
					        i_PR_vendor)
					PR_list.setLineItemValue('custpage_subsidiary', (i + 1),
					        i_PR_subsidiary)
					PR_list.setLineItemValue('custpage_currency', (i + 1),
					        i_PR_currency)
					PR_list.setLineItemValue('custpage_vertical', (i + 1),
					        i_PR_vertical)
					// PR_list.setLineItemValue('custpage_practice',(i+1),i_PR_practice)
					PR_list.setLineItemValue('custpage_description', (i + 1),
					        i_PR_description)
					PR_list.setLineItemValue('custpage_pr_s_no', (i + 1),
					        i_PR_SNO)
					PR_list.setLineItemValue('custpage_delivery_location',
					        (i + 1), i_delivery_location)

				}

			}// Search Results

			f_form.addSubmitButton();
			response.writePage(f_form);

		}// GET
		else if (request.getMethod() == 'POST') {
			var a_split_PO_array = new Array()
			var a_PO_values = new Array()
			var a_vendor_values = new Array()
			var i_create_po = new Array()
			var i_create_po_1 = new Array()
			var i_PR_ID;
			var i_vendor;
			var i_quantity;
			var i_S_NO;
			var i_preferred_vendor;

			var i_vendor_PO;

			var i_rate_1;

			var i_payment_1;

			var i_rate_2;

			var i_payment_2;

			var i_rate_3;

			var i_payment_3;

			var i_vendor_1;

			var i_vendor_2;

			var i_vendor_3;

			var i_serial_no;

			var i_record_ID_PR_item;

			var i_quan_rem;

			var igg;

			var i_PR_no;

			var a_PI_array = new Array();
			var i_pnt = 0;

			i_create_po = request.getParameter('custpage_selected_created_po');

			for (var po = 0; po < i_create_po.length; po++) {
				a_PO_values = i_create_po.split(',')
				break;
			}
			nlapiLogExecution('DEBUG', 'suiteletFunction', ' a_PO_values-->'
			        + a_PO_values)
			nlapiLogExecution('DEBUG', 'suiteletFunction',
			        ' a_PO_values length-->' + a_PO_values.length)

			var a_PO_array_values = new Array()
			var i_data_po = new Array()
			i_data_po = request.getParameter('custpage_data_array');

			for (var dt = 0; dt < i_data_po.length; dt++) {
				a_PO_array_values = i_data_po.split('*,*')
				break;
			}
			nlapiLogExecution('DEBUG', 'suiteletFunction', ' PO Array -->'
			        + a_PO_array_values)
			nlapiLogExecution('DEBUG', 'suiteletFunction',
			        ' PO Array  Length-->' + a_PO_array_values.length)

			var a_vendor_array_values = new Array()
			var i_data_vendor = new Array()
			i_data_vendor = request.getParameter('custpage_vendor_array');

			for (var vt = 0; vt < i_data_vendor.length; vt++) {
				a_vendor_values = i_data_vendor.split(',')
				break;
			}
			nlapiLogExecution('DEBUG', 'suiteletFunction', ' Vendor Array-->'
			        + a_vendor_values)
			nlapiLogExecution('DEBUG', 'suiteletFunction',
			        ' Vendor Array Length-->' + a_vendor_values.length)

			a_vendor_values = removearrayduplicate(a_vendor_values)
			nlapiLogExecution('DEBUG', 'suiteletFunction',
			        ' Vendor Array Duplicates -->' + a_vendor_values)

			// ================= CREATE PURCHASE ORDER
			// ===============================
			var o_PO_OBJ = nlapiCreateRecord('purchaseorder')
			nlapiLogExecution('DEBUG', 'suiteletFunction', ' PO OBJ-->'
			        + o_PO_OBJ)

			var i_sr_no = 1

			var d_todays_date = get_todays_date()

			for (var dp = 0; dp < a_PO_array_values.length; dp++) {
				a_split_PO_array = a_PO_array_values[dp].split('##')

				i_PR_ID = a_split_PO_array[0]

				i_item = a_split_PO_array[1]

				i_quantity = a_split_PO_array[2]

				i_vendor = a_split_PO_array[3]

				i_subsidiary = a_split_PO_array[4]

				i_vertical = a_split_PO_array[5]

				i_practice = a_split_PO_array[6]

				i_currency = a_split_PO_array[7]

				i_description = a_split_PO_array[8]

				i_S_NO = a_split_PO_array[9]

				i_quan_rem = a_split_PO_array[10]

				i_PR_no = a_split_PO_array[11]

				i_PR_ITem_ID = a_split_PO_array[12]// added by swati

				a_PR_ID_Array.push(i_PR_ID)
				a_PR_ID_Array = removearrayduplicate(a_PR_ID_Array)
				nlapiLogExecution('DEBUG', 'suiteletFunction',
				        ' a_PR_ID_Array ' + a_PR_ID_Array)

				nlapiLogExecution('DEBUG', 'suiteletFunction', ' i_quan_rem-->'
				        + i_quan_rem)
				nlapiLogExecution('DEBUG', 'suiteletFunction', ' i_vendor-->'
				        + i_vendor)
				nlapiLogExecution('DEBUG', 'suiteletFunction', ' i_quantity-->'
				        + i_quantity)

				nlapiLogExecution('DEBUG', 'suiteletFunction',
				        ' i_subsidiary-->' + i_subsidiary)
				nlapiLogExecution('DEBUG', 'suiteletFunction', ' i_vertical-->'
				        + i_vertical)
				nlapiLogExecution('DEBUG', 'suiteletFunction', ' i_practice-->'
				        + i_practice)
				nlapiLogExecution('DEBUG', 'suiteletFunction', ' i_currency-->'
				        + i_currency)
				nlapiLogExecution('DEBUG', 'suiteletFunction',
				        ' i_description-->' + i_description)

				nlapiLogExecution('DEBUG', 'suiteletFunction', ' i_vendor '
				        + i_vendor)

				nlapiLogExecution('DEBUG', 'suiteletFunction',
				        ' POOOOOOOOOOOOOOOOOOO ')

				o_PO_OBJ.setFieldValue('custbody_prrefno', a_PR_ID_Array)
				o_PO_OBJ.setFieldValue('trandate', d_todays_date)
				// o_PO_OBJ.setFieldValue('subsidiary',3)
				o_PO_OBJ.setFieldValue('entity', i_vendor)
				o_PO_OBJ.setFieldValue('currency', i_currency)
				o_PO_OBJ.setFieldValue('class', i_vertical)
				o_PO_OBJ.setFieldValue('department', i_practice)

				o_PO_OBJ.selectNewLineItem('item')
				o_PO_OBJ.setCurrentLineItemValue('item', 'custcol_srnumber',
				        (parseInt(i_sr_no++)).toFixed(0));
				o_PO_OBJ.setCurrentLineItemValue('item', 'item', i_item);
				o_PO_OBJ.setCurrentLineItemValue('item', 'description',
				        i_description);
				o_PO_OBJ
				        .setCurrentLineItemValue('item', 'quantity', i_quantity);
				o_PO_OBJ.setCurrentLineItemValue('item', 'class', i_vertical);
				o_PO_OBJ.setCurrentLineItemValue('item', 'department',
				        i_practice);
				o_PO_OBJ.setCurrentLineItemText('item', 'customer', i_customer);
				o_PO_OBJ
				        .setCurrentLineItemValue('item', 'vendorname', i_vendor);
				// o_PO_OBJ.setCurrentLineItemValue('item',
				// 'custcol_po_pr_item', i_PR_ID);
				o_PO_OBJ.setCurrentLineItemValue('item', 'custcol_po_pr_item',
				        i_PR_ITem_ID);// added by swati

				var i_QA_recordID = search_quotation_analysis_recordID(i_PR_ID,
				        i_S_NO, i_item)

				if (_logValidation(i_QA_recordID)) {
					var o_QA_OBJ = nlapiLoadRecord(
					        'customrecord_quotationanalysis', i_QA_recordID)

					if (_logValidation(o_QA_OBJ)) {
						i_preferred_vendor = o_QA_OBJ
						        .getFieldValue('custrecord_preferred_vendor')

						i_vendor_PO = o_QA_OBJ
						        .getFieldValue('custrecord_vendorforpo')

						i_rate_1 = o_QA_OBJ.getFieldValue('custrecord_rate1')

						i_payment_1 = o_QA_OBJ
						        .getFieldValue('custrecord_paymentterms')

						i_rate_2 = o_QA_OBJ.getFieldValue('custrecord_rate2')

						i_payment_2 = o_QA_OBJ
						        .getFieldValue('custrecord_paymenttermsv2')

						i_rate_3 = o_QA_OBJ.getFieldValue('custrecord_rate3')

						i_payment_3 = o_QA_OBJ
						        .getFieldValue('custrecord_paymenttermsv3')

						i_vendor_1 = o_QA_OBJ
						        .getFieldValue('custrecord_vendor1name')

						i_vendor_2 = o_QA_OBJ
						        .getFieldValue('custrecord_vendor2name')

						i_vendor_3 = o_QA_OBJ
						        .getFieldValue('custrecord_vendor3name')

						nlapiLogExecution('DEBUG', 'suiteletFunction',
						        ' i_preferred_vendor ' + i_preferred_vendor)
						nlapiLogExecution('DEBUG', 'suiteletFunction',
						        ' i_vendor_PO ' + i_vendor_PO)
						nlapiLogExecution('DEBUG', 'suiteletFunction',
						        ' i_rate_1 ' + i_rate_1)
						nlapiLogExecution('DEBUG', 'suiteletFunction',
						        ' i_payment_1 ' + i_payment_1)
						nlapiLogExecution('DEBUG', 'suiteletFunction',
						        ' i_rate_2 ' + i_rate_2)
						nlapiLogExecution('DEBUG', 'suiteletFunction',
						        ' i_payment_2 ' + i_payment_2)
						nlapiLogExecution('DEBUG', 'suiteletFunction',
						        ' i_rate_3 ' + i_rate_3)
						nlapiLogExecution('DEBUG', 'suiteletFunction',
						        ' i_payment_3 ' + i_payment_3)
						nlapiLogExecution('DEBUG', 'suiteletFunction',
						        ' i_vendor_1 ' + i_vendor_1)
						nlapiLogExecution('DEBUG', 'suiteletFunction',
						        ' i_vendor_2 ' + i_vendor_2)
						nlapiLogExecution('DEBUG', 'suiteletFunction',
						        ' i_vendor_3 ' + i_vendor_3)

						if ((i_vendor_PO == i_vendor_1)
						        && (((i_vendor_1 != null
						                && i_vendor_1 != undefined && i_vendor_1 != '') && ((i_vendor_2 != null
						                && i_vendor_2 != undefined && i_vendor_2 != '') || (i_vendor_3 != null
						                && i_vendor_3 != undefined && i_vendor_3 != ''))))) {
							o_PO_OBJ.setCurrentLineItemValue('item', 'rate',
							        i_rate_1);
							o_PO_OBJ.setFieldValue('terms', i_payment_1)
							nlapiLogExecution('DEBUG', 'suiteletFunction', ' 1')

						} else if ((i_vendor_PO == i_vendor_2)
						        && (((i_vendor_1 != null
						                && i_vendor_1 != undefined && i_vendor_1 != '') && ((i_vendor_2 != null
						                && i_vendor_2 != undefined && i_vendor_2 != '') || (i_vendor_3 != null
						                && i_vendor_3 != undefined && i_vendor_3 != ''))))) {
							o_PO_OBJ.setCurrentLineItemValue('item', 'rate',
							        i_rate_2);
							o_PO_OBJ.setFieldValue('terms', i_payment_2)
							nlapiLogExecution('DEBUG', 'suiteletFunction', ' 2')
						} else if ((i_vendor_PO == i_vendor_3)
						        && ((i_vendor_1 != null
						                && i_vendor_1 != undefined && i_vendor_1 != '') && ((i_vendor_2 != null
						                && i_vendor_2 != undefined && i_vendor_2 != '') || (i_vendor_3 != null
						                && i_vendor_3 != undefined && i_vendor_3 != '')))) {
							o_PO_OBJ.setCurrentLineItemValue('item', 'rate',
							        i_rate_3);
							o_PO_OBJ.setFieldValue('terms', i_payment_3)
							nlapiLogExecution('DEBUG', 'suiteletFunction', ' 3')
						} else if (((i_vendor_PO == i_vendor_1))
						        && ((i_vendor_1 != null
						                && i_vendor_1 != undefined && i_vendor_1 != '') && ((i_vendor_2 == null
						                || i_vendor_2 == undefined || i_vendor_2 == '') && (i_vendor_3 == null
						                || i_vendor_3 == undefined || i_vendor_3 == '')))) {
							o_PO_OBJ.setCurrentLineItemValue('item', 'rate',
							        i_rate_1);
							o_PO_OBJ.setFieldValue('terms', i_payment_1)
							nlapiLogExecution('DEBUG', 'suiteletFunction', ' 4')
						}

					}// QA OBJ

				}

				i_record_ID_PR_item = search_PR_Item_recordID(i_PR_ID, i_S_NO,
				        i_item)

				a_PI_array[i_pnt++] = i_record_ID_PR_item + '^^^^^'
				        + i_quan_rem;

				/*
				 * if(_logValidation(i_PR_ID)) { var o_PR_OBJ =
				 * nlapiLoadRecord('customrecord_pritem',i_record_ID_PR_item)
				 * 
				 * if(_logValidation(o_PR_OBJ)) {
				 * o_PR_OBJ.setFieldValue('custrecord_remaining_quantity_consolidat',i_quan_rem)
				 * o_PR_OBJ.setFieldValue('custrecord_updated_by_consolidated','T')
				 * 
				 * var i_submitID_PR_item =
				 * nlapiSubmitRecord(o_PR_OBJ,true,true)
				 * nlapiLogExecution('DEBUG', 'afterSubmit_Item_Data_Sourcing','
				 * ***************** Submit ID PR Item***********-->' +
				 * i_submitID_PR_item);
				 * 
				 * a_PR_Item_Array.push(i_submitID_PR_item)
				 * nlapiLogExecution('DEBUG', 'afterSubmit_Item_Data_Sourcing','
				 * a_PR_Item_Array oooooooooooooooooooooo-->' +
				 * a_PR_Item_Array);
				 * 
				 * a_PR_Item_Array = removearrayduplicate(a_PR_Item_Array)
				 * nlapiLogExecution('DEBUG', 'afterSubmit_Item_Data_Sourcing','
				 * a_PR_Item_Array iiiiiiiiiiiiiiiiiiii -->' + a_PR_Item_Array);
				 * 
				 * }//PR OBJ
				 *  }
				 */

				o_PO_OBJ.commitLineItem('item');

			}// PO Array

			var i_submitID_PO = new Array()
			i_submitID_PO = nlapiSubmitRecord(o_PO_OBJ, true, true)
			nlapiLogExecution('DEBUG', 'afterSubmit_Item_Data_Sourcing',
			        ' ***************** Submit ID PO ***********-->'
			                + i_submitID_PO);

			var a_split_array_PI = new Array();
			var i_created_PO = new Array();
			for (var pt = 0; pt < a_PI_array.length; pt++) {
				a_split_array_PI = a_PI_array[pt].split('^^^^^')

				var i_PI_ID = a_split_array_PI[0]

				var i_PI_Quantity = a_split_array_PI[1]

				var o_PR_OBJ = nlapiLoadRecord('customrecord_pritem', i_PI_ID)
				if (_logValidation(o_PR_OBJ)) {
					nlapiLogExecution('DEBUG',
					        'afterSubmit_Item_Data_Sourcing',
					        ' **o_PR_OBJ****-->' + o_PR_OBJ);

					i_created_PO = o_PR_OBJ
					        .getFieldValues('custrecord_created_po_id')
					// nlapiLogExecution('DEBUG',
					// 'afterSubmit_Item_Data_Sourcing',' **i_created_PO****-->'
					// + i_created_PO.toString());
					// nlapiLogExecution('DEBUG',
					// 'afterSubmit_Item_Data_Sourcing',' **i_created_PO
					// length****-->' + i_created_PO.length);
					var igg = new Array()
					if (_logValidation(i_created_PO)) {
						i_create_po_1.push(i_submitID_PO)
						igg = i_created_PO.concat(i_create_po_1)
					} else {
						i_create_po_1.push(i_submitID_PO)
						igg = i_create_po_1
					}

					igg = removearrayduplicate(igg)
					nlapiLogExecution('DEBUG',
					        'afterSubmit_Item_Data_Sourcing',
					        ' **igg cccccccccccccc****-->' + igg);
					o_PR_OBJ.setFieldValue('custrecord_created_po_id', igg)
					o_PR_OBJ.setFieldValue(
					        'custrecord_remaining_quantity_consolidat',
					        i_PI_Quantity)

					// Added by Nitish - 2/23/2016
					o_PR_OBJ.setFieldValue('custrecord_prastatus', '23');

					var quoteSearch = nlapiSearchRecord(
					        'customrecord_quotationanalysis', null, [
					                new nlobjSearchFilter('isinactive', null,
					                        'is', 'T'),
					                new nlobjSearchFilter(
					                        'custrecord_qa_pr_item', null,
					                        'anyof', o_PR_OBJ.getId()) ]);
					if (quoteSearch) {
						nlapiSubmitField('customrecord_quotationanalysis',
						        quoteSearch[0].getId(),
						        'custrecord_approvalstatusforquotation', '8');
						nlapiLogExecution('debug', 'QA status updated');
					}

					// End

					o_PR_OBJ
					        .setFieldValue('custrecord_quantity', i_PI_Quantity)
					var id = nlapiSubmitRecord(o_PR_OBJ, true, true)
					nlapiLogExecution('DEBUG',
					        'afterSubmit_Item_Data_Sourcing',
					        ' ***************** ID ***********-->' + id);

				}
			}

			nlapiSetRedirectURL('RECORD', 'purchaseorder', i_submitID_PO, true,
			        null)
		}// POST

	} catch (exception) {
		nlapiLogExecution('DEBUG', 'ERROR', ' Exception Caught -->' + exception);
	}// CATCH
}

// END SUITELET ====================================================

// BEGIN OBJECT CALLED/INVOKING FUNCTION
// ===================================================

function _logValidation(value) {
	if (value != null && value.toString() != null && value != ''
	        && value != undefined && value.toString() != undefined
	        && value != 'undefined' && value.toString() != 'undefined'
	        && value.toString() != 'NaN' && value != NaN) {
		return true;
	} else {
		return false;
	}
}
function removearrayduplicate(array) {
	var newArray = new Array();
	label: for (var i = 0; i < array.length; i++) {
		for (var j = 0; j < array.length; j++) {
			if (newArray[j] == array[i])
				continue label;
		}
		newArray[newArray.length] = array[i];
	}
	return newArray;
}

function get_todays_date() {
	var today;
	// ============================= Todays Date
	// ==========================================

	var date1 = new Date();
	nlapiLogExecution('DEBUG', 'schedulerFunction', 'Date ==' + date1);

	var offsetIST = 5.5;

	// To convert to UTC datetime by subtracting the current Timezone offset
	var utcdate = new Date(date1.getTime()
	        + (date1.getTimezoneOffset() * 60000));
	nlapiLogExecution('DEBUG', 'schedulerFunction', 'UTC Date' + utcdate);

	// Then cinver the UTS date to the required time zone offset like back to
	// 5.5 for IST
	var istdate = new Date(utcdate.getTime() - ((-offsetIST * 60) * 60000));
	nlapiLogExecution('DEBUG', 'schedulerFunction', 'IST Date ==' + istdate);

	var day = istdate.getDate();
	nlapiLogExecution('DEBUG', 'schedulerFunction', 'Day ==' + day);

	var month = istdate.getMonth() + 1;
	nlapiLogExecution('DEBUG', 'schedulerFunction', 'Month ==' + month);

	var year = istdate.getFullYear();
	nlapiLogExecution('DEBUG', 'schedulerFunction', 'Year ==' + year);

	var date_format = checkDateFormat();
	nlapiLogExecution('DEBUG', 'schedulerFunction', 'Date Format =='
	        + date_format);

	if (date_format == 'YYYY-MM-DD') {
		today = year + '-' + month + '-' + day;
	}
	if (date_format == 'DD/MM/YYYY') {
		today = day + '/' + month + '/' + year;
	}
	if (date_format == 'MM/DD/YYYY') {
		today = month + '/' + day + '/' + year;
	}
	return today;
}
function checkDateFormat() {
	var context = nlapiGetContext();

	var dateFormatPref = context.getPreference('dateformat');

	return dateFormatPref;
}

function search_PR_Item_recordID(i_recordID, i_SR_No_QA, i_item_QA) {
	nlapiLogExecution('DEBUG', 'search_PR_Item_recordID', 'i_recordID =='
	        + i_recordID);
	nlapiLogExecution('DEBUG', 'search_PR_Item_recordID', 'i_SR_No_QA =='
	        + i_SR_No_QA);
	nlapiLogExecution('DEBUG', 'search_PR_Item_recordID', 'i_item_QA =='
	        + i_item_QA);

	var i_internal_id;

	var filter = new Array();
	filter[0] = new nlobjSearchFilter('custrecord_purchaserequest', null, 'is',
	        i_recordID);
	filter[1] = new nlobjSearchFilter('custrecord_prlineitemno', null, 'is',
	        parseInt(i_SR_No_QA));
	filter[2] = new nlobjSearchFilter('custrecord_prmatgrpcategory', null,
	        'is', i_item_QA);

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('internalid');
	columns[1] = new nlobjSearchColumn('custrecord_purchaserequest');
	columns[2] = new nlobjSearchColumn('custrecord_prlineitemno');
	columns[3] = new nlobjSearchColumn('custrecord_prmatgrpcategory');

	var a_seq_searchresults = nlapiSearchRecord('customrecord_pritem', null,
	        filter, columns);

	if (a_seq_searchresults != null && a_seq_searchresults != ''
	        && a_seq_searchresults != undefined) {
		nlapiLogExecution('DEBUG', 'search_PR_Item_recordID',
		        'a_seq_searchresults ==' + a_seq_searchresults.length);

		for (var ty = 0; ty < a_seq_searchresults.length; ty++) {
			i_internal_id = a_seq_searchresults[ty].getValue('internalid');
			nlapiLogExecution('DEBUG', 'search_PR_Item_recordID',
			        ' Internal ID -->' + i_internal_id);

			var i_PR = a_seq_searchresults[ty]
			        .getValue('custrecord_purchaserequest');
			nlapiLogExecution('DEBUG', 'search_PR_Item_recordID',
			        ' i_PR ID -->' + i_PR);

			var i_line_no = a_seq_searchresults[ty]
			        .getValue('custrecord_prlineitemno');
			nlapiLogExecution('DEBUG', 'search_PR_Item_recordID',
			        ' i_line_no -->' + i_line_no);

			var i_item = a_seq_searchresults[ty]
			        .getValue('custrecord_prmatgrpcategory');
			nlapiLogExecution('DEBUG', 'search_PR_Item_recordID', ' i_item-->'
			        + i_item);

			if ((i_PR == i_recordID) && (i_line_no == i_SR_No_QA)
			        && (i_item == i_item_QA)) {
				i_internal_id = i_internal_id
				break
			}

		}// LOOP

	}

	return i_internal_id;

}

function search_quotation_analysis_recordID(i_recordID, i_SR_No_QA, i_item_QA) {
	var i_internal_id;

	var filter = new Array();
	filter[0] = new nlobjSearchFilter('custrecord_prquote', null, 'is',
	        i_recordID);
	filter[1] = new nlobjSearchFilter('custrecord_srno', null, 'is', i_SR_No_QA);
	filter[2] = new nlobjSearchFilter('custrecord_item', null, 'is', i_item_QA);

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('internalid');
	columns[1] = new nlobjSearchColumn('custrecord_item');
	columns[2] = new nlobjSearchColumn('custrecord_srno');
	columns[3] = new nlobjSearchColumn('custrecord_prquote');

	var a_seq_searchresults = nlapiSearchRecord(
	        'customrecord_quotationanalysis', null, filter, columns);

	if (a_seq_searchresults != null && a_seq_searchresults != ''
	        && a_seq_searchresults != undefined) {
		for (var ty = 0; ty < a_seq_searchresults.length; ty++) {
			i_internal_id = a_seq_searchresults[ty].getValue('internalid');
			nlapiLogExecution('DEBUG', 'search_quotation_analysis_recordID',
			        ' Internal ID -->' + i_internal_id);

			var i_PR = a_seq_searchresults[ty].getValue('custrecord_prquote');
			nlapiLogExecution('DEBUG', 'search_quotation_analysis_recordID',
			        ' i_PR ID -->' + i_PR);

			var i_line_no = a_seq_searchresults[ty].getValue('custrecord_srno');
			nlapiLogExecution('DEBUG', 'search_quotation_analysis_recordID',
			        ' i_line_no -->' + i_line_no);

			var i_item = a_seq_searchresults[ty].getValue('custrecord_item');
			nlapiLogExecution('DEBUG', 'search_quotation_analysis_recordID',
			        ' i_item-->' + i_item);

			if ((i_PR == i_recordID) && (i_line_no == i_SR_No_QA)
			        && (i_item == i_item_QA)) {
				i_internal_id = i_internal_id
				break
			}

		}// LOOP
	}

	return i_internal_id;

}

// END OBJECT CALLED/INVOKING FUNCTION
// =====================================================
