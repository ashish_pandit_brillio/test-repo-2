/**
 * @author Jayesh
 */
 /**
 * @author Sai
 */
function suiteletFunction_FP_RevRec_report(request, response)
{
	try {
		var method = request.getMethod();

		if (method == 'GET') {
			suiteletFunction_FP_RevRec_Dashboard(request);
		}else {
			postFormAppoved(request);
		}
	} catch (err) {
		throw  err;
		nlapiLogExecution('Error', 'Rev Rec Page ERROR',err);
	}
}
function suiteletFunction_FP_RevRec_Dashboard(request)
{
	var a_project_list_m = new Array();
	try
	{
		var o_context = nlapiGetContext();
		var i_user_logegdIn_id = o_context.getUser();
		var a_project_list = new Array();
		var a_practice_list=new Array();
		var date_proj=new Date();
		var a_project_search_results = '';
		nlapiLogExecution('DEBUG','USER',i_user_logegdIn_id);
		if(i_user_logegdIn_id==7905 || i_user_logegdIn_id ==62082 || i_user_logegdIn_id ==97260)
		{
			i_user_logegdIn_id=71222;
		}
      if(i_user_logegdIn_id==3165)
		{
			i_user_logegdIn_id=7905;
		}
		
      nlapiLogExecution('DEBUG','USER',i_user_logegdIn_id);
		// design form which will be displayed to user
		var pract_fil=new Array();
		pract_fil[0]=new nlobjSearchFilter('custrecord_practicehead',null,'anyof',i_user_logegdIn_id);
		pract_fil[1]=new nlobjSearchFilter('isinactive',null,'is','F');
		var pract_col=new nlobjSearchColumn('internalid');
		var pract_search=nlapiSearchRecord('department',null,pract_fil,pract_col);
		if(pract_search)
		{
			for(var i_prac=0;i_prac<pract_search.length;i_prac++)
			{
				a_practice_list.push(pract_search[i_prac].getValue('internalid'));
			}
		}
		var o_form_obj = nlapiCreateForm("Plan Vs Effort Trend");
		//mile starts
		var a_pro_li_m= new Array();
		var a_get_logged_in_user_exsiting_revenue_cap_m = '';
		if(_logValidation(a_practice_list)){
			var a_revenue_cap_filter_m = [[['custrecord_revenue_share_project.custentity_projectmanager', 'anyof', i_user_logegdIn_id], 'or',
								['custrecord_revenue_share_project.custentity_deliverymanager', 'anyof', i_user_logegdIn_id], 'or',
								['custrecord_revenue_share_project.custentity_clientpartner', 'anyof', i_user_logegdIn_id], 'or',
								['custrecord_revenue_share_project.custentity_practice','anyof',a_practice_list],'or',
								['custrecord_revenue_share_cust.custentity_clientpartner', 'anyof', i_user_logegdIn_id]],
								'and',
								['custrecord_revenue_share_approval_status','anyof',3],'and',
								['isinactive','is','F']];
			}
		else
		{
			var a_revenue_cap_filter_m = [[['custrecord_revenue_share_project.custentity_projectmanager', 'anyof', i_user_logegdIn_id], 'or',
							['custrecord_revenue_share_project.custentity_deliverymanager', 'anyof', i_user_logegdIn_id], 'or',
							['custrecord_revenue_share_project.custentity_clientpartner', 'anyof', i_user_logegdIn_id], 'or',
							['custrecord_revenue_share_cust.custentity_clientpartner', 'anyof', i_user_logegdIn_id]],
							'and',
							['custrecord_revenue_share_approval_status','anyof',3],'and',
							['isinactive','is','F']];
		}
		var a_columns_existing_cap_srch_m = new Array();
		a_columns_existing_cap_srch_m[0] = new nlobjSearchColumn('custrecord_revenue_share_project');
		a_columns_existing_cap_srch_m[1] = new nlobjSearchColumn('custrecord_revenue_share_total');
		a_columns_existing_cap_srch_m[2] = new nlobjSearchColumn('created').setSort(true);
		a_columns_existing_cap_srch_m[3] = new nlobjSearchColumn('custrecord_revenue_share_approval_status');//(parseInt(i_user_logegdIn_id) == parseInt(41571) ||
		if (parseInt(i_user_logegdIn_id) == parseInt(7905)|| parseInt(i_user_logegdIn_id) == parseInt(35819) || parseInt(i_user_logegdIn_id) == parseInt(41571) || parseInt(i_user_logegdIn_id) == parseInt(72055) || parseInt(i_user_logegdIn_id) == parseInt(30484)|| parseInt(i_user_logegdIn_id) == parseInt(3165))
		{
			var a_project_cap_filter_m = [['custrecord_revenue_share_approval_status','anyof',3],'and',
					['custrecord_revenue_share_project.enddate','onorafter','today'],'and',
					['isinactive','is','F']];
													
			a_get_logged_in_user_exsiting_revenue_cap_m = searchRecord('customrecord_revenue_share', null, a_project_cap_filter_m, a_columns_existing_cap_srch_m);
		 }
		else//
			 a_get_logged_in_user_exsiting_revenue_cap_m = searchRecord('customrecord_revenue_share', null, a_revenue_cap_filter_m, a_columns_existing_cap_srch_m);
			if(a_get_logged_in_user_exsiting_revenue_cap_m)
			{
             	nlapiLogExecution('DEBUG','total',a_get_logged_in_user_exsiting_revenue_cap_m.length);
				for(var i=0;i< a_get_logged_in_user_exsiting_revenue_cap_m.length;i++)
				{
					var revenue_id=a_get_logged_in_user_exsiting_revenue_cap_m[i].getId();
					var status=a_get_logged_in_user_exsiting_revenue_cap_m[i].getValue('custrecord_revenue_share_approval_status');
					var pid_m=a_get_logged_in_user_exsiting_revenue_cap_m[i].getValue('custrecord_revenue_share_project');
					nlapiLogExecution('DEBUG','Projectid',pid_m);
					if(pid_m)
					{	
						var a_JSON_m = {};
						var s_create_update_mode_m = '';
						var linkUrl1 = nlapiResolveURL('SUITELET', '1558', 'customdeploy1');
						
						var a_effort_activity_mnth_end_filter = [
                            ['custrecord_revenue_share_parent_json', 'anyof', parseInt(revenue_id)]
                        ];
                        var a_columns_mnth_end_effort_activity_srch = new Array();
                        a_columns_mnth_end_effort_activity_srch[0] = new nlobjSearchColumn('created').setSort(true);
                        var a_get_mnth_end_effrt_activity = nlapiSearchRecord('customrecord_fp_rev_rec_mnth_end_json', null, a_effort_activity_mnth_end_filter, a_columns_mnth_end_effort_activity_srch);
                        if (a_get_mnth_end_effrt_activity) {
                            var i_mnth_end_json_id = a_get_mnth_end_effrt_activity[0].getId();
                        }
						var project=nlapiLookupField('job',pid_m,['customer','custentity_fp_rev_rec_type','internalid']);
						var proj_cust=project['customer'];
						var pro_id=project['internalid'];
						var fp_type=project['custentity_fp_rev_rec_type'];
					
						{
							var i_project_internal_id_m = pro_id;
							var f_project_existing_revenue_m = 0;
							var i_existing_revenue_share_rcrd_id_m = 0;
							var b_is_proj_value_chngd_flag_m = 'F';
							var s_existing_revenue_share_status_m = '';
							var s_mode='Submit';
				
							a_JSON_m = {
									project_cutomer:proj_cust ,
									project_id: pro_id,
									//project_existing_value: f_project_existing_revenue_m,
									project_rev_rec_type: fp_type,
									suitelet_url: linkUrl1 + '&proj_id=' + pid_m + '&mode=' + s_mode + '&rcrd_id=' + revenue_id + '&status=' + status + '&month_id=' + i_mnth_end_json_id
								};
				
							a_project_list_m.push(a_JSON_m);
							a_pro_li_m.push(a_JSON_m);
						}
					}
				
				}
		
				
				}
		
		
		
		//mile ends
		
		var a_get_logged_in_user_exsiting_revenue_cap = '';
		if(_logValidation(a_practice_list)){
			var a_revenue_cap_filter = [[['custrecord_fp_rev_rec_others_projec.custentity_projectmanager', 'anyof', i_user_logegdIn_id], 'or',
								['custrecord_fp_rev_rec_others_projec.custentity_deliverymanager', 'anyof', i_user_logegdIn_id], 'or',
								['custrecord_fp_rev_rec_others_projec.custentity_clientpartner', 'anyof', i_user_logegdIn_id], 'or',
								['custrecord_fp_rev_rec_others_projec.custentity_practice','anyof',a_practice_list],'or',
								['custrecord_fp_rev_rec_others_customer.custentity_clientpartner', 'anyof', i_user_logegdIn_id]],
								'and',
								['custrecord_revenue_other_approval_status','anyof',[2,3]],'and',
								['isinactive','is','F']];
			}
			else
			{
				var a_revenue_cap_filter = [[['custrecord_fp_rev_rec_others_projec.custentity_projectmanager', 'anyof', i_user_logegdIn_id], 'or',
								['custrecord_fp_rev_rec_others_projec.custentity_deliverymanager', 'anyof', i_user_logegdIn_id], 'or',
								['custrecord_fp_rev_rec_others_projec.custentity_clientpartner', 'anyof', i_user_logegdIn_id], 'or',
								['custrecord_fp_rev_rec_others_customer.custentity_clientpartner', 'anyof', i_user_logegdIn_id]],
								'and',
								['custrecord_revenue_other_approval_status','anyof',parseInt(2)],'and',
								['isinactive','is','F']];
			}
			var a_columns_existing_cap_srch = new Array();
			a_columns_existing_cap_srch[0] = new nlobjSearchColumn('custrecord_fp_rev_rec_others_projec');
			a_columns_existing_cap_srch[1] = new nlobjSearchColumn('custrecord_fp_rev_rec_others_total_rev');
			a_columns_existing_cap_srch[2] = new nlobjSearchColumn('created').setSort(true);
			a_columns_existing_cap_srch[3] = new nlobjSearchColumn('custrecord_revenue_other_approval_status');//(parseInt(i_user_logegdIn_id) == parseInt(41571) ||
			if (parseInt(i_user_logegdIn_id) == parseInt(7905)|| parseInt(i_user_logegdIn_id) == parseInt(35819) || parseInt(i_user_logegdIn_id) == parseInt(41571) || parseInt(i_user_logegdIn_id) == parseInt(72055) ||  parseInt(i_user_logegdIn_id) == parseInt(30484)|| parseInt(i_user_logegdIn_id) == parseInt(3165))
			{
			var a_get_logged_in_user_exsiting_revenue_cap = nlapiSearchRecord("customrecord_fp_rev_rec_others_parent",null,
[["custrecord_revenue_other_approval_status","anyof","2"], 
   "AND", 
   ["custrecord_fp_rev_rec_proj_end_date","onorafter","today"],"AND",
   ['isinactive','is','F']
], 
a_columns_existing_cap_srch
);
			//
			/*	var a_project_cap_filter =[ ['custrecord_revenue_other_approval_status','anyof',parseInt(2)],
   'and',                         ['custrecord_fp_rev_rec_others_projec.enddate','onorafter','today']];
														
				a_get_logged_in_user_exsiting_revenue_cap = searchRecord('customrecord_fp_rev_rec_others_parent', null, a_project_cap_filter, a_columns_existing_cap_srch);
			 */
			 }
			else
			 a_get_logged_in_user_exsiting_revenue_cap = searchRecord('customrecord_fp_rev_rec_others_parent', null, a_revenue_cap_filter, a_columns_existing_cap_srch);
			if(_logValidation(a_get_logged_in_user_exsiting_revenue_cap))
			{
              	nlapiLogExecution('DEBUG','total',a_get_logged_in_user_exsiting_revenue_cap.length);
				for(var i=0;i< a_get_logged_in_user_exsiting_revenue_cap.length;i++)
				{
					var i_revenue_id=a_get_logged_in_user_exsiting_revenue_cap[i].getId();
					var status=a_get_logged_in_user_exsiting_revenue_cap[i].getValue('custrecord_revenue_other_approval_status');
					var pid=a_get_logged_in_user_exsiting_revenue_cap[i].getValue('custrecord_fp_rev_rec_others_projec');
					nlapiLogExecution('DEBUG','Projectid',pid);
					if(pid)
					{			
						var a_JSON = {};
						var s_create_update_mode = '';
						var linkUrl2 = nlapiResolveURL('SUITELET', '1563', 'customdeploy_planvseffortview');
						var a_effort_activity_mnth_end_filter = [
                            ['custrecord_fp_others_mnth_end_fp_parent', 'anyof', parseInt(i_revenue_id)]
                        ];
                        var a_columns_mnth_end_effort_activity_srch = new Array();
                        a_columns_mnth_end_effort_activity_srch[0] = new nlobjSearchColumn('created').setSort(true);
                        var a_get_mnth_end_effrt_activity = nlapiSearchRecord('customrecord_fp_others_mnth_end_json', null, a_effort_activity_mnth_end_filter, a_columns_mnth_end_effort_activity_srch);
                        if (a_get_mnth_end_effrt_activity) {
                            var i_mnth_end_jsonid = a_get_mnth_end_effrt_activity[0].getId();
                        }
						var project=nlapiLookupField('job',pid,['customer','custentity_fp_rev_rec_type','internalid']);
						var proj_cust=project['customer'];
						var pro_id=project['internalid'];
						var fp_type=project['custentity_fp_rev_rec_type'];
					//	for(var i_pro_index = 0; i_pro_index < a_project_search_results.length; i_pro_index++)
						{
							var i_project_internal_id = pro_id;
							var f_project_existing_revenue = 0;
							var i_existing_revenue_share_rcrd_id = 0;
							var b_is_proj_value_chngd_flag = 'F';
							var s_existing_revenue_share_status = '';
							var s_mde='Submit';
				
							a_JSON = {
									project_cutomer: proj_cust,
									project_id: pro_id,
								//	project_existing_value: f_project_existing_revenue,
									project_rev_rec_type: fp_type,
									suitelet_url: linkUrl2 + '&proj_id=' + pid + '&mode=' + s_mde + '&rcrd_id=' + i_revenue_id + '&status=' + status + '&month_id=' + i_mnth_end_jsonid
								};
				
							a_project_list.push(a_JSON);
						}
					}
				
				}
				var f_form_sublist = o_form_obj.addSubList('project_sublist','list','Project List');
				f_form_sublist.addField('project_cutomer','select','Customer','customer').setDisplayType('inline');
				f_form_sublist.addField('project_id','select','Project','job').setDisplayType('inline');
				f_form_sublist.addField('project_rev_rec_type','select','Rev Rec Type','customlist_fp_rev_rec_project_type').setDisplayType('inline');
				f_form_sublist.addField('suitelet_url', 'url', '').setLinkText('View');
				var total_fp_mi =new Array();
				total_fp_mi = a_project_list.concat(a_project_list_m);
				f_form_sublist.setLineItemValues(total_fp_mi);
		
			}
		else
		{
           //throw "No Records Found";
			var s_result_not_found = "<table>";
			s_result_not_found += "<tr><td>";
			s_result_not_found += "NO RESULTS FOUND";
			s_result_not_found += "</td></tr>";
			s_result_not_found += "</table>";
			
			var f_no_rslt_found = o_form_obj.addField('rcrd_nt_found', 'inlinehtml', 'No Record Found');
			f_no_rslt_found.setDefaultValue(s_result_not_found);
                    
		}
		
		response.writePage(o_form_obj);
	}
	catch(err)
	{
		nlapiLogExecution('ERROR','function suiteletFunction_FP_RevRec_Dashboard','ERROR MESSAGE :- '+err);
	}
}

function searchRecord(recordType, savedSearch, arrFilters, arrColumns,
    filterExpression) {

    try {
        var search = null;

        // if a saved search is provided, load it and add the filters and
        // columns
        if (isNotEmpty(savedSearch)) {
            search = nlapiLoadSearch(recordType, savedSearch);

            if (isArrayNotEmpty(arrFilters)) {
                search.addFilters(arrFilters);
            }

            if (isArrayNotEmpty(arrColumns)) {
                search.addColumns(arrColumns);
            }

            if (isArrayNotEmpty(filterExpression)) {
                search.setFilterExpression(filterExpression);
            }
        }
        // create a new search
        else {
            search = nlapiCreateSearch(recordType, arrFilters, arrColumns);
        }

        // run search
        var resultSet = search.runSearch();

        // iterate through the search and get all data 1000 at a time
        var searchResultCount = 0;
        var resultSlice = null;
        var searchResult = [];

        do {
            resultSlice = resultSet.getResults(searchResultCount,
                searchResultCount + 1000);

            if (resultSlice) {

                resultSlice.forEach(function(result) {

                    searchResult.push(result);
                    searchResultCount++;
                });
            }
        } while (isArrayNotEmpty(resultSlice) && resultSlice.length >= 1000);

        return searchResult;
    } catch (err) {
        nlapiLogExecution('ERROR', 'searchRecord', err);
        throw err;
    }
}

function isEmpty(value) {

    return value == null || value == "" || typeof(value) == undefined;
}

function isNotEmpty(value) {

    return !isEmpty(value);
}

function isArrayEmpty(argArray) {

    return !isArrayNotEmpty(argArray);
}

function isArrayNotEmpty(argArray) {

    return (isNotEmpty(argArray) && argArray.length > 0);
}
function _logValidation(value) 
{
 if(value!='null' && value != null && value != null && value != '' && value != undefined && value != undefined && value != 'undefined' && value!= 'undefined'&& value!= 'NaN' && value != NaN) 
 {
  return true;
 }
 else 
 { 
  return false;
 }
 }