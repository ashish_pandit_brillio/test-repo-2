/**
 * Calculate practice wise break-up of projected / recognize revenue for the
 * current project (FB) for the current month
 * 
 * Version Date Author Remarks
 * 
 * 1.00 21 Jun 2016 Nitish Mishra
 * 
 */

function userEventBeforeSubmit(type) {
	try {
		if (type == 'create' || type == 'edit' || type == 'xedit') {
			var projectId = nlapiGetFieldValue('custrecord_prp_project');
			var projectCurrencyValue = nlapiGetFieldValue('custrecord_prp_currency');
			var projectCurrencyText = nlapiGetFieldText('custrecord_prp_currency');

			if (projectId) {
				var startDate = nlapiGetFieldValue('custrecord_prp_start_date');
				var d_startDate = nlapiStringToDate(startDate);
				var endDate = nlapiGetFieldValue('custrecord_prp_end_date');
				var d_endDate = nlapiStringToDate(endDate);
				var today = new Date();

				var projectData = nlapiLookupField('job', projectId, [
				        'jobbillingtype', 'custentity_t_and_m_monthly',
				        'customer', 'custentity_projectvalue',
				        'custentity_practice', 'startdate', 'enddate',
				        'subsidiary', 'entityid' ]);

				if (projectData.jobbillingtype == 'FBM') {

					// if its a past month, do revenue recognition
					if (d_endDate < today) {
						nlapiSetFieldValue('custrecord_prp_is_recognised', 'T');
						revenueRecognition(projectData, projectId, startDate,
						        endDate, d_startDate, d_endDate,
						        projectCurrencyText, projectCurrencyValue);
					} else { // else projection
						revenueProjection(projectData, projectId, startDate,
						        endDate, d_startDate, d_endDate,
						        projectCurrencyText, projectCurrencyValue);
					}
				}
			}
		}
	} catch (err) {
		nlapiLogExecution('ERROR', 'userEventBeforeSubmit', err);
	}
}

function revenueRecognition(projectData, projectId, startDate, endDate,
        d_startDate, d_endDate, projectCurrencyText, projectCurrencyValue)
{
	try {
		// var recognizedAmount = 0;
		var projectName = projectData.entityid;

		var filters = [
		        new nlobjSearchFilter('formuladate', null, 'within', startDate,
		                endDate).setFormula('{trandate}'),
		        new nlobjSearchFilter('custcolprj_name', null, 'startswith',
		                projectName) ];

		// if LLC
		if (projectData.subsidiary == 2) {
			filters.push(new nlobjSearchFilter('account', null, 'anyof',
			        [ '773' ]));
		} else { // BTPL
			filters.push(new nlobjSearchFilter('account', null, 'anyof', [
			        '773', '732' ]));
		}

		var jeSearch = nlapiSearchRecord('transaction', null, filters, [
		        new nlobjSearchColumn('fxamount', null, 'sum'),
		        new nlobjSearchColumn('currency', null, 'group') ]);

		nlapiLogExecution('debug', 'search data', JSON.stringify(jeSearch));

		var recognizedAmount = 0;

		if (jeSearch) {

			jeSearch.forEach(function(je) {
				var currency = je.getText('currency', null, 'group');
				var amount = je.getValue('fxamount', null, 'sum');
				var exchangeRate = nlapiExchangeRate(currency,
				        projectCurrencyText, startDate);
				var convertedAmount = exchangeRate * amount;
				recognizedAmount += convertedAmount;
			});
		}

		// practice wise break-up
		var practiceWiseBreakUp = {};
		var inactivePracticeMapping = getInactivePracticeMapping();

		if (inactivePracticeMapping[projectData.custentity_practice]) {
			projectData.custentity_practice = inactivePracticeMapping[department];
		}

		practiceWiseBreakUp[projectData.custentity_practice] = recognizedAmount;

		// remove all existing lines
		var lineCount = nlapiGetLineItemCount('recmachcustrecord_eprp_project_rev_projection');
		for (var line = lineCount; line > 0; line--) {
			nlapiRemoveLineItem(
			        'recmachcustrecord_eprp_project_rev_projection', line);
		}

		var usdExchangeRate = nlapiExchangeRate(projectCurrencyText, 'USD',
		        endDate);

		// add new lines
		for ( var practice in practiceWiseBreakUp) {
			if (practiceWiseBreakUp[practice] < 0) {
				practiceWiseBreakUp[practice] = 0;
			}

			nlapiSelectNewLineItem('recmachcustrecord_eprp_project_rev_projection');
			nlapiSetCurrentLineItemValue(
			        'recmachcustrecord_eprp_project_rev_projection',
			        'custrecord_eprp_practice', practice);
			nlapiSetCurrentLineItemValue(
			        'recmachcustrecord_eprp_project_rev_projection',
			        'custrecord_eprp_revenue', practiceWiseBreakUp[practice]);
			nlapiSetCurrentLineItemValue(
			        'recmachcustrecord_eprp_project_rev_projection',
			        'custrecord_eprp_currency', projectCurrencyValue);
			nlapiSetCurrentLineItemValue(
			        'recmachcustrecord_eprp_project_rev_projection',
			        'custrecord_eprp_usd_revenue', usdExchangeRate
			                * practiceWiseBreakUp[practice]);
			nlapiCommitLineItem('recmachcustrecord_eprp_project_rev_projection');
		}
	} catch (err) {
		nlapiLogExecution('ERROR', 'revenueRecognition', err);
		throw err;
	}
}

function revenueProjection(projectData, projectId, startDate, endDate,
        d_startDate, d_endDate, projectCurrencyText, projectCurrencyValue)
{
	try {
		// get the total project value
		var totalProjectValue = parseFloat(projectData.custentity_projectvalue);
		nlapiLogExecution('debug', 'total project value', totalProjectValue);

		// get the recognized amount
		var totalRecognizedAmount = getTotalRecognizedAmount(projectId,
		        projectData.subsidiary, projectData.entityid, startDate,
		        projectCurrencyText);
		nlapiLogExecution('debug', 'total recognized value',
		        totalRecognizedAmount);

		// get the remaining amount
		var totalRemainingAmount = totalProjectValue - totalRecognizedAmount;
		nlapiLogExecution('debug', 'total remaining value',
		        totalRemainingAmount);

		// total monthly duration of the project
		var countProjectMonths = getMonthDiff(
		        nlapiStringToDate(projectData.startdate),
		        nlapiStringToDate(projectData.enddate));
		nlapiLogExecution('debug', 'project duration', countProjectMonths);

		// remaining months
		var countRemainingMonths = getMonthDiff(new Date(),
		        nlapiStringToDate(projectData.enddate));
		nlapiLogExecution('debug', 'remaining months', countRemainingMonths);

		// calculate per month amount
		var projectedPerMonthAmount = Math.round(totalRemainingAmount
		        / countRemainingMonths);
		nlapiLogExecution('debug', 'per month amount', projectedPerMonthAmount);

		// practice wise break-up
		var practiceWiseBreakUp = {};
		var inactivePracticeMapping = getInactivePracticeMapping();

		if (inactivePracticeMapping[projectData.custentity_practice]) {
			projectData.custentity_practice = inactivePracticeMapping[department];
		}

		practiceWiseBreakUp[projectData.custentity_practice] = projectedPerMonthAmount;

		// remove all existing lines
		var lineCount = nlapiGetLineItemCount('recmachcustrecord_eprp_project_rev_projection');
		for (var line = lineCount; line > 0; line--) {
			nlapiRemoveLineItem(
			        'recmachcustrecord_eprp_project_rev_projection', line);
		}

		var usdExchangeRate = nlapiExchangeRate(projectCurrencyText, 'USD',
		        endDate);

		// add new lines
		for ( var practice in practiceWiseBreakUp) {
			if (practiceWiseBreakUp[practice] < 0) {
				practiceWiseBreakUp[practice] = 0;
			}

			nlapiSelectNewLineItem('recmachcustrecord_eprp_project_rev_projection');
			nlapiSetCurrentLineItemValue(
			        'recmachcustrecord_eprp_project_rev_projection',
			        'custrecord_eprp_practice', practice);
			nlapiSetCurrentLineItemValue(
			        'recmachcustrecord_eprp_project_rev_projection',
			        'custrecord_eprp_revenue', practiceWiseBreakUp[practice]);
			nlapiSetCurrentLineItemValue(
			        'recmachcustrecord_eprp_project_rev_projection',
			        'custrecord_eprp_currency', projectCurrencyValue);
			nlapiSetCurrentLineItemValue(
			        'recmachcustrecord_eprp_project_rev_projection',
			        'custrecord_eprp_usd_revenue', usdExchangeRate
			                * practiceWiseBreakUp[practice]);
			nlapiCommitLineItem('recmachcustrecord_eprp_project_rev_projection');
		}
	} catch (err) {
		nlapiLogExecution('ERROR', 'revenueProjection', err);
		throw err;
	}
}

function getTotalRecognizedAmount(projectId, subsidiary, projectName,
        monthStartDate, projectCurrency)
{
	try {
		var recognizedAmount = 0;

		nlapiLogExecution('debug', 'monthStartDate', monthStartDate);
		nlapiLogExecution('debug', 'projectName', projectName);

		var filters = [
		        new nlobjSearchFilter('formuladate', null, 'before',
		                monthStartDate).setFormula('{trandate}'),
		        new nlobjSearchFilter('custcolprj_name', null, 'startswith',
		                projectName) ];

		// if LLC
		if (subsidiary == 2) {
			filters.push(new nlobjSearchFilter('account', null, 'anyof',
			        [ '773' ]));
		} else { // BTPL
			filters.push(new nlobjSearchFilter('account', null, 'anyof', [
			        '773', '732' ]));
		}

		var jeSearch = nlapiSearchRecord('transaction', null, filters, [
		        new nlobjSearchColumn('fxamount', null, 'sum'),
		        new nlobjSearchColumn('currency', null, 'group') ]);

		nlapiLogExecution('debug', 'search data', JSON.stringify(jeSearch));

		if (jeSearch) {

			jeSearch.forEach(function(je) {
				var currency = je.getText('currency', null, 'group');
				var amount = je.getValue('fxamount', null, 'sum');
				var exchangeRate = nlapiExchangeRate(currency, projectCurrency,
				        monthStartDate);
				nlapiLogExecution('debug', 'exchangeRate', exchangeRate);
				var convertedAmount = exchangeRate * amount;
				recognizedAmount += convertedAmount;
			});
		}

		return recognizedAmount;
	} catch (err) {
		nlapiLogExecution('ERROR', 'getTotalRecognizedAmount', err);
		throw err;
	}
}

function getMonthDiff(fromDate, toDate) {
	var month = 1;
	for (;; month++) {
		var newDate = nlapiAddMonths(fromDate, month);

		if (newDate >= toDate) {
			break;
		}
	}

	return month;
}
