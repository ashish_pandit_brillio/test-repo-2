/*
   	Script Name: SUT_IIT_Print_Invoice.js
	Author: Pralhad Solanke
	Company: Inspirria Cloudtech Pvt. Ltd.
	Date: 12/07/2017
    Description:


	Script Modification Log:

	-- Date --			-- Modified By --			--Requested By--				-- Description --
	 

 *
 */


function suitelet_invoice(request, response)
{

	try{
		
		
		nlapiLogExecution('DEBUG','userEventBeforeLoad_printButton', 'In Suitelet--------');

		var a_subisidiary = new Array();
		var s_pass_code;
		var stRoundMethod;
		
		var i_AitGlobalRecId = SearchGlobalParameter();
		nlapiLogExecution('DEBUG', 'Bill ', "i_AitGlobalRecId = " + i_AitGlobalRecId);
		
		if (i_AitGlobalRecId != 0) 
		{
			var o_AitGloRec = nlapiLoadRecord('customrecord_indian_tax_glo_parameter', i_AitGlobalRecId);
			
			a_subisidiary = o_AitGloRec.getFieldValues('custrecord_ait_subsidiary');
			nlapiLogExecution('DEBUG', 'Bill ', "a_subisidiary->" + a_subisidiary);
			
			var i_vatCode = o_AitGloRec.getFieldValue('custrecord_ait_vat_code');
			nlapiLogExecution('DEBUG', 'Bill ', "i_vatCode->" + i_vatCode);
			
			var i_taxcode = o_AitGloRec.getFieldValue('custrecord_ait_taxcode');
			nlapiLogExecution('DEBUG', 'Bill ', "i_taxcode->" + i_taxcode);
			
			s_pass_code = o_AitGloRec.getFieldValue('custrecord_encpwdstring');
			nlapiLogExecution('DEBUG', 'Bill ', "s_pass_code->" + s_pass_code);
			
			stRoundMethod = o_AitGloRec.getFieldValue('custrecord_ait_st_roundoff');
			
		}
		var context = nlapiGetContext();
		var environment = context.getEnvironment();
		var domainUrl = '';
		if(environment == 'SANDBOX'){
			domainUrl = 'https://system.sandbox.netsuite.com';
		}
		else if(environment == 'PRODUCTION'){
			domainUrl = 'https://system.na1.netsuite.com';
		}
		var invoiceId = request.getParameter('recId');
		var invoiceType = request.getParameter('recordType');
		var ObjRecord = nlapiLoadRecord(invoiceType, parseInt(invoiceId));
		//var customerName = ObjRecord.getFieldText('entity');//.split(" ");
		var customerId = ObjRecord.getFieldValue('entity');
		var custLoad = nlapiLoadRecord("customer", customerId);
		var customerName = custLoad.getFieldValue('altname');
		
		//*********************************************** Modified By Poobalan.K ************************************//
		
		var ShipAdd = ObjRecord.getFieldValue('shipaddress');
		nlapiLogExecution('DEBUG', 'ShipAdd', ShipAdd);
		var custlinecnt= custLoad.getLineItemCount('addressbook');
		nlapiLogExecution('DEBUG', 'custlinecnt', custlinecnt);
		
		var CustomerAttention ='',CustomerPhone='';
		for(var i=1;i<=custlinecnt;i++)
		{ 
			var attention_to = custLoad.getLineItemValue('addressbook','addressbookaddress_text',i);
			if(attention_to == ShipAdd)
				{
				CustomerAttention = custLoad.getLineItemValue('addressbook','attention',i);
				CustomerPhone =	custLoad.getLineItemValue('addressbook','phone',i);
				}
	    	}
        nlapiLogExecution('DEBUG', 'Attn & Phone',CustomerAttention+"<--Attention Phone-->"+ CustomerPhone);
		if(CustomerAttention == null){CustomerAttention ='';}
		if(CustomerPhone == null){CustomerPhone ='';} 
		
	//*********************************************** Modified By Poobalan.K ************************************//
		//if(!_logValidation(attention_to)){
			//attention_to = '';
		//}
		//var i_phone = custLoad.getLineItemValue('addressbook','phone',1);
		//if(!_logValidation(i_phone)){
			//i_phone = '';
		//}
		
		
		var gstin_bill_addr = '' ;
		var ModeOfTransport = '';
		var VehicleNo = '';
		var eWayNo = '';
		
		var i_memo = ObjRecord.getFieldValue('memo');
		nlapiLogExecution('DEBUG', 'suitelet_generateInvoice', 'i_memo=' + i_memo);
		
		if(!_logValidation(i_memo))
		{
			i_memo='';
		}

		var project_Id = ObjRecord.getFieldValue('job');
		nlapiLogExecution('DEBUG', 'suitelet_generateInvoice', 'project_Id=' + project_Id);
		if(!_logValidation(project_Id))
		{
			project_Id ='';
		}
		if(_logValidation(project_Id)){
			var project_Name = nlapiLookupField('job',project_Id,['companyname','entityid']);
			var companyName = project_Name.companyname;
			var companyID = project_Name.entityid;
		}
		
		var s_PlaceOfSupply = ObjRecord.getFieldText('custbody_iit_pos_invoice');
		nlapiLogExecution('DEBUG', 'suitelet_generateInvoice', 's_PlaceOfSupply=' + s_PlaceOfSupply);
		
		var i_billTo = ObjRecord.getFieldValue('billaddresslist');
		nlapiLogExecution('DEBUG', 'suitelet_generateInvoice', 'i_billTo=' + i_billTo);
		
		var s_billTo = ObjRecord.getFieldText('billaddresslist');
		nlapiLogExecution('DEBUG', 'suitelet_generateInvoice', 's_billTo=' + s_billTo);
		
		var i_shipTo = ObjRecord.getFieldValue('shipaddresslist');
		nlapiLogExecution('DEBUG', 'suitelet_generateInvoice', 'i_shipTo=' + i_shipTo);
		
		var transaction_type = ObjRecord.getFieldValue('custbody_iit_transactiontype');
		nlapiLogExecution('DEBUG', 'suitelet_generateInvoice', 'transaction_type=' + transaction_type);
		
		var revision = ObjRecord.getFieldValue('custbody_iit_tran_body_revision');
		nlapiLogExecution('DEBUG', 'suitelet_generateInvoice', 'revision=' + revision);
		
		var export_chkBox = ObjRecord.getFieldValue('custbody_iit_export');
		nlapiLogExecution('DEBUG', 'suitelet_generateInvoice', 'export_chkBox=' + export_chkBox);
		
		//==========================Get indian Currency internalD and Exchange rate =====================//
		var i_Subtotal = parseFloat(0);
		
		var i_Currency = ObjRecord.getFieldValue('currency');
		nlapiLogExecution('DEBUG', 'Getting i_Currency', 'i_Currency = '+i_Currency);
		var i_INRcurrencyID = getAITIndianCurrency();
		nlapiLogExecution('DEBUG', 'Getting i_INRcurrencyID', 'i_INRcurrencyID = '+i_INRcurrencyID);
		var i_Exchangerate = nlapiExchangeRate(i_Currency, i_INRcurrencyID);
		nlapiLogExecution('DEBUG', 'Getting i_Exchangerate', 'i_Exchangerate = '+i_Exchangerate);
		
		
		ModeOfTransport = ObjRecord.getFieldText('custbody_iit_mode_of_transport');
		nlapiLogExecution('DEBUG', 'suitelet_generateInvoice', 'ModeOfTransport=' + ModeOfTransport);
		if(_nullValidation(ModeOfTransport))
			ModeOfTransport = '';
		
		VehicleNo = ObjRecord.getFieldValue('custbody_iit_vehicle_no');
		nlapiLogExecution('DEBUG', 'suitelet_generateInvoice', 'VehicleNo=' + VehicleNo);
		if(_nullValidation(VehicleNo))
			VehicleNo = '';
		
		eWayNo = ObjRecord.getFieldValue('custbody_iit_eway_bill_no');
		nlapiLogExecution('DEBUG', 'suitelet_generateInvoice', 'eWayNo=' + eWayNo);
		if(_nullValidation(eWayNo))
			eWayNo = '';
		
		if(export_chkBox == 'T')
		{
			var paymentType = ObjRecord.getFieldValue('custbody_iit_gst_payment_under_exp');
		}
		var	i_Count = custLoad.getLineItemCount('addressbook');
		nlapiLogExecution('DEBUG', 'suitelet_generateInvoice', 'i_Count=' + i_Count);
		
		for(var i_Temp = 1; i_Temp <= i_Count; i_Temp++)
		{
			var i_id = custLoad.getLineItemValue('addressbook', 'internalid', i_Temp);
			nlapiLogExecution('DEBUG', 'i_id: ', i_id);
			if(_logValidation(i_id) && (i_id == i_billTo))
			{
				custLoad.selectLineItem('addressbook', i_Temp);
				var o_Subrecord = custLoad.viewCurrentLineItemSubrecord('addressbook', 'addressbookaddress');
				nlapiLogExecution('DEBUG', 'o_Subrecord: ', o_Subrecord);
				var bill_state = o_Subrecord.getFieldText('custrecord_iit_address_gst_state');
				nlapiLogExecution('DEBUG', 'bill_state: ', bill_state);
				
				var bill_state_code = o_Subrecord.getFieldValue('custrecord_iit_address_gst_state_code');
				nlapiLogExecution('DEBUG', 'bill_state_code: ', bill_state_code);
				
				var bill_gstin = o_Subrecord.getFieldValue('custrecord_iit_address_gstn_uid');
				nlapiLogExecution('DEBUG', 'bill_gstin: ', bill_gstin);
				
				var cust_addressee = o_Subrecord.getFieldValue('addressee');
				var cust_addr1 = o_Subrecord.getFieldValue('addr1');
				var cust_addr2 = o_Subrecord.getFieldValue('addr2');
				var cust_city = o_Subrecord.getFieldValue('city');
				var cust_state = o_Subrecord.getFieldText('dropdownstate');
				var cust_zip = o_Subrecord.getFieldValue('zip');
				
				var customer_bill_to_address_without_name = '';
				if(cust_addressee)
					customer_bill_to_address_without_name = cust_addressee + '\n';
				
				if(cust_addr1)
					customer_bill_to_address_without_name += cust_addr1 + '\n';
				
				if(cust_addr2)
					customer_bill_to_address_without_name += cust_addr2 + '\n';
				 
				if(cust_city)
					customer_bill_to_address_without_name += cust_city + ' ';
				
				if(cust_state)
					customer_bill_to_address_without_name += cust_state + ' ';
				
				if(cust_zip)
					customer_bill_to_address_without_name += cust_zip;
			}
			
			if(_logValidation(i_id) && (i_id == i_shipTo))
			{
				custLoad.selectLineItem('addressbook', i_Temp);
				var o_Subrecord = custLoad.viewCurrentLineItemSubrecord('addressbook', 'addressbookaddress');
				nlapiLogExecution('DEBUG', 'o_Subrecord: ', o_Subrecord);
				var ship_state = o_Subrecord.getFieldText('custrecord_iit_address_gst_state');
				nlapiLogExecution('DEBUG', 'ship_state: ', ship_state);
				
				var ship_state_code = o_Subrecord.getFieldValue('custrecord_iit_address_gst_state_code');
				nlapiLogExecution('DEBUG', 'ship_state_code: ', ship_state_code);
				
				var ship_gstin = o_Subrecord.getFieldValue('custrecord_iit_address_gstn_uid');
				nlapiLogExecution('DEBUG', 'ship_gstin: ', ship_gstin);
				
				var cust_addressee = o_Subrecord.getFieldValue('addressee');
				var cust_addr1 = o_Subrecord.getFieldValue('addr1');
				var cust_addr2 = o_Subrecord.getFieldValue('addr2');
				var cust_city = o_Subrecord.getFieldValue('city');
				var cust_state = o_Subrecord.getFieldText('dropdownstate');
				var cust_zip = o_Subrecord.getFieldValue('zip');
				
				var customer_bill_to_address_without_name_ship_to = '';
				if(cust_addressee)
					customer_bill_to_address_without_name_ship_to = cust_addressee + '\n';
				
				if(cust_addr1)
					customer_bill_to_address_without_name_ship_to += cust_addr1 + '\n';
				
				if(cust_addr2)
					customer_bill_to_address_without_name_ship_to += cust_addr2 + '\n';
				 
				if(cust_city)
					customer_bill_to_address_without_name_ship_to += cust_city + ' ';
				
				if(cust_state)
					customer_bill_to_address_without_name_ship_to += cust_state + ' ';
				
				if(cust_zip)
					customer_bill_to_address_without_name_ship_to += cust_zip;
				
			}
		}	
		
		if (_nullValidation(customerName))
		{
         	customerName = '';
        }
		var invDate = ObjRecord.getFieldValue('trandate');
		var invNumber = ObjRecord.getFieldValue('tranid');
		var ship_address = ObjRecord.getFieldValue('shipaddress');
		var bill_address = ObjRecord.getFieldValue('billaddress');
	//	nlapiLogExecution('DEBUG', 'suitelet_generateInvoice','customer name and Id and date =' +custId+ " " +custName+ " "+invDate);
		
		var SplitInvDate = invDate.split("/");
		var Month = SplitInvDate[0];
		var Date = SplitInvDate[1];
		var Year = SplitInvDate[2];
		var NewInvDate = Date +"/"+Month+"/"+Year;
		

	//	var custLoad = nlapiLoadRecord("customer", customerId);
		var id = custLoad.getFieldValue('entityid');
		if (_nullValidation(id))
        {
			id = '';
        }
		
		var i_terms = custLoad.getFieldText('terms');
		if (_nullValidation(i_terms))
        {
			custname = '';
        }
		
		var custname = custLoad.getFieldValue('companyname');
		if (_nullValidation(custname))
        {
			custname = '';
        }
		var recipient = ObjRecord.getFieldValue('custbody_iit_transaction_recipient');
		if (_nullValidation(recipient))
        {
			recipient = '';
        }

		custLoad.selectLineItem('addressbook', 1);
		var subRecord = custLoad.viewCurrentLineItemSubrecord('addressbook', 'addressbookaddress');
		nlapiLogExecution('DEBUG', 'suitelet_generateInvoice', 'subRecord=' + subRecord);
		if(_logValidation(subRecord))
			var custState = subRecord.getFieldText('custrecord_iit_address_gst_state');
		if (_nullValidation(custState))
        {
			custState = '';
        }
		var custStateCode = subRecord.getFieldValue('custrecord_iit_address_gst_state_code');
		if (_nullValidation(custStateCode))
        {
			custStateCode = '';
        }
		nlapiLogExecution('DEBUG', 'suitelet_generateInvoice', 'custState=' + custState);

		var recipienState = ObjRecord.getFieldText('custbody_iit_trn_recipient_state');
		if (_nullValidation(recipienState))
        {
			recipienState = '';
        }
		var recipientadd = ObjRecord.getFieldValue('custbody_iit_transaction_recipient_add');
		if (_nullValidation(recipientadd))
        {
			recipientadd = '';
        }
		var recipientStateCode = ObjRecord.getFieldValue('custbody_iit_trn_recipient_state_code');
		if (_nullValidation(recipientStateCode))
        {
			recipientStateCode = '';
        }
		var customeradd = ObjRecord.getFieldValue('custbody_iit_transaction_address');
		if (_nullValidation(customeradd))
        {
			customeradd = '';
        }

		var lineCount = ObjRecord.getLineItemCount('item');
		nlapiLogExecution('DEBUG', 'suitelet_generateBill','lineCount =' + lineCount);

		var total = ObjRecord.getFieldValue('total');
		//var amtstr = ObjRecord.getFieldValue('custbody_amount_in_words');
		var currencyname_end = 'Paise Only';
        nlapiLogExecution('DEBUG', 'GET', 'currencyname_end  ' + currencyname_end);



        var s_point = total.split('.');
        nlapiLogExecution('DEBUG', 'Points', 'Points==>' + s_point[1]);
        var points = toWords(s_point[1]);

        nlapiLogExecution('DEBUG', 'Points', 'Points after convert==>' + points);
        points = ' And  ' + points + ' ' + currencyname_end;

        var total = total.split(',').join('');
        nlapiLogExecution('DEBUG', 'total', 'total==>' + total);

		//==== Code added on 26-6-2017 to convert amount in INR currency
		if (i_Currency != i_INRcurrencyID) 
		{
			total = (parseFloat(total) * parseFloat(i_Exchangerate));
			nlapiLogExecution('DEBUG', 'Getting total', 'total = '+total);
			total = applySTRoundMethod(stRoundMethod, total);
			nlapiLogExecution('DEBUG', 'Getting total', 'total = '+total);
			
		}
		//==== END : Code added on 26-6-2017 to convert amount in INR currency
		
        var s_words = testSkill(total);
        nlapiLogExecution('DEBUG', 's_words', 's_words==>' + s_words);
        var i_Inr_Number = inrFormat(total);
        nlapiLogExecution('DEBUG', 'i_Inr_Number', 'i_Inr_Number==>' + i_Inr_Number);
        var s_words = checklastCharacter(s_words);
        var amtstr;
        if (_logValidation(s_words)) {

        }
        else {
            s_words = '';
        }

        if (s_point[1] == '00') {
            var amtstr = 'Rupees' +s_words + 'Only' ;
        }
        else {
            amtstr = 'Rupees'+s_words + '' + points;
        }

		var invc_location = ObjRecord.getFieldValue('location');
		var fromAddrss='';
		var gstinNumebr;
		if(invc_location != null && invc_location != '' && invc_location != undefined)
	    {
	     	var LocationRec = nlapiLoadRecord('location',invc_location);
	     	fromAddrss = LocationRec.getFieldValue('mainaddress_text');

	     	gstinNumebr = LocationRec.getFieldValue('custrecord_iit_location_gstn_uid');
	     	if (_nullValidation(gstinNumebr))
	        {
	     		gstinNumebr = '';
	        }
	     	tinNumebr = LocationRec.getFieldValue('custrecord_tinnumber');
	     	if (_nullValidation(tinNumebr))
	        {
	     		tinNumebr = '';
	        }
	     	
	     	
	    }		
		
		var customrecord_taxcompanyinfoSearch = nlapiSearchRecord("customrecord_taxcompanyinfo",null,null,[new nlobjSearchColumn("custrecord_permanent_accountno",null,null),new nlobjSearchColumn("custrecord_companyname",null,null)]);
		nlapiLogExecution('DEBUG', 'suitelet_generateInvoice', 'customrecord_taxcompanyinfoSearch =' + customrecord_taxcompanyinfoSearch);
		var panNumber;
		var comp_name = '';
		if(_logValidation(customrecord_taxcompanyinfoSearch))
		{
			panNumber = customrecord_taxcompanyinfoSearch[0].getValue('custrecord_permanent_accountno');
			nlapiLogExecution('DEBUG', 'suitelet_generateInvoice', 'panNumber =' + panNumber);
			if (_nullValidation(panNumber))
	         {
        		panNumber = '';
	         }
			comp_name = customrecord_taxcompanyinfoSearch[0].getValue('custrecord_companyname');
			nlapiLogExecution('DEBUG', 'suitelet_generateInvoice', 'comp_name =' + comp_name);
			if (_nullValidation(comp_name))
	         {
				comp_name = '';
	         }
		}
		var comp_imgrsrc = 'https://system.na1.netsuite.com/core/media/media.nl?id=6237&c=3883006&h=f9681f3581e51644773b';
		comp_imgrsrc = nlapiEscapeXML(comp_imgrsrc);
		
		var xml = "<?xml version=\"1.0\"?>\n<!DOCTYPE pdf PUBLIC \"-//big.faceless.org//report\" \"report-1.1.dtd\">";
		xml += "<pdf>\n";
		xml += "<head>";
		 /*xml += "  <macrolist>";
		  
		  xml += "   <macro id=\"nlfooter\">";
		  xml += "<p align=\"right\">Page <pagenumber\/> of <totalpages\/><\/p>";
		  xml += "   <\/macro>";
		  xml += "  <\/macrolist>";*/
		  	 
		xml += "<style>";
		xml += "table {";
		xml += "   font-family: Times New Roman, Georgia, Serif;";//font-weight: bold;
		xml += "}";
		xml += "<\/style><\/head>";
		
		//code for line no.
		//xml += " <body footer=\"nlfooter\" footer-height=\"10pt\">";
		
		//code for line no
		xml += " <body >";
		

		/*xml += "<table border=\"1\" width=\"100%\" style=\"border-width: 0px\">";
		xml += "	<tr>";
		xml +="<td>";*/
		
		xml += "<table border=\"1\" width=\"100%\" style=\"border-width: 0px\">";
		xml += "	<tr>";     
		if(invoiceType == 'creditmemo')
		{	
			xml += "		<td align=\"center\" colspan=\"4\" font-size=\"13\"><b> Credit Note <\/b><\/td>";
		}
		else if(transaction_type == 3)
		{
			xml += "		<td align=\"center\" colspan=\"4\" font-size=\"13\"><b> Debit Note <\/b><\/td>";	//Government of India/State
		}
		else if(revision == 'T')
		{
			xml += "		<td align=\"center\" colspan=\"4\" font-size=\"13\"><b> Revised Invoice <\/b><\/td>";	//Government of India/State
		}
		else
		{
			if(export_chkBox == 'T')
			{
				if(paymentType == 1)
				{
					xml += "		<td align=\"center\" colspan=\"4\" font-size=\"10\"><b> SUPPLY MEANT FOR EXPORT ON PAYMENT OF INTEGRATED TAX <\/b><\/td>";	//Government of India/State
				}
				if(paymentType == 2)
				{
					xml += "		<td align=\"center\" colspan=\"4\" font-size=\"10\"><b> SUPPLY MEANT FOR EXPORT UNDER BOND OR LETTER OF UNDERTAKING WITHOUT PAYMENT OF INTEGRATED TAX <\/b><\/td>";	//Government of India/State
				}
			}	
			else
			{
					if(invoiceType == 'invoice')
						xml += "		<td align=\"center\" colspan=\"4\" font-size=\"13\"><b> &nbsp; <\/b><\/td>";	//Government of India/State		
			}
		}	
		xml += "	<\/tr>";
		xml += "<\/table>";
		
		var i_subsidiary = ObjRecord.getFieldValue('subsidiary');
		nlapiLogExecution('DEBUG','suiteletFunction',' i_subsidiary -->'+i_subsidiary);
		
		if (i_subsidiary == null || i_subsidiary == 'undefined' || i_subsidiary == '')
		 {
			i_subsidiary = '';
		 }
		 // Modified by poobalan // this value is fetched from invoice//
		 var i_paymentterms = ObjRecord.getFieldText('terms');
		
		if(_nullValidation(i_paymentterms)){
				i_paymentterms = '';
			}
		//*****************************************//////
		xml += "<table border=\"0\" width=\"50%\" align=\"right\">";
		
		xml += "<tr align=\"center\">";
		xml += "<td width=\"25%\" align=\"right\" valign=\"bottom\">";
		xml += "<b>Original For Recipient <\/b><\/td>";
		xml += "<td width=\"25%\" align=\"right\" >";
		xml += "<img height=\"30\" width=\"60\" src='"
		        + comp_imgrsrc + "'/>"; //width:40px;  //class='companyLogo' style='padding-top:0px;
		xml += "</td>";
		xml += "<\/tr>";
		
		var context = nlapiGetContext();
		var i_subcontext = context.getFeature('SUBSIDIARIES');
		nlapiLogExecution('DEBUG', 'Test ', 'i_subcontext --> ' + i_subcontext);
		
		if (i_subcontext != false) {
			if (i_subsidiary != null && i_subsidiary != '' && i_subsidiary != undefined) {
				var o_subsidiary = nlapiLoadRecord('subsidiary', i_subsidiary);
				var subsidiary_name = o_subsidiary.getFieldValue('name');
				
		var o_LocationAddress = LocationRec.viewSubrecord('mainaddress');
				var addline1 = o_LocationAddress.getFieldValue('addr1');
				var addline2 = o_LocationAddress.getFieldValue('addr2');
				var city = o_LocationAddress.getFieldValue('city');
				var sub_state = o_LocationAddress.getFieldValue('state');
				var zip = o_LocationAddress.getFieldValue('zip');
				var country = o_LocationAddress.getFieldText('country');
				nlapiLogExecution('DEBUG', 'TEST Country', 'country :'+country);
			//	var gstin_values = nlapiLookupField('location', id, fields, text)
				/*city = _nullValidation(city);
				zip = _nullValidation(zip);
				sub_state = _nullValidation(sub_state);
				country = _nullValidation(country);*/
				
				//var mainAddress = o_subsidiary.getFieldValue('mainaddress_text');
				
			/*	if (subsidiary_name != null && subsidiary_name != '' && subsidiary_name != undefined) {
					xml += "	<tr>";
					//xml += "		<td font-size=\"11\"  font-family=\"Helvetica\" align=\"center\" colspan=\"2\"><b>"+nlapiEscapeXML(s_asddresse)+"<\/b><\/td>";
					xml += "		<td font-size=\"14\"  font-family=\"Helvetica\" align=\"center\" colspan=\"2\"><b>" + nlapiEscapeXML(subsidiary_name) + "<\/b><\/td>";
					xml += "	<\/tr>";
				}*/
				//if (addline1 != null && addline1 != '' && addline1 != undefined) {
					//xml += "	<tr>";
					//xml += "		<td font-size=\"11\"  font-family=\"Helvetica\" align=\"center\" colspan=\"2\"><b>"+nlapiEscapeXML(s_asddresse)+"<\/b><\/td>";
					//xml += "		<td font-size=\"10\"  font-family=\"Helvetica\" align=\"center\" colspan=\"2\">" + nlapiEscapeXML(addline1) + "<\/td>";
					//xml += "	<\/tr>";
				//}
				//if (addline2 != null && addline2 != '' && addline2 != undefined) {
					//xml += "	<tr>";
					//xml += "		<td font-size=\"11\"  font-family=\"Helvetica\" align=\"center\" colspan=\"2\"><b>"+nlapiEscapeXML(s_asddresse)+"<\/b><\/td>";
					//xml += "		<td font-size=\"10\"  font-family=\"Helvetica\" align=\"center\" colspan=\"2\">" + nlapiEscapeXML(addline2) + "<\/td>";
					//xml += "	<\/tr>";
				//}
				
				//if ((city != null && city != '' && city != undefined) || (sub_state != null && sub_state != '' && sub_state != undefined) || (zip != null && zip != '' && zip != undefined)|| (country != null && country != '' && country != undefined)) {
				//xml += "	<tr>";
				//xml += "		<td font-size=\"11\"  font-family=\"Helvetica\" align=\"center\" colspan=\"2\"><b>"+nlapiEscapeXML(s_asddresse)+"<\/b><\/td>";
				//xml += "		<td font-size=\"10\"  font-family=\"Helvetica\" align=\"center\" colspan=\"2\">" + city + "," + sub_state + "," + zip +","+country+ "<\/td>";
				//xml += "	<\/tr>";
				//}
				//xml += "	<tr>";
				//xml += "	<td>&nbsp;<\/td>";
				//xml += "	<\/tr>";
				
				/*if (country != null && country != '' && country != undefined) {
				xml += "	<tr>";
				//xml += "		<td font-size=\"11\"  font-family=\"Helvetica\" align=\"center\" colspan=\"2\"><b>"+nlapiEscapeXML(s_asddresse)+"<\/b><\/td>";
				xml += "		<td font-size=\"10\"  font-family=\"Helvetica\" align=\"center\" colspan=\"2\">" + country + "<\/td>";
				xml += "	<\/tr>";*/
				//}
			}
		}
		

		var IRN='';
		var qr_code='000';
		var transactionSearch = nlapiSearchRecord("transaction",null,
														[
														["internalid","anyof",invoiceId], 
														"AND", 
														["mainline","is","T"]
														], 
														[
														new nlobjSearchColumn("custrecord_iit_e_inv_irn","CUSTRECORD_REF_NO",null), 
														new nlobjSearchColumn("custrecord_iit_e_inv_qr","CUSTRECORD_REF_NO",null)

														]
												 );

		if(transactionSearch)
				{
				IRN=transactionSearch[0].getValue("custrecord_iit_e_inv_irn","CUSTRECORD_REF_NO",null)
				nlapiLogExecution('DEBUG','IRN',IRN);

				if(_nullValidation(IRN))
				{
				IRN = '';
				}



				var qr_code= transactionSearch[0].getValue("custrecord_iit_e_inv_qr","CUSTRECORD_REF_NO",null)
				nlapiLogExecution('DEBUG','qr_code',qr_code);

				if(_nullValidation(qr_code))
				{
				qr_code = '000';
				}

		}
		
		var url="https://api.qrserver.com/v1/create-qr-code/?data="+qr_code+"&amp;size=202x170";
		nlapiLogExecution('AUDIT',url,url);

		xml += "<\/table>";
		
		xml += "<table width=\"100%\" >";
		xml += "	<tr>";
		xml += "		<td align=\"center\" width=\"50%\" rowspan=\"3\"  style=\"border-left: 0.1px solid;border-bottom: 0.1px solid;border-top: 0.1px solid;border-right: 0.1px solid;\" font-size=\"20\">TAX INVOICE<\/td>";
		//xml += "		<td align=\"left\" width=\"1%\"  style=\"border-left: 0.1px solid;border-bottom: 0.1px solid;border-top: 0.1px solid;border-right: 0.1px solid;\" font-size=\"0\">&nbsp;<\/td>";
		xml += "		<td align=\"left\" width=\"50%\"  style=\"border-left: 0.1px solid;border-bottom: 0.1px solid;border-top: 0.1px solid;border-right: 0.1px solid;\" font-size=\"10\">Invoice No :"+nlapiEscapeXML(invNumber)+"<\/td>";
		xml += "	<\/tr>";
		
		xml += "	<tr>";
		//xml += "		<td align=\"left\" width=\"1%\"  style=\"border-left: 0.1px solid;border-bottom: 0.1px solid;border-top: 0.1px solid;border-right: 0.1px solid;\" font-size=\"0\">&nbsp;<\/td>";
		//xml += "		<td align=\"left\" width=\"29%\"  style=\"border-left: 0.1px solid;border-bottom: 0.1px solid;border-top: 0.1px solid;border-right: 0.1px solid;\" font-size=\"6\">Duplicate for Supplier/Transporter<\/td>";
		xml += "		<td align=\"left\" width=\"50%\"  style=\"border-left: 0.1px solid;border-bottom: 0.1px solid;border-top: 0.1px solid;border-right: 0.1px solid;\" font-size=\"8\">IRN:"+nlapiEscapeXML(IRN)+"<\/td>";
		xml += "	<\/tr>";
		
		xml += "	<tr>";
		//xml += "		<td align=\"left\" width=\"1%\"  style=\"border-left: 0.1px solid;border-bottom: 0.1px solid;border-top: 0.1px solid;border-right: 0.1px solid;\" font-size=\"0\">&nbsp;<\/td>";
		//xml += "		<td align=\"left\" width=\"29%\"  style=\"border-left: 0.1px solid;border-bottom: 0.1px solid;border-top: 0.1px solid;border-right: 0.1px solid;\" font-size=\"6\">Duplicate for Supplier/Transporter<\/td>";
		xml += "		<td align=\"left\" width=\"50%\"  style=\"border-left: 0.1px solid;border-bottom: 0.1px solid;border-top: 0.1px solid;border-right: 0.1px solid;\" font-size=\"10\">Invoice Date :"+nlapiEscapeXML(NewInvDate)+"<\/td>";
		xml += "	<\/tr>";
		xml += "<\/table>";

		
		xml += "<table width=\"100%\" >";
		xml += "	<tr>";
		xml +="<td width=\"50%\" font-size=\"10\" align=\"left\" style=\"border-left: 0.1px solid;border-right: 0.1px solid;border-bottom: 0.1px solid;\">";
		xml += "<table width=\"100%\" >";
		if (subsidiary_name != null && subsidiary_name != '' && subsidiary_name != undefined) {
			xml += "	<tr>";
			xml += "		<td font-size=\"9\"  font-family=\"Helvetica\" align=\"left\" colspan=\"2\"><b>"+nlapiEscapeXML(subsidiary_name)+"</b><\/td>";
			xml += "	<\/tr>";
		}
		
		if (addline1 != null && addline1 != '' && addline1 != undefined) {
		xml += "	<tr>";
		xml += "		<td font-size=\"9\"  font-family=\"Helvetica\" align=\"left\" colspan=\"2\"><b>" + nlapiEscapeXML(addline1) + "</b><\/td>";
		xml += "	<\/tr>";
		}
		if (addline2 != null && addline2 != '' && addline2 != undefined) {
		xml += "	<tr>";
		xml += "		<td font-size=\"9\"  font-family=\"Helvetica\" align=\"left\" colspan=\"2\"><b>" + nlapiEscapeXML(addline2) + "</b><\/td>";
		xml += "	<\/tr>";
		}
		if ((city != null && city != '' && city != undefined) || (sub_state != null && sub_state != '' && sub_state != undefined) || (zip != null && zip != '' && zip != undefined)|| (country != null && country != '' && country != undefined)) {
		xml += "	<tr>";
		xml += "		<td font-size=\"9\"  font-family=\"Helvetica\" align=\"left\" colspan=\"2\"><b>" + city + "," + sub_state + "," + zip +","+country+ "</b><\/td>";
		xml += "	<\/tr>";
		}
		xml += "	<tr>";
		xml +="<td font-size=\"9\" align=\"left\" >GSTIN No&nbsp;:"+nlapiEscapeXML(gstinNumebr)+"<\/td>";
		xml += "	<\/tr>";
		xml += "	<tr>";
		xml +="<td font-size=\"9\" align=\"left\" >CIN No&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:U22190KA1997FTC022250<\/td>";//"+nlapiEscapeXML(tinNumebr)+"
		xml += "	<\/tr>";
		xml += "	<tr>";
		xml +="<td font-size=\"9\" align=\"left\" >PAN No&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:"+nlapiEscapeXML(panNumber)+"<\/td>";
		xml += "	<\/tr>";
		/*xml += "	<tr>";
		xml += "		<td  font-size=\"10\">State <\/td>";
		xml += "		<td  font-size=\"10\">:<\/td>";
		xml += "		<td  font-size=\"10\">"+nlapiEscapeXML(bill_state)+"<\/td>";
		xml += "		<td  font-size=\"10\" style=\"border-left: 0.1px solid; border-right: 0.1px solid;border-top: 0.1px solid;\">State Code <\/td>";
		xml += "		<td  font-size=\"10\" style=\"border-top: 0.1px solid;\"><\/td>";
		xml += "		<td  font-size=\"10\" style=\"border-top: 0.1px solid;\">"+nlapiEscapeXML(bill_state_code)+"<\/td>";
		xml += "	<\/tr>";*/
		/*xml += "	<tr>";
		xml += "		<td colspan=\"4\" font-size=\"10\">Date of Invoice:"+nlapiEscapeXML(
)+"<\/td>";
	//	xml += "		<td>&nbsp;<\/td>";
		xml += "	<\/tr>";*/
		//xml += "	<tr><td>&nbsp;<\/td><\/tr>";
		xml += "<\/table>";
		xml += "	<\/td>";
		
		xml +="<td width=\"50%\" font-size=\"10\" align=\"left\" style=\"border-right: 0.1px solid;border-bottom: 0.1px solid;\">";
		xml += "<table width=\"100%\" >";
		xml += "	<tr width=\"30%\">";
		xml += "		<td font-size=\"10\">Buyer&nbsp;Contact&nbsp;Person<\/td>";
		xml += "		<td  font-size=\"10\">:<\/td>";
		xml += "		<td  font-size=\"10\">"+nlapiEscapeXML(CustomerAttention)+"<\/td>";
		xml += "	<\/tr>";
		xml += "	<tr>";
		xml += "		<td  font-size=\"10\">Buyer Contact No.<\/td>";
		xml += "		<td  font-size=\"10\">:<\/td>";
		xml += "		<td  font-size=\"10\">"+nlapiEscapeXML(CustomerPhone)+"<\/td>";
		xml += "	<\/tr>";
		xml += "	<tr>";
		xml += "		<td  font-size=\"10\">Reference<\/td>";
		xml += "		<td  font-size=\"10\">:<\/td>";
		xml += "		<td  font-size=\"10\">"+nlapiEscapeXML(i_memo)+"<\/td>";
		xml += "	<\/tr>";
		
		xml += "<\/table>";
		xml += "	<\/td>";
		
		xml += "	<\/tr>";
		//xml += "<\/table>";
		
		/*xml += "<table width=\"100%\" style=\"border-left: 0.1px  solid; border-top: 0.1px  solid; border-right: 0.1px solid;\" >";
		xml += "	<tr>";
		xml += "	<td font-size=\"10\" > Reference &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"+nlapiEscapeXML(i_memo)+" <\/td>"; //i_memo
		//xml += "	<td font-size=\"10\" >:&nbsp; <\/td>"; //i_memo
		//xml += "	<td font-size=\"10\" > "+nlapiEscapeXML(i_memo)+"<\/td>"; //i_memo
		xml += "	<\/tr>";
		xml += "<\/table>";*/
		
		
		//xml += "<table width=\"100%\" style=\"border-left: 0.1px solid;border-top: 0.1px solid;border-right: 0.1px solid;\" >";
		
		//width=\"50%\"
			xml +="<tr width=\"50%\" style=\"border-bottom: 0.1px solid;\">";
			xml +="<td width=\"50%\" font-size=\"10\" align=\"center\" style=\"border-left: 0.1px solid;border-right: 0.1px solid;\" ><b>Details of Receiver | Billed to <\/b><\/td>";
			xml +="<td width=\"50%\" font-size=\"10\" align=\"center\" style=\"border-right: 0.1px solid;\" ><b>Details of Consignee | Shipped to<\/b><\/td>";			
			xml +="<\/tr>";
			
			//width=\"50%\"
			xml +="<tr  style=\"border-bottom: 0.1px solid;\">";
			
			xml +="<td width=\"50%\" font-size=\"10\" align=\"left\" style=\"border-left: 0.1px solid;border-right: 0.1px solid;\">";	
			
			xml += "<table width=\"100%\">";
			xml +="<tr  width=\"30%\" >";
			xml +="<td font-size=\"10\" align=\"left\" ><b>Name</b><\/td>";
			xml +="<td font-size=\"10\" align=\"left\" >:&nbsp;<\/td>";
			xml +="<td font-size=\"10\" align=\"left\" ><b>"+nlapiEscapeXML(customerName/*recipient*/)+"</b><\/td>";
			xml +="<\/tr>";
			xml +="<tr  width=\"30%\" >";
			xml +="<td font-size=\"10\" align=\"left\" >Address<\/td>";
			xml +="<td font-size=\"10\" align=\"left\" >:&nbsp;<\/td>";
			xml +="<td font-size=\"10\" align=\"left\"  >"+nlapiEscapeXML(customer_bill_to_address_without_name/*recipientadd*/)+"<\/td>";
			//xml +="<td font-size=\"10\" align=\"left\" >AddressAddressAddressAddressAddress<\/td>";
			xml +="<\/tr>";
			xml +="<tr  width=\"30%\" >";
			//xml +="<td font-size=\"10\" align=\"left\" >State<\/td>";
			xml +="<td width=\"25%\" font-size=\"10\" align=\"left\" >GSTIN<\/td>";
			xml +="<td font-size=\"10\" align=\"left\" >:&nbsp;<\/td>";
			//xml +="<td font-size=\"10\" align=\"left\" >"+nlapiEscapeXML(bill_state/*recipienState*/)+"<\/td>";
			xml +="<td font-size=\"10\" align=\"left\" >"+nlapiEscapeXML(bill_gstin/*bill_gstin*/)+"<\/td>";
			xml +="<\/tr>";
			xml +="<tr   width=\"50%\" >";
			xml +="<td font-size=\"10\" align=\"left\" >State<\/td>";
			xml +="<td  font-size=\"10\" align=\"left\"  >:&nbsp;<\/td>";
			xml +="<td  font-size=\"10\" align=\"left\" >"+nlapiEscapeXML(bill_state/*recipienState*/)+"<\/td>";
			xml +="<\/tr>";
			xml +="<tr  width=\"50%\" >";
			xml +="<td width=\"30%\" font-size=\"10\" align=\"left\" >State Code<\/td>";
			xml +="<td  font-size=\"10\" align=\"left\" >:&nbsp;<\/td>";
			xml +="<td  font-size=\"10\" align=\"left\" >"+nlapiEscapeXML(bill_state_code)+"<\/td>";
			xml +="<\/tr>";
			/*xml +="<tr  width=\"50%\" >";	
			xml +="<td width=\"25%\" font-size=\"10\" align=\"left\" >GSTIN No.<\/td>";
			xml +="<td font-size=\"10\" align=\"left\" >:&nbsp;<\/td>";
			xml +="<td font-size=\"10\" align=\"left\" >"+nlapiEscapeXML(bill_gstinbill_gstin)+"<\/td>";
			xml +="<\/tr>";*/
			xml +="<\/table>";
			
			xml +="<\/td>";
			
			xml +="<td  width=\"50%\" font-size=\"10\" align=\"left\" style=\"border-right: 0.1px solid;\">";
			xml += "<table width=\"100%\">";
			xml +="<tr  width=\"50%\" >";
			xml +="<td font-size=\"10\" align=\"left\" ><b>Name</b><\/td>";
			xml +="<td font-size=\"10\" align=\"left\" >:&nbsp;<\/td>";
			xml +="<td font-size=\"10\" align=\"left\" ><b>"+nlapiEscapeXML(customerName/*custname*/)+"</b><\/td>";
			xml +="<\/tr>";
			xml +="<tr  width=\"50%\" >";
			xml +="<td font-size=\"10\" align=\"left\" >Address<\/td>";
			xml +="<td font-size=\"10\" align=\"left\" >:&nbsp;<\/td>";
			xml +="<td font-size=\"10\" align=\"left\" >"+nlapiEscapeXML(customer_bill_to_address_without_name_ship_to/*customeradd*/)+"<\/td>";
			xml +="<\/tr>";
			xml +="<tr  width=\"50%\" >";
			//xml +="<td font-size=\"10\" align=\"left\" >State<\/td>";
			xml +="<td font-size=\"10\" align=\"left\" >GSTIN<\/td>";
			xml +="<td font-size=\"10\" align=\"left\" >:&nbsp;<\/td>";
			//xml +="<td font-size=\"10\" align=\"left\" >"+nlapiEscapeXML(ship_state/*custState*/)+"<\/td>";
			xml +="<td font-size=\"10\" align=\"left\" >"+nlapiEscapeXML(ship_gstin/*cust_gstin*/)+"<\/td>";
			xml +="<\/tr>";
			xml +="<tr  width=\"50%\" >";
			xml +="<td  font-size=\"10\" align=\"left\" >State<\/td>";
			xml +="<td  font-size=\"10\" align=\"left\" >:&nbsp;<\/td>";
			xml +="<td  font-size=\"10\" align=\"left\" >"+nlapiEscapeXML(ship_state/*custState*/)+"<\/td>";
			//xml +="<td  font-size=\"9\" align=\"left\" style=\"border-left: 0.1px solid;border-top: 0.1px solid;\" >State Code<\/td>";
			//xml +="<td  font-size=\"9\" align=\"left\" style=\"border-top: 0.1px solid;\" >:&nbsp;<\/td>";
			//xml +="<td font-size=\"9\" align=\"left\" style=\"border-top: 0.1px solid;\" >"+nlapiEscapeXML(ship_state_code)+"<\/td>";
			xml +="<\/tr>";
			
			xml +="<tr  width=\"50%\" >";
			xml +="<td  width=\"30%\" font-size=\"9\" align=\"left\" >State Code<\/td>";
			xml +="<td  font-size=\"9\" align=\"left\"  >:&nbsp;<\/td>";
			xml +="<td font-size=\"9\" align=\"left\" >"+nlapiEscapeXML(ship_state_code)+"<\/td>";
			xml +="<\/tr>";
			/*xml +="<tr  width=\"50%\" >";
			xml +="<td width=\"25%\" font-size=\"10\" align=\"left\" >GSTIN No.<\/td>";
			xml +="<td font-size=\"10\" align=\"left\" >:&nbsp;<\/td>";
			xml +="<td font-size=\"10\" align=\"left\" >"+nlapiEscapeXML(ship_gstincust_gstin)+"<\/td>";
			xml +="<\/tr>";*/
			xml +="<\/table>";
			xml +="<\/td>";
			
			xml +="<\/tr>";				
		xml +="<\/table>";
//border-top: 0.1px  solid;
		xml += "<table width=\"100%\" style=\"border-left: 0.1px  solid;  border-right: 0.1px solid; border-bottom: 1px solid;\" >";
		xml += "	<tr >";
		xml += "	<td font-size=\"10\"><b>Project ID/Name :</b> "+ nlapiEscapeXML(companyID) +"/"+ nlapiEscapeXML(companyName) +"<\/td>";	
		xml += "	<\/tr>";
		xml += "<\/table>";
		
		xml += "<table border=\"0.1\" width=\"100%\">";
		xml +="<tr background-color=\"#C6DBF7\">";
		xml +="<td rowspan = \"1\" style=\"border-left: 0.1px solid;border-bottom: 0.1px solid;\" width=\"5%\" font-size=\"8\" align=\"center\" ><b>Sr.No</b><\/td>";
		//xml +="<td rowspan = \"2\" style=\"border-left: 0.1px solid;border-bottom: 0.1px solid;\" width=\"7%\" font-size=\"8\" align=\"center\">CBU Code<\/td>";
		xml +="<td rowspan = \"1\" style=\"border-left: 0.1px solid;border-bottom: 0.1px solid;\" width=\"15%\" font-size=\"8\" align=\"center\"><b>Name of Product/Service</b><\/td>";
		xml +="<td rowspan = \"1\" style=\"border-left: 0.1px solid;border-bottom: 0.1px solid;\" width=\"7%\" font-size=\"8\" align=\"center\"><b>HSN/SAC</b><\/td>";
		//xml +="<td rowspan = \"2\" style=\"border-left: 0.1px solid;border-bottom: 0.1px solid;\" width=\"7%\" font-size=\"8\" align=\"center\">UOM<\/td>";
		xml +="<td rowspan = \"1\" style=\"border-left: 0.1px solid;border-bottom: 0.1px solid;\" width=\"5%\" font-size=\"8\" align=\"center\"><b>Qty</b><\/td>";
		xml +="<td rowspan = \"1\" style=\"border-left: 0.1px solid;border-bottom: 0.1px solid;\" width=\"5%\" font-size=\"8\" align=\"center\"><b>Rate</b><\/td>";
		xml +="<td rowspan = \"1\" style=\"border-left: 0.1px solid;border-bottom: 0.1px solid;\" width=\"9%\" font-size=\"8\" align=\"center\"><b>Amount</b><\/td>";
		//xml +="<td rowspan = \"2\" style=\"border-left: 0.1px solid;border-bottom: 0.1px solid;\" width=\"5%\" font-size=\"8\" align=\"center\">Discount<\/td>";
		//xml +="<td rowspan = \"2\" style=\"border-left: 0.1px solid;border-bottom: 0.1px solid;\" width=\"10%\" font-size=\"8\" align=\"center\">Taxable Value<\/td>";
		xml +="<td colspan = \"2\" style=\"border-left: 0.1px solid;\" width=\"20%\" font-size=\"8\" align=\"center\"><b>CGST</b><\/td>";
		xml +="<td colspan = \"2\" style=\"border-left: 0.1px solid;\" width=\"20%\" font-size=\"8\" align=\"center\"><b>SGST</b><\/td>";
		xml +="<td colspan = \"2\" style=\"border-left: 0.1px solid;border-right: 0.1px solid;\" width=\"20%\" font-size=\"8\" align=\"center\"><b>IGST</b><\/td>";
		xml +="<\/tr>";
		xml +="<tr background-color=\"#C6DBF7\">";
		xml +="<td colspan = \"6\" style=\"border-left: 0.1px solid;border-top: 0.1px solid\" width=\"10%\" font-size=\"8\" align=\"center\"><\/td>";
		xml +="<td style=\"border-left: 0.1px solid;border-bottom: 0.1px solid;border-top: 0.1px solid;\" width=\"10%\" font-size=\"8\" align=\"center\">Rate<\/td>";
		xml +="<td style=\"border-left: 0.1px solid;border-top: 0.1px solid;\" width=\"10%\" font-size=\"8\" align=\"center\"><b>Amount</b><\/td>";
		xml +="<td style=\"border-left: 0.1px solid;border-top: 0.1px solid;\" width=\"10%\" font-size=\"8\" align=\"center\"><b>Rate</b><\/td>";
		xml +="<td style=\"border-left: 0.1px solid;border-top: 0.1px solid;\" width=\"10%\" font-size=\"8\" align=\"center\"><b>Amount</b><\/td>";
		xml +="<td style=\"border-left: 0.1px solid;border-top: 0.1px solid;\" width=\"10%\" font-size=\"8\" align=\"center\"><b>Rate</b><\/td>";
		xml +="<td style=\"border-left: 0.1px solid;border-top: 0.1px solid;border-right: 0.1px solid;\" width=\"10%\" font-size=\"8\" align=\"center\"><b>Amount</b><\/td>";
		xml +="<\/tr>";

		//==== Code added by Chaitali Gaikwad on 27/04/2017
		var filters = new Array();
		var columns = new Array();
		
		filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
		filters.push(new nlobjSearchFilter('custrecord_iit_ar_tranid', null, 'anyof', parseInt(invoiceId)));
	//	filters.push(new nlobjSearchFilter('custrecord_iit_ar_type', null, 'is', 'invoice'));
		
		filters.push(new nlobjSearchFilter('custrecord_iit_ar_type', null, 'is', invoiceType));
		
	//	columns.push(new nlobjSearchColumn("fxamount",null,null));
	//	columns.push(new nlobjSearchColumn("rate","taxItem",null));
		
		columns.push(new nlobjSearchColumn('custrecord_iit_ar_item'));
		columns.push(new nlobjSearchColumn('custrecord_iit_ar_taxcode'));
		columns.push(new nlobjSearchColumn('custrecord_iit_ar_taxrate1'));
		columns.push(new nlobjSearchColumn('custrecord_iit_ar_tax1amt'));
		columns.push(new nlobjSearchColumn('custrecord_iit_ar_grossamount'));
		
		
		var search_results = nlapiSearchRecord("customrecord_iit_sales_invoice",null, filters, columns);
		nlapiLogExecution('DEBUG', 'suitelet_generateInvoice', 'search_results =' + search_results);
		
		//var customrecord_iit_sales_invoiceSearch = nlapiSearchRecord("customrecord_iit_sales_invoice",null, filters, columns);

		/*var chk_item = search_results[0].getValue(columns[2]);
		nlapiLogExecution('DEBUG', 'suitelet_generateInvoice', 'chk_item =' + chk_item);
		*/
		
		
		//==== END : Code added by Chaitali Gaikwad on 27/04/2017
		var total_TaxableValue = 0.00;
		var total_discount = 0.00;
		var total_cgstAmt = 0.00;
		var total_sgstAmt = 0.00;
		var total_igstAmt = 0.00;
		for (var k = 1; k <= lineCount; k++) 
		{

			var itm = ObjRecord.getLineItemValue('item', 'item', k);
			if(_nullValidation(itm)){
				itm = '';
			}
			var itmqty = ObjRecord.getLineItemValue('item', 'quantity', k);
			if(_nullValidation(itmqty)){
				itmqty = '';
			}
			var itmdescription = ObjRecord.getLineItemValue('item', 'description', k);
			if(_nullValidation(itmdescription))
			{
				itmdescription = '';//itmdescription = ObjRecord.getLineItemText('item', 'item', k);
			}
			var itmrate = ObjRecord.getLineItemValue('item', 'rate', k);
			if(_nullValidation(itmrate)){
				itmrate = '';
			}
			var itmunits = ObjRecord.getLineItemValue('item', 'units', k);
			if(_nullValidation(itmunits)){
				itmunits = '';
			}
			var itmtaxamt = ObjRecord.getLineItemValue('item', 'tax1amt', k);
			if(_nullValidation(itmtaxamt)){
				itmtaxamt = '';
			}
			var itmamt = ObjRecord.getLineItemValue('item', 'amount', k);
			if(_nullValidation(itmamt)){
				itmamt = '';
			}
			var skucode = ObjRecord.getLineItemValue('item', 'olditemid', k);
			if(_nullValidation(skucode)){
				skucode = '';
			}
			var i_grossamt = ObjRecord.getLineItemValue('item', 'grossamt', k);
			if(_nullValidation(i_grossamt)){
				i_grossamt = '';
			}
			
			//==== Code added on 26-6-2017 to convert amount in INR currency
			if (i_Currency != i_INRcurrencyID) 
			{
				if(_logValidation(i_grossamt))
				{
					i_grossamt = (parseFloat(i_grossamt) * parseFloat(i_Exchangerate));
					nlapiLogExecution('DEBUG', 'Getting i_grossamt', 'i_grossamt = '+i_grossamt);
					i_grossamt = applySTRoundMethod(stRoundMethod, i_grossamt);
					nlapiLogExecution('DEBUG', 'Getting i_grossamt', 'i_grossamt = '+i_grossamt);
					
				}	
				if(_logValidation(itmamt))
				{
					itmamt = (parseFloat(itmamt) * parseFloat(i_Exchangerate));
					nlapiLogExecution('DEBUG', 'Getting itmamt', 'itmamt = '+itmamt);
					itmamt = applySTRoundMethod(stRoundMethod, itmamt);
					nlapiLogExecution('DEBUG', 'Getting itmamt', 'itmamt = '+itmamt);
					
				}
				
			}
			//==== END : Code added on 26-6-2017 to convert amount in INR currency

			var itmdiscount = ObjRecord.getLineItemValue('item', 'discount', k);
			if(_nullValidation(itmdiscount)){
				itmdiscount = 0.00;
			}
		
	
			//==== Code added by Chaitali Gaikwad on 27/04/2017
			
			var hsn_sac = ObjRecord.getLineItemValue('item', 'custcol_iit_transaction_hsn_sac', k);
			
			if(_nullValidation(hsn_sac))
			{
				hsn_sac = '';
			}
			
			var taxcode = ObjRecord.getLineItemValue('item', 'taxcode', k);
			taxcode = parseInt(taxcode);
			if(_nullValidation(taxcode))
			{
				taxcode = '';
			}
			nlapiLogExecution('DEBUG', 'suitelet_generateInvoice', 'taxcode =' + taxcode);
			
			var sgstrate = '0.0%'; 
			var sgstvalue = 0.00;//= '';
			var cgstrate = '0.0%'; 
			var cgstvalue = 0.00;//= '';
			var igstrate = '0.0%'; 
			var igstvalue = 0.00 ;//= '';
			
			var i_Count = 0;
			
			if(search_results){
					for (var i = 0 ; i < search_results.length ; i++)
					{
						var i_item_id = search_results[i].getValue('custrecord_iit_ar_item');
						var i_Gross_Amt = search_results[i].getValue('custrecord_iit_ar_grossamount');
						if(invoiceType == 'creditmemo')
						{
							i_Gross_Amt = parseFloat(i_Gross_Amt) * parseFloat(-1);
						}
						nlapiLogExecution('DEBUG', 'suitelet_generateInvoice', 'i_grossamt =' + i_grossamt);
						nlapiLogExecution('DEBUG', 'suitelet_generateInvoice', 'i_Gross_Amt =' + i_Gross_Amt);
						i_item_id = parseInt(i_item_id);
						// if : to check taxcode (on transaction) and item (column in search) 
						if((itm != i_item_id) && (i_Gross_Amt != i_grossamt))
						{
							nlapiLogExecution('DEBUG', 'suitelet_generateInvoice', 'item does not match' );
							
						}
						else if((itm == i_item_id) && (i_Gross_Amt == i_grossamt))
						{
							nlapiLogExecution('DEBUG', 'suitelet_generateInvoice', 'i =' + i);
							
							nlapiLogExecution('DEBUG', 'suitelet_generateInvoice', 'transaction_item =' + itm);
							nlapiLogExecution('DEBUG', 'suitelet_generateInvoice', 'search_item =' + i_item_id);
											
							var s_taxcode = search_results[i].getText('custrecord_iit_ar_taxcode');
							nlapiLogExecution('DEBUG', 'suitelet_generateInvoice', 's_taxcode =' + s_taxcode);
							
							var i_Filters = new Array();
							var i_Columns = new Array();
							
							i_Filters.push(new nlobjSearchFilter('taxgroup', null, 'anyof', taxcode));
							
							i_Columns.push(new nlobjSearchColumn('custrecord_iit_gst_sub_type').setSort(false));
							var salestaxitemSearch = nlapiSearchRecord("salestaxitem",null, i_Filters, i_Columns);
							nlapiLogExecution('DEBUG', 'suitelet_generateInvoice', 'salestaxitemSearch =' + salestaxitemSearch);
							
							if(_logValidation(salestaxitemSearch))
							{
								for (var j_Temp = i_Count ; j_Temp < salestaxitemSearch.length ; j_Temp++)
								{
									var s_Tax_Code = salestaxitemSearch[j_Temp].getText('custrecord_iit_gst_sub_type');
									nlapiLogExecution('DEBUG', 'suitelet_generateInvoice', 's_Tax_Code =' + s_Tax_Code);
									
									var sgst_index = s_Tax_Code.indexOf('SGST/UTGST');
									if(sgst_index >= 0)
									{
										var rate = search_results[i].getValue('custrecord_iit_ar_taxrate1');
										nlapiLogExecution('DEBUG', 'suitelet_generateInvoice', 'SGST rate =' + rate);
										sgstrate = rate;
										
										var i_amount = search_results[i].getValue('custrecord_iit_ar_tax1amt');
										nlapiLogExecution('DEBUG', 'suitelet_generateInvoice', 'SGST i_amount =' + i_amount);
										sgstvalue = parseFloat(i_amount);
										sgstvalue = (sgstvalue);
										if(invoiceType == 'creditmemo')
										{
											sgstvalue = parseFloat(sgstvalue) * parseFloat(-1);
										}
										i_Count++;
										break;
									}

									var cgst_index = s_Tax_Code.indexOf('CGST');
									if(cgst_index >= 0)
									{
										var rate = search_results[i].getValue('custrecord_iit_ar_taxrate1');
										nlapiLogExecution('DEBUG', 'suitelet_generateInvoice', 'CGST rate =' + rate);
										cgstrate = rate;
										
										var i_amount = search_results[i].getValue('custrecord_iit_ar_tax1amt');
										nlapiLogExecution('DEBUG', 'suitelet_generateInvoice', 'CGST i_amount =' + i_amount);
										cgstvalue = parseFloat(i_amount);
										nlapiLogExecution('DEBUG', 'suitelet_generateInvoice', 'CGST cgstvalue =' + cgstvalue);
										cgstvalue = (cgstvalue);
										if(invoiceType == 'creditmemo')
										{
											cgstvalue = parseFloat(cgstvalue) * parseFloat(-1);
										}
										i_Count++;
										break;
									}
									
									var igst_index = s_Tax_Code.indexOf('IGST');
									if(igst_index >= 0)
									{
										var rate = search_results[i].getValue('custrecord_iit_ar_taxrate1');
										nlapiLogExecution('DEBUG', 'suitelet_generateInvoice', 'IGST rate =' + rate);
										igstrate = rate;
										
										var i_amount = search_results[i].getValue('custrecord_iit_ar_tax1amt');
										nlapiLogExecution('DEBUG', 'suitelet_generateInvoice', 'IGST i_amount =' + i_amount);
										igstvalue = parseFloat(i_amount);
										igstvalue = (igstvalue);
										if(invoiceType == 'creditmemo')
										{
											igstvalue = parseFloat(igstvalue) * parseFloat(-1);
										}
										i_Count++;
										break;
									}
									
								}
							}
							
							
						}	
					}			
			}
			//==== END : Code added by Chaitali Gaikwad on 27/04/2017
			
			
			xml +="<tr style=\"border: 1px solid;\">";
			xml +="<td style=\"border-bottom: 0.1px solid;\" width=\"5%\" font-size=\"8\" align=\"center\">"+nlapiEscapeXML(k)+"<\/td>";
			//xml +="<td style=\"border-left: 0.1px solid;\" width=\"7%\" font-size=\"8\" align=\"center\">"+nlapiEscapeXML(''/*itmCbuCode*/)+"<\/td>";
			xml +="<td style=\"border-bottom: 0.1px solid;border-left: 0.1px solid;\" width=\"15%\" font-size=\"8\" align=\"center\">"+nlapiEscapeXML(itmdescription)+"<\/td>";
			xml +="<td style=\"border-bottom: 0.1px solid;border-left: 0.1px solid;\" width=\"7%\" font-size=\"8\" align=\"center\">"+nlapiEscapeXML(hsn_sac/*itmSacCode*/)+"<\/td>";
			xml +="<td style=\"border-bottom: 0.1px solid;border-left: 0.1px solid;\" width=\"7%\" font-size=\"8\" align=\"center\">"+nlapiEscapeXML(itmqty)+"<\/td>";
			//xml +="<td style=\"border-bottom: 0.1px solid;border-left: 0.1px solid;\" width=\"5%\" font-size=\"8\" align=\"center\">"+nlapiEscapeXML(itmunits)+"<\/td>";
			xml +="<td style=\"border-bottom: 0.1px solid;border-left: 0.1px solid;\" width=\"5%\" font-size=\"8\" align=\"center\">"+nlapiEscapeXML(itmrate)+"<\/td>";
			xml +="<td style=\"border-bottom: 0.1px solid;border-left: 0.1px solid;\" width=\"9%\" font-size=\"8\" align=\"right\">"+nlapiEscapeXML(parseFloat(itmamt).toFixed(2))+"<\/td>";
			//xml +="<td style=\"border-bottom: 0.1px solid;border-left: 0.1px solid;\" width=\"5%\" font-size=\"8\" align=\"right\">"+nlapiEscapeXML(itmdiscount)+"<\/td>";
			//xml +="<td style=\"border-bottom: 0.1px solid;border-left: 0.1px solid;\" width=\"10%\" font-size=\"8\" align=\"right\">"+nlapiEscapeXML(parseFloat((itmamt)).toFixed(2))+"<\/td>";
			xml +="<td style=\"border-left: 0.1px solid;border-top: 0.1px solid;\" width=\"10%\" font-size=\"8\" align=\"right\">"+nlapiEscapeXML(cgstrate)+"<\/td>";
			xml +="<td style=\"border-left: 0.1px solid;border-top: 0.1px solid;\" width=\"10%\" font-size=\"8\" align=\"right\">"+nlapiEscapeXML(parseFloat(cgstvalue).toFixed(2))+"<\/td>";
			xml +="<td style=\"border-left: 0.1px solid;border-top: 0.1px solid;\" width=\"10%\" font-size=\"8\" align=\"right\">"+nlapiEscapeXML(sgstrate)+"<\/td>";
			xml +="<td style=\"border-left: 0.1px solid;border-top: 0.1px solid;\" width=\"10%\" font-size=\"8\" align=\"right\">"+nlapiEscapeXML(parseFloat(sgstvalue).toFixed(2))+"<\/td>";
			xml +="<td style=\"border-left: 0.1px solid;border-top: 0.1px solid;border-right: 1px solid;\" width=\"10%\" font-size=\"8\" align=\"right\">"+nlapiEscapeXML(igstrate)+"<\/td>";
			xml +="<td style=\"border-left: 0.1px solid;border-top: 0.1px solid;border-right: 1px solid;\" width=\"10%\" font-size=\"8\" align=\"right\">"+nlapiEscapeXML(parseFloat(igstvalue).toFixed(2))+"<\/td>";
			xml +="<\/tr>";

			nlapiLogExecution('DEBUG', 'suitelet_generateBill','itemList =' + itm+" "+itmqty+" "+itmdescription);
			
			total_TaxableValue = parseFloat(total_TaxableValue) + parseFloat(itmamt) ;
			total_TaxableValue = (total_TaxableValue);
			nlapiLogExecution('DEBUG', 'suitelet_generateBill','total_TaxableValue =' + total_TaxableValue);
			
			total_discount = parseFloat(total_discount) + parseFloat(itmdiscount) ;
			total_discount = (total_discount);
			nlapiLogExecution('DEBUG', 'suitelet_generateBill','total_discount =' + total_discount);
			
			total_cgstAmt = parseFloat(total_cgstAmt) + parseFloat(cgstvalue) ;
			total_cgstAmt = (total_cgstAmt);
			nlapiLogExecution('DEBUG', 'suitelet_generateBill','total_cgstAmt =' + total_cgstAmt);
			
			total_sgstAmt = parseFloat(total_sgstAmt) + parseFloat(sgstvalue) ;
			total_sgstAmt = (total_sgstAmt);
			nlapiLogExecution('DEBUG', 'suitelet_generateBill','total_sgstAmt =' + total_sgstAmt);
			
			total_igstAmt = parseFloat(total_igstAmt) + parseFloat(igstvalue) ;
			total_igstAmt = (total_igstAmt);
			nlapiLogExecution('DEBUG', 'suitelet_generateBill','total_igstAmt =' + total_igstAmt);
			
		}
		var GST_total = parseFloat(total_cgstAmt) + parseFloat(total_sgstAmt)+parseFloat(total_igstAmt);
		nlapiLogExecution('DEBUG', 'suitelet_generateBill','GST_total =' + GST_total);
		
		xml +="<tr background-color=\"#C6DBF7\" style=\"border-bottom: 0.1px solid;\">";
		xml +="<td font-size=\"8\" style=\"border-left: 1px solid;\" align=\"center\"><\/td>";
		xml +="<td font-size=\"8\" style=\"border-left: 1px solid;\" align=\"center\"><\/td>";
		xml +="<td font-size=\"8\" align=\"center\"><\/td>";
		xml +="<td colspan = \"2\" style=\"border-left: 0.1px solid;\" font-size=\"8\" align=\"left\">Total <\/td>";
		xml +="<td font-size=\"8\" style=\"border-left: 0.1px solid;\" align=\"right\"> "+nlapiEscapeXML(parseFloat((total_TaxableValue)).toFixed(2))+"<\/td>";
		//xml +="<td font-size=\"8\" style=\"border-left: 0.1px solid;\" align=\"right\">"+nlapiEscapeXML(parseFloat(total_discount).toFixed(2))+"<\/td>";
		//xml +="<td font-size=\"8\" style=\"border-left: 0.1px solid;\" align=\"right\">"+nlapiEscapeXML(parseFloat(total_TaxableValue).toFixed(2))+"<\/td>";
		xml +="<td colspan = \"2\" font-size=\"8\" style=\"border-top: 0.1px solid;border-left: 0.1px solid;\" align=\"right\">"+nlapiEscapeXML(parseFloat(total_cgstAmt).toFixed(2))+"<\/td>";
		xml +="<td colspan = \"2\" font-size=\"8\" style=\"border-top: 0.1px solid;border-left: 0.1px solid;\" align=\"right\">"+nlapiEscapeXML(parseFloat(total_sgstAmt).toFixed(2))+"<\/td>";
		xml +="<td colspan = \"2\" font-size=\"8\" style=\"border-top: 0.1px solid;border-left: 0.1px solid;\" align=\"right\">"+nlapiEscapeXML(parseFloat(total_igstAmt).toFixed(2))+"<\/td>";
		//xml +="<td font-size=\"8\" style=\"border-top: 0.1px solid;\" align=\"center\"><\/td>";
		xml +="<\/tr>";
		//================================================================================================================================================	
		var lineCount = ObjRecord.getLineItemCount('item');
		
		
			var s_hsnCategory  = '';
			
		var arr_hsn_sac = new Array();
		for(cat = 1; cat<=lineCount;cat++)
		{
			nlapiLogExecution('DEBUG','lineCount',lineCount);
			var i_item = ObjRecord.getLineItemValue('item', 'item', cat);
			var hsn_sac = ObjRecord.getLineItemValue('item', 'custcol_iit_transaction_hsn_sac', cat);
			
			
			
			nlapiLogExecution('DEBUG','i_item',i_item);
			var itemSearch = nlapiSearchRecord("item",null,
			[
			   ["internalidnumber","equalto",i_item]
			], 
			[
			   new nlobjSearchColumn("custitem_iit_hsn_category",null,null)
			]
			);
			if(_logValidation(itemSearch))
			{
				s_hsnCategory = itemSearch[0].getText('custitem_iit_hsn_category');
			}
			if(_nullValidation(s_hsnCategory))
			{
				s_hsnCategory = '';
			}
			if(_nullValidation(hsn_sac))
			{
				hsn_sac = '';
			}
			nlapiLogExecution('DEBUG','arr_hsn_sac',arr_hsn_sac);
			nlapiLogExecution('DEBUG','arr_hsn_sac.indexOf(hsn_sac)',arr_hsn_sac.indexOf(hsn_sac));
			if(_logValidation(hsn_sac)){
			if(arr_hsn_sac.indexOf(hsn_sac) == -1){
			arr_hsn_sac.push(hsn_sac);
			nlapiLogExecution('DEBUG','CATEGORY=',s_hsnCategory);
			xml +="<tr>";
			xml +="<td style=\"border-bottom: 0.1px solid;\" width=\"5%\" font-size=\"8\" colspan = \"12\"><b>"+nlapiEscapeXML(hsn_sac/*itmSacCode*/)+" "+nlapiEscapeXML(s_hsnCategory)+"<\/b><\/td>";
			xml +="<\/tr>";
			}
		  }
		}
		//================================================================================================================================================
		xml +="<\/table>";
		
		//border-top: 0.1px  solid;
		xml += "<table width=\"100%\" style=\"border-left: 0.1px  solid;  border-right: 0.1px solid;border-bottom: 0.1px solid;\" >";
		//xml += "	<tr>";
		//xml += "	<td>&nbsp;<\/td>";
	
		//xml += "	<\/tr>";
		xml += "<\/table>";
		

		xml +="<table width=\"100%\" align=\"right\" style=\"border-left: 0.1px  solid;  border-right: 0.1px solid;border-bottom: 0.1px solid;\">";
		xml +="<tr>";
		xml +="<td width=\"60%\" font-size=\"8\" rowspan=\"2\" style=\"border-right: 0.1px solid;\" align=\"center\"><b>Total Invoice Amount in Words:</b><\/td>";
		xml +="<td width=\"25%\" font-size=\"8\"  style=\"border-bottom: 0.1px solid;border-top: 0.1px solid;border-left: 0.1px solid;\" align=\"left\" background-color=\"#C6DBF7\" ><b> Total  Amount before Tax:</b><\/td>";
		xml +="<td width=\"15%\" font-size=\"8\"  style=\"border-right: 0.1px solid;border-left: 0.1px solid; border-bottom: 0.1px solid;border-top: 0.1px solid;\" align=\"right\" background-color=\"#C6DBF7\"><b>"+nlapiEscapeXML(parseFloat(total_TaxableValue).toFixed(2))+"<\/b><\/td>";
		xml +="<\/tr>";
		xml +="<tr>";
		xml +="<td  font-size=\"8\"  style=\"border-bottom: 0.1px solid;\" align=\"left\"> Add : CGST @ 9%<\/td>";
		xml +="<td  font-size=\"8\"  style=\"border-left: 0.1px solid; border-bottom: 0.1px solid;\" align=\"right\">"+nlapiEscapeXML(parseFloat(total_cgstAmt).toFixed(2))+"<\/td>";
		xml +="<\/tr>";
		xml +="<tr>";
		xml +="<td width=\"60%\" font-size=\"10\" rowspan=\"4\" style=\"border-right: 0.1px solid;\" align=\"center\">"+nlapiEscapeXML(amtstr)+"<\/td>";
		xml +="<td   font-size=\"8\"  style=\"border-bottom: 0.1px solid;\" align=\"left\"> Add : SGST @ 9%<\/td>";
		xml +="<td   font-size=\"8\"  style=\"border-left: 0.1px solid; border-bottom: 0.1px solid;\" align=\"right\">"+nlapiEscapeXML(parseFloat(total_sgstAmt).toFixed(2))+"<\/td>";
		xml +="<\/tr>";
		xml +="<tr>";
		xml +="<td  font-size=\"8\"  style=\"border-bottom: 0.1px solid;\" align=\"left\"  > Add : IGST @ 18%<\/td>";
		xml +="<td  font-size=\"8\"  style=\"border-left: 0.1px solid; border-bottom: 0.1px solid;\" align=\"right\">"+nlapiEscapeXML(parseFloat(total_igstAmt).toFixed(2))+"<\/td>";
		xml +="<\/tr>";
		xml +="<tr>";
		xml +="<td  font-size=\"8\"  style=\"border-bottom: 0.1px solid;border-left: 0.1px solid;border-top: 0.1px solid;\" align=\"left\" background-color=\"#C6DBF7\" ><b> Tax Amount&nbsp;:&nbsp;GST</b><\/td>";
		xml +="<td  font-size=\"8\" background-color=\"#C6DBF7\"   style=\"border-left: 0.1px solid; border-bottom: 0.1px solid;border-top: 0.1px solid;border-right: 0.1px solid;\" align=\"right\"><b>"+nlapiEscapeXML(parseFloat(GST_total).toFixed(2))+"<\/b><\/td>";
		xml +="<\/tr>";
		xml +="<tr>";
		xml +="<td  font-size=\"8\" style=\"border-bottom: 0.1px solid;border-left: 0.1px solid;border-top: 0.1px solid\" align=\"left\" background-color=\"#C6DBF7\" ><b> Total Amount after Tax:</b><\/td>";
		xml +="<td  font-size=\"8\" background-color=\"#C6DBF7\"   style=\"border-left: 0.1px solid;border-top: 0.1px solid;border-bottom: 0.1px solid;border-right: 0.1px solid;\" align=\"right\"><b>"+nlapiEscapeXML(parseFloat((total)).toFixed(2))+"<\/b><\/td>";
		xml +="<\/tr>";
		xml +="<\/table>";
		
		
		xml +="<table width=\"100%\" style=\"border-left: 0.1px solid;border-right: 0.1px solid;border-bottom: 0.1px solid;\">";
		xml +="<tr>";
		
		xml +="<td width=\"50%\" style=\"border-right: 0.1px solid;\">";
		xml +="<table width=\"100%\">";
		xml +="<tr>";
		xml +="<td width=\"50%\" font-size=\"10\" align=\"center\"><b>Bank Details<\/b><\/td>";
		xml +="<\/tr>";
		xml +="<tr style=\"border-bottom: 0.1px solid;\">";
		xml +="<td font-family=\"Helvetica\" width=\"60%\" font-size=\"8\" align=\"left\">"; 
		xml +="<ul>";
		xml +="<li><b>Bank Name<\/b>:Kotak Mahindra Bank Ltd<\/li>"; 
		xml +="<li><b>Bank Account No<\/b>&nbsp;: 1511696478<\/li>"; 
		var bank_ifsc_str = 'Bank Branch IFSC';
		xml +="<li><b>"+bank_ifsc_str+"<\/b>: KKBK0000433 <\/li>";
		xml +="<\/ul><\/td>";
		//xml +="<tr width=\"40%\"><td> <\/td>";
		xml +="<\/tr>";
		xml +="<tr>";
		xml +="<td width=\"50%\" font-size=\"9\" align=\"center\"><b>Terms of Condition<\/b><\/td>";
		xml +="<\/tr>";
		xml +="<tr >";
		xml +="<td  font-family=\"Helvetica\" font-size=\"8\">";
		xml += "<p class='go-right' style='margin-left:10px;'>1)&nbsp;&nbsp;Payment Terms :&nbsp;"+i_paymentterms+"&nbsp;&nbsp;Days <br\/>"
				       +"2) Interest @18% will be charged on the bill if not paid with in due date<br\/>"
				       +"3) All disputes subject to Bangalore Jurisdiction only.</p>";
		xml +="<\/td>";
		xml +="<\/tr>";
		xml +="<\/table>";
		xml +="<\/td>";
		
		xml +="<td width=\"50%\" style=\"border-right: 0.1px solid;\">";
		xml +="<table align=\"center\" margin-top=\"10px\">";
		if(qr_code)
		{
		xml +="<tr><td width=\"100%\" align=\"center\" > <img width=\"120\" height=\"120\"   src= '" + url + "'/><\/td>";
		}
		else
		{
			xml +="<tr><td width=\"100%\" align=\"center\" ><\/td>";
		}
		//xml +="<td align=\"left\" width=\"30%\"><br><\/br>Name of the Signatory<\/td>";
		xml +="<\/tr>";
		xml +="<\/table>";
		xml +="<\/td>";
		
		xml +="<td width=\"55%\">";
		xml +="<table width=\"100%\" align=\"right\">";
		/*xml +="<tr style=\"border-bottom: 0.1px solid;\" background-color=\"#C6DBF7\" >";
		xml +="<td width=\"80%\" align=\"left\" font-size=\"8\"  >Total Amount after Tax:<\/td>";
		xml +="<td width=\"40%\" font-size=\"8\"  style=\"border-left: 0.1px solid;\" align=\"right\">"+nlapiEscapeXML(parseFloat((total)).toFixed(2))+"<\/td>";
		xml +="<\/tr>";*/
		/*xml +="<tr style=\"border-bottom: 0.1px solid;\">";
		xml +="<td font-size=\"8\" colspan = \"2\">&nbsp;<\/td>";
		//xml +="<td style=\"border-bottom: 0.1px solid;\" > <\/td>";
		xml +="<\/tr>";*/
		/*xml +="<tr style=\"border-bottom: 0.1px solid;\">";
		xml +="<td  font-size=\"8\" colspan = \"2\">GST Payable on Reverse Charge <\/td>";		
		xml +="<\/tr>";*/
		xml +="<tr>";
		//colspan = \"2\"
       xml +="<td align=\"left\" colspan = \"2\" font-size=\"8\"  style=\"border-bottom: 0.1px solid;\"  > Declaration: We declare that this invoice shows the actual price of the goods/services  described and that all particulars are true correct. <\/td>";
       xml +="<\/tr>";
	/*	xml +="<tr style=\"border-bottom: 0.1px solid;\">";
		 xml +="<td align=\"left\"  font-size=\"6\" colspan = \"1\">  described and that all particulars are true&correct <\/td>";		
		xml +="<\/tr>";*/
			
		xml +="<tr>";
		//colspan = \"2\"
		xml +="<td align=\"center\" colspan = \"2\" font-size=\"12\" > <b>For Brillio Technologies Pvt. Ltd<\/b> <br\/><br\/><br\/><br\/><br\/><br\/><\/td>";
		xml +="<\/tr>";
		xml +="<tr>";
		//colspan = \"2\"
		xml +="<td align=\"center\" colspan = \"2\" font-size=\"8\" ><\/td>";
		xml +="<\/tr>";
		xml +="<tr>";
		//colspan = \"2\"
		xml +="<td align=\"center\" colspan = \"2\" font-size=\"8\" ><b>Authorised Signatory<\/b><\/td>";
		xml +="<\/tr>";
		xml +="<\/table>";
		xml +="<\/td>";		
		xml +="<\/tr>";		
		xml += "<\/table>";
		
		xml +="<table width=\"100%\" style=\"border-left: 0.1px solid;border-right: 0.1px solid;border-bottom: 0.1px solid;\">";
		xml +="<tr>";
		//xml +="<td align=\"center\" font-size=\"7\"><b>&nbsp;&nbsp;&nbsp;&nbsp;Registered Office:Brillio Technologies Pvt.Ltd, No. 58, 1st Main Road, Mini Forest, J P Nagar 3rd Phase, Bangalore 560 078</b><\/td>";
		xml +="<td align=\"center\" font-size=\"7\"><b>&nbsp;&nbsp;&nbsp;&nbsp;Registered Office:Brillio Technologies Pvt.Ltd,No. 8/2, 4th Floor, Bren Optimus, Dr. M H Marigowda Road, Bangalore, Bengaluru (Bangalore) Urban,Karnataka, 560029</b><\/td>";
		//xml +="<\/td>";
		xml +="<\/tr>";
		xml += "<\/table><\/body>\n</pdf>";
		
		var file = nlapiXMLToPDF(xml);
		response.setContentType('PDF', 'Invoice.pdf', 'inline');
		response.write(file.getValue());

	}
		catch(e)
		{
			nlapiLogExecution('DEBUG', 'suitelet_ew_invoice', 'exception= '+e);
		}



}


function _logValidation(value)
{
	if(value != null && value != '' && value != undefined && value != 'undefined' && value != 'NaN' && value != ' ')
	{
		return true;
	}
	else
	{
		return false;
	}
}


function _nullValidation(value){
	 if (value == null || value == undefined || value == '')
	    {
	        return true;
	    }
	    else
	    {
	        return false;
	    }
}
function testSkill(test)
{
	var numReversed, inWords, actnumber, i, j;
    var junkVal = test;
    junkVal = Math.floor(junkVal);
    nlapiLogExecution('DEBUG', 'junkVal', junkVal);
    var obStr = junkVal.toString();
    nlapiLogExecution('DEBUG', 'obStr', obStr);
    numReversed = obStr.split('');
    nlapiLogExecution('DEBUG', 'numReversed', numReversed);
    actnumber = numReversed.reverse();
    nlapiLogExecution('DEBUG', 'actnumber', actnumber);

    if (Number(junkVal) >= 0) {
        //do nothing
    } else {
        return 'wrong Number cannot be converted';
    }
    if (Number(junkVal) === 0) {
        return 'Rupees Two Thousand Three Hundred Sixty Only Zero Only';
    }
    if (actnumber.length > 9) {
        return 'Oops!!!! the Number is too big to covertes';
    }
    var iWords = ['Zero', ' One', ' Two', ' Three', ' Four', ' Five', ' Six', ' Seven', ' Eight', ' Nine'];
    var ePlace = ['Ten', ' Eleven', ' Twelve', ' Thirteen', ' Fourteen', ' Fifteen', ' Sixteen', ' Seventeen', ' Eighteen', ' Nineteen'];
    var tensPlace = ['', ' Ten', ' Twenty', ' Thirty', ' Forty', ' Fifty', ' Sixty', ' Seventy', ' Eighty', ' Ninety'];
    var inWords = [];


    var iWordsLength = numReversed.length;
    nlapiLogExecution('DEBUG', 'iWordsLength', iWordsLength);
    var finalWord = '';
    j = 0;
    for (var i = 0; i < iWordsLength; i++) {
        switch (i) {
            case 0:
                if (actnumber[i] === '0' || actnumber[i + 1] === '1') {
                    inWords[j] = '';
                    nlapiLogExecution('DEBUG', 'actnumber[i]', actnumber[i]);
                } else {
                    inWords[j] = iWords[actnumber[i]];
                    nlapiLogExecution('DEBUG', 'inWords[j]', inWords[j]);
                }
                inWords[j] = inWords[j];//+ ' Only';
                nlapiLogExecution('DEBUG', 'inWords[j]', inWords[j]);
                break;
            case 1:
                tensComplication();
                break;
            case 2:
                if (actnumber[i] === '0') {
                    inWords[j] = '';
                } else if (actnumber[i - 1] !== '0' && actnumber[i - 2] !== '0') {
                    inWords[j] = iWords[actnumber[i]] + ' Hundred';
                } else {
                    inWords[j] = iWords[actnumber[i]] + ' Hundred';
                }
                break;
            case 3:
                if (actnumber[i] === '0' || actnumber[i + 1] === '1') {
                    inWords[j] = '';
                } else {
                    inWords[j] = iWords[actnumber[i]];
                }
                if (actnumber[i + 1] !== '0' || actnumber[i] > '0') {
                    inWords[j] = inWords[j] + ' Thousand';
                }
                break;
            case 4:
                tensComplication();
                break;
            case 5:
                if (actnumber[i] === '0' || actnumber[i + 1] === '1') {
                    inWords[j] = '';
                } else {
                    inWords[j] = iWords[actnumber[i]];
                }
                if (actnumber[i + 1] !== '0' || actnumber[i] > '0') {
                    inWords[j] = inWords[j] + ' Lakh';
                }
                break;
            case 6:
                tensComplication();
                break;
            case 7:
                if (actnumber[i] === '0' || actnumber[i + 1] === '1') {
                    inWords[j] = '';
                } else {
                    inWords[j] = iWords[actnumber[i]];
                }
                inWords[j] = inWords[j] + ' Crore';
                break;
            case 8:
                tensComplication();
                break;
            default:
                break;
        }
        j++;
    }
    function tensComplication() {
        if(actnumber[i]==0) {
            inWords[j]='';
        }
        else if(actnumber[i]==1) {
            inWords[j]=ePlace[actnumber[i-1]];
        }
        else {
            inWords[j]=tensPlace[actnumber[i]];
        }
    }

    inWords.reverse();
    for (var k = 0; k < inWords.length; k++) {
        finalWord += inWords[k];
    }
    return finalWord;
}

function checklastCharacter(str){
    if (_logValidation(str)) {
        str = str.toString();
        var stringWithoutLastComma = '';
        nlapiLogExecution('DEBUG', 'afterSubmitRecord', 'str =' + str);

        if (str.substring(str.length - 2) == '& ') {
            stringWithoutLastComma = str.substring(0, str.length - 2);
        }
        else {
            stringWithoutLastComma = str;
        }
        nlapiLogExecution('DEBUG', 'afterSubmitRecord', 'stringWithoutLastComma =' + stringWithoutLastComma);
        return stringWithoutLastComma;
    }
    else {
        return '';
    }
}
function inrFormat(nStr) { // nStr is the input string
    nStr += '';
    x = nStr.split('.');
    x1 = x[0];
    x2 = x.length > 1 ? '.' + x[1] : '';
    var rgx = /(\d+)(\d{3})/;
    var z = 0;
    var len = String(x1).length;
    var num = parseInt((len/2)-1);

     while (rgx.test(x1))
     {
       if(z > 0)
       {
         x1 = x1.replace(rgx, '$1' + ',' + '$2');
       }
       else
       {
         x1 = x1.replace(rgx, '$1' + ',' + '$2');
         rgx = /(\d+)(\d{2})/;
       }
       z++;
       num--;
       if(num == 0)
       {
         break;
       }
     }
    return x1 + x2;
}

var th = ['', 'Thousand ', 'Million ', 'Billion ', 'Trillion '];
//uncomment this line for English Number System
//var th = ['','thousand','million', 'milliard','billion'];

var dg = ['Zero', 'One', 'Two', 'Three', 'Four', 'Five', 'Six', 'Seven', 'Eight', 'Nine'];
var tn = ['Ten', 'Eleven', 'Twelve', 'Thirteen', 'Fourteen', 'Fifteen', 'Sixteen', 'Seventeen', 'Eighteen', 'Nineteen'];
var tw = ['Twenty', 'Thirty', 'Forty', 'Fifty', 'Sixty', 'Seventy', 'Eighty', 'Ninety'];

function toWords(s){
    s = s.toString();
    s = s.replace(/[\, ]/g, '');
    if (s != parseFloat(s))
        return 'not a number';
    var x = s.indexOf('.');
    if (x == -1)
        x = s.length;
    if (x > 15)
        return 'too big';
    var n = s.split('');
    var str = '';
    var sk = 0;
    for (var i = 0; i < x; i++) {
        if ((x - i) % 3 == 2) {
            if (n[i] == '1') {
                str += tn[Number(n[i + 1])] + ' ';
                i++;
                sk = 1;
            }
            else
                if (n[i] != 0) {
                    str += tw[n[i] - 2] + ' ';
                    sk = 1;
                }
        }
        else
            if (n[i] != 0) {
                str += dg[n[i]] + ' ';
                if ((x - i) % 3 == 0)
                    str += 'Hundred ';
                sk = 1;
            }
        if ((x - i) % 3 == 1) {
            if (sk)
                str += th[(x - i - 1) / 3] + ' ';
            sk = 0;
        }
    }
    if (x != s.length) {
        var y = s.length;
    }
    return str.replace(/\s+/g, ' ');
}

function applySTRoundMethod(stRoundMethod,taxamount)
{
	var roundedSTAmount = taxamount;
	
	if(stRoundMethod == 2)
	{
		roundedSTAmount = (taxamount);
	}
	if(stRoundMethod == 3)
	{
		roundedSTAmount = (taxamount/10)*10;
	}
	if(stRoundMethod == 4)
	{
		roundedSTAmount = (taxamount/100)*100;
	}
	
	return roundedSTAmount;
}

function SearchGlobalParameter()	
{
    var a_filters = new Array();
    var a_column = new Array();
    var i_globalRecId = 0;
    
    a_column.push(new nlobjSearchColumn('internalid'));
    
    var s_serchResult = nlapiSearchRecord('customrecord_indian_tax_glo_parameter', null, null, a_column);
    
    if (s_serchResult != null && s_serchResult != undefined && s_serchResult != '') 
	{
        for (var i = 0; i < s_serchResult.length; i++) 
		{
            i_globalRecId = s_serchResult[0].getValue('internalid');
            nlapiLogExecution('DEBUG', 'Bill ', "i_globalRecId" + i_globalRecId);
            
        }
        
    }
    
    
    return i_globalRecId;
}