/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       05 Aug 2021     shravan.k
 *
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType
 *   
 * @returns {Boolean} True to continue save, false to abort save
 */
function Validate_Task_Save_Record()
{
	var arr_activityNames = [ "Project Activites", "Project activites",
						"Project activities", "project activities",
						"Bench Task","Bench" ,"Project Activities-Offsite",
						"Project Activities-Onsite", "Operations",
						"Project Activities", "Project activity",
						"project activity", "Project Activity","Internal Project","Bench Activities","Leave","Holiday","OT"];

	var s_title = nlapiGetFieldValue('title');
	//alert('s_title=='+s_title);
	var boo_Milestone = nlapiGetFieldValue('ismilestone');
	//alert('boo_Milestone=='+boo_Milestone);
	if(boo_Milestone == 'F')
		{
			var i_Validate_index = arr_activityNames.indexOf(s_title);
			//alert('i_Validate_index=='+i_Validate_index);
			i_Validate_index = parseInt(i_Validate_index);
			if(i_Validate_index < 0)
				{
					var s_message = 'Please check the Project task Naming, according to Standard the naming should have any of these : '
						s_message += ''+ JSON.stringify(arr_activityNames);
					alert(s_message);
					return false;
				} //// if(i_Validate_index < 0)
		
		
		} ///// if(i_Assignee_Count > 0)
	else
		{
			alert('This is Milestone Task and will not be reflecting on Timsheets');
		} //// else of if(i_Assignee_Count > 0)
    return true;
} ///// function Validate_Task_Save_Record()
