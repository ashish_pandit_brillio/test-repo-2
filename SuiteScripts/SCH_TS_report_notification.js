function SCH_TS_report_notification()//
{
    nlapiLogExecution('DEBUG', 'SCH_TS_report_notification', 'Into Scheduled script...');
    
    var context = nlapiGetContext();
    var usageRemaining = context.getRemainingUsage();
    
    var counter = context.getSetting('SCRIPT', 'custscript_counter_tm');
    nlapiLogExecution('DEBUG', 'SCH_TS_report_notification', 'counter : ' + counter);
    
    try //
    {
        var EmpList = ['jayanths@brillio.com', 'bala@brillio.com', 'tina.vas@brillio.com', 'santhosh.mukundan@brillio.com', 'atul.kumar@brillio.com', 'jeff.wallace@brillio.com', 'mayank.pant@brillio.com', 'jay.nair@brillio.com', 'lakshminarayanann@brillio.com', 'rajat.das@brillio.com', 'sandip.sinha@brillio.com', 'sridhark@brillio.com', 'mosesrajr@brillio.com', 'anand.gupta@brillio.com', 'Srinivasanl@brillio.com', 'karthikeyan@brillio.com', 'rohit.nand@brillio.com', 'vinod.subramanyam@brillio.com', 'anilnair@brillio.com', 'bipinkr@brillio.com', 'danish@brillio.com', 'nitin.dixit@brillio.com', 'sreejith.pillai@brillio.com', 'haribj@brillio.com', 'deepako@brillio.com', 'chanderd@brillio.com', 'rishika@brillio.com', 'debasish.mukherjee@brillio.com', 'jitheshpr@brillio.com', 'jai.kumar@collabera.com', 'ChetanS@brillio.com', 'sajee@brillio.com', 'sumitsr@brillio.com', 'bindu@brillio.com', 'gaurav.shah@brillio.com', 'gopalkrishnab@brillio.com', 'sharathk@brillio.com', 'janardhanm@brillio.com', 'ChockalingamT@brillio.com', 'ImranA@brillio.com', 'hemavathij@brillio.com', 'saisv@brillio.com', 'priyas@brillio.com', 'vasanth.menon@brillio.com', 'nileshpb@brillio.com', 'vishnubrc@brillio.com', 'ramesh.asati@brillio.com', 'jayanta.banerjee@brillio.com', 'vinaynk@brillio.com', 'sirishus@brillio.com', 'vamsikb@brillio.com', 'nagarajgk@brillio.com', 'vinoj.jacob@brillio.com', 'sonali.soni@brillio.com', 'amitkv@brillio.com', 'suryarg@brillio.com', 'roopa.mukundan@brillio.com', 'prakash.bisht@brillio.com', 'malathi.s@brillio.com', 'satyanarayan.kb@brillio.com', 'kiran.susarla@brillio.com', 'aniji@brillio.com', 'gireesh@brillio.com', 'chenthil@brillio.com', 'chella.s@brillio.com', 'asokan.s@brillio.com', 'murali.iyer@brillio.com', 'srinivasan.j@brillio.com', 'sudheero@brillio.com', 'babulalk@brillio.com', 'srinivasmurthys@brillio.com', 'subbaraok@brillio.com', 'durgaps@brillio.com', 'prabhudevas@brillio.com', 'smithat@brillio.com', 'arunkpa@brillio.com', 'pramodyg@brillio.com', 'swarupcs@brillio.com', 'mannemsr@brillio.com', 'sandeep.bhattacharje@brillio.com', 'josgt@brillio.com', 'naveen.sasi@brillio.com', 'baburajang@brillio.com', 'viswavm@brillio.com', 'alfred.manuel@brillio.com', 'swetha.thota@brillio.com', 'Jayachandran.S@brillio.com', 'vinu.gopal@brillio.com', 'sabu.samuel@brillio.com', 'BikashCB@brillio.com', 'rohitkr@brillio.com', 'jagadish.garimella@brillio.com', 'manoj.nair@brillio.com', 'sreenath.s@brillio.com', 'anoop.das@brillio.com', 'SunilMD@brillio.com', 'brittany.hoed@brillio.com', 'akash.jain@brillio.com', 'ruja.patel@brillio.com', 'sahara.alexis@brillio.com', 'anandak@brillio.com', 'anantharamv@brillio.com', 'hemakumars@brillio.com', 'sivakumarr@brillio.com', 'tejasvi.mohan@brillio.com', 'chandrasekarp@brillio.com', 'abhilash.nair@brillio.com', 'shishir.saxena@brillio.com', 'manishp@brillio.com', 'ManishB@brillio.com', 'pramod.nair@brillio.com', 'raj.mamodia@brillio.com', 'shivam.pandya@brillio.com', 'vinay.srivatsa@brillio.com', 'himayathulla.h@brillio.com', 'sandhya.kulkarni@brillio.com', 'tulasib@brillio.com', 'ashish.kumar@brillio.com', 'athresh.krishnappa@brillio.com', 'murali.sankaran@brillio.com', 'biswajit.sen@brillio.com', 'vikram.nalamada@brillio.com', 'padmashri.k@brillio.com', 'subhashish.mishra@brillio.com', 'rajeev.gopinath@brillio.com', 'Ashutosh.sohaney@brillio.com', 'manoj.kalyan@brillio.com'];
        
        var today_date = getDate();
        
        for (var i = 0; i < EmpList.length; i++) //
        ///for (var i = 0; i < emp_List_N.length; i++) //
        {
            nlapiLogExecution('DEBUG', 'SCH_TS_report_notification', 'before counter i : ' + i);
            
            if (validate(counter)) //
            {
                if (counter > 0 && i < counter) //
                {
                    i = counter;
                }
            }
            
            /*
             if (i == 15) //
             {
             nlapiLogExecution('DEBUG', 'SCH_TS_report_notification', 'Stop counter i : ' + i);
             return;
             }
             */
            nlapiLogExecution('DEBUG', 'SCH_TS_report_notification', 'after counter i : ' + i);
            
            var splitValues = EmpList[i].split(";");
            //var splitValues = emp_List_N[i].split(";");
            nlapiLogExecution('DEBUG', 'SCH_TS_report_notification', 'splitValues[1] : ' + splitValues[0]);
            
            var htmltext = '<table border="0" width="100%">';
            htmltext += '<tr>';
            htmltext += '<td colspan="4" valign="top">';
            
            htmltext += '<p>Hello Managers,</p>';
            htmltext += '<p></p>';
            htmltext += '<p>In a series of enabling the visibility of timesheet of your team, here is yet another change.</p>';
            htmltext += '<p></p>';
            htmltext += '<p>The PM report has the following parameters:</p>';
            htmltext += '<p></p>';
            htmltext += '<p>1.	Date range ( from and to date)</p>';
            htmltext += '<p></p>';
            htmltext += '<p>2.	Project Name</p>';
            htmltext += '<p></p>';
            htmltext += '<p>3.	Submitted hours</p>';
            htmltext += '<p></p>';
            htmltext += '<p>4.	Approved hours</p>';
            htmltext += '<p></p>';
            htmltext += '<p>You can check either or both options to generate the report. Hope this will help you in driving the timesheet compliance of your team.</p>';
            htmltext += '<p>Path for the same: ESS PM Dashboard -> Reports -> PM timesheet report</p>';
            
            
            var logourl = 'https://system.na1.netsuite.com/core/media/media.nl?id=6240&c=3883006&h=c6a9de4ba641ed397253&whence=';
            var logourl_2 = 'https://system.na1.netsuite.com/core/media/media.nl?id=6239&c=3883006&h=75ef1b5c53116fd4cb83&whence=';
            
            htmltext += '<p align=\"left\"><img border=\"0\" src=\"' + logourl + ' \" \/><\/p>'; // width=\"150\" height=\"50\"
            htmltext += '<p align=\"left\"><img border=\"0\" src=\"' + logourl_2 + ' \" \/><\/p>'; // width=\"150\" height=\"50\"
            htmltext += '</td></tr>';
            htmltext += '<tr></tr>';
            htmltext += '<tr></tr>';
            htmltext += '<tr></tr>';
            htmltext += '<p>Regards,</p>';
            htmltext += '<p>Information Systems</p>';
            htmltext += '<p>This e-mail contains Privileged and Confidential Information intended solely for the use of the addressee(s). It shall not attach any liability on the sender or Brillio or its affiliates. Any views or opinions presented in this email are solely those of the sender and may not necessarily reflect the opinions of Brillio or its affiliates. If you are not the intended recipient, you should not disseminate, distribute or copy this e-mail. Please notify the sender immediately and destroy all copies of this message and any attachments. WARNING: Computer viruses can be transmitted via email. While Brillio has taken reasonable precautions to minimize this risk, Brillio accepts no liability for any damage that may be caused to you in the event that there is any virus in this e-mail or any attachments attached hereto. It is the addresses(s) duty to check and scan this email and any attachments attached hereto for the presence of viruses prior to opening the email. ** Thank You **</p>';
            htmltext += '</table>';
            htmltext += '<hr width="100%" size="1" noshade color="#CCCCCC">';
            htmltext += '<table width="100%" border="0" cellspacing="0" cellpadding="0">';
            htmltext += '<tr>';
            htmltext += '<td align="right">';
            htmltext += '<font style="font-size:9px; font-family:Verdana,Arial,Helvetica,sans-serif; color:#999999;">Brillio is powered by <a href="http://www.netsuite.com/" style="color:#999999;">NetSuite</a> &#151; One System. No Limits.</font>';
            htmltext += '</td>';
            htmltext += '</tr>';
            htmltext += '</table>';
            htmltext += '</body>';
            htmltext += '</html>';
            
            //nlapiLogExecution('DEBUG', 'SCH_TS_report_notification', 'htmltext : ' + htmltext);
            
            try //
            {
                var subject_line = 'Netsuite - Timesheet report for PM';
                
                // TEST SECTION
                
                //nlapiSendEmail(author, recipient, subject, body, cc, bcc, records, attachments, notifySenderOnBounce, internalOnly)
                //nlapiSendEmail(442, 'vinod.n@brillio.com', subject_line, htmltext);
                //nlapiSendEmail(442, 'jai.kumar@brillio.com', subject_line, htmltext);
                //nlapiSendEmail(442, 'vikrant@aashnacloudtech.com', subject_line, htmltext);
                
                // TEST SECTION ENDS
                
                // ENABLE BELOW LINE AFTER CONFIRMATION
                nlapiSendEmail(442, splitValues[0], subject_line, htmltext);
                
                nlapiLogExecution('DEBUG', 'SCH_TS_report_notification', 'Sent E-Mail id : ' + splitValues[0]);
                
                usageRemaining = context.getRemainingUsage();
                nlapiLogExecution('DEBUG', 'SCH_TS_report_notification ', ' usageRemaining -->' + usageRemaining);
                
                if (usageRemaining < 50) //
                //if (i == 10) //
                {
                    // Define SYSTEM parameters to schedule the script to re-schedule.
                    var params = new Array();
                    params['status'] = 'scheduled';
                    params['runasadmin'] = 'T';
                    var startDate = new Date();
                    params['startdate'] = startDate.toUTCString();
                    
                    // Define CUSTOM parameters to schedule the script to re-schedule.
                    params['custscript_counter_tm'] = i + 1;
                    
                    nlapiLogExecution('DEBUG', 'SCH_TS_report_notification ', ' Script Status --> Before schedule...');
                    
                    var status = nlapiScheduleScript(nlapiGetContext().getScriptId(), nlapiGetContext().getDeploymentId(), params);
                    nlapiLogExecution('DEBUG', 'SCH_TS_report_notification ', ' Script Status -->' + status);
                    
                    ////If script is scheduled then successfuly then check for if status=queued
                    if (status == 'QUEUED') //
                    {
                        nlapiLogExecution('DEBUG', ' SCH_TS_report_notification', ' Script is rescheduled ....................');
                    }
                    
                    return;
                }
                
                // COMMMENT BELOW LINE WHILE EXECUTING IN PRODUCTION 
                //return;
            } 
            catch (ex)//
            {
                nlapiLogExecution('Error', 'SCH_TS_report_notification', 'Not sent ID : ' + splitValues[0] + ' ex : ' + ex);
                //nlapiLogExecution('Error', 'Sent E-Mail id', 'ex : ' + ex);
            }
            
            //return;
        }
    } 
    catch (e) //
    {
        nlapiLogExecution('ERROR', 'Exception', 'Ex : ' + e);
    }
}

function validate(value) //
{
    if (value != null && value != '' && value != 'undefined') // 
    {
        return true;
    }
    return false;
}

function getDate() //
{
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth() + 1; //January is 0!
    var yyyy = today.getFullYear();
    if (dd < 10)//
    {
        dd = '0' + dd
    }
    if (mm < 10)//
    {
        mm = getMonth(mm);
    }
    
    var today = mm + ' ' + dd + ',' + yyyy;
    nlapiLogExecution('DEBUG', 'getDate', 'today : ' + today);
    return today;
}

function getMonth(mm) //
{
    if (mm == 1)//
    {
        return 'January';
    }
    if (mm == 2)//
    {
        return 'February';
    }
    if (mm == 3)//
    {
        return 'March';
    }
    if (mm == 4)//
    {
        return 'April';
    }
    if (mm == 5)//
    {
        return 'May';
    }
    if (mm == 6)//
    {
        return 'June';
    }
    if (mm == 7)//
    {
        return 'July';
    }
    if (mm == 8)//
    {
        return 'August';
    }
    if (mm == 9)//
    {
        return 'September';
    }
    if (mm == 10)//
    {
        return 'October';
    }
    if (mm == 11)//
    {
        return 'November';
    }
    if (mm == 12)//
    {
        return 'December';
    }
}
