/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       13 Dec 2014     amol.sahijwani
 * 2.00		  16 Dec 2020	 Praveena		Replaced search record type as timebill and replaced field id for time entry on attribute custom record
 */

/**
 * @param {nlobjRequest} request Request object
 * @param {nlobjResponse} response Response object
 * @returns {Void} Any output is written via response object
 */
function sutCreateVendorBillScreen(request, response){
	var form = nlapiCreateForm('Create Sub-Tier Vendor Bills');
	
	// Set Script
	form.setScript('customscript_cli_subtiercreatevendorbill');
	
	//Add Subsidiary Fields
	var fld_subsidairy = form.addField('custpage_subsidiary', 'select', 'Subsidiary');
	
	fld_subsidairy.addSelectOption('BLLC', 'Brillio LLC', true);
	fld_subsidairy.addSelectOption('UK', 'Brillio UK', false);
	
	// Add Vendor Field
	var fld_vendor = form.addField('custpage_vendor', 'select', 'Subtier');
	
	var filters = [];
	
	//Removed Filters and Created saved search because of MAX LENGTH Error. If error still persist, please change the date filter in saved search and run
	
  //filters[0] = new nlobjSearchFilter('custrecord_stvd_end_date', null, 'onorafter', '6/1/2016');
	//filters[1] = new nlobjSearchFilter('type', null, 'anyof', 'A');
	//filters[1] = new nlobjSearchFilter('custentity_persontype', 'employee', 'anyof', '1');
	//filters[2] = new nlobjSearchFilter('approvalstatus', null, 'anyof', '3');
	
	
	var columns = [];
	columns[0] = new nlobjSearchColumn('custrecord_stvd_vendor', null, 'group').setSort();
	columns[1] = new nlobjSearchColumn('custrecord_stvd_contractor', null, 'group'); //customsearch_subtier_end_date_filter
	
	//var vendorNewSearch = nlapiCreateSearch('customrecord_subtier_vendor_data', filters, columns);
	var vendorSearchResults = nlapiSearchRecord('customrecord_subtier_vendor_data','customsearch_subtier_end_date_filter',null,columns);
	//var vendorSearchResults = (vendorNewSearch.runSearch()).getResults(0, 1000);
	
	// Add vendor data
	var fld_vendor_data	=	form.addField('custpage_vendor_data', 'longtext', 'Text').setDisplayType('hidden');
	var fld_vendor_data_dup	=	form.addField('custpage_vendor_data_dup', 'longtext', 'Text').setDisplayType('hidden');
	
	o_vendor_data	=	new Array();
	o_vendor_data_dup = new Array();
	o_unique_vendor_data = new Array();
	fld_vendor.addSelectOption('-1', '');
	var o_vendor_data_post = new Array();
	
	for(var i = 0; i < vendorSearchResults.length; i++)
		{
			var vendorId = vendorSearchResults[i].getValue(columns[0]);
			var vendorName = vendorSearchResults[i].getText(columns[0]);
			var contractor_id = vendorSearchResults[i].getValue(columns[1]);
			var contractor_name	=	vendorSearchResults[i].getText(columns[1]);
			o_vendor_data_post[i]= {'vendor_id': vendorId, 'vendor_name': vendorName, 'employee_id': contractor_id, 'employee_name': contractor_name};
			if(i<=600)
			o_vendor_data[i]	=	{'vendor_id': vendorId, 'vendor_name': vendorName, 'employee_id': contractor_id, 'employee_name': contractor_name};
			else
			o_vendor_data_dup.push({'vendor_id': vendorId, 'vendor_name': vendorName, 'employee_id': contractor_id, 'employee_name': contractor_name});
			
			//if((i > 0 && o_vendor_data[i].vendor_id != o_vendor_data[i-1].vendor_id) || i == 0)
				if(o_unique_vendor_data.indexOf(vendorId)<0)
				{
					o_unique_vendor_data.push(vendorId);
					fld_vendor.addSelectOption(vendorId, vendorName);
				}
				
		}
	
	fld_vendor_data.setDefaultValue(JSON.stringify(o_vendor_data));
	fld_vendor_data_dup.setDefaultValue(JSON.stringify(o_vendor_data_dup));
	
	// Add From Date Field
	var fld_from_date = form.addField('custpage_from_date', 'date', 'From Date');
	
	// Add To Date Field
	var fld_to_date = form.addField('custpage_to_date', 'date', 'To Date');
	
	// Add Billing Type Field
	var fld_bill_type = form.addField('custpage_bill_type', 'select', 'Billing Type');
	
	fld_bill_type.addSelectOption('TM', 'T & M', true);
	fld_bill_type.addSelectOption('FBM', 'FP', false);
	
	
	
	// Add Employee Field
	var fld_employee = form.addField('custpage_employee', 'select', 'Employee');
	
	form.addSubmitButton('Load Data');
	
	if(request.getMethod() == 'POST')
		{	

			var i_vendor = request.getParameter('custpage_vendor');
			fld_vendor.setDefaultValue(i_vendor);
			
			fld_employee.addSelectOption(-1, '');
			
			for(var i = 0; i < o_vendor_data_post.length; i++)
			{
				if(o_vendor_data_post[i].vendor_id == i_vendor)
					{
						var employeeId = o_vendor_data_post[i].employee_id;
						var employeeName = o_vendor_data_post[i].employee_name;//employeeSearchResults[i].getText(columns[0]);
					
						fld_employee.addSelectOption(employeeId, employeeName);
					}
			}
			
			var s_from_date = request.getParameter('custpage_from_date');
			var d_from_date = new Date(s_from_date);
			fld_from_date.setDefaultValue(s_from_date);
			
			var s_to_date = request.getParameter('custpage_to_date');
			var d_to_date = new Date(s_to_date);
			fld_to_date.setDefaultValue(s_to_date);
			
			var billing_type = request.getParameter('custpage_bill_type');
			fld_bill_type.setDefaultValue(billing_type);
			
			var i_employee = request.getParameter('custpage_employee');
			if(i_employee != '')
				{
					fld_employee.setDefaultValue(i_employee);
				}
			// Validate vendor details
			
			//var vendor_filter	=	new Array();
			//var vendor_columns	=	new Array();
						
			var a_vendor_details	=	getVendorDetails(i_vendor, i_employee, d_from_date, d_to_date);//nlapiSearchRecord('customrecord_subtier_vendor_data', null, vendor_filter, vendor_columns);
			
			if(a_vendor_details.length != 1)
				{
					var fld_error	=	form.addField('custpage_error', 'inlinehtml', 'Error');var str = '';
					str += '<h3 style="color:red;">Please select a valid date range for this vendor and contractor.</h3>';
					//str += '<table><tr><th>ST Pay Rate</th><th>OT Pay Rate</th><th>Payment Terms</th></tr>';
					for(var i = 0; i < a_vendor_details.length; i++)
						{
							//str += '<tr><td>'+a_vendor_details[i].st_rate+'</td><td>'+a_vendor_details[i].ot_rate+'</td><td>'+a_vendor_details[i].payment_terms+'</td></tr>';
						}
					fld_error.setDefaultValue(str + '</table>');//return;
					response.writePage(form);
					return;
				}
			
			var a_billedTimeEntries = getBilledTimeEntriesArray(d_from_date, d_to_date, i_vendor, i_employee);
			
			var filters = new Array();
			
			filters[0] = new nlobjSearchFilter('type', null, 'anyof', 'A');
			//filters[1] = new nlobjSearchFilter('custentity_vendoragency', 'employee', 'anyof', i_vendor);
			filters[1] = new nlobjSearchFilter('date', null, 'within', d_from_date, d_to_date);
			filters[2] = new nlobjSearchFilter('custcol_subtierpay', null, 'is', 'F');
			filters[3] = new nlobjSearchFilter('jobbillingtype', 'job', 'is', billing_type);
			filters[4] = new nlobjSearchFilter('approvalstatus', null, 'anyof', '3');
			if(billing_type == 'TM')
				{
					filters[filters.length] = new nlobjSearchFilter('status', null, 'is', 'F');
				}
			if(i_employee != '')
				{
					filters[filters.length] = new nlobjSearchFilter('employee', null, 'anyof', i_employee);
				}
			
			var columns = new Array();
			columns[0]	= new nlobjSearchColumn('internalid');
			columns[1]	= new nlobjSearchColumn('employee');
			columns[2]	= new nlobjSearchColumn('customer','job');
			columns[3]	= new nlobjSearchColumn('custcolprj_name');
			columns[4]	= new nlobjSearchColumn('item');
			columns[5]	= new nlobjSearchColumn('durationdecimal');
			columns[6] = new nlobjSearchColumn('custentity_pofrom', 'employee');
			columns[7] = new nlobjSearchColumn('custentity_poto', 'employee');
			columns[8] = new nlobjSearchColumn('department');
			columns[9] = new nlobjSearchColumn('custentity_vertical', 'job');
			columns[10] = new nlobjSearchColumn('date');
			columns[11] = new nlobjSearchColumn('subsidiary');

			
			// Search Records
			var search_results = nlapiSearchRecord('timebill', null, filters, columns);
			
			// Add Sublist
			var o_sublist = form.addSubList('custpage_time_entries', 'list', 'Select entries');
			
			// Add Mark All Button
			o_sublist.addMarkAllButtons();
			
			// Add Create Vendor Bill Button
			o_sublist.addButton('custpage_create_vendor_bill','Create Vendor Bills', 'createVendorBills');
			
			// Add Fields
			o_sublist.addField('custpage_select', 'checkbox', 'Select');
			o_sublist.addField('custpage_internal_id', 'text', 'Internal ID');
			o_sublist.addField('custpage_employee', 'text', 'Employee');
			o_sublist.addField('custpage_vendoragency', 'text', 'Vendor Agency');
			o_sublist.addField('custpage_customer', 'text', 'Customer');
			o_sublist.addField('custpage_project', 'text', 'Project');
			o_sublist.addField('custpage_date', 'text', 'Date');
			o_sublist.addField('custpage_item', 'text', 'Item');
			o_sublist.addField('custpage_hours', 'text', 'Hours');
			o_sublist.addField('custpage_st_pay_rate', 'text', 'ST Pay Rate');
			o_sublist.addField('custpage_ot_pay_rate', 'text', 'OT Pay Rate');
			o_sublist.addField('custpage_paymentterms', 'text', 'Payment Terms');
			o_sublist.addField('custpage_po_from', 'text', 'PO From');
			o_sublist.addField('custpage_po_to', 'text', 'PO To');
			o_sublist.addField('custpage_practice', 'text', 'Practice');
			o_sublist.addField('custpage_vertical', 'text', 'Vertical');
			//o_sublist.addField('custpage_vendor_bill_url', 'url', "", null).setDisplayType('inline').setLinkText("Vendor Bill").setDefaultValue("#");
			
			var a_data = new Array();
			
			for(var i_list_indx = 0;search_results != null && i_list_indx < search_results.length; i_list_indx++)
				{
					var a_row_data = new Object();
					var o_search_result = search_results[i_list_indx];
					
					var i_time_entry_id = o_search_result.getValue('internalid');
					if(a_billedTimeEntries.indexOf(i_time_entry_id) == -1)
					{
						a_row_data['custpage_internal_id'] = o_search_result.getValue('internalid');
						a_row_data['custpage_employee'] = o_search_result.getText('employee');
						a_row_data['custpage_vendoragency'] = a_vendor_details[0].vendor_name;//o_search_result.getText('custentity_vendoragency','employee');
						a_row_data['custpage_customer'] = o_search_result.getText('customer','job');
						a_row_data['custpage_project'] = o_search_result.getValue('custcolprj_name');
						a_row_data['custpage_date']	=	o_search_result.getValue('date');
						a_row_data['custpage_item'] =	o_search_result.getValue('item');
						var dur = o_search_result.getValue('durationdecimal');
						var sub = o_search_result.getValue('subsidiary');
						if((sub == 'Brillio Inc : Brillio LLC : BRILLIO UK LIMITED') && dur != 0)
						{
							a_row_data['custpage_hours'] = 1;
						}
						else if(sub != 'Brillio Inc : Brillio LLC : BRILLIO UK LIMITED')
						{
							a_row_data['custpage_hours'] = o_search_result.getValue('durationdecimal');
						}
						a_row_data['custpage_st_pay_rate'] = a_vendor_details[0].st_rate;//o_search_result.getValue('custentity_stpayrate','employee');
						a_row_data['custpage_ot_pay_rate'] = a_vendor_details[0].ot_rate;//o_search_result.getValue('custentity_otpayrate', 'employee');
						a_row_data['custpage_paymentterms'] = a_vendor_details[0].payment_terms;//o_search_result.getText('custentity_paymentterms', 'employee');
						a_row_data['custpage_po_from'] = o_search_result.getValue('custentity_pofrom');
						a_row_data['custpage_po_to'] = o_search_result.getValue('custentity_poto');
						a_row_data['custpage_practice'] = o_search_result.getText('department');
						a_row_data['custpage_vertical'] = o_search_result.getText('custentity_vertical', 'job');
						//a_row_data['custpage_vendor_bill_url'] = ''
						a_data[i_list_indx] = a_row_data;
					}
					
				}
			o_sublist.setLineItemValues(a_data);
			
		}
	
	response.writePage(form);
}
function getBilledTimeEntriesArray(d_from_date, d_to_date, i_vendor, i_employee)
{
	// Get billed time entries
	var filters = new Array();
	filters[0] = new nlobjSearchFilter('date', 'custrecord_time_entry_id_tb', 'within', d_from_date, d_to_date);
	filters[1] = new nlobjSearchFilter('name', 'custrecord_vendor_bill', 'anyof', i_vendor);
	if(i_employee != '')
		{
			filters[2] = new nlobjSearchFilter('employee', 'custrecord_time_entry_id_tb', 'anyof', i_employee);
		}
	
	var columns = new Array();
	columns[0] = new nlobjSearchColumn('internalid', 'custrecord_time_entry_id_tb');
	
	var a_search_result = nlapiSearchRecord('customrecord_ts_extra_attributes', null, filters, columns);
	var a_list = new Array();
	
	if(!a_search_result)
	{
		return a_list;
	}
	
	for(var i = 0; i < a_search_result.length; i++)
		{
			a_list.push(a_search_result[i].getValue('internalid', 'custrecord_time_entry_id_tb'));
		}
	return a_list;
}


function sutCreateVendorBill(request, response){
	
	// Get the list of Time Entries
	var strRequest = request.getBody();
	
	var a_data = new Array(); // Store Time Entry data
	
	var o_request	=	JSON.parse(strRequest);
	
	var a_list = o_request.data; // Parse the list of Time Entries
	
	var a_days = ['sunday', 'monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday'];
	
	var a_track_updated_time_entries = new Array();
	
	var a_vendor_details	=	getVendorDetails(o_request.vendor, o_request.employee, new Date(o_request.from), new Date(o_request.to));
	
	var i_vendor_id = o_request.vendor;//null;
	nlapiLogExecution("DEBUG",'a_list',JSON.stringify(a_list));
	// Loop through the time entries
	var a_selected_lines=[];
	for(var i_timesheet_indx = 0; i_timesheet_indx < a_list.length; i_timesheet_indx++)
		{
			var i_timeentry_id = a_list[i_timesheet_indx].internalid; // Time Entry Id
			nlapiLogExecution('debug','i_timeentry_id',i_timeentry_id);
			var filters = new Array();
			
			filters[0] = new nlobjSearchFilter('internalid', null, 'is', i_timeentry_id);
			
			var columns = new Array();
			columns[0] = new nlobjSearchColumn('internalid');
			columns[1] = new nlobjSearchColumn('employee');
			//columns[2] = new nlobjSearchColumn('custentity_vendoragency','employee');
			columns[2] = new nlobjSearchColumn('customer','job');
			columns[3] = new nlobjSearchColumn('custcolprj_name');
			columns[4] = new nlobjSearchColumn('item');
			columns[5] = new nlobjSearchColumn('durationdecimal');
			//columns[7] = new nlobjSearchColumn('custentity_stpayrate','employee');
			//columns[8] = new nlobjSearchColumn('custentity_otpayrate', 'employee');
			//columns[9] = new nlobjSearchColumn('custentity_paymentterms', 'employee');
			columns[6] = new nlobjSearchColumn('custentity_pofrom', 'employee');
			columns[7] = new nlobjSearchColumn('custentity_poto', 'employee');
			columns[8] = new nlobjSearchColumn('department');
			columns[9] = new nlobjSearchColumn('custentity_vertical', 'job');
			columns[10] = new nlobjSearchColumn('date');
			columns[11] = new nlobjSearchColumn('internalid','job');
			columns[12]	= new nlobjSearchColumn('entityid', 'job');
			columns[13] = new nlobjSearchColumn('companyname', 'job');
			columns[14]	= new nlobjSearchColumn('jobname', 'job');
			columns[15]	= new nlobjSearchColumn('subsidiary');
			columns[16]	= new nlobjSearchColumn('item');
			
			// Search Records
			var search_results = nlapiSearchRecord('timebill', null, filters, columns);
			
			var o_search_result = search_results[0];
		
			
			// Load Employee Record
			var employeeId = o_search_result.getValue('employee');
			var employeeName = o_search_result.getText('employee');
			//var recEmployee = nlapiLoadRecord('employee', employeeId);
			
			var projectId = o_search_result.getValue('internalid','job');
			var duration = parseFloat(o_search_result.getValue('durationdecimal'));
			var stRate="";
			var otRate="";
			
			var billing_from="";
			var billing_to='';
			for(var pp=0;a_vendor_details.length>pp;pp++)
			{
				if(a_list[i_timesheet_indx].st_rate==a_vendor_details[pp].st_rate && a_list[i_timesheet_indx].ot_rate==a_vendor_details[pp].ot_rate)
				{
					stRate=a_list[i_timesheet_indx].st_rate;
					otRate=a_list[i_timesheet_indx].ot_rate;
					billing_from=a_vendor_details[pp].stvd_startdate;
					billing_to=a_vendor_details[pp].stvd_enddate;
					
					break;
				}
				
			}
			nlapiLogExecution('DEBUG',billing_from+" : "+billing_to+" : "+stRate+" : "+otRate);
			var i_practice = o_search_result.getValue('department');
			
			//var o_projectOBJ = nlapiLoadRecord('job',projectId);
			var i_vertical =  o_search_result.getValue('custentity_vertical', 'job');
			var i_customer_name =  o_search_result.getText('customer','job');
            var i_customer_id =  o_search_result.getValue('customer','job');
			var cust_territory = nlapiLookupField('customer',i_customer_id,'territory');
			
            var i_project_name_ID =  o_search_result.getValue('entityid', 'job');
            var i_project_name =  o_search_result.getValue('jobname', 'job');
            var s_project_description = i_project_name_ID+' '+i_project_name;

			var subsidiary=o_search_result.getValue('subsidiary');//Added by praveena
			var itemId=o_search_result.getValue('item');//Added by praveena
			
			nlapiLogExecution("DEBUG",'itemId',itemId);
			nlapiLogExecution("DEBUG",'subsidiary',subsidiary);
			
            var type = null;
			var rate = 0.0;
			
			if(itemId == '2222' || itemId == '2221' || itemId == 'FP' || itemId == 'ST')//Added By Praveena i.e itemId == 'FP'
				{
					type = 'ST';
					rate = stRate;
				}
			else if(itemId == '2425' || itemId == 'OT')
				{
					type = 'OT';
					rate = otRate;
				}
			else
				{
					type = 'OTHER';
				}
			
								a_data[i_timesheet_indx] = 
										{
											'employee':employeeId, 
											'employee_name':employeeName,
											'project':projectId, 
											'item':itemId, 
											'type': type, 
											'rate': rate, 
											'duration': duration, 
											'practice': i_practice,
											'vertical': i_vertical,
											'customer_name': i_customer_name,
											'cust_territory':cust_territory,
											'project_description': s_project_description,
											'billing_from':billing_from,
											'billing_to':billing_to,
											'internalid':i_timeentry_id
											};
											
											a_selected_lines.push(i_timeentry_id);
		}
	
	var a_grouped_data = groupTimesheetData(a_data);
	nlapiLogExecution("DEBUG",'a_grouped_data',JSON.stringify(a_grouped_data));
	
	//**GROUPING BY BILLING END DATE *****///

		var a_group_billingdates_data = a_grouped_data.reduce(function(a, e) {
			// GROUP BY estimated key (estKey), well, may be a just plain key
			// a -- Accumulator result object
			// e -- sequentally checked Element, the Element that is tested just at this itaration

			// new grouping name may be calculated, but must be based on real value of real field
			var  estKey = (e['billing_to']); 
			(a[estKey] ? a[estKey] : (a[estKey] = null || [])).push(e);
			return a;
		}, {});
		//****END GROUPING BY BILLING END DATE*****/
nlapiLogExecution("DEBUG",'a_grouped_data',JSON.stringify(a_group_billingdates_data));
	
	
	// Set Field Values
	var o_Field_Values = new Object();
	o_Field_Values.entity = i_vendor_id;
	nlapiLogExecution('audit','inside bill creation');
	var a_vendor_bill_id=[];
	var i_vendor_bill_id = null;
	var status = 1;
	
	
	for(var billdate in a_group_billingdates_data)
	{
	
	var recVendorBill = nlapiCreateRecord('vendorbill', {'entity': i_vendor_id});
	 recVendorBill.setFieldValue('customform', 119);
	//recVendorBill.setFieldValue('custbody_financemanager', 5764);
	
	var a_grouped_data="";
	a_grouped_data=a_group_billingdates_data[billdate];
	for(var i_grouped_data_indx = 0; i_grouped_data_indx < a_grouped_data.length; i_grouped_data_indx++)
		{
			if(i_grouped_data_indx==0){
				var billing_start_date=nlapiDateToString(a_grouped_data[i_grouped_data_indx]['billing_from']);
				var billing_end_date=nlapiDateToString(a_grouped_data[i_grouped_data_indx]['billing_to']);
				recVendorBill.setFieldValue('custbody_billfrom',billing_start_date);
				recVendorBill.setFieldValue('custbody_billto', billing_end_date);
				}
					
			i_item_id = null;
			
			var i_line_item_indx = (i_grouped_data_indx + 1);
			var o_grouped_data = a_grouped_data[i_grouped_data_indx];
			
			switch(a_grouped_data[i_grouped_data_indx].type)
			{
				case 'ST':
					i_item_id = 2443;
					break;
				case 'OT':
					i_item_id = 2444;
					break;
			}
			//================= Logic to get practice from Employee record if TS practice is inactice =======================
			var i_practice = o_grouped_data.practice;
			var inactivePractice = ["572","570","567","565","564","563","562","559","558","557","549","544","534","533","531","528","526","525","524","523","522","521","520","519","518","517"]
			if(inactivePractice.indexOf(i_practice) != -1)
			{
				var i_empId = o_grouped_data.employee;
				nlapiLogExecution('debug','i_empId ',i_empId)
				if(i_empId)
					i_practice = nlapiLookupField('employee',i_empId,'department');
				nlapiLogExecution('debug','i_practice AA ',i_practice);
			}
			//=============================================================================
			recVendorBill.setLineItemValue('item', 'item', i_line_item_indx, i_item_id);
			recVendorBill.setLineItemValue('item', 'custcol_temp_project', i_line_item_indx, o_grouped_data.project);
			recVendorBill.setLineItemValue('item', 'custcol_employeenamecolumn', i_line_item_indx, o_grouped_data.employee_name);
			recVendorBill.setLineItemValue('item', 'rate', i_line_item_indx, o_grouped_data.rate);
			recVendorBill.setLineItemValue('item', 'quantity', i_line_item_indx, o_grouped_data.duration);
			recVendorBill.setLineItemValue('item', 'custcolcustcol_temp_customer', i_line_item_indx, o_grouped_data.customer_name);
			recVendorBill.setLineItemValue('item', 'department', i_line_item_indx, i_practice); // updated by Ashish
			recVendorBill.setLineItemValue('item', 'custcolprj_name', i_line_item_indx, o_grouped_data.project_description);
			recVendorBill.setLineItemValue('item', 'class', i_line_item_indx, o_grouped_data.vertical);
			recVendorBill.setLineItemValue('item', 'custcol_territory', i_line_item_indx, o_grouped_data.cust_territory);
			recVendorBill.setFieldValue('custbody_consultantname', o_grouped_data.employee);
			
			nlapiLogExecution("DEBUG","a_grouped_data",JSON.stringify(o_grouped_data));
		}
	
	
	
	var a_actions = new Array();
	
	var i_time_entries_count = a_track_updated_time_entries.length; 
	aa_list='';
		aa_list=o_grouped_data["a_tb_internalids"];
	for(var i_timesheet_indx = 0; i_timesheet_indx <aa_list.length; i_timesheet_indx++)

		{
			var i_timeentry_id = aa_list[i_timesheet_indx];
					
			var filters = new Array();
			filters[0] = new nlobjSearchFilter('custrecord_time_entry_id_tb', null, 'anyof', i_timeentry_id);
					
			var columns = new Array();
			columns[0] = new nlobjSearchColumn('internalid');
			columns[1] = new nlobjSearchColumn('custrecord_vendor_bill');
					
			var search_existing_entries = nlapiSearchRecord('customrecord_ts_extra_attributes', null, filters, columns);
					
			if(search_existing_entries == null)
				{
					a_actions.push({'type':'create','time_entry_id':i_timeentry_id});
					
					nlapiLogExecution("DEBUG","create : a_actions push");
				}
			else if(search_existing_entries.length == 1 && search_existing_entries[0].getValue('custrecord_vendor_bill') == '')
				{
					nlapiLogExecution("DEBUG","create : a_actions push");
					a_actions.push({'type':'update', 'internalid': search_existing_entries[0].getValue('internalid')});
				}
			else
				{
					status = 0;
				}
				
				
		}
		nlapiLogExecution("DEBUG","END FR LOOP");
	
	if(status == 1)
	{
		try
		{
			//Comment submit field to check the functionality 0n 15-05-2020
			i_vendor_bill_id = nlapiSubmitRecord(recVendorBill, true, true);
			a_vendor_bill_id.push(i_vendor_bill_id);
			nlapiLogExecution("DEBUG","i_vendor_bill_id",i_vendor_bill_id);
		}
		catch(e)
		{
			nlapiLogExecution('ERROR', 'Error', e.message);
			status = 0;
		}
	}
	
	if(status == 1)
	{
		for(var i = 0; i < a_actions.length; i++)
			{
				if(a_actions[i].type == 'create')
					{
				
					var newMappingRecord = nlapiCreateRecord('customrecord_ts_extra_attributes');
						newMappingRecord.setFieldValue('custrecord_time_entry_id_tb', a_actions[i].time_entry_id);
						newMappingRecord.setFieldValue('custrecord_vendor_bill', i_vendor_bill_id);
						newMappingRecord.setFieldValue('custrecord_provision_created', 'T'); //NEEDS TO CHECK WT DEEPAK OR SHAMANH
						
                       i_mapping_record_id = nlapiSubmitRecord(newMappingRecord);
						
						//================================================================================================
						//nlapiSubmitField('timesheet',a_actions[i].time_entry_id,'custrecord_provision_completed','T');
						//nlapiLogExecution('Debug','a_actions[i].time_entry_id ',a_actions[i].time_entry_id);
						//================================================================================================
					}
				else if(a_actions[i].type == 'update')
					{
						
						var i_mapping_record_id = a_actions[i].internalid;//search_existing_entries[0].getValue('internalid');
					
                    nlapiSubmitField('customrecord_ts_extra_attributes', i_mapping_record_id, ['custrecord_vendor_bill','custrecord_provision_created'], [i_vendor_bill_id,'T']);//NEEDS TO CHECK WT DEEPAK OR SHAMANH
                    
                 
						//================================================================================================
						//nlapiSubmitField('timesheet',a_actions[i].time_entry_id,'custrecord_provision_completed','T');
						//nlapiLogExecution('Debug','a_actions[i].time_entry_id ',a_actions[i].time_entry_id);
						//================================================================================================
					}
			}
	}
}
nlapiLogExecution("DEBUG",'a_vendor_bill_id',JSON.stringify(a_vendor_bill_id));
	var o_response = new Object();
	o_response.status = status;
	o_response.vendor_bill_id = a_vendor_bill_id;
	//o_response.list = a_list;
	o_response.list = a_selected_lines;
	var context = nlapiGetContext();
	var usageRemaining = context.getRemainingUsage();nlapiLogExecution('AUDIT', 'Remaining Usage: ', usageRemaining);
	
	response.write(JSON.stringify(o_response));
}
function searchTEExtraAttributeRecord(i_timeentry_id)
{
	
}


function createVendorBills()
{
	var s_from_date	=	nlapiGetFieldValue('custpage_from_date');
	var d_from_date	=	nlapiStringToDate(s_from_date, 'date');
	
	var s_to_date	=	nlapiGetFieldValue('custpage_to_date');
	var d_to_date	=	nlapiStringToDate(s_to_date, 'date');
	
	var i_vendor	=	nlapiGetFieldValue('custpage_vendor');
	
	var i_employee	=	nlapiGetFieldValue('custpage_employee');
	
	var arrSelectedValues = new Array();
	
	var lineItemCount = nlapiGetLineItemCount('custpage_time_entries');
	
	for(var i_line_item_indx = 1; i_line_item_indx <= lineItemCount; i_line_item_indx++)
		{
			var isSelected = nlapiGetLineItemValue('custpage_time_entries','custpage_select',i_line_item_indx);
			if(isSelected == 'T')
				{
					arrSelectedValues.push(
					{"internalid":nlapiGetLineItemValue('custpage_time_entries', 'custpage_internal_id',i_line_item_indx)},
					{"st_rate":nlapiGetLineItemValue('custpage_time_entries', 'custpage_st_pay_rate', i_line_item_indx)},
					{"ot_rate":nlapiGetLineItemValue('custpage_time_entries', 'custpage_ot_pay_rate', i_line_item_indx)}
                    );
				}
		}
	
	var url = nlapiResolveURL('SUITELET', 468, 1);
	var strSelectedValues = JSON.stringify({'from':s_from_date, 'to':s_to_date, 'data':arrSelectedValues, 'vendor': i_vendor, 'employee': i_employee});
	
	var response = nlapiRequestURL(url, strSelectedValues, null, {'User-Agent-x':'SuiteScript-Call'});

	var responseData = response.getBody();
	var responseJson = JSON.parse(responseData);
	if(responseJson.status == 1)
		{
			var vendor_bill_url = nlapiResolveURL('RECORD', 'vendorbill', responseJson.vendor_bill_id);
			alert('Vendor bill created');
			for(var i_line_item_indx = 1; i_line_item_indx <= lineItemCount; i_line_item_indx++)
				{
					var i_time_entry_id = nlapiGetLineItemValue('custpage_time_entries', 'custpage_internal_id', i_line_item_indx);
					if(responseJson.list.indexOf(i_time_entry_id) != -1)
						{
							//nlapiSelectLineItem('custpage_time_entries', i_line_item_indx);
							//nlapiRemoveLineItem('custpage_time_entries', i_line_item_indx);
							//nlapiDisableLineItemField('custpage_time_entries', 'custpage_select', true);
							//nlapiSetLineItemValue('custpage_time_entries', 'custpage_vendor_bill_url', i_line_item_indx, vendor_bill_url);
							nlapiSetLineItemValue('custpage_time_entries', 'custpage_select', i_line_item_indx, 'F');
							document.getElementById('custpage_select' + i_line_item_indx).setAttribute('disabled', 'disabled');
						}
				}
			var host = document.URL.substring(0, ( document.URL.indexOf('.com') + 4) );
			window.open(host+vendor_bill_url);
			void(0);
			return true;
		}

	alert(response.getBody());
}


function groupTimesheetData(a_data)
{
	var a_grouped_data = new Array();
	
	for(var i_timesheet_data_indx = 0; i_timesheet_data_indx < a_data.length; i_timesheet_data_indx++)
		{
			o_timesheet_data = a_data[i_timesheet_data_indx];
			
			var groupedRecordFound = false;
			// Search for existing group record
			for(var i_group_data_indx = 0; i_group_data_indx < a_grouped_data.length; i_group_data_indx++)
				{
					o_grouped_data = a_grouped_data[i_group_data_indx];
					
					if(o_timesheet_data.employee == o_grouped_data.employee
							&& o_timesheet_data.project == o_grouped_data.project
							&& o_timesheet_data.item == o_grouped_data.item
							&& o_timesheet_data.rate == o_grouped_data.rate 
							&& o_timesheet_data.billing_from==o_grouped_data.billing_from 
							&& o_timesheet_data.billing_to==o_grouped_data.billing_to)
							{
								groupedRecordFound = true;
								a_grouped_data[i_group_data_indx].duration += parseFloat(o_timesheet_data.duration);
								
								a_grouped_data[i_group_data_indx]["a_tb_internalids"].push(o_timesheet_data['internalid']);
							}
				}
			if(groupedRecordFound == false)
				{
					var o_new_timesheet_data = new Object();
					for ( var i_field in o_timesheet_data) {
						o_new_timesheet_data[i_field] = o_timesheet_data[i_field];
					}
					o_new_timesheet_data["a_tb_internalids"]=[];
					o_new_timesheet_data["a_tb_internalids"].push(o_timesheet_data['internalid']);
					a_grouped_data.push(o_new_timesheet_data);
				}
		}
	
	return a_grouped_data;
}

function _is_Valid(obj) //
{
	if (obj != null && obj != '' && obj != 'undefined' && obj != undefined)//
	{
		return true;
	}
	else //
	{
		return false;
	}
}

function _correct_time(t_time) //
{
	// function is used to correct the time 
	
	if (_is_Valid(t_time)) //
	{
		//nlapiLogExecution('DEBUG', '_correct_time', 't_time : ' + t_time);
		
		var hrs = t_time.split(':')[0]; 
		//nlapiLogExecution('DEBUG', '_correct_time', 'hrs : ' + hrs);
		
		if (t_time.indexOf(':') > -1) //
		{
			var mins = t_time.split(':')[1]; 
			//nlapiLogExecution('DEBUG', '_correct_time', 'mins : ' + mins);
			
			if (_is_Valid(mins)) //
			{
				mins = parseFloat(mins) / parseFloat('60'); 
				//nlapiLogExecution('DEBUG', '_correct_time', 'after correct mins : ' + mins);
				
				hrs = parseFloat(hrs) + parseFloat(mins); 
				//nlapiLogExecution('DEBUG', '_correct_time', 'after adding mins to hrs : ' + hrs);
			}
		}
		//nlapiLogExecution('DEBUG', '_correct_time', 'hrs : ' + hrs);
		return hrs;
	}
	else //
	{
		return 0;
		//nlapiLogExecution('DEBUG', '_correct_time', 't_time is invalid : ' + t_time);
	}
}

// Get Values
function getValues(i_projectID, i_employeeID)
{
    var a_return_array;
    var s_project_description = '';	
    var i_customer_name;
    var i_vertical_head='';
		
	try
	{
		//--------------------------------------------------------------------------------
		 
        if(_logValidation(i_projectID))
        {
            var o_projectOBJ = nlapiLoadRecord('job',i_projectID);
	        i_vertical_head =  o_projectOBJ.getFieldValue('custentity_verticalhead');
    	    nlapiLogExecution('DEBUG', 'i_vertical_head','i_vertical_head-->' + i_vertical_head);
        	    
            i_customer_name =  o_projectOBJ.getFieldText('parent');
            nlapiLogExecution('DEBUG', 'suiteletFunction',' Customer-->' + i_customer_name);
                
            var i_project_name_ID =  o_projectOBJ.getFieldValue('entityid');
            nlapiLogExecution('DEBUG', 'suiteletFunction',' Project Name ID -->' + i_project_name_ID);
            
            var i_project_name =  o_projectOBJ.getFieldValue('companyname');
            nlapiLogExecution('DEBUG', 'suiteletFunction',' Project Name -->' + i_project_name);
            
            s_project_description = i_project_name_ID+' '+i_project_name;
            nlapiLogExecution('DEBUG', 'suiteletFunction',' Project Description -->' + s_project_description);
            
            if(_logValidation(i_employeeID))
            {
                //----------practice value get from employee master
			    var o_employeeOBJ = nlapiLoadRecord('employee',i_employeeID);
                var i_practice=o_employeeOBJ.getFieldValue('department');
                nlapiLogExecution('DEBUG', 'suiteletFunction',' i_practice11111 -->' + i_practice);
            }
            else
            {
                if(_logValidation(o_projectOBJ))
				{
				    ////-------------practice value get from project-------------------
					i_practice =  o_projectOBJ.getFieldValue('custentity_practice');
					nlapiLogExecution('DEBUG', 'suiteletFunction',' i_practice-->' + i_practice);
				}
            }//Project OBJ				
        }//Project ID
        else if(_logValidation(i_employeeID))
		{
		     
			 //----------practice value get from employee master
			var o_employeeOBJ = nlapiLoadRecord('employee',i_employeeID);
			var i_practice=o_employeeOBJ.getFieldValue('department');
			nlapiLogExecution('DEBUG', 'suiteletFunction',' i_practice11111 -->' + i_practice);
		}	

        a_return_array =s_project_description+'&&%%%**'+i_customer_name+'&&%%%**'+i_practice+'&&%%%**'+i_vertical_head;
			
	}
	catch(exception)
	{
        nlapiLogExecution('DEBUG', 'ERROR',' Exception Caught -->' + exception);	
	}//CATCH
		
	return(a_return_array);//response.write(a_return_array);	
}

// This function will return the vendor details
function getVendorDetails(i_vendor, i_employee, d_from_date, d_to_date) 
{
	/*var vendor_filter	=	new Array();
	vendor_filter[0]	=	new nlobjSearchFilter('custrecord_stvd_vendor', null, 'anyof', i_vendor);
	vendor_filter[1]	=	new nlobjSearchFilter('custrecord_stvd_start_date', null, 'onorbefore', d_from_date);
	vendor_filter[2]	=	new nlobjSearchFilter('custrecord_stvd_end_date', null, 'onorafter', d_to_date);
	vendor_filter[3]	=	new nlobjSearchFilter('custrecord_stvd_contractor', null, 'anyof', i_employee);
	vendor_filter[4] =	new nlobjSearchFilter('isinactive', null, 'is', 'F');
	*/
	var s_from_date=nlapiDateToString(d_from_date);
  var s_to_date=nlapiDateToString(d_to_date);
  
		var vendor_filter	=
	[
   [["custrecord_stvd_start_date","within",s_from_date,s_to_date],"OR",["custrecord_stvd_start_date","onorbefore",s_from_date]],
      "AND",
   [["custrecord_stvd_end_date","within",s_from_date,s_to_date],"OR",["custrecord_stvd_end_date","onorafter",s_to_date]], 
   "AND", 
   ["custrecord_stvd_contractor","anyof",i_employee],
    "AND", 
   ["isinactive","is",'F'],
    "AND",
    ["custrecord_stvd_vendor","anyof",i_vendor],
   ];
	
	
	var vendor_columns	=	new Array();
	vendor_columns[0]	=	new nlobjSearchColumn('custrecord_stvd_st_pay_rate');
	vendor_columns[1]	=	new nlobjSearchColumn('custrecord_stvd_ot_pay_rate');
	vendor_columns[2]	=	new nlobjSearchColumn('custrecord_stvd_payment_terms');
	vendor_columns[3]	=	new nlobjSearchColumn('custrecord_stvd_vendor');
	vendor_columns[4]	=	new nlobjSearchColumn('custrecord_stvd_start_date');
	vendor_columns[5]	=	new nlobjSearchColumn('custrecord_stvd_end_date');
	
	var a_search_vendor_details	=	nlapiSearchRecord('customrecord_subtier_vendor_data', null, vendor_filter, vendor_columns);
	
	var i_count		=	a_search_vendor_details == null?0:a_search_vendor_details.length;
	
	var a_vendor_details	=	new Array();
	
	for(var i = 0; i < i_count; i++)
		{
			
				var d_temp_from_date='';
				var d_temp_end_date='';
				var d_stvd_startdate= new Date(a_search_vendor_details[i].getValue('custrecord_stvd_start_date'));
          		var d_stvd_enddate= new Date(a_search_vendor_details[i].getValue('custrecord_stvd_end_date'));
          
				d_temp_from_date=d_stvd_startdate>d_from_date?d_stvd_startdate : d_from_date; //Update End date as subtier end date in case if its mid of the month
          d_temp_end_date=d_stvd_enddate<d_to_date?d_stvd_enddate : d_to_date; //Update End date as subtier end date in case if its mid of the month
          
          
          /*if(((d_stvd_startdate.getMonth()+1)+"|"+(d_stvd_startdate.getFullYear()))==((d_from_date.getMonth()+1)+"|"+d_from_date.getFullYear()))
				{
				d_temp_from_date=d_stvd_startdate>d_from_date?d_stvd_startdate : d_from_date; //Update End date as subtier end date in case if its mid of the month
				}
				else
                {
					d_temp_from_date=d_from_date;
				}
				nlapiLogExecution("DEBUG","d_stvd_startdate",d_stvd_startdate);
         	 nlapiLogExecution("DEBUG","d_temp_from_date",d_temp_from_date);
				
				if(((d_stvd_enddate.getMonth()+1)+"|"+(d_stvd_enddate.getFullYear()))==((d_to_date.getMonth()+1)+"|"+(d_to_date.getFullYear())))
				{
						d_temp_end_date=d_stvd_enddate<d_to_date?d_stvd_enddate : d_to_date; //Update End date as subtier end date in case if its mid of the month
				}
				else{
					d_temp_end_date=d_to_date;
				}
				*/
          		nlapiLogExecution("DEBUG","d_stvd_enddate",d_stvd_enddate);
         	 	nlapiLogExecution("DEBUG","d_to_date",d_to_date);
				
				a_vendor_details.push({'st_rate':a_search_vendor_details[i].getValue('custrecord_stvd_st_pay_rate'), 'ot_rate':a_search_vendor_details[i].getValue('custrecord_stvd_ot_pay_rate'), 'payment_terms':a_search_vendor_details[i].getText('custrecord_stvd_payment_terms'), 'vendor_name':a_search_vendor_details[i].getText('custrecord_stvd_vendor')
				//Added by praveena
				,'stvd_startdate':d_temp_from_date,'stvd_enddate':d_temp_end_date
			});
		}
	
	return a_vendor_details;
}

// Populate contractor drop-down based on vendor drop-down
function getContractors(type, name, linenum){
	if(name == 'custpage_subsidiary' && (nlapiGetFieldValue('custpage_subsidiary') == 'UK'))
	{
		//var Subsidiary = nlapiGetFieldValue('custpage_subsidiary')
		//if(Subsidiary == 'UK')
		{
			//var id = 8;
			var vendorSearch = nlapiSearchRecord("vendor",null,
			[
				["msesubsidiary.internalid","anyof","7"]
			], 
			[
				new nlobjSearchColumn("internalid"),
				new nlobjSearchColumn("entityid"),
				new nlobjSearchColumn("altname")				
   
			]
			);
			
				nlapiRemoveSelectOption('custpage_vendor', null);
			
				nlapiInsertSelectOption('custpage_vendor', -1, '', true);
				var vendor_id = '';
				var vendor_name = '';
			for(var i = 0; i < vendorSearch.length; i++)
			{
				vendor_id = vendorSearch[i].getValue('internalid');
			    vendor_name = vendorSearch[i].getValue('entityid');
				var name = vendorSearch[i].getValue('altname');
				var vendor_name1 = vendor_name + ' ' + name;
				nlapiInsertSelectOption('custpage_vendor',vendor_id,vendor_name1,false);
			}
		}
		
	}
	else if(name == 'custpage_subsidiary' && (nlapiGetFieldValue('custpage_subsidiary') == 'BLLC'))
	{
		var vendorSearchResults = nlapiSearchRecord('vendor',null,
		["msesubsidiary.internalid","anyof","2"],
		
		[
				new nlobjSearchColumn("internalid"),
				new nlobjSearchColumn("entityid"),
				new nlobjSearchColumn("altname")	
   
		]
		);
		nlapiRemoveSelectOption('custpage_vendor', null);
			
		nlapiInsertSelectOption('custpage_vendor', -1, '', true);
		
		var vendor_name = '';
		var vendor_id = '';
		
		for(var i = 0; i < vendorSearchResults.length; i++)
		{
			vendor_id = vendorSearchResults[i].getValue('internalid');
			vendor_name = vendorSearchResults[i].getValue('entityid');
			var name = vendorSearchResults[i].getValue('altname');
			var vendor_name1 = vendor_name + ' ' + name;
			nlapiInsertSelectOption('custpage_vendor',vendor_id,vendor_name1,false);
		}
	}
	
	if(name == 'custpage_vendor')
		{
			var strJson	=	nlapiGetFieldValue('custpage_vendor_data');
			var strJson2	=	nlapiGetFieldValue('custpage_vendor_data_dup');
		
			var a_vendor_data	=	JSON.parse(strJson);
			var a_vendor_data2	=	JSON.parse(strJson2);
			var a_entire_vendor_list = a_vendor_data.concat(a_vendor_data2);
			var i_vendor	=	nlapiGetFieldValue('custpage_vendor');
		
			nlapiRemoveSelectOption('custpage_employee', null);
			
			nlapiInsertSelectOption('custpage_employee', -1, '', true);
			
			//for(var i = 0; i < a_vendor_data.length; i++)
			for(var i = 0; i < a_entire_vendor_list.length; i++)
				{
					if(a_entire_vendor_list[i].vendor_id == i_vendor)
						{
							nlapiInsertSelectOption('custpage_employee', a_entire_vendor_list[i].employee_id, a_entire_vendor_list[i].employee_name, false);
						}
				}	
		}
	
 //window.location = 'https://system.sandbox.netsuite.com' + strUrl;
}

