/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       16 Jun 2015     nitish.mishra
 *
 */

/**
 * @param {nlobjRequest}
 *            request Request object
 * @param {nlobjResponse}
 *            response Response object
 * @returns {Void} Any output is written via response object
 */
function suitelet(request, response) {
	try {

		var currentuser = 1582;// nlapiGetUser();
		var employeeList = getEmployeeProjectWise(currentuser);
		nlapiLogExecution('debug', 'search 1 done', employeeList.length);
		var allocationHistory = createAllocationHistoryReport(employeeList,
				currentuser);
		nlapiLogExecution('debug', 'search 2 done', allocationHistory.length);
	} catch (err) {
		nlapiLogExecution('ERROR', 'suitelet', err);
	}
}

// if the user is a PM / DM
function getEmployeeProjectWise(currentuser) {
	try {

		var allocationSearch = nlapiSearchRecord(
				'resourceallocation',
				null,
				[

						[
								[ 'job.custentity_projectmanager', 'anyof',
										currentuser ],
								'or',
								[ 'job.custentity_deliverymanager', 'anyof',
										currentuser ] ],
						'and',
						[ 'enddate', 'notbefore', 'today' ],
						'and',
						[ 'employee.custentity_implementationteam', 'is', 'F' ],
						'and',
						[ 'employee.custentity_employee_inactive', 'is', 'F' ] ],
				[ new nlobjSearchColumn('resource') ]);

		var allocatedResources = [];

		if (allocationSearch) {

			allocationSearch.forEach(function(allocation) {
				allocatedResources.push(allocation.getValue('resource'));
			});
		}

		return allocatedResources;
	} catch (err) {
		nlapiLogExecution('ERROR', 'getAllocationHistoryProjectWise', err);
		throw err;
	}
}

function getEmployeeAllocationHistory(employeeId, currentuser) {
	try {
		var allocationSearch = nlapiSearchRecord(
				'resourceallocation',
				null,
				[ [ 'resource', 'anyof', employeeId ] ],
				[
						new nlobjSearchColumn('resource'),
						new nlobjSearchColumn('project'),
						new nlobjSearchColumn('startdate'),
						new nlobjSearchColumn('enddate'),
						new nlobjSearchColumn('custeventrbillable'),
						new nlobjSearchColumn('percentoftime'),
						new nlobjSearchColumn('custeventbstartdate'),
						new nlobjSearchColumn('custeventbenddate'),
						new nlobjSearchColumn('custeventwlocation'),
						new nlobjSearchColumn('custevent4'),
						new nlobjSearchColumn('custevent_practice'),
						new nlobjSearchColumn('custentity_projectmanager',
								'job'),
						new nlobjSearchColumn('custentity_deliverymanager',
								'job'), new nlobjSearchColumn('jobtype', 'job') ]);

		return allocationSearch;
	} catch (err) {
		nlapiLogExecution('ERROR', 'getEmployeeAllocationHistory', err);
		throw err;
	}
}

function createAllocationHistoryReport(employeeList) {
	try {
		var reportDefinition = nlapiCreateReportDefinition();
		reportDefinition.setTitle('Allocation History');

		reportDefinition.addRowHierarchy('customer', 'Customer', 'TEXT');
		reportDefinition.addRowHierarchy('project', 'Project', 'TEXT');
		reportDefinition.addRowHierarchy('resource', 'Resource', 'TEXT');

		reportDefinition.addColumn('project2', false, 'Project', null, 'TEXT',
				null);
		reportDefinition.addColumn('startdate', false, 'Start Date', null,
				'TEXT', null);
		reportDefinition.addColumn('enddate', false, 'End Date', null, 'TEXT',
				null);
		reportDefinition.addColumn('custeventrbillable', false, 'Is Billable',
				null, 'TEXT', null);
		reportDefinition.addColumn('percentoftime', false, '%age Time', null,
				'TEXT', null);
		reportDefinition.addColumn('custeventbstartdate', false,
				'Billing Start Date', null, 'TEXT', null);
		reportDefinition.addColumn('custeventbenddate', false,
				'Billing End Date', null, 'TEXT', null);
		reportDefinition.addColumn('custevent4', false, 'Site', null, 'TEXT',
				null);
		reportDefinition.addColumn('custevent_practice', false, 'Practice',
				null, 'TEXT', null);
		reportDefinition.addColumn('custentity_projectmanager', false,
				'Project Manager', null, 'TEXT', null);
		reportDefinition.addColumn('custentity_deliverymanager', false,
				'Delivery Manager', null, 'TEXT', null);
		reportDefinition.addColumn('jobtype', false, 'Project Type', null,
				'TEXT', null);

		nlapiLogExecution('debug', 'check 1');

		if (employeeList.length == 0) {
			employeeList.push("@NONE@");
		}

		/*
		 * reportDefinition.addRowHierarchy('startdate', 'Start Date', 'TEXT');
		 * reportDefinition.addRowHierarchy('enddate', 'End Date', 'TEXT');
		 * reportDefinition.addRowHierarchy('custeventrbillable', 'Billable ?',
		 * 'TEXT'); reportDefinition.addRowHierarchy('percentoftime', '%age
		 * Allocation', 'TEXT');
		 * reportDefinition.addRowHierarchy('custeventbstartdate', 'Billing
		 * Start Date', 'TEXT');
		 * reportDefinition.addRowHierarchy('custeventbenddate', 'Billing End
		 * Date', 'TEXT');
		 * reportDefinition.addRowHierarchy('custeventwlocation', 'Location',
		 * 'TEXT'); reportDefinition.addRowHierarchy('custevent4', 'Site',
		 * 'TEXT'); reportDefinition.addRowHierarchy('custevent_practice',
		 * 'Practice', 'TEXT');
		 * reportDefinition.addRowHierarchy('custentity_projectmanager',
		 * 'Project Manager', 'TEXT');
		 * reportDefinition.addRowHierarchy('custentity_deliverymanager',
		 * 'Delivery Manager', 'TEXT');
		 * reportDefinition.addRowHierarchy('jobtype', 'Project Type', 'TEXT');
		 */

		var mapping = {
			'resource' : new nlobjSearchColumn('resource'),
			'project' : new nlobjSearchColumn('project'),
			'project2' : new nlobjSearchColumn('project'),
			'customer' : new nlobjSearchColumn('customer', 'job'),
			'startdate' : new nlobjSearchColumn('startdate'),
			'enddate' : new nlobjSearchColumn('enddate'),
			'custeventrbillable' : new nlobjSearchColumn('custeventrbillable'),
			'percentoftime' : new nlobjSearchColumn('percentoftime'),
			'custeventbstartdate' : new nlobjSearchColumn('custeventbstartdate'),
			'custeventbstartdate' : new nlobjSearchColumn('custeventbenddate'),
			'custeventwlocation' : new nlobjSearchColumn('custeventwlocation'),
			'custevent4' : new nlobjSearchColumn('custevent4'),
			'custevent_practice' : new nlobjSearchColumn('custevent_practice'),
			'custentity_projectmanager' : new nlobjSearchColumn(
					'custentity_projectmanager', 'job'),
			'custentity_deliverymanager' : new nlobjSearchColumn(
					'custentity_deliverymanager', 'job'),
			'jobtype' : new nlobjSearchColumn('jobtype', 'job')
		};

		var filters = [ new nlobjSearchFilter('resource', null, 'anyof',
				employeeList) ];

		var columns = [ new nlobjSearchColumn('resource'),
				new nlobjSearchColumn('project'),
				new nlobjSearchColumn('customer', 'job'),
				new nlobjSearchColumn('startdate'),
				new nlobjSearchColumn('enddate'),
				new nlobjSearchColumn('custeventrbillable'),
				new nlobjSearchColumn('percentoftime'),
				new nlobjSearchColumn('custeventbstartdate'),
				new nlobjSearchColumn('custeventbenddate'),
				new nlobjSearchColumn('custeventwlocation'),
				new nlobjSearchColumn('custevent4'),
				new nlobjSearchColumn('custevent_practice'),
				new nlobjSearchColumn('custentity_projectmanager', 'job'),
				new nlobjSearchColumn('custentity_deliverymanager', 'job'),
				new nlobjSearchColumn('jobtype', 'job') ];

		reportDefinition.addSearchDataSource('resourceallocation', null,
				filters, columns, mapping);

		var form = nlapiCreateReportForm('Allocation History');
		var pvtTable = reportDefinition.executeReport(form);
		response.writePage(form);
	} catch (err) {
		nlapiLogExecution('ERROR', 'createAllocationHistoryReport', err);
		throw err;
	}
}