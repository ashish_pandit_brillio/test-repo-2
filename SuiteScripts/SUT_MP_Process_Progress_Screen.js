/**
 * Show the progress of various stages of the monthly provision
 * 
 * Version Date Author Remarks 1.00 24 Apr 2016 Nitish Mishra
 * 
 */

function suitelet(request, response) {
	try {
		var context = nlapiGetContext();

		switch (context.getDeploymentId()) {

			case "customdeploy_sut_mp_validation_progress":
				if (request.getMethod() == "GET") {
					createValidationLogScreen(request);
				} else {
					callProvisionEntryCreationScript(request);
				}
			break;

			case "customdeploy_sut_mp_provision_progress":
				if (request.getMethod() == "GET") {
					createProvisionCreationProgressScreen(request);
				} else {
					redirectToProvisionEntryDetailScreen(request);
				}
			break;

			case "customdeploy_sut_mp_je_creation_progress":
				if (request.getMethod() == "GET") {
					createJeCreationProgressScreen(request);
				} else {
					closeProvisionMaster(request);
				}
			break;
		}
	} catch (err) {
		nlapiLogExecution('ERROR', 'suitelet', err);
		displayErrorForm(err, "Monthly Provision");
	}
}

function getRunningProvisionMasterId() {
	try {
		// get the currently active monthly provision
		var provisionMasterSearch = nlapiSearchRecord(
		        'customrecord_provision_master', null, [
		                new nlobjSearchFilter('isinactive', null, 'is', 'F'),
		                new nlobjSearchFilter(
		                        'custrecord_pm_is_currently_active', null,
		                        'is', 'T') ]);

		var provisionMasterId = null;
		if (provisionMasterSearch) {
			provisionMasterId = provisionMasterSearch[0].getId();
		}

		return provisionMasterId;
	} catch (err) {
		nlapiLogExecution('ERROR', 'getRunningProvisionMasterId', err);
		throw err;
	}
}

// Entry Validation Progress Screen
function createValidationLogScreen(request) {
	try {
		var masterProvisionId = request.getParameter('master');

		if (!masterProvisionId) {
			masterProvisionId = getRunningProvisionMasterId();
		}

		if (masterProvisionId) {

			var form = nlapiCreateForm("Monthly Provision - Validation");
			form.setScript('customscript_cs_mp_process_progress');

			var masterProvisionField = form.addField('custpage_master_id',
			        'select', 'Provision Master',
			        'customrecord_provision_master');
			masterProvisionField.setDisplayType('inline');
			masterProvisionField.setDefaultValue(masterProvisionId);
			masterProvisionField.setBreakType('startcol');

			var masterProvisionDetails = nlapiLookupField(
			        'customrecord_provision_master', masterProvisionId, [
			                'custrecord_pm_status', 'custrecord_pm_percent',
			                'custrecord_pm_validation_completed',
			                'custrecord_pm_provision_creation_done',
			                'custrecord_pm_je_creation_done' ]);
			nlapiLogExecution('debug', 'masterProvisionDetails', JSON
			        .stringify(masterProvisionDetails));

			var statusField = form
			        .addField('custpage_status', 'text', 'Status');
			statusField.setDisplayType('inline');
			statusField
			        .setDefaultValue(masterProvisionDetails.custrecord_pm_status);
			statusField.setBreakType('startcol');

			var percentCompleteField = form.addField(
			        'custpage_percent_complete', 'text', 'Percentage Complete');
			percentCompleteField.setDisplayType('inline');
			percentCompleteField
			        .setDefaultValue(masterProvisionDetails.custrecord_pm_percent);
			percentCompleteField.setBreakType('startcol');

			var errorLogs = getErrorLogs(masterProvisionId);
			var progressImageField = form.addField('custpage_progress_image',
			        'inlinehtml', "");
			progressImageField.setBreakType('startcol');
			var imgValue = "";

			if (masterProvisionDetails.custrecord_pm_validation_completed != 'T') {
				imgValue = "<img style='margin-top: 20px;' src='https://system.na1.netsuite.com/core/media/media.nl?id=166771&c=3883006&h=ac44164d223a71a32dca&whence='/>";
			} else {

				if (errorLogs.length == 0) {
					imgValue = "<img style='margin-top: 20px; height: 40px;' src='https://system.na1.netsuite.com/core/media/media.nl?id=166770&c=3883006&h=2476ab2d56865ea3716d&whence='/>";
				} else {
					imgValue = "<img style='margin-top: 20px; height: 40px;' src='https://system.na1.netsuite.com/core/media/media.nl?id=166769&c=3883006&h=7824e03164578c2af9b6&whence='/>";
				}
			}

			progressImageField.setDefaultValue(imgValue);

			// create a sublist and pull all the error logs from the custom
			// table related to this master provision record
			var errorList = form.addSubList('custpage_error_logs',
			        'staticlist', 'Error Logs');
			errorList.addField('comments', 'text', 'Comments');
			errorList.setLineItemValues(errorLogs);

			form.addButton('custpage_refresh', 'Refresh', 'refresh');

			if (masterProvisionDetails.custrecord_pm_validation_completed == 'T'
			        && masterProvisionDetails.custrecord_pm_provision_creation_done == 'F'
			        && masterProvisionDetails.custrecord_pm_je_creation_done == 'F') {
				form.addButton('custpage_redo', 'Redo Validation',
				        'redoValidation');

				if (errorLogs.length == 0) {
					form.addSubmitButton("Create Provision Entries");
				}
			}

			response.writePage(form);
		} else {
			throw "No Active Provision Running";
		}
	} catch (err) {
		nlapiLogExecution('ERROR', 'createValidationLogScreen', err);
		throw err;
	}
}

function getErrorLogs(masterProvisionId) {
	try {
		var logSearch = searchRecord(
		        'customrecord_provision_error_log',
		        null,
		        [
		                new nlobjSearchFilter('isinactive', null, 'is', 'F'),
		                new nlobjSearchFilter(
		                        'custrecord_pel_provision_master', null,
		                        'anyof', masterProvisionId) ],
		        [
		                new nlobjSearchColumn('custrecord_pel_error_comment'),
		                new nlobjSearchColumn('custrecord_pel_provision_master') ]);

		var logList = [];
		if (logSearch) {

			for (var i = 0; i < logSearch.length; i++) {
				logList.push({
					comments : logSearch[i]
					        .getValue('custrecord_pel_error_comment')
				});
			}
		}

		return logList;
	} catch (err) {
		nlapiLogExecution('ERROR', 'getErrorLogs', err);
		throw err;
	}
}

function callProvisionEntryCreationScript(request) {
	try {
		var masterProvisionId = request.getParameter('custpage_master_id');

		// check if the provision entry creation script is not already running

		// if not running, call the schedule script
		nlapiScheduleScript('customscript_sch_mp_create_provision',
		        'customdeploy_sch_mp_create_provision');

		nlapiSubmitField('customrecord_provision_master', masterProvisionId, [
		        'custrecord_pm_status', 'custrecord_pm_percent' ], [
		        'Clearing Old Provision Entries', '0%' ]);

		// redirect to provision entry creation progress screen
		response.sendRedirect('SUITELET',
		        'customscript_sut_mp_process_progress',
		        'customdeploy_sut_mp_provision_progress');
	} catch (err) {
		nlapiLogExecution('ERROR', 'callProvisionEntryCreationScript', err);
		throw err;
	}
}

// Provision Entry Progress Screen
function createProvisionCreationProgressScreen(request) {
	try {
		var masterProvisionId = request.getParameter('master');

		if (!masterProvisionId) {
			masterProvisionId = getRunningProvisionMasterId();
		}

		if (masterProvisionId) {

			var form = nlapiCreateForm("Monthly Provision - Provision Entry Creation");
			form.setScript('customscript_cs_mp_process_progress');

			var masterProvisionField = form.addField('custpage_master_id',
			        'select', 'Provision Master',
			        'customrecord_provision_master');
			masterProvisionField.setDisplayType('inline');
			masterProvisionField.setBreakType('startcol');
			masterProvisionField.setDefaultValue(masterProvisionId);

			var masterProvisionDetails = nlapiLookupField(
			        'customrecord_provision_master', masterProvisionId, [
			                'custrecord_pm_status', 'custrecord_pm_percent',
			                'custrecord_pm_validation_completed',
			                'custrecord_pm_provision_creation_done',
			                'custrecord_pm_je_creation_done' ]);

			nlapiLogExecution('debug', 'masterProvisionDetails', JSON
			        .stringify(masterProvisionDetails));

			var statusField = form
			        .addField('custpage_status', 'text', 'Status');
			statusField.setDisplayType('inline');
			statusField
			        .setDefaultValue(masterProvisionDetails.custrecord_pm_status);
			statusField.setBreakType('startcol');

			var percentCompleteField = form.addField(
			        'custpage_percent_complete', 'text', 'Percentage Complete');
			percentCompleteField.setDisplayType('inline');
			percentCompleteField
			        .setDefaultValue(masterProvisionDetails.custrecord_pm_percent);
			percentCompleteField.setBreakType('startcol');

			var errorLogs = getErrorLogs(masterProvisionId);
			var progressImageField = form.addField('custpage_progress_image',
			        'inlinehtml', "");
			progressImageField.setBreakType('startcol');
			var imgValue = "";

			if (masterProvisionDetails.custrecord_pm_provision_creation_done != 'T') {
				imgValue = "<img style='margin-top: 20px;' src='https://system.na1.netsuite.com/core/media/media.nl?id=166771&c=3883006&h=ac44164d223a71a32dca&whence='/>";
			} else {

				if (errorLogs.length == 0) {
					imgValue = "<img style='margin-top: 20px; height: 40px;' src='https://system.na1.netsuite.com/core/media/media.nl?id=166770&c=3883006&h=2476ab2d56865ea3716d&whence='/>";
				} else {
					imgValue = "<img style='margin-top: 20px; height: 40px;' src='https://system.na1.netsuite.com/core/media/media.nl?id=166769&c=3883006&h=7824e03164578c2af9b6&whence='/>";
				}
			}
			progressImageField.setDefaultValue(imgValue);

			// create a sublist and pull all the error logs from the custom
			// table related to this master provision record
			var errorList = form.addSubList('custpage_error_logs',
			        'staticlist', 'Error Logs');
			errorList.addField('comments', 'text', 'Comments');
			errorList.setLineItemValues(errorLogs);

			form.addButton('custpage_refresh', 'Refresh', 'refresh');

			if (masterProvisionDetails.custrecord_pm_validation_completed == 'T'
			        && masterProvisionDetails.custrecord_pm_provision_creation_done == 'T'
			        && masterProvisionDetails.custrecord_pm_je_creation_done == 'F') {
				form.addButton('custpage_redo', 'Redo Provision',
				        'redoCreation');

				if (errorLogs.length == 0) {
					form.addSubmitButton("Show Provision Entry");
				}
			}

			response.writePage(form);
		} else {
			throw "No Active Provision Running";
		}
	} catch (err) {
		nlapiLogExecution('ERROR', 'createProvisionCreationProgressScreen', err);
		throw err;
	}
}

function redirectToProvisionEntryDetailScreen(request) {
	try {
		response.sendRedirect('SUITELET',
		        'customscript_sut_mp_display_entries',
		        'customdeploy_sut_mp_display_entries');
	} catch (err) {
		nlapiLogExecution('ERROR', 'redirectToProvisionEntryDetailScreen', err);
		throw err;
	}
}

// JE creation progress screen
function createJeCreationProgressScreen(request) {
	try {
		var masterProvisionId = request.getParameter('master');

		if (!masterProvisionId) {
			masterProvisionId = getRunningProvisionMasterId();
		}

		if (masterProvisionId) {

			var form = nlapiCreateForm("Monthly Provision - JE Creation");
			form.setScript('customscript_cs_mp_process_progress');

			var masterProvisionField = form.addField('custpage_master_id',
			        'select', 'Provision Master',
			        'customrecord_provision_master');
			masterProvisionField.setDisplayType('inline');
			masterProvisionField.setBreakType('startcol');
			masterProvisionField.setDefaultValue(masterProvisionId);

			var masterProvisionDetails = nlapiLookupField(
			        'customrecord_provision_master', masterProvisionId, [
			                'custrecord_pm_status', 'custrecord_pm_percent',
			                'custrecord_pm_validation_completed',
			                'custrecord_pm_provision_creation_done',
			                'custrecord_pm_je_creation_done' ]);

			nlapiLogExecution('debug', 'masterProvisionDetails', JSON
			        .stringify(masterProvisionDetails));

			var statusField = form
			        .addField('custpage_status', 'text', 'Status');
			statusField.setDisplayType('inline');
			statusField
			        .setDefaultValue(masterProvisionDetails.custrecord_pm_status);
			statusField.setBreakType('startcol');

			var percentCompleteField = form.addField(
			        'custpage_percent_complete', 'text', 'Percentage Complete');
			percentCompleteField.setDisplayType('inline');
			percentCompleteField
			        .setDefaultValue(masterProvisionDetails.custrecord_pm_percent);
			percentCompleteField.setBreakType('startcol');

			var errorLogs = getErrorLogs(masterProvisionId);
			var progressImageField = form.addField('custpage_progress_image',
			        'inlinehtml', "");
			progressImageField.setBreakType('startcol');
			var imgValue = "";

			if (masterProvisionDetails.custrecord_pm_je_creation_done != 'T') {
				imgValue = "<img style='margin-top: 20px;' src='https://system.na1.netsuite.com/core/media/media.nl?id=166771&c=3883006&h=ac44164d223a71a32dca&whence='/>";
			} else {

				if (errorLogs.length == 0) {
					imgValue = "<img style='margin-top: 20px; height: 40px;' src='https://system.na1.netsuite.com/core/media/media.nl?id=166770&c=3883006&h=2476ab2d56865ea3716d&whence='/>";
				} else {
					imgValue = "<img style='margin-top: 20px; height: 40px;' src='https://system.na1.netsuite.com/core/media/media.nl?id=166769&c=3883006&h=7824e03164578c2af9b6&whence='/>";
				}
			}
			progressImageField.setDefaultValue(imgValue);

			// create a sublist of all JE for this monthly provision
			var jeList = form.addSubList('custpage_je_list', 'staticlist',
			        'Journal Entries');
			jeList.addField('id', 'select', 'JE', 'journalentry')
			        .setDisplayType('inline');
			jeList.addField('memo', 'text', 'Memo').setDisplayType('inline');
			jeList.setLineItemValues(getCreatedJEs(masterProvisionId));

			// add an error log sublist
			var errorList = form.addSubList('custpage_error_logs',
			        'staticlist', 'Error Logs');
			errorList.addField('comments', 'text', 'Comments');
			errorList.setLineItemValues(errorLogs);

			if (masterProvisionDetails.custrecord_pm_is_currently_active == 'T'
			        && masterProvisionDetails.custrecord_pm_validation_completed == 'T'
			        && masterProvisionDetails.custrecord_pm_provision_creation_done == 'T'
			        && masterProvisionDetails.custrecord_pm_je_creation_done == 'T') {

				if (errorLogs.length == 0) {
					form.addSubmitButton("Close Provision");
				}
			}

			form.addButton('custpage_refresh', 'Refresh', 'refresh');

			response.writePage(form);
		} else {
			throw "No Active Provision Running";
		}
	} catch (err) {
		nlapiLogExecution('ERROR', 'createJeCreationProgressScreen', err);
		throw err;
	}
}

function closeProvisionMaster(request) {
	try {
		nlapiSubmitField('customrecord_provision_master', request
		        .getParameter('custpage_master_id'),
		        'custrecord_pm_is_currently_active', 'F');
		response.sendRedirect('SUITELET', 'customscript_sut_mp_process_status',
		        'customdeploy_sut_mp_process_status', null, {
			        master : request.getParameter('custpage_master_id')
		        });
	} catch (err) {
		nlapiLogExecution('ERROR', 'closeProvisionMaster', err);
		throw err;
	}
}

function getCreatedJEs(masterProvisionId) {
	try {
		var search = nlapiSearchRecord('journalentry', null,
		        [ new nlobjSearchFilter('custbody_je_provision_master', null,
		                'anyof', masterProvisionId) ], [
		                new nlobjSearchColumn('memo', null, 'group'),
		                new nlobjSearchColumn('internalid', null, 'group') ]);

		var jeList = [];

		if (search) {

			search.forEach(function(searchRow) {
				jeList.push({
				    id : searchRow.getValue('internalid', null, 'group'),
				    memo : searchRow.getValue('memo', null, 'group')
				});
			});
		}

		return jeList;
	} catch (err) {
		nlapiLogExecution('ERROR', 'getCreatedJEs', err);
		throw err;
	}
}

// Client Script
function refresh() {
	window.location.reload();
}

function redoValidation() {
	try {
		var url = nlapiResolveURL('SUITELET',
		        'customscript_sut_mp_process_status',
		        'customdeploy_sut_mp_process_status');
		var responseReturned = nlapiRequestURL(url, {
			custpage_master : nlapiGetFieldValue('custpage_master_id')
		}, null, null, 'POST');

		nlapiSubmitField('customrecord_provision_master',
		        nlapiGetFieldValue('custpage_master_id'), [
		                'custrecord_pm_status', 'custrecord_pm_percent',
		                'custrecord_pm_validation_completed' ], [ 'Restarting',
		                '0%', 'F' ]);

		window.location.reload();
	} catch (err) {
		alert(err.message);
	}
}

function redoCreation() {
	try {
		var url = nlapiResolveURL('SUITELET',
		        'customscript_sut_mp_process_progress',
		        'customdeploy_sut_mp_validation_progress');
		var responseReturned = nlapiRequestURL(url, {
			custpage_master : nlapiGetFieldValue('custpage_master_id')
		}, null, null, 'POST');

		nlapiSubmitField('customrecord_provision_master',
		        nlapiGetFieldValue('custpage_master_id'), [
		                'custrecord_pm_status', 'custrecord_pm_percent',
		                'custrecord_pm_provision_creation_done' ], [
		                'Restarting', '0%', 'F' ]);

		window.location.reload();
	} catch (err) {
		alert(err.message);
	}
}