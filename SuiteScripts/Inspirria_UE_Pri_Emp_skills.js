/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       25 Nov 2019     Nihal Mulani    9423591358
 *
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType
 * 
 * @param {String} type Operation types: create, edit, delete, xedit,
 *                      approve, cancel, reject (SO, ER, Time Bill, PO & RMA only)
 *                      pack, ship (IF only)
 *                      dropship, specialorder, orderitems (PO only) 
 *                      paybills (vendor payments)
 * @returns {Void}
 */
function Pri_beforeSubmitCheckFields(type)
{
	if(type == 'delete')
	{
		try
		{
			var i_recordId = nlapiGetRecordId();
			var body = {};
			var method = "DELETE";
			var url = "https://fuelnode1.azurewebsites.net/skill/"+i_recordId;
			var response = call_node(url,body,method);
			nlapiLogExecution('debug','Delete Mode: url  ',url);
			nlapiLogExecution("DEBUG", "response Delete Mode: ", JSON.stringify(response));
		}
		catch(error)
		{
			nlapiLogExecution('Debug','Error ',error);
		}
	}
}

function Pri_userEventAfterSubmit(type){
	try
	{
		if( type == 'edit' || type == 'xedit')
		{
			var recordObject = nlapiLoadRecord(nlapiGetRecordType(), nlapiGetRecordId());
			var name = recordObject.getFieldValue('custrecord_employee_skill_primary');
			//var custrecord_created_byuser = recordObject.getFieldValue('custrecord_created_byuser');

					var body = {};
					var method;
					method = "POST";
					body.id = Number(nlapiGetRecordId());
					body.name = name;
					
					nlapiLogExecution("DEBUG", "body edit Mode: ", JSON.stringify(body));
					var url = "https://fuelnode1.azurewebsites.net/skill";
					nlapiLogExecution('debug','body  ',JSON.stringify(body));
					body = JSON.stringify(body);
					nlapiLogExecution("DEBUG", "response edit Mode:body ", body);
					var response = call_node(url,body,method);
					nlapiLogExecution("DEBUG", "response edit Mode: ", JSON.stringify(response));
		}
		else if(type == 'create')
		{
			var recordObject = nlapiLoadRecord(nlapiGetRecordType(), nlapiGetRecordId());
			var name = recordObject.getFieldValue('custrecord_employee_skill_primary');
			var custrecord_created_byuser = recordObject.getFieldValue('custrecord_created_byuser');
				var body = {};
				var method;
				method = "POST";
				body.id = Number(nlapiGetRecordId());
				body.name = name;
			//	body.custrecord_created_byuser = custrecord_created_byuser;
				nlapiLogExecution("DEBUG", "body create Mode: ", JSON.stringify(body));
				var url = "https://fuelnode1.azurewebsites.net/skill";
				nlapiLogExecution('debug','body  ',JSON.stringify(body));
				body = JSON.stringify(body);
				nlapiLogExecution("DEBUG", "response edit Mode:body ", body);
				var response = call_node(url,body,method);
				nlapiLogExecution("DEBUG", "response edit Mode: ", JSON.stringify(response));
		}
	}
	catch(error)
	{
		nlapiLogExecution('Debug','Error ',error);
	}
}
function Emp_beforeSubmitCheckFields(type)
{
	if(type == 'delete')
	{
		try
		{
			var i_recordId = nlapiGetRecordId();
			var body = {};
			var method = "DELETE";
			var url = "https://fuelnode1.azurewebsites.net/employee_skill/"+i_recordId;
			var response = call_node(url,body,method);
			nlapiLogExecution('debug','Delete Mode: url  ',url);
			nlapiLogExecution("DEBUG", "response Delete Mode: ", JSON.stringify(response));
		}
		catch(error)
		{
			nlapiLogExecution('Debug','Error ',error);
		}
	}
}
function Emp_userEventAfterSubmit(type){
	try
	{
		if( type == 'edit' || type == 'xedit')
		{
			var recordObject = nlapiLoadRecord(nlapiGetRecordType(), nlapiGetRecordId());
			var custrecord_skill_resource = recordObject.getFieldValue('custrecord_skill_resource');
			var custrecord_resource_skillset = recordObject.getFieldValue('custrecord_resource_skillset');
			var custrecord_resource_proficency = recordObject.getFieldValue('custrecord_resource_proficency');
			
					var body = {};
					var method;
					method = "POST";
					body.recordInternalId = Number(nlapiGetRecordId());
					body.employeeInternalId = Number(custrecord_skill_resource);
					body.skillId = Number(custrecord_resource_skillset);
					body.proficiency = Number(custrecord_resource_proficency);
					
					nlapiLogExecution("DEBUG", "body edit Mode: ", JSON.stringify(body));
					var url = "https://fuelnode1.azurewebsites.net/employee_skill";
					nlapiLogExecution('debug','body  ',JSON.stringify(body));
					body = JSON.stringify(body);
					nlapiLogExecution("DEBUG", "response edit Mode:body ", body);
					var response = call_node(url,body,method);
					nlapiLogExecution("DEBUG", "response edit Mode: ", JSON.stringify(response));
		}
		else if(type == 'create')
		{
			var recordObject = nlapiLoadRecord(nlapiGetRecordType(), nlapiGetRecordId());
			var custrecord_skill_resource = recordObject.getFieldValue('custrecord_skill_resource');
			var custrecord_resource_skillset = recordObject.getFieldValue('custrecord_resource_skillset');
			var custrecord_resource_proficency = recordObject.getFieldValue('custrecord_resource_proficency');
			
					var body = {};
					var method;
					method = "POST";
					
					body.recordInternalId = Number(nlapiGetRecordId());
					body.employeeInternalId = Number(custrecord_skill_resource);
					body.skillId = Number(custrecord_resource_skillset);
					body.proficiency = Number(custrecord_resource_proficency);
					
				nlapiLogExecution("DEBUG", "body create Mode: ", JSON.stringify(body));
				var url = "https://fuelnode1.azurewebsites.net/employee_skill";
				nlapiLogExecution('debug','body  ',JSON.stringify(body));
				body = JSON.stringify(body);
				nlapiLogExecution("DEBUG", "response edit Mode:body ", body);
				var response = call_node(url,body,method);
				nlapiLogExecution("DEBUG", "response edit Mode: ", JSON.stringify(response));
		}
	}
	catch(error)
	{
		nlapiLogExecution('Debug','Error ',error);
	}
}
