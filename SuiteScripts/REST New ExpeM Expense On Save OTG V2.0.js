/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       05 Aug 2016     deepak.srinivas
 * 2.00 	   19-oct-2020    Shravan Kulkarni,  Added the Status for response refernce
 */

/**
 * @param {Object} dataIn Parameter object
 * @returns {Object} Output object
 */
function postRESTlet(dataIn) {

    try {
        var response = new Response();
        var current_date = nlapiDateToString(new Date());
        //Log for current date
       /* var dataIn = [];
		dataIn = {
    "RequestType": "GET",
    "Data": {
      "Body": {
        "Purpose": "",
        "Project": "BRTP02979 OnTheGo",
        "ExpID": "",
        "Emailid": "ankit.singh1@brillio.com",
        "Complete": true
      },
      "Lines": {
        "Edit": [
        ],
        "New": [
          {
            "RefNo": "N",
            "Amount": 10,
            "Date": "03/26/2019",
            "Memo": "",
            "Currency": "INR",
            "Category": "Refreshments -Corporate",
            "ImgData": "/9j/4AAQSkZJRgABAQAAAQABAAD/4gIoSUNDX1BST0ZJTEUAAQEAAAIYAAAAAAIQAABtbnRyUkdCIFhZWiAAAAAAAAAAAAAAAABhY3NwAAAAAA",
            "Isbillable": true
          },
          {
            "RefNo": "N",
            "Amount": 20,
            "Date": "03/26/2020",
            "Memo": "",
            "Currency": "INR",
            "Category": "Refreshments -Corporate123",
            "ImgData": "",
            "Isbillable": true
          },
        ],
        "Delete": [
          
        ]
      }
    }
  }*/
        //Data 
        nlapiLogExecution('DEBUG', 'Current Date', 'current_date...' + current_date)
        nlapiLogExecution('debug', 'dataIn', JSON.stringify(dataIn));

        var employeeId = getUserUsingEmailId(dataIn.Data.Body.Emailid);
        var requestType = dataIn.RequestType;
        var expense_ID = dataIn.Data.Body.ExpID;

        nlapiLogExecution('debug', 'requestType', requestType);
        nlapiLogExecution('debug', 'employeeId requestType expense_ID', employeeId + requestType + expense_ID);


        //Added logic for expense delete .. 6th FEB 2016
        if (expense_ID)
            var i_expense_Id = getExpenseInternalID(expense_ID);
        /*var employeeId =  getUserUsingEmailId(Email_Id);

        var requestType = 'UPDATE';
        //var employeeId = getUserUsingEmailId(Email_Id);
        
        //var expense_ID = dataIn.expenseId;
        var expense_ID = 'EXP7089';*/
        //var project_ID = 'Information System';

        //nlapiLogExecution('DEBUG', 'Current Date', 'employeeId' + employeeId);
        //	nlapiLogExecution('DEBUG', 'Current Date', 'project_ID' + project_ID);
        //var requestType = dataIn.RequestType;

        /*for(var  i=0; i< obj.category.length;i++){
        	var id_1 = obj.category[i].id;
        	var category = obj.category[i].category;
        	var category = obj.category[i].category;
        	var amount = obj.category[i].amount;
        	var project = obj.category[i].project;
        	var currency = obj.category[i].currency;
        	var Memo = obj.category[i].Memo;
        	
        }*/
        switch (requestType) {

            case M_Constants.Request.Get:

                if (employeeId) {
                    response.Data = onSaveEventExpense(dataIn, employeeId);
                    response.Status = true;
                } else {
                    response.Data = "Some error with the data sent";
                    response.Status = false;
                }
                break;
            case M_Constants.Request.GetAll:
                if (employeeId) {
                    response.Data = getExpensesList(employeeId);
                    response.Status = true;
                } else {
                    response.Data = "Some error with the data sent";
                    response.Status = false;
                }
                break;
            case M_Constants.Request.GetOne:
                if (_logValidation(expense_ID) && _logValidation(i_expense_Id)) {
                    response.Data = getExpensesDetails(expense_ID);
                    response.Status = true;
                } else {
                    response.Data = "Particular expense deleted !!";
                    response.Status = false;
                }
                break;
            case M_Constants.Request.Update:
                if (expense_ID) {
                    response.Data = updateExpense(expense_ID, dataIn);
                    response.Status = true;
                } else {
                    response.Data = "Cannot call without expenseID";
                    response.Status = false;
                }
                break;
            case M_Constants.Request.Delete:
                if (expense_ID) {
                    response.Data = deleteExpenseReport(expense_ID, dataIn);
                    response.Status = true;
                } else {
                    response.Data = "Cannot call without expenseID";
                    response.Status = false;
                }
                break;
        }

    } catch (err) {
        nlapiLogExecution('ERROR', 'postRESTlet', err);
        response.Data = err;
        response.Status = false;
    }
    nlapiLogExecution('debug', 'response', JSON.stringify(response));
    return response;
}

//Deleting The expense Report
function deleteExpenseReport(expense_ID, dataIn) {

    try {
        var getExpensID = getExpenseInternalID(expense_ID);
        var loadRec = nlapiLoadRecord('expensereport', getExpensID);
        var lineCount = loadRec.getLineItemCount('expense');
        var status = loadRec.getFieldValue('status');
        /*if(status =='Approved' || status == 'Approved (Overridden) by Accounting' || status == 'Approved by Accounting'){
        	throw 'Cannot delete approved expenses';
        }*/
        var JsonCategory = {};
        var data_row = {};
        var dataRowsDelete = [];
        var dataRowsEdit = [];
        var dataRowsNew = [];
        var data = dataIn.Data.Lines;
        var obj = data.Delete;
        if (_logValidation(obj)) {
            for (var i = 0; i < obj.length; i++) {
                for (var count = 1; count <= lineCount; count++) {

                    var tempref = loadRec.getLineItemValue('expense', 'refnumber', count);
                    nlapiLogExecution('DEBUG', 'tempref', tempref);

                    var slno = obj[i].RefNo;
                    if ((slno != 'N') && (tempref == slno)) {

                        loadRec.removeLineItem('expense', count);
                    }
                    JsonCategory = {
                        RefNo: slno,
                        refId: obj[i].refId
                    }
                }
                dataRowsDelete.push(JsonCategory);
            }

        }

        nlapiSubmitRecord(loadRec);

        var loadExp = nlapiLoadRecord('expensereport', getExpensID);
        var amount = loadExp.getFieldValue('total');
        data_Row = {
            Total: amount
        }
        json_expenseID = {
            //ExpenseID:expenseID_T,
            Body: data_Row,
            Lines: {
                Edit: dataRowsEdit,
                New: dataRowsNew,
                Delete: dataRowsDelete,
            }
        };
        return json_expenseID;
    } catch (e) {

        nlapiLogExecution('ERROR', 'Error in deleting the expense', e);
        throw e;
    }
}
//Updating the expense details
function updateExpense(exp_id, dataIn) 
{
    try {

        nlapiLogExecution('DEBUG', 'JSON DATA in Update', JSON.stringify(dataIn));

        var expense_InternalID = getExpenseInternalID(exp_id);
        nlapiLogExecution('DEBUG', 'expense_InternalID', expense_InternalID);

        /*	var loadExpense = nlapiLoadRecord('expensereport',expense_InternalID);
        	
        	var lineCount = loadExpense.getLineItemCount('expense');
        	
        	nlapiSubmitRecord(loadExpense);*/

        nlapiLogExecution('DEBUG', 'JSON DATA in Update expense_InternalID', expense_InternalID);

        var dataobj = dataIn.Data.Lines;

        nlapiLogExecution('DEBUG', 'JSON DATA in Update expense_InternalID', dataobj);

        var update_data = dataobj.Edit;
        var new_data = dataobj.New;
        var delete_data = dataobj.Delete;
        nlapiLogExecution('Debug', 'update_data length', update_data.length);
        nlapiLogExecution('Debug', 'new_data length', new_data.length);
        nlapiLogExecution('Debug', 'delete_data length', delete_data.length);
        nlapiLogExecution('Debug', 'Complete Rahul', dataIn.Data.Body.Complete);
	
        if ((update_data.length == 0) && (new_data.length == 0) && (delete_data.length == 0) && dataIn.Data.Body.Complete == true)
			{
            nlapiLogExecution('Debug', 'No data and complete is true');
            var loadExpense = nlapiLoadRecord('expensereport', expense_InternalID);
            var lineCount = loadExpense.getLineItemCount('expense');
            var purpose = dataIn.Data.Body.Purpose;
            nlapiLogExecution('DEBUG', 'Purpose ->', purpose)
            loadExpense.setFieldValue('memo', purpose);
            var project = projectSplit(dataIn.Data.Body.Project);

            nlapiLogExecution('DEBUG', 'projectSplit', project);
            var projectInternalID = projectDetailSearch(project);

            if (!_logValidation(projectInternalID)) {
                throw ('Please select valid project.')
            }
            nlapiLogExecution('DEBUG', 'projectInternalID', projectInternalID);

            for (var i = 1; i <= lineCount; i++) { //loadExpense.selectLineItem('expense',i);	
                var billable = loadExpense.getLineItemValue('expense', 'isbillable', i);
                nlapiLogExecution('Debug', 'Billable to Customer', billable);
                loadExpense.setLineItemValue('expense', 'customer', i, projectInternalID);
                loadExpense.setLineItemValue('expense', 'isbillable', i, billable);
                loadExpense.commitLineItem('expense');
            }
            loadExpense.setFieldValue('complete', 'T');
            nlapiSubmitRecord(loadExpense);
            //return ({status : true});
        } 
		else 
		{
            nlapiLogExecution('DEBUG', 'JSON DATA in Update expense_InternalID', update_data);
            //var data_Row = {};
            //var dataRowsNew = [];
            //var dataRowsEdit = [];
            //var dataRowsDelete = [];
            if (_logValidation(update_data))
				{
                var identifier = 0;
                var loadExpense = nlapiLoadRecord('expensereport', expense_InternalID);
                var check_status = loadExpense.getFieldValue('status'); //statusRef
                var check_status_ref = loadExpense.getFieldValue('statusRef');
                nlapiLogExecution('DEBUG', 'check_status_ref', check_status_ref);
                nlapiLogExecution('DEBUG', 'check_status', check_status); //supervisorapproval accountingapproval complete
                var check_supervisor_approval = loadExpense.getFieldValue('supervisorapproval');
                var check_accountingapproval = loadExpense.getFieldValue('accountingapproval');
                var check_complete = loadExpense.getFieldValue('complete');
                nlapiLogExecution('DEBUG', 'check_supervisor_approval', check_supervisor_approval);
                nlapiLogExecution('DEBUG', 'check_accountingapproval', check_accountingapproval);
                nlapiLogExecution('DEBUG', 'check_complete', check_complete);
                //if(check_status != 'In Progress' || check_status != 'inProgress' || check_status_ref != 'inProgress'){
                if (check_supervisor_approval == 'T' || check_accountingapproval == 'T' || check_complete == 'T') {
                    throw 'Expense Already Submitted';
                }
                var empID = loadExpense.getFieldValue('entity');
                var employeeLookup_U = nlapiLookupField('employee', parseInt(empID), ['entityid']);
					nlapiLogExecution('DEBUG', 'check_status_Shravan', check_status);
                var employee_full_name_U = employeeLookup_U.entityid;
                //Get data

                var complete = dataIn.Data.Body.Complete;
                nlapiLogExecution('debug', 'complete', complete);

                //loadExpense.setFieldValue('complete','T');

                //var update = dataIn.Data.Body;
                //var update_obj = update.Lines.Edit;

                var purpose = dataIn.Data.Body.Purpose;
                loadExpense.setFieldValue('memo', purpose);


                //var obj = {};
                //var obj = update_data.category;
                //nlapiLogExecution('debug', 'Inside Save obj', JSON.stringify(obj));
                //nlapiLogExecution('DEBUG','Expenses Length',obj.length);

                //Removing Line Items
                /*var lineCount = loadExpense.getLineItemCount('expense');
                nlapiLogExecution('DEBUG','Update Expenses Line Length',lineCount);
                for(var n=lineCount ; n>=1; n--){
                loadExpense.removeLineItem('expense',n);
                }
                */
                var refno = "";
                var lineCount = loadExpense.getLineItemCount('expense')
                for (var j = 0; j < update_data.length; j++) {
                    //loadExpense.selectNewLineItem('expense');

                    //var obj = update_data.category;
                    for (var lc = 1; lc <= lineCount; lc++) {

                        var tempref = loadExpense.getLineItemValue('expense', 'refnumber', lc);
                        refno = update_data[j].RefNo
                        if (tempref == refno)
							{
                            loadExpense.selectLineItem('expense', lc);
                            //loadExpense.setCurrentLineItemValue('expense','refnumber',update_data[j].RefNo);
                            //nlapiLogExecution('DEBUG','ID',refno);			
                            loadExpense.setCurrentLineItemValue('expense', 'memo', update_data[j].Memo);
                            nlapiLogExecution('DEBUG', 'Memo', update_data[j].Memo);

                            nlapiLogExecution('DEBUG', 'catProject', dataIn.Data.Body.Project);
							nlapiLogExecution('DEBUG', 'catProject_Shravan', dataIn.Data.Body.Project);
                            var isbillable = update_data[j].isbillable;
                            isbillable = (isbillable == true) ? 'T' : 'F';
                            loadExpense.setCurrentLineItemValue('expense', 'isbillable', isbillable);

                            var project = projectSplit(dataIn.Data.Body.Project);

                            nlapiLogExecution('DEBUG', 'projectSplit', project);
                            var projectInternalID = projectDetailSearch(project);
                            if (!_logValidation(projectInternalID)) {
                                throw ('Please select valid project.')
                            }
                            nlapiLogExecution('DEBUG', 'projectInternalID', projectInternalID);


                            loadExpense.setCurrentLineItemValue('expense', 'customer', projectInternalID);
                            loadExpense.setCurrentLineItemText('expense', 'category', update_data[j].Category);
                            nlapiLogExecution('DEBUG', 'category', update_data[j].Category);
                            if ((update_data[j].ImgData != '' || update_data[j].ImgData != null) && update_data[j].ImgData) {
                                //Update Image
                                var IMG_currentDate = new Date();
                                //Search For Employee Folder
                                var folderID = null;
                                var filters = Array();
                                filters.push(new nlobjSearchFilter('name', null, 'startswith', employee_full_name_U));
								filters.push(new nlobjSearchFilter('parent', null, 'anyof','@NONE@')); /// new addeed as per the Folder Change
					// by shravan

                                var cols = Array();
                                cols.push(new nlobjSearchColumn('internalid'));

                                var searchFolder = nlapiSearchRecord('folder', null, filters, cols);
                                if (searchFolder) {
                                    folderID = searchFolder[0].getValue('internalid');
                                    nlapiLogExecution('DEBUG', 'Image Folder For Existing Employee ID', folderID);
                                }
                                if (!folderID) {
                                    var folder = nlapiCreateRecord('folder');

                                    if (folder) {

                                        // folder.setFieldValue('parent', '10'); // create root level folder

                                        folder.setFieldValue('name', employee_full_name_U);

                                        folderID = nlapiSubmitRecord(folder);
                                        nlapiLogExecution('DEBUG', 'Image Folder For New Employee ID', folderID);
                                    }

                                }

								nlapiLogExecution('debug', 'update_data[j].ImgdataPDF===', update_data[j].ImgdataPDF);
						if( (update_data[j].ImgdataPDF) == true) //// New key value on 24 may 2021
						{
								var obj_Pdf_File = nlapiCreateFile(employeeID +' ' + IMG_currentDate + '-' + j + '.pdf', 'PDF', update_data[j].ImgData);
									obj_Pdf_File.setFolder(folderID); //set the folder here
								var i_Image_Ref_id = nlapiSubmitFile(obj_Pdf_File);
								nlapiLogExecution('debug', 'i_Image_Ref_id', i_Image_Ref_id);
								record.setCurrentLineItemValue('expense', 'expmediaitem', i_Image_Ref_id);
						} ////// if( (temp[j].ImgdataPDF) == true) 
						else{
								var newImg = nlapiCreateFile(employeeID + ' ' + 'rImage' + ' ' + IMG_currentDate + '-' + j + '.png', 'PJPGIMAGE', update_data[j].ImgData);
								//newImg.setFolder(105120); //set the folder here
								newImg.setFolder(folderID); //set the folder here
								var id2 = nlapiSubmitFile(newImg);
								nlapiLogExecution('debug', 'Image id2', id2);
								record.setCurrentLineItemValue('expense', 'expmediaitem', id2);
							} //// Else of if( (temp[j].ImgdataPDF) == true)


       
                            }
                            //End Image Upload
                            loadExpense.setCurrentLineItemValue('expense', 'amount', '');
                            loadExpense.setCurrentLineItemValue('expense', 'foreignamount', update_data[j].Amount);
                            loadExpense.setCurrentLineItemText('expense', 'currency', update_data[j].Currency);


                            var date_ = update_data[j].Date;
                            var tranDate = formatDate(date_);
                            nlapiLogExecution('DEBUG', 'tranDate', tranDate);
                            loadExpense.setCurrentLineItemValue('expense', 'expensedate', update_data[j].Date);
                            //Need to add more fields based on XML tag i,e record &xml=T
                            loadExpense.commitLineItem('expense');

                        }
                    }
                }

                if (dataIn.Data.Body.Complete == false) 
				{
                    nlapiLogExecution('Debug', 'Update Disable Trigger', dataIn.Data.Body.Complete);
                    var id = nlapiSubmitRecord(loadExpense, {
                        disabletriggers: true,
                        enablesourcing: false
                    });
                } else {
                    var id = nlapiSubmitRecord(loadExpense, true, true);
					nlapiLogExecution('DEBUG', 'Shravan','shravan');
                }
                //var id = nlapiSubmitRecord(loadExpense);
                nlapiLogExecution('DEBUG', ' Updated the expense expense_ID ', 'expense_ID:' + id);
                if (id) {
                    var lookUp_Expense = nlapiLookupField('expensereport', id, ['tranid']);
                    var expenseID_T = lookUp_Expense.tranid;
                }
                nlapiLogExecution('DEBUG', ' Response the expense expense_ID', 'expense_ID:' + expenseID_T);

                var json_body = {};
                var JsonCategory = {};
                var data_Row = {};
                var dataRowsEdit = [];

                var expense_details = nlapiLoadRecord('expensereport', id);
                var editcount = expense_details.getLineItemCount('expense');
				var s_Status_Ref = expense_details.getFieldValue('status');
                nlapiLogExecution('DEBUG','s_Status_Ref==', s_Status_Ref);
                nlapiLogExecution('DEBUG', 'Edit Count', editcount);
                var img, file, data;
                //var otg_temp = 0;
                //var refno = 0;
                for (var sr = 1; sr <= editcount; sr++) 
				{
                    //var track = expense_details.selectLineItem('expense',sr);
                    //nlapiLogExecution('Debug','Exp details',track);
                    var ref = expense_details.getLineItemValue('expense', 'refnumber', sr);
                    nlapiLogExecution('Debug', 'Expe details refnumber', ref);
                    for (var rn = 0; rn < update_data.length; rn++)
						{

                        //nlapiLogExecution('Debug','Ref',JSON.stringify(update_data));

                        //if(_logValidation(update_data[otg_temp]))
                        //{
                        //	var refno = update_data[otg_temp].RefNo;
                        //
                        var refno = update_data[rn].RefNo;
                        nlapiLogExecution('Debug', 'Array REfNo', refno);
                        if (ref == refno) 
						{
                            var img = '';
                            var data = '';
                            var file = '';
                            img = expense_details.getLineItemValue('expense', 'expmediaitem', sr);
                            if (img) 
							{
                                file = nlapiLoadFile(img);
                                data = file.getValue();
                                nlapiLogExecution('debug', 'Img data', 'line:' + sr + ',' + 'data:' + data);
                            }
						
                            json_body = {
								Status:s_Status_Ref,
                                Date: expense_details.getFieldValue('trandate'),
                                ExpID: expense_details.getFieldValue('tranid'),
                                Employee: expense_details.getFieldText('entity'),
                                Project: expense_details.getLineItemValue('expense', 'custcolprj_name', sr),
                                From: expense_details.getFieldValue('trandate'),
                                To: expense_details.getFieldValue('duedate'),
                                Purpose: expense_details.getFieldValue('memo'),
                                Total: expense_details.getFieldValue('total'),
                                Total_Non_reimbursable: expense_details.getFieldValue('nonreimbursable'),
                                Total_Due: expense_details.getFieldValue('foreignamount'),
								InternalId : id
                            };
                            var billable = expense_details.getLineItemValue('expense', 'isbillable', sr);
                            billable = (billable == 'T') ? true : false
                            data_Row = json_body;
							nlapiLogExecution('Debug', 'data_Row==', data_Row);
                            JsonCategory = {
                                RefNo: expense_details.getLineItemValue('expense', 'refnumber', sr),
                                //Category: expense_details.getLineItemValue('expense','category_display',sr),
                                //Currency: expense_details.getLineItemText('expense','currency',sr),
                                //Memo: expense_details.getLineItemValue('expense','memo',sr),
                                //refId : update_data[otg_temp].refId,
                                //Billable: billable,
                                //Date : 	expense_details.getLineItemValue('expense','expensedate',sr),
                                //Project : 	expense_details.getLineItemValue('expense','custcolprj_name',sr),
                                //Department : 	expense_details.getLineItemValue('expense','department_display',sr),
                                //Vertical : 	expense_details.getLineItemValue('expense','class_display',sr),
                                //Amount: expense_details.getLineItemValue('expense','amount',sr), //customer_display,
                                //imgData: data

                            }
                            dataRowsEdit.push(JsonCategory);

                        }
                        //otg_temp++
                    }

                    //dataRowsEdit.push(JsonCategory);

                }
                //var submit_update = nlapiSubmitRecord(loadExpense);	
                nlapiLogExecution('Debug', 'Update Response', dataRowsEdit);
				nlapiLogExecution('debug', 'dataRowsEdit', JSON.stringify(dataRowsEdit));
				nlapiLogExecution('Debug', 'Update json_body', json_body);
				nlapiLogExecution('debug', 'json_body', JSON.stringify(json_body));
            }



            if (_logValidation(new_data))
				{

                var identifier = 0;
                var loadExpense = nlapiLoadRecord('expensereport', expense_InternalID);
                var check_status = loadExpense.getFieldValue('status'); //statusRef
                var check_status_ref = loadExpense.getFieldValue('statusRef');
                nlapiLogExecution('DEBUG', 'check_status_ref', check_status_ref);
                nlapiLogExecution('DEBUG', 'check_status', check_status); //supervisorapproval accountingapproval complete
                var check_supervisor_approval = loadExpense.getFieldValue('supervisorapproval');
                var check_accountingapproval = loadExpense.getFieldValue('accountingapproval');
                var check_complete = loadExpense.getFieldValue('complete');
                nlapiLogExecution('DEBUG', 'check_supervisor_approval', check_supervisor_approval);
                nlapiLogExecution('DEBUG', 'check_accountingapproval', check_accountingapproval);
                nlapiLogExecution('DEBUG', 'check_complete', check_complete);
                //if(check_status != 'In Progress' || check_status != 'inProgress' || check_status_ref != 'inProgress'){
                if (check_supervisor_approval == 'T' || check_accountingapproval == 'T' || check_complete == 'T') {
                    throw 'Expense Already Submitted';
                }
                var empID = loadExpense.getFieldValue('entity');
                var employeeLookup_U = nlapiLookupField('employee', parseInt(empID), ['entityid']);

                var employee_full_name_U = employeeLookup_U.entityid;
                //Get data

                var complete = dataIn.Data.Body.Complete;
                nlapiLogExecution('debug', 'complete', complete);

                /*if(complete == true){
                	loadExpense.setFieldValue('complete','T');
                	return ({status:true})
                }*/


                //loadExpense.setFieldValue('complete','T');

                var purpose = dataIn.Data.Body.Purpose;
                loadExpense.setFieldValue('memo', purpose);


                //var obj ={};
                //var obj = dataIn.Data.obj.category;
                //nlapiLogExecution('debug', 'Inside Save obj', JSON.stringify(obj));
                //nlapiLogExecution('DEBUG','Expenses Length',obj.length);

                //Removing Line Items
                var lineCount = loadExpense.getLineItemCount('expense');
                //nlapiLogExecution('DEBUG','Update Expenses Line Length',lineCount);
                //for(var n=lineCount ; n>=1; n--){
                //loadExpense.removeLineItem('expense',n);
                //}

                //var tempRefNo = lineCount;
                var line = 0;
                for (var tl = 1; tl <= lineCount; tl++) {
                    loadExpense.selectLineItem('expense', lineCount);
                    line = loadExpense.getCurrentLineItemValue('expense', 'refnumber');
                    loadExpense.commitLineItem('expense');
                }
                nlapiLogExecution('DEBUG', 'No of lines', line)
                if (parseInt(line) < lineCount) {
                    line = lineCount;
                }
                nlapiLogExecution('DEBUG', 'No of lines new', line)
                for (var j = 0; j < new_data.length; j++) {
                    loadExpense.selectNewLineItem('expense');
                    loadExpense.setCurrentLineItemValue('expense', 'refnumber', parseInt(line) + 1);
                    nlapiLogExecution('DEBUG', 'New Ref Number', parseInt(line) + 1);

                    loadExpense.setCurrentLineItemValue('expense', 'memo', new_data[j].Memo);
                    nlapiLogExecution('DEBUG', 'Memo', new_data[j].Memo);

                    var isbillable = new_data[j].isbillable;
                    isbillable = (isbillable == true) ? 'T' : 'F';
                    loadExpense.setCurrentLineItemValue('expense', 'isbillable', isbillable);

                    nlapiLogExecution('DEBUG', 'catProject', new_data[j].Project);

                    var project = projectSplit(dataIn.Data.Body.Project);

                    nlapiLogExecution('DEBUG', 'projectSplit', new_data[j].Project);
                    var projectInternalID = projectDetailSearch(project);

                    if (!_logValidation(projectInternalID)) {
                        throw ('Please select valid project.')
                    }

                    nlapiLogExecution('DEBUG', 'projectInternalID', projectInternalID);

                    loadExpense.setCurrentLineItemValue('expense', 'customer', projectInternalID);
                    loadExpense.setCurrentLineItemText('expense', 'category', new_data[j].Category);
                    nlapiLogExecution('DEBUG', 'category', new_data[j].Category);
                    if ((new_data[j].ImgData != '' || new_data[j].ImgData != null) && new_data[j].ImgData) {
                        //Update Image
                        var IMG_currentDate = nlapiDateToString(new Date(), 'datetimetz');
                        //Search For Employee Folder
                        var folderID = null;
                        var filters = Array();
                        filters.push(new nlobjSearchFilter('name', null, 'startswith', employee_full_name_U));
						filters.push(new nlobjSearchFilter('parent', null, 'anyof','@NONE@')); /// new addeed as per the Folder Change
					// by shravan
                        var cols = Array();
                        cols.push(new nlobjSearchColumn('internalid'));

                        var searchFolder = nlapiSearchRecord('folder', null, filters, cols);
                        if (searchFolder) {
                            folderID = searchFolder[0].getValue('internalid');
                            nlapiLogExecution('DEBUG', 'Image Folder For Existing Employee ID', folderID);
                        }
                        if (!folderID) {
                            var folder = nlapiCreateRecord('folder');

                            if (folder) {

                                // folder.setFieldValue('parent', '10'); // create root level folder

                                folder.setFieldValue('name', employee_full_name_U);

                                folderID = nlapiSubmitRecord(folder);
                                nlapiLogExecution('DEBUG', 'Image Folder For New Employee ID', folderID);
                            }

                        }
                        identifier++;
						nlapiLogExecution('debug', 'new_data[j].ImgdataPDF===', new_data[j].ImgdataPDF);
						if( (new_data[j].ImgdataPDF) == true) //// New key value on 24 may 2021
						{
								var obj_Pdf_File = nlapiCreateFile(employeeID +' ' + IMG_currentDate + '-' + j + '.pdf', 'PDF', new_data[j].ImgData);
									obj_Pdf_File.setFolder(folderID); //set the folder here
								var i_Image_Ref_id = nlapiSubmitFile(obj_Pdf_File);
								nlapiLogExecution('debug', 'i_Image_Ref_id', i_Image_Ref_id);
								loadExpense.setCurrentLineItemValue('expense', 'expmediaitem', i_Image_Ref_id);
						} ////// if( (temp[j].ImgdataPDF) == true) 
						else{
								var newImg = nlapiCreateFile(employeeID + ' ' + 'rImage' + ' ' + IMG_currentDate + '-' + j + '' + identifier + '.png', 'PJPGIMAGE', new_data[j].ImgData);
								//newImg.setFolder(105120); //set the folder here
								newImg.setFolder(folderID); //set the folder here
								var id2 = nlapiSubmitFile(newImg);
								nlapiLogExecution('debug', 'Image id2', id2);
								loadExpense.setCurrentLineItemValue('expense', 'expmediaitem', id2);
							} //// Else of if( (temp[j].ImgdataPDF) == true)
                     
                    }
                    //End Image Upload
                    loadExpense.setCurrentLineItemValue('expense', 'foreignamount', new_data[j].Amount);
                    loadExpense.setCurrentLineItemText('expense', 'currency', new_data[j].Currency);

                    var date_ = new_data[j].Date;
                    var tranDate = formatDate(date_);
                    nlapiLogExecution('DEBUG', 'tranDate', tranDate);
                    loadExpense.setCurrentLineItemValue('expense', 'expensedate', new_data[j].Date);
                    //Need to add more fields based on XML tag i,e record &xml=T
                    loadExpense.commitLineItem('expense');
                    line++


                }
                //loadExpense.setFieldValue('complete','T')
                /*if(dataIn.Data.Body.Complete == false)
                {
                	var id  = nlapiSubmitRecord(loadExpense, {disabletriggers : true, enablesourcing : false});
                }*/


                var id = nlapiSubmitRecord(loadExpense);

                //var id = nlapiSubmitRecord(loadExpense);
                nlapiLogExecution('DEBUG', ' Updated the expense expense_ID', 'expense_ID:' + id);
                if (id) {
                    var lookUp_Expense = nlapiLookupField('expensereport', id, ['tranid']);
                    var expenseID_T = lookUp_Expense.tranid;
                }
                nlapiLogExecution('DEBUG', ' Response the expense expense_ID', 'expense_ID:' + expenseID_T);

                var json_body = {};

                //var data_Row = {};
                var dataRowsNew = [];
                var refID = '';
                var expense_details = nlapiLoadRecord('expensereport', id);
                var s_Status_Ref = expense_details.getFieldValue('status');
                nlapiLogExecution('DEBUG','s_Status_Ref==', s_Status_Ref);
                
                var newcount = expense_details.getLineItemCount('expense');
                var temp_otg = 0
                for (var nl = 1; nl <= newcount; nl++) 
				{
                    var comp_refno = expense_details.getLineItemValue('expense', 'refnumber', nl);
                    nlapiLogExecution('Debug', 'Line Loop', nl);
                    if (parseInt(comp_refno) > lineCount) {
                        var billable = expense_details.getLineItemValue('expense', 'isbillable', nl);
                        billable = (billable == 'T') ? true : false
                        json_body = {
                        		Status:s_Status_Ref, /// added by shravan
                            Date: expense_details.getFieldValue('trandate'),
                            ExpID: expense_details.getFieldValue('tranid'),
                            Project: expense_details.getLineItemValue('expense', 'custcolprj_name', nl),
                            Employee: expense_details.getFieldText('entity'),
                            From: expense_details.getFieldValue('trandate'),
                            To: expense_details.getFieldValue('duedate'),
                            Purpose: expense_details.getFieldValue('memo'),
                            Total: expense_details.getFieldValue('total'),
                            Total_Non_reimbursable: expense_details.getFieldValue('nonreimbursable'),
                            Total_Due: expense_details.getFieldValue('amount'),
							InternalID : id
                        };
                        data_Row = json_body;
                        //GEt Line Values

                        var img, file, data;

                        var img = '';
                        var data = '';
                        var file = '';
                        img = expense_details.getLineItemValue('expense', 'expmediaitem', nl);
                        if (img) {
                            file = nlapiLoadFile(img);
                            data = file.getValue();
                            nlapiLogExecution('debug', 'Img data', 'line:' + nl + ',' + 'data:' + data);
                        }

                        JsonCategory = {
                            RefNo: expense_details.getLineItemValue('expense', 'refnumber', nl),
                            //Category: expense_details.getLineItemValue('expense','category_display',nl),
                            //Currency: expense_details.getLineItemText('expense','currency',nl),
                            //Memo: expense_details.getLineItemValue('expense','memo',nl),
                            refId: new_data[temp_otg].refId,
                            //Billable : billable,
                            //Date : 	expense_details.getLineItemValue('expense','expensedate',nl),
                            //Project : 	expense_details.getLineItemValue('expense','custcolprj_name',nl),
                            //Department : 	expense_details.getLineItemValue('expense','department_display',nl),
                            //Vertical : 	expense_details.getLineItemValue('expense','class_display',nl),
                            //Amount: expense_details.getLineItemValue('expense','foreignamount',nl), //customer_display,
                            //imgData: data


                        }
                        dataRowsNew.push(JsonCategory);

                        //Response
                        /*json_expenseID ={
                        	ExpenseID:expenseID_T,
                        	Body: data_Row,
                        	LinesNew: dataRows
                        };
                        */
                        //loadExpense.setFieldValue('complete','T');

                        //return json_expenseID;
                        temp_otg++;
                    }
                }
                // var submit_new = nlapiSubmitRecord(loadExpense);	
            }

            if (_logValidation(delete_data)) 
			{
                var JsonCategory = {};
                var dataRowsDelete = [];
                var loadExpense = nlapiLoadRecord('expensereport', expense_InternalID);
                var linecount = loadExpense.getLineItemCount('expense');
                for (var i = 0; i < delete_data.length; i++) {
                    for (var linect = 1; linect <= linecount; linect++) {
                        var temp = loadExpense.getLineItemValue('expense', 'refnumber', linect);
                        var slno = delete_data[i].RefNo;
                        if ((slno != 'N') && (temp == slno)) {
                            loadExpense.removeLineItem('expense', linect);
                        }


                        JsonCategory = {
                            RefNo: slno,
                            refId: delete_data[i].refId
                        }
                        dataRowsDelete.push(JsonCategory);
                    }
                }
                nlapiSubmitRecord(loadExpense);
            }
            if (dataIn.Data.Body.Complete == true)
				{
                var loadExpense = nlapiLoadRecord('expensereport', expense_InternalID);
                loadExpense.setFieldValue('complete', 'T');
                var purpose = dataIn.Data.Body.Purpose;
                if (!_logValidation(purpose)) {
                    throw ('Please enter the purpose');
                }
				var i_Expense_Id = nlapiSubmitRecord(loadExpense);
				nlapiLogExecution('DEBUG','i_Expense_Id in response==', i_Expense_Id);
				/*  Code added by shravan for the auto approval part*/
				var expense_details = nlapiLoadRecord('expensereport', i_Expense_Id);
				var s_Status_Ref = expense_details.getFieldValue('status');
                nlapiLogExecution('DEBUG','s_Status_Ref in response==', s_Status_Ref);
				//return {Status :s_Status_Ref};*/
				json_body = {
                        	Status:s_Status_Ref, /// added by shravan
                            Date: expense_details.getFieldValue('trandate'),
                            ExpID: expense_details.getFieldValue('tranid'),
                            Project: expense_details.getLineItemValue('expense', 'custcolprj_name', 1),
                            Employee: expense_details.getFieldText('entity'),
                            From: expense_details.getFieldValue('trandate'),
                            To: expense_details.getFieldValue('duedate'),
                            Purpose: expense_details.getFieldValue('memo'),
                            Total: expense_details.getFieldValue('total'),
                            Total_Non_reimbursable: expense_details.getFieldValue('nonreimbursable'),
                            Total_Due: expense_details.getFieldValue('amount'),
							InternalID : expense_details.getFieldValue('internalid')
                        };
				json_expenseID = {
                    //ExpenseID:expenseID_T,
                    Body: json_body,
                    Lines: {
                        Edit: dataRowsEdit,
                        New: dataRowsNew,
                        Delete: dataRowsDelete,
                    }
                };
				nlapiLogExecution('debug', 'json_expenseID', JSON.stringify(json_expenseID));
				return json_expenseID;
				/* till here*/
				
            } 
			else 
			{
                json_expenseID = {
                    //ExpenseID:expenseID_T,
                    Body: data_Row,
                    Lines: {
                        Edit: dataRowsEdit,
                        New: dataRowsNew,
                        Delete: dataRowsDelete,
                    }
                };

                return json_expenseID;
            }

        }


    } catch (err) {
        nlapiLogExecution('Debug', 'Error object', err);
        if (err != 'Expense Already Submitted' && err != 'Please select valid project.' && err != 'Please enter the purpose') {
            var error = err.getDetails();

            var err_cat = error.indexOf('Category');
            var err_cur = error.indexOf('Currency');
            var err_amt = error.indexOf('Amount');

            if (err_cat != -1 && err_cur != -1 && err_amt != -1) {
                throw ('Please check the category, currency and amount entered');
            } else if (err_cat != -1 && err_cur != -1) {
                throw ('Please check the category and currency entered');
            } else if (err_amt != -1 && err_cur != -1) {
                throw ('Please check the amount and currency entered');
            } else if (err_amt != -1 && err_cat != -1) {
                throw ('Please check the amount and category entered');
            }

            var error_code = err.getCode();

            if (_logValidation(error_code)) {
                nlapiLogExecution('Debug', 'Error Code', error_code);
                if (error_code == 'INVALID_KEY_OR_REF') {
                    throw ('You are not currently allocated for the selected project');
                } else if (error_code == 'INVALID_FLD_VALUE') {
                    var err_string = JSON.stringify(error);
                    nlapiLogExecution('Debug', 'Error Details', err_string);
                    if (err_string.indexOf('expensedate')) {
                        throw ('Please verify the selected date since accounting period is closed for the selected period.');
                    }
                } else if (error_code == 'TRANS_UNBALNCD') {
                    var err_string = JSON.stringify(error);
                    nlapiLogExecution('Debug', 'Error Details', err_string);
                    if (err_string.indexOf('balance')) {
                        throw ('Please Enter a valid amount');
                    }
                }
            }
            if (_logValidation(error)) {
                if (error == 'Please enter value(s) for: Category') {
                    throw ('Please select an appropriate category.');
                } else if (error == 'The transaction date you specified is not within the date range of your accounting period.') {
                    throw ('Please verify the selected date since accounting period is closed for the selected period.');
                } else if (error == 'Please enter value(s) for: Project ') {
                    throw ('Please select valid project.');
                } else if (error == 'Please enter value(s) for: Currency') {
                    throw ('Please select valid currency.');
                } else if (error == 'Please enter value(s) for: Purpose ') {
                    throw ('Please enter the purpose');
                } else if (error == 'That record does not exist.') {
                    throw ('Unable to load the data since the expense has been deleted from NetSuite, please contact Information System team.');
                } else if (error == 'Please enter value(s) for: Amount') {
                    throw ('Please Enter a valid amount');
                } else {
                    throw err;
                }
            }

        } else {
            throw err;
        }
    }


}

function getExpensesDetails(expenseid) {
    try {
        var flag = true;
        var currency_1;
        var json = {},
            JsonCategory = {};
        var dataRow = [],
            dataRows = [];
        var main = {};
        nlapiLogExecution('DEBUG', 'Expense ID ON GETONE', expenseid);
        var expense_internal_id = getExpenseInternalID(expenseid);

        nlapiLogExecution('DEBUG', 'Expense Internal ID ON GETONE', expense_internal_id);

        var record_Obj = nlapiLoadRecord('expensereport', expense_internal_id);
        var emp_name = record_Obj.getFieldValue('entityname');
        var first_approver = record_Obj.getFieldText('custbody1stlevelapprover');
        var T_purpose = record_Obj.getFieldValue('memo'); //subsidiary
        var subsidiary_ = record_Obj.getFieldValue('subsidiary');
        var subsidiary_text = record_Obj.getFieldText('subsidiary');
        var totalAmt = record_Obj.getFieldValue('total');
        var T_Status = record_Obj.getFieldValue('statusRef');
        if (subsidiary_ == 3) {
            currency_1 = 'INR';
        } else {
            currency_1 = 'USD';
        }
        json = {
            Employee: emp_name,
            ExpID: record_Obj.getFieldValue('tranid'),
            Approver: first_approver,
            Subsidiary: subsidiary_text,
            Status: T_Status,
            Purpose: T_purpose,
            TotalAmt: totalAmt,
			InternalId : expense_internal_id

        };
        dataRow.push(json);

        var lineCount = record_Obj.getLineItemCount('expense');
        for (var i = 1; i <= lineCount; i++) {
            var img = '';
            var data = '';
            var file = '';
            var pro_ = record_Obj.getLineItemValue('expense', 'custcolprj_name', i);

            if (_logValidation(pro_)) {
                var temp = pro_.split(' ');
                var temp_ = pro_.split(':');
                var concat = temp_[0];
            } else {
                var concat = pro_;
            }
            img = record_Obj.getLineItemValue('expense', 'expmediaitem', i);
            if (img) {
                file = nlapiLoadFile(img);
                data = file.getValue();
                nlapiLogExecution('debug', 'Img data', 'line:' + i + ',' + 'data:' + data);
            }

            var billable = record_Obj.getLineItemValue('expense', 'isbillable', i);
            billable = (billable == 'T') ? true : false
            JsonCategory = {
                RefNo: record_Obj.getLineItemValue('expense', 'refnumber', i),
                Category: record_Obj.getLineItemValue('expense', 'category_display', i),
                Currency: record_Obj.getLineItemText('expense', 'currency', i),
                Memo: record_Obj.getLineItemValue('expense', 'memo', i),
                Billable: billable,
                Date: record_Obj.getLineItemValue('expense', 'expensedate', i),
                Amount: record_Obj.getLineItemValue('expense', 'foreignamount', i), //customer_display
                Project: record_Obj.getLineItemValue('expense', 'customer_display', i),
                Project_Display: concat,
                imgData: data

            }
            dataRows.push(JsonCategory);
        }
        main = {
            Body: dataRow,
            Line: dataRows

        };
        //var loadRec =  nlapiLoadRecord('expensereport',expense_internal_id);
        //loadRec.setFieldValue('status','B');
        //nlapiSubmitRecord(loadRec);
        //nlapiSubmitField('expensereport', expense_internal_id, 'status', 'B');
        nlapiLogExecution('debug', 'dataIn', JSON.stringify(main));
        return main;
    } catch (e) {
        nlapiLogExecution('DEBUG', 'Erron in getting the expense details', e);
        throw e;

    }

}

function getExpenseInternalID(expense_id) {
    try {
        var id = '';
        var filters = Array();
        filters.push(new nlobjSearchFilter('tranid', null, 'startswith', expense_id));

        var cols = Array();
        cols.push(new nlobjSearchColumn('internalid'));

        var searchObj = nlapiSearchRecord('expensereport', null, filters, cols);
        if (searchObj)
            id = searchObj[0].getValue('internalid');

        return id;
    } catch (e) {
        nlapiLogExecution('DEBUG', 'Erron in searching expense', e);
        throw e.details;
    }
}

function onSaveEventExpense(dataIn, employeeID) 
{
    try {
        var json_expenseID = {};
        nlapiLogExecution('debug', 'Inside Save DATAIN', JSON.stringify(dataIn));
        nlapiLogExecution('debug', 'Inside Save employeeID', employeeID);
        //var purpose = dataIn.Data.obj.purpose;
        //nlapiLogExecution('debug', 'purpose', purpose);

        var currentDate = nlapiDateToString(new Date());
        var date = new Date();
        nlapiLogExecution('DEBUG', 'currentDate obj', currentDate);

        var complete = dataIn.Data.Body.Complete;
        nlapiLogExecution('debug', 'complete', complete);
        //var imgData = dataIn.Data.imgData;
        //nlapiLogExecution('debug', 'imgData', imgData);

        //you just need to dynamically name your file
        /*	var newImg = nlapiCreateFile(employeeID+' '+'rImage'+' '+currentDate'.png', 'PJPGIMAGE', imgData);
        	newImg.setFolder(105120); //set the folder here
        	var id2=nlapiSubmitFile(newImg);
        	nlapiLogExecution('debug', 'Image id2', id2);*/

        //currentDate = nlapiAddDays(date,1);
        //currentDate = nlapiDateToString(currentDate);
        nlapiLogExecution('DEBUG', 'currentDate add days', currentDate);
        var postingDate = currentDate;

        //	var project = dataIn.project;
        var secondLevelApprover;
        var employeeLookup = nlapiLookupField('employee', parseInt(employeeID), ['approver', 'subsidiary', 'entityid']);
        var firstLevelApprover = employeeLookup.approver;
        var subsidiary_emp = employeeLookup.subsidiary;
        var employee_full_name = employeeLookup.entityid;
        if (subsidiary_emp == 3) {
            secondLevelApprover = 5764; //108786-Santosh Banadawar
        } else {
            secondLevelApprover = 5764; //108984-Govind S Kharayat ID:5764
        }
        var obj = {};
        var temp = {};
        temp = dataIn.Data.Lines.New;
        obj = temp.Category;
        nlapiLogExecution('debug', 'Inside Save obj', JSON.stringify(obj));


        //Test
        //var obj ={};
        //	obj = { "category":[
        //	{"id":"1","category":"Miles-Project","amount":"399","project":"RMG","currency":"USD","Memo":"TEST"}]}
        nlapiLogExecution('DEBUG', 'Expenses Length', temp.length);
        //var expenseId = dataIn.Data.ExpenseId;
        //	var id_= obj.category[0].id;
        //

        //create expense 
        var record = nlapiCreateRecord('expensereport');
        //record.setFieldValue('memo',purpose);
        record.setFieldValue('entity', employeeID);
        //record.setFieldValue('custbody_transactiondate',postingDate);
        //record.setFieldValue('trandate',postingDate);
        record.setFieldValue('custbody1stlevelapprover', firstLevelApprover);
        record.setFieldValue('subsidiary', subsidiary_emp);
        record.setFieldValue('custbody_expenseapprover', secondLevelApprover);
        var purpose = dataIn.Data.Body.Purpose;
        record.setFieldValue('memo', purpose);
        
        for (var j = 0; j < temp.length; j++) {
                nlapiLogExecution('debug', 'temp[j].imgData', temp[j].imgData);
                //you just need to dynamically name your file
                /*var IMG_currentDate = nlapiDateToString(new Date(),'datetimetz');
                if(obj[j].imgData){
                var newImg = nlapiCreateFile(employeeID+' '+'rImage'+' '+IMG_currentDate+'.png', 'PJPGIMAGE', obj[j].imgData);
                newImg.setFolder(105120); //set the folder here
                var id2=nlapiSubmitFile(newImg);
                nlapiLogExecution('debug', 'Image id2', id2);
                }*/
                //Create Line Items From Here
                record.selectNewLineItem('expense');

                record.setCurrentLineItemValue('expense', 'refnumber', j + 1);
                nlapiLogExecution('DEBUG', 'ID', j + 1);
                record.setCurrentLineItemValue('expense', 'memo', temp[j].Memo);
                nlapiLogExecution('DEBUG', 'Memo', temp[j].Memo);

                nlapiLogExecution('DEBUG', 'catProject', dataIn.Data.Body.Project);

                var isbillable = temp[j].isbillable;
                isbillable = (isbillable == true) ? 'T' : 'F';

                record.setCurrentLineItemValue('expense', 'isbillable', isbillable);

                var project = projectSplit(dataIn.Data.Body.Project);

                nlapiLogExecution('DEBUG', 'projectSplit', project);
                var projectInternalID = projectDetailSearch(project);

                if (!_logValidation(projectInternalID)) {
                    throw ('Please select valid project.');
                }

                nlapiLogExecution('DEBUG', 'projectInternalID', projectInternalID);

                record.setCurrentLineItemValue('expense', 'customer', projectInternalID);
                record.setCurrentLineItemText('expense', 'category', temp[j].Category);
                nlapiLogExecution('DEBUG', 'category', temp[j].Category);
                //
                if ((temp[j].ImgData != '' || temp[j].ImgData != null) && temp[j].ImgData) {
                    var IMG_currentDate = nlapiDateToString(new Date(), 'datetimetz');
                    nlapiLogExecution('DEBUG', 'Image at line' + j + 'Data', temp[j].ImgData);

                    //Search For Employee Folder
                    var folderID = null;
                    var filters = Array();
                    filters.push(new nlobjSearchFilter('name', null, 'startswith', employee_full_name));
					filters.push(new nlobjSearchFilter('parent', null, 'anyof','@NONE@')); /// new addeed as per the Folder Change
					// by shravan
                    var cols = Array();
                    cols.push(new nlobjSearchColumn('internalid'));

                    var searchFolder = nlapiSearchRecord('folder', null, filters, cols);
                    if (searchFolder) {
                        folderID = searchFolder[0].getValue('internalid');
                        nlapiLogExecution('DEBUG', 'Image Folder For Existing Employee ID', folderID);
                    }
                    if (!folderID) {
                        var folder = nlapiCreateRecord('folder');

                        if (folder) {

                           // folder.setFieldValue('parent', '10'); // create root level folder

                            folder.setFieldValue('name', employee_full_name);

                            folderID = nlapiSubmitRecord(folder);
                            nlapiLogExecution('DEBUG', 'Image Folder For Employee ID', folderID);
                        }

                    }
					nlapiLogExecution('debug', 'temp[j].ImgdataPDF===', temp[j].ImgdataPDF);
						if( (temp[j].ImgdataPDF) == true) //// New key value on 24 may 2021
						{
								var obj_Pdf_File = nlapiCreateFile(employeeID +' ' + IMG_currentDate + '-' + j + '.pdf', 'PDF', temp[j].ImgData);
									obj_Pdf_File.setFolder(folderID); //set the folder here
								var i_Image_Ref_id = nlapiSubmitFile(obj_Pdf_File);
								nlapiLogExecution('debug', 'i_Image_Ref_id', i_Image_Ref_id);
								record.setCurrentLineItemValue('expense', 'expmediaitem', i_Image_Ref_id);
						} ////// if( (temp[j].ImgdataPDF) == true) 
						else{
								var newImg = nlapiCreateFile(employeeID + ' ' + 'rImage' + ' ' + IMG_currentDate + '-' + j + '.png', 'PJPGIMAGE', temp[j].ImgData);
								//newImg.setFolder(105120); //set the folder here
								newImg.setFolder(folderID); //set the folder here
								var id2 = nlapiSubmitFile(newImg);
								nlapiLogExecution('debug', 'Image id2', id2);
								record.setCurrentLineItemValue('expense', 'expmediaitem', id2);
							} //// Else of if( (temp[j].ImgdataPDF) == true)
                }


                record.setCurrentLineItemValue('expense', 'foreignamount', temp[j].Amount);
                record.setCurrentLineItemText('expense', 'currency', temp[j].Currency);

                var date_ = temp[j].Date;
                var tranDate = formatDate(date_);
                nlapiLogExecution('DEBUG', 'tranDate', tranDate);
                var tranDate = temp[j].Date;
                record.setCurrentLineItemValue('expense', 'expensedate', temp[j].Date);
                //Need to add more fields based on XML tag i,e record &xml=T
                record.commitLineItem('expense');
			}

                record.setFieldValue('custbody_transactiondate', tranDate);
                record.setFieldValue('trandate', tranDate);
				if (complete == false) 
				{
					record.setFieldValue('complete', 'F');
				}
				else
				{
					record.setFieldValue('complete','T');
				}
				var expense_ID = nlapiSubmitRecord(record);

				nlapiLogExecution('DEBUG', ' creating the expense expense_ID', 'expense_ID:' + expense_ID);
				if (expense_ID) {
				var lookUp_Expense = nlapiLookupField('expensereport', expense_ID, ['tranid']);
				var expenseID_T = lookUp_Expense.tranid;
			}

        //var url_print = 'https://system.sandbox.netsuite.com/app/accounting/transactions/exprept.nl?id='+expense_ID+'&print=T&whence=';
        //nlapiLogExecution('DEBUG',' creating the expense expense_ID url_print','url_print:'+url_print);

        var json_body = {};
        var JsonCategory = {};
        var data_Row = {};
        var dataRowsNew = [];
        var expense_details = nlapiLoadRecord('expensereport', expense_ID);
        var s_Status_Ref = expense_details.getFieldValue('status');
        nlapiLogExecution('DEBUG','s_Status_Ref==', s_Status_Ref);

        json_body = {
        	Status:s_Status_Ref,  // added by shravan
            Date: expense_details.getFieldValue('trandate'),
            ExpID: expense_details.getFieldValue('tranid'),
            Employee: expense_details.getFieldText('entity'),
            Project: dataIn.Data.Body.Project,
            From: expense_details.getFieldValue('trandate'),
            To: expense_details.getFieldValue('duedate'),
            Purpose: expense_details.getFieldValue('memo'),
            Total: expense_details.getFieldValue('total'),
            Total_Non_reimbursable: expense_details.getFieldValue('nonreimbursable'),
            Total_Due: expense_details.getFieldValue('amount'),
			InternalId : expense_ID
        };
        data_Row = json_body;
        //GEt Line Values
        var img, file, data, billable;
        var lineCount = expense_details.getLineItemCount('expense');
        var temp_otg = 0
        for (var i = 1; i <= lineCount; i++) {
            var pro_ = expense_details.getLineItemValue('expense', 'custcolprj_name', i);
            billable = expense_details.getLineItemValue('expense', 'isbillable', i);
            billable = (billable == 'T') ? true : false;
            if (_logValidation(pro_)) {
                var temp1 = pro_.split(' ');
                var temp_ = pro_.split(':');
                var concat = temp_[0];
            } else {
                var concat = pro_;
            }
            img = expense_details.getLineItemValue('expense', 'expmediaitem', i);
            if (img) {
                file = nlapiLoadFile(img);
                data = file.getValue();
                nlapiLogExecution('debug', 'Img data', 'line:' + i + ',' + 'data:' + data);
            }

            //for(var j = 0; j < temp.length; j++)
            //{
            JsonCategory = {
                RefNo: expense_details.getLineItemValue('expense', 'refnumber', i),
                //Category: expense_details.getLineItemValue('expense','category_display',i),
                //Currency: expense_details.getLineItemText('expense','currency',i),
                //Memo: expense_details.getLineItemValue('expense','memo',i),
                //Billable: billable,
                refId: temp[temp_otg].refId,
                // Date : 	expense_details.getLineItemValue('expense','expensedate',i),
                //Project : 	expense_details.getLineItemValue('expense','custcolprj_name',i),
                //Department : 	expense_details.getLineItemValue('expense','department_display',i),
                //Vertical : 	expense_details.getLineItemValue('expense','class_display',i),
                //Amount: expense_details.getLineItemValue('expense','foreignamount',i), //customer_display,
                //Project_Display: concat,
                //imgData: data


            }
            dataRowsNew.push(JsonCategory);
            temp_otg++
        }



        //Response
        json_expenseID = {
            //ExpenseID:expenseID_T,
            Body: data_Row,
            Lines: {
                Edit: [],
                New: dataRowsNew,
                Delete: [],
            }
        };

        //	nlapiSubmitField('expensereport', expense_ID, 'statusRef', 'pendingSupApproval');
        return (json_expenseID);



    } 
	catch (err) {
        nlapiLogExecution('Debug', 'Error', err);
        nlapiLogExecution('Debug', 'Error in line no', j);
        if (err != 'Expense Already Submitted' && err != 'Please select valid project.') {
            var error = err.getDetails();
            nlapiLogExecution('Debug', 'Error Details', error);

            var err_cat = error.indexOf('Category');
            var err_cur = error.indexOf('Currency');
            var err_amt = error.indexOf('Amount');

            if (err_cat != -1 && err_cur != -1 && err_amt != -1) {
                throw ('Please check the category, currency and amount entered');
            } else if (err_cat != -1 && err_cur != -1) {
                throw ('Please check the category and currency entered');
            } else if (err_amt != -1 && err_cur != -1) {
                throw ('Please check the amount and currency entered');
            } else if (err_amt != -1 && err_cat != -1) {
                throw ('Please check the amount and category entered');
            }

            var error_code = err.getCode();

            if (_logValidation(error_code)) {
                nlapiLogExecution('Debug', 'Error Code', error_code);
                if (error_code == 'INVALID_KEY_OR_REF') {
                    //throw ('You are not currently allocated for the selected project');
					throw err;
                } else if (error_code == 'INVALID_FLD_VALUE' && error.indexOf('expensedate')) {
                    throw ('Please verify the selected date since accounting period is closed for the selected period.');
                } else if (error_code == 'TRANS_UNBALNCD' && error.indexOf('balance')) {
                    throw ('Please Enter a valid amount');
                }
            }
            if (_logValidation(error)) {
                if (error == 'Please enter value(s) for: Category') {
                    throw ('Please select an appropriate category.');
                } else if (error == 'The transaction date you specified is not within the date range of your accounting period.') {
                    throw ('Please verify the selected date since accounting period is closed for the selected period.');
                } else if (error == 'Please enter value(s) for: Project ') {
                    throw ('Please select valid project.');
                } else if (error == 'Please enter value(s) for: Currency') {
                    throw ('Please select valid currency.');
                } else if (error == 'Please enter value(s) for: Purpose ') {
                    throw ('Please enter the purpose');
                } else if (error == 'That record does not exist.') {
                    throw ('Unable to load the data since the expense has been deleted from NetSuite, please contact Information System team.');
                } else if (error == 'Please enter value(s) for: Amount') {
                    throw ('Please Enter a valid amount');
                } else {
                    throw err;
                }
            }
        } else {
            throw err;
        }
    }
}

function formatDate(Date) {

    if (Date) {
        // fusionDate = "2016-07-14T00:00:00.000Z";
        var datePart = Date.split('T')[0];
        var dateSplit = datePart.split('-');

        // MM/DD/ YYYY
        var netsuiteDateString = dateSplit[1] + "/" + dateSplit[2] + "/" +
            dateSplit[0];
        return netsuiteDateString;
    }
    return "";
}

function projectSplit(project) {

    if (project) {

        var projectSplit = project.split(' ');
        var projectID = projectSplit[0];

        return projectID;
    }
    return "";
}

function projectDetailSearch(project_id) {
    try {
        var id = '';
        var filters = Array();
        filters.push(new nlobjSearchFilter('entityid', null, 'is', project_id));

        var cols = Array();
        cols.push(new nlobjSearchColumn('internalid'));

        var searchObj = nlapiSearchRecord('job', null, filters, cols);
        if (searchObj)
            id = searchObj[0].getValue('internalid');

        return id;
    } catch (e) {
        nlapiLogExecution('DEBUG', 'Project Search Error', e);
        throw e;
    }
}

//Get Expenses from Employee ID
function getExpensesList(empID) {
    try {
        var currency_1;
        var filter = Array();
        filter.push(new nlobjSearchFilter('entity', null, 'is', empID));
        filter.push(new nlobjSearchFilter('mainline', null, 'is', 'T'))

        var columns = Array();
        columns.push(new nlobjSearchColumn('tranid').setSort(true));
        columns.push(new nlobjSearchColumn('memo'));
        columns.push(new nlobjSearchColumn('status'));
        columns.push(new nlobjSearchColumn('fxamount'));
        columns.push(new nlobjSearchColumn('trandate'));
        columns.push(new nlobjSearchColumn('subsidiary')); //

        var expenseSearch = nlapiSearchRecord('expensereport', null, filter, columns);
        if (expenseSearch) {
            var expenseID = expenseSearch[0].getId();
            var expenseLoad = nlapiLoadRecord('expensereport', expenseID);
            var lineCount = expenseLoad.getLineItemCount('expense');
            for (var i = 1; i <= 1; i++) {

                var project = expenseLoad.getLineItemValue('expense', 'customer_display', i);

            }

        }

        var dataRow = [];
        var expenseList = {};

        var employeeLookup_1 = nlapiLookupField('employee', parseInt(empID), ['entityid']);
        var employee_id = employeeLookup_1.entityid;
        if (expenseSearch) {
            for (var i = 0; i < expenseSearch.length; i++) {
                var subsidiary_1 = expenseSearch[i].getValue('subsidiary');
                if (subsidiary_1 == 3) {
                    currency_1 = 'INR';
                } else {
                    currency_1 = 'USD';
                }
                expenseList = {
                    Employee: employee_id,
                    ExpenseID: expenseSearch[i].getValue('tranid'),
                    //Date : expenseSearch[i].getValue('trandate'),
                    Purpose: expenseSearch[i].getValue('memo'),
                    Status: expenseSearch[i].getValue('status'),
                    Amount: expenseSearch[i].getValue('fxamount'),
                    Project: project,
                    Currency: currency_1

                }
                dataRow.push(expenseList);
            }
        }
        return dataRow;

    } catch (e) {
        nlapiLogExecution('DEBUG', 'Expense Search Error', e);
        throw e;
    }
}

function _logValidation(value) {
    if (value != null && value.toString() != null && value != '' && value != undefined && value.toString() != undefined && value != 'undefined' && value.toString() != 'undefined' && value.toString() != 'NaN' && value != NaN) {
        return true;
    } else {
        return false;
    }
}