/**
 * @author Shweta
 */
// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
/*
   	Script Name : UES_Purchase_Order_Controls.js
	Author      : Shweta Chopde
	Date        : 3 July 2014
	Description : Apply controls on Bill Button


	Script Modification Log:

	-- Date --			-- Modified By --				--Requested By--				-- Description --



Below is a summary of the process controls enforced by this script file.  The control logic is described
more fully, below, in the appropriate function headers and code blocks.


     BEFORE LOAD
		- beforeLoadRecord(type)



     BEFORE SUBMIT
		- beforeSubmitRecord(type)


     AFTER SUBMIT
		- afterSubmitRecord(type)



     SUB-FUNCTIONS
		- The following sub-functions are called by the above core functions in order to maintain code
            modularization:

               - NOT USED

*/
}
// END SCRIPT DESCRIPTION BLOCK  ====================================



// BEGIN GLOBAL VARIABLE BLOCK  =====================================
{
	//  Initialize any Global Variables, in particular, debugging variables...




}
// END GLOBAL VARIABLE BLOCK  =======================================





// BEGIN BEFORE LOAD ==================================================

function beforeLoad_PO_ctrls(type,form)
{
	
  var d_PO_end_date = nlapiGetFieldValue('custbody_poto')	
  nlapiLogExecution('DEBUG', 'beforeLoad_PO_ctrls', ' PO End Date -->' + d_PO_end_date);

  var d_todays_date = get_todays_date();
  d_todays_date = nlapiStringToDate(d_todays_date);
  nlapiLogExecution('DEBUG', 'beforeLoad_PO_ctrls', ' Todays Date -->' + d_todays_date);
   
  if(d_PO_end_date!=null && d_PO_end_date!='' && d_PO_end_date!=undefined)
  {
  d_PO_end_date = nlapiStringToDate(d_PO_end_date);
  nlapiLogExecution('DEBUG', 'beforeLoad_PO_ctrls', ' PO End Date -->' + d_PO_end_date);
  
  if(d_PO_end_date<d_todays_date)
  {
  	nlapiLogExecution('DEBUG', 'beforeLoad_PO_ctrls', ' PO End Date is greater ......');
		
	  var buttonOBJ=form.getButton('bill');
	  nlapiLogExecution('DEBUG', 'beforeLoad_PO_ctrls', ' Button Object -->' + buttonOBJ);
			
	  if(buttonOBJ!=null && buttonOBJ!='' && buttonOBJ!=undefined)
	  {
	   buttonOBJ.setDisabled(true);   
	  }		
	
  }  	
	
  }//PO End Date
 
 return true;
}

// END BEFORE LOAD ====================================================





// BEGIN BEFORE SUBMIT ================================================

function beforeSubmitRecord(type)
{
    /*  On before submit:

          - PURPOSE
		-

          FIELDS USED:

          --Field Name--				--ID--


    */


    //  LOCAL VARIABLES


    //  BEFORE SUBMIT CODE BODY


	return true;

}

// END BEFORE SUBMIT ==================================================





// BEGIN AFTER SUBMIT =============================================

function afterSubmitRecord(type)
{
    /*  On after submit:

          - PURPOSE
		-

	FIELDS USED:

          --Field Name--				--ID--


    */


	//  LOCAL VARIABLES



	//  AFTER SUBMIT CODE BODY

	return true;

}

// END AFTER SUBMIT ===============================================





// BEGIN FUNCTION ===================================================
function get_todays_date()
{
	var today;
	// ============================= Todays Date ==========================================
		
	  var date1 = new Date();
      nlapiLogExecution('DEBUG', 'schedulerFunction', 'Date ==' + date1);

      var offsetIST = 5.5;

    //To convert to UTC datetime by subtracting the current Timezone offset
     var utcdate =  new Date(date1.getTime() + (date1.getTimezoneOffset()*60000));
     nlapiLogExecution('DEBUG', 'schedulerFunction', 'UTC Date' + utcdate);

    //Then cinver the UTS date to the required time zone offset like back to 5.5 for IST
     var istdate =  new Date(utcdate.getTime() - ((-offsetIST*60)*60000));
     nlapiLogExecution('DEBUG', 'schedulerFunction', 'IST Date ==' + istdate);
    
     var day = istdate.getDate();
     nlapiLogExecution('DEBUG', 'schedulerFunction', 'Day ==' + day);
	 
     var month = istdate.getMonth()+1;
     nlapiLogExecution('DEBUG', 'schedulerFunction', 'Month ==' + month);

	 var year=istdate.getFullYear();
	 nlapiLogExecution('DEBUG', 'schedulerFunction', 'Year ==' +year);
	  
	 var date_format = checkDateFormat();
	 nlapiLogExecution('DEBUG', 'schedulerFunction', 'Date Format ==' +date_format);
	 
	 if (date_format == 'YYYY-MM-DD')
	 {
	 	today = year + '-' + month + '-' + day;		
	 } 
	 if (date_format == 'DD/MM/YYYY') 
	 {
	  	today = day + '/' + month + '/' + year;
	 }
	 if (date_format == 'MM/DD/YYYY')
	 {
	  	today = month + '/' + day + '/' + year;
	 }	 
	 return today;
}
function checkDateFormat()
{
    var context = nlapiGetContext();

	var dateFormatPref = context.getPreference('dateformat');

    return dateFormatPref;
}
  

// END FUNCTION =====================================================
