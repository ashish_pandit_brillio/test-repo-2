function pageInit_removeAddOption(type, name) {
    try {
        if (type == 'create' || type == 'edit') {
            //nlapiRemoveLineItemOption('custpage_bo_pending_detail', 'custpage_reason', -1)
            /*
            for(var i =1;i
            <=98;i++)
                     {
                     var x = document.getElementById("custpage_bo_pending_detail_custpage_reason"+i+"_fs"); //
                     //var t = document.getElementByClassName("ns-dropdown");
                     var y = x.childNodes;
                     var z = y[2].dataset.options;
                     var j = JSON.parse(z);
                     delete j[1];
                     var str = JSON.stringify(j)
                     for(var k=0;k
                     <j.length;i++)
            {
            j[k]=j[k];
            }
            }
            */
        }
    } catch (e) {
        nlapiLogExecution('Debug', 'process error', e);
    }
}

function practice_filter(type, name) {
    if (name == "custpage_practice") {
        var practice = nlapiGetFieldValue('custpage_practice');
        var value_pass = new Array();
        value_pass['custpage_practice'] = practice;
        var url = 'https://3883006.app.netsuite.com/app/site/hosting/scriptlet.nl?script=1929&deploy=1&import_practice=' + practice;
        // 3883006.app
        window.open(url, '_self');
    }
    if (name == 'custpage_reason') {
        var line_count = nlapiGetLineItemCount('custpage_bo_pending_detail');
        for (var k = 1; k <=
            line_count; k++) {
            if (nlapiGetLineItemValue('custpage_bo_pending_detail', 'custpage_check_box', k) == 'T') {
                var reason_id = nlapiGetLineItemValue('custpage_bo_pending_detail', 'custpage_reason', k);
                var comment = nlapiGetLineItemValue('custpage_bo_pending_detail', 'custpage_comment', k);
                if (reason_id == 7 && comment == " ") {
                    alert("Please put some comment");
                    return false;
                    //  nlapiSetLineItemValue('custpage_bo_pending_detail','custpage_comment',k,'Fill It').setMandatory( true );			 
                }
            }
        }
    }
}

function Bo_pending_allocation_csv() {
    try {
        var practice = nlapiGetFieldValue('custpage_practice');
        var print = true;
        var url = 'https://3883006.app.netsuite.com/app/site/hosting/scriptlet.nl?script=1929&deploy=1&print=' + print + '&import_practice=' + practice;
        //debugger.na1 3883006.app
        var myWindow = window.open(url, '_blank');
   //   setTimeout(function () { myWindow.close();}, 3000);
       // myWindow.close();
    } catch (e) {
        nlapiLogExecution('Debug', 'process error', e);
    }
}

function onSave() {
	
    var line_count = nlapiGetLineItemCount('custpage_bo_pending_detail');
    for (var line_index = 1; line_index <=line_count; line_index++)
	{
		if (nlapiGetLineItemValue('custpage_bo_pending_detail', 'custpage_check_box', line_index) == 'T') 
		{
			var reason = nlapiGetLineItemValue('custpage_bo_pending_detail', 'custpage_reason', line_index);
			var comment = nlapiGetLineItemValue('custpage_bo_pending_detail', 'custpage_comment', line_index);
			if (reason == 7 && comment == " ") 
			{
				alert("Please put some comment");
					return false;
			}
			var assigned_to = nlapiGetLineItemValue('custpage_bo_pending_detail', 'custpage_assigned_to', line_index);
			if (reason == '' || reason == null || reason == undefined)
			{
			   alert("please select the reason");
			   return false; 
			   
			}
			if(_logValidation(reason)|| (reason == 7 && comment != " "))
				
			{
				var frf_internal_id = nlapiGetLineItemValue('custpage_bo_pending_detail', 'custpage_frf_internal_id', line_index);
				var record_id = nlapiLoadRecord('customrecord_frf_details',frf_internal_id);
				nlapiSubmitField('customrecord_frf_details', frf_internal_id,['custrecord_frf_details_bo_comments','custrecord_frf_details_operation_comment','custrecord_frf_details_assigned_to'],[reason,comment,assigned_to]);
				   
			}
		}
	}
	
	return true;
}

 function operation_comment_mandatory(name) {
            if (name == 'custpage_reason') {
                if (nlapiGetFieldValue('custpage_practice') == 7) {
                    detail.addField('custpage_comment', 'textarea', 'Comment').setDisplayType('entry').setMandatory(true);
                }
            }
        }
		
function _logValidation(value) 
{
 if(value != null && value.toString() != null && value != '' && value != undefined && value.toString() != undefined && value != 'undefined' && value.toString() != 'undefined'&& value.toString() != 'NaN' && value != NaN) 
 {
  return true;
 }
 else 
 { 
  return false;
 }
}