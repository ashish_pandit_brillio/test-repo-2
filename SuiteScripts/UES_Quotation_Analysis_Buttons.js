/**
 * @author Shweta
 */
// BEGIN SCRIPT DESCRIPTION BLOCK ==================================
{
	/*
	 * Script Name: UES_Quotation_Analysis_Buttons.js Author : Shweta Chopde
	 * Company : Aashna Cloudtech Date : 11 April 2014 Description: Create a
	 * Approve / Reject Button
	 * 
	 * 
	 * Script Modification Log: -- Date -- -- Modified By -- --Requested By-- --
	 * Description --
	 * 
	 * 
	 * 
	 * Below is a summary of the process controls enforced by this script file.
	 * The control logic is described more fully, below, in the appropriate
	 * function headers and code blocks.
	 * 
	 * 
	 * BEFORE LOAD - beforeLoadRecord(type)
	 * 
	 * 
	 * 
	 * BEFORE SUBMIT - beforeSubmitRecord(type)
	 * 
	 * 
	 * AFTER SUBMIT - afterSubmitRecord(type)
	 * 
	 * 
	 * 
	 * SUB-FUNCTIONS - The following sub-functions are called by the above core
	 * functions in order to maintain code modularization: - NOT USED
	 * 
	 */
}
// END SCRIPT DESCRIPTION BLOCK ====================================

// BEGIN GLOBAL VARIABLE BLOCK =====================================
{
	// Initialize any Global Variables, in particular, debugging variables...

}
// END GLOBAL VARIABLE BLOCK =======================================

// BEGIN BEFORE LOAD ==================================================

function beforeLoad_QA_buttons(type) {

	if (type == 'view') {
		var i_recordID = nlapiGetRecordId();
		var i_current_user = nlapiGetUser();
		var i_vertical_head = nlapiGetFieldValue('custrecord_verticalheadforquotation');
		var i_finance_head = nlapiGetFieldValue('custrecord_financehead');
		var i_approval_status = nlapiGetFieldValue('custrecord_approvalstatusforquotation');
		var i_vendor_PO = nlapiGetFieldValue('custrecord_vendorforpo');
		var i_preferred_vendor = nlapiGetFieldValue('custrecord_preferred_vendor');

		form.setScript('customscript_quotation_analysis_approve');
		form.setScript('customscript_quotation_analysis_reject');

		if (i_vendor_PO && i_preferred_vendor) {

			if (i_vendor_PO == i_preferred_vendor) {

				if ((i_current_user == i_vertical_head && i_approval_status == 1)
				        || i_current_user == i_finance_head
				        && i_approval_status == 2) {
					form.addButton('custpage_approve', 'Approve', 'approve_QA('
					        + i_recordID + ')');
					form.addButton('custpage_reject', 'Reject', 'reject_QA('
					        + i_recordID + ')');
				}
			}
		}
	}// VIEW

	/*
	 * 
	 * var o_recordOBJ =
	 * nlapiLoadRecord('customrecord_quotationanalysis',i_recordID)
	 * 
	 * var v1 = 350
	 * 
	 * var myFld = o_recordOBJ.getField('custrecord_vendorforpo') var
	 * options=myFld.getSelectOptions();
	 * 
	 * for(var i=0;i<options.length;i++) { var countryId=options[i].getId();
	 * var countryText=options[i].getText(); nlapiLogExecution('DEBUG',
	 * 'suiteletFunction','i -->'+i+'Country ID -->'+countryId+'Country Text
	 * -->'+countryText); /* if(countryId == 350) { //
	 * myFld.addSelectOption(countryId,countryText, false);
	 * nlapiLogExecution('DEBUG', 'suiteletFunction',' In Loop ............ '); }
	 */

	/*
	 * 
	 * }//For
	 */

	return true;

}

// END BEFORE LOAD ====================================================

// BEGIN BEFORE SUBMIT ================================================

function beforeSubmitRecord(type) {
	/*
	 * On before submit: - PURPOSE -
	 * 
	 * FIELDS USED:
	 * 
	 * --Field Name-- --ID--
	 * 
	 * 
	 */

	// LOCAL VARIABLES
	// BEFORE SUBMIT CODE BODY
	return true;

}

// END BEFORE SUBMIT ==================================================

// BEGIN AFTER SUBMIT =============================================

function afterSubmit_setdefault_status(type) {

	var i_recordID = nlapiGetRecordId();

	var s_record_type = nlapiGetRecordType();

	if (_logValidation(i_recordID) && _logValidation(s_record_type)) {
		var o_recordOBJ = nlapiLoadRecord(s_record_type, i_recordID)

		if (_logValidation(o_recordOBJ)) {
			var i_status = o_recordOBJ
			        .getFieldValue('custrecord_approvalstatusforquotation');

			var i_item = o_recordOBJ.getFieldValue('custrecord_item');

			var i_S_No = o_recordOBJ.getFieldValue('custrecord_srno');

			var i_PR_No = o_recordOBJ.getFieldValue('custrecord_prquote');

			var i_PR_ID = search_PR_Item_recordID_1(i_PR_No, i_S_No, i_item)

			var i_vendor_PO = o_recordOBJ
			        .getFieldValue('custrecord_vendorforpo');

			nlapiLogExecution('DEBUG', 'afterSubmit_Item_Data_Sourcing',
			        ' i_status -->' + i_status);

			nlapiLogExecution('DEBUG', 'afterSubmit_Item_Data_Sourcing',
			        ' i_item-->' + i_item);

			nlapiLogExecution('DEBUG', 'afterSubmit_Item_Data_Sourcing',
			        ' i_S_No -->' + i_S_No);

			nlapiLogExecution('DEBUG', 'afterSubmit_Item_Data_Sourcing',
			        ' i_PR_ID -->' + i_PR_ID);

			nlapiLogExecution('DEBUG', 'afterSubmit_Item_Data_Sourcing',
			        ' i_PR_No -->' + i_PR_No);

			nlapiLogExecution('DEBUG', 'afterSubmit_Item_Data_Sourcing',
			        ' i_vendor_PO -->' + i_vendor_PO);

			if (i_status == 5) {
				if (_logValidation(i_vendor_PO)) {
					if (_logValidation(i_PR_ID)) {
						var o_PR_Item_OBJ = nlapiLoadRecord(
						        'customrecord_pritem', i_PR_ID)

						if (_logValidation(o_PR_Item_OBJ)) {
							o_PR_Item_OBJ.setFieldValue('custrecord_prastatus',
							        10)
							o_recordOBJ.setFieldValue(
							        'custrecord_approvalstatusforquotation', 1)

							var i_submitID_QA = nlapiSubmitRecord(o_recordOBJ,
							        true, true)
							var i_PR_submitID = nlapiSubmitRecord(
							        o_PR_Item_OBJ, true, true)

							nlapiLogExecution('DEBUG',
							        'afterSubmit_Item_Data_Sourcing',
							        ' i_submitID_QA -->' + i_submitID_QA);

							nlapiLogExecution('DEBUG',
							        'afterSubmit_Item_Data_Sourcing',
							        ' i_PR_submitID -->' + i_PR_submitID);

						}
					}

				}

			}

		}// Record OBJ

	}// Record ID & Record Type

	return true;

}

// END AFTER SUBMIT ===============================================

// BEGIN FUNCTION ===================================================
/**
 * 
 * @param {Object}
 *            value
 * 
 * Description --> If the value is blank /null/undefined returns false else
 * returns true
 */
function _logValidation(value) {
	if (value != null && value.toString() != null && value != ''
	        && value != undefined && value.toString() != undefined
	        && value != 'undefined' && value.toString() != 'undefined'
	        && value.toString() != 'NaN' && value != NaN) {
		return true;
	} else {
		return false;
	}
}

/**
 * 
 * @param {Object}
 *            i_recordID
 * @param {Object}
 *            i_SR_No_QA
 * @param {Object}
 *            i_item_QA
 * 
 * Description --> For the criteria Item, Serial No & Purchase Request search
 * the PR Ã¢â‚¬â€œ Item record & returns the PR Ã¢â‚¬â€œ
 * Item internal ID
 */
function search_PR_Item_recordID_1(i_recordID, i_SR_No_QA, i_item_QA) {
	nlapiLogExecution('DEBUG', 'search_PR_Item_recordID', 'i_recordID =='
	        + i_recordID);
	nlapiLogExecution('DEBUG', 'search_PR_Item_recordID', 'i_SR_No_QA =='
	        + i_SR_No_QA);
	nlapiLogExecution('DEBUG', 'search_PR_Item_recordID', 'i_item_QA =='
	        + i_item_QA);

	var i_internal_id;

	var filter = new Array();
	filter[0] = new nlobjSearchFilter('custrecord_purchaserequest', null, 'is',
	        i_recordID);
	filter[1] = new nlobjSearchFilter('custrecord_prlineitemno', null, 'is',
	        parseInt(i_SR_No_QA));
	filter[2] = new nlobjSearchFilter('custrecord_prmatgrpcategory', null,
	        'is', i_item_QA);

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('internalid');
	columns[1] = new nlobjSearchColumn('custrecord_purchaserequest');
	columns[2] = new nlobjSearchColumn('custrecord_prlineitemno');
	columns[3] = new nlobjSearchColumn('custrecord_prmatgrpcategory');

	var a_seq_searchresults = nlapiSearchRecord('customrecord_pritem', null,
	        filter, columns);

	if (a_seq_searchresults != null && a_seq_searchresults != ''
	        && a_seq_searchresults != undefined) {
		nlapiLogExecution('DEBUG', 'search_PR_Item_recordID',
		        'a_seq_searchresults ==' + a_seq_searchresults.length);

		for (var ty = 0; ty < a_seq_searchresults.length; ty++) {
			i_internal_id = a_seq_searchresults[ty].getValue('internalid');
			nlapiLogExecution('DEBUG', 'search_PR_Item_recordID',
			        ' Internal ID -->' + i_internal_id);

			var i_PR = a_seq_searchresults[ty]
			        .getValue('custrecord_purchaserequest');
			nlapiLogExecution('DEBUG', 'search_PR_Item_recordID',
			        ' i_PR ID -->' + i_PR);

			var i_line_no = a_seq_searchresults[ty]
			        .getValue('custrecord_prlineitemno');
			nlapiLogExecution('DEBUG', 'search_PR_Item_recordID',
			        ' i_line_no -->' + i_line_no);

			var i_item = a_seq_searchresults[ty]
			        .getValue('custrecord_prmatgrpcategory');
			nlapiLogExecution('DEBUG', 'search_PR_Item_recordID', ' i_item-->'
			        + i_item);

			if ((i_PR == i_recordID) && (i_line_no == i_SR_No_QA)
			        && (i_item == i_item_QA)) {
				i_internal_id = i_internal_id
				break
			}

		}// LOOP

	}

	return i_internal_id;
}

// END FUNCTION =====================================================
