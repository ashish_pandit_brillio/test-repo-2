// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
/*
   	Script Name: SUT_Timesheet_Report
	Author: Vikrant
	Company: Aashna CloudTech
	Date: 04-09-2014
	Description: Initial draft of the report


	Script Modification Log:

	-- Date --			-- Modified By --				--Requested By--				-- Description --



Below is a summary of the process controls enforced by this script file.  The control logic is described
more fully, below, in the appropriate function headers and code blocks.


     SUITELET
		- suiteletFunction(request, response)


     SUB-FUNCTIONS
		- The following sub-functions are called by the above core functions in order to maintain code
            modularization:

               - NOT USED

*/
}
// END SCRIPT DESCRIPTION BLOCK  ====================================



// BEGIN GLOBAL VARIABLE BLOCK  =====================================
{
	//  Initialize any Global Variables, in particular, ERRORging variables...




}
// END GLOBAL VARIABLE BLOCK  =======================================





// BEGIN SUITELET ==================================================

function SUT_Timesheet_Report(request, response)//
{

	/*  Suitelet:
	 - EXPLAIN THE PURPOSE OF THIS FUNCTION
	 -
	 FIELDS USED:
	 --Field Name--				--ID--
	 */
	//  LOCAL VARIABLES
	
	
	
	//  SUITELET CODE BODY
	
	
	if (request.getMethod() == 'GET') // 
	{
		//Create the form and add fields to it 
		var form = nlapiCreateForm("Get Timesheet report");
		form.addField('custpage_from_date', 'date', 'From Date');
		form.addField('custpage_to_date', 'date', 'To Date');
		var s_start_date = request.getParameter('custpage_from_date');
		var s_to_date = request.getParameter('custpage_to_date');
		//form.addButton('custombutton', 'Export As CSV', 'CLI_Export_Timesheet_rpt(' + s_start_date + ')');
		
		//form.setScript('customscript_cli_export_timesheet_rpt');
		form.addSubmitButton('Export Report');
		response.writePage(form);
	}
	//POST call
	else //
	{
		try//
		{
			var form = nlapiCreateForm("Get Timesheet report");
			
			//create the fields on the form and populate them with values from the previous screen 
			var from_date = form.addField('custpage_from_date', 'date', 'From Date');
			//nlapiLogExecution('DEBUG', 'SUT_Timesheet_Report', 'from_date : ' + from_date);
			from_date.setDefaultValue(request.getParameter('custpage_from_date'));
			from_date.setDisplayType('inline');
			
			var to_date = form.addField('custpage_to_date', 'date', 'To Date');
			//nlapiLogExecution('DEBUG', 'SUT_Timesheet_Report', 'to_date : ' + to_date);
			to_date.setDefaultValue(request.getParameter('custpage_to_date'));
			to_date.setDisplayType('inline');
			
			var d_from_date = request.getParameter('custpage_from_date');
			//nlapiLogExecution('DEBUG', 'SUT_Timesheet_Report', 'd_from_date : ' + d_from_date);
			
			var d_to_date = request.getParameter('custpage_to_date');
			//nlapiLogExecution('DEBUG', 'SUT_Timesheet_Report', 'd_to_date : ' + d_to_date);
			
			var CSV_String = 'FusionID,GCI ID,Weekending,Task ID,Mon ST,Tue ST,Wed ST,Thu ST,Fri ST,Sat ST,Sun ST,Mon OT,Tue OT,Wed OT,Thu OT,Fri OT,Sat OT,Sun OT,Mon DT,Tue DT,Wed DT,Thu DT,Fri DT,Sat DT,Sun DT,MonTime-Off,TueTime-Off,WedTime-Off,ThuTime-Off,FriTime-Off,SatTime-Off,SunTime-Off,Total ST,Total OT,Total DT,TotalTime-Off,\n';
			var i_lastInternalID = 0; // used for looping when records are greater than 1000
			var previousInternalID = '';
			
			var temp_emp = '';
			var temp_inc_id = '';
			var temp_fusion_id = '';
			var emp = '';
			var date = '';
			var hrs = 0;
			
			var st_1 = 0;
			var st_2 = 0;
			var st_3 = 0;
			var st_4 = 0;
			var st_5 = 0;
			var st_6 = 0;
			var st_7 = 0;
			
			var ot_1 = 0;
			var ot_2 = 0;
			var ot_3 = 0;
			var ot_4 = 0;
			var ot_5 = 0;
			var ot_6 = 0;
			var ot_7 = 0;
			
			var dt_1 = 0;
			var dt_2 = 0;
			var dt_3 = 0;
			var dt_4 = 0;
			var dt_5 = 0;
			var dt_6 = 0;
			var dt_7 = 0;
			
			var to_1 = 0;
			var to_2 = 0;
			var to_3 = 0;
			var to_4 = 0;
			var to_5 = 0;
			var to_6 = 0;
			var to_7 = 0;
			
			var st_sum = 0;
			var ot_sum = 0;
			var dt_sum = 0;
			var to_sum = 0;
			
			var context = nlapiGetContext();
			var remaining_usage = context.getRemainingUsage();
			
			if (_validate(d_from_date)) //
			{
				/*
				 var to_date = nlapiAddDays(new Date(d_from_date), 6);
				 //nlapiLogExecution('DEBUG', 'SUT_Timesheet_Report', 'to_date : ' + to_date);
				 
				 to_date = new Date(to_date);
				 var today = to_date;
				 var dd = today.getDate();
				 var mm = today.getMonth() + 1; //January is 0! var yyyy = today.getFullYear(); if(dd<10){dd='0'+dd} if(mm<10){mm='0'+mm} var today = dd+'/'+mm+'/'+yyyy; document.getElementById("DATE").value = today;
				 var yyyy = today.getFullYear();
				 
				 to_date = mm + '/' + dd + '/' + yyyy;
				 //to_date = dd + '/' + mm + '/' + yyyy;
				 //nlapiLogExecution('DEBUG', 'SUT_Timesheet_Report', 'to_date : ' + to_date);
				 */
				var counter = 1;
				
				do //
				{
					/*
					 var filters_1 = new Array();
					 filters_1[filters_1.length] = new nlobjSearchFilter('date', null, 'within', d_from_date, d_to_date);
					 filters_1[filters_1.length] = new nlobjSearchFilter('internalidnumber', 'employee', 'greaterthan', i_lastInternalID);
					 nlapiLogExecution('DEBUG', 'SUT_Timesheet_Report', 'i_lastInternalID : ' + i_lastInternalID);
					 
					 var column_1 = new Array();
					 column_1[column_1.length] = new nlobjSearchColumn('internalid', 'employee', 'group');
					 column_1[0].setSort();
					 
					 var search_result_0 = nlapiSearchRecord('timebill', 'customsearch_timesheet_report_llc', filters_1, column_1);
					 */
					//if (_validate(search_result_0)) //
					{
						//nlapiLogExecution('DEBUG', 'SUT_Timesheet_Report', 'search_result_0.length : ' + search_result_0.length);
						
						//for (var j = 0; j < search_result_0.length; j++) //
						{
							/*var result_0 = search_result_0[j];
							 var all_cols = result_0.getAllColumns();
							 
							 var emp_id_0 = result_0.getValue(all_cols[0]);
							 nlapiLogExecution('DEBUG', 'SUT_Timesheet_Report', 'emp_id_0 : ' + emp_id_0);
							 
							 i_lastInternalID = emp_id_0;
							 */
							var filters = new Array();
							filters[filters.length] = new nlobjSearchFilter('date', null, 'within', d_from_date, d_to_date);
							filters[filters.length] = new nlobjSearchFilter('internalidnumber', 'employee', 'greaterthan', i_lastInternalID);
							//filters[filters.length] = new nlobjSearchFilter('employee', null, 'is', emp_id_0);
							nlapiLogExecution('DEBUG', 'SUT_Timesheet_Report', 'i_lastInternalID : ' + i_lastInternalID);
							
							var search_result = nlapiSearchRecord('timebill', 'customsearch_timesheet_repor_2', filters, null);
							
							if (_validate(search_result)) //
							{
							
								var line_count = search_result.length;
								//nlapiLogExecution('DEBUG', 'SUT_Timesheet_Report', 'line_count : ' + line_count);
								
								// variable declaration was here earlier
								
								for (var i = 0; i < line_count; i++) //
								{
									//nlapiLogExecution('DEBUG', 'SUT_Timesheet_Report', '********* New Row ************ ' + counter++);
									//previousInternalID = i_lastInternalID;
									
									var result = search_result[i];
									var all_column = result.getAllColumns();
									
									var task_name = result.getValue(all_column[3]);
									//nlapiLogExecution('DEBUG', 'SUT_Timesheet_Report', 'task_name : ' + task_name);
									
									var hrs = result.getValue(all_column[4]);
									//nlapiLogExecution('DEBUG', 'SUT_Timesheet_Report', 'hrs : ' + hrs);
									
									var item = result.getValue(all_column[5]);
									//nlapiLogExecution('DEBUG', 'SUT_Timesheet_Report', 'item : ' + item);
									
									var fusion_ID = result.getValue(all_column[6]);
									//nlapiLogExecution('DEBUG', 'SUT_Timesheet_Report', 'fusion_ID : ' + fusion_ID);
									
									var trandate = result.getValue(all_column[0]);
									//nlapiLogExecution('DEBUG', 'SUT_Timesheet_Report', 'trandate : ' + trandate);
									
									if (i == 0) // For the first data row
									{
										temp_emp = result.getValue(all_column[1]);
										//nlapiLogExecution('DEBUG', 'SUT_Timesheet_Report', 'temp_emp : ' + temp_emp);
										
										temp_inc_id = result.getValue(all_column[2]);
										//nlapiLogExecution('DEBUG', 'SUT_Timesheet_Report', 'temp_inc_id : ' + temp_inc_id);
										
										temp_fusion_id = result.getValue(all_column[6]);
										//nlapiLogExecution('DEBUG', 'SUT_Timesheet_Report', 'temp_fusion_id : ' + temp_fusion_id);
									}
									
									// from the next data row
									i_lastInternalID = temp_emp; // to get the last employee id for next search of 1000 records
									{
										emp = result.getValue(all_column[1]);
										//nlapiLogExecution('DEBUG', 'SUT_Timesheet_Report', 'emp : ' + emp);
										
										if (temp_emp == emp) //
										{
											temp_inc_id = result.getValue(all_column[2]);
											temp_fusion_id = result.getValue(all_column[6]);
											//nlapiLogExecution('DEBUG', 'SUT_Timesheet_Report', 'temp_inc_id : ' + temp_inc_id);
											//nlapiLogExecution('DEBUG', 'SUT_Timesheet_Report', 'd_from_date : ' + d_from_date);
											
											var new_d_from_date = null;
											//if (i == 0) //
											{
												new_d_from_date = nlapiStringToDate(d_from_date);
											}
											
											trandate = nlapiStringToDate(trandate);
											
											////nlapiLogExecution('DEBUG', 'SUT_Timesheet_Report', 'd_from_date : ' + d_from_date + ' trandate : ' + trandate);
											//nlapiLogExecution('DEBUG', 'SUT_Timesheet_Report', 'new_d_from_date : ' + new_d_from_date + ' trandate : ' + trandate);
											
											//var diffDays = d_from_date.getTime() - trandate.getTime();
											//var diffDays = trandate.getTime() - d_from_date.getTime();
											var diffDays = trandate.getTime() - new_d_from_date.getTime();
											diffDays = parseInt(diffDays / 86400000);
											//nlapiLogExecution('DEBUG', 'SUT_Timesheet_Report', 'diffDays : ' + diffDays);
											
											if (diffDays == 0) // Monday
											{
												if (task_name == 'Leave' || task_name == 'Holiday') //
												{
													if (hrs > 0) //
													{
														to_1 = parseFloat(to_1) + parseFloat(hrs);
														
														if (to_1 > 8) //
														{
															//to_1 = 8;
															//nlapiLogExecution('DEBUG', 'SUT_Timesheet_Report', 'to_1 : ' + to_1);
														}
														else //
														{
															to_1 = hrs;
														}
													}
												}
												else 
													if (task_name == 'OT') //
													{
														if (hrs > 0) //
														{
															ot_1 = parseFloat(ot_1) + parseFloat(hrs);
															
															if (ot_1 > 4) //
															{
																dt_1 = parseFloat(ot_1) - 4;
																ot_1 = 4;
															//nlapiLogExecution('DEBUG', 'SUT_Timesheet_Report', 'ot_1 : ' + ot_1);
															}
															else //
															{
															//ot_1 = hrs;
															}
														}
													}
													else 
														if (task_name == 'Project Activities') //
														{
															if (hrs > 0) //
															{
																st_1 = parseFloat(st_1) + parseFloat(hrs);
																
																if (st_1 > 8) //
																{
																	//st_1 = 8;
																//nlapiLogExecution('DEBUG', 'SUT_Timesheet_Report', 'st_1 : ' + st_1);
																}
																else //
																{
																//st_1 = hrs;
																////nlapiLogExecution('DEBUG', 'SUT_Timesheet_Report', 'st_1 : ' + st_1);
																}
															}
														}
											}
											else 
												if (diffDays == 1) // Tuesday
												{
													if (task_name == 'Leave' || task_name == 'Holiday') //
													{
														if (hrs > 0) //
														{
															to_2 = parseFloat(to_2) + parseFloat(hrs);
														}
														else //
														{
														//to_2 = hrs;
														}
														
														if (to_2 > 8) //
														{
															//to_2 = 8;
														}
													}
													else 
														if (task_name == 'OT') //
														{
															if (hrs > 0) //
															{
																ot_2 = parseFloat(ot_2) + parseFloat(hrs);
																
																if (ot_2 > 4) //
																{
																	dt_2 = parseFloat(ot_2) - 4;
																	ot_2 = 4;
																//nlapiLogExecution('DEBUG', 'SUT_Timesheet_Report', 'ot_2 : ' + ot_2);
																}
																else //
																{
																//ot_2 = hrs;
																}
															}
														}
														else 
															if (task_name == 'Project Activities') //
															{
																if (hrs > 0) //
																{
																	st_2 = parseFloat(st_2) + parseFloat(hrs);
																	
																	if (st_2 > 8) //
																	{
																		//st_2 = 8;
																	}
																	else //
																	{
																	//st_2 = hrs;
																	}
																}
															}
												}
												else 
													if (diffDays == 2) // Wednesday
													{
														if (task_name == 'Leave' || task_name == 'Holiday') //
														{
															if (hrs > 0) //
															{
																to_3 = parseFloat(to_3) + parseFloat(hrs);
															}
															else //
															{
															//to_3 = hrs;
															}
															
															if (to_3 > 8) //
															{
																//to_3 = 8;
															}
														}
														else 
															if (task_name == 'OT') //
															{
																if (hrs > 0) //
																{
																	ot_3 = parseFloat(ot_3) + parseFloat(hrs);
																	
																	if (ot_3 > 4) //
																	{
																		dt_3 = parseFloat(ot_3) - 4;
																		ot_3 = 4;
																	}
																	else //
																	{
																	//ot_3 = hrs;
																	}
																}
															}
															else 
																if (task_name == 'Project Activities') //
																{
																	if (hrs > 0) //
																	{
																		st_3 = parseFloat(st_3) + parseFloat(hrs);
																		
																		if (st_3 > 8) //
																		{
																			//st_3 = 8;
																		}
																		else //
																		{
																			st_3 = hrs;
																		}
																	}
																}
													}
													else 
														if (diffDays == 3) // Thursday
														{
															if (task_name == 'Leave' || task_name == 'Holiday') //
															{
																if (hrs > 0) //
																{
																	to_4 = parseFloat(to_4) + parseFloat(hrs);
																}
																else //
																{
																//to_4 = hrs;
																}
																
																if (to_4 > 8) //
																{
																	//to_4 = 8;
																}
															}
															else 
																if (task_name == 'OT') //
																{
																	if (hrs > 0) //
																	{
																		ot_4 = parseFloat(ot_4) + parseFloat(hrs);
																		
																		if (ot_4 > 4) //
																		{
																			dt_4 = parseFloat(ot_4) - 4;
																			ot_4 = 4;
																		}
																		else //
																		{
																		//ot_4 = hrs;
																		}
																	}
																}
																else 
																	if (task_name == 'Project Activities') //
																	{
																		if (hrs > 0) //
																		{
																			st_4 = parseFloat(st_4) + parseFloat(hrs);
																			
																			if (st_4 > 8) //
																			{
																				//st_4 = 8;
																			}
																			else //
																			{
																			//st_4 = hrs;
																			}
																		}
																	}
														}
														else 
															if (diffDays == 4) // Friday
															{
																if (task_name == 'Leave' || task_name == 'Holiday') //
																{
																	if (to_5 > 0) //
																	{
																		to_5 = parseFloat(to_5) + parseFloat(hrs);
																	}
																	else //
																	{
																	//to_5 = hrs;
																	}
																	
																	if (to_5 > 8) //
																	{
																		//to_5 = 8;
																	}
																}
																else 
																	if (task_name == 'OT') //
																	{
																		if (hrs > 0) //
																		{
																			ot_5 = parseFloat(ot_5) + parseFloat(hrs);
																			
																			if (ot_5 > 4) //
																			{
																				dt_5 = parseFloat(ot_5) - 4;
																				ot_5 = 4;
																			}
																			else //
																			{
																			//ot_5 = hrs;
																			}
																		}
																	}
																	else 
																		if (task_name == 'Project Activities') //
																		{
																			if (hrs > 0) //
																			{
																				st_5 = parseFloat(st_5) + parseFloat(hrs);
																				
																				if (st_5 > 8) //
																				{
																					//st_5 = 8;
																				}
																				else //
																				{
																				//st_5 = hrs;
																				}
																			}
																		}
															}
															else 
																if (diffDays == 5) // Saturday
																{
																	if (task_name == 'Leave' || task_name == 'Holiday') //
																	{
																		if (hrs > 0) //
																		{
																			to_6 = parseFloat(to_6) + parseFloat(hrs);
																		}
																		else //
																		{
																		//to_6 = hrs;
																		}
																		
																		if (to_6 > 8) //
																		{
																			//to_6 = 8;
																		}
																	}
																	else 
																		if (task_name == 'OT') //
																		{
																			if (hrs > 0) //
																			{
																				ot_6 = parseFloat(ot_6) + parseFloat(hrs);
																				
																				if (ot_6 > 8) //
																				{
																					dt_6 = parseFloat(ot_6) - 8;
																					ot_6 = 8;
																				}
																				else //
																				{
																				//ot_6 = hrs;
																				}
																			}
																		}
																		else 
																			if (task_name == 'Project Activities') //
																			{
																				if (hrs > 0) //
																				{
																					st_6 = parseFloat(st_6) + parseFloat(hrs);
																					
																					if (st_6 > 8) //
																					{
																						//st_6 = 8;
																					}
																					else //
																					{
																					//st_6 = hrs;
																					}
																				}
																			}
																}
																else 
																	if (diffDays == 6) // Sunday
																	{
																		if (task_name == 'Leave' || task_name == 'Holiday') //
																		{
																			if (hrs > 0) //
																			{
																				to_7 = parseFloat(to_7) + parseFloat(hrs);
																			}
																			else //
																			{
																			//to_7 = hrs;
																			}
																			
																			if (to_7 > 8) //
																			{
																				//to_7 = 8;
																			}
																		}
																		else 
																			if (task_name == 'OT') //
																			{
																				if (hrs > 0) //
																				{
																					ot_7 = parseFloat(ot_7) + parseFloat(hrs);
																					
																					if (ot_7 > 8) //
																					{
																						dt_7 = parseFloat(ot_7) - 8;
																						ot_7 = 8;
																					}
																					else //
																					{
																					//ot_7 = hrs;
																					}
																				}
																			}
																			else 
																				if (task_name == 'Project Activities') //
																				{
																					if (hrs > 0) //
																					{
																						st_7 = parseFloat(st_7) + parseFloat(hrs);
																						
																						if (st_7 > 8) //
																						{
																							//st_7 = 8;
																						}
																						else //
																						{
																						//st_7 = hrs;
																						}
																					}
																				}
																	}
										}
										else 
											if (emp != temp_emp) // if current emp(emp) from search is not equal to earlier emp(temp_emp)
											{
												st_sum = parseFloat(st_1) + parseFloat(st_2) + parseFloat(st_3) + parseFloat(st_4) + parseFloat(st_5) + parseFloat(st_6) + parseFloat(st_7);
												//nlapiLogExecution('DEBUG', 'CSV', 'st_sum : ' + st_sum);
												
												ot_sum = parseFloat(ot_1) + parseFloat(ot_2) + parseFloat(ot_3) + parseFloat(ot_4) + parseFloat(ot_5) + parseFloat(ot_6) + parseFloat(ot_7);
												//nlapiLogExecution('DEBUG', 'CSV', 'ot_sum : ' + ot_sum);
												
												dt_sum = parseFloat(dt_1) + parseFloat(dt_2) + parseFloat(dt_3) + parseFloat(dt_4) + parseFloat(dt_5) + parseFloat(dt_6) + parseFloat(dt_7);
												//nlapiLogExecution('DEBUG', 'CSV', 'dt_sum : ' + dt_sum);
												
												to_sum = parseFloat(to_1) + parseFloat(to_2) + parseFloat(to_3) + parseFloat(to_4) + parseFloat(to_5) + parseFloat(to_6) + parseFloat(to_7);
												//nlapiLogExecution('DEBUG', 'CSV', 'to_sum : ' + to_sum);
												
												CSV_String = CSV_String + '' + temp_fusion_id + ',' + temp_inc_id + ',' + request.getParameter('custpage_to_date') + ',Task ID,' +
												st_1 +
												',' +
												st_2 +
												',' +
												st_3 +
												',' +
												st_4 +
												',' +
												st_5 +
												',' +
												st_6 +
												',' +
												st_7 +
												',' +
												ot_1 +
												',' +
												ot_2 +
												',' +
												ot_3 +
												',' +
												ot_4 +
												',' +
												ot_5 +
												',' +
												ot_6 +
												',' +
												ot_7 +
												',' +
												dt_1 +
												',' +
												dt_2 +
												',' +
												dt_3 +
												',' +
												dt_4 +
												',' +
												dt_5 +
												',' +
												dt_6 +
												',' +
												dt_7 +
												',' +
												to_1 +
												',' +
												to_2 +
												',' +
												to_3 +
												',' +
												to_4 +
												',' +
												to_5 +
												',' +
												to_6 +
												',' +
												to_7 +
												',' +
												st_sum +
												',' +
												ot_sum +
												',' +
												dt_sum +
												',' +
												to_sum +
												',\n';
												
												//nlapiLogExecution('DEBUG', 'CSV', 'CSV_String : ' + CSV_String);
												
												st_1 = 0;
												st_2 = 0;
												st_3 = 0;
												st_4 = 0;
												st_5 = 0;
												st_6 = 0;
												st_7 = 0;
												
												ot_1 = 0;
												ot_2 = 0;
												ot_3 = 0;
												ot_4 = 0;
												ot_5 = 0;
												ot_6 = 0;
												ot_7 = 0;
												
												dt_1 = 0;
												dt_2 = 0;
												dt_3 = 0;
												dt_4 = 0;
												dt_5 = 0;
												dt_6 = 0;
												dt_7 = 0;
												
												to_1 = 0;
												to_2 = 0;
												to_3 = 0;
												to_4 = 0;
												to_5 = 0;
												to_6 = 0;
												to_7 = 0;
												
												st_sum = 0
												dt_sum = 0;
												ot_sum = 0;
												to_sum = 0;
												
												if (i > 950) // 
												{
													// if row is greater than 950
													// then break this loop and send the temp_emp to search for next 1000 records
													// so that we will not miss the some records of any employee
													nlapiLogExecution('DEBUG', 'SUT_Timesheet_Report', 'i : ' + i);
													nlapiLogExecution('DEBUG', 'SUT_Timesheet_Report', 'temp_emp : ' + temp_emp + ', emp ' + emp);
													break;
													
												}
												
												//var trandate = result.getValue(all_column[0]);
												////nlapiLogExecution('DEBUG', 'SUT_Timesheet_Report', 'trandate : ' + trandate);
												
												temp_emp = result.getValue(all_column[1]);
												//nlapiLogExecution('DEBUG', 'SUT_Timesheet_Report', 'temp_emp : ' + temp_emp);
												
												temp_inc_id = result.getValue(all_column[2]);
												//nlapiLogExecution('DEBUG', 'SUT_Timesheet_Report', 'temp_inc_id : ' + temp_inc_id);
												
												temp_fusion_id = result.getValue(all_column[6]);
												//nlapiLogExecution('DEBUG', 'SUT_Timesheet_Report', 'temp_fusion_id : ' + temp_fusion_id);
												
												//d_from_date = nlapiStringToDate(d_from_date);
												
												/*var new_d_from_date = null;
											 if (i == 0) //
											 {
											 new_d_from_date = nlapiStringToDate(d_from_date);
											 }*/
												trandate = nlapiStringToDate(trandate);
												
												////nlapiLogExecution('DEBUG', 'SUT_Timesheet_Report', 'd_from_date : ' + d_from_date + ' trandate : ' + trandate);
												//nlapiLogExecution('DEBUG', 'SUT_Timesheet_Report', 'new_d_from_date : ' + new_d_from_date + ' trandate : ' + trandate);
												
												//var diffDays = d_from_date.getTime() - trandate.getTime();
												var diffDays = trandate.getTime() - new_d_from_date.getTime();
												diffDays = parseInt(diffDays / 86400000);
												//nlapiLogExecution('DEBUG', 'SUT_Timesheet_Report', 'diffDays : ' + diffDays);
												
												if (diffDays == 0) // Monday
												{
													if (task_name == 'Leave' || task_name == 'Holiday') //
													{
														if (hrs > 0) //
														{
															to_1 = parseFloat(to_1) + parseFloat(hrs);
															
															if (to_1 > 8) //
															{
																//to_1 = 8;
															//nlapiLogExecution('DEBUG', 'SUT_Timesheet_Report', 'to_1 : ' + to_1);
															}
															else //
															{
															//to_1 = hrs;
															}
														}
													}
													else 
														if (task_name == 'OT') //
														{
															if (hrs > 0) //
															{
																ot_1 = parseFloat(ot_1) + parseFloat(hrs);
																
																if (ot_1 > 4) //
																{
																	dt_1 = parseFloat(ot_1) - 4;
																	ot_1 = 4;
																//nlapiLogExecution('DEBUG', 'SUT_Timesheet_Report', 'ot_1 : ' + ot_1);
																}
																else //
																{
																//ot_1 = hrs;
																}
															}
														}
														else 
															if (task_name == 'Project Activities') //
															{
																if (hrs > 0) //
																{
																	st_1 = parseFloat(st_1) + parseFloat(hrs);
																	
																	if (st_1 > 8) //
																	{
																		//st_1 = 8;
																	//nlapiLogExecution('DEBUG', 'SUT_Timesheet_Report', 'st_1 : ' + st_1);
																	}
																	else //
																	{
																	//st_1 = hrs;
																	////nlapiLogExecution('DEBUG', 'SUT_Timesheet_Report', 'st_1 : ' + st_1);
																	}
																}
															}
												}
												else 
													if (diffDays == 1) // Tuesday
													{
														if (task_name == 'Leave' || task_name == 'Holiday') //
														{
															if (hrs > 0) //
															{
																to_2 = parseFloat(to_2) + parseFloat(hrs);
															}
															else //
															{
															//to_2 = hrs;
															}
															
															if (to_2 > 8) //
															{
																//to_2 = 8;
															}
														}
														else 
															if (task_name == 'OT') //
															{
																if (hrs > 0) //
																{
																	ot_2 = parseFloat(ot_2) + parseFloat(hrs);
																	
																	if (ot_2 > 4) //
																	{
																		dt_2 = parseFloat(ot_2) - 4;
																		ot_2 = 4;
																	//nlapiLogExecution('DEBUG', 'SUT_Timesheet_Report', 'ot_2 : ' + ot_2);
																	}
																	else //
																	{
																	//ot_2 = hrs;
																	}
																}
															}
															else 
																if (task_name == 'Project Activities') //
																{
																	if (hrs > 0) //
																	{
																		st_2 = parseFloat(st_2) + parseFloat(hrs);
																		
																		if (st_2 > 8) //
																		{
																			//st_2 = 8;
																		}
																		else //
																		{
																		//st_2 = hrs;
																		}
																	}
																}
													}
													else 
														if (diffDays == 2) // Wednesday
														{
															if (task_name == 'Leave' || task_name == 'Holiday') //
															{
																if (hrs > 0) //
																{
																	to_3 = parseFloat(to_3) + parseFloat(hrs);
																}
																else //
																{
																//to_3 = hrs;
																}
																
																if (to_3 > 8) //
																{
																	//to_3 = 8;
																}
															}
															else 
																if (task_name == 'OT') //
																{
																	if (hrs > 0) //
																	{
																		ot_3 = parseFloat(ot_3) + parseFloat(hrs);
																		
																		if (ot_3 > 4) //
																		{
																			dt_3 = parseFloat(ot_3) - 4;
																			ot_3 = 4;
																		}
																		else //
																		{
																		//ot_3 = hrs;
																		}
																	}
																}
																else 
																	if (task_name == 'Project Activities') //
																	{
																		if (hrs > 0) //
																		{
																			st_3 = parseFloat(st_3) + parseFloat(hrs);
																			
																			if (st_3 > 8) //
																			{
																				//st_3 = 8;
																			}
																			else //
																			{
																				st_3 = hrs;
																			}
																		}
																	}
														}
														else 
															if (diffDays == 3) // Thursday
															{
																if (task_name == 'Leave' || task_name == 'Holiday') //
																{
																	if (hrs > 0) //
																	{
																		to_4 = parseFloat(to_4) + parseFloat(hrs);
																	}
																	else //
																	{
																	//to_4 = hrs;
																	}
																	
																	if (to_4 > 8) //
																	{
																		//to_4 = 8;
																	}
																}
																else 
																	if (task_name == 'OT') //
																	{
																		if (hrs > 0) //
																		{
																			ot_4 = parseFloat(ot_4) + parseFloat(hrs);
																			
																			if (ot_4 > 4) //
																			{
																				dt_4 = parseFloat(ot_4) - 4;
																				ot_4 = 4;
																			}
																			else //
																			{
																			//ot_4 = hrs;
																			}
																		}
																	}
																	else 
																		if (task_name == 'Project Activities') //
																		{
																			if (hrs > 0) //
																			{
																				st_4 = parseFloat(st_4) + parseFloat(hrs);
																				
																				if (st_4 > 8) //
																				{
																					//st_4 = 8;
																				}
																				else //
																				{
																				//st_4 = hrs;
																				}
																			}
																		}
															}
															else 
																if (diffDays == 4) // Friday
																{
																	if (task_name == 'Leave' || task_name == 'Holiday') //
																	{
																		if (hrs > 0) //
																		{
																			to_5 = parseFloat(to_5) + parseFloat(hrs);
																		}
																		else //
																		{
																		//to_5 = hrs;
																		}
																		
																		if (to_5 > 8) //
																		{
																			//to_5 = 8;
																		}
																	}
																	else 
																		if (task_name == 'OT') //
																		{
																			if (hrs > 0) //
																			{
																				ot_5 = parseFloat(ot_5) + parseFloat(hrs);
																				
																				if (ot_5 > 4) //
																				{
																					dt_5 = parseFloat(ot_5) - 4;
																					ot_5 = 4;
																				}
																				else //
																				{
																				//ot_5 = hrs;
																				}
																			}
																		}
																		else 
																			if (task_name == 'Project Activities') //
																			{
																				if (hrs > 0) //
																				{
																					st_5 = parseFloat(st_5) + parseFloat(hrs);
																					
																					if (st_5 > 8) //
																					{
																						//st_5 = 8;
																					}
																					else //
																					{
																					//st_5 = hrs;
																					}
																				}
																			}
																}
																else 
																	if (diffDays == 5) // Saturday
																	{
																		if (task_name == 'Leave' || task_name == 'Holiday') //
																		{
																			if (hrs > 0) //
																			{
																				to_6 = parseFloat(to_6) + parseFloat(hrs);
																			}
																			else //
																			{
																			//to_6 = hrs;
																			}
																			
																			if (to_6 > 8) //
																			{
																				//to_6 = 8;
																			}
																		}
																		else 
																			if (task_name == 'OT') //
																			{
																				if (hrs > 0) //
																				{
																					ot_6 = parseFloat(ot_6) + parseFloat(hrs);
																					
																					if (ot_6 > 8) //
																					{
																						dt_6 = parseFloat(ot_6) - 8;
																						ot_6 = 8;
																					}
																					else //
																					{
																					//ot_6 = hrs;
																					}
																				}
																			}
																			else 
																				if (task_name == 'Project Activities') //
																				{
																					if (hrs > 0) //
																					{
																						st_6 = parseFloat(st_6) + parseFloat(hrs);
																						
																						if (st_6 > 8) //
																						{
																							//st_6 = 8;
																						}
																						else //
																						{
																						//st_6 = hrs;
																						}
																					}
																				}
																	}
																	else 
																		if (diffDays == 6) // Sunday
																		{
																			if (task_name == 'Leave' || task_name == 'Holiday') //
																			{
																				if (hrs > 0) //
																				{
																					to_7 = parseFloat(to_7) + parseFloat(hrs);
																				}
																				else //
																				{
																				//to_7 = hrs;
																				}
																				
																				if (to_7 > 8) //
																				{
																					//to_7 = 8;
																				}
																			}
																			else 
																				if (task_name == 'OT') //
																				{
																					if (hrs > 0) //
																					{
																						ot_7 = parseFloat(ot_7) + parseFloat(hrs);
																						
																						if (ot_7 > 8) //
																						{
																							dt_7 = parseFloat(ot_7) - 8;
																							ot_7 = 8;
																						}
																						else //
																						{
																						//ot_7 = hrs;
																						}
																					}
																				}
																				else 
																					if (task_name == 'Project Activities') //
																					{
																						if (hrs > 0) //
																						{
																							st_7 = parseFloat(st_7) + parseFloat(hrs);
																							
																							if (st_7 > 8) //
																							{
																								//st_7 = 8;
																							}
																							else //
																							{
																							//st_7 = hrs;
																							}
																						}
																					}
																		}
											}
										
										if (i == parseInt(line_count - 1)) // if it is last row and 
										{
											st_sum = parseFloat(st_1) + parseFloat(st_2) + parseFloat(st_3) + parseFloat(st_4) + parseFloat(st_5) + parseFloat(st_6) + parseFloat(st_7);
											//nlapiLogExecution('DEBUG', 'CSV', 'st_sum : ' + st_sum);
											
											ot_sum = parseFloat(ot_1) + parseFloat(ot_2) + parseFloat(ot_3) + parseFloat(ot_4) + parseFloat(ot_5) + parseFloat(ot_6) + parseFloat(ot_7);
											//nlapiLogExecution('DEBUG', 'CSV', 'ot_sum : ' + ot_sum);
											
											dt_sum = parseFloat(dt_1) + parseFloat(dt_2) + parseFloat(dt_3) + parseFloat(dt_4) + parseFloat(dt_5) + parseFloat(dt_6) + parseFloat(dt_7);
											//nlapiLogExecution('DEBUG', 'CSV', 'dt_sum : ' + dt_sum);
											
											to_sum = parseFloat(to_1) + parseFloat(to_2) + parseFloat(to_3) + parseFloat(to_4) + parseFloat(to_5) + parseFloat(to_6) + parseFloat(to_7);
											//nlapiLogExecution('DEBUG', 'CSV', 'to_sum : ' + to_sum);
											
											CSV_String = CSV_String + '' + temp_fusion_id + ',' + temp_inc_id + ',' + request.getParameter('custpage_to_date') + ',Task ID,' +
											st_1 +
											',' +
											st_2 +
											',' +
											st_3 +
											',' +
											st_4 +
											',' +
											st_5 +
											',' +
											st_6 +
											',' +
											st_7 +
											',' +
											ot_1 +
											',' +
											ot_2 +
											',' +
											ot_3 +
											',' +
											ot_4 +
											',' +
											ot_5 +
											',' +
											ot_6 +
											',' +
											ot_7 +
											',' +
											dt_1 +
											',' +
											dt_2 +
											',' +
											dt_3 +
											',' +
											dt_4 +
											',' +
											dt_5 +
											',' +
											dt_6 +
											',' +
											dt_7 +
											',' +
											to_1 +
											',' +
											to_2 +
											',' +
											to_3 +
											',' +
											to_4 +
											',' +
											to_5 +
											',' +
											to_6 +
											',' +
											to_7 +
											',' +
											st_sum +
											',' +
											ot_sum +
											',' +
											dt_sum +
											',' +
											to_sum +
											',\n';
											
											nlapiLogExecution('DEBUG', 'CSV', 'If last row...');
										}
									}
									remaining_usage = context.getRemainingUsage();
									//nlapiLogExecution('DEBUG', 'SUT_Timesheet_Report', 'remaining_usage : ' + remaining_usage);
									/*if (i >= 999) //
									 {
									 if (previousInternalID != i_internalID) //
									 {
									 breakFlag = 1;
									 }
									 }
									 
									 if (breakFlag == 1) //
									 {
									 break;
									 }*/
								} // end for loop
							} // end of validate search result
							else //
							{
								nlapiLogExecution('DEBUG', 'SUT_Timesheet_Report', 'search_result is invalid : ' + search_result);
							}
						} // end of for loop
					}
				} // end of Do
				//while (search_result_0 != null)
				while (search_result != null)
				{
					//return CSV_String;
				}
				
				nlapiLogExecution('DEBUG', 'CSV_Data', 'After while loop !!!');
				
				// File export
				var fileName = 'TS_Report_'
				var Datetime = new Date();
				var CSVName = fileName + " - " + Datetime + '.csv';
				//nlapiLogExecution('DEBUG', 'CSV_Data', 'CSV_DATA : ' + CSV_String);
				
				var file = nlapiCreateFile(CSVName, 'CSV', CSV_String.toString());
				//file.setFolder(635);
				//var file_id = nlapiSubmitFile(file);
				//nlapiLogExecution('DEBUG', 'beforeLoad_CalculateWeek', 'file_id ' + file_id);
				
				//nlapiLogExecution('DEBUG', 'suitlet_Po_ClosedStatus', ' PDF Name-->  ');
				//response.setContentType('CSV', CSVName);
				response.setContentType('CSV', CSVName, 'inline');
				
				//nlapiLogExecution('DEBUG', 'suitlet_Po_ClosedStatus', ' PDF Name-->  ');
				//nlapiLogExecution('DEBUG', 'beforeLoad_CalculateWeek', 'file.getValue() ' + file.getValue());
				response.write(file.getValue());
				//window.open(file);
			}
			else //
			{
				nlapiLogExecution('DEBUG', 'SUT_Timesheet_Report', 'from_date is invalid : ' + from_date);
			}
		} //
		catch (ex) //
		{
			nlapiLogExecution('ERROR', 'exception', ex);
			nlapiLogExecution('ERROR', 'exception', ex.message);
		}
		//response.writePage(form);
	}
}

function _validate(obj) //
{
	if(obj != null && obj != '' && obj != 'undefined')//
	{
		return true;
	}
	else //
	{
		return false;
	}
}

function get_Formated_Date(d_date) //
{
	var today = new Date(d_date);
	var dd = today.getDate();
	var mm = today.getMonth() + 1; 
	var yyyy = today.getFullYear();
	
	d_date = mm + '/' + dd + '/' + yyyy;
	
	return d_date;
}

function get_todays_date(d_date)//
{
	var today;
	// ============================= Todays Date ==========================================
	
	var date1 = new Date();
	//nlapiLogExecution('DEBUG', 'schedulerFunction', 'Date ==' + date1);
	
	date1 = d_date;
	
	var offsetIST = 5.5;
	
	//To convert to UTC datetime by subtracting the current Timezone offset
	var utcdate = new Date(date1.getTime() + (date1.getTimezoneOffset() * 60000));
	//nlapiLogExecution('DEBUG', 'schedulerFunction', 'UTC Date' + utcdate);
	
	//Then cinver the UTS date to the required time zone offset like back to 5.5 for IST
	var istdate = new Date(utcdate.getTime() - ((-offsetIST * 60) * 60000));
	//nlapiLogExecution('DEBUG', 'schedulerFunction', 'IST Date ==' + istdate);
	
	var day = istdate.getDate();
	//nlapiLogExecution('DEBUG', 'schedulerFunction', 'Day ==' + day);
	
	var month = istdate.getMonth() + 1;
	//nlapiLogExecution('DEBUG', 'schedulerFunction', 'Month ==' + month);
	
	var year = istdate.getFullYear();
	//nlapiLogExecution('DEBUG', 'schedulerFunction', 'Year ==' + year);
	
	var date_format = checkDateFormat();
	//nlapiLogExecution('DEBUG', 'schedulerFunction', 'Date Format ==' + date_format);
	
	if (date_format == 'YYYY-MM-DD') {
		today = year + '-' + month + '-' + day;
	}
	if (date_format == 'DD/MM/YYYY') {
		today = day + '/' + month + '/' + year;
	}
	if (date_format == 'MM/DD/YYYY') {
		today = month + '/' + day + '/' + year;
	}
	var s_month = '';
	
	if (month == 1) {
		s_month = 'Jan'
		
	}
	else 
		if (month == 2) {
			s_month = 'Feb'
			
		}
		else 
			if (month == 3) {
			
				s_month = 'Mar'
			}
			else 
				if (month == 4) {
					s_month = 'Apr'
					
				}
				else 
					if (month == 5) {
						s_month = 'May'
						
					}
					else 
						if (month == 6) {
						
							s_month = 'Jun'
						}
						else 
							if (month == 7) {
								s_month = 'Jul'
								
							}
							else 
								if (month == 8) {
									s_month = 'Aug'
									
								}
								else 
									if (month == 9) {
										s_month = 'Sep'
										
									}
									else 
										if (month == 10) {
											s_month = 'Oct'
											
										}
										else 
											if (month == 11) {
											
												s_month = 'Nov'
											}
											else 
												if (month == 12) {
												
													s_month = 'Dec'
												}
	
	if (date_format == 'DD-Mon-YYYY') {
		today = day + '-' + s_month + '-' + year;
	}
	
	
	return today;
}


// END SUITELET ====================================================




// BEGIN OBJECT CALLED/INVOKING FUNCTION ===================================================
{



}
// END OBJECT CALLED/INVOKING FUNCTION =====================================================