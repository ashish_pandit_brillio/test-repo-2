/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       22 Jan 2019     Aazamali Khan
 *
 */

/**
 * @param {nlobjRequest} request Request object
 * @param {nlobjResponse} response Response object
 * @returns {Void} Any output is written via response object
 */
function GetAllocationDetails(request, response) {
    var body = request.getBody();
    var jsonBody = JSON.parse(body);
    var n_frf = jsonBody.frf; //bodyObj.frf.id;
    var searchResult = GetFRFDetails(n_frf);
    if (searchResult) {
        var empId = nlapiLookupField("customrecord_frf_details", n_frf, "custrecord_frf_details_selected_emp");
        var empName = nlapiLookupField("customrecord_frf_details", n_frf, "custrecord_frf_details_selected_emp", true);
        if (empId) {
            var searchRes = GetAllocation(empId);
            nlapiLogExecution("ERROR", "searchRes : ", JSON.stringify(searchRes));
            if (searchRes) {
                var jsonObj = {};
                var projectAllocated = searchRes[0].getText("company");
                var percentTime = searchRes[0].getValue("percentoftime");
                var billRate = searchRes[0].getValue("custevent3");
                var startDate = searchRes[0].getValue("startdate");
                var endDate = searchRes[0].getValue("enddate");
                var role = searchRes[0].getValue("title", "employee", null);
                var level = searchRes[0].getValue("employeestatus", "employee", null);
                var practice = searchRes[0].getText("department", "employee", null);
                var searchResult = GetPrimarySkills(empId);
                var primarySkills;
                var secondarySkills;
                var family;
                if (searchResult) {
                    primarySkills = searchResult[0].getText("custrecord_primary_updated");
                    secondarySkills = searchResult[0].getValue("custrecord_secondry_updated");
                    family = searchResult[0].getText("custrecord_family_selected");
                } else {
                    primarySkills = searchResult;
                    secondarySkills = searchResult;
                    family = searchResult;
                }
                jsonObj = {
                    "empName": GetManagerName(empName),
                    "primarySkill": primarySkills,
                    "secondaryskill": secondarySkills,
                    "family": family,
                    "rate": billRate,
                    "startdate": startDate,
                    "enddate": endDate,
                    "role": role,
                    "level": level,
                    "percentofallocation": percentTime,
                    "projectAllocated": projectAllocated,
                    "practice": GetHeadPractice(practice)
                };
                response.write(JSON.stringify(jsonObj));
            } else {
                response.write("{}");
            }
        } else {
            response.write("{}");
        }
    } else {
        response.write("{}");
    }
}

function GetFRFDetails(n_frf) {
    var searchResult = nlapiSearchRecord("customrecord_frf_details", null, [
        ["internalid", "is", n_frf]
    ], []);
    return searchResult;
}

function GetAllocation(empId) {
    var searchResult = nlapiSearchRecord("resourceallocation", null,
        [
            ["resource", "anyof", empId]
        ],
        [
            new nlobjSearchColumn("startdate").setSort(true),
            new nlobjSearchColumn("enddate"),
            new nlobjSearchColumn("custevent3"),
            new nlobjSearchColumn("percentoftime"),
            new nlobjSearchColumn("company"),
            new nlobjSearchColumn("title", "employee", null),
            new nlobjSearchColumn("employeestatus", "employee", null),
            new nlobjSearchColumn("department", "employee", null)
        ]
    );
    return searchResult;
}

function GetManagerName(tempString) {
    var s_manager = "";
    //var tempString = searchResult[int].getText("custentity_projectmanager","custrecord_project_internal_id_sfdc",null);
    temp = tempString.indexOf("-");
    if (temp > 0) {
        var s_manager = tempString.split("-")[1];
    } else {
        var s_manager = tempString;
    }
    return s_manager;
}

function GetHeadPractice(tempString) {
    var s_manager = "";
    //var tempString = searchResult[int].getText("custentity_projectmanager","custrecord_project_internal_id_sfdc",null);
    temp = tempString.indexOf(":");
    if (temp > 0) {
        var s_manager = tempString.split(":")[1];
    } else {
        var s_manager = tempString;
    }
    return s_manager;
}

function GetPrimarySkills(empId) {
    nlapiLogExecution("DEBUG", "employee ID", empId);
    var search = nlapiSearchRecord("customrecord_employee_master_skill_data", null,
        [
            ["custrecord_employee_skill_updated", "anyof", empId]
        ],
        [
            new nlobjSearchColumn("custrecord_primary_updated"),
            new nlobjSearchColumn("custrecord_secondry_updated"),
            new nlobjSearchColumn("custrecord_skill_status"),
            new nlobjSearchColumn("custrecord_family_selected"),
            new nlobjSearchColumn("custrecord_employee_approver"),
            new nlobjSearchColumn("custrecord_new_skill_acquired")
        ]
    );
    if (search) {
        return search;
    } else {
        return "";
    }
}