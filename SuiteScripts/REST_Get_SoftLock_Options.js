/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       06 Mar 2019     Aazamali Khan
 *
 */

/**
 * @param {Object} dataIn Parameter object
 * @returns {Object} Output object
 */
function getRESTlet(dataIn) {
	  try {
	        // Request body from frontend
	        var body = dataIn;//request.getBody();
	        nlapiLogExecution('AUDIT', 'body', body);
	        var mode, firstIndex, secondIndex, rfNumber, user, adminEmp, skillsArray, skillFamily, frfEndDate, index;
	        if (_logValidation(body)) {
	            var bodyObj = body;
	            if (bodyObj.mandatorySkills)
	                var skillsArray = bodyObj.mandatorySkills;
	            if (bodyObj.skillFamily)
	                var skillFamily = bodyObj.skillFamily;
	            if (bodyObj.endDate)
	                var endDate = bodyObj.endDate;
	            if (!(bodyObj.mode)) {
	                mode = 0;
	            } else {
	                mode = bodyObj.mode;
	            }
	            if (bodyObj.frfNumber)
	                frfNumber = bodyObj.frfNumber;
	            if (bodyObj.user)
	                user = bodyObj.user;
	        }
	        // Logged in user 
	        if (user) {
	            var getAdminSearch = getFulfillmentOfficer(user);
	            if (getAdminSearch) {
	                adminEmp = getAdminSearch[0].getValue("custrecord_fuel_admin_name");
	            }
	        }
	        if (_logValidation(endDate)) {
	            frfEndDate = endDate; //endDate;
	        }
	        var dataOut = [];
	        var finalOut = {};
	        var jsonObj = {};
	        // Get persona for the logged in user
	        if (_logValidation(adminEmp)) {
	            var flag = nlapiLookupField("customrecord_frf_details", frfNumber, "custrecord_frf_details_external_hire");
	            if (flag == "F") {
	                jsonObj = {
	                    "mode": "fulfillment"
	                };
	            } else {
	                jsonObj = {
	                    "mode": "externalHire"
	                };
	            }
	        } else {
	            var flag = nlapiLookupField("customrecord_frf_details", frfNumber, "custrecord_frf_details_external_hire");
	            if (flag == "F") {
	                jsonObj = {
	                    "mode": "internalFulfillment"
	                };
	            } else {
	                jsonObj = {
	                    "mode": "externalHire"
	                };
	            }
	        }
	        // push the mode of persona 
	        dataOut.push(jsonObj);
	        /***************************************/
	        // Get Fulfillment records count and push to object 
			if(_logValidation(skillsArray) && _logValidation(skillFamily)){
	        searchResult = getEmployeeSkillPriority(skillsArray, skillFamily);
	        }
	        if (searchResult) {
	            finalOut.totalRecords = searchResult.length;
	        } else {
	            finalOut.totalRecords = "0";
	        }
	        finalOut.pageNumber = parseInt(mode) + 1;

	        /**************************************/

	        var frfDetailsObj = nlapiLoadRecord("customrecord_frf_details", frfNumber);
	        var empIds = [];
	        empIds = frfDetailsObj.getFieldValues("custrecord_frf_details_suggest_pref_mat");
	        nlapiLogExecution("ERROR","before slice empIds",JSON.stringify(empIds));
			nlapiLogExecution("ERROR","before slice empIds",typeof(empIds));
			softLockedEmp = frfDetailsObj.getFieldValue("custrecord_frf_details_selected_emp");
			nlapiLogExecution("ERROR","softLockedEmp",softLockedEmp);
			if(empIds){      
			var counter = empIds.indexOf(softLockedEmp);
			if (counter > -1) {
				empIds.splice(counter, 1); //splice(index,howmany)
			}
			empIds = _.uniq(empIds);  
	        }
	        /*********************************************************/
	        // Get softlocked employees
	        if (softLockedEmp) {
	            var empId = softLockedEmp;
	            s_visa = GetVisa(empId);
	            if (s_visa) {
	                s_visa = s_visa;
	            } else {
	                s_visa = "NA";
	            }
	            empRecObj = nlapiLoadRecord("employee", empId);
	            s_employeeName = empRecObj.getFieldValue("entityid"); //nlapiLookupField("employee",empId,"entityid");
	            s_practice = empRecObj.getFieldText("department"); //;nlapiLookupField("employee",empId,"department");
	            i_practice = empRecObj.getFieldValue("department"); //nlapiLookupField("employee",empId,"department",true);
	            s_empLevel = empRecObj.getFieldValue("employeestatus");
	            s_role = empRecObj.getFieldValue("title");
	            s_location = empRecObj.getFieldText("location");
	            i_location = empRecObj.getFieldValue("location");

	            // Get below information from employee records itself 
	            var searchEmpResult = getEmployeeSkillDetails(empId);
	            if (searchEmpResult) {
	                for (var int = 0; int < searchEmpResult.length; int++) {
	                    s_skillFamily = searchEmpResult[int].getText("custrecord_primary_updated");
	                    a_skillFamily = searchEmpResult[int].getValue("custrecord_primary_updated"); //frfDetailsObj.getFieldValue("custrecord_frf_details_primary_skills");
	                    i_family = searchEmpResult[int].getValue("custrecord_family_selected");
	                    s_family = searchEmpResult[int].getText("custrecord_family_selected");
	                }
	            }
	            var o_searchResult = getAllocationEndDate(empId);
	            if (o_searchResult) {
	                s_avaliablity = "Avaliable From : " + o_searchResult[0].getValue("enddate");
	                i_project = o_searchResult[0].getValue("project");
	                i_customer = i_project ? nlapiLookupField("job", i_project, "custentity_customerlob") : "";
	                s_customer = i_project ? nlapiLookupField("job", i_project, "custentity_customerid") : "";
	            } else {
	                s_avaliablity = "Available";
	            }
	            s_reportingManager = empRecObj.getFieldText("custentity_reportingmanager");
	            jsonObj1 = {};
	            jsonObj1 = {
	                "preferred": {
	                    "id": true,
	                    "name": true
	                },
	                "visa": {
	                    "name": s_visa
	                },
	                "employee": {
	                    "name": GetManagerName(s_employeeName),
	                    "id": empId
	                },
	                "practice": {
	                    "name": s_practice,
	                    "id": i_practice
	                },
	                "emplevel": {
	                    "name": s_empLevel
	                },
	                "skillfamily": {
	                    "name": s_family,
	                    "id": i_family
	                },
	                "primaryskill": {
	                    "name": s_skillFamily,
	                    "id": a_skillFamily
	                },
	                "role": {
	                    "name": s_role
	                },
	                "location": {
	                    "name": s_location,
	                    "id": i_location
	                },
	                "customer": {
	                    "name": s_customer,
	                    "id": i_customer
	                },
	                "availablefrom": {
	                    "name": s_avaliablity
	                },
	                "mandatorySkills": {
	                    "name": "Good Match"
	                },
	                "reportingManager": {
	                    "name": GetManagerName(s_reportingManager)
	                }
	            };
	            dataOut.push(jsonObj1);
	        }
	        //End 
	        /*********************************************************/
	        // Write logic to get the suggested employee details from FRF 
	        /*********************************************************/
	        if (_logValidation(empIds)) {
				nlapiLogExecution("AUDIT","empIds",JSON.stringify(empIds));
	            for (var i = 0; i < empIds.length; i++) {
	                var s_visa, s_employeeName, s_practice, i_practice, s_empLevel, s_role, s_location, i_location, s_skillFamily, a_skillFamily, i_family, s_family, i_project, i_customer, s_customer, s_reportingManager, s_avaliablity, empId;
	                index = empIds.length;
	                var empId = empIds[i];
	                nlapiLogExecution("AUDIT","empId", empId);
	                s_visa = GetVisa(empId);
	                if (s_visa) {
	                    s_visa = s_visa;
	                } else {
	                    s_visa = "NA";
	                }
	                empRecObj = nlapiLoadRecord("employee", empId);
	                s_employeeName = empRecObj.getFieldValue("entityid"); //nlapiLookupField("employee",empId,"entityid");
	                s_practice = empRecObj.getFieldText("department"); //;nlapiLookupField("employee",empId,"department");
	                i_practice = empRecObj.getFieldValue("department"); //nlapiLookupField("employee",empId,"department",true);
	                s_empLevel = empRecObj.getFieldValue("employeestatus");
	                s_role = empRecObj.getFieldValue("title");
	                s_location = empRecObj.getFieldText("location");
	                i_location = empRecObj.getFieldValue("location");

	                // Get below information from employee records itself 
	                var searchEmpResult = getEmployeeSkillDetails(empId);
					nlapiLogExecution("AUDIT","searchEmpResult",JSON.stringify(searchEmpResult))
	                if (searchEmpResult) {
	                    for (var int = 0; int < searchEmpResult.length; int++) {
	                        s_skillFamily = searchEmpResult[int].getText("custrecord_primary_updated");
	                        a_skillFamily = searchEmpResult[int].getValue("custrecord_primary_updated");
	                        i_family = searchEmpResult[int].getValue("custrecord_family_selected");
	                        s_family = searchEmpResult[int].getText("custrecord_family_selected");
	                    }
	                }
	                var o_searchResult = getAllocationEndDate(empId);
	                if (o_searchResult) {
	                    s_avaliablity = "Avaliable From : " + o_searchResult[0].getValue("enddate");
	                    i_project = o_searchResult[0].getValue("project");
	                    i_customer = i_project ? nlapiLookupField("job", i_project, "custentity_customerlob") : "";
	                    s_customer = i_project ? nlapiLookupField("job", i_project, "custentity_customerid") : "";
	                } else {
	                    s_avaliablity = "Available";
	                }
	                s_reportingManager = empRecObj.getFieldText("custentity_reportingmanager");
	                jsonObj1 = {};
	                jsonObj1 = {
	                    "preferred": {
	                        "id": true
	                    },
	                    "visa": {
	                        "name": s_visa
	                    },
	                    "employee": {
	                        "name": GetManagerName(s_employeeName),
	                        "id": empId
	                    },
	                    "practice": {
	                        "name": s_practice,
	                        "id": i_practice
	                    },
	                    "emplevel": {
	                        "name": s_empLevel
	                    },
	                    "skillfamily": {
	                        "name": s_family,
	                        "id": i_family
	                    },
	                    "primaryskill": {
	                        "name": s_skillFamily,
	                        "id": a_skillFamily
	                    },
	                    "role": {
	                        "name": s_role
	                    },
	                    "location": {
	                        "name": s_location,
	                        "id": i_location
	                    },
	                    "customer": {
	                        "name": s_customer,
	                        "id": i_customer
	                    },
	                    "availablefrom": {
	                        "name": s_avaliablity
	                    },
	                    "mandatorySkills": {
	                        "name": "Good Match"
	                    },
	                    "reportingManager": {
	                        "name": GetManagerName(s_reportingManager)
	                    }
	                };
	                dataOut.push(jsonObj1);
	            }
	        }
	        /************************************************/

	        /************************************************/
	        // Get the Index for pagination
	        //nlapiLogExecution("ERROR","mode",mode);
	        if (mode != "0") {
	            firstIndex = parseInt(mode) * 10;
	            secondIndex = firstIndex + 10
	        } else {
	            firstIndex = 0;
	            secondIndex = (firstIndex + 10) - index;
	        }
	        /************************************************************************************/
	        // Get the suggestions for fulfillment.
	        if (searchResult) {
	            // hard coded index to 25 
	            secondIndex = 25
	            if (searchResult.length < secondIndex) {
	                secondIndex = searchResult.length;
	            }
	            for (var int = 0 /*firstIndex*/ ; int < secondIndex; int++) {
	                var obj = {};
	                obj.preferred = {
	                    "id": false
	                };
	                var empId = searchResult[int].getValue("custrecord_employee_skill_updated");
	                var empName = searchResult[int].getText("custrecord_employee_skill_updated");
	                var availableSkills = searchResult[int].getValue("formulanumeric");
	                var visaSearch = GetVisa(empId);
	                if (visaSearch) {
	                    obj.visa = {
	                        "name": visaSearch[0].getText("custrecord_visa")
	                    };
	                } else {
	                    obj.visa = {
	                        "name": "NA"
	                    };
	                }
	                obj.employee = {
	                    "name": GetManagerName(empName),
	                    "id": empId
	                };
	                obj.practice = {
	                    "name": nlapiLookupField("employee", empId, "department", true),
	                    "id": nlapiLookupField("employee", empId, "department", false)
	                };
	                obj.emplevel = {
	                    "name": nlapiLookupField("employee", empId, "employeestatus")
	                };
	                obj.skillfamily = {
	                    "name": searchResult[int].getText("custrecord_family_selected"),
	                    "id": searchResult[int].getValue("custrecord_family_selected")
	                };
	                obj.primaryskill = {
	                    "name": searchResult[int].getText("custrecord_primary_updated"),
	                    "id": searchResult[int].getValue("custrecord_primary_updated")
	                };
	                var searchResultAllocation = getAllocationStatus(empId, frfEndDate);
	                if (searchResultAllocation) {
	                    var workLocation = searchResultAllocation[0].getText("location", "employee", null);
	                    var projectCategory = searchResultAllocation[0].getText("custentity_project_allocation_category", "job", null);
	                    nlapiLogExecution('DEBUG', 'Project Category : ', projectCategory);
	                    obj.role = {
	                        "name": searchResultAllocation[0].getValue("title", "employee", null)
	                    };
	                    obj.location = {
	                        "name": workLocation,
	                        "id": searchResultAllocation[0].getValue("location", "employee", null)
	                    };
	                    obj.customer = {
	                        "name": searchResultAllocation[0].getText("customer"),
	                        "id": searchResultAllocation[0].getValue("customer")
	                    };

	                    if (projectCategory == "Bench") {
	                        s_status = "Bench";
	                        obj.availablefrom = {
	                            "name": "Available" + " : " + s_status
	                        };
	                        //tempArr = [resultArray.length,resultArray,empId,employeeName,s_status,workLocation];
	                    } else {
	                        s_status = searchResultAllocation[0].getValue("enddate");
	                        obj.availablefrom = {
	                            "name": "Available From" + " : " + s_status
	                        };
	                        //tempArr = [resultArray.length,resultArray,empId,employeeName,s_status,workLocation];
	                    }
	                } else {
	                    var workLocation = nlapiLookupField("employee", empId, "location", true);
	                    var locationId = nlapiLookupField("employee", empId, "location");
	                    var role = nlapiLookupField("employee", empId, "title");
	                    obj.role = {
	                        "name": role
	                    };
	                    obj.location = {
	                        "name": workLocation,
	                        "id": locationId
	                    };
	                    obj.customer = {
	                        "name": "NA",
	                        "id": ""
	                    };
	                    obj.availablefrom = {
	                        "name": "Available"
	                    }
	                    //tempArr = [resultArray.length,resultArray,empId,employeeName,s_status,nlapiLookupField("employee",empId,"location",true)];
	                }
	                getMatchRating(skillsArray.length, availableSkills, obj);
	                dataOut.push(obj);
	            }
	        }
	        finalOut.data = dataOut;
	        //nlapiLogExecution("ERROR","finalOut : ",JSON.stringify(finalOut));
	        return {data : finalOut};
	    } catch (error) {
	        nlapiLogExecution('DEBUG', 'Error ', error);
	    }

	}
	function getEmployeeWithSkills(skillArray) {
	    var searchResult = nlapiSearchRecord("customrecord_employee_master_skill_data", null,
	        [
	            ["custrecord_primary_updated", "anyof", skillArray]
	        ],
	        [
	            new nlobjSearchColumn("custrecord_employee_skill_updated"),
	            new nlobjSearchColumn("custrecord_primary_updated")
	        ]
	    );
	    return searchResult;
	}

	function getAllocationStatus(empId, endDate) {
	    var resourceallocationSearch = nlapiSearchRecord("resourceallocation", null,
	        [
	            ["enddate", "onorafter", endDate],
	            "AND",
	            ["resource", "anyof", empId]
	        ],
	        [
	            new nlobjSearchColumn("id").setSort(false),
	            new nlobjSearchColumn("resource"),
	            new nlobjSearchColumn("customer"),
	            new nlobjSearchColumn("company"),
	            new nlobjSearchColumn("projecttask"),
	            new nlobjSearchColumn("numberhours"),
	            new nlobjSearchColumn("startdate"),
	            new nlobjSearchColumn("percentoftime"),
	            new nlobjSearchColumn("allocationunit"),
	            new nlobjSearchColumn("notes"),
	            new nlobjSearchColumn("enddate"),
	            new nlobjSearchColumn("allocationtype"),
	            new nlobjSearchColumn("custeventbstartdate"),
	            new nlobjSearchColumn("custeventbenddate"),
	            new nlobjSearchColumn("custeventrbillable"),
	            new nlobjSearchColumn("custeventwlocation"),
	            new nlobjSearchColumn("custevent_practice"),
	            new nlobjSearchColumn("custevent1"),
	            new nlobjSearchColumn("custevent2"),
	            new nlobjSearchColumn("custevent3"),
	            new nlobjSearchColumn("custevent4"),
	            new nlobjSearchColumn("custevent_otbillable"),
	            new nlobjSearchColumn("custevent_otpayable"),
	            new nlobjSearchColumn("custevent_otrate"),
	            new nlobjSearchColumn("custevent_tenuredisctapplicable"),
	            new nlobjSearchColumn("custevent_workcityra"),
	            new nlobjSearchColumn("custevent_workstatera"),
	            new nlobjSearchColumn("custevent_holiday_service_item"),
	            new nlobjSearchColumn("custevent_leave_service_item"),
	            new nlobjSearchColumn("custevent_ra_is_shadow"),
	            new nlobjSearchColumn("custevent_30_days_prior_notification"),
	            new nlobjSearchColumn("custentity_project_allocation_category", "job", null),
	            new nlobjSearchColumn("title", "employee", null),
	            new nlobjSearchColumn("location", "employee", null)
	        ]
	    );
	    return resourceallocationSearch;
	}
	function createString(skillArray) {
	    var temp = "";
	    for (var int = 0; int < skillArray.length; int++) {
	        temp += "CASE WHEN INSTR({custrecord_primary_updated.id}," + skillArray[int] + ",1)>0 THEN 1 ELSE 0 END";
	        if ((skillArray.length - int) > 1) {
	            temp += " +";
	        }
	    }
	    return temp;
	}
	function getEmployeeSkillPriority(arrSkills, familyId) {
	    arrSkills = getSkillIds(arrSkills);
	    var customrecord_employee_master_skill_dataSearch = nlapiSearchRecord("customrecord_employee_master_skill_data", null,
	        [
	            ["custrecord_primary_updated", "anyof", arrSkills],
	            "AND",
	            ["custrecord_family_selected", "anyof", familyId], "AND",
	            ["custrecord_employee_skill_updated.custentity_employee_inactive", "is", "F"]
	        ],
	        [
	            new nlobjSearchColumn("custrecord_employee_skill_updated"),
	            new nlobjSearchColumn("custrecord_primary_updated"),
	            new nlobjSearchColumn("custrecord_family_selected"),
	            new nlobjSearchColumn("formulatext").setFormula("{custrecord_primary_updated.id}"),
	            new nlobjSearchColumn("formulanumeric").setFormula(createString(arrSkills)).setSort(true)
	        ]
	    );
	    return customrecord_employee_master_skill_dataSearch;
	}
	function getMatchRating(requireSkills, avaliableSkills, obj) {
	    var result = (parseInt(avaliableSkills) / parseInt(requireSkills)) * 100;
	    nlapiLogExecution('AUDIT', 'result : ', result);
	    if (Math.round(result) > 60) {
	        obj.mandatorySkills = {
	            "name": "Good Match" + " : " + avaliableSkills + "/" + requireSkills
	        }
	    } else {
	        obj.mandatorySkills = {
	            "name": "Partial Match" + " : " + avaliableSkills + "/" + requireSkills
	        }
	    }
	}
	function GetManagerName(tempString) {
	    var s_manager = "";
	    //var tempString = searchResult[int].getText("custentity_projectmanager","custrecord_project_internal_id_sfdc",null);
	    temp = tempString.indexOf("-");
	    if (temp > 0) {
	        var s_manager = tempString.split("-")[1];
	    } else {
	        var s_manager = tempString;
	    }
	    return s_manager;
	}
	function GetVisa(empId) {
	    var customrecord_empvisadetailsSearch = nlapiSearchRecord("customrecord_empvisadetails", null,
	        [
	            ["custrecord_visaid", "anyof", empId],
	            "AND",
	            ["custrecord_validtill", "notbefore", "today"]
	        ],
	        [
	            new nlobjSearchColumn("scriptid").setSort(false),
	            new nlobjSearchColumn("custrecord_visa"),
	            new nlobjSearchColumn("custrecord27"),
	            new nlobjSearchColumn("custrecord_validtill"),
	            new nlobjSearchColumn("custrecord28")
	        ]
	    );
	    return customrecord_empvisadetailsSearch;
	}
	function _logValidation(value) {
	    if (value != null && value != '' && value != undefined && value != 'NaN') {
	        return true;
	    } else {
	        return false;
	    }
	}
	function getFulfillmentOfficer(i_user) {
	    var customrecord_fuel_adminSearch = nlapiSearchRecord("customrecord_fuel_admin", null,
	        [
	            ["custrecord_fuel_admin_name", "anyof", i_user]
	        ],
	        [
	            new nlobjSearchColumn("custrecord_fuel_admin_name")
	        ]
	    );
	    return customrecord_fuel_adminSearch;
	}
	function getAllocationEndDate(empId) {
	    var resourceallocationSearch = nlapiSearchRecord("resourceallocation", null,
	        [
	            ["startdate", "onorbefore", "today"],
	            "AND",
	            ["enddate", "onorafter", "today"],
	            "AND",
	            ["resource", "anyof", empId]
	        ],
	        [
	            new nlobjSearchColumn("enddate")
	        ]
	    );
	    return resourceallocationSearch;
	}
	function getSkillIds(s_skills) // this will work to convert string ids to array 
	{
	    var resultArray = new Array();
	    if (_logValidation(s_skills)) {
	        nlapiLogExecution('ERROR', 's_skills in function', s_skills);
	        var temp = s_skills.split(',');
	        for (var i = 0; i < temp.length; i++) {

	            resultArray[i] = temp[i];
	        }
	    }
	    return resultArray;
	}
	function getEmployeeSkillDetails(empId) {
	    var customrecord_employee_master_skill_dataSearch = nlapiSearchRecord("customrecord_employee_master_skill_data", null,
	        [
	            ["custrecord_employee_skill_updated", "anyof", empId]
	        ],
	        [
	            new nlobjSearchColumn("scriptid").setSort(false),
	            new nlobjSearchColumn("custrecord_employee_skill_updated"),
	            new nlobjSearchColumn("custrecord_primary_updated"),
	            new nlobjSearchColumn("custrecord_secondry_updated"),
	            new nlobjSearchColumn("custrecord_skill_status"),
	            new nlobjSearchColumn("custrecord_family_selected"),
	            new nlobjSearchColumn("custrecord_employee_approver"),
	            new nlobjSearchColumn("custrecord_new_skill_acquired")
	        ]
	    );
	    return customrecord_employee_master_skill_dataSearch;
	}