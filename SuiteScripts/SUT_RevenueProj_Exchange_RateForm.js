/**
 * @author Jayesh
 */

function designForm(request,response)
{
	try
	{
		var form_obj = nlapiCreateForm("Revenue Projection Exchange Rate Sheet");
		
		if (request.getMethod() == 'GET')
		{
			var inrField = form_obj.addField('exchange_rate_inr', 'currency','USD to INR ( 1 USD = x INR )');
			var gbpField = form_obj.addField('exchange_rate_gbp', 'currency','USD to GBP ( 1 USD = x GBP )');
			var proj_end_date =	form_obj.addField('custpage_enddate', 'date', 'Project End Date After').setMandatory(true);
			var fld_Rate_Type =	form_obj.addField('custpage_report_type', 'select', 'Report Type');
			fld_Rate_Type.addSelectOption(1,'FP');
			fld_Rate_Type.addSelectOption(2,'TM');
			fld_Rate_Type.setDefaultValue(1);
			
			form_obj.addSubmitButton('Run Report');
			response.writePage(form_obj);
		}
		else
		{
			var inrField = request.getParameter('exchange_rate_inr');
			var gbpField = request.getParameter('exchange_rate_gbp');
			var d_pro_end_date = request.getParameter('custpage_enddate');
			var report_type = request.getParameter('custpage_report_type');
			
			if(report_type == 1)
			{
				var params=new Array();
				params['custscript_usd_inr_rate'] = inrField;
				params['custscript_usd_gbp_rate'] = gbpField;
				params['custscript_proj_end_date_aftr'] = d_pro_end_date;
				
				var status = nlapiScheduleScript('customscript_sch_revenue_report_fb',null,params);
			}
			else
			{
				var params=new Array();
				params['custscript_usd_inr_rate_today'] = inrField;
				params['custscript_usd_gbp_rate_today'] = gbpField;
				params['custscript_proj_end_to_use'] = d_pro_end_date;
				
				var status = nlapiScheduleScript('customscript1017',null,params);
			}
		}
	}
	catch(err)
	{
		nlapiLogExecution('ERROR','ERROR MESAGE:- ',err);
	}
}