/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       09 Jan 2015     amol.sahijwani
 *
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType
 * 
 * @param {String} type Operation types: create, edit, delete, xedit
 *                      approve, reject, cancel (SO, ER, Time Bill, PO & RMA only)
 *                      pack, ship (IF)
 *                      markcomplete (Call, Task)
 *                      reassign (Case)
 *                      editforecast (Opp, Estimate)
 * @returns {Void}
 */
// This script will set the location to blank if invoice subsidiary 
// and the employee subsidiary do not match to prevent the error on save of invoice.
function userEventBeforeSubmit(type){
	if(type == 'create' || type == 'edit')
		{
			// Get Billable Items Count
			var i_billable_time_count = nlapiGetLineItemCount('time');
		
			// Get Invoice Subsidiary
			var i_invoice_subsidiary = nlapiGetFieldValue('subsidiary');
			var filterExpression = new Array();
		
			for(var i = 1; i <= i_billable_time_count; i++)
				{
					var isApply = nlapiGetLineItemValue('time', 'apply', i);
					if(isApply == 'T')
						{
							var i_employee = nlapiGetLineItemValue('time', 'employee', i);
							filterExpression.push(['internalid', 'anyof', i_employee]);
							filterExpression.push('or');
						}
				}
			if(filterExpression.length != 0)
				{
					filterExpression.pop();
				}
		
			var searchEmployeeColumns = [];
			searchEmployeeColumns[0] = new nlobjSearchColumn('internalid');
			searchEmployeeColumns[1] = new nlobjSearchColumn('subsidiary');
		
			var searchEmployeeResult = nlapiSearchRecord('employee', null, filterExpression, searchEmployeeColumns);
		
			var a_employee_data = new Object();
		
			for(var i = 0; searchEmployeeResult != null && i < searchEmployeeResult.length; i++)
				{
					var i_employee_id = searchEmployeeResult[i].getValue('internalid');
				
					a_employee_data[i_employee_id] = searchEmployeeResult[i].getValue('subsidiary');
				}
		
			for(var i = 1; i <= i_billable_time_count; i++)
				{
					var isApply = nlapiGetLineItemValue('time', 'apply', i);
			
					if(isApply == 'T')
						{
							var i_employee = nlapiGetLineItemValue('time', 'employee', i);
					
							var i_employee_subsidiary = a_employee_data[i_employee];
					
							if(i_invoice_subsidiary != i_employee_subsidiary)
								{
									nlapiSetLineItemValue('time', 'location', i, '');
								}
						}
				}
		
		}
}