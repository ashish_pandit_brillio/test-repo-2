/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       09 Sep 2015     nitish.mishra
 *
 */

/**
 * @param {String}
 *            recType Record type internal id
 * @param {Number}
 *            recId Record internal id
 * @returns {Void}
 */
function massUpdate(recType, recId) {

	try {
		nlapiInitiateWorkflow('customerpayment', recId,
		        'customworkflow_mis_payment');
	} catch (err) {
		nlapiLogExecution('error', 'recId ' + recId, err);
	}
}
