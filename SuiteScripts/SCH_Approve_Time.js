/**
 * @author Shweta
 */
// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
/*
   	Script Name : SCH_Approve_Time.js
	Author      : Shweta Chopde
	Date        : 23 May 2014


	Script Modification Log:

	-- Date --			-- Modified By --				--Requested By--				-- Description --
    26 June 2014          Shweta Chopde                      Shekar                    1.) Added logic for deletetion of custom record for which the records are processsed.



Below is a summary of the process controls enforced by this script file.  The control logic is described
more fully, below, in the appropriate function headers and code blocks.


     SCHEDULED FUNCTION
		- scheduledFunction(type)


     SUB-FUNCTIONS
		- The following sub-functions are called by the above core functions in order to maintain code
            modularization:

               - NOT USED

*/
}
// END SCRIPT DESCRIPTION BLOCK  ====================================


// BEGIN SCHEDULED FUNCTION =============================================

function schedulerFunction(type)
{
  var a_split_data_array = new Array();	
  var i_ref_no;
  
  try
  {
  	  var i_context = nlapiGetContext();
	  
	  var d_to_date = i_context.getSetting('SCRIPT','custscript_date_till_now');
	  var i_employee = i_context.getSetting('SCRIPT','custscript_s_employee');
	  var a_data_array = i_context.getSetting('SCRIPT','custscript_data_array_approve');
	  var i_approve = i_context.getSetting('SCRIPT','custscript_approve_time');
	  var i_reject = i_context.getSetting('SCRIPT','custscript_reject_time');
	  var i_current_user = i_context.getSetting('SCRIPT','custscript_current_user_approve');	  
	  var i_selected_data_array = i_context.getSetting('SCRIPT','custscript_selected_data_array_sch');
	  var i_custom_recordID = i_context.getSetting('SCRIPT','custscript_custom_record_id_sch');
	  var i_counter = i_context.getSetting('SCRIPT','custscript_counter_time_approve');
	  	  
	 var s_scriptID =  i_context.getScriptId();
	 var s_script_deployment = i_context.getDeploymentId();
	 nlapiLogExecution('DEBUG', 'schedulerFunction',' Script ID -->' + s_scriptID);
	 nlapiLogExecution('DEBUG', 'schedulerFunction',' Script Deployment -->' + s_script_deployment);
	  
		       
	  i_current_user = parseInt(i_current_user);   
	  var i_current_user_name = get_employee_name((i_current_user))
	  
	 nlapiLogExecution('DEBUG', 'schedulerFunction',' Counter -->' + i_counter);
	 nlapiLogExecution('DEBUG', 'schedulerFunction',' To Date -->' + d_to_date);
	 nlapiLogExecution('DEBUG', 'schedulerFunction',' Employee -->' + i_employee);
	 nlapiLogExecution('DEBUG', 'schedulerFunction',' Data Array  -->' + a_data_array);
	 nlapiLogExecution('DEBUG', 'schedulerFunction',' Approve -->' + i_approve);
	 nlapiLogExecution('DEBUG', 'schedulerFunction',' Reject -->' + i_reject);
	 nlapiLogExecution('DEBUG', 'schedulerFunction',' Current User -->' + i_current_user);
	 nlapiLogExecution('DEBUG', 'schedulerFunction',' Current User Name -->' + i_current_user_name);
	 nlapiLogExecution('DEBUG', 'POST suiteletFunction',' Selected Data Array -->' + i_selected_data_array);	
	 nlapiLogExecution('DEBUG', 'POST suiteletFunction',' Custom Record ID -->' + i_custom_recordID);	
	 
	 
	 
	  if(_logValidation(i_custom_recordID))
	  {
	  	var o_customOBJ = nlapiLoadRecord('customrecord_time_process_record',i_custom_recordID);
		
		 if(_logValidation(o_customOBJ))
		 {
		 	var i_data_sel_process = o_customOBJ.getFieldValue('custrecord_data_p');			
			
			var i_employee_cr = o_customOBJ.getFieldValue('custrecord_emp_p');
						
			 var a_CR_array_values = new Array()	
			 var i_data_CR = new Array()
			 i_data_CR =  i_data_sel_process;
		 
		      for(var ct=0;ct<i_data_CR.length;ct++)
			  {
				 	a_CR_array_values = i_data_CR.split(',')
				    break;				
			  }	
		
			nlapiLogExecution('DEBUG', 'schedulerFunction','CR Array Values-->' +a_CR_array_values);
			nlapiLogExecution('DEBUG', 'schedulerFunction',' CR Array Values Length-->' + a_CR_array_values.length);
		
			nlapiLogExecution('DEBUG', 'POST suiteletFunction',' Data Sel Process -->' + i_data_sel_process);
			nlapiLogExecution('DEBUG', 'POST suiteletFunction',' Data Sel Process Length -->' + i_data_sel_process.length);	
	        nlapiLogExecution('DEBUG', 'POST suiteletFunction',' Employee CR -->' + i_employee_cr);
				
			if(_logValidation(i_counter))
			{
				i_counter = i_counter;
			}//if
			else
			{
				i_counter=0;
			}	
				
							  	 
			 if(_logValidation(a_data_array))
			 {
			 var a_TR_array_values = new Array()	
			 var i_data_TR = new Array()
			 i_data_TR =  a_data_array;
		 
		      for(var dt=0;dt<i_data_TR.length;dt++)
			  {
				 	a_TR_array_values = i_data_TR.split(',')
				    break;				
			  }	
		
			 nlapiLogExecution('DEBUG', 'schedulerFunction','TR Array Values-->' +a_TR_array_values);
			 nlapiLogExecution('DEBUG', 'schedulerFunction',' TR Array Values Length-->' + a_TR_array_values.length);
			
				for(var yt=i_counter;yt<a_TR_array_values.length;yt++)
				{
					a_split_data_array = a_TR_array_values[yt].split('#####')			
					
					i_employee = a_split_data_array[0];
		            i_internal_id = a_split_data_array[1];
		            i_customer = a_split_data_array[2];
		            i_duration = a_split_data_array[3];
									
					 if(_logValidation(i_approve) && _logValidation(a_CR_array_values))
					 {
					 	if(a_CR_array_values.indexOf(i_internal_id)>-1)
						{						
						 	if(i_approve.indexOf('T')>-1)
							{
								approve_timesheet(i_internal_id)
								nlapiLogExecution('DEBUG', 'schedulerFunction',' ************ Approve ******** -->' + i_internal_id);
				
							}//Approve
							
						}//CR Array Values
					
					 }//Approve
					 
					 if(_logValidation(i_reject) && _logValidation(a_CR_array_values))
					 {
					 	if(a_CR_array_values.indexOf(i_internal_id)>-1)
						{
							if(i_reject.indexOf('T')>-1)
							{
								reject_timesheet(i_internal_id,i_current_user,i_employee)
								nlapiLogExecution('DEBUG', 'schedulerFunction',' ************ Reject ******** -->' + i_internal_id);
				
							}//Reject							
						}					 
					 }//Reject						
					
										 
					var i_usage_begin = i_context.getRemainingUsage();
		            nlapiLogExecution('DEBUG', 'suiteletFunction','Usage Begin -->' + i_usage_begin);
					 
					if(i_usage_begin<200)
					{
						 schedule_script_after_usage_exceeded(yt)
						 break;					 
					} 
											
				}//TR Loop					
				
				var i_deleteID = nlapiDeleteRecord('customrecord_time_process_record', i_custom_recordID);
				nlapiLogExecution('DEBUG', 'schedulerFunction',' ************ Delete Custom rec ID  ******** -->' + i_deleteID);
				
						
			 }//Data Array	 
		 }//Custom OBJ		
	  }//Custom ID
  }//TRY
  catch(exception)
  {
  	nlapiLogExecution('DEBUG', 'ERROR',' Exception Caught -->' + exception);	

    var i_deleteID = nlapiDeleteRecord('customrecord_time_process_record', i_custom_recordID);
	nlapiLogExecution('DEBUG', 'ERROR',' ************ Delete Custom rec ID  ******** -->' + i_deleteID);

  }//CATCH
}

// END SCHEDULED FUNCTION ===============================================





// BEGIN FUNCTION ===================================================



/**
 * 
 * @param {Object} value
 * 
 * Description --> If the value is blank /null/undefined returns false else returns true
 */
function _logValidation(value) 
{
 if(value != null && value.toString() != null && value != '' && value != undefined && value.toString() != undefined && value != 'undefined' && value.toString() != 'undefined'&& value.toString() != 'NaN' && value != NaN) 
 {
  return true;
 }
 else 
 { 
  return false;
 }
}

function approve_timesheet(i_internal_id)
{
  if(_logValidation(i_internal_id))
  {
  	var o_recordOBJ = nlapiLoadRecord('timebill',i_internal_id);
	
	if(_logValidation(o_recordOBJ))
	{
	 o_recordOBJ.setFieldValue('custcol_approvalstatus',2)
     o_recordOBJ.setFieldValue('approvalstatus',1)
	 o_recordOBJ.setFieldValue('supervisorapproval','T')
	 
	 var i_submitID = nlapiSubmitRecord(o_recordOBJ,true,true);
	 nlapiLogExecution('DEBUG', 'approve_timesheet',' ************ Time Submit ID *********** -->' + i_submitID);
		
	}//Record OBJ	
  }//Record ID	
}//Approve


function reject_timesheet(i_internal_id,i_current_user,i_employee)
{	
  if(_logValidation(i_internal_id))
  {
  	var i_deleteID = nlapiDeleteRecord('timebill',i_internal_id)
	nlapiLogExecution('DEBUG', 'reject_timesheet',' ************ Deleted Time Submit ID *********** -->' + i_deleteID);
	
	 if(_logValidation(i_deleteID))
	 {
	 	send_mail_reject_timesheet(i_deleteID,i_current_user,i_employee)	
	 }
	
	
  	/*
var o_recordOBJ = nlapiLoadRecord('timebill',i_internal_id);
	
	if(_logValidation(o_recordOBJ))
	{
	  o_recordOBJ.setFieldValue('custcol_approvalstatus',3)
	  o_recordOBJ.setFieldValue('supervisorapproval','F')
	  
	 var i_submitID = nlapiSubmitRecord(o_recordOBJ,true,true);
	 nlapiLogExecution('DEBUG', 'reject_timesheet',' ************ Time Submit ID *********** -->' + i_submitID);
	
	}//Record OBJ	
*/
  }//Record ID		
}//Reject





/**
 * 
 * @param {Object} array
 * 
 * Description --> Returns the unique elements of the array
 */
function removearrayduplicate(array)
{
   var newArray = new Array();
   label:for(var i=0; i<array.length;i++ )
     {  
     for(var j=0; j<array.length;j++ )
      {
      if(newArray[j]==array[i]) 
      continue label;
     }
     newArray[newArray.length] = array[i];
      }
   return newArray;
}


function send_mail_reject_timesheet(i_time_sheetID,i_current_user,i_employee)
{
    var i_author = i_current_user
	
    var i_receipient_mail = get_emailID(i_employee)
		
	var i_recipient = i_receipient_mail
	
	var s_email_body = 'Hi,\n'
	
	s_email_body+=' The submitted Timesheet has been rejected .\n\n'
	s_email_body+='Regards ,\n'
	s_email_body+='Brillio - Team'
	   
	var s_email_subject = ' Timesheet - '+i_time_sheetID+' has been rejected. '
	
	var records = new Object();
    records['entity'] = i_employee	
	
	if(_logValidation(i_author)&&_logValidation(i_recipient))
	{
	  nlapiSendEmail(i_author, i_recipient, s_email_subject, s_email_body, null , null, records, null);
	  nlapiLogExecution('DEBUG', 'send_mail_reject_expense', ' Email Sent ......');	
					
	}//Author / Recipient	
}//send_mail_reject_timesheet



function get_emailID(i_employee)
{
	var s_email;
	if(_logValidation(i_employee))
	{
		var o_employeeOBJ = nlapiLoadRecord('employee',i_employee);
		
		if(_logValidation(o_employeeOBJ))
		{
			s_email = o_employeeOBJ.getFieldValue('email')
			
		}//Employee OBJ
		
	}//Employee	
	return s_email;
}//Email

function get_employee_name(i_employee)
{
	var s_name;
	if(_logValidation(i_employee))
	{
		var o_employeeOBJ = nlapiLoadRecord('employee',i_employee);
		
		if(_logValidation(o_employeeOBJ))
		{
			var s_first_name = o_employeeOBJ.getFieldValue('firstname')
			var s_middle_name = o_employeeOBJ.getFieldValue('middlename')
			var s_last_name = o_employeeOBJ.getFieldValue('lastname')
			
			if(!_logValidation(s_first_name))
			{
				s_first_name = ''
			}
			if(!_logValidation(s_middle_name))
			{
				s_middle_name = ''
			}
			if(!_logValidation(s_last_name))
			{
				s_last_name = ''
			}
			
			s_name = s_first_name+''+s_middle_name+''+s_last_name;
			
		}//Employee OBJ
		
	}//Employee	
	return s_name;
	
}


function schedule_script_after_usage_exceeded(i)
{	
      nlapiLogExecution('DEBUG','schedule_script_after_usage_exceeded','a_salary_upload_monthly_ID_array -->'+ a_salary_upload_monthly_ID_array.length);
	
	
	////Define all parameters to schedule the script for voucher generation.
	 var params=new Array();
	 params['status']='scheduled';
 	 params['runasadmin']='T';
	 params['custscript_counter_time_approve']=i.toString();
		
	 	 
	 var startDate = new Date();
 	 params['startdate']=startDate.toUTCString();
	
	 var status=nlapiScheduleScript(nlapiGetContext().getScriptId(), nlapiGetContext().getDeploymentId(),params);
	 nlapiLogExecution('DEBUG','After Scheduling','Script Scheduled Status -->'+ status);
	 
	 ////If script is scheduled then successfuly then check for if status=queued
	 if (status == 'QUEUED') 
 	 {
		nlapiLogExecution('DEBUG', ' SCHEDULED', ' Script Is Re - Scheduled for -->'+i);
 	 }
}//fun close


// END FUNCTION =====================================================
