/**
 * Module Description
 * 
 * Version Date Author Remarks 1.00 Apr 30 2015 Nitish Mishra
 * 
 */

/**
 * @param {Object}
 *            dataIn Parameter object
 * @returns {Object} Output object
 */
function postRESTlet(dataIn) {

	try {
		nlapiLogExecution('debug', 'dataIn', JSON.stringify(dataIn));
		dataIn = JSON.parse(dataIn);
		nlapiLogExecution('debug', 'start point', dataIn.startpoint);

		var invoices = [ 127492, 127924, 127890, 127890, 127890, 127890, 127924 ];

		nlapiLogExecution('debug', 'total records', invoices.length);
		var count = 0;
		// return;
		var context = nlapiGetContext();
		// ;
		for (var i = dataIn.startpoint; i < dataIn.endpoint; i++) {
			// yieldScript(context);
			try {
				var invoice = nlapiLoadRecord('invoice', invoices[i], {
					recordmode : 'dynamic'
				});

				// invoice.setFieldText('location', "US");

				nlapiSubmitRecord(invoice);
				nlapiLogExecution('debug', 'record id', invoices[i]);
				count++;
			} catch (er) {
				nlapiLogExecution('error', 'failed record id : ' + invoices[i],
						er);
			}
		}
		nlapiLogExecution('debug', 'updated records', count);
	} catch (err) {
		nlapiLogExecution('error', 'postRESTlet', err);
	}

	return "done";
}
