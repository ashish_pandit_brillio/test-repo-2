// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
/*
   	Script SUT_T&M_Dashboard_ProjectList.js
	Author: Ashish Pandit	
	Date: 08/05/2018
    Description: Suitelet to show dashboard for T&M projects   


	Script Modification Log:

	-- Date --			-- Modified By --				--Requested By--				-- Description --
	07-03-2019			  Ashish Pandit						Deepak MS				Admin access given to Gopal Agraval and Hitesh Kapoor
	
	Below is a summary of the process controls enforced by this script file.  The control logic is described
	more fully, below, in the appropriate function headers and code blocks.

   
     SUB-FUNCTIONS
		- The following sub-functions are called by the above core functions in order to maintain code
            modularization:

		scheduled_SendEmailForApproval()



*/
}
// END SCRIPT DESCRIPTION BLOCK  ====================================

function suiteletFunction_TndM_Dashboard(request,response)
{
	try
	{
		var o_context = nlapiGetContext();
		var i_user_logegdIn_id = o_context.getUser();
		var a_project_list = new Array();
		var a_project_search_results = '';
		
      if(i_user_logegdIn_id ==278398 ) //Added user praveena to check the functionality 
	    //Replaced Praveena's employee internal ID on 23rd April - Sitaram
		i_user_logegdIn_id =7905;
		
		if(i_user_logegdIn_id == 242409) //Added user Sitaram  to check the functionality 
		   i_user_logegdIn_id =7905;
      
		// design form which will be displayed to user
		var o_form_obj = nlapiCreateForm("T&M Project Details Page");
		
		var a_project_filter =[];var a_currentuserFilter='';
		if(o_context.getDeploymentId()=='customdeploy_sut_ado_tm_projectdetails'){
			a_currentuserFilter=['customer.custentity_account_delivery_owner', 'anyof', i_user_logegdIn_id];
		}
		else if(o_context.getDeploymentId()=='customdeploy_sut_pm_tm_projectdetails')
		{
			a_currentuserFilter=['custentity_projectmanager', 'anyof', i_user_logegdIn_id];
		}
		else if(o_context.getDeploymentId()=='customdeploy_sut_dm_tm_projectdetails')
		{
			a_currentuserFilter=['custentity_deliverymanager', 'anyof', i_user_logegdIn_id];
		}
		else{
			a_currentuserFilter=[['custentity_projectmanager', 'anyof', i_user_logegdIn_id], 'or',
								['custentity_deliverymanager', 'anyof', i_user_logegdIn_id], 'or',
								['custentity_clientpartner', 'anyof', i_user_logegdIn_id], 'or',
								['custentity_practice.custrecord_practicehead','anyof', i_user_logegdIn_id],'or',
								['customer.custentity_clientpartner', 'anyof', i_user_logegdIn_id]]
			
		}
		
		a_project_filter=[a_currentuserFilter, 'and',
								['status', 'noneof', 1], 'and',
								['jobtype', 'anyof', 2], 'and',
								['jobbillingtype', 'anyof', 'TM']];
								
								
		var a_columns_proj_srch = new Array();
		a_columns_proj_srch[0] = new nlobjSearchColumn('custentity_projectvalue');
		a_columns_proj_srch[1] = new nlobjSearchColumn('customer');
		a_columns_proj_srch[2] = new nlobjSearchColumn('custentity_fp_rev_rec_type');
		a_columns_proj_srch[3] = new nlobjSearchColumn('custentity_ytd_rev_recognized');
		
		if(parseInt(i_user_logegdIn_id) == parseInt(41571)|| parseInt(i_user_logegdIn_id) == parseInt(7905)|| parseInt(i_user_logegdIn_id) == parseInt(258172) ||  
		    parseInt(i_user_logegdIn_id) == parseInt(7981) || parseInt(i_user_logegdIn_id) == parseInt(141228)|| parseInt(i_user_logegdIn_id) == parseInt(133724) || 
			parseInt(i_user_logegdIn_id) == parseInt(4903) || parseInt(i_user_logegdIn_id) == parseInt(9701) || parseInt(i_user_logegdIn_id) == parseInt(9399) || 
			parseInt(i_user_logegdIn_id) == parseInt(262780) ||  parseInt(i_user_logegdIn_id) == parseInt(294103)  ||  parseInt(i_user_logegdIn_id) == parseInt(287311)
          )
		{
			var a_project_filter = [['status', 'noneof', 1], 'and',
								['jobtype', 'anyof', 2], 'and',
								['jobbillingtype', 'anyof', 'TM'],'and',
								['resourceallocation.custeventrbillable','is','T'], 'and',  
								['resourceallocation.custeventbenddate','onorafter','startofthismonth']];
								
			a_project_search_results = searchRecord('job', null, a_project_filter, a_columns_proj_srch);
		}
		else
		{
			a_project_search_results = searchRecord('job', null, a_project_filter, a_columns_proj_srch);
		}
      
      //======================= Code added to provide access to Shweta for 3 projects =============//
		if(i_user_logegdIn_id == 8231)//Shwetha B   SB :204940
		{
			a_project_search_results = nlapiSearchRecord("job",null,
				[
				   ["status","noneof","1"], 
				   "AND", 
				   ["type","anyof","2"], 
				   "AND", 
				   ["jobbillingtype","anyof","TM"], 
				   "AND", 
				   ["internalid","anyof","279930","277046","23694"]  // VEDS04632, VEDS04602, VEDS01780
				], 
				[
				   new nlobjSearchColumn("customer"), 
				   new nlobjSearchColumn("custentity_projectvalue"), 
				   new nlobjSearchColumn("custentity_fp_rev_rec_type"), 
				   new nlobjSearchColumn("custentity_ytd_rev_recognized")
				]
				);
		}
		//=============================================================================================//
		nlapiLogExecution('debug','a_project_search_results Length',a_project_search_results.length);  
		if(a_project_search_results)
		{
			var a_JSON = {};
			var s_create_update_mode = '';
			var linkUrl	= nlapiResolveURL('SUITELET', 'customscript_sut_displayprojectdetails', 'customdeploy_sut_displayprojectdetails'); 
			
			for(var i_pro_index = 0; i_pro_index < a_project_search_results.length; i_pro_index++)
			{
				var i_project_internal_id = a_project_search_results[i_pro_index].getId();
				var f_project_existing_revenue = 0;
				var i_existing_revenue_share_rcrd_id = 0;
				var b_is_proj_value_chngd_flag = 'F';
				var s_existing_revenue_share_status = '';
				var i_project_sow_value = a_project_search_results[i_pro_index].getValue('custentity_projectvalue');
				var s_revenue_share_status = 'No Active Allocation';
				i_project_sow_value = parseFloat(i_project_sow_value).toFixed(2);
				
				{
					s_revenue_share_status = 'Active';
						a_JSON = {
							project_cutomer: a_project_search_results[i_pro_index].getValue('customer'),
							project_id: a_project_search_results[i_pro_index].getId(),
							project_value: i_project_sow_value,
							revenue_share_status: s_revenue_share_status,
							//project_existing_value: f_project_existing_revenue,
							project_rev_rec_type: a_project_search_results[i_pro_index].getValue('custentity_fp_rev_rec_type'),
							suitelet_url: linkUrl + '&proj_id=' + a_project_search_results[i_pro_index].getId()+ '&deployment_id=' + o_context.getDeploymentId()
						}
				}
				
				a_project_list.push(a_JSON);
			}
			
			//create sublist for project
			var f_form_sublist = o_form_obj.addSubList('project_sublist','list','Project List');
			f_form_sublist.addField('project_cutomer','select','Customer','customer').setDisplayType('inline');
			f_form_sublist.addField('project_id','select','Project','job').setDisplayType('inline');
			f_form_sublist.addField('project_value','currency','Project Value');
			f_form_sublist.addField('revenue_share_status','text','Project Setup Status');
			//f_form_sublist.addField('project_existing_value','currency','Captured Revenue Share');
			//f_form_sublist.addField('project_rev_rec_type','select','Rev Rec Type','customlist_fp_rev_rec_project_type').setDisplayType('inline');
			f_form_sublist.addField('suitelet_url', 'url','').setLinkText('Update');
			f_form_sublist.setLineItemValues(a_project_list);
		
		}
		else
		{
			var s_result_not_found = "<table>";
			s_result_not_found += "<tr><td>";
			s_result_not_found += "NO RESULTS FOUND";
			s_result_not_found += "</td></tr>";
			s_result_not_found += "</table>";
			
			var f_no_rslt_found = o_form_obj.addField('rcrd_nt_found', 'inlinehtml', 'No Record Found');
			f_no_rslt_found.setDefaultValue(s_result_not_found);
		}
		
		response.writePage(o_form_obj);
	}
	catch(err)
	{
		nlapiLogExecution('ERROR','function suiteletFunction_FP_RevRec_Dashboard','ERROR MESSAGE :- '+err);
	}
}

function searchRecord(recordType, savedSearch, arrFilters, arrColumns,
        filterExpression)
{

	try {
		var search = null;

		// if a saved search is provided, load it and add the filters and
		// columns
		if (isNotEmpty(savedSearch)) {
			search = nlapiLoadSearch(recordType, savedSearch);

			if (isArrayNotEmpty(arrFilters)) {
				search.addFilters(arrFilters);
			}

			if (isArrayNotEmpty(arrColumns)) {
				search.addColumns(arrColumns);
			}

			if (isArrayNotEmpty(filterExpression)) {
				search.setFilterExpression(filterExpression);
			}
		}
		// create a new search
		else {
			search = nlapiCreateSearch(recordType, arrFilters, arrColumns);
		}

		// run search
		var resultSet = search.runSearch();

		// iterate through the search and get all data 1000 at a time
		var searchResultCount = 0;
		var resultSlice = null;
		var searchResult = [];

		do {
			resultSlice = resultSet.getResults(searchResultCount,
			        searchResultCount + 1000);

			if (resultSlice) {

				resultSlice.forEach(function(result) {

					searchResult.push(result);
					searchResultCount++;
				});
			}
		} while (isArrayNotEmpty(resultSlice) && resultSlice.length >= 1000);

		return searchResult;
	} catch (err) {
		nlapiLogExecution('ERROR', 'searchRecord', err);
		throw err;
	}
}

function isEmpty(value) {

	return value == null || value == "" || typeof (value) == undefined;
}

function isNotEmpty(value) {

	return !isEmpty(value);
}

function isArrayEmpty(argArray) {

	return !isArrayNotEmpty(argArray);
}

function isArrayNotEmpty(argArray) {

	return (isNotEmpty(argArray) && argArray.length > 0);
}