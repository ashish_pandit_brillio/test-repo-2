// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
/*
   	Script Name : UES_CreateLocationString.js
	Author      : Ashish Pandit
	Date        : 24 April 2018
    Description : User Event to create Location name string  


	Script Modification Log:

	-- Date --			-- Modified By --				--Requested By--				-- Description --


Below is a summary of the process controls enforced by this script file.  The control logic is described
more fully, below, in the appropriate function headers and code blocks.


     BEFORE LOAD
		- beforeLoadRecord(type)



     BEFORE SUBMIT
		- beforeSubmitRecord(type)


     AFTER SUBMIT
		- afterSubmitRecord(type)



     SUB-FUNCTIONS
		- The following sub-functions are called by the above core functions in order to maintain code
            modularization:

               - NOT USED

*/
}
// END SCRIPT DESCRIPTION BLOCK  ====================================



// BEGIN GLOBAL VARIABLE BLOCK  =====================================
{
	//  Initialize any Global Variables, in particular, debugging variables...

}
// END GLOBAL VARIABLE BLOCK  =======================================



// BEGIN BEFORE LOAD ==================================================

function beforeSubmitCreateLocationString(type)
{
	if(type=='edit' || type == 'create')
	{
		
		var s_country = nlapiGetFieldValue('custrecord_taleo_location_country');
		var s_state = nlapiGetFieldValue('custrecord_taleo_location_state');
		var s_city = nlapiGetFieldValue('custrecord_taleo_location_city');
		nlapiLogExecution('Debug','s_country ',s_country);
		if(_logValidation(s_country) && _logValidation(s_state) && _logValidation(s_city))
		{
			var s_location  =  s_country +' : '+ s_state +' : '+ s_city;
			nlapiSetFieldValue('name',s_location);
			return true;
		}
		else
		{
			throw "Please select All fields ";
			return false;
		}
	}
}

// END BEFORE LOAD ====================================================


function _logValidation(value) 
{
 if(value != null && value!= 'null' && value != '' && value != undefined && value != undefined && value != 'undefined' && value.toString() != 'NaN' && value != NaN) 
 {
  return true;
 }
 else 
 { 
  return false;
 }
}
// END FUNCTION =====================================================
