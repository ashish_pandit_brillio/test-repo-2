/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       17 Nov 2017     bidhi.raj
 *
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType
 * 
 * @param {String} type Operation types: create, edit, delete, xedit,
 *                      approve, cancel, reject (SO, ER, Time Bill, PO & RMA only)
 *                      pack, ship (IF only)
 *                      dropship, specialorder, orderitems (PO only) 
 *                      paybills (vendor payments)
 * @returns {Void}
 */
function userEventAfterSubmit(type){
	if (type == 'create') {
		var financeMngrs = nlapiGetFieldValues('custbody_sow_approver');
		var status = nlapiGetFieldValue('status');
		var laodRec = nlapiLoadRecord(nlapiGetRecordType(),nlapiGetRecordId());
		var soNum = laodRec.getFieldValue('tranid');
      var customer = laodRec.getFieldText('entity');
      var currency = laodRec.getFieldText('currency');
      var amount = laodRec.getFieldValue('total');
      var project = laodRec.getFieldText('job');
	    var projectID = laodRec.getFieldValue('custbody_sow_project_id_');
		
		if(projectID){
		var projectVal = projectID;
		}
		else{
		var projectVal = project;
		}
      var opp_id = laodRec.getFieldValue('custbody_opp_id_sfdc');
      if(!amount){
        amount = 0.00;
      }
      if(!projectVal){
    	  projectVal = '';
        }
      if(!opp_id){
    	  opp_id = '';
        }
		var mailarr = [];
		var htmltext = '';

		if (status == 'Pending Approval' || status == null) {
			var colum = new Array();
			colum[0] = new nlobjSearchColumn('email');
			var filter = new Array();
			filter[0]	=	new nlobjSearchFilter('internalid', null, 'anyof', financeMngrs);
			var employee = nlapiSearchRecord('employee', null, filter,colum);
			
			for ( var i in employee) {
				var mail = employee[i].getValue('email');
				mailarr.push(mail);
			}
			htmltext += '<table border="0" width="100%"><tr>';
			htmltext += '<td colspan="4" valign="top">';
			htmltext += '<p>Hi,</p>';
            htmltext += '<p> </p>';
			htmltext += '<p>Please find Sow Details below<p>';
			htmltext += '</br>';
			htmltext += '<p>Sow Num: ' + soNum + '</p>';
            htmltext += '<p>Currency: ' + currency + '</p>';
            htmltext += '<p>Customer: ' + customer + '</p>';
            htmltext += '<p>Amount: ' + amount + '</p>';
            htmltext += '<p>Project: ' + projectVal + '</p>';
            htmltext += '<p>Opportunity Id: ' + opp_id + '</p>';
            htmltext += '<p></p>';
            htmltext += '<p></p>';
            htmltext += '<p></p>';
            htmltext += '</br>';
			htmltext += '<p>Thanks & Regards,</p>';
			htmltext += '<p>Information Systems</p>';

			nlapiSendEmail(442, mailarr, 'SOW Pending Approval-'+opp_id, htmltext,null, null, null, null, true, null, null);
		}

	}
	
}