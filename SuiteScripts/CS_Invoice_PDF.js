/**
 * Module Description
 * 
 * Version Date Author Remarks 1.00 21 Aug 2015 nitish.mishra
 * 
 */

function open_BLLC_TM_Discount() {
	var url = nlapiResolveURL('SUITELET',
			'customscript_sut_pdf_inv_customer_disc',
			'customdeploy_sut_pdf_inv');
	var recordId = nlapiGetRecordId();
	window.open(url + "&invoice=" + recordId);
}
