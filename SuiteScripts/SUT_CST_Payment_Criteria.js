// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
/*
   	Script Name: CST Tax Payment Criteria  
	Author:		 Supriya Jadhav
	Company:	 Aashna Cloudtech Pvt Ltd.
	Date:		 
	Version: 	 
	Description: Create criteria form for VAT payment List.


	Script Modification Log:

	-- Date --			-- Modified By --				--Requested By--				-- Description --
   22 sep 2014         Supriya                         Kalpana                       Normal Account Validation     


Below is a summary of the process controls enforced by this script file.  The control logic is described
more fully, below, in the appropriate function headers and code blocks.


     SUITELET
		- suiteletFunction(request, response)
          createCriteriaForm(request, response)


     SUB-FUNCTIONS
		- The following sub-functions are called by the above core functions in order to maintain code
            modularization:

               - NOT USED

*/
}
// END SCRIPT DESCRIPTION BLOCK  ====================================

// BEGIN SUITELET ==================================================

function createCriteriaForm(request, response)
{


  
    //====FOR GET REQUEST ==========

	if(request.getMethod() == 'GET')
	{
		 // ==== CREATE FORM====
		 var form = nlapiCreateForm("CST Tax Payment Criteria Form");

	     //=====GET CURRENT SYSTEM DATE ====
		 var sysdate=new Date
		 var date1=nlapiDateToString(sysdate)

	     //====GET FROM DATE ===
		 var fromdate = form.addField('fromdate', 'date', ' From Date');
		 fromdate.setMandatory(true);
		 fromdate.setDefaultValue(date1)

		 //====GET TODATE =====
		 var todate = form.addField('todate', 'date', ' To Date');
		 todate.setMandatory(true);
		 todate.setDefaultValue(date1)

		 //====ADD SUBMIT BUTTON ON FORM ===
		 form.addSubmitButton('Search');
		 response.writePage(form);
	}// END if(request.getMethod() == 'GET')
	else if(request.getMethod() == 'POST')
	{
         //====FOR POST REQUEST ==========
		 
		var params = new Array();
		params['fromdate'] = request.getParameter('fromdate');
		params['todate']=request.getParameter('todate')
		//=====TO REDIRECT URL ===========
		nlapiSetRedirectURL('SUITELET','customscript_sut_cst_tax_paymentlist', '1', false, params);

	}// END else if(request.getMethod() == 'POST')
	

}

// END SUITELET ====================================================


