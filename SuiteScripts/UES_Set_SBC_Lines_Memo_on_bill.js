//https://system.na1.netsuite.com/app/common/scripting/script.nl?id=940
// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
	/*
			  Script Name: UES_Set_SBC_Lines_Memo_on_bill.js
		Author: Jayesh V Dinde
		Company: Aashna CLoudtech PVT LTD.
		Date: 23 June 2016
		Description: This script will update SBC lines MEMO field as per brillio standard memo on vendor bill.
					 These are populated through AIT script. Need for this script was, we can't edit AIT script hence new script created.
	
	
		Script Modification Log:
	
		-- Date --			-- Modified By --				--Requested By--				-- Description --
	
	
	
	Below is a summary of the process controls enforced by this script file.  The control logic is described
	more fully, below, in the appropriate function headers and code blocks.
	
	
		 BEFORE LOAD
			- beforeLoadRecord(type)
	
	
	
		 BEFORE SUBMIT
			- beforeSubmitRecord(type)
	
	
		 AFTER SUBMIT
			- afterSubmit_UpdateMemo(type)
	
	
	
		 SUB-FUNCTIONS
			- The following sub-functions are called by the above core functions in order to maintain code
				modularization:
	
				   - NOT USED
	
	*/
}
// END SCRIPT DESCRIPTION BLOCK  ====================================



// BEGIN GLOBAL VARIABLE BLOCK  =====================================
{
	//  Initialize any Global Variables, in particular, debugging variables...




}
// END GLOBAL VARIABLE BLOCK  =======================================





// BEGIN BEFORE LOAD ==================================================

function beforeLoadRecord(type) {

	/*  On before load:

		  - EXPLAIN THE PURPOSE OF THIS FUNCTION

		-


		FIELDS USED:

		  --Field Name--				--ID--


	*/



	//  LOCAL VARIABLES

	//  BEFORE LOAD CODE BODY


	return true;



}

// END BEFORE LOAD ====================================================


// BEGIN BEFORE SUBMIT ================================================

function beforeSubmitRecord(type) {
	/*  On before submit:

		  - PURPOSE
		-

		  FIELDS USED:

		  --Field Name--				--ID--

	*/

	//  LOCAL VARIABLES

	try {


		var o_context = nlapiGetContext();
		if (o_context.getExecutionContext() == 'scheduled') {
			return true;
		}

		if (type == 'create' || type == 'edit') {
			var recID = nlapiGetRecordId();

			//Validation for expense lines
			var expense_lines = nlapiGetLineItemCount('expense');

			if (expense_lines > 25)
				return;

			for (var index = 1; index <= expense_lines; index++) {
				var i_proj_id = '';
				var i_employee_id = '';
				var i_practice = '';
				i_practice = nlapiGetLineItemValue('expense', 'department', index);
				i_proj_id = nlapiGetLineItemValue('expense', 'custcol_project_entity_id', index);
				//i_proj_id = i_proj_id.trim();
				if (_logValidation(i_proj_id)) {
					var filters_search_proj = new Array();
					filters_search_proj[0] = new nlobjSearchFilter('entityid', null, 'contains', i_proj_id);
					//filters_search_proj[1] = new nlobjSearchFilter('status',null,'anyof',['2','4']);
					var column_search_proj = new Array();
					column_search_proj[0] = new nlobjSearchColumn('internalid');

					var search_proj_results = nlapiSearchRecord('job', null, filters_search_proj, column_search_proj);
					if (_logValidation(search_proj_results)) {
						//ProjectID is valid
					}
					else {
						throw 'Invalid Project ID at expense line - ' + index;
						return false;
					}
				}
				//Employee Validation
				i_employee_id = nlapiGetLineItemValue('expense', 'custcol_employee_entity_id', index);
				//if(i_employee_id)
				//	i_employee_id = i_employee_id.trim();
				if (_logValidation(i_employee_id)) {
					var filters_emp = new Array();
					filters_emp[0] = new nlobjSearchFilter('entityid', null, 'contains', i_employee_id);
					filters_emp[1] = new nlobjSearchFilter('isinactive', null, 'is', 'F');
					var column_emp = new Array();
					column_emp[0] = new nlobjSearchColumn('internalid');


					var a_results_emp = nlapiSearchRecord('employee', null, filters_emp, column_emp);
					nlapiLogExecution('audit', 'a_results_emp:-- ', a_results_emp);
					if (_logValidation(a_results_emp)) {
					}
					else {
						throw 'Invalid Employee ID at expense line - ' + index;
						return false;
					}
				}
				//Practice Validation

				if (_logValidation(i_practice)) {
					var filters_practice = new Array();
					filters_practice[0] = new nlobjSearchFilter('internalid', null, 'is', i_practice);
					filters_practice[1] = new nlobjSearchFilter('isinactive', null, 'is', 'F');
					var column_practice = new Array();
					column_practice[0] = new nlobjSearchColumn('internalid');


					var a_results_emp = nlapiSearchRecord('department', null, filters_practice, column_practice);
					nlapiLogExecution('audit', 'a_results_emp:-- ', a_results_emp);
					if (_logValidation(a_results_emp)) {
					}
					else {
						throw 'Invalid Practice at expense line - ' + index;
						return false;
					}
				}
				//var is_practice_active_e = nlapiLookupField('department',parseInt(emp_practice),['isinactive']);
				//var isinactive_Practice_e = is_practice_active_e.isinactive;
				//nlapiLogExecution('debug','isinactive_Practice_e',isinactive_Practice_e);


			}

			//Validation for item lines
			var item_lines = nlapiGetLineItemCount('item');

			for (var n = 1; n <= item_lines; n++) {
				var i_proj_id_item = '';
				var i_employee_id_item = '';
				var i_practice_item = '';
				i_practice_item = nlapiGetLineItemValue('item', 'department', n);
				i_proj_id_item = nlapiGetLineItemValue('item', 'custcol_project_entity_id', n);
				//i_proj_id_item = i_proj_id_item.trim();
				if (_logValidation(i_proj_id_item)) {
					var filters_search_proj = new Array();
					filters_search_proj[0] = new nlobjSearchFilter('entityid', null, 'contains', i_proj_id_item);
					//	filters_search_proj[1] = new nlobjSearchFilter('status',null,'anyof',['2','4']);
					var column_search_proj = new Array();
					column_search_proj[0] = new nlobjSearchColumn('internalid');

					var search_proj_results = nlapiSearchRecord('job', null, filters_search_proj, column_search_proj);
					if (_logValidation(search_proj_results)) {
						//ProjectID is valid
					}
					else {
						throw 'Invalid Project ID at item line - ' + n;
						return false;
					}
				}
				//Employee Validation
				i_employee_id_item = nlapiGetLineItemValue('item', 'custcol_employee_entity_id', n);
				//i_employee_id_item = i_employee_id_item.trim();
				if (_logValidation(i_employee_id_item)) {
					var filters_emp = new Array();
					filters_emp[0] = new nlobjSearchFilter('entityid', null, 'contains', i_employee_id_item);
					filters_emp[1] = new nlobjSearchFilter('isinactive', null, 'is', 'F');
					var column_emp = new Array();
					column_emp[0] = new nlobjSearchColumn('internalid');


					var a_results_emp = nlapiSearchRecord('employee', null, filters_emp, column_emp);
					nlapiLogExecution('audit', 'a_results_emp:-- ', a_results_emp);
					if (_logValidation(a_results_emp)) {
					}
					else {
						throw 'Invalid Employee ID at item line - ' + n;
						return false;
					}
				}

				//Practice Validation
				if (_logValidation(i_practice_item)) {
					var filters_practice = new Array();
					filters_practice[0] = new nlobjSearchFilter('internalid', null, 'is', i_practice_item);
					filters_practice[1] = new nlobjSearchFilter('isinactive', null, 'is', 'F');
					var column_practice = new Array();
					column_practice[0] = new nlobjSearchColumn('internalid');


					var a_results_emp = nlapiSearchRecord('department', null, filters_practice, column_practice);
					nlapiLogExecution('audit', 'a_results_emp:-- ', a_results_emp);
					if (_logValidation(a_results_emp)) {
					}
					else {
						throw 'Invalid Practice at item line - ' + index;
						return false;
					}
				}

			}
		}

	}
	catch (e) {
		nlapiLogExecution('debug', 'Before Submit Error', e);
		throw e;
	}


	//  BEFORE SUBMIT CODE BODY


	return true;
}

// END BEFORE SUBMIT ==================================================


// BEGIN AFTER SUBMIT =============================================

function afterSubmit_UpdateMemo(type) {
	/*  On after submit:

		  - PURPOSE
		-

	FIELDS USED:

		  --Field Name--				--ID--

	*/

	//  LOCAL VARIABLES

	var submit_record_flag = 0;

	//  AFTER SUBMIT CODE BODY

	try {
		var o_context = nlapiGetContext();
		/*----------Added by Koushalya 28/12/2021---------*/
		var filters_customrecord_revenue_location_subsidiarySearch = ["custrecord_offsite_onsite", "anyof", "2"];
		var obj_rev_loc_subSrch = Search_revenue_Location_subsdidary(filters_customrecord_revenue_location_subsidiarySearch);
		var obj_offsite_onsite = obj_rev_loc_subSrch["Onsite/Offsite"];
		nlapiLogExecution('audit', 'obj_offsite_onsite ', JSON.stringify(obj_offsite_onsite));
		/*-------------------------------------------------*/
		if (o_context.getExecutionContext() == 'scheduled') {
			return true;
		}

		if (type == 'create' || type == 'edit') {
			var vendor_bill_rcrd = nlapiLoadRecord(nlapiGetRecordType(), nlapiGetRecordId());

			try {
				var transactionNum = vendor_bill_rcrd.getFieldValue('tranid');
				var transaction_internal = nlapiGetRecordId();
				var is_subtier_bill = vendor_bill_rcrd.getFieldValue('custbody_is_subtier_bill');
				nlapiLogExecution('debug', 'is_subtier_bill:-- ' + is_subtier_bill, nlapiGetRecordId());
				if (is_subtier_bill == 'T') {
					var expense_line_count = vendor_bill_rcrd.getLineItemCount('expense');
					nlapiLogExecution('debug', 'expense_line_count1', expense_line_count);
					for (var i = 1; i <= expense_line_count; i++) {
						nlapiLogExecution('debug', 'reached here', i);
						var is_swach_cess_reverse_true = vendor_bill_rcrd.getLineItemValue('expense', 'custcol_ait_swachh_cess_reverse', i);
						if (is_swach_cess_reverse_true == 'T') {
							var memo_body_level = nlapiGetFieldValue('memo');
							vendor_bill_rcrd.selectLineItem('expense', i);
							vendor_bill_rcrd.setCurrentLineItemValue('expense', 'memo', 'SBC-' + memo_body_level);
							vendor_bill_rcrd.commitLineItem('expense');
							submit_record_flag = 1;
						}
					}
				}
				else {
					var item_line_count = vendor_bill_rcrd.getLineItemCount('item');
					for (var j = 1; j <= item_line_count; j++) {
						var is_swach_cess_reverse_true = vendor_bill_rcrd.getLineItemValue('item', 'custcol_ait_swachh_cess_reverse', j);

						if (is_swach_cess_reverse_true == 'T') {
							var vendor_attached = nlapiGetFieldValue('entity');
							var vendor_name = nlapiLookupField('vendor', vendor_attached, 'altname');
							vendor_bill_rcrd.selectLineItem('item', j);
							vendor_bill_rcrd.setCurrentLineItemValue('item', 'memo', 'SBC Charges-' + vendor_name);


							vendor_bill_rcrd.commitLineItem('item');

							submit_record_flag = 1;

						}
					}

					var expense_line_count = vendor_bill_rcrd.getLineItemCount('expense');
					if (expense_line_count < 25) {
						for (var q = 1; q <= expense_line_count; q++) {
							var is_swach_cess_reverse_true = vendor_bill_rcrd.getLineItemValue('expense', 'custcol_ait_swachh_cess_reverse', q);

							if (is_swach_cess_reverse_true == 'T') {
								var vendor_attached = nlapiGetFieldValue('entity');
								var vendor_name = nlapiLookupField('vendor', vendor_attached, 'altname');
								vendor_bill_rcrd.selectLineItem('expense', q);
								vendor_bill_rcrd.setCurrentLineItemValue('expense', 'memo', 'SBC Charges-' + vendor_name);
								vendor_bill_rcrd.commitLineItem('expense');
								submit_record_flag = 1;
							}
						}
					}
				}

				var expense_line_count = vendor_bill_rcrd.getLineItemCount('expense');
				nlapiLogExecution('debug', 'Expense Line Count2', expense_line_count);
				if (expense_line_count < 25) {
					for (var k = 1; k <= expense_line_count; k++) {
						nlapiLogExecution('debug', 'check1, k: ' + k, "k: " + k);
						var emp_type = '';
						var person_type = '';
						var onsite_offsite = '';
						var s_employee_name_split = '';
						var cust_full_name_with_id = '';
						var emp_fusion_id = '';
						var emp_frst_name = '';
						var emp_middl_name = '';
						var emp_lst_name = '';
						var emp_full_name = '';
						var emp_practice = '';
						var emp_practice_text = '';
						var s_proj_desc_split = '';
						var s_employee_name = '';
						var s_employee_id = '';
						var s_employee_name_id = '';
						var proj_desc = '';
						var project_entity_id = '';
						var s_proj_desc_id = '';
						var existing_practice = '';
						var proj_full_name_with_id = '';
						var employee_with_id = '';
						var existing_territory = '';
						var proj_category_val = '';
						var core_practice = '';
						var misc_practice = '';
						var parent_practice = '';
						var is_swach_cess_reverse_true = vendor_bill_rcrd.getLineItemValue('expense', 'custcol_ait_swachh_cess_reverse', k);
						nlapiLogExecution('debug', 'check2, k: ' + k, "is_swach_cess_reverse_true: " + is_swach_cess_reverse_true);
						if (is_swach_cess_reverse_true != 'T') {
							existing_territory = vendor_bill_rcrd.getLineItemValue('expense', 'custcol_territory', k);
							existing_practice = vendor_bill_rcrd.getLineItemValue('expense', 'department', k);
							s_employee_name = vendor_bill_rcrd.getLineItemValue('expense', 'custcol_employeenamecolumn', k);
							s_employee_id = vendor_bill_rcrd.getLineItemValue('expense', 'custcol_employee_entity_id', k);
							if (_logValidation(s_employee_id)) {
								s_employee_name_split = s_employee_id;
								nlapiLogExecution('audit', 's_employee_name_split expense', s_employee_name_split);
							}
							else
								if (_logValidation(s_employee_name)) {
									s_employee_name_id = s_employee_name.split('-');
									s_employee_name_split = s_employee_name_id[0];
									nlapiLogExecution('audit', 's_employee_name', s_employee_name);
								}
							if (_logValidation(s_employee_name) || _logValidation(s_employee_id)) {
								var filters_emp = new Array();
								filters_emp[0] = new nlobjSearchFilter('entityid', null, 'contains', s_employee_name_split);
								var column_emp = new Array();
								column_emp[0] = new nlobjSearchColumn('custentity_persontype');
								column_emp[1] = new nlobjSearchColumn('employeetype');
								column_emp[2] = new nlobjSearchColumn('subsidiary');
								column_emp[3] = new nlobjSearchColumn('custentity_fusion_empid');
								column_emp[4] = new nlobjSearchColumn('firstname');
								column_emp[5] = new nlobjSearchColumn('middlename');
								column_emp[6] = new nlobjSearchColumn('lastname');
								column_emp[7] = new nlobjSearchColumn('department');
								column_emp[8] = new nlobjSearchColumn('location');
								column_emp[9] = new nlobjSearchColumn('custentity_legal_entity_fusion');
								var a_results_emp = nlapiSearchRecord('employee', null, filters_emp, column_emp);
								nlapiLogExecution('audit', 'a_results_emp:-- ', a_results_emp);
								if (_logValidation(a_results_emp)) {
									var emp_id = a_results_emp[0].getId();
									var emp_type = a_results_emp[0].getText('employeetype');
									nlapiLogExecution('audit', 'emp_type:-- ', emp_type);
									person_type = a_results_emp[0].getText('custentity_persontype');
									emp_fusion_id = a_results_emp[0].getValue('custentity_fusion_empid');
									emp_frst_name = a_results_emp[0].getValue('firstname');
									emp_middl_name = a_results_emp[0].getValue('middlename');
									emp_lst_name = a_results_emp[0].getValue('lastname');
									if (emp_frst_name)
										emp_full_name = emp_frst_name;
									if (emp_middl_name)
										emp_full_name = emp_full_name + ' ' + emp_middl_name;
									if (emp_lst_name)
										emp_full_name = emp_full_name + ' ' + emp_lst_name;
									if (!_logValidation(emp_type)) {
										emp_type = '';
									}
									//var person_type = a_results_emp[0].getText('custentity_persontype');
									nlapiLogExecution('audit', 'person_type:-- ', person_type);
									if (!_logValidation(person_type)) {
										person_type = '';
									}
									if (!_logValidation(emp_fusion_id)) {
										emp_fusion_id = '';
									}
									nlapiLogExecution('audit', 'person_type:-- ', person_type);
									emp_practice = a_results_emp[0].getValue('department');
									emp_practice_text = a_results_emp[0].getText('department');

									if (emp_practice) {
										var is_practice_active_e = nlapiLookupField('department', parseInt(emp_practice), ['isinactive', 'custrecord_is_delivery_practice']);
										var isinactive_Practice_e = is_practice_active_e.isinactive;
										nlapiLogExecution('debug', 'isinactive_Practice_e', isinactive_Practice_e);
										core_practice = is_practice_active_e.custrecord_is_delivery_practice;
										nlapiLogExecution('debug', 'core_practice', core_practice);
									}
									var emp_subsidiary = a_results_emp[0].getValue('subsidiary');
									if (_logValidation(emp_subsidiary)) {

										if (emp_subsidiary == 3 && a_results_emp[0].getValue('custentity_legal_entity_fusion') == 'Brillio Technologies Private Limited UK') {
											onsite_offsite = 'Onsite';
										}
										else {
											onsite_offsite = obj_offsite_onsite[emp_subsidiary] ? 'Offshore' : 'Onsite';
										}
									}
									if (emp_fusion_id)
										employee_with_id = emp_fusion_id + '-' + emp_full_name;
									vendor_bill_rcrd.selectLineItem('expense', k);
									vendor_bill_rcrd.setCurrentLineItemValue('expense', 'custcol_employeenamecolumn', employee_with_id);
									vendor_bill_rcrd.setCurrentLineItemValue('expense', 'custcol_employee_type', emp_type);
									vendor_bill_rcrd.setCurrentLineItemValue('expense', 'custcol_person_type', person_type);
									vendor_bill_rcrd.setCurrentLineItemValue('expense', 'custcol_onsite_offsite', onsite_offsite);
									vendor_bill_rcrd.setCurrentLineItemValue('expense', 'custcol_employee_entity_id', emp_fusion_id);
									vendor_bill_rcrd.setCurrentLineItemValue('expense', 'custcol_emp_name_on_a_click_report', emp_full_name);
									if (existing_practice) {
									}
									else if (isinactive_Practice_e == 'F' && emp_practice) {
										vendor_bill_rcrd.setCurrentLineItemValue('expense', 'department', emp_practice);
									}
									vendor_bill_rcrd.commitLineItem('expense');
									submit_record_flag = 1;
								}
							}
							proj_desc = vendor_bill_rcrd.getLineItemValue('expense', 'custcolprj_name', k);
							var customer_ID = vendor_bill_rcrd.getLineItemValue('expense', 'custcol_customer_entityid', k);
							project_entity_id = vendor_bill_rcrd.getLineItemValue('expense', 'custcol_project_entity_id', k);



							nlapiLogExecution('audit', 'project_entity_id', project_entity_id);
							if (project_entity_id) {
								s_proj_desc_split = project_entity_id;
								nlapiLogExecution('audit', 's_proj_desc_split1', s_proj_desc_split);
							}
							else {
								nlapiLogExecution('audit', 'proj_desc', proj_desc);
								if (proj_desc) {
									s_proj_desc_id = proj_desc.substr(0, 9);
									s_proj_desc_split = s_proj_desc_id;
									nlapiLogExecution('audit', 's_proj_desc_split2', s_proj_desc_split);
								}
							}
							nlapiLogExecution('debug', 'check3, k: ' + k, "s_proj_desc_split: " + s_proj_desc_split);
							if (_logValidation(s_proj_desc_split)) {
								s_proj_desc_split = s_proj_desc_split.trim();
								var filters_search_proj = new Array();
								filters_search_proj[0] = new nlobjSearchFilter('entityid', null, 'contains', s_proj_desc_split);
								var column_search_proj = new Array();
								column_search_proj[0] = new nlobjSearchColumn('jobbillingtype');
								column_search_proj[1] = new nlobjSearchColumn('customer');
								column_search_proj[2] = new nlobjSearchColumn('custentity_region', 'customer');
								column_search_proj[3] = new nlobjSearchColumn('entityid');
								column_search_proj[4] = new nlobjSearchColumn('altname');
								column_search_proj[5] = new nlobjSearchColumn('custentity_project_allocation_category');
								column_search_proj[6] = new nlobjSearchColumn('companyname', 'customer');
								column_search_proj[7] = new nlobjSearchColumn('entityid', 'customer');
								column_search_proj[8] = new nlobjSearchColumn('custentity_vertical');
								column_search_proj[9] = new nlobjSearchColumn('territory', 'customer');
								column_search_proj[10] = new nlobjSearchColumn('custentity_practice');
								column_search_proj[11] = new nlobjSearchColumn('custentity_project_services');
								column_search_proj[12] = new nlobjSearchColumn('custentity_region');
								var search_proj_results = nlapiSearchRecord('job', null, filters_search_proj, column_search_proj);
								nlapiLogExecution('debug', 'check4, k: ' + k, "search_proj_results: " + search_proj_results);
								if (_logValidation(search_proj_results)) {
									var proj_billing_type = search_proj_results[0].getText('jobbillingtype');
									var i_region_id = search_proj_results[0].getText('custentity_region');
									var i_proj_rcrd = nlapiLoadRecord('job', search_proj_results[0].getId());
									var i_proj_name = i_proj_rcrd.getFieldValue('altname');
									var i_proj_entity_id = search_proj_results[0].getValue('entityid');
									var i_proj_category = search_proj_results[0].getText('custentity_project_allocation_category');
									var i_proj_vertical = search_proj_results[0].getValue('custentity_vertical');
									var i_proj_practice = search_proj_results[0].getValue('custentity_practice');
									var i_project_service = search_proj_results[0].getValue('custentity_project_services');
									proj_category_val = search_proj_results[0].getValue('custentity_project_allocation_category');
									var i_cust_name = search_proj_results[0].getValue('companyname', 'customer');
									var i_cust_entity_id = search_proj_results[0].getValue('entityid', 'customer');
									var i_cust_territory = search_proj_results[0].getValue('territory', 'customer');
									if (i_proj_practice) {
										var is_practice_active_p = nlapiLookupField('department', parseInt(i_proj_practice), ['isinactive']);
										var isinactive_Practice_p = is_practice_active_p.isinactive;
										nlapiLogExecution('debug', 'isinactive_Practice_p', isinactive_Practice_p);
										parent_practice = nlapiLookupField('department', parseInt(i_proj_practice), 'custrecord_parent_practice', true);
									}
									if (!_logValidation(proj_billing_type))
										proj_billing_type = '';
									nlapiLogExecution('debug', 'check5, k: ' + k, "s_proj_desc_split: " + s_proj_desc_split);
									vendor_bill_rcrd.selectLineItem('expense', k);
									vendor_bill_rcrd.setCurrentLineItemValue('expense', 'custcol_billing_type', proj_billing_type);
									vendor_bill_rcrd.setCurrentLineItemValue('expense', 'custcol_project_entity_id', s_proj_desc_split);
									vendor_bill_rcrd.setCurrentLineItemValue('expense', 'custcol_parent_executing_practice', parent_practice);
									var proj_cust = search_proj_results[0].getText('customer');
									if (_logValidation(proj_cust)) {
										var cust_entity_id = proj_cust.split(' ');
										cust_entity_id = cust_entity_id[0];
										nlapiLogExecution('debug', 'check6, k: ' + k, "cust_entity_id: " + cust_entity_id);
										vendor_bill_rcrd.setCurrentLineItemValue('expense', 'custcol_customer_entityid', cust_entity_id);
									}
									if (existing_practice || s_employee_name_split) {
									}
									else if (isinactive_Practice_p == 'F' && i_proj_practice) {
										nlapiLogExecution('audit', 'inside set proj prac:- ' + existing_practice);
										vendor_bill_rcrd.setCurrentLineItemValue('expense', 'department', i_proj_practice);
									}
									if (proj_category_val) {
										if ((parseInt(proj_category_val) == parseInt(1)) && (core_practice == 'T')) {
											misc_practice = emp_practice;
											var is_practice_active = nlapiLookupField('department', parseInt(misc_practice), ['isinactive']);
											isinactive_Practice_e = is_practice_active.isinactive;
											misc_practice = emp_practice_text;
										}
										else {
											misc_practice = search_proj_results[0].getValue('custentity_practice');
											var is_practice_active = nlapiLookupField('department', parseInt(misc_practice), ['isinactive']);
											isinactive_Practice_e = is_practice_active.isinactive;
											misc_practice = search_proj_results[0].getText('custentity_practice');
										}
									}
									nlapiLogExecution('audit', 'Pratice', 'misc_practice:' + misc_practice);
									if (misc_practice && isinactive_Practice_e == 'F') {
										vendor_bill_rcrd.setCurrentLineItemValue('expense', 'custcol_mis_practice', misc_practice);
									}
									if (i_proj_entity_id)
										proj_full_name_with_id = i_proj_entity_id + ' ' + i_proj_name;
									if (proj_desc) {
									}
									else {
										vendor_bill_rcrd.setCurrentLineItemValue('expense', 'custcolprj_name', proj_full_name_with_id);
									}
									if (project_entity_id) {
									}
									else {
										vendor_bill_rcrd.setCurrentLineItemValue('expense', 'custcol_project_entity_id', i_proj_entity_id);
									}
									if (existing_territory) {
									}
									else {
										vendor_bill_rcrd.setCurrentLineItemValue('expense', 'custcol_territory', i_cust_territory);
									}
									if (cust_entity_id)
										cust_full_name_with_id = cust_entity_id + ' ' + i_cust_name;
									var cust_region = search_proj_results[0].getValue('custentity_region', 'customer');
									vendor_bill_rcrd.setCurrentLineItemValue('expense', 'custcolcustcol_temp_customer', cust_full_name_with_id);
									vendor_bill_rcrd.setCurrentLineItemValue('expense', 'custcol_region_master_setup', cust_region);
									vendor_bill_rcrd.setCurrentLineItemValue('expense', 'custcol_cust_name_on_a_click_report', i_cust_name);
									vendor_bill_rcrd.setCurrentLineItemValue('expense', 'custcol_proj_name_on_a_click_report', i_proj_name);
									vendor_bill_rcrd.setCurrentLineItemValue('expense', 'custcol_project_region', i_region_id);
									vendor_bill_rcrd.setCurrentLineItemValue('expense', 'custcol_proj_category_on_a_click', i_proj_category);
									vendor_bill_rcrd.setCurrentLineItemValue('expense', 'custcol_transaction_project_services', i_project_service);
									vendor_bill_rcrd.commitLineItem('expense');
									submit_record_flag = 1;

								}
							}
						}
					}
				}
				var item_line_count = vendor_bill_rcrd.getLineItemCount('item');
				for (var d = 1; d <= item_line_count; d++) {
					var emp_type = '';
					var person_type = '';
					var onsite_offsite = '';
					var cust_full_name_with_id = '';

					var emp_fusion_id = '';
					var emp_frst_name = '';
					var emp_middl_name = '';
					var emp_lst_name = '';
					var emp_full_name = '';
					var emp_practice = '';
					var emp_practice_text = '';
					var s_proj_desc_split = '';
					var s_employee_name_split = '';
					var proj_name_selected = '';
					var s_employee_name = '';
					var s_employee_id = '';
					var s_employee_name_id = '';
					var proj_name_selected = '';
					var project_entity_id = '';
					var s_proj_desc_id = '';
					var existing_practice = '';
					var proj_full_name_with_id = '';
					var employee_with_id = '';
					var existing_territory = '';
					var misc_practice = '';
					var proj_category_val = '';
					var core_practice = '';
					var parent_practice = '';
					existing_territory = vendor_bill_rcrd.getLineItemValue('item', 'custcol_territory', d);
					existing_practice = vendor_bill_rcrd.getLineItemValue('item', 'department', d);
					s_employee_name = vendor_bill_rcrd.getLineItemValue('item', 'custcol_employeenamecolumn', d);
					s_employee_id = vendor_bill_rcrd.getLineItemValue('item', 'custcol_employee_entity_id', d);
					if (_logValidation(s_employee_id)) {
						s_employee_name_split = s_employee_id;
						nlapiLogExecution('audit', 's_employee_name_split item', s_employee_name_split);
					}
					else if (_logValidation(s_employee_name)) {
						s_employee_name_id = s_employee_name.split('-');
						s_employee_name_split = s_employee_name_id[0];
						nlapiLogExecution('audit', 's_employee_name', s_employee_name);
					}
					if (_logValidation(s_employee_name) || _logValidation(s_employee_id)) {
						//var s_employee_name_id = emp_name_selected.split('-');
						//var s_employee_name_split = s_employee_name_id[0];

						var filters_emp = new Array();
						filters_emp[0] = new nlobjSearchFilter('entityid', null, 'contains', s_employee_name_split);
						var column_emp = new Array();
						column_emp[0] = new nlobjSearchColumn('custentity_persontype');
						column_emp[1] = new nlobjSearchColumn('employeetype');
						column_emp[2] = new nlobjSearchColumn('subsidiary');
						column_emp[3] = new nlobjSearchColumn('custentity_fusion_empid');
						column_emp[4] = new nlobjSearchColumn('firstname');
						column_emp[5] = new nlobjSearchColumn('middlename');
						column_emp[6] = new nlobjSearchColumn('lastname');
						column_emp[7] = new nlobjSearchColumn('department');
						column_emp[8] = new nlobjSearchColumn('location');
						column_emp[9] = new nlobjSearchColumn('custentity_legal_entity_fusion');
						var a_results_emp = nlapiSearchRecord('employee', null, filters_emp, column_emp);
						nlapiLogExecution('audit', 'a_results_emp:-- ', a_results_emp);
						if (_logValidation(a_results_emp)) {
							var emp_id = a_results_emp[0].getId();
							var emp_type = a_results_emp[0].getText('employeetype');
							nlapiLogExecution('audit', 'emp_type:-- ', emp_type);
							if (!_logValidation(emp_type)) {
								emp_type = '';
							}
							var person_type = a_results_emp[0].getText('custentity_persontype');
							nlapiLogExecution('audit', 'person_type:-- ', person_type);
							if (!_logValidation(person_type)) {
								person_type = '';
							}

							nlapiLogExecution('audit', 'person_type:-- ', person_type);
							var emp_subsidiary = a_results_emp[0].getValue('subsidiary');
							person_type = a_results_emp[0].getText('custentity_persontype');
							emp_fusion_id = a_results_emp[0].getValue('custentity_fusion_empid');
							emp_practice = a_results_emp[0].getValue('department');
							emp_practice_text = a_results_emp[0].getText('department');
							emp_frst_name = a_results_emp[0].getValue('firstname');
							emp_middl_name = a_results_emp[0].getValue('middlename');
							emp_lst_name = a_results_emp[0].getValue('lastname');
							if (!_logValidation(emp_fusion_id)) {
								emp_fusion_id = '';
							}
							nlapiLogExecution('audit', 'emp_fusion_id:-- ', emp_fusion_id);
							if (emp_frst_name)
								emp_full_name = emp_frst_name;

							if (emp_middl_name)
								emp_full_name = emp_full_name + ' ' + emp_middl_name;

							if (emp_lst_name)
								emp_full_name = emp_full_name + ' ' + emp_lst_name;
							if (_logValidation(emp_subsidiary)) {
								if (emp_subsidiary == 3 && a_results_emp[0].getValue('custentity_legal_entity_fusion') == 'Brillio Technologies Private Limited UK') {
									onsite_offsite = 'Onsite';
								}
								else {
									onsite_offsite = obj_offsite_onsite[emp_subsidiary] ? 'Offshore' : 'Onsite';
								}
								/*else if(emp_subsidiary == 3 || emp_subsidiary == 9 || emp_subsidiary == 12)
								{
									onsite_offsite = 'Offsite';
								}else 
								{
									onsite_offsite = 'Onsite';
								}*/
							}
							if (emp_fusion_id)
								employee_with_id = emp_fusion_id + '-' + emp_full_name;

							if (emp_practice) {
								var is_practice_active_e = nlapiLookupField('department', parseInt(emp_practice), ['isinactive', 'custrecord_is_delivery_practice']);
								var isinactive_Practice_e = is_practice_active_e.isinactive;
								nlapiLogExecution('debug', 'isinactive_Practice_e', isinactive_Practice_e);
								core_practice = is_practice_active_e.custrecord_is_delivery_practice;
								nlapiLogExecution('debug', 'core_practice', core_practice);
							}

							vendor_bill_rcrd.selectLineItem('item', d);
							vendor_bill_rcrd.setCurrentLineItemValue('item', 'custcol_employeenamecolumn', employee_with_id);

							vendor_bill_rcrd.setCurrentLineItemValue('item', 'custcol_employee_type', emp_type);
							vendor_bill_rcrd.setCurrentLineItemValue('item', 'custcol_person_type', person_type);
							vendor_bill_rcrd.setCurrentLineItemValue('item', 'custcol_onsite_offsite', onsite_offsite);
							vendor_bill_rcrd.setCurrentLineItemValue('item', 'custcol_employee_entity_id', emp_fusion_id);
							vendor_bill_rcrd.setCurrentLineItemValue('item', 'custcol_emp_name_on_a_click_report', emp_full_name);
							if (existing_practice) {
							}
							else if (isinactive_Practice_e == 'F' && emp_practice) {
								vendor_bill_rcrd.setCurrentLineItemValue('item', 'department', emp_practice);
							}
							vendor_bill_rcrd.commitLineItem('item');

							submit_record_flag = 1;
						}

					}

					proj_name_selected = vendor_bill_rcrd.getLineItemValue('item', 'custcolprj_name', d);
					project_entity_id = vendor_bill_rcrd.getLineItemValue('item', 'custcol_project_entity_id', d);
					if (project_entity_id) {
						s_proj_desc_split = project_entity_id;
						nlapiLogExecution('audit', 's_proj_desc_split', s_proj_desc_split);
					}
					else if (proj_name_selected) {
						s_proj_desc_id = proj_name_selected.substr(0, 9);
						s_proj_desc_split = s_proj_desc_id;
						nlapiLogExecution('audit', 'proj_name_selected', proj_name_selected);
					}
					//if (_logValidation(proj_name_selected))
					//{
					//	proj_full_name = proj_name_selected.toString();
					//	proj_id_array = proj_full_name.split(' ');
					//	proj_id = proj_id_array[0];
					if (_logValidation(s_proj_desc_split)) {
						var filters_search_proj = new Array();
						filters_search_proj[0] = new nlobjSearchFilter('entityid', null, 'is', s_proj_desc_split);
						var column_search_proj = new Array();
						column_search_proj[0] = new nlobjSearchColumn('jobbillingtype');
						column_search_proj[1] = new nlobjSearchColumn('customer');
						column_search_proj[2] = new nlobjSearchColumn('custentity_region', 'customer');
						column_search_proj[3] = new nlobjSearchColumn('entityid');
						column_search_proj[4] = new nlobjSearchColumn('altname');
						column_search_proj[5] = new nlobjSearchColumn('custentity_project_allocation_category');
						column_search_proj[6] = new nlobjSearchColumn('companyname', 'customer');
						column_search_proj[7] = new nlobjSearchColumn('entityid', 'customer');
						column_search_proj[8] = new nlobjSearchColumn('custentity_vertical');
						column_search_proj[9] = new nlobjSearchColumn('territory', 'customer');
						column_search_proj[10] = new nlobjSearchColumn('custentity_practice');
						column_search_proj[11] = new nlobjSearchColumn('custentity_project_services');
						column_search_proj[12] = new nlobjSearchColumn('custentity_region');

						var search_proj_results = nlapiSearchRecord('job', null, filters_search_proj, column_search_proj);
						if (_logValidation(search_proj_results)) {
							var proj_billing_type = search_proj_results[0].getText('jobbillingtype');
							var i_region_id = search_proj_results[0].getText('custentity_region');
							var i_proj_rcrd = nlapiLoadRecord('job', search_proj_results[0].getId());
							var i_proj_name = i_proj_rcrd.getFieldValue('altname');
							var i_proj_entity_id = search_proj_results[0].getValue('entityid');
							var i_proj_category = search_proj_results[0].getText('custentity_project_allocation_category');
							var i_proj_vertical = search_proj_results[0].getValue('custentity_vertical');
							var i_proj_practice = search_proj_results[0].getValue('custentity_practice');
							var i_project_service = search_proj_results[0].getValue('custentity_project_services');
							proj_category_val = search_proj_results[0].getValue('custentity_project_allocation_category');
							var i_cust_name = search_proj_results[0].getValue('companyname', 'customer');
							var i_cust_entity_id = search_proj_results[0].getValue('entityid', 'customer');
							var i_cust_territory = search_proj_results[0].getValue('territory', 'customer');

							if (i_proj_practice) {
								var is_practice_active_p = nlapiLookupField('department', parseInt(i_proj_practice), ['isinactive']);
								var isinactive_Practice_p = is_practice_active_p.isinactive;
								nlapiLogExecution('debug', 'isinactive_Practice_p', isinactive_Practice_p);
								parent_practice = nlapiLookupField('department', parseInt(i_proj_practice), 'custrecord_parent_practice', true);

							}

							if (!_logValidation(proj_billing_type))
								proj_billing_type = '';

							vendor_bill_rcrd.selectLineItem('item', d);
							vendor_bill_rcrd.setCurrentLineItemValue('item', 'custcol_billing_type', proj_billing_type);
							vendor_bill_rcrd.setCurrentLineItemValue('item', 'custcol_project_entity_id', s_proj_desc_split);
							vendor_bill_rcrd.setCurrentLineItemValue('item', 'custcol_parent_executing_practice', parent_practice);
							//
							if (proj_category_val) {
								if ((parseInt(proj_category_val) == parseInt(1)) && (core_practice == 'T')) {
									misc_practice = emp_practice;
									var is_practice_active = nlapiLookupField('department', parseInt(misc_practice), ['isinactive']);
									isinactive_Practice_e = is_practice_active.isinactive;
									misc_practice = emp_practice_text;
								}
								else {
									misc_practice = search_proj_results[0].getValue('custentity_practice');
									var is_practice_active = nlapiLookupField('department', parseInt(misc_practice), ['isinactive']);
									isinactive_Practice_e = is_practice_active.isinactive;
									misc_practice = search_proj_results[0].getText('custentity_practice');
								}
							}
							nlapiLogExecution('audit', 'Pratice', 'misc_practice:' + misc_practice);
							if (misc_practice && isinactive_Practice_e == 'F') {
								vendor_bill_rcrd.setCurrentLineItemValue('item', 'custcol_mis_practice', misc_practice);
							}
							//
							var proj_cust = search_proj_results[0].getText('customer');
							if (_logValidation(proj_cust)) {
								var cust_entity_id = proj_cust.split(' ');
								cust_entity_id = cust_entity_id[0];
								vendor_bill_rcrd.setCurrentLineItemValue('item', 'custcol_customer_entityid', cust_entity_id);
							}
							if (s_employee_name_split || existing_practice) {
							}
							else if (isinactive_Practice_p == 'F' && i_proj_practice) {
								vendor_bill_rcrd.setCurrentLineItemValue('item', 'department', i_proj_practice);
							}
							if (i_proj_entity_id)
								proj_full_name_with_id = i_proj_entity_id + ' ' + i_proj_name;
							if (proj_name_selected) {
							}
							else {
								vendor_bill_rcrd.setCurrentLineItemValue('item', 'custcolprj_name', proj_full_name_with_id);
							}
							if (project_entity_id) {
							}
							else {
								vendor_bill_rcrd.setCurrentLineItemValue('item', 'custcol_project_entity_id', i_proj_entity_id);
							}
							if (existing_territory) {
							}
							else {
								vendor_bill_rcrd.setCurrentLineItemValue('item', 'custcol_territory', i_cust_territory);
							}
							if (cust_entity_id)
								cust_full_name_with_id = cust_entity_id + ' ' + i_cust_name;

							var cust_region = search_proj_results[0].getValue('custentity_region', 'customer');
							vendor_bill_rcrd.setCurrentLineItemValue('item', 'custcolcustcol_temp_customer', cust_full_name_with_id);
							vendor_bill_rcrd.setCurrentLineItemValue('item', 'custcol_region_master_setup', cust_region);
							vendor_bill_rcrd.setCurrentLineItemValue('item', 'custcol_cust_name_on_a_click_report', i_cust_name);
							vendor_bill_rcrd.setCurrentLineItemValue('item', 'custcol_proj_name_on_a_click_report', i_proj_name);
							vendor_bill_rcrd.setCurrentLineItemValue('item', 'custcol_project_region', i_region_id);
							vendor_bill_rcrd.setCurrentLineItemValue('item', 'custcol_proj_category_on_a_click', i_proj_category);
							vendor_bill_rcrd.setCurrentLineItemValue('item', 'custcol_transaction_project_services', i_project_service);

							vendor_bill_rcrd.commitLineItem('item');

							submit_record_flag = 1;
						}
					}
					//}

					var tax_amount_at_line = vendor_bill_rcrd.getLineItemValue('item', 'tax1amt', d);
					if (_logValidation(tax_amount_at_line)) {
						//var tax_decimal = tax_amount_at_line.toString();
						//tax_decimal = tax_amount_at_line.split('.');
						//if (parseFloat(tax_decimal) >= parseFloat(50)) {
						//	tax_amount_at_line = Math.round(tax_amount_at_line);
						vendor_bill_rcrd.selectLineItem('item', d);
						vendor_bill_rcrd.setCurrentLineItemValue('item', 'tax1amt', tax_amount_at_line);
						vendor_bill_rcrd.commitLineItem('item');
						//}
						/*else {
							//tax_amount_at_line = Math.round(tax_amount_at_line);
							vendor_bill_rcrd.selectLineItem('item', d);
							vendor_bill_rcrd.setCurrentLineItemValue('item', 'tax1amt', tax_amount_at_line);
							vendor_bill_rcrd.commitLineItem('item');
						}*/
					}
				}

				if (submit_record_flag == 1) {
					var vendor_bill_submitted_id = nlapiSubmitRecord(vendor_bill_rcrd, true, true);
					nlapiLogExecution('debug', 'submitted bill id:-- ', vendor_bill_submitted_id);
				}
			} catch (err) {
				nlapiLogExecution('debug', 'Error:-- ', err);
				var tailMail = '';
				tailMail += '</table>';
				tailMail += ' </td>';
				tailMail += '</tr>';
				tailMail += '</table>';

				var strVar_excel = err_row_excel + tailMail
				//excel_file_obj = generate_excel(strVar_excel);
				var mailTemplate = "";
				mailTemplate += '<html>';
				mailTemplate += '<body>';
				mailTemplate += "<p> This is to inform that following transactions are having Errors and therefore, system is not able to set the project/MIS mandatory fields</p>";
				mailTemplate += "<p> Please make sure, transaction is updated with proper details else may not able to see the project/customer/employee details at Income Statement level.</p>";
				mailTemplate += "<br/>"
				mailTemplate += "<p><b> Script Id:- " + o_context.getScriptId() + "</b></p>";
				mailTemplate += "<p><b> Script Deployment Id:- " + o_context.getDeploymentId() + "</b></p>";
				mailTemplate += "<br/>"
				mailTemplate += strVar_excel
				mailTemplate += "<br/>"
				mailTemplate += "<p>Regards, <br/> Information Systems</p>";
				mailTemplate += '</body>';
				mailTemplate += '</html>';
				//nlapiSendEmail(442, 'billing@brillio.com', 'Issue with updating the MIS data for the transactions', mailTemplate, 'netsuite.support@brillio.com', null, null, null);

			}
		}//if ends here
	}
	catch (e) {
		nlapiLogExecution('ERROR', 'ERROR MESSAGE:-- ', e);
	}

	return true;

}

// END AFTER SUBMIT ===============================================



// BEGIN FUNCTION ===================================================
{

	function _logValidation(value) {
		if (value != null && value.toString() != null && value != '' && value != undefined && value.toString() != undefined && value != 'undefined' && value.toString() != 'undefined' && value.toString() != 'NaN' && value != NaN) {
			return true;
		}
		else {
			return false;
		}
	}

}
// END FUNCTION =====================================================