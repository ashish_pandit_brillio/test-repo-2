var date = '';
function postRestlet(dataIn)
{
		try
		{

			nlapiLogExecution('debug', 'Request', JSON.stringify(dataIn));
			var requestType = dataIn.RequestType;

			//var requestType = 'GET';
			
			//var requestType = 'POST';
			var response = new Response()
			nlapiLogExecution('DEBUG','Request',requestType);

		
			switch (requestType) {

			case M_Constants.Request.Get:
				
				response.Data = getTransactionDetails();
				response.Status = true;
				break;
				
			case M_Constants.Request.Post:
				
				var postData = dataIn.Data;		
				
				response.Data = postTransactionDetails(postData);
				response.Status = true;
				break;
				
			}
		}
		catch (e)
		{
			nlapiLogExecution('debug','error',e);
			response.Status = false;
			response.Data = e;
		}
		
	return response;	
}

function getTransactionDetails()
{ 
	try
	{


		var transaction_list = [] //Response
	
		var customrecord_ns_sp_migration_dateSearch = nlapiSearchRecord("customrecord_ns_sp_migration_date"		,null,
			[
			], 
			[
				new nlobjSearchColumn("custrecord_month_migrated_date").setSort(true)
			]
			);
		var date = 	customrecord_ns_sp_migration_dateSearch[0].getValue('custrecord_month_migrated_date');
		
		var dateObj = nlapiStringToDate(date);
		
		var stDate = new Date(dateObj.getFullYear(), dateObj.getMonth(), 1);
		var enDate = new Date(dateObj.getFullYear(), dateObj.getMonth() + 1, 0);
		

		stDate = nlapiDateToString(stDate);
		enDate = nlapiDateToString(enDate);
		var transactionSearch = nlapiSearchRecord("transaction",null,
		[
		   
			["refnumber","isnotempty",""], 
			"AND", 
			["formulanumeric: CASE WHEN {linefile.internalid}-{file.internalid}=0 THEN 1 ELSE 0 END","equalto","1"], 
			"AND", 
			["type","anyof","ExpRept"], 
			"AND", 
			["trandate","within",stDate,enDate], 
			"AND", 
			["custcol_file_url","isempty",""], 
			"AND", 
			["status","noneof","ExpRept:E","ExpRept:H","ExpRept:B","ExpRept:A"], 
			"AND", 
			["employee.isinactive","is","F"],
			"AND",
			["department.isinactive","is","F"],
			"AND",
			["internalid","noneof","1302988","1322487","1317034","652472"],
			"AND",
			["file.documentsize","lessthan","10000"]
		],

		[
		   new nlobjSearchColumn("refnumber"), 
		   new nlobjSearchColumn("type"), 
		   new nlobjSearchColumn("transactionnumber"), 
		   new nlobjSearchColumn("folder","file",null), 
		   new nlobjSearchColumn("internalid","lineFile",null), 
		   new nlobjSearchColumn("internalid","file",null), 
		   new nlobjSearchColumn("modified","file",null), 
		   new nlobjSearchColumn("name","file",null), 
		   new nlobjSearchColumn("owner","file",null), 
		   new nlobjSearchColumn("filetype","file",null), 
		   new nlobjSearchColumn("url","file",null),
		   new nlobjSearchColumn("created","file",null), 
		   //new nlobjSearchColumn("entityid","employee",null)
		   new nlobjSearchColumn("email","employee",null)
		]
		);
	
		if(_logValidation(transactionSearch))
		{
			nlapiLogExecution('Debug','Search Result',transactionSearch.length);
          	var counter = transactionSearch.length > 20 ? 20 : transactionSearch.length;
			for(var count = 0; count < counter; count++)
			{

				var file_id = transactionSearch[count].getValue('internalid','file');
				var file = nlapiLoadFile(file_id);
				var file_type = transactionSearch[count].getValue('filetype','file');
				if(file_type != 'MISCBINARY' || file_type != 'miscbinary')
				{
					
						transaction_list.push({
						Record_Type : transactionSearch[count].getValue('type'),
						File_type : transactionSearch[count].getValue('filetype','file'),
						Record_int_id : transactionSearch[count].getId(),
						Transaction_no : transactionSearch[count].getValue('transactionnumber'),
						Refrence_no : transactionSearch[count].getValue('refnumber'),	
						File_id :         file_id,					
						File_name : transactionSearch[count].getValue('name','file'),
						File :            file.getValue(),
						Url :        transactionSearch[count].getValue('url','file'),
						Created_date : transactionSearch[count].getValue('created','file'),
						Created_by : transactionSearch[count].getValue('email','employee'),
						Owner : transactionSearch[count].getValue('owner','file')
					
				})
				}
			
				nlapiLogExecution('Debug','Count',count);
			}
		
			nlapiLogExecution('Debug','JSON Response',JSON.stringify(transaction_list));
			return transaction_list;
		}
		else
		{
			var month = nlapiAddMonths(dateObj , 1);
			
			var newRec = nlapiCreateRecord('customrecord_ns_sp_migration_date',{recordmode: 'dynamic'});
			newRec.setFieldValue('custrecord_month_migrated_date',nlapiDateToString(month));
			nlapiSubmitRecord(newRec);
			
			var customrecord_ns_sp_migration_dateSearch = nlapiSearchRecord("customrecord_ns_sp_migration_date"	,null,
			[
			], 
			[
				new nlobjSearchColumn("custrecord_month_migrated_date").setSort(true)
			]
			);
			var date = 	customrecord_ns_sp_migration_dateSearch[0].getValue('custrecord_month_migrated_date');
		
			var dateObj = nlapiStringToDate(date);
		
			var stDate = new Date(dateObj.getFullYear(), dateObj.getMonth(), 1);
			var enDate = new Date(dateObj.getFullYear(), dateObj.getMonth() + 1, 0);
			
			
			var transactionSearch = nlapiSearchRecord("transaction",null,
[
   ["refnumber","isnotempty",""], 
   "AND", 
   ["formulanumeric: CASE WHEN {linefile.internalid}-{file.internalid}=0 THEN 1 ELSE 0 END","equalto","1"], 
   "AND", 
   ["type","anyof","ExpRept"], 
   "AND", 
   ["department.isinactive","is","F"], 
   "AND", 
   ["trandate","within",stDate,enDate], 
   "AND", 
   ["status","noneof","ExpRept:E","ExpRept:H","ExpRept:B","ExpRept:A"], 
   "AND", 
   ["employee.isinactive","is","F"], 
   "AND", 
   ["custcol_file_url","isempty",""], 
   "AND", 
   ["file.documentsize","lessthan","10000"]
], 
[
   new nlobjSearchColumn("refnumber"), 
   new nlobjSearchColumn("type"), 
   new nlobjSearchColumn("transactionnumber"), 
   new nlobjSearchColumn("folder","file",null), 
   new nlobjSearchColumn("internalid","file",null), 
   new nlobjSearchColumn("modified","file",null), 
   new nlobjSearchColumn("name","file",null), 
   new nlobjSearchColumn("owner","file",null), 
   new nlobjSearchColumn("documentsize","file",null), 
   new nlobjSearchColumn("filetype","file",null), 
   new nlobjSearchColumn("url","file",null), 
   new nlobjSearchColumn("created","file",null), 
   new nlobjSearchColumn("internalid","lineFile",null), 
   new nlobjSearchColumn("entityid","employee",null), 
   new nlobjSearchColumn("documentsize","lineFile",null), 
   new nlobjSearchColumn("postingperiod"), 
   new nlobjSearchColumn("department"), 
   new nlobjSearchColumn("custcol_file_url")
]
);
	
			if(_logValidation(transactionSearch))
			{
				nlapiLogExecution('Debug','Search Result',transactionSearch.length);
				for(var count = 0; count < 10; count++)
				{

					var file_id = transactionSearch[count].getValue('internalid','file');
					var file = nlapiLoadFile(file_id);
					var file_type = transactionSearch[count].getValue('filetype','file');
					if(file_type != 'MISCBINARY' || file_type != 'miscbinary')
					{
						transaction_list.push({
							Record_Type : transactionSearch[count].getValue('type'),
							File_type : transactionSearch[count].getValue('filetype','file'),
							Record_int_id : transactionSearch[count].getId(),
							Transaction_no : transactionSearch[count].getValue('transactionnumber'),
							Refrence_no : transactionSearch[count].getValue('refnumber'),	
							File_id :         file_id,					
							File_name : transactionSearch[count].getValue('name','file'),
							File :            file.getValue(),
							Url :        transactionSearch[count].getValue('url','file'),
							Created_date : transactionSearch[count].getValue('created','file'),
							Created_by : transactionSearch[count].getValue('email','employee'),
							Owner : transactionSearch[count].getValue('owner','file')
					
						})
					}
			
					nlapiLogExecution('Debug','Count',count);
				}
		
				nlapiLogExecution('Debug','JSON Response',JSON.stringify(transaction_list));
				return transaction_list;
			
			}
		}
	}
	catch (e)
	{
		nlapiLogExecution('debug','Error in fetching allocation details',e);
		throw e;
	}
}

function postTransactionDetails(postData)
{
	try 
	{
		nlapiLogExecution('Debug','Post block',postData);
		
		for(var count = 0; count < postData.length; count++)
		{
			var exp_id = postData[count].Transaction_no;
			var Refno = postData[count].Refrence_no;
			var sharepointLink = postData[count].File_sharePointLink;
			
			nlapiLogExecution('Debug','exp_id',exp_id);
			nlapiLogExecution('Debug','Refno',Refno);
			nlapiLogExecution('Debug','sharepointLink',sharepointLink);
			
			//To get expense internalId
			var id = getExpenseInternalID(exp_id); 
			nlapiLogExecution('Debug','id',id);
			var loadExpense = nlapiLoadRecord('expensereport',id);
			
			var linecount = loadExpense.getLineItemCount('expense');
			for(var i = 1; i <= linecount; i++)
			{
				var ref = loadExpense.getLineItemValue('expense', 'refnumber', i);
				if(ref == Refno)
				{
					loadExpense.selectLineItem('expense', i);
					loadExpense.setCurrentLineItemValue('expense', 'custcol_file_url', sharepointLink);
					loadExpense.setCurrentLineItemValue('expense', 'custcol_isupdated', 'T');
					loadExpense.commitLineItem('expense');
				}
			}
			
			nlapiSubmitRecord(loadExpense);
		}
		
	}
	catch (err)
	{
		nlapiLogExecution('Debug','Error',err);
		throw err;
	}
}


function getExpenseInternalID(expense_id) {
    try {
        var id = '';
        var filters = Array();
        filters.push(new nlobjSearchFilter('tranid', null, 'startswith', expense_id));

        var cols = Array();
        cols.push(new nlobjSearchColumn('internalid'));

        var searchObj = nlapiSearchRecord('expensereport', null, filters, cols);
        if (searchObj)
            id = searchObj[0].getValue('internalid');

        return id;
    } catch (e) {
        nlapiLogExecution('DEBUG', 'Erron in searching expense', e);
        throw e.details;
    }
}

function _logValidation(value) {
    if (value != null && value.toString() != null && value != '' && value != undefined && value.toString() != undefined && value != 'undefined' && value.toString() != 'undefined' && value.toString() != 'NaN' && value != NaN) {
        return true;
    } else {
        return false;
    }
}