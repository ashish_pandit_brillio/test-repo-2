/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       02 Jul 2015     Nitish Mishra
 *
 */

/**
 * @param {nlobjRequest}
 *            request Request object
 * @param {nlobjResponse}
 *            response Response object
 * @returns {Void} Any output is written via response object
 */
function suitelet(request, response) {
	try {
		var allocation_details = getPyramidReportDetails();
		nlapiLogExecution('debug', 'alocation details', JSON
				.stringify(allocation_details));
		generatePyramidReport(allocation_details);
	} catch (err) {
		nlapiLogExecution('ERROR', 'suitelet', err);
	}
}

function generatePyramidReport(data) {
	try {
		response.write(JSON.stringify(data));
	} catch (err) {
		nlapiLogExecution('ERROR', 'generatePyramidReport', err);
		throw err;
	}
}

function getPyramidReportDetails() {
	try {
		var allocation_data = getAllAllocations();

		var billed_array = [];
		var unbilled_array = [];
		var practice_array = [];
		var vertical_array = [];

		if (isArrayNotEmpty(allocation_data)) {
			var firstSearchRow = allocation_data[0];
			var columns = firstSearchRow.getAllColumns();
			var columnLen = columns.length;

			for (var i = 0; i < columnLen; i++) {
				var column = columns[i];
				var label = column.getLabel();

				if (label == 'Header_Table_1') {
					billed_column = column;
				} else if (label == 'Header_Table_2') {
					table_2_header_column = column;
				} else if (label == 'Practice_Table_Header') {
					practice_header_column = column;
				} else if (label == 'Vertical_Table_Header') {
					vertical_header_column = column;
				} else if (label == 'Customer_Table_Header') {
					customer_header_column = column;
				} else if (label == 'Resource_Site') {
					site_column = column;
				}
			}

			nlapiLogExecution('debug', 'allocation count',
					allocation_data.length);

			allocation_data.forEach(function(resource) {

				var category = resource.getValue(billed_column);
				var practice = resource.getValue(practice_header_column);
				var vertical = resource.getValue(vertical_header_column);
				var level = String(resource.getText('employeestatus',
						'employee', 'group'));
				// read the numeric part of the level
				level = parseInt(level);

				var employee = resource.getValue('resource', null, 'group');

				// billed list
				if (category == 'Billed') {
					var index = getIdentifierIndex(billed_array, practice);

					if (index == -1) {
						billed_array.push({
							Identifier : practice,
							BelowAnd6 : [],
							AboveAnd5 : [],
							BelowAnd7 : [],
							All : []
						});
						index = billed_array.length - 1;
					}

					if (level <= 5) {
						billed_array[index].AboveAnd5.push(employee);
					} else {
						billed_array[index].BelowAnd6.push(employee);
					}

					if (level >= 7) {
						billed_array[index].BelowAnd7.push(employee);
					}
					billed_array[index].All.push(employee);
				}
				// unbilled list
				else {
					var index = getIdentifierIndex(unbilled_array, practice);

					if (index == -1) {
						unbilled_array.push({
							Identifier : practice,
							BelowAnd6 : [],
							AboveAnd5 : [],
							BelowAnd7 : [],
							All : []
						});
						index = unbilled_array.length - 1;
					}

					if (level <= 5) {
						unbilled_array[index].AboveAnd5.push(employee);
					} else {
						unbilled_array[index].BelowAnd6.push(employee);
					}

					if (level >= 7) {
						unbilled_array[index].BelowAnd7.push(employee);
					}
					unbilled_array[index].All.push(employee);
				}

				// return {
				// Billed : billed_array,
				// Unbilled : unbilled_array
				// ,
				// Practice : practice_array,
				// Vertical : vertical_array
				// };

				// practice list
				var index = getIdentifierIndex(practice_array, practice);

				if (index == -1) {
					practice_array.push({
						Identifier : practice,
						BelowAnd6 : [],
						AboveAnd5 : [],
						BelowAnd7 : [],
						All : []
					});
					index = practice_array.length - 1;
				}

				if (level <= 5) {
					practice_array[index].AboveAnd5.push(employee);
				} else {
					practice_array[index].BelowAnd6.push(employee);
				}

				if (level >= 7) {
					practice_array[index].BelowAnd7.push(employee);
				}
				practice_array[index].All.push(employee);

				// vertical list
				var index = getIdentifierIndex(vertical_array, vertical);

				if (index == -1) {
					vertical_array.push({
						Identifier : vertical,
						BelowAnd6 : [],
						AboveAnd5 : [],
						BelowAnd7 : [],
						All : []
					});
					index = vertical_array.length - 1;
				}

				if (level <= 5) {
					vertical_array[index].AboveAnd5.push(employee);
				} else {
					vertical_array[index].BelowAnd6.push(employee);
				}

				if (level >= 7) {
					vertical_array[index].BelowAnd7.push(employee);
				}
				vertical_array[index].All.push(employee);
			});
		}

		return {
			Billed : billed_array,
			Unbilled : unbilled_array
		// ,
		// Practice : practice_array,
		// Vertical : vertical_array
		};
	} catch (err) {
		nlapiLogExecution('ERROR', 'getPyramidReportDetails', err);
		throw err;
	}
}

function getIdentifierIndex(data, value) {
	var index = -1;

	for (var i = 0; i < data.length; i++) {
		if (data[i].Identifier == value) {
			index = i;
			break;
		}
	}
	return index;
}

function getAllAllocations() {
	try {
		var a_allocation_search = searchRecord('resourceallocation', 965);
		return a_allocation_search;
	} catch (err) {
		nlapiLogExecution('ERROR', 'getAllAllocations', err);
		throw err;
	}
}