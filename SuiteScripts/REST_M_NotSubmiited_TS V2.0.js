/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       16 Aug 2017     deepak.srinivas
 * 2.00		  18 Mar 2020	  praveena madem changed join id as timebill instead of timeentry
 * 3.00       03-Sep-2020	  Shravan Changes for REejected TS more than 4 weeks
 */
 var response = new Response();
function postRESTlet(dataIn) {
    try {
     //   
        nlapiLogExecution('debug', 'dataIn', JSON.stringify(dataIn));


        //Log for current date
        var current_date = nlapiDateToString(new Date());
        nlapiLogExecution('DEBUG', 'Current Date', 'current_date...' + current_date)


        var employeeId = getUserUsingEmailId(dataIn.EmailId);
		nlapiLogExecution('DEBUG', ' employeeId', 'employeeId...' + employeeId);
        var requestType = dataIn.RequestType;
		nlapiLogExecution('DEBUG', ' requestType', 'requestType...' + requestType);
		
        var weekStartDate = dataIn.Data ? dataIn.Data.WeekStartDate : '';

        if (weekStartDate) {
            //Get Week StartDate
            var weekStDate = nlapiStringToDate(weekStartDate);
            //var curr = new Date();
            var day = weekStDate.getDay();
            var firstday = new Date(weekStDate.getTime() - 60 * 60 * 24 * day * 1000);
            nlapiLogExecution('DEBUG', 'firstday', 'firstday...' + firstday);
            weekStartDate = nlapiDateToString(firstday);
            nlapiLogExecution('DEBUG', 'firstday', 'weekstart_date...' + weekStartDate);
        }
        //Test Data
        /*	var employeeId = 41571;
        	var requestType = 'NOTSUBMITTED'; //PASTTIMESHEET ,NOTSUBMITTED
        	var weekStartDate = '8/13/2017';*/
        var endDate = nlapiDateToString(nlapiAddDays(nlapiStringToDate(weekStartDate), 6), 'date');
		nlapiLogExecution('DEBUG', 'endDate', 'endDate...' + endDate);
		
        switch (requestType) 
		{

            case M_Constants.Request.PastTimeSheet:

                if (employeeId && weekStartDate) {
					
                    response.Data = past_five_TS(dataIn, employeeId, weekStartDate, endDate);
                    response.Status = true;
                } else {
                    response.Data = "Some error with the data sent";
                    response.Status = false;
                }
                break;
            case M_Constants.Request.NotSubmitted:
                if (employeeId && weekStartDate) {
					nlapiLogExecution('DEBUG', ' M_Constants.Request.NotSubmitted:', 'M_Constants.Request.NotSubmitted:...');
                    response.Data = notSubmitted_TS(dataIn, employeeId, weekStartDate, endDate);
                    response.Status = true;
                } else {
                    response.Data = "Some error with the data sent";
                    response.Status = false;
                }
                break;
				
				 case M_Constants.Request.Reject:
                if (employeeId && weekStartDate) {
					nlapiLogExecution('DEBUG', ' M_Constants.Request.Rejected:', 'M_Constants.Request.Rejected:...');
                    response.Data = rejected_TS(dataIn, employeeId, weekStartDate, endDate);
                    response.Status = true;
                } else {
                    response.Data = "Some error with the data sent";
                    response.Status = false;
                }
                break;
				
        }
    } 
	
	catch (err) 
	{
        nlapiLogExecution('ERROR', 'postRESTlet', err);
        response.Data = err;
    }
    nlapiLogExecution('ERROR', 'response', JSON.stringify(response));
    return response;
}

function past_five_TS(dataIn, empId, startDt, endDt) {
    try {
        var search = nlapiSearchRecord('timesheet', null, [
                new nlobjSearchFilter('timesheetdate', null, 'onorbefore',
                    startDt),
                new nlobjSearchFilter('employee', null, 'anyof',
                    empId),
                new nlobjSearchFilter('approvalstatus', null, 'noneof', 1)
            ],
            [
                new nlobjSearchColumn('internalid'),
                new nlobjSearchColumn('employee'),
                new nlobjSearchColumn('startdate').setSort(true),
                new nlobjSearchColumn('approvalstatus'),
                new nlobjSearchColumn('enddate'),
                new nlobjSearchColumn('totalhours'), //totalhours
                new nlobjSearchColumn('timeapprover', 'employee'),
               // new nlobjSearchColumn('datecreated', 'timebill')//changed join id Changed By praveena on 18-09-2020 
            ]);
        var JSON = {};
        var dataRow = [];
        if (_logValidation(search)) 
		{
            for (var i = 0; i < search.length && i < 5; i++) 
			{
                JSON = {
                    ID: search[i].getValue('internalid'),
                    Employee: search[i].getText('employee'),
                    WeekStartDate: search[i].getValue('startdate'),
                    WeekEndDate: search[i].getValue('enddate'),
                    TotalHrs: search[i].getValue('totalhours'),
                    ApprovalStatus: search[i].getText('approvalstatus'),
                    Approver: search[i].getText('timeapprover', 'employee'),
					SubmittedDate: ""//search[i].getValue('datecreated', 'timebill')
                };
				//Added By praveena
				var s_date_create=[];
				s_date_create.push(new nlobjSearchColumn('datecreated').setSort(true));
				var obj_tb_search=nlapiSearchRecord('timebill',null,[['timesheet.internalid','anyof',search[i].getValue('internalid')]],
				s_date_create)//Changed join id By praveena on 18-09-2020
				if(obj_tb_search)
				{
				JSON.SubmittedDate=obj_tb_search[0].getValue(s_date_create[0]);
				}
                dataRow.push(JSON);
            }

        }
        return dataRow;

    } 
	catch (err) {
        nlapiLogExecution('ERROR', 'postRESTlet', err);
        response.Data = err;

    }
}
//To get last not submitted and rejected TS	
function notSubmitted_TS(dataIn, empId, startDt, endDt) 
{
    try 
	{

		var d_Emp_Hire_Date = nlapiLookupField('employee', empId ,'hiredate');
		d_Emp_Hire_Date = nlapiStringToDate(d_Emp_Hire_Date);
		nlapiLogExecution('DEBUG', 'd_Emp_Hire_Date==', d_Emp_Hire_Date);
        //Get Past 4 weeks dates
        var past_weeks = 6; //4;
        var relative_time = startDt;
        var weeks = new Array();
        var JSON = {};
        var dataRow = [];
		var d_Start_Date = nlapiDateToString(nlapiAddDays(nlapiStringToDate(startDt), -35), 'date');
		var d_End_Date = nlapiDateToString(nlapiAddDays(nlapiStringToDate(startDt), 0), 'date');
		nlapiLogExecution('DEBUG', 'd_Start_Date', 'd_Start_Date...' + d_Start_Date);
		for(var i_Index_loop = 0 ; i_Index_loop < 6 ; i_Index_loop++ )
			{
					nlapiLogExecution('DEBUG', 'i_Index_loop==', i_Index_loop);
					if(i_Index_loop != 0)
						{
							d_Start_Date = nlapiDateToString(nlapiAddDays(nlapiStringToDate(d_Start_Date), 7), 'date');
						} /////  if(i_Index_loop == 0)
					d_End_Date = nlapiDateToString(nlapiAddDays(nlapiStringToDate(d_Start_Date), 6), 'date'); 
					nlapiLogExecution('DEBUG', 'd_Start_Date==', d_Start_Date);
					nlapiLogExecution('DEBUG', 'd_End_Date==', d_End_Date);
					if( ( nlapiStringToDate(d_Emp_Hire_Date) > nlapiStringToDate(d_Start_Date) )  && ( nlapiStringToDate(d_Emp_Hire_Date) < nlapiStringToDate(d_End_Date) ))
						{
							nlapiLogExecution('DEBUG', 'Hire date lesss==', '----');	
							continue;
						}
			           var filters_ = [];
			            filters_[0] = new nlobjSearchFilter('formuladate', null, 'within', d_Start_Date,d_End_Date);
			            filters_[0].setFormula('{startdate}');
			            filters_[1] = new nlobjSearchFilter('employee', null, 'anyof', empId);
			            var search_ = nlapiSearchRecord('timesheet', null, filters_,
			                    [
			                        new nlobjSearchColumn('internalid'),
			                        new nlobjSearchColumn('employee'),
			                        new nlobjSearchColumn('startdate'),
			                        new nlobjSearchColumn('enddate'),
			                        new nlobjSearchColumn('totalhours'),
			                        new nlobjSearchColumn('approvalstatus'),
			                        new nlobjSearchColumn('timeapprover', 'employee')
			                    ]);
			            if(search_)
			            	{
			            		nlapiLogExecution('DEBUG', 'search_.length', 'search_.length...' + search_.length);
			            			var i_Status = search_[0].getValue('approvalstatus')
			            			var s_Status = search_[0].getText('approvalstatus')
			            			if(!s_Status)
			            				{
			            					s_Status = 'Not Created';
			            				} ///// if(!s_Status)
			    					nlapiLogExecution('DEBUG', 'i_Status==', i_Status);
			    					nlapiLogExecution('DEBUG', 's_Status==', s_Status);
			            			if ( (i_Status == 4) || (i_Status == 1) || (! i_Status))
			            			{
			            				   JSON = 
			            					{
			            	                    WeekStartDate: search_[0].getValue('startdate'),
			            	                    WeekEndtDate: search_[0].getValue('enddate'),
			            						//nlapiDateToString(nlapiAddDays(nlapiStringToDate(d_Weekend_Date), 6), 'date'),
			            	                    Status: s_Status,
			            	                    ID: ''
			            	                };
			            				   dataRow.push(JSON);
			            			} ///// if ( (i_Status == 4) || (i_Status == 1) || (! i_Status))
			            			 
			            	} /////   if(search_)
			            else
        				{
        				 	JSON = 
        					{
        	                    WeekStartDate: d_Start_Date,
        	                    WeekEndtDate: d_End_Date,
        	                    Status: 'Not Created',
        	                    ID: ''
        	                };
        				 	dataRow.push(JSON);
        				} //// else of if(search_)
			} //// for(var i_Index_loop = 0 ; i_Index_loop < 6 ; i_Index_loop++ )
        
        return dataRow;

    }
	catch (err) 
	{
        nlapiLogExecution('ERROR', 'postRESTlet', err);
        response.Data = err;

    }
}/// Not Submitted


function rejected_TS(dataIn, empId, startDt, endDt) 
{
    try 
	{


           var past_weeks = 6; //4;
        var relative_time = startDt;
        var weeks = new Array();
        var JSON = {};
        var dataRow = [];
		var d_Start_Date = nlapiDateToString(nlapiAddDays(nlapiStringToDate(startDt), -63), 'date');
		var d_End_Date = nlapiDateToString(nlapiAddDays(nlapiStringToDate(startDt), 0), 'date');
		nlapiLogExecution('DEBUG', 'd_Start_Date', 'd_Start_Date...' + d_Start_Date);
		nlapiLogExecution('DEBUG', 'd_End_Date', 'd_End_Date...' + d_End_Date);
		
		
		
            var filters = [];
            filters[0] = new nlobjSearchFilter('formuladate', null, 'within', d_Start_Date,d_End_Date);
            filters[0].setFormula('{startdate}');
            filters[1] = new nlobjSearchFilter('employee', null, 'anyof', empId);
            filters[2] = new nlobjSearchFilter('approvalstatus', null, 'anyof', 4);  /// rejected

            var search = nlapiSearchRecord('timesheet', null, filters,
                [
                    new nlobjSearchColumn('internalid'),
                    new nlobjSearchColumn('employee'),
                    new nlobjSearchColumn('startdate'),
                    new nlobjSearchColumn('enddate'),
                    new nlobjSearchColumn('totalhours'),
                    new nlobjSearchColumn('approvalstatus'),
                    new nlobjSearchColumn('timeapprover', 'employee')
                ]);
				nlapiLogExecution('DEBUG', 'search', 'search length...' + search.length);
				
				
		/// sunday and out loop
	
        for (var week_count = 0; week_count < search.length ; week_count++) 
		{
           // var date = nlapiDateToString(nlapiAddDays(nlapiStringToDate(startDt), -7), 'date');
            if (_logValidation(search)) 
			{
				
                JSON = {
                    WeekStartDate: search[week_count].getValue('startdate'),
                    WeekEndtDate: search[week_count].getValue('enddate'),
                    Status: 'Rejected',
                    TotalHrs: search[week_count].getValue('totalhours'),
                    ID: search[week_count].getValue('internalid')
                };
                dataRow.push(JSON);
            } 
               // dataRow.push(JSON);

            
           // startDt = date;
        }
	
        return dataRow;
		}
	catch (err) 
	{
        nlapiLogExecution('ERROR', 'postRESTlet', err);
        response.Data = err;

    }
}//rejected_TS

function _logValidation(value) {
    if (value != null && value.toString() != null && value != '' && value != undefined && value.toString() != undefined && value != 'undefined' && value.toString() != 'undefined' && value.toString() != 'NaN' && value != NaN) {
        return true;
    } else {
        return false;
    }
}