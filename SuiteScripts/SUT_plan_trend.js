/**
 * @author Jayesh
 */

var f_current_month_actual_revenue_total_proj_level = 0;
var a_recognized_revenue_total_proj_level = new Array();
var f_total_cost_overall_prac = new Array();
var f_total_cost_overall_prac_plan = new Array();
var net_cost_plan= 0;
var net_cost_effort= 0;
var net_revenue_plan=0;
var net_revenue_effort=0;
var flag_counter=0;
var flag_counter_plan=0;
var f_revenue_arr=new Array();
var flag_counter_arr=0;
var over_all_array=new Array();
function suiteletFunction_FP_RevRec(request,response)
{
	try
	{
		var o_context = nlapiGetContext();
		var i_user_logegdIn_id = o_context.getUser();
		var i_projectId = request.getParameter('proj_id');
		//var i_projectId = parseInt(93828);
		var s_request_mode = request.getParameter('mode');
		//var s_request_mode = 'Submit';
		var i_revenue_share_id = request.getParameter('rcrd_id');
		//var i_revenue_share_id = parseInt(9);
		var i_revnue_share_status = request.getParameter('status');
		//var i_revnue_share_status = 2;
		var i_month_end_activity_id = request.getParameter('month_id');
		//var i_month_end_activity_id = parseInt(237);
		if(!i_month_end_activity_id)
			return;
			
		var projectWiseRevenue = [];
		var projectAllocationPlan=[];
		var a_revenue_recognized_for_project = new Array();
		var a_filter_get_ytd_revenue_recognized = [['custrecord_project_to_recognize_amount', 'anyof', i_projectId]
													];
							
		var a_columns_get_ytd_revenue_recognized = new Array();
		a_columns_get_ytd_revenue_recognized[0] = new nlobjSearchColumn('custrecord_project_to_recognize_amount');
		a_columns_get_ytd_revenue_recognized[1] = new nlobjSearchColumn('custrecord_practice_to_recognize_amount');
		a_columns_get_ytd_revenue_recognized[2] = new nlobjSearchColumn('custrecord_subparctice_to_recognize_amnt');
		a_columns_get_ytd_revenue_recognized[3] = new nlobjSearchColumn('custrecord_role_to_recognize_amount');
		a_columns_get_ytd_revenue_recognized[4] = new nlobjSearchColumn('custrecord_level_to_recognize_amount');
		a_columns_get_ytd_revenue_recognized[5] = new nlobjSearchColumn('custrecord_revenue_recognized');
		a_columns_get_ytd_revenue_recognized[6] = new nlobjSearchColumn('custrecord_month_to_recognize_amount');
		a_columns_get_ytd_revenue_recognized[7] = new nlobjSearchColumn('custrecord_year_to_recognize_amount');
		
		var a_get_ytd_revenue = nlapiSearchRecord('customrecord_fp_rev_rec_recognized_amnt', null, a_filter_get_ytd_revenue_recognized, a_columns_get_ytd_revenue_recognized);
		if (a_get_ytd_revenue)
		{
			nlapiLogExecution('audit','recognized revenue for project found');
			for(var i_revenue_index=0; i_revenue_index<a_get_ytd_revenue.length; i_revenue_index++)
			{
				//nlapiLogExecution('audit','recognized amnt:- ',a_get_ytd_revenue[i_revenue_index].getValue('custrecord_month_to_recognize_amount'));
				a_revenue_recognized_for_project.push({
												'practice_revenue_recognized': a_get_ytd_revenue[i_revenue_index].getValue('custrecord_practice_to_recognize_amount'),
												'subpractice_revenue_recognized': a_get_ytd_revenue[i_revenue_index].getValue('custrecord_subparctice_to_recognize_amnt'),
												'role_revenue_recognized': a_get_ytd_revenue[i_revenue_index].getValue('custrecord_role_to_recognize_amount'),
												'level_revenue_recognized': a_get_ytd_revenue[i_revenue_index].getValue('custrecord_level_to_recognize_amount'),
												'amount_revenue_recognized': a_get_ytd_revenue[i_revenue_index].getValue('custrecord_revenue_recognized'),
												'month_revenue_recognized': a_get_ytd_revenue[i_revenue_index].getValue('custrecord_month_to_recognize_amount'),
												'year_revenue_recognized': a_get_ytd_revenue[i_revenue_index].getValue('custrecord_year_to_recognize_amount')
												});
			}
		}
		
		i_projectId = i_projectId.trim();
			
		var o_project_object = nlapiLoadRecord('job', i_projectId); // load project record object
		// get necessary information about project from project record
		var s_project_region = o_project_object.getFieldValue('custentity_region');
		var i_customer_name = o_project_object.getFieldValue('parent');
		var s_project_name = o_project_object.getFieldValue('companyname');
		var d_proj_start_date = o_project_object.getFieldValue('startdate');
		var d_proj_strt_date_old_proj = o_project_object.getFieldValue('custentity_rev_rec_new_strt_date');
		var d_proj_end_date = o_project_object.getFieldValue('enddate');
		var i_proj_executing_practice = o_project_object.getFieldValue('custentity_practice');
		var i_proj_manager = o_project_object.getFieldValue('custentity_projectmanager');
		var i_proj_manager_practice = nlapiLookupField('employee', i_proj_manager, 'department');
		var i_project_sow_value = o_project_object.getFieldValue('custentity_projectvalue');
		var i_proj_revenue_rec_type = o_project_object.getFieldValue('custentity_fp_rev_rec_type');
		var s_proj_currency = o_project_object.getFieldText('custentity_project_currency');
		var d_proj_end_date_old_proj = o_project_object.getFieldValue('custentity_rev_rec_new_end_date');
		if(d_proj_strt_date_old_proj)
		{
			d_proj_start_date = d_proj_strt_date_old_proj;
		}
		if(d_proj_end_date_old_proj)
		{
			d_proj_end_date = d_proj_end_date_old_proj;
		}
		var monthBreakUp = getMonthsBreakup(
		        nlapiStringToDate(d_proj_start_date),
		        nlapiStringToDate(d_proj_end_date));
				
		var d_pro_strtDate = nlapiStringToDate(d_proj_start_date);
		var i_year_project = d_pro_strtDate.getFullYear();
		
		var s_pro_endDate = nlapiStringToDate(d_proj_end_date);
		var i_year_project_end = s_pro_endDate.getFullYear();
		
		var i_project_mnth = d_pro_strtDate.getMonth();
		var i_prject_end_mnth = s_pro_endDate.getMonth();
		
		var s_currency_symbol_proj = getCurrency_Symbol(s_proj_currency);
		var s_currency_symbol_usd = getCurrency_Symbol('USD');
		
		// get previous mnth effrt for true up and true down scenario
		var d_today_date = new Date();
		var i_current_mnth = d_today_date.getMonth();
		var i_current_year = d_today_date.getFullYear();
		
		var i_prev_month = parseFloat(i_current_mnth) - parseFloat(1);
			
		{
		var a_total_effort_json = [];
		var a_effort_activity_mnth_end_filter = [['custrecord_fp_rev_rec_month_end_parent_l', 'anyof', parseInt(i_revenue_share_id)],'and',['custrecord_fp_created_by_json','is','F']];
		var a_columns_mnth_end_effort_activity_srch = new Array();
		a_columns_mnth_end_effort_activity_srch[0] = new nlobjSearchColumn('created').setSort(true);	
		var a_get_mnth_end_effrt_activity = nlapiSearchRecord('customrecord_fp_rev_rec_others_mont_end', null, a_effort_activity_mnth_end_filter, a_columns_mnth_end_effort_activity_srch);
		if (a_get_mnth_end_effrt_activity)
		{
			// Get resource allocation for current month
			var a_unique_sub_prac = new Array();
			var a_proj_prac_details = new Array();
			var a_current_pract = new Array();
			var a_current_pract_unique_list_sub_prac = new Array();
			var a_current_pract_unique_list_level = new Array();
			var a_current_pract_unique_list_role = new Array();	
			var d_todays_date = nlapiStringToDate(nlapiDateToString(new Date()));
			var d_firstDay_mnth = new Date(d_todays_date.getFullYear(), d_todays_date.getMonth(), 1);
			var d_lastDay_mnth = new Date(d_todays_date.getFullYear(), d_todays_date.getMonth()+1, 0);
			var s_year_current = d_todays_date.getFullYear();
			var i_month_current = d_todays_date.getMonth();
			var s_month_current = getMonthName_FromMonthNumber(i_month_current);												
			var i_mnth_end_effrt_activity_parent = a_get_mnth_end_effrt_activity[0].getId();
			nlapiLogExecution('audit','mnth end id:- ',i_mnth_end_effrt_activity_parent);
			var a_effort_plan_mnth_end_filter = [['custrecord_fp_others_mnth_act_parent', 'anyof', parseInt(i_mnth_end_effrt_activity_parent)]];
			var a_columns_mnth_end_effort_plan_srch = new Array();
			a_columns_mnth_end_effort_plan_srch[0] = new nlobjSearchColumn('custrecordfp_othr_month_end_sub_practice',null,'group').setSort(false);
			a_columns_mnth_end_effort_plan_srch[1] = new nlobjSearchColumn('custrecordfp_others_mnth_end_practice',null,'group');
			a_columns_mnth_end_effort_plan_srch[2] = new nlobjSearchColumn('custrecord_fp_month_end_role',null,'group').setSort(false);
			a_columns_mnth_end_effort_plan_srch[3] = new nlobjSearchColumn('custrecordfp_other_mnth_end_level',null,'group').setSort(false);
			a_columns_mnth_end_effort_plan_srch[4] = new nlobjSearchColumn('custrecord_fp_others__location_mnth_end',null,'group').setSort(false);
			a_columns_mnth_end_effort_plan_srch[5] = new nlobjSearchColumn('custrecord_fp_others__resource_cost',null,'group');
			a_columns_mnth_end_effort_plan_srch[6] = new nlobjSearchColumn('custrecord_fp_others_mnth_end_alloc_per',null,'group');
			a_columns_mnth_end_effort_plan_srch[7] = new nlobjSearchColumn('custrecord_fp_others_mnth_end_year',null,'group');
			a_columns_mnth_end_effort_plan_srch[8] = new nlobjSearchColumn('custrecord_fp_others_mnth_end_year_y',null,'group');
			//a_columns_mnth_end_effort_plan_srch[9] = new nlobjSearchColumn('custrecord_revenue_share_mnth_end',null,'group');
			a_columns_mnth_end_effort_plan_srch[9] = new nlobjSearchColumn('custrecord_fp_others_mnth_end_year',null,'group');
			var a_get_mnth_end_effrt_plan_prac = nlapiSearchRecord('customrecord_fp_rev_rec_othr_mnth_end_ef', null, a_effort_plan_mnth_end_filter, a_columns_mnth_end_effort_plan_srch);
			var mnth_split_fil=['custrecord_fp_rev_rec_other','anyof',parseInt(i_revenue_share_id)];
			var mnth_split_col=new Array();
			mnth_split_col[0]=new nlobjSearchColumn('custrecord_other_month');
			mnth_split_col[1]=new nlobjSearchColumn('created').setSort(false);
			var mnth_split_search=nlapiSearchRecord('customrecord_fp_other_monthly_rev',null,mnth_split_fil,mnth_split_col);
			
			if (a_get_mnth_end_effrt_plan_prac)
			{
				nlapiLogExecution('audit','effrt length(CUSTOM TABLE):- '+a_get_mnth_end_effrt_plan_prac.length);
				var i_previous_sub_prac = 0;
				var i_previous_role = 0;
				var i_previous_level = 0;
				var i_previous_location = 0;
				var s_prev_month = '';
				var f_revised_allocation = 0;
				var i_line_no = 1;
				var a_contractor_avearge = new Array();
				var i_frst_contractor = 0;
				var i_total_contractor_row_flag = 0;
				var f_total_contractor_resource_cost = 0;
				for(var i_mnth_end_plan=0; i_mnth_end_plan<a_get_mnth_end_effrt_plan_prac.length; i_mnth_end_plan++)
				{
					if(i_previous_sub_prac == 0)
					{
						i_previous_sub_prac = a_get_mnth_end_effrt_plan_prac[i_mnth_end_plan].getValue('custrecordfp_othr_month_end_sub_practice',null,'group');
						i_previous_role = a_get_mnth_end_effrt_plan_prac[i_mnth_end_plan].getValue('custrecord_fp_month_end_role',null,'group');
						i_previous_level = a_get_mnth_end_effrt_plan_prac[i_mnth_end_plan].getValue('custrecordfp_other_mnth_end_level',null,'group');
						i_previous_location = a_get_mnth_end_effrt_plan_prac[i_mnth_end_plan].getValue('custrecord_fp_others__location_mnth_end',null,'group');
					}
					if(i_previous_sub_prac != a_get_mnth_end_effrt_plan_prac[i_mnth_end_plan].getValue('custrecordfp_othr_month_end_sub_practice',null,'group'))
					{
									//i_current_sublist_length
						i_line_no++;
						i_previous_sub_prac = a_get_mnth_end_effrt_plan_prac[i_mnth_end_plan].getValue('custrecordfp_othr_month_end_sub_practice',null,'group');
						i_previous_role = a_get_mnth_end_effrt_plan_prac[i_mnth_end_plan].getValue('custrecord_fp_month_end_role',null,'group');
						i_previous_level = a_get_mnth_end_effrt_plan_prac[i_mnth_end_plan].getValue('custrecordfp_other_mnth_end_level',null,'group');
						i_previous_location = a_get_mnth_end_effrt_plan_prac[i_mnth_end_plan].getValue('custrecord_fp_others__location_mnth_end',null,'group');
					}
					else if(i_previous_sub_prac == a_get_mnth_end_effrt_plan_prac[i_mnth_end_plan].getValue('custrecordfp_othr_month_end_sub_practice',null,'group'))
					{
						if(i_previous_level != a_get_mnth_end_effrt_plan_prac[i_mnth_end_plan].getValue('custrecordfp_other_mnth_end_level',null,'group'))
						{
							if (a_get_mnth_end_effrt_plan_prac[i_mnth_end_plan].getValue('custrecordfp_other_mnth_end_level',null,'group')) {
								i_line_no++;
								i_previous_sub_prac = a_get_mnth_end_effrt_plan_prac[i_mnth_end_plan].getValue('custrecordfp_othr_month_end_sub_practice',null,'group');
								i_previous_role = a_get_mnth_end_effrt_plan_prac[i_mnth_end_plan].getValue('custrecord_fp_month_end_role',null,'group');
								i_previous_level = a_get_mnth_end_effrt_plan_prac[i_mnth_end_plan].getValue('custrecordfp_other_mnth_end_level',null,'group');
								i_previous_location = a_get_mnth_end_effrt_plan_prac[i_mnth_end_plan].getValue('custrecord_fp_others__location_mnth_end',null,'group');
							}
						}
						else if(i_previous_role != a_get_mnth_end_effrt_plan_prac[i_mnth_end_plan].getValue('custrecord_fp_month_end_role',null,'group'))
						{
							if (a_get_mnth_end_effrt_plan_prac[i_mnth_end_plan].getValue('custrecord_fp_month_end_role',null,'group')) {
								i_line_no++;
								i_previous_sub_prac = a_get_mnth_end_effrt_plan_prac[i_mnth_end_plan].getValue('custrecordfp_othr_month_end_sub_practice',null,'group');
								i_previous_role = a_get_mnth_end_effrt_plan_prac[i_mnth_end_plan].getValue('custrecord_fp_month_end_role',null,'group');
								i_previous_level = a_get_mnth_end_effrt_plan_prac[i_mnth_end_plan].getValue('custrecordfp_other_mnth_end_level',null,'group');
								i_previous_location = a_get_mnth_end_effrt_plan_prac[i_mnth_end_plan].getValue('custrecord_fp_others__location_mnth_end',null,'group');
							}
						}
						else if(i_previous_location != a_get_mnth_end_effrt_plan_prac[i_mnth_end_plan].getValue('custrecord_fp_others__location_mnth_end',null,'group'))
						{
							i_line_no++;
							i_previous_sub_prac = a_get_mnth_end_effrt_plan_prac[i_mnth_end_plan].getValue('custrecordfp_othr_month_end_sub_practice',null,'group');
							i_previous_role = a_get_mnth_end_effrt_plan_prac[i_mnth_end_plan].getValue('custrecord_fp_month_end_role',null,'group');
							i_previous_level = a_get_mnth_end_effrt_plan_prac[i_mnth_end_plan].getValue('custrecordfp_other_mnth_end_level',null,'group');
							i_previous_location = a_get_mnth_end_effrt_plan_prac[i_mnth_end_plan].getValue('custrecord_fp_others__location_mnth_end',null,'group');
										
						}
					}
					var i_is_prac_present_already = find_prac_exist(a_current_pract,a_get_mnth_end_effrt_plan_prac[i_mnth_end_plan].getValue('custrecordfp_othr_month_end_sub_practice',null,'group'),a_get_mnth_end_effrt_plan_prac[i_mnth_end_plan].getValue('custrecordfp_other_mnth_end_level',null,'group'),a_get_mnth_end_effrt_plan_prac[i_mnth_end_plan].getValue('custrecord_fp_others__location_mnth_end',null,'group'));
					if(i_is_prac_present_already == 0)
					{
						
						a_current_pract.push({
							'i_sub_practice': a_get_mnth_end_effrt_plan_prac[i_mnth_end_plan].getValue('custrecordfp_othr_month_end_sub_practice',null,'group'),
							'i_level': a_get_mnth_end_effrt_plan_prac[i_mnth_end_plan].getValue('custrecordfp_other_mnth_end_level',null,'group'),
							'i_location': a_get_mnth_end_effrt_plan_prac[i_mnth_end_plan].getValue('custrecord_fp_others__location_mnth_end',null,'group'),
							'i_line_no': i_line_no
						});
					}
								
					var s_mnth_year_1 = '';
					var s_mnth_name = a_get_mnth_end_effrt_plan_prac[i_mnth_end_plan].getValue('custrecord_fp_others_mnth_end_year',null,'group');
					var s_year_name = a_get_mnth_end_effrt_plan_prac[i_mnth_end_plan].getValue('custrecord_fp_others_mnth_end_year_y',null,'group');
					s_year_name = s_year_name.split('.');
					s_year_name = s_year_name[0];
					s_mnth_year_1 = s_mnth_name+'_'+s_year_name;
					var s_mnth_year = s_month_current+'_'+s_year_current;
					var i_allocation_to_display = a_get_mnth_end_effrt_plan_prac[i_mnth_end_plan].getValue('custrecord_fp_others_mnth_end_alloc_per',null,'group');
					i_allocation_to_display = parseFloat(i_allocation_to_display);
					var i_practice = a_get_mnth_end_effrt_plan_prac[i_mnth_end_plan].getValue('custrecordfp_others_mnth_end_practice',null,'group');
					var s_practice = a_get_mnth_end_effrt_plan_prac[i_mnth_end_plan].getText('custrecordfp_others_mnth_end_practice',null,'group');
					var i_sub_practice =  a_get_mnth_end_effrt_plan_prac[i_mnth_end_plan].getValue('custrecordfp_othr_month_end_sub_practice',null,'group');
					var s_sub_practice =  a_get_mnth_end_effrt_plan_prac[i_mnth_end_plan].getText('custrecordfp_othr_month_end_sub_practice',null,'group');
					var i_role = a_get_mnth_end_effrt_plan_prac[i_mnth_end_plan].getValue('custrecord_fp_month_end_role',null,'group');
					var s_role = a_get_mnth_end_effrt_plan_prac[i_mnth_end_plan].getText('custrecord_fp_month_end_role',null,'group');
					var i_level = a_get_mnth_end_effrt_plan_prac[i_mnth_end_plan].getValue('custrecordfp_other_mnth_end_level',null,'group');
					var s_level = a_get_mnth_end_effrt_plan_prac[i_mnth_end_plan].getText('custrecordfp_other_mnth_end_level',null,'group');
					var i_location = a_get_mnth_end_effrt_plan_prac[i_mnth_end_plan].getValue('custrecord_fp_others__location_mnth_end',null,'group');
					var s_location =a_get_mnth_end_effrt_plan_prac[i_mnth_end_plan].getText('custrecord_fp_others__location_mnth_end',null,'group');
					var s_mnth=a_get_mnth_end_effrt_plan_prac[i_mnth_end_plan].getValue('custrecord_fp_others_mnth_end_year',null,'group');
					var s_year=a_get_mnth_end_effrt_plan_prac[i_mnth_end_plan].getValue('custrecord_fp_others_mnth_end_year_y',null,'group');
					var f_revenue_plan = a_get_mnth_end_effrt_plan_prac[i_mnth_end_plan].getValue('custrecord_fp_others__resource_cost',null,'group');
						if (!f_revenue_plan) 
							f_revenue_plan = 0;
					s_year=parseInt(s_year);
					var s_month_name = s_mnth + '_' + s_year;
					var total_revenue_plan = parseFloat(i_allocation_to_display) * parseFloat(f_revenue_plan);
						total_revenue_plan = parseFloat(total_revenue_plan).toFixed(2);
					for(var i_mnth_split=0;i_mnth_split<1;i_mnth_split++)
					{
						var mnth_split_json=mnth_split_search[i_mnth_split].getValue('custrecord_other_month');
						var mnth_json_arr=JSON.parse(mnth_split_json);						
						var mnth_json_len=mnth_json_arr[0].length;
						for(var i_mnth_json=0;i_mnth_json<mnth_json_len;i_mnth_json++)
						{
							var mnth_json=mnth_json_arr[0][i_mnth_json].month;
							var amount_json=mnth_json_arr[0][i_mnth_json].amount;
							if(s_month_name==mnth_json)
							var f_revenue_share=parseInt(amount_json);
						}
					}	
					
					var i_prac_subPrac_role_level = i_practice + '_' + i_sub_practice + '_' + i_role + '_' + i_level + '_' + i_location;
					if (!projectAllocationPlan[i_prac_subPrac_role_level]) {
							projectAllocationPlan[i_prac_subPrac_role_level] = {
								practice_name:s_practice ,
								sub_prac_name:s_sub_practice ,
								sub_practice: i_sub_practice,
								role_name: s_role,
								level_name: s_level,
								location_name: s_location,
								allocation:parseFloat(i_allocation_to_display),
								RevenueDataPlan: {}
							};
						projectAllocationPlan[i_prac_subPrac_role_level].RevenueDataPlan[i_practice] = new yearData(i_year_project);
						projectAllocationPlan[i_prac_subPrac_role_level].RevenueDataPlan[i_practice][s_month_name].Amount = total_revenue_plan;
						projectAllocationPlan[i_prac_subPrac_role_level].RevenueDataPlan[i_practice][s_month_name].prcnt_allocated = i_allocation_to_display;
						projectAllocationPlan[i_prac_subPrac_role_level].RevenueDataPlan[i_practice][s_month_name].revenue_share_plan = f_revenue_share;
						projectAllocationPlan[i_prac_subPrac_role_level].RevenueDataPlan[i_practice][s_month_name].practice_share = pract_share_split;
						projectAllocationPlan[i_prac_subPrac_role_level].RevenueDataPlan[i_practice][s_month_name].f_mnth_flag = mnt_flag;
					//	projectWiseRevenue[i_prac_subPrac_role_level].RevenueDataPlan[i_practice][s_month_name].mnth_strt = s_mnth_strt_date;
					//	projectWiseRevenue[i_prac_subPrac_role_level].RevenueDataPlan[i_practice][s_month_name].mnth_end = s_mnth_end_date;	
					}	
					else {
						projectAllocationPlan[i_prac_subPrac_role_level].RevenueDataPlan[i_practice][s_month_name].Amount = total_revenue_plan;
						projectAllocationPlan[i_prac_subPrac_role_level].RevenueDataPlan[i_practice][s_month_name].prcnt_allocated = i_allocation_to_display;
						projectAllocationPlan[i_prac_subPrac_role_level].RevenueDataPlan[i_practice][s_month_name].revenue_share_plan = f_revenue_share;
						//projectWiseRevenue[i_prac_subPrac_role_level].RevenueDataPlan[i_practice][s_month_name].mnth_strt = s_mnth_strt_date;
						//projectWiseRevenue[i_prac_subPrac_role_level].RevenueDataPlan[i_practice][s_month_name].mnth_end = s_mnth_end_date;
					
					}
				}
			}
								
						
		}
			var o_mnth_end_effrt = nlapiLoadRecord('customrecord_fp_others_mnth_end_json',i_month_end_activity_id);
			
			var s_effrt_json = o_mnth_end_effrt.getFieldValue('custrecord_fp_others_mnth_end_fld_json1');
			//s_effrt_json = JSON.stringify(s_effrt_json);
			var s_effrt_json_2 = o_mnth_end_effrt.getFieldValue('custrecord_fp_others_mnth_end_fld_json2');
			//s_effrt_json_2 = JSON.stringify(s_effrt_json_2);
			var s_effrt_json_3 = o_mnth_end_effrt.getFieldValue('custrecord_fp_others_mnth_end_fld_json3');
			var s_effrt_json_4 = o_mnth_end_effrt.getFieldValue('custrecord_fp_others_mnth_end_fld_json4');
			
			var a_duplicate_sub_prac_count = new Array();
			var a_unique_list_practice_sublist = new Array();
			
			var a_subprac_searched_once = new Array();
			var a_subprac_searched_once_month = new Array();
			var a_subprac_searched_once_year = new Array();
			
			if(s_effrt_json)
			{
				a_total_effort_json.push(s_effrt_json);
				
				var i_practice_previous = 0;
				var f_total_revenue_for_tenure = 0;
				//for (var i_mnth_end_plan = 0; i_mnth_end_plan < a_get_mnth_end_effrt_plan.length; i_mnth_end_plan++)
				var s_entire_json_clubed = JSON.parse(s_effrt_json);
				for (var i_mnth_end_plan = 0; i_mnth_end_plan < s_entire_json_clubed.length; i_mnth_end_plan++)
				{
					var a_row_json_data = JSON.parse(JSON.stringify(s_entire_json_clubed[i_mnth_end_plan]));
					for (var i_row_json_index = 0; i_row_json_index < a_row_json_data.length; i_row_json_index++)
					{
						var i_practice = a_row_json_data[i_row_json_index].prac;
						var s_practice = a_row_json_data[i_row_json_index].prac_text;
						var i_sub_practice = a_row_json_data[i_row_json_index].subprac;
						var s_sub_practice = a_row_json_data[i_row_json_index].subprac_text;
						var i_role = a_row_json_data[i_row_json_index].role;
						var s_role = a_row_json_data[i_row_json_index].role_text;
						var i_level = a_row_json_data[i_row_json_index].level;
						var s_level = a_row_json_data[i_row_json_index].level_text;
						var i_location = a_row_json_data[i_row_json_index].loc;
						var s_location = a_row_json_data[i_row_json_index].loc_text;
						var f_revenue = a_row_json_data[i_row_json_index].cost;
						if (!f_revenue) 
							f_revenue = 0;
						
						var f_revenue_share = a_row_json_data[i_row_json_index].share;
						if (!f_revenue_share) 
							f_revenue_share = 0;
						var pract_share_split=a_row_json_data[i_row_json_index].pract_share;
						if(!pract_share_split)
						pract_share_split=0;
						var mnt_flag=a_row_json_data[i_row_json_index].mnth_flag;
						var i_no_of_resources = a_row_json_data[i_row_json_index].allo;
						var s_mnth_strt_date = '1/31/2017';
						var s_mnth_end_date = '31/31/2017';
						var s_mnth = a_row_json_data[i_row_json_index].mnth;
						var s_year = a_row_json_data[i_row_json_index].year;
						
						var f_revenue_recognized = 0;
						
						if(a_unique_list_practice_sublist.indexOf(a_row_json_data[i_row_json_index].subprac)<0)
						{
							a_unique_list_practice_sublist.push(a_row_json_data[i_row_json_index].subprac);
							a_duplicate_sub_prac_count.push({
														'sub_prac': a_row_json_data[i_row_json_index].subprac,
														'sub_prac_name': a_row_json_data[i_row_json_index].subprac_text,
														'revenue_share': 50000,
														'no_count': 0
														});
														
						}
						
						f_revenue_recognized = find_recognized_revenue(a_revenue_recognized_for_project,i_practice,i_sub_practice,i_role,i_level,s_mnth,s_year,a_subprac_searched_once,a_subprac_searched_once_month,a_subprac_searched_once_year);
						
						if (!f_revenue_recognized) 
							f_revenue_recognized = 0;
							
						
						
						var s_month_name = s_mnth + '_' + s_year;
						
						var total_revenue = parseFloat(i_no_of_resources) * parseFloat(f_revenue);
						total_revenue = parseFloat(total_revenue).toFixed(2);
						
					
						
						if (i_practice_previous == 0) {
							i_practice_previous = a_row_json_data[i_row_json_index].prac_row;
						}
						
						if (i_practice_previous != a_row_json_data[i_row_json_index].prac_row) {
							f_total_revenue_for_tenure = 0;
							i_practice_previous = a_row_json_data[i_row_json_index].prac_row;
						}
					
						f_total_revenue_for_tenure = parseFloat(f_total_revenue_for_tenure) + parseFloat(total_revenue);
						f_total_revenue_for_tenure = parseFloat(f_total_revenue_for_tenure).toFixed(2);
						
						var i_prac_subPrac_role_level = i_practice + '_' + i_sub_practice + '_' + i_role + '_' + i_level + '_' + i_location;
						
						if (!projectWiseRevenue[i_prac_subPrac_role_level]) {
							projectWiseRevenue[i_prac_subPrac_role_level] = {
								practice_name: s_practice,
								sub_prac_name: s_sub_practice,
								sub_practice: i_sub_practice,
								role_name: s_role,
								level_name: s_level,
								location_name: s_location,
								total_revenue_for_tenure: f_total_revenue_for_tenure,
								//revenue_share: f_revenue_share,
								RevenueData: {}
							};
							
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice] = new yearData(i_year_project);
							
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].Amount = total_revenue;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].prcnt_allocated = i_no_of_resources;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].recognized_revenue = f_revenue_recognized;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].mnth_strt = s_mnth_strt_date;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].mnth_end = s_mnth_end_date;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].revenue_share = f_revenue_share;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].practice_share = pract_share_split;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].f_mnth_flag = mnt_flag;
						}
						else {
							projectWiseRevenue[i_prac_subPrac_role_level].total_revenue_for_tenure = f_total_revenue_for_tenure;
							
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].Amount = total_revenue;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].prcnt_allocated = i_no_of_resources;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].recognized_revenue = f_revenue_recognized;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].mnth_strt = s_mnth_strt_date;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].mnth_end = s_mnth_end_date;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].revenue_share = f_revenue_share;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].practice_share = pract_share_split;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].f_mnth_flag = mnt_flag;
						}
					}
				}
			}
			
			if(s_effrt_json_2)
			{
				var i_practice_previous = 0;
				var f_total_revenue_for_tenure = 0;
				//for (var i_mnth_end_plan = 0; i_mnth_end_plan < a_get_mnth_end_effrt_plan.length; i_mnth_end_plan++)
				var s_entire_json_clubed = JSON.parse(s_effrt_json_2);
				for (var i_mnth_end_plan = 0; i_mnth_end_plan < s_entire_json_clubed.length; i_mnth_end_plan++)
				{
					var a_row_json_data = JSON.parse(JSON.stringify(s_entire_json_clubed[i_mnth_end_plan]));
					for (var i_row_json_index = 0; i_row_json_index < a_row_json_data.length; i_row_json_index++)
					{
						var i_practice = a_row_json_data[i_row_json_index].prac;
						var s_practice = a_row_json_data[i_row_json_index].prac_text;
						var i_sub_practice = a_row_json_data[i_row_json_index].subprac;
						var s_sub_practice = a_row_json_data[i_row_json_index].subprac_text;
						var i_role = a_row_json_data[i_row_json_index].role;
						var s_role = a_row_json_data[i_row_json_index].role_text;
						var i_level = a_row_json_data[i_row_json_index].level;
						var s_level = a_row_json_data[i_row_json_index].level_text;
						var i_location = a_row_json_data[i_row_json_index].loc;
						var s_location = a_row_json_data[i_row_json_index].loc_text;
						var f_revenue = a_row_json_data[i_row_json_index].cost;
						if (!f_revenue) 
							f_revenue = 0;
						
						var f_revenue_share = a_row_json_data[i_row_json_index].share;
						if (!f_revenue_share) 
							f_revenue_share = 0;
						var pract_share_split=a_row_json_data[i_row_json_index].pract_share;
						if(!pract_share_split)
						pract_share_split=0;
						var mnt_flag=a_row_json_data[i_row_json_index].mnth_flag;
						var i_no_of_resources = a_row_json_data[i_row_json_index].allo;
						var s_mnth_strt_date = '1/31/2017';
						var s_mnth_end_date = '31/31/2017';
						var s_mnth = a_row_json_data[i_row_json_index].mnth;
						var s_year = a_row_json_data[i_row_json_index].year;
						
						var f_revenue_recognized = 0;
						
						if(a_unique_list_practice_sublist.indexOf(a_row_json_data[i_row_json_index].subprac)<0)
						{
							a_unique_list_practice_sublist.push(a_row_json_data[i_row_json_index].subprac);
							a_duplicate_sub_prac_count.push({
														'sub_prac': a_row_json_data[i_row_json_index].subprac,
														'sub_prac_name': a_row_json_data[i_row_json_index].subprac_text,
														'revenue_share': 50000,
														'no_count': 0
														});
														
						}
						
						f_revenue_recognized = find_recognized_revenue(a_revenue_recognized_for_project,i_practice,i_sub_practice,i_role,i_level,s_mnth,s_year,a_subprac_searched_once,a_subprac_searched_once_month,a_subprac_searched_once_year);
						
						if (!f_revenue_recognized) 
							f_revenue_recognized = 0;
						
					
						
						var s_month_name = s_mnth + '_' + s_year;
						
						var total_revenue = parseFloat(i_no_of_resources) * parseFloat(f_revenue);
						total_revenue = parseFloat(total_revenue).toFixed(2);
						
						
						if (i_practice_previous == 0) {
							i_practice_previous = a_row_json_data[i_row_json_index].prac_row;
						}
						
						if (i_practice_previous != a_row_json_data[i_row_json_index].prac_row) {
							f_total_revenue_for_tenure = 0;
							i_practice_previous = a_row_json_data[i_row_json_index].prac_row;
						}
					
						f_total_revenue_for_tenure = parseFloat(f_total_revenue_for_tenure) + parseFloat(total_revenue);
						f_total_revenue_for_tenure = parseFloat(f_total_revenue_for_tenure).toFixed(2);
						
						var i_prac_subPrac_role_level = i_practice + '_' + i_sub_practice + '_' + i_role + '_' + i_level + '_' + i_location;
						
						if (!projectWiseRevenue[i_prac_subPrac_role_level]) {
							projectWiseRevenue[i_prac_subPrac_role_level] = {
								practice_name: s_practice,
								sub_prac_name: s_sub_practice,
								sub_practice: i_sub_practice,
								role_name: s_role,
								level_name: s_level,
								location_name: s_location,
								total_revenue_for_tenure: f_total_revenue_for_tenure,
								//revenue_share: f_revenue_share,
								RevenueData: {}
							};
							
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice] = new yearData(i_year_project);
							
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].Amount = total_revenue;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].prcnt_allocated = i_no_of_resources;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].recognized_revenue = f_revenue_recognized;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].mnth_strt = s_mnth_strt_date;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].mnth_end = s_mnth_end_date;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].revenue_share = f_revenue_share;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].practice_share = pract_share_split;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].f_mnth_flag = mnt_flag;
						}
						else {
							projectWiseRevenue[i_prac_subPrac_role_level].total_revenue_for_tenure = f_total_revenue_for_tenure;
							
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].Amount = total_revenue;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].prcnt_allocated = i_no_of_resources;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].recognized_revenue = f_revenue_recognized;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].mnth_strt = s_mnth_strt_date;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].mnth_end = s_mnth_end_date;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].revenue_share = f_revenue_share;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].practice_share = pract_share_split;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].f_mnth_flag = mnt_flag;
						}
					}
				}
			}
			
			if(s_effrt_json_3)
			{
				var i_practice_previous = 0;
				var f_total_revenue_for_tenure = 0;
				//for (var i_mnth_end_plan = 0; i_mnth_end_plan < a_get_mnth_end_effrt_plan.length; i_mnth_end_plan++)
				var s_entire_json_clubed = JSON.parse(s_effrt_json_3);
				for (var i_mnth_end_plan = 0; i_mnth_end_plan < s_entire_json_clubed.length; i_mnth_end_plan++)
				{
					var a_row_json_data = JSON.parse(JSON.stringify(s_entire_json_clubed[i_mnth_end_plan]));
					for (var i_row_json_index = 0; i_row_json_index < a_row_json_data.length; i_row_json_index++)
					{
						var i_practice = a_row_json_data[i_row_json_index].prac;
						var s_practice = a_row_json_data[i_row_json_index].prac_text;
						var i_sub_practice = a_row_json_data[i_row_json_index].subprac;
						var s_sub_practice = a_row_json_data[i_row_json_index].subprac_text;
						var i_role = a_row_json_data[i_row_json_index].role;
						var s_role = a_row_json_data[i_row_json_index].role_text;
						var i_level = a_row_json_data[i_row_json_index].level;
						var s_level = a_row_json_data[i_row_json_index].level_text;
						var i_location = a_row_json_data[i_row_json_index].loc;
						var s_location = a_row_json_data[i_row_json_index].loc_text;
						var f_revenue = a_row_json_data[i_row_json_index].cost;
						if (!f_revenue) 
							f_revenue = 0;
						
						var f_revenue_share = a_row_json_data[i_row_json_index].share;
						if (!f_revenue_share) 
							f_revenue_share = 0;
						var pract_share_split=a_row_json_data[i_row_json_index].pract_share;
						if(!pract_share_split)
							pract_share_split=0;
						var mnt_flag=a_row_json_data[i_row_json_index].mnth_flag;	
						var i_no_of_resources = a_row_json_data[i_row_json_index].allo;
						var s_mnth_strt_date = '1/31/2017';
						var s_mnth_end_date = '31/31/2017';
						var s_mnth = a_row_json_data[i_row_json_index].mnth;
						var s_year = a_row_json_data[i_row_json_index].year;
						
						var f_revenue_recognized = 0;
						
						if(a_unique_list_practice_sublist.indexOf(a_row_json_data[i_row_json_index].subprac)<0)
						{
							a_unique_list_practice_sublist.push(a_row_json_data[i_row_json_index].subprac);
							a_duplicate_sub_prac_count.push({
														'sub_prac': a_row_json_data[i_row_json_index].subprac,
														'sub_prac_name': a_row_json_data[i_row_json_index].subprac_text,
														'revenue_share': 50000,
														'no_count': 0
														});
											
						}
						
						f_revenue_recognized = find_recognized_revenue(a_revenue_recognized_for_project,i_practice,i_sub_practice,i_role,i_level,s_mnth,s_year,a_subprac_searched_once,a_subprac_searched_once_month,a_subprac_searched_once_year);
						
						if (!f_revenue_recognized) 
							f_revenue_recognized = 0;
						
						
						var s_month_name = s_mnth + '_' + s_year;
						
						var total_revenue = parseFloat(i_no_of_resources) * parseFloat(f_revenue);
						total_revenue = parseFloat(total_revenue).toFixed(2);
						
						/*if(i_practice_previous == 0)
						 {
						 i_practice_previous = a_get_mnth_end_effrt_plan[i_mnth_end_plan].getValue('custrecord_mnth_end_practice');
						 }
						 
						 if(i_practice_previous != a_get_mnth_end_effrt_plan[i_mnth_end_plan].getValue('custrecord_mnth_end_practice'))
						 {
						 f_total_revenue_for_tenure = 0;
						 i_practice_previous = a_get_mnth_end_effrt_plan[i_mnth_end_plan].getValue('custrecord_mnth_end_practice');
						 }*/
						
						if (i_practice_previous == 0) {
							i_practice_previous = a_row_json_data[i_row_json_index].prac_row;
						}
						
						if (i_practice_previous != a_row_json_data[i_row_json_index].prac_row) {
							f_total_revenue_for_tenure = 0;
							i_practice_previous = a_row_json_data[i_row_json_index].prac_row;
						}
					
						f_total_revenue_for_tenure = parseFloat(f_total_revenue_for_tenure) + parseFloat(total_revenue);
						f_total_revenue_for_tenure = parseFloat(f_total_revenue_for_tenure).toFixed(2);
						
						var i_prac_subPrac_role_level = i_practice + '_' + i_sub_practice + '_' + i_role + '_' + i_level + '_' + i_location;
						
						if (!projectWiseRevenue[i_prac_subPrac_role_level]) {
							projectWiseRevenue[i_prac_subPrac_role_level] = {
								practice_name: s_practice,
								sub_prac_name: s_sub_practice,
								sub_practice: i_sub_practice,
								role_name: s_role,
								level_name: s_level,
								location_name: s_location,
								total_revenue_for_tenure: f_total_revenue_for_tenure,
								//revenue_share: f_revenue_share,
								RevenueData: {}
							};
							
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice] = new yearData(i_year_project);
							
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].Amount = total_revenue;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].prcnt_allocated = i_no_of_resources;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].recognized_revenue = f_revenue_recognized;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].mnth_strt = s_mnth_strt_date;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].mnth_end = s_mnth_end_date;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].revenue_share = f_revenue_share;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].practice_share = pract_share_split;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].f_mnth_flag = mnt_flag;
						}
						else {
							projectWiseRevenue[i_prac_subPrac_role_level].total_revenue_for_tenure = f_total_revenue_for_tenure;
							
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].Amount = total_revenue;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].prcnt_allocated = i_no_of_resources;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].recognized_revenue = f_revenue_recognized;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].mnth_strt = s_mnth_strt_date;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].mnth_end = s_mnth_end_date;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].revenue_share = f_revenue_share;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].practice_share = pract_share_split;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].f_mnth_flag = mnt_flag;
						}
					}
				}
			}
			
			if(s_effrt_json_4)
			{
				var i_practice_previous = 0;
				var f_total_revenue_for_tenure = 0;
				//for (var i_mnth_end_plan = 0; i_mnth_end_plan < a_get_mnth_end_effrt_plan.length; i_mnth_end_plan++)
				var s_entire_json_clubed = JSON.parse(s_effrt_json_4);
				for (var i_mnth_end_plan = 0; i_mnth_end_plan < s_entire_json_clubed.length; i_mnth_end_plan++)
				{
					var a_row_json_data = JSON.parse(JSON.stringify(s_entire_json_clubed[i_mnth_end_plan]));
					for (var i_row_json_index = 0; i_row_json_index < a_row_json_data.length; i_row_json_index++)
					{
						var i_practice = a_row_json_data[i_row_json_index].prac;
						var s_practice = a_row_json_data[i_row_json_index].prac_text;
						var i_sub_practice = a_row_json_data[i_row_json_index].subprac;
						var s_sub_practice = a_row_json_data[i_row_json_index].subprac_text;
						var i_role = a_row_json_data[i_row_json_index].role;
						var s_role = a_row_json_data[i_row_json_index].role_text;
						var i_level = a_row_json_data[i_row_json_index].level;
						var s_level = a_row_json_data[i_row_json_index].level_text;
						var i_location = a_row_json_data[i_row_json_index].loc;
						var s_location = a_row_json_data[i_row_json_index].loc_text;
						var f_revenue = a_row_json_data[i_row_json_index].cost;
						if (!f_revenue) 
							f_revenue = 0;
						
						var f_revenue_share = a_row_json_data[i_row_json_index].share;
						if (!f_revenue_share) 
							f_revenue_share = 0;
						var pract_share_split=a_row_json_data[i_row_json_index].pract_share;
						if(!pract_share_split)
							pract_share_split=0;
						var mnt_flag=a_row_json_data[i_row_json_index].mnth_flag;	
						var i_no_of_resources = a_row_json_data[i_row_json_index].allo;
						var s_mnth_strt_date = '1/31/2017';
						var s_mnth_end_date = '31/31/2017';
						var s_mnth = a_row_json_data[i_row_json_index].mnth;
						var s_year = a_row_json_data[i_row_json_index].year;
						
						var f_revenue_recognized = 0;
						
						if(a_unique_list_practice_sublist.indexOf(a_row_json_data[i_row_json_index].subprac)<0)
						{
							a_unique_list_practice_sublist.push(a_row_json_data[i_row_json_index].subprac);
							a_duplicate_sub_prac_count.push({
														'sub_prac': a_row_json_data[i_row_json_index].subprac,
														'sub_prac_name': a_row_json_data[i_row_json_index].subprac_text,
														'revenue_share': 50000,
														'no_count': 0
														});
									
						}
						
						f_revenue_recognized = find_recognized_revenue(a_revenue_recognized_for_project,i_practice,i_sub_practice,i_role,i_level,s_mnth,s_year,a_subprac_searched_once,a_subprac_searched_once_month,a_subprac_searched_once_year);
						
						if (!f_revenue_recognized) 
							f_revenue_recognized = 0;
						
												
						var s_month_name = s_mnth + '_' + s_year;
						
						var total_revenue = parseFloat(i_no_of_resources) * parseFloat(f_revenue);
						total_revenue = parseFloat(total_revenue).toFixed(2);
						
						
						
						if (i_practice_previous == 0) {
							i_practice_previous = a_row_json_data[i_row_json_index].prac_row;
						}
						
						if (i_practice_previous != a_row_json_data[i_row_json_index].prac_row) {
							f_total_revenue_for_tenure = 0;
							i_practice_previous = a_row_json_data[i_row_json_index].prac_row;
						}
					
						f_total_revenue_for_tenure = parseFloat(f_total_revenue_for_tenure) + parseFloat(total_revenue);
						f_total_revenue_for_tenure = parseFloat(f_total_revenue_for_tenure).toFixed(2);
						
						var i_prac_subPrac_role_level = i_practice + '_' + i_sub_practice + '_' + i_role + '_' + i_level + '_' + i_location;
						
						if (!projectWiseRevenue[i_prac_subPrac_role_level]) {
							projectWiseRevenue[i_prac_subPrac_role_level] = {
								practice_name: s_practice,
								sub_prac_name: s_sub_practice,
								sub_practice: i_sub_practice,
								role_name: s_role,
								level_name: s_level,
								location_name: s_location,
								total_revenue_for_tenure: f_total_revenue_for_tenure,
								//revenue_share: f_revenue_share,
								RevenueData: {}
							};
							
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice] = new yearData(i_year_project);
							
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].Amount = total_revenue;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].prcnt_allocated = i_no_of_resources;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].recognized_revenue = f_revenue_recognized;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].mnth_strt = s_mnth_strt_date;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].mnth_end = s_mnth_end_date;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].revenue_share = f_revenue_share;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].practice_share = pract_share_split;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].f_mnth_flag = mnt_flag;
						}
						else {
							projectWiseRevenue[i_prac_subPrac_role_level].total_revenue_for_tenure = f_total_revenue_for_tenure;
							
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].Amount = total_revenue;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].prcnt_allocated = i_no_of_resources;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].recognized_revenue = f_revenue_recognized;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].mnth_strt = s_mnth_strt_date;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].mnth_end = s_mnth_end_date;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].revenue_share = f_revenue_share;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].practice_share = pract_share_split;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].f_mnth_flag = mnt_flag;
						}
					}
				}
			}
			
						
			
			
			var o_form_obj = nlapiCreateForm('Project Month End Detail View');
			//
			var linkUrl1	= nlapiResolveURL('SUITELET', '1564', 'customdeploy1');
			linkUrl1 = linkUrl1+'&proj_id='+i_projectId+'&mode=Submit&rcrd_id='+i_revenue_share_id+'&month_id='+i_month_end_activity_id+'&revenue_rcrd_status=2';

			o_form_obj.addFieldGroup('custpage_project_export', 'Export');
			var fld_link_to_setup_plan = o_form_obj.addField('link_to_export_plan', 'url','',null,'custpage_project_export').setDisplayType('inline').setLinkText('Export');
			fld_link_to_setup_plan.setDefaultValue(linkUrl1);
			
			//
			o_form_obj.addFieldGroup('custpage_proj_info', 'Project Information');
			
			// create region field
			var fld_region_field = o_form_obj.addField('proj_region', 'select','Region','customrecord_region','custpage_proj_info').setDisplayType('inline');
			fld_region_field.setDefaultValue(s_project_region);
			
			// create customer field
			var fld_customer_field = o_form_obj.addField('customer_selected', 'select','Customer','customer','custpage_proj_info').setDisplayType('inline');
			fld_customer_field.setDefaultValue(i_customer_name);
			
			// create project field
			var fld_proj_field = o_form_obj.addField('proj_selected', 'select','Project','job','custpage_proj_info').setDisplayType('inline');
			fld_proj_field.setDefaultValue(i_projectId);
			
			// fields used for post call
			var fld_revenue_share_id = o_form_obj.addField('revenue_share_id', 'integer','i_revenue_share_id').setDisplayType('hidden');
			fld_revenue_share_id.setDefaultValue(i_revenue_share_id);
			
			var fld_revenue_share_stat = o_form_obj.addField('revenue_share_stat', 'integer','i_revnue_share_status').setDisplayType('hidden');
			fld_revenue_share_stat.setDefaultValue(i_revnue_share_status);
			
			var fld_mnth_end_json_id = o_form_obj.addField('mnth_end_json_id', 'integer','i_month_end_activity_id').setDisplayType('hidden');
			fld_mnth_end_json_id.setDefaultValue(i_month_end_activity_id);
			// end of fields used for post call
			
			// create project sow value field
			var fld_proj_sow_value = o_form_obj.addField('proj_sow_value', 'text','Project SOW value',null,'custpage_proj_info').setDisplayType('inline');
			fld_proj_sow_value.setDefaultValue(i_project_sow_value);
			
			// create field for project start date
			var fld_proj_start_date = o_form_obj.addField('proj_strt_date', 'date','Project Start Date',null,'custpage_proj_info').setDisplayType('inline');
			fld_proj_start_date.setDefaultValue(d_proj_start_date);
			
			// create field for projet end date
			var fld_proj_end_date = o_form_obj.addField('proj_end_date', 'date','Project End Date',null,'custpage_proj_info').setDisplayType('inline');
			fld_proj_end_date.setDefaultValue(d_proj_end_date);
			
			// create field for projet end date
			var fld_proj_end_date = o_form_obj.addField('proj_currency', 'text','Project Currency',null,'custpage_proj_info').setDisplayType('inline');
			fld_proj_end_date.setDefaultValue(s_proj_currency);
			
			// create field for project practice
			var fld_proj_practice = o_form_obj.addField('proj_practice', 'select','Project Executing Practice','department','custpage_proj_info').setDisplayType('inline');
			fld_proj_practice.setDefaultValue(i_proj_executing_practice);
			
			// create field for poject manager practice field
			var fld_proj_manager_practice = o_form_obj.addField('proj_manager_practice', 'select','Project Manager Practice','department','custpage_proj_info').setDisplayType('inline');
			fld_proj_manager_practice.setDefaultValue(i_proj_manager_practice);
			
			// create field for project revenue rec type
			var fld_proj_revRec_type = o_form_obj.addField('rev_rec_type', 'select','Revenue Recognition Type','customlist_fp_rev_rec_project_type','custpage_proj_info').setDisplayType('inline');
			fld_proj_revRec_type.setDefaultValue(i_proj_revenue_rec_type);		
			o_form_obj.addFieldGroup('custpage_total_view', 'Overall P&L Summary');
			for (var i_dupli = 0; i_dupli < a_duplicate_sub_prac_count.length; i_dupli++)
			{
				//var f_prev_mnth_cost = generate_cost_view_prev_mnth(projectWiseRevenue_previous_effrt,monthBreakUp,i_year_project,i_project_mnth,2,a_duplicate_sub_prac_count[i_dupli].sub_prac,s_currency_symbol_proj,s_currency_symbol_usd,i_year_project_end,i_prject_end_mnth);
				//nlapiLogExecution('audit','cost for prev mnth:- '+f_prev_mnth_cost,'sub prac:- '+a_duplicate_sub_prac_count[i_dupli].sub_prac);				
				var f_total_cost_breakUp = generate_total_cost(projectWiseRevenue,monthBreakUp,i_year_project,i_project_mnth,2,a_duplicate_sub_prac_count[i_dupli].sub_prac,s_currency_symbol_proj,s_currency_symbol_usd,i_year_project_end,i_prject_end_mnth,fld_true_up_msg);
				var f_total_cost_breakUp_plan = generate_total_cost_plan(projectAllocationPlan,monthBreakUp,i_year_project,i_project_mnth,2,a_duplicate_sub_prac_count[i_dupli].sub_prac,s_currency_symbol_proj,s_currency_symbol_usd,i_year_project_end,i_prject_end_mnth,fld_true_up_msg);
			}
			f_current_month_actual_revenue_total_proj_level = 0;
			a_recognized_revenue_total_proj_level = new Array();
			// Effort View table
			for (var i_dupli = 0; i_dupli < a_duplicate_sub_prac_count.length; i_dupli++)
			{
				//var f_prev_mnth_cost = generate_cost_view_prev_mnth(projectWiseRevenue_previous_effrt,monthBreakUp,i_year_project,i_project_mnth,2,a_duplicate_sub_prac_count[i_dupli].sub_prac,s_currency_symbol_proj,s_currency_symbol_usd,i_year_project_end,i_prject_end_mnth);
				//nlapiLogExecution('audit','cost for prev mnth:- '+f_prev_mnth_cost,'sub prac:- '+a_duplicate_sub_prac_count[i_dupli].sub_prac);
				
				o_form_obj.addFieldGroup('custpage_cost_view_'+i_dupli, ''+a_duplicate_sub_prac_count[i_dupli].sub_prac_name);
				var fld_true_up_msg = o_form_obj.addField('true_up_msg'+i_dupli, 'inlinehtml', null, null, 'custpage_cost_view_'+i_dupli).setLayoutType('outsideabove','startrow');
				var h_tableHtml_effort_view = generate_cost_view(projectWiseRevenue,monthBreakUp,i_year_project,i_project_mnth,2,a_duplicate_sub_prac_count[i_dupli].sub_prac,s_currency_symbol_proj,s_currency_symbol_usd,i_year_project_end,i_prject_end_mnth,fld_true_up_msg,f_total_cost_breakUp,projectAllocationPlan,f_total_cost_breakUp_plan);
				o_form_obj.addField('cost_view_'+i_dupli, 'inlinehtml', null, null, 'custpage_cost_view_'+i_dupli).setDefaultValue(h_tableHtml_effort_view);
			}
			var h_tableHtml_total_margin_view = generate_total_net_margin_view(net_cost_plan,net_cost_effort,net_revenue_plan,net_revenue_effort);
			
			o_form_obj.addField('total_view', 'inlinehtml', null, null,'custpage_total_view').setDefaultValue(h_tableHtml_total_margin_view);
			if(s_request_mode != 'Submit')
			{
				o_form_obj.addSubmitButton('Confirm');
			}
			
			response.writePage(o_form_obj);
		}
		
		nlapiLogExecution('audit','remaining usage:- ',nlapiGetContext().getRemainingUsage());
	}
	catch(err)
	{
		nlapiLogExecution('ERROR','suitelet_month_end_confirmation','ERROR MESSAGE:- '+err);
	}
}

function generate_cost_view_prev_mnth(projectWiseRevenue_previous_effrt,monthBreakUp,i_year_project,i_project_mnth,mode,sub_prac,s_currency_symbol_proj,s_currency_symbol_usd,i_year_project_end,i_prject_end_mnth)
{
	
	
	var today = new Date();
	var currentMonthName = getMonthName(nlapiDateToString(today));
	
	
	var a_sub_prac_already_displayed = new Array();
	var i_subpractice_id = 0;
	var a_amount = new Array();
	var a_recognized_revenue = new Array();
	var i_frst_row = 0;
	var i_diplay_frst_column = 0;
	var i_total_row_revenue = 0;
	var i_total_per_row = 0;
	var total_prcnt_aloocated = new Array();
	var b_first_line_copied = 0;
	var f_total_allocated_row = 0;
	
	for (var emp_internal_id in projectWiseRevenue_previous_effrt)
	{
		if (projectWiseRevenue_previous_effrt[emp_internal_id].sub_practice == sub_prac)
		{
			f_total_allocated_row = 0;
			for (var project_internal_id in projectWiseRevenue_previous_effrt[emp_internal_id].RevenueData)
			{
				var i_index_mnth = 0;
				for (var month in projectWiseRevenue_previous_effrt[emp_internal_id].RevenueData[project_internal_id])
				{
					var a_MonthNames = MonthNames(i_year_project);
					var currentMonthPos = a_MonthNames.indexOf(month);
					var s_current_month = a_MonthNames[currentMonthPos];
					var s_current_mnth_yr = s_current_month.substr(4, 8);
					
					var i_nxt_yr_flg = 0;
					if(currentMonthPos >=12)
					{
						currentMonthPos = currentMonthPos - 12;
						i_nxt_yr_flg = 1;
					}
					
					var i_curr_yr_dis_flag = 0;
					if(parseInt(s_current_mnth_yr) < i_year_project_end && i_prject_end_mnth < currentMonthPos)
					{
						i_curr_yr_dis_flag = 1;
					}
					
					if (((i_project_mnth <= currentMonthPos && ((i_prject_end_mnth >= currentMonthPos || i_curr_yr_dis_flag == 1) && i_nxt_yr_flg == 0) && parseInt(s_current_mnth_yr) <= i_year_project) && parseInt(s_current_mnth_yr) <= i_year_project_end) || (i_prject_end_mnth >= currentMonthPos && parseInt(s_current_mnth_yr) <= i_year_project_end && i_nxt_yr_flg == 1))
					{
						//nlapiLogExecution('audit','currentMonthPos:- ',+currentMonthPos);
						if(b_first_line_copied == 1)
						{
							var f_allocated_prcnt = total_prcnt_aloocated[i_index_mnth];
							f_allocated_prcnt = parseFloat(f_allocated_prcnt) + parseFloat(projectWiseRevenue_previous_effrt[emp_internal_id].RevenueData[project_internal_id][month].prcnt_allocated);
							total_prcnt_aloocated[i_index_mnth] = f_allocated_prcnt;
							i_index_mnth++;							
						}
						else
						{
							total_prcnt_aloocated.push(projectWiseRevenue_previous_effrt[emp_internal_id].RevenueData[project_internal_id][month].prcnt_allocated);
						}
						
						f_total_allocated_row = parseFloat(projectWiseRevenue_previous_effrt[emp_internal_id].RevenueData[project_internal_id][month].prcnt_allocated) + parseFloat(f_total_allocated_row);
						
					}
				}
			}
			
			b_first_line_copied = 1;
			
			var s_sub_prac_name = projectWiseRevenue_previous_effrt[emp_internal_id].sub_prac_name;
			var s_practice_name = projectWiseRevenue_previous_effrt[emp_internal_id].practice_name;
			
			i_diplay_frst_column = 1;
		}
	}
	i_diplay_frst_column = 0;
	
	
	
	var f_total_row_allocation = 0;
	for(var i_index_total_allocated=0; i_index_total_allocated<total_prcnt_aloocated.length; i_index_total_allocated++)
	{
		f_total_row_allocation = parseFloat(f_total_row_allocation) + parseFloat(total_prcnt_aloocated[i_index_total_allocated]);
	}
	
	for ( var emp_internal_id in projectWiseRevenue_previous_effrt) {
		
		for ( var project_internal_id in projectWiseRevenue_previous_effrt[emp_internal_id].RevenueData) {
			
			if(projectWiseRevenue_previous_effrt[emp_internal_id].sub_practice == sub_prac)
			{
				
				var i_sub_practice_internal_id = 0;
				var i_amt = 0;
				for (var month in projectWiseRevenue_previous_effrt[emp_internal_id].RevenueData[project_internal_id])
				{
					var a_MonthNames = MonthNames(i_year_project);
					var currentMonthPos = a_MonthNames.indexOf(month);
					var s_current_month = a_MonthNames[currentMonthPos];
					var s_current_mnth_yr = s_current_month.substr(4, 8);
					
					var i_nxt_yr_flg = 0;
					if(currentMonthPos >=12)
					{
						currentMonthPos = currentMonthPos - 12;
						i_nxt_yr_flg = 1;
					}
					
					var i_curr_yr_dis_flag = 0;
					if(parseInt(s_current_mnth_yr) < i_year_project_end && i_prject_end_mnth < currentMonthPos)
					{
						i_curr_yr_dis_flag = 1;
					}
					
					if (((i_project_mnth <= currentMonthPos && ((i_prject_end_mnth >= currentMonthPos || i_curr_yr_dis_flag == 1) && i_nxt_yr_flg == 0) && parseInt(s_current_mnth_yr) <= i_year_project) && parseInt(s_current_mnth_yr) <= i_year_project_end) || (i_prject_end_mnth >= currentMonthPos && parseInt(s_current_mnth_yr) <= i_year_project_end && i_nxt_yr_flg == 1))
					{
						if (mode == 2)
						{
							
							
							var total_revenue_format = parseFloat(projectWiseRevenue_previous_effrt[emp_internal_id].RevenueData[project_internal_id][month].Amount).toFixed(1);
							
							
							if(i_frst_row == 0)
							{
								//nlapiLogExecution('audit','inside push amnt');
								a_amount.push(total_revenue_format);
								a_recognized_revenue.push(projectWiseRevenue_previous_effrt[emp_internal_id].RevenueData[project_internal_id][month].recognized_revenue);
							}
							else
							{
								var amnt_mnth = a_amount[i_amt];
								amnt_mnth = parseFloat(amnt_mnth) + parseFloat(projectWiseRevenue_previous_effrt[emp_internal_id].RevenueData[project_internal_id][month].Amount);
								
								a_amount[i_amt] = amnt_mnth;
								
								var i_existing_recognised_amount = a_recognized_revenue[i_amt];
								i_existing_recognised_amount = parseFloat(i_existing_recognised_amount) + parseFloat(projectWiseRevenue_previous_effrt[emp_internal_id].RevenueData[project_internal_id][month].recognized_revenue);
								
								a_recognized_revenue[i_amt] = i_existing_recognised_amount;
								
								i_amt++;
							}
							i_total_row_revenue = parseFloat(i_total_row_revenue) + parseFloat(projectWiseRevenue_previous_effrt[emp_internal_id].RevenueData[project_internal_id][month].Amount);
							i_total_per_row = parseFloat(i_total_per_row) + parseFloat(projectWiseRevenue_previous_effrt[emp_internal_id].RevenueData[project_internal_id][month].Amount);
						}
						
						
					}
				}
				
				if (mode == 2)
				{
					i_total_per_row = 0;
					//i_total_row_revenue = parseFloat(i_total_row_revenue) + parseFloat(projectWiseRevenue[emp_internal_id].total_revenue_for_tenure);
				}
				
				i_frst_row = 1;
	
				i_diplay_frst_column = 1;
				
				var f_revenue_share = projectWiseRevenue_previous_effrt[emp_internal_id].revenue_share;
			}			
		}
	}
	
	var s_sub_prac_name = '';
	for (var emp_internal_id in projectWiseRevenue_previous_effrt)
	{
		if (projectWiseRevenue_previous_effrt[emp_internal_id].sub_practice == sub_prac)
		{
			s_sub_prac_name = projectWiseRevenue_previous_effrt[emp_internal_id].sub_prac_name;
			s_practice_name = projectWiseRevenue_previous_effrt[emp_internal_id].practice_name;
			//projectWiseRevenue[emp_internal_id].revenue_share_revised = i_total_row_revenue;
			//projectWiseRevenue[emp_internal_id].total_revenue_for_tenure = i_total_row_revenue;
		}
	}
	
	
	for (var i_amt = 0; i_amt < a_amount.length; i_amt++)
	{
		
	}
	
	
	// Percent row
	
	var f_total_prcnt = 0;
	for (var i_amt = 0; i_amt < a_amount.length; i_amt++)
	{
		var f_prcnt_revenue = parseFloat(a_amount[i_amt]) / parseFloat(i_total_row_revenue);
		f_prcnt_revenue = f_prcnt_revenue * 100;
		if(!f_prcnt_revenue)
			f_prcnt_revenue = 0;
			
		f_total_prcnt = parseFloat(f_prcnt_revenue) + parseFloat(f_total_prcnt);
		
	}
	
	//Total reveue recognized per month row
	
	var d_today_date = new Date();
	var i_current_month = d_today_date.getMonth();
	var i_current_year = d_today_date.getFullYear();
	
	var f_total_revenue = 0;
	var f_revenue_amount_till_current_month = 0;
	for (var i_amt = 0; i_amt < a_amount.length; i_amt++)
	{
		var f_prcnt_revenue = parseFloat(a_amount[i_amt]) / parseFloat(i_total_row_revenue);
		f_prcnt_revenue = f_prcnt_revenue * 100;
		
		var f_revenue_amount = (parseFloat(f_prcnt_revenue) * parseFloat(f_revenue_share)) / 100;
		if(!f_revenue_amount)
			f_revenue_amount = 0;
		
		if(i_amt <= i_current_month)
		{
			//nlapiLogExecution('audit','i_current_month:- '+i_current_month,'a_amount.length:- '+a_amount.length);
			f_revenue_amount_till_current_month = parseFloat(f_revenue_amount) + parseFloat(f_revenue_amount_till_current_month);
			if(!f_revenue_amount_till_current_month)
				f_revenue_amount_till_current_month = 0;
		}
		
		f_total_revenue = parseFloat(f_revenue_amount) + parseFloat(f_total_revenue);
		
	}
	
	for (var emp_internal_id in projectWiseRevenue_previous_effrt)
	{
		for (var project_internal_id in projectWiseRevenue_previous_effrt[emp_internal_id].RevenueData)
		{
			var i_amount_map = 0;
			if (projectWiseRevenue_previous_effrt[emp_internal_id].sub_practice == sub_prac)
			{
				for (var month in projectWiseRevenue_previous_effrt[emp_internal_id].RevenueData[project_internal_id])
				{
					var a_MonthNames = MonthNames(i_year_project);
					var currentMonthPos = a_MonthNames.indexOf(month);
					var s_current_month = a_MonthNames[currentMonthPos];
					var s_current_mnth_yr = s_current_month.substr(4, 8);
					
					var i_nxt_yr_flg = 0;
					if(currentMonthPos >=12)
					{
						currentMonthPos = currentMonthPos - 12;
						i_nxt_yr_flg = 1;
					}
					
					var i_curr_yr_dis_flag = 0;
					if(parseInt(s_current_mnth_yr) < i_year_project_end && i_prject_end_mnth < currentMonthPos)
					{
						i_curr_yr_dis_flag = 1;
					}
					
					if (((i_project_mnth <= currentMonthPos && ((i_prject_end_mnth >= currentMonthPos || i_curr_yr_dis_flag == 1) && i_nxt_yr_flg == 0) && parseInt(s_current_mnth_yr) <= i_year_project) && parseInt(s_current_mnth_yr) <= i_year_project_end) || (i_prject_end_mnth >= currentMonthPos && parseInt(s_current_mnth_yr) <= i_year_project_end && i_nxt_yr_flg == 1))
					{
						var f_prcnt_revenue = parseFloat(a_amount[i_amount_map]) / parseFloat(i_total_row_revenue);
						f_prcnt_revenue = f_prcnt_revenue * 100;
						
						var f_revenue_amount = (parseFloat(f_prcnt_revenue) * parseFloat(f_revenue_share)) / 100;
		
						projectWiseRevenue_previous_effrt[emp_internal_id].RevenueData[project_internal_id][month].total_revenue = f_revenue_amount;
						projectWiseRevenue_previous_effrt[emp_internal_id].RevenueData[project_internal_id][month].prcent_complt = f_prcnt_revenue;
						i_amount_map++;
					}
				}	
			}
		}
	}
	
	var today = new Date();
	var currentMonthName = getMonthName(nlapiDateToString(today));
	
	var i_total_revenue_recognized_ytd = 0;
	for(var i_revenue_index=0; i_revenue_index<a_recognized_revenue.length; i_revenue_index++)
	{
		i_total_revenue_recognized_ytd = parseFloat(i_total_revenue_recognized_ytd) + parseFloat(a_recognized_revenue[i_revenue_index]);
		
	}
	
	//Actual revenue to be recognized 
	
	if(parseInt(i_year_project) != parseInt(i_current_year))
	{
		var i_total_project_tenure = 11 + parseInt(i_current_month);
		var f_total_prev_mnths = parseFloat(i_total_project_tenure) - parseFloat(i_project_mnth);
		i_current_month = parseFloat(f_total_prev_mnths) + parseFloat(1);
	}
	else
	{
		i_current_month = parseFloat(i_current_month) - parseFloat(i_project_mnth);
	}
	
	var f_current_month_actual_revenue = parseFloat(f_revenue_amount_till_current_month) - parseFloat(i_total_revenue_recognized_ytd);
	//var i_total_revenue_recognized_ytd = 0;
	
	//nlapiLogExecution('audit','i_current_month:- '+i_current_month);
	for(var i_revenue_index=0; i_revenue_index<=i_current_month-1; i_revenue_index++)
	{
		//nlapiLogExecution('audit','f_current_month_actual_revenue:- '+f_current_month_actual_revenue,'f_revenue_amount_till_current_month:- '+f_revenue_amount_till_current_month);
		//i_total_revenue_recognized_ytd = parseFloat(i_total_revenue_recognized_ytd) + parseFloat(a_recognized_revenue[i_revenue_index]);
		
	}
	
	//f_current_month_actual_revenue_total_proj_level = parseFloat(f_current_month_actual_revenue_total_proj_level) + parseFloat(f_current_month_actual_revenue);
	
	var i_total_actual_revenue_recognized = parseFloat(i_total_revenue_recognized_ytd)+ parseFloat(f_current_month_actual_revenue);
	
	/*for(var i_revenue_index=i_current_month; i_revenue_index<a_recognized_revenue.length; i_revenue_index++)
	{
		html += "<td class='projected-amount'>";
		html += '<b>';
		html += "</td>";
	}*/
	
	
	{
		var f_prcnt_revenue = parseFloat(a_amount[i_current_month]) / parseFloat(i_total_row_revenue);
		f_prcnt_revenue = f_prcnt_revenue * 100;
		
		var f_revenue_amount = (parseFloat(f_prcnt_revenue) * parseFloat(f_revenue_share)) / 100;
		if(!f_revenue_amount)
			f_revenue_amount = 0;
			
		return f_revenue_amount;
		
		if(i_amt < i_current_month)
		{
			//nlapiLogExecution('audit','i_current_month:- '+i_current_month,'a_amount.length:- '+a_amount.length);
			f_revenue_amount_till_current_month = parseFloat(f_revenue_amount) + parseFloat(f_revenue_amount_till_current_month);
			if(!f_revenue_amount_till_current_month)
				f_revenue_amount_till_current_month = 0;
		}
		
		f_total_revenue = parseFloat(f_revenue_amount) + parseFloat(f_total_revenue);
		
		i_total_actual_revenue_recognized = parseFloat(i_total_actual_revenue_recognized) + parseFloat(f_revenue_amount);
	}
	
	//Total revenue recognized for previous months
	
	
	for(var i_exist_index=0; i_exist_index<=a_recognized_revenue.length; i_exist_index++)
	{
		if(a_recognized_revenue_total_proj_level[i_exist_index])
		{
			var f_existing_cost_in_arr = parseFloat(a_recognized_revenue_total_proj_level[i_exist_index]);
		}
		else
		{
			var f_existing_cost_in_arr = 0;
		}
		
		var f_existing_cost = a_recognized_revenue[i_exist_index];
		f_existing_cost = parseFloat(f_existing_cost) + parseFloat(f_existing_cost_in_arr);
		
		a_recognized_revenue_total_proj_level[i_exist_index] = f_existing_cost;
	}
	//a_recognized_revenue_total_proj_level = a_recognized_revenue;
	
	//return html;
}
function generate_total_net_margin_view(net_cost_plan,net_cost_effort,net_revenue_plan,net_revenue_effort)
{
	var css_file_url = "";

	var context = nlapiGetContext();
	if (context.getEnvironment() == "SANDBOX") {
		css_file_url = "https://system.sandbox.netsuite.com/core/media/media.nl?id=199133&c=3883006&h=64fa194295cd4ab64d58&mv=iq3kjr6g&_xt=.css&whence=";
	} else if (context.getEnvironment() == "PRODUCTION") {
		css_file_url = "https://system.na1.netsuite.com/core/media/media.nl?id=199133&c=3883006&h=64fa194295cd4ab64d58&mv=iq3kjr6g&_xt=.css&whence=";
	}
	var html = "";
	html += "<link href='" + css_file_url + "'  rel='stylesheet'>";

	html += "<table class='projection-table'>";

	// header
	html += "<tr class='header-row'>";
	html += "<td width:15px>";
	html += " ";
	html += "</td>";
	html += "<td width:15px>";
	html += " BaseLine (Revenue)";
	html += "</td>";
	html += "<td width:15px>";
	html += "Latest (Revenue)";
	html += "</td>";
	html += "</td>";
	html += "<td width:15px>";
	html += "BaseLine (Cost)";
	html += "</td>";
	html += "</td>";
	html += "<td width:15px>";
	html += "Latest (Cost)";
	html += "</td>";
	html += "</td>";
	html += "<td width:15px>";
	html += "BaseLine(Margin)%";
	html += "</td>";
	html += "</td>";
	html += "<td width:15px>";
	html += "Latest (Margin)%";
	html += "</td>";

	html += "</tr>";
	for(var total_ind=0;total_ind<over_all_array.length;total_ind++)
	{
		html += "<tr>";
		html += "<td class='label-name' >";
		html += over_all_array[total_ind].sub_practice;
		html += "</td>";
		var plan_revenue=over_all_array[total_ind].plan_revenue;
		html += "<td class='monthly-amount' width:15px>";
		html += format2(plan_revenue);
		html += "</td>";
		html += "<td class='monthly-amount' width:15px>";
		html += format2(over_all_array[total_ind].current_revenue);
		html += "</td>";
		html += "<td class='monthly-amount' width:15px>";
		html += format2(over_all_array[total_ind].plan_cost);
		html += "</td>";
		html += "<td class='monthly-amount' width:15px>";
		html += format2(over_all_array[total_ind].current_cost);
		html += "</td>";
		html += "<td class='monthly-amount' width:15px>";
		html += over_all_array[total_ind].plan_margin;
		html += "</td>";
		html += "<td class='monthly-amount' width:15px>";
		html += over_all_array[total_ind].current_margin;
		html += "</td>";
		html += "</tr>";
	}
	
	html += "<tr>";
	html += "<td class='label-name' width:15px>";
	html += "<b>Overall";
	html += "</td>";
	html += "<td class='projected-amount' width:15px>";
	html += format2(net_revenue_plan);
	html += "</td>";
	html += "<td class='projected-amount' width:15px>";
	html += format2(net_revenue_effort);
	html += "</td>";
	html += "<td class='projected-amount' width:15px>";
	html += format2(net_cost_plan);
	html += "</td>";
	html += "<td class='projected-amount' width:15px>";
	html += format2(net_cost_effort);
	html += "</td>";
	if(net_revenue_plan!=0)
	{
		var net_margin_plan=(net_revenue_plan-net_cost_plan)/(net_revenue_plan);
		net_margin_plan=net_margin_plan*100;
	}
	else
	{
		var net_margin_plan=0;
	}

	html += "</td>";
	html += "<td class='projected-amount' width:15px>";
	html +=parseFloat(net_margin_plan).toFixed(1)+' %';
	html += "</td>";
	if(net_revenue_effort!=0)
	{
		var net_margin_effort=(net_revenue_effort-net_cost_effort)/net_revenue_effort;
		net_margin_effort=net_margin_effort*100;
	}
	else
	{
		var net_margin_effort=0;
	}
	html += "</td>";
	html += "<td class='projected-amount' width:15px>";
	html += parseFloat(net_margin_effort).toFixed(1)+' %';
	html += "</td>";
	html += "</tr>";
	html += "</table>";
	 
	 return html;
}
function generate_cost_view(projectWiseRevenue,monthBreakUp,i_year_project,i_project_mnth,mode,sub_prac,s_currency_symbol_proj,s_currency_symbol_usd,i_year_project_end,i_prject_end_mnth,fld_true_up_msg,f_total_cost_breakUp,projectAllocationPlan,f_total_cost_breakUp_plan)
{
	var css_file_url = "";

	var context = nlapiGetContext();
	if (context.getEnvironment() == "SANDBOX") {
		css_file_url = "https://system.sandbox.netsuite.com/core/media/media.nl?id=199133&c=3883006&h=64fa194295cd4ab64d58&mv=iq3kjr6g&_xt=.css&whence=";
	} else if (context.getEnvironment() == "PRODUCTION") {
		css_file_url = "https://system.na1.netsuite.com/core/media/media.nl?id=199133&c=3883006&h=64fa194295cd4ab64d58&mv=iq3kjr6g&_xt=.css&whence=";
	}

	var html = "";
	html += "<link href='" + css_file_url + "'  rel='stylesheet'>";

	html += "<table class='projection-table'>";

	// header
	html += "<tr class='header-row'>";
	
	
	/*html += "<td width:15px>";
	html += "Practice";
	html += "</td>";*/
	
	html += "<td width:30px>";
	html += "Practice/Sub Practice";
	html += "</td>";
	
	/*html += "<td width=\"10%\">";
	html += "Role";
	html += "</td>";*/
	
	html += "<td width=\"10%\">";
	html += "Level";
	html += "</td>";
	
	html += "<td>";
	html += "Location";
	html += "</td>";
		
	var a_proj_prac_details = new Array();
	var today = new Date();
	var currentMonthName = getMonthName(nlapiDateToString(today));
	
	/*for (var j = 0; j < monthBreakUp.length; j++)
	{
		var months = monthBreakUp[j];
		var s_month_name = getMonthName(months.Start); // get month name
		var s_year = nlapiStringToDate(months.Start)
		s_year = s_year.getFullYear();
		s_month_name = s_month_name+'_'+s_year;
				
		html += "<td>";
		html += s_month_name;
		html += "</td>";
	}*/
		
	html += "<td>";
	html += "BaseLine(FTE)";
	html += "</td>";
	
	html += "<td>";
	html += "Latest(FTE)";
	html += "</td>";
	
	html += "<td>";
	html += "BaseLine (Cost)";
	html += "</td>";
	
	html += "<td>";
	html += "Latest (Cost)";
	html += "</td>";
	
	html += "</tr>";
	
	var a_sub_prac_already_displayed = new Array();
	var i_subpractice_id = 0;
	var a_amount = new Array();
	var a_recognized_revenue = new Array();
	var a_recognized_revenue_plan= new Array();
	var i_frst_row = 0;
	var i_first_row_plan=0;
	var a_amount_plan=new Array();
	var i_diplay_frst_column = 0;
	var i_diplay_frst_column_plan = 0;
	var i_total_row_revenue = 0;
	var i_total_row_revenue_plan = 0;
	var i_total_per_row = 0;
	var i_total_per_row_plan = 0;
	var total_prcnt_aloocated = new Array();
	var total_prcnt_alocated_plan=new Array();
	var b_first_line_copied = 0;
	var b_first_line_copied_plan = 0;
	var f_total_allocated_row = 0;
	var revenue_share_arr=new Array();
	var practice_share_arr=new Array();
	var mnth_flag_arr=new Array();
	var revenue_share_arr_plan=new Array();
	var practice_share_arr_plan=new Array();
	var mnth_flag_arr_plan=new Array();
	var margin_rev=new Array();
	var margin_rev_plan=new Array();
	var effort=0;
	for (var emp_internal_id_plan in projectAllocationPlan)
	{
		if (projectAllocationPlan[emp_internal_id_plan].sub_practice == sub_prac)
		{
			f_total_allocated_row = 0;
			for (var project_internal_id in projectAllocationPlan[emp_internal_id_plan].RevenueDataPlan)
			{
				projectAllocationPlan[emp_internal_id_plan].sub_practice;				
				var i_index_mnth = 0;
				for (var month in projectAllocationPlan[emp_internal_id_plan].RevenueDataPlan[project_internal_id])
				{
					var a_MonthNames = MonthNames(i_year_project);
					var currentMonthPos = a_MonthNames.indexOf(month);
					var s_current_month = a_MonthNames[currentMonthPos];
					var s_current_mnth_yr = s_current_month.substr(4, 8);
					
					var i_nxt_yr_flg = 0;
					if(currentMonthPos >=12)
					{
						currentMonthPos = currentMonthPos - 12;
						i_nxt_yr_flg = 1;
					}
					
					var i_curr_yr_dis_flag = 0;
					if(parseInt(s_current_mnth_yr) < i_year_project_end && i_prject_end_mnth < currentMonthPos)
					{
						i_curr_yr_dis_flag = 1;
					}
					
					if (((i_project_mnth <= currentMonthPos && ((i_prject_end_mnth >= currentMonthPos || i_curr_yr_dis_flag == 1) && i_nxt_yr_flg == 0) && parseInt(s_current_mnth_yr) <= i_year_project) && parseInt(s_current_mnth_yr) <= i_year_project_end) || (i_prject_end_mnth >= currentMonthPos && parseInt(s_current_mnth_yr) <= i_year_project_end && i_nxt_yr_flg == 1))
					{
						//nlapiLogExecution('audit','currentMonthPos:- ',+currentMonthPos);
						if(b_first_line_copied_plan == 1)
						{
							var f_allocated_prcnt = total_prcnt_alocated_plan[i_index_mnth];
							//var f_allocated_prcnt = total_prcnt_aloocated[i_index_mnth];
							f_allocated_prcnt = parseFloat(f_allocated_prcnt) + parseFloat(projectAllocationPlan[emp_internal_id_plan].RevenueDataPlan[project_internal_id][month].prcnt_allocated);
							total_prcnt_alocated_plan[i_index_mnth] = f_allocated_prcnt;
							i_index_mnth++;							
						}
						else
						{
							total_prcnt_alocated_plan.push(projectAllocationPlan[emp_internal_id_plan].RevenueDataPlan[project_internal_id][month].prcnt_allocated);
						}
						
						f_total_allocated_row = parseFloat(projectAllocationPlan[emp_internal_id_plan].RevenueDataPlan[project_internal_id][month].prcnt_allocated) + parseFloat(f_total_allocated_row);
						var i_is_practice_alreday_present = findAllocationPracticePresent(a_proj_prac_details,projectAllocationPlan[emp_internal_id_plan].level_name, projectAllocationPlan[emp_internal_id_plan].location_name);
						if(i_is_practice_alreday_present >= 0)
						{
									var f_already_alloc_prcnt = a_proj_prac_details[i_is_practice_alreday_present].f_prcnt_allocated;
									f_already_alloc_prcnt = parseFloat(f_already_alloc_prcnt) + parseFloat(projectAllocationPlan[emp_internal_id_plan].RevenueDataPlan[project_internal_id][month].prcnt_allocated);
									f_already_alloc_prcnt = parseFloat(f_already_alloc_prcnt).toFixed(2);
									
									//nlapiLogExecution('audit','already present prcnt:- '+a_proj_prac_details[i_is_practice_alreday_present].f_prcnt_allocated,'f_already_alloc_prcnt:- '+f_already_alloc_prcnt);
									a_proj_prac_details[i_is_practice_alreday_present] = {
															'i_sub_practice': projectAllocationPlan[emp_internal_id_plan].sub_prac_name,
															'i_practice': projectAllocationPlan[emp_internal_id_plan].practice_name,
															'f_prcnt_allocated': f_already_alloc_prcnt,
															's_emp_level':  projectAllocationPlan[emp_internal_id_plan].level_name,
															'i_resource_location': projectAllocationPlan[emp_internal_id_plan].location_name,
															//'subsidiary': a_project_allocation_result[i_prac_count].getValue('subsidiary','employee','group'),
															//'emp_role':a_project_allocation_result[i_prac_count].getValue('title','employee','group'),
															'actual_effort': 0
														};
						}
						else
						{
							
							a_proj_prac_details.push(
							{
										'i_sub_practice':projectAllocationPlan[emp_internal_id_plan].sub_prac_name,
										'i_practice': projectAllocationPlan[emp_internal_id_plan].practice_name,
										'f_prcnt_allocated': parseFloat(projectAllocationPlan[emp_internal_id_plan].RevenueDataPlan[project_internal_id][month].prcnt_allocated),
										's_emp_level': projectAllocationPlan[emp_internal_id_plan].level_name,
										'i_resource_location': projectAllocationPlan[emp_internal_id_plan].location_name,
										//'subsidiary': a_project_allocation_result[i_prac_count].getValue('subsidiary','employee','group'),
										//'emp_role':a_project_allocation_result[i_prac_count].getValue('title','employee','group'),
										'actual_effort':0
							});
						}
						//html += "<td class='monthly-amount'>";
						//html += parseFloat(projectAllocationPlan[emp_internal_id_plan].RevenueDataPlan[project_internal_id][month].prcnt_allocated).toFixed(2);
						//html += "</td>";
					}
				}
			}
			
			b_first_line_copied_plan = 1;
			
			//html += "<td class='monthly-amount'>";
			//html += parseFloat(f_total_allocated_row).toFixed(2);
			//html += "</td>";			
			var s_sub_prac_name = projectAllocationPlan[emp_internal_id_plan].sub_prac_name;
			var s_practice_name = projectAllocationPlan[emp_internal_id_plan].practice_name;
			
			i_diplay_frst_column_plan = 1;
		}
	}
	if(total_prcnt_alocated_plan.length>0)
	{
		

	}
	
	//Effort as per allocation
	for (var emp_internal_id in projectWiseRevenue)
	{
		if (projectWiseRevenue[emp_internal_id].sub_practice == sub_prac)
		{
			f_total_allocated_row = 0;
			for (var project_internal_id in projectWiseRevenue[emp_internal_id].RevenueData)
			{
				
				
				var i_index_mnth = 0;
				for (var month in projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id])
				{
					var a_MonthNames = MonthNames(i_year_project);
					var currentMonthPos = a_MonthNames.indexOf(month);
					var s_current_month = a_MonthNames[currentMonthPos];
					var s_current_mnth_yr = s_current_month.substr(4, 8);
					
					var i_nxt_yr_flg = 0;
					if(currentMonthPos >=12)
					{
						currentMonthPos = currentMonthPos - 12;
						i_nxt_yr_flg = 1;
					}
					
					var i_curr_yr_dis_flag = 0;
					if(parseInt(s_current_mnth_yr) < i_year_project_end && i_prject_end_mnth < currentMonthPos)
					{
						i_curr_yr_dis_flag = 1;
					}
					
					if (((i_project_mnth <= currentMonthPos && ((i_prject_end_mnth >= currentMonthPos || i_curr_yr_dis_flag == 1) && i_nxt_yr_flg == 0) && parseInt(s_current_mnth_yr) <= i_year_project) && parseInt(s_current_mnth_yr) <= i_year_project_end) || (i_prject_end_mnth >= currentMonthPos && parseInt(s_current_mnth_yr) <= i_year_project_end && i_nxt_yr_flg == 1))
					{
						//nlapiLogExecution('audit','currentMonthPos:- ',+currentMonthPos);
						if(b_first_line_copied == 1)
						{
							var f_allocated_prcnt = total_prcnt_aloocated[i_index_mnth];
							f_allocated_prcnt = parseFloat(f_allocated_prcnt) + parseFloat(projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id][month].prcnt_allocated);
							total_prcnt_aloocated[i_index_mnth] = f_allocated_prcnt;
							i_index_mnth++;							
						}
						else
						{
							total_prcnt_aloocated.push(projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id][month].prcnt_allocated);
						}
						
						f_total_allocated_row = parseFloat(projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id][month].prcnt_allocated) + parseFloat(f_total_allocated_row);
						var i_is_practice_alreday_present = findAllocationPracticePresent(a_proj_prac_details,projectWiseRevenue[emp_internal_id].level_name, projectWiseRevenue[emp_internal_id].location_name);
						if(i_is_practice_alreday_present >= 0)
						{
									var effort_prcnt=a_proj_prac_details[i_is_practice_alreday_present].f_prcnt_allocated;
									var f_already_alloc_prcnt = a_proj_prac_details[i_is_practice_alreday_present].actual_effort;
									f_already_alloc_prcnt = parseFloat(f_already_alloc_prcnt) +parseFloat(projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id][month].prcnt_allocated);
									f_already_alloc_prcnt = parseFloat(f_already_alloc_prcnt).toFixed(2);
									
									//nlapiLogExecution('audit','already present prcnt:- '+a_proj_prac_details[i_is_practice_alreday_present].f_prcnt_allocated,'f_already_alloc_prcnt:- '+f_already_alloc_prcnt);
									a_proj_prac_details[i_is_practice_alreday_present] = {
															'i_sub_practice': projectWiseRevenue[emp_internal_id].sub_prac_name,
															'i_practice': projectWiseRevenue[emp_internal_id].practice_name,
															'f_prcnt_allocated': effort_prcnt,
															's_emp_level':  projectWiseRevenue[emp_internal_id].level_name,
															'i_resource_location':projectWiseRevenue[emp_internal_id].location_name,
															//'subsidiary': a_project_allocation_result[i_prac_count].getValue('subsidiary','employee','group'),
															//'emp_role':a_project_allocation_result[i_prac_count].getValue('title','employee','group'),
															'actual_effort': f_already_alloc_prcnt
														};
						}
						else
						{
							
							a_proj_prac_details.push(
							{
										
										'i_sub_practice':projectWiseRevenue[emp_internal_id].sub_prac_name,
										'i_practice': projectWiseRevenue[emp_internal_id].practice_name,
										'f_prcnt_allocated': 0,
										's_emp_level': projectWiseRevenue[emp_internal_id].level_name,
										'i_resource_location': projectWiseRevenue[emp_internal_id].location_name,
										//'subsidiary': a_project_allocation_result[i_prac_count].getValue('subsidiary','employee','group'),
										//'emp_role':a_project_allocation_result[i_prac_count].getValue('title','employee','group'),
										'actual_effort':parseFloat(projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id][month].prcnt_allocated)
							});
						}
						//html += "<td class='monthly-amount'>";
						//html += parseFloat(projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id][month].prcnt_allocated).toFixed(2);
						//html += "</td>";
					}
				}
			}
			
			b_first_line_copied = 1;
			
			//html += "<td class='monthly-amount'>";
		//	html += parseFloat(f_total_allocated_row).toFixed(2);
			//html += "</td>";
			
			//html += "</tr>";
			
			var s_sub_prac_name = projectWiseRevenue[emp_internal_id].sub_prac_name;
			var s_practice_name = projectWiseRevenue[emp_internal_id].practice_name;
			
			i_diplay_frst_column = 1;
		}
	}
	i_diplay_frst_column = 0;
	if(total_prcnt_aloocated.length>0)
	{	
	}
	//
	var cnt=new Array();
	for ( var emp_internal_id in projectWiseRevenue) {
		
		for ( var project_internal_id in projectWiseRevenue[emp_internal_id].RevenueData) {
			
			if(projectWiseRevenue[emp_internal_id].sub_practice == sub_prac)
			{
				/*html += "<tr>";
				
				if (i_diplay_frst_column == 0)
				{
					html += "<td class='label-name'>";
					html += '<b>Cost View';
					html += "</td>";
				}
				html += "<td class='label-name'>";
				html += projectWiseRevenue[emp_internal_id].sub_prac_name;
				html += "</td>";
			
				html += "<td class='label-name'>";
				html += projectWiseRevenue[emp_internal_id].role_name;
				html += "</td>";
					
				html += "<td class='label-name'>";
				html += projectWiseRevenue[emp_internal_id].level_name;
				html += "</td>";
				
				html += "<td class='label-name'>";
				html += projectWiseRevenue[emp_internal_id].location_name;
				html += "</td>";
					*/
				var i_sub_practice_internal_id = 0;
				var i_amt = 0;
				for (var month in projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id])
				{
					var a_MonthNames = MonthNames(i_year_project);
					var currentMonthPos = a_MonthNames.indexOf(month);
					var s_current_month = a_MonthNames[currentMonthPos];
					var s_current_mnth_yr = s_current_month.substr(4, 8);
					
					var i_nxt_yr_flg = 0;
					if(currentMonthPos >=12)
					{
						currentMonthPos = currentMonthPos - 12;
						i_nxt_yr_flg = 1;
					}
					
					var i_curr_yr_dis_flag = 0;
					if(parseInt(s_current_mnth_yr) < i_year_project_end && i_prject_end_mnth < currentMonthPos)
					{
						i_curr_yr_dis_flag = 1;
					}
					
					if (((i_project_mnth <= currentMonthPos && ((i_prject_end_mnth >= currentMonthPos || i_curr_yr_dis_flag == 1) && i_nxt_yr_flg == 0) && parseInt(s_current_mnth_yr) <= i_year_project) && parseInt(s_current_mnth_yr) <= i_year_project_end) || (i_prject_end_mnth >= currentMonthPos && parseInt(s_current_mnth_yr) <= i_year_project_end && i_nxt_yr_flg == 1))
					{
						if (mode == 2)
						{
							
							//html += "<td class='monthly-amount'>";
							var total_revenue_format = parseFloat(projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id][month].Amount).toFixed(1);
							//html += ''+s_currency_symbol_proj+' '+format2(total_revenue_format);
							
							if(i_frst_row == 0)
							{
								//nlapiLogExecution('audit','inside push recog amnt:- '+projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id][month].recognized_revenue);
								a_amount.push(total_revenue_format);
								a_recognized_revenue.push(projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id][month].recognized_revenue);
								revenue_share_arr.push(parseFloat(projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id][month].revenue_share));
								practice_share_arr.push(parseFloat(projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id][month].practice_share));
								mnth_flag_arr.push(parseFloat(projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id][month].f_mnth_flag));
							}
							else
							{
								var amnt_mnth = a_amount[i_amt];
								amnt_mnth = parseFloat(amnt_mnth) + parseFloat(projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id][month].Amount);
								
								a_amount[i_amt] = amnt_mnth;
								revenue_share_arr[i_amt]=parseFloat(projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id][month].revenue_share);
								practice_share_arr[i_amt]=(parseFloat(projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id][month].practice_share));
								mnth_flag_arr[i_amt]=(parseFloat(projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id][month].f_mnth_flag));
								var i_existing_recognised_amount = a_recognized_revenue[i_amt];
								i_existing_recognised_amount = parseFloat(i_existing_recognised_amount) + parseFloat(projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id][month].recognized_revenue);
								
								a_recognized_revenue[i_amt] = i_existing_recognised_amount;
								
								i_amt++;
							}
							i_total_row_revenue = parseFloat(i_total_row_revenue) + parseFloat(projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id][month].Amount);
							i_total_per_row = parseFloat(i_total_per_row) + parseFloat(projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id][month].Amount);
						}
						
						//html += "</td>";
					}
				}
				
				if (mode == 2)
				{

					var i_is_practice_alreday_present = findAllocationPracticePresent(a_proj_prac_details,projectWiseRevenue[emp_internal_id].level_name, projectWiseRevenue[emp_internal_id].location_name);				
					if(cnt.indexOf(i_is_practice_alreday_present)<0)
					{
						cnt.push(i_is_practice_alreday_present);
						i_total_per_row=(i_total_per_row);
					}
					else
					{
						var re_effort_cost=a_proj_prac_details[i_is_practice_alreday_present].effort_cost;
						i_total_per_row=(i_total_per_row)+	(re_effort_cost);
					}
					if((i_is_practice_alreday_present >= 0) )
					{
									//nlapiLogExecution('audit','already present prcnt:- '+a_proj_prac_details[i_is_practice_alreday_present].f_prcnt_allocated,'f_already_alloc_prcnt:- '+f_already_alloc_prcnt);
								a_proj_prac_details[i_is_practice_alreday_present] = {
														'i_sub_practice': a_proj_prac_details[i_is_practice_alreday_present].i_sub_practice,
														'i_practice':a_proj_prac_details[i_is_practice_alreday_present].i_practice,
														'f_prcnt_allocated': a_proj_prac_details[i_is_practice_alreday_present].f_prcnt_allocated,
														's_emp_level': a_proj_prac_details[i_is_practice_alreday_present].s_emp_level,
														'i_resource_location':a_proj_prac_details[i_is_practice_alreday_present].i_resource_location,
														//'subsidiary': a_project_allocation_result[i_prac_count].getValue('subsidiary','employee','group'),
														//'emp_role':a_project_allocation_result[i_prac_count].getValue('title','employee','group'),
														'actual_effort': a_proj_prac_details[i_is_practice_alreday_present].actual_effort,
														'effort_cost':(i_total_per_row),
														'plan_cost':0
													};
						
					}
					
					//html += "<td class='monthly-amount'>";
					//html += ''+s_currency_symbol_proj+' '+format2(i_total_per_row);
					//html += "</td>";
					i_total_per_row = 0;
					
				}
				
				i_frst_row = 1;
	
				//html += "</tr>";
				
				i_diplay_frst_column = 1;
				
				var f_revenue_share = projectWiseRevenue[emp_internal_id].revenue_share;
			}		
		}
	}
	
	var s_sub_prac_name = '';
	for (var emp_internal_id in projectWiseRevenue)
	{
		if (projectWiseRevenue[emp_internal_id].sub_practice == sub_prac)
		{
			s_sub_prac_name = projectWiseRevenue[emp_internal_id].sub_prac_name;
			s_practice_name = projectWiseRevenue[emp_internal_id].practice_name;
		
		}
	}
	

	/*
	html += "<tr>";
	//Cost percentage
	html += '<b>Monthly Margin % ';
	html += "</tr>";*/
	var plan_count_array=new Array();
	for ( var emp_internal_id_plan in projectAllocationPlan) {
		
		for ( var project_internal_id in projectAllocationPlan[emp_internal_id_plan].RevenueDataPlan) {
			
			if(projectAllocationPlan[emp_internal_id_plan].sub_practice == sub_prac)
			{
				/*html += "<tr>";
				
				if (i_diplay_frst_column == 0)
				{
					html += "<td class='label-name'>";
					html += '<b>Cost View';
					html += "</td>";
				}
				
			
					*/
				var i_sub_practice_internal_id = 0;
				var i_amt = 0;
				for (var month in projectAllocationPlan[emp_internal_id_plan].RevenueDataPlan[project_internal_id])
				{
					var a_MonthNames = MonthNames(i_year_project);
					var currentMonthPos = a_MonthNames.indexOf(month);
					var s_current_month = a_MonthNames[currentMonthPos];
					var s_current_mnth_yr = s_current_month.substr(4, 8);
					
					var i_nxt_yr_flg = 0;
					if(currentMonthPos >=12)
					{
						currentMonthPos = currentMonthPos - 12;
						i_nxt_yr_flg = 1;
					}
					
					var i_curr_yr_dis_flag = 0;
					if(parseInt(s_current_mnth_yr) < i_year_project_end && i_prject_end_mnth < currentMonthPos)
					{
						i_curr_yr_dis_flag = 1;
					}
					
					if (((i_project_mnth <= currentMonthPos && ((i_prject_end_mnth >= currentMonthPos || i_curr_yr_dis_flag == 1) && i_nxt_yr_flg == 0) && parseInt(s_current_mnth_yr) <= i_year_project) && parseInt(s_current_mnth_yr) <= i_year_project_end) || (i_prject_end_mnth >= currentMonthPos && parseInt(s_current_mnth_yr) <= i_year_project_end && i_nxt_yr_flg == 1))
					{
						if (mode == 2)
						{
							
							//html += "<td class='monthly-amount'>";
							var total_revenue_format = parseFloat(projectAllocationPlan[emp_internal_id_plan].RevenueDataPlan[project_internal_id][month].Amount).toFixed(1);
							//html += ''+s_currency_symbol_proj+' '+format2(total_revenue_format);
							
							if(i_first_row_plan == 0)
							{
								//nlapiLogExecution('audit','inside push recog amnt:- '+projectAllocationPlan[emp_internal_id_plan].RevenueDataPlan[project_internal_id][month].recognized_revenue);
								a_amount_plan.push(total_revenue_format);
								a_recognized_revenue_plan.push(projectAllocationPlan[emp_internal_id_plan].RevenueDataPlan[project_internal_id][month].recognized_revenue);
								revenue_share_arr_plan.push(parseFloat(projectAllocationPlan[emp_internal_id_plan].RevenueDataPlan[project_internal_id][month].revenue_share_plan));
								practice_share_arr_plan.push(parseFloat(projectAllocationPlan[emp_internal_id_plan].RevenueDataPlan[project_internal_id][month].practice_share));
								mnth_flag_arr_plan.push(parseFloat(projectAllocationPlan[emp_internal_id_plan].RevenueDataPlan[project_internal_id][month].f_mnth_flag));
							}
							else
							{
								var amnt_mnth = a_amount_plan[i_amt];
								amnt_mnth = parseFloat(amnt_mnth) + parseFloat(projectAllocationPlan[emp_internal_id_plan].RevenueDataPlan[project_internal_id][month].Amount);
								a_amount_plan[i_amt] = amnt_mnth;
								revenue_share_arr_plan[i_amt]=parseFloat(projectAllocationPlan[emp_internal_id_plan].RevenueDataPlan[project_internal_id][month].revenue_share_plan);
								practice_share_arr_plan[i_amt]=(parseFloat(projectAllocationPlan[emp_internal_id_plan].RevenueDataPlan[project_internal_id][month].practice_share));
								mnth_flag_arr_plan[i_amt]=(parseFloat(projectAllocationPlan[emp_internal_id_plan].RevenueDataPlan[project_internal_id][month].f_mnth_flag));
								var i_existing_recognised_amount = a_recognized_revenue[i_amt];
								i_existing_recognised_amount = parseFloat(i_existing_recognised_amount) + parseFloat(projectAllocationPlan[emp_internal_id_plan].RevenueDataPlan[project_internal_id][month].recognized_revenue);
								a_recognized_revenue[i_amt] = i_existing_recognised_amount;	
								i_amt++;
							}
							i_total_row_revenue_plan = parseFloat(i_total_row_revenue_plan) + parseFloat(projectAllocationPlan[emp_internal_id_plan].RevenueDataPlan[project_internal_id][month].Amount);
							i_total_per_row_plan = parseFloat(i_total_per_row_plan) + parseFloat(projectAllocationPlan[emp_internal_id_plan].RevenueDataPlan[project_internal_id][month].Amount);
						}
						
						
						//html += "</td>";
					}
				}
				
				if (mode == 2)
				{
					var i_is_practice_alreday_present = findAllocationPracticePresent(a_proj_prac_details,projectAllocationPlan[emp_internal_id_plan].level_name, projectAllocationPlan[emp_internal_id_plan].location_name);
					if(plan_count_array.indexOf(i_is_practice_alreday_present)<0)
					{
						plan_count_array.push(i_is_practice_alreday_present);
						i_total_per_row_plan=(i_total_per_row_plan);
					}
					else
					{
						var re_effort_cost=a_proj_prac_details[i_is_practice_alreday_present].plan_cost;
						i_total_per_row_plan=(i_total_per_row_plan)+	(re_effort_cost);
					}
						if(i_is_practice_alreday_present >= 0)
						{
								//	var effort_prcnt=a_proj_prac_details[i_is_practice_alreday_present].f_prcnt_allocated;
								/*	var effort_prcnt=a_proj_prac_details[i_is_practice_alreday_present].plan_cost;
									effort_prcnt= parseFloat(effort_prcnt)+parseFloat(i_total_per_row_plan)
									effort_prcnt=format2(effort_prcnt);
									*/
									//nlapiLogExecution('audit','already present prcnt:- '+a_proj_prac_details[i_is_practice_alreday_present].f_prcnt_allocated,'f_already_alloc_prcnt:- '+f_already_alloc_prcnt);
									a_proj_prac_details[i_is_practice_alreday_present] = {
															'i_sub_practice': a_proj_prac_details[i_is_practice_alreday_present].i_sub_practice,
															'i_practice':a_proj_prac_details[i_is_practice_alreday_present].i_practice,
															'f_prcnt_allocated': a_proj_prac_details[i_is_practice_alreday_present].f_prcnt_allocated,
															's_emp_level': a_proj_prac_details[i_is_practice_alreday_present].s_emp_level,
															'i_resource_location':a_proj_prac_details[i_is_practice_alreday_present].i_resource_location,
															//'subsidiary': a_project_allocation_result[i_prac_count].getValue('subsidiary','employee','group'),
															//'emp_role':a_project_allocation_result[i_prac_count].getValue('title','employee','group'),
															'actual_effort': a_proj_prac_details[i_is_practice_alreday_present].actual_effort,
															'effort_cost':a_proj_prac_details[i_is_practice_alreday_present].effort_cost,
															'plan_cost': (i_total_per_row_plan)
														};
						}
					//html += "<td class='monthly-amount'>";
					//html += ''+s_currency_symbol_proj+' '+format2(i_total_per_row);
					//html += "</td>";
					i_total_per_row_plan = 0;
					
				}
				
				i_first_row_plan = 1;
	
				//html += "</tr>";
				
				i_diplay_frst_column = 1;
				
				var f_revenue_share = projectAllocationPlan[emp_internal_id_plan].revenue_share;
			}		
		}
	}
	var f_total_revenue_plan = 0;
	if(a_amount_plan.length>0)
	{
		//Total reveue recognized per month row for effort
		/*html += "<tr>";
		
		html += "<td class='label-name'>";
		html += '<b>Revenue to be recognized/month by plan ';
		html += "</td>";
			
		html += "<td class='label-name'>";
		html += '';
		html += "</td>";*/
		
		var d_today_date = new Date();
		var i_current_month = d_today_date.getMonth();
		var i_current_year = d_today_date.getFullYear();
		
		
		var f_revenue_amount_till_current_month = 0;
	
		
		for (var i_amt = 0; i_amt < a_amount_plan.length; i_amt++)
		{
			var f_prcnt_revenue = parseFloat(a_amount_plan[i_amt]) / parseFloat(f_total_cost_breakUp_plan[i_amt]);
			f_prcnt_revenue = f_prcnt_revenue * 100;
		
			var test_var=revenue_share_arr_plan[i_amt];
			var f_revenue_amount = (parseFloat(f_prcnt_revenue) * parseFloat(revenue_share_arr_plan[i_amt])) / 100;
			//}
			if(!f_revenue_amount)
				f_revenue_amount = 0;
			margin_rev_plan[i_amt]=f_revenue_amount;
			if (parseInt(i_year_project) != parseInt(i_current_year))
			{
				var i_total_project_tenure = 11 + parseInt(i_current_month);
				var f_total_prev_mnths = parseFloat(i_total_project_tenure) - parseFloat(i_project_mnth);
				f_total_prev_mnths = parseFloat(f_total_prev_mnths) + parseFloat(1);
			}
			else
			{
				var f_total_prev_mnths = parseFloat(i_current_month) - parseFloat(i_project_mnth);
			}
			
			if(i_amt <= f_total_prev_mnths)
			{
				//nlapiLogExecution('audit','f_revenue_amount:- '+f_revenue_amount,'a_amount.length:- '+a_amount.length);
				f_revenue_amount_till_current_month = parseFloat(f_revenue_amount) + parseFloat(f_revenue_amount_till_current_month);
			}
			
			f_total_revenue_plan = parseFloat(f_revenue_amount) + parseFloat(f_total_revenue_plan);
			
			//html += "<td class='projected-amount'>";
			
			//html += '<b>'+s_currency_symbol_proj+' '+format2(f_revenue_amount);
			//}
			//html += "</td>";
		//	f_revenue_arr[i_amt]=parseFloat(f_revenue_arr[i_amt])+parseFloat(f_revenue_amount);
			//flag_counter_arr=1;
		}
		
		for (var emp_internal_id in projectWiseRevenue)
		{
			for (var project_internal_id in projectWiseRevenue[emp_internal_id].RevenueData)
			{
				var i_amount_map = 0;
				if (projectWiseRevenue[emp_internal_id].sub_practice == sub_prac)
				{
					for (var month in projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id])
					{
						var a_MonthNames = MonthNames(i_year_project);
						var currentMonthPos = a_MonthNames.indexOf(month);
						var s_current_month = a_MonthNames[currentMonthPos];
						var s_current_mnth_yr = s_current_month.substr(4, 8);
						
						var i_nxt_yr_flg = 0;
						if(currentMonthPos >=12)
						{
							currentMonthPos = currentMonthPos - 12;
							i_nxt_yr_flg = 1;
						}
						
						var i_curr_yr_dis_flag = 0;
						if(parseInt(s_current_mnth_yr) < i_year_project_end && i_prject_end_mnth < currentMonthPos)
						{
							i_curr_yr_dis_flag = 1;
						}
						
						if (((i_project_mnth <= currentMonthPos && ((i_prject_end_mnth >= currentMonthPos || i_curr_yr_dis_flag == 1) && i_nxt_yr_flg == 0) && parseInt(s_current_mnth_yr) <= i_year_project) && parseInt(s_current_mnth_yr) <= i_year_project_end) || (i_prject_end_mnth >= currentMonthPos && parseInt(s_current_mnth_yr) <= i_year_project_end && i_nxt_yr_flg == 1))
						{
							var f_prcnt_revenue = parseFloat(a_amount[i_amount_map]) / parseFloat(i_total_row_revenue);
							f_prcnt_revenue = f_prcnt_revenue * 100;
							
							var f_revenue_amount = (parseFloat(f_prcnt_revenue) * parseFloat(revenue_share_arr[i_amount_map])) / 100;
			
							projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id][month].total_revenue = f_revenue_amount;
							projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id][month].prcent_complt = f_prcnt_revenue;
							i_amount_map++;
						}
					}	
				}
			}
		}
		
		//html += "<td class='projected-amount'>";
	//	html += '<b>'+s_currency_symbol_proj+' '+format2(f_total_revenue_plan);
		var total_rev=f_total_revenue_plan;
	//	html += "</td>";
	//	
	//	html += "</tr>";
	}
	else
	{	
		var total_rev=0;
	}
	
	//
	if(a_amount.length>0)
	{
		//Total reveue recognized per month row
	/*	html += "<tr>";
		
		html += "<td class='label-name'>";
		html += '<b>Revenue to be recognized/month';
		html += "</td>";
		
		html += "<td class='label-name'>";
		html += '<b>'+s_practice_name;
		html += "</td>";
				
		html += "<td class='label-name'>";
		html += '<b>'+s_sub_prac_name;
		html += "</td>";

		html += "<td class='label-name'>";
		html += '';
		html += "</td>";
			
		html += "<td class='label-name'>";
		html += '';
		html += "</td>";
		
		html += "<td class='label-name'>";
		html += '';
		html += "</td>";*/
		
		var d_today_date = new Date();
		var i_current_month = d_today_date.getMonth();
		var i_current_year = d_today_date.getFullYear();
		
		var f_total_revenue = 0;
		var f_revenue_amount_till_current_month = 0;
		if(flag_counter_arr==0){
		for (var i_amt = 0; i_amt < a_amount.length; i_amt++)
		{
			f_revenue_arr[i_amt] = 0;
			
		}
		}
		for (var i_amt = 0; i_amt < a_amount.length; i_amt++)
		{
			var f_prcnt_revenue = parseFloat(a_amount[i_amt]) / parseFloat(f_total_cost_breakUp[i_amt]);
			f_prcnt_revenue = f_prcnt_revenue * 100;
			//var f_revenue_amount = (parseFloat(f_prcnt_revenue) * parseFloat(100000)) / 100
			if(mnth_flag_arr[i_amt]==1)
			{
				var f_revenue_amount = practice_share_arr[i_amt];
			}
			else{
			var f_revenue_amount = (parseFloat(f_prcnt_revenue) * parseFloat(revenue_share_arr[i_amt])) / 100;
			}
			if(!f_revenue_amount)
				f_revenue_amount = 0;
			margin_rev[i_amt]=f_revenue_amount;
			if (parseInt(i_year_project) != parseInt(i_current_year))
			{
				var i_total_project_tenure = 11 + parseInt(i_current_month);
				var f_total_prev_mnths = parseFloat(i_total_project_tenure) - parseFloat(i_project_mnth);
				f_total_prev_mnths = parseFloat(f_total_prev_mnths) + parseFloat(1);
			}
			else
			{
				var f_total_prev_mnths = parseFloat(i_current_month) - parseFloat(i_project_mnth);
			}
			
			if(i_amt <= f_total_prev_mnths)
			{
				//nlapiLogExecution('audit','f_revenue_amount:- '+f_revenue_amount,'a_amount.length:- '+a_amount.length);
				f_revenue_amount_till_current_month = parseFloat(f_revenue_amount) + parseFloat(f_revenue_amount_till_current_month);
			}
			
			f_total_revenue = parseFloat(f_revenue_amount) + parseFloat(f_total_revenue);
			
		//	html += "<td class='projected-amount'>";
			
			//html += '<b>'+s_currency_symbol_proj+' '+format2(f_revenue_amount);
			//}
		//	html += "</td>";
			f_revenue_arr[i_amt]=parseFloat(f_revenue_arr[i_amt])+parseFloat(f_revenue_amount);
			flag_counter_arr=1;
		}
		
		for (var emp_internal_id in projectWiseRevenue)
		{
			for (var project_internal_id in projectWiseRevenue[emp_internal_id].RevenueData)
			{
				var i_amount_map = 0;
				if (projectWiseRevenue[emp_internal_id].sub_practice == sub_prac)
				{
					for (var month in projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id])
					{
						var a_MonthNames = MonthNames(i_year_project);
						var currentMonthPos = a_MonthNames.indexOf(month);
						var s_current_month = a_MonthNames[currentMonthPos];
						var s_current_mnth_yr = s_current_month.substr(4, 8);
						
						var i_nxt_yr_flg = 0;
						if(currentMonthPos >=12)
						{
							currentMonthPos = currentMonthPos - 12;
							i_nxt_yr_flg = 1;
						}
						
						var i_curr_yr_dis_flag = 0;
						if(parseInt(s_current_mnth_yr) < i_year_project_end && i_prject_end_mnth < currentMonthPos)
						{
							i_curr_yr_dis_flag = 1;
						}
						
						if (((i_project_mnth <= currentMonthPos && ((i_prject_end_mnth >= currentMonthPos || i_curr_yr_dis_flag == 1) && i_nxt_yr_flg == 0) && parseInt(s_current_mnth_yr) <= i_year_project) && parseInt(s_current_mnth_yr) <= i_year_project_end) || (i_prject_end_mnth >= currentMonthPos && parseInt(s_current_mnth_yr) <= i_year_project_end && i_nxt_yr_flg == 1))
						{
							var f_prcnt_revenue = parseFloat(a_amount[i_amount_map]) / parseFloat(i_total_row_revenue);
							f_prcnt_revenue = f_prcnt_revenue * 100;
							
							var f_revenue_amount = (parseFloat(f_prcnt_revenue) * parseFloat(revenue_share_arr[i_amount_map])) / 100;
			
							projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id][month].total_revenue = f_revenue_amount;
							projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id][month].prcent_complt = f_prcnt_revenue;
							i_amount_map++;
						}
					}	
				}
			}
		}
		
		//html += "<td class='projected-amount'>";
		//html += '<b>'+s_currency_symbol_proj+' '+format2(f_total_revenue);
		var total_rev_effort=f_total_revenue;
		//html += "</td>";
		
		//html += "</tr>";
	}
	else
	{
		var total_rev_effort=0;
				
	}
	//Margin % per plan
	
	var f_total_mar_prcnt = 0;
	if(a_amount_plan.length>0)
	{
		for (var i_amt_mar = 0; i_amt_mar < a_amount_plan.length; i_amt_mar++)
		{
			var f_prcnt_mar_revenue = parseFloat(margin_rev_plan[i_amt_mar]-a_amount_plan[i_amt_mar]) / parseFloat(margin_rev_plan[i_amt_mar]);
			f_prcnt_mar_revenue = f_prcnt_mar_revenue * 100;
			if((!_logValidation(f_prcnt_mar_revenue))||(margin_rev_plan[i_amt_mar]<=0))
				f_prcnt_mar_revenue = 0;
				
			//f_total_mar_prcnt = parseFloat(f_prcnt_mar_revenue) + parseFloat(f_total_mar_prcnt);
			f_total_mar_prcnt=(parseFloat(f_total_revenue_plan)-parseFloat(i_total_row_revenue_plan))/parseFloat(f_total_revenue_plan);
			f_total_mar_prcnt=f_total_mar_prcnt*100;
			if(!_logValidation(f_total_mar_prcnt) || f_total_revenue_plan ==0)
				f_total_mar_prcnt=0;
			//html += "<td class='projected-amount'>";
		//	html += '<b>'+parseFloat(f_prcnt_mar_revenue).toFixed(1)+' %';
		//	html += "</td>";
		}
	}
	else
	{
		
	}
//	html += "<td class='projected-amount'>";
	//html += '<b>'+parseFloat(f_total_mar_prcnt).toFixed(1)+' %';
	var margin_plan=parseFloat(f_total_mar_prcnt).toFixed(1)+' %';
	//html += "</td>";
	
	//html += "</tr>";
	//
	//** Margin % per effort
		
	//Cost percentage
	
	var f_total_mar_prcnt = 0;
	if(a_amount.length>0)
	{
		for (var i_amt_mar = 0; i_amt_mar < a_amount.length; i_amt_mar++)
		{
			var f_prcnt_mar_revenue = parseFloat(margin_rev[i_amt_mar]-a_amount[i_amt_mar]) / parseFloat(margin_rev[i_amt_mar]);
			f_prcnt_mar_revenue = f_prcnt_mar_revenue * 100;
			if((!_logValidation(f_prcnt_mar_revenue))||(margin_rev[i_amt_mar]<=0))
				f_prcnt_mar_revenue = 0;
				
			//f_total_mar_prcnt = parseFloat(f_prcnt_mar_revenue) + parseFloat(f_total_mar_prcnt);
			f_total_mar_prcnt=(parseFloat(f_total_revenue)-parseFloat(i_total_row_revenue))/parseFloat(f_total_revenue);
			f_total_mar_prcnt=f_total_mar_prcnt*100;
			if(!_logValidation(f_total_mar_prcnt) || f_total_revenue==0)
				f_total_mar_prcnt=0;
			//html += "<td class='monthly-amount'>";
			//html += '<b>'+parseFloat(f_prcnt_mar_revenue).toFixed(1)+' %';
			//html += "</td>";
		}
	}
	else
	{
	
	}
	//html += "<td class='monthly-amount'>";
	//html += '<b>'+parseFloat(f_total_mar_prcnt).toFixed(1)+' %';
	var margin_effort=parseFloat(f_total_mar_prcnt).toFixed(1)+' %';
	//html += "</td>";
	
	//html += "</tr>";
	//** Code for total view **//
	
	for(var final_list=0;final_list<a_proj_prac_details.length;final_list++)
	{
		html += "<tr>";
		/*html += "<td class='label-name' width:15px>";
		html += a_proj_prac_details[final_list].i_practice;
		html += "</td>";*/
		html += "<td class='label-name' width:15px>";
		html += a_proj_prac_details[final_list].i_sub_practice;
		html += "</td>";
		html += "<td class='label-name' width:15px>";
		html += a_proj_prac_details[final_list].s_emp_level;
		html += "</td>";
		html += "<td class='label-name' width:15px>";
		html += a_proj_prac_details[final_list].i_resource_location;
		html += "</td>";
		html += "<td class='monthly-amount' width:15px>";
		html += a_proj_prac_details[final_list].f_prcnt_allocated;
		html += "</td>";
		html += "<td class='monthly-amount' width:15px>";
		html += a_proj_prac_details[final_list].actual_effort;
		html += "</td>";
		html += "<td class='monthly-amount' width:15px>";
		html += format2(a_proj_prac_details[final_list].plan_cost);
		html += "</td>";
		html += "<td class='monthly-amount' width:15px>";
		html += format2(a_proj_prac_details[final_list].effort_cost);
		html += "</td>";
		
		html += "<tr>";
	}
		html += "<tr>";
		
		html += "<td class='label-name' width:15px>";
		html += "<b>Sub Total";
		html += "</td>";
		/*html += "<td class='label-name' width:15px>";//sub practice
		html += "";
		html += "</td>";*/
		html += "<td class='label-name' width:15px>";//level
		html += "";
		html += "</td>";
		html += "<td class='label-name' width:15px>";//location
		html += "";
		html += "</td>";
		html += "<td class='monthly-amount' width:15px>";
		html += "";
		html += "</td>";
		html += "<td class='monthly-amount' width:15px>";
		html += "";
		html += "</td>";
	
		var plan_total=0;
		var cost_total=0;
		for(var tot=0;tot<a_proj_prac_details.length;tot++)
		{
			plan_total=plan_total+a_proj_prac_details[tot].plan_cost;
			cost_total=cost_total+a_proj_prac_details[tot].effort_cost
		
		}
		net_cost_plan=net_cost_plan+plan_total;
		net_cost_effort=net_cost_effort+cost_total;
		html += "<td class='monthly-amount' width:15px>";
		html += format2( plan_total);
		html += "</td>";
		
		html += "<td class='monthly-amount' width:15px>";
		html += format2(cost_total);
		html += "</td>";

		html += "</tr>";
		html += "</table>";
		
		html += "<table class='projection-table'>";

		net_revenue_plan=net_revenue_plan+total_rev;
		net_revenue_effort=net_revenue_effort+total_rev_effort;
	// header
	html += "<tr class='header-row'>";
	
	
	/*html += "<td width:15px>";
	html += "Practice";
	html += "</td>";*/
	
	html += "<td width:30px>";
	html += "Practice/Sub Practice";
	html += "</td>";
	
	html += "<td width:30px>";
	html += "BaseLine (Revenue)";
	html += "</td>";
	html += "<td width:30px>";
	html += "Latest (Revenue)";
	html += "</td>";
	html += "<td width:30px>";
	html += "BaseLine (Margin)%";
	html += "</td>";
	html += "<td width:30px>";
	html += "Latest (Margin)%";
	html += "</td>";
	html += "</tr>";
	
	html += "<tr>";
	/*html += "<td class='label-name' width:15px>";
	html += a_proj_prac_details[0].i_practice;
	html += "</td>";*/
	html += "<td class='label-name' width:15px>";
	html += a_proj_prac_details[0].i_sub_practice;
	html += "</td>";
	html += "<td class='projected-amount' width:15px>";
	html += format2(total_rev);
	html += "</td>";
	html += "<td class='label-name' width:15px>";
	html += format2(total_rev_effort);
	html += "</td>";
	html += "<td class='projected-amount' width:15px>";
	html += margin_plan;
	html += "</td>";
	html += "<td class='label-name' width:15px>";
	html += margin_effort;
	html += "</td>";
	//**EOC**//
		over_all_array.push({
		'sub_practice':a_proj_prac_details[0].i_sub_practice,
		'plan_revenue':total_rev,
		'plan_cost':plan_total,
		'current_revenue':total_rev_effort,
		'current_cost':cost_total,
		'plan_margin':margin_plan,
		'current_margin':margin_effort
	});
	//
	
	html += "<tr>";
	
	html += "</table>";
	
	for(var i_exist_index=0; i_exist_index<=a_recognized_revenue.length; i_exist_index++)
	{
		if(a_recognized_revenue_total_proj_level[i_exist_index])
		{
			var f_existing_cost_in_arr = parseFloat(a_recognized_revenue_total_proj_level[i_exist_index]);
		}
		else
		{
			var f_existing_cost_in_arr = 0;
		}
		
		var f_existing_cost = a_recognized_revenue[i_exist_index];
		f_existing_cost = parseFloat(f_existing_cost) + parseFloat(f_existing_cost_in_arr);
		
		a_recognized_revenue_total_proj_level[i_exist_index] = f_existing_cost;
	}
	//a_recognized_revenue_total_proj_level = a_recognized_revenue;
	
	return html;
}



function format2(n) {
    n = parseFloat(n).toFixed(1).replace(/(\d)(?=(\d{3})+\.)/g, "$1,");
	n = n.toString().split('.');
	return n[0];
}

function getCurrency_Symbol(s_proj_currency)
{
	var s_currency_symbols = {
    'USD': '$', // US Dollar
    'EUR': '€', // Euro
    'GBP': '£', // British Pound Sterling
    'INR': '₹', // Indian Rupee
	};
	
	if(s_currency_symbols[s_proj_currency]!==undefined) {
	    return s_currency_symbols[s_proj_currency];
	}
}

function getMonthsBreakup(d_startDate, d_endDate) {
	
	try
	{
		var dateBreakUp = [];
		var new_start_date = new Date(d_startDate.getFullYear(), d_startDate
		        .getMonth(), 1);
				
		var nxt_mnth_strt_date = new Date(d_startDate.getFullYear(), d_startDate
		        .getMonth()+1, 1);
		
		var endDate = new Date(nxt_mnth_strt_date - 1);
				
		//var endDate = nlapiAddDays(nlapiAddMonths(d_startDate, 1), -1);
		endDate = nlapiDateToString(endDate, 'date');
	
		dateBreakUp.push({
		    Start : nlapiDateToString(new_start_date, 'date'),
		    End : endDate
		});
	
		for (var i = 1;; i++) {
			var new_date = nlapiAddMonths(new_start_date, i);
	
			if (new_date > d_endDate) {
				break;
			}
			
			endDate = nlapiAddDays(nlapiAddMonths(new_date, 1), -1);
			endDate = nlapiDateToString(endDate, 'date');
					
			dateBreakUp.push({
			    Start : nlapiDateToString(new_date, 'date'),
			    End : endDate
			});
		}
	}
	catch(err)
	{
		nlapiLogExecution('ERROR','getMonthsBreakup','ERROR MESSAGE :- '+err);
	}

	return dateBreakUp;
}

function getMonthName(currentDate) {
	currentDate = nlapiStringToDate(currentDate)
	var monthNames = [ "JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JUL", "AUG",
	        "SEP", "OCT", "NOV", "DEC" ];
	return monthNames[currentDate.getMonth()];
}

function MonthNames(yr) {
	var yr1 = Number(yr) + 1;
	
	var monthNames = [ "JAN_"+yr, "FEB_"+yr, "MAR_"+yr, "APR_"+yr, "MAY_"+yr, "JUN_"+yr, "JUL_"+yr, "AUG_"+yr,
	        "SEP_"+yr, "OCT_"+yr, "NOV_"+yr, "DEC_"+yr ];
	var month_nxt_yr = ["JAN_"+yr1, "FEB_"+yr1, "MAR_"+yr1, "APR_"+yr1, "MAY_"+yr1, "JUN_"+yr1, "JUL_"+yr1, "AUG_"+yr1,
	        "SEP_"+yr1, "OCT_"+yr1, "NOV_"+yr1, "DEC_"+yr1 ];
	monthNames = monthNames.concat(month_nxt_yr);
	
	return monthNames;
}

function yearData(yr) {
	var yr1 = Number(yr) + 1;
	
	var monthNames = [ "JAN_"+yr, "FEB_"+yr, "MAR_"+yr, "APR_"+yr, "MAY_"+yr, "JUN_"+yr, "JUL_"+yr, "AUG_"+yr,
	        "SEP_"+yr, "OCT_"+yr, "NOV_"+yr, "DEC_"+yr ];
	var month_nxt_yr = ["JAN_"+yr1, "FEB_"+yr1, "MAR_"+yr1, "APR_"+yr1, "MAY_"+yr1, "JUN_"+yr1, "JUL_"+yr1, "AUG_"+yr1,
	        "SEP_"+yr1, "OCT_"+yr1, "NOV_"+yr1, "DEC_"+yr1 ];
	monthNames = monthNames.concat(month_nxt_yr);
	
	for (var i = 0; i < monthNames.length; i++) {
		this[monthNames[i]] = {
		    Amount : 0,
			prcnt_allocated : 0,
			total_revenue : 0,
			recognized_revenue : 0,
			actual_revenue : 0,
			mnth_strt : 0,
			mnth_end : 0,
			prcent_complt : 0
		};
	}
}

function generate_previous_effrt_revenue(s_effrt_json,projectWiseRevenue_previous_effrt,a_revenue_recognized_for_project,i_year_project)
{
	if(s_effrt_json)
	{
		var i_practice_previous = 0;
		var f_total_revenue_for_tenure = 0;
		//for (var i_mnth_end_plan = 0; i_mnth_end_plan < a_get_mnth_end_effrt_plan.length; i_mnth_end_plan++)
		var s_entire_json_clubed = JSON.parse(s_effrt_json);
		for (var i_mnth_end_plan = 0; i_mnth_end_plan < s_entire_json_clubed.length; i_mnth_end_plan++)
		{
			var a_row_json_data = JSON.parse(JSON.stringify(s_entire_json_clubed[i_mnth_end_plan]));
			for (var i_row_json_index = 0; i_row_json_index < a_row_json_data.length; i_row_json_index++)
			{
				var i_practice = a_row_json_data[i_row_json_index].prac;
				var s_practice = a_row_json_data[i_row_json_index].prac_text;
				var i_sub_practice = a_row_json_data[i_row_json_index].subprac;
				var s_sub_practice = a_row_json_data[i_row_json_index].subprac_text;
				var i_role = a_row_json_data[i_row_json_index].role;
				var s_role = a_row_json_data[i_row_json_index].role_text;
				var i_level = a_row_json_data[i_row_json_index].level;
				var s_level = a_row_json_data[i_row_json_index].level_text;
				var i_location = a_row_json_data[i_row_json_index].loc;
				var s_location = a_row_json_data[i_row_json_index].loc_text;
				var f_revenue = a_row_json_data[i_row_json_index].cost;
				if (!f_revenue) 
					f_revenue = 0;
				
				var f_revenue_share = a_row_json_data[i_row_json_index].share;
				if (!f_revenue_share) 
					f_revenue_share = 0;
					
				var i_no_of_resources = a_row_json_data[i_row_json_index].allo;
				var s_mnth_strt_date = '1/31/2017';
				var s_mnth_end_date = '31/31/2017';
				var s_mnth = a_row_json_data[i_row_json_index].mnth;
				var s_year = a_row_json_data[i_row_json_index].year;
				
				var f_revenue_recognized = find_recognized_revenue_prev(a_revenue_recognized_for_project,i_practice,i_sub_practice,i_role,i_level,s_mnth,s_year,a_prev_subprac_searched_once);
				
				if (!f_revenue_recognized) 
					f_revenue_recognized = 0;
				
				//nlapiLogExecution('audit','f_revenue_recognized:- '+f_revenue_recognized);
				
				var s_month_name = s_mnth + '_' + s_year;
				
				var total_revenue = parseFloat(i_no_of_resources) * parseFloat(f_revenue);
				total_revenue = parseFloat(total_revenue).toFixed(2);
				
				if (i_practice_previous == 0) {
					i_practice_previous = a_row_json_data[i_row_json_index].prac_row;
				}
				
				if (i_practice_previous != a_row_json_data[i_row_json_index].prac_row) {
					f_total_revenue_for_tenure = 0;
					i_practice_previous = a_row_json_data[i_row_json_index].prac_row;
				}
			
				f_total_revenue_for_tenure = parseFloat(f_total_revenue_for_tenure) + parseFloat(total_revenue);
				f_total_revenue_for_tenure = parseFloat(f_total_revenue_for_tenure).toFixed(2);
				
				var i_prac_subPrac_role_level = i_practice + '_' + i_sub_practice + '_' + i_role + '_' + i_level + '_' + i_location;
				
				if (!projectWiseRevenue_previous_effrt[i_prac_subPrac_role_level]) {
					projectWiseRevenue_previous_effrt[i_prac_subPrac_role_level] = {
						practice_name: s_practice,
						sub_prac_name: s_sub_practice,
						sub_practice: i_sub_practice,
						role_name: s_role,
						level_name: s_level,
						location_name: s_location,
						total_revenue_for_tenure: f_total_revenue_for_tenure,
						revenue_share: f_revenue_share,
						RevenueData: {}
					};
					
					projectWiseRevenue_previous_effrt[i_prac_subPrac_role_level].RevenueData[i_practice] = new yearData(i_year_project);
					
					projectWiseRevenue_previous_effrt[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].Amount = total_revenue;
					projectWiseRevenue_previous_effrt[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].prcnt_allocated = i_no_of_resources;
					projectWiseRevenue_previous_effrt[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].recognized_revenue = f_revenue_recognized;
					projectWiseRevenue_previous_effrt[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].mnth_strt = s_mnth_strt_date;
					projectWiseRevenue_previous_effrt[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].mnth_end = s_mnth_end_date;
				}
				else {
					projectWiseRevenue_previous_effrt[i_prac_subPrac_role_level].total_revenue_for_tenure = f_total_revenue_for_tenure;
					
					projectWiseRevenue_previous_effrt[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].Amount = total_revenue;
					projectWiseRevenue_previous_effrt[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].prcnt_allocated = i_no_of_resources;
					projectWiseRevenue_previous_effrt[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].recognized_revenue = f_revenue_recognized;
					projectWiseRevenue_previous_effrt[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].mnth_strt = s_mnth_strt_date;
					projectWiseRevenue_previous_effrt[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].mnth_end = s_mnth_end_date;
				}
			}
		}
	}
	
	//return projectWiseRevenue_previous_effrt;
}

function find_recognized_revenue_prev(a_revenue_recognized_for_project,practice,subpractice,role,level,month,year,a_prev_subprac_searched_once)
{
	var f_recognized_amount = 0;
	
	if(a_prev_subprac_searched_once.indexOf(subpractice) < 0)
	{
		for(var i_index_find=0; i_index_find<a_revenue_recognized_for_project.length; i_index_find++)
		{
			if(practice == a_revenue_recognized_for_project[i_index_find].practice_revenue_recognized)
			{
				if(subpractice == a_revenue_recognized_for_project[i_index_find].subpractice_revenue_recognized)
				{
					//if(role == a_revenue_recognized_for_project[i_index_find].role_revenue_recognized)
					{
						//if(level == a_revenue_recognized_for_project[i_index_find].level_revenue_recognized)
						{
							if(month == a_revenue_recognized_for_project[i_index_find].month_revenue_recognized)
							{
								if(year == a_revenue_recognized_for_project[i_index_find].year_revenue_recognized)
								{
									f_recognized_amount = a_revenue_recognized_for_project[i_index_find].amount_revenue_recognized;
									//nlapiLogExecution('audit','f_recognized_amount:- '+f_recognized_amount);
									a_prev_subprac_searched_once.push(subpractice);
								}
							}
						}
					}
				}
			}
		}
		
		return f_recognized_amount;
	}
	else
	{
		return 0;
	}
}

function find_recognized_revenue(a_revenue_recognized_for_project,practice,subpractice,role,level,month,year,a_subprac_searched_once,a_subprac_searched_once_month,a_subprac_searched_once_year)
{
	var f_recognized_amount = 0;
	
	if(func_search_prac_processed_fr_mnth(a_subprac_searched_once,subpractice,month,year) < 0)
	{
		for(var i_index_find=0; i_index_find<a_revenue_recognized_for_project.length; i_index_find++)
		{				
			if(practice == a_revenue_recognized_for_project[i_index_find].practice_revenue_recognized)
			{
				if(subpractice ==  a_revenue_recognized_for_project[i_index_find].subpractice_revenue_recognized)
				{
					//if(role == a_revenue_recognized_for_project[i_index_find].role_revenue_recognized)
					{
						//if(level == a_revenue_recognized_for_project[i_index_find].level_revenue_recognized)
						{
							if(month == a_revenue_recognized_for_project[i_index_find].month_revenue_recognized)
							{	
								if(parseInt(year) == parseInt(a_revenue_recognized_for_project[i_index_find].year_revenue_recognized))
								{
									f_recognized_amount = a_revenue_recognized_for_project[i_index_find].amount_revenue_recognized;
									//nlapiLogExecution('audit','f_recognized_amount inside look for prev mnth amnt once pr sub prac:- ',f_recognized_amount);
									a_subprac_searched_once.push(
																{
																	'subpractice': subpractice,
																	'month': month,
																	'year': year
																});
									//a_subprac_searched_once_month.push(month);
									//a_subprac_searched_once_year.push(year);
									
									if(subpractice == 325)
									{
										nlapiLogExecution('audit','month:- '+month,'year:- '+year);
										nlapiLogExecution('audit','f_recognized_amount:- '+f_recognized_amount);
									}
								}
							}
						}
					}
				}
			}
		}
		
		return f_recognized_amount;
	}
	else
	{
		return 0;
	}
}

function func_search_prac_processed_fr_mnth(a_subprac_searched_once,subpractice,month,year)
{
	var i_return_var = -1;
	for(var i_loop=0; i_loop<a_subprac_searched_once.length; i_loop++)
	{
		if(a_subprac_searched_once[i_loop].subpractice == subpractice)
		{
			if(a_subprac_searched_once[i_loop].month == month)
			{
				if(a_subprac_searched_once[i_loop].year == year)
				{
					i_return_var = i_loop;
					break;
				}
			}
		}
	}
	
	return i_return_var;
}
function generate_total_cost(projectWiseRevenue,monthBreakUp,i_year_project,i_project_mnth,mode,sub_prac,s_currency_symbol_proj,s_currency_symbol_usd,i_year_project_end,i_prject_end_mnth,fld_true_up_msg)
{
	var css_file_url = "";

	var context = nlapiGetContext();
	if (context.getEnvironment() == "SANDBOX") {
		css_file_url = "https://system.sandbox.netsuite.com/core/media/media.nl?id=199133&c=3883006&h=64fa194295cd4ab64d58&mv=iq3kjr6g&_xt=.css&whence=";
	} else if (context.getEnvironment() == "PRODUCTION") {
		css_file_url = "https://system.na1.netsuite.com/core/media/media.nl?id=199133&c=3883006&h=64fa194295cd4ab64d58&mv=iq3kjr6g&_xt=.css&whence=";
	}

	var html = "";
	html += "<link href='" + css_file_url + "'  rel='stylesheet'>";

	html += "<table class='projection-table'>";

	// header
	html += "<tr class='header-row'>";
	
	html += "<td>";
	html += "";
	html += "</td>";
	
	html += "<td width:15px>";
	html += "Practice";
	html += "</td>";
	
	html += "<td width:30px>";
	html += "Sub Practice";
	html += "</td>";
	
	html += "<td width=\"10%\">";
	html += "Role";
	html += "</td>";
	
	html += "<td width=\"10%\">";
	html += "Level";
	html += "</td>";
	
	html += "<td>";
	html += "Location";
	html += "</td>";
		
	
	var today = new Date();
	var currentMonthName = getMonthName(nlapiDateToString(today));
	
	for (var j = 0; j < monthBreakUp.length; j++)
	{
		var months = monthBreakUp[j];
		var s_month_name = getMonthName(months.Start); // get month name
		var s_year = nlapiStringToDate(months.Start)
		s_year = s_year.getFullYear();
		s_month_name = s_month_name+'_'+s_year;
				
		html += "<td>";
		html += s_month_name;
		html += "</td>";
	}
		
	html += "<td>";
	html += "Total";
	html += "</td>";
			
	html += "</tr>";
	
	var a_sub_prac_already_displayed = new Array();
	var i_subpractice_id = 0;
	var a_amount = new Array();
	var a_recognized_revenue = new Array();
	var i_frst_row = 0;
	var i_diplay_frst_column = 0;
	var i_total_row_revenue = 0;
	var i_total_per_row = 0;
	var total_prcnt_aloocated = new Array();
	var b_first_line_copied = 0;
	var f_total_allocated_row = 0;
	
	for (var emp_internal_id in projectWiseRevenue)
	{
		if (projectWiseRevenue[emp_internal_id].sub_practice == sub_prac)
		{
			f_total_allocated_row = 0;
			for (var project_internal_id in projectWiseRevenue[emp_internal_id].RevenueData)
			{
				html += "<tr>";
				
				if(i_diplay_frst_column == 0)
				{
					html += "<td class='label-name'>";
					html += '<b>Effort View';
					html += "</td>";
				}
				else
				{
					html += "<td class='label-name'>";
					html += '<b>';
					html += "</td>";
				}
				
				html += "<td class='label-name' width:15px>";
				html += projectWiseRevenue[emp_internal_id].practice_name;
				html += "</td>";
						
				html += "<td class='label-name' width:30px>";
				html += projectWiseRevenue[emp_internal_id].sub_prac_name;
				html += "</td>";
			
				html += "<td class='label-name' width=\"10%\">";
				html += projectWiseRevenue[emp_internal_id].role_name;
				html += "</td>";
					
				html += "<td class='label-name' width=\"10%\">";
				html += projectWiseRevenue[emp_internal_id].level_name;
				html += "</td>";
				
				html += "<td class='label-name'>";
				html += projectWiseRevenue[emp_internal_id].location_name;
				html += "</td>";
				
				var i_index_mnth = 0;
				for (var month in projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id])
				{
					var a_MonthNames = MonthNames(i_year_project);
					var currentMonthPos = a_MonthNames.indexOf(month);
					var s_current_month = a_MonthNames[currentMonthPos];
					var s_current_mnth_yr = s_current_month.substr(4, 8);
					
					var i_nxt_yr_flg = 0;
					if(currentMonthPos >=12)
					{
						currentMonthPos = currentMonthPos - 12;
						i_nxt_yr_flg = 1;
					}
					
					var i_curr_yr_dis_flag = 0;
					if(parseInt(s_current_mnth_yr) < i_year_project_end && i_prject_end_mnth < currentMonthPos)
					{
						i_curr_yr_dis_flag = 1;
					}
					
					if (((i_project_mnth <= currentMonthPos && ((i_prject_end_mnth >= currentMonthPos || i_curr_yr_dis_flag == 1) && i_nxt_yr_flg == 0) && parseInt(s_current_mnth_yr) <= i_year_project) && parseInt(s_current_mnth_yr) <= i_year_project_end) || (i_prject_end_mnth >= currentMonthPos && parseInt(s_current_mnth_yr) <= i_year_project_end && i_nxt_yr_flg == 1))
					{
						//nlapiLogExecution('audit','currentMonthPos:- ',+currentMonthPos);
						if(b_first_line_copied == 1)
						{
							var f_allocated_prcnt = total_prcnt_aloocated[i_index_mnth];
							f_allocated_prcnt = parseFloat(f_allocated_prcnt) + parseFloat(projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id][month].prcnt_allocated);
							total_prcnt_aloocated[i_index_mnth] = f_allocated_prcnt;
							i_index_mnth++;							
						}
						else
						{
							total_prcnt_aloocated.push(projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id][month].prcnt_allocated);
						}
						
						f_total_allocated_row = parseFloat(projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id][month].prcnt_allocated) + parseFloat(f_total_allocated_row);
						
						html += "<td class='monthly-amount'>";
						html += parseFloat(projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id][month].prcnt_allocated).toFixed(2);
						html += "</td>";
					}
				}
			}
			
			b_first_line_copied = 1;
			
			html += "<td class='monthly-amount'>";
			html += parseFloat(f_total_allocated_row).toFixed(2);
			html += "</td>";
			
			html += "</tr>";
			
			var s_sub_prac_name = projectWiseRevenue[emp_internal_id].sub_prac_name;
			var s_practice_name = projectWiseRevenue[emp_internal_id].practice_name;
			
			i_diplay_frst_column = 1;
		}
	}
	i_diplay_frst_column = 0;
	
	html += "<tr>";
	html += "<td class='label-name'>";
	html += '<b>Sub Total';
	html += "</td>";
	
	html += "<td class='label-name'>";
	html += '<b>'+s_practice_name;
	html += "</td>";
			
	html += "<td class='label-name'>";
	html += '<b>'+s_sub_prac_name;
	html += "</td>";

	html += "<td class='label-name'>";
	html += '';
	html += "</td>";
		
	html += "<td class='label-name'>";
	html += '';
	html += "</td>";
	
	html += "<td class='label-name'>";
	html += '';
	html += "</td>";
	
	var f_total_row_allocation = 0;
	for(var i_index_total_allocated=0; i_index_total_allocated<total_prcnt_aloocated.length; i_index_total_allocated++)
	{
		html += "<td class='monthly-amount'>";
		html += '<b>'+parseFloat(total_prcnt_aloocated[i_index_total_allocated]).toFixed(2);
		html += "</td>";
		
		f_total_row_allocation = parseFloat(f_total_row_allocation) + parseFloat(total_prcnt_aloocated[i_index_total_allocated]);
	}
	
	html += "<td class='monthly-amount'>";
	html += '<b>'+parseFloat(f_total_row_allocation).toFixed(2);
	html += "</td>";
		
	html += "</tr>";
	
	
	for ( var emp_internal_id in projectWiseRevenue) {
		
		for ( var project_internal_id in projectWiseRevenue[emp_internal_id].RevenueData) {
			
			if(projectWiseRevenue[emp_internal_id].sub_practice == sub_prac)
			{
				html += "<tr>";
				
				if (i_diplay_frst_column == 0)
				{
					html += "<td class='label-name'>";
					html += '<b>Cost View';
					html += "</td>";
				}
				else
				{
					html += "<td class='label-name'>";
					html += '<b>';
					html += "</td>";
				}
				
				html += "<td class='label-name'>";
				html += projectWiseRevenue[emp_internal_id].practice_name;
				html += "</td>";
						
				html += "<td class='label-name'>";
				html += projectWiseRevenue[emp_internal_id].sub_prac_name;
				html += "</td>";
			
				html += "<td class='label-name'>";
				html += projectWiseRevenue[emp_internal_id].role_name;
				html += "</td>";
					
				html += "<td class='label-name'>";
				html += projectWiseRevenue[emp_internal_id].level_name;
				html += "</td>";
				
				html += "<td class='label-name'>";
				html += projectWiseRevenue[emp_internal_id].location_name;
				html += "</td>";
					
				var i_sub_practice_internal_id = 0;
				var i_amt = 0;
				for (var month in projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id])
				{
					var a_MonthNames = MonthNames(i_year_project);
					var currentMonthPos = a_MonthNames.indexOf(month);
					var s_current_month = a_MonthNames[currentMonthPos];
					var s_current_mnth_yr = s_current_month.substr(4, 8);
					
					var i_nxt_yr_flg = 0;
					if(currentMonthPos >=12)
					{
						currentMonthPos = currentMonthPos - 12;
						i_nxt_yr_flg = 1;
					}
					
					var i_curr_yr_dis_flag = 0;
					if(parseInt(s_current_mnth_yr) < i_year_project_end && i_prject_end_mnth < currentMonthPos)
					{
						i_curr_yr_dis_flag = 1;
					}
					
					if (((i_project_mnth <= currentMonthPos && ((i_prject_end_mnth >= currentMonthPos || i_curr_yr_dis_flag == 1) && i_nxt_yr_flg == 0) && parseInt(s_current_mnth_yr) <= i_year_project) && parseInt(s_current_mnth_yr) <= i_year_project_end) || (i_prject_end_mnth >= currentMonthPos && parseInt(s_current_mnth_yr) <= i_year_project_end && i_nxt_yr_flg == 1))
					{
						if (mode == 2)
						{
							
							html += "<td class='monthly-amount'>";
							var total_revenue_format = parseFloat(projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id][month].Amount).toFixed(1);
							html += ''+s_currency_symbol_proj+' '+format2(total_revenue_format);
							
							if(i_frst_row == 0)
							{
								//nlapiLogExecution('audit','inside push recog amnt:- '+projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id][month].recognized_revenue);
								a_amount.push(total_revenue_format);
								a_recognized_revenue.push(projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id][month].recognized_revenue);
							}
							else
							{
								var amnt_mnth = a_amount[i_amt];
								amnt_mnth = parseFloat(amnt_mnth) + parseFloat(projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id][month].Amount);
								
								a_amount[i_amt] = amnt_mnth;
								
								var i_existing_recognised_amount = a_recognized_revenue[i_amt];
								//nlapiLogExecution('audit','month name:- '+month);
								//nlapiLogExecution('audit','index:- '+i_amt+'::existing rev:- '+i_existing_recognised_amount,'Rev REcg:- '+projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id][month].recognized_revenue);
								i_existing_recognised_amount = parseFloat(i_existing_recognised_amount) + parseFloat(projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id][month].recognized_revenue);
								
								a_recognized_revenue[i_amt] = i_existing_recognised_amount;
								
								i_amt++;
							}
							i_total_row_revenue = parseFloat(i_total_row_revenue) + parseFloat(projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id][month].Amount);
							i_total_per_row = parseFloat(i_total_per_row) + parseFloat(projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id][month].Amount);
						}
						
						html += "</td>";
					}
				}
				
				if (mode == 2)
				{
					html += "<td class='monthly-amount'>";
					//html += ''+s_currency_symbol_usd+' '+format2(projectWiseRevenue[emp_internal_id].total_revenue_for_tenure);
					html += ''+s_currency_symbol_proj+' '+format2(i_total_per_row);
					html += "</td>";
					i_total_per_row = 0;
					//i_total_row_revenue = parseFloat(i_total_row_revenue) + parseFloat(projectWiseRevenue[emp_internal_id].total_revenue_for_tenure);
				}
				
				i_frst_row = 1;
	
				html += "</tr>";
				
				i_diplay_frst_column = 1;
				
				var f_revenue_share = projectWiseRevenue[emp_internal_id].revenue_share;
			}			
		}
	}
	
	var s_sub_prac_name = '';
	for (var emp_internal_id in projectWiseRevenue)
	{
		if (projectWiseRevenue[emp_internal_id].sub_practice == sub_prac)
		{
			s_sub_prac_name = projectWiseRevenue[emp_internal_id].sub_prac_name;
			s_practice_name = projectWiseRevenue[emp_internal_id].practice_name;
			//projectWiseRevenue[emp_internal_id].revenue_share_revised = i_total_row_revenue;
			//projectWiseRevenue[emp_internal_id].total_revenue_for_tenure = i_total_row_revenue;
		}
	}
	
	html += "</tr background-color: #05B04B;>";
	
	html += "<td class='label-name'>";
	html += '<b>Sub total';
	html += "</td>";
				
	html += "<td class='label-name'>";
	html += '<b>'+s_practice_name;
	html += "</td>";
			
	html += "<td class='label-name'>";
	html += '<b>'+s_sub_prac_name;
	html += "</td>";

	html += "<td class='label-name'>";
	html += '';
	html += "</td>";
		
	html += "<td class='label-name'>";
	html += '';
	html += "</td>";
	
	html += "<td class='label-name'>";
	html += '';
	html += "</td>";
	if(flag_counter==0){
	for (var i_amt = 0; i_amt < a_amount.length; i_amt++)
	{
		f_total_cost_overall_prac[i_amt] = 0;
		
	}
	}
	for (var i_amt = 0; i_amt < a_amount.length; i_amt++)
	{
		html += "<td class='projected-amount'>";
		html += '<b>'+s_currency_symbol_proj+' '+format2(a_amount[i_amt]);
		html += "</td>";
		f_total_cost_overall_prac[i_amt] = parseFloat(a_amount[i_amt]) + parseFloat(f_total_cost_overall_prac[i_amt]);
		flag_counter=1;
	}
	
	html += "<td class='projected-amount'>";
	html += '<b>'+s_currency_symbol_proj+' '+format2(i_total_row_revenue);
	html += "</td>";
	
	html += "</tr>";
	
	// Percent row
	html += "</tr>";
	
	html += "<td class='label-name'>";
	html += '<b>Monthly % Cost Completion';
	html += "</td>";
	
	html += "<td class='label-name'>";
	html += '<b>'+s_practice_name;
	html += "</td>";
			
	html += "<td class='label-name'>";
	html += '<b>'+s_sub_prac_name;
	html += "</td>";

	html += "<td class='label-name'>";
	html += '';
	html += "</td>";
		
	html += "<td class='label-name'>";
	html += '';
	html += "</td>";
	
	html += "<td class='label-name'>";
	html += '';
	html += "</td>";
	
	var f_total_prcnt = 0;
	for (var i_amt = 0; i_amt < a_amount.length; i_amt++)
	{
		var f_prcnt_revenue = parseFloat(a_amount[i_amt]) / parseFloat(i_total_row_revenue);
		f_prcnt_revenue = f_prcnt_revenue * 100;
		if(!f_prcnt_revenue)
			f_prcnt_revenue = 0;
			
		f_total_prcnt = parseFloat(f_prcnt_revenue) + parseFloat(f_total_prcnt);
		
		html += "<td class='projected-amount'>";
		html += '<b>'+parseFloat(f_prcnt_revenue).toFixed(1)+' %';
		html += "</td>";
	}
	
	html += "<td class='projected-amount'>";
	html += '<b>'+parseFloat(f_total_prcnt).toFixed(1)+' %';
	html += "</td>";
	
	html += "</tr>";
	
	//Total reveue recognized per month row
	html += "<tr>";
	
	html += "<td class='label-name'>";
	html += '<b>Revenue to be reconized/month';
	html += "</td>";
	
	html += "<td class='label-name'>";
	html += '<b>'+s_practice_name;
	html += "</td>";
			
	html += "<td class='label-name'>";
	html += '<b>'+s_sub_prac_name;
	html += "</td>";

	html += "<td class='label-name'>";
	html += '';
	html += "</td>";
		
	html += "<td class='label-name'>";
	html += '';
	html += "</td>";
	
	html += "<td class='label-name'>";
	html += '';
	html += "</td>";
	
	var d_today_date = new Date();
	var i_current_month = d_today_date.getMonth();
	var i_current_year = d_today_date.getFullYear();
	
	var f_total_revenue = 0;
	var f_revenue_amount_till_current_month = 0;
	for (var i_amt = 0; i_amt < a_amount.length; i_amt++)
	{
		var f_prcnt_revenue = parseFloat(a_amount[i_amt]) / parseFloat(i_total_row_revenue);
		f_prcnt_revenue = f_prcnt_revenue * 100;
		
		var f_revenue_amount = (parseFloat(f_prcnt_revenue) * parseFloat(f_revenue_share)) / 100;
		if(!f_revenue_amount)
			f_revenue_amount = 0;
		
		if (parseInt(i_year_project) != parseInt(i_current_year))
		{
			var i_total_project_tenure = 11 + parseInt(i_current_month);
			var f_total_prev_mnths = parseFloat(i_total_project_tenure) - parseFloat(i_project_mnth);
			f_total_prev_mnths = parseFloat(f_total_prev_mnths) + parseFloat(1);
		}
		else
		{
			var f_total_prev_mnths = parseFloat(i_current_month) - parseFloat(i_project_mnth);
		}
		
		if(i_amt <= f_total_prev_mnths)
		{
			//nlapiLogExecution('audit','f_revenue_amount:- '+f_revenue_amount,'a_amount.length:- '+a_amount.length);
			f_revenue_amount_till_current_month = parseFloat(f_revenue_amount) + parseFloat(f_revenue_amount_till_current_month);
		}
		
		f_total_revenue = parseFloat(f_revenue_amount) + parseFloat(f_total_revenue);
		
		html += "<td class='projected-amount'>";
		html += '<b>'+s_currency_symbol_proj+' '+format2(f_revenue_amount);
		
		html += "</td>";
	}

	
	html += "</tr>";
	
	html += "</table>";
	
	html += "<table class='projection-table'>";
	
	html += "<tr class='header-row'>";
	
	html += "<td>";
	html += "";
	html += "</td>";
	
	html += "<td width:15px>";
	html += "Practice";
	html += "</td>";
	
	html += "<td width:30px>";
	html += "Sub Practice";
	html += "</td>";
	
	var today = new Date();
	var currentMonthName = getMonthName(nlapiDateToString(today));
	
	
	for (var i_amt = i_current_month+1; i_amt < a_amount.length; i_amt++)
	{
		var f_prcnt_revenue = parseFloat(a_amount[i_amt]) / parseFloat(i_total_row_revenue);
		f_prcnt_revenue = f_prcnt_revenue * 100;
		
		var f_revenue_amount = (parseFloat(f_prcnt_revenue) * parseFloat(f_revenue_share)) / 100;
		if(!f_revenue_amount)
			f_revenue_amount = 0;
		
		if(i_amt < i_current_month)
		{
			//nlapiLogExecution('audit','i_current_month:- '+i_current_month,'a_amount.length:- '+a_amount.length);
		//	f_revenue_amount_till_current_month = parseFloat(f_revenue_amount) + parseFloat(f_revenue_amount_till_current_month);
			if(!f_revenue_amount_till_current_month)
				f_revenue_amount_till_current_month = 0;
		}
		
		f_total_revenue = parseFloat(f_revenue_amount) + parseFloat(f_total_revenue);
		
		html += "<td class='projected-amount'>";
		html += '<b>'+s_currency_symbol_proj+' '+format2(f_revenue_amount);
		html += "</td>";
		
		//i_total_actual_revenue_recognized = parseFloat(i_total_actual_revenue_recognized) + parseFloat(f_revenue_amount);
	}
	
	
	html += "<td class='projected-amount'>";
	//html += '<b>'+s_currency_symbol_proj+' '+format2(i_total_actual_revenue_recognized);;
	html += "</td>";
	
	html += "</tr>";
	
	
	
	//if(i_total_revenue_recognized_ytd == 0)
		i_total_revenue_recognized_ytd = f_revenue_share;
	
	html += "<td class='monthly-amount'>";
	html += s_currency_symbol_proj+' '+format2(i_total_revenue_recognized_ytd);
	html += "</td>";
	
	html += "</tr>";
	
	html += "</table>";
	

	//a_recognized_revenue_total_proj_level = a_recognized_revenue;
	
	return f_total_cost_overall_prac;
}
function generate_total_cost_plan(projectAllocationPlan,monthBreakUp,i_year_project,i_project_mnth,mode,sub_prac,s_currency_symbol_proj,s_currency_symbol_usd,i_year_project_end,i_prject_end_mnth,fld_true_up_msg)
{
	var css_file_url = "";

	var context = nlapiGetContext();
	if (context.getEnvironment() == "SANDBOX") {
		css_file_url = "https://system.sandbox.netsuite.com/core/media/media.nl?id=199133&c=3883006&h=64fa194295cd4ab64d58&mv=iq3kjr6g&_xt=.css&whence=";
	} else if (context.getEnvironment() == "PRODUCTION") {
		css_file_url = "https://system.na1.netsuite.com/core/media/media.nl?id=199133&c=3883006&h=64fa194295cd4ab64d58&mv=iq3kjr6g&_xt=.css&whence=";
	}

	var html = "";
	html += "<link href='" + css_file_url + "'  rel='stylesheet'>";
	// header
	var today = new Date();
	var currentMonthName = getMonthName(nlapiDateToString(today));
	
	var a_sub_prac_already_displayed = new Array();
	var i_subpractice_id = 0;
	var a_amount = new Array();
	var a_recognized_revenue = new Array();
	var i_frst_row = 0;
	var i_diplay_frst_column = 0;
	var i_total_row_revenue = 0;
	var i_total_per_row = 0;
	var total_prcnt_aloocated = new Array();
	var b_first_line_copied = 0;
	var f_total_allocated_row = 0;
	
	for (var emp_internal_id in projectAllocationPlan)
	{
		if (projectAllocationPlan[emp_internal_id].sub_practice == sub_prac)
		{
			f_total_allocated_row = 0;
			for (var project_internal_id in projectAllocationPlan[emp_internal_id].RevenueDataPlan)
			{
				var i_index_mnth = 0;
				for (var month in projectAllocationPlan[emp_internal_id].RevenueDataPlan[project_internal_id])
				{
					var a_MonthNames = MonthNames(i_year_project);
					var currentMonthPos = a_MonthNames.indexOf(month);
					var s_current_month = a_MonthNames[currentMonthPos];
					var s_current_mnth_yr = s_current_month.substr(4, 8);
					
					var i_nxt_yr_flg = 0;
					if(currentMonthPos >=12)
					{
						currentMonthPos = currentMonthPos - 12;
						i_nxt_yr_flg = 1;
					}
					
					var i_curr_yr_dis_flag = 0;
					if(parseInt(s_current_mnth_yr) < i_year_project_end && i_prject_end_mnth < currentMonthPos)
					{
						i_curr_yr_dis_flag = 1;
					}
					
					if (((i_project_mnth <= currentMonthPos && ((i_prject_end_mnth >= currentMonthPos || i_curr_yr_dis_flag == 1) && i_nxt_yr_flg == 0) && parseInt(s_current_mnth_yr) <= i_year_project) && parseInt(s_current_mnth_yr) <= i_year_project_end) || (i_prject_end_mnth >= currentMonthPos && parseInt(s_current_mnth_yr) <= i_year_project_end && i_nxt_yr_flg == 1))
					{
						//nlapiLogExecution('audit','currentMonthPos:- ',+currentMonthPos);
						if(b_first_line_copied == 1)
						{
							var f_allocated_prcnt = total_prcnt_aloocated[i_index_mnth];
							f_allocated_prcnt = parseFloat(f_allocated_prcnt) + parseFloat(projectAllocationPlan[emp_internal_id].RevenueDataPlan[project_internal_id][month].prcnt_allocated);
							total_prcnt_aloocated[i_index_mnth] = f_allocated_prcnt;
							i_index_mnth++;							
						}
						else
						{
							total_prcnt_aloocated.push(projectAllocationPlan[emp_internal_id].RevenueDataPlan[project_internal_id][month].prcnt_allocated);
						}
						
						f_total_allocated_row = parseFloat(projectAllocationPlan[emp_internal_id].RevenueDataPlan[project_internal_id][month].prcnt_allocated) + parseFloat(f_total_allocated_row);
					}
				}
			}
			
			b_first_line_copied = 1;
			
			var s_sub_prac_name = projectAllocationPlan[emp_internal_id].sub_prac_name;
			var s_practice_name = projectAllocationPlan[emp_internal_id].practice_name;
			
			i_diplay_frst_column = 1;
		}
	}
	i_diplay_frst_column = 0;		
	var f_total_row_allocation = 0;
	for(var i_index_total_allocated=0; i_index_total_allocated<total_prcnt_aloocated.length; i_index_total_allocated++)
	{		
		f_total_row_allocation = parseFloat(f_total_row_allocation) + parseFloat(total_prcnt_aloocated[i_index_total_allocated]);
	}
	for ( var emp_internal_id in projectAllocationPlan) {
		
		for ( var project_internal_id in projectAllocationPlan[emp_internal_id].RevenueDataPlan) {
			
			if(projectAllocationPlan[emp_internal_id].sub_practice == sub_prac)
			{
				if (i_diplay_frst_column == 0)
				{	
					//html += '<b>Cost View';
				}
				else
				{
		
				}					
				var i_sub_practice_internal_id = 0;
				var i_amt = 0;
				for (var month in projectAllocationPlan[emp_internal_id].RevenueDataPlan[project_internal_id])
				{
					var a_MonthNames = MonthNames(i_year_project);
					var currentMonthPos = a_MonthNames.indexOf(month);
					var s_current_month = a_MonthNames[currentMonthPos];
					var s_current_mnth_yr = s_current_month.substr(4, 8);
					
					var i_nxt_yr_flg = 0;
					if(currentMonthPos >=12)
					{
						currentMonthPos = currentMonthPos - 12;
						i_nxt_yr_flg = 1;
					}
					
					var i_curr_yr_dis_flag = 0;
					if(parseInt(s_current_mnth_yr) < i_year_project_end && i_prject_end_mnth < currentMonthPos)
					{
						i_curr_yr_dis_flag = 1;
					}
					
					if (((i_project_mnth <= currentMonthPos && ((i_prject_end_mnth >= currentMonthPos || i_curr_yr_dis_flag == 1) && i_nxt_yr_flg == 0) && parseInt(s_current_mnth_yr) <= i_year_project) && parseInt(s_current_mnth_yr) <= i_year_project_end) || (i_prject_end_mnth >= currentMonthPos && parseInt(s_current_mnth_yr) <= i_year_project_end && i_nxt_yr_flg == 1))
					{
						if (mode == 2)
						{
							var total_revenue_format = parseFloat(projectAllocationPlan[emp_internal_id].RevenueDataPlan[project_internal_id][month].Amount).toFixed(1);
							if(i_frst_row == 0)
							{
								//nlapiLogExecution('audit','inside push recog amnt:- '+projectAllocationPlan[emp_internal_id].RevenueDataPlan[project_internal_id][month].recognized_revenue);
								a_amount.push(total_revenue_format);
								a_recognized_revenue.push(projectAllocationPlan[emp_internal_id].RevenueDataPlan[project_internal_id][month].recognized_revenue);
							}
							else
							{
								var amnt_mnth = a_amount[i_amt];
								amnt_mnth = parseFloat(amnt_mnth) + parseFloat(projectAllocationPlan[emp_internal_id].RevenueDataPlan[project_internal_id][month].Amount);
								
								a_amount[i_amt] = amnt_mnth;
								
								var i_existing_recognised_amount = a_recognized_revenue[i_amt];
								//nlapiLogExecution('audit','month name:- '+month);
								//nlapiLogExecution('audit','index:- '+i_amt+'::existing rev:- '+i_existing_recognised_amount,'Rev REcg:- '+projectAllocationPlan[emp_internal_id].RevenueDataPlan[project_internal_id][month].recognized_revenue);
								i_existing_recognised_amount = parseFloat(i_existing_recognised_amount) + parseFloat(projectAllocationPlan[emp_internal_id].RevenueDataPlan[project_internal_id][month].recognized_revenue);
								
								a_recognized_revenue[i_amt] = i_existing_recognised_amount;
								
								i_amt++;
							}
							i_total_row_revenue = parseFloat(i_total_row_revenue) + parseFloat(projectAllocationPlan[emp_internal_id].RevenueDataPlan[project_internal_id][month].Amount);
							i_total_per_row = parseFloat(i_total_per_row) + parseFloat(projectAllocationPlan[emp_internal_id].RevenueDataPlan[project_internal_id][month].Amount);
						}		
					}
				}
				
				if (mode == 2)
				{
				
					i_total_per_row = 0;
					
				}
				
				i_frst_row = 1;
				i_diplay_frst_column = 1;
				var f_revenue_share = projectAllocationPlan[emp_internal_id].revenue_share;
			}			
		}
	}
	
	var s_sub_prac_name = '';
	for (var emp_internal_id in projectAllocationPlan)
	{
		if (projectAllocationPlan[emp_internal_id].sub_practice == sub_prac)
		{
			s_sub_prac_name = projectAllocationPlan[emp_internal_id].sub_prac_name;
			s_practice_name = projectAllocationPlan[emp_internal_id].practice_name;
			//projectAllocationPlan[emp_internal_id].revenue_share_revised = i_total_row_revenue;
			//projectAllocationPlan[emp_internal_id].total_revenue_for_tenure = i_total_row_revenue;
		}
	}
	if(flag_counter_plan==0){
	for (var i_amt = 0; i_amt < a_amount.length; i_amt++)
	{
		f_total_cost_overall_prac_plan[i_amt] = 0;
		
	}
	}
	for (var i_amt = 0; i_amt < a_amount.length; i_amt++)
	{
	
		f_total_cost_overall_prac_plan[i_amt] = parseFloat(a_amount[i_amt]) + parseFloat(f_total_cost_overall_prac_plan[i_amt]);
		flag_counter_plan=1;
	}
			
	//Total reveue recognized per month row
	var d_today_date = new Date();
	var i_current_month = d_today_date.getMonth();
	var i_current_year = d_today_date.getFullYear();
	var f_total_revenue = 0;
	var f_revenue_amount_till_current_month = 0;
	for (var i_amt = 0; i_amt < a_amount.length; i_amt++)
	{
		var f_prcnt_revenue = parseFloat(a_amount[i_amt]) / parseFloat(i_total_row_revenue);
		f_prcnt_revenue = f_prcnt_revenue * 100;
		
		var f_revenue_amount = (parseFloat(f_prcnt_revenue) * parseFloat(f_revenue_share)) / 100;
		if(!f_revenue_amount)
			f_revenue_amount = 0;
		
		if (parseInt(i_year_project) != parseInt(i_current_year))
		{
			var i_total_project_tenure = 11 + parseInt(i_current_month);
			var f_total_prev_mnths = parseFloat(i_total_project_tenure) - parseFloat(i_project_mnth);
			f_total_prev_mnths = parseFloat(f_total_prev_mnths) + parseFloat(1);
		}
		else
		{
			var f_total_prev_mnths = parseFloat(i_current_month) - parseFloat(i_project_mnth);
		}
		
		if(i_amt <= f_total_prev_mnths)
		{
			//nlapiLogExecution('audit','f_revenue_amount:- '+f_revenue_amount,'a_amount.length:- '+a_amount.length);
			f_revenue_amount_till_current_month = parseFloat(f_revenue_amount) + parseFloat(f_revenue_amount_till_current_month);
		}
		
		f_total_revenue = parseFloat(f_revenue_amount) + parseFloat(f_total_revenue);
		
	}

	var today = new Date();
	var currentMonthName = getMonthName(nlapiDateToString(today));
	
	
	for (var i_amt = i_current_month+1; i_amt < a_amount.length; i_amt++)
	{
		var f_prcnt_revenue = parseFloat(a_amount[i_amt]) / parseFloat(i_total_row_revenue);
		f_prcnt_revenue = f_prcnt_revenue * 100;
		
		var f_revenue_amount = (parseFloat(f_prcnt_revenue) * parseFloat(f_revenue_share)) / 100;
		if(!f_revenue_amount)
			f_revenue_amount = 0;
		
		if(i_amt < i_current_month)
		{
			//nlapiLogExecution('audit','i_current_month:- '+i_current_month,'a_amount.length:- '+a_amount.length);
		//	f_revenue_amount_till_current_month = parseFloat(f_revenue_amount) + parseFloat(f_revenue_amount_till_current_month);
			if(!f_revenue_amount_till_current_month)
				f_revenue_amount_till_current_month = 0;
		}
		
		f_total_revenue = parseFloat(f_revenue_amount) + parseFloat(f_total_revenue);
	}	
	return f_total_cost_overall_prac_plan;
}
function _logValidation(value) 
{
 if(value!='null' && value != null && value != null && value != '' && value != undefined && value != undefined && value != 'undefined' && value!= 'undefined'&& value!= 'NaN' && value != NaN) 
 {
  return true;
 }
 else 
 { 
  return false;
 }
 }
 function getMonthName_FromMonthNumber(month_number)
{
	var a_month_name = [ "JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JUL", "AUG",
	        "SEP", "OCT", "NOV", "DEC" ];
								
	return a_month_name[month_number];
}
function find_prac_exist(a_current_pract,i_prac_to_find,i_level_to_find)
{
	var i_prac_present = 0;
	for(var i_prac_find=0; i_prac_find<a_current_pract.length; i_prac_find++)
	{
		if(a_current_pract[i_prac_find].i_sub_practice == i_prac_to_find && a_current_pract[i_prac_find].i_level == i_level_to_find)
		{
			i_prac_present = 1;
		}
	}
	
	return i_prac_present;
}
function findAllocationPracticePresent(a_proj_prac_details, level, i_resource_location)
{
	for(var i_index_arr_srchng=0; i_index_arr_srchng<a_proj_prac_details.length; i_index_arr_srchng++)
	{
		if((a_proj_prac_details[i_index_arr_srchng].s_emp_level) == (level))
		{
			if((a_proj_prac_details[i_index_arr_srchng].i_resource_location) == (i_resource_location))
			{
							//nlapiLogExecution('audit','loc:- '+i_resource_location);
				return i_index_arr_srchng;
			}
					
		}
			
		
	}
	
	return -1;
}