/**
 * Module Description
 * 
 * Version    Date            	Author           		Remarks
 * 1.00       16 March 2020     Prabhat Gupta
 *	1.1		   07-12-2020    	Shravan 				All practice HRBP fields and used commented by Shravan   
 */

/**
 * @param {Object} dataIn Parameter object
 * @returns {Object} Output object
 */
 function getRESTlet(dataIn) {

	return {};
}

/**
 * @param {Object} dataIn Parameter object
 * @returns {Object} Output object
 */
function postRESTlet(dataIn) {

	try {
		
        var response = new Response();
        //nlapiLogExecution('debug', 'dataIn', JSON.stringify(dataIn));
        var currentDateAndTime = sysDate();
		//Log for current date



		var receivedDate = dataIn.lastUpdatedTimestamp;
		//nlapiLogExecution('DEBUG', 'Current Date', 'receivedDate...' + receivedDate);
		nlapiLogExecution('DEBUG', 'Current Date', 'currentDateAndTime...' + currentDateAndTime);

		//var receivedDate = '7/12/2019 1:00 am';
		//Check if the timestamp is empty
		if (!_logValidation(receivedDate)) {          // for empty timestanp in this we will give whole data as a response  NIS-1293 prabhat gupta 06/04/2020
			var filters = new Array();
			filters = [
				
				  ["isinactive","is","F"]			 
				
				];
		//	nlapiLogExecution('debug', 'filters ', filters);
		}else{
          
          throw new Error("request with only empty timestamp")
        }
		
		// created search by grouping 
		var searchResults = searchRecord("department",null,
				filters, 
				[
				      
					  
					  new nlobjSearchColumn("name").setSort(false), 
					   new nlobjSearchColumn("custrecord_parent_practice"), 
					 //  new nlobjSearchColumn("internalid","CUSTRECORD_PARENT_PRACTICE",null), 
					   new nlobjSearchColumn("custrecord_practicehead"), 
					   new nlobjSearchColumn("email","CUSTRECORD_PRACTICEHEAD",null), 
					   new nlobjSearchColumn("custrecord_onsite_practice_head"), 
					   new nlobjSearchColumn("email","CUSTRECORD_ONSITE_PRACTICE_HEAD",null), 
					 //  new nlobjSearchColumn("custrecord_hrbusinesspartner"), 
					 //  new nlobjSearchColumn("email","CUSTRECORD_HRBUSINESSPARTNER",null), 
					   new nlobjSearchColumn("isinactive"), 
					   new nlobjSearchColumn("custrecord_practice_type_ticker")
										
				]
				);	


						
		
		
		var practiceData = [];
        
		if (searchResults) {
			
			
			for(var i=0; i<searchResults.length; i++){    
				
		    var practice = {};
                   
			var practiceInternalId = searchResults[i].getId();
			var practiceName = searchResults[i].getValue("name");
			var parentPracticeId = searchResults[i].getValue("custrecord_parent_practice");
			var parentPracticeName = searchResults[i].getText("custrecord_parent_practice");
			var practiceHeadId = searchResults[i].getValue("custrecord_practicehead") || '';
			var practiceHeadName = searchResults[i].getText("custrecord_practicehead")  || '';
			var practiceHeadEmail =  searchResults[i].getValue("email","CUSTRECORD_PRACTICEHEAD",null) || '';
			var onsiteHeadId =  searchResults[i].getValue("custrecord_onsite_practice_head") || '';
			var onsiteHeadName =  searchResults[i].getText("custrecord_onsite_practice_head") || '';
			var onsiteHeadEmail =  searchResults[i].getValue("email","CUSTRECORD_ONSITE_PRACTICE_HEAD",null) || '';
		//	var hrbpId =  searchResults[i].getValue("custrecord_hrbusinesspartner") || '';
		//	var hrbpName =  searchResults[i].getText("custrecord_hrbusinesspartner") || '';
		//	var hrbpEmail =  searchResults[i].getValue("email","CUSTRECORD_HRBUSINESSPARTNER",null) || '';
			var inactiveStatus =  searchResults[i].getValue("isinactive");
           
			
			
			
			practice.practiceInternalId = practiceInternalId;
			practice.practiceName = practiceName;
			practice.parentPracticeId = parentPracticeId;
			practice.parentPracticeName = parentPracticeName;
			practice.practiceHeadId = practiceHeadId;
			practice.practiceHeadName = practiceHeadName;
			practice.practiceHeadEmail = practiceHeadEmail;
			practice.onsiteHeadId = onsiteHeadId;
			practice.onsiteHeadName = onsiteHeadName;
			practice.onsiteHeadEmail = onsiteHeadEmail;
			//practice.hrbpId = hrbpId;
			//practice.hrbpName = hrbpName;
			//practice.hrbpEmail = hrbpEmail;
			practice.inactiveStatus = inactiveStatus;
			
			
			
				
				
			practiceData.push(practice);
			
		    }
		}
		

		response.timeStamp = currentDateAndTime;
		response.data = practiceData;
		//response.data = dataRow;
		response.status = true;
	//	nlapiLogExecution('debug', 'response',JSON.stringify(response));
	} catch (err) {
		nlapiLogExecution('ERROR', 'postRESTlet', err);
		response.timeStamp = '';
		response.data = err;
		response.status = false;
	}
	return response;

}
//validate blank entries
function _logValidation(value) {
	if (value != null && value.toString() != null && value != '' && value != undefined && value.toString() != undefined && value != 'undefined' && value.toString() != 'undefined' && value.toString() != 'NaN' && value != NaN) {
		return true;
	} else {
		return false;
	}
}


//Returns timestamp
function timestamp() {
	var str = "";

	var currentTime = new Date();
	var hours = currentTime.getHours();
	var minutes = currentTime.getMinutes();
	var seconds = currentTime.getSeconds();
	var meridian = "";
	if (hours > 12) {
		meridian += "pm";
	} else {
		meridian += "am";
	}
	if (hours > 12) {

		hours = hours - 12;
	}
	if (minutes < 10) {
		minutes = "0" + minutes;
	}
	if (seconds < 10) {
		seconds = "0" + seconds;
	}
	//str += hours + ":" + minutes + ":" + seconds + " ";
	str += hours + ":" + minutes + " ";
	return str + meridian;
}

function Response() {
	this.status = "";
	this.timeStamp = "";
	this.data = "";

}

//Search function for results more than 1000
function searchRecord(recordType, savedSearch, arrFilters, arrColumns,
		filterExpression) {

	try {
		var search = null;

		// if a saved search is provided, load it and add the filters and
		// columns
		if (savedSearch) {
			search = nlapiLoadSearch(recordType, savedSearch);

			if (isArrayNotEmpty(arrFilters)) {
				search.addFilters(arrFilters);
			}

			if (isArrayNotEmpty(arrColumns)) {
				search.addColumns(arrColumns);
			}

			if (isArrayNotEmpty(filterExpression)) {
				search.setFilterExpression(filterExpression);
			}
		}
		// create a new search
		else {
			search = nlapiCreateSearch(recordType, arrFilters, arrColumns);
		}

		// run search
		var resultSet = search.runSearch();

		// iterate through the search and get all data 1000 at a time
		var searchResultCount = 0;
		var resultSlice = null;
		var searchResult = [];

		do {
			resultSlice = resultSet.getResults(searchResultCount,
					searchResultCount + 1000);

			if (resultSlice) {

				resultSlice.forEach(function(result) {

					searchResult.push(result);
					searchResultCount++;
				});
			}
		} while (isArrayNotEmpty(resultSlice) && resultSlice.length >= 1000);

		return searchResult;
	} catch (err) {
		nlapiLogExecution('ERROR', 'searchRecord', err);
		throw err;
	}
}



function isEmpty(value) {

	return value == null || value == "" || typeof(value) == undefined;
}

function isNotEmpty(value) {

	return !isEmpty(value);
}

function isArrayEmpty(argArray) {

	return !isArrayNotEmpty(argArray);
}

function isArrayNotEmpty(argArray) {

	return (isNotEmpty(argArray) && argArray.length > 0);
}
//function to format date object into NetSuite's mm/dd/yyyy format.
function formatNSDate(dateObj) {
	if (dateObj) {
		var nsFormatDate = dateObj.getFullYear() + '-' + addZeros(dateObj.getMonth() + 1, 2) + '-' + addZeros(dateObj.getDate(), 2);
		return nsFormatDate;
	}
	return null;
}

//function to add leading zeros on date parts.
function addZeros(num, len) {
	var str = num.toString();
	while (str.length < len) {
		str = '0' + str;
	}
	return str;
}