/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       07 Nov 2019     Jyoti Shinde
 *
 */

/**
 * @param {Object} dataIn Parameter object
 * @returns {Object} Output object
 */
//SB - https://3883006-sb1.restlets.api.netsuite.com/app/site/hosting/restlet.nl?script=2021&deploy=1
function postRESTlet(dataIn) {
    try {

        nlapiLogExecution("DEBUG", "dataIn", JSON.stringify(dataIn));
        
		var account = dataIn.accountInternalId;
        var a_resorce = dataIn.addDMList;
        var deleteResources = dataIn.deleteDMList;
        var user = dataIn.userEmailId;
		var i_user = getUserUsingEmailId(user);
		var status = false;
        nlapiLogExecution('DEBUG', 'account', account);
        nlapiLogExecution("DEBUG", "resources", JSON.stringify(deleteResources));
        if(account && a_resorce) 
		{
			nlapiLogExecution('debug', 'a_resorce ', JSON.stringify(a_resorce));           
		    for(var i=0;i<a_resorce.length;i++)
			{
				var recObj = nlapiCreateRecord('customrecord_fuel_delivery_anchor');
				recObj.setFieldValue('custrecord_fuel_anchor_practice', a_resorce[i].practiceInternalId);
				recObj.setFieldValue('custrecord_fuel_anchor_customer', account);
				recObj.setFieldValue('custrecord_fuel_anchor_employee', a_resorce[i].employeeInternalId);
				recObj.setFieldValue('custrecord_fuel_anchor_created_by', i_user);
				var recId = nlapiSubmitRecord(recObj);
				nlapiLogExecution('debug', 'recId ', recId);
			}
			status = true;
        } 
		if(account && deleteResources)
		{
			nlapiLogExecution('debug', 'deleteResources ', deleteResources);
			for(var i=0;i<deleteResources.length;i++)
			{
				var customrecord_fuel_delivery_anchorSearch = nlapiSearchRecord("customrecord_fuel_delivery_anchor",null,
				[
				   ["custrecord_fuel_anchor_employee","anyof",deleteResources[i].employeeInternalId], 
				   "AND", 
				   ["custrecord_fuel_anchor_customer","anyof",account]/*, 
				   "AND", 
				   ["custrecord_fuel_anchor_practice","anyof",deleteResources[i].practiceInternalId]*/
				], 
				[
				   new nlobjSearchColumn("internalid")  
				]
				);
				if(customrecord_fuel_delivery_anchorSearch)
				{
					var recObj = nlapiDeleteRecord('customrecord_fuel_delivery_anchor',customrecord_fuel_delivery_anchorSearch[0].getId());
				}
				nlapiLogExecution('debug', 'recObj ', recObj);
			}
			status = true;
        }
		return{
			"status":status
		}
	} 
	catch (e) 
	{
        nlapiLogExecution('Debug','Error ',e);
		return{
            "status": false
        }
    }
}

function getEmployeeIds(resources) {
    var resultArray = new Array();
    if (_logValidation(resources)) {
        nlapiLogExecution('Debug', 'Employee in function', resources);
        var temp = resources.split(',');
        for (var i = 0; i < temp.length; i++) {
            resultArray.push(temp[i]);
        }
    }
    return resultArray;
}
function _logValidation(value) 
{
	if(value != null && value!= 'null' && value != '' && value != undefined && value != undefined && value != 'undefined' && value.toString() != 'NaN' && value != NaN) 
	{
		return true;
	}
	else 
	{ 
		return false;
	}
}
function getUserUsingEmailId(emailId) 
{
	try 
	{
		var employeeSearch = nlapiSearchRecord('employee', null, [
		        new nlobjSearchFilter('email', null, 'is', emailId),
		        new nlobjSearchFilter('custentity_employee_inactive', null, 'is', 'F') ]);

		if (employeeSearch) 
		{
			return employeeSearch[0].getId();
		} 
		else 
		{
			throw "User does not exist";
		}
	} 
	catch (err) 
	{
		nlapiLogExecution('ERROR', 'getUserUsingEmailId', err);
		throw err;
	}
}