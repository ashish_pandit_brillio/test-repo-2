function scheduled()
{
//	try
	{
		nlapiLogExecution('Audit','Delete the timesheet')
var timesheetSearch = nlapiSearchRecord("timesheet",null,
[
   ["timesheetdate","onorafter","05/11/2020"], 
   "AND", 
   ["timeentry.customer","anyof","23694"], 
   "AND", 
   ["timeentry.billingstatus","is","T"]
], 
[
   new nlobjSearchColumn("id"), 
   new nlobjSearchColumn("employee"), 
   new nlobjSearchColumn("startdate"), 
   new nlobjSearchColumn("totalhours"), 
   new nlobjSearchColumn("approvalstatus"), 
   new nlobjSearchColumn("billingstatus","timeEntry",null)
]
);
			
		//	var msg = 'Hours wrongly entered because of glitch from OTG end'
			
		if(timesheetSearch)	
		{
			for(var i = 0; i < timesheetSearch.length; i++)
			{
				var id = timesheetSearch[i].getValue("id");
				nlapiLogExecution('Audit','id:',id);
				try{
						nlapiDeleteRecord('timesheet',id);
					}
					catch(e)
					{
						nlapiLogExecution('Audit','Catch:',e);
						continue;
					}	
			}
		}
	}
}