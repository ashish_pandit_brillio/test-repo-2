// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
/*
   	Script Name:CLI Vendor Statement  
	Author:     Sachin K
	Company:    Aashna cloudtech Pvt Ltd.
	Date: 
	Version:
	Description:
    

	Script Modification Log:

	-- Date --			-- Modified By --				--Requested By--				-- Description --
   22 sep 2014         Supriya                         Kalpana                       Normal Account Validation     



	Below is a summary of the process controls enforced by this script file.  The control logic is described
	more fully, below, in the appropriate function headers and code blocks.

     PAGE INIT
		- pageInit(type)
          NOT USED

     SAVE RECORD
		- saveRecord()
         NOT USED

     VALIDATE FIELD
		- validateField(type, name, linenum)
        NOT USED

     FIELD CHANGED
		- fieldChanged(type, name, linenum)
        NOT USED
 
     POST SOURCING
		- postSourcing(type, name)
        NOT USED

	LINE INIT
		- lineInit(type)
        NOT USED

     VALIDATE LINE
		- validateLine()
        NOT USED

     RECALC
		- reCalc()
        NOT USED

     SUB-FUNCTIONS
		- The following sub-functions are called by the above core functions in order to maintain code
            modularization:
            clientScriptPDF()
            clientScriptXL()




*/
}
// END SCRIPT DESCRIPTION BLOCK  ====================================



// BEGIN SCRIPT UPDATION BLOCK  ====================================
/*


*/
// END SCRIPT UPDATION BLOCK  ====================================




// BEGIN GLOBAL VARIABLE BLOCK  =====================================
{

	//  Initialize any Global Variables, in particular, debugging variables...


}
// END GLOBAL VARIABLE BLOCK  =======================================





// BEGIN PAGE INIT ==================================================

function pageInit(type)
{
    /*  On page init:

	- PURPOSE

		FIELDS USED:

		--Field Name--				--ID--			--Line Item Name--

    */

    //  LOCAL VARIABLES


    //  PAGE INIT CODE BODY
   
  
  
}

// END PAGE INIT ====================================================





// BEGIN SAVE RECORD ================================================

function saveRecord()
{
    /*  On save record:

	- PURPOSE



	  FIELDS USED:

		--Field Name--			--ID--		--Line Item Name--


    */


    //  LOCAL VARIABLES



    //  SAVE RECORD CODE BODY
    
   
   
	return true;

}

// END SAVE RECORD ==================================================





// BEGIN VALIDATE FIELD =============================================

function validateField(type, name, linenum)
{

	/*  On validate field:

          - EXPLAIN THE PURPOSE OF THIS FUNCTION


          FIELDS USED:

          --Field Name--				--ID--


    */


	//  LOCAL VARIABLES



	//  VALIDATE FIELD CODE BODY


	return true;

}

// END VALIDATE FIELD ===============================================





// BEGIN FIELD CHANGED ==============================================

function fieldChanged(type, name, linenum)
{
    /*  On field changed:

          - PURPOSE


          FIELDS USED:

			--Field Name--				--ID--


    */


    //  LOCAL VARIABLES


    //  FIELD CHANGED CODE BODY

	//alert('Field Name='+name+'\nLine Item Type='+type+'\nField Value='+nlapiGetFieldValue(name)+'\nLine Item Value='+nlapiGetLineItemValue(type, name, 1));
	
	
	
	
	
}

// END FIELD CHANGED ================================================





// BEGIN POST SOURCING ==============================================

function postSourcing(type, name)
{

    /*  On post sourcing:

	- PURPOSE

          FIELDS USED:

		--Field Name--			--ID--		--Line Item Name--


    */

    //  LOCAL VARIABLES


    //  POST SOURCING CODE BODY



}

// END POST SOURCING ================================================



// BEGIN LINE INIT ==============================================

function lineInit(type)
{

    /*  On Line Init:

	- PURPOSE

          FIELDS USED:

		--Field Name--			--ID--		--Line Item Name--


    */

    //  LOCAL VARIABLES


    //  LINE INIT CODE BODY



}

// END LINE INIT ================================================


// BEGIN VALIDATE LINE ==============================================

function validateLine(type)
{

	/*  On validate line:

          - EXPLAIN THE PURPOSE OF THIS FUNCTION


          FIELDS USED:

          --Field Name--				--ID--


    */


	//  LOCAL VARIABLES


	//  VALIDATE LINE CODE BODY


	return true;

}

// END VALIDATE LINE ================================================





// BEGIN RECALC =====================================================

function recalc(type)
{

	/*  On recalc:

          - EXPLAIN THE PURPOSE OF THIS FUNCTION


          FIELDS USED:

          --Field Name--				--ID--


    */


	//  LOCAL VARIABLES


	//  RECALC CODE BODY


}

// END RECALC =======================================================





// BEGIN FUNCTION ===================================================



// END FUNCTION =====================================================

function clientScriptPDF()
{
   var vendorname = nlapiGetFieldValue('vendorname');
   var locationname = nlapiGetFieldValue('locationname');
   var startdate = nlapiGetFieldValue('startdate');
   var enddate = nlapiGetFieldValue('enddate');
   var url = nlapiResolveURL('SUITELET', 'customscript_sut_vendor_statement_pdf_ne', '1');
   url = url+'&vendorname='+vendorname+'&locationname='+locationname+'&startdate='+startdate+'&enddate='+enddate;
   var newwindow = window.open(url, 'print')
} // END clientScriptPDF()

function clientScriptXL()
{
   //alert('Email');
   var vendorname = nlapiGetFieldValue('vendorname');
   var locationname = nlapiGetFieldValue('locationname');
   var startdate = nlapiGetFieldValue('startdate');
   var enddate = nlapiGetFieldValue('enddate');
   var url = nlapiResolveURL('SUITELET', 'customscript_sut_vendor_statement_xml_ne', '1');
   url = url+'&vendorname='+vendorname+'&locationname='+locationname+'&startdate='+startdate+'&enddate='+enddate;
   //alert('url '+url);
   var newwindow = window.open(url, 'print', 'height=200,width=400');
} // END clientScriptXL()