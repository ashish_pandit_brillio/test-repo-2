/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       08 Aug 2016     shruthi.l
 *
 */

/**
 * @param {Object} dataIn Parameter object
 * @returns {Object} Output object
 */
function postRESTlet(dataIn) {
	var response = new Response();
	try {
		nlapiLogExecution('debug', 'dataIn', JSON.stringify(dataIn));
		nlapiLogExecution('Debug', 'dataIn', dataIn.EmailId);
		var employeeId = getUserUsingEmailId(dataIn.EmailId);
		var requestType = dataIn.RequestType;
		
		switch (requestType) {

		case M_Constants.Request.Get:

			if (employeeId) {
				response.Data = getEmployeeDetail(employeeId);
				response.Status = true;
			} 
		break;

	}
		
		
	} catch (err) {
		nlapiLogExecution('ERROR', 'postRESTlet', err);
		response.Data = err;
	}
	return response;
}

function getEmployeeDetail(employeeId) {
	try {
		//var employeeRec = nlapiLoadRecord('employee', employeeId);
		var employeeDetails = {};
		
	/*	
		var filters = new Array();
		filters[filters.length] = new nlobjSearchFilter('internalid', 'employee', 'anyof', employeeId);
		var current_date = nlapiDateToString(new Date());
		nlapiLogExecution('DEBUG', 'getEmployeeDetail', 'current_date...' + current_date)
		filters[filters.length] = new nlobjSearchFilter('enddate', null, 'before', current_date);
		//filters[filters.length] = new nlobjSearchFilter('startdate', null, 'onorbefore', current_date);

		var returncols = new Array();
		returncols[0] = new nlobjSearchColumn('company');
		returncols[1] = new nlobjSearchColumn('startdate');
		returncols[2] = new nlobjSearchColumn('enddate').setSort(true);
		//returncols[3] = new nlobjSearchColumn('salesrep');
		//returncols[4] = new nlobjSearchColumn('amount');
		
		//nlapiLogExecution('DEBUG', 'getEmployeeDetail', '2 Before Search...');
		var employeeAllocatedProjects = nlapiSearchRecord('resourceallocation', null, filters, returncols);
		nlapiLogExecution('DEBUG', 'getEmployeeDetail', employeeAllocatedProjects);
		employeeDetails.project = employeeAllocatedProjects[0];*/
		
	
		var empRecDetails = nlapiLoadRecord('employee', employeeId);
		var dep= empRecDetails.getFieldText('department');
		var loc = empRecDetails.getFieldText('location');
		nlapiLogExecution('DEBUG', 'getEmployeeDetail', empRecDetails.getFieldText('department'));
		
		
		employeeDetails = {"department":dep,"location": loc};
		          			          
		
		return employeeDetails;
		// check access
/*		if (expenseReportRec.getFieldValue('custbody1stlevelapprover') == firstLevelApprover) {
			var expenseDetails = {};

			expenseDetails.Employee = expenseReportRec.getFieldText('entity');
			expenseDetails.FirstLevelApprover = expenseReportRec
			        .getFieldText('custbody1stlevelapprover');
			expenseDetails.SecondLevelApprover = expenseReportRec
			        .getFieldText('custbody_expenseapprover');

			expenseDetails.Expense = {
			    Reference : expenseReportRec.getFieldValue('tranid'),
			    Purpose : isEmpty(expenseReportRec.getFieldValue('memo')) ? ''
			            : expenseReportRec.getFieldValue('memo'),
			    Status : expenseReportRec.getFieldValue('status'),
			    InternalId : expenseReportRec.getId(),
			    AdvanceToApply : expenseReportRec.getFieldValue('advance'),
			    VAT : expenseReportRec.getFieldValue('tax1amt'),
			    PostingDate : expenseReportRec.getFieldValue('trandate'),
			    TransactionDate : expenseReportRec
			            .getFieldValue('custbody_transactiondate'),
			    AccountName : expenseReportRec.getFieldText('account'),
			    Account : expenseReportRec.getFieldValue('account'),
			    // Reason : '',
			    Currency : expenseReportRec.getFieldText('currency'),
			    DocumentReceived : expenseReportRec
			            .getFieldValue('custbody_exp_doc_received'),
			    DocumentReceivedDate : '',
			    ItemList : []
			// ApproverRemark : expenseReportRec.getFieldValue(''),
			};

			expenseDetails.Expense.Summary = {
			    Expenses : expenseReportRec.getFieldValue('total'),
			    NonReimbursableExpenses : expenseReportRec
			            .getFieldValue('nonreimbursable'),
			    ReimbursableExpenses : expenseReportRec
			            .getFieldValue('reimbursable'),
			    AdvanceToApply : expenseReportRec.getFieldValue('advance'),
			    TotalReimbursableAmount : expenseReportRec
			            .getFieldValue('amount')
			};

			// get all line items
			var lineItemCount = expenseReportRec.getLineItemCount('expense');
			var expenseList = [];
			var isReimbursable = null;
			var projectId = null;
			var projectIdList = [];

			for (var line = 1; line <= lineItemCount; line++) {
				isReimbursable = expenseReportRec.getLineItemValue('expense',
				        'isnonreimbursable', line);
				projectId = expenseReportRec.getLineItemValue('expense',
				        'customer', line);
				projectIdList.push(projectId);

				// if (isFalse(isReimbursable)) {

				if (!expenseDetails.ProjectText) {
					expenseDetails.ProjectText = expenseReportRec
					        .getLineItemValue('expense', 'custcolprj_name',
					                line);
				}
				expenseList.push({
				    ProjectText : expenseReportRec.getLineItemValue('expense',
				            'custcolprj_name', line),
				    ProjectId : projectId,
				    Vertical : expenseReportRec.getLineItemValue('expense',
				            'class_display', line),
				    CategoryText : expenseReportRec.getLineItemValue('expense',
				            'category_display', line),
				    Amount : expenseReportRec.getLineItemValue('expense',
				            'foreignamount', line),
				    ForeignAmount : expenseReportRec.getLineItemValue(
				            'expense', 'amount', line),
				    Currency : expenseReportRec.getLineItemText('expense',
				            'currency', line),
				    IsReimbursable : isReimbursable,
				    DeliveryManager : null
				});
				// }
			}

			expenseDetails.Expense.ItemList = expenseList;
			return expenseDetails;
		} else {
			throw "You are not authorised to access this record";
		}*/
	} catch (err) {
		nlapiLogExecution('error', 'getExpenseDetail', err);
		throw err;
	}
}
