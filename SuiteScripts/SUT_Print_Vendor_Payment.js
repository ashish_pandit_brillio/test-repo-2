// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
/*
   	Script Name: SUT_Print_Vendor_Payment.js
	Author: Jayesh V Dinde
	Company: Aashna Cloudtech PVT LTD
	Date: 9 May 2016
	Description: This script is used to design pdf layout for vendor payment record.


	Script Modification Log:

	-- Date --			-- Modified By --				--Requested By--				-- Description --



Below is a summary of the process controls enforced by this script file.  The control logic is described
more fully, below, in the appropriate function headers and code blocks.


     SUITELET
		- Print_Vendor_Payment(request, response)


     SUB-FUNCTIONS
		- The following sub-functions are called by the above core functions in order to maintain code
            modularization:

               - NOT USED

*/
}
// END SCRIPT DESCRIPTION BLOCK  ====================================

// BEGIN SUITELET ==================================================
function Print_Vendor_Payment()
{
	try
	{
		var vendor_bill_rcrd_id = request.getParameter('rcrd_id');
		vendor_bill_rcrd_id = vendor_bill_rcrd_id.trim();
		var recrd_type = request.getParameter('recrd_type');
		recrd_type = recrd_type.trim();
		recrd_type = recrd_type.toString();
		if (!_logValidation(vendor_bill_rcrd_id))
		{
			return;
		}
		
		var vendor_bill_rcrd_obj = nlapiLoadRecord(recrd_type,vendor_bill_rcrd_id);
		
		var paymnt_no = vendor_bill_rcrd_obj.getFieldValue('transactionnumber');
		if(!_logValidation(paymnt_no))
		{
			paymnt_no = '';
		}
		var cheque_no = vendor_bill_rcrd_obj.getFieldValue('tranid');
		if(!_logValidation(cheque_no))
		{
			cheque_no = '';
		}
		var date = vendor_bill_rcrd_obj.getFieldValue('trandate');
		if(!_logValidation(date))
		{
			date = '';
		}
		
		var vendor_bill_amount = vendor_bill_rcrd_obj.getFieldValue('usertotal');
		var vendor_bill_currency = vendor_bill_rcrd_obj.getFieldText('currency');
		
		var comp_imgrsrc = 'https://system.sandbox.netsuite.com/core/media/media.nl?id=6237&c=3883006&h=f9681f3581e51644773b';
		comp_imgrsrc = nlapiEscapeXML(comp_imgrsrc);
		
		var i_companyobj = nlapiLoadConfiguration('companyinformation');
		
		var i_comp_address = i_companyobj.getFieldValue('mainaddress_text');
		i_comp_address = nlapiEscapeXML(i_comp_address);
		var i_comp_name = i_companyobj.getFieldValue('companyname');
		var i_comp_email = i_companyobj.getFieldValue('email');
		
		var strVar = "";
        strVar += "";
        strVar += "<table width=\"150%\">";
		
	        strVar += "	<tr>";
	        strVar += " <td width=\"75%\"><img width=\"110\" height=\"50\" src=\"" + comp_imgrsrc + "\"><\/img><\/td>";
			//strVar += " <td width=\"75%\" font-size=\"10\">Bill No : " + paymnt_no + "<br/>Date : " + date + "<br/>Cheque No : " + cheque_no + "<\/td>";
			strVar += " <td width=\"75%\" font-size=\"10\">Bill No : " + paymnt_no + "<br/>Date : " + date + "<\/td>";
			strVar += "	<\/tr>";
		
			strVar += "	<tr height=\"50px\">";
			strVar += "	<\/tr>";
			
			strVar += "	<tr>";
			strVar += " <td width=\"75%\" font-size=\"10\">" +i_comp_address + "<br/>Phone No. : 201-305-0921 <br/>Email : " + i_comp_email + "<\/td>";
			strVar += " <td width=\"75%\" font-size=\"10\"><\/td>";
			strVar += "	<\/tr>";
			
			strVar += "	<tr height=\"30px\">";
			strVar += "	<\/tr>";
			
			strVar += "	<tr>";
			strVar += " <td width=\"75%\" font-size=\"10\"><b>Payment Amount Specification :</b><\/td>";
			strVar += " <td width=\"75%\" font-size=\"10\"><\/td>";
			strVar += "	<\/tr>";
			
		strVar += "<\/table>";
		
		strVar += "<table width=\"100%\">";
			
			var Vendor_Name = "Vendor<br/>Name";
			var Begin_Date = "Begin<br/>Date";
			var End_Date = "End<br/>Date";
			var ST_Hours = "ST<br/>Hours";
			var ST_Rate = "ST<br/>Rate";
			var OT_Hours = "OT<br/>Hours";
			var OT_Rate = "OT<br/>Rate";
			var Payment_Term = "Payment<br/>Term";
			strVar += "<tr background-color='#D0D0D0'>";
			strVar += "<td width='15%' align=\"center\" font-size=\"10\"><b>" + Vendor_Name + "</b><\/td>";
			strVar += "<td width='15%' align=\"center\" font-size=\"10\"><b>Consultant</b><\/td>";
			strVar += "<td width='10%' font-size=\"10\"><b>" + Begin_Date + "</b><\/td>";
			strVar += "<td width='10%' font-size=\"10\"><b>" + End_Date + "</b><\/td>";
			strVar += "<td width='5%' font-size=\"10\"><b>" + ST_Hours + "</b><\/td>";
			strVar += "<td width='5%' font-size=\"10\"><b>" + ST_Rate + "</b><\/td>";
			strVar += "<td width='5%' font-size=\"10\"><b>" + OT_Hours + "</b><\/td>";
			strVar += "<td width='5%' font-size=\"10\"><b>" + OT_Rate + "</b><\/td>";
			strVar += "<td width='10%' font-size=\"10\"><b>Amount</b><\/td>";
			strVar += "<td width='5%' font-size=\"10\"><b>Invoice#</b><\/td>";
			strVar += "<td width='20%' align=\"center\" font-size=\"10\"><b>Client</b><\/td>";
			strVar += "<td width='15%' font-size=\"10\"><b>" + Payment_Term + "</b><\/td>";
			strVar += "</tr>";
			
			var ST_bill_item_quantity = new Array();
			var ST_bill_item_rate = new Array();
			var OT_bill_item_quantity = new Array();
			var OT_bill_item_rate = new Array();
			
			var linked_invoice_customer = '';
			var linked_invoice_terms = '';
			var apply_bill_consultant = '';
			var apply_bill_vendor_name = '';
			var st_ot_flag = 0;
			var apply_bill_from_date = vendor_bill_rcrd_obj.getFieldValue('custbody_billfrom');
			if (!_logValidation(apply_bill_from_date))
				apply_bill_from_date = '';
				
			var apply_bill_to_date = vendor_bill_rcrd_obj.getFieldValue('custbody_billto');
			if (!_logValidation(apply_bill_to_date))
				apply_bill_to_date = '';
				
			var apply_bill_consultant_id = vendor_bill_rcrd_obj.getFieldValue('custbody_consultantname');
			if (_logValidation(apply_bill_consultant_id))
			{
				apply_bill_consultant = nlapiLookupField('employee', apply_bill_consultant_id, 'firstname');
				apply_bill_consultant += ' ';
				apply_bill_consultant += nlapiLookupField('employee', apply_bill_consultant_id, 'middlename');
				apply_bill_consultant += ' ';
				apply_bill_consultant += nlapiLookupField('employee', apply_bill_consultant_id, 'lastname');
			}
			var apply_bill_vendor_id = vendor_bill_rcrd_obj.getFieldValue('entity');
			if (_logValidation(apply_bill_vendor_id))
			{
				apply_bill_vendor_name = nlapiLookupField('vendor', apply_bill_vendor_id, 'altname');
				apply_bill_vendor_name = nlapiEscapeXML(apply_bill_vendor_name);
				apply_bill_vendor_name = replaceSpace(apply_bill_vendor_name);
			}
			
			var appply_bill_linked_invoice_no = vendor_bill_rcrd_obj.getFieldValue('custbody_oldinvoicenumber');
			
			if (_logValidation(appply_bill_linked_invoice_no))
			{
				var filters_search_invoice_linked = new Array();
				filters_search_invoice_linked[0] = new nlobjSearchFilter('recordtype', null, 'is', 'invoice');
				filters_search_invoice_linked[1] = new nlobjSearchFilter('tranid', null, 'is', appply_bill_linked_invoice_no);
				var column_search_invoice_linked = new Array();
				column_search_invoice_linked[0] = new nlobjSearchColumn('entity');
				column_search_invoice_linked[1] = new nlobjSearchColumn('terms');
				
				var search_linked_invoice = nlapiSearchRecord('transaction', null, filters_search_invoice_linked, column_search_invoice_linked);
				if (_logValidation(search_linked_invoice))
				{
					var linked_invoice_customer_id = search_linked_invoice[0].getValue('entity');
					linked_invoice_customer = nlapiLookupField('customer',linked_invoice_customer_id,'altname');
					linked_invoice_customer = nlapiEscapeXML(linked_invoice_customer);
					linked_invoice_customer = replaceSpace(linked_invoice_customer);
					linked_invoice_terms = search_linked_invoice[0].getText('terms');
				}
			}
			else
				appply_bill_linked_invoice_no = '';
							
			var line_item_count = vendor_bill_rcrd_obj.getLineItemCount('item');
			for(var k=1; k<=line_item_count ; k++)
			{
				var apply_bill_item = vendor_bill_rcrd_obj.getLineItemValue('item','item',k);
				var apply_bill_item_quantity = vendor_bill_rcrd_obj.getLineItemValue('item','quantity',k);
				var appply_bill_item_rate = vendor_bill_rcrd_obj.getLineItemValue('item','rate',k);
				
				if(apply_bill_item == 2443)
				{
					st_ot_flag = 1;
					ST_bill_item_quantity.push(apply_bill_item_quantity);
					ST_bill_item_rate.push(appply_bill_item_rate);
				}
				else if(apply_bill_item == 2444)
				{
					st_ot_flag = 1;
					OT_bill_item_quantity.push(apply_bill_item_quantity);
					OT_bill_item_rate.push(appply_bill_item_rate);
				}
				else
				{
					strVar += "<tr>";
					strVar += "<td width='15%' font-size=\"9\">" + apply_bill_vendor_name + "<\/td>";
					strVar += "<td width='15%' font-size=\"9\">" + nlapiEscapeXML(apply_bill_consultant) + "<\/td>";
					strVar += "<td width='10%' font-size=\"9\">" + apply_bill_from_date + "<\/td>";
					strVar += "<td width='10%' font-size=\"9\">" + apply_bill_to_date + "<\/td>";
					strVar += "<td width='5%' font-size=\"9\">" + apply_bill_item_quantity + "<\/td>";
					strVar += "<td width='5%' font-size=\"9\">" + appply_bill_item_rate + "<\/td>";
					strVar += "<td width='5%' font-size=\"9\"><\/td>";
					strVar += "<td width='5%' font-size=\"9\"><\/td>";
					strVar += "<td width='10%' font-size=\"9\">" + vendor_bill_amount + "<\/td>";
					strVar += "<td width='5%' font-size=\"9\">" + nlapiEscapeXML(appply_bill_linked_invoice_no) + "<\/td>";
					strVar += "<td width='20%' font-size=\"9\" align=\"left\">" + linked_invoice_customer + "<\/td>";
					strVar += "<td width='15%' font-size=\"9\">" + nlapiEscapeXML(linked_invoice_terms) + "<\/td>";
					strVar += "</tr>";
				}
			}
			
			if(st_ot_flag == 1)
			{
				if(ST_bill_item_quantity.length == OT_bill_item_quantity.length)
				{
					for(var t=0; t<ST_bill_item_quantity.length; t++)
					{
						strVar += "<tr>";
						strVar += "<td width='15%' font-size=\"9\">" + apply_bill_vendor_name + "<\/td>";
						strVar += "<td width='15%' font-size=\"9\">" + nlapiEscapeXML(apply_bill_consultant) + "<\/td>";
						strVar += "<td width='10%' font-size=\"9\">" + apply_bill_from_date + "<\/td>";
						strVar += "<td width='10%' font-size=\"9\">" + apply_bill_to_date + "<\/td>";
						strVar += "<td width='5%' font-size=\"9\">" + ST_bill_item_quantity[t] + "<\/td>";
						strVar += "<td width='5%' font-size=\"9\">" + ST_bill_item_rate[t] + "<\/td>";
						strVar += "<td width='5%' font-size=\"9\">" + OT_bill_item_quantity[t] + "<\/td>";
						strVar += "<td width='5%' font-size=\"9\">" + OT_bill_item_rate[t] + "<\/td>";
						strVar += "<td width='10%' font-size=\"9\">" + vendor_bill_amount + "<\/td>";
						strVar += "<td width='5%' font-size=\"9\">" + nlapiEscapeXML(appply_bill_linked_invoice_no) + "<\/td>";
						strVar += "<td width='20%' font-size=\"9\" align=\"left\">" + linked_invoice_customer + "<\/td>";
						strVar += "<td width='15%' font-size=\"9\">" + nlapiEscapeXML(linked_invoice_terms) + "<\/td>";
						strVar += "</tr>";
					}
				}
				else if(ST_bill_item_quantity.length < OT_bill_item_quantity.length)
				{
					if(ST_bill_item_quantity.length == 0)
					{
						for(var t=0; t<OT_bill_item_quantity.length; t++)
						{
							strVar += "<tr>";
							strVar += "<td width='15%' font-size=\"9\">" + apply_bill_vendor_name + "<\/td>";
							strVar += "<td width='15%' font-size=\"9\">" + nlapiEscapeXML(apply_bill_consultant) + "<\/td>";
							strVar += "<td width='10%' font-size=\"9\">" + apply_bill_from_date + "<\/td>";
							strVar += "<td width='10%' font-size=\"9\">" + apply_bill_to_date + "<\/td>";
							strVar += "<td width='5%' font-size=\"9\"><\/td>";
							strVar += "<td width='5%' font-size=\"9\"><\/td>";
							strVar += "<td width='5%' font-size=\"9\">" + OT_bill_item_quantity[t] + "<\/td>";
							strVar += "<td width='5%' font-size=\"9\">" + OT_bill_item_rate[t] + "<\/td>";
							strVar += "<td width='10%' font-size=\"9\">" + vendor_bill_amount + "<\/td>";
							strVar += "<td width='5%' font-size=\"9\">" + nlapiEscapeXML(appply_bill_linked_invoice_no) + "<\/td>";
							strVar += "<td width='20%' font-size=\"9\" align=\"left\">" + linked_invoice_customer + "<\/td>";
							strVar += "<td width='15%' font-size=\"9\">" + nlapiEscapeXML(linked_invoice_terms) + "<\/td>";
							strVar += "</tr>";
						}
					}
					else
					{
						for(var t=0; t<OT_bill_item_quantity.length; t++)
						{
							strVar += "<tr>";
							strVar += "<td width='15%' font-size=\"9\">" + apply_bill_vendor_name + "<\/td>";
							strVar += "<td width='15%' font-size=\"9\">" + nlapiEscapeXML(apply_bill_consultant) + "<\/td>";
							strVar += "<td width='10%' font-size=\"9\">" + apply_bill_from_date + "<\/td>";
							strVar += "<td width='10%' font-size=\"9\">" + apply_bill_to_date + "<\/td>";
							strVar += "<td width='5%' font-size=\"9\">" + ST_bill_item_quantity[0] + "<\/td>";
							strVar += "<td width='5%' font-size=\"9\">" + ST_bill_item_rate[0] + "<\/td>";
							strVar += "<td width='5%' font-size=\"9\">" + OT_bill_item_quantity[t] + "<\/td>";
							strVar += "<td width='5%' font-size=\"9\">" + OT_bill_item_rate[t] + "<\/td>";
							strVar += "<td width='10%' font-size=\"9\">" + vendor_bill_amount + "<\/td>";
							strVar += "<td width='5%' font-size=\"9\">" + nlapiEscapeXML(appply_bill_linked_invoice_no) + "<\/td>";
							strVar += "<td width='20%' font-size=\"9\" align=\"left\">" + linked_invoice_customer + "<\/td>";
							strVar += "<td width='15%' font-size=\"9\">" + nlapiEscapeXML(linked_invoice_terms) + "<\/td>";
							strVar += "</tr>";
						}
					}
				}
				else if(ST_bill_item_quantity.length > OT_bill_item_quantity.length)
				{
					if(OT_bill_item_quantity.length == 0)
					{
						for(var t=0; t<ST_bill_item_quantity.length; t++)
						{
							strVar += "<tr>";
							strVar += "<td width='15%' font-size=\"9\">" + apply_bill_vendor_name + "<\/td>";
							strVar += "<td width='15%' font-size=\"9\">" + nlapiEscapeXML(apply_bill_consultant) + "<\/td>";
							strVar += "<td width='10%' font-size=\"9\">" + apply_bill_from_date + "<\/td>";
							strVar += "<td width='10%' font-size=\"9\">" + apply_bill_to_date + "<\/td>";
							strVar += "<td width='5%' font-size=\"9\">" + ST_bill_item_quantity[t] + "<\/td>";
							strVar += "<td width='5%' font-size=\"9\">" + ST_bill_item_rate[t] + "<\/td>";
							strVar += "<td width='5%' font-size=\"9\"><\/td>";
							strVar += "<td width='5%' font-size=\"9\"><\/td>";
							strVar += "<td width='10%' font-size=\"9\">" + vendor_bill_amount + "<\/td>";
							strVar += "<td width='5%' font-size=\"9\">" + nlapiEscapeXML(appply_bill_linked_invoice_no) + "<\/td>";
							strVar += "<td width='20%' font-size=\"9\" align=\"left\">" + linked_invoice_customer + "<\/td>";
							strVar += "<td width='15%' font-size=\"9\">" + nlapiEscapeXML(linked_invoice_terms) + "<\/td>";
							strVar += "</tr>";
						}
					}
					else
					{
						for(var t=0; t<ST_bill_item_quantity.length; t++)
						{
							strVar += "<tr>";
							strVar += "<td width='15%' font-size=\"9\">" + apply_bill_vendor_name + "<\/td>";
							strVar += "<td width='15%' font-size=\"9\">" + nlapiEscapeXML(apply_bill_consultant) + "<\/td>";
							strVar += "<td width='10%' font-size=\"9\">" + apply_bill_from_date + "<\/td>";
							strVar += "<td width='10%' font-size=\"9\">" + apply_bill_to_date + "<\/td>";
							strVar += "<td width='5%' font-size=\"9\">" + ST_bill_item_quantity[t] + "<\/td>";
							strVar += "<td width='5%' font-size=\"9\">" + ST_bill_item_rate[t] + "<\/td>";
							strVar += "<td width='5%' font-size=\"9\">" + OT_bill_item_quantity[0] + "<\/td>";
							strVar += "<td width='5%' font-size=\"9\">" + OT_bill_item_rate[0] + "<\/td>";
							strVar += "<td width='10%' font-size=\"9\">" + vendor_bill_amount + "<\/td>";
							strVar += "<td width='5%' font-size=\"9\">" + nlapiEscapeXML(appply_bill_linked_invoice_no) + "<\/td>";
							strVar += "<td width='20%' font-size=\"9\" align=\"left\">" + linked_invoice_customer + "<\/td>";
							strVar += "<td width='15%' font-size=\"9\">" + nlapiEscapeXML(linked_invoice_terms) + "<\/td>";
							strVar += "</tr>";
						}
					}
				}
				else
				{
					
				}
			}
					
			strVar += "	<tr height=\"20px\">";
			strVar += "	<\/tr>";
			
			var Payment_Amount = "Payment<br/>Amount";
			strVar += "<tr background-color='#D0D0D0'>";
			strVar += "<td width='10%' font-size=\"9\"><\/td>";
			strVar += "<td width='10%' font-size=\"9\"><\/td>";
			strVar += "<td width='10%' font-size=\"9\"><\/td>";
			strVar += "<td width='10%' font-size=\"9\"><\/td>";
			strVar += "<td width='10%' font-size=\"9\"><\/td>";
			strVar += "<td width='10%' font-size=\"9\"><\/td>";
			strVar += "<td width='10%' font-size=\"9\"><\/td>";
			strVar += "<td width='10%' font-size=\"9\"><\/td>";
			strVar += "<td width='10%' font-size=\"9\"><\/td>";
			strVar += "<td width='10%' font-size=\"10\">" + Payment_Amount + "<\/td>";
			strVar += "<td width='10%' font-size=\"9\"><\/td>";
			strVar += "<td width='15%' font-size=\"10\">" + vendor_bill_amount + "<\/td>";
			/*strVar += "<td width='35%' font-size=\"10\">Payment Amount<\/td>";
			strVar += "<td width='15%' font-size=\"10\">" + vendor_paymnt_currency + "<\/td>";
			strVar += "<td width='25%' font-size=\"10\">" + vendor_payment_amount + "<\/td>";*/
			strVar += "</tr>";			
			
		strVar += "<\/table>";
		
		var xml = "<?xml version=\"1.0\"?>\n<!DOCTYPE pdf PUBLIC \"-//big.faceless.org//report\" \"report-1.1.dtd\">\n";
        xml += "<pdf>\n<body size=\"letter-landscape\">";
        xml += strVar;
        xml += "</body>\n</pdf>";
		
		var file = nlapiXMLToPDF(xml);
		var file_name = 'Vendor Payment_' + cheque_no + '.pdf';
		file.setName(''+file_name)
		response.setContentType('PDF', ''+file_name, 'inline');
		response.write(file.getValue());
	}
	catch(err)
	{
		nlapiLogExecution('ERROR','ERROR MESSAGE:-- ',err);
	}
}
// END SUITELET ==================================================

function _logValidation(value) {
    if (value != null && value != '' && value != undefined && value != 'NaN') {
        return true;
    }
    else {
        return false;
    }
}

function replaceSpace(value){
    // value = value.replace(/" "/g, "&nbsp;");
    value = value.split(' ');
    value = ((value.toString()).replace(/,/g, "&nbsp;"));
    return value;
}