/**
 * @author Shweta
 */
// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
/*
   	Script Name: UES_VendorBill_Email_Automation.js
	Author     : Shweta Chopde
	Date       : 6 May 2014
	Description: Trigger an email on create on Vendor Bill


	Script Modification Log:

	-- Date --			-- Modified By --				--Requested By--				-- Description --



Below is a summary of the process controls enforced by this script file.  The control logic is described
more fully, below, in the appropriate function headers and code blocks.


     BEFORE LOAD
		- beforeLoadRecord(type)



     BEFORE SUBMIT
		- beforeSubmitRecord(type)


     AFTER SUBMIT
		- afterSubmitRecord(type)



     SUB-FUNCTIONS
		- The following sub-functions are called by the above core functions in order to maintain code
            modularization:

               - NOT USED

*/
}
// END SCRIPT DESCRIPTION BLOCK  ====================================



// BEGIN GLOBAL VARIABLE BLOCK  =====================================
{
	//  Initialize any Global Variables, in particular, debugging variables...




}
// END GLOBAL VARIABLE BLOCK  =======================================





// BEGIN BEFORE LOAD ==================================================

function beforeLoadRecord(type)
{

	/*  On before load:

          - EXPLAIN THE PURPOSE OF THIS FUNCTION

		-


		FIELDS USED:

          --Field Name--				--ID--


	*/



	//  LOCAL VARIABLES

    //  BEFORE LOAD CODE BODY


	return true;



}

// END BEFORE LOAD ====================================================





// BEGIN BEFORE SUBMIT ================================================

function beforeSubmitRecord(type)
{
    /*  On before submit:

          - PURPOSE
		-

          FIELDS USED:

          --Field Name--				--ID--


    */


    //  LOCAL VARIABLES


    //  BEFORE SUBMIT CODE BODY


	return true;

}

// END BEFORE SUBMIT ==================================================





// BEGIN AFTER SUBMIT =============================================

function afterSubmit_email_trigger(type)
{
		
  var i_vertical_head_email;
  var i_vertical_head_name;

 if(type == 'create')
  {  		
   try
   {
   	var i_recordID = nlapiGetRecordId();
	var s_record_type = nlapiGetRecordType();
	var i_user = nlapiGetUser();
	
	if(_logValidation(i_recordID) && _logValidation(s_record_type))
	{
	  var o_recordOBJ = nlapiLoadRecord(s_record_type,i_recordID);	
		
	  if(_logValidation(o_recordOBJ))
	  {
	  	var i_vertical_head = o_recordOBJ.getFieldValue('custbody_verticalhead')
		nlapiLogExecution('DEBUG', 'afterSubmit_email_trigger',' Vertical Head -->' + i_vertical_head);
			
		var a_split_array = new Array();
		var a_employee_array =  get_employee_details(i_vertical_head)
		
		if(_logValidation(a_employee_array))
		{
			a_split_array = a_employee_array[0].split('####')
			
			i_vertical_head_email = a_split_array[0]
			
			i_vertical_head_name = a_split_array[1]
			
		}//Employee Array
			
		nlapiLogExecution('DEBUG', 'afterSubmit_email_trigger',' Vertical Head Email -->' + i_vertical_head_email);
		
		var i_transaction_number = o_recordOBJ.getFieldValue('transactionnumber')
		nlapiLogExecution('DEBUG', 'afterSubmit_email_trigger',' Transaction Number -->' + i_transaction_number);
						
		var i_author = i_user
		
		var i_recipient = i_vertical_head_email
		
		var s_email_body = 'Hi,\n'
		s_email_body+=i_vertical_head_name+'\n\n'
		s_email_body+='Vendor Bill has been created , it is pending for your approval.\n\n'
		s_email_body+='Regards ,\n'
		s_email_body+='Brillio- NetSuite Team'
		   
		var s_email_subject = ' Vendor Bill - '+i_transaction_number+' Pending for your Approval '
		
		var records = new Object();
	    records['transaction'] = i_recordID	
		
		if(_logValidation(i_author)&&_logValidation(i_recipient))
		{
		  nlapiSendEmail(i_author, i_recipient, s_email_subject, s_email_body, null , null, records, null);
		  nlapiLogExecution('DEBUG', 'afterSubmit_email_trigger', ' Email Sent ......');	
						
		}//Author / Recipient		
		
	  }//Record OBJ
		
	}//Record ID & Record type
	   	
   }//TRY
   catch(exception)
   {
   	 nlapiLogExecution('DEBUG', 'ERROR',' Exception Caught -->' + exception);
   }//CATCH  	
	
  }//CREATE	

	return true;

}

// END AFTER SUBMIT ===============================================





// BEGIN FUNCTION ===================================================
function _logValidation(value) 
{
 if(value != null && value.toString() != null && value != '' && value != undefined && value.toString() != undefined && value != 'undefined' && value.toString() != 'undefined'&& value.toString() != 'NaN' && value != NaN) 
 {
  return true;
 }
 else 
 { 
  return false;
 }
}

function get_employee_details(i_employee)
{	
  var s_email;
  var s_name ;
  var a_return_array = new Array();
  
  if(_logValidation(i_employee))
  {
  	var o_employeeOBJ = nlapiLoadRecord('employee',i_employee)
	
	if(_logValidation(o_employeeOBJ))
	{
	  s_email = o_employeeOBJ.getFieldValue('email')  
	  nlapiLogExecution('DEBUG', 'get_employee_email',' Email -->' + s_email);
	  
	  var s_first_name = o_employeeOBJ.getFieldValue('firstname')  
	  nlapiLogExecution('DEBUG', 'get_employee_email',' First Name -->' + s_first_name);
	  
	  var s_middle_name = o_employeeOBJ.getFieldValue('middlename')  
	  nlapiLogExecution('DEBUG', 'get_employee_email',' Middle Name -->' + s_middle_name);
	  
	  var s_last_name = o_employeeOBJ.getFieldValue('lastname')  
	  nlapiLogExecution('DEBUG', 'get_employee_email',' Last Name -->' + s_last_name);
	  
	  if(!_logValidation(s_first_name))
	  {
	  	s_first_name = ''
	  }
	   if(!_logValidation(s_middle_name))
	  {
	  	s_middle_name = ''
	  }
	   if(!_logValidation(s_last_name))
	  {
	  	s_last_name = ''
	  }
	
	  s_name = 	s_first_name+' '+s_middle_name+' '+s_last_name
			
	}//Employee OBJ
		
  }//Employee
  
  a_return_array[0] = s_email+'####'+s_name
  
  return a_return_array ;
  	
}//Recipient Email



// END FUNCTION =====================================================
