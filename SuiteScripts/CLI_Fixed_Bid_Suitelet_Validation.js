/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       04 Sep 2014     Swati
 *
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType
 * 
 * @param {String} type Sublist internal id
 * @param {String} name Field internal id
 * @param {Number} linenum Optional line item number, starts from 1
 * @returns {Void}
 */
function clientFieldChanged(type, name, linenum)
{//fun start 
	//alert('name'+name);
	//alert('type'+type);
if(name == 'custfield_curr_date')
	{
	var d_date_value=nlapiGetFieldValue('custfield_curr_date');
	if(d_date_value == null || d_date_value == '')
		{
		
		}
	else
		{

			var date=new Date(d_date_value);
			
			var d_current_date=new Date();
			
			if(date <=  d_current_date)
				{
				
				}
			else
				{
				alert('Selected Date Can Not Be Greater Than Current Date');
				nlapiSetFieldValue('custfield_curr_date','',false);
				}
	
		}
	}
}//fun close
