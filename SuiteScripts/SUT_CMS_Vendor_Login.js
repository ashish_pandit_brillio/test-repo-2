/**
 * Vendor Login Screen
 * 
 * Version Date Author Remarks 1.00 15 Apr 2016 Nitish Mishra
 * 
 */

function suitelet(request, response) {
	try {

		if (request.getMethod() == "GET") {
			createLoginScreen();
		} else {
			submitLoginScreen(request);
		}
	} catch (err) {
		nlapiLogExecution('ERROR', 'suitelet', err);
		displayErrorForm(err, "Vendor Login");
	}
}

function createLoginScreen(errorMessage) {
	try {
		if (!errorMessage) {
			errorMessage = '';
		}

		var form = nlapiCreateForm('Vendor Login');

		var emailField = form.addField('custpage_vendor_email', 'email',
		        'Email');
		emailField.setMandatory(true);
		emailField.setLayoutType('startrow', 'startrow');

		var passwordField = form.addField('custpage_vendor_password',
		        'password', 'Password');
		passwordField.setMandatory(true);
		passwordField.setLayoutType('startrow', 'startrow');

		var messageField = form.addField('custpage_message', 'inlinehtml', '');
		messageField.setDefaultValue("<p style='color:red'>" + errorMessage
		        + "</p>");
		messageField.setLayoutType('startrow', 'startrow');

		form.addSubmitButton('Login');
		response.writePage(form);
	} catch (err) {
		nlapiLogExecution('ERROR', 'createLoginScreen', err);
		throw err;
	}
}

function submitLoginScreen(request) {
	try {
		var email = request.getParameter('custpage_vendor_email');
		var password = request.getParameter('custpage_vendor_password');

		if (email && password) {
			password = nlapiEncrypt(password);
			var vendorSearch = nlapiSearchRecord(
			        'customrecord_contract_management', null,
			        [ new nlobjSearchFilter('custrecord_cms_admin_email', null,
			                'is', email) ], [ new nlobjSearchColumn(
			                'custrecord_cms_login_password') ]);

			if (vendorSearch) {

				// check if password match
				if (vendorSearch[0].getValue('custrecord_cms_login_password') == password) {
					// generate access token
					var newAccessTokenA = "set";
					var newAccessTokenB = "set";

					// set token in vendor record
					nlapiSubmitField('customrecord_contract_management',
					        vendorSearch[0].getId(), [
					                'custrecord_cms_login_access_token_1',
					                'custrecord_cms_login_access_token_2' ], [
					                newAccessTokenA, newAccessTokenB ]);

					nlapiLogExecution('debug', 'access token reset');

					var fileUploadSuiteletUrl = 'https://forms.sandbox.netsuite.com/app/site/hosting/scriptlet.nl?script=863&deploy=1&compid=3883006&h=4e5e3a8366b43c3085d8';

					var context = nlapiGetContext();
					var environment = context.getEnvironment();

					// : todo
					if (environment == 'SANDBOX') {

					} else {

					}

					fileUploadSuiteletUrl = fileUploadSuiteletUrl + "&t1="
					        + newAccessTokenA + "&t2=" + newAccessTokenB
					        + "&e=" + email;

					nlapiLogExecution('debug', 'fileUploadSuiteletUrl',
					        fileUploadSuiteletUrl);

					// redirect to the file upload page
					response.sendRedirect('EXTERNAL', fileUploadSuiteletUrl);
				}
			} else {
				createLoginScreen("Wrong Email Id or Password");
			}
		} else {
			createLoginScreen("Mandatory field Missing");
		}
	} catch (err) {
		nlapiLogExecution('ERROR', 'submitLoginScreen', err);
		throw err;
	}
}
