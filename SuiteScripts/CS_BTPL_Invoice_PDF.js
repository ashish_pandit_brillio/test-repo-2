/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       14 Jul 2016     deepak.srinivas
 *
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType 
 * 
 * @param {String} type Access mode: create, copy, edit
 * @returns {Void}
 */
function fxn_generatePDF(internalid)
{
try{

nlapiLogExecution('DEBUG','InternalID',internalid);
window.open("/app/site/hosting/scriptlet.nl?script=1135&deploy=1&id="+internalid);
}
catch(err){
nlapiLogExecution('ERROR','BTPL PDF Generation Error',err);
}
}
