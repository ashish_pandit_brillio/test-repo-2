/**
 * Disable the first level approver field of an expense report
 * 
 * Version Date Author Remarks 1.00 17 Feb 2015 nitish.mishra
 * 
 */

function beforeLoad(type, form) {

	try {
		var user_role = nlapiGetRole();
		
		// if user is not admin or full access
		if (user_role != constant.Role.Administrator && user_role != constant.Role.FullAccess && user_role != constant.Role.businessOps) {
			var first_approver = form.getField(constant.ExpenseReport.FirstLevelApprover);
			first_approver.setDisplayType('inline');
		
			var second_approver = form.getField(constant.ExpenseReport.SecondLevelApprover);
			second_approver.setDisplayType('inline');
         
		}
	}
	catch (err) {
		nlapiLogExecution('ERROR', 'beforeLoad', err);
	}
}