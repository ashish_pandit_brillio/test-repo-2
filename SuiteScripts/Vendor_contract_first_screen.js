function vendor_contract_screen(){
	
	
	
	try{
			if (request.getMethod() == 'GET'){
						
							var objUser = nlapiGetContext();
							var o_values  = new Object();
							var i_employee_id = objUser.getUser();
							var i_roleID = objUser.getRole();
							var s_role	=	getRoleName(i_employee_id, objUser.getRole());
							var o_employee_data	=	nlapiLookupField('employee', i_employee_id, ['firstname', 'lastname', 'employeestatus']);
						
							o_values['username']	=	o_employee_data.firstname + ' ' + o_employee_data.lastname;
							o_values['role']		=	s_role;
							
							var s_new_request_url	=	nlapiResolveURL('SUITELET', 'customscript_sut_recruiters_screen', 'customdeploy1');
							var s_new_home_url	=	nlapiResolveURL('SUITELET', 'customscript_vendor_contract_screen', 'customdeploy1');
							var s_my_requests_url	=	nlapiResolveURL('SUITELET', 'customscript_sut_my_request_form', 'customdeploy1');
							var s_search_URL = nlapiResolveURL('SUITELET', 'customscript_sut_searchvendorinformation', 'customdeploy1');
							var s_edit_mode_URL = nlapiResolveURL('SUITELET', 'customscript_sut_va_screen_in_edit_mode', 'customdeploy1');
							var s_edit_mode_ref_request = nlapiResolveURL('SUITELET', 'customscript_sut_referral_request', 'customdeploy1');
							var s_edit_mode_inprocess_request = nlapiResolveURL('SUITELET', 'customscript_sut_vendor_in_process', 'customdeploy1');
							var s_edit_mode_vendor_pending = nlapiResolveURL('SUITELET', 'customscript_sut_vendor_pending_approval', 'customdeploy1');
							var s_edit_mode_senddocu = nlapiResolveURL('SUITELET', 'customscript_sut_showdocusignscreen', 'customdeploy1');
							o_values['s_new_home_url']	=	s_new_home_url;
						 	o_values['new_request_url']	=	s_new_request_url;
						 	o_values['my_requests_url']	=	s_my_requests_url;
						 	o_values['s_search_URL'] = 	s_search_URL;
						 	o_values['edit_mode_URL']=	s_edit_mode_URL;
						 	o_values['s_edit_mode_inprocess_request']=	s_edit_mode_inprocess_request;
						 	o_values['s_edit_mode_ref_request']=	s_edit_mode_ref_request;
						 	o_values['s_edit_mode_vendor_pending']=	s_edit_mode_vendor_pending;
						 	o_values['s_edit_mode_senddocu']=	s_edit_mode_senddocu;
							
						 	var o_json = getrequiterdata();
						 	var o_json_ref = getreferraldata();
						 	var o_json_inprocess = getinprocessdata();
						 	var o_json_vendor_pending =getvendorpendingdata();
						 	var o_json_send_docu = getvendorsenddocu();
						 	var i_newrequest_count = o_json.length;
							var i_refrequest_count = o_json_ref.length;
							var i_inprocess_count = o_json_inprocess.length;
							var i_vendor_pending_count = o_json_vendor_pending.length;
							var i_vendor_docu_send_count = o_json_send_docu.length;
						 	o_values['i_newrequest_count'] = i_newrequest_count;
						 	o_values['i_refrequest_count'] = i_refrequest_count;
						 	o_values['i_inprocess_count'] = i_inprocess_count;
						 	o_values['i_vendor_pending_count'] = i_vendor_pending_count;
						 	o_values['i_vendor_docu_send_count'] = i_vendor_docu_send_count;
						 	var file = nlapiLoadFile(2046233);  // contract screen file which is store in FC.
						 	var contents = file.getValue();    //get the contents
						 	//nlapiLogExecution('debug', 's_tablestring', s_tablestring);
						 	contents = replaceValues(contents, o_values);
						 	// nlapiLogExecution('debug', 'contents', contents);
						 	response.write(contents);
			}
		
		
	}catch(e){
		
		nlapiLogExecution('debug', 'vendor_contract_screen ERROR', e.getDetails());
	}
	
}

function getvendorsenddocu(){
	
	var temp = 0;
	//nlapiLogExecution('debug', 'getrequiterdata', i_employee_id);
 	var filters =	new Array();
 	var columns	=	new Array();
 	//if(i_employee_id != null && i_employee_id != ''){
 		filters[temp++]	=	new nlobjSearchFilter('custrecord_docusign_process',null,'is','T'); // send document check box is checked 
 		filters[temp++]	=	new nlobjSearchFilter('custrecord_approval_status_recruiters',null,'anyof',2); // in process 
 	//}
 	

	
	var searchResults	=	searchRecord('customrecord_recruiters_details', null, filters, columns);
	nlapiLogExecution('debug', 'searchResults', searchResults.length);
	var a_data	=	new Array();

	if(searchResults){
			
			var k =1;
			for(var i = 0; i < searchResults.length; i++){
			var s_record_id	=	searchResults[i].getValue(columns[0]);
			a_data.push({ 'record_id': s_record_id, 'sr_no': k});
			k++;
		}
		

	}
	
	return a_data;
	
}

function getrequiterdata()	{
 	
	var temp = 0;
	//nlapiLogExecution('debug', 'getrequiterdata', i_employee_id);
 	var filters =	new Array();
 	var columns	=	new Array();
 	//if(i_employee_id != null && i_employee_id != ''){
 		filters[temp++]	=	new nlobjSearchFilter('custrecord_approval_status_recruiters',null,'anyof',4);
 		filters[temp++]	=	new nlobjSearchFilter('custrecord_contract_type',null,'noneof',4);
 	//}
 	

	
	var searchResults	=	searchRecord('customrecord_recruiters_details', null, filters, columns);
	nlapiLogExecution('debug', 'searchResults', searchResults.length);
	var a_data	=	new Array();

	if(searchResults){
			
			var k =1;
			for(var i = 0; i < searchResults.length; i++){
			var s_record_id	=	searchResults[i].getValue(columns[0]);
			a_data.push({ 'record_id': s_record_id, 'sr_no': k});
			k++;
		}
		

	}
	
	return a_data;
}
// get referral request data count 

function getreferraldata()	{
 	
	var temp = 0;
	//nlapiLogExecution('debug', 'getrequiterdata', i_employee_id);
 	var filters =	new Array();
 	var columns	=	new Array();
 	//if(i_employee_id != null && i_employee_id != ''){
 	
 		filters[temp++]	=	new nlobjSearchFilter('custrecord_contract_type',null,'anyof',4);
 		filters[temp++]	=	new nlobjSearchFilter('custrecord_approval_status_recruiters',null,'noneof',3);
 		filters[temp++]	=	new nlobjSearchFilter('custrecord_docusign_process',null,'is','F');
 		
	var searchResults	=	searchRecord('customrecord_recruiters_details', null, filters, columns);
	nlapiLogExecution('debug', 'searchResults', searchResults.length);
	var a_data	=	new Array();

	if(searchResults){
			
			var k =1;
			for(var i = 0; i < searchResults.length; i++){
			var s_record_id	=	searchResults[i].getValue(columns[0]);
			a_data.push({ 'record_id': s_record_id, 'sr_no': k});
			k++;
		}
		

	}
	
	return a_data;
}

// get vendor in process data none of Referral Deal 
function getinprocessdata(){
	var temp = 0;
	//nlapiLogExecution('debug', 'getrequiterdata', i_employee_id);
 	var filters =	new Array();
 	var columns	=	new Array();
 	//if(i_employee_id != null && i_employee_id != ''){
 	
 		filters[temp++]	=	new nlobjSearchFilter('custrecord_approval_status_recruiters',null,'anyof',2);
 		//filters[temp++]	=	new nlobjSearchFilter('custrecord_contract_type',null,'noneof',4);
 		filters[temp++]	=	new nlobjSearchFilter('custrecord_docusign_process',null,'is',"F");
 	//}
 	
	
	var searchResults	=	searchRecord('customrecord_recruiters_details', null, filters, columns);
	nlapiLogExecution('debug', 'searchResults', searchResults.length);
	var a_data	=	new Array();

	if(searchResults){
			
			var k =1;
			for(var i = 0; i < searchResults.length; i++){
			var s_record_id	=	searchResults[i].getValue(columns[0]);
			a_data.push({ 'record_id': s_record_id, 'sr_no': k});
			k++;
		}
		

	}
	
	return a_data;
}
// Vendor Pending approval 

function getvendorpendingdata(){
	var temp = 0;
	//nlapiLogExecution('debug', 'getrequiterdata', i_employee_id);
 	var filters =	new Array();
 	var columns	=	new Array();
 	//if(i_employee_id != null && i_employee_id != ''){
 	
 		filters[temp++]	=	new nlobjSearchFilter('custrecord_docusign_process',null,'is','T');
 		filters[temp++]	=	new nlobjSearchFilter('custrecord_approval_status_recruiters',null,'anyof',2);
 	//}
 		
	
	var searchResults	=	searchRecord('customrecord_recruiters_details', null, filters, columns);
	nlapiLogExecution('debug', 'searchResults', searchResults.length);
	var a_data	=	new Array();

	if(searchResults){
			
			var k =1;
			for(var i = 0; i < searchResults.length; i++){
			var s_record_id	=	searchResults[i].getValue(columns[0]);
			a_data.push({ 'record_id': s_record_id, 'sr_no': k});
			k++;
		}
		

	}
	
	return a_data;
}

// replace content replaceValues

function replaceValues(content, oValues)
{
	for(param in oValues)
		{
			// Replace null values with blank
			var s_value	=	(oValues[param] == null)?'':oValues[param];
			
			// Replace content
			content = content.replace(new RegExp('{{' + param + '}}','gi'), s_value);
		}
	
	return content;
}

//get role name 
function getRoleName(i_user_id, i_role_id)
{
	var a_search_results	=	nlapiSearchRecord('employee', null, [new nlobjSearchFilter('internalid', null, 'anyof', i_user_id), new nlobjSearchFilter('role', null, 'anyof', i_role_id)], new nlobjSearchColumn('role'));
	
	if(a_search_results != null && a_search_results.length == 1)
		{
			return a_search_results[0].getText('role');
		}
	else
		{
			return '';
		}
}

//search code for all search 
function searchRecord(recordType, savedSearch, arrFilters, arrColumns,  filterExpression){

	try {
		var search = null;

		// if a saved search is provided, load it and add the filters and
		// columns
		if (isNotEmpty(savedSearch)) {
			search = nlapiLoadSearch(recordType, savedSearch);

			if (isArrayNotEmpty(arrFilters)) {
				search.addFilters(arrFilters);
			}

			if (isArrayNotEmpty(arrColumns)) {
				search.addColumns(arrColumns);
			}

			if (isArrayNotEmpty(filterExpression)) {
				search.setFilterExpression(filterExpression);
			}
		}
		// create a new search
		else {
			search = nlapiCreateSearch(recordType, arrFilters, arrColumns);
		}

		// run search
		var resultSet = search.runSearch();

		// iterate through the search and get all data 1000 at a time
		var searchResultCount = 0;
		var resultSlice = null;
		var searchResult = [];

		do {
			resultSlice = resultSet.getResults(searchResultCount,
			        searchResultCount + 1000);

			if (resultSlice) {

				resultSlice.forEach(function(result) {

					searchResult.push(result);
					searchResultCount++;
				});
			}
		} while (isArrayNotEmpty(resultSlice) && resultSlice.length >= 1000);

		return searchResult;
	} catch (err) {
		nlapiLogExecution('ERROR', 'searchRecord', err);
		throw err;
	}
}

function isEmpty(value) {

	return value == null || value == "" || typeof (value) == undefined;
}

function isNotEmpty(value) {

	return !isEmpty(value);
}

function isArrayEmpty(argArray) {

	return !isArrayNotEmpty(argArray);
}

function isArrayNotEmpty(argArray) {

	return (isNotEmpty(argArray) && argArray.length > 0);
}

function _logValidation(value)  
{
	if(value != null && value != '' && value != undefined && value.toString() != 'NaN' && value != NaN)
	{
		return true;
	}
	else
	{
		return false;
	}
}