 // BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
/*
   	Script Name : REST_POCSearchCount.js
	Author      : ASHISH PANDIT
	Date        : 08 July 2019
    Description : RESTlet to show main dashboard 


	Script Modification Log:

 


Below is a summary of the process controls enforced by this script file.  The control logic is described
more fully, below, in the appropriate function headers and code blocks.


     SCHEDULED FUNCTION
		- scheduledFunction(type)


     SUB-FUNCTIONS
		- The following sub-functions are called by the above core functions in order to maintain code
            modularization:

               - NOT USED

*/
}
// END SCRIPT DESCRIPTION BLOCK  ====================================
var d_today = '';
var month_array = ['JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUN', 'JUL', 'AUG', 'SEP', 'OCT', 'NOV', 'DEC'];
//Main function 
function CreateDashboard(dataIn)
{
    try
	{
		//nlapiLogExecution('debug','dataIn ',JSON.stringify(dataIn));
		var values = {};
		var i_userEmail = dataIn.user;
		var i_user = getUserUsingEmailId(i_userEmail);
		var s_filters = dataIn.globalFilters;
		var s_graphfilters = dataIn.graphFilter;
		s_filters = JSON.stringify(s_filters);
		s_graphfilters = JSON.stringify(s_graphfilters);
		//nlapiLogExecution('debug','s_filters *** ',s_filters);
		/***For testing Region Head Persona 05-08-2019***/
		//if(i_user == 94862)
			//i_user = 148781;
		/*************************************************/
		var adminUser = GetAdmin(i_user);
		var spocSearch = GetPracticeSPOC(i_user);
		var region = GetRegion(i_user);
		//d_today = dataIn.date;
		d_today = '08/09/2019';
		d_today = nlapiStringToDate(d_today);
		if(adminUser)
		{
			// Create JSON for dashboard table when logged in user in practice SPOC
			var o_openFRFCount = OpenFRFCount(null,null,true,null,s_filters);
			var o_externalRiskCount = ExternalRiskCount(null,null,true,null,s_filters);
			var o_internalRiskCount = InternalRiskCount(null,null,true,null,s_filters);
			var o_overdueCount = OverdueCount(null,null,true,null,s_filters);
			var o_utilizationPercent = UtilizationPercent(null,null,true,null,s_filters);
			var o_leakageAmount = LeakageAmount(null,null,true,null,s_filters);
			var o_ontimePercent =OntimePercent(null,null,true,null,s_filters);
			var o_internalFulfillmentPercent = InternalFulfillmentPercent(null,null,true,null,s_filters);
			var o_projectsCount = ProjectsCount(null,null,true,null,s_filters);
			var o_projectsWithRiskCount = ProjectsWithRiskCount(null,null,true,null,s_filters);
			var o_projectPositionsAtRiskCount = ProjectPositionsAtRiskCount(null,null,true,null,s_filters);
			var o_mlOpportunitiesCount = MLOpportunitiesCount(null,null,true,null,s_filters);
			var o_mlOpportunitiesPositionsRequestedCount = MLOpportunitiesPositionsRequestedCount(null,null,true,null,s_filters);
			var o_mlOpportunitiesFrfsCreatedCount = MLOpportunitiesFrfsCreatedCount(null,null,true,null,s_filters);
			var o_pipelineOpportunitiesCount = PipelineOpportunitiesCount(null,null,true,null,s_filters);
			var o_pipelinePositionsRequestedCount = PipelinePositionsRequestedCount(null,null,true,null,s_filters);
			var o_pipelineFrfsCreatedCount = PipelineFrfsCreatedCount(null,null,true,null,s_filters);
			var o_graphData = graphData(null,null,true,null,s_graphfilters,s_filters); 
			////nlapiLogExecution('AUDIT', 'o_headerTileData adminUser', o_headerTileData);
			
			values.openFrfCount = parseInt(o_openFRFCount);
			values.externalRiskCount = parseInt(o_externalRiskCount);
			values.internalRiskCount = parseInt(o_internalRiskCount);
			values.overdueCount = parseInt(o_overdueCount);
			values.utilizationPercent = parseFloat(o_utilizationPercent);
			values.leakageAmount = parseFloat(o_leakageAmount);
			values.ontimePercent = parseFloat(o_ontimePercent);
			values.internalFulfillmentPercent = parseFloat(o_internalFulfillmentPercent);
			values.projectsCount = parseInt(o_projectsCount);
			values.projectsWithRiskCount = parseInt(o_projectsWithRiskCount);
			values.projectPositionsAtRiskCount = parseInt(o_projectPositionsAtRiskCount);
			values.mlOpportunitiesCount = parseInt(o_mlOpportunitiesCount);
			values.mlOpportunitiesPositionsRequestedCount = parseInt(o_mlOpportunitiesPositionsRequestedCount);
			values.mlOpportunitiesFrfsCreatedCount = parseInt(o_mlOpportunitiesFrfsCreatedCount);
			values.pipelineOpportunitiesCount = parseInt(o_pipelineOpportunitiesCount);
			values.pipelinePositionsRequestedCount = parseInt(o_pipelinePositionsRequestedCount);
			values.pipelineFrfsCreatedCount = parseInt(o_pipelineFrfsCreatedCount);
			values.graphData = o_graphData;
		}
		else if(spocSearch.length>0) 
		{
			//nlapiLogExecution('AUDIT', 'In spoc', i_practice);
			var i_practice = spocSearch;//[0].getValue('custrecord_practice');
			var a_subPractices = getSubPractices(i_practice);
			// Create JSON for dashboard table when logged in user in practice SPOC
			var o_openFRFCount = OpenFRFCount(null,a_subPractices,null,null,s_filters);
			var o_externalRiskCount = ExternalRiskCount(null,a_subPractices,null,null,s_filters);
			var o_internalRiskCount = InternalRiskCount(null,a_subPractices,null,null,s_filters);
			var o_overdueCount = OverdueCount(null,a_subPractices,null,null,s_filters);
			var o_utilizationPercent = UtilizationPercent(null,a_subPractices,null,null,s_filters);
			var o_leakageAmount = LeakageAmount(null,a_subPractices,null,null,s_filters);
			var o_ontimePercent =OntimePercent(null,a_subPractices,null,null,s_filters);
			var o_internalFulfillmentPercent = InternalFulfillmentPercent(null,a_subPractices,null,null,s_filters);
			var o_projectsCount = ProjectsCount(null,a_subPractices,null,null,s_filters);
			var o_projectsWithRiskCount = ProjectsWithRiskCount(null,a_subPractices,null,null,s_filters);
			var o_projectPositionsAtRiskCount = ProjectPositionsAtRiskCount(null,a_subPractices,null,null,s_filters);
			var o_mlOpportunitiesCount = MLOpportunitiesCount(null,a_subPractices,null,null,s_filters);
			var o_mlOpportunitiesPositionsRequestedCount = MLOpportunitiesPositionsRequestedCount(null,a_subPractices,null,null,s_filters);
			var o_mlOpportunitiesFrfsCreatedCount = MLOpportunitiesFrfsCreatedCount(null,a_subPractices,null,null,s_filters);
			var o_pipelineOpportunitiesCount = PipelineOpportunitiesCount(null,a_subPractices,null,null,s_filters);
			var o_pipelinePositionsRequestedCount = PipelinePositionsRequestedCount(null,a_subPractices,null,null,s_filters);
			var o_pipelineFrfsCreatedCount = PipelineFrfsCreatedCount(null,a_subPractices,null,null,s_filters);
			var o_graphData = graphData(i_user,a_subPractices,null,null,s_graphfilters); 
			////nlapiLogExecution('AUDIT', 'o_headerTileData adminUser', o_headerTileData);
			
			values.openFrfCount = parseInt(o_openFRFCount);
			values.externalRiskCount = parseInt(o_externalRiskCount);
			values.internalRiskCount = parseInt(o_internalRiskCount);
			values.overdueCount = parseInt(o_overdueCount);
			values.utilizationPercent = parseFloat(o_utilizationPercent);
			values.leakageAmount = parseFloat(o_leakageAmount);
			values.ontimePercent = parseFloat(o_ontimePercent);
			values.internalFulfillmentPercent = parseFloat(o_internalFulfillmentPercent);
			values.projectsCount = parseInt(o_projectsCount);
			values.projectsWithRiskCount = parseInt(o_projectsWithRiskCount);
			values.projectPositionsAtRiskCount = parseInt(o_projectPositionsAtRiskCount);
			values.mlOpportunitiesCount = parseInt(o_mlOpportunitiesCount);
			values.mlOpportunitiesPositionsRequestedCount = parseInt(o_mlOpportunitiesPositionsRequestedCount);
			values.mlOpportunitiesFrfsCreatedCount = parseInt(o_mlOpportunitiesFrfsCreatedCount);
			values.pipelineOpportunitiesCount = parseInt(o_pipelineOpportunitiesCount);
			values.pipelinePositionsRequestedCount = parseInt(o_pipelinePositionsRequestedCount);
			values.pipelineFrfsCreatedCount = parseInt(o_pipelineFrfsCreatedCount);
			values.graphData = o_graphData;
		}
		else if(region)
		{
			// Create JSON for dashboard table when logged in user in practice SPOC
			//var o_headerTileData = GetDashBoardDataPractice(null,null,null,region);
			var o_openFRFCount = OpenFRFCount(null,null,null,region,s_filters);
			var o_externalRiskCount = ExternalRiskCount(null,null,null,region,s_filters);
			var o_internalRiskCount = InternalRiskCount(null,null,null,region,s_filters);
			var o_overdueCount = OverdueCount(null,null,null,region,s_filters);
			var o_utilizationPercent = UtilizationPercent(i_user,null,null,region,s_filters);
			var o_leakageAmount = LeakageAmount(null,null,null,region,s_filters);
			var o_ontimePercent =OntimePercent(null,null,null,region,s_filters);
			var o_internalFulfillmentPercent = InternalFulfillmentPercent(null,null,null,region,s_filters);
			var o_projectsCount = ProjectsCount(null,null,null,region,s_filters);
			var o_projectsWithRiskCount = ProjectsWithRiskCount(null,null,null,region,s_filters);
			var o_projectPositionsAtRiskCount = ProjectPositionsAtRiskCount(null,null,null,region,s_filters);
			var o_mlOpportunitiesCount = MLOpportunitiesCount(null,null,null,region,s_filters);
			var o_mlOpportunitiesPositionsRequestedCount = MLOpportunitiesPositionsRequestedCount(null,null,null,region,s_filters);
			var o_mlOpportunitiesFrfsCreatedCount = MLOpportunitiesFrfsCreatedCount(null,null,null,region,s_filters);
			var o_pipelineOpportunitiesCount = PipelineOpportunitiesCount(null,null,null,region,s_filters);
			var o_pipelinePositionsRequestedCount = PipelinePositionsRequestedCount(null,null,null,region,s_filters);
			var o_pipelineFrfsCreatedCount = PipelineFrfsCreatedCount(null,null,null,region,s_filters);
			var o_graphData = graphData(i_user,null,null,region,s_graphfilters); 
			////nlapiLogExecution('AUDIT', 'o_headerTileData adminUser', o_headerTileData);
			
			values.openFrfCount = parseInt(o_openFRFCount);
			values.externalRiskCount = parseInt(o_externalRiskCount);
			values.internalRiskCount = parseInt(o_internalRiskCount);
			values.overdueCount = parseInt(o_overdueCount);
			values.utilizationPercent = parseFloat(o_utilizationPercent);
			values.leakageAmount = parseFloat(o_leakageAmount);
			values.ontimePercent = parseFloat(o_ontimePercent);
			values.internalFulfillmentPercent = parseFloat(o_internalFulfillmentPercent);
			values.projectsCount = parseInt(o_projectsCount);
			values.projectsWithRiskCount = parseInt(o_projectsWithRiskCount);
			values.projectPositionsAtRiskCount = parseInt(o_projectPositionsAtRiskCount);
			values.mlOpportunitiesCount = parseInt(o_mlOpportunitiesCount);
			values.mlOpportunitiesPositionsRequestedCount = parseInt(o_mlOpportunitiesPositionsRequestedCount);
			values.mlOpportunitiesFrfsCreatedCount = parseInt(o_mlOpportunitiesFrfsCreatedCount);
			values.pipelineOpportunitiesCount = parseInt(o_pipelineOpportunitiesCount);
			values.pipelinePositionsRequestedCount = parseInt(o_pipelinePositionsRequestedCount);
			values.pipelineFrfsCreatedCount = parseInt(o_pipelineFrfsCreatedCount);
			values.graphData = o_graphData;
		}
		else if(i_user)
		{
			var o_openFRFCount = OpenFRFCount(i_user,null,null,null,s_filters);
			var o_externalRiskCount = ExternalRiskCount(i_user,null,null,null,s_filters);
			var o_internalRiskCount = InternalRiskCount(i_user,null,null,null,s_filters);
			var o_overdueCount = OverdueCount(i_user,null,null,null,s_filters);
			var o_utilizationPercent = UtilizationPercent(i_user,null,null,null,s_filters);
			var o_leakageAmount = LeakageAmount(i_user,null,null,null,s_filters);
			var o_ontimePercent =OntimePercent(i_user,null,null,null,s_filters);
			var o_internalFulfillmentPercent = InternalFulfillmentPercent(i_user,null,null,null,s_filters);
			var o_projectsCount = ProjectsCount(i_user,null,null,null,s_filters);
			var o_projectsWithRiskCount = ProjectsWithRiskCount(i_user,null,null,null,s_filters);
			var o_projectPositionsAtRiskCount = ProjectPositionsAtRiskCount(i_user,null,null,null,s_filters);
			var o_mlOpportunitiesCount = MLOpportunitiesCount(i_user,null,null,null,s_filters);
			var o_mlOpportunitiesPositionsRequestedCount = MLOpportunitiesPositionsRequestedCount(i_user,null,null,null,s_filters);
			var o_mlOpportunitiesFrfsCreatedCount = MLOpportunitiesFrfsCreatedCount(i_user,null,null,null,s_filters);
			var o_pipelineOpportunitiesCount = PipelineOpportunitiesCount(i_user,null,null,null,s_filters);
			var o_pipelinePositionsRequestedCount = PipelinePositionsRequestedCount(i_user,null,null,null,s_filters);
			var o_pipelineFrfsCreatedCount = PipelineFrfsCreatedCount(i_user,null,null,null,s_filters);
			var o_graphData = graphData(i_user,null,null,null,s_graphfilters); 
			////nlapiLogExecution('AUDIT', 'o_headerTileData adminUser', o_headerTileData);
			
			values.openFrfCount = parseInt(o_openFRFCount);
			values.externalRiskCount = parseInt(o_externalRiskCount);
			values.internalRiskCount = parseInt(o_internalRiskCount);
			values.overdueCount = parseInt(o_overdueCount);
			values.utilizationPercent = parseFloat(o_utilizationPercent);
			values.leakageAmount = parseFloat(o_leakageAmount);
			values.ontimePercent = parseFloat(o_ontimePercent);
			values.internalFulfillmentPercent = parseFloat(o_internalFulfillmentPercent);
			values.projectsCount = parseInt(o_projectsCount);
			values.projectsWithRiskCount = parseInt(o_projectsWithRiskCount);
			values.projectPositionsAtRiskCount = parseInt(o_projectPositionsAtRiskCount);
			values.mlOpportunitiesCount = parseInt(o_mlOpportunitiesCount);
			values.mlOpportunitiesPositionsRequestedCount = parseInt(o_mlOpportunitiesPositionsRequestedCount);
			values.mlOpportunitiesFrfsCreatedCount = parseInt(o_mlOpportunitiesFrfsCreatedCount);
			values.pipelineOpportunitiesCount = parseInt(o_pipelineOpportunitiesCount);
			values.pipelinePositionsRequestedCount = parseInt(o_pipelinePositionsRequestedCount);
			values.pipelineFrfsCreatedCount = parseInt(o_pipelineFrfsCreatedCount);
			values.graphData = o_graphData;
		}
		////nlapiLogExecution('AUDIT','values ',JSON.Stringify(values));
		return values;
	}
	catch(exc)
	{
		//nlapiLogExecution('Debug','Exception ',exc);
        return {
            "code": "Error",
            "message": exc
        }
	}
	
}

function GetAdmin(user) 
{
  	var adminSearch = nlapiSearchRecord("customrecord_fuel_leadership",null,
			[
			 ["custrecord_fuel_leadership_employee","anyof",user],
			  "AND",
			 ["isinactive","is","F"]
			], 
			[
			  new nlobjSearchColumn("custrecord_fuel_leadership_employee")
			]
	);
	return adminSearch;
}
function GetPracticeSPOC(user) 
{
  	var practiceArray = new Array();
	var spocSearch = nlapiSearchRecord("customrecord_practice_spoc",null,
			[
			 ["custrecord_spoc","anyof",user],
			  "AND",
			 ["isinactive","is","F"]
			], 
			[
			  new nlobjSearchColumn("custrecord_practice")
			]
	);
	if(spocSearch)
	{
		for(var i=0; i<spocSearch.length; i++)
		{
			practiceArray.push(spocSearch[i].getValue('custrecord_practice'));
		}
	}
	return practiceArray;
}
function OpenFRFCount(i_user, practiceId, isAdmin,i_region, s_filters)
{
	try
	{
		
		var filters = new Array();
		var columns = new Array();
		//nlapiLogExecution('audit','s_filters in count function ',s_filters);
		s_filters = JSON.parse(s_filters);
		//nlapiLogExecution('audit','s_filters in count function ',JSON.stringify(s_filters));
		if(isAdmin == true)
		{
			filters.push(["isinactive","is","F"],"AND",["custrecord_frf_details_status_flag","anyof","1","3"],"AND",["custrecord_frf_details_opp_id.custrecord_stage_sfdc","noneof","8","9"],"AND",["custrecord_frf_details_status","is","F"]); 
			if(isArrayNotEmpty(s_filters.region))
			{
				filters.push('AND');
				filters.push(["custrecord_frf_details_account.custentity_region","anyof",s_filters.region]);
			}
			if(isArrayNotEmpty(s_filters.practice))
			{
				filters.push('AND');
				filters.push(["custrecord_frf_details_res_practice","anyof",s_filters.practice]);
			}
			if(isArrayNotEmpty(s_filters.account))
			{
				filters.push('AND');
				filters.push(["custrecord_frf_details_account","anyof",s_filters.account]);
			}
			if(isArrayNotEmpty(s_filters.project))
			{
				filters.push('AND');
				filters.push(["custrecord_frf_details_project","anyof",s_filters.project],"OR",["custrecord_frf_details_opp_id","anyof",s_filters.project]);
			}
		}
		else if(practiceId)
		{
			var practiceArray = getSubPractices(practiceId);
		   	filters.push(["custrecord_frf_details_res_practice","anyof",practiceArray]);
		   	filters.push('AND');
		   	filters.push(["isinactive","is","F"],"AND",["custrecord_frf_details_status_flag","anyof","1","3"],"AND",["custrecord_frf_details_opp_id.custrecord_stage_sfdc","noneof","8","9"],"AND",["custrecord_frf_details_status","is","F"]); 
			if(isArrayNotEmpty(s_filters.region))
			{
				filters.push('AND');
				filters.push(["custrecord_frf_details_account.custentity_region","anyof",s_filters.region]);
			}
			if(isArrayNotEmpty(s_filters.practice))
			{
				filters.push('AND');
				filters.push(["custrecord_frf_details_res_practice","anyof",s_filters.practice]);
			}
			if(isArrayNotEmpty(s_filters.account))
			{
				filters.push('AND');
				filters.push(["custrecord_frf_details_account","anyof",s_filters.account]);
			}
			if(isArrayNotEmpty(s_filters.project))
			{
				filters.push('AND');
				filters.push(["custrecord_frf_details_project","anyof",s_filters.project],"OR",["custrecord_frf_details_opp_id","anyof",s_filters.project]);
			}
		}
		else if(i_region)
		{
			 
		   filters.push(["custrecord_frf_details_account.custentity_region","anyof",i_region]);
		   filters.push('AND');
		   filters.push(["isinactive","is","F"],"AND",["custrecord_frf_details_status_flag","anyof","1","3"],"AND",["custrecord_frf_details_opp_id.custrecord_stage_sfdc","noneof","8","9"],"AND",["custrecord_frf_details_status","is","F"]); 
			if(isArrayNotEmpty(s_filters.region))
			{
				filters.push('AND');
				filters.push(["custrecord_frf_details_account.custentity_region","anyof",s_filters.region]);
			}
			if(isArrayNotEmpty(s_filters.practice))
			{
				filters.push('AND');
				filters.push(["custrecord_frf_details_res_practice","anyof",s_filters.practice]);
			}
			if(isArrayNotEmpty(s_filters.account))
			{
				filters.push('AND');
				filters.push(["custrecord_frf_details_account","anyof",s_filters.account]);
			}
			if(isArrayNotEmpty(s_filters.project))
			{
				filters.push('AND');
				filters.push(["custrecord_frf_details_project","anyof",s_filters.project],"OR",["custrecord_frf_details_opp_id","anyof",s_filters.project]);
			}
		}
		else if(i_user)
		{
			filters.push([["custrecord_frf_details_project.custentity_projectmanager","anyof",i_user],"OR",["custrecord_frf_details_project.custentity_deliverymanager","anyof",i_user],"OR",["custrecord_frf_details_project.custentity_clientpartner","anyof",i_user]],"AND",["isinactive","is","F"],"AND",["custrecord_frf_details_status_flag","anyof","1","3"],"AND",["custrecord_frf_details_opp_id.custrecord_stage_sfdc","noneof","8","9"],"AND",["custrecord_frf_details_status","is","F"]);
			filters.push('OR');
			filters.push([["custrecord_frf_details_opp_id.custrecord_sfdc_opp_pm","anyof",i_user],"OR",["custrecord_frf_details_opp_id.custrecord_sfdc_opportunity_dm","anyof",i_user],"OR",["custrecord_frf_details_opp_id.custrecord_sfdc_client_partner","anyof",i_user],"AND",["isinactive","is","F"],"AND",["custrecord_frf_details_status_flag","anyof","1","3"],"AND",["custrecord_frf_details_opp_id.custrecord_stage_sfdc","noneof","8","9"],"AND",["custrecord_frf_details_status","is","F"]]);
			
			if(isArrayNotEmpty(s_filters.region))
			{
				filters.push('AND');
				filters.push(["custrecord_frf_details_account.custentity_region","anyof",s_filters.region]);
			}
			if(isArrayNotEmpty(s_filters.practice))
			{
				filters.push('AND');
				filters.push(["custrecord_frf_details_res_practice","anyof",s_filters.practice]);
			}
			if(isArrayNotEmpty(s_filters.account))
			{
				filters.push('AND');
				filters.push(["custrecord_frf_details_account","anyof",s_filters.account]);
			}
			if(isArrayNotEmpty(s_filters.project))
			{
				filters.push('AND');
				filters.push(["custrecord_frf_details_project","anyof",s_filters.project],"OR",["custrecord_frf_details_opp_id","anyof",s_filters.project]);
			}
			//nlapiLogExecution('audit','Filters !!!!',filters);
 
		}
		 //nlapiLogExecution('audit','Filters !!!!',filters);
		columns = [
		  new nlobjSearchColumn("internalid",null,"COUNT")
		]
		var customrecord_frf_detailsSearch = searchRecord("customrecord_frf_details", null, filters, columns,null)	
		if(customrecord_frf_detailsSearch) 
		{
			var dataArray = new Array();
			/*for(var i = 0; i<customrecord_frf_detailsSearch.length; i++)
			{
				var data = {};
				data.count = {"name":customrecord_frf_detailsSearch[i].getValue("internalid",null,"COUNT")}
				dataArray.push(data);
			}*/
			
			return customrecord_frf_detailsSearch[0].getValue("internalid",null,"COUNT");
		}
		else
		{
			var dataArray = [];
			return dataArray;
		}
	}
	catch(error)
	{
		return error;
	}
}

//======================================================================================================================
function ExternalRiskCount(i_user, practiceId, isAdmin,i_region,s_filters)
{
	try
	{
		var filters = new Array();
		var columns = new Array();
		s_filters = JSON.parse(s_filters);
		if(isAdmin == true)
		{
			
			filters.push(["isinactive","is","F"], 
				   "AND", 
				   ["custrecord_frf_details_status","is","F"], 
				   "AND", 
				   ["custrecord_frf_details_external_hire","is","T"], 
				   "AND", 
				   ["custrecord_frf_details_opp_id.custrecord_stage_sfdc","noneof","8","9"], 
				   "AND", 
				   ["formulanumeric: {custrecord_frf_details_availabledate} - {custrecord_frf_details_start_date}","greaterthan","0"], 
				   "AND", 
				   [["custrecord_frf_details_project.type","anyof","2"],"OR",["custrecord_frf_details_opp_id","noneof","@NONE@"]], 
				   "AND", 
				   ["custrecord_frf_details_start_date","onorafter",d_today], 
				   "AND", 
				   ["custrecord_frf_details_status_flag","anyof","1"], 
				   "AND", 
				   ["custrecord_frf_details_availabledate","after",d_today], 
				   "AND", 
				   ["custrecord_frf_details_availabledate","isnotempty",""]);
					if(isArrayNotEmpty(s_filters.region))
					{
						filters.push('AND');
						filters.push(["custrecord_frf_details_account.custentity_region","anyof",s_filters.region]);
					}
					if(isArrayNotEmpty(s_filters.practice))
					{
						filters.push('AND');
						filters.push(["custrecord_frf_details_res_practice","anyof",s_filters.practice]);
					}
					if(isArrayNotEmpty(s_filters.account))
					{
						filters.push('AND');
						filters.push(["custrecord_frf_details_account","anyof",s_filters.account]);
					}
					if(isArrayNotEmpty(s_filters.project))
					{
						filters.push('AND');
						filters.push(["custrecord_frf_details_project","anyof",s_filters.project],"OR",["custrecord_frf_details_opp_id","anyof",s_filters.project]);
					}

		}
		else if(practiceId)
		{
			var practiceArray = getSubPractices(practiceId);
			
			   filters.push( ["custrecord_frf_details_res_practice","anyof",practiceArray],
			   "AND",
			   ["isinactive","is","F"], 
			   "AND", 
			   ["custrecord_frf_details_availabledate","after",d_today], 
			   "AND", 
			   ["custrecord_frf_details_availabledate","isnotempty",""], 
			   "AND", 
			   ["custrecord_frf_details_status","is","F"], 
			   "AND", 
			   ["custrecord_frf_details_external_hire","is","T"], 
			   "AND", 
			   ["custrecord_frf_details_opp_id.custrecord_stage_sfdc","noneof","8","9"], 
			   "AND", 
			   ["formulanumeric: {custrecord_frf_details_availabledate} - {custrecord_frf_details_start_date}","greaterthan","0"], 
			   "AND", 
			   ["custrecord_frf_details_start_date","onorafter",d_today], 
			   "AND", 
			   ["custrecord_frf_details_status_flag","anyof","1"], 
			   "AND", 
			   [["custrecord_frf_details_project.type","anyof","2"],"OR",["custrecord_frf_details_opp_id","noneof","@NONE@"]]);
			   if(isArrayNotEmpty(s_filters.region))
				{
					filters.push('AND');
					filters.push(["custrecord_frf_details_account.custentity_region","anyof",s_filters.region]);
				}
				if(isArrayNotEmpty(s_filters.practice))
				{
					filters.push('AND');
					filters.push(["custrecord_frf_details_res_practice","anyof",s_filters.practice]);
				}
				if(isArrayNotEmpty(s_filters.account))
				{
					filters.push('AND');
					filters.push(["custrecord_frf_details_account","anyof",s_filters.account]);
				}
				if(isArrayNotEmpty(s_filters.project))
				{
					filters.push('AND');
					filters.push(["custrecord_frf_details_project","anyof",s_filters.project],"OR",["custrecord_frf_details_opp_id","anyof",s_filters.project]);
				}
				
		}
		else if(i_region)
		{
			 
		   filters.push(["custrecord_frf_details_account.custentity_region","anyof",i_region],
           "AND",
           ["isinactive","is","F"], 
		   "AND", 
		   ["custrecord_frf_details_availabledate","after",d_today], 
		   "AND", 
		   ["custrecord_frf_details_availabledate","isnotempty",""], 
		   "AND", 
		   ["custrecord_frf_details_status","is","F"], 
		   "AND", 
		   ["custrecord_frf_details_external_hire","is","T"], 
		   "AND", 
		   ["custrecord_frf_details_opp_id.custrecord_stage_sfdc","noneof","8","9"], 
		   "AND", 
		   ["formulanumeric: {custrecord_frf_details_availabledate} - {custrecord_frf_details_start_date}","greaterthan","0"], 
		   "AND", 
		   ["custrecord_frf_details_start_date","onorafter",d_today], 
		   "AND", 
		   ["custrecord_frf_details_status_flag","anyof","1"], 
		   "AND", 
		   [["custrecord_frf_details_project.type","anyof","2"],"OR",["custrecord_frf_details_opp_id","noneof","@NONE@"]]);
		   if(isArrayNotEmpty(s_filters.region))
			{
				filters.push('AND');
				filters.push(["custrecord_frf_details_account.custentity_region","anyof",s_filters.region]);
			}
			if(isArrayNotEmpty(s_filters.practice))
			{
				filters.push('AND');
				filters.push(["custrecord_frf_details_res_practice","anyof",s_filters.practice]);
			}
			if(isArrayNotEmpty(s_filters.account))
			{
				filters.push('AND');
				filters.push(["custrecord_frf_details_account","anyof",s_filters.account]);
			}
			if(isArrayNotEmpty(s_filters.project))
			{
				filters.push('AND');
				filters.push(["custrecord_frf_details_project","anyof",s_filters.project],"OR",["custrecord_frf_details_opp_id","anyof",s_filters.project]);
			}
		}
		else if(i_user)
		{
			/*filters = [
				   [[["custrecord_frf_details_project.custentity_projectmanager","anyof",i_user],
				   "OR",
				   ["custrecord_frf_details_project.custentity_deliverymanager","anyof",i_user],
				    "OR",
				   ["custrecord_frf_details_project.custentity_clientpartner","anyof",i_user]],
				   "AND",
				   ["isinactive","is","F"], 
				   "AND", 
				   ["custrecord_frf_details_availabledate","after",d_today], 
				   "AND", 
				   ["custrecord_frf_details_availabledate","isnotempty",""], 
				   "AND", 
				   ["custrecord_frf_details_status","is","F"], 
				   "AND", 
				   ["custrecord_frf_details_external_hire","is","T"], 
				   "AND", 
				   ["custrecord_frf_details_opp_id.custrecord_stage_sfdc","noneof","8","9"], 
				   "AND", 
				   ["formulanumeric: {custrecord_frf_details_availabledate} - {custrecord_frf_details_start_date}","greaterthan","0"], 
				   "AND", 
				   ["custrecord_frf_details_start_date","onorafter",d_today], 
				   "AND", 
				   ["custrecord_frf_details_status_flag","anyof","1"], 
				   "AND", 
				   [["custrecord_frf_details_project.type","anyof","2"],"OR",["custrecord_frf_details_opp_id","noneof","@NONE@"]]],
				   "OR",
				   [[["custrecord_frf_details_opp_id.custrecord_sfdc_opp_pm","anyof",i_user],
				   "OR",
				   ["custrecord_frf_details_opp_id.custrecord_sfdc_opportunity_dm","anyof",i_user],
				    "OR",
				   ["custrecord_frf_details_opp_id.custrecord_sfdc_client_partner","anyof",i_user]],
				   "AND",
				   ["isinactive","is","F"], 
				   "AND", 
				   ["custrecord_frf_details_availabledate","after",d_today], 
				   "AND", 
				   ["custrecord_frf_details_availabledate","isnotempty",""], 
				   "AND", 
				   ["custrecord_frf_details_status","is","F"], 
				   "AND", 
				   ["custrecord_frf_details_external_hire","is","T"], 
				   "AND", 
				   ["custrecord_frf_details_opp_id.custrecord_stage_sfdc","noneof","8","9"], 
				   "AND", 
				   ["formulanumeric: {custrecord_frf_details_availabledate} - {custrecord_frf_details_start_date}","greaterthan","0"], 
				   "AND", 
				   ["custrecord_frf_details_start_date","onorafter",d_today], 
				   "AND", 
				   ["custrecord_frf_details_status_flag","anyof","1"], 
				   "AND", 
				   [["custrecord_frf_details_project.type","anyof","2"],"OR",["custrecord_frf_details_opp_id","noneof","@NONE@"]]]
				   ]*/
		   filters.push( [[["custrecord_frf_details_project.custentity_projectmanager","anyof",i_user],
			   "OR",
			   ["custrecord_frf_details_project.custentity_deliverymanager","anyof",i_user],
			    "OR",
			   ["custrecord_frf_details_project.custentity_clientpartner","anyof",i_user]],
			   "AND",
			   ["isinactive","is","F"], 
			   "AND", 
			   ["custrecord_frf_details_availabledate","after",d_today], 
			   "AND", 
			   ["custrecord_frf_details_availabledate","isnotempty",""], 
			   "AND", 
			   ["custrecord_frf_details_status","is","F"], 
			   "AND", 
			   ["custrecord_frf_details_external_hire","is","T"], 
			   "AND", 
			   ["custrecord_frf_details_opp_id.custrecord_stage_sfdc","noneof","8","9"], 
			   "AND", 
			   ["formulanumeric: {custrecord_frf_details_availabledate} - {custrecord_frf_details_start_date}","greaterthan","0"], 
			   "AND", 
			   ["custrecord_frf_details_start_date","onorafter",d_today], 
			   "AND", 
			   ["custrecord_frf_details_status_flag","anyof","1"], 
			   "AND", 
			   [["custrecord_frf_details_project.type","anyof","2"],"OR",["custrecord_frf_details_opp_id","noneof","@NONE@"]]]);
		   filters.push('OR');
		   filters.push([[["custrecord_frf_details_opp_id.custrecord_sfdc_opp_pm","anyof",i_user],
			   "OR",
			   ["custrecord_frf_details_opp_id.custrecord_sfdc_opportunity_dm","anyof",i_user],
			    "OR",
			   ["custrecord_frf_details_opp_id.custrecord_sfdc_client_partner","anyof",i_user]],
			   "AND",
			   ["isinactive","is","F"], 
			   "AND", 
			   ["custrecord_frf_details_availabledate","after",d_today], 
			   "AND", 
			   ["custrecord_frf_details_availabledate","isnotempty",""], 
			   "AND", 
			   ["custrecord_frf_details_status","is","F"], 
			   "AND", 
			   ["custrecord_frf_details_external_hire","is","T"], 
			   "AND", 
			   ["custrecord_frf_details_opp_id.custrecord_stage_sfdc","noneof","8","9"], 
			   "AND", 
			   ["formulanumeric: {custrecord_frf_details_availabledate} - {custrecord_frf_details_start_date}","greaterthan","0"], 
			   "AND", 
			   ["custrecord_frf_details_start_date","onorafter",d_today], 
			   "AND", 
			   ["custrecord_frf_details_status_flag","anyof","1"], 
			   "AND", 
			   [["custrecord_frf_details_project.type","anyof","2"],"OR",["custrecord_frf_details_opp_id","noneof","@NONE@"]]]);
		  	   if(isArrayNotEmpty(s_filters.region))
				{
					filters.push('AND');
					filters.push(["custrecord_frf_details_account.custentity_region","anyof",s_filters.region]);
				}
				if(isArrayNotEmpty(s_filters.practice))
				{
					filters.push('AND');
					filters.push(["custrecord_frf_details_res_practice","anyof",s_filters.practice]);
				}
				if(isArrayNotEmpty(s_filters.account))
				{
					filters.push('AND');
					filters.push(["custrecord_frf_details_account","anyof",s_filters.account]);
				}
				if(isArrayNotEmpty(s_filters.project))
				{
					filters.push('AND');
					filters.push(["custrecord_frf_details_project","anyof",s_filters.project],"OR",["custrecord_frf_details_opp_id","anyof",s_filters.project]);
				}

		}
		columns = [
		  new nlobjSearchColumn("internalid",null,"COUNT")
		]
		var customrecord_frf_detailsSearch = searchRecord("customrecord_frf_details", null, filters, columns,null)	
		if(customrecord_frf_detailsSearch) 
		{
			var dataArray = new Array();
			/*for(var i = 0; i<customrecord_frf_detailsSearch.length; i++)
			{
				var data = {};
				data.count = {"name":customrecord_frf_detailsSearch[i].getValue("internalid",null,"COUNT")}
				dataArray.push(data);
			}*/
			
			return customrecord_frf_detailsSearch[0].getValue("internalid",null,"COUNT");
		}
		else
		{
			var dataArray = [];
			return dataArray;
		}
	}
	catch(error)
	{
		return error;
	}
}
//====================================================================================================
function InternalRiskCount(i_user, practiceId, isAdmin,i_region,s_filters)
{
	try
	{
		var filters = new Array();
		var columns = new Array()
		var d_today_temp = nlapiAddDays(d_today, 20)
		s_filters = JSON.parse(s_filters);
		if(isAdmin == true)
		{
			filters.push(["isinactive","is","F"], 
				   "AND", 
				   ["custrecord_frf_details_status","is","F"], 
				   "AND", 
				   ["custrecord_frf_details_status_flag","anyof","1"], 
				   "AND", 
				   ["custrecord_frf_details_start_date","within",d_today,d_today_temp], 
				   "AND", 
				   ["custrecord_frf_details_opp_id.custrecord_stage_sfdc","noneof","8","9"], 
				   "AND", 
				   [["custrecord_frf_details_project.type","anyof","2"],"OR",["custrecord_frf_details_opp_id","noneof","@NONE@"]], 
				   "AND", 
				   ["custrecord_frf_details_external_hire","is","F"]);
			       if(isArrayNotEmpty(s_filters.region))
					{
						filters.push('AND');
						filters.push(["custrecord_frf_details_account.custentity_region","anyof",s_filters.region]);
					}
					if(isArrayNotEmpty(s_filters.practice))
					{
						filters.push('AND');
						filters.push(["custrecord_frf_details_res_practice","anyof",s_filters.practice]);
					}
					if(isArrayNotEmpty(s_filters.account))
					{
						filters.push('AND');
						filters.push(["custrecord_frf_details_account","anyof",s_filters.account]);
					}
					if(isArrayNotEmpty(s_filters.project))
					{
						filters.push('AND');
						filters.push(["custrecord_frf_details_project","anyof",s_filters.project],"OR",["custrecord_frf_details_opp_id","anyof",s_filters.project]);
					}
		}
		else if(practiceId)
		{
			var practiceArray = getSubPractices(practiceId);
			
			filters.push( ["custrecord_frf_details_res_practice","anyof",practiceArray],
				   "AND",
				   ["isinactive","is","F"], 
				   "AND", 
				   ["custrecord_frf_details_status","is","F"], 
				   "AND", 
				   ["custrecord_frf_details_status_flag","anyof","1"], 
				   "AND", 
				    ["custrecord_frf_details_start_date","within",d_today,d_today_temp], 
				   "AND", 
				   ["custrecord_frf_details_opp_id.custrecord_stage_sfdc","noneof","8","9"], 
				   "AND", 
				   [["custrecord_frf_details_project.type","anyof","2"],"OR",["custrecord_frf_details_opp_id","noneof","@NONE@"]], 
				   "AND", 
				   ["custrecord_frf_details_external_hire","is","F"]);
					if(isArrayNotEmpty(s_filters.region))
					{
						filters.push('AND');
						filters.push(["custrecord_frf_details_account.custentity_region","anyof",s_filters.region]);
					}
					if(isArrayNotEmpty(s_filters.practice))
					{
						filters.push('AND');
						filters.push(["custrecord_frf_details_res_practice","anyof",s_filters.practice]);
					}
					if(isArrayNotEmpty(s_filters.account))
					{
						filters.push('AND');
						filters.push(["custrecord_frf_details_account","anyof",s_filters.account]);
					}
					if(isArrayNotEmpty(s_filters.project))
					{
						filters.push('AND');
						filters.push(["custrecord_frf_details_project","anyof",s_filters.project],"OR",["custrecord_frf_details_opp_id","anyof",s_filters.project]);
					}
				
		}
		else if(i_region)
		{
			 
		filters.push(["custrecord_frf_details_account.custentity_region","anyof",i_region],
                "AND",
               ["isinactive","is","F"], 
			   "AND", 
			   ["custrecord_frf_details_status","is","F"], 
			   "AND", 
			   ["custrecord_frf_details_status_flag","anyof","1"], 
			   "AND", 
			    ["custrecord_frf_details_start_date","within",d_today,d_today_temp], 
			   "AND", 
			   ["custrecord_frf_details_opp_id.custrecord_stage_sfdc","noneof","8","9"], 
			   "AND", 
			   [["custrecord_frf_details_project.type","anyof","2"],"OR",["custrecord_frf_details_opp_id","noneof","@NONE@"]], 
			   "AND", 
			   ["custrecord_frf_details_external_hire","is","F"]);
				if(isArrayNotEmpty(s_filters.region))
				{
					filters.push('AND');
					filters.push(["custrecord_frf_details_account.custentity_region","anyof",s_filters.region]);
				}
				if(isArrayNotEmpty(s_filters.practice))
				{
					filters.push('AND');
					filters.push(["custrecord_frf_details_res_practice","anyof",s_filters.practice]);
				}
				if(isArrayNotEmpty(s_filters.account))
				{
					filters.push('AND');
					filters.push(["custrecord_frf_details_account","anyof",s_filters.account]);
				}
				if(isArrayNotEmpty(s_filters.project))
				{
					filters.push('AND');
					filters.push(["custrecord_frf_details_project","anyof",s_filters.project],"OR",["custrecord_frf_details_opp_id","anyof",s_filters.project]);
				}
		}
		else if(i_user)
		{
			
		   filters.push( [[["custrecord_frf_details_project.custentity_projectmanager","anyof",i_user],
				   "OR",
				   ["custrecord_frf_details_project.custentity_deliverymanager","anyof",i_user],
				    "OR",
				   ["custrecord_frf_details_project.custentity_clientpartner","anyof",i_user]],
				   "AND",
				   ["isinactive","is","F"], 
				   "AND", 
				   ["custrecord_frf_details_status","is","F"], 
				   "AND", 
				   ["custrecord_frf_details_status_flag","anyof","1"], 
				   "AND", 
				    ["custrecord_frf_details_start_date","within",d_today,d_today_temp], 
				   "AND", 
				   ["custrecord_frf_details_opp_id.custrecord_stage_sfdc","noneof","8","9"], 
				   "AND", 
				   [["custrecord_frf_details_project.type","anyof","2"],"OR",["custrecord_frf_details_opp_id","noneof","@NONE@"]], 
				   "AND", 
				   ["custrecord_frf_details_external_hire","is","F"]]);
		   filters.push('OR');
		   filters.push([["custrecord_frf_details_opp_id.custrecord_sfdc_opp_pm","anyof",i_user],
				   "OR",
				   ["custrecord_frf_details_opp_id.custrecord_sfdc_opportunity_dm","anyof",i_user],
				    "OR",
				   ["custrecord_frf_details_opp_id.custrecord_sfdc_client_partner","anyof",i_user]],
				   "AND",
				   ["isinactive","is","F"], 
				   "AND", 
				   ["custrecord_frf_details_status","is","F"], 
				   "AND", 
				   ["custrecord_frf_details_status_flag","anyof","1"], 
				   "AND", 
				    ["custrecord_frf_details_start_date","within",d_today,d_today_temp], 
				   "AND", 
				   ["custrecord_frf_details_opp_id.custrecord_stage_sfdc","noneof","8","9"], 
				   "AND", 
				   [["custrecord_frf_details_project.type","anyof","2"],"OR",["custrecord_frf_details_opp_id","noneof","@NONE@"]], 
				   "AND", 
				   ["custrecord_frf_details_external_hire","is","F"]);
		  	    if(isArrayNotEmpty(s_filters.region))
				{
					filters.push('AND');
					filters.push(["custrecord_frf_details_account.custentity_region","anyof",s_filters.region]);
				}
				if(isArrayNotEmpty(s_filters.practice))
				{
					filters.push('AND');
					filters.push(["custrecord_frf_details_res_practice","anyof",s_filters.practice]);
				}
				if(isArrayNotEmpty(s_filters.account))
				{
					filters.push('AND');
					filters.push(["custrecord_frf_details_account","anyof",s_filters.account]);
				}
				if(isArrayNotEmpty(s_filters.project))
				{
					filters.push('AND');
					filters.push(["custrecord_frf_details_project","anyof",s_filters.project],"OR",["custrecord_frf_details_opp_id","anyof",s_filters.project]);
				}
		}
		//nlapiLogExecution('audit','filters &&&&99 ',filters);
		columns = [
		  new nlobjSearchColumn("internalid",null,"COUNT")
		]
		var customrecord_frf_detailsSearch = searchRecord("customrecord_frf_details", null, filters, columns,null)	
		if(customrecord_frf_detailsSearch) 
		{
			var dataArray = new Array();
			return customrecord_frf_detailsSearch[0].getValue("internalid",null,"COUNT");
		}
		else
		{
			var dataArray = [];
			return dataArray;
		}
	}
	catch(error)
	{
		return error;
	}
}

//===================================================================================================================
function OntimePercent(i_user, practiceId, isAdmin,i_region,s_filters)
{
	try
	{
		var totalOnTime = 0;
		var totalSoftlocked = 0;
		var i_openFRFCount = 0;
		var filters = new Array();
		var columns = new Array();
		//var d_today = new Date();
		var preDate = nlapiAddMonths(d_today,-3);
		var i_month = preDate.getMonth();
		var startOfMonth = getFirstDate(i_month);
		//nlapiLogExecution('audit','startOfMonth '+startOfMonth,'preDate '+preDate);
		//nlapiLogExecution('audit','d_today ',d_today);
		s_filters = JSON.parse(s_filters);
		if(isAdmin == true)
		{
			
	   		filters.push(["isinactive","is","F"], 
			   "AND", 
			   ["custrecord_frf_details_status","is","F"], 
			   "AND", 
			   ["custrecord_frf_details_start_date","within",startOfMonth,nlapiDateToString(d_today)], 
			   "AND", 
			   [["custrecord_frf_details_project.type","anyof","2"],
			   "OR",
			   ["custrecord_frf_details_opp_id","noneof","@NONE@"]]); 
			   if(isArrayNotEmpty(s_filters.region))
				{
					filters.push('AND');
					filters.push(["custrecord_frf_details_account.custentity_region","anyof",s_filters.region]);
				}
				if(isArrayNotEmpty(s_filters.practice))
				{
					filters.push('AND');
					filters.push(["custrecord_frf_details_res_practice","anyof",s_filters.practice]);
				}
				if(isArrayNotEmpty(s_filters.account))
				{
					filters.push('AND');
					filters.push(["custrecord_frf_details_account","anyof",s_filters.account]);
				}
				if(isArrayNotEmpty(s_filters.project))
				{
					filters.push('AND');
					filters.push(["custrecord_frf_details_project","anyof",s_filters.project],"OR",["custrecord_frf_details_opp_id","anyof",s_filters.project]);
				}
		}
		else if(practiceId)
		{
			var practiceArray = getSubPractices(practiceId);
		 	filters.push(["custrecord_frf_details_res_practice","anyof",practiceArray],
				   "AND",
				   ["isinactive","is","F"], 
				   "AND", 
				   ["custrecord_frf_details_status","is","F"], 
				   "AND", 
				   ["custrecord_frf_details_start_date","within",startOfMonth,nlapiDateToString(d_today)], 
				   "AND", 
				   [["custrecord_frf_details_project.type","anyof","2"],
				   "OR",
				   ["custrecord_frf_details_opp_id","noneof","@NONE@"]]);
		   		if(isArrayNotEmpty(s_filters.region))
				{
					filters.push('AND');
					filters.push(["custrecord_frf_details_account.custentity_region","anyof",s_filters.region]);
				}
				if(isArrayNotEmpty(s_filters.practice))
				{
					filters.push('AND');
					filters.push(["custrecord_frf_details_res_practice","anyof",s_filters.practice]);
				}
				if(isArrayNotEmpty(s_filters.account))
				{
					filters.push('AND');
					filters.push(["custrecord_frf_details_account","anyof",s_filters.account]);
				}
				if(isArrayNotEmpty(s_filters.project))
				{
					filters.push('AND');
					filters.push(["custrecord_frf_details_project","anyof",s_filters.project],"OR",["custrecord_frf_details_opp_id","anyof",s_filters.project]);
				}
				
		}
		else if(i_region)
		{
			 
		   filters.push(["custrecord_frf_details_account.custentity_region","anyof",i_region],
                "AND",
			   ["isinactive","is","F"], 
			   "AND", 
			   ["custrecord_frf_details_status","is","F"], 
			   "AND", 
			   ["custrecord_frf_details_start_date","within",startOfMonth,nlapiDateToString(d_today)], 
			   "AND", 
			   [["custrecord_frf_details_project.type","anyof","2"],
			   "OR",
			   ["custrecord_frf_details_opp_id","noneof","@NONE@"]]);
	  			if(isArrayNotEmpty(s_filters.region))
				{
					filters.push('AND');
					filters.push(["custrecord_frf_details_account.custentity_region","anyof",s_filters.region]);
				}
				if(isArrayNotEmpty(s_filters.practice))
				{
					filters.push('AND');
					filters.push(["custrecord_frf_details_res_practice","anyof",s_filters.practice]);
				}
				if(isArrayNotEmpty(s_filters.account))
				{
					filters.push('AND');
					filters.push(["custrecord_frf_details_account","anyof",s_filters.account]);
				}
				if(isArrayNotEmpty(s_filters.project))
				{
					filters.push('AND');
					filters.push(["custrecord_frf_details_project","anyof",s_filters.project],"OR",["custrecord_frf_details_opp_id","anyof",s_filters.project]);
				}
		}
		else if(i_user)
		{
			
	   		filters.push([[["custrecord_frf_details_project.custentity_projectmanager","anyof",i_user],
				   "OR",
				   ["custrecord_frf_details_project.custentity_deliverymanager","anyof",i_user],
				   "OR",
				   ["custrecord_frf_details_project.custentity_clientpartner","anyof",i_user]],
				    "AND",
			   	   ["isinactive","is","F"], 
			       "AND", 
			       ["custrecord_frf_details_status","is","F"], 
			       "AND", 
			       ["custrecord_frf_details_start_date","within",startOfMonth,nlapiDateToString(d_today)], 
			       "AND", 
			       [["custrecord_frf_details_project.type","anyof","2"],
			       "OR",
			       ["custrecord_frf_details_opp_id","noneof","@NONE@"]]]);
		   filters.push('OR');
		   filters.push([["custrecord_frf_details_opp_id.custrecord_sfdc_opp_pm","anyof",i_user],
				   "OR",
				   ["custrecord_frf_details_opp_id.custrecord_sfdc_opportunity_dm","anyof",i_user],
				    "OR",
				   ["custrecord_frf_details_opp_id.custrecord_sfdc_client_partner","anyof",i_user]],
				    "AND",
				   ["isinactive","is","F"], 
				   "AND", 
				   ["custrecord_frf_details_status","is","F"], 
				   "AND", 
				   ["custrecord_frf_details_start_date","within",startOfMonth,nlapiDateToString(d_today)], 
				   "AND", 
				   [["custrecord_frf_details_project.type","anyof","2"],
				   "OR",
				   ["custrecord_frf_details_opp_id","noneof","@NONE@"]]);
	  			if(isArrayNotEmpty(s_filters.region))
				{
					filters.push('AND');
					filters.push(["custrecord_frf_details_account.custentity_region","anyof",s_filters.region]);
				}
				if(isArrayNotEmpty(s_filters.practice))
				{
					filters.push('AND');
					filters.push(["custrecord_frf_details_res_practice","anyof",s_filters.practice]);
				}
				if(isArrayNotEmpty(s_filters.account))
				{
					filters.push('AND');
					filters.push(["custrecord_frf_details_account","anyof",s_filters.account]);
				}
				if(isArrayNotEmpty(s_filters.project))
				{
					filters.push('AND');
					filters.push(["custrecord_frf_details_project","anyof",s_filters.project],"OR",["custrecord_frf_details_opp_id","anyof",s_filters.project]);
				}
		}
		//nlapiLogExecution('audit','filters User = ',filters);
		columns = [
			   new nlobjSearchColumn("formulatext",null,"GROUP").setFormula("CASE WHEN {custrecord_frf_details_allocated_date}<= {custrecord_frf_details_start_date} THEN 'On Time' ELSE 'Delayed' END"), 
			   new nlobjSearchColumn("custrecord_frf_details_status_flag",null,"GROUP"), 
			   new nlobjSearchColumn("internalid",null,"COUNT")
		]
		
		var customrecord_frf_detailsSearch = searchRecord("customrecord_frf_details", null, filters, columns,null)	
		if(customrecord_frf_detailsSearch) 
		{
			for(var i=0; i<customrecord_frf_detailsSearch.length;i++)
			{
				var a_columns = customrecord_frf_detailsSearch[i].getAllColumns();
				if(customrecord_frf_detailsSearch[i].getValue(a_columns[0])=="Delayed" && customrecord_frf_detailsSearch[i].getText("custrecord_frf_details_status_flag",null,"GROUP")=="Closed")
					var totalDelayed = customrecord_frf_detailsSearch[i].getValue("internalid",null,"COUNT");
				if(customrecord_frf_detailsSearch[i].getValue(a_columns[0])=="On Time" && customrecord_frf_detailsSearch[i].getText("custrecord_frf_details_status_flag",null,"GROUP")=="Closed")
					var totalOnTime = customrecord_frf_detailsSearch[i].getValue("internalid",null,"COUNT");
			}
		}
		//nlapiLogExecution('Audit','totalDelayed '+totalDelayed,'totalOnTime '+totalOnTime);
		if(totalDelayed &&totalOnTime){
			var i_onTimePercent  = parseFloat(totalOnTime)/(parseFloat(totalDelayed) + parseFloat(totalOnTime))*100;
			//nlapiLogExecution('debug','i_onTimePercent ',i_onTimePercent);
			return i_onTimePercent;
		}
		else
		{
			return 0;
		}
		//------------------------------------------------------------------
	}
	catch(error)
	{
		return error;
	}
}
//===================================================================================================================
function InternalFulfillmentPercent(i_user, practiceId, isAdmin,i_region,s_filters)
{
	try
	{
		var totalOnTime = 0;
		var totalSoftlocked = 0;
		var i_openFRFCount = 0;
		var filters = new Array();
		var columns = new Array();
		s_filters = JSON.parse(s_filters);
		if(isAdmin == true)
		{
			filters.push(["isinactive","is","F"], 
					   "AND", 
					   ["custrecord_frf_details_status","is","F"], 
					   "AND", 
					   [[["custrecord_frf_details_project.type","anyof","2"]],"OR",[["custrecord_frf_details_opp_id.custrecord_stage_sfdc","noneof","8","9"],
					   "AND",["custrecord_frf_details_opp_id","noneof","@NONE@"]]]);
				if(isArrayNotEmpty(s_filters.region))
				{
					filters.push('AND');
					filters.push(["custrecord_frf_details_account.custentity_region","anyof",s_filters.region]);
				}
				if(isArrayNotEmpty(s_filters.practice))
				{
					filters.push('AND');
					filters.push(["custrecord_frf_details_res_practice","anyof",s_filters.practice]);
				}
				if(isArrayNotEmpty(s_filters.account))
				{
					filters.push('AND');
					filters.push(["custrecord_frf_details_account","anyof",s_filters.account]);
				}
				if(isArrayNotEmpty(s_filters.project))
				{
					filters.push('AND');
					filters.push(["custrecord_frf_details_project","anyof",s_filters.project],"OR",["custrecord_frf_details_opp_id","anyof",s_filters.project]);
				}

					  
		}
		else if(practiceId)
		{
			var practiceArray = getSubPractices(practiceId);
			filters.push(["custrecord_frf_details_res_practice","anyof",practiceArray],
				   "AND",
				   ["isinactive","is","F"], 
					 "AND", 
				   ["custrecord_frf_details_status","is","F"], 
					 "AND", 
				   [[["custrecord_frf_details_project.type","anyof","2"]],"OR",[["custrecord_frf_details_opp_id.custrecord_stage_sfdc","noneof","8","9"],
					  "AND",
				   ["custrecord_frf_details_opp_id","noneof","@NONE@"]]]);
			if(isArrayNotEmpty(s_filters.region))
				{
					filters.push('AND');
					filters.push(["custrecord_frf_details_account.custentity_region","anyof",s_filters.region]);
				}
				if(isArrayNotEmpty(s_filters.practice))
				{
					filters.push('AND');
					filters.push(["custrecord_frf_details_res_practice","anyof",s_filters.practice]);
				}
				if(isArrayNotEmpty(s_filters.account))
				{
					filters.push('AND');
					filters.push(["custrecord_frf_details_account","anyof",s_filters.account]);
				}
				if(isArrayNotEmpty(s_filters.project))
				{
					filters.push('AND');
					filters.push(["custrecord_frf_details_project","anyof",s_filters.project],"OR",["custrecord_frf_details_opp_id","anyof",s_filters.project]);
				}
				
		}
		else if(i_region)
		{
			 
			filters.push(["custrecord_frf_details_account.custentity_region","anyof",i_region],
                "AND",
                ["isinactive","is","F"], 
					 "AND", 
				   ["custrecord_frf_details_status","is","F"], 
					 "AND", 
				   [[["custrecord_frf_details_project.type","anyof","2"]],"OR",[["custrecord_frf_details_opp_id.custrecord_stage_sfdc","noneof","8","9"],
					  "AND",
				   ["custrecord_frf_details_opp_id","noneof","@NONE@"]]]);
			if(isArrayNotEmpty(s_filters.region))
				{
					filters.push('AND');
					filters.push(["custrecord_frf_details_account.custentity_region","anyof",s_filters.region]);
				}
				if(isArrayNotEmpty(s_filters.practice))
				{
					filters.push('AND');
					filters.push(["custrecord_frf_details_res_practice","anyof",s_filters.practice]);
				}
				if(isArrayNotEmpty(s_filters.account))
				{
					filters.push('AND');
					filters.push(["custrecord_frf_details_account","anyof",s_filters.account]);
				}
				if(isArrayNotEmpty(s_filters.project))
				{
					filters.push('AND');
					filters.push(["custrecord_frf_details_project","anyof",s_filters.project],"OR",["custrecord_frf_details_opp_id","anyof",s_filters.project]);
				}
		}
		else if(i_user)
		{
			
		   filters.push([[["custrecord_frf_details_project.custentity_projectmanager","anyof",i_user],
				   "OR",
				   ["custrecord_frf_details_project.custentity_deliverymanager","anyof",i_user],
				    "OR",
				   ["custrecord_frf_details_project.custentity_clientpartner","anyof",i_user]],
				   "AND",
				   ["isinactive","is","F"],
					"AND",
					["custrecord_frf_details_status","is","F"],
					"AND",
					["custrecord_frf_details_opp_id.custrecord_stage_sfdc","noneof","9","8"]]);
		   filters.push('OR');
		   filters.push([["custrecord_frf_details_opp_id.custrecord_sfdc_opp_pm","anyof",i_user],
				   "OR",
				   ["custrecord_frf_details_opp_id.custrecord_sfdc_opportunity_dm","anyof",i_user],
				    "OR",
				   ["custrecord_frf_details_opp_id.custrecord_sfdc_client_partner","anyof",i_user]],
				   "AND",
				   ["isinactive","is","F"], 
					 "AND", 
				   ["custrecord_frf_details_status","is","F"], 
					 "AND", 
				   [[["custrecord_frf_details_project.type","anyof","2"]],"OR",[["custrecord_frf_details_opp_id.custrecord_stage_sfdc","noneof","8","9"],
					  "AND",
				   ["custrecord_frf_details_opp_id","noneof","@NONE@"]]]);
		   if(isArrayNotEmpty(s_filters.region))
				{
					filters.push('AND');
					filters.push(["custrecord_frf_details_account.custentity_region","anyof",s_filters.region]);
				}
				if(isArrayNotEmpty(s_filters.practice))
				{
					filters.push('AND');
					filters.push(["custrecord_frf_details_res_practice","anyof",s_filters.practice]);
				}
				if(isArrayNotEmpty(s_filters.account))
				{
					filters.push('AND');
					filters.push(["custrecord_frf_details_account","anyof",s_filters.account]);
				}
				if(isArrayNotEmpty(s_filters.project))
				{
					filters.push('AND');
					filters.push(["custrecord_frf_details_project","anyof",s_filters.project],"OR",["custrecord_frf_details_opp_id","anyof",s_filters.project]);
				}
		}
		columns = [
			   new nlobjSearchColumn("formulatext",null,"GROUP").setFormula("CASE WHEN {custrecord_frf_details_allocated_date}< {custrecord_frf_details_start_date} THEN 'On Time' ELSE 'Delayed' END"), 
			   new nlobjSearchColumn("custrecord_frf_details_status_flag",null,"GROUP"), 
			   new nlobjSearchColumn("internalid",null,"COUNT")
		]
		
		var customrecord_frf_detailsSearch = searchRecord("customrecord_frf_details", null, filters, columns,null)	
		if(customrecord_frf_detailsSearch) 
		{
			for(var i=0; i<customrecord_frf_detailsSearch.length;i++)
			{
				if(customrecord_frf_detailsSearch[i].getText("custrecord_frf_details_status_flag",null,"GROUP")=="Softlocked")
					totalSoftlocked = customrecord_frf_detailsSearch[i].getValue("internalid",null,"COUNT");
				if(customrecord_frf_detailsSearch[i].getText("custrecord_frf_details_status_flag",null,"GROUP")=="Open")
					i_openFRFCount = customrecord_frf_detailsSearch[i].getValue("internalid",null,"COUNT");
			}
		}
		var i_internalFulfillmentPercent   = parseInt(totalSoftlocked)/(parseInt(totalSoftlocked) + parseInt(i_openFRFCount))*100;
		return i_internalFulfillmentPercent;
		//------------------------------------------------------------------
	}
	catch(error)
	{
		return error;
	}
}
//===================================================================================================================
function UtilizationPercent(user, practice, isAdmin,region)
{
	var filters = new Array();
	var columns = new Array();
	if(isAdmin==true)
	{
		var practiceArray = getAdminPractice();
		filters =  [["startdate","notonorafter",d_today], 
				   "AND", 
				   ["enddate","notonorbefore",d_today], 
				   "AND", 
				   ["employee.custentity_employee_inactive","is","F"], 
				   "AND", 
				   ["employee.custentity_implementationteam","is","F"],
				   "AND",
				   ["employee.department","anyof",practiceArray]]
	}
	else if(practice)
	{
		var practiceArray = getSubPractices(practice);
		filters = [["startdate","notonorafter",d_today], 
				   "AND", 
				   ["enddate","notonorbefore",d_today], 
				   "AND", 
				   ["employee.custentity_employee_inactive","is","F"], 
				   "AND", 
				   ["employee.custentity_implementationteam","is","F"], 
				   "AND", 
				   ["employee.department","anyof",practiceArray]]
	}
	else if(region)
	{
		var practiceArray = getAdminPractice();
		filters =  [["startdate","notonorafter",d_today], 
				   "AND", 
				   ["enddate","notonorbefore",d_today], 
				   "AND", 
				   ["employee.custentity_employee_inactive","is","F"], 
				   "AND", 
				   ["employee.custentity_implementationteam","is","F"],
				   "AND",
				   ["employee.department","anyof",practiceArray]]
	}
	else if(user)
	{
		var practice = nlapiLookupField('employee',user,'department');
		var parentPractice = nlapiLookupField("department",practice,"custrecord_parent_practice");
		var a_subPractices = getSubPractices(parentPractice);
		var practiceArray = getAdminPractice();
		filters = [["startdate","notonorafter",d_today], 
				   "AND", 
				   ["enddate","notonorbefore",d_today], 
				   "AND", 
				   ["employee.custentity_employee_inactive","is","F"], 
				   "AND", 
				   ["employee.custentity_implementationteam","is","F"], 
				   "AND", 
				   ["employee.department","anyof",practiceArray]]

	}  
	var resourceallocationSearch = nlapiSearchRecord("resourceallocation",null,filters,
	[
	    new nlobjSearchColumn("custeventrbillable",null,"GROUP"), 
		new nlobjSearchColumn("internalid",null,"COUNT")
	]
	);
	if(resourceallocationSearch)
	{
		var i_billable = 0;
		var i_nonBillable = 0;
		for(var i=0;i<resourceallocationSearch.length;i++)
		{
			var data = {};
			var isBillable = resourceallocationSearch[i].getValue("custeventrbillable",null,"GROUP");
			var count = resourceallocationSearch[i].getValue("internalid",null,"COUNT");
			if(isBillable=='F')
			{
				i_nonBillable = count;
			}
			if(isBillable=='T')
			{
				i_billable = count;
			}
		}
		var per_utilisation = parseInt(i_billable)/(parseInt(i_billable)+parseInt(i_nonBillable))*100;
		return per_utilisation;
	}
	else 
	{
		return 0;
	}
}
//===================================================================================================================
function OverdueCount(i_user, practiceId, isAdmin,i_region,s_filters)
{
	try
	{
		var filters = new Array();
		var columns = new Array();
		s_filters = JSON.parse(s_filters);
		if(isAdmin == true)
		{
			/*filters =  [["custrecord_frf_details_opp_id.custrecord_stage_sfdc","noneof","8","9"], 
					   "AND", 
					   ["custrecord_frf_details_status_flag","anyof","1","3"], 
					   "AND", 
					   ["custrecord_frf_details_start_date","before",d_today], 
					   "AND", 
					   ["custrecord_frf_details_status","is","F"], 
					   "AND", 
					   ["isinactive","is","F"], 
					   "AND", 
					   [["custrecord_frf_details_project.type","anyof","2"],"OR",["custrecord_frf_details_opp_id","noneof","@NONE@"]]]*/
		   filters.push(["custrecord_frf_details_opp_id.custrecord_stage_sfdc","noneof","8","9"], 
					   "AND", 
					   ["custrecord_frf_details_status_flag","anyof","1","3"], 
					   "AND", 
					   ["custrecord_frf_details_start_date","before",d_today], 
					   "AND", 
					   ["custrecord_frf_details_status","is","F"], 
					   "AND", 
					   ["isinactive","is","F"], 
					   "AND", 
					   [["custrecord_frf_details_project.type","anyof","2"],"OR",["custrecord_frf_details_opp_id","noneof","@NONE@"]]);
		  
		   if(isArrayNotEmpty(s_filters.region))
				{
					filters.push('AND');
					filters.push(["custrecord_frf_details_account.custentity_region","anyof",s_filters.region]);
				}
				if(isArrayNotEmpty(s_filters.practice))
				{
					filters.push('AND');
					filters.push(["custrecord_frf_details_res_practice","anyof",s_filters.practice]);
				}
				if(isArrayNotEmpty(s_filters.account))
				{
					filters.push('AND');
					filters.push(["custrecord_frf_details_account","anyof",s_filters.account]);
				}
				if(isArrayNotEmpty(s_filters.project))
				{
					filters.push('AND');
					filters.push(["custrecord_frf_details_project","anyof",s_filters.project],"OR",["custrecord_frf_details_opp_id","anyof",s_filters.project]);
				}
		}
		else if(practiceId)
		{
			var practiceArray = getSubPractices(practiceId);
			/*filters = [
				   ["custrecord_frf_details_res_practice","anyof",practiceArray],
				   "AND",
				   ["custrecord_frf_details_opp_id.custrecord_stage_sfdc","noneof","8","9"], 
				   "AND", 
				   ["custrecord_frf_details_status_flag","anyof","1","3"], 
				   "AND", 
				   ["custrecord_frf_details_start_date","before",d_today], 
				   "AND", 
				   ["custrecord_frf_details_status","is","F"], 
				   "AND", 
				   ["isinactive","is","F"], 
				   "AND", 
				   [["custrecord_frf_details_project.type","anyof","2"],"OR",["custrecord_frf_details_opp_id","noneof","@NONE@"]]]*/
		   filters.push(["custrecord_frf_details_res_practice","anyof",practiceArray],
				   "AND",
				   ["custrecord_frf_details_opp_id.custrecord_stage_sfdc","noneof","8","9"], 
				   "AND", 
				   ["custrecord_frf_details_status_flag","anyof","1","3"], 
				   "AND", 
				   ["custrecord_frf_details_start_date","before",d_today], 
				   "AND", 
				   ["custrecord_frf_details_status","is","F"], 
				   "AND", 
				   ["isinactive","is","F"], 
				   "AND", 
				   [["custrecord_frf_details_project.type","anyof","2"],"OR",["custrecord_frf_details_opp_id","noneof","@NONE@"]]);
		   if(isArrayNotEmpty(s_filters.region))
				{
					filters.push('AND');
					filters.push(["custrecord_frf_details_account.custentity_region","anyof",s_filters.region]);
				}
				if(isArrayNotEmpty(s_filters.practice))
				{
					filters.push('AND');
					filters.push(["custrecord_frf_details_res_practice","anyof",s_filters.practice]);
				}
				if(isArrayNotEmpty(s_filters.account))
				{
					filters.push('AND');
					filters.push(["custrecord_frf_details_account","anyof",s_filters.account]);
				}
				if(isArrayNotEmpty(s_filters.project))
				{
					filters.push('AND');
					filters.push(["custrecord_frf_details_project","anyof",s_filters.project],"OR",["custrecord_frf_details_opp_id","anyof",s_filters.project]);
				}
				
		}
		else if(i_region)
		{
			 
			/*filters = [
				["custrecord_frf_details_account.custentity_region","anyof",i_region],
				"AND",
               ["custrecord_frf_details_opp_id.custrecord_stage_sfdc","noneof","8","9"], 
			   "AND", 
			   ["custrecord_frf_details_status_flag","anyof","1","3"], 
			   "AND", 
			   ["custrecord_frf_details_start_date","before",d_today], 
			   "AND", 
			   ["custrecord_frf_details_status","is","F"], 
			   "AND", 
			   ["isinactive","is","F"], 
			   "AND", 
			   [["custrecord_frf_details_project.type","anyof","2"],"OR",["custrecord_frf_details_opp_id","noneof","@NONE@"]]]*/
		filters.push(["custrecord_frf_details_account.custentity_region","anyof",i_region],
				"AND",
               ["custrecord_frf_details_opp_id.custrecord_stage_sfdc","noneof","8","9"], 
			   "AND", 
			   ["custrecord_frf_details_status_flag","anyof","1","3"], 
			   "AND", 
			   ["custrecord_frf_details_start_date","before",d_today], 
			   "AND", 
			   ["custrecord_frf_details_status","is","F"], 
			   "AND", 
			   ["isinactive","is","F"], 
			   "AND", 
			   [["custrecord_frf_details_project.type","anyof","2"],"OR",["custrecord_frf_details_opp_id","noneof","@NONE@"]]);
		if(isArrayNotEmpty(s_filters.region))
				{
					filters.push('AND');
					filters.push(["custrecord_frf_details_account.custentity_region","anyof",s_filters.region]);
				}
				if(isArrayNotEmpty(s_filters.practice))
				{
					filters.push('AND');
					filters.push(["custrecord_frf_details_res_practice","anyof",s_filters.practice]);
				}
				if(isArrayNotEmpty(s_filters.account))
				{
					filters.push('AND');
					filters.push(["custrecord_frf_details_account","anyof",s_filters.account]);
				}
				if(isArrayNotEmpty(s_filters.project))
				{
					filters.push('AND');
					filters.push(["custrecord_frf_details_project","anyof",s_filters.project],"OR",["custrecord_frf_details_opp_id","anyof",s_filters.project]);
				}
		}
		else if(i_user)
		{
			/*filters = [
				   [[["custrecord_frf_details_project.custentity_projectmanager","anyof",i_user],
				   "OR",
				   ["custrecord_frf_details_project.custentity_deliverymanager","anyof",i_user],
				    "OR",
				   ["custrecord_frf_details_project.custentity_clientpartner","anyof",i_user]],
				   "AND",
				   ["custrecord_frf_details_opp_id.custrecord_stage_sfdc","noneof","8","9"], 
				   "AND", 
				   ["custrecord_frf_details_status_flag","anyof","1","3"], 
				   "AND", 
				   ["custrecord_frf_details_start_date","before",d_today], 
				   "AND", 
				   ["custrecord_frf_details_status","is","F"], 
				   "AND", 
				   ["isinactive","is","F"], 
				   "AND", 
				   [["custrecord_frf_details_project.type","anyof","2"],"OR",["custrecord_frf_details_opp_id","noneof","@NONE@"]]],
				   "OR",
				   [[["custrecord_frf_details_opp_id.custrecord_sfdc_opp_pm","anyof",i_user],
				   "OR",
				   ["custrecord_frf_details_opp_id.custrecord_sfdc_opportunity_dm","anyof",i_user],
				    "OR",
				   ["custrecord_frf_details_opp_id.custrecord_sfdc_client_partner","anyof",i_user]],
				   "AND",
				   ["custrecord_frf_details_opp_id.custrecord_stage_sfdc","noneof","8","9"], 
				   "AND", 
				   ["custrecord_frf_details_status_flag","anyof","1","3"], 
				   "AND", 
				   ["custrecord_frf_details_start_date","before",d_today], 
				   "AND", 
				   ["custrecord_frf_details_status","is","F"], 
				   "AND", 
				   ["isinactive","is","F"], 
				   "AND", 
				   [["custrecord_frf_details_project.type","anyof","2"],"OR",["custrecord_frf_details_opp_id","noneof","@NONE@"]]]
				   ]*/
				   filters.push([[["custrecord_frf_details_project.custentity_projectmanager","anyof",i_user],
				   "OR",
				   ["custrecord_frf_details_project.custentity_deliverymanager","anyof",i_user],
				    "OR",
				   ["custrecord_frf_details_project.custentity_clientpartner","anyof",i_user]],
				   "AND",
				   ["custrecord_frf_details_opp_id.custrecord_stage_sfdc","noneof","8","9"], 
				   "AND", 
				   ["custrecord_frf_details_status_flag","anyof","1","3"], 
				   "AND", 
				   ["custrecord_frf_details_start_date","before",d_today], 
				   "AND", 
				   ["custrecord_frf_details_status","is","F"], 
				   "AND", 
				   ["isinactive","is","F"], 
				   "AND", 
				   [["custrecord_frf_details_project.type","anyof","2"],"OR",["custrecord_frf_details_opp_id","noneof","@NONE@"]]]);
		   filters.push('OR');
		   filters.push([[["custrecord_frf_details_opp_id.custrecord_sfdc_opp_pm","anyof",i_user],
				   "OR",
				   ["custrecord_frf_details_opp_id.custrecord_sfdc_opportunity_dm","anyof",i_user],
				    "OR",
				   ["custrecord_frf_details_opp_id.custrecord_sfdc_client_partner","anyof",i_user]],
				   "AND",
				   ["custrecord_frf_details_opp_id.custrecord_stage_sfdc","noneof","8","9"], 
				   "AND", 
				   ["custrecord_frf_details_status_flag","anyof","1","3"], 
				   "AND", 
				   ["custrecord_frf_details_start_date","before",d_today], 
				   "AND", 
				   ["custrecord_frf_details_status","is","F"], 
				   "AND", 
				   ["isinactive","is","F"], 
				   "AND", 
				   [["custrecord_frf_details_project.type","anyof","2"],"OR",["custrecord_frf_details_opp_id","noneof","@NONE@"]]])	   
				   if(isArrayNotEmpty(s_filters.region))
				{
					filters.push('AND');
					filters.push(["custrecord_frf_details_account.custentity_region","anyof",s_filters.region]);
				}
				if(isArrayNotEmpty(s_filters.practice))
				{
					filters.push('AND');
					filters.push(["custrecord_frf_details_res_practice","anyof",s_filters.practice]);
				}
				if(isArrayNotEmpty(s_filters.account))
				{
					filters.push('AND');
					filters.push(["custrecord_frf_details_account","anyof",s_filters.account]);
				}
				if(isArrayNotEmpty(s_filters.project))
				{
					filters.push('AND');
					filters.push(["custrecord_frf_details_project","anyof",s_filters.project],"OR",["custrecord_frf_details_opp_id","anyof",s_filters.project]);
				}
		}
		columns = [
		  new nlobjSearchColumn("internalid",null,"COUNT")
		]
		var customrecord_frf_detailsSearch = searchRecord("customrecord_frf_details", null, filters, columns,null)	
		if(customrecord_frf_detailsSearch) 
		{
			var dataArray = new Array();
			/*for(var i = 0; i<customrecord_frf_detailsSearch.length; i++)
			{
				var data = {};
				data.count = {"name":customrecord_frf_detailsSearch[i].getValue("internalid",null,"COUNT")}
				dataArray.push(data);
			}*/
			return customrecord_frf_detailsSearch[0].getValue("internalid",null,"COUNT");
		}
		else
		{
			var dataArray = [];
			return dataArray;
		}
	}
	catch(error)
	{
		return error;
	}
}
//===================================================================================================================
/*********************************************Function to get Search Results**********************************************/
function searchRecord(recordType, savedSearch, arrFilters, arrColumns,
        filterExpression)
{

	try {
		var search = null;

		// if a saved search is provided, load it and add the filters and
		// columns
		if (isNotEmpty(savedSearch)) {
			search = nlapiLoadSearch(recordType, savedSearch);

			if (isArrayNotEmpty(arrFilters)) {
				search.addFilters(arrFilters);
			}

			if (isArrayNotEmpty(arrColumns)) {
				search.addColumns(arrColumns);
			}

			if (isArrayNotEmpty(filterExpression)) {
				search.setFilterExpression(filterExpression);
			}
		}
		// create a new search
		else {
			search = nlapiCreateSearch(recordType, arrFilters, arrColumns);
		}

		// run search
		var resultSet = search.runSearch();

		// iterate through the search and get all data 1000 at a time
		var searchResultCount = 0;
		var resultSlice = null;
		var searchResult = [];

		do {
			resultSlice = resultSet.getResults(searchResultCount,
			        searchResultCount + 1000);

			if (resultSlice) {

				resultSlice.forEach(function(result) {

					searchResult.push(result);
					searchResultCount++;
				});
			}
		} while (isArrayNotEmpty(resultSlice) && resultSlice.length >= 1000);

		return searchResult;
	} catch (err) {
		//nlapiLogExecution('ERROR', 'searchRecord', err);
		throw err;
	}
}

function isEmpty(value) {

	return value == null || value == "" || typeof (value) == undefined;
}

function isNotEmpty(value) {

	return !isEmpty(value);
}

function isArrayEmpty(argArray) {

	return !isArrayNotEmpty(argArray);
}

function isArrayNotEmpty(argArray) {

	return (isNotEmpty(argArray) && argArray.length > 0);
}

//*******************GET REGION***************************//

function GetRegion(user) 
{
  	try
	{
		var regionSearch = nlapiSearchRecord("customrecord_region",null,
				[
				 ["custrecord_region_head","anyof",user],
				  "AND",
				 ["isinactive","is","F"]
				], 
				[
				  new nlobjSearchColumn("name")
				]
		);
		if(regionSearch)
		{
			var region = regionSearch[0].getId();
			return region;
		}
		else
		{
			return '';
		}
		
	}
	catch(e)
	{
		return e; 
	}
	
}
//function to get the subpractice tagged to parent 
function getSubPractices(i_practice){
	var temp = [];
	var departmentSearch = nlapiSearchRecord("department",null,
[
   ["isinactive","is","F"], 
   "AND", 
   ["custrecord_parent_practice","anyof",i_practice]
], 
[
]
);
if(departmentSearch){
	for(var i = 0 ; i < departmentSearch.length ;i++){
		temp.push(departmentSearch[i].getId());
	}
	return temp;
}else{
	return i_practice
}
}



function getAdminPractice()
{
	var departmentSearch = nlapiSearchRecord("department",null,
	[
	   ["isinactive","is","F"], 
	   "AND", 
	   ["custrecord_is_delivery_practice","is","T"]
	], 
	[
	   new nlobjSearchColumn("internalid")
	]
	);
	if(departmentSearch)
	{
		var practiceArray = new Array();
		for(var i=0; i<departmentSearch.length;i++)
		{
			practiceArray.push(departmentSearch[i].getValue('internalid'));
		}
		//nlapiLogExecution('Debug','practiceArray ',practiceArray);
		return practiceArray;		
	}
}

function _logValidation(value) {
    if (value != null && value != '' && value != undefined && value != 'NaN') {
        return true;
    } else {
        return false;
    }
}


function ProjectsCount(i_user,i_practice,isAdmin,i_region,s_filters)
{
	try
	{
		////nlapiLogExecution('Debug','o_projectData ',i_user);
		//nlapiLogExecution('Debug','i_practice34534 ',i_practice);
		
		var filters = new Array();
		var columns = new Array();
		s_filters = JSON.parse(s_filters);
		if(isAdmin == true)
		{
			 /*filters = [[["custrecord_fulfill_dashboard_opp_id.custrecord_projection_status_sfdc","anyof","3"],
						 "AND",
						 ["isinactive","is","F"],
						 "AND",
						 ["custrecord_fulfill_dashboard_opp_id.custrecord_project_internal_id_sfdc","noneof","@NONE@"]], 
						 "OR", 
						 [["isinactive","is","F"],
						 "AND",
						 ["custrecord_fulfill_dashboard_project","noneof","@NONE@"],
						 "AND",
						 ["custrecord_fulfill_dashboard_opp_id","anyof","@NONE@"],
						 "AND",
						 ["custrecord_fulfill_dashboard_project.status","noneof","1"]]]*/
			 filters.push([["custrecord_fulfill_dashboard_opp_id.custrecord_projection_status_sfdc","anyof","3"],
						 "AND",
						 ["isinactive","is","F"],
						 "AND",
						 ["custrecord_fulfill_dashboard_opp_id.custrecord_project_internal_id_sfdc","noneof","@NONE@"]], 
						 "OR", 
						 [["isinactive","is","F"],
						 "AND",
						 ["custrecord_fulfill_dashboard_project","noneof","@NONE@"],
						 "AND",
						 ["custrecord_fulfill_dashboard_opp_id","anyof","@NONE@"],
						 "AND",
						 ["custrecord_fulfill_dashboard_project.status","noneof","1"]]);
					    if(isArrayNotEmpty(s_filters.region))
						{
							filters.push('AND');
							filters.push(["custrecord_fulfill_dashboard_project.custentity_region","anyof",s_filters.region]);
						}
						if(isArrayNotEmpty(s_filters.practice))
						{
							filters.push('AND');
							filters.push(["custrecord_fulfill_dashboard_project.custentity_practice","anyof",s_filters.practice]);
						}
						if(isArrayNotEmpty(s_filters.account))
						{
							filters.push('AND');
							filters.push(["custrecord_fulfill_dashboard_account","anyof",s_filters.account]);
						}
						if(isArrayNotEmpty(s_filters.project))
						{
							filters.push('AND');
							filters.push(["custrecord_fulfill_dashboard_project","anyof",s_filters.project],"OR",["custrecord_fulfill_dashboard_opp_id","anyof",s_filters.project]);
						}
						
		}
		else if(i_practice)
		{
			var practiceArray = getSubPractices(i_practice);
			/*filters = [
					[
					 ["custrecord_fulfill_dashboard_project.custentity_practice","anyof",practiceArray],
					 "AND",
					 ["custrecord_fulfill_dashboard_opp_id.custrecord_projection_status_sfdc","anyof","3"],
					 "AND",
					 ["isinactive","is","F"],
					 "AND",
					 ["custrecord_fulfill_dashboard_opp_id.custrecord_project_internal_id_sfdc","noneof","@NONE@"]], 
					 "OR", 
					 [["custrecord_fulfill_dashboard_project.custentity_practice","anyof",i_practice],
					 "AND",
					 ["isinactive","is","F"],
					 "AND",
					 ["custrecord_fulfill_dashboard_project","noneof","@NONE@"],
					 "AND",
					 ["custrecord_fulfill_dashboard_opp_id","anyof","@NONE@"],
					 "AND",
					 ["custrecord_fulfill_dashboard_project.status","noneof","1"]]]*/
		 filters.push([
					 ["custrecord_fulfill_dashboard_project.custentity_practice","anyof",practiceArray],
					 "AND",
					 ["custrecord_fulfill_dashboard_opp_id.custrecord_projection_status_sfdc","anyof","3"],
					 "AND",
					 ["isinactive","is","F"],
					 "AND",
					 ["custrecord_fulfill_dashboard_opp_id.custrecord_project_internal_id_sfdc","noneof","@NONE@"]], 
					 "OR", 
					 [["custrecord_fulfill_dashboard_project.custentity_practice","anyof",i_practice],
					 "AND",
					 ["isinactive","is","F"],
					 "AND",
					 ["custrecord_fulfill_dashboard_project","noneof","@NONE@"],
					 "AND",
					 ["custrecord_fulfill_dashboard_opp_id","anyof","@NONE@"],
					 "AND",
					 ["custrecord_fulfill_dashboard_project.status","noneof","1"]]);
		 				 if(isArrayNotEmpty(s_filters.region))
						{
							filters.push('AND');
							filters.push(["custrecord_fulfill_dashboard_project.custentity_region","anyof",s_filters.region]);
						}
						if(isArrayNotEmpty(s_filters.practice))
						{
							filters.push('AND');
							filters.push(["custrecord_fulfill_dashboard_project.custentity_practice","anyof",s_filters.practice]);
						}
						if(isArrayNotEmpty(s_filters.account))
						{
							filters.push('AND');
							filters.push(["custrecord_fulfill_dashboard_account","anyof",s_filters.account]);
						}
						if(isArrayNotEmpty(s_filters.project))
						{
							filters.push('AND');
							filters.push(["custrecord_fulfill_dashboard_project","anyof",s_filters.project],"OR",["custrecord_fulfill_dashboard_opp_id","anyof",s_filters.project]);
						}
			
		}
		else if(i_region)
		{
			//nlapiLogExecution('Debug','Region12345 ',i_region);
            /*filters = [
				["custrecord_fulfill_dashboard_project.custentity_region","anyof",i_region],
				"AND",
                ["isinactive","is","F"],
				 "AND",
				 ["custrecord_fulfill_dashboard_project","noneof","@NONE@"],
				 "AND",
				 ["custrecord_fulfill_dashboard_opp_id","anyof","@NONE@"],
				 "AND",
				 ["custrecord_fulfill_dashboard_project.status","noneof","1"]
				 
				 ]*/
		filters.push(["custrecord_fulfill_dashboard_project.custentity_region","anyof",i_region],
				"AND",
               /* [["custrecord_fulfill_dashboard_opp_id.custrecord_projection_status_sfdc","anyof","3"],
				 "AND",
				 ["isinactive","is","F"],
				 "AND",
				 ["custrecord_fulfill_dashboard_opp_id.custrecord_project_internal_id_sfdc","noneof","@NONE@"]]
				 ,"OR", */
				 ["isinactive","is","F"],
				 "AND",
				 ["custrecord_fulfill_dashboard_project","noneof","@NONE@"],
				 "AND",
				 ["custrecord_fulfill_dashboard_opp_id","anyof","@NONE@"],
				 "AND",
				 ["custrecord_fulfill_dashboard_project.status","noneof","1"]); 
				if(isArrayNotEmpty(s_filters.region))
				{
					filters.push('AND');
					filters.push(["custrecord_fulfill_dashboard_project.custentity_region","anyof",s_filters.region]);
				}
				if(isArrayNotEmpty(s_filters.practice))
				{
					filters.push('AND');
					filters.push(["custrecord_fulfill_dashboard_project.custentity_practice","anyof",s_filters.practice]);
				}
				if(isArrayNotEmpty(s_filters.account))
				{
					filters.push('AND');
					filters.push(["custrecord_fulfill_dashboard_account","anyof",s_filters.account]);
				}
				if(isArrayNotEmpty(s_filters.project))
				{
					filters.push('AND');
					filters.push(["custrecord_fulfill_dashboard_project","anyof",s_filters.project],"OR",["custrecord_fulfill_dashboard_opp_id","anyof",s_filters.project]);
				}	
		}
		else if(i_user)
		{
			/*filters = [[
				["custrecord_fulfill_dashboard_project.custentity_projectmanager","anyof",i_user],
				"OR",
				["custrecord_fulfill_dashboard_project.custentity_deliverymanager","anyof",i_user],
				 "OR",
				["custrecord_fulfill_dashboard_project.custentity_clientpartner","anyof",i_user]],
				"AND",
				[[["custrecord_fulfill_dashboard_opp_id.custrecord_projection_status_sfdc","anyof","3"],
				 "AND",
				 ["isinactive","is","F"],
				 "AND",
				 ["custrecord_fulfill_dashboard_opp_id.custrecord_project_internal_id_sfdc","noneof","@NONE@"]], 
				 "OR", 
				 [["isinactive","is","F"],
				 "AND",
				 ["custrecord_fulfill_dashboard_project","noneof","@NONE@"],
				 "AND",
				 ["custrecord_fulfill_dashboard_opp_id","anyof","@NONE@"],
				 "AND",
				 ["custrecord_fulfill_dashboard_project.status","noneof","1"]]]]*/
		
		/*filters.push(["custrecord_fulfill_dashboard_project.custentity_projectmanager","anyof",i_user],
				"OR",
				["custrecord_fulfill_dashboard_project.custentity_deliverymanager","anyof",i_user],
				 "OR",
				["custrecord_fulfill_dashboard_project.custentity_clientpartner","anyof",i_user]);
		filters.push('AND');
		filters.push([["custrecord_fulfill_dashboard_opp_id.custrecord_projection_status_sfdc","anyof","3"],
				 "AND",
				 ["isinactive","is","F"],
				 "AND",
				 ["custrecord_fulfill_dashboard_opp_id.custrecord_project_internal_id_sfdc","noneof","@NONE@"]], 
				 "OR", 
				 [["isinactive","is","F"],
				 "AND",
				 ["custrecord_fulfill_dashboard_project","noneof","@NONE@"],
				 "AND",
				 ["custrecord_fulfill_dashboard_opp_id","anyof","@NONE@"],
				 "AND",
				 ["custrecord_fulfill_dashboard_project.status","noneof","1"]]);*/
		 
		filters.push([
				["custrecord_fulfill_dashboard_project.custentity_projectmanager","anyof",i_user],
				"OR",
				["custrecord_fulfill_dashboard_project.custentity_deliverymanager","anyof",i_user],
				 "OR",
				["custrecord_fulfill_dashboard_project.custentity_clientpartner","anyof",i_user]],
				"AND",
				[[["custrecord_fulfill_dashboard_opp_id.custrecord_projection_status_sfdc","anyof","3"],
				 "AND",
				 ["isinactive","is","F"],
				 "AND",
				 ["custrecord_fulfill_dashboard_opp_id.custrecord_project_internal_id_sfdc","noneof","@NONE@"]], 
				 "OR", 
				 [["isinactive","is","F"],
				 "AND",
				 ["custrecord_fulfill_dashboard_project","noneof","@NONE@"],
				 "AND",
				 ["custrecord_fulfill_dashboard_opp_id","anyof","@NONE@"],
				 "AND",
				 ["custrecord_fulfill_dashboard_project.status","noneof","1"]]]);	
				     
			    if(isArrayNotEmpty(s_filters.region))
				{
					filters.push('AND');
					filters.push(["custrecord_fulfill_dashboard_project.custentity_region","anyof",s_filters.region]);
				}
				if(isArrayNotEmpty(s_filters.practice))
				{
					filters.push('AND');
					filters.push(["custrecord_fulfill_dashboard_project.custentity_practice","anyof",s_filters.practice]);
				}
				if(isArrayNotEmpty(s_filters.account))
				{
					filters.push('AND');
					filters.push(["custrecord_fulfill_dashboard_account","anyof",s_filters.account]);
				}
				if(isArrayNotEmpty(s_filters.project))
				{
					filters.push('AND');
					filters.push(["custrecord_fulfill_dashboard_project","anyof",s_filters.project],"OR",["custrecord_fulfill_dashboard_opp_id","anyof",s_filters.project]);
				}
		}
		//nlapiLogExecution('Debug','filters GetProjectDetails',filters);
		
		columns = [
		  // new nlobjSearchColumn("custrecord_fulfill_dashboard_status"), 
		   new nlobjSearchColumn("custrecord_project_internal_id_sfdc","CUSTRECORD_FULFILL_DASHBOARD_OPP_ID",null), 
		   new nlobjSearchColumn('custrecord_fulfill_dashboard_opp_id'), 
		   new nlobjSearchColumn("custrecord_projection_status_sfdc","CUSTRECORD_FULFILL_DASHBOARD_OPP_ID",null), 
		 
		]
		var customrecord_fulfillment_dashboard_dataSearch = searchRecord("customrecord_fulfillment_dashboard_data",null, filters,columns,null);
		if(customrecord_fulfillment_dashboard_dataSearch) 
		{
			var dataArray = new Array();
			for(var i = 0; i<customrecord_fulfillment_dashboard_dataSearch.length; i++)
			{
				dataArray.push(customrecord_fulfillment_dashboard_dataSearch[0].getValue("internalid"));
				var i_project = customrecord_fulfillment_dashboard_dataSearch[i].getValue("custrecord_project_internal_id_sfdc","CUSTRECORD_FULFILL_DASHBOARD_OPP_ID",null);
				var i_opportunity = customrecord_fulfillment_dashboard_dataSearch[i].getValue('custrecord_fulfill_dashboard_opp_id');
				var s_projStatus = customrecord_fulfillment_dashboard_dataSearch[i].getText("custrecord_projection_status_sfdc","CUSTRECORD_FULFILL_DASHBOARD_OPP_ID",null);
				if(_logValidation(i_project) && s_projStatus == "Booked" && _logValidation(i_opportunity))
				{
					dataArray.pop();
				}
			}
			//return customrecord_fulfillment_dashboard_dataSearch[0].getValue("internalid",null,"COUNT");
			return dataArray.length;
		}
		else
		{
			var dataArray = [];
			return dataArray;
		}
		}
	catch(error)
	{
		return error;
	}
}
//======================================================================

function MLOpportunitiesCount(i_user,i_practice,isAdmin,i_region,s_filters)
{
	try
	{
		var filters = new Array();
		var columns = new Array();
		s_filters = JSON.parse(s_filters);
		if(isAdmin == true)
		{
			 /*filters =  [[["isinactive","is","F"],
						"AND", 
						["custrecord_fulfill_dashboard_opp_id.custrecord_stage_sfdc","noneof","9","8"],
						"AND", 
						["custrecord_fulfill_dashboard_opp_id.custrecord_projection_status_sfdc","anyof","2"],
						"AND", 
						["custrecord_fulfill_dashboard_opp_id.custrecord_opp_type_sfdc","is","New Business"]],
						"OR",
						[["custrecord_fulfill_dashboard_opp_id.custrecord_projection_status_sfdc","anyof","3"],
						"AND",
						["custrecord_fulfill_dashboard_project","anyof","@NONE@"],
						"AND",
						["isinactive","is","F"],
						"AND", 
						["custrecord_fulfill_dashboard_opp_id.custrecord_stage_sfdc","noneof","9","8"],
						"AND", 
						["custrecord_fulfill_dashboard_opp_id.custrecord_opp_type_sfdc","is","New Business"]]] */
				filters.push([["isinactive","is","F"],
						"AND", 
						["custrecord_fulfill_dashboard_opp_id.custrecord_stage_sfdc","noneof","9","8"],
						"AND", 
						["custrecord_fulfill_dashboard_opp_id.custrecord_projection_status_sfdc","anyof","2"],
						"AND", 
						["custrecord_fulfill_dashboard_opp_id.custrecord_opp_type_sfdc","is","New Business"]],
						"OR",
						[["custrecord_fulfill_dashboard_opp_id.custrecord_projection_status_sfdc","anyof","3"],
						"AND",
						["custrecord_fulfill_dashboard_project","anyof","@NONE@"],
						"AND",
						["isinactive","is","F"],
						"AND", 
						["custrecord_fulfill_dashboard_opp_id.custrecord_stage_sfdc","noneof","9","8"],
						"AND", 
						["custrecord_fulfill_dashboard_opp_id.custrecord_opp_type_sfdc","is","New Business"]]);
				if(isArrayNotEmpty(s_filters.region))
						{
							filters.push('AND');
							filters.push(["custrecord_fulfill_dashboard_project.custentity_region","anyof",s_filters.region]);
						}
						if(isArrayNotEmpty(s_filters.practice))
						{
							filters.push('AND');
							filters.push(["custrecord_fulfill_dashboard_project.custentity_practice","anyof",s_filters.practice]);
						}
						if(isArrayNotEmpty(s_filters.account))
						{
							filters.push('AND');
							filters.push(["custrecord_fulfill_dashboard_account","anyof",s_filters.account]);
						}
						if(isArrayNotEmpty(s_filters.project))
						{
							filters.push('AND');
							filters.push(["custrecord_fulfill_dashboard_project","anyof",s_filters.project],"OR",["custrecord_fulfill_dashboard_opp_id","anyof",s_filters.project]);
						}
		
		}
		else if(i_practice)
		{
			var practiceArray = getSubPractices(i_practice);
			/*filters = [
					["custrecord_fulfill_dashboard_opp_id.custrecord_practice_internal_id_sfdc","anyof",practiceArray],
					"AND",
					[["isinactive","is","F"],
					"AND", 
					["custrecord_fulfill_dashboard_opp_id.custrecord_stage_sfdc","noneof","9","8"],
					"AND", 
					["custrecord_fulfill_dashboard_opp_id.custrecord_projection_status_sfdc","anyof","2"],
					"AND", 
					["custrecord_fulfill_dashboard_opp_id.custrecord_opp_type_sfdc","is","New Business"]],
					"OR",
					[["custrecord_fulfill_dashboard_opp_id.custrecord_projection_status_sfdc","anyof","3"],
					"AND",
					["custrecord_fulfill_dashboard_project","anyof","@NONE@"],
					"AND",
					["isinactive","is","F"],
					"AND", 
					["custrecord_fulfill_dashboard_opp_id.custrecord_stage_sfdc","noneof","9","8"],
					"AND", 
					["custrecord_fulfill_dashboard_opp_id.custrecord_opp_type_sfdc","is","New Business"]]]*/
			filters.push(["custrecord_fulfill_dashboard_opp_id.custrecord_practice_internal_id_sfdc","anyof",practiceArray],
					"AND",
					[["isinactive","is","F"],
					"AND", 
					["custrecord_fulfill_dashboard_opp_id.custrecord_stage_sfdc","noneof","9","8"],
					"AND", 
					["custrecord_fulfill_dashboard_opp_id.custrecord_projection_status_sfdc","anyof","2"],
					"AND", 
					["custrecord_fulfill_dashboard_opp_id.custrecord_opp_type_sfdc","is","New Business"]],
					"OR",
					[["custrecord_fulfill_dashboard_opp_id.custrecord_projection_status_sfdc","anyof","3"],
					"AND",
					["custrecord_fulfill_dashboard_project","anyof","@NONE@"],
					"AND",
					["isinactive","is","F"],
					"AND", 
					["custrecord_fulfill_dashboard_opp_id.custrecord_stage_sfdc","noneof","9","8"],
					"AND", 
					["custrecord_fulfill_dashboard_opp_id.custrecord_opp_type_sfdc","is","New Business"]]);
			if(isArrayNotEmpty(s_filters.region))
						{
							filters.push('AND');
							filters.push(["custrecord_fulfill_dashboard_project.custentity_region","anyof",s_filters.region]);
						}
						if(isArrayNotEmpty(s_filters.practice))
						{
							filters.push('AND');
							filters.push(["custrecord_fulfill_dashboard_project.custentity_practice","anyof",s_filters.practice]);
						}
						if(isArrayNotEmpty(s_filters.account))
						{
							filters.push('AND');
							filters.push(["custrecord_fulfill_dashboard_account","anyof",s_filters.account]);
						}
						if(isArrayNotEmpty(s_filters.project))
						{
							filters.push('AND');
							filters.push(["custrecord_fulfill_dashboard_project","anyof",s_filters.project],"OR",["custrecord_fulfill_dashboard_opp_id","anyof",s_filters.project]);
						}
			
		}
		else if(i_region)
		{

            /*filters = [
				["custrecord_fulfill_dashboard_opp_id.custrecord_sfdc_opp_account_region","anyof",i_region],
				"AND",
				[["isinactive","is","F"],
				"AND", 
				["custrecord_fulfill_dashboard_opp_id.custrecord_stage_sfdc","noneof","9","8"],
				"AND", 
				["custrecord_fulfill_dashboard_opp_id.custrecord_projection_status_sfdc","anyof","2"],
				"AND", 
				["custrecord_fulfill_dashboard_opp_id.custrecord_opp_type_sfdc","is","New Business"]],
				"OR",
				[["custrecord_fulfill_dashboard_opp_id.custrecord_projection_status_sfdc","anyof","3"],
				"AND",
				["custrecord_fulfill_dashboard_project","anyof","@NONE@"],
				"AND",
				["isinactive","is","F"],
				"AND", 
				["custrecord_fulfill_dashboard_opp_id.custrecord_stage_sfdc","noneof","9","8"],
				"AND", 
				["custrecord_fulfill_dashboard_opp_id.custrecord_opp_type_sfdc","is","New Business"]]]*/
		filters.push(["custrecord_fulfill_dashboard_opp_id.custrecord_sfdc_opp_account_region","anyof",i_region],
				"AND",
				[["isinactive","is","F"],
				"AND", 
				["custrecord_fulfill_dashboard_opp_id.custrecord_stage_sfdc","noneof","9","8"],
				"AND", 
				["custrecord_fulfill_dashboard_opp_id.custrecord_projection_status_sfdc","anyof","2"],
				"AND", 
				["custrecord_fulfill_dashboard_opp_id.custrecord_opp_type_sfdc","is","New Business"]],
				"OR",
				[["custrecord_fulfill_dashboard_opp_id.custrecord_projection_status_sfdc","anyof","3"],
				"AND",
				["custrecord_fulfill_dashboard_project","anyof","@NONE@"],
				"AND",
				["isinactive","is","F"],
				"AND", 
				["custrecord_fulfill_dashboard_opp_id.custrecord_stage_sfdc","noneof","9","8"],
				"AND", 
				["custrecord_fulfill_dashboard_opp_id.custrecord_opp_type_sfdc","is","New Business"]]);
		if(isArrayNotEmpty(s_filters.region))
						{
							filters.push('AND');
							filters.push(["custrecord_fulfill_dashboard_project.custentity_region","anyof",s_filters.region]);
						}
						if(isArrayNotEmpty(s_filters.practice))
						{
							filters.push('AND');
							filters.push(["custrecord_fulfill_dashboard_project.custentity_practice","anyof",s_filters.practice]);
						}
						if(isArrayNotEmpty(s_filters.account))
						{
							filters.push('AND');
							filters.push(["custrecord_fulfill_dashboard_account","anyof",s_filters.account]);
						}
						if(isArrayNotEmpty(s_filters.project))
						{
							filters.push('AND');
							filters.push(["custrecord_fulfill_dashboard_project","anyof",s_filters.project],"OR",["custrecord_fulfill_dashboard_opp_id","anyof",s_filters.project]);
						}
		}
		else if(i_user)
		{
			/*filters = [[
				["custrecord_fulfill_dashboard_opp_id.custrecord_sfdc_opp_pm","anyof",i_user],
				"OR",
				["custrecord_fulfill_dashboard_opp_id.custrecord_sfdc_opportunity_dm","anyof",i_user],
				 "OR",
				["custrecord_fulfill_dashboard_opp_id.custrecord_sfdc_client_partner","anyof",i_user]],
				 "AND",
				[["isinactive","is","F"],
				"AND", 
				["custrecord_fulfill_dashboard_opp_id.custrecord_stage_sfdc","noneof","9","8"],
				"AND", 
				["custrecord_fulfill_dashboard_opp_id.custrecord_projection_status_sfdc","anyof","2"],
				"AND", 
				["custrecord_fulfill_dashboard_opp_id.custrecord_opp_type_sfdc","is","New Business"]],
				"OR",
				[["custrecord_fulfill_dashboard_opp_id.custrecord_projection_status_sfdc","anyof","3"],
				"AND",
				["custrecord_fulfill_dashboard_project","anyof","@NONE@"],
				"AND",
				["isinactive","is","F"],
				"AND", 
				["custrecord_fulfill_dashboard_opp_id.custrecord_stage_sfdc","noneof","9","8"],
				"AND", 
				["custrecord_fulfill_dashboard_opp_id.custrecord_opp_type_sfdc","is","New Business"]]]*/

		filters.push([
				["custrecord_fulfill_dashboard_opp_id.custrecord_sfdc_opp_pm","anyof",i_user],
				"OR",
				["custrecord_fulfill_dashboard_opp_id.custrecord_sfdc_opportunity_dm","anyof",i_user],
				 "OR",
				["custrecord_fulfill_dashboard_opp_id.custrecord_sfdc_client_partner","anyof",i_user]],
				 "AND",
				[["isinactive","is","F"],
				"AND", 
				["custrecord_fulfill_dashboard_opp_id.custrecord_stage_sfdc","noneof","9","8"],
				"AND", 
				["custrecord_fulfill_dashboard_opp_id.custrecord_projection_status_sfdc","anyof","2"],
				"AND", 
				["custrecord_fulfill_dashboard_opp_id.custrecord_opp_type_sfdc","is","New Business"]]);
		filters.push('OR');
		filters.push([["custrecord_fulfill_dashboard_opp_id.custrecord_projection_status_sfdc","anyof","3"],
				"AND",
				["custrecord_fulfill_dashboard_project","anyof","@NONE@"],
				"AND",
				["isinactive","is","F"],
				"AND", 
				["custrecord_fulfill_dashboard_opp_id.custrecord_stage_sfdc","noneof","9","8"],
				"AND", 
				["custrecord_fulfill_dashboard_opp_id.custrecord_opp_type_sfdc","is","New Business"]]);
		                if(isArrayNotEmpty(s_filters.region))
						{
							filters.push('AND');
							filters.push(["custrecord_fulfill_dashboard_project.custentity_region","anyof",s_filters.region]);
						}
						if(isArrayNotEmpty(s_filters.practice))
						{
							filters.push('AND');
							filters.push(["custrecord_fulfill_dashboard_project.custentity_practice","anyof",s_filters.practice]);
						}
						if(isArrayNotEmpty(s_filters.account))
						{
							filters.push('AND');
							filters.push(["custrecord_fulfill_dashboard_account","anyof",s_filters.account]);
						}
						if(isArrayNotEmpty(s_filters.project))
						{
							filters.push('AND');
							filters.push(["custrecord_fulfill_dashboard_project","anyof",s_filters.project],"OR",["custrecord_fulfill_dashboard_opp_id","anyof",s_filters.project]);
						}
		}
		
		columns = [
		  // new nlobjSearchColumn("custrecord_fulfill_dashboard_status"), 
		   new nlobjSearchColumn("internalid",null,"COUNT"), 
		 
		]
		var customrecord_fulfillment_dashboard_dataSearch = searchRecord("customrecord_fulfillment_dashboard_data",null, filters,columns,null);
		if(customrecord_fulfillment_dashboard_dataSearch) 
		{
			var dataArray = new Array();
			return customrecord_fulfillment_dashboard_dataSearch[0].getValue("internalid",null,"COUNT");
		}
		else
		{
			var dataArray = [];
			return dataArray;
		}
		}
	catch(error)
	{
		return error;
	}
}
//======================================================================================================================
function MLOpportunitiesPositionsRequestedCount(i_user,i_practice,isAdmin,i_region,s_filters)
{
	try
	{
		var filters = new Array();
		var columns = new Array();
		s_filters = JSON.parse(s_filters);
		if(isAdmin == true)
		{
			/* filters =  [["custrecord_fulfill_plan_opp_id.custrecord_projection_status_sfdc","anyof","2"], 
						   "AND", 
						   ["custrecord_fulfill_plan_opp_id.custrecord_stage_sfdc","noneof","8","9"], 
						   "AND", 
						   ["isinactive","is","F"], 
						   "AND", 
						   ["custrecord_fulfill_plan_opp_id.custrecord_opp_type_sfdc","is","New Business"],
						    "AND",
						   ["custrecord_fulfill_plan_status","isnot", "Cancelled"]]*/
				   filters.push(["custrecord_fulfill_plan_opp_id.custrecord_projection_status_sfdc","anyof","2"], 
						   "AND", 
						   ["custrecord_fulfill_plan_opp_id.custrecord_stage_sfdc","noneof","8","9"], 
						   "AND", 
						   ["isinactive","is","F"], 
						   "AND", 
						   ["custrecord_fulfill_plan_opp_id.custrecord_opp_type_sfdc","is","New Business"],
						    "AND",
						   ["custrecord_fulfill_plan_status","isnot", "Cancelled"]);
				   if(isArrayNotEmpty(s_filters.region))
						{
							filters.push('AND');
							filters.push(["custrecord_fulfill_dashboard_project.custentity_region","anyof",s_filters.region]);
						}
						if(isArrayNotEmpty(s_filters.practice))
						{
							filters.push('AND');
							filters.push(["custrecord_fulfill_dashboard_project.custentity_practice","anyof",s_filters.practice]);
						}
						if(isArrayNotEmpty(s_filters.account))
						{
							filters.push('AND');
							filters.push(["custrecord_fulfill_dashboard_account","anyof",s_filters.account]);
						}
						if(isArrayNotEmpty(s_filters.project))
						{
							filters.push('AND');
							filters.push(["custrecord_fulfill_dashboard_project","anyof",s_filters.project],"OR",["custrecord_fulfill_dashboard_opp_id","anyof",s_filters.project]);
						}
		
		}
		else if(i_practice)
		{
			var practiceArray = getSubPractices(i_practice);
			/*filters = [
					["custrecord_fulfill_plan_opp_id.custrecord_practice_internal_id_sfdc","anyof",practiceArray],
					"AND",
					["custrecord_fulfill_plan_opp_id.custrecord_projection_status_sfdc","anyof","2"], 
				   "AND", 
				   ["custrecord_fulfill_plan_opp_id.custrecord_stage_sfdc","noneof","8","9"], 
				   "AND", 
				   ["isinactive","is","F"], 
				   "AND", 
				   ["custrecord_fulfill_plan_opp_id.custrecord_opp_type_sfdc","is","New Business"],
					"AND",
				  ["custrecord_fulfill_plan_status","isnot", "Cancelled"]]*/
		  filters.push(["custrecord_fulfill_plan_opp_id.custrecord_practice_internal_id_sfdc","anyof",practiceArray],
					"AND",
					["custrecord_fulfill_plan_opp_id.custrecord_projection_status_sfdc","anyof","2"], 
				   "AND", 
				   ["custrecord_fulfill_plan_opp_id.custrecord_stage_sfdc","noneof","8","9"], 
				   "AND", 
				   ["isinactive","is","F"], 
				   "AND", 
				   ["custrecord_fulfill_plan_opp_id.custrecord_opp_type_sfdc","is","New Business"],
					"AND",
				  ["custrecord_fulfill_plan_status","isnot", "Cancelled"]);
		  if(isArrayNotEmpty(s_filters.region))
						{
							filters.push('AND');
							filters.push(["custrecord_fulfill_dashboard_project.custentity_region","anyof",s_filters.region]);
						}
						if(isArrayNotEmpty(s_filters.practice))
						{
							filters.push('AND');
							filters.push(["custrecord_fulfill_dashboard_project.custentity_practice","anyof",s_filters.practice]);
						}
						if(isArrayNotEmpty(s_filters.account))
						{
							filters.push('AND');
							filters.push(["custrecord_fulfill_dashboard_account","anyof",s_filters.account]);
						}
						if(isArrayNotEmpty(s_filters.project))
						{
							filters.push('AND');
							filters.push(["custrecord_fulfill_dashboard_project","anyof",s_filters.project],"OR",["custrecord_fulfill_dashboard_opp_id","anyof",s_filters.project]);
						}
			
		}
		else if(i_region)
		{
			//var a_customers = customerSearch(i_region);
           /* filters = [
				["custrecord_fulfill_plan_opp_id.custrecord_sfdc_opp_account_region","anyof",i_region],
				"AND",
				["custrecord_fulfill_plan_opp_id.custrecord_projection_status_sfdc","anyof","2"], 
			    "AND", 
			    ["custrecord_fulfill_plan_opp_id.custrecord_stage_sfdc","noneof","8","9"], 
			    "AND", 
			    ["isinactive","is","F"], 
			    "AND", 
			    ["custrecord_fulfill_plan_opp_id.custrecord_opp_type_sfdc","is","New Business"],
				"AND",
			    ["custrecord_fulfill_plan_status","isnot", "Cancelled"]]*/
	    filters.push(["custrecord_fulfill_plan_opp_id.custrecord_sfdc_opp_account_region","anyof",i_region],
				"AND",
				["custrecord_fulfill_plan_opp_id.custrecord_projection_status_sfdc","anyof","2"], 
			    "AND", 
			    ["custrecord_fulfill_plan_opp_id.custrecord_stage_sfdc","noneof","8","9"], 
			    "AND", 
			    ["isinactive","is","F"], 
			    "AND", 
			    ["custrecord_fulfill_plan_opp_id.custrecord_opp_type_sfdc","is","New Business"],
				"AND",
			    ["custrecord_fulfill_plan_status","isnot", "Cancelled"]);
	    if(isArrayNotEmpty(s_filters.region))
						{
							filters.push('AND');
							filters.push(["custrecord_fulfill_dashboard_project.custentity_region","anyof",s_filters.region]);
						}
						if(isArrayNotEmpty(s_filters.practice))
						{
							filters.push('AND');
							filters.push(["custrecord_fulfill_dashboard_project.custentity_practice","anyof",s_filters.practice]);
						}
						if(isArrayNotEmpty(s_filters.account))
						{
							filters.push('AND');
							filters.push(["custrecord_fulfill_dashboard_account","anyof",s_filters.account]);
						}
						if(isArrayNotEmpty(s_filters.project))
						{
							filters.push('AND');
							filters.push(["custrecord_fulfill_dashboard_project","anyof",s_filters.project],"OR",["custrecord_fulfill_dashboard_opp_id","anyof",s_filters.project]);
						}
		}
		else if(i_user)
		{
			/*filters = [[
				["custrecord_fulfill_plan_opp_id.custrecord_sfdc_opp_pm","anyof",i_user],
				"OR",
				["custrecord_fulfill_plan_opp_id.custrecord_sfdc_opportunity_dm","anyof",i_user],
				"OR",
				["custrecord_fulfill_plan_opp_id.custrecord_sfdc_client_partner","anyof",i_user]],
				"AND",
				["custrecord_fulfill_plan_opp_id.custrecord_projection_status_sfdc","anyof","2"], 
			    "AND", 
			    ["custrecord_fulfill_plan_opp_id.custrecord_stage_sfdc","noneof","8","9"], 
			    "AND", 
			    ["isinactive","is","F"], 
			    "AND", 
			    ["custrecord_fulfill_plan_opp_id.custrecord_opp_type_sfdc","is","New Business"],
				"AND",
			    ["custrecord_fulfill_plan_status","isnot", "Cancelled"]]*/
	    filters.push([
				["custrecord_fulfill_plan_opp_id.custrecord_sfdc_opp_pm","anyof",i_user],
				"OR",
				["custrecord_fulfill_plan_opp_id.custrecord_sfdc_opportunity_dm","anyof",i_user],
				"OR",
				["custrecord_fulfill_plan_opp_id.custrecord_sfdc_client_partner","anyof",i_user]],
				"AND",
				["custrecord_fulfill_plan_opp_id.custrecord_projection_status_sfdc","anyof","2"], 
			    "AND", 
			    ["custrecord_fulfill_plan_opp_id.custrecord_stage_sfdc","noneof","8","9"], 
			    "AND", 
			    ["isinactive","is","F"], 
			    "AND", 
			    ["custrecord_fulfill_plan_opp_id.custrecord_opp_type_sfdc","is","New Business"],
				"AND",
			    ["custrecord_fulfill_plan_status","isnot", "Cancelled"]);
	                    if(isArrayNotEmpty(s_filters.region))
						{
							filters.push('AND');
							filters.push(["custrecord_fulfill_dashboard_project.custentity_region","anyof",s_filters.region]);
						}
						if(isArrayNotEmpty(s_filters.practice))
						{
							filters.push('AND');
							filters.push(["custrecord_fulfill_dashboard_project.custentity_practice","anyof",s_filters.practice]);
						}
						if(isArrayNotEmpty(s_filters.account))
						{
							filters.push('AND');
							filters.push(["custrecord_fulfill_dashboard_account","anyof",s_filters.account]);
						}
						if(isArrayNotEmpty(s_filters.project))
						{
							filters.push('AND');
							filters.push(["custrecord_fulfill_dashboard_project","anyof",s_filters.project],"OR",["custrecord_fulfill_dashboard_opp_id","anyof",s_filters.project]);
						}
		}
		
		columns = [
		  // new nlobjSearchColumn("custrecord_fulfill_dashboard_status"), 
			new nlobjSearchColumn("formulanumeric",null,"SUM").setFormula("TO_NUMBER({custrecord_fulfill_plan_position})"),
			new nlobjSearchColumn("internalid",null,"COUNT")
		]
		var customrecord_fulfillment_plan_sfdcSearch  = searchRecord("customrecord_fulfillment_plan_sfdc",null, filters,columns,null);
		if(customrecord_fulfillment_plan_sfdcSearch ) 
		{
			var a_columns = customrecord_fulfillment_plan_sfdcSearch[0].getAllColumns();
			////nlapiLogExecution('debug','a_columns ',customrecord_fulfillment_plan_sfdcSearch[0].getValue(a_columns[0]));
			var dataArray = new Array();
			////nlapiLogExecution('Debug','customrecord_fulfillment_plan_sfdcSearch  Length ',customrecord_fulfillment_plan_sfdcSearch .length);
			//return customrecord_fulfillment_plan_sfdcSearch[0].getValue("internalid",null,"COUNT");
			if(customrecord_fulfillment_plan_sfdcSearch[0].getValue(a_columns[0]))
				return customrecord_fulfillment_plan_sfdcSearch[0].getValue(a_columns[0]);
			else 
				return 0; 
		}
		else
		{
			var dataArray = [];
			return dataArray;
		}
		}
	catch(error)
	{
		return error;
	}
}

//===================================================================================================================
function MLOpportunitiesFrfsCreatedCount(i_user, practiceId, isAdmin,i_region,s_filters)
{
	try
	{
		var filters = new Array();
		var columns = new Array();
		s_filters = JSON.parse(s_filters);
		if(isAdmin == true)
		{
			/*filters =  [  ["custrecord_frf_details_status","is","F"], 
						   "AND", 
						   ["custrecord_frf_details_opp_id.custrecord_projection_status_sfdc","anyof","2"], 
						   "AND", 
						   ["custrecord_frf_details_opp_id.custrecord_stage_sfdc","noneof","8","9"], 
						   "AND", 
						   ["custrecord_frf_details_opp_id.custrecord_opp_type_sfdc","is","New Business"], 
						   "AND", 
						   ["isinactive","is","F"]]*/
				filters.push(["custrecord_frf_details_status","is","F"], 
						   "AND", 
						   ["custrecord_frf_details_opp_id.custrecord_projection_status_sfdc","anyof","2"], 
						   "AND", 
						   ["custrecord_frf_details_opp_id.custrecord_stage_sfdc","noneof","8","9"], 
						   "AND", 
						   ["custrecord_frf_details_opp_id.custrecord_opp_type_sfdc","is","New Business"], 
						   "AND", 
						   ["isinactive","is","F"]);
				if(isArrayNotEmpty(s_filters.region))
				{
					filters.push('AND');
					filters.push(["custrecord_frf_details_account.custentity_region","anyof",s_filters.region]);
				}
				if(isArrayNotEmpty(s_filters.practice))
				{
					filters.push('AND');
					filters.push(["custrecord_frf_details_res_practice","anyof",s_filters.practice]);
				}
				if(isArrayNotEmpty(s_filters.account))
				{
					filters.push('AND');
					filters.push(["custrecord_frf_details_account","anyof",s_filters.account]);
				}
				if(isArrayNotEmpty(s_filters.project))
				{
					filters.push('AND');
					filters.push(["custrecord_frf_details_project","anyof",s_filters.project],"OR",["custrecord_frf_details_opp_id","anyof",s_filters.project]);
				}
		}
		else if(practiceId)
		{
			var practiceArray = getSubPractices(practiceId);
			/*filters = [
				   ["custrecord_frf_details_res_practice","anyof",practiceArray],
				   "AND",
				   ["custrecord_frf_details_status","is","F"], 
				   "AND", 
				   ["custrecord_frf_details_opp_id.custrecord_projection_status_sfdc","anyof","2"], 
				   "AND", 
				   ["custrecord_frf_details_opp_id.custrecord_stage_sfdc","noneof","8","9"], 
				   "AND", 
				   ["custrecord_frf_details_opp_id.custrecord_opp_type_sfdc","is","New Business"], 
				   "AND", 
				   ["isinactive","is","F"]]*/
		   filters.push( ["custrecord_frf_details_res_practice","anyof",practiceArray],
				   "AND",
				   ["custrecord_frf_details_status","is","F"], 
				   "AND", 
				   ["custrecord_frf_details_opp_id.custrecord_projection_status_sfdc","anyof","2"], 
				   "AND", 
				   ["custrecord_frf_details_opp_id.custrecord_stage_sfdc","noneof","8","9"], 
				   "AND", 
				   ["custrecord_frf_details_opp_id.custrecord_opp_type_sfdc","is","New Business"], 
				   "AND", 
				   ["isinactive","is","F"]);
			   if(isArrayNotEmpty(s_filters.region))
				{
					filters.push('AND');
					filters.push(["custrecord_frf_details_account.custentity_region","anyof",s_filters.region]);
				}
				if(isArrayNotEmpty(s_filters.practice))
				{
					filters.push('AND');
					filters.push(["custrecord_frf_details_res_practice","anyof",s_filters.practice]);
				}
				if(isArrayNotEmpty(s_filters.account))
				{
					filters.push('AND');
					filters.push(["custrecord_frf_details_account","anyof",s_filters.account]);
				}
				if(isArrayNotEmpty(s_filters.project))
				{
					filters.push('AND');
					filters.push(["custrecord_frf_details_project","anyof",s_filters.project],"OR",["custrecord_frf_details_opp_id","anyof",s_filters.project]);
				}
				
		}
		else if(i_region)
		{
			 
			/*filters = [
				["custrecord_frf_details_account.custentity_region","anyof",i_region],
				"AND",
                ["custrecord_frf_details_status","is","F"], 
			   "AND", 
			   ["custrecord_frf_details_opp_id.custrecord_projection_status_sfdc","anyof","2"], 
			   "AND", 
			   ["custrecord_frf_details_opp_id.custrecord_stage_sfdc","noneof","8","9"], 
			   "AND", 
			   ["custrecord_frf_details_opp_id.custrecord_opp_type_sfdc","is","New Business"], 
			   "AND", 
			   ["isinactive","is","F"]]*/
	   filters.push(["custrecord_frf_details_account.custentity_region","anyof",i_region],
				"AND",
                ["custrecord_frf_details_status","is","F"], 
			   "AND", 
			   ["custrecord_frf_details_opp_id.custrecord_projection_status_sfdc","anyof","2"], 
			   "AND", 
			   ["custrecord_frf_details_opp_id.custrecord_stage_sfdc","noneof","8","9"], 
			   "AND", 
			   ["custrecord_frf_details_opp_id.custrecord_opp_type_sfdc","is","New Business"], 
			   "AND", 
			   ["isinactive","is","F"]);
	   if(isArrayNotEmpty(s_filters.region))
			{
				filters.push('AND');
				filters.push(["custrecord_frf_details_account.custentity_region","anyof",s_filters.region]);
			}
			if(isArrayNotEmpty(s_filters.practice))
			{
				filters.push('AND');
				filters.push(["custrecord_frf_details_res_practice","anyof",s_filters.practice]);
			}
			if(isArrayNotEmpty(s_filters.account))
			{
				filters.push('AND');
				filters.push(["custrecord_frf_details_account","anyof",s_filters.account]);
			}
			if(isArrayNotEmpty(s_filters.project))
			{
				filters.push('AND');
				filters.push(["custrecord_frf_details_project","anyof",s_filters.project],"OR",["custrecord_frf_details_opp_id","anyof",s_filters.project]);
			}
		}
		else if(i_user)
		{
			/*filters = [
				   [[["custrecord_frf_details_project.custentity_projectmanager","anyof",i_user],
				   "OR",
				   ["custrecord_frf_details_project.custentity_deliverymanager","anyof",i_user],
				    "OR",
				   ["custrecord_frf_details_project.custentity_clientpartner","anyof",i_user]],
				   "AND",
				   ["custrecord_frf_details_status","is","F"], 
				   "AND", 
				   ["custrecord_frf_details_opp_id.custrecord_projection_status_sfdc","anyof","2"], 
				   "AND", 
				   ["custrecord_frf_details_opp_id.custrecord_stage_sfdc","noneof","8","9"], 
				   "AND", 
				   ["custrecord_frf_details_opp_id.custrecord_opp_type_sfdc","is","New Business"], 
				   "AND", 
				   ["isinactive","is","F"]],
				   "OR",
				   [[["custrecord_frf_details_opp_id.custrecord_sfdc_opp_pm","anyof",i_user],
				   "OR",
				   ["custrecord_frf_details_opp_id.custrecord_sfdc_opportunity_dm","anyof",i_user],
				    "OR",
				   ["custrecord_frf_details_opp_id.custrecord_sfdc_client_partner","anyof",i_user]],
				   "AND",
				   ["custrecord_frf_details_status","is","F"], 
				   "AND", 
				   ["custrecord_frf_details_opp_id.custrecord_projection_status_sfdc","anyof","2"], 
				   "AND", 
				   ["custrecord_frf_details_opp_id.custrecord_stage_sfdc","noneof","8","9"], 
				   "AND", 
				   ["custrecord_frf_details_opp_id.custrecord_opp_type_sfdc","is","New Business"], 
				   "AND", 
				   ["isinactive","is","F"]]
				   ]*/

		   filters.push([[["custrecord_frf_details_project.custentity_projectmanager","anyof",i_user],
				   "OR",
				   ["custrecord_frf_details_project.custentity_deliverymanager","anyof",i_user],
				    "OR",
				   ["custrecord_frf_details_project.custentity_clientpartner","anyof",i_user]],
				   "AND",
				   ["custrecord_frf_details_status","is","F"], 
				   "AND", 
				   ["custrecord_frf_details_opp_id.custrecord_projection_status_sfdc","anyof","2"], 
				   "AND", 
				   ["custrecord_frf_details_opp_id.custrecord_stage_sfdc","noneof","8","9"], 
				   "AND", 
				   ["custrecord_frf_details_opp_id.custrecord_opp_type_sfdc","is","New Business"], 
				   "AND", 
				   ["isinactive","is","F"]]);
		   filters.push('OR');
		   filters.push([[["custrecord_frf_details_opp_id.custrecord_sfdc_opp_pm","anyof",i_user],
				   "OR",
				   ["custrecord_frf_details_opp_id.custrecord_sfdc_opportunity_dm","anyof",i_user],
				    "OR",
				   ["custrecord_frf_details_opp_id.custrecord_sfdc_client_partner","anyof",i_user]],
				   "AND",
				   ["custrecord_frf_details_status","is","F"], 
				   "AND", 
				   ["custrecord_frf_details_opp_id.custrecord_projection_status_sfdc","anyof","2"], 
				   "AND", 
				   ["custrecord_frf_details_opp_id.custrecord_stage_sfdc","noneof","8","9"], 
				   "AND", 
				   ["custrecord_frf_details_opp_id.custrecord_opp_type_sfdc","is","New Business"], 
				   "AND", 
				   ["isinactive","is","F"]]);
		   if(isArrayNotEmpty(s_filters.region))
			{
				filters.push('AND');
				filters.push(["custrecord_frf_details_account.custentity_region","anyof",s_filters.region]);
			}
			if(isArrayNotEmpty(s_filters.practice))
			{
				filters.push('AND');
				filters.push(["custrecord_frf_details_res_practice","anyof",s_filters.practice]);
			}
			if(isArrayNotEmpty(s_filters.account))
			{
				filters.push('AND');
				filters.push(["custrecord_frf_details_account","anyof",s_filters.account]);
			}
			if(isArrayNotEmpty(s_filters.project))
			{
				filters.push('AND');
				filters.push(["custrecord_frf_details_project","anyof",s_filters.project],"OR",["custrecord_frf_details_opp_id","anyof",s_filters.project]);
			}
		}
		columns = [
		  new nlobjSearchColumn("internalid",null,"COUNT")
		]
		var customrecord_frf_detailsSearch = searchRecord("customrecord_frf_details", null, filters, columns,null)	
		if(customrecord_frf_detailsSearch) 
		{
			var dataArray = new Array();
			return customrecord_frf_detailsSearch[0].getValue("internalid",null,"COUNT");
		}
		else
		{
			var dataArray = [];
			return dataArray;
		}
	}
	catch(error)
	{
		return error;
	}
}
//===================================================================================================
function PipelineOpportunitiesCount(i_user,i_practice,isAdmin,i_region,s_filters)
{
	try
	{
		var filters = new Array();
		var columns = new Array();
		s_filters = JSON.parse(s_filters);
		if(isAdmin == true)
		{
			/* filters = [["custrecord_fulfill_dashboard_opp_id.custrecord_stage_sfdc","noneof","8","9"], 
					   "AND", 
					   ["custrecord_fulfill_dashboard_opp_id.isinactive","is","F"], 
					   "AND", 
					   ["custrecord_fulfill_dashboard_opp_id.custrecord_projection_status_sfdc","anyof","1"], 
					   "AND", 
					   ["custrecord_fulfill_dashboard_opp_id.custrecord_opp_type_sfdc","is","New Business"]
					   ]*/
			   filters.push(["custrecord_fulfill_dashboard_opp_id.custrecord_stage_sfdc","noneof","8","9"], 
					   "AND", 
					   ["custrecord_fulfill_dashboard_opp_id.isinactive","is","F"], 
					   "AND", 
					   ["custrecord_fulfill_dashboard_opp_id.custrecord_projection_status_sfdc","anyof","1"], 
					   "AND", 
					   ["custrecord_fulfill_dashboard_opp_id.custrecord_opp_type_sfdc","is","New Business"]);
			   if(isArrayNotEmpty(s_filters.region))
						{
							filters.push('AND');
							filters.push(["custrecord_fulfill_dashboard_project.custentity_region","anyof",s_filters.region]);
						}
						if(isArrayNotEmpty(s_filters.practice))
						{
							filters.push('AND');
							filters.push(["custrecord_fulfill_dashboard_project.custentity_practice","anyof",s_filters.practice]);
						}
						if(isArrayNotEmpty(s_filters.account))
						{
							filters.push('AND');
							filters.push(["custrecord_fulfill_dashboard_account","anyof",s_filters.account]);
						}
						if(isArrayNotEmpty(s_filters.project))
						{
							filters.push('AND');
							filters.push(["custrecord_fulfill_dashboard_project","anyof",s_filters.project],"OR",["custrecord_fulfill_dashboard_opp_id","anyof",s_filters.project]);
						}
						
		}
		else if(i_practice)
		{
			var practiceArray = getSubPractices(i_practice);
			//nlapiLogExecution('Debug','i_practice ',i_practice);
			/*filters = [
					["custrecord_fulfill_dashboard_opp_id.custrecord_practice_internal_id_sfdc","anyof",practiceArray],
					"AND",
					["custrecord_fulfill_dashboard_opp_id.custrecord_stage_sfdc","noneof","8","9"], 
				   "AND", 
				   ["custrecord_fulfill_dashboard_opp_id.isinactive","is","F"], 
				   "AND", 
				   ["custrecord_fulfill_dashboard_opp_id.custrecord_projection_status_sfdc","anyof","1"], 
				   "AND", 
				   ["custrecord_fulfill_dashboard_opp_id.custrecord_opp_type_sfdc","is","New Business"]]*/
		   filters.push(["custrecord_fulfill_dashboard_opp_id.custrecord_practice_internal_id_sfdc","anyof",practiceArray],
					"AND",
					["custrecord_fulfill_dashboard_opp_id.custrecord_stage_sfdc","noneof","8","9"], 
				   "AND", 
				   ["custrecord_fulfill_dashboard_opp_id.isinactive","is","F"], 
				   "AND", 
				   ["custrecord_fulfill_dashboard_opp_id.custrecord_projection_status_sfdc","anyof","1"], 
				   "AND", 
				   ["custrecord_fulfill_dashboard_opp_id.custrecord_opp_type_sfdc","is","New Business"]);
		   if(isArrayNotEmpty(s_filters.region))
						{
							filters.push('AND');
							filters.push(["custrecord_fulfill_dashboard_project.custentity_region","anyof",s_filters.region]);
						}
						if(isArrayNotEmpty(s_filters.practice))
						{
							filters.push('AND');
							filters.push(["custrecord_fulfill_dashboard_project.custentity_practice","anyof",s_filters.practice]);
						}
						if(isArrayNotEmpty(s_filters.account))
						{
							filters.push('AND');
							filters.push(["custrecord_fulfill_dashboard_account","anyof",s_filters.account]);
						}
						if(isArrayNotEmpty(s_filters.project))
						{
							filters.push('AND');
							filters.push(["custrecord_fulfill_dashboard_project","anyof",s_filters.project],"OR",["custrecord_fulfill_dashboard_opp_id","anyof",s_filters.project]);
						}
			
		}
		else if(i_region)
		{

           /* filters = [
				["custrecord_fulfill_dashboard_opp_id.custrecord_sfdc_opp_account_region","anyof",i_region],
				"AND",
                ["custrecord_fulfill_dashboard_opp_id.custrecord_stage_sfdc","noneof","8","9"], 
			   "AND", 
			   ["custrecord_fulfill_dashboard_opp_id.isinactive","is","F"], 
			   "AND", 
			   ["custrecord_fulfill_dashboard_opp_id.custrecord_projection_status_sfdc","anyof","1"], 
			   "AND", 
			   ["custrecord_fulfill_dashboard_opp_id.custrecord_opp_type_sfdc","is","New Business"]]*/

	   filters.push(["custrecord_fulfill_dashboard_opp_id.custrecord_sfdc_opp_account_region","anyof",i_region],
				"AND",
                ["custrecord_fulfill_dashboard_opp_id.custrecord_stage_sfdc","noneof","8","9"], 
			   "AND", 
			   ["custrecord_fulfill_dashboard_opp_id.isinactive","is","F"], 
			   "AND", 
			   ["custrecord_fulfill_dashboard_opp_id.custrecord_projection_status_sfdc","anyof","1"], 
			   "AND", 
			   ["custrecord_fulfill_dashboard_opp_id.custrecord_opp_type_sfdc","is","New Business"]);
	   if(isArrayNotEmpty(s_filters.region))
						{
							filters.push('AND');
							filters.push(["custrecord_fulfill_dashboard_project.custentity_region","anyof",s_filters.region]);
						}
						if(isArrayNotEmpty(s_filters.practice))
						{
							filters.push('AND');
							filters.push(["custrecord_fulfill_dashboard_project.custentity_practice","anyof",s_filters.practice]);
						}
						if(isArrayNotEmpty(s_filters.account))
						{
							filters.push('AND');
							filters.push(["custrecord_fulfill_dashboard_account","anyof",s_filters.account]);
						}
						if(isArrayNotEmpty(s_filters.project))
						{
							filters.push('AND');
							filters.push(["custrecord_fulfill_dashboard_project","anyof",s_filters.project],"OR",["custrecord_fulfill_dashboard_opp_id","anyof",s_filters.project]);
						}
		}
		else if(i_user)
		{
			/*filters = [[
				["custrecord_fulfill_dashboard_opp_id.custrecord_sfdc_opp_pm","anyof",i_user],
				"OR",
				["custrecord_fulfill_dashboard_opp_id.custrecord_sfdc_opportunity_dm","anyof",i_user],
				 "OR",
				["custrecord_fulfill_dashboard_opp_id.custrecord_sfdc_client_partner","anyof",i_user]],
				"AND",
				["custrecord_fulfill_dashboard_opp_id.custrecord_stage_sfdc","noneof","8","9"], 
			   "AND", 
			   ["custrecord_fulfill_dashboard_opp_id.isinactive","is","F"], 
			   "AND", 
			   ["custrecord_fulfill_dashboard_opp_id.custrecord_projection_status_sfdc","anyof","1"], 
			   "AND", 
			   ["custrecord_fulfill_dashboard_opp_id.custrecord_opp_type_sfdc","is","New Business"]]*/
	   filters.push([
				["custrecord_fulfill_dashboard_opp_id.custrecord_sfdc_opp_pm","anyof",i_user],
				"OR",
				["custrecord_fulfill_dashboard_opp_id.custrecord_sfdc_opportunity_dm","anyof",i_user],
				 "OR",
				["custrecord_fulfill_dashboard_opp_id.custrecord_sfdc_client_partner","anyof",i_user]],
				"AND",
				["custrecord_fulfill_dashboard_opp_id.custrecord_stage_sfdc","noneof","8","9"], 
			   "AND", 
			   ["custrecord_fulfill_dashboard_opp_id.isinactive","is","F"], 
			   "AND", 
			   ["custrecord_fulfill_dashboard_opp_id.custrecord_projection_status_sfdc","anyof","1"], 
			   "AND", 
			   ["custrecord_fulfill_dashboard_opp_id.custrecord_opp_type_sfdc","is","New Business"]);
	  				 if(isArrayNotEmpty(s_filters.region))
						{
							filters.push('AND');
							filters.push(["custrecord_fulfill_dashboard_project.custentity_region","anyof",s_filters.region]);
						}
						if(isArrayNotEmpty(s_filters.practice))
						{
							filters.push('AND');
							filters.push(["custrecord_fulfill_dashboard_project.custentity_practice","anyof",s_filters.practice]);
						}
						if(isArrayNotEmpty(s_filters.account))
						{
							filters.push('AND');
							filters.push(["custrecord_fulfill_dashboard_account","anyof",s_filters.account]);
						}
						if(isArrayNotEmpty(s_filters.project))
						{
							filters.push('AND');
							filters.push(["custrecord_fulfill_dashboard_project","anyof",s_filters.project],"OR",["custrecord_fulfill_dashboard_opp_id","anyof",s_filters.project]);
						}
		}
		//nlapiLogExecution('Debug','filters P',filters);
		
		columns = [
		  // new nlobjSearchColumn("custrecord_fulfill_dashboard_status"), 
		   new nlobjSearchColumn("internalid",null,"COUNT"), 
		 
		]
		var customrecord_fulfillment_dashboard_dataSearch = searchRecord("customrecord_fulfillment_dashboard_data",null, filters,columns);
		if(customrecord_fulfillment_dashboard_dataSearch) 
		{
			var dataArray = new Array();
			return customrecord_fulfillment_dashboard_dataSearch[0].getValue("internalid",null,"COUNT");
		}
		else
		{
			var dataArray = [];
			return dataArray;
		}
		}
	catch(error)
	{
		return error;
	}
}

//======================================================================================================================
function PipelinePositionsRequestedCount(i_user,i_practice,isAdmin,i_region,s_filters)
{
	try
	{
		var filters = new Array();
		var columns = new Array();
		s_filters = JSON.parse(s_filters);
		if(isAdmin == true)
		{
			 /*filters =  [ ["custrecord_fulfill_plan_opp_id.custrecord_projection_status_sfdc","anyof","1"], 
						   "AND", 
						   ["custrecord_fulfill_plan_opp_id.custrecord_stage_sfdc","noneof","8","9"], 
						   "AND", 
						   ["isinactive","is","F"], 
						   "AND", 
						   ["custrecord_fulfill_plan_opp_id.custrecord_opp_type_sfdc","is","New Business"],
							"AND",
						   ["custrecord_fulfill_plan_status","isnot", "Cancelled"]]*/

					filters.push(["custrecord_fulfill_plan_opp_id.custrecord_projection_status_sfdc","anyof","1"], 
						   "AND", 
						   ["custrecord_fulfill_plan_opp_id.custrecord_stage_sfdc","noneof","8","9"], 
						   "AND", 
						   ["isinactive","is","F"], 
						   "AND", 
						   ["custrecord_fulfill_plan_opp_id.custrecord_opp_type_sfdc","is","New Business"],
							"AND",
						   ["custrecord_fulfill_plan_status","isnot", "Cancelled"]);	
						   if(isArrayNotEmpty(s_filters.region))
							{
								filters.push('AND');
								filters.push(["custrecord_fulfill_plan_opp_id.custrecord_sfdc_opp_account_region","anyof",s_filters.region]);
							}
							if(isArrayNotEmpty(s_filters.practice))
							{
								filters.push('AND');
								filters.push(["custrecord_fulfill_plan_opp_id.custrecord_practice_internal_id_sfdc","anyof",s_filters.practice]);
							}
							/*if(isArrayNotEmpty(s_filters.account))
							{
								filters.push('AND');
								filters.push(["custrecord_fulfill_dashboard_account","anyof",s_filters.account]);
							}
							if(isArrayNotEmpty(s_filters.project))
							{
								filters.push('AND');
								filters.push(["custrecord_fulfill_dashboard_project","anyof",s_filters.project],"OR",["custrecord_fulfill_dashboard_opp_id","anyof",s_filters.project]);
							}	*/
		}
		else if(i_practice)
		{
			var practiceArray = getSubPractices(i_practice);
			/*filters = [
						["custrecord_fulfill_plan_opp_id.custrecord_practice_internal_id_sfdc","anyof",practiceArray],
						"AND",
						["custrecord_fulfill_plan_opp_id.custrecord_projection_status_sfdc","anyof","1"], 
						"AND", 
						["custrecord_fulfill_plan_opp_id.custrecord_stage_sfdc","noneof","8","9"], 
						"AND", 
						["isinactive","is","F"], 
						"AND", 
						["custrecord_fulfill_plan_opp_id.custrecord_opp_type_sfdc","is","New Business"],
						"AND",
						 ["custrecord_fulfill_plan_status","isnot", "Cancelled"]]*/
			 filters.push(["custrecord_fulfill_plan_opp_id.custrecord_practice_internal_id_sfdc","anyof",practiceArray],
						"AND",
						["custrecord_fulfill_plan_opp_id.custrecord_projection_status_sfdc","anyof","1"], 
						"AND", 
						["custrecord_fulfill_plan_opp_id.custrecord_stage_sfdc","noneof","8","9"], 
						"AND", 
						["isinactive","is","F"], 
						"AND", 
						["custrecord_fulfill_plan_opp_id.custrecord_opp_type_sfdc","is","New Business"],
						"AND",
						 ["custrecord_fulfill_plan_status","isnot", "Cancelled"]);
			 						   if(isArrayNotEmpty(s_filters.region))
							{
								filters.push('AND');
								filters.push(["custrecord_fulfill_plan_opp_id.custrecord_sfdc_opp_account_region","anyof",s_filters.region]);
							}
							if(isArrayNotEmpty(s_filters.practice))
							{
								filters.push('AND');
								filters.push(["custrecord_fulfill_plan_opp_id.custrecord_practice_internal_id_sfdc","anyof",s_filters.practice]);
							}
							/*if(isArrayNotEmpty(s_filters.account))
							{
								filters.push('AND');
								filters.push(["custrecord_fulfill_dashboard_account","anyof",s_filters.account]);
							}
							if(isArrayNotEmpty(s_filters.project))
							{
								filters.push('AND');
								filters.push(["custrecord_fulfill_dashboard_project","anyof",s_filters.project],"OR",["custrecord_fulfill_dashboard_opp_id","anyof",s_filters.project]);
							}	*/
			
		}
		else if(i_region)
		{

           /* filters = [
				["custrecord_fulfill_plan_opp_id.custrecord_sfdc_opp_account_region","anyof",i_region],
				"AND",
				["custrecord_fulfill_plan_opp_id.custrecord_projection_status_sfdc","anyof","1"], 
			   "AND", 
			   ["custrecord_fulfill_plan_opp_id.custrecord_stage_sfdc","noneof","8","9"], 
			   "AND", 
			   ["isinactive","is","F"], 
			   "AND", 
			   ["custrecord_fulfill_plan_opp_id.custrecord_opp_type_sfdc","is","New Business"],
				"AND",
				["custrecord_fulfill_plan_status","isnot", "Cancelled"]]*/
		filters.push(["custrecord_fulfill_plan_opp_id.custrecord_sfdc_opp_account_region","anyof",i_region],
				"AND",
				["custrecord_fulfill_plan_opp_id.custrecord_projection_status_sfdc","anyof","1"], 
			   "AND", 
			   ["custrecord_fulfill_plan_opp_id.custrecord_stage_sfdc","noneof","8","9"], 
			   "AND", 
			   ["isinactive","is","F"], 
			   "AND", 
			   ["custrecord_fulfill_plan_opp_id.custrecord_opp_type_sfdc","is","New Business"],
				"AND",
				["custrecord_fulfill_plan_status","isnot", "Cancelled"]);
								   if(isArrayNotEmpty(s_filters.region))
							{
								filters.push('AND');
								filters.push(["custrecord_fulfill_plan_opp_id.custrecord_sfdc_opp_account_region","anyof",s_filters.region]);
							}
							if(isArrayNotEmpty(s_filters.practice))
							{
								filters.push('AND');
								filters.push(["custrecord_fulfill_plan_opp_id.custrecord_practice_internal_id_sfdc","anyof",s_filters.practice]);
							}
							/*if(isArrayNotEmpty(s_filters.account))
							{
								filters.push('AND');
								filters.push(["custrecord_fulfill_dashboard_account","anyof",s_filters.account]);
							}
							if(isArrayNotEmpty(s_filters.project))
							{
								filters.push('AND');
								filters.push(["custrecord_fulfill_dashboard_project","anyof",s_filters.project],"OR",["custrecord_fulfill_dashboard_opp_id","anyof",s_filters.project]);
							}	*/
		}
		else if(i_user)
		{
			/*filters = [[
				["custrecord_fulfill_plan_opp_id.custrecord_sfdc_opp_pm","anyof",i_user],
				"OR",
				["custrecord_fulfill_plan_opp_id.custrecord_sfdc_opportunity_dm","anyof",i_user],
				 "OR",
				["custrecord_fulfill_plan_opp_id.custrecord_sfdc_client_partner","anyof",i_user]],
				 "AND",
				["custrecord_fulfill_plan_opp_id.custrecord_projection_status_sfdc","anyof","1"], 
			   "AND", 
			   ["custrecord_fulfill_plan_opp_id.custrecord_stage_sfdc","noneof","8","9"], 
			   "AND", 
			   ["isinactive","is","F"], 
			   "AND", 
			   ["custrecord_fulfill_plan_opp_id.custrecord_opp_type_sfdc","is","New Business"],
				"AND",
				["custrecord_fulfill_plan_status","isnot", "Cancelled"]]*/
		filters.push([
				["custrecord_fulfill_plan_opp_id.custrecord_sfdc_opp_pm","anyof",i_user],
				"OR",
				["custrecord_fulfill_plan_opp_id.custrecord_sfdc_opportunity_dm","anyof",i_user],
				 "OR",
				["custrecord_fulfill_plan_opp_id.custrecord_sfdc_client_partner","anyof",i_user]],
				 "AND",
				["custrecord_fulfill_plan_opp_id.custrecord_projection_status_sfdc","anyof","1"], 
			   "AND", 
			   ["custrecord_fulfill_plan_opp_id.custrecord_stage_sfdc","noneof","8","9"], 
			   "AND", 
			   ["isinactive","is","F"], 
			   "AND", 
			   ["custrecord_fulfill_plan_opp_id.custrecord_opp_type_sfdc","is","New Business"],
				"AND",
				["custrecord_fulfill_plan_status","isnot", "Cancelled"]);
								   if(isArrayNotEmpty(s_filters.region))
							{
								filters.push('AND');
								filters.push(["custrecord_fulfill_plan_opp_id.custrecord_sfdc_opp_account_region","anyof",s_filters.region]);
							}
							if(isArrayNotEmpty(s_filters.practice))
							{
								filters.push('AND');
								filters.push(["custrecord_fulfill_plan_opp_id.custrecord_practice_internal_id_sfdc","anyof",s_filters.practice]);
							}
							/*if(isArrayNotEmpty(s_filters.account))
							{
								filters.push('AND');
								filters.push(["custrecord_fulfill_dashboard_account","anyof",s_filters.account]);
							}
							if(isArrayNotEmpty(s_filters.project))
							{
								filters.push('AND');
								filters.push(["custrecord_fulfill_dashboard_project","anyof",s_filters.project],"OR",["custrecord_fulfill_dashboard_opp_id","anyof",s_filters.project]);
							}	*/
		}
		
		columns = [
		  // new nlobjSearchColumn("custrecord_fulfill_dashboard_status"), 
			new nlobjSearchColumn("formulanumeric",null,"SUM").setFormula("TO_NUMBER({custrecord_fulfill_plan_position})"),
			new nlobjSearchColumn("internalid",null,"COUNT")
		]
		var customrecord_fulfillment_plan_sfdcSearch  = searchRecord("customrecord_fulfillment_plan_sfdc",null, filters,columns,null);
		if(customrecord_fulfillment_plan_sfdcSearch ) 
		{
			var a_columns = customrecord_fulfillment_plan_sfdcSearch[0].getAllColumns();
			//nlapiLogExecution('debug','a_columns ',customrecord_fulfillment_plan_sfdcSearch[0].getValue(a_columns[0]));
			var dataArray = new Array();
			//return customrecord_fulfillment_plan_sfdcSearch[0].getValue("internalid",null,"COUNT");
			if(customrecord_fulfillment_plan_sfdcSearch[0].getValue(a_columns[0]))
				return customrecord_fulfillment_plan_sfdcSearch[0].getValue(a_columns[0]);
			else 
				return 0;
		}
		else
		{
			var dataArray = [];
			return dataArray;
		}
		}
	catch(error)
	{
		return error;
	}
}

//===================================================================================================================
function PipelineFrfsCreatedCount(i_user, practiceId, isAdmin,i_region,s_filters)
{
	try
	{
		var filters = new Array();
		var columns = new Array();
		s_filters = JSON.parse(s_filters);
		if(isAdmin == true)
		{
			/*filters =  [   ["custrecord_frf_details_status","is","F"], 
					   "AND", 
					   ["custrecord_frf_details_opp_id.custrecord_projection_status_sfdc","anyof","1"], 
					   "AND", 
					   ["custrecord_frf_details_opp_id.custrecord_stage_sfdc","noneof","8","9"], 
					   "AND", 
					   ["custrecord_frf_details_opp_id.custrecord_opp_type_sfdc","is","New Business"], 
					   "AND", 
					   ["isinactive","is","F"]]*/
			   filters.push( ["custrecord_frf_details_status","is","F"], 
					   "AND", 
					   ["custrecord_frf_details_opp_id.custrecord_projection_status_sfdc","anyof","1"], 
					   "AND", 
					   ["custrecord_frf_details_opp_id.custrecord_stage_sfdc","noneof","8","9"], 
					   "AND", 
					   ["custrecord_frf_details_opp_id.custrecord_opp_type_sfdc","is","New Business"], 
					   "AND", 
					   ["isinactive","is","F"]);
			   if(isArrayNotEmpty(s_filters.region))
			{
				filters.push('AND');
				filters.push(["custrecord_frf_details_account.custentity_region","anyof",s_filters.region]);
			}
			if(isArrayNotEmpty(s_filters.practice))
			{
				filters.push('AND');
				filters.push(["custrecord_frf_details_res_practice","anyof",s_filters.practice]);
			}
			if(isArrayNotEmpty(s_filters.account))
			{
				filters.push('AND');
				filters.push(["custrecord_frf_details_account","anyof",s_filters.account]);
			}
			if(isArrayNotEmpty(s_filters.project))
			{
				filters.push('AND');
				filters.push(["custrecord_frf_details_project","anyof",s_filters.project],"OR",["custrecord_frf_details_opp_id","anyof",s_filters.project]);
			}
		}
		else if(practiceId)
		{
			var practiceArray = getSubPractices(practiceId);
			/*filters = [
				  ["custrecord_frf_details_res_practice","anyof",practiceArray],
				   "AND",
				   ["custrecord_frf_details_status","is","F"], 
				   "AND", 
				   ["custrecord_frf_details_opp_id.custrecord_projection_status_sfdc","anyof","1"], 
				   "AND", 
				   ["custrecord_frf_details_opp_id.custrecord_stage_sfdc","noneof","8","9"], 
				   "AND", 
				   ["custrecord_frf_details_opp_id.custrecord_opp_type_sfdc","is","New Business"], 
				   "AND", 
				   ["isinactive","is","F"]]*/
		   filters.push(["custrecord_frf_details_res_practice","anyof",practiceArray],
				   "AND",
				   ["custrecord_frf_details_status","is","F"], 
				   "AND", 
				   ["custrecord_frf_details_opp_id.custrecord_projection_status_sfdc","anyof","1"], 
				   "AND", 
				   ["custrecord_frf_details_opp_id.custrecord_stage_sfdc","noneof","8","9"], 
				   "AND", 
				   ["custrecord_frf_details_opp_id.custrecord_opp_type_sfdc","is","New Business"], 
				   "AND", 
				   ["isinactive","is","F"]);
		   if(isArrayNotEmpty(s_filters.region))
			{
				filters.push('AND');
				filters.push(["custrecord_frf_details_account.custentity_region","anyof",s_filters.region]);
			}
			if(isArrayNotEmpty(s_filters.practice))
			{
				filters.push('AND');
				filters.push(["custrecord_frf_details_res_practice","anyof",s_filters.practice]);
			}
			if(isArrayNotEmpty(s_filters.account))
			{
				filters.push('AND');
				filters.push(["custrecord_frf_details_account","anyof",s_filters.account]);
			}
			if(isArrayNotEmpty(s_filters.project))
			{
				filters.push('AND');
				filters.push(["custrecord_frf_details_project","anyof",s_filters.project],"OR",["custrecord_frf_details_opp_id","anyof",s_filters.project]);
			}
				
		}
		else if(i_region)
		{
			 
			/*filters = [
				["custrecord_frf_details_account.custentity_region","anyof",i_region],
				"AND",
                ["custrecord_frf_details_status","is","F"], 
			   "AND", 
			   ["custrecord_frf_details_opp_id.custrecord_projection_status_sfdc","anyof","1"], 
			   "AND", 
			   ["custrecord_frf_details_opp_id.custrecord_stage_sfdc","noneof","8","9"], 
			   "AND", 
			   ["custrecord_frf_details_opp_id.custrecord_opp_type_sfdc","is","New Business"], 
			   "AND", 
			   ["isinactive","is","F"]]*/
	   filters.push(["custrecord_frf_details_account.custentity_region","anyof",i_region],
				"AND",
                ["custrecord_frf_details_status","is","F"], 
			   "AND", 
			   ["custrecord_frf_details_opp_id.custrecord_projection_status_sfdc","anyof","1"], 
			   "AND", 
			   ["custrecord_frf_details_opp_id.custrecord_stage_sfdc","noneof","8","9"], 
			   "AND", 
			   ["custrecord_frf_details_opp_id.custrecord_opp_type_sfdc","is","New Business"], 
			   "AND", 
			   ["isinactive","is","F"]);
	   if(isArrayNotEmpty(s_filters.region))
			{
				filters.push('AND');
				filters.push(["custrecord_frf_details_account.custentity_region","anyof",s_filters.region]);
			}
			if(isArrayNotEmpty(s_filters.practice))
			{
				filters.push('AND');
				filters.push(["custrecord_frf_details_res_practice","anyof",s_filters.practice]);
			}
			if(isArrayNotEmpty(s_filters.account))
			{
				filters.push('AND');
				filters.push(["custrecord_frf_details_account","anyof",s_filters.account]);
			}
			if(isArrayNotEmpty(s_filters.project))
			{
				filters.push('AND');
				filters.push(["custrecord_frf_details_project","anyof",s_filters.project],"OR",["custrecord_frf_details_opp_id","anyof",s_filters.project]);
			}
		}
		else if(i_user)
		{
			/*filters = [
				   [[["custrecord_frf_details_project.custentity_projectmanager","anyof",i_user],
				   "OR",
				   ["custrecord_frf_details_project.custentity_deliverymanager","anyof",i_user],
				    "OR",
				   ["custrecord_frf_details_project.custentity_clientpartner","anyof",i_user]],
				   "AND",
				    ["custrecord_frf_details_status","is","F"], 
				   "AND", 
				   ["custrecord_frf_details_opp_id.custrecord_projection_status_sfdc","anyof","1"], 
				   "AND", 
				   ["custrecord_frf_details_opp_id.custrecord_stage_sfdc","noneof","8","9"], 
				   "AND", 
				   ["custrecord_frf_details_opp_id.custrecord_opp_type_sfdc","is","New Business"], 
				   "AND", 
				   ["isinactive","is","F"]],
				   "OR",
				   [[["custrecord_frf_details_opp_id.custrecord_sfdc_opp_pm","anyof",i_user],
				   "OR",
				   ["custrecord_frf_details_opp_id.custrecord_sfdc_opportunity_dm","anyof",i_user],
				    "OR",
				   ["custrecord_frf_details_opp_id.custrecord_sfdc_client_partner","anyof",i_user]],
				   "AND",
				    ["custrecord_frf_details_status","is","F"], 
				   "AND", 
				   ["custrecord_frf_details_opp_id.custrecord_projection_status_sfdc","anyof","1"], 
				   "AND", 
				   ["custrecord_frf_details_opp_id.custrecord_stage_sfdc","noneof","8","9"], 
				   "AND", 
				   ["custrecord_frf_details_opp_id.custrecord_opp_type_sfdc","is","New Business"], 
				   "AND", 
				   ["isinactive","is","F"]]
				   ]*/

	   filters.push( [[["custrecord_frf_details_project.custentity_projectmanager","anyof",i_user],
				   "OR",
				   ["custrecord_frf_details_project.custentity_deliverymanager","anyof",i_user],
				    "OR",
				   ["custrecord_frf_details_project.custentity_clientpartner","anyof",i_user]],
				   "AND",
				    ["custrecord_frf_details_status","is","F"], 
				   "AND", 
				   ["custrecord_frf_details_opp_id.custrecord_projection_status_sfdc","anyof","1"], 
				   "AND", 
				   ["custrecord_frf_details_opp_id.custrecord_stage_sfdc","noneof","8","9"], 
				   "AND", 
				   ["custrecord_frf_details_opp_id.custrecord_opp_type_sfdc","is","New Business"], 
				   "AND", 
				   ["isinactive","is","F"]]);
	   filters.push('OR');
	   filters.push([[["custrecord_frf_details_opp_id.custrecord_sfdc_opp_pm","anyof",i_user],
				   "OR",
				   ["custrecord_frf_details_opp_id.custrecord_sfdc_opportunity_dm","anyof",i_user],
				    "OR",
				   ["custrecord_frf_details_opp_id.custrecord_sfdc_client_partner","anyof",i_user]],
				   "AND",
				    ["custrecord_frf_details_status","is","F"], 
				   "AND", 
				   ["custrecord_frf_details_opp_id.custrecord_projection_status_sfdc","anyof","1"], 
				   "AND", 
				   ["custrecord_frf_details_opp_id.custrecord_stage_sfdc","noneof","8","9"], 
				   "AND", 
				   ["custrecord_frf_details_opp_id.custrecord_opp_type_sfdc","is","New Business"], 
				   "AND", 
				   ["isinactive","is","F"]]);
	   if(isArrayNotEmpty(s_filters.region))
			{
				filters.push('AND');
				filters.push(["custrecord_frf_details_account.custentity_region","anyof",s_filters.region]);
			}
			if(isArrayNotEmpty(s_filters.practice))
			{
				filters.push('AND');
				filters.push(["custrecord_frf_details_res_practice","anyof",s_filters.practice]);
			}
			if(isArrayNotEmpty(s_filters.account))
			{
				filters.push('AND');
				filters.push(["custrecord_frf_details_account","anyof",s_filters.account]);
			}
			if(isArrayNotEmpty(s_filters.project))
			{
				filters.push('AND');
				filters.push(["custrecord_frf_details_project","anyof",s_filters.project],"OR",["custrecord_frf_details_opp_id","anyof",s_filters.project]);
			}
		}
		columns = [
		  new nlobjSearchColumn("internalid",null,"COUNT")
		]
		var customrecord_frf_detailsSearch = searchRecord("customrecord_frf_details", null, filters, columns,null)	
		if(customrecord_frf_detailsSearch) 
		{
			var dataArray = new Array();
			return customrecord_frf_detailsSearch[0].getValue("internalid",null,"COUNT");
		}
		else
		{
			var dataArray = [];
			return dataArray;
		}
	}
	catch(error)
	{
		return error;
	}
}


//===================================================================================================================
function ProjectsWithRiskCount (i_user, practiceId, isAdmin,i_region,s_filters)
{
	try
	{
		var filters = new Array();
		var columns = new Array();
		var d_today_temp = nlapiAddDays(d_today, 20)
		//nlapiLogExecution('audit','d_today_temp 2@@ '+d_today_temp,'d_today @@ '+d_today);
		s_filters = JSON.parse(s_filters);
		if(isAdmin == true)
		{
			/*filters =  [["isinactive","is","F"], 
					   "AND", 
					   ["custrecord_frf_details_project","noneof","@NONE@"], 
					   "AND", 
					   ["custrecord_frf_details_opp_id","anyof","@NONE@"], 
					   "AND", 
					   ["custrecord_frf_details_project.status","noneof","1"], 
					   "AND", 
					   ["custrecord_frf_details_status","is","F"], 
					   "AND", 
					   [[["custrecord_frf_details_start_date","within",d_today,d_today_temp],"AND",["custrecord_frf_details_external_hire","is","F"]],"OR",[["formulanumeric: {custrecord_frf_details_availabledate} - {custrecord_frf_details_start_date}","greaterthan","0"],"AND",["custrecord_frf_details_external_hire","is","T"]]],
					   "AND", 
					   ["custrecord_frf_details_start_date","onorafter",d_today], 
					   "AND", 
					   ["custrecord_frf_details_status_flag","anyof","1"]]*/
			   filters.push(["isinactive","is","F"], 
					   "AND", 
					   ["custrecord_frf_details_project","noneof","@NONE@"], 
					   "AND", 
					   ["custrecord_frf_details_opp_id","anyof","@NONE@"], 
					   "AND", 
					   ["custrecord_frf_details_project.status","noneof","1"], 
					   "AND", 
					   ["custrecord_frf_details_status","is","F"], 
					   "AND", 
					   [[["custrecord_frf_details_start_date","within",d_today,d_today_temp],"AND",["custrecord_frf_details_external_hire","is","F"]],"OR",[["formulanumeric: {custrecord_frf_details_availabledate} - {custrecord_frf_details_start_date}","greaterthan","0"],"AND",["custrecord_frf_details_external_hire","is","T"]]],
					   "AND", 
					   ["custrecord_frf_details_start_date","onorafter",d_today], 
					   "AND", 
					   ["custrecord_frf_details_status_flag","anyof","1"]);
			   if(isArrayNotEmpty(s_filters.region))
				{
					filters.push('AND');
					filters.push(["custrecord_frf_details_account.custentity_region","anyof",s_filters.region]);
				}
				if(isArrayNotEmpty(s_filters.practice))
				{
					filters.push('AND');
					filters.push(["custrecord_frf_details_res_practice","anyof",s_filters.practice]);
				}
				if(isArrayNotEmpty(s_filters.account))
				{
					filters.push('AND');
					filters.push(["custrecord_frf_details_account","anyof",s_filters.account]);
				}
				if(isArrayNotEmpty(s_filters.project))
				{
					filters.push('AND');
					filters.push(["custrecord_frf_details_project","anyof",s_filters.project],"OR",["custrecord_frf_details_opp_id","anyof",s_filters.project]);
				}
		}
		else if(practiceId)
		{
			
			var practiceArray = getSubPractices(practiceId);	
			/*filters = [
				   ["custrecord_frf_details_res_practice","anyof",practiceArray],
				   "AND",
				   ["isinactive","is","F"], 
				   "AND", 
				   ["custrecord_frf_details_project","noneof","@NONE@"], 
				   "AND", 
				   ["custrecord_frf_details_opp_id","anyof","@NONE@"], 
				   "AND", 
				   ["custrecord_frf_details_project.status","noneof","1"], 
				   "AND", 
				   ["custrecord_frf_details_status","is","F"], 
				   "AND", 
				   [[["custrecord_frf_details_start_date","within",d_today,d_today_temp],"AND",["custrecord_frf_details_external_hire","is","F"]],"OR",[["formulanumeric: {custrecord_frf_details_availabledate} - {custrecord_frf_details_start_date}","greaterthan","0"],"AND",["custrecord_frf_details_external_hire","is","T"]]], 
				   "AND", 
				   ["custrecord_frf_details_start_date","onorafter",d_today], 
				   "AND", 
				   ["custrecord_frf_details_status_flag","anyof","1"]]*/
		   filters.push( ["custrecord_frf_details_res_practice","anyof",practiceArray],
				   "AND",
				   ["isinactive","is","F"], 
				   "AND", 
				   ["custrecord_frf_details_project","noneof","@NONE@"], 
				   "AND", 
				   ["custrecord_frf_details_opp_id","anyof","@NONE@"], 
				   "AND", 
				   ["custrecord_frf_details_project.status","noneof","1"], 
				   "AND", 
				   ["custrecord_frf_details_status","is","F"], 
				   "AND", 
				   [[["custrecord_frf_details_start_date","within",d_today,d_today_temp],"AND",["custrecord_frf_details_external_hire","is","F"]],"OR",[["formulanumeric: {custrecord_frf_details_availabledate} - {custrecord_frf_details_start_date}","greaterthan","0"],"AND",["custrecord_frf_details_external_hire","is","T"]]], 
				   "AND", 
				   ["custrecord_frf_details_start_date","onorafter",d_today], 
				   "AND", 
				   ["custrecord_frf_details_status_flag","anyof","1"]);
		   if(isArrayNotEmpty(s_filters.region))
				{
					filters.push('AND');
					filters.push(["custrecord_frf_details_account.custentity_region","anyof",s_filters.region]);
				}
				if(isArrayNotEmpty(s_filters.practice))
				{
					filters.push('AND');
					filters.push(["custrecord_frf_details_res_practice","anyof",s_filters.practice]);
				}
				if(isArrayNotEmpty(s_filters.account))
				{
					filters.push('AND');
					filters.push(["custrecord_frf_details_account","anyof",s_filters.account]);
				}
				if(isArrayNotEmpty(s_filters.project))
				{
					filters.push('AND');
					filters.push(["custrecord_frf_details_project","anyof",s_filters.project],"OR",["custrecord_frf_details_opp_id","anyof",s_filters.project]);
				}
				
		}
		else if(i_region)
		{
			 
			/*filters = [
				["custrecord_frf_details_account.custentity_region","anyof",i_region],
				"AND",
                ["isinactive","is","F"], 
			   "AND", 
			   ["custrecord_frf_details_project","noneof","@NONE@"], 
			   "AND", 
			   ["custrecord_frf_details_opp_id","anyof","@NONE@"], 
			   "AND", 
			   ["custrecord_frf_details_project.status","noneof","1"], 
			   "AND", 
			   ["custrecord_frf_details_status","is","F"], 
			   "AND", 
			   [[["custrecord_frf_details_start_date","within",d_today,d_today_temp],"AND",["custrecord_frf_details_external_hire","is","F"]],"OR",[["formulanumeric: {custrecord_frf_details_availabledate} - {custrecord_frf_details_start_date}","greaterthan","0"],"AND",["custrecord_frf_details_external_hire","is","T"]]], 
			   "AND", 
			   ["custrecord_frf_details_start_date","onorafter",d_today], 
			   "AND", 
			   ["custrecord_frf_details_status_flag","anyof","1"]]*/
   		filters.push(["custrecord_frf_details_account.custentity_region","anyof",i_region],
				"AND",
                ["isinactive","is","F"], 
			   "AND", 
			   ["custrecord_frf_details_project","noneof","@NONE@"], 
			   "AND", 
			   ["custrecord_frf_details_opp_id","anyof","@NONE@"], 
			   "AND", 
			   ["custrecord_frf_details_project.status","noneof","1"], 
			   "AND", 
			   ["custrecord_frf_details_status","is","F"], 
			   "AND", 
			   [[["custrecord_frf_details_start_date","within",d_today,d_today_temp],"AND",["custrecord_frf_details_external_hire","is","F"]],"OR",[["formulanumeric: {custrecord_frf_details_availabledate} - {custrecord_frf_details_start_date}","greaterthan","0"],"AND",["custrecord_frf_details_external_hire","is","T"]]], 
			   "AND", 
			   ["custrecord_frf_details_start_date","onorafter",d_today], 
			   "AND", 
			   ["custrecord_frf_details_status_flag","anyof","1"]);
   		if(isArrayNotEmpty(s_filters.region))
				{
					filters.push('AND');
					filters.push(["custrecord_frf_details_account.custentity_region","anyof",s_filters.region]);
				}
				if(isArrayNotEmpty(s_filters.practice))
				{
					filters.push('AND');
					filters.push(["custrecord_frf_details_res_practice","anyof",s_filters.practice]);
				}
				if(isArrayNotEmpty(s_filters.account))
				{
					filters.push('AND');
					filters.push(["custrecord_frf_details_account","anyof",s_filters.account]);
				}
				if(isArrayNotEmpty(s_filters.project))
				{
					filters.push('AND');
					filters.push(["custrecord_frf_details_project","anyof",s_filters.project],"OR",["custrecord_frf_details_opp_id","anyof",s_filters.project]);
				}
		}
		else if(i_user)
		{
			/*filters = [
				   [[["custrecord_frf_details_project.custentity_projectmanager","anyof",i_user],
				   "OR",
				   ["custrecord_frf_details_project.custentity_deliverymanager","anyof",i_user],
				    "OR",
				   ["custrecord_frf_details_project.custentity_clientpartner","anyof",i_user]],
				   "AND",
				   ["isinactive","is","F"], 
				   "AND", 
				   ["custrecord_frf_details_project","noneof","@NONE@"], 
				   "AND", 
				   ["custrecord_frf_details_opp_id","anyof","@NONE@"], 
				   "AND", 
				   ["custrecord_frf_details_project.status","noneof","1"], 
				   "AND", 
				   ["custrecord_frf_details_status","is","F"], 
				   "AND", 
				   [[["custrecord_frf_details_start_date","within",d_today,d_today_temp],"AND",["custrecord_frf_details_external_hire","is","F"]],"OR",[["formulanumeric: {custrecord_frf_details_availabledate} - {custrecord_frf_details_start_date}","greaterthan","0"],"AND",["custrecord_frf_details_external_hire","is","T"]]], 
				   "AND", 
				   ["custrecord_frf_details_start_date","onorafter",d_today], 
				   "AND", 
				   ["custrecord_frf_details_status_flag","anyof","1"]],
				   "OR",
				   [[["custrecord_frf_details_opp_id.custrecord_sfdc_opp_pm","anyof",i_user],
				   "OR",
				   ["custrecord_frf_details_opp_id.custrecord_sfdc_opportunity_dm","anyof",i_user],
				    "OR",
				   ["custrecord_frf_details_opp_id.custrecord_sfdc_client_partner","anyof",i_user]],
				   "AND",
				    ["isinactive","is","F"], 
				   "AND", 
				   ["custrecord_frf_details_project","noneof","@NONE@"], 
				   "AND", 
				   ["custrecord_frf_details_opp_id","anyof","@NONE@"], 
				   "AND", 
				   ["custrecord_frf_details_project.status","noneof","1"], 
				   "AND", 
				   ["custrecord_frf_details_status","is","F"], 
				   "AND", 
				   [[["custrecord_frf_details_start_date","within",d_today,d_today_temp],"AND",["custrecord_frf_details_external_hire","is","F"]],"OR",[["formulanumeric: {custrecord_frf_details_availabledate} - {custrecord_frf_details_start_date}","greaterthan","0"],"AND",["custrecord_frf_details_external_hire","is","T"]]], 
				   "AND", 
				   ["custrecord_frf_details_start_date","onorafter",d_today], 
				   "AND", 
				   ["custrecord_frf_details_status_flag","anyof","1"]]
				   ]*/

		   filters.push([[["custrecord_frf_details_project.custentity_projectmanager","anyof",i_user],
				   "OR",
				   ["custrecord_frf_details_project.custentity_deliverymanager","anyof",i_user],
				    "OR",
				   ["custrecord_frf_details_project.custentity_clientpartner","anyof",i_user]],
				   "AND",
				   ["isinactive","is","F"], 
				   "AND", 
				   ["custrecord_frf_details_project","noneof","@NONE@"], 
				   "AND", 
				   ["custrecord_frf_details_opp_id","anyof","@NONE@"], 
				   "AND", 
				   ["custrecord_frf_details_project.status","noneof","1"], 
				   "AND", 
				   ["custrecord_frf_details_status","is","F"], 
				   "AND", 
				   [[["custrecord_frf_details_start_date","within",d_today,d_today_temp],"AND",["custrecord_frf_details_external_hire","is","F"]],"OR",[["formulanumeric: {custrecord_frf_details_availabledate} - {custrecord_frf_details_start_date}","greaterthan","0"],"AND",["custrecord_frf_details_external_hire","is","T"]]], 
				   "AND", 
				   ["custrecord_frf_details_start_date","onorafter",d_today], 
				   "AND", 
				   ["custrecord_frf_details_status_flag","anyof","1"]]);
		   filters.push('OR');
		   filters.push([[["custrecord_frf_details_opp_id.custrecord_sfdc_opp_pm","anyof",i_user],
				   "OR",
				   ["custrecord_frf_details_opp_id.custrecord_sfdc_opportunity_dm","anyof",i_user],
				    "OR",
				   ["custrecord_frf_details_opp_id.custrecord_sfdc_client_partner","anyof",i_user]],
				   "AND",
				    ["isinactive","is","F"], 
				   "AND", 
				   ["custrecord_frf_details_project","noneof","@NONE@"], 
				   "AND", 
				   ["custrecord_frf_details_opp_id","anyof","@NONE@"], 
				   "AND", 
				   ["custrecord_frf_details_project.status","noneof","1"], 
				   "AND", 
				   ["custrecord_frf_details_status","is","F"], 
				   "AND", 
				   [[["custrecord_frf_details_start_date","within",d_today,d_today_temp],"AND",["custrecord_frf_details_external_hire","is","F"]],"OR",[["formulanumeric: {custrecord_frf_details_availabledate} - {custrecord_frf_details_start_date}","greaterthan","0"],"AND",["custrecord_frf_details_external_hire","is","T"]]], 
				   "AND", 
				   ["custrecord_frf_details_start_date","onorafter",d_today], 
				   "AND", 
				   ["custrecord_frf_details_status_flag","anyof","1"]]);
		   if(isArrayNotEmpty(s_filters.region))
				{
					filters.push('AND');
					filters.push(["custrecord_frf_details_account.custentity_region","anyof",s_filters.region]);
				}
				if(isArrayNotEmpty(s_filters.practice))
				{
					filters.push('AND');
					filters.push(["custrecord_frf_details_res_practice","anyof",s_filters.practice]);
				}
				if(isArrayNotEmpty(s_filters.account))
				{
					filters.push('AND');
					filters.push(["custrecord_frf_details_account","anyof",s_filters.account]);
				}
				if(isArrayNotEmpty(s_filters.project))
				{
					filters.push('AND');
					filters.push(["custrecord_frf_details_project","anyof",s_filters.project],"OR",["custrecord_frf_details_opp_id","anyof",s_filters.project]);
				}
		}
		//nlapiLogExecution('debug','Filters &&&&&#### = ',filters);
		columns = [
		 new nlobjSearchColumn("custrecord_frf_details_project",null,"GROUP")
		]
		var customrecord_frf_detailsSearch = searchRecord("customrecord_frf_details", null, filters, columns,null)	
		if(customrecord_frf_detailsSearch) 
		{
			var dataArray = new Array();
			//for(var i=0; i<customrecord_frf_detailsSearch.length;i++)
			//{
			//	dataArray.push(customrecord_frf_detailsSearch[i].getValue("custrecord_frf_details_project",null,"GROUP"))
			//}
			return customrecord_frf_detailsSearch.length;
		}
		else
		{
			var dataArray = [];
			return dataArray;
		}
	}
	catch(error)
	{
		return error;
	}
}


//===================================================================================================================
function ProjectPositionsAtRiskCount(i_user, practiceId, isAdmin,i_region,s_filters)
{
	try
	{
		var filters = new Array();
		var columns = new Array();
		var d_today_temp = nlapiAddDays(d_today, 20)
		s_filters = JSON.parse(s_filters);
		if(isAdmin == true)
		{
			/*filters =  [  ["isinactive","is","F"], 
						   "AND", 
						   ["custrecord_frf_details_project","noneof","@NONE@"], 
						   "AND", 
						   ["custrecord_frf_details_opp_id","anyof","@NONE@"], 
						   "AND", 
						   ["custrecord_frf_details_project.status","noneof","1"], 
						   "AND", 
						   ["custrecord_frf_details_status","is","F"], 
						   "AND", 
						   [[["custrecord_frf_details_start_date","within",d_today,d_today_temp],"AND",["custrecord_frf_details_external_hire","is","F"]],"OR",[["formulanumeric: {custrecord_frf_details_availabledate} - {custrecord_frf_details_start_date}","greaterthan","0"],"AND",["custrecord_frf_details_external_hire","is","T"]]], 
						   "AND", 
						   ["custrecord_frf_details_start_date","onorafter",d_today], 
						   "AND", 
						   ["custrecord_frf_details_status_flag","anyof","1"]]*/
			   filters.push( ["isinactive","is","F"], 
						   "AND", 
						   ["custrecord_frf_details_project","noneof","@NONE@"], 
						   "AND", 
						   ["custrecord_frf_details_opp_id","anyof","@NONE@"], 
						   "AND", 
						   ["custrecord_frf_details_project.status","noneof","1"], 
						   "AND", 
						   ["custrecord_frf_details_status","is","F"], 
						   "AND", 
						   [[["custrecord_frf_details_start_date","within",d_today,d_today_temp],"AND",["custrecord_frf_details_external_hire","is","F"]],"OR",[["formulanumeric: {custrecord_frf_details_availabledate} - {custrecord_frf_details_start_date}","greaterthan","0"],"AND",["custrecord_frf_details_external_hire","is","T"]]], 
						   "AND", 
						   ["custrecord_frf_details_start_date","onorafter",d_today], 
						   "AND", 
						   ["custrecord_frf_details_status_flag","anyof","1"]);
			   if(isArrayNotEmpty(s_filters.region))
				{
					filters.push('AND');
					filters.push(["custrecord_frf_details_account.custentity_region","anyof",s_filters.region]);
				}
				if(isArrayNotEmpty(s_filters.practice))
				{
					filters.push('AND');
					filters.push(["custrecord_frf_details_res_practice","anyof",s_filters.practice]);
				}
				if(isArrayNotEmpty(s_filters.account))
				{
					filters.push('AND');
					filters.push(["custrecord_frf_details_account","anyof",s_filters.account]);
				}
				if(isArrayNotEmpty(s_filters.project))
				{
					filters.push('AND');
					filters.push(["custrecord_frf_details_project","anyof",s_filters.project],"OR",["custrecord_frf_details_opp_id","anyof",s_filters.project]);
				}
		}
		else if(practiceId)
		{
			var practiceArray = getSubPractices(practiceId);	
			/*filters = [
				   ["custrecord_frf_details_res_practice","anyof",practiceArray],
				   "AND",
				   ["isinactive","is","F"], 
				   "AND", 
				   ["custrecord_frf_details_project","noneof","@NONE@"], 
				   "AND", 
				   ["custrecord_frf_details_opp_id","anyof","@NONE@"], 
				   "AND", 
				   ["custrecord_frf_details_project.status","noneof","1"], 
				   "AND", 
				   ["custrecord_frf_details_status","is","F"], 
				   "AND", 
				   [[["custrecord_frf_details_start_date","within",d_today,d_today_temp],"AND",["custrecord_frf_details_external_hire","is","F"]],"OR",[["formulanumeric: {custrecord_frf_details_availabledate} - {custrecord_frf_details_start_date}","greaterthan","0"],"AND",["custrecord_frf_details_external_hire","is","T"]]], 
				   "AND", 
				   ["custrecord_frf_details_start_date","onorafter",d_today], 
				   "AND", 
				   ["custrecord_frf_details_status_flag","anyof","1"]]*/
			filters.push( ["custrecord_frf_details_res_practice","anyof",practiceArray],
				   "AND",
				   ["isinactive","is","F"], 
				   "AND", 
				   ["custrecord_frf_details_project","noneof","@NONE@"], 
				   "AND", 
				   ["custrecord_frf_details_opp_id","anyof","@NONE@"], 
				   "AND", 
				   ["custrecord_frf_details_project.status","noneof","1"], 
				   "AND", 
				   ["custrecord_frf_details_status","is","F"], 
				   "AND", 
				   [[["custrecord_frf_details_start_date","within",d_today,d_today_temp],"AND",["custrecord_frf_details_external_hire","is","F"]],"OR",[["formulanumeric: {custrecord_frf_details_availabledate} - {custrecord_frf_details_start_date}","greaterthan","0"],"AND",["custrecord_frf_details_external_hire","is","T"]]], 
				   "AND", 
				   ["custrecord_frf_details_start_date","onorafter",d_today], 
				   "AND", 
				   ["custrecord_frf_details_status_flag","anyof","1"]);
			if(isArrayNotEmpty(s_filters.region))
				{
					filters.push('AND');
					filters.push(["custrecord_frf_details_account.custentity_region","anyof",s_filters.region]);
				}
				if(isArrayNotEmpty(s_filters.practice))
				{
					filters.push('AND');
					filters.push(["custrecord_frf_details_res_practice","anyof",s_filters.practice]);
				}
				if(isArrayNotEmpty(s_filters.account))
				{
					filters.push('AND');
					filters.push(["custrecord_frf_details_account","anyof",s_filters.account]);
				}
				if(isArrayNotEmpty(s_filters.project))
				{
					filters.push('AND');
					filters.push(["custrecord_frf_details_project","anyof",s_filters.project],"OR",["custrecord_frf_details_opp_id","anyof",s_filters.project]);
				}
				
		}
		else if(i_region)
		{
			 
			/*filters = [
				["custrecord_frf_details_account.custentity_region","anyof",i_region],
				"AND",
                ["isinactive","is","F"], 
			   "AND", 
			   ["custrecord_frf_details_project","noneof","@NONE@"], 
			   "AND", 
			   ["custrecord_frf_details_opp_id","anyof","@NONE@"], 
			   "AND", 
			   ["custrecord_frf_details_project.status","noneof","1"], 
			   "AND", 
			   ["custrecord_frf_details_status","is","F"], 
			   "AND", 
			   [[["custrecord_frf_details_start_date","within",d_today,d_today_temp],"AND",["custrecord_frf_details_external_hire","is","F"]],"OR",[["formulanumeric: {custrecord_frf_details_availabledate} - {custrecord_frf_details_start_date}","greaterthan","0"],"AND",["custrecord_frf_details_external_hire","is","T"]]], 
			   "AND", 
			   ["custrecord_frf_details_start_date","onorafter",d_today], 
			   "AND", 
			   ["custrecord_frf_details_status_flag","anyof","1"]]*/
		filters.push(["custrecord_frf_details_account.custentity_region","anyof",i_region],
				"AND",
                ["isinactive","is","F"], 
			   "AND", 
			   ["custrecord_frf_details_project","noneof","@NONE@"], 
			   "AND", 
			   ["custrecord_frf_details_opp_id","anyof","@NONE@"], 
			   "AND", 
			   ["custrecord_frf_details_project.status","noneof","1"], 
			   "AND", 
			   ["custrecord_frf_details_status","is","F"], 
			   "AND", 
			   [[["custrecord_frf_details_start_date","within",d_today,d_today_temp],"AND",["custrecord_frf_details_external_hire","is","F"]],"OR",[["formulanumeric: {custrecord_frf_details_availabledate} - {custrecord_frf_details_start_date}","greaterthan","0"],"AND",["custrecord_frf_details_external_hire","is","T"]]], 
			   "AND", 
			   ["custrecord_frf_details_start_date","onorafter",d_today], 
			   "AND", 
			   ["custrecord_frf_details_status_flag","anyof","1"]);
		if(isArrayNotEmpty(s_filters.region))
				{
					filters.push('AND');
					filters.push(["custrecord_frf_details_account.custentity_region","anyof",s_filters.region]);
				}
				if(isArrayNotEmpty(s_filters.practice))
				{
					filters.push('AND');
					filters.push(["custrecord_frf_details_res_practice","anyof",s_filters.practice]);
				}
				if(isArrayNotEmpty(s_filters.account))
				{
					filters.push('AND');
					filters.push(["custrecord_frf_details_account","anyof",s_filters.account]);
				}
				if(isArrayNotEmpty(s_filters.project))
				{
					filters.push('AND');
					filters.push(["custrecord_frf_details_project","anyof",s_filters.project],"OR",["custrecord_frf_details_opp_id","anyof",s_filters.project]);
				}
		}
		else if(i_user)
		{
			/*filters = [
				   [[["custrecord_frf_details_project.custentity_projectmanager","anyof",i_user],
				   "OR",
				   ["custrecord_frf_details_project.custentity_deliverymanager","anyof",i_user],
				    "OR",
				   ["custrecord_frf_details_project.custentity_clientpartner","anyof",i_user]],
				   "AND",
				   ["isinactive","is","F"], 
				   "AND", 
				   ["custrecord_frf_details_project","noneof","@NONE@"], 
				   "AND", 
				   ["custrecord_frf_details_opp_id","anyof","@NONE@"], 
				   "AND", 
				   ["custrecord_frf_details_project.status","noneof","1"], 
				   "AND", 
				   ["custrecord_frf_details_status","is","F"], 
				   "AND", 
				   [[["custrecord_frf_details_start_date","within",d_today,d_today_temp],"AND",["custrecord_frf_details_external_hire","is","F"]],"OR",[["formulanumeric: {custrecord_frf_details_availabledate} - {custrecord_frf_details_start_date}","greaterthan","0"],"AND",["custrecord_frf_details_external_hire","is","T"]]], 
				   "AND", 
				   ["custrecord_frf_details_start_date","onorafter",d_today], 
				   "AND", 
				   ["custrecord_frf_details_status_flag","anyof","1"]],
				   "OR",
				   [[["custrecord_frf_details_opp_id.custrecord_sfdc_opp_pm","anyof",i_user],
				   "OR",
				   ["custrecord_frf_details_opp_id.custrecord_sfdc_opportunity_dm","anyof",i_user],
				    "OR",
				   ["custrecord_frf_details_opp_id.custrecord_sfdc_client_partner","anyof",i_user]],
				   "AND",
				    ["isinactive","is","F"], 
				   "AND", 
				   ["custrecord_frf_details_project","noneof","@NONE@"], 
				   "AND", 
				   ["custrecord_frf_details_opp_id","anyof","@NONE@"], 
				   "AND", 
				   ["custrecord_frf_details_project.status","noneof","1"], 
				   "AND", 
				   ["custrecord_frf_details_status","is","F"], 
				   "AND", 
				   [[["custrecord_frf_details_start_date","within",d_today,d_today_temp],"AND",["custrecord_frf_details_external_hire","is","F"]],"OR",[["formulanumeric: {custrecord_frf_details_availabledate} - {custrecord_frf_details_start_date}","greaterthan","0"],"AND",["custrecord_frf_details_external_hire","is","T"]]], 
				   "AND", 
				   ["custrecord_frf_details_start_date","onorafter",d_today], 
				   "AND", 
				   ["custrecord_frf_details_status_flag","anyof","1"]]
				   ]*/
		   filters.push([[["custrecord_frf_details_project.custentity_projectmanager","anyof",i_user],
				   "OR",
				   ["custrecord_frf_details_project.custentity_deliverymanager","anyof",i_user],
				    "OR",
				   ["custrecord_frf_details_project.custentity_clientpartner","anyof",i_user]],
				   "AND",
				   ["isinactive","is","F"], 
				   "AND", 
				   ["custrecord_frf_details_project","noneof","@NONE@"], 
				   "AND", 
				   ["custrecord_frf_details_opp_id","anyof","@NONE@"], 
				   "AND", 
				   ["custrecord_frf_details_project.status","noneof","1"], 
				   "AND", 
				   ["custrecord_frf_details_status","is","F"], 
				   "AND", 
				   [[["custrecord_frf_details_start_date","within",d_today,d_today_temp],"AND",["custrecord_frf_details_external_hire","is","F"]],"OR",[["formulanumeric: {custrecord_frf_details_availabledate} - {custrecord_frf_details_start_date}","greaterthan","0"],"AND",["custrecord_frf_details_external_hire","is","T"]]], 
				   "AND", 
				   ["custrecord_frf_details_start_date","onorafter",d_today], 
				   "AND", 
				   ["custrecord_frf_details_status_flag","anyof","1"]]);
		   filters.push('OR');
		   filters.push([[["custrecord_frf_details_opp_id.custrecord_sfdc_opp_pm","anyof",i_user],
				   "OR",
				   ["custrecord_frf_details_opp_id.custrecord_sfdc_opportunity_dm","anyof",i_user],
				    "OR",
				   ["custrecord_frf_details_opp_id.custrecord_sfdc_client_partner","anyof",i_user]],
				   "AND",
				    ["isinactive","is","F"], 
				   "AND", 
				   ["custrecord_frf_details_project","noneof","@NONE@"], 
				   "AND", 
				   ["custrecord_frf_details_opp_id","anyof","@NONE@"], 
				   "AND", 
				   ["custrecord_frf_details_project.status","noneof","1"], 
				   "AND", 
				   ["custrecord_frf_details_status","is","F"], 
				   "AND", 
				   [[["custrecord_frf_details_start_date","within",d_today,d_today_temp],"AND",["custrecord_frf_details_external_hire","is","F"]],"OR",[["formulanumeric: {custrecord_frf_details_availabledate} - {custrecord_frf_details_start_date}","greaterthan","0"],"AND",["custrecord_frf_details_external_hire","is","T"]]], 
				   "AND", 
				   ["custrecord_frf_details_start_date","onorafter",d_today], 
				   "AND", 
				   ["custrecord_frf_details_status_flag","anyof","1"]]);
		   if(isArrayNotEmpty(s_filters.region))
				{
					filters.push('AND');
					filters.push(["custrecord_frf_details_account.custentity_region","anyof",s_filters.region]);
				}
				if(isArrayNotEmpty(s_filters.practice))
				{
					filters.push('AND');
					filters.push(["custrecord_frf_details_res_practice","anyof",s_filters.practice]);
				}
				if(isArrayNotEmpty(s_filters.account))
				{
					filters.push('AND');
					filters.push(["custrecord_frf_details_account","anyof",s_filters.account]);
				}
				if(isArrayNotEmpty(s_filters.project))
				{
					filters.push('AND');
					filters.push(["custrecord_frf_details_project","anyof",s_filters.project],"OR",["custrecord_frf_details_opp_id","anyof",s_filters.project]);
				}
		}
		columns = [
				new nlobjSearchColumn("custrecord_frf_details_project",null,"GROUP"), 
				new nlobjSearchColumn("internalid",null,"COUNT")
		]
		var customrecord_frf_detailsSearch = searchRecord("customrecord_frf_details", null, filters, columns,null)	
		if(customrecord_frf_detailsSearch) 
		{
			var dataArray = new Array();
			var i_tempCount = 0;
			for(var i=0; i<customrecord_frf_detailsSearch.length;i++)
			{
				var i_positions = customrecord_frf_detailsSearch[i].getValue("internalid",null,"COUNT");
				i_tempCount+=parseInt(i_positions);
			}
			//return customrecord_frf_detailsSearch[0].getValue("internalid",null,"COUNT");
			return i_tempCount;
		}
		else
		{
			var dataArray = [];
			return dataArray;
		}
	}
	catch(error)
	{
		return error;
	}
}

//===================================================================================================================
function LeakageAmount(i_user, practiceId, isAdmin,i_region,s_filters)
{
	try
	{
		var filters = new Array();
		var columns = new Array();
		//var d_today = new Date();
		var preDate = nlapiAddMonths(d_today,-3);
		var i_month = preDate.getMonth();
		var d_today_temp = nlapiAddDays(d_today,-1);
		//nlapiLogExecution('debug','d_today ',d_today);
		var startOfMonth = getFirstDate(i_month);
		//nlapiLogExecution('debug','startOfMonth ',startOfMonth);
		s_filters = JSON.parse(s_filters);
		if(isAdmin == true)
		{
			   filters.push(["isinactive","is","F"], 
					   "AND", 
					   ["custrecord_frf_details_status_flag","anyof","1","3"], 
					   "AND", 
					   ["custrecord_frf_details_status","is","F"], 
					   "AND", 
					   ["custrecord_frf_details_start_date","within",startOfMonth,nlapiDateToString(d_today_temp)],
					   "AND",
					   ["custrecord_frf_details_opp_id.custrecord_stage_sfdc","noneof","8","9"],
					   "AND",
					   ["custrecord_frf_details_bill_rate","isnotempty",""],
					   "AND",
					   [["custrecord_frf_details_project.type","anyof","2"],"OR",["custrecord_frf_details_opp_id","noneof","@NONE@"]]);
					    if(isArrayNotEmpty(s_filters.region))
						{
							filters.push('AND');
							filters.push(["custrecord_frf_details_account.custentity_region","anyof",s_filters.region]);
						}
						if(isArrayNotEmpty(s_filters.practice))
						{
							filters.push('AND');
							filters.push(["custrecord_frf_details_res_practice","anyof",s_filters.practice]);
						}
						if(isArrayNotEmpty(s_filters.account))
						{
							filters.push('AND');
							filters.push(["custrecord_frf_details_account","anyof",s_filters.account]);
						}
						if(isArrayNotEmpty(s_filters.project))
						{
							filters.push('AND');
							filters.push(["custrecord_frf_details_project","anyof",s_filters.project],"OR",["custrecord_frf_details_opp_id","anyof",s_filters.project]);
						}
		}
		else if(practiceId)
		{
			var practiceArray = getSubPractices(practiceId);
			/*filters = [
				   ["custrecord_frf_details_res_practice","anyof",practiceArray],
				   "AND",
					["isinactive","is","F"], 
				   "AND", 
				   ["custrecord_frf_details_status_flag","anyof","1","3"], 
				   "AND", 
				   ["custrecord_frf_details_status","is","F"], 
				   "AND", 
				   ["custrecord_frf_details_start_date","within",startOfMonth,nlapiDateToString(d_today_temp)],
				   "AND",
				   ["custrecord_frf_details_opp_id.custrecord_stage_sfdc","noneof","8","9"],
				   "AND",
				   ["custrecord_frf_details_bill_rate","isnotempty",""],
				   "AND",
				   [["custrecord_frf_details_project.type","anyof","2"],
				   "OR",
				   ["custrecord_frf_details_opp_id","noneof","@NONE@"]]]*/
		   filters.push( ["custrecord_frf_details_res_practice","anyof",practiceArray],
				   "AND",
					["isinactive","is","F"], 
				   "AND", 
				   ["custrecord_frf_details_status_flag","anyof","1","3"], 
				   "AND", 
				   ["custrecord_frf_details_status","is","F"], 
				   "AND", 
				   ["custrecord_frf_details_start_date","within",startOfMonth,nlapiDateToString(d_today_temp)],
				   "AND",
				   ["custrecord_frf_details_opp_id.custrecord_stage_sfdc","noneof","8","9"],
				   "AND",
				   ["custrecord_frf_details_bill_rate","isnotempty",""],
				   "AND",
				   [["custrecord_frf_details_project.type","anyof","2"],
				   "OR",
				   ["custrecord_frf_details_opp_id","noneof","@NONE@"]]);
		    	if(isArrayNotEmpty(s_filters.region))
				{
					filters.push('AND');
					filters.push(["custrecord_frf_details_account.custentity_region","anyof",s_filters.region]);
				}
				if(isArrayNotEmpty(s_filters.practice))
				{
					filters.push('AND');
					filters.push(["custrecord_frf_details_res_practice","anyof",s_filters.practice]);
				}
				if(isArrayNotEmpty(s_filters.account))
				{
					filters.push('AND');
					filters.push(["custrecord_frf_details_account","anyof",s_filters.account]);
				}
				if(isArrayNotEmpty(s_filters.project))
				{
					filters.push('AND');
					filters.push(["custrecord_frf_details_project","anyof",s_filters.project],"OR",["custrecord_frf_details_opp_id","anyof",s_filters.project]);
				}
				
		}
		else if(i_region)
		{
			 
			/*filters = [
				["custrecord_frf_details_account.custentity_region","anyof",i_region],
				"AND",
                ["isinactive","is","F"], 
			   "AND", 
			   ["custrecord_frf_details_status_flag","anyof","1","3"], 
			   "AND", 
			   ["custrecord_frf_details_status","is","F"], 
			   "AND", 
			   ["custrecord_frf_details_start_date","within",startOfMonth,nlapiDateToString(d_today_temp)],
			   "AND",
			   ["custrecord_frf_details_opp_id.custrecord_stage_sfdc","noneof","8","9"],
			   "AND",
			   ["custrecord_frf_details_bill_rate","isnotempty",""],
			   "AND",
			   [["custrecord_frf_details_project.type","anyof","2"],
			   "OR",
			   ["custrecord_frf_details_opp_id","noneof","@NONE@"]]]*/
	   filters.push(["custrecord_frf_details_account.custentity_region","anyof",i_region],
				"AND",
                ["isinactive","is","F"], 
			   "AND", 
			   ["custrecord_frf_details_status_flag","anyof","1","3"], 
			   "AND", 
			   ["custrecord_frf_details_status","is","F"], 
			   "AND", 
			   ["custrecord_frf_details_start_date","within",startOfMonth,nlapiDateToString(d_today_temp)],
			   "AND",
			   ["custrecord_frf_details_opp_id.custrecord_stage_sfdc","noneof","8","9"],
			   "AND",
			   ["custrecord_frf_details_bill_rate","isnotempty",""],
			   "AND",
			   [["custrecord_frf_details_project.type","anyof","2"],
			   "OR",
			   ["custrecord_frf_details_opp_id","noneof","@NONE@"]]);
	    	if(isArrayNotEmpty(s_filters.region))
				{
					filters.push('AND');
					filters.push(["custrecord_frf_details_account.custentity_region","anyof",s_filters.region]);
				}
				if(isArrayNotEmpty(s_filters.practice))
				{
					filters.push('AND');
					filters.push(["custrecord_frf_details_res_practice","anyof",s_filters.practice]);
				}
				if(isArrayNotEmpty(s_filters.account))
				{
					filters.push('AND');
					filters.push(["custrecord_frf_details_account","anyof",s_filters.account]);
				}
				if(isArrayNotEmpty(s_filters.project))
				{
					filters.push('AND');
					filters.push(["custrecord_frf_details_project","anyof",s_filters.project],"OR",["custrecord_frf_details_opp_id","anyof",s_filters.project]);
				}
		}
		else if(i_user)
		{
			//nlapiLogExecution('audit','&&&&&*********',i_user)
			/*filters = [
				   [[["custrecord_frf_details_project.custentity_projectmanager","anyof",i_user],
				   "OR",
				   ["custrecord_frf_details_project.custentity_deliverymanager","anyof",i_user],
				    "OR",
				   ["custrecord_frf_details_project.custentity_clientpartner","anyof",i_user]],
				   "AND",
				   ["isinactive","is","F"], 
				   "AND", 
				   ["custrecord_frf_details_status_flag","anyof","1","3"], 
				   "AND", 
				   ["custrecord_frf_details_status","is","F"], 
				   "AND", 
				   ["custrecord_frf_details_start_date","within",startOfMonth,nlapiDateToString(d_today_temp)],
				   "AND",
				   ["custrecord_frf_details_opp_id.custrecord_stage_sfdc","noneof","8","9"],
				   "AND",
				   ["custrecord_frf_details_bill_rate","isnotempty",""],
				   "AND",
				   [["custrecord_frf_details_project.type","anyof","2"],"OR",["custrecord_frf_details_opp_id","noneof","@NONE@"]]],
				   "OR",
				   [[["custrecord_frf_details_opp_id.custrecord_sfdc_opp_pm","anyof",i_user],
				   "OR",
				   ["custrecord_frf_details_opp_id.custrecord_sfdc_opportunity_dm","anyof",i_user],
				    "OR",
				   ["custrecord_frf_details_opp_id.custrecord_sfdc_client_partner","anyof",i_user]],
				   "AND",
				   ["isinactive","is","F"], 
				   "AND", 
				   ["custrecord_frf_details_status_flag","anyof","1","3"], 
				   "AND", 
				   ["custrecord_frf_details_status","is","F"], 
				   "AND", 
				   ["custrecord_frf_details_start_date","within",startOfMonth,nlapiDateToString(d_today_temp)],
				   "AND",
				   ["custrecord_frf_details_opp_id.custrecord_stage_sfdc","noneof","8","9"],
				   "AND",
				   ["custrecord_frf_details_bill_rate","isnotempty",""],
				   "AND",
				   [["custrecord_frf_details_project.type","anyof","2"],"OR",["custrecord_frf_details_opp_id","noneof","@NONE@"]]]
				   ]*/
		   filters.push([[["custrecord_frf_details_project.custentity_projectmanager","anyof",i_user],
				   "OR",
				   ["custrecord_frf_details_project.custentity_deliverymanager","anyof",i_user],
				    "OR",
				   ["custrecord_frf_details_project.custentity_clientpartner","anyof",i_user]],
				   "AND",
				   ["isinactive","is","F"], 
				   "AND", 
				   ["custrecord_frf_details_status_flag","anyof","1","3"], 
				   "AND", 
				   ["custrecord_frf_details_status","is","F"], 
				   "AND", 
				   ["custrecord_frf_details_start_date","within",startOfMonth,nlapiDateToString(d_today_temp)],
				   "AND",
				   ["custrecord_frf_details_opp_id.custrecord_stage_sfdc","noneof","8","9"],
				   "AND",
				   ["custrecord_frf_details_bill_rate","isnotempty",""],
				   "AND",
				   [["custrecord_frf_details_project.type","anyof","2"],"OR",["custrecord_frf_details_opp_id","noneof","@NONE@"]]]);
		   filters.push('OR');
		   filters.push([[["custrecord_frf_details_opp_id.custrecord_sfdc_opp_pm","anyof",i_user],
				   "OR",
				   ["custrecord_frf_details_opp_id.custrecord_sfdc_opportunity_dm","anyof",i_user],
				    "OR",
				   ["custrecord_frf_details_opp_id.custrecord_sfdc_client_partner","anyof",i_user]],
				   "AND",
				   ["isinactive","is","F"], 
				   "AND", 
				   ["custrecord_frf_details_status_flag","anyof","1","3"], 
				   "AND", 
				   ["custrecord_frf_details_status","is","F"], 
				   "AND", 
				   ["custrecord_frf_details_start_date","within",startOfMonth,nlapiDateToString(d_today_temp)],
				   "AND",
				   ["custrecord_frf_details_opp_id.custrecord_stage_sfdc","noneof","8","9"],
				   "AND",
				   ["custrecord_frf_details_bill_rate","isnotempty",""],
				   "AND",
				   [["custrecord_frf_details_project.type","anyof","2"],"OR",["custrecord_frf_details_opp_id","noneof","@NONE@"]]]);
		       if(isArrayNotEmpty(s_filters.region))
				{
					filters.push('AND');
					filters.push(["custrecord_frf_details_account.custentity_region","anyof",s_filters.region]);
				}
				if(isArrayNotEmpty(s_filters.practice))
				{
					filters.push('AND');
					filters.push(["custrecord_frf_details_res_practice","anyof",s_filters.practice]);
				}
				if(isArrayNotEmpty(s_filters.account))
				{
					filters.push('AND');
					filters.push(["custrecord_frf_details_account","anyof",s_filters.account]);
				}
				if(isArrayNotEmpty(s_filters.project))
				{
					filters.push('AND');
					filters.push(["custrecord_frf_details_project","anyof",s_filters.project],"OR",["custrecord_frf_details_opp_id","anyof",s_filters.project]);
				}
		}
		//nlapiLogExecution('audit','filters Leackage######## ',filters);
		columns = [
			//new nlobjSearchColumn("formulanumeric").setFormula("{today} - {custrecord_frf_details_start_date}"), 
			new nlobjSearchColumn("custrecord_frf_details_start_date"),
			new nlobjSearchColumn("custrecord_frf_details_bill_rate"),
			new nlobjSearchColumn("formulanumeric").setFormula("ROUND(((TO_CHAR({custrecord_frf_details_start_date}, 'J') - TO_CHAR({today}, 'J'))) + MOD(({custrecord_frf_details_start_date} - {today}), 1) - ((((TRUNC({custrecord_frf_details_start_date}, 'D')) - (TRUNC({today}, 'D')))/7)*2) - (CASE WHEN TO_CHAR({today}, 'DY') = 'SUN' THEN 1 ELSE 0 END) - (CASE WHEN TO_CHAR({custrecord_frf_details_start_date}, 'DY') = 'SAT' THEN 1 ELSE 0 END), 2)")
		]
		var customrecord_frf_detailsSearch = searchRecord("customrecord_frf_details", null, filters, columns,null)	
		if(customrecord_frf_detailsSearch) 
		{
			//nlapiLogExecution('debug','customrecord_frf_detailsSearch.length  $%#%$ ',customrecord_frf_detailsSearch.length);
			//var d_today = new Date();
			//d_today = nlapiAddDays(d_today,-1);
			var i_leakageAmt = 0;
			//var i_workingDays = customrecord_frf_detailsSearch[0].getAllColumns();
			var a_columns = customrecord_frf_detailsSearch[0].getAllColumns();
			// //nlapiLogExecution('debug','a_columns ***** ',customrecord_frf_detailsSearch[0].getAllColumns());
			// //nlapiLogExecution('debug','a_columns%%%% ',customrecord_frf_detailsSearch[0].getValue(a_columns[2]));
			for(var i=0; i<customrecord_frf_detailsSearch.length;i++) //customrecord_frf_detailsSearch.length
			{
				var d_startDate = customrecord_frf_detailsSearch[i].getValue("custrecord_frf_details_start_date");
				var i_billRate = customrecord_frf_detailsSearch[i].getValue("custrecord_frf_details_bill_rate");
				var i_workingDays = customrecord_frf_detailsSearch[i].getValue(a_columns[2]);
				////nlapiLogExecution('Debug','i_workingDays '+Math.round(i_workingDays),'Math.round(i_workingDays) '+i_workingDays);
				////nlapiLogExecution('Debug','i_billRate ',i_billRate);
				
				if(i_billRate && i_workingDays)
					i_leakageAmt =  parseFloat(i_leakageAmt) + parseFloat((-parseFloat(i_workingDays)) * parseFloat(i_billRate) * 8);
				
				////nlapiLogExecution('debug','i_leakageAmt ',i_leakageAmt);
			}
			return i_leakageAmt;
		}
		else
		{
			var dataArray = [];
			return dataArray;
		}
	}
	catch(error)
	{
		return error;
	}
}

//========================================================================
const _MS_PER_DAY = 1000 * 60 * 60 * 24;

// a and b are javascript Date objects
function dateDiffInDays(a, b) {
  // Discard the time and time-zone information.
  const utc1 = Date.UTC(a.getFullYear(), a.getMonth(), a.getDate());
  const utc2 = Date.UTC(b.getFullYear(), b.getMonth(), b.getDate());

  return Math.floor((utc2 - utc1) / _MS_PER_DAY);
}
//==================================================
// Calculate Business Days

function calcBusinessDays(d_startDate, d_endDate) { // input given as Date

 

        // objects

        var startDate = new Date(d_startDate.getTime());

        var endDate = new Date(d_endDate.getTime());

        // Validate input

        if (endDate < startDate)

               return 0;

 

        // Calculate days between dates

        var millisecondsPerDay = 86400 * 1000; // Day in milliseconds

        startDate.setHours(0, 0, 0, 1); // Start just after midnight

        endDate.setHours(23, 59, 59, 999); // End just before midnight

        var diff = endDate - startDate; // Milliseconds between datetime objects

        var days = Math.ceil(diff / millisecondsPerDay);

 

        // Subtract two weekend days for every week in between

        var weeks = Math.floor(days / 7);

        var days = days - (weeks * 2);

 

        // Handle special cases

        var startDay = startDate.getDay();

        var endDay = endDate.getDay();

 

        // Remove weekend not previously removed.

        if (startDay - endDay > 1)

               days = days - 2;

 

        // Remove start day if span starts on Sunday but ends before Saturday

        if (startDay == 0 && endDay != 6)

               days = days - 1

 

               // Remove end day if span ends on Saturday but starts after Sunday

        if (endDay == 6 && startDay != 0)

               days = days - 1

 

        return days;

 

}
//=================================================
	
function countWorkingDays(d_startDate,d_today)
{
	var i_weekends = getWeekend(d_startDate,d_today);
	////nlapiLogExecution('Debug','i_weekends ',i_weekends);
	////nlapiLogExecution('Debug','d_today '+d_today,'d_startDate '+d_startDate);
	var diffDays = d_today.getDate() - d_startDate.getDate(); 
	////nlapiLogExecution('debug','d_today.getDate() '+d_today.getDate(),'d_startDate.getDate() '+d_startDate.getDate());
	////nlapiLogExecution('debug','diffDays',diffDays);
	
	return parseInt(diffDays) - parseInt(i_weekends);
}

//================================================
function getWeekend(startDate, endDate) //
{
	var i_no_of_sat = 0;
	var i_no_of_sun = 0;
	
	
	var date_format = checkDateFormat();
	
	var startDate_1 = startDate
	var endDate_1 = endDate
	
	//startDate = nlapiStringToDate(startDate);
	//endDate = nlapiStringToDate(endDate);
	
	var i_count_day = startDate.getDate();
	
	var i_count_last_day = endDate.getDate();
	
	i_month = startDate.getMonth() + 1;
	
	i_year = startDate.getFullYear();
	
	var d_f = new Date();
	var getTot = getDatediffIndays(startDate_1, endDate_1); //Get total days in a month
	var sat = new Array(); //Declaring array for inserting Saturdays
	var sun = new Array(); //Declaring array for inserting Sundays
	
	for (var i = i_count_day; i <= i_count_last_day; i++) //
	{
		//looping through days in month
		if (date_format == 'YYYY-MM-DD') //
		{
			var newDate = i_year + '-' + i_month + '-' + i;
		}
		if (date_format == 'DD/MM/YYYY') //
		{
			var newDate = i + '/' + i_month + '/' + i_year;
		}
		if (date_format == 'MM/DD/YYYY') //
		{
			var newDate = i_month + '/' + i + '/' + i_year;
		}
		
		newDate = nlapiStringToDate(newDate);
		
		if (newDate.getDay() == 0) //
		{ //if Sunday
			sat.push(i);
			i_no_of_sat++;
		}
		if (newDate.getDay() == 6) //
		{ //if Saturday
			sun.push(i);
			i_no_of_sun++;
		}
	}
	
	var i_total_days_sat_sun = parseInt(i_no_of_sat) + parseInt(i_no_of_sun);
	////nlapiLogExecution('DEBUG', 'Check Date', '2 i_total_days_sat_sun : ' + i_total_days_sat_sun);
	
	return i_total_days_sat_sun;
}
function checkDateFormat()
{
    var context = nlapiGetContext();

	var dateFormatPref = context.getPreference('dateformat');

    return dateFormatPref;
}
function getDatediffIndays(startDate, endDate)
{
    var one_day=1000*60*60*24;
    var fromDate = startDate;
    var toDate=endDate

	var date1 = nlapiStringToDate(fromDate);

    var date2 = nlapiStringToDate(toDate);

    var date3=(date2-date1)/one_day;

    return (date3+1);
}


//=================================================================================
function graphData(i_user, practiceId, isAdmin,i_region,s_graphFilters,s_filters)
{
	nlapiLogExecution("AUDIT", "graphData :s_filters ", s_filters);
	//nlapiLogExecution('debug','i_user Graph function ',i_user);
	o_filters = JSON.parse(s_graphFilters);
	var s_month = o_filters.current;
	var monthTempArray = s_month.split('-'); 
	var month = monthTempArray[0].toUpperCase();
	var incNumber = parseInt(o_filters.incValue);
	var graphData = {};
	var bechArray = [];
	var projectPositionsArray = [];
	var mlPositionsArray = [];
	var revenueBooked = [];
	var revenueML = [];
	var date = new Date();
	var monthNumber = month_array.indexOf(month)//date.getMonth();
	nlapiLogExecution("AUDIT", "graphData :monthNumber ", monthNumber);
	monthNumber = parseInt(monthNumber);
	//nlapiLogExecution('debug','MonthNumber ',monthNumber);
	for(var i = 0 ; i <incNumber ; i++){
		var count = 0;
		var firstDate = getFirstDate(monthNumber);
		var lastDate = getLastDate(monthNumber);
		nlapiLogExecution("ERROR","firstDate",firstDate)
		nlapiLogExecution("ERROR","lastDate",lastDate)
		newObj = {
		"date" : getDateMonth(monthNumber),
		"value" : getBenchCount(firstDate,lastDate,i_user, practiceId, isAdmin,i_region,s_filters)
		}
		bechArray.push(newObj);
		newObj = {
		"date" : getDateMonth(monthNumber),
		"value" : getProjectResourceCount(firstDate,lastDate,i_user, practiceId, isAdmin,i_region,s_filters)
		}
		projectPositionsArray.push(newObj);
		newObj = {
		"date" : getDateMonth(monthNumber),
		"value" : getMLResourceCount(firstDate,lastDate,i_user, practiceId, isAdmin,i_region,s_filters)
		}
		mlPositionsArray.push(newObj);
		newObj = {
		"date" : getDateMonth(monthNumber),
		"value" : getBookedRevenue(firstDate,i_user, practiceId, isAdmin,i_region,s_filters)
		}
		revenueBooked.push(newObj);
		newObj = {
		"date" : getDateMonth(monthNumber),
		"value" : getMLRevenue(firstDate,i_user, practiceId, isAdmin,i_region,s_filters)
		}
		revenueML.push(newObj);
		monthNumber++;
	}
	graphData.bench = bechArray;
	graphData.projectPositions = projectPositionsArray;
	graphData.mlPositions = mlPositionsArray;
	graphData.revenueBooked = revenueBooked;
	graphData.revenueML = revenueML;
	graphData.revenueProjectPositions = projectPositionsArray;
	graphData.revenueMlPositions = mlPositionsArray;
	return graphData;
}
function getBenchCount(startDate,endDate,i_user, i_practice, isAdmin, i_region,s_filters) 
{
	//nlapiLogExecution('Debug','s_filters$$$ ',s_filters);
	//nlapiLogExecution('Debug','i_practice ',i_practice);
	s_filters = JSON.parse(s_filters);
	//======================================================================
    var filters = [];
	if(isAdmin) 
	{
		filters = [
			["job.custentity_project_allocation_category", "anyof", "4"],
			"AND",
			["job.status", "anyof", "2"],
			"AND",
			["employee.custentity_employee_inactive", "is", "F"],
			"AND",
			["startdate", "notafter", endDate], //+ 12 month
			"AND",
			["enddate", "notbefore", startDate]
		]
		if(isArrayNotEmpty(s_filters.practice))
		{
			var practiceArray = getSubPractices(s_filters.practice);
			filters.push('AND');
			filters.push(["job.custentity_practice","anyof",practiceArray]);
		}
	
	} 
	else if(i_practice) 
	{
		var department = nlapiLookupField("employee",i_user,"department");
		//nlapiLogExecution("ERROR","department",department);
		var parentPractice = nlapiLookupField("department",department,"custrecord_parent_practice");
		//nlapiLogExecution("ERROR","parent practice :",parentPractice);
		var practiceArray = getSubPractices(i_practice);
		filters = [
			["job.custentity_project_allocation_category", "anyof", "4"],
			"AND",
			["job.status", "anyof", "2"],
			"AND",
			["employee.custentity_employee_inactive", "is", "F"],
			"AND",
			["startdate", "notafter", endDate], //+ 12 month
			"AND",
			["enddate", "notbefore", startDate],
			"AND", 
			["job.custentity_practice", "anyof", practiceArray]
			
		]
		if(isArrayNotEmpty(s_filters.practice))
		{
			var practiceArray = getSubPractices(s_filters.practice);
			filters.push('AND');
			filters.push(["job.custentity_practice","anyof",practiceArray]);
		}
	} 
	else if(i_region) 
	{
		filters = [
			["job.custentity_project_allocation_category", "anyof", "4"],
			"AND",
			["job.status", "anyof", "2"],
			"AND",
			["employee.custentity_employee_inactive", "is", "F"],
			"AND",
			["startdate", "notafter", endDate], //+ 12 month
			"AND",
			["enddate", "notbefore", startDate]
		]
		if(isArrayNotEmpty(s_filters.practice))
		{
			var practiceArray = getSubPractices(s_filters.practice);
			filters.push('AND');
			filters.push(["job.custentity_practice","anyof",practiceArray]);
		}
	} 
	else if(i_user)
	{
		var department = nlapiLookupField("employee",i_user,"department");
		//nlapiLogExecution("ERROR","department",department);
		var parentPractice = nlapiLookupField("department",department,"custrecord_parent_practice");
		//nlapiLogExecution("ERROR","parent practice :",parentPractice);
		var practiceArray = getSubPractices(parentPractice);
		filters = [
			["job.custentity_project_allocation_category", "anyof", "4"],
			"AND",
			["job.status", "anyof", "2"],
			"AND",
			["employee.custentity_employee_inactive", "is", "F"],
			"AND",
			["startdate", "notafter", endDate], //+ 12 month
			"AND",
			["enddate", "notbefore", startDate]/*,
			"AND", 
			["job.custentity_practice", "anyof", practiceArray]*/
			
		]
		if(isArrayNotEmpty(s_filters.practice))
		{
			var practiceArray = getSubPractices(s_filters.practice);
			filters.push('AND');
			filters.push(["job.custentity_practice","anyof",practiceArray]);
		}
	}
	//==================================================================
/*	filters = [
		["job.custentity_project_allocation_category", "anyof", "4"],
		"AND",
		["job.status", "anyof", "2"],
		"AND",
		["employee.custentity_employee_inactive", "is", "F"],
		"AND",
		["startdate", "notafter", endDate], //+ 12 month
		"AND",
		["enddate", "notbefore", startDate]
	]*/
	var columns = [
		new nlobjSearchColumn("resource", null, "GROUP"),
        new nlobjSearchColumn("altname", "customer", "GROUP"),
		new nlobjSearchColumn("customer", "job", "GROUP"),
        new nlobjSearchColumn("project", null, "GROUP"),
        new nlobjSearchColumn("custentity_practice", "job", "GROUP"),
        new nlobjSearchColumn("startdate", null, "GROUP"),
        new nlobjSearchColumn("enddate", null, "GROUP"),
        new nlobjSearchColumn("percentoftime", null, "GROUP")]
	var resourceallocationSearch = searchRecord("resourceallocation", null, filters, columns, null);
	if (resourceallocationSearch) {
		return resourceallocationSearch.length;
	} else {
		return 0;
	}
}

function getMLResourceCount(startDate,endDate,i_user, practiceId, isAdmin,i_region,s_filters) 
{
	//nlapiLogExecution('debug','getMLResourceCount ');
	s_filters = JSON.parse(s_filters);
	var filters = [];
	if(isAdmin)
	{
		filters = [
				["custrecord_frf_details_status", "is", "F"],
				"AND",
				["custrecord_frf_details_opp_id.custrecord_stage_sfdc", "noneof", "9", "8"], 
				"AND",
				["custrecord_frf_details_opp_id.custrecord_projection_status_sfdc", "anyof", "2"],
				"AND",
				["custrecord_frf_details_start_date", "within", startDate, endDate], //+ 12 month
				"AND",
				["isinactive","is","F"],
				"AND",
				["custrecord_frf_details_status","is","F"]
			];
		if(isArrayNotEmpty(s_filters.region))
		{
			filters.push('AND');
			filters.push(["custrecord_frf_details_account.custentity_region","anyof",s_filters.region]);
		}
		if(isArrayNotEmpty(s_filters.practice))
		{
			filters.push('AND');
			filters.push(["custrecord_frf_details_res_practice","anyof",s_filters.practice]);
		}
		if(isArrayNotEmpty(s_filters.account))
		{
			filters.push('AND');
			filters.push(["custrecord_frf_details_account","anyof",s_filters.account]);
		}
		if(isArrayNotEmpty(s_filters.project))
		{
			filters.push('AND');
			filters.push(["custrecord_frf_details_project","anyof",s_filters.project],"OR",["custrecord_frf_details_opp_id","anyof",s_filters.project]);
		}
	}
    else if(practiceId)
	{
		var practiceArray = getSubPractices(practiceId);
		filters = [
				["custrecord_frf_details_res_practice","anyof",practiceArray],
				"AND",
				["custrecord_frf_details_status", "is", "F"],
				"AND",
				["custrecord_frf_details_opp_id.custrecord_stage_sfdc", "noneof", "9", "8"], 
				"AND",
				["custrecord_frf_details_opp_id.custrecord_projection_status_sfdc", "anyof", "2"],
				"AND",
				["custrecord_frf_details_start_date", "within", startDate, endDate], //+ 12 month
				"AND",
				["isinactive","is","F"],
				"AND",
				["custrecord_frf_details_status","is","F"]
			];
		if(isArrayNotEmpty(s_filters.region))
		{
			filters.push('AND');
			filters.push(["custrecord_frf_details_account.custentity_region","anyof",s_filters.region]);
		}
		if(isArrayNotEmpty(s_filters.practice))
		{
			filters.push('AND');
			filters.push(["custrecord_frf_details_res_practice","anyof",s_filters.practice]);
		}
		if(isArrayNotEmpty(s_filters.account))
		{
			filters.push('AND');
			filters.push(["custrecord_frf_details_account","anyof",s_filters.account]);
		}
		if(isArrayNotEmpty(s_filters.project))
		{
			filters.push('AND');
			filters.push(["custrecord_frf_details_project","anyof",s_filters.project],"OR",["custrecord_frf_details_opp_id","anyof",s_filters.project]);
		}
	}
	else if(i_region)
	{
		filters = [
				["custrecord_frf_details_account.custentity_region","anyof",i_region],
				"AND",
				["custrecord_frf_details_status", "is", "F"],
				"AND",
				["custrecord_frf_details_opp_id.custrecord_stage_sfdc", "noneof", "9", "8"], 
				"AND",
				["custrecord_frf_details_opp_id.custrecord_projection_status_sfdc", "anyof", "2"],
				"AND",
				["custrecord_frf_details_start_date", "within", startDate, endDate], //+ 12 month
				"AND",
				["isinactive","is","F"],
				"AND",
				["custrecord_frf_details_status","is","F"]
			];
		if(isArrayNotEmpty(s_filters.region))
		{
			filters.push('AND');
			filters.push(["custrecord_frf_details_account.custentity_region","anyof",s_filters.region]);
		}
		if(isArrayNotEmpty(s_filters.practice))
		{
			filters.push('AND');
			filters.push(["custrecord_frf_details_res_practice","anyof",s_filters.practice]);
		}
		if(isArrayNotEmpty(s_filters.account))
		{
			filters.push('AND');
			filters.push(["custrecord_frf_details_account","anyof",s_filters.account]);
		}
		if(isArrayNotEmpty(s_filters.project))
		{
			filters.push('AND');
			filters.push(["custrecord_frf_details_project","anyof",s_filters.project],"OR",["custrecord_frf_details_opp_id","anyof",s_filters.project]);
		}
	}
	else if(i_user)
	{
		filters = [
				   [[["custrecord_frf_details_project.custentity_projectmanager","anyof",i_user],
				   "OR",
				   ["custrecord_frf_details_project.custentity_deliverymanager","anyof",i_user],
				    "OR",
				   ["custrecord_frf_details_project.custentity_clientpartner","anyof",i_user]],
				   "AND",
					["custrecord_frf_details_status", "is", "F"],
					"AND",
					["custrecord_frf_details_opp_id.custrecord_stage_sfdc", "noneof", "9", "8"], 
					"AND",
					["custrecord_frf_details_opp_id.custrecord_projection_status_sfdc", "anyof", "2"],
					"AND",
					["custrecord_frf_details_start_date", "within", startDate, endDate], //+ 12 month
					"AND",
					["isinactive","is","F"],
					"AND",
					["custrecord_frf_details_status","is","F"]],
				   "OR",
				   [[["custrecord_frf_details_opp_id.custrecord_sfdc_opp_pm","anyof",i_user],
				   "OR",
				   ["custrecord_frf_details_opp_id.custrecord_sfdc_opportunity_dm","anyof",i_user],
				    "OR",
				   ["custrecord_frf_details_opp_id.custrecord_sfdc_client_partner","anyof",i_user]],
				   "AND",
					["custrecord_frf_details_status", "is", "F"],
					"AND",
					["custrecord_frf_details_opp_id.custrecord_stage_sfdc", "noneof", "9", "8"], 
					"AND",
					["custrecord_frf_details_opp_id.custrecord_projection_status_sfdc", "anyof", "2"],
					"AND",
					["custrecord_frf_details_start_date", "within", startDate, endDate], //+ 12 month
					"AND",
					["isinactive","is","F"],
					"AND",
					["custrecord_frf_details_status","is","F"]]
				   ]
		if(isArrayNotEmpty(s_filters.region))
		{
			filters.push('AND');
			filters.push(["custrecord_frf_details_account.custentity_region","anyof",s_filters.region]);
		}
		if(isArrayNotEmpty(s_filters.practice))
		{
			filters.push('AND');
			filters.push(["custrecord_frf_details_res_practice","anyof",s_filters.practice]);
		}
		if(isArrayNotEmpty(s_filters.account))
		{
			filters.push('AND');
			filters.push(["custrecord_frf_details_account","anyof",s_filters.account]);
		}
		if(isArrayNotEmpty(s_filters.project))
		{
			filters.push('AND');
			filters.push(["custrecord_frf_details_project","anyof",s_filters.project],"OR",["custrecord_frf_details_opp_id","anyof",s_filters.project]);
		}
	}
	var columns = [];
	var customrecord_frf_detailsSearch = searchRecord("customrecord_frf_details", null,
		filters, columns, null);
	if (customrecord_frf_detailsSearch) {
		return customrecord_frf_detailsSearch.length;
	} else {
		return "0";
	}
}

function getProjectResourceCount(startDate,endDate,i_user, practiceId, isAdmin,i_region,s_filters)
{
	s_filters = JSON.parse(s_filters);
	//nlapiLogExecution('debug','in getProjectResourceCount');
//=====================================================================/

var filters = [];
	if(isAdmin)
	{
		filters = [
				["custrecord_frf_details_opp_id", "anyof", "@NONE@"],
				"AND",
				["custrecord_frf_details_project", "noneof", "@NONE@"],
				"AND",
				["custrecord_frf_details_start_date", "within", startDate, endDate], //+ 12 month
				//"AND",
				//["custrecord_frf_details_end_date", "notbefore", startDate],
				"AND",
				["isinactive","is","F"],
				"AND",
				["custrecord_frf_details_status","is","F"]
			];
		if(isArrayNotEmpty(s_filters.region))
		{
			filters.push('AND');
			filters.push(["custrecord_frf_details_account.custentity_region","anyof",s_filters.region]);
		}
		if(isArrayNotEmpty(s_filters.practice))
		{
			filters.push('AND');
			filters.push(["custrecord_frf_details_res_practice","anyof",s_filters.practice]);
		}
		if(isArrayNotEmpty(s_filters.account))
		{
			filters.push('AND');
			filters.push(["custrecord_frf_details_account","anyof",s_filters.account]);
		}
		if(isArrayNotEmpty(s_filters.project))
		{
			filters.push('AND');
			filters.push(["custrecord_frf_details_project","anyof",s_filters.project],"OR",["custrecord_frf_details_opp_id","anyof",s_filters.project]);
		}
	}
    else if(practiceId)
	{
		var practiceArray = getSubPractices(practiceId);
		filters = [
				["custrecord_frf_details_res_practice","anyof",practiceArray],
				"AND",
				["custrecord_frf_details_opp_id", "anyof", "@NONE@"],
				"AND",
				["custrecord_frf_details_project", "noneof", "@NONE@"],
				"AND",
				["custrecord_frf_details_start_date", "within", startDate, endDate], //+ 12 month
				//"AND",
				//["custrecord_frf_details_end_date", "notbefore", startDate],
				"AND",
				["isinactive","is","F"],
				"AND",
				["custrecord_frf_details_status","is","F"]
			];
		if(isArrayNotEmpty(s_filters.region))
		{
			filters.push('AND');
			filters.push(["custrecord_frf_details_account.custentity_region","anyof",s_filters.region]);
		}
		if(isArrayNotEmpty(s_filters.practice))
		{
			filters.push('AND');
			filters.push(["custrecord_frf_details_res_practice","anyof",s_filters.practice]);
		}
		if(isArrayNotEmpty(s_filters.account))
		{
			filters.push('AND');
			filters.push(["custrecord_frf_details_account","anyof",s_filters.account]);
		}
		if(isArrayNotEmpty(s_filters.project))
		{
			filters.push('AND');
			filters.push(["custrecord_frf_details_project","anyof",s_filters.project],"OR",["custrecord_frf_details_opp_id","anyof",s_filters.project]);
		}
	}
	else if(i_region)
	{
		filters = [
				["custrecord_frf_details_account.custentity_region","anyof",i_region],
				"AND",
				["custrecord_frf_details_opp_id", "anyof", "@NONE@"],
				"AND",
				["custrecord_frf_details_project", "noneof", "@NONE@"],
				"AND",
				["custrecord_frf_details_start_date", "within", startDate, endDate], //+ 12 month
				//"AND",
				//["custrecord_frf_details_end_date", "notbefore", startDate],
				"AND",
				["isinactive","is","F"],
				"AND",
				["custrecord_frf_details_status","is","F"]
			];
		if(isArrayNotEmpty(s_filters.region))
		{
			filters.push('AND');
			filters.push(["custrecord_frf_details_account.custentity_region","anyof",s_filters.region]);
		}
		if(isArrayNotEmpty(s_filters.practice))
		{
			filters.push('AND');
			filters.push(["custrecord_frf_details_res_practice","anyof",s_filters.practice]);
		}
		if(isArrayNotEmpty(s_filters.account))
		{
			filters.push('AND');
			filters.push(["custrecord_frf_details_account","anyof",s_filters.account]);
		}
		if(isArrayNotEmpty(s_filters.project))
		{
			filters.push('AND');
			filters.push(["custrecord_frf_details_project","anyof",s_filters.project],"OR",["custrecord_frf_details_opp_id","anyof",s_filters.project]);
		}
	}
	else if(i_user)
	{
		filters = [
				   [[["custrecord_frf_details_project.custentity_projectmanager","anyof",i_user],
				   "OR",
				   ["custrecord_frf_details_project.custentity_deliverymanager","anyof",i_user],
				    "OR",
				   ["custrecord_frf_details_project.custentity_clientpartner","anyof",i_user]],
				   "AND",
					["custrecord_frf_details_opp_id", "anyof", "@NONE@"],
					"AND",
					["custrecord_frf_details_project", "noneof", "@NONE@"],
					"AND",
					["custrecord_frf_details_start_date", "within", startDate, endDate], //+ 12 month
					//"AND",
					//["custrecord_frf_details_end_date", "notbefore", startDate],
					"AND",
					["isinactive","is","F"],
					"AND",
					["custrecord_frf_details_status","is","F"]],
				   "OR",
				   [[["custrecord_frf_details_opp_id.custrecord_sfdc_opp_pm","anyof",i_user],
				   "OR",
				   ["custrecord_frf_details_opp_id.custrecord_sfdc_opportunity_dm","anyof",i_user],
				    "OR",
				   ["custrecord_frf_details_opp_id.custrecord_sfdc_client_partner","anyof",i_user]],
				   "AND",
					["custrecord_frf_details_opp_id", "anyof", "@NONE@"],
					"AND",
					["custrecord_frf_details_project", "noneof", "@NONE@"],
					"AND",
					["custrecord_frf_details_open_close_status", "noneof", "2"],
					"AND",
					["custrecord_frf_details_start_date", "within", startDate, endDate], //+ 12 month
					//"AND",
					//["custrecord_frf_details_end_date", "notbefore", startDate],
					"AND",
					["isinactive","is","F"],
					"AND",
					["custrecord_frf_details_status","is","F"]]
				   ]
		if(isArrayNotEmpty(s_filters.region))
		{
			filters.push('AND');
			filters.push(["custrecord_frf_details_account.custentity_region","anyof",s_filters.region]);
		}
		if(isArrayNotEmpty(s_filters.practice))
		{
			filters.push('AND');
			filters.push(["custrecord_frf_details_res_practice","anyof",s_filters.practice]);
		}
		if(isArrayNotEmpty(s_filters.account))
		{
			filters.push('AND');
			filters.push(["custrecord_frf_details_account","anyof",s_filters.account]);
		}
		if(isArrayNotEmpty(s_filters.project))
		{
			filters.push('AND');
			filters.push(["custrecord_frf_details_project","anyof",s_filters.project],"OR",["custrecord_frf_details_opp_id","anyof",s_filters.project]);
		}
	}
//====================================================================	


/*var filters = [
		["custrecord_frf_details_opp_id", "anyof", "@NONE@"],
		"AND",
		["custrecord_frf_details_project", "noneof", "@NONE@"],
		"AND",
		["custrecord_frf_details_open_close_status", "noneof", "2"],
		"AND",
		["custrecord_frf_details_start_date", "within", startDate, endDate], //+ 12 month
		//"AND",
		//["custrecord_frf_details_end_date", "notbefore", startDate],
		"AND",
		["isinactive","is","F"],
		"AND",
		["custrecord_frf_details_status","is","F"]
	];*/
	var columns = [];
	var customrecord_frf_detailsSearch = searchRecord("customrecord_frf_details", null,
		filters, columns, null);
	if (customrecord_frf_detailsSearch) {
		return customrecord_frf_detailsSearch.length;
	} else {
		return "0";
	}
}

function getStartDate() {
	var date = new Date();
	var startDate = new Date(date.getFullYear(), date.getMonth(), 1);
	return nlapiDateToString(startDate);
}

function getEndDate() {
	var date = new Date();
	var endDate = new Date(date.getFullYear(), date.getMonth() + 12, 0);
	return nlapiDateToString(endDate);
}
function getDateMonth(monthNumber){
	monthNumber = parseInt(monthNumber);
	var newDate = new Date();
	var month_array = ['JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUN', 'JUL', 'AUG', 'SEP', 'OCT', 'NOV', 'DEC'];
	return month_array[(monthNumber)] + " " + newDate.getFullYear();
}
function getFirstDate(monthNumber){
	monthNumber = parseInt(monthNumber) + 1;
	var year = new Date().getFullYear()
	var dateStr = monthNumber +  "/" + "01" + "/" + year
	return dateStr;
}
function getLastDate(monthNumber){
	monthNumber = parseInt(monthNumber) + 1;
	var year = new Date().getFullYear()
	var dateStr = monthNumber +  "/" + daysInMonth(monthNumber, year) + "/" + year
	return dateStr;
}
function daysInMonth (month, year) { 
    return new Date(year, month, 0).getDate(); 
}
function getBookedRevenue(monthDate,i_user, practiceId, isAdmin,i_region,s_filters){
	//nlapiLogExecution('debug','isAdmin '+isAdmin,'practiceId '+practiceId);
	//nlapiLogExecution('debug','monthDate990 ',monthDate);
	s_filters = JSON.parse(s_filters);
	var filters = [];
	var columns = [];
	if(isAdmin)
	{
		filters = [
		   ["custrecord_sfdc_revenue_data_prostatus","is","Booked"], 
		   "AND", 
		   ["custrecord_sfdc_fuel_forecast_rev_ext.custrecord_sfdc_fuel_fore_modayyear","on",monthDate], 
		   "AND", 
		   ["isinactive","is","F"]
		]
		if(isArrayNotEmpty(s_filters.region))
		{
			filters.push('AND');
			filters.push(["custrecord_sfdc_rev_data_ns_region","anyof",s_filters.region]);
		}
		if(isArrayNotEmpty(s_filters.practice))
		{
			filters.push('AND');
			filters.push(["custrecord_sfdc_fuel_forecast_rev_ext.custrecord_sfdc_fuel_forecast_rev_pract","anyof",s_filters.practice]);
		}
		if(isArrayNotEmpty(s_filters.account))
		{
			filters.push('AND');
			filters.push([["custrecord_sfdc_revenue_data_opp_id.custrecord_customer_internal_id_sfdc","anyof",s_filters.account],"OR",["custrecord_sfdc_revenue_data_proj_id_ns.customer","anyof",s_filters.account]]);
		}
		if(isArrayNotEmpty(s_filters.project))
		{
			filters.push('AND');
			filters.push(["custrecord_sfdc_revenue_data_proj_id_ns","anyof",s_filters.project]);
		}
	}
	else if(practiceId)
	{
		var practiceName = nlapiLookupField("department",practiceId,"name");
		filters = [
		   ["custrecord_sfdc_revenue_data_prostatus","is","Booked"], 
		   "AND", 
		   ["custrecord_sfdc_fuel_forecast_rev_ext.custrecord_sfdc_fuel_fore_modayyear","on",monthDate], 
		   "AND", 
		   ["isinactive","is","F"],
			"AND",
		   ["custrecord_sfdc_fuel_forecast_rev_ext.custrecord_sfdc_fuel_forecast_rev_pract", "is", practiceName]
		]
		if(isArrayNotEmpty(s_filters.region))
		{
			filters.push('AND');
			filters.push(["custrecord_sfdc_rev_data_ns_region","anyof",s_filters.region]);
		}
		if(isArrayNotEmpty(s_filters.practice))
		{
			filters.push('AND');
			filters.push(["custrecord_sfdc_fuel_forecast_rev_ext.custrecord_sfdc_fuel_forecast_rev_pract","anyof",s_filters.practice]);
		}
		if(isArrayNotEmpty(s_filters.account))
		{
			filters.push('AND');
			filters.push([["custrecord_sfdc_revenue_data_opp_id.custrecord_customer_internal_id_sfdc","anyof",s_filters.account],"OR",["custrecord_sfdc_revenue_data_proj_id_ns.customer","anyof",s_filters.account]]);
		}
		if(isArrayNotEmpty(s_filters.project))
		{
			filters.push('AND');
			filters.push(["custrecord_sfdc_revenue_data_proj_id_ns","anyof",s_filters.project]);
		}
	}
	else if(i_region)
	{
		filters = [
			   ["custrecord_sfdc_revenue_data_prostatus","is","Booked"], 
			   "AND", 
			   ["custrecord_sfdc_fuel_forecast_rev_ext.custrecord_sfdc_fuel_fore_modayyear","on",monthDate], 
			   "AND", 
			   ["isinactive","is","F"],
				"AND",
				[["custrecord_sfdc_revenue_data_opp_id.custrecord_sfdc_opp_account_region", "anyof", i_region],
				"OR",
				["custrecord_sfdc_revenue_data_proj_id_ns.custentity_region", "anyof", i_region], 
				"OR", 
				["custrecord_sfdc_rev_data_ns_region", "anyof", i_region]]
				]
		if(isArrayNotEmpty(s_filters.region))
		{
			filters.push('AND');
			filters.push(["custrecord_sfdc_rev_data_ns_region","anyof",s_filters.region]);
		}
		if(isArrayNotEmpty(s_filters.practice))
		{
			filters.push('AND');
			filters.push(["custrecord_sfdc_fuel_forecast_rev_ext.custrecord_sfdc_fuel_forecast_rev_pract","anyof",s_filters.practice]);
		}
		if(isArrayNotEmpty(s_filters.account))
		{
			filters.push('AND');
			filters.push([["custrecord_sfdc_revenue_data_opp_id.custrecord_customer_internal_id_sfdc","anyof",s_filters.account],"OR",["custrecord_sfdc_revenue_data_proj_id_ns.customer","anyof",s_filters.account]]);
		}
		if(isArrayNotEmpty(s_filters.project))
		{
			filters.push('AND');
			filters.push(["custrecord_sfdc_revenue_data_proj_id_ns","anyof",s_filters.project]);
		}
	}
	else if(i_user)
	{
		filters = [
			["custrecord_sfdc_revenue_data_prostatus","is","Booked"], 
		    "AND", 
		    ["custrecord_sfdc_fuel_forecast_rev_ext.custrecord_sfdc_fuel_fore_modayyear","on",monthDate], 
		    "AND", 
		    ["isinactive","is","F"],
			"AND",
            ["custrecord_sfdc_revenue_data_proj_id_ns.custentity_projectmanager", "anyof", i_user], 
			"OR", 
			["custrecord_sfdc_revenue_data_opp_id.custrecord_sfdc_opp_pm", "anyof", i_user], 
			"OR", 
			["custrecord_sfdc_revenue_data_opp_id.custrecord_sfdc_opportunity_dm", "anyof", i_user], 
			"OR", 
			["custrecord_sfdc_revenue_data_proj_id_ns.custentity_deliverymanager", "anyof", i_user]]
		if(isArrayNotEmpty(s_filters.region))
		{
			filters.push('AND');
			filters.push(["custrecord_sfdc_rev_data_ns_region","anyof",s_filters.region]);
		}
		if(isArrayNotEmpty(s_filters.practice))
		{
			filters.push('AND');
			filters.push(["custrecord_sfdc_fuel_forecast_rev_ext.custrecord_sfdc_fuel_forecast_rev_pract","anyof",s_filters.practice]);
		}
		if(isArrayNotEmpty(s_filters.account))
		{
			filters.push('AND');
			filters.push([["custrecord_sfdc_revenue_data_opp_id.custrecord_customer_internal_id_sfdc","anyof",s_filters.account],"OR",["custrecord_sfdc_revenue_data_proj_id_ns.customer","anyof",s_filters.account]]);
		}
		if(isArrayNotEmpty(s_filters.project))
		{
			filters.push('AND');
			filters.push(["custrecord_sfdc_revenue_data_proj_id_ns","anyof",s_filters.project]);
		}
	}
	columns = [
	   new nlobjSearchColumn("custrecord_sfdc_fuel_fore_modayyear","CUSTRECORD_SFDC_FUEL_FORECAST_REV_EXT","GROUP").setSort(false), 
	   new nlobjSearchColumn("custrecord_sfdc_fuel_forecast_rev_amount","CUSTRECORD_SFDC_FUEL_FORECAST_REV_EXT","SUM")
	]
var customrecord_fuel_sfdc_revenue_dataSearch = nlapiSearchRecord("customrecord_fuel_sfdc_revenue_data",null,filters,columns);
if(customrecord_fuel_sfdc_revenue_dataSearch){
//nlapiLogExecution('debug','customrecord_fuel_sfdc_revenue_dataSearch length ',customrecord_fuel_sfdc_revenue_dataSearch.length);
	//nlapiLogExecution('debug','customrecord_fuel_sfdc_revenue_dataSearch ',customrecord_fuel_sfdc_revenue_dataSearch.length);
	var amount = customrecord_fuel_sfdc_revenue_dataSearch[0].getValue("custrecord_sfdc_fuel_forecast_rev_amount","CUSTRECORD_SFDC_FUEL_FORECAST_REV_EXT","SUM")
	//for(var i = 0 ; i < customrecord_fuel_sfdc_revenue_dataSearch.length ; i++ ){
		//amount = parseFloat(amount) + parseFloat(customrecord_fuel_sfdc_revenue_dataSearch[i].getValue("custrecord_sfdc_fuel_forecast_rev_amount","CUSTRECORD_SFDC_FUEL_FORECAST_REV_EXT",null))
	//}
	//nlapiLogExecution('Debug','amount ',amount);
	return amount;
}else{
	return "0"
}
}
function getMLRevenue(monthDate,i_user, practiceId, isAdmin,i_region,s_filters)
{
	s_filters = JSON.parse(s_filters);
	var filters = [];
	var columns = [];
	if(isAdmin)
	{
		filters = [
		   ["custrecord_sfdc_revenue_data_prostatus","is","Most Likely"], 
		   "AND", 
		   ["custrecord_sfdc_fuel_forecast_rev_ext.custrecord_sfdc_fuel_fore_modayyear","on",monthDate], 
		   "AND", 
		   ["isinactive","is","F"]
		]
		if(isArrayNotEmpty(s_filters.region))
		{
			filters.push('AND');
			filters.push(["custrecord_sfdc_rev_data_ns_region","anyof",s_filters.region]);
		}
		if(isArrayNotEmpty(s_filters.practice))
		{
			filters.push('AND');
			filters.push(["custrecord_sfdc_fuel_forecast_rev_ext.custrecord_sfdc_fuel_forecast_rev_pract","anyof",s_filters.practice]);
		}
		if(isArrayNotEmpty(s_filters.account))
		{
			filters.push('AND');
			filters.push([["custrecord_sfdc_revenue_data_opp_id.custrecord_customer_internal_id_sfdc","anyof",s_filters.account],"OR",["custrecord_sfdc_revenue_data_proj_id_ns.customer","anyof",s_filters.account]]);
		}
		if(isArrayNotEmpty(s_filters.project))
		{
			filters.push('AND');
			filters.push(["custrecord_sfdc_revenue_data_proj_id_ns","anyof",s_filters.project]);
		}
	}
	else if(practiceId)
	{
		var practiceName = nlapiLookupField("department",practiceId,"name");
		
		filters = [
		   ["custrecord_sfdc_revenue_data_prostatus","is","Most Likely"], 
		   "AND", 
		   ["custrecord_sfdc_fuel_forecast_rev_ext.custrecord_sfdc_fuel_fore_modayyear","on",monthDate], 
		   "AND", 
		   ["isinactive","is","F"],
			"AND",
		   ["custrecord_sfdc_fuel_forecast_rev_ext.custrecord_sfdc_fuel_forecast_rev_pract", "is", practiceName]
		]
		if(isArrayNotEmpty(s_filters.region))
		{
			filters.push('AND');
			filters.push(["custrecord_sfdc_rev_data_ns_region","anyof",s_filters.region]);
		}
		if(isArrayNotEmpty(s_filters.practice))
		{
			filters.push('AND');
			filters.push(["custrecord_sfdc_fuel_forecast_rev_ext.custrecord_sfdc_fuel_forecast_rev_pract","anyof",s_filters.practice]);
		}
		if(isArrayNotEmpty(s_filters.account))
		{
			filters.push('AND');
			filters.push([["custrecord_sfdc_revenue_data_opp_id.custrecord_customer_internal_id_sfdc","anyof",s_filters.account],"OR",["custrecord_sfdc_revenue_data_proj_id_ns.customer","anyof",s_filters.account]]);
		}
		if(isArrayNotEmpty(s_filters.project))
		{
			filters.push('AND');
			filters.push(["custrecord_sfdc_revenue_data_proj_id_ns","anyof",s_filters.project]);
		}
	}
	else if(i_region)
	{
		//nlapiLogExecution('audit','monthDate &&&& '+monthDate,'i_region**&& '+i_region);
		filters = [
			   ["custrecord_sfdc_revenue_data_prostatus","is","Most Likely"], 
			   "AND", 
			   ["custrecord_sfdc_fuel_forecast_rev_ext.custrecord_sfdc_fuel_fore_modayyear","on",monthDate], 
			   "AND", 
			   ["isinactive","is","F"],
			   "AND",
			   [["custrecord_sfdc_revenue_data_opp_id.custrecord_sfdc_opp_account_region", "anyof", i_region],
			   "OR",
			   ["custrecord_sfdc_revenue_data_proj_id_ns.custentity_region", "anyof", i_region], 
			   "OR", 
			   ["custrecord_sfdc_rev_data_ns_region", "anyof", i_region]]
			  ]
		if(isArrayNotEmpty(s_filters.region))
		{
			filters.push('AND');
			filters.push(["custrecord_sfdc_rev_data_ns_region","anyof",s_filters.region]);
		}
		if(isArrayNotEmpty(s_filters.practice))
		{
			filters.push('AND');
			filters.push(["custrecord_sfdc_fuel_forecast_rev_ext.custrecord_sfdc_fuel_forecast_rev_pract","anyof",s_filters.practice]);
		}
		if(isArrayNotEmpty(s_filters.account))
		{
			filters.push('AND');
			filters.push([["custrecord_sfdc_revenue_data_opp_id.custrecord_customer_internal_id_sfdc","anyof",s_filters.account],"OR",["custrecord_sfdc_revenue_data_proj_id_ns.customer","anyof",s_filters.account]]);
		}
		if(isArrayNotEmpty(s_filters.project))
		{
			filters.push('AND');
			filters.push(["custrecord_sfdc_revenue_data_proj_id_ns","anyof",s_filters.project]);
		}
	}
	else if(i_user)
	{
		filters = [
			["custrecord_sfdc_revenue_data_prostatus","is","Most Likely"], 
		    "AND", 
		    ["custrecord_sfdc_fuel_forecast_rev_ext.custrecord_sfdc_fuel_fore_modayyear","on",monthDate], 
		    "AND", 
		    ["isinactive","is","F"],
			"AND",
            ["custrecord_sfdc_revenue_data_proj_id_ns.custentity_projectmanager", "anyof", i_user], 
			"OR", 
			["custrecord_sfdc_revenue_data_opp_id.custrecord_sfdc_opp_pm", "anyof", i_user], 
			"OR", 
			["custrecord_sfdc_revenue_data_opp_id.custrecord_sfdc_opportunity_dm", "anyof", i_user], 
			"OR", 
			["custrecord_sfdc_revenue_data_proj_id_ns.custentity_deliverymanager", "anyof", i_user]]
		if(isArrayNotEmpty(s_filters.region))
		{
			filters.push('AND');
			filters.push(["custrecord_sfdc_rev_data_ns_region","anyof",s_filters.region]);
		}
		if(isArrayNotEmpty(s_filters.practice))
		{
			filters.push('AND');
			filters.push(["custrecord_sfdc_fuel_forecast_rev_ext.custrecord_sfdc_fuel_forecast_rev_pract","anyof",s_filters.practice]);
		}
		if(isArrayNotEmpty(s_filters.account))
		{
			filters.push('AND');
			filters.push([["custrecord_sfdc_revenue_data_opp_id.custrecord_customer_internal_id_sfdc","anyof",s_filters.account],"OR",["custrecord_sfdc_revenue_data_proj_id_ns.customer","anyof",s_filters.account]]);
		}
		if(isArrayNotEmpty(s_filters.project))
		{
			filters.push('AND');
			filters.push(["custrecord_sfdc_revenue_data_proj_id_ns","anyof",s_filters.project]);
		}
	}

	columns = [
				new nlobjSearchColumn("custrecord_sfdc_fuel_fore_modayyear","CUSTRECORD_SFDC_FUEL_FORECAST_REV_EXT","GROUP").setSort(false), 
				new nlobjSearchColumn("custrecord_sfdc_fuel_forecast_rev_amount","CUSTRECORD_SFDC_FUEL_FORECAST_REV_EXT","SUM")
			  ]
var customrecord_fuel_sfdc_revenue_dataSearch = nlapiSearchRecord("customrecord_fuel_sfdc_revenue_data",null,filters,columns);
if(customrecord_fuel_sfdc_revenue_dataSearch){
	var amount = customrecord_fuel_sfdc_revenue_dataSearch[0].getValue("custrecord_sfdc_fuel_forecast_rev_amount","CUSTRECORD_SFDC_FUEL_FORECAST_REV_EXT","SUM")
	//for(var i = 0 ; i < customrecord_fuel_sfdc_revenue_dataSearch.length ; i++ ){
		//amount = parseFloat(amount) + parseFloat(customrecord_fuel_sfdc_revenue_dataSearch[i].getValue("custrecord_sfdc_fuel_forecast_rev_amount","CUSTRECORD_SFDC_FUEL_FORECAST_REV_EXT",null))
	//}
	return amount;
}else{
	return "0"
}
} 

//Search for customers based on region
function customerSearch(region){
	try{
	//nlapiLogExecution('DEBUG','Customer Search Region',region);
	var customerList = [];
	var filters = [];
	filters.push(new nlobjSearchFilter('custentity_region',null,'anyof',parseInt(region)));
	
	var cols=[];
	cols.push(new nlobjSearchColumn('internalid'));
	
	var searchResults_customer = searchRecord('customer',null,filters,cols);
	if(searchResults_customer){
		
		for(var i=0;i<searchResults_customer.length;i++){
			customerList.push(searchResults_customer[i].getValue('internalid'));
		}
	}
	//nlapiLogExecution('DEBUG','Customer Search customerList',customerList);
	var searchResults_prospect = searchRecord('prospect',null,filters,cols);
	if(searchResults_prospect){
		var temp = [];
		for(var j=0;j<searchResults_prospect.length;j++){
			customerList.push(searchResults_prospect[j].getValue('internalid'));
			temp.push(searchResults_prospect[j].getValue('internalid'));
		}
	}
	//nlapiLogExecution('DEBUG','Prospect Search temp',temp);
		return customerList;
	//Return array of customer ID's
}
catch(e){
	//nlapiLogExecution('Error','Customer Region Search Error',e);
}
}
