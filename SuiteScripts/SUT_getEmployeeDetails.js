// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
/*
   	Script Name: SUT_getEmployeeDetails.js
	Author: Ashish Pandit 
	Company: Inspirria CloudTech
	Date: 13-04-2018
    Description: Suitelet to get Employee data


	Script Modification Log:

	-- Date --			-- Modified By --				--Requested By--				-- Description --
  


	Below is a summary of the process controls enforced by this script file.  The control logic is described
	more fully, below, in the appropriate function headers and code blocks.

    

     SUB-FUNCTIONS
		- The following sub-functions are called by the above core functions in order to maintain code
            modularization:





*/
}
// END SCRIPT DESCRIPTION BLOCK  ====================================



// BEGIN SCRIPT UPDATION BLOCK  ====================================
/*


*/
// END SCRIPT UPDATION BLOCK  ====================================




// BEGIN GLOBAL VARIABLE BLOCK  =====================================
{

	//  Initialize any Global Variables, in particular, debugging variables...


}
// END GLOBAL VARIABLE BLOCK  =======================================




// BEGIN PAGE INIT ==================================================
function suiteletGetEmployeeData(request,rsponse)
{
	try
	{
		
		if(request.getMethod() == 'GET')
		{	
	var a_json_array = []
	
			 
			var i_employee = request.getParameter('i_employee');
			var o_employee_details = nlapiLookupField('employee',i_employee,[ 'hiredate', 'custentity_lwd','department','location']);
			
			var d_employee_hire_date = nlapiStringToDate(o_employee_details.hiredate);
			var d_employee_last_working_date = nlapiStringToDate(o_employee_details.custentity_lwd);
			var i_practice = o_employee_details.department;
			var i_location = o_employee_details.location;
			
			var o_json_obj = {}
			
			o_json_obj.d_employee_hire_date = d_employee_hire_date;
			o_json_obj.d_employee_last_working_date = d_employee_last_working_date;
			o_json_obj.i_practice = i_practice;
			
			o_json_obj.i_location = i_location;
			a_json_array.push(o_json_obj)
			
			
			response.write(JSON.stringify(a_json_array));
		}
			
		
	}
	catch(err)
	{
		nlapiLogExecution('ERROR','ERROR MESSAGE :- ',err);
	}
}
