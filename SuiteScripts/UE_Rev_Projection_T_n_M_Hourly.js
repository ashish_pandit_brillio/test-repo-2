/**
 * Calculate practice wise break-up of projected revenue for the current project
 * (Hourly) for the current month
 * 
 * Version Date Author Remarks 1.00
 * 
 * 20 Jun 2016 Nitish Mishra
 * 
 */

function userEventBeforeSubmit(type) {
	try {

		if (type == 'create' || type == 'edit' || type == 'xedit') {
			var projectId = nlapiGetFieldValue('custrecord_prp_project');

			if (projectId) {
				var startDate = nlapiGetFieldValue('custrecord_prp_start_date');
				var d_startDate = nlapiStringToDate(startDate);
				var d_endDate = nlapiAddDays(nlapiAddMonths(d_startDate, 1), -1);
				var endDate = nlapiDateToString(d_endDate, 'date');
				var today = new Date();
				nlapiSetFieldValue('custrecord_prp_end_date', endDate);
				nlapiSetFieldValue('custrecord_prp_month_name',
				        getMonthName(startDate));
				nlapiSetFieldValue('custrecord_prp_year', getYear(d_startDate));

				var projectData = nlapiLookupField('job', projectId, [
				        'jobbillingtype', 'custentity_t_and_m_monthly',
				        'customer', 'entityid', 'custentity_project_currency' ]);

				nlapiSetFieldValue('custrecord_prp_currency',
				        projectData.custentity_project_currency);

				var projectCurrencyValue = nlapiGetFieldValue('custrecord_prp_currency');
				var projectCurrencyText = nlapiGetFieldText('custrecord_prp_currency');

				if (projectData.jobbillingtype == 'TM'
				        && projectData.custentity_t_and_m_monthly == 'F') {

					// if its a past month, do revenue recognition
					if (d_endDate < today) {
						nlapiSetFieldValue('custrecord_prp_is_recognised', 'T');
						revenueRecognition(projectData, projectId, startDate,
						        endDate, d_startDate, d_endDate,
						        projectCurrencyText, projectCurrencyValue);
					} else { // else projection
						revenueProjection(projectData, projectId, startDate,
						        endDate, d_startDate, d_endDate,
						        projectCurrencyText, projectCurrencyValue);
					}
				}
			}
		}
	} catch (err) {
		nlapiLogExecution('ERROR', 'userEventBeforeSubmit', err);
	}
}

function revenueRecognition(projectData, projectId, startDate, endDate,
        d_startDate, d_endDate, projectCurrencyText, projectCurrencyValue)
{
	try {
		var recognizedAmount = 0;
		var projectName = projectData.entityid;

		nlapiLogExecution('debug', 'monthStartDate', startDate);
		nlapiLogExecution('debug', 'projectId', projectId);
		nlapiLogExecution('debug', 'projectName', projectName);

		var filters = [
		        new nlobjSearchFilter('formulanumeric', null, 'equalto', 0)
		                .setFormula("CASE WHEN {number} = 'Memorized' THEN 1 ELSE 0 END"),
		        new nlobjSearchFilter('formuladate', null, 'within', startDate,
		                endDate).setFormula('{trandate}'),
		        new nlobjSearchFilter('custcolprj_name', null, 'startswith',
		                projectName),
		        new nlobjSearchFilter('account', null, 'anyof',
		                [ '647', '644' ]) ];

		var jeSearch = nlapiSearchRecord('transaction', null, filters, [
		        new nlobjSearchColumn('department', null, 'group'),
		        new nlobjSearchColumn('fxamount', null, 'sum'),
		        new nlobjSearchColumn('currency', null, 'group') ]);

		nlapiLogExecution('debug', 'search data', JSON.stringify(jeSearch));

		var practiceWiseBreakup = {};
		var inactivePracticeMapping = getInactivePracticeMapping();

		if (jeSearch) {

			jeSearch.forEach(function(je) {
				var department = je.getValue('department', null, 'group');

				if (inactivePracticeMapping[department]) {
					department = inactivePracticeMapping[department];
				}

				var currency = je.getText('currency', null, 'group');
				var amount = je.getValue('fxamount', null, 'sum');

				var exchangeRate = nlapiExchangeRate(currency,
				        projectCurrencyText, startDate);
				var convertedAmount = exchangeRate * amount;

				if (!practiceWiseBreakup[department]) {
					practiceWiseBreakup[department] = 0;
				}

				practiceWiseBreakup[department] += amount;
			});
		}

		// remove all existing lines
		var lineCount = nlapiGetLineItemCount('recmachcustrecord_eprp_project_rev_projection');
		for (var line = lineCount; line > 0; line--) {
			nlapiRemoveLineItem(
			        'recmachcustrecord_eprp_project_rev_projection', line);
		}

		var usdExchangeRate = nlapiExchangeRate(projectCurrencyText, 'USD',
		        endDate);

		// add new lines
		for ( var practice in practiceWiseBreakup) {
			if (practiceWiseBreakup[practice] < 0) {
				practiceWiseBreakup[practice] = 0;
			}

			nlapiSelectNewLineItem('recmachcustrecord_eprp_project_rev_projection');
			nlapiSetCurrentLineItemValue(
			        'recmachcustrecord_eprp_project_rev_projection',
			        'custrecord_eprp_practice', practice);
			nlapiSetCurrentLineItemValue(
			        'recmachcustrecord_eprp_project_rev_projection',
			        'custrecord_eprp_revenue', practiceWiseBreakup[practice]);
			nlapiSetCurrentLineItemValue(
			        'recmachcustrecord_eprp_project_rev_projection',
			        'custrecord_eprp_currency', projectCurrencyValue);
			nlapiSetCurrentLineItemValue(
			        'recmachcustrecord_eprp_project_rev_projection',
			        'custrecord_eprp_usd_revenue', usdExchangeRate
			                * practiceWiseBreakup[practice]);
			nlapiCommitLineItem('recmachcustrecord_eprp_project_rev_projection');
		}
	} catch (err) {
		nlapiLogExecution('ERROR', 'revenueRecognition', err);
		throw err;
	}
}

function isNumeric(n) {
	return !isNaN(parseFloat(n)) && isFinite(n);
}

function revenueProjection(projectData, projectId, startDate, endDate,
        d_startDate, d_endDate, projectCurrencyText, projectCurrencyValue)
{
	try {
		// get project holiday list (only working days holiday)
		var holidayList = getWorkingDayHolidays(startDate, endDate, projectId,
		        projectData.customer);

		// get practice wise revenue projection
		var practiceWiseBreakUp = getDepartmentWiseBreakup(projectId,
		        startDate, endDate, holidayList, d_startDate, d_endDate);

		// remove all existing lines
		var lineCount = nlapiGetLineItemCount('recmachcustrecord_eprp_project_rev_projection');
		for (var line = lineCount; line > 0; line--) {
			nlapiRemoveLineItem(
			        'recmachcustrecord_eprp_project_rev_projection', line);
		}

		var usdExchangeRate = nlapiExchangeRate(projectCurrencyText, 'USD',
		        endDate);

		// add new lines
		for ( var practice in practiceWiseBreakUp) {
			if (practiceWiseBreakUp[practice] < 0) {
				practiceWiseBreakUp[practice] = 0;
			}

			nlapiSelectNewLineItem('recmachcustrecord_eprp_project_rev_projection');
			nlapiSetCurrentLineItemValue(
			        'recmachcustrecord_eprp_project_rev_projection',
			        'custrecord_eprp_practice', practice);
			nlapiSetCurrentLineItemValue(
			        'recmachcustrecord_eprp_project_rev_projection',
			        'custrecord_eprp_revenue', practiceWiseBreakUp[practice]);
			nlapiSetCurrentLineItemValue(
			        'recmachcustrecord_eprp_project_rev_projection',
			        'custrecord_eprp_currency', projectCurrencyValue);
			nlapiSetCurrentLineItemValue(
			        'recmachcustrecord_eprp_project_rev_projection',
			        'custrecord_eprp_usd_revenue', usdExchangeRate
			                * practiceWiseBreakUp[practice]);
			nlapiCommitLineItem('recmachcustrecord_eprp_project_rev_projection');
		}
	} catch (err) {
		nlapiLogExecution('ERROR', 'revenueProjection', err);
		throw err;
	}
}

function getDepartmentWiseBreakup(project, startDate, endDate, holidayList,
        d_startDate, d_endDate)
{
	try {
		var allocationSearch = nlapiSearchRecord(
		        'resourceallocation',
		        null,
		        [
		                new nlobjSearchFilter('project', null, 'anyof', project),
		                new nlobjSearchFilter('custeventrbillable', null, 'is',
		                        'T'),
		                new nlobjSearchFilter('startdate', null, 'notafter',
		                        endDate),
		                new nlobjSearchFilter('enddate', null, 'notbefore',
		                        startDate) ],
		        [ new nlobjSearchColumn('startdate'),
		                new nlobjSearchColumn('enddate'),
		                new nlobjSearchColumn('percentoftime'),
		                new nlobjSearchColumn('department', 'employee'),
		                new nlobjSearchColumn('subsidiary', 'employee'),
		                new nlobjSearchColumn('custevent3'),
		                new nlobjSearchColumn('custentity_hoursperday', 'job') ]);

		var departmentMainObject = {};
		var inactivePracticeMapping = getInactivePracticeMapping();

		if (allocationSearch) {

			allocationSearch
			        .forEach(function(allocation) {
				        var department = allocation.getValue('department',
				                'employee');

				        if (inactivePracticeMapping[department]) {
					        department = inactivePracticeMapping[department];
				        }

				        if (!departmentMainObject[department]) {
					        departmentMainObject[department] = 0;
				        }

				        var allocationStartDate = nlapiStringToDate(allocation
				                .getValue('startdate'));
				        var allocationEndDate = nlapiStringToDate(allocation
				                .getValue('enddate'));

				        var newStartDate = allocationStartDate > d_startDate ? allocationStartDate
				                : d_startDate;
				        var newEndDate = allocationEndDate < d_endDate ? allocationEndDate
				                : d_endDate;

				        departmentMainObject[department] += getEmployeeRevenueBetweenDates(
				                newStartDate, newEndDate, parseFloat(allocation
				                        .getValue('custevent3')), allocation
				                        .getValue('custentity_hoursperday',
				                                'job'), allocation
				                        .getValue('percentoftime'),
				                holidayList, allocation.getValue('subsidiary',
				                        'employee'));
			        });
		}

		nlapiLogExecution('debug', 'departmentMainObject', JSON
		        .stringify(departmentMainObject));
		return departmentMainObject;
	} catch (err) {
		nlapiLogExecution('ERROR', 'getDepartmentWiseBreakup', err);
		throw err;
	}
}

function getEmployeeRevenueBetweenDates(d_startDate, d_endDate, hourlyRate,
        hoursPerDay, percentAllocation, holidayList, subsidiary)
{
	try {
		nlapiLogExecution('debug', 'hourlyRate', hourlyRate);
		nlapiLogExecution('debug', 'hoursPerDay', hoursPerDay);

		if (!hoursPerDay) {
			hoursPerDay = 8;
		}

		var noOfWorkingsDays = getWorkingDays(d_startDate, d_endDate);
		nlapiLogExecution('debug', 'noOfWorkingsDays', noOfWorkingsDays);

		// holidays between allocation dates
		var holidayCount = 0;

		holidayList.forEach(function(holiday) {

			if (d_startDate <= holiday.Date && holiday.Date <= d_endDate
			        && subsidiary == holiday.Subsidiary) {
				holidayCount += 1;
			}
		});

		// final working days
		noOfWorkingsDays -= holidayCount;
		nlapiLogExecution('debug', 'holidayCount', holidayCount);

		percentAllocation = parseFloat((percentAllocation.split('%')[0].trim())) / 100;
		nlapiLogExecution('debug', 'percentAllocation', percentAllocation);

		var noOfWorkingHours = noOfWorkingsDays * percentAllocation
		        * hoursPerDay;
		nlapiLogExecution('debug', 'noOfWorkingHours', noOfWorkingHours);

		var revenue = noOfWorkingHours * hourlyRate;
		nlapiLogExecution('debug', 'revenue', revenue);
		return revenue;
	} catch (err) {
		nlapiLogExecution('ERROR', 'getEmployeeRevenueBetweenDates', err);
		throw err;
	}
}

function getWorkingDays(d_startDate, d_endDate) {
	try {
		var numberOfWorkingDays = 0;

		for (var i = 0;; i++) {
			var currentDate = nlapiAddDays(d_startDate, i);

			if (currentDate > d_endDate) {
				break;
			}

			if (currentDate.getDay() != 0 && currentDate.getDay() != 6) {
				numberOfWorkingDays += 1;
			}
		}

		return numberOfWorkingDays;
	} catch (err) {
		nlapiLogExecution('ERROR', 'getWorkingDays', err);
		throw err;
	}
}

function getWorkingDayHolidays(start_date, end_date, project, customer) {
	var project_holiday = nlapiLookupField('job', project,
	        'custentityproject_holiday');

	if (project_holiday == 1) {
		return get_company_holidays(start_date, end_date);
	} else {
		return get_customer_holidays(start_date, end_date, customer);
	}
}

function get_company_holidays(start_date, end_date) {
	try {
		var holiday_list = [];

		var search_company_holiday = nlapiSearchRecord('customrecord_holiday',
		        'customsearch_company_holiday_search',
		        [ new nlobjSearchFilter('custrecord_date', null, 'within',
		                start_date, end_date) ], [
		                new nlobjSearchColumn('custrecord_date'),
		                new nlobjSearchColumn('custrecordsubsidiary') ]);

		if (search_company_holiday) {

			for (var i = 0; i < search_company_holiday.length; i++) {
				var holidayDate = nlapiStringToDate(search_company_holiday[i]
				        .getValue('custrecord_date'));

				if (holidayDate.getDay() != 0 && holidayDate.getDay() != 6) {
					holiday_list.push({
					    Subsidiary : search_company_holiday[i]
					            .getValue('custrecordsubsidiary'),
					    Date : holidayDate
					});
				}
			}
		}

		return holiday_list;
	} catch (err) {
		nlapiLogExecution('ERROR', 'get_company_holidays', err);
		throw err;
	}
}

function get_customer_holidays(start_date, end_date, customer) {
	var holiday_list = [];

	var search_customer_holiday = nlapiSearchRecord(
	        'customrecordcustomerholiday', 'customsearch_customer_holiday', [
	                new nlobjSearchFilter('custrecordholidaydate', null,
	                        'within', start_date, end_date),
	                new nlobjSearchFilter('custrecord13', null, 'anyof',
	                        customer) ], [
	                new nlobjSearchColumn('custrecordholidaydate', null,
	                        'group'),
	                new nlobjSearchColumn('custrecordcustomersubsidiary', null,
	                        'group') ]);

	if (search_customer_holiday) {

		for (var i = 0; i < search_customer_holiday.length; i++) {
			var holidayDate = nlapiStringToDate(search_customer_holiday[i]
			        .getValue('custrecordholidaydate', null, 'group'));

			if (holidayDate.getDay() != 0 && holidayDate.getDay() != 6) {
				holiday_list.push({
				    Subsidiary : search_customer_holiday[i].getValue(
				            'custrecordcustomersubsidiary', null, 'group'),
				    Date : holidayDate
				});
			}
		}
	}

	return holiday_list;
}

function getMonthName(currentDate) {
	currentDate = nlapiStringToDate(currentDate)
	var monthNames = [ "JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JUL", "AUG",
	        "SEP", "OCT", "NOV", "DEC" ];
	return monthNames[currentDate.getMonth()];
}

function getYear(currentDate) {
	return Math.round(currentDate.getFullYear()).toString();
}