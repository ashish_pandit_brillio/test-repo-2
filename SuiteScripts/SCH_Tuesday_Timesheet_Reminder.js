/**
 * Send reminder emails for all employees with open timesheets for the past week
 * 
 * Version Date Author Remarks 1.00 May 05 2015 Nitish Mishra
 * 
 */

var Timesheet_Approval_Status = {
	Approved: '3',
	PendingApproval: '2',
	Rejected: '4',
	Open: '1'
};

/**
 * @param {String}
 *            type Context Types: scheduled, ondemand, userinterface, aborted,
 *            skipped
 * @returns {Void}
 */
function scheduled(type) {
	nlapiLogExecution('debug', 'started');
	var a_table_data = new Array();
	a_table_data.push(["Employee ID", "Employee Name", "Error Code", "Error Message"])
	var b_err_flag = false;
		// get all employees that have saved the timesheet in open state for the
		// last week
		var arr_Employee_list_Particular_Project = [];
		var arr_Resrce_Allcn_Filter = [
			[[["job.type", "anyof", "2"], "AND", ["formulatext: {job.custentity_project_allocation_category}", "contains", "Practice Dedicated Resources"], "AND", ["job.status", "noneof", "1"]], "OR", [["job.type", "anyof", "2"], "AND", ["job.custentity_project_allocation_category", "anyof", "1"], "AND", ["job.status", "noneof", "1"]]],
			"AND",
			["job.internalid", "noneof", "157500"],
			"AND",
			["enddate", "onorafter", "today"]
		];
		var arr_Resrce_Allcn_col = [
			new nlobjSearchColumn("id").setSort(false),
			new nlobjSearchColumn("internalid", "resource", null)
		];

		var obj_Rsc_Allcn_Search = searchRecord('resourceallocation', null, arr_Resrce_Allcn_Filter, arr_Resrce_Allcn_col);
		if (obj_Rsc_Allcn_Search) {
			var i_Employee_Internal_id;
			nlapiLogExecution('DEBUG', 'obj_Rsc_Allcn_Search.length==', obj_Rsc_Allcn_Search.length);
			for (var i_Index_RA_Index = 0; i_Index_RA_Index < obj_Rsc_Allcn_Search.length; i_Index_RA_Index++) {
				i_Employee_Internal_id = obj_Rsc_Allcn_Search[i_Index_RA_Index].getValue("internalid", "resource", null);
				//	nlapiLogExecution('debug', 'id', i_Employee_Internal_id);
				if (arr_Employee_list_Particular_Project.indexOf(i_Employee_Internal_id) == -1) {
					arr_Employee_list_Particular_Project.push(i_Employee_Internal_id);
				}
			}
		}
		nlapiLogExecution('DEBUG', 'arr_Employee_list_Particular_Project.length==', arr_Employee_list_Particular_Project.length);

		var search_employee_submitted_timesheet = searchRecord('timesheet', null, [
			// new nlobjSearchFilter('internalid', 'employee', 'anyof', '9673'),
			new nlobjSearchFilter('approvalstatus', null, 'anyof',
				[Timesheet_Approval_Status.Open]),
			new nlobjSearchFilter('employee', null, 'anyof', arr_Employee_list_Particular_Project),
			new nlobjSearchFilter('timesheetdate', null, 'on',
				'startoflastweek')
	       /* new nlobjSearchFilter('custentity_employee_inactive', 'employee',
	        		'is', 'F')  */  ], [
			new nlobjSearchColumn('internalid', 'employee').setSort(),
			new nlobjSearchColumn('firstname', 'employee'),
			new nlobjSearchColumn('email', 'employee'),
			new nlobjSearchColumn('timeapprover', 'employee')]);

		nlapiLogExecution('debug',
			'no. of employees submitted the timesheet as open',
			search_employee_submitted_timesheet.length);

		// week dates
		var d_week_end_date = new Date();
		d_week_end_date = nlapiAddDays(d_week_end_date, -3);
		var d_week_start_date = nlapiAddDays(d_week_end_date, -6);
		var s_week_end_date = nlapiDateToString(d_week_end_date, 'date');
		var s_week_start_date = nlapiDateToString(d_week_start_date, 'date');
		var failedMails = [];
		var i_time_approver, cc = [];
		var s_time_approver_email;
		var context = nlapiGetContext();

		// send email to all not-submitted employees
		for (var index = 0; index < search_employee_submitted_timesheet.length; index++) {
			try {
				yieldScript(context);
				s_reporting_manager_email = null;
				s_time_approver_email = null;
				cc = [constant.EmailId.TimeSheet];

				nlapiLogExecution('DEBUG', 'Emp_Mail==', search_employee_submitted_timesheet[index].getValue(
					'email', 'employee'));

				var mailContent = tuesdayTimesheetReminderTemplate(
					search_employee_submitted_timesheet[index].getValue(
						'firstname', 'employee'), s_week_start_date,
					s_week_end_date);

				nlapiSendEmail(constant.Employee.InformationSystem,
					search_employee_submitted_timesheet[index].getValue(
						'email', 'employee'), mailContent.Subject,
					mailContent.Body, cc, null, {
					'entity': search_employee_submitted_timesheet[index]
						.getValue('internalid', 'employee')
				});

				nlapiLogExecution('debug', 'send to',
					search_employee_submitted_timesheet[index].getValue(
						'email', 'employee'));
			}
			catch (er) {
				b_err_flag = true;
				var a_row = new Array();
				a_row.push(all_employee_list[index].getId());
				a_row.push(all_employee_list[index].getValue('entityid'));
				a_row.push(err.code);
				a_row.push(err.message);
				a_table_data.push(a_row);
			}
		}
		if (b_err_flag) {
			Send_Exception_Mail(a_table_data);
		}
	nlapiLogExecution('debug', 'ended');
}

function tuesdayTimesheetReminderTemplate(first_name, week_start_date,
	week_end_date) {

	var htmltext = '<table border="0" width="100%">';
	htmltext += '<tr>';
	htmltext += '<td colspan="4" valign="top">';
	htmltext += '<p>Hi ' + first_name + ',</p>';
	htmltext += '<p></p>';

	htmltext += '<p>A gentle reminder!</p>';

	htmltext += "Your time sheet for the week <b>" + week_start_date + " - "
		+ week_end_date + "</b> is still in 'Open' status.";


	htmltext += "<p><b>Kindly make sure that you click on 'Submit' button to route the timesheet to the approval queue.</b></p>";
	htmltext += '<p></p>';
	htmltext += '<p>Note: All dates are in MM/DD/YYYY format. </p>';
	htmltext += '<p></p>';
	htmltext += 'Information Systems</p>';

	htmltext += '</td></tr>';
	htmltext += '</table>';
	htmltext += '<hr width="100%" size="1" noshade color="#CCCCCC">';
	htmltext += '<table width="100%" border="0" cellspacing="0" cellpadding="0">';
	htmltext += '<tr>';
	htmltext += '<td align="right">';
	htmltext += '<font style="font-size:9px; font-family:Verdana,Arial,Helvetica,sans-serif; color:#999999;">Brillio is powered by <a href="http://www.netsuite.com/" style="color:#999999;">NetSuite</a> &#151; One System. No Limits.</font>';
	htmltext += '</td>';
	htmltext += '</tr>';
	htmltext += '</table>';
	htmltext += '</body>';
	htmltext += '</html>';

	return {
		Subject: 'Weekly Timesheet Reminder : ' + week_start_date + " - "
			+ week_end_date + " - Timesheet is still Open",
		Body: htmltext
	};
}

function yieldScript(currentContext) {

	if (currentContext.getRemainingUsage() <= 100) {
		nlapiLogExecution('AUDIT', 'API Limit Exceeded');
		var state = nlapiYieldScript();

		if (state.status == "FAILURE") {
			nlapiLogExecution('ERROR', 'Yield Script Failed', 'Reason : '
				+ state.reason + ' / Size : ' + state.size);
			return false;
		} else if (state.status == "RESUME") {
			nlapiLogExecution('AUDIT', 'Script Resumed');
		}
	}
}
function Send_Exception_Mail(a_table_data) {
	try {
		var context = nlapiGetContext();
		var s_DeploymentID = context.getDeploymentId();
		var i_ScriptID = context.getScriptId();
		var s_Subject = 'Issue on SCH Tuesday Open TS Reminder';

		var s_Body = 'This is to inform that SCH Tuesday Open TS Reminder is having an issue and System is not able to send the email notification to the employees.';
		s_Body += '<br/>Script: ' + i_ScriptID;
		s_Body += '<br/>Deployment: ' + s_DeploymentID;
		s_Body += '<br/><table width="100%" border="1" cellspacing="1" cellpadding="1">';
		for (var index = 0; index < a_table_data.length; index++) {
			s_Body += '<tr>';
			s_Body += '<td>' + a_table_data[index][0] + '</td>';
			s_Body += '<td>' + a_table_data[index][1] + '</td>';
			s_Body += '<td>' + a_table_data[index][2] + '</td>';
			s_Body += '<td>' + a_table_data[index][3] + '</td>';
			s_Body += '</tr>';
		}
		s_Body += '</table>';
		s_Body += '<br/>Please have a look and do the needfull.';
		s_Body += '<br/><br/>Note: This is system genarated email, incase of any failure while sending the notification to employee(s)';

		s_Body += '<br/><br/>Regards,';
		s_Body += '<br/>Information Systems';
		nlapiSendEmail('422', 'netsuite.support@brillio.com', s_Subject, s_Body,null, null, null, null);
	}
	catch (err) {
		nlapiLogExecution('error', ' Send_Exeception_Mail', err.message);
	}
}
// END Send_Exception_Mail Function
