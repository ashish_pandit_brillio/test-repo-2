/**
 * Module Description
 * 
 * Version Date Author Remarks 1.00 FEB 7 2019 Sai Saranya
 * 
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your
 * script deployment.
 * 
 * @appliedtorecord recordType
 * 
 * @param {String}
 *        type Operation types: create, edit, delete, xedit, approve, cancel,
 *        reject (SO, ER, Time Bill, PO & RMA only) pack, ship (IF only)
 *        dropship, specialorder, orderitems (PO only) paybills (vendor
 *        payments)
 * @returns {Void}
 */
function userEventAfterSubmit(type) {

	try {
			var recid=nlapiGetRecordId();
    	  	var rectype=nlapiGetRecordType();
     	 	var rec =nlapiLoadRecord(rectype,recid);
			var status= rec.getFieldValue('custrecord_skill_approval_status');
			nlapiLogExecution('debug', 'type', type);
			nlapiLogExecution('debug', 'status', status);
     	 var reporting_manager = rec.getFieldValue('custrecord_skill_approver');
      var employee_name = rec.getFieldText('custrecord_skill_update_employee');
		if(reporting_manager){
		var emp_lookUP = nlapiLookupField('employee',parseInt(reporting_manager),['firstname','email']);
		var reporting_manager_name = emp_lookUP.firstname;
          var approver_mail = emp_lookUP.email;
		}
		if(reporting_manager_name)
		var name = reporting_manager_name;
		else
		var name = 'RM'
			//Approve Mail
			if(status == 1)
        	 {
            	var strVar = '';
			strVar += '<html>';
			strVar += '<body>';
			
			strVar += '<p>Dear '+name+',</p>';
		strVar += '<p>'+employee_name+' has updated the skill field on the tool. Request you to kindly validate the same via accessing below mentioned link.</p>';
		strVar += '<p>This activity will help in achieving following objectives:</p>';
			
		strVar += '<p>•	Determining Learning Plans for Brillians basis validated skills<br>';
		strVar += '   •	Redeployment of Brillian across Projects</p>';
		/*strVar += 'Customer:- '+s_project_cust+'<br>';
		strVar += 'Project:- '+s_project_name+'<br>';
		strVar += 'Project Start '+s_project_strt+' and End Date '+s_project_end+'<br>';
		strVar += 'Executing Practice:- '+s_project_prac+'</p>';*/
			
		strVar += '<a href=https://system.na1.netsuite.com/app/site/hosting/scriptlet.nl?script=1760&deploy=1>Link to Approve Skill Record in Netsuite</a>';
			
		strVar += '<p>Regards,<br>';
		strVar += 'Fulfilment Team</p>';
				
		strVar += '</body>';
		strVar += '</html>';
			
			var a_emp_attachment = new Array();
			a_emp_attachment['entity'] = '7905';
			var file_obj = nlapiLoadFile('857977');
			nlapiSendEmail(129799, approver_mail, 'Employee Skill Updated: '+employee_name+'.', strVar,null,'deepak.srinivas@brillio.com',a_emp_attachment,file_obj);
            
          }
		// if false, no need to send email
		else if (status == 3 ) {

				// get the person type
				var employeeId = rec.getFieldValue('custrecord_skill_update_employee');
				var mailContent = null;
				var employeeDetails = nlapiLookupField('employee', employeeId, [
					'firstname', 'email', 'custentity_persontype','custentity_skill_updated'
				]);
				 nlapiSubmitField('employee',employeeId, 'custentity_skill_updated', 'F');
				mailContent = getSalariedMailTemplate(employeeDetails);
				// send email to the new joinee
				if (isNotEmpty(mailContent)) {

					nlapiSendEmail(129799, employeeDetails.email,
							mailContent.Subject, mailContent.Body, null, null, {
								entity : employeeId
							});
                                        //sendNewJoineeEmail(employeeId);
				}
				else {
					throw "Invalid Person Type";
				}
			}
		}
	catch (err) {
		nlapiLogExecution('error', 'userEventAfterSubmit', err);
		throw err;
	}
}
function getSalariedMailTemplate(employeeDetails) {

	var htmlText = '<style> p,li {font-size : 11; font-family : "verdana"}</style>';
	htmlText += '<p>Hi ' + employeeDetails.firstname + ',<p>';
	htmlText +=
			'<p>Skills updated by you has been rejected, please reach out to your manager and re-update the skills in the portal. </p>'
					+ '<p>Please visit the below link to update your Skills and certification.</p>'
					+ '<a href=https://system.na1.netsuite.com/app/site/hosting/scriptlet.nl?script=1729&deploy=1>Skills and certification</a>'
					+ '<br/><p>Thanks and All The Best,<br/>' + 'Fulfillment Team</p>';

	return {
		Subject : 'Skill Update',
		Body : addMailTemplate(htmlText)
	};
}
