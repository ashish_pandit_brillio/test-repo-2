/**
 * Hide Some Columns for the employee center
 * 
 * Version Date Author Remarks
 * 
 * 1.00 11 Jul 2016 Nitish Mishra
 * 
 */

function userEventBeforeLoad(type, form, request) {
	try {

		if (type == 'create' || type == 'view' || type == 'edit') {
			var context = nlapiGetContext();
			
			//var lineField = nlapiGetLineItemField('expense', 'custcolexpense_emp_category');
			//lineField.setMandatory(true);
			if (context.getRoleCenter() == "EMPLOYEE") {
				
			//	var lineField = nlapiGetLineItemField('expense', 'custcolexpense_emp_category');
			//	lineField.setDisplayType('normal');
				// for defaulting the project and currency
				var user = nlapiGetFieldValue('entity');
				var allocationSearch = null;

				if (user) {
					allocationSearch = nlapiSearchRecord('resourceallocation',
					        null, [
					                new nlobjSearchFilter('resource', null,
					                        'anyof', user),
					                new nlobjSearchFilter('startdate', null,
					                        'notafter', 'today'),
					                new nlobjSearchFilter('enddate', null,
					                        'notbefore', 'today') ],
					        [ new nlobjSearchColumn('project') ]);
				}

				var defaultProject = '';

				if (allocationSearch && allocationSearch.length == 1) {
					defaultProject = allocationSearch[0].getValue('project');
				}

				nlapiLogExecution('debug', 'default project', defaultProject);
				var projectField = form.addField('custpage_default_project',
				        'select', 'Default Project', 'job');
				projectField.setDefaultValue(defaultProject);
				projectField.setDisplayType('hidden');

				var defaultCurrency = '';

				if (nlapiGetFieldValue('subsidiary') == 3) {
					defaultCurrency = '6';
				} else {
					defaultCurrency = '1';
				}

				nlapiLogExecution('debug', 'default currency', defaultCurrency);
				var currencyField = form.addField('custpage_default_currency',
				        'select', 'Default Currency', 'currency');
				currencyField.setDefaultValue(defaultCurrency);
				currencyField.setDisplayType('hidden');

				// hiding columns
				var expenseReport = nlapiCreateRecord('expensereport');
				var allColumns = expenseReport.getAllLineItemFields('expense')

				var columnsToShow = [ "expensedate","category", "custcolexpense_emp_category", "quantity","amount",
				        "rate", "foreignamount", "currency",  "memo",
				        "customer", "isbillable", "expmediaitem" ];

				allColumns
				        .forEach(function(fieldId) {

					        if (columnsToShow.indexOf(fieldId) == -1) {

						        var field = nlapiGetLineItemField('expense',
						                fieldId, 1);
						        if (field) {
							        field.setDisplayType('hidden');
						        }
					        }
				        });

				// hiding fields
				var fieldsToHide = [ 'advance', 
				        'custbody_transactiondate', 'tax1amt', 
				        'usemulticurrency', 'complete', 'supervisorapproval' ]; //, 'entity','subsidiary',

				if (type == 'create') {
					fieldsToHide.push('tranid');
					fieldsToHide.push('custbody_financemgrremarks');
				}

				fieldsToHide.forEach(function(fieldId) {
					form.getField(fieldId).setDisplayType('hidden');
				});

				form.getField('custbody1stlevelapprover').setBreakType(
				        'startcol');
				form.getField('custbody1stlevelapprover').setPadding(1);
			}
		}
	} catch (err) {
		nlapiLogExecution('ERROR', 'userEventBeforeLoad', err);
	}
}
