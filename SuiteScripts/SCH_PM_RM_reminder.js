// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
/*
   	Script Name: SCH_PM_RM_reminder
	Author: Vikrant
	Company: Aashna
	Date: 03-09-2014


	Script Modification Log:

	-- Date --			-- Modified By --				--Requested By--				-- Description --
	03-09-2014				-------							Jai Kumar					Initials draft for sending mail to PM for reminding about the timesheet unapproved.



Below is a summary of the process controls enforced by this script file.  The control logic is described
more fully, below, in the appropriate function headers and code blocks.


     SCHEDULED FUNCTION
		- SCH_PM_RM_reminder(type)


     SUB-FUNCTIONS
		- The following sub-functions are called by the above core functions in order to maintain code
            modularization:

               - NOT USED

*/
}
// END SCRIPT DESCRIPTION BLOCK  ====================================


// BEGIN SCHEDULED FUNCTION =============================================

function SCH_PM_RM_reminder(type) // 
{
	/*  On scheduled function:
	 - PURPOSE
	 -
	 FIELDS USED:
	 --Field Name--				--ID--
	 */
	//  LOCAL VARIABLES
	
	
	
	//  SCHEDULED FUNCTION CODE BODY
	try //
	{
		var context = nlapiGetContext();
		var usageRemaining = context.getRemainingUsage();
		
		var counter = context.getSetting('SCRIPT', 'custscript_pm_rem_counter');
		//nlapiLogExecution('DEBUG', 'SCH_PM_RM_reminder', 'counter : ' + counter);
		
		var filters = new Array();
		//filters[filters.length] = new nlobjSearchFilter('internalidnumber', 'supervisor', 'greaterthan', 0);
		
		//var search_result = nlapiSearchRecord('timebill', 'customsearch142', null, null); // SB Saved Search
		var search_result = nlapiSearchRecord('timebill', 'customsearch_pending_pm_ts_approve', filters, null); // Production Search
		if (_validate(search_result)) //
		{
			var count = search_result.length;
			nlapiLogExecution('DEBUG', 'SCH_PM_RM_reminder', 'count : ' + count);
			
			//return;
			
			var all_columns = search_result[0].getAllColumns();
			var project_name = '';
			var project_manager = '';
			var manager_email = '';
			
			var project_name_L = new Array();
			var emp_name_L = new Array();
			var mail = 0;
			for (var i = 0; i < count; i++) //
			{
				var current_manager_name = search_result[i].getValue(all_columns[2]);
				//nlapiLogExecution('DEBUG', 'SCH_PM_RM_reminder', 'current_manager_name : ' + current_manager_name);
				
				var emp_name = search_result[i].getText(all_columns[0]);
				//var emp_name = search_result[i].getValue(all_columns[0]);
				//nlapiLogExecution('DEBUG', 'SCH_PM_RM_reminder', 'emp_name : ' + emp_name);
				
				var pro_name = search_result[i].getValue(all_columns[1]);
				//nlapiLogExecution('DEBUG', 'SCH_PM_RM_reminder', 'pro_name : ' + pro_name);
				
				if (i == 0) //
				{
					project_manager = current_manager_name;
					project_name_L.push(pro_name);
					emp_name_L.push(emp_name);
					
					continue;
				}
				
				//nlapiLogExecution('DEBUG', 'SCH_PM_RM_reminder', 'project_manager : ' + project_manager);
				if (project_manager != current_manager_name || (i == count - 1)) // when project manager name changes send mail to manager and set the arrays to initial value
				{
					try //
					{
						nlapiLogExecution('DEBUG', 'SCH_PM_RM_reminder', 'Project manager changes...');
						
						var manager_rec = nlapiLoadRecord('employee', project_manager);
						//nlapiLogExecution('DEBUG', 'SCH_PM_RM_reminder', 'manager_rec : ' + manager_rec);
						var manager_F_name = '';
						
						if (_validate(manager_rec)) //
						{
							manager_F_name = manager_rec.getFieldValue('firstname');
							manager_email = manager_rec.getFieldValue('email');
						}
						else //
						{
							manager_F_name = '';
						}
						nlapiLogExecution('DEBUG', 'SCH_PM_RM_reminder', 'manager_F_name : ' + manager_F_name+', manager_email : ' + manager_email);
						//nlapiLogExecution('DEBUG', 'SCH_PM_RM_reminder', 'manager_email : ' + manager_email);
						
						var htmltext = '<table border="0" width="100%">';
						htmltext += '<tr>';
						htmltext += '<td colspan="4" valign="top">';
						htmltext += '<p>Hi ' + manager_F_name + ',</p>';
						htmltext += '<p>Below is a list of employee(s) timesheet due for PM Approval of the last week.</p>';
						htmltext += '</td></tr>';
						//htmltext += '<tr><td></td></tr>';
						//htmltext += '<tr><td></td></tr>';
						htmltext += '<tr ><td colspan = "2" font-size="14pt" align="center" bgcolor="#AEAEAE"><b>TimeSheet - PM Pending Approval</b></td></tr>';
						//htmltext += '<tr><td font-size="14pt" align="center"><b>Date</b></td><td font-size="14pt" align="center"><b>Customer</b></td><td font-size="14pt" align="center"><b>Project Manager</b></td><td font-size="14pt" align="center"><b>Employee</b></td><td font-size="14pt" align="center"><b>Hours</b></td></tr>';
						htmltext += '<tr><td font-size="14pt" align="center"><b>Employee Name</b></td><td font-size="14pt" align="center"><b>Project Name</b></td></tr>';
						
						//nlapiLogExecution('ERROR', 'SCH_PM_RM_reminder', 'emp_name_L : ' + emp_name_L);
						//nlapiLogExecution('ERROR', 'SCH_PM_RM_reminder', 'project_name_L : ' + project_name_L);
						//nlapiLogExecution('ERROR', 'SCH_PM_RM_reminder', 'emp_name_L.length : ' + emp_name_L.length);
						//nlapiLogExecution('ERROR', 'SCH_PM_RM_reminder', 'emp_name_L.length : ' + project_name_L.length);
						
						for (var j = 0; j < emp_name_L.length; j++) //
						{
							//nlapiLogExecution('ERROR', 'SCH_PM_RM_reminder', 'j : ' + j);
							htmltext += '<tr>';
							htmltext += '<td font-size="14pt" align="center">' + emp_name_L[j] + '</td>';
							htmltext += '<td font-size="14pt" align="center">' + project_name_L[j] + '</td>';
							htmltext += '</tr>';
						}
						
						htmltext += '<tr><td></td></tr>';
						htmltext += '<tr><td></td></tr>';
						//htmltext += '<tr><td></td></tr>';
						//htmltext += '<tr><td></td></tr>';
						htmltext += '<tr></tr>';
						htmltext += '<tr></tr>';
						htmltext += '<tr></tr>';
						htmltext += '<p>Regards,</p>';
						htmltext += '<p>Information Systems</p>';
						htmltext += '<tr><td colspan = "4"><b>Note:</b>This is a system generated email. In case you have any questions on the above data, please write to ';
						htmltext += '<a href="mailto:information.systems@brillio.com">information.systems@brillio.com</a></td></tr>';
						htmltext += '</table>';
						
						nlapiLogExecution('DEBUG', 'SCH_PM_RM_reminder', 'htmltext : ' + htmltext);
						try //
						{
							nlapiSendEmail(442, manager_email, 'TimeSheet - PM Pending Approval', htmltext, null, 'information.systems@brillio.com'); // produtions mail trigger.
							//nlapiSendEmail(442, 'vikrant@aashnacloudtech.com', 'TimeSheet - PM Pending Approval', htmltext, null, 'information.systems@brillio.com'); // commented after testing
							//nlapiLogExecution('DEBUG', 'SCH_PM_RM_reminder', 'Mail sent to project manager...');
							
							usageRemaining = context.getRemainingUsage();
							nlapiLogExecution('DEBUG', 'SCH_PM_RM_reminder ', ' usageRemaining -->' + usageRemaining + ', Manager Name : ' + manager_email);
							
							if (usageRemaining < 50) // use this at time of sending to all...
							//if (i == 10) // for testing purpose
							{
								// Define SYSTEM parameters to schedule the script to re-schedule.
								var params = new Array();
								params['status'] = 'scheduled';
								params['runasadmin'] = 'T';
								var startDate = new Date();
								params['startdate'] = startDate.toUTCString();
								
								// Define CUSTOM parameters to schedule the script to re-schedule.
								params['custscript_pm_rem_counter'] = i + 1;
								
								//nlapiLogExecution('DEBUG', 'SCH_PM_RM_reminder ', ' Script Status --> Before schedule...');
								
								var status = nlapiScheduleScript(nlapiGetContext().getScriptId(), nlapiGetContext().getDeploymentId(), params);
								//nlapiLogExecution('DEBUG', 'SCH_PM_RM_reminder ', ' Script Status -->' + status);
								
								////If script is scheduled then successfully then check for if status=queued
								if (status == 'QUEUED') //
								{
									nlapiLogExecution('DEBUG', ' SCH_PM_RM_reminder', 'Script is rescheduled ....................');
								}
								
								return;
							}
							
						//COMMMENT BELOW LINE WHILE EXECUTING IN PRODUCTION 
						//return;
						
						}
						catch (ex) //
						{
							nlapiLogExecution('ERROR', 'SCH_PM_RM_reminder', 'Mail not sent to project manager...');
							nlapiLogExecution('ERROR', 'SCH_PM_RM_reminder', 'ex : ' + ex);
						}
						
						project_manager = current_manager_name;
						
						project_name_L = new Array();
						emp_name_L = new Array();
						
						project_name_L.push(pro_name);
						emp_name_L.push(emp_name);
						
						if (mail == 2) //
						{
						//return; // this is a exception setting, which causes stop of execution after 3 mails it is enabled only while testing.
						}
						else //
						{
							mail++;
						//nlapiLogExecution('DEBUG', 'SCH_PM_RM_reminder', 'project_manager : ' + project_manager);
						}
					} 
					catch (ex)//
					{
						nlapiLogExecution('ERROR', 'SCH_PM_RM_reminder', 'Error while executing following project manager... ' + ex);
						nlapiLogExecution('ERROR', 'SCH_PM_RM_reminder', 'ex.message : ' + ex.message);
					}
					
				}
				else // if project manager name is same add the employee name and project name to array
				{
					//nlapiLogExecution('DEBUG', 'SCH_PM_RM_reminder', 'Project manager is same ...');
					
					//project_name_L[i] = pro_name;
					//emp_name_L[i] = emp_name;
					
					project_name_L.push(pro_name);
					emp_name_L.push(emp_name);
				}
			}
		}
		else //
		{
			nlapiLogExecution('DEBUG', 'SCH_PM_RM_reminder', 'search_result is invalid : ' + search_result);
		}
	} 
	catch (ex) //
	{
		nlapiLogExecution('ERROR', 'SCH_PM_RM_reminder', 'ex : ' + ex);
		nlapiLogExecution('ERROR', 'SCH_PM_RM_reminder', 'ex.message : ' + ex.message);
	}
}

// END SCHEDULED FUNCTION ===============================================

function _validate(obj) //
{
	if (obj != null && obj != '' && obj != 'undefined')//
	{
		return true;
	}
	else //
	{
		return false;
	}
}



// BEGIN FUNCTION ===================================================
{



}
// END FUNCTION =====================================================
