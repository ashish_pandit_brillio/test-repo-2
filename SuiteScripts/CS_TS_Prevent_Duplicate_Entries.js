/**
 * check if any project and task are not duplicated in the TimeSheet
 * 
 * Version Date Author Remarks 1.00 23 Feb 2015 Nitish Mishra Created the file
 * 
 * Version Date Author Remarks 2.00 28 Apr 2015 Nitish Mishra Refactored the
 * file and removed unused code
 * Version Date Author Remarks 3.00 16 Mar 2020 Praveena Madem Changed time sheet and time tracking records related data's iternalid
 * file and removed unused code
 */

/**
 * check if any project and task are not duplicated in the TimeSheet
 * 
 * @appliedtorecord TimeSheet
 * 
 * @returns {Boolean} True to continue save, false to abort save
 */
 
 
 
 
 function checkForDuplicateEntries() {

	try {
		// get the timesheet status
		var ts_status = nlapiGetFieldValue('approvalstatus');

		if (ts_status != '4') {
			var i_line_count = nlapiGetLineItemCount('timeitem');
			var i_current_project_id = null;
			var i_current_item_id = null;

			// loop through all the time grid
			for (var i_line_indx = 1; i_line_indx <= i_line_count; i_line_indx++) {
				i_current_project_id = nlapiGetLineItemValue('timeitem',
						'customer', i_line_indx);
				i_current_item_id = nlapiGetLineItemValue('timeitem',
						'casetaskevent', i_line_indx);

				// loop through time grids ahead of the current one and check
				// for
				// duplicate
				for (var j_line_indx = i_line_indx + 1; j_line_indx <= i_line_count; j_line_indx++) {
					var i_next_project_id = nlapiGetLineItemValue('timeitem',
							'customer', j_line_indx);
					var i_next_item_id = nlapiGetLineItemValue('timeitem',
							'casetaskevent', j_line_indx);

					if (i_next_project_id == i_current_project_id
							&& i_current_item_id == i_next_item_id) {
						alert('For a given day for the same project, task should not be duplicated');
						//return false;
						return true;
					}
				}
			}
		}

		return true;
	} catch (err) {
		nlapiLogExecution('ERROR', 'checkForDuplicateEntries', err);
		alert(err.message);
	}
}
