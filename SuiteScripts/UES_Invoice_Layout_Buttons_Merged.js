// BEGIN SCRIPT DESCRIPTION BLOCK ==================================
/*
	Date		: 24/11/2021
	Author		: Raghav Gupta
	Remarks		: Userevent Script to load buttons that generates invoice pdf depedning upon layout type and subsidary.
	Script Name	: UES_Invoice_Layout_Buttons_Merged.js
	Script Type	: UserEvent Script (1.0)

	Script Modification Log:s
	-- Date --			-- Modified By --				--Requested By--				-- Description --

	Function used and their Descriptions:
	beforeLoad_Invoice_Buttons FUNCTION		: Main function to generate the button to generate PDF(s).
*/
// END SCRIPT DESCRIPTION BLOCK ==================================


// BEGIN beforeLoad_Invoice_Buttons
function beforeLoad_Invoice_Buttons(type, form) {
	try {
		form.setScript('customscript_cli_invoice_layout_buttons');
		if (type == 'view') {
			var i_rcrd_id = nlapiGetRecordId();
			var i_layout_type = nlapiGetFieldValue('custbody_layout_type');

			if (i_layout_type == 1 || i_layout_type == 2 || i_layout_type == 3 || i_layout_type == 4 || i_layout_type == 17 || i_layout_type == 18 || i_layout_type == 19 || i_layout_type == 20) {
				form.addButton('custpage_Invoice_PDF_button', 'Generate Invoice', 'open_Invoice()');
			}// Common Invoice Button

			if (i_layout_type == 15) {
				form.addButton('custpage_llc_comity_tm', 'Generate Invoice', 'open_BLLC_Comity_TM()');
			}// BLLC Comity T&M Invoice

			if (i_layout_type == 16) {
				form.addButton('custpage_llc_comity_fp', 'Generate Invoice', 'open_BLLC_Comity_FP()');
			}// BLLC Comity FP Invoice

			var i_subsidiary = nlapiGetFieldValue('subsidiary');
			if (i_subsidiary == 3) {
				form.addButton('custpage_addbutton_BTPL', 'Generate Invoice', 'open_BTPL_Invoice(\'' + i_rcrd_id + '\');');
			}// BTPL PDF for subsidary BTPL
		}
	}

	catch (error) {
		nlapiLogExecution('ERROR', 'Processing PDF Error', error);
	}
	return true;
}
// END beforeLoad_Invoice_Buttons