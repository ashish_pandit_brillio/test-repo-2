/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       29 June 2017     Jayesh Dinde
 *
 */
// Commented because suitelet is changed to library file due to invoice bulk approval process
//function sutCreateVendorBill(request, response){
function sutCreateVendorBill(invoice_details) {
    try {

        nlapiLogExecution('audit', 'invoice_details:- ' + invoice_details);
        // Get the list of Time Entries
        //var strRequest = request.getBody();

        var a_data = new Array(); // Store Time Entry data

        var o_request = JSON.parse(invoice_details);

        var i_employee_id = o_request.employee;

        //Look up Added By praveena
		var i_invoice_inter_id=o_request.i_invoice_id;
        var lookup_inv_obj = nlapiLookupField('invoice',i_invoice_inter_id, ['tranid']);
        var s_invoice_no = lookup_inv_obj.tranid; //FETCH TRAN ID OF INVOICE USING LOOKUPD

        nlapiLogExecution('audit', 'i_employee_id:- ' + i_employee_id);
        nlapiLogExecution('audit', 'date range:- ' + new Date(o_request.from), new Date(o_request.to));
        //return;

        var a_list = new Array();

        var a_days = ['sunday', 'monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday'];
		var d_from_date=new Date(o_request.from);
		var d_to_date=new Date(o_request.to);
        var a_vendor_details = getVendorDetails(o_request.employee,d_from_date,d_to_date);
        if (a_vendor_details) {

nlapiLogExecution('DEBUG', 'a_vendor_details:- ',JSON.stringify(a_vendor_details));

            //var i_vendor_id = a_vendor_details[0].vendor_id;//Commented by Praveena on 22-05-2020
            var i_list_indx1 = 0; //praveena@22-05-2020: Adding vendors;
            var a_all_list = []; //praveena@22-05-2020 : Declare array to store all time entries internalid.
            for (var ii = 0; a_vendor_details.length > ii; ii++) {//praveena@22-05-2020 : multiple subtier records iternation 
                var i_vendor_id = a_vendor_details[ii].vendor_id;
                var d_temp_from_date = '';
                var d_temp_end_date = '';
                var d_stvd_startdate = new Date(a_vendor_details[ii].stvd_startdate);//Startdate of subtier record
                var d_stvd_enddate = new Date(a_vendor_details[ii].stvd_enddate);//EndDate of subtier record

                d_temp_from_date = d_stvd_startdate > d_from_date ? d_stvd_startdate : d_from_date; //Update start date as subtier end date in case if its mid of the month
                d_temp_end_date = d_stvd_enddate < d_to_date ? d_stvd_enddate : d_to_date; //Update End date as subtier end date in case if its mid of the month

                // Loop through the time entries
                if (i_employee_id) {
                    var filters = new Array();
                    filters[0] = new nlobjSearchFilter('type', null, 'anyof', 'A');
                    //filters[1] = new nlobjSearchFilter('date', null, 'within', new Date(o_request.from), new Date(o_request.to));//Commented by Praveena
                    filters[1] = new nlobjSearchFilter('date', null, 'within', d_temp_from_date, d_temp_end_date);//added by praveena
                    filters[2] = new nlobjSearchFilter('custcol_subtierpay', null, 'is', 'F');
                    filters[3] = new nlobjSearchFilter('jobbillingtype', 'job', 'is', 'TM');
                    filters[4] = new nlobjSearchFilter('approvalstatus', null, 'anyof', '3');

                    filters[filters.length] = new nlobjSearchFilter('status', null, 'is', 'F');
                    filters[filters.length] = new nlobjSearchFilter('employee', null, 'anyof', i_employee_id);

                    var columns = new Array();
                    columns[0] = new nlobjSearchColumn('internalid');
                    columns[1] = new nlobjSearchColumn('employee');
                    columns[2] = new nlobjSearchColumn('customer', 'job');
                    columns[3] = new nlobjSearchColumn('custcolprj_name');
                    columns[4] = new nlobjSearchColumn('item');
                    columns[5] = new nlobjSearchColumn('durationdecimal');
                    columns[6] = new nlobjSearchColumn('custentity_pofrom', 'employee');
                    columns[7] = new nlobjSearchColumn('custentity_poto', 'employee');
                    columns[8] = new nlobjSearchColumn('department');
                    columns[9] = new nlobjSearchColumn('custentity_vertical', 'job');
                    columns[10] = new nlobjSearchColumn('date');
                    columns[11] = new nlobjSearchColumn('internalid', 'job');
                    columns[12] = new nlobjSearchColumn('entityid', 'job');
                    columns[13] = new nlobjSearchColumn('companyname', 'job');
                    columns[14] = new nlobjSearchColumn('jobname', 'job');
                    columns[15] = new nlobjSearchColumn('item');


                    // Search Records
                    var search_results = nlapiSearchRecord('timebill', null, filters, columns);

                    if (search_results) {
                        var a_billedTimeEntries = getBilledTimeEntriesArray(d_temp_from_date, d_temp_end_date, i_employee_id);

                        for (var i_search_indx = 0; i_search_indx < search_results.length; i_search_indx++) {
                            var o_search_result = search_results[i_search_indx];
                            var i_timeentry_id = o_search_result.getValue('internalid');

                            if (a_billedTimeEntries.indexOf(i_timeentry_id) == -1) {

                                var s_employee_name_id = '';
                                a_list.push(o_search_result.getValue('internalid'));

                                var employeeId = o_search_result.getValue('employee');
                                var employeeName = o_search_result.getText('employee');

                                if (employeeName) {
                                    s_employee_name_id = employeeName.split('-');
                                    s_employee_name_id = s_employee_name_id[0];
                                    //nlapiLogExecution('audit','s_employee_name',s_employee_name_id);
                                }

                                var projectId = o_search_result.getValue('internalid', 'job');
                                var duration = parseFloat(o_search_result.getValue('durationdecimal'));
                                var stRate = a_vendor_details[ii].st_rate;
                                var otRate = a_vendor_details[ii].ot_rate;
                                var i_practice = o_search_result.getValue('department');

                                var i_vertical = o_search_result.getValue('custentity_vertical', 'job');
                                var i_customer_name = o_search_result.getText('customer', 'job');
                                var i_customer_id = o_search_result.getValue('customer', 'job');
                                var cust_territory = nlapiLookupField('customer', i_customer_id, 'territory');

                                var i_project_name_ID = o_search_result.getValue('entityid', 'job');
                                var i_project_name = o_search_result.getValue('jobname', 'job');
                                var s_project_description = i_project_name_ID + ' ' + i_project_name;


                                var itemId = o_search_result.getValue('item');

                                var type = null;
                                var rate = 0.0;

                                if (itemId == '2222' || itemId == '2221' || itemId == 'FP' || itemId == 'ST') {//Added FP and ST by praveena
                                    type = 'ST';
                                    rate = stRate;
                                } else if (itemId == '2425' || itemId == 'OT') {//Added OT by praveena
                                    type = 'OT';
                                    rate = otRate;
                                } else {
                                    type = 'OTHER';
                                }
                                a_all_list.push(i_timeentry_id); //push internalid of time entry
                                a_data[i_list_indx1] = {
                                    'employee': employeeId,
                                    'employee_name': employeeName,
                                    'employee_id': s_employee_name_id,
                                    'project_id': i_project_name_ID,
                                    'project': projectId,
                                    'item': itemId,
                                    'type': type,
                                    'rate': rate,
                                    'duration': duration,
                                    'practice': i_practice,
                                    'vertical': i_vertical,
                                    'customer_name': i_customer_name,
                                    'cust_territory': cust_territory,
                                    'project_description': s_project_description,
                                    'internalid': i_timeentry_id,//Added by praveena@22-05-2020
                                    'billing_from': d_temp_from_date,//Added by praveena@22-05-2020
                                    'billing_to': d_temp_end_date,//Added by praveena@22-05-2020
                                    'i_vendor_id': i_vendor_id//Added by praveena@22-05-2020

                                };
								i_list_indx1++;
                            }
                        }
                    }
                }
            } //for loop vendor subtier iteration


            var a_grouped_data = groupTimesheetData(a_data);
            nlapiLogExecution("DEBUG", 'a_grouped_data', JSON.stringify(a_grouped_data));

            //**GROUPING BY BILLING END DATE *****///
            var a_group_billingdates_data = a_grouped_data.reduce(function(a, e) {
                // GROUP BY estimated key (estKey), well, may be a just plain key
                // a -- Accumulator result object
                // e -- sequentally checked Element, the Element that is tested just at this itaration
                // new grouping name may be calculated, but must be based on real value of real field
                var estKey = (e['billing_to']);
                (a[estKey] ? a[estKey] : (a[estKey] = null || [])).push(e);
                return a;
            }, {});

            //****END GROUPING BY BILLING END DATE*****/
            nlapiLogExecution("DEBUG", 'a_grouped_data', JSON.stringify(a_group_billingdates_data));

            var a_vendor_bill_id = []; //Declare vendor bill internalids to set on invoice and send as resonse of newly created vendor bill ;
            var status = 1;
            for (var billdate in a_group_billingdates_data) //FOr loop with different subtier records
            {

                var a_grouped_data = "";
                a_grouped_data = a_group_billingdates_data[billdate];

                var recVendorBill = "";
                for (var i_grouped_data_indx = 0; i_grouped_data_indx < a_grouped_data.length; i_grouped_data_indx++) {

                    if (i_grouped_data_indx == 0) //Logic is added in case of different vendors in case of mid of the month
                    {
                        var i_vendor_id = a_grouped_data[i_grouped_data_indx].i_vendor_id; //VENDOR ID FROM SUBTIER RECORD

                        recVendorBill = nlapiCreateRecord('vendorbill', {
                            'entity': i_vendor_id
                        }); //CREATE VENDOR BILL
                        recVendorBill.setFieldValue('customform', 119);
                        //recVendorBill.setFieldValue('custbody_financemanager', 5764);
                        recVendorBill.setFieldValue('custbody_billfrom', nlapiDateToString(a_grouped_data[i_grouped_data_indx].billing_from)); //SET BILLING FROM subtier record or requested date
                        recVendorBill.setFieldValue('custbody_billto', nlapiDateToString(a_grouped_data[i_grouped_data_indx].billing_to)); //SET BILLING TO subtier record or requested date

                    }//END i_grouped_data_indx == 0

                    i_item_id = null;

                    var i_line_item_indx = (i_grouped_data_indx + 1);
                    var o_grouped_data = a_grouped_data[i_grouped_data_indx];

                    switch (a_grouped_data[i_grouped_data_indx].type) {
                        case 'ST':
                            i_item_id = 2443;
                            break;
                        case 'OT':
                            i_item_id = 2444;
                            break;
                    }
                    recVendorBill.setLineItemValue('item', 'item', i_line_item_indx, i_item_id);
                    recVendorBill.setLineItemValue('item', 'custcol_temp_project', i_line_item_indx, o_grouped_data.project);
                    recVendorBill.setLineItemValue('item', 'custcol_employeenamecolumn', i_line_item_indx, o_grouped_data.employee_name);
                    recVendorBill.setLineItemValue('item', 'custcol_project_entity_id', i_line_item_indx, o_grouped_data.project_id);
                    recVendorBill.setLineItemValue('item', 'custcol_employee_entity_id', i_line_item_indx, o_grouped_data.employee_id);
                    recVendorBill.setLineItemValue('item', 'rate', i_line_item_indx, o_grouped_data.rate);
                    recVendorBill.setLineItemValue('item', 'quantity', i_line_item_indx, o_grouped_data.duration);
                    recVendorBill.setLineItemValue('item', 'custcolcustcol_temp_customer', i_line_item_indx, o_grouped_data.customer_name);
                    recVendorBill.setLineItemValue('item', 'department', i_line_item_indx, o_grouped_data.practice);
                    recVendorBill.setLineItemValue('item', 'custcolprj_name', i_line_item_indx, o_grouped_data.project_description);
                    recVendorBill.setLineItemValue('item', 'class', i_line_item_indx, o_grouped_data.vertical);
                    recVendorBill.setLineItemValue('item', 'custcol_territory', i_line_item_indx, o_grouped_data.cust_territory);
                    recVendorBill.setFieldValue('custbody_consultantname', o_grouped_data.employee);
                }

                var i_vendor_bill_id = null;


                var a_actions = new Array();
                var a_list = [];
                a_list = o_grouped_data.a_tb_internalids;
                for (var i_timesheet_indx = 0; i_timesheet_indx < a_list.length; i_timesheet_indx++) {
                    var i_timeentry_id = a_list[i_timesheet_indx];

                    var filters = new Array();
                    filters[0] = new nlobjSearchFilter('custrecord_time_entry_id_tb', null, 'anyof', i_timeentry_id);

                    var columns = new Array();
                    columns[0] = new nlobjSearchColumn('internalid');
                    columns[1] = new nlobjSearchColumn('custrecord_vendor_bill');

                    var search_existing_entries = nlapiSearchRecord('customrecord_ts_extra_attributes', null, filters, columns);

                    if (search_existing_entries == null) {
                        a_actions.push({
                            'type': 'create',
                            'time_entry_id': i_timeentry_id
                        });
                    } else if (search_existing_entries.length == 1 && search_existing_entries[0].getValue('custrecord_vendor_bill') == '') {
                        a_actions.push({
                            'type': 'update',
                            'internalid': search_existing_entries[0].getValue('internalid')
                        });
                    } else {
                        status = 0;
                    }
                }

                if (status == 1 && a_grouped_data.length > 0) {
                    try {
                        nlapiLogExecution('audit', 'Status 1', 'In first loop');
                       // var a_existing_bill_id = new Array();

                        recVendorBill.setFieldValue('custbody_oldinvoicenumber', s_invoice_no);
                        recVendorBill.setFieldValue('custbody_created_by_user', 100325);

                        i_vendor_bill_id = nlapiSubmitRecord(recVendorBill, true, true);
						nlapiLogExecution('audit', 'i_vendor_bill_id', i_vendor_bill_id);
						
						a_vendor_bill_id.push(i_vendor_bill_id);
                    } catch (e) {
                        nlapiLogExecution('ERROR', 'Error', e);
                        status = 0;
                    }
                }

                if (status == 1) {
                    for (var i = 0; i < a_actions.length; i++) {
                        nlapiLogExecution('audit', 'a_actions[i].type Create', a_actions[i].type);
                        if (a_actions[i].type == 'create') {
                            nlapiLogExecution('audit', 'timeentry id:- ' + a_actions[i].time_entry_id, 'vendor id:- ' + i_vendor_bill_id);
                            var newMappingRecord = nlapiCreateRecord('customrecord_ts_extra_attributes');
                            newMappingRecord.setFieldValue('custrecord_time_entry_id_tb', a_actions[i].time_entry_id);
                            newMappingRecord.setFieldValue('custrecord_vendor_bill', i_vendor_bill_id);
							newMappingRecord.setFieldValue('custrecord_provision_created', 'T');
                            i_mapping_record_id = nlapiSubmitRecord(newMappingRecord);
                           // nlapiSubmitField('customrecord_ts_extra_attributes', i_mapping_record_id, 'custrecord_provision_created', 'T');

                        } else if (a_actions[i].type == 'update') {
                            nlapiLogExecution('audit', 'a_actions[i].type Update', a_actions[i].type);
                            var i_mapping_record_id = a_actions[i].internalid; //search_existing_entries[0].getValue('internalid');
                            nlapiSubmitField('customrecord_ts_extra_attributes', i_mapping_record_id, ['custrecord_vendor_bill', 'custrecord_provision_created'], [i_vendor_bill_id, 'T']);

                        }
                    }
                }
            } //FOr loop with different subtier records
			var a_existing_bill_id=[];
            var o_invoice_rcrd = nlapiLoadRecord('invoice', i_invoice_inter_id);
            if (o_invoice_rcrd) {
				
                nlapiLogExecution('audit', 'o_invoice_rcrd:- ' + o_invoice_rcrd, 'invoice id:- ' + i_invoice_inter_id);
                var a_axisting_bills = o_invoice_rcrd['custbody_auto_invoice_bllc'];
                if (a_axisting_bills) {
                    for (var j = 0; j < a_axisting_bills.length; j++) {
                        a_existing_bill_id.push(a_axisting_bills[j]);
                    }
                }

            }
            if (a_vendor_bill_id) {

                for (var l = 0; a_vendor_bill_id.length > l; l++) {
                    nlapiLogExecution('audit', 'vendor bill id:- ' + i_vendor_bill_id);
                    a_existing_bill_id.push(a_vendor_bill_id[l]);
                }
                o_invoice_rcrd.setFieldValues('custbody_auto_invoice_bllc', a_existing_bill_id);
                o_invoice_rcrd.setFieldValue('custbody_auto_vendor_bill_invoice', 'F');
                nlapiSubmitRecord(o_invoice_rcrd, true, true);
                //nlapiSubmitRecord(o_invoice_rcrd, {disabletriggers : true, enablesourcing : true});
                //Code added for the issue 
                /*var fields = new Array(); 
                var values = new Array();
                fields[0] = 'custbody_auto_invoice_bllc';
                values[0] = a_existing_bill_id;
                fields[1] = 'custbody_auto_vendor_bill_invoice';
                values[1] = 'F';
                var updatefields = nlapiSubmitField('invoice',o_invoice_rcrd, fields, values);
                nlapiLogExecution('Debug','updatefields ',updatefields); */
            }




            var o_response = new Object();
            o_response.status = status;
            //commented by praveena
            //o_response.vendor_bill_id = i_vendor_bill_id;
            //o_response.list = a_all_list;

            // Added logic send response for multiple vendors : @22-05-2020 Praveena
            o_response.vendor_bill_id = a_vendor_bill_id;
            o_response.list = a_list;


            var context = nlapiGetContext();
            var usageRemaining = context.getRemainingUsage();
            nlapiLogExecution('AUDIT', 'Remaining Usage: ', usageRemaining);

            return JSON.stringify(o_response);
            //response.write(JSON.stringify(o_response));
        }
    } catch (e) {
        var o_invoice_rcrd = nlapiLoadRecord('invoice', o_request.i_invoice_id);
        if (o_invoice_rcrd) {
            o_invoice_rcrd.setFieldValue('custbody_auto_vendor_bill_invoice', 'F');
            //nlapiSubmitRecord(o_invoice_rcrd,true,true);
            nlapiSubmitRecord(o_invoice_rcrd, {
                disabletriggers: true,
                enablesourcing: true
            });
        }
        nlapiLogExecution('DEBUG', 'Error occured', e);
    }
}

function groupTimesheetData(a_data) {
    var a_grouped_data = new Array();

    for (var i_timesheet_data_indx = 0; i_timesheet_data_indx < a_data.length; i_timesheet_data_indx++) {
        o_timesheet_data = a_data[i_timesheet_data_indx];

        var groupedRecordFound = false;
        // Search for existing group record
        for (var i_group_data_indx = 0; i_group_data_indx < a_grouped_data.length; i_group_data_indx++) {
            o_grouped_data = a_grouped_data[i_group_data_indx];

		//Changed logic to add employee
            if (o_timesheet_data.employee == o_grouped_data.employee &&
                o_timesheet_data.project == o_grouped_data.project &&
                o_timesheet_data.item == o_grouped_data.item &&
                o_timesheet_data.rate == o_grouped_data.rate &&
                o_timesheet_data.billing_from == o_grouped_data.billing_from &&
                o_timesheet_data.billing_to == o_grouped_data.billing_to &&
                o_timesheet_data.i_vendor_id == o_timesheet_data.i_vendor_id) {
                groupedRecordFound = true;
                a_grouped_data[i_group_data_indx].duration += parseFloat(o_timesheet_data.duration);
                a_grouped_data[i_group_data_indx]["a_tb_internalids"].push(o_timesheet_data['internalid']);
            }
        }
        if (groupedRecordFound == false) {
            var o_new_timesheet_data = new Object();
            for (var i_field in o_timesheet_data) {
                o_new_timesheet_data[i_field] = o_timesheet_data[i_field];
            }
            o_new_timesheet_data["a_tb_internalids"] = [];
            o_new_timesheet_data["a_tb_internalids"].push(o_timesheet_data['internalid']);
            a_grouped_data.push(o_new_timesheet_data);

        }
    }

    return a_grouped_data;
}

function _is_Valid(obj) //
{
    if (obj != null && obj != '' && obj != 'undefined' && obj != undefined) //
    {
        return true;
    } else //
    {
        return false;
    }
}

function _correct_time(t_time) //
{
    // function is used to correct the time 

    if (_is_Valid(t_time)) //
    {
        //nlapiLogExecution('DEBUG', '_correct_time', 't_time : ' + t_time);

        var hrs = t_time.split(':')[0];
        //nlapiLogExecution('DEBUG', '_correct_time', 'hrs : ' + hrs);

        if (t_time.indexOf(':') > -1) //
        {
            var mins = t_time.split(':')[1];
            //nlapiLogExecution('DEBUG', '_correct_time', 'mins : ' + mins);

            if (_is_Valid(mins)) //
            {
                mins = parseFloat(mins) / parseFloat('60');
                //nlapiLogExecution('DEBUG', '_correct_time', 'after correct mins : ' + mins);

                hrs = parseFloat(hrs) + parseFloat(mins);
                //nlapiLogExecution('DEBUG', '_correct_time', 'after adding mins to hrs : ' + hrs);
            }
        }
        //nlapiLogExecution('DEBUG', '_correct_time', 'hrs : ' + hrs);
        return hrs;
    } else //
    {
        return 0;
        //nlapiLogExecution('DEBUG', '_correct_time', 't_time is invalid : ' + t_time);
    }
}

// This function will return the vendor details
function getVendorDetails(i_employee, d_from_date, d_to_date) {
    /*
    var vendor_filter	=	new Array();
    vendor_filter[0]	=	new nlobjSearchFilter('custrecord_stvd_start_date', null, 'onorbefore', d_to_date);
    vendor_filter[1]	=	new nlobjSearchFilter('custrecord_stvd_end_date', null, 'onorafter', d_from_date);
    vendor_filter[2]	=	new nlobjSearchFilter('custrecord_stvd_contractor', null, 'anyof', i_employee);
    vendor_filter[3] =	new nlobjSearchFilter('isinactive', null, 'is', 'F');
    */

    var s_from_date = nlapiDateToString(d_from_date);
    var s_to_date = nlapiDateToString(d_to_date);

    var vendor_filter = [
        [
            ["custrecord_stvd_start_date", "within", s_from_date, s_to_date], "OR", ["custrecord_stvd_start_date", "onorbefore", s_from_date]
        ],
        "AND",
        [
            ["custrecord_stvd_end_date", "within", s_from_date, s_to_date], "OR", ["custrecord_stvd_end_date", "onorafter", s_to_date]
        ],
        "AND",
        ["custrecord_stvd_contractor", "anyof", i_employee],
        "AND",
        ["isinactive", "is", 'F']
    ];

    var vendor_columns = new Array();
    vendor_columns[0] = new nlobjSearchColumn('custrecord_stvd_st_pay_rate');
    vendor_columns[1] = new nlobjSearchColumn('custrecord_stvd_ot_pay_rate');
    vendor_columns[2] = new nlobjSearchColumn('custrecord_stvd_payment_terms');
    vendor_columns[3] = new nlobjSearchColumn('custrecord_stvd_vendor');
	vendor_columns[4] = new nlobjSearchColumn('custrecord_stvd_start_date');
    vendor_columns[5] = new nlobjSearchColumn('custrecord_stvd_end_date');
    var a_search_vendor_details = nlapiSearchRecord('customrecord_subtier_vendor_data', null, vendor_filter, vendor_columns);

    var i_count = a_search_vendor_details == null ? 0 : a_search_vendor_details.length;

    var a_vendor_details = new Array();

    for (var i = 0; i < i_count; i++) {
        a_vendor_details.push({
            'st_rate': a_search_vendor_details[i].getValue('custrecord_stvd_st_pay_rate'),
            'ot_rate': a_search_vendor_details[i].getValue('custrecord_stvd_ot_pay_rate'),
            'payment_terms': a_search_vendor_details[i].getText('custrecord_stvd_payment_terms'),
            'vendor_name': a_search_vendor_details[i].getText('custrecord_stvd_vendor'),
            'vendor_id': a_search_vendor_details[i].getValue('custrecord_stvd_vendor')

                //Fetch Subtier start date and end date : Added by praveena
                ,
            'stvd_startdate': a_search_vendor_details[i].getValue('custrecord_stvd_start_date'),
            'stvd_enddate': a_search_vendor_details[i].getValue('custrecord_stvd_end_date')
        });
    }

    return a_vendor_details;
}

function getBilledTimeEntriesArray(d_from_date, d_to_date, i_employee) {
    // Get billed time entries
    var filters = new Array();
    filters[0] = new nlobjSearchFilter('date', 'custrecord_time_entry_id_tb', 'within', d_from_date, d_to_date);
    if (i_employee != '') {
        filters[1] = new nlobjSearchFilter('employee', 'custrecord_time_entry_id_tb', 'anyof', i_employee);
    }

    var columns = new Array();
    columns[0] = new nlobjSearchColumn('internalid', 'custrecord_time_entry_id_tb');

    var a_search_result = nlapiSearchRecord('customrecord_ts_extra_attributes', null, filters, columns);
    var a_list = new Array();

    if (!a_search_result) {
        return a_list;
    }

    for (var i = 0; i < a_search_result.length; i++) {
        a_list.push(a_search_result[i].getValue('internalid', 'custrecord_time_entry_id_tb'));
    }
    return a_list;
}

function getTimesheetId(timeEntryId) {
    var timeentrySearch = nlapiSearchRecord("timeentry", null,
        [
            ["internalidnumber", "equalto", timeEntryId]
        ],
        [
            new nlobjSearchColumn("timesheet")
        ]
    );
    var timesheetId = timeentrySearch[0].getValue('timesheet');
    return timesheetId;
}