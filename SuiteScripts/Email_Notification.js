/*
	Email notification
	1. Project Managers for approving Timesheet
		1.1 Every Monday morning
		1.2 Follow up next day
		1.3 Escalation to DM
	2. Mass emailers for every resource to fill timesheet
		2.1 Every Friday
		2.2 Follow up on Monday
*/

function TimeSheet_Approval()
{
	// 1. Get List of Project Managers from Projects
	// 2. Loop through the project managers and fetch the resources for whom the timesheet is not approved
	// 3. Email notification to project managers 
	
	var cols = new Array();
	cols[cols.length] = new nlobjSearchColumn('custentity_projectmanager', null, 'group');
	
	var filters = new Array();
	filters[filters.length] = new nlobjSearchFilter('customer', null, 'noneof', 4223);
	filters[filters.length] = new nlobjSearchFilter('customer', null, 'noneof', 7128);
	filters[filters.length] = new nlobjSearchFilter('jobtype', null, 'anyof', 2);
	filters[filters.length] = new nlobjSearchFilter('custentity_deliverymanager', null, 'noneof', 1586); //Exclude Trivandrum Projects 
	filters[filters.length] = new nlobjSearchFilter('custentity_projectmanager', null, 'noneof', '@NONE@');
	filters[filters.length] = new nlobjSearchFilter('custentity_projectmanager', null, 'noneof', 5706);
	filters[filters.length] = new nlobjSearchFilter('status', null, 'anyof', 2);
	
	var searchresult = nlapiSearchRecord('job', null, filters, cols);
	if(searchresult != null)
	{
		for(var i=0; i < searchresult.length; i++)
		{
			var Fields = ['email', 'firstname'];
			var columns = nlapiLookupField('employee', searchresult[i].getValue('custentity_projectmanager', null, 'group'), Fields);
			
			var cols1 = new Array();
			cols1[cols1.length] = new nlobjSearchColumn('date');
			cols1[cols1.length] = new nlobjSearchColumn('employee');
			cols1[cols1.length] = new nlobjSearchColumn('hours');
			cols1[cols1.length] = new nlobjSearchColumn('customer');
			cols1[cols1.length] = new nlobjSearchColumn('type');
			cols1[cols1.length] = new nlobjSearchColumn('email', 'employee');
		
			var filters1 = new Array();
			filters1[filters1.length] = new nlobjSearchFilter('custcol_approvalstatus', null, 'anyof', '1');
			filters1[filters1.length] = new nlobjSearchFilter('date', null, 'within', '7/20/2014', '7/26/2014');
			filters1[filters1.length] = new nlobjSearchFilter('type', null, 'is', 'A');
			filters1[filters1.length] = new nlobjSearchFilter('custcol_projectmanager', null, 'is', searchresult[i].getValue('custentity_projectmanager', null, 'group'));
			
			var searchresult1 = nlapiSearchRecord('timebill', null, filters1, cols1);
			if(searchresult1 != null)
			{
				var htmltext = '<table border="0" width="100%">';
				htmltext += '<tr>';
				htmltext += '<td colspan="4" valign="top">';
				htmltext += '<p>Hi ' + columns.firstname + ',</p>';
				htmltext += '<p>Below are the list of employee(s) timesheet due for Approval for the week 7/20/2014 to 7/26/2014</p>';
				htmltext += '</td></tr>';
				htmltext += '<tr><td></td></tr>';
				htmltext += '<tr><td></td></tr>';
				htmltext += '<tr ><td colspan = "4" font-size="14pt" align="center" bgcolor="#AEAEAE"><b>TimeSheet - Pending Approval</b></td></tr>';
				htmltext += '<tr><td font-size="14pt" align="center"><b>Date</b></td><td font-size="14pt" align="center"><b>Customer</b></td><td font-size="14pt" align="center"><b>Employee</b></td><td font-size="14pt" align="center"><b>Hours</b></td></tr>';
				
				for(var j=0; j< searchresult1.length; j++)
				{
					htmltext += '<tr>';
						htmltext += '<td font-size="14pt" align="center">' + searchresult1[j].getValue('date') + '</td>';
						htmltext += '<td font-size="14pt" align="center">' + searchresult1[j].getText('customer') + '</td>';
						htmltext += '<td font-size="14pt" align="center">' + searchresult1[j].getText('employee') + '</td>';
						htmltext += '<td font-size="14pt" align="center">' + searchresult1[j].getValue('hours') + '</td>';
					htmltext += '</tr>';
				}
				htmltext += '<tr><td></td></tr>';
				htmltext += '<tr><td></td></tr>';
				htmltext += '<tr><td></td></tr>';
				htmltext += '<tr><td></td></tr>';
				htmltext += '<tr><td colspan = "4"><b>Note:</b>This is a system generated email. In case you have any questions on the above data, please write to ';
				htmltext += '<a href="mailto:information.systems@brillio.com">information.systems@brillio.com</a></td></tr>';
				htmltext += '</table>';

				var ProjManager = nlapiLookupField('employee', searchresult[i].getValue('custentity_projectmanager', null, 'group'), 'email');
				nlapiSendEmail(442, columns.email, 'TimeSheet - Pending Approval', htmltext, null, 'information.systems@brillio.com');
				//columns.email
			}
		}
	}
}


function Email_CollaberaResources()
{
	var EmailList = ['Syed;syed.anwar@brillio.com',
'Chenna;chennakesava.j@brillio.com',
'Gurjit;gurjits@brillio.com',
'Pramod;pramoddp@brillio.com',
'Sandhya;sandhya.konduru@brillio.com',
'Vaidyanathan;vaidyanathan.raman@brillio.com',
'Linda;lindapq@brillio.com',
'Mathivanan;mathivananr@brillio.com',
'Dattatray;Dattatrayad@brillio.com',
'Senthil;senthil.kannan@brillio.com',
'Simi;simi.luthra@brillio.com',
'Tonya;tonya.green@brillio.com',
'Almara;almara.fazli@brillio.com',
'Rajesh;rajeshkr@brillio.com',
'Ravindar;ravindar.chandraseka@brillio.com',
'Syed;syed.hassan@brillio.com',
'Umashanker;umashanker.krishnamo@brillio.com',
'Pushpakumar;pushpakumarc@brillio.com',
'Venkata Ramana Reddy;venkataramanarm@brillio.com',
'Linette;linette.perry@brillio.com',
'Pavan Kumar Reddy;pavan.kankanala@brillio.com',
'Michael;michael.bobrowicz@brillio.com',
'Zain;zain.bhombal@brillio.com',
'Erwin;erwin.tung@brillio.com',
'Fnu;amit.kumar@brillio.com',
'Jagadeeswaran;JagadeeswaranAC@brillio.com',
'Alfred;alfreda@brillio.com',
'Aly;SARWARAI@SCE.COM',
'Enrique;enrique.celis@brillio.com',
'Jason;jason.leos@brillio.com',
'Ming;NGUYENMT@SCE.COM',
'David;David.Huizar@brillio.com',
'Akbar;akbar.hussain@brillio.com',
'Debsankar;debsankar.pramanik@brillio.com',
'Mala;mala.mehta@brillio.com',
'Kishor;kishorbr@brillio.com',
'Karthick;karthickg@brillio.com',
'Ajay;tyagi.shalu@yahoo.co.in',
'Muhammad;faaiz@brillio.com',
'Sheilagh;sheilagh.robinson@brillio.com',
'Praveen;praveenkumar.pattar@brillio.com',
'Donna;donna.trinh@brillio.com',
'Enu;enu.mittal@brillio.com',
'Chao Wen;ehuang90@yahoo.com',
'Roger;NIGGRA@SCE.COM',
'Hemilkumar;hemil.damor@brillio.com',
'Edward;edhouser2000@yahoo.com',
'Dinesh;dinesh.muthuraj@brillio.com',
'Arun;arun.konapur@brillio.com',
'Mahesh;maheshg@brillio.com',
'Pramod;pramod.chinnam@brillio.com',
'Joseph;joseph.george@brillio.com',
'Nilesh;nileshpb@brillio.com',
'Melvin;melvin.michael@brillio.com',
'Chandrasekhar;chandrasekharry@brillio.com',
'Pradeep;pradeepktm@brillio.com',
'Prabal;prabalkd@brillio.com',
'Hari;harips@brillio.com',
'Sukant;sukantcd@brillio.com',
'Vijaya;vijaya.krishna@brillio.com',
'Adarsh;adarshgj@brillio.com',
'Madhusmita;madhusmita.chakrabor@brillio.com',
'Shaik;ShaikNB@brillio.com',
'Kotesh;koteshg@brillio.com',
'Hithendra;hithendrakp@brillio.com',
'Ratna;ratnakb@brillio.com',
'Varsha;varsha.dubey@brillio.com',
'Vinay;vinayrm@brillio.com',
'Deepak;deepakks@brillio.com',
'Jaimurugan;jaimurugans@brillio.com',
'Anusha;anusha.garimella@brillio.com',
'Jagan;jagang@brillio.com',
'Tridib;tridib.dev@brillio.com',
'Umashankara;umashankaradntm@brillio.com',
'Sumesh;sumeshvs@brillio.com',
'Chandrasekar;chandrasekar.raja@brillio.com',
'Pavan;alluri.pavan@brillio.com',
'Girish;girishkg@brillio.com',
'Vidya;vidyapdn@brillio.com',
'Subhash;subhash.mehtre@brillio.com',
'Ritu;ritu.chaudhary@brillio.com',
'Catherine;catknapp540@comcast.net',
'Bikash;BikashCB@brillio.com',
'Doyle;verizonman36@hotmail.com',
'Jeffry;jeffrymb@yahoo.com',
'Allan;ohai@comcast.net',
'Nicholas;njb303@gmail.com',
'Benjamin;benjamin.arp@brillio.com',
'Christopher;christopher.m@brillio.com',
'Ronald;roncook8@aim.com',
'Aparna;aparna.sadavarte@capitalone.com',
'Steven;Steve@StevenJThompson.com',
'Prashanth;prashanth@brillio.com',
'Sunil;sunils@brillio.com',
'Sudhansu;sudhansusb@brillio.com',
'Andrew;Andrew.F.Howell@gmail.com',
'Seetharamu;karthikhs@brillio.com',
'Michael;mamorris358@msn.com',
'Dan;dan.coykendall@brillio.com',
'Jennifer;jenystaiman@gmail.com',
'Babu;babum@brillio.com',
'Andrew;Andrew5124@gmail.com',
'Shomithraya;shomithrk@brillio.com',
'Jong;jonniefriday@yahoo.com',
'Bryan;bryhiga@gmail.com',
'Daniel;daniel.vail@hotmail.com',
'Autumn;autumn.kull@brillio.com',
'Zaki;zakiak@brillio.com',
'Santhosh;santoshkatti@brillio.com',
'Jonathan;jonathan.thimany@brillio.com',
'Lizeth;bonillalizeth@hotmail.com',
'Richard;richardrozier13@yahoo.com',
'Lester;dusty.parrott@gmail.com',
'John;johnc934@msn.com',
'Falon;myhirome@gmail.com',
'Jeffrey;jaguth@gmail.com',
'Benjamin;soulman76@hotmail.com',
'James;james.fouts@brillio.com',
'Janette;janetterng@hotmail.com',
'Ashoka;ashokakp@brillio.com',
'Wayne;wayneb226@hotmail.com',
'Ryan;ryan.lohrman@gmail.com',
'Sarah;sarahclaybaugh@googlemail.com',
'Gorden;doncalos2000@yahoo.com',
'Russell;russ4oh@yahoo.com',
'Cassidy;littlebuggy@gmail.com',
'Waylon;waylon.inman@brillio.com',
'Eugene;eugene.tensley@yahoo.com',
'Scott;scott.deitering@brillio.com',
'Sameh;samehyousef2008@gmail.com',
'Robin;rcl52@rocketmail.com',
'Abir;Abir.Khan@gmail.com',
'Howard;hpc29@hotmail.com',
'William;lilwill102@hotmail.com',
'Christopher;c.morgan_robinson@hotmail.com',
'Steven;steven.pender@brillio.com',
'Bryant;bryantjames_43113@yahoo.com',
'Earnest;blackhawk41@gmail.com',
'Rajasheker;rseelam_67@yahoo.com',
'Terrell;termel@insight.rr.com',
'Heath;htapley38@hotmail.com',
'Tesfu;tewahdo1@gmail.com',
'Orion;orionhughes@hotmail.com',
'Curtis;coatescurtis@hotmail.com',
'Brian;brian.larson@brillio.com',
'Joshua;lovenet54@yahoo.com',
'Mary;MISKOM@GMAIL.COM',
'Jatin;jatin22@yahoo.com',
'Devin;devin.mudock@brillio.com',
'Thomas;thomas_hall@LIve.com',
'Jonathan;jonathan.lewis@brillio.com',
'Johnnie;Daughertyjl@yahoo.com',
'Michael;michael.mchenry@brillio.com',
'Shang Heng;shanghengwu@hotmail.com',
'Agbor;liltusestar@yahoo.fr',
'Nathan;nate479@gmail.com',
'Richard;rick.connect@gmail.com',
'Jacqueline;jlaing01200@yahoo.com',
'Emma;etalvarez@gmail.com',
'Sean;seanyoo44@gmail.com',
'Garrett;garrett.shawstad@gmail.com',
'Joshua;J_Gaerts@live.com',
'Andrew;ez_maq@yahoo.com',
'Matthew;airmattdog@hotmail.com',
'Dean;dshima@gmail.com',
'David;wooddj@outlook.com',
'David;David.Yeamans@gmail.com',
'Dedra;dedavis27@gmail.com',
'Everett;everett.gresham@gmail.com',
'Nasseer;NasseerSlim@gmail.com',
'John;jgutheil1@hotmail.com',
'Raghu Prasad;raghubr@brillio.com',
'Elizabeth;l_kozee@yahoo.com',
'Devin;drm9469@wowway.com',
'Avnish;avnish.kumar@brillio.com',
'Kenneth;kenbakerjobs@gmail.com',
'Tsehay;tseterefe@yahoo.com',
'Meiru;meiru_sun@hotmail.com',
'Rajesh;rajeshdulam@gmail.com',
'Samuel;sampoku4@hotmail.com',
'Jose;joseshipe@yahoo.com',
'Shirley Nita;shirleynita@yahoo.com',
'Tiria;spiritcouncil@msn.com',
'Joseph;raczkowskijoe@gmail.com',
'David;towedave@live.com',
'Jean;jgpierre@msn.com',
'Catie;since1916@gmail.com',
'Lankesh;lankesh.devalapura@brillio.com',
'Nitin;nitinharkara@gmail.com',
'Damodaran;damodaran.rajendaran@brillio.com',
'Julie;julie_c_barker@hotmail.com',
'Joseph;jsphhoward@gmail.com',
'Bryan;bchavez316@yahoo.com',
'Gunasekaran;gunasamy12@gmail.com',
'Ryan;RyCHuertas@gmail.com',
'Kasi Viswanadham;kasi.pusuluri@brillio.com',
'Prasath Krishna;prasathkbk@brillio.com',
'Kato;kato.fransen@brillio.com',
'Mayankkumar;mayank.patel@brillio.com',
'Jean Louis;jean.louis@brillio.com',
'Javed;javedak@brillio.com',
'Debasish;debasish.mukherjee@brillio.com',
'Gopinath;rajeev.gopinath@brillio.com',
'Sriramachandra;sriramachandramk@brillio.com',
'Sathyan;sathyanb@brillio.com',
'Mahendra;mahendranp@brillio.com',
'Iyer;iyervv@brillio.com',
'Brian;brianh@brillio.com',
'Ranadeep;ranadeep.debnath@brillio.com',
'Lakshmi;lakshmi.bondada@brillio.com',
'Laxxmi;laxmi.mahapatro@brillio.com',
'Shwetha;Shwetha.GH@brillio.com',
'Venkateswara Rao;VenkateshM@brillio.com',
'Arindam;arindam.pain@brillio.com',
'Ravish;RavishKS@brillio.com',
'Jebaraj;jebarajk@brillio.com',
'Aparna;aparnamk@brillio.com',
'Nitin;NitinA@brillio.com',
'Vikram;vikram.pottam@brillio.com',
'Swarna Priya;swarna.tummala@brillio.com',
'Nianxuan;ivy0928@hotmail.com',
'Dhivya;dhivyas@brillio.com',
'Raghavendra;raghavendra.murthy@brillio.com',
'Krishnaraj;bkrishna@brillio.com',
'Harish;harishkn@brillio.com',
'Rajesh;rajeshp@brillio.com',
'Fnu;amit.kumar@brillio.com',
'Ramana;ramana_km@yahoo.com',
'Srinagavignesh;srinagavignesh.s@brillio.com',
'Muralidharan;muralidharans@brillio.com',
'Pyda;pavankumar@brillio.com',
'Felix Kizhakkel;felixkj@brillio.com',
'Vijaya;vijayakm@brillio.com',
'Bhanu Prakash;bhanupbg@brillio.com',
'Shrinidhi;shrinidhi.holla@brillio.com',
'Manish;manish2@brillio.com',
'Rohit;rohitsc@brillio.com',
'Abhishek;abhishek.sharma@brillio.com',
'Trilok;trilokpn@brillio.com',
'Ajai;ajai.balachandran@brillio.com',
'Pankaj;pankajkc@brillio.com',
'Kishore;kishorekumarcv@brillio.com',
'Prathyush;prathyush.garikipati@brillio.com',
'Krishna;krishnaky@brillio.com',
'Arindom;arindom.das@brillio.com',
'Sushma;sushmabg@brillio.com',
'Sharath;sharathk@brillio.com',
'Lohiya;lohiyacg@brillio.com',
'Phani;phanimsv@brillio.com',
'Sudipta;sudipta.ghosh@brillio.com',
'Suhas;suhas.ukkalam@brillio.com',
'Sunil;sunilks@brillio.com',
'Kunal;kunal.basu@brillio.com',
'Subhaschandra;subhaschandrams@brillio.com',
'Kartheek;kartheekt@brillio.com',
'Deeba;deebaai@brillio.com',
'Apurva;apurva.mistry@brillio.com',
'Justin;justin.dsouza@brillio.com',
'Yvette;ygreene1964@yahoo.com',
'Anjaneya;anjaneya.kamat@brillio.com',
'Ericson;edongon71@outlook.com',
'Marcos;mark.soliz@sbcglobal.net',
'Olumide;oawobona@gmail.com',
'Mohammed;irfan.shirur@brillio.com',
'George;marshg2k@yahoo.com',
'Benjamin;b.crews@comcast.net',
'Henry;henrydagr8t@outlook.com',
'Dillip;dillipks@brillio.com',
'Ferdinana;ferdinana.caviles@brillio.com',
'Mark Anthony;markymartin510@gmail.com',
'John;JohneleRoy40@gmail.com',
'Justin;justin.clark@brillio.com',
'Keri;keric@xley.com',
'Harold;haroldjenningsjr@hotmail.com',
'Karl;kcerf40@yahoo.com',
'Juan;alvrz_11@yahoo.com',
'Christopher;christopher.piper@brillio.com',
'Mamadou;mamatech7@gmail.com',
'Derrick;derrickgeorge74@gmail.com',
'Ernest;ernestm@brillio.com',
'Warren;warren.vehec@brillio.com',
'Nathan;atkinsnatha@yahoo.com',
'Sean;vohen85@hotmail.com',
'Tia;tiaproudmomof2@yahoo.com',
'Rambod;rambod_e@hotmail.com',
'Olasunkanmi;olasunkanmisijuade@hotmail.com',
'Jack;jclift73@sbcglobal.net',
'Melissa;melissa.lara@brillio.com',
'Ilia;ilia.johnson@brillio.com',
'Anthony;anthony.womelsdorf@brillio.com',
'James;Booker345690@gmail.com',
'Irfan;irfanish@gmail.com',
'Steven;gymcoach98@gmail.com',
'Shelby;shelby.murphy@brillio.com',
'Festus;festus.ominde@gmail.com',
'Adrian;asthill@hotmail.com',
'Charlene;cjoseph991@gmail.com',
'Daryl;DARYLALLEN@LIVE.COM',
'Christopher;landrycr@gmail.com',
'Andy;andy.fisher76@gmail.com',
'Elham;mosadegh.elham@gmail.com',
'Kevin;kevinr.curry@yahoo.com',
'Noree;eclipsesmuse@yahoo.com',
'Alan;aglamon@comcast.net',
'Abhilash;abhilash.nair@brillio.com',
'Marina;marina.skurko@brillio.com',
'Jordan;jordan.qn@gmail.com',
'Eryn;tay_tay02@yahoo.com',
'Alondra;Lopezweb2@yahoo.com',
'Nathan;nrasta12@gmail.com',
'Shannon Shanan;Rayell1984@yahoo.com',
'Muhammad;sheikh.muhammad86@gmail.com',
'Bryan;bpierson7@gmail.com',
'Chadrick;vidrine76209@yahoo.com',
'Christopher;chris.cunningham15@ymail.com',
'Badri;bgautam81@yahoo.com',
'Yvonna;vonna5219@gmail.com',
'Natasha;npierre0120@gmail.com',
'James;Jmpayne2@charter.net',
'Matthew;matthewmarquez21@yahoo.com',
'Naveen;buntyjakhu@gmail.com',
'Edward;edprezidents@yahoo.com',
'Marcus;mahqwon@gmail.com',
'Mara;hedylamarra@gmail.com',
'Christopher;cbhandley_2000@yahoo.com',
'Sheron;sheronb89@hotmail.com',
'Jason;jasonwitt@hotmail.com',
'Rebecca;Rebecca.Tuck@yahoo.com',
'Michelle;michelle.P@brillio.com',
'Virinchi;virinchi.darbha@brillio.com',
'Jose;einar28@hotmail.com',
'John;ac3xiii@gmail.com',
'Kenneth;rcomputerguy@hotmail.com',
'Robert;r.peyton@hotmail.com',
'Garret;hellfish1974@gmail.com',
'Vi;vi0317@hotmail.com',
'Robert;robert.mayers@brillio.com',
'Desiree;dterr13@gmail.com',
'Salah;shs_san@Yahoo.com',
'Whitney;whitney_sampson@yahoo.com',
'Deron;dardenderon@yahoo.com',
'Antionette;antionettetemple@hotmail.com',
'Michael;michaelpnegron@gmail.com',
'Jason;ason.nelsen@yahoo.com',
'Michael;Bukowskim57@hotmail.com',
'Robert;robertcornelll@icloud.com',
'Shartel;keith.134@buckeyemail.osu.edu',
'Jordan;Jacey@XYZElement.com',
'Cherif;sanghott@yahoo.com',
'Austin;austinpd95@gmail.com',
'Monroe;monroe.harvey88@gmail.com',
'Taurean;yourtechworld00@gmail.com',
'Jarvis;jarvisbrantley1@gmail.com',
'Malcolm;mallo2082@yahoo.com',
'Sanjiv;sbacchas@gmail.com',
'Saeed;saeed.computer.sc@gmail.com',
'Marcus;computerguy1@outlook.com',
'Manikandan;manikandanr@brillio.com',
'Chander;chanderd@brillio.com',
'Chandramouli;chandramouli@brillio.com',
'Meghashyam;meghashyams@brillio.com',
'Ancy;ancyea@brillio.com',
'Shodhan;shodhanyb@brillio.com',
'Abdul;abdul.rahim@brillio.com',
'Jayanta;jayanta.banerjee@brillio.com',
'Livin;livinkv@brillio.com',
'Shinub;shinubgt@brillio.com',
'Vivek;vivek.rawat@brillio.com',
'Akash;akashcs@brillio.com',
'Sandeep;sandeep.pavithran@brillio.com',
'Akhilandam;akhilandamp@brillio.com',
'Sirish;sirishus@brillio.com',
'Tabrez;tabrezas@brillio.com',
'Narendranath;narendranathrg@brillio.com',
'Nirban;nirbangt@brillio.com',
'Sreekanth;sreekanthj@brillio.com',
'Kiran;kiranbp@brillio.com',
'Sindhu;sindhuhp@brillio.com',
'Mehul;mehulkw@brillio.com',
'Zaki;zakiak@brillio.com',
'Kisan;kisankm@brillio.com',
'Praveen;praveenkn@brillio.com',
'Meghana;meghanars@brillio.com',
'Yagina;yagina.gurudas@brillio.com',
'Manoj;manoj.kumar@brillio.com',
'Rasna;rasna.ramesh@brillio.com',
'Mohan;mohan.kumar@brillio.com',
'Krishnan;krishnank@brillio.com',
'Sreevally;sreevallypv@brillio.com',
'Ashwini;ashwinibs@brillio.com',
'Sreejith;sreejithk@brillio.com',
'Pragin;pragink@brillio.com',
'Charan;charanhn@brillio.com',
'Ujjwal;ujjwal.krishna@brillio.com',
'Dillip;dillipks@brillio.com',
'Lajith;lajith.raman@brillio.com',
'Babulal;babulalk@brillio.com',
'Mahesh;mahesh.vishenne@brillio.com',
'Iyer;iyervv@brillio.com',
'Nitheesh;nitheeshav@brillio.com',
'Mahalakshmi;mahalakshmim@brillio.com',
'H;chandrasph@brillio.com',
'Sudhakar;sudhakarv@brillio.com',
'Gaurav;gaurav.aditya@brillio.com',
'Rajeeva;rajeeva.acharya@brillio.com',
'Mohd;salman.hauq@brillio.com',
'Nayana;nayana.pai@brillio.com',
'Kannan;kannan.meiappan@brillio.com',
'Suresh;sureshry@brillio.com',
'Suchithra;suchithraj@brillio.com',
'Wasim;wasim.raza@brillio.com',
'Thejeshwer;tejeshwersg@brillio.com',
'Budha;budharbd@brillio.com',
'Kalyani;kalyanig@brillio.com',
'Anashwar;anashwarn@brillio.com',
'Vinayraj;vinayraj.shet@brillio.com',
'Goutham;gouthammc@brillio.com',
'Brajesh;brajeshks@brillio.com',
'Chandrashekar;chandrashekarr@brillio.com',
'Abhijith;abhijithrs@brillio.com',
'Selva;selva.m@brillio.com',
'Deepashree;deepasreetk@brillio.com',
'Vikas;vikasma@brillio.com',
'Suvitha;suvithas@brillio.com',
'Vinesh;vineshc@brillio.com',
'Niva;nivakm@brillio.com',
'Dipin;dipinak@brillio.com',
'Chandrashekara;chandrashekaraum@brillio.com',
'Nivya;nivyacj@brillio.com',
'Javeed;javeed.salman@brillio.com',
'Nawaf;nawaf.orompurath@brillio.com',
'Remya;remyar@brillio.com',
'Shilpa;shilpas@brillio.com',
'Divya;divyar@brillio.com',
'Deeparajan;deeparajanv@brillio.com',
'Uma;umaa@brillio.com',
'Abhin;abhinmp@brillio.com',
'Kalpana;kalpana.mishra@brillio.com',
'Kavitha;kavithaa@brillio.com',
'Prateek;prateek.verma@brillio.com',
'Selvam;selvamp@brillio.com',
'Himanshu;himanshu.agarwal@brillio.com',
'Vishalini;vishaliniks@brillio.com',
'Aditya;adityak@brillio.com',
'Priya;priyaps@brillio.com',
'Selva;selvakumarp@brillio.com',
'Sana;sanaar@brillio.com',
'Yashaswini;yashaswinik@brillio.com',
'Murali;muralig@brillio.com',
'Chaitanya;chaitanyaavsk@brillio.com',
'Debanjan;debanjan.ray@brillio.com',
'Shruthi;shruthipgv@brillio.com',
'Pankaj;pankaj.baberwal@brillio.com',
'Shilpa;shilpa.mittal@brillio.com',
'Supriya;supriya.sarkar@brillio.com',
'Ranjan;ranjankp@brillio.com',
'Naveen;naveen.ramakrishna@brillio.com',
'Sunil;Sunil.d@brillio.com',
'Raju;rajur@brillio.com',
'Adwitya;adwitiya.sushant@brillio.com',
'Sanjeev;sanjeevkg@brillio.com',
'Nayana;nayan.bhatt@brillio.com',
'Sreenath;sreenath.mantha@brillio.com',
'Vijay;vijay.babu@brillio.com',
'Subha;subha@brillio.com',
'Vinod;vinod.kumar@brillio.com',
'Harish;harish.gopalaraju@brillio.com',
'Pankaj;pankaj.singh2@brillio.com',
'Alok;alok.tripathi@brillio.com',
'Linda;lindac100@verizon.net',
'Ramana;ramana_km@yahoo.com',
'Ramana;ramana_km@yahoo.com',
'Ramana;ramana_km@yahoo.com',
'Ramana;ramana_km@yahoo.com',
'Ramana;ramana_km@yahoo.com',
'Ramana;ramana_km@yahoo.com',
'Ramana;ramana_km@yahoo.com',
'Ramana;ramana_km@yahoo.com',
'Souvik;souvik.mondal@brillio.com',
'Prabhulingayya;prabhu@brillio.com',
'Sumanth;sumanthup@brillio.com',
'Ramana;ramana_km@yahoo.com',
'Sandhya;sandhya.sugavanam@brillio.com',
'Rajiv;rajivjc@brillio.com',
'Tulasi;tulasib@brillio.com',
'Pyda;pavankumar@brillio.com',
'Sudeep;sudeepkm@brillio.com',
'Manish;manishp@brillio.com',
'Smitha;smitha.sudhakar@brillio.com',
'Rajesh;rajesh.chakrabarty@brillio.com',
'Sreekanth;sreekanthb@brillio.com',
'Naveen;naveenkb@brillio.com',
'Ramkumar;ramkumarpp@brillio.com',
'Ashish;ashishkv@brillio.com',
'Thimmaraju;mthimmaraju@brillio.com',
'Pankaj;pankaj.kumar@brillio.com',
'Vivek;vivekrn@brillio.com',
'Pallavi;pallavi.bhuniya@brillio.com',
'Ashish;ashish.rathore@brillio.com',
'Sivamurthy;sivamurthys@brillio.com',
'Karthik;karthik.ganesan@brillio.com',
'Debanjana;debanjana.bagchi@brillio.com',
'Sabareesh;sabareesh.v@brillio.com',
'Nitendra;nitendra.singh@brillio.com',
'Naveen;naveen.naik@brillio.com',
'Atish;atishcp@brillio.com',
'Karthikeyan;karthikeyanr@brillio.com',
'Ramakrishna;ramakrishnag@brillio.com',
'Meenakshi;meenakshi.rana@brillio.com',
'Shridhar;shridharro@brillio.com',
'Nidheesh;nidheesh.puthalath@brillio.com',
'Tejas;tejasad@brillio.com',
'Naveen;naveen.sasi@brillio.com',
'Sugandh;sugandh.sinha@brillio.com',
'Priyabrata;priyabrata.dash@brillio.com',
'Asraruddin;asraruddin@brillio.com',
'Sujata;sujata.devi@brillio.com',
'Nageshwara;nageshwararm@brillio.com',
'Sravanthi;shravsetl@gmail.com',
'Hemanth;hemanthkmn@brillio.com',
'Apoorva;apoorvamj@brillio.com',
'Chethan;chethank@brillio.com',
'Anusha;anusha.devarapalli@brillio.com',
'Mrinal;mrinal_kg@hotmail.com',
'Bryan;bryan.sheasby@brillio.com',
'Ranjini;SIROSHR@SCE.COM',
'Prabhakaran;prabhakaran.c@brillio.com',
'Basavaraj;basavarajsm@brillio.com',
'Anupama;anupama.mithran@brillio.com',
'Arundhati;arundhati.gurjar@brillio.com',
'Sandeep;sandeep.chinnari@brillio.com',
'Harish;harishks@brillio.com',
'N;nagarjunan@brillio.com',
'Prakash;prakash.bisht@brillio.com',
'Vivek;vivek.gupta@brillio.com',
'Kartheek;kartheekt@brillio.com',
'Nishant;nishant.sharma@brillio.com',
'Priyatharsini;Priyatharsini.h@brillio.com',
'Vinod;vinodk@brillio.com',
'Andre;andreanderson@gmail.com',
'Sathish;satishkb@brillio.com',
'Fnu;alok.kumar@brillio.com',
'Naveen;naveenrp@brillio.com',
'Pradipa;pradipkm@brillio.com',
'Raghu;raghug@brillio.com',
'Ravindra;ravindram@brillio.com',
'Utsarga;utsargakp@brillio.com',
'Sarath;sarathk@brillio.com',
'Sridhar;sridhar.subramani@brillio.com',
'Kiran;kiran.rajanala@brillio.com',
'Sandeep;sandeep.k@brillio.com',
'Michael;mhedges21@gmail.com',
'Alan;aglamon@comcast.net',
'Jesus;marcamjes_2001@yahoo.com',
'Jessica;jessicakane90@gmail.com',
'Bryan;bchavez316@yahoo.com',
'Jared;JaredHecht@gmail.com',
'Steven;smartocci@gmail.com',
'Simanta;simanta.parida@brillio.com',
'Jagadeesh;jagadeesh.hulagur@brillio.com',
'Ramya;ramya.krishnamurthy@brillio.com',
'Damodar;damodar.naidu@brillio.com',
'Reddi;reddi.prasad@brillio.com',
'Sasi;sasi.c@brillio.com',
'Smitha;smitha.l@brillio.com',
'Udaykumar;udayakumar.d@brillio.com',
'Muskan;muskan@brillio.com',
'Manjunatha;manjunatham@brillio.com',
'Surendra;surendrakn@brillio.com',
'Tulasi;tulasib@brillio.com',
'Rajendar;rajendar.athmanathan@brillio.com',
'Anand;anand.jayaseelan@brillio.com',
'Debraj;Debraj.Sharma@brillio.com',
'Chingchieh;gingerc_chen@yahoo.com',
'Anoop;anoop.philip@brillio.com',
'Javed;javedak@brillio.com',
'Thomas;tb17mccoy@yahoo.com',
'Stacey;ccnp2slee@yahoo.com',
'Jagadeesh;jagadeesh.hulagur@brillio.com',
'Manjunatha;manjunatham@brillio.com',
'Vivek;vivek.gupta@brillio.com',
'Udaykumar;udayakumar.d@brillio.com',
'Sreekanth;sreekanthj@brillio.com',
'Sreekanth;sreekanthj@brillio.com',
'Viraj;viraj3_16@yahoo.com',
'Binny;binnyjohn@brillio.com',
'Mallikarjuna;mallikarjuna.kanala@brillio.com',
'Mallikarjuna;mallikarjuna.kanala@brillio.com',
'Seetharamu;karthikhs@brillio.com',
'Dhivya;dhivyas@brillio.com',
'Harish;harishkn@brillio.com',
'Geetesh;geeteshbl@brillio.com',
'Geeta;geetath@brillio.com',
'Sumanth;sumanthup@brillio.com',
'Prabhulingayya;prabhu@brillio.com',
'Abhishek;abhishek.ks@brillio.com',
'Shriti;shriti.bhowmick@brillio.com',
'Manoj;Manoj.Pattnayak@brillio.com',
'Ganesh;ganeshrbg@brillio.com',
'Vidya;vidyam@brillio.com',
'Senthil;senthilkg@brillio.com',
'Ravi;ravikbk@brillio.com',
'James;JamesDaltonBell@gmail.com',
'William;Mrlogan3@hotmail.com',
'Souvik;souvik.mondal@brillio.com',
'Sunil;sajwans81@gmail.com',
'Manjunatha;manjunathamh@brillio.com',
'Suresh;suresh.jettipalle@brillio.com',
'Akansha;akansha.yadav@brillio.com',
'Vikram;vikram.ravi@brillio.com',
'Manjunatha;manjunathamh@brillio.com',
'Rahul;rahultm@brillio.com',
'Umamaheshwar;umamaheswar.hegde@brillio.com',
'Jayakumar;Jayakumar.Chinta@brillio.com',
'Lawrence;lawrence.sands@brillio.com',
'Nagaraj;nagarajgk@brillio.com',
'Pankaj;pankajkc@brillio.com',
'Sudipta;sudipta.ghosh@brillio.com',
'Kunal;kunal.basu@brillio.com',
'Naveen;naveen.ramakrishna@brillio.com',
'Christine;christine.flores@brillio.com',
'Uma;meeteeeuma@gmail.com',
'Uma;meeteeeuma@gmail.com',
'Mohammed;salaoddin.linux@gmail.com',
'Venkat;jampanibi@yahoo.com',
'Sheilagh;sheilagh.robinson@brillio.com',
'Rajiv;rajivjc@brillio.com',
'Gajanan;gajanan.tawshikar@brillio.com',
'Ranjini;SIROSHR@SCE.COM',
'Bryan;bryan.sheasby@brillio.com',
'Roger;NIGGRA@SCE.COM',
'Venkata Praveen;venkata3978@gmail.com',
'Nandika;diasdeke@sbcglobal.net',
'Amol;amol.java7@gmail.com',
'Nagarjuna;nagarjunan@brillio.com',
'Harish;harishks@brillio.com',
'Chethan;chethank@brillio.com',
'Apoorva;apoorvamj@brillio.com',
'Hemanth;hemanthkmn@brillio.com'];

	nlapiLogExecution('DEBUG', 'Count', EmailList.length);
	for(var i=0; i< EmailList.length; i++)
	{
		var recDetails = EmailList[i].split(';');
		nlapiLogExecution('DEBUG', 'Details', 'First Name - ' + recDetails[0] + ', Email - ' + recDetails[1]);

		var htmltext = '<table border="0" width="100%">';
		htmltext += '<tr>';
		htmltext += '<td colspan="4" valign="top">';
		htmltext += '<p>Hi ' + recDetails[0] +  ',</p>';
		htmltext += '<p></p>';
		htmltext += '<p>This is with regard to the email notification sent from <a href="mailto:information.systems@brillio.com">Information Systems</a> to fill timesheet in NetSuite system on July 7, 2014.</p>';
		htmltext += '<p></p>';
		htmltext += '<p>Please ignore the previous system generated email and we request you to continue to fill timesheet in Collabera Inc. until further notification.</p>';
		htmltext += '</td></tr>';
		htmltext += '<tr></tr>';
		htmltext += '<tr></tr>';
		htmltext += '<tr></tr>';
		htmltext += '<p>Regards,</p>';
		htmltext += '<p>Information Systems</p>';
		htmltext += '</table>';
		htmltext += '<hr width="100%" size="1" noshade color="#CCCCCC">';
		htmltext += '<table width="100%" border="0" cellspacing="0" cellpadding="0">';
		htmltext += '<tr>';
		htmltext += '<td align="right">';
		htmltext += '<font style="font-size:9px; font-family:Verdana,Arial,Helvetica,sans-serif; color:#999999;">Brillio is powered by <a href="http://www.netsuite.com/" style="color:#999999;">NetSuite</a> &#151; One System. No Limits.</font>';
		htmltext += '</td>';
		htmltext += '</tr>';
		htmltext += '</table>';
		htmltext += '</body>';
		htmltext += '</html>';

		nlapiSendEmail(442, recDetails[1], 'Please ignore previous mail sent on timesheet', htmltext);
	}
}

function TimeSheet_DM_Notify()
{
	// 1. Get List of Delivery Managers from Projects
	// 2. Loop through the delivery managers and fetch the PM resources who have not approved timesheet
	// 3. Email notification to delivery managers 
	
	var cols = new Array();
	cols[0] = new nlobjSearchColumn('custentity_deliverymanager', null, 'group');
	//cols[1] = new nlobjSearchColumn('custentity_projectmanager', null, 'group');
	cols[0].setSort();
	
	var filters = new Array();
	filters[filters.length] = new nlobjSearchFilter('customer', null, 'noneof', 4223);
	filters[filters.length] = new nlobjSearchFilter('customer', null, 'noneof', 7128);
	filters[filters.length] = new nlobjSearchFilter('jobtype', null, 'anyof', 2);
	filters[filters.length] = new nlobjSearchFilter('custentity_deliverymanager', null, 'noneof', 1586); //Exclude Trivandrum Projects 
	filters[filters.length] = new nlobjSearchFilter('custentity_deliverymanager', null, 'noneof', '@NONE@');
	filters[filters.length] = new nlobjSearchFilter('custentity_deliverymanager', null, 'noneof', 5706);
	filters[filters.length] = new nlobjSearchFilter('status', null, 'anyof', 2);
	
	var searchresult = nlapiSearchRecord('job', null, filters, cols);
	if(searchresult != null)
	{
		for(var i=0; i < searchresult.length; i++)
		{
			var DlvryMgr = searchresult[i].getValue('custentity_deliverymanager', null, 'group');
			TimeSheet_PMDetails(DlvryMgr);
		}
	}
}

function TimeSheet_PMDetails(DlvryMgr, PMgr)
{
	var cols = new Array();
	cols[0] = new nlobjSearchColumn('custentity_projectmanager', null, 'group');

	var filters = new Array();
	filters[filters.length] = new nlobjSearchFilter('customer', null, 'noneof', 4223);
	filters[filters.length] = new nlobjSearchFilter('customer', null, 'noneof', 7128);
	filters[filters.length] = new nlobjSearchFilter('jobtype', null, 'anyof', 2);
	filters[filters.length] = new nlobjSearchFilter('custentity_deliverymanager', null, 'noneof', 1586); //Exclude Trivandrum Projects 
	filters[filters.length] = new nlobjSearchFilter('custentity_projectmanager', null, 'noneof', '@NONE@');
	filters[filters.length] = new nlobjSearchFilter('custentity_projectmanager', null, 'noneof', 5706);
	filters[filters.length] = new nlobjSearchFilter('custentity_deliverymanager', null, 'anyof', DlvryMgr);
	filters[filters.length] = new nlobjSearchFilter('status', null, 'anyof', 2);
	
	var searchresult = nlapiSearchRecord('job', null, filters, cols);
	if(searchresult != null)
	{
		for(var i=0; i < searchresult.length; i++)
		{
			var Fields = ['email', 'firstname'];
			var columns = nlapiLookupField('employee', DlvryMgr, Fields);
			
			var cols1 = new Array();
			cols1[cols1.length] = new nlobjSearchColumn('date');
			cols1[cols1.length] = new nlobjSearchColumn('employee');
			cols1[cols1.length] = new nlobjSearchColumn('hours');
			cols1[cols1.length] = new nlobjSearchColumn('customer');
			cols1[cols1.length] = new nlobjSearchColumn('type');
			cols1[cols1.length] = new nlobjSearchColumn('email', 'employee');
		
			var filters1 = new Array();
			filters1[filters1.length] = new nlobjSearchFilter('custcol_approvalstatus', null, 'anyof', '1');
			filters1[filters1.length] = new nlobjSearchFilter('date', null, 'within', '7/20/2014', '7/26/2014');
			filters1[filters1.length] = new nlobjSearchFilter('type', null, 'is', 'A');
			filters1[filters1.length] = new nlobjSearchFilter('custcol_projectmanager', null, 'is', searchresult[i].getValue('custentity_projectmanager', null, 'group'));
			
			var Fields = ['email', 'firstname'];
			var searchresult1 = nlapiSearchRecord('timebill', null, filters1, cols1);
			if(searchresult1 != null)
			{
				var htmltext = '<table border="0" width="100%">';
				htmltext += '<tr>';
				htmltext += '<td colspan="4" valign="top">';
				htmltext += '<p>Hi ' + columns.firstname + ',</p>';
				htmltext += '<p>Below are the list of employee(s) timesheet due for PM Approval for the week 7/20/2014 to 7/26/2014.</p>';
				htmltext += '</td></tr>';
				htmltext += '<tr><td></td></tr>';
				htmltext += '<tr><td></td></tr>';
				htmltext += '<tr ><td colspan = "5" font-size="14pt" align="center" bgcolor="#AEAEAE"><b>TimeSheet - PM Pending Approval</b></td></tr>';
				htmltext += '<tr><td font-size="14pt" align="center"><b>Date</b></td><td font-size="14pt" align="center"><b>Customer</b></td><td font-size="14pt" align="center"><b>Project Manager</b></td><td font-size="14pt" align="center"><b>Employee</b></td><td font-size="14pt" align="center"><b>Hours</b></td></tr>';
				
				for(var j=0; j< searchresult1.length; j++)
				{
					htmltext += '<tr>';
						htmltext += '<td font-size="14pt" align="center">' + searchresult1[j].getValue('date') + '</td>';
						htmltext += '<td font-size="14pt" align="center">' + searchresult1[j].getText('customer') + '</td>';
						htmltext += '<td font-size="14pt" align="center">' + searchresult[i].getText('custentity_projectmanager', null, 'group') + '</td>';
						htmltext += '<td font-size="14pt" align="center">' + searchresult1[j].getText('employee') + '</td>';
						htmltext += '<td font-size="14pt" align="center">' + searchresult1[j].getValue('hours') + '</td>';
					htmltext += '</tr>';
				}
				htmltext += '<tr><td></td></tr>';
				htmltext += '<tr><td></td></tr>';
				htmltext += '<tr><td></td></tr>';
				htmltext += '<tr><td></td></tr>';
				htmltext += '<tr><td colspan = "4"><b>Note:</b>This is a system generated email. In case you have any questions on the above data, please write to ';
				htmltext += '<a href="mailto:information.systems@brillio.com">information.systems@brillio.com</a></td></tr>';
				htmltext += '</table>';

				nlapiSendEmail(442, columns.email, 'TimeSheet - PM Pending Approval', htmltext, null, 'information.systems@brillio.com');
			}
		}
	}
}