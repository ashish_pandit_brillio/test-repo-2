/**
 * Populate the entry fields based on the salary
 * 
 * Version Date Author Remarks 1.00 18 Apr 2016 Nitish Mishra
 * 
 */

function userEventBeforeSubmit(type) {
	try {

		if (type == 'create' || type == 'edit' || type == 'xedit') {
			var startDate = nlapiGetFieldValue('custrecord_urt_start_date');
			var endDate = nlapiGetFieldValue('custrecord_urt_end_date');
			var project = nlapiGetFieldValue('custrecord_urt_project');

			if (startDate && endDate && project) {
				var d_startDate = nlapiStringToDate(startDate);
				var d_endDate = nlapiStringToDate(endDate);
				var totalDays = getWorkingDays(startDate, endDate);
				var finalSalaryCost = 0, finalContractCost = 0;

				// get all the allocated employees for this period
				var allocationSearch = nlapiSearchRecord('resourceallocation',
				        null, [
				                new nlobjSearchFilter('formuladate', null,
				                        'notafter', endDate)
				                        .setFormula('{startdate}'),
				                new nlobjSearchFilter('formuladate', null,
				                        'notbefore', startDate)
				                        .setFormula('{enddate}'),
				                new nlobjSearchFilter('project', null, 'anyof',
				                        project) ], [
				                new nlobjSearchColumn('resource'),
				                new nlobjSearchColumn('custentity_persontype',
				                        'resource'),
				                new nlobjSearchColumn('startdate'),
				                new nlobjSearchColumn('enddate'),
				                new nlobjSearchColumn('custeventrbillable'),
				                new nlobjSearchColumn('custevent4'),
				                new nlobjSearchColumn('percentoftime') ]);

				// create a list of salaried and contract employees
				var salariedEmployees = [];
				var contractEmployees = [];

				if (allocationSearch) {

					allocationSearch.forEach(function(allocation) {

						// check if employee is salaried or contract
						if (allocation.getValue('custentity_persontype',
						        'resource') == '2') {
							salariedEmployees.push(allocation
							        .getValue('resource'));
						} else {
							contractEmployees.push(allocation
							        .getValue('resource'));
						}
					});
				}

				// get all salary details for salaried employees from the JE
				if (salariedEmployees.length > 0) {
					finalSalaryCost = getSalaryCost(salariedEmployees, project,
					        startDate, endDate);
				}

				// get subtier details for the contract employees
				if (contractEmployees.length > 0) {
					var subtierSearch = nlapiSearchRecord(
					        'customrecord_subtier_vendor_data',
					        null,
					        [
					                new nlobjSearchFilter(
					                        'custrecord_stvd_contractor', null,
					                        'anyof', contractEmployees),
					                new nlobjSearchFilter('formuladate', null,
					                        'notafter', endDate)
					                        .setFormula('{custrecord_stvd_start_date}'),
					                new nlobjSearchFilter('formuladate', null,
					                        'notbefore', startDate)
					                        .setFormula('{custrecord_stvd_end_date}') ],
					        [
					                new nlobjSearchColumn(
					                        'custrecord_stvd_contractor'),
					                new nlobjSearchColumn(
					                        'custrecord_stvd_start_date'),
					                new nlobjSearchColumn('currency',
					                        'custrecord_stvd_subsidiary'),
					                new nlobjSearchColumn(
					                        'custrecord_stvd_end_date'),
					                new nlobjSearchColumn(
					                        'custrecord_stvd_rate_type'),
					                new nlobjSearchColumn(
					                        'custrecord_stvd_st_pay_rate'),
					                new nlobjSearchColumn(
					                        'custrecord_stvd_ot_pay_rate') ]);

					if (subtierSearch) {
						nlapiLogExecution('debug', 'subtier count',
						        subtierSearch.length);
					}
				}

				// for each allocation, get the per day allocation and
				// find the salary cost from the salary or subtier search
				// as per the employee type
				if (allocationSearch) {

					allocationSearch
					        .forEach(function(allocation) {
						        var employeeId = allocation
						                .getValue('resource');
						        var allocationStartDate = allocation
						                .getValue('startdate');
						        var allocationEndDate = allocation
						                .getValue('enddate');
						        var d_allocationStartDate = nlapiStringToDate(allocationStartDate);
						        var d_allocationEndDate = nlapiStringToDate(allocationEndDate);
						        var percentageAllocation = allocation
						                .getValue('percentoftime');

						        // if employee is salaried
						        if (allocation.getValue(
						                'custentity_persontype', 'resource') == '2') {
							        // Skip
						        } else if (allocation.getValue(
						                'custentity_persontype', 'resource') == '1') { // employee
							        // is
							        // contract

							        // get the subtier record for this employee
							        for (var i = 0; i < subtierSearch.length; i++) {

								        if (subtierSearch[i]
								                .getValue('custrecord_stvd_contractor') == employeeId) {
									        var s_subtierStartDate = subtierSearch[i]
									                .getValue('custrecord_stvd_start_date');
									        var s_subtierEndDate = subtierSearch[i]
									                .getValue('custrecord_stvd_end_date');
									        var stRate = subtierSearch[i]
									                .getValue('custrecord_stvd_st_pay_rate');
									        var otRate = subtierSearch[i]
									                .getValue('custrecord_stvd_ot_pay_rate');
									        var currency = subtierSearch[i]
									                .getValue('currency',
									                        'custrecord_stvd_subsidiary');

									        var newStartDate = MaximumDate(
									                d_startDate,
									                d_allocationStartDate,
									                nlapiStringToDate(s_subtierStartDate));
									        var newEndDate = MinimumDate(
									                d_endDate,
									                d_allocationEndDate,
									                nlapiStringToDate(s_subtierEndDate));

									        var s_newStartDate = nlapiDateToString(
									                newStartDate, 'date');
									        var s_newEndDate = nlapiDateToString(
									                newEndDate, 'date');

									        if (subtierSearch[i]
									                .getValue('custrecord_stvd_rate_type') == "3") {
										        finalContractCost += getMonthlyContractCost(
										                employeeId, project,
										                s_newStartDate,
										                s_newEndDate, stRate,
										                otRate, totalDays,
										                percentageAllocation,
										                currency);
									        } else if (subtierSearch[i]
									                .getValue('custrecord_stvd_rate_type') == "1") { // type
										        // is
										        // hourly
										        finalContractCost += getHourlyContractCost(
										                employeeId, project,
										                s_newStartDate,
										                s_newEndDate, stRate,
										                otRate, totalDays,
										                percentageAllocation,
										                currency);
									        } else { // day

									        }

								        }
							        }
						        }
					        });
				}
			}

			nlapiLogExecution('debug', 'final salary cost', finalSalaryCost);
			nlapiLogExecution('debug', 'final contract cost', finalContractCost);

			nlapiSetFieldValue('custrecord_urt_contract_cost',
			        finalContractCost);
			nlapiSetFieldValue('custrecord_urt_salaried_cost', finalSalaryCost);

			var totalCost = finalContractCost + finalSalaryCost;

			if (totalCost > 0) {
				var contractCostPercent = (finalContractCost * 100) / totalCost;
				nlapiSetFieldValue('custrecord_urt_contractor_cost_percent',
				        contractCostPercent);
			} else {
				nlapiSetFieldValue('custrecord_urt_contractor_cost_percent', 0);
			}
		}
	} catch (err) {
		nlapiLogExecution('ERROR', 'userEventAfterSubmit', err);
	}
}

// get the salary cost for all the employees on this project from the salary
// upload JE
function getSalaryCost(employeeList, projectId, startDate, endDate) {
	try {
		var jeSalarySearch = nlapiSearchRecord('journalentry', '1369', [
		        new nlobjSearchFilter('custcol_sow_project', null, 'anyof',
		                projectId),
		        // new nlobjSearchFilter('custcol_employee_id', null, 'anyof',
		        // employeeList),
		        new nlobjSearchFilter('trandate', null, 'within', startDate,
		                endDate) ]);

		var salaryCost = 0;

		if (jeSalarySearch) {

			jeSalarySearch.forEach(function(je) {
				var currency = je.getText('currency', null, 'group');
				var amount = je.getValue('fxamount', null, 'sum');
				var exchangeRate = nlapiExchangeRate(currency, 'USD', endDate);
				var convertedAmount = exchangeRate * amount;
				salaryCost += convertedAmount;
			});
		}

		return parseFloat(salaryCost.toFixed(2));
	} catch (err) {
		nlapiLogExecution('ERROR', 'getSalaryCost', err);
		throw err;
	}
}

function getMonthlyContractCost(employeeId, projectId, startDate, endDate,
        stRate, otRate, totalDaysInMonth, percentageAllocation, currency)
{
	try {
		// get the no. of hours the employee is allocated during this period for
		// this project
		var percentageNumeric = (parseFloat(percentageAllocation.split("%")[0]) / 100);
		var allocatedHours = getWorkingDays(startDate, endDate)
		        * percentageNumeric;

		// calculate the salary cost
		var salaryCost = (stRate / totalDaysInMonth) * allocatedHours;

		var exchangeRate = nlapiExchangeRate(currency, 'USD', endDate);
		var convertedAmount = exchangeRate * salaryCost;
		nlapiLogExecution('debug', 'contract monthly', convertedAmount + " = "
		        + exchangeRate + " * " + salaryCost);
		return parseFloat(convertedAmount.toFixed(2));

		// not taking timesheet into consideration
		var percentageNumeric = (parseFloat(percentageAllocation.split("%")[0]) / 100);
		var timesheetData = getMonthlyFilledTimesheet(employeeId, projectId,
		        startDate, endDate);
		var salaryCost = (stRate / totalDaysInMonth) * timesheetData.ST
		        * percentageNumeric;
		var exchangeRate = nlapiExchangeRate(currency, 'USD', endDate);
		var convertedAmount = exchangeRate * salaryCost;
		return parseFloat(convertedAmount.toFixed(2));
	} catch (err) {
		nlapiLogExecution('ERROR', 'getMonthlyContractCost', err);
		throw err;
	}
}

function getMonthlyFilledTimesheet(employeeId, projectId, startDate, endDate) {
	try {
		var stDays = 0;
		var otDays = 0;

		var timeBillSearch = nlapiSearchRecord('timebill', 'customsearch3270', [
		        new nlobjSearchFilter('employee', null, 'anyof', employeeId),
		        new nlobjSearchFilter('customer', null, 'anyof', projectId),
		        new nlobjSearchFilter('date', null, 'within', startDate,
		                endDate) ]);

		if (timeBillSearch) {

			timeBillSearch.forEach(function(time) {
				var item = time.getValue('item', null, 'group');

				if (item == '2425') {
					otDays = time.getValue('date', null, 'count');
				} else {
					stDays = time.getValue('date', null, 'count');
				}
			});
		}

		return {
		    ST : stDays,
		    OT : otDays
		};
	} catch (err) {
		nlapiLogExecution('ERROR', 'getMonthlyFilledTimesheet', err);
		throw err;
	}
}

// get the salary cost for the contract employee for the period
function getHourlyContractCost(employeeId, projectId, startDate, endDate,
        stRate, otRate, totalDaysInMonth, percentageAllocation, currency)
{
	try {
		// get the no. of hours the employee is allocated during this period for
		// this project
		var percentageNumeric = (parseFloat(percentageAllocation.split("%")[0]) / 100);
		var allocatedHours = getWorkingDays(startDate, endDate) * 8
		        * percentageNumeric;

		// calculate the salary cost
		var salaryCost = stRate * allocatedHours;
		var exchangeRate = nlapiExchangeRate(currency, 'USD', endDate);
		var convertedAmount = exchangeRate * salaryCost;
		return parseFloat(convertedAmount.toFixed(2));

		// not taking timesheets into considerations
		var timesheetData = getHourlyFilledTimesheet(employeeId, projectId,
		        startDate, endDate);
		var salaryCost = stRate * timesheetData.ST;
		var exchangeRate = nlapiExchangeRate(currency, 'USD', endDate);
		var convertedAmount = exchangeRate * salaryCost;
		return parseFloat(convertedAmount.toFixed(2));
	} catch (err) {
		nlapiLogExecution('ERROR', 'getHourlyContractCost', err);
		throw err;
	}
}

// get the no. of hours the employee is allocated to this project for this
// period
function getHourlyFilledTimesheet(employeeId, projectId, startDate, endDate) {
	try {
		var stHours = 0;
		var otHours = 0;

		var timeBillSearch = nlapiSearchRecord('timebill', 'customsearch3269', [
		        // new nlobjSearchFilter('employee', null, 'anyof', employeeId),
		        new nlobjSearchFilter('customer', null, 'anyof', projectId),
		        new nlobjSearchFilter('formuladate', null, 'within', startDate,
		                endDate).setFormula('{date}') ]);

		if (timeBillSearch) {

			timeBillSearch.forEach(function(time) {
				var item = time.getValue('item', null, 'group');

				if (item == '2425') {
					otHours = time.getValue('durationdecimal', null, 'sum');
				} else if (item == '2222' || item == '2221' || item == '2426'
				        || item == '2224') {
					stHours = time.getValue('durationdecimal', null, 'sum');
				}
			});
		}

		return {
		    ST : stHours,
		    OT : otHours
		};
	} catch (err) {
		nlapiLogExecution('ERROR', 'getHourlyFilledTimesheet', err);
		throw err;
	}
}