/**
 * Get the employee details using the provided email id
 * 
 * Version Date Author Remarks
 * 
 * 1.00 20 May 2016 Nitish Mishra
 * 
 */

function postRESTlet(dataIn) {
	var response = new Response();

	try {
		// var employeeId = "";
		// if (dataIn.EmailId) {
		// employeeId = getUserUsingEmailId(dataIn.EmailId);
		// } else if (dataIn.Bench) {
		// employeeId = getAllBenchAllocatedResource();
		// }

		// nlapiLogExecution('debug', 'dataIn', JSON.stringify(dataIn));
		var requestType = dataIn.RequestType;

		switch (requestType) {

			case Sharepoint_Constants.Request.Get:
				response.Data = getEmployeeData();
				response.Status = true;
			break;
		}

	} catch (err) {
		nlapiLogExecution('ERROR', 'postRESTlet', err);
		response.Data = err;
	}
	nlapiLogExecution('debug', 'response', JSON.stringify(response));
	return response;
}

function getEmployeeData() {
	try {
		var parentPractices = [ '@NONE@' ];

		var employeeSearch = searchRecord('employee', null, [
		        new nlobjSearchFilter('custentity_employee_inactive', null,
		                'is', 'F'),
		        new nlobjSearchFilter('custentity_implementationteam', null,
		                'is', 'F') ], [
		        new nlobjSearchColumn('firstname'),
		        new nlobjSearchColumn('middlename'),
		        new nlobjSearchColumn('lastname'),
		        new nlobjSearchColumn('title'),
		        new nlobjSearchColumn('hiredate'),
		        new nlobjSearchColumn('custentity_fusion_empid'),
		        new nlobjSearchColumn('custentity_reportingmanager'),
		        new nlobjSearchColumn('department'),
		        new nlobjSearchColumn('employeestatus'),
		        new nlobjSearchColumn('location'),
		        new nlobjSearchColumn('custentity_persontype'),
		        new nlobjSearchColumn('custrecord_practicehead', 'department'),
		        new nlobjSearchColumn('custrecord_parent_practice',
		                'department') ]);

		var employeeList = [];

		if (employeeSearch) {

			employeeSearch.forEach(function(employee) {
				parentPractices.push(employee.getValue(
				        'custrecord_parent_practice', 'department'));

				employeeList.push({
				    Id : employee.getId(),
				    Name : employee.getValue('firstname') + " "
				            + employee.getValue('lastname'),
				    Title : employee.getValue('title'),
				    HireDate : employee.getValue('hiredate'),
				    FusionId : employee.getValue('custentity_fusion_empid'),
				    ReportingManager : employee
				            .getText('custentity_reportingmanager'),
				    SubPractice : employee.getText('department'),
				    Level : employee.getText('employeestatus'),
				    Location : employee.getText('location'),
				    Type : employee.getText('custentity_persontype'),
				    SubPracticeHead : employee.getText(
				            'custrecord_practicehead', 'department'),
				    Practice : employee.getValue('custrecord_parent_practice',
				            'department'),
				    PracticeHead : '',
				    AllocationList : []
				});
			});
		}

		// search for the employee's current allocation
		var allocationSearch = searchRecord(
		        'resourceallocation',
		        null,
		        [
		                new nlobjSearchFilter('enddate', null, 'onorafter',
		                        'today'),
		                new nlobjSearchFilter('startdate', null, 'onorbefore',
		                        'today') ],
		        [
		                new nlobjSearchColumn('formulanumeric')
		                        .setFormula("CASE WHEN INSTR({company} , 'Bench', 1) > 0 THEN 1 ELSE 0 END"),
		                new nlobjSearchColumn('resource'),
		                new nlobjSearchColumn('customer'),
		                new nlobjSearchColumn('company') ]);

		if (allocationSearch) {

			allocationSearch.forEach(function(allocation) {

				for (var i = 0; i < employeeList.length; i++) {

					if (allocation.getValue('resource') == employeeList[i].Id) {
						employeeList[i].AllocationList
						        .push({
						            Project : allocation.getText('company'),
						            Customer : allocation.getText('customer'),
						            IsBench : allocation
						                    .getValue('formulanumeric') == 1
						        });

						break;
					}
				}
			});
		}

		// add the parent practice details
		var practiceSearch = nlapiSearchRecord('department', null, null, [
		        new nlobjSearchColumn('name'),
		        new nlobjSearchColumn('custrecord_practicehead') ]);

		if (practiceSearch) {

			for (var i = 0; i < employeeList.length; i++) {
				employeeList[i].Id = ""; //removing the id
				
				for (var j = 0; j < practiceSearch.length; j++) {

					if (employeeList[i].Practice == practiceSearch[j].getId()) {
						employeeList[i].Practice = practiceSearch[j]
						        .getValue("name");
						employeeList[i].PracticeHead = practiceSearch[j]
						        .getText("custrecord_practicehead");
						break;
					}
				}
			}
		}

		return employeeList;
	} catch (err) {
		nlapiLogExecution('ERROR', 'getEmployeeData', err);
		throw err;
	}
}

function getAllBenchAllocatedResource() {
	try {
		var employeeList = [ '@NONE@' ];

		// search for the employee's current allocation
		var allocationSearch = searchRecord(
		        'resourceallocation',
		        null,
		        [
		                new nlobjSearchFilter('enddate', null, 'onorafter',
		                        'today'),
		                new nlobjSearchFilter('startdate', null, 'onorbefore',
		                        'today'),
		                new nlobjSearchFilter('formulanumeric', null,
		                        'equalto', 1)
		                        .setFormula("CASE WHEN INSTR({company} , 'Bench', 1) > 0 THEN 1 ELSE 0 END") ],
		        [ new nlobjSearchColumn('resource', null, 'group').setSort() ]);

		if (allocationSearch) {

			allocationSearch.forEach(function(allocation) {
				employeeList.push(allocation
				        .getValue('resource', null, 'group'));
			});
		}

		return employeeList;
	} catch (err) {
		nlapiLogExecution('ERROR', 'getEmployeeData', err);
		throw err;
	}
}