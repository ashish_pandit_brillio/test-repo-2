/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       22 Dec 2017     deepak.srinivas
 *
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType
 * 
 * @param {String} type Operation types: create, edit, delete, xedit,
 *                      approve, cancel, reject (SO, ER, Time Bill, PO & RMA only)
 *                      pack, ship (IF only)
 *                      dropship, specialorder, orderitems (PO only) 
 *                      paybills (vendor payments)
 * @returns {Void}
 */
function userEventAfterSubmit(type){
  
	try{
		var dataRow = [];
		var JSON_ = {};
		var current_date = nlapiDateToString(new Date());
		var d_currentDate = nlapiStringToDate(current_date);
		
		var loadRec = nlapiLoadRecord(nlapiGetRecordType(),nlapiGetRecordId());
		var data_1 = loadRec.getFieldValue('custrecord_json_mnth_end_effrt');
		var b_isconfirmed=loadRec.getFieldValue('custrecord_is_mnth_effrt_confirmed');
		//if(data_1 ){
      if(data_1 && b_isconfirmed=="T"){
		var data_1_parse = JSON.parse(data_1);
		var temp_Obj = data_1_parse[0];
		var i_project = temp_Obj[0].project;
		if(i_project){
		var load_Project = nlapiLoadRecord('job',parseInt(i_project));
		
	
			
				
				var project_id = load_Project.getFieldValue('entityid');
				var project_name = load_Project.getFieldValue('companyname');
				var projectLookup_billingType_T = load_Project.getFieldText('custentity_fp_rev_rec_type');
			}
			var SOW_ID = searchSow(i_project);
			JSON_ ={
				'ProjectID': project_id,
				'BillingType': projectLookup_billingType_T,
				'Status':'Booked',
				'SOWList':SOW_ID
			};
			dataRow.push(JSON_);
			// nlapiLogExecution('DEBUG','JSON',JSON.stringify(dataRow)); 
		//}
			//Include SFDC Integration call
			{
				var accessURL = getAccessToken();
		        var method = 'POST';
				
		        var response = nlapiRequestURL(accessURL,null,null,method);
		        var temp = response.body;
		       // nlapiLogExecution('DEBUG','JSON',temp);
		        var data = JSON.parse(response.body);
		        var access_token_SFDC = data.access_token;
		        var instance_URL_Sfdc = data.instance_url;
		    //    nlapiLogExecution('DEBUG','JSON',data);
			        
			        var dynamic_URL = instance_URL_Sfdc+'/services/apexrest/Sowbooked/v1.0/';
			        var auth = 'Bearer'+' '+access_token_SFDC;
			        
			        //Test
			      //Setting up Headers 
			        /*var headers = {"User-Agent-x": "SuiteScript-Call",
			                       "Authorization": "Bearer "+access_token_SFDC,
			                       "Content-Type": "application/json"};*/
			        
			        var headers = {"Authorization": "Bearer "+access_token_SFDC,
			        		 "Content-Type": "application/json",
			        		 "accept": "application/json"};
			        //
			        var method_ = 'POST';
			        var response_ = nlapiRequestURL(dynamic_URL,JSON.stringify(dataRow),headers,method_);
			       
			        var data = JSON.parse(response_.body);
			        nlapiLogExecution('DEBUG','Response SFDC',data); 
			        var message = data[0].message;
			        var details_ = data.details;
			      //  nlapiLogExecution('DEBUG','JSON',message); 
			     //   nlapiLogExecution('DEBUG','details_',details_); 
			        nlapiLogExecution('DEBUG','JSON',JSON.stringify(dataRow)); 
				
			}
			
			//End
        }
		}
		
	
	catch (err) {
		nlapiLogExecution('ERROR', 'After Submit Trigger', err);
		throw err;
	}
	
}
function searchSow(pro_id){
	try{
		var searchRes = nlapiSearchRecord('salesorder',null,[new nlobjSearchFilter('internalid','jobmain','anyof',pro_id),
		                                                     new nlobjSearchFilter('mainline',null,'is','T'),
		                                                     new nlobjSearchFilter('status',null,'anyof',['SalesOrd:F','SalesOrd:E','SalesOrd:G'])],
															[new nlobjSearchColumn('internalid'),
															 new nlobjSearchColumn('tranid'), 
															 new nlobjSearchColumn('custbody_opp_id_sfdc')]);
		var Sow_array = [];	
		if(searchRes){
			for(var i=0;i<searchRes.length;i++){
			Sow_array.push({
				SOW_ID: searchRes[i].getValue('tranid'),
				Opp_id: searchRes[i].getValue('custbody_opp_id_sfdc')
			});
			}
		}
		return Sow_array;
	}
	catch(e){
		nlapiLogExecution('DEBUG','searchSow Error',e);
	}
}