/**
 * @author Shweta
 */
// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
    /*
     Script Name : SUT_OT_Calculations_PDF.js
     Author      : Shweta Chopde
     Date        : 28 April 2014
     Description :
     
     Script Modification Log:
     
     -- Date --			-- Modified By --				--Requested By--				-- Description --
     
     Below is a summary of the process controls enforced by this script file.  The control logic is described
     more fully, below, in the appropriate function headers and code blocks.
     
     SUITELET
     - suiteletFunction(request, response)
     
     SUB-FUNCTIONS
     - The following sub-functions are called by the above core functions in order to maintain code
     modularization:
     
     - NOT USED
     */
}
// END SCRIPT DESCRIPTION BLOCK  ====================================



// BEGIN GLOBAL VARIABLE BLOCK  =====================================
{
    //  Initialize any Global Variables, in particular, debugging variables...




}
// END GLOBAL VARIABLE BLOCK  =======================================





// BEGIN SUITELET ==================================================

function suiteletFunction(request, response){

    try {
        var a_data_array = new Array();
        
        var a_hours_array = new Array();
        
        var a_employee_array = new Array();
        
        var a_line_array = new Array();
        var i_inc_cnt = 0;
        var i_cnt = 0;
        var i_hours_per_day;
        var i_hours_per_week;
        var i_days_for_month;
        var i_OT_applicable;
        var i_OT_rate_ST;
        var i_week_days;
        var a_location_data_array = new Array();
        var i_rate = 0;
        var otrVar;
        var ctrVar;
        var i_flag = 0;
        var i_project ;
		var i_project_text;
		var i_total_amount_INV = 0;
        var i_recordID = request.getParameter('i_recordID');
        nlapiLogExecution('DEBUG', 'suiteletFunction', ' Record ID  -->' + i_recordID);
        
        
        var i_record_type = 'invoice'
        
        if (_logValidation(i_recordID) && _logValidation(i_record_type)) {
            var o_recordOBJ = nlapiLoadRecord(i_record_type, i_recordID)
            
            if (_logValidation(o_recordOBJ)) 
			{
				 i_total_amount_INV = o_recordOBJ.getFieldValue('total')
		      nlapiLogExecution('DEBUG','afterSubmit_calculate_OT', ' Invoice Total --> ' + i_total_amount_INV);			
			
            
                var i_billable_time_count = o_recordOBJ.getLineItemCount('time')
                //nlapiLogExecution('DEBUG', 'suiteletFunction', ' Billable Time Count --> ' + i_billable_time_count);
                
                var i_tran_date = o_recordOBJ.getFieldValue('trandate')
                //nlapiLogExecution('DEBUG', 'suiteletFunction', ' Tran Date --> ' + i_tran_date);
                
                var i_tran_id = o_recordOBJ.getFieldValue('tranid')
                //nlapiLogExecution('DEBUG', 'suiteletFunction', ' Invoice Number --> ' + i_tran_id);
                
				  if (!_logValidation(i_tran_id)) 
				  {
				  	i_tran_id = ''
				  }
				
                var i_PO_ID = o_recordOBJ.getFieldValue('otherrefnum')
                //nlapiLogExecution('DEBUG', 'suiteletFunction', ' PO # --> ' + i_PO_ID);
                
				  if (!_logValidation(i_PO_ID)) 
				  {
				  	i_PO_ID = ''
				  }
				
                var i_terms = o_recordOBJ.getFieldText('terms')
                //nlapiLogExecution('DEBUG', 'suiteletFunction', ' Terms --> ' + i_terms);
	                 
				  if (!_logValidation(i_terms)) 
				  {
				  	i_terms = ''
				  }
				var i_customer = o_recordOBJ.getFieldValue('entity')
                nlapiLogExecution('DEBUG', 'suiteletFunction', ' Customer--> ' + i_customer);
				
				 var i_customer_address = get_customer_address(i_customer)
			 	nlapiLogExecution('DEBUG', 'suiteletFunction', ' Customer Address--> ' + i_customer_address);
				
				
				 var i_subsidiary = o_recordOBJ.getFieldValue('subsidiary')
				 var i_address = get_address(i_subsidiary)
				 nlapiLogExecution('DEBUG', 'suiteletFunction', ' Address --> ' + i_address);
                
				
				var i_line_count = o_recordOBJ.getLineItemCount('item')
				
			  if (_logValidation(i_line_count)) 
			  {
			  	i_project_1 = o_recordOBJ.getLineItemValue('item','job',1)
				i_project_text = o_recordOBJ.getLineItemValue('item','job_display',1)
				
				nlapiLogExecution('DEBUG','afterSubmit_calculate_OT', ' i_project_1 --> ' + i_project_1);
			   nlapiLogExecution('DEBUG','afterSubmit_calculate_OT', ' i_project_text --> ' + i_project_text);
			
				
			  }//Loop
				
				  if (!_logValidation(i_project_text)) 
				  {
				  	i_project_text = ''
				  }
                // ========================== Create Invoice PDF ==============================
                
                var image_URL = 'https://system.sandbox.netsuite.com/core/media/media.nl?id=11&c=3883006&h=e396b1ed7417b4cdd254'
                image_URL = nlapiEscapeXML(image_URL)
                
				i_address = formatAndReplaceSpacesofMessage(nlapiEscapeXML(i_address));
				i_customer_address = formatAndReplaceSpacesofMessage(nlapiEscapeXML(i_customer_address));
				
				
                var strVar = "";
                strVar += "<table border=\"0\" width=\"100%\">";
                strVar += "	<tr>";
                //strVar += "		<td colspan=\"3\">Image<\/td>";
                if (image_URL != '' && image_URL != null && image_URL != '' && image_URL != 'undefined' && image_URL != ' ' && image_URL != '&nbsp') 
				{
                    strVar += " <td colspan=\"3\"><img width=\"150\" height=\"75\" align=\"left\" src=\"" + image_URL + "\"><\/img><\/td>";
                }
                strVar += "		<td font-family=\"Helvetica\" font-size=\"9\" colspan=\"3\">"+i_address+"<\/td>";
                strVar += "	<\/tr>";
                strVar += "	<tr>";
                strVar += "		<td font-family=\"Helvetica\" align=\"center\" colspan=\"6\">________________________________________________________________________________<\/td>";
                strVar += "	<\/tr>";
                
                
                strVar += "	<tr>";
                strVar += "		<td font-family=\"Helvetica\" font-size=\"8\" colspan=\"3\">Invoice Date : " + i_tran_date + "<\/td>";
            
				strVar += "		<td font-family=\"Helvetica\" font-size=\"8\" colspan=\"3\">Invoice Number : " + i_tran_id + "<\/td>";
                strVar += "	<\/tr>";
                strVar += "	<tr>";
                strVar += "		<td font-family=\"Helvetica\" font-size=\"8\" colspan=\"3\">Client Information<\/td>";
				
                strVar += "		<td font-family=\"Helvetica\" font-size=\"8\" colspan=\"3\">Remit Payment To<\/td>";
                strVar += "	<\/tr>";
				
				strVar += "	<tr>";
                strVar += "		<td font-family=\"Helvetica\" font-size=\"8\" colspan=\"3\">"+i_customer_address+"<\/td>";
				
                strVar += "		<td font-family=\"Helvetica\" font-size=\"8\" colspan=\"3\"><\/td>";
                strVar += "	<\/tr>";
                strVar += "	<tr>";
                strVar += "		<td font-family=\"Helvetica\" font-size=\"8\" colspan=\"3\" >&nbsp;<\/td>";
                
                strVar += "		<td font-family=\"Helvetica\" font-size=\"8\" colspan=\"3\">&nbsp;<\/td>";
                strVar += "	<\/tr>";
                strVar += "	<tr>";
                strVar += "		<td font-family=\"Helvetica\" font-size=\"8\">Project<\/td>";
                strVar += "		<td font-family=\"Helvetica\" font-size=\"8\">"+nlapiEscapeXML(i_project_text)+"<\/td>";
                strVar += "		<td font-family=\"Helvetica\" font-size=\"8\">&nbsp;<\/td>";
                strVar += "		<td font-family=\"Helvetica\" font-size=\"8\">Terms<\/td>";
                strVar += "		<td font-family=\"Helvetica\" font-size=\"8\">" + nlapiEscapeXML(i_terms) + "<\/td>";
                strVar += "		<td font-family=\"Helvetica\" font-size=\"8\"><\/td>";
                strVar += "	<\/tr>";
                strVar += "	<tr>";
                strVar += "		<td font-family=\"Helvetica\" font-size=\"8\">PO Number<\/td>";
                strVar += "		<td font-family=\"Helvetica\" font-size=\"8\">" + nlapiEscapeXML(i_PO_ID) + "<\/td>";
                strVar += "		<td font-family=\"Helvetica\" font-size=\"8\">&nbsp;<\/td>";
                strVar += "		<td font-family=\"Helvetica\" font-size=\"8\">&nbsp;<\/td>";
                strVar += "		<td font-family=\"Helvetica\" font-size=\"8\">&nbsp;<\/td>";
                strVar += "		<td font-family=\"Helvetica\" font-size=\"8\">&nbsp;<\/td>";
                strVar += "	<\/tr>";
                strVar += "	<tr>";
                strVar += "		<td font-family=\"Helvetica\" font-size=\"8\" colspan=\"6\">&nbsp;<\/td>";
                strVar += "	<\/tr>";
                strVar += "	<tr>";
                strVar += "		<td font-family=\"Helvetica\" font-size=\"8\" colspan=\"6\">&nbsp;<\/td>";
                strVar += "	<\/tr>";
                strVar += "	<tr>";
                strVar += "		<td font-family=\"Helvetica\" font-size=\"8\" colspan=\"6\">Notes:<\/td>";
                strVar += "	<\/tr>";
                strVar += "	<tr>";
                strVar += "		<td font-family=\"Helvetica\" font-size=\"8\" colspan=\"6\">&nbsp;<\/td>";
                strVar += "	<\/tr>";
                strVar += "	<tr>";
                strVar += "		<td font-family=\"Helvetica\" font-size=\"8\" colspan=\"6\"><b>Invoice Comments :<\/b><\/td>";
                strVar += "	<\/tr>";
                strVar += "	<tr>";
                strVar += "		<td font-family=\"Helvetica\" font-size=\"8\" colspan=\"6\">&nbsp;<\/td>";
                strVar += "	<\/tr>";
                
                strVar += "	<tr>";
                strVar += "		<td colspan=\"6\">";
                strVar += "		<table border=\"0\" width=\"100%\">";
                strVar += "			<tr>";
                strVar += "				<td border-top=\"1\" border-left=\"1\" border-right=\"1\" font-family=\"Helvetica\" align=\"center\" font-size=\"8\"><b>Weekend<\/b><\/td>";
                strVar += "				<td border-top=\"1\" border-right=\"1\" font-family=\"Helvetica\" align=\"center\" font-size=\"8\"><b>ST Rate<\/b><\/td>";
                strVar += "				<td border-top=\"1\" border-right=\"1\" font-family=\"Helvetica\" align=\"center\" font-size=\"8\"><b>ST Units<\/b><\/td>";
                strVar += "				<td border-top=\"1\" border-right=\"1\" font-family=\"Helvetica\" align=\"center\" font-size=\"8\"><b>ST Amount<\/b><\/td>";
				strVar += "				<td border-top=\"1\" border-right=\"1\" font-family=\"Helvetica\" align=\"center\" font-size=\"8\"><b>OT Rate<\/b><\/td>";
                strVar += "				<td border-top=\"1\" border-right=\"1\" font-family=\"Helvetica\" align=\"center\" font-size=\"8\"><b>OT Units<\/b><\/td>";
                strVar += "				<td border-top=\"1\" border-right=\"1\" font-family=\"Helvetica\" align=\"center\" font-size=\"8\"><b>OT Amount<\/b><\/td>";
                strVar += "			<\/tr>";
                strVar += "			<tr>";
                strVar += "				<td border-bottom=\"1\" border-right=\"1\" border-left=\"1\" font-family=\"Helvetica\" align=\"center\" font-size=\"8\">&nbsp;<\/td>";
                strVar += "				<td border-bottom=\"1\" border-right=\"1\" font-family=\"Helvetica\" align=\"center\" font-size=\"8\"><b>(Hourly)<\/b><\/td>";
                strVar += "				<td border-bottom=\"1\" border-right=\"1\" font-family=\"Helvetica\" align=\"center\" font-size=\"8\"><b>(Hours)<\/b><\/td>";
				strVar += "				<td border-bottom=\"1\" border-right=\"1\" font-family=\"Helvetica\" align=\"center\" font-size=\"8\">&nbsp;<\/td>";
                strVar += "				<td border-bottom=\"1\" border-right=\"1\" font-family=\"Helvetica\" align=\"center\" font-size=\"8\"><b>(Hourly)<\/b><\/td>";
                strVar += "				<td border-bottom=\"1\" border-right=\"1\" font-family=\"Helvetica\" align=\"center\" font-size=\"8\"><b>(Hours)<\/b><\/td>";
                strVar += "				<td border-bottom=\"1\" border-right=\"1\" font-family=\"Helvetica\" align=\"center\" font-size=\"8\">&nbsp;<\/td>";
                strVar += "			<\/tr>";
                
                
                
                
                if (_logValidation(i_billable_time_count)) {
                    for (var i = 1; i <= i_billable_time_count; i++) {
                        var i_date = o_recordOBJ.getLineItemValue('time', 'billeddate', i)
                        
                        var i_employee = o_recordOBJ.getLineItemValue('time', 'employeedisp', i)
                        
                        var i_project = o_recordOBJ.getLineItemValue('time', 'job', i)
                        
                        var i_item = o_recordOBJ.getLineItemValue('time', 'item', i)
                        
                        var i_rate = o_recordOBJ.getLineItemValue('time', 'rate', i)
                        
                        var i_hours = o_recordOBJ.getLineItemValue('time', 'quantity', i)
                        
						var i_marked = o_recordOBJ.getLineItemValue('time','apply',i)
							
                        nlapiLogExecution('DEBUG', 'suiteletFunction', ' Date --> ' + i_date);
                        
                        nlapiLogExecution('DEBUG', 'suiteletFunction', ' Employee --> ' + i_employee);
                        
                        nlapiLogExecution('DEBUG', 'suiteletFunction', ' Project --> ' + i_project);
                        
                        nlapiLogExecution('DEBUG', 'suiteletFunction', ' Item --> ' + i_item);
                        
                        nlapiLogExecution('DEBUG', 'suiteletFunction', ' Rate --> ' + i_rate);
                        
                        nlapiLogExecution('DEBUG', 'suiteletFunction', ' Hours --> ' + i_hours);
                        
						if(i_marked == 'T')
						{
						 var d_start_of_week = get_start_of_week(i_date)
                         var d_end_of_week = get_end_of_week(i_date)
                        
                         a_employee_array[i_cnt++] = i_employee + '&&&' + i_project + '&&&' + i_item + '&&&' + d_start_of_week + '&&&' + d_end_of_week
                        
                         nlapiLogExecution('DEBUG', 'suiteletFunction', ' *******************Start & End Of Week *************** --> [' + d_start_of_week + ' , ' + d_end_of_week + ']');
                        }
                       
                    }//Loop Billable Time Count Outer				
                    //nlapiLogExecution('DEBUG', 'suiteletFunction', ' Employee Array Before --> ' + a_employee_array);
                    //nlapiLogExecution('DEBUG', 'suiteletFunction', ' Employee Array Length Before --> ' + a_employee_array.length);
                    
                    a_employee_array = removearrayduplicate(a_employee_array)
                    
                    //nlapiLogExecution('DEBUG', 'suiteletFunction', ' Employee Array --> ' + a_employee_array);
                    //nlapiLogExecution('DEBUG', 'suiteletFunction', ' Employee Array Length--> ' + a_employee_array.length);
                }//Billable Time Count
                var a_split_array = new Array();
                
                for (var k = 0; k < a_employee_array.length; k++) 
				{
                    var i_hours = 0
                    var i_minutes = 0;
                    var i_seconds = 0;
                    
                    a_split_array = a_employee_array[k].split('&&&')
                    
                    var i_employee_arr = a_split_array[0]
                    
                    var i_project_arr = a_split_array[1]
                    
                    var i_item_arr = a_split_array[2]
                    
                    var i_start_of_week_arr = a_split_array[3]
                    
                    var i_end_of_week_arr = a_split_array[4]
                    
                    //nlapiLogExecution('DEBUG', 'suiteletFunction', ' Employee --> ' + i_employee_arr + ' Project --> ' + i_project_arr + ' Item --> ' + i_item_arr + '  Start Of Week --> ' + i_start_of_week_arr + ' End Of Week --> ' + i_end_of_week_arr);
                    
                    for (var lt = 1; lt <= i_billable_time_count; lt++) 
					{
                        var i_date_new = o_recordOBJ.getLineItemValue('time', 'billeddate', lt)
                        
                        var i_employee_new = o_recordOBJ.getLineItemValue('time', 'employeedisp', lt)
                        
                        var i_project_new = o_recordOBJ.getLineItemValue('time', 'job', lt)
                        
                        var i_item_new = o_recordOBJ.getLineItemValue('time', 'item', lt)
                        
                        var i_rate_new = o_recordOBJ.getLineItemValue('time', 'rate', lt)
                        
                        var i_hours_new = o_recordOBJ.getLineItemValue('time', 'quantity', lt)
                        
                        i_rate = i_rate_new
                        
                        var d_start_of_week_new = get_start_of_week(i_date_new)
                        var d_end_of_week_new = get_end_of_week(i_date_new)
                        
                        var a_split_time_array = new Array();
                        
                        if ((i_employee_arr == i_employee_new) && (i_project_arr == i_project_new) && (i_item_arr == i_item_new)) 
						{
                            if ((d_start_of_week_new == i_start_of_week_arr) && (d_end_of_week_new == i_end_of_week_arr)) 
							{
                                a_split_time_array = i_hours_new.split(':')                                
                                var hours = a_split_time_array[0]
                                var minutes = a_split_time_array[1]
                                
                                i_hours = parseFloat(i_hours) + parseFloat(hours)
                                i_minutes = parseFloat(i_minutes) + parseFloat(minutes)
                                
                                a_location_data_array[i_inc_cnt++] = i_date_new + '$$$$' + i_employee_new + '$$$$' + i_project_new + '$$$$' + i_item_new + '$$$$' + i_rate_new + '$$$$' + i_hours_new;
                                
                            }//Start & End Date Validation 
                        }//Employee / Project / Item	
                    }//Billable Count		
                    if (parseInt(i_minutes) < 0) 
					{
                        i_minutes = parseFloat(i_minutes) + parseFloat(60)
                        //nlapiLogExecution('DEBUG', 'suiteletFunction', ' duration_2 minus -->' + i_minutes);
                    }
                    var i_total_time = (i_hours) + ':' + (i_minutes)
                    nlapiLogExecution('DEBUG', 'suiteletFunction', ' @@@@@@@@@@@@  Total Time  @@@@@@@@@@@@--> ' + i_total_time);
                    
                    nlapiLogExecution('DEBUG', 'suiteletFunction', ' Location Data Array --> ' + a_location_data_array);
                    
                    var i_employeeID = get_employeeID(i_employee_arr)
                    nlapiLogExecution('DEBUG', 'suiteletFunction', ' Employee ID --> ' + i_employeeID);
                    
                    var i_location = get_time_sheet_location(i_employeeID, i_project_arr, i_item_arr, i_start_of_week_arr, i_end_of_week_arr)
                    nlapiLogExecution('DEBUG', 'suiteletFunction', ' Location --> ' + i_location);
                    
                    var a_project_array = get_project_OT_values(i_project_arr);
                    nlapiLogExecution('DEBUG', 'suiteletFunction', ' Project Data Array --> ' + a_project_array);
                    
                    var a_split_project_array = new Array();
                    
                    if (_logValidation(a_project_array)) {
                        a_split_project_array = a_project_array[0].split('!!')
                        
                        i_hours_per_day = a_split_project_array[0]
                        
                        i_hours_per_week = a_split_project_array[1]
                        
                        i_days_for_month = a_split_project_array[2]
                        
                        i_OT_applicable = a_split_project_array[3]
                        
                        i_OT_rate_ST = a_split_project_array[4]
                        
                        i_week_days = a_split_project_array[5]
                        
                    }//Project Array
                    if (i_OT_applicable == 'T') {
                        strVar += "			<tr>";
                        strVar += "				<td border-left=\"1\" border-right=\"1\" font-family=\"Helvetica\" align=\"center\" font-size=\"8\"><b>" + nlapiEscapeXML(i_employee_arr) + "<\/b><\/td>";
                        strVar += "				<td border-right=\"1\" font-family=\"Helvetica\" align=\"center\" font-size=\"8\">&nbsp;<\/td>";
                        strVar += "				<td border-right=\"1\" font-family=\"Helvetica\" align=\"center\" font-size=\"8\">&nbsp;<\/td>";
                        strVar += "				<td border-right=\"1\" font-family=\"Helvetica\" align=\"center\" font-size=\"8\">&nbsp;<\/td>";
                        strVar += "				<td border-right=\"1\" font-family=\"Helvetica\" align=\"center\" font-size=\"8\">&nbsp;<\/td>";
						strVar += "				<td border-right=\"1\" font-family=\"Helvetica\" align=\"center\" font-size=\"8\">&nbsp;<\/td>";
                        strVar += "				<td border-right=\"1\" font-family=\"Helvetica\" align=\"center\" font-size=\"8\">&nbsp;<\/td>";
                        strVar += "			<\/tr>";
                        
                        if (i_location == 'California') 
						{
                         //  if (i_flag == 0) 
							{
                                i_flag = 1
                                otrVar = calculate_OT_California(i_project_arr ,i_item_arr , i_start_of_week_arr ,  i_end_of_week_arr,i_employee_arr,i_recordID, i_location, i_project_new, i_item_new, a_location_data_array, i_rate, i_hours_per_day, i_hours_per_week, i_days_for_month, i_OT_applicable, i_OT_rate_ST, i_week_days)
                                strVar += otrVar;
                                //nlapiLogExecution('DEBUG', 'suiteletFunction', ' California strVar --> ' + strVar);
                            }
                            
                        }//California
                        if (i_location != 'California') 
						{
                            ctrVar = calculate_OT_other_than_California(i_end_of_week_arr,i_rate_new, i_date_new, i_employee_arr, i_recordID, i_location, i_project_new, i_item_new, i_total_time, i_hours_per_day, i_hours_per_week, i_days_for_month, i_OT_applicable, i_OT_rate_ST, i_week_days)
                            strVar += ctrVar;
                            //nlapiLogExecution('DEBUG', 'suiteletFunction', ' Not California strVar --> ' + strVar);
                            
                        }//Other than California	
                    }//OT Applicable			 		 	 
                }//Loop			
				
				
				var i_total_amount = add_OT_line_item(i_recordID,i_total_amount_INV)
				
                strVar += "			<tr>";
                strVar += "				<td border-top=\"0\" border-left=\"1\" border-bottom=\"1\" font-family=\"Helvetica\" align=\"center\" font-size=\"8\">&nbsp;<\/td>";
				strVar += "				<td border-top=\"0\" border-bottom=\"1\" font-family=\"Helvetica\" align=\"center\" font-size=\"8\">&nbsp;<\/td>";
                strVar += "				<td border-top=\"0\" border-bottom=\"1\" font-family=\"Helvetica\" align=\"center\" font-size=\"8\">&nbsp;<\/td>";
                strVar += "				<td border-top=\"0\" border-bottom=\"1\" font-family=\"Helvetica\" align=\"center\" font-size=\"8\">&nbsp;<\/td>";
                strVar += "				<td border-top=\"0\" border-bottom=\"1\" font-family=\"Helvetica\" align=\"center\" font-size=\"8\">&nbsp;<\/td>";
                strVar += "				<td border-top=\"0\" border-bottom=\"1\" font-family=\"Helvetica\" align=\"center\" font-size=\"8\"><b>Invoice Total :<\/b><\/td>";
                strVar += "				<td border-top=\"0\" border-right=\"1\" border-bottom=\"1\" font-family=\"Helvetica\" align=\"center\" font-size=\"8\"><b>"+i_total_amount+"<\/b><\/td>";
                strVar += "			<\/tr>";
                strVar += "		<\/table>";
                strVar += "		<\/td>";
                strVar += "	<\/tr>";
                
                
                strVar += "	<tr>";
                strVar += "		<td font-family=\"Helvetica\" align=\"center\" font-size=\"8\" colspan=\"6\">&nbsp;<\/td>";
                strVar += "	<\/tr>";
                strVar += "	<tr>";
                strVar += "		<td font-family=\"Helvetica\" align=\"center\" font-size=\"8\" colspan=\"6\">&nbsp;<\/td>";
                strVar += "	<\/tr>";
                strVar += "	<tr>";
                strVar += "		<td font-family=\"Helvetica\" align=\"center\" font-size=\"8\" colspan=\"6\">&nbsp;<\/td>";
                strVar += "	<\/tr>";
                strVar += "<\/table>";
                strVar += "";
                
                var xml = "<?xml version=\"1.0\"?>\n<!DOCTYPE pdf PUBLIC \"-//big.faceless.org//report\" \"report-1.1.dtd\">\n";
                xml += "<pdf>";
                xml += "<head>";
                xml += "  <macrolist>";
                xml += " <macro id=\"myfooter\">";
                xml += "<i font-size=\"7\">";
                xml += " <table width=\"100%\">"
                xml += "<tr>"
                xml += "<td border-top=\"1\" font-size=\"8\"  font-family=\"Helvetica\" width=\"100%\" align=\"center\"> Brillio LLC. certifies that this invoice is correct, that payment has not been received and that it is presented with the understanding that the<\/td>"
                xml += "<\/tr> "
                xml += "<tr>"
                xml += "<td font-size=\"8\"  font-family=\"Helvetica\" width=\"100%\" align=\"center\">amount to be paid thereunder can be audited to verify.<\/td>"
                xml += "<\/tr> "
                xml += "<\/table> "
                xml += "<\/i>";
                xml += "    <\/macro>";
                xml += "  <\/macrolist>";
                xml += "<\/head>";
                
                xml += "<body margin-top=\"0pt\"  footer=\"myfooter\" footer-height=\"2em\">";
                xml += strVar;
                xml += "</body>\n</pdf>";
                
                // run the BFO library to convert the xml document to a PDF
                var file = nlapiXMLToPDF(xml);
                //Create PDF File with TimeStamp in File Name
                
                var d_currentTime = new Date();
                var timestamp = d_currentTime.getTime();
                //Create PDF File with TimeStamp in File Name
                
                var fileObj = nlapiCreateFile('Invoice - ST OT Calculations' + '.pdf', 'PDF', file.getValue());
                fileObj.setFolder(14);// Folder PDF File is to be stored
                var fileId = nlapiSubmitFile(fileObj);
                nlapiLogExecution('DEBUG', 'OT Calculations', '************* PDF ID *********** -->' + fileId);
                response.setContentType('PDF', 'Invoice - ST OT Calculations.pdf', 'inline');
                response.write(file.getValue());
            }//Record OBJ 	
        }//Record ID & Record Type
    } 
    catch (exception) {
        nlapiLogExecution('DEBUG', 'ERROR', ' Exception Caught --> ' + exception);
    }
    
    return true;
    
}

// END SUITELET ====================================================




// BEGIN OBJECT CALLED/INVOKING FUNCTION ===================================================
function _logValidation(value){
    if (value != null && value.toString() != null && value != '' && value != undefined && value.toString() != undefined && value != 'undefined' && value.toString() != 'undefined' && value.toString() != 'NaN' && value != NaN) {
        return true;
    }
    else {
        return false;
    }
}



// return an array of date objects for start (monday)
// and end (sunday) of week based on supplied 
// date object or current date
function get_start_of_week(date){
    // If no date object supplied, use current date
    // Copy date so don't modify supplied date
    var now = date ? new Date(date) : new Date();
    
    // set time to some convenient value
    now.setHours(0, 0, 0, 0);
    
    // Get the previous Monday
    var monday = new Date(now);
    monday.setDate(monday.getDate() - monday.getDay() + 1);
    monday = nlapiDateToString(monday)
    // nlapiLogExecution('DEBUG','afterSubmit_calculate_OT', ' Monday --> ' + monday);
    
    // Get next Sunday
    var sunday = new Date(now);
    sunday.setDate(sunday.getDate() - sunday.getDay() + 7);
    sunday = nlapiDateToString(sunday)
    // nlapiLogExecution('DEBUG','afterSubmit_calculate_OT', ' Sunday --> ' + sunday);
    // Return array of date objects
    return monday;
}

function get_end_of_week(date){
    // If no date object supplied, use current date
    // Copy date so don't modify supplied date
    var now = date ? new Date(date) : new Date();
    
    // set time to some convenient value
    now.setHours(0, 0, 0, 0);
    
    // Get the previous Monday
    var monday = new Date(now);
    monday.setDate(monday.getDate() - monday.getDay() + 1);
    monday = nlapiDateToString(monday)
    // nlapiLogExecution('DEBUG','afterSubmit_calculate_OT', ' Monday --> ' + monday);
    
    // Get next Sunday
    var sunday = new Date(now);
    sunday.setDate(sunday.getDate() - sunday.getDay() + 7);
    sunday = nlapiDateToString(sunday)
    // nlapiLogExecution('DEBUG','afterSubmit_calculate_OT', ' Sunday --> ' + sunday);
    // Return array of date objects
    return sunday;
}


function removearrayduplicate(array){
    var newArray = new Array();
    label: for (var i = 0; i < array.length; i++) {
        for (var j = 0; j < array.length; j++) {
            if (newArray[j] == array[i]) 
                continue label;
        }
        newArray[newArray.length] = array[i];
    }
    return newArray;
}

function get_project_OT_values(i_project){
    var i_week_days = 0;
    var a_data_array = new Array();
    
    if (_logValidation(i_project)) {
        var filter = new Array();
        filter[0] = new nlobjSearchFilter('internalid', null, 'is', i_project);
        
        var columns = new Array();
        columns[0] = new nlobjSearchColumn('internalid');
        
        var a_search_results = nlapiSearchRecord('job', null, filter, columns);
        
        if (_logValidation(a_search_results)) {
            var i_internal_id = a_search_results[0].getValue('internalid');
            //nlapiLogExecution('DEBUG', 'get_project_OT_values', ' Internal ID -->' + i_internal_id);
            
            if (_logValidation(i_internal_id)) {
                var o_recordOBJ = nlapiLoadRecord('job', i_internal_id)
                
                if (_logValidation(o_recordOBJ)) {
                    var i_hours_per_day = o_recordOBJ.getFieldValue('custentity_hoursperday')
                    
                    var i_hours_per_week = o_recordOBJ.getFieldValue('custentity_hoursperweek')
                    
                    var i_days_for_month = o_recordOBJ.getFieldValue('custentity_daysformonth')
                    
                    var i_OT_applicable = o_recordOBJ.getFieldValue('custentity_otapplicable')
                    
                    var i_OT_rate_ST = o_recordOBJ.getFieldValue('custentity3')
                    
                    var i_sunday = o_recordOBJ.getFieldValue('custentity_sunday')
                    
                    var i_monday = o_recordOBJ.getFieldValue('custentity_monday')
                    
                    var i_tuesday = o_recordOBJ.getFieldValue('custentity_tuesday')
                    
                    var i_wednesday = o_recordOBJ.getFieldValue('custentity_wednesday')
                    
                    var i_thursday = o_recordOBJ.getFieldValue('custentity_thursday')
                    
                    var i_friday = o_recordOBJ.getFieldValue('custentity_fridaycustomerform')
                    
                    var i_saturday = o_recordOBJ.getFieldValue('custentity_saturday')
                    
                    if (i_monday == 'T' && i_tuesday == 'T' && i_wednesday == 'T' && i_thursday == 'T' && i_friday == 'T') {
                        i_week_days = 5;
                        //nlapiLogExecution('DEBUG', 'get_project_OT_values', ' i_week_days -->' + i_week_days);
                    }
                    else 
                        if (i_monday == 'T' && i_tuesday == 'T' && i_wednesday == 'T' && i_thursday == 'T' && i_friday == 'T' && i_saturday == 'T') {
                            i_week_days = 6;
                            //nlapiLogExecution('DEBUG', 'get_project_OT_values', ' i_week_days -->' + i_week_days);
                        }
                        else 
                            if (i_sunday == 'T' && i_monday == 'T' && i_tuesday == 'T' && i_wednesday == 'T' && i_thursday == 'T' && i_friday == 'T' && i_saturday == 'T') {
                                i_week_days = 7;
                                //nlapiLogExecution('DEBUG', 'get_project_OT_values', ' i_week_days -->' + i_week_days);
                            }
                    
                    a_data_array[0] = i_hours_per_day + '!!' + i_hours_per_week + '!!' + i_days_for_month + '!!' + i_OT_applicable + '!!' + i_OT_rate_ST + '!!' + i_week_days;
                    
                }//Record OBJ			
            }//Internal ID
        }//Search Results
    }//Project
    //nlapiLogExecution('DEBUG', 'get_project_OT_values', ' Data Array -->' + a_data_array);
    
    
    return a_data_array;
    
}//Project OT 
function get_time_sheet_location(i_employee, i_project, i_item, d_start_of_week, d_end_of_week){
    var i_location;
    
    if (_logValidation(i_employee) && _logValidation(i_project) && _logValidation(i_item) && _logValidation(d_start_of_week) && _logValidation(d_end_of_week)) {
        var filter = new Array();
        filter[0] = new nlobjSearchFilter('employee', null, 'is', i_employee);
        filter[1] = new nlobjSearchFilter('customer', null, 'is', i_project);
        filter[2] = new nlobjSearchFilter('item', null, 'is', i_item);
        filter[3] = new nlobjSearchFilter('date', null, 'onorafter', d_start_of_week);
        filter[4] = new nlobjSearchFilter('date', null, 'onorbefore', d_end_of_week);
        
        var columns = new Array();
        columns[0] = new nlobjSearchColumn('internalid');
        columns[1] = new nlobjSearchColumn('custcol_worklocation');
        
        var a_search_results = nlapiSearchRecord('timebill', null, filter, columns);
        //nlapiLogExecution('DEBUG', 'get_time_sheet_location', ' a_search_results -->' + a_search_results.length);
        
        if (a_search_results != null && a_search_results != '' && a_search_results != undefined) {
            var i_internal_id = a_search_results[0].getValue('internalid');
            //nlapiLogExecution('DEBUG', 'get_time_sheet_location', ' Internal ID -->' + i_internal_id);
            
            i_location = a_search_results[0].getText('custcol_worklocation');
            nlapiLogExecution('DEBUG', 'get_time_sheet_location', ' Location -->' + i_location);
        }
        
    }//Employee / Project / Item
    return i_location;
    
}//Item
function get_employeeID(i_employee){
    var i_internal_id;
    if (_logValidation(i_employee)) {
        var filter = new Array();
        filter[0] = new nlobjSearchFilter('entityid', null, 'is', i_employee);
        
        var columns = new Array();
        columns[0] = new nlobjSearchColumn('internalid');
        
        var a_search_results = nlapiSearchRecord('employee', null, filter, columns);
        //nlapiLogExecution('DEBUG', 'get_employeeID', ' a_search_results -->' + a_search_results.length);
        
        if (a_search_results != null && a_search_results != '' && a_search_results != undefined) {
            i_internal_id = a_search_results[0].getValue('internalid');
            //nlapiLogExecution('DEBUG', 'get_employeeID', ' Internal ID -->' + i_internal_id);
            
        }
        
    }//Employee	
    return i_internal_id;
}

function calculate_OT_California(i_project_a ,i_item_a , i_start_of_week, i_end_of_week ,i_employee_arr,i_recordID, i_location, i_project, i_item, a_location_data_array, i_rate, i_hours_per_day, i_hours_per_week, i_days_for_month, i_OT_applicable, i_OT_rate_ST, i_week_days){
    var otrVar = '';
    var i_date = 0;
    var i_employee = 0;
    var i_project = 0;
    var i_item = 0;
    var i_rate = 0;
    var i_hours = 0;
    var a_split_array = new Array();
    var i_day_hours;
    var i_day_minutes;
    var a_split_day_array = new Array();
	var flag = 0
    
    nlapiLogExecution('DEBUG', 'calculate_OT_California', ' a_location_data_array-->' + a_location_data_array);
    nlapiLogExecution('DEBUG', 'calculate_OT_California', ' i_hours_per_week-->' + i_hours_per_week);
    nlapiLogExecution('DEBUG', 'calculate_OT_California', ' i_OT_rate_ST-->' + i_OT_rate_ST);
    nlapiLogExecution('DEBUG', 'calculate_OT_California', ' i_week_days-->' + i_week_days);
    
    var i_per_day_hours = parseInt(i_hours_per_week) / parseInt(i_week_days);
    i_per_day_hours = parseFloat(i_per_day_hours)
    nlapiLogExecution('DEBUG', 'calculate_OT_California', ' i_per_day_hours-->' + i_per_day_hours);
    
    for (var lt = 0; lt < a_location_data_array.length; lt++) 
	{
        a_split_array = a_location_data_array[lt].split('$$$$')
        
        i_date = a_split_array[0]
        i_employee = a_split_array[1]
        i_project = a_split_array[2]
        i_item = a_split_array[3]
        i_rate = a_split_array[4]
        i_hours = a_split_array[5]
        
        nlapiLogExecution('DEBUG', 'calculate_OT_California', ' i_date-->' + i_date);
        nlapiLogExecution('DEBUG', 'calculate_OT_California', ' i_employee-->' + i_employee);
        nlapiLogExecution('DEBUG', 'calculate_OT_California', ' i_project-->' + i_project);
        nlapiLogExecution('DEBUG', 'calculate_OT_California', ' i_item-->' + i_item);
        nlapiLogExecution('DEBUG', 'calculate_OT_California', ' i_rate-->' + i_rate);
        nlapiLogExecution('DEBUG', 'calculate_OT_California', ' i_hours-->' + i_hours);
		
		 var d_start_of_week_new = get_start_of_week(i_date)
         var d_end_of_week_new = get_end_of_week(i_date)
        
		 if ((i_employee_arr == i_employee) && (i_project_a == i_project) && (i_item_a == i_item))
		 {
		 	if ((d_start_of_week_new == i_start_of_week) && (d_end_of_week_new == i_end_of_week)) 
			{
				
			if (_logValidation(i_hours)) 
			{
            if (i_hours.indexOf(':') > -1) 
			{
                a_split_time_hrs_array = i_hours.split(':')
                
                i_total_hours = a_split_time_hrs_array[0]
                
                i_total_minutes = a_split_time_hrs_array[1]
                
            }//Contains :
            else {
                i_total_hours = i_hours
                
                i_total_minutes = 0
            }
            
        }//Hours Per Week	
        /*
         if(_logValidation(parseFloat(i_per_day_hours)))
         {
         if(parseFloat(i_per_day_hours).indexOf(':')>-1)
         {
         a_split_day_array = parseFloat(i_per_day_hours).split(':')
         
         i_day_hours = a_split_day_array[0]
         
         i_day_minutes = a_split_day_array[1]
         
         }//Contains :
         else
         {
         i_day_hours = i_per_day_hours
         
         i_day_minutes = 0
         }
         
         }//Hours Per Week
         */
        i_day_hours = i_per_day_hours
        
        i_day_minutes = 0
        
        nlapiLogExecution('DEBUG', 'calculate_OT_California', ' Day Hours -->' + i_day_hours);
        nlapiLogExecution('DEBUG', 'calculate_OT_California', ' Day Minutes -->' + i_day_minutes);
        nlapiLogExecution('DEBUG', 'calculate_OT_California', ' Total Hours -->' + i_total_hours);
        nlapiLogExecution('DEBUG', 'calculate_OT_California', ' Total Minutes -->' + i_total_minutes);
                
        var i_extra_hours_time = parseInt(i_total_hours) - parseInt(i_day_hours)
        
        var i_extra_minutes_time = parseInt(i_total_minutes) - parseInt(i_day_minutes)
        
        nlapiLogExecution('DEBUG', 'calculate_OT_California', ' Extra Hours -->' + i_extra_hours_time);
        nlapiLogExecution('DEBUG', 'calculate_OT_California', ' Extra Minutes -->' + i_extra_minutes_time);
        
        if (((((parseInt(i_extra_hours_time) * parseInt(-1)) <= -1) && ((parseInt(i_extra_hours_time)>0))) || (((parseInt(i_extra_minutes_time) * parseInt(-1)) <= -1) && parseInt(i_extra_minutes_time)>0))) 
		{
           
            nlapiLogExecution('DEBUG', 'calculate_OT_California', ' Extra Minutes -->' + i_extra_minutes_time + ' is the OT Time .....');
            nlapiLogExecution('DEBUG', 'calculate_OT_California', ' Extra Hours -->' + i_extra_hours_time + ' is the OT Time .....');
            
            i_extra_time = parseInt(i_extra_hours_time) + '.' + (parseInt(i_extra_minutes_time))
            i_extra_time = parseFloat(i_extra_time)
            nlapiLogExecution('DEBUG', 'calculate_OT_California', ' i_extra_time -->' + i_extra_time);
              nlapiLogExecution('DEBUG', 'calculate_OT_California', '((((i_extra_time) * parseInt(-1)) <= -1) &&((i_extra_time)>0))  -->' + ((((i_extra_time) * parseInt(-1)) <= -1) &&((i_extra_time)>0)) );
               nlapiLogExecution('DEBUG', 'calculate_OT_California', '(i_extra_time) * parseInt(-1)) <= -1)  -->' +(i_extra_time) * parseInt(-1));
        
				if((((i_extra_time) * parseInt(-1))<=-1) &&((i_extra_time)>0)) 
				{
			   var i_total_rate = (parseFloat(i_OT_rate_ST) * parseFloat(i_rate)) / 100
               nlapiLogExecution('DEBUG', 'calculate_OT_California', ' i_total_rate -->' + i_total_rate);
                     
			i_OT_rate_time = parseFloat(i_rate)+parseFloat(i_total_rate)
            i_OT_rate_time = parseFloat(i_OT_rate_time).toFixed(2)
            nlapiLogExecution('DEBUG', 'calculate_OT_California', ' i_OT_rate_time -->' + i_OT_rate_time);
            
           
			otrVar += "			<tr>";
            otrVar += "				<td border-bottom=\"1\" border-left=\"1\" border-right=\"1\" font-family=\"Helvetica\" align=\"center\" font-size=\"8\">"+i_date+"<\/td>";
            otrVar += "				<td border-bottom=\"1\" border-right=\"1\" font-family=\"Helvetica\" align=\"center\" font-size=\"8\">" + i_rate + "<\/td>";
            otrVar += "				<td border-bottom=\"1\" border-right=\"1\" font-family=\"Helvetica\" align=\"center\" font-size=\"8\">" + i_per_day_hours + "<\/td>";
			otrVar += "				<td border-bottom=\"1\" border-right=\"1\" font-family=\"Helvetica\" align=\"center\" font-size=\"8\">" + (parseFloat(i_rate) * parseFloat(i_per_day_hours)).toFixed(2) + "<\/td>";
            otrVar += "				<td border-bottom=\"1\" border-right=\"1\" font-family=\"Helvetica\" align=\"center\" font-size=\"8\">"+parseFloat(i_OT_rate_ST)+"<\/td>";
            otrVar += "				<td border-bottom=\"1\" border-right=\"1\" font-family=\"Helvetica\" align=\"center\" font-size=\"8\">"+i_OT_rate_time+"<\/td>";
            otrVar += "				<td border-bottom=\"1\" border-right=\"1\" font-family=\"Helvetica\" align=\"center\" font-size=\"8\">" + (parseFloat(i_OT_rate_ST) * parseFloat(i_OT_rate_time)).toFixed(2) + "<\/td>";
            otrVar += "			<\/tr>";
					
           	}
		}
		else
		{		
			otrVar += "			<tr>";
            otrVar += "				<td border-bottom=\"1\" border-left=\"1\" border-right=\"1\" font-family=\"Helvetica\" align=\"center\" font-size=\"8\">"+i_date+"<\/td>";
            otrVar += "				<td border-bottom=\"1\" border-right=\"1\" font-family=\"Helvetica\" align=\"center\" font-size=\"8\">" + i_rate + "<\/td>";
            otrVar += "				<td border-bottom=\"1\" border-right=\"1\" font-family=\"Helvetica\" align=\"center\" font-size=\"8\">" + i_per_day_hours + "<\/td>";
			otrVar += "				<td border-bottom=\"1\" border-right=\"1\" font-family=\"Helvetica\" align=\"center\" font-size=\"8\">" + (parseFloat(i_rate) * parseFloat(i_per_day_hours)).toFixed(2) + "<\/td>";
            otrVar += "				<td border-bottom=\"1\" border-right=\"1\" font-family=\"Helvetica\" align=\"center\" font-size=\"8\"><\/td>";
            otrVar += "				<td border-bottom=\"1\" border-right=\"1\" font-family=\"Helvetica\" align=\"center\" font-size=\"8\"><\/td>";
    		otrVar += "				<td border-bottom=\"1\" border-right=\"1\" font-family=\"Helvetica\" align=\"center\" font-size=\"8\">" +parseFloat(0)+ "<\/td>";
            otrVar += "			<\/tr>";
			
		 }
				
		 	}
		 }
		
	}//Loop
    return otrVar;
}//California
function calculate_OT_other_than_California(i_end_of_week_arr,i_rate,i_date,i_employee,i_recordID,i_location,i_project,i_item,i_total_time,i_hours_per_day, i_hours_per_week, i_days_for_month, i_OT_applicable, i_OT_rate_ST, i_week_days){
    var ctrVar = ''
    nlapiLogExecution('DEBUG', 'calculate_OT_other_than_California', ' i_total_time-->' + i_total_time);
    nlapiLogExecution('DEBUG', 'calculate_OT_other_than_California', ' i_hours_per_week-->' + i_hours_per_week);
    nlapiLogExecution('DEBUG', 'calculate_OT_other_than_California', ' i_OT_rate_ST-->' + i_OT_rate_ST);
	 nlapiLogExecution('DEBUG', 'calculate_OT_other_than_California', ' i_rate-->' + i_rate);
    
    var i_week_hours;
    var i_week_minutes;
    var i_total_hours;
    var i_total_minutes;
    var i_extra_time = 0;
    var i_OT_rate_time = 0;
    
    var a_split_total_time_array = new Array();
    
    if (_logValidation(i_total_time)) {
        a_split_array = i_total_time.split(':')
        
        i_total_hours = a_split_array[0]
        
        i_total_minutes = a_split_array[1]
        
        nlapiLogExecution('DEBUG', 'calculate_OT_other_than_California', ' Total Hours -->' + i_total_hours);
        nlapiLogExecution('DEBUG', 'calculate_OT_other_than_California', ' Total Minutes -->' + i_total_minutes);
        
    }//Total Time
    var a_split_week_array = new Array();
    
    if (_logValidation(i_hours_per_week)) {
        if (i_hours_per_week.indexOf(':') > -1) {
            a_split_week_array = i_hours_per_week.split(':')
            
            i_week_hours = a_split_week_array[0]
            
            i_week_minutes = a_split_week_array[1]
            
        }//Contains :
        else {
            i_week_hours = i_hours_per_week
            
            i_week_minutes = 0
        }
        
    }//Hours Per Week
    nlapiLogExecution('DEBUG', 'calculate_OT_other_than_California', ' Week Hours -->' + i_week_hours);
    nlapiLogExecution('DEBUG', 'calculate_OT_other_than_California', ' Week Minutes -->' + i_week_minutes);
    
    var i_extra_hours_time = parseInt(i_total_hours) - parseInt(i_week_hours)
    
    var i_extra_minutes_time = parseInt(i_total_minutes) - parseInt(i_week_minutes)
    
    nlapiLogExecution('DEBUG', 'calculate_OT_other_than_California', ' Extra Hours -->' + i_extra_hours_time);
    nlapiLogExecution('DEBUG', 'calculate_OT_other_than_California', ' Extra Minutes -->' + i_extra_minutes_time);
    
    if ((((parseInt(i_extra_hours_time) * parseInt(-1)) <= -1) && parseInt(i_extra_hours_time)>0) || ((parseInt(i_extra_minutes_time) * parseInt(-1)) <= -1 && parseInt(i_extra_minutes_time)>0)) 
	{
        nlapiLogExecution('DEBUG', 'calculate_OT_other_than_California', ' Extra Minutes -->' + i_extra_minutes_time + ' is the OT Time .....');
        nlapiLogExecution('DEBUG', 'calculate_OT_other_than_California', ' Extra Hours -->' + i_extra_hours_time + ' is the OT Time .....');
        
        i_extra_time = parseInt(i_extra_hours_time) + '.' + (parseInt(i_extra_minutes_time))
        i_extra_time = parseFloat(i_extra_time)
        nlapiLogExecution('DEBUG', 'calculate_OT_other_than_California', ' i_extra_time -->' + i_extra_time);
        nlapiLogExecution('DEBUG', 'calculate_OT_other_than_California', ' ((((i_extra_time) * parseInt(-1)) <= -1) &&((i_extra_time)>0))  -->' + ((((i_extra_time) * parseInt(-1)) <= -1) &&((i_extra_time)>0)) );
        
		
		if((((i_extra_time) * parseInt(-1)) <= -1) &&((i_extra_time)>0)) 
		{		
        var i_total_rate = (parseFloat(i_OT_rate_ST) * parseFloat(i_rate)) / 100
        nlapiLogExecution('DEBUG', 'calculate_OT_other_than_California', ' i_total_rate -->' + i_total_rate);
        
        i_OT_rate_time = parseFloat(i_total_rate) * parseFloat(i_total_rate)
		i_OT_rate_time = parseFloat(i_rate)+parseFloat(i_OT_rate_time)
		
        i_OT_rate_time = parseFloat(i_OT_rate_time).toFixed(2)
        nlapiLogExecution('DEBUG', 'calculate_OT_other_than_California', ' i_OT_rate_time -->' + i_OT_rate_time);
        
        ctrVar += "			<tr>";
        ctrVar += "				<td border-bottom=\"1\" border-left=\"1\" border-right=\"1\" font-family=\"Helvetica\" align=\"center\" font-size=\"8\">"+i_end_of_week_arr+"<\/td>";
        ctrVar += "				<td border-bottom=\"1\" border-right=\"1\" font-family=\"Helvetica\" align=\"center\" font-size=\"8\">"+i_rate+"<\/td>";
        ctrVar += "				<td border-bottom=\"1\" border-right=\"1\" font-family=\"Helvetica\" align=\"center\" font-size=\"8\">"+i_hours_per_week+"<\/td>";
		ctrVar += "				<td border-bottom=\"1\" border-right=\"1\" font-family=\"Helvetica\" align=\"center\" font-size=\"8\">" + (parseFloat(i_rate) * parseFloat(i_hours_per_week)).toFixed(2) + "<\/td>";
        ctrVar += "				<td border-bottom=\"1\" border-right=\"1\" font-family=\"Helvetica\" align=\"center\" font-size=\"8\">" + parseFloat(i_OT_rate_ST) + "<\/td>";
        ctrVar += "				<td border-bottom=\"1\" border-right=\"1\" font-family=\"Helvetica\" align=\"center\" font-size=\"8\">" + i_OT_rate_time + "<\/td>";
        ctrVar += "				<td border-bottom=\"1\" border-right=\"1\" font-family=\"Helvetica\" align=\"center\" font-size=\"8\">" + (parseFloat(i_OT_rate_ST) * parseFloat(i_OT_rate_time)).toFixed(2) + "<\/td>";
        ctrVar += "			<\/tr>";
        
    }
    else 
	{
        ctrVar += "			<tr>";
        ctrVar += "				<td border-bottom=\"1\" border-left=\"1\" border-right=\"1\" font-family=\"Helvetica\" align=\"center\" font-size=\"8\">"+i_end_of_week_arr+"<\/td>";
        ctrVar += "				<td border-bottom=\"1\" border-right=\"1\" font-family=\"Helvetica\" align=\"center\" font-size=\"8\">"+i_rate+"<\/td>";
        ctrVar += "				<td border-bottom=\"1\" border-right=\"1\" font-family=\"Helvetica\" align=\"center\" font-size=\"8\">"+i_hours_per_week+"<\/td>";
		ctrVar += "				<td border-bottom=\"1\" border-right=\"1\" font-family=\"Helvetica\" align=\"center\" font-size=\"8\">" + (parseFloat(i_rate) * parseFloat(i_hours_per_week)).toFixed(2) + "<\/td>";
        ctrVar += "				<td border-bottom=\"1\" border-right=\"1\" font-family=\"Helvetica\" align=\"center\" font-size=\"8\"><\/td>";
        ctrVar += "				<td border-bottom=\"1\" border-right=\"1\" font-family=\"Helvetica\" align=\"center\" font-size=\"8\"><\/td>";
        ctrVar += "				<td border-bottom=\"1\" border-right=\"1\" font-family=\"Helvetica\" align=\"center\" font-size=\"8\">" + 0 + "<\/td>";
        ctrVar += "			<\/tr>";
        
    }
			
			
		}
	
    return ctrVar;
    
}//Other than California


function get_address(i_subsidiary)
{
   var i_address;	
   if (_logValidation(i_subsidiary))
   {
     var o_subsidiaryOBJ = nlapiLoadRecord('subsidiary',i_subsidiary)	
	
	 if (_logValidation(o_subsidiaryOBJ))
	 {
	 	i_address = o_subsidiaryOBJ.getFieldValue('addrtext')
		
	 }
   }	
   
  return i_address;	
}


function formatAndReplaceSpacesofMessage(messgaeToBeSendPara)
{
 if(messgaeToBeSendPara!=null && messgaeToBeSendPara!=''&& messgaeToBeSendPara!=undefined)
 {
   messgaeToBeSendPara = messgaeToBeSendPara.toString();
   nlapiLogExecution('DEBUG','In formatAndReplaceSpacesofMessage', "  messgaeToBeSend.toString() =====" + messgaeToBeSendPara);
  
   messgaeToBeSendPara = messgaeToBeSendPara.replace(/\n/g,"<br/>");/// /g
   nlapiLogExecution('DEBUG','In formatAndReplaceSpacesofMessage', "  messgaeToBeSend After Replcing spaces with % =====" + messgaeToBeSendPara);

  return messgaeToBeSendPara;
 }

}

function add_OT_line_item(i_recordID,i_total_amount_INV)
{
  var i_ST_amount = 0;
  var i_OT_amount = 0;
  var i_total_amount = 0;
  	
   var filter = new Array();
   filter[0] = new nlobjSearchFilter('custrecord_ot_invoice', null, 'is',i_recordID);
   
   var columns = new Array();
   columns[0] = new nlobjSearchColumn('custrecord_st_amount')
   columns[1] = new nlobjSearchColumn('custrecord_ot_amount')
   columns[2] = new nlobjSearchColumn('internalid')
   
    var a_search_results = nlapiSearchRecord('customrecord_ot_rate',null,filter,columns);
	
	if (a_search_results != null && a_search_results != '' && a_search_results != undefined)
	{
		for(var i=0 ;i<a_search_results.length;i++)
		{
		 i_ST_amount = a_search_results[i].getValue('custrecord_st_amount');
		 nlapiLogExecution('DEBUG', 'add_OT_line_item', ' ST Amount -->' + i_ST_amount);	
		
		 i_OT_amount = a_search_results[i].getValue('custrecord_ot_amount');
		 nlapiLogExecution('DEBUG', 'add_OT_line_item', ' OT Amount -->' + i_OT_amount);	
		
		  i_total_amount = parseFloat(i_total_amount)+parseFloat(i_ST_amount)+parseFloat(i_OT_amount)		
			
		}//Loop		
	}//Search Results
	
	nlapiLogExecution('DEBUG', 'add_OT_line_item', ' Total Amount -->' + i_total_amount);
return i_total_amount;
	
}//Add OT Line Item


function get_customer_address(i_customer)
{
  var i_address;	
 if(_logValidation(i_customer))
 {
 	var o_customerOBJ = nlapiLoadRecord('customer',i_customer)
	
	 if(_logValidation(o_customerOBJ))
	 {
	 	i_address = o_customerOBJ.getFieldValue('defaultaddress')
		nlapiLogExecution('DEBUG', 'get_customer_address', ' i_address -->' + i_address);
	 }
	
 }
	
  return i_address;	
}

// END OBJECT CALLED/INVOKING FUNCTION =====================================================
