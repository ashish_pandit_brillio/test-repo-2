/**
 * Module Description
 * 
 * Version Date Author Remarks 1.00 Apr 09 2015 Nitish Mishra
 * 
 */

function displayErrorForm(err, title) {

	nlapiLogExecution('ERROR', 'displayErrorForm', err);
        var pageTitle = title ? title : "Some Error Occurred";
	var errorForm = nlapiCreateForm(pageTitle , false);
	errorForm.addField('custpage_err_field', 'inlinehtml', '').setDefaultValue(
			'Some Error Occurred : ' + err);
	response.writePage(errorForm);
}