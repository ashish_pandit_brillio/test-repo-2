/**
 * Screen to perform project task related operations for the employee
 * 
 * Version Date Author Remarks 1.00 17 Feb 2016 Nitish Mishra
 * 
 */

// PROGRESS, NOTSTART
// main function
function suitelet(request, response) {
	try {
		var employeeId = nlapiGetUser();
      if(employeeId==97260){
        employeeId = 1816 ;
      }
		var projectTaskId = request.getParameter('taskid');
		var screenMode = request.getParameter('edit');
		var mode = request.getParameter('mode');

		if (request.getMethod() == "GET") {

			if (mode == "NEWTASK") {
				createNewTaskScreen(employeeId);
			} else if (projectTaskId && screenMode == 'T') {
				createEditTaskScreen(projectTaskId, employeeId);
			} else {
				createListPage(employeeId);
			}
		} else {

			if (request.getParameter('custpage_mode') == "NEWTASK") {
				submitNewTaskScreen(request, employeeId);
			} else if (request.getParameter('custpage_mode') == "EDITTASK") {
				submitEditTaskScreen(request, employeeId);
			}
		}
	} catch (err) {
		nlapiLogExecution('ERROR', 'SUITELET', err);
	}
}

// Create the list page for all the project task for the logged employee
function createListPage(employeeId) {
	try {
		var form = nlapiCreateForm('Project Task');

		// get all my active allocated projects list
		var projectList = getMyMoveActiveAllocatedProjects(employeeId);
		nlapiLogExecution('debug', 'projects count', projectList.length);
		nlapiLogExecution('debug', 'project', JSON.stringify(projectList));

		// loop through the project list and add a sublist for each
		for (var i = 0; i < projectList.length; i++) {
			var subList = form.addSubList('custpage_' + projectList[i].ID,
			        'staticlist', projectList[i].Name);
			subList.addField('edit', 'text', 'Edit').setDisplayType('inline');
			subList.addField('jiraid', 'text', 'JIRA ID');
			subList.addField('title', 'text', 'Title');

			// get all the project tasks assigned to the logged user
			var projectTaskList = getMyProjectTaskList(projectList[i].ID,
			        employeeId);

			nlapiLogExecution('debug', 'project task count',
			        projectTaskList.length);
			nlapiLogExecution('debug', 'project task', JSON
			        .stringify(projectTaskList));

			// set them in the sublist
			subList.setLineItemValues(projectTaskList);
		}

		form.addButton('custpage_new_task', 'New Task', 'AddNewProjectTask()');
		form.setScript('customscript_cs_sut_move_task_list');
		response.writePage(form);
	} catch (err) {
		nlapiLogExecution('ERROR', 'createListPage', err);
		throw err;
	}
}

// get all active allocated project for the logged employee
function getMyMoveActiveAllocatedProjects(employeeId) {
	try {

		var allocationSearch = nlapiSearchRecord('resourceallocation', null, [
		        new nlobjSearchFilter('customer', null, 'anyof', '8381'), // Move
		        new nlobjSearchFilter('enddate', null, 'notbefore', 'today'),
				new nlobjSearchFilter('startdate', null, 'notafter', 'today'),
		        new nlobjSearchFilter('resource', null, 'anyof', employeeId) ],
		        [ new nlobjSearchColumn('company') ]);

		var projectList = [];

		if (allocationSearch) {

			allocationSearch.forEach(function(allocation) {
				projectList.push({
				    ID : allocation.getValue('company'),
				    Name : allocation.getText('company'),
				});
			});
		}

		return projectList;
	} catch (err) {
		nlapiLogExecution('ERROR', 'getMyMoveActiveAllocatedProjects', err);
		throw err;
	}
}

// get all active projects for the logged employee in the active project
// allocation
function getMyProjectTaskList(projectId, employeeId) {
	try {

		var projectTaskSearch = searchRecord('projecttask', null, [
		        new nlobjSearchFilter('company', null, 'anyof', projectId),
		        new nlobjSearchFilter('ismilestone', null, 'is', 'F'),
		        new nlobjSearchFilter('title', null, 'isnot',
		                'Project Activities'),
		        new nlobjSearchFilter('nonbillabletask', null, 'is', 'F'),
		        new nlobjSearchFilter('status', null, 'noneof', 'COMPLETE') ],
		        [ new nlobjSearchColumn('title'),
		                new nlobjSearchColumn('custevent_move_task_title') ]);

		var projectTaskList = [];

		var context = nlapiGetContext();
		/*var url = nlapiResolveURL('SUITELET', context.getScriptId(), context
		        .getDeploymentId());*/
		var url = 'https://3883006.app.netsuite.com/app/site/hosting/scriptlet.nl?script=785&deploy=1'		

		if (projectTaskSearch) {

			projectTaskSearch.forEach(function(task) {
				projectTaskList.push({
				    id : task.getId(),
				    jiraid : task.getValue('title'),
				    title : task.getValue('custevent_move_task_title'),
				    edit : "<a href='" + url + "&taskid=" + task.getId()
				            + "&edit=T&unlayered=F'>edit</a>"
				});
			});
		}

		return projectTaskList;
	} catch (err) {
		nlapiLogExecution('ERROR', 'getMyProjectTaskList', err);
		throw err;
	}
}

// create screen for editing a project task
function createEditTaskScreen(taskId, employeeId) {
	try {
		var taskRecord = nlapiLoadRecord('projecttask', taskId);

		var form = nlapiCreateForm('Project Task - '
		        + taskRecord.getFieldValue('title'));
		form.addField('custpage_mode', 'text', 'Mode').setDisplayType('hidden')
		        .setDefaultValue('EDITTASK');
		form.addField('custpage_task_id', 'text', 'Task Id').setDisplayType(
		        'hidden').setDefaultValue(taskId);

		var isTaskCompleted = taskRecord.getFieldValue('status') == 'COMPLETE';

		form.addField('custpage_project', 'select', 'Project', 'job')
		        .setDisplayType('inline').setDefaultValue(
		                taskRecord.getFieldValue('company'));

		// Title
		var titleField = form.addField('custpage_title', 'text', 'Title');
		titleField.setDefaultValue(taskRecord
		        .getFieldValue('custevent_move_task_title'));
		titleField.setMandatory(true);
		if (isTaskCompleted) {
			titleField.setDisplayType('inline');
		}

		// JIRA ID
		var jiraIdField = form.addField('custpage_task_jira_id', 'text',
		        'Jira Id');
		jiraIdField.setDefaultValue(taskRecord.getFieldValue('title'));
		jiraIdField.setMandatory(true);
		if (isTaskCompleted) {
			jiraIdField.setDisplayType('inline');
		}

		var isCompletedField = form.addField('custpage_is_completed',
		        'checkbox', 'Is Completed ?');
		isCompletedField.setDefaultValue(isTaskCompleted ? 'T' : 'F');
		if (isTaskCompleted) {
			isCompletedField.setDisplayType('inline');
		}

		// add the assignee list
		// var listType = isTaskCompleted ? 'staticlist' : 'inlineeditor';
		var assigneeSublist = form.addSubList('custpage_assignee_list',
		        'staticlist', 'Assignees');

		var assigneeField = assigneeSublist.addField('employee', 'select',
		        'Employee', 'employee');
		assigneeField.setDisplayType('inline');
		// getAllocatedEmployeeList(taskRecord.getFieldValue('company')).forEach(
		// function(allocation) {
		// assigneeField.addSelectOption(allocation.value,
		// allocation.name);
		// });
		// assigneeField.addSelectOption('', 'Select an employee', true);
		// assigneeField.setMandatory(true);

		// if (isTaskCompleted) {
		// assigneeField.setDisplayType('disabled');
		// }

		// create a list of all the assignee
		var assigneeList = [];
		for (var line = 1; line <= taskRecord.getLineItemCount('assignee'); line++) {
			assigneeList.push({
				'employee' : taskRecord.getLineItemValue('assignee',
				        'resource', line)
			});
		}

		// set the assignee list to the form sublist
		assigneeSublist.setLineItemValues(assigneeList);
		form.addSubmitButton('Submit');

		response.writePage(form);
	} catch (err) {
		nlapiLogExecution('ERROR', 'createEditTaskScreen', err);
		throw err;
	}
}

// submit edit project task
function submitEditTaskScreen(request, employeeId) {
	try {
		var projectId = request.getParameter('custpage_project');
		var taskTitle = request.getParameter('custpage_title');
		var taskJiraId = request.getParameter('custpage_task_jira_id');
		var taskId = request.getParameter('custpage_task_id');

		if (projectId && taskId && taskTitle && taskJiraId) {

			// validate project allocation
			if (validateEmployeeAllocation(employeeId, projectId)) {
				var projectTaskRec = nlapiLoadRecord('projecttask', taskId);
				projectTaskRec.setFieldValue('title', taskJiraId);
				projectTaskRec.setFieldValue('custevent_move_task_title',
				        taskTitle);
				projectTaskRec.setFieldValue('custevent_move_task_title',
				        taskTitle);

				var isCompleted = request.getParameter('custpage_is_completed');
				if (isCompleted == 'T') {
					projectTaskRec.setFieldValue('status', 'COMPLETE');
				}

				/*
				 * // get the unit cost of all the resources (new / old) var
				 * oldAssigneeCount = projectTaskRec
				 * .getLineItemCount('assignee'); var resourceList = []; for
				 * (var i = 1; i <= oldAssigneeCount; i++) {
				 * resourceList.push(projectTaskRec.getLineItemValue(
				 * 'assignee', 'resource', i)); }
				 * 
				 * var newAssigneeCount = request
				 * .getLineItemCount('custpage_assignee_list'); for (var i = 1;
				 * i <= newAssigneeCount; i++) {
				 * resourceList.push(request.getLineItemValue(
				 * 'custpage_assignee_list', 'employee', i)); } // search on
				 * allocation to get bill rate var costSearch =
				 * nlapiSearchRecord('resourceallocation', null, [ new
				 * nlobjSearchFilter('resource', null, 'anyof', resourceList),
				 * new nlobjSearchFilter('project', null, 'anyof', projectId),
				 * new nlobjSearchFilter('enddate', null, 'notbefore', 'today'),
				 * new nlobjSearchFilter('startdate', null, 'notafter', 'today') ], [
				 * new nlobjSearchColumn('custevent3') ]); // removal from the
				 * assignee list for (var i = oldAssigneeCount; i >= 1; i--) {
				 * var allocatedEmployee = projectTaskRec.getLineItemValue(
				 * 'assignee', 'resource', i);
				 * 
				 * var employeeFound = false; // check if the employee is
				 * present in the list for (var j = 1; j <= newAssigneeCount;
				 * j++) { var newEmployee = request.getLineItemValue(
				 * 'custpage_assignee_list', 'employee', j);
				 * 
				 * if (allocatedEmployee == newEmployee) { employeeFound = true;
				 * break; } } // if employee not found, add him to the list if
				 * (!employeeFound) { //
				 * projectTaskRec.removeLineItem('assignee', i); } }
				 * 
				 * nlapiLogExecution('debug', 'resource removal done'); //
				 * addition to assignee list for (var i = 1; i <=
				 * newAssigneeCount; i++) { var newEmployee =
				 * request.getLineItemValue( 'custpage_assignee_list',
				 * 'employee', i); var employeeFound = false; // check if the
				 * employee is present in the list for (var j = 1; j <=
				 * oldAssigneeCount; j++) { var allocatedEmployee =
				 * projectTaskRec .getLineItemValue('assignee', 'resource', j);
				 * 
				 * if (allocatedEmployee == newEmployee) { employeeFound = true;
				 * break; } } // if employee not found, add him to the list if
				 * (!employeeFound) {
				 * projectTaskRec.selectNewLineItem('assignee');
				 * projectTaskRec.setCurrentLineItemValue('assignee',
				 * 'resource', newEmployee);
				 * projectTaskRec.setCurrentLineItemValue('assignee', 'units',
				 * '100.0%'); projectTaskRec.setCurrentLineItemValue('assignee',
				 * 'estimatedwork', 100);
				 * projectTaskRec.setCurrentLineItemValue('assignee',
				 * 'serviceitem', '2222'); // get employee unit cost from the
				 * allocation search // done var unitPrice = 0.0;
				 * 
				 * if (costSearch) { for (var k = 0; k < costSearch.length; k++) {
				 * unitPrice = costSearch[k] .getValue('custevent3'); } }
				 * projectTaskRec.setCurrentLineItemValue('assignee',
				 * 'unitcost', 0);
				 * projectTaskRec.setCurrentLineItemValue('assignee',
				 * 'unitprice', unitPrice);
				 * projectTaskRec.commitLineItem('assignee'); } }
				 * 
				 * nlapiLogExecution('debug', 'resource addition done');
				 */

				var taskId = nlapiSubmitRecord(projectTaskRec);
				nlapiLogExecution('AUDIT', 'Project Task submitted', taskId);

				// redirect to the task edit page
				var context = nlapiGetContext();
				response.sendRedirect('SUITELET', context.getScriptId(),
				        context.getDeploymentId(), null, {
					        unlayered : 'T'
				        });
			} else {
				throw "You are not authorised to access this project";
			}
		} else {
			throw "Some values missing";
		}
	} catch (err) {
		nlapiLogExecution('ERROR', 'submitEditTaskScreen', err);
		throw err;
	}
}

// validate resource allocation
// create screen for creating new project task
function createNewTaskScreen(employeeId) {
	try {
		var projectList = getMyMoveActiveAllocatedProjects(employeeId);
		nlapiLogExecution('ERROR', 'projectList', projectList);
		if (projectList.length > 0) {
			var form = nlapiCreateForm('New Project Task');
			form.addField('custpage_mode', 'text', 'Mode').setDisplayType(
			        'hidden').setDefaultValue('NEWTASK');

			var projectField = form.addField('custpage_project', 'select',
			        'Project');
			projectList.forEach(function(project) {
				projectField.addSelectOption(project.ID, project.Name);
			});
			projectField.addSelectOption('', "Select Any Project", true);
			projectField.setMandatory(true);

			var titleField = form.addField('custpage_title', 'text', 'Title');
			titleField.setMandatory(true);

			var jiraIdField = form.addField('custpage_task_jira_id', 'text',
			        'Jira Id');
			jiraIdField.setMandatory(true);

			// add the assignee list
			// var assigneeSublist = form.addSubList('custpage_assignee_list',
			// 'inlineeditor', 'Assignees');
			// assigneeSublist.addField('employee', 'select', 'Employee',
			// 'employee').setMandatory(true);

			form.addSubmitButton('Submit');
			response.writePage(form);
		} else {
			throw "No Active Allocations Found";
		}
	} catch (err) {
		nlapiLogExecution('ERROR', 'createNewTaskScreen', err);
		throw err;
	}
}

// submit new project task
function submitNewTaskScreen(request, employeeId) {
	try {
		var projectId = request.getParameter('custpage_project');
		var taskTitle = request.getParameter('custpage_title');
		var taskJiraId = request.getParameter('custpage_task_jira_id');

		if (projectId && taskTitle && taskJiraId) {

			// validate project allocation
			if (validateEmployeeAllocation(employeeId, projectId)) {
				
				var project_task_name = taskJiraId;
				 
				 var filters = new Array();
				 filters[0] = new nlobjSearchFilter('title', null, 'is', project_task_name);
				 filters[1] = new nlobjSearchFilter('company', null, 'is', projectId);
				 
				 var columns = new Array();
				 columns[0] = new nlobjSearchColumn('company');
					
				 var results1 = nlapiSearchRecord('projecttask', null, filters, columns);
				 
				// nlapiLogExecution('Debug', 'results', results.length);
				 if(results1){
					 if(results1.length>0){
						 var err = "This Task already exists";
						 
                       var htmltext = '<html>'+err+'</html>';
                       var form = nlapiCreateForm('Notice');
		form.addField('custpage_html', 'inlinehtml', 'html',
        'job').setDefaultValue(htmltext);
		
		response.writePage(form);
                       throw err;
					 }
				 	}
				 
				
				var projectTaskRec = nlapiCreateRecord('projecttask');

				projectTaskRec.setFieldValue('company', projectId);
				projectTaskRec.setFieldValue('title', taskJiraId);
				projectTaskRec.setFieldValue('custevent_move_task_title',
				        taskTitle);
				projectTaskRec.setFieldValue('estimatedwork', 100);
				projectTaskRec.setFieldValue('ismilestone', 'F');

				// assignee all active allocated resource to this task
				// search on allocation to get bill rate
				/*var costSearch = nlapiSearchRecord('resourceallocation', null,
				        [
				                new nlobjSearchFilter('project', null, 'anyof',
				                        projectId),
				                
				                new nlobjSearchFilter('startdate', null,
				                        'notafter', 'today') ], [
				                new nlobjSearchColumn('custevent3'),
				                new nlobjSearchColumn('resource') ]);*/
				var costSearch = nlapiSearchRecord('resourceallocation', null,
		        [
		                new nlobjSearchFilter('project', null, 'anyof',
		                        projectId),
		                new nlobjSearchFilter('enddate', null,
		                        'notbefore', 'today'),
		                new nlobjSearchFilter('startdate', null,
		                        'notafter', 'today') ], [
		                new nlobjSearchColumn('custevent3'),
		                new nlobjSearchColumn('resource') ]);


				if (costSearch) {

					for (var i = 0; i < costSearch.length; i++) {
						projectTaskRec.selectNewLineItem('assignee');
						projectTaskRec.setCurrentLineItemValue('assignee',
						        'resource', costSearch[i].getValue('resource'));
						projectTaskRec.setCurrentLineItemValue('assignee',
						        'units', '100.0%');
						projectTaskRec.setCurrentLineItemValue('assignee',
						        'estimatedwork', 0.5);
						projectTaskRec.setCurrentLineItemValue('assignee',
						        'serviceitem', '2222');
						projectTaskRec.setCurrentLineItemValue('assignee',
						        'unitcost', 0);
						projectTaskRec.setCurrentLineItemValue('assignee',
						        'unitprice', costSearch[i]
						                .getValue('custevent3'));
						projectTaskRec.commitLineItem('assignee');
					}
				}

				var taskId = nlapiSubmitRecord(projectTaskRec);
				nlapiLogExecution('AUDIT', 'New Project Task Created', taskId);

				/*
				 * // addition to assignee list for (var i = 1; i <=
				 * newAssigneeCount; i++) { var newEmployee =
				 * request.getLineItemValue( 'custpage_assignee_list',
				 * 'employee', i); var employeeFound = false; // check if the
				 * employee is present in the list for (var j = 1; j <=
				 * oldAssigneeCount; j++) { var allocatedEmployee =
				 * projectTaskRec .getLineItemValue('assignee', 'resource', j);
				 * 
				 * if (allocatedEmployee == newEmployee) { employeeFound = true;
				 * break; } } // if employee not found, add him to the list if
				 * (!employeeFound) {
				 * projectTaskRec.selectNewLineItem('assignee');
				 * projectTaskRec.setCurrentLineItemValue('assignee',
				 * 'resource', newEmployee);
				 * projectTaskRec.setCurrentLineItemValue('assignee', 'units',
				 * '100.0%'); projectTaskRec.setCurrentLineItemValue('assignee',
				 * 'estimatedwork', 100);
				 * projectTaskRec.setCurrentLineItemValue('assignee',
				 * 'serviceitem', '2222'); // get employee unit cost from the
				 * allocation search // done var unitPrice = 0.0;
				 * 
				 * if (costSearch) { for (var k = 0; k < costSearch.length; k++) {
				 * unitPrice = costSearch[k] .getValue('custevent3'); } }
				 * projectTaskRec.setCurrentLineItemValue('assignee',
				 * 'unitcost', 0);
				 * projectTaskRec.setCurrentLineItemValue('assignee',
				 * 'unitprice', unitPrice);
				 * projectTaskRec.commitLineItem('assignee'); } }
				 */

				var context = nlapiGetContext();
				response.sendRedirect('SUITELET', context.getScriptId(),
				        context.getDeploymentId(), null, {
					        // taskid : taskId,
					        // edit : 'T',
					        unlayered : 'F'
				        });
			} else {
				throw "You are not authorised to access this project";
			}
		} else {
			throw "Some values missing";
		}
	} catch (err) {
		nlapiLogExecution('ERROR', 'submitNewTaskScreen', err);
		throw err;
	}
}

// check for active allocation
function validateEmployeeAllocation(employeeId, projectId) {
	try {
		var allocationSearch = nlapiSearchRecord('resourceallocation', null, [
		        new nlobjSearchFilter('customer', null, 'anyof', '8381'), // Move
		        new nlobjSearchFilter('enddate', null, 'notbefore', 'today'),
		        new nlobjSearchFilter('resource', null, 'anyof', employeeId),
		        new nlobjSearchFilter('project', null, 'anyof', projectId) ]);

		return allocationSearch ? true : false;
	} catch (err) {
		nlapiLogExecution('ERROR', 'validateEmployeeAllocation', err);
		throw err;
	}
}

// get the allocated employee list
function getAllocatedEmployeeList(projectId) {
	try {
		nlapiLogExecution('debug', 'projectId', projectId);
		var allocationSearch = nlapiSearchRecord('resourceallocation', null, [
		        new nlobjSearchFilter('enddate', null, 'notbefore', 'today'),
		        new nlobjSearchFilter('project', null, 'anyof', projectId) ],
		        [ new nlobjSearchColumn('resource') ]);

		var allocationList = [];

		if (allocationSearch) {

			allocationSearch.forEach(function(allocation) {
				allocationList.push({
				    value : allocation.getValue('resource'),
				    name : allocation.getText('resource')
				});
			});
		}

		return allocationList;
	} catch (err) {
		nlapiLogExecution('ERROR', 'getAllocatedEmployeeList', err);
		throw err;
	}
}