 /**
 * Module Description
 * User Event script to handle employee record creation and updation from the XML file generated from Fusion
 * 
 * Version                      Date                                                Author                           Remarks
 * 1.00                            5th November, 2015                Manikandan V          This script saves the form data into Survey key Response Master custom record from the Survey.html 
 *
 */


function feedbackSuitelet(request, response)
{
  var method	=request.getMethod();
  if (method == 'GET' )
  {
    var file = nlapiLoadFile(77600);   //load the HTML file
    var contents = file.getValue();    //get the contents
    response.write(contents);     //render it on  suitlet
  
  }
  else
  {
  //var form = nlapiCreateForm("Suitelet - POST call" );
  
  var feedback = new Object();
  feedback['Name'] = request.getParameter('TextBox1');
  feedback['Phone'] = request.getParameter('TextBox2');
  feedback['Email'] = request.getParameter('TextBox3');
  feedback['Contact_Time'] = request.getParameter('TextBox4');
  feedback['Time_Zone']	=	request.getParameter('TextBox11');
 
  feedback['Employee_Name'] = request.getParameter('TextBox5');
  feedback['Employee_Email'] = request.getParameter('TextBox6');
  
  

  feedback['Response1'] = request.getParameter('OptionsGroup1');
  feedback['Response2'] = request.getParameter('OptionsGroup2');
  feedback['Response3'] = request.getParameter('OptionsGroup3');
  feedback['Response4'] = request.getParameter('OptionsGroup4');
  feedback['Comments'] = request.getParameter('txtComments');
 
  var status = saveRequest(feedback);
   //response.write("Thank You!");
   //response.write("Your survey responses have been recorded!");
    var thanks_note = nlapiLoadFile(50389);   //load the HTML file
    var thanks_contents = thanks_note.getValue();    //get the contents
    response.write(thanks_contents);     //render it on  suitlet
   }

function saveRequest(feedback)
{
	try
	{
               var feedback_form = nlapiCreateRecord('customrecordfeedback_survey_for_business');
               feedback_form.setFieldValue('custrecord_name_ops', feedback.Name);
	           feedback_form.setFieldValue('custrecordphone_ops', feedback.Phone);
			   feedback_form.setFieldValue('custrecord_email_ops', feedback.Email);
               feedback_form.setFieldValue('custrecord_contact_time', feedback.Contact_Time);
			    feedback_form.setFieldValue('custrecord_time_zone', feedback.Time_Zone);
			   
			   
			   
			   feedback_form.setFieldValue('custrecord_employee_name', feedback.Employee_Name);
	           feedback_form.setFieldValue('custrecord_employee_email', feedback.Employee_Email);
			   
			   
			   


               feedback_form.setFieldValue('custrecord_query_1',"How would you rate the quality of resolution/clarification received for your query/concern?");
               feedback_form.setFieldValue('custrecord_query_2', "How satisfied are you with the response time in resolving your query?");
               feedback_form.setFieldValue('custrecord_query_3', "Would you say your point of contact was knowledgeable and able to get you answers to your questions?");
			   
			   feedback_form.setFieldValue('custrecord_query_4', "Were your questions answered within the timeframe you anticipated?");
			   
			   feedback_form.setFieldValue('custrecord_feed_back', "Feedback for Improvement");
             


               feedback_form.setFieldValue('custrecord_answer_1_ops', feedback.Response1);
               feedback_form.setFieldValue('custrecord_answer_2_ops', feedback.Response2);
               feedback_form.setFieldValue('custrecord_answer_3_ops', feedback.Response3);
			   feedback_form.setFieldValue('custrecord_answer_4_ops', feedback.Response4);
               
               feedback_form.setFieldValue('custrecord_comments', feedback.Comments);
			   feedback_form.setFieldValue('custrecord_survey_type',5);
               
     
              
               var id = nlapiSubmitRecord(feedback_form, false,true);
               nlapiLogExecution('debug', 'Record Saved', id);
        }
catch(err)
{
nlapiLogExecution('error', 'Record not saved', err);
feedback_form.setFieldValue('custrecord_error_log_ops','');
}
}
 }