var strVar= '';
//var s_tablestring = '';
var l_link_URL = '';
//var linenumber = '';
//var i_currentPageFirstLine;
function suiteletFunction_my_request(request, response){
	
	//try
	{
		if (request.getMethod() == 'GET'){
			
			var objUser = nlapiGetContext();
			var o_values  = new Object();
			var i_employee_id = objUser.getUser();
			var i_roleID = objUser.getRole();
			var s_role	=	getRoleName(i_employee_id, objUser.getRole());
			var o_employee_data	=	nlapiLookupField('employee', i_employee_id, ['firstname', 'lastname', 'employeestatus']);
			
			var s_status = request.getParameter('custscript_flag');  
			//nlapiLogExecution('DEBUG', 'suitelet_employeForm', 's_status = '+s_status);
			var linenum_new= request.getParameter('custscript_linenum');
			nlapiLogExecution('DEBUG', 'suitelet_employeForm', 'IN GET function****= '+linenum_new);
			
			var mode = request.getParameter('mode');
			var counter = request.getParameter('counter');
			
			var i_currentPageFirstLine = request.getParameter('custscript_curntpgcnt');
			nlapiLogExecution('DEBUG', 'suitelet_employeForm in GET FUNCTION', 'i_currentPageFirstLine  = '+i_currentPageFirstLine);
			var o_json = index_search(s_status,linenum_new,i_currentPageFirstLine);
			//var i_currentPageFirstLine = linenum_new;
			if(o_json)
				o_values['record_found'] = o_json.length-1;
			
			o_values['username']	=	o_employee_data.firstname + ' ' + o_employee_data.lastname;
			o_values['role']		=	s_role;
			
			
			if(mode == '' || mode == null){
				var finalvalue = parseInt(1) + parseInt(counter);
				o_values['counter'] = finalvalue;
				
			}
			if(mode == 'next'){
				var finalvalue = parseInt(1) + parseInt(counter);
				o_values['counter'] = finalvalue;
			}
			if(mode == 'prev' || counter =='' ||counter == null){
				var finalvalue = parseInt(1);
				o_values['counter'] = finalvalue;
			}
			if(mode == 'prev' && counter !='' && counter != null){
				var finalvalue = parseInt(1);
				o_values['counter'] = finalvalue;
			}
			//var counter = request.getParameter('counter');
			
			strVar += "<html>";
			strVar += "<body>";
			var prevURL = '';
			for(var key in o_json){
				
				 if(key <10)
				 {
					 if(_logValidation(o_json[key].sr_no)){
						 
						 //var find_prev_id = o_json[0].record_id;
						 strVar += '<tr  class="noExl">';
						 //var newid = o_json[key].record_id;
						 strVar += '<td>'+o_json[key].sr_no+'</td>';
						 strVar += '<td>'+o_json[key].vendor_name+'</td>';
						 strVar += '<td>'+o_json[key].federal_id+'</td>';
						 strVar += '<td>'+o_json[key].vendor_contracttype+'</td>';
						 strVar += '<td>'+o_json[key].vendor_emailaddress+'</td>';
						 strVar += '<td>'+o_json[key].vendor_contactperson+'</td>';
						 strVar += '<td>'+o_json[key].vendor_status+'</td>';
						 strVar += '<th style="display:none" id = "recordid">'+ o_json[key].record_id+'</th';
					 }
					
					
				 }       
				  var linenumber = o_json[key].linenum;// + ' ';
				 
				strVar+= '</tr>';
			 }
			 strVar += "<\/body>";
			 strVar += "<\/html>";
			
			 if(mode == '' || mode == null || finalvalue == 1)
			 {
				 nlapiLogExecution('debug', ' in if prevURL ++++++', prevURL);
				 o_values['btn_disable_prev'] = 'disabled';
				 o_values['s_href_prev'] = '#';
				}else{
					 nlapiLogExecution('debug', 'else prevURL ++++++', prevURL);
					 prevURL = "https://3883006.app.netsuite.com/app/site/hosting/scriptlet.nl?script=1188&deploy=1&mode=prev&custscript_flag=T&custscript_linenum="+linenumber+"&counter="+finalvalue+"&custscript_curntpgcnt="+linenum_new;
					 nlapiLogExecution('debug', 'linenumber in foor loop', linenumber);
					 o_values['s_href_prev'] = prevURL;
				//	o_values['s_href_prev'] = prevURL;// "https://system.sandbox.netsuite.com/app/site/hosting/scriptlet.nl?script=1158&deploy=1&mode=prev&custscript_flag=T&custscript_linenum="+linenumber+"&counter="+finalvalue+"&custscript_curntpgcnt="+linenumber;
				}

			 var s_new_request_url	=	nlapiResolveURL('SUITELET', 'customscript_sut_recruiters_screen', 'customdeploy1');
			 var s_my_requests_url	=	nlapiResolveURL('SUITELET', 'customscript_sut_my_request_form', 'customdeploy1');
			 var s_search_URL = nlapiResolveURL('SUITELET', 'customscript_sut_searchvendorinformation', 'customdeploy1');
			
			 if(o_json)
			 {
			 	if(o_json.length <=1 || o_json.length == null ){
					
					o_values['btn_disable_next'] = 'disabled';
					o_values['s_href_next'] = '#';
					
					o_values['btn_disable_prev'] = 'disabled';
					o_values['s_href_prev'] = '#';
				
				}
				 if(o_json.length < 10 ){
					 o_values['btn_disable_next'] = 'disabled';
					 o_values['s_href_next'] = '#';
					 
				 }
				 if(o_json.length >10 ){
					
					 o_values['s_href_next'] = "https://3883006.app.netsuite.com/app/site/hosting/scriptlet.nl?script=1188&deploy=1&mode=next&custscript_linenum="+linenumber+"&counter="+finalvalue+"&custscript_curntpgcnt="+i_currentPageFirstLine;
					
				 } 
			 }
			 
			 o_values['new_request_url']	=	s_new_request_url;
			 o_values['my_requests_url']	=	s_my_requests_url;
			 o_values['s_search_URL'] = s_search_URL;
			 o_values['s_table_content'] = strVar; 
			 //o_values['table_content'] = s_tablestring;
			 var file = nlapiLoadFile(2046234); 
			 var contents = file.getValue();    //get the contents
			 //nlapiLogExecution('debug', 's_tablestring', s_tablestring);
			 contents = replaceValues(contents, o_values);
			 // nlapiLogExecution('debug', 'contents', contents);
			 response.write(contents);
			
		}
		
	}
	//catch(e)
	{
		
	}
	
	
	
}
//code on the basis of index 
function index_search(s_status,linenum_new,i_currentPageFirstLine){
	
	
	
	try
	{
		//nlapiLogExecution('DEBUG', 'index_search', 's_status  = '+s_status);
		//nlapiLogExecution('DEBUG', 'index_search', 'linenum_new  = '+linenum_new);
			nlapiLogExecution('DEBUG', 'index_search', 'i_currentPageFirstLine  = '+i_currentPageFirstLine);
		//if(request.getMethod() == 'GET')
		{
			
			var linenum = 1;	
			if(!_logValidation(linenum_new)){
				var linenum_new= "";
				
			}
			//var i_currentPageFirstLine = linenum_new;
			//var i_tempLine = "";	
					
			//var filters =	new Array();
		 	var columns	=	new Array();
		 	var a_data	=	new Array();

			//linenum_new= request.getParameter('custscript_linenum');
			//nlapiLogExecution('DEBUG', 'suitelet_employeForm', 'linenum new = '+linenum_new);
			
			
			
			//if(_logValidation(o_subList))
			{
				if( i_currentPageFirstLine == '' || i_currentPageFirstLine == 'NaN' ){
					var  i_currentPageFirstLine = linenum;
				}
					
					if(_logValidation(linenum_new) && s_status != 'T') //  button click 
					{
						i_currentPageFirstLine = linenum_new;
						nlapiLogExecution('DEBUG', 'index_search', 'Inside***i_currentPageFirstLine = '+i_currentPageFirstLine);
					}
//***************************Employee logic starts**************************************
				
				
				columns[0]	=	new nlobjSearchColumn('internalid').setSort(true);
				columns[1]	=	new nlobjSearchColumn('custrecord_vendor_name');
				columns[2]	=	new nlobjSearchColumn('custrecord_federal_id');
				columns[3]	=	new nlobjSearchColumn('custrecord_email_for_administration'); // email for admin 
				columns[4]	=	new nlobjSearchColumn('custrecord_contract_type'); // contract type
				columns[5]	=	new nlobjSearchColumn('custrecord_contact_person'); // contact person 
				columns[6]	=	new nlobjSearchColumn('custrecord_approval_status_recruiters'); // status 
				
				//var o_search = nlapiSearchRecord('employee', 'customsearch2039', null, column);
			//	nlapiLogExecution('DEBUG', 'suitelet_employeForm', 'o_search = '+o_search);
				var o_search	=	searchRecord('customrecord_recruiters_details', null, null, columns);
				//nlapiLogExecution('debug', 'o_search', o_search.length);
				
				//va s_tablestring="";
				//var k=1;
				if(o_search)
				{
					var o_searchlen = parseInt(o_search.length);
					// functionality for NEXT button CLICK 
					if(s_status != 'T')
					{
						if((linenum_new) != '')
						{
							linenum = parseInt(linenum_new);
							nlapiLogExecution('DEBUG', 'index_search', 'linenum assigned = '+linenum);
						}
						nlapiLogExecution('DEBUG', 'index_search', 'linenum not assginged = '+linenum);
						//var temp1 = o_search.length - parseInt(linenum);
						var temp1 = parseInt(linenum) + parseInt(9);
						nlapiLogExecution('DEBUG', 'index_search', 'temp1 = '+temp1);
					//for(var i = 0; i < o_search.length; i++){
						
						for(var i = linenum ; i <= temp1; i++)
						{
							
							var searchRes = o_search[i-1];
							//nlapiLogExecution('DEBUG', 'suitelet_employeForm', 'searchRes= '+searchRes);
							var s_record_id	=	searchRes.getValue(columns[0]);
							var s_vendor_name	=	searchRes.getValue(columns[1]);
							var s_federal_id	=	searchRes.getValue(columns[2]);
							var s_emailaddress = searchRes.getValue(columns[3]);
							var s_contracttype = searchRes.getText(columns[4]);
							var s_contactperson =searchRes.getValue(columns[5]);
							var s_status = searchRes.getText(columns[6]);
							a_data.push({ 'record_id': s_record_id, 'vendor_name': s_vendor_name, 'federal_id': s_federal_id, 'sr_no': linenum,'vendor_contracttype':s_contracttype,'vendor_emailaddress':s_emailaddress,'vendor_contactperson':s_contactperson,'vendor_status':s_status});
							//k++;
							//linenumone++;
							linenum++;
							
							if(linenum == o_search.length + 1)
							{
								break;
							}
							
						}
							a_data.push({'linenum' : linenum});//['linenum'] =linenum;
							nlapiLogExecution('DEBUG', 'index_search', 'linenum = '+linenum);
							
						
					}
							
				}
				if(s_status == 'T') // previous button functionality 
				{
					
					linenum = parseInt(i_currentPageFirstLine)- parseInt(10);
					nlapiLogExecution('debug', 'linenum11', linenum);
				
					if(_logValidation(linenum_new))
					{
						//linenum = parseInt(linenum_new)-parseInt(10);
						//i_currentPageFirstLine = parseInt(linenum_new)- parseInt(10);
						//nlapiLogExecution('DEBUG', 'suitelet_employeForm', 'linenum assigned = '+linenum);
					}
					//i_currentPageFirstLine = linenum;
					for(var k = linenum ; k < i_currentPageFirstLine; k++)
					{
						var searchRes = o_search[k-1];
						//nlapiLogExecution('DEBUG', 'suitelet_employeForm', 'searchRes= '+searchRes);
						var s_record_id	=	searchRes.getValue(columns[0]);
						var s_vendor_name	=	searchRes.getValue(columns[1]);
						var s_federal_id	=	searchRes.getValue(columns[2]);
						var s_emailaddress = searchRes.getValue(columns[3]);
						var s_contracttype = searchRes.getText(columns[4]);
						var s_contactperson =searchRes.getValue(columns[5]);
						var s_status = searchRes.getText(columns[6]);
						a_data.push({ 'record_id': s_record_id, 'vendor_name': s_vendor_name, 'federal_id': s_federal_id, 'sr_no': linenum,'vendor_contracttype':s_contracttype,'vendor_emailaddress':s_emailaddress,'vendor_contactperson':s_contactperson,'vendor_status':s_status});
											
						//linenumone_one++;
						linenum++;
						
						if(linenum < 1)
						{
							break;
						}
					}
					nlapiLogExecution('DEBUG', 'index_search', 'linenum = '+linenum);
					a_data.push({'linenum' : linenum});//['linenum'] =linenum;
					//i_currentPageFirstLine = linenum_new;
				}
				//nlapiLogExecution('DEBUG', 'suitelet_employeForm', '******i_currentPageFirstLine after i_templine = '+i_currentPageFirstLine);
				//nlapiLogExecution('DEBUG', 'suitelet_employeForm', '******linenum = '+linenum);
			
				
				
			}
	
			return a_data;
				
		} 	
	}catch(exp)
	{
		nlapiLogExecution('DEBUG', 'index_search', 'exp = '+exp.message);
	}
}

// search code 
function searchRecord(recordType, savedSearch, arrFilters, arrColumns,  filterExpression){

	try {
		var search = null;

		// if a saved search is provided, load it and add the filters and
		// columns
		if (isNotEmpty(savedSearch)) {
			search = nlapiLoadSearch(recordType, savedSearch);

			if (isArrayNotEmpty(arrFilters)) {
				search.addFilters(arrFilters);
			}

			if (isArrayNotEmpty(arrColumns)) {
				search.addColumns(arrColumns);
			}

			if (isArrayNotEmpty(filterExpression)) {
				search.setFilterExpression(filterExpression);
			}
		}
		// create a new search
		else {
			search = nlapiCreateSearch(recordType, arrFilters, arrColumns);
		}

		// run search
		var resultSet = search.runSearch();

		// iterate through the search and get all data 1000 at a time
		var searchResultCount = 0;
		var resultSlice = null;
		var searchResult = [];

		do {
			resultSlice = resultSet.getResults(searchResultCount,
			        searchResultCount + 1000);

			if (resultSlice) {

				resultSlice.forEach(function(result) {

					searchResult.push(result);
					searchResultCount++;
				});
			}
		} while (isArrayNotEmpty(resultSlice) && resultSlice.length >= 1000);

		return searchResult;
	} catch (err) {
		nlapiLogExecution('ERROR', 'searchRecord', err);
		throw err;
	}
}

function isEmpty(value) {

	return value == null || value == "" || typeof (value) == undefined;
}

function isNotEmpty(value) {

	return !isEmpty(value);
}

function isArrayEmpty(argArray) {

	return !isArrayNotEmpty(argArray);
}

function isArrayNotEmpty(argArray) {

	return (isNotEmpty(argArray) && argArray.length > 0);
}

function _logValidation(value)  
{
	if(value != null && value != '' && value != undefined && value.toString() != 'NaN' && value != NaN)
	{
		return true;
	}
	else
	{
		return false;
	}
}

//get role name 
function getRoleName(i_user_id, i_role_id)
{
	var a_search_results	=	nlapiSearchRecord('employee', null, [new nlobjSearchFilter('internalid', null, 'anyof', i_user_id), new nlobjSearchFilter('role', null, 'anyof', i_role_id)], new nlobjSearchColumn('role'));
	
	if(a_search_results != null && a_search_results.length == 1)
		{
			return a_search_results[0].getText('role');
		}
	else
		{
			return '';
		}
}
// END SUITELET ====================================================

function replaceValues(content, oValues)
{
	for(param in oValues)
		{
			// Replace null values with blank
			var s_value	=	(oValues[param] == null)?'':oValues[param];
			
			// Replace content
			content = content.replace(new RegExp('{{' + param + '}}','gi'), s_value);
		}
	
	return content;
}