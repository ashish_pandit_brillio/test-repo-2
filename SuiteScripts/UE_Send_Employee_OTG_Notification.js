/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       14 Jun 2021     shravan.k
 *
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType
 * 
 * @param {String} type Operation types: create, edit, delete, xedit
 *                      approve, reject, cancel (SO, ER, Time Bill, PO & RMA only)
 *                      pack, ship (IF)
 *                      markcomplete (Call, Task)
 *                      reassign (Case)
 *                      editforecast (Opp, Estimate)
 * @returns {Void}
 */
function Send_Delete_Data(type)
{
	try
	{
		nlapiLogExecution('DEBUG', 'type==', type);
		if(type == 'delete')
			{
				var obj_Record_Employee = nlapiGetOldRecord();
				var i_Internal_Id = nlapiGetRecordId();
				nlapiLogExecution('DEBUG', 'i_Internal_Id==', i_Internal_Id);
				if(obj_Record_Employee)
					{
						var s_Email_address = obj_Record_Employee.getFieldValue('email');
						var i_Fusion_Id = obj_Record_Employee.getFieldValue('custentity_fusion_empid');
						var s_Name = obj_Record_Employee.getFieldValue('entityid');
						var obj_Json = {
								Email : s_Email_address,
								Fusion_ID : i_Fusion_Id,
								Name : s_Name
								
						}
						nlapiLogExecution('DEBUG', 'obj_Json==', JSON.stringify(obj_Json));
						 var commonHeader = {
			            		"Content-Type": "application/json",
			           		    "accept": "application/json"
			            };
			            //************************Token Generation steps*****************************//
					   var tokenURL = 'https://otgv2prod-fusion-and-performance-management-node.azurewebsites.net/api/otg-fusion-and-performance-management/leaves/get-access-and-refresh-tokens?userName=NSUser&password=NSOtgIntegrations'
					   var tokenResponse = nlapiRequestURL(tokenURL,null, commonHeader, null, 'GET');
			            nlapiLogExecution("DEBUG", "tokenResponse Body :", (tokenResponse.body)); //JSON.stringify
			            var tken = JSON.parse(tokenResponse.body);
			           // nlapiLogExecution("DEBUG", "tokenResponse token :", (tken.token));
						nlapiLogExecution("DEBUG", "tokenResponse accessToken :", (tken.data.accessToken));
			            //********************END***********************************//
			            var url = 'https://onthegov2prodnetsuitemanagement.azurewebsites.net/api/otg-netsuite-management/delete-employee-sync'
			            var method = "POST";
			            var headers = {
			                	"Authorization": "Bearer "+tken.data.accessToken,
			                    "x-api-key": "615679DD-ABEA-48CE-B916-28974A309F77",
			                    "x-api-customer-key": "98F78573-3172-4A8D-A521-3DC619AF2BE1",
			                    "x-api-token-key": "8D660F3B-7CA9-47D0-848A-E8D7BE1B2786",
			                    "x.api.user": "113056",
			                    "content-type": "application/json",
			                    "Accept": "application/json"

			                };
			            var JSONBody = JSON.stringify(obj_Json, replacer);
			            var responseObj = nlapiRequestURL(url, JSONBody, headers, null, method);
			            nlapiLogExecution("DEBUG", "Response Body :", (responseObj.body)); //JSON.stringify

			            if (!(parseInt(JSON.parse(responseObj.getCode())) == 200)) {
			            	var a_emp_attachment = new Array();
			        		a_emp_attachment['entity'] = 83134;
			                // send Email to fuel
			                // send Email to fuel
			                var file = nlapiCreateFile("payload.json", "PLAINTEXT", JSONBody);
			                var emailSub = "Error in Employee Deletion #" + nlapiGetRecordId(); //onthego
			                var emailBoody = "Team," + "\r\n" + "Error while deleting the Employee details into OTG server  #" + nlapiGetRecordId() + "\r\n";
			                nlapiSendEmail(442, "onthego@BRILLIO.COM", emailSub, emailBoody, 'information.systems@brillio.com', null, a_emp_attachment, file);
			            }
					} //// if(obj_Record_Employee)
			} //// if(type == 'delete')
		
	} // End of try
	catch(err)
	{
		var a_emp_attachment = new Array();
		a_emp_attachment['entity'] = 83134;
		 var emailSub = "Error in Script Sending Delete Employee#" + nlapiGetRecordId(); //onthego
		 var emailBoody = "Team," + "\r\n" + "Error while deleting the Employee details into OTG server #" + nlapiGetRecordId() + "\r\n" + "Please find below NS error." + err + "\r\n";
        nlapiSendEmail(442, "onthego@BRILLIO.COM", emailSub, emailBoody, 'information.systems@brillio.com', null, a_emp_attachment);
        nlapiLogExecution('DEBUG', 'Employee Data BS - OTG', err)
	}
} /// function Send_Delete_Data(type)

/*** Support Functions */
function replacer(key, value) {
    if (typeof value == "number" && !isFinite(value)) {
        return String(value);
    }
    return value;
}

function _logValidation(value) {
    if (value != null && value.toString() != null && value != '' && value != undefined && value.toString() != undefined && value != 'undefined' && value.toString() != 'undefined' && value.toString() != 'NaN' && value != NaN) {
        return true;
    } else {
        return false;
    }
}