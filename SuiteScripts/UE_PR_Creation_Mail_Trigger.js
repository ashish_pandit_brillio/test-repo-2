/**
 * Send Email to employee and approver for PR Creation
 * 
 * Version Date Author Remarks 1.00 05 Jan 2016 nitish.mishra
 * 
 */

function afterSubmit(type) {
	try {

		if (type == 'create') {
			var prDetails = getPurchaseRequestMasterDetails(nlapiGetRecordId());

			// send email to the employee
			var mailTemplate = purchaseRequestCreationEmployeeMailTemplate(prDetails);
			nlapiSendEmail(constant.Mail_Author.InformationSystems,
			        prDetails.Employee.Id, mailTemplate.Subject,
			        mailTemplate.Body, 'nithish.murthy@brillio.com', null, {
				        'entity' : prDetails.Employee.Id
			        });
			nlapiLogExecution('DEBUG', 'Employee Mail Triggered',
			        prDetails.Employee.Email);

			// send email for each item to delivery manager and sub-practice
			// head as per the status
			var recipientDetails = null;

			nlapiLogExecution('DEBUG', 'PR Item Count', prDetails.Items.length);

			// send FYI mail to client partner
			sendEmailToClientPartner(prDetails);
			nlapiLogExecution('DEBUG', 'Client Partner Email Triggered');

			for (var index = 0; index < prDetails.Items.length; index++) {
				recipientDetails = null;

				// check the status of the PR Item
				if (prDetails.Items[index].Status == 2) { // sub-practice head
					recipientDetails = nlapiLookupField('employee',
					        prDetails.Items[index].SubPracticeHead, [
					                'firstname', 'email', 'internalid' ]);
					nlapiLogExecution('DEBUG', 'sub-practice head',
					        recipientDetails.email);
				} else if (prDetails.Items[index].Status == 30) { // delivery
					// manager
					recipientDetails = nlapiLookupField('employee',
					        prDetails.Items[index].DeliveryManager, [
					                'firstname', 'email', 'internalid' ]);
					nlapiLogExecution('DEBUG', 'delivery manager',
					        recipientDetails.email);
				}

				if (recipientDetails) {
					mailTemplate = purchaseRequestCreationApproverMailTemplate(
					        prDetails, index, recipientDetails);

					// send email
					nlapiSendEmail(constant.Mail_Author.InformationSystems,
					        recipientDetails.internalid, mailTemplate.Subject,
					        mailTemplate.Body,'nithish.murthy@brillio.com', null, {
						        'entity' : recipientDetails.internalid
					        });

					nlapiLogExecution('DEBUG', 'Approver Mail Triggered',
					        recipientDetails.email);
				}
			}
		}
	} catch (err) {
		nlapiLogExecution('ERROR', 'afterSubmit', err);
	}
}

// get all mail related details

function getPurchaseRequestMasterDetails(prId) {
	try {
		var prRecord = nlapiLoadRecord('customrecord__prpurchaserequest', prId);
		var employeeDetails = nlapiLookupField('employee', prRecord
		        .getFieldValue('custrecord_requisitionname'), [ 'firstname',
		        'lastname', 'email' ]);

		var itemCount = prRecord
		        .getLineItemCount('recmachcustrecord_purchaserequest');

		var itemList = [];
		for (var line = 1; line <= itemCount; line++) {
			itemList.push({
			    Id : prRecord.getLineItemValue(
			            'recmachcustrecord_purchaserequest', 'id', line),
			    Item : prRecord.getLineItemText(
			            'recmachcustrecord_purchaserequest',
			            'custrecord_prmatgrpcategory', line),
			    ItemCategory : prRecord.getLineItemText(
			            'recmachcustrecord_purchaserequest',
			            'custrecord_itemcategory', line),
			    Description : prRecord.getLineItemValue(
			            'recmachcustrecord_purchaserequest',
			            'custrecord_prmatldescription', line),
			    Project : prRecord.getLineItemText(
			            'recmachcustrecord_purchaserequest',
			            'custrecord_project', line),
			    ProjectId : prRecord.getLineItemValue(
			            'recmachcustrecord_purchaserequest',
			            'custrecord_project', line),
			    Quantity : prRecord.getLineItemValue(
			            'recmachcustrecord_purchaserequest',
			            'custrecord_quantity', line),
			    UnitPrice : prRecord.getLineItemValue(
			            'recmachcustrecord_purchaserequest',
			            'custrecord_valueperunit', line),
			    Status : prRecord.getLineItemValue(
			            'recmachcustrecord_purchaserequest',
			            'custrecord_prastatus', line),
			    StatusText : prRecord.getLineItemText(
			            'recmachcustrecord_purchaserequest',
			            'custrecord_prastatus', line),
			    DeliveryManager : prRecord.getLineItemValue(
			            'recmachcustrecord_purchaserequest',
			            'custrecord_pri_delivery_manager', line),
			    SubPracticeHead : prRecord.getLineItemValue(
			            'recmachcustrecord_purchaserequest',
			            'custrecord_subpracticehead', line),
			    DeliveryManagerText : prRecord.getLineItemText(
			            'recmachcustrecord_purchaserequest',
			            'custrecord_pri_delivery_manager', line),
			    SubPracticeHeadText : prRecord.getLineItemText(
			            'recmachcustrecord_purchaserequest',
			            'custrecord_subpracticehead', line)
			});
		}

		return {
		    Details : {
			    Reference : prRecord.getFieldValue('custrecord_prno')
		    },
		    Items : itemList,
		    Employee : {
		        FirstName : employeeDetails.firstname,
		        Email : employeeDetails.email,
		        Id : prRecord.getFieldValue('custrecord_requisitionname'),
		        FullName : prRecord.getFieldText('custrecord_requisitionname')
		    }
		};
	} catch (err) {
		nlapiLogExecution('ERROR', 'getPurchaseRequestMasterDetails', err);
		throw err;
	}
}

// item table template for employee mail
function createItemTable(prDetails) {
	var table = "<table>";

	table += "<tr>";
	table += "<td style='background:#E3E1E0'>Item</td>";
	table += "<td style='background:#E3E1E0'>Description</td>";
	table += "<td style='background:#E3E1E0'>Item Category</td>";
	table += "<td style='background:#E3E1E0'>Project</td>";
	table += "<td style='background:#E3E1E0'>Quantity</td>";
	table += "<td style='background:#E3E1E0'>Unit Price</td>";
	table += "<td style='background:#E3E1E0'>Pending Approval From</td>";
	table += "</tr>";

	for (var i = 0; i < prDetails.Items.length; i++) {
		table += "<tr>";
		table += "<td>" + prDetails.Items[i].Item + "</td>";
		table += "<td>" + prDetails.Items[i].Description + "</td>";
		table += "<td>" + prDetails.Items[i].ItemCategory + "</td>";
		table += "<td>" + prDetails.Items[i].Project + "</td>";
		table += "<td>" + prDetails.Items[i].Quantity + "</td>";
		table += "<td>" + prDetails.Items[i].UnitPrice + "</td>";
		table += "<td>"
		        + (prDetails.Items[i].Status == 30 ? prDetails.Items[i].DeliveryManagerText
		                : prDetails.Items[i].SubPracticeHeadText) + "</td>";
		table += "</tr>";
	}
	table += "</table>";

	return table;
}

// employee mail template
function purchaseRequestCreationEmployeeMailTemplate(prDetails) {
	var htmltext = '<p>Hi ' + prDetails.Employee.FirstName + ',</p>';

	htmltext += "<p>This is to inform you that your purchase request"
	        + " has been sucessfully created and is pending approval.</p>";

	htmltext += createItemTable(prDetails);

	htmltext += '<p>Regards,<br/>';
	htmltext += 'Information Systems</p>';

	return {
	    Subject : 'Purchase Request : ' + prDetails.Details.Reference
	            + ' - Created',
	    Body : addMailTemplate(htmltext)
	};
}

// approver mail template
function purchaseRequestCreationApproverMailTemplate(purhaseRequestDetails,
        itemIndex, recipientDetails)
{
	var htmltext = '<p>Hi ' + recipientDetails.firstname + ',</p>';

	htmltext += "<p>This is to inform you that a purchase request"
	        + " has been raised and is pending your approval.</p>";

	// details grid
	var table = "<table>";
	table += "<tr>";
	table += "<td style='background:#E3E1E0'>Employee</td>";
	table += "<td>" + purhaseRequestDetails.Employee.FullName + "</td>";
	table += "</tr>";
	table += "<tr>";
	table += "<td style='background:#E3E1E0'>Item</td>";
	table += "<td>" + purhaseRequestDetails.Items[itemIndex].Item + "</td>";
	table += "</tr>";
	table += "<tr>";
	table += "<td style='background:#E3E1E0'>Description</td>";
	table += "<td>" + purhaseRequestDetails.Items[itemIndex].Description
	        + "</td>";
	table += "</tr>";
	table += "<tr>";
	table += "<td style='background:#E3E1E0'>Item Category</td>";
	table += "<td>" + purhaseRequestDetails.Items[itemIndex].ItemCategory
	        + "</td>";
	table += "</tr>";
	table += "<tr>";
	table += "<td style='background:#E3E1E0'>Project</td>";
	table += "<td>" + purhaseRequestDetails.Items[itemIndex].Project + "</td>";
	table += "</tr>";
	table += "<tr>";
	table += "<td style='background:#E3E1E0'>Quantity</td>";
	table += "<td>" + purhaseRequestDetails.Items[itemIndex].Quantity + "</td>";
	table += "</tr>";
	table += "<tr>";
	table += "<td style='background:#E3E1E0'>Unit Price</td>";
	table += "<td>" + purhaseRequestDetails.Items[itemIndex].UnitPrice
	        + "</td>";
	table += "</tr>";
	table += "</table>";

	htmltext += table;

	// add the approval link
	var url = "https://system.na1.netsuite.com"
	        + nlapiResolveURL('RECORD', 'customrecord_pritem',
	                purhaseRequestDetails.Items[itemIndex].Id, 'VIEW');
	var link = "<a href='" + url + "'>Click here to view the record</a>";

	htmltext += "<br/>" + link;

	htmltext += '<p>Regards,<br/>';
	htmltext += 'Information Systems</p>';

	return {
	    Subject : 'Purchase Request : '
	            + purhaseRequestDetails.Details.Reference + ' - '
	            + purhaseRequestDetails.Items[itemIndex].StatusText,
	    Body : addMailTemplate(htmltext)
	};
}

// send mail to the client partner
function sendEmailToClientPartner(prDetails) {
	try {
		var processedProjectList = [];
		var currentProject = null;

		for (var i = 0; i < prDetails.Items.length; i++) {
			currentProject = prDetails.Items[i].ProjectId;
			nlapiLogExecution('debug', 'CP Current Project', currentProject);

			// check if mail is already send for this project
			if (_.indexOf(processedProjectList, currentProject) != -1) {
				nlapiLogExecution('debug', 'CP Current Project', 'SKIPPED');
				continue;
			} else {
				// nlapiLogExecution('debug', 'CP Checkpoint', 'A');
				// add the project to the send list
				processedProjectList.push(currentProject);
				var table = "";

				// loop and get all item for this project and create a table
				for (var j = 0; j < prDetails.Items.length; j++) {

					if (prDetails.Items[j].ProjectId == currentProject) {
						table += "<tr>";
						table += "<td>" + prDetails.Items[j].Item + "</td>";
						table += "<td>" + prDetails.Items[j].Description
						        + "</td>";
						table += "<td>" + prDetails.Items[j].ItemCategory
						        + "</td>";
						table += "<td>" + prDetails.Items[j].Project + "</td>";
						table += "<td>" + prDetails.Items[j].Quantity + "</td>";
						table += "<td>" + prDetails.Items[j].UnitPrice
						        + "</td>";
						table += "<td>"
						        + (prDetails.Items[j].Status == 30 ? prDetails.Items[j].DeliveryManagerText
						                : prDetails.Items[j].SubPracticeHeadText)
						        + "</td>";
						table += "</tr>";
					}
				}

				// nlapiLogExecution('debug', 'table', table);
				if (table) {
					// nlapiLogExecution('debug', 'CP Checkpoint', 'B');
					var projectDetails = nlapiLookupField('job',
					        currentProject, [ 'custentity_clientpartner',
					                'altname', 'entityid' ]);

					if (projectDetails.custentity_clientpartner) {
						// nlapiLogExecution('debug', 'CP Checkpoint', 'C');
						var mailTemplate = "";
						mailTemplate = '<p>Hi '
						        + nlapiLookupField(
						                'employee',
						                projectDetails.custentity_clientpartner,
						                'firstname') + ',</p>';

						mailTemplate += "<p><b>This mail is just for your information.</b></p>";

						mailTemplate += "<p>This is to inform you that a purchase request"
						        + " has been raised by <b>"
						        + projectDetails.Employee.FullName
						        + "</b> under <b>"
						        + projectDetails.entityid
						        + " " + projectDetails.altname + "</b>.</p>";

						mailTemplate += "<table>";
						mailTemplate += "<tr>";
						mailTemplate += "<td style='background:#E3E1E0'>Item</td>";
						mailTemplate += "<td style='background:#E3E1E0'>Description</td>";
						mailTemplate += "<td style='background:#E3E1E0'>Item Category</td>";
						mailTemplate += "<td style='background:#E3E1E0'>Project</td>";
						mailTemplate += "<td style='background:#E3E1E0'>Quantity</td>";
						mailTemplate += "<td style='background:#E3E1E0'>Unit Price</td>";
						mailTemplate += "<td style='background:#E3E1E0'>Pending Approval From</td>";
						mailTemplate += "</tr>";
						mailTemplate += table;
						mailTemplate += "</table>";

						mailTemplate += '<p>Regards,<br/>';
						mailTemplate += 'Information Systems</p>';

						nlapiSendEmail('442',
						        projectDetails.custentity_clientpartner,
						        'FYI : Purchase Request #'
						                + prDetails.Details.Reference
						                + ' - New Purchase Request Raised',
						        mailTemplate);
						nlapiLogExecution('debug', 'CP Current Project', 'SEND');
					}
				}
			}
		}
	} catch (err) {
		nlapiLogExecution('ERROR', 'sendEmailToClientPartner', err);
	}
}
