function suitelet(request, response) {
    try {
      if(request.getMethod()=="GET"){
		var context=nlapiGetContext();
		var user=context.getUser()
        var form = nlapiCreateForm("Project Profitability");

        form.addFieldGroup('custpage_grp_1', 'Filters');
        var field_customer = form.addField('custpage_customer', 'select', 'Customer', null, 'custpage_grp_1');
        field_customer.setBreakType('startcol');
        field_customer.setMandatory(true);
		//field_customer.setDisplaySize(200, 20);
		
        
		
		field_customer.addSelectOption('', '');
		
		
		// get all projects he is a ADO
		var customerList = getRelatedCustomers(user,context.getDeploymentId());
		
        if (customerList) {
			nlapiLogExecution('debug','customerList',customerList.length);
            for (var i = 0; i < customerList.length; i++) {
                field_customer.addSelectOption(customerList[i].getId(),
                    customerList[i].getValue('entityid') + " " +
                    customerList[i].getValue('altname'));
            }
        }
        var field_project_statu=form.addField('custpage_project_status', 'select', 'Project Status',null,'custpage_grp_1');
         field_project_statu.setBreakType('startcol');
        field_project_statu.setMandatory(true);
        var request_projectstatus_id=request.getParameter('custpage_project_status');

						if(!request_projectstatus_id){
					    field_project_statu.addSelectOption('', '', true);
						//field_project_statu.addSelectOption('All', 'All');
						}
						else
						{
					    field_project_statu.addSelectOption('', '');
						//field_project_statu.addSelectOption('All', 'All');
						}
						
						field_project_statu.addSelectOption(2,"Active");
						field_project_statu.addSelectOption(1,"Closed");
						field_project_statu.addSelectOption(17,"Hold");
						field_project_statu.addSelectOption(4,"Pending");
						if(request_projectstatus_id){	
						
				        field_project_statu.setDefaultValue(request_projectstatus_id);
						}
        var field_project = form.addField('custpage_project', 'multiselect', 'Project', null, 'custpage_grp_1');
        field_project.setBreakType('startcol');
        field_project.setMandatory(true);
        
       var field_fromdate = form.addField('custpage_fromdate', 'date', 'From', null, 'custpage_grp_1');
        field_fromdate.setBreakType('startcol');
        field_fromdate.setDisplaySize(40, 40);
        //field_fromdate.setMandatory(true);
        var field_todate = form.addField('custpage_todate', 'date', 'To', null, 'custpage_grp_1');
        field_todate.setBreakType('startcol');
			var deploy_id = form.addField('custpage_currentuser_deployementid', 'text', 'deployementid').setDisplayType('hidden');
			deploy_id.setDefaultValue(context.getDeploymentId());
			var s_currentusertype = form.addField('s_currentusertype', 'text', 's_currentusertype').setDisplayType('hidden');
			
				
			  if(context.getDeploymentId()=='customdeploy_sut_ado_pp_screen')
			  {
				  s_currentusertype.setDefaultValue("customer.custentity_account_delivery_owner");
			  }
			  else if(context.getDeploymentId()=='customdeploy_sut_pm_pp_screen')
			  {
				  s_currentusertype.setDefaultValue('custentity_projectmanager');
			  }
			  else if(context.getDeploymentId()=='customdeploy_sut_dm_pp_screen')
			  {
				  s_currentusertype.setDefaultValue('custentity_deliverymanager');
			  }
        
        var field_efc=form.addField('custpage_efc','checkbox','Exclude Facility Cost','custpage_grp_1')
        
		field_todate.setDisplaySize(40, 40);
        
		form.setScript('customscript_cli_ado_project_profitabili');
      

     // response.writePage(form);

		form.addButton('custpage_ppbutton',"Project Profitability",'refreshProfibility');
         response.writePage(form);
      }
        else
          {
			  var context=nlapiGetContext();
			  var form = nlapiCreateForm("Project Profitability");
			  var deploy_id = form.addField('custpage_currentuser_deployementid', 'text', 'deployementid').setDisplayType('normal');
				deploy_id.setDefaultValue(context.getDeploymentId());
			  var deployementid='';
			  if(context.getDeploymentId()=='customdeploy_sut_ado_pp_screen')
			  {
				  deployementid='customdeploy_sut_ado_project_profitabili';
			  }
			  else if(context.getDeploymentId()=='customdeploy_sut_pm_pp_screen')
			  {
				  deployementid='customdeploy_sut_pm_project_profitabili';
			  }
			  else if(context.getDeploymentId()=='customdeploy_sut_dm_pp_screen')
			  {
				  deployementid='customdeploy_sut_dm_project_profitabili';
			  }
            var var_url_servlet = nlapiResolveURL('SUITELET','customscript_sut_ado_project_profitabili',deployementid, false);
			//to send synchronous ajax, To make async add callback param
			var response = nlapiRequestURL(var_url_servlet);
						//alert(request.getParameter('custpage_customer'));
						
			
	var suiteletUrl = nlapiResolveURL('SUITELET', 'customscript_sut_ado_project_profitabili', deployementid, true);

      var param = '&customer=' + request.getParameter('custpage_customer');

       var finalUrl = suiteletUrl + param;

       var newWindowParams = "width=750, height=400,resizeable = 1, scrollbars = 1," +

       "toolbar = 0, location = 0, directories = 0, status = 0, menubar = 0, copyhistory = 0";

       var setWindow = "window.open('" + finalUrl + "',_self,'" + newWindowParams + "')";
        var response = nlapiRequestURL(finalUrl);
      //Add Button and attach suitelet to that button

     // form.addButton("custpage_redirect", "Redirect", setWindow);
             response.writePage(form);
          }

   
      
} catch (err) {
    nlapiLogExecution('ERROR', 'soldMarginTable', err);
}

}

function getRelatedProjects(currentUser, request_customer_id) {
    var filters = [
        ["customer.custentity_account_delivery_owner", "anyof", currentUser],
        'and',
        [
            ['status', 'anyof', '2'], 'or', ['status', 'anyof', '4']
        ]
    ];
    if (request_customer_id) {
        filters.push('and');
        filters.push(["customer", "anyof", request_customer_id]);
    }

    var columns = [new nlobjSearchColumn("entityid"),
        new nlobjSearchColumn("altname")
    ];

    var search = nlapiSearchRecord('job', null, filters, columns);
    return search;
}

function getRelatedCustomers(currentUser,deployementid) {
	
				
				
			  var s_currentuserFilter='';
			  if(deployementid=='customdeploy_sut_ado_pp_screen')
			  {
				  s_currentuserFilter=['custentity_account_delivery_owner', "anyof", currentUser];
			  }
			  else if(deployementid=='customdeploy_sut_pm_pp_screen')
			  {
				s_currentuserFilter=['job.custentity_projectmanager', 'anyof', currentUser];
				
			  }
			  else if(deployementid=='customdeploy_sut_dm_pp_screen')
			  {
				 s_currentuserFilter=['job.custentity_deliverymanager', 'anyof', currentUser];
			  }
			  	  
			
				var filters = [s_currentuserFilter,
													
							  'and',
        [
            ['job.status', 'anyof', '2'], 'or', ['job.status', 'anyof', '4']
        ]
    ];
							  
		
			var columns = [new nlobjSearchColumn("entityid"),
				new nlobjSearchColumn("altname")
			];

		var search = nlapiSearchRecord('customer', null, filters, columns);
		return search;
}

function goBack() {
	window.location = nlapiResolveURL('SUITELET',
	        'customscript1240',
	        'customdeploy_ado');
}

function refreshProfibility() {
	//var customer = nlapiGetFieldValue('custpage_customer');
	var project = nlapiGetFieldValues('custpage_project');
	//var status = nlapiGetFieldValue('custpage_filter_status');
	var startdate = nlapiGetFieldValue('custpage_fromdate');
	var enddate = nlapiGetFieldValue('custpage_todate');
	var s_deployementid=nlapiGetFieldValue('custpage_currentuser_deployementid');
	var s_tragetdeployemnetid='';
	if(s_deployementid=='customdeploy_sut_ado_pp_screen')
	{
		s_tragetdeployemnetid='customdeploy_sut_ado_project_profitabili';
	}
	else if(s_deployementid=='customdeploy_sut_pm_pp_screen')
	{
		s_tragetdeployemnetid='customdeploy_sut_ado_project_profitabili';
	}
	else if(s_deployementid=='customdeploy_sut_dm_pp_screen')
	{
		s_tragetdeployemnetid='customdeploy_sut_dm_project_profitabili';
	}
nlapiLogExecution('debug','project',project);
	window.location = nlapiResolveURL('SUITELET',
	        'customscript_sut_ado_project_profitabili',
	        s_tragetdeployemnetid)
	       // + "&customer="
	        //+ customer
	        + "&project="
	        + project
	        + "&startdate="
	        + startdate + "&enddate=" + enddate; //+ "&status=" + status;
}