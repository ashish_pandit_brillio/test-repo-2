// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
/*
   	Script Name:SUI_Fixed_Bid_Project_Screen_Show
	Author:Swati Kurariya
	Company:Brillio
	Date:20-08-2014
	Description:Show the list wise project based on month and year.


	Script Modification Log:

	-- Date --			-- Modified By --				--Requested By--				-- Description --



Below is a summary of the process controls enforced by this script file.  The control logic is described
more fully, below, in the appropriate function headers and code blocks.


     SUITELET
		- suiteletFunction(request, response)


     SUB-FUNCTIONS
		- The following sub-functions are called by the above core functions in order to maintain code
            modularization:

               - NOT USED

*/
}
// END SCRIPT DESCRIPTION BLOCK  ====================================



// BEGIN GLOBAL VARIABLE BLOCK  =====================================
{
	//  Initialize any Global Variables, in particular, debugging variables...




}
// END GLOBAL VARIABLE BLOCK  =======================================





// BEGIN SUITELET ==================================================

function suiteletFunction_Fixed_Bid_Pro(request, response)
{

	/*  Suitelet:

          - EXPLAIN THE PURPOSE OF THIS FUNCTION

		-Show the list of project based on month and year.


		FIELDS USED:

          --Field Name--				--ID--


	*/



	//  LOCAL VARIABLES



    //  SUITELET CODE BODY

//-----------------------------------------------------------------------------------
	
	var form=nlapiCreateForm("Create Fixed Bid Revenue Rec. Journal Entries");
	var dataArr=new Array();
	form.setScript('customscript_fixed_bid_suitelet_valid');
	//-----------------------field creation---------------------------------
	var month=form.addField('custfield_month', 'select', 'Month').setMandatory(true);
	month.addSelectOption('','');
	month.addSelectOption('1','January');
	month.addSelectOption('2','February');
	month.addSelectOption('3','March');
	month.addSelectOption('4','April');
	month.addSelectOption('5','May');
	month.addSelectOption('6','June');
	month.addSelectOption('7','July');
	month.addSelectOption('8','August');
	month.addSelectOption('9','September');
	month.addSelectOption('10','October');
	month.addSelectOption('11','November');
	month.addSelectOption('12','December');
	//--------------------------------------------------------
	var year=form.addField('custfield_year', 'select', 'Year').setMandatory(true);//, 'customlist_year'
	var date=new Date()
	var curr_year=parseInt(date.getFullYear());
	year.addSelectOption('','');
	year.addSelectOption('1','2014');
	year.addSelectOption('2','2015');
	year.addSelectOption('3','2016');
	year.addSelectOption('4','2017');
	year.addSelectOption('5','2019');
	year.addSelectOption('6','2019');
	year.addSelectOption('7','2020');
	year.addSelectOption('8','2021');
    //--------------------------------------------------------
	var d_suite_date=form.addField('custfield_curr_date', 'date', 'Date').setMandatory(true);
	
	var context = nlapiGetContext();

	var date_format = context.getPreference('dateformat');
	
	var curr_date=new Date();
	
	var mon=curr_date.getMonth()+1;
	var yy=curr_date.getFullYear();
	var date=curr_date.getDate();
	
	if (date_format == 'YYYY-MM-DD')
	 {
		 
		 var today = yy+'/'+mon+'/'+date;
	 }
	else if (date_format == 'DD/MM/YYYY') 
		{
		 
		 var today = date+'/'+mon+'/'+yy;
		}
	else if (date_format == 'MM/DD/YYYY') 
		{
		 var today = mon+'/'+date+'/'+yy;
		}
	
	 d_suite_date.setDefaultValue(today);
	//----------------------------------------------------------------------------
	
	 var d_month = (request.getParameter("custfield_month") == null) ? "" : request.getParameter("custfield_month");
	 nlapiLogExecution('DEBUG','d_month',d_month);
	 
	var d_year=(request.getParameter("custfield_year") == null) ? "" : request.getParameter("custfield_year");
	nlapiLogExecution('DEBUG','d_year',d_year);
	
	var d_date=(request.getParameter("custfield_curr_date") == null) ? "" : request.getParameter("custfield_curr_date");
	nlapiLogExecution('DEBUG','d_date',d_date);
	
	//---------------------------------------------------------------------------
	if(d_month == "" && d_year == "" && d_date == "")
	{//if start
		form.addSubmitButton('LOAD DATA');
	}//if close
	else
	{
		
		var linkURL = nlapiResolveURL('SUITELET', 'customscript_fixed_bid_project_screen','customdeploy1', null);
		var createNewReqLink = form.addField('custpage_new_req_link','inlinehtml', null, null,null);
		var url='https://system.sandbox.netsuite.com/app/site/hosting/scriptlet.nl?script=263&deploy=1&compid=3883006&whence=';
		createNewReqLink.setDefaultValue('<B>Click <A HREF="' + linkURL +'">Here</A>To Home Page.</B>');
		 
		form.addSubmitButton('GENERATE');
		month.setDefaultValue(d_month);
		year.setDefaultValue(d_year);
		 d_suite_date.setDefaultValue(d_date);
		
		month.setDisplayType('disabled');
		year.setDisplayType('disabled');
		d_suite_date.setDisplayType('disabled');

		var sublist=form.addSubList('sublist','list', 'Sublist');
		sublist.addField('custfield_select','checkbox', 'Select');
		sublist.addField('custfield_project','select', 'Project','job').setDisplayType('hidden');
		sublist.addField('custfield_project_name','text', 'Project');
		
		sublist.addMarkAllButtons();
		//------------------search project------------------------
		var project_filter=new Array();
		//project_filter[0]=new nlobjSearchFilter('startdate', null, 'within',d_month+'/1/'+d_year,d_month+'/31/'+d_year);
	 	project_filter[0]=new nlobjSearchFilter('custrecord_fpr_month', null, 'is',d_month);
		project_filter[1]=new nlobjSearchFilter('custrecord_fpr_year', null, 'is',d_year);
		var o_Rev_Rec_search=nlapiSearchRecord('customrecord_fpr_input_revenue_rec', 'customsearch244_2', project_filter,null);//241
	
		//--------------------------------------------------------
		if(o_Rev_Rec_search != null)
			{//if start
			nlapiLogExecution('DEBUG','o_Rev_Rec_search',o_Rev_Rec_search.length);
			for(var kk=0,i=1 ; kk<o_Rev_Rec_search.length ; kk++,i++)
				{//for start
				
				    var rev_rec_id=o_Rev_Rec_search[kk].getId();
				    var result=o_Rev_Rec_search[kk];
				    var all_column=result.getAllColumns();
					var project_id=result.getValue(all_column[0]);
					nlapiLogExecution('DEBUG','project_id',project_id);
					
					//var project_name=result.getText(all_column[6]);
					//nlapiLogExecution('DEBUG','project_name',project_name);
					
					//var load_parent=nlapiLoadRecord('customrecord_fpr_input_revenue_rec',rev_rec_id);
					var load_parent=nlapiLoadRecord('job',project_id);
					var project_name=load_parent.getFieldValue('entityid');
					nlapiLogExecution('DEBUG','project_name',project_name);
			   
				  dataArr.push({
		              // "sno" : i,
		               "custfield_project" : project_id,
		               "custfield_select" : 'F',
		               "custfield_project_name" : project_name,
		               
								});
				}//for close
			}//if close
	 	sublist.setLineItemValues(dataArr);

	
	}	
	//----------------------to get the  sublist line item------------------------
	var project_id_arr=new Array();
	
	if(request.getLineItemCount('sublist') == 0 || request.getLineItemCount('sublist')== -1 || request.getLineItemCount('sublist')== null)
	{ 
	}
	else
	{
			for (var i=1,j=0;i<=request.getLineItemCount('sublist');i++,j++)
		    {//2 for start
		        if (request.getLineItemValue('sublist','custfield_select',i) == 'T')
		        {//2 if start
		        	project_id_arr[j]= request.getLineItemValue('sublist','custfield_project', i);
		           
		
				}//2 if close
			}//2 for close
			nlapiLogExecution('DEBUG','project_id_arr[j]',project_id_arr);
			nlapiLogExecution('DEBUG','d_month',d_month);
			nlapiLogExecution('DEBUG','d_year',d_year);
			//-----------------------call schedule script for future process----------------------------
			 var params=new Array();
		     params['custscript_project_arr'] = project_id_arr.toString();
		     params['custscript_month'] = d_month;
		     params['custscript_year'] = d_year;
		     params['custscript_date'] = d_date;
			// params['custscript_arrcount'] = 0;
		  
		     //schedule(project_id_arr,d_month,d_year);
			var status=nlapiScheduleScript('customscript265',null,params);
		   nlapiLogExecution('DEBUG', 'suiteletFunction', ' Status -->' + status);
		   
		   var param=new Array();
		   param['status']=status;
		   nlapiSetRedirectURL('SUITELET','customscript_status_show_for_schedule','customdeploy1', null,param);
			//------------------------------------------------------------------------------------------
	}
	//---------------------------------------------------------------------------
	//form.addSubmitButton('Generate');
	response.writePage( form );

	//-----------------------------------------------------------------------------------

}

// END SUITELET ====================================================




// BEGIN OBJECT CALLED/INVOKING FUNCTION ===================================================
{



}
// END OBJECT CALLED/INVOKING FUNCTION =====================================================
function schedule(project_id_arr,d_month,d_year)
{//fun start

    //Added by Sitaram
    /*var vendorList = {
		2: '209190',
		3: '209299',
		5: '209360',
		6: '209361',
		7: '209300',
		8: '209358',
		9: '209359',
		10: '209301',
		21: '209302',
		23: '209303',
		
	}*/
  
    var vendorList = { };

var default_vendor_mappingSearch = nlapiSearchRecord("customrecord_default_vendor_mapping",null,
[
],
[
new nlobjSearchColumn("custrecord_subsidiary_je"),
new nlobjSearchColumn("custrecord_default_vendor")
]
);

for (var i_index = 0; i_index < default_vendor_mappingSearch.length; i_index++)
{
    var subsidiary_je = default_vendor_mappingSearch[i_index].getValue('custrecord_subsidiary_je');
    var default_vendor_je = default_vendor_mappingSearch[i_index].getValue('custrecord_default_vendor');
    
    vendorList[subsidiary_je] = default_vendor_je;

}
	
	nlapiLogExecution('DEBUG','project_id_arr[j]=================>',project_id_arr);
	nlapiLogExecution('DEBUG','d_month===================>',d_month);
	nlapiLogExecution('DEBUG','d_year====================>',d_year);
	nlapiLogExecution('DEBUG','project_id_arr.length============>',project_id_arr.length);
	var per_after_amt_sum=0;
	nlapiLogExecution('DEBUG','project_id_arr[0]============>',project_id_arr[0]);
	nlapiLogExecution('DEBUG','project_id_arr[1]============>',project_id_arr[1]);
	for(var ab=0 ;ab<project_id_arr.length ; ab++)
	{//for start
	var project_id=project_id_arr[ab];
	nlapiLogExecution('DEBUG','project_id============>',project_id);
	var project_load=nlapiLoadRecord('job', project_id);
	 
	 var dr_acc=project_load.getFieldValue('custentity_fpr_draccount');
	 nlapiLogExecution('DEBUG','dr_acc',dr_acc);
	 
	 var cr_acc=project_load.getFieldValue('custentity_fpr_craccount');
	 nlapiLogExecution('DEBUG','cr_acc',cr_acc);
	 
	 var subsi=project_load.getFieldValue('subsidiary');
	 nlapiLogExecution('DEBUG','subsi',subsi);
	 
	 var class_name=project_load.getFieldValue('custentity_vertical');
	 nlapiLogExecution('DEBUG','class_name',class_name);
	
	 //-----------------------------------------------------------------------------------------------
	 //----------------------------input revenue record search----------------------------------------
	 var rev_rec_filter=new Array();
	 rev_rec_filter[0]=new nlobjSearchFilter('custrecord_fpr_parent', null, 'is', project_id);
	 rev_rec_filter[1]=new nlobjSearchFilter('custrecord_fpr_month', null, 'is', d_month);
	 rev_rec_filter[2]=new nlobjSearchFilter('custrecord_fpr_year', null, 'is', d_year);
	 rev_rec_filter[3]=new nlobjSearchFilter('custrecord_select', null, 'is', 'F');
	 
	 var rev_rec_column=new Array();
	 rev_rec_column[0]=new nlobjSearchColumn('custrecord_fpr_per_completed');
	 rev_rec_column[1]=new nlobjSearchColumn('custrecord_fpr_ano_milestone');
	 
	 var rev_rec_search=nlapiSearchRecord('customrecord_fpr_input_revenue_rec', null, rev_rec_filter,rev_rec_column);
	 if(rev_rec_search != null)
	{
		for(var ks=0 ; ks<rev_rec_search.length ; ks++)
		{
		    var rev_rec_id=rev_rec_search[ks].getId();
		 	nlapiLogExecution('DEBUG','rev_rec_search.length', rev_rec_search.length);
		 	var per_completed=parseFloat(rev_rec_search[ks].getValue('custrecord_fpr_per_completed'));
		 	nlapiLogExecution('DEBUG', 'per_completed', per_completed);
		 	
		 	var milestone=parseInt(rev_rec_search[ks].getValue('custrecord_fpr_ano_milestone'));
	    	nlapiLogExecution('DEBUG', 'milestone', milestone);
		 	
		 
			 //-----------------------------------------------------------------------------------------------
			 //---------------------------milestone record search---------------------------------------------
			 var milestone_filter=new Array();
			 milestone_filter[0]=new nlobjSearchFilter('custrecordfpr_projparent', null, 'is', project_id);
			 milestone_filter[1]=new nlobjSearchFilter('custrecord_fpr_milestone', null, 'is', milestone);
			 
			 var milestone_column=new Array();
			 milestone_column[0]=new nlobjSearchColumn('custrecord_fpr_amount');
			 
			 var milestone_search=nlapiSearchRecord('customrecord_fpr_milestone_wi', null, milestone_filter,milestone_column);
			 if(milestone_search != null)
				{//milestone if start
				 
					for(var ll=0 ; ll<milestone_search.length ; ll++)
					{//for start
						per_after_amt_sum=0;
						nlapiLogExecution('DEBUG','milestone_search.length', milestone_search.length);
						var milestone_id=milestone_search[0].getId();
						nlapiLogExecution('DEBUG','milestone_id', milestone_id);
						
						var milestone_amt=parseFloat(milestone_search[0].getValue('custrecord_fpr_amount'));
						nlapiLogExecution('DEBUG','milestone_amt', milestone_amt);
						
						var after_per_cal_amt=parseFloat((milestone_amt*(per_completed/100)));
						nlapiLogExecution('DEBUG','after_per_cal_amt', after_per_cal_amt);
						//---------------------------practice percentage search-----------------------------------
						 var pra_per_filter=new Array();
						 pra_per_filter[0]=new nlobjSearchFilter('custrecord_fpr_prac_project_par', null, 'is', milestone_id);
						
						 var pra_per_column=new Array();
						 pra_per_column[0]=new nlobjSearchColumn('custrecord_fpr_percentage');
						 pra_per_column[1]=new nlobjSearchColumn('custrecord_fpr_practice');
						 
						 var practice_per_search=nlapiSearchRecord('customrecord_fpr_prac_wise_re', null, pra_per_filter, pra_per_column);
						 if(practice_per_search != null)
							{//if start
								nlapiLogExecution('DEBUG','practice_per_search.length',practice_per_search.length);
								//----------------------------create JE------------------------------------------
								var create_je=nlapiCreateRecord('journalentry');
									create_je.setFieldValue('subsidiary',subsi);
									create_je.setFieldValue('custbody_financehead','1610');
									create_je.setFieldValue('custbody_revenue_record_ref',rev_rec_id);
								for(var ss=0 ; ss<practice_per_search.length ; ss++)
								{//for start
								  
									var i_percentage=parseInt(practice_per_search[ss].getValue('custrecord_fpr_percentage'));
									nlapiLogExecution('DEBUG','i_percentage',i_percentage);
									
									var i_practice=parseInt(practice_per_search[ss].getValue('custrecord_fpr_practice'));
									nlapiLogExecution('DEBUG','i_practice',i_practice);
									
									var per_after_amt=(after_per_cal_amt*(i_percentage/100));
									nlapiLogExecution('DEBUG', 'per_after_amt',per_after_amt);
									per_after_amt_sum=per_after_amt_sum+per_after_amt;
									create_je.selectNewLineItem('line');
									
									create_je.setCurrentLineItemValue('line', 'account', dr_acc);
									create_je.setCurrentLineItemValue('line','debit', per_after_amt);
									create_je.setCurrentLineItemValue('line','department', i_practice);//pratice
									create_je.setCurrentLineItemValue('line','class', class_name);//vertical
									//Added by Sitaram
									if(_logValidation(vendorList[subsi])){
									create_je.setCurrentLineItemValue('line', 'entity', vendorList[subsi]);												
									}
									create_je.commitLineItem('line');
									
								}//for close
								
								nlapiLogExecution('DEBUG', 'per_after_amt_sum',per_after_amt_sum);
								nlapiLogExecution('DEBUG', 'cr_acc',cr_acc);
								
								create_je.selectNewLineItem('line');
								create_je.setCurrentLineItemValue('line','account',cr_acc);
								create_je.setCurrentLineItemValue('line','credit', per_after_amt_sum);
								//Added by Sitaram
								if(_logValidation(vendorList[subsi])){
								create_je.setCurrentLineItemValue('line', 'entity', vendorList[subsi]);												
								}
								create_je.commitLineItem('line');
								var je_id=nlapiSubmitRecord(create_je);
								nlapiLogExecution('DEBUG','je_id',je_id);
								
								//------------------------------------------------------------------------------
								var JE_Record_In_Project=nlapiCreateRecord('customrecord_journal_entry');
								JE_Record_In_Project.setFieldValue('custrecord_fpr_je',je_id);
								JE_Record_In_Project.setFieldValue('custrecord_revenue_record',rev_rec_id);
								JE_Record_In_Project.setFieldValue('custrecord_month',d_month);
								JE_Record_In_Project.setFieldValue('custrecord_year',d_year);
								JE_Record_In_Project.setFieldValue('custrecord_parent',project_id);
								var JE_Record_In_Project_id=nlapiSubmitRecord(JE_Record_In_Project);
								nlapiLogExecution('DEBUG','JE_Record_In_Project_id',JE_Record_In_Project_id);
								//----------------------------------------------------------------------------
								nlapiSubmitField('customrecord_fpr_input_revenue_rec', rev_rec_id, 'custrecord_select', 'T');
								
							}//if close
						//----------------------------------------------------------------------------------------
					}//for close
				 }//milestone if close
		}
	}
	 //-----------------------------------------------------------------------------------------------
	}//for close
}//fun close

//Added by Sitaram
function _logValidation(value) 
{
 if(value!='null' && value != null && value != null && value != '' && value != undefined && value != undefined && value != 'undefined' && value!= 'undefined'&& value!= 'NaN' && value != NaN) 
 {
  return true;
 }
 else 
 { 
  return false;
 }
}
