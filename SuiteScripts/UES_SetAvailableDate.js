// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
/*
   	Script Name : UES_SetAvailableDate.js
	Author      : Ashish Pandit
	Date        : 24 April 2018
    Description : User Event to set Resource Available Date  


	Script Modification Log:

	-- Date --			-- Modified By --				--Requested By--				-- Description --


Below is a summary of the process controls enforced by this script file.  The control logic is described
more fully, below, in the appropriate function headers and code blocks.


     BEFORE LOAD
		- beforeLoadRecord(type)



     BEFORE SUBMIT
		- beforeSubmitRecord(type)


     AFTER SUBMIT
		- afterSubmitRecord(type)



     SUB-FUNCTIONS
		- The following sub-functions are called by the above core functions in order to maintain code
            modularization:

               - NOT USED

*/
}
// END SCRIPT DESCRIPTION BLOCK  ====================================



// BEGIN GLOBAL VARIABLE BLOCK  =====================================
{
	//  Initialize any Global Variables, in particular, debugging variables...

}
// END GLOBAL VARIABLE BLOCK  =======================================



// BEGIN BEFORE LOAD ==================================================

function afterSubmitSetAvailableDate(type)
{
	if(type=='edit' || type == 'create' || type == 'xedit')
	{
		try
		{
			var recId = nlapiGetRecordId();
			var recType = nlapiGetRecordType();
			var recordObject = nlapiLoadRecord(recType,recId);
			
			var b_extHire = recordObject.getFieldValue('custrecord_frf_details_external_hire');
			nlapiLogExecution('Debug','b_extHire ',b_extHire);
			if(b_extHire=='T')
			{
				var s_location = recordObject.getFieldText('custrecord_frf_details_res_location');
				nlapiLogExecution('Debug','s_location ',s_location);
				var Location = s_location.split(':');
				nlapiLogExecution('Debug','Location [0] ',Location[0]);
				Location = Location[0];
				if(Location.trim() == 'United States')
				{
					Location = 'US';
				}
				nlapiLogExecution('Debug','Location ',Location);
				var s_practice = recordObject.getFieldValue('custrecord_frf_details_res_practice');
				nlapiLogExecution('Debug','s_practice ',s_practice);
				var d_startDate = recordObject.getFieldValue('custrecord_frf_details_start_date');
				var i_skills = recordObject.getFieldValue('custrecord_frf_details_primary_skills');
				nlapiLogExecution('debug','i_skills ',i_skills);
				var skills = getSkillIds(i_skills);
				var tempAge = 0;
				if(skills.length>0)
				{
					nlapiLogExecution('debug','skills.length ',skills.length);
					var customrecord_fuel_skill_ageSearch = nlapiSearchRecord("customrecord_fuel_skill_age",null,
					[
					   ["custrecord_fuel_skillage_skill","anyof",skills], 
					   "AND", 
					   ["formulatext: {custrecord_fuel_frfage_location}","is",Location.trim()]					  
					   /*["custrecord_fuel_frfage_location","contains",Location], 
					   "AND", 
					   ["custrecord_fuel_skillage_practice","anyof",s_practice]*/
					], 
					[
					   new nlobjSearchColumn("custrecord_fuel_frfage_hire_count"), 
					   new nlobjSearchColumn("custrecord_fuel_frfage_classification")
					]
					);
					if(customrecord_fuel_skill_ageSearch)
					{
						nlapiLogExecution('Debug','customrecord_fuel_skill_ageSearch ',customrecord_fuel_skill_ageSearch.length);
						for(var j=0;j<customrecord_fuel_skill_ageSearch.length;j++)
						{
							var days = customrecord_fuel_skill_ageSearch[j].getValue('custrecord_fuel_frfage_hire_count');
                            nlapiLogExecution('Debug','days ',days);
							if(parseInt(tempAge) < parseInt(days))
							{
								tempAge = parseInt(days);
								nlapiLogExecution('Debug','tempAge ',tempAge);
							}
						}
					}
					var dateToSet = nlapiAddDays(nlapiStringToDate(d_startDate), tempAge);
					nlapiLogExecution('Debug','dateToSet',dateToSet);
					recordObject.setFieldValue('custrecord_frf_details_availabledate',dateToSet);
					var recObj = nlapiSubmitRecord(recordObject);
				}
				// Set RRF Number
				/*var rrfNumber = getRRFNumber(nlapiGetRecordId());
				recordObject.setFieldValue("custrecord_frf_details_ns_rrf_number",rrfNumber);
				var recObj = nlapiSubmitRecord(recordObject);*/
			}
		}
		catch(error)
		{
			nlapiLogExecution('Debug','Error ',error);
		}
		
	}
}

// END BEFORE LOAD ====================================================


function _logValidation(value) 
{
 if(value != null && value!= 'null' && value != '' && value != undefined && value != undefined && value != 'undefined' && value.toString() != 'NaN' && value != NaN) 
 {
  return true;
 }
 else 
 { 
  return false;
 }
}
// END FUNCTION =====================================================
function getSkillIds(s_skills)
{
	var resultArray = new Array();
	if(_logValidation(s_skills))
	{
		nlapiLogExecution('Debug','s_skills in function',s_skills);
		var temp = s_skills.split(',');
		for(var i=0; i<temp.length;i++)
		{
			resultArray.push(temp[i]);
		}
	}
	return resultArray;
}
function getRRFNumber(recId) {
	var str = "" + recId;
	var pad = "00000000";
	var s_number = pad.substring(0, pad.length - str.length) + str;
	return "R"+s_number;
}