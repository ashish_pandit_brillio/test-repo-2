/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       23 Nov 2017     deepak.srinivas
 *
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType
 * 
 * @param {String} type Operation types: create, edit, delete, xedit,
 *                      approve, cancel, reject (SO, ER, Time Bill, PO & RMA only)
 *                      pack, ship (IF only)
 *                      dropship, specialorder, orderitems (PO only) 
 *                      paybills (vendor payments)
 * @returns {Void}
 */
function userEventAfterSubmit(type){
	
	try{
		if(type == 'create' || type == 'edit'){
		
			//Conversion rate search
			var inr_to_usd = 0;
			var gbp_to_usd = 0;
			var eur_to_usd = 0;
			var a_conversion_rate_table = nlapiSearchRecord('customrecord_conversion_rate_sfdc',null,null,[new nlobjSearchColumn('custrecord_rate_inr_to_usd'),
			                                                              new nlobjSearchColumn('custrecord_rate_gbp_to_usd'),
			                                                              new nlobjSearchColumn('custrecord_rate_eur_to_usd'),
			                                                              new nlobjSearchColumn('internalid').setSort(true)]);
			if(a_conversion_rate_table){
				inr_to_usd = 1 / parseFloat(a_conversion_rate_table[0].getValue('custrecord_rate_inr_to_usd'));
				gbp_to_usd = a_conversion_rate_table[0].getValue('custrecord_rate_gbp_to_usd');
				eur_to_usd = a_conversion_rate_table[0].getValue('custrecord_rate_eur_to_usd');
		}
        var JSON_OBJ  = {};
		var dataRow = [];
		var project_Obj = nlapiLoadRecord(nlapiGetRecordType(),nlapiGetRecordId());
		var pro_St_date =  project_Obj.getFieldValue('startdate');
		var pro_end_date =  project_Obj.getFieldValue('enddate');
	
		var o_job_old_rcrd = nlapiGetOldRecord();
	//	var old_status = o_job_old_rcrd.getFieldText('entitystatus');
		var new_status = project_Obj.getFieldText('entitystatus');
		var i_pm = project_Obj.getFieldValue('custentity_projectmanager');
		var check_sfdc_update = project_Obj.getFieldValue('custentity_project_id_update_sfdc');
		var billingType =  project_Obj.getFieldText('jobbillingtype');
		var billingType_revRec =  project_Obj.getFieldText('custentity_fp_rev_rec_type');
		var i_ProjectValue =  project_Obj.getFieldValue('custentity_projectvalue');
		var s_currency = project_Obj.getFieldText('custentity_project_currency');
		//Conversion factor
		var f_currency_conversion = 1;
		if(s_currency =='USD')
			f_currency_conversion = 1;
		if(s_currency =='INR')
			f_currency_conversion = inr_to_usd;
		if(s_currency =='EUR')
			f_currency_conversion = eur_to_usd;
		if(s_currency =='GBP')
			f_currency_conversion = gbp_to_usd;
		
		if(!_logValidation(i_ProjectValue)){
			i_ProjectValue = 0;
		}
		if(_logValidation(billingType_revRec))
			var s_billing_type = billingType_revRec;
		else
			var s_billing_type = billingType;
		
		var pm_email ='';
		//Sow's list 
		var SOW_ID_ = searchSow(nlapiGetRecordId());
		if(i_pm){
			var employee_look = nlapiLookupField('employee',i_pm,['email']);
			pm_email = employee_look.email;
		}
		//
		if(check_sfdc_update == 'F' && _logValidation(i_pm)){
			//var sow_id = searchSOW(nlapiGetRecordId(),pro_St_date,pro_end_date);
			if(project_Obj.getFieldText('jobtype') == 'External' )//&& old_status != new_status
			{
				JSON_OBJ = {
					'ProjectID': project_Obj.getFieldValue('entityid'),
					'ProjectName': project_Obj.getFieldValue('companyname'),
					'Customer': project_Obj.getFieldText('parent'),
					'Status': project_Obj.getFieldText('entitystatus'),
					'BillingType': s_billing_type,
					'StartDate': project_Obj.getFieldValue('startdate'),
					'EndDate': project_Obj.getFieldValue('enddate'), //custentity_practice
					'SubPractice': project_Obj.getFieldText('custentity_practice'),
					'ProjectValue': (parseFloat(f_currency_conversion) * parseFloat(i_ProjectValue)).toFixed(1), //custentity_project_currency
                 	'ProjectManager': project_Obj.getFieldText('custentity_projectmanager'),
					'ProjectManager_Email':	pm_email,
					'SOW_ID': SOW_ID_
			};
				dataRow.push(JSON_OBJ);
				
			var temp = nlapiSubmitField('job',nlapiGetRecordId(),'custentity_project_id_update_sfdc','T');	
			//Calling SFDC
		var status =	call_sfdc(dataRow,check_sfdc_update );
			
		}
		}
		else{
			if(project_Obj.getFieldText('jobtype') == 'External' && _logValidation(i_pm) )//&& old_status != new_status
			{
				JSON_OBJ = {
					'ProjectID': project_Obj.getFieldValue('entityid'),
					'ProjectName': project_Obj.getFieldValue('companyname'),
					'BillingType': s_billing_type, 
					//'Customer': project_Obj.getFieldText('parent'),
					//'Status': project_Obj.getFieldText('entitystatus'),
					//'StartDate': project_Obj.getFieldValue('startdate'),
					//'EndDate': project_Obj.getFieldValue('enddate'), //custentity_practice
					//'SubPractice': project_Obj.getFieldText('custentity_practice'),
				    'ProjectValue': (parseFloat(f_currency_conversion) * parseFloat(i_ProjectValue)).toFixed(1),
                  	'ProjectManager': project_Obj.getFieldText('custentity_projectmanager'),
					'ProjectManager_Email':	pm_email,
					'SOW_ID': SOW_ID_
			};
				dataRow.push(JSON_OBJ);
				//call sfdc
				var status =	call_sfdc(dataRow,check_sfdc_update );
				
		}
		}
		
	
		
		}    
	       
	       
			//End of SuiteScript  
	}
	
	catch(e){
		
		nlapiLogExecution('DEBUG','Process Error',e);
		throw e;
	}
  
}

function replacer(key, value){
    if (typeof value == "number" && !isFinite(value)){
        return String(value);
    }
    return value;
}

function objToString(obj, ndeep) {
	  if(obj == null){ return String(obj); }
	  switch(typeof obj){
	    case "string": return '"'+obj+'"';
	    case "function": return obj.name || obj.toString();
	    case "object":
	      var indent = Array(ndeep||1).join('\t'), isArray = Array.isArray(obj);
	      return '{['[+isArray] + Object.keys(obj).map(function(key){
	           return '\n\t' + indent + key + ': ' + objToString(obj[key], (ndeep||1)+1);
	         }).join(',') + '\n' + indent + '}]'[+isArray];
	    default: return obj.toString();
	  }
	}

function call_sfdc(dataRows,check_sfdc_update){
	try{
	//Start of SuiteScript
	
	var accessURL = getAccessToken();
	
    var method = 'POST';
    
 
    var response = nlapiRequestURL(accessURL,null,null,method);
    var temp = response.body;
    nlapiLogExecution('DEBUG','JSON',temp);
    var data = JSON.parse(response.body);
    var access_token_SFDC = data.access_token;
    var instance_URL_Sfdc = data.instance_url;
    nlapiLogExecution('DEBUG','JSON',data);
    
	if(check_sfdc_update == 'F'){
	//SFDC Rest URL
	var dynamic_URL = instance_URL_Sfdc+'/services/apexrest/Project/v1.0/';
	}
	else{
	//var dynamic_URL = 'https://brillio--Brillio.cs50.my.salesforce.com/services/apexrest/ProjectBackUpdate/v1.0';	
	var dynamic_URL = instance_URL_Sfdc+'/services/apexrest/ProjectBackUpdate/v1.0';	
	}	     
    
    var headers = {"Authorization": "Bearer "+access_token_SFDC,
    		 "Content-Type": "application/json",
    		 "accept": "application/json"};
    //
    var method_ = 'POST';
    var response_ = nlapiRequestURL(dynamic_URL,JSON.stringify(dataRows),headers,method_);
    var data = JSON.parse(response_.body);
    var message = data[0].message;
    nlapiLogExecution('DEBUG','JSON',message); 
    nlapiLogExecution('DEBUG','JSON',JSON.stringify(dataRows)); 
	}
	catch(e){
		nlapiLogExecution('DEBUG','SFDC Call error',e);
		
	}
}
function searchSOW(pro_id,pro_stDate,pro_endDate){
	try{
		var searchRes = nlapiSearchRecord('salesorder',null,[new nlobjSearchFilter('internalid','jobmain','anyof',pro_id),
		                                                     new nlobjSearchFilter('mainline',null,'is','T'),
		                                                     new nlobjSearchFilter('status',null,'anyof',['SalesOrd:F','SalesOrd:E','SalesOrd:G'])],
															[new nlobjSearchColumn('internalid'),
															 new nlobjSearchColumn('tranid'), 
															 new nlobjSearchColumn('custbody_opp_id_sfdc')]);
		var Sow_array = [];	
		if(searchRes){
			for(var i=0;i<searchRes.length;i++){
			Sow_array.push({
				SOW_ID: searchRes[i].getValue('tranid'),
				Opp_id: searchRes[i].getValue('custbody_opp_id_sfdc')
			});
			}
		}
		return Sow_array;
	}
	catch(e){
		nlapiLogExecution('DEBUG','Process Error',e);
		throw e;
	}
}

function credentials(){
    this.email = "deepak.srinivas@brillio.com";
    this.account = "3883006";
    this.role = "3";
    this.password = "Welcome@1992";
}
 
function replacer(key, value){
    if (typeof value == "number" && !isFinite(value)){
        return String(value);
    }
    return value;
}
//validate blank entries
function _logValidation(value) 
{
 if(value != null && value.toString() != null && value != '' && value != undefined && value.toString() != undefined && value != 'undefined' && value.toString() != 'undefined'&& value.toString() != 'NaN' && value != NaN) 
 {
  return true;
 }
 else 
 { 
  return false;
 }
}
//Search For SOW's'
function searchSow(pro_id){
	try{
		var searchRes = nlapiSearchRecord('salesorder',null,[new nlobjSearchFilter('internalid','jobmain','anyof',pro_id),
		                                                     new nlobjSearchFilter('mainline',null,'is','T'),
		                                                     new nlobjSearchFilter('status',null,'anyof',['SalesOrd:F','SalesOrd:E','SalesOrd:G'])],
															[new nlobjSearchColumn('internalid'),
															 new nlobjSearchColumn('tranid'), 
															 new nlobjSearchColumn('custbody_opp_id_sfdc')]);
		var Sow_array = [];	
		if(searchRes){
			for(var i=0;i<searchRes.length;i++){
			Sow_array.push({
				SOW_ID: searchRes[i].getValue('tranid'),
				Opp_id: searchRes[i].getValue('custbody_opp_id_sfdc')
			});
			}
		}
		return Sow_array;
	}
	catch(e){
		nlapiLogExecution('DEBUG','searchSow Error',e);
	}
}