/**
 * @author Sai
 */

var f_current_month_actual_revenue_total_proj_level = 0;
var a_recognized_revenue_total_proj_level = new Array();
var f_total_cost_overall_prac = new Array();
var f_revenue_arr=new Array();
 var flag_counter_arr=0;
var flag_counter = 0;
var mnth_array=new Array();
var active_practices = new Array();
mnth_array=['JAN','FEB','MAR','APR','MAY','JUN','JUL','AUG','SEP','OCT','NOV','DEC']
function schedule_createJE()
{
	try
	{
		var o_context = nlapiGetContext();
		var i_fp_revrec_id = o_context.getSetting('SCRIPT','custscript_validate_recrd_id');
    // var i_fp_revrec_id = parseInt(44);
		nlapiLogExecution('DEBUG','recid',i_fp_revrec_id);
		if(!i_fp_revrec_id)
			return;
			
		var a_fp_revrec_rcrd_details = nlapiLookupField('customrecord_fp_rev_rec_je_creation',i_fp_revrec_id,
		['custrecord_debit_gl_fp_rev_rec', 'custrecord_credit_gl_fp_rev_rec', 'custrecord_fp_revrec_subsidiary']);
		
		var o_rev_rec_rcrd = nlapiLoadRecord('customrecord_fp_rev_rec_je_creation',i_fp_revrec_id);
		var a_ignore_proj = o_rev_rec_rcrd.getFieldValues('custrecord_rev_rec_ignore_proj');
		
		var d_todays_date = nlapiStringToDate(nlapiDateToString(new Date()));
		var d_firstDay_mnth = new Date(d_todays_date.getFullYear(), d_todays_date.getMonth(), 1);
		var d_lastDay_mnth = new Date(d_todays_date.getFullYear(), d_todays_date.getMonth()+1, 0);
		var i_days_in_mnth = getDatediffIndays(d_firstDay_mnth,d_lastDay_mnth);
		
		var s_currentMonthName = o_rev_rec_rcrd.getFieldText('custrecord_month_fp_rev_rec');
		s_currentMonthName = s_currentMonthName.substr(0,3);
		s_currentMonthName = s_currentMonthName.toUpperCase();
		var s_currentYear = o_rev_rec_rcrd.getFieldText('custrecord_year_fp_rev_rec');
		var subsi=o_rev_rec_rcrd.getFieldValue('custrecord_fp_revrec_subsidiary');
		var del=delete_existing(s_currentMonthName,s_currentYear,subsi);
      //Added to handle Inactive Practice
     var departmentSearch = nlapiSearchRecord("department",null,
[["isinactive","is","F"]],[ new nlobjSearchColumn("name").setSort(false),
   new nlobjSearchColumn("internalid")]);
      for(var act_pract_indx = 0;act_pract_indx < departmentSearch.length; act_pract_indx++)
        {
           active_practices.push(
             departmentSearch[act_pract_indx].getValue('internalid')
           );
        }
      //ended
		var s_selected_mnth_last_date = get_previous_month_end_date(s_currentMonthName,s_currentYear);
		var d_selected_mnth_last_date = nlapiStringToDate(s_selected_mnth_last_date);
      var d_firstDay_mnth = new Date(d_selected_mnth_last_date.getFullYear(), d_selected_mnth_last_date.getMonth(), 1);
		var d_prev_lastday_mnth = new Date(d_selected_mnth_last_date.getFullYear(), d_selected_mnth_last_date.getMonth(), 0);
		
		//*added for start date and endDate
		
		var d_month_start = nlapiAddDays(d_selected_mnth_last_date, -1 * (d_selected_mnth_last_date.getDate() - 1));
		var s_month_start = d_month_start.getMonth() + 1 + '/'
		        + d_month_start.getDate() + '/' + d_month_start.getFullYear();
		/*var d_month_end = nlapiAddDays(nlapiAddMonths(d_month_start, 1), -1);
		var s_month_end = d_month_end.getMonth() + 1 + '/'
		        + d_month_end.getDate() + '/' + d_month_end.getFullYear();*/
				
		//*************//
		var a_project_filter = new Array();
		a_project_filter[0] = new nlobjSearchFilter('status', null, 'noneof', 1);
		a_project_filter[1] = new nlobjSearchFilter('jobtype', null, 'anyof', 2);
		a_project_filter[2] = new nlobjSearchFilter('jobbillingtype', null, 'anyof', 'FBM');
		a_project_filter[3] = new nlobjSearchFilter('custentity_fp_rev_rec_type', null, 'anyof', [1,2,4]);
		a_project_filter[4] = new nlobjSearchFilter('subsidiary', null, 'anyof', parseInt(a_fp_revrec_rcrd_details.custrecord_fp_revrec_subsidiary));
		
		
		if(a_ignore_proj)
		{
			//nlapiLogExecution('audit','proj a_ignore_proj:- '+a_ignore_proj);
			a_project_filter[5] = new nlobjSearchFilter('internalid', null, 'noneof', a_ignore_proj);
		}
		//a_project_filter[6] = new nlobjSearchFilter('internalid', null, 'anyof',56395);
		var a_project_search_results = searchRecord('job', null, a_project_filter, null);
		
		if (a_project_search_results)
		{
			nlapiLogExecution('audit','proj len:- '+a_project_search_results.length);
			for (var i_proj_index = 0; i_proj_index < a_project_search_results.length; i_proj_index++)
			{
				if(o_context.getRemainingUsage() <= 2000)
				{
					nlapiYieldScript();
				}
				
				var i_hold_milestone_flag = 0;
				var a_subpractice_ytd_created = new Array();
				
				var i_proj = a_project_search_results[i_proj_index].getId();
				nlapiLogExecution('AUDIT','Project Id',i_proj);
				
				
				
				var o_project_object = nlapiLoadRecord('job', a_project_search_results[i_proj_index].getId()); // load project record object
				// get necessary information about project from project record
				var s_project_region = o_project_object.getFieldValue('custentity_region');
				var i_customer_name = o_project_object.getFieldValue('parent');
				var s_customer_name = o_project_object.getFieldText('parent');
				var customer_lookup = nlapiLookupField('customer',parseInt(i_customer_name),'custentity_sow_validation_exception');
				var s_project_name = o_project_object.getFieldValue('companyname');
				var s_proj_entity_id = o_project_object.getFieldValue('entityid');
				s_project_name = s_proj_entity_id +' '+s_project_name;
				var d_proj_start_date = o_project_object.getFieldValue('startdate');
				var d_proj_strt_date_old_proj = o_project_object.getFieldValue('custentity_rev_rec_new_strt_date');
				var d_proj_enddate_old_proj = o_project_object.getFieldValue('custentity_rev_rec_new_end_date');
				var d_proj_end_date = o_project_object.getFieldValue('enddate');
				var i_proj_executing_practice = o_project_object.getFieldValue('custentity_practice');
				var i_proj_manager = o_project_object.getFieldValue('custentity_projectmanager');
				var i_proj_manager_practice = nlapiLookupField('employee', i_proj_manager, 'department');
				var i_project_sow_value = o_project_object.getFieldValue('custentity_projectvalue');
				var i_proj_revenue_rec_type = o_project_object.getFieldValue('custentity_fp_rev_rec_type');
				var s_proj_currency = o_project_object.getFieldText('custentity_project_currency');
				var i_project_value_ytd = o_project_object.getFieldValue('custentity_ytd_rev_recognized');
				if(!i_project_value_ytd)
					i_project_value_ytd = 0;
				i_project_sow_value = parseFloat(i_project_sow_value) - parseFloat(i_project_value_ytd);
				i_project_sow_value = parseFloat(i_project_sow_value).toFixed(2);
              var customrecord_fp_rev_rec_recognized_amntSearch = nlapiSearchRecord("customrecord_fp_rev_rec_recognized_amnt",null,
[["custrecord_project_to_recognize_amount","anyof",a_project_search_results[i_proj_index].getId()]], 
[new nlobjSearchColumn("custrecord_revenue_recognized",null,"SUM")]
);
              var amount_rec_till_date = customrecord_fp_rev_rec_recognized_amntSearch[0].getValue("custrecord_revenue_recognized",null,"SUM");
				if(i_proj_revenue_rec_type==1)
				{
				
				//{
					if(d_proj_strt_date_old_proj)
					{
						d_proj_start_date = d_proj_strt_date_old_proj;
					}
					if(d_proj_enddate_old_proj)
					{
						d_proj_end_date = d_proj_enddate_old_proj;
					}
			
					var monthBreakUp = getMonthsBreakup(nlapiStringToDate(d_proj_start_date), nlapiStringToDate(d_proj_end_date));
				
					var d_pro_strtDate = nlapiStringToDate(d_proj_start_date);
					var i_year_project = d_pro_strtDate.getFullYear();
					
					var s_pro_endDate = nlapiStringToDate(d_proj_end_date);
					var i_year_project_end = s_pro_endDate.getFullYear();
					
					var i_project_mnth = d_pro_strtDate.getMonth();
					var i_prject_end_mnth = s_pro_endDate.getMonth();
					
					var s_currency_symbol_proj = getCurrency_Symbol(s_proj_currency);
					var s_currency_symbol_usd = getCurrency_Symbol('USD');
					if((s_pro_endDate >= d_firstDay_mnth ))
                     {
					var a_revenue_recognized_for_project = new Array();
					var a_filter_get_ytd_revenue_recognized = [['custrecord_project_to_recognize_amount', 'anyof', a_project_search_results[i_proj_index].getId()]];
					
					var a_columns_get_ytd_revenue_recognized = new Array();
					a_columns_get_ytd_revenue_recognized[0] = new nlobjSearchColumn('custrecord_project_to_recognize_amount');
					a_columns_get_ytd_revenue_recognized[1] = new nlobjSearchColumn('custrecord_practice_to_recognize_amount');
					a_columns_get_ytd_revenue_recognized[2] = new nlobjSearchColumn('custrecord_subparctice_to_recognize_amnt');
					a_columns_get_ytd_revenue_recognized[3] = new nlobjSearchColumn('custrecord_role_to_recognize_amount');
					a_columns_get_ytd_revenue_recognized[4] = new nlobjSearchColumn('custrecord_level_to_recognize_amount');
					a_columns_get_ytd_revenue_recognized[5] = new nlobjSearchColumn('custrecord_revenue_recognized');
					a_columns_get_ytd_revenue_recognized[6] = new nlobjSearchColumn('custrecord_month_to_recognize_amount');
					a_columns_get_ytd_revenue_recognized[7] = new nlobjSearchColumn('custrecord_year_to_recognize_amount');
					
					var a_get_ytd_revenue = nlapiSearchRecord('customrecord_fp_rev_rec_recognized_amnt', null, a_filter_get_ytd_revenue_recognized, a_columns_get_ytd_revenue_recognized);
					if (a_get_ytd_revenue)
					{
						for (var i_revenue_index = 0; i_revenue_index < a_get_ytd_revenue.length; i_revenue_index++)
						{
							a_revenue_recognized_for_project.push({
								'practice_revenue_recognized': a_get_ytd_revenue[i_revenue_index].getValue('custrecord_practice_to_recognize_amount'),
								'subpractice_revenue_recognized': a_get_ytd_revenue[i_revenue_index].getValue('custrecord_subparctice_to_recognize_amnt'),
								'role_revenue_recognized': a_get_ytd_revenue[i_revenue_index].getValue('custrecord_role_to_recognize_amount'),
								'level_revenue_recognized': a_get_ytd_revenue[i_revenue_index].getValue('custrecord_level_to_recognize_amount'),
								'amount_revenue_recognized': a_get_ytd_revenue[i_revenue_index].getValue('custrecord_revenue_recognized'),
								'month_revenue_recognized': a_get_ytd_revenue[i_revenue_index].getValue('custrecord_month_to_recognize_amount'),
								'year_revenue_recognized': a_get_ytd_revenue[i_revenue_index].getValue('custrecord_year_to_recognize_amount')
							});
						}
					}
				
					var a_revenue_cap_filter = [['custrecord_revenue_share_project', 'anyof', parseInt(a_project_search_results[i_proj_index].getId())]];
					
					var a_column = new Array();
					a_column[0] = new nlobjSearchColumn('created').setSort(true);
					a_column[1] = new nlobjSearchColumn('custrecord_auto_number_counter');
					a_column[2] = new nlobjSearchColumn('custrecord_revenue_share_approval_status');
					a_column[3]= new nlobjSearchColumn('custrecord_sow_value_change_status');
					a_column[4]= new nlobjSearchColumn('custrecord_revenue_share_total');
					
					var a_get_logged_in_user_exsiting_revenue_cap = nlapiSearchRecord('customrecord_revenue_share', null, a_revenue_cap_filter, a_column);
					if (a_get_logged_in_user_exsiting_revenue_cap)
					{
						var i_revenue_share_id = a_get_logged_in_user_exsiting_revenue_cap[0].getId();
						var i_revenue_share_status = a_get_logged_in_user_exsiting_revenue_cap[0].getValue('custrecord_revenue_share_approval_status');
						var i_saved_status=a_get_logged_in_user_exsiting_revenue_cap[0].getValue('custrecord_sow_value_change_status');
						var pro_revenue_share= a_get_logged_in_user_exsiting_revenue_cap[0].getValue('custrecord_revenue_share_total');
						if (parseInt(i_revenue_share_status) == parseInt(3) || (i_saved_status=='Saved')) {
							
							var a_get_mnth_end_effrt_activity='';//30-11-2020@Nihal- Updated the Decalration variable as Global variable instead of local parameter as the script was fetched the different month end record.
							nlapiLogExecution('audit', 'revenue share found:- ' + i_revenue_share_id);
							if (parseInt(i_revenue_share_status) == parseInt(3))
							{
							var a_effort_activity_mnth_end_filter = [['custrecord_revenue_share_parent_json', 'anyof', parseInt(i_revenue_share_id)], 'and', ['created', 'onorbefore', d_selected_mnth_last_date],'and',['custrecord_is_mnth_effrt_confirmed','is','T']];
							var a_columns_mnth_end_effort_activity_srch = new Array();
							a_columns_mnth_end_effort_activity_srch[0] = new nlobjSearchColumn('created').setSort(true);
							a_get_mnth_end_effrt_activity = nlapiSearchRecord('customrecord_fp_rev_rec_mnth_end_json', null, a_effort_activity_mnth_end_filter, a_columns_mnth_end_effort_activity_srch);
							}
							else if((i_saved_status=='Saved') )
							{
                              if(parseFloat(pro_revenue_share) <= parseFloat(i_project_sow_value))
                              {
								var a_effort_activity_mnth_end_filter = [['custrecord_revenue_share_parent_json', 'anyof', parseInt(i_revenue_share_id)], 'and', ['created', 'onorbefore', d_selected_mnth_last_date],'and',['custrecord_is_mnth_effrt_confirmed','is','T']];
								var a_columns_mnth_end_effort_activity_srch = new Array();
								a_columns_mnth_end_effort_activity_srch[0] = new nlobjSearchColumn('created').setSort(true);
								
								a_get_mnth_end_effrt_activity = nlapiSearchRecord('customrecord_fp_rev_rec_mnth_end_json', null, a_effort_activity_mnth_end_filter, a_columns_mnth_end_effort_activity_srch);
                              }
							}
							var a_duplicate_sub_prac_count = new Array();//30-11-2020@Nihal- Updated the Decalration variable as Global variable instead of local parameter as the script was fetched the different month end record.
							
							if (a_get_mnth_end_effrt_activity) {
								var i_month_end_activity_id = a_get_mnth_end_effrt_activity[0].getId();
								nlapiLogExecution('audit', 'i_month_end_activity_id:- ' + i_month_end_activity_id);
								var projectWiseRevenue = [];
								
								var s_effrt_json = nlapiLookupField('customrecord_fp_rev_rec_mnth_end_json', i_month_end_activity_id, 'custrecord_json_mnth_end_effrt');
								var s_effrt_json_2 = nlapiLookupField('customrecord_fp_rev_rec_mnth_end_json', i_month_end_activity_id, 'custrecord_json_mnth_end_effrt2');
								var s_effrt_json_3 = nlapiLookupField('customrecord_fp_rev_rec_mnth_end_json', i_month_end_activity_id, 'custrecord_json_mnth_end_effrt3');
								var s_effrt_json_4 = nlapiLookupField('customrecord_fp_rev_rec_mnth_end_json', i_month_end_activity_id, 'custrecord_json_mnth_end_effrt4');
								
								
								var a_unique_list_practice_sublist = new Array();
								var a_unique_list_level = new Array();
								
								var a_subprac_searched_once = new Array();
								var a_subprac_searched_once_month = new Array();
								var a_subprac_searched_once_year = new Array();
								
								if (s_effrt_json) {
									var i_practice_previous = 0;
									var f_total_revenue_for_tenure = 0;
									//for (var i_mnth_end_plan = 0; i_mnth_end_plan < a_get_mnth_end_effrt_plan.length; i_mnth_end_plan++)
									var s_entire_json_clubed = JSON.parse(s_effrt_json);
									for (var i_mnth_end_plan = 0; i_mnth_end_plan < s_entire_json_clubed.length; i_mnth_end_plan++) {
										var a_row_json_data = JSON.parse(JSON.stringify(s_entire_json_clubed[i_mnth_end_plan]));
										for (var i_row_json_index = 0; i_row_json_index < a_row_json_data.length; i_row_json_index++) {
											if (a_unique_list_practice_sublist.indexOf(a_row_json_data[i_row_json_index].subprac) < 0) {
												a_unique_list_practice_sublist.push(a_row_json_data[i_row_json_index].subprac);
												a_duplicate_sub_prac_count.push({
													'prac': a_row_json_data[i_row_json_index].prac,
													'sub_prac': a_row_json_data[i_row_json_index].subprac,
													'sub_prac_name': a_row_json_data[i_row_json_index].subprac_text,
													'emp_role': a_row_json_data[i_row_json_index].role,
													'emp_level': a_row_json_data[i_row_json_index].level,
													'emp_level_text': a_row_json_data[i_row_json_index].level_text,
													'emp_cost': a_row_json_data[i_row_json_index].cost,
													'revenue_share': 50000,
													'no_count': 0
												});
											}
											
											var i_practice = a_row_json_data[i_row_json_index].prac;
											var s_practice = a_row_json_data[i_row_json_index].prac_text;
											var i_sub_practice = a_row_json_data[i_row_json_index].subprac;
											var s_sub_practice = a_row_json_data[i_row_json_index].subprac_text;
											var i_role = a_row_json_data[i_row_json_index].role;
											var s_role = a_row_json_data[i_row_json_index].role_text;
											var i_level = a_row_json_data[i_row_json_index].level;
											var s_level = a_row_json_data[i_row_json_index].level_text;
											var i_location = a_row_json_data[i_row_json_index].loc;
											var s_location = a_row_json_data[i_row_json_index].loc_text;
											var f_revenue = a_row_json_data[i_row_json_index].cost;
											if (!f_revenue) 
												f_revenue = 0;
											
											var f_revenue_share = a_row_json_data[i_row_json_index].share;
											if (!f_revenue_share) 
												f_revenue_share = 0;
											
											var i_no_of_resources = a_row_json_data[i_row_json_index].allo;
											var s_mnth_strt_date = '1/31/2017';
											var s_mnth_end_date = '31/31/2017';
											var s_mnth = a_row_json_data[i_row_json_index].mnth;
											var s_year = a_row_json_data[i_row_json_index].year;
											
											var f_revenue_recognized = find_recognized_revenue(a_revenue_recognized_for_project,i_practice,i_sub_practice,i_role,i_level,s_mnth,s_year,a_subprac_searched_once,a_subprac_searched_once_month,a_subprac_searched_once_year);
											
											if (!f_revenue_recognized) 
												f_revenue_recognized = 0;
											
											var s_month_name = s_mnth + '_' + s_year;
											
											var total_revenue = parseFloat(i_no_of_resources) * parseFloat(f_revenue);
											total_revenue = parseFloat(total_revenue).toFixed(2);
											
											if (i_practice_previous == 0) {
												i_practice_previous = a_row_json_data[i_row_json_index].prac_row;
											}
											
											if (i_practice_previous != a_row_json_data[i_row_json_index].prac_row) {
												f_total_revenue_for_tenure = 0;
												i_practice_previous = a_row_json_data[i_row_json_index].prac_row;
											}
											
											f_total_revenue_for_tenure = parseFloat(f_total_revenue_for_tenure) + parseFloat(total_revenue);
											f_total_revenue_for_tenure = parseFloat(f_total_revenue_for_tenure).toFixed(2);
											
											var i_prac_subPrac_role_level = i_practice + '_' + i_sub_practice + '_' + i_role + '_' + i_level + '_' + i_location;
											
											if (!projectWiseRevenue[i_prac_subPrac_role_level]) {
												projectWiseRevenue[i_prac_subPrac_role_level] = {
													practice_name: s_practice,
													sub_prac_name: s_sub_practice,
													sub_practice: i_sub_practice,
													role_name: s_role,
													level_name: s_level,
													location_name: s_location,
													total_revenue_for_tenure: f_total_revenue_for_tenure,
													revenue_share: f_revenue_share,
													RevenueData: {}
												};
												
												projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice] = new yearData(i_year_project);
												
												projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].Amount = total_revenue;
												projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].prcnt_allocated = i_no_of_resources;
												projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].recognized_revenue = f_revenue_recognized;
												projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].mnth_strt = s_mnth_strt_date;
												projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].mnth_end = s_mnth_end_date;
											}
											else {
												projectWiseRevenue[i_prac_subPrac_role_level].total_revenue_for_tenure = f_total_revenue_for_tenure;
												
												projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].Amount = total_revenue;
												projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].prcnt_allocated = i_no_of_resources;
												projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].recognized_revenue = f_revenue_recognized;
												projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].mnth_strt = s_mnth_strt_date;
												projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].mnth_end = s_mnth_end_date;
											}
										}
									}
								}
								
								if (s_effrt_json_2) {
									var i_practice_previous = 0;
									var f_total_revenue_for_tenure = 0;
									//for (var i_mnth_end_plan = 0; i_mnth_end_plan < a_get_mnth_end_effrt_plan.length; i_mnth_end_plan++)
									var s_entire_json_clubed = JSON.parse(s_effrt_json_2);
									for (var i_mnth_end_plan = 0; i_mnth_end_plan < s_entire_json_clubed.length; i_mnth_end_plan++) {
										var a_row_json_data = JSON.parse(JSON.stringify(s_entire_json_clubed[i_mnth_end_plan]));
										for (var i_row_json_index = 0; i_row_json_index < a_row_json_data.length; i_row_json_index++) {
											if (a_unique_list_practice_sublist.indexOf(a_row_json_data[i_row_json_index].subprac) < 0) {
												a_unique_list_practice_sublist.push(a_row_json_data[i_row_json_index].subprac);
												a_duplicate_sub_prac_count.push({
													'prac': a_row_json_data[i_row_json_index].prac,
													'sub_prac': a_row_json_data[i_row_json_index].subprac,
													'sub_prac_name': a_row_json_data[i_row_json_index].subprac_text,
													'emp_role': a_row_json_data[i_row_json_index].role,
													'emp_level': a_row_json_data[i_row_json_index].level,
													'emp_level_text': a_row_json_data[i_row_json_index].level_text,
													'emp_cost': a_row_json_data[i_row_json_index].cost,
													'revenue_share': 50000,
													'no_count': 0
												});
											}
											
											var i_practice = a_row_json_data[i_row_json_index].prac;
											var s_practice = a_row_json_data[i_row_json_index].prac_text;
											var i_sub_practice = a_row_json_data[i_row_json_index].subprac;
											var s_sub_practice = a_row_json_data[i_row_json_index].subprac_text;
											var i_role = a_row_json_data[i_row_json_index].role;
											var s_role = a_row_json_data[i_row_json_index].role_text;
											var i_level = a_row_json_data[i_row_json_index].level;
											var s_level = a_row_json_data[i_row_json_index].level_text;
											var i_location = a_row_json_data[i_row_json_index].loc;
											var s_location = a_row_json_data[i_row_json_index].loc_text;
											var f_revenue = a_row_json_data[i_row_json_index].cost;
											if (!f_revenue) 
												f_revenue = 0;
											
											var f_revenue_share = a_row_json_data[i_row_json_index].share;
											if (!f_revenue_share) 
												f_revenue_share = 0;
											
											//nlapiLogExecution('audit','f_revenue_recognized:- '+f_revenue_recognized);
											
											var i_no_of_resources = a_row_json_data[i_row_json_index].allo;
											var s_mnth_strt_date = '1/31/2017';
											var s_mnth_end_date = '31/31/2017';
											var s_mnth = a_row_json_data[i_row_json_index].mnth;
											var s_year = a_row_json_data[i_row_json_index].year;
											
											var f_revenue_recognized = 0;
											var f_revenue_recognized = find_recognized_revenue(a_revenue_recognized_for_project,i_practice,i_sub_practice,i_role,i_level,s_mnth,s_year,a_subprac_searched_once,a_subprac_searched_once_month,a_subprac_searched_once_year);
											
											if (!f_revenue_recognized) 
												f_revenue_recognized = 0;
											
											var s_month_name = s_mnth + '_' + s_year;
											
											var total_revenue = parseFloat(i_no_of_resources) * parseFloat(f_revenue);
											total_revenue = parseFloat(total_revenue).toFixed(2);
											
											if (i_practice_previous == 0) {
												i_practice_previous = a_row_json_data[i_row_json_index].prac_row;
											}
											
											if (i_practice_previous != a_row_json_data[i_row_json_index].prac_row) {
												f_total_revenue_for_tenure = 0;
												i_practice_previous = a_row_json_data[i_row_json_index].prac_row;
											}
											
											f_total_revenue_for_tenure = parseFloat(f_total_revenue_for_tenure) + parseFloat(total_revenue);
											f_total_revenue_for_tenure = parseFloat(f_total_revenue_for_tenure).toFixed(2);
											
											var i_prac_subPrac_role_level = i_practice + '_' + i_sub_practice + '_' + i_role + '_' + i_level + '_' + i_location;
											
											if (!projectWiseRevenue[i_prac_subPrac_role_level]) {
												projectWiseRevenue[i_prac_subPrac_role_level] = {
													practice_name: s_practice,
													sub_prac_name: s_sub_practice,
													sub_practice: i_sub_practice,
													role_name: s_role,
													level_name: s_level,
													location_name: s_location,
													total_revenue_for_tenure: f_total_revenue_for_tenure,
													revenue_share: f_revenue_share,
													RevenueData: {}
												};
												
												projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice] = new yearData(i_year_project);
												
												projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].Amount = total_revenue;
												projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].prcnt_allocated = i_no_of_resources;
												projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].recognized_revenue = f_revenue_recognized;
												projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].mnth_strt = s_mnth_strt_date;
												projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].mnth_end = s_mnth_end_date;
											}
											else {
												projectWiseRevenue[i_prac_subPrac_role_level].total_revenue_for_tenure = f_total_revenue_for_tenure;
												
												projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].Amount = total_revenue;
												projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].prcnt_allocated = i_no_of_resources;
												projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].recognized_revenue = f_revenue_recognized;
												projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].mnth_strt = s_mnth_strt_date;
												projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].mnth_end = s_mnth_end_date;
											}
										}
									}
								}
								
								if (s_effrt_json_3) {
									var i_practice_previous = 0;
									var f_total_revenue_for_tenure = 0;
									//for (var i_mnth_end_plan = 0; i_mnth_end_plan < a_get_mnth_end_effrt_plan.length; i_mnth_end_plan++)
									var s_entire_json_clubed = JSON.parse(s_effrt_json_3);
									for (var i_mnth_end_plan = 0; i_mnth_end_plan < s_entire_json_clubed.length; i_mnth_end_plan++) {
										var a_row_json_data = JSON.parse(JSON.stringify(s_entire_json_clubed[i_mnth_end_plan]));
										for (var i_row_json_index = 0; i_row_json_index < a_row_json_data.length; i_row_json_index++) {
											if (a_unique_list_practice_sublist.indexOf(a_row_json_data[i_row_json_index].subprac) < 0) {
												a_unique_list_practice_sublist.push(a_row_json_data[i_row_json_index].subprac);
												a_duplicate_sub_prac_count.push({
													'prac': a_row_json_data[i_row_json_index].prac,
													'sub_prac': a_row_json_data[i_row_json_index].subprac,
													'sub_prac_name': a_row_json_data[i_row_json_index].subprac_text,
													'emp_role': a_row_json_data[i_row_json_index].role,
													'emp_level': a_row_json_data[i_row_json_index].level,
													'emp_level_text': a_row_json_data[i_row_json_index].level_text,
													'emp_cost': a_row_json_data[i_row_json_index].cost,
													'revenue_share': 50000,
													'no_count': 0
												});
											}
											
											var i_practice = a_row_json_data[i_row_json_index].prac;
											var s_practice = a_row_json_data[i_row_json_index].prac_text;
											var i_sub_practice = a_row_json_data[i_row_json_index].subprac;
											var s_sub_practice = a_row_json_data[i_row_json_index].subprac_text;
											var i_role = a_row_json_data[i_row_json_index].role;
											var s_role = a_row_json_data[i_row_json_index].role_text;
											var i_level = a_row_json_data[i_row_json_index].level;
											var s_level = a_row_json_data[i_row_json_index].level_text;
											var i_location = a_row_json_data[i_row_json_index].loc;
											var s_location = a_row_json_data[i_row_json_index].loc_text;
											var f_revenue = a_row_json_data[i_row_json_index].cost;
											if (!f_revenue) 
												f_revenue = 0;
											
											var f_revenue_share = a_row_json_data[i_row_json_index].share;
											if (!f_revenue_share) 
												f_revenue_share = 0;
											
											//nlapiLogExecution('audit','f_revenue_recognized:- '+f_revenue_recognized);
											
											var i_no_of_resources = a_row_json_data[i_row_json_index].allo;
											var s_mnth_strt_date = '1/31/2017';
											var s_mnth_end_date = '31/31/2017';
											var s_mnth = a_row_json_data[i_row_json_index].mnth;
											var s_year = a_row_json_data[i_row_json_index].year;
											
											var f_revenue_recognized = 0;
											var f_revenue_recognized = find_recognized_revenue(a_revenue_recognized_for_project,i_practice,i_sub_practice,i_role,i_level,s_mnth,s_year,a_subprac_searched_once,a_subprac_searched_once_month,a_subprac_searched_once_year);
											
											if (!f_revenue_recognized) 
												f_revenue_recognized = 0;
											
											var s_month_name = s_mnth + '_' + s_year;
											
											var total_revenue = parseFloat(i_no_of_resources) * parseFloat(f_revenue);
											total_revenue = parseFloat(total_revenue).toFixed(2);
											
											if (i_practice_previous == 0) {
												i_practice_previous = a_row_json_data[i_row_json_index].prac_row;
											}
											
											if (i_practice_previous != a_row_json_data[i_row_json_index].prac_row) {
												f_total_revenue_for_tenure = 0;
												i_practice_previous = a_row_json_data[i_row_json_index].prac_row;
											}
											
											f_total_revenue_for_tenure = parseFloat(f_total_revenue_for_tenure) + parseFloat(total_revenue);
											f_total_revenue_for_tenure = parseFloat(f_total_revenue_for_tenure).toFixed(2);
											
											var i_prac_subPrac_role_level = i_practice + '_' + i_sub_practice + '_' + i_role + '_' + i_level + '_' + i_location;
											
											if (!projectWiseRevenue[i_prac_subPrac_role_level]) {
												projectWiseRevenue[i_prac_subPrac_role_level] = {
													practice_name: s_practice,
													sub_prac_name: s_sub_practice,
													sub_practice: i_sub_practice,
													role_name: s_role,
													level_name: s_level,
													location_name: s_location,
													total_revenue_for_tenure: f_total_revenue_for_tenure,
													revenue_share: f_revenue_share,
													RevenueData: {}
												};
												
												projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice] = new yearData(i_year_project);
												
												projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].Amount = total_revenue;
												projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].prcnt_allocated = i_no_of_resources;
												projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].recognized_revenue = f_revenue_recognized;
												projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].mnth_strt = s_mnth_strt_date;
												projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].mnth_end = s_mnth_end_date;
											}
											else {
												projectWiseRevenue[i_prac_subPrac_role_level].total_revenue_for_tenure = f_total_revenue_for_tenure;
												
												projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].Amount = total_revenue;
												projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].prcnt_allocated = i_no_of_resources;
												projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].recognized_revenue = f_revenue_recognized;
												projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].mnth_strt = s_mnth_strt_date;
												projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].mnth_end = s_mnth_end_date;
											}
										}
									}
								}
								
								if (s_effrt_json_4) {
									var i_practice_previous = 0;
									var f_total_revenue_for_tenure = 0;
									//for (var i_mnth_end_plan = 0; i_mnth_end_plan < a_get_mnth_end_effrt_plan.length; i_mnth_end_plan++)
									var s_entire_json_clubed = JSON.parse(s_effrt_json_4);
									for (var i_mnth_end_plan = 0; i_mnth_end_plan < s_entire_json_clubed.length; i_mnth_end_plan++) {
										var a_row_json_data = JSON.parse(JSON.stringify(s_entire_json_clubed[i_mnth_end_plan]));
										for (var i_row_json_index = 0; i_row_json_index < a_row_json_data.length; i_row_json_index++) {
											if (a_unique_list_practice_sublist.indexOf(a_row_json_data[i_row_json_index].subprac) < 0) {
												a_unique_list_practice_sublist.push(a_row_json_data[i_row_json_index].subprac);
												a_duplicate_sub_prac_count.push({
													'prac': a_row_json_data[i_row_json_index].prac,
													'sub_prac': a_row_json_data[i_row_json_index].subprac,
													'sub_prac_name': a_row_json_data[i_row_json_index].subprac_text,
													'emp_role': a_row_json_data[i_row_json_index].role,
													'emp_level': a_row_json_data[i_row_json_index].level,
													'emp_level_text': a_row_json_data[i_row_json_index].level_text,
													'emp_cost': a_row_json_data[i_row_json_index].cost,
													'revenue_share': 50000,
													'no_count': 0
												});
											}
											
											var i_practice = a_row_json_data[i_row_json_index].prac;
											var s_practice = a_row_json_data[i_row_json_index].prac_text;
											var i_sub_practice = a_row_json_data[i_row_json_index].subprac;
											var s_sub_practice = a_row_json_data[i_row_json_index].subprac_text;
											var i_role = a_row_json_data[i_row_json_index].role;
											var s_role = a_row_json_data[i_row_json_index].role_text;
											var i_level = a_row_json_data[i_row_json_index].level;
											var s_level = a_row_json_data[i_row_json_index].level_text;
											var i_location = a_row_json_data[i_row_json_index].loc;
											var s_location = a_row_json_data[i_row_json_index].loc_text;
											var f_revenue = a_row_json_data[i_row_json_index].cost;
											if (!f_revenue) 
												f_revenue = 0;
											
											var f_revenue_share = a_row_json_data[i_row_json_index].share;
											if (!f_revenue_share) 
												f_revenue_share = 0;
											
											//nlapiLogExecution('audit','f_revenue_recognized:- '+f_revenue_recognized);
											
											var i_no_of_resources = a_row_json_data[i_row_json_index].allo;
											var s_mnth_strt_date = '1/31/2017';
											var s_mnth_end_date = '31/31/2017';
											var s_mnth = a_row_json_data[i_row_json_index].mnth;
											var s_year = a_row_json_data[i_row_json_index].year;
											
											var f_revenue_recognized = 0;
											var f_revenue_recognized = find_recognized_revenue(a_revenue_recognized_for_project,i_practice,i_sub_practice,i_role,i_level,s_mnth,s_year,a_subprac_searched_once,a_subprac_searched_once_month,a_subprac_searched_once_year);
											
											if (!f_revenue_recognized) 
												f_revenue_recognized = 0;
											
											var s_month_name = s_mnth + '_' + s_year;
											
											var total_revenue = parseFloat(i_no_of_resources) * parseFloat(f_revenue);
											total_revenue = parseFloat(total_revenue).toFixed(2);
											
											if (i_practice_previous == 0) {
												i_practice_previous = a_row_json_data[i_row_json_index].prac_row;
											}
											
											if (i_practice_previous != a_row_json_data[i_row_json_index].prac_row) {
												f_total_revenue_for_tenure = 0;
												i_practice_previous = a_row_json_data[i_row_json_index].prac_row;
											}
											
											f_total_revenue_for_tenure = parseFloat(f_total_revenue_for_tenure) + parseFloat(total_revenue);
											f_total_revenue_for_tenure = parseFloat(f_total_revenue_for_tenure).toFixed(2);
											
											var i_prac_subPrac_role_level = i_practice + '_' + i_sub_practice + '_' + i_role + '_' + i_level + '_' + i_location;
											
											if (!projectWiseRevenue[i_prac_subPrac_role_level]) {
												projectWiseRevenue[i_prac_subPrac_role_level] = {
													practice_name: s_practice,
													sub_prac_name: s_sub_practice,
													sub_practice: i_sub_practice,
													role_name: s_role,
													level_name: s_level,
													location_name: s_location,
													total_revenue_for_tenure: f_total_revenue_for_tenure,
													revenue_share: f_revenue_share,
													RevenueData: {}
												};
												
												projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice] = new yearData(i_year_project);
												
												projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].Amount = total_revenue;
												projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].prcnt_allocated = i_no_of_resources;
												projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].recognized_revenue = f_revenue_recognized;
												projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].mnth_strt = s_mnth_strt_date;
												projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].mnth_end = s_mnth_end_date;
											}
											else {
												projectWiseRevenue[i_prac_subPrac_role_level].total_revenue_for_tenure = f_total_revenue_for_tenure;
												
												projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].Amount = total_revenue;
												projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].prcnt_allocated = i_no_of_resources;
												projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].recognized_revenue = f_revenue_recognized;
												projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].mnth_strt = s_mnth_strt_date;
												projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].mnth_end = s_mnth_end_date;
											}
										}
									}
								}
								
								// Get Cost to be recognized for each sub practice
								var a_practice_nt_processed = new Array();
								var a_index_prac_nt_processed = new Array();
								
								//for(var i_allo_index=0; i_allo_index<a_allocation_details.length; i_allo_index++)
								{
									for (var i_dupli = 0; i_dupli < a_duplicate_sub_prac_count.length; i_dupli++) {
										if (o_context.getRemainingUsage() <= 1000) {
											nlapiYieldScript();
										}
										
										var f_actual_revenue_to_recognize = generate_cost_view(projectWiseRevenue, monthBreakUp, i_year_project, i_project_mnth, 2, a_duplicate_sub_prac_count[i_dupli].sub_prac, s_currency_symbol_proj, s_currency_symbol_usd, i_year_project_end, i_prject_end_mnth,s_currentMonthName);
										nlapiLogExecution('audit', 'json to process:- ' + f_actual_revenue_to_recognize, 'level:-- ' + a_duplicate_sub_prac_count[i_dupli].emp_level_text);
										
										f_actual_revenue_to_recognize = f_actual_revenue_to_recognize.split('##');
										var f_amount_to_recognize = f_actual_revenue_to_recognize[0];
										var f_amount_sub_total = f_actual_revenue_to_recognize[1];
										
										if (f_amount_sub_total == 0) {
											f_amount_sub_total = 1;
										}
										
										var f_resource_cost = a_duplicate_sub_prac_count[i_dupli].emp_cost;
										nlapiLogExecution('audit', 'f_amount_to_recognize:- ' + f_amount_to_recognize + ':::: f_amount_sub_total:- ' + f_amount_sub_total, 'f_resource_cost:- ' + f_resource_cost);
										
										{
                                          //Added for Invalid Practice
											if (a_subpractice_ytd_created.indexOf(a_duplicate_sub_prac_count[i_dupli].sub_prac) < 0  && active_practices.indexOf(a_duplicate_sub_prac_count[i_dupli].sub_prac) >= 0 && active_practices.indexOf(a_duplicate_sub_prac_count[i_dupli].prac)>=0) {
												var o_ytd_rcrd = nlapiCreateRecord('customrecord_fp_other_validate_excel_dat');
												o_ytd_rcrd.setFieldValue('custrecord_fp_other_validate_project', a_project_search_results[i_proj_index].getId());
												o_ytd_rcrd.setFieldValue('custrecord_fp_other_validate_practice', a_duplicate_sub_prac_count[i_dupli].prac);
												o_ytd_rcrd.setFieldValue('custrecord_fp_othr_validate_sub_pract', a_duplicate_sub_prac_count[i_dupli].sub_prac);
												o_ytd_rcrd.setFieldValue('custrecord_fp_othr_validate_role', a_duplicate_sub_prac_count[i_dupli].emp_role);
												o_ytd_rcrd.setFieldValue('custrecord_fp_othr_validate_level', a_duplicate_sub_prac_count[i_dupli].emp_level);
												o_ytd_rcrd.setFieldValue('custrecord_fp_othr_validate_rev_amnt', parseFloat(f_amount_to_recognize));
												//o_ytd_rcrd.setFieldValue('custrecord_subtotal_amount', f_amount_sub_total);
												o_ytd_rcrd.setFieldValue('custrecord_fp_othr_validate_mnth', s_currentMonthName);
												o_ytd_rcrd.setFieldValue('custrecord_fp_othr_validate_year', s_currentYear);
												o_ytd_rcrd.setFieldValue('custrecord_validate_subsi', subsi);
												//o_ytd_rcrd.setFieldValue('custrecord_resource_name',a_allocation_details[i_allo_index].i_resource_allo_text);
												o_ytd_rcrd.setFieldValue('custrecord_fp_othr_proj_type', i_proj_revenue_rec_type);
												var i_ytd_rcrd_id = nlapiSubmitRecord(o_ytd_rcrd, true, true);
												
												a_subpractice_ytd_created.push(a_duplicate_sub_prac_count[i_dupli].sub_prac);
											}
										}
									}
								}
							}
						}
					}
					}
				}
				else{
							f_current_month_actual_revenue_total_proj_level = 0;
							a_recognized_revenue_total_proj_level = new Array();
							flag_counter=0;
							f_revenue_arr=new Array();
							flag_counter_arr=0;
							f_total_cost_overall_prac=new Array();
					
							if(d_proj_strt_date_old_proj)
							{
								d_proj_start_date = d_proj_strt_date_old_proj;
							}
							if(d_proj_enddate_old_proj)
							{
								d_proj_end_date = d_proj_enddate_old_proj;
							}
						var monthBreakUp = getMonthsBreakup(nlapiStringToDate(d_proj_start_date), nlapiStringToDate(d_proj_end_date));
					
						var d_pro_strtDate = nlapiStringToDate(d_proj_start_date);
						var i_year_project = d_pro_strtDate.getFullYear();
						
						var s_pro_endDate = nlapiStringToDate(d_proj_end_date);
						var i_year_project_end = s_pro_endDate.getFullYear();
						//added for enddate
						// s_pro_endDate >= d_prev_lastday_mnt
					if((s_pro_endDate >= d_firstDay_mnth ))
					{
						var i_project_mnth = d_pro_strtDate.getMonth();
						var i_prject_end_mnth = s_pro_endDate.getMonth();
						
						var s_currency_symbol_proj = getCurrency_Symbol(s_proj_currency);
						var s_currency_symbol_usd = getCurrency_Symbol('USD');
						
						var a_revenue_recognized_for_project = new Array();
						var a_filter_get_ytd_revenue_recognized = [['custrecord_project_to_recognize_amount', 'anyof', a_project_search_results[i_proj_index].getId()],'and',
														['custrecord_fp_rev_rec_type', 'anyof' , 4]];
						
						var a_columns_get_ytd_revenue_recognized = new Array();
						a_columns_get_ytd_revenue_recognized[0] = new nlobjSearchColumn('custrecord_project_to_recognize_amount');
						a_columns_get_ytd_revenue_recognized[1] = new nlobjSearchColumn('custrecord_practice_to_recognize_amount');
						a_columns_get_ytd_revenue_recognized[2] = new nlobjSearchColumn('custrecord_subparctice_to_recognize_amnt');
						a_columns_get_ytd_revenue_recognized[3] = new nlobjSearchColumn('custrecord_role_to_recognize_amount');
						a_columns_get_ytd_revenue_recognized[4] = new nlobjSearchColumn('custrecord_level_to_recognize_amount');
						a_columns_get_ytd_revenue_recognized[5] = new nlobjSearchColumn('custrecord_revenue_recognized');
						a_columns_get_ytd_revenue_recognized[6] = new nlobjSearchColumn('custrecord_month_to_recognize_amount');
						a_columns_get_ytd_revenue_recognized[7] = new nlobjSearchColumn('custrecord_year_to_recognize_amount');
						
						var a_get_ytd_revenue = nlapiSearchRecord('customrecord_fp_rev_rec_recognized_amnt', null, a_filter_get_ytd_revenue_recognized, a_columns_get_ytd_revenue_recognized);
						if (a_get_ytd_revenue)
						{
							for (var i_revenue_index = 0; i_revenue_index < a_get_ytd_revenue.length; i_revenue_index++)
							{
								a_revenue_recognized_for_project.push({
									'practice_revenue_recognized': a_get_ytd_revenue[i_revenue_index].getValue('custrecord_practice_to_recognize_amount'),
									'subpractice_revenue_recognized': a_get_ytd_revenue[i_revenue_index].getValue('custrecord_subparctice_to_recognize_amnt'),
									'role_revenue_recognized': a_get_ytd_revenue[i_revenue_index].getValue('custrecord_role_to_recognize_amount'),
									'level_revenue_recognized': a_get_ytd_revenue[i_revenue_index].getValue('custrecord_level_to_recognize_amount'),
									'amount_revenue_recognized': a_get_ytd_revenue[i_revenue_index].getValue('custrecord_revenue_recognized'),
									'month_revenue_recognized': a_get_ytd_revenue[i_revenue_index].getValue('custrecord_month_to_recognize_amount'),
									'year_revenue_recognized': a_get_ytd_revenue[i_revenue_index].getValue('custrecord_year_to_recognize_amount')
								});
							}
						}
					
						var a_revenue_cap_filter = [['custrecord_fp_rev_rec_others_projec', 'anyof', parseInt(a_project_search_results[i_proj_index].getId())]];
						
						var a_column = new Array();
						a_column[0] = new nlobjSearchColumn('created').setSort(true);
						a_column[1] = new nlobjSearchColumn('custrecord_fp_rev_rec_others_auto_numb');
						a_column[2] = new nlobjSearchColumn('custrecord_revenue_other_approval_status');
						
						var a_get_logged_in_user_exsiting_revenue_cap = nlapiSearchRecord('customrecord_fp_rev_rec_others_parent', null, a_revenue_cap_filter, a_column);
						if (a_get_logged_in_user_exsiting_revenue_cap)
						{
							var i_revenue_share_id = a_get_logged_in_user_exsiting_revenue_cap[0].getId();
							var i_revenue_share_status = a_get_logged_in_user_exsiting_revenue_cap[0].getValue('custrecord_revenue_other_approval_status');
							
							if (parseInt(i_revenue_share_status) == parseInt(2)) {
								nlapiLogExecution('audit', 'revenue share found:- ' + i_revenue_share_id);
								
								var a_effort_activity_mnth_end_filter = [['custrecord_fp_others_mnth_end_fp_parent', 'anyof', parseInt(i_revenue_share_id)], 'and', ['created', 'onorbefore', d_selected_mnth_last_date],'and',['custrecord_is_mnth_end_cfrmd','is','T']];
								var a_columns_mnth_end_effort_activity_srch = new Array();
								a_columns_mnth_end_effort_activity_srch[0] = new nlobjSearchColumn('created').setSort(true);
								
								var a_get_mnth_end_effrt_activity = nlapiSearchRecord('customrecord_fp_others_mnth_end_json', null, a_effort_activity_mnth_end_filter, a_columns_mnth_end_effort_activity_srch);
								if (a_get_mnth_end_effrt_activity) {
									var i_month_end_activity_id = a_get_mnth_end_effrt_activity[0].getId();
									nlapiLogExecution('audit', 'i_month_end_activity_id:- ' + i_month_end_activity_id);
									var projectWiseRevenue = [];
									
									var s_effrt_json = nlapiLookupField('customrecord_fp_others_mnth_end_json', i_month_end_activity_id, 'custrecord_fp_others_mnth_end_fld_json1');
									var s_effrt_json_2 = nlapiLookupField('customrecord_fp_others_mnth_end_json', i_month_end_activity_id, 'custrecord_fp_others_mnth_end_fld_json2');
									var s_effrt_json_3 = nlapiLookupField('customrecord_fp_others_mnth_end_json', i_month_end_activity_id, 'custrecord_fp_others_mnth_end_fld_json3');
									var s_effrt_json_4 = nlapiLookupField('customrecord_fp_others_mnth_end_json', i_month_end_activity_id, 'custrecord_fp_others_mnth_end_fld_json4');
									//added for JSON5
									var s_effrt_json_5 = nlapiLookupField('customrecord_fp_others_mnth_end_json', i_month_end_activity_id, 'custrecord_fp_others_mnth_end_fld_json5');
									
									var a_duplicate_sub_prac_count = new Array();
									var a_unique_list_practice_sublist = new Array();
									var a_unique_list_level = new Array();
									
									var a_subprac_searched_once = new Array();
									var a_subprac_searched_once_month = new Array();
									var a_subprac_searched_once_year = new Array();
									
									if (s_effrt_json) {
										var i_practice_previous = 0;
										var f_total_revenue_for_tenure = 0;
										//for (var i_mnth_end_plan = 0; i_mnth_end_plan < a_get_mnth_end_effrt_plan.length; i_mnth_end_plan++)
										var s_entire_json_clubed = JSON.parse(s_effrt_json);
										for (var i_mnth_end_plan = 0; i_mnth_end_plan < s_entire_json_clubed.length; i_mnth_end_plan++) {
											var a_row_json_data = JSON.parse(JSON.stringify(s_entire_json_clubed[i_mnth_end_plan]));
											for (var i_row_json_index = 0; i_row_json_index < a_row_json_data.length; i_row_json_index++) {
												if (a_unique_list_practice_sublist.indexOf(a_row_json_data[i_row_json_index].subprac) < 0) {
													a_unique_list_practice_sublist.push(a_row_json_data[i_row_json_index].subprac);
													a_duplicate_sub_prac_count.push({
														'prac': a_row_json_data[i_row_json_index].prac,
														'sub_prac': a_row_json_data[i_row_json_index].subprac,
														'sub_prac_name': a_row_json_data[i_row_json_index].subprac_text,
														'emp_role': a_row_json_data[i_row_json_index].role,
														'emp_level': a_row_json_data[i_row_json_index].level,
														'emp_level_text': a_row_json_data[i_row_json_index].level_text,
														'emp_cost': a_row_json_data[i_row_json_index].cost,
														'revenue_share': 50000,
														'no_count': 0
													});
												}
												
												var i_practice = a_row_json_data[i_row_json_index].prac;
												var s_practice = a_row_json_data[i_row_json_index].prac_text;
												var i_sub_practice = a_row_json_data[i_row_json_index].subprac;
												var s_sub_practice = a_row_json_data[i_row_json_index].subprac_text;
												var i_role = a_row_json_data[i_row_json_index].role;
												var s_role = a_row_json_data[i_row_json_index].role_text;
												var i_level = a_row_json_data[i_row_json_index].level;
												var s_level = a_row_json_data[i_row_json_index].level_text;
												var i_location = a_row_json_data[i_row_json_index].loc;
												var s_location = a_row_json_data[i_row_json_index].loc_text;
												var f_revenue = a_row_json_data[i_row_json_index].cost;
												if (!f_revenue) 
													f_revenue = 0;
												
												var f_revenue_share = a_row_json_data[i_row_json_index].share;
												if (!f_revenue_share) 
													f_revenue_share = 0;
												var pract_share_split=a_row_json_data[i_row_json_index].pract_share;
												if(!pract_share_split)
													pract_share_split=0;
												var mnt_flag=a_row_json_data[i_row_json_index].mnth_flag;
												var i_no_of_resources = a_row_json_data[i_row_json_index].allo;
												var s_mnth_strt_date = '1/31/2017';
												var s_mnth_end_date = '31/31/2017';
												var s_mnth = a_row_json_data[i_row_json_index].mnth;
												var s_year = a_row_json_data[i_row_json_index].year;
												
												var f_revenue_recognized = find_recognized_revenue(a_revenue_recognized_for_project,i_practice,i_sub_practice,i_role,i_level,s_mnth,s_year,a_subprac_searched_once,a_subprac_searched_once_month,a_subprac_searched_once_year);
												
												if (!f_revenue_recognized) 
													f_revenue_recognized = 0;
												
												var s_month_name = s_mnth + '_' + s_year;
												
												var total_revenue = parseFloat(i_no_of_resources) * parseFloat(f_revenue);
												total_revenue = parseFloat(total_revenue).toFixed(2);
												
												if (i_practice_previous == 0) {
													i_practice_previous = a_row_json_data[i_row_json_index].prac_row;
												}
												
												if (i_practice_previous != a_row_json_data[i_row_json_index].prac_row) {
													f_total_revenue_for_tenure = 0;
													i_practice_previous = a_row_json_data[i_row_json_index].prac_row;
												}
												
												f_total_revenue_for_tenure = parseFloat(f_total_revenue_for_tenure) + parseFloat(total_revenue);
												f_total_revenue_for_tenure = parseFloat(f_total_revenue_for_tenure).toFixed(2);
												
												var i_prac_subPrac_role_level = i_practice + '_' + i_sub_practice + '_' + i_role + '_' + i_level + '_' + i_location;
												
												if (!projectWiseRevenue[i_prac_subPrac_role_level]) {
													projectWiseRevenue[i_prac_subPrac_role_level] = {
														practice_name: s_practice,
														sub_prac_name: s_sub_practice,
														sub_practice: i_sub_practice,
														role_name: s_role,
														level_name: s_level,
														location_name: s_location,
														total_revenue_for_tenure: f_total_revenue_for_tenure,
														revenue_share: f_revenue_share,
														RevenueData: {}
													};
													
													projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice] = new yearData(i_year_project);
													
													projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].Amount = total_revenue;
													projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].prcnt_allocated = i_no_of_resources;
													projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].recognized_revenue = f_revenue_recognized;
													projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].mnth_strt = s_mnth_strt_date;
													projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].mnth_end = s_mnth_end_date;
													projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].revenue_share = f_revenue_share;
													projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].practice_share = pract_share_split;
													projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].f_mnth_flag = mnt_flag;
												}
												else {
													projectWiseRevenue[i_prac_subPrac_role_level].total_revenue_for_tenure = f_total_revenue_for_tenure;
													
													projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].Amount = total_revenue;
													projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].prcnt_allocated = i_no_of_resources;
													projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].recognized_revenue = f_revenue_recognized;
													projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].mnth_strt = s_mnth_strt_date;
													projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].mnth_end = s_mnth_end_date;
													projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].revenue_share = f_revenue_share;
													projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].practice_share = pract_share_split;
													projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].f_mnth_flag = mnt_flag;
												}
											}
										}
									}
									
									if (s_effrt_json_2) {
										var i_practice_previous = 0;
										var f_total_revenue_for_tenure = 0;
										//for (var i_mnth_end_plan = 0; i_mnth_end_plan < a_get_mnth_end_effrt_plan.length; i_mnth_end_plan++)
										var s_entire_json_clubed = JSON.parse(s_effrt_json_2);
										for (var i_mnth_end_plan = 0; i_mnth_end_plan < s_entire_json_clubed.length; i_mnth_end_plan++) {
											var a_row_json_data = JSON.parse(JSON.stringify(s_entire_json_clubed[i_mnth_end_plan]));
											for (var i_row_json_index = 0; i_row_json_index < a_row_json_data.length; i_row_json_index++) {
												if (a_unique_list_practice_sublist.indexOf(a_row_json_data[i_row_json_index].subprac) < 0) {
													a_unique_list_practice_sublist.push(a_row_json_data[i_row_json_index].subprac);
													a_duplicate_sub_prac_count.push({
														'prac': a_row_json_data[i_row_json_index].prac,
														'sub_prac': a_row_json_data[i_row_json_index].subprac,
														'sub_prac_name': a_row_json_data[i_row_json_index].subprac_text,
														'emp_role': a_row_json_data[i_row_json_index].role,
														'emp_level': a_row_json_data[i_row_json_index].level,
														'emp_level_text': a_row_json_data[i_row_json_index].level_text,
														'emp_cost': a_row_json_data[i_row_json_index].cost,
														'revenue_share': 50000,
														'no_count': 0
													});
												}
												
												var i_practice = a_row_json_data[i_row_json_index].prac;
												var s_practice = a_row_json_data[i_row_json_index].prac_text;
												var i_sub_practice = a_row_json_data[i_row_json_index].subprac;
												var s_sub_practice = a_row_json_data[i_row_json_index].subprac_text;
												var i_role = a_row_json_data[i_row_json_index].role;
												var s_role = a_row_json_data[i_row_json_index].role_text;
												var i_level = a_row_json_data[i_row_json_index].level;
												var s_level = a_row_json_data[i_row_json_index].level_text;
												var i_location = a_row_json_data[i_row_json_index].loc;
												var s_location = a_row_json_data[i_row_json_index].loc_text;
												var f_revenue = a_row_json_data[i_row_json_index].cost;
												if (!f_revenue) 
													f_revenue = 0;
												
												var f_revenue_share = a_row_json_data[i_row_json_index].share;
												if (!f_revenue_share) 
													f_revenue_share = 0;
												var pract_share_split=a_row_json_data[i_row_json_index].pract_share;
												if(!pract_share_split)
												pract_share_split=0;
												var mnt_flag=a_row_json_data[i_row_json_index].mnth_flag;
												//nlapiLogExecution('audit','f_revenue_recognized:- '+f_revenue_recognized);
												
												var i_no_of_resources = a_row_json_data[i_row_json_index].allo;
												var s_mnth_strt_date = '1/31/2017';
												var s_mnth_end_date = '31/31/2017';
												var s_mnth = a_row_json_data[i_row_json_index].mnth;
												var s_year = a_row_json_data[i_row_json_index].year;
												
												var f_revenue_recognized = 0;
												var f_revenue_recognized = find_recognized_revenue(a_revenue_recognized_for_project,i_practice,i_sub_practice,i_role,i_level,s_mnth,s_year,a_subprac_searched_once,a_subprac_searched_once_month,a_subprac_searched_once_year);
												
												if (!f_revenue_recognized) 
													f_revenue_recognized = 0;
												
												var s_month_name = s_mnth + '_' + s_year;
												
												var total_revenue = parseFloat(i_no_of_resources) * parseFloat(f_revenue);
												total_revenue = parseFloat(total_revenue).toFixed(2);
												
												if (i_practice_previous == 0) {
													i_practice_previous = a_row_json_data[i_row_json_index].prac_row;
												}
												
												if (i_practice_previous != a_row_json_data[i_row_json_index].prac_row) {
													f_total_revenue_for_tenure = 0;
													i_practice_previous = a_row_json_data[i_row_json_index].prac_row;
												}
												
												f_total_revenue_for_tenure = parseFloat(f_total_revenue_for_tenure) + parseFloat(total_revenue);
												f_total_revenue_for_tenure = parseFloat(f_total_revenue_for_tenure).toFixed(2);
												
												var i_prac_subPrac_role_level = i_practice + '_' + i_sub_practice + '_' + i_role + '_' + i_level + '_' + i_location;
												
												if (!projectWiseRevenue[i_prac_subPrac_role_level]) {
													projectWiseRevenue[i_prac_subPrac_role_level] = {
														practice_name: s_practice,
														sub_prac_name: s_sub_practice,
														sub_practice: i_sub_practice,
														role_name: s_role,
														level_name: s_level,
														location_name: s_location,
														total_revenue_for_tenure: f_total_revenue_for_tenure,
														revenue_share: f_revenue_share,
														RevenueData: {}
													};
													
													projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice] = new yearData(i_year_project);
													
													projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].Amount = total_revenue;
													projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].prcnt_allocated = i_no_of_resources;
													projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].recognized_revenue = f_revenue_recognized;
													projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].mnth_strt = s_mnth_strt_date;
													projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].mnth_end = s_mnth_end_date;
													projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].revenue_share = f_revenue_share;
													projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].practice_share = pract_share_split;
													projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].f_mnth_flag = mnt_flag;
												}
												else {
													projectWiseRevenue[i_prac_subPrac_role_level].total_revenue_for_tenure = f_total_revenue_for_tenure;
													
													projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].Amount = total_revenue;
													projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].prcnt_allocated = i_no_of_resources;
													projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].recognized_revenue = f_revenue_recognized;
													projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].mnth_strt = s_mnth_strt_date;
													projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].mnth_end = s_mnth_end_date;
													projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].revenue_share = f_revenue_share;
													projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].practice_share = pract_share_split;
													projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].f_mnth_flag = mnt_flag;
												}
											}
										}
									}
									
									if (s_effrt_json_3) {
										var i_practice_previous = 0;
										var f_total_revenue_for_tenure = 0;
										//for (var i_mnth_end_plan = 0; i_mnth_end_plan < a_get_mnth_end_effrt_plan.length; i_mnth_end_plan++)
										var s_entire_json_clubed = JSON.parse(s_effrt_json_3);
										for (var i_mnth_end_plan = 0; i_mnth_end_plan < s_entire_json_clubed.length; i_mnth_end_plan++) {
											var a_row_json_data = JSON.parse(JSON.stringify(s_entire_json_clubed[i_mnth_end_plan]));
											for (var i_row_json_index = 0; i_row_json_index < a_row_json_data.length; i_row_json_index++) {
												if (a_unique_list_practice_sublist.indexOf(a_row_json_data[i_row_json_index].subprac) < 0) {
													a_unique_list_practice_sublist.push(a_row_json_data[i_row_json_index].subprac);
													a_duplicate_sub_prac_count.push({
														'prac': a_row_json_data[i_row_json_index].prac,
														'sub_prac': a_row_json_data[i_row_json_index].subprac,
														'sub_prac_name': a_row_json_data[i_row_json_index].subprac_text,
														'emp_role': a_row_json_data[i_row_json_index].role,
														'emp_level': a_row_json_data[i_row_json_index].level,
														'emp_level_text': a_row_json_data[i_row_json_index].level_text,
														'emp_cost': a_row_json_data[i_row_json_index].cost,
														'revenue_share': 50000,
														'no_count': 0
													});
												}
												
												var i_practice = a_row_json_data[i_row_json_index].prac;
												var s_practice = a_row_json_data[i_row_json_index].prac_text;
												var i_sub_practice = a_row_json_data[i_row_json_index].subprac;
												var s_sub_practice = a_row_json_data[i_row_json_index].subprac_text;
												var i_role = a_row_json_data[i_row_json_index].role;
												var s_role = a_row_json_data[i_row_json_index].role_text;
												var i_level = a_row_json_data[i_row_json_index].level;
												var s_level = a_row_json_data[i_row_json_index].level_text;
												var i_location = a_row_json_data[i_row_json_index].loc;
												var s_location = a_row_json_data[i_row_json_index].loc_text;
												var f_revenue = a_row_json_data[i_row_json_index].cost;
												if (!f_revenue) 
													f_revenue = 0;
												
												var f_revenue_share = a_row_json_data[i_row_json_index].share;
												if (!f_revenue_share) 
													f_revenue_share = 0;
												var pract_share_split=a_row_json_data[i_row_json_index].pract_share;
												if(!pract_share_split)
													pract_share_split=0;
												var mnt_flag=a_row_json_data[i_row_json_index].mnth_flag;
												//nlapiLogExecution('audit','f_revenue_recognized:- '+f_revenue_recognized);
												
												var i_no_of_resources = a_row_json_data[i_row_json_index].allo;
												var s_mnth_strt_date = '1/31/2017';
												var s_mnth_end_date = '31/31/2017';
												var s_mnth = a_row_json_data[i_row_json_index].mnth;
												var s_year = a_row_json_data[i_row_json_index].year;
												
												var f_revenue_recognized = 0;
												var f_revenue_recognized = find_recognized_revenue(a_revenue_recognized_for_project,i_practice,i_sub_practice,i_role,i_level,s_mnth,s_year,a_subprac_searched_once,a_subprac_searched_once_month,a_subprac_searched_once_year);
												
												if (!f_revenue_recognized) 
													f_revenue_recognized = 0;
												
												var s_month_name = s_mnth + '_' + s_year;
												
												var total_revenue = parseFloat(i_no_of_resources) * parseFloat(f_revenue);
												total_revenue = parseFloat(total_revenue).toFixed(2);
												
												if (i_practice_previous == 0) {
													i_practice_previous = a_row_json_data[i_row_json_index].prac_row;
												}
												
												if (i_practice_previous != a_row_json_data[i_row_json_index].prac_row) {
													f_total_revenue_for_tenure = 0;
													i_practice_previous = a_row_json_data[i_row_json_index].prac_row;
												}
												
												f_total_revenue_for_tenure = parseFloat(f_total_revenue_for_tenure) + parseFloat(total_revenue);
												f_total_revenue_for_tenure = parseFloat(f_total_revenue_for_tenure).toFixed(2);
												
												var i_prac_subPrac_role_level = i_practice + '_' + i_sub_practice + '_' + i_role + '_' + i_level + '_' + i_location;
												
												if (!projectWiseRevenue[i_prac_subPrac_role_level]) {
													projectWiseRevenue[i_prac_subPrac_role_level] = {
														practice_name: s_practice,
														sub_prac_name: s_sub_practice,
														sub_practice: i_sub_practice,
														role_name: s_role,
														level_name: s_level,
														location_name: s_location,
														total_revenue_for_tenure: f_total_revenue_for_tenure,
														revenue_share: f_revenue_share,
														RevenueData: {}
													};
													
													projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice] = new yearData(i_year_project);
													
													projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].Amount = total_revenue;
													projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].prcnt_allocated = i_no_of_resources;
													projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].recognized_revenue = f_revenue_recognized;
													projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].mnth_strt = s_mnth_strt_date;
													projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].mnth_end = s_mnth_end_date;
													projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].revenue_share = f_revenue_share;
													projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].practice_share = pract_share_split;
													projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].f_mnth_flag = mnt_flag;
												}
												else {
													projectWiseRevenue[i_prac_subPrac_role_level].total_revenue_for_tenure = f_total_revenue_for_tenure;
													
													projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].Amount = total_revenue;
													projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].prcnt_allocated = i_no_of_resources;
													projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].recognized_revenue = f_revenue_recognized;
													projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].mnth_strt = s_mnth_strt_date;
													projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].mnth_end = s_mnth_end_date;
													projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].revenue_share = f_revenue_share;
													projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].practice_share = pract_share_split;
													projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].f_mnth_flag = mnt_flag;
												}
											}
										}
									}
									
									if (s_effrt_json_4) {
										var i_practice_previous = 0;
										var f_total_revenue_for_tenure = 0;
										//for (var i_mnth_end_plan = 0; i_mnth_end_plan < a_get_mnth_end_effrt_plan.length; i_mnth_end_plan++)
										var s_entire_json_clubed = JSON.parse(s_effrt_json_4);
										for (var i_mnth_end_plan = 0; i_mnth_end_plan < s_entire_json_clubed.length; i_mnth_end_plan++) {
											var a_row_json_data = JSON.parse(JSON.stringify(s_entire_json_clubed[i_mnth_end_plan]));
											for (var i_row_json_index = 0; i_row_json_index < a_row_json_data.length; i_row_json_index++) {
												if (a_unique_list_practice_sublist.indexOf(a_row_json_data[i_row_json_index].subprac) < 0) {
													a_unique_list_practice_sublist.push(a_row_json_data[i_row_json_index].subprac);
													a_duplicate_sub_prac_count.push({
														'prac': a_row_json_data[i_row_json_index].prac,
														'sub_prac': a_row_json_data[i_row_json_index].subprac,
														'sub_prac_name': a_row_json_data[i_row_json_index].subprac_text,
														'emp_role': a_row_json_data[i_row_json_index].role,
														'emp_level': a_row_json_data[i_row_json_index].level,
														'emp_level_text': a_row_json_data[i_row_json_index].level_text,
														'emp_cost': a_row_json_data[i_row_json_index].cost,
														'revenue_share': 50000,
														'no_count': 0
													});
												}
												
												var i_practice = a_row_json_data[i_row_json_index].prac;
												var s_practice = a_row_json_data[i_row_json_index].prac_text;
												var i_sub_practice = a_row_json_data[i_row_json_index].subprac;
												var s_sub_practice = a_row_json_data[i_row_json_index].subprac_text;
												var i_role = a_row_json_data[i_row_json_index].role;
												var s_role = a_row_json_data[i_row_json_index].role_text;
												var i_level = a_row_json_data[i_row_json_index].level;
												var s_level = a_row_json_data[i_row_json_index].level_text;
												var i_location = a_row_json_data[i_row_json_index].loc;
												var s_location = a_row_json_data[i_row_json_index].loc_text;
												var f_revenue = a_row_json_data[i_row_json_index].cost;
												if (!f_revenue) 
													f_revenue = 0;
												
												var f_revenue_share = a_row_json_data[i_row_json_index].share;
												if (!f_revenue_share) 
													f_revenue_share = 0;
												var pract_share_split=a_row_json_data[i_row_json_index].pract_share;
												if(!pract_share_split)
													pract_share_split=0;
												var mnt_flag=a_row_json_data[i_row_json_index].mnth_flag;
												//nlapiLogExecution('audit','f_revenue_recognized:- '+f_revenue_recognized);
												
												var i_no_of_resources = a_row_json_data[i_row_json_index].allo;
												var s_mnth_strt_date = '1/31/2017';
												var s_mnth_end_date = '31/31/2017';
												var s_mnth = a_row_json_data[i_row_json_index].mnth;
												var s_year = a_row_json_data[i_row_json_index].year;
												
												var f_revenue_recognized = 0;
												var f_revenue_recognized = find_recognized_revenue(a_revenue_recognized_for_project,i_practice,i_sub_practice,i_role,i_level,s_mnth,s_year,a_subprac_searched_once,a_subprac_searched_once_month,a_subprac_searched_once_year);
												
												if (!f_revenue_recognized) 
													f_revenue_recognized = 0;
												
												var s_month_name = s_mnth + '_' + s_year;
												
												var total_revenue = parseFloat(i_no_of_resources) * parseFloat(f_revenue);
												total_revenue = parseFloat(total_revenue).toFixed(2);
												
												if (i_practice_previous == 0) {
													i_practice_previous = a_row_json_data[i_row_json_index].prac_row;
												}
												
												if (i_practice_previous != a_row_json_data[i_row_json_index].prac_row) {
													f_total_revenue_for_tenure = 0;
													i_practice_previous = a_row_json_data[i_row_json_index].prac_row;
												}
												
												f_total_revenue_for_tenure = parseFloat(f_total_revenue_for_tenure) + parseFloat(total_revenue);
												f_total_revenue_for_tenure = parseFloat(f_total_revenue_for_tenure).toFixed(2);
												
												var i_prac_subPrac_role_level = i_practice + '_' + i_sub_practice + '_' + i_role + '_' + i_level + '_' + i_location;
												
												if (!projectWiseRevenue[i_prac_subPrac_role_level]) {
													projectWiseRevenue[i_prac_subPrac_role_level] = {
														practice_name: s_practice,
														sub_prac_name: s_sub_practice,
														sub_practice: i_sub_practice,
														role_name: s_role,
														level_name: s_level,
														location_name: s_location,
														total_revenue_for_tenure: f_total_revenue_for_tenure,
														revenue_share: f_revenue_share,
														RevenueData: {}
													};
													
													projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice] = new yearData(i_year_project);
													
													projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].Amount = total_revenue;
													projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].prcnt_allocated = i_no_of_resources;
													projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].recognized_revenue = f_revenue_recognized;
													projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].mnth_strt = s_mnth_strt_date;
													projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].mnth_end = s_mnth_end_date;
													projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].revenue_share = f_revenue_share;
													projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].practice_share = pract_share_split;
													projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].f_mnth_flag = mnt_flag;
												}
												else {
													projectWiseRevenue[i_prac_subPrac_role_level].total_revenue_for_tenure = f_total_revenue_for_tenure;
													
													projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].Amount = total_revenue;
													projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].prcnt_allocated = i_no_of_resources;
													projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].recognized_revenue = f_revenue_recognized;
													projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].mnth_strt = s_mnth_strt_date;
													projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].mnth_end = s_mnth_end_date;
													projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].revenue_share = f_revenue_share;
													projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].practice_share = pract_share_split;
													projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].f_mnth_flag = mnt_flag;
												}
											}
										}
									}
									//added for JSON5
									if (s_effrt_json_5) {
										var i_practice_previous = 0;
										var f_total_revenue_for_tenure = 0;
										//for (var i_mnth_end_plan = 0; i_mnth_end_plan < a_get_mnth_end_effrt_plan.length; i_mnth_end_plan++)
										var s_entire_json_clubed = JSON.parse(s_effrt_json_5);
										for (var i_mnth_end_plan = 0; i_mnth_end_plan < s_entire_json_clubed.length; i_mnth_end_plan++) {
											var a_row_json_data = JSON.parse(JSON.stringify(s_entire_json_clubed[i_mnth_end_plan]));
											for (var i_row_json_index = 0; i_row_json_index < a_row_json_data.length; i_row_json_index++) {
												if (a_unique_list_practice_sublist.indexOf(a_row_json_data[i_row_json_index].subprac) < 0) {
													a_unique_list_practice_sublist.push(a_row_json_data[i_row_json_index].subprac);
													a_duplicate_sub_prac_count.push({
														'prac': a_row_json_data[i_row_json_index].prac,
														'sub_prac': a_row_json_data[i_row_json_index].subprac,
														'sub_prac_name': a_row_json_data[i_row_json_index].subprac_text,
														'emp_role': a_row_json_data[i_row_json_index].role,
														'emp_level': a_row_json_data[i_row_json_index].level,
														'emp_level_text': a_row_json_data[i_row_json_index].level_text,
														'emp_cost': a_row_json_data[i_row_json_index].cost,
														'revenue_share': 50000,
														'no_count': 0
													});
												}
												
												var i_practice = a_row_json_data[i_row_json_index].prac;
												var s_practice = a_row_json_data[i_row_json_index].prac_text;
												var i_sub_practice = a_row_json_data[i_row_json_index].subprac;
												var s_sub_practice = a_row_json_data[i_row_json_index].subprac_text;
												var i_role = a_row_json_data[i_row_json_index].role;
												var s_role = a_row_json_data[i_row_json_index].role_text;
												var i_level = a_row_json_data[i_row_json_index].level;
												var s_level = a_row_json_data[i_row_json_index].level_text;
												var i_location = a_row_json_data[i_row_json_index].loc;
												var s_location = a_row_json_data[i_row_json_index].loc_text;
												var f_revenue = a_row_json_data[i_row_json_index].cost;
												if (!f_revenue) 
													f_revenue = 0;
												
												var f_revenue_share = a_row_json_data[i_row_json_index].share;
												if (!f_revenue_share) 
													f_revenue_share = 0;
												var pract_share_split=a_row_json_data[i_row_json_index].pract_share;
												if(!pract_share_split)
													pract_share_split=0;
												var mnt_flag=a_row_json_data[i_row_json_index].mnth_flag;
												//nlapiLogExecution('audit','f_revenue_recognized:- '+f_revenue_recognized);
												
												var i_no_of_resources = a_row_json_data[i_row_json_index].allo;
												var s_mnth_strt_date = '1/31/2017';
												var s_mnth_end_date = '31/31/2017';
												var s_mnth = a_row_json_data[i_row_json_index].mnth;
												var s_year = a_row_json_data[i_row_json_index].year;
												
												var f_revenue_recognized = 0;
												var f_revenue_recognized = find_recognized_revenue(a_revenue_recognized_for_project,i_practice,i_sub_practice,i_role,i_level,s_mnth,s_year,a_subprac_searched_once,a_subprac_searched_once_month,a_subprac_searched_once_year);
												
												if (!f_revenue_recognized) 
													f_revenue_recognized = 0;
												
												var s_month_name = s_mnth + '_' + s_year;
												
												var total_revenue = parseFloat(i_no_of_resources) * parseFloat(f_revenue);
												total_revenue = parseFloat(total_revenue).toFixed(2);
												
												if (i_practice_previous == 0) {
													i_practice_previous = a_row_json_data[i_row_json_index].prac_row;
												}
												
												if (i_practice_previous != a_row_json_data[i_row_json_index].prac_row) {
													f_total_revenue_for_tenure = 0;
													i_practice_previous = a_row_json_data[i_row_json_index].prac_row;
												}
												
												f_total_revenue_for_tenure = parseFloat(f_total_revenue_for_tenure) + parseFloat(total_revenue);
												f_total_revenue_for_tenure = parseFloat(f_total_revenue_for_tenure).toFixed(2);
												
												var i_prac_subPrac_role_level = i_practice + '_' + i_sub_practice + '_' + i_role + '_' + i_level + '_' + i_location;
												
												if (!projectWiseRevenue[i_prac_subPrac_role_level]) {
													projectWiseRevenue[i_prac_subPrac_role_level] = {
														practice_name: s_practice,
														sub_prac_name: s_sub_practice,
														sub_practice: i_sub_practice,
														role_name: s_role,
														level_name: s_level,
														location_name: s_location,
														total_revenue_for_tenure: f_total_revenue_for_tenure,
														revenue_share: f_revenue_share,
														RevenueData: {}
													};
													
													projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice] = new yearData(i_year_project);
													
													projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].Amount = total_revenue;
													projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].prcnt_allocated = i_no_of_resources;
													projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].recognized_revenue = f_revenue_recognized;
													projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].mnth_strt = s_mnth_strt_date;
													projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].mnth_end = s_mnth_end_date;
													projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].revenue_share = f_revenue_share;
													projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].practice_share = pract_share_split;
													projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].f_mnth_flag = mnt_flag;
												}
												else {
													projectWiseRevenue[i_prac_subPrac_role_level].total_revenue_for_tenure = f_total_revenue_for_tenure;
													
													projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].Amount = total_revenue;
													projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].prcnt_allocated = i_no_of_resources;
													projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].recognized_revenue = f_revenue_recognized;
													projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].mnth_strt = s_mnth_strt_date;
													projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].mnth_end = s_mnth_end_date;
													projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].revenue_share = f_revenue_share;
													projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].practice_share = pract_share_split;
													projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].f_mnth_flag = mnt_flag;
												}
											}
										}
									}
									
									// Get Cost to be recognized for each sub practice
									var a_practice_nt_processed = new Array();
									var a_index_prac_nt_processed = new Array();
									
									//for(var i_allo_index=0; i_allo_index<a_allocation_details.length; i_allo_index++)
									{
									for (var i_dupli = 0; i_dupli < a_duplicate_sub_prac_count.length; i_dupli++)
									{
										var f_total_cost_breakUp = generate_resource_cost_total(projectWiseRevenue,monthBreakUp,i_year_project,i_project_mnth,2,a_duplicate_sub_prac_count[i_dupli].sub_prac,s_currency_symbol_proj,s_currency_symbol_usd,i_year_project_end,i_prject_end_mnth);
									}
										for (var i_dupli = 0; i_dupli < a_duplicate_sub_prac_count.length; i_dupli++) {
											if (o_context.getRemainingUsage() <= 1000) {
												nlapiYieldScript();
											}
											
											var f_actual_revenue_to_recognize = generate_cost_view_other(projectWiseRevenue, monthBreakUp, i_year_project, i_project_mnth, 2, a_duplicate_sub_prac_count[i_dupli].sub_prac, s_currency_symbol_proj, s_currency_symbol_usd, i_year_project_end, i_prject_end_mnth,f_total_cost_breakUp,s_currentMonthName,s_currentYear,i_project_sow_value,customer_lookup);
											nlapiLogExecution('audit', 'json to process:- ' + f_actual_revenue_to_recognize, 'level:-- ' + a_duplicate_sub_prac_count[i_dupli].emp_level_text);
											
											f_actual_revenue_to_recognize = f_actual_revenue_to_recognize.split('##');
											var f_amount_to_recognize = f_actual_revenue_to_recognize[0];
											var f_amount_sub_total = f_actual_revenue_to_recognize[1];
											
											if (f_amount_sub_total == 0) {
												f_amount_sub_total = 1;
											}
											
											var f_resource_cost = a_duplicate_sub_prac_count[i_dupli].emp_cost;
											nlapiLogExecution('audit', 'f_amount_to_recognize:- ' + f_amount_to_recognize + ':::: f_amount_sub_total:- ' + f_amount_sub_total, 'f_resource_cost:- ' + f_resource_cost);
											
											{
												if (a_subpractice_ytd_created.indexOf(a_duplicate_sub_prac_count[i_dupli].sub_prac) < 0  && active_practices.indexOf(a_duplicate_sub_prac_count[i_dupli].sub_prac) >= 0 && active_practices.indexOf(a_duplicate_sub_prac_count[i_dupli].prac)>=0) {
													var o_ytd_rcrd = nlapiCreateRecord('customrecord_fp_other_validate_excel_dat');
													o_ytd_rcrd.setFieldValue('custrecord_fp_other_validate_project', a_project_search_results[i_proj_index].getId());
													o_ytd_rcrd.setFieldValue('custrecord_fp_other_validate_practice', a_duplicate_sub_prac_count[i_dupli].prac);
													o_ytd_rcrd.setFieldValue('custrecord_fp_othr_validate_sub_pract', a_duplicate_sub_prac_count[i_dupli].sub_prac);
													o_ytd_rcrd.setFieldValue('custrecord_fp_othr_validate_role', a_duplicate_sub_prac_count[i_dupli].emp_role);
													o_ytd_rcrd.setFieldValue('custrecord_fp_othr_validate_level', a_duplicate_sub_prac_count[i_dupli].emp_level);
													o_ytd_rcrd.setFieldValue('custrecord_fp_othr_validate_rev_amnt', parseFloat(f_amount_to_recognize));
												//	o_ytd_rcrd.setFieldValue('custrecord_subtotal_amount', f_amount_sub_total);
													o_ytd_rcrd.setFieldValue('custrecord_fp_othr_validate_mnth', s_currentMonthName);
													o_ytd_rcrd.setFieldValue('custrecord_fp_othr_validate_year', s_currentYear);
													o_ytd_rcrd.setFieldValue('custrecord_fp_othr_proj_type', i_proj_revenue_rec_type);
													o_ytd_rcrd.setFieldValue('custrecord_validate_subsi', subsi);
													//o_ytd_rcrd.setFieldValue('custrecord_resource_name',a_allocation_details[i_allo_index].i_resource_allo_text);
												var i_ytd_rcrd_id = nlapiSubmitRecord(o_ytd_rcrd, true, true);
													
													a_subpractice_ytd_created.push(a_duplicate_sub_prac_count[i_dupli].sub_prac);
												}
											}
										}
									}
								}
							}
						}
					}
					}
				}
			
		var a_fld_arr = new Array();
			a_fld_arr[0] = 'custrecord_journal_entries_fp_rev_rec';
			a_fld_arr[1] = 'custrecord_status_fp_rev_rce';
			a_fld_arr[2] = 'custrecord_validate_data_export';
			
			var a_fld_values = new Array();
			a_fld_values[0] = '';
			a_fld_values[1] = 4;
			a_fld_values[2] = 'T';
			
			nlapiSubmitField('customrecord_fp_rev_rec_je_creation',i_fp_revrec_id,a_fld_arr,a_fld_values);
			
			
		}
	
	
	}
	catch(err)
	{
		nlapiLogExecution('ERROR','schedule_createJE','ERROR:- '+err);
	}
}

function getDatediffIndays(startDate, endDate)
{
    var one_day=1000*60*60*24;
    var fromDate = startDate;
    var toDate=endDate

	//var date1 = nlapiStringToDate(fromDate);

   //var date2 = nlapiStringToDate(toDate);
	
	var date1 = fromDate;
	var date2 = toDate;
	
    var date3=Math.round((date2-date1)/one_day);

    return (date3+1);
}

function find_level_present_fr_practice(a_unique_list_level,subprac,level)
{
	var i_level_found_index = -1;
	for(var i_level_index=0; i_level_index<a_unique_list_level.length; i_level_index++)
	{
		if(a_unique_list_level[i_level_index].asso_sub_prac == subprac)
		{
			if(a_unique_list_level[i_level_index].asso_level == level)
			{
				i_level_found_index = i_level_index;
			}
		}
	}
	
	return i_level_found_index;
}
function generate_cost_view_other(projectWiseRevenue,monthBreakUp,i_year_project,i_project_mnth,mode,sub_prac,s_currency_symbol_proj,s_currency_symbol_usd,i_year_project_end,i_prject_end_mnth,f_total_cost_breakUp,s_currentMonthName,s_currentYear,i_project_sow_value,customer_lookup)
{
	var a_sub_prac_already_displayed = new Array();
	var i_subpractice_id = 0;
	var a_amount = new Array();
	var a_recognized_revenue = new Array();
	var i_frst_row = 0;
	var i_diplay_frst_column = 0;
	var i_total_row_revenue = 0;
	var i_total_per_row = 0;
	var total_prcnt_aloocated = new Array();
	var b_first_line_copied = 0;
	var f_total_allocated_row = 0;
	var revenue_share_arr=new Array();
	var practice_share_arr=new Array();
	var mnth_flag_arr=new Array();
	var d_today_date = new Date();
	var i_current_month=mnth_array.indexOf(s_currentMonthName);
	
	//var i_current_month = d_today_date.getMonth();
	var i_current_year = d_today_date.getFullYear();
	
	for (var emp_internal_id in projectWiseRevenue)
	{
		if (projectWiseRevenue[emp_internal_id].sub_practice == sub_prac)
		{
			f_total_allocated_row = 0;
			for (var project_internal_id in projectWiseRevenue[emp_internal_id].RevenueData)
			{
				var i_index_mnth = 0;
				for (var month in projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id])
				{
					var a_MonthNames = MonthNames(i_year_project);
					var currentMonthPos = a_MonthNames.indexOf(month);
					var s_current_month = a_MonthNames[currentMonthPos];
					var s_current_mnth_yr = s_current_month.substr(4, 8);
					
					var i_nxt_yr_flg = 0;
					if(currentMonthPos >=12)
					{
						currentMonthPos = currentMonthPos - 12;
						i_nxt_yr_flg = 1;
					}
					
					var i_curr_yr_dis_flag = 0;
					if(parseInt(s_current_mnth_yr) < i_year_project_end && i_prject_end_mnth < currentMonthPos)
					{
						i_curr_yr_dis_flag = 1;
					}
					
					if (((i_project_mnth <= currentMonthPos && ((i_prject_end_mnth >= currentMonthPos || i_curr_yr_dis_flag == 1) && i_nxt_yr_flg == 0) && parseInt(s_current_mnth_yr) <= i_year_project) && parseInt(s_current_mnth_yr) <= i_year_project_end) || (i_prject_end_mnth >= currentMonthPos && parseInt(s_current_mnth_yr) <= i_year_project_end && i_nxt_yr_flg == 1))
					{
						//nlapiLogExecution('audit','currentMonthPos:- ',+currentMonthPos);
						if(b_first_line_copied == 1)
						{
							var f_allocated_prcnt = total_prcnt_aloocated[i_index_mnth];
							f_allocated_prcnt = parseFloat(f_allocated_prcnt) + parseFloat(projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id][month].prcnt_allocated);
							total_prcnt_aloocated[i_index_mnth] = f_allocated_prcnt;
							i_index_mnth++;							
						}
						else
						{
							total_prcnt_aloocated.push(projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id][month].prcnt_allocated);
						}
						
						f_total_allocated_row = parseFloat(projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id][month].prcnt_allocated) + parseFloat(f_total_allocated_row);
						
					}
				}
			}
			
			b_first_line_copied = 1;
			
			var s_sub_prac_name = projectWiseRevenue[emp_internal_id].sub_prac_name;
			var s_practice_name = projectWiseRevenue[emp_internal_id].practice_name;
			
			i_diplay_frst_column = 1;
		}
	}
	i_diplay_frst_column = 0;
	
	var f_total_row_allocation = 0;
	for(var i_index_total_allocated=0; i_index_total_allocated<total_prcnt_aloocated.length; i_index_total_allocated++)
	{
		f_total_row_allocation = parseFloat(f_total_row_allocation) + parseFloat(total_prcnt_aloocated[i_index_total_allocated]);
	}
	
	for ( var emp_internal_id in projectWiseRevenue) {
		
		for ( var project_internal_id in projectWiseRevenue[emp_internal_id].RevenueData) {
			
			if(projectWiseRevenue[emp_internal_id].sub_practice == sub_prac)
			{	
				var i_sub_practice_internal_id = 0;
				var i_amt = 0;
				for (var month in projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id])
				{
					var a_MonthNames = MonthNames(i_year_project);
					var currentMonthPos = a_MonthNames.indexOf(month);
					var s_current_month = a_MonthNames[currentMonthPos];
					var s_current_mnth_yr = s_current_month.substr(4, 8);
					
					var i_nxt_yr_flg = 0;
					if(currentMonthPos >=12)
					{
						currentMonthPos = currentMonthPos - 12;
						i_nxt_yr_flg = 1;
					}
					
					var i_curr_yr_dis_flag = 0;
					if(parseInt(s_current_mnth_yr) < i_year_project_end && i_prject_end_mnth < currentMonthPos)
					{
						i_curr_yr_dis_flag = 1;
					}
					
					if (((i_project_mnth <= currentMonthPos && ((i_prject_end_mnth >= currentMonthPos || i_curr_yr_dis_flag == 1) && i_nxt_yr_flg == 0) && parseInt(s_current_mnth_yr) <= i_year_project) && parseInt(s_current_mnth_yr) <= i_year_project_end) || (i_prject_end_mnth >= currentMonthPos && parseInt(s_current_mnth_yr) <= i_year_project_end && i_nxt_yr_flg == 1))
					{
						if (mode == 2)
						{
							var i_mnth_posi = parseFloat(i_current_month) - parseFloat(i_project_mnth);
							var total_revenue_format = parseFloat(projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id][month].Amount).toFixed(1);
							
							if(i_frst_row == 0)
							{
								//nlapiLogExecution('audit','inside push amnt');
								a_amount.push(total_revenue_format);
								
								//if(i_amt < i_mnth_posi-2)
									a_recognized_revenue.push(projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id][month].recognized_revenue);
								revenue_share_arr.push(parseFloat(projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id][month].revenue_share));
								practice_share_arr.push(parseFloat(projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id][month].practice_share));
								mnth_flag_arr.push(parseFloat(projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id][month].f_mnth_flag));
							}
							else
							{
								var amnt_mnth = a_amount[i_amt];
								amnt_mnth = parseFloat(amnt_mnth) + parseFloat(projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id][month].Amount);
								
								a_amount[i_amt] = amnt_mnth;
								revenue_share_arr[i_amt]=parseFloat(projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id][month].revenue_share);
								practice_share_arr[i_amt]=(parseFloat(projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id][month].practice_share));
								mnth_flag_arr[i_amt]=(parseFloat(projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id][month].f_mnth_flag));
								var i_existing_recognised_amount = a_recognized_revenue[i_amt];
								i_existing_recognised_amount = parseFloat(i_existing_recognised_amount) + parseFloat(projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id][month].recognized_revenue);
								
								//if(i_amt < i_mnth_posi-2)
									a_recognized_revenue[i_amt] = i_existing_recognised_amount;
								
								i_amt++;
							}
							//nlapiLogExecution('audit','monthly amount:- '+parseFloat(projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id][month].Amount));
							i_total_row_revenue = parseFloat(i_total_row_revenue) + parseFloat(projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id][month].Amount);
							i_total_per_row = parseFloat(i_total_per_row) + parseFloat(projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id][month].Amount);
						}
						
					}
				}
				
				if (mode == 2)
				{
					i_total_per_row = 0;
					//i_total_row_revenue = parseFloat(i_total_row_revenue) + parseFloat(projectWiseRevenue[emp_internal_id].total_revenue_for_tenure);
				}
				
				i_frst_row = 1;
	
				i_diplay_frst_column = 1;
				
				var f_revenue_share = projectWiseRevenue[emp_internal_id].revenue_share;
			}			
		}
	}
	
	var f_total_revenue = 0;
	var f_revenue_amount_till_current_month = 0;
	var f_tot_revenue_till_current_month=0;
	for (var i_amt = 0; i_amt < a_amount.length; i_amt++)
	{
		var f_prcnt_revenue = parseFloat(a_amount[i_amt]) /parseFloat(f_total_cost_breakUp[i_amt]);
		f_prcnt_revenue = f_prcnt_revenue * 100;
		if((mnth_flag_arr[i_amt]==1) && (a_amount[i_amt] != 0))
		{
			var f_revenue_amount = practice_share_arr[i_amt];
		}
		else
		{
			var f_revenue_amount = (parseFloat(f_prcnt_revenue) * parseFloat(revenue_share_arr[i_amt])) / 100;
		}
		//var f_revenue_amount = (parseFloat(f_prcnt_revenue) * parseFloat(f_revenue_share)) / 100;
		if(!f_revenue_amount)
			f_revenue_amount = 0;
			
		if (parseInt(i_year_project) != parseInt(i_current_year))
		{
          var i_current_month_from_module = i_current_month;
          	var i_current_month_pr = d_today_date.getMonth();
			var i_total_project_tenure = 11 + parseInt(i_current_month_pr);
			var f_total_prev_mnths = parseFloat(i_total_project_tenure) - parseFloat(i_project_mnth);
          //Added for the revenue calculation on next month
         if(i_current_month_from_module == i_current_month_pr)
			f_total_prev_mnths = parseFloat(f_total_prev_mnths) + parseFloat(1);
		}
		else
		{
			var f_total_prev_mnths = parseFloat(i_current_month) - parseFloat(i_project_mnth);
			//f_total_prev_mnths = parseFloat(f_total_prev_mnths) - parseFloat(1);
		}
		
		if(i_amt <= f_total_prev_mnths)
		{
			f_revenue_amount_till_current_month = parseFloat(f_revenue_amount);
			//nlapiLogExecution('audit','f_revenue_amount:- '+f_revenue_amount,'a_amount.length:- '+a_amount.length);
			
		}
		if(i_amt < f_total_prev_mnths)
		{
			f_tot_revenue_till_current_month = parseFloat(f_revenue_amount) + parseFloat(f_tot_revenue_till_current_month);
						
		}
		
		f_total_revenue = parseFloat(f_revenue_amount) + parseFloat(f_total_revenue);
		
	}
	
	for (var emp_internal_id in projectWiseRevenue)
	{
		for (var project_internal_id in projectWiseRevenue[emp_internal_id].RevenueData)
		{
			var i_amount_map = 0;
			if (projectWiseRevenue[emp_internal_id].sub_practice == sub_prac)
			{
				for (var month in projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id])
				{
					var a_MonthNames = MonthNames(i_year_project);
					var currentMonthPos = a_MonthNames.indexOf(month);
					var s_current_month = a_MonthNames[currentMonthPos];
					var s_current_mnth_yr = s_current_month.substr(4, 8);
					
					var i_nxt_yr_flg = 0;
					if(currentMonthPos >=12)
					{
						currentMonthPos = currentMonthPos - 12;
						i_nxt_yr_flg = 1;
					}
					
					var i_curr_yr_dis_flag = 0;
					if(parseInt(s_current_mnth_yr) < i_year_project_end && i_prject_end_mnth < currentMonthPos)
					{
						i_curr_yr_dis_flag = 1;
					}
					
					if (((i_project_mnth <= currentMonthPos && ((i_prject_end_mnth >= currentMonthPos || i_curr_yr_dis_flag == 1) && i_nxt_yr_flg == 0) && parseInt(s_current_mnth_yr) <= i_year_project) && parseInt(s_current_mnth_yr) <= i_year_project_end) || (i_prject_end_mnth >= currentMonthPos && parseInt(s_current_mnth_yr) <= i_year_project_end && i_nxt_yr_flg == 1))
					{
						var f_prcnt_revenue = parseFloat(a_amount[i_amount_map]) / parseFloat(i_total_row_revenue);
						f_prcnt_revenue = f_prcnt_revenue * 100;
						
						//var f_revenue_amount = (parseFloat(f_prcnt_revenue) * parseFloat(f_revenue_share)) / 100;
						var f_revenue_amount = (parseFloat(f_prcnt_revenue) * revenue_share_arr[i_amount_map]) / 100;
						projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id][month].total_revenue = f_revenue_amount;
						projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id][month].prcent_complt = f_prcnt_revenue;
						i_amount_map++;
					}
				}	
			}
		}
	}
	
	var today = new Date();
	var currentMonthName = getMonthName(nlapiDateToString(today));
	
	for (var j = 0; j < monthBreakUp.length; j++)
	{
		var months = monthBreakUp[j];
		var s_month_name = getMonthName(months.Start); // get month name
		var s_year = nlapiStringToDate(months.Start)
		s_year = s_year.getFullYear();
		s_month_name = s_month_name+'_'+s_year;
		
	}
	
	var i_total_revenue_recognized_ytd = 0;
	for(var i_revenue_index=0; i_revenue_index<a_recognized_revenue.length; i_revenue_index++)
	{
		i_total_revenue_recognized_ytd = parseFloat(i_total_revenue_recognized_ytd) + parseFloat(a_recognized_revenue[i_revenue_index]);
		
	}
	
	nlapiLogExecution('audit','f_revenue_amount_till_current_month:- '+f_revenue_amount_till_current_month,'i_total_revenue_recognized_ytd:- '+i_total_revenue_recognized_ytd)
	//var f_current_month_actual_revenue = parseFloat(f_revenue_amount_till_current_month) - parseFloat(i_total_revenue_recognized_ytd);
	//var i_total_revenue_recognized_ytd = 0;
	var f_current_month_actual_revenue = parseFloat(f_revenue_amount_till_current_month);
	f_current_month_actual_revenue_total_proj_level = parseFloat(f_current_month_actual_revenue_total_proj_level) + parseFloat(f_current_month_actual_revenue);
	if((parseInt(i_project_sow_value)==parseInt(f_tot_revenue_till_current_month))&& customer_lookup!= 'T')
	{
		f_current_month_actual_revenue=0;
	}
	f_current_month_actual_revenue = parseInt(f_current_month_actual_revenue);
	var json_to_process = f_current_month_actual_revenue+'##'+a_amount[f_total_prev_mnths];
	return json_to_process;
}
function generate_cost_view(projectWiseRevenue,monthBreakUp,i_year_project,i_project_mnth,mode,sub_prac,s_currency_symbol_proj,s_currency_symbol_usd,i_year_project_end,i_prject_end_mnth,s_currentMonthName)
{
	var a_sub_prac_already_displayed = new Array();
	var i_subpractice_id = 0;
	var a_amount = new Array();
	var a_recognized_revenue = new Array();
	var i_frst_row = 0;
	var i_diplay_frst_column = 0;
	var i_total_row_revenue = 0;
	var i_total_per_row = 0;
	var total_prcnt_aloocated = new Array();
	var b_first_line_copied = 0;
	var f_total_allocated_row = 0;
	
	var d_today_date = new Date();
	var i_current_month = mnth_array.indexOf(s_currentMonthName);
	//var i_current_month = d_today_date.getMonth();
	
	var i_current_year = d_today_date.getFullYear();
	
	for (var emp_internal_id in projectWiseRevenue)
	{
		if (projectWiseRevenue[emp_internal_id].sub_practice == sub_prac)
		{
			f_total_allocated_row = 0;
			for (var project_internal_id in projectWiseRevenue[emp_internal_id].RevenueData)
			{
				var i_index_mnth = 0;
				for (var month in projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id])
				{
					var a_MonthNames = MonthNames(i_year_project);
					var currentMonthPos = a_MonthNames.indexOf(month);
					var s_current_month = a_MonthNames[currentMonthPos];
					var s_current_mnth_yr = s_current_month.substr(4, 8);
					
					var i_nxt_yr_flg = 0;
					if(currentMonthPos >=12)
					{
						currentMonthPos = currentMonthPos - 12;
						i_nxt_yr_flg = 1;
					}
					
					var i_curr_yr_dis_flag = 0;
					if(parseInt(s_current_mnth_yr) < i_year_project_end && i_prject_end_mnth < currentMonthPos)
					{
						i_curr_yr_dis_flag = 1;
					}
					
					if (((i_project_mnth <= currentMonthPos && ((i_prject_end_mnth >= currentMonthPos || i_curr_yr_dis_flag == 1) && i_nxt_yr_flg == 0) && parseInt(s_current_mnth_yr) <= i_year_project) && parseInt(s_current_mnth_yr) <= i_year_project_end) || (i_prject_end_mnth >= currentMonthPos && parseInt(s_current_mnth_yr) <= i_year_project_end && i_nxt_yr_flg == 1))
					{
						//nlapiLogExecution('audit','currentMonthPos:- ',+currentMonthPos);
						if(b_first_line_copied == 1)
						{
							var f_allocated_prcnt = total_prcnt_aloocated[i_index_mnth];
							f_allocated_prcnt = parseFloat(f_allocated_prcnt) + parseFloat(projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id][month].prcnt_allocated);
							total_prcnt_aloocated[i_index_mnth] = f_allocated_prcnt;
							i_index_mnth++;							
						}
						else
						{
							total_prcnt_aloocated.push(projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id][month].prcnt_allocated);
						}
						
						f_total_allocated_row = parseFloat(projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id][month].prcnt_allocated) + parseFloat(f_total_allocated_row);
						
					}
				}
			}
			
			b_first_line_copied = 1;
			
			var s_sub_prac_name = projectWiseRevenue[emp_internal_id].sub_prac_name;
			var s_practice_name = projectWiseRevenue[emp_internal_id].practice_name;
			
			i_diplay_frst_column = 1;
		}
	}
	i_diplay_frst_column = 0;
	
	var f_total_row_allocation = 0;
	for(var i_index_total_allocated=0; i_index_total_allocated<total_prcnt_aloocated.length; i_index_total_allocated++)
	{
		f_total_row_allocation = parseFloat(f_total_row_allocation) + parseFloat(total_prcnt_aloocated[i_index_total_allocated]);
	}
	
	for ( var emp_internal_id in projectWiseRevenue) {
		
		for ( var project_internal_id in projectWiseRevenue[emp_internal_id].RevenueData) {
			
			if(projectWiseRevenue[emp_internal_id].sub_practice == sub_prac)
			{	
				var i_sub_practice_internal_id = 0;
				var i_amt = 0;
				for (var month in projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id])
				{
					var a_MonthNames = MonthNames(i_year_project);
					var currentMonthPos = a_MonthNames.indexOf(month);
					var s_current_month = a_MonthNames[currentMonthPos];
					var s_current_mnth_yr = s_current_month.substr(4, 8);
					
					var i_nxt_yr_flg = 0;
					if(currentMonthPos >=12)
					{
						currentMonthPos = currentMonthPos - 12;
						i_nxt_yr_flg = 1;
					}
					
					var i_curr_yr_dis_flag = 0;
					if(parseInt(s_current_mnth_yr) < i_year_project_end && i_prject_end_mnth < currentMonthPos)
					{
						i_curr_yr_dis_flag = 1;
					}
					
					if (((i_project_mnth <= currentMonthPos && ((i_prject_end_mnth >= currentMonthPos || i_curr_yr_dis_flag == 1) && i_nxt_yr_flg == 0) && parseInt(s_current_mnth_yr) <= i_year_project) && parseInt(s_current_mnth_yr) <= i_year_project_end) || (i_prject_end_mnth >= currentMonthPos && parseInt(s_current_mnth_yr) <= i_year_project_end && i_nxt_yr_flg == 1))
					{
						if (mode == 2)
						{
							var i_mnth_posi = parseFloat(i_current_month) - parseFloat(i_project_mnth);
							var total_revenue_format = parseFloat(projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id][month].Amount).toFixed(1);
							
							if(i_frst_row == 0)
							{
								//nlapiLogExecution('audit','inside push amnt');
								a_amount.push(total_revenue_format);
								
								//if(i_amt < i_mnth_posi-2)
									a_recognized_revenue.push(projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id][month].recognized_revenue);
							}
							else
							{
								var amnt_mnth = a_amount[i_amt];
								amnt_mnth = parseFloat(amnt_mnth) + parseFloat(projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id][month].Amount);
								
								a_amount[i_amt] = amnt_mnth;
								
								var i_existing_recognised_amount = a_recognized_revenue[i_amt];
								i_existing_recognised_amount = parseFloat(i_existing_recognised_amount) + parseFloat(projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id][month].recognized_revenue);
								
								//if(i_amt < i_mnth_posi-2)
									a_recognized_revenue[i_amt] = i_existing_recognised_amount;
								
								i_amt++;
							}
							//nlapiLogExecution('audit','monthly amount:- '+parseFloat(projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id][month].Amount));
							i_total_row_revenue = parseFloat(i_total_row_revenue) + parseFloat(projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id][month].Amount);
							i_total_per_row = parseFloat(i_total_per_row) + parseFloat(projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id][month].Amount);
						}
						
					}
				}
				
				if (mode == 2)
				{
					i_total_per_row = 0;
					//i_total_row_revenue = parseFloat(i_total_row_revenue) + parseFloat(projectWiseRevenue[emp_internal_id].total_revenue_for_tenure);
				}
				
				i_frst_row = 1;
	
				i_diplay_frst_column = 1;
				
				var f_revenue_share = projectWiseRevenue[emp_internal_id].revenue_share;
			}			
		}
	}
	
	var f_total_revenue = 0;
	var f_revenue_amount_till_current_month = 0;
	for (var i_amt = 0; i_amt < a_amount.length; i_amt++)
	{
		var f_prcnt_revenue = parseFloat(a_amount[i_amt]) / parseFloat(i_total_row_revenue);
		f_prcnt_revenue = f_prcnt_revenue * 100;
		
		var f_revenue_amount = (parseFloat(f_prcnt_revenue) * parseFloat(f_revenue_share)) / 100;
		if(!f_revenue_amount)
			f_revenue_amount = 0;
			
		if (parseInt(i_year_project) != parseInt(i_current_year))
		{
          	var i_current_month_from_module = i_current_month;
          	//added for new year
          	var i_current_month_pr = d_today_date.getMonth();
			var i_total_project_tenure = 11 + parseInt(i_current_month_pr);
			var f_total_prev_mnths = parseFloat(i_total_project_tenure) - parseFloat(i_project_mnth);
          //Added for revenue calculation after month change
           if(i_current_month_from_module == i_current_month_pr)
			f_total_prev_mnths = parseFloat(f_total_prev_mnths) + parseFloat(1);
		}
		else
		{
			var f_total_prev_mnths = parseFloat(i_current_month) - parseFloat(i_project_mnth);
			//f_total_prev_mnths = parseFloat(f_total_prev_mnths) - parseFloat(1);
		}
		//if(i_amt < f_total_prev_mnths)
		if(i_amt <= f_total_prev_mnths)
		{
			//nlapiLogExecution('audit','f_revenue_amount:- '+f_revenue_amount,'a_amount.length:- '+a_amount.length);
			f_revenue_amount_till_current_month = parseFloat(f_revenue_amount) + parseFloat(f_revenue_amount_till_current_month);
		}
		
		f_total_revenue = parseFloat(f_revenue_amount) + parseFloat(f_total_revenue);
		
	}
	
	for (var emp_internal_id in projectWiseRevenue)
	{
		for (var project_internal_id in projectWiseRevenue[emp_internal_id].RevenueData)
		{
			var i_amount_map = 0;
			if (projectWiseRevenue[emp_internal_id].sub_practice == sub_prac)
			{
				for (var month in projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id])
				{
					var a_MonthNames = MonthNames(i_year_project);
					var currentMonthPos = a_MonthNames.indexOf(month);
					var s_current_month = a_MonthNames[currentMonthPos];
					var s_current_mnth_yr = s_current_month.substr(4, 8);
					
					var i_nxt_yr_flg = 0;
					if(currentMonthPos >=12)
					{
						currentMonthPos = currentMonthPos - 12;
						i_nxt_yr_flg = 1;
					}
					
					var i_curr_yr_dis_flag = 0;
					if(parseInt(s_current_mnth_yr) < i_year_project_end && i_prject_end_mnth < currentMonthPos)
					{
						i_curr_yr_dis_flag = 1;
					}
					
					if (((i_project_mnth <= currentMonthPos && ((i_prject_end_mnth >= currentMonthPos || i_curr_yr_dis_flag == 1) && i_nxt_yr_flg == 0) && parseInt(s_current_mnth_yr) <= i_year_project) && parseInt(s_current_mnth_yr) <= i_year_project_end) || (i_prject_end_mnth >= currentMonthPos && parseInt(s_current_mnth_yr) <= i_year_project_end && i_nxt_yr_flg == 1))
					{
						var f_prcnt_revenue = parseFloat(a_amount[i_amount_map]) / parseFloat(i_total_row_revenue);
						f_prcnt_revenue = f_prcnt_revenue * 100;
						
						var f_revenue_amount = (parseFloat(f_prcnt_revenue) * parseFloat(f_revenue_share)) / 100;
		
						projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id][month].total_revenue = f_revenue_amount;
						projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id][month].prcent_complt = f_prcnt_revenue;
						i_amount_map++;
					}
				}	
			}
		}
	}
	
	var today = new Date();
	var currentMonthName = getMonthName(nlapiDateToString(today));
	
	for (var j = 0; j < monthBreakUp.length; j++)
	{
		var months = monthBreakUp[j];
		var s_month_name = getMonthName(months.Start); // get month name
		var s_year = nlapiStringToDate(months.Start)
		s_year = s_year.getFullYear();
		s_month_name = s_month_name+'_'+s_year;
		
	}
	
	var i_total_revenue_recognized_ytd = 0;
	for(var i_revenue_index=0; i_revenue_index<a_recognized_revenue.length; i_revenue_index++)
	{
		i_total_revenue_recognized_ytd = parseFloat(i_total_revenue_recognized_ytd) + parseFloat(a_recognized_revenue[i_revenue_index]);
		
	}
	
	nlapiLogExecution('audit','f_revenue_amount_till_current_month:- '+f_revenue_amount_till_current_month,'i_total_revenue_recognized_ytd:- '+i_total_revenue_recognized_ytd)
	var f_current_month_actual_revenue = parseFloat(f_revenue_amount_till_current_month) - parseFloat(i_total_revenue_recognized_ytd);
	//var i_total_revenue_recognized_ytd = 0;
	
	f_current_month_actual_revenue_total_proj_level = parseFloat(f_current_month_actual_revenue_total_proj_level) + parseFloat(f_current_month_actual_revenue);
	
	f_current_month_actual_revenue = parseInt(f_current_month_actual_revenue);
	var json_to_process = f_current_month_actual_revenue+'##'+a_amount[f_total_prev_mnths];
	return json_to_process;
}


function getMonthsBreakup(d_startDate, d_endDate) {
	
	try
	{
		var dateBreakUp = [];
		var new_start_date = new Date(d_startDate.getFullYear(), d_startDate
		        .getMonth(), 1);
				
		var nxt_mnth_strt_date = new Date(d_startDate.getFullYear(), d_startDate
		        .getMonth()+1, 1);
		
		var endDate = new Date(nxt_mnth_strt_date - 1);
				
		//var endDate = nlapiAddDays(nlapiAddMonths(d_startDate, 1), -1);
		endDate = nlapiDateToString(endDate, 'date');
	
		dateBreakUp.push({
		    Start : nlapiDateToString(new_start_date, 'date'),
		    End : endDate
		});
	
		for (var i = 1;; i++) {
			var new_date = nlapiAddMonths(new_start_date, i);
	
			if (new_date > d_endDate) {
				break;
			}
			
			endDate = nlapiAddDays(nlapiAddMonths(new_date, 1), -1);
			endDate = nlapiDateToString(endDate, 'date');
					
			dateBreakUp.push({
			    Start : nlapiDateToString(new_date, 'date'),
			    End : endDate
			});
		}
	}
	catch(err)
	{
		nlapiLogExecution('ERROR','getMonthsBreakup','ERROR MESSAGE :- '+err);
	}

	return dateBreakUp;
}

function getMonthName(currentDate) {
	currentDate = nlapiStringToDate(currentDate)
	var monthNames = [ "JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JUL", "AUG",
	        "SEP", "OCT", "NOV", "DEC" ];
	return monthNames[currentDate.getMonth()];
}

function MonthNames(yr) {
	var yr1 = Number(yr) + 1;
	
	var monthNames = [ "JAN_"+yr, "FEB_"+yr, "MAR_"+yr, "APR_"+yr, "MAY_"+yr, "JUN_"+yr, "JUL_"+yr, "AUG_"+yr,
	        "SEP_"+yr, "OCT_"+yr, "NOV_"+yr, "DEC_"+yr ];
	var month_nxt_yr = ["JAN_"+yr1, "FEB_"+yr1, "MAR_"+yr1, "APR_"+yr1, "MAY_"+yr1, "JUN_"+yr1, "JUL_"+yr1, "AUG_"+yr1,
	        "SEP_"+yr1, "OCT_"+yr1, "NOV_"+yr1, "DEC_"+yr1 ];
	monthNames = monthNames.concat(month_nxt_yr);
	
	return monthNames;
}

function yearData(yr) {
	var yr1 = Number(yr) + 1;
	
	var monthNames = [ "JAN_"+yr, "FEB_"+yr, "MAR_"+yr, "APR_"+yr, "MAY_"+yr, "JUN_"+yr, "JUL_"+yr, "AUG_"+yr,
	        "SEP_"+yr, "OCT_"+yr, "NOV_"+yr, "DEC_"+yr ];
	var month_nxt_yr = ["JAN_"+yr1, "FEB_"+yr1, "MAR_"+yr1, "APR_"+yr1, "MAY_"+yr1, "JUN_"+yr1, "JUL_"+yr1, "AUG_"+yr1,
	        "SEP_"+yr1, "OCT_"+yr1, "NOV_"+yr1, "DEC_"+yr1 ];
	monthNames = monthNames.concat(month_nxt_yr);
	
	for (var i = 0; i < monthNames.length; i++) {
		this[monthNames[i]] = {
		    Amount : 0,
			prcnt_allocated : 0,
			total_revenue : 0,
			recognized_revenue : 0,
			actual_revenue : 0,
			mnth_strt : 0,
			mnth_end : 0,
			prcent_complt : 0
		};
	}
}

function format2(n) {
    n = parseFloat(n).toFixed(1).replace(/(\d)(?=(\d{3})+\.)/g, "$1,");
	n = n.toString().split('.');
	return n[0];
}

function getCurrency_Symbol(s_proj_currency)
{
	var s_currency_symbols = {
    'USD': '$', // US Dollar
    'EUR': 'â‚¬', // Euro
    'GBP': 'Â£', // British Pound Sterling
    'INR': 'â‚¹', // Indian Rupee
	};
	
	if(s_currency_symbols[s_proj_currency]!==undefined) {
	    return s_currency_symbols[s_proj_currency];
	}
}

function find_recognized_revenue(a_revenue_recognized_for_project,practice,subpractice,role,level,month,year,a_subprac_searched_once,a_subprac_searched_once_month,a_subprac_searched_once_year)
{
	var f_recognized_amount = 0;
	var count =0;
	if(func_search_prac_processed_fr_mnth(a_subprac_searched_once,subpractice,month,year) < 0)
	{
		for(var i_index_find=0; i_index_find<a_revenue_recognized_for_project.length; i_index_find++)
		{				
			if(practice == a_revenue_recognized_for_project[i_index_find].practice_revenue_recognized)
			{
				if(subpractice ==  a_revenue_recognized_for_project[i_index_find].subpractice_revenue_recognized)
				{
					//if(role == a_revenue_recognized_for_project[i_index_find].role_revenue_recognized)
					{
						//if(level == a_revenue_recognized_for_project[i_index_find].level_revenue_recognized)
						{
							if(month == a_revenue_recognized_for_project[i_index_find].month_revenue_recognized)
							{	
								if(parseInt(year) == parseInt(a_revenue_recognized_for_project[i_index_find].year_revenue_recognized))
								{
                                  	count = f_recognized_amount;
									f_recognized_amount = a_revenue_recognized_for_project[i_index_find].amount_revenue_recognized;
									//nlapiLogExecution('audit','f_recognized_amount inside look for prev mnth amnt once pr sub prac:- ',f_recognized_amount);
									a_subprac_searched_once.push(
																{
																	'subpractice': subpractice,
																	'month': month,
																	'year': year
																});
									//a_subprac_searched_once_month.push(month);
									//a_subprac_searched_once_year.push(year);
									f_recognized_amount =parseFloat(f_recognized_amount) +parseFloat(count);
									if(subpractice == 325)
									{
										nlapiLogExecution('audit','month:- '+month,'year:- '+year);
										nlapiLogExecution('audit','f_recognized_amount:- '+f_recognized_amount);
									}
								}
							}
						}
					}
				}
			}
		}
		
		return f_recognized_amount;
	}
	else
	{
		return 0;
	}
}

function func_search_prac_processed_fr_mnth(a_subprac_searched_once,subpractice,month,year)
{
	var i_return_var = -1;
	for(var i_loop=0; i_loop<a_subprac_searched_once.length; i_loop++)
	{
		if(a_subprac_searched_once[i_loop].subpractice == subpractice)
		{
			if(a_subprac_searched_once[i_loop].month == month)
			{
				if(a_subprac_searched_once[i_loop].year == year)
				{
					i_return_var = i_loop;
					break;
				}
			}
		}
	}
	
	return i_return_var;
}

function searchRecord(recordType, savedSearch, arrFilters, arrColumns,
        filterExpression)
{

	try {
		var search = null;

		// if a saved search is provided, load it and add the filters and
		// columns
		if (isNotEmpty(savedSearch)) {
			search = nlapiLoadSearch(recordType, savedSearch);

			if (isArrayNotEmpty(arrFilters)) {
				search.addFilters(arrFilters);
			}

			if (isArrayNotEmpty(arrColumns)) {
				search.addColumns(arrColumns);
			}

			if (isArrayNotEmpty(filterExpression)) {
				search.setFilterExpression(filterExpression);
			}
		}
		// create a new search
		else {
			search = nlapiCreateSearch(recordType, arrFilters, arrColumns);
		}

		// run search
		var resultSet = search.runSearch();

		// iterate through the search and get all data 1000 at a time
		var searchResultCount = 0;
		var resultSlice = null;
		var searchResult = [];

		do {
			resultSlice = resultSet.getResults(searchResultCount,
			        searchResultCount + 1000);

			if (resultSlice) {

				resultSlice.forEach(function(result) {

					searchResult.push(result);
					searchResultCount++;
				});
			}
		} while (isArrayNotEmpty(resultSlice) && resultSlice.length >= 1000);

		return searchResult;
	} catch (err) {
		nlapiLogExecution('ERROR', 'searchRecord', err);
		throw err;
	}
}

function isEmpty(value) {

	return value == null || value == "" || typeof (value) == undefined;
}

function isNotEmpty(value) {

	return !isEmpty(value);
}

function isArrayEmpty(argArray) {

	return !isArrayNotEmpty(argArray);
}

function isArrayNotEmpty(argArray) {

	return (isNotEmpty(argArray) && argArray.length > 0);
}

function get_previous_month(i_month)
{
	if (i_month) 
 	{
 	  if(i_month == 'Jan' || i_month == 'January' || i_month == 'JAN' || i_month == 'JANUARY')
	  {	  	
	  		i_month = 'DEC';				
	  }	
	  else if(i_month == 'Feb' || i_month == 'February' || i_month == 'FEB' || i_month == 'FEBRUARY')
	  {
	  	i_month = 'JAN';
			
	  }		
	  else if(i_month == 'Mar' || i_month == 'March' || i_month == 'MAR' || i_month == 'MARCH')
	  {	  
	     i_month = 'FEB';
	
	  }	
	  else if(i_month == 'Apr' || i_month == 'April' || i_month == 'APR' || i_month == 'APRIL')
	  {
	  	i_month = 'MAR';
	
	  }	
	  else if(i_month == 'May' || i_month == 'MAY')
	  {	  
		  i_month = 'APR';
	
	  }	  
	  else if(i_month == 'June' || i_month == 'Jun' || i_month == 'JUN' || i_month == 'JUNE')
	  {	 
		 i_month = 'MAY';	 
			
	  }	
	  else if(i_month == 'Jul' || i_month == 'July' || i_month == 'JUL' || i_month == 'JULY')
	  {	  
		i_month = 'JUN';
	  }	
	  else if(i_month == 'August' || i_month == 'Aug' || i_month == 'AUG' || i_month == 'AUGUST')
	  {	  
		 i_month = 'JUL';
	  }  
	  else if(i_month == 'Sep' || i_month == 'September' || i_month == 'SEP' || i_month == 'SEPTEMBER')
	  {  	
	      i_month = 'AUG';
	
	  }	
	  else if(i_month == 'Oct' || i_month == 'October' || i_month == 'OCT' || i_month == 'OCTOBER')
	  {	 	
			i_month = 'SEP';	
	  }	
	  else if(i_month == 'Nov' || i_month == 'November' || i_month == 'NOV' || i_month == 'NOVEMBER')
	  {	  
		 i_month = 'OCT';
			
	  }	
	  else if(i_month == 'Dec' || i_month == 'December' || i_month == 'DEC' || i_month == 'DECEMBER')
	  {	 
		 i_month = 'NOV';
			
	  }	
 }//Month & Year
	
	return i_month;
}

function get_previous_month_end_date(i_month,i_year)
{
   var d_start_date = '';
   var d_end_date = '';
	
   var date_format = checkDateFormat();	 
	
 if (_logValidation(i_month) && _logValidation(i_year)) 
 {
 	  if(i_month == 'Jan' || i_month == 'January' || i_month == 'JAN' || i_month == 'JANUARY')
	  {	  	
		 if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 1 + '-' + 1;		
			d_end_date = i_year + '-' + 1 + '-' + 31;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 1 + '/' + i_year;
			d_end_date = 31 + '/' + 1 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 1 + '/' + 1 + '/' + i_year;
			d_end_date = 1 + '/' + 31 + '/' + i_year;
		 }	 
				
	  }	
	  else if(i_month == 'Feb' || i_month == 'February' || i_month == 'FEB' || i_month == 'FEBRUARY')
	  {
	  	if((i_year%4) == 0)
		{
			i_day = 29;
		}
		else
		{
			i_day = 28;
		}
			
		 if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 2 + '-' + 1;		
			d_end_date = i_year + '-' + 2 + '-' + i_day;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 2 + '/' + i_year;
			d_end_date = i_day + '/' + 2 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 2 + '/' + 1 + '/' + i_year;
			d_end_date = 2 + '/' + i_day + '/' + i_year;
		 }	 
			
	  }		
	  else if(i_month == 'Mar' || i_month == 'March' || i_month == 'MAR' || i_month == 'MARCH')
	  {	  
	     if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 3 + '-' + 1;		
			d_end_date = i_year + '-' + 3 + '-' + 31;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 3 + '/' + i_year;
			d_end_date = 31 + '/' + 3 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 3 + '/' + 1 + '/' + i_year;
			d_end_date = 3 + '/' + 31 + '/' + i_year;
		 }	 
	
	  }	
	  else if(i_month == 'Apr' || i_month == 'April' || i_month == 'APR' || i_month == 'APRIL')
	  {
	  	if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 4 + '-' + 1;		
			d_end_date = i_year + '-' + 4 + '-' + 30;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 4 + '/' + i_year;
			d_end_date = 30 + '/' + 4 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 4 + '/' + 1 + '/' + i_year;
			d_end_date = 4 + '/' + 30 + '/' + i_year;
	     }	 
	 
	
	  }	
	  else if(i_month == 'May' || i_month == 'MAY')
	  {	  
		 if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 5 + '-' + 1;		
			d_end_date = i_year + '-' + 5 + '-' + 31;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 5 + '/' + i_year;
			d_end_date = 31 + '/' + 5 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 5 + '/' + 1 + '/' + i_year;
			d_end_date = 5 + '/' + 31 + '/' + i_year;
	     }	 
	
	  }	  
	  else if(i_month == 'June' || i_month == 'Jun' || i_month == 'JUN' || i_month == 'JUNE')
	  {	 
		 if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 6 + '-' + 1;		
			d_end_date = i_year + '-' + 6 + '-' + 30;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 6 + '/' + i_year;
			d_end_date = 30 + '/' + 6 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 6 + '/' + 1 + '/' + i_year;
			d_end_date = 6 + '/' + 30 + '/' + i_year;
	     }	 
			
	  }	
	  else if(i_month == 'Jul' || i_month == 'July' || i_month == 'JUL' || i_month == 'JULY')
	  {	  
		 if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 7 + '-' + 1;		
			d_end_date = i_year + '-' + 7 + '-' + 31;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 7 + '/' + i_year;
			d_end_date = 31 + '/' + 7 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 7 + '/' + 1 + '/' + i_year;
			d_end_date = 7 + '/' + 31 + '/' + i_year;
	     }	 
		
	  }	
	  else if(i_month == 'August' || i_month == 'Aug' || i_month == 'AUG' || i_month == 'AUGUST')
	  {	  
		 if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 8 + '-' + 1;		
			d_end_date = i_year + '-' + 8 + '-' + 31;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 8 + '/' + i_year;
			d_end_date = 31 + '/' + 8 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 8 + '/' + 1 + '/' + i_year;
			d_end_date = 8 + '/' + 31 + '/' + i_year;
	     }	 
		
	  }  
	  else if(i_month == 'Sep' || i_month == 'September' || i_month == 'SEP' || i_month == 'SEPTEMBER')
	  {  	
	     if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 9 + '-' + 1;		
			d_end_date = i_year + '-' + 9 + '-' + 30;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 9 + '/' + i_year;
			d_end_date = 30 + '/' + 9 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 9 + '/' + 1 + '/' + i_year;
			d_end_date = 9 + '/' + 30 + '/' + i_year;
	     }	 
	
	  }	
	  else if(i_month == 'Oct' || i_month == 'October' || i_month == 'OCT' || i_month == 'OCTOBER')
	  {	 	
		if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 10 + '-' + 1;		
			d_end_date = i_year + '-' + 10 + '-' + 31;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 10 + '/' + i_year;
			d_end_date = 31 + '/' + 10 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 10 + '/' + 1 + '/' + i_year;
			d_end_date = 10 + '/' + 31 + '/' + i_year;
	     }		
	  }	
	  else if(i_month == 'Nov' || i_month == 'November' || i_month == 'NOV' || i_month == 'NOVEMBER')
	  {	  
		if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 11 + '-' + 1;		
			d_end_date = i_year + '-' + 11 + '-' + 30;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 11 + '/' + i_year;
			d_end_date = 30 + '/' + 11 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 11 + '/' + 1 + '/' + i_year;
			d_end_date = 11 + '/' + 30 + '/' + i_year;
	     }	 
			
	  }	
	  else if(i_month == 'Dec' || i_month == 'December' || i_month == 'DEC' || i_month == 'DECEMBER')
	  {	 
		if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 12 + '-' + 1;		
			d_end_date = i_year + '-' + 12 + '-' + 31;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 12 + '/' + i_year;
			d_end_date = 31 + '/' + 12 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 12 + '/' + 1 + '/' + i_year;
			d_end_date = 12 + '/' + 31 + '/' + i_year;
	     }	 
			
	  }	
 }//Month & Year
 return d_end_date;
}

function checkDateFormat()
{
    var context = nlapiGetContext();

	var dateFormatPref = context.getPreference('dateformat');

    return dateFormatPref;
}

function _logValidation(value) 
{
 if(value!='null' && value != null && value != null && value != '' && value != undefined && value != undefined && value != 'undefined' && value!= 'undefined'&& value!= 'NaN' && value != NaN) 
 {
  return true;
 }
 else 
 { 
  return false;
 }
}
function generate_resource_cost_total(projectWiseRevenue,monthBreakUp,i_year_project,i_project_mnth,mode,sub_prac,s_currency_symbol_proj,s_currency_symbol_usd,i_year_project_end,i_prject_end_mnth,s_entire_json_Obj,a_total_Json_Obj)
{
	var css_file_url = "";

	var context = nlapiGetContext();


	var html = "";
	

	// header
	
		
	
	var today = new Date();
	var currentMonthName = getMonthName(nlapiDateToString(today));
	
	for (var j = 0; j < monthBreakUp.length; j++)
	{
		var months = monthBreakUp[j];
		var s_month_name = getMonthName(months.Start); // get month name
		var s_year = nlapiStringToDate(months.Start)
		s_year = s_year.getFullYear();
		s_month_name = s_month_name+'_'+s_year;
		
	}
		
	
	
	var a_sub_prac_already_displayed = new Array();
	var i_subpractice_id = 0;
	var a_amount = new Array();
	var i_frst_row = 0;
	var i_diplay_frst_column = 0;
	var i_total_row_revenue = 0;
	var total_prcnt_aloocated = new Array();
	var b_first_line_copied = 0;
	var f_total_allocated_row = 0;
	var f_total_per_row_cost = 0;
	
	for (var emp_internal_id in projectWiseRevenue)
	{
		if (projectWiseRevenue[emp_internal_id].sub_practice == sub_prac)
		{
			f_total_allocated_row = 0;
			for (var project_internal_id in projectWiseRevenue[emp_internal_id].RevenueData)
			{
				
				
				if(i_diplay_frst_column == 0)
				{
					
				}
				else
				{
					
				}
								
				var i_index_mnth = 0;
				
				for (var month in projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id])
				{
					var a_MonthNames = MonthNames(i_year_project);
					var currentMonthPos = a_MonthNames.indexOf(month);
					var s_current_month = a_MonthNames[currentMonthPos];
					var s_current_mnth_yr = s_current_month.substr(4, 8);
					
					var i_nxt_yr_flg = 0;
					if(currentMonthPos >=12)
					{
						currentMonthPos = currentMonthPos - 12;
						i_nxt_yr_flg = 1;
					}
					
					var i_curr_yr_dis_flag = 0;
					if(parseInt(s_current_mnth_yr) < i_year_project_end && i_prject_end_mnth < currentMonthPos)
					{
						i_curr_yr_dis_flag = 1;
					}
					
					//nlapiLogExecution('audit','currentMonthPos::::'+currentMonthPos,'i_project_mnth:- '+i_project_mnth+':: i_prject_end_mnth:- '+i_prject_end_mnth);
					if (((i_project_mnth <= currentMonthPos && ((i_prject_end_mnth >= currentMonthPos || i_curr_yr_dis_flag == 1) && i_nxt_yr_flg == 0) && parseInt(s_current_mnth_yr) <= i_year_project) && parseInt(s_current_mnth_yr) <= i_year_project_end) || (i_prject_end_mnth >= currentMonthPos && parseInt(s_current_mnth_yr) <= i_year_project_end && i_nxt_yr_flg == 1))
					{
						//nlapiLogExecution('audit','i_project_mnth:- '+i_project_mnth+':::i_prject_end_mnth:- '+i_prject_end_mnth+'::::currentMonthPos:- '+currentMonthPos,s_current_mnth_yr);
						if(b_first_line_copied == 1)
						{
							var f_allocated_prcnt = total_prcnt_aloocated[i_index_mnth];
							f_allocated_prcnt = parseFloat(f_allocated_prcnt) + parseFloat(projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id][month].prcnt_allocated);
							total_prcnt_aloocated[i_index_mnth] = f_allocated_prcnt;
							i_index_mnth++;							
						}
						else
						{
							total_prcnt_aloocated.push(projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id][month].prcnt_allocated);
						}
						
						f_total_allocated_row = parseFloat(projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id][month].prcnt_allocated) + parseFloat(f_total_allocated_row);
						
						
					}
				}
			}
			
			b_first_line_copied = 1;
			
			
			var s_sub_prac_name = projectWiseRevenue[emp_internal_id].sub_prac_name;
			var s_practice_name = projectWiseRevenue[emp_internal_id].practice_name;
			
			i_diplay_frst_column = 1;
		}
	}
	i_diplay_frst_column = 0;
	

	
	var f_total_row_allocation = 0;
	for(var i_index_total_allocated=0; i_index_total_allocated<total_prcnt_aloocated.length; i_index_total_allocated++)
	{
		
		
		f_total_row_allocation = parseFloat(f_total_row_allocation) + parseFloat(total_prcnt_aloocated[i_index_total_allocated]);
	}
	


	
	for ( var emp_internal_id in projectWiseRevenue) {
		f_total_per_row_cost = 0;
		for ( var project_internal_id in projectWiseRevenue[emp_internal_id].RevenueData) {
			
			if(projectWiseRevenue[emp_internal_id].sub_practice == sub_prac)
			{
			
					
				var i_sub_practice_internal_id = 0;
				var i_amt = 0;
				for (var month in projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id])
				{
					var a_MonthNames = MonthNames(i_year_project);
					var currentMonthPos = a_MonthNames.indexOf(month);
					var s_current_month = a_MonthNames[currentMonthPos];
					var s_current_mnth_yr = s_current_month.substr(4, 8);
					var i_nxt_yr_flg = 0;
					
					if(currentMonthPos >=12)
					{
						i_nxt_yr_flg = 1;
						currentMonthPos = currentMonthPos - 12;
					}
					
					var i_curr_yr_dis_flag = 0;
					if(parseInt(s_current_mnth_yr) < i_year_project_end && i_prject_end_mnth < currentMonthPos)
					{
						i_curr_yr_dis_flag = 1;
					}
					
					if (((i_project_mnth <= currentMonthPos && ((i_prject_end_mnth >= currentMonthPos || i_curr_yr_dis_flag == 1)&& i_nxt_yr_flg == 0) && parseInt(s_current_mnth_yr) <= i_year_project) && parseInt(s_current_mnth_yr) <= i_year_project_end) || (i_prject_end_mnth >= currentMonthPos && parseInt(s_current_mnth_yr) <= i_year_project_end && i_nxt_yr_flg == 1))
					{
						
						if (mode == 2)
						{
							
							var total_revenue_format = parseFloat(projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id][month].Amount).toFixed(1);
							
							
							f_total_per_row_cost = parseFloat(projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id][month].Amount) + parseFloat(f_total_per_row_cost);
							
							if(i_frst_row == 0)
							{
								a_amount.push(total_revenue_format);
							}
							else
							{
								var amnt_mnth = a_amount[i_amt];
								amnt_mnth = parseFloat(amnt_mnth) + parseFloat(projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id][month].Amount);
								
								a_amount[i_amt] = amnt_mnth;
								i_amt++;
							}
						}
						
					
					}
				}
				
				if (mode == 2)
				{
				
					
					
					//i_total_row_revenue = parseFloat(i_total_row_revenue) + parseFloat(projectWiseRevenue[emp_internal_id].total_revenue_for_tenure);
					i_total_row_revenue = parseFloat(i_total_row_revenue) + parseFloat(f_total_per_row_cost);
				}
				
				i_frst_row = 1;
	
				
				
				i_diplay_frst_column = 1;
				
				var f_revenue_share = projectWiseRevenue[emp_internal_id].revenue_share;
			}			
		}
	}
	
	var s_sub_prac_name = '';
	for (var emp_internal_id in projectWiseRevenue)
	{
		if (projectWiseRevenue[emp_internal_id].sub_practice == sub_prac)
		{
			s_sub_prac_name = projectWiseRevenue[emp_internal_id].sub_prac_name;
			s_practice_name = projectWiseRevenue[emp_internal_id].practice_name;
			//projectWiseRevenue[emp_internal_id].revenue_share_revised = i_total_row_revenue;
			//projectWiseRevenue[emp_internal_id].total_revenue_for_tenure = i_total_row_revenue;
		}
	}
	
	if(flag_counter==0){
	for (var i_amt = 0; i_amt < a_amount.length; i_amt++)
	{
		f_total_cost_overall_prac[i_amt] = 0;
		
	}
	}
	for (var i_amt = 0; i_amt < a_amount.length; i_amt++)
	{
		flag_counter=1;
				
		f_total_cost_overall_prac[i_amt] = parseFloat(a_amount[i_amt]) + parseFloat(f_total_cost_overall_prac[i_amt]);
	}
		
	// Percent row

	
	var f_total_prcnt = 0;
	for (var i_amt = 0; i_amt < a_amount.length; i_amt++)
	{
		var f_prcnt_revenue = parseFloat(a_amount[i_amt]) / parseFloat(i_total_row_revenue);
		f_prcnt_revenue = f_prcnt_revenue * 100;
		f_total_prcnt = parseFloat(f_prcnt_revenue) + parseFloat(f_total_prcnt);
		
		
	}
	
	html += '<b>Revenue to be reconized/month';
	
	
	var f_total_revenue = 0;
	for (var i_amt = 0; i_amt < a_amount.length; i_amt++)
	{	

	
		var f_prcnt_revenue=0;
		
		f_prcnt_revenue = f_prcnt_revenue * 100;
		//nlapiLogExecution('DEBUG','Percentage',f_total_cost_breakUp);
		nlapiLogExecution('DEBUG','Amunt',parseFloat(a_amount[i_amt]));
		var f_revenue_amount = (parseFloat(f_prcnt_revenue)* parseFloat(25000) ) /100;
		//var f_revenue_amount = (parseFloat(f_prcnt_revenue) * parseFloat(f_revenue_share)) / 100;
		
		f_total_revenue = parseFloat(f_revenue_amount) + parseFloat(f_total_revenue);
		
		
	}
	
	for (var emp_internal_id in projectWiseRevenue)
	{
		for (var project_internal_id in projectWiseRevenue[emp_internal_id].RevenueData)
		{
			var i_amount_map = 0;
			if (projectWiseRevenue[emp_internal_id].sub_practice == sub_prac)
			{
				for (var month in projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id])
				{
					var a_MonthNames = MonthNames(i_year_project);
					var currentMonthPos = a_MonthNames.indexOf(month);
					var s_current_month = a_MonthNames[currentMonthPos];
					var s_current_mnth_yr = s_current_month.substr(4, 8);
					var i_nxt_yr_flg = 0;
					
					if(currentMonthPos >=12)
					{
						i_nxt_yr_flg = 1;
						currentMonthPos = currentMonthPos - 12;
					}
					
					var i_curr_yr_dis_flag = 0;
					if(parseInt(s_current_mnth_yr) < i_year_project_end && i_prject_end_mnth < currentMonthPos)
					{
						i_curr_yr_dis_flag = 1;
					}
					
					if (((i_project_mnth <= currentMonthPos && ((i_prject_end_mnth >= currentMonthPos || i_curr_yr_dis_flag == 1) && i_nxt_yr_flg == 0) && parseInt(s_current_mnth_yr) <= i_year_project) && parseInt(s_current_mnth_yr) <= i_year_project_end) || (i_prject_end_mnth >= currentMonthPos && parseInt(s_current_mnth_yr) <= i_year_project_end && i_nxt_yr_flg == 1))
					{
						var f_prcnt_revenue = parseFloat(a_amount[i_amount_map]) / parseFloat(i_total_row_revenue);
						f_prcnt_revenue = f_prcnt_revenue * 100;
						
						var f_revenue_amount = (parseFloat(f_prcnt_revenue) * parseFloat(f_revenue_share)) / 100;
		
						projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id][month].total_revenue = f_revenue_amount;
						i_amount_map++;
					}
				}	
			}
		}
	}
	
	

	return f_total_cost_overall_prac;
}
function delete_existing(s_currentMonthName,s_currentYear,subsi)
{
	var del_fil=new Array();
	var del_col=new Array();
	del_col[0]=new nlobjSearchColumn('internalid');
	del_fil[0]=new nlobjSearchFilter('custrecord_fp_othr_validate_mnth',null,'is',s_currentMonthName);
	del_fil[1]=new nlobjSearchFilter('custrecord_fp_othr_validate_year',null,'is',s_currentYear);
	del_fil[2]=new nlobjSearchFilter('custrecord_validate_subsi',null,'anyof',subsi);
	var del_search=nlapiSearchRecord('customrecord_fp_other_validate_excel_dat',null,del_fil,del_col);
	if(del_search)
	{
		for(var del_indx=0;del_indx<del_search.length;del_indx++)
		{
			var del_id=del_search[del_indx].getValue('internalid');
			var id=nlapiDeleteRecord('customrecord_fp_other_validate_excel_dat',parseInt(del_id));
		}
	}
}