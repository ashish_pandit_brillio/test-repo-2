/**
 * Delete the TimeSheet Quick Approval Pool record
 * 
 * Version Date Author Remarks 1.00 03 Sep 2015 nitish.mishra
 * 
 */

function massUpdate(recType, recId) {
	try {
		nlapiDeleteRecord(recType, recId)
	} catch (err) {
		nlapiLogExecution('error', 'massUpdate', err);
	}
}
