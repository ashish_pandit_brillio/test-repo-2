/**
 * Module Description
 * 
 * Version Date Author Remarks 1.00 17 Mar 2015 nitish.mishra
 * 
 */

/**
 * @param {nlobjRequest}
 *            request Request object
 * @param {nlobjResponse}
 *            response Response object
 * @returns {Void} Any output is written via response object
 */
function suitelet(request, response) {

	try {
		var mode = request.getParameter("mode");

		if (mode == "EXCEL") {
			exportToExcel();
		} else if (mode == "DRILLDOWN") {
			generateDrillDownReport(request);
		} else {
			createAllocationTable();
		}
	} catch (err) {
		displayErrorForm(err);
	}
}

function exportToExcel() {

	try {
		var deployment = nlapiGetContext().getDeploymentId();

		if (deployment == "customdeploy_sut_allocation_rep_vertical") {
			identifierColumn = "Vertical";
			tableColumnHeader = "Customer Vertical";
		} else if (deployment == "customdeploy_sut_allocation_rep_practice") {
			identifierColumn = "Practice";
			tableColumnHeader = "Employee Practice";
		} else if (deployment == "customdeploy_sut_allocation_rep_customer") {
			identifierColumn = "Customer";
			tableColumnHeader = "Customer";
		} else {
			throw "Wrong deployment";
		}

		var allocationData = getAllAllocation();
		var excelData = "";

		var classificationList_1 = getNormalClassification();
		var classificationList_2 = getProjectClassification();

		var allocationList = pullAllocation(allocationData,
		        classificationList_1, classificationList_2);

		// create normal view
		excelData += 'Allocation View\n';
		excelData += generateExcelTable(allocationList.Report1,
		        classificationList_1, tableColumnHeader);

		// create billing view
		excelData += '\n\nBilling View\n';
		excelData += generateExcelTable(allocationList.Report2,
		        classificationList_2, tableColumnHeader);

		var fileName = 'Allocation Report.csv';
		var file = nlapiCreateFile(fileName, 'CSV', excelData);
		response.setContentType('CSV', fileName);
		response.write(file.getValue());
	} catch (err) {
		nlapiLogExecution('ERROR', 'exportToExcel', err);
		throw err;
	}
}

function createAllocationTable() {

	try {
		var form = nlapiCreateForm('Allocation Report', false);
		var allocationData = getAllAllocation();

		var context = nlapiGetContext();
		var deployment = context.getDeploymentId();

		var url = nlapiResolveURL('SUITELET', context.getScriptId(), deployment);
		url += "&mode=EXCEL";

		var tableData = getAllocationTableData(allocationData);
		var html = "";
		html += "<p class='reportLabel'>Allocation View</p>";
		html += tableData.Table1;
		html += "<p class='reportLabel'>Billing View</p>";
		html += tableData.Table2;
		html += "<br/><br/><a href='" + url
		        + "' target='_blank'>Export To Excel</a>";

		form.addField('custpage_0', 'inlinehtml', '').setDefaultValue(html);
		response.writePage(form);
	} catch (err) {
		nlapiLogExecution('ERROR', 'createAllocationTable', err);
		throw err;
	}
}

function getAllocationTableData(allocationData) {

	try {
		var deployment = nlapiGetContext().getDeploymentId();
		var identifierColumn = null, tableColumnHeader = null;

		if (deployment == "customdeploy_sut_allocation_rep_vertical") {
			identifierColumn = "Vertical";
			tableColumnHeader = "Customer Vertical";
		} else if (deployment == "customdeploy_sut_allocation_rep_practice") {
			identifierColumn = "Practice";
			tableColumnHeader = "Employee Practice";
		} else if (deployment == "customdeploy_sut_allocation_rep_customer") {
			identifierColumn = "Customer";
			tableColumnHeader = "Project";
		} else {
			throw "Wrong deployment";
		}

		var classificationList_1 = getNormalClassification();
		var classificationList_2 = getProjectClassification();

		var allocationList = pullAllocation(allocationData,
		        classificationList_1, classificationList_2, identifierColumn);
		return {
		    Table1 : generateTable(allocationList.Report1,
		            classificationList_1, tableColumnHeader),
		    Table2 : generateTable(allocationList.Report2,
		            classificationList_2, tableColumnHeader)
		}
	} catch (err) {
		nlapiLogExecution('ERROR', 'getAllocationTableData', err);
		throw err;
	}
}

// return a list of all the project classification
function getProjectClassification() {

	try {
		var classificationList = [];
		var K_REVENUE = 2;

		classificationList.push({
		    Id : '',
		    Name : 'Billed'
		});

		classificationList.push({
		    Id : '',
		    Name : 'Unbilled'
		});

		nlapiSearchRecord(
		        'customlist_job_allocation_category',
		        null,
		        [ new nlobjSearchFilter('name', null, 'isnot', 'Revenue'),
		                new nlobjSearchFilter('name', null, 'isnot', 'Trainee') ],
		        new nlobjSearchColumn('name')).forEach(function(classification)
		{

			classificationList.push({
			    Id : classification.getId(),
			    Name : classification.getValue('name')
			});
		});

		classificationList.push({
		    Id : classificationList.length,
		    Name : 'Total'
		});

		return classificationList;
	} catch (err) {
		nlapiLogExecution('ERROR', 'getProjectClassification', err);
		throw err;
	}
}

function addNewNodeBillingView(allocationList, nodeName, classificationList) {

	try {
		allocationList.push({
		    NodeName : nodeName,
		    NodeId : '',
		    OffSite : {},
		    OnSite : {},
		    Total : 0
		});

		var nodeIndex = allocationList.length - 1;

		classificationList.forEach(function(classification) {

			allocationList[nodeIndex].OffSite[classification.Name] = 0;
			allocationList[nodeIndex].OnSite[classification.Name] = 0;
		});

	} catch (err) {
		nlapiLogExecution('ERROR', 'addNewNodeBillingView', err);
		throw err;
	}
}

function generateTable(allocationList, projectClassificationList, headerLabel) {

	try {
		var colspan = projectClassificationList.length;

		// add the CSS
		var htmlText = getTableCss();

		htmlText += "<table class='tableCss' width='100%'>";

		// First Row
		htmlText += "<tr class='headRow boldCell'>" + "<td rowspan=2>"
		        + headerLabel + "</td>" + "<td colspan=" + colspan
		        + ">OffSite</td>" + "<td colspan=" + colspan + ">OnSite</td>"
		        + "<td rowspan=2>Grand Total</td>" + "</tr>";

		// Second Row
		htmlText += "<tr class='headRow boldCell'>";
		projectClassificationList.forEach(function(classification) {

			htmlText += "<td>" + classification.Name + "</td>";
		});

		projectClassificationList.forEach(function(classification) {

			htmlText += "<td>" + classification.Name + "</td>";
		});

		htmlText += "</tr>";

		var line = "";
		var data = "";
		var row = null;
		var ctr = 0;

		// add Rows from allocation
		for (var i = 0; i < allocationList.length - 1; i++) {
			row = allocationList[i];
			line = "<tr>";
			line += "<td class='leftCell boldCell headColumn'>" + row.NodeName
			        + "</td>";

			// Off Site
			ctr = 0;
			projectClassificationList.forEach(function(classification) {

				ctr += 1;

				if (ctr != colspan) {
					line += "<td class='OffSiteCell'>"
					        + formatValue(row.OffSite[classification.Name])
					        + "</td>";
				} else {
					line += "<td class='totalCell OffSiteCell'>"
					        + formatValue(row.OffSite[classification.Name])
					        + "</td>";
				}
			});

			// On Site
			ctr = 0;
			projectClassificationList.forEach(function(classification) {

				ctr += 1;
				if (ctr != colspan) {
					line += "<td class='OnSiteCell'>"
					        + formatValue(row.OnSite[classification.Name])
					        + "</td>";
				} else {
					line += "<td class='totalCell OnSiteCell'>"
					        + formatValue(row.OnSite[classification.Name])
					        + "</td>";
				}
			});
			line += "<td class='boldCell totalColumn'>"
			        + formatValue(row.Total) + "</td>";
			line += "</tr>";
			data += line;
		}

		// add grand total row
		row = allocationList[allocationList.length - 1];
		line = "<tr class='totalRow'>";
		line += "<td class='leftCell boldCell'>" + row.NodeName + "</td>";

		ctr = 0;
		projectClassificationList.forEach(function(classification) {

			ctr += 1;
			if (ctr != colspan) {
				line += "<td>" + formatValue(row.OffSite[classification.Name])
				        + "</td>";
			} else {
				line += "<td>" + formatValue(row.OffSite[classification.Name])
				        + "</td>";
			}
		});

		ctr = 0;
		projectClassificationList.forEach(function(classification) {

			ctr += 1;
			if (ctr != colspan) {
				line += "<td>" + formatValue(row.OnSite[classification.Name])
				        + "</td>";
			} else {
				line += "<td>" + formatValue(row.OnSite[classification.Name])
				        + "</td>";
			}
		});

		line += "<td class='boldCell'>" + formatValue(row.Total) + "</td>";
		line += "</tr>";
		data += line;

		htmlText += data;
		htmlText += "</table>";
		return htmlText;
	} catch (err) {
		nlapiLogExecution('ERROR', 'generateTable', err);
		throw err;
	}
}

function generateExcelTable(allocationList, projectClassificationList,
        headerLabel)
{

	try {
		var colspan = projectClassificationList.length - 1;

		// generate header line 1
		var header1 = "," + "OffSite,";

		for (var i = 0; i < colspan; i++) {
			header1 += ",";
		}

		header1 += "OnSite,";

		for (var i = 0; i < colspan; i++) {
			header1 += ",";
		}

		header1 += "Grant Total\n";

		// generate header line 2
		var header2 = headerLabel + ",";
		projectClassificationList.forEach(function(classification) {

			header2 += classification.Name + ",";
		});
		projectClassificationList.forEach(function(classification) {

			header2 += classification.Name + ",";
		});
		header2 += ",\n";

		// ---
		var line = "";
		var data = "";
		var row = null;
		var ctr = 0;

		// add Rows from allocation
		for (var i = 1; i < allocationList.length - 1; i++) {
			row = allocationList[i];
			line = "";
			line += row.NodeName + ",";

			// Off Site
			ctr = 0;
			projectClassificationList
			        .forEach(function(classification) {

				        ctr += 1;

				        if (ctr != colspan) {
					        line += formatValue(row.OffSite[classification.Name])
					                + ",";
				        } else {
					        line += formatValue(row.OffSite[classification.Name])
					                + ",";
				        }
			        });

			// On Site
			ctr = 0;
			projectClassificationList.forEach(function(classification) {

				ctr += 1;
				if (ctr != colspan) {
					line += formatValue(row.OnSite[classification.Name]) + ",";
				} else {
					line += formatValue(row.OnSite[classification.Name]) + ",";
				}
			});
			line += formatValue(row.Total) + "\n";
			data += line;
		}

		row = allocationList[0];
		line = "";
		line += row.NodeName + ",";

		// Off Site
		ctr = 0;
		projectClassificationList.forEach(function(classification) {

			ctr += 1;

			if (ctr != colspan) {
				line += formatValue(row.OffSite[classification.Name]) + ",";
			} else {
				line += formatValue(row.OffSite[classification.Name]) + ",";
			}
		});

		// On Site
		ctr = 0;
		projectClassificationList.forEach(function(classification) {

			ctr += 1;
			if (ctr != colspan) {
				line += formatValue(row.OnSite[classification.Name]) + ",";
			} else {
				line += formatValue(row.OnSite[classification.Name]) + ",";
			}
		});
		line += formatValue(row.Total) + "\n";
		data += line;

		// create the final CSV data
		data = header1 + header2 + data;
		return data;
	} catch (err) {
		nlapiLogExecution('ERROR', 'generateExcelTable', err);
		throw err;
	}
}

/*
 * get the css for the tables
 */
function getTableCss() {

	return "<style>"
	        + ".leftCell { text-align : left !important;}"
	        + ".boldCell { font-weight: bold}"
	        + ".tableCss td { text-align : center }"
	        + ".OffSiteCell{background-color: #FFFFE6}"
	        + ".OnSiteCell{background-color: #EBFFEB}"
	        + ".totalCell{}"
	        + ".headRow{background-color: #607998 !important; color: white;}"
	        + ".totalRow{background-color: #607998 !important; color: white;}"
	        + ".totalColumn{}"
	        + ".headColumn{}"
	        + ".tableCss{}"
	        + ".reportLabel{ font-size: 20px;text-align : center; color: #607998; }"
	        + ".utilization{background-color: #FFFFE6}" + "</style>";
}

/*
 * get the columns for the normal allocation table
 */
function getNormalClassification() {

	return [ {
	    Id : '',
	    Name : 'Billed'
	}, {
	    Id : '',
	    Name : 'Unbilled'
	}, {
	    Id : '',
	    Name : 'Total'
	} ];
}

// ---

function generateDrillDownReport(request) {

	try {

		var deployment = nlapiGetContext().getDeploymentId();

		var parentVertical = request.getParameter('vertical');
		var vertical = getAllChildVertical(parentVertical);

		var parentPractice = request.getParameter('practice');
		var practice = getAllChildPractice(parentPractice);

		var site = request.getParameter('site');
		var column = request.getParameter('column');
		var table = request.getParameter('table');
		var classificationList = null;
		var employeeList = [];
		var formHeader = '';

		if (table == 'normal') {
			classificationList = getNormalClassification();
		} else if (table == 'billing') {
			classificationList = '';
		}

		var identifier = null, identifierList = null;

		if (deployment == 'customdeploy_sut_allocation_rep_vertical') {
			identifier = "Vertical";
			identifierList = getVerticalList();
			formHeader = 'Allocation Report - Vertical';
		} else if (deployment == 'customdeploy_sut_allocation_rep_practice') {
			identifier = "Practice";
			identifierList = classificationList;
			formHeader = 'Allocation Report - Practice';
		}

		var allocationData = getAllAllocation();
		var details = null;

		if (table == 'normal') {
			details = pullAllocation(allocationData, classificationList,
			        identifier, identifierList);
		} else if (table == 'billing') {
			details = pullAllocationBillingWise(allocationData,
			        classificationList, identifier, identifierList);
		}

		// pull the required node name from the details
		for (var i = 0; i < details.length; i++) {

			if (details[i].NodeName == vertical) {
				details = details[i];
			}
		}

		if (deployment == 'customdeploy_sut_allocation_rep_vertical') {
			employeeList = details[site][column];
		} else if (deployment == 'customdeploy_sut_allocation_rep_practice') {
			employeeList = details[site][column];
		}

		var form = nlapiCreateForm(formHeader, false);
		var htmlText = getDrillDownTable(employeeList);

		form.addField('custpage_0', 'inlinehtml', '').setDefaultValue(htmlText);

		response.writePage(form);

	} catch (err) {
		nlapiLogExecution('ERROR', 'generateDrillDownReport', err);
		throw err;
	}
}

function getDrillDownTable(employeeList) {

	// nlapiLogExecution('debug', 'employeeList', employeeList.length);
	var htmlText = getTableCss();

	htmlText += "<table class='tableCss' width='100%'>";

	htmlText += "<tr class='headRow'>";
	htmlText += "<td>Resource</td>";
	htmlText += "<td>Department</td>";
	htmlText += "<td>Customer</td>";
	htmlText += "<td>Project</td>";
	htmlText += "<td>Vertical</td>";
	htmlText += "</tr>";

	employeeList.forEach(function(employee) {

		htmlText += "<tr>";
		htmlText += "<td class='OffSiteCell'>" + employee.Name + "</td>";
		htmlText += "<td class='OffSiteCell'>" + employee.Practice + "</td>";
		htmlText += "<td class='OffSiteCell'>" + employee.Customer + "</td>";
		htmlText += "<td class='OffSiteCell'>" + employee.Project + "</td>";
		htmlText += "<td class='OffSiteCell'>" + employee.Vertical + "</td>";
		htmlText += "</tr>";
	});

	htmlText += "</table>";

	return htmlText;
}

function formatValue(value) {

	value = parseFloat(value.toFixed(2)).toString();
	return value;

	// check if it has any decimal value
	var hasDecimal = value % 1 > 0;
	nlapiLogExecution('debug', 'has decimal', hasDecimal);

	if (!hasDecimal) {
		return parseInt(value);
	} else {
		return value.toFixed(2);
	}
}

function pullAllocation(searchResult, classificationList1, classificationList2,
        verticalFilter, practiceFilter)
{

	try {
		var identifierColumn = null;
		var deployment = nlapiGetContext().getDeploymentId();

		if (deployment == "customdeploy_sut_allocation_rep_vertical"
		        || deployment == "customdeploy_st_utilization_rep_vertical") {
			identifierColumn = "Vertical";
		} else if (deployment == "customdeploy_sut_allocation_rep_practice"
		        || deployment == "customdeploy_st_utilization_rep_practice") {
			identifierColumn = "Practice";
		} else if (deployment == "customdeploy_sut_allocation_rep_customer"
		        || deployment == "customdeploy_st_utilization_rep_customer") {
			identifierColumn = "Customer";
		} else {
			throw "Wrong deployment";
		}

		// get the header column index
		var firstSearchRow = searchResult[0];

		var table_1_header_column = -1;
		var table_2_header_column = -1;
		var practice_header_column = -1;
		var vertical_header_column = -1;
		var customer_header_column = -1;
		var site_column = -1;

		var columns = firstSearchRow.getAllColumns();
		var columnLen = columns.length;

		for (var i = 0; i < columnLen; i++) {
			var column = columns[i];
			var label = column.getLabel();

			if (label == 'Header_Table_1') {
				table_1_header_column = column;
			} else if (label == 'Header_Table_2') {
				table_2_header_column = column;
			} else if (label == 'Practice_Table_Header') {
				practice_header_column = column;
			} else if (label == 'Vertical_Table_Header') {
				vertical_header_column = column;
			} else if (label == 'Customer_Table_Header') {
				customer_header_column = column;
			} else if (label == 'Resource_Site') {
				site_column = column;
			}
		}

		var reportList_1 = [];
		var reportList_2 = [];
		var nodesList = [];
		var customer, customerName, project, projectname, site_column;
		var employee, customerVertical, resourcePractice, resourcePracticeName;
		var was_added_1, was_added_2;
		var resourceSite, isResourceBillable, projectClassification, projectClassificationName;
		var table_header_1, table_header_2, allocatedUnit, identifierValue;

		// adding the grand total as the first element
		var total_row_name = "Grand Total";
		addNewNodeBillingView(reportList_1, total_row_name, classificationList1);
		addNewNodeBillingView(reportList_2, total_row_name, classificationList2);
		nodesList.push(total_row_name);
		var grandTotalRowIndex = 0;

		searchResult
		        .forEach(function(allocation) {

			        // read the values from search row
			        customerName = allocation
			                .getText('customer', null, 'group');
			        project = allocation.getValue('company', null, 'group');
			        projectName = allocation.getText('company', null, 'group');

			        customerVertical = allocation.getValue(
			                'custentity_vertical', 'customer', 'group');
			        customerVerticalName = allocation.getText(
			                'custentity_vertical', 'customer', 'group');

			        projectVerticalName = allocation.getText(
			                'custentity_vertical', 'job', 'group');

			        resourceFunction = allocation.getValue(
			                'custentity_emp_function', 'employee', 'group');

			        resourcePractice = allocation.getValue(
			                'departmentnohierarchy', 'employee', 'group');
			        resourcePracticeName = allocation.getText(
			                'departmentnohierarchy', 'employee', 'group');

			        resourceSite = allocation.getValue('custevent4', null,
			                'group');
			        isResourceBillable = allocation.getValue(
			                'custeventrbillable', null, 'group');
			        parentPractice = allocation.getValue('formulatext', null,
			                'group');
			        projectClassification = allocation.getValue(
			                'custentity_project_allocation_category', 'job',
			                'group');
			        projectClassificationName = allocation.getText(
			                'custentity_project_allocation_category', 'job',
			                'group');

			        allocatedUnit = parseFloat(allocation.getValue(
			                'percentoftime', null, 'group').split('%')[0]) / 100;
			        site = allocation.getValue(site_column);

			        table_header_1 = allocation.getValue(table_1_header_column);
			        table_header_2 = allocation.getValue(table_2_header_column);
			        customerName = allocation.getValue(customer_header_column);

			        employee = allocation.getValue('resource', null, 'group');

			        // check if its a vertical or practice report
			        if (identifierColumn == "Vertical") {
				        identifierValue = customerVerticalName;

				        // pull the project vertical
				        // Brillio is 6
				        if (customerVertical == "6") {
					        // identifierValue = projectVerticalName;
				        }
			        } else if (identifierColumn == "Practice") {
				        identifierValue = parentPractice;

				        // RMG Projects are to be divided into subcategories
				        if (resourcePractice == 'RMG - Bench (IN)') {
					        identifierValue = 'Resource Management Group : RMG Bench';
				        }
				        // else if (resourcePractice == 'RMG - Trainee (IN)') {
				        // identifierValue = 'Resource Management Group : RMG
				        // Trainee';
				        // }
			        } else if (identifierColumn == "Customer") {
				        identifierValue = customerName;
			        }

			        // no project classification, allocation is excluded
			        if (isEmpty(projectClassification)) {
				        return true;
			        }

			        // find the position of the identifier in the main array
			        var nodeIndex = -1;

			        for (var j = 0; j < nodesList.length; j++) {
				        if (identifierValue.trim().toUpperCase() == nodesList[j]
				                .trim().toUpperCase()) {
					        nodeIndex = j;
				        }
			        }

			        // new identifier, add it
			        if (nodeIndex == -1) {
				        addNewNodeBillingView(reportList_1, identifierValue,
				                classificationList1);
				        addNewNodeBillingView(reportList_2, identifierValue,
				                classificationList2);
				        nodesList.push(identifierValue);
				        nodeIndex = nodesList.length - 1;
			        }

			        nlapiLogExecution('debug', 'check B',
			                'nodes added, index : ' + nodeIndex);

			        nlapiLogExecution('debug', 'reportList_1', JSON
			                .stringify(reportList_1));
			        nlapiLogExecution('debug', 'reportList_2', JSON
			                .stringify(reportList_2));

			        var site = "";
			        if (isOnSite(resourceSite)) {
				        site = "OnSite";
			        } else {
				        site = "OffSite";
			        }

			        // table 1
			        reportList_1[nodeIndex][site][table_header_1] += parseFloat(allocatedUnit);
			        reportList_1[nodeIndex][site]['Total'] += parseFloat(allocatedUnit);
			        reportList_1[grandTotalRowIndex][site][table_header_1] += parseFloat(allocatedUnit);
			        reportList_1[grandTotalRowIndex][site]['Total'] += parseFloat(allocatedUnit);
			        reportList_1[nodeIndex]['Total'] += parseFloat(allocatedUnit);
			        reportList_1[grandTotalRowIndex]['Total'] += parseFloat(allocatedUnit);
			        // reportList_1[nodeIndex].NodeId = identifierValue;

			        // table 2
			        reportList_2[nodeIndex][site][table_header_2] += parseFloat(allocatedUnit);
			        reportList_2[nodeIndex][site]['Total'] += parseFloat(allocatedUnit);
			        reportList_2[grandTotalRowIndex][site][table_header_2] += parseFloat(allocatedUnit);
			        reportList_2[grandTotalRowIndex][site]['Total'] += parseFloat(allocatedUnit);
			        reportList_2[nodeIndex]['Total'] += parseFloat(allocatedUnit);
			        reportList_2[grandTotalRowIndex]['Total'] += parseFloat(allocatedUnit);
			        // reportList_2[nodeIndex].NodeId = identifierValue;
		        });

		return {
		    Report1 : reportList_1,
		    Report2 : reportList_2
		};
	} catch (err) {
		nlapiLogExecution('ERROR', 'pullAllocation', err);
		throw err;
	}
}

function generateTable(allocationList, projectClassificationList, headerLabel) {

	try {
		var colspan = projectClassificationList.length;

		// add the CSS
		var htmlText = getTableCss();

		htmlText += "<table class='tableCss' width='100%'>";

		// First Row
		htmlText += "<tr class='headRow boldCell'>" + "<td rowspan=2>"
		        + headerLabel + "</td>" + "<td colspan=" + colspan
		        + ">OffSite</td>" + "<td colspan=" + colspan + ">OnSite</td>"
		        + "<td rowspan=2>Grand Total</td>" + "</tr>";

		// Second Row
		htmlText += "<tr class='headRow boldCell'>";
		projectClassificationList.forEach(function(classification) {

			htmlText += "<td>" + classification.Name + "</td>";
		});

		projectClassificationList.forEach(function(classification) {

			htmlText += "<td>" + classification.Name + "</td>";
		});

		htmlText += "</tr>";

		var line = "";
		var data = "";
		var row = null;
		var ctr = 0;

		// add Rows from allocation
		for (var i = 1; i < allocationList.length; i++) {
			row = allocationList[i];
			line = "<tr>";
			line += "<td class='leftCell boldCell headColumn'>" + row.NodeName
			        + "</td>";

			// Off Site
			ctr = 0;
			projectClassificationList.forEach(function(classification) {

				ctr += 1;

				if (ctr != colspan) {
					line += "<td class='OffSiteCell'>"
					        + formatValue(row.OffSite[classification.Name])
					        + "</td>";
				} else {
					line += "<td class='totalCell OffSiteCell'>"
					        + formatValue(row.OffSite[classification.Name])
					        + "</td>";
				}
			});

			// On Site
			ctr = 0;
			projectClassificationList.forEach(function(classification) {

				ctr += 1;
				if (ctr != colspan) {
					line += "<td class='OnSiteCell'>"
					        + formatValue(row.OnSite[classification.Name])
					        + "</td>";
				} else {
					line += "<td class='totalCell OnSiteCell'>"
					        + formatValue(row.OnSite[classification.Name])
					        + "</td>";
				}
			});
			line += "<td class='boldCell totalColumn'>"
			        + formatValue(row.Total) + "</td>";
			line += "</tr>";
			data += line;
		}

		// add grand total row
		row = allocationList[0];
		line = "<tr class='totalRow'>";
		line += "<td class='leftCell boldCell'>" + row.NodeName + "</td>";

		ctr = 0;
		projectClassificationList.forEach(function(classification) {

			ctr += 1;
			if (ctr != colspan) {
				line += "<td>" + formatValue(row.OffSite[classification.Name])
				        + "</td>";
			} else {
				line += "<td>" + formatValue(row.OffSite[classification.Name])
				        + "</td>";
			}
		});

		ctr = 0;
		projectClassificationList.forEach(function(classification) {

			ctr += 1;
			if (ctr != colspan) {
				line += "<td>" + formatValue(row.OnSite[classification.Name])
				        + "</td>";
			} else {
				line += "<td>" + formatValue(row.OnSite[classification.Name])
				        + "</td>";
			}
		});

		line += "<td class='boldCell'>" + formatValue(row.Total) + "</td>";
		line += "</tr>";
		data += line;

		htmlText += data;
		htmlText += "</table>";
		return htmlText;
	} catch (err) {
		nlapiLogExecution('ERROR', 'generateTable', err);
		throw err;
	}
}

function isOnSite(resourceSite) {

	return resourceSite == '1';
}

// function isTraineeResource(projectClassification) {

// return projectClassification == '2';
// }

// function isRevenueProject(projectClassification) {

// return projectClassification == '1';
// }

function getAllAllocation(verticalFilter, practiceFilter) {

	try {
		var resource_allocation_search = 752;

		var filters = null;
		var filterExpression = null;
		var deployment = nlapiGetContext().getDeploymentId();
		var report_access = getUserReportAccessList();

		if (deployment == "customdeploy_sut_allocation_rep_vertical"
		        || deployment == "customdeploy_st_utilization_rep_vertical") {
			filters = [];
			filters.push(new nlobjSearchFilter('custentity_vertical',
			        'customer', 'anyof', report_access.VerticalList.Values));

			// if (isNotEmpty(verticalFilter)) {
			// filters.push(new nlobjSearchFilter('custentity_vertical',
			// 'customer', 'anyof',
			// verticalFilter));
			// }

		} else if (deployment == "customdeploy_sut_allocation_rep_practice"
		        || deployment == "customdeploy_st_utilization_rep_practice") {
			filters = [];
			filters.push(new nlobjSearchFilter('department', 'employee',
			        'anyof', report_access.PracticeList.Values));

			// if (isNotEmpty(practiceFilter)) {
			// filters.push(new nlobjSearchFilter('department', 'customer',
			// employee, 'anyof',
			// practiceFilter));
			// }
		} else if (deployment == "customdeploy_sut_allocation_rep_customer"
		        || deployment == "customdeploy_st_utilization_rep_customer") {
			var customerList = report_access.CustomerList;
			var add_or = false;
			filterExpression = [];

			customerList.forEach(function(cust) {

				if (add_or) {
					filterExpression.push('or');
				}

				filterExpression.push([
				        [ 'custevent_practice', 'anyof', cust.Practice ],
				        'and', [ 'customer', 'anyof', cust.CustomerValue ] ]);

				add_or = true;
			});

			nlapiLogExecution('debug', 'filterExpression', JSON
			        .stringify(filterExpression));
		}

		var a_allocation_search = searchRecord('resourceallocation',
		        resource_allocation_search, filters, null, filterExpression);
		return a_allocation_search;
	} catch (err) {
		nlapiLogExecution('ERROR', 'getAllAllocation', err);
		throw err;
	}
}
