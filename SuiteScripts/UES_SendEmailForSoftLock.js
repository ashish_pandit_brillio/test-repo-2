//BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
    /*
   	Script Name : UES_SendEmailForSoftLock.js
	Author      : Ashish Pandit
	Date        : 10 May 2019
    Description : User Event to send email for Softlocked resource  


	Script Modification Log:

	-- Date --			-- Modified By --				--Requested By--				-- Description --
   05 July 2019           Ashish Pandit					    Deepak                      Code added for sending notification for External Hire 

Below is a summary of the process controls enforced by this script file.  The control logic is described
more fully, below, in the appropriate function headers and code blocks.


     BEFORE LOAD
		- beforeLoadRecord(type)



     BEFORE SUBMIT
		- beforeSubmitRecord(type)


     AFTER SUBMIT
		- afterSubmitRecord(type)



     SUB-FUNCTIONS
		- The following sub-functions are called by the above core functions in order to maintain code
            modularization:

               - NOT USED

	 */
}
//END SCRIPT DESCRIPTION BLOCK  ====================================



//BEGIN GLOBAL VARIABLE BLOCK  =====================================
{
    //  Initialize any Global Variables, in particular, debugging variables...

}
//END GLOBAL VARIABLE BLOCK  =======================================



//BEGIN BEFORE LOAD ==================================================

function afterSubmitSendEmailSoftlock(type, name) {
    nlapiLogExecution('Debug', 'type ', type);
    if (type == 'edit' || type == 'xedit') {
        try {

            var oldRecord = nlapiGetOldRecord();
            var i_OldSoftlockResource = oldRecord.getFieldValue('custrecord_frf_details_selected_emp');
            var i_OldExtResource = oldRecord.getFieldValue('custrecord_frf_details_taleo_emp_select');
            var b_OldExtHire = oldRecord.getFieldValue('custrecord_frf_details_external_hire');
            var s_OldSoftLockedResource = oldRecord.getFieldText('custrecord_frf_details_selected_emp');
            var i_OldTaleoRecord = oldRecord.getFieldValue('custrecord_frf_details_rrf_number');

            var b_OldIsBillable = oldRecord.getFieldValue('custrecord_frf_details_billiable');
            var s_OldAllocation = oldRecord.getFieldValue('custrecord_frf_details_allocation');
            //var b_OldIsCriticalRole = oldRecord.getFieldValue('custrecord_frf_details_critical_role');
            //var i_OldRequestType = oldRecord.getFieldValue('custrecord_frf_type');
            var s_OldBillRate = oldRecord.getFieldValue('custrecord_frf_details_bill_rate');
            var d_OldStartDate = oldRecord.getFieldValue('custrecord_frf_details_start_date');

            var d_oldDesiredDate = oldRecord.getFieldValue('custrecord_frf_desired_start_date'); //prabhat gupta 12/6/2020

            var d_OldEndDate = oldRecord.getFieldValue('custrecord_frf_details_end_date');
            var o_OldDesiredDate = oldRecord.getFieldValue('custrecord_frf_desired_start_date');
            var recId = nlapiGetRecordId();
            var recType = nlapiGetRecordType();
            var recordObject = nlapiLoadRecord(recType, recId);
            var i_NewSoftlockResource = recordObject.getFieldValue('custrecord_frf_details_selected_emp');
            var i_NewExtResource = recordObject.getFieldValue('custrecord_frf_details_taleo_emp_select');
            var b_NewExtHire = recordObject.getFieldValue('custrecord_frf_details_external_hire');
            var i_NewSoftLockedResource = recordObject.getFieldValue('custrecord_frf_details_selected_emp');
			
			

            var taleo_record = recordObject.getFieldValue('custrecord_frf_details_rrf_number'); //prabhat gupta

            var b_NewIsBillable = recordObject.getFieldValue('custrecord_frf_details_billiable');
            var s_NewAllocation = recordObject.getFieldValue('custrecord_frf_details_allocation');
            //nlapiLogExecution('Debug','s_NewAllocation ',s_NewAllocation);
            //nlapiLogExecution('Debug','s_OldAllocation ',s_OldAllocation);
            //nlapiLogExecution('Debug','s_NewAllocation parseInt ',parseInt(s_NewAllocation));
            //nlapiLogExecution('Debug','s_OldAllocation parseInt ',parseInt(s_OldAllocation));
            //var b_NewIsCriticalRole = recordObject.getFieldValue('custrecord_frf_details_critical_role');
            //var i_NewRequestType = recordObject.getFieldValue('custrecord_frf_type');
            var s_NewBillRate = recordObject.getFieldValue('custrecord_frf_details_bill_rate');
            var d_NewStartDate = recordObject.getFieldValue('custrecord_frf_details_start_date');
            var d_NewEndDate = recordObject.getFieldValue('custrecord_frf_details_end_date');
			
			
			var old_redDate = oldRecord.getFieldValue("custrecord_frf_details_red_date");
			var redDate = recordObject.getFieldValue('custrecord_frf_details_red_date');
			
			if((old_redDate != redDate)&& (d_OldEndDate == d_NewEndDate)){
				return;
			}
			
			
            var d_NewDesiredDate = recordObject.getFieldValue('custrecord_frf_desired_start_date'); //prabhat gupta 12/6/2020

            // for getting the desired  field value   Added by gaurav jaiswal 18/02/2020
            var d_desired_date = recordObject.getFieldValue('custrecord_frf_desired_start_date');

            //nlapiLogExecution('Debug','i_NewSoftlockResource ',i_NewSoftlockResource);
            var i_project = recordObject.getFieldValue('custrecord_frf_details_project');
            var i_account = recordObject.getFieldText('custrecord_frf_details_account');
            var i_acc_id = recordObject.getFieldValue('custrecord_frf_details_account');
            var s_project = recordObject.getFieldText('custrecord_frf_details_project');
            var i_opp = recordObject.getFieldValue('custrecord_frf_details_opp_id');
            var frfNumber = recordObject.getFieldValue('custrecord_frf_details_frf_number');
            var d_startDate = recordObject.getFieldValue('custrecord_frf_details_start_date');
            var d_endDate = recordObject.getFieldValue('custrecord_frf_details_end_date');
            var i_allocation = recordObject.getFieldValue('custrecord_frf_details_allocation');
            var s_location = recordObject.getFieldText('custrecord_frf_details_res_location');
            var i_billRate = recordObject.getFieldValue('custrecord_frf_details_bill_rate');
            var b_isBillable = recordObject.getFieldValue('custrecord_frf_details_billiable');
            var userName = recordObject.getFieldText('custrecord_frf_details_softlocked_by');

            //---------------------------------------------------------------------------------------------------------------			
            //prabhat gupta
            var softlockBy = recordObject.getFieldValue('custrecord_frf_details_softlocked_by');

            var createdBy = recordObject.getFieldValue('custrecord_frf_details_created_by');

            //----------------------------------------------------------------------------------------------------------------			

            var d_billingStartDate = '';
            var d_billingEndDate = '';
            //nlapiLogExecution('Debug','b_isBillable ',b_isBillable);
            var s_billable = 'Non-billable';

            // created a variable if desired date is present then it is stored else resource start date is stored Added by gaurav jaiswal 28/02/2020
            var desire_or_st_date = "";
            if (d_desired_date) {
                desire_or_st_date = d_desired_date;
            } else {
                desire_or_st_date = d_startDate;
            }



            if (b_isBillable == 'T') {

                // if desired date is present then putting the desired else the resource start date  Added by gaurav jaiswal 18/02/2020

                if (d_desired_date) {
                    d_billingStartDate = d_desired_date
                } else {
                    d_billingStartDate = d_startDate;
                }
                d_billingEndDate = d_endDate;
                s_billable = 'Billable';
            }
            if (!i_OldSoftlockResource && i_NewSoftlockResource || (i_NewSoftlockResource && i_OldSoftlockResource && (i_OldSoftlockResource != i_NewSoftlockResource)))
            //if(!i_OldSoftlockResource && i_NewSoftlockResource)
            {
                //var recordObject = nlapiLoadRecord(recType,recId);
                var selectedResources = recordObject.getFieldText('custrecord_frf_details_selected_emp');
                var ccArray = new Array();
                var toArray = new Array();
                //-----------------------------------------------------------------------------------------------------------			
                //prabhat gupta
                if (softlockBy) {
                    var softlockByEmail = nlapiLookupField('employee', softlockBy, 'email');
                    ccArray.push(softlockByEmail);
                }

                if (createdBy) {
                    var createdByEmail = nlapiLookupField('employee', createdBy, 'email');
                    ccArray.push(createdByEmail);
                }
                //-----------------------------------------------------------------------------------------------------------			
                if (_logValidation(i_opp)) {
                    s_project = nlapiLookupField('customrecord_sfdc_opportunity_record', i_opp, 'custrecord_opportunity_name_sfdc');
                    s_oppId = nlapiLookupField('customrecord_sfdc_opportunity_record', i_opp, 'custrecord_opportunity_id_sfdc');
                    var s_oppName = s_oppId + ', ' + s_project;
                    var i_pmOpp = nlapiLookupField('customrecord_sfdc_opportunity_record', i_opp, 'custrecord_sfdc_opp_pm');
                    var i_dmOpp = nlapiLookupField('customrecord_sfdc_opportunity_record', i_opp, 'custrecord_sfdc_opportunity_dm');
                    var i_cpOpp = nlapiLookupField('customrecord_sfdc_opportunity_record', i_opp, 'custrecord_sfdc_client_partner');
                    if (i_pmOpp)
                        toArray.push(i_pmOpp);
                    if (i_dmOpp)
                        toArray.push(i_dmOpp);
                    if (i_cpOpp)
                        var s_cpOppEmail = nlapiLookupField('employee', i_cpOpp, 'email');
                    if (s_cpOppEmail)
                        ccArray.push(s_cpOppEmail);
                    ccArray.push('business.ops@brillio.com'); //Business Operations Team
                    ccArray.push('fuel.support@brillio.com');
                    //nlapiLogExecution('Debug','ccArray i_opp ',ccArray);
                } else if (_logValidation(i_project)) {
                    var i_pm = nlapiLookupField('job', i_project, 'custentity_projectmanager');
                    var s_pm = nlapiLookupField('job', i_project, 'custentity_projectmanager', true);
                    var i_dm = nlapiLookupField('job', i_project, 'custentity_deliverymanager');
                    var i_cp = nlapiLookupField('job', i_project, 'custentity_clientpartner');
                    if (i_dm)
                        var s_dmEmail = nlapiLookupField('employee', i_dm, 'email');
                    if (s_dmEmail)
                        ccArray.push(s_dmEmail);
                    if (i_cp)
                        var s_cpEmail = nlapiLookupField('employee', i_cp, 'email');
                    if (s_cpEmail)
                        ccArray.push(s_cpEmail);
                    ccArray.push('business.ops@brillio.com'); //Business Operations Team
                    ccArray.push('fuel.support@brillio.com');
                    //nlapiLogExecution('Debug','ccArray i_project ',ccArray);
                }

                if (_logValidation(i_pm)) {


                    //nlapiLogExecution('Debug','i_pm ',i_pm);
                    var records = new Array();
                    var emailSubject = frfNumber + ': FUEL Notification: Employee Soft Locked'
                    var emailBody = 'Dear ' + s_pm + '<br>' + selectedResources + ' has been soft locked for FRF ' +
                        frfNumber + '.Opportunity/Project ' + s_project + ' and Account ' + i_account + '<br><br>Details for Business Operations Team:<br><br><B>Resource Name:</B> ' +
                        selectedResources + '<br><\B>Project Name: <\/B>' + s_project + '<br><B>Start date: </B>' +
                        desire_or_st_date + '<br><B>End date:</B> ' + d_endDate + '<br><B>Percentage of Time: </B>' + i_allocation + '<br><B>Billing start date: </B>' + d_billingStartDate + '<br><B>Billing end date: </B>' + d_billingEndDate + '<br><B>Location: </B>' + s_location + '<br><B>Bill rates: </B>' + i_billRate + '<br><B>Billable Status:</B> ' + s_billable + '<br><br>Brillio-FUEL<br>This email was sent from a notification only address.<br>If you need further assistance, please write to fuel.support@brillio.com';
                    //nlapiLogExecution('Debug','emailSubject ',emailSubject);
                    //nlapiLogExecution('Debug','emailBody ',emailBody);
                    records['entity'] = 140512;
                    //nlapiSendEmail(154256, 94862, emailSubject,emailBody, ccArray, 'deepak.srinivas@brillio.com', records);
                    nlapiSendEmail(154256, i_pm, emailSubject, emailBody, ccArray, 'deepak.srinivas@brillio.com', records);
                    nlapiLogExecution('Debug', 'ccArray ', ccArray);
                } else if (toArray.length > 0) {

                    var records = new Array();
                    var emailSubject = frfNumber + ': FUEL Notification: Employee Soft Locked'
                    var emailBody = 'Dear All<br>' + selectedResources + ' has been soft locked for FRF ' + frfNumber + '.Opportunity/Project ' + s_oppName + ' and Account ' + i_account + '<br><br>Details for Business Operations Team:<br><br><B>Resource Name:</B> ' + selectedResources + '<br><B>Opportunity Name:</B> ' + s_oppName + '<br><B>Start date: </B>' +
                        desire_or_st_date + '<br><B>End date:</B> ' + d_endDate + '<br><B>Percentage of Time:</B> ' + i_allocation + '<br><B>Billing start date: </B>' + d_billingStartDate + '<br><B>Billing end date: </B>' + d_billingEndDate + '<br><B>Location: </B>' + s_location + '<br><B>Bill rates: </B>' + i_billRate + '<br><B>Billable Status:</B> ' + s_billable + '<br><br>Brillio-FUEL<br>This email was sent from a notification only address.<br>If you need further assistance, please write to fuel.support@brillio.com';
                    records['entity'] = 140512;
                    nlapiLogExecution('Debug', 'ccArray ', ccArray);
                    nlapiSendEmail(154256, toArray, emailSubject, emailBody, ccArray, 'deepak.srinivas@brillio.com', records);
                } else {


                    nlapiLogExecution('Debug', 'Send to CC Array NM');
                    var records = new Array();
                    var emailSubject = frfNumber + ': FUEL Notification: Employee Soft Locked'
                    var emailBody = 'Dear All<br>' + selectedResources + ' has been soft locked for FRF ' + frfNumber + '.Opportunity/Project ' + s_oppName + ' and Account ' + i_account + '<br><br>Details for Business Operations Team:<br><br><B>Resource Name:</B> ' + selectedResources + '<br><B>Opportunity Name:</B> ' + s_oppName + '<br><B>Start date: </B>' +
                        desire_or_st_date + '<br><B>End date:</B> ' + d_endDate + '<br><B>Percentage of Time:</B> ' + i_allocation + '<br><B>Billing start date: </B>' + d_billingStartDate + '<br><B>Billing end date: </B>' + d_billingEndDate + '<br><B>Location: </B>' + s_location + '<br><B>Bill rates: </B>' + i_billRate + '<br><B>Billable Status:</B> ' + s_billable + '<br><br>Brillio-FUEL<br>This email was sent from a notification only address.<br>If you need further assistance, please write to fuel.support@brillio.com';
                    records['entity'] = 140512;
                    //nlapiSendEmail(154256, 94862, emailSubject,emailBody, null, 'deepak.srinivas@brillio.com', records);
                    nlapiSendEmail(154256, ccArray, emailSubject, emailBody, null, 'deepak.srinivas@brillio.com', records);
                    nlapiLogExecution('Debug', 'ccArray ', ccArray);

                }
            }
            if (!i_OldExtResource && i_NewExtResource) {

                //var recordObject = nlapiLoadRecord(recType,recId);
                var selectedResources = recordObject.getFieldText('custrecord_frf_details_taleo_emp_select');

                var ccArray = new Array();
                var toArray = new Array();
                //nlapiLogExecution('Debug','s_country '+s_country,'s_state '+s_state);
                //nlapiLogExecution('Debug','s_city ',s_city);
                if (_logValidation(i_opp)) {
                    s_project = nlapiLookupField('customrecord_sfdc_opportunity_record', i_opp, 'custrecord_opportunity_name_sfdc');
                    s_oppId = nlapiLookupField('customrecord_sfdc_opportunity_record', i_opp, 'custrecord_opportunity_id_sfdc');
                    var s_oppName = s_oppId + ', ' + s_project;
                    var i_pmOpp = nlapiLookupField('customrecord_sfdc_opportunity_record', i_opp, 'custrecord_sfdc_opp_pm');
                    var i_dmOpp = nlapiLookupField('customrecord_sfdc_opportunity_record', i_opp, 'custrecord_sfdc_opportunity_dm');
                    var i_cpOpp = nlapiLookupField('customrecord_sfdc_opportunity_record', i_opp, 'custrecord_sfdc_client_partner');
                    if (i_pmOpp)
                        toArray.push(i_pmOpp);
                    if (i_dmOpp)
                        toArray.push(i_dmOpp);
                    if (i_cpOpp)
                        var s_cpOppEmail = nlapiLookupField('employee', i_cpOpp, 'email');
                    if (s_cpOppEmail)
                        ccArray.push(s_cpOppEmail);
                    ccArray.push('business.ops@brillio.com'); //Business Operations Team
                    ccArray.push('fuel.support@brillio.com');
                    nlapiLogExecution('Debug', 'ccArray opp ', ccArray);
                } else if (_logValidation(i_project)) {
                    var i_pm = nlapiLookupField('job', i_project, 'custentity_projectmanager');
                    var s_pm = nlapiLookupField('job', i_project, 'custentity_projectmanager', true);
                    var i_dm = nlapiLookupField('job', i_project, 'custentity_deliverymanager');
                    var i_cp = nlapiLookupField('job', i_project, 'custentity_clientpartner');
                    if (i_dm)
                        var s_dmEmail = nlapiLookupField('employee', i_dm, 'email');
                    if (s_dmEmail)
                        ccArray.push(s_dmEmail);
                    if (i_cp)
                        var s_cpEmail = nlapiLookupField('employee', i_cp, 'email');
                    if (s_cpEmail)
                        ccArray.push(s_cpEmail);
                    ccArray.push('business.ops@brillio.com'); //Business Operations Team
                    ccArray.push('fuel.support@brillio.com');
                    nlapiLogExecution('Debug', 'ccArray pro ', ccArray);
                }

                if (_logValidation(i_pm)) {
                    //nlapiLogExecution('Debug','i_pm ',i_pm);
                    var records = new Array();
                    var emailSubject = frfNumber + ': FUEL Notification: Employee Joined'
                    var emailBody = 'Dear ' + s_pm + '<br>' + selectedResources + ' has joined Brillio against FRF ' + frfNumber + '.Opportunity/Project ' + s_project + ' and Account ' + i_account + '<br><br>Details for Business Operations Team:<br><br><B>Resource Name:</B> ' + selectedResources + '<br><\B>Project Name: <\/B>' + s_project + '<br><B>Start date: </B>' + desire_or_st_date + '<br><B>End date:</B> ' + d_endDate + '<br><B>Percentage of Time: </B>' + i_allocation + '<br><B>Billing start date: </B>' + d_billingStartDate + '<br><B>Billing end date: </B>' + d_billingEndDate + '<br><B>Location: </B>' + s_location + '<br><B>Bill rates: </B>' + i_billRate + '<br><B>Billable Status:</B> ' + s_billable + '<br><br>Brillio-FUEL<br>This email was sent from a notification only address.<br>If you need further assistance, please write to fuel.support@brillio.com';
                    nlapiLogExecution('Debug', 'emailBody ', emailBody);
                    records['entity'] = ['171458'];
                    //nlapiSendEmail(154256, 94862, emailSubject,emailBody, ccArray, 'deepak.srinivas@brillio.com', records);
                    nlapiSendEmail(154256, i_pm, emailSubject, emailBody, ccArray, 'deepak.srinivas@brillio.com', records);
                    nlapiLogExecution('Debug', 'ccArray ', ccArray);
                } else if (toArray.length > 0) {
                    var records = new Array();
                    var emailSubject = frfNumber + ': FUEL Notification: Employee Joined'
                    var emailBody = 'Dear All<br>' + selectedResources + ' has joined Brillio against FRF ' + frfNumber + '.Opportunity/Project ' + s_oppName + ' and Account ' + i_account + '<br><br>Details for Business Operations Team:<br><br><B>Resource Name:</B> ' + selectedResources + '<br><B>Opportunity Name:</B> ' + s_oppName + '<br><B>Start date: </B>' + desire_or_st_date + '<br><B>End date:</B> ' + d_endDate + '<br><B>Percentage of Time:</B> ' + i_allocation + '<br><B>Billing start date: </B>' + d_billingStartDate + '<br><B>Billing end date: </B>' + d_billingEndDate + '<br><B>Location: </B>' + s_location + '<br><B>Bill rates: </B>' + i_billRate + '<br><B>Billable Status:</B> ' + s_billable + '<br><br>Brillio-FUEL<br>This email was sent from a notification only address.<br>If you need further assistance, please write to fuel.support@brillio.com';
                    records['entity'] = 140512;
                    //nlapiSendEmail(154256, 94862, emailSubject,emailBody, null, 'deepak.srinivas@brillio.com', records);
                    nlapiSendEmail(154256, toArray, emailSubject, emailBody, ccArray, 'deepak.srinivas@brillio.com', records);
                    nlapiLogExecution('Debug', 'ccArray ', ccArray);
                }
            }

            //==============================Code to Set Softlocked resource Blank for Ext Hire Resource ============================================
            if (b_OldExtHire == 'F' && b_NewExtHire == 'T') {

                var s_user = recordObject.getFieldText('custrecord_frf_details_last_updatedby');
                //----------------------------------------------------------------------------------------------------				
                //prabhat gupta
                if (!taleo_record) {

                    var rrfNum = createTaleoExternalHireRecord(recId);
                    recordObject.setFieldValue('custrecord_frf_details_rrf_number', rrfNum);

                }
                //---------------------------------------------------------------------------------------------------------				
                if (s_OldSoftLockedResource) {
                    var ccArray = new Array();
                    var toArray = new Array();

                    if (softlockBy) {
                        var softlockByEmail = nlapiLookupField('employee', softlockBy, 'email');
                        ccArray.push(softlockByEmail);
                    }

                    if (_logValidation(i_opp)) {
                        s_project = nlapiLookupField('customrecord_sfdc_opportunity_record', i_opp, 'custrecord_opportunity_name_sfdc');
                        s_oppId = nlapiLookupField('customrecord_sfdc_opportunity_record', i_opp, 'custrecord_opportunity_id_sfdc');
                        var s_oppName = s_oppId + ', ' + s_project;
                        var i_pmOpp = nlapiLookupField('customrecord_sfdc_opportunity_record', i_opp, 'custrecord_sfdc_opp_pm');
                        var i_dmOpp = nlapiLookupField('customrecord_sfdc_opportunity_record', i_opp, 'custrecord_sfdc_opportunity_dm');
                        var i_cpOpp = nlapiLookupField('customrecord_sfdc_opportunity_record', i_opp, 'custrecord_sfdc_client_partner');
                        if (i_pmOpp)
                            toArray.push(i_pmOpp);
                        if (i_dmOpp)
                            toArray.push(i_dmOpp);
                        if (i_cpOpp)
                            var s_cpOppEmail = nlapiLookupField('employee', i_cpOpp, 'email');
                        if (s_cpOppEmail)
                            ccArray.push(s_cpOppEmail);
                        ccArray.push('business.ops@brillio.com'); //Business Operations Team
                        ccArray.push('fuel.support@brillio.com');
                    } else if (_logValidation(i_project)) {
                        var i_pm = nlapiLookupField('job', i_project, 'custentity_projectmanager');
                        var s_pm = nlapiLookupField('job', i_project, 'custentity_projectmanager', true);
                        var i_dm = nlapiLookupField('job', i_project, 'custentity_deliverymanager');
                        var i_cp = nlapiLookupField('job', i_project, 'custentity_clientpartner');
                        if (i_dm)
                            var s_dmEmail = nlapiLookupField('employee', i_dm, 'email');
                        if (s_dmEmail)
                            ccArray.push(s_dmEmail);
                        if (i_cp)
                            var s_cpEmail = nlapiLookupField('employee', i_cp, 'email');
                        if (s_cpEmail)
                            ccArray.push(s_cpEmail);
                        ccArray.push('business.ops@brillio.com'); //Business Operations Team
                        ccArray.push('fuel.support@brillio.com');
                        nlapiLogExecution('Debug', 'ccArray ', ccArray);
                    }
                    /*******Set Softlocked resource field as blank*********/
                    recordObject.setFieldValue('custrecord_frf_details_selected_emp', '');
                    recordObject.setFieldValue('custrecord_frf_details_status_flag', 1);
                    recordObject.setFieldValue('custrecord_frf_details_open_close_status', 1);
                    var submitRecord = nlapiSubmitRecord(recordObject);
                    if (_logValidation(i_pm)) {


                        //nlapiLogExecution('Debug','i_pm ',i_pm);
                        var records = new Array();
                        var emailSubject = frfNumber + ': FUEL Notification: Stop allocation for soft locked FRF';
                        var emailBody = 'Dear All<br>' + s_user + ' has changed the FRF ' + frfNumber + ' from internal to external hire and hence the soft locked candidate ' +
                            s_OldSoftLockedResource + ' is being released back to practice pool. Please do not allocate the soft locked candidate to the ' +
                            s_project + ' & Tagging under Account ' + i_account + '<br><br>Details for Business Operations Team:<br><br><B>Resource Name:</B> ' +
                            s_OldSoftLockedResource + '<br><B>Opportunity Name:</B> ' + s_project + '<br><B>Start date: </B>' +
                            desire_or_st_date + '<br><B>End date:</B> ' + d_endDate + '<br><B>Percentage of Time:</B> ' +
                            i_allocation + '<br><B>Billing start date: </B>' + d_billingStartDate + '<br><B>Billing end date: </B>' + d_billingEndDate + '<br><B>Location: </B>' +
                            s_location + '<br><B>BiEmployee Soft Lockedll rates: </B>' + i_billRate + '<br><B>Billable Status:</B> ' +
                            s_billable + '<br><br>Brillio-FUEL<br>This email was sent from a notification only address.<br>If you need further assistance, please write to fuel.support@brillio.com';
                        nlapiLogExecution('Debug', 'emailBody ', emailBody);
                        records['entity'] = 140512;
                        //nlapiSendEmail(154256, 94862, emailSubject,emailBody, ccArray, 'deepak.srinivas@brillio.com', records);
                        nlapiSendEmail(154256, i_pm, emailSubject, emailBody, ccArray, 'deepak.srinivas@brillio.com', records);
                        nlapiLogExecution('Debug', 'ccArray ', ccArray);
                    } else if (toArray.length > 0) {



                        var records = new Array();
                        var emailSubject = frfNumber + ': FUEL Notification: Stop allocation for soft locked FRF';
                        var emailBody = 'Dear All<br>' + s_user + ' has changed the FRF ' + frfNumber + ' from internal to external hire and hence the soft locked candidate ' +
                            s_OldSoftLockedResource + ' is being released back to practice pool. Please do not allocate the soft locked candidate to the ' +
                            s_oppName + ' & Tagging under Account ' + i_account + '<br><br>Details for Business Operations Team:<br><br><B>Resource Name:</B> ' +
                            s_OldSoftLockedResource + '<br><B>Opportunity Name:</B> ' + s_oppName + '<br><B>Start date: </B>' +
                            desire_or_st_date + '<br><B>End date:</B> ' + d_endDate + '<br><B>Percentage of Time:</B> ' + i_allocation + '<br><B>Billing start date: </B>' +
                            d_billingStartDate + '<br><B>Billing end date: </B>' + d_billingEndDate + '<br><B>Location: </B>' + s_location + '<br><B>Bill rates: </B>' +
                            i_billRate + '<br><B>Billable Status:</B> ' + s_billable + '<br><br>Brillio-FUEL<br>This email was sent from a notification only address.<br>If you need further assistance, please write to fuel.support@brillio.com';
                        records['entity'] = 140512;
                        //nlapiSendEmail(154256, 94862, emailSubject,emailBody, null, 'deepak.srinivas@brillio.com', records);
                        nlapiSendEmail(154256, toArray, emailSubject, emailBody, ccArray, 'deepak.srinivas@brillio.com', records);
                        nlapiLogExecution('Debug', 'ccArray ', ccArray);
                    }

                }
            }
            /*********Remove Softlock Resourse **********/
            if (i_OldSoftlockResource && !i_NewSoftlockResource) {
                //var recordObject = nlapiLoadRecord(recType,recId);
                var userName = nlapiLookupField(nlapiGetRecordType(), nlapiGetRecordId(), 'custrecord_frf_details_softlocked_by', true);
                nlapiLogExecution('debug', 'userName ', userName);
                var selectedResources = oldRecord.getFieldText('custrecord_frf_details_taleo_emp_select');
                nlapiLogExecution('Debug', 'selectedResources ', selectedResources);
                var ccArray = new Array();
                var toArray = new Array();
                //nlapiLogExecution('Debug','s_country '+s_country,'s_state '+s_state);
                //nlapiLogExecution('Debug','s_city ',s_city);

                if (softlockBy) {
                    var softlockByEmail = nlapiLookupField('employee', softlockBy, 'email');
                    ccArray.push(softlockByEmail);
                }

                if (_logValidation(i_opp)) {
                    s_project = nlapiLookupField('customrecord_sfdc_opportunity_record', i_opp, 'custrecord_opportunity_name_sfdc');
                    s_oppId = nlapiLookupField('customrecord_sfdc_opportunity_record', i_opp, 'custrecord_opportunity_id_sfdc');
                    var s_oppName = s_oppId + ', ' + s_project;
                    var i_pmOpp = nlapiLookupField('customrecord_sfdc_opportunity_record', i_opp, 'custrecord_sfdc_opp_pm');
                    var i_dmOpp = nlapiLookupField('customrecord_sfdc_opportunity_record', i_opp, 'custrecord_sfdc_opportunity_dm');
                    var i_cpOpp = nlapiLookupField('customrecord_sfdc_opportunity_record', i_opp, 'custrecord_sfdc_client_partner');
                    if (i_pmOpp)
                        toArray.push(i_pmOpp);
                    if (i_dmOpp)
                        toArray.push(i_dmOpp);
                    if (i_cpOpp)
                        var s_cpOppEmail = nlapiLookupField('employee', i_cpOpp, 'email');
                    if (s_cpOppEmail)
                        ccArray.push(s_cpOppEmail);
                    ccArray.push('business.ops@brillio.com'); //Business Operations Team
                    ccArray.push('fuel.support@brillio.com');
                    nlapiLogExecution('Debug', 'ccArray ', ccArray);
                } else if (_logValidation(i_project)) {
                    var i_pm = nlapiLookupField('job', i_project, 'custentity_projectmanager');
                    var s_pm = nlapiLookupField('job', i_project, 'custentity_projectmanager', true);
                    var i_dm = nlapiLookupField('job', i_project, 'custentity_deliverymanager');
                    var i_cp = nlapiLookupField('job', i_project, 'custentity_clientpartner');
                    if (i_dm)
                        var s_dmEmail = nlapiLookupField('employee', i_dm, 'email');
                    if (s_dmEmail)
                        ccArray.push(s_dmEmail);
                    if (i_cp)
                        var s_cpEmail = nlapiLookupField('employee', i_cp, 'email');
                    if (s_cpEmail)
                        ccArray.push(s_cpEmail);
                    ccArray.push('business.ops@brillio.com'); //Business Operations Team
                    ccArray.push('fuel.support@brillio.com');
                    nlapiLogExecution('Debug', 'ccArray ', ccArray);
                }

                if (_logValidation(i_pm)) {
                    //nlapiLogExecution('Debug','i_pm ',i_pm);
                    var records = new Array();
                    var emailSubject = frfNumber + ': FUEL Notification: Stop allocation of soft locked employee'
                    var emailBody = 'Dear ' + s_pm + '<br>Soft locking of Emp ' + selectedResources + ' has been removed for FRF ' + frfNumber + ' by ' + userName + '. Please do not allocate the employee to ' + s_project + ' and Account ' + i_account + '<br><br>Details for Business Operations Team:<br><br><B>Resource Name:</B> ' + selectedResources + '<br><\B>Project Name: <\/B>' + s_project + '<br><B>Start date: </B>' + desire_or_st_date + '<br><B>End date:</B> ' + d_endDate + '<br><B>Percentage of Time: </B>' + i_allocation + '<br><B>Billing start date: </B>' + d_billingStartDate + '<br><B>Billing end date: </B>' + d_billingEndDate + '<br><B>Location: </B>' + s_location + '<br><B>Bill rates: </B>' + i_billRate + '<br><B>Billable Status:</B> ' + s_billable + '<br><br>Brillio-FUEL<br>This email was sent from a notification only address.<br>If you need further assistance, please write to fuel.support@brillio.com';
                    nlapiLogExecution('Debug', 'emailBody ', emailBody);
                    records['entity'] = 140512;
                    //nlapiSendEmail(154256, 94862, emailSubject,emailBody, ccArray, 'deepak.srinivas@brillio.com', records);
                    nlapiSendEmail(154256, i_pm, emailSubject, emailBody, ccArray, 'deepak.srinivas@brillio.com', records);
                    nlapiLogExecution('Debug', 'ccArray ', ccArray);
                } else if (toArray.length > 0) {
                    var records = new Array();
                    var emailSubject = frfNumber + ': FUEL Notification: Stop allocation of soft locked employee'
                    var emailBody = 'Dear All<br>Soft locking of Emp ' + selectedResources + ' has been removed for FRF ' + frfNumber + ' by ' + userName + '. Please do not allocate the employee to ' + s_oppName + ' under Account ' + i_account + '<br><br>Details for Business Operations Team:<br><br><B>Resource Name:</B> ' + selectedResources + '<br><B>Opportunity Name:</B> ' + s_oppName + '<br><B>Start date: </B>' + desire_or_st_date + '<br><B>End date:</B> ' + d_endDate + '<br><B>Percentage of Time:</B> ' + i_allocation + '<br><B>Billing start date: </B>' + d_billingStartDate + '<br><B>Billing end date: </B>' + d_billingEndDate + '<br><B>Location: </B>' + s_location + '<br><B>Bill rates: </B>' + i_billRate + '<br><B>Billable Status:</B> ' + s_billable + '<br><br>Brillio-FUEL<br>This email was sent from a notification only address.<br>If you need further assistance, please write to fuel.support@brillio.com';
                    records['entity'] = 140512;
                    //nlapiSendEmail(154256, 94862, emailSubject,emailBody, null, 'deepak.srinivas@brillio.com', records);
                    nlapiSendEmail(154256, toArray, emailSubject, emailBody, ccArray, 'deepak.srinivas@brillio.com', records);
                    nlapiLogExecution('Debug', 'ccArray ', ccArray);
                }
            }
            //=================================================================================================================
            /*********Remove Softlock Resourse **********/
            var context = nlapiGetContext();
            var scriptContext = context.getExecutionContext();
            nlapiLogExecution('debug', 'scriptContext ', scriptContext);
            if (scriptContext != 'scheduled') {
                if (b_OldIsBillable != b_NewIsBillable || s_OldBillRate != s_NewBillRate || d_oldDesiredDate != d_NewDesiredDate || d_OldStartDate != d_NewStartDate || d_OldEndDate != d_NewEndDate || parseInt(s_OldAllocation) != parseInt(s_NewAllocation)) {
                    var b_isSoftLocked = oldRecord.getFieldValue('custrecord_frf_details_selected_emp');
                    var b_isExternalHire = oldRecord.getFieldValue('custrecord_frf_details_external_hire');
                    var i_rrfNumber = oldRecord.getFieldValue('custrecord_frf_details_rrf_number');
                    var s_status = "";
                    if (b_isExternalHire == "T" && i_rrfNumber) {
                        s_status = nlapiLookupField('customrecord_taleo_external_hire', i_rrfNumber, 'custrecord_taleo_ext_hire_status');
                    }
                    var i_createdBy = oldRecord.getFieldValue('custrecord_frf_details_created_by');
                    var s_createdBy = oldRecord.getFieldText('custrecord_frf_details_created_by');
                    var userName = nlapiLookupField(nlapiGetRecordType(), nlapiGetRecordId(), 'custrecord_frf_details_last_updatedby', true);
                    nlapiLogExecution('debug', 'userName ', userName);

                    var ccArray = new Array();

                    if (_logValidation(i_opp)) {
                        s_project = nlapiLookupField('customrecord_sfdc_opportunity_record', i_opp, 'custrecord_opportunity_name_sfdc');
                        s_oppId = nlapiLookupField('customrecord_sfdc_opportunity_record', i_opp, 'custrecord_opportunity_id_sfdc');
                        var s_oppName = s_oppId + ', ' + s_project;
                        var i_pmOpp = nlapiLookupField('customrecord_sfdc_opportunity_record', i_opp, 'custrecord_sfdc_opp_pm');
                        var i_dmOpp = nlapiLookupField('customrecord_sfdc_opportunity_record', i_opp, 'custrecord_sfdc_opportunity_dm');
                        var i_cpOpp = nlapiLookupField('customrecord_sfdc_opportunity_record', i_opp, 'custrecord_sfdc_client_partner');
                        if (i_pmOpp)
                            ccArray.push(i_pmOpp);
                        if (i_dmOpp)
                            ccArray.push(i_dmOpp);
                        if (i_cpOpp)
                            var s_cpOppEmail = nlapiLookupField('employee', i_cpOpp, 'email');
                        if (s_cpOppEmail)
                            ccArray.push(s_cpOppEmail);
                        ccArray.push('fuel.support@brillio.com');
                        if (b_isSoftLocked || (b_isExternalHire == 'T' && s_status == 'Filled'))
                            ccArray.push('business.ops@brillio.com'); //Business Operations Team
                        nlapiLogExecution('Debug', 'ccArray op', ccArray);
                    } else if (_logValidation(i_project)) {
                        var i_pm = nlapiLookupField('job', i_project, 'custentity_projectmanager');
                        var s_pm = nlapiLookupField('job', i_project, 'custentity_projectmanager', true);
                        var i_dm = nlapiLookupField('job', i_project, 'custentity_deliverymanager');
                        var i_cp = nlapiLookupField('job', i_project, 'custentity_clientpartner');
                        if (i_dm)
                            var s_dmEmail = nlapiLookupField('employee', i_dm, 'email');
                        if (s_dmEmail)
                            ccArray.push(s_dmEmail);
                        if (i_cp)
                            var s_cpEmail = nlapiLookupField('employee', i_cp, 'email');
                        if (s_cpEmail)
                            ccArray.push(s_cpEmail);
                        ccArray.push('fuel.support@brillio.com');
                        if (b_isSoftLocked || (b_isExternalHire == 'T' && s_status == 'Filled'))
                            ccArray.push('business.ops@brillio.com'); //Business Operations Team
                        nlapiLogExecution('Debug', 'ccArray pr', ccArray);
                    }
                    var fldName = "Field Name";
                    var oldValue = "Old Value";
                    var newValue = "New Value";
                    var fieldDetails = "";
                    fieldDetails += "<table border=1><tr bgcolor = 'yellow'><th>" + fldName + "</th><th>" + oldValue + "</th><th>" + newValue + "</th>"
                    if (b_OldIsBillable != b_NewIsBillable) {
                        if (b_OldIsBillable == "T")
                            b_OldIsBillable = "Billable";
                        else
                            b_OldIsBillable = "Non Billable";
                        if (b_NewIsBillable == "T")
                            b_NewIsBillable = "Billable";
                        else
                            b_NewIsBillable = "Non Billable";
                        fieldDetails += "<tr><td>Category</td><td>" + b_OldIsBillable + "</td><td>" + b_NewIsBillable + "</td></tr>"
                        //-----------------------------------------------------------------------------------------------------------------				//prabhat gupta 07/12/2020	NIS-1871
                        if (b_OldIsBillable == "Billable" && b_NewIsBillable == "Non Billable") {

                            ccArray = getFuelAccountMapping(i_acc_id, ccArray);


                            ccArray.push("sandeep.sayal@BRILLIO.COM")

                        }
                        //-------------------------------------------------------------------------------------------------------------------					

                    }
                    if (s_OldBillRate != s_NewBillRate)
                        fieldDetails += "<tr><td>Bill Rate</td><td>" + s_OldBillRate + "</td><td>" + s_NewBillRate + "</td></tr>"
                    if (d_oldDesiredDate != d_NewDesiredDate)
                        fieldDetails += "<tr><td>Start Date</td><td>" + d_oldDesiredDate + "</td><td>" + d_NewDesiredDate + "</td></tr>" //prabhat gupta 12/6/2020
                    if (d_OldEndDate != d_NewEndDate)
                        fieldDetails += "<tr><td>End Date</td><td>" + d_OldEndDate + "</td><td>" + d_NewEndDate + "</td></tr>"
                    if (parseInt(s_OldAllocation) != parseInt(s_NewAllocation))
                        fieldDetails += "<tr><td>Allocation</td><td>" + s_OldAllocation + "</td><td>" + s_NewAllocation + "</td></tr>"
                    fieldDetails += "</table>"
                    if (_logValidation(i_createdBy)) {
                        var records = new Array();
                        var emailSubject = frfNumber + ': FUEL Notification: FRF details updated'
                        var emailBody = 'Dear ' + s_createdBy + '<br>FRF # ' + frfNumber + ' has been updated by ' + userName + ' on ' + nlapiDateToString(new Date()) + ' <br><br>' + fieldDetails + '<br><br>Brillio-FUEL<br>This email was sent from a notification only address.<br>If you need further assistance, please write to fuel.support@brillio.com';
                        //nlapiLogExecution('Debug','emailBody ',emailBody);
                        records['entity'] = 140512;
                        //nlapiSendEmail(154256, 94862, emailSubject,emailBody, ccArray, 'deepak.srinivas@brillio.com', records);
                        nlapiSendEmail(154256, i_createdBy, emailSubject, emailBody, ccArray, 'deepak.srinivas@brillio.com', records);
                        nlapiLogExecution('Debug', 'ccArray ', ccArray);
                    }
                }
            }
            //=================================================================================================================

        } catch (error) {
            nlapiLogExecution('Debug', 'Error ', error);
        }
    }
}

function createTaleoExternalHireRecord(i_osfRec) {
    var taleoExternalHireRec = nlapiCreateRecord("customrecord_taleo_external_hire");
    taleoExternalHireRec.setFieldValue("custrecord_taleo_ext_hire_frf_details", i_osfRec);
    return nlapiSubmitRecord(taleoExternalHireRec);
}
//END BEFORE LOAD ====================================================


function _logValidation(value) {
    if (value != null && value != 'null' && value != '' && value != undefined && value != undefined && value != 'undefined' && value.toString() != 'NaN' && value != NaN) {
        return true;
    } else {
        return false;
    }
}

//-----------------------------------------------------------------------------------------------------------------				//prabhat gupta 07/12/2020	NIS-1871

function getFuelAccountMapping(i_account, ccArray) {

    var customrecord_fuel_recruiterSearch = nlapiSearchRecord("customrecord_fuel_customer_wise_notify", null,
        [
            ["custrecord_fuel_employee.custentity_employee_inactive", "is", "F"], "AND",
            ["custrecord_fuel_customer", "anyof", i_account]
        ],
        [
            new nlobjSearchColumn("internalid").setSort(true),
            new nlobjSearchColumn("email", "custrecord_fuel_employee", null)

        ]);

    if (_logValidation(i_account)) {
        var account_mail_search = customrecord_fuel_recruiterSearch;

        if (_logValidation(account_mail_search)) {

            for (var i = 0; i < account_mail_search.length; i++) {

                var mail = account_mail_search[i].getValue("email", "custrecord_fuel_employee", null);

                if (_logValidation(mail)) {
                    ccArray.push(mail);
                }

            }
        }

    }

    return ccArray;
}

//-----------------------------------------------------------------------------------------------------------------------

//END FUNCTION =====================================================