/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       04 Mar 2015     amol.sahijwani
 *
 */

/**
 * @param {nlobjRequest} request Request object
 * @param {nlobjResponse} response Response object
 * @returns {Void} Any output is written via response object
 */
function suitelet(request, response){
	nlapiLogExecution('AUDIT', 'Report Run', '');
	
	var deploymentId = nlapiGetContext().getDeploymentId();
	
	var f_total_revenue	=	0.0;
	var form = nlapiCreateForm('Revenue Report');
	
	// Add Generate Excel Field
	var fld_generate_excel = form.addField('custpage_export_to_excel', 'inlinehtml');
	
	form.addSubmitButton('Load Data');
	
	var mode = request.getParameter('mode');
	
	//var a_milestone_data	=	getMilestoneData();
	
	var excelImportUrl =
		nlapiResolveURL('SUITELET', 'customscript_sut_revenue_report_fp',
				deploymentId);
	
	fld_generate_excel.setDefaultValue('<a href="' + excelImportUrl + '&mode=excel" target="_blank">Export to Excel</a>');
	
	// Add Sublist
	var o_sublist = form.addSubList('custpage_time_entries', 'list', '');

	// Add Fields
	o_sublist.addField('custpage_date', 'text', 'Date');
	//o_sublist.addField('custpage_employee', 'text', 'Employee');
	o_sublist.addField('custpage_project', 'text', 'Project');
	o_sublist.addField('custpage_customer', 'text', 'Customer');
	o_sublist.addField('custpage_end_customer', 'text', 'End Customer');
	o_sublist.addField('custpage_milestone', 'text', 'Milestone');
	o_sublist.addField('custpage_practice', 'text', 'Practice');
	o_sublist.addField('custpage_percentage', 'text', 'Percentage');
	o_sublist.addField('custpage_amount', 'text', 'Amount');
	o_sublist.addField('custpage_practice_amount', 'text', 'Practice Amount');
	o_sublist.addField('custpage_month', 'text', 'Month');
	o_sublist.addField('custpage_year', 'text', 'Year');
	o_sublist.addField('custpage_percent_completed', 'text', 'Percent Completed');
	o_sublist.addField('custpage_completed_amount', 'text', 'Percent Completed Amount');
	var a_milestone_data	=	getMilestoneData();
	var a_percent_complete_data	=	getPercentComplete();
	
	var i_line_number	=	1;
	for(var i = 0; i < a_milestone_data.length; i++)
		{
			
			
			var i_milestone_id	=	a_milestone_data[i].milestone;
			if(a_percent_complete_data[i_milestone_id] != undefined)
				{
					var a_percent_complete	=	a_percent_complete_data[i_milestone_id];
					
					for(var j = 0; j < a_percent_complete.length; j++)
					{
						o_sublist.setLineItemValue('custpage_date', (i_line_number), a_milestone_data[i].date);
						o_sublist.setLineItemValue('custpage_project', (i_line_number), a_milestone_data[i].project_name);
						o_sublist.setLineItemValue('custpage_customer', (i_line_number), a_milestone_data[i].customer_name);
						o_sublist.setLineItemValue('custpage_end_customer', (i_line_number), a_milestone_data[i].end_customer);
						o_sublist.setLineItemValue('custpage_milestone', (i_line_number), a_milestone_data[i].milestone_name);
						o_sublist.setLineItemValue('custpage_practice', (i_line_number), a_milestone_data[i].practice);
						o_sublist.setLineItemValue('custpage_percentage', (i_line_number), a_milestone_data[i].percentage)
						o_sublist.setLineItemValue('custpage_amount', (i_line_number), a_milestone_data[i].amount);
						o_sublist.setLineItemValue('custpage_practice_amount', (i_line_number), a_milestone_data[i].practice_amount);
						
						o_sublist.setLineItemValue('custpage_month', (i_line_number), a_percent_complete[j].month_name);
						o_sublist.setLineItemValue('custpage_year', (i_line_number), a_percent_complete[j].year);
						o_sublist.setLineItemValue('custpage_percent_completed', (i_line_number), a_percent_complete[j].percent_completed);
						
						var f_completed_amount	=	a_milestone_data[i].practice_amount * parseFloat(a_percent_complete[j].percent_completed) / 100.0;
						o_sublist.setLineItemValue('custpage_completed_amount', (i_line_number), f_completed_amount);
						f_total_revenue	=	f_completed_amount;
						i_line_number++;
					}
				}
			else
				{
					o_sublist.setLineItemValue('custpage_date', (i_line_number), a_milestone_data[i].date);
					o_sublist.setLineItemValue('custpage_project', (i_line_number), a_milestone_data[i].project_name);
					o_sublist.setLineItemValue('custpage_customer', (i_line_number), a_milestone_data[i].customer_name);
					o_sublist.setLineItemValue('custpage_end_customer', (i_line_number), a_milestone_data[i].end_customer);
					o_sublist.setLineItemValue('custpage_milestone', (i_line_number), a_milestone_data[i].milestone_name);
					o_sublist.setLineItemValue('custpage_practice', (i_line_number), a_milestone_data[i].practice);
					o_sublist.setLineItemValue('custpage_percentage', (i_line_number), a_milestone_data[i].percentage);
					o_sublist.setLineItemValue('custpage_amount', (i_line_number), a_milestone_data[i].amount);
					o_sublist.setLineItemValue('custpage_practice_amount', (i_line_number), a_milestone_data[i].practice_amount);
				
					o_sublist.setLineItemValue('custpage_month', (i_line_number), '');
					o_sublist.setLineItemValue('custpage_year', (i_line_number), '');
					o_sublist.setLineItemValue('custpage_percent_completed', (i_line_number), '0');
					o_sublist.setLineItemValue('custpage_completed_amount', (i_line_number), a_milestone_data[i].practice_amount * parseFloat(0.00) / 100.0);
				
					i_line_number++;
				}

		}
	o_sublist.setLabel('Revenue: ' + f_total_revenue);
	
	if(mode == 'excel')
	{
		exportToExcel(o_sublist);
		return;
	}
	
	response.writePage(form);
}
function getPercentComplete()
{
	var filters = [];
	
	var columns = [];
	columns[0]	=	new nlobjSearchColumn('custrecord_fpr_month');
	columns[1]	=	new nlobjSearchColumn('custrecord_fpr_year');
	columns[2]	=	new nlobjSearchColumn('custrecord_fpr_ano_milestone');
	columns[3]	=	new nlobjSearchColumn('custrecord_fpr_per_completed');
	
	var search_results	=	searchRecord('customrecord_fpr_input_revenue_rec', null, filters, columns);
	
	var a_data	=	new Object();
	for(var i = 0; i < search_results.length; i++)
		{
			var o_data	=	new Object();
			var i_milestone_id	=	search_results[i].getValue(columns[2]);
			
			o_data['month']				=	search_results[i].getValue(columns[0]);
			o_data['month_name']		=	search_results[i].getText(columns[0]);
			o_data['year']				=	search_results[i].getText(columns[1]);
			o_data['percent_completed']	=	search_results[i].getValue(columns[3]);
			
			if(a_data[i_milestone_id] == undefined)
				{
					a_data[i_milestone_id]	=	new Array();
				}
			
			a_data[i_milestone_id].push(o_data);
		}
	
	return a_data;
}
function getMilestoneData()
{
	var filters = [];
	
	var columns = [];
	columns[0]	=	new nlobjSearchColumn('custrecord_fpr_date');
	columns[1]	=	new nlobjSearchColumn('custrecordfpr_projparent');
	columns[2]	=	new nlobjSearchColumn('customer', 'custrecordfpr_projparent');
	columns[3]	=	new nlobjSearchColumn('custrecord_fpr_milestone');
	columns[4]	=	new nlobjSearchColumn('custrecord_fpr_practice', 'CUSTRECORD_FPR_PRAC_PROJECT_PAR');
	columns[5]	=	new nlobjSearchColumn('custrecord_fpr_percentage', 'CUSTRECORD_FPR_PRAC_PROJECT_PAR');
	columns[6]	=	new nlobjSearchColumn('custrecord_fpr_amount');
	columns[7]	=	new nlobjSearchColumn('formulanumeric').setFormula('{custrecord_fpr_prac_project_par.custrecord_fpr_percentage} * {custrecord_fpr_amount}');
	columns[8]	=	new nlobjSearchColumn('custentity_endcustomer', 'custrecordfpr_projparent');
	
	var search_results	=	searchRecord('customrecord_fpr_milestone_wi', null, filters, columns);
	var a_data	=	[];
	for(var i = 0; i < search_results.length; i++)
		{
			var o_data	=	new Object();
			
			o_data['date']				=	search_results[i].getValue(columns[0]);
			o_data['project']			=	search_results[i].getValue(columns[1]);
			o_data['project_name']		=	search_results[i].getText(columns[1]);
			o_data['customer']			=	search_results[i].getValue(columns[2]);
			o_data['customer_name']		=	search_results[i].getText(columns[2]);
			o_data['milestone']			=	search_results[i].getValue(columns[3]);
			o_data['milestone_name']	=	search_results[i].getText(columns[3]);
			o_data['practice']			=	search_results[i].getText(columns[4]);
			o_data['percentage']		=	search_results[i].getValue(columns[5]);
			o_data['amount']			=	search_results[i].getValue(columns[6]);
			o_data['practice_amount']	=	search_results[i].getValue(columns[7]);
			o_data['end_customer']		=	search_results[i].getText(columns[8]);
			
			a_data.push(o_data);
		}
	
	return a_data;
}

function searchRecord(recordType, savedSearch, arrFilters, arrColumns) {

	try {
		var search = null;

		// if a saved search is provided, load it and add the filters and
		// columns
		if (isNotEmpty(savedSearch)) {
			search = nlapiLoadSearch(recordType, savedSearch);
			search.addFilters(arrFilters);
			search.addColumns(arrColumns);
		}
		// create a new search
		else {
			search = nlapiCreateSearch(recordType, arrFilters, arrColumns);
		}

		// run search
		var resultSet = search.runSearch();

		// iterate through the search and get all data 1000 at a time
		var searchResultCount = 0;
		var resultSlice = null;
		var searchResult = [];

		do {
			resultSlice = resultSet.getResults(searchResultCount,
					searchResultCount + 1000);

			if (resultSlice) {

				resultSlice.forEach(function(result) {

					searchResult.push(result);
					searchResultCount++;
				});
			}
		} while (isArrayNotEmpty(resultSlice) && resultSlice.length >= 1000);

		return searchResult;
	} catch (err) {
		nlapiLogExecution('ERROR', 'searchRecord', err);
		throw err;
	}
}

function isArrayEmpty(argArray) {

	return !isArrayNotEmpty(argArray);
}

function isArrayNotEmpty(argArray) {

	return (isNotEmpty(argArray) && argArray.length > 0);
}

function isEmpty(value) {

	return value == null || value == "" || typeof (value) == undefined;
}

function isNotEmpty(value) {

	return !isEmpty(value);
}

function isTrue(value) {

	return value == 'T';
}

function isFalse(value) {

	return value != 'T';
}

function calcBusinessDays(d_startDate, d_endDate) { // input given as Date
	// objects
	var startDate = new Date(d_startDate.getTime());
	var endDate = new Date(d_endDate.getTime());
	// Validate input
	if (endDate < startDate)
		return 0;

	// Calculate days between dates
	var millisecondsPerDay = 86400 * 1000; // Day in milliseconds
	startDate.setHours(0, 0, 0, 1); // Start just after midnight
	endDate.setHours(23, 59, 59, 999); // End just before midnight
	var diff = endDate - startDate; // Milliseconds between datetime objects
	var days = Math.ceil(diff / millisecondsPerDay);

	// Subtract two weekend days for every week in between
	var weeks = Math.floor(days / 7);
	var days = days - (weeks * 2);

	// Handle special cases
	var startDay = startDate.getDay();
	var endDay = endDate.getDay();

	// Remove weekend not previously removed.
	if (startDay - endDay > 1)
		days = days - 2;

	// Remove start day if span starts on Sunday but ends before Saturday
	if (startDay == 0 && endDay != 6)
		days = days - 1

		// Remove end day if span ends on Saturday but starts after Sunday
	if (endDay == 6 && startDay != 0)
		days = days - 1

	return days;

}

function searchSubDepartments(i_department_id) {

	var s_department_name = nlapiLookupField('department', i_department_id,
			'name');

	var filters = new Array();
	if (s_department_name.indexOf(' : ') == -1) {
		filters[0] = new nlobjSearchFilter('formulatext', null, 'is',
				s_department_name);
		filters[0]
				.setFormula('TRIM(CASE WHEN INSTR({name} , \' : \', 1) > 0 THEN SUBSTR({name}, 1 , INSTR({name} , \' : \', 1)) ELSE {name} END)');
	} else {
		filters[0] = new nlobjSearchFilter('internalid', null, 'anyof',
				i_department_id);
	}
	var columns = new Array();
	columns[0] = new nlobjSearchColumn('internalid');

	var search_sub_departments = nlapiSearchRecord('department', null, filters,
			columns);

	var a_sub_departments = new Array();

	for (var i = 0; i < search_sub_departments.length; i++) {
		a_sub_departments
				.push(search_sub_departments[i].getValue('internalid'));
	}

	return a_sub_departments;
}

function getHolidays(d_start_date, d_end_date) {
	var filters = new Array();
	filters[0] = new nlobjSearchFilter('custrecord_date', null, 'within',
			d_start_date, d_end_date);

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_date');
	columns[1] = new nlobjSearchColumn('custrecordsubsidiary');

	var search_holidays = nlapiSearchRecord('customrecord_holiday', null,
			filters, columns);

	var a_holidays = new Object();

	for (var i = 0; search_holidays != null && i < search_holidays.length; i++) {
		var i_subsidiary = search_holidays[i].getValue('custrecordsubsidiary');
		var o_holiday = {
			'date' : nlapiStringToDate(search_holidays[i]
					.getValue('custrecord_date'))
		};
		if (a_holidays[i_subsidiary] == undefined) {
			a_holidays[i_subsidiary] = [ o_holiday ];
		} else {
			a_holidays[i_subsidiary].push(o_holiday);
		}
	}

	return a_holidays;
}

function exportToExcel(o_sublist) {

	var count = o_sublist.getLineItemCount();
	var globalArray = new Array();
	var o_columns = new Object();
	o_columns['custpage_date'] = 'Date';
	o_columns['custpage_project'] = 'Project';
	o_columns['custpage_customer'] = 'Customer';
	o_columns['custpage_end_customer'] = 'End Customer';
	o_columns['custpage_milestone'] = 'Milestone';
	o_columns['custpage_practice'] = 'Practice';
	o_columns['custpage_percentage'] = 'Percentage';
	o_columns['custpage_amount'] = 'Amount';
	o_columns['custpage_practice_amount'] = 'Practice Amount';
	o_columns['custpage_month'] = 'Month';
	o_columns['custpage_year'] = 'Year';
	//o_columns['custpage_not_submitted_amount'] = 'Not Submitted Amount';
	o_columns['custpage_percent_completed'] = 'Percent Completed';
	o_columns['custpage_completed_amount'] = 'Percent Completed Amount';
	//o_columns['custpage_holiday_hours'] = 'Holidays';
	//o_columns['custpage_rate'] = 'Billable Rate';
	//o_columns['custpage_percent'] = 'Percent Allocated';

	// Enter the headings
	var s_line = '';
	for ( var s_column in o_columns) {
		s_line += o_columns[s_column] + ',';
	}
	globalArray.push(s_line);
	for (var i = 1; i <= count; i++) {
		s_line = '\n';
		var a_line = new Array();
		for ( var s_column in o_columns) {
			var s_value = o_sublist.getLineItemValue(s_column, i);
			s_value = s_value == null?'':s_value.toString();
			s_value = s_value.replace(/[|]/g, " ");
			//s_value = s_value.replace(/[,]/g, " ");
			a_line.push('"' + s_value + '"'); // [s_column]
			// + ',';
		}
		s_line += a_line.toString();
		globalArray.push(s_line);
	}

	var fileName = 'Revenue report from FP'
	var Datetime = new Date();
	var CSVName = fileName + " - " + Datetime + '.csv';
	var file = nlapiCreateFile(CSVName, 'CSV', globalArray.toString());

	// nlapiLogExecution('DEBUG', 'suitlet_Po_ClosedStatus', ' PDF Name--> ');
	response.setContentType('CSV', CSVName);

	// nlapiLogExecution('DEBUG', 'suitlet_Po_ClosedStatus', ' PDF Name--> ');
	response.write(file.getValue());

	for ( var s_column in o_columns) {

	}
}

function getLoggedUserAccessInfo() {
	try {
		var currentEmployeeId = nlapiGetUser();
		var permissionSearch = nlapiSearchRecord(
				'customrecord_report_permission_list', null, [
						new nlobjSearchFilter('custrecord_rpl_employee', null,
								'anyof', [ currentEmployeeId ]),
						new nlobjSearchFilter('isinactive', null, 'is', 'F') ]);

		if (isArrayNotEmpty(permissionSearch)) {

			var permission_record = nlapiLoadRecord(
					'customrecord_report_permission_list', permissionSearch[0]
							.getId());
			var a_vertical_value_list = permission_record
					.getFieldValues('custrecord_rpl_vertical');
			var a_vertical_text_list = permission_record
					.getFieldTexts('custrecord_rpl_vertical');
			var a_department_value_list = permission_record
					.getFieldValues('custrecord_rpl_department');
			var a_department_text_list = permission_record
					.getFieldTexts('custrecord_rpl_department');
			var a_department_list = [], a_vertical_list = [];

			if (isArrayEmpty(a_vertical_value_list)) {
				a_vertical_value_list = [];
			}

			if (isArrayEmpty(a_department_value_list)) {
				a_department_value_list = [];
			}

			var count_department = a_department_value_list.length, count_vertical = a_vertical_value_list.length;

			// both department and verticals cannot go unselected
			// if (count_department == 0 || count_vertical == 0) {
			// throw "You are not authorized to access this page";
			// }

			for (var i = 0; i < count_department || i < count_vertical; i++) {
				if (i < count_department) {
					a_department_list.push({
						Value : a_department_value_list[i],
						Text : a_department_text_list[i],
						IsSelected : false
					});
				}

				if (i < count_vertical) {
					a_vertical_list.push({
						Value : a_vertical_value_list[i],
						Text : a_vertical_text_list[i],
						IsSelected : false
					});
				}
			}

			if (count_department == 1) {
				a_department_list[0].IsSelected = true;
			}

			if (count_vertical == 1) {
				a_vertical_list[0].IsSelected = true;
			}

			// check deployment
			var deploymentId = nlapiGetContext().getDeploymentId();

			if (deploymentId == 'customdeploy1') {
				a_department_list = [];
			} else if (deploymentId == 'customdeploy_sut_revenue_rep_department') {
				a_vertical_list = [];
			}

			return {
				VerticalList : a_vertical_list,
				DepartmentList : a_department_list
			};
		} else {
			throw "You are not authorized to access this page";
		}
	} catch (err) {
		nlapiLogExecution('ERROR', 'getLoggedUserAccessInfo', err);
		// Show Error Page
	}
}

function getDepartmentList() {
	try {
		var currentEmployeeId = nlapiGetUser();
		var permissionSearch = nlapiSearchRecord(
				'customrecord_report_permission_list', null, [
						new nlobjSearchFilter('custrecord_rpl_employee', null,
								'anyof', [ currentEmployeeId ]),
						new nlobjSearchFilter('isinactive', null, 'is', 'F'),
						new nlobjSearchFilter('custrecord_rpl_has_dept_access',
								null, 'is', 'T') ]);

		if (isArrayNotEmpty(permissionSearch)) {

			var permission_record = nlapiLoadRecord(
					'customrecord_report_permission_list', permissionSearch[0]
							.getId());
			var a_department_value_list = permission_record
					.getFieldValues('custrecord_rpl_department');
			var a_department_text_list = permission_record
					.getFieldTexts('custrecord_rpl_department');
			var a_department_list = [];

			if (isArrayEmpty(a_department_value_list)) {
				throw "No department specified";
			}

			var count_department = a_department_value_list.length;

			for (var i = 0; i < count_department; i++) {
				a_department_list.push({
					Value : a_department_value_list[i],
					Text : a_department_text_list[i],
					IsSelected : false
				});
			}

			if (count_department == 1) {
				a_department_list[0].IsSelected = true;
			}

			return a_department_list;
		} else {
			throw "You are not authorized to access this page";
		}
	} catch (err) {
		nlapiLogExecution('ERROR', 'getDepartmentList', err);
	}
}

function getVerticalList() {
	try {
		var currentEmployeeId = nlapiGetUser();
		var permissionSearch = nlapiSearchRecord(
				'customrecord_report_permission_list', null, [
						new nlobjSearchFilter('custrecord_rpl_employee', null,
								'anyof', [ currentEmployeeId ]),
						new nlobjSearchFilter('isinactive', null, 'is', 'F'),
						new nlobjSearchFilter(
								'custrecord__rpl_has_vertical_access', null,
								'is', 'T') ]);

		if (isArrayNotEmpty(permissionSearch)) {

			var permission_record = nlapiLoadRecord(
					'customrecord_report_permission_list', permissionSearch[0]
							.getId());
			var a_vertical_value_list = permission_record
					.getFieldValues('custrecord_rpl_vertical');
			var a_vertical_text_list = permission_record
					.getFieldTexts('custrecord_rpl_vertical');
			var a_vertical_list = [];

			if (isArrayEmpty(a_vertical_value_list)) {
				throw "No Verticals Specified";
			}

			var count_vertical = a_vertical_value_list.length;

			for (var i = 0; i < count_vertical; i++) {
				a_vertical_list.push({
					Value : a_vertical_value_list[i],
					Text : a_vertical_text_list[i],
					IsSelected : false
				});
			}

			if (count_vertical == 1) {
				a_vertical_list[0].IsSelected = true;
			}

			return a_vertical_list;
		} else {
			throw "You are not authorized to access this page";
		}
	} catch (err) {
		nlapiLogExecution('ERROR', 'getVerticalList', err);
	}
}
