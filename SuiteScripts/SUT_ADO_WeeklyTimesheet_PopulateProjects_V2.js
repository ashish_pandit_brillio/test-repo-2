function suitelet(request, response) {
    try {
		
        var request_customer_id = request.getParameter('i_customer_id');
        nlapiLogExecution('debug', 'request_customer_id', request_customer_id);
        var s_currentusertype=request.getParameter('s_currentusertype');
			var context = nlapiGetContext();
				var currentUser = context.getUser();
				nlapiLogExecution('debug', 'currentUser', currentUser);
				var projectList = '';
				if (request_customer_id) {
					projectList = getRelatedProjects(currentUser, request_customer_id,s_currentusertype);
				} else {
					var status = request.getParameter('status');
					var request_customer = request.getParameter('i_customer');
					projectList = getRelatedProjectslist(currentUser, request_customer, status,s_currentusertype);
				}

				var project_array = [];
				for (var i = 0; i < projectList.length; i++) {
					project_array[i] = projectList[i];
				}
					
		
        
    } catch (err) {
        nlapiLogExecution('error', 'suitelet', err);
        //displayErrorForm(err, "Timesheet Report - Weekly");
    }
    response.write(JSON.stringify(project_array));

}

function getRelatedProjects(currentUser, request_customer_id,s_currentusertype) {
    var filters = [
        [s_currentusertype, "anyof", currentUser],
        'and',
        [
            ['status', 'anyof', '2'], 'or', ['status', 'anyof', '4']
        ]
    ];
    if (request_customer_id) {
        filters.push('and');
        filters.push(["customer", "anyof", request_customer_id]);
    }

    var columns = [new nlobjSearchColumn("entityid"),
        new nlobjSearchColumn("altname"),
        new nlobjSearchColumn("internalid")
    ];

    var search = nlapiSearchRecord('job', null, filters, columns);
    return search;
}


function getRelatedProjectslist(currentUser, request_customer_id, status,s_currentusertype) {
    var filters = [[s_currentusertype, "anyof", currentUser]];
       
    if (request_customer_id) {
        filters.push('and');
        filters.push(["customer", "anyof", request_customer_id]);
    }

    if (status) {
        filters.push('and');
        filters.push(["status", "anyof", status]);
    }

    var columns = [new nlobjSearchColumn("entityid"),
        new nlobjSearchColumn("altname")
    ];

    var search = nlapiSearchRecord('job', null, filters, columns);
    return search;
}