/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       07 June 2017     Deepak MS
 *
 */

/**
 * @param {Object} dataIn Parameter object
 * @returns {Object} Output object
 */

function postRESTlet(dataIn) {
	
	var response = new Response();
	try {
		nlapiLogExecution('debug', 'dataIn', JSON.stringify(dataIn));
		
		//var employeeId = getUserUsingEmailId('krishna.v@brillio.com');
		var employeeId = getUserUsingEmailId(dataIn.EmailId);
		nlapiLogExecution('debug', 'dataIn employeeId', employeeId);
		
		var expense_ID = dataIn.Data ? dataIn.Data.ExpenseID : '';
		if(expense_ID){
			var i_expense_Id = getExpenseInternalID(expense_ID,employeeId);
		}
		
		var requestType = dataIn.RequestType;
		//var requestType = 'GET';
		//var transactionDate = dataIn.Data.Date;
		//nlapiLogExecution('debug', 'dataIn transactionDate', transactionDate);
		//var T_date = dateSplit(transactionDate);
		//nlapiLogExecution('debug', 'T_date transactionDate', T_date);
		
		//Testign Data
		/*var employeeId = 30237;
		var transactionDate = '22/8/2016';
		var T_date = dateSplit(transactionDate);*/
		
		//var requestType = dataIn.RequestType;
		//var expenseId = dataIn.Data.ExpenseId;
	//	var project_ID = dataIn.ProjectName;
		switch (requestType) {

		case M_Constants.Request.Get:
			if (employeeId && i_expense_Id) {
				response.Data = getExpensesDetails(employeeId,i_expense_Id);
				response.Status = true;
			}
			else if (employeeId ) {
				response.Data = getEmployeeExpenseDetail(employeeId);
				response.Status = true;
			}
			break;
		}
		
		
	} catch (err) {
		nlapiLogExecution('ERROR', 'postRESTlet', err);
		response.Data = err;
		response.Status = false;
	}
	nlapiLogExecution('debug', 'response', JSON.stringify(response));
	return response;
}

function dateSplit(tranDate){
	
	try{
		var date = tranDate.split('/');
		var day = date[0];
		var month = date[1];
		var year = date[2];
		var tran_Date = month+'/'+day+'/'+year;
		return tran_Date;
	}
	catch(e){
		nlapiLogExecution('ERROR', 'Date Splitting Error', err);
		throw e;
	}
}

function getEmployeeExpenseDetail(employeeId) {
	try {
		//var employeeRec = nlapiLoadRecord('employee', employeeId);
		var pendingApproval = ['ExpRept:C','ExpRept:B','ExpRept:D'];
		var overall = [];
		var filters = [];
		var dataRow = [];
		var expenseList ={};
		var filters = [];
		//filters.push(new nlobjSearchFilter('type', null, 'anyof', 'ExpRept'));
		filters.push(new nlobjSearchFilter('status', null, 'anyof', pendingApproval));
		filters.push(new nlobjSearchFilter('employee', null, 'anyof', employeeId));
		filters.push(new nlobjSearchFilter('mainline', null, 'is', 'T'));
		filters.push(new nlobjSearchFilter('field', 'systemnotes', 'anyof', 'TRANDOC.KSTATUS'));
		filters.push(new nlobjSearchFilter('newvalue', 'systemnotes', 'is', 'Pending Supervisor Approval'));
		
		
		var columns = Array();
		columns.push(new nlobjSearchColumn('tranid'));
		columns.push(new nlobjSearchColumn('memo'));
		columns.push(new nlobjSearchColumn('status'));
		columns.push(new nlobjSearchColumn('fxamount'));
		columns.push(new nlobjSearchColumn('trandate'));
		columns.push(new nlobjSearchColumn('date','systemNotes').setSort(true));
		columns.push(new nlobjSearchColumn('subsidiary')); //

		var expenseSearch = nlapiSearchRecord('expensereport',null,filters,columns);
		var len = 0;
		
		var employeeLookup_1 = nlapiLookupField('employee',parseInt(employeeId),['entityid']);
		var employee_id = employeeLookup_1.entityid;
		if(expenseSearch){
			nlapiLogExecution('debug', 'expenseSearch Pending', expenseSearch.length);
			if(expenseSearch.length > 10)
				len = 10;
				else
				len = expenseSearch.length;	
			for(var i=0;i<len;i++){
			
			var expenseID = expenseSearch[i].getId();
			var expenseLoad = nlapiLoadRecord('expensereport',expenseID);
			var lineCount = expenseLoad.getLineItemCount('expense');
			
			
			for(var j=1;j<= 1;j++){
				
				var project =  expenseLoad.getLineItemValue('expense','customer_display',j);
				
				}
		//	var currency_1 ='';	
			var subsidiary = expenseSearch[i].getValue('subsidiary');
		//	var subsidiary_ = expenseSearch_P[i].getValue('subsidiary');
			var lookupSUbsidairy_ = nlapiLookupField('subsidiary',subsidiary,['currency'],true);
			var currency_1 = lookupSUbsidairy_.currency;
			var amount_f = expenseSearch[i].getValue('fxamount');
			amount_f = parseFloat(amount_f).toFixed(1);
		
		expenseList ={
		SlNo: i+1,		
		Employee: employee_id,
		ExpenseID : expenseSearch[i].getValue('tranid'),
		Date : expenseSearch[i].getValue('trandate'),
		Purpose : expenseSearch[i].getValue('memo'),
		Status : expenseSearch[i].getValue('status'),
		Amount : amount_f,
		Project: project,
		Subsidiary: expenseSearch[i].getText('subsidiary'),
		Currency: currency_1
		};
		dataRow.push(expenseList);
		}
		}
		overall.push(dataRow);
		
		//Past Expenses
		var filters_P = [];
		var pastArray = ['ExpRept:I','ExpRept:E','ExpRept:D'];
		var len_P = 0;
		var expenseList_P = {};
		var dataRows = [];
		//filters.push(new nlobjSearchFilter('type', null, 'anyof', 'ExpRept'));
		filters_P.push(new nlobjSearchFilter('status', null, 'anyof', pastArray));
		filters_P.push(new nlobjSearchFilter('employee', null, 'anyof', employeeId));
		filters_P.push(new nlobjSearchFilter('mainline', null, 'is', 'T'));
		filters_P.push(new nlobjSearchFilter('field', 'systemnotes', 'anyof', 'TRANDOC.KSTATUS'));
		filters_P.push(new nlobjSearchFilter('newvalue', 'systemnotes', 'is', 'Pending Supervisor Approval'));
		
		var columns_P = Array();
		columns_P.push(new nlobjSearchColumn('tranid'));
		columns_P.push(new nlobjSearchColumn('memo'));
		columns_P.push(new nlobjSearchColumn('status'));
		columns_P.push(new nlobjSearchColumn('fxamount'));
		columns_P.push(new nlobjSearchColumn('trandate'));
		columns_P.push(new nlobjSearchColumn('subsidiary'));
		columns_P.push(new nlobjSearchColumn('date','systemNotes').setSort(true));//

		var expenseSearch_P = nlapiSearchRecord('expensereport',null,filters_P,columns_P);
		
		var employeeLookup_1_P = nlapiLookupField('employee',parseInt(employeeId),['entityid']);
		var employee_id_P = employeeLookup_1_P.entityid;
		
		if(expenseSearch_P){
			
			nlapiLogExecution('debug', 'expenseSearch Paid', expenseSearch_P.length);
			if(expenseSearch_P.length > 10)
				len_P = 10;
				else
				len_P = expenseSearch_P.length;	
			
			for(var i=0;i<len_P;i++){
				var expenseID = '';
			expenseID = expenseSearch_P[i].getId();
			var expenseLoad = nlapiLoadRecord('expensereport',expenseID);
			var lineCount = expenseLoad.getLineItemCount('expense');
			
			if(lineCount >=1){
			for(var j=1;j<= 1;j++){
				
				var project =  expenseLoad.getLineItemValue('expense','customer_display',j);
				
				}
			}
			
			
			var subsidiary_ = expenseSearch_P[i].getValue('subsidiary');
			var lookupSUbsidairy = nlapiLookupField('subsidiary',subsidiary_,['currency'],true);
			var currency_2 = lookupSUbsidairy.currency;
			var amount_fll = expenseSearch_P[i].getValue('fxamount');
			amount_fll = parseFloat(amount_fll).toFixed(1);
			/*if(subsidiary_ == 3){
				currency_2 = 'INR';
				}
				else{
					currency_2 = 'USD';
				}*/
		
		expenseList_P ={
		SlNo: i+1,		
		Employee: employee_id,
		ExpenseID : expenseSearch_P[i].getValue('tranid'),
		Date : expenseSearch_P[i].getValue('trandate'),
		Purpose : expenseSearch_P[i].getValue('memo'),
		Status : expenseSearch_P[i].getValue('status'),
		Amount : amount_fll,
		Project: project,
		Subsidiary: expenseSearch_P[i].getText('subsidiary'),
		Currency: currency_2		
		};
		dataRows.push(expenseList_P);
		}
		}
		overall.push(dataRows);
		nlapiLogExecution('debug','Response',JSON.stringify(overall));
		return overall;
	
	} catch (err) {
		nlapiLogExecution('error', 'getExpenseDetail', err);
		throw err;
	}
}

function getExpenseInternalID(expense_id,emp) {
	try{
	var id = '';
	var filters = Array();
	filters.push(new nlobjSearchFilter('tranid', null, 'startswith', expense_id));
	filters.push(new nlobjSearchFilter('employee', null, 'anyof', emp));
	filters.push(new nlobjSearchFilter('mainline', null, 'is', 'T'));

	var cols = Array();
	cols.push(new nlobjSearchColumn('internalid'));

	var searchObj = nlapiSearchRecord('expensereport',null,filters,cols);
	if(searchObj)
	id = searchObj[0].getValue('internalid');

	return id;
	}
	catch(e){
		nlapiLogExecution('DEBUG','Erron in searching expense',e);
		throw e.details;
	}
}
function getExpensesDetails(emp,expenseid){
	try{
		var flag = true;
		var currency_1;
		var json ={},JsonCategory ={};
		var dataRow =[],dataRows=[];
		var main ={};
		nlapiLogExecution('DEBUG','Expense ID ON GETONE',expenseid);
		//var expense_internal_id = getExpenseInternalID(expenseid);
				
		//nlapiLogExecution('DEBUG','Expense Internal ID ON GETONE',expense_internal_id);
		
		var record_Obj = nlapiLoadRecord('expensereport',expenseid);
		var emp_name = record_Obj.getFieldValue('entityname');
		var first_approver = record_Obj.getFieldText('custbody1stlevelapprover');
		var T_purpose = record_Obj.getFieldValue('memo'); //subsidiary
		var subsidiary_ = record_Obj.getFieldValue('subsidiary');
		var subsidiary_text = record_Obj.getFieldText('subsidiary');
		var totalAmt = record_Obj.getFieldValue('total');
		totalAmt = parseFloat(totalAmt).toFixed(1);
		var T_Status = record_Obj.getFieldValue('statusRef');
		var trandate = record_Obj.getFieldValue('trandate');
		
		var lineCount = record_Obj.getLineItemCount('expense');
		
		for(var jk=1;jk<= 1;jk++){
			
			var project =  record_Obj.getLineItemValue('expense','customer_display',jk);
			
			}
		if(subsidiary_ == 3){
			currency_1 = 'INR';
			}
			else{
			currency_1 = 'USD';
			}
		json ={
			Employee: emp_name,
			Currency: currency_1,
			Date: trandate,
			Approver: first_approver,
			Subsidiary: subsidiary_text,
			Project: project,
			Status: T_Status,
			Purpose: T_purpose,
			TotalAmt: totalAmt
				
		};
		dataRow.push(json);
		
		
		
		for(var i=1;i<= lineCount;i++){
			var img ='';
			var data = '';
			var file = '';
			var amount_line = record_Obj.getLineItemValue('expense','foreignamount',i);
			amount_line = parseFloat(amount_line).toFixed(1);
			 img = record_Obj.getLineItemValue('expense','expmediaitem',i);
			if(img){
			 file  = nlapiLoadFile(img);
			 data = file.getValue();
			 nlapiLogExecution('debug','Img data','line:'+i+','+'data:'+data);
		}
			JsonCategory ={
			 RefNo: record_Obj.getLineItemValue('expense','refnumber',i),		
			 Category: record_Obj.getLineItemValue('expense','category_display',i),
			 Currency: record_Obj.getLineItemText('expense','currency',i),
			 Memo: record_Obj.getLineItemValue('expense','memo',i),
			 Date : 	record_Obj.getLineItemValue('expense','expensedate',i),
			 Amount: amount_line, //customer_display
			 Project: record_Obj.getLineItemValue('expense','customer_display',i),
			 imgData: data
			
			}
			dataRows.push(JsonCategory);
		}
		main = {
			Body: dataRow,
			Line:dataRows
				
		};
		//var loadRec =  nlapiLoadRecord('expensereport',expense_internal_id);
		//loadRec.setFieldValue('status','B');
		//nlapiSubmitRecord(loadRec);
		//nlapiSubmitField('expensereport', expense_internal_id, 'status', 'B');
		nlapiLogExecution('debug', 'dataIn', JSON.stringify(main));
		return main;
	}
	catch(e){
		nlapiLogExecution('DEBUG','Erron in getting the expense details',e);
		throw e;
		
	}
	
}
function _logValidation(value) 
{
 if(value != null && value.toString() != null && value != '' && value != undefined && value.toString() != undefined && value != 'undefined' && value.toString() != 'undefined'&& value.toString() != 'NaN' && value != NaN) 
 {
  return true;
 }
 else 
 { 
  return false;
 }
}