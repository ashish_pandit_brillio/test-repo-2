/**
 * @author Shweta
 */
// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
/*
   	Script Name : UES_Project_Project_Manager_Validations.js
	Author      : Shweta Chopde
	Date        :
	Description :


	Script Modification Log:

	-- Date --			-- Modified By --				--Requested By--				-- Description --



Below is a summary of the process controls enforced by this script file.  The control logic is described
more fully, below, in the appropriate function headers and code blocks.


     BEFORE LOAD
		- beforeLoadRecord(type)



     BEFORE SUBMIT
		- beforeSubmitRecord(type)


     AFTER SUBMIT
		- afterSubmitRecord(type)



     SUB-FUNCTIONS
		- The following sub-functions are called by the above core functions in order to maintain code
            modularization:

               - NOT USED

*/
}
// END SCRIPT DESCRIPTION BLOCK  ====================================



// BEGIN GLOBAL VARIABLE BLOCK  =====================================
{
	//  Initialize any Global Variables, in particular, debugging variables...




}
// END GLOBAL VARIABLE BLOCK  =======================================





// BEGIN BEFORE LOAD ==================================================

function beforeLoadRecord(type)
{

	/*  On before load:

          - EXPLAIN THE PURPOSE OF THIS FUNCTION

		-


		FIELDS USED:

          --Field Name--				--ID--


	*/



	//  LOCAL VARIABLES

    //  BEFORE LOAD CODE BODY


	return true;



}

// END BEFORE LOAD ====================================================





// BEGIN BEFORE SUBMIT ================================================

function beforeSubmit_PM_Validations(type)
{
  var s_record_type = nlapiGetRecordType();
  nlapiLogExecution('DEBUG', 'beforeSubmit_PM_Validations', ' Record Type -->' + s_record_type);
				    
  if(s_record_type == 'projectTask')
  {
  	if(type =='edit')
	{			
	
  	var i_project = nlapiGetFieldValue('company')
	nlapiLogExecution('DEBUG', 'beforeSubmit_PM_Validations', ' Project -->' + i_project);
	
	if(_logValidation(i_project))
	{
	  var o_recordOBJ = nlapiLoadRecord('job',i_project)	
		
	 if(_logValidation(o_recordOBJ))
	 {
	 	 var i_project_manager = o_recordOBJ.getFieldValue('custentity_projectmanager');
	     nlapiLogExecution('DEBUG', 'beforeSubmit_PM_Validations', ' Project Manager -->' + i_project_manager);
		 
		 if(i_project_manager == null || i_project_manager == undefined || i_project_manager =='')
		 {
		 	throw ' You are not allowed to save the record as no Project Manager is assigned for the project .'
		    return false;
		 }
		
	 }//o_recordOBJ
	 
	}//Project 
		
		
	}
	
	
  }//Project Task
  
  if(s_record_type == 'timebill')
  {
	if(type =='edit')
	{
	var i_time_count = nlapiGetLineItemCount('timeitem')
	nlapiLogExecution('DEBUG', 'beforeSubmit_PM_Validations', ' Time Count -->' + i_time_count);
		 
  	if(_logValidation(i_time_count))
	{
		for(var i=1;i<=i_time_count;i++)
		{
			var i_project = nlapiGetLineItemValue('timeitem','customer',i)
			nlapiLogExecution('DEBUG', 'beforeSubmit_PM_Validations', ' Project -->' + i_project);
			
			if(_logValidation(i_project))
			{
			  var o_recordOBJ = nlapiLoadRecord('job',i_project)	
				
			 if(_logValidation(o_recordOBJ))
			 {
			 	 var i_project_manager = o_recordOBJ.getFieldValue('custentity_projectmanager');
			     nlapiLogExecution('DEBUG', 'beforeSubmit_PM_Validations', ' Project Manager -->' + i_project_manager);
				 
				 if(i_project_manager == null || i_project_manager == undefined || i_project_manager =='')
				 {
				 	throw ' You are not allowed to save the record as no Project Manager is assigned for the project .'
				    return false;
				 }
				
			 }//o_recordOBJ
			 
			}//Project 
	
		}
	}//Time Count
		}
	

	
  }//Project Task
	return true;

}

// END BEFORE SUBMIT ==================================================





// BEGIN AFTER SUBMIT =============================================

function afterSubmitRecord(type)
{
    /*  On after submit:

          - PURPOSE
		-

	FIELDS USED:

          --Field Name--				--ID--


    */


	//  LOCAL VARIABLES



	//  AFTER SUBMIT CODE BODY

	return true;

}

// END AFTER SUBMIT ===============================================





// BEGIN FUNCTION ===================================================
function _logValidation(value) 
{
 if(value != null && value.toString() != null && value != '' && value != undefined && value.toString() != undefined && value != 'undefined' && value.toString() != 'undefined'&& value.toString() != 'NaN' && value != NaN) 
 {
  return true;
 }
 else 
 { 
  return false;
 }
}
// END FUNCTION =====================================================
