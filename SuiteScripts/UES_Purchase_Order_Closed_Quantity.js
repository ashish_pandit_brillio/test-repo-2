/**
 * @author Shweta
 */
// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
/*
   	Script Name : UES_Purchase_Order_Closed_Quantity.js
	Author      : Shweta Chopde
	Date        : 5 May 2014
	Description :


	Script Modification Log:

	-- Date --			-- Modified By --				--Requested By--				-- Description --



Below is a summary of the process controls enforced by this script file.  The control logic is described
more fully, below, in the appropriate function headers and code blocks.


     BEFORE LOAD
		- beforeLoadRecord(type)



     BEFORE SUBMIT
		- beforeSubmitRecord(type)


     AFTER SUBMIT
		- afterSubmitRecord(type)



     SUB-FUNCTIONS
		- The following sub-functions are called by the above core functions in order to maintain code
            modularization:

               - NOT USED

*/
}
// END SCRIPT DESCRIPTION BLOCK  ====================================



// BEGIN GLOBAL VARIABLE BLOCK  =====================================
{
	//  Initialize any Global Variables, in particular, debugging variables...




}
// END GLOBAL VARIABLE BLOCK  =======================================





// BEGIN BEFORE LOAD ==================================================

function beforeLoadRecord(type)
{

	/*  On before load:

          - EXPLAIN THE PURPOSE OF THIS FUNCTION

		-


		FIELDS USED:

          --Field Name--				--ID--


	*/



	//  LOCAL VARIABLES

    //  BEFORE LOAD CODE BODY


	return true;



}

// END BEFORE LOAD ====================================================





// BEGIN BEFORE SUBMIT ================================================

function beforeSubmitRecord(type)
{
    /*  On before submit:

          - PURPOSE
		-

          FIELDS USED:

          --Field Name--				--ID--


    */


    //  LOCAL VARIABLES


    //  BEFORE SUBMIT CODE BODY


	return true;

}

// END BEFORE SUBMIT ==================================================





// BEGIN AFTER SUBMIT =============================================

function afterSubmit_closed_quantity(type)
{
   try
   {
   	 var i_recordID = nlapiGetRecordId();
	 
	 var s_record_type = nlapiGetRecordType();
	 
	 if(_logValidation(i_recordID)&&_logValidation(s_record_type))
	 {
	 	var o_recordOBJ = nlapiLoadRecord(s_record_type,i_recordID)
		
		 if(_logValidation(o_recordOBJ))
		 {
		 	var i_status = o_recordOBJ.getFieldValue('status');
			nlapiLogExecution('DEBUG', 'afterSubmit_closed_quantity',' Status -->' + i_status);
			
			if(_logValidation(i_status))
			{
				var i_line_count = o_recordOBJ.getLineItemCount('item');
				
				if(_logValidation(i_line_count))
				{
					for(var i=1;i<=i_line_count;i++)
					{
						var i_PR_ID = o_recordOBJ.getLineItemValue('item','custcol_po_pr_item',i)
						nlapiLogExecution('DEBUG', 'afterSubmit_closed_quantity',' PR ID -->' + i_PR_ID);
						
						var i_quantity = o_recordOBJ.getLineItemValue('item','quantity',i)
						nlapiLogExecution('DEBUG', 'afterSubmit_closed_quantity',' Quantity -->' + i_quantity);
			 
			           if(_logValidation(i_PR_ID))
					   {
					   	var o_PR_OBJ = nlapiLoadRecord('customrecord_pritem',i_PR_ID);
						
					    if(_logValidation(o_PR_OBJ))
					    {
					  	  o_PR_OBJ.setFieldValue('custrecord_quantity',i_quantity)
			
						  var i_submitID = nlapiSubmitRecord(o_PR_OBJ,true,true)
						  nlapiLogExecution('DEBUG', 'afterSubmit_closed_quantity',' ****************** Submit ID -->' + i_submitID);
			 						
					    }
						
					   }//PR ID		
			
					}//Loop					
					
				}//Line Count
				
			}//Status
			
		 }//Record OBJ		
	 	
	 }//Record ID & Record Type   	
   }//TRY
   catch(exception)
   {
   	nlapiLogExecution('DEBUG', 'ERROR',' Exception Caught -->' + exception);
   }
	return true;

}

// END AFTER SUBMIT ===============================================





// BEGIN FUNCTION ===================================================
function _logValidation(value) 
{
 if(value != null && value.toString() != null && value != '' && value != undefined && value.toString() != undefined && value != 'undefined' && value.toString() != 'undefined'&& value.toString() != 'NaN' && value != NaN) 
 {
  return true;
 }
 else 
 { 
  return false;
 }
}




// END FUNCTION =====================================================
