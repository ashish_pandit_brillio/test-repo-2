/*
Purpose : Updation of TA and EA to RM for Internal and Multiple Allocations
Return Type : Null
*/

function intApproverChanges(type) {
	try {

		var columns = new Array();
		columns[0] = new nlobjSearchColumn('internalid');
		columns[1] = new nlobjSearchColumn('custentity_reportingmanager');
		columns[2] = new nlobjSearchColumn('timeapprover');
		columns[3] = new nlobjSearchColumn('approver');
		var search = nlapiSearchRecord('employee', 'customsearch_rmupdates', null, columns);
		if (search) {
			for (var j = 0; j < search.length; j++) {

				var eid = search[j].getValue('internalid');
				var col = new Array();
				col[0] = new nlobjSearchColumn('resource');
				col[1] = new nlobjSearchColumn('project');
				col[2] = new nlobjSearchColumn('enddate');
				col[3] = new nlobjSearchColumn('jobtype', 'job');
				col[4] = new nlobjSearchColumn('percentoftime');
				var filter = new Array();
				filter[0] = new nlobjSearchFilter('resource', null, 'anyOf', eid);
				filter[1] = new nlobjSearchFilter('enddate', null, 'onorafter', 'today');
				filter[2] = new nlobjSearchFilter('startdate', null, 'onorbefore', 'today');
				var s = nlapiSearchRecord('resourceallocation', 'null', filter, col);
				for (var i = 0; s != null && i < s.length; i++) {
					var empid = s[i].getValue('resource');
					var allo_per = parseFloat(s[i].getValue('percentoftime'));
					var jtype = s[i].getValue('jobtype', 'job');
					var tapprover = search[j].getValue('timeapprover');
					var approver = search[j].getValue('approver');
					var r_mngr = search[j].getValue('custentity_reportingmanager');
					if (jtype == 1 || allo_per < 100) {
						var flag_check = false;
						var employe = nlapiLoadRecord('employee', empid);
						if (tapprover != r_mngr) {
							employe.setFieldValue('timeapprover', r_mngr);
							flag_check = true;
						}
						if (approver != r_mngr) {
							employe.setFieldValue('approver', r_mngr);
							flag_check = true;
							nlapiLogExecution('AUDIT', 'Info', 'Updated#' + empid);
						}
						if (flag_check == true) {
							var id = nlapiSubmitRecord(employe);
						}
					}
				}
			}
		}
		var a_emp_attachment = new Array();
		a_emp_attachment['entity'] = '62082';
		nlapiSendEmail(62082, 'sai.vannamareddy@brillio.com', 'script executed', 'executes sucessfully', null, null, a_emp_attachment);
	}
	catch (err) {
		nlapiLogExecution('error', 'Main', err);
		var a_emp_attachment = new Array();
		a_emp_attachment['entity'] = '62082';
		var context = nlapiGetContext();
		Send_Exception_Mail(err);
	}
}
	
function Send_Exception_Mail(err)
{​​​​​
    try
    {​​​​​   
        var context = nlapiGetContext();
        var s_DeploymentID =context.getDeploymentId();
        var s_ScriptID =context.getScriptId();
        var s_Subject ='Issue on Rev Projection Project Wise';
        
        var s_Body = ' This is to inform that Internal_TA _EAUpdate has been failed and therefore, system is not able to send details';
        s_Body += '<br/><b>Issue: Code:</b> '+err.code+' Message: '+err.message;
        s_Body += '<br/><b>Script: </b>'+s_ScriptID;
        s_Body += '<br/><b>Deployment:</b> '+s_DeploymentID;
        s_Body += '<br/>Please have a look and do the needfull.';
        s_Body += '<br/><br/>Note: This is system genarated email incase of any failure while sending the notification to employees';

        s_Body += '<br/><br/>Regards,';
        s_Body += '<br/>Digital Office';
        nlapiSendEmail(442,"netsuite.support@brillio.com",'script failed',s_Body,null, null, a_emp_attachment);
    }​​​​​
    catch(err)
    {​​​​​
        nlapiLogExecution('error',' Send_Exception_Mail', err.message); 
		
    }​​​​​
}​​​​​
