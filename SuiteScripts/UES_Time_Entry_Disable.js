/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       01 Dec 2014     amol.sahijwani
 * Disable Time Entry while a report is being generated.
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType
 *   
 * @param {String} type Operation types: create, edit, view, copy, print, email
 * @param {nlobjForm} form Current form
 * @param {nlobjRequest} request Request object
 * @returns {Void}
 */
function userEventBeforeLoad(type, form, request){
	
  	
	var showTillTime = Date.UTC(2015,1,9,17,0,0);
	
	var currentTime = new Date();
	
	var currentMilliseconds = currentTime.getTime(); // - currentTime.getTimezoneOffset() * 60 * 1000;
	nlapiLogExecution('AUDIT', 'Test', 'Test');
	var remainingMilliseconds = showTillTime - currentMilliseconds;
if(remainingMilliseconds < 0)
{
	var scriptRecord = nlapiLoadRecord('usereventscript', 349);
	scriptRecord.setFieldValue('isinactive','F');
	nlapiSubmitRecord(scriptRecord);
	
	nlapiLogExecution('AUDIT','Script Disabled','Time: ' + currentTime.toString());
}
try
	{
		nlapiSetRedirectURL('SUITELET', 348, 1, null, null);
	}
	catch(e)
	{
		nlapiLogExecution('ERROR', 'Error', e.message);
	}


}

function disableApprove(type)
{
	var o_error = nlapiCreateError('404', 'Please edit timesheets after 2 hours.');
	nlapiLogExecution('AUDIT', 'Approval Test', 'Type: ' + type);
	throw o_error;
}
