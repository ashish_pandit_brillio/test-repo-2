/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       06 Aug 2021     shravan.k
 *
 */

/**
 * @param {String} type Context Types: scheduled, ondemand, userinterface, aborted, skipped
 * @returns {Void}
 */
function Validate_Assignees(type)
{
	var arr_Task_Details = [];
	try
	{
		var obj_Usage_Context = nlapiGetContext();
		var d_Date_obj = new Date();
		var Initial_Time = d_Date_obj.getSeconds();
		nlapiLogExecution('DEBUG', 'Begining Usage  ==',obj_Usage_Context.getRemainingUsage());
		var t_time_Begin = d_Date_obj.getTime();
		nlapiLogExecution('DEBUG', 't_time_Begin   ==',t_time_Begin);
		
		/* Search to get Resource allocation Changed */
		var arr_Projects_Internal_Id = [];
		var arr_Resource_Internal_Id = [];
		var arr_Projects_Ot_Internal_Id = [];
		var arr_Allocation_OT_Array = [];
		var arr_Allocation_Array =[];
		var arr_Service_Item = [];
		var arr_Filter_RA = [["systemnotes.date","on","today"],"OR",["systemnotes.date","on","yesterday"]];
		var arr_Columns_RA = [
	        				   new nlobjSearchColumn("company"), 
	        				   new nlobjSearchColumn("resource"), 
	        				   new nlobjSearchColumn("custevent_otbillable"),
	        				   new nlobjSearchColumn("custevent1")
	        				];
		var obj_Resrc_Allcn_Search = searchRecord('resourceallocation',null,arr_Filter_RA,arr_Columns_RA);
		if(obj_Resrc_Allcn_Search)
		{
			nlapiLogExecution('DEBUG','obj_Resrc_Allcn_Search.length==', obj_Resrc_Allcn_Search.length);
			for(var i_Ra_Index = 0 ;i_Ra_Index < obj_Resrc_Allcn_Search.length ; i_Ra_Index++  )
				{
					var i_Project_Id = obj_Resrc_Allcn_Search[i_Ra_Index].getValue('company');
					var i_Resource_Id = obj_Resrc_Allcn_Search[i_Ra_Index].getValue('resource');
					var i_Service_Item = obj_Resrc_Allcn_Search[i_Ra_Index].getValue('custevent1');
					var ot_applicable = obj_Resrc_Allcn_Search[i_Ra_Index].getValue('custevent_otbillable')
					if(ot_applicable == 'T')
						{
							if(arr_Projects_Ot_Internal_Id.indexOf(i_Project_Id)== -1)
								{
									arr_Projects_Ot_Internal_Id.push(i_Project_Id);
									arr_Allocation_OT_Array[i_Project_Id] = [];
									arr_Allocation_OT_Array[i_Project_Id].push(i_Resource_Id);
								} /// if(arr_Projects_Ot_Internal_Id.indexOf(i_Project_Id)== -1)
							else
								{
									arr_Allocation_OT_Array[i_Project_Id].push(i_Resource_Id);
								} ///// ELSE OF 	if(arr_Projects_Ot_Internal_Id.indexOf(i_Project_Id)== -1)
								
						} //// if(ot_applicable == 'T')
					if(arr_Resource_Internal_Id.indexOf(i_Resource_Id)== -1)
						{
							arr_Resource_Internal_Id.push(i_Resource_Id);
						} ///// if(arr_Resource_Internal_Id.indexOf(i_Resource_Id)== -1)
					if(arr_Projects_Internal_Id.indexOf(i_Project_Id)== -1)
						{
							arr_Projects_Internal_Id.push(i_Project_Id);/// non-duplicates project array
							arr_Allocation_Array[i_Project_Id] = [];
							arr_Allocation_Array[i_Project_Id].push(i_Resource_Id);
							arr_Service_Item[i_Project_Id]= [];
							arr_Service_Item[i_Project_Id][i_Resource_Id] = i_Service_Item;
						
						} ///// if(arr_Projects_Internal_Id.indexOf(i_Project_Id)== -1)
					else
						{
							arr_Allocation_Array[i_Project_Id].push(i_Resource_Id);
							arr_Service_Item[i_Project_Id][i_Resource_Id] = i_Service_Item;
						} ////// else of if(arr_Projects_Internal_Id.indexOf(i_Project_Id)== -1)
					//nlapiLogExecution('DEBUG','i_Project_Id==', i_Project_Id);
					//nlapiLogExecution('DEBUG','arr_Allocation_Array[i_Project_Id]==', JSON.stringify(arr_Allocation_Array[i_Project_Id]));
					
				} ///// for(var i_Ra_Index = 0 ;i_Ra_Index < obj_Resrc_Allcn_Search.length ; i_Ra_Index++  )
			nlapiLogExecution('DEBUG','arr_Resource_Internal_Id.length==', arr_Resource_Internal_Id.length);
			/* Search to get Resource allocation Changed */
			
			yieldScript(obj_Usage_Context); /* to check usage and yield */
			
			/* Search with Project activites Task */
			var s_Task_Name = "Project";
			var i_title_id = null;
			nlapiLogExecution('DEBUG','Starting for ==', s_Task_Name);
			check_Add_Assignee(s_Task_Name,i_title_id,arr_Allocation_Array,arr_Projects_Internal_Id,arr_Service_Item);
			nlapiLogExecution('DEBUG','End for ==', s_Task_Name);
			/* Search with Project activites Task */
		
			yieldScript(obj_Usage_Context); /* to check usage and yield */
			
			/* Search with Leave Task */
			var s_Task_Name = "Leave";
			var i_title_id = 2479; /// this is a item on assignee tab which needs to hardcoded service item
			nlapiLogExecution('DEBUG','Starting for ==', s_Task_Name);
			check_Add_Assignee(s_Task_Name,i_title_id,arr_Allocation_Array,arr_Projects_Internal_Id,arr_Service_Item);
			nlapiLogExecution('DEBUG','End for ==', s_Task_Name);
			/* Search with Leave Task */
			
			yieldScript(obj_Usage_Context); /* to check usage and yield */
			
			/* Search with Holiday Task */
			var s_Task_Name = "Holiday";
			var i_title_id = 2480; /// this is a item on assignee tab which needs to hardcoded service item
			nlapiLogExecution('DEBUG','Starting for ==', s_Task_Name);
			check_Add_Assignee(s_Task_Name,i_title_id,arr_Allocation_Array,arr_Projects_Internal_Id,arr_Service_Item);
			nlapiLogExecution('DEBUG','End for ==', s_Task_Name);
			/* Search with Holiday Task */
			
			yieldScript(obj_Usage_Context); /* to check usage and yield */
			
			/* Search with OT Task */
			nlapiLogExecution('DEBUG','arr_Projects_Ot_Internal_Id.length==', arr_Projects_Ot_Internal_Id.length);
			if(arr_Projects_Ot_Internal_Id.length > 0)
				{
					var s_Task_Name = "OT";
					var i_title_id = 2425; /// this is a item on assignee tab which needs to hardcoded service item
					nlapiLogExecution('DEBUG','Starting for ==', s_Task_Name);
					check_Add_Assignee(s_Task_Name,i_title_id,arr_Allocation_OT_Array,arr_Projects_Ot_Internal_Id,arr_Service_Item);
					nlapiLogExecution('DEBUG','End for ==', s_Task_Name);
				} ////// if(arr_Projects_Ot_Internal_Id.length > 0)
			/* Search with OT Task */
			var d_Date_New = new Date();
			var t_time_End = d_Date_New.getTime();
			nlapiLogExecution('DEBUG', 't_time_End   ==',t_time_End);
			nlapiLogExecution('DEBUG', 'final_Time ==',((t_time_End - t_time_Begin)/1000));
			//nlapiLogExecution('DEBUG', 'Gap Seconds  ==',(Initial_Time - final_Time));
			nlapiLogExecution('DEBUG', 'End Usage  ==',(10000 - obj_Usage_Context.getRemainingUsage()));
			
		} /////// if(obj_Resrc_Allcn_Search)

	} /// End of try
	catch(s_Exp)
	{
		nlapiLogExecution('DEBUG', 'Exception in Validate_Assignees ==', s_Exp);
	} //// End of try
} ////function Validate_Assignees(type)

/* Function to check and add assignee */
function check_Add_Assignee(s_Task_Name,i_title_id,arr_Allocation_Array,arr_Projects_Internal_Id,arr_Service_Item)
{
	try
	{
		var arr_Filter_Task_Search = [];
	
		if(s_Task_Name == 'Project')
			{
			
				
				arr_Filter_Task_Search = [
			       					  [ ["title","startswith",s_Task_Name], 
			       					    "OR",
			       						["title","startswith","Bench"],
			       						"OR",
			       						["title","startswith","Operations"],
			       						"OR",
			       						["title","startswith","Internal"],
			       						"OR",
			       						["title","startswith","project"]],
			    					   "AND", 
			    					   ["job.internalid","anyof",arr_Projects_Internal_Id]];
	
			} ////// if(s_Task_Name == 'Project')
		else
			{
			 	arr_Filter_Task_Search = [
			       					   ["title","startswith",s_Task_Name], 
			    					   "AND", 
			    					   ["job.internalid","anyof",arr_Projects_Internal_Id]]; 
			} //// Else of if(s_Task_Name == 'Project')
	
		//nlapiLogExecution('DEBUG','arr_Filter_Task_Search==', JSON.stringify(arr_Filter_Task_Search))   					
			var arr_Cols_Task_Search = [
			     					   new nlobjSearchColumn("internalid"), 
			    					   new nlobjSearchColumn("company").setSort(true), 
			    					   new nlobjSearchColumn("resource","projectTaskAssignment",null)
			    					];
			var obj_Proj_Task_search = searchRecord('projecttask',null,arr_Filter_Task_Search,arr_Cols_Task_Search);
			var arr_Projects_task = [];
			var arr_Resourecs_adding = [];
			if(obj_Proj_Task_search)
				{
					nlapiLogExecution('DEBUG', 'obj_Proj_Task_search.length==', obj_Proj_Task_search.length);
					for(var i_Task_Index =0 ;i_Task_Index < obj_Proj_Task_search.length ; i_Task_Index++ )
						{
							var i_Resource_Id = obj_Proj_Task_search[i_Task_Index].getValue("resource","projectTaskAssignment");
							var i_Project_Id = obj_Proj_Task_search[i_Task_Index].getValue("company");
							if(arr_Projects_task.indexOf(i_Project_Id) == -1)
								{
									var i_Task_Internal_id = obj_Proj_Task_search[i_Task_Index].getValue("internalid");
									arr_Projects_task.push(i_Project_Id);
									arr_Resourecs_adding[i_Project_Id] = {"ProjecttaskId" : i_Task_Internal_id,"Resource" : [] };
									if(i_Resource_Id)
										{
											arr_Resourecs_adding[i_Project_Id]["Resource"].push(i_Resource_Id);
										} //// if(i_Resource_Id)
								} ///// if(arr_Projects_task.indexOf(i_Project_Id) == -1)
							else
								{
									if(i_Resource_Id)
										{
											arr_Resourecs_adding[i_Project_Id]["Resource"].push(i_Resource_Id); 
										} //// if(i_Resource_Id)
								
								} /// Else of if(arr_Projects_task.indexOf(i_Project_Id) == -1)
							//nlapiLogExecution('DEBUG','i_Project_Id==', i_Project_Id);
							//nlapiLogExecution('DEBUG','arr_Resourecs_adding[i_Project_Id]["Resource"]==', JSON.stringify(arr_Resourecs_adding[i_Project_Id]["Resource"]));
						}  ///// for(var i_Task_index =0 ;i_Task_index < obj_Proj_Task_search.length ; i_Task_index++ )
					/* Loop to check the Project activities is there or not */
					for(var i_main_index = 0 ; i_main_index < arr_Projects_Internal_Id.length ; i_main_index++) /// Iterating for all Projects got from resource allocation
						{
							var i_Project_Internal_id = arr_Projects_Internal_Id[i_main_index];
							//nlapiLogExecution('DEBUG','i_Project_Internal_id==', i_Project_Internal_id);
							for(var i_second_index = 0 ; i_second_index < arr_Allocation_Array[i_Project_Internal_id].length ; i_second_index++)
								{
									var cb_Flag = false;
									var i_Compare_resource = arr_Allocation_Array[i_Project_Internal_id][i_second_index];
								
									if(arr_Resourecs_adding[i_Project_Internal_id])
										{
											var i_Task_id = arr_Resourecs_adding[i_Project_Internal_id]["ProjecttaskId"];
											var obj_Task_record = nlapiLoadRecord('projecttask',i_Task_id);
											if(arr_Resourecs_adding[i_Project_Internal_id]["Resource"].indexOf(i_Compare_resource)  == -1 )
												{
													nlapiLogExecution('DEBUG','Resource missing allocation for Task ==',s_Task_Name);
													nlapiLogExecution('DEBUG','Resource missing allocation for Id==',i_Compare_resource);
													nlapiLogExecution('DEBUG','i_Task_id ==',i_Task_id);
													cb_Flag = true;
													/* Set task assignee*/
													obj_Task_record.selectNewLineItem('assignee')
													obj_Task_record.setCurrentLineItemValue('assignee','resource',i_Compare_resource);
													obj_Task_record.setCurrentLineItemValue('assignee','unitcost',0);
													if(s_Task_Name == 'Project')
													{
														i_title_id = arr_Service_Item[i_Project_Internal_id][i_Compare_resource];
														nlapiLogExecution('DEBUG','i_title_id ==',i_title_id);
														obj_Task_record.setCurrentLineItemText('assignee','serviceitem',i_title_id);
													} ///// if(i_title_id == null)
													else
														{
														  obj_Task_record.setCurrentLineItemValue('assignee','serviceitem',i_title_id);
														} /// else of if(i_title_id == null)
													obj_Task_record.setCurrentLineItemValue('assignee','unitprice',0);
													obj_Task_record.setCurrentLineItemValue('assignee','estimatedwork',100);
													obj_Task_record.commitLineItem('assignee');
													/* Set task assignee*/
												} //if(arr_Resourecs_adding[i_Project_Internal_id]["Resource"].indexOf(i_Compare_resource)  == -1 )
										} ////if(arr_Resourecs_adding[i_Project_Internal_id]["Resource"])
									else
										{
											nlapiLogExecution('DEBUG','Missing Task for == ',s_Task_Name);
											nlapiLogExecution('DEBUG','Missing Task for ProjectId == ',i_Project_Internal_id);
										} //else of if(arr_Resourecs_adding[i_Project_Internal_id]["Resource"])
									if(cb_Flag == true )
										{
											try
											{
												nlapiSubmitRecord(obj_Task_record, false, true);
											} //// End  of try
											catch(s_Task_Exception)
											{
												nlapiLogExecution('DEBUG','Error While Submitting Task Project ',i_Project_Internal_id )
												nlapiLogExecution('DEBUG','Error While Submitting Task Id ',i_Task_id )
												nlapiLogExecution('DEBUG','Error == ',s_Task_Exception)
												return;
											} /// End of catch
										} //// if(cb_Flag == true )
								} ////for(var i_second_index = 0 ; i_second_index < arr_Allocation_Array[i_Project_Internal_id].length ; i_second_index++)
				
						} //// for(var i_main_index = 0 ; i_main_index < arr_Resource_Internal_Id.length ; i_main_index++  )

					/* Loop to check the Project activities is there or not */
				} ///// if(obj_Proj_Task_search)
	} ///// ens of try
	catch(s_Exp)
	{
		nlapiLogExecution('DEBUG', 'Exception in check_Add_Assignee == ', s_Exp);
		var s_Subject = 'Task assignee Scheduled Script Failure';
		var s_Recipient = 'shravan.k@brillio.com';
		var s_Body = 'Please find Exception '+ s_Exp;
		nlapiSendEmail(442,s_Recipient , s_Subject, s_Body,'netsuite.support@brillio.com', null, {entity:442})
		return;
	} ////// catch(s_Exp)

} ///// function check_Add_Assignee()
/* Function to check and add assignee */
/* Function for Yield Script */
function yieldScript(currentContext) {
    if (currentContext.getRemainingUsage() <= 2000) 
    {
        nlapiLogExecution('AUDIT', 'API Limit Exceeded');
        var state = nlapiYieldScript();
        if (state.status == "FAILURE") 
        {
            nlapiLogExecution('ERROR', 'Yield Script Failed', 'Reason : ' +
                state.reason + ' / Size : ' + state.size);
            return false;
        }
        else if (state.status == "RESUME") {
            nlapiLogExecution('AUDIT', 'Script Resumed');
        }
    }
}
/* Function for Yield Script */