-/**
 * @author Shweta
 */
// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
    /*
           Script Name : SCH_Salary_Upload_JE_Creation.js
        Author      : Shweta Chopde
        Date        : 4 Aug 2014
        Description : Creation of Journal Entries
    
    
        Script Modification Log:
    
        -- Date --	    -- Modified By --				--Requested By--				-- Description --
        14-10-2014      swati kurariya                    nitin                        1.debit and credit a/c clubbing
                                                                                       2.
    
    
    
    
    Below is a summary of the process controls enforced by this script file.  The control logic is described
    more fully, below, in the appropriate function headers and code blocks.
    
    
         SCHEDULED FUNCTION
            - scheduledFunction(type)
    
    
         SUB-FUNCTIONS
            - The following sub-functions are called by the above core functions in order to maintain code
                modularization:
    
                   - NOT USED
    
    */
    }
    // END SCRIPT DESCRIPTION BLOCK  ====================================
    
    
    // BEGIN SCHEDULED FUNCTION =============================================
    
    function Practice_head_ticker(type) //
    {
        try //
        {
            nlapiLogExecution('ERROR', 'schedulerFunction', '*** Schedular Process Starts ***');
            
            var i_context = nlapiGetContext();
                    
            var i_CSV_File_ID =4455826;// //i_context.getSetting('SCRIPT', 'custscript_csv_file_id_je');
            var i_monthly_employee = 'T';////i_context.getSetting('SCRIPT', 'custscript_monthly_employee_je');
            var i_hourly_employee ='F'; //i_context.getSetting('SCRIPT', 'custscript_hourly_employee_je');
            var i_account_debit =1297; //i_context.getSetting('SCRIPT', 'custscript_account_debit_je');
            var i_account_credit =1296; //i_context.getSetting('SCRIPT', 'custscript_account_credit_je');
            var i_recordID =1951 //i_context.getSetting('SCRIPT', 'custscript_record_id_je');
            var i_monthly_salary_record_arr =''; //i_context.getSetting('SCRIPT', 'custscript_record_id_array');
            
            var i_Salaried_OT ='F'; //i_context.getSetting('SCRIPT', 'custscript_sal_emp_ot_hrs_je_create');
            var i_Salaried_OT_Diff ='F'; //i_context.getSetting('SCRIPT', 'custscript_sal_emp_ot_hrs_diff_create');
            var i_Hourly_Diff ='F'; //i_context.getSetting('SCRIPT', 'custscript_hourly_emp_diff_hrs_je_create');
            
            var i_hourly_FP ='F'; //i_context.getSetting('SCRIPT', 'custscript_hourly_emp_fp_je_create');
            var i_hourly_FP_DIFF = 'F';//i_context.getSetting('SCRIPT', 'custscript_hourly_emp_fp_diff_je_create');
            
            var i_hourly_FP_Internal = 'F';//i_context.getSetting('SCRIPT', 'custscript_hourly_emp_internal_je_create');
            var i_hourly_FP_Internal_Diff = 'F';//i_context.getSetting('SCRIPT', 'custscript_hourly_emp_internal_diff_je');
            
            
            
            if (_logValidation(i_monthly_salary_record_arr)) //
            {
                //nlapiLogExecution('DEBUG', 'schedulerFunction',' Monthly Salary Record Arr Length -->' + i_monthly_salary_record_arr.length);
                
                
                var a_CRT_array_values = new Array()
                var i_data_CRT = new Array()
                i_data_CRT = i_monthly_salary_record_arr;
                
                if (_logValidation(i_data_CRT)) //
                {
                    for (var dct = 0; dct < i_data_CRT.length; dct++) //
                    {
                        a_CRT_array_values = i_data_CRT.split(',')
                        break;
                    }
                }
                //nlapiLogExecution('DEBUG', 'time_tracking_details','CRT Array Values-->' +a_CRT_array_values);
                //nlapiLogExecution('DEBUG', 'time_tracking_details',' CRT Array Values Length-->' + a_CRT_array_values.length);
            }
            
            var i_usage_begin = i_context.getRemainingUsage();
            //nlapiLogExecution('DEBUG', 'schedulerFunction',' Usage Begin -->' + i_usage_begin);
            
            var i_counter = i_context.getSetting('SCRIPT', 'custscript_counter');
            
            if (i_counter != null && i_counter != '' && i_counter != undefined) //
            {
                i_counter = i_counter;
            }//if
            else //
            {
                i_counter = 0;
            }
            
            // ================== Monthly Employee ================
            
			
			
            if (i_monthly_employee == 'T') //
            {
                monthly_JE_creation(i_context, i_counter, a_CRT_array_values, i_recordID, i_CSV_File_ID, i_account_debit, i_account_credit, i_monthly_employee, i_hourly_employee);
                
            }//Monthly Employee
           
        
        } 
        catch (exception) //
        {
            
            
            
        }//CATCH
        
        nlapiLogExecution('ERROR', 'schedulerFunction', '*** Schedular Process Ends ***');
    }
    
    // END SCHEDULED FUNCTION ===============================================
    
    
    
    
    
    // BEGIN FUNCTION ===================================================
    /**
     *
     * @param {Object} value
     *
     * Description --> If the value is blank /null/undefined returns false else returns true
     */
    
    function _logValidation(value)
    {
     if(value!='null' && value != null && value != null && value != '' && value != undefined && value != undefined && value != 'undefined' && value!= 'undefined'&& value!= 'NaN' && value != NaN)
     {
      return true;
     }
     else
     {
      return false;
     }
    }
    
    var hourly_DIFF_Check = false;
    
    
    
    
    function monthly_JE_creation(i_context, i_counter, i_monthly_salary_record_arr, i_recordID, i_CSV_File_ID, i_account_debit, i_account_credit, i_monthly_employee, i_hourly_employee) //
    {

        var a_employee_array = new Array();
        var a_data_array = new Array();
        var a_process_emp_array = new Array();
        var a_project_array = new Array();
        var a_mon_data_array = new Array();
        var a_employee_revision_array = new Array();
        var a_employee_revision_date_array = new Array();
        var i_rnt = 0;
        var i_m_flag = 0;
        var a_JE_array = new Array();
        var i_sal_recordID;
        var a_salary_upload_monthly_ID_array = new Array();
        var i_sch = 0;
        var i_project_wise_total = 0;
        var new_status = 5;
        
        var employee_array_credit = new Array();
        var employee_cost = new Array();
        // ======================= Search  Salary Upload Monthly Records ===================
        //nlapiLogExecution('DEBUG', 'schedulerFunction',' --------------------------------- i_recordID-->' + i_recordID);

        var vendorList = { };

       var default_vendor_mappingSearch = searchRecord("customrecord_default_vendor_mapping",null,
        [
        ], 
        [
          new nlobjSearchColumn("custrecord_subsidiary_je"), 
          new nlobjSearchColumn("custrecord_default_vendor")
        ]
        );

         for (var i_index = 0; i_index < default_vendor_mappingSearch.length; i_index++)
            {
             var subsidiary_je = default_vendor_mappingSearch[i_index].getValue('custrecord_subsidiary_je');
             var default_vendor_je = default_vendor_mappingSearch[i_index].getValue('custrecord_default_vendor');
    
              vendorList[subsidiary_je] = default_vendor_je;
                
            }


        if (_logValidation(i_recordID) && _logValidation(i_CSV_File_ID)) //
        {
            i_sal_recordID = i_recordID;
            var filters = new Array();
            filters[0] = new nlobjSearchFilter('custrecord_csv_file_m_s_p_u', null, 'is', i_CSV_File_ID)
            // filters[0] = new nlobjSearchFilter('custrecord_csv_file_m_s_p_u', null, 'is','7389')
            //filters[0] = new nlobjSearchFilter('custrecord_salary_upload_id_m', null, 'is','28')
            filters[1] = new nlobjSearchFilter('custrecord_salary_upload_id_m', null, 'is', i_recordID)//custrecord_salary_upload_id_m
            filters[2] = new nlobjSearchFilter('custrecord_emp_type_m_s_p_u', null, 'is', 1)
            //filters[3] = new nlobjSearchFilter('custrecord_process_check', null, 'is', 'F')
            filters[3] = new nlobjSearchFilter('custrecord_employee_rec_id', null, 'anyof', ['1794','232766'])
            
            /*   if(_logValidation(i_monthly_salary_record_arr))
             {
             filters[3] = new nlobjSearchFilter('internalid', null,'noneof',i_monthly_salary_record_arr)
             }*/
            var column = new Array();
            column[0] = new nlobjSearchColumn('internalid')
            column[1] = new nlobjSearchColumn('custrecord_employee_rec_id')//custrecord_employee_rec_id
            column[2] = new nlobjSearchColumn('custrecord_subsidiary_id_s')
            column[3] = new nlobjSearchColumn('custrecord_location_id_s')
            column[4] = new nlobjSearchColumn('custrecord_practice_m_s_p_u')
            column[5] = new nlobjSearchColumn('custrecord_currency_m_s_p_u')//custrecord_currency_id_m_sp_u
            column[6] = new nlobjSearchColumn('custrecord_revision_date_m_s_p_u')//custrecord_emp_id_m_s_p_u
            column[7] = new nlobjSearchColumn('custrecord_month_m_s_p_u')
            column[8] = new nlobjSearchColumn('custrecord_year_m_s_p_u')
            column[9] = new nlobjSearchColumn('custrecord_cost_m_s_p_u')
            
            var a_results = searchRecord('customrecord_salary_upload_monthly_file', null, filters, column);
            
            
            var i_usage_begin_1_b = i_context.getRemainingUsage();
            //nlapiLogExecution('DEBUG', 'schedulerFunction',' --------------------------------- Usage Begin Process-->' + i_usage_begin_1_b);
            
            var count = 0;
            if (_logValidation(a_results)) //
            {//if result start
                //nlapiLogExecution('DEBUG', 'monthly_JE_creation', ' ************************** Monthly Upload Results Length **************************  -->' + a_results.length);
                
                for (var i = 0, count = 1; i < a_results.length; i++, count++) //
                {//resul
                    if (count == '450') //
                    {
                        break;
                    }
                   
                    
                    var i_recordID = a_results[i].getValue('internalid');
                    //nlapiLogExecution('DEBUG', 'monthly_JE_creation', ' Record ID -->' + i_recordID);
                    
                    var i_employeeID = a_results[i].getValue('custrecord_employee_rec_id');
                    //nlapiLogExecution('DEBUG', 'monthly_JE_creation', ' Employee ID -->' + i_employeeID);
                    
                    var i_subsidiary = a_results[i].getValue('custrecord_subsidiary_id_s');
                    //nlapiLogExecution('DEBUG', 'monthly_JE_creation', ' Subsidiary  -->' + i_subsidiary);
                    
                    var i_location = a_results[i].getValue('custrecord_location_id_s');
                    //nlapiLogExecution('DEBUG', 'monthly_JE_creation', ' Location -->' + i_location);
                    
                    var i_practice = a_results[i].getValue('custrecord_practice_m_s_p_u');
                    //nlapiLogExecution('DEBUG', 'monthly_JE_creation', ' Practice -->' + i_practice);
                    
                    var i_currency = a_results[i].getValue('custrecord_currency_m_s_p_u');
                    //nlapiLogExecution('DEBUG', 'monthly_JE_creation', ' Currency -->' + i_currency);
                    
                    var d_revision_date = a_results[i].getValue('custrecord_revision_date_m_s_p_u');
                    //nlapiLogExecution('DEBUG', 'monthly_JE_creation', ' Revision Date -->' + d_revision_date);
                    
                    var i_month = a_results[i].getValue('custrecord_month_m_s_p_u');
                    //nlapiLogExecution('DEBUG', 'monthly_JE_creation', ' Month -->' + i_month);
                    
                    var i_year = a_results[i].getValue('custrecord_year_m_s_p_u');
                    //nlapiLogExecution('DEBUG', 'monthly_JE_creation', ' Year -->' + i_year);
                    
                    var i_cost = a_results[i].getValue('custrecord_cost_m_s_p_u');
                    //nlapiLogExecution('DEBUG', 'monthly_JE_creation', ' Cost -->' + i_cost);
                    
                    for (var it = 1; it <= 50; it++) //
                    {
                        if (i_year == it) //
                        {
                            i_year = '20' + i_year;
                            break;
                        }
                    }
                    //nlapiLogExecution('DEBUG', 'monthly_JE_creation', ' After Year -->' + i_year);
                    
                    if (_logValidation(i_monthly_salary_record_arr)) //
                    {
                        //nlapiLogExecution('DEBUG', 'monthly_JE_creation', ' REcord ID Present -->' + i_monthly_salary_record_arr.indexOf(i_recordID));
                        
                        if (i_monthly_salary_record_arr.indexOf(i_recordID) == -1) //
                        {
                            if (_logValidation(d_revision_date)) //
                            {
                                // 	 i_rnt++;
                                a_employee_revision_date_array[i_rnt++] = i_employeeID + '%%%^@@@' + d_revision_date;
                                a_employee_revision_array.push(i_employeeID);
                            }
                            
                            a_employee_array.push(i_employeeID);
                            employee_array_credit.push(i_employeeID);
                            
                            employee_array_credit = removearrayduplicate(employee_array_credit)
                            
                            a_data_array[i] = i_recordID + '###%%###' + i_month + '###%%###' + i_year + '###%%###' + i_employeeID + '###%%###' + i_subsidiary + '###%%###' + i_location + '###%%###' + i_practice + '###%%###' + i_currency + '###%%###' + d_revision_date + '###%%###' + i_cost;
                        }
                        
                    }
                    else //
                    {
                        if (_logValidation(d_revision_date)) //
                        {
                            // 	 i_rnt++;
                            a_employee_revision_date_array[i_rnt++] = i_employeeID + '%%%^@@@' + d_revision_date;
                            a_employee_revision_array.push(i_employeeID);
                        }
                        
                        a_employee_array.push(i_employeeID);
                        employee_array_credit.push(i_employeeID);
                        
                        employee_array_credit = removearrayduplicate(employee_array_credit)
                        
                        a_data_array[i] = i_recordID + '###%%###' + i_month + '###%%###' + i_year + '###%%###' + i_employeeID + '###%%###' + i_subsidiary + '###%%###' + i_location + '###%%###' + i_practice + '###%%###' + i_currency + '###%%###' + d_revision_date + '###%%###' + i_cost;
                    }
                    var i_usage_begin_1_b = i_context.getRemainingUsage();
                    //nlapiLogExecution('DEBUG', 'schedulerFunction',' --------------------------------- Usage Begin Process-->' + i_usage_begin_1_b);
                
                }//Results Loop		
                
                
                
                
                var i_usage_begin_1 = i_context.getRemainingUsage();
                //nlapiLogExecution('DEBUG', 'after loop end schedulerFunction',' ---------------------------------- Usage Begin 1-->' + i_usage_begin_1);
                
                var i_emp_l;
                var a_process_array = new Array();
                
                if (_logValidation(a_employee_array)) //
                {//1 if start
                    i_emp_l = parseInt(a_employee_array.length) / parseInt(10);
                    //nlapiLogExecution('DEBUG', 'monthly_JE_creation', ' i_emp_l -->' + i_emp_l);
                    
                    i_emp_l = Math.ceil(i_emp_l)
                    //nlapiLogExecution('DEBUG', 'monthly_JE_creation', ' No. Of JEs to be Created -->' + i_emp_l);
                    
                    //for(var et = 0; et<i_emp_l ; et++)
                    {//Employee Array Length For 2..........c
                        var i_emp_nt = 0;
                        
                        if (i_emp_nt < 500) //
                        {
                            //more that if start
                            // ===================== Create JE =======================
                            
                            var o_journalOBJ = nlapiCreateRecord('journalentry')
                            
                            // ==================== Employee Array ===============
                            
                            if (_logValidation(employee_array_credit)) //
                            {
                                for (var bg = 0; bg < employee_array_credit.length; bg++) //
                                {
                                    //nlapiLogExecution('DEBUG', 'schedulerFunction',' --------------------------------- bg-->' + bg);
                                    var i_hire_date = '';
                                    var i_resigned_date = '';
                                    //var i_project_wise_total = 0;
                                    var i_p_flag = 0;
                                    
                                    
                                    var o_empOBJ = nlapiLoadRecord('employee', employee_array_credit[bg])
                                    
                                    if (_logValidation(o_empOBJ)) //
                                    {//Employee OBJ
                                        i_hire_date = o_empOBJ.getFieldValue('hiredate');
                                        nlapiLogExecution('DEBUG', 'monthly_JE_creation', ' Hire Date  -->' + i_hire_date);
                                        
                                        //i_resigned_date = o_empOBJ.getFieldValue('releasedate');  //custentity_lwd
                                        i_resigned_date = o_empOBJ.getFieldValue('custentity_lwd'); //
                                        nlapiLogExecution('DEBUG', 'monthly_JE_creation', ' Resigned Date -->' + i_resigned_date);
                                    }
                                    
                                    if (_logValidation(a_data_array)) //
                                    {//data array if
                                        var a_split_array = new Array();
                                        
                                        for (var me = 0; me < a_data_array.length; me++) //
                                        {
                                            //data array for
                                            
                                            if (_logValidation(a_data_array[me])) //
                                            {
                                                //data array null
                                                a_split_array = a_data_array[me].split('###%%###');
                                                
                                                var i_recordID = a_split_array[0];
                                                var i_month = a_split_array[1];
                                                var i_year = a_split_array[2];
                                                var i_employeeID = a_split_array[3];
                                                var i_subsidiary = a_split_array[4];
                                                var i_location = a_split_array[5];
                                                var i_practice = a_split_array[6];
                                                var i_currency = a_split_array[7];
                                                var d_revision_date = a_split_array[8];
                                                var i_cost = a_split_array[9];
                                                
                                                if (employee_array_credit[bg] == i_employeeID && i_p_flag == 0) //
                                                {
                                                    if ((a_process_array.indexOf(i_recordID) == -1)) //
                                                    {
                                                        var i_project_wise_cost = 0;
                                                        
                                                        //nlapiLogExecution('DEBUG','i_currency============>'+i_currency);
                                                        //nlapiLogExecution('DEBUG','i_subsidiary============>'+i_subsidiary);
                                                        //o_journalOBJ.setFieldValue('currency',i_currency);
                                                        
                                                        if (i_currency == 'USD') //
                                                        {
                                                            i_currency = '1';
                                                        }
                                                        else 
                                                            if (i_currency == 'GBP') //
                                                            {
                                                                i_currency = '2';
                                                            }
                                                            else 
                                                                if (i_currency == 'EUR') //
                                                                {
                                                                    i_currency = '4';
                                                                }
                                                                else 
                                                                    if (i_currency == 'PHP') //
                                                                    {
                                                                        i_currency = '5';
                                                                    }
                                                                    else 
                                                                        if (i_currency == 'INR') //
                                                                        {
                                                                            i_currency = '6';
                                                                        }
                                                                        else 
                                                                            if (i_currency == 'SGD') //
                                                                            {
                                                                                i_currency = '7';
                                                                            }
                                                                            
                                                        o_journalOBJ.setFieldValue('currency', i_currency);
                                                        o_journalOBJ.setFieldValue('subsidiary', i_subsidiary);
                                                        o_journalOBJ.setFieldValue('trandate', get_todays_date());
                                                        o_journalOBJ.setFieldValue('custbody_salary_upload_process_je', i_sal_recordID);
                                                        
                                                        // ================= Get Month Details =======================
                                                        
                                                        
                                                        var d_start_date = '';
                                                        var d_end_date = '';
                                                        var i_day;
                                                        var i_total_sat_sun = '';
                                                        
                                                        
                                                        // ==================== New Joinee & Resignee Details ===============
                                                        
                                                        
                                                        var s_month_start_date = get_current_month_start_date(i_month, i_year);
                                                        var s_month_end_date = get_current_month_end_date(i_month, i_year);
                                                        
                                                        
                                                        var i_hire_date_dt_format = nlapiStringToDate(i_hire_date);
                                                        var i_resigned_date_dt_format = nlapiStringToDate(i_resigned_date);
                                                        var s_month_start_date_dt_format = nlapiStringToDate(s_month_start_date);
                                                        var s_month_end_date_dt_format = nlapiStringToDate(s_month_end_date);
                                                        
                                                        //nlapiLogExecution('DEBUG', ' monthly_JE_creation',' Month Start Date  .....'+s_month_start_date);
                                                        //nlapiLogExecution('DEBUG', ' monthly_JE_creation',' Month End Date .....'+s_month_end_date);
                                                        
                                                        if (_logValidation(i_hire_date_dt_format) && !_logValidation(i_resigned_date_dt_format)) //
                                                        {
                                                            if ((Date.parse(i_hire_date_dt_format) <= Date.parse(s_month_end_date_dt_format) && Date.parse(i_hire_date_dt_format) >= Date.parse(s_month_start_date_dt_format))) //
                                                            {
                                                                //nlapiLogExecution('DEBUG', ' monthly_JE_creation',' 1 Hire Date is current month .....');												  
                                                                
                                                                d_start_date = i_hire_date
                                                                d_end_date = get_current_month_end_date(i_month, i_year)
                                                                
                                                            }
                                                            else //
                                                            {
                                                                d_start_date = get_current_month_start_date(i_month, i_year)
                                                                d_end_date = get_current_month_end_date(i_month, i_year)
                                                                
                                                            }														
                                                        }
                                                        
                                                        if (_logValidation(i_hire_date_dt_format) && _logValidation(i_resigned_date_dt_format)) //
                                                        {
                                                            if ((Date.parse(i_hire_date_dt_format) <= Date.parse(s_month_end_date_dt_format) && Date.parse(i_hire_date_dt_format) >= Date.parse(s_month_start_date_dt_format))) //
                                                            {
                                                                if ((Date.parse(i_resigned_date_dt_format) <= Date.parse(s_month_end_date_dt_format) && Date.parse(i_resigned_date_dt_format) >= Date.parse(s_month_start_date_dt_format))) //
                                                                {
                                                                    //nlapiLogExecution('DEBUG', ' monthly_JE_creation',' 2 Hire Date is current month .....');
                                                                    d_start_date = i_hire_date;
                                                                    
                                                                    //nlapiLogExecution('DEBUG', ' monthly_JE_creation',' 2 Resigned Date is current month .....');
                                                                    d_end_date = i_resigned_date
                                                                }
                                                                else //
                                                                {
                                                                    //nlapiLogExecution('DEBUG', ' monthly_JE_creation',' 2 A Hire Date is current month .....');
                                                                    d_start_date = i_hire_date;
                                                                    
                                                                    //nlapiLogExecution('DEBUG', ' monthly_JE_creation',' 2 A Resigned Date is current month .....');
                                                                    d_end_date = get_current_month_end_date(i_month, i_year)
                                                                    
                                                                }
                                                                
                                                            }
                                                            else 
                                                                if ((Date.parse(i_resigned_date_dt_format) <= Date.parse(s_month_end_date_dt_format) && Date.parse(i_resigned_date_dt_format) >= Date.parse(s_month_start_date_dt_format))) //
                                                                {
                                                                    d_start_date = get_current_month_start_date(i_month, i_year);
                                                                    d_end_date = i_resigned_date;																
                                                                }
                                                                else //
                                                                {
                                                                    d_start_date = get_current_month_start_date(i_month, i_year)
                                                                    d_end_date = get_current_month_end_date(i_month, i_year)															
                                                                }
                                                        }
                                                        
                                                        if (!_logValidation(i_hire_date_dt_format) && _logValidation(i_resigned_date_dt_format)) //
                                                        {
                                                            if ((Date.parse(i_resigned_date_dt_format) <= Date.parse(s_month_end_date_dt_format) && Date.parse(i_resigned_date_dt_format) >= Date.parse(s_month_start_date_dt_format))) //
                                                            {
                                                                //nlapiLogExecution('DEBUG', ' monthly_JE_creation',' 3 Resigned Date is current month .....');
                                                                
                                                                d_start_date = get_current_month_start_date(i_month, i_year)
                                                                d_end_date = i_resigned_date
                                                                
                                                            }
                                                            else //
                                                            {
                                                                d_start_date = get_current_month_start_date(i_month, i_year)
                                                                d_end_date = get_current_month_end_date(i_month, i_year)
                                                                
                                                            }
                                                            
                                                        }
                                                        
                                                        if (!_logValidation(i_hire_date_dt_format) && !_logValidation(i_resigned_date_dt_format)) //
                                                        {
                                                            //nlapiLogExecution('DEBUG', ' monthly_JE_creation',' No Hire Date & Resigned Date .....');
                                                            
                                                            d_start_date = get_current_month_start_date(i_month, i_year)
                                                            d_end_date = get_current_month_end_date(i_month, i_year)
                                                            
                                                        }//Hire Date is not blank & Termination Date is blank
                                                        //nlapiLogExecution('DEBUG', ' monthly_JE_creation',' ................. Start Date  ............'+d_start_date);
                                                        //nlapiLogExecution('DEBUG', ' monthly_JE_creation',' ................. End Date  ............ '+d_end_date);
                                                        
                                                        
                                                        if (_logValidation(d_start_date)) //
                                                        {
                                                            d_start_date = nlapiStringToDate(d_start_date);
                                                            d_start_date = nlapiDateToString(d_start_date);
                                                        }
                                                        
                                                        if (_logValidation(d_end_date)) //
                                                        {
                                                            d_end_date = nlapiStringToDate(d_end_date);
                                                            d_end_date = nlapiDateToString(d_end_date)
                                                            
                                                        }
                                                        //nlapiLogExecution('DEBUG', 'monthly_JE_creation','Start Date -->' +d_start_date);
                                                        //nlapiLogExecution('DEBUG', 'monthly_JE_creation','End Date -->' +d_end_date);
                                                        
                                                        i_total_days = getDatediffIndays(d_start_date, d_end_date);
                                                        
                                                        i_total_sat_sun = getWeekend(d_start_date, d_end_date);
                                                        
                                                        //-----------------------------------------------------------------------------------
                                                        var d_start_date_whole_month = get_current_month_start_date(i_month, i_year)
                                                        var d_end_date_whole_month = get_current_month_end_date(i_month, i_year)
                                                        
                                                        var i_total_days_whole_month = getDatediffIndays(d_start_date_whole_month, d_end_date_whole_month);
                                                        
                                                        var i_total_sat_sun_whole_month = getWeekend(d_start_date_whole_month, d_end_date_whole_month);
                                                        var i_no_days_in_Whole_month = parseInt(i_total_days_whole_month) - parseInt(i_total_sat_sun_whole_month);
                                                        
                                                        //-------------------------------------------------------------------------------------
                                                        
                                                        //nlapiLogExecution('DEBUG', 'monthly_JE_creation',' Total Sat n Sun -->' +i_total_sat_sun);
                                                        //nlapiLogExecution('DEBUG', 'monthly_JE_creation',' Total Days -->' +i_total_days);
                                                        
                                                        nlapiLogExecution('DEBUG', 'monthly_JE_creation', ' i_employeeID -->' + i_employeeID);
                                                        nlapiLogExecution('DEBUG', 'monthly_JE_creation', ' d_start_date -->' + d_start_date);
                                                        nlapiLogExecution('DEBUG', 'monthly_JE_creation', ' d_end_date -->' + d_end_date);
                                                        
                                                        var i_usage_begin_1_1 = i_context.getRemainingUsage();
                                                        //nlapiLogExecution('DEBUG', 'schedulerFunction',' ---------------------------------- Usage Begin Before Set -->' + i_usage_begin_1_1);
                                                        
                                                        var a_pr_split_array = new Array();
                                                        var a_project_ID_arr = new Array();
                                                        
                                                        var a_monthly_cal_details = new Array();
                                                        a_monthly_cal_details_array = search_monthly_calculation_details(i_employeeID, d_start_date, d_end_date)
                                                        //nlapiLogExecution('DEBUG', 'monthly_JE_creation', ' Monthly Cal Details Arr Details -->' + a_monthly_cal_details_array)
                                                        
                                                        if (_logValidation(a_monthly_cal_details_array)) //
                                                        {
                                                            a_pr_split_array = a_monthly_cal_details_array[0].split('^^^^***^^^^');
                                                            a_project_ID_arr = a_pr_split_array[0];
                                                            a_monthly_cal_details = a_pr_split_array[1];
                                                        }
                                                        
                                                        var a_PRT_array_values = new Array()
                                                        var i_data_PRT = new Array()
                                                        i_data_PRT = a_project_ID_arr;
                                                        
                                                        for (var dct = 0; dct < i_data_PRT.length; dct++) //
                                                        {
                                                            a_PRT_array_values = i_data_PRT.split(',')
                                                            break;
                                                        }
                                                        a_PRT_array_values = removearrayduplicate(a_PRT_array_values)
                                                        
                                                        
                                                        //nlapiLogExecution('DEBUG', 'monthly_JE_creation','PRT Array Values-->' +a_PRT_array_values);
                                                        //nlapiLogExecution('DEBUG', 'monthly_JE_creation',' PRT Array Values Length-->' + a_PRT_array_values.length);
                                                        
                                                        
                                                        var a_CRT_array_values = new Array()
                                                        var i_data_CRT = new Array()
                                                        i_data_CRT = a_monthly_cal_details;
                                                        
                                                        for (var dct = 0; dct < i_data_CRT.length; dct++) //
                                                        {
                                                            a_CRT_array_values = i_data_CRT.split(',')
                                                            break;
                                                        }
                                                        
                                                        //nlapiLogExecution('DEBUG', 'monthly_JE_creation','CRT Array Values-->' +a_CRT_array_values);
                                                        //nlapiLogExecution('DEBUG', 'monthly_JE_creation',' CRT Array Values Length-->' + a_CRT_array_values.length);
                                                        
                                                        var a_split_project_array = new Array();
                                                        var a_split_project_results_array = new Array();
                                                        //	 if (i_emp_nt < 11)
                                                        {
                                                            if (_logValidation(a_CRT_array_values) && _logValidation(a_PRT_array_values)) //
                                                            {
                                                                //var i_project_wise_total=0;
                                                                for (var pid = 0; pid < a_PRT_array_values.length; pid++) //
                                                                {
                                                                    a_split_project_results_array = a_PRT_array_values[pid].split('^^&&&**^^');
                                                                    
                                                                    var i_project_ID_PE = a_split_project_results_array[0];
                                                                    nlapiLogExecution('DEBUG', 'monthly_JE_creation', 'i_project_ID_PE-->' + i_project_ID_PE);
                                                                    
                                                                    var i_vertical_PE = a_split_project_results_array[1];
                                                                    var i_practice_PE = a_split_project_results_array[2];
                                                                    
                                                                    
                                                                    var i_total_duration = 0;
                                                                    var i_hours_per_week_total = 0;
                                                                    var i_total_salary_for_month = 0;
                                                                    var i_employee_salary_per_day = 0;
                                                                    var i_allocated_hours_on_project = 0;
                                                                    var i_actual_hours_worked_on_project = 0;
                                                                    var i_no_of_days_worked_on_project = 0;
                                                                    var i_no_days_in_month = 0;
                                                                    
                                                                    var i_internal_ID;
                                                                    var i_vertical;
                                                                    var i_practice;
                                                                    var i_projectID;
                                                                    var i_rate;
                                                                    var i_duration;
                                                                    var i_item;
                                                                    var i_hours_per_week;
                                                                    var i_allocated_hours;
                                                                    var i_project_name;
                                                                    
                                                                    
                                                                    i_total_salary_for_month = i_cost;
                                                                    i_no_days_in_month = parseInt(i_total_days) - parseInt(i_total_sat_sun);
                                                                    nlapiLogExecution('DEBUG', 'monthly_JE_creation', ' No Days In Month-->' + i_no_days_in_month);
                                                                    nlapiLogExecution('DEBUG', 'monthly_JE_creation', ' i_total_days-->' + i_total_days);
                                                                    nlapiLogExecution('DEBUG', 'monthly_JE_creation', ' i_total_sat_sun-->' + i_total_sat_sun);
                                                                    
                                                                    for (var pe = 0; pe < a_CRT_array_values.length; pe++) //
                                                                    {
                                                                        a_split_project_array = a_CRT_array_values[pe].split('&&&%%&&&');
                                                                        
                                                                        i_internal_ID = a_split_project_array[0];
                                                                        i_vertical = a_split_project_array[1];
                                                                        i_practice = a_split_project_array[2];
                                                                        i_projectID = a_split_project_array[3];
                                                                        //nlapiLogExecution('DEBUG', 'monthly_JE_creation','i_projectID-->' +i_projectID);
                                                                        //nlapiLogExecution('DEBUG', 'monthly_JE_creation','i_project_ID_PE-->' +i_project_ID_PE);
                                                                        
                                                                        i_rate = a_split_project_array[4];
                                                                        i_duration = a_split_project_array[5];
                                                                        i_item = a_split_project_array[6];
                                                                        i_hours_per_week = a_split_project_array[7];
                                                                        i_allocated_hours = a_split_project_array[8];
                                                                        i_project_name = a_split_project_array[9];
                                                                        
                                                                        
                                                                        if (i_projectID == i_project_ID_PE) //
                                                                        {
                                                                            //nlapiLogExecution('DEBUG', 'monthly_JE_creation','*** Project Matched ***');
                                                                            // Making Sum of Allocated hours for the employee (project wise)
                                                                            i_total_duration = parseFloat(i_total_duration) + parseFloat(i_duration);
                                                                            i_hours_per_week_total = i_allocated_hours;
                                                                        }
                                                                    }
                                                                    var i_Number_Of_days = 0;
                                                                    
                                                                    nlapiLogExecution('DEBUG', 'monthly_JE_creation', ' i_no_days_in_Whole_month-->' + i_no_days_in_Whole_month);
                                                                    
                                                                    nlapiLogExecution('DEBUG', 'monthly_JE_creation', ' i_no_days_in_month-->' + i_no_days_in_month);
                                                                    
                                                                    if (i_no_days_in_Whole_month == i_no_days_in_month) //
                                                                    {
                                                                        nlapiLogExecution('DEBUG', 'monthly_JE_creation', '*** 1 ***');
                                                                        
                                                                        i_actual_hours_worked_on_project = i_total_duration;
                                                                        i_allocated_hours_on_project = i_hours_per_week_total;
                                                                        
                                                                        i_employee_salary_per_day = parseFloat(i_total_salary_for_month) / parseFloat(i_no_days_in_month)
                                                                        i_no_of_days_worked_on_project = ((parseFloat(i_actual_hours_worked_on_project)) / parseFloat(8));
                                                                        //i_Number_Of_days = parseFloat(i_no_of_days_worked_on_project) * parseFloat(8);
                                                                        i_Number_Of_days = parseFloat(i_no_of_days_worked_on_project); // * parseFloat(8);
                                                                        
                                                                        i_project_wise_cost = parseFloat(i_employee_salary_per_day) * parseFloat(i_no_of_days_worked_on_project)
                                                                        
                                                                        //nlapiLogExecution('DEBUG', 'monthly_JE_creation', ' No Days In Month-->' + i_no_days_in_month);
                                                                        nlapiLogExecution('DEBUG', 'monthly_JE_creation', ' i_no_of_days_worked_on_project-->' + i_no_of_days_worked_on_project);
                                                                        
                                                                        nlapiLogExecution('DEBUG', 'monthly_JE_creation', ' i_actual_hours_worked_on_project -->' + i_actual_hours_worked_on_project);
                                                                        nlapiLogExecution('DEBUG', 'monthly_JE_creation', ' i_employee_salary_per_day -->' + i_employee_salary_per_day);
                                                                        nlapiLogExecution('DEBUG', 'monthly_JE_creation', ' i_project_wise_cost -->' + i_project_wise_cost);
                                                                        
                                                                    }
                                                                    else //
                                                                    {
                                                                        nlapiLogExecution('DEBUG', 'monthly_JE_creation', '*** 2 ***');
                                                                        
                                                                        //i_Number_Of_days = parseFloat(i_no_days_in_month) * parseFloat(8);
                                                                        i_Number_Of_days = parseFloat(i_total_duration) / parseFloat(8);
                                                                        i_employee_salary_per_day = parseFloat(i_total_salary_for_month) / parseFloat(i_no_days_in_Whole_month);
                                                                        //i_project_wise_cost = parseFloat(i_employee_salary_per_day) * parseFloat(i_no_days_in_month);
                                                                        i_project_wise_cost = parseFloat(i_employee_salary_per_day) * parseFloat(i_Number_Of_days);
                                                                        
                                                                        nlapiLogExecution('DEBUG', 'monthly_JE_creation', ' i_total_duration new cal -->' + i_total_duration);
                                                                        
                                                                        nlapiLogExecution('DEBUG', 'monthly_JE_creation', ' i_project_wise_cost new cal -->' + i_project_wise_cost);
                                                                        nlapiLogExecution('DEBUG', 'monthly_JE_creation', ' i_Number_Of_days new cal -->' + i_Number_Of_days);
                                                                        nlapiLogExecution('DEBUG', 'monthly_JE_creation', ' i_employee_salary_per_day new cal -->' + i_employee_salary_per_day);
                                                                        nlapiLogExecution('DEBUG', 'monthly_JE_creation', ' i_no_days_in_month new cal -->' + i_no_days_in_month);
                                                                        //nlapiLogExecution('DEBUG', 'monthly_JE_creation', ' i_no_days_in_Whole_month new cal -->' + i_no_days_in_Whole_month);
                                                                    }
                                                                    
                                                                    i_project_wise_cost = parseFloat(i_project_wise_cost).toFixed(2);
                                                                    
                                                                    i_project_wise_total = parseFloat(i_project_wise_total) + parseFloat(i_project_wise_cost);
                                                                    
                                                                    i_project_wise_total = parseFloat(i_project_wise_total).toFixed(2);
                                                                    
                                                                    
                                                                    if (!_logValidation(i_practice)) //
                                                                    {
                                                                        i_practice = '';
                                                                    }
                                                                    if (!_logValidation(i_vertical)) //
                                                                    {
                                                                        i_vertical = '';
                                                                    }
                                                                    if (!_logValidation(i_project_name)) //
                                                                    {
                                                                        i_project_name = '';
                                                                    }
                                                                    if (!_logValidation(i_projectID)) //
                                                                    {
                                                                        i_projectID = '';
                                                                    }
                                                                    
                                                                    
                                                                    
                                                                    var a_project_arr_name_dt = get_project_name(i_project_ID_PE)
                                                                    var a_split_arr_p_a = new Array();
                                                                    var i_project_nm = '';
                                                                    var i_customer_nm = ''
                                                                    
                                                                    if (_logValidation(a_project_arr_name_dt)) //
                                                                    {
                                                                        a_split_arr_p_a = a_project_arr_name_dt[0].split('&&&****&&&')
                                                                        
                                                                        i_project_nm = a_split_arr_p_a[0];
                                                                        i_customer_nm = a_split_arr_p_a[1];
                                                                    }
                                                                    
                                                                    var i_usage_begin_1_1 = i_context.getRemainingUsage();
                                                                    //nlapiLogExecution('DEBUG', 'schedulerFunction',' ---------------------------------- Usage Begin Before Set -->' + i_usage_begin_1_1);
                                                                    //nlapiLogExecution('DEBUG', 'hourly_JE_creation', 'i_project_wise_cost -->' + i_project_wise_cost);
                                                                    o_journalOBJ.selectNewLineItem('line')
                                                                    o_journalOBJ.setCurrentLineItemValue('line', 'custcol_srnumber', parseInt(i) + parseInt(1));
                                                                    o_journalOBJ.setCurrentLineItemValue('line', 'account', i_account_debit);
                                                                    //o_journalOBJ.setCurrentLineItemValue('line', 'account', '616');
                                                                    o_journalOBJ.setCurrentLineItemValue('line', 'debit', i_project_wise_cost);
                                                                    o_journalOBJ.setCurrentLineItemValue('line', 'department', i_practice_PE);
                                                                    o_journalOBJ.setCurrentLineItemValue('line', 'class', i_vertical_PE);
                                                                    
                                                                    o_journalOBJ.setCurrentLineItemValue('line', 'custcol_employee_id', i_employeeID);//custcol_employee
                                                                   // o_journalOBJ.setCurrentLineItemValue('line', 'entity', i_employeeID);//Added by Ashish on 13-07-2021
                                                                    o_journalOBJ.setCurrentLineItemValue('line', 'location', i_location);
                                                                    
                                                                    //o_journalOBJ.setCurrentLineItemValue('line', 'custcol_hour', i_actual_hours_worked_on_project); // i_Number_Of_days
                                                                    o_journalOBJ.setCurrentLineItemValue('line', 'custcol_hour', (parseFloat(i_Number_Of_days) * parseFloat(8))); // 
                                                                    o_journalOBJ.setCurrentLineItemValue('line', 'custcol_billable_rate', i_employee_salary_per_day);
                                                                    //Added default vendor 10th Aug 2021
                                                                    //o_journalOBJ.setCurrentLineItemValue('line', 'entity', 209190);
                                                                    if(_logValidation(vendorList[i_subsidiary])){
                                                                        o_journalOBJ.setCurrentLineItemValue('line', 'entity', vendorList[i_subsidiary]);												
                                                                    }
                                                                    
                                                                    o_journalOBJ.setCurrentLineItemValue('line', 'custcol_itemlistinje', 2222); // Default it will be set to ST service item
                                                                    o_journalOBJ.setCurrentLineItemValue('line', 'memo', 'Salaried Employee Process'); // Default it will be set to ST service item
                                                                    if (i_project_ID_PE == '4407') //
                                                                    {
                                                                    
                                                                    }
                                                                    else //
                                                                    {
                                                                        //nlapiLogExecution('DEBUG','i_project_nm',i_project_nm);
                                                                        o_journalOBJ.setCurrentLineItemValue('line', 'custcol_temp_project', i_project_nm);
                                                                        o_journalOBJ.setCurrentLineItemValue('line', 'custcol_sow_project', i_project_ID_PE);
                                                                        o_journalOBJ.setCurrentLineItemValue('line', 'custcolprj_name', i_project_nm);
                                                                    }
                                                                    o_journalOBJ.setCurrentLineItemValue('line', 'custcolcustcol_temp_customer', i_customer_nm);
                                                                    
                                                                    
                                                                    o_journalOBJ.commitLineItem('line');
                                                                    
                                                                    a_process_array.push(i_recordID);
                                                                    i_emp_nt++;
                                                                    i_m_flag = 1
                                                                    a_salary_upload_monthly_ID_array.push(i_recordID)
                                                                    i_p_flag = 1
                                                                    var i_usage_begin_1_2 = i_context.getRemainingUsage();
                                                                //nlapiLogExecution('DEBUG', 'schedulerFunction',' ---------------------------------- Usage Begin after Set -->' + i_usage_begin_1_2);
                                                                }//Project ID Array
                                                                break;
                                                            }
                                                        }
                                                        
                                                        if (i_emp_nt > 500) //
                                                        {
                                                            break;
                                                        }
                                                        
                                                        
                                                    }//process array if
                                                    employee_cost.push(i_employeeID + '*****&&&' + i_project_wise_total)
                                                    
                                                }//Check
                                                var i_usage_begin_loop = i_context.getRemainingUsage();
                                                //nlapiLogExecution('DEBUG', 'schedulerFunction',' --------------------------------- Usage Begin Loop-->' + i_usage_begin_loop);
                                                //nlapiLogExecution('DEBUG', 'schedulerFunction',' --------------------------------- me-->' + me);
                                                //nlapiLogExecution('DEBUG', 'schedulerFunction',' --------------------------------- me-->' + me);
                                                
                                                
                                                //------------------------------------------------------------------------------------------------
                                                if (me == 250) //if(i_usage_begin_loop<300)//200
                                                {
                                                    new_status = 3;
                                                    i_sch = 1;
                                                    schedule_script_after_usage_exceeded(a_salary_upload_monthly_ID_array, i_CSV_File_ID, i_sal_recordID, i_account_debit, i_account_credit, i_hourly_employee, i_monthly_employee)
                                                    
                                                    
                                                   
                                                    break;
                                                }
                                                //--------------------------------------------------------------------------------------------------------------------------
                                            }
                                        }
                                    }
                                    if (i_sch == 1) //
                                    {
                                        break;
                                    }
                                    
                                }//Emp Loop
                            }//Employee Credit Array
                        }//More than 10 employees
                        //nlapiLogExecution('DEBUG', 'i_project_wise_total', ' ---------------------- i_project_wise_total ----------------------->' + i_project_wise_total);
                        //-------------------------------------------------------------------------------------------
                        if (i_project_wise_total != 0 && i_project_wise_total != 0.0) {
                            o_journalOBJ.selectNewLineItem('line')
                            //o_journalOBJ.setCurrentLineItemValue('line', 'custcol_srnumber', parseInt(i)+parseInt(2));
                            o_journalOBJ.setCurrentLineItemValue('line', 'account', i_account_credit);
                            //o_journalOBJ.setCurrentLineItemValue('line', 'account', '617');
                            //Added default vendor - 
                            // o_journalOBJ.setCurrentLineItemValue('line', 'entity', 209190);
                            if(_logValidation(vendorList[i_subsidiary])){
                                o_journalOBJ.setCurrentLineItemValue('line', 'entity', vendorList[i_subsidiary]);												
                            }
                            o_journalOBJ.setCurrentLineItemValue('line', 'credit', i_project_wise_total);
                            //o_journalOBJ.setCurrentLineItemValue('line', 'custcol_sow_project', i_project_ID_PE);
                            o_journalOBJ.commitLineItem('line');
                        }
                        //--------------------------------------------------------------------------------------
                        if (i_m_flag == 1) {
                            a_salary_upload_monthly_ID_array = removearrayduplicate(a_salary_upload_monthly_ID_array)
                            
                            o_journalOBJ.setFieldValue('custbody_salary_monthly_records', a_salary_upload_monthly_ID_array);
                            o_journalOBJ.setFieldValue('custbody_approvalstatusonje', 2);
                            o_journalOBJ.setFieldValue('approved', 'T');
                            o_journalOBJ.setFieldValue('custbody_approved_by_salary_upload', 'T');
                            
                            //nlapiLogExecution('DEBUG', 'monthly_JE_creation', '*** Execution terminated ***');
                            //return;
                            
                            var i_JE_submitID = nlapiSubmitRecord(o_journalOBJ, true, true);
                            nlapiLogExecution('DEBUG', 'monthly_JE_creation', ' ---------------------- JE Submit ID ----------------------->' + i_JE_submitID);
                            a_JE_array.push(i_JE_submitID);
                            
                            i_m_flag = 0;
                            
                           /* for (var ss = 0; ss < a_salary_upload_monthly_ID_array.length; ss++) {//for start
                                var fields = new Array();
                                var values = new Array();
                                fields[0] = 'custrecord_jv_ref';
                                values[0] = i_JE_submitID;
                                fields[1] = 'custrecord_process_check';
                                values[1] = "T";
                                
                                var updatefields = nlapiSubmitField('customrecord_salary_upload_monthly_file', a_salary_upload_monthly_ID_array[ss], fields, values);
                                
                            }//for close
							*/
                        }
                        //---------------------------------------------------------------------------------------
                    }//Employee Array Length For 2..........c
                }//1.............
            }//Results if close
            var i_usage_begin_2 = i_context.getRemainingUsage();
            //nlapiLogExecution('DEBUG', 'schedulerFunction',' --------------------------------- Usage Begin 2-->' + i_usage_begin_2);
            
            //------------------------------------------------------------------------------------------------------------
            if (_logValidation(i_sal_recordID)) {
                var o_salary_uploadOBJ = nlapiLoadRecord('customrecord_salary_upload_process', i_sal_recordID);
                var jv_val = o_salary_uploadOBJ.getFieldValue('custrecord_journal_entries_created');
                
                if (_logValidation(a_JE_array)) {
                    var s_notes = 'Journal Entries are created for the data entered in CSV File.';
                    
                    o_salary_uploadOBJ.setFieldValue('custrecord_notes', s_notes);
                    
                    o_salary_uploadOBJ.setFieldValue('custrecord_file_status', new_status);
                    
                    //o_salary_uploadOBJ.setFieldValue('custrecord_file_status',5);
                    o_salary_uploadOBJ.setFieldValue('custrecord_is_journal_entry_created', 'T')
                }
                if (!_logValidation(a_JE_array)) {
                    var s_notes = 'Journal Entries are not created for the data entered in CSV File.';
                    
                    if (i_m_flag != 1) //
                    {
                        s_notes = s_notes + ' , No OT hours found for all employees in the file.!';
                    }
                    
                    o_salary_uploadOBJ.setFieldValue('custrecord_file_status', 7);
                    o_salary_uploadOBJ.setFieldValue('custrecord_notes', s_notes);
                    //o_salary_uploadOBJ.setFieldValue('custrecord_is_journal_entry_created','T')
                }
                if (jv_val == null || jv_val == '') {
                    o_salary_uploadOBJ.setFieldValue('custrecord_journal_entries_created', a_JE_array)
                }
                else {
                    a_JE_array[a_JE_array.length] = jv_val;
                    o_salary_uploadOBJ.setFieldValue('custrecord_journal_entries_created', a_JE_array)
                }
                
                var i_submitID = nlapiSubmitRecord(o_salary_uploadOBJ, true, true);
                //nlapiLogExecution('DEBUG', 'hourly_JE_creation', ' *********************** Submit ID ****************** -->'+i_submitID);
            
            }
            //--------------------------------------------------------------------------------------------------------------------------
        
        }//Validations
        var i_usage_begin_1_b = i_context.getRemainingUsage();
        //nlapiLogExecution('DEBUG', 'schedulerFunction',' --------------------------------- Usage Begin Process-->' + i_usage_begin_1_b);
    }//Monthly JE Creation
    
   
    
    
    
    function get_Month(i_month) //
    {
        if (i_month == 'Jan' || i_month == 'January' || i_month == 'JAN' || i_month == 'JANUARY') {
            return '01';
        }
        else 
            if (i_month == 'Feb' || i_month == 'February' || i_month == 'FEB' || i_month == 'FEBRUARY') {
                return '02';
            }
            else 
                if (i_month == 'Mar' || i_month == 'March' || i_month == 'MAR' || i_month == 'MARCH') {
                    return '03';
                }
                else 
                    if (i_month == 'Apr' || i_month == 'April' || i_month == 'APR' || i_month == 'APRIL') {
                        return '04';
                    }
                    else 
                        if (i_month == 'May' || i_month == 'MAY') {
                            return '05';
                        }
                        else 
                            if (i_month == 'June' || i_month == 'Jun' || i_month == 'JUN' || i_month == 'JUNE') {
                                return '06';
                            }
                            else 
                                if (i_month == 'Jul' || i_month == 'July' || i_month == 'JUL' || i_month == 'JULY') {
                                    return '07';
                                }
                                else 
                                    if (i_month == 'August' || i_month == 'Aug' || i_month == 'AUG' || i_month == 'AUGUST') {
                                        return '08';
                                    }
                                    else 
                                        if (i_month == 'Sep' || i_month == 'September' || i_month == 'SEP' || i_month == 'SEPTEMBER') {
                                            return '09';
                                        }
                                        else 
                                            if (i_month == 'Oct' || i_month == 'October' || i_month == 'OCT' || i_month == 'OCTOBER') {
                                                return '10';
                                            }
                                            else 
                                                if (i_month == 'Nov' || i_month == 'November' || i_month == 'NOV' || i_month == 'NOVEMBER') {
                                                    return '11';
                                                }
                                                else 
                                                    if (i_month == 'Dec' || i_month == 'December' || i_month == 'DEC' || i_month == 'DECEMBER') {
                                                        return '12';
                                                    }
    }
    
    
    var a_MP_Data = new Array();
    
    function get_M_P_Data(s_Month_Year, i_Employee_ID) //
    {
        // Get data sent trough parameter
        nlapiLogExecution('DEBUG', 'get_M_P_Data', 's_Month_Year : ' + s_Month_Year);
        nlapiLogExecution('DEBUG', 'get_M_P_Data', 'i_Employee_ID : ' + i_Employee_ID);
        
        // Section added to get month parameter to pass it on for search
        var i_Month = s_Month_Year.split('-')[1];
    
        nlapiLogExecution('DEBUG', 'i_Month', 'i_Month : ' + i_Month);	
    
        i_Month = parseInt(parseFloat(i_Month));
        nlapiLogExecution('DEBUG', 'get_M_P_Data', 'i_Month : ' + i_Month);
        // Section added to get month parameter to pass it on for search
        
        // Create a search for Monthly provision data stored after processing..
        var a_Filter_MP_Data = new Array();
        a_Filter_MP_Data[0] = new nlobjSearchFilter('custrecord_month_of_date', null, 'is', s_Month_Year);
        a_Filter_MP_Data[1] = new nlobjSearchFilter('internalid', 'custrecord_employee_id', 'anyof', i_Employee_ID);
        a_Filter_MP_Data[2] = new nlobjSearchFilter('custrecord_month_val', null, 'anyof', i_Month); // New parameter added 21-Apr-2015
        
        var a_Columns_MP_Data = new Array();
        a_Columns_MP_Data[0] = new nlobjSearchColumn('custrecord_employee_id').setSort();
        a_Columns_MP_Data[1] = new nlobjSearchColumn('custrecord_project_id');
        a_Columns_MP_Data[2] = new nlobjSearchColumn('custrecord_hour');
        a_Columns_MP_Data[3] = new nlobjSearchColumn('custrecord_item_list');
        a_Columns_MP_Data[4] = new nlobjSearchColumn('custrecord_after_per_app_not_sub_hour');
        
        a_Columns_MP_Data[5] = new nlobjSearchColumn('custrecord_practics');
        a_Columns_MP_Data[6] = new nlobjSearchColumn('custrecord_project_vertical');
        
        //a_Columns_MP_Data[5] = new nlobjSearchColumn('custrecord_billed_hour'); //custrecord_billed_hour
        
        // 13-Apr-2015 Billed days fields will be in use after the process update in Monthly provision... 
        a_Columns_MP_Data[7] = new nlobjSearchColumn('custrecord_billed_days'); //custrecord_billed_days
        
        //a_Columns_MP_Data[4] = new nlobjSearchColumn('custrecord_item_list');
        
        
        var s_Search_Rec = searchRecord('customrecord_time_bill_rec', null, a_Filter_MP_Data, a_Columns_MP_Data);
        
        a_MP_Data = s_Search_Rec;
        
        if (_logValidation(s_Search_Rec)) //
        {
            /*
             for (var counter_I = 0; counter_I < s_Search_Rec.length; counter_I++) //
             {
             var temp_STR = s_Search_Rec[counter_I].getValue(a_Columns_MP_Data[0]);
             //nlapiLogExecution('DEBUG', 'get_M_P_Data', 'temp_STR : ' + temp_STR);
             
             a_MP_Data = s_Search_Rec; // setting search result to variable array.
             }
             */
            
            //nlapiLogExecution('DEBUG', 'get_M_P_Data', 'a_MP_Data : ' + a_MP_Data.length);
        }
        else //
        {
            nlapiLogExecution('ERROR', 'get_M_P_Data', 'No data found for "Monthly Provision". For Employee : ' + i_Employee_ID);
        }
        
        // Store the same data into an Array
    }
    
    function get_Hourly_FP_External_Data(s_Month_Year, i_employeeID) //
    {
        var a_Filters = new Array();
        var a_Columns = new Array();
        var s_Search_Result = '';
        
        //a_Filters[0] = new nlobjSearchFilter('custrecord_salary_upload_id_m', null, 'anyof', i_Sal_Upload_ID);
        //a_Filters[1] = new nlobjSearchFilter('custrecord_total_ot_hours', null, 'isnotempty', i_Sal_Upload_ID);
        //a_Filters[2] = new nlobjSearchFilter('custrecord_process_check', null, 'is', 'T');
        
        //a_Columns[0] = new nlobjSearchColumn('name');
        //a_Columns[1] = new nlobjSearchColumn('custrecord_total_ot_hours');
        //a_Columns[2] = new nlobjSearchColumn('custrecord_emp_id_m_s_p_u');
        //a_Columns[3] = new nlobjSearchColumn('custrecord_total_ot_hours');
        
        
        a_Filters[0] = new nlobjSearchFilter('custrecord_sal_ot_data_month_year', null, 'is', s_Month_Year);
        a_Filters[1] = new nlobjSearchFilter('custrecord_emp_id_sal_ot_data', null, 'anyof', i_employeeID);
        //a_Filters[2] = new nlobjSearchFilter('custrecord_sal_ot_data_project', null, 'anyof', i_Project_ID); // 
        a_Filters[2] = new nlobjSearchFilter('custrecord_sal_emp_ot_process_type', null, 'anyof', 3); // 3 means it is External FP Process processed record.
        
        a_Columns[0] = new nlobjSearchColumn('custrecord_sal_upload_rec_id');
        a_Columns[1] = new nlobjSearchColumn('custrecord_emp_id_sal_ot_data');
        a_Columns[2] = new nlobjSearchColumn('custrecord_sal_ot_data_project');
        a_Columns[3] = new nlobjSearchColumn('custrecord_sal_ot_data_ot_hrs');
        a_Columns[4] = new nlobjSearchColumn('custrecord_st_hours');
        a_Columns[5] = new nlobjSearchColumn('custrecord_st_rate');
        
        //s_Search_Result = searchRecord('customrecord_salary_upload_monthly_file', null, a_Filters, a_Columns);
        
        // Searching into Salary upload data for Hourly Diff
        s_Search_Result = searchRecord('customrecord_salary_upload_ot_data', null, a_Filters, a_Columns);
        
        if(_logValidation(s_Search_Result)) //
        {
            if(s_Search_Result.length > 0) //
            {
                return s_Search_Result; //[0].getValue('custrecord_sal_ot_data_ot_hrs');
            }
            else //
            {
                nlapiLogExecution('ERROR', 'get_Salaried_OT_Hours', 'No Data found for Salary Upload in OT.');
                return null;
            }
        }
        else
        {
            nlapiLogExecution('ERROR', 'get_Salaried_OT_Hours', 'No Data found for Salary Upload in OT.');
            return 0;
        }
        
    }
    
    function get_Hourly_FP_Internal_Data(s_Month_Year, i_employeeID) //
    {
        var a_Filters = new Array();
        var a_Columns = new Array();
        var s_Search_Result = '';
        
        //a_Filters[0] = new nlobjSearchFilter('custrecord_salary_upload_id_m', null, 'anyof', i_Sal_Upload_ID);
        //a_Filters[1] = new nlobjSearchFilter('custrecord_total_ot_hours', null, 'isnotempty', i_Sal_Upload_ID);
        //a_Filters[2] = new nlobjSearchFilter('custrecord_process_check', null, 'is', 'T');
        
        //a_Columns[0] = new nlobjSearchColumn('name');
        //a_Columns[1] = new nlobjSearchColumn('custrecord_total_ot_hours');
        //a_Columns[2] = new nlobjSearchColumn('custrecord_emp_id_m_s_p_u');
        //a_Columns[3] = new nlobjSearchColumn('custrecord_total_ot_hours');
        
        
        a_Filters[0] = new nlobjSearchFilter('custrecord_sal_ot_data_month_year', null, 'is', s_Month_Year);
        a_Filters[1] = new nlobjSearchFilter('custrecord_emp_id_sal_ot_data', null, 'anyof', i_employeeID);
        //a_Filters[2] = new nlobjSearchFilter('custrecord_sal_ot_data_project', null, 'anyof', i_Project_ID); // 
        a_Filters[2] = new nlobjSearchFilter('custrecord_sal_emp_ot_process_type', null, 'anyof', 4); // 4 means it is Internal FP Process processed record.
        
        a_Columns[0] = new nlobjSearchColumn('custrecord_sal_upload_rec_id');
        a_Columns[1] = new nlobjSearchColumn('custrecord_emp_id_sal_ot_data');
        a_Columns[2] = new nlobjSearchColumn('custrecord_sal_ot_data_project');
        a_Columns[3] = new nlobjSearchColumn('custrecord_sal_ot_data_ot_hrs');
        a_Columns[4] = new nlobjSearchColumn('custrecord_st_hours');
        a_Columns[5] = new nlobjSearchColumn('custrecord_st_rate');
        
        //s_Search_Result = searchRecord('customrecord_salary_upload_monthly_file', null, a_Filters, a_Columns);
        
        // Searching into Salary upload data for Hourly Diff
        s_Search_Result = searchRecord('customrecord_salary_upload_ot_data', null, a_Filters, a_Columns);
        
        if(_logValidation(s_Search_Result)) //
        {
            if(s_Search_Result.length > 0) //
            {
                return s_Search_Result; //[0].getValue('custrecord_sal_ot_data_ot_hrs');
            }
            else //
            {
                nlapiLogExecution('ERROR', 'get_Salaried_OT_Hours', 'No Data found for Salary Upload in OT.');
                return null;
            }
        }
        else
        {
            nlapiLogExecution('ERROR', 'get_Salaried_OT_Hours', 'No Data found for Salary Upload in OT.');
            return 0;
        }
        
    }
    
    function get_Salaried_OT_Hours(s_Month_Year_Save, i_Employee_ID, i_Project_ID) //
    {
        var a_Filters = new Array();
        var a_Columns = new Array();
        var s_Search_Result = '';
        
        //a_Filters[0] = new nlobjSearchFilter('custrecord_salary_upload_id_m', null, 'anyof', i_Sal_Upload_ID);
        //a_Filters[1] = new nlobjSearchFilter('custrecord_total_ot_hours', null, 'isnotempty', i_Sal_Upload_ID);
        //a_Filters[2] = new nlobjSearchFilter('custrecord_process_check', null, 'is', 'T');
        
        //a_Columns[0] = new nlobjSearchColumn('name');
        //a_Columns[1] = new nlobjSearchColumn('custrecord_total_ot_hours');
        //a_Columns[2] = new nlobjSearchColumn('custrecord_emp_id_m_s_p_u');
        //a_Columns[3] = new nlobjSearchColumn('custrecord_total_ot_hours');
        
        
        a_Filters[0] = new nlobjSearchFilter('custrecord_sal_ot_data_month_year', null, 'is', s_Month_Year_Save);
        a_Filters[1] = new nlobjSearchFilter('custrecord_emp_id_sal_ot_data', null, 'anyof', i_Employee_ID);
        a_Filters[2] = new nlobjSearchFilter('custrecord_sal_ot_data_project', null, 'anyof', i_Project_ID); // 
        a_Filters[3] = new nlobjSearchFilter('custrecord_sal_emp_ot_process_type', null, 'anyof', 1); // 1 means it is OT processed record, 2 is for Diff entry in system
        
        a_Columns[0] = new nlobjSearchColumn('custrecord_sal_upload_rec_id');
        a_Columns[1] = new nlobjSearchColumn('custrecord_emp_id_sal_ot_data');
        a_Columns[2] = new nlobjSearchColumn('custrecord_sal_ot_data_project');
        a_Columns[3] = new nlobjSearchColumn('custrecord_sal_ot_data_ot_hrs');
        
        //s_Search_Result = searchRecord('customrecord_salary_upload_monthly_file', null, a_Filters, a_Columns);
        s_Search_Result = searchRecord('customrecord_salary_upload_ot_data', null, a_Filters, a_Columns);
        
        if(_logValidation(s_Search_Result)) //
        {
            if(s_Search_Result.length > 0) //
            {
                return s_Search_Result[0].getValue('custrecord_sal_ot_data_ot_hrs');
            }
            else //
            {
                nlapiLogExecution('ERROR', 'get_Salaried_OT_Hours', 'No Data found for Salary Upload in OT.');
                return 0;
            }
        }
        else
        {
            nlapiLogExecution('ERROR', 'get_Salaried_OT_Hours', 'No Data found for Salary Upload in OT.');
            return 0;
        }
        
    }
    
    function removearrayduplicate(array)
    {
        var newArray = new Array();
        label: for (var i = 0; i < array.length; i++)
        {
            for (var j = 0; j < array.length; j++)
            {
                if (newArray[j] == array[i])
                    continue label;
            }
            newArray[newArray.length] = array[i];
        }
        return newArray;
    }
    
    function search_hourly_calculation_details(i_employee,d_start_date,d_end_date, s_Param_Search) //
    {
        var i_internal_ID;
        var i_date;
        var i_employee;
        var i_project;
        var i_item;
        var i_duration;
        var i_type;
        var i_rate;
        var i_projectID;
        var i_employeeID;
        var i_practice;
        var i_vertical;
        var i_hours_per_week;
        var a_data_array = new Array();
        var a_project_data_array = new Array();
        var a_project_ID_arr = new Array();
        var i_project_name;
        var i_actual_time=0;
        var i_allocated_time=0;
    
         if (_logValidation(i_employee) && _logValidation(d_start_date) && _logValidation(d_end_date)) //
         {
             var filters = new Array();
             
             filters[0] = new nlobjSearchFilter('employee', null, 'is', i_employee)
             filters[1] = new nlobjSearchFilter('date', null, 'onorafter', d_start_date)
             filters[2] = new nlobjSearchFilter('date', null, 'onorbefore', d_end_date)
             
             // 13-Apr-2015
                // Searching pattern has been changes after the new searches created for each criteria
                
                if (s_Param_Search == 'Hourly') //
                { // Nihal Search has doubt
                    var i_search_results = searchRecord('timebill', 'customsearch_hourly_new_time_2', filters, null);
                }
                
                // Code added on 28-Apr-2015 by Vikrant to Add new search and with refined criteria 
                
                if (s_Param_Search == 'Hourly_Diff') //
                {
                    var i_search_results = searchRecord('timebill', 'customsearch_hourly_new_time_3', filters, null);
                }
                
                // Code added on 28-Apr-2015 by Vikrant to Add new search and with refined criteria 
                
                if (s_Param_Search == 'Internal') //
                {
                    var i_search_results = searchRecord('timebill', 'customsearch3417', filters, null);
                }
                
                if (s_Param_Search == 'External') //
                {
                    var i_search_results = searchRecord('timebill', 'customsearch_hourly_new_time', filters, null);
                }
                
                // Code added by Vikrant on 29-Apr-2015
                // Setting Dummy value in case of no data found for time cards(Not submitted case)
                if (s_Param_Search == 'Hourly' || s_Param_Search == 'Hourly_Diff') //
                {
                    a_project_ID_arr[0] = 'Dummy' + '^^&&&**^^' + '' + '^^&&&**^^' + '';
                }
                
                if (_logValidation(i_search_results)) //
                {
                    //nlapiLogExecution('DEBUG', 'hourly_JE_creation', ' ----------- Hourly Time Search Length --------->' + i_search_results.length);
                     nlapiLogExecution('DEBUG', 'no. of timesheets', i_search_results.length);
                    for (var c = 0; c < i_search_results.length; c++) //
                    {
                        var a_search_transaction_result = i_search_results[c];
                        
                        if (a_search_transaction_result == null || a_search_transaction_result == '' || a_search_transaction_result == undefined) //
                        {
                            break;
                        }
                        var columns = a_search_transaction_result.getAllColumns();
                        
                        var columnLen = columns.length;
                        
    
                        for (var hg = 0; hg < columnLen; hg++) //
                        {
                            var column = columns[hg];
                            var label = column.getLabel();
                            var value = a_search_transaction_result.getValue(column)
                            var text = a_search_transaction_result.getText(column)
                            
                            if (label == 'Internal ID')  //
                            {
                                i_internal_ID = value;
                            }
                            if (label == 'Date') //
                            {
                                i_date = value;
                            }
                            if (label == 'Employee') //
                            {
                                i_employee = value;
                            }
                            if (label == 'Customer') //
                            {
                                i_project = value;
                            }
                            if (label == 'Item') //
                            {
                                i_item = value;
                            }
                            if (label == 'Duration') //
                            {
                                i_duration = value;
                            }
                            if (label == 'Type') //
                            {
                                i_type = value;
                            }
                            if (label == 'Rate') //
                            {
                                i_rate = value;
                            }
                            if (label == 'Project ID') //
                            {
                                i_projectID = value;
                            }
                            if (label == 'Employee ID') //
                            {
                                i_employeeID = value;
                            }
                            if (label == 'Practice')  //
                            {
                                i_practice = value;
                            }
                            if (label == 'Vertical')  //
                            {
                                i_vertical = value;
                            }
                            if (label == 'Hours Per Week')  //
                            {
                                i_hours_per_week = value;
                            }
                            if (label == 'Project Name')  //
                            {
                                i_project_name = value;
                            }
                            //if (label == 'Actual Time') added by SK
                            if (label == 'Duration')  //
                            {
                                i_actual_time = value;
                            }
                            if (label == 'Allocated Time')  //
                            {
                                i_allocated_time = value;
                            }
                            
                        }
                        
                        a_project_ID_arr[c] = i_projectID + '^^&&&**^^' + i_vertical + '^^&&&**^^' + i_practice
                        
                        
                        a_data_array[c] = i_internal_ID + '&&&%%&&&' + i_vertical + '&&&%%&&&' + i_practice + '&&&%%&&&' + i_projectID + '&&&%%&&&' + i_rate + '&&&%%&&&' + i_duration + '&&&%%&&&' + i_item + '&&&%%&&&' + i_hours_per_week + '&&&%%&&&' + i_actual_time + '&&&%%&&&' + i_allocated_time
                        
                    }
                    
                }
                
            }
    
         a_project_data_array[0] = a_project_ID_arr+'^^^^***^^^^'+a_data_array;
         
         nlapiLogExecution('DEBUG','a_project_data_array', JSON.stringify(a_project_data_array));
    
        return a_project_data_array;
    }//Monthly Details
    
    function  search_monthly_calculation_details(i_employee,d_start_date,d_end_date)
    {
        var i_internal_ID;
        var i_date;
        var i_employee;
        var i_project;
        var i_item;
        var i_duration;
        var i_type;
        var i_rate;
        var i_projectID;
        var i_employeeID;
        var i_practice;
        var i_vertical;
        var i_hours_per_week;
        var a_data_array = new Array();
        var a_project_data_array = new Array();
        var a_project_ID_arr = new Array();
        var i_project_name;
    
         if (_logValidation(i_employee) && _logValidation(d_start_date)&& _logValidation(d_end_date))
         {
          var filters = new Array();
          filters[0] = new nlobjSearchFilter('employee', null, 'is', i_employee)
          filters[1] = new nlobjSearchFilter('date', null, 'onorafter', d_start_date)
          filters[2] = new nlobjSearchFilter('date', null, 'onorbefore', d_end_date)
    
             var i_search_results = searchRecord('timebill','customsearch_salaried_new_ti_6',filters, null);
    
            if (_logValidation(i_search_results))
            {
                //nlapiLogExecution('DEBUG', 'monthly_JE_creation', ' ----------- Salaried Time Search Length --------->' + i_search_results.length);
    
            for (var c = 0; c < i_search_results.length; c++)
            {
                var a_search_transaction_result = i_search_results[c];
    
                if (a_search_transaction_result == null || a_search_transaction_result == '' || a_search_transaction_result == undefined)
                {
                    break;
                }
                var columns = a_search_transaction_result.getAllColumns();
    
                var columnLen = columns.length;
    
                for (var hg = 0; hg < columnLen; hg++)
                {
                    var column = columns[hg];
                    var label = column.getLabel();
                    var value = a_search_transaction_result.getValue(column)
                    var text = a_search_transaction_result.getText(column)
    
                    if (label == 'Internal ID')
                    {
                        i_internal_ID = value;
                    }
                    if (label == 'Date')
                    {
                        i_date = value;
                    }
                    if (label == 'Employee')
                    {
                        i_employee = value;
                    }
                    if (label == 'Customer')
                    {
                        i_project = value;
                    }
                    if (label == 'Item')
                    {
                        i_item = value;
                    }
                    if (label == 'Duration')
                    {
                        i_duration = value;
                    }
                    if (label == 'Type')
                    {
                        i_type = value;
                    }
                    if (label == 'Rate')
                    {
                        i_rate = value;
                    }
                    if (label == 'Project ID')
                    {
                        i_projectID = value;
                    }
                    if (label == 'Employee ID')
                    {
                        i_employeeID = value;
                    }
                    if (label == 'Practice')
                    {
                        i_practice = value;
                    }
                    if (label == 'Vertical')
                    {
                        i_vertical = value;
                    }
                    if (label == 'Hours Per Week')
                    {
                        i_hours_per_week = value;
                    }
                    if (label == 'Allocated Hours')
                    {
                        i_allocated_hours = value;
                    }
                    if (label == 'Project Name')
                    {
                        i_project_name = value;
                    }
    
                }
    
            a_project_ID_arr[c]  = i_projectID +'^^&&&**^^'+i_vertical+'^^&&&**^^'+i_practice
    
            a_data_array[c] = i_internal_ID+'&&&%%&&&'+i_vertical+'&&&%%&&&'+i_practice+'&&&%%&&&'+i_projectID+'&&&%%&&&'+i_rate+'&&&%%&&&'+i_duration+'&&&%%&&&'+i_item+'&&&%%&&&'+i_hours_per_week+'&&&%%&&&'+i_allocated_hours
    
            }
    
            }
    
         }
    
         a_project_data_array[0] = a_project_ID_arr+'^^^^***^^^^'+a_data_array;
    
        return a_project_data_array;
    }//Monthly Details
    
    function search_monthly_calculation_details_OT(i_employee, d_start_date, d_end_date) //
    {
        var i_internal_ID;
        var i_date;
        var i_employee;
        var i_project;
        var i_item;
        var i_duration;
        var i_type;
        var i_rate;
        var i_projectID;
        var i_employeeID;
        var i_practice;
        var i_vertical;
        var i_hours_per_week;
        var a_data_array = new Array();
        var a_project_data_array = new Array();
        var a_project_ID_arr = new Array();
        var i_project_name;
        
        if (_logValidation(i_employee) && _logValidation(d_start_date) && _logValidation(d_end_date)) //
        {
            var filters = new Array();
            filters[0] = new nlobjSearchFilter('employee', null, 'is', i_employee)
            filters[1] = new nlobjSearchFilter('date', null, 'onorafter', d_start_date)
            filters[2] = new nlobjSearchFilter('date', null, 'onorbefore', d_end_date)
            
            nlapiLogExecution('DEBUG', 'search_monthly_calculation_details_OT', 'i_employee : '+ i_employee);
            nlapiLogExecution('DEBUG', 'search_monthly_calculation_details_OT', 'd_start_date : '+ d_start_date);
            nlapiLogExecution('DEBUG', 'search_monthly_calculation_details_OT', 'd_end_date : '+ d_end_date);
            
            var i_search_results = searchRecord('timebill', 'customsearch_salaried_ot_hou', filters, null);
            
            if (_logValidation(i_search_results)) //
            {
                nlapiLogExecution('DEBUG', 'search_monthly_calculation_details_OT', 'i_search_results.length : ' + i_search_results.length);
                
                for (var c = 0; c < i_search_results.length; c++) //
                {
                    var a_search_transaction_result = i_search_results[c];
                    
                    if (a_search_transaction_result == null || a_search_transaction_result == '' || a_search_transaction_result == undefined) //
                    {
                        break;
                    }
                    var columns = a_search_transaction_result.getAllColumns();
                    
                    var columnLen = columns.length;
                    
                    for (var hg = 0; hg < columnLen; hg++) //
                    {
                        var column = columns[hg];
                        var label = column.getLabel();
                        var value = a_search_transaction_result.getValue(column)
                        var text = a_search_transaction_result.getText(column)
                        
                        if (label == 'Internal ID') //
                        {
                            i_internal_ID = value;
                        }
                        if (label == 'Date') //
                        {
                            i_date = value;
                        }
                        if (label == 'Employee') //
                        {
                            i_employee = value;
                        }
                        if (label == 'Customer') //
                        {
                            i_project = value;
                        }
                        if (label == 'Item') //
                        {
                            i_item = value;
                        }
                        if (label == 'Duration') //
                        {
                            i_duration = value;
                        }
                        if (label == 'Type') //
                        {
                            i_type = value;
                        }
                        if (label == 'Rate') //
                        {
                            i_rate = value;
                        }
                        if (label == 'Project ID') //
                        {
                            i_projectID = value;
                        }
                        if (label == 'Employee ID') //
                        {
                            i_employeeID = value;
                        }
                        if (label == 'Practice') //
                        {
                            i_practice = value;
                        }
                        if (label == 'Vertical') //
                        {
                            i_vertical = value;
                        }
                        if (label == 'Hours Per Week') //
                        {
                            i_hours_per_week = value;
                        }
                        if (label == 'Allocated Hours') //
                        {
                            i_allocated_hours = value;
                        }
                        if (label == 'Project Name') //
                        {
                            i_project_name = value;
                        }
                    }
                    
                    a_project_ID_arr[c] = i_projectID + '^^&&&**^^' + i_vertical + '^^&&&**^^' + i_practice
                    
                    a_data_array[c] = i_internal_ID + '&&&%%&&&' + i_vertical + '&&&%%&&&' + i_practice + '&&&%%&&&' + i_projectID + '&&&%%&&&' + i_rate + '&&&%%&&&' + i_duration + '&&&%%&&&' + i_item + '&&&%%&&&' + i_hours_per_week + '&&&%%&&&' + i_allocated_hours
                }
            }
            else //
            {			
                nlapiLogExecution('ERROR', 'search_monthly_calculation_details_OT', 'No data found for Time sheets of OT hours...');
                return null;
            }
        }
        
        a_project_data_array[0] = a_project_ID_arr + '^^^^***^^^^' + a_data_array;
        
        return a_project_data_array;
    }	//Monthly Details OT
    
    function get_month_details(i_month,i_year)
    {
     var d_start_date = '';
     var d_end_date = '';
    
     var date_format = checkDateFormat();
    
     var i_day;
    
     if (_logValidation(i_month) && _logValidation(i_year))
     {
           if(i_month == 'Jan' || i_month == 'January' || i_month == 'JAN' || i_month == 'JANUARY')
          {
             if (date_format == 'YYYY-MM-DD')
             {
                 d_start_date = i_year + '-' + 1 + '-' + 1;
                d_end_date = i_year + '-' + 1 + '-' + 31;
             }
             if (date_format == 'DD/MM/YYYY')
             {
                  d_start_date = 1 + '/' + 1 + '/' + i_year;
                d_end_date = 31 + '/' + 1 + '/' + i_year;
             }
             if (date_format == 'MM/DD/YYYY')
             {
                  d_start_date = 1 + '/' + 1 + '/' + i_year;
                d_end_date = 1 + '/' + 31 + '/' + i_year;
             }
    
          }
          else if(i_month == 'Feb' || i_month == 'February' || i_month == 'FEB' || i_month == 'FEBRUARY')
          {
              if(parseInt(i_year) % parseInt (4) == 0)
            {
                i_day = 29;
            }
            else
            {
                i_day = 28;
            }
    
             if (date_format == 'YYYY-MM-DD')
             {
                 d_start_date = i_year + '-' + 2 + '-' + 1;
                d_end_date = i_year + '-' + 2 + '-' + i_day;
             }
             if (date_format == 'DD/MM/YYYY')
             {
                  d_start_date = 1 + '/' + 2 + '/' + i_year;
                d_end_date = i_day + '/' + 2 + '/' + i_year;
             }
             if (date_format == 'MM/DD/YYYY')
             {
                  d_start_date = 2 + '/' + 1 + '/' + i_year;
                d_end_date = 2 + '/' + i_day + '/' + i_year;
             }
    
          }
          else if(i_month == 'Mar' || i_month == 'March' || i_month == 'MAR' || i_month == 'MARCH')
          {
             if (date_format == 'YYYY-MM-DD')
             {
                 d_start_date = i_year + '-' + 3 + '-' + 1;
                d_end_date = i_year + '-' + 3 + '-' + 31;
             }
             if (date_format == 'DD/MM/YYYY')
             {
                  d_start_date = 1 + '/' + 3 + '/' + i_year;
                d_end_date = 31 + '/' + 3 + '/' + i_year;
             }
             if (date_format == 'MM/DD/YYYY')
             {
                  d_start_date = 3 + '/' + 1 + '/' + i_year;
                d_end_date = 3 + '/' + 31 + '/' + i_year;
             }
    
          }
          else if(i_month == 'Apr' || i_month == 'April' || i_month == 'APR' || i_month == 'APRIL')
          {
              if (date_format == 'YYYY-MM-DD')
             {
                 d_start_date = i_year + '-' + 4 + '-' + 1;
                d_end_date = i_year + '-' + 4 + '-' + 30;
             }
             if (date_format == 'DD/MM/YYYY')
             {
                  d_start_date = 1 + '/' + 4 + '/' + i_year;
                d_end_date = 30 + '/' + 4 + '/' + i_year;
             }
             if (date_format == 'MM/DD/YYYY')
             {
                  d_start_date = 4 + '/' + 1 + '/' + i_year;
                d_end_date = 4 + '/' + 30 + '/' + i_year;
             }
    
    
          }
          else if(i_month == 'May' || i_month == 'MAY')
          {
             if (date_format == 'YYYY-MM-DD')
             {
                 d_start_date = i_year + '-' + 5 + '-' + 1;
                d_end_date = i_year + '-' + 5 + '-' + 31;
             }
             if (date_format == 'DD/MM/YYYY')
             {
                  d_start_date = 1 + '/' + 5 + '/' + i_year;
                d_end_date = 31 + '/' + 5 + '/' + i_year;
             }
             if (date_format == 'MM/DD/YYYY')
             {
                  d_start_date = 5 + '/' + 1 + '/' + i_year;
                d_end_date = 5 + '/' + 31 + '/' + i_year;
             }
    
          }
          else if(i_month == 'June' || i_month == 'Jun' || i_month == 'JUN' || i_month == 'JUNE')
          {
             if (date_format == 'YYYY-MM-DD')
             {
                 d_start_date = i_year + '-' + 6 + '-' + 1;
                d_end_date = i_year + '-' + 6 + '-' + 30;
             }
             if (date_format == 'DD/MM/YYYY')
             {
                  d_start_date = 1 + '/' + 6 + '/' + i_year;
                d_end_date = 30 + '/' + 6 + '/' + i_year;
             }
             if (date_format == 'MM/DD/YYYY')
             {
                  d_start_date = 6 + '/' + 1 + '/' + i_year;
                d_end_date = 6 + '/' + 30 + '/' + i_year;
             }
    
          }
          else if(i_month == 'Jul' || i_month == 'July' || i_month == 'JUL' || i_month == 'JULY')
          {
             if (date_format == 'YYYY-MM-DD')
             {
                 d_start_date = i_year + '-' + 7 + '-' + 1;
                d_end_date = i_year + '-' + 7 + '-' + 31;
             }
             if (date_format == 'DD/MM/YYYY')
             {
                  d_start_date = 1 + '/' + 7 + '/' + i_year;
                d_end_date = 31 + '/' + 7 + '/' + i_year;
             }
             if (date_format == 'MM/DD/YYYY')
             {
                  d_start_date = 7 + '/' + 1 + '/' + i_year;
                d_end_date = 7 + '/' + 31 + '/' + i_year;
             }
    
          }
          else if(i_month == 'August' || i_month == 'Aug' || i_month == 'AUG' || i_month == 'AUGUST')
          {
             if (date_format == 'YYYY-MM-DD')
             {
                 d_start_date = i_year + '-' + 8 + '-' + 1;
                d_end_date = i_year + '-' + 8 + '-' + 31;
             }
             if (date_format == 'DD/MM/YYYY')
             {
                  d_start_date = 1 + '/' + 8 + '/' + i_year;
                d_end_date = 31 + '/' + 8 + '/' + i_year;
             }
             if (date_format == 'MM/DD/YYYY')
             {
                  d_start_date = 8 + '/' + 1 + '/' + i_year;
                d_end_date = 8 + '/' + 31 + '/' + i_year;
             }
    
          }
          else if(i_month == 'Sep' || i_month == 'September' || i_month == 'SEP' || i_month == 'SEPTEMBER')
          {
             if (date_format == 'YYYY-MM-DD')
             {
                 d_start_date = i_year + '-' + 9 + '-' + 1;
                d_end_date = i_year + '-' + 9 + '-' + 30;
             }
             if (date_format == 'DD/MM/YYYY')
             {
                  d_start_date = 1 + '/' + 9 + '/' + i_year;
                d_end_date = 30 + '/' + 9 + '/' + i_year;
             }
             if (date_format == 'MM/DD/YYYY')
             {
                  d_start_date = 9 + '/' + 1 + '/' + i_year;
                d_end_date = 9 + '/' + 30 + '/' + i_year;
             }
    
          }
          else if(i_month == 'Oct' || i_month == 'October' || i_month == 'OCT' || i_month == 'OCTOBER')
          {
            if (date_format == 'YYYY-MM-DD')
             {
                 d_start_date = i_year + '-' + 10 + '-' + 1;
                d_end_date = i_year + '-' + 10 + '-' + 31;
             }
             if (date_format == 'DD/MM/YYYY')
             {
                  d_start_date = 1 + '/' + 10 + '/' + i_year;
                d_end_date = 31 + '/' + 10 + '/' + i_year;
             }
             if (date_format == 'MM/DD/YYYY')
             {
                  d_start_date = 10 + '/' + 1 + '/' + i_year;
                d_end_date = 10 + '/' + 31 + '/' + i_year;
             }
    
    
          }
          else if(i_month == 'Nov' || i_month == 'November' || i_month == 'NOV' || i_month == 'NOVEMBER')
          {
            if (date_format == 'YYYY-MM-DD')
             {
                 d_start_date = i_year + '-' + 11 + '-' + 1;
                d_end_date = i_year + '-' + 11 + '-' + 30;
             }
             if (date_format == 'DD/MM/YYYY')
             {
                  d_start_date = 1 + '/' + 11 + '/' + i_year;
                d_end_date = 30 + '/' + 11 + '/' + i_year;
             }
             if (date_format == 'MM/DD/YYYY')
             {
                  d_start_date = 11 + '/' + 1 + '/' + i_year;
                d_end_date = 11 + '/' + 30 + '/' + i_year;
             }
    
          }
          else if(i_month == 'Dec' || i_month == 'December' || i_month == 'DEC' || i_month == 'DECEMBER')
          {
            if (date_format == 'YYYY-MM-DD')
             {
                 d_start_date = i_year + '-' + 12 + '-' + 1;
                d_end_date = i_year + '-' + 12 + '-' + 31;
             }
             if (date_format == 'DD/MM/YYYY')
             {
                  d_start_date = 1 + '/' + 12 + '/' + i_year;
                d_end_date = 31 + '/' + 12 + '/' + i_year;
             }
             if (date_format == 'MM/DD/YYYY')
             {
                  d_start_date = 12 + '/' + 1 + '/' + i_year;
                d_end_date = 12 + '/' + 31 + '/' + i_year;
             }
    
          }
     }//Month & Year
    }
    
    function get_no_saturdays(i_month,i_year)
    {
    var i_no_of_sat = 0	;
    var i_no_of_sun = 0	;
    
    var d = new Date();
    var getTot = daysInMonth(i_month,i_year); //Get total days in a month
    var sat = new Array();   //Declaring array for inserting Saturdays
    var sun = new Array();   //Declaring array for inserting Sundays
    for(var i=1;i<=getTot;i++)
    {    //looping through days in month
        var newDate = new Date(i_month,i_year,i)
        if(newDate.getDay()==0)
        {   //if Sunday
            sat.push(i);
            i_no_of_sat++;
        }
        if(newDate.getDay()==6)
        {   //if Saturday
            sun.push(i);
            i_no_of_sun++;
        }
    
    }
     var i_total_days_sat_sun = parseInt(i_no_of_sat)+parseInt(i_no_of_sun);
     return i_total_days_sat_sun ;
    }
    
    function daysInMonth(month,year)
    {
        return new Date(year, month, 0).getDate();
    }
    
    function removearrayduplicate(array)
    {
        var newArray = new Array();
        label: for (var i = 0; i < array.length; i++)
        {
            for (var j = 0; j < array.length; j++)
            {
                if (newArray[j] == array[i])
                    continue label;
            }
            newArray[newArray.length] = array[i];
        }
        return newArray;
    }
    
    function get_todays_date()
    {
        var today;
        // ============================= Todays Date ==========================================
    
          var date1 = new Date();
          //nlapiLogExecution('DEBUG', 'schedulerFunction', 'Date ==' + date1);
    
         var offsetIST = 5.5;
    
         var day = date1.getDate();
         //nlapiLogExecution('DEBUG', 'schedulerFunction', 'Day ==' + day);
    
         var month = date1.getMonth()+1;
         //nlapiLogExecution('DEBUG', 'schedulerFunction', 'Month ==' + month);
    
         var year_sd =date1.getYear();
         //nlapiLogExecution('DEBUG', 'schedulerFunction', 'Year SD==' +year_sd);
    
         var year=date1.getFullYear();
         //nlapiLogExecution('DEBUG', 'schedulerFunction', 'Year ==' +year);
    
         var date_format = checkDateFormat();
         //nlapiLogExecution('DEBUG', 'schedulerFunction', 'Date Format ==' +date_format);
    
         if (date_format == 'YYYY-MM-DD')
         {
             today = year + '-' + month + '-' + day;
         }
         if (date_format == 'DD/MM/YYYY')
         {
              today = day + '/' + month + '/' + year;
         }
         if (date_format == 'MM/DD/YYYY')
         {
              today = month + '/' + day + '/' + year;
         }
         return today;
    }
    
    function checkDateFormat()
    {
        var context = nlapiGetContext();
    
        var dateFormatPref = context.getPreference('dateformat');
    
        return dateFormatPref;
    }
    
    function daysInMonth(month,year)
    {
        return new Date(year, month, 0).getDate();
    }
    
    function getWeekend(startDate,endDate)
    {
    var i_no_of_sat = 0	;
    var i_no_of_sun = 0	;
    
    
     var date_format = checkDateFormat();
    
    var startDate_1 = startDate
     var endDate_1 = endDate
    
     startDate = nlapiStringToDate(startDate);
     endDate = nlapiStringToDate(endDate);
    
     var i_count_day = startDate.getDate();
    
     var i_count_last_day = endDate.getDate();
    
     i_month = startDate.getMonth()+1;
    
     i_year = startDate.getFullYear();
    
    var d_f = new Date();
    var getTot = getDatediffIndays(startDate_1, endDate_1); //Get total days in a month
    
    var sat = new Array();   //Declaring array for inserting Saturdays
    var sun = new Array();   //Declaring array for inserting Sundays
    
    for(var i=i_count_day;i<=i_count_last_day;i++)
    {    //looping through days in month
    
         if (date_format == 'YYYY-MM-DD')
         {
             var newDate = i_year + '-' + i_month + '-' + i;
         }
         if (date_format == 'DD/MM/YYYY')
         {
              var newDate = i + '/' + i_month + '/' + i_year;
         }
         if (date_format == 'MM/DD/YYYY')
         {
              var newDate = i_month + '/' + i + '/' + i_year;
         }
    
        newDate = nlapiStringToDate(newDate);
    
        if(newDate.getDay()==0)
        {   //if Sunday
            sat.push(i);
            i_no_of_sat++;
        }
        if(newDate.getDay()==6)
        {   //if Saturday
            sun.push(i);
            i_no_of_sun++;
        }
    
    
    }
    
     var i_total_days_sat_sun = parseInt(i_no_of_sat)+parseInt(i_no_of_sun);
     return i_total_days_sat_sun ;
    }
    
    function getDatediffIndays(startDate, endDate)
    {
        var one_day=1000*60*60*24;
        var fromDate = startDate;
        var toDate=endDate
    
        var date1 = nlapiStringToDate(fromDate);
    
        var date2 = nlapiStringToDate(toDate);
    
        var date3=Math.round((date2-date1)/one_day);
    
        return (date3+1);
    }
    
    function get_project_name(i_project)
    {
        nlapiLogExecution('DEBUG', 'get_project_name', 'i_project : '+i_project);
        
        var i_project_name = '';
        var i_project_ID= '';
        var i_project_nms = '';
        var i_customer = '';
        var return_arr = new Array();
    
        if (_logValidation(i_project))
        {
          var o_projectOBJ = nlapiLoadRecord('job',i_project)
    
          if (_logValidation(o_projectOBJ))
          {
              i_project_nms = o_projectOBJ.getFieldValue('companyname')
            i_project_ID = o_projectOBJ.getFieldValue('entityid')
            i_project_name = i_project_ID+' '+i_project_nms;
    
            i_customer =  o_projectOBJ.getFieldText('parent');
          }//Project OBJ
        }//Project
    
        return_arr[0] = i_project_name+'&&&****&&&'+i_customer;
        
        //nlapiLogExecution('DEBUG', 'get_project_name', 'return_arr : '+return_arr);
        
        return return_arr;
    }
    
    function get_current_month_start_date(i_month,i_year)
    {
       var d_start_date = '';
       var d_end_date = '';
    
       var date_format = checkDateFormat();
       //nlapiLogExecution('DEBUG','get_current_month_start_date+i_month',i_month);
        //nlapiLogExecution('DEBUG','get_current_month_start_date+i_year',i_year);
    
     if (_logValidation(i_month) && _logValidation(i_year))
     {
           if(i_month == 'Jan' || i_month == 'January' || i_month == 'JAN' || i_month == 'JANUARY')
          {
             if (date_format == 'YYYY-MM-DD')
             {
                 d_start_date = i_year + '-' + 1 + '-' + 1;
                d_end_date = i_year + '-' + 1 + '-' + 31;
             }
             if (date_format == 'DD/MM/YYYY')
             {
                  d_start_date = 1 + '/' + 1 + '/' + i_year;
                d_end_date = 31 + '/' + 1 + '/' + i_year;
             }
             if (date_format == 'MM/DD/YYYY')
             {
                  d_start_date = 1 + '/' + 1 + '/' + i_year;
                d_end_date = 1 + '/' + 31 + '/' + i_year;
             }
    
          }
          else if(i_month == 'Feb' || i_month == 'February' || i_month == 'FEB' || i_month == 'FEBRUARY')
          {
              if(parseInt(i_year) % parseInt (4) == 0)
            {
                i_day = 29;
            }
            else
            {
                i_day = 28;
            }
    
             if (date_format == 'YYYY-MM-DD')
             {
                 d_start_date = i_year + '-' + 2 + '-' + 1;
                d_end_date = i_year + '-' + 2 + '-' + i_day;
             }
             if (date_format == 'DD/MM/YYYY')
             {
                  d_start_date = 1 + '/' + 2 + '/' + i_year;
                d_end_date = i_day + '/' + 2 + '/' + i_year;
             }
             if (date_format == 'MM/DD/YYYY')
             {
                  d_start_date = 2 + '/' + 1 + '/' + i_year;
                d_end_date = 2 + '/' + i_day + '/' + i_year;
             }
    
          }
          else if(i_month == 'Mar' || i_month == 'March' || i_month == 'MAR' || i_month == 'MARCH')
          {
             if (date_format == 'YYYY-MM-DD')
             {
                 d_start_date = i_year + '-' + 3 + '-' + 1;
                d_end_date = i_year + '-' + 3 + '-' + 31;
             }
             if (date_format == 'DD/MM/YYYY')
             {
                  d_start_date = 1 + '/' + 3 + '/' + i_year;
                d_end_date = 31 + '/' + 3 + '/' + i_year;
             }
             if (date_format == 'MM/DD/YYYY')
             {
                  d_start_date = 3 + '/' + 1 + '/' + i_year;
                d_end_date = 3 + '/' + 31 + '/' + i_year;
             }
    
          }
          else if(i_month == 'Apr' || i_month == 'April' || i_month == 'APR' || i_month == 'APRIL')
          {
              if (date_format == 'YYYY-MM-DD')
             {
                 d_start_date = i_year + '-' + 4 + '-' + 1;
                d_end_date = i_year + '-' + 4 + '-' + 30;
             }
             if (date_format == 'DD/MM/YYYY')
             {
                  d_start_date = 1 + '/' + 4 + '/' + i_year;
                d_end_date = 30 + '/' + 4 + '/' + i_year;
             }
             if (date_format == 'MM/DD/YYYY')
             {
                  d_start_date = 4 + '/' + 1 + '/' + i_year;
                d_end_date = 4 + '/' + 30 + '/' + i_year;
             }
    
    
          }
          else if(i_month == 'May' || i_month == 'MAY')
          {
             if (date_format == 'YYYY-MM-DD')
             {
                 d_start_date = i_year + '-' + 5 + '-' + 1;
                d_end_date = i_year + '-' + 5 + '-' + 31;
             }
             if (date_format == 'DD/MM/YYYY')
             {
                  d_start_date = 1 + '/' + 5 + '/' + i_year;
                d_end_date = 31 + '/' + 5 + '/' + i_year;
             }
             if (date_format == 'MM/DD/YYYY')
             {
                  d_start_date = 5 + '/' + 1 + '/' + i_year;
                d_end_date = 5 + '/' + 31 + '/' + i_year;
             }
    
          }
          else if(i_month == 'June' || i_month == 'Jun' || i_month == 'JUN' || i_month == 'JUNE')
          {
             if (date_format == 'YYYY-MM-DD')
             {
                 d_start_date = i_year + '-' + 6 + '-' + 1;
                d_end_date = i_year + '-' + 6 + '-' + 30;
             }
             if (date_format == 'DD/MM/YYYY')
             {
                  d_start_date = 1 + '/' + 6 + '/' + i_year;
                d_end_date = 30 + '/' + 6 + '/' + i_year;
             }
             if (date_format == 'MM/DD/YYYY')
             {
                  d_start_date = 6 + '/' + 1 + '/' + i_year;
                d_end_date = 6 + '/' + 30 + '/' + i_year;
             }
    
          }
          else if(i_month == 'Jul' || i_month == 'July' || i_month == 'JUL' || i_month == 'JULY')
          {
             if (date_format == 'YYYY-MM-DD')
             {
                 d_start_date = i_year + '-' + 7 + '-' + 1;
                d_end_date = i_year + '-' + 7 + '-' + 31;
             }
             if (date_format == 'DD/MM/YYYY')
             {
                  d_start_date = 1 + '/' + 7 + '/' + i_year;
                d_end_date = 31 + '/' + 7 + '/' + i_year;
             }
             if (date_format == 'MM/DD/YYYY')
             {
                  d_start_date = 7 + '/' + 1 + '/' + i_year;
                d_end_date = 7 + '/' + 31 + '/' + i_year;
             }
    
          }
          else if(i_month == 'August' || i_month == 'Aug' || i_month == 'AUG' || i_month == 'AUGUST')
          {
             if (date_format == 'YYYY-MM-DD')
             {
                 d_start_date = i_year + '-' + 8 + '-' + 1;
                d_end_date = i_year + '-' + 8 + '-' + 31;
             }
             if (date_format == 'DD/MM/YYYY')
             {
                  d_start_date = 1 + '/' + 8 + '/' + i_year;
                d_end_date = 31 + '/' + 8 + '/' + i_year;
             }
             if (date_format == 'MM/DD/YYYY')
             {
                  d_start_date = 8 + '/' + 1 + '/' + i_year;
                d_end_date = 8 + '/' + 31 + '/' + i_year;
             }
    
          }
          else if(i_month == 'Sep' || i_month == 'September' || i_month == 'SEP' || i_month == 'SEPTEMBER')
          {
             if (date_format == 'YYYY-MM-DD')
             {
                 d_start_date = i_year + '-' + 9 + '-' + 1;
                d_end_date = i_year + '-' + 9 + '-' + 30;
             }
             if (date_format == 'DD/MM/YYYY')
             {
                  d_start_date = 1 + '/' + 9 + '/' + i_year;
                d_end_date = 30 + '/' + 9 + '/' + i_year;
             }
             if (date_format == 'MM/DD/YYYY')
             {
                  d_start_date = 9 + '/' + 1 + '/' + i_year;
                d_end_date = 9 + '/' + 30 + '/' + i_year;
             }
    
          }
          else if(i_month == 'Oct' || i_month == 'October' || i_month == 'OCT' || i_month == 'OCTOBER')
          {
            if (date_format == 'YYYY-MM-DD')
             {
                 d_start_date = i_year + '-' + 10 + '-' + 1;
                d_end_date = i_year + '-' + 10 + '-' + 31;
             }
             if (date_format == 'DD/MM/YYYY')
             {
                  d_start_date = 1 + '/' + 10 + '/' + i_year;
                d_end_date = 31 + '/' + 10 + '/' + i_year;
             }
             if (date_format == 'MM/DD/YYYY')
             {
                  d_start_date = 10 + '/' + 1 + '/' + i_year;
                d_end_date = 10 + '/' + 31 + '/' + i_year;
             }
          }
          else if(i_month == 'Nov' || i_month == 'November' || i_month == 'NOV' || i_month == 'NOVEMBER')
          {
             //nlapiLogExecution('DEBUG','nov')
            if (date_format == 'YYYY-MM-DD')
             {
                 d_start_date = i_year + '-' + 11 + '-' + 1;
                d_end_date = i_year + '-' + 11 + '-' + 30;
             }
             if (date_format == 'DD/MM/YYYY')
             {
                  d_start_date = 1 + '/' + 11 + '/' + i_year;
                d_end_date = 30 + '/' + 11 + '/' + i_year;
             }
             if (date_format == 'MM/DD/YYYY')
             {
                  d_start_date = 11 + '/' + 1 + '/' + i_year;
                d_end_date = 11 + '/' + 30 + '/' + i_year;
             }
    
          }
          else if(i_month == 'Dec' || i_month == 'December' || i_month == 'DEC' || i_month == 'DECEMBER')
          {
            if (date_format == 'YYYY-MM-DD')
             {
                 d_start_date = i_year + '-' + 12 + '-' + 1;
                d_end_date = i_year + '-' + 12 + '-' + 31;
             }
             if (date_format == 'DD/MM/YYYY')
             {
                  d_start_date = 1 + '/' + 12 + '/' + i_year;
                d_end_date = 31 + '/' + 12 + '/' + i_year;
             }
             if (date_format == 'MM/DD/YYYY')
             {
                  d_start_date = 12 + '/' + 1 + '/' + i_year;
                d_end_date = 12 + '/' + 31 + '/' + i_year;
             }
    
          }
     }//Month & Year
     
           //nlapiLogExecution('DEBUG','inside function d_start_date',d_start_date);
     return d_start_date;
    }
    
    function get_current_month_end_date(i_month,i_year)
    {
        var d_start_date = '';
        var d_end_date = '';
    
        var date_format = checkDateFormat();
    
     if (_logValidation(i_month) && _logValidation(i_year))
     {
           if(i_month == 'Jan' || i_month == 'January' || i_month == 'JAN' || i_month == 'JANUARY')
          {
             if (date_format == 'YYYY-MM-DD')
             {
                 d_start_date = i_year + '-' + 1 + '-' + 1;
                d_end_date = i_year + '-' + 1 + '-' + 31;
             }
             if (date_format == 'DD/MM/YYYY')
             {
                  d_start_date = 1 + '/' + 1 + '/' + i_year;
                d_end_date = 31 + '/' + 1 + '/' + i_year;
             }
             if (date_format == 'MM/DD/YYYY')
             {
                  d_start_date = 1 + '/' + 1 + '/' + i_year;
                d_end_date = 1 + '/' + 31 + '/' + i_year;
             }
    
          }
          else if(i_month == 'Feb' || i_month == 'February' || i_month == 'FEB' || i_month == 'FEBRUARY')
          {
              if(parseInt(i_year) % parseInt (4) == 0)
            {
                i_day = 29;
            }
            else
            {
                i_day = 28;
            }
    
             if (date_format == 'YYYY-MM-DD')
             {
                 d_start_date = i_year + '-' + 2 + '-' + 1;
                d_end_date = i_year + '-' + 2 + '-' + i_day;
             }
             if (date_format == 'DD/MM/YYYY')
             {
                  d_start_date = 1 + '/' + 2 + '/' + i_year;
                d_end_date = i_day + '/' + 2 + '/' + i_year;
             }
             if (date_format == 'MM/DD/YYYY')
             {
                  d_start_date = 2 + '/' + 1 + '/' + i_year;
                d_end_date = 2 + '/' + i_day + '/' + i_year;
             }
    
          }
          else if(i_month == 'Mar' || i_month == 'March' || i_month == 'MAR' || i_month == 'MARCH')
          {
             if (date_format == 'YYYY-MM-DD')
             {
                 d_start_date = i_year + '-' + 3 + '-' + 1;
                d_end_date = i_year + '-' + 3 + '-' + 31;
             }
             if (date_format == 'DD/MM/YYYY')
             {
                  d_start_date = 1 + '/' + 3 + '/' + i_year;
                d_end_date = 31 + '/' + 3 + '/' + i_year;
             }
             if (date_format == 'MM/DD/YYYY')
             {
                  d_start_date = 3 + '/' + 1 + '/' + i_year;
                d_end_date = 3 + '/' + 31 + '/' + i_year;
             }
    
          }
          else if(i_month == 'Apr' || i_month == 'April' || i_month == 'APR' || i_month == 'APRIL')
          {
              if (date_format == 'YYYY-MM-DD')
             {
                 d_start_date = i_year + '-' + 4 + '-' + 1;
                d_end_date = i_year + '-' + 4 + '-' + 30;
             }
             if (date_format == 'DD/MM/YYYY')
             {
                  d_start_date = 1 + '/' + 4 + '/' + i_year;
                d_end_date = 30 + '/' + 4 + '/' + i_year;
             }
             if (date_format == 'MM/DD/YYYY')
             {
                  d_start_date = 4 + '/' + 1 + '/' + i_year;
                d_end_date = 4 + '/' + 30 + '/' + i_year;
             }
    
    
          }
          else if(i_month == 'May' || i_month == 'MAY')
          {
             if (date_format == 'YYYY-MM-DD')
             {
                 d_start_date = i_year + '-' + 5 + '-' + 1;
                d_end_date = i_year + '-' + 5 + '-' + 31;
             }
             if (date_format == 'DD/MM/YYYY')
             {
                  d_start_date = 1 + '/' + 5 + '/' + i_year;
                d_end_date = 31 + '/' + 5 + '/' + i_year;
             }
             if (date_format == 'MM/DD/YYYY')
             {
                  d_start_date = 5 + '/' + 1 + '/' + i_year;
                d_end_date = 5 + '/' + 31 + '/' + i_year;
             }
    
          }
          else if(i_month == 'June' || i_month == 'Jun' || i_month == 'JUN' || i_month == 'JUNE')
          {
             if (date_format == 'YYYY-MM-DD')
             {
                 d_start_date = i_year + '-' + 6 + '-' + 1;
                d_end_date = i_year + '-' + 6 + '-' + 30;
             }
             if (date_format == 'DD/MM/YYYY')
             {
                  d_start_date = 1 + '/' + 6 + '/' + i_year;
                d_end_date = 30 + '/' + 6 + '/' + i_year;
             }
             if (date_format == 'MM/DD/YYYY')
             {
                  d_start_date = 6 + '/' + 1 + '/' + i_year;
                d_end_date = 6 + '/' + 30 + '/' + i_year;
             }
    
          }
          else if(i_month == 'Jul' || i_month == 'July' || i_month == 'JUL' || i_month == 'JULY')
          {
             if (date_format == 'YYYY-MM-DD')
             {
                 d_start_date = i_year + '-' + 7 + '-' + 1;
                d_end_date = i_year + '-' + 7 + '-' + 31;
             }
             if (date_format == 'DD/MM/YYYY')
             {
                  d_start_date = 1 + '/' + 7 + '/' + i_year;
                d_end_date = 31 + '/' + 7 + '/' + i_year;
             }
             if (date_format == 'MM/DD/YYYY')
             {
                  d_start_date = 7 + '/' + 1 + '/' + i_year;
                d_end_date = 7 + '/' + 31 + '/' + i_year;
             }
    
          }
          else if(i_month == 'August' || i_month == 'Aug' || i_month == 'AUG' || i_month == 'AUGUST')
          {
             if (date_format == 'YYYY-MM-DD')
             {
                 d_start_date = i_year + '-' + 8 + '-' + 1;
                d_end_date = i_year + '-' + 8 + '-' + 31;
             }
             if (date_format == 'DD/MM/YYYY')
             {
                  d_start_date = 1 + '/' + 8 + '/' + i_year;
                d_end_date = 31 + '/' + 8 + '/' + i_year;
             }
             if (date_format == 'MM/DD/YYYY')
             {
                  d_start_date = 8 + '/' + 1 + '/' + i_year;
                d_end_date = 8 + '/' + 31 + '/' + i_year;
             }
    
          }
          else if(i_month == 'Sep' || i_month == 'September' || i_month == 'SEP' || i_month == 'SEPTEMBER')
          {
             if (date_format == 'YYYY-MM-DD')
             {
                 d_start_date = i_year + '-' + 9 + '-' + 1;
                d_end_date = i_year + '-' + 9 + '-' + 30;
             }
             if (date_format == 'DD/MM/YYYY')
             {
                  d_start_date = 1 + '/' + 9 + '/' + i_year;
                d_end_date = 30 + '/' + 9 + '/' + i_year;
             }
             if (date_format == 'MM/DD/YYYY')
             {
                  d_start_date = 9 + '/' + 1 + '/' + i_year;
                d_end_date = 9 + '/' + 30 + '/' + i_year;
             }
    
          }
          else if(i_month == 'Oct' || i_month == 'October' || i_month == 'OCT' || i_month == 'OCTOBER')
          {
            if (date_format == 'YYYY-MM-DD')
             {
                 d_start_date = i_year + '-' + 10 + '-' + 1;
                d_end_date = i_year + '-' + 10 + '-' + 31;
             }
             if (date_format == 'DD/MM/YYYY')
             {
                  d_start_date = 1 + '/' + 10 + '/' + i_year;
                d_end_date = 31 + '/' + 10 + '/' + i_year;
             }
             if (date_format == 'MM/DD/YYYY')
             {
                  d_start_date = 10 + '/' + 1 + '/' + i_year;
                d_end_date = 10 + '/' + 31 + '/' + i_year;
             }
    
    
          }
          else if(i_month == 'Nov' || i_month == 'November' || i_month == 'NOV' || i_month == 'NOVEMBER')
          {
            if (date_format == 'YYYY-MM-DD')
             {
                 d_start_date = i_year + '-' + 11 + '-' + 1;
                d_end_date = i_year + '-' + 11 + '-' + 30;
             }
             if (date_format == 'DD/MM/YYYY')
             {
                  d_start_date = 1 + '/' + 11 + '/' + i_year;
                d_end_date = 30 + '/' + 11 + '/' + i_year;
             }
             if (date_format == 'MM/DD/YYYY')
             {
                  d_start_date = 11 + '/' + 1 + '/' + i_year;
                d_end_date = 11 + '/' + 30 + '/' + i_year;
             }
    
          }
          else if(i_month == 'Dec' || i_month == 'December' || i_month == 'DEC' || i_month == 'DECEMBER')
          {
            if (date_format == 'YYYY-MM-DD')
             {
                 d_start_date = i_year + '-' + 12 + '-' + 1;
                d_end_date = i_year + '-' + 12 + '-' + 31;
             }
             if (date_format == 'DD/MM/YYYY')
             {
                  d_start_date = 1 + '/' + 12 + '/' + i_year;
                d_end_date = 31 + '/' + 12 + '/' + i_year;
             }
             if (date_format == 'MM/DD/YYYY')
             {
                  d_start_date = 12 + '/' + 1 + '/' + i_year;
                d_end_date = 12 + '/' + 31 + '/' + i_year;
             }
    
          }
     }//Month & Year
     return d_end_date;
    }
    
    function schedule_script_after_usage_exceeded(a_salary_upload_monthly_ID_array,i_CSV_File_ID,i_recordID,i_account_debit,i_account_credit,i_hourly_employee,i_monthly_employee)
    {
          //nlapiLogExecution('DEBUG','schedule_script_after_usage_exceeded','a_salary_upload_monthly_ID_array -->'+ a_salary_upload_monthly_ID_array.length);
    
    
        ////Define all parameters to schedule the script for voucher generation.
         var params=new Array();
         params['status']='scheduled';
          params['runasadmin']='T';
         params['custscript_record_id_array']=a_salary_upload_monthly_ID_array.toString();
         params['custscript_csv_file_id_je']=i_CSV_File_ID;
         params['custscript_monthly_employee_je']=i_monthly_employee;
         params['custscript_hourly_employee_je']=i_hourly_employee;
         params['custscript_account_debit_je']=i_account_debit;
         params['custscript_account_credit_je']=i_account_credit;
         params['custscript_record_id_je']=i_recordID;
    
         var startDate = new Date();
          params['startdate']=startDate.toUTCString();
    
         var status=nlapiScheduleScript(nlapiGetContext().getScriptId(), nlapiGetContext().getDeploymentId(),params);
         //nlapiLogExecution('DEBUG','After Scheduling','Script Scheduled Status -->'+ status);
    
         ////If script is scheduled then successfuly then check for if status=queued
         if (status == 'QUEUED')
          {
            //nlapiLogExecution('DEBUG', ' SCHEDULED', ' Script Is Re - Scheduled for -->'+a_salary_upload_monthly_ID_array);
          }
    }//fun close
    // END FUNCTION =====================================================
	
function searchRecord(recordType, savedSearch, arrFilters, arrColumns,
    filterExpression) {
    try {
        var search = null;
        // if a saved search is provided, load it and add the filters and
        // columns
        if (isNotEmpty(savedSearch)) {
            search = nlapiLoadSearch(recordType, savedSearch);
            if (isArrayNotEmpty(arrFilters)) {
                search.addFilters(arrFilters);
            }
            if (isArrayNotEmpty(arrColumns)) {
                search.addColumns(arrColumns);
            }
            if (isArrayNotEmpty(filterExpression)) {
                search.setFilterExpression(filterExpression);
            }
        }
        // create a new search
        else {
            search = nlapiCreateSearch(recordType, arrFilters, arrColumns);
        }
        // run search
        var resultSet = search.runSearch();
        // iterate through the search and get all data 1000 at a time
        var searchResultCount = 0;
        var resultSlice = null;
        var searchResult = [];
        do {
            resultSlice = resultSet.getResults(searchResultCount,
                searchResultCount + 1000);
            if (resultSlice) {
                resultSlice.forEach(function(result) {
                    searchResult.push(result);
                    searchResultCount++;
                });
            }
        } while (isArrayNotEmpty(resultSlice) && resultSlice.length >= 1000);
        return searchResult;
    } catch (err) {
        nlapiLogExecution('ERROR', 'searchRecord', err);
        throw err;
    }
}
function isEmpty(value) {
    return value == null || value == "" || typeof(value) == undefined;
}
function isNotEmpty(value) {
    return !isEmpty(value);
}
function isArrayEmpty(argArray) {
    return !isArrayNotEmpty(argArray);
}
function isArrayNotEmpty(argArray) {
    return (isNotEmpty(argArray) && argArray.length > 0);
}