/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       18 Dec 2018     Aazamali Khan	   Update the FRF details on fulfillment dashboard DB record.
 *
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType
 * 
 * @param {String} type Operation types: create, edit, delete, xedit,
 *                      approve, cancel, reject (SO, ER, Time Bill, PO & RMA only)
 *                      pack, ship (IF only)
 *                      dropship, specialorder, orderitems (PO only) 
 *                      paybills (vendor payments)
 * @returns {Void}
 */
function GetRRFDetails(type){
	try {
		var fl_externalHire;
		var frfDetailsObj;
		nlapiLogExecution("DEBUG","Type : ",type);
		if (type == 'create') {
			frfDetailsObj = nlapiLoadRecord(nlapiGetRecordType(), nlapiGetRecordId());
			var fl_cancelled =  frfDetailsObj.getFieldValue('custrecord_frf_details_status');
			var i_oppId = frfDetailsObj.getFieldValue('custrecord_frf_details_opp_id');
			if (i_oppId) {
				nlapiLogExecution("DEBUG","oppId : ",i_oppId);
				var o_dashboard_db = GetFulfillmentID(i_oppId); // Get the fulfillment dashboard data records
				var i_dashboard_db = o_dashboard_db[0].getId();
				nlapiLogExecution("DEBUG","i_dashboard_db : ",i_dashboard_db);
				if (fl_cancelled != 'T') {
					fl_externalHire = frfDetailsObj.getFieldValue('custrecord_frf_details_external_hire');
					if (fl_externalHire == 'T') {
						var objDash = nlapiLoadRecord('customrecord_fulfillment_dashboard_data', i_dashboard_db);
						var s_number = objDash.getFieldValue('custrecord_fulfill_dashboard_rrf');
						objDash.setFieldValue('custrecord_fulfill_dashboard_rrf', parseInt(s_number)+1);
						var id = nlapiSubmitRecord(objDash);
						nlapiLogExecution('DEBUG', 'will delete log after testing - ID of DB record created', id);
					}else{
						var objDash = nlapiLoadRecord('customrecord_fulfillment_dashboard_data', i_dashboard_db);
						var s_number = objDash.getFieldValue('custrecord_fulfill_dashboard_frf');
						objDash.setFieldValue('custrecord_fulfill_dashboard_frf', parseInt(s_number)+1);
						var id = nlapiSubmitRecord(objDash);
						nlapiLogExecution('DEBUG', 'will delete later ID of DB record updated', id);
					}
				}
			}else{
				var i_project = frfDetailsObj.getFieldValue('custrecord_frf_details_project');// for augmentation
				nlapiLogExecution("DEBUG", "Else :i_project ", i_project);
				var n_teamSize = GetTeamSize(i_project);
				var n_peopleExit = GetEmployeeExist(i_project);
				nlapiLogExecution('DEBUG', 'TeamSize:'+n_teamSize, 'People Exist : '+n_peopleExit);
				var searchResultDash = GetFulfillmentProjectID(i_project);
				if (searchResultDash != null) {
					for (var int = 0; int < searchResultDash.length; int++) {
						var i_dashboard_db = searchResultDash[0].getId();
						var objDash = nlapiLoadRecord('customrecord_fulfillment_dashboard_data', i_dashboard_db);
						if (fl_cancelled != 'T') {
							fl_externalHire = frfDetailsObj.getFieldValue('custrecord_frf_details_external_hire');
							if (fl_externalHire == 'T') {
								var s_number = objDash.getFieldValue('custrecord_fulfill_dashboard_rrf');
								objDash.setFieldValue('custrecord_fulfill_dashboard_rrf', parseInt(s_number)+1);
							}else{
								var s_number = objDash.getFieldValue('custrecord_fulfill_dashboard_frf');
								objDash.setFieldValue('custrecord_fulfill_dashboard_frf', parseInt(s_number)+1);

							}
							objDash.setFieldValue('custrecord_fulfill_dashboard_pro_team', n_teamSize);
							objDash.setFieldValue('custrecord_fulfill_dashboard_exiting', n_peopleExit);
							var id = nlapiSubmitRecord(objDash);
						}
					}
				}else{
					// TODO logic for creating  
					var fulfillmentDashBoardObj = nlapiCreateRecord('customrecord_fulfillment_dashboard_data');
					fulfillmentDashBoardObj.setFieldValue('custrecord_frf_details_project', i_project);
					fulfillmentDashBoardObj.setFieldValue('custrecord_fulfill_dashboard_pro_team', n_teamSize);
					fulfillmentDashBoardObj.setFieldValue('custrecord_fulfill_dashboard_exiting', n_peopleExit);
					fulfillmentDashBoardObj.setFieldValue('custrecord_fulfill_dashboard_practice', nlapiLookupField('job', i_project, 'custentity_practice'));
					fulfillmentDashBoardObj.setFieldValue('custrecord_fulfill_dashboard_manager', GetManagerName(nlapiLookupField('job', i_project, 'custentity_projectmanager',true)));
					fulfillmentDashBoardObj.setFieldValue('custrecord_fulfill_dashboard_start_date', nlapiLookupField('job', i_project, 'startdate'));
					fl_externalHire = frfDetailsObj.getFieldValue('custrecord_frf_details_external_hire');
					if (fl_externalHire == 'T') {
						var s_number = objDash.getFieldValue('custrecord_fulfill_dashboard_rrf');
						fulfillmentDashBoardObj.setFieldValue('custrecord_fulfill_dashboard_rrf', parseInt(s_number)+1);
					}else{
						var s_number = objDash.getFieldValue('custrecord_fulfill_dashboard_frf');
						fulfillmentDashBoardObj.setFieldValue('custrecord_fulfill_dashboard_frf', parseInt(s_number)+1);

					}
					var id = nlapiSubmitRecord(fulfillmentDashBoardObj);
				}
			}
		}
		if (type == 'edit') {
			var newRec = nlapiLoadRecord(nlapiGetRecordType(), nlapiGetRecordId());
			var oldRec = nlapiGetOldRecord();
			var i_oppId = newRec.getFieldValue('custrecord_frf_details_opp_id');
			if (i_oppId) {
				var o_dashboard_db = GetFulfillmentID(i_oppId);
				var i_dashboard_db = o_dashboard_db[0].getId();
				var objDash = nlapiLoadRecord('customrecord_fulfillment_dashboard_data', i_dashboard_db);
				var n_frf = parseInt(objDash.getFieldValue("custrecord_fulfill_dashboard_frf"));
				var n_rrf = parseInt(objDash.getFieldValue("custrecord_fulfill_dashboard_rrf"));
				if (newRec.getFieldValue("custrecord_frf_details_external_hire") != oldRec.getFieldValue("custrecord_frf_details_external_hire") ) {
					if (oldRec.getFieldValue("custrecord_frf_details_external_hire") == 'F') {
						n_rrf = n_rrf + 1 ;
						if(n_frf >  0 ){
							n_frf = n_frf - 1 ;
						}
					}/*else{
						n_frf = n_frf + 1 ; 
					}*/
					if (oldRec.getFieldValue("custrecord_frf_details_external_hire") == 'T'){
						n_frf = n_frf + 1 ;
						if (n_rrf > 0) {
							n_rrf = n_rrf - 1;
						}
					}/*else{
						if (n_rrf != 0) {
							n_rrf = n_rrf - 1;
						}
					}*/
				}
				if (newRec.getFieldValue("custrecord_frf_details_status") != oldRec.getFieldValue("custrecord_frf_details_status") ) {
					if ((oldRec.getFieldValue("custrecord_frf_details_status") == 'F' )&&(newRec.getFieldValue("custrecord_frf_details_external_hire") == 'T' )) {
						if(n_rrf >  0 ){
							n_rrf = n_rrf - 1 ;
						} 
					}else{
						if(n_frf >  0 ){
							n_frf = n_frf - 1 ;
						}
					}
				}
				nlapiLogExecution('AUDIT','n_frf after processing : ',n_frf);
				objDash.setFieldValue('custrecord_fulfill_dashboard_frf', n_frf);
				objDash.setFieldValue('custrecord_fulfill_dashboard_rrf', n_rrf);
				var idRet = nlapiSubmitRecord(objDash);
				nlapiLogExecution("AUDIT", "edit processed", idRet);
			}else{
              	var newRec = nlapiLoadRecord(nlapiGetRecordType(), nlapiGetRecordId());
				var oldRec = nlapiGetOldRecord();
				var i_projectId = newRec.getFieldValue('custrecord_frf_details_project');
				var o_dashboard_db = GetFulfillmentProjectID(i_projectId);
				var i_dashboard_db = o_dashboard_db[0].getId();
				var objDash = nlapiLoadRecord('customrecord_fulfillment_dashboard_data', i_dashboard_db);
				var n_frf = parseInt(objDash.getFieldValue("custrecord_fulfill_dashboard_frf"));
				var n_rrf = parseInt(objDash.getFieldValue("custrecord_fulfill_dashboard_rrf"));
				if (newRec.getFieldValue("custrecord_frf_details_external_hire") != oldRec.getFieldValue("custrecord_frf_details_external_hire") ) {
					if (oldRec.getFieldValue("custrecord_frf_details_external_hire") == 'F') {
						n_rrf = n_rrf + 1 ; 
						if(n_frf >  0 ){
							n_frf = n_frf - 1 ;
						}
					}/*else{
						n_frf = n_frf + 1 ; 
					}*/
					if (oldRec.getFieldValue("custrecord_frf_details_external_hire") == 'T'){
						n_frf = n_frf + 1;
						if (n_rrf > 0) {
							n_rrf = n_rrf - 1;
						}
					}/*else{
						if (n_rrf != 0) {
							n_rrf = n_rrf - 1;
						}
					}*/
				}
				if (newRec.getFieldValue("custrecord_frf_details_status") != oldRec.getFieldValue("custrecord_frf_details_status") ) {
					if ((oldRec.getFieldValue("custrecord_frf_details_status") == 'F' )&&(newRec.getFieldValue("custrecord_frf_details_external_hire") == 'T' )) {
						if(n_rrf > 0 ){
							n_rrf = n_rrf - 1 ;
						} 
					}else{
						if(n_frf >  0 ){
							n_frf = n_frf - 1 ;
						}
					}
				}
				objDash.setFieldValue('custrecord_fulfill_dashboard_frf', n_frf);
				objDash.setFieldValue('custrecord_fulfill_dashboard_rrf', n_rrf);
				var idRet = nlapiSubmitRecord(objDash);
				nlapiLogExecution("AUDIT", "else edit processed", idRet);
			}
		}
	}
	catch (e) {
		// TODO: handle exception
		nlapiLogExecution('DEBUG', 'GETRRFDetails ', e);
	}
}
function GetFulfillmentID(i_fulfillmentDB) {
	var customrecord_fulfillment_dashboard_dataSearch = nlapiSearchRecord("customrecord_fulfillment_dashboard_data",null,
			[
			 ["custrecord_fulfill_dashboard_opp_id","anyof",i_fulfillmentDB]
			 ], 
			 [

			  ]
	);
	return customrecord_fulfillment_dashboard_dataSearch;
}
function GetFulfillmentIDForProject(i_fulfillmentDB) {
	var customrecord_fulfillment_dashboard_dataSearch = nlapiSearchRecord("customrecord_fulfillment_dashboard_data",null,
			[
			 ["custrecord_fulfill_dashboard_project","anyof",i_fulfillmentDB]
			 ], 
			 [

			  ]
	);
	return customrecord_fulfillment_dashboard_dataSearch;
}
function GetFulfillmentProjectID(i_fulfillmentDB) {
	var customrecord_fulfillment_dashboard_dataSearch = nlapiSearchRecord("customrecord_fulfillment_dashboard_data",null,
			[
			 ["custrecord_fulfill_dashboard_project","anyof",i_fulfillmentDB]
			 ], 
			 [
			  ]
	);
	return customrecord_fulfillment_dashboard_dataSearch;
}
function _logValidation(value){
	if (value != null && value != '' && value != undefined && value != 'NaN') {
		return true;
	}
	else {
		return false;
	}
}
function GetTeamSize(i_project) {
	var jobSearch = nlapiSearchRecord("job",null,
			[
			 ["internalid","anyof",i_project],
			 "AND", 
			 ["resourceallocation.startdate","notafter","today"], 
			 "AND", 
			 ["resourceallocation.enddate","notbefore","today"]
			 ], 
			 [
			  new nlobjSearchColumn("resource","resourceAllocation",null)
			  ]
	);
	if(jobSearch){
		var resourceCheck = jobSearch[0].getValue("resource","resourceAllocation",null);
		if (_logValidation(resourceCheck)) {
			return jobSearch.length;
		}
	}else{
		return "0";
	}

}
function GetEmployeeExist(i_project) {
	Date.prototype.addDays = function(days) {
		var date = new Date(this.valueOf());
		date.setDate(date.getDate() + days);
		return date;
	}
	var date = new Date();
	var nextDate = date.addDays(30);
	nlapiLogExecution("AUDIT", "exiting date", date);
	var resourceallocationSearch = nlapiSearchRecord("resourceallocation",null,
			[
			 ["job.internalid","anyof",i_project], //5515 project internal ID
			 "AND", 
			 ["employee.custentity_lwd","within",date,nextDate] // 01/05/2019 calculated dynamically using current date and adding 30 days to it. 
			 ], 
			 [
			  new nlobjSearchColumn("resource",null,'GROUP')
			  ]
	);
	if (resourceallocationSearch) {
		return resourceallocationSearch.length;
	}else{
		return "0";
	}
}
function GetManagerName(tempString) {
	var s_manager = "";
	//var tempString = searchResult[int].getText("custentity_projectmanager","custrecord_project_internal_id_sfdc",null);
	temp = tempString.indexOf("-");
	if(temp>0)
	{
		var s_manager = tempString.split("-")[1];
	}
	else{
		var s_manager = tempString;
	}
	return s_manager;
}