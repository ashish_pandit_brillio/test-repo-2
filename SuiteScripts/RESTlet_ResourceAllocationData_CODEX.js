function postRESTlet(dataIn)
{
	
	var response = new Response();
	try 
	{	nlapiLogExecution('debug', 'dataIn', JSON.stringify(dataIn));
		var current_date = nlapiDateToString(new Date());
		nlapiLogExecution('debug', 'current_date', current_date);
		
		var requestType = dataIn.RequestType;
		switch (requestType) {

		case M_Constants.Request.Get :
		
		response.Data = getActiveResourceAllocation();
		response.Status = true;
		break;
		}
     nlapiLogExecution('debug', 'dataIn', JSON.stringify(response));
		return response;
	}
	
	catch(error)
	{
		nlapiLogExecution('debug','postRESTlet',error);
		response.Data = error;
		response.status = false;
		return response;
	}
}

function getActiveResourceAllocation()
{ 
	try
	{
		var JSON = {};
		var datarows = [];
		var resourceallocationSearch = searchRecord("resourceallocation",null,
	[
		["startdate","notonorafter","today"], 
		"AND", 
		["enddate","notonorbefore","today"]
	], 
	[
	new nlobjSearchColumn("company"), 
	new nlobjSearchColumn("custentity_fusion_empid","employee",null), 
	new nlobjSearchColumn("firstname","employee",null), 
	new nlobjSearchColumn("middlename","employee",null), 
	new nlobjSearchColumn("lastname","employee",null), 
	new nlobjSearchColumn("email","employee",null), 
	new nlobjSearchColumn("custeventwlocation"),
	new nlobjSearchColumn("department","employee",null), 
	new nlobjSearchColumn("custevent4"), 
	new nlobjSearchColumn("customer"), 
	new nlobjSearchColumn("subsidiary","employee",null), 
	new nlobjSearchColumn("employeetype","employee",null), 
	new nlobjSearchColumn("startdate"), 
	new nlobjSearchColumn("enddate")
	]
	);
	
	if(resourceallocationSearch)
	{
		var len = resourceallocationSearch.length;
		for(var i = 0; i < len; i++)
		{
			var full_name = '';
			var first_name = resourceallocationSearch[i].getValue('firstname','employee');
			if(_logValidation(first_name))
			{
				full_name = full_name + first_name;
			}
			var middle_name = resourceallocationSearch[i].getValue('middlename','employee');
			if(_logValidation(middle_name))
			{
				full_name = full_name + ' ' + middle_name;
			}
			var last_name = resourceallocationSearch[i].getValue('lastname','employee');
			if(_logValidation(last_name))
			{
				full_name = full_name + ' ' + last_name;
			}
			
			var JSON = {
				Project : resourceallocationSearch[i].getText('company'),
				EmployeeID : resourceallocationSearch[i].getValue('custentity_fusion_empid','employee'),
				Name : full_name,
				Email : resourceallocationSearch[i].getValue('email','employee'),
				Department : resourceallocationSearch[i].getText('department','employee'),
				WorkLocation : resourceallocationSearch[i].getText('custeventwlocation'),
				OnsiteOffsite : resourceallocationSearch[i].getText('custevent4'),
				Customer : resourceallocationSearch[i].getText('customer'),
				Subsidary : resourceallocationSearch[i].getText('subsidiary','employee'),
				EmployeeType : resourceallocationSearch[i].getText('employeetype','employee'),
				AllocationStartDate : resourceallocationSearch[i].getValue('startdate'),
				AllocationEndDate : resourceallocationSearch[i].getValue('enddate')
			};
			datarows.push(JSON);
		}
	}
		//nlapiLogExecution('debug','Response',JSON.stringify(datarows));
		return datarows;
	
	}
	catch(e) 
	{
		nlapiLogExecution('error','Datarows',e);
		return e;
	}

	
}

function _logValidation(value) {
    if (value != null && value != '' && value != undefined && value.toString() != undefined && value.toString() != 'NaN' && value != NaN && value != 'undefined' && value != ' ') {
        return true;
    } else {
        return false;
    }
}