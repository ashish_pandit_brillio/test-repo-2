/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       05 May 2016     amol.sahijwani
 *
 */

/**
 * @param {nlobjRequest} request Request object
 * @param {nlobjResponse} response Response object
 * @returns {Void} Any output is written via response object
 */
function suitelet(request, response){

	var context	=	nlapiGetContext();	
	
	var form	 =	nlapiCreateForm('Payroll Diff Report');
	var st_date	 =	form.addField('custpage_from_date', 'date', 'Start Date');
	var end_date =	form.addField('custpage_end_date', 'date', 'End Date');
	var created_date = '';
	
	form.addSubmitButton('Run');
	
	var s_run_st_date	=	null;
	
	var s_run_end_date	=	null;
	
	var mode = '';
	
	if(request.getMethod() == 'POST')
		{
		
			var logRecord	=	nlapiCreateRecord('customrecord_payroll_ts_diff_rep_run_log');
			logRecord.setFieldValue('custrecord_pdrl_run_by', context.getUser());
			var log_record_id	=	nlapiSubmitRecord(logRecord);
			
			s_run_st_date	=	request.getParameter('custpage_from_date');
			
			//s_run_end_date	=	request.getParameter('custpage_end_date');
			
			s_run_end_date = nlapiLookupField('customrecord_payroll_ts_diff_rep_run_log', log_record_id, 'created');
			
			//select.setDefaultValue(s_last_run_date);
			
			var link	=	form.addField('exporttoexcellink', 'url', "")
			
			var s_url	=	'https://' + request.getHeader('Host') + nlapiResolveURL('SUITELET', context.getScriptId(), context.getDeploymentId()) + '&mode=excel&from_date=' + encodeURI(s_run_st_date) + '&to_date=' + encodeURI(s_run_end_date)
			
			link.setDisplayType('inline').setLinkText('Export To Excel').setDefaultValue(s_url);
		
			mode	=	'display_report';
		}
	else
		{

			if(request.getParameter('mode') == 'excel')
				{
					s_run_st_date	=	decodeURI(request.getParameter('from_date'));
					
					s_run_end_date	=	decodeURI(request.getParameter('to_date'));
				    
					mode	=	'export_to_excel';
				}
		}
	
	if(mode == 'display_report' || mode == 'export_to_excel')
		{
			// Get Changed TimeSheet Records
			var filters	=	new Array();
			filters[0]	=	new nlobjSearchFilter('created', null,'within', s_run_st_date, s_run_end_date);
			filters[1]	=	new nlobjSearchFilter('custentity_persontype', 'custrecord_ptdr_employee', 'anyof', 2);
			filters[2]	=	new nlobjSearchFilter('subsidiary', 'custrecord_ptdr_employee', 'anyof', 2);
			
			var columns	=	new Array();
			columns[0]	=	new nlobjSearchColumn('custrecord_ptdr_employee').setSort();
			columns[1]	=	new nlobjSearchColumn('custrecord_ptdr_weekending').setSort();
			columns[2]	=	new nlobjSearchColumn('created').setSort();
			columns[3]	=	new nlobjSearchColumn('custrecord_ptdr_old_st_hours');
			columns[4]	=	new nlobjSearchColumn('custrecord_ptdr_new_st_hours');
			columns[5]	=	new nlobjSearchColumn('custrecord_ptdr_old_ot_hours');
			columns[6]	=	new nlobjSearchColumn('custrecord_ptdr_new_ot_hours');
			columns[7]	=	new nlobjSearchColumn('custrecord_ptdr_old_leave_hours');
			columns[8]	=	new nlobjSearchColumn('custrecord_ptdr_new_leave_hours');
			columns[9]	=	new nlobjSearchColumn('custrecord_ptdr_old_holiday_hours');
			columns[10]	=	new nlobjSearchColumn('custrecord_ptdr_new_holiday_hours');
			columns[11]	=	new nlobjSearchColumn('employeetype', 'custrecord_ptdr_employee');
			columns[12]	=	new nlobjSearchColumn('custrecord_ptdr_updated_by');
			columns[13] = 	new nlobjSearchColumn('custrecord_ptdr_action');
			
			var search_results	=	searchRecord('customrecord_payroll_ts_diff_rep_data', null, filters, columns);
			
			//nlapiLogExecution('AUDIT', 'Test' + search_results.length, s_last_run_date);
			
			var o_data	=	new Object();
			
			for(var i = 0; search_results != null && i < search_results.length; i++)
				{
					var i_employee_id	=	search_results[i].getValue('custrecord_ptdr_employee');
					var s_employee		=	search_results[i].getText('custrecord_ptdr_employee');
					var s_weekending	=	search_results[i].getValue('custrecord_ptdr_weekending');
					var f_old_st_hours	=	search_results[i].getValue('custrecord_ptdr_old_st_hours');
					var f_new_st_hours	=	search_results[i].getValue('custrecord_ptdr_new_st_hours');
					var f_old_ot_hours	=	search_results[i].getValue('custrecord_ptdr_old_ot_hours');
					var f_new_ot_hours	=	search_results[i].getValue('custrecord_ptdr_new_ot_hours');
					var f_old_leave_hours	=	search_results[i].getValue('custrecord_ptdr_old_leave_hours');
					var f_new_leave_hours	=	search_results[i].getValue('custrecord_ptdr_new_leave_hours');
					var f_old_holiday_hours	=	search_results[i].getValue('custrecord_ptdr_old_holiday_hours');
					var f_new_holiday_hours	=	search_results[i].getValue('custrecord_ptdr_new_holiday_hours');
					var s_employee_type	=	search_results[i].getText('employeetype', 'custrecord_ptdr_employee');
					var d_created		=	nlapiStringToDate(search_results[i].getValue('created'), 'datetimetz');
					var event_type = search_results[i].getValue('custrecord_ptdr_action');
					var s_updated_by	=	search_results[i].getText('custrecord_ptdr_updated_by');
					if(!_logValidation(f_new_leave_hours))
					{
							  f_new_leave_hours	 = 0;
					}
					if(!_logValidation(f_old_leave_hours))
					{
							  f_old_leave_hours	 = 0;
					}
					if(!_logValidation(f_old_holiday_hours))
					{
							  f_old_holiday_hours	 = 0;
					}
					if(!_logValidation(f_new_holiday_hours))
					{
							  f_new_holiday_hours	 = 0;
					}
					
					if(o_data[i_employee_id] == undefined)
						{
							o_data[i_employee_id]	=	new Object();
						}
					
					if(o_data[i_employee_id][s_weekending] == undefined)
						{
							o_data[i_employee_id][s_weekending]	=	{'Employee': s_employee, 'Old_ST_Hours': f_old_st_hours, 'New_ST_Hours': f_new_st_hours, 'Old_OT_Hours': f_old_ot_hours, 'New_OT_Hours': f_new_ot_hours, 'Old_Leave_Hours': f_old_leave_hours, 'New_Leave_Hours': f_new_leave_hours, 'Old_Holiday_Hours': f_old_holiday_hours, 'New_Holiday_Hours': f_new_holiday_hours, 'date_created': d_created, 'employee_type': s_employee_type,'last_modified': d_created, 'last_modified_by': s_updated_by, 'type' : event_type};
						}
					else
						{
							if(o_data[i_employee_id][s_weekending].date_created > d_created)
								{
									o_data[i_employee_id][s_weekending].Old_ST_Hours	=	f_old_st_hours;
									o_data[i_employee_id][s_weekending].Old_OT_Hours	=	f_old_ot_hours;
									o_data[i_employee_id][s_weekending].Old_Leave_Hours	=	f_old_leave_hours;
									o_data[i_employee_id][s_weekending].Old_Holiday_Hours	=	f_old_holiday_hours;
								}
							else
								{
									o_data[i_employee_id][s_weekending].New_ST_Hours	=	f_new_st_hours;
									o_data[i_employee_id][s_weekending].New_OT_Hours	=	f_new_ot_hours;
									o_data[i_employee_id][s_weekending].New_Leave_Hours	=	f_new_leave_hours;
									o_data[i_employee_id][s_weekending].New_Holiday_Hours	=	f_new_holiday_hours;
									o_data[i_employee_id][s_weekending].last_modified	=	d_created;
									o_data[i_employee_id][s_weekending].last_modified_by	=	s_updated_by;
									o_data[i_employee_id][s_weekending].type = event_type;
								}
						}
				}
			
			var a_list	=	new Array();
			
			for(var i_employee_id in o_data)
				{
					for(var s_weekending in o_data[i_employee_id])
						{
						
							var o_list_row	=	new Object();
						
							var o_data_record	=	o_data[i_employee_id][s_weekending];
						
							var f_st_diff	=	o_data_record.New_ST_Hours - o_data_record.Old_ST_Hours;
							var f_ot_diff	=	o_data_record.New_OT_Hours - o_data_record.Old_OT_Hours;
							var f_leave_diff	=	o_data_record.New_Leave_Hours - o_data_record.Old_Leave_Hours;
							var f_holiday_diff	=	o_data_record.New_Holiday_Hours - o_data_record.Old_Holiday_Hours;
							
							if(f_st_diff != 0 || f_ot_diff != 0)
								{
									o_list_row['employee']	=	o_data_record.Employee;
									o_list_row['weekending']	=	s_weekending;
									o_list_row['old_st_hours']	=	durationToHours(o_data_record.Old_ST_Hours);
									o_list_row['new_st_hours']	=	durationToHours(o_data_record.New_ST_Hours);
									o_list_row['st_hours_diff']	=	durationToHours(f_st_diff);
									o_list_row['old_ot_hours']	=	durationToHours(o_data_record.Old_OT_Hours);
									o_list_row['new_ot_hours']	=	durationToHours(o_data_record.New_OT_Hours);
									o_list_row['ot_hours_diff']	=	durationToHours(f_ot_diff);
									o_list_row['old_leave_hours']	=	durationToHours(o_data_record.Old_Leave_Hours);
									o_list_row['new_leave_hours']	=	durationToHours(o_data_record.New_Leave_Hours);
									o_list_row['leave_hours_diff']	=	durationToHours(f_leave_diff);
									o_list_row['old_holiday_hours']	=	durationToHours(o_data_record.Old_Holiday_Hours);
									o_list_row['new_holiday_hours']	=	durationToHours(o_data_record.New_Holiday_Hours);
									o_list_row['holiday_hours_diff']	=	durationToHours(f_holiday_diff);
									o_list_row['employee_type']	=	o_data_record.employee_type;
									o_list_row['last_modified']	=	nlapiDateToString(o_data_record.last_modified, 'datetime');
									o_list_row['last_modified_by']	=	o_data_record.last_modified_by;
									o_list_row['type']	=	o_data_record.type;
									a_list.push(o_list_row);
								}					
						}				
				}
			
			if(mode == 'display_report')
				{
					var sublist1 = form.addSubList('record', 'list', 'Payroll Diff Report(Changes between ' + s_run_st_date + ' and ' + s_run_end_date + ')');
					sublist1.addField('employee', 'text', 'Employee');
					sublist1.addField('weekending', 'text', 'Weekending');
					sublist1.addField('old_st_hours', 'text', 'ST Hours Was');
					sublist1.addField('new_st_hours', 'text', 'ST Hours Is');
					sublist1.addField('st_hours_diff', 'text', 'ST Hours Diff');	
					sublist1.addField('old_ot_hours', 'text', 'OT Hours Was');
					sublist1.addField('new_ot_hours', 'text', 'OT Hours Is');
					sublist1.addField('ot_hours_diff', 'text', 'OT Hours Diff');
					sublist1.addField('old_leave_hours', 'text', 'Leave Hours Was');
					sublist1.addField('new_leave_hours', 'text', 'Leave Hours Is');
					sublist1.addField('leave_hours_diff', 'text', 'Leave Hours Diff');	
					sublist1.addField('old_holiday_hours', 'text', 'Holiday Hours Was');
					sublist1.addField('new_holiday_hours', 'text', 'Holiday Hours Is');
					sublist1.addField('holiday_hours_diff', 'text', 'Holiday Hours Diff');
					sublist1.addField('employee_type', 'text', 'Employee Type');
					sublist1.addField('last_modified', 'text', 'Last Modified');
					sublist1.addField('last_modified_by', 'text', 'Last Modified By');
					sublist1.addField('type','text','Type')
					sublist1.setLineItemValues(a_list);
				}
			else
				{
					//s_run_st_date	=	request.getParameter('custpage_from_date');
			
					//s_run_end_date	=	request.getParameter('custpage_end_date');
					
					exportToExcel(a_list, s_run_st_date, s_run_end_date);
					
					return;
				}
		}	
	
	response.writePage(form);
	
}

//Export To Excel
function exportToExcel(a_data, s_run_st_date, s_run_end_date) {

	var count = a_data.length;
	var globalArray = new Array();
	var o_columns = new Object();
	o_columns['employee'] = 'Employee';
	o_columns['weekending'] = 'Weekending';
	o_columns['old_st_hours'] = 'ST Hours Was';
	o_columns['new_st_hours'] = 'ST Hours Is';
	o_columns['st_hours_diff'] = 'ST Hours Diff';
	o_columns['old_ot_hours'] = 'OT Hours Was';
	o_columns['new_ot_hours'] = 'OT Hours Is';
	o_columns['ot_hours_diff'] = 'OT Hours Diff';
	o_columns['old_leave_hours'] = 'Leave Hours Was';
	o_columns['new_leave_hours'] = 'Leave Hours Is';
	o_columns['leave_hours_diff'] = 'Leave Hours Diff';
	o_columns['old_holiday_hours'] = 'Holiday Hours Was';
	o_columns['new_holiday_hours'] = 'Holiday Hours Is';
	o_columns['holiday_hours_diff'] = 'Holiday Hours Diff';
	o_columns['employee_type'] = 'Employee Type';
	o_columns['last_modified'] = 'Last Modified';
	o_columns['last_modified_by']	=	'Last Modified By';
	o_columns['type'] = 'Type'

	// Enter the headings
	var s_line = '';
	for ( var s_column in o_columns) {
		s_line += o_columns[s_column] + ',';
	}
	globalArray.push(s_line);
	for (var i = 0; i < count; i++) {
		s_line = '\n';
		var a_line = new Array();
		for ( var s_column in o_columns) {
			var s_value = a_data[i][s_column].toString();
			s_value = s_value.toString().replace(/[|]/g, " ");
			s_value = s_value.replace(/[,]/g, " ");
			a_line.push(s_value);// o_sublist.getLineItemValue(s_column, i));
			// // [s_column]
			// + ',';
		}
		s_line += a_line.toString();
		globalArray.push(s_line);
	}

	var fileName = 'Payroll Diff Report(Changes After ' + s_run_st_date + ')';
	var CSVName = fileName + " run at - " + s_run_end_date + '.csv';
	var file = nlapiCreateFile(CSVName, 'CSV', globalArray.toString());

	// nlapiLogExecution('DEBUG', 'suitlet_Po_ClosedStatus', ' PDF Name--> ');
	response.setContentType('CSV', CSVName);

	// nlapiLogExecution('DEBUG', 'suitlet_Po_ClosedStatus', ' PDF Name--> ');
	response.write(file.getValue());
}

function durationToHours(f_duration)
{
	//var i_hours = Math.floor(f_duration);
	//var f_minutes	=	f_duration - i_hours;
	
	//var i_minutes	=	Math.round(f_minutes * 60);
	
	return parseFloat(f_duration).toFixed(2);
}

function _logValidation(value) 
{
 if(value != null && value.toString() != null && value != '' && value != undefined && value.toString() != undefined && value != 'undefined' && value.toString() != 'undefined'&& value.toString() != 'NaN' && value != NaN) 
 {
  return true;
 }
 else 
 { 
  return false;
 }
}