/*** Rest API for getting the employee skill based on email passed from client ***/

function postRESTlet(dataIn) {
	
	try{
		var response = new Response(); 
		
		var current_date = nlapiDateToString(new Date());
		//Log for current date
		nlapiLogExecution('DEBUG', 'Current Date', 'current_date...' + current_date)
		nlapiLogExecution('debug', 'dataIn', JSON.stringify(dataIn));
		
		var employeeId =  getUserUsingEmailId(dataIn.EmailId);
		var requestType = dataIn.RequestType;
		switch (requestType) {

		case M_Constants.Request.Get:

			if (employeeId) {
				response.Data = getSkillDetails(employeeId);
				response.Status = true;
			} 
			else {
				response.Data = "Some error with the data sent";  
				response.Status = false;
			}
			break;
		}
			
		
	}
	catch(err){
		nlapiLogExecution('error', 'Restlet Submit Function', err);
		response.Data = err;
		response.Status = false;
	}
	nlapiLogExecution('debug', 'response', JSON.stringify(response));
	return response;
}

function getSkillDetails(emp_id){
try{
	var a_data = [];
var customrecord_emp_skill_master_dataSearch = nlapiSearchRecord("customrecord_emp_skill_master_data",null,
[
   ["custrecord_skill_resource","anyof",emp_id],
   "AND", 
   ["isinactive","is","F"]
], 
[
   new nlobjSearchColumn("scriptid").setSort(false), 
   new nlobjSearchColumn("custrecord_skill_resource"), 
   new nlobjSearchColumn("custrecord_res_dept"), 
   new nlobjSearchColumn("custrecord_resource_skillset"), 
   new nlobjSearchColumn("custrecord_resource_proficency"), 
   new nlobjSearchColumn("custrecord_res_certificate")
]
);
if(customrecord_emp_skill_master_dataSearch){
for(var i=0;i<customrecord_emp_skill_master_dataSearch.length;i++){
	var data_obj = {};
	data_obj.Employee = customrecord_emp_skill_master_dataSearch[i].getText('custrecord_skill_resource');
	data_obj.Department = customrecord_emp_skill_master_dataSearch[i].getText('custrecord_res_dept');
	data_obj.Skill = customrecord_emp_skill_master_dataSearch[i].getText('custrecord_resource_skillset');
	data_obj.Proficiency = customrecord_emp_skill_master_dataSearch[i].getText('custrecord_resource_proficency');
	data_obj.Certificate = customrecord_emp_skill_master_dataSearch[i].getValue('custrecord_res_certificate');
	a_data.push(data_obj);	
}
}
return a_data;
}
catch(e){
nlapiLogExecution('DEBUG','Skil Search Error',e);
throw e;
}
}
