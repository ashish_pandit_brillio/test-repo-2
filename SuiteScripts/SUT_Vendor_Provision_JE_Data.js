


function suiteletFunction_Vendor_Pro_JE_Data(request, response)
{
	try
	{
		var vendor_provision_month =  request.getParameter('vendor_provision_month');
		vendor_provision_month = vendor_provision_month.trim();
		var vendor_provision_year =  request.getParameter('vendor_provision_year');
		vendor_provision_year = vendor_provision_year.trim();
		var vendor_provision_subsidiary =  request.getParameter('vendor_provision_subsidiary');
		
		vendor_provision_subsidiary = vendor_provision_subsidiary.trim();
		var rate_type = request.getParameter('vendor_provision_rate_type');
		rate_type = rate_type.trim();
		
		var filters_JE_data = new Array();
		filters_JE_data[0] = new nlobjSearchFilter('custrecord_month_sal_up_ven', null, 'is', vendor_provision_month);
		filters_JE_data[1] = new nlobjSearchFilter('custrecord_year_sal_up_ven', null, 'is', vendor_provision_year);
		filters_JE_data[2] = new nlobjSearchFilter('custrecord_rate_type_sal_upload', null, 'is', parseInt(rate_type));
		filters_JE_data[3] = new nlobjSearchFilter('custrecord_subsidiary_sal_up_ven', null, 'is', parseInt(vendor_provision_subsidiary));
		filters_JE_data[4] = new nlobjSearchFilter('custrecord_is_processed', null, 'is', 'T');
		
		var column = new Array();
		column[0]  = new nlobjSearchColumn('custrecord_employee_rec_id_sal_up_ven');
		column[1]  = new nlobjSearchColumn('custrecord_location_sla_up_ven');
		column[2]  = new nlobjSearchColumn('custrecord_cost_sal_up_ven');
		column[3]  = new nlobjSearchColumn('custrecord_project_assigned');
		column[4]  = new nlobjSearchColumn('custrecord_proj_hours_allocated');
		column[5]  = new nlobjSearchColumn('custrecord_percent_of_time_allocated');
		column[6]  = new nlobjSearchColumn('custrecord_no_of_leaves_taken');
		column[7]  = new nlobjSearchColumn('custrecord_no_of_sub_nt_approved_days');
		column[8]  = new nlobjSearchColumn('custrecord_no_of_approved_days');
		column[9]  = new nlobjSearchColumn('custrecord_no_of_nt_submitted_days');
		column[10]  = new nlobjSearchColumn('custrecord_allocated_time');
		column[11]  = new nlobjSearchColumn('custrecord_currency_ven_pro');
		column[12]  = new nlobjSearchColumn('custrecord_no_of_holidays');
		column[13]  = new nlobjSearchColumn('custrecord_approved_cost');
		column[14]  = new nlobjSearchColumn('custrecord_nt_approved_cost');
		column[15]  = new nlobjSearchColumn('custrecord_nt_submitted_cost');
		column[16]  = new nlobjSearchColumn('custrecord_hire_date');
		column[17]  = new nlobjSearchColumn('custrecord_release_date');
				
		var down_excel = nlapiSearchRecord('customrecord_salary_upload_file', null, filters_JE_data, column);
		if (_logValidation(down_excel))
		{
			down_excel_function(down_excel, vendor_provision_month, vendor_provision_year,vendor_provision_subsidiary);
		}
	}
	catch(err)
	{
		nlapiLogExecution('ERROR','ERROR MESSAGE:- ',err);
	}
}

function down_excel_function(down_excel,vendor_provision_month,vendor_provision_year,vendor_provision_subsidiary)
{
	try
	{
		var strVar1= '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:ss="urn:schemas-microsoft-com:office:spreadsheet" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40">'+
		'<head>'+
		'<meta http-equiv=Content-Type content="text/html; charset=windows-1252"/>'+
        '<meta name=ProgId content=Excel.Sheet/>'+
        '<meta name=Generator content="Microsoft Excel 11"/>'+
        '<!--[if gte mso 9]><xml>'+
        '<x:excelworkbook>'+
        '<x:excelworksheets>'+
        '<x:excelworksheet=sheet1>'+
        '<x:name>** ESTIMATE FILE**</x:name>'+
        '<x:worksheetoptions>'+
        '<x:selected></x:selected>'+
        '<x:freezepanes></x:freezepanes>'+
        '<x:frozennosplit></x:frozennosplit>'+
        '<x:splithorizontal>** FROZEN ROWS + 1 **</x:splithorizontal>'+
        '<x:toprowbottompane>** FROZEN ROWS + 1 **</x:toprowbottompane>'+
        '<x:splitvertical>** FROZEN COLUMNS + 1 **</x:splitvertical>'+
        '<x:leftcolumnrightpane>** FROZEN COLUMNS + 1**</x:leftcolumnrightpane>'+
		    '<x:activepane>0</x:activepane>'+// 0
		    '<x:panes>'+
			'<x:pane>'+
			'<x:number>3</x:number>'+
			'</x:pane>'+
			'<x:pane>'+
		    '<x:number>1</x:number>'+
		    '</x:pane>'+
		    '<x:pane>'+
			'<x:number>2</x:number>'+
        '</x:pane>'+
        '<x:pane>'+
        '<x:number>0</x:number>'+//1
        '</x:pane>'+
        '</x:panes>'+
        '<x:protectcontents>False</x:protectcontents>'+
        '<x:protectobjects>False</x:protectobjects>'+
        '<x:protectscenarios>False</x:protectscenarios>'+
        '</x:worksheetoptions>'+
        '</x:excelworksheet>'+
		'</x:excelworksheets>'+
        '<x:protectstructure>False</x:protectstructure>'+
        '<x:protectwindows>False</x:protectwindows>'+
		'</x:excelworkbook>'+
        
        // '<style>'+
			
			//-------------------------------------
        '</x:excelworkbook>'+
        '</xml><![endif]-->'+
         '<style>'+
		'p.MsoFooter, li.MsoFooter, div.MsoFooter'+
		'{ margin:0in; margin-bottom:.0001pt; mso-pagination:widow-orphan; tab-stops:center 3.0in right 6.0in; font-size:12.0pt;}'+
		'<style>'+

		'<!-- /* Style Definitions */'+

		//'@page Section1'+
		//'{ size:8.5in 11.0in; margin:1.9cm 1.27cm 1.9cm 1.27cm; mso-header-margin:.5in; mso-footer-margin:.5in; mso-title-page:yes; mso-header: h1; mso-footer: f1; mso-first-header: fh1; mso-first-footer: ff1; mso-paper-source:0;}'+

		'div.Section1'+
		'{ page:Section1;}'+

		'table#hrdftrtbl'+
		'{ margin:0in 0in 0in 900in; width:1px; height:1px; overflow:hidden; font-family:Arial;}-->'+

		'</style>'+
		
		
		'</head>'+
		

		'<body>'+
		
		'<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">'
		
		var strVar2 = '';    
		if(vendor_provision_subsidiary == 2)
		{
		strVar2 += "<table width=\"100%\">";
		strVar2 += "<tr>";
		strVar2 += "<td Width=\"5%\" style='text-align:left;font-weight:bold;' font-size=\"10\" \>Month - Year</td>";
		strVar2 += "<td Width=\"15%\" style='text-align:left;font-weight:bold;' font-size=\"10\" \>Employee Name</td>";
		strVar2 += "<td Width=\"5%\" style='text-align:left;font-weight:bold;' font-size=\"10\" \>Location</td>";
		strVar2 += "<td Width=\"5%\" style='text-align:left;font-weight:bold;' font-size=\"10\" \>Currency</td>";
		strVar2 += "<td Width=\"5%\" style='text-align:left;font-weight:bold;' font-size=\"10\" \>Hourly Rate</td>";
		strVar2 += "<td Width=\"15%\" style='text-align:left;font-weight:bold;' font-size=\"10\" \>Project</td>";
		strVar2 += "<td Width=\"5%\" style='text-align:left;font-weight:bold;' font-size=\"10\" \>Project Allocated Hours</td>";
		strVar2 += "<td Width=\"5%\" style='text-align:left;font-weight:bold;' font-size=\"10\" \>Percent of Time</td>";
		strVar2 += "<td Width=\"5%\" style='text-align:left;font-weight:bold;' font-size=\"10\" \>Total Leaves Taken (Hours)</td>";
		strVar2 += "<td Width=\"5%\" style='text-align:left;font-weight:bold;' font-size=\"10\" \>Total Holidays(Hours)</td>";
		strVar2 += "<td Width=\"5%\" style='text-align:left;font-weight:bold;' font-size=\"10\" \>Not Approved (Hours)</td>";
		strVar2 += "<td Width=\"5%\" style='text-align:left;font-weight:bold;' font-size=\"10\" \>Not Approved Hours Cost</td>";
		strVar2 += "<td Width=\"5%\" style='text-align:left;font-weight:bold;' font-size=\"10\" \>Approved (Hours)</td>";
		strVar2 += "<td Width=\"5%\" style='text-align:left;font-weight:bold;' font-size=\"10\" \>Approved Hours Cost</td>";
		strVar2 += "<td Width=\"5%\" style='text-align:left;font-weight:bold;' font-size=\"10\" \>Not Submitted (Hours)</td>";
		strVar2 += "<td Width=\"5%\" style='text-align:left;font-weight:bold;' font-size=\"10\" \>Not Submitted Hours Cost</td>";
		strVar2 += "<td Width=\"5%\" style='text-align:left;font-weight:bold;' font-size=\"10\" \>Employee Hire Date</td>";
		strVar2 += "<td Width=\"5%\" style='text-align:left;font-weight:bold;' font-size=\"10\" \>EmployeeRelease Date</td>";
		strVar2 += "<\/tr>";
		}
		else
		{
		strVar2 += "<table width=\"100%\">";
		strVar2 += "<tr>";
		strVar2 += "<td Width=\"5%\" style='text-align:left;font-weight:bold;' font-size=\"10\" \>Month - Year</td>";
		strVar2 += "<td Width=\"15%\" style='text-align:left;font-weight:bold;' font-size=\"10\" \>Employee Name</td>";
		strVar2 += "<td Width=\"5%\" style='text-align:left;font-weight:bold;' font-size=\"10\" \>Location</td>";
		strVar2 += "<td Width=\"5%\" style='text-align:left;font-weight:bold;' font-size=\"10\" \>Currency</td>";
		strVar2 += "<td Width=\"5%\" style='text-align:left;font-weight:bold;' font-size=\"10\" \>Monthly Rate</td>";
		strVar2 += "<td Width=\"15%\" style='text-align:left;font-weight:bold;' font-size=\"10\" \>Project</td>";
		strVar2 += "<td Width=\"5%\" style='text-align:left;font-weight:bold;' font-size=\"10\" \>Project Allocated Hours</td>";
		strVar2 += "<td Width=\"5%\" style='text-align:left;font-weight:bold;' font-size=\"10\" \>Percent of Time</td>";
		strVar2 += "<td Width=\"5%\" style='text-align:left;font-weight:bold;' font-size=\"10\" \>Total Leaves Taken (days)</td>";
		strVar2 += "<td Width=\"5%\" style='text-align:left;font-weight:bold;' font-size=\"10\" \>Total Holidays(days)</td>";
		strVar2 += "<td Width=\"5%\" style='text-align:left;font-weight:bold;' font-size=\"10\" \>Not Approved (days)</td>";
		strVar2 += "<td Width=\"5%\" style='text-align:left;font-weight:bold;' font-size=\"10\" \>Not Approved Days Cost</td>";
		strVar2 += "<td Width=\"5%\" style='text-align:left;font-weight:bold;' font-size=\"10\" \>Approved (days)</td>";
		strVar2 += "<td Width=\"5%\" style='text-align:left;font-weight:bold;' font-size=\"10\" \>Approved Days Cost</td>";
		strVar2 += "<td Width=\"5%\" style='text-align:left;font-weight:bold;' font-size=\"10\" \>Not Submitted (days)</td>";
		strVar2 += "<td Width=\"5%\" style='text-align:left;font-weight:bold;' font-size=\"10\" \>Not Submitted Days Cost</td>";
		strVar2 += "<td Width=\"5%\" style='text-align:left;font-weight:bold;' font-size=\"10\" \>Employee Hire Date</td>";
		strVar2 += "<td Width=\"5%\" style='text-align:left;font-weight:bold;' font-size=\"10\" \>EmployeeRelease Date</td>";
		strVar2 += "<\/tr>";	
		}
		if (_logValidation(down_excel)) //
		{
			for (var i = 0; i < down_excel.length; i++) //
			{
				var Emp_name = down_excel[i].getText('custrecord_employee_rec_id_sal_up_ven');
				var Emp_location = down_excel[i].getValue('custrecord_location_sla_up_ven');
				var Emp_currency = down_excel[i].getValue('custrecord_currency_ven_pro');
				var Emp_monthly_rate = down_excel[i].getValue('custrecord_cost_sal_up_ven');
				var Emp_Project = down_excel[i].getValue('custrecord_project_assigned');
				var Emp_proj_hours = down_excel[i].getValue('custrecord_proj_hours_allocated');
				var Emp_proj_percent_hours = down_excel[i].getValue('custrecord_percent_of_time_allocated');
				var leave_duration_days = down_excel[i].getValue('custrecord_no_of_leaves_taken');
				var not_approved_days = down_excel[i].getValue('custrecord_no_of_sub_nt_approved_days');
				var approved_days = down_excel[i].getValue('custrecord_no_of_approved_days');
				var not_submitted_days = down_excel[i].getValue('custrecord_no_of_nt_submitted_days');
				var allocated_days = down_excel[i].getValue('custrecord_allocated_time');
				var holidays = down_excel[i].getValue('custrecord_no_of_holidays');
				var approved_cost = down_excel[i].getValue('custrecord_approved_cost');
				var nt_approved_cost = down_excel[i].getValue('custrecord_nt_approved_cost');
				var nt_submitted_cost = down_excel[i].getValue('custrecord_nt_submitted_cost');				
				var emp_hire_Date = down_excel[i].getValue('custrecord_hire_date');	
				var emp_release_date = down_excel[i].getValue('custrecord_release_date');	
				
				strVar2 += "<tr>";
				strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \>" + vendor_provision_month + "-" + vendor_provision_year + "</td>";
				strVar2 += "<td Width=\"15%\" style='text-align:left;' font-size=\"10\" \>" + Emp_name + "</td>";
				strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \>" + Emp_location + "</td>";
				strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \>" + Emp_currency + "</td>";
				strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \>" + Emp_monthly_rate + "</td>";
				strVar2 += "<td Width=\"15%\" style='text-align:left;' font-size=\"10\" \>" + Emp_Project + "</td>";
				strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \>" + allocated_days + "</td>";
				strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \>" + Emp_proj_percent_hours + "</td>";
				strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \>" + leave_duration_days + "</td>";
				strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \>" + holidays + "</td>";
				strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \>" + not_approved_days + "</td>";
				strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \>" + nt_approved_cost + "</td>";
				strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \>" + approved_days + "</td>";
				strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \>" + approved_cost + "</td>";
				strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \>" + not_submitted_days + "</td>";
				strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \>" + nt_submitted_cost + "</td>";
				strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \>" + emp_hire_Date + "</td>";
				strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \>" + emp_release_date + "</td>";
				strVar2 += "<\/tr>";
			}
		}
		else
		{
			//NO RESULTS FOUND
		}
					
		strVar2 += "<\/table>";			
		strVar1 = strVar1 + strVar2;
		var file = nlapiCreateFile('Vendor Provision JE Data.xls', 'XMLDOC', strVar1);
		nlapiLogExecution('debug','file:- ',file);
		response.setContentType('XMLDOC','Vendor Provision JE Data.xls');
		response.write( file.getValue() );	 
		
	}
	catch(err)
	{
		nlapiLogExecution('ERROR','ERROR MESSAGE:- ',err);
	}
}

function _logValidation(value) 
{
 if(value != null && value.toString() != null && value != '' && value != undefined && value.toString() != undefined && value != 'undefined' && value.toString() != 'undefined'&& value.toString() != 'NaN' && value != NaN) 
 {
  return true;
 }
 else 
 { 
  return false;
 }
}
