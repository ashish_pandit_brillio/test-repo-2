/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       17 Jan 2022     Chaitali Gaikwad
 *
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType
 *   
 * @param {String} type Operation types: create, edit, view, copy, print, email
 * @param {nlobjForm} form Current form
 * @param {nlobjRequest} request Request object
 * @returns {Void}
 */
function userEventBeforeLoad(type, form, request)
{
 
}

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType
 * 
 * @param {String} type Operation types: create, edit, delete, xedit
 *                      approve, reject, cancel (SO, ER, Time Bill, PO & RMA only)
 *                      pack, ship (IF)
 *                      markcomplete (Call, Task)
 *                      reassign (Case)
 *                      editforecast (Opp, Estimate)
 * @returns {Void}
 */
function BeforeSub_auto_num(type)
{
	var ref_no = nlapiGetFieldValue('tranid');
	nlapiLogExecution('DEBUG', 'Before Submit ref_no', ref_no);
	
	nlapiSetFieldValue('tranid', ref_no, true, true);
//	nlapiSetFieldValue('generatetranidonsave', 'T');
	nlapiSetFieldValue('custbody_test_refernce_no', ref_no, true, true);
	nlapiLogExecution('DEBUG', 'Before Submit nlapiGetFieldValue(tranid)', nlapiGetFieldValue('tranid'));
	
 
}

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType
 * 
 * @param {String} type Operation types: create, edit, delete, xedit,
 *                      approve, cancel, reject (SO, ER, Time Bill, PO & RMA only)
 *                      pack, ship (IF only)
 *                      dropship, specialorder, orderitems (PO only) 
 *                      paybills (vendor payments)
 * @returns {Void}
 */
function AfterSub_auto_num(type)
{
	 var i_Rec_Id = nlapiGetRecordId();
	 var i_Rec_Type = nlapiGetRecordType();
	 nlapiLogExecution('DEBUG', 'AfterSub_auto_num', 'Transaction Type = '+i_Rec_Type);
	 nlapiLogExecution('DEBUG', 'AfterSub_auto_num', 'Transaction ID = '+i_Rec_Id);
	 
	 
	 var o_Record = nlapiLoadRecord(i_Rec_Type, i_Rec_Id);
	 nlapiLogExecution('DEBUG', 'AfterSub_auto_num', 'o_Record = '+o_Record);
	 
	 
		
	var ref_no = o_Record.getFieldValue('tranid');
	nlapiLogExecution('DEBUG', 'After Submit ref_no', ref_no);
	
	o_Record.setFieldValue('tranid', ref_no);
	o_Record.setFieldValue('custbody_test_refernce_no', ref_no);
	var sub_rec = nlapiSubmitRecord(o_Record, true, true);
	nlapiLogExecution('DEBUG', 'Aftre Submit sub_rec', sub_rec);
	
 
  
}
