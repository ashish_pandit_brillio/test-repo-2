							
// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
/*
   	Script Name : FUEL | UES Project PM/DM table Notification
	Author      : Ashish Pandit
	Date        : 17 Sept 2019
    Description : User Event to update MariaDB on Field change (PM/DM) in Project record   


	Script Modification Log:

	-- Date --			-- Modified By --				--Requested By--				-- Description --

Below is a summary of the process controls enforced by this script file.  The control logic is described
more fully, below, in the appropriate function headers and code blocks.


     BEFORE LOAD
		- beforeLoadRecord(type)



     BEFORE SUBMIT
		- beforeSubmitRecord(type)


     AFTER SUBMIT
		- afterSubmitRecord(type)



     SUB-FUNCTIONS
		- The following sub-functions are called by the above core functions in order to maintain code
            modularization:

               - NOT USED

*/
}
// END SCRIPT DESCRIPTION BLOCK  ====================================

function afterSubmitCheckFields(type)
{
	nlapiLogExecution('Debug','type ',type);
	if(type == 'edit' || type == 'xedit')
	{
		try
		{
			var oldRecord = nlapiGetOldRecord();
			var i_OldProjectManager = oldRecord.getFieldValue('custentity_projectmanager');
			var i_OldDeliveryManager = oldRecord.getFieldValue('custentity_deliverymanager');
			var recordObject = nlapiLoadRecord(nlapiGetRecordType(), nlapiGetRecordId());
			var i_NewProjectManager = recordObject.getFieldValue('custentity_projectmanager');
			var i_NewDeliveryManager = recordObject.getFieldValue('custentity_deliverymanager');
			var i_accountId = recordObject.getFieldValue('parent');
			var s_projectType = recordObject.getFieldText('jobtype');
			var s_status = recordObject.getFieldText('entitystatus');
			var s_allocationCategory = recordObject.getFieldText('custentity_project_allocation_category');
			var custentity_practice = recordObject.getFieldValue('custentity_practice');
			var practiceInternalId,parentPracticeName='';
			var parentPractice = Number(nlapiLookupField('department',custentity_practice,'custrecord_parent_practice'));
			var companyname = recordObject.getFieldValue('companyname');
			if(parentPractice)
			{
				practiceInternalId = parentPractice;
				parentPracticeName = nlapiLookupField('department',custentity_practice,'custrecord_parent_practice',true);
			}
			else
			{
				practiceInternalId = custentity_practice;
				parentPracticeName = recordObject.getFieldText('custentity_practice');
			}
			var body = {};
			var method = '';
           
		   if(i_OldProjectManager != i_NewProjectManager || i_OldDeliveryManager != i_NewDeliveryManager)
			{
				method = "POST";
				body.internalId = nlapiGetRecordId();
				body.name = companyname;
				body.practiceInternalId = practiceInternalId;
				body.parentPractice = parentPracticeName;
				if(i_NewProjectManager)
					body.pmEmailId = nlapiLookupField("employee", i_NewProjectManager, "email");
				else
					body.pmEmailId = "";
				if(i_NewDeliveryManager)
					body.dmEmailId = nlapiLookupField("employee", i_NewDeliveryManager, "email"); 
				else
					body.dmEmailId = "";
				
				if(i_accountId)
					body.accountInternalId = i_accountId; 
				else
					body.accountInternalId = "";
				if(s_projectType)
					body.projectType = s_projectType;
				else
					body.projectType = "";
				if(s_status)
					body.status = s_status;
				else
					body.status = "";
				if(s_allocationCategory)
					body.projectAllocationCategory = s_allocationCategory;
				else
					body.projectAllocationCategory = "";
				nlapiLogExecution("DEBUG", "body edit Mode: ", JSON.stringify(body));
				var url = "https://fuelnode1.azurewebsites.net/project_pm_dm";
				nlapiLogExecution('debug','body  ',JSON.stringify(body));
				body = JSON.stringify(body);
				var response = call_node(url,body,method);
			//	nlapiLogExecution("DEBUG", "response Edit Mode: ", JSON.stringify(response));
			}
		}
		catch(error)
		{
			nlapiLogExecution('Debug','Error ',error);
		}
	}
	if(type == 'create')
	{
		try
		{
			var recordObject = nlapiLoadRecord(nlapiGetRecordType(), nlapiGetRecordId());
			var i_NewProjectManager = recordObject.getFieldValue('custentity_projectmanager');
			var i_NewDeliveryManager = recordObject.getFieldValue('custentity_deliverymanager');
			var i_accountId = recordObject.getFieldValue('parent');
			var s_projectType = recordObject.getFieldText('jobtype');
			var s_status = recordObject.getFieldText('entitystatus');
			var s_allocationCategory = recordObject.getFieldText('custentity_project_allocation_category');
			
			var custentity_practice = recordObject.getFieldValue('custentity_practice');
			var practiceInternalId='';
			var parentPractice = Number(nlapiLookupField('department',custentity_practice,'custrecord_parent_practice'));
			if(parentPractice)
			{
				practiceInternalId = parentPractice;
			}
			else
			{
				practiceInternalId = custentity_practice;
			}
			
			var body = {};
			var method;
			method = "POST";
			body.internalId = nlapiGetRecordId();
			if(i_NewProjectManager)
				body.pmEmailId = nlapiLookupField("employee", i_NewProjectManager, "email"); 
			else
				body.pmEmailId = "";
			if(i_NewDeliveryManager)
				body.dmEmailId = nlapiLookupField("employee", i_NewDeliveryManager, "email"); 
			else
				body.dmEmailId = "";
			if(i_accountId)
				body.accountInternalId = i_accountId; 
			else
				body.accountInternalId = "";
			if(s_projectType)
				body.projectType = s_projectType;
			else
				body.projectType = "";
			if(s_status)
				body.status = s_status;
			else
				body.status = "";
			if(s_allocationCategory)
				body.projectAllocationCategory = s_allocationCategory;
			else
				body.projectAllocationCategory = "";
			
			
			body.practiceInternalId = practiceInternalId;
			
			nlapiLogExecution("DEBUG", "body create Mode: ", JSON.stringify(body));
			var url = "https://fuelnode1.azurewebsites.net/project_pm_dm";
			nlapiLogExecution('debug','body  ',JSON.stringify(body));
			body = JSON.stringify(body);
			var response = call_node(url,body,method);
		//	nlapiLogExecution("DEBUG", "response Create Mode: ", JSON.stringify(response));
		}
		catch(error)
		{
			nlapiLogExecution('Debug','Error ',error);
		}
	}
	
}
function beforeSubmitCheckFields(type)
{
	if (type == 'delete')
	{
		var body = {};
		var method = "DELETE";
		//body.internalId = nlapiGetRecordId();
		var url = "https://fuelnode1.azurewebsites.net/project_pm_dm/internal_id/"+nlapiGetRecordId();
		nlapiLogExecution('debug','body  ',JSON.stringify(body));
		//body = JSON.stringify(body);
		var response = call_node(url,body,method);
		nlapiLogExecution("DEBUG", "response Create Mode: ", JSON.stringify(response));
	}
}
// END BEFORE LOAD ====================================================


function _logValidation(value) 
{
 if(value != null && value!= 'null' && value != '' && value != undefined && value != undefined && value != 'undefined' && value.toString() != 'NaN' && value != NaN) 
 {
  return true;
 }
 else 
 { 
  return false;
 }
}
// END FUNCTION =====================================================
