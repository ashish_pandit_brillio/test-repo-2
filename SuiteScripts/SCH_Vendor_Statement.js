// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
/*
   	Script Name:Sch Vendor statement
	Author:		Nikhil jain
	Company:	Aashnacloudtech Pvt. Ltd.
	Date:		24 April 2015


	Script Modification Log:

	-- Date --			-- Modified By --				--Requested By--				-- Description --




Below is a summary of the process controls enforced by this script file.  The control logic is described
more fully, below, in the appropriate function headers and code blocks.


     SCHEDULED FUNCTION
		- scheduledFunction(type)


     SUB-FUNCTIONS
		- The following sub-functions are called by the above core functions in order to maintain code
            modularization:

               - NOT USED

*/
}
// END SCRIPT DESCRIPTION BLOCK  ====================================


// BEGIN SCHEDULED FUNCTION =============================================

function HTMLDesignPopUpPDF(type)
{
    /*  On scheduled function:

          - PURPOSE
		-

	FIELDS USED:

          --Field Name--				--ID--


    */


	//  LOCAL VARIABLES



	//  SCHEDULED FUNCTION CODE BODY

	
		var htmlMsg = "";
		var company_id = getcompanyinfo();
		
		if(company_id != null && company_id != '' && company_id != undefined)
		{
			var o_comapny_obj = nlapiLoadRecord('customrecord_taxcompanyinfo',company_id);
			
			var companyname = o_comapny_obj.getFieldValue('custrecord_companyname')
			companyname = logvalidation(companyname)
			
			var nameofpremises = o_comapny_obj.getFieldValue('custrecord_nameofthe_premises_buildi')
			nameofpremises = logvalidation(nameofpremises)
			
			var road = o_comapny_obj.getFieldValue('custrecord_road_street_lane')
			road = logvalidation(road)
			
			var area= o_comapny_obj.getFieldValue('custrecord_area_location')
			area = logvalidation(area)
			
			var city = o_comapny_obj.getFieldValue('custrecord_town_city_district')
			city = logvalidation(city)
			
			var state = o_comapny_obj.getFieldText('custrecord_state')
			state = logvalidation(state)
			
			var pincode = o_comapny_obj.getFieldValue('custrecord_pincode')
			pincode = logvalidation(pincode)
			
			var company_address  = ' ';
			if(nameofpremises != null && nameofpremises != '' && nameofpremises != undefined)
			{
				company_address = nameofpremises
			}
			if(road != null && road != '' && road != undefined)
			{
				company_address = company_address +','+road
			}
			if(area != null && area != '' && area != undefined)
			{
				company_address = company_address +','+area
			}
			if(city != null && city != '' && city != undefined)
			{
				company_address = company_address +','+city
			}
			if(state != null && state != '' && state != undefined)
			{
				company_address = company_address +','+state
			}
			if(pincode != null && pincode != '' && pincode != undefined)
			{
				company_address = company_address +','+pincode
			}
		}
		
		nlapiLogExecution('DEBUG', 'searchVendorTransactions', 'Searching Vendor Transactions');
		var context = nlapiGetContext();
		
		/*
		var vendor_a = context.getSetting('SCRIPT', 'custscript_vendorname')
		
		var locationname = context.getSetting('SCRIPT', 'custscript_locationname')
		
		var start = context.getSetting('SCRIPT', 'custscript_startdate')
		
		var end = context.getSetting('SCRIPT', 'custscript_enddate')
		
		nlapiLogExecution('DEBUG', 'searchVendorTransactions', 'end date = '+end);
		
		var subsidiary = context.getSetting('SCRIPT', 'custscript_subsidiary')
		
		nlapiLogExecution('DEBUG', 'searchVendorTransactions', 'subsidiary = '+subsidiary);
		
		var recepient = context.getSetting('SCRIPT', 'custscript_recepient')
		
		var currency_criteria = context.getSetting('SCRIPT', 'custscript_ait_pdf_currency')
			
		*/
		var venstatid = context.getSetting('SCRIPT', 'custscript_ait_venstatid')
		
		var venstatobj = nlapiLoadRecord('customrecord_vendor_statement_detail_rec',venstatid)
		
		var vendor_a = venstatobj.getFieldValues('custrecord_ait_venstat_vendor')
		
		var locationname = venstatobj.getFieldValue('custrecord_ait_venstat_loc')
		
		var start = venstatobj.getFieldValue('custrecord_ait_venstat_startdate')
		
		var end = venstatobj.getFieldValue('custrecord_ait_vendstat_enddate')
		
		nlapiLogExecution('DEBUG', 'searchVendorTransactions', 'end date = '+end);
		
		var subsidiary = venstatobj.getFieldValue('custrecord_ait_venstat_subsidiary')
		
		nlapiLogExecution('DEBUG', 'searchVendorTransactions', 'subsidiary = '+subsidiary);
		
		var recepient = venstatobj.getFieldValue('custrecord_ait_venstat_email')
		
		var currency_criteria = venstatobj.getFieldValue('custrecord_ait_venstat_currency')
			
		var startdate = nlapiStringToDate(start);
		var enddate = nlapiStringToDate(end);
		
		
		if (parseInt(currency_criteria) == parseInt(1)) 
		{
			var custCurrency = "INR";
			
			var curFilters = new Array();
			var curColumns = new Array();
			curFilters.push(new nlobjSearchFilter('symbol', null, 'is', 'INR'));
			curColumns.push(new nlobjSearchColumn('internalid'));
			var curSearchResults = nlapiSearchRecord('currency', null, curFilters, curColumns);
			{
				var currency_id = curSearchResults[0].getValue('internalid');
				nlapiLogExecution('DEBUG', 'searchVendorTransactions', 'currency = ' + currency_id);
			}
			
			
		}
		else
		{
			var custCurrency = "Multi-Currency"
		}
		
		
		nlapiLogExecution('DEBUG', 'searchVendorTransactions', 'Vendor Name = '+vendor);
		nlapiLogExecution('DEBUG', 'searchVendorTransactions', 'Location Name = '+locationname);
		nlapiLogExecution('DEBUG', 'searchVendorTransactions', 'Start date = '+startdate);
		nlapiLogExecution('DEBUG', 'searchVendorTransactions', 'End date = '+enddate);
		
		
		//var vendor_array = new Array();
		//vendor_array = vendor_a.split(',')
		for (var z = 0; z < vendor_a.length; z++) 
		{
			var vendor = vendor_a[z];
			if (vendor != null && vendor != '' && vendor != undefined) 
			{
				if (z != 0) 
				{
					htmlMsg += "<pbr/>";
				}
				var custFilters = new Array();
				var custColumns = new Array();
				
				custFilters.push(new nlobjSearchFilter('internalid', null, 'is', vendor));
				custColumns.push(new nlobjSearchColumn('datecreated'));
				custColumns.push(new nlobjSearchColumn('entityid'));
				custColumns.push(new nlobjSearchColumn('address'));
				custColumns.push(new nlobjSearchColumn('currency'));
				
				var custSearchResults = nlapiSearchRecord('vendor', null, custFilters, custColumns);
				
				if (custSearchResults != null) 
				{
					var custCreatedDate = custSearchResults[0].getValue('datecreated');
					custCreatedDate = nlapiStringToDate(custCreatedDate);
					var custName = custSearchResults[0].getValue('entityid');
					var custAddr = custSearchResults[0].getValue('address');
					//var custCurrency = custSearchResults[0].getText('currency');
					//==============================function to get the first transaction date===================//
					var firstTranDate = getFirstTransactionDate(vendor);
					
					if(firstTranDate != null && firstTranDate != '' && firstTranDate != undefined)
					{
						custCreatedDate = nlapiStringToDate(firstTranDate);
					}
					nlapiLogExecution('DEBUG', 'searchVendorTransactions', 'custCreatedDate = ' + custCreatedDate);
					nlapiLogExecution('DEBUG', 'searchVendorTransactions', 'custName = ' + custName);
					nlapiLogExecution('DEBUG', 'searchVendorTransactions', 'custAddr = ' + custAddr);
					nlapiLogExecution('DEBUG', 'searchVendorTransactions', 'custCurrency = ' + custCurrency);
					
					custAddr = nlapiEscapeXML(custAddr);
					custAddr = afterReplaceCR(custAddr);
					
					var locationnametext;
					if (locationname != '' && locationname != null && locationname != 'undefined' && locationname != undefined) 
					{
						locationnametext = getLocationName(locationname);
						nlapiLogExecution('DEBUG', 'searchVendorTransactions', 'locationnametext = ' + locationnametext);
					}
					else 
					{
						locationnametext = "All"
					}
					locationnametext = nlapiEscapeXML(locationnametext);
					
					var opBalanceEndDatems = new Date(startdate.getTime());
					nlapiLogExecution('DEBUG', 'searchVendorTransactions', 'opBalanceEndDatems = ' + opBalanceEndDatems);
					
					opBalanceEndDatems = opBalanceEndDatems - 86400000;
					nlapiLogExecution('DEBUG', 'searchVendorTransactions', 'opBalanceEndDatems = ' + opBalanceEndDatems);
					
					var opBalanceEndDate = new Date(opBalanceEndDatems);
					nlapiLogExecution('DEBUG', 'searchVendorTransactions', 'opBalanceEndDate = ' + opBalanceEndDate);
					
					var openingBalanceDate = start;
					nlapiLogExecution('DEBUG', 'searchVendorTransactions', 'openingBalanceDate = ' + openingBalanceDate);
					
					//var combined_opening_balance = getOpeningBalance(vendor, locationname, custCreatedDate, opBalanceEndDate,subsidiary);
					
					//var opening_balance_array = combined_opening_balance.split('#')
					
					//var openingBalance = opening_balance_array[0];
					
					//var fxopeningBalance = opening_balance_array[1];
					
					var openingBalance = getOpeningBalance(vendor, locationname, custCreatedDate, opBalanceEndDate,subsidiary)
					
					//--------------------------Begin Print the company header name and address-------------------------------//
		
					htmlMsg = htmlMsg + "<table align=\"center\">";
					htmlMsg = htmlMsg + "<tr>";
	   				htmlMsg = htmlMsg + "<td align=\"center\" class=\"textVeryLargeBold\">"+companyname+"</td>";
					htmlMsg = htmlMsg + "</tr>";
					htmlMsg = htmlMsg + "<tr>";
	    			htmlMsg = htmlMsg + "<td align=\"justified\" class=\"textGoodThin\">"+company_address+"<BR/><BR/></td>";
					htmlMsg = htmlMsg + "</tr>";
					htmlMsg = htmlMsg + "</table>";
		
					//--------------------------End Print the company header name and address-------------------------------//
		
					//=====================================Begin : Print the vendor statement header details========================//
					
					htmlMsg = htmlMsg + "<table align=\"center\">";
					htmlMsg = htmlMsg + "<tr>";
					htmlMsg = htmlMsg + "<td class=\"textLargeBold\">Vendor Statement<BR/><BR/></td>";
					htmlMsg = htmlMsg + "</tr>";
					htmlMsg = htmlMsg + "</table>";
					
					htmlMsg = htmlMsg + "<table width=\"100%\">";
					htmlMsg = htmlMsg + "<tr>";
					
					htmlMsg = htmlMsg + "<td width=\"50%\">";
					htmlMsg = htmlMsg + "<table>";
					htmlMsg = htmlMsg + "<tr>";
					htmlMsg = htmlMsg + "<td class=\"textLargeBold\" align=\"left\">Vendor : " + nlapiEscapeXML(custName) + "</td>";
					htmlMsg = htmlMsg + "</tr>";
					htmlMsg = htmlMsg + "<tr>";
					htmlMsg = htmlMsg + "<td class=\"textLargeBold\" align=\"left\">" + custAddr + "</td>";
					htmlMsg = htmlMsg + "</tr>";
					htmlMsg = htmlMsg + "</table>";
					htmlMsg = htmlMsg + "</td>";
					
					htmlMsg = htmlMsg + "<td width=\"25%\">";
					htmlMsg = htmlMsg + "<table>";
					htmlMsg = htmlMsg + "<tr>";
					htmlMsg = htmlMsg + "<td>";
					htmlMsg = htmlMsg + "&nbsp;";
					htmlMsg = htmlMsg + "</td>";
					htmlMsg = htmlMsg + "</tr>";
					htmlMsg = htmlMsg + "</table>";
					htmlMsg = htmlMsg + "</td>";
					
					htmlMsg = htmlMsg + "<td width=\"25%\">";
					htmlMsg = htmlMsg + "<table width=\"100%\">";
					htmlMsg = htmlMsg + "<tr>";
					htmlMsg = htmlMsg + "<td class=\"textGoodBold\" align =\"left\" width=\"40%\">From Date : </td>";
					htmlMsg = htmlMsg + "<td class=\"textGoodBold\" width=\"60%\">" + nlapiEscapeXML(start) + "</td>";
					htmlMsg = htmlMsg + "</tr>";
					htmlMsg = htmlMsg + "<tr>";
					htmlMsg = htmlMsg + "<td class=\"textGoodBold\" align =\"left\" width=\"40%\">To Date : </td>";
					htmlMsg = htmlMsg + "<td class=\"textGoodBold\" width=\"60%\">" + nlapiEscapeXML(end) + "</td>";
					htmlMsg = htmlMsg + "</tr>";
					//htmlMsg = htmlMsg + "<tr>";
				//	htmlMsg = htmlMsg + "<td class=\"textGoodBold\" align =\"left\" width=\"40%\">Location : </td>";
					//htmlMsg = htmlMsg + "<td class=\"textGoodBold\" width=\"60%\">" + nlapiEscapeXML(locationnametext) + "</td>";
					//htmlMsg = htmlMsg + "</tr>";
					htmlMsg = htmlMsg + "<tr>";
					htmlMsg = htmlMsg + "<td class=\"textGoodBold\" align =\"left\" width=\"30%\">Currency : </td>";
					htmlMsg = htmlMsg + "<td class=\"textGoodBold\" width=\"70%\">" + nlapiEscapeXML(custCurrency) + "</td>";
					htmlMsg = htmlMsg + "</tr>";
					htmlMsg = htmlMsg + "</table>";
					htmlMsg = htmlMsg + "</td>";
					
					htmlMsg = htmlMsg + "</tr>";
					htmlMsg = htmlMsg + "</table>";
					
					//=====================================End : Print vendor statement header details========================//
					
					htmlMsg = htmlMsg + "<table align=\"center\" width=\"100%\" border =\"1px\" border-collapse = \"collapse\">";
					//htmlMsg = htmlMsg + "<tr>";
					htmlMsg = htmlMsg + "<thead style = \"display:table-header-group;\">"
					htmlMsg = htmlMsg + "<tr>";
					htmlMsg = htmlMsg + "<th style=\"border-right-width:0.1px; border-bottom-width:0.1px\" width=\"5%\" align=\"center\" colspan=\"1\" class=\"textGoodBold\"  background-color=\"#B0B0B0\">Transaction No.</th>";
					htmlMsg = htmlMsg + "<th style=\"border-right-width:0.1px; border-bottom-width:0.1px\" width=\"5%\" align=\"center\" colspan=\"1\" class=\"textGoodBold\"  background-color=\"#B0B0B0\">Date</th>";
					htmlMsg = htmlMsg + "<th style=\"border-right-width:0.1px; border-bottom-width:0.1px\" width=\"7%\" align=\"center\" colspan=\"1\" class=\"textGoodBold\"  background-color=\"#B0B0B0\">Transaction Type</th>";
					htmlMsg = htmlMsg + "<th style=\"border-right-width:0.1px; border-bottom-width:0.1px\" width=\"4%\" align=\"center\" colspan=\"1\" class=\"textGoodBold\"  background-color=\"#B0B0B0\">Currency</th>";
					htmlMsg = htmlMsg + "<th style=\"border-right-width:0.1px; border-bottom-width:0.1px\" width=\"4%\" align=\"center\" colspan=\"1\" class=\"textGoodBold\"  background-color=\"#B0B0B0\">Exchange Rate</th>";
					htmlMsg = htmlMsg + "<th style=\"border-right-width:0.1px; border-bottom-width:0.1px\" width=\"6%\" align=\"center\" colspan=\"1\" class=\"textGoodBold\"  background-color=\"#B0B0B0\">Reference No.</th>";
					htmlMsg = htmlMsg + "<th style=\"border-right-width:0.1px; border-bottom-width:0.1px\" width=\"6%\" align=\"center\" colspan=\"1\" class=\"textGoodBold\"  background-color=\"#B0B0B0\">Memo</th>";
					//htmlMsg = htmlMsg + "<th style=\"border-right-width:0.1px; border-bottom-width:0.1px\" width=\"6%\" align=\"center\" colspan=\"1\" class=\"textGoodBold\"  background-color=\"#B0B0B0\">Location</th>";
					htmlMsg = htmlMsg + "<th style=\"border-right-width:0.1px; border-bottom-width:0.1px\" width=\"6%\" align=\"center\" colspan=\"1\" class=\"textGoodBold\"  background-color=\"#B0B0B0\">Account</th>";
					htmlMsg = htmlMsg + "<th style=\"border-right-width:0.1px; border-bottom-width:0.1px\" width=\"7%\" align=\"center\" colspan=\"1\" class=\"textGoodBold\"  background-color=\"#B0B0B0\">Debit</th>";
					htmlMsg = htmlMsg + "<th style=\"border-right-width:0.1px; border-bottom-width:0.1px\" width=\"7%\" align=\"center\" colspan=\"1\" class=\"textGoodBold\"  background-color=\"#B0B0B0\">Credit</th>";
					htmlMsg = htmlMsg + "<th style=\"border-right-width:0.1px; border-bottom-width:0.1px\" width=\"9%\" align=\"center\" colspan=\"1\" class=\"textGoodBold\"  background-color=\"#B0B0B0\">Balance</th>";
					//htmlMsg = htmlMsg + "<td style=\"border-right-width:0.1px; border-bottom-width:0.1px\" width=\"7%\" align=\"center\" colspan=\"1\" class=\"textGoodBold\">Debit In Foriegn Currency</td>";
				    //htmlMsg = htmlMsg + "<td style=\"border-right-width:0.1px; border-bottom-width:0.1px\" width=\"7%\" align=\"center\" colspan=\"1\" class=\"textGoodBold\">Credit In Foriegn Currency</td>";
					//htmlMsg = htmlMsg + "<td style=\"border-right-width:0.1px; border-bottom-width:0.1px\" width=\"9%\" align=\"center\" colspan=\"1\" class=\"textGoodBold\">Balance In Foriegn Currency</td>";
					htmlMsg = htmlMsg + "<th style=\"border-bottom-width:0.1px\" width=\"5%\" align=\"center\" colspan=\"1\" class=\"textGoodBold\"  background-color=\"#B0B0B0 \">System Date<BR/></th>";
					htmlMsg = htmlMsg + "</tr>";
					htmlMsg = htmlMsg + "</thead>";
		
					var blank = '-';
					var description = 'Opening Balance';
					htmlMsg = htmlMsg + "<tr>";
					htmlMsg = htmlMsg + "<td style=\"border-right-width:0.1px; border-bottom-width:0.1px\" width=\"5%\" align=\"center\" colspan=\"1\" class=\"textGoodBold\">" +nlapiEscapeXML(blank)   + "</td>";
					htmlMsg = htmlMsg + "<td style=\"border-right-width:0.1px; border-bottom-width:0.1px\" width=\"5%\" align=\"center\" colspan=\"1\" class=\"textGoodBold\">" +  nlapiEscapeXML(openingBalanceDate)+ "</td>";
					htmlMsg = htmlMsg + "<td style=\"border-right-width:0.1px; border-bottom-width:0.1px\" width=\"21%\" align=\"center\" colspan=\"4\" class=\"textGoodBold\">" + nlapiEscapeXML(description) + "</td>";
					htmlMsg = htmlMsg + "<td style=\"border-right-width:0.1px; border-bottom-width:0.1px\" width=\"6%\" align=\"center\" colspan=\"1\" class=\"textGoodBold\">" + nlapiEscapeXML(blank) + "</td>";
					//htmlMsg = htmlMsg + "<td style=\"border-right-width:0.1px; border-bottom-width:0.1px\" width=\"6%\" align=\"center\" colspan=\"1\" class=\"textGoodBold\">" + nlapiEscapeXML(blank) + "</td>";
					htmlMsg = htmlMsg + "<td style=\"border-right-width:0.1px; border-bottom-width:0.1px\" width=\"6%\" align=\"center\" colspan=\"1\" class=\"textGoodBold\">" + nlapiEscapeXML(blank) + "</td>";
					htmlMsg = htmlMsg + "<td style=\"border-right-width:0.1px; border-bottom-width:0.1px\" width=\"7%\" align=\"center\" colspan=\"1\" class=\"textGoodBold\">" + nlapiEscapeXML(blank) + "</td>";
					htmlMsg = htmlMsg + "<td style=\"border-right-width:0.1px; border-bottom-width:0.1px\" width=\"7%\" align=\"center\" colspan=\"1\" class=\"textGoodBold\">" + nlapiEscapeXML(blank) + "</td>";
					htmlMsg = htmlMsg + "<td style=\"border-right-width:0.1px; border-bottom-width:0.1px\" width=\"9%\" align=\"center\" colspan=\"1\" class=\"textGoodBold\">" + nlapiEscapeXML(openingBalance) + "</td>";
					//htmlMsg = htmlMsg + "<td style=\"border-right-width:0.1px; border-bottom-width:0.1px\" width=\"7%\" align=\"center\" colspan=\"1\" class=\"textGoodBold\">" + nlapiEscapeXML(blank) + "</td>";
					//htmlMsg = htmlMsg + "<td style=\"border-right-width:0.1px; border-bottom-width:0.1px\" width=\"7%\" align=\"center\" colspan=\"1\" class=\"textGoodBold\">" + nlapiEscapeXML(blank) + "</td>";
					//htmlMsg = htmlMsg + "<td style=\"border-right-width:0.1px; border-bottom-width:0.1px\" width=\"9%\" align=\"center\" colspan=\"1\" class=\"textGoodBold\">" + nlapiEscapeXML(fxopeningBalance) + "</td>";
					htmlMsg = htmlMsg + "<td style=\"border-bottom-width:0.1px\" width=\"5%\" align=\"center\" colspan=\"1\" class=\"textGoodBold\">" +nlapiEscapeXML(blank) + "</td>";
					htmlMsg = htmlMsg + "</tr>";
					
					var balance = 0;
					var lastInternalID = 0;
					var totalAmount = 0;
					totalAmount = parseFloat(totalAmount);
					var debitTotal = 0;
					debitTotal = parseFloat(debitTotal);
					var creditTotal = 0;
					creditTotal = parseFloat(creditTotal);
					
					//var fxbalance = 0;
					
					//var fxtotalAmount = 0;
					//fxtotalAmount = parseFloat(fxtotalAmount);
					//var fxdebitTotal = 0;
					//fxdebitTotal = parseFloat(fxdebitTotal);
					//var fxcreditTotal = 0;
					//fxcreditTotal = parseFloat(fxcreditTotal);
					
					var recordCount = 0;
					
					var monthlydebitTotal = 0;
					monthlydebitTotal = parseFloat(monthlydebitTotal);
					var monthlycreditTotal = 0;
					monthlycreditTotal = parseFloat(monthlycreditTotal);
					//var fxmonthlydebitTotal = 0;
					//fxmonthlydebitTotal = parseFloat(fxmonthlydebitTotal);
					//var fxmonthlycreditTotal = 0;
					//fxmonthlycreditTotal = parseFloat(fxmonthlycreditTotal);
				
					var filters = new Array();
					filters.push(new nlobjSearchFilter('internalidnumber', null, 'greaterthan', lastInternalID));
					filters.push(new nlobjSearchFilter('name', null, 'is', vendor));
					filters.push(new nlobjSearchFilter('trandate', null, 'within', startdate, enddate));
					
					var context = nlapiGetContext();
					
	   				var i_subcontext = context.getFeature('SUBSIDIARIES');
					
					if (i_subcontext != false) 
					{
						filters.push(new nlobjSearchFilter('subsidiary', null, 'is', subsidiary));
					}
					if(parseInt(currency_criteria) == parseInt(1)) 
					{
						filters.push(new nlobjSearchFilter('currency', null, 'is', currency_id));
					}
					//if (locationname != '' && locationname != null) 
					//{
					//   filters.push( new nlobjSearchFilter('location', null, 'is', locationname));
					//} // END if(locationname != '' && locationname != null)
					
					var searchResults = nlapiSearchRecord('transaction', 'customsearch_vendor_statement_new', filters, null);
					
					nlapiLogExecution('DEBUG', 'searchVendorTransactions', 'openingBalance = end ');
					
					var breakFlag = 0;
					
					if (searchResults != null && searchResults != '') 
					{
						var length = searchResults.length;
						nlapiLogExecution('DEBUG', 'ScheduleTest', 'search result length = ' + length);
						
						for (var counter = 0; counter < searchResults.length; counter++) 
						{
							recordCount = recordCount + 1;
							
							var result = searchResults[counter];
							var columns = result.getAllColumns();
							var columnLen = columns.length;
							
							var internalID = '';
							var amount = '';
							var type = '';
							var date = '';
							var number = '';
							var typenumber = '';
							var memo = '';
							var previousInternalID = '';
							var exchangerate = '';
							var currency = '';
							var location = '';
							var account = '';
							var createddate='';
							
							
							for (var i = 0; i < columnLen; i++) 
							{
								var column = columns[i];
								var fieldName = column.getName();
								var value = result.getValue(column);
								
								if (fieldName == 'internalid') 
								{
									previousInternalID = lastInternalID;
								
									internalID = value;
									
								} // END if(fieldName == 'internalid')
								
								if (fieldName == 'tranid') 
								{
									number = value;
									nlapiLogExecution('DEBUG', 'ScheduleTest Number', 'number = ' + number);
									typenumber = type + ' ' + number;
									nlapiLogExecution('DEBUG', 'ScheduleTest TypeNumber', 'typenumber = ' + typenumber);
								} // END if(fieldName == 'tranid')
								
								if (fieldName == 'trandate') 
								{
									date = value;
									nlapiLogExecution('DEBUG', 'ScheduleTest Date', 'date = ' + date);
								} // END if(fieldName == 'trandate')
								
								if (fieldName == 'type') 
								{
									type = value;
									nlapiLogExecution('DEBUG', 'ScheduleTest Type', 'type = ' + type);
								} // END if(fieldName == 'type')
								
								if (fieldName == 'currency') 
								{
									currency = result.getText(column);
									currency = nlapiEscapeXML(currency);
									nlapiLogExecution('DEBUG', 'ScheduleTest currency', 'currency = ' + currency);
								} // END if(fieldName == 'currency')
								
								if (fieldName == 'exchangerate') 
								{
									exchangerate = value;
									exchangerate = nlapiEscapeXML(exchangerate);
									nlapiLogExecution('DEBUG', 'ScheduleTest exchangerate', 'exchangerate = ' + exchangerate);
								} // END if(fieldName == 'exchangerate')
							
								if (fieldName == 'memomain') 
								{
									memo = value;
									memo = nlapiEscapeXML(memo);
									nlapiLogExecution('DEBUG', 'ScheduleTest Memo', 'memo = ' + memo);
								} // END if(fieldName == 'memomain')
								
								if (fieldName == 'location') 
								{
									location = result.getText(column);
									location = nlapiEscapeXML(location);
									nlapiLogExecution('DEBUG', 'ScheduleTest Memo', 'location = ' + location);
								} // END if(fieldName == 'location')
								
								if (fieldName == 'account') 
								{
									account = result.getText(column);
									account = nlapiEscapeXML(account);
									nlapiLogExecution('DEBUG', 'ScheduleTest Number', 'account = ' + account);
									
								} // END if(fieldName == 'account')
								
								if (fieldName == 'amount') 
								{
									amount = value;
									amount = parseFloat(amount);
									amount = Math.round(amount * 100) / 100;
									nlapiLogExecution('DEBUG', 'ScheduleTest Amount', 'amount = ' + amount);
								} // END if(fieldName == 'amount')
								
								/*
if (fieldName == 'fxamount') 
								{
									fxamount = value;
									fxamount = parseFloat(fxamount);
									fxamount = Math.round(fxamount * 100) / 100;
									nlapiLogExecution('DEBUG', 'ScheduleTest Amount', 'fxamount = ' + fxamount);
								} // END if(fieldName == 'amount'
*/
								if (fieldName == 'datecreated') 
								{
									createddate = value
									createddate = nlapiEscapeXML(createddate);
									nlapiLogExecution('DEBUG', 'ScheduleTest Memo', 'createddate = ' + createddate);
								} // END if(fieldName == 'location')
								
								
								if (counter >= 900) 
								{
									if (previousInternalID != internalID) 
									{
										breakFlag = 1;
									} // END if(previousInternalID != internalID)
								} // END if(counter >= 900)
							} // END for (var i = 0; i < columnLen; i++)
							if (breakFlag == 1) 
							{
								break;
							} // END if(breakFlag == 1)
							
							var o_date_obj = nlapiStringToDate(date)
							var month = o_date_obj.getMonth();
							if(counter == 0)
							{
								var o_date_obj = nlapiStringToDate(date)
								var new_month = o_date_obj.getMonth();
								
							}
							if(month == new_month)
							{
								
							}
							else
							{
								var currentBalance = parseFloat(openingBalance) + parseFloat(totalAmount);
								currentBalance = Math.round(currentBalance * 100) / 100;
								nlapiLogExecution('DEBUG', 'ScheduleTest', 'currentBalance = ' + currentBalance);
								
								//var fxcurrentBalance = parseFloat(fxopeningBalance) + parseFloat(fxtotalAmount);
								//fxcurrentBalance = Math.round(fxcurrentBalance * 100) / 100;
								//nlapiLogExecution('DEBUG', 'ScheduleTest', 'currentBalance = ' + fxcurrentBalance);
								
								//===============================Begin Print the monthly closing Balance============================//
								
								var o_date_obj = nlapiStringToDate(date)
								var new_month = o_date_obj.getMonth();
								var blank = '-';
								var endDescription = 'Month Closing Balance';
								htmlMsg = htmlMsg + "<tr>";
								htmlMsg = htmlMsg + "<td style=\"border-right-width:0.1px; border-bottom-width:0.1px\" width=\"5%\" align=\"center\" colspan=\"1\" class=\"textGoodThin\">" + nlapiEscapeXML(blank) + "</td>";
								htmlMsg = htmlMsg + "<td style=\"border-right-width:0.1px; border-bottom-width:0.1px\" width=\"5%\" align=\"center\" colspan=\"1\" class=\"textGoodThin\">" + nlapiEscapeXML(blank) + "</td>";
								htmlMsg = htmlMsg + "<td style=\"border-right-width:0.1px; border-bottom-width:0.1px\" width=\"21%\" align=\"center\"  colspan=\"4\" class=\"textGoodBold\">" + nlapiEscapeXML(endDescription) + "</td>";
								htmlMsg = htmlMsg + "<td style=\"border-right-width:0.1px; border-bottom-width:0.1px\" width=\"6%\" align=\"center\" colspan=\"1\" class=\"textGoodThin\">" + nlapiEscapeXML(blank) + "</td>";
								//htmlMsg = htmlMsg + "<td style=\"border-right-width:0.1px; border-bottom-width:0.1px\" width=\"6%\" align=\"center\" colspan=\"1\" class=\"textGoodThin\">" + nlapiEscapeXML(blank) + "</td>";
								htmlMsg = htmlMsg + "<td style=\"border-right-width:0.1px; border-bottom-width:0.1px\" width=\"6%\" align=\"center\" colspan=\"1\" class=\"textGoodThin\">" + nlapiEscapeXML(blank) + "</td>";
								htmlMsg = htmlMsg + "<td style=\"border-right-width:0.1px; border-bottom-width:0.1px\" width=\"7%\" align=\"center\" colspan=\"1\" class=\"textGoodBold\">" + nlapiEscapeXML(-(monthlydebitTotal)) + "</td>";
								htmlMsg = htmlMsg + "<td style=\"border-right-width:0.1px; border-bottom-width:0.1px\" width=\"7%\" align=\"center\" colspan=\"1\" class=\"textGoodBold\">" + nlapiEscapeXML(monthlycreditTotal) + "</td>";
								htmlMsg = htmlMsg + "<td style=\"border-right-width:0.1px; border-bottom-width:0.1px\" width=\"9%\" align=\"center\" colspan=\"1\" class=\"textGoodBold\">" + nlapiEscapeXML(currentBalance) + "</td>";
								//htmlMsg = htmlMsg + "<td style=\"border-right-width:0.1px; border-bottom-width:0.1px\" width=\"7%\" align=\"center\" colspan=\"1\" class=\"textGoodBold\">" + nlapiEscapeXML(-(fxmonthlydebitTotal)) + "</td>";
								//htmlMsg = htmlMsg + "<td style=\"border-right-width:0.1px; border-bottom-width:0.1px\" width=\"7%\" align=\"center\" colspan=\"1\" class=\"textGoodBold\">" + nlapiEscapeXML(fxmonthlycreditTotal) + "</td>";
								//htmlMsg = htmlMsg + "<td style=\"border-right-width:0.1px; border-bottom-width:0.1px\" width=\"9%\" align=\"center\" colspan=\"1\" class=\"textGoodBold\">" + nlapiEscapeXML(fxcurrentBalance)+ "</td>";
								htmlMsg = htmlMsg + "<td style=\"border-bottom-width:0.1px\" width=\"5%\" align=\"center\" colspan=\"1\" class=\"textGoodBold\">" +  nlapiEscapeXML(blank)+ "</td>";
								htmlMsg = htmlMsg + "</tr>"
								
								//=============================End Print the monthly closing Balance ==============================//
								
								//Initializing the zero for monthly debit and credit total after print
								
								monthlydebitTotal = 0;
								monthlycreditTotal = 0;
								//fxmonthlydebitTotal = 0;
								//fxmonthlycreditTotal = 0;
							}

							totalAmount = parseFloat(totalAmount) - parseFloat(amount);
							totalAmount = Math.round(totalAmount * 100) / 100;
							nlapiLogExecution('DEBUG', 'ScheduleTest', 'current totalAmount = ' + totalAmount);
							
							//fxtotalAmount = parseFloat(fxtotalAmount) - parseFloat(fxamount);
							//fxtotalAmount = Math.round(fxtotalAmount * 100) / 100;
							//nlapiLogExecution('DEBUG', 'ScheduleTest', 'current fxtotalAmount = ' + fxtotalAmount);
							
							var blank = '-';
							htmlMsg = htmlMsg + "<tr>";
							
							if (number == '' || number == null) 
							{
								htmlMsg = htmlMsg + "<td style=\"border-right-width:0.1px; border-bottom-width:0.1px\" width=\"5%\" align=\"center\" colspan=\"1\" class=\"textGoodThin\">" + nlapiEscapeXML(blank) + "</td>";
							} // END if(number == '' || number == null)
							else 
							{
								htmlMsg = htmlMsg + "<td style=\"border-right-width:0.1px; border-bottom-width:0.1px\" width=\"5%\" align=\"justified\" colspan=\"1\" class=\"textGoodThin\">" + nlapiEscapeXML(number) + "</td>";
							} // END else
							
							htmlMsg = htmlMsg + "<td style=\"border-right-width:0.1px; border-bottom-width:0.1px\" width=\"5%\" align=\"left\" colspan=\"1\" class=\"textGoodThin\">" + nlapiEscapeXML(date) + "</td>";
							
							htmlMsg = htmlMsg + "<td style=\"border-right-width:0.1px; border-bottom-width:0.1px\" width=\"7%\" align=\"justified\" colspan=\"1\" class=\"textGoodThin\">" + nlapiEscapeXML(type) + "</td>";
							
							if (currency == '' || currency == null) 
							{
								htmlMsg = htmlMsg + "<td style=\"border-right-width:0.1px; border-bottom-width:0.1px\" width=\"4%\" align=\"center\" colspan=\"1\" class=\"textGoodThin\">" + nlapiEscapeXML(blank) + "</td>";
							} // END if(memo == '' || memo == null)
							else 
							{
								htmlMsg = htmlMsg + "<td style=\"border-right-width:0.1px; border-bottom-width:0.1px\" width=\"4%\" align=\"justified\" colspan=\"1\" class=\"textGoodThin\">" + nlapiEscapeXML(currency) + "</td>";
							} // END else
							
							if (exchangerate == '' || exchangerate == null) 
							{
								htmlMsg = htmlMsg + "<td style=\"border-right-width:0.1px; border-bottom-width:0.1px\" width=\"4%\" align=\"center\" colspan=\"1\" class=\"textGoodThin\">" + nlapiEscapeXML(blank) + "</td>";
							} // END if(memo == '' || memo == null)
							else 
							{
								htmlMsg = htmlMsg + "<td style=\"border-right-width:0.1px; border-bottom-width:0.1px\" width=\"4%\" align=\"justified\" colspan=\"1\" class=\"textGoodThin\">" + nlapiEscapeXML(exchangerate) + "</td>";
							} // END else
							
							if (number == '' || number == null) 
							{
								htmlMsg = htmlMsg + "<td style=\"border-right-width:0.1px; border-bottom-width:0.1px\" width=\"6%\" align=\"center\" colspan=\"1\" class=\"textGoodThin\">" + nlapiEscapeXML(blank) + "</td>";
							} // END if(number == '' || number == null)
							else 
							{
								htmlMsg = htmlMsg + "<td style=\"border-right-width:0.1px; border-bottom-width:0.1px\" width=\"6%\" align=\"justified\" colspan=\"1\" class=\"textGoodThin\">" + nlapiEscapeXML(number) + "</td>";
							} // END else
							
							if (memo == '' || memo == null) 
							{
								htmlMsg = htmlMsg + "<td style=\"border-right-width:0.1px; border-bottom-width:0.1px\" width=\"6%\" align=\"center\" colspan=\"1\" class=\"textGoodThin\">" + nlapiEscapeXML(blank) + "</td>";
							} // END if(memo == '' || memo == null)
							else 
							{
								htmlMsg = htmlMsg + "<td style=\"border-right-width:0.1px; border-bottom-width:0.1px\" width=\"6%\" align=\"justified\" colspan=\"1\" class=\"textGoodThin\">" + nlapiEscapeXML(memo) + "</td>";
							} // END else
							
								/*
if (location == '' || location == null) 
							{
								htmlMsg = htmlMsg + "<td style=\"border-right-width:0.1px; border-bottom-width:0.1px\" width=\"6%\" align=\"center\" colspan=\"1\" class=\"textGoodThin\">" + nlapiEscapeXML(blank) + "</td>";
							} // END if(memo == '' || memo == null)
							else 
							{
								htmlMsg = htmlMsg + "<td style=\"border-right-width:0.1px; border-bottom-width:0.1px\" width=\"6%\" align=\"justified\" colspan=\"1\" class=\"textGoodThin\">" + nlapiEscapeXML(location) + "</td>";
							} // END else
*/
							if (account == '' || account == null) 
							{
								htmlMsg = htmlMsg + "<td style=\"border-right-width:0.1px; border-bottom-width:0.1px\" width=\"6%\" align=\"center\" colspan=\"1\" class=\"textGoodThin\">" + nlapiEscapeXML(blank) + "</td>";
							} // END if(memo == '' || memo == null)
							else 
							{
								htmlMsg = htmlMsg + "<td style=\"border-right-width:0.1px; border-bottom-width:0.1px\" width=\"6%\" align=\"justified\" colspan=\"1\" class=\"textGoodThin\">" + nlapiEscapeXML(account) + "</td>";
							} // END else
							
							//if (type == 'VendBill') //changes 0n 1-06-2015
							if((type == 'VendPymt' || type == 'VendCred')|| (type == 'Journal' && parseFloat(amount) <= parseFloat(0) ))
							{
								htmlMsg = htmlMsg + "<td style=\"border-right-width:0.1px; border-bottom-width:0.1px\" width=\"7%\" align=\"right\" colspan=\"1\" class=\"textGoodThin\">" + nlapiEscapeXML(-(amount)) + "</td>";
								htmlMsg = htmlMsg + "<td style=\"border-right-width:0.1px; border-bottom-width:0.1px\" width=\"7%\" align=\"center\" colspan=\"1\" class=\"textGoodThin\">" + nlapiEscapeXML(blank) + "</td>";
								debitTotal = parseFloat(debitTotal) - parseFloat(amount);
								debitTotal = Math.round(debitTotal * 100) / 100;
								
								//====================Begin Code to get the montly debit total==============================
								if(counter == 0)
								{
								var o_date_obj = nlapiStringToDate(date)
								var debit_new_month = o_date_obj.getMonth();
								
								}
								if (month == debit_new_month) 
								{
									monthlydebitTotal = parseFloat(monthlydebitTotal) + parseFloat(amount);
									monthlydebitTotal = Math.round(monthlydebitTotal * 100) / 100;
								}
								else 
								{
									var o_date_obj = nlapiStringToDate(date)
									var debit_new_month = o_date_obj.getMonth();
									monthlydebitTotal = parseFloat(amount);
									
								}
								//====================End Code to get the montly debit total ==============================//
								
							} // END if(type == 'VendBill')
							
							//if (type == 'VendPymt' || type == 'Journal' || type == 'VendCred') //changes 0n 1-06-2015
							if((type == 'VendBill')|| (type == 'Journal' && parseFloat(amount) > parseFloat(0)))
							{
								htmlMsg = htmlMsg + "<td style=\"border-right-width:0.1px; border-bottom-width:0.1px\" width=\"7%\" align=\"center\" colspan=\"1\" class=\"textGoodThin\">" + blank + "</td>";
								htmlMsg = htmlMsg + "<td style=\"border-right-width:0.1px; border-bottom-width:0.1px\" width=\"7%\" align=\"right\" colspan=\"1\" class=\"textGoodThin\">" + nlapiEscapeXML(amount) + "</td>";
								creditTotal = parseFloat(creditTotal) + parseFloat(amount);
								creditTotal = Math.round(creditTotal * 100) / 100;
								
								//====================Begin Code to get the montly credit total==============================//
								if(counter == 0)
								{
								var o_date_obj = nlapiStringToDate(date)
								var credit_new_month = o_date_obj.getMonth();
								
								}
								if (month == credit_new_month) 
								{
									monthlycreditTotal = parseFloat(monthlycreditTotal) + parseFloat(amount);
									monthlycreditTotal = Math.round(monthlycreditTotal * 100) / 100;
								}
								else 
								{
									var o_date_obj = nlapiStringToDate(date)
									var credit_new_month = o_date_obj.getMonth();
									monthlycreditTotal = parseFloat(amount);
									
								}
								//====================End Code to get the montly credit total==============================\\
								
							} // END if(type == 'VendPymt' || type == 'Journal' || type == 'VendCred')
							
							if (counter == 0 && lastInternalID == 0) 
							{
								nlapiLogExecution('DEBUG', 'ScheduleTest', 'first time');
								nlapiLogExecution('DEBUG', 'ScheduleTest', 'openingBalance = ' + openingBalance);
								balance = parseFloat(openingBalance) - parseFloat(amount);
								balance = Math.round(balance * 100) / 100;
								nlapiLogExecution('DEBUG', 'ScheduleTest', 'balance = ' + balance);
								
								//fxbalance = parseFloat(fxopeningBalance) - parseFloat(fxamount);
								//fxbalance = Math.round(fxbalance * 100) / 100;
								//nlapiLogExecution('DEBUG', 'ScheduleTest', 'fxbalance = ' + fxbalance);
							} // END if(counter == 0 && lastInternalID == 0)
							else 
							{
								nlapiLogExecution('DEBUG', 'ScheduleTest', 'not first time');
								balance = parseFloat(balance) - parseFloat(amount);
								balance = Math.round(balance * 100) / 100;
								nlapiLogExecution('DEBUG', 'ScheduleTest', 'balance = ' + balance);
								
								//fxbalance = parseFloat(fxbalance) - parseFloat(fxamount);
								//fxbalance = Math.round(fxbalance * 100) / 100;
								//nlapiLogExecution('DEBUG', 'ScheduleTest', 'fxbalance = ' + fxbalance);
							} // END else
							
							htmlMsg = htmlMsg + "<td style=\"border-right-width:0.1px; border-bottom-width:0.1px\" width=\"9%\" align=\"right\" colspan=\"1\" class=\"textGoodThin\">" + nlapiEscapeXML(balance) + "</td>";
							
							/*
//if (type == 'VendBill') 
							if((type == 'VendPymt' || type == 'VendCred')|| (type == 'Journal' && parseFloat(amount) <= parseFloat(0) ))
							{
								
								htmlMsg = htmlMsg + "<td style=\"border-right-width:0.1px; border-bottom-width:0.1px\" width=\"7%\" align=\"right\" colspan=\"1\" class=\"textGoodThin\">" + nlapiEscapeXML(-(fxamount)) + "</td>";
								htmlMsg = htmlMsg + "<td style=\"border-right-width:0.1px; border-bottom-width:0.1px\" width=\"7%\" align=\"center\" colspan=\"1\" class=\"textGoodThin\">" + nlapiEscapeXML(blank) + "</td>";
								fxdebitTotal = parseFloat(fxdebitTotal) - parseFloat(fxamount);
								fxdebitTotal = Math.round(fxdebitTotal * 100) / 100;
								
								//====================Begin Code to get the montly fx debit total==============================\\
								if(counter == 0)
								{
								var o_date_obj = nlapiStringToDate(date)
								var fxdebit_new_month = o_date_obj.getMonth();
								
								}
								if (month == fxdebit_new_month) 
								{
									fxmonthlydebitTotal = parseFloat(fxmonthlydebitTotal) + parseFloat(amount);
									fxmonthlydebitTotal = Math.round(fxmonthlydebitTotal * 100) / 100;
								}
								else 
								{
									var o_date_obj = nlapiStringToDate(date)
									var fxdebit_new_month = o_date_obj.getMonth();
									fxmonthlydebitTotal = parseFloat(amount);
									
								}
								//====================End Code to get the montly fx debit total==============================//
							
							} // END if(type == 'VendBill')
							
							//if (type == 'VendPymt' || type == 'Journal' || type == 'VendCred') 
							if((type == 'VendBill')|| (type == 'Journal' && parseFloat(amount) > parseFloat(0)))
							{
								
								htmlMsg = htmlMsg + "<td style=\"border-right-width:0.1px; border-bottom-width:0.1px\" width=\"7%\" align=\"center\" colspan=\"1\" class=\"textGoodThin\">" + blank + "</td>";
								htmlMsg = htmlMsg + "<td style=\"border-right-width:0.1px; border-bottom-width:0.1px\" width=\"7%\" align=\"right\" colspan=\"1\" class=\"textGoodThin\">" + nlapiEscapeXML(fxamount) + "</td>";
								fxcreditTotal = parseFloat(fxcreditTotal) + parseFloat(fxamount);
								fxcreditTotal = Math.round(fxcreditTotal * 100) / 100;
								
								//====================Begin Code to get the montly fx credit total==============================//
								if(counter == 0)
								{
								var o_date_obj = nlapiStringToDate(date)
								var fxcredit_new_month = o_date_obj.getMonth();
								
								}
								if (month == fxcredit_new_month) 
								{
									fxmonthlycreditTotal = parseFloat(fxmonthlycreditTotal) + parseFloat(amount);
									fxmonthlycreditTotal = Math.round(fxmonthlycreditTotal * 100) / 100;
								}
								else 
								{
									var o_date_obj = nlapiStringToDate(date)
									var fxcredit_new_month = o_date_obj.getMonth();
									fxmonthlycreditTotal = parseFloat(amount);
									
								}
								//====================End Code to get the montly fx credit total==============================\\
								
							} // END if(type == 'VendPymt' || type == 'Journal' || type == 'VendCred')
							
							htmlMsg = htmlMsg + "<td style=\"border-right-width:0.1px; border-bottom-width:0.1px\" width=\"9%\" align=\"center\" colspan=\"1\" class=\"textGoodThin\">" + nlapiEscapeXML(fxbalance) + "</td>";

*/
							htmlMsg = htmlMsg + "<td style=\"border-bottom-width:0.1px\" width=\"5%\" align=\"right\" colspan=\"1\" class=\"textGoodThin\">" + nlapiEscapeXML(createddate) + "</td>";
							htmlMsg = htmlMsg + "</tr>";
							
							lastInternalID = internalID;
							nlapiLogExecution('DEBUG', 'ScheduleTest', 'test lastInternalID = ' + lastInternalID);
							
						}// for (var counter = 0; counter < searchResults.length; counter++)
						
					} // if(searchResults != null && searchResults != '')
					
					nlapiLogExecution('DEBUG', 'searchVendorTransactions', 'openingBalance 3 = end ');
					totalAmount = parseFloat(totalAmount);
					totalAmount = Math.round(totalAmount * 100) / 100;
					nlapiLogExecution('DEBUG', 'ScheduleTest', 'totalAmount = ' + totalAmount);
					
					//fxtotalAmount = parseFloat(fxtotalAmount);
					//fxtotalAmount = Math.round(fxtotalAmount * 100) / 100;
					//nlapiLogExecution('DEBUG', 'ScheduleTest', 'totalAmount = ' + totalAmount);
					
					nlapiLogExecution('DEBUG', 'ScheduleTest', 'recordCount = ' + recordCount);
					
					var currentBalance = parseFloat(openingBalance) + parseFloat(totalAmount);
					currentBalance = Math.round(currentBalance * 100) / 100;
					nlapiLogExecution('DEBUG', 'ScheduleTest', 'currentBalance = ' + currentBalance);
					
					//var fxcurrentBalance = parseFloat(fxopeningBalance) + parseFloat(fxtotalAmount);
					//fxcurrentBalance = Math.round(fxcurrentBalance * 100) / 100;
					//nlapiLogExecution('DEBUG', 'ScheduleTest', 'fxcurrentBalance = ' + fxcurrentBalance);
					
					var blank = '-';
					var endDescription = 'Debit Total';
					htmlMsg = htmlMsg + "<tr>";
					htmlMsg = htmlMsg + "<td style=\"border-right-width:0.1px; border-bottom-width:0.1px\" width=\"5%\" align=\"left\" colspan=\"1\" class=\"textGoodThin\">" + nlapiEscapeXML(blank)+ "</td>";
					htmlMsg = htmlMsg + "<td style=\"border-right-width:0.1px; border-bottom-width:0.1px\" width=\"5%\" align=\"left\" colspan=\"1\" class=\"textGoodThin\">" + nlapiEscapeXML(end) + "</td>";
					htmlMsg = htmlMsg + "<td style=\"border-right-width:0.1px; border-bottom-width:0.1px\" width=\"21%\" align=\"justified\"  colspan=\"4\" class=\"textGoodThin\">" + nlapiEscapeXML(endDescription) + "</td>";
					htmlMsg = htmlMsg + "<td style=\"border-right-width:0.1px; border-bottom-width:0.1px\" width=\"6%\" align=\"center\" colspan=\"1\" class=\"textGoodThin\">" + blank + "</td>";
				//	htmlMsg = htmlMsg + "<td style=\"border-right-width:0.1px; border-bottom-width:0.1px\" width=\"6%\" align=\"center\" colspan=\"1\" class=\"textGoodThin\">" + blank + "</td>";
					htmlMsg = htmlMsg + "<td style=\"border-right-width:0.1px; border-bottom-width:0.1px\" width=\"6%\" align=\"center\" colspan=\"1\" class=\"textGoodThin\">" + blank + "</td>";
					htmlMsg = htmlMsg + "<td style=\"border-right-width:0.1px; border-bottom-width:0.1px\" width=\"7%\" align=\"right\" colspan=\"1\" class=\"textGoodThin\">" + nlapiEscapeXML(debitTotal) + "</td>";
					htmlMsg = htmlMsg + "<td style=\"border-right-width:0.1px; border-bottom-width:0.1px\" width=\"7%\" align=\"center\" colspan=\"1\" class=\"textGoodThin\">" + blank + "</td>";
					htmlMsg = htmlMsg + "<td style=\"border-right-width:0.1px; border-bottom-width:0.1px\" width=\"9%\" align=\"center\" colspan=\"1\" class=\"textGoodThin\">" + blank + "</td>";
					//htmlMsg = htmlMsg + "<td style=\"border-right-width:0.1px; border-bottom-width:0.1px\" width=\"7%\" align=\"right\" colspan=\"1\" class=\"textGoodThin\">" + nlapiEscapeXML(fxdebitTotal) + "</td>";
					//htmlMsg = htmlMsg + "<td style=\"border-right-width:0.1px; border-bottom-width:0.1px\" width=\"7%\" align=\"center\" colspan=\"1\" class=\"textGoodThin\">" + blank + "</td>";
					//htmlMsg = htmlMsg + "<td style=\"border-right-width:0.1px; border-bottom-width:0.1px\" width=\"9%\" align=\"center\" colspan=\"1\" class=\"textGoodThin\">" + blank + "</td>";
					htmlMsg = htmlMsg + "<td style=\"border-bottom-width:0.1px\" width=\"5%\" align=\"center\" colspan=\"1\" class=\"textGoodThin\">" + blank + "</td>";
					
					htmlMsg = htmlMsg + "</tr>";
					
					var blank = '-';
					var endDescription = 'Credit Total';
					htmlMsg = htmlMsg + "<tr>";
					htmlMsg = htmlMsg + "<td style=\"border-right-width:0.1px; border-bottom-width:0.1px\" width=\"5%\" align=\"left\" colspan=\"1\" class=\"textGoodThin\">" + nlapiEscapeXML(blank)+ "</td>";
					htmlMsg = htmlMsg + "<td style=\"border-right-width:0.1px; border-bottom-width:0.1px\" width=\"5%\" align=\"left\" colspan=\"1\" class=\"textGoodThin\">" + nlapiEscapeXML(end) + "</td>";
					htmlMsg = htmlMsg + "<td style=\"border-right-width:0.1px; border-bottom-width:0.1px\" width=\"21%\" align=\"justified\" colspan=\"4\" class=\"textGoodThin\">" + nlapiEscapeXML(endDescription) + "</td>";
					htmlMsg = htmlMsg + "<td style=\"border-right-width:0.1px; border-bottom-width:0.1px\" width=\"6%\" align=\"center\" colspan=\"1\" class=\"textGoodThin\">" + nlapiEscapeXML(blank) + "</td>";
					//htmlMsg = htmlMsg + "<td style=\"border-right-width:0.1px; border-bottom-width:0.1px\" width=\"6%\" align=\"center\" colspan=\"1\" class=\"textGoodThin\">" + nlapiEscapeXML(blank) + "</td>";
					htmlMsg = htmlMsg + "<td style=\"border-right-width:0.1px; border-bottom-width:0.1px\" width=\"6%\" align=\"center\" colspan=\"1\" class=\"textGoodThin\">" + nlapiEscapeXML(blank) + "</td>";
					htmlMsg = htmlMsg + "<td style=\"border-right-width:0.1px; border-bottom-width:0.1px\" width=\"7%\" align=\"center\" colspan=\"1\" class=\"textGoodThin\">" + nlapiEscapeXML(blank) + "</td>";
					htmlMsg = htmlMsg + "<td style=\"border-right-width:0.1px; border-bottom-width:0.1px\" width=\"7%\" align=\"right\" colspan=\"1\" class=\"textGoodThin\">" + nlapiEscapeXML((creditTotal)) + "</td>";
					htmlMsg = htmlMsg + "<td style=\"border-right-width:0.1px; border-bottom-width:0.1px\" width=\"9%\" align=\"center\" colspan=\"1\" class=\"textGoodThin\">" + nlapiEscapeXML(blank) + "</td>";
					//htmlMsg = htmlMsg + "<td style=\"border-right-width:0.1px; border-bottom-width:0.1px\" width=\"7%\" align=\"center\" colspan=\"1\" class=\"textGoodThin\">" + nlapiEscapeXML(blank) + "</td>";
					//htmlMsg = htmlMsg + "<td style=\"border-right-width:0.1px; border-bottom-width:0.1px\" width=\"7%\" align=\"right\" colspan=\"1\" class=\"textGoodThin\">" + nlapiEscapeXML(fxcreditTotal) + "</td>";
					//htmlMsg = htmlMsg + "<td style=\"border-right-width:0.1px; border-bottom-width:0.1px\" width=\"9%\" align=\"right\" colspan=\"1\" class=\"textGoodThin\">" + nlapiEscapeXML(blank) + "</td>";
					htmlMsg = htmlMsg + "<td style=\"border-bottom-width:0.1px\" width=\"5%\" align=\"center\" colspan=\"1\" class=\"textGoodThin\">" + nlapiEscapeXML(blank) + "</td>";
					htmlMsg = htmlMsg + "</tr>";
					
					if (currentBalance < 0) 
					{
						var blank = '-';
						var endDescription = 'Closing Balance';
						var newbalance = (-(currentBalance));
						//var fxnewbalance = (-(fxcurrentBalance));
						htmlMsg = htmlMsg + "<tr>";
						htmlMsg = htmlMsg + "<td style=\"border-right-width:0.1px;\" width=\"5%\" align=\"left\" colspan=\"1\" class=\"textGoodThin\">" + nlapiEscapeXML(blank)+ "</td>";
						htmlMsg = htmlMsg + "<td style=\"border-right-width:0.1px;\"  width=\"5%\" align=\"left\" colspan=\"1\" class=\"textGoodThin\">" + nlapiEscapeXML(end) + "</td>";
						htmlMsg = htmlMsg + "<td style=\"border-right-width:0.1px;\"  width=\"21%\" align=\"justified\"  colspan=\"4\" class=\"textGoodThin\">" + nlapiEscapeXML(endDescription) + "</td>";
						htmlMsg = htmlMsg + "<td style=\"border-right-width:0.1px;\"  width=\"6%\" align=\"center\" colspan=\"1\" class=\"textGoodThin\">" + nlapiEscapeXML(blank) + "</td>";
						//htmlMsg = htmlMsg + "<td style=\"border-right-width:0.1px;\"  width=\"6%\" align=\"center\" colspan=\"1\" class=\"textGoodThin\">" + nlapiEscapeXML(blank) + "</td>";
						htmlMsg = htmlMsg + "<td style=\"border-right-width:0.1px;\"  width=\"6%\" align=\"center\" colspan=\"1\" class=\"textGoodThin\">" + nlapiEscapeXML(blank) + "</td>";
						htmlMsg = htmlMsg + "<td style=\"border-right-width:0.1px;\"  width=\"7%\" align=\"center\" colspan=\"1\" class=\"textGoodThin\">" + nlapiEscapeXML(newbalance) + "</td>";
						htmlMsg = htmlMsg + "<td style=\"border-right-width:0.1px;\"  width=\"7%\" align=\"center\" colspan=\"1\" class=\"textGoodThin\">" + nlapiEscapeXML(blank) + "</td>";
						htmlMsg = htmlMsg + "<td style=\"border-right-width:0.1px;\"  width=\"9%\" align=\"right\" colspan=\"1\" class=\"textGoodThin\">" + nlapiEscapeXML(blank) + "</td>";
						//htmlMsg = htmlMsg + "<td style=\"border-right-width:0.1px;\"  width=\"7%\" align=\"center\" colspan=\"1\" class=\"textGoodThin\">" + nlapiEscapeXML(fxnewbalance) + "</td>";
						//htmlMsg = htmlMsg + "<td style=\"border-right-width:0.1px;\"  width=\"7%\" align=\"center\" colspan=\"1\" class=\"textGoodThin\">" + nlapiEscapeXML(blank) + "</td>";
						//htmlMsg = htmlMsg + "<td style=\"border-right-width:0.1px;\"  width=\"9%\" align=\"center\" colspan=\"1\" class=\"textGoodThin\">" + nlapiEscapeXML(blank) + "</td>";
						htmlMsg = htmlMsg + "<td width=\"5%\" align=\"right\" colspan=\"1\" class=\"textGoodThin\">" + nlapiEscapeXML(blank) + "</td>";
						htmlMsg = htmlMsg + "</tr>";
					} // END if(currentBalance < 0)
					else 
						if (currentBalance > 0) 
						{
							var blank = '-';
							var endDescription = 'Closing Balance';
							htmlMsg = htmlMsg + "<tr>";
							htmlMsg = htmlMsg + "<td style=\"border-right-width:0.1px;\"  width=\"5%\" align=\"left\" colspan=\"1\" class=\"textGoodThin\">" + nlapiEscapeXML(blank)+ "</td>";
							htmlMsg = htmlMsg + "<td style=\"border-right-width:0.1px;\"  width=\"5%\" align=\"left\" colspan=\"1\" class=\"textGoodThin\">" + nlapiEscapeXML(end) + "</td>";
							htmlMsg = htmlMsg + "<td style=\"border-right-width:0.1px;\"  width=\"21%\" align=\"justified\"   colspan=\"4\" class=\"textGoodThin\">" + nlapiEscapeXML(endDescription) + "</td>";
							htmlMsg = htmlMsg + "<td style=\"border-right-width:0.1px;\"  width=\"6%\" align=\"center\" colspan=\"1\" class=\"textGoodThin\">" + nlapiEscapeXML(blank) + "</td>";
							//htmlMsg = htmlMsg + "<td style=\"border-right-width:0.1px;\"  width=\"6%\" align=\"center\" colspan=\"1\" class=\"textGoodThin\">" + nlapiEscapeXML(blank) + "</td>";
							htmlMsg = htmlMsg + "<td style=\"border-right-width:0.1px;\"  width=\"6%\" align=\"center\" colspan=\"1\" class=\"textGoodThin\">" + nlapiEscapeXML(blank) + "</td>";
							htmlMsg = htmlMsg + "<td style=\"border-right-width:0.1px;\"  width=\"7%\" align=\"center\" colspan=\"1\" class=\"textGoodThin\">" + nlapiEscapeXML(blank) + "</td>";
							htmlMsg = htmlMsg + "<td style=\"border-right-width:0.1px;\"  width=\"7%\" align=\"center\" colspan=\"1\" class=\"textGoodThin\">" + nlapiEscapeXML(currentBalance) + "</td>";
							htmlMsg = htmlMsg + "<td style=\"border-right-width:0.1px;\"  width=\"9%\" align=\"right\" colspan=\"1\" class=\"textGoodThin\">" + nlapiEscapeXML(blank) + "</td>";
							//htmlMsg = htmlMsg + "<td style=\"border-right-width:0.1px;\"  width=\"7%\" align=\"right\" colspan=\"1\" class=\"textGoodThin\">" + nlapiEscapeXML(blank) + "</td>";
							//htmlMsg = htmlMsg + "<td style=\"border-right-width:0.1px;\"  width=\"7%\" align=\"center\" colspan=\"1\" class=\"textGoodThin\">" + nlapiEscapeXML(fxcurrentBalance) + "</td>";
						    //htmlMsg = htmlMsg + "<td style=\"border-right-width:0.1px;\"  width=\"9%\" align=\"center\" colspan=\"1\" class=\"textGoodThin\">" + nlapiEscapeXML(blank) + "</td>";
							htmlMsg = htmlMsg + "<td width=\"5%\" align=\"right\" colspan=\"1\" class=\"textGoodThin\">" + nlapiEscapeXML(blank) + "</td>";
							htmlMsg = htmlMsg + "</tr>";
						} // END else if(currentBalance > 0)
						else 
						{
							var blank = '-';
							var endDescription = 'Closing Balance';
							htmlMsg = htmlMsg + "<tr>";
							htmlMsg = htmlMsg + "<td style=\"border-right-width:0.1px;\"  width=\"5%\" align=\"left\" colspan=\"1\" class=\"textGoodThin\">" + nlapiEscapeXML(blank)+ "</td>";
							htmlMsg = htmlMsg + "<td style=\"border-right-width:0.1px;\"   width=\"5%\" align=\"left\" colspan=\"1\" class=\"textGoodThin\">" + nlapiEscapeXML(end) + "</td>";
							htmlMsg = htmlMsg + "<td style=\"border-right-width:0.1px;\"   width=\"21%\" align=\"justified\"  colspan=\"4\" class=\"textGoodThin\">" + nlapiEscapeXML(endDescription) + "</td>";
							htmlMsg = htmlMsg + "<td style=\"border-right-width:0.1px;\"   width=\"6%\" align=\"center\" colspan=\"1\" class=\"textGoodThin\">" + nlapiEscapeXML(blank) + "</td>";
							//htmlMsg = htmlMsg + "<td style=\"border-right-width:0.1px;\"   width=\"6%\" align=\"center\" colspan=\"1\" class=\"textGoodThin\">" + nlapiEscapeXML(blank) + "</td>";
							htmlMsg = htmlMsg + "<td style=\"border-right-width:0.1px;\"   width=\"6%\" align=\"center\" colspan=\"1\" class=\"textGoodThin\">" + nlapiEscapeXML(blank) + "</td>";
							htmlMsg = htmlMsg + "<td style=\"border-right-width:0.1px;\"   width=\"7%\" align=\"center\" colspan=\"1\" class=\"textGoodThin\">" + nlapiEscapeXML(blank) + "</td>";
							htmlMsg = htmlMsg + "<td style=\"border-right-width:0.1px;\"   width=\"7%\" align=\"center\" colspan=\"1\" class=\"textGoodThin\">" + nlapiEscapeXML(blank) + "</td>";
							htmlMsg = htmlMsg + "<td style=\"border-right-width:0.1px;\"   width=\"9%\" align=\"right\" colspan=\"1\" class=\"textGoodThin\">" + nlapiEscapeXML(currentBalance) + "</td>";
							//htmlMsg = htmlMsg + "<td style=\"border-right-width:0.1px;\"   width=\"7%\" align=\"center\" colspan=\"1\" class=\"textGoodThin\">" + nlapiEscapeXML(blank) + "</td>";
							//htmlMsg = htmlMsg + "<td style=\"border-right-width:0.1px;\"   width=\"7%\" align=\"center\" colspan=\"1\" class=\"textGoodThin\">" + nlapiEscapeXML(blank) + "</td>";
							//htmlMsg = htmlMsg + "<td style=\"border-right-width:0.1px;\"   width=\"9%\" align=\"center\" colspan=\"1\" class=\"textGoodThin\">" + nlapiEscapeXML(fxcurrentBalance) + "</td>";
							htmlMsg = htmlMsg + "<td width=\"5%\" align=\"right\" colspan=\"1\" class=\"textGoodThin\">" + nlapiEscapeXML(blank) + "</td>";
							htmlMsg = htmlMsg + "</tr>";
						} // END else
					htmlMsg = htmlMsg + "</table>";
					
					//htmlMsg = htmlMsg + "</body>";
					htmlMsg = htmlMsg + "";
				}
			}
		}
		
		var fileID = XMLConversion(htmlMsg,recepient)
		
		venstatobj.setFieldValue('custrecord_ait_vendorstatement',fileID);
		venstatobj.setFieldValue('custrecord_ait_venstat_status','completed');
		
		var venStatID = nlapiSubmitRecord(venstatobj);
		nlapiLogExecution('DEBUG', 'generate statemnet', 'vendor Statement ID->' + venStatID)
		
		var context = nlapiGetContext();
		var usageRemaining = context.getRemainingUsage();
		nlapiLogExecution('DEBUG', 'usage', 'usageRemaining obj->' + usageRemaining)
		
}
// END SCHEDULED FUNCTION ===============================================


// BEGIN FUNCTION ===================================================

function getOpeningBalance(vendor, locationname, custCreatedDate, opBalanceEndDate,subsidiary)
{
	
	//====== CODE FOR GETTIMNG OPENING BALANCE ======
	var lastInternalID = 0;
	var totalAmount = 0;
	//var totalfxamount = 0;
	totalAmount = parseFloat(totalAmount);
	//totalfxamount = parseFloat(totalfxamount);
		var filters = new Array();
		
		filters.push( new nlobjSearchFilter('internalidnumber', null, 'greaterthan', lastInternalID));
		filters.push( new nlobjSearchFilter('name', null, 'is', vendor));
		
		filters.push( new nlobjSearchFilter('trandate', null, 'within', custCreatedDate, opBalanceEndDate));
		
		if(locationname != '' && locationname != null)
		{
			//filters.push( new nlobjSearchFilter('location', null, 'is', locationname));
		} // END if(locationname != '' && locationname != null)
		
		var context = nlapiGetContext();
	    var i_subcontext = context.getFeature('SUBSIDIARIES');
		if (i_subcontext != false) 
		{
			filters.push(new nlobjSearchFilter('subsidiary', null, 'is', subsidiary));
		}
		
		var searchResults = nlapiSearchRecord('transaction', 'customsearch_vendor_statement_new', filters, null);

		var breakFlag = 0;
		if(searchResults != null && searchResults != '')
		{
			var length = searchResults.length;
			nlapiLogExecution('DEBUG', 'getOpeningBalance', 'search result length = ' +length);

			for (var counter = 0; counter < searchResults.length; counter++)
			{
				nlapiLogExecution('DEBUG', 'getOpeningBalance', 'counter for begin= ' +counter);
				var result = searchResults[counter];
				var columns = result.getAllColumns();
				var columnLen = columns.length;

				var internalID = '';
				var amount = '';
				//var fxamount = '';
				var type = '';
				var previousInternalID = '';

				for (var i = 0; i < columnLen; i++)
				{
					var column = columns[i];
					var fieldName = column.getName();
					var value = result.getValue(column);

					if(fieldName == 'internalid')
					{
						previousInternalID = lastInternalID;
						nlapiLogExecution('DEBUG', 'getOpeningBalance', 'previousInternalID = ' +previousInternalID);
						internalID = value;
						nlapiLogExecution('DEBUG', 'getOpeningBalance', 'internalID = ' +internalID);
					}

					if(fieldName == 'amount')
					{
						amount = value;
						amount = parseFloat(amount);
						amount = Math.round(amount*100)/100;
					}
					
					/*
					if(fieldName == 'fxamount')
					{
						fxamount = value;
						fxamount = parseFloat(fxamount);
						fxamount = Math.round(fxamount*100)/100;
					}

*/

					if(fieldName == 'type')
					{
						type = value;
					}
					if(counter >= 900)
					{
						if(previousInternalID != internalID)
						{
							breakFlag = 1;
						}
					}
				}
				if(breakFlag == 1)
				{
					break;
				}
				lastInternalID = internalID;

				totalAmount = parseFloat(totalAmount) - parseFloat(amount);
				totalAmount = Math.round(totalAmount*100)/100;
				
				//totalfxamount = parseFloat(totalfxamount) - parseFloat(fxamount);
				//totalfxamount = Math.round(totalfxamount*100)/100;
				
			}// END for (var counter = 0; counter < searchResults.length; counter++)
			
		} // END if(searchResults != null && searchResults != '')
	totalAmount = parseFloat(totalAmount);
	totalAmount = Math.round(totalAmount*100)/100;
	
	var getOpeningBalancefunction = parseFloat(totalAmount);
	
	nlapiLogExecution('DEBUG', 'getOpeningBalance', 'getOpeningBalancefunction = ' +getOpeningBalancefunction);
	
	//totalfxamount = parseFloat(totalfxamount);
	//totalfxamount = Math.round(totalfxamount*100)/100;
	
	//nlapiLogExecution('DEBUG', 'getOpeningBalance', 'getOpeningBalancefunction fx amount = ' +totalfxamount);
	
	//return_amount = totalAmount+'#'+totalfxamount
	
	return totalAmount;
} // END getOpeningBalance(vendor, locationname, custCreatedDate, opBalanceEndDate)

function GetCurrentTimeStamp()
{
	//====CODE TO GET CURRENT TIME STAMP ===========
	return (new Date().getTime());
} // END GetCurrentTimeStamp()

function replaceCommaWithSemiColon(data)
{
	//==== CODE FOR REPLACING COMMA WITH SEMICOLON ======
	if(data != null && data != '')
	{
		var numberOfCommas = 0;
		var lengthData = data.length;
		for(var n = 0; n < lengthData; n++)
		{
			var currentChar = data.charAt(n);
			if(currentChar == ",")
			numberOfCommas = numberOfCommas + 1;
		}
		var trimmedData = data;
	    for(var m = 0; m < numberOfCommas; m++)
	    {
	    	var trim = trimmedData.replace(",",";");
	    	trimmedData = trim;
	    }
	}
    return trimmedData;
} // END replaceCommaWithSemiColon(data)

	function afterReplaceAddComma(custAddr)
{
    
	/// ====== CODE TO ADD COMMA AFTER REPLACE ====
	var custAddrString = custAddr.toString();

    custAddrString = custAddrString.replace(/\r/g, '-');/// /g
    custAddrString = custAddrString.replace(/\n/g, '--');/// /g

    return custAddrString;
} // END afterReplaceAddComma(custAddr)

	function getLocationName(locationname)
{
	//======= CODE TO GET LOCATION NAME  ==========
	var Filters = new Array();
	var Columns = new Array();
	
	Filters.push( new nlobjSearchFilter('internalid', null, 'is', locationname));
	Columns.push( new nlobjSearchColumn('internalid'));
	Columns.push( new nlobjSearchColumn('name'));

	var SearchResults = nlapiSearchRecord('location', null, Filters, Columns);
	
	if(SearchResults != null)
	{
		var InternalID = SearchResults[0].getValue('internalid');
		var LocName = SearchResults[0].getValue('name');
		nlapiLogExecution('DEBUG', 'getOpeningBalance', 'InternalID = ' +InternalID);
		nlapiLogExecution('DEBUG', 'getOpeningBalance', 'LocName = ' +LocName);
		return LocName;
	}
}
	
function afterReplaceCR(custAddr)
{
    //======= CODE TO REPLACE SPECIAL SYMBOL ===
	var custAddrString = custAddr.toString();

    custAddrString = custAddrString.replace(/\r/g, '<BR/>');/// /g

    return custAddrString;

} // END afterReplaceCR(custAddr)


function GetHeadFmt()
{
    
	//===== CODE TO GET THE HEADER FORMAT ========
	var format = "<head>" +
    "<style>" +
    ".textreg { font-family:Arial, Helvetica; font-size: 8pt; }" +
    ".linetextreg { font-family:Arial, Helvetica; font-size: 8pt; border-bottom: 1px dotted;}" +
    ".textregTopBorder { font-family:Arial, Helvetica; font-size: 9.5pt; border-top:solid; }" +
    ".textregBoldTopBorder { font-family:Arial, Helvetica; font-size: 9.5pt; font-weight: bold; border-top:solid; }" +
    ".textregbold { font-family: Arial,Helvetica; font-size: 8pt; font-weight: bold;}" +
    ".tabletextregbold { font-family: Arial,Helvetica; font-size: 8pt; font-weight: bold;background-color: #FFCCCC}" +
    ".textHeaderBold { font-family:Arial, Helvetica; font-size: 12pt; font-weight: bold;}" +
    ".textLargeBold { font-family:Arial, Helvetica; font-size: 10.5pt; font-weight: bold; text-align:center;}" +
    ".textGoodBold { font-family:Arial, Helvetica; font-size: 9.5pt; font-weight: bold;}" +
    ".textGoodThin { font-family:Arial, Helvetica; font-size: 9pt;}" +
    ".textVeryLargeBold { font-family:Arial, Helvetica; font-size: 18pt; font-weight: bold;}" +
    ".textregSmall { font-family:Arial, Helvetica; font-size: 8pt; }" +
    ".texttitle { font-family: Arial, Helvetica; font-size: 16pt ; font-weight: bold; font-style:italic; }" +
    ".textprojtitle { font-family: Arial, Helvetica; font-size: 11pt; font-weight: bold; }" +
    ".textregAddr { font-family:Arial, Helvetica; font-size: 7pt; }" +
    ".footer, .push {height: 6em;}" +
    ".P { margin-top: 10pt; margin-bottom:10pt; margin-left:0pt; margin-right:0pt; }" +
    "</style>" +
    "</head>";

    return format;
} // END GetHeadFmt()
function XMLConversion(htmlMsg,recepient)
{
    var set_date = new Date();
	var offset =  (3600000*(+5.50+7));
	var current_date_time =  new Date(set_date.getTime() + offset)
	var o_current_date_time_obj = current_date_time.toString();
	var display_date = o_current_date_time_obj.substring(0, 25)
	nlapiLogExecution('DEBUG', 'Script Scheduled ', ' Script Status 2-->' + new Date(set_date.getTime() + offset));
	
	//==== CODE FOR HTML TO XML CONVERSION =====
	
    var xml = "<?xml version=\"1.0\"?>\n<!DOCTYPE pdf PUBLIC \"-//big.faceless.org//report\" \"report-1.1.dtd\">\n";
    xml += "<pdf lang=\"ru-RU\" xml:lang=\"ru-RU\">\n";
	xml += "<head>";
    xml += "  <style>";
	xml +=".textLargeBold { font-family:Arial, Helvetica; font-size: 10.5pt; font-weight: bold; text-align:center;}" +
    ".textGoodBold { font-family:Arial, Helvetica; font-size: 9.5pt; font-weight: bold;}" +
    ".textGoodThin { font-family:Arial, Helvetica; font-size: 9pt;}" +
    ".textVeryLargeBold { font-family:Arial, Helvetica; font-size: 18pt; font-weight: bold;}" 
   
    xml += "  <\/style>";
    xml += "  <macrolist>";  
    xml += " <macro id=\"myfooter\">";
    xml += "      <p align=\"right\" font-size=\"12\">";
	xml += display_date 
    xml += "<br/><pagenumber\/> of <totalpages\/>"
    xml += "      <\/p>";
    xml += "    <\/macro>"; 
    xml += "  <\/macrolist>";
    xml += "<\/head>";
	xml += "<body margin-top=\"0pt\" footer=\"myfooter\" footer-height=\"2em\" size=\"A4-landscape\">";
   	xml += htmlMsg;
	xml += "</body>\n</pdf>";
	var file = nlapiXMLToPDF(xml);
	file.setName('Vendor Statement'+display_date+'.pdf')
	var folder_id = get_folderID();
	file.setFolder(folder_id);
	file.setIsOnline(true);
	var file_id = nlapiSubmitFile(file);

	var user = nlapiGetUser();
	
	if(recepient != null && recepient != '' && recepient != undefined)
	{
		var mail = nlapiSendEmail(user, recepient, 'Vendor Statment', 'Vendor statment', null, null, null, file, null, null)
		nlapiLogExecution('DEBUG', 'Script Scheduled ', ' mail Status -->' + mail);
	}
	//response.write( file.getValue());
	return file_id;
} // END XMLConversion(html)
function getcompanyinfo()
{
	var  i_internalid = '';
	var company_filters = new Array();
	var company_column = new Array();
	company_column.push( new nlobjSearchColumn('internalid'));    
	
	var company_results = nlapiSearchRecord('customrecord_taxcompanyinfo', null, company_filters, company_column);
	if (company_results != null && company_results != '' && company_results != undefined) 
	{
		i_internalid = company_results[0].getValue('internalid')
		nlapiLogExecution('DEBUG', 'company info record', 'internalid->' + i_internalid)
	}
	return i_internalid;
}
function get_folderID()
{
			var folder_filters = new Array();
			folder_filters.push(new nlobjSearchFilter('name', null, 'is', 'AIT Vendor Statment'));
	
			var folder_column = new Array();
			folder_column.push( new nlobjSearchColumn('internalid'));    
	
			var folder_results = nlapiSearchRecord('folder', null, folder_filters, folder_column);
			if (folder_results != null && folder_results != '' && folder_results != undefined) 
			{
				var folderId = folder_results[0].getId();
				
			}
			else
			{
				var o_folder_obj = nlapiCreateRecord('folder')
				o_folder_obj.setFieldValue('name','AIT Vendor Statment')
				var folderId = nlapiSubmitRecord(o_folder_obj)
				
			}
			return folderId;
		
	
}
function getFirstTransactionDate(vendor)
{
	var first_trandate = '';
	var date_filters = new Array();
	date_filters.push(new nlobjSearchFilter('internalid', null, 'is', vendor));
	
	var date_column = new Array();
	date_column[0] = new nlobjSearchColumn('trandate','transaction');
	date_column[0].setSort(false);
	var date_results = nlapiSearchRecord('vendor', null, date_filters, date_column);
	if (date_results != null && date_results != '' && date_results != undefined) 
	{
		first_trandate = date_results[0].getValue(date_column[0]);
		
	}
	return first_trandate;
}
function logvalidation(name)
{
	if(name != null && name != '' && name != undefined)
	{
		name = name
	}
	else
	{
		name = ''
	}
	return name;
}
// END FUNCTION =====================================================
