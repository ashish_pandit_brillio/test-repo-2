// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
/*
   	Script Name: SUT_Fetch_Values
	Author:
	Company:
	Date:
	Description:


	Script Modification Log:

	-- Date --			-- Modified By --				--Requested By--				-- Description --



Below is a summary of the process controls enforced by this script file.  The control logic is described
more fully, below, in the appropriate function headers and code blocks.


     SUITELET
		- suiteletFunction(request, response)


     SUB-FUNCTIONS
		- The following sub-functions are called by the above core functions in order to maintain code
            modularization:

               - NOT USED

*/
}
// END SCRIPT DESCRIPTION BLOCK  ====================================



// BEGIN GLOBAL VARIABLE BLOCK  =====================================
{
	//  Initialize any Global Variables, in particular, debugging variables...




}
// END GLOBAL VARIABLE BLOCK  =======================================





// BEGIN SUITELET ==================================================

function SUT_Fetch_Values(request, response) //
{
	var i_status;
	try //
	{
		if (request.getMethod() == 'GET') //
		{
			var i_project = request.getParameter('custscript_pro_id');
			var i_req_type = request.getParameter('custscript_req_type');
			
			//nlapiLogExecution('DEBUG', ' suiteletFunction', ' Project  -->' + i_project);
			nlapiLogExecution('DEBUG', ' suiteletFunction', ' i_req_type  -->' + i_req_type);
			
			if (_validate(i_project)) //
			{
				if (i_req_type == 'emp_type') //
				{
					var o_employee_OBJ = nlapiLoadRecord('employee', i_project);
					
					if (o_employee_OBJ != null && o_employee_OBJ != '' && o_employee_OBJ != undefined) //
					{
						//nlapiLogExecution('DEBUG', ' suiteletFunction', 'Project found !!!');
						response.write(o_employee_OBJ.getFieldValue('custentity_persontype'));
					}
					else //
					{
						response.write('undefined');
					}
				}
				
				if (i_req_type == 'project') //
				{
					var o_projectOBJ = nlapiLoadRecord('job', i_project);
					
					if (o_projectOBJ != null && o_projectOBJ != '' && o_projectOBJ != undefined) //
					{
						//nlapiLogExecution('DEBUG', ' suiteletFunction', 'Project found !!!');
						response.write(o_projectOBJ);
					}
					else //
					{
						response.write('undefined');
					}
				}
				
				if (i_req_type == 'customer_id') //
				{
					var o_projectOBJ = nlapiLoadRecord('job', i_project);
					
					if (o_projectOBJ != null && o_projectOBJ != '' && o_projectOBJ != undefined) //
					{
						var temp = o_projectOBJ.getFieldValue('parent');
						
						if (_validate(temp)) //
						{
							response.write(temp);
						}
						else //
						{
							response.write('undefined');
						}
					}
					else //
					{
						response.write('undefined');
					}
				}
				
				if (i_req_type == 'project_details') //
				{
					var o_projectOBJ = nlapiLoadRecord('job', i_project);
					
					if (o_projectOBJ != null && o_projectOBJ != '' && o_projectOBJ != undefined) //
					{
						var temp_1 = o_projectOBJ.getFieldValue('custentity_hoursperday');
						var temp_2 = o_projectOBJ.getFieldValue('custentity_hoursperweek');
						var temp_3 = o_projectOBJ.getFieldValue('custentity_otapplicable');
						
						temp_1 = i_project + '$' + temp_1 + '$' + temp_2 + '$' + temp_3;
						
						if (_validate(temp_1)) //
						{
							response.write(temp_1);
						}
						else //
						{
							response.write('undefined');
						}
					}
					else //
					{
						response.write('undefined');
					}
				}
				
				if (i_req_type == 'custentity_otapplicable') //
				{
					var o_projectOBJ = nlapiLoadRecord('job', i_project);
					
					if (o_projectOBJ != null && o_projectOBJ != '' && o_projectOBJ != undefined) //
					{
						var temp = o_projectOBJ.getFieldValue('custentity_otapplicable');
						
						if (_validate(temp)) //
						{
							response.write(temp);
						}
						else //
						{
							response.write('undefined');
						}
						
					}
					else //
					{
						response.write('undefined');
					}
				}
				
				if (i_req_type == 'custentity_hoursperday') //
				{
					var o_projectOBJ = nlapiLoadRecord('job', i_project);
					
					if (o_projectOBJ != null && o_projectOBJ != '' && o_projectOBJ != undefined) //
					{
						var temp = (o_projectOBJ.getFieldValue('custentity_hoursperday'));
						
						if (_validate(temp)) //
						{
							response.write(temp);
						}
						else //
						{
							response.write('undefined');
						}
						
					}
					else //
					{
						response.write('undefined');
					}
				}
				
				if (i_req_type == 'custentity_hoursperweek') //
				{
					var o_projectOBJ = nlapiLoadRecord('job', i_project);
					
					if (o_projectOBJ != null && o_projectOBJ != '' && o_projectOBJ != undefined) //
					{
						var temp = (o_projectOBJ.getFieldValue('custentity_hoursperweek'));
						
						if (_validate(temp)) //
						{
							response.write(temp);
						}
						else //
						{
							response.write('undefined');
						}
						
					}
					else //
					{
						response.write('undefined');
					}
				}
				
				if (i_req_type == 'companyname') //
				{
					var o_projectOBJ = nlapiLoadRecord('job', i_project);
					
					if (o_projectOBJ != null && o_projectOBJ != '' && o_projectOBJ != undefined) //
					{
						var temp = (o_projectOBJ.getFieldValue('companyname'));
						
						if (_validate(temp)) //
						{
							response.write(temp);
						}
						else //
						{
							response.write('undefined');
						}
						
					}
					else //
					{
						response.write('undefined');
					}
				}
				
				if (i_req_type == 'jobtype') //
				{
					var o_projectOBJ = nlapiLoadRecord('job', i_project);
					
					if (o_projectOBJ != null && o_projectOBJ != '' && o_projectOBJ != undefined) //
					{
						var temp = (o_projectOBJ.getFieldValue('jobtype'));
						
						if (_validate(temp)) //
						{
							response.write(temp);
						}
						else //
						{
							response.write('undefined');
						}
					}
					else //
					{
						response.write('undefined');
					}
				}
				
				if (i_req_type == 'fh_task') //
				{
					var fh_task = get_FH_Task_ID(i_project);
					//nlapiLogExecution('DEBUG', ' suiteletFunction', 'fh_task : ' + fh_task);
					
					if (_validate(fh_task)) //
					{
						response.write(fh_task);
					}
					else {
						response.write('undefined');
					}
					
				}
				
				if (i_req_type == 'st_task') //
				{
					var st_task = get_ST_Task_ID(i_project);
					//nlapiLogExecution('DEBUG', ' suiteletFunction', 'st_task : ' + st_task);
					
					if (_validate(st_task)) //
					{
						response.write(st_task);
					}
					else {
						response.write('undefined');
					}
					
				}
				
				if (i_req_type == 'ot_task') //
				{
					var ot_task = get_OT_Task_ID(i_project);
					//nlapiLogExecution('DEBUG', ' suiteletFunction', 'ot_task : ' + ot_task);
					
					if (_validate(ot_task)) //
					{
						response.write(ot_task);
					}
					else {
						response.write('undefined');
					}
					
				}
				
				if (i_req_type == 'lv_task') //
				{
					var lv_task = get_Leave_Task_ID(i_project);
					//nlapiLogExecution('DEBUG', ' suiteletFunction', 'lv_task : ' + lv_task);
					
					if (_validate(lv_task)) //
					{
						response.write(lv_task);
					}
					else //
					{
						response.write('undefined');
					}
					
				}
				
				if (i_req_type == 'hd_task') //
				{
					var hd_task = get_Holiday_Task_ID(i_project);
					//nlapiLogExecution('DEBUG', ' suiteletFunction', 'hd_task : ' + hd_task);
					
					if (_validate(hd_task)) //
					{
						response.write(hd_task);
					}
					else //
					{
						response.write('undefined');
					}
					
					
				}
			}
			else //
			{
				nlapiLogExecution('ERROR', ' suiteletFunction', 'Project is not specified : ' + i_project);
			}
			
		}//GET		
	} 
	catch (exception) //
	{
		nlapiLogExecution('ERROR', 'ERROR', ' Exception Caught -->' + exception);
	}
	
	//response.write(i_status);
	
}

// END SUITELET ====================================================

function _validate(obj) //
{
    if (obj != null && obj != '' && obj != 'undefined' && obj != undefined)//
    {
        return true;
    }
    else //
    {
        return false;
    }
}

function get_ST_Task_ID(projectID) // search for ST task in project task list
{
    var filters = new Array();
    filters[filters.length] = new nlobjSearchFilter('internalid', 'job', 'anyof', projectID);
    
    // In case of any issue, please take a look at saved search "customsearch_task_search_for_st_task"
    // And confirm that Task names of "OT", "Leave" and "Holiday" has not been change in NetSuite 
    // You can check this by analyzing result if the saved search
    
    var search_result = nlapiSearchRecord('projecttask', 'customsearch_task_search_for_st_task', filters, null);
    
    if (_validate(search_result)) //
    {
        ////nlapiLogExecution('DEBUG', 'get_Task_ID', 'search_result.length : ' + search_result.length);
        
        for (var i = 0; i < search_result.length; i++) //
        {
            var result = search_result[i];
            var all_columns = result.getAllColumns();
            
            var st_task_ID = result.getValue(all_columns[0]);
            ////nlapiLogExecution('DEBUG', 'get_Task_ID', 'st_task_ID : ' + st_task_ID);
            
            return st_task_ID;
        }
    }
    return null;
}

function get_Holiday_Task_ID(projectID) // search for Holiday task in project task list
{
    var filters = new Array();
    filters[filters.length] = new nlobjSearchFilter('internalid', 'job', 'anyof', projectID);
    
    // In case of any issue, please take a look at saved search "customsearch_task_search_holiday_task"
    // And confirm that Task names of "Holiday" has not been change in NetSuite 
    // You can check this by analyzing result if the saved search
    
    var search_result = nlapiSearchRecord('projecttask', 'customsearch_task_search_holiday_task', filters, null);
    
    if (_validate(search_result)) //
    {
        ////nlapiLogExecution('DEBUG', 'get_Holiday_Task_ID', 'search_result.length : ' + search_result.length);
        
        for (var i = 0; i < search_result.length; i++) //
        {
            var result = search_result[i];
            var all_columns = result.getAllColumns();
            
            var hday_task_ID = result.getValue(all_columns[0]);
            ////nlapiLogExecution('DEBUG', 'get_Holiday_Task_ID', 'hday_task_ID : ' + hday_task_ID);
            
            return hday_task_ID;
        }
    }
    return null;
}

function get_FH_Task_ID(projectID) // search for Holiday task in project task list
{
    var filters = new Array();
    filters[filters.length] = new nlobjSearchFilter('internalid', 'job', 'anyof', projectID);
    
    // In case of any issue, please take a look at saved search "customsearch_task_search_for_fh_task"
    // And confirm that Task names of "Floating Holiday" has not been change in NetSuite 
    // You can check this by analyzing result if the saved search
    
    var search_result = nlapiSearchRecord('projecttask', 'customsearch_task_search_for_fh_task', filters, null);
    
    if (_validate(search_result)) //
    {
        ////nlapiLogExecution('DEBUG', 'get_FH_Task_ID', 'search_result.length : ' + search_result.length);
        
        for (var i = 0; i < search_result.length; i++) //
        {
            var result = search_result[i];
            var all_columns = result.getAllColumns();
            
            var fh_task_ID = result.getValue(all_columns[0]);
            ////nlapiLogExecution('DEBUG', 'get_FH_Task_ID', 'fh_task_ID : ' + fh_task_ID);
            
            return fh_task_ID;
        }
    }
    return null;
}

function get_Leave_Task_ID(projectID) // search for Holiday task in project task list
{
    var filters = new Array();
    filters[filters.length] = new nlobjSearchFilter('internalid', 'job', 'anyof', projectID);
    
    // In case of any issue, please take a look at saved search "customsearch_task_search_for_leave_task"
    // And confirm that Task names of "Leave" has not been change in NetSuite 
    // You can check this by analyzing result if the saved search
    
    var search_result = nlapiSearchRecord('projecttask', 'customsearch_task_search_for_leave_task', filters, null);
    
    if (_validate(search_result)) //
    {
        ////nlapiLogExecution('DEBUG', 'get_Leave_Task_ID', 'search_result.length : ' + search_result.length);
        
        for (var i = 0; i < search_result.length; i++) //
        {
            var result = search_result[i];
            var all_columns = result.getAllColumns();
            
            var leave_task_ID = result.getValue(all_columns[0]);
            ////nlapiLogExecution('DEBUG', 'get_Leave_Task_ID', 'leave_task_ID : ' + leave_task_ID);
            
            return leave_task_ID;
        }
    }
    return null;
}

function get_OT_Task_ID(projectID) // search for Holiday task in project task list
{
	var filters = new Array();
	filters[filters.length] = new nlobjSearchFilter('internalid', 'job', 'anyof', projectID);
	
	// In case of any issue, please take a look at saved search "customsearch_task_search_for_ot_task"
	// And confirm that Task names of "Leave" has not been change in NetSuite 
	// You can check this by analyzing result if the saved search
	
	var search_result = nlapiSearchRecord('projecttask', 'customsearch_task_search_for_ot_task', filters, null);
	
	if (_validate(search_result)) //
	{
		////nlapiLogExecution('DEBUG', 'get_Leave_Task_ID', 'search_result.length : ' + search_result.length);
		
		for (var i = 0; i < search_result.length; i++) //
		{
			var result = search_result[i];
			var all_columns = result.getAllColumns();
			
			var leave_task_ID = result.getValue(all_columns[0]);
            ////nlapiLogExecution('DEBUG', 'get_Leave_Task_ID', 'leave_task_ID : ' + leave_task_ID);
			
			return leave_task_ID;
		}
	}
	return null;
}

// BEGIN OBJECT CALLED/INVOKING FUNCTION ===================================================
{



}
// END OBJECT CALLED/INVOKING FUNCTION =====================================================