/**
 * @author Jayesh
 */

function suitelet(request, response) {
	try {
		var userId = nlapiGetUser();
		
		var requestedProject = request.getParameterValues('custpage_project');
		//var requestedCustomer = request.getParameterValues('custpage_customer');
		//var requestedRegion = request.getParameterValues('custpage_region');
		
		var requestedInrExchangeRate = request.getParameter('custpage_exchange_rate_inr');
		var requestedGbpExchangeRate = request.getParameter('custpage_exchange_rate_gbp');
		
		var formTitle = 'Revenue Projection For FP';

		// Form
		var form = nlapiCreateForm(formTitle);
		form.addFieldGroup('custpage_1', 'Select Options');

		// Project Field
		var projectField = form.addField('custpage_project', 'select','Project', null, 'custpage_1');
		projectField.addSelectOption('', ' ');
		var taggedProjects = getTaggedProjectList();
		taggedProjects.forEach(function(project) {
			var projectName = project.getValue('entityid') + " : "
			        + project.getValue('altname');
			projectField.addSelectOption(project.getId(), projectName);
		});
		if (requestedProject) {
			projectField.setDefaultValue('');
		}
		
		// Currency Field for INR
		var inrField = form.addField('custpage_exchange_rate_inr', 'currency',
		        'USD to INR ( 1 USD = x INR )', null, 'custpage_1');
		inrField.setBreakType('startcol');
		inrField.setDefaultValue('');
		
		if (requestedInrExchangeRate) {
			inrField.setDefaultValue(requestedInrExchangeRate);
		}
		
		// Currency Field For GBP
		var gbpField = form.addField('custpage_exchange_rate_gbp', 'currency',
		        'USD to GBP ( 1 USD = x GBP )', null, 'custpage_1');
		//gbpField.setBreakType('startcol');
		gbpField.setDefaultValue('');
		
		if (requestedGbpExchangeRate) {
			gbpField.setDefaultValue(requestedGbpExchangeRate);
		}
		
		//nlapiLogExecution('audit','gbp:- ',requestedGbpExchangeRate);
		var requestedExchangeRateTable = {
		    USD : 1,
		    INR : parseFloat(requestedInrExchangeRate),
		    GBP : parseFloat(requestedGbpExchangeRate)
		};
		
		// Select Options field group end
		
		form.addSubmitButton('Load Data');
			
		nlapiLogExecution('audit','pro selctd:- ',JSON.stringify(requestedProject));
		var tableHtml = createProjectionTable(requestedProject,requestedExchangeRateTable);
		var file_obj = generate_excel(tableHtml);
		response.setContentType('XMLDOC','FP Revenue Data.xls');
		response.write( file_obj.getValue());
		
		//form.addFieldGroup('custpage_2', 'Revenue Data');
		//form.addField('custpage_table', 'inlinehtml', null, null, 'custpage_2').setDefaultValue(tableHtml);
		//response.writePage(form);
		
	} catch (err) {
		nlapiLogExecution('ERROR', 'suitelet', err);
		displayErrorForm(err, "Revenue Projection");
	}
}

function getTaggedProjectList() {
	try {
		
		var project_search_results = searchRecord('job', '1517', null,
		        [ new nlobjSearchColumn("entityid"),
		                new nlobjSearchColumn("altname"),
						new nlobjSearchColumn("customer") ]);

		nlapiLogExecution('debug', 'project count',
		        project_search_results.length);

		if (project_search_results.length == 0) {
			throw "You don't have any projects under you.";
		}

		return project_search_results;
	} catch (err) {
		nlapiLogExecution('ERROR', 'getTaggedProjectList', err);
		throw err;
	}
}

function getTaggedCustList() {
	try {
		
		var project_search_results = searchRecord('job', '1517', null, new nlobjSearchColumn('customer',null,'group'));

		nlapiLogExecution('debug', 'project count',
		        project_search_results.length);

		if (project_search_results.length == 0) {
			throw "You don't have any projects under you.";
		}

		return project_search_results;
	} catch (err) {
		nlapiLogExecution('ERROR', 'getTaggedProjectList', err);
		throw err;
	}
}

function getProjectDetails(projectList, requestedProject) {
	try {
		var projectData = {};

		var filters = [ new nlobjSearchFilter('internalid', null, 'anyof',
		        projectList) ];

		if (requestedProject) {
			filters.push(new nlobjSearchFilter('internalid', null, 'anyof',
			        requestedProject));
		}

		searchRecord(
		        'job',
		        null,
		        filters,
		        [ new nlobjSearchColumn('startdate'),
		                new nlobjSearchColumn('enddate'),
		                new nlobjSearchColumn('jobbillingtype'),
		                new nlobjSearchColumn('entityid'),
		                new nlobjSearchColumn('altname'),
		                new nlobjSearchColumn('custentity_t_and_m_monthly') ])
		        .forEach(
		                function(project) {
			                projectData[project.getId()] = {
			                    EntityId : project.getValue('entityid'),
			                    AltName : project.getValue('altname'),
			                    StartDate : project.getValue('startdate'),
			                    EndDate : project.getValue('enddate'),
			                    BillingType : project.getText('jobbillingtype')
			                            + (project
			                                    .getValue('custentity_t_and_m_monthly') == 'T' ? ' Monthly'
			                                    : '')
			                };
		                });

		return projectData;
	} catch (err) {
		nlapiLogExecution('ERROR', 'getProjectDetails', err);
		throw err;
	}
}

function getProjectDetails_practiceWise(prac_list) {
	try {
		var projectData = {};

		var filters = [ new nlobjSearchFilter('custentity_practice', null, 'anyof',
		        prac_list) ];


		searchRecord(
		        'job',
		        null,
		        filters,
		        [ new nlobjSearchColumn('startdate'),
		                new nlobjSearchColumn('enddate'),
		                new nlobjSearchColumn('jobbillingtype'),
		                new nlobjSearchColumn('entityid'),
		                new nlobjSearchColumn('altname'),
		                new nlobjSearchColumn('custentity_t_and_m_monthly') ])
		        .forEach(
		                function(project) {
			                projectData[project.getId()] = {
			                    EntityId : project.getValue('entityid'),
			                    AltName : project.getValue('altname'),
			                    StartDate : project.getValue('startdate'),
			                    EndDate : project.getValue('enddate'),
			                    BillingType : project.getText('jobbillingtype')
			                            + (project
			                                    .getValue('custentity_t_and_m_monthly') == 'T' ? ' Monthly'
			                                    : '')
			                };
		                });

		return projectData;
	} catch (err) {
		nlapiLogExecution('ERROR', 'getProjectDetails', err);
		throw err;
	}
}

function getProjectRevenueData(projectList, startDate, endDate,
        requestedProject)
{
	try {
		var filters = [
		        new nlobjSearchFilter('custrecord_prp_project',
		                'custrecord_eprp_project_rev_projection', 'anyof',
		                projectList),
		        new nlobjSearchFilter('custrecord_prp_start_date',
		                'custrecord_eprp_project_rev_projection', 'within',
		                startDate, endDate),
		        new nlobjSearchFilter('isinactive',
		                'custrecord_eprp_project_rev_projection', 'is', 'F'),
		        new nlobjSearchFilter('isinactive', null, 'is', 'F'),
		        new nlobjSearchFilter('custrecord_prp_is_past',
		                'custrecord_eprp_project_rev_projection', 'is', 'F') ];

		if (requestedProject) {
			filters.push(new nlobjSearchFilter('custrecord_prp_project',
			        'custrecord_eprp_project_rev_projection', 'anyof',
			        requestedProject));
		}

		var revenueSearch = nlapiSearchRecord(
		        'customrecord_emp_practice_rev_projection', null, filters, [
		                new nlobjSearchColumn('custrecord_prp_project',
		                        'custrecord_eprp_project_rev_projection'),
		                new nlobjSearchColumn('custrecord_prp_start_date',
		                        'custrecord_eprp_project_rev_projection'),
		                new nlobjSearchColumn('custrecord_prp_month_name',
		                        'custrecord_eprp_project_rev_projection'),
		                new nlobjSearchColumn('custrecord_eprp_practice'),
		                new nlobjSearchColumn('custrecord_eprp_revenue'),
		                new nlobjSearchColumn('custrecord_eprp_usd_revenue'),
		                new nlobjSearchColumn('custrecord_eprp_currency') ]);

		if (revenueSearch) {
			nlapiLogExecution('debug', 'revenueSearch', revenueSearch.length);
		}

		return revenueSearch;
	} catch (err) {
		nlapiLogExecution('ERROR', 'getProjectRevenueData', err);
		throw err;
	}
}

function getProjectRevenueData_ByPractice(prac_list, startDate, endDate,
        requestedProject)
{
	try {
		var filters = [
		        new nlobjSearchFilter('custrecord_eprp_practice',
		                null, 'anyof',
		                prac_list),
		        new nlobjSearchFilter('custrecord_prp_start_date',
		                'custrecord_eprp_project_rev_projection', 'within',
		                startDate, endDate),
		        new nlobjSearchFilter('isinactive',
		                'custrecord_eprp_project_rev_projection', 'is', 'F'),
		        new nlobjSearchFilter('isinactive', null, 'is', 'F'),
		        new nlobjSearchFilter('custrecord_prp_is_past',
		                'custrecord_eprp_project_rev_projection', 'is', 'F') ];

		var revenueSearch = nlapiSearchRecord(
		        'customrecord_emp_practice_rev_projection', null, filters, [
		                new nlobjSearchColumn('custrecord_prp_project',
		                        'custrecord_eprp_project_rev_projection'),
		                new nlobjSearchColumn('custrecord_prp_start_date',
		                        'custrecord_eprp_project_rev_projection'),
		                new nlobjSearchColumn('custrecord_prp_month_name',
		                        'custrecord_eprp_project_rev_projection'),
		                new nlobjSearchColumn('custrecord_eprp_practice'),
		                new nlobjSearchColumn('custrecord_eprp_revenue'),
		                new nlobjSearchColumn('custrecord_eprp_usd_revenue'),
		                new nlobjSearchColumn('custrecord_eprp_currency') ]);

		if (revenueSearch) {
			nlapiLogExecution('debug', 'revenueSearch', revenueSearch.length);
		}

		return revenueSearch;
	} catch (err) {
		nlapiLogExecution('ERROR', 'getProjectRevenueData', err);
		throw err;
	}
}

function createProjectionTable(requestedProject,requestedExchangeRateTable)
{
	try {
		
		var projectWiseRevenue = {};
		var sr_no = 0;
		var fltr_counter = 0;
		var emp_unique_proj = new Array();
		
		var arrFilters = new Array();
		
		if(requestedProject)
		{
			arrFilters[fltr_counter] = new nlobjSearchFilter('custrecord_project_name_revenue',null,'anyof',requestedProject);
			fltr_counter++;
		}
		
		var today = new Date();
		var yyyy = today.getFullYear();
		
		var startDate_revenue = "1/1/"+yyyy;
		var endDate_revenue = "12/31/"+yyyy;
		arrFilters[fltr_counter] = new nlobjSearchFilter('custrecord_strt_date_revenue',null,'within',startDate_revenue,endDate_revenue);
		fltr_counter++;
		arrFilters[fltr_counter] = new nlobjSearchFilter('custrecord_fb_project',null,'is','T');
		fltr_counter++;
		//arrFilters[fltr_counter] = new nlobjSearchFilter('custrecord_project_name_revenue',null,'anyof',19202);
		
		var arrColumns = new Array();	
		arrColumns[0] = new nlobjSearchColumn('custrecord_project_name_revenue');
		arrColumns[1] = new nlobjSearchColumn('custrecord_region_revenue');
		arrColumns[2] = new nlobjSearchColumn('custrecord_practice_revenue');
		arrColumns[3] = new nlobjSearchColumn('custrecord_cust_revenue');
		arrColumns[4] = new nlobjSearchColumn('custrecord_resource_revenue');
		arrColumns[5] = new nlobjSearchColumn('custrecord_usd_cost_revenue');
		arrColumns[6] = new nlobjSearchColumn('custrecord_emp_practice_revenue');
		arrColumns[7] = new nlobjSearchColumn('custrecord_month_revenue');
		arrColumns[8] = new nlobjSearchColumn('custrecord_recognised');
		arrColumns[9] = new nlobjSearchColumn('internalid');
		arrColumns[10] = new nlobjSearchColumn('custrecord_currency_revenue');
		arrColumns[11] = new nlobjSearchColumn('custrecord_pro_strt_date');
		arrColumns[12] = new nlobjSearchColumn('custrecord_pro_end_date');
		arrColumns[13] = new nlobjSearchColumn('custrecord_practice_id');
		arrColumns[14] = new nlobjSearchColumn('custrecord_fb_practice');
		arrColumns[15] = new nlobjSearchColumn('custrecord_fb_project_value');
		arrColumns[16] = new nlobjSearchColumn('custrecord_total_recognised_amount');
		arrColumns[17] = new nlobjSearchColumn('custrecord_lst_participating_practice');
		arrColumns[18] = new nlobjSearchColumn('custrecord_fp_proj_status');
		arrColumns[19] = new nlobjSearchColumn('custrecord_fp_remaining_mnths');
		arrColumns[20] = new nlobjSearchColumn('custrecord_fp_extended');
		arrColumns[21] = new nlobjSearchColumn('custrecord_fp_previous_enddate');		
		
		var revenueSearchData = searchRecord('customrecord_practice_revenue_empwise', null, arrFilters, arrColumns);

		if (revenueSearchData)
		{
			nlapiLogExecution('audit','srch len:- ',revenueSearchData.length);
			for (var i = 0; i < revenueSearchData.length; i++)
			{
				var projectId = revenueSearchData[i].getValue('custrecord_project_name_revenue');
				var projectName = revenueSearchData[i].getText('custrecord_project_name_revenue');
				var proj_region = revenueSearchData[i].getText('custrecord_region_revenue');
				var proj_prac = revenueSearchData[i].getValue('custrecord_practice_revenue');
				var cust = revenueSearchData[i].getText('custrecord_cust_revenue');
				var emp_id = revenueSearchData[i].getValue('custrecord_resource_revenue');
				var emp_name = revenueSearchData[i].getText('custrecord_resource_revenue');
				var revenue = revenueSearchData[i].getValue('custrecord_usd_cost_revenue');
				var emp_prac = revenueSearchData[i].getValue('custrecord_emp_practice_revenue');
				var isRecognised = revenueSearchData[i].getValue('custrecord_recognised');
				var monthName = revenueSearchData[i].getValue('custrecord_month_revenue');
				var proj_currency = revenueSearchData[i].getText('custrecord_currency_revenue');
				var proj_actual_strt_date = revenueSearchData[i].getValue('custrecord_pro_strt_date');
				var proj_actual_end_date = revenueSearchData[i].getValue('custrecord_pro_end_date');
				var proj_practice_id = revenueSearchData[i].getValue('custrecord_practice_id');
				var pro_practice_name = revenueSearchData[i].getValue('custrecord_fb_practice');
				var project_value = revenueSearchData[i].getValue('custrecord_fb_project_value');
				var total_recognised_amnt = revenueSearchData[i].getValue('custrecord_total_recognised_amount');
				var lst_project_entry = revenueSearchData[i].getValue('custrecord_lst_participating_practice');
				var internal_id = revenueSearchData[i].getId();
				var proj_status = revenueSearchData[i].getValue('custrecord_fp_proj_status');
				var proj_remaining_mnths = revenueSearchData[i].getValue('custrecord_fp_remaining_mnths');
				var proj_is_extended = revenueSearchData[i].getValue('custrecord_fp_extended');
				var proj_lst_date = revenueSearchData[i].getValue('custrecord_fp_previous_enddate');
				
				var is_revenue_prsnt = 0;
				
				if(parseInt(revenue) > 0 || parseInt(revenue) < 0)
				{
					//nlapiLogExecution('audit','proj_currency:- ',proj_currency);
					if (requestedExchangeRateTable[proj_currency])
					{
						//nlapiLogExecution('audit','rate:- ',requestedExchangeRateTable[proj_currency]);
						var exchangeRate = requestedExchangeRateTable[proj_currency];
						//revenue = Math.round(revenue * exchangeRate);
						revenue = Math.round(revenue * (1 / exchangeRate),2);
					}
					revenue = parseFloat(revenue).toFixed(2);
					
					is_revenue_prsnt = 1;
				}
				else
				{
					revenue = parseFloat(0);
				}
				
				if(proj_practice_id)
					is_revenue_prsnt = 1;
					
					
				if (total_recognised_amnt)
				{
					total_recognised_amnt = parseFloat(total_recognised_amnt).toFixed(2);
				}
				else
					total_recognised_amnt = 0;
					
				
				if(lst_project_entry != 'T')
				{
					//project_value = 0;
					total_recognised_amnt = 0;
				}
				
				//if (!projectWiseRevenue[emp_id])
				//{
					/*var emp_exist = searchEmp(emp_id,projectId,emp_unique_proj);
					//nlapiLogExecution('audit','emp_exist:- ',emp_exist);
					if(emp_exist)
					{
						nlapiLogExecution('audit','emp_exist:- true');
						projectWiseRevenue[emp_id].RevenueData[projectId][monthName].Amount = revenue;
						projectWiseRevenue[emp_id].RevenueData[projectId][monthName].IsRecognised = isRecognised;
					}
					else
					{
						nlapiLogExecution('audit','emp_exist:- false');
						emp_unique_proj[sr_no] = {
											employee_id:emp_id,
											project_id:projectId
										};
						sr_no++;*/
					var emp_id_prj_id = projectId +'_'+ proj_practice_id;
					
					if(lst_project_entry == 'T')
					{
						
					}
					if (!projectWiseRevenue[emp_id_prj_id])
					{
						//nlapiLogExecution('audit','inside create emp rcrd');
						
						projectWiseRevenue[emp_id_prj_id] = {
							project_id: projectId,
							projectName: projectName,
							proj_region: proj_region,
							proj_prac: proj_prac,
							cust: cust,
							emp_id: emp_id,
							emp_name: emp_name,
							revenue: revenue,
							emp_prac: emp_prac,
							IsRecognised: isRecognised,
							proj_actual_strt_date: proj_actual_strt_date,
							proj_actual_end_date: proj_actual_end_date,
							pro_practice_name: pro_practice_name,
							proj_practice_id: proj_practice_id,
							project_value: project_value,
							proj_currency: proj_currency,
							proj_status: proj_status,
							proj_remaining_mnths: proj_remaining_mnths,
							proj_is_extended: proj_is_extended,
							proj_lst_date: proj_lst_date,
							is_revenue_prsnt: is_revenue_prsnt,
							RevenueData: {}
						};
						
						projectWiseRevenue[emp_id_prj_id].RevenueData[projectId] = new yearData();
						
						projectWiseRevenue[emp_id_prj_id].RevenueData[projectId][monthName].Amount = revenue;
						projectWiseRevenue[emp_id_prj_id].RevenueData[projectId][monthName].IsRecognised = isRecognised;
						projectWiseRevenue[emp_id_prj_id].RevenueData[projectId][monthName].is_revenue_present = is_revenue_prsnt;
						projectWiseRevenue[emp_id_prj_id].RevenueData[projectId]["recogAmnt"].RecognisedAmount = total_recognised_amnt;
						
						//unique_proj.push(projectId);
						
						/*if (!projectWiseRevenue[emp_id].RevenueData[projectId]) {
							projectWiseRevenue[emp_id].RevenueData[projectId] = new yearData();
						}
						
						projectWiseRevenue[emp_id].RevenueData[projectId][monthName].Amount = revenue;
						projectWiseRevenue[emp_id].RevenueData[projectId][monthName].IsRecognised = isRecognised;*/
						
						
					}
					else
					{
						if (!projectWiseRevenue[emp_id_prj_id].RevenueData[projectId])
						{
							//emp_id = emp_id +'_'+sr_no;
						
							//nlapiLogExecution('audit','inside create proj rcrd');
							projectWiseRevenue[emp_id_prj_id] = {
								project_id: projectId,
								projectName: projectName,
								proj_region: proj_region,
								proj_prac: proj_prac,
								cust: cust,
								emp_id: emp_id,
								emp_name: emp_name,
								revenue: revenue,
								emp_prac: emp_prac,
								IsRecognised: isRecognised,
								proj_actual_strt_date: proj_actual_strt_date,
								proj_actual_end_date: proj_actual_end_date,
								pro_practice_name: pro_practice_name,
								proj_practice_id: proj_practice_id,
								project_value: project_value,
								proj_currency: proj_currency,
								proj_status: proj_status,
								proj_remaining_mnths: proj_remaining_mnths,
								proj_is_extended: proj_is_extended,
								proj_lst_date: proj_lst_date,
								is_revenue_prsnt: is_revenue_prsnt,
								RevenueData: {}
							};
							
							projectWiseRevenue[emp_id_prj_id].RevenueData[projectId] = new yearData();
							
							projectWiseRevenue[emp_id_prj_id].RevenueData[projectId][monthName].Amount = revenue;
							projectWiseRevenue[emp_id_prj_id].RevenueData[projectId][monthName].IsRecognised = isRecognised;
							projectWiseRevenue[emp_id_prj_id].RevenueData[projectId][monthName].is_revenue_present = is_revenue_prsnt;
							projectWiseRevenue[emp_id_prj_id].RevenueData[projectId]["recogAmnt"].RecognisedAmount = total_recognised_amnt;
							
						}
						else
						{
							if(projectWiseRevenue[emp_id_prj_id].is_revenue_prsnt == 0)
								projectWiseRevenue[emp_id_prj_id].is_revenue_prsnt = is_revenue_prsnt;
								
							if(project_value)
								projectWiseRevenue[emp_id_prj_id].project_value = project_value;
							
							projectWiseRevenue[emp_id_prj_id].RevenueData[projectId][monthName].Amount = revenue;
							projectWiseRevenue[emp_id_prj_id].RevenueData[projectId][monthName].IsRecognised = isRecognised;
							projectWiseRevenue[emp_id_prj_id].RevenueData[projectId][monthName].is_revenue_present = is_revenue_prsnt;
							projectWiseRevenue[emp_id_prj_id].RevenueData[projectId]["recogAmnt"].RecognisedAmount = total_recognised_amnt;
							
						}
					}
					
				//}
				
				/*if (!projectWiseRevenue[emp_id].RevenueData[emp_prac]) {
					projectWiseRevenue[internal_id].RevenueData[internal_id] = new yearData();
				}

				projectWiseRevenue[internal_id].RevenueData[internal_id][monthName].Amount = revenue;
				projectWiseRevenue[internal_id].RevenueData[internal_id][monthName].IsRecognised = isRecognised;*/
				
			}
		}
		
		var css_file_url = "";

		var context = nlapiGetContext();
		if (context.getEnvironment() == "SANDBOX") {
			css_file_url = "https://system.na1.netsuite.com/core/media/media.nl?id=199133&c=3883006&h=64fa194295cd4ab64d58&mv=iq3kjr6g&_xt=.css&whence=";
		} else if (context.getEnvironment() == "PRODUCTION") {
			css_file_url = "https://system.na1.netsuite.com/core/media/media.nl?id=199133&c=3883006&h=64fa194295cd4ab64d58&mv=iq3kjr6g&_xt=.css&whence=";
		}

		var html = "";
		html += "<link href='" + css_file_url + "'  rel='stylesheet'>";

		html += "<table class='projection-table'>";

		// header
		html += "<tr class='header-row'>";
		html += "<td>";
		html += "Customer Name";
		html += "</td>";
		
		html += "<td>";
		html += "Region";
		html += "</td>";

		html += "<td>";
		html += "Project Name";
		html += "</td>";
		
		html += "<td>";
		html += "Project Start Date";
		html += "</td>";
		
		html += "<td>";
		html += "Project End Date";
		html += "</td>";

		html += "<td>";
		html += "Executing Practice";
		html += "</td>";
		
		html += "<td>";
		html += "Participating Practice";
		html += "</td>";
		
		html += "<td>";
		html += "Project Currency";
		html += "</td>";
		
		html += "<td>";
		html += "SOW Value";
		html += "</td>";
		
		html += "<td>";
		html += "Project Status";
		html += "</td>";
		
		html += "<td>";
		html += "Remaining Months";
		html += "</td>";
		
		html += "<td>";
		html += "Project Extended";
		html += "</td>";
		
		html += "<td>";
		html += "Project Previous End Date";
		html += "</td>";
		
		/*html += "<td>";
		html += "Recognised Amount";
		html += "</td>";*/

		var monthNames = [ "JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JUL",
		        "AUG", "SEP", "OCT", "NOV", "DEC", "Recognised Amount"];

		var today = new Date();
		var currentMonthName = getMonthName(nlapiDateToString(today));
		
		for (var i = 0; i < monthNames.length; i++) {
			html += "<td>";
			html += monthNames[i];
			html += "</td>";
		}
		
		html += "</tr>";

		for ( var emp_internal_id in projectWiseRevenue) {
			//nlapiLogExecution('audit','emp_internal_id');
			for ( var project_internal_id in projectWiseRevenue[emp_internal_id].RevenueData) {
				//nlapiLogExecution('audit','projectData[project].EntityId',projectData[project].EntityId);
				var display_row = 0;
				for ( var month in projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id])
				{
					var is_revenue_thr_for_practice = projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id][month].is_revenue_present;
					
					if(parseInt(is_revenue_thr_for_practice) > 0)
					{
						//if(projectWiseRevenue[emp_internal_id].proj_practice_id)
						{
							//nlapiLogExecution('audit','revenue prsnt:- '+projectWiseRevenue[emp_internal_id].pro_practice_name,projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id][month].is_revenue_present);
							display_row = 1;
						}
					}	
				}
				
				if (display_row == 1)
				{
				
					html += "<tr>";
					html += "<td class='label-name'>";
					html += projectWiseRevenue[emp_internal_id].cust;
					html += "</td>";
					
					html += "<td class='label-name'>";
					html += projectWiseRevenue[emp_internal_id].proj_region;
					html += "</td>";
					
					html += "<td class='label-name'>";
					html += projectWiseRevenue[emp_internal_id].projectName;
					html += "</td>";
					
					html += "<td class='label-name'>";
					html += projectWiseRevenue[emp_internal_id].proj_actual_strt_date;
					html += "</td>";
					
					html += "<td class='label-name'>";
					html += projectWiseRevenue[emp_internal_id].proj_actual_end_date;
					html += "</td>";
					
					html += "<td class='label-name'>";
					html += projectWiseRevenue[emp_internal_id].proj_prac;
					html += "</td>";
					
					html += "<td class='label-name'>";
					html += projectWiseRevenue[emp_internal_id].pro_practice_name;
					html += "</td>";
					
					html += "<td class='label-name'>";
					html += projectWiseRevenue[emp_internal_id].proj_currency;
					html += "</td>";
					
					html += "<td class='label-name'>";
					html += projectWiseRevenue[emp_internal_id].project_value;
					html += "</td>";
					
					html += "<td class='label-name'>";
					html += projectWiseRevenue[emp_internal_id].proj_status;
					html += "</td>";
					
					html += "<td class='label-name'>";
					html += projectWiseRevenue[emp_internal_id].proj_remaining_mnths;
					html += "</td>";
					
					html += "<td class='label-name'>";
					html += projectWiseRevenue[emp_internal_id].proj_is_extended;
					html += "</td>";
					
					html += "<td class='label-name'>";
					html += projectWiseRevenue[emp_internal_id].proj_lst_date;
					html += "</td>";
					
					/*html += "<td class='label-name'>";
	 html += projectWiseRevenue[emp_internal_id].total_recognised_amnt;
	 html += "</td>";*/
					for (var month in projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id]) {
						// check if past month
						//nlapiLogExecution('audit','month in arr: '+projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id][month].Amount);
						var currentMonthPos = monthNames.indexOf(month);
						var currentMonthDate = nlapiStringToDate((currentMonthPos + 1) +
						'/31/2016');
						
						if (currentMonthDate < new Date()) {
							// if
							// (projectWiseRevenue[project].RevenueData[practice][month].IsRecognised)
							// {
							html += "<td class='monthly-amount'>";
						}
						else {
							html += "<td class='projected-amount'>";
						}
						//month = 'AUG';
						html += projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id][month].Amount;
						html += "</td>";
						
					}
					html += "<td class='projected-amount'>";
					html += projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id][month].RecognisedAmount;
					html += "</td>";
					
					html += "</tr>";
				}
			}
		}

		html += "</table>";

		return html;
	} catch (err) {
		nlapiLogExecution('ERROR', 'createProjectionTable', err);
		throw err;
	}
}

function searchRecord(recordType, savedSearch, arrFilters, arrColumns,filterExpression)
{
	try {
		var search = null;

		// if a saved search is provided, load it and add the filters and
		// columns
		if (isNotEmpty(savedSearch)) {
			search = nlapiLoadSearch(recordType, savedSearch);

			if (isArrayNotEmpty(arrFilters)) {
				search.addFilters(arrFilters);
			}

			if (isArrayNotEmpty(arrColumns)) {
				search.addColumns(arrColumns);
			}

			if (isArrayNotEmpty(filterExpression)) {
				search.setFilterExpression(filterExpression);
			}
		}
		// create a new search
		else {
			search = nlapiCreateSearch(recordType, arrFilters, arrColumns);
		}

		// run search
		var resultSet = search.runSearch();

		// iterate through the search and get all data 1000 at a time
		var searchResultCount = 0;
		var resultSlice = null;
		var searchResult = [];

		do {
			resultSlice = resultSet.getResults(searchResultCount,
			        searchResultCount + 1000);

			if (resultSlice) {

				resultSlice.forEach(function(result) {

					searchResult.push(result);
					searchResultCount++;
				});
			}
		} while (isArrayNotEmpty(resultSlice) && resultSlice.length >= 1000);

		return searchResult;
	} catch (err) {
		nlapiLogExecution('ERROR', 'searchRecord', err);
		throw err;
	}
}

function searchEmp(emp_id,projectId,emp_unique_proj)
{
	for(var jk=0; jk<emp_unique_proj.length ; jk++)
	{
		var proj_ID = emp_unique_proj[jk].project_id;
		var Emp_ID = emp_unique_proj[jk].employee_id;
		
		if(parseInt(Emp_ID) == parseInt(emp_id) && parseInt(proj_ID)== parseInt(projectId))
		{
			return true;
		}
		else
		{
			return false;
		}
	}
}

function isEmpty(value) {

	return value == null || value == "" || typeof (value) == undefined;
}

function isNotEmpty(value) {

	return !isEmpty(value);
}

function isArrayEmpty(argArray) {

	return !isArrayNotEmpty(argArray);
}

function isArrayNotEmpty(argArray) {

	return (isNotEmpty(argArray) && argArray.length > 0);
}

function yearData() {
	var monthNames = [ "JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JUL", "AUG",
	        "SEP", "OCT", "NOV", "DEC", "recogAmnt" ];

	for (var i = 0; i < monthNames.length; i++) {
		this[monthNames[i]] = {
		    Amount : 0,
		    IsRecognised : true,
			is_revenue_present : 0
		};
	}
}

function legendsTable() {
	var htmlText = "";
	htmlText += "<table class='legend-table'>";
	htmlText += "<tr><td class=''></td><td>Legends</td></tr>";
	htmlText += "<tr><td class='recognised-circle'></td><td>Recognised</td></tr>";
	htmlText += "<tr><td class='projected-circle'></td><td>Projected</td></tr>";
	htmlText += "</table>";
	return htmlText;
}

function getMonthName(currentDate) {
	currentDate = nlapiStringToDate(currentDate)
	var monthNames = [ "JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JUL", "AUG",
	        "SEP", "OCT", "NOV", "DEC" ];
	return monthNames[currentDate.getMonth()];
}

function getDataRefreshDate() {
	try {
		var refreshDate = '';

		var dateSearch = nlapiSearchRecord(
		        'customrecord_emp_practice_rev_projection', null, null,
		        new nlobjSearchColumn('formuladate', null, 'max')
		                .setFormula('{created}'));

		if (dateSearch) {
			refreshDate = dateSearch[0].getValue('formuladate', null, 'max');
		}

		return refreshDate;
	} catch (err) {
		nlapiLogExecution('ERROR', 'getDataRefreshDate', err);
		throw err;
	}
}

function isRefreshOngoing() {
	try {
		var isRefreshOnGoing = false;

		var search = nlapiSearchRecord('scheduledscriptinstance', null,
		        [
		                new nlobjSearchFilter('internalid', 'script', 'anyof',
		                        [ 938 ]),
		                new nlobjSearchFilter('status', null, 'anyof', [
		                        'RETRY', 'PROCESSING', 'PENDING' ]) ])

		if (search) {
			isRefreshOnGoing = true;
		}

		return isRefreshOnGoing;
	} catch (err) {
		nlapiLogExecution('ERROR', 'isRefreshOngoing', err);
		throw err;
	}
}

function _logValidation(value){
	if (value != 'null' && value != null && value != null && value != '' && value != undefined && value != undefined && value != 'undefined' && value != 'undefined' && value != 'NaN' && value != NaN) {
		return true;
	}
	else {
		return false;
	}
}

function generate_excel(strVar_excel)
{
	var strVar1= '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:ss="urn:schemas-microsoft-com:office:spreadsheet" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40">'+
		'<head>'+
		'<meta http-equiv=Content-Type content="text/html; charset=windows-1252"/>'+
        '<meta name=ProgId content=Excel.Sheet/>'+
        '<meta name=Generator content="Microsoft Excel 11"/>'+
        '<!--[if gte mso 9]><xml>'+
        '<x:excelworkbook>'+
        '<x:excelworksheets>'+
        '<x:excelworksheet=sheet1>'+
        '<x:name>** ESTIMATE FILE**</x:name>'+
        '<x:worksheetoptions>'+
        '<x:selected></x:selected>'+
        '<x:freezepanes></x:freezepanes>'+
        '<x:frozennosplit></x:frozennosplit>'+
        '<x:splithorizontal>** FROZEN ROWS + 1 **</x:splithorizontal>'+
        '<x:toprowbottompane>** FROZEN ROWS + 1 **</x:toprowbottompane>'+
        '<x:splitvertical>** FROZEN COLUMNS + 1 **</x:splitvertical>'+
        '<x:leftcolumnrightpane>** FROZEN COLUMNS + 1**</x:leftcolumnrightpane>'+
		    '<x:activepane>0</x:activepane>'+// 0
		    '<x:panes>'+
			'<x:pane>'+
			'<x:number>3</x:number>'+
			'</x:pane>'+
			'<x:pane>'+
		    '<x:number>1</x:number>'+
		    '</x:pane>'+
		    '<x:pane>'+
			'<x:number>2</x:number>'+
        '</x:pane>'+
        '<x:pane>'+
        '<x:number>0</x:number>'+//1
        '</x:pane>'+
        '</x:panes>'+
        '<x:protectcontents>False</x:protectcontents>'+
        '<x:protectobjects>False</x:protectobjects>'+
        '<x:protectscenarios>False</x:protectscenarios>'+
        '</x:worksheetoptions>'+
        '</x:excelworksheet>'+
		'</x:excelworksheets>'+
        '<x:protectstructure>False</x:protectstructure>'+
        '<x:protectwindows>False</x:protectwindows>'+
		'</x:excelworkbook>'+
        
        // '<style>'+
			
			//-------------------------------------
        '</x:excelworkbook>'+
        '</xml><![endif]-->'+
         '<style>'+
		'p.MsoFooter, li.MsoFooter, div.MsoFooter'+
		'{ margin:0in; margin-bottom:.0001pt; mso-pagination:widow-orphan; tab-stops:center 3.0in right 6.0in; font-size:12.0pt;}'+
		'<style>'+

		'<!-- /* Style Definitions */'+

		//'@page Section1'+
		//'{ size:8.5in 11.0in; margin:1.9cm 1.27cm 1.9cm 1.27cm; mso-header-margin:.5in; mso-footer-margin:.5in; mso-title-page:yes; mso-header: h1; mso-footer: f1; mso-first-header: fh1; mso-first-footer: ff1; mso-paper-source:0;}'+

		'div.Section1'+
		'{ page:Section1;}'+

		'table#hrdftrtbl'+
		'{ margin:0in 0in 0in 900in; width:1px; height:1px; overflow:hidden; font-family:Arial;}-->'+

		'</style>'+
		
		
		'</head>'+
		

		'<body>'+
		
		'<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">'
		
		var strVar2 = strVar_excel;
		strVar1 = strVar1 + strVar2;
		var file = nlapiCreateFile('Resource Allocation Data.xls', 'XMLDOC', strVar1);
		return file;
}