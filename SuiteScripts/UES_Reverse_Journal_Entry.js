// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
    /*
     Script Name: UES_Reverse_Journal_Entry.js
     Author:      Swati Kurariya
     Company:     Aashna Cloudtech Pvt Ltd.
     Date:        27-03-2014
     Description: Fix-bid-Revenue Reversal process of journal entry. 
     
     Script Modification Log:
     
     -- Date --			-- Modified By --				--Requested By--				-- Description --
      
     Below is a summary of the process controls enforced by this script file.  The control logic is described
     more fully, below, in the appropriate function headers and code blocks.
     
     BEFORE LOAD
     - beforeLoadRecord(type)
     
     NOT USED
     BEFORE SUBMIT
     - beforeSubmitRecord(type)
     
     NOT USED
     AFTER SUBMIT
     - afterSubmitRecord(type)
     
     afterSubmitRecord(type)
     SUB-FUNCTIONS
     
     - The following sub-functions are called by the above core functions in order to maintain code
     modularization:
     
     - NOT USED
     */
}
// END SCRIPT DESCRIPTION BLOCK  ====================================



// BEGIN GLOBAL VARIABLE BLOCK  =====================================
{
    //  Initialize any Global Variables, in particular, debugging variables...




}
// END GLOBAL VARIABLE BLOCK  =======================================





// BEGIN BEFORE LOAD ==================================================

function beforeLoadRecord(type)	
{

    /*  On before load:
     - EXPLAIN THE PURPOSE OF THIS FUNCTION
     -
     FIELDS USED:
     --Field Name--				--ID--
     */
    //  LOCAL VARIABLES
    
    //  BEFORE LOAD CODE BODY
    
    
    return true;
    
    
    
}

// END BEFORE LOAD ====================================================





// BEGIN BEFORE SUBMIT ================================================

function beforeSubmitRecord(type)	
{
    /*  On before submit:
     - PURPOSE
     -
     FIELDS USED:
     --Field Name--				--ID--
     */
    //  LOCAL VARIABLES
    
    
    //  BEFORE SUBMIT CODE BODY
	
	
	
	
    
    return true;
    
}

// END BEFORE SUBMIT ==================================================





// BEGIN AFTER SUBMIT =============================================

function afterSubmitRecord_Reverse_JV(type)	
{
    /*  On after submit:
     - PURPOSE
     -
     FIELDS USED:
     --Field Name--				--ID--
     */
    //  LOCAL VARIABLES
       var i_Project_DR='';
    
    
    //  AFTER SUBMIT CODE BODY
       
    /*
     * In create mode load the invoice and create journal entry.
     * and set journal entry id into body field
     */
      
   if(type != 'delete' && type == 'create')
   {//create if start
	   
	         //custcol_milestone=column field id on invoice(source:-project task)
	         //custbody_journal_entry=journal entry field on body level
   	        // === create journal entry , based on milestone and project===
			 nlapiLogExecution('DEBUG', 'Invoive ', "type" + type);
			try 
			{
				
				var i_InvoiceID=nlapiGetRecordId();
				var s_InvoiceTYPE=nlapiGetRecordType();
				
				var o_InvoiceOBJ=nlapiLoadRecord(s_InvoiceTYPE, i_InvoiceID);
				
				if(_logValidation(o_InvoiceOBJ))
					{//invoiceOBJ if start
					  
					   var i_Project_Val=o_InvoiceOBJ.getFieldValue('job');
					   nlapiLogExecution('DEBUG', 'Invoive ', "i_Project_Val" + i_Project_Val);
					   
					   var i_Inv_Subsi=o_InvoiceOBJ.getFieldValue('subsidiary');
					   nlapiLogExecution('DEBUG', 'Invoice ', "i_Inv_Subsi" + i_Inv_Subsi);
					   
					   var i_Inv_Curren=o_InvoiceOBJ.getFieldValue('currency');
					   nlapiLogExecution('DEBUG', 'Invoice ', "i_Inv_Curren" + i_Inv_Curren);
					   
					   var d_Inv_date=o_InvoiceOBJ.getFieldValue('trandate');
					   nlapiLogExecution('DEBUG', 'Invoice ', "d_Inv_date" + d_Inv_date);
					   
					   
					   
					   if(_logValidation(i_Project_Val))
						   {//if start
						   
						      var o_Load_ProjectOBJ=nlapiLoadRecord('job',i_Project_Val);
						       i_Project_DR=o_Load_ProjectOBJ.getFieldValue('custentity_fpr_draccount');
						      
						   }//if close
					     
					   var o_InvCount=o_InvoiceOBJ.getLineItemCount('item');
					   nlapiLogExecution('DEBUG', 'Invoive ', "o_InvCount" +o_InvCount);
					   
					   if(_logValidation(o_InvCount))
						   {//inv count if start
						   
						   
						 //-----------------------------------Create journal entry--------------------------------
						     var o_journalOBJ = nlapiCreateRecord('journalentry');
			        	     o_journalOBJ.setFieldValue('trandate',d_Inv_date);
			        	    // o_journalOBJ.setFieldValue('reversaldate',d_Inv_date);	
			        	     //o_journalOBJ.setFieldValue('reversaldefer','T');//
			        	     o_journalOBJ.setFieldValue('subsidiary',i_Inv_Subsi);
			        	     o_journalOBJ.setFieldValue('currency',i_Inv_Curren);
						   //-----------------------------------------------------------------------------   
			        	     
						         
						         for(var ss=1 ; ss<=o_InvCount ; ss++)
						        	 {//inv count for start
						        	 
						        	 	
						        	     var i_Item_Id=o_InvoiceOBJ.getLineItemValue('item','item', ss);
						        	     nlapiLogExecution('DEBUG', 'Invoive ', "i_Item_Id" + i_Item_Id);
						        	      
						        	     var filters = new nlobjSearchFilter('internalid', null, 'is', i_Item_Id);
               						 	var searchresults = nlapiSearchRecord('item', null, filters, null);
               							//alert(searchresults.length);
               						  	var record_type=searchresults[0].getRecordType();
						        	     
						        	     if(_logValidation(record_type))
						        	    	 {//if start
						        	    	 
							        	    	 var o_Load_ItemOBJ=nlapiLoadRecord(record_type,i_Item_Id);
							        	    	 var i_Item_Income_Ac_CR=o_Load_ItemOBJ.getFieldValue('incomeaccount');
							        	    	 nlapiLogExecution('DEBUG', 'Invoive ', "i_Item_Income_Ac_CR" + i_Item_Income_Ac_CR);
						        	    	 
						        	    	 }//of close
						        	     
						        	     var i_Amount=o_InvoiceOBJ.getLineItemValue('item','amount', ss);
						        	     nlapiLogExecution('DEBUG', 'Invoive ', "i_Amount" + i_Amount);
						        	    
						        	     var i_Milestone=o_InvoiceOBJ.getLineItemValue('item','custcol_milestone', ss);
						        	     nlapiLogExecution('DEBUG', 'Invoive ', "i_Milestone" + i_Milestone);
						        	     //--------------------------------------------------
						        	     
						        	          
						        	     
						        	    	    o_journalOBJ.selectNewLineItem('line');
						        	    	    o_journalOBJ.setCurrentLineItemValue('line', 'account', i_Project_DR);
						        	    	    o_journalOBJ.setCurrentLineItemValue('line', 'credit', i_Amount);
						        	    	    o_journalOBJ.setCurrentLineItemValue('line', 'custcol_milestone', i_Milestone);
						        	    	    o_journalOBJ.commitLineItem('line');	 
						        	    	    	 
						        	    	    o_journalOBJ.selectNewLineItem('line');	 
						        	    	    o_journalOBJ.setCurrentLineItemValue('line', 'account', i_Item_Income_Ac_CR);	 
						        	    	    o_journalOBJ.setCurrentLineItemValue('line', 'debit', i_Amount);
						        	    	    o_journalOBJ.setCurrentLineItemValue('line', 'custcol_milestone', i_Milestone);
						        	    	    o_journalOBJ.commitLineItem('line');
						        	    	    
						        	    	    
						        	        
						        	    
						        	 }//inv count for close
						   
						   }//inv count if close
					   
					   var i_journal_entry_SubmitID = nlapiSubmitRecord(o_journalOBJ,true,true)
						nlapiLogExecution('DEBUG', ' Invoice ', ' *********** Journal Entry Submit ID  *********** -->' + i_journal_entry_SubmitID);
					
					   //set journal entry id
					   o_InvoiceOBJ.setFieldValue('custbody_journal_entry',i_journal_entry_SubmitID);
					   nlapiSubmitRecord(o_InvoiceOBJ,true,true);
					
					}//invoiceOBJ if close
				
				
				
			}// try block end
			catch(EX)
			{
				 nlapiLogExecution('DEBUG', 'Bill ', "Exception Thrown" + EX);
			}// catch block end
	
	
   }//create if close
   
   //--------------------------Type == Edit ---------------------------------------------------------
   /*
    * Edit mode get the journal entry id.
    * load the journal entry.
    * delete the all line item of JV 
    * get the count form invoice and again set into JV.
    */
    if(type == 'edit')
    {//edit if start
    	   
    	
    	try 
		{
			
			var i_InvoiceID=nlapiGetRecordId();
			var s_InvoiceTYPE=nlapiGetRecordType();
			
			var o_InvoiceOBJ=nlapiLoadRecord(s_InvoiceTYPE, i_InvoiceID);
			if(_logValidation(o_InvoiceOBJ))
				{//invoiceOBJ if start
				  
				   var i_Project_Val=o_InvoiceOBJ.getFieldValue('job');
				   nlapiLogExecution('DEBUG', 'Invoive ', "i_Project_Val" + i_Project_Val);
				    
				   if(_logValidation(i_Project_Val))
					   {//if start
					   
					      var o_Load_ProjectOBJ=nlapiLoadRecord('job',i_Project_Val);
					       i_Project_DR=o_Load_ProjectOBJ.getFieldValue('custentity_fpr_draccount');
					      
					   }//if close
				   
				   var i_Journal_Entry_Id=o_InvoiceOBJ.getFieldValue('custbody_journal_entry');
				   //-----------------------------------jv creation--------------------------------
				     var o_journalOBJ = nlapiLoadRecord('journalentry',i_Journal_Entry_Id);
	        	    
				     var i_Journal_Count=o_journalOBJ.getLineItemCount('line');
				     
				     for(var kk=i_Journal_Count ; kk>=1 ; kk--)
				    	 {
				    	 o_journalOBJ.removeLineItem('line', kk);
				    	 }
				   //-----------------------------------------------------------------------------     
				   var o_InvCount=o_InvoiceOBJ.getLineItemCount('item');
				   nlapiLogExecution('DEBUG', 'Invoive ', "o_InvCount" +o_InvCount);
				   
				   if(_logValidation(o_InvCount))
					   {//inv count if start
					   
					         
					         for(var ss=1 ; ss<=o_InvCount ; ss++)
					        	 {//inv count for start
					        	 
					        	 	
					        	     var i_Item_Id=o_InvoiceOBJ.getLineItemValue('item','item', ss);
					        	     nlapiLogExecution('DEBUG', 'Invoive ', "i_Item_Id" + i_Item_Id);
					        	      
					        	    
						        	     var filters = new nlobjSearchFilter('internalid', null, 'is', i_Item_Id);
               						 	var searchresults = nlapiSearchRecord('item', null, filters, null);
               							//alert(searchresults.length);
               						  	var record_type=searchresults[0].getRecordType();
					        	     
					        	     
					        	     
					        	     if(_logValidation(record_type))
					        	    	 {//if start
					        	    	 
						        	    	 var o_Load_ItemOBJ=nlapiLoadRecord(record_type,i_Item_Id);
						        	    	 var i_Item_Income_Ac_CR=o_Load_ItemOBJ.getFieldValue('incomeaccount');
						        	    	 nlapiLogExecution('DEBUG', 'Invoive ', "i_Item_Income_Ac_CR" + i_Item_Income_Ac_CR);
					        	    	 
					        	    	 }//of close
					        	     
					        	     var i_Amount=o_InvoiceOBJ.getLineItemValue('item','amount', ss);
					        	     nlapiLogExecution('DEBUG', 'Invoive ', "i_Amount" + i_Amount);
					        	    
					        	     var i_Milestone=o_InvoiceOBJ.getLineItemValue('item','custcol_milestone', ss);
					        	     nlapiLogExecution('DEBUG', 'Invoive ', "i_Milestone" + i_Milestone);
					        	     //--------------------------------------------------
					        	     
					        	    // var i_JV_Count=o_JvOBJ.getLineItemCount('line');
					        	     
					        	    	    o_journalOBJ.selectNewLineItem('line');
					        	    	    o_journalOBJ.setCurrentLineItemValue('line', 'account', i_Project_DR);
					        	    	    o_journalOBJ.setCurrentLineItemValue('line', 'credit', i_Amount);
					        	    	     o_journalOBJ.setCurrentLineItemValue('line', 'custcol_milestone', i_Milestone);
					        	    	    o_journalOBJ.commitLineItem('line');	 
					        	    	    	 
					        	    	    o_journalOBJ.selectNewLineItem('line');	 
					        	    	    o_journalOBJ.setCurrentLineItemValue('line', 'account', i_Item_Income_Ac_CR);	 
					        	    	    o_journalOBJ.setCurrentLineItemValue('line', 'debit', i_Amount);
					        	    	     o_journalOBJ.setCurrentLineItemValue('line', 'custcol_milestone', i_Milestone);
					        	    	    o_journalOBJ.commitLineItem('line');
					        	    	    
					        	    	    
					        	        
					        	    
					        	 }//inv count for close
					   
					   }//inv count if close
				   
				   var i_journal_entry_SubmitID = nlapiSubmitRecord(o_journalOBJ,true,true)
					nlapiLogExecution('DEBUG', ' Invoice ', ' *********** Journal Entry Submit ID  *********** -->' + i_journal_entry_SubmitID);
				
				
				}//invoiceOBJ if close
			
			
			
		}// try block end
		catch(EX)
		{
			 nlapiLogExecution('DEBUG', 'Bill ', "Exception Thrown" + EX);
		}// catch block end
    	
    }//edit if close
    
	
    
    return true;
    
}

// END AFTER SUBMIT ===============================================





// BEGIN FUNCTION ===================================================
{
    
	function _logValidation(value) 
	{
	 if(value!='null' && value != null && value != null && value != '' && value != undefined && value != undefined && value != 'undefined' && value!= 'undefined'&& value!= 'NaN' && value != NaN) 
	 {
	  return true;
	 }
	 else 
	 { 
	  return false;
	 }
	}
	//--
    
}
// END FUNCTION =====================================================
