// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
    /*
           Script Name : REST_SaveFRF.js
        Author      : ASHISH PANDIT
        Date        : 12 APRIL 2019
        Description : RESTlet to Save FRF Plan 
    
    
        Script Modification Log:
    
        -- Date --			-- Modified By --				--Requested By--				-- Description --
         20-08-2019				Aazamali Khan					Deepak							Added Status as Approved for successfully created RRFs.	
    
    
    Below is a summary of the process controls enforced by this script file.  The control logic is described
    more fully, below, in the appropriate function headers and code blocks.
    
    
         SCHEDULED FUNCTION
            - scheduledFunction(type)
    
    
         SUB-FUNCTIONS
            - The following sub-functions are called by the above core functions in order to maintain code
                modularization:
    
                   - NOT USED
    
    */
}
// END SCRIPT DESCRIPTION BLOCK  ====================================

/*Global Variable*/

function REST_SaveFRF(recObj) {


    /*
      var recObj=[];
      recObj={"loggedUser":"ganapati.bhat@brillio.com","mode":"Filled","opportunity_id":"","record_id":"313245","submitmode":""}
    	
        */

    /* 
     var recObj = {"mode":"save","opportunity_id":"","resource":"","loggedUser":"203054","account":8381,"level":"3A","project":56080,"opportunity":"","practice":520,"role":"Angular Developer","billrole":"F","frfsource":4,"positions":1,"location":"5","primaryskills":[4],"otherskills":[],"startDate":"10/29/2020","endDate":"10/30/2020","allocation":100,"billrate":"0.00","criticalrole":"F","externalhire":"F","suggestedpeople":[],"backuprequired":"F","jobtitle":"testing","jobdescription":"testing","instruction":"testing","educationlevel":2,"educationprogram":null,"firstinterviewer":"","secondinterviewer":"","custinterview":"F","custintervieweremail":"","externalhirereason":"","createdby":"203054","positionType":"1","splRequirements":"","desiredStartDate":"10/22/2020","replacingCandidates":[],"frfFitment":"Account Fitment","submitmode":"submit"}
     
 */
    try {
        var response = new Response();
        nlapiLogExecution('Debug', 'recObj  ', JSON.stringify(recObj));
        var mode = recObj.mode;
        var submitmode = recObj.submitmode;
        nlapiLogExecution('Debug', 'submitmode ', submitmode);
        var i_recId = recObj.record_id;
        if (recObj.opportunity_id) {
            var i_opp_id = recObj.opportunity_id;
            var customrecord_sfdc_opportunity_recordSearch = nlapiSearchRecord("customrecord_sfdc_opportunity_record", null,
                [
                    ["custrecord_opportunity_id_sfdc", "is", i_opp_id]
                ],
                [
                    new nlobjSearchColumn("internalid")
                ]
            );
            for (var i = 0; i < customrecord_sfdc_opportunity_recordSearch.length; i++) {
                var searchObj = customrecord_sfdc_opportunity_recordSearch[i];

                var i_opp = searchObj.getValue('internalid');
            }
        } else {
            var i_opp = "";
        }
        var cancel_reason = recObj.reason;

        nlapiLogExecution('Debug', 'i_opp ', i_opp);
        nlapiLogExecution('Debug', 'mode ', mode);
        nlapiLogExecution('Debug', 'i_recId $$$$ ', i_recId);

        if (mode == 'save' || mode == 'edit') {
            nlapiLogExecution('Debug', 'recObj', recObj);
            var recId = createRecord(recObj, mode, i_recId, i_opp, submitmode);
            nlapiLogExecution('Debug', 'recId $$$$$', JSON.stringify(recId));
            nlapiLogExecution('Debug', 'recId.Status $$$$$', recId.Status);
            if (recId.Status == true) {
                response.Data = recId;
                response.Status = true;
                return response;
            } else {
                response.Data = recId;
                response.Status = false;
                return response;
            }

            //return recId;
        }
        if (mode == 'clone') {
            var cloneRecordId = nlapiCopyRecord('customrecord_frf_plan_details', i_recId);
            nlapiLogExecution('Debug', 'Record Copied', cloneRecordId);
            var copiedId = nlapiSubmitRecord(cloneRecordId);
            if (copiedId) {
                response.Data = copiedId;
                response.Status = true;
                return response;
            } else {
                response.Status = false;
                return response;
            }
            //return copiedId;
        }
        if (mode == 'delete') {
            var deleteRecordId = nlapiDeleteRecord('customrecord_frf_plan_details', i_recId);
            nlapiLogExecution('Debug', 'Record Deleted', deleteRecordId);
            if (deleteRecordId) {
                response.Data = deleteRecordId;
                response.Status = true;
                return response;
            } else {
                response.Status = false;
                return response;
            }
            //return deleteRecordId;
        }
        if (mode == 'cancel') {
            var cancelReason = recObj.cancelReason;
            var d_cancelDate = recObj.cancelDate
            var cancelFrf = nlapiLoadRecord('customrecord_frf_details', i_recId);
            cancelFrf.setFieldValue('custrecord_frf_details_status', 'T');
            if (d_cancelDate) {
                d_cancelDate = nlapiStringToDate(d_cancelDate);
                cancelFrf.setFieldValue('custrecord_frf_details_cancelled_date', new Date());
            }
            cancelFrf.setFieldText('custrecord_frf_details_cancellation_reas', cancelReason);
            cancelFrf.setFieldValue('custrecord_frf_details_status_flag', 4);
            cancelFrf.setFieldValue('custrecord_frf_details_open_close_status', 4);
            if (recObj.loggedUser) {
                var i_user = getUserUsingEmailId(recObj.loggedUser);
                cancelFrf.setFieldValue('custrecord_frf_details_rrf_cancelled_by', i_user);
            }
            var recCancel = nlapiSubmitRecord(cancelFrf);
            nlapiLogExecution('Debug', 'cancelFrf in Cancel Mode', recCancel);
            //var cancelFrf = nlapiSubmitField('customrecord_frf_details',i_recId,'custrecord_frf_details_status','T');
            if (recCancel) {
                response.Data = recCancel;
                response.Status = true;
                return response;
            } else {
                response.Status = false;
                return response;
            }
            //return recCancel;
        }




        // if the frf is in filled status we are marking it as closed            Added by gaurav jaiswal 20/02/2020

        if (mode == 'Filled') {
            nlapiLogExecution('Debug', 'recObj', recObj);
            var d_cancelDate = recObj.cancelDate
            var filledFrf = nlapiLoadRecord('customrecord_frf_details', i_recId);
            var rrfNumber = filledFrf.getFieldValue('custrecord_frf_details_rrf_number');
            var rrf = nlapiLoadRecord('customrecord_taleo_external_hire', rrfNumber);
            var rrfStatus = rrf.getFieldValue('custrecord_taleo_ext_hire_status');
            var recFilled = '';
            if (rrfStatus == 'Filled') {
                filledFrf.setFieldValue('custrecord_frf_details_status_flag', 5); // closed by rmg  //NS-1243 
                filledFrf.setFieldValue('custrecord_frf_details_open_close_status', 5);
                //add the cancellation reason
                // var cancel_reason='Cancelled By Fulfillment Team';
                if (d_cancelDate) {
                    d_cancelDate = nlapiStringToDate(d_cancelDate);
                    filledFrf.setFieldValue('custrecord_frf_details_cancelled_date', new Date());
                }
                filledFrf.setFieldText('custrecord_frf_details_cancellation_reas', cancel_reason);
                //add logged in user closed
                if (recObj.loggedUser) {
                    var i_user = getUserUsingEmailId(recObj.loggedUser);
                    filledFrf.setFieldValue('custrecord_frf_details_rrf_cancelled_by', i_user);
                }
                recFilled = nlapiSubmitRecord(filledFrf);
            }

            nlapiLogExecution('Debug', 'filledFrf in Close Mode', recFilled);
            if (recFilled) {
                response.Data = recFilled;
                response.Status = true;
                return response;
            } else {
                response.Status = false;
                response.Data = 'Error - RRF is not in filled state !!'
                return response;
            }
        }


        if (mode == 'frfEdit') {
            nlapiLogExecution('Debug', 'recObj', recObj);
            var recId = editFRFDetailsRecord(recObj, mode, i_recId);
            var rrfId = recId.rrfNum;
            var rrfDocNum = recId.rrfDocNum;
            var rrfStatus = recId.rrfStatus;
            if (recId.Status == true) {
                response.Data = recId;
                response.Status = true;
                nlapiLogExecution('Debug', 'response ', response);
                return response;
            } else {
                response.Data = recId;
                response.Status = false;
                return response;
            }

            //return recId;
        }
        nlapiLogExecution('Debug', 'response  ', JSON.stringify(response));
    } catch (e) {
        nlapiLogExecution('Debug', 'Error in call', e);
        response.Data = e;
        response.Status = false;
        return response;
    }

}

function createRecord(recObj, mode, i_recordId, i_opp, submitmode) {
    var response = new Response();
    try {
        nlapiLogExecution('debug', 'recObj-create ', JSON.stringify(recObj));
        //if(mode=='edit')
        var a_emp = [];
        // var replacement_emp = [];
        if (recObj.resource) {
            var i_resource = recObj.resource;
        } else {
            i_resource = "";
        }
        if (recObj.loggedUser) {
            var i_user = recObj.loggedUser;
        } else {
            i_user = "";
        }
        nlapiLogExecution('Debug', 'i_user ', i_user);
        if (recObj.account) {
            var i_account = recObj.account;
            nlapiLogExecution('ERROR', 'i_account ', i_account);
        } else {
            i_account = "";
        }

        if (recObj.project) {
            var i_project = recObj.project;
        } else {
            i_project = "";
        }
        if (recObj.opportunity) {
            var i_opportunity_id = recObj.opportunity;
            var customrecord_sfdc_opportunity_recordSearch = nlapiSearchRecord("customrecord_sfdc_opportunity_record", null,
                [
                    ["custrecord_opportunity_id_sfdc", "is", i_opportunity_id]
                ],
                [
                    new nlobjSearchColumn("internalid")
                ]
            );
            for (var i = 0; i < customrecord_sfdc_opportunity_recordSearch.length; i++) {
                var searchObj = customrecord_sfdc_opportunity_recordSearch[i];

                var i_opportunity = searchObj.getValue('internalid');
            }
        } else {
            i_opportunity = "";
        }
        nlapiLogExecution('Debug', 'i_opportunity ' + i_opportunity, 'i_project ' + i_project);
        if (!i_opportunity && !i_project) {
            response.Data = 'Both Project and Opportunity cannot be blank';
            response.Status = false;
            return response;
        }
        if (recObj.practice) {
            var i_practice = recObj.practice;
        } else {
            i_practice = "";
        }

        if (recObj.emplevel) {
            var i_empLevel = recObj.emplevel;
        } else {
            i_empLevel = "";
        }
        nlapiLogExecution('Debug', 'i_empLevel', i_empLevel);

        if (recObj.role) {
            var s_projectRole = recObj.role;
        } else {
            s_projectRole = "";
        }
        nlapiLogExecution('Debug', 's_projectRole', s_projectRole);
        if (recObj.billrole) {
            var b_billabelRole = recObj.billrole;
        } else {
            b_billabelRole = "";
        }
        nlapiLogExecution('Debug', 'b_billabelRole', b_billabelRole);

        if (recObj.frfsource) {
            var i_requesttype = recObj.frfsource;
        } else {
            i_requesttype = "";
        }

        //-------------------------------------------------------------------------------------------------
        //prabhat gupta NIS-1723 04/09/2020 getting fitment type for updating on frf plan record

        if (_logValidation(recObj.frfFitment)) {
            var i_fitmentType = recObj.frfFitment;
        } else {

            nlapiLogExecution('Debug', 'Fitment Type', "Please Select the Fitment Type");

            throw new Error("Please Select the Fitment Type");
        }


        //------------------------------------------------------------------------------------------------

        //-------------------------------------------------------------------------------------------------
        //prabhat gupta NIS-1755 06/10/2020 getting Level for updating on frf plan record

        if (_logValidation(recObj.level)) {
            var i_level = recObj.level;
        } else {

            nlapiLogExecution('Debug', 'Employee Level', "Please Select the Employee Level");

            throw new Error("Please Select the Employee Level");
        }


        //------------------------------------------------------------------------------------------------




        if (recObj.positions) {
            var i_position = recObj.positions;
        } else {
            i_position = "";
        }

        if (recObj.location) {
            var i_location = recObj.location;
        } else {
            response.Data = 'Location is blank';
            response.Status = false;
            return response;
        }
        nlapiLogExecution('Debug', 'i_location ', i_location);
        if (recObj.family) {
            var s_skillFamily = recObj.family;
        } else {
            s_skillFamily = "";
        }

        if (recObj.primaryskills) {
            var s_skills = recObj.primaryskills;
        } else {
            s_skills = "";
        }

        nlapiLogExecution('Debug', 's_skills', s_skills);

        if (recObj.otherskills) {
            var s_otherSkills = recObj.otherskills;
        } else {
            s_otherSkills = "";
        }
        nlapiLogExecution('Debug', 's_otherSkills', s_otherSkills);
        var a_skillArray = new Array();
        var a_OtherSkillArray = new Array();
        a_skillArray = s_skills; //getSkillIds(s_skills);
        nlapiLogExecution('Debug', 'a_skillArray ', a_skillArray);
        a_OtherSkillArray = s_otherSkills; //getSkillIds(s_otherSkills);

        if (recObj.startDate) {
            var d_startDate = recObj.startDate;
        } else {
            d_startDate = "";
        }

        nlapiLogExecution('Debug', 'd_startDate ', d_startDate);
        if (recObj.endDate) {
            var d_endDate = recObj.endDate;
        } else {
            d_endDate = "";
        }

        nlapiLogExecution('Debug', 'd_endDate ', d_endDate);
        if (recObj.allocation) {
            var s_allocation = recObj.allocation;
        } else {
            s_allocation = "";
        }
        if (recObj.billrate) {
            var s_BillRate = recObj.billrate;
        } else {
            s_BillRate = "0";
        }
        if (recObj.criticalrole) {
            var b_criticalRole = recObj.criticalrole;
        } else {
            b_criticalRole = "";
        }

        if (recObj.externalhire) {
            var b_externalHire = recObj.externalhire;
        } else {
            b_externalHire = "";
        }
        if (recObj.suggestedpeople) {
            var s_searchResource = recObj.suggestedpeople;
            nlapiLogExecution('Debug', 's_searchResource ', JSON.stringify(s_searchResource));

            for (var ii = 0; ii < s_searchResource.length; ii++) {
                a_emp.push(s_searchResource[ii])
            }
            nlapiLogExecution('Debug', 'a_emp ', a_emp);
        } else {
            s_searchResource = "";
        }

        //----------------------------------------------------------------------------------------------
        //prabhat gupta


        if (recObj.replacingCandidates) {
            var replace_Resource = recObj.replacingCandidates;
            nlapiLogExecution('Debug', 'replace_Resource ', JSON.stringify(replace_Resource));
            /*
            for(var i =0;i<replace_Resource.length;i++){
            replacement_emp.push(replace_Resource[i])
            }
            nlapiLogExecution('Debug','replacement_emp ',replacement_emp);
            */
        } else {
            var replace_Resource = "";
        }

        //------------------------------------------------------------------------------------------------


        var a_searchResources = new Array();
        //a_searchResources = getSkillIds(s_searchResource);
        nlapiLogExecution('Debug', 's_searchResource Create ', s_searchResource);

        if (recObj.splrequirment) {
            var s_splRequirment = recObj.splrequirment;
        } else {
            s_splRequirment = "";
        }
        nlapiLogExecution('Debug', 's_splRequirment ', s_splRequirment);
        //=================================================================================================
        if (recObj.backuprequired) {
            var b_backupRequired = recObj.backuprequired;
        } else {
            b_backupRequired = "";
        }

        if (recObj.jobtitle) {
            var s_jobTitle = recObj.jobtitle;
        } else {
            s_jobTitle = "";
        }
        if (recObj.jobdescription) {
            var s_jobDesc = recObj.jobdescription;
        } else {
            s_jobDesc = "";
        }
        if (recObj.instruction) {
            var s_instructions = recObj.instruction;
        } else {
            s_instructions = "";
        }
        if (recObj.educationlevel) {
            var i_eduLevel = recObj.educationlevel;
        } else {
            i_eduLevel = "";
        }
        if (recObj.educationprogram) {
            var i_eduProgram = recObj.educationprogram;
        } else {
            i_eduProgram = "";
        }
        if (recObj.employeestatus) {
            var i_empStatus = recObj.employeestatus;
        } else {
            i_empStatus = "";
        }
        if (recObj.firstinterviewer) {
            var s_firstInterviewer = recObj.firstinterviewer;
        } else {
            s_firstInterviewer = "";
        }
        if (recObj.secondinterviewer) {
            var s_secondInterviewer = recObj.secondinterviewer;
        } else {
            s_secondInterviewer = "";
        }
        if (recObj.custinterview) {
            var b_custInterview = recObj.custinterview;
        } else {
            b_custInterview = "";
        }

        if (recObj.custintervieweremail) {
            var s_custIntEmail = recObj.custintervieweremail;
        } else {
            s_custIntEmail = "";
        }
        if (recObj.externalhirereason) {
            var s_externalHireReason = recObj.externalhirereason;
        } else {
            s_externalHireReason = "";
        }
        if (recObj.positionType) {
            var i_positionType = recObj.positionType;
        } else {
            i_positionType = "";
        }
        nlapiLogExecution('Debug', 'i_positionType ', i_positionType);
        var desiredStartDate;
        if (recObj.desiredStartDate) {
            desiredStartDate = recObj.desiredStartDate;
        } else {
            desiredStartDate = '';
        }
        if (recObj.skillCategory) {
            var s_skillCategory = recObj.skillCategory
        }
        else {
            var s_skillCategory = ""
        }
        if (recObj.hireType) {
            var s_hireType = recObj.hireType
        }
        else {
            var s_hireType = ""
        }
        nlapiLogExecution('DEBUG', 's_hireType', s_hireType)
        //----------------------------------------------------------------------------------------------------------
        //prabhat gupta
        if ((i_position != replace_Resource.length) && (i_requesttype == '7' || i_requesttype == '8')) {

            throw new Error("Position is not equal to replacing candidate");
        }


        //-----------------------------------------------------------------------------------------------------------


        //=================================================================================================
        /**If Record already exist then update OR create**/
        if ((i_recordId) && mode == 'edit') {
            var recObject = nlapiLoadRecord('customrecord_frf_plan_details', i_recordId);
            nlapiLogExecution('Debug', 'In Edit Mode', recObject);
        } else if (mode == 'save') {
            var recObject = nlapiCreateRecord('customrecord_frf_plan_details');
        } else {
            nlapiLogExecution('Debug', 'error ' + i_recordId, mode);
        }
        if (_logValidation(desiredStartDate))
            recObject.setFieldValue('custrecord_desired_start_date', desiredStartDate);

        if (_logValidation(i_positionType))
            recObject.setFieldValue('custrecord_frf_plan_details_position_typ', i_positionType);
        if (_logValidation(i_resource))
            recObject.setFieldValue('custrecord_frf_plan_details_emp_notice_r', i_resource);
        if (_logValidation(i_user))
            recObject.setFieldValue('custrecord_frf_plan_details_created_by', i_user);
        if (_logValidation(i_account))
            recObject.setFieldValue('custrecord_frf_plan_details_ns_account', i_account);

        if (i_project)
            recObject.setFieldValue('custrecord_frf_plan_details_ns_project', i_project);
        else
            recObject.setFieldValue('custrecord_frf_plan_details_opp_id', i_opportunity)

        if (i_practice)
            recObject.setFieldValue('custrecord_frf_plan_details_practice', i_practice);
        if (i_empLevel)
            recObject.setFieldText('custrecord_frf_plan_details_emp_lvl', i_empLevel);
        if (s_projectRole)
            recObject.setFieldText('custrecord_frf_plan_details_project_role', s_projectRole);
        if (b_billabelRole)
            recObject.setFieldValue('custrecord_frf_plan_details_bill_role', b_billabelRole);
        if (i_requesttype)
            recObject.setFieldValue('custrecord_frf_plan_request_type', i_requesttype);

        //--------------------------------------------------------------------------------------
        //prabhat gupta NIS-1723 04/09/2020

        if (i_fitmentType)
            recObject.setFieldText('custrecord_frf_plan_fitment_type', i_fitmentType);

        //--------------------------------------------------------------------------------------

        //--------------------------------------------------------------------------------------
        //prabhat gupta NIS-1755 06/10/2020

        if (i_level)
            nlapiLogExecution('Debug', 'Employee Level', i_level);
        recObject.setFieldText('custrecord_frf_plan_level', i_level);

        //--------------------------------------------------------------------------------------

        //----------------------------------------------------------------------------------------------------------
        //prabhat gupta
        if (replace_Resource) {
            recObject.setFieldValues('custrecord_frf_plan_replacement_emp', replace_Resource);

        }
        //----------------------------------------------------------------------------------------------------------

        if (i_position)
            recObject.setFieldValue('custrecord_frf_plan_details_positions', i_position);
        if (i_location)
            recObject.setFieldValue('custrecord_frf_plan_details_location', i_location);
        if (s_skillFamily)
            recObject.setFieldValue('custrecord_frf_plan_details_skill_family', s_skillFamily);
        if (a_skillArray)
            recObject.setFieldValues('custrecord_frf_plan_details_primary', a_skillArray);
        if (a_OtherSkillArray)
            recObject.setFieldValues('custrecord_frf_plan_details_other_skills', a_OtherSkillArray);
        if (d_startDate)
            d_startDate = nlapiStringToDate(d_startDate);
        recObject.setFieldValue('custrecord_frf_plan_details_startdate', d_startDate);
        if (d_endDate)
            d_endDate = nlapiStringToDate(d_endDate);
        nlapiLogExecution("AUDIT", "d_endDate", d_endDate);
        recObject.setFieldValue('custrecord_frf_plan_details_enddate', d_endDate);
        if (s_allocation)
            recObject.setFieldValue('custrecord_frf_plan_details_allocation', s_allocation);
        if (s_BillRate)
            recObject.setFieldValue('custrecord_frf_plan_details_bill_rate', s_BillRate);
        if (b_criticalRole)
            recObject.setFieldValue('custrecord_frf_plan_details_criticalrole', b_criticalRole);
        //if(s_loss)
        //recObject.setFieldValue('custrecord_frf_plan_dollar_loss',s_loss);
        if (b_externalHire)
            recObject.setFieldValue('custrecord_frf_plan_details_externalhire', b_externalHire);
        if (a_emp)
            recObject.setFieldValues('custrecord_frf_plan_details_suggest_res', a_emp);



        if (s_splRequirment)
            recObject.setFieldValue('custrecord_frf_plan_details_special_req', s_splRequirment);
        nlapiLogExecution("AUDIT", "a_emp", a_emp);
        if (i_opp)
            recObject.setFieldValue('custrecord_frf_plan_details_opp_id', i_opp);
        recObject.setFieldValue('custrecord_frf_plan_details_created_date', new Date());
        //=======================================================================================
        if (b_backupRequired)
            recObject.setFieldValue('custrecord_frf_pla_det_backup_require', b_backupRequired);
        if (s_jobTitle)
            recObject.setFieldValue('custrecord_frf_plan_job_title_rrf', s_jobTitle);
        if (s_jobDesc)
            recObject.setFieldValue('custrecord_frf_plan_desc_rrf', s_jobDesc);
        if (s_instructions)
            recObject.setFieldValue('custrecord_frf_plan_instruction_rrf', s_instructions);
        if (i_eduLevel)
            recObject.setFieldValue('custrecord_frf_plan_edu_lvl_rrf', i_eduLevel);
        if (i_eduProgram)
            recObject.setFieldValue('custrecord_frf_plan_edu_pro_rrf', i_eduProgram);
        if (i_empStatus)
            recObject.setFieldValue('custrecord_frf_plan_emp_status_rrf', i_empStatus);
        if (s_firstInterviewer)
            recObject.setFieldValue('custrecord_frf_plan_first_inte_rrf', s_firstInterviewer);
        if (s_secondInterviewer)
            recObject.setFieldValue('custrecord_frf_plan_sec_inte_rrf', s_secondInterviewer);
        if (b_custInterview)
            recObject.setFieldValue('custrecord_frf_plan_cus_inter_rrf', b_custInterview);
        if (s_custIntEmail)
            recObject.setFieldValue('custrecord_frf_plan_cust_inter_email_rrf', s_custIntEmail);
        if (s_externalHireReason)
            recObject.setFieldValue('custrecord_frf_plan_reason_rrf', s_externalHireReason);
        if (s_skillCategory)
            recObject.setFieldValue('custrecord_frf_plan_details_skill_class', s_skillCategory);
        if (s_hireType)
            recObject.setFieldValue('custrecord_frf_plan_details_hire_type', s_hireType);
        //=======================================================================================
        var data = {};
        var recordId = nlapiSubmitRecord(recObject);
        response.Data = recordId;
        if (recordId)
            response.Status = true;
        nlapiLogExecution('Debug', 'recordId Create Mode ', recordId);
        nlapiLogExecution('Debug', 'submitmode 2', submitmode);
        var submitmode = recObj.submitmode;
        nlapiLogExecution('Debug', 'submitmode 2', submitmode);
        if (submitmode == "submit") {
            /*var projectId = recObj.project;
            nlapiLogExecution("DEBUG", "projectId", projectId);
            var oppId = recObj.opportunity_id;
            nlapiLogExecution("DEBUG", "oppId", oppId);
            var accId = recObj.account;
            nlapiLogExecution("DEBUG", "accId", accId);
            var params = [];
            params["custscript_account"] = accId;
            params["custscript_project_id_create_frf"] = projectId;
            params["custscript_opp_id"] = oppId;
            var status = nlapiScheduleScript("customscript_sch_createfrfdetails", "customdeploy_sch_createfrfdetails", params);
            return status;*/


            try {
                var user = recObj.loggedUser;
                nlapiLogExecution('Debug', 'user3242342342 ', user);
                var projectId = recObj.project;
                nlapiLogExecution("DEBUG", "projectId", projectId);
                var i_opp = recObj.opportunity_id;
                nlapiLogExecution("DEBUG", "i_opp", i_opp);
                var customrecord_sfdc_opportunity_recordSearch = nlapiSearchRecord("customrecord_sfdc_opportunity_record", null,
                    [
                        ["custrecord_opportunity_id_sfdc", "is", i_opp]
                    ],
                    [
                        new nlobjSearchColumn("internalid")
                    ]
                );
                for (var i = 0; i < customrecord_sfdc_opportunity_recordSearch.length; i++) {
                    var searchObj = customrecord_sfdc_opportunity_recordSearch[i];

                    var oppId = searchObj.getValue('internalid');
                }

                var accId = recObj.account;
                nlapiLogExecution("DEBUG", "accId", accId);


                if (projectId) {
                    var jobSearch = nlapiSearchRecord("job", null,
                        [
                            ["internalid", "anyof", projectId]
                        ],
                        [
                            new nlobjSearchColumn("customer"),
                            new nlobjSearchColumn("altname")
                        ]
                    );
                }
                if (jobSearch) {
                    nlapiLogExecution('Debug', 'jobSearch length ', jobSearch.length);
                }
                if (oppId) {
                    var customrecord_sfdc_opportunity_recordSearch = nlapiSearchRecord("customrecord_sfdc_opportunity_record", null,
                        [
                            ["internalid", "anyof", oppId]
                        ],
                        [
                            new nlobjSearchColumn("scriptid").setSort(false),
                            new nlobjSearchColumn("custrecord_opportunity_id_sfdc")
                        ]
                    );
                }
                if (customrecord_sfdc_opportunity_recordSearch) {
                    nlapiLogExecution('Debug', 'customrecord_sfdc_opportunity_recordSearch length ', customrecord_sfdc_opportunity_recordSearch.length);
                }
                if (accId) {
                    var customerSearch = nlapiSearchRecord("customer", null,
                        [
                            ["internalid", "anyof", accId]
                        ],
                        [
                            new nlobjSearchColumn("entityid").setSort(false),
                            new nlobjSearchColumn("altname")
                        ]
                    );
                }
                if (customerSearch) {
                    nlapiLogExecution('Debug', 'customerSearch length ', customerSearch.length);
                }
                if (((jobSearch) || (customrecord_sfdc_opportunity_recordSearch)) && (customerSearch)) {
                    nlapiLogExecution('Debug', 'User   4354 ', user);
                    var params = [];
                    params["custscript_account"] = accId;
                    params["custscript_project_id_create_frf"] = projectId;
                    params["custscript_opp_id"] = oppId;
                    params["custscript_user_id"] = user;
                    params["custscript_frf_plan_id"] = recordId;
                    nlapiLogExecution('Debug', 'Scheduled Script ', 'Scheduled Script');
                    var sch_data = nlapiScheduleScript("customscript_sch_createfrfdetails", null, params);
                    nlapiLogExecution('Debug', 'sch_data', JSON.stringify(sch_data));
                    //	response.Data = sch_data;
                    //	nlapiLogExecution('Debug','response.data',JSON.stringify(response.Data));
                    response.Status = true;
                    nlapiLogExecution('Debug', 'response.Status', response.Status);
                    response.recordId = recordId;
                    nlapiLogExecution('Debug', 'response.recordId', response.recordId);
                    nlapiLogExecution('Debug', 'response', JSON.stringify(response));
                    return response;
                } else {
                    throw 'Account OR Project OR Opportunity Cannot be blank!!';
                    return false;
                }
            } catch (err) {
                nlapiLogExecution('ERROR', 'postRESTlet', err);
                response.Data = err;
                response.Status = false;
                return response;
            }
        }
        return response;
    } catch (error) {
        nlapiLogExecution('Debug', 'Error In creating Record', error);
        response.Data = error;
        response.Status = false;
        return response;
    }
}

function getSkillIds(s_skills) {
    var resultArray = new Array();
    if (_logValidation(s_skills)) {
        nlapiLogExecution('Debug', 's_skills in function', s_skills);
        var temp = s_skills.split(',');
        for (var i = 0; i < temp.length; i++) {
            resultArray.push(temp[i]);
        }
    }
    return resultArray;
}

function _logValidation(value) {
    if (value != null && value != 'null' && value != '' && value != undefined && value != undefined && value != 'undefined' && value.toString() != 'NaN' && value != NaN) {
        return true;
    } else {
        return false;
    }
}
//=============================================================================
function editFRFDetailsRecord(recObj, mode, i_recordId) {
    var rrfNum, rrfStatus, rrfDocNum;
    var response = new Response();
    try {
        var a_emp = new Array;
        if (recObj.resource) {
            var i_resource = recObj.resource;
        } else {
            i_resource = "";
        }
        if (recObj.loggedUser) {
            var i_user = recObj.loggedUser;
            //nlapiLogExecution('debug','i_user ',i_user);
            var i_user = getUserUsingEmailId(i_user);
        } else {
            i_user = "";
        }
        nlapiLogExecution('Debug', 'i_user ', i_user);
        if (recObj.editedDate) {
            var d_lastUpdatedOn = recObj.editedDate;
        } else {
            d_lastUpdatedOn = "";
        }

        if (recObj.account) {
            var i_account = recObj.account;
        } else {
            i_account = "";
        }

        //if(i_account == 'selected')
        //	i_account = '';

        if (recObj.project) {
            var i_project = recObj.project;
        } else {
            i_project = "";
        }

        if (i_project == 'selected')
            i_project = '';

        if (recObj.practice) {
            var i_practice = recObj.practice;
        } else {
            i_practice = "";
        }

        if (recObj.role) {
            var s_projectRole = recObj.role;
        } else {
            s_projectRole = "";
        }
        nlapiLogExecution('Debug', 's_projectRole', s_projectRole);
        if (recObj.billrole) {
            var b_billabelRole = recObj.billrole;
        } else {
            b_billabelRole = "";
        }
        if (b_billabelRole == true)
            b_billabelRole = 'T';
        else
            b_billabelRole = 'F';

        nlapiLogExecution('Debug', 'b_billabelRole', b_billabelRole);
        if (recObj.frfsource) {
            var s_frfSource = recObj.frfsource;
        } else {
            s_frfSource = "";
        }

        if (recObj.positions) {
            var i_position = recObj.positions;
        } else {
            i_position = "";
        }

        if (recObj.location) {
            var i_location = recObj.location;
        } else {
            response.Data = 'Location is blank';
            response.Status = false;
            return response;
        }

        if (recObj.family) {
            var s_skillFamily = recObj.family;
        } else {
            s_skillFamily = "";
        }

        if (recObj.primaryskills) {
            var s_skills = recObj.primaryskills;
        } else {
            s_skills = "";
        }

        nlapiLogExecution('Debug', 's_skills', s_skills);

        if (recObj.otherskills) {
            var s_otherSkills = recObj.otherskills;
        } else {
            s_otherSkills = "";
        }

        var a_skillArray = new Array();
        var a_OtherSkillArray = new Array();
        //a_skillArray = getSkillIds(s_skills);
        //a_OtherSkillArray = getSkillIds(s_otherSkills);

        if (recObj.startDate) {
            var d_startDate = recObj.startDate;
        } else {
            d_startDate = "";
        }

        nlapiLogExecution('Debug', 'd_startDate ', d_startDate);
        if (recObj.endDate) {
            var d_endDate = recObj.endDate;
        } else {
            d_endDate = "";
        }

        nlapiLogExecution('Debug', 'd_endDate ', d_endDate);
        if (recObj.allocation) {
            var s_allocation = recObj.allocation;
        } else {
            s_allocation = "";
        }
        if (recObj.billrate) {
            var s_BillRate = recObj.billrate;
        } else {
            s_BillRate = "0";
        }
        nlapiLogExecution('audit', 's_BillRate ', s_BillRate);
        if (recObj.criticalrole) {
            var b_criticalRole = recObj.criticalrole;
        } else {
            b_criticalRole = "";
        }

        if (recObj.dollarloss) {
            var s_loss = recObj.dollarloss;
        } else {
            s_loss = "";
        }
        if (recObj.externalhire) {
            var b_externalHire = recObj.externalhire;
        } else {
            b_externalHire = "";
        }
        nlapiLogExecution('Debug', 'b_externalHire', b_externalHire);

        if (recObj.suggestedpeople) {
            var s_searchResource = recObj.suggestedpeople;
            nlapiLogExecution('Debug', 's_searchResource ', JSON.stringify(s_searchResource));

            for (var ii = 0; ii < s_searchResource.length; ii++) {
                a_emp.push(s_searchResource[ii].id)
            }
            nlapiLogExecution('Debug', 'a_emp ', a_emp);
        } else {
            s_searchResource = "";
        }

        var a_searchResources = new Array();
        //a_searchResources = getSkillIds(s_searchResource);
        //nlapiLogExecution('Debug','a_searchResources ',a_searchResources);
        if (recObj.splrequirment) {
            var s_splRequirment = recObj.splrequirment;
        } else {
            s_splRequirment = "";
        }

        //=================================================================================================
        if (recObj.backuprequired) {
            var b_backupRequired = recObj.backuprequired;
        } else {
            b_backupRequired = "";
        }
        if (recObj.jobtitle) {
            var s_jobTitle = recObj.jobtitle;
        } else {
            s_jobTitle = "";
        }
        if (recObj.jobdescription) {
            var s_jobDesc = recObj.jobdescription;
        } else {
            s_jobDesc = "";
        }
        if (recObj.instruction) {
            var s_instructions = recObj.instruction;
        } else {
            s_instructions = "";
        }
        if (recObj.educationlevel) {
            var i_eduLevel = recObj.educationlevel;
        } else {
            i_eduLevel = "";
        }
        if (recObj.educationprogram) {
            var i_eduProgram = recObj.educationprogram;
        } else {
            i_eduProgram = "";
        }
        if (recObj.employeestatus) {
            var i_empStatus = recObj.employeestatus;
        } else {
            i_empStatus = "";
        }
        if (recObj.firstinterviewer) {
            var s_firstInterviewer = recObj.firstinterviewer;
        } else {
            s_firstInterviewer = "";
        }
        if (recObj.secondinterviewer) {
            var s_secondInterviewer = recObj.secondinterviewer;
        } else {
            s_secondInterviewer = "";
        }
        if (recObj.custinterview) {
            var b_custInterview = recObj.custinterview;
        } else {
            b_custInterview = "";
        }

        if (recObj.custintervieweremail) {
            var s_custIntEmail = recObj.custintervieweremail;
        } else {
            s_custIntEmail = "";
        }
        if (recObj.externalhirereason) {
            var s_externalHireReason = recObj.externalhirereason;
        } else {
            s_externalHireReason = "";
        }
        if (recObj.positionType) {
            var i_posiotionType = recObj.positionType;
        } else {
            i_posiotionType = "";
        }
        if (recObj.splRequirements) {
            var s_splRequirment = recObj.splRequirements;
        } else {
            s_splRequirment = "";
        }
        nlapiLogExecution('Debug', 'i_posiotionType ', i_posiotionType);
        if (recObj.rrfNumber) {
            var s_rrfNumber = recObj.rrfNumber;
        } else {
            s_rrfNumber = "";
        }
        if (recObj.requestType) {
            var i_requesttype = recObj.requestType;
        } else {
            i_requesttype = "";
        }

        //---------------------------------------------------------------------------------------------------------
        //prabhat gupta NIS-1723 08/09/2020 added fitment type for update request in frf details record
        if (_logValidation(recObj.frfFitment)) {
            var i_fitmentType = recObj.frfFitment;
        } else {
            throw new Error("Please Select the Fitment Type");
        }
        //--------------------------------------------------------------------------------------------------------------	

        //---------------------------------------------------------------------------------------------------------
        //prabhat gupta NIS-1755 06/10/2020 
        if (_logValidation(recObj.level)) {
            var i_level = recObj.level;
        } else {
            throw new Error("Please Select the Employee Level");
        }
        //--------------------------------------------------------------------------------------------------------------		
        var desiredStartDate = '';
        if (recObj.desiredStartDate) {
            desiredStartDate = recObj.desiredStartDate;
        } else {
            desiredStartDate = "";
        }

        var redDate = '';
        if (recObj.redDate) {
            redDate = recObj.redDate;
        } else {
            redDate = '';
        }
        nlapiLogExecution('debug', 'redDate', redDate);
        if (recObj.skillCategory) {
            var s_skillCategory = recObj.skillCategory
        }
        else {
            var s_skillCategory = "";
        }
        nlapiLogExecution('debug', 's_skillCategory ', s_skillCategory);
        if (recObj.hireType) {
            var s_hireType = recObj.hireType
        }
        else {
            var s_hireType = "";
        }
        nlapiLogExecution('debug', 's_hireType ', s_hireType);
        //=================================================================================================
        /**If Record already exist then update OR create**/
        var recObject = nlapiLoadRecord('customrecord_frf_details', i_recordId);
        var i_rrfNumber = recObject.getFieldValue('custrecord_frf_details_rrf_number');
        nlapiLogExecution('debug', 'i_rrfNumber ', i_rrfNumber);
        var getRRFStatus = recObject.getFieldValue("custrecord_frf_details_talep_created");
        nlapiLogExecution('debug', 'recObject load record ', recObject);
        //if(_logValidation(i_account))
        //recObject.setFieldValue('custrecord_frf_details_account',i_account);
        //if(_logValidation(i_project))
        //recObject.setFieldValue('custrecord_frf_details_project',i_project);
        var old_ExtHire = recObject.getFieldValue('custrecord_frf_details_external_hire');
        nlapiLogExecution('debug', 'old_ExtHire  ', old_ExtHire);
        var old_rrfNumber = recObject.getFieldValue('custrecord_frf_details_rrf_number');
        nlapiLogExecution('debug', 'old_rrfNumber  ', old_rrfNumber);

        if (_logValidation(i_resource))
            recObject.setFieldValue('custrecord_frf_emp_notice_rotation', i_resource);
        if (_logValidation(desiredStartDate))
            recObject.setFieldValue('custrecord_frf_desired_start_date', desiredStartDate);
        if (_logValidation(i_user))
            recObject.setFieldValue('custrecord_frf_details_last_updatedby', i_user);
        if (i_practice)
            recObject.setFieldValue('custrecord_frf_details_res_practice', i_practice);
        if (s_projectRole)
            recObject.setFieldText('custrecord_frf_details_role', s_projectRole);
        if (b_billabelRole)
            recObject.setFieldValue('custrecord_frf_details_billiable', b_billabelRole);
        if (s_frfSource)
            recObject.setFieldText('custrecord_frf_details_source', s_frfSource);
        if (i_location)
            recObject.setFieldValue('custrecord_frf_details_res_location', i_location);
        if (s_skillFamily)
            recObject.setFieldValue('custrecord_frf_details_skill_family', s_skillFamily);
        if (s_skills)
            recObject.setFieldValues('custrecord_frf_details_primary_skills', s_skills);
        if (s_skillCategory)
            recObject.setFieldValue('custrecord_frf_details_skill_class', s_skillCategory);
        if (s_hireType)
            recObject.setFieldValue('custrecord_frf_details_hire_type', s_hireType);
        if (a_OtherSkillArray)
            recObject.setFieldValues('custrecord_frf_details_secondary_skills', s_otherSkills); //a_OtherSkillArray
        if (d_startDate) {
            d_startDate = nlapiStringToDate(d_startDate);
            recObject.setFieldValue('custrecord_frf_details_start_date', d_startDate);
        }
        if (d_endDate) {
            d_endDate = nlapiStringToDate(d_endDate);
            recObject.setFieldValue('custrecord_frf_details_end_date', d_endDate);
        }
        if (s_allocation)
            recObject.setFieldValue('custrecord_frf_details_allocation', s_allocation);
        if (s_BillRate)
            recObject.setFieldValue('custrecord_frf_details_bill_rate', s_BillRate);
        if (b_criticalRole)
            recObject.setFieldValue('custrecord_frf_details_critical_role', b_criticalRole);
        if (s_loss)
            recObject.setFieldValue('custrecord_frf_details_dollar_loss', s_loss);
        if (b_externalHire)
            recObject.setFieldValue('custrecord_frf_details_external_hire', b_externalHire);
        //if(a_emp)
        //recObject.setFieldValues('custrecord_frf_details_suggest_pref_mat',a_emp);
        if (s_splRequirment)
            recObject.setFieldValue('custrecord_frf_details_special_req', s_splRequirment);
        if (b_backupRequired)
            recObject.setFieldValue('custrecord_frf_details_backup_require', b_backupRequired);
        if (s_jobTitle)
            recObject.setFieldValue('custrecord_frf_details_job_title', s_jobTitle);
        nlapiLogExecution('debug', 'recObject load s_jobTitle ', s_jobTitle);
        if (s_jobDesc)
            recObject.setFieldValue('custrecord_frf_details_job_descriptions', s_jobDesc);
        if (s_instructions)
            recObject.setFieldValue('custrecord_frf_details_instruction_team', s_instructions);
        if (i_eduLevel)
            recObject.setFieldValue('custrecord_frf_details_edu_lvl', i_eduLevel);
        if (i_eduProgram)
            recObject.setFieldValue('custrecord_frf_details_edu_program', i_eduProgram);
        if (i_empStatus)
            recObject.setFieldValue('custrecord_frf_details_emp_status', i_empStatus);
        if (s_firstInterviewer)
            recObject.setFieldValue('custrecord_frf_details_first_interviewer', s_firstInterviewer);
        if (s_secondInterviewer)
            recObject.setFieldValue('custrecord_frf_details_sec_interviewer', s_secondInterviewer);
        if (b_custInterview)
            recObject.setFieldValue('custrecord_frf_details_cust_interview', b_custInterview);
        if (s_custIntEmail)
            recObject.setFieldValue('custrecord_frf_details_cust_inter_email', s_custIntEmail);
        if (s_externalHireReason)
            recObject.setFieldValue('custrecord_frf_details_reason_external', s_externalHireReason);
        if (d_lastUpdatedOn)
            recObject.setFieldValue('custrecord_frf_details_last_updated_on', d_lastUpdatedOn);
        if (_logValidation(i_posiotionType))
            recObject.setFieldValue('custrecord_frf_details_position_type', i_posiotionType);
        if (i_requesttype)
            recObject.setFieldValue('custrecord_frf_type', i_requesttype);

        //--------------------------------------------------------------------------------------
        //prabhat gupta NIS-1723 04/9/2020
        if (i_fitmentType) {
            recObject.setFieldText('custrecord_frf_details_fitment_type', i_fitmentType);
        }

        //-----------------------------------------------------------------------------------------

        //--------------------------------------------------------------------------------------
        //prabhat gupta NIS-1755 06/10/2020
        if (i_level) {
            nlapiLogExecution('Debug', 'Employee Level', i_level);
            recObject.setFieldText('custrecord_frf_details_level', i_level);
        }

        //-----------------------------------------------------------------------------------------

        //=======================================================================================

        if (b_externalHire == 'T' && old_ExtHire == 'F') {
            if (!i_rrfNumber) {
                rrfNum = createTaleoExternalHireRecord(i_recordId, getRRFStatus);
                nlapiLogExecution('Debug', 'rrfNum', rrfNum);
                if (rrfNum) {
                    var fields = ['custrecord_taleo_external_rrf_number', 'custrecord_taleo_ext_hire_status']
                    var columns = nlapiLookupField('customrecord_taleo_external_hire', rrfNum, fields);
                    rrfDocNum = columns.custrecord_taleo_external_rrf_number
                    rrfStatus = columns.custrecord_taleo_ext_hire_status
                    nlapiLogExecution('Debug', 'rrrfDocNum', rrfDocNum);
                    nlapiLogExecution('Debug', 'rrfStatus', rrfStatus);
                }
                recObject.setFieldValue('custrecord_frf_details_rrf_number', rrfNum);
            }
        } else if (b_externalHire == 'F' && old_ExtHire == 'T') {
            //nlapiDeleteRecord('customrecord_taleo_external_hire',parseInt(s_rrfNumber));
        }

        if (redDate) {
            recObject.setFieldValue('custrecord_frf_details_red_date', redDate);
        }

        //=======================================================================================
        var recordId = nlapiSubmitRecord(recObject);
        nlapiLogExecution('Debug', 'recordId ', recordId);
        response.data = recordId;
        response.rrfNum = rrfNum;
        response.rrfDocNum = rrfDocNum;
        response.rrfStatus = rrfStatus
        response.Status = true;
        nlapiLogExecution('Debug', 'response', JSON.stringify(response));
        return response;
    } catch (error) {
        nlapiLogExecution('Debug', 'Error In creating Record', error);
        response.Data = error;
        response.Status = false;
        return response;
    }
}

function createTaleoExternalHireRecord(i_osfRec, getRRFStatus) {
    var taleoExternalHireRec = nlapiCreateRecord("customrecord_taleo_external_hire");
    taleoExternalHireRec.setFieldValue("custrecord_taleo_ext_hire_frf_details", i_osfRec);
    //if(getRRFStatus == "T")
    {
        taleoExternalHireRec.setFieldValue("custrecord_taleo_ext_hire_status", "To Be Approved");
    }
    return nlapiSubmitRecord(taleoExternalHireRec);
}