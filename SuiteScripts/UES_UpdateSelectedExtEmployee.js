							
// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
/*
   	Script Name : UES_UpdateSelectedExtEmployee.js
	Author      : Ashish Pandit
	Date        : 05 July 2018
    Description : User Event to update External hire employee and Joining date to FRF Details record   


	Script Modification Log:

	-- Date --			-- Modified By --				--Requested By--				-- Description --


Below is a summary of the process controls enforced by this script file.  The control logic is described
more fully, below, in the appropriate function headers and code blocks.


     BEFORE LOAD
		- beforeLoadRecord(type)



     BEFORE SUBMIT
		- beforeSubmitRecord(type)


     AFTER SUBMIT
		- afterSubmitRecord(type)



     SUB-FUNCTIONS
		- The following sub-functions are called by the above core functions in order to maintain code
            modularization:

               - NOT USED

*/
}
// END SCRIPT DESCRIPTION BLOCK  ====================================



// BEGIN GLOBAL VARIABLE BLOCK  =====================================
{
	//  Initialize any Global Variables, in particular, debugging variables...

}
// END GLOBAL VARIABLE BLOCK  =======================================



// BEGIN BEFORE LOAD ==================================================

function afterSubmitUpdateExtEmployee(type)
{
	nlapiLogExecution('Debug','type ',type);
	if(type == 'edit' || type == 'xedit')
	{
		try
		{
			var oldRecord = nlapiGetOldRecord();
			//var i_OldResource = oldRecord.getFieldValue('custrecord_taleo_selected_employee');
			var i_OldResource = oldRecord.getFieldValue('custrecord_offered_emp_email');
			nlapiLogExecution('Debug','i_OldResource ',i_OldResource);
			var s_OldStatus = oldRecord.getFieldValue('custrecord_taleo_ext_hire_status');
			var recId = nlapiGetRecordId();
			var recType = nlapiGetRecordType();
			var recordObject = nlapiLoadRecord(recType,recId);
			//var newRecord = nlapiGetNewRecord();
			//var i_NewResource = recordObject.getFieldValue('custrecord_taleo_selected_employee');
			var i_NewResource = recordObject.getFieldValue('custrecord_offered_emp_email');
			var s_NewStatus = recordObject.getFieldValue('custrecord_taleo_ext_hire_status');
			
			var d_joiningDate = recordObject.getFieldValue('custrecord_taleo_emp_joining_date');
			nlapiLogExecution('Debug','i_NewResource ',i_NewResource);
			if(!i_OldResource && i_NewResource)
			{
				var i_frfDetails = recordObject.getFieldText('custrecord_taleo_ext_hire_frf_details');
				if(_logValidation(i_frfDetails))
				{
					var employeeSearch = nlapiSearchRecord("employee",null,
					[
						[["custentity_personalemailid","is",i_NewResource],"OR",
						["email","is",i_NewResource]],'AND',
                      ["custentity_employee_inactive","is",'F']
					  // ["entityid","contains",i_NewResource]
					], 
					[
					   new nlobjSearchColumn("entityid").setSort(false), 
					   new nlobjSearchColumn("internalid")
					]
					);
					if(employeeSearch)
					{
						var i_selectedEmp = employeeSearch[0].getValue("internalid");
						nlapiLogExecution('Debug','i_selectedEmp ',i_selectedEmp);
						var i_url_servlet = 'https://3883006.extforms.netsuite.com/app/site/hosting/scriptlet.nl?script=1908&deploy=1&compid=3883006&h=b0880a9f185a4e6297fe&custpage_frfnumber='+i_frfDetails+'&custpage_employee='+i_selectedEmp+'&custpage_joindate='+d_joiningDate;
						var responce = nlapiRequestURL(i_url_servlet);
						nlapiLogExecution('Debug','responce ',responce);
					}
					else
					{
						nlapiLogExecution('Debug','Employee Not Fond In Netsuite');
					}
				}
			}
			/****Code To Update External Hire into Internal Hire****/
			if(s_OldStatus!='Rejected' && s_NewStatus == 'Rejected')
			{
				var i_frfNo = recordObject.getFieldValue('custrecord_taleo_ext_hire_frf_details');
				if(_logValidation(i_frfNo))
				{
					var recObj = nlapiLoadRecord('customrecord_frf_details',i_frfNo);
					recObj.setFieldValue('custrecord_frf_details_external_hire','F');// Set External to Internal 
					var s_creator = recObj.getFieldText('custrecord_frf_details_created_by');
					var i_creator = recObj.getFieldValue('custrecord_frf_details_created_by');
					var i_project = recObj.getFieldValue('custrecord_frf_details_project');
					var s_project = recObj.getFieldText('custrecord_frf_details_project');
					var i_opp = recObj.getFieldValue('custrecord_frf_details_opp_id');
					var frfNumber = recObj.getFieldValue('custrecord_frf_details_frf_number');
					
					var ccArray = new Array();
					var toArray = new Array();
					if(_logValidation(i_opp))
					{
						s_project = nlapiLookupField('customrecord_sfdc_opportunity_record',i_opp,'custrecord_opportunity_name_sfdc');
						s_oppId = nlapiLookupField('customrecord_sfdc_opportunity_record',i_opp,'custrecord_opportunity_id_sfdc');
						var s_oppName = s_oppId +', '+s_project;
						var i_pmOpp = nlapiLookupField('customrecord_sfdc_opportunity_record',i_opp,'custrecord_sfdc_opp_pm');
						var i_dmOpp = nlapiLookupField('customrecord_sfdc_opportunity_record',i_opp,'custrecord_sfdc_opportunity_dm');
						var i_cpOpp = nlapiLookupField('customrecord_sfdc_opportunity_record',i_opp,'custrecord_sfdc_client_partner');
						if(i_creator)
							toArray.push(i_creator);
						//if(i_pmOpp)
							//toArray.push(i_pmOpp);
						//if(i_dmOpp)
							//toArray.push(i_dmOpp);
						if(i_pmOpp)
							var s_pmOppEmail = nlapiLookupField('employee',i_pmOpp,'email');
						if(i_dmOpp)
							var s_dmOppEmail = nlapiLookupField('employee',i_dmOpp,'email');
						
						if(s_pmOppEmail)
							ccArray.push(s_pmOppEmail);
						if(s_dmOppEmail)
							ccArray.push(s_dmOppEmail);
						ccArray = removearrayduplicate(ccArray);
						ccArray.push('fuel.support@brillio.com');
					}
					else if(_logValidation(i_project))
					{
						var i_pm = nlapiLookupField('job',i_project,'custentity_projectmanager');
						var s_pm = nlapiLookupField('job',i_project,'custentity_projectmanager',true);
						var i_dm = nlapiLookupField('job',i_project,'custentity_deliverymanager');
						var i_cp = nlapiLookupField('job',i_project,'custentity_clientpartner');
						
						if(i_creator)
							toArray.push(i_creator);
						if(i_dm)
						var s_dmEmail  = nlapiLookupField('employee',i_dm,'email');
						if(s_dmEmail)	
							ccArray.push(s_dmEmail);
						if(i_pm)
						var s_pmEmail = nlapiLookupField('employee',i_pm,'email');
						if(s_pmEmail)	
							ccArray.push(s_pmEmail);
					
						ccArray.push('fuel.support@brillio.com');
						nlapiLogExecution('Debug','ccArray ',ccArray);
						ccArray = removearrayduplicate(ccArray);
					}
					var submitRecord = nlapiSubmitRecord(recObj);
					/*if(_logValidation(i_creator))
					{
						nlapiLogExecution('Debug','i_pm ',i_pm);
						var records = new Array();
						var emailSubject = frfNumber+': FUEL Notification: External hire request (RRF) rejected';
						var emailBody = 'Dear '+s_creator+' <br>The external hire request for FRF '+frfNumber+' has been rejected by the RRF approver in Taleo.<br>System has converted this FRF to internal and enabled internal resource soft locking option, kindly do the needful. <br><br>Brillio-FUEL<br>This email was sent from a notification only address.<br>If you need further assistance, please write to fuel.support@brillio.com';
						nlapiLogExecution('Debug','emailBody ',emailBody);
						records['entity'] = ['94862','41571'];
						//nlapiSendEmail(154256, 94862, emailSubject,emailBody, ccArray, 'deepak.srinivas@brillio.com', records);
						nlapiSendEmail(154256, i_creator, emailSubject,emailBody, ccArray, 'deepak.srinivas@brillio.com', records);
					}
					else*/ if(toArray.length>0)
					{
						var records = new Array();
						var emailSubject = frfNumber+': FUEL Notification: External hire request (RRF) rejected';
						var emailBody = 'Dear '+s_creator+' <br>The external hire request for FRF '+frfNumber+' has been rejected by the RRF approver in Taleo.<br>System has converted this FRF to internal and enabled internal resource soft locking option, kindly do the needful. <br><br>Brillio-FUEL<br>This email was sent from a notification only address.<br>If you need further assistance, please write to fuel.support@brillio.com';
						records['entity'] = ['94862','41571'];
						//nlapiSendEmail(154256, 94862, emailSubject,emailBody, null, 'deepak.srinivas@brillio.com', records);
						nlapiSendEmail(154256, toArray, emailSubject,emailBody, ccArray, 'deepak.srinivas@brillio.com', records);
					}
					
				}
			}
		}
		catch(error)
		{
			nlapiLogExecution('Debug','Error ',error);
		}
	}
}

// END BEFORE LOAD ====================================================


function _logValidation(value) 
{
 if(value != null && value!= 'null' && value != '' && value != undefined && value != undefined && value != 'undefined' && value.toString() != 'NaN' && value != NaN) 
 {
  return true;
 }
 else 
 { 
  return false;
 }
}
// END FUNCTION =====================================================

function removearrayduplicate(array) {
    var newArray = new Array();
    label: for (var i = 0; i < array.length; i++) {
          for (var j = 0; j < array.length; j++) {
                if (newArray[j] == array[i])
                      continue label;
          }
          newArray[newArray.length] = array[i];
    }
    return newArray;
}




