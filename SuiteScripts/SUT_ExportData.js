/**
 * @author Jayesh
 */

function suiteletFunction_xportData(request, response)
{
	try
	{
		var d_startDate =  request.getParameter('d_startDate');
		var d_endDate =  request.getParameter('d_endDate');
		//d_endDate = nlapiStringToDate(d_endDate);
		var i_subsidiary =  request.getParameter('i_subsidiary');
		i_subsidiary = i_subsidiary.trim();
		var i_pro_type = request.getParameter('i_pro_type');
		i_pro_type = i_pro_type.trim();
		var i_emp_type = request.getParameter('i_emp_type');
		i_emp_type = i_emp_type.trim();
		var s_pro_bill_type = request.getParameter('s_pro_bill_type');
		s_pro_bill_type = s_pro_bill_type.trim();
		var i_view_type = request.getParameter('i_view_type');
		i_view_type = i_view_type.trim();
		var i_emp_category = request.getParameter('i_emp_category');
		i_emp_category = i_emp_category.trim();
		
		var d_todays_date = nlapiStringToDate(nlapiDateToString(new Date()));
		var a_filters_allocation = [ ['startdate', 'onorbefore', d_todays_date ], 'and',
									['enddate', 'onorafter', d_todays_date ], 'and',
									['employee.custentity_employee_inactive', 'is', 'F'], 'and',
									['employee.subsidiary', 'is', parseInt(i_subsidiary)], 'and',
									['employee.employeetype', 'is', parseInt(i_emp_type)], 'and',
									['employee.custentity_persontype', 'is', parseInt(i_emp_category)], 'and',
									['job.jobtype', 'anyof', parseInt(i_pro_type)], 'and',
									['job.jobbillingtype', 'anyof', s_pro_bill_type]
								  ];
						
		var a_columns = new Array();
		a_columns[0] = new nlobjSearchColumn('resource',null,'group');
		
		var a_project_allocation_result = searchRecord('resourceallocation', null, a_filters_allocation, a_columns);
		if (a_project_allocation_result)
		{
			var s_monthTimesheetTable = '';
			var a_active_emp_list = new Array();
			var a_emp_list = new Array();
			
			nlapiLogExecution('audit', 'resource count length:- ' + a_project_allocation_result.length);
			
			for (var i_allocation_index = 0; i_allocation_index < a_project_allocation_result.length; i_allocation_index++) {
				a_active_emp_list.push({
					Emp_id: a_project_allocation_result[i_allocation_index].getValue('resource', null, 'group'),
					Emp_name: a_project_allocation_result[i_allocation_index].getText('resource', null, 'group')
				});
				
				a_emp_list.push(a_project_allocation_result[i_allocation_index].getValue('resource', null, 'group'));
			}
			
			s_monthTimesheetTable = createMonthTimesheetTable(a_emp_list, a_active_emp_list, d_startDate, d_endDate, i_view_type);
			if (s_monthTimesheetTable) {
				var file = nlapiCreateFile('Not Submitted Hours Data.xls', 'XMLDOC', s_monthTimesheetTable);
				nlapiLogExecution('debug', 'file:- ', file);
				response.setContentType('XMLDOC', 'Not Submitted Hours Data.xls');
				response.write(file.getValue());
			}
		}
	}
	catch(err)
	{
		nlapiLogExecution('ERROR','ERROR MESSAGE:- ',err);
	}
}

function createMonthTimesheetTable(a_emp_list, a_active_emp_list, startDate, endDate, i_view_type) {
	try {
		// create a blank month table
		var table = {};
					
		var d_startDate = nlapiStringToDate(startDate);
		var d_endDate = nlapiStringToDate(endDate);
		var end_week = d_endDate.getWeekNumber();
		
		var i_days_diff = getDatediffIndays(startDate,endDate);
		var i_total_weeks = 0;
		
		if(i_view_type == 2)
		{
			for(var i_emp_index=0; i_emp_index<a_active_emp_list.length; i_emp_index++)
			{
				for (var i = 0; i<i_days_diff; i++)
				{
					var currentDate = nlapiAddDays(d_startDate, i);
					var s_currentDate = nlapiDateToString(currentDate, 'date');
					
					var i_unique_key = a_active_emp_list[i_emp_index].Emp_id + '_' + currentDate.getDate() + '_' + currentDate.getMonth() + '_' + currentDate.getFullYear();
					
					//var weekOfYear = currentDate.getWeekNumber();
					
					//nlapiLogExecution('audit','week entry:- '+weekOfYear+' ::strt date:-'+d_startDate,currentDate);
					
					if (currentDate > d_endDate) {
						break;
					}
					
					//if (!table[i_unique_key])
					{
						table[i_unique_key] = {
							EmpName: a_active_emp_list[i_emp_index].Emp_name,
						    Week : 'No',
						    Name : currentDate.getWeekName(2)
						};
					}
				}
			}
		}
		else
		{
			for(var i_emp_index=0; i_emp_index<a_active_emp_list.length; i_emp_index++)
			{
				for (var i = 0; i<i_days_diff; i++)
				{
					var currentDate = nlapiAddDays(d_startDate, i);
					var s_currentDate = nlapiDateToString(currentDate, 'date');
					var weekOfYear = currentDate.getWeekNumber();
					
					var i_unique_key = a_active_emp_list[i_emp_index].Emp_id + '_' + weekOfYear;
					
					//nlapiLogExecution('audit','week entry:- '+weekOfYear+' ::strt date:-'+d_startDate,currentDate);
					
					if (weekOfYear > end_week) {
						break;
					}
					
					if (!table[i_unique_key])
					{
						table[i_unique_key] = {
							EmpName: a_active_emp_list[i_emp_index].Emp_name,
						    Week : 'No',
						    Name : currentDate.getWeekName(1)
						};
						
						if(i_emp_index == 0)
						{
							i_total_weeks++;
						}
					}
				}
			}
		}
		
		// get all timesheets within the period
		var a_filters_time = new Array();
		a_filters_time[0] = new nlobjSearchFilter('employee', null, 'anyof', a_emp_list);
		a_filters_time[1] = new nlobjSearchFilter('type', null, 'anyof', 'A');
		a_filters_time[2] = new nlobjSearchFilter('date', null, 'within', startDate,endDate);
					
		var a_columns_time = new Array();
		a_columns_time[0] = new nlobjSearchColumn('date',null,'group');
		a_columns_time[1] = new nlobjSearchColumn('employee',null,'group');
		a_columns_time[2] = new nlobjSearchColumn('durationdecimal',null,'sum');
		a_columns_time[3] = new nlobjSearchColumn('totalhours','timesheet','max');
		
		var timeEntrySearch = searchRecord('timebill', null, a_filters_time, a_columns_time);//Replaced searched record type on 10-04-2020

		if (timeEntrySearch) {
			
			nlapiLogExecution('audit','time entry len:- '+timeEntrySearch.length);
			timeEntrySearch.forEach(function(timeEntry) {
				
				var timeEntryDate = nlapiStringToDate(timeEntry.getValue('date', null, 'group'));
				nlapiLogExecution('audit','date time:- '+timeEntryDate);
				//var s_currentDate = nlapiDateToString(currentDate, 'date');
				//var d_currentDate = new Date(s_currentDate);
				var weekOfYear = timeEntryDate.getWeekNumber();
				var dayOfWeek = timeEntryDate.getDay();
				
				var i_employee = timeEntry.getValue('employee', null, 'group');
				var f_duration = timeEntry.getValue('durationdecimal', null, 'sum');
				var f_timesheet_hours = timeEntry.getValue('totalhours','timesheet','max');
				
				//nlapiLogExecution('audit','week of year:- '+weekOfYear,'timeEntryDate:- '+timeEntryDate+'hours:- '+f_timesheet_hours);
				
				if(i_view_type == 2)
				{
					nlapiLogExecution('audit','full year:- '+timeEntryDate.getFullYear());
					var i_unique_key = i_employee + '_' + timeEntryDate.getDate() + '_' + timeEntryDate.getMonth() + '_' + timeEntryDate.getFullYear();
					table[i_unique_key].Week = f_duration;
				}
				else
				{
					if(timeEntryDate.getDay() != 0)
					{
						var i_unique_key = i_employee + '_' + weekOfYear;
						table[i_unique_key].Week = f_timesheet_hours;
					}
				}
				
			});
		}
		
		var strVar1= '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:ss="urn:schemas-microsoft-com:office:spreadsheet" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40">'+
		'<head>'+
		'<meta http-equiv=Content-Type content="text/html; charset=windows-1252"/>'+
        '<meta name=ProgId content=Excel.Sheet/>'+
        '<meta name=Generator content="Microsoft Excel 11"/>'+
        '<!--[if gte mso 9]><xml>'+
        '<x:excelworkbook>'+
        '<x:excelworksheets>'+
        '<x:excelworksheet=sheet1>'+
        '<x:name>** ESTIMATE FILE**</x:name>'+
        '<x:worksheetoptions>'+
        '<x:selected></x:selected>'+
        '<x:freezepanes></x:freezepanes>'+
        '<x:frozennosplit></x:frozennosplit>'+
        '<x:splithorizontal>** FROZEN ROWS + 1 **</x:splithorizontal>'+
        '<x:toprowbottompane>** FROZEN ROWS + 1 **</x:toprowbottompane>'+
        '<x:splitvertical>** FROZEN COLUMNS + 1 **</x:splitvertical>'+
        '<x:leftcolumnrightpane>** FROZEN COLUMNS + 1**</x:leftcolumnrightpane>'+
		    '<x:activepane>0</x:activepane>'+// 0
		    '<x:panes>'+
			'<x:pane>'+
			'<x:number>3</x:number>'+
			'</x:pane>'+
			'<x:pane>'+
		    '<x:number>1</x:number>'+
		    '</x:pane>'+
		    '<x:pane>'+
			'<x:number>2</x:number>'+
        '</x:pane>'+
        '<x:pane>'+
        '<x:number>0</x:number>'+//1
        '</x:pane>'+
        '</x:panes>'+
        '<x:protectcontents>False</x:protectcontents>'+
        '<x:protectobjects>False</x:protectobjects>'+
        '<x:protectscenarios>False</x:protectscenarios>'+
        '</x:worksheetoptions>'+
        '</x:excelworksheet>'+
		'</x:excelworksheets>'+
        '<x:protectstructure>False</x:protectstructure>'+
        '<x:protectwindows>False</x:protectwindows>'+
		'</x:excelworkbook>'+
        
        // '<style>'+
			
			//-------------------------------------
        '</x:excelworkbook>'+
        '</xml><![endif]-->'+
         '<style>'+
		'p.MsoFooter, li.MsoFooter, div.MsoFooter'+
		'{ margin:0in; margin-bottom:.0001pt; mso-pagination:widow-orphan; tab-stops:center 3.0in right 6.0in; font-size:12.0pt;}'+
		'<style>'+

		'<!-- /* Style Definitions */'+

		//'@page Section1'+
		//'{ size:8.5in 11.0in; margin:1.9cm 1.27cm 1.9cm 1.27cm; mso-header-margin:.5in; mso-footer-margin:.5in; mso-title-page:yes; mso-header: h1; mso-footer: f1; mso-first-header: fh1; mso-first-footer: ff1; mso-paper-source:0;}'+

		'div.Section1'+
		'{ page:Section1;}'+

		'table#hrdftrtbl'+
		'{ margin:0in 0in 0in 900in; width:1px; height:1px; overflow:hidden; font-family:Arial;}-->'+

		'</style>'+
		
		
		'</head>'+
		

		'<body>'+
		
		'<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">'

		var strVar2 = '';    
		strVar2 += "<table width=\"100%\">";
		strVar2 += "<tr>";
		strVar2 += "<td Width=\"5%\" style='text-align:left;font-weight:bold;' font-size=\"10\" \>Employee</td>";
		
		var temp_counter = 1;
		for ( var week in table) {
			
			if(i_view_type == 2)
			{
				if(temp_counter <= i_days_diff)
				{
					temp_counter++;
					strVar2 += "<td Width=\"5%\" style='text-align:left;font-weight:bold;' font-size=\"10\" \>"
				        + table[week].Name + "</td>";
				}
			}
			else
			{
				if(temp_counter <= i_total_weeks)
				{
					strVar2 += "<td Width=\"5%\" style='text-align:left;font-weight:bold;' font-size=\"10\" \>"
				        + table[week].Name + "</td>";
						
					temp_counter++;
				}
			}
		}
		strVar2 += "<\/tr>";
		
		var i_previous_emp = 0;
		
		for (var week in table)
		{
			var i_emp_id = week.toString().split('_');
			i_emp_id = i_emp_id[0];
			
			if (i_previous_emp == 0)
			{
				i_previous_emp = i_emp_id;
				strVar2 += "<tr>";
				strVar2 += "<td Width=\"5%\" style='text-align:left;font-weight:bold;' font-size=\"10\" \>"+table[week].EmpName+"</td>";
			}
			
			if (i_previous_emp != i_emp_id)
			{
				strVar2 += "<\/tr>";
				strVar2 += "<tr>";
				strVar2 += "<td Width=\"5%\" style='text-align:left;font-weight:bold;' font-size=\"10\" \>"+table[week].EmpName+"</td>";
				i_previous_emp = i_emp_id;
			}
			
			if(table[week].Week == 'No')
			{
				strVar2 += "<td Width=\"5%\" style='text-align:left;font-weight:bold;' font-size=\"10\" \>N-0</td>";
			}
			else
			{
				strVar2 += "<td Width=\"5%\" style='text-align:left;font-weight:bold;' font-size=\"10\" \>" + table[week].Week + "</td>";
			}
		}
		strVar2 += "<\/tr>";
		 
		strVar2 += "<\/table><br/>";
		
		strVar1 = strVar1 + strVar2;
		return strVar1;
	} catch (err) {
		nlapiLogExecution('ERROR', 'createMonthTimesheetTable', err);
		throw err;
	}
}

Date.prototype.getWeekName = function(view) {
	
	var d = new Date(+this);
	var days = d.getDay();
	
	var startDate = nlapiAddDays(d, -days);
	var endDate = nlapiAddDays(startDate, 6);
	
	if(view == 2)
	{
		return nlapiDateToString(d, 'date');
	}
	else
	{
		return nlapiDateToString(startDate, 'date') + " - "
	        + nlapiDateToString(endDate, 'date');
	}
};

Date.prototype.getWeekNumber = function() {
	var d = new Date(+this);
	d.setHours(0, 0, 0);
	d.setDate(d.getDate() + 4 - (d.getDay() || 7));
	return Math.ceil((((d - new Date(d.getFullYear(), 0, 1)) / 8.64e7) + 1) / 7);
};

function getDatediffIndays(startDate, endDate) {
	var one_day = 1000 * 60 * 60 * 24;
	var fromDate = startDate;
	var toDate = endDate;
	var date1 = nlapiStringToDate(fromDate);
	var date2 = nlapiStringToDate(toDate);
	var date3 = Math.round((date2 - date1) / one_day);
	return (date3 + 1);
}

function _logValidation(value) 
{
 if(value != null && value.toString() != null && value != '' && value != undefined && value.toString() != undefined && value != 'undefined' && value.toString() != 'undefined'&& value.toString() != 'NaN' && value != NaN) 
 {
  return true;
 }
 else 
 { 
  return false;
 }
}

// search code 
function searchRecord(recordType, savedSearch, arrFilters, arrColumns,  filterExpression){

	try {
		var search = null;

		// if a saved search is provided, load it and add the filters and
		// columns
		if (isNotEmpty(savedSearch)) {
			search = nlapiLoadSearch(recordType, savedSearch);

			if (isArrayNotEmpty(arrFilters)) {
				search.addFilters(arrFilters);
			}

			if (isArrayNotEmpty(arrColumns)) {
				search.addColumns(arrColumns);
			}

			if (isArrayNotEmpty(filterExpression)) {
				search.setFilterExpression(filterExpression);
			}
		}
		// create a new search
		else {
			search = nlapiCreateSearch(recordType, arrFilters, arrColumns);
		}

		// run search
		var resultSet = search.runSearch();
		
		// iterate through the search and get all data 1000 at a time
		var searchResultCount = 0;
		var resultSlice = null;
		var searchResult = [];

		do {
			resultSlice = resultSet.getResults(searchResultCount,
			        searchResultCount + 1000);

			if (resultSlice) {

				resultSlice.forEach(function(result) {

					searchResult.push(result);
					searchResultCount++;
				});
			}
		} while (isArrayNotEmpty(resultSlice) && resultSlice.length >= 1000);

		return searchResult;
	} catch (err) {
		nlapiLogExecution('ERROR', 'searchRecord', err);
		throw err;
	}
}

function isEmpty(value) {

	return value == null || value == "" || typeof (value) == undefined;
}

function isNotEmpty(value) {

	return !isEmpty(value);
}

function isArrayEmpty(argArray) {

	return !isArrayNotEmpty(argArray);
}

function isArrayNotEmpty(argArray) {

	return (isNotEmpty(argArray) && argArray.length > 0);
}
