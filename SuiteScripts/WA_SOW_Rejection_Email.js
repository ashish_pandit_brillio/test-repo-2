/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       29 Mar 2018     deepak.srinivas
 *
 */

/**
 * @returns {Void} Any or no return value
 */
function workflowAction() {
try{
	nlapiLogExecution('AUDIT', 'SOW ID: ', nlapiGetRecordId());
	var dataRow = [];
	var JSON_ = {};
	
	//Conversion rate search
	var inr_to_usd = 0;
	var gbp_to_usd = 0;
	var eur_to_usd = 0;
	var a_conversion_rate_table = nlapiSearchRecord('customrecord_conversion_rate_sfdc',null,null,[new nlobjSearchColumn('custrecord_rate_inr_to_usd'),
	                                                              new nlobjSearchColumn('custrecord_rate_gbp_to_usd'),
	                                                              new nlobjSearchColumn('custrecord_rate_eur_to_usd'),
	                                                              new nlobjSearchColumn('internalid').setSort(true)]);
	if(a_conversion_rate_table){
		inr_to_usd = 1 / parseFloat(a_conversion_rate_table[0].getValue('custrecord_rate_inr_to_usd'));
		gbp_to_usd = a_conversion_rate_table[0].getValue('custrecord_rate_gbp_to_usd');
		eur_to_usd = a_conversion_rate_table[0].getValue('custrecord_rate_eur_to_usd');
}
	
	var rec = nlapiLoadRecord(nlapiGetRecordType(),nlapiGetRecordId());
	var s_status = rec.getFieldText('custbody_invoicestatus');
	var s_currency = rec.getFieldText('currency');
	var i_Sow_Value = rec.getFieldValue('total');
	//if(s_status == 'Rejected'){
		JSON_  = {
			'SOW_ID': rec.getFieldValue('tranid'),
			'Opp_ID': rec.getFieldValue('custbody_opp_id_sfdc'),
			'Status': 'Rejected'
		};
	dataRow.push(JSON_);
	
	//Conversion factor
	var f_currency_conversion = 1;
	if(s_currency =='USD')
		f_currency_conversion = 1;
	if(s_currency =='INR')
		f_currency_conversion = inr_to_usd;
	if(s_currency =='EUR')
		f_currency_conversion = eur_to_usd;
	if(s_currency =='GBP')
		f_currency_conversion = gbp_to_usd;
	
	var i_Sow_Value_ = (parseFloat(f_currency_conversion) * parseFloat(i_Sow_Value)).toFixed(1);
	
		var to = [];
		var cp_email ='';
		var regionHead_email = '';
		var recid = nlapiGetRecordId();
		//nlapiRequestURL(nlapiResolveURL('/app/accounting/transactions/salesordermanager.nl?type=cancel&id=' + nlapiGetRecordId()));
		rec.setFieldText('status','Cancelled');
		rec.setFieldValue('statusRef','cancelled');
		rec.setFieldText('custbody_invoicestatus','Rejected');
		for(var i = 1; i <= rec.getLineItemCount('item'); i++){
		    rec.setLineItemValue('item', 'isclosed', i, 'T');
		}
		nlapiSubmitRecord(rec);
		nlapiLogExecution('DEBUG','Rejecetd',recid);
		var cus_id = rec.getFieldValue('entity');
		if(_logValidation(cus_id)){
		var customer_lookup = nlapiLookupField('customer',parseInt(cus_id),['custentity_clientpartner','custentity_region']);
		var region_lookup = nlapiLookupField('customrecord_region',parseInt(customer_lookup.custentity_region),'custrecord_region_head');
		if(_logValidation(customer_lookup.custentity_clientpartner))
		var cp_email = nlapiLookupField('employee',parseInt(customer_lookup.custentity_clientpartner),'email');
		if(_logValidation(region_lookup))
		var regionHead_email = nlapiLookupField('employee',parseInt(region_lookup),'email');
		//TO List
		to.push(cp_email);
		to.push(regionHead_email);
		
		var strVar = '';
		strVar += '<html>';
		strVar += '<body>';
		
		strVar += '<p>Dear Stakeholders,</p>';
		//strVar += '<br/>';
		strVar += '<p>Opportunity '+rec.getFieldValue('custbody_opp_id_sfdc')+',dt '+rec.getFieldValue('trandate')+', '+rec.getFieldValue('tranid')+' with amount $'+i_Sow_Value_+' has been rejected by Business Operations.';
		strVar += 'This opportunity has been reverted to "Selected - Negotiate & Close" stage with confidence level at "10%" hence reckoned as Stretch for now.';
		strVar += 'Please update the opportunity to make it part of Most Likely by changing the confidence level as desired.</p>';

		strVar += '<p>Note: System will generate new SoW as and when you mark this opportunity as "Closed Won" again in the future.</p>';
		//strVar += '<br/>';
		//strVar += '<a href="https://system.sandbox.netsuite.com/app/site/hosting/scriptlet.nl?script=1044&deploy=1&proj_id='+nlapiGetRecordId()+'>Project Setup</a>';
		
		strVar += '<p>Thanks & Regards,</p>';
		strVar += '<p>Team IS</p>';
			
		strVar += '</body>';
		strVar += '</html>';
		
		//Send Email
		var a_emp_attachment = new Array();
		a_emp_attachment['entity'] = 41571;
		
		nlapiSendEmail(442,to,'SoW rejection from NS:Ref ' +rec.getFieldValue('tranid')+', '+rec.getFieldValue('custbody_opp_id_sfdc')+', amount $'+i_Sow_Value_+', Customer '+rec.getFieldText('entity')+'', strVar,null,['deepak.srinivas@brillio.com','sumukh.am@brillio.com'],a_emp_attachment);
		nlapiLogExecution('audit','mail sent');	
		var accessURL = getAccessToken();
        var method = 'POST';
		
        var response = nlapiRequestURL(accessURL,null,null,method);
        var temp = response.body;
        nlapiLogExecution('DEBUG','JSON',temp);
        var data = JSON.parse(response.body);
        var access_token_SFDC = data.access_token;
        var instance_URL_Sfdc = data.instance_url;
        nlapiLogExecution('DEBUG','JSON',data);
	        
	        var dynamic_URL = instance_URL_Sfdc+'/services/apexrest/SowReject/v1.0/';
	        var auth = 'Bearer'+' '+access_token_SFDC;
	        
	        //Test
	      //Setting up Headers 
	        /*var headers = {"User-Agent-x": "SuiteScript-Call",
	                       "Authorization": "Bearer "+access_token_SFDC,
	                       "Content-Type": "application/json"};*/
	        
	        var headers = {"Authorization": "Bearer "+access_token_SFDC,
	        		 "Content-Type": "application/json",
	        		 "accept": "application/json"};
	        //
	        var method_ = 'POST';
	        var response_ = nlapiRequestURL(dynamic_URL,JSON.stringify(dataRow),headers,method_);
	        var data = JSON.parse(response_.body);
	        var message = data[0].message;
	        var details_ = data.details;
	        nlapiLogExecution('DEBUG','JSON',message); 
	        nlapiLogExecution('DEBUG','details_',details_); 
	       
		
	}
	
	nlapiLogExecution('DEBUG','Response',JSON.stringify(dataRow));
	
}
catch(e){
	
	nlapiLogExecution('DEBUG','Rejection Trigger Error',e);
}
}

function _logValidation(value) 
{
 if(value != null && value.toString() != null && value != '' && value != undefined && value.toString() != undefined && value != 'undefined' && value.toString() != 'undefined'&& value.toString() != 'NaN' && value != NaN) 
 {
  return true;
 }
 else 
 { 
  return false;
 }
}
