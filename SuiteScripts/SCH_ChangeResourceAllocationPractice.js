/**
 * @author Jayesh
 */

function schdld_updateAllocation()
{
	try
	{
		var i_context = nlapiGetContext();
		
		var column_allocation = new Array();	
		column_allocation[0] = new nlobjSearchColumn('department', 'employee');
		column_allocation[1] = new nlobjSearchColumn('startdate');
		column_allocation[2] = new nlobjSearchColumn('enddate');
			
		var allocation_srch = searchRecord('resourceallocation', 'customsearch1587', null, column_allocation);
		if(allocation_srch)
		{
			nlapiLogExecution('audit','Length:- ',allocation_srch.length);
			for(var index=0; index<allocation_srch.length ; index++)
			{
				var i_usage_end = i_context.getRemainingUsage();
				if (i_usage_end < 100) //
				{
					nlapiYieldScript();
				}
				
				nlapiSubmitField('resourceallocation',allocation_srch[index].getId(),'custevent_practice',allocation_srch[index].getValue('department', 'employee'));
			}			
		}
	}
	catch(err)
	{
		nlapiLogExecution('ERROR','ERROR MESSAGE :- ',err);
	}
}

function searchRecord(recordType, savedSearch, arrFilters, arrColumns,
        filterExpression)
{

	try {
		var search = null;

		// if a saved search is provided, load it and add the filters and
		// columns
		if (isNotEmpty(savedSearch)) {
			search = nlapiLoadSearch(recordType, savedSearch);

			if (isArrayNotEmpty(arrFilters)) {
				search.addFilters(arrFilters);
			}

			if (isArrayNotEmpty(arrColumns)) {
				search.addColumns(arrColumns);
			}

			if (isArrayNotEmpty(filterExpression)) {
				search.setFilterExpression(filterExpression);
			}
		}
		// create a new search
		else {
			search = nlapiCreateSearch(recordType, arrFilters, arrColumns);
		}

		// run search
		var resultSet = search.runSearch();

		// iterate through the search and get all data 1000 at a time
		var searchResultCount = 0;
		var resultSlice = null;
		var searchResult = [];

		do {
			resultSlice = resultSet.getResults(searchResultCount,
			        searchResultCount + 1000);

			if (resultSlice) {

				resultSlice.forEach(function(result) {

					searchResult.push(result);
					searchResultCount++;
				});
			}
		} while (isArrayNotEmpty(resultSlice) && resultSlice.length >= 1000);

		return searchResult;
	} catch (err) {
		nlapiLogExecution('ERROR', 'searchRecord', err);
		throw err;
	}
}

function isEmpty(value) {

	return value == null || value == "" || typeof (value) == undefined;
}

function isNotEmpty(value) {

	return !isEmpty(value);
}

function isArrayEmpty(argArray) {

	return !isArrayNotEmpty(argArray);
}

function isArrayNotEmpty(argArray) {

	return (isNotEmpty(argArray) && argArray.length > 0);
}
