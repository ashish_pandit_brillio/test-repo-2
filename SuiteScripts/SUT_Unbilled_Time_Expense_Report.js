///////////////check feasibility/////////////////////
//1. role wise subsidairy come means project.
//2. customer filter put on main screen for multiselection and ,customer drop down ,only role wise customer show,
//------------------------------------------------------------------------------------------------------------
/**
 * @author Shweta
 */
// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
/*
   	Script Name : SUT_Unbilled_Time_Expense_Report.js
	Author      : Shweta Chopde
	Date        : 12 May 2014
	Description : Create a Unbilled Expense / Time form


	Script Modification Log:

	-- Date --			-- Modified By --				--Requested By--				-- Description --
    13-10-2014          swati kurariya                   nitin                            1.add from date and to date field
	                                                                                      2.pass the filter form and to date.
    31-10-2014          swati kurariya                   nitin                            1.role wise customer show in suitelet screen                     
	                                                                                      2.role wise subsidairy show 
    04-02-2015			swati kurariya                   vaibhav						  1.create Month and Year list field.													  
                                                                                         																						  

Below is a summary of the process controls enforced by this script file.  The control logic is described
more fully, below, in the appropriate function headers and code blocks.


     SUITELET
		- suiteletFunction(request, response)


     SUB-FUNCTIONS
		- The following sub-functions are called by the above core functions in order to maintain code
            modularization:

               - NOT USED

*/
}
// END SCRIPT DESCRIPTION BLOCK  ====================================



// BEGIN GLOBAL VARIABLE BLOCK  =====================================
{
	//  Initialize any Global Variables, in particular, debugging variables...




}
// END GLOBAL VARIABLE BLOCK  =======================================





// BEGIN SUITELET ==================================================

function suiteletFunction(request, response)
{
	try
	{
		if (request.getMethod() == 'GET') 
		{	
	    	var i_subsidiary_user ;
		    var i_current_user =  nlapiGetUser();		
			var i_current_role =  nlapiGetRole();
			var s_script_status='';
			var i_percent_complete='';
			
			//nlapiLogExecution('DEBUG', 'suiteletFunction',' Current User -->' + i_current_user);	
			nlapiLogExecution('DEBUG', 'suiteletFunction',' Current Role -->' + i_current_role);
			if(i_current_role == '3')
			{
			i_subsidiary_user=2;
			}
			else if(i_current_role == '1024' || i_current_role =='1025' )//bllc
			{
			i_subsidiary_user=2;
			}
			else if(i_current_role == '1015')//btpl
			{
			i_subsidiary_user=3
			}
			else if(i_current_role == '1004')//employee center BTPL delivery
			{
			i_subsidiary_user=3
			}
			
			
		/*	if(_logValidation(i_current_user))
			{
			    var emp_filter=new Array();
				    emp_filter[0]= new nlobjSearchFilter('role',null,'is',i_current_role);
					 emp_filter[0]= new nlobjSearchFilter('internalid',null,'is',i_current_user);
			       var employee_search=nlapiSearchRecord('employee',null,emp_filter,null);
				   if(employee_search != null)
				   {
						nlapiLogExecution('DEBUG','employee_search',employee_search.length);
						var emp_id=employee_search[0].getId();
						var o_userOBJ = nlapiLoadRecord('employee',emp_id)
				   }
				
				
				
				if(_logValidation(o_userOBJ))
				{
					i_subsidiary_user = o_userOBJ.getFieldValue('subsidiary')
					nlapiLogExecution('DEBUG', 'suiteletFunction',' User Subsidiary -->' + i_subsidiary_user);
						
				}//User OBJ				
			}//Current User*/
				
						
			var f_form = nlapiCreateForm(" UNBILLED EXPENSE / TIME REPORT");
			f_form.setScript('customscriptcli_unbilled_time_expense_va')
			//------------------------------------testing purpose add------------------------------------------------------------------------------------------
			/*var fld = f_form.addField('custpage_rows', 'select', 'View Row Numbers');
			fld.addSelectOption('3', '1 to 3');
			fld.addSelectOption('6', '4 to 6');
			fld.addSelectOption('9', '7 to 9');
			fld.setDefaultValue(fld);*/
			//-------------------------------------------------------------
			
			
			//------------------------------------------------------------
			
			 
			var i_current_date = f_form.addField('custpage_current_date', 'date', 'As on Date').setDisplayType('hidden');
			//----------------------------------------------------------------------------------------------------------------------------
			var i_Month = f_form.addField('custpage_month', 'select', 'Month','customlist_month');	
			var i_Year = f_form.addField('custpage_year', 'select', 'Year','customlist_year');
			var i_from_date = f_form.addField('custpage_from_date', 'date','From Date')//.setDisplayType('inline');
			var i_to_date = f_form.addField('custpage_to_date', 'date','To Date')//.setDisplayType('inline');
			//----------------------------------------------------------------------------------------------------------------------------
			var i_unbilled_type= f_form.addField('custpage_unbilled_type','select', ' Type','customlist_unbilled_type_list');
			var i_unbilled_receivable_GL  = f_form.addField('custpage_unbilled_receivable','select', ' Unbilled Receivable GL ( Debit )','account');
			var i_unbilled_revenue_GL  = f_form.addField('custpage_unbilled_revenue','select', ' Unbilled Revenue GL ( Credit )','account');
			var i_reverse_date = f_form.addField('custpage_reverse_date', 'date', 'Reverse Date');	
			var i_criteria_f = f_form.addField('custpage_criteria', 'select', 'Criteria','customlist_criteria_list_provision');	
				
			var i_subsidiary_f = f_form.addField('custpage_subsidiary', 'select', 'Subsidiary','subsidiary').setDisplayType('inline');	
			i_subsidiary_f.setDefaultValue(i_subsidiary_user)
			
			var i_search_count_f = f_form.addField('custpage_search_count', 'textarea', 'No. Of Records in Time / Expense Search').setDisplayType('inline');
			
			var i_user_role_f= f_form.addField('custpage_user_role', 'text', 'Role').setDisplayType('hidden');				
			i_user_role_f.setDefaultValue(i_current_role)
			
			var i_user_subsidiary_f = f_form.addField('custpage_user_subsidiary', 'textarea', 'Subsidiary').setDisplayType('hidden');				
			i_user_subsidiary_f.setDefaultValue(i_subsidiary_user)
			
			
									
             i_Month.setMandatory(true);
			 i_Year.setMandatory(true);
			
			
			 i_current_date.setDefaultValue(get_todays_date());
			 i_from_date.setMandatory(true)
			 i_to_date.setMandatory(true)
			 
			  var i_refresh_status =  request.getParameter('custscript_status');
			 nlapiLogExecution('DEBUG', 'i_refresh_status',i_refresh_status);
			//------------------------refresh page code---------------------------------------------
			if(i_refresh_status == 'T') 
				{
					var i_subsidiary_new =  request.getParameter('custpage_user_subsidiary');
					//var i_role =   request.getParameter('custpage_user_role');
					var i_current_date_new =  request.getParameter('custpage_current_date');
					
					var i_from_date_new =  request.getParameter('custpage_from_date');
					var i_to_date_new =  request.getParameter('custpage_to_date');
					
					var i_unbilled_type_new =   request.getParameter('custpage_unbilled_type');
					var i_unbilled_receivable_GL_new =  request.getParameter('custpage_unbilled_receivable');
					var i_unbilled_revenue_GL_new =  request.getParameter('custpage_unbilled_revenue');
					var i_reverse_date_new =  request.getParameter('custpage_reverse_date');
					var i_criteria_new =  request.getParameter('custpage_criteria');
					var i_customer_new =  request.getParameter('custpage_customer');
					var i_search_count_f_new =  request.getParameter('custpage_search_count');
					
					var i_month_new =  request.getParameter('custpage_month');
					var i_year_new =  request.getParameter('custpage_year');
					
					
					 i_current_date.setDefaultValue(get_todays_date());
					 i_subsidiary_f.setDefaultValue(i_subsidiary_new);
					 i_from_date.setDefaultValue(i_from_date_new);
					 i_to_date.setDefaultValue(i_to_date_new);
					 i_unbilled_type.setDefaultValue(i_unbilled_type_new);
					 i_unbilled_receivable_GL.setDefaultValue(i_unbilled_receivable_GL_new);
					 i_unbilled_revenue_GL.setDefaultValue(i_unbilled_revenue_GL_new);
					 i_Month.setDefaultValue(i_month_new);
					 i_Year.setDefaultValue(i_year_new);//
					 i_search_count_f.setDefaultValue(i_search_count_f_new);
					 i_criteria_f.setDefaultValue(i_criteria_new);
					 i_reverse_date.setDefaultValue(i_reverse_date_new);
					 
					 //-----------------------------Delete Schedule script show-------------------------------------------------------
					 if(_logValidation(get_todays_date()))
						{//if start									
							var column = new Array();
							var filters = new Array();
							filters.push(new nlobjSearchFilter('status', null, 'anyof', ['PROCESSING', 'PENDING','COMPLETE']));
									
							filters.push(new nlobjSearchFilter('internalid','script','is','459'));//250 sand box id
							
							column.push(new nlobjSearchColumn('name', 'script'));
							column.push(new nlobjSearchColumn('internalid', 'scriptdeployment'));
							column.push(new nlobjSearchColumn('datecreated'));
							column.push(new nlobjSearchColumn('status'));
							column.push(new nlobjSearchColumn('startdate'));
							column.push(new nlobjSearchColumn('enddate'));
							column.push(new nlobjSearchColumn('queue'));
							column.push(new nlobjSearchColumn('percentcomplete'));
							column.push(new nlobjSearchColumn('queueposition'));
							column.push(new nlobjSearchColumn('percentcomplete'));
					
							var a_search_results =  nlapiSearchRecord('scheduledscriptinstance', null, filters, column);
							//nlapiLogExecution('DEBUG','suiteletFunction','Search Results Length -->'+a_search_results.length);
							if(_logValidation(a_search_results)) 
						    {
						    	nlapiLogExecution('DEBUG','suiteletFunction','Search Results Length -->'+a_search_results.length);
						    	
								for(var i=0;i<a_search_results.length;i++)
						    	{
						    		var s_script  = a_search_results[i].getValue('name', 'script');
									//nlapiLogExecution('DEBUG','suiteletFunction','Script -->'+s_script);
					
									var d_date_created  = a_search_results[i].getValue('datecreated');
									//nlapiLogExecution('DEBUG','suiteletFunction','Date Created -->'+d_date_created);
					    
						           	 i_percent_complete  = a_search_results[i].getValue('percentcomplete');
									//nlapiLogExecution('DEBUG','suiteletFunction','Percent Complete -->'+i_percent_complete);
					
									s_script_status  = a_search_results[i].getValue('status');
									nlapiLogExecution('DEBUG','suiteletFunction','Script Status -->'+s_script_status);
								
								    i_percent_complete = i_percent_complete;              
								
						    	}
						    }
						
						}//if close
					//-------------------------------------------------------------------------------------
					 
				}
			
			 if(i_percent_complete =='' || i_percent_complete == null || i_percent_complete == undefined)
			 {
			 	s_script_status = 'Not Started';
				i_percent_complete = '0%';
			 }
			    f_form.setScript('customscriptcli_time_bill_suitelet_ref_p');
				var i_percent_complete_f = f_form.addField('custpage_percent_complete', 'text', 'Percent Complete').setDisplayType('inline');
				i_percent_complete_f.setDefaultValue(i_percent_complete);
				
				var i_status_f = f_form.addField('custpage_status', 'text', 'Status').setDisplayType('inline');
				i_status_f.setDefaultValue(s_script_status);
				
				
				
				
				
			i_unbilled_type.setMandatory(true)
			i_unbilled_receivable_GL.setMandatory(true)
			i_unbilled_revenue_GL.setMandatory(true)
			i_reverse_date.setMandatory(true)
			
			if(s_script_status == 'Complete')
		    {//if start
		    	 i_percent_complete_f.setDefaultValue('100%');
		    	 i_status_f.setDefaultValue(s_script_status);
		    	
		    	 nlapiLogExecution('DEBUG', 'inside if status complete');
		        
		         
			}//if close
			 f_form.addSubmitButton('LOAD DATA');
           
			f_form.addButton('custpage_delete','Delete','delete_time_cus_rec()');
			f_form.addButton('custpage_refresh','REFRESH','refresh()');
			
		    response.writePage(f_form);
		}//GET
		else if (request.getMethod() == 'POST') 	
		{
			var i_subsidiary =  request.getParameter('custpage_user_subsidiary');
			var i_role =   request.getParameter('custpage_user_role');
			var i_current_date =  request.getParameter('custpage_current_date');
			//--------------------------------------------------------------------------
			var i_from_date =  request.getParameter('custpage_from_date');
			var i_to_date =  request.getParameter('custpage_to_date');
			//--------------------------------------------------------------------------
			var i_unbilled_type =   request.getParameter('custpage_unbilled_type');
			var i_unbilled_receivable_GL =  request.getParameter('custpage_unbilled_receivable');
			var i_unbilled_revenue_GL =  request.getParameter('custpage_unbilled_revenue');
			var i_reverse_date =  request.getParameter('custpage_reverse_date');
			var i_criteria =  request.getParameter('custpage_criteria');
			var i_customer =  request.getParameter('custpage_customer');
			var i_search_count_f =  request.getParameter('custpage_search_count');
			
			var i_month =  request.getParameter('custpage_month');
			var i_year =  request.getParameter('custpage_year');
			
			//nlapiLogExecution('DEBUG', 'suiteletFunction',' User Role -->' + i_role);			
			//nlapiLogExecution('DEBUG', 'suiteletFunction',' User Subsidiary -->' + i_subsidiary);	
			nlapiLogExecution('DEBUG', 'suiteletFunction',' Current Date -->' + i_current_date);
			nlapiLogExecution('DEBUG', 'suiteletFunction',' From Date -->' + i_from_date);	
			nlapiLogExecution('DEBUG', 'suiteletFunction',' To Date -->' + i_to_date);				
			//nlapiLogExecution('DEBUG', 'suiteletFunction',' Unbilled Type -->' + i_unbilled_type);
			//nlapiLogExecution('DEBUG', 'suiteletFunction',' Unbilled Receivable GL -->' + i_unbilled_receivable_GL);
			//nlapiLogExecution('DEBUG', 'suiteletFunction',' Unbilled Revenue GL -->' + i_unbilled_revenue_GL);
			//nlapiLogExecution('DEBUG', 'suiteletFunction',' Reverse Date -->' + i_reverse_date);	
		     nlapiLogExecution('DEBUG', 'suiteletFunction',' Criteria -->' + i_criteria);	
		     nlapiLogExecution('DEBUG', 'suiteletFunction',' i_customer -->' + i_customer);	
			 
			 nlapiLogExecution('DEBUG', 'suiteletFunction',' Month -->' + i_month);	
		     nlapiLogExecution('DEBUG', 'suiteletFunction',' Year -->' + i_year);	
		
		    var params=new Array();
		    params['custscript_current_date'] = i_current_date
			
			params['custscript_from_date'] = i_from_date
			params['custscript_to_date'] = i_to_date
			
			params['custscript_unbilled_type'] = i_unbilled_type
			params['custscript_unbilled_receivable_gl'] = i_unbilled_receivable_GL
			params['custscript_unbilled_revenue_gl'] = i_unbilled_revenue_GL
			params['custscript_reverse_date'] = i_reverse_date
			params['custscript_provision_creation_criteria'] = i_criteria 	
			params['custscript_user_role'] = i_role
			params['custscript_user_subsidiary'] = i_subsidiary 
            params['custscript_customer'] = i_customer 	
            params['custscript_count'] = i_search_count_f 
			params['custscript_month'] = i_month 	
			params['custscript_year'] = i_year 	 
			 
		//	response.write('https://system.sandbox.netsuite.com/app/common/search/searchresults.nl?searchid=138') 
		    if(i_unbilled_type == '2')
			{
			
			
					var params_1=new Array();
				// params['custscript_current_date_sch'] = i_current_date
				//----------------------------------------------------------------
				  params_1['custscript_sch_from_date'] = i_from_date
				  params_1['custscript_sch_to_date'] = i_to_date
				 //----------------------------------------------------------------
				
				 params_1['custscript_sch_subsidiary'] = i_subsidiary
				 params_1['custscript_criteria'] = i_criteria;
				 params_1['custscript_month_id'] = i_month;
				 params_1['custscript_year_id'] = i_year;
					
			var status=nlapiScheduleScript('customscript_sch_timebill_rec_create',null,params_1);	 
			nlapiSetRedirectURL('suitelet', 'customscript_sui_create_timebill_rec', 'customdeploy1', null,params);
			//nlapiLogExecution('DEBUG', 'suiteletFunction', ' Suitelet Called for criteria 3......... ' );
			}
			else
			{
			nlapiSetRedirectURL('suitelet', 'customscript_provision_unbilled_journal', 'customdeploy1', null,params);
			nlapiLogExecution('DEBUG', 'suiteletFunction', ' Suitelet Called ......... ' );
			}			
			
		}//POST		
		
	}//TRY
	catch(exception)
	{
		nlapiLogExecution('DEBUG', 'ERROR',' Exception Caught -->' + exception);	
					
	}//CATCH
}

// END SUITELET ====================================================


// BEGIN OBJECT CALLED/INVOKING FUNCTION ===================================================
function get_todays_date()
{
	var today;
	// ============================= Todays Date ==========================================
		
	  var date1 = new Date();
      //nlapiLogExecution('DEBUG', 'schedulerFunction', 'Date ==' + date1);

      var offsetIST = 5.5;

    //To convert to UTC datetime by subtracting the current Timezone offset
     var utcdate =  new Date(date1.getTime() + (date1.getTimezoneOffset()*60000));
     //nlapiLogExecution('DEBUG', 'schedulerFunction', 'UTC Date' + utcdate);

    //Then cinver the UTS date to the required time zone offset like back to 5.5 for IST
     var istdate =  new Date(utcdate.getTime() - ((-offsetIST*60)*60000));
    // nlapiLogExecution('DEBUG', 'schedulerFunction', 'IST Date ==' + istdate);
    
     var day = istdate.getDate();
    // nlapiLogExecution('DEBUG', 'schedulerFunction', 'Day ==' + day);
	 
     var month = istdate.getMonth()+1;
     //nlapiLogExecution('DEBUG', 'schedulerFunction', 'Month ==' + month);

	 var year=istdate.getFullYear();
	// nlapiLogExecution('DEBUG', 'schedulerFunction', 'Year ==' +year);
	  
	 var date_format = checkDateFormat();
	// nlapiLogExecution('DEBUG', 'schedulerFunction', 'Date Format ==' +date_format);
	 
	 if (date_format == 'YYYY-MM-DD')
	 {
	 	today = year + '-' + month + '-' + day;		
	 } 
	 if (date_format == 'DD/MM/YYYY') 
	 {
	  	today = day + '/' + month + '/' + year;
	 }
	 if (date_format == 'MM/DD/YYYY')
	 {
	  	today = month + '/' + day + '/' + year;
	 }	 
	 return today;
}
function checkDateFormat()
{
    var context = nlapiGetContext();

	var dateFormatPref = context.getPreference('dateformat');

    return dateFormatPref;
}

function _logValidation(value) 
{
 if(value != null && value.toString() != null && value != '' && value != undefined && value.toString() != undefined && value != 'undefined' && value.toString() != 'undefined'&& value.toString() != 'NaN' && value != NaN) 
 {
  return true;
 }
 else 
 { 
  return false;
 }
}

// END OBJECT CALLED/INVOKING FUNCTION =====================================================
