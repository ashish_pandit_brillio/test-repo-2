function massUpdate(recType, recordId) {
	try {
		var rec = nlapiLoadRecord(recType, recordId);

		// var prId = rec.getFieldValue('custrecord_prquote');
		// var srNo = rec.getFieldValue('custrecord_srno');
		var item = rec.getFieldValue('custrecord_qa_pr_item');

		if (item) {
			updateQAStatus(item);
		} else {
			nlapiLogExecution('error', 'massUpdate', 'No PR Item');
		}

		// if (prId) {
		// var search = nlapiSearchRecord('customrecord_pritem', null, [
		// new nlobjSearchFilter('custrecord_purchaserequest', null,
		// 'anyof', prId),
		// new nlobjSearchFilter('custrecord_prlineitemno', null,
		// 'is', srNo) ]);

		// if (search) {
		// var prItemId = search[0].getId();
		//
		// nlapiSubmitField('customrecord_quotationanalysis', recordId,
		// 'custrecord_qa_pr_item', prItemId);

		// updateQAStatus(prItemId);
		// }
		// } else {
		// nlapiLogExecution('error', 'massUpdate', 'No PR Set');
		// }
	} catch (err) {
		nlapiLogExecution('error', 'massUpdate', err);
	}
}

function updateQAStatus(prId) {
	try {
		var qaSearch = nlapiSearchRecord('customrecord_quotationanalysis',
		        null, [
		                new nlobjSearchFilter('custrecord_qa_pr_item', null,
		                        'anyof', prId),
		                new nlobjSearchFilter('isinactive', null, 'is', 'F') ]);

		var prItemStatus = nlapiLookupField('customrecord_pritem', prId,
		        'custrecord_prastatus');

		if (qaSearch) {
			var qaStatus = "";
			nlapiLogExecution('debug', 'prItemStatus', prItemStatus);

			if (prItemStatus) {
				prItemStatus = parseInt(prItemStatus);
			}

			switch (prItemStatus) {

				// PR approved pending for Quotation analysis
				case 7:
					qaStatus = 5;
				break;

				// QA Pending for Practice Head Approval
				case 24:
					qaStatus = 6;
				break;

				// Pending PO Creation
				case 27:
					qaStatus = 7;
				break;

				// PO created
				case 23:
					qaStatus = 8;
				break;

				// Closed
				// Closed, Issued from Stock
				case 5:
				case 29:
					qaStatus = 10;
				break;

				case 6:
					qaStatus = 4;
				break;
			}

			if (qaStatus) {
				nlapiSubmitField('customrecord_quotationanalysis', qaSearch[0]
				        .getId(), 'custrecord_approvalstatusforquotation',
				        qaStatus);
				nlapiLogExecution('debug', 'quotation record updated', qaStatus);
			}
		} else {
			nlapiLogExecution('debug', 'no qa found');
		}
	} catch (err) {
		nlapiLogExecution('ERROR', 'updateQAStatus', err);
	}
}