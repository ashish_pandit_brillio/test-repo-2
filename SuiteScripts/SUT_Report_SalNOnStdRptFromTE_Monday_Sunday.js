//BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
    /*
     Script Name:SUT_Report_SalNOnStdRptFromTS.js
     Author:
     Company:Aashna Cloud tech
     Date:20-08-2014
     Description:
     Script Modification Log:
     -- Date --			-- Modified By --				--Requested By--				-- Description --
     27-08-2014           Rujuta K.                      Anuradha K.                       Backup Script
     27-08-2014           Rujuta K.                      Anuradha K.                       Changes
     1.Calculate and show DT column only for california work location.
     2. Project_city should fetched from work location from resource allocation table on project.
     3. When DT calulated , reduce DT hrs from OT and display in OT column.
     4. When week1 or week2  ST hrs are 40 and no OT hrs are there, exclude that time sheet entry from report.
     5. When week1 or week2 ST task along with holiday task hrs is 40 then too exclude that time sheet entry from report.
     Below is a summary of the process controls enforced by this script file.  The control logic is described
     more fully, below, in the appropriate function headers and code blocks.
     SUITELET
     - suiteletFunction(request, response )
     SUB-FUNCTIONS
     - The following sub-functions are called by the above core functions in order to maintain code
     modularization:
     - NOT USED
     */
}
//END SCRIPT DESCRIPTION BLOCK  ====================================

//END GLOBAL VARIABLE BLOCK  =======================================

//BEGIN SUITELET ==================================================

function GenerateReport(request, response) //
{
    try //
    {
        var a_employeearray = new Array();
        if (request.getMethod() == 'GET') //
        {
            var s_period = '';
            var form = nlapiCreateForm('Sal-Non-Std Rpt-From TS(Monday-Sunday)');
            var oldvalues = false;
            var flag = 0;
            form.setScript('customscriptcli_checkdayofweek_0')
            var contextObj = nlapiGetContext();
            //nlapiLogExecution('DEBUG', 'GET', ' contextObj===>' + contextObj)
            
            var beginUsage = contextObj.getRemainingUsage();
            //Added Date Field in suitelet
            //var select = form.addField('custpage_startdate', 'Date', 'Start Date :');
            var select = form.addField('custpage_startdate', 'Date', 'Period End Date :');
            //Add Week 1 hours and Week 2 hours to the form
            var wk1hours = form.addField('custpage_wk1_hours', 'float', 'Week 1 Hours').setMandatory(true);
            var wk2hours = form.addField('custpage_wk2_hours', 'float', 'Week 2 Hours').setMandatory(true);
            //Get the Value of start Date
            var s_beforedate = request.getParameter('custpage_startdate');
            //nlapiLogExecution('DEBUG', 'Search results ', ' s_beforedate =' + s_beforedate)
            //Get the Value of End Date
            var s_afterdate = request.getParameter('custpage_fromdate');
            //nlapiLogExecution('DEBUG', 'Search results ', ' s_afterdate =' + s_afterdate)
            var f_wk1_hours, f_wk2_hours;
            //Get the Value of Week 1 hours
            if(request.getParameter('custpage_wk1_hours') != null)
            	{
            		f_wk1_hours = parseFloat(request.getParameter('custpage_wk1_hours'));
            		form.setFieldValues({'custpage_wk1_hours': f_wk1_hours});
            	}
            
            //Get the Value of Week 2 hours
            if(request.getParameter('custpage_wk2_hours') != null)
            	{
            		f_wk2_hours = parseFloat(request.getParameter('custpage_wk2_hours'));
            		form.setFieldValues({'custpage_wk2_hours': f_wk2_hours});
            	}
            
            //Added Sublist in Report
            var sublist1 = form.addSubList('record', 'list', 'Sal-Non-Std Rpt-From TS');
            //this id is used while Exporting the report in CSV format it checks if id==Export that means we need to CSV conversion. 
            var id = request.getParameter('id');
            //nlapiLogExecution('DEBUG', 'Post results ', ' id=' + id)
            //if id==
            
            if (id == 'Export') {
                var param = request.getParameter('param');
                //nlapiLogExecution('DEBUG', 'Post results ', ' param=' + param)
                
                var datesplit = param.toString().split('@')
                //nlapiLogExecution('DEBUG', 'Post results ', ' datesplit=' + datesplit)
                
                s_beforedate = datesplit[0]
                //nlapiLogExecution('DEBUG', 'Post results ', ' s_beforedate=' + s_beforedate)
                s_afterdate = datesplit[1]
                //nlapiLogExecution('DEBUG', 'Post results ', ' s_afterdate=' + s_afterdate)
                f_wk1_hours = parseFloat(datesplit[2]);
                
                f_wk2_hours = parseFloat(datesplit[3]);
            }
            
            
            if (s_beforedate == null) {
                oldvalues = false;
            }
            else {
                oldvalues = true;
            }
            
            
            if (s_afterdate == null) {
                oldvalues = false;
            }
            else {
            
                oldvalues = true;
            }
            //nlapiLogExecution('DEBUG', 'GET', ' oldvalues===>' + oldvalues)
            /////////////////////////////////////POST ON SUBMIT ////////////////////////////
            if (oldvalues) {
                form.addButton('custombutton', 'Export As CSV', 'fxn_generatePDF_Dec(\'' + s_beforedate + "@" + s_afterdate + "@" + f_wk1_hours + "@" + f_wk2_hours +'\');');
                
                var id = request.getParameter('id');
                //nlapiLogExecution('DEBUG', 'Post results ', ' id=' + id)
                if (id == 'Export') {
                    var param = request.getParameter('param');
                    //nlapiLogExecution('DEBUG', 'Post results ', ' param=' + param)
                    
                    var datesplit = param.toString().split('@')
                    //nlapiLogExecution('DEBUG', 'Post results ', ' datesplit=' + datesplit)
                    
                    s_beforedate = datesplit[0]
                    s_afterdate = datesplit[1]
                    f_wk1_hours = parseFloat(datesplit[2]);
                    f_wk2_hours = parseFloat(datesplit[3]);
                }
                
                var searchData = SearchData(s_beforedate, s_afterdate)
                //nlapiLogExecution('DEBUG', 'Post results ', ' searchData=' + searchData.length)
                
                //return;
                
                var SearchDTDetailresult = SearchDTData(s_beforedate, s_afterdate)
                //nlapiLogExecution('DEBUG', 'Post results ', ' SearchDTDetailresult=' + SearchDTDetailresult.length)
                
                var weeklyDTTotal = getDTTotalsweekly(sublist1, SearchDTDetailresult, s_afterdate)
                
                var data = setValuesublist(sublist1, searchData, s_afterdate, weeklyDTTotal, s_beforedate, f_wk1_hours, f_wk2_hours)
                
                /////detail search fr OT time sheet fr cal of DT
            
            
                //nlapiLogExecution('DEBUG', 'Post results ', ' searchData=' + data.length)
                //nlapiLogExecution('DEBUG', 'Post results', 's_beforedate=' + s_beforedate)
                //nlapiLogExecution('DEBUG', 'Post results', 's_afterdate=' + s_afterdate)
            }
            
            if (oldvalues) {
                select.setDefaultValue(s_beforedate);
                
            }
            
            if (id != 'Export') {
                form.addSubmitButton('Submit');
                response.writePage(form);
            }
            else {
                Callsuitelet(sublist1, param);
            }
            
        }
        else {
            var date = request.getParameter('custpage_startdate');
            
            var f_week1_hours = request.getParameter('custpage_wk1_hours');
            
            var f_week2_hours = request.getParameter('custpage_wk2_hours');
            //nlapiLogExecution('DEBUG', 'Post results ', ' date =' + date)
            var date1 = nlapiStringToDate(date)
            //nlapiLogExecution('DEBUG', 'Post results ', ' date after string to date1 =' + date1)
            var fromDate = nlapiAddDays(date1, -13)
            //nlapiLogExecution('DEBUG', 'Post results ', ' fromDate =' + fromDate)
            fromDate = nlapiDateToString(fromDate)
            //nlapiLogExecution('DEBUG', 'Post results ', ' fromDate after conversion =' + fromDate)
            
            
            var params = new Array();
            params['custpage_startdate'] = date;
            params['custpage_fromdate'] = fromDate;
            params['custpage_wk1_hours'] = f_week1_hours;
            params['custpage_wk2_hours'] = f_week2_hours;
            nlapiSetRedirectURL('SUITELET', 'customscriptsut_rep_salnonstdrptfrmte_0', 'customdeploy1', false, params);
            //nlapiSetRedirectURL('SUITELET', 'customscriptsut_report_salnonstdrptfrom', 'customdeploy1', false, params);
        }
    } 
    catch (e) {
        //nlapiLogExecution('DEBUG', 'Search results ', ' value of e-====== **************** =' + e)
    }
}

function setValuesublist(sublist1, searchData, s_afterdate1, weeklyDTTotal, s_beforedate, f_week1_hours, f_week2_hours){
    /*
     var s_CoCode = sublist1.addField('custevent_cocode', 'text', 'Co Code');
     var s_batchid = sublist1.addField('custevent_batchid', 'text', 'Batch ID');
     */
    var s_file = sublist1.addField('custevent_file', 'text', 'GCI(INC) ID');
    var s_Employeefile = sublist1.addField('custevent_employeeid', 'text', 'Fusion ID');
    var s_firstName = sublist1.addField('custevent_firstname', 'text', 'First Name');
    var s_lastName = sublist1.addField('custevent_lastname', 'text', 'Last Name');
    var s_WK_ST = sublist1.addField('custevent_wkst1', 'text', 'WK1 ST');
    s_WK_ST.setDefaultValue('0');
    
    var s_WK_OT = sublist1.addField('custevent_wkot1', 'text', 'WK1 OT');
    var s_WK_DT = sublist1.addField('custevent_wkdt1', 'text', 'WK1 DT');
    var s_WK1_Timeoff = sublist1.addField('custevent_wk1timeoff1', 'text', 'WK1 TIMEOFF');
    var s_WK2_ST = sublist1.addField('custevent_wkst2', 'text', 'WK2 ST');
    var s_WK2_OT = sublist1.addField('custevent_wkot2', 'text', 'WK2 OT');
    var s_WK2_DT = sublist1.addField('custevent_wkdt2', 'text', 'WK2 DT');
    var s_WK2_Timeoff = sublist1.addField('custevent_wk1timeoff2', 'text', 'WK2 TIMEOFF');
    var s_ot_hours = sublist1.addField('custevent_othours', 'text', 'OT Hours');
    var s_Total_Time_Off = sublist1.addField('custevent_totaltimeoff', 'text', 'Total Timeoff');
    var s_Total_DT_hrs = sublist1.addField('custevent_totaldttime', 'text', 'Total DT');
    var s_Floating_H = sublist1.addField('custevent_floatingh', 'text', 'Float H(if in either wk)');
    
    var s_ProjectCity = sublist1.addField('custevent_projectcity', 'text', 'Project_City');
    var s_ProjectLocation = sublist1.addField('custevent_projectloc', 'text', 'Project Location');
    var s_Employee_Type = sublist1.addField('custevent_employeetype', 'text', 'Employee_Type');
    var s_Department = sublist1.addField('custevent_department', 'textarea', 'Department');
    var s_User_Notes = sublist1.addField('custevent_usernotes', 'textarea', 'User_Notes');
    var s_Customer = sublist1.addField('custevent_customer', 'text', 'Customer');
    var s_id = ''
    var linenumber = 1;
    var tempInc = ''
    var currentLinenumber = 0;
    var tempdate = '';
    var includerecord = ''
    /////////////get stardateweekno
    s_afterdate1 = nlapiStringToDate(s_afterdate1);
    //************** pass the date as parameter to getWeekNumber function *****************//
    var startdateweek = getWeekNumber(s_afterdate1)
    //nlapiLogExecution('DEBUG', 'beforeLoad_CalculateWeek', 'startdateweek=' + startdateweek);
    //////////////////////////////////////
    
    
    var a_result_array = new Array();
    var i_totalTime = 0;
    var i_totalTime_wk2 = 0;
    var dtsetvalue = false;
    var i_week1_st = 0;
    var i_week1_lv = 0;
    var i_week1_hl = 0;
    var i_week1_fl = 0;
    
    var i_week2_st = 0;
    var i_week2_lv = 0;
    var i_week2_hl = 0;
    var i_week2_fl = 0;
    
    
    if (searchData.length != '' && searchData.length != null && searchData.length != 'undefined') {
        /////////Loop through rows of search/////////////////////////////////////////////////////
        for (var c = 0; c < searchData.length; c++) {
            var i_Calendar_Week = '';
            var i_Employee = '';
            var i_Duration = '';
            var i_Project_Task_Name = '';
            var i_Duration_decimal = 0;
            var i_Dayofweek = '';
            
            var i_Employee_First_Name = '';
            var i_Employee_Last_Name = '';
            var i_Employee_Inc_Id = '';
            var Employee_Type = '';
            var Employment_Category = '';
            var i_note = '';
            var s_Customer = '';
            var a_search_transaction_result = searchData[c];
            //nlapiLogExecution('DEBUG', 'search_ComplaintCategory', 'a_search_transaction_result  -->' + a_search_transaction_result);
            //nlapiLogExecution('DEBUG', 'search_ComplaintCategory', 'c  -->' + c);
            var columns = a_search_transaction_result.getAllColumns();
            //nlapiLogExecution('DEBUG', 'search_ComplaintCategory', 'columns  -->' + columns);
            
            var columnLen = columns.length;
            //nlapiLogExecution('DEBUG', 'search_ComplaintCategory', 'columnLen  -->' + columnLen);
            
            
            for (var hg = 0; hg < columnLen; hg++) {
            
                var column = columns[hg];
                //nlapiLogExecution('DEBUG', 'search_ComplaintCategory', 'column  -->' + column);
                var label = column.getLabel();
                //nlapiLogExecution('DEBUG', 'search_ComplaintCategory', '*************************label********************  -->' + label);
                
                var value = a_search_transaction_result.getValue(column);
                var text = a_search_transaction_result.getText(column);
                //nlapiLogExecution('DEBUG', 'search_ComplaintCategory', 'value  -->' + value);
                if (label == 'Calendar Week') {
                    i_Calendar_Week = value;
                }
                if (label == 'Employee') {
                    i_Employee = value;
                }
                if (label == 'Duration') {
                    i_Duration = value;
                }
                if (label == 'Project Task : Name') {
                    i_Project_Task_Name = value;
                }
                if (label == 'Duration:Decimal') {
                    i_Duration_decimal = parseFloat(value);
                }
                if (label == 'Employee : First Name') {
                    i_Employee_First_Name = value;
                }
                if (label == 'Employee : Last Name') {
                    i_Employee_Last_Name = value;
                }
                if (label == 'Employee : Inc Id') {
                    i_Employee_Inc_Id = value;
                }
                /*
                 if (label == 'Employee : Type') {
                 Employee_Type = text;
                 }
                 */
                if (label == 'Employee : Employment Category') {
                    Employment_Category = value;
                }
                if (label == 'Note') {
                    i_note = value;
                }
                if (label == 'User_Notes') {
                    i_note = value;
                }
                if (label == 'Department') {
                    s_Department = text;
                }
                if (label == 'Emp ID') {
                    s_Employeefile = value;
                }
                if (label == 'Project Location') {
                    s_ProjectLocation = text;
                    
                }
                if (label == 'Project City') {
                    s_ProjectCity = text;
                }
                
                if (label == 'EmpIntID') {
                    s_id = value;
                }
                if (label == 'Dayofweek') {/////Added for sunday hrs
                    i_Dayofweek = value;
                }
                if(label == 'Customer') {
                	s_Customer = text;
                }
                //nlapiLogExecution('DEBUG', 'SearchData', 'hg===' + hg)
            }
            //column for loop end
            
            if (s_Employeefile != '' && s_Employeefile != null && s_Employeefile != 'undefined' && s_Employeefile != '- None -') {
                s_Employeefile = s_Employeefile;
            }
            else {
                s_Employeefile = '';
            }
            
            
            if (i_Employee_First_Name != '' && i_Employee_First_Name != null && i_Employee_First_Name != 'undefined' && i_Employee_First_Name != '- None -') {
                i_Employee_First_Name = i_Employee_First_Name;
            }
            else {
                i_Employee_First_Name = '';
            }
            
            if (i_Employee_Last_Name != '' && i_Employee_Last_Name != null && i_Employee_Last_Name != 'undefined' && i_Employee_Last_Name != '- None -') {
                i_Employee_Last_Name = i_Employee_Last_Name;
            }
            else {
                i_Employee_Last_Name = '';
            }
            
            if (s_Department != '' && s_Department != null && s_Department != 'undefined' && s_Department != '- None -') {
                s_Department = s_Department;
            }
            else {
                s_Department = '';
            }
            if (i_Employee_Inc_Id != '' && i_Employee_Inc_Id != null && i_Employee_Inc_Id != 'undefined' && i_Employee_Inc_Id != '- None -') {
                i_Employee_Inc_Id = i_Employee_Inc_Id;
            }
            else {
                i_Employee_Inc_Id = '';
            }
            if (i_note != '' && i_note != null && i_note != 'undefined' && i_note != '- None -') {
                i_note = i_note;
            }
            else {
                i_note = '';
            }
            if (Employment_Category != '' && Employment_Category != null && Employment_Category != 'undefined' && Employment_Category != '- None -') {
                Employment_Category = Employment_Category;
            }
            else {
                Employment_Category = '';
            }
            
            if (s_ProjectLocation != '' && s_ProjectLocation != null && s_ProjectLocation != 'undefined' && s_ProjectLocation != '- None -') {
                s_ProjectLocation = s_ProjectLocation;
            }
            else {
                s_ProjectLocation = '';
            }
            if (s_ProjectCity != '' && s_ProjectCity != null && s_ProjectCity != 'undefined' && s_ProjectCity != '- None -') {
                s_ProjectCity = s_ProjectCity;
            }
            else {
                s_ProjectCity = '';
            }
            if (s_Customer != '' && s_Customer != null && s_Customer != 'undefined' && s_Customer != '- None -') {
                s_Customer = s_Customer;
            }
            else {
                s_Customer = '';
            }
            
            //////////////////Uppercase conversion of taksnames for comparison to get week hrs as all ids per project are different/////////////
            i_Project_Task_Name = i_Project_Task_Name.toString().toUpperCase();
            
            var s_OTTask = "OT"
            var a_STTask = [("Project Activities").toUpperCase(), ("Bench Task").toUpperCase()];
            var s_LeaveTask = "Leave"
            var s_HolidayTask = "Holiday"
            var s_FHTask = "Floating Holiday"
            
            
            s_OTTask = s_OTTask.toString().toUpperCase();
            //s_STTask = s_STTask.toString().toUpperCase();
            s_LeaveTask = s_LeaveTask.toString().toUpperCase();
            s_HolidayTask = s_HolidayTask.toString().toUpperCase();
            s_FHTask = s_FHTask.toString().toUpperCase();
            
            //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	
            
            //nlapiLogExecution('DEBUG', 'SearchData', 'tempInc===' + tempInc)
            //nlapiLogExecution('DEBUG', 'SearchData', 's_id===' + s_id)
            /////calculate current date week/////////////////////
            if (i_Calendar_Week != s_beforedate) {
                i_Calendar_Week = nlapiStringToDate(i_Calendar_Week);
                i_Calendar_Week = nlapiAddDays(i_Calendar_Week, 1)
            }
            var currentdateweek = getWeekNumber(i_Calendar_Week)
            
            
            
            ////**************Calculate the week of the Dayofweek column to get if there is any sunday filled time sheets with not matching weekno.
            /////Added for sunday hrs/////////////////////////////////////////////////
            var i_Calendar_Week_Dayofweekdate = nlapiStringToDate(i_Dayofweek);
            var Dayofweekno = getWeekNumber(i_Calendar_Week_Dayofweekdate);
            //nlapiLogExecution('DEBUG', 'SearchData', 'Dayofweekno===' + Dayofweekno)
            /////////////////////////////new code added to consider sunday also in same week missed in search/////////////////////////////////////
            
            
            var defaultvalue = 0.0;
            
            
            
            if (tempInc != s_id) {
            
                i_totalTime = 0;
                i_totalTime_wk2 = 0;
                i_week1_st = 0;
                i_week2_st = 0;
                var i_Week1FloatingHoliday = 0.0;
                var i_Week2FloatingHoliday = 0.0;
                var excludeValues = searchexcludeValues(s_id, searchData, startdateweek, currentdateweek, s_beforedate, c, f_week1_hours, f_week2_hours)
                //nlapiLogExecution('DEBUG', 'SearchData', 'excludeValues===' + excludeValues)
                if (!excludeValues) {
                    includerecord = s_id
                }
                
                ////////////////Line will be inclueded according control added///
                if (excludeValues) {
                
                    sublist1.setLineItemValue('custevent_file', linenumber, i_Employee_Inc_Id);
                    sublist1.setLineItemValue('custevent_employeeid', linenumber, s_Employeefile);
                    sublist1.setLineItemValue('custevent_firstname', linenumber, i_Employee_First_Name);
                    sublist1.setLineItemValue('custevent_lastname', linenumber, i_Employee_Last_Name);
                    sublist1.setLineItemValue('custevent_employeetype', linenumber, Employment_Category);
                    sublist1.setLineItemValue('custevent_department', linenumber, s_Department);
                    sublist1.setLineItemValue('custevent_usernotes', linenumber, i_note.toString());
                    sublist1.setLineItemValue('custevent_wkst1', linenumber, parseFloat(defaultvalue));
                    sublist1.setLineItemValue('custevent_wkot1', linenumber, parseFloat(defaultvalue));
                    sublist1.setLineItemValue('custevent_wkot2', linenumber, parseFloat(defaultvalue));
                    
                    sublist1.setLineItemValue('custevent_wkst2', linenumber, parseFloat(defaultvalue));
                    sublist1.setLineItemValue('custevent_wk1timeoff1', linenumber, parseFloat(defaultvalue));
                    sublist1.setLineItemValue('custevent_wk1timeoff2', linenumber, parseFloat(defaultvalue));
                    sublist1.setLineItemValue('custevent_othours', linenumber, parseFloat(defaultvalue));
                    sublist1.setLineItemValue('custevent_totaltimeoff', linenumber, parseFloat(defaultvalue));
                    sublist1.setLineItemValue('custevent_totaldttime', linenumber, parseFloat(defaultvalue));
                    sublist1.setLineItemValue('custevent_wkdt1', linenumber, parseFloat(defaultvalue));
                    sublist1.setLineItemValue('custevent_wkdt2', linenumber, parseFloat(defaultvalue));
                    sublist1.setLineItemValue('custevent_projectcity', linenumber, s_ProjectCity);
                    sublist1.setLineItemValue('custevent_projectloc', linenumber, s_ProjectLocation);
                    sublist1.setLineItemValue('custevent_floatingh', linenumber, parseFloat(defaultvalue));
                    sublist1.setLineItemValue('custevent_customer', linenumber, s_Customer);
                    
                    if (a_STTask.indexOf(i_Project_Task_Name) != -1) {
                        if (startdateweek == currentdateweek || Dayofweekno == startdateweek) {
                            i_week1_st = parseFloat(i_week1_st) + parseFloat(i_Duration_decimal);
                            //nlapiLogExecution('DEBUG', 'Log', 'i_week1_st : '+i_week1_st);
                            ////	sublist1.setLineItemValue('custevent_wkst1', currentLinenumber, parseFloat(i_week1_st));
                            
                            
                        }
                        else 
                            if ((currentdateweek == startdateweek + 1) || (Dayofweekno == startdateweek + 1)) //sec week , Added for sunday hrs 
                            {
                                i_week2_st = parseFloat(i_week2_st) + parseFloat(i_Duration_decimal);
                                //nlapiLogExecution('DEBUG', 'Log', 'i_week2_st : '+i_week2_st);
                                
                            }
                    }
                    else 
                        if (i_Project_Task_Name == s_LeaveTask || i_Project_Task_Name == s_HolidayTask) //
                        {
                            //nlapiLogExecution('DEBUG', 'SearchData', 'i_Project_Task_Name===' + i_Project_Task_Name)
                            
                            
                            if (startdateweek == currentdateweek || Dayofweekno == startdateweek) //
                            {
                                if (i_Project_Task_Name == s_LeaveTask) //
                                {
                                    i_totalTime = parseFloat(i_totalTime) + parseFloat(i_Duration_decimal)
                                //nlapiLogExecution('DEBUG', 'SearchData', 'if i_totalTime in Leave===' + i_totalTime);
                                }
                                else 
                                    if (i_Project_Task_Name == s_HolidayTask) //
                                    {
                                        //i_totalTime = parseFloat(i_totalTime) + parseFloat(i_Duration_decimal)
                                    //nlapiLogExecution('DEBUG', 'SearchData', 'if i_totalTime in Holiday===' + i_totalTime);
                                    }
                                
                                //sublist1.setLineItemValue('custevent_wk1timeoff1', linenumber, i_totalTime);
                            //nlapiLogExecution('DEBUG', 'SearchData', 'custevent_wk1timeoff1===' + i_totalTime);
                            
                            }
                            else 
                                if ((currentdateweek == startdateweek + 1) || (Dayofweekno == startdateweek + 1)) //sec week , Added for sunday hrs 
                                {
                                    if (i_Project_Task_Name == s_LeaveTask) //
                                    {
                                        i_totalTime_wk2 = parseFloat(i_totalTime_wk2) + parseFloat(i_Duration_decimal)
                                    //nlapiLogExecution('DEBUG', 'SearchData', 'if i_totalTime in Leave===' + i_totalTime);
                                    }
                                    else 
                                        if (i_Project_Task_Name == s_HolidayTask) //
                                        {
                                            //i_totalTime_wk2 = parseFloat(i_totalTime_wk2) + parseFloat(i_Duration_decimal)
                                        //nlapiLogExecution('DEBUG', 'SearchData', 'if i_totalTime in Holiday===' + i_totalTime);
                                        }
                                    
                                    //sublist1.setLineItemValue('custevent_wk1timeoff2', linenumber, i_totalTime_wk2);
                                //nlapiLogExecution('DEBUG', 'SearchData', 'custevent_wk1timeoff2===' + i_totalTime_wk2);
                                
                                }
                            
                        }
                        else 
                            if (i_Project_Task_Name == s_FHTask && parseFloat(i_Duration_decimal) > 0) //
                            {
                                sublist1.setLineItemValue('custevent_floatingh', linenumber, parseFloat(1));
                                
                                if (startdateweek == currentdateweek || Dayofweekno == startdateweek) {
                                	i_Week1FloatingHoliday++;
                                    //var i_timeoff_wk1 = sublist1.getLineItemValue('custevent_wk1timeoff1', linenumber);
                                    //sublist1.setLineItemValue('custevent_wk1timeoff1', linenumber, parseFloat(i_timeoff_wk1) - 8.0)
                                }
                                else 
                                    if ((currentdateweek == startdateweek + 1) || (Dayofweekno == startdateweek + 1)) //sec week , Added for sunday hrs
                                    {
                                    	i_Week2FloatingHoliday++;
                                    	//var i_timeoff_wk2 = sublist1.getLineItemValue('custevent_wk1timeoff2', linenumber);
                                        //sublist1.setLineItemValue('custevent_wk1timeoff2', linenumber, parseFloat(i_timeoff_wk2) - 8.0)    
                                    }
                            }
                            else //
                            {
                            	//Amol: The below line is not required
                                //sublist1.setLineItemValue('custevent_floatingh', linenumber, parseFloat(0));
                            }
                    
                    sublist1.setLineItemValue('custevent_wkst1', linenumber, parseFloat(i_week1_st));
                    if(parseFloat(i_week1_st) != 0)
                    	{
                    		sublist1.setLineItemValue('custevent_wk1timeoff1', linenumber, f_week1_hours - parseFloat(i_week1_st) - (i_Week1FloatingHoliday > 0?1.0:0.0) * 8.0);
                    	}
                    else if(parseFloat(i_totalTime) != 0)
                    	{
                    		sublist1.setLineItemValue('custevent_wk1timeoff1', linenumber, parseFloat(i_totalTime));
                    	}
                    
                    
                    sublist1.setLineItemValue('custevent_wkst2', linenumber, parseFloat(i_week2_st));
                    if(parseFloat(i_week2_st) != 0)
                    	{
                    		sublist1.setLineItemValue('custevent_wk1timeoff2', linenumber, f_week2_hours - parseFloat(i_week2_st) - (i_Week2FloatingHoliday > 0?1.0:0.0) * 8.0);
                    	}
                    else if(parseFloat(i_totalTime_wk2) != 0)
                    	{
                    		sublist1.setLineItemValue('custevent_wk1timeoff2', linenumber, parseFloat(i_totalTime_wk2));
                    	}
                    
                    currentLinenumber = linenumber;
                    
                    // Amol: Add total for one line also
                    
      //////////////OT and Week off totals columns calculations//////////////////////////////////////
                    var week1OT = sublist1.getLineItemValue('custevent_wkot1', currentLinenumber);
                    var week2OT = sublist1.getLineItemValue('custevent_wkot2', currentLinenumber);
                    
                    
                    //nlapiLogExecution('DEBUG', 'SearchData', 'week1OT,week2OT===' + week1OT + ',,' + week2OT);
                    if (week1OT == '' || week1OT == 'undefined' || week1OT == null) {
                        week1OT = 0;
                    }
                    if (week2OT == '' || week2OT == 'undefined' || week2OT == null) {
                        week2OT = 0;
                    }
                    //nlapiLogExecution('DEBUG', 'SearchData', 'week1OT,week2OT===' + week1OT + ',,' + week2OT);
                    var OTweekstotals = parseFloat(week1OT) + parseFloat(week2OT);
                    //nlapiLogExecution('DEBUG', 'SearchData', 'week1OT,week2OT===' + parseFloat(week1OT) + ',,' + parseFloat(week2OT));
                    //nlapiLogExecution('DEBUG', 'SearchData', 'OTweekstotals===' + OTweekstotals);
                    sublist1.setLineItemValue('custevent_othours', currentLinenumber, parseFloat(OTweekstotals));
                    
                    var week1off = sublist1.getLineItemValue('custevent_wk1timeoff1', currentLinenumber);
                    var week2off = sublist1.getLineItemValue('custevent_wk1timeoff2', currentLinenumber);
                    //nlapiLogExecution('DEBUG', 'SearchData', 'week1OT,week2OT===' + week1OT + ',,' + week2OT);
                    if (week1off == '' || week1off == 'undefined' || week1off == null) {
                        week1off = 0;
                    }
                    if (week2off == '' || week2off == 'undefined' || week2off == null) {
                        week2off = 0;
                    }
                    //nlapiLogExecution('DEBUG', 'SearchData', 'week1off,week2off===' + week1off + ',,' + week2off);
                    var weeksOFFtotals = parseFloat(week1off) + parseFloat(week2off);
                    //nlapiLogExecution('DEBUG', 'SearchData', 'weeksOFFtotals===' + weeksOFFtotals);
                    sublist1.setLineItemValue('custevent_totaltimeoff', currentLinenumber, parseFloat(weeksOFFtotals));
                    
                    var week1dt = sublist1.getLineItemValue('custevent_wkdt1', currentLinenumber);
                    var week2dt = sublist1.getLineItemValue('custevent_wkdt2', currentLinenumber);
                    //nlapiLogExecution('DEBUG', 'SearchData', 'week1dt,week2dt===' + week1dt + ',,' + week2dt);
                    if (week1dt == '' || week1dt == 'undefined' || week1dt == null) {
                        week1dt = 0;
                    }
                    if (week2dt == '' || week2dt == 'undefined' || week2dt == null) {
                        week2dt = 0;
                    }
                    //nlapiLogExecution('DEBUG', 'SearchData', 'week1off,week2off===' + week1off + ',,' + week2off);
                    var weeksdttotals = parseFloat(week1dt) + parseFloat(week2dt);
                    //nlapiLogExecution('DEBUG', 'SearchData', 'weeksOFFtotals===' + weeksOFFtotals);
                    sublist1.setLineItemValue('custevent_totaldttime', currentLinenumber, parseFloat(weeksdttotals));
                ////////////////////////////////////////////////////////////////////////////////////////////////
                    
                    linenumber++;
                }
            }
            else 
                if (includerecord != s_id) {
                
                    if (tempInc == s_id) {
                    
                    
                        if (a_STTask.indexOf(i_Project_Task_Name) != -1) {
                            if (startdateweek == currentdateweek || Dayofweekno == startdateweek) {
                            
                                i_week1_st = parseFloat(i_week1_st) + parseFloat(i_Duration_decimal);
                                //nlapiLogExecution('DEBUG', 'Log', 'i_week1_st : '+i_week1_st);
                            }
                            else 
                                if ((currentdateweek == startdateweek + 1) || (Dayofweekno == startdateweek + 1)) //sec week , Added for sunday hrs
                                {
                                    i_week2_st = parseFloat(i_week2_st) + parseFloat(i_Duration_decimal);
                                    //nlapiLogExecution('DEBUG', 'Log', 'i_week2_st : '+i_week2_st);
                                    
                                //sublist1.setLineItemValue('custevent_wkst2', currentLinenumber, parseFloat(i_Duration_decimal));
                                }
                        }
                        else 
                            if (i_Project_Task_Name == s_LeaveTask || i_Project_Task_Name == s_HolidayTask) //
                            {
                                if (startdateweek == currentdateweek || Dayofweekno == startdateweek) {
                                    if (i_Project_Task_Name == s_LeaveTask) //
                                    {
                                        i_totalTime = parseFloat(i_totalTime) + parseFloat(i_Duration_decimal)
                                    }
                                    else //
                                         if (i_Project_Task_Name == s_HolidayTask) //
                                        {
                                            //nlapiLogExecution('DEBUG', 'SearchData', 'i_Project_Task_Name===' + i_Project_Task_Name)
                                            //i_totalTime = parseFloat(i_totalTime) + parseFloat(i_Duration_decimal)
                                        }
                                    
                                    //sublist1.setLineItemValue('custevent_wk1timeoff1', currentLinenumber, i_totalTime);
                                }
                                else 
                                    if ((currentdateweek == startdateweek + 1) || (Dayofweekno == startdateweek + 1)) //sec week , Added for sunday hrs
                                    {
                                        if (i_Project_Task_Name == s_LeaveTask) //
                                        {
                                            i_totalTime_wk2 = parseFloat(i_totalTime_wk2) + parseFloat(i_Duration_decimal)
                                        }
                                        else //
                                             if (i_Project_Task_Name == s_HolidayTask) //
                                            {
                                                //nlapiLogExecution('DEBUG', 'SearchData', 'i_Project_Task_Name===' + i_Project_Task_Name)
                                                //i_totalTime_wk2 = parseFloat(i_totalTime_wk2) + parseFloat(i_Duration_decimal)
                                                
                                            }
                                        //nlapiLogExecution('DEBUG', 'SearchData', 'In week off 2');
                                        //sublist1.setLineItemValue('custevent_wk1timeoff2', currentLinenumber, i_totalTime_wk2);
                                    }
                            }
                            else 
                                if (i_Project_Task_Name == s_FHTask && parseFloat(i_Duration_decimal) > 0) //
                                {
                                    sublist1.setLineItemValue('custevent_floatingh', currentLinenumber, parseFloat(1));
                                    
                                    if (startdateweek == currentdateweek || Dayofweekno == startdateweek) {
                                    	i_Week1FloatingHoliday++;
                                        //var i_timeoff_wk1 = sublist1.getLineItemValue('custevent_wk1timeoff1', currentLinenumber);
                                        //sublist1.setLineItemValue('custevent_wk1timeoff1', currentLinenumber, parseFloat(i_timeoff_wk1) - 8.0)
                                    }
                                    else 
                                        if ((currentdateweek == startdateweek + 1) || (Dayofweekno == startdateweek + 1)) //sec week , Added for sunday hrs
                                        {
                                        	i_Week2FloatingHoliday++;
                                        	//var i_timeoff_wk2 = sublist1.getLineItemValue('custevent_wk1timeoff2', currentLinenumber);
                                            //sublist1.setLineItemValue('custevent_wk1timeoff2', currentLinenumber, parseFloat(i_timeoff_wk2) - 8.0)    
                                        }
                                }
                                else //
                                {
                                	//Amol: The below line is not required
                                    //sublist1.setLineItemValue('custevent_floatingh', currentLinenumber, parseFloat(0));
                                }
                        
                        sublist1.setLineItemValue('custevent_wkst1', currentLinenumber, parseFloat(i_week1_st));
                        if(parseFloat(i_week1_st) != 0)
                        	{
                        		sublist1.setLineItemValue('custevent_wk1timeoff1', currentLinenumber, f_week1_hours - parseFloat(i_week1_st) - (i_Week1FloatingHoliday > 0?1.0:0.0) * 8.0);
                        	}
                        else if(parseFloat(i_totalTime) != 0)
                        	{
                        		sublist1.setLineItemValue('custevent_wk1timeoff1', currentLinenumber, parseFloat(i_totalTime));
                        	}
                        
                        sublist1.setLineItemValue('custevent_wkst2', currentLinenumber, parseFloat(i_week2_st));
                        if(parseFloat(i_week2_st) != 0) 
                        	{
                        		sublist1.setLineItemValue('custevent_wk1timeoff2', currentLinenumber, f_week2_hours - parseFloat(i_week2_st) - (i_Week2FloatingHoliday > 0?1.0:0.0) * 8.0);
                        	}
                        else if(parseFloat(i_totalTime_wk2) != 0)
                        	{
                        		sublist1.setLineItemValue('custevent_wk1timeoff2', currentLinenumber, parseFloat(i_totalTime_wk2));
                        	}
                        
                        ////////////////DT Column set///////////////////////
                        //nlapiLogExecution('DEBUG', 'SearchData', 'dtsetvalue===' + dtsetvalue)
                        //if (s_ProjectLocation == "California") {
                        //if (!dtsetvalue) {
                        dtsetvalue = setDTvalue(sublist1, currentLinenumber, weeklyDTTotal, s_id, startdateweek, currentdateweek, s_ProjectLocation);
                        //nlapiLogExecution('DEBUG', 'SearchData', 'dtsetvalue' + dtsetvalue);
                        //}
                        
                        //}
                        
                        //nlapiLogExecution('DEBUG', 'SearchData', 'Settiing DT value');
                        //////////////OT and Week off totals columns calculations//////////////////////////////////////
                        var week1OT = sublist1.getLineItemValue('custevent_wkot1', currentLinenumber);
                        var week2OT = sublist1.getLineItemValue('custevent_wkot2', currentLinenumber);
                        
                        
                        //nlapiLogExecution('DEBUG', 'SearchData', 'week1OT,week2OT===' + week1OT + ',,' + week2OT);
                        if (week1OT == '' || week1OT == 'undefined' || week1OT == null) {
                            week1OT = 0;
                        }
                        if (week2OT == '' || week2OT == 'undefined' || week2OT == null) {
                            week2OT = 0;
                        }
                        //nlapiLogExecution('DEBUG', 'SearchData', 'week1OT,week2OT===' + week1OT + ',,' + week2OT);
                        var OTweekstotals = parseFloat(week1OT) + parseFloat(week2OT);
                        //nlapiLogExecution('DEBUG', 'SearchData', 'week1OT,week2OT===' + parseFloat(week1OT) + ',,' + parseFloat(week2OT));
                        //nlapiLogExecution('DEBUG', 'SearchData', 'OTweekstotals===' + OTweekstotals);
                        sublist1.setLineItemValue('custevent_othours', currentLinenumber, parseFloat(OTweekstotals));
                        
                        var week1off = sublist1.getLineItemValue('custevent_wk1timeoff1', currentLinenumber);
                        var week2off = sublist1.getLineItemValue('custevent_wk1timeoff2', currentLinenumber);
                        //nlapiLogExecution('DEBUG', 'SearchData', 'week1OT,week2OT===' + week1OT + ',,' + week2OT);
                        if (week1off == '' || week1off == 'undefined' || week1off == null) {
                            week1off = 0;
                        }
                        if (week2off == '' || week2off == 'undefined' || week2off == null) {
                            week2off = 0;
                        }
                        //nlapiLogExecution('DEBUG', 'SearchData', 'week1off,week2off===' + week1off + ',,' + week2off);
                        var weeksOFFtotals = parseFloat(week1off) + parseFloat(week2off);
                        //nlapiLogExecution('DEBUG', 'SearchData', 'weeksOFFtotals===' + weeksOFFtotals);
                        sublist1.setLineItemValue('custevent_totaltimeoff', currentLinenumber, parseFloat(weeksOFFtotals));
                        
                        var week1dt = sublist1.getLineItemValue('custevent_wkdt1', currentLinenumber);
                        var week2dt = sublist1.getLineItemValue('custevent_wkdt2', currentLinenumber);
                        //nlapiLogExecution('DEBUG', 'SearchData', 'week1dt,week2dt===' + week1dt + ',,' + week2dt);
                        if (week1dt == '' || week1dt == 'undefined' || week1dt == null) {
                            week1dt = 0;
                        }
                        if (week2dt == '' || week2dt == 'undefined' || week2dt == null) {
                            week2dt = 0;
                        }
                        //nlapiLogExecution('DEBUG', 'SearchData', 'week1off,week2off===' + week1off + ',,' + week2off);
                        var weeksdttotals = parseFloat(week1dt) + parseFloat(week2dt);
                        //nlapiLogExecution('DEBUG', 'SearchData', 'weeksOFFtotals===' + weeksOFFtotals);
                        sublist1.setLineItemValue('custevent_totaldttime', currentLinenumber, parseFloat(weeksdttotals));
                    ////////////////////////////////////////////////////////////////////////////////////////////////
                    }
                }
            
            tempInc = s_id
            
            
        }
    }
    //nlapiLogExecution('DEBUG', 'SearchData', 'a_result_array===' + a_result_array)
    //nlapiLogExecution('DEBUG', 'SearchData', 'a_result_array===' + a_result_array.length)
    return a_result_array;
}

function _SearchData(s_beforedate, s_afterdate){
    /*
     //nlapiLogExecution('DEBUG', 'SearchData', 'In Search Data')
     //nlapiLogExecution('DEBUG', 'SearchData', 's_beforedate===' + s_beforedate)
     //nlapiLogExecution('DEBUG', 'SearchData', 's_afterdate===' + s_afterdate)
     */
    var a_filters = new Array();
    //a_filters[0] = new nlobjSearchFilter('date', null, 'on', s_beforedate);
    a_filters[0] = new nlobjSearchFilter('date', null, 'onorbefore', s_beforedate);
    a_filters[1] = new nlobjSearchFilter('date', null, 'onorafter', s_afterdate);
    
    
    var a_columns = new Array();
    a_columns[0] = new nlobjSearchColumn('employee');
    a_columns[1] = new nlobjSearchColumn('item');
    
    var searchresult = nlapiSearchRecord('timebill', 'customsearchsalnonstdrptts', a_filters, null);
    return searchresult
}

function SearchData(s_beforedate, s_afterdate) //
{
    var LastfetchedInternalid_T = 0;
    var searchresultFinal_T = new Array();
    var searchresult_T = new Array();
    
    do //
    {
        var a_filters = new Array();
        a_filters[0] = new nlobjSearchFilter('date', null, 'onorbefore', s_beforedate);
        a_filters[1] = new nlobjSearchFilter('date', null, 'onorafter', s_afterdate);
        a_filters[2] = new nlobjSearchFilter('internalidnumber', 'employee', 'greaterthan', LastfetchedInternalid_T);////is not before 1sept
        var a_columns = new Array();
        a_columns[0] = new nlobjSearchColumn('employee');
        a_columns[1] = new nlobjSearchColumn('item');
        
        var searchresult_T = nlapiSearchRecord('timebill', 'customsearchsalnonstdrptts', a_filters, null);
        
        if (searchresult_T != null && searchresult_T != undefined && searchresult_T != '') //
        {
            //nlapiLogExecution('DEBUG', 'SearchData', 'searchresult_T == ' + searchresult_T.length);
            var temp_emp = '';
            var emp = '';
            
            for (var i = 0; i < searchresult_T.length; i++) //
            {
                var a_search_empdata = searchresult_T[i];
                var all_columns = a_search_empdata.getAllColumns();
                emp = a_search_empdata.getValue(all_columns[13]);
                
                if (emp != null) //
                {
                    if (i == 0) //
                    {
                        temp_emp = emp;
                    }
                    
                    LastfetchedInternalid_T = temp_emp;
                    //nlapiLogExecution('DEBUG', 'SearchData', 'i : ' + i + ' temp_emp ==' + temp_emp);
                    if (emp == temp_emp) //if employee id is same then add to final array
                    {
                        searchresultFinal_T.push(searchresult_T[i]);
                    }
                    else //else employee id does nt match
                    {
                        //nlapiLogExecution('DEBUG', 'SearchData', 'Emp change row number : ' + i);
                        if (i > 950)//
                        {
                            //nlapiLogExecution('DEBUG', 'SearchData', 'LastfetchedInternalid_T==' + LastfetchedInternalid_T);
                            break;
                        }
                        else //
                        {
                            searchresultFinal_T.push(searchresult_T[i]);
                            temp_emp = emp;
							LastfetchedInternalid_T = temp_emp;
                        }
                    }
                }
                //nlapiLogExecution('DEBUG','researchcodes','lastid=='+a_search_empdata.getValue('internalid',null,'group'));
            }
            //nlapiLogExecution('DEBUG','researchcodes','lastid=='+a_search_empdata.getValue('internalid',null,'group'));
            //LastfetchedInternalid_T = searchresult_T[searchresult_T.length - 1].getValue('internalid', 'employee', 'group');
        
        }
        //return searchresult
    }
    while (searchresult_T != null && searchresult_T != undefined && searchresult_T != '') //
    {
    
    }
    
    //nlapiLogExecution('DEBUG', 'SearchData', 'searchresultFinal_T length==' + searchresultFinal_T.length);
    return searchresultFinal_T;
}

/*
 Function name :-getWeekNumber()
 Parameter :- date
 return type :- returns week number of given year
 */
function getWeekNumber(d){
    //Copy date so don't modify original
    d = new Date(+d);
    d.setHours(0, 0, 0);
    //Set to nearest Thursday: current date + 4 - current day number
    //Make Sunday's day number 7
    d.setDate(d.getDate() + 4 - (d.getDay() || 7));
    //Get first day of year
    var yearStart = new Date(d.getFullYear(), 0, 1);
    //Calculate full weeks to nearest Thursday
    var weekNo = Math.ceil((((d - yearStart) / 86400000) + 1) / 7)
    //nlapiLogExecution('DEBUG', 'beforeLoad_CalculateWeek', 'weekNo=' + weekNo);
    //Return array of year and week number
    return weekNo;
}

function Callsuitelet(sublist1, param){

    var count = sublist1.getLineItemCount();
    //alert(count);
    
    var globalArray = new Array();
    var arr = new Array();
    var month1 = nlapiGetFieldText('custpage_startdate');
    
    var curUser = nlapiGetUser()
    //alert(month1)
    for (var i = 1; i <= count; i++) {
        //var cocode = sublist1.getLineItemValue('custevent_cocode', i);
        ////alert(custevent_pruchesdetalis);
        //var batchid = sublist1.getLineItemValue('custevent_batchid', i);
        //alert(custevent_devicesold);
        var file = sublist1.getLineItemValue('custevent_file', i);
        //alert('custevent_month0' + custevent_month0);
        var firstname = sublist1.getLineItemValue('custevent_firstname', i);
        //alert('custevent_month1' + custevent_month1);
        var lastname = sublist1.getLineItemValue('custevent_lastname', i);
        
        if (file != '' && file != null && file != 'undefined') {
            file = file
        }
        else {
            file = ''
        }
        
        if (firstname != '' && firstname != null && firstname != 'undefined') {
            firstname = firstname
        }
        else {
            firstname = ''
        }
        if (lastname != '' && lastname != null && lastname != 'undefined') {
            lastname = lastname
        }
        else {
            lastname = ''
        }
        //alert('custevent_month2' + custevent_month2);
        var wkst1 = sublist1.getLineItemValue('custevent_wkst1', i);
        //alert('custevent_month3' + custevent_month3);
        var wkot1 = sublist1.getLineItemValue('custevent_wkot1', i);
        //alert('custevent_month4' + custevent_month4);
        var wkdt1 = sublist1.getLineItemValue('custevent_wkdt1', i);
        //alert('custevent_month5' + custevent_month5);
        var wk1timeoff1 = sublist1.getLineItemValue('custevent_wk1timeoff1', i);
        //alert('custevent_month6' + custevent_month6);
        var wkst2 = sublist1.getLineItemValue('custevent_wkst2', i);
        //alert('custevent_month7' + custevent_month7);
        var wkot2 = sublist1.getLineItemValue('custevent_wkot2', i);
        //alert('custevent_month8' + custevent_month8);
        var wkdt2 = sublist1.getLineItemValue('custevent_wkdt2', i);
        //alert('custevent_month9' + custevent_month9);
        var wk1timeoff2 = sublist1.getLineItemValue('custevent_wk1timeoff2', i);
        var totalothours = sublist1.getLineItemValue('custevent_othours', i);
        //alert('custevent_month10' + custevent_month10);
        var totaltimeoff = sublist1.getLineItemValue('custevent_totaltimeoff', i);
        var total_dt_time = sublist1.getLineItemValue('custevent_totaldttime', i);
        var floatingh = sublist1.getLineItemValue('custevent_floatingh', i);
        var employeeid = sublist1.getLineItemValue('custevent_employeeid', i);
        var department = sublist1.getLineItemValue('custevent_department', i);
        var projectloc = sublist1.getLineItemValue('custevent_projectloc', i);
        
        var projectcity = sublist1.getLineItemValue('custevent_projectcity', i);
        var employeetype = sublist1.getLineItemValue('custevent_employeetype', i);
        var usernotes = sublist1.getLineItemValue('custevent_usernotes', i);
        var customer = sublist1.getLineItemValue('custevent_customer', i);
        
        if (projectloc != '' && projectloc != null && projectloc != 'undefined') {
            projectloc = projectloc
            projectloc = projectloc.toString().replace(/[,]/g, "; ")
        }
        else {
            projectcity = ''
        }
        if (usernotes != '' && usernotes != null && usernotes != 'undefined') {
            usernotes = usernotes
        }
        else {
            usernotes = ''
        }
        if (employeetype != '' && employeetype != null && employeetype != 'undefined') {
            employeetype = employeetype
        }
        else {
            employeetype = ''
        }
        usernotes = usernotes.toString().replace(/[|]/g, " ")
        usernotes = usernotes.toString().replace(/[,]/g, " ")
        //nlapiLogExecution('DEBUG', 'beforeLoad_CalculateWeek', 'usernotes-----' + usernotes);
        
        if (i == 1) {
            //nlapiLogExecution('DEBUG', 'beforeLoad_CalculateWeek', 'arr[i]=' + arr);
            arr[i] = "GCI(INC) ID,Fusion ID,First Name,Last Name,WK1 ST,WK1 OT,WK1 DT,WK1 TIMEOFF,WK2 ST,WK2 OT,WK2 DT,WK2 TIMEOFF,OT Hours,Total Timeoff,Total DT,Float H(if in either wk),Project_City,Project Location,Employee_Type,Department,User Notes,Customer\n" + file + "," + employeeid + "," + firstname + "," + lastname + "," + wkst1 + "," + wkot1 + "," + wkdt1 + "," + wk1timeoff1 + "," + wkst2 + "," + wkot2 + "," + wkdt2 + "," + wk1timeoff2 + "," + totalothours + "," + totaltimeoff + ',' + total_dt_time + ',' + floatingh + ',' + projectcity + "," + projectloc + ',' + employeetype + ',' + department + "," + usernotes + "," + customer;
        }
        else {
            arr[i] = "\n" + file + "," + employeeid + "," + firstname + "," + lastname + "," + wkst1 + "," + wkot1 + "," + wkdt1 + "," + wk1timeoff1 + "," + wkst2 + "," + wkot2 + "," + wkdt2 + "," + wk1timeoff2 + "," + totalothours + "," + totaltimeoff + ',' + total_dt_time + ',' + floatingh + ',' + projectcity + "," + projectloc + ',' + employeetype + ',' + department + "," + usernotes + "," + customer;
        }
        //nlapiLogExecution('DEBUG', 'beforeLoad_CalculateWeek', 'arr[i]=' + arr[i]);
        
        globalArray[i - 1] = (arr[i]);
        //nlapiLogExecution('DEBUG', 'beforeLoad_CalculateWeek', 'globalArray######' + globalArray);
    }
    
    var fileName = 'Sal- NOn- Std  Rpt - From TS'
    var Datetime = new Date();
    var CSVName = fileName + " - " + Datetime + '.csv';
    var file = nlapiCreateFile(CSVName, 'CSV', globalArray.toString());
    
    //nlapiLogExecution('DEBUG', 'suitlet_Po_ClosedStatus', ' PDF Name-->  ');
    response.setContentType('CSV', CSVName);
    
    //nlapiLogExecution('DEBUG', 'suitlet_Po_ClosedStatus', ' PDF Name-->  ');
    response.write(file.getValue());
    
}

function setDTvalue(sublist1, currentLinenumber, weeklyDTTotal, s_id, startdateweek, currentdateweek, s_ProjectLocation){
    //nlapiLogExecution('DEBUG', 'SearchData', 'weeklyDTTotal===' + weeklyDTTotal)
    
    if (weeklyDTTotal != null && weeklyDTTotal != '' && weeklyDTTotal != 'undefined') {
        for (var i = 0; i < weeklyDTTotal.length; i++) {
            var split = weeklyDTTotal[i].toString().split("##")
            //nlapiLogExecution('DEBUG', 'SearchData', 'split===' + split)
            var i_DTTime = split[0]
            //nlapiLogExecution('DEBUG', 'SearchData', 'i_DTTime===' + i_DTTime)
            var i_employeeid = split[1]
            //nlapiLogExecution('DEBUG', 'SearchData', 'i_employeeid===' + i_employeeid)
            var i_weekno = split[2]
            //nlapiLogExecution('DEBUG', 'SearchData', 'i_weekno===' + i_weekno)
            var i_othours = split[3]
            //nlapiLogExecution('DEBUG', 'SearchData', 'i_othours===' + i_othours)
            //nlapiLogExecution('DEBUG', 'SearchData', 's_id===' + s_id)
            if (i_employeeid == s_id) {
                if (startdateweek == i_weekno) {
                    var week1OT = sublist1.getLineItemValue('custevent_wkot1', currentLinenumber);
                    //nlapiLogExecution('DEBUG', 'SearchData', 'week1OT===' + week1OT)
                    
                    if (s_ProjectLocation == "California") {
                        sublist1.setLineItemValue('custevent_wkdt1', currentLinenumber, parseFloat(i_DTTime));
                    }
                    else {
                        //i_othours = parseFloat(i_othours) + parseFloat(4)
                    }
                    
                    //week1OT = parseFloat(week1OT) - parseFloat(sublist1.getLineItemValue('custevent_wkdt1', currentLinenumber))
                    sublist1.setLineItemValue('custevent_wkot1', currentLinenumber, parseFloat(i_othours));
                    //nlapiLogExecution('DEBUG', 'SearchData', 'week1OT===' + week1OT)
                    //nlapiRemoveLineItem('record',1)
                }
                else {
                    var week2OT = sublist1.getLineItemValue('custevent_wkot2', currentLinenumber);
                    //nlapiLogExecution('DEBUG', 'SearchData', 'week2OT===' + week2OT)
                    if (s_ProjectLocation == "California") {
                        sublist1.setLineItemValue('custevent_wkdt2', currentLinenumber, parseFloat(i_DTTime));
                        
                    }
                    else {
                        //i_othours = parseFloat(i_othours) + parseFloat(4)
                    }
                    //week2OT = parseFloat(week2OT) - parseFloat(sublist1.getLineItemValue('custevent_wkdt2', currentLinenumber))
                    //nlapiLogExecution('DEBUG', 'SearchData', 'week2OT===' + week2OT)
                    sublist1.setLineItemValue('custevent_wkot2', currentLinenumber, parseFloat(i_othours));
                    //nlapiLogExecution('DEBUG', 'SearchData', 'week2OT===' + week2OT)
                
                }
            }
        }
    }
    
    return true;
}

function getDTTotalsweekly(sublist1, searchData, s_afterdate1){

    /////////////get stardateweekno
    s_afterdate1 = nlapiStringToDate(s_afterdate1);
    //************** pass the date as parameter to getWeekNumber function *****************//
    var startdateweek = getWeekNumber(s_afterdate1)
    //nlapiLogExecution('DEBUG', 'beforeLoad_CalculateWeek', 'startdateweek=' + startdateweek);
    //////////////////////////////////////
    
    var othoursarray = new Array();
    var a_week1_array = new Array();
    var a_weekarray = new Array();
    var finalarray = new Array();
    if (searchData.length != '' && searchData.length != null && searchData.length != 'undefined') {
        /////////Loop through rows of search/////////////////////////////////////////////////////
        for (var x = 0; x < searchData.length; x++) {
            var a_search_transaction_result1 = searchData[x];
            var columns1 = a_search_transaction_result1.getAllColumns();
            
            var columnLen1 = columns1.length;
            
            var i_Calendar_Week = ''
            var i_Duration_decimal = 0;
            var i_Employee = '';
            var s_location = ''
            for (var hg1 = 0; hg1 < columnLen1; hg1++) {
            
            
                var column1 = columns1[hg1];
                //nlapiLogExecution('DEBUG', 'search_ComplaintCategory', 'column  -->' + column);
                var label1 = column1.getLabel();
                //nlapiLogExecution('DEBUG', 'search_ComplaintCategory', '*************************label********************  -->' + label1);
                var grpfieldName = column1.getName();
                //nlapiLogExecution('DEBUG', 'search_ComplaintCategory', '*************************grpfieldName********************  -->' + grpfieldName);
                
                var value = a_search_transaction_result1.getValue(column1)
                var text = a_search_transaction_result1.getText(column1)
                
                //nlapiLogExecution('DEBUG', 'search_ComplaintCategory', '*************************value********************  -->' + value);
                //nlapiLogExecution('DEBUG', 'search_ComplaintCategory', '*************************text********************  -->' + text);
                
                //nlapiLogExecution('DEBUG', 'search_ComplaintCategory', 'value  -->' + value);
                if (grpfieldName == 'date') {
                    var i_Calendar_Week = value;
                }
                if (grpfieldName == 'durationdecimal') {
                    var i_Duration = value;
                }
                if (grpfieldName == 'custentity_worklocation') {
                    var s_location = text;
                }
                if (grpfieldName == 'internalid') {
                    var i_Employee = value
                }
            }
            
            i_Duration_decimal = parseFloat(i_Duration_decimal)
            var i_Calendar_Week1 = nlapiStringToDate(i_Calendar_Week);
            var currentdateweek = getWeekNumber(i_Calendar_Week1)
            var startdateweek = getWeekNumber(s_afterdate1)
            
            var day = i_Calendar_Week1.getDay();
            //nlapiLogExecution('DEBUG', 'beforeLoad_CalculateWeek', 'day=' + day);
            if(a_weekarray.indexOf(i_Employee + "##" + currentdateweek + '##' + day) == -1)
            	{
            		a_weekarray.push(i_Employee + "##" + currentdateweek + '##' + day)
            	}
            a_week1_array.push(i_Employee + "##" + i_Duration + "##" + currentdateweek + "##" + s_location + '##' + day)
            
            //a_weekarray.push(i_Employee + "##" + currentdateweek)
            //a_week1_array.push(i_Employee + "##" + i_Duration + "##" + currentdateweek + "##" + s_location)
        
        }
        var uniqueweekArray = a_weekarray;//removearrayduplicate(a_weekarray)
        
        for (var a = 0; a < uniqueweekArray.length; a++) //
        {
            var i_totalTime = 0;
            var i_totalTime_1 = 0;
            var othhours = 0
            var i_split = uniqueweekArray[a].toString().split("##")
            //nlapiLogExecution('DEBUG', 'beforeLoad_CalculateWeek', 'i_split=' + i_split);
            
            var i_empid1 = i_split[0]
            //nlapiLogExecution('DEBUG', 'beforeLoad_CalculateWeek', 'i_empid1=' + i_empid1);
            
            var i_weekno1 = i_split[1]
            //nlapiLogExecution('DEBUG', 'beforeLoad_CalculateWeek', 'i_weekno1=' + i_weekno1);
            
            for (var b = 0; b < a_week1_array.length; b++) //
            {
                var i_split_1 = a_week1_array[b].toString().split("##")
                //nlapiLogExecution('DEBUG', 'beforeLoad_CalculateWeek', 'i_split_1=' + i_split_1);
                
                var i_empid2 = i_split_1[0]
                
                
                var i_hours = i_split_1[1]
                //nlapiLogExecution('DEBUG', 'beforeLoad_CalculateWeek', 'i_hours=' + i_hours);
                
                var i_weekno2 = i_split_1[2]
                //nlapiLogExecution('DEBUG', 'beforeLoad_CalculateWeek', 'i_weekno2=' + i_weekno2);
                var i_location = i_split_1[3]
                //nlapiLogExecution('DEBUG', 'beforeLoad_CalculateWeek', 'i_location=' + i_location);
                
                var i_day = i_split_1[4]
                //nlapiLogExecution('DEBUG', 'beforeLoad_CalculateWeek', 'i_day = ' + i_day);
                
                if (i_empid1 == i_empid2 && i_weekno1 == i_weekno2) //
                {
                    var ot_limit = 4;
                    if (i_day == 6 || i_day == 0) //if day is Sunday or Saturday....
                    {
                        ot_limit = 8;
                    }
                    
                    if (i_hours > ot_limit) //
                    {
                        i_totalTime = parseFloat(i_hours) - parseFloat(ot_limit)
                        if (i_location == 'California') //
                        {
                            othhours = parseFloat(othhours) + parseFloat(ot_limit)
                        }
                        else //
                        {
                            othhours = parseFloat(othhours) + parseFloat(i_hours)
                        }
                        
                        i_totalTime_1 = parseFloat(i_totalTime_1) + parseFloat(i_totalTime)
                    }
                    else //
                    {
                        othhours = parseFloat(othhours) + parseFloat(i_hours)
                    }
                    
                }
            }
            if (i_totalTime_1 != null && i_totalTime_1 != 'undefined' && i_totalTime_1 != '') //
            {
                i_totalTime_1 = i_totalTime_1
            }
            else //
            {
                i_totalTime_1 = 0
            }
            
            finalarray.push(i_totalTime_1 + "##" + i_empid1 + "##" + i_weekno1 + "##" + othhours)
        }
        //nlapiLogExecution('DEBUG', 'beforeLoad_CalculateWeek', 'finalarray=' + finalarray);
    }
    
    return finalarray;
}

function _SearchDTData(s_beforedate, s_afterdate) //
{
    /*
     //nlapiLogExecution('DEBUG', 'SearchDTData', 'In Search Data')
     //nlapiLogExecution('DEBUG', 'SearchDTData', 's_beforedate===' + s_beforedate)
     //nlapiLogExecution('DEBUG', 'SearchDTData', 's_afterdate===' + s_afterdate)
     */
    var a_filters = new Array();
    //a_filters[0] = new nlobjSearchFilter('date', null, 'on', s_beforedate);
    a_filters[0] = new nlobjSearchFilter('date', null, 'onorbefore', s_beforedate);
    a_filters[1] = new nlobjSearchFilter('date', null, 'onorafter', s_afterdate);
    
    var a_columns = new Array();
    a_columns[0] = new nlobjSearchColumn('employee');
    a_columns[1] = new nlobjSearchColumn('item');
    a_columns[2] = new nlobjSearchColumn('date');
    a_columns[3] = new nlobjSearchColumn('hours');
    //a_columns[4] = new nlobjSearchColumn('custentity_worklocation',);
    
    var searchresult = nlapiSearchRecord('timebill', 'customsearchsalnonstdrptts_2', a_filters, null);
    return searchresult
}

function SearchDTData(s_beforedate, s_afterdate) //
{
    var LastfetchedInternalid_T = 0;
    var searchresultFinal_T = new Array();
    var searchresult = new Array();
    
    do //
    {
        var a_filters = new Array();
        a_filters[0] = new nlobjSearchFilter('date', null, 'onorbefore', s_beforedate);
        a_filters[1] = new nlobjSearchFilter('date', null, 'onorafter', s_afterdate);
        a_filters[2] = new nlobjSearchFilter('internalidnumber', 'employee', 'greaterthan', LastfetchedInternalid_T);////is not before 1sept
        var a_columns = new Array();
        a_columns[0] = new nlobjSearchColumn('employee');
        a_columns[1] = new nlobjSearchColumn('item');
        a_columns[2] = new nlobjSearchColumn('date');
        a_columns[3] = new nlobjSearchColumn('hours');
        
        var searchresult = nlapiSearchRecord('timebill', 'customsearchsalnonstdrptts_2', a_filters, null);
        //nlapiLogExecution('DEBUG', 'SearchDTData', 'searchresult===' + searchresult.length)
        
        if (searchresult != null && searchresult != undefined && searchresult != '') //
        {
            //nlapiLogExecution('DEBUG', '_SearchDTData', 'searchresult == ' + searchresult.length);
            var temp_emp = '';
            var emp = '';
            
            for (var i = 0; i < searchresult.length; i++) //
            {
                var a_search_empdata = searchresult[i];
                var all_columns = a_search_empdata.getAllColumns();
                emp = a_search_empdata.getValue(all_columns[13]);
                //nlapiLogExecution('DEBUG', 'SearchData', 'i : ' + i + ' emp ==' + emp);
                if (emp != null) //
                {
                    if (i == 0) //
                    {
                        temp_emp = emp;
                    }
                    
                    LastfetchedInternalid_T = temp_emp;
                    //nlapiLogExecution('DEBUG', 'SearchData', 'i : ' + i + ' temp_emp ==' + temp_emp);
                   
				    if (emp == temp_emp) //if employee id is same then add to final array
                    {
                        searchresultFinal_T.push(searchresult[i]);
                    }
                    else //else employee id does not match
                    {
                        //nlapiLogExecution('DEBUG', 'SearchData', 'Emp change row number : ' + i);
                        if (i > 950)//
                        {
                            //nlapiLogExecution('DEBUG', 'SearchData', 'LastfetchedInternalid_T==' + LastfetchedInternalid_T);
                            break;
                        }
                        else //
                        {
                            searchresultFinal_T.push(searchresult[i]);
                            temp_emp = emp;
							LastfetchedInternalid_T = temp_emp;
                        }
                    }
                }
                
                //searchresultFinal_T.push(searchresult[i]);
                //nlapiLogExecution('DEBUG','researchcodes','lastid=='+a_search_empdata.getValue('internalid',null,'group'));
            }
            //nlapiLogExecution('DEBUG','researchcodes','lastid=='+a_search_empdata.getValue('internalid',null,'group'));
            //LastfetchedInternalid_T = searchresult[searchresult.length - 1].getValue('internalid', 'employee', null);
            //nlapiLogExecution('DEBUG', '_SearchDTData', 'LastfetchedInternalid_T==' + LastfetchedInternalid_T);
        }
    }
    while (searchresult != null && searchresult != undefined && searchresult != '')
    {
    
    }
    return searchresultFinal_T;
    
}

function removearrayduplicate(array){
    var newArray = new Array();
    label: for (var i = 0; i < array.length; i++) {
        for (var j = 0; j < array.length; j++) {
            if (newArray[j] == array[i]) 
                continue label;
        }
        newArray[newArray.length] = array[i];
    }
    return newArray;
}

function searchexcludeValues(s_id, searchData, startdateweek, currentdateweek, s_beforedate, c, f_week1_hours, f_week2_hours){
    var a_excludeArray = new Array();
    var s_OTTask = "OT"
    var a_STTask = ["Project Activities".toUpperCase(), "Bench Task".toUpperCase()]
    var s_HolidayTask = "Holiday"
    var s_FHTask = "Floating Holiday"
    var s_LeaveTask = "Leave"
    s_OTTask = s_OTTask.toString().toUpperCase();
    //s_STTask = s_STTask.toString().toUpperCase();
    s_HolidayTask = s_HolidayTask.toString().toUpperCase();
    s_FHTask = s_FHTask.toString().toUpperCase();
    s_LeaveTask = s_LeaveTask.toString().toUpperCase();
    
    var isEmployeeFound = false;
    
    var b_entryFlag = true;
    var i_Wk1OtHrs = 0
    var i_Wk2OtHrs = 0
    var i_Wk1STHrs = 0
    var i_Wk2STHrs = 0
    var i_Wk1HLHrs = 0
    var i_Wk2HLHrs = 0
    var i_Wk1FLHrs = 0;
    var i_Wk2FLHrs = 0;
    
    //	Check if timesheet is filled
    var isWk1TimesheetFilled	=	false;
    var isWk2TimesheetFilled	=	false;
    
    for (var d = c; d < searchData.length; d++) {
        var i_Calendar_Week = ''
        var i_Employee = ''
        var i_Duration = ''
        var i_Project_Task_Name = '';
        var i_Duration_decimal = 0;
        var i_empid = ''
        
        var i_Employee_First_Name = '';
        var i_Employee_Last_Name = '';
        var i_Employee_Inc_Id = '';
        var Employee_Type = ''
        var Employment_Category = ''
        var i_note = ''
        var i_Dayofweek = '';
        
        var a_search_transaction_result = searchData[d];
        //nlapiLogExecution('DEBUG', 'search_ComplaintCategory', 'a_search_transaction_result  -->' + a_search_transaction_result);
        //nlapiLogExecution('DEBUG', 'search_ComplaintCategory', 'c  -->' + c);
        var columns = a_search_transaction_result.getAllColumns();
        //nlapiLogExecution('DEBUG', 'search_ComplaintCategory', 'columns  -->' + columns);
        
        var columnLen = columns.length;
        //nlapiLogExecution('DEBUG', 'search_ComplaintCategory', 'columnLen  -->' + columnLen);
        
        
        for (var hg1 = 0; hg1 < columnLen; hg1++) {
        
            var column = columns[hg1];
            //nlapiLogExecution('DEBUG', 'search_ComplaintCategory', 'column  -->' + column);
            var label = column.getLabel();
            //nlapiLogExecution('DEBUG', 'search_ComplaintCategory', '*************************label********************  -->' + label);
            
            var value = a_search_transaction_result.getValue(column)
            var text = a_search_transaction_result.getText(column)
            //nlapiLogExecution('DEBUG', 'search_ComplaintCategory', 'value  -->' + value);
            if (label == 'Calendar Week') {
                i_Calendar_Week = value
            }
            if (label == 'Employee') {
                i_Employee = value;
            }
            if (label == 'Duration') {
                i_Duration = value;
            }
            if (label == 'Project Task : Name') {
                i_Project_Task_Name = value;
            }
            if (label == 'Duration:Decimal') {
                i_Duration_decimal = parseFloat(value);
            }
            if (label == 'Employee : First Name') {
                i_Employee_First_Name = value;
            }
            if (label == 'Employee : Last Name') {
                i_Employee_Last_Name = value;
            }
            if (label == 'Employee : Inc Id') {
                i_Employee_Inc_Id = value;
            }
            if (label == 'Employee : Employment Category') {
                Employment_Category = value;
            }
            if (label == 'Note') {
                i_note = value
            }
            if (label == 'User_Notes') {
                i_note = value
            }
            if (label == 'Department') {
                s_Department = text;
            }
            if (label == 'Emp ID') {
                s_Employeefile = value;
            }
            if (label == 'Project Location') {
                s_ProjectLocation = text;
                
            }
            if (label == 'Project City') {
                s_ProjectCity = text;
            }
            
            if (label == 'EmpIntID') {
                i_empid = value;
            }
            if (label == 'Dayofweek') {/////Added for sunday hrs
                i_Dayofweek = value
            }
        }
        if (i_Calendar_Week != s_beforedate) {
            i_Calendar_Week = nlapiStringToDate(i_Calendar_Week);
            i_Calendar_Week = nlapiAddDays(i_Calendar_Week, 1)
        }
        //************** pass the date as parameter to getWeekNumber function *****************//
        var currentdateweek = getWeekNumber(i_Calendar_Week)
        
        ////**************Calculate the week of the Dayofweek column to get if there is any sunday filled time sheets with not matching weekno.
        /////Added for sunday hrs/////////////////////////////////////////////////
        var i_Calendar_Week_Dayofweekdate = nlapiStringToDate(i_Dayofweek);
        var Dayofweekno = getWeekNumber(i_Calendar_Week_Dayofweekdate);
        //nlapiLogExecution('DEBUG', 'SearchData', 'Dayofweekno===' + Dayofweekno)
        /////////////////////////////new code added to consider sunday also in same week missed in search/////////////////////////////////////
        
        //nlapiLogExecution('DEBUG', 'SearchData', 'i_Calendar_Week===' + i_Calendar_Week)
        //nlapiLogExecution('DEBUG', 'SearchData', 'startdateweek===' + startdateweek)
        //nlapiLogExecution('DEBUG', 'SearchData', 'currentdateweek===' + currentdateweek)
        //nlapiLogExecution('DEBUG', 'SearchData', 'i_Project_Task_Name===' + i_Project_Task_Name)
        //nlapiLogExecution('DEBUG', 'SearchData', 'i_empid===' + i_empid)
        i_Project_Task_Name = i_Project_Task_Name.toString().toUpperCase();
        if (s_id == i_empid) {
        	isEmployeeFound = true;
        	
        	if (startdateweek == currentdateweek || Dayofweekno == startdateweek) //1st week Added for sunday hrs
            {
                //i_Wk1OtHrs = i_Duration_decimal
                isWk1TimesheetFilled	=	true;
            }
            else 
                if ((currentdateweek == startdateweek + 1) || (Dayofweekno == startdateweek + 1)) {
                    //i_Wk2OtHrs = i_Duration_decimal
                    isWk2TimesheetFilled	=	true;
                }
        	
            //nlapiLogExecution('DEBUG', 'SearchData', 's_id===' + s_id)
            if (i_Project_Task_Name == s_OTTask) {
                //nlapiLogExecution('DEBUG', 'SearchData', 'i_Project_Task_Name===' + i_Project_Task_Name)
                //nlapiLogExecution('DEBUG', 'SearchData', 'i_Duration_decimal===' + i_Duration_decimal)
                if (startdateweek == currentdateweek || Dayofweekno == startdateweek) //1st week Added for sunday hrs
                {
                    //i_Wk1OtHrs = i_Duration_decimal
                    i_Wk1OtHrs = parseFloat(i_Wk1OtHrs) + parseFloat(i_Duration_decimal)
                }
                else 
                    if ((currentdateweek == startdateweek + 1) || (Dayofweekno == startdateweek + 1)) {
                        //i_Wk2OtHrs = i_Duration_decimal
                        i_Wk2OtHrs = parseFloat(i_Wk2OtHrs) + parseFloat(i_Duration_decimal)
                    }
            }
            else 
                if (a_STTask.indexOf(i_Project_Task_Name) != -1) {
                    //nlapiLogExecution('DEBUG', 'SearchData', 'i_Project_Task_Name===' + i_Project_Task_Name)
                    //nlapiLogExecution('DEBUG', 'SearchData', 'i_Duration_decimal===' + i_Duration_decimal)
                    if (startdateweek == currentdateweek || Dayofweekno == startdateweek) //1st week Added for sunday hrs
                    {
                        i_Wk1STHrs = parseFloat(i_Wk1STHrs) + parseFloat(i_Duration_decimal)
                    //nlapiLogExecution('DEBUG', 'SearchData', 'i_Wk1STHrs===' + i_Wk1STHrs)
                    }
                    else 
                        if ((currentdateweek == startdateweek + 1) || (Dayofweekno == startdateweek + 1)) {
                            i_Wk2STHrs = parseFloat(i_Wk2STHrs) + parseFloat(i_Duration_decimal)
                        //nlapiLogExecution('DEBUG', 'SearchData', 'i_Wk2STHrs===' + i_Wk2STHrs)
                        }
                }
                else {
                    if (i_Project_Task_Name == s_HolidayTask) {
                        //nlapiLogExecution('DEBUG', 'SearchData', 'i_Project_Task_Name===' + i_Project_Task_Name)
                        
                        if (startdateweek == currentdateweek || Dayofweekno == startdateweek) //1st week Added for sunday hrs
                        {
                            i_Wk1HLHrs = parseFloat(i_Wk1HLHrs) + parseFloat(i_Duration_decimal)
                        //i_Wk1HLHrs = i_Duration_decimal
                        }
                        else 
                            if ((currentdateweek == startdateweek + 1) || (Dayofweekno == startdateweek + 1)) {
                                i_Wk2HLHrs = parseFloat(i_Wk2HLHrs) + parseFloat(i_Duration_decimal)
                            //i_Wk2HLHrs = i_Duration_decimal
                            }
                    //	//nlapiLogExecution('DEBUG', 'SearchData', 'i_Wk1HLHrs===' + i_Wk1HLHrs)
                    //	//nlapiLogExecution('DEBUG', 'SearchData', 'i_Wk1STHrs===' + i_Wk1STHrs)
                    
                    }
                    else 
                        if (i_Project_Task_Name == s_FHTask) {
                            if (startdateweek == currentdateweek || Dayofweekno == startdateweek) //1st week Added for sunday hrs
                            {
                                //i_Wk1FLHrs = i_Duration_decimal
                                i_Wk1FLHrs = parseFloat(i_Wk1FLHrs) + parseFloat(i_Duration_decimal)
                            }
                            else 
                                if ((currentdateweek == startdateweek + 1) || (Dayofweekno == startdateweek + 1)) {
                                    i_Wk2FLHrs = parseFloat(i_Wk2FLHrs) + parseFloat(i_Duration_decimal)
                                //i_Wk2FLHrs = i_Duration_decimal
                                }
                        }
                }
            
        }
        else if(isEmployeeFound == true)
        	{
        		break;
        	}
        	
    }
    
    
        //nlapiLogExecution('DEBUG', 'SearchData', 's_id===' + s_id)
        
        //nlapiLogExecution('DEBUG', 'SearchData', 'i_Wk2STHrs===' + i_Wk2STHrs)
        //nlapiLogExecution('DEBUG', 'SearchData', 'i_Wk1STHrs===' + i_Wk1STHrs)
        
        //nlapiLogExecution('DEBUG', 'SearchData', 'i_Wk1OtHrs===' + i_Wk1OtHrs + "i_Wk1STHrs====" + i_Wk1STHrs + "i_Wk1HLHrs===" + i_Wk1HLHrs)
        //nlapiLogExecution('DEBUG', 'SearchData', 'i_Wk1STHrs+i_Wk1HLHrs===' + (parseFloat(i_Wk1STHrs)+parseFloat(i_Wk1HLHrs)))
        if (i_Wk1OtHrs == 0 && (i_Wk1STHrs == f_week1_hours || (parseFloat(i_Wk1STHrs) + parseFloat(i_Wk1HLHrs)) == f_week1_hours || isWk1TimesheetFilled == false)) {
            if (i_Wk1FLHrs == 0) {
                b_entryFlag = false;
            }
            else {
                b_entryFlag = true;
            }
            
        }
        else {
            b_entryFlag = true;
        }
        if (b_entryFlag != true) {
            //nlapiLogExecution('DEBUG', 'SearchData', 'i_Wk2OtHrs===' + i_Wk2OtHrs + "i_Wk2STHrs====" + i_Wk2STHrs + "i_Wk2HLHrs===" + i_Wk2HLHrs)
            if (i_Wk2OtHrs == 0 && (i_Wk2STHrs == f_week2_hours || (parseFloat(i_Wk2STHrs) + parseFloat(i_Wk2HLHrs)) == f_week2_hours || isWk2TimesheetFilled == false)) {
                if (i_Wk1FLHrs == 0) {
                    b_entryFlag = false;
                }
                else {
                    b_entryFlag = true;
                }
                
            }
            else {
                b_entryFlag = true;
            }
        }
        //nlapiLogExecution('DEBUG', 'SearchData', 's_id===' + s_id)
        //nlapiLogExecution('DEBUG', 'SearchData', 'empid===' + i_empid)
        //nlapiLogExecution('DEBUG', 'SearchData', 'b_entryFlag===' + b_entryFlag)
        return b_entryFlag;
    
    //nlapiLogExecution('DEBUG', 'SearchData', 'a_excludeArray===' + a_excludeArray)
}
