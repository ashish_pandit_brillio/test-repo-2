// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
/*
   	Script SCH_SendEmailSavedNotSubmittedProject.js
	Author: Ashish Pandit	
	Date:24-April-2018	
    Description: Scheduled script to send Email to PM for Project confirmation for Not Submitted Projects  


	Script Modification Log:

	-- Date --			-- Modified By --				--Requested By--				-- Description --
	
	Below is a summary of the process controls enforced by this script file.  The control logic is described
	more fully, below, in the appropriate function headers and code blocks.

   
     SUB-FUNCTIONS
		- The following sub-functions are called by the above core functions in order to maintain code
            modularization:

		scheduled_SendEmailForApproval()



*/
}
// END SCRIPT DESCRIPTION BLOCK  ====================================


function scheduled_SendEmailNotSubmittedProject(type) {
	//Getting freeze date
	var o_freeze_date = nlapiLoadRecord('customrecord_fp_rev_rec_freeze_date',1);
		if(o_freeze_date)
		{
			var s_freeze_date = o_freeze_date.getFieldValue('custrecord_freeze_date');
			var i_freeze_day = nlapiStringToDate(s_freeze_date).getDate();
			var i_freeze_month = nlapiStringToDate(s_freeze_date).getMonth();
			var d_today_date = new Date();
			var i_today_date = d_today_date.getDate();
			var i_today_month = d_today_date.getMonth();
			var i_today_year = d_today_date.getFullYear();
			
			if(parseInt(i_freeze_month) != parseInt(i_today_month))
				i_freeze_day = 25;
			
			i_today_month = parseInt(i_today_month) + parseInt(1);
			
			nlapiLogExecution('audit','i_freeze_day:- '+i_freeze_day,'i_today_date:- '+i_today_date);
			if(i_today_date < 20)
			{
				if(i_today_date == 5 || i_today_date == 10 || i_today_date == 15)
				{
					
				}
				else
				{
					nlapiLogExecution('audit','freeze date trigger date yet to come');
					return;
				}	
			}
			
			if(i_today_date >= i_freeze_day)
			{
				nlapiLogExecution('audit','freeze date trigger crossed');
				return;
			}
		}
	var a_project_list = new Array();
	//Getting Current month 
	var d_today_date = new Date();
	//nlapiLogExecution('DEBUG', 'd_today_date', d_today_date);
	var i_month = d_today_date.getMonth();
	//nlapiLogExecution('DEBUG', 'i_month', i_month);
	// Search to get all Projects for FP Rev Rec Milestone 
	
	var a_project_search_results = nlapiSearchRecord("job",null,
			[
				["status", "noneof", "1"],
				"AND",
				["jobtype", "anyof", "2"],
				"AND",
				["jobbillingtype", "anyof", "FBM"],
				"AND",
				["enddate", "onorafter","startofthismonth"],
				"AND",
				["custentity_fp_rev_rec_type", "anyof", "1"],
             	 "AND",
				["subsidiary", "noneof", "9","8"]
				
			],
			[
			   new nlobjSearchColumn("internalid",null,null).setSort(false), 
			   new nlobjSearchColumn("entityid",null,null),
			   new nlobjSearchColumn("custentity_projectmanager",null,null),
			   new nlobjSearchColumn("custentity_deliverymanager",null,null),
			   new nlobjSearchColumn("custentity_clientpartner",null,null),
			   new nlobjSearchColumn("custentity_project_currency",null,null),
			   new nlobjSearchColumn("custentity_discount_project",null,null)
			]
			);
	if(_logValidation(a_project_search_results))
		{		
			var other = 0;
			var apprved = 0;
			for(var i_pro_index = 0; i_pro_index < a_project_search_results.length; i_pro_index++)
			{
				var i_project_internal_id = a_project_search_results[i_pro_index].getId();
				// Search to get Approved Projects 
				var customrecord_revenue_shareSearch = nlapiSearchRecord("customrecord_revenue_share",null,
				[
				   ["custrecord_revenue_share_approval_status","anyof","3"], 
				   "AND", 
				   ["custrecord_revenue_share_project","anyof",i_project_internal_id]
				], 
				[
				   new nlobjSearchColumn("id").setSort(false), 
				   new nlobjSearchColumn("custrecord_revenue_share_project"), 
				   new nlobjSearchColumn("custrecord_revenue_share_approval_status")
				]
				);
				
				if(_logValidation(customrecord_revenue_shareSearch))
				{
					nlapiLogExecution('DEBUG','Approved =',i_project_internal_id);
					apprved++;
				}
				else
				{
					other++;
				// Code to send emails which are Not Approved / Saved / Submit for review / submit for approval. 
				var i_project = a_project_search_results[i_pro_index].getValue('internalid');
				var s_projectName = a_project_search_results[i_pro_index].getValue('entityid');
				nlapiLogExecution('DEBUG','s_projectName==',s_projectName);
				if(_logValidation(i_project))
				{
				var b_discProject = a_project_search_results[i_pro_index].getValue('custentity_discount_project');
				nlapiLogExecution('Debug', 'b_discProject ='+b_discProject,s_projectName);
				// To skip Discount projects
				if(b_discProject == 'T')
				{
					nlapiLogExecution('Debug','in break');	
					continue;
				}
				var i_projManager = a_project_search_results[i_pro_index].getValue('custentity_projectmanager');
				var s_projectManager = a_project_search_results[i_pro_index].getText('custentity_projectmanager');
				var i_deliveryManager = a_project_search_results[i_pro_index].getValue('custentity_deliverymanager');
				var i_clientPartner = a_project_search_results[i_pro_index].getValue('custentity_clientpartner');
				if(i_projManager)
					var i_projManagerEmail = nlapiLookupField('employee',i_projManager,'email');
				if(i_deliveryManager)
					var i_deliveryManagerEmail = nlapiLookupField('employee',i_deliveryManager,'email');
				if(i_clientPartner)
					var i_clientPartnerEmail = nlapiLookupField('employee',i_clientPartner,'email');
				var s_proj_currency = a_project_search_results[i_pro_index].getValue('custentity_project_currency');
				var d_today = new Date();	
				var getCurrentMonth = d_today.getMonth() + 1;
				var getCurrentYear = d_today.getFullYear();
				var s_month_name = getMonthName(nlapiDateToString(d_today));
				var s_emailSub = 'Monthly revenue confirmation for the Project '+ s_projectName +  ' Forecast Setup / Approval Reminder';
				s_projectManager = s_projectManager.split('-');
				//nlapiLogExecution('DEBUG','s_projectManager '+s_projectManager[1]);
				var s_emailBody = 'Dear '+s_projectManager[1] +'<br> Please create an effort / revenue plan for your project '+s_projectName+' and get it approved by the practice head. <br>Confirm the recognition plan for the month  '+s_month_name +'  '+ getCurrentYear +' before the freeze date '+ i_freeze_day +'  '+ s_month_name +'. <br> Should you fail to confirm before freeze date, system will not recognize the revenue for this month and same is true for future months unless you confirm the plan at least once.<br>';
				s_emailBody +=  '<br>Note: If there is any help required in setting this up or get it approved, please work with prashanth.s@brillio.com and sapan.shah@brillio.com immediately, before the freeze date.<br><br>Thanks & Regards,<br>Team IS'; 
				var recordarray = new Array();
				recordarray['entity'] = 94862; // Employee Ashish for testing 
				
				var a_participating_mail_id = new Array();
				if(i_deliveryManagerEmail)
					a_participating_mail_id.push(i_deliveryManagerEmail);
				if(i_clientPartnerEmail)	
					a_participating_mail_id.push(i_clientPartnerEmail);
				a_participating_mail_id.push('billing@brillio.com');
				a_participating_mail_id.push('team.fpa@brillio.com');
				
				//Sending Email to PM  
				nlapiSendEmail(442,i_projManagerEmail,s_emailSub,s_emailBody,a_participating_mail_id,['information.systems@brillio.com','deepak.srinivas@brillio.com'],null,null,false,false,null);
				
				//nlapiSendEmail(442,'ashish.p@inspirria.com',s_emailSub,s_emailBody,null,null,recordarray,null,false,false,null);
				
				var context = nlapiGetContext();
				if(context.getRemainingUsage() < 200)
					{
						var state = nlapiYieldScript();
					}
				}
				}			
			}
			nlapiLogExecution('Debug','Approved ='+apprved,'Other ='+other);
		}	
			
	}
	
	function getMonthName(currentDate) {
	currentDate = nlapiStringToDate(currentDate)
	var monthNames = [ "JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JUL", "AUG",
	        "SEP", "OCT", "NOV", "DEC" ];
	return monthNames[currentDate.getMonth()];
	}
function _logValidation(value) 
{
 if(value!='null' && value != null && value != null && value != '' && value != undefined && value != undefined && value != 'undefined' && value!= 'undefined'&& value!= 'NaN' && value != NaN) 
 {
  return true;
 }
 else 
 { 
  return false;
 }
}
 

function isEmpty(value) {

	return value == null || value == "" || typeof (value) == undefined;
}

function isNotEmpty(value) {

	return !isEmpty(value);
}

function isArrayEmpty(argArray) {

	return !isArrayNotEmpty(argArray);
}

function isArrayNotEmpty(argArray) {

	return (isNotEmpty(argArray) && argArray.length > 0);
}