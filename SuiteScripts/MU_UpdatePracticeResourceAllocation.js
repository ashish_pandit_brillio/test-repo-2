/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       28 Jan 2015     Amol
 *
 */

/**
 * @param {String} recType Record type internal id
 * @param {Number} recId Record internal id
 * @returns {Void}
 */
function massUpdate(recType, recId) {
	/*try{nlapiInitiateWorkflowAsync('resourceallocation',recId, 42);nlapiLogExecution('AUDIT', 'ID', recId);}catch(e){nlapiLogExecution('ERROR', 'Error: ' + recId, e.message);}return;var */rec = nlapiLoadRecord(recType, recId);
	var practice = rec.getFieldValue('custevent_practice');
	
	var resource = nlapiLoadRecord('employee', rec.getFieldValue('allocationresource'));
	
	var newPractice = resource.getFieldValue('department');// nlapiLookupField('employee', resource, 'department');
	
	if(practice != newPractice)
		{
			rec.setFieldValue('custevent_practice', newPractice);
			nlapiSubmitRecord(rec);//, doSourcing, ignoreMandatoryFields) Field(recType, recId, 'custevent_practice', newPractice);
		}
}
