/**
 * Screen for ADO to view all the allocations under him
 * 
 * Version    Date            Author           Remarks
 * 1.00       07 may 2021     Praveena madem
 *
 */

/**
 * @param {nlobjRequest}
 *            request Request object
 * @param {nlobjResponse}
 *            response Response object
 * @returns {Void} Any output is written via response object
 */
function suitelet(request, response) {
	try {
		var method = request.getMethod();
		var mode = request.getParameter('mode');

		if (method == 'GET') {

			if (mode == 'excel') {
				exportFormToExcel(request);
			} else {
				createGetForm(request);
			}
		} else {
			submitForm(request);
		}
	} catch (err) {
		nlapiLogExecution('error', 'suitelet', err);
		displayErrorForm(err, "Resource Allocation");
	}
}

function exportFormToExcel(request) {
	try {
		var user = nlapiGetUser();

		if (user == 9673 || user==203752 || user==200580 ) {
			 //user = 3165;
		}

		var filterProject = request.getParameter('project');
		var filterCustomer = request.getParameter('customer');
		var filterStartDate = request.getParameter('startdate');
		var filterEndDate = request.getParameter('enddate');
		var filterStatus = request.getParameter('status');
		var isDetailedView = '';

		nlapiLogExecution('debug', 'filterProject', filterProject);
		nlapiLogExecution('debug', 'filterCustomer', filterCustomer);
		nlapiLogExecution('debug', 'filterStartDate', filterStartDate);
		nlapiLogExecution('debug', 'filterEndDate', filterEndDate);

		// var projectId = request.getParameter('pj');
		var project_list = getRelatedProjects(user, isDetailedView,
		        filterProject, filterCustomer, filterStatus);

		nlapiLogExecution('debug', 'project_list.DropDownList.length',
		        project_list.DropDownList.length);

		// var excelText = "";
		var excelText= getHeaderInfo("<head>");
		

		var context = nlapiGetContext();


		for (var k = 0; k < project_list.DropDownList.length; k++) {
			var project = project_list.DropDownList[k];
			nlapiLogExecution('debug', 'context.getRemainingUsage()', context
			        .getRemainingUsage() + ', '+ project);

					excelText += getProjectCsvDetails(project, filterCustomer,
			        filterStartDate, filterEndDate);

			if (context.getRemainingUsage() < 50) {
				break;
			}
		}
      
      excelText += "</table>";
	  excelText += "</body>";

		var file = nlapiCreateFile('Allocation Details.xls', 'EXCEL', nlapiEncrypt(excelText, 'base64'));
		response.setContentType('EXCEL', 'Allocation Details.xls');
		response.write(file.getValue());
	} catch (err) {
		nlapiLogExecution('error', 'exportFormToExcel', err);
		throw err;
	}
}

function getProjectCsvDetails(projectDetails, customer, startDate, endDate) {
	
	var projectId = projectDetails.Id;
    nlapiLogExecution('debug', 'projectId', projectId);
	var allocation_details = getProjectAllocationDetails(projectId, customer,
	        startDate, endDate);
  if(projectId == 6957){
    nlapiLogExecution('debug', 'Allocation details - stringify', JSON.stringify(allocation_details.PresentAllocation));
  }


 	var billingType = projectDetails.BillingType;
	var isMonthly = projectDetails.IsMonthly == 'T' ? " Monthly" : "";
	var projectName = projectDetails.EntityId + ":" + projectDetails.Name;
  
    //var startDate = nlapiStringToDate(projectDetails.StartDate);
    //var endDate = nlapiStringToDate(projectDetails.EndDate)nlapiDateToString

	

	// var allocationStr = getAllocationCsv(allocation_details.PresentAllocation,
	//         "Allocations", projectName);
	
	

	var strName =  "<tr>";

			    strName += "<td width='30px' style='align:left;' font-size='10px'>"+projectDetails.Customer+"</td>";
			    strName += "<td width='30px' style='align:left;' font-size='10px'>"+projectDetails.EntityId+"</td>"; 
			    strName += "<td width='30px' style='align:left;' font-size='10px'>"+projectDetails.Name+"</td>"; 
			    strName += "<td width='30px' style='align:left;' font-size='10px'>"+billingType + isMonthly+"</td>"; 
			    strName += "<td width='30px' style='align:left;' font-size='10px'>"+dateString(projectDetails.StartDate)+"</td>";
			    
			    strName += "<td width='30px' style='align:left;' font-size='10px'>"+dateString(projectDetails.EndDate)+"</td>"; 
			    strName += "<td width='30px' style='align:left;' font-size='10px'>"+projectDetails.ProjectManager+"</td>"; 
			    strName += "<td width='30px' style='align:left;' font-size='10px'>"+projectDetails.DeliveryManager+"</td>"; 
				strName += "<td width='30px' style='align:left;' font-size='10px'>"+projectDetails.ClientPartner+"</td>";
				
				var allocationStr = getAllocationCsv(allocation_details.PresentAllocation, projectName,strName);

					
                
                return allocationStr;
}

function getAllocationCsv(allocationList, projectName,strName) {
	
var lineCount = allocationList.length;
	var allocStr='';

	for (var linenum = 0; linenum < lineCount; linenum++) {
		allocStr+=strName
      
 
		
		//allocStr +="<tr>"
		allocStr += "<td width='30px' style='align:left;' font-size='10px'>"+allocationList[linenum].resourcename+"</td>";
		allocStr += "<td width='30px' style='align:left;' font-size='10px'>"+allocationList[linenum].practice+"</td>";
		
		allocStr += "<td width='30px' style='align:left;' font-size='10px'>"+allocationList[linenum].sub_practice+"</td>";
		allocStr += "<td width='30px' style='align:left;' font-size='10px'>"+allocationList[linenum].timeapprover_text+"</td>"; 
		allocStr += "<td width='30px' style='align:left;' font-size='10px'>"+allocationList[linenum].expenseapprover_text+"</td>"; 
		allocStr += "<td width='30px' style='align:left;' font-size='10px'>"+dateString(allocationList[linenum].startdate)+"</td>"; 
		allocStr += "<td width='30px' style='align:left;' font-size='10px'>"+dateString(allocationList[linenum].enddate)+"</td>"; 
		allocStr += "<td width='30px' style='align:left;' font-size='10px'>"+(allocationList[linenum].isbillable == 'T' ? 'Yes' : 'No')+"</td>";
		
		allocStr += "<td width='30px' style='align:left;' font-size='10px'>"+allocationList[linenum].billrate+"</td>"; 
		allocStr += "<td width='30px' style='align:left;' font-size='10px'>"+allocationList[linenum].percent+"</td>"; 
		allocStr += "<td width='30px' style='align:left;' font-size='10px'>"+dateString(allocationList[linenum].billingstartdate)+"</td>"; 
		allocStr += "<td width='30px' style='align:left;' font-size='10px'>"+dateString(allocationList[linenum].billingenddate)+"</td>";
		allocStr += "<td width='30px' style='align:left;' font-size='10px'>"+allocationList[linenum].site_text+"</td>";
		allocStr += "<td width='30px' style='align:left;' font-size='10px'>"+allocationList[linenum].location_text+"</td>";
		//allocStr +="</tr>"


	}

	return allocStr;
}

function createGetForm(request) {
	try {
		var filterProject = request.getParameter('project');
		var filterCustomer = request.getParameter('customer');
		var filterStartDate = request.getParameter('startdate');
		var filterEndDate = request.getParameter('enddate');
		var filterStatus = request.getParameter('status');
		
		if (!filterStatus) {
			filterStatus = 2; // Default to active
		}

		var isDetailedView = "";

		var user = nlapiGetUser();

		if (user == 9673 || user==203752 || user==200580 ) {
			//user = 3165;
		}

		var project_list = [];

		nlapiLogExecution('debug', 'start', 'A');

		project_list = getRelatedProjects(user, isDetailedView, filterProject,
		        filterCustomer, filterStatus);

		nlapiLogExecution('debug', 'start', 'B');

		var form = nlapiCreateForm('Resource Allocation');

		if (getRelatedProjects.length > 0) {
			var customerField = form.addField('custpage_filter_customer',
			        'select', 'Customer');

			if (filterCustomer) {
				customerField.addSelectOption('', '', false);
			} else {
				customerField.addSelectOption('', '', true);
			}

			var addedCustomer = [];
			getRelatedProjects(user).DropDownList
			        .forEach(function(project) {
				        var customerId = project.CustomerId;

				        if (_.indexOf(addedCustomer, customerId) == -1) {
					        addedCustomer.push(customerId);
					        customerField.addSelectOption(customerId,
					                project.Customer);
				        }
			        });

			nlapiLogExecution('debug', 'start', 'C');

			if (filterCustomer) {
				customerField.setDefaultValue(filterCustomer);
			}

			var projectField = form.addField('custpage_filter_project',
			        'select', 'Project');

			if (filterProject) {
				projectField.addSelectOption('', '', false);
			} else {
				projectField.addSelectOption('', '', true);
			}

			var addedProject = [];
			var relatedProjects = getRelatedProjects(user, isDetailedView,
			        null, filterCustomer, null);

			nlapiLogExecution('debug', 'start', 'D');

			relatedProjects.DropDownList.forEach(function(project) {
				var projectId = project.Id;

				if (_.indexOf(addedProject, projectId) == -1) {
					addedProject.push(projectId);
					projectField.addSelectOption(projectId, project.EntityId
					        + " : " + project.Name);
				}
			});

			if (filterProject) {
				projectField.setDefaultValue(filterProject);
			}

			var statusField = form.addField('custpage_filter_status', 'select',
			        'Project Status');
			statusField.addSelectOption('2', 'Active', true);
			statusField.addSelectOption('4', 'Pending');

			if (filterStatus) {
				statusField.setDefaultValue(filterStatus);
			}

			var startDateField = form.addField('custpage_filter_start_date',
			        'date', 'From Date');
			if (filterStartDate) {
				startDateField.setDefaultValue(filterStartDate);
			}

			var endDateField = form.addField('custpage_filter_end_date',
			        'date', 'To Date');
			if (filterEndDate) {
				endDateField.setDefaultValue(filterEndDate);
			}
			var context = nlapiGetContext();
			
			var o_current_deployment = form.addField('custpage_currentdeploymentid',
			        'text', 'Current deployment').setDisplayType('hidden');
			
			o_current_deployment.setDefaultValue(context.getDeploymentId());
			
			
			//form.setScript('customscript_cs_sut_pm_alloc');
			form.setScript('customscript_cs_sut_ado_alloc');
			var i = 0;

			nlapiLogExecution('debug', 'project_list.DropDownList.length',
			        project_list.DropDownList.length);
			
			
			for (var k = 0; k < project_list.DropDownList.length; k++) {
				var project = project_list.DropDownList[k];
				nlapiLogExecution('debug', 'context.getRemainingUsage()',
				        context.getRemainingUsage());

				i = i + 1;
				addProjectTab(form, project, i, filterCustomer,
				        filterStartDate, filterEndDate, filterStatus);

				if (context.getRemainingUsage() < 50) {
					break;
				}
			}

			form.addField('custpage_job_count', 'integer', 'Project Count')
			        .setDisplayType('hidden').setDefaultValue(i);

			form.addSubmitButton('Submit Remarks');
			form.addButton('custpage_refresh', 'Refresh', 'refreshAllocations');
			form.addButton('custpage_export', 'Export All', "exportToExcel('"+context.getDeploymentId()+"')");
		} else {
			form.addField('custpage_label', 'text', '')
			        .setDisplayType('inline').setDefaultValue(
			                'No Projects Available');
		}

		response.writePage(form);
	} catch (err) {
		nlapiLogExecution('error', 'createGetForm', err);
		throw err;
	}
}

function addProjectTab(form, projectDetails, jobIndex, customer, startDate,
        endDate, status)
{

	var projectId = projectDetails.Id;
	var tabId = 'custpage_tab_' + projectDetails.Id;
	form.addTab(tabId, projectDetails.Name);

	addProjectSublist(form, projectId, tabId, projectDetails);

	// get all project allocations
	var allocation_details = getProjectAllocationDetails(projectId, customer,
	        startDate, endDate);

	var allocationSubTabId = 'custpage_allocation_tab_' + projectId;
	form.addSubTab(allocationSubTabId, 'Allocation Details', tabId);
	form.addField('custpage_job_comment_' + projectId, 'textarea',
	        'Project Comments', null, allocationSubTabId);

	// present allocation
	var present_allocation_list = form.addSubList('custpage_present_list_'
	        + projectId, 'list', 'Allocations', allocationSubTabId);
	addSublistColumns(present_allocation_list);
	present_allocation_list
	        .setLineItemValues(allocation_details.PresentAllocation);

	// past allocation
	// var past_allocation_list = form.addSubList('custpage_past_list_'
	// + projectId, 'list', 'Past Allocations', tabId);
	// addSublistColumns(past_allocation_list);
	// past_allocation_list.setLineItemValues(allocation_details.PastAllocation);

	var deployemnetid='';
	var context=nlapiGetContext();
	var current_deploymentid=context.getDeploymentId();
	if(current_deploymentid=='customdeploy_sut_ado_weekly_allocation')
	{
		deployemnetid='customdeploy_project_details_under_ado';
	}
	else if(current_deploymentid=='customdeploy_sut_pm_weekly_allocation')
	{
		deployemnetid='customdeploy_project_details_under_pm';
	}
	else if(current_deploymentid=='customdeploy_sut_dm_weekly_allocation')
	{
		deployemnetid='customdeploy_project_details_under_dm';
	}
	if(deployemnetid)
	{
	var projectUrl = nlapiResolveURL('SUITELET',
	        'customscript_sut_pm_project_details',
	        deployemnetid)
	        + "&pi=" + projectId;
	}
	// form.addField('custpage_job_link_' + jobIndex, 'url', 'Project', null,
	// tabId).setDisplayType('inline').setLinkText(projectDetails.Name)
	// .setDefaultValue(projectUrl);

	form
	        .addField('custpage_job_' + jobIndex, 'select', 'Project', 'job',
	                tabId).setDisplayType('hidden').setDefaultValue(projectId);

	if (allocation_details.PresentAllocation.length > 0) {
		var csvExportUrl = nlapiResolveURL('SUITELET',
		        'customscript_sut_ado_weekly_allocation',
		        current_deploymentid)
		        + "&mode=excel&project="
		        + (projectId ? projectId : '')
		        + "&customer="
		        + (customer ? customer : '')
		        + "&startdate="
		        + (startDate ? startDate : '')
		        + "&enddate="
		        + (endDate ? endDate : '')
		        + "&status="
		        + (status ? status : '2');

		form.addField('custpage_export_' + jobIndex, 'url', '', null, tabId)
		        .setDisplayType('inline').setLinkText('Export To Excel')
		        .setDefaultValue(csvExportUrl);
	}
}

function addProjectSublist(form, projectId, tabId, projectDetails) {
	try {
		// var projectRecord = nlapiLoadRecord('job', projectId);


		var deployemnetid='';
	var context=nlapiGetContext();
	if(context.getDeploymentId()=='customdeploy_sut_ado_weekly_allocation')
	{
		deployemnetid='customdeploy_project_details_under_ado';
	}
	else if(context.getDeploymentId()=='customdeploy_sut_pm_weekly_allocation')
	{
		deployemnetid='customdeploy_project_details_under_pm';
	}
	else if(context.getDeploymentId()=='customdeploy_sut_dm_weekly_allocation')
	{
		deployemnetid='customdeploy_project_details_under_dm';
	}
	if(deployemnetid)
	{
		var projectUrl = nlapiResolveURL('SUITELET',
		        'customscript_sut_pm_project_details',
		        deployemnetid)
		        + "&pi=" + projectId;
	}
	//var projectUrl = nlapiResolveURL('SUITELET','customscript_sut_pm_project_details','customdeploy_project_details_under_ado')+ "&pi=" + projectId;
				
		var projectList = form.addSubList('custpage_project_details'
		        + projectId, 'staticlist', 'Project Details', tabId);
		projectList.addField('text_a', 'text', '').setDisplayType('inline');
		projectList.addField('text_b', 'text', '').setDisplayType('inline');
		projectList.addField('text_c', 'text', '').setDisplayType('inline');
		projectList.addField('text_d', 'text', '').setDisplayType('inline');

		var billingType = projectDetails.BillingType;
		var isMonthly = projectDetails.IsMonthly == 'T' ? " Monthly" : "";

		var valueList = [
		        {
		            text_a : '<b>Project : </b>' + '<a target="_blank" href="'
		                    + projectUrl + '">' + projectDetails.EntityId
		                    + " : " + projectDetails.Name + '</a>',
		            text_b : '<b>Customer : </b>' + projectDetails.Customer,
		            text_c : '<b>Status : </b>' + projectDetails.Status,
		            text_d : '<b>Billing Type : </b>' + billingType + isMonthly
		        },
		        {
		            text_a : '<b>Vertical : </b>' + projectDetails.Vertical,
		            text_b : '<b>Practice : </b>' + projectDetails.Practice,
		            text_c : '<b>Start Date : </b>' + projectDetails.StartDate,
		            text_d : '<b>End Date : </b>' + projectDetails.EndDate
		        },
		        {
		            text_a : '<b>Project Manager : </b>'
		                    + projectDetails.ProjectManager,
		            text_b : '<b>Delivery Manager : </b>'
		                    + projectDetails.DeliveryManager,
		            text_c : '<b>Client Partner : </b>'
		                    + projectDetails.ClientPartner,
		            text_d : '<b>Project Value : </b>'
		                    + projectDetails.ProjectValue
		        } ];

		projectList.setLineItemValues(valueList);
	} catch (err) {
		nlapiLogExecution('ERROR', 'addProjectSublist', err);
		throw err;
	}
}

function addSublistColumns(sublist) {
	sublist.addField('resource', 'select', 'Resource', 'employee')
	        .setDisplayType('inline');
	
	sublist.addField('internalid', 'text', 'Resource Allocation')
	        .setDisplayType('hidden');
	sublist.addField('resourcename', 'text', 'Resource Name').setDisplayType(
	        'hidden');
  sublist.addField('practice', 'text', 'Practice').setDisplayType(
	        'inline');
	sublist.addField('sub_practice', 'text', 'sub practice').setDisplayType(
	        'inline');
	sublist.addField('startdate', 'date', 'Start Date')
	        .setDisplayType('inline');
	sublist.addField('enddate', 'date', 'End Date').setDisplayType('inline');
	sublist.addField('isbillable', 'checkbox', 'Is Billable?').setDisplayType(
	        'inline');
	sublist.addField('billrate', 'currency', 'Bill Rate').setDisplayType(
	        'inline');
	sublist.addField('percent', 'percent', '% Allocation').setDisplayType(
	        'inline');
	sublist.addField('billingstartdate', 'date', 'Billing Start Date')
	        .setDisplayType('inline');
	sublist.addField('billingenddate', 'date', 'Billing End Date')
	        .setDisplayType('inline');
	sublist.addField('timeapprover', 'select', 'Time Approver', 'employee')
	        .setDisplayType('inline');
	sublist.addField('expenseapprover', 'select', 'Expense Approver',
	        'employee').setDisplayType('inline');
	sublist.addField('site', 'select', 'Site', 'customlistonsiteoffsitelist')
	        .setDisplayType('inline');
	sublist.addField('change', 'text', 'Change Made').setDisplayType('inline');
	sublist.addField('ismarked', 'checkbox', 'Modified').setDisplayType(
	        'hidden');
	sublist.addField('remarks', 'textarea', 'Remarks').setDisplaySize(20, 2)
	        .setDisplayType('entry');
}

function getProjectAllocationDetails(projectIds, customerIds, startDate,
        endDate)
{
  
 nlapiLogExecution('debug', 'Overview',projectIds + ', '+customerIds+', '+startDate+', '+ endDate);
	try {
		var filters = [ new nlobjSearchFilter('project', null, 'anyof',
		        projectIds) ];

		if (startDate && endDate) {
			filters.push(new nlobjSearchFilter('enddate', null, 'notbefore',
			        startDate));
			filters.push(new nlobjSearchFilter('startdate', null, 'notafter',
			        endDate));
		} else if (startDate) {
			filters.push(new nlobjSearchFilter('enddate', null, 'notbefore',
			        startDate));
		} else if (endDate) {
			filters.push(new nlobjSearchFilter('startdate', null, 'notafter',
			        endDate));
		} else {
			filters.push(new nlobjSearchFilter('enddate', null, 'onorafter',
			        'today'));
		}

		nlapiLogExecution('debug', 'filters count', filters.length);

		var allocationSearch = searchRecord(
		        'resourceallocation',
		        null,
		        filters,
		        [
		                new nlobjSearchColumn('resource'),
		                new nlobjSearchColumn('project'),
		                new nlobjSearchColumn('startdate'),
		                new nlobjSearchColumn('enddate'),
		                new nlobjSearchColumn('custeventrbillable'),
		                new nlobjSearchColumn('percentoftime'),
		                new nlobjSearchColumn('custeventbstartdate'),
		                new nlobjSearchColumn('custeventbenddate'),
		                new nlobjSearchColumn('custeventwlocation'),
		                new nlobjSearchColumn('custevent3'),
		                new nlobjSearchColumn('custevent_practice'),
		                new nlobjSearchColumn('custevent4'),
		                new nlobjSearchColumn('custevent_allocation_status'),
		                new nlobjSearchColumn('approver', 'employee'),
		                new nlobjSearchColumn('custevent_ra_last_modified_date')
		                        .setSort(true),
		                new nlobjSearchColumn('timeapprover', 'employee'),
new nlobjSearchColumn("department","employee",null),						]);

		nlapiLogExecution('debug', 'allocation count', allocationSearch.length);

		var presentAllocation = [];
		var pastAllocation = [];
		var futureAllocation = [];

		if (allocationSearch) {
			var currentDate = new Date();
			var weekStartDate = getWeekStartDate();

			allocationSearch
			        .forEach(function(allocation) {
				        var endDate = nlapiStringToDate(allocation
				                .getValue('enddate'));
				        var startDate = nlapiStringToDate(allocation
				                .getValue('startdate'));
              if(allocation.getId() == 80283){
                nlapiLogExecution('debug', 'TIME-Approver', allocation.getValue('timeapprover',
				                    'employee'));
                  nlapiLogExecution('debug', 'TIME-Approver', allocation.getText(
				                    'timeapprover', 'employee'));
              }
                  
					var sub_practice = allocation.getText("department","employee",null);
					var subpractice_text = ''
					if(sub_practice.split(':').length==1){
					subpractice_text = sub_practice;
					}
					else {
					subpractice_text = sub_practice.split(':')[1];
					}
					var practice =  sub_practice.split(':')? sub_practice.split(':')[0]:sub_practice;
				  
				 
				        var currentAllocation = {
				            internalid : allocation.getId(),
				            resource : allocation.getValue('resource'),
				            resourcename : allocation.getText('resource'),
				            startdate : allocation.getValue('startdate'),
				            enddate : allocation.getValue('enddate'),
				            isbillable : allocation
				                    .getValue('custeventrbillable'),
				            billrate : allocation.getValue('custevent3'),
				            percent : allocation.getValue('percentoftime'),
				            location : allocation
				                    .getValue('custeventwlocation'),
				            billingstartdate : allocation
				                    .getValue('custeventbstartdate'),
				            billingenddate : allocation
				                    .getValue('custeventbenddate'),
				           
				            timeapprover : allocation.getValue('timeapprover',
				                    'employee'),
				            timeapprover_text : allocation.getText(
				                    'timeapprover', 'employee'),
				            expenseapprover : allocation.getValue('approver',
				                    'employee'),
				            expenseapprover_text : allocation.getText(
				                    'approver', 'employee'),
				            site : allocation.getValue('custevent4'),
				            site_text : allocation.getText('custevent4'),
				            location : allocation
				                    .getValue('custeventwlocation'),
				            location_text : allocation
				                    .getText('custeventwlocation'),
				            ismarked : 'F',
				            lastmodified : '',
				            change : '',
							sub_practice: subpractice_text,
							practice:practice
				        };

				        var last_modified_date = nlapiStringToDate(allocation
				                .getValue('custevent_ra_last_modified_date'));

				        if (last_modified_date) {
					        currentAllocation.lastmodified = nlapiDateToString(
					                last_modified_date, 'date');

					        if (weekStartDate <= last_modified_date) {
						        currentAllocation.ismarked = 'T';
						        currentAllocation.change = allocation
						                .getText('custevent_allocation_status');

					        }
				        }

				        // if (currentDate > endDate) {
				        // pastAllocation.push(currentAllocation);
				        // } //else if (currentDate < startDate) {
				        // futureAllocation.push(currentAllocation);
				        // } else {
				        presentAllocation.push(currentAllocation);
				        // }
			        });
		}

		return {
		    PresentAllocation : presentAllocation,
		    FutureAllocation : futureAllocation,
		    PastAllocation : pastAllocation
		};
	} catch (err) {
		nlapiLogExecution('ERROR', 'getProjectAllocationDetails', err);
		throw err;
	}
}

function getWeekStartDate() {
	var currentDate = new Date();
	var daysFromSunday = currentDate.getDay();
	var sundayOfWeek = nlapiAddDays(currentDate, -daysFromSunday);
	return sundayOfWeek;
}

function getRelatedProjects(currentUser, detailedView, projectId, customerId,
        filterStatus)
{
	try {
		if (!filterStatus) {
			filterStatus = 2;
		}
		if(parseInt(currentUser) == parseInt(41571)){
          currentUser = 1670;
        }
		
		var context=nlapiGetContext();
		var filters =[];
		if(context.getDeploymentId()=='customdeploy_sut_ado_weekly_allocation')
		{
			filters.push([ 'customer.custentity_account_delivery_owner', 'anyof', currentUser]);
		}
		else if(context.getDeploymentId()=='customdeploy_sut_pm_weekly_allocation')
		{
			filters.push(['custentity_projectmanager', 'anyof', currentUser]);
		}
		else if(context.getDeploymentId()=='customdeploy_sut_dm_weekly_allocation')
		{
			filters.push(['custentity_deliverymanager', 'anyof', currentUser]);
		}
		if(filters.length!=0)
		{
		 filters.push('and');
		 filters.push(['status', 'anyof', filterStatus]);
		}
		if (projectId) {
			filters.push('and');
			filters.push(['internalid', 'anyof', projectId]);
		}

		if (customerId) {
			filters.push('and');
			filters.push(['customer', 'anyof', customerId]);
		}

		var projectSearch = nlapiSearchRecord('job', null, filters, [
		        new nlobjSearchColumn('entityid'),
		        new nlobjSearchColumn('altname'),
		        new nlobjSearchColumn('companyname'),
		        new nlobjSearchColumn('customer'),
		        new nlobjSearchColumn('startdate').setSort(true),
		        new nlobjSearchColumn('jobbillingtype'),
		        new nlobjSearchColumn('custentity_t_and_m_monthly'),
		        new nlobjSearchColumn('entitystatus'),
		        new nlobjSearchColumn('custentity_vertical'),
		        new nlobjSearchColumn('custentity_practice'),
		        new nlobjSearchColumn('enddate'),
		        new nlobjSearchColumn('custentity_projectmanager'),
		        new nlobjSearchColumn('custentity_deliverymanager'),
		        new nlobjSearchColumn('custentity_clientpartner'),
		        new nlobjSearchColumn('custentity_projectvalue') ]);

		var project_id = [];
		var project_list = [];

		if (projectSearch) {
			nlapiLogExecution('debug', 'project count', projectSearch.length);

			projectSearch.forEach(function(project) {
				project_id.push(project.getId());

				project_list
				        .push({
				            Id : project.getId(),
				            Name : project.getValue('companyname'),
				            EntityId : project.getValue('entityid'),
				            Customer : project.getText('customer'),
				            CustomerId : project.getValue('customer'),
				            StartDate : project.getValue('startdate'),
				            EndDate : project.getValue('enddate'),
				            BillingType : project.getText('jobbillingtype'),
				            IsMonthly : project
				                    .getText('custentity_t_and_m_monthly'),
				            Status : project.getText('entitystatus'),
				            Vertical : project.getText('custentity_vertical'),
				            Practice : project.getText('custentity_practice'),
				            ProjectManager : project
				                    .getText('custentity_projectmanager'),
				            ClientPartner : project
				                    .getText('custentity_clientpartner'),
				            DeliveryManager : project
				                    .getText('custentity_deliverymanager'),
				            ProjectValue : project
				                    .getValue('custentity_projectvalue')
				        });
			});
		} else {
			throw "No Projects Found";
		}

		return {
		    IdList : project_id,
		    DropDownList : project_list
		};
	} catch (err) {
		nlapiLogExecution('ERROR', 'getRelatedProjects', err);
		throw err;
	}
}

// -- POST

function getPracticeRelatedProjects(practiceList) {
	try {
		var project_id = [], project_list = [];

		// search for the related practices in the practice master
		var projectSearch = nlapiSearchRecord('job', null, [
		        new nlobjSearchFilter('custentity_practice', null, 'anyof',
		                practiceList),
		        new nlobjSearchFilter('isinactive', null, 'is', 'F'),
		        new nlobjSearchFilter('status', null, 'anyof', '2') ],
		        [ new nlobjSearchColumn('altname') ]);

		if (projectSearch) {

			projectSearch.forEach(function(project) {
				project_id.push(project.getId());

				project_list.push({
				    Id : project.getId(),
				    Name : project.getValue('altname')
				});
			});
		}

		return {
		    IdList : project_id,
		    DropDownList : project_list
		};
	} catch (err) {
		nlapiLogExecution('ERROR', 'getPracticeRelatedProjects', err);
		throw err;
	}
}

function submitForm(request) {
	try {
		var project_count = request.getParameter('custpage_job_count');
		nlapiLogExecution('debug', 'project_count', project_count);
		var context=nlapiGetContext();
		var s_current_deployment=context.getDeploymentId();
		var loggedUser = nlapiGetUser();
		// var userDetails = nlapiLookupField('employee', loggedUser, [
		// 'firstname', 'lastname', 'email' ]);
		// var requestorName = userDetails.firstname + " " +
		// userDetails.lastname;

		for (var i = 1; i <= project_count; i++) {
			var project_field_id = 'custpage_job_' + i;
			var project_id = request.getParameter(project_field_id);
			nlapiLogExecution('debug', '----',
			        '----------------------------------------');
			nlapiLogExecution('debug', 'project_id', project_id);
			var present_sublist_id = 'custpage_present_list_' + project_id;
			var allocation_comment_id = 'custpage_job_comment_' + project_id;

			// var past_sublist_id = 'custpage_past_list_' + project_id;
			// var future_sublist_id = 'custpage_future_list_' + project_id;
			var remarksObject = {};

			var mailContent = "";
			var details = generateMailContentLineLevel(request,
			        present_sublist_id, "Allocations", remarksObject,
			        allocation_comment_id);
			mailContent += details.Remarks_Text;
			// nlapiLogExecution('debug', '1', mailContent);
			if (mailContent) {
				mailContent += "\n";
			}

			// mailContent += generateMailContentLineLevel(request,
			// past_sublist_id, "Past Allocations", case_comments);

			// if (mailContent) {
			// mailContent += "\n";
			// }
			nlapiLogExecution('debug', '2', mailContent);

			// mailContent += generateMailContentLineLevel(request,
			// future_sublist_id, "Future Allocations", case_comments);

			nlapiLogExecution('debug', '3', mailContent);

			if (mailContent) {
				var projectDetails = nlapiLookupField('job', project_id, [
				        'entityid', 'altname' ]);
				mailContent = "Project : " + projectDetails.entityid + " "
				        + projectDetails.altname + "\n" + mailContent;

				var caseRecord = nlapiCreateRecord('supportcase', {
				    'customform' : '41', // 39 // 41
				    'recordmode' : 'dynamic'
				});

				caseRecord.setFieldValue('title', 'Allocation Changes : '
				        + projectDetails.entityid + " "
				        + projectDetails.altname);
				caseRecord.setFieldValue('assigned', '13122'); // 10843 //
				// 13122
				caseRecord.setFieldValue('custevent_case_project', project_id);
				caseRecord.setFieldValue('custevent_case_notes', JSON
				        .stringify(details.Remarks_Object));
				caseRecord.setFieldValue('company', nlapiGetUser());
				caseRecord.setFieldValue('profile', '2');
				caseRecord.setFieldValue('item', '2526'); // 2512 // 2526
				caseRecord.setFieldValue('category', '4');
				caseRecord.setFieldValue('issue', '1');
				caseRecord.setFieldValue('origin', '3');
				caseRecord.setFieldValue('incomingmessage', mailContent);

				var caseId = nlapiSubmitRecord(caseRecord);
				nlapiLogExecution('audit', 'New Case Record Created',
				        'Internal Id' + caseId);
			}
		}

		var form = nlapiCreateForm('Resource Allocation');
		//form.setScript('customscript_cs_sut_pm_alloc');
        form.setScript('customscript_cs_sut_ado_alloc');
		form.addField('custpage_msg', 'text', '').setDisplayType('inline')
		        .setDefaultValue('Your request has been submitted.');

		form.addButton('custpage_btn_back', 'Go Back', "goBack('"+s_current_deployment+"')");

		response.writePage(form);
	} catch (err) {
		nlapiLogExecution('ERROR', 'submitForm', err);
		throw err;
	}
}

function generateMailContentLineLevel(request, sublistId, allocationType,
        remarksObject, additionalCommentsFieldId)
{
	try {
		// remarksObject = {};
		var case_comments = [];
		var lineCount = request.getLineItemCount(sublistId);
		nlapiLogExecution('debug', sublistId + " " + allocationType, lineCount);

		var anyRemarks = false;
		var text = "\n" + allocationType + " :- \n";

		for (var linenum = 1; linenum <= lineCount; linenum++) {
			var remarks = request.getLineItemValue(sublistId, 'remarks',
			        linenum);

			if (isNotEmpty(remarks)) {
				nlapiLogExecution('debug', 'remarks', remarks);
				anyRemarks = true;
				text += request.getLineItemValue(sublistId, 'resourcename',
				        linenum);
				text += " : " + remarks;
				text += "\n";
				case_comments.push({
				    i : request.getLineItemValue(sublistId, 'internalid',
				            linenum),
				    r : remarks
				});
			}
		}

		remarksObject.ar = case_comments;

		// additional comments part
		var additionalComments = request
		        .getParameter(additionalCommentsFieldId);
		if (additionalComments) {

			if (!anyRemarks)
				text = "";

			text += "\n";
			text += "Project Comments : \n" + additionalComments;
			text += "\n";
			remarksObject.ac = additionalComments;
			anyRemarks = true;
		}

		nlapiLogExecution('debug', 'A remarks ' + allocationType, text);
		if (!anyRemarks) {
			text = "";
		}
		nlapiLogExecution('debug', 'B remarks ' + allocationType, text);

		return {
		    Remarks_Object : remarksObject,
		    Remarks_Text : text
		};
	} catch (err) {
		nlapiLogExecution('ERROR', 'generateMailContentLineLevel', err);
		throw err;
	}
}

function getAllocationCaseMailTemplate(tableData, userName, projectName,
        requestorName)
{
	var htmltext = '<table border="0" width="100%">';
	htmltext += '<tr>';
	htmltext += '<td colspan="4" valign="top">';
	htmltext += '<p>Hi,</p>';
	htmltext += '<p></p>';
	htmltext += '<p>This is to inform you that <b>' + userName
	        + '</b> has updated allocation details for <b>' + projectName
	        + '</b>.</p>';

	htmltext += tableData;

	htmltext += '<p></p>';
	htmltext += '</td></tr>';
	htmltext += '<tr></tr>';
	htmltext += '<tr></tr>';
	htmltext += '<p><br/>Regards,<br/>';
	htmltext += 'Information Systems</p>';
	htmltext += '</table>';
	htmltext += '<hr width="100%" size="1" noshade color="#CCCCCC">';
	htmltext += '<table width="100%" border="0" cellspacing="0" cellpadding="0">';
	htmltext += '<tr>';
	htmltext += '<td align="right">';
	htmltext += '<font style="font-size:9px; font-family:Verdana,Arial,Helvetica,sans-serif; color:#999999;">'
	        + 'Brillio is powered by <a href="http://www.netsuite.com/" style="color:#999999;">NetSuite</a>'
	        + ' &#151; One System. No Limits.</font>';
	htmltext += '</td>';
	htmltext += '</tr>';
	htmltext += '</table>';
	htmltext += '</body>';
	htmltext += '</html>';

	return {
	    Subject : 'Allocation Updation',
	    Body : htmltext
	};
}

function getPracticeHeadRelatedPractice() {
	try {
		var user = nlapiGetUser();
		var relatedPractices = [];

		// search for the related practices in the practice master
		var practiceSearch = nlapiSearchRecord('department', null, [
		        new nlobjSearchFilter('custrecord_practicehead', null, 'anyof',
		                user),
		        new nlobjSearchFilter('isinactive', null, 'is', 'F') ]);

		if (practiceSearch) {
			// var relatedPractices = [];

			practiceSearch.forEach(function(prac) {
				relatedPractices.push(prac.getId());
			});
		}

		return relatedPractices;
	} catch (err) {
		nlapiLogExecution('ERROR', 'getPracticeHeadRelatedPractice', err);
		throw err;
	}
}

// ----------Client Script

function goBack(s_current_deployment) {
	window.location = nlapiResolveURL('SUITELET',
	        'customscript_sut_ado_weekly_allocation',
	        s_current_deployment);
}

function refreshAllocations() {
	var customer = nlapiGetFieldValue('custpage_filter_customer');
	var project = nlapiGetFieldValue('custpage_filter_project');
	var status = nlapiGetFieldValue('custpage_filter_status');
	var startdate = nlapiGetFieldValue('custpage_filter_start_date');
	var enddate = nlapiGetFieldValue('custpage_filter_end_date');
	var s_current_deployment=nlapiGetFieldValue('custpage_currentdeploymentid');
	window.location = nlapiResolveURL('SUITELET',
	        'customscript_sut_ado_weekly_allocation',
	        s_current_deployment)
	        + "&customer="
	        + customer
	        + "&project="
	        + project
	        + "&startdate="
	        + startdate + "&enddate=" + enddate + "&status=" + status;
}

function goToDashboard() {
	var dashboardUrl = "/app/center/card.nl";
	window.location = dashboardUrl;
}

function getCSProjectDetails() {
	try {
		var selected_project = nlapiGetFieldValue('custpage_current_project');

		if (selected_project) {
			var script = 'customscript_sut_pm';
			var deployment = 'customdeploy_sut_pm';
			var url = "https://system.sandbox.netsuite.com/app/site/hosting/scriptlet.nl?script=585&deploy=1"
			        + "&project=" + selected_project;
			window.location = url;
		} else {
			alert("No Project Selected");
		}
	} catch (err) {
		nlapiLogExecution('ERROR', 'getCSProjectDetails', err);
		alert(err.message);
	}
}

function changeProject() {
	try {
		var url = "https://system.sandbox.netsuite.com/app/site/hosting/scriptlet.nl?script=585&deploy=1";
		window.location = url;
	} catch (err) {
		nlapiLogExecution('ERROR', 'getCSProjectDetails', err);
		alert(err.message);
	}
}

function fieldChangeEvent(type, name, linenum) {

	if (name == 'custpage_filter_customer') {
		nlapiSetFieldValue('custpage_filter_project', '');
		refreshAllocations();
	} else if (name == '') {
		refreshAllocations();
	}
}

function exportToExcel(s_current_deployment) {
	try {
		
		var startdate = nlapiGetFieldValue('custpage_filter_start_date');
		var enddate = nlapiGetFieldValue('custpage_filter_end_date');
		var customer = nlapiGetFieldValue('custpage_filter_customer');
		var project = nlapiGetFieldValue('custpage_filter_project');
		var status = nlapiGetFieldValue('custpage_filter_status');

		var csvExportUrl = nlapiResolveURL('SUITELET',
		        'customscript_sut_ado_weekly_allocation',
		       // 'customdeploy_sut_ado_weekly_allocation')
			   s_current_deployment)
		        + "&mode=excel&project="
		        + project
		        + "&customer="
		        + customer
		        + "&startdate="
		        + startdate
		        + "&enddate="
		        + enddate
		        + "&status=" + status;

		var win = window.open(csvExportUrl);
		win.focus();
	} catch (err) {
		nlapiLogExecution('ERROR', 'exportToExcel', err);
		nlapiLogExecution('DEBUG', 'exportToExcel', err);
		alert(err.message);
	}
}

function readSublistDetails(allocation_details, projectId) {
	var projectName = nlapiGetFieldText(fieldId);
	var text = "";
	text += "Project Name" + projectName + "\n";

	return text;
}

function pageInit() {
	var cssElementContent = " .marked-row td { background-color : #E9FFE9 !important; } ";
	var headElement = document.getElementsByTagName('head')[0];
	var cssStyleElement = document.createElement('style');
	cssStyleElement.innerHTML = cssElementContent;
	headElement.appendChild(cssStyleElement);

	var projectCount = nlapiGetFieldValue('custpage_job_count');

	for (var i = 1; i <= projectCount; i++) {
		var projectId = nlapiGetFieldValue('custpage_job_' + i);
		var sublists = [ 'custpage_past_list_' + projectId,
		        'custpage_present_list_' + projectId ];

		for (var j = 0; j < 2; j++) {
			var linecount = nlapiGetLineItemCount(sublists[j]);

			for (var linenum = 1; linenum <= linecount; linenum++) {
				var isMarked = nlapiGetLineItemValue(sublists[j], 'ismarked',
				        linenum);

				if (isMarked == 'T') {
					document
					        .getElementById(sublists[j] + "row" + (linenum - 1)).style.fontWeight = "bold";
					document
					        .getElementById(sublists[j] + "row" + (linenum - 1)).className += " marked-row";

				}
			}
		}
	}
}



function getHeaderInfo(strName){
/*
	strName+="<style>";
	strName+="th{background-color: #3c8dbc; color:white;}";
	strName+=".BorderTopLeft{border-top:solid;border-left:solid}";
	strName+=".BorderBottomLeft{border-bottom:solid;border-left:solid}";
	strName+=".BorderTopRight{border-top:solid;border-right:solid}";
	strName+=".BorderBottomRight{border-bottom:solid;border-right:solid}";
	strName+=".BorderTop{border-top:solid;}";
	strName+=".BorderLeft{border-left:solid}";
	strName+=".BoderAll{border-bottom:solid;border-right:solid;border-Top:solid;border-left:solid}";
	strName+=".BorderRight{border-right:solid;}";
	strName+=".BorderBottom{border-bottom:solid;}";
	strName+=".BorderTopLeftRight{border-Top:solid;border-Right:solid;border-Left:solid;}"; 
	strName+="body{font-family:sans-serif;font-size:8px;margin-top:0px;}";
	strName+="</style>";
	strName+="<macrolist>";
	strName+="<macro id='myfooter'>";
	strName+="<table  style='width: 100%;'><tr>";
	strName+="</tr></table>";
	strName+="</macro>";
	strName+="<macro id='myHead'>";
	strName+="</macro>";
	strName+="</macrolist>";
	strName+="</head>";
	
	*/
	strName += "<table width='1430px' border='1'>";
	strName += "<tr>";
	strName += "<td width='30px' style='align:left;' font-size='10px'><b>Client</b></td>"; 
	strName += "<td width='30px' style='align:left;' font-size='10px'><b>Project ID	</b></td>"; 
	strName += "<td width='30px' style='align:left;' font-size='10px'><b> Project Name</b></td>"; 
	strName += "<td width='30px' style='align:left;' font-size='10px'><b> Project Type</b></td>"; 
	strName += "<td width='30px' style='align:left;' font-size='10px'><b> Start Date</b></td>"; 
	strName += "<td width='30px' style='align:left;' font-size='10px'><b> End Date</b></td>";
	
	strName += "<td width='30px' style='align:left;' font-size='10px'><b> Project Manager</b></td>";
	strName += "<td width='30px' style='align:left;' font-size='10px'><b> Delivery Manager</b></td>";
	strName += "<td width='30px' style='align:left;' font-size='10px'><b> Client Partner</b></td>";
	strName += "<td width='30px' style='align:left;' font-size='10px'><b>Resource</b></td>";
	strName += "<td width='30px' style='align:left;' font-size='10px'><b> Practice</b></td>";
	strName += "<td width='30px' style='align:left;' font-size='10px'><b>Sub Practice</b></td>";
	
	strName += "<td width='30px' style='align:left;' font-size='10px'><b> Time Approver</b></td>";
	strName += "<td width='30px' style='align:left;' font-size='10px'><b> Expense Approver</b></td>";
	strName += "<td width='30px' style='align:left;' font-size='10px'><b> Start Date </b></td>";
	strName += "<td width='30px' style='align:left;' font-size='10px'><b> End Date</b></td>";
	strName += "<td width='30px' style='align:left;' font-size='10px'><b> Billable </b></td>";
	strName += "<td width='30px' style='align:left;' font-size='10px'><b> Bill Rate </b></td>";
	
	strName += "<td width='30px' style='align:left;' font-size='10px'><b> %age Allocation</b></td>";
	strName += "<td width='30px' style='align:left;' font-size='10px'><b> Billing Start Date</b></td>";
    strName += "<td width='30px' style='align:left;' font-size='10px'><b> Billing End Date</b></td>";
	strName += "<td width='30px' style='align:left;' font-size='10px'><b> Site</b></td>";
	strName += "<td width='30px' style='align:left;' font-size='10px'><b> Location</b></td>";

	strName += "</tr>";

	return strName;
}

function dateString(dateStr) {
    if(!(_logValidation(dateStr))){
      return dateStr;
    }
    var newDate = new Date(dateStr);
    var dateStrValue =  newDate.getDate()+'-'+(newDate.getMonth()+1)+'-'+newDate.getFullYear();
    return dateStrValue;

}

function _logValidation(value) 
{
 if(value != null && value.toString() != null && value != '' && value != undefined && value.toString() != undefined && value != 'undefined' && value.toString() != 'undefined'&& value.toString() != 'NaN' && value != NaN) 
 {
  return true;
 }
 else 
 { 
  return false;
 }
}