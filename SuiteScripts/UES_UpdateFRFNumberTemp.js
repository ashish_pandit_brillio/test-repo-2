function afterSubmitSetFRFNumber(type)
{
	if(type=='edit' ||type=='create')
	{
		try
		{
			var recId = nlapiGetRecordId();
			var recType = nlapiGetRecordType();
			var recordObject = nlapiLoadRecord(recType,recId);
            nlapiLogExecution('Debug','recId ',recId);
			if(recId)
			{
				recordObject.setFieldValue('custrecord_frf_details_frf_number',getFRFNumber(recId));
                var submitId = nlapiSubmitRecord(recordObject);
                nlapiLogExecution('Debug','submitId ',submitId);
			}
		}
		catch(error)
		{
			nlapiLogExecution('Debug','Error ',error);
		}
		
	}
}

function getFRFNumber(recId) {
	var str = "" + recId;
	var pad = "00000000";
	var s_number = pad.substring(0, pad.length - str.length) + str;
	return "F"+s_number;
}