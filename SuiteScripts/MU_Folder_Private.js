/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       21 Jul 2015     nitish.mishra
 *
 */

/**
 * @param {String}
 *            recType Record type internal id
 * @param {Number}
 *            recId Record internal id
 * @returns {Void}
 */
function massUpdate(recType, recId) {
	var rec = nlapiLoadRecord(recType, recId);
	rec.setFieldValue('isprivate', 'F');
	rec.setFieldValue('group', '11363');
	nlapiSubmitRecord(rec);
}
