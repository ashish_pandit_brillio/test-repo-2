/**
 * @author Nikhil Jain
 */
// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
/*
   	Script Name:TDS disable Void button
	Author:Nikhil jain
	Company:Aashnacloudtech Pvt. Ltd.
	Date:19-12-2014
	Description:Disable the void button on check record


	Script Modification Log:

	-- Date --			-- Modified By --				--Requested By--				-- Description --



Below is a summary of the process controls enforced by this script file.  The control logic is described
more fully, below, in the appropriate function headers and code blocks.


     BEFORE LOAD
		- beforeLoadRecord(type)



     BEFORE SUBMIT
		- beforeSubmitRecord(type)


     AFTER SUBMIT
		- afterSubmitRecord(type)



     SUB-FUNCTIONS
		- The following sub-functions are called by the above core functions in order to maintain code
            modularization:

               - NOT USED

*/
}
// END SCRIPT DESCRIPTION BLOCK  ====================================



// BEGIN GLOBAL VARIABLE BLOCK  =====================================
{
	//  Initialize any Global Variables, in particular, debugging variables...




}
// END GLOBAL VARIABLE BLOCK  =======================================





// BEGIN BEFORE LOAD ==================================================

function beforeLoadRecord_disable_void(type)
{

	/*  On before load:

          - EXPLAIN THE PURPOSE OF THIS FUNCTION

		-


		FIELDS USED:

          --Field Name--				--ID--


	*/



	//  LOCAL VARIABLES

    //  BEFORE LOAD CODE BODY
	
	//--------------------------------------------If record type is check disable void button if indian subsidiary-----------------------------------//
	
	var recordType = nlapiGetRecordType()
	nlapiLogExecution('DEBUG', 'afterSubmitRecord check', " recordType " + recordType)
	
	if (recordType == 'check') 
	{
		
		var accountpref_config = nlapiLoadConfiguration('accountingpreferences')
		var b_reversalvoiding = accountpref_config.getFieldValue('REVERSALVOIDING');
		
		if ((type == 'edit') || (type =='view' && b_reversalvoiding == 'T')) 
		{
			
			var i_AitGlobalRecId = SearchGlobalParameter();
			nlapiLogExecution('DEBUG', 'Bill ', "i_AitGlobalRecId" + i_AitGlobalRecId);
			
			if (i_AitGlobalRecId != 0) 
			{
				var o_AitGloRec = nlapiLoadRecord('customrecord_indian_tax_glo_parameter', i_AitGlobalRecId);
				
				var a_subisidiary = new Array()
				a_subisidiary = o_AitGloRec.getFieldValues('custrecord_ait_subsidiary');
				nlapiLogExecution('DEBUG', 'check ', "a_subisidiary->" + a_subisidiary);
			
				
			}// end if(i_AitGlobalRecId != 0 )
			var checkid = nlapiGetRecordId()
			nlapiLogExecution('DEBUG', 'afterSubmitRecord check ', " checkid " + checkid)
			
			var recordType = nlapiGetRecordType()
			nlapiLogExecution('DEBUG', 'afterSubmitRecord check', " recordType " + recordType)
			
			var o_checkobj = nlapiLoadRecord(recordType, checkid)
			nlapiLogExecution('DEBUG', 'afterSubmitRecord check', " recordType " + recordType)
			
			if (o_checkobj != null && o_checkobj != '' && o_checkobj != undefined) 
			{
				//var a_subsidiary1 = a_subisidiary.toString();
				var Flag = 0;
				
				if (a_subisidiary[1] != undefined && a_subisidiary[1] != null && a_subisidiary[1] != '') 
				{
				
					for (var y = 0; y < a_subisidiary.length; y++) 
					{
						var i_subsidiary = o_checkobj.getFieldValue('subsidiary')
						
						//==== IF SUBSIDIARY IS INDIAN SUBSIDIARY ====
						if (a_subisidiary[y] == i_subsidiary)
						 {
							Flag = 1;
							break;
						}// END if (a_subisidiary[y] == i_subsidiary)
					}// END for (var y = 0; y < a_subisidiary.length; y++)
				}// END  if (a_subsidiary1.indexOf(',') > -1)
				else {
					// === IF COMMA NOT FOUND ===
					var i_subsidiary = o_checkobj.getFieldValue('subsidiary')
					
					//==== IF SUBSIDIARY IS INDIAN SUBSIDIARY ====
					if (a_subisidiary[0] == i_subsidiary) {
						Flag = 1;
					}// END  if (a_subsidiary1 == i_subsidiary)
				}// END  if (a_subsidiary1 == i_subsidiary)
				//===== FOR SINGLE SUBSIDARY ACCOUNT  =======
				var context = nlapiGetContext();
				var i_subcontext = context.getFeature('SUBSIDIARIES');
				if (i_subcontext == false) 
				{
					Flag = 1;
				}
				
				
				if (Flag == 1) 
				{
					var b_Adavncetoved = o_checkobj.getFieldValue('custbody_tds_advancepay')
					nlapiLogExecution('DEBUG', 'afterSubmitRecord check', " advance to vendor " + b_Adavncetoved)
					
					
					if (b_Adavncetoved == 'T') 
					{
						var i_Bill_ref_no = o_checkobj.getFieldValue('custbody_tds_billcredit_ref')
						nlapiLogExecution('DEBUG', 'afterSubmitRecord check', "Bill credit id" + i_Bill_ref_no)
						
						if (i_Bill_ref_no != '' && i_Bill_ref_no != null && i_Bill_ref_no != undefined) 
						{
						
							var o_billcredit_obj = nlapiLoadRecord('vendorcredit', i_Bill_ref_no);
							nlapiLogExecution('DEBUG', 'afterSubmitRecord check', "bill credit object" + o_billcredit_obj)
							
							if (o_billcredit_obj != null && o_billcredit_obj != '' && o_billcredit_obj != undefined) 
							{
								var line_count = o_billcredit_obj.getLineItemCount('apply');
								nlapiLogExecution('DEBUG', 'afterSubmitRecord check', "line_count" + line_count)
								
								var apply_flag = 0;
								for (var i = 1; i <= line_count; i++) 
								{
									var apply = o_billcredit_obj.getLineItemValue('apply', 'apply', i);
									nlapiLogExecution('DEBUG', 'afterSubmitRecord check', "apply" + apply)
									
									if (apply == 'T') 
									{
										apply_flag = 1;
										break;
									}
								}
								if (apply_flag == 1) 
								{
									var void_btn = form.getButton('void')
									void_btn.setDisabled(true)
									nlapiLogExecution('DEBUG', 'Beforeload', 'Button disabled===>')
								}
							}
						}
					}
				}
			}
		}
		
	}
	
	if(recordType == 'vendorcredit')
	{
		var accountpref_config = nlapiLoadConfiguration('accountingpreferences')
		var b_reversalvoiding = accountpref_config.getFieldValue('REVERSALVOIDING');
							
		if (type == 'edit' && b_reversalvoiding == 'F') 
		{
		
			var i_AitGlobalRecId = SearchGlobalParameter();
			nlapiLogExecution('DEBUG', 'vendor credit ', "i_AitGlobalRecId" + i_AitGlobalRecId);
			
			if (i_AitGlobalRecId != 0) 
			{
				var o_AitGloRec = nlapiLoadRecord('customrecord_indian_tax_glo_parameter', i_AitGlobalRecId);
				
				var a_subisidiary = new Array()
				a_subisidiary = o_AitGloRec.getFieldValues('custrecord_ait_subsidiary');
				nlapiLogExecution('DEBUG', 'Bill ', "a_subisidiary->" + a_subisidiary);
				
				
			}// end if(i_AitGlobalRecId != 0 )
			var vendorcredit_id = nlapiGetRecordId()
			nlapiLogExecution('DEBUG', 'disable vendor credit void button', "vendor credit id " + vendorcredit_id)
			
			var recordType = nlapiGetRecordType()
			nlapiLogExecution('DEBUG', 'disable vendor credit void button', " recordType " + recordType)
			
			var o_billcredit_obj = nlapiLoadRecord(recordType, vendorcredit_id)
			nlapiLogExecution('DEBUG', 'disable vendor credit void button', " recordType " + recordType)
			
			if (o_billcredit_obj != null && o_billcredit_obj != '' && o_billcredit_obj != undefined) 
			{
				//var a_subsidiary1 = a_subisidiary.toString();
				var Flag = 0;
				
				if (a_subisidiary[1] != undefined && a_subisidiary[1] != null && a_subisidiary[1] != '') 
				{
				
					for (var y = 0; y < a_subisidiary.length; y++) {
						var i_subsidiary = o_billcredit_obj.getFieldValue('subsidiary')
						
						//==== IF SUBSIDIARY IS INDIAN SUBSIDIARY ====
						if (a_subisidiary[y] == i_subsidiary) {
							Flag = 1;
							break;
						}// END if (a_subisidiary[y] == i_subsidiary)
					}// END for (var y = 0; y < a_subisidiary.length; y++)
				}// END  if (a_subsidiary1.indexOf(',') > -1)
				else 
				{
					// === IF COMMA NOT FOUND ===
					var i_subsidiary = o_billcredit_obj.getFieldValue('subsidiary')
					
					//==== IF SUBSIDIARY IS INDIAN SUBSIDIARY ====
					if (a_subisidiary[0] == i_subsidiary) 
					{
						Flag = 1;
					}// END  if (a_subsidiary1 == i_subsidiary)
				}// END  if (a_subsidiary1 == i_subsidiary)
				//===== FOR SINGLE SUBSIDARY ACCOUNT  =======
				var context = nlapiGetContext();
				var i_subcontext = context.getFeature('SUBSIDIARIES');
				if (i_subcontext == false) 
				{
					Flag = 1;
				}
				
				
				if (Flag == 1) 
				{
				
					var check_refno = o_billcredit_obj.getFieldValue('custbody_tds_advance_check')
					
					if (check_refno != null && check_refno != '' && check_refno != undefined) 
					{
						var b_Adavncetoved = nlapiLookupField('check', check_refno, 'custbody_tds_advancepay')
						
						if (b_Adavncetoved == 'T') 
						{
							var void_btn = form.getButton('void')
							//nlapiLogExecution('DEBUG', 'Beforeload', 'Button disabled vendor credit===>'+void_btn)
							void_btn.setDisabled(true)
							nlapiLogExecution('DEBUG', 'Beforeload', 'Button disabled vendor credit===>')
							
						}
					}
				}
			}
		}
		
					
				
		
	}

	return true;



}

// END BEFORE LOAD ====================================================





// BEGIN BEFORE SUBMIT ================================================

function beforeSubmitRecord_uncheck_advance_tds(type)
{
    /*  On before submit:

          - PURPOSE
		-

          FIELDS USED:

          --Field Name--				--ID--


    */


    //  LOCAL VARIABLES


    //  BEFORE SUBMIT CODE BODY
	var recordType = nlapiGetRecordType()
	if (recordType == 'vendorcredit') 
	{
		var i_AitGlobalRecId = SearchGlobalParameter();
		nlapiLogExecution('DEBUG', 'Bill ', "i_AitGlobalRecId" + i_AitGlobalRecId);
		
		if (i_AitGlobalRecId != 0) 
		{
			var o_AitGloRec = nlapiLoadRecord('customrecord_indian_tax_glo_parameter', i_AitGlobalRecId);
			
			var a_subisidiary = new Array()
			a_subisidiary = o_AitGloRec.getFieldValues('custrecord_ait_subsidiary');
			nlapiLogExecution('DEBUG', 'Bill ', "a_subisidiary->" + a_subisidiary);
		}
		// end if(i_AitGlobalRecId != 0 )
		
		
		//var a_subsidiary1 = a_subisidiary.toString();
		var Flag = 0;
		
		if (a_subisidiary[1] != undefined && a_subisidiary[1] != null && a_subisidiary[1] != '') 
		{
		
			for (var y = 0; y < a_subisidiary.length; y++) {
				var i_subsidiary = nlapiGetFieldValue('subsidiary')
				
				//==== IF SUBSIDIARY IS INDIAN SUBSIDIARY ====
				if (a_subisidiary[y] == i_subsidiary) 
				{
					Flag = 1;
					break;
				}// END if (a_subisidiary[y] == i_subsidiary)
			}// END for (var y = 0; y < a_subisidiary.length; y++)
		}// END  if (a_subsidiary1.indexOf(',') > -1)
		else 
		{
			// === IF COMMA NOT FOUND ===
			var i_subsidiary = nlapiGetFieldValue('subsidiary')
			
			//==== IF SUBSIDIARY IS INDIAN SUBSIDIARY ====
			if (a_subisidiary[0] == i_subsidiary) {
				Flag = 1;
			}// END  if (a_subsidiary1 == i_subsidiary)
		}// END  if (a_subsidiary1 == i_subsidiary)
		//===== FOR SINGLE SUBSIDARY ACCOUNT  =======
		var context = nlapiGetContext();
		var i_subcontext = context.getFeature('SUBSIDIARIES');
		if (i_subcontext == false) 
		{
			Flag = 1;
		}
		
		if (Flag == 1) 
		{
		
			var advance_tds_vendor = nlapiGetFieldValue('custbody_advance_tds_vendor')
			
			if (advance_tds_vendor == 'T') {
				var unapplied_amt = nlapiGetFieldValue('unapplied')
				
				if (parseFloat(unapplied_amt) == parseFloat(0)) 
				{
					nlapiSetFieldValue('custbody_advance_tds_vendor', 'F')
				}
			}
			
		}
		
	}

	return true;

}

// END BEFORE SUBMIT ==================================================





// BEGIN AFTER SUBMIT =============================================

function afterSubmitRecord(type)
{
    /*  On after submit:

          - PURPOSE
		-

	FIELDS USED:

          --Field Name--				--ID--


    */


	//  LOCAL VARIABLES



	//  AFTER SUBMIT CODE BODY

	return true;

}

// END AFTER SUBMIT ===============================================





// BEGIN FUNCTION ===================================================
{
// == FUNCTION FOR SEARCHING SUBSIDIARY GOLBAL PARAMETER ==
    function SearchGlobalParameter()	
	{
    
        var a_filters = new Array();
        var a_column = new Array();
        var i_globalRecId = 0;
        
        a_column.push(new nlobjSearchColumn('internalid'));
        
        var s_serchResult = nlapiSearchRecord('customrecord_indian_tax_glo_parameter', null, null, a_column)
        
        if (s_serchResult != null && s_serchResult != undefined && s_serchResult != '') 
		{
            for (var i = 0; i < s_serchResult.length; i++) 
			{
                i_globalRecId = s_serchResult[0].getValue('internalid');
                nlapiLogExecution('DEBUG', 'Bill ', "i_globalRecId" + i_globalRecId);
                
            }
            
        }
        
        
        return i_globalRecId;
    }
    



}
// END FUNCTION =====================================================
