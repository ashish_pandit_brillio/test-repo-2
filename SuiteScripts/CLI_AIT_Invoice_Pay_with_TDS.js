// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
/*
   	Script Name:CLI_AIT_Invoice_Pay_with_TDS.js
	Author:		Nikhil jain
	Company:	Aashna cloudtech Pvt. Ltd.
	Date:		26 Oct 2015
    Description:validate invoice tds deduction form


	Script Modification Log:

	-- Date --			-- Modified By --				--Requested By--				-- Description --




	Below is a summary of the process controls enforced by this script file.  The control logic is described
	more fully, below, in the appropriate function headers and code blocks.

     PAGE INIT
		- pageInit(type)


     SAVE RECORD
		- saveRecord()


     VALIDATE FIELD
		- validateField(type, name, linenum)


     FIELD CHANGED
		- fieldChanged(type, name, linenum)


     POST SOURCING
		- postSourcing(type, name)


	LINE INIT
		- lineInit(type)


     VALIDATE LINE
		- validateLine()


     RECALC
		- reCalc()


     SUB-FUNCTIONS
		- The following sub-functions are called by the above core functions in order to maintain code
            modularization:





*/
}
// END SCRIPT DESCRIPTION BLOCK  ====================================



// BEGIN SCRIPT UPDATION BLOCK  ====================================
/*


*/
// END SCRIPT UPDATION BLOCK  ====================================




// BEGIN GLOBAL VARIABLE BLOCK  =====================================
{

	//  Initialize any Global Variables, in particular, debugging variables...


}
// END GLOBAL VARIABLE BLOCK  =======================================





// BEGIN PAGE INIT ==================================================

function pageInit_disable_linefield(type)
{
    /*  On page init:

	- PURPOSE

		FIELDS USED:

		--Field Name--				--ID--			--Line Item Name--

    */

    //  LOCAL VARIABLES


    //  PAGE INIT CODE BODY
	
	//nlapiDisableLineItemField('invoice_line_items', 'tdsamount',true)
}

// END PAGE INIT ====================================================





// BEGIN SAVE RECORD ================================================

function saveRecord_validateTDSInvoices()
{
    /*  On save record:

	- PURPOSE



	  FIELDS USED:

		--Field Name--			--ID--		--Line Item Name--


    */


    //  LOCAL VARIABLES



    //  SAVE RECORD CODE BODY
	
	var i_linecount = nlapiGetLineItemCount('invoice_line_items');
	
	var k = parseFloat(0);
	
	for (var i = 1; i <= i_linecount; i++) 
	{
		var apply = nlapiGetLineItemValue('invoice_line_items', 'invoiceapply', i)
			
			if (apply == 'T') 
			{
				k = (parseFloat(k) + parseFloat(1))
				
				if(k > 20)
				{
					alert('You are allowed to deduct TDS of 20 Invoices in one time');
					return false;
				}
			}
	}


	return true;

}

// END SAVE RECORD ==================================================





// BEGIN VALIDATE FIELD =============================================

function validateField(type, name, linenum)
{

	/*  On validate field:

          - EXPLAIN THE PURPOSE OF THIS FUNCTION


          FIELDS USED:

          --Field Name--				--ID--


    */


	//  LOCAL VARIABLES



	//  VALIDATE FIELD CODE BODY


	return true;

}

// END VALIDATE FIELD ===============================================





// BEGIN FIELD CHANGED ==============================================

function fieldChanged_setTDSRate(type, name, linenum)
{
	
    /*  On field changed:

          - PURPOSE


          FIELDS USED:

			--Field Name--				--ID--


    */


    //  LOCAL VARIABLES


    //  FIELD CHANGED CODE BODY

	//alert('Field Name='+name+'\nLine Item Type='+type+'\nField Value='+nlapiGetFieldValue(name)+'\nLine Item Value='+nlapiGetLineItemValue(type, name, 1));
	if (name == 'custpage_customer') 
	{
		var customer = nlapiGetFieldValue('custpage_customer')
		
		var fromDate = nlapiGetFieldValue('custpage_fromdate')
		
		var toDate = nlapiGetFieldValue('custpage_todate')
		
		var tdsrecivable = nlapiGetFieldValue('custpage_tdsrecivable')
		
		var tdspercent = nlapiGetFieldValue('custpage_tdspercent')
		
		var location = nlapiGetFieldValue('location')
		
		var s_class = nlapiGetFieldValue('class')
		
		var department = nlapiGetFieldValue('department')
		
		var url = nlapiResolveURL('SUITELET', 'customscript_ait_invoice_pay_with_tds', '1')
		
		var urlwithparam = url + '&customer=' + customer + '&fromDate=' + fromDate + '&toDate=' + toDate + '&tdsrecivable=' + tdsrecivable + '&tdspercent=' + tdspercent + '&department=' + department + '&s_class=' + s_class + '&location=' + location;
		
		window.onbeforeunload = null;
		window.open(urlwithparam, '_self')
	}
	
	if(name == 'custpage_tdsrecivable')
	{
		var tdsrecivable = nlapiGetFieldValue('custpage_tdsrecivable');
		
		if(tdsrecivable != null && tdsrecivable != '' && tdsrecivable != undefined)
		{
			var tdsrate = nlapiLookupField('customrecord_tdsmaster',tdsrecivable,'custrecord_netper')
			
			nlapiSetFieldValue('custpage_tdspercent',tdsrate,true,true);
		}
		
		
	}
	if (name == 'custpage_tdspercent') 
	{
		var tdsRoundMethod = nlapiGetFieldValue('custpage_tdsround');
		
		var totalamount = parseFloat(0);
	
		var totaltds = parseFloat(0);
	
		var i_linecount = nlapiGetLineItemCount('invoice_line_items');
		
		var tdspercent = nlapiGetFieldValue('custpage_tdspercent')
		
		for (var i = 1; i <= i_linecount; i++) 
		{
			var apply = nlapiGetLineItemValue('invoice_line_items', 'invoiceapply', i)
			
			if (apply == 'T') 
			{
				var amount = nlapiGetLineItemValue('invoice_line_items', 'baseamount', i)
				
				var tdsamount = parseFloat(0);
				
				if (tdspercent != null && tdspercent != '' && tdspercent != undefined) 
				{
					var a_tdspercent = tdspercent.split('%')
					var rate = a_tdspercent[0];
					//alert('rate'+rate)
					tdsamount = parseFloat(amount * (parseFloat(rate) / 100))
					tdsamount = parseFloat(tdsamount)
					tdsamount = tdsamount.toFixed(2)
					tdsamount = applyTdsRoundMethod(tdsRoundMethod,tdsamount)	
					
				}
				totaltds = (parseFloat(totaltds) + parseFloat(tdsamount))
				
				nlapiSetLineItemValue('invoice_line_items', 'tdsamount', i, tdsamount)
				
				totalamount = (parseFloat(totalamount) + parseFloat(amount))
				
				nlapiSetLineItemValue('invoice_line_items', 'paymentbaseamount', i, amount)
			}
			
		}
		
		nlapiSetFieldValue('custpage_total_amt', totalamount.toFixed(2));
			
		nlapiSetFieldValue('custpage_total_tds_amt', totaltds.toFixed(2));
	}
	
	if (name == 'invoiceapply') {
		
		var tdsRoundMethod = nlapiGetFieldValue('custpage_tdsround');
			
		var tdsamount = parseFloat(0);
		
		var apply = nlapiGetLineItemValue('invoice_line_items', 'invoiceapply', linenum)
		
		var totalamount = nlapiGetFieldValue('custpage_total_amt')
		
		var totaltds = nlapiGetFieldValue('custpage_total_tds_amt')
		
		if (apply == 'T') {
			var amount = nlapiGetLineItemValue('invoice_line_items', 'baseamount', linenum)
			
			var tdspercent = nlapiGetFieldValue('custpage_tdspercent')
			
			if (tdspercent != null && tdspercent != '' && tdspercent != undefined) {
				var a_tdspercent = tdspercent.split('%')
				var rate = a_tdspercent[0];
				//alert('rate'+rate)
				tdsamount = parseFloat(amount * (parseFloat(rate) / 100))
				tdsamount = parseFloat(tdsamount)
				tdsamount = tdsamount.toFixed(2)
				tdsamount = applyTdsRoundMethod(tdsRoundMethod,tdsamount)	
				
			}
			
			totaltds = (parseFloat(totaltds) + parseFloat(tdsamount))
			
			totalamount = (parseFloat(totalamount) + parseFloat(amount))
			
			nlapiSetFieldValue('custpage_total_amt', totalamount.toFixed(2));
			
			nlapiSetFieldValue('custpage_total_tds_amt', totaltds.toFixed(2));
			
			nlapiSetLineItemValue('invoice_line_items', 'paymentbaseamount', linenum, amount, false, false)
			
			nlapiSetLineItemValue('invoice_line_items', 'tdsamount', linenum, tdsamount, false, false)
		}
		else {
			var amount = nlapiGetLineItemValue('invoice_line_items', 'paymentbaseamount', linenum)
			
			var tdsamount = nlapiGetLineItemValue('invoice_line_items', 'tdsamount', linenum)
			
			totaltds = (parseFloat(totaltds) - parseFloat(tdsamount));
			
			totalamount = (parseFloat(totalamount) - parseFloat(amount));
			
			nlapiSetFieldValue('custpage_total_tds_amt', totaltds.toFixed(2));
			
			nlapiSetFieldValue('custpage_total_amt', totalamount.toFixed(2));
			
			nlapiSetLineItemValue('invoice_line_items', 'paymentbaseamount', linenum, '', false, false)
			
			nlapiSetLineItemValue('invoice_line_items', 'tdsamount', linenum, '', false, false)
		}
	}
	
	if (name == 'paymentbaseamount') 
	{
		
		var tdsRoundMethod = nlapiGetFieldValue('custpage_tdsround');
			
		var totaltds = parseFloat(0);
		
		var totalamount = parseFloat(0);

		var i_linecount = nlapiGetLineItemCount('invoice_line_items');
		
		var tdspercent = nlapiGetFieldValue('custpage_tdspercent')
			
		for (var i = 1; i <= i_linecount; i++) 
		{
			var apply = nlapiGetLineItemValue('invoice_line_items', 'invoiceapply', i)
			
			if (apply == 'T') 
			{
				//alert('hello in amount paid')
				var amount = nlapiGetLineItemValue('invoice_line_items', 'paymentbaseamount', i)
				
				var tdsamount = parseFloat(0);
				
				if (tdspercent != null && tdspercent != '' && tdspercent != undefined) 
				{
					var a_tdspercent = tdspercent.split('%')
					var rate = a_tdspercent[0];
					//alert('rate'+rate)
					tdsamount = parseFloat(amount * (parseFloat(rate) / 100))
					tdsamount = parseFloat(tdsamount)
					tdsamount = tdsamount.toFixed(2)
					tdsamount = applyTdsRoundMethod(tdsRoundMethod,tdsamount)	
				}
				totaltds = (parseFloat(totaltds) + parseFloat(tdsamount))
				
				nlapiSetLineItemValue('invoice_line_items', 'tdsamount', i, tdsamount)
				
				totalamount = (parseFloat(totalamount) + parseFloat(amount))
				//alert('amount'+amount)
				nlapiSetLineItemValue('invoice_line_items', 'paymentbaseamount', i, amount)
			}
		}
		nlapiSetFieldValue('custpage_total_amt', totalamount.toFixed(2));
		
		nlapiSetFieldValue('custpage_total_tds_amt', totaltds.toFixed(2));
	}
}

// END FIELD CHANGED ================================================





// BEGIN POST SOURCING ==============================================

function postSourcing(type, name)
{

    /*  On post sourcing:

	- PURPOSE

          FIELDS USED:

		--Field Name--			--ID--		--Line Item Name--


    */

    //  LOCAL VARIABLES


    //  POST SOURCING CODE BODY



}

// END POST SOURCING ================================================



// BEGIN LINE INIT ==============================================

function lineInit(type)
{

    /*  On Line Init:

	- PURPOSE

          FIELDS USED:

		--Field Name--			--ID--		--Line Item Name--


    */

    //  LOCAL VARIABLES


    //  LINE INIT CODE BODY



}

// END LINE INIT ================================================


// BEGIN VALIDATE LINE ==============================================

function validateLine(type)
{

	/*  On validate line:

          - EXPLAIN THE PURPOSE OF THIS FUNCTION


          FIELDS USED:

          --Field Name--				--ID--


    */


	//  LOCAL VARIABLES


	//  VALIDATE LINE CODE BODY


	return true;

}

// END VALIDATE LINE ================================================




// BEGIN RECALC =====================================================

function recalc(type)
{

	/*  On recalc:

          - EXPLAIN THE PURPOSE OF THIS FUNCTION


          FIELDS USED:

          --Field Name--				--ID--


    */


	//  LOCAL VARIABLES


	//  RECALC CODE BODY


}

// END RECALC =======================================================




// BEGIN FUNCTION ===================================================
function bulkpaymarkall(){
	var totalamount = parseFloat(0);
	
	var totaltds = parseFloat(0);
	
	var tdspercent = nlapiGetFieldValue('custpage_tdspercent')
	
	var tdsRoundMethod = nlapiGetFieldValue('custpage_tdsround');
	
	var i_linecount = nlapiGetLineItemCount('invoice_line_items');
	
	for (var i = 1; i <= i_linecount; i++) {
		nlapiSetLineItemValue('invoice_line_items', 'invoiceapply', i, 'T')
		
		var amount = nlapiGetLineItemValue('invoice_line_items', 'baseamount', i)
		
		//amount = amount.toFixed(2)
		
		
		var tdsamount = parseFloat(0);
		
		if (tdspercent != null && tdspercent != '' && tdspercent != undefined) {
		
		
			var a_tdspercent = tdspercent.split('%')
			var rate = a_tdspercent[0];
			
			tdsamount = parseFloat(amount * (parseFloat(rate) / 100))
			
			tdsamount = parseFloat(tdsamount)
			
			tdsamount = tdsamount.toFixed(2)
			
			tdsamount = applyTdsRoundMethod(tdsRoundMethod, tdsamount)
			
		}
		totaltds = (parseFloat(totaltds) + parseFloat(tdsamount))
		
		nlapiSetLineItemValue('invoice_line_items', 'tdsamount', i, tdsamount)
		
		totalamount = (parseFloat(totalamount) + parseFloat(amount))
		//alert('totalamount'+totalamount)
		nlapiSetLineItemValue('invoice_line_items', 'paymentbaseamount', i, amount)
		
	}
	nlapiSetFieldValue('custpage_total_amt', totalamount.toFixed(2));
	
	nlapiSetFieldValue('custpage_total_tds_amt', totaltds.toFixed(2));
}
function bulkpayunmarkall()
{
	var i_linecount = nlapiGetLineItemCount('invoice_line_items');
	
	for(var i = 1;i<=i_linecount ;i++)
	{
			nlapiSetLineItemValue('invoice_line_items', 'invoiceapply', i,'F')
			nlapiSetLineItemValue('invoice_line_items', 'paymentbaseamount', i, '')
			nlapiSetLineItemValue('invoice_line_items', 'tdsamount', i, '')
	}
	nlapiSetFieldValue('custpage_total_amt',0);
	
	nlapiSetFieldValue('custpage_total_tds_amt',0);
}

function customerinvoicesearchcritera()
{
	var customer = nlapiGetFieldValue('custpage_customer')

	var fromDate = nlapiGetFieldValue('custpage_fromdate')
	
	var toDate = nlapiGetFieldValue('custpage_todate')
	
	var tdsrecivable = nlapiGetFieldValue('custpage_tdsrecivable')
	
	var tdspercent = nlapiGetFieldValue('custpage_tdspercent')
	
	var location = nlapiGetFieldValue('location')
	
	var s_class = nlapiGetFieldValue('class')
	
	var department = nlapiGetFieldValue('department')
	
	var url = nlapiResolveURL('SUITELET', 'customscript_ait_invoice_pay_with_tds', '1')
	
	var urlwithparam = url + '&customer=' + customer + '&fromDate=' + fromDate + '&toDate=' + toDate + '&tdsrecivable=' + tdsrecivable + '&tdspercent=' + tdspercent+ '&department=' + department+ '&s_class=' + s_class+ '&location=' + location;
	
	window.onbeforeunload = null;
	window.open(urlwithparam,'_self')
}

function customerInvoiceCriteriaRefersh()
{
	var url = nlapiResolveURL('SUITELET', 'customscript_ait_invoice_pay_with_tds', '1')
	
	window.open(url,'_self')
}
function applyTdsRoundMethod(tdsRoundMethod,tdsAmount)
{
	var roundedtdsAmount = tdsAmount;
	
	if(tdsRoundMethod == 2)
	{
		roundedtdsAmount = Math.round(tdsAmount)
	}
	if(tdsRoundMethod == 3)
	{
		roundedtdsAmount = Math.round(tdsAmount/10)*10;
	}
	if(tdsRoundMethod == 4)
	{
		roundedtdsAmount = Math.round(tdsAmount/100)*100;
	}
	
	return roundedtdsAmount;
}


// END FUNCTION =====================================================
