/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       18 Mar 2019     Aazamali Khan
 *
 */

/**
 * @param {String} type Context Types: scheduled, ondemand, userinterface, aborted, skipped
 * @returns {Void}
 */
 function scheduled(type) {
     try {
        var dataArray = null;
        var projectId = " ";
    
        dataArray = call_sfdc();
        if (dataArray) {
            deleteRecord();
            //nlapiLogExecution("DEBUG", "dataArray : ", JSON.stringify(dataArray[0]));
            
            var allOpp = AllOpportunity();
            
            for (var j = 0; j < dataArray.length; j++) {
                CheckMetering();
                var revenueDataRec = nlapiCreateRecord("customrecord_fuel_sfdc_revenue_data");
                revenueDataRec.setFieldValue("custrecord_sfdc_revenue_data_stage", dataArray[j].Stage);
               // nlapiLogExecution("DEBUG", "dataArray[j].Stage : ", dataArray[j].Stage);
                revenueDataRec.setFieldValue("custrecord_sfdc_revenue_data_sowid", dataArray[j].SOWID);
                var startDate = nlapiStringToDate(dateFormat(dataArray[j].ProjectStartDate));
               // nlapiLogExecution("DEBUG", "startDate", startDate);
                revenueDataRec.setFieldValue("custrecord_sfdc_revenue_data_startdate", startDate);
                var endDate = nlapiStringToDate(dateFormat(dataArray[j].ProjectEndDate));
               // nlapiLogExecution("DEBUG", "endDate", endDate);
               nlapiLogExecution("DEBUG", "oppIdRef", dataArray[j].OppId);
               var projectId = dataArray[j].ProjectId;
               if(projectId && dataArray[j].ProjectionStatus == "Most Likely"&& dataArray[j].Stage =="Closed Won"){
                    var projectStatus = getProjectStatus(projectId)
                    if(projectStatus == "F"){
                     projectId = projectId
                    }
                    else{
                     projectId = " ";
                    }
                   
               }
    
               var closeDate = nlapiStringToDate(dateFormat(dataArray[j].CloseDate));
               var mostLikleyDate = nlapiStringToDate(dateFormat(dataArray[j].MostLikelyDate));
                revenueDataRec.setFieldValue("custrecord_sfdc_revenue_data_enddate", endDate);
                revenueDataRec.setFieldValue("custrecord_sfdc_revenue_data_prostatus", dataArray[j].ProjectionStatus);
                revenueDataRec.setFieldValue("custrecord_sfdc_revenue_data_projectid", projectId);
                revenueDataRec.setFieldValue("custrecord_sfdc_revenue_data_oppid", dataArray[j].OppId);
                revenueDataRec.setFieldValue("custrecord_sfdc_revenue_data_netcustid", dataArray[j].NetsuiteCustomerId);
                revenueDataRec.setFieldValue("custrecord_sfdc_revenue_data_accregion", dataArray[j].AccountRegion);
                revenueDataRec.setFieldValue("custrecord_sfdc_revenue_data_accname", dataArray[j].AccountName);
                
                var parent_opp_id='';
                       var parent_opp = dataArray[j].MasterOppId;
                       if(_logValidation(parent_opp)){				          			  
                          var parent_opp_s = allOpp[parent_opp];
                          if(_logValidation(parent_opp_s)){ 
                             parent_opp_id = parent_opp_s;
                             revenueDataRec.setFieldValue("custrecord_sfdc_revenue_parent_opp_id", parent_opp_id );
                              
                          }
                                                              
                        }	
                
                var id = nlapiSubmitRecord(revenueDataRec);
                var subDataArray = dataArray[j].ForecastRevenue;
                for (var i = 0; i < subDataArray.length; i++) {
                    CheckMetering();
                    var dataRec = nlapiCreateRecord("customrecord_sfdc_forecast_revenue_child");
                    var dateForecast = subDataArray[i].ForcastDate;
                    var dateArray = dateForecast.split("-");
                    var dateString = dateArray[1] + "/01/" + dateArray[0];
                    var practiceSearch = getPracticeId(subDataArray[i].Practice);
                    dataRec.setFieldValue("custrecord_sfdc_fuel_forecast_rev_ext", id);
                    if(practiceSearch){
                        dataRec.setFieldValue("custrecord_sfdc_fuel_forecast_rev_pract", practiceSearch[0].getId());	
                    }
                    dataRec.setFieldValue("custrecord_sfdc_fuel_forecast_rev_moyear", subDataArray[i].ForcastMonthYear);
                    dataRec.setFieldValue("custrecord_sfdc_fuel_fore_modayyear", dateString);
                    dataRec.setFieldValue("custrecord_sfdc_fuel_forecast_rev_amount", subDataArray[i].Amount);
                    nlapiSubmitRecord(dataRec);
                }
                CheckMetering()
    
                var oppIdRef = dataArray[j].OppId;
                //nlapiLogExecution("DEBUG", "oppIdRef", oppIdRef);
                var searchCount = getSearchCount(oppIdRef);
                if(searchCount == 0){
                    var amount = dataArray[j].TCV;
                    var type = dataArray[j].Type;
                    var stage = dataArray[j].Stage;
                    var projectionStatus = dataArray[j].ProjectionStatus;
                    var practice = dataArray[j].OrganizationPractice;
                    var oppName = dataArray[j].OppName;
                    var customerId = dataArray[j].NetsuiteCustomerId;
    
                    var confidence = dataArray[j].Confidence;
                    var accountRegion = dataArray[j].AccountRegion;
                    var accountType = dataArray[j].AccountType;
                    var fulfillmentRaised = dataArray[j].FulfilmentPlanRaised;
                    if(fulfillmentRaised!=null){
                        if(fulfillmentRaised == true){
                            var fulfillmentRaisedValue = "T";
                        }
                        else{
                            var fulfillmentRaisedValue = "F";
                        }
                    }
                   var ownerEmail = dataArray[j].AccountOwnerEmail;
                    var clientPartnerId = getUserUsingEmailId(ownerEmail, practice);
                    var sfdcRec = nlapiCreateRecord("customrecord_sfdc_opportunity_record");
                    sfdcRec.setFieldValue("custrecord_opportunity_name_sfdc", oppName);
                    sfdcRec.setFieldValue("custrecord_opportunity_id_sfdc", oppIdRef);
                    sfdcRec.setFieldValue("custrecord_practice_sfdc", practice);
                    sfdcRec.setFieldText("custrecord_projection_status_sfdc", projectionStatus);
                    sfdcRec.setFieldValue("custrecord_confidence_sfdc", confidence);
                    sfdcRec.setFieldValue("custrecord_start_date_sfdc", startDate);
                    sfdcRec.setFieldValue("custrecord_end_date_sfdc", endDate);
                    sfdcRec.setFieldValue("custrecord_close_date_sfdc", closeDate);
                    sfdcRec.setFieldValue("custrecord_account_type_sfdc", accountType);
                    if(customerId){
                        sfdcRec.setFieldValue("custrecord_customer_sfdc", customerId);
                    }
                    sfdcRec.setFieldText("custrecord_stage_sfdc", stage);
                    if(projectId){
                        sfdcRec.setFieldValue("custrecord_project_sfdc", projectId);
                    }
                    sfdcRec.setFieldValue("custrecord_opp_account_region", accountRegion);
                    sfdcRec.setFieldValue("custrecord_tcv_sfdc", amount);
                    sfdcRec.setFieldValue("custrecord_sfdc_client_partner", clientPartnerId);
                    sfdcRec.setFieldValue("custrecord_opp_type_sfdc", type);
                    if (projectionStatus == "Most Likely") {
    
                        if (!_logValidation(projectId)) {
                            sfdcRec.setFieldValue("custrecord_most_likely_date_sfdc", mostLikleyDate);
    
                        } else {
    
                            if (_logValidation(mostLikleyDate)) {
    
                                sfdcRec.setFieldValue("custrecord_most_likely_date_sfdc", mostLikleyDate);
    
                            } 
                        }
    
                    }
                        sfdcRec.setFieldValue("custrecord_sfdc_parent_opp_id", parent_opp_id);
                    sfdcRec.setFieldValue("custrecord_fulfilment_plan_sfdc",fulfillmentRaisedValue)
                        var sfdcId = nlapiSubmitRecord(sfdcRec);
                        nlapiLogExecution("DEBUG", "sfdcId", sfdcId);  
                }
            
            }
        }
     } catch (error) {
        nlapiLogExecution("ERROR", error.name,error.message);
     }
}
function getProjectStatus(projectId){
    try {
        var jobSearchObj =  nlapiSearchRecord("job",null,
        [
           ["entityid","is",projectId]
        ], 
        [
           new nlobjSearchColumn("isinactive")
        ]
        );
    
        var status = jobSearchObj[0].getValue("isinactive")
        return status;
    } catch (error) {
        nlapiLogExecution("ERROR", error.name,error.message);
    }
}
function getUserUsingEmailId(emailId, practice) {
    try {
        if (practice == "Cognetik") {

            var prefix_email = emailId.split("@")[0];
            var employeeSearch = nlapiSearchRecord('employee', null, [
                new nlobjSearchFilter('email', null, 'contains', prefix_email),
                new nlobjSearchFilter('custentity_employee_inactive', null, 'is', 'F')
            ]);
        } else {
            var employeeSearch = nlapiSearchRecord('employee', null, [
                new nlobjSearchFilter('email', null, 'is', emailId),
                new nlobjSearchFilter('custentity_employee_inactive', null, 'is', 'F')
            ]);
        }
        if(employeeSearch){
            return employeeSearch[0].getId();
        }
           else{
               return ''
           } 
    } catch (err) {
        nlapiLogExecution('ERROR', 'getUserUsingEmailId', err.message);
    }
}

function getSearchCount(oppIdRef){
    try {
        var customrecord_sfdc_opportunity_recordSearch = nlapiSearchRecord("customrecord_sfdc_opportunity_record",null,
[
   ["custrecord_opportunity_id_sfdc","is",oppIdRef]
], 
[
   new nlobjSearchColumn("internalid")
]
);
if(customrecord_sfdc_opportunity_recordSearch == null){
    var searchLength = 0; 
}
else{
    var searchLength = customrecord_sfdc_opportunity_recordSearch.length;
}
return searchLength;
    } catch (error) {
        nlapiLogExecution('ERROR',error.name,error.message)
    }
}

function dateFormat(date) {
    try {
        var date_return = '';
        if (date) {
            var dateObj = date.split('-');
            var month = dateObj[1];
            var year = dateObj[0];
            var date_d = dateObj[2];
    
            date_return = month + '/' + date_d + '/' + year;
        }
        return date_return;
    } catch (error) {
        nlapiLogExecution('ERROR',error.name,error.message)
    }
}

function call_sfdc() {
    try {
        //Start of SuiteScript
        var accessURL = '';
        if(nlapiGetContext().getEnvironment()=="PRODUCTION"){
           accessURL = getAccessToken(); // for production
        }  
        else if(nlapiGetContext().getEnvironment()=="SANDBOX") {
            accessURL = Staging_getAccessToken(); // for sandbox
        }
	   
        var method = 'POST';


        var response = nlapiRequestURL(accessURL, null, null, method);
        var temp = response.body;
        //nlapiLogExecution('DEBUG', 'JSON', temp);
        var data = JSON.parse(response.body);
        var access_token_SFDC = data.access_token;
        var instance_URL_Sfdc = data.instance_url;
        //nlapiLogExecution('DEBUG', 'JSON', data);

        var dynamic_URL = instance_URL_Sfdc + '/services/apexrest/MLBKDOppDetails/v1.0/';

        var headers = {
            "Authorization": "Bearer " + access_token_SFDC,
            "Content-Type": "application/json",
            "accept": "application/json"
        };
        //
        var method_ = 'POST';
        var dataRows = {};
        var response_ = nlapiRequestURL(dynamic_URL, dataRows, headers, method_);
        var data = JSON.parse(response_.body);
        var message = data[0].message;
        //nlapiLogExecution('DEBUG', 'JSON', message);
        //nlapiLogExecution('DEBUG', 'JSON', JSON.stringify(dataRows));
        return data;
    } catch (e) {
        nlapiLogExecution('DEBUG', 'SFDC Call error', e);

    }
}

function CheckMetering() {
    var CTX = nlapiGetContext();
    var START_TIME = new Date().getTime();
    //	want to try to only check metering every 15 seconds
    var remainingUsage = CTX.getRemainingUsage();
    var currentTime = new Date().getTime();
    var timeDifference = currentTime - START_TIME;
    //	changing to 15 minutes, should cause little if any impact, but willmake runaway scripts faster to kill
    if (remainingUsage < 800 || timeDifference > 900000) {
        START_TIME = new Date().getTime();
        var status = nlapiYieldScript();
        nlapiLogExecution('AUDIT', 'STATUS = ', JSON.stringify(status));
    }
}

function deleteRecord() {
	//nlapiLogExecution("DEBUG","delete","start");
    var customrecord_fuel_sfdc_revenue_dataSearch = searchRecord("customrecord_fuel_sfdc_revenue_data", null,
        null,null,null);
    
    if (customrecord_fuel_sfdc_revenue_dataSearch) {
        for (var i = 0; i < customrecord_fuel_sfdc_revenue_dataSearch.length; i++) {
            //nlapiLogExecution("DEBUG","record count :",customrecord_fuel_sfdc_revenue_dataSearch.length);
			CheckMetering();
            var internalId = customrecord_fuel_sfdc_revenue_dataSearch[i].getId();
            //nlapiLogExecution("DEBUG","internalId :",internalId);
			var customrecord_sfdc_forecast_revenue_childSearch = nlapiSearchRecord("customrecord_sfdc_forecast_revenue_child", null,
                [
                    ["custrecord_sfdc_fuel_forecast_rev_ext", "anyof", internalId]
                ],
                []
            );
            if (customrecord_sfdc_forecast_revenue_childSearch) {
                for (var j = 0; j < customrecord_sfdc_forecast_revenue_childSearch.length; j++) {
                    nlapiDeleteRecord("customrecord_sfdc_forecast_revenue_child", customrecord_sfdc_forecast_revenue_childSearch[j].getId());
                }
            }
            nlapiDeleteRecord("customrecord_fuel_sfdc_revenue_data", internalId);
        }
    }


    // get the internal ids of sfdc revenue child records
    /*var customrecord_sfdc_forecast_revenue_childSearch = nlapiSearchRecord("customrecord_sfdc_forecast_revenue_child",null,
[
], 
[

]
);
if(customrecord_sfdc_forecast_revenue_childSearch){
	for(var i = 0 ; i < customrecord_sfdc_forecast_revenue_childSearch.length ; i++){
			CheckMetering()
			nlapiDeleteRecord("customrecord_sfdc_forecast_revenue_child",customrecord_sfdc_forecast_revenue_childSearch[0].getId());
	}
}
//  get the internal ids of sfdc revenue records

var customrecord_fuel_sfdc_revenue_dataSearch = nlapiSearchRecord("customrecord_fuel_sfdc_revenue_data",null,
[
], 
[

]
);
if(customrecord_fuel_sfdc_revenue_dataSearch){
	for(var i = 0 ; i < customrecord_fuel_sfdc_revenue_dataSearch.length ; i++){
			CheckMetering()
			nlapiDeleteRecord("customrecord_fuel_sfdc_revenue_data",customrecord_fuel_sfdc_revenue_dataSearch[0].getId());
	}
}*/
}
function getPracticeId(s_practice){
	var departmentSearch = nlapiSearchRecord("department",null,
[
   ["isinactive","is","F"], 
   "AND", 
   ["name","is",s_practice]
], 
[
]
);
return departmentSearch;
}

function AllOpportunity() {
	
    var customrecord_sfdc_opportunity_recordSearch = searchRecord("customrecord_sfdc_opportunity_record", null,
        [
   ["custrecord_sfdc_parent_opp_id","anyof","@NONE@"]
],
        [           
            new nlobjSearchColumn("custrecord_opportunity_id_sfdc")
        ]);
	var oppData = {};	
	
	if(_logValidation(customrecord_sfdc_opportunity_recordSearch)){
		
		for(var i=0; i<customrecord_sfdc_opportunity_recordSearch.length; i++){
			
			var opp = customrecord_sfdc_opportunity_recordSearch[i].getValue("custrecord_opportunity_id_sfdc");
			var opp_id = customrecord_sfdc_opportunity_recordSearch[i].getId();
			
			oppData[opp] = opp_id;
			
		}	
		
	}	
	
    return oppData;
}

function searchRecord(recordType, savedSearch, arrFilters, arrColumns,
		filterExpression) {

	try {
		var search = null;

		// if a saved search is provided, load it and add the filters and
		// columns
		if (savedSearch) {
			search = nlapiLoadSearch(recordType, savedSearch);

			if (isArrayNotEmpty(arrFilters)) {
				search.addFilters(arrFilters);
			}

			if (isArrayNotEmpty(arrColumns)) {
				search.addColumns(arrColumns);
			}

			if (isArrayNotEmpty(filterExpression)) {
				search.setFilterExpression(filterExpression);
			}
		}
		// create a new search
		else {
			search = nlapiCreateSearch(recordType, arrFilters, arrColumns);
		}

		// run search
		var resultSet = search.runSearch();

		// iterate through the search and get all data 1000 at a time
		var searchResultCount = 0;
		var resultSlice = null;
		var searchResult = [];

		do {
			resultSlice = resultSet.getResults(searchResultCount,
					searchResultCount + 1000);

			if (resultSlice) {

				resultSlice.forEach(function(result) {

					searchResult.push(result);
					searchResultCount++;
				});
			}
		} while (isArrayNotEmpty(resultSlice) && resultSlice.length >= 1000);

		return searchResult;
	} catch (err) {
		nlapiLogExecution('ERROR', 'searchRecord', err);
		throw err;
	}
}

function isEmpty(value) {

	return value == null || value == "" || typeof(value) == undefined;
}

function isNotEmpty(value) {

	return !isEmpty(value);
}

function isArrayEmpty(argArray) {

	return !isArrayNotEmpty(argArray);
}

function isArrayNotEmpty(argArray) {

	return (isNotEmpty(argArray) && argArray.length > 0);
}

function _logValidation(value) {
    if (value != null && value.toString() != null && value != '' && value != undefined && value.toString() != undefined && value != 'undefined' && value.toString() != 'undefined' && value.toString() != 'NaN' && value != NaN) {
        return true;
    } else {
        return false;
    }
}
