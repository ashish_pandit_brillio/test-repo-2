<!doctype html>
<html>
<head>
<title>{{s_title}} - Project Profitability Report</title>
<style>
table {
	border-collapse: separate;
	border-spacing: 0;
	color: #4a4a4d;
	font: 14px/1.4 "Helvetica Neue", Helvetica, Arial, sans-serif;
	margin-left: auto;
	margin-right: auto;
}

th,td {
	padding: 8px 4px;
	vertical-align: middle;
}

thead {
	background: #395870;
	background: linear-gradient(#49708f, #293f50);
	color: #fff;
	font-size: 11px;
	text-transform: uppercase;
}

th:first-child {
	border-top-left-radius: 5px;
	text-align: left;
}

th:last-child {
	border-top-right-radius: 5px;
}

tbody tr:nth-child(even) {
	background: #f0f0f2;
}

td {
	border-bottom: 1px solid #cecfd5;
	border-right: 1px solid #cecfd5;
}

td:first-child {
	border-left: 1px solid #cecfd5;
}

.book-title {
	color: #395870;
	display: block;
}

.text-offset {
	color: #7c7c80;
	font-size: 12px;
}

.item-stock,.item-qty {
	text-align: center;
}

.item-price {
	text-align: right;
}

.item-multiple {
	display: block;
}

tfoot {
	text-align: right;
}

tfoot tr:last-child {
	background: #f0f0f2;
	color: #395870;
	font-weight: bold;
}

tfoot tr:last-child td:first-child {
	border-bottom-left-radius: 5px;
}

tfoot tr:last-child td:last-child {
	border-bottom-right-radius: 5px;
}

a:link {
	text-decoration: none;
}

a:hover {
	text-decoration: underline;
}

body {
	font-family: "Trebuchet MS", "Helvetica", "Arial", "Verdana",
		"sans-serif";
	font-size: 62.5%;
}
</style>
<link
	href="https://system.na1.netsuite.com/core/media/media.nl?id=139623&c=3883006&h=a9f92a7038ecb93bb4b5&_xt=.css"
	rel="stylesheet" type="text/css">
<link rel="stylesheet"
	href="/core/media/media.nl?id=150907&c=3883006&h=3442757ae9937f648ea8&_xt=.css">
<script
	src="/core/media/media.nl?id=150908&c=3883006&h=5b5ef06f2123c6ed1685&_xt=.js"></script>
<script
	src="/core/media/media.nl?id=150909&c=3883006&h=74f6b8f98ae272eaba10&_xt=.js"></script>
<script
	src="/core/media/media.nl?id=150255&c=3883006&h=8f1241963b709c4f59f7&_xt=.js"></script>
<!-- Accounting.js -->

<!--pqSelect dependencies-->
<link rel="stylesheet"
	href="/core/media/media.nl?id=154358&c=3883006&h=c511418ab8d7d0e4477b&_xt=.css" />
<script
	src="/core/media/media.nl?id=154359&c=3883006&h=21f1afeae1334e22142d&_xt=.js"></script>
</head>
<body>
	<div
		style="background-color: #607799; padding: 5px 10px; margin-bottom: 5px;">
		<a href="https://system.na1.netsuite.com/app/center/card.nl"
			style="color: #FFFFFF;"><img
			src="/uirefresh/img/header/icon_home.svg" alt="Home"></a>
	</div>
	<div class="accordion">
		<h3>Filter</h3>
		<div>
			<form action="{{url}}" method="GET">
				<input type="hidden" name="script" id="script"
					value="{{i_script_id}}" /> <input type="hidden" name="deploy"
					id="deploy" value="{{i_deployment_id}}" /> <input type="hidden"
					name="user" id="user" value="{{i_user_id}}" /> <label
					for="project">Project </label><select name="project" id="project"
					multiple="multiple">{{s_project_options}}
				</select> 
				<label
				for="customer">Customer </label><select name="customer" id="customer"
				multiple="multiple">{{s_customer_options}}
			</select>
				<label for="from">From </label><input type="text" name="from"
					id="from" value="{{s_from}}" /> <label for="to">To </label><input
					type="text" name="to" id="to" value="{{s_to}}" /><input
					type="submit" name="go" value="Refresh" />
					</form>
		</div>
	</div>
	<div class="">
		<h3 style="text-align: center;">Revenue vs Cost</h3>
		<div id="chart"></div>
	</div>
	<div class="">
	<h3 style="text-align: center;">Sold Margin Table</h3>
		<div>{{s_margin_indicator_table}}</div>
		<h3 style="text-align: center;">Trend</h3>
		<div>{{s_table}}</div>
		<h3 style="text-align: center;">Cost Indicator (beta)</h3>
		<div>{{s_cost_indicator_table}}</div>
		
		<div style='text-align: center;'>* Contractor Cost is based on
			allocation and not actual billing</div>
			<div style='text-align: center;'>* pm - person month</div>
	</div>

	<script
		src="https://system.na1.netsuite.com/core/media/media.nl?id=139625&c=3883006&h=7b2158408e037652eab7&_xt=.js"
		charset="utf-8"></script>
	<script
		src="https://system.na1.netsuite.com/core/media/media.nl?id=139624&c=3883006&h=e323e15adc99fda7776b&_xt=.js"></script>
	<script>
			var chart = c3.generate(
				{
					data:{
						columns:{{a_chart}},
						/*axes:{
							'Profit': 'y2'
						},*/ 
						type: "bar", 
						/*types: {Profit: 'line'},*/
						colors:
							
								{{colors}},
							
						groups: [{{a_group}}, {{a_income_group}}]}, 
						axis:{
							x:{
								type:"category", 
								categories:{{a_period_list}}
							},
							y:{
								tick:{
									format: d3.format("$,")
								}
							},
							/*y2:{
								show: true,
								label: {
										text: 'Profit %',
										position: 'outer-middle'
								},
								tick:{
									format: d3.format(",%")
								}
							}*/
						}
				});
				
				var jsonData	=	{{json_data}};
				
				$(function() {
						$( "#dialog" ).dialog({
							autoOpen: false,
							show: {
								effect: "blind",
								duration: 1000
							},
							hide: {
								effect: "blind",
								duration: 1000
							},
							width: '80%'
						});
						var today = new Date();
						$( "#from" ).datepicker({
							changeMonth: true,
							changeYear: true,
							minDate: (new Date(2015, 0, 1)),
							maxDate: -1 * today.getDate()
						});
						
						$( "#to" ).datepicker({
							changeMonth: true,
							changeYear: true,
							minDate: (new Date(2015, 0, 1)),
							maxDate: -1 * today.getDate()
						});
						
						//initialize the pqSelect widget.
						$("#project").pqSelect({
							multiplePlaceholder: 'Select Projects',    
							checkbox: true //adds checkbox to options    
						});
						
						$( ".accordion" ).accordion({
							heightStyle: "content",
							collapsible: true
						});
						
						//initialize the pqSelect widget.
						$("#customer").pqSelect({
							multiplePlaceholder: 'Select Customers',    
							checkbox: true //adds checkbox to options    
						});
						
						$( ".accordion" ).accordion({
							heightStyle: "content",
							collapsible: true
						});
						
				});
				
				function displayPopup(category, period)
				{
					var strHtml	=	'<table><thead><th>Date</th><th>Type</th><th>Transaction #</th><th>Memo</th><th>Amount</th></thead>';
					
					$("#dialog").dialog('option', 'title', category + ' - ' + period);
					
					var transactionList	= jsonData[category];
					
					var f_amount	=	0.0;
					
					for(var i = 0; transactionList != undefined && i < transactionList.length; i++)
					{
						if(transactionList[i].period == period)
						{
							strHtml	+=	'<tr>';
							strHtml +=	'<td>' + transactionList[i].dt + '</td>';
							strHtml +=	'<td>' + transactionList[i].type + '</td>';
							strHtml	+=	'<td>' + transactionList[i].num + '</td>';
							strHtml +=	'<td>' + (transactionList[i].memo == '- None -'?'':transactionList[i].memo) + '</td>';
							strHtml +=	'<td>' + accounting.formatNumber(transactionList[i].amount,2) + '</td>';
							strHtml	+=	'</tr>';
							
							f_amount += transactionList[i].amount;
						}
					}
					
					strHtml	+=	'<tfoot><tr><td>Total</td><td></td><td></td><td></td><td>' + accounting.formatNumber(f_amount,2) + '</td></tr></tfoot></table>';
					
					$("#dialog").html(strHtml);
					
					$( "#dialog" ).dialog( "open" );
				}
		</script>
	<div id="dialog" style="font-size: 0.91em;" title=""></div>
</body>
</html>