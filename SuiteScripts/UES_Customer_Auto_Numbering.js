/**
 * @author Shweta
 */
// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
/*
   	Script Name : UES_Customer_Auto_Numbering.js
	Author      : Shweta Chopde
	Date        : 1 April 2014
	Description : 


	Script Modification Log:

	-- Date --			-- Modified By --				--Requested By--				-- Description --



Below is a summary of the process controls enforced by this script file.  The control logic is described
more fully, below, in the appropriate function headers and code blocks.


     BEFORE LOAD
		- beforeLoadRecord(type)



     BEFORE SUBMIT
		- beforeSubmitRecord(type)


     AFTER SUBMIT
		- afterSubmitRecord(type)



     SUB-FUNCTIONS
		- The following sub-functions are called by the above core functions in order to maintain code
            modularization:

               - NOT USED

*/
}
// END SCRIPT DESCRIPTION BLOCK  ====================================



// BEGIN GLOBAL VARIABLE BLOCK  =====================================
{
	//  Initialize any Global Variables, in particular, debugging variables...




}
// END GLOBAL VARIABLE BLOCK  =======================================





// BEGIN BEFORE LOAD ==================================================

function beforeLoadRecord(type)
{

	/*  On before load:

          - EXPLAIN THE PURPOSE OF THIS FUNCTION

		-


		FIELDS USED:

          --Field Name--				--ID--


	*/



	//  LOCAL VARIABLES

    //  BEFORE LOAD CODE BODY


	return true;



}

// END BEFORE LOAD ====================================================





// BEGIN BEFORE SUBMIT ================================================

function beforeSubmit_customer(type)
{
 	var a_split_array = new Array();
	var s_split_child_array = new Array();
	var s_customer_prefix = '';
	var f_flag = ''
	var i_current_number = 0;
	var o_custom_recOBJ1;
	
	nlapiLogExecution('DEBUG', 'beforeSubmit_customer', ' Type --> ' + type);  
				
  
		if(type == 'create' || type == 'copy')
		{       
			{
				var s_company_name = nlapiGetFieldValue('companyname')
				nlapiLogExecution('DEBUG', 'beforeSubmit_customer', ' Company Name --> ' + s_company_name);  
			
              
				var s_parent_customer = nlapiGetFieldValue('parent')
				nlapiLogExecution('DEBUG', 'beforeSubmit_customer', ' Parent Customer --> ' + s_parent_customer);  
				
				if(s_parent_customer == null || s_parent_customer == '' || s_parent_customer == undefined)
				{					
				if(s_company_name!=null && s_company_name!='' && s_company_name!=undefined)
				{
					a_split_array = s_company_name.split(' ')
					
					var s_first_name = a_split_array[0]
															
					var s_middle_name = a_split_array[1]
					
					if((s_middle_name.toString()=='OR')||(s_middle_name.toString()=='AND')||(s_middle_name.toString()=='Or')||(s_middle_name.toString()=='And')||(s_middle_name.toString()=='or')||(s_middle_name.toString()=='and'))
					{
						s_middle_name = a_split_array[2];
						var s_last_name = a_split_array[3]
					}
					else
					{
						var s_last_name = a_split_array[2]
					}
					
					s_first_name = escape_special_chars(s_first_name)
					s_middle_name = escape_special_chars(s_middle_name)
					s_last_name = escape_special_chars(s_last_name)
					
					nlapiLogExecution('DEBUG', 'nlapiGetFieldValue', ' First Name --> ' + s_first_name);  
				
					nlapiLogExecution('DEBUG', 'nlapiGetFieldValue', ' Middle Name --> ' + s_middle_name);  
					
					nlapiLogExecution('DEBUG', 'nlapiGetFieldValue', ' Last Name --> ' + s_last_name);  
					
					// ========= First Name , Middle Name & Last Name exists ===============
						
					if(s_first_name!=null && s_first_name!='' && s_first_name!=undefined)
					{
						if(s_middle_name!=null && s_middle_name!='' && s_middle_name!=undefined)
						{
							if(s_last_name!=null && s_last_name!='' && s_last_name!=undefined)
							{
								 if((s_first_name.toString()=='THE')||(s_first_name.toString()=='OR')||(s_first_name.toString()=='AND')||(s_first_name.toString()=='the')||(s_first_name.toString()=='The')||(s_first_name.toString()=='Or')||(s_first_name.toString()=='or')||(s_first_name.toString()=='and')||(s_first_name.toString()=='And'))
								{
									throw "Company First Name has special characters AND THE OR \nThese are not allowed\n";
								    nlapiSetFieldValue('companyname','')
									return false;
								}
								 if((s_middle_name.toString()=='THE')||(s_middle_name.toString()=='the')||(s_middle_name.toString()=='The'))
								{							
									throw "Company Middle Name has special characters AND THE OR \nThese are not allowed\n";
								    nlapiSetFieldValue('companyname','')
									return false;
								}
								
								if((s_last_name.toString()=='THE')||(s_last_name.toString()=='OR')||(s_last_name.toString()=='AND')||(s_last_name.toString()=='the')||(s_last_name.toString()=='The')||(s_last_name.toString()=='Or')||(s_last_name.toString()=='or')||(s_last_name.toString()=='and')||(s_last_name.toString()=='And'))
								{						
									 throw "Company Last Name has special characters AND THE OR \nThese are not allowed\n";
								     nlapiSetFieldValue('companyname','')
									 return false;
								}			
																
								s_customer_prefix = s_first_name.substring(0,2)+''+s_middle_name.substring(0,1)+''+s_last_name.substring(0,1)
								s_customer_prefix = s_customer_prefix.toUpperCase()
								nlapiLogExecution('DEBUG', 'nlapiGetFieldValue', ' Customer Prefix --> ' + s_customer_prefix);  
													
							}//Last Name						
						
					   }//Middle Name						
						
					}//First Name		
					
					// ========= First Name , Middle Name exists & Last Name does not exists ===============
						
					if(s_first_name!=null && s_first_name!='' && s_first_name!=undefined)
					{
						if(s_middle_name!=null && s_middle_name!='' && s_middle_name!=undefined)
						{
							if(s_last_name ==null || s_last_name=='' || s_last_name==undefined)
							{	
							     if((s_first_name.toString()=='THE')||(s_first_name.toString()=='OR')||(s_first_name.toString()=='AND')||(s_first_name.toString()=='the')||(s_first_name.toString()=='The')||(s_first_name.toString()=='Or')||(s_first_name.toString()=='or')||(s_first_name.toString()=='and')||(s_first_name.toString()=='And'))
								{
									throw "Company First Name has special characters AND THE OR \nThese are not allowed\n";
								    nlapiSetFieldValue('companyname','')
									return false;
								}
								 if((s_middle_name.toString()=='THE')||(s_middle_name.toString()=='the')||(s_middle_name.toString()=='The'))
								{							
									throw "Company Middle Name has special characters AND THE OR \nThese are not allowed\n";
								    nlapiSetFieldValue('companyname','')
									return false;
								}
																
							 							
								s_customer_prefix = s_first_name.substring(0,2)+''+s_middle_name.substring(0,2)
								s_customer_prefix = s_customer_prefix.toUpperCase()
								nlapiLogExecution('DEBUG', 'nlapiGetFieldValue', ' Customer Prefix --> ' + s_customer_prefix);  
													
							}//Last Name						
						
					   }//Middle Name						
						
					}//First Name				
										
					// ========= First Name  exists &  Middle Name , Last Name does not exists ===============
						
					if(s_first_name!=null && s_first_name!='' && s_first_name!=undefined)
					{
						if(s_middle_name==null || s_middle_name =='' || s_middle_name ==undefined)
						{
							if(s_last_name ==null || s_last_name=='' || s_last_name==undefined)
							{		
							    if((s_first_name.toString()=='THE')||(s_first_name.toString()=='OR')||(s_first_name.toString()=='AND')||(s_first_name.toString()=='the')||(s_first_name.toString()=='The')||(s_first_name.toString()=='Or')||(s_first_name.toString()=='or')||(s_first_name.toString()=='and')||(s_first_name.toString()=='And'))
								{
									throw "Company First Name has special characters AND THE OR \nThese are not allowed\n";
								    nlapiSetFieldValue('companyname','')
									return false;
								}
															
								s_customer_prefix = s_first_name.substring(0,4)
								s_customer_prefix = s_customer_prefix.toUpperCase()
								nlapiLogExecution('DEBUG', 'nlapiGetFieldValue', ' Customer Prefix --> ' + s_customer_prefix);  
													
							}//Last Name						
						
					   }//Middle Name						
						
					}//First Name	
					
					// =================== Search Duplicates ========================
					
					   var filter = new Array();
					   filter[0] = new nlobjSearchFilter('entityid', null, 'startswith', s_customer_prefix);
							
					   var columns = new Array();
					   columns[0] = new nlobjSearchColumn('internalid');
					   
					   var a_searchresults = nlapiSearchRecord('customer',null,filter,columns);
					   
					   if(a_searchresults!=null && a_searchresults!='' && a_searchresults!=undefined)
					   {					   	
						 f_flag = true						 
						 
						  if(s_first_name!=null && s_first_name!='' && s_first_name!=undefined)
							{
								if(s_middle_name!=null && s_middle_name!='' && s_middle_name!=undefined)
								{
									//if(s_last_name!=null && s_last_name!='' && s_last_name!=undefined)
									{	
									   
											 if((s_first_name.toString()=='THE')||(s_first_name.toString()=='OR')||(s_first_name.toString()=='AND')||(s_first_name.toString()=='the')||(s_first_name.toString()=='The')||(s_first_name.toString()=='Or')||(s_first_name.toString()=='or')||(s_first_name.toString()=='and')||(s_first_name.toString()=='And'))
											{
												throw "Company First Name has special characters AND THE OR \nThese are not allowed\n";
											    nlapiSetFieldValue('companyname','')
												return false;
											}
											 if((s_middle_name.toString()=='THE')||(s_middle_name.toString()=='the')||(s_middle_name.toString()=='The'))
											{							
												throw "Company Middle Name has special characters AND THE OR \nThese are not allowed\n";
											    nlapiSetFieldValue('companyname','')
												return false;
											}
										
																
										s_customer_prefix = s_first_name.substring(0,3)+''+s_middle_name.substring(0,1)
										s_customer_prefix = s_customer_prefix.toUpperCase()
										nlapiLogExecution('DEBUG', 'nlapiGetFieldValue', ' Customer Prefix For Duplicates --> ' + s_customer_prefix);  
															
									}//Last Name						
								
							   }//Middle Name						
								
							}//First Name		
						 		
						 			
					   }//Search Results
					   else
					   {
					   	 f_flag = false 
					   }
					   
					  // =================== Search Sequence Number ========================
				 
				       var filter = new Array();
		               filter[0] = new nlobjSearchFilter('entityid', null, 'startswith', s_customer_prefix);
		   	
					   var columns = new Array();
					   columns[0] = new nlobjSearchColumn('entityid');
					   columns[0].setSort()
					   
					   var a_seq_searchresults = nlapiSearchRecord('customer',null,filter,columns);
					   
					   if(a_seq_searchresults!=null && a_seq_searchresults!='' && a_seq_searchresults!=undefined)
					   {					 
					   	var i_customer_entity = a_searchresults[0].getValue('entityid');
				        nlapiLogExecution('DEBUG', 'search_sub_customers_ID', ' Entityyyyy -->' + i_customer_entity);
								
					   }//Search Results	
					   
					   i_current_number = 0
					   
					   // ================== Load Custom record =================
					   
					  
					   var columns = new Array();
					   columns[0] = new nlobjSearchColumn('internalid');
					   
					   var a_seq_searchresults = nlapiSearchRecord('customrecord_serial_number_generation',null,null,columns);
					   
					   if(a_seq_searchresults!=null && a_seq_searchresults!='' && a_seq_searchresults!=undefined)
					   {					 
	                    o_custom_recOBJ1 = nlapiLoadRecord('customrecord_serial_number_generation', a_seq_searchresults[0].getValue('internalid'));
	                    nlapiLogExecution('DEBUG', 'nlapiGetFieldValue', 'o_custom_recOBJ --> ' + o_custom_recOBJ1);
	                    							 
						 				
					   }//Search Results							   
					   
					  // if(f_flag == false)
					   {
					   											   	 
						    var i_new_cust_number = parseInt(i_current_number) + 1; // increment current customer id by 1
	                        i_current_number = i_new_cust_number;
	                        if (i_new_cust_number <= 9) //	Add 0 if numbers are between 0 to 9
	                        {
	                            i_new_cust_number = '0' + i_new_cust_number.toString();
	                        }
		                        
						  nlapiLogExecution('DEBUG', 'nlapiGetFieldValue', 'Customer ID -->' + i_new_cust_number);
                        
						  nlapiLogExecution('DEBUG', 'nlapiGetFieldValue', 'sssssssssssssss-->' + s_customer_prefix +'-'+ i_new_cust_number);
                        
						 						  
						   nlapiLogExecution('DEBUG', 'nlapiGetFieldValue',' s_customer_prefix -->'+s_customer_prefix)	
						   nlapiLogExecution('DEBUG', 'nlapiGetFieldValue',' s_customer_prefix.toString().length-->'+s_customer_prefix.toString().length)					   
						                         					  
							if(s_customer_prefix.toString().length<4)
							{
								throw ' Less than four letters are not allowed in Customer Prefix '
								return false;
							}					   
											
						    var i_is_customer_prefix = isValid(s_customer_prefix) 														
							if(i_is_customer_prefix == false)
							{								
								throw "Company Name has special characters .@()~`!#$%^&*+=-[]\\\';,/{}|\":<>?. \nThese are not allowed\n";
							  	return false;
							}
						   					
					   }//No Duplicates 
					 
								
				}//Company Name	
					
				}//Parent Customer
				else
				{
					var o_parentOBJ = nlapiLoadRecord('customer',s_parent_customer)
					
					 if (o_parentOBJ != null && o_parentOBJ != '' && o_parentOBJ != undefined) 
					 {
					 	var i_customerID = o_parentOBJ.getFieldValue('entityid')
						nlapiLogExecution('DEBUG', 'afterSubmit_auto_numbering', 'Customer ID --> ' + i_customerID);
	                
					    var i_sub_customer_company_name =  search_sub_customers_ID(s_parent_customer)
					    nlapiLogExecution('DEBUG', 'afterSubmit_auto_numbering', 'Sub Customer Company Name --> ' + i_sub_customer_company_name);
	                
					    //================== If Sub - Customer does not exists ==================
					
					    if(i_sub_customer_company_name == null || i_sub_customer_company_name == '' || i_sub_customer_company_name == undefined)
						{
							i_customerID = i_customerID
							nlapiLogExecution('DEBUG', 'afterSubmit_auto_numbering', ' Prefix + Sequence Number (Parent) --> ' + i_customerID);
	                
						}//No Sub - Customer 
						else
						{
						  i_customerID = i_sub_customer_company_name
						  nlapiLogExecution('DEBUG', 'afterSubmit_auto_numbering', 'Prefix + Sequence Number (Sub - Customer) --> ' + i_customerID);
	                	
						}
					
						 if (i_customerID != null && i_customerID != '' && i_customerID != undefined)
						 {
						 	s_split_child_array = i_customerID.split('-')
							
							var s_prefix = s_split_child_array[0]
							nlapiLogExecution('DEBUG', 'afterSubmit_auto_numbering', 'Prefix --> ' + s_prefix);	                
							
							var i_sequence_number = s_split_child_array[1]
							nlapiLogExecution('DEBUG', 'afterSubmit_auto_numbering', 'Sequence Number --> ' + i_sequence_number);
	                
							var i_new_cust_number = parseInt(i_sequence_number) + 1; // increment current customer id by 1
	                        i_sequence_number = i_new_cust_number;
	                        if (i_new_cust_number <= 9) //	Add 0 if numbers are between 0 to 9
	                        {
	                            i_new_cust_number = '0' + i_new_cust_number.toString();
	                        }
		                        
							nlapiLogExecution('DEBUG', 'afterSubmit_auto_numbering', 'Sub - Customer ID -->' + i_new_cust_number);
                            nlapiLogExecution('DEBUG', 'afterSubmit_auto_numbering', 'Prefix -->' + s_prefix);
                        						  						  
						   nlapiLogExecution('DEBUG', 'afterSubmit_auto_numbering',' s_prefix -->'+s_prefix)	
						   nlapiLogExecution('DEBUG', 'afterSubmit_auto_numbering',' s_prefix.toString().length-->'+s_prefix.toString().length)					   
						                         					  
							if(s_prefix.toString().length<4)
							{
								throw ' Less than four letters are not allowed in Customer Prefix '
								return false;
							}
						   	var i_is_customer_prefix = isValid(s_prefix) 														
							if(i_is_customer_prefix == false)
							{								
								throw "Company Name has special characters .@()~`!#$%^&*+=-[]\\\';,/{}|\":<>?. \nThese are not allowed\n";
							  	return false;
							}					
						
						 }//Customer ID
						
					 }//Parent OBJ
					       
				}//Sub Customer
				
			}//Record OBJ			
			
		}//Create		
		
  return true;	
}

// END BEFORE SUBMIT ==================================================





// BEGIN AFTER SUBMIT =============================================

function afterSubmit_auto_numbering(type)
{
	var a_split_array = new Array();
	var s_split_child_array = new Array();
	var s_customer_prefix = '';
	var f_flag = ''
	var i_current_number = 0;
	var o_custom_recOBJ1;
	
	nlapiLogExecution('DEBUG', 'afterSubmit_auto_numbering', ' Type --> ' + type);  
	
  
	    if(type == 'create' || type == 'copy')
		{
			var i_recordID = nlapiGetRecordId()
			
			var s_record_type = nlapiGetRecordType()
			
			var o_recordOBJ = nlapiLoadRecord(s_record_type,i_recordID)
			
            if(o_recordOBJ!=null && o_recordOBJ!='' && o_recordOBJ!=undefined)
			{
				var s_company_name = o_recordOBJ.getFieldValue('companyname')
				nlapiLogExecution('DEBUG', 'afterSubmit_auto_numbering', ' Company Name --> ' + s_company_name);  
					
				var s_parent_customer = o_recordOBJ.getFieldValue('parent')
				nlapiLogExecution('DEBUG', 'afterSubmit_auto_numbering', ' Parent Customer --> ' + s_parent_customer);  
				
				if(s_parent_customer == null || s_parent_customer == '' || s_parent_customer == undefined)
				{					
				if(s_company_name!=null && s_company_name!='' && s_company_name!=undefined)
				{
					a_split_array = s_company_name.split(' ')
					
					var s_first_name = a_split_array[0]
															
					var s_middle_name = a_split_array[1]
					
					if((s_middle_name.toString()=='OR')||(s_middle_name.toString()=='AND')||(s_middle_name.toString()=='Or')||(s_middle_name.toString()=='And')||(s_middle_name.toString()=='or')||(s_middle_name.toString()=='and'))
					{
						s_middle_name = a_split_array[2];
						var s_last_name = a_split_array[3]
					}
					else
					{
						var s_last_name = a_split_array[2]
					}
					
					s_first_name = escape_special_chars(s_first_name)
					s_middle_name = escape_special_chars(s_middle_name)
					s_last_name = escape_special_chars(s_last_name)
					
					nlapiLogExecution('DEBUG', 'afterSubmit_auto_numbering', ' First Name --> ' + s_first_name);  
				
					nlapiLogExecution('DEBUG', 'afterSubmit_auto_numbering', ' Middle Name --> ' + s_middle_name);  
					
					nlapiLogExecution('DEBUG', 'afterSubmit_auto_numbering', ' Last Name --> ' + s_last_name);  
					
					// ========= First Name , Middle Name & Last Name exists ===============
						
					if(s_first_name!=null && s_first_name!='' && s_first_name!=undefined)
					{
						if(s_middle_name!=null && s_middle_name!='' && s_middle_name!=undefined)
						{
							if(s_last_name!=null && s_last_name!='' && s_last_name!=undefined)
							{
								 if((s_first_name.toString()=='THE')||(s_first_name.toString()=='OR')||(s_first_name.toString()=='AND')||(s_first_name.toString()=='the')||(s_first_name.toString()=='The')||(s_first_name.toString()=='Or')||(s_first_name.toString()=='or')||(s_first_name.toString()=='and')||(s_first_name.toString()=='And'))
								{
									throw "Company First Name has special characters AND THE OR \nThese are not allowed\n";
								    nlapiSetFieldValue('companyname','')
									return false;
								}
								 if((s_middle_name.toString()=='THE')||(s_middle_name.toString()=='the')||(s_middle_name.toString()=='The'))
								{							
									throw "Company Middle Name has special characters AND THE OR \nThese are not allowed\n";
								    nlapiSetFieldValue('companyname','')
									return false;
								}
								
								if((s_last_name.toString()=='THE')||(s_last_name.toString()=='OR')||(s_last_name.toString()=='AND')||(s_last_name.toString()=='the')||(s_last_name.toString()=='The')||(s_last_name.toString()=='Or')||(s_last_name.toString()=='or')||(s_last_name.toString()=='and')||(s_last_name.toString()=='And'))
								{						
									 throw "Company Last Name has special characters AND THE OR \nThese are not allowed\n";
								     nlapiSetFieldValue('companyname','')
									 return false;
								}	
																
								s_customer_prefix = s_first_name.substring(0,2)+''+s_middle_name.substring(0,1)+''+s_last_name.substring(0,1)
								s_customer_prefix = s_customer_prefix.toUpperCase()
								nlapiLogExecution('DEBUG', 'afterSubmit_auto_numbering', ' Customer Prefix --> ' + s_customer_prefix);  
													
							}//Last Name						
						
					   }//Middle Name						
						
					}//First Name		
					
					// ========= First Name , Middle Name exists & Last Name does not exists ===============
						
					if(s_first_name!=null && s_first_name!='' && s_first_name!=undefined)
					{
						if(s_middle_name!=null && s_middle_name!='' && s_middle_name!=undefined)
						{
							if(s_last_name ==null || s_last_name=='' || s_last_name==undefined)
							{	
							   if((s_first_name.toString()=='THE')||(s_first_name.toString()=='OR')||(s_first_name.toString()=='AND')||(s_first_name.toString()=='the')||(s_first_name.toString()=='The')||(s_first_name.toString()=='Or')||(s_first_name.toString()=='or')||(s_first_name.toString()=='and')||(s_first_name.toString()=='And'))
								{
									throw "Company First Name has special characters AND THE OR \nThese are not allowed\n";
								    nlapiSetFieldValue('companyname','')
									return false;
								}
								 if((s_middle_name.toString()=='THE')||(s_middle_name.toString()=='the')||(s_middle_name.toString()=='The'))
								{							
									throw "Company Middle Name has special characters AND THE OR \nThese are not allowed\n";
								    nlapiSetFieldValue('companyname','')
									return false;
								}									
							 							
								s_customer_prefix = s_first_name.substring(0,2)+''+s_middle_name.substring(0,2)
								s_customer_prefix = s_customer_prefix.toUpperCase()
								nlapiLogExecution('DEBUG', 'afterSubmit_auto_numbering', ' Customer Prefix --> ' + s_customer_prefix);  
													
							}//Last Name						
						
					   }//Middle Name						
						
					}//First Name				
										
					// ========= First Name  exists &  Middle Name , Last Name does not exists ===============
						
					if(s_first_name!=null && s_first_name!='' && s_first_name!=undefined)
					{
						if(s_middle_name==null || s_middle_name =='' || s_middle_name ==undefined)
						{
							if(s_last_name ==null || s_last_name=='' || s_last_name==undefined)
							{		
							    if((s_first_name.toString()=='THE')||(s_first_name.toString()=='OR')||(s_first_name.toString()=='AND')||(s_first_name.toString()=='the')||(s_first_name.toString()=='The')||(s_first_name.toString()=='Or')||(s_first_name.toString()=='or')||(s_first_name.toString()=='and')||(s_first_name.toString()=='And'))
								{
									throw "Company First Name has special characters AND THE OR \nThese are not allowed\n";
								    nlapiSetFieldValue('companyname','')
									return false;
								}													
								s_customer_prefix = s_first_name.substring(0,4)
								s_customer_prefix = s_customer_prefix.toUpperCase()
								nlapiLogExecution('DEBUG', 'afterSubmit_auto_numbering', ' Customer Prefix --> ' + s_customer_prefix);  
													
							}//Last Name						
						
					   }//Middle Name						
						
					}//First Name	
					
					// =================== Search Duplicates ========================
					
					   var filter = new Array();
					   filter[0] = new nlobjSearchFilter('entityid', null, 'startswith', s_customer_prefix);
							
					   var columns = new Array();
					   columns[0] = new nlobjSearchColumn('internalid');
					   
					   var a_searchresults = nlapiSearchRecord('customer',null,filter,columns);
					   
					   if(a_searchresults!=null && a_searchresults!='' && a_searchresults!=undefined)
					   {					   	
						 f_flag = true						 
						 
						  if(s_first_name!=null && s_first_name!='' && s_first_name!=undefined)
							{
								if(s_middle_name!=null && s_middle_name!='' && s_middle_name!=undefined)
								{
									//if(s_last_name!=null && s_last_name!='' && s_last_name!=undefined)
									{	
									   
									if((s_first_name.toString()=='THE')||(s_first_name.toString()=='OR')||(s_first_name.toString()=='AND')||(s_first_name.toString()=='the')||(s_first_name.toString()=='The')||(s_first_name.toString()=='Or')||(s_first_name.toString()=='or')||(s_first_name.toString()=='and')||(s_first_name.toString()=='And'))
									{
										throw "Company First Name has special characters AND THE OR \nThese are not allowed\n";
									    nlapiSetFieldValue('companyname','')
										return false;
									}
									 if((s_middle_name.toString()=='THE')||(s_middle_name.toString()=='the')||(s_middle_name.toString()=='The'))
									{							
										throw "Company Middle Name has special characters AND THE OR \nThese are not allowed\n";
									    nlapiSetFieldValue('companyname','')
										return false;
									}
									
																
										s_customer_prefix = s_first_name.substring(0,3)+''+s_middle_name.substring(0,1)
										s_customer_prefix = s_customer_prefix.toUpperCase()
										nlapiLogExecution('DEBUG', 'afterSubmit_auto_numbering', ' Customer Prefix For Duplicates --> ' + s_customer_prefix);  
															
									}//Last Name						
								
							   }//Middle Name						
								
							}//First Name		
						 		
						 			
					   }//Search Results
					   else
					   {
					   	 f_flag = false 
					   }
					   
					  // =================== Search Sequence Number ========================
				 
				       var filter = new Array();
		               filter[0] = new nlobjSearchFilter('entityid', null, 'startswith', s_customer_prefix);
		   	
					   var columns = new Array();
					   columns[0] = new nlobjSearchColumn('entityid');
					   columns[0].setSort()
					   
					   var a_seq_searchresults = nlapiSearchRecord('customer',null,filter,columns);
					   
					   if(a_seq_searchresults!=null && a_seq_searchresults!='' && a_seq_searchresults!=undefined)
					   {					 
					   	var i_customer_entity = a_searchresults[0].getValue('entityid');
				        nlapiLogExecution('DEBUG', 'search_sub_customers_ID', ' Entityyyyy -->' + i_customer_entity);
								
					   }//Search Results	
					   
					   i_current_number = 0
					   
					   // ================== Load Custom record =================
					   
					  
					   var columns = new Array();
					   columns[0] = new nlobjSearchColumn('internalid');
					   
					   var a_seq_searchresults = nlapiSearchRecord('customrecord_serial_number_generation',null,null,columns);
					   
					   if(a_seq_searchresults!=null && a_seq_searchresults!='' && a_seq_searchresults!=undefined)
					   {					 
	                    o_custom_recOBJ1 = nlapiLoadRecord('customrecord_serial_number_generation', a_seq_searchresults[0].getValue('internalid'));
	                    nlapiLogExecution('DEBUG', 'afterSubmit_auto_numbering', 'o_custom_recOBJ --> ' + o_custom_recOBJ1);
	                    							 
						 				
					   }//Search Results							   
					   
					  // if(f_flag == false)
					   {
					   	 try
	                     {											   	 
						    var i_new_cust_number = parseInt(i_current_number) + 1; // increment current customer id by 1
	                        i_current_number = i_new_cust_number;
	                        if (i_new_cust_number <= 9) //	Add 0 if numbers are between 0 to 9
	                        {
	                            i_new_cust_number = '0' + i_new_cust_number.toString();
	                        }
		                        
						  nlapiLogExecution('DEBUG', 'afterSubmit_auto_numbering', 'Customer ID -->' + i_new_cust_number);
                        
						  nlapiLogExecution('DEBUG', 'afterSubmit_auto_numbering', 'sssssssssssssss-->' + s_customer_prefix +'-'+ i_new_cust_number);
                        
						
					   	   o_recordOBJ.setFieldValue('entityid', s_customer_prefix +'-'+ i_new_cust_number)
						
						   						  
						   nlapiLogExecution('DEBUG', 'afterSubmit_auto_numbering',' s_customer_prefix -->'+s_customer_prefix)	
						   nlapiLogExecution('DEBUG', 'afterSubmit_auto_numbering',' s_customer_prefix.toString().length-->'+s_customer_prefix.toString().length)					   
						                         					  
							if(s_customer_prefix.toString().length<4)
							{
								throw ' Less than four letters are not allowed in Customer Prefix ';
								o_recordOBJ.setFieldValue('entityid', '')
								return false;
							}
						    var i_is_customer_prefix = isValid(s_customer_prefix) 														
							
							if(i_is_customer_prefix == false)
							{								
								throw "Company Name has special characters .@()~`!#$%^&*+=-[]\\\';,/{}|\":<>?. \nThese are not allowed\n";
							  	return false;
							}	
						
						   o_custom_recOBJ1.setFieldValue('custrecord_current_serial_number', s_customer_prefix +'-'+ i_new_cust_number);
                           o_custom_recOBJ1.setFieldValue('custrecord_current_sequence_number', i_new_cust_number);
                        
						   var i_cust_rec_submitID = nlapiSubmitRecord(o_custom_recOBJ1,true,true)
						   
						   var i_customer_submitID = nlapiSubmitRecord(o_recordOBJ,true,true)
						   
						   nlapiLogExecution('DEBUG', 'afterSubmit_auto_numbering', 'Custom Record Submit ID --> ' + i_cust_rec_submitID);
	                       
						   nlapiLogExecution('DEBUG', 'afterSubmit_auto_numbering', 'Customer Submit ID --> ' + i_customer_submitID);
	                       
						   }//TRY   
						    catch(exception)
							{
							   nlapiLogExecution('DEBUG', 'ERROR', ' Exception Caught --> ' + exception);            	
							}//CATCH						
					   }//No Duplicates 
					 							
				}//Company Name	
					
				}//Parent Customer
				else
				{
					var o_parentOBJ = nlapiLoadRecord('customer',s_parent_customer)
					
					 if (o_parentOBJ != null && o_parentOBJ != '' && o_parentOBJ != undefined) 
					 {
					 	var i_customerID = o_parentOBJ.getFieldValue('entityid')
						nlapiLogExecution('DEBUG', 'afterSubmit_auto_numbering', 'Customer ID --> ' + i_customerID);
	                
					    var i_sub_customer_company_name =  search_sub_customers_ID(s_parent_customer)
					    nlapiLogExecution('DEBUG', 'afterSubmit_auto_numbering', 'Sub Customer Company Name --> ' + i_sub_customer_company_name);
	                
					    //================== If Sub - Customer does not exists ==================
					
					    if(i_sub_customer_company_name == null || i_sub_customer_company_name == '' || i_sub_customer_company_name == undefined)
						{
							i_customerID = i_customerID
							nlapiLogExecution('DEBUG', 'afterSubmit_auto_numbering', ' Prefix + Sequence Number (Parent) --> ' + i_customerID);
	                
						}//No Sub - Customer 
						else
						{
						  i_customerID = i_sub_customer_company_name
						  nlapiLogExecution('DEBUG', 'afterSubmit_auto_numbering', 'Prefix + Sequence Number (Sub - Customer) --> ' + i_customerID);
	                	
						}
					
						 if (i_customerID != null && i_customerID != '' && i_customerID != undefined)
						 {
						 	s_split_child_array = i_customerID.split('-')
							
							var s_prefix = s_split_child_array[0]
							nlapiLogExecution('DEBUG', 'afterSubmit_auto_numbering', 'Prefix --> ' + s_prefix);	                
							
							var i_sequence_number = s_split_child_array[1]
							nlapiLogExecution('DEBUG', 'afterSubmit_auto_numbering', 'Sequence Number --> ' + i_sequence_number);
	                
							var i_new_cust_number = parseInt(i_sequence_number) + 1; // increment current customer id by 1
	                        i_sequence_number = i_new_cust_number;
	                        if (i_new_cust_number <= 9) //	Add 0 if numbers are between 0 to 9
	                        {
	                            i_new_cust_number = '0' + i_new_cust_number.toString();
	                        }
		                        
							nlapiLogExecution('DEBUG', 'afterSubmit_auto_numbering', 'Sub - Customer ID -->' + i_new_cust_number);
                            nlapiLogExecution('DEBUG', 'afterSubmit_auto_numbering', 'Prefix -->' + s_prefix);
                        
						   o_recordOBJ.setFieldValue('entityid', s_prefix +'-'+ i_new_cust_number)
						   						  
						   nlapiLogExecution('DEBUG', 'afterSubmit_auto_numbering',' s_customer_prefix -->'+s_customer_prefix)	
						   nlapiLogExecution('DEBUG', 'afterSubmit_auto_numbering',' s_customer_prefix.toString().length-->'+s_customer_prefix.toString().length)					   
						                         					  
							if(s_prefix.toString().length<4)
							{
								throw ' Less than four letters are not allowed in Customer Prefix '
								o_recordOBJ.setFieldValue('entityid', '')
								return false;
							}
						    var i_is_customer_prefix = isValid(s_prefix) 														
							
							if(i_is_customer_prefix == false)
							{								
								throw "Company Name has special characters .@()~`!#$%^&*+=-[]\\\';,/{}|\":<>?. \nThese are not allowed\n";
							  	return false;
							}	
						
						   var i_customer_submitID = nlapiSubmitRecord(o_recordOBJ,true,true)
						   
						   nlapiLogExecution('DEBUG', 'afterSubmit_auto_numbering', 'Customer Submit ID --> ' + i_customer_submitID);
	                       
							
						 }//Customer ID
						
					 }//Parent OBJ
					       
				}//Sub Customer
				
			}//Record OBJ			
			
		}//Create		
		
  return true;	

}

// END AFTER SUBMIT ===============================================

// BEGIN FUNCTION ===================================================

function search_sub_customers_ID(s_parent_customer)
{
	var i_company_name;
	
	 if (s_parent_customer != null && s_parent_customer != '' && s_parent_customer != undefined) 
	 {
	 	   var filter = new Array();
		   filter[0] = new nlobjSearchFilter('parent', null, 'is', s_parent_customer);
		   				
		   var columns = new Array();
		   columns[0] = new nlobjSearchColumn('internalid');
		   columns[0].setSort(true);
		   columns[1] = new nlobjSearchColumn('entityid');
		   
		   var a_searchresults = nlapiSearchRecord('customer',null,filter,columns);
		   
		   if(a_searchresults!=null && a_searchresults!='' && a_searchresults!=undefined)
		   {
		   	nlapiLogExecution('DEBUG', 'search_sub_customers_ID', '  a_searchresults -->' + a_searchresults.length);
			
			if(a_searchresults.length > 1)
			{
				if(a_searchresults[1].getValue('internalid')!=null && a_searchresults[1].getValue('internalid')!='' && a_searchresults[1].getValue('internalid')!=undefined)
				{
					var i_customerID = a_searchresults[1].getValue('internalid');
					nlapiLogExecution('DEBUG', 'search_sub_customers_ID', '  Customer ID -->' + i_customerID);
					
					i_company_name = a_searchresults[1].getValue('entityid');
					nlapiLogExecution('DEBUG', 'search_sub_customers_ID', '  Company Name -->' + i_company_name);
					
				}			
				
			}									   	
			 else if(a_searchresults.length == 1)	
			 {
			 	var i_customerID = a_searchresults[0].getValue('internalid');
				nlapiLogExecution('DEBUG', 'search_sub_customers_ID', '  Customer ID -->' + i_customerID);
				
				i_company_name = a_searchresults[0].getValue('entityid');
				nlapiLogExecution('DEBUG', 'search_sub_customers_ID', '  Company Name -->' + i_company_name);
			 }
							 			
		   }//Search Results
		  		
	 }//Parent Customer
	
	return i_company_name;
	
}//Sub Customer ID

function isValid(str) 
{
  str = str.trim();
  var iChars = ".@()~`!#$%^&*+=-[]\\\';,/{}|\":<>?.";
	for (var i = 0; i < str.length; i++)
	{
	   if (iChars.indexOf(str.charAt(i)) != -1)
	   {
	       alert ("Company Name has special characters .@()~`!#$%^&*+=-[]\\\';,/{}|\":<>?.AND THE OR \nThese are not allowed\n");
	       return false;
	   }   
	}	
	return true;
}
function escape_special_chars(text)
{
	var iChars = ".@()~`!#$%^&*+=-[]\\\';,/{}|\":<>?.";
	var text_new =''
		
	if(text!=null && text!='' && text!=undefined)
	{
		for(var y=0; y<text.toString().length; y++)
		{	
		  if(iChars.indexOf(text[y])== -1)
		  {		  
			text_new +=text[y]
		  }		
			
		}//Loop				
	}//Text Validation	
	return text_new;
}




// END FUNCTION =====================================================
