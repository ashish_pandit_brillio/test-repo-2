/**
 * Display month wise revenue of the project
 * 
 * Version Date Author Remarks 1.00 22 Jun 2016 Nitish Mishra
 * 
 */

function suitelet(request, response) {
	try {
		var userId = nlapiGetUser();

		if (userId == 9673 || userId == 442 || userId == 39108 || userId == 7905) {
			userId = 15195;
		}

		var taggedProjects = getTaggedProjectList(userId);
		var projectList = [];

		// User Inputs
		var requestedProject = request.getParameterValues('custpage_project');
		var requestedInrExchangeRate = request
		        .getParameter('custpage_exchange_rate_inr');
		var requestedGbpExchangeRate = request
		        .getParameter('custpage_exchange_rate_gbp');
		var requestedYear = request.getParameter('custpage_year');

		var isRefreshOnGoing = isRefreshOngoing();

		var formTitle = 'Revenue Projection';

		if (isRefreshOnGoing) {
			formTitle += " ( Data Refresh In Progress )";
		} else {
			var refreshDate = getDataRefreshDate();

			if (refreshDate) {
				formTitle += " ( Last updated on " + refreshDate + " )";
			}
		}

		// Form
		var form = nlapiCreateForm(formTitle);
		form.addSubmitButton('Refresh');

		form.addFieldGroup('custpage_1', 'Select Options');

		// Project Field
		var projectField = form.addField('custpage_project', 'multiselect',
		        'Project', null, 'custpage_1');
		projectField.setBreakType('startcol');

		taggedProjects.forEach(function(project) {
			projectList.push(project.getId());
			var projectName = project.getValue('entityid') + " : "
			        + project.getValue('altname');
			projectField.addSelectOption(project.getId(), projectName);
		});

		if (requestedProject) {
			projectField.setDefaultValue(requestedProject);

			for (var i = 0; i < requestedProject.length; i++) {
				var projectFound = false;

				for (var j = 0; j < projectList.length; j++) {

					if (projectList[j] == requestedProject[i]) {
						projectFound = true;
						break;
					}
				}

				if (!projectFound) {
					throw "Access not allowed";
				}
			}
		}

		var projectData = getProjectDetails(projectList, requestedProject);

		// Currency fields
		var inrField = form.addField('custpage_exchange_rate_inr', 'currency',
		        'USD to INR ( 1 USD = x INR )', null, 'custpage_1');
		inrField.setBreakType('startcol');

		if (requestedInrExchangeRate) {
			inrField.setDefaultValue(requestedInrExchangeRate);
		}

		var gbpField = form.addField('custpage_exchange_rate_gbp', 'currency',
		        'USD to GBP ( 1 USD = x GBP )', null, 'custpage_1');
		gbpField.setBreakType('startcol');

		if (requestedGbpExchangeRate) {
			gbpField.setDefaultValue(requestedGbpExchangeRate);
		}

		var requestedExchangeRateTable = {
		    USD : 1,
		    INR : parseFloat(requestedInrExchangeRate),
		    GBP : parseFloat(requestedGbpExchangeRate)
		};

		// Dates Field
		var yearField = form.addField('custpage_year', 'select', 'Year', null,
		        'custpage_1');
		yearField.setBreakType('startcol');
		yearField.setMandatory(true);
		var currentYear = (new Date()).getFullYear();

		for (var i = 2016; i <= currentYear + 4; i++) {
			yearField.addSelectOption(i, i);
		}

		if (requestedYear) {
			yearField.setDefaultValue(requestedYear);
		} else {
			requestedYear = currentYear;
			yearField.setDefaultValue(currentYear);
		}

		var startDate = "1/1/" + requestedYear;
		var endDate = "12/31/" + requestedYear;

		nlapiLogExecution('debug', 'start date', startDate);
		nlapiLogExecution('debug', 'end date', endDate);

		// Legends Field
		var legendTable = form.addField('custpage_legend', 'inlinehtml', null,
		        null, 'custpage_1');
		legendTable.setBreakType('startcol');
		legendTable.setDefaultValue(legendsTable());

		// Projection Table
		/*var revenueSearchData = getProjectRevenueData(projectList, startDate,
		        endDate, requestedProject);*/
		var prac_list = new Array();
		prac_list.push(190);
		prac_list.push(320);
		prac_list.push(191);
		prac_list.push(316);
		prac_list.push(407);
		prac_list.push(448);
		prac_list.push(446);
		prac_list.push(325);
		prac_list.push(328);
		prac_list.push(479);
		prac_list.push(427);
		prac_list.push(323);
		prac_list.push(451);
		prac_list.push(324);
		prac_list.push(327);
		prac_list.push(329);
		prac_list.push(326);
		
		var projectData = getProjectDetails_practiceWise(prac_list);
		nlapiLogExecution('audit','projectData len',projectData.length);
		
		var revenueSearchData = getProjectRevenueData_ByPractice(prac_list, startDate,
		        endDate, requestedProject);
		
		var tableHtml = createProjectionTable(revenueSearchData, projectData,
		        requestedExchangeRateTable, requestedYear);
		form.addFieldGroup('custpage_2', 'Revenue Data');
		form.addField('custpage_table', 'inlinehtml', null, null, 'custpage_2')
		        .setDefaultValue(tableHtml);

		response.writePage(form);
	} catch (err) {
		nlapiLogExecution('ERROR', 'suitelet', err);
		displayErrorForm(err, "Revenue Projection");
	}
}

function getProjectDetails(projectList, requestedProject) {
	try {
		var projectData = {};

		var filters = [ new nlobjSearchFilter('internalid', null, 'anyof',
		        projectList) ];

		if (requestedProject) {
			filters.push(new nlobjSearchFilter('internalid', null, 'anyof',
			        requestedProject));
		}

		searchRecord(
		        'job',
		        null,
		        filters,
		        [ new nlobjSearchColumn('startdate'),
		                new nlobjSearchColumn('enddate'),
		                new nlobjSearchColumn('jobbillingtype'),
		                new nlobjSearchColumn('entityid'),
		                new nlobjSearchColumn('altname'),
		                new nlobjSearchColumn('custentity_t_and_m_monthly') ])
		        .forEach(
		                function(project) {
			                projectData[project.getId()] = {
			                    EntityId : project.getValue('entityid'),
			                    AltName : project.getValue('altname'),
			                    StartDate : project.getValue('startdate'),
			                    EndDate : project.getValue('enddate'),
			                    BillingType : project.getText('jobbillingtype')
			                            + (project
			                                    .getValue('custentity_t_and_m_monthly') == 'T' ? ' Monthly'
			                                    : '')
			                };
		                });

		return projectData;
	} catch (err) {
		nlapiLogExecution('ERROR', 'getProjectDetails', err);
		throw err;
	}
}

function getProjectDetails_practiceWise(prac_list) {
	try {
		var projectData = {};

		var filters = [ new nlobjSearchFilter('custentity_practice', null, 'anyof',
		        prac_list) ];


		searchRecord(
		        'job',
		        null,
		        filters,
		        [ new nlobjSearchColumn('startdate'),
		                new nlobjSearchColumn('enddate'),
		                new nlobjSearchColumn('jobbillingtype'),
		                new nlobjSearchColumn('entityid'),
		                new nlobjSearchColumn('altname'),
		                new nlobjSearchColumn('custentity_t_and_m_monthly') ])
		        .forEach(
		                function(project) {
			                projectData[project.getId()] = {
			                    EntityId : project.getValue('entityid'),
			                    AltName : project.getValue('altname'),
			                    StartDate : project.getValue('startdate'),
			                    EndDate : project.getValue('enddate'),
			                    BillingType : project.getText('jobbillingtype')
			                            + (project
			                                    .getValue('custentity_t_and_m_monthly') == 'T' ? ' Monthly'
			                                    : '')
			                };
		                });

		return projectData;
	} catch (err) {
		nlapiLogExecution('ERROR', 'getProjectDetails', err);
		throw err;
	}
}

function getProjectRevenueData(projectList, startDate, endDate,
        requestedProject)
{
	try {
		var filters = [
		        new nlobjSearchFilter('custrecord_prp_project',
		                'custrecord_eprp_project_rev_projection', 'anyof',
		                projectList),
		        new nlobjSearchFilter('custrecord_prp_start_date',
		                'custrecord_eprp_project_rev_projection', 'within',
		                startDate, endDate),
		        new nlobjSearchFilter('isinactive',
		                'custrecord_eprp_project_rev_projection', 'is', 'F'),
		        new nlobjSearchFilter('isinactive', null, 'is', 'F'),
		        new nlobjSearchFilter('custrecord_prp_is_past',
		                'custrecord_eprp_project_rev_projection', 'is', 'F') ];

		if (requestedProject) {
			filters.push(new nlobjSearchFilter('custrecord_prp_project',
			        'custrecord_eprp_project_rev_projection', 'anyof',
			        requestedProject));
		}

		var revenueSearch = nlapiSearchRecord(
		        'customrecord_emp_practice_rev_projection', null, filters, [
		                new nlobjSearchColumn('custrecord_prp_project',
		                        'custrecord_eprp_project_rev_projection'),
		                new nlobjSearchColumn('custrecord_prp_start_date',
		                        'custrecord_eprp_project_rev_projection'),
		                new nlobjSearchColumn('custrecord_prp_month_name',
		                        'custrecord_eprp_project_rev_projection'),
		                new nlobjSearchColumn('custrecord_eprp_practice'),
		                new nlobjSearchColumn('custrecord_eprp_revenue'),
		                new nlobjSearchColumn('custrecord_eprp_usd_revenue'),
		                new nlobjSearchColumn('custrecord_eprp_currency') ]);

		if (revenueSearch) {
			nlapiLogExecution('debug', 'revenueSearch', revenueSearch.length);
		}

		return revenueSearch;
	} catch (err) {
		nlapiLogExecution('ERROR', 'getProjectRevenueData', err);
		throw err;
	}
}

function getProjectRevenueData_ByPractice(prac_list, startDate, endDate,
        requestedProject)
{
	try {
		var filters = [
		        new nlobjSearchFilter('custrecord_eprp_practice',
		                null, 'anyof',
		                prac_list),
		        new nlobjSearchFilter('custrecord_prp_start_date',
		                'custrecord_eprp_project_rev_projection', 'within',
		                startDate, endDate),
		        new nlobjSearchFilter('isinactive',
		                'custrecord_eprp_project_rev_projection', 'is', 'F'),
		        new nlobjSearchFilter('isinactive', null, 'is', 'F'),
		        new nlobjSearchFilter('custrecord_prp_is_past',
		                'custrecord_eprp_project_rev_projection', 'is', 'F') ];

		var revenueSearch = nlapiSearchRecord(
		        'customrecord_emp_practice_rev_projection', null, filters, [
		                new nlobjSearchColumn('custrecord_prp_project',
		                        'custrecord_eprp_project_rev_projection'),
		                new nlobjSearchColumn('custrecord_prp_start_date',
		                        'custrecord_eprp_project_rev_projection'),
		                new nlobjSearchColumn('custrecord_prp_month_name',
		                        'custrecord_eprp_project_rev_projection'),
		                new nlobjSearchColumn('custrecord_eprp_practice'),
		                new nlobjSearchColumn('custrecord_eprp_revenue'),
		                new nlobjSearchColumn('custrecord_eprp_usd_revenue'),
		                new nlobjSearchColumn('custrecord_eprp_currency') ]);

		if (revenueSearch) {
			nlapiLogExecution('debug', 'revenueSearch', revenueSearch.length);
		}

		return revenueSearch;
	} catch (err) {
		nlapiLogExecution('ERROR', 'getProjectRevenueData', err);
		throw err;
	}
}

function createProjectionTable(revenueSearchData, projectData,
        requestedExchangeRateTable, requestedYear)
{
	try {
		var projectWiseRevenue = {};

		if (revenueSearchData) {

			for (var i = 0; i < revenueSearchData.length; i++) {
				var projectId = revenueSearchData[i].getValue(
				        'custrecord_prp_project',
				        'custrecord_eprp_project_rev_projection');
				var projectName = revenueSearchData[i].getText(
				        'custrecord_prp_project',
				        'custrecord_eprp_project_rev_projection');
				var startDate = nlapiStringToDate(revenueSearchData[i]
				        .getValue('custrecord_prp_start_date',
				                'custrecord_eprp_project_rev_projection'));
				var yearName = revenueSearchData[i].getValue(
				        'custrecord_prp_year',
				        'custrecord_eprp_project_rev_projection');
				var monthName = revenueSearchData[i].getValue(
				        'custrecord_prp_month_name',
				        'custrecord_eprp_project_rev_projection');
				var isRecognised = revenueSearchData[i].getValue(
				        'custrecord_prp_is_recognised',
				        'custrecord_eprp_project_rev_projection') == 'T';
				var revenue = revenueSearchData[i]
				        .getValue('custrecord_eprp_revenue');
				var convertedRevenue = revenueSearchData[i]
				        .getValue('custrecord_eprp_usd_revenue');
				var practice = revenueSearchData[i]
				        .getText('custrecord_eprp_practice');
				var currency = revenueSearchData[i]
				        .getText('custrecord_eprp_currency');
				
				var exchangeRate = 0;

				if (requestedExchangeRateTable[currency]) {
					exchangeRate = requestedExchangeRateTable[currency];
					convertedRevenue = Math.round(revenue * (1 / exchangeRate),
					        2);
				}

				if (!projectWiseRevenue[projectId]) {
					//var billingType = nlapiLookupField('job',projectId,'jobbillingtype');
						//BillingType : billingType,
					projectWiseRevenue[projectId] = {
					    Name : projectName,
					    Currency : currency,
						Practice : practice,
					    RevenueData : {}
					};
				}

				if (!projectWiseRevenue[projectId].RevenueData[practice]) {
					projectWiseRevenue[projectId].RevenueData[practice] = new yearData();
				}

				projectWiseRevenue[projectId].RevenueData[practice][monthName].Amount = convertedRevenue;
				projectWiseRevenue[projectId].RevenueData[practice][monthName].IsRecognised = isRecognised;
			}
		}
		
		nlapiLogExecution('audit','projectWiseRevenue len',projectWiseRevenue.length);

		var css_file_url = "";

		var context = nlapiGetContext();
		if (context.getEnvironment() == "SANDBOX") {
			css_file_url = "https://system.na1.netsuite.com/core/media/media.nl?id=199133&c=3883006&h=64fa194295cd4ab64d58&mv=iq3kjr6g&_xt=.css&whence=";
		} else if (context.getEnvironment() == "PRODUCTION") {
			css_file_url = "https://system.na1.netsuite.com/core/media/media.nl?id=199133&c=3883006&h=64fa194295cd4ab64d58&mv=iq3kjr6g&_xt=.css&whence=";
		}

		var html = "";
		html += "<link href='" + css_file_url + "'  rel='stylesheet'>";

		html += "<table class='projection-table'>";

		// header
		html += "<tr class='header-row'>";
		html += "<td>";
		html += "Project";
		html += "</td>";

		/*html += "<td>";
		html += "Billing Type";
		html += "</td>";

		html += "<td>";
		html += "Start Date";
		html += "</td>";

		html += "<td>";
		html += "End Date";
		html += "</td>";*/

		html += "<td>";
		html += "Practice";
		html += "</td>";

		html += "<td>";
		html += "Currency";
		html += "</td>";

		var monthNames = [ "JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JUL",
		        "AUG", "SEP", "OCT", "NOV", "DEC" ];

		var today = new Date();
		var currentMonthName = getMonthName(nlapiDateToString(today));
		var monthPos = 0;

		if (requestedYear < today.getFullYear()) {
			monthPos = -1;
		} else if (requestedYear == today.getFullYear()) {
			monthPos = monthNames.indexOf(currentMonthName);
		} else {
			monthPos = 13;
		}

		for (var i = 0; i < monthNames.length; i++) {
			html += "<td>";
			html += monthNames[i];
			html += "</td>";
		}

		html += "</tr>";

		for ( var project in projectWiseRevenue) {

			for ( var practice in projectWiseRevenue[project].RevenueData) {
				//nlapiLogExecution('audit','projectData[project].EntityId',projectData[project].EntityId);
				html += "<tr>";
				html += "<td class='label-name'>";
				html += projectWiseRevenue[project].Name;
				html += "</td>";

				/*html += "<td class='label-name'>";
				html += projectWiseRevenue[project].BillingType;
				html += "</td>";

				html += "<td class='label-name'>";
				html += projectData[project].StartDate;
				html += "</td>";

				html += "<td class='label-name'>";
				html += projectData[project].EndDate;
				html += "</td>";*/

				html += "<td class='label-name'>";
				html += projectWiseRevenue[project].Practice;
				html += "</td>";

				html += "<td class='label-name'>";
				html += projectWiseRevenue[project].Currency;
				html += "</td>";

				for ( var month in projectWiseRevenue[project].RevenueData[practice]) {
					// check if past month
					var currentMonthPos = monthNames.indexOf(month);
					var currentMonthDate = nlapiStringToDate((currentMonthPos + 1)
					        + '/28/' + requestedYear);

					if (currentMonthDate < new Date()) {
						// if
						// (projectWiseRevenue[project].RevenueData[practice][month].IsRecognised)
						// {
						html += "<td class='monthly-amount'>";
					} else {
						html += "<td class='projected-amount'>";
					}

					html += projectWiseRevenue[project].RevenueData[practice][month].Amount;
					html += "</td>";

				}
				html += "</tr>";
			}
		}

		html += "</table>";

		return html;
	} catch (err) {
		nlapiLogExecution('ERROR', 'createProjectionTable', err);
		throw err;
	}
}

function yearData() {
	var monthNames = [ "JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JUL", "AUG",
	        "SEP", "OCT", "NOV", "DEC" ];

	for (var i = 0; i < monthNames.length; i++) {
		this[monthNames[i]] = {
		    Amount : 0,
		    IsRecognised : true
		};
	}
}

function legendsTable() {
	var htmlText = "";
	htmlText += "<table class='legend-table'>";
	htmlText += "<tr><td class=''></td><td>Legends</td></tr>";
	htmlText += "<tr><td class='recognised-circle'></td><td>Recognised</td></tr>";
	htmlText += "<tr><td class='projected-circle'></td><td>Projected</td></tr>";
	htmlText += "</table>";
	return htmlText;
}

function getMonthName(currentDate) {
	currentDate = nlapiStringToDate(currentDate)
	var monthNames = [ "JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JUL", "AUG",
	        "SEP", "OCT", "NOV", "DEC" ];
	return monthNames[currentDate.getMonth()];
}

function getDataRefreshDate() {
	try {
		var refreshDate = '';

		var dateSearch = nlapiSearchRecord(
		        'customrecord_emp_practice_rev_projection', null, null,
		        new nlobjSearchColumn('formuladate', null, 'max')
		                .setFormula('{created}'));

		if (dateSearch) {
			refreshDate = dateSearch[0].getValue('formuladate', null, 'max');
		}

		return refreshDate;
	} catch (err) {
		nlapiLogExecution('ERROR', 'getDataRefreshDate', err);
		throw err;
	}
}

function isRefreshOngoing() {
	try {
		var isRefreshOnGoing = false;

		var search = nlapiSearchRecord('scheduledscriptinstance', null,
		        [
		                new nlobjSearchFilter('internalid', 'script', 'anyof',
		                        [ 938 ]),
		                new nlobjSearchFilter('status', null, 'anyof', [
		                        'RETRY', 'PROCESSING', 'PENDING' ]) ])

		if (search) {
			isRefreshOnGoing = true;
		}

		return isRefreshOnGoing;
	} catch (err) {
		nlapiLogExecution('ERROR', 'isRefreshOngoing', err);
		throw err;
	}
}
