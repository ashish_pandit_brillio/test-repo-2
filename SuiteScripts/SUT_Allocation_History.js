/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       17 Jun 2015     nitish.mishra
 *
 */

/**
 * @param {nlobjRequest}
 *            request Request object
 * @param {nlobjResponse}
 *            response Response object
 * @returns {Void} Any output is written via response object
 */
function suitelet(request, response) {
	try {
		var mode = request.getParameter("mode");

		if (mode == "export") {
			exportToCsv();
		} else {
			createAllocationHistoryReport();
		}
	} catch (err) {
		displayErrorForm(err, 'Allocation History');
	}
}

function createAllocationHistoryReport() {
	try {
		var mainData = getAllocationHistoryReportData();

		var html = "";

		if (isArrayNotEmpty(mainData)) {

			html += "<script type='text/javascript'"
					+ " src='https://system.na1.netsuite.com/core/media/media.nl?id=26996&c=3883006&h=d8fbf3dbfbb8590115c9&_xt=.js'>"
					+ "</script>";

			html += "<style>"
					+ ".addPadding { padding-top: 5px; padding-bottom: 5px;}"
					+ "</style>";

			html += "<style"
					+ " href='https://system.na1.netsuite.com/core/media/media.nl?id=27061&c=3883006&h=6e97cfe5ef73fcc58e66&_xt=.css'>"
					+ "</style>";

			var context = nlapiGetContext();
			var url = nlapiResolveURL('SUITELET', context.getScriptId(),
					context.getDeploymentId());

			html += "<a href='" + url
					+ "&mode=export' target='_blank'>Export To Excel</a>";
			html += "<br/><br/>";

			html += "<ul class='collapsibleList'>";

			mainData
					.forEach(function(customer) {

						html += "<li class='addPadding'>";
						html += "<div style='width:100%; border: solid black 1px; font-weight: bold;"
								+ "background-color: #c3d1de;'>"
								+ customer.Customer + "</div>";

						html += "<ul class='collapsibleList'>";
						customer.Projects
								.forEach(function(project) {

									html += "<li class='addPadding'>";
									html += "<div style='width:100%; border: solid black 1px; font-weight: bold;"
											+ "background-color: #FFFFE6;'>"
											+ project.Project + "</div>";
									html += "<ul class='collapsibleList'>";

									project.Resources
											.forEach(function(resource) {
												html += "<li class='addPadding'>";
												html += "<div style='width:100%; border: solid black 1px; font-weight: bold;"
														+ "background-color: #E5E5E5;'>"
														+ resource.Name
														+ "</div>";

												html += "<ul class='collapsibleList' style='list-style-type: none;'>";
												html += "<li>";
												html += "<table style='width:100%; padding-top:5px;'>";

												html += "<tr style='background-color:#4d5f79;color:white;'>";
												html += "<td>Project</td>";
												html += "<td>Start Date</td>";
												html += "<td>End Date</td>";
												html += "<td>Is Billable ?</td>";
												html += "<td>%age Allocation</td>";
												html += "<td>Billing Start Date</td>";
												html += "<td>Billing End Date</td>";
												html += "<td>Location</td>";
												html += "<td>Site</td>";
												html += "<td>Practice</td>";
												html += "<td>Project Manager</td>";
												html += "<td>Delivery Manager</td>";
												html += "</tr>";

												resource.Allocations
														.forEach(function(
																allocation) {

															html += "<tr style='background-color:#EBFFEB;'>";
															html += "<td>"
																	+ allocation.Project
																	+ "</td>";
															html += "<td>"
																	+ allocation.StartDate
																	+ "</td>";
															html += "<td>"
																	+ allocation.EndDate
																	+ "</td>";
															html += "<td>"
																	+ allocation.IsBillable
																	+ "</td>";
															html += "<td>"
																	+ allocation.PercentAllocation
																	+ "</td>";
															html += "<td>"
																	+ allocation.BillingStartDate
																	+ "</td>";
															html += "<td>"
																	+ allocation.BillingEndDate
																	+ "</td>";
															html += "<td>"
																	+ allocation.Location
																	+ "</td>";
															html += "<td>"
																	+ allocation.Site
																	+ "</td>";
															html += "<td>"
																	+ allocation.Practice
																	+ "</td>";
															html += "<td>"
																	+ allocation.ProjectManager
																	+ "</td>";
															html += "<td>"
																	+ allocation.DeliveryManager
																	+ "</td>";

															html += "</tr>";
														});
												html += "</table>";

												html += "</li>";
												html += "</ul>";

												html += "</li>";
											});
									html += "</ul>";
									html += "</li>";
								});

						html += "</ul>";
						html += "</li>";
					});

			html += "</ul>";

			html += "<script>";
			html += "CollapsibleLists.apply();";
			html += "</script>";
		}

		var form = nlapiCreateForm('Allocation History', false);
		form.addField('custpage_0', 'inlinehtml', '').setDefaultValue(html);
		response.writePage(form);
	} catch (err) {
		nlapiLogExecution('ERROR', 'createAllocationHistoryReport', err);
		throw err;
	}
}

function getAllocationHistoryReportData() {
	try {
		var currentUser = nlapiGetUser();

		// get the list of active projects under the current user
		var projectDetails = getRelatedProjects(currentUser);

		if (projectDetails.Customers.length == 0) {
			throw "No projects found.";
		}

		nlapiLogExecution('debug', 'Customers count',
				projectDetails.Customers.length);

		nlapiLogExecution('debug', 'project count',
				projectDetails.Project_Id.length);

		// get the list of all employees under these projects
		var allocated_employees = getProjectAllocationDetails(projectDetails.Project_Id);

		nlapiLogExecution('debug', 'employee count',
				allocated_employees.Employees.length);

		// get allocation history of all the employees
		var allocation_history_search = getEmployeeAllocationHistory(allocated_employees.Employees);

		nlapiLogExecution('debug', 'allocation history search count',
				allocation_history_search.length);

		// consolidate the result in one array
		var allocation_history_array = [];

		for (var l = 0; l < projectDetails.Customers.length; l++) {
			var current_customer_name = projectDetails.Customers[l].Text;
			var current_customer_id = projectDetails.Customers[l].Id;
			var projects = [];

			for (var i = 0; i < projectDetails.Projects.length; i++) {

				if (projectDetails.Projects[i].customerId == current_customer_id) {
					var current_project = projectDetails.Projects[i].projectId;
					var resources = [];

					for (var j = 0; j < allocated_employees.Allocation.length; j++) {

						if (allocated_employees.Allocation[j].Project == current_project) {
							var current_employee = allocated_employees.Allocation[j].Employee;
							var emp_allocation_list = [];

							for (var k = 0; k < allocation_history_search.length; k++) {

								if (allocation_history_search[k]
										.getValue('resource') == current_employee) {
									emp_allocation_list
											.push({
												Project : allocation_history_search[k]
														.getText('project'),
												StartDate : allocation_history_search[k]
														.getValue('startdate'),
												EndDate : allocation_history_search[k]
														.getValue('enddate'),
												IsBillable : isTrue(allocation_history_search[k]
														.getValue('custeventrbillable')) ? 'Yes'
														: 'No',
												PercentAllocation : allocation_history_search[k]
														.getValue('percentoftime'),
												BillingStartDate : allocation_history_search[k]
														.getValue('custeventbstartdate'),
												BillingEndDate : allocation_history_search[k]
														.getValue('custeventbenddate'),
												Location : allocation_history_search[k]
														.getText('custeventwlocation'),
												Site : allocation_history_search[k]
														.getText('custevent4'),
												Practice : allocation_history_search[k]
														.getText('custevent_practice'),
												ProjectManager : allocation_history_search[k]
														.getText(
																'custentity_projectmanager',
																'job'),
												DeliveryManager : allocation_history_search[k]
														.getText(
																'custentity_deliverymanager',
																'job')
											});
								}
							}

							resources
									.push({
										Name : allocated_employees.Allocation[j].EmployeeName,
										Allocations : emp_allocation_list
									});
						}

					}

					projects.push({
						Project : projectDetails.Projects[i].projectName,
						Resources : resources
					});
				}

			}

			allocation_history_array.push({
				Customer : current_customer_name,
				Projects : projects
			});
		}

		return allocation_history_array;

	} catch (err) {
		nlapiLogExecution('ERROR', 'getAllocationHistoryReportData', err);
		throw err;
	}
}

function getProjectAllocationDetails(projectIds) {
	try {

		var allocationSearch = searchRecord('resourceallocation', null, [
				new nlobjSearchFilter('project', null, 'anyof', projectIds),
				new nlobjSearchFilter('enddate', null, 'notbefore', 'today') ],
				[ new nlobjSearchColumn('resource'),
						new nlobjSearchColumn('project') ]);

		var allocatedResources = [];
		var employee_list = [];

		if (allocationSearch) {

			allocationSearch.forEach(function(allocation) {
				allocatedResources.push({
					EmployeeName : allocation.getText('resource'),
					Employee : allocation.getValue('resource'),
					Project : allocation.getValue('project')
				});
				employee_list.push(allocation.getValue('resource'));
			});
		}

		return {
			Allocation : allocatedResources,
			Employees : employee_list
		};
	} catch (err) {
		nlapiLogExecution('ERROR', 'getProjectAllocationDetails', err);
		throw err;
	}
}

function getRelatedProjects(currentUser) {
	try {

		var projectSearch = nlapiSearchRecord('job', null,
				[

						[
								[ 'custentity_projectmanager', 'anyof',
										currentUser ],
								'or',
								[ 'custentity_deliverymanager', 'anyof',
										currentUser ] ], 'and',
						[ 'status', 'anyof', '2' ] ], [
						new nlobjSearchColumn('entityid'),
						new nlobjSearchColumn('altname'),
						new nlobjSearchColumn('customer') ]);

		var projects = [];
		var project_id = [];
		var customers = [];

		if (projectSearch) {

			projectSearch.forEach(function(project) {
				projects.push({
					projectName : project.getValue('entityid') + " "
							+ project.getValue('altname'),
					// customer : project.getText('customer'),
					customerId : project.getValue('customer'),
					projectId : project.getId()
				});

				project_id.push(project.getId());

				customers.push({
					Id : project.getValue('customer'),
					Text : project.getText('customer')
				});
			});
		}

		var uniqueList = _.uniq(customers, function(item, key, Id) {
			return item.Id;
		});

		return {
			Customers : uniqueList,
			Projects : projects,
			Project_Id : project_id
		};

	} catch (err) {
		nlapiLogExecution('ERROR', 'getAllocationHistoryProjectWise', err);
		throw err;
	}
}

function getEmployeeAllocationHistory(employeeId) {
	try {
		var allocationSearch = nlapiSearchRecord(
				'resourceallocation',
				null,
				[ [ 'resource', 'anyof', employeeId ] ],
				[
						new nlobjSearchColumn('resource'),
						new nlobjSearchColumn('project'),
						new nlobjSearchColumn('startdate'),
						new nlobjSearchColumn('enddate'),
						new nlobjSearchColumn('custeventrbillable'),
						new nlobjSearchColumn('percentoftime'),
						new nlobjSearchColumn('custeventbstartdate'),
						new nlobjSearchColumn('custeventbenddate'),
						new nlobjSearchColumn('custeventwlocation'),
						new nlobjSearchColumn('custevent4'),
						new nlobjSearchColumn('custevent_practice'),
						new nlobjSearchColumn('custentity_projectmanager',
								'job'),
						new nlobjSearchColumn('custentity_deliverymanager',
								'job'), new nlobjSearchColumn('jobtype', 'job') ]);

		return allocationSearch;
	} catch (err) {
		nlapiLogExecution('ERROR', 'getEmployeeAllocationHistory', err);
		throw err;
	}
}

function exportToCsv() {
	try {
		var mainData = getAllocationHistoryReportData();
		var csv = "";

		csv += "Customer" + ",";
		csv += "Project" + ",";
		csv += "Resource" + ",";
		csv += "Allocation" + "\r\n";
		csv += ",,,";
		csv += "Project" + ",";
		csv += "Start Date" + ",";
		csv += "End Date" + ",";
		csv += "Is Billable" + ",";
		csv += "Percent Allocation" + ",";
		csv += "Billing Start Date" + ",";
		csv += "Billing End Date" + ",";
		csv += "Location" + ",";
		csv += "Site" + ",";
		csv += "Practice" + ",";
		csv += "Project Manager" + ",";
		csv += "Delivery Manager" + ",";
		csv += "\r\n";

		if (isArrayNotEmpty(mainData)) {

			mainData.forEach(function(customer) {

				customer.Projects.forEach(function(project) {

					project.Resources.forEach(function(resource) {

						resource.Allocations.forEach(function(allocation) {
							csv += customer.Customer + ",";
							csv += project.Project + ",";
							csv += resource.Name + ",";
							csv += allocation.Project + ",";
							csv += allocation.StartDate + ",";
							csv += allocation.EndDate + ",";
							csv += allocation.IsBillable + ",";
							csv += allocation.PercentAllocation + ",";
							csv += allocation.BillingStartDate + ",";
							csv += allocation.BillingEndDate + ",";
							csv += allocation.Location + ",";
							csv += allocation.Site + ",";
							csv += allocation.Practice + ",";
							csv += allocation.ProjectManager + ",";
							csv += allocation.DeliveryManager + ",";
							csv += "\r\n";
						});
					});
				});
			});
		}

		var fileName = 'Allocation History.csv';
		var file = nlapiCreateFile(fileName, 'CSV', csv);
		response.setContentType('CSV', fileName);
		response.write(file.getValue());
	} catch (err) {
		nlapiLogExecution('ERROR', 'exportToCsv', err);
		throw err;
	}
}