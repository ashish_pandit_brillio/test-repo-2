/**
 * @author Shweta
 */
// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
/*
   	Script Name : CLI_Purchase_Order_Billing_Dates.js
	Author      : Shweta Chopde
	Date        : 3 July 2014
	Description : Apply controls on Billing Dates


	Script Modification Log:

	-- Date --			-- Modified By --				--Requested By--				-- Description --




	Below is a summary of the process controls enforced by this script file.  The control logic is described
	more fully, below, in the appropriate function headers and code blocks.

     PAGE INIT
		- pageInit(type)


     SAVE RECORD
		- saveRecord()


     VALIDATE FIELD
		- validateField(type, name, linenum)


     FIELD CHANGED
		- fieldChanged(type, name, linenum)


     POST SOURCING
		- postSourcing(type, name)


	LINE INIT
		- lineInit(type)


     VALIDATE LINE
		- validateLine()


     RECALC
		- reCalc()


     SUB-FUNCTIONS
		- The following sub-functions are called by the above core functions in order to maintain code
            modularization:





*/
}
// END SCRIPT DESCRIPTION BLOCK  ====================================



// BEGIN SCRIPT UPDATION BLOCK  ====================================
/*


*/
// END SCRIPT UPDATION BLOCK  ====================================




// BEGIN GLOBAL VARIABLE BLOCK  =====================================
{

	//  Initialize any Global Variables, in particular, debugging variables...


}
// END GLOBAL VARIABLE BLOCK  =======================================





// BEGIN PAGE INIT ==================================================

function pageInit(type)
{
    /*  On page init:

	- PURPOSE

		FIELDS USED:

		--Field Name--				--ID--			--Line Item Name--

    */

    //  LOCAL VARIABLES


    //  PAGE INIT CODE BODY


}

// END PAGE INIT ====================================================





// BEGIN SAVE RECORD ================================================

function saveRecord()
{
    /*  On save record:

	- PURPOSE



	  FIELDS USED:

		--Field Name--			--ID--		--Line Item Name--


    */


    //  LOCAL VARIABLES



    //  SAVE RECORD CODE BODY


	return true;

}

// END SAVE RECORD ==================================================





// BEGIN VALIDATE FIELD =============================================

function validateField(type, name, linenum)
{

	/*  On validate field:

          - EXPLAIN THE PURPOSE OF THIS FUNCTION


          FIELDS USED:

          --Field Name--				--ID--


    */


	//  LOCAL VARIABLES



	//  VALIDATE FIELD CODE BODY


	return true;

}

// END VALIDATE FIELD ===============================================





// BEGIN FIELD CHANGED ==============================================

function fieldChanged_bill_dates(type, name, linenum)
{
  if(name == 'custbody_pofromdate')
  {
  	var d_PO_from = nlapiGetFieldValue('custbody_pofromdate');
	d_PO_from = nlapiStringToDate(d_PO_from);
	
	var d_PO_to = nlapiGetFieldValue('custbody_poto');
	d_PO_to = nlapiStringToDate(d_PO_to);
		
	if(d_PO_to!=null && d_PO_to!='' && d_PO_to!=undefined)
	{
		if(d_PO_from!=null && d_PO_from!='' && d_PO_from!=undefined)
		{
			if(d_PO_to<d_PO_from)
			{
				alert(' PO From Date should be greater than PO To Date')
				nlapiSetFieldValue('custbody_pofromdate','')				
			}//Check Dates	
		}//PO From				
	}//PO To
  	
  }//PO From	
  if(name == 'custbody_poto')
  {
  	var d_PO_from = nlapiGetFieldValue('custbody_pofromdate');
	d_PO_from = nlapiStringToDate(d_PO_from);
	
	var d_PO_to = nlapiGetFieldValue('custbody_poto');
	d_PO_to = nlapiStringToDate(d_PO_to);
	
	if(d_PO_to!=null && d_PO_to!='' && d_PO_to!=undefined)
	{	
		if(d_PO_from!=null && d_PO_from!='' && d_PO_from!=undefined)
		{		
			if(d_PO_to < d_PO_from)
			{
				alert(' PO From Date should be greater than PO To Date')
				nlapiSetFieldValue('custbody_poto','')				
			}//Check Dates			
		}//PO From				
	}//PO To	
  	
  }//PO To
	
  
}

// END FIELD CHANGED ================================================





// BEGIN POST SOURCING ==============================================

function postSourcing(type, name)
{

    /*  On post sourcing:

	- PURPOSE

          FIELDS USED:

		--Field Name--			--ID--		--Line Item Name--


    */

    //  LOCAL VARIABLES


    //  POST SOURCING CODE BODY



}

// END POST SOURCING ================================================



// BEGIN LINE INIT ==============================================

function lineInit(type)
{

    /*  On Line Init:

	- PURPOSE

          FIELDS USED:

		--Field Name--			--ID--		--Line Item Name--


    */

    //  LOCAL VARIABLES


    //  LINE INIT CODE BODY



}

// END LINE INIT ================================================


// BEGIN VALIDATE LINE ==============================================

function validateLine(type)
{

	/*  On validate line:

          - EXPLAIN THE PURPOSE OF THIS FUNCTION


          FIELDS USED:

          --Field Name--				--ID--


    */


	//  LOCAL VARIABLES


	//  VALIDATE LINE CODE BODY


	return true;

}

// END VALIDATE LINE ================================================




// BEGIN RECALC =====================================================

function recalc(type)
{

	/*  On recalc:

          - EXPLAIN THE PURPOSE OF THIS FUNCTION


          FIELDS USED:

          --Field Name--				--ID--


    */


	//  LOCAL VARIABLES


	//  RECALC CODE BODY


}

// END RECALC =======================================================





// BEGIN FUNCTION ===================================================




// END FUNCTION =====================================================
