/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       05 Oct 2016     shruthi.l
 *
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType
 * 
 * @param {String} type Operation types: create, edit, delete, xedit
 *                      approve, reject, cancel (SO, ER, Time Bill, PO & RMA only)
 *                      pack, ship (IF)
 *                      markcomplete (Call, Task)
 *                      reassign (Case)
 *                      editforecast (Opp, Estimate)
 * @returns {Void}
 */
function userEventBeforeSubmit(type){

	 
	var daysOfWeek = [ 'sunday', 'monday', 'tuesday', 'wednesday','thursday', 'friday', 'saturday' ];
	var lineitemcount=nlapiGetLineItemCount('timegrid');
	
	var totalHours = nlapiGetFieldValue('totalhours').substring(':');
	totalHours = totalHours.split(':');
	totalHours = totalHours[0];
	
	var timeArray;
	var timeEntered ;
	var totalHours_PT = 0; 
	//var flag=0;
	for(var i=1;i<=lineitemcount;i++){
		nlapiSelectLineItem('timegrid', i);
		for (var j = 0; j < 7; j++) 
		{
			var subrecord = daysOfWeek[j];
		//	alert("subrecord : "+ subrecord);
			var indx = nlapiViewCurrentLineItemSubrecord('timegrid', subrecord);
		//	alert("day : "+ indx);
			if(indx)
			{
				var customer = indx.getFieldValue('customer');// item
				var item = indx.getFieldValue('item');
				 timeArray = nlapiLookupField('job', customer, ['custentity_hoursperday','custentity_hoursperweek']);
				 timeEntered = indx.getFieldValue('hours');
				// timeEntered = indx.getFieldValue('hours');
				 if(item == 2222 || item == 2221){
				 totalHours_PT = parseFloat(totalHours_PT) + parseFloat(timeEntered) ;
                  
				 }
               
			// alert("Inside : " + timeArray.custentity_hoursperweek );
			// nlapiLogExecution('Debug', 'inside for Hours per week',timeArray.custentity_hoursperweek );
				if(timeArray.custentity_hoursperday){
					if(parseFloat(timeEntered) > parseFloat(timeArray.custentity_hoursperday) && (item == 2222 || item == 2221)){
						//alert("Please enter hours per day Lesser than or equal to : "+ timeArray.custentity_hoursperday);
						var err = nlapiCreateError('404', "Please enter hours per day Lesser than or equal to : " + timeArray.custentity_hoursperday);
						throw err;
					}
				}
			}
			
		}
	}
	nlapiLogExecution('Debug','BeforetotalHours' ,totalHours_PT);
	totalHours_PT= Math.round(totalHours_PT);
	nlapiLogExecution('Debug','AftertotalHours' ,totalHours_PT);
	//alert("Outside : totalHours " + totalHours +"  timeArray.custentity_hoursperweek : "+timeArray.custentity_hoursperweek );
	//nlapiLogExecution('Debug', 'outside for Hours per week',timeArray.custentity_hoursperweek );
	
		 if(timeArray.custentity_hoursperweek){
			if(parseFloat(totalHours_PT) > parseFloat(timeArray.custentity_hoursperweek) ){ //&& ((item == 2222 || item == 2221)
				//alert("Please enter hours per week Lesser than or equal to : " + timeArray.custentity_hoursperweek);
				var err =  nlapiCreateError('404', "Please enter hours per week Lesser than or equal to : " + timeArray.custentity_hoursperweek);
				throw err;
			}
		} 
		
   

}
