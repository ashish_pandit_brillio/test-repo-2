/**
 * Contains the constants for the application
 * 
 * Version Date Author Remarks 1.00 08 Sep 2015 nitish.mishra
 * 
 */

var K_CONSTANTS = {

};

var K_RECORDS = {
	MonthlyIncomeStatement : 'customrecord_monthly_income_statement'
};

K_MONTHLY_INCOME_STATEMENT = {
    Account : 'custrecord_mis_account',
    TypeId : 'custrecord_mis_transaction_type_id',
    TypeName : 'custrecord_mis_transaction_type_name',
    TranId : 'custrecord_mis_transaction_id',
    Date : 'custrecord_mis_transaction_date',
    Number : 'custrecord_mis_transaction_number',
    Amount : 'custrecord_mis_amount',
    Customer : 'custrecord_mis_customer',
    Project : 'custrecord_mis_project',
    Employee : 'custrecord_mis_employee',
    Practice : 'custrecord_mis_practice',
    Vertical : 'custrecord_mis_vertical',
    Location : 'custrecord_mis_location',
    Description : 'custrecord_mis_description',
    AmountApplied : 'custrecord_mis_amount_applied',
    DiscountApplied : 'custrecord_mis_discount_applied',
    TotalAmount : 'custrecord_mis_total_amount',
    TotalDiscount : 'custrecord_mis_total_discount_applied',
    EntityName : 'custrecord_mis_entity_name',
    Milestone : 'custrecord_mis_milestone'
};

function MonthlyIncomeStatement() {
	this.Account = null;
	this.TypeId = null;
	this.TypeName = null;
	this.TranId = null;
	this.Date = null;
	this.Number = null;
	this.Amount = 0;
	this.Customer = null;
	this.Project = null;
	this.Employee = '';
	this.Practice = null;
	this.Vertical = null;
	this.Location = null;
	this.Description = null;
	this.AmountApplied = 0;
	this.TotalAmount = 0;
	this.TotalDiscount = 0;
	this.EntityName = '';
	this.Milestone = '';
}