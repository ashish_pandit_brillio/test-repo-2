/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       08 Aug 2016     shruthi.l
 *
 */

/**
 * @param {String} type Context Types: scheduled, ondemand, userinterface, aborted, skipped
 * @returns {Void}
 */
function scheduled(type) {
	  try
	  {
	  	  
	  var columns = new Array();
	  columns[0]  =new nlobjSearchColumn('firstname');
	  columns[1]  =new nlobjSearchColumn('email');
	  columns[2]  =new nlobjSearchColumn('email','custentity_reportingmanager');
	  columns[3]  =new nlobjSearchColumn('firstname', 'custentity_reportingmanager');
	  columns[4]  =new nlobjSearchColumn('internalid');
	  columns[5]  =new nlobjSearchColumn('custentity_probationenddate');
	  columns[6]  =new nlobjSearchColumn('custentity_reportingmanager');
	  columns[7]  =new nlobjSearchColumn('custentity_future_termination_date', 'custentity_reportingmanager');
	  columns[8]  =new nlobjSearchColumn('internalid', 'custentity_reportingmanager');
	  columns[9]  =new nlobjSearchColumn('custentity_fusion_empid');
	  columns[10]  =new nlobjSearchColumn('custentity_extended_probation_date');
	  //columns[11]= new nlobjSearchColumn('custrecord_hrbusinesspartner','department');
	  // Commented by shravan for HRBP integration on 7-dec-2020
	  columns[11]  =new nlobjSearchColumn('custentity_emp_hrbp');
	  columns[12] = new nlobjSearchColumn('custentity_future_term_date');
	  /*
	   * Search Employees with probation end date ends in 10 days
	   * 
	   */
	  var emp_search = nlapiSearchRecord('employee', 'customsearch_confirmation_firstphase_10d', null,columns);
	  /*
	   * Search Employees with probation end date ends in 3 days
	   * 
	   */
	  var emp_search2 = nlapiSearchRecord('employee', 'customsearch_confirmation_firstphase_3d', null,columns);
	  
	  /*
	   * Search Employees with Extended probation end date ends in 10 days
	   * 
	   */
	  var emp_search3 = nlapiSearchRecord('employee', 'customsearch_confirmation_extended_10d', null,columns);

	  
	  /*
	   * Search Employees with Extended probation end date ends in 3 days
	   * 
	   */
	  var emp_search4 = nlapiSearchRecord('employee', 'customsearch_confirmation_extended_3d', null,columns);
	  
	  /*
	   * Search Employees with Extended probation end date ends in 3 days
	   * 
	   */
	  var emp_search5 = nlapiSearchRecord('employee', 'customsearch_confirmation_extended_2d', null,columns);
	  
	  //mani
	  
	 if(isNotEmpty(emp_search))
	  {
	nlapiLogExecution('debug', 'emp_search.length',emp_search.length);
	
	  	for(var i=0;i<emp_search.length;i++)
	  	{
			//var date = emp_search[i].getValue('custentity_future_term_date');
			if(!_logValidation(emp_search[i].getValue('custentity_future_term_date'))){
			//var hr_bp_id = emp_search[i].getValue('custrecord_hrbusinesspartner','department');
			// Commented by shravan for HRBP integration on 7-dec-2020
var hr_bp_id = emp_search[i].getValue('custentity_emp_hrbp');
/// Added by shravan for HRBP integration on 7-dec-2020
			var hr_bp_email = null;
		   
		   if(isNotEmpty(hr_bp_id)){
					hr_bp_email = nlapiLookupField('employee',hr_bp_id, 'email');
					
				}
				else{
						hr_bp_email = 'hrbpteam@brillio.com' ;//'hrb@brillio.com';
					} /// Added by shravan K
		
	  		var rm_email = emp_search[i].getValue('email','custentity_reportingmanager');
	  		var rm_firstname=emp_search[i].getValue('firstname', 'custentity_reportingmanager');
	  		//nlapiLogExecution('debug', 'termination date',emp_search[i].getValue('custentity_future_termination_date','custentity_reportingmanager'));
	  		//nlapiLogExecution('debug', 'internal ID of RM',emp_search[i].getValue('internalid','custentity_reportingmanager'));
	  		
	  		/*
	  		 * If the employee's reporting manager is serving notice period then set rm_email to his reporting manager's email ID
	  		 * 
	  		 * */
	  		if(emp_search[i].getValue('custentity_future_termination_date','custentity_reportingmanager'))
	  		{
	  			var rmRecord = nlapiLoadRecord('employee', emp_search[i].getValue('internalid','custentity_reportingmanager'));
	  			nlapiLogExecution('debug', 'rmRecord',rmRecord);
	  			var rm_rmRecord = nlapiLoadRecord('employee', rmRecord.getFieldValue('custentity_reportingmanager'));
	  			nlapiLogExecution('debug', 'rm_rmRecord',rm_rmRecord);
	  			
	  			rm_email=rm_rmRecord.getFieldValue('email');
	  			rm_firstname = rm_email.getFieldValue('firstname');
	  			nlapiLogExecution('debug', 'insiderm_email',rm_email);
	  		}
	  		
	       sendServiceMails(
	  			emp_search[i].getValue('firstname'),
	  			rm_email,
	  			rm_firstname,
	  			emp_search[i].getValue('internalid'),
	  			emp_search[i].getValue('custentity_probationenddate'), 
	  			emp_search[i].getValue('custentity_reportingmanager'),hr_bp_email,
				emp_search[i].getId());
	  	}
		else
		{
			var id = emp_search[i].getValue('internalid');
			var Rec = nlapiLoadRecord('employee',id);
			Rec.setFieldValue('custentity_probationenddate','');
			nlapiSubmitRecord(Rec,true,true);
		}
	  }
	  }
	  
	  
	  if(isNotEmpty(emp_search2))
	  {
	  	nlapiLogExecution('debug', 'emp_search.length',emp_search2.length);
	  	for(var i=0;i<emp_search2.length;i++)
	  	{
			if(!_logValidation(emp_search2[i].getValue('custentity_future_term_date'))){
	  	// var hr_bp_id = emp_search2[i].getValue('custrecord_hrbusinesspartner','department');
			// Commented by shravan for HRBP integration on 7-dec-2020
var hr_bp_id = emp_search2[i].getValue('custentity_emp_hrbp');
/// Added by shravan for HRBP integration on 7-dec-2020
		 var hr_bp_email = null;
		   
		   if(isNotEmpty(hr_bp_id)){
					hr_bp_email = nlapiLookupField('employee',hr_bp_id, 'email');
					
				}
				else{
						hr_bp_email = 'hrbpteam@brillio.com' ;//'hrb@brillio.com';
					} /// Added by shravan K
			var rm_email = emp_search2[i].getValue('email','custentity_reportingmanager');
	  		var rm_firstname=emp_search2[i].getValue('firstname', 'custentity_reportingmanager');
	  		//nlapiLogExecution('debug', 'termination date',emp_search[i].getValue('custentity_future_termination_date','custentity_reportingmanager'));
	  		//nlapiLogExecution('debug', 'internal ID of RM',emp_search[i].getValue('internalid','custentity_reportingmanager'));
	  		
	  		/*
	  		 * If the employee's reporting manager is serving notice period then set rm_email to his reporting manager's email ID
	  		 * 
	  		 * */
	  		if(emp_search2[i].getValue('custentity_future_termination_date','custentity_reportingmanager'))
	  		{
	  			var rmRecord = nlapiLoadRecord('employee', emp_search2[i].getValue('internalid','custentity_reportingmanager'));
	  			nlapiLogExecution('debug', 'rmRecord',rmRecord);
	  			var rm_rmRecord = nlapiLoadRecord('employee', rmRecord.getFieldValue('custentity_reportingmanager'));
	  			nlapiLogExecution('debug', 'rm_rmRecord',rm_rmRecord);
	  			
	  			rm_email=rm_rmRecord.getFieldValue('email');
	  			rm_firstname = rm_email.getFieldValue('firstname');
	  			nlapiLogExecution('debug', 'insiderm_email',rm_email);
	  		}
	  		
	       sendServiceMails2(
	  			emp_search2[i].getValue('firstname'),
	  			rm_email,
	  			rm_firstname,
	  			emp_search2[i].getValue('internalid'),
	  			emp_search2[i].getValue('custentity_probationenddate'), 
	  			emp_search2[i].getValue('custentity_reportingmanager'),hr_bp_email);
	  	}
		else
		{
			var id = emp_search2[i].getValue('internalid');
			var Rec = nlapiLoadRecord('employee',id);
			Rec.setFieldValue('custentity_probationenddate','');
			nlapiSubmitRecord(Rec,true,true);
		}
		}
	  }
	  
	  if(isNotEmpty(emp_search3))
	  {
	  	nlapiLogExecution('debug', 'emp_search.length',emp_search3.length);
	  	for(var i=0;i<emp_search3.length;i++)
	  	{
	  	if(!_logValidation(emp_search3[i].getValue('custentity_future_term_date')))
		{			
		//var hr_bp_id = emp_search3[i].getValue('custrecord_hrbusinesspartner','department');
			// Commented by shravan for HRBP integration on 7-dec-2020
var hr_bp_id = emp_search3[i].getValue('custentity_emp_hrbp');
/// Added by shravan for HRBP integration on 7-dec-2020
		var hr_bp_email = null;
		   
		   if(isNotEmpty(hr_bp_id)){
					hr_bp_email = nlapiLookupField('employee',hr_bp_id, 'email');
					
				}
				else{
						hr_bp_email = 'hrbpteam@brillio.com' ;//'hrb@brillio.com';
					} /// Added by shravan K
	  		var rm_email = emp_search3[i].getValue('email','custentity_reportingmanager');
	  		var rm_firstname=emp_search3[i].getValue('firstname', 'custentity_reportingmanager');
	  		//nlapiLogExecution('debug', 'termination date',emp_search[i].getValue('custentity_future_termination_date','custentity_reportingmanager'));
	  		//nlapiLogExecution('debug', 'internal ID of RM',emp_search[i].getValue('internalid','custentity_reportingmanager'));
	  		
	  		/*
	  		 * If the employee's reporting manager is serving notice period then set rm_email to his reporting manager's email ID
	  		 * 
	  		 * */
	  		if(emp_search3[i].getValue('custentity_future_termination_date','custentity_reportingmanager'))
	  		{
	  			var rmRecord = nlapiLoadRecord('employee', emp_search3[i].getValue('internalid','custentity_reportingmanager'));
	  			nlapiLogExecution('debug', 'rmRecord',rmRecord);
	  			var rm_rmRecord = nlapiLoadRecord('employee', rmRecord.getFieldValue('custentity_reportingmanager'));
	  			nlapiLogExecution('debug', 'rm_rmRecord',rm_rmRecord);
	  			
	  			rm_email=rm_rmRecord.getFieldValue('email');
	  			rm_firstname = rm_email.getFieldValue('firstname');
	  			nlapiLogExecution('debug', 'insiderm_email',rm_email);
	  		}
	  		var filt= new Array();
			filt[0]  = new nlobjSearchFilter('custrecord_employee_fusion_num',null,'is',emp_search3[i].getValue('custentity_fusion_empid'));
			var col=new Array();
			col[0] = new nlobjSearchColumn('custrecord_extension_month');
			col[1] = new nlobjSearchColumn('custrecord_extension_comments');
			var feedback_rec = nlapiSearchRecord('customrecord_emp_confirmation_feedback',null,filt,col);
	  		
			nlapiLogExecution('DEBUG', "Feebdabckrec", feedback_rec[0].getId());
	       
			sendServiceMails3(
	  			emp_search3[i].getValue('firstname'),
	  			rm_email,
	  			rm_firstname,
	  			emp_search3[i].getValue('internalid'),
	  			emp_search3[i].getValue('custentity_probationenddate'), 
	  			emp_search3[i].getValue('custentity_reportingmanager'),hr_bp_email,

	  			feedback_rec[0].getId(),
	  			emp_search3[i].getValue('custentity_extended_probation_date'),
	  			feedback_rec[0].getValue('custrecord_extension_month'),
	  			feedback_rec[0].getValue('custrecord_extension_comments'));
	  	}
		else
		{
			var id = emp_search3[i].getValue('internalid');
			var Rec = nlapiLoadRecord('employee',id);
			Rec.setFieldValue('custentity_probationenddate','');
			nlapiSubmitRecord(Rec,true,true);
		}
		}
	  }
	  
	  if(isNotEmpty(emp_search4))
	  {
	  	nlapiLogExecution('debug', 'emp_search.length',emp_search4.length);
	  	for(var i=0;i<emp_search4.length;i++)
	  	{
	  		if(!_logValidation(emp_search4[i].getValue('custentity_future_term_date'))){
	  // var hr_bp_id = emp_search4[i].getValue('custrecord_hrbusinesspartner','department');
	  	// Commented by shravan for HRBP integration on 7-dec-2020
var hr_bp_id = emp_search4[i].getValue('custentity_emp_hrbp');
/// Added by shravan for HRBP integration on 7-dec-2020
	   var hr_bp_email = null;
		   
		   if(isNotEmpty(hr_bp_id)){
					hr_bp_email = nlapiLookupField('employee',hr_bp_id, 'email');
					
				}	
				else{
						hr_bp_email = 'hrbpteam@brillio.com' ;//'hrb@brillio.com';
					} /// Added by shravan K
      		var rm_email = emp_search4[i].getValue('email','custentity_reportingmanager');
	  		var rm_firstname=emp_search4[i].getValue('firstname', 'custentity_reportingmanager');
	  		//nlapiLogExecution('debug', 'termination date',emp_search[i].getValue('custentity_future_termination_date','custentity_reportingmanager'));
	  		//nlapiLogExecution('debug', 'internal ID of RM',emp_search[i].getValue('internalid','custentity_reportingmanager'));
	  		
	  		/*
	  		 * If the employee's reporting manager is serving notice period then set rm_email to his reporting manager's email ID
	  		 * 
	  		 * */
	  		if(emp_search4[i].getValue('custentity_future_termination_date','custentity_reportingmanager'))
	  		{
	  			var rmRecord = nlapiLoadRecord('employee', emp_search4[i].getValue('internalid','custentity_reportingmanager'));
	  			nlapiLogExecution('debug', 'rmRecord',rmRecord);
	  			var rm_rmRecord = nlapiLoadRecord('employee', rmRecord.getFieldValue('custentity_reportingmanager'));
	  			nlapiLogExecution('debug', 'rm_rmRecord',rm_rmRecord);
	  			
	  			rm_email=rm_rmRecord.getFieldValue('email');
	  			rm_firstname = rm_email.getFieldValue('firstname');
	  			nlapiLogExecution('debug', 'insiderm_email',rm_email);
	  		}
	  		var filt= new Array();
			filt[0]  = new nlobjSearchFilter('custrecord_employee_fusion_num',null,'is',emp_search4[i].getValue('custentity_fusion_empid'));
			var col=new Array();
			col[0] = new nlobjSearchColumn('custrecord_extension_month');
			col[1] = new nlobjSearchColumn('custrecord_extension_comments');
			var feedback_rec = nlapiSearchRecord('customrecord_emp_confirmation_feedback',null,filt,col);
	  		
			nlapiLogExecution('DEBUG', "Feebdabckrec", feedback_rec[0].getId());
	       
			sendServiceMails4(
	  			emp_search4[i].getValue('firstname'),
	  			rm_email,
	  			rm_firstname,
	  			emp_search4[i].getValue('internalid'),
	  			emp_search4[i].getValue('custentity_probationenddate'), 
	  			emp_search4[i].getValue('custentity_reportingmanager'),hr_bp_email,
	  			feedback_rec[0].getId(),
	  			emp_search4[i].getValue('custentity_extended_probation_date'),
	  			feedback_rec[0].getValue('custrecord_extension_month'),
	  			feedback_rec[0].getValue('custrecord_extension_comments'));
	  	}
		else
		{
			var id = emp_search4[i].getValue('internalid');
			var Rec = nlapiLoadRecord('employee',id);
			Rec.setFieldValue('custentity_probationenddate','');
			nlapiSubmitRecord(Rec,true,true);
		}
		}
	  }
	  
	  if(isNotEmpty(emp_search5))
	  {
	  	nlapiLogExecution('debug', 'emp_search.length',emp_search5.length);
	  	for(var i=0;i<emp_search5.length;i++)
	  	{
			if(!_logValidation(emp_search5[i].getValue('custentity_future_term_date')))
			{
		 //var hr_bp_id = emp_search5[i].getValue('custrecord_hrbusinesspartner','department');
		 	// Commented by shravan for HRBP integration on 7-dec-2020
var hr_bp_id = emp_search5[i].getValue('custentity_emp_hrbp');
/// Added by shravan for HRBP integration on 7-dec-2020
		 var hr_bp_email = null;
		   
		   if(isNotEmpty(hr_bp_id)){
					hr_bp_email = nlapiLookupField('employee',hr_bp_id, 'email');
					
				}
				else{
						hr_bp_email = 'hrbpteam@brillio.com' ;//'hrb@brillio.com';
					} /// Added by shravan K
	  		
	  		var rm_email = emp_search5[i].getValue('email','custentity_reportingmanager');
	  		var rm_firstname=emp_search5[i].getValue('firstname', 'custentity_reportingmanager');
	  		//nlapiLogExecution('debug', 'termination date',emp_search[i].getValue('custentity_future_termination_date','custentity_reportingmanager'));
	  		//nlapiLogExecution('debug', 'internal ID of RM',emp_search[i].getValue('internalid','custentity_reportingmanager'));
	  		
	  		/*
	  		 * If the employee's reporting manager is serving notice period then set rm_email to his reporting manager's email ID
	  		 * 
	  		 * */
	  		if(emp_search5[i].getValue('custentity_future_termination_date','custentity_reportingmanager'))
	  		{
	  			var rmRecord = nlapiLoadRecord('employee', emp_search5[i].getValue('internalid','custentity_reportingmanager'));
	  			nlapiLogExecution('debug', 'rmRecord',rmRecord);
	  			var rm_rmRecord = nlapiLoadRecord('employee', rmRecord.getFieldValue('custentity_reportingmanager'));
	  			nlapiLogExecution('debug', 'rm_rmRecord',rm_rmRecord);
	  			
	  			rm_email=rm_rmRecord.getFieldValue('email');
	  			rm_firstname = rm_email.getFieldValue('firstname');
	  			nlapiLogExecution('debug', 'insiderm_email',rm_email);
	  		}
	  		var filt= new Array();
			filt[0]  = new nlobjSearchFilter('custrecord_employee_fusion_num',null,'is',emp_search5[i].getValue('custentity_fusion_empid'));
			var col=new Array();
			col[0] = new nlobjSearchColumn('custrecord_extension_month');
			col[1] = new nlobjSearchColumn('custrecord_extension_comments');
			var feedback_rec = nlapiSearchRecord('customrecord_emp_confirmation_feedback',null,filt,col);
	  		
			nlapiLogExecution('DEBUG', "Feebdabckrec", feedback_rec[0].getId());
	       
			sendServiceMails5(
	  			emp_search5[i].getValue('firstname'),
	  			rm_email,
	  			emp_search5[i].getText('custentity_reportingmanager'),hr_bp_email);
	  	}
		else
		{
			var id = emp_search5[i].getValue('internalid');
			var Rec = nlapiLoadRecord('employee',id);
			Rec.setFieldValue('custentity_probationenddate','');
			nlapiSubmitRecord(Rec,true,true);
		}
		}
	  }
	  
	  }
	  catch(err){
	  nlapiLogExecution('error', 'Main', err);
	  }
}

function sendServiceMails(firstName, reporting_mng_email, reporting_mng_first_name, employee_internal_id, probation_end_date, reporting_mng_id,hr_bp_email)
{
	
	try{       
		var fileObj = nlapiLoadFile(175408); //PDF that needs to be attached
		var mailTemplate = serviceTemplate(firstName, reporting_mng_email, reporting_mng_first_name, employee_internal_id, probation_end_date);		
		nlapiLogExecution('debug', 'checkpoint',reporting_mng_email);
		nlapiLogExecution('debug', 'mailTemplate',mailTemplate.MailBody);
		nlapiSendEmail(10730, reporting_mng_email, mailTemplate.MailSubject, mailTemplate.MailBody,hr_bp_email,null,{ entity : reporting_mng_id},fileObj,null);
		
	}
catch(err){
          nlapiLogExecution('error', 'sendServiceMails', err);
          throw err;
     }
}

function sendServiceMails2(firstName, reporting_mng_email, reporting_mng_first_name, employee_internal_id, probation_end_date, reporting_mng_id,hr_bp_email)
{
	
	try{       
		var fileObj = nlapiLoadFile(175408); //PDF that needs to be attached
		var mailTemplate = serviceTemplate2(firstName, reporting_mng_email, reporting_mng_first_name, employee_internal_id, probation_end_date);		
		nlapiLogExecution('debug', 'checkpoint',reporting_mng_email);
		nlapiLogExecution('debug', 'mailTemplate',mailTemplate.MailBody);
		nlapiSendEmail(10730, reporting_mng_email, mailTemplate.MailSubject, mailTemplate.MailBody,hr_bp_email,null,{ entity : reporting_mng_id},fileObj,null);
		
	}
catch(err){
          nlapiLogExecution('error', 'sendServiceMails', err);
          throw err;
     }
}

function sendServiceMails3(firstName, reporting_mng_email, reporting_mng_first_name, employee_internal_id, probation_end_date, reporting_mng_id,hr_bp_email,feedback_id,extended_prob_end_date,ext_month,ext_comments)
{
	
	try{       
		var fileObj = nlapiLoadFile(175408); //PDF that needs to be attached
		var mailTemplate = serviceTemplate3(firstName, reporting_mng_email, reporting_mng_first_name, employee_internal_id, probation_end_date,feedback_id,extended_prob_end_date,ext_month,ext_comments);		
		nlapiLogExecution('debug', 'checkpoint',reporting_mng_email);
		nlapiLogExecution('debug', 'mailTemplate',mailTemplate.MailBody);
		nlapiSendEmail(10730, reporting_mng_email, mailTemplate.MailSubject, mailTemplate.MailBody,hr_bp_email,null,{ entity : reporting_mng_id},fileObj,null);
		
	}
catch(err){
          nlapiLogExecution('error', 'sendServiceMails', err);
          throw err;
     }
}

function sendServiceMails4(firstName, reporting_mng_email, reporting_mng_first_name, employee_internal_id, probation_end_date, reporting_mng_id,hr_bp_email,feedback_id,extended_prob_end_date)
{
	
	try{       
		var fileObj = nlapiLoadFile(175408); //PDF that needs to be attached
		var mailTemplate = serviceTemplate4(firstName, reporting_mng_email, reporting_mng_first_name, employee_internal_id, probation_end_date,feedback_id,extended_prob_end_date);		
		nlapiLogExecution('debug', 'checkpoint',reporting_mng_email);
		nlapiLogExecution('debug', 'mailTemplate',mailTemplate.MailBody);
		nlapiSendEmail(10730, reporting_mng_email, mailTemplate.MailSubject, mailTemplate.MailBody,hr_bp_email,null,{ entity : reporting_mng_id},fileObj,null);
		
	}
catch(err){
          nlapiLogExecution('error', 'sendServiceMails', err);
          throw err;
     }
}

function sendServiceMails5(firstName, reporting_mng_email,reporting_mng,hr_bp_email)
{
	
	try{       
		//var fileObj = nlapiLoadFile(175408); //PDF that needs to be attached
		var mailTemplate = serviceTemplate5(firstName, reporting_mng_email,reporting_mng);		
		//nlapiLogExecution('debug', 'checkpoint',reporting_mng_email);
		nlapiLogExecution('debug', 'mailTemplate',mailTemplate.MailBody);
		nlapiSendEmail(10730, 'japnit.sethi@brillio.com', mailTemplate.MailSubject, mailTemplate.MailBody,hr_bp_email,null,{ entity : reporting_mng_id},null,null);
		
	}
catch(err){
          nlapiLogExecution('error', 'sendServiceMails', err);
          throw err;
     }
}

function serviceTemplate(firstName, reporting_mng_email, reporting_mng_first_name, employee_internal_id, probation_end_date) 
{
    
	var feedbackFormUrl = 'https://forms.na1.netsuite.com/app/site/hosting/scriptlet.nl?script=996&deploy=1&compid=3883006&h=8bad2f9478c47330e994';
	feedbackFormUrl +='&custparam_employee_id='+employee_internal_id+'&feedback_record_id=' ;
	
	     
	var htmltext = '';
    
   htmltext += '<table border="0" width="100%"><tr>';
   htmltext += '<td colspan="4" valign="top">';
  
   htmltext += '<p>Hi  ' + reporting_mng_first_name + ',</p>';
   
   htmltext += '<p> This is to bring to your notice that '+firstName+' is due for confirmation on '+probation_end_date+'. Please click this <a href  ="'+feedbackFormUrl+'" >Link</a> and provide your inputs. The system will trigger an email to your reportee accordingly.</p>';
 
   htmltext +='<p>Please find attached the process document for your reference.</p>';
   htmltext +='<p>Note: if you do not provide your feedback before the due date, the system shall trigger the confirmation email to your reportee on the due date.</p>'
   
   htmltext += '<p>Thanks & Regards,</p>';
   htmltext += '<p>Team HR</p>';
 
 
    htmltext += '</td></tr>';
    htmltext += '</table>';
    htmltext += '<hr width="100%" size="1" noshade color="#CCCCCC">';
    htmltext += '<table width="100%" border="0" cellspacing="0" cellpadding="0">';
    htmltext += '<tr>';
    htmltext += '<td align="right">';
    htmltext += '<font style="font-size:9px; font-family:Verdana,Arial,Helvetica,sans-serif; color:#999999;">Brillio is powered by <a href="http://www.netsuite.com/" style="color:#999999;">NetSuite</a> One System. No Limits.</font>';
    htmltext += '</td>';
    htmltext += '</tr>';
    htmltext += '</table>';
    htmltext += '</body>';
    htmltext += '</html>';
 
    return {
        MailBody : htmltext,
        MailSubject : "Confirmation feedback initiation"      
    };
}


function serviceTemplate2(firstName, reporting_mng_email, reporting_mng_first_name, employee_internal_id, probation_end_date) 
{
    
	var feedbackFormUrl = 'https://forms.na1.netsuite.com/app/site/hosting/scriptlet.nl?script=996&deploy=1&compid=3883006&h=8bad2f9478c47330e994';
	feedbackFormUrl +='&custparam_employee_id='+employee_internal_id+'&feedback_record_id=' ;
	
	     
	var htmltext = '';
    
   htmltext += '<table border="0" width="100%"><tr>';
   htmltext += '<td colspan="4" valign="top">';
  
   htmltext += '<p>Hi  ' + reporting_mng_first_name + ',</p>';
   
   htmltext += '<p> This is to bring to your notice that '+firstName+' is due for confirmation on '+probation_end_date+'. Please click this <a href  ="'+feedbackFormUrl+'" >Link</a> and provide your inputs. The system will trigger an email to your reportee accordingly.</p>';
   htmltext +='<p>Please find attached the process document for your reference.</p>';
   htmltext +='<p>Note: if you do not provide your feedback before the due date, the system shall trigger the confirmation email to your reportee on the due date.</p>'
   htmltext += '<p>Thanks & Regards,</p>';
   htmltext += '<p>Team HR</p>';
 
 
    htmltext += '</td></tr>';
    htmltext += '</table>';
    htmltext += '<hr width="100%" size="1" noshade color="#CCCCCC">';
    htmltext += '<table width="100%" border="0" cellspacing="0" cellpadding="0">';
    htmltext += '<tr>';
    htmltext += '<td align="right">';
    htmltext += '<font style="font-size:9px; font-family:Verdana,Arial,Helvetica,sans-serif; color:#999999;">Brillio is powered by <a href="http://www.netsuite.com/" style="color:#999999;">NetSuite</a> One System. No Limits.</font>';
    htmltext += '</td>';
    htmltext += '</tr>';
    htmltext += '</table>';
    htmltext += '</body>';
    htmltext += '</html>';
 
    return {
        MailBody : htmltext,
        MailSubject : "Confirmation feedback initiation"      
    };
}



function serviceTemplate3(firstName, reporting_mng_email, reporting_mng_first_name, employee_internal_id, probation_end_date,feedback_id,extended_prob_end_date,ext_month,ext_comments) 
{
    
	var feedbackFormUrl = 'https://forms.na1.netsuite.com/app/site/hosting/scriptlet.nl?script=998&deploy=1&compid=3883006&h=eb5b1afb22802abdaf67';
	feedbackFormUrl +='&custparam_employee_id='+employee_internal_id+'&feedback_record_id='+feedback_id ;
	
	     
	var htmltext = '';
    
   htmltext += '<table border="0" width="100%"><tr>';
   htmltext += '<td colspan="4" valign="top">';
  
   htmltext += '<p>Hi  ' + reporting_mng_first_name + ',</p>';
   
   htmltext += '<p> This is to bring to your notice that '+firstName+' is due for confirmation on '+extended_prob_end_date+'. His/Her probation was extended for '+ext_month+' last time. Please click this <a href  ="'+feedbackFormUrl+'" >Link</a> and provide your inputs.</p>';
   htmltext +='<p>Please find below the feedback provided last time when the probation was extended.</p><b><p>&nbsp;&nbsp;'+ext_comments+'</p></b>';
   htmltext +='<p>Please find attached the process document for your reference.</p>';
   htmltext +='<p>Note: if you do not provide your feedback before the due date, the system shall trigger the confirmation email to your reportee on the due date.</p>'
   htmltext += '<p>Thanks & Regards,</p>';
   htmltext += '<p>Team HR</p>';
 
 
    htmltext += '</td></tr>';
    htmltext += '</table>';
    htmltext += '<hr width="100%" size="1" noshade color="#CCCCCC">';
    htmltext += '<table width="100%" border="0" cellspacing="0" cellpadding="0">';
    htmltext += '<tr>';
    htmltext += '<td align="right">';
    htmltext += '<font style="font-size:9px; font-family:Verdana,Arial,Helvetica,sans-serif; color:#999999;">Brillio is powered by <a href="http://www.netsuite.com/" style="color:#999999;">NetSuite</a> One System. No Limits.</font>';
    htmltext += '</td>';
    htmltext += '</tr>';
    htmltext += '</table>';
    htmltext += '</body>';
    htmltext += '</html>';
 
    return {
        MailBody : htmltext,
        MailSubject : "Confirmation feedback initiation"      
    };
}


function serviceTemplate4(firstName, reporting_mng_email, reporting_mng_first_name, employee_internal_id, probation_end_date,feedback_id,ext_month,ext_comments) 
{
    
	var feedbackFormUrl = 'https://forms.na1.netsuite.com/app/site/hosting/scriptlet.nl?script=998&deploy=1&compid=3883006&h=eb5b1afb22802abdaf67';
	feedbackFormUrl +='&custparam_employee_id='+employee_internal_id+'&feedback_record_id='+feedback_id ;
	
	     
    
	var htmltext = '';
    
   htmltext += '<table border="0" width="100%"><tr>';
   htmltext += '<td colspan="4" valign="top">';
  
   htmltext += '<p>Hi  ' + reporting_mng_first_name + ',</p>';
   
   htmltext += '<p> This is to bring to your notice that '+firstName+' is due for confirmation on '+extended_prob_end_date+'. His/Her probation was extended for '+ext_month+' last time. Please click this <a href  ="'+feedbackFormUrl+'" >Link</a> and provide your inputs.</p>';
   htmltext +='<p>Please find below the feedback provided last time when the probation was extended.</p><b><p>&nbsp;&nbsp;'+ext_comments+'</p></b>';
   htmltext +='<p>Please find attached the process document for your reference.</p>';
   htmltext +='<p>Note: if you do not provide your feedback before the due date, the system shall trigger the confirmation email to your reportee on the due date.</p>'
   htmltext += '<p>Thanks & Regards,</p>';
   htmltext += '<p>Team HR</p>';
 
 
    htmltext += '</td></tr>';
    htmltext += '</table>';
    htmltext += '<hr width="100%" size="1" noshade color="#CCCCCC">';
    htmltext += '<table width="100%" border="0" cellspacing="0" cellpadding="0">';
    htmltext += '<tr>';
    htmltext += '<td align="right">';
    htmltext += '<font style="font-size:9px; font-family:Verdana,Arial,Helvetica,sans-serif; color:#999999;">Brillio is powered by <a href="http://www.netsuite.com/" style="color:#999999;">NetSuite</a> One System. No Limits.</font>';
    htmltext += '</td>';
    htmltext += '</tr>';
    htmltext += '</table>';
    htmltext += '</body>';
    htmltext += '</html>';
 
    return {
        MailBody : htmltext,
        MailSubject : "Confirmation feedback initiation"      
    };
}

function serviceTemplate5(firstName, reporting_mng_email,reporting_mng) 
{
	var htmltext = '';
    
	   htmltext += '<table border="0" width="100%"><tr>';
	   htmltext += '<td colspan="4" valign="top">';
	  
	   htmltext += '<p>Dear HR,</p>';
	   htmltext += ' The manager of '+reporting_mng+' has not provided his feedback on extension case for the employee : '+firstName+'.';
	   htmltext += '<p>Kindly take it to closure.</p>';
	   htmltext += '<p>Thanks & Regards,</p>';
	   htmltext += '<p>Team HR</p>';
	 
	 
	    htmltext += '</td></tr>';
	    htmltext += '</table>';
	    htmltext += '<hr width="100%" size="1" noshade color="#CCCCCC">';
	    htmltext += '<table width="100%" border="0" cellspacing="0" cellpadding="0">';
	    htmltext += '<tr>';
	    htmltext += '<td align="right">';
	    htmltext += '<font style="font-size:9px; font-family:Verdana,Arial,Helvetica,sans-serif; color:#999999;">Brillio is powered by <a href="http://www.netsuite.com/" style="color:#999999;">NetSuite</a> One System. No Limits.</font>';
	    htmltext += '</td>';
	    htmltext += '</tr>';
	    htmltext += '</table>';
	    htmltext += '</body>';
	    htmltext += '</html>';
	    return {
	        MailBody : htmltext,
	        MailSubject : "Confirmation feedback initiation Not performed"      
	    };
}
	
	//validate blank entries
function _logValidation(value) 
{
 if(value != null && value.toString() != null && value != '' && value != undefined && value.toString() != undefined && value != 'undefined' && value.toString() != 'undefined'&& value.toString() != 'NaN' && value != NaN) 
 {
  return true;
 }
 else 
 { 
  return false;
 }
}