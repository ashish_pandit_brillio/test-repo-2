/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       16 Aug 2017     deepak.srinivas
 *
 */

function postRESTlet(dataIn) {
	try{
		var response = new Response();
		nlapiLogExecution('debug', 'dataIn', JSON.stringify(dataIn));
		
		
		//Log for current date
		var current_date = nlapiDateToString(new Date());
		nlapiLogExecution('DEBUG', 'Current Date', 'current_date...' + current_date)
		
		
		var employeeId =  getUserUsingEmailId(dataIn.EmailId);
		var requestType = dataIn.RequestType;
		var weekStartDate = dataIn.Data ? dataIn.Data.WeekStartDate : '';
		
		var weekStartDate = '03/08/2020';
		if(weekStartDate){
			
		//Get Week StartDate
		var weekStDate = nlapiStringToDate(weekStartDate);
		//var curr = new Date();
		/*var day = weekStDate.getDay();
		var firstday = new Date(weekStDate.getTime() - 60*60*24* day*1000); 
		nlapiLogExecution('DEBUG', 'firstday', 'firstday...' + firstday);
		weekStartDate = nlapiDateToString(firstday);
		nlapiLogExecution('DEBUG', 'firstday', 'weekstart_date...' + weekStartDate); */
		
		//
		//var date1 = new Date();
		//nlapiLogExecution('DEBUG', 'schedulerFunction', 'Date ==' + date1);
	
		date1 = weekStDate;
	
		var offsetIST = 5.5;
	
		//To convert to UTC datetime by subtracting the current Timezone offset
		var utcdate = new Date(date1.getTime() + (date1.getTimezoneOffset() * 60000));
		//nlapiLogExecution('DEBUG', 'schedulerFunction', 'UTC Date' + utcdate);
	
		//Then cinver the UTS date to the required time zone offset like back to 5.5 for IST
		var istdate = new Date(utcdate.getTime() - ((-offsetIST * 60) * 60000));
		//nlapiLogExecution('DEBUG', 'schedulerFunction', 'IST Date ==' + istdate);
		var day = istdate.getDay();
		var firstday = new Date(istdate.getTime() - 60*60*24* day*1000); 
		nlapiLogExecution('DEBUG', 'firstday', 'firstday...' + firstday);
		weekStartDate = nlapiDateToString(firstday);
		nlapiLogExecution('DEBUG', 'firstday', 'weekstart_date...' + weekStartDate)
		
		}
		//Test Data
		//var employeeId = 43690;
		//var requestType = 'NOTSUBMITTED'; //PASTTIMESHEET ,NOTSUBMITTED
		//var weekStartDate = '8/13/2017';
		
		var endDate = nlapiDateToString(nlapiAddDays(nlapiStringToDate(weekStartDate), 6), 'date');
		
		switch (requestType) {

		case M_Constants.Request.PastTimeSheet:

			if (employeeId && weekStartDate) {
				response.Data = past_five_TS(dataIn,employeeId,weekStartDate,endDate);
				response.Status = true;
			} else {
				response.Data = "Some error with the data sent";
				response.Status = false;
		}
			break;
		case M_Constants.Request.NotSubmitted:
			if(employeeId && weekStartDate){
				response.Data = rejected_notSubmitted_TS(dataIn,employeeId,weekStartDate,endDate);
				response.Status = true;				
			} else {
				response.Data = "Some error with the data sent";
				response.Status = false;
		}
		break;
		}
	}
	catch (err) {
		nlapiLogExecution('ERROR', 'postRESTlet', err);
		response.Data = err;
	}
	nlapiLogExecution('ERROR', 'response', JSON.stringify(response));
	return response;
}

function past_five_TS(dataIn,empId,startDt,endDt){
	try{
		var search = nlapiSearchRecord('timesheet',null,[
		                                               				new nlobjSearchFilter('timesheetdate', null, 'onorbefore',
		                                               						startDt),
		                                            				new nlobjSearchFilter('employee', null, 'anyof',
		                                            						empId),
		                                            				new nlobjSearchFilter('approvalstatus', null, 'noneof',1)		
		                                               				], 
		                                            				[
		                                            				new nlobjSearchColumn('internalid'),
		                                            				new nlobjSearchColumn('employee'),
		                                            				new nlobjSearchColumn('startdate').setSort(true),
		                                            				new nlobjSearchColumn('approvalstatus'),
		                                            				new nlobjSearchColumn('enddate'),
		                                            				new nlobjSearchColumn('totalhours'),//totalhours
		                                            				new nlobjSearchColumn('timeapprover','employee'),
		                                            				new nlobjSearchColumn('datecreated','timeentry')
		                                            				]);
		var JSON = {};
		var dataRow = [];
		if(_logValidation(search)){
		for(var i=0;i<search.length && i<5;i++){
			JSON = {
				ID: search[i].getValue('internalid'),
				Employee: search[i].getText('employee'),
				WeekStartDate: search[i].getValue('startdate'),
				WeekEndDate: search[i].getValue('enddate'),
				TotalHrs: search[i].getValue('totalhours'),
				ApprovalStatus: search[i].getText('approvalstatus'),
				Approver: search[i].getText('timeapprover','employee'),
				SubmittedDate: search[i].getValue('datecreated','timeentry')
			};
			dataRow.push(JSON);
		}
			
		}
		return dataRow;
		
	}
	catch (err) {
		nlapiLogExecution('ERROR', 'postRESTlet', err);
		response.Data = err;
	
}
}
//To get last not submitted and rejected TS	
function rejected_notSubmitted_TS(dataIn,empId,startDt,endDt){
		try{
			
			
		//Get Past 4 weeks dates
			var past_weeks = 4;
			var relative_time = startDt;
			var weeks = new Array();
			var JSON = {};
			var dataRow = [];
			for(var week_count=0;week_count<past_weeks;week_count++) {
				var date = nlapiDateToString(nlapiAddDays(nlapiStringToDate(startDt), -7), 'date');
			
				var filters = [];
				filters[0] = new nlobjSearchFilter('formuladate', null, 'on',date);
				filters[0].setFormula('{startdate}');
				filters[1] = new nlobjSearchFilter('employee', null, 'anyof',empId);
				filters[2] = new nlobjSearchFilter('approvalstatus', null, 'anyof',4);
				
			var search = nlapiSearchRecord('timesheet',null,filters, 
			                                            				[
			                                            				new nlobjSearchColumn('internalid'),
			                                            				new nlobjSearchColumn('employee'),
			                                            				new nlobjSearchColumn('startdate'),
			                                            				new nlobjSearchColumn('enddate'),
			                                            				new nlobjSearchColumn('totalhours'),
			                                            				new nlobjSearchColumn('approvalstatus'),
			                                            				new nlobjSearchColumn('timeapprover','employee')
			                                            				]);
				
				var filters_ = [];
				filters_[0] = new nlobjSearchFilter('formuladate', null, 'on',date);
				filters_[0].setFormula('{startdate}');
				filters_[1] = new nlobjSearchFilter('employee', null, 'anyof',empId);
				filters_[2] = new nlobjSearchFilter('approvalstatus', null, 'noneof',['4','1']);
				
			var search_ = nlapiSearchRecord('timesheet',null,filters_, 
			                                            				[
			                                            				new nlobjSearchColumn('internalid'),
			                                            				new nlobjSearchColumn('employee'),
			                                            				new nlobjSearchColumn('startdate'),
			                                            				new nlobjSearchColumn('enddate'),
			                                            				new nlobjSearchColumn('totalhours'),
			                                            				new nlobjSearchColumn('approvalstatus'),
			                                            				new nlobjSearchColumn('timeapprover','employee')
			                                            				]);
			
			if(_logValidation(search)){
			JSON = {
				WeekStartDate: date,
				WeekEndtDate: search[0].getValue('enddate'),
				Status: 'Rejected',
				TotalHrs: search[0].getValue('totalhours'),
				ID: search[0].getValue('internalid')
			
			};
			dataRow.push(JSON);
			}
			else if(_logValidation(search_)){
				
				JSON = {
						WeekStartDate: date,
						WeekEndtDate: nlapiDateToString(nlapiAddDays(nlapiStringToDate(date), 6), 'date'),
						Status: 'Not Submitted',
						ID: ''	
					
					};
					dataRow.push(JSON);
				
			}
			startDt = date;
			}
			return dataRow;
			
		}
		catch (err) {
			nlapiLogExecution('ERROR', 'postRESTlet', err);
			response.Data = err;
		
	}	
}
function _logValidation(value) 
{
 if(value != null && value.toString() != null && value != '' && value != undefined && value.toString() != undefined && value != 'undefined' && value.toString() != 'undefined'&& value.toString() != 'NaN' && value != NaN) 
 {
  return true;
 }
 else 
 { 
  return false;
 }
}
