/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       09 Dec 2014     amol.sahijwani
 * 1.01		  12 Mar 2020	 praveena madem		Changed the sublist id and fields which are related to timesheet
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType
 * 
 * @param {String} type Operation types: create, edit, delete, xedit
 *                      approve, reject, cancel (SO, ER, Time Bill, PO & RMA only)
 *                      pack, ship (IF)
 *                      markcomplete (Call, Task)
 *                      reassign (Case)
 *                      editforecast (Opp, Estimate)
 * @returns {Void}
 */
function userEventBeforeSubmit(type){
	if(type == 'delete' || type == 'approve' || type == 'reject')
	{
		return;
	}
	else if(type == 'markcomplete')
	{
		try
		{
			nlapiLogExecution('AUDIT', 'Workflow Called', type);
			nlapiInitiateWorkflowAsync('timesheet', nlapiGetRecordId(), 'customworkflow_update_timesheet_fields');
		}
		catch(e)
		{
			nlapiLogExecution('AUDIT', 'Workflow Called Error', e.message);
		}
			return;
	}	
	
	return;
	var startTime = new Date();
	var recType = nlapiGetRecordType();
			
	var recId	= nlapiGetRecordId();
			
	// Load the Time Sheet Record
	var recTS = nlapiLoadRecord(recType, recId, {recordmode: 'dynamic'});
			
	var isRecordChanged = false;
			
	// Array of Subrecord names
	//var daysOfWeek = [ 'sunday', 'monday', 'tuesday', 'wednesday','thursday', 'friday', 'saturday' ];
	var daysOfWeek=['timebill0','timebill1','timebill2','timebill3','timebill4','timebill5','timebill6'];
			
	var i_employee_id = recTS.getFieldValue('employee');
	var s_employee_name = recTS.getFieldText('employee');				
	// Get Start Date
	var d_start_date	=	new Date(recTS.getFieldValue('startdate'));
			
	// Get End Date
	var d_end_date		=	new Date(recTS.getFieldValue('enddate'));
			
	// Get Line Count
	var i_line_count = recTS.getLineItemCount('timeitem');//recTS.getLineItemCount('timegrid');
	
	//nlapiLogExecution('AUDIT', 'Number of Lines', i_line_count);		
	var o_projects = new Object();
	
	// Get Resource Allocations for the employee
	var a_resource_allocations = getResourceAllocationsForEmployee(i_employee_id, d_start_date, d_end_date);
	// Log String
	var strLog = '';
	// Loop through Line Items
	for(var i_line_indx = 1; i_line_indx <= i_line_count; i_line_indx++)
		{
			
			//nlapiLogExecution('AUDIT', 'Test', JSON.stringify(a_days_data));		
						
			recTS.selectLineItem('timeitem',i_line_indx);//recTS.getLineItemCount('timegrid');
			
			var isLineItemChanged = false;
						
			for(var i_day_indx = 0; i_day_indx < 7; i_day_indx++)
				{
					var sub_record_name = a_days[i_day_indx];
					var o_sub_record_view = recTS.viewCurrentLineItemSubrecord('timeitem', sub_record_name);
					if(o_sub_record_view)
						{
							var d_current_date = nlapiStringToDate(o_sub_record_view.getFieldValue('day'), 'date');
							var i_project_id = o_sub_record_view.getFieldValue('customer');
							var isCurrentlyBillable = o_sub_record_view.getFieldValue('isbillable');
							var currentRate = o_sub_record_view.getFieldValue('rate');
							var i_item_id = o_sub_record_view.getFieldValue('item');
							var i_approval_status = o_sub_record_view.getFieldValue('approvalstatus');
							var isResourceAllocationFound = false;
							//nlapiLogExecution('AUDIT', 'Test', 'Project: ' + i_project_id + ', Item: ' + i_item_id);			
							// Check resource allocation
							if(i_project_id != null && i_approval_status != 3)
								{
									// Initialise to billable:'F' by default
									o_day_data = {'billable':'F', 'rate':0.0};
												
									if(a_resource_allocations != null)
										{
											for(var i_ra_indx = 0; i_ra_indx < a_resource_allocations.length; i_ra_indx++)
												{
													var o_resource_allocation = a_resource_allocations[i_ra_indx];
																
													//var d_current_date = nlapiAddDays(d_start_date, i_day_indx);
													if(o_resource_allocation.project_id == i_project_id)
														{
															if(d_current_date >= o_resource_allocation.start_date 
																&& d_current_date <= o_resource_allocation.end_date)
																{
																	var i_rate = 0;
																	if(i_item_id == '2222' || i_item_id == '2221')
																		{
																			// Calculate rate for monthly billing
																			if(o_resource_allocation.is_monthly_billing == 'T')
																				{
																					i_rate = o_resource_allocation.monthly_rate/(8.0 * getWorkingDays(d_current_date));
																				}
																			else
																				{
																					i_rate = o_resource_allocation.st_rate;
																				}
																		}
																	else if(i_item_id == '2425')
																		{
																			i_rate = o_resource_allocation.ot_rate;
																		}
																	isResourceAllocationFound = true;
																	
																	var isResourceBillable = 'F';
																	if(d_current_date >= o_resource_allocation.billing_start_date
																			&& d_current_date <= o_resource_allocation.billing_end_date)
																		{
																			if(o_resource_allocation.is_billable == 'T')
																				{
																					isResourceBillable = 'T';
																				}
																		}
																	else
																		{
																			nlapiLogExecution('AUDIT', 'Method: ' + type + ': TimeSheet ID: ' + recId + ' Employee ID: '+ s_employee_name + ' - ' + d_start_date.toDateString(), sub_record_name + 'Out of billable range.<br />');
																		}
																	o_day_data = {'billable':isResourceBillable, 'rate': i_rate, 'vertical': o_resource_allocation.vertical, 'practice': o_resource_allocation.practice, 'customer': o_resource_allocation.customer, 'employee':s_employee_name, 'project_description':o_resource_allocation.project_description};
																}
															else
																{
																	nlapiLogExecution('AUDIT', 'Method: ' + type + ': TimeSheet ID: ' + recId + ' Employee ID: '+ s_employee_name + ' - ' + d_start_date.toDateString(), sub_record_name + 'Out of resource allocation range.<br />');
																}
															
															
														}
												}
										}
									
									nlapiLogExecution('AUDIT', 'data', JSON.stringify(o_day_data));
									var isRateDifferent = false;
									nlapiLogExecution('AUDIT', 'check', 'CurrentRate: ' + currentRate + ', o_day_data.rate: ' + o_day_data.rate);
									if(o_day_data.rate !== currentRate)
										{
											isRateDifferent = true;
										}
									if(!isNaN(currentRate) && !isNaN(o_day_data.rate) && parseFloat(currentRate) == parseFloat(o_day_data.rate))
										{
											isRateDifferent = false;
										}
									if(o_day_data.billable != isCurrentlyBillable || isRateDifferent == true || o_sub_record_view.getFieldValue('custcolprj_name') == '')//custrecordprj_name_ts
										{
											var o_sub_record = recTS.editCurrentLineItemSubrecord('timegrid', sub_record_name);
											
											o_sub_record.setFieldValue('class', o_day_data.vertical);
											o_sub_record.setFieldValue('department', o_day_data.practice);
											o_sub_record.setFieldValue('custcolcustcol_temp_customer', o_day_data.customer);
											o_sub_record.setFieldValue('custcolprj_name', o_day_data.project_description);
											o_sub_record.setFieldValue('custcol_employeenamecolumn', s_employee_name);
											
											// Set Billable
											if(i_item_id != '2479' && i_item_id != '2480' && i_item_id != '2481')
												{
													o_sub_record.setFieldValue('isbillable', o_day_data.billable);
													strLog += sub_record_name + ': Billable: ' + isCurrentlyBillable + ' => ' + o_day_data.billable + '<br />';
												}
											// Set Rate
											if(isResourceAllocationFound == true)
												{
													if(isRateDifferent == true)
														{
															o_sub_record.setFieldValue('price', -1);
															o_sub_record.setFieldValue('rate', o_day_data.rate);
															strLog += sub_record_name + ': Rate: ' + currentRate + ' => ' + o_day_data.rate + '<br />';
														}
												}
											
											o_sub_record.commit();
											isLineItemChanged = true;
										}
								}
						}	
				}
							
				if(isLineItemChanged == true)
					{
						recTS.commitLineItem('timeitem');
						isRecordChanged = true;
					}
								
						
		}
			
		if(isRecordChanged)
		{
			nlapiSubmitRecord(recTS);
		}
		var endTime = new Date();
		nlapiLogExecution('AUDIT', 'Time: ' + ((endTime.getTime() - startTime.getTime())/1000.0) + ', Method: ' + type + ': TimeSheet ID: ' + recId + ' Employee ID: '+ s_employee_name + ' - ' + d_start_date.toDateString(), strLog); return true;
}


function getResourceAllocationsForEmployee(i_employee_id, d_start_date, d_end_date)
{
	// Store resource allocations for project and employee
	var a_resource_allocations = new Array();
	
	// Get Resource allocations for this week
	var filters = new Array();
	filters[0]	=	new nlobjSearchFilter('resource', null, 'anyof', i_employee_id);
	//filters[1]	=	new nlobjSearchFilter('project', null, 'anyof', i_project_id);
	filters[1]	=	new nlobjSearchFilter('startdate', null, 'onorbefore', d_end_date);
	filters[2]	=	new nlobjSearchFilter('enddate', null, 'onorafter', d_start_date);
	//filters[3]	=	new nlobjSearchFilter('billable', null, 'is', 'T');

	var columns = new Array();
	columns[0]	= new nlobjSearchColumn('custeventbstartdate');
	columns[1]	= new nlobjSearchColumn('custeventbenddate');
	columns[2]	= new nlobjSearchColumn('custeventrbillable');
	columns[3]  = new nlobjSearchColumn('custevent3');
	columns[4]	= new nlobjSearchColumn('custevent_otrate');
	columns[5]  = new nlobjSearchColumn('project');
	columns[6]	= new nlobjSearchColumn('startdate');
	columns[7]	= new nlobjSearchColumn('enddate');
	columns[8]	= new nlobjSearchColumn('internalid');
	columns[9]	= new nlobjSearchColumn('custentity_t_and_m_monthly', 'job');
	columns[10]	= new nlobjSearchColumn('custevent_monthly_rate');
	columns[11] = new nlobjSearchColumn('custentity_vertical', 'job');
	columns[12] = new nlobjSearchColumn('entityid', 'job');
	columns[13] = new nlobjSearchColumn('jobname', 'job');
	columns[14] = new nlobjSearchColumn('department', 'employee');
	columns[15] = new nlobjSearchColumn('customer', 'job');
	var search_results = nlapiSearchRecord('resourceallocation', null, filters, columns);
	
	if(search_results != null && search_results.length > 0)
	{
		for(var i_search_indx = 0; i_search_indx < search_results.length; i_search_indx++)
			{
				var i_project_id = search_results[i_search_indx].getValue('project');
				
				var recResourceAllocation = nlapiLoadRecord('resourceallocation', search_results[i_search_indx].getValue('internalid'));
				
				var resource_allocation_start_date = new Date(recResourceAllocation.getFieldValue('startdate'));
				var resource_allocation_end_date   = new Date(recResourceAllocation.getFieldValue('enddate'));
				var billing_start_date	=	new Date(recResourceAllocation.getFieldValue('custeventbstartdate'));
				var billing_end_date	=	new Date(recResourceAllocation.getFieldValue('custeventbenddate'));
				var is_resource_billable	= search_results[i_search_indx].getValue('custeventrbillable');
				var stRate					= search_results[i_search_indx].getValue('custevent3');
				var otRate					= search_results[i_search_indx].getValue('custevent_otrate');
				var is_monthly_billing	=	search_results[i_search_indx].getValue('custentity_t_and_m_monthly', 'job');
				var f_monthly_rate	=	parseFloat(search_results[i_search_indx].getValue('custevent_monthly_rate'));
				var i_project_vertical = search_results[i_search_indx].getValue('custentity_vertical', 'job');
				var i_project_name_id	=	search_results[i_search_indx].getValue('entityid', 'job');
				var s_project_name	=	search_results[i_search_indx].getValue('jobname', 'job');
				var s_customer_name	=	search_results[i_search_indx].getText('customer', 'job');
				var i_practice		=	search_results[i_search_indx].getValue('department', 'employee');
				
				a_resource_allocations[i_search_indx] = {
															'project_id': i_project_id,
															'start_date':resource_allocation_start_date,
															'end_date':resource_allocation_end_date,
															'billing_start_date':billing_start_date,
															'billing_end_date':billing_end_date,
															'is_billable': is_resource_billable,
															'st_rate': stRate,
															'ot_rate': otRate,
															'is_monthly_billing': is_monthly_billing,
															'monthly_rate': f_monthly_rate,
															'vertical': i_project_vertical,
															'project_description': getProjectDescription(i_project_name_id, s_project_name),
															'customer': s_customer_name,
															'practice': i_practice
														};
			}
	}
	else
	{
		a_resource_allocations = null;
	}
	
	return a_resource_allocations;
}

// Get Project Description text
function getProjectDescription(s_project_id, s_project_name)
{
	return s_project_id + ' ' + s_project_name;
}

// Get Working Days for the month
function getWorkingDays(d_date)
{
	var i_month	=	d_date.getMonth();
	
	var i_year	=	d_date.getFullYear();
	
	var firstDay = new Date(i_year, i_month, 1);
	var lastDay = new Date(i_year, i_month + 1, 0);
	
	return calcBusinessDays(firstDay, lastDay);
}

function calcBusinessDays(d_startDate, d_endDate) { // input given as Date objects
    var startDate	=	new Date(d_startDate.getTime());
    var endDate		=	new Date(d_endDate.getTime());
	// Validate input
    if (endDate < startDate)
        return 0;
    
    // Calculate days between dates
    var millisecondsPerDay = 86400 * 1000; // Day in milliseconds
    startDate.setHours(0,0,0,1);  // Start just after midnight
    endDate.setHours(23,59,59,999);  // End just before midnight
    var diff = endDate - startDate;  // Milliseconds between datetime objects    
    var days = Math.ceil(diff / millisecondsPerDay);
    
    // Subtract two weekend days for every week in between
    var weeks = Math.floor(days / 7);
    var days = days - (weeks * 2);

    // Handle special cases
    var startDay = startDate.getDay();
    var endDay = endDate.getDay();
    
    // Remove weekend not previously removed.   
    if (startDay - endDay > 1)         
        days = days - 2;      
    
    // Remove start day if span starts on Sunday but ends before Saturday
    if (startDay == 0 && endDay != 6)
        days = days - 1  
            
    // Remove end day if span ends on Saturday but starts after Sunday
    if (endDay == 6 && startDay != 0)
        days = days - 1  
    
    return days;
     
    }