/**
 *@NApiVersion 2.x
 *@NScriptType Suitelet
 */

// This Suitelet is to create input form to send report

define(['N/ui/serverWidget', 'N/email', 'N/runtime','N/task'],
    function(ui, email, runtime,task) {
        function onRequest(context) {
            if (context.request.method === 'GET') {
                var form = ui.createForm({
                    title: "Project Revenue Vrs FRF's"
                });
				var customerFld = form.addField({
					id : "custpage_customer",
					label : "Customer",
					type : "select",
					source: 'customer'
				})
				var projectType = form.addField({
					id : "custpage_project_type",
					label : "Project Type",
					type : "select"
				})
               
				projectType.addSelectOption({
					value: '1',
					text: 'T&M'
				});
				projectType.addSelectOption({
					value: '2',
					text: 'FP'
				});
				form.addSubmitButton({
                    label: 'Submit'
                });
				
				context.response.writePage(form);
            } else {
                var currentUser = runtime.getCurrentUser();
                var email = currentUser.email;
                var userId = currentUser.id;
				var i_customerId = context.request.parameters.custpage_customer;
				var i_project_type = context.request.parameters.custpage_project_type;
				log.debug('i_customerId '+i_customerId);
                var html = "<h2 style=color: #5e9ca0;>Please check email for report!</h2></br><h2>Note : It take's time to process - please wait for some time.</h2>";
                var form = ui.createForm({
                    title: "Project Revenue Vrs FRF's"
                });
				var labelHTML = form.addField({
					id : "custpage_label",
					label : "message",
					type : "INLINEHTML"
				})
				labelHTML.defaultValue = html;
				context.response.writePage(form);
				
				if(i_customerId && i_project_type == 1)
				{
					var scheduleScriptTaskObj = task.create({
						taskType: task.TaskType.SCHEDULED_SCRIPT,
						scriptId: 2255,
						params: {"custscript_customer": i_customerId}
					});
					var mrTaskId = scheduleScriptTaskObj.submit();
				}
				else if(i_project_type == 1)
				{
					var scheduleScriptTaskObj = task.create({
						taskType: task.TaskType.SCHEDULED_SCRIPT,
						scriptId: 2255
					});
					var mrTaskId = scheduleScriptTaskObj.submit();
				}
				else if(i_customerId && i_project_type == 2)
				{
					var scheduleScriptTaskObj = task.create({
						taskType: task.TaskType.SCHEDULED_SCRIPT,
						scriptId: 2256,
						params: {"custscript_customer1": i_customerId}
					});
					var mrTaskId = scheduleScriptTaskObj.submit();
				}
				else if(i_project_type == 2)
				{
					var scheduleScriptTaskObj = task.create({
						taskType: task.TaskType.SCHEDULED_SCRIPT,
						scriptId: 2256
					});
					var mrTaskId = scheduleScriptTaskObj.submit();
				}
			}
        }
        return {
            onRequest: onRequest
        };
    });