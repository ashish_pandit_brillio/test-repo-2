/**
 * @author Shweta
 */
// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
/*
   	Script Name : CLI_Expense_Report_Approval.js
	Author      : Shweta Chopde
	Date        : 22 Aug 2014
	Description : Create Approve Button  on Expense Report


	Script Modification Log:

	-- Date --			-- Modified By --				--Requested By--				-- Description --




	Below is a summary of the process controls enforced by this script file.  The control logic is described
	more fully, below, in the appropriate function headers and code blocks.

     PAGE INIT
		- pageInit(type)


     SAVE RECORD
		- saveRecord()


     VALIDATE FIELD
		- validateField(type, name, linenum)


     FIELD CHANGED
		- fieldChanged(type, name, linenum)


     POST SOURCING
		- postSourcing(type, name)


	LINE INIT
		- lineInit(type)


     VALIDATE LINE
		- validateLine()


     RECALC
		- reCalc()


     SUB-FUNCTIONS
		- The following sub-functions are called by the above core functions in order to maintain code
            modularization:





*/
}
// END SCRIPT DESCRIPTION BLOCK  ====================================



// BEGIN SCRIPT UPDATION BLOCK  ====================================
/*


*/
// END SCRIPT UPDATION BLOCK  ====================================




// BEGIN GLOBAL VARIABLE BLOCK  =====================================
{

	//  Initialize any Global Variables, in particular, debugging variables...


}
// END GLOBAL VARIABLE BLOCK  =======================================





// BEGIN PAGE INIT ==================================================

function pageInit_expense_approval(type)
{
  nlapiDisableLineItemField('expense','custcol_approvalstatus',true)

}

// END PAGE INIT ====================================================





// BEGIN SAVE RECORD ================================================

function saveRecord()
{
    /*  On save record:

	- PURPOSE



	  FIELDS USED:

		--Field Name--			--ID--		--Line Item Name--


    */


    //  LOCAL VARIABLES



    //  SAVE RECORD CODE BODY


	return true;

}

// END SAVE RECORD ==================================================





// BEGIN VALIDATE FIELD =============================================

function validateField(type, name, linenum)
{

	/*  On validate field:

          - EXPLAIN THE PURPOSE OF THIS FUNCTION


          FIELDS USED:

          --Field Name--				--ID--


    */


	//  LOCAL VARIABLES



	//  VALIDATE FIELD CODE BODY


	return true;

}

// END VALIDATE FIELD ===============================================





// BEGIN FIELD CHANGED ==============================================

function fieldChanged_expense_approval(type, name, linenum)
{
    nlapiDisableLineItemField('expense','custcol_approvalstatus',true)
}

// END FIELD CHANGED ================================================





// BEGIN POST SOURCING ==============================================

function postSourcing(type, name)
{

    /*  On post sourcing:

	- PURPOSE

          FIELDS USED:

		--Field Name--			--ID--		--Line Item Name--


    */

    //  LOCAL VARIABLES


    //  POST SOURCING CODE BODY



}

// END POST SOURCING ================================================



// BEGIN LINE INIT ==============================================

function lineInit_expense_approval(type)
{
   nlapiDisableLineItemField('expense','custcol_approvalstatus',true)
}

// END LINE INIT ================================================


// BEGIN VALIDATE LINE ==============================================

function validateLine(type)
{

	/*  On validate line:

          - EXPLAIN THE PURPOSE OF THIS FUNCTION


          FIELDS USED:

          --Field Name--				--ID--


    */


	//  LOCAL VARIABLES


	//  VALIDATE LINE CODE BODY


	return true;

}

// END VALIDATE LINE ================================================




// BEGIN RECALC =====================================================

function recalc(type)
{

	/*  On recalc:

          - EXPLAIN THE PURPOSE OF THIS FUNCTION


          FIELDS USED:

          --Field Name--				--ID--


    */


	//  LOCAL VARIABLES


	//  RECALC CODE BODY


}

// END RECALC =======================================================





// BEGIN FUNCTION ===================================================

function approval_expense_report()
{
  try
  {
  	var i_recordID = nlapiGetRecordId();
		alert('i_recordID'+i_recordID)
		
	if(_logValidation(i_recordID)) 
	{
		var o_recordOBJ = nlapiLoadRecord('expensereport',parseInt(i_recordID));
	//	alert('o_recordOBJ'+o_recordOBJ)
		
		
		if(_logValidation(o_recordOBJ)) 
		{	    	
			var i_line_count = o_recordOBJ.getLineItemCount('expense');
		//	alert('i_line_count'+i_line_count)
		
			if(_logValidation(i_line_count)) 
			{
				for(var t=1;t<=i_line_count;t++)
				{	
			//		alert('t'+t)
					
				  o_recordOBJ.setLineItemValue('expense','custcol_approvalstatus',t,2)	
					
				}//Loop				
			}//Line Count
			
		//	alert('sssssss')
								
			o_recordOBJ.setFieldValue('accountingapproval','T')
	        o_recordOBJ.setFieldValue('account',756)
		
			var i_submitID = nlapiSubmitRecord(o_recordOBJ,true,true)
			alert('i_submitID'+i_submitID)
		}//Record OBJ
		
	}//Record ID
  }
  catch(exception)
  {
  	alert(exception.getDetails())
  }  		
  location.reload();
}//Expense Report Approval

function _logValidation(value) 
{
 if(value!='null' && value != null && value.toString() != null && value != '' && value != undefined && value.toString() != undefined && value != 'undefined' && value.toString() != 'undefined'&& value.toString() != 'NaN' && value != NaN) 
 {
  return true;
 }
 else 
 { 
  return false;
 }
}

// END FUNCTION =====================================================
