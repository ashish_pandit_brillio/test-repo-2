function Before_Submit_Test_Capex()
{
	for (var i=1; i<= nlapiGetLineItemCount('item'); i++)
	{
		nlapiLogExecution('DEBUG', 'CAPEX', nlapiGetLineItemValue('item', 'custcol_is_capex', i));
		if (nlapiGetLineItemValue('item', 'custcol_is_capex', i) == 'T')
		{
			nlapiLogExecution('DEBUG', 'Account - Before', nlapiGetLineItemValue('item', 'account', i));
			nlapiSetLineItemValue('item', 'account', i, '58');
			nlapiLogExecution('DEBUG', 'Account - Before', nlapiGetLineItemValue('item', 'account', i));
		}
	}
}

function Employee_Automation()
{
	var test = nlapiLoadRecord('employee', -5);
	nlapiLogExecution('DEBUG', 'Entity', test.getFieldValue('entityid'));
	nlapiLogExecution('DEBUG', 'subsidiary', test.getFieldValue('subsidiary'));
	nlapiLogExecution('DEBUG', 'location', test.getFieldValue('location'));
	nlapiLogExecution('DEBUG', 'subsidiary', test.getFieldText('subsidiary'));
	nlapiLogExecution('DEBUG', 'location', test.getFieldText('location'));
	
	
	var Empfile = nlapiLoadFile('SFTP/From Oracle SFTP/Employee.xml'); 
	if (Empfile) 
	{
		var xmlDocument = nlapiStringToXML(Empfile.getValue());
		var EmpNode = nlapiSelectNode(xmlDocument, "//*[name()='Employee']")
		nlapiLogExecution('DEBUG', 'EmployeeId', nlapiSelectValue(EmpNode, "//*[name()='EmployeeId']"));
		
		var record = nlapiCreateRecord('employee');
		//record.setFieldValue('entityid', nlapiSelectValue(EmpNode, "//*[name()='EmployeeId']"));
		record.setFieldValue('salutation', nlapiSelectValue(EmpNode, "//*[name()='Salutation']"));
		record.setFieldValue('firstname', nlapiSelectValue(EmpNode, "//*[name()='FirstName']"));
		record.setFieldValue('middlename', nlapiSelectValue(EmpNode, "//*[name()='MiddleName']"));
		record.setFieldValue('lastname', nlapiSelectValue(EmpNode, "//*[name()='LastName']"));
		record.setFieldValue('initials', nlapiSelectValue(EmpNode, "//*[name()='Initials']"));
		record.setFieldValue('title', nlapiSelectValue(EmpNode, "//*[name()='JobTitle']"));
		record.setFieldValue('supervisor', GetDetails(nlapiSelectValue(EmpNode, "//*[name()='ReportingManager']"), 0));
		record.setFieldValue('email', nlapiSelectValue(EmpNode, "//*[name()='Email']"));
		record.setFieldValue('phone', nlapiSelectValue(EmpNode, "//*[name()='Phone']"));
		record.setFieldValue('fax', nlapiSelectValue(EmpNode, "//*[name()='Fax']"));
		record.setFieldValue('officephone', nlapiSelectValue(EmpNode, "//*[name()='OfficePhone']"));
		record.setFieldValue('homephone', nlapiSelectValue(EmpNode, "//*[name()='HomePhone']"));
		record.setFieldValue('mobilephone', nlapiSelectValue(EmpNode, "//*[name()='MobilePhone']"));
		record.setFieldValue('subsidiary', GetDetails(nlapiSelectValue(EmpNode, "//*[name()='Subsidiary']"), 1));
		//record.setFieldValue('department', nlapiSelectValue(EmpNode, "//*[name()='EmployeeId']"));
		//record.setFieldValue('class', nlapiSelectValue(EmpNode, "//*[name()='EmployeeId']"));
		record.setFieldValue('location', GetDetails(nlapiSelectValue(EmpNode, "//*[name()='Location']"), 2));
		record.setFieldValue('comments', nlapiSelectValue(EmpNode, "//*[name()='Notes']"));
		record.setFieldValue('socialsecuritynumber', nlapiSelectValue(EmpNode, "//*[name()='SSN']"));
		record.setFieldValue('approver', GetDetails(nlapiSelectValue(EmpNode, "//*[name()='ExpenseApprover']"), 0));
		record.setFieldValue('expenseapprover', GetDetails(nlapiSelectValue(EmpNode, "//*[name()='PurchaseApprover']"), 0));
		record.setFieldValue('timeapprover', GetDetails(nlapiSelectValue(EmpNode, "//*[name()='TimeApprover']"), 0));
		record.setFieldValue('isjobresource', (nlapiSelectValue(EmpNode, "//*[name()='ProjectResource']") == 0? 'F':'T'));
		//record.setFieldValue('employeetype', nlapiSelectValue(EmpNode, "//*[name()='Type']"));
		//record.setFieldValue('birthdate', nlapiSelectValue(EmpNode, "//*[name()='BirthDate']"));
		//record.setFieldValue('hiredate', nlapiSelectValue(EmpNode, "//*[name()='HireDate']"));
		record.setFieldValue('jobdescription', nlapiSelectValue(EmpNode, "//*[name()='JobDescription']"));
		
		var recordid = nlapiSubmitRecord(record);
		nlapiLogExecution('DEBUG', 'Recordid', recordid);
	} 
	else
	{
		nlapiLogExecution('DEBUG', 'Event', 'No file;');	
	}
}

function GetDetails(SearchName, Type)
{
	var ReturnId = 0;
	var searchrecordtype = '';
	switch(Type)
	{
		case 0:
			var filters = new Array();
			filters[0] = new nlobjSearchFilter('entityid', null, 'is', SearchName);
			searchrecordtype = 'employee';
			break;
		
		case 1:
			var filters = new Array();
			filters[0] = new nlobjSearchFilter('name', null, 'contains', SearchName);
			searchrecordtype = 'subsidiary';
			break;
		
		case 2:
			var filters = new Array();
			filters[0] = new nlobjSearchFilter('name', null, 'is', SearchName);
			searchrecordtype = 'location';
			break;
	
		default:
			break;
	}
	nlapiLogExecution('DEBUG', 'record type', searchrecordtype);
	
	var searchResult = nlapiSearchRecord(searchrecordtype, null, filters, null)
	if (searchResult != null)
	{
		for(var i=0; i < searchResult.length; i++)
		{
			ReturnId = searchResult[i].getId();
		}
	}
	return ReturnId;
}


function GetManagerDetails(EmpName)
{
	var EmpId = 0;
	
	var filters = new Array();
	filters[0] = new nlobjSearchFilter('entityid', null, 'is', EmpName);

	var searchResult = nlapiSearchRecord('employee', null, filters, null)
	if (searchResult != null)
	{
		for(var i=0; i < searchResult.length; i++)
		{
			EmpId = searchResult[i].getId();
		}
	}
	return EmpId;
}

function GetSubsidiaryId(SubsidiaryName)
{
	var SubId = 0;

	var filters = new Array();
	filters[0] = new nlobjSearchFilter('name', null, 'contains', SubsidiaryName);

	var searchResult = nlapiSearchRecord('subsidiary', null, filters, null)
	if (searchResult != null)
	{
		for(var i=0; i < searchResult.length; i++)
		{
			SubId = searchResult[i].getId();
		}
	}
	nlapiLogExecution('DEBUG', 'SubId', SubId);
	return SubId;
}

function GetLocation(LocationName)
{


}