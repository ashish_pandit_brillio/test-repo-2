/**
 * Generate a zero allocation report and send it to business OPS
 * 
 * Version    Date            Author           Remarks
 * 1.00       01 Jun 2015     Nitish Mishra
 *
 */

/**
 * @param {String}
 *            type Context Types: scheduled, ondemand, userinterface, aborted,
 *            skipped
 * @returns {Void}
 */
function scheduled(type) {
	try {
		var context = nlapiGetContext();
		//var userId = nlapiGetUser();
		generateAndSendZeroAllocationReport();
	} catch (err) {
		nlapiLogExecution('error', 'scheduled', err);
	}
}

function getAllocatedEmployeeList(date) {
	try {
		var context = nlapiGetContext();
		var allocated_employees = [];

		date = 'today';
		nlapiLogExecution('debug', 'date', date);

		// get all active allocation for the current date
		var search_result = searchRecord('resourceallocation', null, [
		        new nlobjSearchFilter('custentity_employee_inactive',
		                'employee', 'is', 'F'),
				new nlobjSearchFilter('startdate', null, 'notafter', date),
		        new nlobjSearchFilter('enddate', null, 'notbefore', date) ],
		        [ new nlobjSearchColumn('resource', null, 'group') ]);

		nlapiLogExecution('debug', 'search_result', search_result.length);

		search_result.forEach(function(allocation) {
			// nlapiLogExecution('debug', 'testing', allocation.getValue(
			// 'resource', null, 'group'));
			allocated_employees.push(allocation.getValue('resource', null,
			        'group'));
		});

		nlapiLogExecution('debug', 'allocated resources',
		        allocated_employees.length);

		return allocated_employees;
	} catch (err) {
		nlapiLogExecution('error', 'getAllocatedEmployeeList', err);
		throw err;
	}
}

function getAllocatedEmployeeList_greaterthan45k(date) {
	try {
		var context = nlapiGetContext();
		var allocated_employees_45k = [];

		date = 'today';
		nlapiLogExecution('debug', 'date', date);

		// get all active allocation for the current date
		var search_result = searchRecord('resourceallocation', null, [
		        new nlobjSearchFilter('custentity_employee_inactive',
		                'employee', 'is', 'F'),
				new nlobjSearchFilter('formulanumeric', 'employee', 'greaterthan',
			    	        		35000).setFormula('{internalid}'),			
		        new nlobjSearchFilter('startdate', null, 'notafter', date),
		        new nlobjSearchFilter('enddate', null, 'notbefore', date) ],
		        [ new nlobjSearchColumn('resource', null, 'group') ]);

		nlapiLogExecution('debug', 'search_result', search_result.length);

		search_result.forEach(function(allocation) {
			// nlapiLogExecution('debug', 'testing', allocation.getValue(
			// 'resource', null, 'group'));
			allocated_employees_45k.push(allocation.getValue('resource', null,
			        'group'));
		});

		nlapiLogExecution('debug', 'allocated resources_45k',
		        allocated_employees_45k.length);

		return allocated_employees_45k;
	} catch (err) {
		nlapiLogExecution('error', 'getAllocatedEmployeeList', err);
		throw err;
	}
}

function generateAndSendZeroAllocationReport(date) {
	try {
		var zero_allocated_list = getZeroAllocationEmployee(date);
		// nlapiLogExecution('debug', 'zero_allocated_list', JSON
		// .stringify(zero_allocated_list));
		var userId = nlapiGetContext().getUser();
		var context = nlapiGetContext();
		var userEmail = context.getEmail();
		nlapiLogExecution('debug','Logged User',userId);
		nlapiLogExecution('debug','Logged userEmail',userEmail);
		var usageEnd = context.getRemainingUsage();
							if (usageEnd < 1000) 
							{
								yieldScript(context);
							}
		var csvText = "";
			csvText += "Employee,";
			csvText += "Hire Date,";
			csvText += "Practice,";
			csvText += "Subsidiary,";
			csvText += "Employee Type,";
			csvText += "Employee Level,";
			csvText += "Last Allocated Project,";
			csvText += "Project Region,";//mani
			csvText += "Customer Subsidiary,";//shamanth
			csvText += "Last Allocated Customer,";
			csvText += "Last Allocation Start Date,";
			csvText += "Last Allocation End Date,";
			csvText += "Last Allocation Record,\n";
			

			zero_allocated_list.forEach(function(emp) {
				csvText += emp.entityid + ",";
				csvText += emp.hiredate + ",";
				csvText += emp.practice + ",";
				csvText += emp.subsidiary + ",";
				csvText += emp.employeetype + ",";
				csvText += emp.employee_Level + ",";
				csvText += emp.last_allocation_name + ",";
				csvText += emp.last_region + ","; //mani
				csvText += emp.last_allocation_customer_name + ",";
				csvText += emp.customer_subsidiary + ",";//shamanth
				csvText += emp.last_allocation_start_date + ",";
				csvText += emp.last_allocation_end_date + ",";
				csvText += emp.open_allocation + ",\n";
			});

			//Generate Excel 
			var today = new Date();
		var dd = today.getDate();
		var mm = today.getMonth()+1;
		var yyyy = today.getFullYear();
		var hours = today.getHours();
		var minutes = today.getMinutes();
		
		if(dd<10){
		    dd='0'+dd
		} 
		if(mm<10){
		    mm='0'+mm
		} 
		var today = dd+'/'+mm+'/'+yyyy;
		
			var strVar1= '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:ss="urn:schemas-microsoft-com:office:spreadsheet" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40">'+
		'<head>'+
		'<meta http-equiv=Content-Type content="text/html; charset=windows-1252"/>'+
        '<meta name=ProgId content=Excel.Sheet/>'+
        '<meta name=Generator content="Microsoft Excel 11"/>'+
        '<!--[if gte mso 9]><xml>'+
        '<x:excelworkbook>'+
        '<x:excelworksheets>'+
        '<x:excelworksheet=sheet1>'+
        '<x:name>** ESTIMATE FILE**</x:name>'+
        '<x:worksheetoptions>'+
        '<x:selected></x:selected>'+
        '<x:freezepanes></x:freezepanes>'+
        '<x:frozennosplit></x:frozennosplit>'+
        '<x:splithorizontal>** FROZEN ROWS + 1 **</x:splithorizontal>'+
        '<x:toprowbottompane>** FROZEN ROWS + 1 **</x:toprowbottompane>'+
        '<x:splitvertical>** FROZEN COLUMNS + 1 **</x:splitvertical>'+
        '<x:leftcolumnrightpane>** FROZEN COLUMNS + 1**</x:leftcolumnrightpane>'+
		    '<x:activepane>0</x:activepane>'+// 0
		    '<x:panes>'+
			'<x:pane>'+
			'<x:number>3</x:number>'+
			'</x:pane>'+
			'<x:pane>'+
		    '<x:number>1</x:number>'+
		    '</x:pane>'+
		    '<x:pane>'+
			'<x:number>2</x:number>'+
        '</x:pane>'+
        '<x:pane>'+
        '<x:number>0</x:number>'+//1
        '</x:pane>'+
        '</x:panes>'+
        '<x:protectcontents>False</x:protectcontents>'+
        '<x:protectobjects>False</x:protectobjects>'+
        '<x:protectscenarios>False</x:protectscenarios>'+
        '</x:worksheetoptions>'+
        '</x:excelworksheet>'+
		'</x:excelworksheets>'+
        '<x:protectstructure>False</x:protectstructure>'+
        '<x:protectwindows>False</x:protectwindows>'+
		'</x:excelworkbook>'+
        
        // '<style>'+
			
			//-------------------------------------
        '</x:excelworkbook>'+
        '</xml><![endif]-->'+
         '<style>'+
		'p.MsoFooter, li.MsoFooter, div.MsoFooter'+
		'{ margin:0in; margin-bottom:.0001pt; mso-pagination:widow-orphan; tab-stops:center 3.0in right 6.0in; font-size:12.0pt;}'+
		'<style>'+

		'<!-- /* Style Definitions */'+

		//'@page Section1'+
		//'{ size:8.5in 11.0in; margin:1.9cm 1.27cm 1.9cm 1.27cm; mso-header-margin:.5in; mso-footer-margin:.5in; mso-title-page:yes; mso-header: h1; mso-footer: f1; mso-first-header: fh1; mso-first-footer: ff1; mso-paper-source:0;}'+

		'div.Section1'+
		'{ page:Section1;}'+

		'table#hrdftrtbl'+
		'{ margin:0in 0in 0in 900in; width:1px; height:1px; overflow:hidden; font-family:Arial;}-->'+

		'</style>'+
		
		
		'</head>'+
		

		'<body>'+
		
		'<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">'
		
		var strVar_excel = '';
		//var strVar = '';
		//strVar += '<html>';
		//strVar += '<body>';
		
		//strVar += '<p>Daily Ticker Resource Data dated '+today+' '+hours+':'+minutes+' (24 Hour Format)</p>';
			
		strVar_excel += '<table>';
		
		strVar_excel += '	<tr>';
		strVar_excel += ' <td width="100%">';
		strVar_excel += '<table width="100%" border="1">';
		
			strVar_excel += '	<tr>';
			strVar_excel += ' <td width="6%" font-size="11" align="center" style="background-color:#999999">Employee</td>';
			strVar_excel += ' <td width="6%" font-size="11" align="center" style="background-color:#999999">Hire Date</td>';
			strVar_excel += ' <td width="6%" font-size="11" align="center" style="background-color:#999999">Practice</td>';
			strVar_excel += ' <td width="6%" font-size="11" align="center" style="background-color:#999999">Subsidiary</td>';
			strVar_excel += ' <td width="6%" font-size="11" align="center" style="background-color:#999999">Employee Type</td>';
			strVar_excel += ' <td width="10%" font-size="11" align="left" style="background-color:#999999">Employee Level</td>';
			strVar_excel += ' <td width="6%" font-size="11" align="center" style="background-color:#999999">Last Allocated Project</td>';
			strVar_excel += ' <td width="6%" font-size="11" align="center" style="background-color:#999999">Project Region</td>';//mani
			
			strVar_excel += ' <td width="6%" font-size="11" align="center" style="background-color:#999999">Last Allocated Customer</td>';
			strVar_excel += ' <td width="6%" font-size="11" align="center" style="background-color:#999999">Customer Subsidiary</td>'; //shamanth
			strVar_excel += ' <td width="6%" font-size="11" align="center" style="background-color:#999999">Last Allocation Start Date</td>';
			strVar_excel += ' <td width="6%" font-size="11" align="center" style="background-color:#999999">Last Allocation End Date</td>';
			strVar_excel += ' <td width="30%" font-size="11" align="center" style="background-color:#999999">Last Allocation Record</td>';
			
			strVar_excel += '</tr>';
			
			zero_allocated_list.forEach(function(emp) {
			strVar_excel += '<tr>';
				strVar_excel += ' <td width="6%" font-size="11" align="center">' +emp.entityid+ '</td>';
				strVar_excel += ' <td width="6%" font-size="11" align="center">' +emp.hiredate+ '</td>';
				strVar_excel += ' <td width="6%" font-size="11" align="center">' +emp.practice+ '</td>';
				strVar_excel += ' <td width="6%" font-size="11" align="center">' +emp.subsidiary+ '</td>';
				strVar_excel += ' <td width="6%" font-size="11" align="center">' +emp.employeetype+ '</td>';
				strVar_excel += ' <td width="10%" font-size="11" align="left">' +emp.employee_Level+ '</td>';
				strVar_excel += ' <td width="6%" font-size="11" align="center">' +emp.last_allocation_name+ '</td>';
				strVar_excel += ' <td width="6%" font-size="11" align="center">' +emp.last_region+ '</td>';//mani
				
				strVar_excel += ' <td width="6%" font-size="11" align="center">' +emp.last_allocation_customer_name+ '</td>';
				
				strVar_excel += ' <td width="6%" font-size="11" align="center">' +emp.customer_subsidiary+ '</td>';//shamanth
				
				strVar_excel += ' <td width="6%" font-size="11" align="center">' +emp.last_allocation_start_date+ '</td>';
				strVar_excel += ' <td width="6%" font-size="11" align="center">' +emp.last_allocation_end_date+ '</td>';
				strVar_excel += ' <td width="30%" font-size="11" align="center">' +emp.open_allocation+ '</td>';
				
				strVar_excel += '	</tr>';
				
			});
			strVar_excel += '</table>';
			strVar_excel += ' </td>';	
			strVar_excel += '	</tr>';
		
			strVar_excel += '</table>';
			var usageEnd = context.getRemainingUsage();
							if (usageEnd < 1000) 
							{
								yieldScript(context);
							}
			
			 strVar1 = strVar1 + strVar_excel;
			var fileName = 'Unallocated Resource - '
			        + nlapiDateToString(new Date(), 'date') + '.xls';
			var csvFile = nlapiCreateFile(fileName, 'XMLDOC', strVar1);
			//csvFile.setFolder('105427'); //Sandbox
			csvFile.setFolder('138530');  //Prod
			 var fileID = nlapiSubmitFile(csvFile);
			 nlapiLogExecution('debug','File ID',fileID);
			//response.setContentType('CSV', fileName);
			//response.write(csvFile.getValue());

		if (fileID) {
			// nlapiLogExecution('debug', 'send emails');
			var author = 442;
			var recipient = 'business.ops@brillio.com'; //business.ops
			//var recipient ='manu.chavan@brillio.com';
			//var recipient = 'sai.vannamareddy@brillio.com';
			var cc = new Array();
			
			cc[cc.length] = 'information.systems@brillio.com';
			//cc[1] = 'deepak.srinivas@brillio.com';
			// cc[cc.length] = 'vimal.pandey@brillio.com'; Deployer Shravan, Requestor Sitaram, Commented on 16th April 2021
			//cc[0] = 'sai.vannamareddy@brillio.com';
			cc[cc.length] = 'subin.benedict@BRILLIO.COM';
          	cc[cc.length] = 'gopal.agrawal@BRILLIO.COM';
         // 	cc[cc.length] = 'anil.sanghi@BRILLIO.COM';
            cc[cc.length] = 'hitesh.k@BRILLIO.COM';
            cc[cc.length] = 'prateek.yadav@BRILLIO.COM';
            cc[cc.length] = 'swetha.b@BRILLIO.COM';
			cc[cc.length] = 'bharath.s@BRILLIO.COM';
			cc[cc.length] = 'senjuti.c@BRILLIO.COM';
			cc[cc.length] = 'Sharathkumar.s@brillio.com';
			cc[cc.length] = 'Sravan.Bharanikana@brillio.com';
		//	cc[cc.length] = 'akhilesh.garg@BRILLIO.COM';
			cc[cc.length] = 'balaram.srinivasan@BRILLIO.COM';
			cc[cc.length] = 'ravishankar.shet@brillio.com';
            cc[cc.length] = 'harish.gopalaraju@brillio.com';
			cc[cc.length] = 'udaya.bhaskar@BRILLIO.COM';
          //  cc[cc.length] = 'reema.ks@brillio.com';
			cc[cc.length] = 'prabhakaran.g@brillio.com';
            cc[cc.length] = 'lavanya.p@brillio.com';
		//	cc[cc.length] = 'sunil.chakraborty@brillio.com';
         	cc[cc.length] = 'Rupali.Jain@brillio.com';
          	cc[cc.length] = 'swapnil.g@brillio.com';
          	cc[cc.length] = 'yogesh.gokhale@brillio.com';
          	cc[cc.length] = 'archana.r@brillio.com';
            cc[cc.length] = 'mahesh.kotiyan@brillio.com';
          cc[cc.length] = 'sharrel.p@brillio.com';
            cc[cc.length] = 'swastika.mishra@brillio.com';
          cc[cc.length] = 'manigandan.b@brillio.com';
            cc[cc.length] = 'senjuti.c1@brillio.com';
          //Added by Shreyash Mundhra 02/08/2021 NIS-2321
            cc[cc.length] = 'Deepesh.Bansal@brillio.com';
            cc[cc.length] = 'balaji.pr@brillio.com';
          // Added by Sitaram on 22/11/2021
            cc[cc.length] = '9cf12f64.brillio.com@apac.teams.ms';
            cc[cc.length] = 'Midhun.Mohan@brillio.com';
            cc[cc.length] = 'Shyamkrishnan.S@brillio.com';
            cc[cc.length] = 'ghanshyam.tiwari@brillio.com';
            cc[cc.length] = 'Rani.Harini@brillio.com';
            cc[cc.length] = 'Somineni.M@brillio.com';
            
          
          
 			
			var bcc = 'deepak.srinivas@brillio.com';
			var newAttachment = nlapiLoadFile(fileID);
			var subject  = 'Un Allocated Resources as on '+ nlapiDateToString(new Date(), 'date') ;
			var body = 'Please find the attached unallocated Report for dated' + ' - ' + nlapiDateToString(new Date(), 'date');
			
			nlapiSendEmail(author, recipient, subject, body, cc, bcc, null, newAttachment);
			nlapiLogExecution('debug','Mail Sent to User','USER ->'+recipient);
			//Get second suitelet URL and attach parameter to it

		//var suiteletUrl = nlapiResolveURL('SUITELET', 'customscript1036', 'customdeploy1', false);

		//var param = '&fileid=' + fileID;

		//var finalUrl = suiteletUrl + param;
		//var req = nlapiRequestURL('https://debugger.sandbox.netsuite.com'+finalUrl);
		//var setWindow = window.open(finalUrl );
		}
	} catch (err) {
		nlapiLogExecution('error', 'generateAndSendZeroAllocationReport', err);
		throw err;
	}
}

function getZeroAllocationEmployee(date) {
	try {
		var context = nlapiGetContext();
		var allocated_employees = getAllocatedEmployeeList(date);
		//var allocated_employees_Grt_45k = getAllocatedEmployeeList_greaterthan45k(date);
		
		// nlapiLogExecution('debug', 'allocated resources', JSON
		// .stringify(allocated_employees));

		if (isArrayNotEmpty(allocated_employees)) {
			// pull the left out active employees
			
			var unallocated_employees_search_active = searchRecord('employee', null, [
			        new nlobjSearchFilter('custentity_employee_inactive', null,
			                'is', 'F'),
			        // new nlobjSearchFilter('custentity_lwd', null,
			        // 'notbefore',
			        // date),
			        new nlobjSearchFilter('custentity_implementationteam',
			                null, 'is', 'F'),
			       // new nlobjSearchFilter('formulanumeric', null, 'lessthanorequalto',
			    	//        		60000).setFormula('{internalid}'),
			         ], [
			        new nlobjSearchColumn('internalid')
			         ]);

			 nlapiLogExecution('debug', 'unallocated_employees_search_active ',
			 unallocated_employees_search_active.length);
			 var active_emp_list = [];
			 if(unallocated_employees_search_active){
			 for(var index=0;index<unallocated_employees_search_active.length;index++){
			 active_emp_list.push(unallocated_employees_search_active[index].getValue('internalid'));
			 }
			 }
			 var flag = 0;
			 var active_unallocated_emp = [];
			if(active_emp_list){			 
						 for (var i = 0; i<allocated_employees.length; i++) {
				var arrlen = active_emp_list.length;
				for (var j = 0; j<arrlen; j++) {
					if (allocated_employees[i] == active_emp_list[j]) {
						active_emp_list = active_emp_list.slice(0, j).concat(active_emp_list.slice(j+1, arrlen));
					}//if close
				}//for close
				}//for close
			}
			 
			/* if(active_emp_list){
			 for(var i=0;i<active_emp_list.length;i++){
				for(j=0;j<allocated_employees.length;j++){
				if(active_emp_list[i] == allocated_employees[j]){
				flag = 1;
				break;
				}
				if(flag == 0){
				active_unallocated_emp.push(active_emp_list[i]);
				}
				//if(active_emp_list.indexOf(allocated_employees[j]) == -1)
				//active_unallocated_emp.push(active_emp_list[i]);
				}
			 
			 }
			 } */
			
			var unallocated_employees_search = nlapiSearchRecord('employee', null, [
			        //new nlobjSearchFilter('custentity_employee_inactive', null,
			        //        'is', 'F'),
			        // new nlobjSearchFilter('custentity_lwd', null,
			        // 'notbefore',
			        // date),
			        //new nlobjSearchFilter('custentity_implementationteam',
			       //         null, 'is', 'F'),
			       // new nlobjSearchFilter('formulanumeric', null, 'lessthanorequalto',
			    	//        		60000).setFormula('{internalid}'),
			        new nlobjSearchFilter('internalid', null, 'anyof',
			                active_emp_list) ], [
			        new nlobjSearchColumn('entityid'),
			        new nlobjSearchColumn('department'),
			        new nlobjSearchColumn('location'),
			        new nlobjSearchColumn('hiredate'),
			        new nlobjSearchColumn('subsidiary'),
			        new nlobjSearchColumn('title'),
			        new nlobjSearchColumn('custentity_persontype'),
					new nlobjSearchColumn('employeestatus'),
			        new nlobjSearchColumn('custentity_employeetype') ]);

			 nlapiLogExecution('debug', 'unallocated_employees_search less than 35000',
			 unallocated_employees_search.length);
			var usageEnd = context.getRemainingUsage();
							if (usageEnd < 1000) 
							{
								yieldScript(context);
							}
			var unallocated_employees = [];
			var employee_list = [];

			unallocated_employees_search
			        .forEach(function(employee) {
				        unallocated_employees
				                .push({
				                    entityid : employee.getValue('entityid'),
				                    internalid : employee.getId(),
				                    viewlink : '<a target="_blank" style="color:#255599;text-decoration:none;"'
				                            + ' href="/app/common/entity/employee.nl?id='
				                            + employee.getId() + '">View</a>',
				                    practice : employee.getText('department'),
				                    location : employee.getText('location'),
				                    jobtitle : employee.getValue('title'),
				                    hiredate : employee.getValue('hiredate'),
				                    subsidiary : employee.getText('subsidiary'),
				                    employeetype : employee
				                            .getText('custentity_persontype'),
				                    persontype : employee
				                            .getText('custentity_employeetype'),
									employee_Level : employee
				                            .getText('employeestatus'),
				                    last_allocation : '',
									customer_subsidiary : '', //shamanth
									last_region    :'',//mani
				                    last_allocation_customer : '',
				                    last_allocation_customer_name : '',
				                    last_allocation_name : '',
				                    last_allocation_end_date : '',
				                    last_allocation_start_date : '',
				                    open_allocation : ''
				                });

				        employee_list.push(employee.getId());
			        });
			
			/*var unallocated_employees_search_45k = nlapiSearchRecord('employee', 'customsearch1740', [
	       // new nlobjSearchFilter('custentity_employee_inactive', null,
	       //         'is', 'F'),
	        // new nlobjSearchFilter('custentity_lwd', null,
	        // 'notbefore',
	        // date),
	       // new nlobjSearchFilter('custentity_implementationteam',
	       //         null, 'is', 'F'),
	       // new nlobjSearchFilter('formulanumeric', null, 'greaterthan',
	    	//        		60000).setFormula('{internalid}'),
			new nlobjSearchFilter('internalid', null, 'noneof',
	                allocated_employees) ], [
	        new nlobjSearchColumn('entityid'),
	        new nlobjSearchColumn('department'),
	        new nlobjSearchColumn('location'),
	        new nlobjSearchColumn('hiredate'),
	        new nlobjSearchColumn('subsidiary'),
	        new nlobjSearchColumn('title'),
	        new nlobjSearchColumn('custentity_persontype'),
			new nlobjSearchColumn('employeestatus'),
	        new nlobjSearchColumn('custentity_employeetype') ]);
		   
		   nlapiLogExecution('debug', 'unallocated_employees_search_45k greater than 35000',
					 unallocated_employees_search_45k.length);
			unallocated_employees_search_45k
	        .forEach(function(employee) {
		        unallocated_employees
		                .push({
		                    entityid : employee.getValue('entityid'),
		                    internalid : employee.getId(),
		                    viewlink : '<a target="_blank" style="color:#255599;text-decoration:none;"'
		                            + ' href="/app/common/entity/employee.nl?id='
		                            + employee.getId() + '">View</a>',
		                    practice : employee.getText('department'),
		                    location : employee.getText('location'),
		                    jobtitle : employee.getValue('title'),
		                    hiredate : employee.getValue('hiredate'),
		                    subsidiary : employee.getText('subsidiary'),
		                    employeetype : employee
		                            .getText('custentity_persontype'),
		                    persontype : employee
		                            .getText('custentity_employeetype'),
							employee_Level : employee
				                    .getText('employeestatus'),
		                    last_allocation : '',
		                    last_allocation_customer : '',
		                    last_allocation_customer_name : '',
		                    last_allocation_name : '',
		                    last_allocation_end_date : '',
		                    last_allocation_start_date : '',
		                    open_allocation : ''
		                });

		        employee_list.push(employee.getId());
	        }); */
			
			
			
			// get the last allocation of all the employee
			try {
				var usageEnd = context.getRemainingUsage();
							if (usageEnd < 1000) 
							{
								yieldScript(context);
							}
					var filter = [];
					filter.push(new nlobjSearchFilter('isinactive', 'employee' , 'anyof', 'F'));
					filter.push(new nlobjSearchFilter('custentity_implementationteam', 'employee' , 'anyof', 'F'));
					filter.push(new nlobjSearchFilter('resource', null , 'anyof', employee_list));
				var cols  = [];
				cols.push(new nlobjSearchColumn('resource'));
				cols.push(new nlobjSearchColumn('enddate').setSort(true));
				cols.push(new nlobjSearchColumn('startdate'));
				cols.push(new nlobjSearchColumn('company'));
				cols.push(new nlobjSearchColumn('customer'));
				cols.push(new nlobjSearchColumn('subsidiary','customer'));
				cols.push(new nlobjSearchColumn('internalid'));
				cols.push(new nlobjSearchColumn('custentity_region','job'));

				//var allocationSearchResult = nlapiSearchRecord(
				//        'resourceallocation', 1076, [ new nlobjSearchFilter(
				 //               'resource', null, 'anyof', employee_list) ],cols);

				var ofc_cost_search = nlapiCreateSearch('resourceallocation',filter,cols);	
				var resultset_ = ofc_cost_search.runSearch();
					var allocationSearchResult = [];
					var searchid_ = 0;
					
					var currentContext_C = nlapiGetContext();
					var remaining_usage = currentContext_C.getRemainingUsage();
					//nlapiLogExecution('DEBUG','OFC Cost remaining_usage',remaining_usage);
					do {
						var resultslice_ = resultset_.getResults(searchid_, searchid_ + 1000);
						for ( var re in resultslice_) {
							if(currentContext_C.getRemainingUsage() <= 1000){
							nlapiYieldScript();
							}
							allocationSearchResult.push(resultslice_[re]);
							searchid_++;
						}
					} while (resultslice_.length >= 1000);
				//End
				if (allocationSearchResult) {

					for (var i = 0; i < unallocated_employees.length; i++) {

						for (var j = 0; j < allocationSearchResult.length; j++) {

							if (parseInt(unallocated_employees[i].internalid) == parseInt(allocationSearchResult[j].getValue('resource'))) {
								var employ = unallocated_employees[i].internalid ;
								unallocated_employees[i].last_allocation_end_date = allocationSearchResult[j]
								        .getValue('enddate');
								unallocated_employees[i].last_allocation_start_date = allocationSearchResult[j]
								        .getValue('startdate');
								unallocated_employees[i].last_allocation = allocationSearchResult[j]
								        .getValue('company');
								unallocated_employees[i].last_allocation_name = allocationSearchResult[j]
								        .getText('company');
								unallocated_employees[i].last_allocation_customer = allocationSearchResult[j]
								        .getValue('customer');
								unallocated_employees[i].last_allocation_customer_name = allocationSearchResult[j]
								        .getText('customer');
								unallocated_employees[i].customer_subsidiary = allocationSearchResult[j]
								        .getText('subsidiary','customer');	 //shamanth	
										
								unallocated_employees[i].last_region = allocationSearchResult[j]
								       .getText('custentity_region','job');	//mani
										
								unallocated_employees[i].open_allocation = 'https://system.na1.netsuite.com/app/accounting/project/allocation.nl?id='
								        + allocationSearchResult[j].getId();

								break;
							}
						}
					}
				}
			} catch (ex) {
				nlapiLogExecution('error', 'allocationSearchResult', ex);
			}

			nlapiLogExecution('debug', 'final data', JSON
			        .stringify(unallocated_employees));
			return unallocated_employees;
		}
		return [];
	} catch (err) {
		nlapiLogExecution('error', 'getZeroAllocationEmployee', err);
		throw err;
	}
}

function suitelet(request, response) {
	try {
		var zero_allocated_list = getZeroAllocationEmployee(request
		        .getParameter('custpage_date'));

		var mode = request.getParameter('mode');

		if (mode != "export") {
			// creating a form to display the result
			var form = nlapiCreateForm('Unallocated Employees');
			var context = nlapiGetContext();

			var url = "<a href='"
			        + nlapiResolveURL('SUITELET', context.getScriptId(),
			                context.getDeploymentId())
			        + "&mode=export'> CSV Export </a>";
			form.addField("custpage_export_link", "inlinehtml", "")
			        .setDisplayType('inline').setDefaultValue(url);

			form.addTab('custpage_tab_1', 'Resource List');

			var list = form.addSubList('custpage_unallocated_emp_list',
			        'staticlist', 'List', 'custpage_tab_1');
			list.addField('viewlink', 'text', '').setDisplayType('inline');
			list.addField('entityid', 'text', 'Employee').setDisplayType(
			        'inline');
			list.addField('hiredate', 'date', 'Hire Date').setDisplayType(
			        'inline');
			list.addField('practice', 'text', 'Practice').setDisplayType(
			        'inline');
			list.addField('subsidiary', 'text', 'Subsidiary').setDisplayType(
			        'inline');
			list.addField('employeetype', 'text', 'Employee Type')
			        .setDisplayType('inline');
			// list.addField('persontype', 'text', 'Person
			// Type').setDisplayType(
			// 'inline');
			list.addField('last_allocation', 'select', 'Last Project', 'job')
			        .setDisplayType('inline');
					
			list.addField('last_region', 'select', 'Last Region', 'job')
			        .setDisplayType('inline');//mani
							
					
			list.addField('last_allocation_customer', 'select',
			        'Last Customer', 'customer').setDisplayType('inline');
			list.addField('last_allocation_start_date', 'date', 'Start Date')
			        .setDisplayType('inline');
			list.addField('last_allocation_end_date', 'date', 'End Date')
			        .setDisplayType('inline');
			list.addField('open_allocation', 'text', 'Last Allocation')
			        .setDisplayType('inline');
			list.setLineItemValues(zero_allocated_list);
			response.writePage(form);
		} else {
			var csvText = "";
			csvText += "Employee,";
			csvText += "Hire Date,";
			csvText += "Practice,";
			csvText += "Subsidiary,";
			csvText += "Employee Type,";
			csvText += "Last Allocated Project,";
			csvText += "Last Allocated Customer,";
			csvText += "Last Subsidary,";
			csvText += "Project Region,";//mani
			csvText += "Last Allocation Start Date,";
			csvText += "Last Allocation End Date,\n";

			zero_allocated_list.forEach(function(emp) {
				csvText += emp.entityid + ",";
				csvText += emp.hiredate + ",";
				csvText += emp.practice + ",";
				csvText += emp.subsidiary + ",";
				csvText += emp.employeetype + ",";
				csvText += emp.last_allocation_name + ",";
				csvText += emp.last_allocation_customer_name + ",";
				csvText += emp.customer_subsidiary + ","; //shamanth
				csvText += emp.last_region + ",";//mani
				csvText += emp.last_allocation_start_date + ",";
				csvText += emp.last_allocation_end_date + ",\n";
			});

			var fileName = 'Unallocated Resource - '
			        + nlapiDateToString(new Date(), 'date') + '.csv';
			var csvFile = nlapiCreateFile(fileName, 'CSV', csvText);
			response.setContentType('CSV', fileName);
			response.write(csvFile.getValue());
		}
	} catch (err) {
		nlapiLogExecution('error', 'suitelet', err);
		displayErrorForm(err);
	}
}

function exportToExcel() {
	try {

	} catch (err) {

	}
}
function yieldScript(currentContext) {

		nlapiLogExecution('AUDIT', 'API Limit Exceeded');
		var state = nlapiYieldScript();

		if (state.status == "FAILURE") {
			nlapiLogExecution('ERROR', 'Yield Script Failed', 'Reason : '
			        + state.reason + ' / Size : ' + state.size);
			return false;
		} else if (state.status == "RESUME") {
			nlapiLogExecution('AUDIT', 'Script Resumed');
		}
}