// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
    /*
     Script Name: SCH_Update_Leave_Holiday
     Author: Vikrant
     Company: Aashna CloudTech
     Date: 20-11-2014
     Script Modification Log:
     -- Date --			-- Modified By --				--Requested By--				-- Description --
     Below is a summary of the process controls enforced by this script file.  The control logic is described
     more fully, below, in the appropriate function headers and code blocks.
     SCHEDULED FUNCTION
     - SCH_Update_Leave_Holiday(type)
     SUB-FUNCTIONS
     - The following sub-functions are called by the above core functions in order to maintain code
     modularization:
     - NOT USED
     */
}
// END SCRIPT DESCRIPTION BLOCK  ====================================


// BEGIN SCHEDULED FUNCTION =============================================

function SCH_Update_Leave_Holiday(type)//
{
    try //
    {
        nlapiLogExecution('DEBUG', 'SCH_Update_Leave_Holiday', '**** Execution Started ****')
        
        var search_Result = null;
        do //
        {
            search_Result = nlapiSearchRecord('projecttask', 'customsearch_update_search', null, null);
            
            if (_isValid(search_Result)) //
            {
                var a_all_columns = search_Result[0].getAllColumns();
                nlapiLogExecution('DEBUG', 'SCH_Update_Leave_Holiday', 'search_Result : ' + search_Result.length);
				
                for (var counter_I = 0; counter_I < search_Result.length; counter_I++) //
                {
                    nlapiLogExecution('DEBUG', 'SCH_Update_Leave_Holiday', '**** ' + counter_I + ' ****')
                    
                    var i_task_id = search_Result[counter_I].getValue(a_all_columns[0]);
                    nlapiLogExecution('DEBUG', 'SCH_Update_Leave_Holiday', 'i_task_id : ' + i_task_id);
                    
                    var r_task_Rec = nlapiLoadRecord('projecttask', i_task_id);
                    
                    if (_isValid(r_task_Rec)) //
                    {
                        var i_line_count = r_task_Rec.getLineItemCount('assignee');
                        
                        if (i_line_count > 0) //
                        {
                            for (var counter_J = 1; counter_J <= i_line_count; counter_J++) //
                            {
                                var i_resource = r_task_Rec.getLineItemValue('assignee', 'resource', counter_J);
                                var i_serviceitem = r_task_Rec.getLineItemValue('assignee', 'serviceitem', counter_J);
                                var f_unitprice = r_task_Rec.getLineItemValue('assignee', 'unitprice', counter_J);
                                
                                //f_unitprice = parseFloat(f_unitprice);
                                
                                r_task_Rec.setLineItemValue('assignee', 'serviceitem', counter_J, 2479); // Set as Leave...
                                r_task_Rec.setLineItemValue('assignee', 'unitprice', counter_J, f_unitprice);
                                
                            }
                            
                            try //
                            {
                                var submitted = nlapiSubmitRecord(r_task_Rec);
                                nlapiLogExecution('DEBUG', 'SCH_Update_Leave_Holiday', 'Record Submitted : ' + submitted);
                                
                                //return;
                            } 
                            catch (exc) //
                            {
                                nlapiLogExecution('ERROR', 'SCH_Update_Leave_Holiday', 'Inner Transaction ID : ' + i_task_id);
                                nlapiLogExecution('ERROR', 'SCH_Update_Leave_Holiday', 'Inner Ex : ' + exc);
                                nlapiLogExecution('ERROR', 'SCH_Update_Leave_Holiday', 'Inner Ex : ' + exc.message);
                            }
                            
                            if (nlapiGetContext().getRemainingUsage() < 100) //
                            {
                                try //
                                {
                                    nlapiYieldScript();
                                } 
                                catch (ex_yld) //
                                {
                                    // Define SYSTEM parameters to schedule the script to re-schedule.
                                    var params = new Array();
                                    params['status'] = 'scheduled';
                                    params['runasadmin'] = 'T';
                                    var startDate = new Date();
                                    params['startdate'] = startDate.toUTCString();
                                    
                                    // Define CUSTOM parameters to schedule the script to re-schedule.
                                    //params['custscript_invoice_id'] = initial_ID;
                                    
                                    //nlapiLogExecution('DEBUG', 'SCH_Update_Leave_Holiday ', ' Script Status --> Before schedule...');
                                    
                                    var status = nlapiScheduleScript(nlapiGetContext().getScriptId(), nlapiGetContext().getDeploymentId(), params);
                                    //nlapiLogExecution('DEBUG', 'SCH_Update_Leave_Holiday ', ' Script Status -->' + status);
                                    
                                    ////If script is scheduled then successfully then check for if status=queued
                                    if (status == 'QUEUED') //
                                    {
                                        nlapiLogExecution('DEBUG', 'SCH_Update_Leave_Holiday', ' Script is rescheduled ....................');
                                    }
                                    return;
                                }
                            }
                        }
                    }
                }
            }
        }
        while (_isValid(search_Result) == false) //
        {
        
        }
        
    } 
    catch (Ex) //
    {
        nlapiLogExecution('ERROR', 'SCH_Update_Leave_Holiday', 'Ex : ' + Ex);
        nlapiLogExecution('ERROR', 'SCH_Update_Leave_Holiday', 'Ex : ' + Ex.message);
    }
    nlapiLogExecution('DEBUG', 'SCH_Update_Leave_Holiday', '**** Execution Completed ****')
}


// END SCHEDULED FUNCTION ===============================================





// BEGIN FUNCTION ===================================================
{
    function _isValid(obj) //
    {
        if (obj != null && obj != '' && obj != 'undefined')//
        {
            return true;
        }
        else //
        {
            return false;
        }
    }
    
    
}
// END FUNCTION =====================================================
