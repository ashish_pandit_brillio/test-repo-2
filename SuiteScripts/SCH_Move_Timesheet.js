/**
 * Module Description
 * 
 * Version Date Author Remarks 1.00 30 Jun 2015 nitish.mishra
 * Version Date Author Remarks 2.00 22 Apr 2020 Praveena  Changed sublist ids on timesheet
 */

//function main() {
function massUpdate() {
	var rec = nlapiSearchRecord(null, 954);

	for (var i = 0; i < rec.length; i++) {
		try {
			moveTimesheet(rec[i].getId());
			// break;
		} catch (err) {
			nlapiLogExecution('error', rec[i].getId(), err);
		}

	}
}

/**
 * @param {String}
 *            type Context Types: scheduled, ondemand, userinterface, aborted,
 *            skipped
 * @returns {Void}
 */
function moveTimesheet(recId) {

	var ab = [ recId ];

	for (var j = 0; j < ab.length; j++) {
		nlapiLogExecution('debug', 'ID: ', ab[j]);
		var a_days = [ 'timebill0', 'timebill1', 'timebill2', 'timebill3', 'timebill4',
				'timebill5', 'timebill6' ];

		var a;
		try {
			a = nlapiLoadRecord('timesheet', ab[j]);
		} catch (e) {
			nlapiLogExecution('ERROR', 'e', e.message);
			if (e.message == 'That record does not exist.') {
				continue;
			}
			break;
		}

		for (var i = 1; i <= a.getLineItemCount('timeitem'); i++) {
			a.selectLineItem('timeitem', i);
			for (var i_day_indx = 0; i_day_indx < 7; i_day_indx++) {
				var o_sub_record_view = a.getCurrentLineItemValue(
						'timeitem', a_days[i_day_indx]);

				if (o_sub_record_view) {
					nlapiLogExecution("DEBUG",'o_sub_record_view+i',i+" : "+i_day_indx+" : "+o_sub_record_view);
					// if (o_sub_record_view.getFieldValue('customer') != 4398)
					// {
					//a.setCurrentLineItemValue('timeitem', a_days[i_day_indx]);

					a.setCurrentLineItemValue('timeitem','customer', 11181);
					// var new_task_item = null;

					// switch (o_sub_record.getFieldValue('casetaskevent')) {

					// case '17005':
					// case '1432':
					// new_task_item = 17023;
					// break;
					// case '17006':
					// case '1433':
					// new_task_item = 17024;
					// break;
					// case '1434':
					// case '17007':
					// new_task_item = 17025;
					// break;
					// case '5017':
					// case '17008':
					// new_task_item = 17026;
					// break;
					// case '7071':
					// new_task_item = '';
					// break;
					// }
					a.setCurrentLineItemValue('timeitem','casetaskevent', 17737);

					
					// }
				}
			}
nlapiLogExecution("DEBUG",'o_sub_record_view',o_sub_record_view);
			a.commitLineItem('timeitem');
		}
		nlapiLogExecution('DEBUG','before save');
		var i_recid=nlapiSubmitRecord(a);
		nlapiLogExecution('DEBUG','i_recid',i_recid);
	}
}