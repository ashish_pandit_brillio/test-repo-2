/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       29 Dec 2014     amol.sahijwani
 * 2.00		  22 Dec 2020	  Praveena Madem
 */

/**
 * @param {String} recType Record type internal id
 * @param {Number} recId Record internal id
 * @returns {Void}
 */
function massUpdate(recType, recId) {
    // Load the Time Sheet Record
    var recTS = nlapiLoadRecord(recType, recId, {
        recordmode: 'dynamic'
    });
    nlapiLogExecution('AUDIT', 'Timesheet: ' + recId, 'Started');
    var isRecordChanged = false;

    // Array of Subrecord names
    var a_days = ['timebill0', 'timebill1', 'timebill2', 'timebill3', 'timebill4', 'timebill5', 'timebill6'];

    var i_employee_id = recTS.getFieldValue('employee');

    // Get Start Date
    var d_start_date = new Date(recTS.getFieldValue('startdate'));

    // Get End Date
    var d_end_date = new Date(recTS.getFieldValue('enddate'));

    // Get Line Count
    var i_line_count = recTS.getLineItemCount('timeitem'); //recTS.getLineItemCount('timeitem');
    nlapiLogExecution('DEBUG', 'Number of Lines', i_line_count);
    var o_projects = new Object();

    var a_resource_allocations = getResourceAllocationsForEmployee(i_employee_id, d_start_date, d_end_date);
    nlapiLogExecution('DEBUG', 'Test', JSON.stringify(a_resource_allocations));
    // Loop through Line Items
    for (var i_line_indx = 1; i_line_indx <= i_line_count; i_line_indx++) {

        //nlapiLogExecution('AUDIT', 'Test', JSON.stringify(a_days_data));		

        recTS.selectLineItem('timeitem', i_line_indx);

        var isLineItemChanged = false;

        for (var i_day_indx = 0; i_day_indx < 7; i_day_indx++) {
            var sub_record_name = a_days[i_day_indx];
            var o_sub_record_view = recTS.getCurrentLineItemValue('timeitem', sub_record_name);
            if (o_sub_record_view) {
                var d_current_date = nlapiAddDays(nlapiStringToDate(o_sub_record_view.getFieldValue('startdate'), 'date'),i_day_indx);
                var i_project_id = recTS.getCurrentLineItemValue('timeitem','customer');
                var isCurrentlyBillable = recTS.getCurrentLineItemValue('timeitem','isbillable');
                var currentRate = recTS.getCurrentLineItemValue('timeitem','rate');
                var i_item_id = recTS.getCurrentLineItemValue('timeitem','item');
                var i_approval_status = recTS.getCurrentLineItemValue('timeitem','approvalstatus');

                nlapiLogExecution('DEBUG', 'Test', 'Project: ' + i_project_id + ', Item: ' + i_item_id);
                // Check resource allocation
                if (i_project_id != null && i_approval_status != 3) {
                    // Initialise to billable:'F' by default
                    o_day_data = {
                        billable: 'F',
                        rate: 0
                    };

                    if (a_resource_allocations != null) {
                        for (var i_ra_indx = 0; i_ra_indx < a_resource_allocations.length; i_ra_indx++) {
                            var o_resource_allocation = a_resource_allocations[i_ra_indx];

                            //var d_current_date = nlapiAddDays(d_start_date, i_day_indx);
                            if (o_resource_allocation.project_id == i_project_id && o_resource_allocation.is_billable == 'T') {
                                if (d_current_date >= o_resource_allocation.start_date &&
                                    d_current_date <= o_resource_allocation.end_date) {
                                    var i_rate = 0;
                                    if (i_item_id == '2222' || i_item_id == '2221') {
                                        i_rate = o_resource_allocation.st_rate;
                                    } else if (i_item_id == '2425') {
                                        i_rate = o_resource_allocation.ot_rate;
                                    }
                                    o_day_data = {
                                        'billable': 'T',
                                        'rate': i_rate
                                    };
                                }
                            }

                        }
                    }

                    nlapiLogExecution('DEBUG', 'data', JSON.stringify(o_day_data));
                    try {
                        if (o_day_data.billable != isCurrentlyBillable || o_day_data.rate != currentRate) {
                            var o_sub_record = recTS.getCurrentLineItemValue('timeitem', sub_record_name);
                            if (i_item_id != '2479' && i_item_id != '2480' && i_item_id != '2481') {
                               
								nlapiSubmitField("timebill",sub_record_name,['price','rate','isbillable'],[-1,o_day_data.rate,o_day_data.billable]);
                            }
							else{
								nlapiSubmitField("timebill",sub_record_name,['price','rate'],[-1,o_day_data.rate]);
							}
							
                            isLineItemChanged = true;
                            nlapiLogExecution('AUDIT', 'TimesheetId: ' + recId + ', Date: ' + d_current_date.toDateString(), isCurrentlyBillable + ' - ' + o_day_data.billable + ' , ' + currentRate + ' - ' + o_day_data.rate);
                            nlapiLogExecution('DEBUG', 'TimesheetId: ' + recId + ', Date: ' + d_current_date.toDateString(), isCurrentlyBillable + ' - ' + o_day_data.billable + ' , ' + currentRate + ' - ' + o_day_data.rate);
                        }
                    } catch (e) {
                        nlapiLogExecution('ERROR', 'TimesheetId: ' + recId, e.message);
                    }

                }


            }

        }

        if (isLineItemChanged == true) {
            recTS.commitLineItem('timeitem');
            isRecordChanged = true;
        }


    }

    if (isRecordChanged) {
        try {
			 var recTS = nlapiLoadRecord(recType, recId, {
        recordmode: 'dynamic'
    });
           nlapiSubmitRecord(recTS, true, true);
            nlapiLogExecution('AUDIT', 'TimesheetId: ' + recId, 'Record Submitted');
        } catch (e) {
            nlapiLogExecution('ERROR', 'TimesheetId: ' + recId, 'Record Submitted Error');
        }

    }

    nlapiLogExecution('AUDIT', 'Timesheet: ' + recId, 'End');
}

function getResourceAllocationsForEmployee(i_employee_id, d_start_date, d_end_date) {
    // Store resource allocations for project and employee
    var a_resource_allocations = new Array();

    // Get Resource allocations for this week
    var filters = new Array();
    filters[0] = new nlobjSearchFilter('resource', null, 'anyof', i_employee_id);
    //filters[1]	=	new nlobjSearchFilter('project', null, 'anyof', i_project_id);
    filters[1] = new nlobjSearchFilter('custeventbstartdate', null, 'onorbefore', d_end_date);
    filters[2] = new nlobjSearchFilter('custeventbenddate', null, 'onorafter', d_start_date);
    filters[3] = new nlobjSearchFilter('custeventrbillable', null, 'is', 'T');

    var columns = new Array();
    columns[0] = new nlobjSearchColumn('custeventbstartdate');
    columns[1] = new nlobjSearchColumn('custeventbenddate');
    columns[2] = new nlobjSearchColumn('custeventrbillable');
    columns[3] = new nlobjSearchColumn('custevent3');
    columns[4] = new nlobjSearchColumn('custevent_otrate');
    columns[5] = new nlobjSearchColumn('project');

    var search_results = nlapiSearchRecord('resourceallocation', null, filters, columns);

    if (search_results != null && search_results.length > 0) {
        for (var i_search_indx = 0; i_search_indx < search_results.length; i_search_indx++) {
            var i_project_id = search_results[i_search_indx].getValue('project');
            var resource_allocation_start_date = new Date(search_results[i_search_indx].getValue('custeventbstartdate'));
            var resource_allocation_end_date = new Date(search_results[i_search_indx].getValue('custeventbenddate'));
            var is_resource_billable = search_results[i_search_indx].getValue('custeventrbillable');
            var stRate = search_results[i_search_indx].getValue('custevent3');
            var otRate = search_results[i_search_indx].getValue('custevent_otrate');
            a_resource_allocations[i_search_indx] = {
                project_id: i_project_id,
                start_date: resource_allocation_start_date,
                end_date: resource_allocation_end_date,
                is_billable: is_resource_billable,
                st_rate: stRate,
                ot_rate: otRate
            };
        }
    } else {
        a_resource_allocations = null;
    }

    return a_resource_allocations;
}