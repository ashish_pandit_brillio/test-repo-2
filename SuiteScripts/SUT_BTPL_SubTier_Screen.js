/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       25 Jan 2016     amol.sahijwani
 *
 */

/**
 * @param {nlobjRequest} request Request object
 * @param {nlobjResponse} response Response object
 * @returns {Void} Any output is written via response object
 */
function suitelet(request, response) {

	if (request.getMethod() == 'GET' || request.getMethod() == 'POST') {
		var form = nlapiCreateForm('Create Vendor Bill');

		var fld_vendor = form.addField('custpage_vendor', 'select', 'Vendor');

		var fld_Rate_Type = form.addField('custpage_rate_type', 'select', 'Rate Type', 'customlist_btpl_subtier_rate_type');

		var fld_start_date = form.addField('custpage_startdate', 'date', 'Start Date').setMandatory(true);

		var fld_end_date = form.addField('custpage_enddate', 'date', 'End Date').setMandatory(true);

		var columns = [];
		columns[0] = new nlobjSearchColumn('custrecord_stvd_vendor', null, 'group').setSort();
		columns[1] = new nlobjSearchColumn('custrecord_stvd_contractor', null, 'group');

		var vendorSearchResults = searchRecord('customrecord_subtier_vendor_data', null, [new nlobjSearchFilter('custrecord_stvd_subsidiary', null, 'anyof', 3)], columns);

		//var vendorSearchResults = (vendorNewSearch.runSearch()).getResults(0, 1000);

		// Add vendor data
		//var fld_vendor_data	=	form.addField('custpage_vendor_data', 'longtext', 'Text').setDisplayType('hidden');

		o_vendor_data = new Array();

		fld_vendor.addSelectOption('-1', 'All Vendors');

		for (var i = 0; i < vendorSearchResults.length; i++) {
			var vendorId = vendorSearchResults[i].getValue(columns[0]);
			var vendorName = vendorSearchResults[i].getText(columns[0]);
			var contractor_id = vendorSearchResults[i].getValue(columns[1]);
			var contractor_name = vendorSearchResults[i].getText(columns[1]);
			o_vendor_data[i] = { 'vendor_id': vendorId, 'vendor_name': vendorName, 'employee_id': contractor_id, 'employee_name': contractor_name };

			if ((i > 0 && o_vendor_data[i].vendor_id != o_vendor_data[i - 1].vendor_id) || i == 0) {
				fld_vendor.addSelectOption(vendorId, vendorName);
			}
		}

		// Add Create Vendor Bill Button
		form.addSubmitButton('Load Employees');

		if (request.getMethod() == 'POST' || request.getParameter('custpage_vendor') != null) {
			var i_vendor = request.getParameter('custpage_vendor');

			var i_rate_type = request.getParameter('custpage_rate_type');

			var s_start_date = request.getParameter('custpage_startdate');

			var s_end_date = request.getParameter('custpage_enddate');

			var d_start_date = nlapiStringToDate(s_start_date, 'date');

			var d_end_date = nlapiStringToDate(s_end_date, 'date');

			var back_url = nlapiResolveURL('SUITELET', 'customscript_sut_btpl_subtier_screen', 'customdeploy1');

			var s_parameters = '&custpage_vendor=' + i_vendor + '&custpage_startdate=' + s_start_date + '&custpage_enddate=' + s_end_date + '&custpage_rate_type=' + i_rate_type + '&mode=excel';

			var mode = request.getParameter('mode');

			var export_to_excel_link = form.addField('export_to_excel_link', 'inlinehtml', 'Back Link').setDefaultValue('<a style="font-weight:bold;" target="_blank" href="' + back_url + s_parameters + '">Export to Excel</a>');

			fld_vendor.setDefaultValue(i_vendor);
			fld_Rate_Type.setDefaultValue(i_rate_type);
			fld_start_date.setDefaultValue(s_start_date);
			fld_end_date.setDefaultValue(s_end_date);

			var filters = [];
			if (i_vendor != -1) {
				filters[0] = new nlobjSearchFilter('custrecord_stvd_vendor', null, 'anyof', i_vendor);
			}
			filters[filters.length] = new nlobjSearchFilter('isinactive', null, 'is', 'F');
			filters[filters.length] = new nlobjSearchFilter('custrecord_stvd_subsidiary', null, 'anyof', 3);
			filters[filters.length] = new nlobjSearchFilter('custrecord_stvd_rate_type', null, 'anyof', i_rate_type);

			var columns = [];
			columns[0] = new nlobjSearchColumn('custrecord_stvd_contractor');
			columns[1] = new nlobjSearchColumn('custrecord_stvd_start_date');
			columns[2] = new nlobjSearchColumn('custrecord_stvd_end_date');
			columns[3] = new nlobjSearchColumn('custrecord_stvd_st_pay_rate');
			columns[4] = new nlobjSearchColumn('department', 'custrecord_stvd_contractor');

			var a_vendor_search_results = searchRecord('customrecord_subtier_vendor_data', null, filters, columns);

			var a_contractor_list = new Array();

			for (var i = 0; i < a_vendor_search_results.length; i++) {
				a_contractor_list.push(a_vendor_search_results[i].getValue(columns[0]));
			}

			var a_resource_allocation_data = getResourceAllocations(a_contractor_list, d_start_date, d_end_date);

			var sublistField = form.addSubList('contractors', 'list', 'Contractors');

			//var sublistForm	=	nlapiCreateList('Contractors');

			sublistField.addField('contractor', 'text', 'Contractor');
			sublistField.addField('start_date', 'date', 'Work Order Start Date');
			sublistField.addField('end_date', 'date', 'Work Order End Date');
			sublistField.addField('project_description', 'text', 'Project Description');
			sublistField.addField('allocation_start_date', 'date', 'Allocation Start Date');
			sublistField.addField('allocation_end_date', 'date', 'Allocation End Date');
			sublistField.addField('st_pay_rate', 'text', 'Pay Rate');

			sublistField.addField('url', 'url', '').setLinkText('View');

			var linkUrl = nlapiResolveURL('SUITELET', 'customscript_sut_subtier_btpl', 'customdeploy1');

			var i_line_item_index = 1;

			var a_data = new Array();

			for (var i = 0; i < a_vendor_search_results.length; i++) {
				var i_contractor = a_vendor_search_results[i].getValue('custrecord_stvd_contractor');
				var s_contractor = a_vendor_search_results[i].getText('custrecord_stvd_contractor');
				var s_wo_start_date = a_vendor_search_results[i].getValue('custrecord_stvd_start_date');
				var s_wo_end_date = a_vendor_search_results[i].getValue('custrecord_stvd_end_date');
				var f_pay_rate = a_vendor_search_results[i].getValue('custrecord_stvd_st_pay_rate');
				var s_department = a_vendor_search_results[i].getText('department', 'custrecord_stvd_contractor');

				if (a_resource_allocation_data[i_contractor] != undefined) {
					for (var j = 0; j < a_resource_allocation_data[i_contractor].length; j++) {
						var s_project_description = a_resource_allocation_data[i_contractor][j].project_description;
						var s_allocation_start_date = nlapiDateToString(a_resource_allocation_data[i_contractor][j].start_date, 'date');
						var s_allocation_end_date = nlapiDateToString(a_resource_allocation_data[i_contractor][j].end_date, 'date');
						var s_customer_vertical = a_resource_allocation_data[i_contractor][j].vertical;
						var s_customer_name = a_resource_allocation_data[i_contractor][j].customer;

						a_data.push
							(
								{
									'contractor': s_contractor,
									'practice': s_department,
									'vertical': s_customer_vertical,
									'customer': s_customer_name,
									'start_date': s_wo_start_date,
									'end_date': s_wo_end_date,
									'project_description': s_project_description,
									'allocation_start_date': s_allocation_start_date,
									'allocation_end_date': s_allocation_end_date,
									'st_pay_rate': f_pay_rate,
									'url': linkUrl + '&custpage_vendor=' + i_vendor + '&custpage_contractor=' + i_contractor + '&custpage_start_date=' + s_start_date + '&custpage_end_date=' + s_end_date + '&custpage_rate_type=' + i_rate_type
								}
							);
							
						i_line_item_index++;
					}
				}
				else {
					var s_project_description = '';

					a_data.push(
						{
							'contractor': s_contractor,
							'practice': s_department,
							'vertical': '',
							'customer': '',
							'start_date': s_wo_start_date,
							'end_date': s_wo_end_date,
							'project_description': s_project_description,
							'allocation_start_date': '',
							'allocation_end_date': '',
							'st_pay_rate': f_pay_rate,
							'url': linkUrl + '&custpage_vendor=' + i_vendor + '&custpage_contractor=' + i_contractor + '&custpage_start_date=' + s_start_date + '&custpage_end_date=' + s_end_date + '&custpage_rate_type=' + i_rate_type
						}
					);
					

					i_line_item_index++;
				}
			}

			sublistField.setLineItemValues(a_data);

			if (mode == 'excel') {
				exportToExcel(a_data);
				return;// response.write('Test')
			}

			//response.writePage(sublistForm);
			//response.sendRedirect('SUITELET', 'SUITELET', '', editmode, parameters)
		}

		response.writePage(form);
	}

	//fld_vendor_data.setDefaultValue(JSON.stringify(o_vendor_data));
}

function getResourceAllocations(a_contractor_list, d_start_date, d_end_date) {
	var filters = new Array();
	filters[0] = new nlobjSearchFilter('resource', null, 'anyof', a_contractor_list);
	filters[1] = new nlobjSearchFilter('startdate', null, 'onorbefore', d_end_date);
	filters[2] = new nlobjSearchFilter('enddate', null, 'onorafter', d_start_date);

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('resource');
	columns[1] = new nlobjSearchColumn('entityid', 'job');
	columns[2] = new nlobjSearchColumn('jobname', 'job');
	columns[3] = new nlobjSearchColumn('startdate');
	columns[4] = new nlobjSearchColumn('enddate');
	columns[5] = new nlobjSearchColumn('custentity_vertical', 'job');
	columns[6] = new nlobjSearchColumn('customer', 'job');

	var resource_allocations = searchRecord('resourceallocation', null, filters, columns);

	var a_resource_allocation_data = new Object();

	for (var i = 0; resource_allocations != null && i < resource_allocations.length; i++) {
		var s_contractor = resource_allocations[i].getValue('resource');
		var s_project_id = resource_allocations[i].getValue('entityid', 'job');
		var s_project_name = resource_allocations[i].getValue('jobname', 'job');
		var d_start_date = nlapiStringToDate(resource_allocations[i].getValue('startdate'), 'date');
		var d_end_date = nlapiStringToDate(resource_allocations[i].getValue('enddate'), 'date');
		var s_vertical = resource_allocations[i].getText('custentity_vertical', 'job');
		var s_customer_name = resource_allocations[i].getText('customer', 'job');

		var o_resource_allocation_data = { 'project_description': getProjectDescription(s_project_id, s_project_name), 'start_date': d_start_date, 'end_date': d_end_date, 'vertical': s_vertical, 'customer': s_customer_name };
		
		if (a_resource_allocation_data[s_contractor] == undefined) {
			a_resource_allocation_data[s_contractor] = [o_resource_allocation_data];
		}
		else {
			a_resource_allocation_data[s_contractor].push(o_resource_allocation_data);
		}
	}

	return a_resource_allocation_data;
}

//Get Project Description text
function getProjectDescription(s_project_id, s_project_name) {
	return s_project_id + ' ' + s_project_name;
}

//Export To Excel
function exportToExcel(a_data) {

	var count = a_data.length;
	var globalArray = new Array();
	var o_columns = new Object();
	o_columns['contractor'] = 'Contractor';
	o_columns['practice'] = 'Practice';
	o_columns['vertical'] = 'Customer Vertical';
	o_columns['customer'] = 'Customer';
	o_columns['start_date'] = 'Work Order Start Date';
	o_columns['end_date'] = 'Work Order End Date';
	o_columns['project_description'] = 'Project Description';
	o_columns['allocation_start_date'] = 'Allocation Start Date';
	o_columns['allocation_end_date'] = 'Allocation End Date';
	o_columns['st_pay_rate'] = 'Pay Rate';

	// Enter the headings
	var s_line = '';
	for (var s_column in o_columns) {
		s_line += o_columns[s_column] + ',';
	}
	globalArray.push(s_line);
	for (var i = 0; i < count; i++) {
		s_line = '\n';
		var a_line = new Array();
		for (var s_column in o_columns) {
			var s_value = a_data[i][s_column].toString();
			s_value = s_value.replace(/[|]/g, " ");
			s_value = s_value.replace(/[,]/g, " ");
			a_line.push(s_value);// o_sublist.getLineItemValue(s_column, i));
			// // [s_column]
			// + ',';
		}
		s_line += a_line.toString();
		globalArray.push(s_line);
	}

	var fileName = 'Contractor Report'
	var Datetime = new Date();
	var CSVName = fileName + " - " + Datetime + '.csv';
	var file = nlapiCreateFile(CSVName, 'CSV', globalArray.toString());

	// nlapiLogExecution('DEBUG', 'suitlet_Po_ClosedStatus', ' PDF Name--> ');
	response.setContentType('CSV', CSVName);

	// nlapiLogExecution('DEBUG', 'suitlet_Po_ClosedStatus', ' PDF Name--> ');
	response.write(file.getValue());

	for (var s_column in o_columns) {

	}
}

//custcol_project_entity_id,custcol_employee_entity_id