/**
 *
 * Script Name: SCH | Resource Allocation Transfer
 * Author:Praveena Madem
 * Company:INSPIRRIA CLOUDTECH
 * Date: 11 - Jan - 2020
 * Description:1. This script will check the how many T&M forecase records will delete and Recreate the T & M forecast records.
                2. Call shecudle script if data is more to delete and re create
        	
                   
 * Script Modification Log:
 *  -- Date --			-- Modified By --				--Requested By--				-- Description --
*/

function _bulk_resource_allocation_transfer() {
    try {
        var context = nlapiGetContext();
        //FETCH the Script parameters like transfer allocation details , project name, billing type
        var a_get_transfer_allocation_details = context.getSetting('SCRIPT', 'custscript_transfer_allocation_details');
        var s_billing_type = context.getSetting('SCRIPT', 'custscript_billing_type');
        var i_project_id = context.getSetting('SCRIPT', 'custscript_project_');
        var i_from_project_id = context.getSetting('SCRIPT', 'custscript_from_project_');
        var s_user_email = context.getSetting('SCRIPT', 'custscript_useremail_');
        a_get_transfer_allocation_details = JSON.parse(a_get_transfer_allocation_details);
        var failedAllocations = [];
        var createdAllocations = [];
        var serviceItem = null;
        if (s_billing_type == 'TM') {
            serviceItem = 'ST';
        } else {
            serviceItem = 'FP';
        }
        var s_to_project_name = nlapiLookupField('job', i_project_id, ["altname"]);
        var s_from_prpject = nlapiLookupField('job', i_from_project_id, ["altname"]);


        s_to_project_name = s_to_project_name["altname"];
        s_from_prpject = s_from_prpject["altname"];
        nlapiLogExecution('DEBUG', "s_to_project_name", s_to_project_name);
        nlapiLogExecution('DEBUG', "s_from_prpject", s_from_prpject);
        s_to_project_name = s_to_project_name ? s_to_project_name.split(' : ') : "";
        s_to_project_name = s_to_project_name ? s_to_project_name[s_to_project_name.length - 1] : "";

        s_from_prpject = s_from_prpject ? s_from_prpject.split(' : ') : "";
        s_from_prpject = s_from_prpject ? s_from_prpject[s_from_prpject.length - 1] : "";

        nlapiLogExecution('DEBUG', "s_to_project_name", s_to_project_name);
        nlapiLogExecution('DEBUG', "s_from_prpject", s_from_prpject);

        var strVar = "";
        strVar += "<html>";
        strVar += "<title>Allocation Transfer - Result : Form Project - : " + s_from_prpject + " :: To Project -:" + s_to_project_name["altname"] + " <\/title>";
        strVar += "<table style=\"border-collapse: collapse;table-layout: fixed;width: 100%\">";
        strVar += "<thead>";
        strVar += "<tr>";
        strVar += "<td>Allocations<\/td>";
        //strVar += "<td>&nbsp;<\/td>";
        strVar += "<\/tr>";
        strVar += "<\/thead>";
        var strVar1 = "";
        strVar1 += "<html>";
        strVar1 += "<title>Allocations Failed<\/title>";
        strVar1 += "<table style=\"border-collapse: collapse;table-layout: fixed;width: 100%\">";
        strVar1 += "<thead>";
        strVar1 += "<tr>";
        strVar1 += "<td><span>Resource<\/span><\/td>";
        strVar1 += "<td><span>Error<\/span><\/td>";
        strVar1 += "<\/tr>";
        strVar1 += "<\/thead>";

        for (var linenum = 0; linenum <= a_get_transfer_allocation_details.length - 1; linenum++) {
            if (context.getRemainingUsage() < 300) {
                nlapiYieldScript(); //Added Yeild API
            }
            var resource = a_get_transfer_allocation_details[linenum]["resource"];
            var doCreate = a_get_transfer_allocation_details[linenum]["doCreate"];
            if (isTrue(doCreate)) {
                try {
                    var allocationRecord = nlapiCreateRecord('resourceallocation');
                    allocationRecord.setFieldValue('allocationresource', resource);
                    var s_resource = allocationRecord.getFieldText('allocationresource');
                    allocationRecord.setFieldValue('project', i_project_id);
                    allocationRecord.setFieldValue('startdate', a_get_transfer_allocation_details[linenum]["newstartdate"]);
                    allocationRecord.setFieldValue('enddate', a_get_transfer_allocation_details[linenum]["newenddate"]);

                    var isBillable = a_get_transfer_allocation_details[linenum]["isBillable"];
                    allocationRecord.setFieldValue('custeventrbillable', isBillable);
                    if (isTrue(isBillable)) {
                        allocationRecord.setFieldValue('custeventbstartdate', a_get_transfer_allocation_details[linenum]["newstartdate"]);
                        allocationRecord.setFieldValue('custeventbenddate', a_get_transfer_allocation_details[linenum]["newenddate"]);
                        allocationRecord.setFieldValue('custevent_allocation_status', '1');
                    } else {
                        allocationRecord.setFieldValue('custevent_allocation_status', '2');
                    }

                    if (a_get_transfer_allocation_details[linenum]["newbillrate"]) {
                        allocationRecord.setFieldValue('custevent3', a_get_transfer_allocation_details[linenum]["newbillrate"]);
                    } else {
                        allocationRecord.setFieldValue('custevent3', 0);
                    }

                    if (a_get_transfer_allocation_details[linenum]["newotrate"]) {
                        allocationRecord.setFieldValue('custevent_otrate', a_get_transfer_allocation_details[linenum]["newotrate"]);
                    }

                    // unit cost
                    allocationRecord.setFieldValue('custevent2', '0.0');

                    allocationRecord.setFieldValue('custevent4', a_get_transfer_allocation_details[linenum]["newsite"]);

                    allocationRecord.setFieldValue('custevent1', serviceItem);

                    allocationRecord.setFieldValue('custeventwlocation', a_get_transfer_allocation_details[linenum]["newlocation"]);
                    allocationRecord.setFieldValue('allocationamount', (a_get_transfer_allocation_details[linenum]["newpercent"]).split('%')[0].trim());

                    allocationRecord.setFieldValue('allocationunit', 'P');
                    //118115-Joshy C D,Blue Planet – Enterprise – T & M, 02/18/2020 - 03/31/2020
                    // var s_resource_allcoation_name = s_resource + ", " + s_to_project_name["altname"] + ", " + allocationRecord.getFieldValue('allocationamount') + " Percentage, " + a_get_transfer_allocation_details[linenum]["newstartdate"] + " - " + a_get_transfer_allocation_details[linenum]["newenddate"];
                    var s_resource_allcoation_name = s_resource + ", " + s_to_project_name + ", " + a_get_transfer_allocation_details[linenum]["newstartdate"] + " - " + a_get_transfer_allocation_details[linenum]["newenddate"];

                    var id = nlapiSubmitRecord(allocationRecord, true, true);
                    nlapiLogExecution('debug', 'Allocation Created', id);
                    createdAllocations.push({ id: id });
                    if (id) {
                        strVar += "<tr>";
                        strVar += "<td style=\"align:center;font-size: 9px;\">" + s_resource_allcoation_name + "<\/td>";
                        //strVar += "<td>&nbsp;<\/td>";
                        strVar += "<\/tr>";
                    }
                } catch (e) {
                    nlapiLogExecution('ERROR', 'Allocation Creation', e);
                    failedAllocations.push({ 'resource': resource, 'error': e });
                    strVar1 += "<tr>";
                    strVar1 += "<td style=\"align:center;font-size: 9px;\">" + s_resource + "<\/td>";
                    strVar1 += "<td style=\"align:center;font-size: 9px;\">" + e + "<\/td>";
                    strVar1 += "<\/tr>";
                }
            }
        }

        strVar += "<\/thead>";
        strVar += "<\/tbody>";
        strVar += "<\/table>";
        strVar1 += "<\/thead>";
        strVar1 += "<\/tbody>";
        strVar1 += "<\/table>";
        strVar1 += "<\/html>";


        var merge_table = "";
        merge_table += "<tbody>";
        merge_table += "<tr>";
        merge_table += "<td align=center><b><span>ALLOCATIONS CREATED<\/span><\/b><\/td>";
        merge_table += "<td>&nbsp;<\/td>";
        merge_table += "<\/tr>";
        merge_table += "<tr>";
        merge_table += "<td>" + strVar + "<\/td>";
        merge_table += "<td>&nbsp;<\/td>";
        merge_table += "<\/tr>";
        merge_table += "<tr>";
        merge_table += "<td align=center><b><span>ALLOCATIONS FAILED<\/span><\/b><\/td>";
        merge_table += "<td>&nbsp;<\/td>";
        merge_table += "<\/tr>";
        merge_table += "<tr>";
        merge_table += "<td>" + strVar1 + "<\/td>";
        merge_table += "<td>&nbsp;<\/td>";
        merge_table += "<\/tr>";
        merge_table += "<\/tbody>";
        merge_table += "<\/table>";

        //nlapiSendEmail(constant.Mail_Author.InformationSystem,s_user_email,"Allocation Transfer - Result : Form Project - : "+s_from_prpject["altname"]+" :: To Project -:"+s_to_project_name["altname"] , merge_table);
        nlapiSendEmail(constant.Mail_Author.InformationSystem, s_user_email, "Allocation Transfer - Result : Form Project(" + s_from_prpject + ") :: To Project(" + s_to_project_name + ")", merge_table);

    } catch (e) {
        nlapiLogExecution('DEBUG', 'errorin sch script', e);
        Send_Exception_Mail(e);
    }
}
    function Send_Exception_Mail(e,s_user_email)
    {​​​​​
    try
    {​​​​​   
        var s_user_email = context.getSetting('SCRIPT', 'custscript_useremail_');
        var context = nlapiGetContext();
        var s_DeploymentID =context.getDeploymentId();
        var s_ScriptID =context.getScriptId();
        var s_Subject ='Issue on Allocation Transfer';
        
        var s_Body = 'This is to inform that Allocation Transferis having an issue and System is not able to send the email notification to employees.';
        s_Body += '<br/>Issue: Code: '+e.code+' Message: '+e.message;
        s_Body += '<br/>Script: '+s_ScriptID;
        s_Body += '<br/>Deployment: '+s_DeploymentID;
        s_Body += '<br/>Please have a look and do the needfull.';
        s_Body += '<br/><br/>Note: This is system genarated email incase of any failure while sending the notification to employees';

        s_Body += '<br/><br/>Regards,';
        s_Body += '<br/>Digital Office';
        nlapiSendEmail(442,s_user_email,s_Subject,s_Body,null, null, null, null);
    }​​​​​
    catch(err)
    {​​​​​
        nlapiLogExecution('error',' Send_Exception_Mail', e.message); 
		
    }​​​​​
}​​​​​
