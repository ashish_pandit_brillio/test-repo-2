/**
 *@NApiVersion 2.x
 *@NScriptType MapReduceScript
 */
define(
    [
        'N/file', 'N/sftp', 'N/search', 'N/url', 'N/runtime', 'N/config', 'N/record','N/email','N/format','SuiteScripts/moment','SuiteScripts/Underscore','SuiteScripts/underscore-min'
    ],

    /**
    *
    * @param {file} file
    */
    /**
    * 
    *
    * @param {search} search
    */

    function (file, sftp, search, url, runtime, configmod,record,email,format,moment,Underscore,underscore) {

        /**
        * Marks the beginning of the Map/Reduce process and generates input data.
        *
        * @typedef {Object} ObjectRef
        * @property {number} id - Internal ID of the record instance
        * @property {string} type - Record type id
        *
        * @return {Array|Object|Search|RecordRef} inputSummary
        * @since 2015.1
        */

        function getInputData() 
        {
            var date = new Date();

            var dateToKeep = date.setDate(date.getDate() - 6);
            log.debug('dateToKeep', dateToKeep);

            var getDateToKeepFinal = formatDate(dateToKeep);
            log.debug('getDateToKeepFinal', getDateToKeepFinal);

            var dateToKeepCheck = new Date(dateToKeep);

            var getFinalDate = dateToKeepCheck.setDate(dateToKeepCheck.getDate() - 98);
            log.debug('getFinalDate', getFinalDate);

            getFinalDate = formatDate(getFinalDate);
            log.debug('getFinalDate', getFinalDate);

            // Resource Allocation Search

            var filters = [];
                    
                    filters.push(["custeventbstartdate","notafter",getDateToKeepFinal]);
                    filters.push("And");
                    filters.push(["custeventbenddate","notbefore",getFinalDate]);
                    filters.push("And");
                    filters.push(["custeventrbillable","is","T"]);
                    filters.push("And");
                    filters.push(["job.jobbillingtype","anyof","TM"]);
                    filters.push("And");
                    filters.push(["resource","anyof","58590","47449","140520","140527"]);

                    var columns = [];

                       var resourceallocationSearchObj = search.create({
                       type: "resourceallocation",
                       filters:filters,
                       columns:
                       [
                          search.createColumn({
                             name: "custeventbstartdate",
                             summary: "GROUP",
                             label: "Billing strat Date"
                          }),
                          search.createColumn({
                             name: "custeventbenddate",
                             summary: "GROUP",
                             label: "Billing End Date"
                          }),
                          search.createColumn({
                             name: "resource",
                             summary: "GROUP",
                             label: "Resource"
                          }),
                          search.createColumn({
                             name: "custevent4",
                             summary: "GROUP",
                             label: "Onsite/Offsite"
                          }),
                          search.createColumn({
                             name: "custevent3",
                             summary: "GROUP",
                             label: "Bill Rate"
                          }),
                          search.createColumn({
                             name: "custevent_practice",
                             summary: "GROUP",
                             label: "Practice"
                          }),
                          search.createColumn({
                             name: "project",
                             summary: "GROUP",
                             label: "Project"
                          }),
                          search.createColumn({
                             name: "custentity_hoursperday",
                             join: "job",
                             summary: "GROUP",
                             label: "Hours Per Day"
                          }),
                          search.createColumn({
                             name: "custentity_onsite_hours_per_day",
                             join: "job",
                             summary: "GROUP",
                             label: "Onsite/Offsite Hours Per Day"
                          }),
                          search.createColumn({
                             name: "percentoftime",
                             summary: "AVG",
                             label: "Percentage of Time"
                          }),
                          search.createColumn({
                             name: "custentityproject_holiday",
                             join: "job",
                             summary: "GROUP",
                             label: "Project Holiday"
                          }),
                          search.createColumn({
                             name: "subsidiary",
                             join: "employee",
                             summary: "GROUP",
                             label: "Subsidiary"
                          }),
                          search.createColumn({
                             name: "customer",
                             join: "job",
                             summary: "GROUP",
                             label: "Customer"
                          }),
                          search.createColumn({
                             name: "timeapprover",
                             join: "employee",
                             summary: "GROUP",
                             label: "Time Approver"
                          }),
                          search.createColumn({
                             name: "currency",
                             join: "job",
                             summary: "GROUP",
                             label: "Currency"
                          }),
                          search.createColumn({
                             name: "department",
                             join: "employee",
                             summary: "GROUP",
                             label: "Practice"
                          })
                       ]
                       });

                return resourceallocationSearchObj;
        }

        function map(context)
        {
            try
            {
                log.debug('context', context.value);

                //Parse the OBJECT
                var o_values = JSON.parse(context.value);

                // Fetch the Values
                var allocationStartDate = o_values.values["GROUP(custeventbstartdate)"];
                log.debug('allocationStartDate', allocationStartDate);

                var allocationEndDate = o_values.values["GROUP(custeventbenddate)"];
                log.debug('allocationEndDate', allocationEndDate);

                var s_employee_name = o_values.values["GROUP(resource)"].value;
                log.debug('s_employee_name', s_employee_name);

                var bill_rate = o_values.values["GROUP(custevent3)"];
                log.debug('bill_rate', bill_rate);

                var s_employeeName = o_values.values["GROUP(resource)"].text;
                log.debug('s_employeeName', s_employeeName);

                var onsite_offsite = o_values.values["GROUP(custevent4)"].value;
                log.debug('onsite_offsite', onsite_offsite);

                var offsiteHrs = o_values.values["GROUP(custentity_hoursperday.job)"];
                log.debug('offsiteHrs', offsiteHrs);

                var onsiteHrs = o_values.values["GROUP(custentity_onsite_hours_per_day.job)"];
                log.debug('onsiteHrs', onsiteHrs);

                var percentTime = o_values.values["AVG(percentoftime)"];
                log.debug('percentTime', percentTime);

                var projectHoliday = o_values.values["GROUP(custentityproject_holiday.job)"].value;
                log.debug('projectHoliday', projectHoliday);

                var customerText = o_values.values["GROUP(customer.job)"].text;
                log.debug('customerText', customerText);

                var customer = o_values.values["GROUP(customer.job)"].value;
                log.debug('customer', customer);

                var project = o_values.values["GROUP(project)"].text;
                log.debug('project', project);

                var currency = o_values.values["GROUP(currency.job)"].value;
                log.debug('currency', currency);

                var empSubsidiary = o_values.values["GROUP(subsidiary.employee)"].value;
                log.debug('empSubsidiary', empSubsidiary);

                var timeApprover = o_values.values["GROUP(timeapprover.employee)"].text;
                log.debug('timeApprover', timeApprover);

                var empPractice = o_values.values["GROUP(department.employee)"].value;
                log.debug('empPractice', empPractice);

                // Holidays Count
                var date = new Date();

                var dateToKeep = date.setDate(date.getDate() - 6);
                log.debug('dateToKeep', dateToKeep);

                var getDateToKeepFinal = formatDate(dateToKeep);
                log.debug('getDateToKeepFinal', getDateToKeepFinal);

                var dateToKeepCheck = new Date(dateToKeep);

                var getFinalDate = dateToKeepCheck.setDate(dateToKeepCheck.getDate() - 98);
                log.debug('getFinalDate', getFinalDate);

                getFinalDate = formatDate(getFinalDate);
                log.debug('getFinalDate', getFinalDate);

                var customerHoliday = CustomerHolidayss(getFinalDate,getDateToKeepFinal);
                var companyHolidays = HolidayCompany(getFinalDate,getDateToKeepFinal);

                //Resouce Allocation Array Creation for resources

                var a_data = new Object();
                var employeeList = new Array();  //List to store all employee's who have the allocation
                var practiceHeadList = new Array();
                var employeeNameWise = new Object();
                var employeeWeekHrs = new Object();


                var i_perday_hours = "";

                if (onsite_offsite == "1")//Onsite
                {
                   i_perday_hours = onsiteHrs;
                }

                if (onsite_offsite == "2")//Offsite
                {
                    i_perday_hours = offsiteHrs;
                }
                i_perday_hours = i_perday_hours ? i_perday_hours : 8;

                if(i_perday_hours == '- None -'){
                    i_perday_hours = 8;

                }

                if(!_logValidation(employeeNameWise[s_employee_name])){
                    employeeNameWise[s_employee_name] = s_employeeName
                }

                var d_allocationStartDate = allocationStartDate;
                var d_allocationEndDate = allocationEndDate;

                if (_logValidation(d_allocationStartDate) && _logValidation(d_allocationStartDate)) {

                                    var startDate = new Date(d_allocationStartDate).getTime() > new Date(getFinalDate).getTime() ? d_allocationStartDate :
                                        getFinalDate;
                                    var endDate = new Date(d_allocationEndDate).getTime() < new Date(getDateToKeepFinal).getTime() ? d_allocationEndDate :
                                        getDateToKeepFinal;
                                    var actualAllocationStart = startDate;
                                    var actualAllocationEnd = endDate;

                                    log.debug("s_employee_name",s_employee_name);
                                    log.debug("allocation startDate",startDate);
                                    log.debug("allocation endDate",endDate);
                                    log.debug("actualAllocationStart",actualAllocationStart);
                                    log.debug("actualAllocationEnd",actualAllocationEnd);

                                    if(new Date(endDate).getDay()==0){

                                        var endDate_prev = new Date(endDate);
                                        var endDate= formatDate(new Date(endDate_prev).setDate(new Date(endDate_prev).getDate()-1));
                                        log.debug("Log", 'endDate :' + endDate);
                                        
                                    }

                                    end_day = new Date(endDate).getDay();
                                    var sunday_of_end_week= formatDate(new Date(endDate).setDate(new Date(endDate).getDate()-end_day));
                                    log.debug("Log", 'sunday_of_end_week :' + sunday_of_end_week);

                                    endDate = formatDate(new Date(sunday_of_end_week).setDate(new Date(sunday_of_end_week).getDate()+6));
                                    log.debug("Log", 'endDate :' + endDate);

                                    end_day1 = new Date(startDate).getDay();
                                    startDate= formatDate(new Date(startDate).setDate(new Date(startDate).getDate()-end_day1));
                                    log.debug("Log", 'startDate :' + startDate);

                                    if(_logValidation(employeeWeekHrs[s_employee_name]))
                                    {}

                                    var weekStartDateList = [];
                                    if (a_data[s_employee_name]) 
                                    {
                                        log.debug("Log", 'If loop :' + 'If loop');

                                        var weekList = a_data[s_employee_name];
                                        for (var i = 0;; i += 7) 
                                        {
                                            d_current_date= formatDate(new Date(startDate).setDate(new Date(startDate).getDate()+i));
                                            log.debug("Log", 'd_current_date :' + d_current_date);

                                            current_end_date= formatDate(new Date(startDate).setDate(new Date(startDate).getDate()+(i+6)));
                                            log.debug("Log", 'current_end_date :' + current_end_date);

                                            if (new Date(d_current_date).getTime() > new Date(endDate).getTime() ) { //|| d_current_date < startDate
                                                log.debug("Log", 'Break loop :' + 'Break loop');
                                                break;
                                            }

                                            var startDateCurrent = d_current_date;
                                            log.debug("Log", 'startDateCurrent:' + startDateCurrent);

                                            /////       -----      Logic for taking hours from allocation week wise ---------------///

                                             var startDateHrs = new Date(actualAllocationStart).getTime() > new Date(d_current_date).getTime() ? actualAllocationStart :
                                             d_current_date;
                                             log.debug("Log", 'startDateHrs :' + startDateHrs);

                                             var endDateHrs = new Date(actualAllocationEnd).getTime() > new Date(current_end_date).getTime() ? current_end_date :
                                             actualAllocationEnd;
                                             log.debug("Log", 'endDateHrs :' + endDateHrs);

                                             startDateHrs = new Date(startDateHrs);
                                             var startDateHrs = format.format({
                                                value: startDateHrs,
                                                type: format.Type.DATE
                                            });

                                            //startDateHrs = moment(startDateHrs).utcOffset("+05:30").format("MM/DD/YYYY");
                                            //startDateHrs = formatDate(new Date(startDateHrs));

                                            endDateHrs = new Date(endDateHrs);
                                            var endDateHrs = format.format({
                                                value: endDateHrs,
                                                type: format.Type.DATE
                                            });

                                            //endDateHrs = moment(endDateHrs).utcOffset("+05:30").format("MM/DD/YYYY");
                                            //endDateHrs = formatDate(new Date(endDateHrs));

                                            log.debug("Log", 'startDateHrs + endDateHrs :' + startDateHrs + ', '+endDateHrs);

                                            var i_working_days = calcBusinessDays(startDateHrs, endDateHrs);
                                            log.debug("Log", 'i_working_days :' + i_working_days);
                                            var holidaysCount = 0;
                                            if(projectHoliday == 1){

                                                 holidaysCount =  companyHolidayCount(startDateHrs,endDateHrs,companyHolidays,empSubsidiary);
                                                 log.debug("Log", 'holidaysCount :' + holidaysCount);

                                             } else{

                                                 holidaysCount = CustomerHolidaysCount(startDateHrs,endDateHrs,customerHoliday,customer,empSubsidiary,s_employee_name);
                                                 log.debug("Log", 'holidaysCount :' + holidaysCount);

                                             }

                                             var hoursNotSub = parseFloat(((parseFloat(i_working_days)) - parseFloat(holidaysCount)) * parseInt(i_perday_hours) * parseFloat(percentTime) / 100.0)
                                            log.debug("Log", 'hoursNotSub :' + hoursNotSub);

                                            if(currency!=1)
                                            {
                                                var startMonth = new Date(startDateCurrent).getMonth();
                                                log.debug("Log", 'startMonth :' + startMonth);

                                                var  months = ["January", "February", "March", "April", "May", "June","July", "August", "September", "October", "November", "December"];
                                                var monthName=months[startMonth];
                                                log.debug("Log", 'monthName :' + monthName);

                                                var startYear = new Date(startDateCurrent).getFullYear();
                                                log.debug("Log", 'startYear :' + startYear);

                                                var  years = [];
                                                years[2014] = "1";
                                                years[2015] = "2";
                                                years[2016] = "3";
                                                years[2017] = "4";
                                                years[2018] = "5";
                                                years[2019] = "6";
                                                years[2020] = "7";
                                                years[2021] = "8";
                                                years[2022] = "9";
                                                years[2023] = "10";
                                                var yearName=years[startYear];
                                                log.debug("Log", 'yearName :' + yearName);

                                                var currencyRate = _Currency_exchangeRate(currency,monthName,yearName);
                                                log.debug("Log", 'currencyRate :' + currencyRate);

                                                bill_rate = parseFloat(bill_rate/currencyRate);
                                                log.debug("Log", 'bill_rate :' + bill_rate);
                                                
                                                var hoursNotSubAmt = parseFloat(hoursNotSub*bill_rate);
                                                log.debug("Log", 'hoursNotSubAmt :' + hoursNotSubAmt);
                                            }
                                            else

                                            var hoursNotSubAmt = parseFloat(hoursNotSub*bill_rate);
                                            log.debug("Log", 'hoursNotSubAmt :' + hoursNotSubAmt);


                                             if(!_logValidation(employeeWeekHrs[s_employee_name]))
                                             {

                                                 employeeWeekHrs[s_employee_name] = new Object();
                     
                                                 if(!_logValidation(employeeWeekHrs[s_employee_name][startDateCurrent])){

                                                    log.debug("Log", '1 :' + '1');
                     
                                                     employeeWeekHrs[s_employee_name][startDateCurrent] = new Object();
                                                     employeeWeekHrs[s_employee_name][startDateCurrent] = hoursNotSub +"$#$"+hoursNotSubAmt +"$#$"+customerText+"$#$"+project+"$#$"+timeApprover
                     
                     
                                                 } else{
                                                     var previousHrs = employeeWeekHrs[s_employee_name][startDateCurrent];

                                                     hoursNotSubAmtFinal = parseFloat((hoursNotSub*bill_rate) + (previousHrs*bill_rate));
                                                     employeeWeekHrs[s_employee_name][startDateCurrent] = (hoursNotSub + previousHrs) +"$#$"+hoursNotSubAmtFinal +"$#$"+customerText+"$#$"+project+"$#$"+timeApprover

                                                     log.debug("Log", '2 :' + '2');
                     
                                                 }
                     
                                            }else {
                                                 if(!_logValidation(employeeWeekHrs[s_employee_name][startDateCurrent])){
                                                     employeeWeekHrs[s_employee_name][startDateCurrent] = new Object();
                                                     //employeeWeekHrs[s_employee_name][startDateCurrent] = previousHrs + hoursNotSub;
                                                     employeeWeekHrs[s_employee_name][startDateCurrent] = hoursNotSub +"$#$"+hoursNotSubAmt +"$#$"+customerText+"$#$"+project+"$#$"+timeApprover

                                                     log.debug("Log", '3 :' + '3');
                     
                     
                                                 } else{
                                                     var previousHrs = employeeWeekHrs[s_employee_name][startDateCurrent];

                                                     hoursNotSubAmtFinal = parseFloat((hoursNotSub*bill_rate) + (previousHrs*bill_rate));
                                                     employeeWeekHrs[s_employee_name][startDateCurrent] = (hoursNotSub + previousHrs) +"$#$"+hoursNotSubAmtFinal +"$#$"+customerText+"$#$"+project+"$#$"+timeApprover
                                                     //employeeWeekHrs[s_employee_name][startDateCurrent] = hoursNotSub;

                                                     log.debug("Log", '4 :' + '4');
                     
                                                 }
                     
                                            }

                                            if (weekList.indexOf(startDateCurrent) == -1) {  //&& d_current_date>=request_from_date && d_current_date<=d_end_date
                                       
                                                weekList.push(startDateCurrent); //For timeSheet weeks
           
                                            } //indexof finished
                                        }

                                        a_data[s_employee_name] = weekList;

                                    }else{

                                        log.debug("Log", 'Else loop :' + 'Else loop');

                                        for (var i = 0;; i += 7) 
                                        {
                                            d_current_date= formatDate(new Date(startDate).setDate(new Date(startDate).getDate()+i));
                                            log.debug("Log", 'd_current_date :' + d_current_date);

                                            current_end_date= formatDate(new Date(startDate).setDate(new Date(startDate).getDate()+(i+6)));
                                            log.debug("Log", 'current_end_date :' + current_end_date);

                                            if (new Date(d_current_date).getTime() > new Date(endDate).getTime() ) { //|| d_current_date < startDate
                                                break;
                                            }

                                            var currentDateStr  = d_current_date;
                                            log.debug("Log", 'currentDateStr :' + currentDateStr);
                                            weekStartDateList.push(currentDateStr);

                                            /////       -----      Logic for taking hours from allocation week wise ---------------///

                                             var startDateHrs = new Date(actualAllocationStart).getTime() > new Date(d_current_date).getTime() ? actualAllocationStart :
                                             d_current_date;

                                             var endDateHrs = new Date(actualAllocationEnd).getTime() > new Date(current_end_date).getTime() ? current_end_date :
                                             actualAllocationEnd;

                                             startDateHrs = formatDate(new Date(startDateHrs));

                                             //startDateHrs = moment(startDateHrs).utcOffset("+05:30").format("MM/DD/YYYY");
                                            //startDateHrs = formatDate(new Date(startDateHrs));

                                            endDateHrs = formatDate(new Date(endDateHrs));

                                            //endDateHrs = moment(endDateHrs).utcOffset("+05:30").format("MM/DD/YYYY");
                                            //endDateHrs = formatDate(new Date(endDateHrs));

                                            log.debug("Log", 'startDateHrs + endDateHrs :' + startDateHrs + ', '+endDateHrs);

                                            var i_working_days = calcBusinessDays(startDateHrs, endDateHrs);
                                            log.debug("Log", 'i_working_days :' + i_working_days);

                                            var holidaysCount = 0;

                                            log.debug("Log", 'projectHoliday :' + projectHoliday);

                                            if(projectHoliday == 1){

                                                 holidaysCount =  companyHolidayCount(startDateHrs,endDateHrs,companyHolidays,empSubsidiary);
                                                 log.debug("Log", 'holidaysCount :' + holidaysCount);

                                             } else{

                                                 holidaysCount = CustomerHolidaysCount(startDateHrs,endDateHrs,customerHoliday,customer,empSubsidiary,s_employee_name);
                                                 log.debug("Log", 'holidaysCount :' + holidaysCount);

                                             }

                                             var hoursNotSub = parseFloat(((parseFloat(i_working_days)) - parseFloat(holidaysCount)) * parseInt(i_perday_hours) * parseFloat(percentTime) / 100.0)
                                            log.debug("Log", 'hoursNotSub :' + hoursNotSub);

                                            if(currency!=1)
                                            {
                                                var startMonth = new Date(currentDateStr).getMonth();
                                                log.debug("Log", 'startMonth :' + startMonth);

                                                var  months = ["1", "2", "3", "4", "5", "6","7", "8", "9", "10", "11", "12"];
                                                var monthName=months[startMonth];
                                                log.debug("Log", 'monthName :' + monthName);

                                                var startYear = new Date(currentDateStr).getFullYear();
                                                log.debug("Log", 'startYear :' + startYear);

                                                var  years = [];
                                                years[2014] = "1";
                                                years[2015] = "2";
                                                years[2016] = "3";
                                                years[2017] = "4";
                                                years[2018] = "5";
                                                years[2019] = "6";
                                                years[2020] = "7";
                                                years[2021] = "8";
                                                years[2022] = "9";
                                                years[2023] = "10";
                                                var yearName=years[startYear];
                                                log.debug("Log", 'yearName :' + yearName);


                                                var currencyRate = _Currency_exchangeRate(currency,monthName,yearName);
                                                log.debug("Log", 'currencyRate :' + currencyRate);

                                                bill_rate = parseFloat(bill_rate/currencyRate);
                                                log.debug("Log", 'bill_rate :' + bill_rate);

                                                var hoursNotSubAmt = parseFloat(hoursNotSub*bill_rate);
                                                log.debug("Log", 'hoursNotSubAmt :' + hoursNotSubAmt);
                                            }
                                            else

                                            var hoursNotSubAmt = parseFloat(hoursNotSub*bill_rate);
                                            log.debug("Log", 'hoursNotSubAmt :' + hoursNotSubAmt);


                                             if(!_logValidation(employeeWeekHrs[s_employee_name]))
                                             {

                                                 employeeWeekHrs[s_employee_name] = new Object();
                     
                                                 if(!_logValidation(employeeWeekHrs[s_employee_name][currentDateStr])){

                                                    log.debug("Log", '1 :' + '1');
                     
                                                     employeeWeekHrs[s_employee_name][currentDateStr] = new Object();
                                                     employeeWeekHrs[s_employee_name][currentDateStr] = hoursNotSub +"$#$"+hoursNotSubAmt+"$#$"+customerText+"$#$"+project+"$#$"+timeApprover
                     
                                                 } else{
                                                     var previousHrs = employeeWeekHrs[s_employee_name][currentDateStr];
                                                     //employeeWeekHrs[s_employee_name][currentDateStr] = hoursNotSub + previousHrs;

                                                     hoursNotSubAmtFinal = parseFloat((hoursNotSub*bill_rate) + (previousHrs*bill_rate));
                                                     employeeWeekHrs[s_employee_name][currentDateStr] = (hoursNotSub + previousHrs) +"$#$"+hoursNotSubAmtFinal+"$#$"+customerText+"$#$"+project+"$#$"+timeApprover

                                                     log.debug("Log", '2 :' + '2');
                     
                                                 }
                     
                                            }else {
                                                 if(!_logValidation(employeeWeekHrs[s_employee_name][currentDateStr])){
                                                     employeeWeekHrs[s_employee_name][currentDateStr] = new Object();
                                                     //employeeWeekHrs[s_employee_name][startDateCurrent] = previousHrs + hoursNotSub;
                                                     employeeWeekHrs[s_employee_name][currentDateStr] = hoursNotSub +"$#$"+hoursNotSubAmt+"$#$"+customerText+"$#$"+project+"$#$"+timeApprover

                                                     log.debug("Log", '3 :' + '3');
                     
                                                 } else{
                                                     var previousHrs = employeeWeekHrs[s_employee_name][currentDateStr];
                                                     //employeeWeekHrs[s_employee_name][currentDateStr] = previousHrs + hoursNotSub;

                                                     hoursNotSubAmtFinal = parseFloat((hoursNotSub*bill_rate) + (previousHrs*bill_rate));
                                                     employeeWeekHrs[s_employee_name][currentDateStr] = (hoursNotSub + previousHrs) +"$#$"+hoursNotSubAmtFinal+"$#$"+customerText+"$#$"+project+"$#$"+timeApprover

                                                     log.debug("Log", '4 :' + '4');
                     
                                                 }
                     
                                            }

                                        }
                                    }

                                    if (a_data[s_employee_name] == undefined) {
                                        a_data[s_employee_name] = weekStartDateList;

                                    }

                                    if (employeeList.indexOf(s_employee_name)) {
                                        employeeList.push(s_employee_name);

                                    }

                                }

                                log.debug("Log", 'employeeWeekHrs :' + JSON.stringify(employeeWeekHrs));

                                //employeeList -- list of those employees who haven't submitted their timesheets yet
                            //
                                    var timesheetSearchObj = search.create({
                                       type: "timesheet",
                                       filters:
                                       [
                                          ["employee","anyof",employeeList], 
                                          "AND", 
                                          ["timesheetdate","within",getFinalDate,getDateToKeepFinal],
                                          "AND", 
                                          ["approvalstatus","anyof","3","2"]
                                       ],
                                       columns:
                                       [
                                          search.createColumn({
                                             name: "employee",
                                             summary: "GROUP",
                                             sort: search.Sort.DESC,
                                             label: "Employee"
                                          }),
                                          search.createColumn({name: "startdate", summary: "GROUP",label: "Start Date"}),
                                          search.createColumn({name: "enddate", summary: "GROUP",label: "enddate"})
                                          //search.createColumn({name: "internalid", label: "Internal ID"})
                                       ]
                                    });
                                    var timeSearchResultCount = timesheetSearchObj.runPaged().count;
                                    log.debug("timesheetSearchObj result count",timeSearchResultCount);

                                    var prevEmpID = '';

                                    var empListSubmitted = new Array();
                                    var empListNS = new Object();
                                    var empSubmittedOnce = new Array();

                                    if(timeSearchResultCount>0)
                                    {
                                        var results = [];
                                        var count = 0;
                                        var pageSize = timeSearchResultCount;
                                        var start = 0;
                                        do {
                                        var subresults = timesheetSearchObj.run().getRange({
                                        start: start,
                                        end: start + pageSize
                                        });

                                        results = results.concat(subresults);
                                        count = subresults.length;
                                        start += pageSize;
                                        } while (count == pageSize);

                                        for (var indx = 0; indx < timeSearchResultCount; indx++) 
                                        {
                                            var currEmpID = results[indx].getValue({name: "employee",summary: "GROUP"});
                                            log.debug("currEmpID",currEmpID);

                                            if (indx == 0) {
                                                prevEmpID = currEmpID;

                                            }

                                            if(empSubmittedOnce.indexOf(currEmpID)==-1){
                                                empSubmittedOnce.push(currEmpID);
                                            }

                                            var start = results[indx].getValue({name: "startdate",summary: "GROUP"});
                                            log.debug("start",start);

                                            if (currEmpID != prevEmpID) {
                                                if (a_data[prevEmpID]) 
                                                {
                                                    var RAData = a_data[prevEmpID];
                                                    var empNotSubmitted = inAButNotInB(RAData, empListSubmitted);
                                                    if(empNotSubmitted.length > 0){
                                                        empListNS[prevEmpID] = empNotSubmitted;
                                                    }
                                                }

                                                empListSubmitted = [];
                                                empListSubmitted.push(start);
                                            } else {

                                                empListSubmitted.push(start);

                                            }

                                            prevEmpID = currEmpID;
                                        }
                                    }

                                        if(empListSubmitted.length>0){
                                            var empNotSubmittedd = inAButNotInB(a_data[currEmpID], empListSubmitted);
                                            if(empNotSubmittedd.length>0){
                                                empListNS[prevEmpID] = empNotSubmittedd;
                                            }

                                        }

                                        var notSumbittedAtAll = inAButNotInB(employeeList, empSubmittedOnce);
                                        if(notSumbittedAtAll.length > 0){
                                            for(var nsub=0; nsub<notSumbittedAtAll.length;nsub++){
                                                empListNS[notSumbittedAtAll[nsub]] = a_data[notSumbittedAtAll[nsub]];
                                            }
                                        }
                                    
                                    log.debug("Log", 'empListNS :' + JSON.stringify(empListNS));

                                    //employeeList -- list of those employees whose timesheets are in pending approval status

                                    var timesheetSearchNS = search.create({
                                       type: "timesheet",
                                       filters:
                                       [
                                          ["employee","anyof",employeeList], 
                                          "AND", 
                                          ["timesheetdate","within",getFinalDate,getDateToKeepFinal],
                                          "AND", 
                                          ["approvalstatus","anyof","2"]
                                       ],
                                       columns:
                                       [
                                          search.createColumn({
                                             name: "employee",
                                             summary: "GROUP",
                                             sort: search.Sort.DESC,
                                             label: "Employee"
                                          }),
                                          search.createColumn({name: "startdate", summary: "GROUP",label: "Start Date"}),
                                          search.createColumn({name: "enddate", summary: "GROUP",label: "enddate"})
                                          //search.createColumn({name: "internalid", label: "Internal ID"})
                                       ]
                                    });
                                    var timeSearchCount = timesheetSearchNS.runPaged().count;
                                    log.debug("timeSearchCount",timeSearchCount);

                                    var empListNSTA = new Object();

                                    if(timeSearchCount>0)
                                    {
                                        var results = [];
                                        var count = 0;
                                        var pageSize = timeSearchCount;
                                        var start = 0;
                                        do {
                                        var subresults = timesheetSearchNS.run().getRange({
                                        start: start,
                                        end: start + pageSize
                                        });

                                        results = results.concat(subresults);
                                        count = subresults.length;
                                        start += pageSize;
                                        } while (count == pageSize);

                                        for (var indx_TA = 0; indx_TA < timeSearchCount; indx_TA++) 
                                        {
                                            var EmpID = results[indx_TA].getValue({name: "employee",summary: "GROUP"});
                                            log.debug("EmpID",EmpID);

                                            var startDate = results[indx_TA].getValue({name: "startdate",summary: "GROUP"});
                                            log.debug("startDate",startDate);

                                            if(!_logValidation(empListNSTA[EmpID]))
                                            {
                                                empListNSTA[EmpID] = new Object();

                                                empListNSTA[EmpID] = new Array();
                                                
                                                empListNSTA[EmpID].push(startDate);
                                                
                                            }
                                            else
                                            {
                                                empListNSTA[EmpID].push(startDate);
                                                
                                            }
                                        }

                                        log.debug("Log", 'empListNSTA :' + JSON.stringify(empListNSTA));
                                    }

              /*  var obj_pair = {};
                
                obj_pair[project][employeeid]={
                    empListNS : empListNS,
                    empListNSTA : empListNSTA,
                   employeeNameWise : employeeNameWise,
                   employeeWeekHrs : employeeWeekHrs,
                };
                */
                
                var obj_pair = {};
                obj_pair.empListNS = empListNS;
                obj_pair.empListNSTA = empListNSTA;
                obj_pair.employeeNameWise = employeeNameWise;
                obj_pair.employeeWeekHrs = employeeWeekHrs;
                
                var filters = new Array();
                var data = new Object();
                //data['empListNS'] = empListNS;
                //data['empListNSTA'] = empListNSTA;
                //filters.push(data);

                context.write({
                    key: empPractice,
                    value: obj_pair
                });
            }

            catch(err)
            {
                log.debug('Error: ' + err.message);
            }
        }

    function reduce(context) 
    {
            
        try
        {
         //Fetch the key
            var practice= context.key;
            log.debug("Reduce", 'practice :' + practice);

            //Fetch the Practice Head
            var lookupDept = search.lookupFields({
                                type: search.Type.DEPARTMENT,
                                id: practice,
                                columns: ['custrecord_practicehead']            
                                });
                                var lookupDeptHead = lookupDept.custrecord_practicehead[0];
                                if(typeof(lookupDeptHead)!= "undefined")
                                {
                                    var practiceHead = lookupDeptHead.value;
                                    log.debug({
                                        title: 'Log',
                                        details: 'practiceHead: '+practiceHead
                                    });

                                    var practiceHeadText = lookupDeptHead.text;
                                    log.debug({
                                        title: 'Log',
                                        details: 'practiceHeadText: '+practiceHeadText
                                    });

                                    var practiceHeadText = practiceHeadText.split("-")[1];  

                                }

                                //fetch practice SPOCS

                                var spocsEmail = SPOCSearch(practice);
                                log.debug({
                                        title: 'Log',
                                        details: 'spocsEmail: '+spocsEmail
                                        });
                                var spocsEmailArray = new Array();

                                if(typeof(spocsEmail)!= "undefined" && spocsEmail!= null)
                                {
                                    if(spocsEmail.indexOf((";")>-1))
                                    {
                                        var spocsEmaillength = spocsEmail.split(';').length
                                        log.debug({
                                        title: 'Log',
                                        details: 'spocsEmaillength: '+spocsEmaillength
                                        });

                                        for (var spoc = 0; spoc<spocsEmaillength; spoc++)
                                        {
                                            spocsEmailSplit = spocsEmail.split(";")[spoc];
                                            log.debug({
                                            title: 'Log',
                                            details: 'spocsEmailSplit: '+spocsEmailSplit
                                            });
                                            spocsEmailArray.push(spocsEmailSplit);
                                            log.debug({
                                            title: 'Log',
                                            details: 'spocsEmailArray: '+spocsEmailArray
                                            });
                                        }
                                    }
                                    else
                                    {
                                        spocsEmailArray.push(spocsEmail);
                                    }
                                }

                                var htmlBody = '';
            
                        htmlBody += '<!DOCTYPE html> <html><body>'
                        htmlBody +='<html lang="en" xmlns="http://www.w3.org/1999/xhtml" xmlns:o="urn:schemas-microsoft-com:office:office">'
                        htmlBody +='<head>';
                        htmlBody +='<meta charset="utf-8">';
                        htmlBody +='<meta name="viewport" content="width=device-width,initial-scale=1">';
                        htmlBody +='<meta name="x-apple-disable-message-reformatting">';
                        htmlBody +='<title></title>';
                        htmlBody +='<!--[if mso]>';
                        htmlBody +='  <style>';
                        htmlBody +='    table {border-collapse:collapse;border-spacing:0;border:none;margin-left: auto; margin-right: auto;}';
                        htmlBody +='    div, td {padding:0;}';
                        htmlBody +='    div {margin:0 !important;}';
                        htmlBody +='    </style>';
                        htmlBody +='  <noscript>';
                        htmlBody +='    <xml>';
                        htmlBody +='      <o:OfficeDocumentSettings>';
                        htmlBody +='        <o:PixelsPerInch>96</o:PixelsPerInch>';
                        htmlBody +='      </o:OfficeDocumentSettings>';
                        htmlBody +='    </xml>';
                        htmlBody +='  </noscript>';
                        htmlBody +='  <![endif]-->';
                        htmlBody +='  <style>';
                        htmlBody +='table, td, div, h1, p {';
                        htmlBody +='font-family: Arial, sans-serif;';
                        htmlBody +='    }';
                        htmlBody +='    @media screen and (max-width: 700px) {';
                        htmlBody +='      .unsub {';
                        htmlBody +='        display: block;';   
                        htmlBody +='        padding: 8px;';
                        htmlBody +='        margin-top: 5px;';
                        htmlBody +='        border-radius: 6px;';
                        htmlBody +='        background-color: #555555;';
                        htmlBody +='       text-decoration: none !important;';
                        htmlBody +='        font-weight: bold;';
                        htmlBody +='      }';
                        htmlBody +='      .col-lge {';
                        htmlBody +='        max-width: 100% !important;';
                        htmlBody +='      }';
                        htmlBody +='    }';                                     
                        htmlBody +='    @media screen and (min-width: 700px) {';
                        htmlBody +='      .col-sml {';                  
                        htmlBody +='       max-width: 27% !important;';
                        htmlBody +='     }';
                        htmlBody +='      .col-lge {';
                        htmlBody +='        max-width: 73% !important;';
                        htmlBody +='      }';
                        htmlBody +='    }';
                        htmlBody +='  </style>';
                        htmlBody +='</head>';
                        htmlBody +='<body style="margin:0;padding:0;word-spacing:normal;">';                                                                                                    
                        htmlBody +='  <div role="article" aria-roledescription="email" lang="en" style="text-size-adjust:100%;-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;">';
                        htmlBody +='    <table role="presentation" style="width:96%;border:none;border-spacing:0;">';
                        htmlBody +='      <tr>';                                    
                        htmlBody +='        <td align="center" style="padding:0;">';
                        htmlBody +='          <!--[if mso]>';                                                   
                        htmlBody +='          <table role="presentation" align="center" style="width:900px;">';
                        htmlBody +='          <tr>';
                        htmlBody +='          <td>';
                        htmlBody +='          <![endif]-->';                                                                                                                                                                            
                        htmlBody +='          <table role="presentation" style="width:96%;max-width:900px;border:none;border-spacing:0;text-align:left;font-family:Arial,sans-serif;font-size:13px;line-height:22px;color:#363636;">';
                        htmlBody +='            <tr>';                                                                                          
                        htmlBody +='              <td style="padding:40px 30px 30px 30px;text-align:center;font-size:13px;font-weight:bold;">';
                        htmlBody +='              </td>';
                        htmlBody +='           </tr>';
                        htmlBody +='            <tr>';                                                  
                        htmlBody +='              <td style="padding:30px;background-color:#ffffff;">';  
                        htmlBody += '<p><img src="https://3883006-sb1.app.netsuite.com/core/media/media.nl?id=4760323&c=3883006_SB1&h=Q2oI5ubOox_n1nk1U_BeKf81Slh1nUCL1CRVv7USePV7rzD_" width="100%" alt="Logo" style="max-width:100%;height:auto;border:none;text-decoration:none;color:#ffffff;"></p>'
                        htmlBody+='<p><span style="font-family: Calibri; font-size: 11.0pt;">Dear '+practiceHeadText+',</span></p> '
                        htmlBody+='<p>&nbsp;</p>'
                        htmlBody+='<p><span style="font-family: Calibri; font-size: 11.0pt;">As you know, timesheet submissions are due each week by COB Mondays, and timesheet approvals are due by COB Thursdays. The following is a list of all team members that have outstanding timesheet submissions and/or outstanding timesheet approvals.  We request you reach out to the individuals and ensure the necessary timesheet submissions/approvals are completed asap. &nbsp;</span></p>'
                        htmlBody+='<p>&nbsp;</p>'
                        htmlBody+='<p><span style="font-family: Calibri; font-size: 11.0pt">If you need any assistance or believe the timesheets above have been submitted/approved, please reach out to</span><span style="color: #4472c4; font-family: Calibri; font-size: 11.0pt;"> timesheet@brillio.com.</span></p>'
                        htmlBody+='<p>&nbsp;</p>'
                        htmlBody += '<table style="border:1px solid black;width:100%;margin: 0 auto; text-align:center;"><tbody><tr><th style="border-bottom:1px solid black; border-right:1px solid black;text-align:left;"><b>Employee</b></th>    <th style="border-bottom:1px solid black; border-right:1px solid black;text-align:left;"><b>Project</b></th>     <th style="border-bottom:1px solid black; border-right:1px solid black;text-align:left;"><b>Customer</b></th>     <th style="border-bottom:1px solid black; border-right:1px solid black;text-align:left;"><b>Time Approver</b></th>     <th style="border-bottom:1px solid black; border-right:1px solid black;text-align:center;"><b>No.of Timesheets</b></th>    <th style="border-bottom:1px solid black; border-right:1px solid black;text-align:center;"><b>Pending Submission hours</b></th>   <th style="border-bottom:1px solid black;text-align:center;"><b>Amount</b></th></tr>'

                        customerArray = new Object();
                        customerArrayTA = new Object();

            // Fetch the values

            var csv_out_data = '';
            var len = context.values.length;
            var arr_temp = [];
            var employeeNameWiseFinal = [];var empListNSFinal=[];var empListNSTAFinal=[];
            var employeeWeekHrsFinal=[];
            for (var i = 0; i < len; i++) {
                var parserVal = JSON.parse(context.values[i]);
                arr_temp.push(parserVal)

                var empListNS = arr_temp[i].empListNS
                log.debug("Reduce", 'empListNS :' + JSON.stringify(empListNS));
                empListNSFinal.push(empListNS);
                
                var empListNSTA = arr_temp[i].empListNSTA
                empListNSTAFinal.push(empListNSTA);
                log.debug("Reduce", 'empListNSTA :' + JSON.stringify(empListNSTA));
                
                var employeeNameWise = arr_temp[i].employeeNameWise
                employeeNameWiseFinal.push(employeeNameWise);
                log.debug("Reduce", 'employeeNameWise :' + JSON.stringify(employeeNameWise));

                var employeeWeekHrs = arr_temp[i].employeeWeekHrs
                log.debug("Reduce", 'employeeWeekHrs :' + JSON.stringify(employeeWeekHrs));
                employeeWeekHrsFinal.push(employeeWeekHrs);

            }
                    log.debug("Reduce", 'employeeNameWiseFinal :' + JSON.stringify(employeeNameWiseFinal));
                    log.debug("Reduce", 'empListNSFinal :' + JSON.stringify(empListNSFinal));
                    log.debug("Reduce", 'employeeWeekHrsFinal :' + JSON.stringify(employeeWeekHrsFinal));
                       // for(pass in empListNS)
                           for(var pass=0;empListNSFinal.length>pass;pass++)
                            {
                                log.debug("Reduce", 'empListNSFinal'+pass,JSON.stringify(empListNSFinal[pass]));
                                log.debug("Reduce", 'employeeNameWiseFinal'+pass,JSON.stringify(employeeNameWiseFinal[pass]));
                                
                                for(var key in empListNSFinal[pass])
                                {
                                    log.debug("Reduce", 'key'+key);
                                     var EmployeeName = employeeNameWiseFinal[pass][key].split("-");
                                        if(EmployeeName.length>1){
                                        EmployeeName = EmployeeName[1];
                                        }

                                     var missingArray = empListNSFinal[pass][key];
                                    log.debug("Log", 'missingArray check ns :' + missingArray);

                                    if(missingArray!= '' && missingArray!= null && typeof(missingArray)!= "undefined")
                                        {
                                            log.debug("Log", 'missingArray inside:' + missingArray.length);
                                            for(var index_pass=0;index_pass<missingArray.length;index_pass++){

                                                var startDate = missingArray[index_pass];
                                                log.debug("Log", 'startDate :' + startDate);
                                                 startDated = formatDate(startDate);
                                                 var d_endDate= formatDate(new Date(startDated).setDate(new Date(startDated).getDate()+6));
                                                 d_endDate = formatDate(d_endDate);

                                                     var notSubmittedHr = employeeWeekHrsFinal[pass][key][startDate]
                                                     log.debug("Log", 'notSubmittedHr check ns :' + notSubmittedHr);

                                                 if(typeof(notSubmittedHr)!= "undefined")
                                                 {
                                                    var notSubmittedHrFinal = notSubmittedHr.split("$#$")[0];
                                                     var notSubmittedAmt = notSubmittedHr.split("$#$")[1];
                                                     var Customer = notSubmittedHr.split("$#$")[2];
                                                     var project = notSubmittedHr.split("$#$")[3];
                                                     var timeApprover = notSubmittedHr.split("$#$")[4];
                                                     var timeApproverSplit = timeApprover.split("-")[1];

                                                    // Customer-Project wise Array

                                                    if(!_logValidation(customerArray[Customer])){

                                                         customerArray[Customer] = new Object();
                                                         customerArray[Customer][timeApprover] = new Array();      
                                                         //if(customerArray[Customer][timeApprover].indexOf((pass)<0)){
                                                         customerArray[Customer][timeApprover].push(empListNSFinal[pass]);

                                                         log.debug("Log", '1:1 :' + '1:1');
                                                        //}
                                                         
                                                    } else{
                                                        if(!_logValidation(customerArray[Customer][timeApprover])){
                                                            //customerArray[Customer] = new Object();  
                                                             customerArray[Customer][timeApprover] = new Array();
                                                             //if(customerArray[Customer][timeApprover].indexOf((pass)<0)){       
                                                             customerArray[Customer][timeApprover].push(empListNSFinal[pass]);
                                                            log.debug("Log", '2:2 :' + '2:2');
                                                                
                                                            //} 
                                                        }else {
                                                                //customerArray[Customer][timeApprover] = new Array();
                                                                  //if(customerArray[Customer][timeApprover].indexOf((pass)<0)){
                                                                  customerArray[Customer][timeApprover].push(empListNSFinal[pass]);
                                                                  log.debug("Log", '3:3 :' + '3:3');
                                                                //}
                                                              }
                                                        
                                                    }
                                                    customerArray[Customer][timeApprover] = customerArray[Customer][timeApprover].filter(function(v, i, self)
                                                    {
                                                          
                                                        // It returns the index of the first
                                                        // instance of each value
                                                        return i == self.indexOf(v);
                                                    });
                                                    customerArray[Customer][timeApprover].sort();
                                                 }

                                            }
                                        }
                                }
                            }

                        log.debug("Log", 'customerArray :' + JSON.stringify(customerArray));
                                //var i=0;
                                for(var cust in customerArray)
                                {
                                    var totalHours = 0;
                                    var noTS = 0;
                                    var tCounter = 0;
                                    for (var ta in customerArray[cust])
                                    {
                                        tCounter++;
                                        var proj_NoTS = 0;
                                        var pro_Totalhrs = 0;
                                        var pro_TotalAMt = 0;
                                        for (var emp in customerArray[cust][ta])
                                        {
                                            for(var pass1=0;empListNSFinal.length>pass1;pass1++)
                                            {
                                                for(var key in customerArray[cust][ta][emp])
                                                {   
                                                    log.debug("Reduce", 'key1:'+key);
                                                   
                                                    var EmployeeNamePrint = employeeNameWiseFinal[pass1][key]
                                                    if(typeof(EmployeeNamePrint)!= "undefined")
                                                    {
                                                        EmployeeNamePrint = EmployeeNamePrint.split("-");
                                                        if(EmployeeNamePrint.length>1){
                                                        EmployeeNamePrint = EmployeeNamePrint[1];
                                                        log.debug("Reduce", 'EmployeeNamePrint:'+EmployeeNamePrint);
                                                        }
                                                  //  }
                                                        
                                                    var missingArrayPrint = empListNSFinal[pass1][key];
                                                    log.debug("Log", 'missingArrayPrint :' + missingArrayPrint);

                                                    var notSubmittedHrSum = 0;
                                                    var notSubmittedAmtSum = 0;

                                                    if(missingArrayPrint!= '' && missingArrayPrint!= null)
                                                    {
                                                        for(var i_pass=0;i_pass<missingArrayPrint.length;i_pass++){

                                                            var weekNumbers = missingArrayPrint.length
                                                            log.debug("Log", 'weekNumbers :' + weekNumbers);

                                                            var startDate = missingArrayPrint[i_pass];
                                                             startDated = formatDate(startDate);
                                                             var d_endDate= formatDate(new Date(startDated).setDate(new Date(startDated).getDate()+6));
                                                             d_endDate = formatDate(d_endDate);

                                                             var notSubmittedHr = employeeWeekHrsFinal[pass1][key][startDate]
                                                             if(typeof(notSubmittedHr)!= "undefined")
                                                            {
                                                                 var notSubmittedHrFinal = notSubmittedHr.split("$#$")[0];
                                                                 log.debug("Log", 'notSubmittedHrFinal :' + notSubmittedHrFinal);

                                                                 notSubmittedHrSum = parseInt(notSubmittedHrSum) + parseInt(notSubmittedHrFinal);
                                                                 log.debug("Log", 'notSubmittedHrSum :' + notSubmittedHrSum);

                                                                 var notSubmittedAmtFinal = notSubmittedHr.split("$#$")[1];
                                                                 log.debug("Log", 'notSubmittedAmtFinal :' + notSubmittedAmtFinal);

                                                                 notSubmittedAmtSum = parseFloat(notSubmittedAmtSum) + parseFloat(notSubmittedAmtFinal);
                                                                 log.debug("Log", 'notSubmittedAmtSum :' + notSubmittedAmtSum);

                                                                 var Customer = notSubmittedHr.split("$#$")[2];
                                                                 var project = notSubmittedHr.split("$#$")[3];
                                                                 var timeApprover = notSubmittedHr.split("$#$")[4];
                                                                 var timeApproverSplit = timeApprover.split("-")[1];
                                                             }

                                                        }
                                                    }
                                           
                                                    proj_NoTS = parseFloat(proj_NoTS) + parseFloat(weekNumbers);
                                                    log.debug("Log", 'proj_NoTS :' + proj_NoTS);

                                                    pro_Totalhrs = parseFloat(pro_Totalhrs) + parseFloat(notSubmittedHrSum);
                                                    log.debug("Log", 'pro_Totalhrs :' + pro_Totalhrs);

                                                    pro_TotalAMt = parseFloat(pro_TotalAMt) + parseFloat(notSubmittedAmtSum);
                                                    log.debug("Log", 'pro_TotalAMt :' + pro_TotalAMt);

                                                    htmlBody+= '<tr><td style="border-bottom:1px solid black; border-right:1px solid black; text-align:left;">'+EmployeeNamePrint+'</td><td style="border-bottom:1px solid black; border-right:1px solid black;text-align:left;">'+project+'</td><td style="border-bottom:1px solid black; border-right:1px solid black;text-align:left;">'+Customer+'</td><td style="border-bottom:1px solid black; border-right:1px solid black;text-align:left;">'+timeApproverSplit+'</td><td style="border-bottom:1px solid black; border-right:1px solid black;">'+weekNumbers+'</td><td style="border-bottom:1px solid black; border-right:1px solid black;">'+notSubmittedHrSum+'</td><td style="border-bottom:1px solid black;">'+parseFloat(notSubmittedAmtSum).toFixed(2)+'</td></tr>'
                                                    //i++;
                                                }
                                            }
                                            }
                                        }

                                        htmlBody+= '<tr><td>&nbsp;</td><td style="border-right:1px solid black;text-align:center;">Total</td><td>&nbsp;</td><td>&nbsp;</td><td style="border-right:1px solid black;text-align:center;">'+proj_NoTS+'</td><td style="border-right:1px solid black;text-align:center;">'+pro_Totalhrs+'</td><td style="text-align:center;">'+parseFloat(pro_TotalAMt).toFixed(2)+'</td></tr>'
                                        htmlBody +='</tbody></table>';
                                        htmlBody +='</div>';
                                    }

                                }

                        //For Pending approval Timesheets

                                for(var pass=0;empListNSTAFinal.length>pass;pass++)
                                {
                                
                                    for(key in empListNSTAFinal[pass])
                                    {
                                        var EmployeeName = employeeNameWiseFinal[pass][key].split("-");
                                        if(EmployeeName.length>1){
                                            EmployeeName = EmployeeName[1];
                                            log.debug("Log", 'EmployeeName :' + EmployeeName);
                                        }

                                        var missingArrayTA = empListNSTAFinal[pass][key];
                                        log.debug("Log", 'missingArrayTA :' + missingArrayTA);

                                        if(missingArrayTA!= '' && missingArrayTA!= null && typeof(missingArrayTA)!= "undefined")
                                        {
                                            log.debug("Log", 'Check :' + 'check');
                                            for(var index_pass=0;index_pass<missingArrayTA.length;index_pass++){

                                                var startDate = missingArrayTA[index_pass];
                                                 startDated = formatDate(startDate);
                                                 var d_endDate= formatDate(new Date(startDated).setDate(new Date(startDated).getDate()+6));
                                                 d_endDate = formatDate(d_endDate);

                                                 var notSubmittedHr = employeeWeekHrsFinal[pass][key][startDate]
                                                 if(typeof(notSubmittedHr)!= "undefined")
                                                 {
                                                    var notSubmittedHrFinal = notSubmittedHr.split("$#$")[0];
                                                     var notSubmittedAmt = notSubmittedHr.split("$#$")[1];
                                                     var Customer = notSubmittedHr.split("$#$")[2];
                                                     var project = notSubmittedHr.split("$#$")[3];
                                                     var timeApprover = notSubmittedHr.split("$#$")[4];
                                                     timeApprover = timeApprover.split("-")[1];
                                                     var timeApproverSplit = timeApprover.split("-")[1];

                                                    // Customer-Project wise Array

                                                    if(!_logValidation(customerArrayTA[Customer])){

                                                         customerArrayTA[Customer] = new Object();
                                                         customerArrayTA[Customer][timeApprover] = new Array();       
                                                         //if(customerArrayTA[Customer][timeApprover].indexOf((pass)<0)){
                                                         customerArrayTA[Customer][timeApprover].push(empListNSTAFinal[pass]);

                                                         log.debug("Log", '1:1 :' + '1:1');
                                                        //}
                                                         
                                                    } else{
                                                        if(!_logValidation(customerArrayTA[Customer][timeApprover])){
                                                            //customerArray[Customer] = new Object();  
                                                             customerArrayTA[Customer][timeApprover] = new Array();
                                                             //if(customerArrayTA[Customer][timeApprover].indexOf((pass)<0)){       
                                                             customerArrayTA[Customer][timeApprover].push(empListNSTAFinal[pass]);
                                                            log.debug("Log", '2:2 :' + '2:2');
                                                                
                                                            //} 
                                                        }else {
                                                                  //customerArrayTA[Customer][timeApprover] = new Array();
                                                                  //if(!customerArrayTA[Customer][timeApprover].indexOf((pass)>-1)){
                                                                  customerArrayTA[Customer][timeApprover].push(empListNSTAFinal[pass]);
                                                                  log.debug("Log", '3:3 :' + '3:3');
                                                                //}
                                                              }
                                                        
                                                    }
                                                    customerArrayTA[Customer][timeApprover] = customerArrayTA[Customer][timeApprover].filter(function(v, i, self)
                                                    {
                                                          
                                                        // It returns the index of the first
                                                        // instance of each value
                                                        return i == self.indexOf(v);
                                                    });
                                                    customerArrayTA[Customer][timeApprover].sort();

                                                }
                                            }
                                        }
                                    }
                                }

                                log.debug("Log", 'customerArrayTA :' + JSON.stringify(customerArrayTA));
                                    htmlBody+='<p>&nbsp;</p>'
                                    htmlBody += '<table style="border:1px solid black;width:100%;margin: 0 auto; text-align:center;"><tbody><tr> <tr>     <th style="border-bottom:1px solid black; border-right:1px solid black;text-align:left;"><b>Time Approver</b></th>    <th style="border-bottom:1px solid black; border-right:1px solid black;text-align:left;"><b>Project</b></th>     <th style="border-bottom:1px solid black; border-right:1px solid black;text-align:left;"><b>Customer</b></th>     <th style="border-bottom:1px solid black; border-right:1px solid black;text-align:left;"><b>Employee</b></th>     <th style="border-bottom:1px solid black; border-right:1px solid black;text-align:center;"><b>No.of Timesheets</b></th>    <th style="border-bottom:1px solid black; border-right:1px solid black;text-align:center;"><b>Pending approval hours</b></th>   <th style="border-bottom:1px solid black;text-align:center;"><b>Amount</b></th></tr>'
                                    for(cust in customerArrayTA)
                                    {
                                        var totalHours = 0;
                                        var noTS = 0;
                                        var tCounter = 0;

                                        for (ta in customerArrayTA[cust])
                                        {
                                            tCounter++;
                                            var proj_NoTS = 0;
                                            var pro_Totalhrs = 0;
                                            var pro_TotalAMt = 0;

                                            for (emp in customerArrayTA[cust][ta])
                                            {
                                                for(var pass1=0;empListNSTAFinal.length>pass1;pass1++)
                                                {
                                                    for(var key in customerArrayTA[cust][ta][emp])
                                                    {   
                                                        log.debug("Reduce", 'key1:'+key);

                                                        var EmployeeNamePrint = employeeNameWiseFinal[pass1][key]
                                                        if(typeof(EmployeeNamePrint)!= "undefined")
                                                        {
                                                            EmployeeNamePrint = EmployeeNamePrint.split("-");
                                                            if(EmployeeNamePrint.length>1){
                                                            EmployeeNamePrint = EmployeeNamePrint[1];
                                                            log.debug("Reduce", 'EmployeeNamePrint:'+EmployeeNamePrint);
                                                            }
                                                        //}

                                                        var missingArrayPrintTA = empListNSTAFinal[pass1][key];
                                                        log.debug("Log", 'missingArrayPrintTA :' + missingArrayPrintTA);

                                                        var notSubmittedHrSum = 0;
                                                        var notSubmittedAmtSum = 0;

                                                        if(missingArrayPrintTA!= '' && missingArrayPrintTA!= null)
                                                        {
                                                            for(var i_pass=0;i_pass<missingArrayPrintTA.length;i_pass++){

                                                                var weekNumbers = missingArrayPrintTA.length
                                                                log.debug("Log", 'weekNumbers :' + weekNumbers);

                                                                var startDate = missingArrayPrintTA[i_pass];
                                                                 startDated = formatDate(startDate);
                                                                 var d_endDate= formatDate(new Date(startDated).setDate(new Date(startDated).getDate()+6));
                                                                 d_endDate = formatDate(d_endDate);

                                                                 var notSubmittedHr = employeeWeekHrsFinal[pass1][key][startDate]
                                                                 if(typeof(notSubmittedHr)!= "undefined")
                                                                 {
                                                                     var notSubmittedHrFinal = notSubmittedHr.split("$#$")[0];
                                                                     log.debug("Log", 'notSubmittedHrFinal :' + notSubmittedHrFinal);

                                                                     notSubmittedHrSum = parseInt(notSubmittedHrSum) + parseInt(notSubmittedHrFinal);
                                                                     log.debug("Log", 'notSubmittedHrSum :' + notSubmittedHrSum);

                                                                     var notSubmittedAmtFinal = notSubmittedHr.split("$#$")[1];
                                                                     log.debug("Log", 'notSubmittedAmtFinal :' + notSubmittedAmtFinal);

                                                                     notSubmittedAmtSum = parseFloat(notSubmittedAmtSum) + parseFloat(notSubmittedAmtFinal);
                                                                     log.debug("Log", 'notSubmittedAmtSum :' + notSubmittedAmtSum);

                                                                     var Customer = notSubmittedHr.split("$#$")[2];
                                                                     var project = notSubmittedHr.split("$#$")[3];
                                                                     var timeApprover = notSubmittedHr.split("$#$")[4];
                                                                     var timeApproverSplit = timeApprover.split("-")[1];
                                                                 }

                                                                 //project wise object


                                                            }
                                                        }
                                   
                                                        proj_NoTS = parseFloat(proj_NoTS) + parseFloat(weekNumbers);
                                                        log.debug("Log", 'proj_NoTS :' + proj_NoTS);

                                                        pro_Totalhrs = parseFloat(pro_Totalhrs) + parseFloat(notSubmittedHrSum);
                                                        log.debug("Log", 'pro_Totalhrs :' + pro_Totalhrs);

                                                        pro_TotalAMt = parseFloat(pro_TotalAMt) + parseFloat(notSubmittedAmtSum);
                                                        log.debug("Log", 'pro_TotalAMt :' + pro_TotalAMt);

                                                       htmlBody+= '<tr><td style="border-bottom:1px solid black; border-right:1px solid black;text-align:left;">'+timeApproverSplit+'</td><td style="border-bottom:1px solid black; border-right:1px solid black;text-align:left;">'+project+'</td><td style="border-bottom:1px solid black; border-right:1px solid black;text-align:left;">'+Customer+'</td><td style="border-bottom:1px solid black; border-right:1px solid black;text-align:left;">'+EmployeeNamePrint+'</td><td style="border-bottom:1px solid black; border-right:1px solid black;text-align:center;">'+weekNumbers+'</td><td style="border-bottom:1px solid black; border-right:1px solid black;text-align:center;">'+notSubmittedHrSum+'</td><td style="border-bottom:1px solid black;text-align:center;">'+parseFloat(notSubmittedAmtSum).toFixed(2)+'</td></tr>'
                                                    
                                                    }
                                                }
                                                }
                                            }

                                            htmlBody+= '<tr><td>&nbsp;</td><td style="border-right:1px solid black;text-align:center">Total</td><td>&nbsp;</td><td>&nbsp;</td><td style="border-right:1px solid black;text-align:center">'+proj_NoTS+'</td><td style="border-right:1px solid black;text-align:center;">'+pro_Totalhrs+'</td><td text-align="right">'+parseFloat(pro_TotalAMt).toFixed(2)+'</td></tr>'
                                            htmlBody +='</tbody></table>';
                                            htmlBody +='</div>';
                                        }
                                    }


                        var notSubmittedcount = 0;

                        htmlBody+='<p>&nbsp;</p>'
                        htmlBody+='<p><span style="font-family: Calibri; font-size: 11.0pt;">Thank you in advance for your support.</span></p>'
                        htmlBody+='<p>&nbsp;</p>'
                        //htmlBody+='<p><span style="font-family: Calibri; font-size: 11.0pt; align:right">'+'Dated-'+datePrintEmail+'&nbsp;'+timePrintEmail+'&nbsp;'+offset+'</span></p>'
                        //htmlBody+='<p>&nbsp;</p>'
                        htmlBody+='<p><span style="font-family: Calibri; font-size: 11.0pt">Regards,</span></p><p><span style="font-family: Calibri; font-size: 11.0pt;">Brillio HR </span></p>'
                        htmlBody+='<p><img src="https://3883006-sb1.app.netsuite.com/core/media/media.nl?id=4776179&c=3883006_SB1&h=Ap_D8kgQKiT1fIr6X0gWwBXfWpDRrXtihcRIpDiGgPCyzgXY" alt="Right click to download image." width="100%" style="max-width:100%;height:auto;border:none;text-decoration:none;color:#ffffff;">';
                        htmlBody+='</p>';

                        htmlBody +='              </td>';
                        htmlBody +='            </tr>';           
                        htmlBody +='            <!--tr>';
                        htmlBody +='              <td style="padding:35px 30px 11px 30px;font-size:0;background-color:#ffffff;border-bottom:1px solid #f0f0f5;border-color:rgba(201,201,207,.35);">';
                        htmlBody +='                <!--[if mso]>';
                        htmlBody +='                <table role="presentation" width="100%">';
                        htmlBody +='                <tr>';
                        htmlBody +='                <td style="width:145px;" align="left" valign="top">';
                        htmlBody +='               <![endif]-->';                
                        htmlBody +='                <!--[if mso]>';
                        htmlBody +='                </td>';
                        htmlBody +='                <td style="width:395px;padding-bottom:20px;" valign="top">';
                        htmlBody +='                <![endif]-->';
                                     
                        htmlBody +='               <!--[if mso]>';
                        htmlBody +='                </td>';
                        htmlBody +='                </tr>';
                        htmlBody +='                </table>';
                        htmlBody +='                <![endif]-->';
                        htmlBody +='              <!--/td>';
                        htmlBody +='            <!--/tr>';
                        htmlBody +='          </table>';
                        htmlBody +='          <!--[if mso]>';
                        htmlBody +='          </td>';
                        htmlBody +='          </tr>';
                        htmlBody +='          </table>';
                        htmlBody +='          <![endif]-->';
                        htmlBody +='        </td>';
                        htmlBody +='      </tr>';
                        htmlBody +='    </table>';
                        htmlBody +='  </div>';
                        htmlBody +='</body>';
                        htmlBody +='</html>';

                        var notSubmittedWeeks = empListNS[pass];;

                        if((missingArray!= '' && missingArray!= null) || (missingArrayTA!= '' && missingArrayTA!= null))
                        {
                            if (spocsEmailArray!= '' && spocsEmailArray!= null)
                            {
                                log.debug("Log", 'mail check 1 :' + 'mail check 1 ');

                                email.send({
                                author: 442,
                                recipients: practiceHead,
                                cc:spocsEmailArray,
                                subject: 'Pending Timesheet Submissions & Approvals For the week',
                                body: htmlBody
                                });
                            }
                            else
                            {
                                log.debug("Log", 'mail check 2 :' + 'mail check 2');

                                email.send({
                                author: 442,
                                recipients: practiceHead,
                                subject: 'Pending Timesheet Submissions & Approvals For the week',
                                body: htmlBody
                                });
                            }

                            log.debug({
                            title: 'Log',
                            details: 'Mail sent: '+'Sent'
                            });
                        }
            }

            catch(err)
            {
                log.debug('Error: ' + err.message);
            }
    }

        function summarize(summary) 
        {

        }

        function SPOCSearch (prac)
        {
            var customrecord648SearchObj = search.create({
               type: "customrecord648",
               filters:
               [
                  ["custrecord_prac_internal_id","equalto",prac]
               ],
               columns:
               [
                  search.createColumn({name: "custrecord_prac_email", label: "Prac Email"})
               ]
            });
            var searchResultCount = customrecord648SearchObj.runPaged().count;
            log.debug("customrecord648SearchObj result count",searchResultCount);

            if(searchResultCount> 0)
            {
                var searchResult = customrecord648SearchObj.run().getRange({
                            start: 0,
                            end: 100
                        });
                var emailId = searchResult[0].getValue({
                            name: 'custrecord_prac_email'
                        });
                log.debug("Search ID", 'emailId' + emailId);

                return emailId;
            }
        }

        function _Currency_exchangeRate(currency,monthName,yearName) {

            var customrecord_pl_currency_exchange_ratesSearchObj = search.create({
               type: "customrecord_pl_currency_exchange_rates",
               filters:
               [
                  ["custrecord_pl_currency_exchange_month","anyof",monthName], 
                  "AND", 
                  ["custrecord_pl_currency_exchange_year","anyof",yearName]
               ],
               columns:
               [
                  search.createColumn({
                     name: "custrecord_pl_currency_exchange_month",
                     sort: search.Sort.ASC,
                     label: "Month"
                  }),
                  search.createColumn({
                     name: "custrecord_pl_currency_exchange_year",
                     sort: search.Sort.ASC,
                     label: "Year"
                  }),
                  search.createColumn({name: "custrecord_pl_currency_exchange_revnue_r", label: "USD-INR Revenue Rate"}),
                  search.createColumn({name: "custrecord_pl_currency_exchange_cost", label: "USD-INR Cost Rate"}),
                  search.createColumn({name: "custrecord_pl_crc_cost_rate", label: "CRC Cost Conversion"}),
                  search.createColumn({name: "custrecord_pl_nok_cost_rate", label: "NOK Cost Rate"}),
                  search.createColumn({name: "custrecord_gbp_converstion_rate", label: "USD to GBP"}),
                  search.createColumn({name: "custrecord_eur_usd_conv_rate", label: "USD to EUR"}),
                  search.createColumn({name: "custrecord_aud_to_usd", label: "USD to AUD"}),
                  search.createColumn({name: "custrecord_usd_cad_conversion_rate", label: "USD to CAD"}),
                  search.createColumn({name: "custrecord_usd_ron_conversion_rate", label: "USD to RON"})
               ]
            });
            var searchResultCount = customrecord_pl_currency_exchange_ratesSearchObj.runPaged().count;
            log.debug("customrecord_pl_currency_exchange_ratesSearchObj result count",searchResultCount);

            if(searchResultCount> 0)
            {
                var searchResult = customrecord_pl_currency_exchange_ratesSearchObj.run().getRange({
                            start: 0,
                            end: 1
                        });

                if (currency == 2)
                {
                    var i_currency = searchResult[0].getValue({
                            name: 'custrecord_gbp_converstion_rate'
                        });
                    log.debug("Currency", 'i_currency' + i_currency);
                }
                if (currency == 3)
                {
                    var i_currency = searchResult[0].getValue({
                            name: 'custrecord_usd_cad_conversion_rate'
                        });
                    log.debug("Currency", 'i_currency' + i_currency);
                }

                if (currency == 4)
                {
                    var i_currency = searchResult[0].getValue({
                            name: 'custrecord_eur_usd_conv_rate'
                        });
                    log.debug("Currency", 'i_currency' + i_currency);
                }

                if (currency == 6)
                {
                    var i_currency = searchResult[0].getValue({
                            name: 'custrecord_pl_currency_exchange_cost'
                        });
                    log.debug("Currency", 'i_currency' + i_currency);
                }

                if (currency == 10)
                {
                    var i_currency = searchResult[0].getValue({
                            name: 'custrecord_aud_to_usd'
                        });
                    log.debug("Currency", 'i_currency' + i_currency);
                }
                if (currency == 11)
                {
                    var i_currency = searchResult[0].getValue({
                            name: 'custrecord_usd_ron_conversion_rate'
                        });
                    log.debug("Currency", 'i_currency' + i_currency);
                }

            }

            else
            {
                var customrecord_pl_currency_exchange_ratesSearchObj = search.create({
                   type: "customrecord_pl_currency_exchange_rates",
                   filters:
                   [
                   ],
                   columns:
                   [
                      search.createColumn({name: "custrecord_pl_currency_exchange_month", label: "Month"}),
                      search.createColumn({
                         name: "custrecord_pl_currency_exchange_year",
                         sort: search.Sort.ASC,
                         label: "Year"
                      }),
                      search.createColumn({name: "custrecord_pl_currency_exchange_revnue_r", label: "USD-INR Revenue Rate"}),
                      search.createColumn({name: "custrecord_pl_currency_exchange_cost", label: "USD-INR Cost Rate"}),
                      search.createColumn({name: "custrecord_pl_crc_cost_rate", label: "CRC Cost Conversion"}),
                      search.createColumn({name: "custrecord_pl_nok_cost_rate", label: "NOK Cost Rate"}),
                      search.createColumn({name: "custrecord_gbp_converstion_rate", label: "USD to GBP"}),
                      search.createColumn({name: "custrecord_eur_usd_conv_rate", label: "USD to EUR"}),
                      search.createColumn({name: "custrecord_aud_to_usd", label: "USD to AUD"}),
                      search.createColumn({name: "custrecord_usd_cad_conversion_rate", label: "USD to CAD"}),
                      search.createColumn({name: "custrecord_usd_ron_conversion_rate", label: "USD to RON"})
                   ]
                });
                var searchResult = customrecord_pl_currency_exchange_ratesSearchObj.runPaged().count;
                log.debug("customrecord_pl_currency_exchange_ratesSearchObj result count",searchResultCount);

                if(searchResult> 0)
                {
                    var searchResult = customrecord_pl_currency_exchange_ratesSearchObj.run().getRange({
                                start: 0,
                                end: 1
                            });

                    if (currency == 2)
                    {
                        var i_currency = searchResult[0].getValue({
                                name: 'custrecord_gbp_converstion_rate'
                            });
                        log.debug("Currency", 'i_currency' + i_currency);
                    }
                    if (currency == 3)
                    {
                        var i_currency = searchResult[0].getValue({
                                name: 'custrecord_usd_cad_conversion_rate'
                            });
                        log.debug("Currency", 'i_currency' + i_currency);
                    }

                    if (currency == 4)
                    {
                        var i_currency = searchResult[0].getValue({
                                name: 'custrecord_eur_usd_conv_rate'
                            });
                        log.debug("Currency", 'i_currency' + i_currency);
                    }

                    if (currency == 6)
                    {
                        var i_currency = searchResult[0].getValue({
                                name: 'custrecord_pl_currency_exchange_cost'
                            });
                        log.debug("Currency", 'i_currency' + i_currency);
                    }

                    if (currency == 10)
                    {
                        var i_currency = searchResult[0].getValue({
                                name: 'custrecord_aud_to_usd'
                            });
                        log.debug("Currency", 'i_currency' + i_currency);
                    }
                    if (currency == 11)
                    {
                        var i_currency = searchResult[0].getValue({
                                name: 'custrecord_usd_ron_conversion_rate'
                            });
                        log.debug("Currency", 'i_currency' + i_currency);
                    }

                }
            }
            

            return i_currency;

    }


        function _logValidation(value) 
        {
            if (value != '- None -' && value != null && value.toString() != null && value != '' && value != undefined && value.toString() != undefined && value != 'undefined' && value.toString() != 'undefined' && value.toString() != 'NaN' && value != NaN) {
                return true;
            } else {
                return false;
            }
        }


        function formatDate(date)
       {
          date = new Date(date)
          var dd = date.getDate(); 
          var mm = date.getMonth()+1;
          var yyyy = date.getFullYear(); 
          if(dd<10){dd='0'+dd} 
          if(mm<10){mm='0'+mm};
          return d = mm+'/'+dd+'/'+yyyy
       }

       function formatDatePrint(date)
       {
          var  months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
          date = new Date(date)
          var dd = date.getDate(); 
          var mm = date.getMonth()+1;
          var monthName=months[date.getMonth()];
          //var shortmm = date.toLocaleString('en-us', { month: 'short' })+1;
          var yyyy = date.getFullYear(); 
          if(dd<10){dd='0'+dd} 
          if(mm<10){mm='0'+mm};
          return d = dd+'-'+monthName+'-'+yyyy
       }

       function formatTime(date)
       {
          date = new Date(date)
          var hh = date.getHours(); 
          var mm = date.getMinutes();
          var ss = date.getSeconds(); 
          if(hh<10){hh='0'+hh} 
          if(mm<10){mm='0'+mm};
          if(ss<10){ss='0'+ss};
          return d = hh+':'+mm+':'+ss
       }

       function inAButNotInB(A, B) 
        {
            return _.filter(A, function(a) {
                return !_.contains(B, a);
            });
        }

       function CustomerHolidayss(start_date,end_date)
       {
            try
            {
                var customerholidaySearch = search.create({
                       type: "customrecordcustomerholiday",
                       filters:
                       [
                          ["custrecordholidaydate","within",start_date,end_date]
                       ],
                       columns:
                       [
                          search.createColumn({
                             name: "custrecordholidaydate",
                             label: "holiday"
                          }),
                          search.createColumn({
                             name: "custrecord13",
                             sort: search.Sort.DESC,
                             label: "custrecord13"
                          }),
                          search.createColumn({
                             name: "custrecordcustomersubsidiary",
                             sort: search.Sort.DESC,
                             label: "subsidiary"
                          })
                        ]
                    });

            var customerMaster = new Object();

                if (customerholidaySearch) 
                {
                    var holidaySearchLength = customerholidaySearch.runPaged().count;
                    log.debug("holidaySearchLength",holidaySearchLength);

                    var results = [];
                                var count = 0;
                                var pageSize = holidaySearchLength;
                                var start = 0;
                                do {
                                var subresults = customerholidaySearch.run().getRange({
                                start: start,
                                end: start + pageSize
                                });

                                results = results.concat(subresults);
                                count = subresults.length;
                                start += pageSize;
                                } while (count == pageSize);

                    for (var i = 0; i < holidaySearchLength; i++) 
                    {
                        var holidayDate = results[i].getValue({name: "custrecordholidaydate"});
                        log.debug("holidayDate",holidayDate);

                        var customer = results[i].getValue({name: "custrecord13"});
                        log.debug("customer",customer);

                        var empSubsidiary = results[i].getValue({name: "custrecordcustomersubsidiary"});
                        log.debug("empSubsidiary",empSubsidiary);

                        if(!customerMaster[customer]){
                         customerMaster[customer] =new Object();
                         customerMaster[customer][empSubsidiary] = new Array();
                         customerMaster[customer][empSubsidiary].push(holidayDate);
                        
                        } else {
                             if(!customerMaster[customer][empSubsidiary]){
                             customerMaster[customer][empSubsidiary] = new Array();
                             customerMaster[customer][empSubsidiary].push(holidayDate)
                             
                             
                             } else {
                             
                             customerMaster[customer][empSubsidiary].push(holidayDate);
                             
                             }
                        
                        }

                    }
                }

                return customerMaster;
            }

            catch(err)
            {
                log.debug('Error: ' + err.message);
            }
            
       }

       function HolidayCompany(start_date,end_date)
       {
            try
            {
                var search_results_Holiday = search.create({
                       type: "customrecord_holiday",
                       filters:
                       [
                          ["custrecord_date","within",start_date,end_date]
                       ],
                       columns:
                       [
                          search.createColumn({
                             name: "custrecord_date",
                             label: "holiday"
                          }),
                          search.createColumn({
                             name: "custrecordsubsidiary",
                             sort: search.Sort.DESC,
                             label: "subsidiary"
                          }),
                          search.createColumn({
                             name: "custrecord_holiday_location",
                             label: "location"
                          })
                        ]
                    });

            var holidayMaster = new Object();

                if (search_results_Holiday) 
                {
                    var holidaySearchLength = search_results_Holiday.runPaged().count;
                    log.debug("holidaySearchLength",holidaySearchLength);

                    var results = [];
                                var count = 0;
                                var pageSize = holidaySearchLength;
                                var start = 0;
                                do {
                                var subresults = search_results_Holiday.run().getRange({
                                start: start,
                                end: start + pageSize
                                });

                                results = results.concat(subresults);
                                count = subresults.length;
                                start += pageSize;
                                } while (count == pageSize);

                    for (var i = 0; i < holidaySearchLength; i++) 
                    {
                        var date = results[i].getValue({name: "custrecord_date"});
                        log.debug("date",date);

                        var location = results[i].getValue({name: "custrecord_holiday_location"});
                        log.debug("location",location);

                        var subsidiary = results[i].getValue({name: "custrecordsubsidiary"});
                        log.debug("subsidiary",subsidiary);

                        if(!holidayMaster[subsidiary]){
                        holidayMaster[subsidiary] = new Array();
                        holidayMaster[subsidiary].push(date)
                   
                        } else {
                        holidayMaster[subsidiary].push(date)
                        }

                    }
                }

                return holidayMaster;
            }

            catch(err)
            {
                log.debug('Error: ' + err.message);
            }
            
       }

    function calcBusinessDays(d_startDate, d_endDate) { // input given as Date

        // objects
        var startDate = new Date(d_startDate).getTime();
        var endDate = new Date(d_endDate).getTime();
        // Validate input
        if (endDate < startDate)
            return 0;

        // Calculate days between dates
        var millisecondsPerDay = 86400 * 1000; // Day in milliseconds
        new Date(startDate).setHours(0, 0, 0, 1); // Start just after midnight
        new Date(endDate).setHours(23, 59, 59, 999); // End just before midnight
        var diff = endDate - startDate;
        log.debug("diff",diff); // Milliseconds between datetime objects
        var days = Math.ceil(diff / millisecondsPerDay);
        log.debug("days",days);

        // Subtract two weekend days for every week in between
        var weeks = Math.floor(days / 7);
        var days = days - (weeks * 2);

        // Handle special cases
        var startDay = new Date(startDate).getDay();
        log.debug("startDay",startDay);
        var endDay = new Date(endDate).getDay();
        log.debug("endDay",endDay);

        // Remove weekend not previously removed.
        if (startDay - endDay > 1)
            days = days - 2;

        // Remove start day if span starts on Sunday but ends before Saturday
        if (startDay == 0 && endDay != 6)
            days = days - 1

            // Remove end day if span ends on Saturday but starts after Sunday
        if (endDay == 6 && startDay != 0)
            days = days - 1

        if (endDay == startDay)
            days = 1

        if (days>5)
            return 5;
        else
        return days;

}

function companyHolidayCount(startDateHrs,endDateHrs,companyHolidays,empSubsidiary)  
    {

            var count = 0;
            
            if(_logValidation(companyHolidays[empSubsidiary])){
            
            var cmpHolidays =  companyHolidays[empSubsidiary];
            log.debug("Log", 'cmpHolidays :' + cmpHolidays);
            
            for (var i = 0;; i++) {
                                    var d_current_date= formatDate(new Date(startDateHrs).setDate(new Date(startDateHrs).getDate()+i));
                                    log.debug("Log", 'd_current_date :' + d_current_date);
          
                                      if (d_current_date > endDateHrs ) { //|| d_current_date < startDate
                                          break;
                                      }
          
                                      d_current_date = new Date(d_current_date);
                                      var startDateCurrent = format.format({
                                        value: d_current_date,
                                        type: format.Type.DATE
                                        });
                                      log.debug("Log", 'startDateCurrent :' + startDateCurrent);
                                     
                                      if(cmpHolidays.indexOf(d_current_date)!=-1){
                                           count++;
                                      }
                                      
                                      }
            
            
            }
            
            return count;
          
          
    }

    function CustomerHolidaysCount(startDateHrs,endDateHrs,customerHoliday,customer,empSubsidiary,s_employee_name)  
        {

            log.debug("Log", 'customer Holiday :' + empSubsidiary +', '+ customer+', '+s_employee_name);

            var customerHolidayList = '';
            var count = 0;

            try{
                 customerHolidayList = customerHoliday[customer][empSubsidiary]
            } catch(e){
                log.debug("Log", 'e :' + e);

            }
          
            
            
            if(!_logValidation(customerHolidayList)){
                return count;
            }

            else {
                var custHolidays =  customerHoliday[customer][empSubsidiary];
            
            for (var i = 0;; i++) {
                                      var d_current_date= formatDate(new Date(startDateHrs).setDate(new Date(startDateHrs).getDate()+i));
                                      log.debug("Log", 'd_current_date :' + d_current_date);
          
                                      if (d_current_date > endDateHrs ) { //|| d_current_date < startDate
                                          break;
                                      }
          
                                      d_current_date = new Date(d_current_date);
                                      var startDateCurrent = format.format({
                                        value: d_current_date,
                                        type: format.Type.DATE
                                        });
                                      log.debug("Log", 'startDateCurrent :' + startDateCurrent);
                                      
                                      if(custHolidays.indexOf(d_current_date)!=-1){
                                           count++;
                                      }
                                      
                                      }

            }
            
            
            
            
            
            
            return count;
          
          
        }

        return {
            getInputData: getInputData,
            map: map,
            reduce: reduce
            //summarize: summarize
        };
    });