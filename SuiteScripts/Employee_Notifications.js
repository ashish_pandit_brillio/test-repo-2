function Notify_BusinessOps()
{
	var cols = new Array();
	cols[cols.length] = new nlobjSearchColumn('email');
	cols[cols.length] = new nlobjSearchColumn('entityid');
	cols[cols.length] = new nlobjSearchColumn('custentity_fusion_empid'); //Fusion Id
	cols[cols.length] = new nlobjSearchColumn('custentity_empid'); //Solution Id
	cols[cols.length] = new nlobjSearchColumn('custentity_incid'); //Inc Id
	cols[cols.length] = new nlobjSearchColumn('firstname');
	cols[cols.length] = new nlobjSearchColumn('middlename');
	cols[cols.length] = new nlobjSearchColumn('lastname');
	
	var filters = new Array();
	filters[filters.length] = new nlobjSearchFilter('datecreated', null, 'within', new Date(), nlapiAddDays(new Date(), 1));
	
	var searchresult = nlapiSearchRecord('employee', null, filters, cols);
	nlapiLogExecution('DEBUG', 'COUNT', searchresult.length);
	
	if(searchresult != null)
	{
		var htmltext = '<table border="0" width="100%">';
		htmltext += '<tr>';
		htmltext += '<td colspan="6" valign="top">';
		htmltext += '<p>Hi Team,</p>';
		var Tdate = new Date();
		Tdate.toDateString();
		htmltext += '<p>Below are the list of employee(s) which has been created in the system as on ' + Tdate + '</p>';
		htmltext += '</td></tr>';
		htmltext += '<tr><td></td></tr>';
		htmltext += '<tr><td></td></tr>';
		//htmltext += '<tr ><td colspan = "6" font-size="14pt" align="center" bgcolor="#AEAEAE"><b>TimeSheet - Pending Approval</b></td></tr>';
		htmltext += '<tr>';
		htmltext += '<td font-size="14pt" align="center"><b>Sl. No</b></td>';
		htmltext += '<td font-size="14pt" align="center"><b>Employee</b></td>';
		htmltext += '<td font-size="14pt" align="center"><b>Employee Id</b></td>';
		htmltext += '<td font-size="14pt" align="center"><b>Email</b></td>';
		htmltext += '<td font-size="14pt" align="center"><b>Solution Id.</b></td>';
		htmltext += '<td font-size="14pt" align="center"><b>Inc Id.</b></td>';
		htmltext += '</tr>';
		
		for(var i=0; i < searchresult.length; i++)
		{
			htmltext += '<tr>';
			htmltext += '<td font-size="14pt">' + parseInt(i+1) + '</td>';
			htmltext += '<td font-size="14pt">' + searchresult[i].getValue('firstname') + ' ' + searchresult[i].getValue('middlename') + ' ' + searchresult[i].getValue('lastname') + '</td>';
			htmltext += '<td font-size="14pt">' + searchresult[i].getValue('entityid') + '</td>';
			htmltext += '<td font-size="14pt">' + searchresult[i].getValue('email') + '</td>';
			htmltext += '<td font-size="14pt">' + searchresult[i].getValue('custentity_empid') + '</td>';
			htmltext += '<td font-size="14pt">' + searchresult[i].getValue('custentity_incid') + '</td>';
			htmltext += '</tr>';
		}
		htmltext += '<tr></tr>';
		htmltext += '<tr><td>';
		htmltext += '<p>Regards,</p>';
		htmltext += '<p>Information Systems</p>';
		htmltext += '</td></tr></table>';
		htmltext += '<hr width="100%" size="1" noshade color="#CCCCCC">';
		htmltext += '<table width="100%" border="0" cellspacing="0" cellpadding="0">';
		htmltext += '<tr>';
		htmltext += '<td align="right">';
		htmltext += '<font style="font-size:9px; font-family:Verdana,Arial,Helvetica,sans-serif; color:#999999;">Brillio is powered by <a href="http://www.netsuite.com/" style="color:#999999;">NetSuite</a> &#151; One System. No Limits.</font>';
		htmltext += '</td>';
		htmltext += '</tr>';
		htmltext += '</table>';
		htmltext += '</body>';
		htmltext += '</html>';
		
	}
	
	nlapiLogExecution('DEBUG', 'html', htmltext);
	nlapiSendEmail(442, 'vinod.n@brillio.com', 'New Resource Allocation to be done in NetSuite', htmltext, null);
}

/*
function AVA_ConvertDate(Trans_Date)
{
	var CurrentDate, month, day, year;

	CurrentDate = nlapiStringToDate(new Date());
	month = parseInt(CurrentDate.getMonth() + 1);
	day = CurrentDate.getDate();
	year = CurrentDate.getFullYear();
	
	month = month.toString();
	day = day.toString();

	if (month.length == 1)
	{
		month = '0' + month;
	}

	if (day.length == 1)
	{
		day = '0' + day;
	}

	CurrentDate = year + '-' + month + '-' + day;
	CurrentDate = year + '-' + month + '-' + day;
	return CurrentDate;
}
*/