/**
 * @author Jayesh
 */

// BEGIN SUITELET ==================================================

function suiteletFunction(request, response)
{
  try
  {
  	if (request.getMethod() == 'GET')
	{
		var i_current_line_index = request.getParameter('i_line_index');
		nlapiLogExecution('DEBUG', 'suiteletFunction', ' i_current_line_index-->' + i_current_line_index);
		
		if(!i_current_line_index)
		{
			return;
		}
		
		var i_sublist = request.getParameter('sublist');
		
		var f_form = nlapiCreateForm('Contractor Monthly Cost Setup');
		
		var s_cost_msg = '<html>';
		s_cost_msg += '<p><font size="3" color="red">Please take 168 hours into consideration for monthly calculation.</font></p></html>';
		var fld_cost_msg = f_form.addField('cost_msg', 'inlinehtml',null,null);
		fld_cost_msg.setDefaultValue(s_cost_msg);
		
		var f_contractor_cost = f_form.addField('contractor_cost', 'float', 'Contractor Monthly Cost(USD)').setLayoutType('outsideabove','startrow');
		
		var f_line_index = f_form.addField('line_index', 'integer', 'Line Index');
		f_line_index.setDefaultValue(i_current_line_index);
		f_line_index.setDisplayType('hidden');
		
		var f_sublist_type = f_form.addField('sublist', 'integer', 'sublist type');
		f_sublist_type.setDefaultValue(i_sublist);
		f_sublist_type.setDisplayType('hidden');
		
		f_form.addSubmitButton();
		response.writePage(f_form);
		
	}//GET
	else if (request.getMethod() == 'POST')
	{
		var s_costDetails;
		var i_current_line_index =  request.getParameter('line_index');
		nlapiLogExecution('DEBUG','suiteletFunction',' i_current_line_index -->'+i_current_line_index)
		
		var i_sublist_type =  request.getParameter('sublist');
		
		var f_contractor_cost =  request.getParameter('contractor_cost');
		nlapiLogExecution('DEBUG','suiteletFunction',' Contractor cost -->'+f_contractor_cost)
		
		s_costDetails = i_current_line_index+'##'+f_contractor_cost+'##'+i_sublist_type;		
		
		response.write('<html><head><script>window.opener.setContractorCost("' + s_costDetails + '");self.close();</script></head><body></body></html>');
	}//POST
  }
  catch(exception)
  {
  	 nlapiLogExecution('ERROR','suiteletFunction','ERROR ->'+exception);
  }//CATCH
}
// END SUITELET ====================================================