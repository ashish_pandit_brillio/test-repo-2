/**
 * Screen to select a vendor and list / add new sub-tier record for it
 * 
 * Version Date Author Remarks 1.00 13 May 2016 Nitish Mishra
 * 
 */

function suitelet(request, response) {
	try {

		if (request.getMethod() == "GET") {

			var selectedVendor = request.getParameter('vendor');

			if (!selectedVendor) {
				createVendorSelectScreen(request);
			} else {
				createExtensionScreen(request);
			}
		} else {
			var mode = request.getParameter('custpage_mode');

			if (mode == 'Show') {
				createExtensionScreen(request);
			} else if (mode == 'Extend') {
				submitExtensionScreen(request);
			}
		}
	} catch (err) {
		nlapiLogExecution('ERROR', 'createExtensionScreen', err);
		displayErrorForm(err, "Subtier Extension Form");
	}
}

function createVendorSelectScreen(request) {
	try {
		var form = nlapiCreateForm('Subtier Extension Form');
		var vendorField = form.addField('custpage_vendor', 'select', 'Vendor')
		        .setMandatory(true);

		var vendorSearch = nlapiSearchRecord('vendor', null, [
		        new nlobjSearchFilter('isinactive', null, 'is', 'F'),
		        new nlobjSearchFilter('subsidiary', null, 'anyof', '3'),
		        new nlobjSearchFilter('category', null, 'anyof', 12) ], [
		        new nlobjSearchColumn('altname'),
		        new nlobjSearchColumn('entityid') ]);

		vendorSearch.forEach(function(vendor) {
			vendorField.addSelectOption(vendor.getId(), vendor
			        .getValue('entityid')
			        + " " + vendor.getValue('altname'));
		});

		vendorField.addSelectOption('', '', true);

		form.addField('custpage_mode', 'text', 'Mode').setDisplayType('hidden')
		        .setDefaultValue('Show');
		form.addSubmitButton('List Contractors');
		response.writePage(form);
	} catch (err) {
		nlapiLogExecution('ERROR', 'createVendorSelectScreen', err);
		throw err;
	}
}

function createExtensionScreen(request) {
	try {
		var selectedVendor = request.getParameter('custpage_vendor');

		if (selectedVendor) {
			var subTierData = getMappedSubtierRecords(selectedVendor);

			var form = nlapiCreateForm('Subtier Extension Form');
			form.setScript('customscript_cs_sut_subtier_extension');
			form.addButton('custpage_go_back', 'Go Back', 'GoBack()');

			form.addField('custpage_vendor', 'select', 'Vendor', 'vendor')
			        .setDisplayType('inline').setDefaultValue(selectedVendor);
			form.addField('custpage_mode', 'text', 'Mode').setDisplayType(
			        'hidden').setDefaultValue('Extend');

			var subTierList = form.addSubList('custpage_subtier_list', 'list',
			        'Sub-tier Details');

			subTierList.addField('subtier', 'select', 'Sub Tier Record',
			        'customrecord_subtier_vendor_data')
			        .setDisplayType('hidden');
			//subTierList.addField('original', 'select',
			    //    'Original Sub Tier Record',
			     //   'customrecord_subtier_vendor_data')
			      //  .setDisplayType('hidden');
			subTierList.addField('employee', 'select', 'Employee', 'employee')
			        .setDisplayType('inline');
			// subTierList.addField('type', 'select', 'Vendor Type',
			// 'customlist_subtier_vendor_type').setDisplayType('inline');
			subTierList.addField('start_date', 'date', 'Start Date');
			subTierList.addField('end_date', 'date', 'End Date');
			subTierList.addField('st_pay_rate', 'currency', 'ST Rate');
			// subTierList.addField('ot_pay_rate', 'currency', 'OT Rate');
			//var oldEndDate = nlapiGetLineItemValue(type, 'end_date', linenum);
			subTierList.addField('payment_term', 'select', 'Payment Term',
			        'term').setDisplayType('inline');
			subTierList.addField('rate_type', 'select', 'Rate Type',
			        'customlist_btpl_subtier_rate_type').setDisplayType(
			        'inline');
			subTierList.addField('new_start_date', 'date', 'New Start Date')
			        .setDisplayType('entry');
			subTierList.addField('new_end_date', 'date', 'New End Date')
			        .setDisplayType('entry');
			subTierList.addField('new_st_pay_rate', 'currency', 'New ST Rate')
			        .setDisplayType('entry');
			subTierList.addField('new_ot_pay_rate', 'currency', 'New OT Rate')
			        .setDisplayType('entry');
			// subTierList.addField('hire_date', 'date', 'Hire Date');
			// subTierList.addField('last_working_date', 'date',
			// 'Last Working Date');
			// subTierList.addField('subsidiary', 'select', 'Subsidiary',
			// 'subsidiary').setDisplayType('inline');

			subTierList.setLineItemValues(subTierData);
			form.addSubmitButton('Submit');
			response.writePage(form);
		} else {
			throw "Vendor Id Missing";
		}
	} catch (err) {
		nlapiLogExecution('ERROR', 'createExtensionScreen', err);
		throw err;
	}
}

function getMappedSubtierRecords(vendorId) {
	try {
		var latestSubTierSearch = nlapiSearchRecord(
		        'customrecord_subtier_vendor_data', null, [
		                new nlobjSearchFilter('isinactive', null, 'is', 'F'),
		                new nlobjSearchFilter('isinactive',
		                        'custrecord_stvd_vendor', 'is', 'F'),
		                new nlobjSearchFilter('isinactive',
		                        'custrecord_stvd_contractor', 'is', 'F'),
		                new nlobjSearchFilter('custrecord_stvd_vendor', null,
		                        'anyof', vendorId) ], [
		                new nlobjSearchColumn('custrecord_stvd_contractor',
		                        null, 'group'),
		                new nlobjSearchColumn('custrecord_stvd_end_date',null,'max') ]);

		var subTierSearch = nlapiSearchRecord(
		        'customrecord_subtier_vendor_data', null, [
		                new nlobjSearchFilter('isinactive', null, 'is', 'F'),
		                new nlobjSearchFilter('isinactive',
		                        'custrecord_stvd_vendor', 'is', 'F'),
		                new nlobjSearchFilter('custentity_employee_inactive',
		                        'custrecord_stvd_contractor', 'is', 'F'),
		                new nlobjSearchFilter('custrecord_stvd_vendor', null,
		                        'anyof', vendorId) ], [
		                new nlobjSearchColumn('custrecord_stvd_vendor_type'),
		                new nlobjSearchColumn('custrecord_stvd_contractor'),
		                new nlobjSearchColumn('custrecord_stvd_start_date'),
		                new nlobjSearchColumn('custrecord_stvd_end_date'),
		                new nlobjSearchColumn('custrecord_stvd_st_pay_rate'),
		                new nlobjSearchColumn('custrecord_stvd_ot_pay_rate'),
		                new nlobjSearchColumn('custrecord_stvd_payment_terms'),
		                new nlobjSearchColumn('custrecord_stvd_subsidiary'),
		                new nlobjSearchColumn('custrecord_stvd_rate_type'),
		                new nlobjSearchColumn(
		                        'custrecord_stvd_original_subtier'),
		                new nlobjSearchColumn('hiredate',
		                        'custrecord_stvd_contractor'),
		                new nlobjSearchColumn('custentity_lwd',
		                        'custrecord_stvd_contractor') ]);

		var subTierData = [];

		if (subTierSearch) {

			subTierSearch
			        .forEach(function(subTier) {
				        var endDate = subTier
				                .getValue('custrecord_stvd_end_date');
				        var consultant = subTier
				                .getValue('custrecord_stvd_contractor');

				        for (var i = 0; i < latestSubTierSearch.length; i++) {

					        if (latestSubTierSearch[i]
					                .getValue('custrecord_stvd_contractor',
					                        null, 'group') == consultant
					                && latestSubTierSearch[i].getValue(
					                        'custrecord_stvd_end_date', null,
					                        'max') == endDate) {
						        subTierData
						                .push({
						                    subtier : subTier.getId(),
						                   // original : subTier
						                        //    .getValue('custrecord_stvd_original_subtier'),
						                    employee : subTier
						                            .getValue('custrecord_stvd_contractor'),
						                    type : subTier
						                            .getValue('custrecord_stvd_vendor_type'),
						                    start_date : subTier
						                            .getValue('custrecord_stvd_start_date'),
						                    end_date : subTier
						                            .getValue('custrecord_stvd_end_date'),
						                    st_pay_rate : subTier
						                            .getValue('custrecord_stvd_st_pay_rate'),
						                    ot_pay_rate : subTier
						                            .getValue('custrecord_stvd_ot_pay_rate'),
						                    payment_term : subTier
						                            .getValue('custrecord_stvd_payment_terms'),
						                    subsidiary : subTier
						                            .getValue('custrecord_stvd_subsidiary'),
						                    rate_type : subTier
						                            .getValue('custrecord_stvd_rate_type'),
						                    new_st_pay_rate : subTier
						                            .getValue('custrecord_stvd_st_pay_rate'),
						                    new_ot_pay_rate : subTier
						                            .getValue('custrecord_stvd_ot_pay_rate'),
						                    hire_date : subTier
						                            .getValue('hiredate',
						                                    'custrecord_stvd_contractor'),
						                    last_working_date : subTier
						                            .getValue('custentity_lwd',
						                                    'custrecord_stvd_contractor')
						                });
						        break;
					        }
				        }
			        });

		}

		return subTierData;
	} catch (err) {
		nlapiLogExecution('ERROR', 'getMappedSubtierRecords', err);
		throw err;
	}
}

function submitExtensionScreen(request) {
	try {
		var vendorId = request.getParameter('custpage_vendor');
	
		if (vendorId) {
			var lineCount = request.getLineItemCount('custpage_subtier_list');
			var responseList = [];

			for (var linenum = 1; linenum <= lineCount; linenum++) {
				var existingSubtierRecordId = request.getLineItemValue(
				        'custpage_subtier_list', 'subtier', linenum);
				var newStartDate = request.getLineItemValue(
				        'custpage_subtier_list', 'new_start_date', linenum);
				var newEndDate = request.getLineItemValue(
				        'custpage_subtier_list', 'new_end_date', linenum);
				var contractorId = request.getLineItemValue(
				        'custpage_subtier_list', 'employee', linenum);
				var newStRate = request.getLineItemValue(
				        'custpage_subtier_list', 'new_st_pay_rate', linenum);
				var oldStRate = request.getLineItemValue(
				       'custpage_subtier_list', 'st_pay_rate', linenum);
				//var originalSubtierRecordId = request.getLineItemValue(
				   //     'custpage_subtier_list', 'original', linenum);

				//if (!originalSubtierRecordId) {
				//	originalSubtierRecordId = existingSubtierRecordId;
				//}
				
				// if (newStRate) {
				// newStRate = request.getLineItemValue(
				// 'custpage_subtier_list', 'st_pay_rate', linenum);
				// }

				var newOtRate = request.getLineItemValue(
				        'custpage_subtier_list', 'new_ot_pay_rate', linenum);

				// if (newOtRate) {
				// newOtRate = 0;
				// }
				
				var remarks = "";
//Sub-Line Item Creation Only When ST rate Changes
				if (newStartDate && newEndDate && newStRate) {

					try {
						if(newStRate == oldStRate)
						{
						var newSubtierRecord = nlapiLoadRecord(
						        'customrecord_subtier_vendor_data',
						        existingSubtierRecordId);
						}
						else
						{
							var newSubtierRecord = nlapiCopyRecord(
						        'customrecord_subtier_vendor_data',
						        existingSubtierRecordId);
                           newSubtierRecord.setFieldValue(
						        'custrecord_stvd_start_date', newStartDate);
						}
						newSubtierRecord.setFieldValue(
						        'custrecord_stvd_wo_number',
						        GenerateWorkOrderNumber(contractorId));
						newSubtierRecord.setFieldValue(
						        'custrecord_stvd_new_start_date', newStartDate);
						newSubtierRecord.setFieldValue(
						        'custrecord_stvd_end_date', newEndDate);
						newSubtierRecord.setFieldValue(
						        'custrecord_stvd_st_pay_rate', newStRate);
						newSubtierRecord.setFieldValue(
						        'custrecord_stvd_ot_pay_rate', newOtRate);
						//newSubtierRecord.setFieldValue(
						      //  'custrecord_stvd_original_subtier',
						      //  existingSubtierRecordId);
						newSubtierRecord.setFieldValue(
						        'custrecord_stvd_is_extension', 'T');
						newSubtierRecord.setFieldValue('custrecord_subtier_currency',6);
						var newSubtierRecordId = nlapiSubmitRecord(newSubtierRecord);
						nlapiLogExecution('debug',
						        'New Subtier Record Created',
						        newSubtierRecordId);
						remarks = 'Extended';
					} catch (er) {
						nlapiLogExecution('ERROR',
						        'Failed Creating Subtier Record', er);
						remarks = er;
					}

					responseList.push({
					    employee : contractorId,
					    startdate : newStartDate,
					    enddate : newEndDate,
					    remarks : remarks
					});
				}
				//nlapiLogExecution('DEBUG','Ot are same', oldStRate);
			}

			var form = nlapiCreateForm('Subtier Extension Form - Result ( Do not refresh this page )');
			form.setScript('customscript_cs_sut_subtier_extension');
			form.addButton('custpage_go_back', 'Go Back', 'GoBack()');
			form.addField('custpage_vendor', 'select', 'Vendor', 'vendor')
			        .setDisplayType('inline').setDefaultValue(vendorId);

			var subTierList = form.addSubList('custpage_subtier_list', 'list',
			        'Sub-tier Details');
			subTierList.addField('employee', 'select', 'Employee', 'employee')
			        .setDisplayType('inline');
			subTierList.addField('startdate', 'date', 'Start Date');
			subTierList.addField('enddate', 'date', 'End Date');
			subTierList.addField('remarks', 'text', 'Remarks');
			subTierList.setLineItemValues(responseList);
			response.writePage(form);
		} else {
			throw "Vendor Id Missing";
		}
	} catch (err) {
		nlapiLogExecution('ERROR', 'createExtensionScreen', err);
		throw err;
	}
}

function GenerateWorkOrderNumber(contractorId) {
	try {
		var fusionId = nlapiLookupField('employee', contractorId,
		        'custentity_fusion_empid');

		// get the no. of subtier for this employee
		var search = nlapiSearchRecord('customrecord_subtier_vendor_data',
		        null, [ new nlobjSearchFilter('custrecord_stvd_contractor',
		                null, 'anyof', contractorId) ]);

		var serialNo = 0;

		if (search) {
			serialNo = search.length;
		}

		serialNo += 1;

		// to make it 5 digits
		serialNo = ((100000 + serialNo).toString()).substring(1);
		nlapiLogExecution('ERROR', 'WO number',fusionId + "-" + serialNo );
		return fusionId + "-" + serialNo;
		
	} catch (err) {
		nlapiLogExecution('ERROR', 'GenerateWorkOrderNumber', err);
		throw err;
	}
}

// --------------------- Client Script

function GoBack() {
	var url = nlapiResolveURL('SUITELET', 'customscript_sut_subtier_extension',
	        'customdeploy_sut_subtier_extension');
	window.location = url;
}

function fieldChanged(type, name, linenum) {

	if (type == 'custpage_subtier_list') {

		if (name == 'new_start_date' || name == 'new_end_date') {
			var oldStartDate = nlapiGetLineItemValue(type, 'start_date',
			        linenum);
			var oldEndDate = nlapiGetLineItemValue(type, 'end_date', linenum);
			var d_oldEndDate = nlapiStringToDate(oldEndDate);
			var newEndDate = nlapiGetLineItemValue(type, 'new_end_date',
			        linenum);

			// if new start date is entered
			if (name == 'new_start_date') {
				var newStartDate = nlapiGetLineItemValue(type,
				        'new_start_date', linenum);

				if (newStartDate) {
					var d_newStartDate = nlapiStringToDate(newStartDate);

					// check if the new start date is after old end date
					if (d_newStartDate <= d_oldEndDate) {
						alert("Extension start date has to be after the previous end date");
						nlapiSetLineItemValue(type, name, linenum, '');
						return;
					}
					var gap=((d_newStartDate-d_oldEndDate)/(1000 * 60 * 60 * 24));
					if(gap	>1)
					{
						alert('Gap Between Allocation is not Allowed');
						nlapiSetLineItemValue(type, name, linenum, '');
						return false;
					}
				}
			}

			// if end date is entered
			else if (name == 'new_end_date') {
				var newEndDate = nlapiGetLineItemValue(type, 'new_end_date',
				        linenum);

				if (newEndDate) {
					var d_newEndDate = nlapiStringToDate(newEndDate);
					var newStartDate = nlapiGetLineItemValue(type,
					        'new_start_date', linenum);

					// start date has to be entered first
					if (newStartDate) {
						var d_newStartDate = nlapiStringToDate(newStartDate);

						// new end date has to be after new start date
						if (d_newEndDate <= d_newStartDate) {
							alert("Extension end date has to be after the extension start date");
							nlapiSetLineItemValue(type, name, linenum, '');
							return;
						}
					} else {
						alert("Enter Start Date First");
						nlapiSetLineItemValue(type, name, linenum, '');
						return;
					}
				}
			}
		}

		// if st rate or ot rate is entered
		else if (name == 'new_st_pay_rate' || name == 'new_ot_pay_rate') {
			var newStartDate = nlapiGetLineItemValue(type, 'new_start_date',
			        linenum);
			var newEndDate = nlapiGetLineItemValue(type, 'new_end_date',
			        linenum);
			var newStRate = nlapiGetLineItemValue(type, 'new_st_pay_rate',
			        linenum);

			if (newStartDate && newEndDate) {

				// if OT rate
				if (name == 'new_ot_pay_rate') {

					// ST rate has to be entered first
					if (newStRate) {

					} else {
						alert("Enter ST Pay Rate first");
						nlapiSetLineItemValue(type, name, linenum, '');
						return;
					}
				}
			} else {
				alert("Enter Start Date & End Date first");
				nlapiSetLineItemValue(type, name, linenum, '');
				return;
			}
		}
	}
}

function saveRecord() {
	var count = nlapiGetLineItemCount('custpage_subtier_list');

	var changes = 0;

	for (var linenum = 1; linenum <= count; linenum++) {
		var newStartDate = nlapiGetLineItemValue('custpage_subtier_list',
		        'new_start_date', linenum);
		var newEndDate = nlapiGetLineItemValue('custpage_subtier_list',
		        'new_end_date', linenum);
		var newStRate = nlapiGetLineItemValue('custpage_subtier_list',
		        'new_st_pay_rate', linenum);
		var newOtRate = nlapiGetLineItemValue('custpage_subtier_list',
		        'new_ot_pay_rate', linenum);

		// one of the data is entered
		if (newStartDate && newEndDate) {

			if (!newStRate) {
				alert("ST Rate not entered at line " + linenum);
				return false;
			} else {
				changes += 1;
			}
		}
	}

	if (changes > 0) {

		if (window.confirm('Submit ' + changes + ' extensions ?')) {
			return true;
		} else {
			return false;
		}
	} else {
		alert("No extension entered");
		return false;
	}
}