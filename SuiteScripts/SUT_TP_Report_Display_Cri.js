// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
/*
   	Script Name:
	Author:
	Company:
	Date:
	Description:


	Script Modification Log:

	-- Date --			-- Modified By --				--Requested By--				-- Description --



Below is a summary of the process controls enforced by this script file.  The control logic is described
more fully, below, in the appropriate function headers and code blocks.


     SUITELET
		- suiteletFunction(request, response)


     SUB-FUNCTIONS
		- The following sub-functions are called by the above core functions in order to maintain code
            modularization:

               - NOT USED

*/
}
// END SCRIPT DESCRIPTION BLOCK  ====================================



// BEGIN GLOBAL VARIABLE BLOCK  =====================================
{
	//  Initialize any Global Variables, in particular, debugging variables...




}
// END GLOBAL VARIABLE BLOCK  =======================================





// BEGIN SUITELET ==================================================

function suiteletFunction(request, response)
{

	/*  Suitelet:

          - EXPLAIN THE PURPOSE OF THIS FUNCTION

		-


		FIELDS USED:

          --Field Name--				--ID--


	*/



	//  LOCAL VARIABLES



    //  SUITELET CODE BODY
	nlapiLogExecution('DEBUG','Request Method', " " + request.getMethod());
		if(request.getMethod() == 'GET')
		{
			var form = nlapiCreateForm("Transfer Price Report");

		     var sysdate=new Date
			 var date1=nlapiDateToString(sysdate)

		    var fromdate = form.addField('fromdate', 'date', ' From Date');
			fromdate.setMandatory(true);
			fromdate.setDefaultValue(date1)

			var todate = form.addField('todate', 'date', ' To Date');
			todate.setMandatory(true);
			todate.setDefaultValue(date1)

			var fromsubsidiary = form.addField('fromsubsidiary', 'select', ' From Subsidiary','subsidiary');
			fromsubsidiary.setMandatory(true);
			//fromsubsidiary.setDefaultValue(date1)

			var tosubsidiary = form.addField('tosubsidiary', 'select', ' To Subsidiary','subsidiary');
			tosubsidiary.setMandatory(true);
			//tosubsidiary.setDefaultValue(date1)

			form.addSubmitButton('Show Report');
			response.writePage(form);
		}
		else if(request.getMethod() == 'POST')
		{
			var params = new Array();
			params['fromdate'] = request.getParameter('fromdate');
			params['todate']=request.getParameter('todate')
			params['fromsubsidiary']=request.getParameter('fromsubsidiary')
			params['tosubsidiary']=request.getParameter('tosubsidiary')
			nlapiSetRedirectURL('SUITELET','customscript_sut_tp_report_show', '1', false, params);
		}





}

// END SUITELET ====================================================




// BEGIN OBJECT CALLED/INVOKING FUNCTION ===================================================
{



}
// END OBJECT CALLED/INVOKING FUNCTION =====================================================
