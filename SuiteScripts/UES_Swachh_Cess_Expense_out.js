// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
/*
   	Script Name:UES_Swachh_Cess_expense_out.js
	Author:		Nikhil jain
	Company:	Aashna Cloudtech Pvt. Ltd.
	Date:		15/01/2016
	Description:post the swachh cess expense in same expense account


	Script Modification Log:

	-- Date --			-- Modified By --				--Requested By--				-- Description --
	      23 May 2016          Nikhil                                                       remove hardcodings of tax types  


Below is a summary of the process controls enforced by this script file.  The control logic is described
more fully, below, in the appropriate function headers and code blocks.


     BEFORE LOAD
		- beforeLoadRecord(type)



     BEFORE SUBMIT
		- beforeSubmitRecord(type)


     AFTER SUBMIT
		- afterSubmitRecord(type)



     SUB-FUNCTIONS
		- The following sub-functions are called by the above core functions in order to maintain code
            modularization:

               - NOT USED

*/
}
// END SCRIPT DESCRIPTION BLOCK  ====================================



// BEGIN GLOBAL VARIABLE BLOCK  =====================================
{
	//  Initialize any Global Variables, in particular, debugging variables...




}
// END GLOBAL VARIABLE BLOCK  =======================================





// BEGIN BEFORE LOAD ==================================================

function beforeLoadRecord(type)
{

	/*  On before load:

          - EXPLAIN THE PURPOSE OF THIS FUNCTION

		-


		FIELDS USED:

          --Field Name--				--ID--


	*/



	//  LOCAL VARIABLES

    //  BEFORE LOAD CODE BODY


	return true;



}

// END BEFORE LOAD ====================================================





// BEGIN BEFORE SUBMIT ================================================

function beforeSubmitRecord_swachh_cess_expense(type){
	/*  On before submit:
	 - PURPOSE
	 -
	 FIELDS USED:
	 --Field Name--				--ID--
	 */
	//  LOCAL VARIABLES
	
	
	//  BEFORE SUBMIT CODE BODY
	
	try {
		if (type != 'delete') {
		
			var a_array = new Array();
			var a_taxcode = {};
			var a_subisidiary = new Array()
			var s_pass_code;
			var i_AitGlobalRecId = SearchGlobalParameter();
			var a_st_details = new Array();
			var st_details_obj = {};
			var st_serachRes;
			if (i_AitGlobalRecId != 0) {
				var o_AitGloRec = nlapiLoadRecord('customrecord_indian_tax_glo_parameter', i_AitGlobalRecId);
				
				a_subisidiary = o_AitGloRec.getFieldValues('custrecord_ait_subsidiary');
				
				var i_vatCode = o_AitGloRec.getFieldValue('custrecord_ait_vat_code');
				
				var i_taxcode = o_AitGloRec.getFieldValue('custrecord_ait_taxcode');
				
				var stRoundMethod = o_AitGloRec.getFieldValue('custrecord_ait_st_roundoff');
				
				s_pass_code = o_AitGloRec.getFieldValue('custrecord_encpwdstring');
				
				//================================Begin:- Code to Get the Service tax Details=============================//
				
				var st_filters = new Array();
				var st_columns = new Array();
				
				st_filters.push(new nlobjSearchFilter('custrecord_ait_service_tax_global_link', null, 'anyOf', i_AitGlobalRecId))
				st_filters.push(new nlobjSearchFilter('custrecord_ait_service_expense_out', null, 'is', 'T'))
				st_filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'))
				
				st_columns.push(new nlobjSearchColumn('custrecord_ait_service_tax_type'))
				st_columns.push(new nlobjSearchColumn('custrecord_ait_service_tax_rate'))
				st_columns.push(new nlobjSearchColumn('custrecord_ait_service_expense_out'))
				
				var st_serachRes = nlapiSearchRecord('customrecord_ait_service_tax_details', null, st_filters, st_columns)
				
				if (st_serachRes != null && st_serachRes != '' && st_serachRes != undefined) {
					for (var t = 0; t < st_serachRes.length; t++) {
						var st_taxtype = st_serachRes[t].getValue('custrecord_ait_service_tax_type')
						var st_rate = st_serachRes[t].getValue('custrecord_ait_service_tax_rate')
						var st_expense = st_serachRes[t].getValue('custrecord_ait_service_expense_out')
						
						st_details_obj[st_taxtype] = st_rate + '@' + st_expense
						
						a_st_details.push(st_details_obj);
					}
				}
				
			//===============================End:- Code to Get the Service tax Details=============================//
			}
			
			var Flag = 0;
			var i_subsidiary = nlapiGetFieldValue('subsidiary');
			var Flag = isindia_subsidiary(a_subisidiary, s_pass_code, i_subsidiary)
			
			if (Flag == 1) 
			{
				var exLinecount = nlapiGetLineItemCount('expense')
				nlapiLogExecution('DEBUG', 'Bill After submit', "exLinecount->" + exLinecount)
				//=== ITERATE THROUGH EVERY EXPENSE LINE ===
				for (var l = exLinecount; l >= 1; l--) 
				{
					var isReversetrue = nlapiGetLineItemValue('expense', 'custcol_ait_swachh_cess_reverse', l)
					nlapiLogExecution('DEBUG', 'Bill After submit', "swachh cess isReversetrue->" + isReversetrue)
					// ===== IF IS REVERES IS TRUE THEN REMOVE THE LINE ====
					if (isReversetrue == 'T') 
					{
						nlapiSelectLineItem('expense', l)
						nlapiRemoveLineItem('expense', l)
						
					} // END if(isReversetrue=='T')
				}
				
				var context = nlapiGetContext();
				var usage = context.getRemainingUsage();
				nlapiLogExecution('DEBUG', 'ven bill details ', 'Before process ---> ' + usage)
				var exp_line_count = nlapiGetLineItemCount('expense');
				
				for (var j = 1; j <= exp_line_count; j++) 
				{
					var isReversetrue = nlapiGetLineItemValue('expense', 'custcol_st_reverseline', j)
					nlapiLogExecution('DEBUG', 'Bill After submit', "isReversetrue->" + isReversetrue)
					// ===== IF IS REVERES IS TRUE THEN REMOVE THE LINE ====
					if (isReversetrue != 'T') 
					{
						var amount = nlapiGetLineItemValue('expense', 'amount', j)
						
						var i_Line_taxamount = nlapiGetLineItemValue('expense', 'tax1amt', j)
						
						var i_line_taxRate = nlapiGetLineItemValue('expense', 'taxrate1', j)
						
						if (parseFloat(i_line_taxRate) != parseFloat(0)) 
						{
							var i_exp_acc = nlapiGetLineItemValue('expense', 'account', j)
							
							var taxcode = nlapiGetLineItemValue('expense', 'taxcode', j)
							
							var location1 = nlapiGetLineItemValue('expense', 'location', j);
							nlapiLogExecution('DEBUG', 'Bill After submit', 'location1->' + location1)
							
							var department1 = nlapiGetLineItemValue('expense', 'department', j);
							nlapiLogExecution('DEBUG', 'Bill After submit', 'department1->' + department1)
							
							var class1 = nlapiGetLineItemValue('expense', 'class', j);
							nlapiLogExecution('DEBUG', 'Bill After submit', 'class1->' + class1)
							
							if (taxcode in a_taxcode) 
							{
								var s_taxAccounts = a_taxcode[taxcode]
								
								for (var i = 0; i < s_taxAccounts.length; i = 2 + parseInt(i)) 
								{
									//nlapiLogExecution('DEBUG', 'Bill After submit', "taxAccounts->" + s_taxAccounts[i])
									//nlapiLogExecution('DEBUG', 'Bill After submit', "tax rate->" + s_taxAccounts[i + 1])
									
									var i_purchase_Tax_acc = s_taxAccounts[i];
									
									var f_rate = s_taxAccounts[i + 1];
									
									var taxAmount = parseFloat(amount) * (parseFloat(f_rate) / 100)
									taxAmount = applySTRoundMethod(stRoundMethod, taxAmount, i_Line_taxamount, i_line_taxRate, f_rate, amount)
									nlapiSelectNewLineItem('expense');
									nlapiSetCurrentLineItemValue('expense', 'account', i_exp_acc);
									nlapiSetCurrentLineItemValue('expense', 'amount', taxAmount);
									nlapiSetCurrentLineItemValue('expense', 'taxcode', i_taxcode);
									nlapiSetCurrentLineItemValue('expense', 'custcol_ait_swachh_cess_reverse', 'T');
									nlapiSetCurrentLineItemValue('expense', 'location', location1);
									nlapiSetCurrentLineItemValue('expense', 'department', department1);
									nlapiSetCurrentLineItemValue('expense', 'class', class1);
									nlapiSetCurrentLineItemValue('expense', 'memo', 'SBC');
									nlapiCommitLineItem('expense');
									
									nlapiSelectNewLineItem('expense');
									nlapiSetCurrentLineItemValue('expense', 'account', i_purchase_Tax_acc);
									nlapiSetCurrentLineItemValue('expense', 'amount', -taxAmount);
									nlapiSetCurrentLineItemValue('expense', 'taxcode', i_taxcode);
									nlapiSetCurrentLineItemValue('expense', 'custcol_ait_swachh_cess_reverse', 'T');
									
									nlapiSetCurrentLineItemValue('expense', 'location', location1);
									nlapiSetCurrentLineItemValue('expense', 'department', department1);
									nlapiSetCurrentLineItemValue('expense', 'class', class1);
									nlapiSetCurrentLineItemValue('expense', 'memo', 'SBC');
									nlapiCommitLineItem('expense');
								}
							}
							else 
							{
								var s_taxAccounts = getTaxaccount(taxcode, st_details_obj, a_st_details, st_serachRes);
								
								if (s_taxAccounts != null && s_taxAccounts != '' && s_taxAccounts != undefined) {
									a_taxcode[taxcode] = s_taxAccounts
									
									a_array.push(a_taxcode);
									
									for (var i = 0; i < s_taxAccounts.length; i = 2 + parseInt(i)) 
									{
										nlapiLogExecution('DEBUG', 'Bill After submit', "taxAccounts->" + s_taxAccounts[i])
										nlapiLogExecution('DEBUG', 'Bill After submit', "tax rate->" + s_taxAccounts[i + 1])
										
										var i_purchase_Tax_acc = s_taxAccounts[i];
										
										var f_rate = s_taxAccounts[i + 1];
										
										var taxAmount = parseFloat(amount) * (parseFloat(f_rate) / 100)
										taxAmount = applySTRoundMethod(stRoundMethod, taxAmount, i_Line_taxamount, i_line_taxRate, f_rate, amount)
										nlapiSelectNewLineItem('expense');
										nlapiSetCurrentLineItemValue('expense', 'account', i_exp_acc);
										nlapiSetCurrentLineItemValue('expense', 'amount', taxAmount);
										nlapiSetCurrentLineItemValue('expense', 'taxcode', i_taxcode);
										nlapiSetCurrentLineItemValue('expense', 'custcol_ait_swachh_cess_reverse', 'T');
										nlapiSetCurrentLineItemValue('expense', 'location', location1);
										nlapiSetCurrentLineItemValue('expense', 'department', department1);
										nlapiSetCurrentLineItemValue('expense', 'class', class1);
										nlapiSetCurrentLineItemValue('expense', 'memo', 'SBC');
										nlapiCommitLineItem('expense');
										
										nlapiSelectNewLineItem('expense');
										nlapiSetCurrentLineItemValue('expense', 'account', i_purchase_Tax_acc);
										nlapiSetCurrentLineItemValue('expense', 'amount', -taxAmount);
										nlapiSetCurrentLineItemValue('expense', 'taxcode', i_taxcode);
										nlapiSetCurrentLineItemValue('expense', 'custcol_ait_swachh_cess_reverse', 'T');
										
										nlapiSetCurrentLineItemValue('expense', 'location', location1);
										nlapiSetCurrentLineItemValue('expense', 'department', department1);
										nlapiSetCurrentLineItemValue('expense', 'class', class1);
										nlapiSetCurrentLineItemValue('expense', 'memo', 'SBC');
										nlapiCommitLineItem('expense');
									}
								}
							}
						}
					}
				}
				
				var i_itemLinecount = nlapiGetLineItemCount('item')
				
				//=== ITERATE THROUGHT EVERY Item LINE ===
				for (var p = 1; p <= i_itemLinecount; p++) {
				
					var amount = nlapiGetLineItemValue('item', 'amount', p)
					
					
					var i_exp_acc = nlapiGetLineItemValue('item', 'custcol_ait_expense_acc', p)
					
					if (i_exp_acc != null && i_exp_acc != '' && i_exp_acc != undefined) {
						var taxcode = nlapiGetLineItemValue('item', 'taxcode', p)
						
						var i_Line_taxamount = nlapiGetLineItemValue('item', 'tax1amt', p)
						
						var i_line_taxRate = nlapiGetLineItemValue('item', 'taxrate1', p)
						
						if (parseFloat(i_line_taxRate) != parseFloat(0)) {
							var location1 = nlapiGetLineItemValue('item', 'location', p);
							//nlapiLogExecution('DEBUG', 'Bill After submit', 'location1->' + location1)
							
							var department1 = nlapiGetLineItemValue('item', 'department', p);
							//nlapiLogExecution('DEBUG', 'Bill After submit', 'department1->' + department1)
							
							var class1 = nlapiGetLineItemValue('item', 'class', p);
							//nlapiLogExecution('DEBUG', 'Bill After submit', 'class1->' + class1)
							
							
							if (taxcode in a_taxcode) 
							{
								var s_taxAccounts = a_taxcode[taxcode]
								
								for (var i = 0; i < s_taxAccounts.length; i = 2 + parseInt(i)) {
									//nlapiLogExecution('DEBUG', 'Bill After submit', "taxAccounts->" + s_taxAccounts[i])
									//nlapiLogExecution('DEBUG', 'Bill After submit', "tax rate->" + s_taxAccounts[i + 1])
									
									var i_purchase_Tax_acc = s_taxAccounts[i];
									
									var f_rate = s_taxAccounts[i + 1];
									
									var taxAmount = parseFloat(amount) * (parseFloat(f_rate) / 100)
									taxAmount = applySTRoundMethod(stRoundMethod, taxAmount, i_Line_taxamount, i_line_taxRate, f_rate, amount)
									nlapiSelectNewLineItem('expense');
									nlapiSetCurrentLineItemValue('expense', 'account', i_exp_acc);
									nlapiSetCurrentLineItemValue('expense', 'amount', taxAmount);
									nlapiSetCurrentLineItemValue('expense', 'taxcode', i_taxcode);
									nlapiSetCurrentLineItemValue('expense', 'custcol_ait_swachh_cess_reverse', 'T');
									nlapiSetCurrentLineItemValue('expense', 'location', location1);
									nlapiSetCurrentLineItemValue('expense', 'department', department1);
									nlapiSetCurrentLineItemValue('expense', 'class', class1);
									nlapiSetCurrentLineItemValue('expense', 'memo', 'SBC');
									nlapiCommitLineItem('expense');
									
									nlapiSelectNewLineItem('expense');
									nlapiSetCurrentLineItemValue('expense', 'account', i_purchase_Tax_acc);
									nlapiSetCurrentLineItemValue('expense', 'amount', -taxAmount);
									nlapiSetCurrentLineItemValue('expense', 'taxcode', i_taxcode);
									nlapiSetCurrentLineItemValue('expense', 'custcol_ait_swachh_cess_reverse', 'T');
									
									nlapiSetCurrentLineItemValue('expense', 'location', location1);
									nlapiSetCurrentLineItemValue('expense', 'department', department1);
									nlapiSetCurrentLineItemValue('expense', 'class', class1);
									nlapiSetCurrentLineItemValue('expense', 'memo', 'SBC');
									nlapiCommitLineItem('expense');
								}
							}
							else {
								var s_taxAccounts = getTaxaccount(taxcode, st_details_obj, a_st_details, st_serachRes);
								
								if (s_taxAccounts != null && s_taxAccounts != '' && s_taxAccounts != undefined) {
									a_taxcode[taxcode] = s_taxAccounts
									
									a_array.push(a_taxcode);
									
									for (var i = 0; i < s_taxAccounts.length; i = 2 + parseInt(i)) {
										//nlapiLogExecution('DEBUG', 'Bill After submit', "taxAccounts->" + s_taxAccounts[i])
										//nlapiLogExecution('DEBUG', 'Bill After submit', "tax rate->" + s_taxAccounts[i + 1])
										
										var i_purchase_Tax_acc = s_taxAccounts[i];
										
										var f_rate = s_taxAccounts[i + 1];
										
										var taxAmount = parseFloat(amount) * (parseFloat(f_rate) / 100)
										taxAmount = applySTRoundMethod(stRoundMethod, taxAmount, i_Line_taxamount, i_line_taxRate, f_rate, amount)
										nlapiSelectNewLineItem('expense');
										nlapiSetCurrentLineItemValue('expense', 'account', i_exp_acc);
										nlapiSetCurrentLineItemValue('expense', 'amount', taxAmount);
										nlapiSetCurrentLineItemValue('expense', 'taxcode', i_taxcode);
										nlapiSetCurrentLineItemValue('expense', 'custcol_ait_swachh_cess_reverse', 'T');
										nlapiSetCurrentLineItemValue('expense', 'location', location1);
										nlapiSetCurrentLineItemValue('expense', 'department', department1);
										nlapiSetCurrentLineItemValue('expense', 'class', class1);
										nlapiSetCurrentLineItemValue('expense', 'memo', 'SBC');
										nlapiCommitLineItem('expense');
										
										nlapiSelectNewLineItem('expense');
										nlapiSetCurrentLineItemValue('expense', 'account', i_purchase_Tax_acc);
										nlapiSetCurrentLineItemValue('expense', 'amount', -taxAmount);
										nlapiSetCurrentLineItemValue('expense', 'taxcode', i_taxcode);
										nlapiSetCurrentLineItemValue('expense', 'custcol_ait_swachh_cess_reverse', 'T');
										
										nlapiSetCurrentLineItemValue('expense', 'location', location1);
										nlapiSetCurrentLineItemValue('expense', 'department', department1);
										nlapiSetCurrentLineItemValue('expense', 'class', class1);
										nlapiSetCurrentLineItemValue('expense', 'memo', 'SBC');
										nlapiCommitLineItem('expense');
									}
								}
							}
						}
					}
				}
			}
		//var usage = context.getRemainingUsage();
		//nlapiLogExecution('DEBUG', 'ven bill details ', 'Before process ---> ' + usage)
		}
	} 
	catch (exception) {
		nlapiLogExecution('DEBUG', 'Bill After submit', "exception->" + exception)
	}
	return true;
}

// END BEFORE SUBMIT ==================================================





// BEGIN AFTER SUBMIT =============================================

function afterSubmitRecord(type)
{
    /*  On after submit:

          - PURPOSE
		-

	FIELDS USED:

          --Field Name--				--ID--


    */


	//  LOCAL VARIABLES
	

	//  AFTER SUBMIT CODE BODY

	return true;

}

// END AFTER SUBMIT ===============================================





// BEGIN FUNCTION ===================================================
function getTaxaccount(taxcode, st_details_obj, a_st_details,st_serachRes)
{
	var taxAccountarray = new Array();
		
	try 
	{
		if (st_serachRes != null && st_serachRes != '' && st_serachRes != undefined) 
		{
			var taxgrpobj = nlapiLoadRecord('taxgroup', taxcode) //Load Tax group
			var taxgrplineitemcount = taxgrpobj.getLineItemCount('taxitem'); //Get line item on tax group
			nlapiLogExecution('DEBUG', 'getTaxaccount', "taxgrplineitemcount" + taxgrplineitemcount)
			
			var p = 0
			
			for (var l = 1; l <= taxgrplineitemcount; l++) 
			{
				var taxname = taxgrpobj.getLineItemValue('taxitem', 'taxname', l) //Get Tax name
				nlapiLogExecution('DEBUG', 'getTaxaccount ', "taxname->" + taxname)
				
				if (taxname != null && taxname != '' && taxname != undefined) 
				{
					var codeobj = nlapiLoadRecord('salestaxitem', taxname) //Get tax code object
					var taxtype = codeobj.getFieldValue('taxtype') //Get account code
					//nlapiLogExecution('DEBUG', 'getTaxaccount', "taxtype->" + taxtype)
					
					if (taxtype in st_details_obj) 
					{
						var accountcode = codeobj.getFieldValue('purchaseaccount') //Get account code
					//	nlapiLogExecution('DEBUG', 'getTaxaccount', "accountcode->" + accountcode)
						
						var rate = codeobj.getFieldValue('rate') //Get account code
						//nlapiLogExecution('DEBUG', 'getTaxaccount', "rate->" + rate)
						
						taxAccountarray[p++] = accountcode
						taxAccountarray[p++] = rate
					}
					
				} // END if (taxname != null)
			} // END for (var j = 1; j <= taxgrplineitemcount; j++)
		}
	} 
	catch (e) {
		nlapiLogExecution('DEBUG', 'getTaxaccount', "Not a tax Group")
	}
	return taxAccountarray
}// END getTaxaccount(taxcode)
 
	 function SearchGlobalParameter(){
	 
	 	var a_filters = new Array();
	 	var a_column = new Array();
	 	var i_globalRecId = 0;
	 	
	 	a_column.push(new nlobjSearchColumn('internalid'));
	 	
	 	var s_serchResult = nlapiSearchRecord('customrecord_indian_tax_glo_parameter', null, null, a_column)
	 	
	 	if (s_serchResult != null && s_serchResult != undefined && s_serchResult != '') {
	 		for (var i = 0; i < s_serchResult.length; i++) {
	 			i_globalRecId = s_serchResult[0].getValue('internalid');
	 			nlapiLogExecution('DEBUG', 'Bill ', "i_globalRecId" + i_globalRecId);
	 			
	 		}
	 		
	 	}
	 	
	 	
	 	return i_globalRecId;
	 }
	function applySTRoundMethod(stRoundMethod, taxamount,i_Line_taxamount,i_line_taxRate,f_rate,amount)
	{
		var roundedSTAmount = taxamount;
		
		if (stRoundMethod == 2) 
		{
			//roundedSTAmount = Math.round(taxamount)
			//roundedSTAmount = Math.round(taxamount)
			var actualTaxAmt = (parseFloat(amount) * (parseFloat(i_line_taxRate) / 100))
			
			var difference = (parseFloat(i_Line_taxamount) - parseFloat(actualTaxAmt))
			nlapiLogExecution('DEBUG', 'Bill ', "difference" + difference);
			
			var prorateDiffernce = (parseFloat(f_rate) / parseFloat(i_line_taxRate))
			nlapiLogExecution('DEBUG', 'Bill ', "prorateDiffernce" + prorateDiffernce);
			
			var actualDifference = (parseFloat(prorateDiffernce) * parseFloat((difference)))
			nlapiLogExecution('DEBUG', 'Bill ', "actualDifference" + actualDifference);
			
			roundedSTAmount = (parseFloat(roundedSTAmount) + parseFloat(actualDifference))
			nlapiLogExecution('DEBUG', 'Bill ', "roundedSTAmount" + roundedSTAmount);
			
			roundedSTAmount = roundedSTAmount.toFixed(2)
			nlapiLogExecution('DEBUG', 'Bill ', "roundedSTAmount" + roundedSTAmount);
		}
		if (stRoundMethod == 3) 
		{
			roundedSTAmount = Math.round(taxamount / 10) * 10;
		}
		if (stRoundMethod == 4) 
		{
			roundedSTAmount = Math.round(taxamount / 100) * 100;
		}
		
		return roundedSTAmount;
	}

		
// END FUNCTION =====================================================
