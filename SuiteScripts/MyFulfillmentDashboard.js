/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       06 Mar 2019     Aazamali Khan
 *
 */

/**
 * @param {Object} dataIn Parameter object
 * @returns {Object} Output object
 */
var projectCounter = 0;
var counter = 1;
var a_JSON = [];
var finalJSON = {};
var a_proj_array = new Array();
var a_proj_array_filter = new Array();
var a_sfdc_projects = [];
var total_frf = 0;
var total_rrf = 0;

function getRESTlet(dataIn) {
 // User from API
 var i_user = dataIn.user;
 // Get Logged In user's executing practice
 var spocSearch = GetPracticeSPOC(i_user);
 if (spocSearch) {
  var i_practice = spocSearch[0].getValue('custrecord_practice');
  // Create JSON for dashboard table when logged in user in practice SPOC
  GetDashBoardDataPractice(i_practice);
  GetDashboardProjectManager(i_user);
 } else {
  // Create JSON for dashboard when logged in user is project manager
  GetDashboardProjectManager(i_user);
 }
 finalJSON.totalfrf = total_frf;
 finalJSON.totalrrf = total_rrf;
 finalJSON.totalprojects = projectCounter;
 finalJSON.tableData = a_proj_array;
 return finalJSON;
}

function GetDashBoardDataPractice(practiceId) {
 var searchResult = nlapiSearchRecord("customrecord_fulfillment_dashboard_data", null, [
  ["custrecord_fulfill_dashboard_practice", "anyof", practiceId],
  "AND", ["custrecord_fulfill_dashboard_opp_id", "noneof", "@NONE@"],
  "AND", ["isinactive", "is", "F"]
 ], [
  new nlobjSearchColumn("scriptid"),
  new nlobjSearchColumn("custrecord_fulfill_dashboard_status"),
  new nlobjSearchColumn("custrecord_fulfill_dashboard_project"),
  new nlobjSearchColumn("custrecord_fulfill_dashboard_account"),
  new nlobjSearchColumn("custrecord_fulfill_dashboard_rev_sta_pro"),
  new nlobjSearchColumn("custrecord_fulfill_dashboard_reve_confid"),
  new nlobjSearchColumn("custrecord_fulfill_dashboard_manager"),
  new nlobjSearchColumn("custrecord_fulfill_dashboard_practice"),
  new nlobjSearchColumn("custrecord_fulfill_dashboard_pro_team"),
  new nlobjSearchColumn("custrecord_fulfill_dashboard_rampup"),
  new nlobjSearchColumn("custrecord_fulfill_dashboard_exiting"),
  new nlobjSearchColumn("custrecord_fulfill_dashboard_frf").setSort(false),
  new nlobjSearchColumn("custrecord_fulfill_dashboard_rrf"),
  new nlobjSearchColumn("custrecord_fulfill_dashboard_track"),
  new nlobjSearchColumn("custrecord_fulfill_dashboard_opp_id"),
  new nlobjSearchColumn("custrecord_fulfill_dashboard_last_update"),
  new nlobjSearchColumn("custrecord_fulfill_dashboard_start_date"),
  new nlobjSearchColumn("custrecord_opportunity_name_sfdc", "CUSTRECORD_FULFILL_DASHBOARD_OPP_ID", null),
  new nlobjSearchColumn("custrecord_customer_internal_id_sfdc", "CUSTRECORD_FULFILL_DASHBOARD_OPP_ID", null)
 ]);
 if (searchResult) {
  for (var int = 0; int < searchResult.length; int++) {
   /*var n_frf;
   if (searchResult[int].getValue('custrecord_fulfill_dashboard_project')) {
   	var projId = searchResult[int].getValue('custrecord_fulfill_dashboard_project');
   	var oppId = searchResult[int].getValue('custrecord_fulfill_dashboard_opp_id');
   	var s_project_link = '<span class="project_name" title="' +searchResult[int].getText('custrecord_fulfill_dashboard_project')+ '"><a class="project_link" href="'+u_peopleView+'&custpage_projectid='+projId+'&custpage_oppid='+oppId+'">'+searchResult[int].getText('custrecord_fulfill_dashboard_project')+'</a></span>';
   	nlapiLogExecution("AUDIT", "projectName:"+searchResult[int].getValue("custrecord_opportunity_name_sfdc","CUSTRECORD_FULFILL_DASHBOARD_OPP_ID",null), "number of frf : "+ searchResult[int].getValue('custrecord_fulfill_dashboard_frf'));
   	if (searchResult[int].getValue('custrecord_fulfill_dashboard_frf') == "0" && searchResult[int].getValue('custrecord_fulfill_dashboard_rrf') == "0" ) {
   		n_frf = '<a class="prev-page" href="'+u_createFRF+'&s_project='+projId+'&s_opp='+oppId+'"><button class="btn create-frf-btn">Plan</button> </a>';
   	}else{
   		n_frf = searchResult[int].getValue('custrecord_fulfill_dashboard_frf');
   	}
   }else{
   	var oppId = searchResult[int].getValue('custrecord_fulfill_dashboard_opp_id');
   	var projId = searchResult[int].getValue('custrecord_fulfill_dashboard_project');
   	var s_project_link = '<span class="project_name" title="' +searchResult[int].getValue("custrecord_opportunity_name_sfdc","CUSTRECORD_FULFILL_DASHBOARD_OPP_ID",null)+ '"><a class="project_link" href="'+u_peopleView+'&custpage_projectid='+projId+'&custpage_oppid='+oppId+'">'+searchResult[int].getValue("custrecord_opportunity_name_sfdc","CUSTRECORD_FULFILL_DASHBOARD_OPP_ID",null)+'</a></span>';
   	nlapiLogExecution("AUDIT", "projectName:"+searchResult[int].getValue("custrecord_opportunity_name_sfdc","CUSTRECORD_FULFILL_DASHBOARD_OPP_ID",null), "number of frf : "+ searchResult[int].getValue('custrecord_fulfill_dashboard_frf'))
   	if (searchResult[int].getValue('custrecord_fulfill_dashboard_frf') == "0" && searchResult[int].getValue('custrecord_fulfill_dashboard_rrf') == "0" ) {
   		n_frf = '<a class="prev-page" href="'+u_createFRF+'&s_project='+projId+'&s_opp='+oppId+'"><button class="btn create-frf-btn">Plan</button> </a>';
   	}else{
   		n_frf = searchResult[int].getValue('custrecord_fulfill_dashboard_frf');
   	}
   }
			
   var s_startdate = searchResult[int].getValue('custrecord_fulfill_dashboard_start_date');
   s_startdate ='<p class="start_date">'+s_startdate+'</p>';
   var s_team = '<p class="team_size">'+Math.round(searchResult[int].getValue('custrecord_fulfill_dashboard_pro_team'))+'</p>';
   var s_pm = '<span class="project_name exec_pract" title="' +searchResult[int].getValue('custrecord_fulfill_dashboard_manager')+ '">'+searchResult[int].getValue('custrecord_fulfill_dashboard_manager')+'</span>';
   var s_exec_practice = '<span class="project_name" title="' +searchResult[int].getText('custrecord_fulfill_dashboard_practice')+ '">'+searchResult[int].getText('custrecord_fulfill_dashboard_practice')+'</span>';
   // Following JSON for render 
   a_JSON = {
   		// pr_status: counter,In next MVP
   		project_account: s_project_link + s_projName,//searchResult[int].getValue("custrecord_opportunity_name_sfdc","CUSTRECORD_FULFILL_DASHBOARD_OPP_ID",null),
   		rev_status: searchResult[int].getValue('custrecord_fulfill_dashboard_rev_sta_pro')+'<br>'+searchResult[int].getValue('custrecord_fulfill_dashboard_reve_confid')+"%",
   		pm_practice: s_pm+'<br>'+s_exec_practice,
   		start_date: calender+s_startdate ,
   		team: team + s_team,
   		ramp_up: rampup + searchResult[int].getValue('custrecord_fulfill_dashboard_rampup'),
   		exiting:exiting + Math.round(searchResult[int].getValue('custrecord_fulfill_dashboard_exiting')),
   		frf:n_frf,
   		rrf:searchResult[int].getValue('custrecord_fulfill_dashboard_rrf'),
   		star:star
   }
   total_frf = total_frf + parseInt(searchResult[int].getValue('custrecord_fulfill_dashboard_frf'));
   total_rrf = total_rrf + parseInt(searchResult[int].getValue('custrecord_fulfill_dashboard_rrf'));
   a_proj_array.push(a_JSON);
   projectCounter++;
   counter++;*/
   var s_project;
   var i_project = 0;
   var i_oppID = 0;
   if (searchResult[int].getValue('custrecord_fulfill_dashboard_project')) {
    s_project = searchResult[int].getText('custrecord_fulfill_dashboard_project');
    i_project = searchResult[int].getValue('custrecord_fulfill_dashboard_project');
   } else {
    s_project = searchResult[int].getValue("custrecord_opportunity_name_sfdc", "CUSTRECORD_FULFILL_DASHBOARD_OPP_ID", null);
    i_oppID = searchResult[int].getValue("custrecord_customer_internal_id_sfdc", "CUSTRECORD_FULFILL_DASHBOARD_OPP_ID", null);
   }
   a_JSON = {
    project: s_project, //searchResult[int].getValue("custrecord_opportunity_name_sfdc","CUSTRECORD_FULFILL_DASHBOARD_OPP_ID",null),
    projectID: i_project,
    i_oppID: i_oppID,
    account: searchResult[int].getValue("custrecord_fulfill_dashboard_account"),
    revstatus: searchResult[int].getValue("custrecord_fulfill_dashboard_reve_confid"),
    practice: searchResult[int].getText("custrecord_fulfill_dashboard_practice"),
    teamsize: searchResult[int].getValue("custrecord_fulfill_dashboard_pro_team"),
    frf: searchResult[int].getValue("custrecord_fulfill_dashboard_frf"),
    rrf: searchResult[int].getValue("custrecord_fulfill_dashboard_rrf"),
    startdate: searchResult[int].getValue("custrecord_fulfill_dashboard_start_date"),
    manager: searchResult[int].getValue("custrecord_fulfill_dashboard_manager")
   }
   total_frf = total_frf + parseInt(searchResult[int].getValue('custrecord_fulfill_dashboard_frf'));
   total_rrf = total_rrf + parseInt(searchResult[int].getValue('custrecord_fulfill_dashboard_rrf'));
   projectCounter++;
   a_proj_array.push(a_JSON);
  }
 }
}

function GetDashboardProjectManager(i_user) {
 var searchResult = nlapiSearchRecord("customrecord_fulfillment_dashboard_data", null, [
  [
   ["custrecord_fulfill_dashboard_project.custentity_projectmanager", "anyof", i_user],
   "OR", ["custrecord_fulfill_dashboard_project.custentity_deliverymanager", "anyof", i_user]
  ],
  "AND", ["isinactive", "is", "F"]
 ], [
  new nlobjSearchColumn("scriptid").setSort(false),
  new nlobjSearchColumn("custrecord_fulfill_dashboard_status"),
  new nlobjSearchColumn("custrecord_fulfill_dashboard_project"),
  new nlobjSearchColumn("custrecord_fulfill_dashboard_account"),
  new nlobjSearchColumn("custrecord_fulfill_dashboard_rev_sta_pro"),
  new nlobjSearchColumn("custrecord_fulfill_dashboard_reve_confid"),
  new nlobjSearchColumn("custrecord_fulfill_dashboard_manager"),
  new nlobjSearchColumn("custrecord_fulfill_dashboard_practice"),
  new nlobjSearchColumn("custrecord_fulfill_dashboard_pro_team"),
  new nlobjSearchColumn("custrecord_fulfill_dashboard_rampup"),
  new nlobjSearchColumn("custrecord_fulfill_dashboard_exiting"),
  new nlobjSearchColumn("custrecord_fulfill_dashboard_frf"),
  new nlobjSearchColumn("custrecord_fulfill_dashboard_rrf"),
  new nlobjSearchColumn("custrecord_fulfill_dashboard_track"),
  new nlobjSearchColumn("custrecord_fulfill_dashboard_opp_id"),
  new nlobjSearchColumn("custrecord_fulfill_dashboard_last_update"),
  new nlobjSearchColumn("custrecord_fulfill_dashboard_start_date"),
  new nlobjSearchColumn("altname", "CUSTRECORD_FULFILL_DASHBOARD_PROJECT", null)
 ]);
 if (searchResult) {
  for (var int = 0; int < searchResult.length; int++) {
   /*var projId = searchResult[int].getValue('custrecord_fulfill_dashboard_project');
   var oppId = searchResult[int].getValue('custrecord_fulfill_dashboard_opp_id');
   var s_project_link = '<span class="project_name" title="' +searchResult[int].getValue("altname","CUSTRECORD_FULFILL_DASHBOARD_PROJECT",null)+ '"><a class="project_link" href="'+u_peopleView+'&custpage_projectid='+projId+'&custpage_oppid='+oppId+'">'+searchResult[int].getValue("altname","CUSTRECORD_FULFILL_DASHBOARD_PROJECT",null)+'</a></span>';
   var s_projName='<br><span class="project_name" title="' +searchResult[int].getValue('custrecord_fulfill_dashboard_account')+ '">'+searchResult[int].getValue('custrecord_fulfill_dashboard_account')+'</span>'
   var s_startdate = searchResult[int].getValue('custrecord_fulfill_dashboard_start_date');
   s_startdate ='<p class="start_date">'+s_startdate+'</p>';
   var s_pm = '<span class="project_name exec_pract" title="' +searchResult[int].getValue('custrecord_fulfill_dashboard_manager')+ '">'+searchResult[int].getValue('custrecord_fulfill_dashboard_manager')+'</span>';
   var s_exec_practice = '<span class="project_name" title="' +searchResult[int].getText('custrecord_fulfill_dashboard_practice')+ '">'+searchResult[int].getText('custrecord_fulfill_dashboard_practice')+'</span>';
   var n_frf = searchResult[int].getValue('custrecord_fulfill_dashboard_frf'); 
   if (searchResult[int].getValue('custrecord_fulfill_dashboard_frf') == "0" && searchResult[int].getValue('custrecord_fulfill_dashboard_rrf') == "0"&& _logValidation(oppId)) {
   	n_frf = '<a class="prev-page" href='+u_createFRF+'&s_project='+projId+'&s_opp='+oppId+'><button class="btn create-frf-btn">Plan</button> </a>';
   }else{
   	n_frf = searchResult[int].getValue('custrecord_fulfill_dashboard_frf');
   }
   // Following JSON for rendering 
   a_JSON = {
   		// pr_status: counter, In next MVP
   		project_account: s_project_link + s_projName,
   		rev_status: searchResult[int].getValue('custrecord_fulfill_dashboard_reve_confid')+'<br>'+ searchResult[int].getValue('custrecord_fulfill_dashboard_rev_sta_pro'),
   		pm_practice: s_pm + '<br>' + s_exec_practice,//searchResult[int].getValue('custrecord_fulfill_dashboard_manager')+'<br>'+searchResult[int].getText('custrecord_fulfill_dashboard_practice'),
   		start_date: calender+ s_startdate,
   		team: team+' '+ Math.round(searchResult[int].getValue('custrecord_fulfill_dashboard_pro_team')),
   		ramp_up: rampup + searchResult[int].getValue('custrecord_fulfill_dashboard_rampup'),
   		exiting:exiting + Math.round(searchResult[int].getValue('custrecord_fulfill_dashboard_exiting')),
   		frf:n_frf,
   		rrf:searchResult[int].getValue('custrecord_fulfill_dashboard_rrf'),
   		star:star
   }
   // Following JSON for filter 
   b_JSON = {
   		// pr_status: counter,In next MVP
   		project_account: searchResult[int].getValue('custrecord_fulfill_dashboard_account'),
   		rev_status: searchResult[int].getValue('custrecord_fulfill_dashboard_rev_sta_pro'),
   		pm_practice:searchResult[int].getText('custrecord_fulfill_dashboard_practice')
   		}
   total_frf = total_frf + parseInt(searchResult[int].getValue('custrecord_fulfill_dashboard_frf'));
   total_rrf = total_rrf + parseInt(searchResult[int].getValue('custrecord_fulfill_dashboard_rrf'));
   a_proj_array.push(a_JSON);
   a_proj_array_filter.push(b_JSON);
   projectCounter++;
   counter++;*/
   var s_project;
   var i_project = 0;
   var i_oppID = 0;
   if (searchResult[int].getValue('custrecord_fulfill_dashboard_project')) {
    s_project = searchResult[int].getText('custrecord_fulfill_dashboard_project');
    i_project = searchResult[int].getValue('custrecord_fulfill_dashboard_project');
   } else {
    s_project = searchResult[int].getValue("custrecord_opportunity_name_sfdc", "CUSTRECORD_FULFILL_DASHBOARD_OPP_ID", null);
    i_oppID = searchResult[int].getValue("custrecord_customer_internal_id_sfdc", "CUSTRECORD_FULFILL_DASHBOARD_OPP_ID", null);
   }
   a_JSON = {
    project: s_project, //searchResult[int].getValue("custrecord_opportunity_name_sfdc","CUSTRECORD_FULFILL_DASHBOARD_OPP_ID",null),
    projectID: i_project,
    i_oppID: i_oppID,
    account: searchResult[int].getValue("custrecord_fulfill_dashboard_account"),
    revstatus: searchResult[int].getValue("custrecord_fulfill_dashboard_reve_confid"),
    practice: searchResult[int].getText("custrecord_fulfill_dashboard_practice"),
    teamsize: searchResult[int].getValue("custrecord_fulfill_dashboard_pro_team"),
    frf: searchResult[int].getValue("custrecord_fulfill_dashboard_frf"),
    rrf: searchResult[int].getValue("custrecord_fulfill_dashboard_rrf"),
    startdate: searchResult[int].getValue("custrecord_fulfill_dashboard_start_date"),
    manager: searchResult[int].getValue("custrecord_fulfill_dashboard_manager")
   }
   total_frf = total_frf + parseInt(searchResult[int].getValue('custrecord_fulfill_dashboard_frf'));
   total_rrf = total_rrf + parseInt(searchResult[int].getValue('custrecord_fulfill_dashboard_rrf'));
   projectCounter++;
   a_proj_array.push(a_JSON);
  }
 }
}

function GetPracticeSPOC(user) {
 nlapiLogExecution('AUDIT', 'user', user);
 var spocSearch = nlapiSearchRecord("customrecord_practice_spoc", null, [
  ["custrecord_spoc", "anyof", user]
 ], [
  new nlobjSearchColumn("custrecord_practice")
 ]);
 return spocSearch;
}

function _logValidation(value) {
 if (value != null && value != '' && value != undefined && value != 'NaN') {
  return true;
 } else {
  return false;
 }
}