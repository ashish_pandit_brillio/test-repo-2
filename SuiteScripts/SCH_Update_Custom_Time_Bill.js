/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       18 Nov 2014     Swati
 *
 */

/**
 * @param {String} type Context Types: scheduled, ondemand, userinterface, aborted, skipped
 * @returns {Void}
 */
function scheduled(type)
{//fun start
	 var i_context = nlapiGetContext();
	 var a_TR_ID_array = i_context.getSetting('SCRIPT','custscript_date_array');
	 nlapiLogExecution('DEBUG', 'a_TR_ID_array',a_TR_ID_array);
	 
	 var i_journal_entry_SubmitID = i_context.getSetting('SCRIPT','custscript_jvid');
	 nlapiLogExecution('DEBUG', 'i_journal_entry_SubmitID',i_journal_entry_SubmitID);
	 
	 var d_reverse_date = i_context.getSetting('SCRIPT','custscript_reverse');
	 nlapiLogExecution('DEBUG', 'd_reverse_date',d_reverse_date);
	 
	 var fields=new Array();
		var values=new Array();
		var a_TR_ID_array_new=new Array();
		a_TR_ID_array_new=a_TR_ID_array.split(',');
	  for(var mm=0 ; mm<a_TR_ID_array_new.length ; mm++)
		{
		try{
		  time_bill_cus_id=a_TR_ID_array_new[mm];//
		  nlapiLogExecution('DEBUG', 'time_bill_cus_id',time_bill_cus_id);
			fields[0]='custrecord_journal_entry_ref';
			values[0] = i_journal_entry_SubmitID;
			fields[1] = 'custrecord_reverse_date';
			values[1] = d_reverse_date;
			fields[2] = 'custrecord_processed';
			values[2] = 'T';
			
			nlapiSubmitField('customrecord_time_bill_rec',time_bill_cus_id,fields,values);
		}
		catch(err)
		{
			nlapiLogExecution('error','error',err);
			Send_Exeception_Mail(err);
		}
		
		}
}//fun close
function Send_Exeception_Mail(err)
{​​​​​
    try
    {​​​​​   
        var context = nlapiGetContext();
        var s_DeploymentID =context.getDeploymentId();
        var s_ScriptID =context.getScriptId();
        var s_Subject ='Issue on Update_Custom_Time_Bill' ;
        
        var s_Body = 'This is to inform that Update_Custom_Time_Bill is having an issue';
        s_Body += '<br/>Issue: Code: '+err.code+' Message: '+err.message;
        s_Body += '<br/>Script: '+s_ScriptID;
        s_Body += '<br/>Deployment: '+s_DeploymentID;
        s_Body += '<br/>Please have a look and do the needfull.';
        s_Body += '<br/><br/>Note: This is system genarated email incase of any failure while sending the notification to employees';

        s_Body += '<br/><br/>Regards,';
        s_Body += '<br/>Digital Office';
        nlapiSendEmail(442,"netsuite.support@brillio.com",s_Subject,s_Body,null, null, null, null);
    }​​​​​
    catch(err)
    {​​​​​
        nlapiLogExecution('error',' Send_Exeception_Mail', err.message); 
		
    }​​​​​
}​​​​​

