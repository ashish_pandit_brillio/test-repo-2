function beforeload() {
    try {
        var sublist_item = nlapiGetLineItemField('item', 'custcol_project_entity_id');

       sublist_item.setMandatory(true);
    } catch (e) {
        nlapiLogExecution('debug', 'Error', e);
        return e;
    }

}

function beforeSubmit() {
    var error_log = [];
    try {
		var currentContext = nlapiGetContext();
		 if(currentContext.getExecutionContext() == 'userinterface')
		 {
        var count = nlapiGetLineItemCount('item');
		var practice_name;
		var practice_id;
		
        for (var i = 1; i <= count; i++) {
            var project_id = nlapiGetLineItemValue('item', 'custcol_project_entity_id', i);
            if (_logValidation(project_id)) {
                var jobSearch = nlapiSearchRecord("job", null,
                    [
                        ["entityid", "is", project_id.trim()],
                        "AND",
                        [
                            [
                                ["status", "anyof", "2", "4"]
                            ], "OR", [
                                ["systemnotes.type", "is", "F"], "AND", ["systemnotes.date", "onorafter", "threemonthsagotodate"], "AND", ["systemnotes.newvalue", "startswith", "PROJECT-Closed"], "AND", ["systemnotes.field", "anyof", "CUSTJOB.KENTITYSTATUS"]
                            ]
                        ]
                    ],
                    [
                        new nlobjSearchColumn("altname"),
                        new nlobjSearchColumn("startdate"),
                        new nlobjSearchColumn("entitystatus"),
                        new nlobjSearchColumn("formuladate").setFormula("{enddate}"),
                        new nlobjSearchColumn("custentity_practice"),
                        new nlobjSearchColumn("entityid").setSort(false)
                    ]
                );

                if (_logValidation(jobSearch)) {
                    practice_id = jobSearch[0].getValue('custentity_practice');
                    practice_name = jobSearch[0].getText('custentity_practice');

                    if (_logValidation(practice_id)) {
                        var searchActive = nlapiLookupField('department', parseInt(practice_id), 'isinactive');
                        if (searchActive == 'T') {
                            error_log.push({
                                'Msg': "'Practice '" + practice_name + "' is inactivated for project '" + project_id + "' at line: '" + i 
                            });
                        }
                    }
                } else {
                    error_log.push({
                        'Msg': "'Invalid projectID '" + project_id + "' at line: '" + i
                    });
                }


            }
			var project_entered = nlapiGetFieldValue('job');
			var project_id= nlapiLookupField('job',project_entered,'entityid');
			nlapiSetLineItemValue('item','custcol_project_entity_id',i,project_id);

            var emp_id = nlapiGetLineItemValue('item', 'custcol_employee_entity_id', i);
			var emp_practice_name;
            var emp_practice
			if(_logValidation(emp_id))
			{
				var empSearch = nlapiSearchRecord('employee', null, [
                        new nlobjSearchFilter('custentity_fusion_empid', null,
                            'is', emp_id),
                        new nlobjSearchFilter('isinactive',
                            null, 'is', 'F')
                    ],
                    [new nlobjSearchColumn(
                            'internalid'),
                        new nlobjSearchColumn(
                            'department')
                    ]);


                if (_logValidation(empSearch)) 
				{
                    emp_practice = empSearch[0].getValue('department');
                    emp_practice_name = empSearch[0].getText('department');
                

					if (_logValidation(emp_practice)) {
						var emp_practice_active = nlapiLookupField('department', emp_practice, 'isinactive')
						if (!(_logValidation(emp_practice_active)) && emp_practice_active == 'T') {
							error_log.push({
								'Msg': "'Practice '" + emp_practice_name + "'is inactivated for employee '" + emp_id + "' at line: '" + i 
							});

						}
					}
				}
				else {
                error_log.push({
                    'Msg': "'Invalid EmployeeID '" + emp_id + "' at line: '" + i
                });
				}
            }
        }

        if ((_logValidation(error_log)) && error_log.length > 0) {
			var log_text = displayArrayObjects(error_log);
            throw nlapiCreateError('False', log_text, true);
        }
	 }
    } catch (err) {
        nlapiLogExecution('debug', 'Error in the process', err);
        throw err;
    }
}

function displayArrayObjects(arrayObjects) {
    var len = arrayObjects.length;
    var text = "";

    for (var i = 0; i < len; i++) {
        var myObject = arrayObjects[i];
        
        for (var x in myObject) {
            text += ( x + ": " + myObject[x] + " ");
        }
        text += "<br/>";
    }

    return text;
}

function _logValidation(value) {
    if (value != null && value != '' && value != undefined && value.toString() != undefined && value.toString() != 'NaN' && value != NaN && value != 'undefined' && value != ' ') {
        return true;
    } else {
        return false;
    }
}