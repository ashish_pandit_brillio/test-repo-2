// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
    /*
     Script Name: UES_Invoice_Approval
     Author: Vikrant Sunil Adhav
     Company: Aashna CloudTech
     Date: 24-04-2014
     Description: Initial draft for initializing Invoice approval process
     Script Modification Log:
     -- Date --			-- Modified By --				--Requested By--				-- Description --
     Below is a summary of the process controls enforced by this script file.  The control logic is described
     more fully, below, in the appropriate function headers and code blocks.
     BEFORE LOAD
     - beforeLoadRecord(type)
     BEFORE SUBMIT
     - beforeSubmitRecord(type)
     AFTER SUBMIT
     - afterSubmitRecord_Initiate(type)
     SUB-FUNCTIONS
     - The following sub-functions are called by the above core functions in order to maintain code
     modularization:
     - NOT USED
     */
}
// END SCRIPT DESCRIPTION BLOCK  ====================================



// BEGIN GLOBAL VARIABLE BLOCK  =====================================
{
    //  Initialize any Global Variables, in particular, debugging variables...




}
// END GLOBAL VARIABLE BLOCK  =======================================





// BEGIN BEFORE LOAD ==================================================

function beforeLoadRecord(type){

    /*  On before load:
     - EXPLAIN THE PURPOSE OF THIS FUNCTION
     -
     FIELDS USED:
     --Field Name--				--ID--
     */
    //  LOCAL VARIABLES
    
    //  BEFORE LOAD CODE BODY
    
    
    return true;
    
    
    
}

// END BEFORE LOAD ====================================================





// BEGIN BEFORE SUBMIT ================================================

function beforeSubmitRecord(type){
    /*  On before submit:
     - PURPOSE
     -
     FIELDS USED:
     --Field Name--				--ID--
     */
    //  LOCAL VARIABLES
    
    
    //  BEFORE SUBMIT CODE BODY
    
    
    return true;
    
}

// END BEFORE SUBMIT ==================================================





// BEGIN AFTER SUBMIT =============================================

function afterSubmitRecord_Initiate(type)//
{
    /*  On after submit:
     - PURPOSE
     -
     FIELDS USED:
     --Field Name--				--ID--
     */
    //  LOCAL VARIABLES
    
    
    
    //  AFTER SUBMIT CODE BODY
    
    //if (type == 'create' || type == 'edit') //
	if (type == 'create') //
    {
        nlapiLogExecution('DEBUG', 'Invoice_Approval', 'type : ' + type);
        try //
        {
            var s_record_ID = nlapiGetRecordId();
            nlapiLogExecution('DEBUG', 'Invoice_Approval', 's_record_ID : ' + s_record_ID);
            
            var invoice_status = nlapiGetFieldValue('custbody_invoicestatus');
            
            if (invoice_status == 2)//
            {
           		
            }
		
            var r_Invoice_rec = nlapiLoadRecord('invoice', s_record_ID);
            var r_old_invoice_rec = r_Invoice_rec;
            nlapiLogExecution('DEBUG', 'Invoice_Approval', 'r_Invoice_rec : ' + r_Invoice_rec);
            nlapiLogExecution('DEBUG', 'Invoice_Approval', 'r_old_invoice_rec : ' + r_old_invoice_rec);
            
            var item_lines_count = r_Invoice_rec.getLineItemCount('item');
            nlapiLogExecution('DEBUG', 'Invoice_Approval', 'item_lines_count : ' + item_lines_count);
            
            var s_project_name = '';
            var pro_man_ID = r_Invoice_rec.getFieldValue('custbody_draft_invoice_app');
            var approvar_rec = nlapiLoadRecord('employee', pro_man_ID);
            var pro_mangre_email = approvar_rec.getFieldValue('email');
			
			
            var count = r_Invoice_rec.getLineItemCount('time')
            nlapiLogExecution('DEBUG', 'Invoice', 'count:-' + count)
			
            for (var i = 1; i <= count; i++) //
            {
                nlapiLogExecution('DEBUG', 'Invoice', 'i:--------------------------------------------------' + i)
                var item = r_Invoice_rec.getLineItemValue('time', 'item', i)
                nlapiLogExecution('DEBUG', 'Invoice', 'item:- ' + item)
				
                var qty = r_Invoice_rec.getLineItemValue('time', 'quantity', i)
                nlapiLogExecution('DEBUG', 'Invoice', 'qty:- ' + qty)
				
                var job = r_Invoice_rec.getLineItemValue('time', 'job', i)
                nlapiLogExecution('DEBUG', 'Invoice', 'job:- ' + job)
				
                var amount = r_Invoice_rec.getLineItemValue('time', 'amount', i)
                nlapiLogExecution('DEBUG', 'Invoice', 'amount:- ' + amount)
				
                var rate = r_Invoice_rec.getLineItemValue('time', 'rate', i)
                nlapiLogExecution('DEBUG', 'Invoice', 'rate:- ' + rate)
                
                var r_custom_rec = nlapiCreateRecord('customrecord_invoice_line_items');
                
				r_custom_rec.setFieldValue('custrecord_invoice_line_number', i);
                r_custom_rec.setFieldValue('name', s_record_ID);
                r_custom_rec.setFieldValue('custrecord_invoice_item', item);
                r_custom_rec.setFieldValue('custrecord_invoice_qty', qty);
                r_custom_rec.setFieldValue('custrecord_invoice_item_amount', amount);
                r_custom_rec.setFieldValue('custrecord_invoice_id', job);
                r_custom_rec.setFieldValue('custrecord_invoice_item_rate', rate);
                r_custom_rec.setFieldValue('custrecord_invoice_list_type', 'time');
                
                //var s_ID = '';
				nlapiLogExecution('DEBUG', 'Invoice_Approval', 'Custom Record Submitted ! ID : ' + s_ID);
				
				var s_ID = nlapiSubmitRecord(r_custom_rec);
                nlapiLogExecution('DEBUG', 'Invoice_Approval', 'Custom Record Submitted ! ID : ' + s_ID);
            }
            
            if (item_lines_count > 0) //
            {
                for (var i = 1; i <= item_lines_count; i++) //
                {
                    var item_name = r_Invoice_rec.getLineItemValue('item', 'item', i);
                    nlapiLogExecution('DEBUG', 'Invoice_Approval', 'item_name : ' + item_name);
                    
                    //var item_qty = r_Invoice_rec.getLineItemValue('item', 'item', i);
                    //nlapiLogExecution('DEBUG', 'Invoice_Approval', 'item_qty : ' + item_qty);
                    
                    var item_amount = r_Invoice_rec.getLineItemValue('item', 'amount', i);
                    nlapiLogExecution('DEBUG', 'Invoice_Approval', 'item_amount : ' + item_amount);
                    
                    if (i == 1 || (s_project_name == '' || s_project_name == 'undefined' || s_project_name == null)) //
                    {
                        s_project_name = r_Invoice_rec.getLineItemValue('item', 'custcol_sow_project', i);
                        nlapiLogExecution('DEBUG', 'Invoice_Approval', 's_project_name : ' + s_project_name);
                    }
                    
                    var r_custom_rec = nlapiCreateRecord('customrecord_invoice_line_items');
                    
                    r_custom_rec.setFieldValue('custrecord_invoice_line_number', i);
                    r_custom_rec.setFieldValue('name', s_record_ID);
                    r_custom_rec.setFieldValue('custrecord_invoice_item', item_name);
                    //r_custom_rec.setFieldValue('custrecord_invoice_qty', item_qty);
                    r_custom_rec.setFieldValue('custrecord_invoice_item_amount', item_amount);
                    r_custom_rec.setFieldValue('custrecord_invoice_id', s_record_ID);
                    r_custom_rec.setFieldValue('custrecord_invoice_list_type', 'item');
                    
                    var s_ID = nlapiSubmitRecord(r_custom_rec);
                    nlapiLogExecution('DEBUG', 'Invoice_Approval', 'Custom Record Submitted ! ID : ' + s_ID);
                    
                    r_Invoice_rec.setLineItemValue('item', 'amount', i, 0);
                    
                    //r_Invoice_rec.setLineItemValue('item', 'item', i, 0);
                }
         
		       
           
		        if (s_project_name != null || s_project_name == '' || s_project_name == 'undefined') //
                {
                    var r_project_rec = nlapiLoadRecord('job', s_project_name);
                    var s_project_manager = r_project_rec.getFieldValue('custentity_projectmanager');
                    nlapiLogExecution('DEBUG', 'Invoice_Approval', 's_project_manager : ' + s_project_manager);
                    
                    if (s_project_manager != null) //
                    {
                        r_Invoice_rec.setFieldValue('custbody_invoice_approver', s_project_manager);
						
						var r_pro_manager_rec=nlapiLoadRecord('employee', s_project_manager);
						//pro_mangre_email = r_pro_manager_rec.getFieldValue('email');
                        //nlapiLogExecution('DEBUG', 'Invoice_Approval', 'pro_mangre_email : ' + pro_mangre_email);
                        //nlapiLogExecution('DEBUG', 'Invoice_Approval', 'Setting Project Manager on Invoice');
                    }
                }
                
                r_Invoice_rec.setFieldValue('custbody_invoicestatus', 3);
                //nlapiSubmitRecord(r_Invoice_rec);
                //nlapiLogExecution('DEBUG', 'Invoice_Approval', 'Invoice Record Submitted !');
				
                var inv_internal_ID = get_Invoice_ID(type);
				nlapiLogExecution('DEBUG', 'get_Invoice_ID', 'inv_internal_ID '+inv_internal_ID);
				r_Invoice_rec.setFieldValue('custbody_invoice_id', inv_internal_ID);
				
				nlapiSubmitRecord(r_Invoice_rec);
                nlapiLogExecution('DEBUG', 'Invoice_Approval', 'Invoice Record Submitted !');
            }
			
			// Invoice PDF Sending Code
            
                //send_email(s_record_ID, 'vikrant@aashnacloudtech.com', s_record_ID, 'vikrant@aashnacloudtech.com', r_old_invoice_rec);
                send_email(s_record_ID, nlapiGetUser(), s_record_ID, pro_mangre_email, r_old_invoice_rec, pro_man_ID);
                
                //return;
            
                // End of Invoice PDF Sending Code 
        } 
        catch (ex)//
        {
            nlapiLogExecution('DEBUG', 'ERROR', 'Error : ' + ex);
        }
    }
    
    if (type == 'edit') //
    {
        //var inv_internal_ID = get_Invoice_ID(type);
    }
    
    return true;
    
}

// END AFTER SUBMIT ===============================================





// BEGIN FUNCTION ===================================================

function get_Invoice_ID(type) //
{
	//nlapiLogExecution('DEBUG', 'get_Invoice_ID', 'Into get_Invoice_ID');
	
    var r_cust_rec = nlapiLoadRecord('customrecord_invoice_auto_numbering', 1);	
    
	//nlapiLogExecution('DEBUG', 'get_Invoice_ID', 'After load');
	
    //if (type == 'create') //
	if(r_cust_rec != null)
    {
        var int_id = r_cust_rec.getFieldValue('custrecord_draft_invoice_id');
        int_id = parseInt(int_id) + 1;
        r_cust_rec.setFieldValue('custrecord_draft_invoice_id', int_id);
		//nlapiLogExecution('DEBUG', 'get_Invoice_ID', 'before submit');
		
        nlapiSubmitRecord(r_cust_rec, true);
        int_id = 'Draft Invoice No. ' + int_id;
		
		return int_id;
    }
}


function send_email(i_record_ID, s_sent_email, i_invoice_no, i_recepient_mail, r_invoice_rec, i_recepient_rec_id) //
{
    nlapiLogExecution('DEBUG', 'MSG', 'into send_email');
    if (i_record_ID != null && i_record_ID != '' && i_record_ID != undefined) //
    {
        var s_email_body = ' Hi, \n Please find the attched Invoice pending for your approval.';
        var file_ID = createFile(s_email_body, i_record_ID, r_invoice_rec);
        var file_n = nlapiLoadFile(file_ID);
        nlapiLogExecution('DEBUG', 'printPDF', '************** File N  ************** -->' + file_n);
        
        var newAttachment = file_n;
        nlapiLogExecution('DEBUG', 'printPDF', '************** New Attachment  ************** -->' + newAttachment);
        
        var records = new Object();
        records['transaction'] = i_record_ID
        
        var s_email_subject = ' Invoice Draft has been created for Invoice - '
        s_email_subject = s_email_subject + ' ' + i_invoice_no
        
        //Send the PDF as an attachment
        
        var s_email_sender = s_sent_email
        
        if (s_email_sender != null && s_email_sender != '' && s_email_sender != undefined) //
        {
            if (i_recepient_mail != null && i_recepient_mail != '' && i_recepient_mail != undefined) //
            {
                var records = new Object();
                records['entity'] = i_recepient_rec_id
                
                //Send the PDF as an attachment
                nlapiSendEmail(s_email_sender, i_recepient_mail, s_email_subject, s_email_body, null, null, records, newAttachment);
                nlapiLogExecution('DEBUG', 'print_Item_Fulfillment_PDF', ' Email Sent ......');
                
                //nlapiSendEmail(s_email_sender, i_recepient_mail, s_email_subject, s_email_body, null, null, records, newAttachment);
                //nlapiSendEmail(s_email_sender, 'vikrant@aashnacloudtech.com', s_email_subject, s_email_body, null, null, records, newAttachment);
                //nlapiLogExecution('DEBUG', 'send_email', ' Email Sent ......');
            
            }//s_email_sender				
        }//s_email_sender
    }//s_naming_convention
}//Send Email

function createFile(s_email_body, i_invoice_no, r_invoice_rec) // 
{
    nlapiLogExecution('DEBUG', 'MSG', 'into createFile');
    try//
    {
        var tran_date = r_invoice_rec.getFieldValue('trandate');
        nlapiLogExecution('DEBUG', 'Invoice', 'tran_date:- ' + tran_date)
        
        var invoice_no = r_invoice_rec.getFieldValue('tranid');
        nlapiLogExecution('DEBUG', 'Invoice', 'invoice_no:- ' + invoice_no)
        
        var cust_name = r_invoice_rec.getFieldText('entity');
        nlapiLogExecution('DEBUG', 'Invoice', 'cust_name:- ' + cust_name)
        
        var cust_addr = r_invoice_rec.getFieldText('billaddress');
        if (cust_addr == null)//
        {
            cust_addr = 'NA';
        }
        nlapiLogExecution('DEBUG', 'Invoice', 'cust_addr:- ' + cust_addr)
        
        var term_payment = r_invoice_rec.getFieldText('terms');
        nlapiLogExecution('DEBUG', 'Invoice', 'term_payment:- ' + term_payment)
        
        var cust_ID = r_invoice_rec.getFieldValue('entity');
        nlapiLogExecution('DEBUG', 'Invoice', 'cust_ID:- ' + cust_ID)
        
        var r_cust_rec = nlapiLoadRecord('customer', cust_ID);
        var s_ref_no = '';
        var s_vat_no = '';
        
        if (r_cust_rec != null && r_cust_rec != 'undefined') //
        {
            s_ref_no = r_cust_rec.getFieldValue('custentity_msarefnumber');
            nlapiLogExecution('DEBUG', 'Invoice', 's_ref_no:- ' + s_ref_no)
            
            
            s_vat_no = r_cust_rec.getFieldValue('vatregnumber');
            nlapiLogExecution('DEBUG', 'Invoice', 's_vat_no:- ' + s_vat_no)
        }
        
        var so_id = r_invoice_rec.getFieldValue('otherrefnum');
        nlapiLogExecution('DEBUG', 'Invoice', 'so_id:- ' + so_id)
        
        var s_ref_no_2 = 'SOW dated ';
        
        if (so_id != null && so_id != 'undefined' && so_id != '')// 
        {
            var r_SO_rec = nlapiLoadRecord('salesorder', so_id);
            
            if (r_SO_rec != null && r_SO_rec != 'undefined' && r_SO_rec != '')//
            {
                var s_date = r_SO_rec.getFieldValue('trandate');
                nlapiLogExecution('DEBUG', 'Invoice', 's_date:- ' + s_date)
                
                var s_memo = r_SO_rec.getFieldValue('memo');
                nlapiLogExecution('DEBUG', 'Invoice', 's_memo:- ' + s_memo)
                
                if (s_date != null)//
                {
                    s_ref_no_2 = s_ref_no_2 + s_date;
                    nlapiLogExecution('DEBUG', 'Invoice', 's_ref_no_2:- ' + s_ref_no_2)
                    
                }
                if (s_memo != null) //
                {
                    s_ref_no_2 = s_ref_no_2 + "_" + s_memo;
                    nlapiLogExecution('DEBUG', 'Invoice', 's_ref_no_2:- ' + s_ref_no_2)
                }
            }
        }
        
        
        var image_URL = 'https://system.sandbox.netsuite.com/core/media/media.nl?id=11&c=3883006&h=e396b1ed7417b4cdd254'
        image_URL = nlapiEscapeXML(image_URL);
        
        var strVar = "";
        strVar += "<table style=\"border: thin solid #000000;\" width=\"100%\">";
        strVar += "        <tr>";
        strVar += "            <td style=\"border: thin solid #000000\" colspan=\"2\">";
        strVar += "                <b>Brillio Technologies Pvt Ltd <\/b>";
        strVar += "                <br \/>";
        strVar += "                " + nlapiEscapeXML("# 58, Ist Main Road, Mini Forest") + "<br \/>";
        strVar += "                " + nlapiEscapeXML("J.P Nagar 3rd Phase") + "<br \/>";
        strVar += "                " + nlapiEscapeXML("Bangalore 560078") + "";
        
        if (image_URL != '' && image_URL != null && image_URL != '' && image_URL != 'undefined' && image_URL != ' ' && image_URL != '&nbsp') // 
        {
            strVar += "                <img width=\"150\" height=\"75\" align=\"right\" src=\"" + image_URL + "\"><\/img>";
        }
        
        strVar += "            <\/td>";
        strVar += "        <\/tr>";
        
        
        strVar += "        <tr>";
        strVar += "            <td colspan=\"2\">";
        strVar += "                <b>UK Branch Office Address<br \/>";
        strVar += "                <\/b>" + nlapiEscapeXML("29Th Floor, One Canada Square,") + "<br \/>";
        strVar += "                " + nlapiEscapeXML("Canary Wharf, London, E14 5DY") + "";
        strVar += "            <\/td>";
        strVar += "        <\/tr>";
        
        
        strVar += "        <tr>";
        strVar += "            <td colspan=\"2\">";
        strVar += "                <table class=\"style1\" style=\"border: thin solid #000000\" width=\"100%\">";
        strVar += "                    <tr>";
        strVar += "                        <td class=\"style2\">";
        strVar += "                            " + nlapiEscapeXML("Invoice No: ") + "" + nlapiEscapeXML(invoice_no) + "";
        //strVar += "                            Invoice No:";
        strVar += "                        <\/td>";
        strVar += "                        <td class=\"style2\">";
        strVar += "                            " + nlapiEscapeXML("Date : ") + "" + nlapiEscapeXML(tran_date) + "";
        strVar += "                        <\/td>";
        strVar += "                    <\/tr>";
        strVar += "                    <tr>";
        strVar += "                        <td class=\"style2\">";
        strVar += "                            &nbsp;";
        strVar += "                        <\/td>";
        strVar += "                        <td class=\"style2\">";
        strVar += "                            &nbsp;";
        strVar += "                        <\/td>";
        strVar += "                    <\/tr>";
        strVar += "                    <tr>";
        strVar += "                        <td colspan=\"2\" class=\"style2\">";
        strVar += "                            <b>" + nlapiEscapeXML("Reference 1: ") + "<\/b>" + nlapiEscapeXML(s_ref_no) + "";
        strVar += "                        <\/td>";
        strVar += "                    <\/tr>";
        strVar += "                    <tr>";
        strVar += "                        <td colspan=\"2\" class=\"style9\">";
        strVar += "                            <b>" + nlapiEscapeXML("Reference 2:") + "<\/b> " + nlapiEscapeXML(s_ref_no_2) + "";
        strVar += "                        <\/td>";
        strVar += "                    <\/tr>";
        strVar += "                    <tr>";
        strVar += "                        <td colspan=\"2\" class=\"style2\">";
        strVar += "                            <b>" + nlapiEscapeXML("VAT Registration No: ") + "<\/b>" + nlapiEscapeXML(s_vat_no) + "";
        strVar += "                        <\/td>";
        strVar += "                    <\/tr>";
        strVar += "                    <tr>";
        strVar += "                        <td class=\"style4\" style=\"vertical-align: top\">";
        strVar += "                            <table class=\"style6\" width=\"100%\">";
        strVar += "                                <tr>";
        strVar += "                                    <td>";
        strVar += "                                        <b>Bill To <\/b>";
        strVar += "                                    <\/td>";
        strVar += "                                <\/tr>";
        strVar += "                                <tr>";
        strVar += "                                    <td>";
        strVar += "                                        <b>" + nlapiEscapeXML(cust_name) + "<\/b>";
        strVar += "                                    <\/td>";
        strVar += "                                <\/tr>";
        strVar += "                                <tr>";
        strVar += "                                    <td>";
        strVar += "                                        " + nlapiEscapeXML(cust_addr) + "";
        strVar += "                                    <\/td>";
        strVar += "                                <\/tr>";
        strVar += "                                <tr>";
        strVar += "                                    <td>";
        strVar += "                                        ";
        strVar += "                                    <\/td>";
        strVar += "                                <\/tr>";
        strVar += "                                <tr>";
        strVar += "                                    <td>";
        strVar += "                                        ";
        strVar += "                                    <\/td>";
        strVar += "                                <\/tr>";
        strVar += "                                <tr>";
        strVar += "                                    <td>";
        strVar += "                                        ";
        strVar += "                                    <\/td>";
        strVar += "                                <\/tr>";
        strVar += "                                <tr>";
        strVar += "                                    <td>";
        strVar += "                                        ";
        strVar += "                                    <\/td>";
        strVar += "                                <\/tr>";
        strVar += "                            <\/table>";
        strVar += "                        <\/td>";
        strVar += "                        <td rowspan=\"7\" class=\"style3\">";
        strVar += "                            <table style=\"border: thin solid #000000\" width=\"100%\">";
        strVar += "                                <tr>";
        strVar += "                                    <td>";
        strVar += "                                         ";
        strVar += "                                    <\/td>";
        strVar += "                                    <td>";
        strVar += "                                        &nbsp;";
        strVar += "                                    <\/td>";
        strVar += "                                <\/tr>";
        strVar += "                                <tr>";
        strVar += "                                    <td>";
        strVar += "                                        <b>" + nlapiEscapeXML("Remittance Details: ") + "<\/b>";
        strVar += "                                    <\/td>";
        strVar += "                                    <td>";
        strVar += "                                        &nbsp;";
        strVar += "                                    <\/td>";
        strVar += "                                <\/tr>";
        strVar += "                                <tr>";
        strVar += "                                    <td colspan=\"2\">";
        strVar += "                                        " + nlapiEscapeXML("Request you to send the wire transfer in Sterling Pounds To:") + "";
        strVar += "                                    <\/td>";
        strVar += "                                <\/tr>";
        strVar += "                                <tr>";
        strVar += "                                    <td>";
        strVar += "                                        Bank Name";
        strVar += "                                    <\/td>";
        strVar += "                                    <td>";
        strVar += "                                        <b>Bank of America NA<\/b>";
        strVar += "                                    <\/td>";
        strVar += "                                <\/tr>";
        strVar += "                                <tr>";
        strVar += "                                    <td>";
        strVar += "                                        " + nlapiEscapeXML("Account number") + "";
        strVar += "                                    <\/td>";
        strVar += "                                    <td>";
        strVar += "                                        70106051";
        strVar += "                                    <\/td>";
        strVar += "                                <\/tr>";
        strVar += "                                <tr>";
        strVar += "                                    <td>";
        strVar += "                                        Account name";
        strVar += "                                    <\/td>";
        strVar += "                                    <td>";
        strVar += "                                        " + nlapiEscapeXML("Brillio Technologies Pvt. Ltd") + "";
        strVar += "                                    <\/td>";
        strVar += "                                <\/tr>";
        strVar += "                                <tr>";
        strVar += "                                    <td>";
        strVar += "                                        Bank address";
        strVar += "                                    <\/td>";
        strVar += "                                    <td>";
        strVar += "                                        " + nlapiEscapeXML("Bank of America N.A., Chennai, India") + "";
        strVar += "                                    <\/td>";
        strVar += "                                <\/tr>";
        strVar += "                                <tr>";
        strVar += "                                    <td>";
        strVar += "                                        Swift code";
        strVar += "                                    <\/td>";
        strVar += "                                    <td>";
        strVar += "                                        BOFAIN4XMAA";
        strVar += "                                    <\/td>";
        strVar += "                                <\/tr>";
        strVar += "                                <tr>";
        strVar += "                                    <td colspan=\"2\">";
        strVar += "                                        <b>Correspondent Bank Account details<\/b>";
        strVar += "                                    <\/td>";
        strVar += "                                <\/tr>";
        strVar += "                                <tr>";
        strVar += "                                    <td>";
        strVar += "                                        Bank Swift code";
        strVar += "                                    <\/td>";
        strVar += "                                    <td>";
        strVar += "                                        BOFAGB22";
        strVar += "                                    <\/td>";
        strVar += "                                <\/tr>";
        strVar += "                                <tr>";
        strVar += "                                    <td>";
        strVar += "                                        " + nlapiEscapeXML("Bank name & address") + "";
        strVar += "                                    <\/td>";
        strVar += "                                    <td>";
        strVar += "                                        " + nlapiEscapeXML("Bank of America N.A., London, U.K.") + "";
        strVar += "                                    <\/td>";
        strVar += "                                <\/tr>";
        strVar += "                                <tr>";
        strVar += "                                    <td>";
        strVar += "                                        Sort code";
        strVar += "                                    <\/td>";
        strVar += "                                    <td>";
        strVar += "                                        " + nlapiEscapeXML("16-50-50") + "";
        strVar += "                                    <\/td>";
        strVar += "                                <\/tr>";
        strVar += "                                <tr>";
        strVar += "                                    <td>";
        strVar += "                                        Nostro a\/c with London branch";
        strVar += "                                    <\/td>";
        strVar += "                                    <td>";
        strVar += "                                        " + nlapiEscapeXML("GBP: 600896215019") + "";
        strVar += "                                    <\/td>";
        strVar += "                                <\/tr>";
        strVar += "                                <tr>";
        strVar += "                                    <td>";
        strVar += "                                        &nbsp;";
        strVar += "                                    <\/td>";
        strVar += "                                    <td>";
        strVar += "                                        &nbsp;";
        strVar += "                                    <\/td>";
        strVar += "                                <\/tr>";
        strVar += "                            <\/table>";
        strVar += "                        <\/td>";
        strVar += "                    <\/tr>";
        strVar += "                <\/table>";
        strVar += "            <\/td>";
        strVar += "        <\/tr>";
        
        
        
        strVar += "			<tr>";
        strVar += "            <td>";
        strVar += "                Terms of Payment";
        strVar += "            <\/td>";
        strVar += "            <td>";
        strVar += "                " + nlapiEscapeXML(term_payment) + "";
        strVar += "            <\/td>";
        strVar += "        <\/tr>";
        
        
        strVar += "        <tr>";
        strVar += "            <td>";
        strVar += "                Billing Description";
        strVar += "            <\/td>";
        strVar += "            <td>";
        strVar += "                Professional Service charges - Offshore";
        strVar += "            <\/td>";
        strVar += "        <\/tr>";
        
        
        strVar += "			<tr>";
        strVar += "            <td colspan=\"2\" style=\"border: thin solid #000000\">";
        strVar += "                <table class=\"style6\" width=\"100%\">";
        strVar += "                    <tr>";
        strVar += "                        <td rowspan=\"2\" style=\"border: thin solid #000000\">";
        strVar += "                            <b>SL #<\/b>";
        strVar += "                        <\/td>";
        strVar += "                        <td rowspan=\"2\" style=\"border: thin solid #000000\">";
        strVar += "                            <b>Billing Description<\/b>";
        strVar += "                        <\/td>";
        strVar += "                        <td class=\"style7\" colspan=\"2\" style=\"border: thin solid #000000\">";
        strVar += "                            <b style=\"text-align: center\">Billing Period<\/b>";
        strVar += "                        <\/td>";
        strVar += "                        <td class=\"style7\" colspan=\"2\" style=\"border: thin solid #000000\">";
        strVar += "                            <b style=\"text-align: center\">No. of Units Billed<\/b>";
        strVar += "                        <\/td>";
        strVar += "                        <td rowspan=\"2\" class=\"style7\" style=\"border: thin solid #000000\">";
        strVar += "                            <b>Unit of Measures<\/b>";
        strVar += "                        <\/td>";
        strVar += "                        <td rowspan=\"2\" class=\"style7\" style=\"border: thin solid #000000\">";
        strVar += "                            <b>Amount<\/b>";
        strVar += "                        <\/td>";
        strVar += "                    <\/tr>";
        strVar += "                    <tr>";
        strVar += "                        <td class=\"style7\" style=\"border: thin solid #000000\">";
        strVar += "                            <b>From<\/b>";
        strVar += "                        <\/td>";
        strVar += "                        <td class=\"style7\" style=\"border: thin solid #000000\">";
        strVar += "                            <b>To<\/b>";
        strVar += "                        <\/td>";
        strVar += "                        <td class=\"style7\" style=\"border: thin solid #000000\">";
        strVar += "                            <b>Regular<\/b>";
        strVar += "                        <\/td>";
        strVar += "                        <td class=\"style7\" style=\"border: thin solid #000000\">";
        strVar += "                            <b>Excess<\/b>";
        strVar += "                        <\/td>";
        strVar += "                    <\/tr>";
        
        var count = r_invoice_rec.getLineItemCount('time')
        nlapiLogExecution('DEBUG', 'Invoice', 'count:-' + count)
        
        var item_name = '';
        var item_desc = '';
        var item_amount = '';
        var item_unit = '';
        var from_date = '';
        var to_date = '';
        var regular = '';
        var excess = '';
        var tax_amount = r_invoice_rec.getFieldValue('taxtotal');
        nlapiLogExecution('DEBUG', 'Invoice', 'tax_amount:-' + tax_amount)
        
        var grand_total = r_invoice_rec.getFieldValue('total');
        nlapiLogExecution('DEBUG', 'Invoice', 'grand_total:-' + grand_total)
        
        for (var i = 1; i <= count; i++) //
        {
            if (i == 1)//
            {
                from_date = r_invoice_rec.getLineItemValue('time', 'billeddate', i);
                nlapiLogExecution('DEBUG', 'Invoice', 'from_date:- ' + from_date)
                
                to_date = r_invoice_rec.getLineItemValue('time', 'billeddate', count);
                nlapiLogExecution('DEBUG', 'Invoice', 'to_date:- ' + to_date)
            }
            
            //invRec.getLineItemValue('time', 'item', i)
            item_name = r_invoice_rec.getLineItemValue('time', 'item', i);
            if (item_name == null) //
            {
                item_name = 'NA';
            }
            else //
            {
                var r_item = nlapiLoadRecord('serviceitem', item_name);
                if (r_item != null)//
                {
                    item_name = r_item.getFieldValue('displayname');
                }
            }
            nlapiLogExecution('DEBUG', 'Invoice', 'item_name:- ' + item_name)
            
            item_desc = r_invoice_rec.getLineItemValue('time', 'memo', i);
            if (item_desc == null) //
            {
                item_desc = 'NA';
            }
            nlapiLogExecution('DEBUG', 'Invoice', 'item_desc:-' + item_desc)
            
            item_amount = r_invoice_rec.getLineItemValue('time', 'amount', i);
            if (item_amount == null) //
            {
                item_amount = 'NA';
            }
            nlapiLogExecution('DEBUG', 'Invoice', 'item_amount:-' + item_amount)
            
            item_unit = r_invoice_rec.getLineItemValue('time', 'qty', i);
            if (item_unit == null) //
            {
                item_unit = 'NA';
            }
            nlapiLogExecution('DEBUG', 'Invoice', 'item_unit:-' + item_unit)
            
            //from_date = r_invoice_rec.getLineItemText('time','unit', i);
            nlapiLogExecution('DEBUG', 'Invoice', 'from_date:-' + from_date)
            
            //to_date = r_invoice_rec.getLineItemText('time','unit', i);
            nlapiLogExecution('DEBUG', 'Invoice', 'to_date:-' + to_date)
            
            //regular = r_invoice_rec.getLineItemText('time','unit', i);
            nlapiLogExecution('DEBUG', 'Invoice', 'regular:-' + regular)
            
            //excess = r_invoice_rec.getLineItemText('time','unit', i);
            nlapiLogExecution('DEBUG', 'Invoice', 'excess:-' + excess)
            
            tax_amount = r_invoice_rec.getFieldValue('taxtotal');
            if (tax_amount == null) //
            {
                tax_amount = 'NA';
            }
            nlapiLogExecution('DEBUG', 'Invoice', 'tax_amount:-' + tax_amount)
            
            
            strVar += "                    <tr>";
            strVar += "                        <td style=\"border: thin solid #000000\">";
            strVar += "                            " + nlapiEscapeXML(i) + "";
            strVar += "                        <\/td>";
            strVar += "                        <td style=\"border: thin solid #000000\">";
            strVar += "                            " + nlapiEscapeXML(item_name) + "";
            strVar += "                        <\/td>";
            strVar += "                        <td style=\"border: thin solid #000000\">";
            strVar += "                            " + nlapiEscapeXML(from_date) + "";
            strVar += "                        <\/td>";
            strVar += "                        <td style=\"border: thin solid #000000\">";
            strVar += "                            " + nlapiEscapeXML(to_date) + "";
            strVar += "                        <\/td>";
            strVar += "                        <td style=\"border: thin solid #000000\">";
            strVar += "                            " + nlapiEscapeXML(regular) + "";
            strVar += "                        <\/td>";
            strVar += "                        <td style=\"border: thin solid #000000\">";
            strVar += "                            " + nlapiEscapeXML(excess) + "";
            strVar += "                        <\/td>";
            strVar += "                        <td style=\"border: thin solid #000000\">";
            strVar += "                            " + nlapiEscapeXML(item_unit) + "";
            strVar += "                        <\/td>";
            strVar += "                        <td style=\"border: thin solid #000000\">";
            strVar += "                            " + nlapiEscapeXML(item_amount) + "";
            strVar += "                        <\/td>";
            strVar += "                    <\/tr>";
        }
        
        strVar += "                    <tr>";
        strVar += "                        <td style=\"border: thin solid #000000\">";
        strVar += "                            ";
        strVar += "                        <\/td>";
        strVar += "                        <td style=\"border: thin solid #000000\">";
        strVar += "                            Add: VAT 20%";
        strVar += "                        <\/td>";
        strVar += "                        <td style=\"border: thin solid #000000\">";
        strVar += "                            ";
        strVar += "                        <\/td>";
        strVar += "                        <td style=\"border: thin solid #000000\">";
        strVar += "                            ";
        strVar += "                        <\/td>";
        strVar += "                        <td style=\"border: thin solid #000000\">";
        strVar += "                            ";
        strVar += "                        <\/td>";
        strVar += "                        <td style=\"border: thin solid #000000\">";
        strVar += "                            ";
        strVar += "                        <\/td>";
        strVar += "                        <td style=\"border: thin solid #000000\">";
        strVar += "                            ";
        strVar += "                        <\/td>";
        strVar += "                        <td style=\"border: thin solid #000000\">";
        strVar += "                            " + nlapiEscapeXML(tax_amount) + "";
        strVar += "                        <\/td>";
        strVar += "                    <\/tr>";
        
        
        
        strVar += "                    <tr>";
        strVar += "                        <td colspan=\"6\" style=\"border: thin solid #000000;\">";
        strVar += "                            Billing Currency - Pound Sterling";
        strVar += "                        <\/td>";
        strVar += "                        <td style=\"border: thin solid #000000;\" class=\"style8\">";
        strVar += "                            Grand Total";
        strVar += "                        <\/td>";
        strVar += "                        <td class=\"style8\" style=\"border: thin solid #000000\">";
        strVar += "                            " + grand_total + "";
        strVar += "                        <\/td>";
        strVar += "                    <\/tr>";
        
        
        
        strVar += "                    <tr>";
        strVar += "                        <td colspan=\"8\" style=\"border: thin solid #000000;\">";
        strVar += "                            Amount in Words: GBP " + inWords(r_invoice_rec.getFieldValue('total'));
        strVar += "                        <\/td>";
        strVar += "                    <\/tr>";
        
        
        strVar += "                <\/table>";
        strVar += "            <\/td>";
        strVar += "        <\/tr>";
        
        
        
        strVar += "        <tr>";
        strVar += "            <td>";
        strVar += "                This is a computer generated document, signature not required.";
        strVar += "            <\/td>";
        strVar += "            <td style=\"font-weight: 700; text-align: center\">";
        strVar += "                <b>For Brillio Technologies Pvt Ltd <\/b>";
        strVar += "                <br \/>";
        strVar += "                <br \/>";
        strVar += "                <br \/>";
        strVar += "                <br \/>";
        strVar += "                Authorised Signatory";
        strVar += "            <\/td>";
        strVar += "        <\/tr>";
        
        
        strVar += "        <tr>";
        strVar += "            <td colspan=\"2\" style=\"font-weight: 700\">";
        strVar += "                Registered Office : Tower &#39;D&#39;, 6th Floor, Subramanya Arcade, No. 12, Bannerghatta";
        strVar += "                Road, Bangalore - 560 076";
        strVar += "            <\/td>";
        strVar += "        <\/tr>";
        
        
        strVar += "        <tr>";
        strVar += "            <td colspan=\"2\">";
        strVar += "                &nbsp;";
        strVar += "            <\/td>";
        strVar += "        <\/tr>";
        
        strVar += "    <\/table>";
        
        //nlapiLogExecution('DEBUG', 'createFile', 'strVar : ' + strVar);
        nlapiEscapeXML(strVar);
        
        var xml = "<?xml version=\"1.0\"?>\n<!DOCTYPE pdf PUBLIC \"-//big.faceless.org//report\" \"report-1.1.dtd\">\n";
        //xml += "<pdf>\n<body font-size=\"10\" font-family=\"Verdena\">";
        xml += "<pdf>\n<body font-size=\"10\">";
        xml += strVar;
        xml += "</body>\n</pdf>";
        
        var file = nlapiXMLToPDF(xml);
        nlapiLogExecution('DEBUG', 'printPDF', 'File ==' + file);
        
        var d_currentTime = new Date();
        var timestamp = d_currentTime.getTime();
        //Create PDF File with TimeStamp in File Name
        
        var fileObj = nlapiCreateFile(i_invoice_no + '_' + timestamp + '.pdf', 'PDF', file.getValue());
        
        nlapiLogExecution('DEBUG', 'printPDF', ' PDF fileObj=' + fileObj);
        fileObj.setFolder(15);
        var fileId = nlapiSubmitFile(fileObj);
        nlapiLogExecution('DEBUG', 'printPDF', '********** PDF File ID -->' + fileId);
        return fileId;
        //o_propertiesRec.setFieldValue('custrecord_inv_doc_pdf', fileId);
        //var id = nlapiSubmitRecord(o_propertiesRec, true);
    
    } 
    catch (e)//
    {
        nlapiLogExecution('DEBUG', 'createFile', 'error : ' + e);
        return null;
    }
    
}

function inWords(num) //
{
    num = parseInt(num);
    nlapiLogExecution('DEBUG', 'inWords', 'num ' + num);
    
    
    var a = ['', 'one ', 'two ', 'three ', 'four ', 'five ', 'six ', 'seven ', 'eight ', 'nine ', 'ten ', 'eleven ', 'twelve ', 'thirteen ', 'fourteen ', 'fifteen ', 'sixteen ', 'seventeen ', 'eighteen ', 'nineteen '];
    var b = ['', '', 'twenty', 'thirty', 'forty', 'fifty', 'sixty', 'seventy', 'eighty', 'ninety'];
    
    
    if ((num = num.toString()).length > 9) 
        return 'overflow';
    n = ('000000000' + num).substr(-9).match(/^(\d{2})(\d{2})(\d{2})(\d{1})(\d{2})$/);
    if (!n) 
        return;
    var str = '';
    str += (n[1] != 0) ? (a[Number(n[1])] || b[n[1][0]] + ' ' + a[n[1][1]]) + 'crore ' : '';
    str += (n[2] != 0) ? (a[Number(n[2])] || b[n[2][0]] + ' ' + a[n[2][1]]) + 'lakh ' : '';
    str += (n[3] != 0) ? (a[Number(n[3])] || b[n[3][0]] + ' ' + a[n[3][1]]) + 'thousand ' : '';
    str += (n[4] != 0) ? (a[Number(n[4])] || b[n[4][0]] + ' ' + a[n[4][1]]) + 'hundred ' : '';
    str += (n[5] != 0) ? ((str != '') ? 'and ' : '') + (a[Number(n[5])] || b[n[5][0]] + ' ' + a[n[5][1]]) + 'only ' : '';
    nlapiLogExecution('DEBUG', 'inWords', 'str ' + str);
    return str;
}

// END FUNCTION =====================================================
