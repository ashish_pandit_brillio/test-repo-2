/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       31 Jan 2015     amol.sahijwani
 *
 */

/**
 * @param {String} recType Record type internal id
 * @param {Number} recId Record internal id
 * @returns {Void}
 */
function massUpdate(recType, recId) {
	var isRecordChanged = false;
	//var recType = nlapiGetRecordType();
	
	//var recId	= nlapiGetRecordId();
			
	// Load the Time Sheet Record
	var recTS = nlapiLoadRecord(recType, recId, {recordmode: 'dynamic'});
	
	var strJSONOld = JSON.stringify(recTS);
	
	// Array of Subrecord names
	var a_days = ['timebill0', 'timebill1', 'timebill2', 'timebill3', 'timebill4', 'timebill5', 'timebill6'];
	
	var i_employee_id = recTS.getFieldValue('employee');
	
	var rec_employee = nlapiLoadRecord('employee', i_employee_id);
	
	var i_practice = rec_employee.getFieldValue('department');
	
	// Get Line Count
	var i_line_count = recTS.getLineItemCount('timeitem');
	var i_previous_project_id = -1;
	var i_previous_vertical_id = -1;
	for(var i_line_indx = 1; i_line_indx <= i_line_count; i_line_indx++)
	{
		
		//nlapiLogExecution('AUDIT', 'Test', JSON.stringify(a_days_data));		
					
		recTS.selectLineItem('timeitem',i_line_indx);
		
		//var isLineItemChanged = false;
		
		for(var i_day_indx = 0; i_day_indx < 7; i_day_indx++)
			{
				var sub_record_name = a_days[i_day_indx];
				
				var o_sub_record_view = recTS.getCurrentLineItemValue('timeitem', sub_record_name);
				
				if(o_sub_record_view)
					{
						var d_current_date = nlapiAddDays(nlapiStringToDate(recTS.getFieldValue('startdate'), 'date'),i_day_indx);
						var d_first_april = nlapiStringToDate('04/01/2015');
						
						if(d_current_date >= d_first_april)
							{
									
								var i_project_id = recTS.getCurrentLineItemValue('timeitem', 'customer');
									
									//var rec_project = nlapiLoadRecord('job', i_project_id);
									
								var i_vertical = null;
					if(i_previous_project_id != i_project_id)
						{
							var i_end_customer = nlapiLookupField('job',i_project_id,'custentity_endcustomer');
						
							//var i_vertical = null;
						
							if(i_end_customer != '' && i_end_customer != null && i_end_customer != undefined)
								{
									i_vertical = nlapiLookupField('job', i_project_id, 'custentity_vertical');
								}
							else
								{
									var i_customer = nlapiLookupField('job',i_project_id,'customer');
									i_vertical = nlapiLookupField('customer', i_customer, 'custentity_vertical');
								}	
						}
					else
						{
							i_vertical = i_previous_vertical_id;
						}
									
							if(recTS.getCurrentLineItemValue('timeitem', 'department') != i_practice || 
										recTS.getCurrentLineItemValue('timeitem', 'class') != i_vertical)
								{
									isRecordChanged = true;
									nlapiSubmitField("timebill",sub_record_name,['department','class'],[i_practice,i_vertical]);
								}
							}
					}
			}
		if(isRecordChanged)
			{
				recTS.commitLineItem('timeitem');
			}
		
		
	}
	
	if(isRecordChanged)
		{
			try
			{
				//nlapiSubmitRecord(recTS);
				nlapiLogExecution('AUDIT', 'Record Id: ' + recId, 'Record Saved.');
			}
			catch(e)
			{
				nlapiLogExecution('ERROR', 'Record ID: ' + recId, e.message);
			}
		}
	
	// Load the Time Sheet Record after update
	//var recTSAfterUpdate = nlapiLoadRecord(recType, recId, {recordmode: 'dynamic'});
	
	//var strJsonAfterUpdate = JSON.stringify(recTSAfterUpdate);
	
	//nlapiLogExecution('AUDIT', 'Record Id: ' + recId, 'Old Record: ' + strJSONOld);
}