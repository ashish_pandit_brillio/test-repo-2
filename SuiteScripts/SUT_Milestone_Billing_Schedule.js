/**
 * Module Description
 * 
 * Version Date Author Remarks 1.00 18 Sep 2015 nitish.mishra
 * 
 */

function suitelet(request, response) {
	try {
		var mode = request.getMethod();

		if (mode == "GET") {
			createProjectSelectionForm();
		} else {

			mode = request.getParameter('custpage_next_mode');

			if (mode == "Validate") {
				createMilestoneListForm(request);
			} else if (mode == "ImportFromFile") {
				submitMilestonesFromFile(request);
			} else if (mode == "ImportFromScreen") {
				submitMilestonesFromScreen(request);
			}
		}
	} catch (err) {
		nlapiLogExecution('ERROR', 'suitelet', err);
	}
}

function createProjectSelectionForm() {
	try {
		var form = nlapiCreateForm("Project Milestone - Billing Schedule - Upload");
		form.addField('custpage_next_mode', 'text', 'Mode').setDisplayType(
		        'hidden').setDefaultValue('Validate');
		form.addField('custpage_job', 'select', 'Project', 'job');
		form.addField('custpage_csv', 'file', 'Upload CSV File');
		form.addSubmitButton('Upload File');
		response.writePage(form);
	} catch (err) {
		nlapiLogExecution('ERROR', 'createProjectSelectionForm', err);
		throw err;
	}
}

function createArrayFromCsv(fileId) {
	try {
		var file = nlapiLoadFile(fileId);
		var fileValue = file.getValue();
		var rows = fileValue.split("\r\n");
		var milestoneList = [];
		var isFileValid = true;

		for (var i = 1; i < rows.length; i++) {

			if (rows[i]) {
				var rowDetails = rows[i].split(",");

				milestoneList.push({
				    name : rowDetails[0],
				    amount : rowDetails[1],
				    startdate : rowDetails[2],
				    error : ''
				});
			}
		}

		// validate the array
		for (var i = 0; i < milestoneList.length; i++) {
			var name = milestoneList[i].name;
			var amount = milestoneList[i].amount;
			var startDate = milestoneList[i].startdate;

			if (name && amount && startDate) {
				try {
					milestoneList[i].amount = parseFloat(milestoneList[i].amount);
					milestoneList[i].startdate = nlapiDateToString(nlapiStringToDate(milestoneList[i].startdate));
				} catch (ex) {
					milestoneList[i].error = ex;
					isFileValid = false;
				}
			} else {
				milestoneList[i].error = "Some required field missing";
				isFileValid = false;
			}
		}

		return {
		    List : milestoneList,
		    IsFileValid : isFileValid
		};
	} catch (err) {
		nlapiLogExecution('ERROR', 'createArrayFromCsv', err);
		throw err;
	}
}

function createMilestoneListForm(request) {
	try {
		var form = nlapiCreateForm("Project Milestone - Billing Schedule - Upload");

		form.addField('custpage_job', 'select', 'Project', 'job')
		        .setDisplayType('inline').setDefaultValue(
		                request.getParameter('custpage_job'));

		// upload the file to cabinet
		var csvFile = request.getFile('custpage_csv');

		if (csvFile) {
			form.addField('custpage_next_mode', 'text', 'Mode').setDisplayType(
			        'hidden').setDefaultValue('ImportFromFile');

			csvFile.setFolder("-15");
			var csvFileId = nlapiSubmitFile(csvFile);

			form.addField('custpage_csv', 'select', 'CSV File Id', 'file')
			        .setDisplayType('inline').setDefaultValue(csvFileId);

			var milestoneDetails = createArrayFromCsv(csvFileId);

			var sublist = form.addSubList('custpage_milestone', 'list',
			        'Milestones');
			sublist.addField('name', 'text', 'Milestone');
			sublist.addField('amount', 'currency', 'Amount');
			sublist.addField('startdate', 'date', 'Due Date');
			sublist.addField('error', 'text', 'Error');

			sublist.setLineItemValues(milestoneDetails.List);

			nlapiLogExecution('debug', 'milestoneDetails.IsFileValid',
			        milestoneDetails.IsFileValid);

			if (milestoneDetails.IsFileValid) {
				form.addSubmitButton("Create Milestones");
			}
		} else {
			form.addField('custpage_next_mode', 'text', 'Mode').setDisplayType(
			        'hidden').setDefaultValue('ImportFromScreen');

			var sublist = form.addSubList('custpage_milestone', 'editor',
			        'Milestones');

			sublist.addField('name', 'text', 'Milestone');
			sublist.addField('amount', 'currency', 'Amount');
			sublist.addField('startdate', 'date', 'Due Date');
			// sublist.addField('error', 'text', 'Error');

			form.addSubmitButton("Create Milestones");
		}
		response.writePage(form);
	} catch (err) {
		nlapiLogExecution('ERROR', 'createMilestoneListForm', err);
		throw err;
	}
}

function createProjectMilestones(projectId, milestoneList) {
	try {

		for (var i = 0; i < milestoneList.length; i++) {
			milestoneList[i].milestoneid = createMilestone(projectId,
			        milestoneList[i].name, milestoneList[i].startdate);
		}

		return milestoneList;
	} catch (err) {
		nlapiLogExecution('ERROR', 'createProjectMilestones', err);
		throw err;
	}
}

function createMilestone(projectId, milestoneName, startDate) {
	try {
		var milestoneRecord = nlapiCreateRecord('projecttask');
		milestoneRecord.setFieldValue('company', projectId);
		milestoneRecord.setFieldValue('ismilestone', 'T');
		milestoneRecord.setFieldValue('title', milestoneName);
		milestoneRecord.setFieldValue('constrainttype', "FIXEDSTART");
		milestoneRecord.setFieldValue('startdate', startDate);
		return nlapiSubmitRecord(milestoneRecord);
	} catch (err) {
		nlapiLogExecution('ERROR', 'createMilestone', err);
		throw err;
	}
}

function createBillingSchedule(projectId, milestoneList, billingScheduleName) {
	try {
		var billingSchedule = nlapiCreateRecord('billingschedule', {
		    project : projectId,
		    schedtype : 'FBM',
		    recordmode : 'dynamic'
		});

		billingSchedule.setFieldValue('name', billingScheduleName);
		billingSchedule.setFieldValue('initialamount', '100%');
		// billingSchedule.setFieldValue('scheduletype', 'FBM');

		for (var i = 0; i < milestoneList.length; i++) {
			billingSchedule.selectNewLineItem('milestone');
			billingSchedule.setCurrentLineItemValue('milestone',
			        'milestoneamount', '0%');
			// billingSchedule.setCurrentLineItemValue('milestone',
			// 'milestonedate', '11/21/2013');
			billingSchedule.setCurrentLineItemValue('milestone', 'projecttask',
			        milestoneList[i].milestoneid);
			billingSchedule.setCurrentLineItemValue('milestone', 'comments',
			        "$ " + milestoneList[i].amount);
			billingSchedule.commitLineItem('milestone');
		}

		var billingScheduleId = nlapiSubmitRecord(billingSchedule);
		nlapiLogExecution('DEBUG', 'Billing Schedule Created',
		        billingScheduleId);
		return billingScheduleId;
	} catch (err) {
		nlapiLogExecution('ERROR', 'createBillingSchedule', err);
		throw err;
	}
}

function linkScheduleToProject(projectId, billingScheduleId) {

	try {
		nlapiSubmitField('job', projectId, 'billingschedule', billingScheduleId);
		nlapiLogExecution('DEBUG', 'Project Updated With Billing Schedule');
	} catch (err) {
		nlapiLogExecution('ERROR', 'linkScheduleToProject', err);
		throw err;
	}
}

function submitMilestonesFromFile(request) {
	try {
		var form = nlapiCreateForm("Project Milestone - Billing Schedule - Upload");
		var projectId = request.getParameter('custpage_job');
		var csvFileId = request.getParameter('custpage_csv');

		nlapiLogExecution('debug', 'project', projectId);
		nlapiLogExecution('debug', 'csvFileId', csvFileId);

		form.addField('custpage_next_mode', 'text', 'Mode').setDisplayType(
		        'hidden').setDefaultValue('Go Back');
		form.addField('custpage_job', 'select', 'Project', 'job')
		        .setDisplayType('inline').setDefaultValue(projectId);

		form.addField('custpage_csv', 'text', 'CSV File Id').setDisplayType(
		        'hidden').setDefaultValue(csvFileId);

		var milestoneDetails = createArrayFromCsv(csvFileId);

		if (milestoneDetails.IsFileValid) {
			nlapiLogExecution('debug', 'project', projectId);
			var milestoneList = createProjectMilestones(projectId,
			        milestoneDetails.List);
			nlapiLogExecution('debug', 'milestones created');

			var billingScheduleName = nlapiLookupField('job', projectId,
			        'entityid');
			var billingScheduleId = createBillingSchedule(projectId,
			        milestoneList, billingScheduleName + " - Billing Schedule");
			linkScheduleToProject(projectId, billingScheduleId);

			form.addField('custpage_response', 'text', '').setDisplayType(
			        'inline').setDefaultValue("Import Sucessful");
		} else {
			form.addField('custpage_response', 'text', '').setDisplayType(
			        'inline').setDefaultValue("Invalid File");
		}

		response.writePage(form);
	} catch (err) {
		nlapiLogExecution('ERROR', 'submitMilestonesFromFile', err);
		throw err;
	}
}
function submitMilestonesFromScreen(request) {
	try {
		var form = nlapiCreateForm("Project Milestone - Billing Schedule - Upload");
		var projectId = request.getParameter('custpage_job');

		nlapiLogExecution('debug', 'project', projectId);

		form.addField('custpage_job', 'select', 'Project', 'job')
		        .setDisplayType('inline').setDefaultValue(projectId);

		var milestoneDetails = readMilestonesFromLine(request);

		if (milestoneDetails.IsFileValid) {
			nlapiLogExecution('debug', 'project', projectId);
			var milestoneList = createProjectMilestones(projectId,
			        milestoneDetails.List);
			nlapiLogExecution('debug', 'milestones created');
			var billingScheduleId = createBillingSchedule(projectId,
			        milestoneList, "Test");
			linkScheduleToProject(projectId, billingScheduleId);

			form.addField('custpage_response', 'text', '').setDisplayType(
			        'inline').setDefaultValue("Import Sucessful");
		} else {
			form.addField('custpage_response', 'text', '').setDisplayType(
			        'inline')
			        .setDefaultValue("Some Milestones are not correct");
		}

		response.writePage(form);
	} catch (err) {
		nlapiLogExecution('ERROR', 'submitMilestonesFromScreen', err);
		throw err;
	}
}
function readMilestonesFromLine(request) {
	try {

		var linecount = request.getLineItemCount('custpage_milestone');

		var milestoneList = [];
		var isFileValid = true;

		for (var i = 1; i <= linecount; i++) {

			milestoneList.push({
			    name : request
			            .getLineItemValue('custpage_milestone', 'name', i),
			    amount : request.getLineItemValue('custpage_milestone',
			            'amount', i),
			    startdate : request.getLineItemValue('custpage_milestone',
			            'startdate', i),
			    error : ''
			});
		}

		// validate the array
		for (var i = 0; i < milestoneList.length; i++) {
			var name = milestoneList[i].name;
			var amount = milestoneList[i].amount;
			var startDate = milestoneList[i].startdate;

			if (name && amount && startDate) {
				try {
					milestoneList[i].amount = parseFloat(milestoneList[i].amount);
					milestoneList[i].startdate = nlapiDateToString(nlapiStringToDate(milestoneList[i].startdate));
				} catch (ex) {
					milestoneList[i].error = ex;
					isFileValid = false;
				}
			} else {
				milestoneList[i].error = "Some required field missing";
				isFileValid = false;
			}
		}

		return {
		    List : milestoneList,
		    IsFileValid : isFileValid
		};
	} catch (err) {
		nlapiLogExecution('ERROR', 'readMilestonesFromLine', err);
		throw err;
	}
}