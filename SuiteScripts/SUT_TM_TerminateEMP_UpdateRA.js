/**
 *
 * Script Name: SUT | T&M | Employee Termination_RA Update
 * Author:Praveena Madem
 * Company:INSPIRRIA CLOUDTECH
 * Date: 04 - Mar - 2020
 * Description:1.Fetch resource allocations data and update the Billing ENd date and Terminate the employee records
				
			
 			   
 * Script Modification Log:
 *  -- Date --			-- Modified By --				--Requested By--				-- Description --
*/
{
  var id="";
}
function _main_suitelet(request, response) {
    try {
       nlapiLogExecution('DEBUG', 'START DATE', new Date());
        var body = request.getBody();
        nlapiLogExecution('DEBUG', 'body', body);
        var emp_term_data = JSON.parse(body);
        var emp_data = emp_term_data.Assignment;
        var fusionNumber = emp_data.FusionID;
        var termDate = emp_data.Termination_Date;
        var result = FusionRecordSearch(fusionNumber, termDate);
        nlapiLogExecution('DEBUG', 'result', JSON.stringify(result));
        response.write(JSON.stringify(result));
       nlapiLogExecution('DEBUG', 'END DATE', new Date());
   } catch (e) {
   
        var obj = {
            "Code": "",
            "Details": ""
        }
        obj.Code = e.getCode();
        obj.Details = e.getDetails();
      	obj.id=id;
        response.write(JSON.stringify(obj));
    /* var recieptients=["deepak.srinivas@brillio.com","information.systems@brillio.com"];//157414
          var s_emailbody="This is informed you that employee termination is failed due to"+e;
          nlapiSendEmail("200580", recieptients, 'Failed Employee Termination',s_emailbody, null, null,null);*/
        nlapiLogExecution('DEBUG', '_main_suitelet', JSON.stringify(obj));
     nlapiLogExecution('DEBUG', '_main_suitelet error', JSON.stringify(e));
    }
}

var TermList = [];

function FusionRecordSearch(fusionNumber, termDate) {
    //try {
    if (fusionNumber != null) {
        var search_employee = nlapiSearchRecord('employee', null, ["formulatext: {custentity_fusion_empid}", "is", fusionNumber], [new nlobjSearchColumn(
            'internalid')]);
        if (_logValidation(search_employee)) {


           id = search_employee[0].getId();
            nlapiLogExecution('DEBUG', 'EMP Internalid', id);
            //nlapiSetFieldValue('custrecord_fu_emp_id',id);
           // var employeeRecord = nlapiLoadRecord('employee', parseInt(id));
            //Update Last working date in Resource Allocation

            var resourceAllocationSearch = nlapiSearchRecord('resourceallocation', null, [
                new nlobjSearchFilter('internalid', 'employee', 'anyof', id),
                new nlobjSearchFilter('enddate', null, 'onorafter', formatDate(termDate))
            ]);
            if (isArrayNotEmpty(resourceAllocationSearch)) {
                nlapiLogExecution('DEBUG', 'isArrayNotEmpty(resourceAllocationSearch)', isArrayNotEmpty(resourceAllocationSearch));
                for (var index = 0; index < resourceAllocationSearch.length; index++) {
                    nlapiLogExecution('DEBUG', 'Allocation', resourceAllocationSearch.length);
                    var resAlloRec = nlapiLoadRecord('resourceallocation', resourceAllocationSearch[index].getId());
                    var s_startDate = resAlloRec.getFieldValue('startdate');
                    var d_startDate = nlapiStringToDate(s_startDate);
                    var s_termDate = formatDate(termDate);
                    var d_termDate = nlapiStringToDate(s_termDate);
                    nlapiLogExecution('DEBUG', 'Start Date', resAlloRec.getFieldValue('startdate'));
                    nlapiLogExecution('debug', 'd_startDate', d_startDate);
                    nlapiLogExecution('DEBUG', 'term date', d_termDate);
                    if (d_startDate > d_termDate) {
                        nlapiLogExecution('DEBUG', 'Termdate', formatDate(termDate));
                        nlapiLogExecution('debug', 'Resource ID', resourceAllocationSearch[index].getId());
                        nlapiDeleteRecord('resourceallocation', resourceAllocationSearch[index].getId());
                    } else {
                      nlapiLogExecution('DEBUG', 'Update allocation');
                        resAlloRec.setFieldValue('enddate', formatDate(termDate)); // Allocation End date
                        resAlloRec.setFieldValue('custeventbenddate', formatDate(termDate)); // Billing End date
                        resAlloRec.setFieldValue('custevent_allocation_status', '22'); // Allocation status, Employee terminated 
                        resAlloRec.setFieldValue('notes', 'Employee terminated, hence allocation end date changed');
                       //resAlloRec.setFieldValue('custevent_ra_last_modified_by',"442");
                    
                        //resAlloRec.setFieldValue('custevent_ra_last_modified_date', nlapiDateToString(new Date(), 'datetimetz'));
                       nlapiLogExecution('DEBUG', 'Before submitta allcaotion');
                        //Updated submit parameters
                       // var i_ra_id = nlapiSubmitRecord(resAlloRec);
                       var i_ra_id=nlapiSubmitRecord(resAlloRec);
                        nlapiLogExecution('DEBUG', 'Allocation ended');
                        nlapiLogExecution('DEBUG', 'Allocation', i_ra_id);
                    }

                }
            }
            //nlapiLogExecution('DEBUG', '1', "sfsfdf");
            // Update the subtier records
            // Get all subtier records that end after the LWD

            // Update the subtier records
            // Get all subtier records that end after the LWD
            var subtierSearch = nlapiSearchRecord('customrecord_subtier_vendor_data', null, [new nlobjSearchFilter('custrecord_stvd_contractor', null, 'anyof', id), new nlobjSearchFilter('custrecord_stvd_end_date', null,
                'onorafter', formatDate(termDate))]);

            // change the end date of all the found subtier record
            if (subtierSearch) {

                for (var index = 0; subtierSearch != null & index < subtierSearch.length; index++) {
                    var subTierRec = nlapiLoadRecord(
                        'customrecord_subtier_vendor_data',
                        subtierSearch[index].getId());

                    // if start date is after the termination date,
                    // change
                    // the start date
                    if (nlapiStringToDate(subTierRec.getFieldValue('custrecord_stvd_start_date')) > nlapiStringToDate(formatDate(termDate))) {
                        subTierRec.setFieldValue('isinactive', 'T');
                        subTierRec.setFieldValue('custrecord_stvd_start_date', formatDate(termDate));
                    }

                    subTierRec.setFieldValue('custrecord_stvd_end_date', formatDate(termDate));

                    nlapiSubmitRecord(subTierRec, false, true);
                }
            }

            nlapiLogExecution('DEBUG', '3', "sfsfdf");
            TermList.push({
                'Employee ID': id
            });
            var obj = {
                'success': "",
                "stuff": {}
            };
            obj.success = "T";
            obj.stuff = TermList;
        }
    }
    return obj;
    /*}	
    	catch(e){
          var obj={'success':"","stuff":{}};
              obj.success="F";
              obj.stuff=e.getDetails();
           return obj;
    			nlapiLogExecution('debug', 'We are in catch block : FusionRecordSearch function', e);
    			
    	}
        */
}



function _logValidation(value) {
    if (value != null && value != '' && value != undefined && value.toString() != 'NaN' && value != NaN) {
        return true;
    } else {
        return false;
    }
}

function dateFormat(date) {
    var date_return = '';
    if (date) {
        var dateObj = date.split('-');
        var month = dateObj[1];
        var year = dateObj[0];
        var date_d = dateObj[2].split('T');
        //var date_d = dateObj[2].split('T');
        nlapiLogExecution('DEBUG', 'dateObj[2]', dateObj[2]);
        var temp = new Array();
        temp = dateObj[2].substring(0, 2);
        //date_d = date_d.split(',')[0];


        date_return = month + '/' + temp + '/' + year;
    }
    return date_return;
}

function formatDate(fusionDate) {

    if (fusionDate) {
        // fusionDate = "2016-07-14T00:00:00.000Z";
        var datePart = fusionDate.split('T')[0];
        var dateSplit = datePart.split('-');
        // MM/DD/ YYYY
        var netsuiteDateString = dateSplit[1] + "/" + dateSplit[2] + "/" +
            dateSplit[0];
        return netsuiteDateString;
    }
    return "";
}