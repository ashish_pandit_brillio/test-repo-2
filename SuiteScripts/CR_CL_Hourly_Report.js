/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       09 Aug 2017     bidhi.raj
 *
 */

/**
 * @param {nlobjRequest} request Request object
 * @param {nlobjResponse} response Response object
 * @returns {Void} Any output is written via response object
 */

function pageInit(type){

}

function fxn_generateHourPDF(param){
   // alert("param" + param)
    //<em class='rn_Highlight'>call</em> the <em class='rn_Highlight'>Suitelet</em> created in Step 1
    var createPDFURL1 = nlapiResolveURL("SUITELET", 'customscript_hourlyrprtfrmtmentry', 'customdeployhourlyreportfrmtimeentry', false);
    
    //pass the internal id of the current record
    createPDFURL1 += '&id=' + 'Export' + "&param=" + param;
    // alert(createPDFURL1);
    
    //show the PDF file 
    newWindow = window.open(createPDFURL1);
}

function fxn_generateHourPDF1(param){
   // alert("param" + param)
    //<em class='rn_Highlight'>call</em> the <em class='rn_Highlight'>Suitelet</em> created in Step 1
    var createPDFURL1 = nlapiResolveURL("SUITELET", 'customscript_hourlyrprtfrmtmentry', 'customdeployhourlyreportfrmtimeentry', false);
    
    //pass the internal id of the current record
    createPDFURL1 += '&id=' + 'Export' + "&param=" + param;
    // alert(createPDFURL1);
    
    //show the PDF file 
    newWindow = window.open(createPDFURL1);
}

function fxn_generateHourPDF_Dec(param){
	   // alert("param" + param)
	    //<em class='rn_Highlight'>call</em> the <em class='rn_Highlight'>Suitelet</em> created in Step 1
	    var createPDFURL1 = nlapiResolveURL("SUITELET", 'customscript_hourlyrprtfrmtmentry', 'customdeployhourlyreportfrmtimeentry', false);
	    
	    //pass the internal id of the current record
	    createPDFURL1 += '&id=' + 'Export' + "&param=" + param;
	    // alert(createPDFURL1);
	    
	    //show the PDF file 
	    newWindow = window.open(createPDFURL1);
	}
function fieldChanged(type, name, linenum){


    if (name == 'custpage_startdate') {
        var s_period = nlapiGetFieldValue('custpage_startdate')
        //alert("S_period=======" + s_period)
        if (s_period != null && s_period != '' && s_period != 'undefined') {
            var date = nlapiStringToDate(s_period)
            //alert("date=======" + date)
            if (date != null && date != '' && date != 'undefined') {
                var day = date.getDay()
                //alert("day=======" + day)
                if (day != 6) {
                    alert("Please select enddate as Saturday.")
                    nlapiSetFieldValue('custpage_startdate', '')
                }
            }
        }
        
    }
 
}







// BEGIN SAVE RECORD ================================================
// END FIELD CHANGED ================================================

