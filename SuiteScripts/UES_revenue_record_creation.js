	/*	Script Name:    UES T&M Revenue Creation 
	 Author:        Sai Saranya
	 Company:		Brillio
	 Date:
	 Version:
	 
	 Description:	This script Call Schedule Script to Create a new record for revenue amount for T&M projects.
	 * Script Modification Log:
	 * 
	 -- Date --		-- Modified By --			--Requested By--				-- Description --
	 11 June 2018         Supriya                    Deepak                  
	 Below is a summary of the process controls enforced by this script file.  The control logic is described
	 more fully, below, in the appropriate function headers and code blocks.
	 
	 BEFORE LOAD
	 - beforeLoadRecord(type)
	 Not Used
	 
	 BEFORE SUBMIT
	 - beforeSubmitRecord(type)
	 Not Used
	 
	 AFTER SUBMIT
	 - afterSubmitRecord(type)
	 afterSubmitRecord(type)
	 
	 SUB-FUNCTIONS
	 - The following sub-functions are called by the above core functions in order to maintain code
	 modularization:
	
	 */
function beforeLoadRecord(type, form)	
{
	
	    return true;  
	    
}
	// END BEFORE LOAD ====================================================
	// BEGIN BEFORE SUBMIT ================================================
	
function beforeSubmitRecord_ait_adjustTDS_Amt(type){
		
		return true;
}
	
	// END BEFORE SUBMIT ==============================================
	
	// BEGIN AFTER SUBMIT =============================================
function afterSubmitRecord(type)	
{
	if ((type == 'create') || (type == 'edit'))
	{
		var id = nlapiGetRecordId();
     	var status_confirm= nlapiGetFieldValue('custrecord_is_confirm');
      	if(status_confirm == 'T')
          {
				var params = new Array();
				params['custscript_tm_json_id'] = id;
				nlapiLogExecution('DEBUG','InternalID',params);
				var status = nlapiScheduleScript('customscript_tm_forecast_creation','customdeploy1',params);
          }
	}
		
}// afterSubmitRecord(type)	
	
	
	