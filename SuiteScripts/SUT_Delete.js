//https://debugger.na1.netsuite.com/app/common/scripting/script.nl?id=1109
function deleteFunction()
{
try{
var search = nlapiSearchRecord('timesheet','customsearch2024',null,
[new nlobjSearchColumn('internalid'),
new nlobjSearchColumn("startdate"), 
new nlobjSearchColumn("employee"), 
new nlobjSearchColumn("enddate"), 
new nlobjSearchColumn("approvalstatus"),
new nlobjSearchColumn("subsidiary","employee",null)]
);
nlapiLogExecution('debug','Record search',search.length);
var msg = 'Suitelet Deletion';
if(search){

for(var i=0;i<search.length;i++){
var id = search[i].getValue('internalid');
if(id){
var newTimesheet = nlapiLoadRecord('timesheet',parseInt(id));
var employee = newTimesheet.getFieldText('employee');
var employee = newTimesheet.setFieldValue('custrecord_ts_remark',msg);
var status ;
var timesheetId = nlapiSubmitRecord(newTimesheet);
//nlapiDeleteRecord('timesheet',parseInt(timesheetId));
//nlapiLogExecution('debug','Record Deleted',id);
var timesheetId_Emp =timesheetId + employee+' , ';
nlapiLogExecution('debug', 'timesheetId + Date', timesheetId_Emp);

					

var mailTemplate = "<p>Hi, </p>";
	mailTemplate += "<p> This is to inform you that a timesheet has been deleted. </p>";

	mailTemplate += "<b>Employee : </b>"
			        + search[i].getText('employee') + "<br/>";
	mailTemplate += "<b>Week : </b>" + search[i].getValue('startdate')
			        + " - " + search[i].getValue('enddate') + "<br/>";
			mailTemplate += "<b>Status : </b>"
			        + search[i].getText('approvalstatus') + "<br/>";
			mailTemplate += "<b>Deleted By : </b>"
			        + nlapiGetContext().getName() + "<br/>";
			mailTemplate += "<b>Deleted On : </b>"
			        + nlapiDateToString(new Date(), 'datetimetz') + "<br/>";
			mailTemplate += "<b>Reason for Deletion : </b>" + msg + "<br/>";

			mailTemplate += "<p>Regards, <br/> Information Systems</p>";
		var subsidiary = search[i].getValue("subsidiary","employee",null);
			if (subsidiary == '2') {
				recipient = 'shivam.pandya@brillio.com,lisa.joshy@brillio.com,information.systems@brillio.com,vertica@brillio.com,himanshu.negi@brillio.com,kaushal.thakar@brillio.com';
			} else {
				recipient = 'shivam.pandya@brillio.com,lisa.joshy@brillio.com,information.systems@brillio.com,himanshu.negi@brillio.com,kaushal.thakar@brillio.com';
			}			
nlapiDeleteRecord('timesheet',parseInt(id));

nlapiSendEmail(442, recipient,
			        'A Timesheet has been deleted', mailTemplate, null,
			        "nitish.mishra@brillio.com");
nlapiLogExecution('debug','Mail sent to -> ',recipient);					
nlapiLogExecution('debug','Record Deleted',i);

}
}
}
}
catch(e){
nlapiLogExecution('debug','Error Record id',id);
nlapiLogExecution('debug','Error in Deletion',e);
}
}