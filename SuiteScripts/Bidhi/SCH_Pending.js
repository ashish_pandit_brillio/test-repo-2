/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       12 Apr 2017     bidhi.raj
 *
 */

/**
 * @param {String}
 *            type Context Types: scheduled, ondemand, userinterface, aborted,
 *            skipped
 * @returns {Void}
 */
function Main() {
	try {
		var cp = new Array();
		var columns = new Array();
		var cp_array = new Array();
		var proj_for_cp_array = new Array();
		var cp_proj = new Array();
		var count = 0;
		columns[0] = new nlobjSearchColumn('entityid');
		columns[1] = new nlobjSearchColumn('altname');
		columns[2] = new nlobjSearchColumn('startdate');
		columns[3] = new nlobjSearchColumn('enddate');
		columns[4] = new nlobjSearchColumn('custentity_projectmanager');
		columns[5] = new nlobjSearchColumn('custentity_region');
		columns[6] = new nlobjSearchColumn('custentity_clientpartner');
		columns[7] = new nlobjSearchColumn('custentity_deliverymanager');
		columns[8] = new nlobjSearchColumn('datecreated');
		columns[9] = new nlobjSearchColumn('internalid');

		var pending_projects_search = nlapiSearchRecord('job', 'customsearch1797', null, columns);

		nlapiLogExecution('debug', 'pending_projects_search.length# ',
			pending_projects_search.length);

		var tem;
		//pending_projects_search = pending_projects_search.slice().sort();
		if ((pending_projects_search)) {

			for (var i = 0; i < pending_projects_search.length; i++) {

				tem = pending_projects_search[i]
					.getValue('custentity_clientpartner');

				if (tem != '' || tem != null) {
					cp[i] = tem;
				}
				proj_for_cp_array[i] = pending_projects_search[i]
					.getValue('internalid');

			}
			cp = cp.slice().sort();
			nlapiLogExecution('debug', 'totalprojects# ', cp.length);
		}

		var uniq_cp = new Array();
		uniq_cp = removearrayduplicate(cp);
	//	nlapiLogExecution('debug', 'duplicate ',uniq_cp.length);
		var html = '';
		var table;
		var pending = 'Pending';
		if ((pending_projects_search)) {
			for (var j = 0; j < uniq_cp.length; j++) {
				//html = '';
				table = '';
				for (var i = 0; i < pending_projects_search.length; i++) {
					var proj_client_partner = pending_projects_search[i]
						.getValue('custentity_clientpartner');

					if (proj_client_partner == uniq_cp[j]) {
						html = '';

						try {
							var clientpartner = nlapiLookupField('employee',parseInt(proj_client_partner),
								['firstname', 'email', 'internalid']);
						} catch (e) {
							// TODO: handle exception
						}
						var proj_id = pending_projects_search[i]
							.getValue('entityid');

						var proj_name = pending_projects_search[i]
							.getValue('altname');
						var proj_start_date = pending_projects_search[i]
							.getValue('startdate');
						var proj_end_date = pending_projects_search[i]
							.getValue('enddate');
						var proj_manager = pending_projects_search[i]
							.getValue('custentity_projectmanager');
						var proj_region = pending_projects_search[i]
							.getText('custentity_region');
						var proj_client_partner = pending_projects_search[i]
							.getValue('custentity_clientpartner');
						var proj_delivery_manager = pending_projects_search[i]
							.getValue('custentity_deliverymanager');
						var proj_dateCreated = pending_projects_search[i]
							.getValue('datecreated');
						var proj_manager_name = nlapiLookupField('employee',
							parseInt(proj_manager), ['firstname',
							'internalid', 'lastname']);

						var proj_delivery_manager_name = nlapiLookupField(
							'employee', parseInt(proj_delivery_manager), [
							'firstname', 'internalid', 'lastname']);
						var business_op_email = new Array();
						business_op_email[0] = 'business.ops@brillio.com';
						var mailSubject = 'Pending Project Trigger';
						try {
							cp_array[i] = clientpartner.internalid;

							var cpdetails = nlapiLookupField('employee',
								parseInt(proj_client_partner), ['firstname',
								'internalid', 'email']);
							var firstname = cpdetails.firstname;
							cp_email = cpdetails.email;

						} catch (e) {
							// TODO: handle exception
							var proj_mangr_dtl = nlapiLookupField('employee', parseInt(proj_manager), ['firstname', 'internalid', 'email']);
							firstname = proj_mangr_dtl.firstname;
							cp_email = proj_mangr_dtl.email;
						}

						html += '<tr>';
						html += '<td>' + proj_id + '</td>';
						html += '<td>' + proj_name + '</td>';
						html += '<td>' + proj_start_date + '</td>';
						html += '<td>' + proj_end_date + '</td>';
						html += '<td>' + pending + '</td>';
						html += '<td>' + proj_dateCreated + '</td>';
						html += '<td>' + proj_manager_name.firstname + " " + proj_manager_name.lastname + '</td>';
						html += '<td>' + proj_delivery_manager_name.firstname + " " + proj_delivery_manager_name.lastname + '</td>';
						html += '<td>' + proj_region + '</td>';
						html += '</tr>';
						var tem_html = html;
						table = table + tem_html;
						//nlapiLogExecution('debug', 'idsofproj# ',proj_id);
					}

				}//arrValues.contains("Sam")

				var html = '';
				html += '<html>';
				html += '<head>';
				html += '<table border="0" width="100%">';
				html += '</head>';
				html += '<body>';
				html += '<p>Dear ' + firstname + ',</p>';
				html += '<p>These projects are in pending status in NetSuite due to any or all of the following pending â€“ executed SOW/PO or updated CR. Request you to kindly share the pending document with Business Operations team at the earliest. Thanks!.</p>';
				html += '</br>';
				html += '<table border="2" width="100%">';
				html += '<thead>';
				html += '<tr text-align: left padding: 8px><td><b>Project ID</b></td><td><b>Project Name</b></td><td><b>Project start date</b></td><td><b>Project End date</b></td><td><b>Project Status</b></td><td><b>Project creation date</b></td><td><b>Project Manager</b></td><td><b>Delivery Manager</b></td><td><b>Region</b></td></tr>';
				html += '</thead>';
				html += table;
				html += '</table>';
				html += '</br>';
				html += '<p>Thanks & Regards,</p>';
				html += '<p>Information Systems</p>';
				html += '</body>';
				html += '</html>';

				nlapiSendEmail(442, cp_email, mailSubject, html, business_op_email, null, null, null, true, null, null);
				count = count + 1;
				table = '';
				nlapiLogExecution('debug', 'cp ', firstname);
			}

		}
	} catch (err) {
		nlapiLogExecution('error', 'Main', err);
		var context = nlapiGetContext();

		var mailTemplate = "";
		mailTemplate += "<p> This is to inform that Pending Project Trigger has been failed.And It is having Error - <b>[" + err.code + " : " + err.message + "]</b></p>";

		mailTemplate += "<p> Kindly have a look on to the error message and Please do needfull.</p>";
		mailTemplate += "<p><b> Script Id:- " + context.getScriptId() + "</b></p>";
		mailTemplate += "<p><b> Script Deployment Id:- " + context.getDeploymentId() + "</b></p>";
		mailTemplate += "<p> Note: This is system genarated email, incase of any failure while sending the notification to employee(s).</p>";
		mailTemplate += "<br/>"
		mailTemplate += "<p>Regards, <br/> Digital Office</p>";
		nlapiSendEmail(442, 15279, 'Failure Notification on Pending Project Trigger', mailTemplate, null, null, null, null);
	}
	function removearrayduplicate(cp) {
		var newArray = new Array();
		label: for (var i = 0; i < cp.length; i++) {
			for (var j = 0; j < cp.length; j++) {
				if (newArray[j] == cp[i])
					continue label;
			}
			newArray[newArray.length] = cp[i];
		}
		return newArray;
	}

	function getBO_email(proj_region) {

		var BO = null;

		if (proj_region == 'US-Central') {

			BO = 'sushma.bhandari@brillio.com';// Sushma

		} else if (proj_region == 'US-East') {

			BO = 'pushpendra.kumar@brillio.com';// Pushpendra

		} else if (proj_region == 'US-T&NW') {

			BO = 'Jibin.philip@brillio.com';// Jibin

		} else if (proj_region == 'US-West') {

			BO = 'mit.trivedi@brillio.com';// Mit

		} else if (proj_region == 'India' || proj_region == 'UK'
			|| proj_region == 'Brillio') {

			BO = 'chetan.barot@brillio.com';// Chetan
		}
		return BO;
	}
	nlapiLogExecution('debug', 'sentmail# ', count);
}