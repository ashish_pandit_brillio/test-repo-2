/**
 * @author Jayesh
 */

function suitelet(request, response) {
	try {
		var userId = nlapiGetUser();
		
		var requestedProject = request.getParameterValues('custpage_project');
		var requestedCustomer = request.getParameterValues('custpage_customer');
		//var requestedRegion = request.getParameterValues('custpage_region');
		
		var requestedInrExchangeRate = request.getParameter('custpage_exchange_rate_inr');
		var requestedGbpExchangeRate = request.getParameter('custpage_exchange_rate_gbp');
		
		var formTitle = 'Revenue Projection';

		// Form
		var form = nlapiCreateForm(formTitle);
		form.addFieldGroup('custpage_1', 'Select Options');

		// Customer Field
		var customerField = form.addField('custpage_customer', 'select','Customer', null, 'custpage_1');
		customerField.addSelectOption('', ' ');
		/*var taggedProjects = getTaggedProjectList();
		taggedProjects.forEach(function(project) {
			var Customer_Name = project.getText('customer');
			var cust_id = project.getValue('customer');
			customerField.addSelectOption(cust_id, Customer_Name);
		});*/
		if (requestedCustomer) {
			customerField.setDefaultValue('');
		}
		
		// Project Field
		var projectField = form.addField('custpage_project', 'select','Project', null, 'custpage_1');
		projectField.addSelectOption('', ' ');
		var taggedProjects = getTaggedProjectList();
		/*taggedProjects.forEach(function(project) {
			var projectName = project.getValue('entityid') + " : "
			        + project.getValue('altname');
			projectField.addSelectOption(project.getId(), projectName);
		});*/
		if (requestedProject) {
			projectField.setDefaultValue('');
		}
		
		// Currency Field for INR
		var inrField = form.addField('custpage_exchange_rate_inr', 'currency',
		        'USD to INR ( 1 USD = x INR )', null, 'custpage_1');
		inrField.setBreakType('startcol');
		inrField.setDefaultValue('');
		
		if (requestedInrExchangeRate) {
			inrField.setDefaultValue(requestedInrExchangeRate);
		}
		
		// Currency Field For GBP
		var gbpField = form.addField('custpage_exchange_rate_gbp', 'currency',
		        'USD to GBP ( 1 USD = x GBP )', null, 'custpage_1');
		//gbpField.setBreakType('startcol');
		gbpField.setDefaultValue('');
		
		if (requestedGbpExchangeRate) {
			gbpField.setDefaultValue(requestedGbpExchangeRate);
		}
		
		//nlapiLogExecution('audit','gbp:- ',requestedGbpExchangeRate);
		var requestedExchangeRateTable = {
		    USD : 1,
		    INR : parseFloat(requestedInrExchangeRate),
		    GBP : parseFloat(requestedGbpExchangeRate)
		};
		
		//Region Field
		/*var regionField = form.addField('custpage_region', 'select','Region', null, 'custpage_1');
		regionField.addSelectOption(1, ' ');
		regionField.addSelectOption(2, 'UK');
		regionField.addSelectOption(3, 'US-East');
		regionField.addSelectOption(4, 'US-West');
		regionField.addSelectOption(5, 'India');
		regionField.addSelectOption(6, 'US-T&NW');
		regionField.addSelectOption(7, 'US-Central');
		regionField.addSelectOption(8, 'Europe');
		regionField.addSelectOption(9, 'Brillio');*/
		
		// Select Options field group end
		
		form.addSubmitButton('Load Data');
			
		nlapiLogExecution('audit','cust selctd:- ',JSON.stringify(requestedCustomer));
		nlapiLogExecution('audit','pro selctd:- ',JSON.stringify(requestedProject));
		var tableHtml = createProjectionTable(requestedProject,requestedCustomer,requestedExchangeRateTable);
		var file_obj = generate_excel(tableHtml);
		response.setContentType('XMLDOC','T&M Revenue Projection Data.xls');
		response.write( file_obj.getValue());
		
		//form.addFieldGroup('custpage_2', 'Revenue Data');
		//form.addField('custpage_table', 'inlinehtml', null, null, 'custpage_2').setDefaultValue(tableHtml);
		//response.writePage(form);
		
	} catch (err) {
		nlapiLogExecution('ERROR', 'suitelet', err);
		displayErrorForm(err, "Revenue Projection");
	}
}

function getTaggedProjectList() {
	try {
		
		var project_search_results = searchRecord('job', '1517', null,
		        [ new nlobjSearchColumn("entityid"),
		                new nlobjSearchColumn("altname"),
						new nlobjSearchColumn("customer") ]);

		nlapiLogExecution('debug', 'project count',
		        project_search_results.length);

		if (project_search_results.length == 0) {
			throw "You don't have any projects under you.";
		}

		return project_search_results;
	} catch (err) {
		nlapiLogExecution('ERROR', 'getTaggedProjectList', err);
		throw err;
	}
}

function getTaggedCustList() {
	try {
		
		var project_search_results = searchRecord('job', '1517', null, new nlobjSearchColumn('customer',null,'group'));

		nlapiLogExecution('debug', 'project count',
		        project_search_results.length);

		if (project_search_results.length == 0) {
			throw "You don't have any projects under you.";
		}

		return project_search_results;
	} catch (err) {
		nlapiLogExecution('ERROR', 'getTaggedProjectList', err);
		throw err;
	}
}

function getProjectDetails(projectList, requestedProject) {
	try {
		var projectData = {};

		var filters = [ new nlobjSearchFilter('internalid', null, 'anyof',
		        projectList) ];

		if (requestedProject) {
			filters.push(new nlobjSearchFilter('internalid', null, 'anyof',
			        requestedProject));
		}

		searchRecord(
		        'job',
		        null,
		        filters,
		        [ new nlobjSearchColumn('startdate'),
		                new nlobjSearchColumn('enddate'),
		                new nlobjSearchColumn('jobbillingtype'),
		                new nlobjSearchColumn('entityid'),
		                new nlobjSearchColumn('altname'),
		                new nlobjSearchColumn('custentity_t_and_m_monthly') ])
		        .forEach(
		                function(project) {
			                projectData[project.getId()] = {
			                    EntityId : project.getValue('entityid'),
			                    AltName : project.getValue('altname'),
			                    StartDate : project.getValue('startdate'),
			                    EndDate : project.getValue('enddate'),
			                    BillingType : project.getText('jobbillingtype')
			                            + (project
			                                    .getValue('custentity_t_and_m_monthly') == 'T' ? ' Monthly'
			                                    : '')
			                };
		                });

		return projectData;
	} catch (err) {
		nlapiLogExecution('ERROR', 'getProjectDetails', err);
		throw err;
	}
}

function getProjectDetails_practiceWise(prac_list) {
	try {
		var projectData = {};

		var filters = [ new nlobjSearchFilter('custentity_practice', null, 'anyof',
		        prac_list) ];


		searchRecord(
		        'job',
		        null,
		        filters,
		        [ new nlobjSearchColumn('startdate'),
		                new nlobjSearchColumn('enddate'),
		                new nlobjSearchColumn('jobbillingtype'),
		                new nlobjSearchColumn('entityid'),
		                new nlobjSearchColumn('altname'),
		                new nlobjSearchColumn('custentity_t_and_m_monthly') ])
		        .forEach(
		                function(project) {
			                projectData[project.getId()] = {
			                    EntityId : project.getValue('entityid'),
			                    AltName : project.getValue('altname'),
			                    StartDate : project.getValue('startdate'),
			                    EndDate : project.getValue('enddate'),
			                    BillingType : project.getText('jobbillingtype')
			                            + (project
			                                    .getValue('custentity_t_and_m_monthly') == 'T' ? ' Monthly'
			                                    : '')
			                };
		                });

		return projectData;
	} catch (err) {
		nlapiLogExecution('ERROR', 'getProjectDetails', err);
		throw err;
	}
}

function getProjectRevenueData(projectList, startDate, endDate,
        requestedProject)
{
	try {
		var filters = [
		        new nlobjSearchFilter('custrecord_prp_project',
		                'custrecord_eprp_project_rev_projection', 'anyof',
		                projectList),
		        new nlobjSearchFilter('custrecord_prp_start_date',
		                'custrecord_eprp_project_rev_projection', 'within',
		                startDate, endDate),
		        new nlobjSearchFilter('isinactive',
		                'custrecord_eprp_project_rev_projection', 'is', 'F'),
		        new nlobjSearchFilter('isinactive', null, 'is', 'F'),
		        new nlobjSearchFilter('custrecord_prp_is_past',
		                'custrecord_eprp_project_rev_projection', 'is', 'F') ];

		if (requestedProject) {
			filters.push(new nlobjSearchFilter('custrecord_prp_project',
			        'custrecord_eprp_project_rev_projection', 'anyof',
			        requestedProject));
		}

		var revenueSearch = nlapiSearchRecord(
		        'customrecord_emp_practice_rev_projection', null, filters, [
		                new nlobjSearchColumn('custrecord_prp_project',
		                        'custrecord_eprp_project_rev_projection'),
		                new nlobjSearchColumn('custrecord_prp_start_date',
		                        'custrecord_eprp_project_rev_projection'),
		                new nlobjSearchColumn('custrecord_prp_month_name',
		                        'custrecord_eprp_project_rev_projection'),
		                new nlobjSearchColumn('custrecord_eprp_practice'),
		                new nlobjSearchColumn('custrecord_eprp_revenue'),
		                new nlobjSearchColumn('custrecord_eprp_usd_revenue'),
		                new nlobjSearchColumn('custrecord_eprp_currency') ]);

		if (revenueSearch) {
			nlapiLogExecution('debug', 'revenueSearch', revenueSearch.length);
		}

		return revenueSearch;
	} catch (err) {
		nlapiLogExecution('ERROR', 'getProjectRevenueData', err);
		throw err;
	}
}

function getProjectRevenueData_ByPractice(prac_list, startDate, endDate,
        requestedProject)
{
	try {
		var filters = [
		        new nlobjSearchFilter('custrecord_eprp_practice',
		                null, 'anyof',
		                prac_list),
		        new nlobjSearchFilter('custrecord_prp_start_date',
		                'custrecord_eprp_project_rev_projection', 'within',
		                startDate, endDate),
		        new nlobjSearchFilter('isinactive',
		                'custrecord_eprp_project_rev_projection', 'is', 'F'),
		        new nlobjSearchFilter('isinactive', null, 'is', 'F'),
		        new nlobjSearchFilter('custrecord_prp_is_past',
		                'custrecord_eprp_project_rev_projection', 'is', 'F') ];

		var revenueSearch = nlapiSearchRecord(
		        'customrecord_emp_practice_rev_projection', null, filters, [
		                new nlobjSearchColumn('custrecord_prp_project',
		                        'custrecord_eprp_project_rev_projection'),
		                new nlobjSearchColumn('custrecord_prp_start_date',
		                        'custrecord_eprp_project_rev_projection'),
		                new nlobjSearchColumn('custrecord_prp_month_name',
		                        'custrecord_eprp_project_rev_projection'),
		                new nlobjSearchColumn('custrecord_eprp_practice'),
		                new nlobjSearchColumn('custrecord_eprp_revenue'),
		                new nlobjSearchColumn('custrecord_eprp_usd_revenue'),
		                new nlobjSearchColumn('custrecord_eprp_currency') ]);

		if (revenueSearch) {
			nlapiLogExecution('debug', 'revenueSearch', revenueSearch.length);
		}

		return revenueSearch;
	} catch (err) {
		nlapiLogExecution('ERROR', 'getProjectRevenueData', err);
		throw err;
	}
}

function createProjectionTable(requestedProject,requestedCustomer,requestedExchangeRateTable)
{
	try {
		
		var projectWiseRevenue = {};
		var sr_no = 0;
		var fltr_counter = 0;
		var emp_unique_proj = new Array();
		
		var arrFilters = new Array();
		
		if(requestedProject)
		{
			arrFilters[fltr_counter] = new nlobjSearchFilter('custrecord_project_name_tm_transaction',null,'anyof',requestedProject);
			fltr_counter++;
		}
		
		if(requestedCustomer)
		{
			arrFilters[fltr_counter] = new nlobjSearchFilter('custrecord_customer_tm_transaction',null,'anyof',requestedCustomer);
			fltr_counter++;
		}
		
		var today = new Date();
		var yyyy = today.getFullYear();

		var startDate_revenue = "1/1/"+yyyy;
		yyyy = yyyy + 1;
		var endDate_revenue = "12/31/"+yyyy;
		arrFilters[fltr_counter] = new nlobjSearchFilter('custrecord_strt_date_tm_transaction',null,'within',startDate_revenue,endDate_revenue);
		fltr_counter++;
		//arrFilters[fltr_counter] = new nlobjSearchFilter('custrecord_resource_tm_transaction',null,'startswith','110025');
		
		var arrColumns = new Array();	
		arrColumns[0] = new nlobjSearchColumn('custrecord_project_name_tm_transaction');
		arrColumns[1] = new nlobjSearchColumn('custrecord_region_tm_transaction');
		arrColumns[2] = new nlobjSearchColumn('custrecord_practice_tm_transaction');
		arrColumns[3] = new nlobjSearchColumn('custrecord_customer_tm_transaction');
		arrColumns[4] = new nlobjSearchColumn('custrecord_resource_tm_transaction');
		arrColumns[5] = new nlobjSearchColumn('custrecord_usd_revenue');
		arrColumns[6] = new nlobjSearchColumn('custrecord_emp_practice_tm_transaction');
		arrColumns[7] = new nlobjSearchColumn('custrecord_month_tm_transaction');
		arrColumns[8] = new nlobjSearchColumn('custrecord_reognised_tm_transaction');
		arrColumns[9] = new nlobjSearchColumn('internalid');
		arrColumns[10] = new nlobjSearchColumn('custrecord_currency_tm_transaction');
		arrColumns[11] = new nlobjSearchColumn('custrecord_pro_strt_date_tm_transaction');
		arrColumns[12] = new nlobjSearchColumn('custrecord_pro_end_date_tm_transaction');
		arrColumns[13] = new nlobjSearchColumn('custrecord_allo_strt_date_tm_transaction');
		arrColumns[14] = new nlobjSearchColumn('custrecord_allo_end_date_tm_transaction');
		arrColumns[15] = new nlobjSearchColumn('custrecord_emp_bill_rate_tm_transaction');
		arrColumns[16] = new nlobjSearchColumn('custrecord_tm_monthly_transaction');
		arrColumns[17] = new nlobjSearchColumn('custrecord_emp_onsite_tm_transaction');
		arrColumns[18] = new nlobjSearchColumn('custrecord_prcnt_allocated_tm');
		arrColumns[19] = new nlobjSearchColumn('custrecord_resource_id_tm');
		arrColumns[20] = new nlobjSearchColumn('custrecord_year_tm_transaction');
		
		var revenueSearchData = searchRecord('customrecord_revenue_projection_tm', 'customsearch1744', arrFilters, arrColumns);

		if (revenueSearchData)
		{
			nlapiLogExecution('audit','srch len:- ',revenueSearchData.length);
			for (var i = 0; i < revenueSearchData.length; i++)
			{
				var projectId = revenueSearchData[i].getValue('custrecord_project_name_tm_transaction');
				var projectName = revenueSearchData[i].getText('custrecord_project_name_tm_transaction');
				var proj_region = revenueSearchData[i].getText('custrecord_region_tm_transaction');
				var proj_prac = revenueSearchData[i].getValue('custrecord_practice_tm_transaction');
				var cust = revenueSearchData[i].getText('custrecord_customer_tm_transaction');
				var emp_id = revenueSearchData[i].getValue('custrecord_resource_tm_transaction');
				var emp_intrnal_id = revenueSearchData[i].getValue('custrecord_resource_id_tm');
				var revenue = revenueSearchData[i].getValue('custrecord_usd_revenue');
				var emp_prac = revenueSearchData[i].getValue('custrecord_emp_practice_tm_transaction');
				var isRecognised = revenueSearchData[i].getValue('custrecord_reognised_tm_transaction');
				var monthName = revenueSearchData[i].getValue('custrecord_month_tm_transaction');
				var yearName = revenueSearchData[i].getValue('custrecord_year_tm_transaction');
				var proj_currency = revenueSearchData[i].getValue('custrecord_currency_tm_transaction');
				var pro_currency_name = revenueSearchData[i].getText('custrecord_currency_tm_transaction');
				var pro_actual_strt_date = revenueSearchData[i].getValue('custrecord_pro_strt_date_tm_transaction');
				var pro_actual_end_date = revenueSearchData[i].getValue('custrecord_pro_end_date_tm_transaction');
				var allocation_strt_date = revenueSearchData[i].getValue('custrecord_allo_strt_date_tm_transaction');
				var allocation_end_date = revenueSearchData[i].getValue('custrecord_allo_end_date_tm_transaction');
				var emp_bill_rate = revenueSearchData[i].getValue('custrecord_emp_bill_rate_tm_transaction');
				var is_monthly_proj = revenueSearchData[i].getValue('custrecord_tm_monthly_transaction');
				var emp_onsite_offsite = revenueSearchData[i].getValue('custrecord_emp_onsite_tm_transaction');
				var emp_prcnt_allocation = revenueSearchData[i].getValue('custrecord_prcnt_allocated_tm');
				var emp_practice_internal_id = revenueSearchData[i].getValue('custrecord_practice_id_tm_transaction');
				var internal_id = revenueSearchData[i].getId();
				
				//nlapiLogExecution('audit','allocation_strt_date:- '+allocation_strt_date,allocation_end_date);
				if(_logValidation(revenue))
				{					
					//revenue = Math.round(revenue * (1 / 66),2);
					
					revenue = parseFloat(revenue).toFixed(2);
				}
				else
				{
					revenue = parseFloat(0);
				}
				
				var is_monthly_hourly = 'Hourly';
				if(is_monthly_proj == 'T')
				{
					is_monthly_hourly = 'Monthly';
				}
				
				monthName = monthName +"_"+ yearName;
				
					var emp_entity_id = emp_id.split('-');
					emp_entity_id = emp_entity_id[0];
					var emp_id_prj_id = emp_intrnal_id+'_'+emp_entity_id+'_'+projectId;
					if (!projectWiseRevenue[emp_id_prj_id])
					{						
						projectWiseRevenue[emp_id_prj_id] = {
							project_id: projectId,
							projectName: projectName,
							proj_region: proj_region,
							proj_prac: proj_prac,
							cust: cust,
							emp_id: emp_id,
							emp_name: emp_id,
							revenue: revenue,
							emp_prac: emp_prac,
							IsRecognised: isRecognised,
							pro_actual_strt_date: pro_actual_strt_date,
							pro_actual_end_date: pro_actual_end_date,
							allo_strt_date: allocation_strt_date,
							allo_end_date: allocation_end_date,
							emp_bill_rate: emp_bill_rate,
							is_monthly_hourly: is_monthly_hourly,
							emp_onsite_offsite: emp_onsite_offsite,
							projectCurrency: pro_currency_name,
							emp_prcnt_allocation: emp_prcnt_allocation,
							RevenueData: {}
						};
						
						projectWiseRevenue[emp_id_prj_id].RevenueData[projectId] = new yearData(2016);
						
						projectWiseRevenue[emp_id_prj_id].RevenueData[projectId][monthName].Amount = revenue;
						projectWiseRevenue[emp_id_prj_id].RevenueData[projectId][monthName].IsRecognised = isRecognised;
					
					}
					else
					{
						{
							if(allocation_strt_date)
							{
								projectWiseRevenue[emp_id_prj_id].allo_strt_date = allocation_strt_date;
								projectWiseRevenue[emp_id_prj_id].allo_end_date = allocation_end_date;
								projectWiseRevenue[emp_id_prj_id].emp_bill_rate = emp_bill_rate;
								projectWiseRevenue[emp_id_prj_id].emp_prcnt_allocation = emp_prcnt_allocation;
								projectWiseRevenue[emp_id_prj_id].emp_prac = emp_prac;
							}
							
							projectWiseRevenue[emp_id_prj_id].RevenueData[projectId][monthName].Amount = revenue;
							projectWiseRevenue[emp_id_prj_id].RevenueData[projectId][monthName].IsRecognised = isRecognised;
						}
					}
				
			}
		}
	
		var css_file_url = "";

		var context = nlapiGetContext();
		if (context.getEnvironment() == "SANDBOX") {
			css_file_url = "https://system.na1.netsuite.com/core/media/media.nl?id=199133&c=3883006&h=64fa194295cd4ab64d58&mv=iq3kjr6g&_xt=.css&whence=";
		} else if (context.getEnvironment() == "PRODUCTION") {
			css_file_url = "https://system.na1.netsuite.com/core/media/media.nl?id=199133&c=3883006&h=64fa194295cd4ab64d58&mv=iq3kjr6g&_xt=.css&whence=";
		}

		var html = "";
		html += "<link href='" + css_file_url + "'  rel='stylesheet'>";

		html += "<table class='projection-table'>";

		// header
		html += "<tr class='header-row'>";
		html += "<td>";
		html += "Customer Name";
		html += "</td>";

		html += "<td>";
		html += "Region";
		html += "</td>";
		
		html += "<td>";
		html += "Project Name";
		html += "</td>";
		
		html += "<td>";
		html += "Project Curreny";
		html += "</td>";
		
		html += "<td>";
		html += "Project Start Date";
		html += "</td>";
		
		html += "<td>";
		html += "Project End Date";
		html += "</td>";

		html += "<td>";
		html += "Executing Practice";
		html += "</td>";

		html += "<td>";
		html += "Employee Name";
		html += "</td>";

		html += "<td>";
		html += "Employee Practice";
		html += "</td>";
		
		html += "<td>";
		html += "Allocation Start Date";
		html += "</td>";
		
		html += "<td>";
		html += "Allocation End Date";
		html += "</td>";
		
		html += "<td>";
		html += "Percent Allocated";
		html += "</td>";
		
		html += "<td>";
		html += "Bill Rate";
		html += "</td>";
		
		html += "<td>";
		html += "Monthly/Hourly";
		html += "</td>";
		
		html += "<td>";
		html += "Onsite/Offsite";
		html += "</td>";

		
		var monthNames = [ "JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JUL",
	        "AUG", "SEP", "OCT", "NOV", "DEC" ];
		
			
		var today = new Date();
		var currentMonthName = getMonthName(nlapiDateToString(today));
		
		for (var yr=2016; yr<=2017; yr++)
		{
			for (var i = 0; i < monthNames.length; i++)
			{
				html += "<td>";
				html += monthNames[i]+"_"+yr;
				html += "</td>";
			}
		}

		html += "</tr>";

		for ( var emp_internal_id in projectWiseRevenue) {
			//nlapiLogExecution('audit','emp_internal_id');
			for ( var project_internal_id in projectWiseRevenue[emp_internal_id].RevenueData) {
				nlapiLogExecution('audit','inside display');
				html += "<tr>";
				html += "<td class='label-name'>";
				html += projectWiseRevenue[emp_internal_id].cust;
				html += "</td>";

				html += "<td class='label-name'>";
				html += projectWiseRevenue[emp_internal_id].proj_region;
				html += "</td>";
				
				html += "<td class='label-name'>";
				html += projectWiseRevenue[emp_internal_id].projectName;
				html += "</td>";
				
				html += "<td class='label-name'>";
				html += projectWiseRevenue[emp_internal_id].projectCurrency;
				html += "</td>";
				
				html += "<td class='label-name'>";
				html += projectWiseRevenue[emp_internal_id].pro_actual_strt_date;
				html += "</td>";
				
				html += "<td class='label-name'>";
				html += projectWiseRevenue[emp_internal_id].pro_actual_end_date;
				html += "</td>";
				
				html += "<td class='label-name'>";
				html += projectWiseRevenue[emp_internal_id].proj_prac;
				html += "</td>";

				html += "<td class='label-name'>";
				html += projectWiseRevenue[emp_internal_id].emp_id;
				html += "</td>";

				html += "<td class='label-name'>";
				html += projectWiseRevenue[emp_internal_id].emp_prac;
				html += "</td>";
				
				html += "<td class='label-name'>";
				html += projectWiseRevenue[emp_internal_id].allo_strt_date;
				html += "</td>";
				
				html += "<td class='label-name'>";
				html += projectWiseRevenue[emp_internal_id].allo_end_date;
				html += "</td>";
				
				html += "<td class='label-name'>";
				html += projectWiseRevenue[emp_internal_id].emp_prcnt_allocation;
				html += "</td>";
				
				html += "<td class='label-name'>";
				html += projectWiseRevenue[emp_internal_id].emp_bill_rate;
				html += "</td>";
				
				html += "<td class='label-name'>";
				html += projectWiseRevenue[emp_internal_id].is_monthly_hourly;
				html += "</td>";
				
				html += "<td class='label-name'>";
				html += projectWiseRevenue[emp_internal_id].emp_onsite_offsite;
				html += "</td>";

				for ( var month in projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id]) {
					// check if past month
					//nlapiLogExecution('audit','month in arr: '+projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id][month].Amount);
					var currentMonthPos = monthNames.indexOf(month);
					var currentMonthDate = nlapiStringToDate((currentMonthPos + 1)
					        + '/31/2016');

					if (currentMonthDate < new Date()) {
						// if
						// (projectWiseRevenue[project].RevenueData[practice][month].IsRecognised)
						// {
						html += "<td class='monthly-amount'>";
					} else {
						html += "<td class='projected-amount'>";
					}
					//month = 'AUG';
					html += projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id][month].Amount;
					html += "</td>";

				}
				html += "</tr>";
			}
		}

		html += "</table>";

		return html;
	} catch (err) {
		nlapiLogExecution('ERROR', 'createProjectionTable', err);
		throw err;
	}
}

function searchRecord(recordType, savedSearch, arrFilters, arrColumns,filterExpression)
{
	try {
		var search = null;

		// if a saved search is provided, load it and add the filters and
		// columns
		if (isNotEmpty(savedSearch)) {
			search = nlapiLoadSearch(recordType, savedSearch);

			if (isArrayNotEmpty(arrFilters)) {
				search.addFilters(arrFilters);
			}

			if (isArrayNotEmpty(arrColumns)) {
				search.addColumns(arrColumns);
			}

			if (isArrayNotEmpty(filterExpression)) {
				search.setFilterExpression(filterExpression);
			}
		}
		// create a new search
		else {
			search = nlapiCreateSearch(recordType, arrFilters, arrColumns);
		}

		// run search
		var resultSet = search.runSearch();

		// iterate through the search and get all data 1000 at a time
		var searchResultCount = 0;
		var resultSlice = null;
		var searchResult = [];

		do {
			resultSlice = resultSet.getResults(searchResultCount,
			        searchResultCount + 1000);

			if (resultSlice) {

				resultSlice.forEach(function(result) {

					searchResult.push(result);
					searchResultCount++;
				});
			}
		} while (isArrayNotEmpty(resultSlice) && resultSlice.length >= 1000);

		return searchResult;
	} catch (err) {
		nlapiLogExecution('ERROR', 'searchRecord', err);
		throw err;
	}
}

function searchEmp(emp_id,projectId,emp_unique_proj)
{
	for(var jk=0; jk<emp_unique_proj.length ; jk++)
	{
		var proj_ID = emp_unique_proj[jk].project_id;
		var Emp_ID = emp_unique_proj[jk].employee_id;
		
		if(parseInt(Emp_ID) == parseInt(emp_id) && parseInt(proj_ID)== parseInt(projectId))
		{
			return true;
		}
		else
		{
			return false;
		}
	}
}

function isEmpty(value) {

	return value == null || value == "" || typeof (value) == undefined;
}

function isNotEmpty(value) {

	return !isEmpty(value);
}

function isArrayEmpty(argArray) {

	return !isArrayNotEmpty(argArray);
}

function isArrayNotEmpty(argArray) {

	return (isNotEmpty(argArray) && argArray.length > 0);
}

function yearData(yr) {
	var yr1 = Number(yr) + 1;
	
	var monthNames = [ "JAN_"+yr, "FEB_"+yr, "MAR_"+yr, "APR_"+yr, "MAY_"+yr, "JUN_"+yr, "JUL_"+yr, "AUG_"+yr,
	        "SEP_"+yr, "OCT_"+yr, "NOV_"+yr, "DEC_"+yr ];
	var month_nxt_yr = ["JAN_"+yr1, "FEB_"+yr1, "MAR_"+yr1, "APR_"+yr1, "MAY_"+yr1, "JUN_"+yr1, "JUL_"+yr1, "AUG_"+yr1,
	        "SEP_"+yr1, "OCT_"+yr1, "NOV_"+yr1, "DEC_"+yr1 ];
	monthNames = monthNames.concat(month_nxt_yr);	
	for (var i = 0; i < monthNames.length; i++) {
		this[monthNames[i]] = {
		    Amount : 0,
		    IsRecognised : true
		};
	}
}

/*function yearData() {
	var monthNames = [ "JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JUL", "AUG",
	        "SEP", "OCT", "NOV", "DEC" ];

	for (var i = 0; i < monthNames.length; i++) {
		this[monthNames[i]] = {
		    Amount : 0,
		    IsRecognised : true
		};
	}
}*/

function legendsTable() {
	var htmlText = "";
	htmlText += "<table class='legend-table'>";
	htmlText += "<tr><td class=''></td><td>Legends</td></tr>";
	htmlText += "<tr><td class='recognised-circle'></td><td>Recognised</td></tr>";
	htmlText += "<tr><td class='projected-circle'></td><td>Projected</td></tr>";
	htmlText += "</table>";
	return htmlText;
}

function getMonthName(currentDate) {
	currentDate = nlapiStringToDate(currentDate)
	var monthNames = [ "JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JUL", "AUG",
	        "SEP", "OCT", "NOV", "DEC" ];
	return monthNames[currentDate.getMonth()];
}

function getDataRefreshDate() {
	try {
		var refreshDate = '';

		var dateSearch = nlapiSearchRecord(
		        'customrecord_emp_practice_rev_projection', null, null,
		        new nlobjSearchColumn('formuladate', null, 'max')
		                .setFormula('{created}'));

		if (dateSearch) {
			refreshDate = dateSearch[0].getValue('formuladate', null, 'max');
		}

		return refreshDate;
	} catch (err) {
		nlapiLogExecution('ERROR', 'getDataRefreshDate', err);
		throw err;
	}
}

function isRefreshOngoing() {
	try {
		var isRefreshOnGoing = false;

		var search = nlapiSearchRecord('scheduledscriptinstance', null,
		        [
		                new nlobjSearchFilter('internalid', 'script', 'anyof',
		                        [ 938 ]),
		                new nlobjSearchFilter('status', null, 'anyof', [
		                        'RETRY', 'PROCESSING', 'PENDING' ]) ])

		if (search) {
			isRefreshOnGoing = true;
		}

		return isRefreshOnGoing;
	} catch (err) {
		nlapiLogExecution('ERROR', 'isRefreshOngoing', err);
		throw err;
	}
}

function _logValidation(value){
	if (value != 'null' && value != null && value != null && value != '' && value != undefined && value != undefined && value != 'undefined' && value != 'undefined' && value != 'NaN' && value != NaN) {
		return true;
	}
	else {
		return false;
	}
}

function generate_excel(strVar_excel)
{
	var strVar1= '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:ss="urn:schemas-microsoft-com:office:spreadsheet" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40">'+
		'<head>'+
		'<meta http-equiv=Content-Type content="text/html; charset=windows-1252"/>'+
        '<meta name=ProgId content=Excel.Sheet/>'+
        '<meta name=Generator content="Microsoft Excel 11"/>'+
        '<!--[if gte mso 9]><xml>'+
        '<x:excelworkbook>'+
        '<x:excelworksheets>'+
        '<x:excelworksheet=sheet1>'+
        '<x:name>** ESTIMATE FILE**</x:name>'+
        '<x:worksheetoptions>'+
        '<x:selected></x:selected>'+
        '<x:freezepanes></x:freezepanes>'+
        '<x:frozennosplit></x:frozennosplit>'+
        '<x:splithorizontal>** FROZEN ROWS + 1 **</x:splithorizontal>'+
        '<x:toprowbottompane>** FROZEN ROWS + 1 **</x:toprowbottompane>'+
        '<x:splitvertical>** FROZEN COLUMNS + 1 **</x:splitvertical>'+
        '<x:leftcolumnrightpane>** FROZEN COLUMNS + 1**</x:leftcolumnrightpane>'+
		    '<x:activepane>0</x:activepane>'+// 0
		    '<x:panes>'+
			'<x:pane>'+
			'<x:number>3</x:number>'+
			'</x:pane>'+
			'<x:pane>'+
		    '<x:number>1</x:number>'+
		    '</x:pane>'+
		    '<x:pane>'+
			'<x:number>2</x:number>'+
        '</x:pane>'+
        '<x:pane>'+
        '<x:number>0</x:number>'+//1
        '</x:pane>'+
        '</x:panes>'+
        '<x:protectcontents>False</x:protectcontents>'+
        '<x:protectobjects>False</x:protectobjects>'+
        '<x:protectscenarios>False</x:protectscenarios>'+
        '</x:worksheetoptions>'+
        '</x:excelworksheet>'+
		'</x:excelworksheets>'+
        '<x:protectstructure>False</x:protectstructure>'+
        '<x:protectwindows>False</x:protectwindows>'+
		'</x:excelworkbook>'+
        
        // '<style>'+
			
			//-------------------------------------
        '</x:excelworkbook>'+
        '</xml><![endif]-->'+
         '<style>'+
		'p.MsoFooter, li.MsoFooter, div.MsoFooter'+
		'{ margin:0in; margin-bottom:.0001pt; mso-pagination:widow-orphan; tab-stops:center 3.0in right 6.0in; font-size:12.0pt;}'+
		'<style>'+

		'<!-- /* Style Definitions */'+

		//'@page Section1'+
		//'{ size:8.5in 11.0in; margin:1.9cm 1.27cm 1.9cm 1.27cm; mso-header-margin:.5in; mso-footer-margin:.5in; mso-title-page:yes; mso-header: h1; mso-footer: f1; mso-first-header: fh1; mso-first-footer: ff1; mso-paper-source:0;}'+

		'div.Section1'+
		'{ page:Section1;}'+

		'table#hrdftrtbl'+
		'{ margin:0in 0in 0in 900in; width:1px; height:1px; overflow:hidden; font-family:Arial;}-->'+

		'</style>'+
		
		
		'</head>'+
		

		'<body>'+
		
		'<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">'
		
		var strVar2 = strVar_excel;
		strVar1 = strVar1 + strVar2;
		var file = nlapiCreateFile('T&M Revenue Data.xls', 'XMLDOC', strVar1);
		return file;
}