/**
 * Resend the PDF to the vendor
 * 
 * Version Date Author Remarks
 * 
 * 1.00 14 Jul 2016 Nitish Mishra
 * 
 */

function suiteletSendWoPdf(request, response) {
	try {
		var ccEmail = [ 'indiatalentacquisition@brillio.com',
		        'swetha.b@brillio.com' ];
		var bccEmail = [ 'govind.kharayat@brillio.com' ];

		var subtierRecordId = request.getParameter('subtier');

		if (!subtierRecordId) {
			throw "Subtier Record Id Missing";
		}

		var subtierRecord = nlapiLoadRecord('customrecord_subtier_vendor_data',
		        subtierRecordId);

		var employeeDetails = nlapiLookupField('employee', subtierRecord
		        .getFieldValue('custrecord_stvd_contractor'), [ 'firstname',
		        'lastname' ]);

		var vendorDetails = nlapiLookupField('vendor', subtierRecord
		        .getFieldValue('custrecord_stvd_vendor'), [ 'altname', 'email',
		        'internalid' ]);

		var pdfFileId = subtierRecord.getFieldValue('custrecord_stvd_wo_file');

		if (!pdfFileId) {
			throw "WO Pdf Not attached to the subtier record";
		}

		var pdfFile = nlapiLoadFile(pdfFileId);

		// get all the vendor contacts to send an email
		var contactSearch = nlapiSearchRecord('contact', null, [
		        new nlobjSearchFilter('company', null, 'anyof',
		                vendorDetails.internalid),
		        new nlobjSearchFilter('isinactive', null, 'is', 'F') ],
		        [ new nlobjSearchColumn('firstname') ]);

		if (contactSearch) {

			// send an email to the vendor contact
			for (var i = 0; i < contactSearch.length; i++) {
				var woEmailContent = getWOEmailTemplate(subtierRecord
				        .getFieldValue('custrecord_stvd_wo_number'),
				        contactSearch[i].getValue('firstname'),
				        employeeDetails.firstname + " "
				                + employeeDetails.lastname);

				nlapiSendEmail(constant.Mail_Author.TalentAcquisition,
				        contactSearch[i].getId(), woEmailContent.Subject,
				        woEmailContent.Body, ccEmail, bccEmail, {
				            'record' : subtierRecordId,
				            'recordtype' : 'customrecord_subtier_vendor_data'
				        }, pdfFile);
			}

			nlapiLogExecution('DEBUG', 'Email send to contacts');
			result = 'Email send to contacts';
		} else {
			// no contacts found, send email directly to the vendor
			// record
			var woEmailContent = getWOEmailTemplate(subtierRecord
			        .getFieldValue('custrecord_stvd_wo_number'), '',
			        employeeDetails.firstname + " " + employeeDetails.lastname);

			nlapiSendEmail(constant.Mail_Author.TalentAcquisition,
			        vendorDetails.internalid, woEmailContent.Subject,
			        woEmailContent.Body, ccEmail, bccEmail, {
			            'record' : subtierRecordId,
			            'recordtype' : 'customrecord_subtier_vendor_data'
			        }, pdfFile);

			nlapiLogExecution('DEBUG', 'Email send to vendor');
			result = 'Email send to vendor';
		}
	} catch (err) {
		nlapiLogExecution('ERROR', 'suiteletSendWoPdf', err);
		result = err.message;
	}

	response.write(JSON.stringify(result));
}

function resentWoPdf() {
	try {
		nlapiRequestURL('/app/site/hosting/scriptlet.nl?script=958&deploy=1', {
			'subtier' : nlapiGetRecordId()
		}, null, handleResponse);
	} catch (err) {
		nlapiLogExecution('ERROR', 'resentWoPdf', err);
		alert(err.message);
	}
}

function handleResponse(response) {
	alert(response.getBody());
}

function getWOEmailTemplate(woNumber, vendorName, employeeName) {
	var htmltext = '<p>Hi ' + vendorName + ',</p>';

	htmltext += "<p>Please find the attached Extended Work Order for "
	        + employeeName + ".</p>";
	htmltext += "<p>Please review the same and share back with the signed copy.</p>";

	htmltext += '<p>Regards,<br/>';
	htmltext += 'Brillio Team </p>';

	return {
	    Subject : 'Work Order Extension #' + woNumber + " " + employeeName,
	    Body : addMailTemplate(htmltext)
	};
}