function myFieldChange(type, name)
{
	if(name == 'custpage_family')
	{
			nlapiGetCurrentLineItemValue('effort_sublist','family_selected');
		var listVal = nlapiGetCurrentLineItemValue('skill_sublist','custpage_family');
		var user=nlapiGetUser();
	
	nlapiRemoveLineItemOption('skill_sublist','custpage_primary_skill');
	
	var family_select=nlapiGetCurrentLineItemValue('skill_sublist','custpage_family');
	var project_list=getSkills(family_select);
	nlapiInsertLineItemOption('skill_sublist','custpage_primary_skill', '', '', false);
	for(var i=0; i< project_list.length;i++)
	{
		//nlapiSetFieldValue('custpage_field1',project_list[i].getValue('internalid'));
	nlapiInsertLineItemOption('skill_sublist','custpage_primary_skill', project_list[i].getValue('custrecord_emp_primary_skill'), project_list[i].getText('custrecord_emp_primary_skill'), false);
	}
	}
	if(name == 'custpage_primary')
	{
		var primary_selected = nlapiGetCurrentLineItemValue('skill_sublist','custpage_primary');
		if(primary_selected == 'T')
			nlapiSetCurrentLineItemValue('skill_sublist','custpage_secondary','F',false);	
	}
	if(name == 'custpage_secondary')
	{
		var secondary_selected = nlapiGetCurrentLineItemValue('skill_sublist','custpage_primary');
		if(secondary_selected == 'T')
		nlapiSetCurrentLineItemValue('skill_sublist','custpage_primary','F');	
	}
	if(name == 'custpage_primary_skill')
	{
		var line_count = nlapiGetLineItemCount('skill_sublist');
		if(line_count >= 1)
		{
			for(var line_indx = 1 ; line_indx <= line_count; line_indx++)
			{
				var skill_selected = nlapiGetLineItemValue('skill_sublist','custpage_skills_updated',line_indx);
				var current_skill = nlapiGetCurrentLineItemValue('skill_sublist','custpage_primary_skill');
				if(skill_selected && current_skill)
				{
					if(skill_selected == nlapiGetCurrentLineItemText('skill_sublist','custpage_primary_skill'))
					{
						nlapiSetCurrentLineItemValue('skill_sublist','custpage_primary_skill','',false);
						alert('Duplicate skills. Skill can be either primary or Secondary');
						return false;
					}
				}
			}
			
		}
		nlapiSetCurrentLineItemValue('skill_sublist','custpage_skills_updated',nlapiGetCurrentLineItemText('skill_sublist','custpage_primary_skill'),false);
	}
	if(name == 'custpage_prev_certificate')
	{
		if (parseFloat(nlapiGetFieldValue('custpage_prev_skill_length')) < parseFloat(nlapiGetCurrentLineItemIndex('prev_skill_sublist')))
		{
			alert('You cannot update this field in the existing row. You can add a new skill to capture any change in New Skill& Certification Details');
			nlapiSetCurrentLineItemValue('prev_skill_sublist','custpage_prev_certificate','',false);
			return false;
		}
		else
		{
			nlapiSetCurrentLineItemValue('prev_skill_sublist','custpage_updated_skill','T');
		}
	}
	if(name == 'custpage_prev_proficncy')
	{
		if (parseFloat(nlapiGetFieldValue('custpage_prev_skill_length'))< parseFloat(nlapiGetCurrentLineItemIndex('prev_skill_sublist')))
		{
			alert('You cannot update this field in the existing row. You can add a new skill to capture any change in New Skill& Certification Details');
			nlapiSetCurrentLineItemValue('prev_skill_sublist','custpage_prev_proficncy','',false);
			return false;
		}
		else
		{
			nlapiSetCurrentLineItemValue('prev_skill_sublist','custpage_updated_skill','T');
		}
	}
	

}
function onSave()
{
	var employee_name = nlapiGetFieldValue('custpage_employee');
	var department = nlapiGetFieldValue('custpage_department');
	var reporting_manager = nlapiGetFieldValue('reprtng_id');
	var emp_id= nlapiGetUser();
	//var skill_record = nlapiCreateRecord('customrecord_employee_skill_master');
	
	//var id_skill = nlapiSubmitRecord(skill_record,true,true);
	var prev_skill_length = nlapiGetLineItemCount('prev_skill_sublist');
	var existing_prev_skills =nlapiGetFieldValue('custpage_prev_skill_length');
	if(existing_prev_skills != prev_skill_length)
	{
		alert('You Cannot Add Skills In Current Skills Tab. You can add a new skill to capture any change in New Skill& Certification Details');
		return false;
	}
	for(var prev_s_indx =1 ; prev_s_indx <= prev_skill_length; prev_s_indx++)
	{
		var change_trigger = nlapiGetLineItemValue('prev_skill_sublist','custpage_updated_skill',prev_s_indx);
		if(change_trigger == 'T')
		{
			var prev_prof = nlapiGetLineItemValue('prev_skill_sublist','custpage_prev_proficncy',prev_s_indx);
			var prev_certf = nlapiGetLineItemValue('prev_skill_sublist','custpage_prev_certificate',prev_s_indx);
			var prev_rec_id = nlapiGetLineItemValue('prev_skill_sublist','custpage_prev_recrd_id',prev_s_indx);
			var prev_rec = nlapiLoadRecord('customrecord_employee_skill_details',prev_rec_id);
			prev_rec.setFieldValue('custrecord_proficieny_level',prev_prof);
			prev_rec.setFieldValue('custrecord_certificate',prev_certf);
			prev_rec.setFieldValue('custrecord_skill_approval_status', 1);
			nlapiSubmitRecord(prev_rec);
		}
	}
	var skill_length = nlapiGetLineItemCount('skill_sublist');
	for (var i_index_ = 1; i_index_ <= skill_length; i_index_++) // iterate through sublist
	{
		
		var skill_certificate_record = nlapiCreateRecord('customrecord_employee_skill_details');
		var family = nlapiGetLineItemValue('skill_sublist','custpage_family',i_index_);
		if(!family)
		{	
			alert('please select family');
			return false;
		}
		var skill_selected  = nlapiGetLineItemValue('skill_sublist','custpage_primary_skill',i_index_);
		var skill_selected  = nlapiGetLineItemValue('skill_sublist','custpage_skills_updated',i_index_);
		if(!skill_selected)
		{	
			alert('please select Skills');
			return false;
		}
		var primary_skill   =  nlapiGetLineItemValue('skill_sublist','custpage_primary',i_index_);
		var secondary_skill = nlapiGetLineItemValue('skill_sublist','custpage_secondary',i_index_);
		if((primary_skill == 'F') && (secondary_skill == 'F'))
		{
			alert('Skill should be either Primary/ Secondary');
			return false;
		}
		if(!secondary_skill)
			secondary_skill = 'F';
		var proficiency =  nlapiGetLineItemValue('skill_sublist','custpage_proficncy',i_index_);
		if(!proficiency)
		{	
			alert('please select proficiency');
			return false;
		}
		skill_certificate_record.setFieldValue('custrecord_skill_update_employee',employee_name);
		skill_certificate_record.setFieldValue('custrecord_skill_emp_dept',department);
		var certificate_parent = nlapiGetLineItemValue('skill_sublist', 'custpage_certificate', i_index_);
		//var c_startdate = nlapiGetLineItemValue('skill_sublist', 'custpage_cstart_date', i_index_);
		//var c_enddate = nlapiGetLineItemValue('skill_sublist', 'custpage_cend_date', i_index_);
		skill_certificate_record.setFieldValue('custrecord_family_list_master', family);
		skill_certificate_record.setFieldValue('custrecord_skill_pri_list', skill_selected);
		skill_certificate_record.setFieldValue('custrecord_primary_check', primary_skill);
		skill_certificate_record.setFieldValue('custrecord_second_check', secondary_skill);
		skill_certificate_record.setFieldValue('custrecord_skill_approval_status', 1);
		skill_certificate_record.setFieldValue('custrecord_proficieny_level', proficiency);
		skill_certificate_record.setFieldValue('custrecord_skill_approver',reporting_manager );
		skill_certificate_record.setFieldValue('custrecord_certificate', (certificate_parent));
		//skill_certificate_record.setFieldValue('custrecord_certificate_start_date', c_startdate);
		//skill_certificate_record.setFieldValue('custrecord_certificate_end_date', c_enddate);
		//skill_certificate_record.setFieldValue('custrecord_skillparent_', id_skill);
		var id_rev_share = nlapiSubmitRecord(skill_certificate_record);
					
	}
		// nlapiSubmitField('employee',emp_id, 'custentity_skill_updated', 'T');
		// mail to BO team and participating practice head
		/*var strVar = '';
		strVar += '<html>';
		strVar += '<body>';
			
		strVar += '<p>Dear RM,</p>';
		strVar += '<p>'+employee_name+' who has recently joined your team has updated the skill field on the tool. Request you to kindly validate the same via accessing below mentioned link.';
		strVar += 'This activity will help in achieving following objectives:</p>';
			
		strVar += '<p>•	Determining Learning Plans for Brillians basis validated skills<br>';
		strVar += '<p>•	Redeployment of Brillian across Projects</p>';
				
		strVar += '<a href=https://system.na1.netsuite.com/app/site/hosting/scriptlet.nl?script=1672&deploy=1&empid=>Link to Approve Skill Record in Netsuite</a>';
			
		strVar += '<p>Regards,</p>';
		strVar += '<p>Fulfilment Team</p>';
				
		strVar += '</body>';
		strVar += '</html>';
			
			var a_emp_attachment = new Array();
			a_emp_attachment['entity'] = '7905';
			
			nlapiSendEmail(442, approver_mail, 'Employee Skill Updated: '+employee_name+'.', strVar,null,null,a_emp_attachment);
			*/
	

	return true;
}
 function getSkills(family_select) {
	try {
			var skill_filter = 
		        [
		            [ 'custrecord_employee_skil_family', 'is', family_select ],               
		              'and',
		        [ 'isinactive', 'is', 'F' ] ];

		var skill_search_results = searchRecord('customrecord_employee_skills', null, skill_filter,
		        [ new nlobjSearchColumn("custrecord_emp_primary_skill"),
		               		new nlobjSearchColumn("internalid")]);

		nlapiLogExecution('debug', 'Results count',
		        skill_search_results.length);

		if (skill_search_results.length == 0) {
			throw "You don't have any Primary Skills under Family Selected.";
		}
	
		return skill_search_results;
	} catch (err) {
		nlapiLogExecution('ERROR', 'getTaggedProjectList', err);
		throw err;
	}
		
	
}
function validate_line(type)
{
	if(type == 'skill_sublist')
	{
		var family_Selected = nlapiGetCurrentLineItemValue('skill_sublist','custpage_family');
		var skill_selected = nlapiGetCurrentLineItemValue('skill_sublist','custpage_primary_skill');
		var skill_selected_text  = nlapiGetCurrentLineItemValue('skill_sublist','custpage_skills_updated');
		var primary_selected = nlapiGetCurrentLineItemValue('skill_sublist','custpage_primary');
		var sec_selected = nlapiGetCurrentLineItemValue('skill_sublist','custpage_secondary');
		var prof_selected = nlapiGetCurrentLineItemValue('skill_sublist','custpage_proficncy');
		if(!family_Selected)
		{
			alert('Please Select Family');
			return false;
		}
		if(!skill_selected_text)
		{
			if(!skill_selected)
			{
				alert('Please Select Skill from Dropdown');
				return false;
			}
		}
		if((primary_selected == 'F') && (sec_selected == 'F'))
		{
			alert('Skill should be either Primary/ Secondary');
			return false;
		}
		if(!prof_selected)
		{
			alert('Please Select Proficiency of skill');
			return false;
		}
		
		var line_count = nlapiGetLineItemCount('skill_sublist');
		if(line_count >= 1)
		{
			for(var line_indx = 1 ; line_indx <= line_count; line_indx++)
			{
				var skill_selected = nlapiGetLineItemValue('skill_sublist','custpage_skills_updated',line_indx);
				var current_skill = nlapiGetCurrentLineItemValue('skill_sublist','custpage_skills_updated');
				var current_skill_list = nlapiGetCurrentLineItemValue('skill_sublist','custpage_primary_skill');
				if(skill_selected && current_skill && current_skill_list)
				{
					if(skill_selected == nlapiGetCurrentLineItemValue('skill_sublist','custpage_skills_updated'))
					{
						nlapiSetCurrentLineItemValue('skill_sublist','custpage_skills_updated','');
						nlapiSetCurrentLineItemValue('skill_sublist','custpage_primary_skill','');
						alert('Duplicate skills. Skill can be either primary or Secondary');
						return false;
					}
				}
			}
		}
		nlapiSetCurrentLineItemValue('skill_sublist','custpage_primary_skill','',false);
		
	}
	if(type == 'prev_skill_sublist')
	{
		if (parseFloat(nlapiGetFieldValue('custpage_prev_skill_length')) < parseFloat(nlapiGetCurrentLineItemIndex('prev_skill_sublist')))
		{
			alert('You cannot add a new row. You can add a new skill to capture any change in New Skill& Certification Details');
			nlapiSetCurrentLineItemValue('prev_skill_sublist','custpage_prev_certificate','',false);
			nlapiSetCurrentLineItemValue('prev_skill_sublist','custpage_prev_proficncy','',false);
			return false;
		}
				
	}
	if(type == 'certificate_sublist')
	{
	var c_strt_date = nlapiGetCurrentLineItemValue('certificate_sublist', 'custpage_cstart_date');
	c_strt_date = nlapiStringToDate(c_strt_date);
	var c_end_date = nlapiGetCurrentLineItemValue('certificate_sublist', 'custpage_cend_date');
	c_end_date = nlapiStringToDate(c_end_date);
	var certificate = nlapiGetCurrentLineItemValue('certificate_sublist','custpage_certificate');
	
	if(c_strt_date > c_end_date)
	{
		alert('Certification end date cannot be less than Certification start date');
		return false;
	}
	if(!certificate)
	{
		alert('Please select certifaicate');
		return false;
	}
	if(!c_strt_date)
	{
		alert('Please select certifaicate');
		return false;
	}
	if(!c_end_date)
	{
		alert('Please select certifaicate');
		return false;
	}
	
	}
	return true;
}
function approveInvoices(url)
{

	//
		  var linecount=nlapiGetLineItemCount('custpage_skill_list');
		var resList=[];
		for(var i=1;i<=linecount;i++)
		{
			var linevalue=nlapiGetLineItemValue('custpage_skill_list','select',i);
			var invoice=nlapiGetLineItemValue('custpage_skill_list','skillid',i);
			
			if(linevalue== 'T')
			{
				var recid = nlapiGetLineItemValue('custpage_skill_list','internalid', i);
				var rec=nlapiSubmitField('customrecord_employee_skill_details',recid,['custrecord_skill_approval_status'],['2']);
		
				nlapiLogExecution('debug', 'invoice', recid);
			
			resList.push({
					    invoice : invoice
					   
					});
			}
		}
  //
	//var response =nlapiRequestURL(nlapiResolveURL('SUITELET', 'customscript_skill_approval', 'customdeploy1'));
 window.open(url,'_self');
}
function rejectInvoices(url)
{

	//
		  var linecount=nlapiGetLineItemCount('custpage_skill_list');
		var resList=[];
		for(var i=1;i<=linecount;i++)
		{
			var linevalue=nlapiGetLineItemValue('custpage_skill_list','select',i);
			var invoice=nlapiGetLineItemValue('custpage_skill_list','skillid',i);
			
			if(linevalue== 'T')
			{
				var recid = nlapiGetLineItemValue('custpage_skill_list','internalid', i);
				var rec=nlapiSubmitField('customrecord_employee_skill_details',recid,['custrecord_skill_approval_status'],['3']);
		
				nlapiLogExecution('debug', 'invoice', recid);
			
			resList.push({
					    invoice : invoice
					   
					});
			}
		}
  //
window.open(url,'_self');
}
function validate_insert(type)
{
	if(type == 'prev_skill_sublist')
	{
		alert('Inserting lines is not allowed');
		return false;
	}
	return true;
}
function validate_delete(type)
{
	if(type == 'prev_skill_sublist')
	{
		alert('Deleting lines is not allowed');
		return false;
	}
	return true;
}