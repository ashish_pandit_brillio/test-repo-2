/**
 * Module Description
 * Scheduled script to handle employee termination
 * 
 * Version    Date                              Author                      Remarks
 * 1.00         11th August 2015       Anuradha Sinha       This script runs at 9 a.m. and 9 p.m.      
 *
 */

//Search for the fusion Id from the XML file and match it with the existing employee records in NetSuite

function Main(){
try{ 
nlapiLogExecution("debug",'started');

//var folder=32413;

var filters = new Array();
filters[0] = new nlobjSearchFilter('internalid', null, 'anyof', '32413' );

// get two columns so we can build a drop down list: file name and file internal ID

var columns = new Array();
columns[0] = new nlobjSearchColumn('name', 'file');
columns[1]  = new nlobjSearchColumn('internalid', 'file');
columns[2]  = new nlobjSearchColumn('modified', 'file').setSort(true);
var file_load;
var searchResult = nlapiSearchRecord('folder',null,filters,columns);
if(searchResult) {
        
                 file_load = searchResult[0].getValue('internalid','file');

                  }
nlapiLogExecution('debug', 'searchResult.length',searchResult.length);
var xmlFile = nlapiLoadFile(file_load);

//var xmlData = nlapiStringToXML(nlapiGetFieldValue('custrecord_fi_fusion_xml_data'));

var xmlDocument = xmlFile.getValue();


var xmlDoc=nlapiStringToXML(xmlDocument);
var xmlData=nlapiSelectNodes(xmlDoc, '//Assignment');
if(xmlData!=0 && xmlData.length>0)
{
for(var i = 0 ; i < xmlData.length;i++) 
{
var xmlValue=nlapiCreateRecord('customrecord_fi_employee_xml_master',{recordmode: 'dynamic'});
xmlValue.setFieldValue('custrecord_fi_type',3);

var regex = /<Assignment>/gi, result, firstIndices = [];
while ( (result = regex.exec(xmlDocument)) ) {
    firstIndices.push(result.index);
}
var regexs = /<\/Assignment>/gi, results, lastIndices = [];
while ( (results = regexs.exec(xmlDocument)) ) {
    lastIndices.push(results.index);
}
var res=xmlDocument.substring(firstIndices[i],lastIndices[i]);

xmlValue.setFieldValue('custrecord_fi_fusion_xml_data',res+'</Assignment>');
var id = nlapiSubmitRecord(xmlValue, false, true);
nlapiLogExecution('debug','XML Termination data','Termination Record Created');
}
}
}
catch(err){
nlapiLogExecution('error', 'main', err);
}
}