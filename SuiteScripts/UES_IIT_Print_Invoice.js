/*
   	Script Name: UES_IIT_Print_Invoice.js
	Author: Onkar Gogi
	Company: Inspirria
	Date: 12/04/2017
    Description:  


	Script Modification Log:

	-- Date --			-- Modified By --			--Requested By--				-- Description --


 * 
 */

function userEventBeforeLoad_printButton(type, form, request){
	if(type == 'view'){
		nlapiLogExecution('DEBUG','userEventBeforeLoad_printButton', 'In UES--------');
      	
		var recId = nlapiGetRecordId();
		var recordType = nlapiGetRecordType();
		nlapiLogExecution('DEBUG','userEventBeforeLoad_printButton', 'In UES--------'+recId +recordType);
		
		var i_Flagcheck = 0;
		var Flag = 0;
		var a_subisidiary = new Array();
		var s_pass_code;
		var i_Flag_Status = 0;
		
		var i_AitGlobalRecId = SearchGlobalParameter();
		nlapiLogExecution('DEBUG', 'Bill ', "i_AitGlobalRecId" + i_AitGlobalRecId);
		
		if (i_AitGlobalRecId != 0) 
		{
			var o_AitGloRec = nlapiLoadRecord('customrecord_indian_tax_glo_parameter', i_AitGlobalRecId);
			
			a_subisidiary = o_AitGloRec.getFieldValues('custrecord_ait_subsidiary');
			nlapiLogExecution('DEBUG', 'Bill ', "a_subisidiary->" + a_subisidiary);
			
			var i_vatCode = o_AitGloRec.getFieldValue('custrecord_ait_vat_code');
			nlapiLogExecution('DEBUG', 'Bill ', "i_vatCode->" + i_vatCode);
			
			var i_taxcode = o_AitGloRec.getFieldValue('custrecord_ait_taxcode');
			nlapiLogExecution('DEBUG', 'Bill ', "i_taxcode->" + i_taxcode);
			
			var s_pass_code = o_AitGloRec.getFieldValue('custrecord_encpwdstring');
			nlapiLogExecution('DEBUG', 'Bill ', "s_pass_code->" + s_pass_code);
			
			
		}// end if(i_AitGlobalRecId != 0 )
		
		var i_subsidiary = nlapiGetFieldValue('subsidiary');
		
		Flag = isindia_subsidiary(a_subisidiary, s_pass_code, i_subsidiary);
		
		if(Flag == 1)
		{
			var DebitNote = nlapiGetFieldValue('custbody_iit_transactiontype');
			nlapiLogExecution('DEBUG','userEventBeforeLoad_printButton', 'DebitNote = '+DebitNote);
			
			//var url = nlapiResolveURL('SUITELET', 'customscript_sut_iit_print_invoice', 'customdeploy_sut_iit_print_invoice');
			var url = nlapiResolveURL('SUITELET', 'customscript_sut_iit_print_invoice_new', 'customdeploy_sut_iit_print_invoice_new');
			var url = url + '&recId=' +recId + '&recordType=' +recordType;
			
			var path ="win = window.open('"+ url +"','_blank')";
			
			if(recordType == 'creditmemo')
			{
				form.addButton('custpage_print_invoice','Print Credit Note', path);
			}
			else if(DebitNote == 3)
			{
				form.addButton('custpage_print_invoice','Debit Note', path);
			}
			else if(recordType == 'invoice')
			{
				form.addButton('custpage_print_invoice','GST Invoice', path);
			}
			
			
			
			
			// Code to show button on Invoice for Bill of supply
			if(recordType != 'creditmemo')
			{	
				var url1 = nlapiResolveURL('SUITELET', 'customscript_sut_iit_print_billofsupply', 'customdeploy_sut_iit_print_billofsupply');
				var url2 = url1 + '&recId=' +recId + '&recordType=' +recordType;
				
				var path1 ="win = window.open('"+ url2 +"','_blank')";
				
				form.addButton('custpage_print_billofsupply','Bill Of Supply', path1);
			}
		}	
		
		
		
	}

}

function SearchGlobalParameter()	
{
    var a_filters = new Array();
    var a_column = new Array();
    var i_globalRecId = 0;
    
    a_column.push(new nlobjSearchColumn('internalid'));
    
    var s_serchResult = nlapiSearchRecord('customrecord_indian_tax_glo_parameter', null, null, a_column);
    
    if (s_serchResult != null && s_serchResult != undefined && s_serchResult != '') 
	{
        for (var i = 0; i < s_serchResult.length; i++) 
		{
            i_globalRecId = s_serchResult[0].getValue('internalid');
            nlapiLogExecution('DEBUG', 'Bill ', "i_globalRecId" + i_globalRecId);
            
        }
        
    }
    
    
    return i_globalRecId;
}