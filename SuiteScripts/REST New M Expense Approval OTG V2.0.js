/**
 * RESTlet to get expenses for approval
 * 
 * Version    Date            Author           Remarks
 * 1.00       23 Nov 2015     nitish.mishra
 *
 */

/**
 * @param {Object}
 *            dataIn Parameter object
 * @returns {Object} Output object
 */
function postRESTlet(dataIn) {
	var response = new Response();
	try {
		nlapiLogExecution('debug', 'dataIn', JSON.stringify(dataIn));
		var employeeId = getUserUsingEmailId(dataIn.EmailId);
		var requestType = dataIn.RequestType;
		var expenseId = dataIn.Data.ExpenseId;
		var remarks = dataIn.Data.Remarks;
		nlapiLogExecution('debug', 'remarks', remarks);
		//Test
		//var employeeId = getUserUsingEmailId('rahul.murali@brillio.com');
		//var requestType = 'GET';
		//var expenseId = '390801';

		switch (requestType) {

			case M_Constants.Request.Get:

				if (expenseId) {
					response.Data = getExpenseDetail(expenseId, employeeId);
					response.Status = true;
				} else {
					response.Data = getExpensesPendingApproval(employeeId);
					response.Status = true;
				}
			break;

			case M_Constants.Request.Approve:
				response.Data = approveExpenses(dataIn.Data.ExpenseList,
				        employeeId);
				response.Status = true;
			break;

			case M_Constants.Request.Reject:
				response.Data = rejectExpenses(dataIn.Data.ExpenseList,
				        employeeId,remarks);
				response.Status = true;
			break;
		}

	} catch (err) {
		nlapiLogExecution('ERROR', 'postRESTlet', err);
		response.Data = err;
	}
	nlapiLogExecution('debug', 'dataIn', JSON.stringify(response));
	return response;
}

function getExpenseDetail(expenseId, firstLevelApprover) {
	try {
	var createdDate_ = '';
	
	
	var filters = [];
		filters.push(new nlobjSearchFilter('internalid',null,'anyof',expenseId));
		
		var cols = [];
		cols.push(new nlobjSearchColumn('date','systemnotes'));
		
		//var expensCreatedSearch = nlapiSearchRecord('transaction','customsearch1759',filters); //Sandbox
		var expensCreatedSearch = nlapiSearchRecord('transaction','customsearch1880',filters); //Prod
		 
		if(_logValidation(expensCreatedSearch)){
			nlapiLogExecution('debug','expensCreatedSearch',expensCreatedSearch.length);
		var result1=expensCreatedSearch[0];
		var column1=result1.getAllColumns(); 
			 createdDate_ = new Date(result1.getValue(column1[0]));	
		     nlapiLogExecution('debug','Created Date',createdDate_);
		}
		
		
		var expenseReportRec = nlapiLoadRecord('expensereport', expenseId);
		
		var emp_name = nlapiLookupField('employee',expenseReportRec.getFieldValue('entity'),'email');

		// check access
		if (expenseReportRec.getFieldValue('custbody1stlevelapprover') == firstLevelApprover) {
			var expenseDetails = {};

			expenseDetails.Employee = expenseReportRec.getFieldText('entity');
			expenseDetails.empName = _logValidation(emp_name) == false ? ' ' : emp_name.split('@')[0],
			expenseDetails.FirstLevelApprover = expenseReportRec
			        .getFieldText('custbody1stlevelapprover');
			expenseDetails.SecondLevelApprover = expenseReportRec
			        .getFieldText('custbody_expenseapprover');
			if(createdDate_)
				expenseDetails.ExpenseSubmittedDate = createdDate_; 
				else
				expenseDetails.ExpenseSubmittedDate = new Date(expenseReportRec.getFieldValue('createddate'));

			expenseDetails.Expense = {
			    Reference : expenseReportRec.getFieldValue('tranid'),
			    Purpose : expenseReportRec.getFieldValue('memo') ? ''
			            : expenseReportRec.getFieldValue('memo'),
			    Status : expenseReportRec.getFieldValue('status'),
			    InternalId : expenseReportRec.getId(),
			    AdvanceToApply : expenseReportRec.getFieldValue('advance'),
			    VAT : expenseReportRec.getFieldValue('tax1amt'),
			    PostingDate : expenseReportRec.getFieldValue('trandate'),
			    TransactionDate : expenseReportRec
			            .getFieldValue('custbody_transactiondate'),
			    AccountName : expenseReportRec.getFieldText('account'),
			    Account : expenseReportRec.getFieldValue('account'),
			    // Reason : '',
			    Currency : expenseReportRec.getFieldText('basecurrency'),
			    DocumentReceived : expenseReportRec
			            .getFieldValue('custbody_exp_doc_received'),
			    DocumentReceivedDate : '',
			    ItemList : []
			// ApproverRemark : expenseReportRec.getFieldValue(''),
			};
			var amount_f = expenseReportRec.getFieldValue('total');
			amount_f = parseFloat(amount_f).toFixed(1);
			expenseDetails.Expense.Summary = {
			    Expenses : amount_f,
			    NonReimbursableExpenses : expenseReportRec
			            .getFieldValue('nonreimbursable'),
			    ReimbursableExpenses : expenseReportRec
			            .getFieldValue('reimbursable'),
			    AdvanceToApply : expenseReportRec.getFieldValue('advance'),
			    TotalReimbursableAmount : expenseReportRec
			            .getFieldValue('amount')
			};

			// get all line items
			var lineItemCount = expenseReportRec.getLineItemCount('expense');
			var expenseList = [];
			var isReimbursable = null;
			var projectId = null;
			var projectIdList = [];
			var memo = '';
			var billable = '';

			for (var line = 1; line <= lineItemCount; line++) {
				isReimbursable = expenseReportRec.getLineItemValue('expense',
				        'isnonreimbursable', line);
				projectId = expenseReportRec.getLineItemValue('expense',
				        'customer', line);
				projectIdList.push(projectId);
				var amount_line_f = expenseReportRec.getLineItemValue('expense',
			            'foreignamount', line);
				amount_line_f = parseFloat(amount_line_f).toFixed(1);
				billable = expenseReportRec.getLineItemValue(
				            'expense', 'isbillable', line),	
							
				billable = (billable == 'T') ? true : false;			
				// if (isFalse(isReimbursable)) {

				if (!expenseDetails.ProjectText) {
					expenseDetails.ProjectText = expenseReportRec
					        .getLineItemValue('expense', 'custcolprj_name',
					                line);
				}
				expenseList.push({
				    ProjectText : expenseReportRec.getLineItemValue('expense',
				            'custcolprj_name', line),
				    ProjectId : projectId,
				    Vertical : expenseReportRec.getLineItemValue('expense',
				            'class_display', line),
				    CategoryText : expenseReportRec.getLineItemValue('expense',
				            'category_display', line),
				    Amount : amount_line_f,
				    ForeignAmount : expenseReportRec.getLineItemValue(
				            'expense', 'amount', line),
				    Currency : expenseReportRec.getLineItemText('expense',
				            'currency', line),
					Memo : 	expenseReportRec.getLineItemValue(
				            'expense', 'memo', line),	
					Billable : billable,
				    IsReimbursable : isReimbursable,
				    DeliveryManager : null
				});
				// }
			}

			expenseDetails.Expense.ItemList = expenseList;
			return expenseDetails;
		} else {
			throw "You are not authorised to access this record";
		}
	} catch (err) {
		nlapiLogExecution('error', 'getExpenseDetail', err);
		throw err;
	}
}

function getExpensesPendingApproval(firstLevelApprover) {
	try {
		var filters = [
		        new nlobjSearchFilter('custbody1stlevelapprover', null,
		                'anyof', firstLevelApprover),
		        new nlobjSearchFilter('status', null, 'anyof', [ 'ExpRept:B' ]),
		        new nlobjSearchFilter('mainline', null, 'is', 'T') ];

		var columns = [ new nlobjSearchColumn('transactionnumber'),
		        new nlobjSearchColumn('memo'),
		        new nlobjSearchColumn('trandate').setSort(true),
		        new nlobjSearchColumn('fxamount'),
		        new nlobjSearchColumn('currency'),
		        new nlobjSearchColumn('subsidiary'),
				new nlobjSearchColumn('internalid'),
		        new nlobjSearchColumn('entity') ];

		var pendingExpenseSearch = nlapiSearchRecord('expensereport', null,
		        filters, columns);

		var expenseList = [];
		var time_Stamp = timestamp ();

		if (pendingExpenseSearch) {
			for (var i = 0; i < pendingExpenseSearch.length && i < 50; i++) {
				//Fetching project Info
				var expense_Id = pendingExpenseSearch[i].getValue('internalid');
				var loadObj = nlapiLoadRecord('expensereport',parseInt(expense_Id));
				var lineCount = loadObj.getLineItemCount('expense');
				for(var jk=1;jk<= 1;jk++){
	
				var project =  loadObj.getLineItemValue('expense','customer_display',jk);
				
				}
				var amount_f = pendingExpenseSearch[i].getValue('fxamount');
				amount_f = parseFloat(amount_f).toFixed(1);
				var expense = new ExpensesListItem();
				expense.InternalId = pendingExpenseSearch[i].getValue('internalid');
				
				expense.ExpenseName = pendingExpenseSearch[i]
				        .getValue('transactionnumber');
				expense.EmployeeName = pendingExpenseSearch[i]
				        .getText('entity');
				expense.TotalAmount = amount_f;
				expense.Currency = pendingExpenseSearch[i].getText('currency');
				expense.Subsidiary = pendingExpenseSearch[i]
				        .getText('subsidiary');
				expense.TransactionDate = pendingExpenseSearch[i]
				        .getValue('trandate');
				expense.Memo = pendingExpenseSearch[i].getValue('memo');
				if(project){
				expense.Project = project;
				}
				expense.CurrentDate = nlapiDateToString(new Date());
				expense.timeStamp = time_Stamp;
				expenseList.push(expense);
			}
		}
		nlapiLogExecution('debug', 'dataIn', JSON.stringify(expenseList));
		return expenseList;
		
	} catch (err) {
		nlapiLogExecution('error', 'getExpensesPendingApproval', err);
		throw err;
	}
}

function approveExpenses(expenseList, firstApproverId) {
	try {

		if (expenseList) {
			var noOfApprovedExpenses = 0;

			for (var i = 0; i < expenseList.length; i++) {
				var expenseRecord = nlapiLoadRecord('expensereport',
				        expenseList[i], {
					        recordmode : 'dynamic'
				        });
				
				// check access
				if (expenseRecord.getFieldValue('custbody1stlevelapprover') == firstApproverId) {

					// check the status of the expense
					if (expenseRecord.getFieldValue('supervisorapproval') == 'F') {
						expenseRecord.setFieldValue('supervisorapproval', 'T');
						try {
							var expenseId = nlapiSubmitRecord(expenseRecord);
							createLog('expensereport', expenseId, 'Update',
							        firstApproverId, 'Approved');
							noOfApprovedExpenses += 1;
						} catch (e) {
							nlapiLogExecution('error',
							        'failed to approve expense : '
							                + expenseList[i], e);

							if (expenseList.length == 1) {
								throw e;
							}
						}
					} else if (expenseList.length == 1) {
						throw "You cannot approve this expense.";
					}
				} else if (expenseList.length == 1) {
					throw "You are not authorized to access this record.";
				}
			}

			// check no. of expenses approved
			if (noOfApprovedExpenses == expenseList.length) {
				return "Expenses Approved Succesfully";
			} else {
				return "Done. Some approvals failed.";
			}
		} else {
			throw "No Expenses to approve.";
		}
	} catch (err) {
		nlapiLogExecution('ERROR', 'approveExpenses', err);
		throw err;
	}
}

function rejectExpenses(expenseList, firstApproverId,remarks_) {
	try {

		if (expenseList) {
			var noOfApprovedExpenses = 0;

			for (var i = 0; i < expenseList.length; i++) {
				/*var expenseRecord = nlapiLoadRecord('expensereport',
				        expenseList[i], {
					        recordmode : 'dynamic'
				        });*/
				var expenseRecord = nlapiLoadRecord('expensereport',
				        expenseList[i]);

				// check access
				if (expenseRecord.getFieldValue('custbody1stlevelapprover') == firstApproverId) {

					// mark the expense as in progress
					expenseRecord.setFieldValue('complete', 'F');
					expenseRecord.setFieldValue('custbody_financemgrremarks', remarks_);
					try {
						var expenseId = nlapiSubmitRecord(expenseRecord);
						createLog('expensereport', expenseId, 'Update',
						        firstApproverId, 'Rejected');
						noOfApprovedExpenses += 1;
					} catch (e) {
						nlapiLogExecution('error',
						        'failed to reject expense : ' + expenseList[i],
						        e);

						if (expenseList.length == 1) {
							throw e;
						}
					}
				} else if (expenseList.length == 1) {
					throw "You are not authorized to access this record.";
				}
			}

			// check no. of expenses approved
			if (noOfApprovedExpenses == expenseList.length) {
				return "Expenses Rejected Succesfully";
			} else {
				return "Done. Some rejections failed.";
			}
		} else {
			throw "No Expenses to reject.";
		}
	} catch (err) {
		nlapiLogExecution('ERROR', 'rejectExpenses', err);
		throw err;
	}
}

function timestamp() {
var str = "";

var currentTime = new Date();
var hours = currentTime.getHours();
var minutes = currentTime.getMinutes();
var seconds = currentTime.getSeconds();
var meridian = "";
if (hours > 12) {
    meridian += "pm";
} else {
    meridian += "am";
}
if (hours > 12) {

    hours = hours - 12;
}
if (minutes < 10) {
    minutes = "0" + minutes;
}
if (seconds < 10) {
    seconds = "0" + seconds;
}
str += hours + ":" + minutes + ":" + seconds + " ";

return str + meridian;
}

function _logValidation(value) 
{
 if(value != null && value.toString() != null && value != '' && value != undefined && value.toString() != undefined && value != 'undefined' && value.toString() != 'undefined'&& value.toString() != 'NaN' && value != NaN) 
 {
  return true;
 }
 else 
 { 
  return false;
 }
}