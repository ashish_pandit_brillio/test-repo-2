/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       27 Feb 2015     nitish.mishra
 *
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your
 * script deployment.
 * 
 * @appliedtorecord recordType
 * 
 * @returns {Boolean} True to continue save, false to abort save
 */
function clientSaveRecord() {
	
	//-----------check for the Work from Office--------------
	
	var workFromOffice = nlapiGetFieldValue('custevent_work_location_resource');
	
	if(workFromOffice == '2' || workFromOffice == 2){
		var shiftInfo = nlapiGetFieldValue('custevent_resource_shift_info');
		if(isEmpty(shiftInfo)){
			alert('Please select the shift info');
			return false;
		}
		
		var odcCheck = nlapiGetFieldValue('custevent_resource_working_from_odc');
        nlapiLogExecution('debug', 'odcCheck', odcCheck);
		if(odcCheck == 'F'){
		alert('ODC is mandatory for working from Office');
	    return false;	
		}
          
          
        
		
		
		
	}
	
	///-------------------- ends ------------------------
	
	
	

	// check if the selected project is active
	if (!checkProjectIsActive()) {
		return false;
	}
	

	if (isTrue(nlapiGetFieldValue('custeventrbillable'))) {
		var project_id = nlapiGetFieldValue('project');

		if (isNotEmpty(project_id)) {
			var project_details = nlapiLookupField('job', project_id, [
					'jobbillingtype', 'custentity_t_and_m_monthly' ]);

			var project_type = project_details.jobbillingtype;
			var is_t_and_m_monthly = isTrue(project_details.custentity_t_and_m_monthly);

			if (project_type == 'TM' && !is_t_and_m_monthly) {
				var billRate = nlapiGetFieldValue('custevent3');

				if (isEmpty(billRate)) {
					alert('Bill Rate cannot be empty');
					return false;
				} else {
					billRate = parseInt(billRate);

					if (billRate < 1) {
						alert('Bill Rate cannot be zero');
						return false;
					}
				}
			} else if (project_type == 'TM' && is_t_and_m_monthly) {

				var billRate = nlapiGetFieldValue('custevent_monthly_rate');

				if (isEmpty(billRate)) {
					alert('Monthly Rate cannot be empty');
					return false;
				} else {
					billRate = parseInt(billRate);

					if (billRate < 1) {
						alert('Monthly Rate cannot be zero');
						return false;
					}
				}
			}
		} else {
			alert('No Project Selected');
			return false;
		}
	}

	return true;
}

function clientFieldValidate(type, name) {

	if (name == 'project') {
		return checkProjectIsActive();
	}

	return true;
}

function checkProjectIsActive() {

	var project = nlapiGetFieldValue('project');
	nlapiLogExecution('debug', 'project', project);

	if (project) {
		var projectStatus = nlapiLookupField('job', project, 'entitystatus');
		nlapiLogExecution('debug', 'projectStatus', projectStatus);

		// allow Active (2) and Pending(4) project
		if (projectStatus == 2 || projectStatus == 4) {
			return true;
		} else {
			alert('Selected Project is not active');
			return false;
		}
	}
}