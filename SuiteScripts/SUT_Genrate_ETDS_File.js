// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
/*
   	Script Name:SUT Generate ETDS Return 
	Author:Nikhil jain
	Company:Aashna cloudtech Pvt Ltd.
	Date:8-01-2015
	Description:Generate an Etds quarterly return form and generte and Etds text file


	Script Modification Log:

	-- Date --			-- Modified By --				--Requested By--				-- Description --



Below is a summary of the process controls enforced by this script file.  The control logic is described
more fully, below, in the appropriate function headers and code blocks.


     SUITELET
		- suiteletFunction(request, response)


     SUB-FUNCTIONS
		- The following sub-functions are called by the above core functions in order to maintain code
            modularization:

               - NOT USED

*/
}
// END SCRIPT DESCRIPTION BLOCK  ====================================



// BEGIN GLOBAL VARIABLE BLOCK  =====================================
{
	//  Initialize any Global Variables, in particular, debugging variables...




}
// END GLOBAL VARIABLE BLOCK  =======================================





// BEGIN SUITELET ==================================================

function suiteletFunction_generate_etds(request, response)
{

	/*  Suitelet:
	 - EXPLAIN THE PURPOSE OF THIS FUNCTION
	 -
	 FIELDS USED:
	 --Field Name--				--ID--
	 */
	//  LOCAL VARIABLES


	//  SUITELET CODE BODY
	if (request.getMethod() == 'GET')
		{		
        var form = nlapiCreateForm("Generate Quarterly eTDS Return");
		
		form.setScript('customscript_cli_genrate_etds');
		
		var year = form.addField('custpage_year', 'select', 'Financial Year');
        year.addSelectOption('', '');
        popluatyear(request, response, year);
		
		//-----------------------------------------------Begin Populate the Default person responsible Detail-------------------------------------//
		
		var person_name = '';
		var per_designation='';
		var company_deductor_type='';
		var company_email='';
		
		var company_filters = new Array();
	
		var company_column = new Array();
		company_column.push( new nlobjSearchColumn('custrecord_typeof_deductor'));    
		company_column.push( new nlobjSearchColumn('custrecord_personname'));    
		company_column.push( new nlobjSearchColumn('custrecord_designation'));    
		company_column.push( new nlobjSearchColumn('custrecord_email'));    
		
		var company_results = nlapiSearchRecord('customrecord_taxcompanyinfo', null, company_filters, company_column);
		if (company_results != null && company_results != '' && company_results != undefined) 
		{
			person_name = company_results[0].getValue('custrecord_personname')
			
			per_designation = company_results[0].getValue('custrecord_designation')
			
			company_deductor_type = company_results[0].getValue('custrecord_typeof_deductor')
			
			company_email = company_results[0].getValue('custrecord_email')
			
		}
		
		var personname = form.addField('custpage_personname', 'select', 'Person Resposible','employee');
        personname.setDisplayType('entry');
		personname.setDefaultValue(person_name)
		
		var designation = form.addField('custpage_designation', 'text', 'Designation');
        designation.setDisplayType('entry');
		designation.setDefaultValue(per_designation);
		designation.setDisplayType('disabled');
		
		var deductor_type = form.addField('custpage_typeof_deductor', 'text', 'Deductor Type');
		deductor_type.setDefaultValue(company_deductor_type);
		deductor_type.setDisplayType('disabled');
		
		var email = form.addField('custpage_email', 'email', 'Email');
		email.setDefaultValue(company_email);
		
		//-----------------------------------------------End Populate the Default person responsible Detail-------------------------------------//
		
		
		//deductor_type.setDefaultValue(company_deductor_type);
		
		var quarter = form.addField('custpage_quarter', 'select', 'Quarter', 'customlist_quater');
       	quarter.setDisplayType('entry');
		
		
		var Book_entry = form.addField('custpage_book_entry', 'select', 'Book Entry', 'customlist_book_entry');
      	Book_entry.setDisplayType('entry');
		
		var return_type = form.addField('custpage_return_type', 'select', 'Return type', 'customlist_return_type');
     	return_type.setDisplayType('entry');
		
		var loc = form.addField('custpage_location', 'select', 'Location');
        loc.addSelectOption('', '');
        popluatlocation(request, response, loc);
	
		var sysdate=new Date
		var date1=nlapiDateToString(sysdate)
		var pdate=form.addField('pdate','date', 'File Creation Date');
		pdate.setDefaultValue(date1)
		pdate.setDisplayType('disabled');
		
		var context = nlapiGetContext();
		var i_subcontext = context.getFeature('SUBSIDIARIES');
		
		if (i_subcontext != false) 
		{
			var i_AitGlobalRecId =  SearchGlobalParameter();
			
	
			if(i_AitGlobalRecId != 0 )
			{
				var o_AitGloRec = nlapiLoadRecord('customrecord_indian_tax_glo_parameter',i_AitGlobalRecId);
				
				var a_subisidiary = new Array();
				
				a_subisidiary = o_AitGloRec.getFieldValues('custrecord_ait_subsidiary');
				
		
			}
			var subsidiary = form.addField('subsidiary', 'select', 'Subsidiary');
			subsidiary.addSelectOption('', '');
        	popluatsubsidiaries(request, response, subsidiary,a_subisidiary);
			subsidiary.setMandatory(true)
		}
	
		var minor_head = form.addField('custpage_minor_head', 'select', 'Minor Head Of Challan 200-TDS payable by taxpayer | 400-TDS regular assessment (Raised by IT Dept)', 'customlist_minor_head');
       	minor_head.setDisplayType('entry');
		
		
		form.addSubmitButton('Generate eTDS File');
		
		
		form.addButton('custpage_email_etds_File', 'Generate & Email eTDS File',"genrate_etds_file();");
		
		response.writePage(form);
		}
		else
        if (request.getMethod() == 'POST')
		{
            create_quartely_detailrecord(request);
			
		   // nlapiSetRedirectURL('SUITELET', 'customscript_sut_genrate_etds', '1', false);
        }
}

// END SUITELET ====================================================




// BEGIN OBJECT CALLED/INVOKING FUNCTION ===================================================
{
function SearchGlobalParameter()
{
	var a_filters = new Array();
	var a_column = new Array();
	var i_globalRecId = 0;
	
    a_column.push(new nlobjSearchColumn('internalid'));
	
	var s_serchResult = nlapiSearchRecord('customrecord_indian_tax_glo_parameter',null,null,a_column)
	
	if(s_serchResult != null && s_serchResult != undefined && s_serchResult != '')
	{
		for (var i=0; i<s_serchResult.length && s_serchResult != null; i++) 	
		{
			i_globalRecId = s_serchResult[0].getValue('internalid');
			nlapiLogExecution('DEBUG','Bill ', "i_globalRecId"+i_globalRecId);
		
		}
		
	}
	return i_globalRecId;
}

function popluatlocation(request, response, taxagency)
	{
	   // var subsidiary = request.getParameter('subsidiary')
	    var ved_filchk1 = new Array();
	    var ved_columnsCheck1 = new Array();

	   // ved_filchk1.push( new nlobjSearchFilter('subsidiary', null, 'anyof', subsidiary));

	    ved_columnsCheck1.push( new nlobjSearchColumn('internalid'));
	    ved_columnsCheck1.push( new nlobjSearchColumn('name'));

	    var ved_searchResultsCheck1 = nlapiSearchRecord('customrecord_tds_state_master', null, ved_filchk1, ved_columnsCheck1);
	    if (ved_searchResultsCheck1 != null)
		{
	        for (i = 0; ved_searchResultsCheck1 != null && i < ved_searchResultsCheck1.length; i++)
			{
	            taxagency.addSelectOption(ved_searchResultsCheck1[i].getValue('internalid'), ved_searchResultsCheck1[i].getValue('name'), false);

	        }
	    }

	}
function popluatyear(request, response, taxagency)
	{
	   // var subsidiary = request.getParameter('subsidiary')
	    var ved_filchk1 = new Array();
	    var ved_columnsCheck1 = new Array();

	   // ved_filchk1.push( new nlobjSearchFilter('subsidiary', null, 'anyof', subsidiary));

	    ved_columnsCheck1.push( new nlobjSearchColumn('internalid'));
	    ved_columnsCheck1.push( new nlobjSearchColumn('custrecord_fy_code'));

	    var ved_searchResultsCheck1 = nlapiSearchRecord('customrecord_financial_year', null, ved_filchk1, ved_columnsCheck1);
	    if (ved_searchResultsCheck1 != null)
		{
	        for (i = 0; ved_searchResultsCheck1 != null && i < ved_searchResultsCheck1.length; i++)
			{
	            taxagency.addSelectOption(ved_searchResultsCheck1[i].getValue('internalid'),ved_searchResultsCheck1[i].getValue('custrecord_fy_code'), false);

	        }
	    }

	}
function popluatsubsidiaries(request, response, taxagency,subsidiary_id)
	{
	   // var subsidiary = request.getParameter('subsidiary')
	    var ved_filchk1 = new Array();
	    var ved_columnsCheck1 = new Array();

	   ved_filchk1.push( new nlobjSearchFilter('internalid', null, 'anyof', subsidiary_id));

	    ved_columnsCheck1.push( new nlobjSearchColumn('internalid'));
	    ved_columnsCheck1.push( new nlobjSearchColumn('name'));

	    var ved_searchResultsCheck1 = nlapiSearchRecord('subsidiary', null, ved_filchk1, ved_columnsCheck1);
	    if (ved_searchResultsCheck1 != null)
		{
	        for (i = 0; ved_searchResultsCheck1 != null && i < ved_searchResultsCheck1.length; i++)
			{
	            taxagency.addSelectOption(ved_searchResultsCheck1[i].getValue('internalid'),ved_searchResultsCheck1[i].getValue('name'), false);

	        }
	    }

	}

function create_quartely_detailrecord(request)
{
	var subsidiary = request.getParameter('subsidiary')
	nlapiLogExecution('DEBUG', 'create_quartely_detailrecord', 'subsidiary->' + subsidiary)
	
	var minor_head = request.getParameter('custpage_minor_head')
	nlapiLogExecution('DEBUG', 'create_quartely_detailrecord', 'minor_head->' + minor_head)
	
	var creation_date = request.getParameter('pdate')
	nlapiLogExecution('DEBUG', 'create_quartely_detailrecord', 'creation_date->' + creation_date)
	
	var financial_yr = request.getParameter('custpage_year')
	nlapiLogExecution('DEBUG', 'create_quartely_detailrecord', 'financial yr->' + financial_yr)
	
	var personname = request.getParameter('custpage_personname')
	nlapiLogExecution('DEBUG', 'create_quartely_detailrecord', 'person name->' + personname)
	
	var designation = request.getParameter('custpage_designation')
	nlapiLogExecution('DEBUG', 'create_quartely_detailrecord', 'designation->' + designation)
	
	var deductor_type = request.getParameter('custpage_typeof_deductor')
	nlapiLogExecution('DEBUG', 'create_quartely_detailrecord', 'deductor_type->' + deductor_type)
	
	var email = request.getParameter('custpage_email')
	nlapiLogExecution('DEBUG', 'create_quartely_detailrecord', 'email->' + email)
	
	var quarter = request.getParameter('custpage_quarter')
	nlapiLogExecution('DEBUG', 'create_quartely_detailrecord', 'quarter->' + quarter)
	
	var book_entry = request.getParameter('custpage_book_entry')
	nlapiLogExecution('DEBUG', 'create_quartely_detailrecord', 'book entry->' + book_entry)
	
	var return_type = request.getParameter('custpage_return_type')
	nlapiLogExecution('DEBUG', 'create_quartely_detailrecord', 'return_type ->' + return_type)
	
	var location = request.getParameter('custpage_location')
	nlapiLogExecution('DEBUG', 'create_quartely_detailrecord', 'location->' + location)
	
    var filters = new Array();
	filters.push( new nlobjSearchFilter('custrecord_financial_year', null, 'is',financial_yr));
	filters.push( new nlobjSearchFilter('custrecord_quater', null, 'is', quarter));
	
	var context = nlapiGetContext();
	var i_subcontext = context.getFeature('SUBSIDIARIES');
		
	if (i_subcontext != false) 
	{
		filters.push(new nlobjSearchFilter('custrecord_quarterlyreturn_subsidiary', null, 'is', subsidiary));
	} 
	var column = new Array();
	column.push( new nlobjSearchColumn('internalid'));    
	
	var results = nlapiSearchRecord('customrecord_quarterly_return_details', null, filters, column);
	if(results != null && results != '' && results != undefined)
	{
			nlapiLogExecution('DEBUG', 'create_quartely_detailrecord', 'inside search')
			var i_internaid = results[0].getValue('internalid')
			nlapiLogExecution('DEBUG', 'create_quartely_detailrecord', 'i_internaid->' + i_internaid)
			
			var o_quarterly_recordobj = nlapiLoadRecord('customrecord_quarterly_return_details',i_internaid)
			nlapiLogExecution('DEBUG', 'create_quartely_detailrecord', 'load Quarterly detail record->' + o_quarterly_recordobj)
	}
	else
	{
			nlapiLogExecution('DEBUG', 'create_quartely_detailrecord', 'inside else search')
			var o_quarterly_recordobj = nlapiCreateRecord('customrecord_quarterly_return_details')
			nlapiLogExecution('DEBUG', 'create_quartely_detailrecord', 'create Quarterly detail record->' + o_quarterly_recordobj)
	}
	
	if (o_quarterly_recordobj != null && o_quarterly_recordobj != '' && o_quarterly_recordobj != undefined) 
	{
		o_quarterly_recordobj.setFieldValue('custrecord_financial_year', financial_yr)
		
		o_quarterly_recordobj.setFieldValue('custrecord_minor_head', minor_head)
		
		o_quarterly_recordobj.setFieldValue('custrecord_person_resposible', personname)
		
		o_quarterly_recordobj.setFieldValue('custrecord_email_id',email)
		
		o_quarterly_recordobj.setFieldValue('custrecord_person_resp_designation', designation)
		
		o_quarterly_recordobj.setFieldValue('custrecord_deductee_type', deductor_type)
		
		o_quarterly_recordobj.setFieldValue('custrecord_quater', quarter)
		
		o_quarterly_recordobj.setFieldValue('custrecord_bookentry', book_entry)
		
		o_quarterly_recordobj.setFieldValue('custrecord_return_type', return_type)
		
		o_quarterly_recordobj.setFieldValue('custrecord_quaterlyreturn_location', location)
		
		if (i_subcontext != false) 
		{
			o_quarterly_recordobj.setFieldValue('custrecord_quarterlyreturn_subsidiary', subsidiary)
		}
		o_quarterly_recordobj.setFieldValue('custrecord_quarterlyreturn_filecreated', creation_date)
		
		o_quarterly_recordobj.setFieldValue('custrecord_tds_file_status','In-Progress')
		
		nlapiLogExecution('DEBUG', 'create_quartely_detailrecord', 'inside create')
	
		var submit_id = nlapiSubmitRecord(o_quarterly_recordobj);
		nlapiLogExecution('DEBUG', 'create_quartely_detailrecord', 'Quarterly record id->' + submit_id)	
		
		if (submit_id != null && submit_id != '' && submit_id != undefined) 
		{
			var params = new Array();
			params['runasadmin'] = 'T';
			params['custscript_recordid'] = submit_id;
			params['custscript_sent_etdsmail'] = 'F';
			var status = nlapiScheduleScript('customscript_genrate_etds', null, params);
			nlapiLogExecution('DEBUG', 'Script Scheduled ', ' Script Status -->' + status);
			
			nlapiSetRedirectURL('RECORD', 'customrecord_quarterly_return_details',submit_id,'VIEW');
		}
	}
}

}

// END OBJECT CALLED/INVOKING FUNCTION =====================================================
