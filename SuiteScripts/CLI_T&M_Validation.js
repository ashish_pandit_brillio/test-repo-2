// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
    /*
     Script Name: CLI_T&M_Validation
     Author: Ashish Panditt
     Company:Brillio 
     Date: 16-05-2018
     Description: Script for validations
     
	 Script Modification Log:
     This script will Block Direct creation of Sale order, Estimate, Invoice.
     -- Date --			-- Modified By --				--Requested By--				-- Description --
    
	
	Below is a summary of the process controls enforced by this script file.  The control logic is described
     more fully, below, in the appropriate function headers and code blocks.
     PAGE INIT
     - pageInit(type)
     SAVE RECORD
     - saveRecord()
     VALIDATE FIELD
     - validateField(type, name, linenum)
     FIELD CHANGED
     - fieldChanged(type, name, linenum)
     POST SOURCING
     - postSourcing(type, name)
     LINE INIT
     - lineInit_ActivePriceList(type)
     VALIDATE LINE
     - validateLine()
     RECALC
     - reCalc()
     SUB-FUNCTIONS
     - The following sub-functions are called by the above core functions in order to maintain code
     modularization:
     */
}
// END SCRIPT DESCRIPTION BLOCK  ====================================


// BEGIN GLOBAL VARIABLE BLOCK  =====================================
{

    //  Initialize any Global Variables, in particular, debugging variables...
}
// END GLOBAL VARIABLE BLOCK  =======================================
function pageInit(type)
{
	var obj_remove_bttn = document.getElementById('people_plan_sublist_remove');
	if(obj_remove_bttn){
	obj_remove_bttn.remove();
	}
	var obj_remove_bttn1 = document.getElementById('people_plan_sublist1_remove');
	if(obj_remove_bttn1){
	obj_remove_bttn1.remove();
	}
	

}
function validateLine(type)
{
	//alert('type'+type);
	if(type == 'people_plan_sublist1')
	{
		
		var i_currentIndex = nlapiGetCurrentLineItemIndex('people_plan_sublist1');
		var Count = nlapiGetLineItemCount('people_plan_sublist1');
		var i_rampListNext = nlapiGetCurrentLineItemValue('people_plan_sublist1','ramp_list');
		var i_subPracticeNext = nlapiGetCurrentLineItemValue('people_plan_sublist1','select_practices');
		for(i_index= 1; i_index<=Count;i_index++)
		{
			if(i_index != i_currentIndex)
			{
				var i_preList = nlapiGetLineItemValue('people_plan_sublist1','ramp_list',i_index);
				var i_preSubpractice = nlapiGetLineItemValue('people_plan_sublist1','select_practices',i_index);
				if(i_rampListNext ==i_preList && i_subPracticeNext ==i_preSubpractice)
				{	
					alert ('Please Select differnt Sub Practice');
					return false;
				}
			}
		
		}
		if(!i_subPracticeNext)
		{
			alert('Please Select Sub Practice');
			return false;
		}
		if(!i_rampListNext)
		{
			alert('Please Select Ramp List');
			return false;
		}
		
	}
	
	
  return true;
}
//=======================================================Validate Field============================================
function fieldChange(type, name)
{
	
   // if field is not having string characters, fail validation
   if (type == 'people_plan_sublist1')
   {
		var i_rampList = nlapiGetCurrentLineItemValue('people_plan_sublist1','ramp_list');
		if(i_rampList ==3 || i_rampList ==2)
		{
		
			if(name !='select_practices' && name != 'ramp_list' && name != 'text_note')
			{
			var i_value = nlapiGetCurrentLineItemValue('people_plan_sublist1',name);
			
				if(i_value>0)
				{
				alert("Please Enter Negative Value");
				nlapiSetCurrentLineItemValue('people_plan_sublist1',name,'',false,false);
				return false;
				}
			}
		}
		if(i_rampList == 1)
		{
				
				if(name !='select_practices' && name != 'ramp_list' && name != 'text_note')
				{
					var i_value = nlapiGetCurrentLineItemValue('people_plan_sublist1',name);
				
					if(i_value<0)
					{
					alert("Please Enter Positive Value");
					nlapiSetCurrentLineItemValue('people_plan_sublist1',name,'',false,false);
					return false;
					}
				}
		}
		//if(i_rampList !=2)
		var i_allocationValue = nlapiGetLineItemValue('people_plan_sublist1',name,1);
		//alert('In Function');	 
		  var tempValue = nlapiGetLineItemValue('people_plan_sublist1',name, nlapiGetCurrentLineItemIndex('people_plan_sublist1'));
			if(name !='select_practices' && name != 'ramp_list' && name != 'text_note')
			{
				var i_value = nlapiGetCurrentLineItemValue('people_plan_sublist1',name);
				//alert('name'+name);
				if(!/^[-+]?[0-9].+$/.test(i_value))
				{
					 if(nlapiGetCurrentLineItemIndex('people_plan_sublist1')==1)
						{
							alert('You are not allow to edit this field');
							nlapiSetCurrentLineItemValue('people_plan_sublist1',name, tempValue,false,false);
						}
						else
						{
							alert("Please Enter Numeric Characters");
							nlapiSetCurrentLineItemValue('people_plan_sublist1',name,'',false,false);
						}
				
				return false;
				}
				if(i_allocationValue==0)
				{
					alert('You dont have allocation for this month');
					nlapiSetCurrentLineItemValue('people_plan_sublist1',name,'',false,false);
					return false;
				}
				
			}
				
		
   }
   var tempValue = nlapiGetLineItemValue('people_plan_sublist1',name, nlapiGetCurrentLineItemIndex('people_plan_sublist1'));
   if(type == 'people_plan_sublist1' && nlapiGetCurrentLineItemIndex('people_plan_sublist1')==1)
	{
		alert('You are not allow to edit this field');
		nlapiSetCurrentLineItemValue('people_plan_sublist1',name, tempValue,false,false);
	}
   //  Always return true at this level, to continue validating other fields
   return true;
}
//=======================================================Start Save Function============================================================================

//function to validate revenue share doesn't exceed project value
function saveRecord_TndM()
{
	try
	{
		var s_customer = nlapiGetFieldValue('customer_selected');
		var s_project_region = nlapiGetFieldText('proj_region');
		var s_project_cust = nlapiGetFieldText('customer_selected');
		var s_project_strt = nlapiGetFieldValue('proj_strt_date');
		var s_project_end = nlapiGetFieldValue('proj_end_date');
		//alert('s_project_end'+s_project_end);
		var s_project_prac = nlapiGetFieldText('proj_practice');
		var i_count_allocation_details = nlapiGetLineItemCount('people_plan_sublist');
		//alert('i_count_allocation_details'+i_count_allocation_details);
		var i_projectId = nlapiGetFieldValue('proj_selected');
		var d_todays_date = new Date();
		var d_proj_start_date = nlapiDateToString(d_todays_date);
		var custRecDate = nlapiGetFieldValue('cust_rec_date');
		var i_count_allocation_details_next = nlapiGetLineItemCount('people_plan_sublist1');
		//alert('i_count_allocation_details_next',i_count_allocation_details_next);
		var monthBreakUp = getMonthsBreakup(
				        nlapiStringToDate(d_proj_start_date),
				        nlapiStringToDate(s_project_end));
		//alert('monthBreakUp'+monthBreakUp.length);
		
		//validate for Blank practice in ramp up tab
		var ramup_count = nlapiGetLineItemCount('people_plan_sublist1');
		for(var ramp_ind=1;ramp_ind<=ramup_count;ramp_ind++){
		var i_rampUP = nlapiGetLineItemText('people_plan_sublist1','ramp_list',ramp_ind);
		var i_ramp_Practice = nlapiGetLineItemText('people_plan_sublist1','select_practices',ramp_ind);
		
		if( _logValidation(i_rampUP) && _nullValidation(i_ramp_Practice)){
		alert('You cannot submit the ramup plan with blank practice !!');
		return false;
		}
		}
		
		var i_current_month = '';
		var a_total_row_clubbed_array = [];
		for(var i_index_count=1; i_index_count<=i_count_allocation_details; i_index_count++)
			{
				var i_practice = nlapiGetLineItemValue('people_plan_sublist','parent_practice_month_end',i_index_count);
				var i_practice_text = nlapiGetLineItemText('people_plan_sublist','parent_practice_month_end',i_index_count);
				var i_sub_practice = nlapiGetLineItemValue('people_plan_sublist','sub_practice_month_end',i_index_count);
				var i_sub_practice_text = nlapiGetLineItemText('people_plan_sublist','sub_practice_month_end',i_index_count);
				var i_role = nlapiGetLineItemValue('people_plan_sublist','role_month_end',i_index_count);
				var i_role_text = nlapiGetLineItemText('people_plan_sublist','role_month_end',i_index_count);
				var i_level = nlapiGetLineItemValue('people_plan_sublist','level_month_end',i_index_count);
				var i_level_text = nlapiGetLineItemText('people_plan_sublist','level_month_end',i_index_count);
				var i_location = nlapiGetLineItemValue('people_plan_sublist','location_month_end',i_index_count);
				var i_location_text = nlapiGetLineItemText('people_plan_sublist','location_month_end',i_index_count);
				var i_bill_Rate = nlapiGetLineItemValue('people_plan_sublist','resource_cost_month_end',i_index_count);
				
				var a_per_row_array = new Array();
				
				for (var j = 0; j < monthBreakUp.length; j++)
				{
					var months = monthBreakUp[j];
					var s_month_name = getMonthName(months.Start); // get month name
					var month_strt = nlapiStringToDate(months.Start);
					var month_end = nlapiStringToDate(months.End);
					var i_month = month_strt.getMonth();
					var i_year = month_strt.getFullYear();
					var fld_s_month_name = s_month_name +'_'+ i_year;
					var d_today_date = new Date();
					var mnth_share=0;
					if(i_month == d_today_date.getMonth())
					{
						i_current_month = i_month;
					}
					
					//if(month_strt > d_today_date)
					{
						var i_s_month_name_value_fte = nlapiGetLineItemValue('people_plan_sublist',''+fld_s_month_name.toLowerCase()+'_first',i_index_count);
						var i_s_month_name_value_workDay = nlapiGetLineItemValue('people_plan_sublist',''+fld_s_month_name.toLowerCase()+'_second',i_index_count);
						var i_s_month_name_value = nlapiGetLineItemValue('people_plan_sublist',''+fld_s_month_name.toLowerCase()+'_third',i_index_count);
						if(!i_s_month_name_value)
							i_s_month_name_value = 0;
						//alert('fld_s_month_name.toLowerCase()'+fld_s_month_name.toLowerCase()+'_third');
						a_per_row_array.push({
										'prac': i_practice,
										'subprac': i_sub_practice,
										'subprac_text': i_sub_practice_text,
										'role': i_role,
										'level': i_level,
										'level_text': i_level_text,
										'loc': i_location,
										'cost': i_bill_Rate,
										'project': i_projectId,
										'fte': i_s_month_name_value_fte,
										'workingdays':i_s_month_name_value_workDay,
										'allo': i_s_month_name_value,
										'mnth':s_month_name,
										'year':i_year
										});
					}
				}
				//alert(JSON.stringify(a_per_row_array));
				a_total_row_clubbed_array.push(a_per_row_array);
				
				if(i_index_count == 25)
				{
					//alert('frst:- '+i_index_count);
					var o_allocation_json = nlapiCreateRecord('customrecord_tm_month_total_json');
					//o_allocation_json.setFieldValue('custrecord_revenue_share_parent_json',i_revenue_share_id);
					o_allocation_json.setFieldValue('custrecord_current_mnth_number',parseInt(i_current_month));
					o_allocation_json.setFieldValue('custrecord_json1',JSON.stringify(a_total_row_clubbed_array));
					o_allocation_json.setFieldValue('custrecord_project_json',i_projectId);
					
					var a_per_row_array = new Array();
					var a_total_row_clubbed_array = [];
				}
				else if((i_index_count > 25 && i_index_count == 50) || (i_index_count == i_count_allocation_details && i_index_count > 25 && i_index_count <= 50))
				{
					//alert('second line:- '+i_index_count);
					o_allocation_json.setFieldValue('custrecord_json2',JSON.stringify(a_total_row_clubbed_array));
				
					var a_per_row_array = new Array();
					var a_total_row_clubbed_array = [];
				}
				else if((i_index_count > 50 && i_index_count == 75) || (i_index_count == i_count_allocation_details && i_index_count > 50 && i_index_count <= 75))
				{
					//alert('third line:- '+i_index_count);
					o_allocation_json.setFieldValue('custrecord_json3',JSON.stringify(a_total_row_clubbed_array));
				
					var a_per_row_array = new Array();
					var a_total_row_clubbed_array = [];
				}
				else if((i_index_count > 75 && i_index_count == 100) || (i_index_count == i_count_allocation_details && i_index_count > 75))
				{
					o_allocation_json.setFieldValue('custrecord_json4',JSON.stringify(a_total_row_clubbed_array));
				
					var a_per_row_array = new Array();
					var a_total_row_clubbed_array = [];
				}
			}
			if(i_count_allocation_details < 25)
			{
				//alert('inside line < 4');
				var o_allocation_json = nlapiCreateRecord('customrecord_tm_month_total_json');
				o_allocation_json.setFieldValue('custrecord_json_mnth_end_effrt',JSON.stringify(a_total_row_clubbed_array));
				 o_allocation_json.setFieldValue('custrecord_json1',JSON.stringify(a_total_row_clubbed_array));
				//o_allocation_json.setFieldValue('custrecord_revenue_share_parent_json',i_revenue_share_id);
				o_allocation_json.setFieldValue('custrecord_current_mnth_number',parseInt(i_current_month));
				o_allocation_json.setFieldValue('custrecord_project_json',i_projectId);
			}
			//============================================================================================
			var a_total_row_clubbed_array1 = [];
			for(var i_index=1; i_index<=i_count_allocation_details_next; i_index++)
			{
				var i_rampList = nlapiGetLineItemValue('people_plan_sublist1','ramp_list',i_index);
				var i_rampList_text = nlapiGetLineItemText('people_plan_sublist1','parent_practice_month_end',i_index);
				var i_sub_practice = nlapiGetLineItemValue('people_plan_sublist1','select_practices',i_index);
				var i_sub_practice_text = nlapiGetLineItemText('people_plan_sublist1','select_practices',i_index);
				//alert('i_sub_practice'+i_sub_practice);
				var s_note = nlapiGetLineItemText('people_plan_sublist1','text_note',i_index);
				
				var a_per_row_array_next = new Array();
				
				for (var j = 0; j < monthBreakUp.length; j++)
				{
					var months = monthBreakUp[j];
					var s_month_name = getMonthName(months.Start); // get month name
					var month_strt = nlapiStringToDate(months.Start);
					var month_end = nlapiStringToDate(months.End);
					var i_month = month_strt.getMonth();
					var i_year = month_strt.getFullYear();
					var fld_s_month_name = s_month_name +'_'+ i_year;
					var d_today_date = new Date();
					
					if(i_month == d_today_date.getMonth())
					{
						i_current_month = i_month;
					}
					
					//if(month_strt > d_today_date)
					{
						var i_s_month_name_value = nlapiGetLineItemValue('people_plan_sublist1',''+fld_s_month_name.toLowerCase()+'_second',i_index);
						//alert('i_s_month_name_value'+i_s_month_name_value);
						if(!i_s_month_name_value)
							i_s_month_name_value = 0;
						
						a_per_row_array_next.push({
										'rampList': i_rampList,
										'subprac': i_sub_practice,
										'subprac_text': i_sub_practice_text,
										'project': i_projectId,
										'note': s_note,
										'allo': i_s_month_name_value,
										'mnth':s_month_name,
										'year':i_year
										});
					}
				}
				//alert(JSON.stringify(a_per_row_array_next));
				a_total_row_clubbed_array1.push(a_per_row_array_next);
				//alert('i_allocation_json'+i_allocation_json);
				if(i_index == 25)
				{
					o_allocation_json.setFieldValue('custrecord_rampupjson1',JSON.stringify(a_total_row_clubbed_array1));
					var a_per_row_array_next = new Array();
					var a_total_row_clubbed_array1 = [];
				}
				else if((i_index > 25 && i_index == 50) || (i_index == i_count_allocation_details_next && i_index > 25 && i_index <= 50))
				{
					//alert('second line:- '+i_index);
					o_allocation_json.setFieldValue('custrecord_rampupjson2',JSON.stringify(a_total_row_clubbed_array1));
				
					var a_per_row_array_next = new Array();
					var a_total_row_clubbed_array1 = [];
				}
				else if((i_index > 50 && i_index == 75) || (i_index == i_count_allocation_details_next && i_index > 50 && i_index <= 75))
				{
					//alert('third line:- '+i_index);
					o_allocation_json.setFieldValue('custrecord_rampupjson3',JSON.stringify(a_total_row_clubbed_array1));
				
					var a_per_row_array_next = new Array();
					var a_total_row_clubbed_array1 = [];
				}
				else if((i_index > 75 && i_index == 100) || (i_index == i_count_allocation_details_next && i_index > 75))
				{
					o_allocation_json.setFieldValue('custrecord_rampupjson4',JSON.stringify(a_total_row_clubbed_array1));
				
					var a_per_row_array_next = new Array();
					var a_total_row_clubbed_array1 = [];
				}
			}
			if(i_count_allocation_details_next < 25)
			{
				//var o_allocation_json_next = nlapiCreateRecord('customrecord_tnm_total_ramp_up_json');
				o_allocation_json.setFieldValue('custrecord_rampupjson1',JSON.stringify(a_total_row_clubbed_array1));
				
			}
			
			
			//=====================================================================================================
			var Count = nlapiGetLineItemCount('people_plan_sublist1');
			var i_practices_involved = nlapiGetFieldValue('amt_practices_involved');
			var a_pract= new Array();
			a_pract = i_practices_involved.split('');
			//alert('i_practices_involved'+i_practices_involved);
			var i_total = 0;
			var s_subPracticeTotal = nlapiGetFieldValue('sub_practice_total');
			//alert('s_subPracticeTotal'+s_subPracticeTotal);
			var a_sub_pract= new Array();
			a_sub_pract = s_subPracticeTotal.split('');
			//alert('a_sub_pract length'+a_sub_pract.length);
			//alert('a_sub_pract '+a_sub_pract);
			for(i_practice = 0;i_practice<a_pract.length;i_practice++)
			{
				for (var j = 0; j < monthBreakUp.length; j++)
				{
					var months = monthBreakUp[j];
					var s_month_name = getMonthName(months.Start); // get month name
					var month_strt = nlapiStringToDate(months.Start);
					var month_end = nlapiStringToDate(months.End);
					var i_month = month_strt.getMonth();
					var i_year = month_strt.getFullYear();
					var fld_s_month_name = s_month_name +'_'+ i_year;
					var d_today_date = new Date();
					var mnth_share=0;
					//fld_sublist_effort_plan_mnth_end_next_tab.setLineItemValue(''+s_month_name.toLowerCase()+'_second',i_Counter+1,parseFloat(total_revenue));people_plan_sublist1
					
					var firstLineTotal_ = nlapiGetLineItemValue('people_plan_sublist1',''+fld_s_month_name.toLowerCase()+'_second',1);
					for(var i_index= 2; i_index<=Count;i_index++)
					{
						var i_lineTotal = nlapiGetLineItemValue('people_plan_sublist1',''+fld_s_month_name.toLowerCase()+'_second',i_index);
						if(!i_lineTotal)
							i_lineTotal=0;
						var i_subPractice = nlapiGetLineItemValue('people_plan_sublist1','select_practices',i_index);
						var s_subPract = nlapiGetLineItemText('people_plan_sublist1','select_practices',i_index);
						if(i_subPractice == i_practice)
						{
							i_total = parseInt(i_total) + parseInt(i_lineTotal);
							//alert('i_total='+i_total);
						}
					}
					//alert('a_sub_pract[j] '+j+a_sub_pract[j]);
					//alert('(parseInt(a_sub_pract[j]) + parseInt(i_total))'+(parseInt(a_sub_pract[j]) + parseInt(i_total)));
					if((parseInt(firstLineTotal_) + parseInt(i_total)) < 0)
					{
						alert('The Month '+s_month_name+' '+i_year+' have Negative value in '+s_subPract+' sub-practice');
						return false;
					}
					i_total = 0;
				}
				i_total = 0;
				
			}
			//============================================================================================
			try
			{
				var i_allocation_json = nlapiSubmitRecord(o_allocation_json,true,true);
				nlapiSetFieldValue('custome_record',i_allocation_json);
			}
			catch(err_rcrd)
			{
				alert('record creation error:- '+err_rcrd);
			}
			
			alert('Execution complete:- '+i_allocation_json);
		return true;
	}
	catch(err)
	{
		alert(err);
	}
}
//=======================================================End Save Function============================================================================
function finalSuitelet()
{
	alert('In Function');
	var url = nlapiResolveURL('SUITELET', 'customscript_sut_finaldisplaysuitelet_tm','customdeploy_sut_finaldisplaysuitelet_tm');
	suitelet_url: url + '&proj_id=' + 
	window.open(url);
}

function getMonthsBreakup(d_startDate, d_endDate) {
	
	try
	{
		var dateBreakUp = [];
		var new_start_date = new Date(d_startDate.getFullYear(), d_startDate
		        .getMonth(), 1);
				
		var endDate = nlapiAddDays(nlapiAddMonths(d_startDate, 1), -1);
		endDate = nlapiDateToString(endDate, 'date');
	
		dateBreakUp.push({
		    Start : nlapiDateToString(new_start_date, 'date'),
		    End : endDate
		});
	
		for (var i = 1;; i++) {
			var new_date = nlapiAddMonths(new_start_date, i);
	
			if (new_date > d_endDate) {
				break;
			}
			
			endDate = nlapiAddDays(nlapiAddMonths(new_date, 1), -1);
			endDate = nlapiDateToString(endDate, 'date');
					
			dateBreakUp.push({
			    Start : nlapiDateToString(new_date, 'date'),
			    End : endDate
			});
		}
	}
	catch(err)
	{
		nlapiLogExecution('ERROR','getMonthsBreakup','ERROR MESSAGE :- '+err);
	}

	return dateBreakUp;
}

function getMonthName(currentDate) {
	currentDate = nlapiStringToDate(currentDate)
	var monthNames = [ "JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JUL", "AUG",
	        "SEP", "OCT", "NOV", "DEC" ];
	return monthNames[currentDate.getMonth()];
}

function custom_plan()
{
	try{
		
		var a_revenue_cap_filter = [['custrecord_project_json', 'anyof', parseInt(nlapiGetFieldValue('proj_selected'))]];
				
				var a_column = new Array();
				a_column[0] = new nlobjSearchColumn('created').setSort(true);
				
				var a_get_logged_in_user_exsiting_revenue_cap = nlapiSearchRecord('customrecord_tm_month_total_json', null, a_revenue_cap_filter, a_column);
				var i_revenue_share_id='';
				if (a_get_logged_in_user_exsiting_revenue_cap) {
					i_revenue_share_id = a_get_logged_in_user_exsiting_revenue_cap[0].getId();
				}
		//	var emails = nlapiLookupField('job',nlapiGetFieldValue('proj_selected'),['custentity_clientpartner.email','custentity_projectmanager.email','custentity_deliverymanager.email']);
		
		//Updated the logic because of permission vialtion error from NS for employee center roles
		var response = nlapiRequestURL(nlapiResolveURL('SUITELET', 'customscript_fetch_projectdetails_tm', 'customdeploy_fetch_projectdetails_tm') + '&i_project_interal_id='+nlapiGetFieldValue('proj_selected'), null, null);
		var res = response.getBody();
		nlapiLogExecution('DEBUG','Suitelet response',(res));
		res = JSON.parse(res);
		nlapiLogExecution('DEBUG','Suitelet response JSON.parse',(res));
		//nlapiLogExecution('DEBUG','Suitelet response res[0].CP',res[0].CP);
		nlapiLogExecution('DEBUG','Suitelet response res.CP',res.CP);
			var a_recipient_mail_id = [];	
			var a_participating_mail_id = [];
			a_recipient_mail_id.push(res.CP);
			a_recipient_mail_id.push(res.DM);
			a_recipient_mail_id.push(res.PM);
			var i_ADO=res.i_ADO? res.i_ADO : "";
			a_participating_mail_id.push('billing@brillio.com');
			//a_participating_mail_id.push('vikash.garodia@brillio.com');
		//	a_participating_mail_id.push('bhavanishankar.t@brillio.com');
			a_participating_mail_id.push('team.fpa@brillio.com');
	
			// mail to BO team and participating practice head
			var strVar = '';
			strVar += '<html>';
			strVar += '<body>';
			
			strVar += '<p>Hello All,</p>';
			strVar += '<p> The plan for the following T&M project has been confirmed in NetSuite by PM </p>';
						
			strVar += '<p>Region:- '+nlapiGetFieldText('proj_region')+'<br>';
			strVar += 'Customer:- '+nlapiGetFieldText('customer_selected')+'<br>';
			strVar += 'Project:- '+nlapiGetFieldText('proj_selected')+'<br>';
			strVar += 'Project Start '+nlapiGetFieldValue('proj_strt_date')+' and End Date '+nlapiGetFieldValue('proj_end_date')
			+'<br>';
			strVar += 'Executing Practice:- '+nlapiGetFieldText('proj_practice')+'</p>';
			var r_url = nlapiResolveURL('SUITELET', 'customscript_sut_displayprojectdetails','customdeploy_sut_displayprojectdetails');
			var ahref = 'https://system.na1.netsuite.com'+r_url

			ahref+= '&proj_region='+nlapiGetFieldValue('proj_region');
			ahref+='&view=email';
			// ahref+='&revenue_rcrd_status=1';
			// ahref+='&customer_selected='+nlapiGetFieldValue('customer_selected');
			// ahref+='&proj_selected='+nlapiGetFieldValue('proj_selected');
			// ahref+='&proj_sow_value='+nlapiGetFieldValue('proj_sow_value');
			// ahref+='&proj_strt_date='+nlapiGetFieldValue('proj_strt_date');
			// ahref+='&proj_end_date='+nlapiGetFieldValue('proj_end_date');
			// ahref+='&proj_currency='+nlapiGetFieldValue('proj_currency');
			// ahref+='&proj_practice='+nlapiGetFieldValue('proj_practice');
			// ahref+='&proj_manager_practice='+nlapiGetFieldValue('proj_manager_practice');
			// ahref+='&amt_practices_involved='+nlapiGetFieldValue('amt_practices_involved');
			var obj = JSON.stringify({
										"proj_region":nlapiGetFieldValue('proj_region'),
										"existing_rcrd_id":i_revenue_share_id,
										"customer_selected":nlapiGetFieldValue('customer_selected'),
										"proj_selected":nlapiGetFieldValue('proj_selected'),
										"proj_sow_value":nlapiGetFieldValue('proj_sow_value'),
										"proj_strt_date":nlapiGetFieldValue('proj_strt_date'),
										"proj_end_date":nlapiGetFieldValue('proj_end_date'),
										"proj_currency":nlapiGetFieldValue('proj_currency'),
										"proj_practice":nlapiGetFieldValue('proj_practice'),
										"proj_manager_practice":nlapiGetFieldValue('proj_manager_practice'),
										"amt_practices_involved":nlapiGetFieldValue('amt_practices_involved'),
										"i_practices_involved_Ids":nlapiGetFieldValue('amt_practices_involved_id'),
                                        "recordIdJSON":nlapiGetFieldValue('custome_record')
									});	
			ahref+='&data='+nlapiEncrypt(obj, "aes", "48656C6C6F0B0B0B0B0B0B0B0B0B0B0B");
			strVar += '<a href='+ahref+' >Link to Revenue share and Budget effort screen in Netsuite</a>';
			
			strVar += '<p>Regards,</p>';
			strVar += '<p>Finance Team</p>';
				
			strVar += '</body>';
			strVar += '</html>';
			
			var a_emp_attachment = new Array();
			a_emp_attachment['entity'] = '41571';
			
			nlapiSendEmail(442, a_recipient_mail_id, 'T&M project plan has been confirmed:'+nlapiGetFieldText('proj_selected'), strVar,a_participating_mail_id,null,a_emp_attachment);

	var recId = nlapiGetFieldValue('cust_rec_id');
	//alert('recId'+recId);
	
	//var con = confirm('Record Confirmed');
	//if(con==true)
	{
		//nlapiSubmitField('customrecord_tm_month_total_json',recId,'custrecord_is_confirm','T');
		var rec_submit = nlapiLoadRecord('customrecord_tm_month_total_json',recId);
		rec_submit.setFieldValue('custrecord_is_confirm','T');
		nlapiSubmitRecord(rec_submit);
      
	         /*   var url = nlapiResolveURL('SUITELET','customscript_call_sfdc_t_and_m', 'customdeploy_call_sfdc_t_and_m',true);
                url +='&ProjectId='+nlapiGetFieldValue('proj_selected');
                var a = new Array();
                a['User-Agent-x'] = 'SuiteScript-Call';
                nlapiLogExecution("DEBUG","Call the data");
                nlapiRequestURL(url, null, a); 
        */
		
		var url ='';
      
      //nlapiResolveURL('SUITELET', 'customscript_sut_tnm_dashboard_projectli','customdeploy_sut_tnm_dashboard_projectli');
	
	/*if(i_ADO==nlapiGetUser()){
	url = nlapiResolveURL('SUITELET', 'customscript_sut_tnm_dashboard_projectli','customdeploy_sut_ado_tm_projectdetails');
	}else{
	url = nlapiResolveURL('SUITELET', 'customscript_sut_tnm_dashboard_projectli','customdeploy_sut_tnm_dashboard_projectli');
	}	
	*/	

	var s_deploymentid=nlapiGetFieldValue('custpage_currentuser_deployementid');
	s_deploymentid=s_deploymentid?s_deploymentid:'customdeploy_sut_tnm_dashboard_projectli';
      url = nlapiResolveURL('SUITELET', 'customscript_sut_tnm_dashboard_projectli',s_deploymentid);
		window.open(url,'_self');
		
		//window.close();
	}
	
}
catch(error){
	nlapiLogExecution('ERROR','Confirm Function','ERROR MESSAGE :- '+error.message);
}

}
function _logValidation(value) 
{
 if(value != null && value.toString() != null && value != '' && value != undefined && value.toString() != undefined && value != 'undefined' && value.toString() != 'undefined'&& value.toString() != 'NaN' && value != NaN) 
 {
  return true;
 }
 else 
 { 
  return false;
 }
}
///NULLVALIDATION FUNCTION///
function _nullValidation(value)
{
    if (value == null || value == 'NULL' || value == 'NaN' || value == "" || value == '' || value == undefined || value == '&nbsp;')
    {
        return true;
    }
    else
    {
        return false;
    }
}