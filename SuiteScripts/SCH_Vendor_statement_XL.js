// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
/*
   	Script Name:SCH Vendor statment XL
	Author:		Nikhil jain
	Company:	Aashnacloudtech pvt.Ltd
	Date:		2nd May 2015


	Script Modification Log:

	-- Date --			-- Modified By --				--Requested By--				-- Description --




Below is a summary of the process controls enforced by this script file.  The control logic is described
more fully, below, in the appropriate function headers and code blocks.


     SCHEDULED FUNCTION
		- scheduledFunction(type)


     SUB-FUNCTIONS
		- The following sub-functions are called by the above core functions in order to maintain code
            modularization:

               - NOT USED

*/
}
// END SCRIPT DESCRIPTION BLOCK  ====================================


// BEGIN SCHEDULED FUNCTION =============================================

function schedulerFunction_xl_vendor_statement(type)
{
	/*  On scheduled function:
	 - PURPOSE
	 -
	 FIELDS USED:
	 --Field Name--				--ID--
	 */
	//==== CODE FOR DESGNING POP UP XL ======
	
		
		var csvCount = 0;
		var lineItemDataArray = new Array();
		var company_id = getcompanyinfo();
		
		if(company_id != null && company_id != '' && company_id != undefined)
		{
			var o_comapny_obj = nlapiLoadRecord('customrecord_taxcompanyinfo',company_id);
			var companyname = o_comapny_obj.getFieldValue('custrecord_companyname')
			var nameofpremises = o_comapny_obj.getFieldValue('custrecord_nameofthe_premises_buildi')
			var road = o_comapny_obj.getFieldValue('custrecord_road_street_lane')
			var area= o_comapny_obj.getFieldValue('custrecord_area_location')
			var city = o_comapny_obj.getFieldValue('custrecord_town_city_district')
			var state = o_comapny_obj.getFieldText('custrecord_state')
			var pincode = o_comapny_obj.getFieldValue('custrecord_pincode')
			
			var company_address  = ' ';
			if(nameofpremises != null && nameofpremises != '' && nameofpremises != undefined)
			{
				company_address = nameofpremises
			}
			if(road != null && road != '' && road != undefined)
			{
				company_address = company_address +','+road
			}
			if(area != null && area != '' && area != undefined)
			{
				company_address = company_address +','+area
			}
			if(city != null && city != '' && city != undefined)
			{
				company_address = company_address +','+city
			}
			if(state != null && state != '' && state != undefined)
			{
				company_address = company_address +','+state
			}
			if(pincode != null && pincode != '' && pincode != undefined)
			{
				company_address = company_address +','+pincode
			}
		
			var empty = " ";
			lineItemDataArray[csvCount] = empty+ "," +empty +"," + empty + "," + empty + "," + empty + "," + empty + "," + empty+ "," +  companyname + "," + empty + "," + empty + "," + empty + "," + empty +"," + empty+ "," + empty + "," + empty ;
			csvCount = csvCount + 1;
			lineItemDataArray[csvCount] = empty+ "," +empty +"," + empty + "," + empty + "," + company_address+ "," + empty + "," + empty + "," + empty + "," + empty +"," + empty+ "," + empty + "," + empty ;
			csvCount = csvCount + 1;
		}
		
		//nlapiLogExecution('DEBUG', 'HTMLDesignPopUp', 'Request Method = ' + request.getMethod());
		var htmlMsg = "<HTML>";
		//var htmlMsg = "";
		
			var htmlMsg = "";
			nlapiLogExecution('DEBUG', 'searchVendorTransactions', 'Searching Vendor Transactions');
			//var vendor = request.getParameter('vendorname');
			//var locationname = request.getParameter('locationname');
			//var start = request.getParameter('startdate');
			//var end = request.getParameter('enddate');
			//nlapiLogExecution('DEBUG', 'searchVendorTransactions', 'Searching Vendor Transactions');
			var context = nlapiGetContext();
		
			/*
			var vendor_a = context.getSetting('SCRIPT', 'custscript_ait_xl_vendorname')
		
			var locationname = context.getSetting('SCRIPT', 'custscript_ait_xl_locationname')
		
			var start = context.getSetting('SCRIPT', 'custscript_ait_xl_startdate')
		
			var end = context.getSetting('SCRIPT', 'custscript_ait_xl_enddate')
		
			nlapiLogExecution('DEBUG', 'searchVendorTransactions', 'end date = '+end);
		
			var subsidiary = context.getSetting('SCRIPT', 'custscript_ait_xl_subsidiary')
		
			nlapiLogExecution('DEBUG', 'searchVendorTransactions', 'subsidiary = '+subsidiary);
			
			var recepient = context.getSetting('SCRIPT', 'custscript_ait_xl_recepient')
			
			var currency_criteria = context.getSetting('SCRIPT', 'custscript_ait_xl_currency')
			
*/
		
		var venstatid = context.getSetting('SCRIPT', 'custscript_ait_csvvenstatid')
		
		var venstatobj = nlapiLoadRecord('customrecord_vendor_statement_detail_rec',venstatid)
		
		var vendor_a = venstatobj.getFieldValues('custrecord_ait_venstat_vendor')
		
		var locationname = venstatobj.getFieldValue('custrecord_ait_venstat_loc')
		
		var start = venstatobj.getFieldValue('custrecord_ait_venstat_startdate')
		
		var end = venstatobj.getFieldValue('custrecord_ait_vendstat_enddate')
		
		nlapiLogExecution('DEBUG', 'searchVendorTransactions', 'end date = '+end);
		
		var subsidiary = venstatobj.getFieldValue('custrecord_ait_venstat_subsidiary')
		
		nlapiLogExecution('DEBUG', 'searchVendorTransactions', 'subsidiary = '+subsidiary);
		
		var recepient = venstatobj.getFieldValue('custrecord_ait_venstat_email')
		
		var currency_criteria = venstatobj.getFieldValue('custrecord_ait_venstat_currency')
			
			if(parseInt(currency_criteria) == parseInt(1))
			{
				var custCurrency = "INR";
				
				var curFilters = new Array();
				var curColumns = new Array();
				curFilters.push(new nlobjSearchFilter('symbol', null, 'is', 'INR'));
				curColumns.push(new nlobjSearchColumn('internalid'));
				var curSearchResults = nlapiSearchRecord('currency', null, curFilters, curColumns);
				{
					var currency_id = curSearchResults[0].getValue('internalid');
					nlapiLogExecution('DEBUG', 'searchVendorTransactions', 'currency = ' + currency_id);
				}
					
				
			}
			else
			{
				var custCurrency = "Multi-Currency"
			}
			
			var startdate = nlapiStringToDate(start);
			var enddate = nlapiStringToDate(end);
			
			nlapiLogExecution('DEBUG', 'searchVendorTransactions', 'Vendor Name = ' + vendor);
			nlapiLogExecution('DEBUG', 'searchVendorTransactions', 'Location Name = ' + locationname);
			nlapiLogExecution('DEBUG', 'searchVendorTransactions', 'Start date = ' + startdate);
			nlapiLogExecution('DEBUG', 'searchVendorTransactions', 'End date = ' + enddate);
			
			//var vendor_array = new Array();
			//vendor_array = vendor_a.split(',')
			for (var z = 0; z < vendor_a.length; z++) 
			{
				var vendor = vendor_a[z];
				
				if (vendor != null && vendor != '' && vendor != undefined) 
				{
					var empty = " ";
					if (z != 0) 
					{
						lineItemDataArray[csvCount] = empty + "," + empty + "," + empty + "," + empty + "," + empty + "," + empty + "," + empty;
						csvCount = csvCount + 1;
						lineItemDataArray[csvCount] = empty + "," + empty + "," + empty + "," + empty + "," + empty + "," + empty + "," + empty;
						csvCount = csvCount + 1;
						lineItemDataArray[csvCount] = empty + "," + empty + "," + empty + "," + empty + "," + empty + "," + empty + "," + empty;
						csvCount = csvCount + 1;
					}
					
					lineItemDataArray[csvCount] = empty + "," + empty + "," + empty + "," + empty + "," + empty + "," + empty + "," + empty;
					csvCount = csvCount + 1;
					
					var custFilters = new Array();
					var custColumns = new Array();
					custFilters.push(new nlobjSearchFilter('internalid', null, 'is', vendor));
					custColumns.push(new nlobjSearchColumn('datecreated'));
					custColumns.push(new nlobjSearchColumn('entityid'));
					custColumns.push(new nlobjSearchColumn('address'));
					custColumns.push(new nlobjSearchColumn('currency'));
					var custSearchResults = nlapiSearchRecord('vendor', null, custFilters, custColumns);
					
					if (custSearchResults != null && custSearchResults != undefined && custSearchResults != '') 
					{
						var custCreatedDate = custSearchResults[0].getValue('datecreated');
						custCreatedDate = nlapiStringToDate(custCreatedDate);
						var custName = custSearchResults[0].getValue('entityid');
						var custAddr = custSearchResults[0].getValue('address');
						//var custCurrency = custSearchResults[0].getText('currency');
						
						//==============================Begin :- function to get the first transaction date===================//
						var firstTranDate = getFirstTransactionDate(vendor);
					
						if(firstTranDate != null && firstTranDate != '' && firstTranDate != undefined)
						{
							custCreatedDate = nlapiStringToDate(firstTranDate);
						}
						//==============================Begin :- function to get the first transaction date===================//
						nlapiLogExecution('DEBUG', 'searchVendorTransactions', 'custCreatedDate = ' + custCreatedDate);
						nlapiLogExecution('DEBUG', 'searchVendorTransactions', 'custName = ' + custName);
						nlapiLogExecution('DEBUG', 'searchVendorTransactions', 'custAddr = ' + custAddr);
						nlapiLogExecution('DEBUG', 'searchVendorTransactions', 'custCurrency = ' + custCurrency);
						
						custAddr = '"' + afterReplaceAddComma(custAddr) + '"';
						nlapiLogExecution('DEBUG', 'searchVendorTransactions', 'custAddr after replace = ' + custAddr);
						var locationnametext;
						if (locationname != '' && locationname != null && locationname != 'undefined' && locationname != undefined) 
						{
							locationnametext = getLocationName(locationname);
							nlapiLogExecution('DEBUG', 'searchVendorTransactions', 'locationnametext = ' + locationnametext);
						}
						else 
						{
							locationnametext = "All"
						}
						//custCurrency = getCurrency(custCurrency);
						//nlapiLogExecution('DEBUG', 'searchVendorTransactions', 'custCurrency = ' + custCurrency);
						
						var opBalanceEndDatems = new Date(startdate.getTime());
						nlapiLogExecution('DEBUG', 'searchVendorTransactions', 'opBalanceEndDatems = ' + opBalanceEndDatems);
						opBalanceEndDatems = opBalanceEndDatems - 86400000;
						nlapiLogExecution('DEBUG', 'searchVendorTransactions', 'opBalanceEndDatems = ' + opBalanceEndDatems);
						var opBalanceEndDate = new Date(opBalanceEndDatems);
						nlapiLogExecution('DEBUG', 'searchVendorTransactions', 'opBalanceEndDate = ' + opBalanceEndDate);
						
						var openingBalanceDate = start;
						nlapiLogExecution('DEBUG', 'searchVendorTransactions', 'openingBalanceDate = ' + openingBalanceDate);
						
						//var combined_opening_balance = getOpeningBalance(vendor, locationname, custCreatedDate, opBalanceEndDate,subsidiary);
					
						//var opening_balance_array = combined_opening_balance.split('#')
					
						//var openingBalance = opening_balance_array[0];
					
						//var fxopeningBalance = opening_balance_array[1];
						
						var openingBalance = getOpeningBalance(vendor, locationname, custCreatedDate, opBalanceEndDate,subsidiary);
						var empty = " ";
						
						//------------CSV IMPORT START------------------------------------
						lineItemDataArray[csvCount] = empty+ "," +empty +"," + empty + "," + empty + "," + empty + "," + empty + "," + empty+ "," + "Vendor Statement" + "," + empty + "," + empty + "," + empty + "," + empty +"," + empty+ "," + empty + "," + empty ;
						csvCount = csvCount + 1;
						lineItemDataArray[csvCount] = empty + "," + empty + "," + empty + "," + empty + "," + empty + "," + empty + "," + empty;
						csvCount = csvCount + 1;
						lineItemDataArray[csvCount] = "Vendor :-" + "," +custName +"," + empty + "," + empty + "," + empty + "," + empty + "," + empty+ "," + empty + "," + empty + "," + empty + "," + empty + "," + empty +"," + "From Date :-" + "," + start + "," + empty ;
						csvCount = csvCount + 1;
						lineItemDataArray[csvCount] = "Address :-" + "," + custAddr + "," + empty + "," + empty + "," + empty + "," + empty + "," + empty+ "," + empty + "," + empty + "," + empty + "," + empty + "," + empty + "," + "To Date :-" + "," + end + "," + empty ;
						csvCount = csvCount + 1;
						//lineItemDataArray[csvCount] = empty + "," +empty + "," +empty + "," +empty + "," +empty + "," +empty + "," +empty + "," +empty + "," +empty + "," +empty + "," +empty + "," +empty + "," +"Location :-" + "," + locationnametext + "," + empty ;
						//lineItemDataArray[csvCount] = empty + "," +empty + "," +empty + "," +empty + "," +empty + "," +empty + "," +empty + "," +empty + "," +empty + "," +empty + "," +empty + "," +empty + "," +"Location :-" + "," + empty + "," + empty ;
						//csvCount = csvCount + 1;
						lineItemDataArray[csvCount] = empty + "," +empty + "," +empty + "," +empty + "," +empty + "," +empty + "," +empty + "," +empty + "," +empty + "," +empty + "," +empty + "," +empty +  "," +"Currency :-" + "," + custCurrency + "," + empty ;
						csvCount = csvCount + 1;
						lineItemDataArray[csvCount] = empty + "," + empty + "," + empty + "," + empty + "," + empty + "," + empty + "," + empty;
						csvCount = csvCount + 1;
						
						
						htmlMsg = htmlMsg + "<body>";
						htmlMsg = htmlMsg + "<CENTER>";
						
						
						htmlMsg = htmlMsg + "<table id=detailsTable border=1>";
						htmlMsg = htmlMsg + "<tr>";
						htmlMsg = htmlMsg + "<td align=center width=10%>Date</td>";
						htmlMsg = htmlMsg + "<td align=center width=15%>Transaction</td>";
						htmlMsg = htmlMsg + "<td align=center width=15%>Reference No.</td>";
						htmlMsg = htmlMsg + "<td align=center width=20%>Memo</td>";
						htmlMsg = htmlMsg + "<td align=center width=13%>Debit</td>";
						htmlMsg = htmlMsg + "<td align=center width=13%>Credit</td>";
						htmlMsg = htmlMsg + "<td align=center width=14%>Balance<BR/></td>";
						
						htmlMsg = htmlMsg + "</tr>";
						
						//CSV
						//lineItemDataArray[csvCount] = "Transaction No." + "," +"Date" + "," + "Transaction Type" + "," + "Currency"+ "," + "Exchange Rate"+ "," + "Reference No." + "," + "Memo" + "," + "Location"+ "," + "Account"+ "," + "Debit" + "," + "Credit" + "," + "Balance"+ "," +"System Date";// "Debit In Foriegn Currency" + "," + "Credit In Foriegn Currency" + "," + "Balance In Foriegn Currency"+ "," + "System Date";
						lineItemDataArray[csvCount] = "Transaction No." + "," +"Date" + "," + "Transaction Type" + "," + "Currency"+ "," + "Exchange Rate"+ "," + "Reference No." + "," + "Memo" +"," + "Account"+ "," + "Debit" + "," + "Credit" + "," + "Balance"+ "," +"System Date";// "Debit In Foriegn Currency" + "," + "Credit In Foriegn Currency" + "," + "Balance In Foriegn Currency"+ "," + "System Date";
						csvCount = csvCount + 1;
						
						var blank = '-';
						
						var description = 'Opening Balance';
						htmlMsg = htmlMsg + "<tr>";
						htmlMsg = htmlMsg + "<td align=left width=10%>" + nlapiEscapeXML(openingBalanceDate) + "</td>";
						htmlMsg = htmlMsg + "<td align=justified width=15%>" + nlapiEscapeXML(description) + "</td>";
						htmlMsg = htmlMsg + "<td align=justified width=15%>" + nlapiEscapeXML(blank) + "</td>";
						htmlMsg = htmlMsg + "<td align=center width=20%>" + nlapiEscapeXML(blank) + "</td>";
						htmlMsg = htmlMsg + "<td align=center width=13%>" + nlapiEscapeXML(blank) + "</td>";
						htmlMsg = htmlMsg + "<td align=center width=13%>" + nlapiEscapeXML(blank) + "</td>";
						htmlMsg = htmlMsg + "<td align=right width=14%>" + nlapiEscapeXML(openingBalance) + "</td>";
						htmlMsg = htmlMsg + "</tr>";
						
						//CSV
						//lineItemDataArray[csvCount] =  blank + "," + openingBalanceDate + "," + description + "," + blank+ "," + blank+ "," + blank  + "," + blank  + "," + blank+ "," + blank + "," + blank + "," + blank + "," + openingBalance+ "," + blank ;//+ "," + blank + "," + fxopeningBalance+ "," + blank;
						lineItemDataArray[csvCount] =  blank + "," + openingBalanceDate + "," + description + "," + blank+ "," + blank+ "," + blank  + "," + blank  + ","+ blank + "," + blank + "," + blank + "," + openingBalance+ "," + blank ;//+ "," + blank + "," + fxopeningBalance+ "," + blank;
						csvCount = csvCount + 1;
						
						var balance = 0;
						var lastInternalID = 0;
						var totalAmount = 0;
						totalAmount = parseFloat(totalAmount);
						var debitTotal = 0;
						debitTotal = parseFloat(debitTotal);
						var creditTotal = 0;
						creditTotal = parseFloat(creditTotal);
						//var fxtotalAmount = 0;
						//fxtotalAmount = parseFloat(fxtotalAmount);
						//var fxdebitTotal = 0;
						//fxdebitTotal = parseFloat(fxdebitTotal);
						//var fxcreditTotal = 0;
						//fxcreditTotal = parseFloat(fxcreditTotal);
					
						var recordCount = 0;
					
						var monthlydebitTotal = 0;
						monthlydebitTotal = parseFloat(monthlydebitTotal);
						var monthlycreditTotal = 0;
						monthlycreditTotal = parseFloat(monthlycreditTotal);
						//var fxmonthlydebitTotal = 0;
						//fxmonthlydebitTotal = parseFloat(fxmonthlydebitTotal);
						//var fxmonthlycreditTotal = 0;
					//	fxmonthlycreditTotal = parseFloat(fxmonthlycreditTotal);
				
						var filters = new Array();
						filters.push(new nlobjSearchFilter('internalidnumber', null, 'greaterthan', lastInternalID));
						filters.push(new nlobjSearchFilter('name', null, 'is', vendor));
						filters.push(new nlobjSearchFilter('trandate', null, 'within', startdate, enddate));
						
					/*
	if (locationname != '' && locationname != null) 
						{
							filters.push(new nlobjSearchFilter('location', null, 'is', locationname));
						}
						
*/
						if(parseInt(currency_criteria) == parseInt(1)) 
						{
							filters.push(new nlobjSearchFilter('currency', null, 'is', currency_id));
						}
						var context = nlapiGetContext();
						
						var i_subcontext = context.getFeature('SUBSIDIARIES');
						
						if (i_subcontext != false) {
							filters.push(new nlobjSearchFilter('subsidiary', null, 'is', subsidiary));
						}
						var searchResults = nlapiSearchRecord('transaction', 'customsearch_vendor_statement_new', filters, null);
						
						var breakFlag = 0;
						if (searchResults != null && searchResults != '') {
							var length = searchResults.length;
							nlapiLogExecution('DEBUG', 'ScheduleTest', 'search result length = ' + length);
							
							for (var counter = 0; counter < searchResults.length; counter++) {
								recordCount = recordCount + 1;
								
								var result = searchResults[counter];
								var columns = result.getAllColumns();
								var columnLen = columns.length;
								
								var internalID = '';
								var amount = '';
								var type = '';
								var date = '';
								var number = '';
								var typenumber = '';
								var memo = '';
								var previousInternalID = '';
								var exchangerate = '';
								var currency = '';
								var location = '';
								var account = '';
								var createddate='';
								
								for (var i = 0; i < columnLen; i++) 
								{
									var column = columns[i];
									var fieldName = column.getName();
									var value = result.getValue(column);
									
									if (fieldName == 'internalid') 
									{
										previousInternalID = lastInternalID;
										nlapiLogExecution('DEBUG', 'getOpeningBalance', 'previousInternalID = ' + previousInternalID);
										internalID = value;
										nlapiLogExecution('DEBUG', 'ScheduleTest Internal ID', 'internalID = ' + internalID);
									}
									if (fieldName == 'tranid') 
									{
										number = value;
										//number = replaceCommaWithSemiColon(number);
										nlapiLogExecution('DEBUG', 'ScheduleTest Number', 'number = ' + number);
										//typenumber = type + ' ' + number;
										//typenumber = replaceCommaWithSemiColon(typenumber);
										//nlapiLogExecution('DEBUG', 'ScheduleTest TypeNumber', 'typenumber = ' + typenumber);
									}
									
									if (fieldName == 'trandate') 
									{
										date = value;
										nlapiLogExecution('DEBUG', 'ScheduleTest Date', 'date = ' + date);
									}
									if (fieldName == 'type') 
									{
										type = value;
										nlapiLogExecution('DEBUG', 'ScheduleTest Type', 'type = ' + type);
									}
									if (fieldName == 'currency') 
									{
										currency = result.getText(column);
										currency = nlapiEscapeXML(currency);
										nlapiLogExecution('DEBUG', 'ScheduleTest currency', 'currency = ' + currency);
									} // END if(fieldName == 'currency')
									if (fieldName == 'exchangerate') 
									{
										exchangerate = value;
										exchangerate = nlapiEscapeXML(exchangerate);
										nlapiLogExecution('DEBUG', 'ScheduleTest exchangerate', 'exchangerate = ' + exchangerate);
									} // END if(fieldName == 'exchangerate')
									if (fieldName == 'memomain') {
										memo = value;
										memo = replaceCommaWithSemiColon(memo);
										nlapiLogExecution('DEBUG', 'ScheduleTest Memo', 'memo = ' + memo);
									}
									if (fieldName == 'location') 
									{
										location = result.getText(column);
										location = nlapiEscapeXML(location);
										nlapiLogExecution('DEBUG', 'ScheduleTest Memo', 'location = ' + location);
									} // END if(fieldName == 'location')
									if (fieldName == 'account') 
									{
										account = result.getText(column);
										account = nlapiEscapeXML(account);
										nlapiLogExecution('DEBUG', 'ScheduleTest Number', 'account = ' + account);
										
									} // END if(fieldName == 'account')
									if (fieldName == 'amount') 
									{
										amount = value;
										amount = parseFloat(amount);
										amount = Math.round(amount * 100) / 100;
										nlapiLogExecution('DEBUG', 'ScheduleTest Amount', 'amount = ' + amount);
									}
									/*
if (fieldName == 'fxamount') 
									{
										fxamount = value;
										fxamount = parseFloat(fxamount);
										fxamount = Math.round(fxamount * 100) / 100;
										nlapiLogExecution('DEBUG', 'ScheduleTest Amount', 'fxamount = ' + fxamount);
									} // END if(fieldName == 'amount'
*/
									if (fieldName == 'datecreated') 
									{
									createddate = value
									createddate = nlapiEscapeXML(createddate);
									nlapiLogExecution('DEBUG', 'ScheduleTest Memo', 'createddate = ' + createddate);
									} // END if(fieldName == 'datecreated')
									if (counter >= 900) {
										if (previousInternalID != internalID) {
											breakFlag = 1;
										}
									}
								}
								if (breakFlag == 1) {
									break;
								}
								var o_date_obj = nlapiStringToDate(date)
							var month = o_date_obj.getMonth();
							if(counter == 0)
							{
								var o_date_obj = nlapiStringToDate(date)
								var new_month = o_date_obj.getMonth();
								
							}
							if(month == new_month)
							{
								
							}
							else
							{
								var currentBalance = parseFloat(openingBalance) + parseFloat(totalAmount);
								currentBalance = Math.round(currentBalance * 100) / 100;
								nlapiLogExecution('DEBUG', 'ScheduleTest', 'currentBalance = ' + currentBalance);
								
								//var fxcurrentBalance = parseFloat(fxopeningBalance) + parseFloat(fxtotalAmount);
							    //fxcurrentBalance = Math.round(fxcurrentBalance * 100) / 100;
								//nlapiLogExecution('DEBUG', 'ScheduleTest', 'currentBalance = ' + fxcurrentBalance);
								
								//===============================Begin Print the monthly closing Balance============================//
								
								var o_date_obj = nlapiStringToDate(date)
								var new_month = o_date_obj.getMonth();
								var blank = '-';
								var endDescription = 'Month Closing Balance';
								/*
								htmlMsg = htmlMsg + "<tr>";
								htmlMsg = htmlMsg + "<td style=\"border-right-width:0.1px; border-bottom-width:0.1px\" width=\"6%\" align=\"left\" colspan=\"1\" class=\"textGoodThin\">" + nlapiEscapeXML(blank) + "</td>";
								htmlMsg = htmlMsg + "<td style=\"border-right-width:0.1px; border-bottom-width:0.1px\" width=\"6%\" align=\"left\" colspan=\"1\" class=\"textGoodThin\">" + nlapiEscapeXML(blank) + "</td>";
								htmlMsg = htmlMsg + "<td style=\"border-right-width:0.1px; border-bottom-width:0.1px\" width=\"21%\" align=\"justified\"  colspan=\"4\" class=\"textGoodBold\">" + nlapiEscapeXML(endDescription) + "</td>";
								htmlMsg = htmlMsg + "<td style=\"border-right-width:0.1px; border-bottom-width:0.1px\" width=\"7%\" align=\"center\" colspan=\"1\" class=\"textGoodThin\">" + nlapiEscapeXML(blank) + "</td>";
								htmlMsg = htmlMsg + "<td style=\"border-right-width:0.1px; border-bottom-width:0.1px\" width=\"7%\" align=\"center\" colspan=\"1\" class=\"textGoodThin\">" + nlapiEscapeXML(blank) + "</td>";
								htmlMsg = htmlMsg + "<td style=\"border-right-width:0.1px; border-bottom-width:0.1px\" width=\"7%\" align=\"center\" colspan=\"1\" class=\"textGoodThin\">" + nlapiEscapeXML(blank) + "</td>";
								htmlMsg = htmlMsg + "<td style=\"border-right-width:0.1px; border-bottom-width:0.1px\" width=\"7%\" align=\"center\" colspan=\"1\" class=\"textGoodBold\">" + nlapiEscapeXML(-(monthlydebitTotal)) + "</td>";
								htmlMsg = htmlMsg + "<td style=\"border-right-width:0.1px; border-bottom-width:0.1px\" width=\"7%\" align=\"center\" colspan=\"1\" class=\"textGoodBold\">" + nlapiEscapeXML(monthlycreditTotal) + "</td>";
								htmlMsg = htmlMsg + "<td style=\"border-right-width:0.1px; border-bottom-width:0.1px\" width=\"9%\" align=\"right\" colspan=\"1\" class=\"textGoodBold\">" + nlapiEscapeXML(currentBalance) + "</td>";
								htmlMsg = htmlMsg + "<td style=\"border-right-width:0.1px; border-bottom-width:0.1px\" width=\"7%\" align=\"center\" colspan=\"1\" class=\"textGoodBold\">" + nlapiEscapeXML(fxmonthlydebitTotal) + "</td>";
								htmlMsg = htmlMsg + "<td style=\"border-right-width:0.1px; border-bottom-width:0.1px\" width=\"7%\" align=\"center\" colspan=\"1\" class=\"textGoodBold\">" + nlapiEscapeXML(fxmonthlycreditTotal) + "</td>";
								htmlMsg = htmlMsg + "<td style=\"border-bottom-width:0.1px\" width=\"9%\" align=\"right\" colspan=\"1\" class=\"textGoodBold\">" + nlapiEscapeXML(fxcurrentBalance) + "</td>";
								htmlMsg = htmlMsg + "</tr>"
*/
								lineItemDataArray[csvCount] =  blank + "," +blank + "," + endDescription + "," +blank+ "," +blank+ "," +blank+ ","+ blank  + "," + blank + "," + (-(monthlydebitTotal)) + "," + monthlycreditTotal + "," + currentBalance+ "," + blank; //(-(fxmonthlydebitTotal)) + "," + fxmonthlycreditTotal + "," + fxcurrentBalance+ "," + blank;
								csvCount = csvCount + 1;
								//=============================End Print the monthly closing Balance ==============================//
								
								//Initializing the zero for monthly debit and credit total after print
								
								monthlydebitTotal = 0;
								monthlycreditTotal = 0;
								//fxmonthlydebitTotal = 0;
								//fxmonthlycreditTotal = 0;
							}
								totalAmount = parseFloat(totalAmount) - parseFloat(amount);
								totalAmount = Math.round(totalAmount * 100) / 100;
								nlapiLogExecution('DEBUG', 'ScheduleTest', 'current totalAmount = ' + totalAmount);
								
								
								//fxtotalAmount = parseFloat(fxtotalAmount) - parseFloat(fxamount);
								//fxtotalAmount = Math.round(fxtotalAmount * 100) / 100;
								//nlapiLogExecution('DEBUG', 'ScheduleTest', 'current fxtotalAmount = ' + fxtotalAmount);
							
								
								var blank = '-';
								//htmlMsg = htmlMsg + "<tr>";
								//htmlMsg = htmlMsg + "<td align=left width=10%>" + date + "</td>";
								//htmlMsg = htmlMsg + "<td align=justified width=15%>" + type + "</td>";
								//CSV
								//lineItemDataArray[csvCount] = date + "," + type;
								if (number == '' || number == null || number == undefined) 
								{
									htmlMsg = htmlMsg + "<td align=center width=15%>" + nlapiEscapeXML(blank) + "</td>";
									//CSV
									lineItemDataArray[csvCount] = blank;
								}
								else 
								{
									htmlMsg = htmlMsg + "<td align=justified width=15%>" + nlapiEscapeXML(number) + "</td>";
									//CSV
									lineItemDataArray[csvCount] = number;
								}
								htmlMsg = htmlMsg + "<td align=justified width=15%>" + nlapiEscapeXML(date) + "</td>";
									//CSV
								lineItemDataArray[csvCount] = lineItemDataArray[csvCount] + "," + date;
								
								htmlMsg = htmlMsg + "<td align=justified width=15%>" + nlapiEscapeXML(type) + "</td>";
									//CSV
								lineItemDataArray[csvCount] = lineItemDataArray[csvCount] + "," + type;
								if (currency == '' || currency == null) 
								{
									htmlMsg = htmlMsg + "<td align=center width=20%>" + nlapiEscapeXML(blank) + "</td>";
									//CSV
									lineItemDataArray[csvCount] = lineItemDataArray[csvCount] + "," + blank;
								}
								else 
								{
									htmlMsg = htmlMsg + "<td align=justified width=20%>" + nlapiEscapeXML(currency) + "</td>";
									//CSV
									lineItemDataArray[csvCount] = lineItemDataArray[csvCount] + "," + currency;
								}
								if (exchangerate == '' || exchangerate == null) 
								{
									htmlMsg = htmlMsg + "<td align=center width=20%>" + nlapiEscapeXML(blank) + "</td>";
									//CSV
									lineItemDataArray[csvCount] = lineItemDataArray[csvCount] + "," + blank;
								}
								else 
								{
									htmlMsg = htmlMsg + "<td align=justified width=20%>" + nlapiEscapeXML(exchangerate) + "</td>";
									//CSV
									lineItemDataArray[csvCount] = lineItemDataArray[csvCount] + "," + exchangerate;
								}
								if (number == '' || number == null) 
								{
									htmlMsg = htmlMsg + "<td align=center width=15%>" + nlapiEscapeXML(blank) + "</td>";
									//CSV
									lineItemDataArray[csvCount] = lineItemDataArray[csvCount] + "," + blank;
								}
								else 
								{
									htmlMsg = htmlMsg + "<td align=justified width=15%>" + nlapiEscapeXML(number) + "</td>";
									//CSV
									lineItemDataArray[csvCount] = lineItemDataArray[csvCount] + "," + number;
								}
								if (memo == '' || memo == null) 
								{
									htmlMsg = htmlMsg + "<td align=center width=20%>" + nlapiEscapeXML(blank) + "</td>";
									//CSV
									lineItemDataArray[csvCount] = lineItemDataArray[csvCount] + "," + blank;
								}
								else 
								{
									htmlMsg = htmlMsg + "<td align=justified width=20%>" + nlapiEscapeXML(memo) + "</td>";
									//CSV
									lineItemDataArray[csvCount] = lineItemDataArray[csvCount] + "," + memo;
								}
								/*
if (location == '' || location == null) 
								{
									htmlMsg = htmlMsg + "<td align=center width=20%>" + nlapiEscapeXML(blank) + "</td>";
									//CSV
									lineItemDataArray[csvCount] = lineItemDataArray[csvCount] + "," + blank;
								}
								else 
								{
									htmlMsg = htmlMsg + "<td align=justified width=20%>" + nlapiEscapeXML(location) + "</td>";
									//CSV
									lineItemDataArray[csvCount] = lineItemDataArray[csvCount] + "," + location;
								}
*/
								if (account == '' || account == null) 
								{
									htmlMsg = htmlMsg + "<td align=center width=20%>" + nlapiEscapeXML(blank) + "</td>";
									//CSV
									lineItemDataArray[csvCount] = lineItemDataArray[csvCount] + "," + blank;
								}
								else 
								{
									htmlMsg = htmlMsg + "<td align=justified width=20%>" + nlapiEscapeXML(account) + "</td>";
									//CSV
									lineItemDataArray[csvCount] = lineItemDataArray[csvCount] + "," + account;
								}
								//if (type == 'VendBill') 
								if((type == 'VendPymt' || type == 'VendCred')|| (type == 'Journal' && parseFloat(amount) <= parseFloat(0) ))
								{
									htmlMsg = htmlMsg + "<td align=right width=13%>" + nlapiEscapeXML(amount) + "</td>";
									htmlMsg = htmlMsg + "<td align=center width=13%>" + nlapiEscapeXML(blank) + "</td>";
									//CSV
									lineItemDataArray[csvCount] = lineItemDataArray[csvCount] + "," + nlapiEscapeXML(-(amount)) + "," + blank;
									debitTotal = parseFloat(debitTotal) - parseFloat(amount);
									debitTotal = Math.round(debitTotal * 100) / 100;
									
									//====================Begin Code to get the montly debit total==============================
									if(counter == 0)
								{
								var o_date_obj = nlapiStringToDate(date)
								var debit_new_month = o_date_obj.getMonth();
								
								}
								if (month == debit_new_month) 
								{
									monthlydebitTotal = parseFloat(monthlydebitTotal) + parseFloat(amount);
									monthlydebitTotal = Math.round(monthlydebitTotal * 100) / 100;
								}
								else 
								{
									var o_date_obj = nlapiStringToDate(date)
									var debit_new_month = o_date_obj.getMonth();
									monthlydebitTotal = parseFloat(amount);
									
								}
								//====================End Code to get the montly debit total ==============================//
								
								}
								//if (type == 'VendPymt' || type == 'Journal' || type == 'VendCred') 
								if((type == 'VendBill')|| (type == 'Journal' && parseFloat(amount) > parseFloat(0)))
								{
									htmlMsg = htmlMsg + "<td align=center width=13%>" + blank + "</td>";
									htmlMsg = htmlMsg + "<td align=right width=13%>" + nlapiEscapeXML(amount) + "</td>";
									//CSV
									lineItemDataArray[csvCount] = lineItemDataArray[csvCount] + "," + nlapiEscapeXML(blank) + "," + (amount);
									creditTotal = parseFloat(creditTotal) + parseFloat(amount);
									creditTotal = Math.round(creditTotal * 100) / 100;
									
									//====================Begin Code to get the montly credit total==============================//
								if(counter == 0)
								{
								var o_date_obj = nlapiStringToDate(date)
								var credit_new_month = o_date_obj.getMonth();
								
								}
								if (month == credit_new_month) 
								{
									monthlycreditTotal = parseFloat(monthlycreditTotal) + parseFloat(amount);
									monthlycreditTotal = Math.round(monthlycreditTotal * 100) / 100;
								}
								else 
								{
									var o_date_obj = nlapiStringToDate(date)
									var credit_new_month = o_date_obj.getMonth();
									monthlycreditTotal = parseFloat(amount);
									
								}
								//====================End Code to get the montly credit total==============================\\
								
								}
								
								if (counter == 0 && lastInternalID == 0) {
									nlapiLogExecution('DEBUG', 'ScheduleTest', 'first time');
									nlapiLogExecution('DEBUG', 'ScheduleTest', 'openingBalance = ' + openingBalance);
									balance = parseFloat(openingBalance) - parseFloat(amount);
									balance = Math.round(balance * 100) / 100;
									nlapiLogExecution('DEBUG', 'ScheduleTest', 'balance = ' + balance);
									
									//fxbalance = parseFloat(fxopeningBalance) - parseFloat(fxamount);
									//fxbalance = Math.round(fxbalance * 100) / 100;
									//nlapiLogExecution('DEBUG', 'ScheduleTest', 'fxbalance = ' + fxbalance);
								}
								else 
								{
									nlapiLogExecution('DEBUG', 'ScheduleTest', 'not first time');
									balance = parseFloat(balance) - parseFloat(amount);
									balance = Math.round(balance * 100) / 100;
									nlapiLogExecution('DEBUG', 'ScheduleTest', 'balance = ' + balance);
									
									//fxbalance = parseFloat(fxbalance) - parseFloat(fxamount);
								//	fxbalance = Math.round(fxbalance * 100) / 100;
									//nlapiLogExecution('DEBUG', 'ScheduleTest', 'fxbalance = ' + fxbalance);
								}
								htmlMsg = htmlMsg + "<td align=right width=14%>" + nlapiEscapeXML(balance) + "</td>";
								//CSV
								lineItemDataArray[csvCount] = lineItemDataArray[csvCount] + "," + balance;
								/*
if((type == 'VendPymt' || type == 'VendCred')|| (type == 'Journal' && parseFloat(amount) <= parseFloat(0) ))
								{
									htmlMsg = htmlMsg + "<td align=right width=13%>" + nlapiEscapeXML(fxamount) + "</td>";
									htmlMsg = htmlMsg + "<td align=center width=13%>" + nlapiEscapeXML(blank) + "</td>";
									//CSV
									lineItemDataArray[csvCount] = lineItemDataArray[csvCount] + "," + nlapiEscapeXML(-(fxamount)) + "," + blank;
									fxdebitTotal = parseFloat(fxdebitTotal) - parseFloat(fxamount);
									fxdebitTotal = Math.round(fxdebitTotal * 100) / 100;
									
									//====================Begin Code to get the montly fx debit total==============================\\
								if(counter == 0)
								{
								var o_date_obj = nlapiStringToDate(date)
								var fxdebit_new_month = o_date_obj.getMonth();
								
								}
								if (month == fxdebit_new_month) 
								{
									fxmonthlydebitTotal = parseFloat(fxmonthlydebitTotal) + parseFloat(amount);
									fxmonthlydebitTotal = Math.round(fxmonthlydebitTotal * 100) / 100;
								}
								else 
								{
									var o_date_obj = nlapiStringToDate(date)
									var fxdebit_new_month = o_date_obj.getMonth();
									fxmonthlydebitTotal = parseFloat(amount);
									
								}
								//====================End Code to get the montly fx debit total==============================//
							
								}
								//if (type == 'VendPymt' || type == 'Journal' || type == 'VendCred') 
								if((type == 'VendBill')|| (type == 'Journal' && parseFloat(amount) > parseFloat(0)))
								{
									htmlMsg = htmlMsg + "<td align=center width=13%>" + blank + "</td>";
									htmlMsg = htmlMsg + "<td align=right width=13%>" + nlapiEscapeXML(fxamount) + "</td>";
									//CSV
									lineItemDataArray[csvCount] = lineItemDataArray[csvCount] + "," + nlapiEscapeXML(blank) + "," + (fxamount);
									fxcreditTotal = parseFloat(fxcreditTotal) + parseFloat(fxamount);
									fxcreditTotal = Math.round(fxcreditTotal * 100) / 100;
									
									//====================Begin Code to get the montly fx credit total==============================//
								if(counter == 0)
								{
								var o_date_obj = nlapiStringToDate(date)
								var fxcredit_new_month = o_date_obj.getMonth();
								
								}
								if (month == fxcredit_new_month) 
								{
									fxmonthlycreditTotal = parseFloat(fxmonthlycreditTotal) + parseFloat(amount);
									fxmonthlycreditTotal = Math.round(fxmonthlycreditTotal * 100) / 100;
								}
								else 
								{
									var o_date_obj = nlapiStringToDate(date)
									var fxcredit_new_month = o_date_obj.getMonth();
									fxmonthlycreditTotal = parseFloat(amount);
									
								}
								}
								htmlMsg = htmlMsg + "<td align=right width=14%>" + nlapiEscapeXML(fxbalance) + "</td>";
								lineItemDataArray[csvCount] = lineItemDataArray[csvCount] + "," + fxbalance;
*/
									lineItemDataArray[csvCount] = lineItemDataArray[csvCount] + "," + createddate;
								csvCount = csvCount + 1;
								htmlMsg = htmlMsg + "</tr>";
								
								lastInternalID = internalID;
								nlapiLogExecution('DEBUG', 'ScheduleTest', 'test lastInternalID = ' + lastInternalID);
							}
						}
						//}while(searchResults != null)
						
						totalAmount = parseFloat(totalAmount);
						totalAmount = Math.round(totalAmount * 100) / 100;
						nlapiLogExecution('DEBUG', 'ScheduleTest', 'totalAmount = ' + totalAmount);
						nlapiLogExecution('DEBUG', 'ScheduleTest', 'recordCount = ' + recordCount);
						
						//fxtotalAmount = parseFloat(fxtotalAmount);
						//fxtotalAmount = Math.round(fxtotalAmount * 100) / 100;
						//nlapiLogExecution('DEBUG', 'ScheduleTest', 'totalAmount = ' + totalAmount);
					
						
						var currentBalance = parseFloat(openingBalance) + parseFloat(totalAmount);
						currentBalance = Math.round(currentBalance * 100) / 100;
						nlapiLogExecution('DEBUG', 'ScheduleTest', 'currentBalance = ' + currentBalance);
						
						//var fxcurrentBalance = parseFloat(fxopeningBalance) + parseFloat(fxtotalAmount);
						//fxcurrentBalance = Math.round(fxcurrentBalance * 100) / 100;
						//nlapiLogExecution('DEBUG', 'ScheduleTest', 'fxcurrentBalance = ' + fxcurrentBalance);
					
						var blank = '-';
						var endDescription = 'Debit Total';
						htmlMsg = htmlMsg + "<tr>";
						htmlMsg = htmlMsg + "<td border-width=\"0.1\" width=\"10%\" align=\"left\" colspan=\"1\" class=\"textGoodThin\">" + nlapiEscapeXML(end) + "</td>";
						htmlMsg = htmlMsg + "<td border-width=\"0.1\" width=\"15%\" align=\"justified\" colspan=\"1\" class=\"textGoodThin\">" + nlapiEscapeXML(endDescription) + "</td>";
						htmlMsg = htmlMsg + "<td border-width=\"0.1\" width=\"15%\" align=\"center\" colspan=\"1\" class=\"textGoodThin\">" + nlapiEscapeXML(blank) + "</td>";
						htmlMsg = htmlMsg + "<td border-width=\"0.1\" width=\"20%\" align=\"center\" colspan=\"1\" class=\"textGoodThin\">" + nlapiEscapeXML(blank) + "</td>";
						htmlMsg = htmlMsg + "<td border-width=\"0.1\" width=\"13%\" align=\"right\" colspan=\"1\" class=\"textGoodThin\">" + nlapiEscapeXML(debitTotal) + "</td>";
						htmlMsg = htmlMsg + "<td border-width=\"0.1\" width=\"13%\" align=\"center\" colspan=\"1\" class=\"textGoodThin\">" + nlapiEscapeXML(blank) + "</td>";
						htmlMsg = htmlMsg + "<td border-width=\"0.1\" width=\"114%\" align=\"center\" colspan=\"1\" class=\"textGoodThin\">" + nlapiEscapeXML(blank) + "</td>";
						//CSV
						lineItemDataArray[csvCount] =  blank + "," +end + "," + endDescription + ","+ blank + "," + blank + "," + blank+ "," + blank + "," + blank + "," + debitTotal + "," + blank + "," + blank + ","+blank; //fxdebitTotal+"," + blank + "," + blank+ "," + blank;
						csvCount = csvCount + 1;
						htmlMsg = htmlMsg + "</tr>";
						
						var blank = '-';
						var endDescription = 'Credit Total';
						htmlMsg = htmlMsg + "<tr>";
						htmlMsg = htmlMsg + "<td border-width=\"0.1\" width=\"10%\" align=\"left\" colspan=\"1\" class=\"textGoodThin\">" + nlapiEscapeXML(end) + "</td>";
						htmlMsg = htmlMsg + "<td border-width=\"0.1\" width=\"15%\" align=\"justified\" colspan=\"1\" class=\"textGoodThin\">" + nlapiEscapeXML(endDescription) + "</td>";
						htmlMsg = htmlMsg + "<td border-width=\"0.1\" width=\"15%\" align=\"center\" colspan=\"1\" class=\"textGoodThin\">" + nlapiEscapeXML(blank) + "</td>";
						htmlMsg = htmlMsg + "<td border-width=\"0.1\" width=\"20%\" align=\"center\" colspan=\"1\" class=\"textGoodThin\">" + nlapiEscapeXML(blank) + "</td>";
						htmlMsg = htmlMsg + "<td border-width=\"0.1\" width=\"13%\" align=\"center\" colspan=\"1\" class=\"textGoodThin\">" + blank + "</td>";
						htmlMsg = htmlMsg + "<td border-width=\"0.1\" width=\"13%\" align=\"right\" colspan=\"1\" class=\"textGoodThin\">" + nlapiEscapeXML((-(creditTotal))) + "</td>";
						htmlMsg = htmlMsg + "<td border-width=\"0.1\" width=\"14%\" align=\"center\" colspan=\"1\" class=\"textGoodThin\">" + blank + "</td>";
						//CSV
						lineItemDataArray[csvCount] = blank + "," +end + "," + endDescription + "," + blank + ","+ blank + "," + blank + "," + blank + "," + blank + "," + blank + "," +creditTotal+ "," + blank+ "," + blank ;//+ "," + fxcreditTotal + "," + blank+ "," + blank;
						csvCount = csvCount + 1;
						htmlMsg = htmlMsg + "</tr>";
						
						var blank = '-';
						var endDescription = 'Closing Balance';
						htmlMsg = htmlMsg + "<tr>";
						htmlMsg = htmlMsg + "<td align=left width=10%>" + end + "</td>";
						htmlMsg = htmlMsg + "<td align=justified width=15%>" + nlapiEscapeXML(endDescription) + "</td>";
						htmlMsg = htmlMsg + "<td align=center width=15%>" + blank + "</td>";
						htmlMsg = htmlMsg + "<td align=center width=20%>" + blank + "</td>";
						htmlMsg = htmlMsg + "<td align=center width=13%>" + blank + "</td>";
						htmlMsg = htmlMsg + "<td align=center width=13%>" + blank + "</td>";
						htmlMsg = htmlMsg + "<td align=right width=14%>" + nlapiEscapeXML(currentBalance) + "</td>";
						
						if (currentBalance < 0) 
						{
							//CSV
							var newBalance = (-(currentBalance));
							//var fxnewbalance = (-(fxcurrentBalance));
							lineItemDataArray[csvCount] = blank + "," +end + "," + endDescription + "," + blank+ ","+ blank+ "," + blank+ "," + blank + "," + blank + "," + newBalance + "," + blank + "," + blank+ "," + blank ; //fxnewbalance + "," + blank + "," + blank+ "," + blank;
							csvCount = csvCount + 1;
						}
						else 
							if (currentBalance > 0) 
							{
								//CSV
								lineItemDataArray[csvCount] = blank + "," +end + "," + endDescription + "," + blank+ "," + blank+ "," + blank+ "," + blank + "," + blank + "," + blank + "," + currentBalance + "," + blank;//+ "," + blank + "," + fxcurrentBalance + "," + blank+ "," + blank;
								csvCount = csvCount + 1;
							}
							else 
							{
								//CSV
								lineItemDataArray[csvCount] = blank + "," +end + "," + endDescription + "," + blank+ ","+ blank+ "," + blank + "," + blank + "," + blank + "," + blank + "," + blank+ "," + currentBalance+ "," + blank ;//+ "," + blank + "," + fxcurrentBalance+ "," + blank;
								csvCount = csvCount + 1;
							}
						
						htmlMsg = htmlMsg + "</tr>";
						
						htmlMsg = htmlMsg + "</table>";
						htmlMsg = htmlMsg + "</CENTER>";
						
						htmlMsg = htmlMsg + "</body>";
						htmlMsg = htmlMsg + "";
						htmlMsg = htmlMsg + "</HTML>";
						//nlapiLogExecution('DEBUG', 'ScheduleTest', 'htmlMsg = ' +htmlMsg);
						//response.write(htmlMsg);
						
						//CSV
						var MultiArray = '';
						for (var icnt = 0; lineItemDataArray != null && icnt < lineItemDataArray.length; icnt++) {
							nlapiLogExecution('DEBUG', 'PrintCSV', 'icnt=' + icnt);
							if (icnt == 0) {
								MultiArray = lineItemDataArray[icnt];
							} // END if(icnt == 0)
							else {
								MultiArray = MultiArray + "\n" + lineItemDataArray[icnt];
							} // END else
						} // END for (var icnt = 0; lineItemDataArray != null && icnt < lineItemDataArray.length; icnt++)
						
					} // result null
				}
			}
			var datetime = GetCurrentTimeStamp();
			nlapiLogExecution('DEBUG', 'UnixTimeStamp', 'UnixTimeStamp = ' + datetime);
			var fileObj = nlapiCreateFile('VendorStatement' + '_' + datetime + '.csv', 'CSV', MultiArray);//strName
			nlapiLogExecution('DEBUG', 'CSV', 'CSV fileObj = ' + fileObj);
					
			var emailBody = 'PFA';
			var user = nlapiGetUser();
			
			if (recepient != null && recepient != '' && recepient != undefined) 
			{
				nlapiSendEmail(user, recepient, 'Vendor Statement', emailBody, null, null, null, fileObj);
			}
			fileObj.setName('Vendor Statement'+datetime+'.csv')
			var folder_id = get_folderID();
			fileObj.setFolder(folder_id);
			fileObj.setIsOnline(true);
			var fileID = nlapiSubmitFile(fileObj);
			venstatobj.setFieldValue('custrecord_ait_vendorstatement',fileID);
			venstatobj.setFieldValue('custrecord_ait_venstat_status','completed');
		
			var venStatID = nlapiSubmitRecord(venstatobj);
			nlapiLogExecution('DEBUG', 'generate statemnet', 'vendor Statement ID->' + venStatID)
					
}


// END SCHEDULED FUNCTION ===============================================





// BEGIN FUNCTION ===================================================
function getOpeningBalance(vendor, locationname, custCreatedDate, opBalanceEndDate,subsidiary)
{
	
	//====== CODE FOR GETTIMNG OPENING BALANCE ======
	var lastInternalID = 0;
	var totalAmount = 0;
	//var totalfxamount = 0;
	totalAmount = parseFloat(totalAmount);
	//totalfxamount = parseFloat(totalfxamount);
		var filters = new Array();
		
		filters.push( new nlobjSearchFilter('internalidnumber', null, 'greaterthan', lastInternalID));
		filters.push( new nlobjSearchFilter('name', null, 'is', vendor));
		
		filters.push( new nlobjSearchFilter('trandate', null, 'within', custCreatedDate, opBalanceEndDate));
		
		if(locationname != '' && locationname != null)
		{
			//filters.push( new nlobjSearchFilter('location', null, 'is', locationname));
		} // END if(locationname != '' && locationname != null)
		
		var context = nlapiGetContext();
	    var i_subcontext = context.getFeature('SUBSIDIARIES');
		if (i_subcontext != false) 
		{
			filters.push(new nlobjSearchFilter('subsidiary', null, 'is', subsidiary));
		}
		
		var searchResults = nlapiSearchRecord('transaction', 'customsearch_vendor_statement_new', filters, null);

		var breakFlag = 0;
		if(searchResults != null && searchResults != '')
		{
			var length = searchResults.length;
			nlapiLogExecution('DEBUG', 'getOpeningBalance', 'search result length = ' +length);

			for (var counter = 0; counter < searchResults.length; counter++)
			{
				nlapiLogExecution('DEBUG', 'getOpeningBalance', 'counter for begin= ' +counter);
				var result = searchResults[counter];
				var columns = result.getAllColumns();
				var columnLen = columns.length;

				var internalID = '';
				var amount = '';
				//var fxamount = '';
				var type = '';
				var previousInternalID = '';

				for (var i = 0; i < columnLen; i++)
				{
					var column = columns[i];
					var fieldName = column.getName();
					var value = result.getValue(column);

					if(fieldName == 'internalid')
					{
						previousInternalID = lastInternalID;
						nlapiLogExecution('DEBUG', 'getOpeningBalance', 'previousInternalID = ' +previousInternalID);
						internalID = value;
						nlapiLogExecution('DEBUG', 'getOpeningBalance', 'internalID = ' +internalID);
					}

					if(fieldName == 'amount')
					{
						amount = value;
						amount = parseFloat(amount);
						amount = Math.round(amount*100)/100;
					}
					
					/*
					if(fieldName == 'fxamount')
					{
						fxamount = value;
						fxamount = parseFloat(fxamount);
						fxamount = Math.round(fxamount*100)/100;
					}

					*/

					if(fieldName == 'type')
					{
						type = value;
					}
					if(counter >= 900)
					{
						if(previousInternalID != internalID)
						{
							breakFlag = 1;
						}
					}
				}
				if(breakFlag == 1)
				{
					break;
				}
				lastInternalID = internalID;

				totalAmount = parseFloat(totalAmount) - parseFloat(amount);
				totalAmount = Math.round(totalAmount*100)/100;
				
				//totalfxamount = parseFloat(totalfxamount) - parseFloat(fxamount);
				//totalfxamount = Math.round(totalfxamount*100)/100;
				
			}// END for (var counter = 0; counter < searchResults.length; counter++)
			
		} // END if(searchResults != null && searchResults != '')
	totalAmount = parseFloat(totalAmount);
	totalAmount = Math.round(totalAmount*100)/100;
	
	var getOpeningBalancefunction = parseFloat(totalAmount);
	
	nlapiLogExecution('DEBUG', 'getOpeningBalance', 'getOpeningBalancefunction = ' +getOpeningBalancefunction);
	
	//totalfxamount = parseFloat(totalfxamount);
	//totalfxamount = Math.round(totalfxamount*100)/100;
	
	//nlapiLogExecution('DEBUG', 'getOpeningBalance', 'getOpeningBalancefunction fx amount = ' +totalfxamount);
	
	//return_amount = totalAmount+'#'+totalfxamount
	
	return totalAmount;
} // END getOpeningBalance(vendor, locationname, custCreatedDate, opBalanceEndDate)
function getLocationName(locationname)
{
	//======= CODE TO GET LOCATION NAME  ==========
	var Filters = new Array();
	var Columns = new Array();
	
	Filters.push( new nlobjSearchFilter('internalid', null, 'is', locationname));
	Columns.push( new nlobjSearchColumn('internalid'));
	Columns.push( new nlobjSearchColumn('name'));

	var SearchResults = nlapiSearchRecord('location', null, Filters, Columns);
	
	if(SearchResults != null)
	{
		var InternalID = SearchResults[0].getValue('internalid');
		var LocName = SearchResults[0].getValue('name');
		nlapiLogExecution('DEBUG', 'getOpeningBalance', 'InternalID = ' +InternalID);
		nlapiLogExecution('DEBUG', 'getOpeningBalance', 'LocName = ' +LocName);
		return LocName;
	}
}
function replaceCommaWithSemiColon(data)
{
	//==== CODE FOR REPLACING COMMA WITH SEMICOLON ======
	if(data != null && data != '')
	{
		var numberOfCommas = 0;
		var lengthData = data.length;
		for(var n = 0; n < lengthData; n++)
		{
			var currentChar = data.charAt(n);
			if(currentChar == ",")
			numberOfCommas = numberOfCommas + 1;
		}
		var trimmedData = data;
	    for(var m = 0; m < numberOfCommas; m++)
	    {
	    	var trim = trimmedData.replace(",",";");
	    	trimmedData = trim;
	    }
	}
    return trimmedData;
} // END replaceCommaWithSemiColon(data)
function afterReplaceAddComma(custAddr)
{
    
	/// ====== CODE TO ADD COMMA AFTER REPLACE ====
	var custAddrString = custAddr.toString();

    custAddrString = custAddrString.replace(/\r/g, '-');/// /g
    custAddrString = custAddrString.replace(/\n/g, '--');/// /g

    return custAddrString;
} // END afterReplaceAddComma(custAddr)
function GetCurrentTimeStamp()
{
	 var set_date = new Date();
	var offset =  (3600000*(+5.50+7));
	var current_date_time =  new Date(set_date.getTime() + offset)
	var o_current_date_time_obj = current_date_time.toString();
	var display_date = o_current_date_time_obj.substring(0, 25)
	nlapiLogExecution('DEBUG', 'Script Scheduled ', ' Script Status 2-->' + new Date(set_date.getTime() + offset));
	return display_date;
	
}
function getcompanyinfo()
{
	var  i_internalid = '';
	var company_filters = new Array();
	var company_column = new Array();
	company_column.push( new nlobjSearchColumn('internalid'));    
	
	var company_results = nlapiSearchRecord('customrecord_taxcompanyinfo', null, company_filters, company_column);
	if (company_results != null && company_results != '' && company_results != undefined) 
	{
		i_internalid = company_results[0].getValue('internalid')
		nlapiLogExecution('DEBUG', 'company info record', 'internalid->' + i_internalid)
	}
	return i_internalid;
}
function get_folderID()
{
			var folder_filters = new Array();
			folder_filters.push(new nlobjSearchFilter('name', null, 'is', 'AIT Vendor Statment'));
	
			var folder_column = new Array();
			folder_column.push( new nlobjSearchColumn('internalid'));    
	
			var folder_results = nlapiSearchRecord('folder', null, folder_filters, folder_column);
			if (folder_results != null && folder_results != '' && folder_results != undefined) 
			{
				var folderId = folder_results[0].getId();
				
			}
			else
			{
				var o_folder_obj = nlapiCreateRecord('folder')
				o_folder_obj.setFieldValue('name','AIT Vendor Statment')
				var folderId = nlapiSubmitRecord(o_folder_obj)
				
			}
			return folderId;
		
	
}
function getFirstTransactionDate(vendor)
{
	var first_trandate = '';
	var date_filters = new Array();
	date_filters.push(new nlobjSearchFilter('internalid', null, 'is', vendor));
	
	var date_column = new Array();
	date_column[0] = new nlobjSearchColumn('trandate','transaction');
	date_column[0].setSort(false);
	var date_results = nlapiSearchRecord('vendor', null, date_filters, date_column);
	if (date_results != null && date_results != '' && date_results != undefined) 
	{
		first_trandate = date_results[0].getValue(date_column[0]);
		nlapiLogExecution('DEBUG', 'company info record', 'first_trandate->' + first_trandate)
		
	}
	return first_trandate;
}
// END FUNCTION =====================================================
