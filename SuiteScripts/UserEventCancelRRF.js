/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       12 Jun 2019     Aazamali Khan
 *
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType
 * 
 * @param {String} type Operation types: create, edit, delete, xedit
 *                      approve, reject, cancel (SO, ER, Time Bill, PO & RMA only)
 *                      pack, ship (IF)
 *                      markcomplete (Call, Task)
 *                      reassign (Case)
 *                      editforecast (Opp, Estimate)
 * @returns {Void}
 */
function cancelRRFOnDelete(type){
	
	if (type == "delete") {
		// get the RRF ids
		var payLoad = {};
	    var headers = {
	        "Content-Type": "application/json",
	        "x-access-token": getTokens()
	    };
		var rrfNumber = nlapiGetFieldValue("custrecord_taleo_external_rrf_number");
		var url = "https://netsuite-taleo-transformer.azurewebsites.net/cancelRRF"
		payLoad.contestNumber = frfNumber;
		payLoad.comment = "Cancelled";
		var JSONBody = JSON.stringify(payLoad, replacer);
		var responseObj = nlapiRequestURL(url, JSONBody, headers);
	}
}
