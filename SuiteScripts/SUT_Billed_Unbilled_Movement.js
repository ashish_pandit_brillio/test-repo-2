/**
 * Billed To Unbilled Resource Movement
 * 
 * 1.00 05 Oct 2015 Nitish Mishra Created the file
 * 
 */

/**
 * Form to change TA / EA for resource allocated in a project
 * 
 * Version Date Author Remarks 1.00 29 Sep 2015 nitish.mishra
 * 
 */

function suitelet(request, response) {

	try {
		var method = request.getMethod();
		nlapiLogExecution('debug', 'method', method);

		if (method == "GET") {
			renderProjectSelectionPage();
		} else {
			method = request.getParameter('custpage_next_step');
			nlapiLogExecution('debug', 'method', method);

			if (method == "DisplayResourceDetails") {
				renderResourceDetailsPage(request);
			} else if (method == "DisplayResourceDetails") {
				renderResultPage(request);
			} else {
				renderProjectSelectionPage();
			}
		}

	} catch (err) {
		nlapiLogExecution('ERROR', 'suitelet', err);
	}
}

function renderProjectSelectionPage() {
	try {
		var getForm = nlapiCreateForm('Unbilled - Billed Allocation Movement',
		        false);
		getForm.addFieldGroup('custpage_1', 'Select a project');
		getForm.addField('custpage_project', 'select', 'Project', 'job',
		        'custpage_1').setMandatory(true);

		getForm.addFieldGroup('custpage_2', 'Select the type of movement');
		var movement = getForm.addField('custpage_movement', 'select',
		        'Movement', null, 'custpage_2').setMandatory(true);
		movement.addSelectOption('', '', true);
		movement.addSelectOption('1', 'Unbilled To Billed');
		movement.addSelectOption('2', 'Billed To Unbilled');

		getForm.addField('custpage_next_step', 'text', 'Next Step')
		        .setDisplayType('hidden').setDefaultValue(
		                'DisplayResourceDetails');

		getForm.addSubmitButton('Get Project Resources');
		response.writePage(getForm);
	} catch (err) {
		nlapiLogExecution('ERROR', 'renderProjectSelectionPage', err);
		throw err;
	}
}

function renderResourceDetailsPage(request) {
	try {
		var projectId = request.getParameter('custpage_project');
		var movementId = request.getParameter('custpage_movement');
		nlapiLogExecution('debug', 'projectId', projectId);
		nlapiLogExecution('debug', 'movementId', movementId);

		if (projectId && movementId) {
			var getForm = nlapiCreateForm(
			        'Unbilled - Billed Allocation Movement', false);
			var movementText = "";
			var billedFilter = "F";

			if (movementId == 1) {
				movementText = "Unbilled To Billed";
			} else if (movementId == 2) {
				movementText = "Billed To Unbilled";
				billedFilter = "T";
			} else {
				throw "Invalid Movement";
			}

			// Project
			getForm.addFieldGroup('custpage_1', 'Selected Details');
			getForm.addField('custpage_project', 'select', 'Project', 'job',
			        'custpage_1').setDisplayType('inline').setDefaultValue(
			        projectId);
			getForm.addField('custpage_movement', 'text', 'Movement', null,
			        'custpage_1').setDisplayType('inline').setDefaultValue(
			        movementText);

			/*
			 * var projectDetails = nlapiLookupField('job', projectId, [
			 * 'custentity_projectmanager', 'custentity_deliverymanager',
			 * 'custentity_clientpartner' ]); // Project Manager
			 * getForm.addFieldGroup('custpage_2', 'Project Manager');
			 * getForm.addField('custpage_pm_old', 'select', 'Old', 'employee',
			 * 'custpage_2').setDisplayType('inline').setDefaultValue(
			 * projectDetails.custentity_projectmanager);
			 * getForm.addField('custpage_pm_new', 'select', 'New', 'employee',
			 * 'custpage_2'); // Delivery Manager
			 * getForm.addFieldGroup('custpage_3', 'Delivery Manager');
			 * getForm.addField('custpage_dm_old', 'select', 'Old', 'employee',
			 * 'custpage_3').setDisplayType('inline').setDefaultValue(
			 * projectDetails.custentity_deliverymanager);
			 * getForm.addField('custpage_dm_new', 'select', 'New', 'employee',
			 * 'custpage_3'); // Client Partner
			 * getForm.addFieldGroup('custpage_4', 'Client Partner');
			 * getForm.addField('custpage_cp_old', 'select', 'Old', 'employee',
			 * 'custpage_4').setDisplayType('inline').setDefaultValue(
			 * projectDetails.custentity_clientpartner);
			 * getForm.addField('custpage_cp_new', 'select', 'New', 'employee',
			 * 'custpage_4');
			 */

			// get all allocated resources
			var allocationSearch = nlapiSearchRecord('resourceallocation',
			        null, [
			                new nlobjSearchFilter('project', null, 'anyof',
			                        projectId),
			                new nlobjSearchFilter('enddate', null, 'notbefore',
			                        'today'),
			                new nlobjSearchFilter(
			                        'custentity_implementationteam',
			                        'employee', 'is', 'F'),
			                new nlobjSearchFilter('custeventrbillable', null,
			                        'is', billedFilter),
			                new nlobjSearchFilter(
			                        'custentity_employee_inactive', 'employee',
			                        'is', 'F') ], [
			                new nlobjSearchColumn('resource'),
			                new nlobjSearchColumn('startdate'),
			                new nlobjSearchColumn('enddate'),
			                new nlobjSearchColumn('custevent3'),
			                new nlobjSearchColumn('custeventrbillable') ]);

			var allocationData = [];

			if (allocationSearch) {
				allocationSearch.forEach(function(allocation) {
					allocationData.push({
					    employee : allocation.getValue('resource'),
					    startdate : allocation.getValue('startdate'),
					    enddate : allocation.getValue('enddate'),
					    billrate : allocation.getValue('custevent3'),
					    billable : allocation.getValue('custeventrbillable')
					});
				});
			}

			// add resource sublist
			var subList = getForm.addSubList('custpage_resource', 'list',
			        'Resources');
			subList.addField('employee', 'select', 'Employee', 'employee')
			        .setDisplayType('inline');
			subList.addField('startdate', 'date', 'Start Date').setDisplayType(
			        'inline');
			subList.addField('enddate', 'date', 'End Date').setDisplayType(
			        'inline');
			subList.addField('billrate', 'currency', 'Bill Rate')
			        .setDisplayType('inline');
			subList.addField('billable', 'checkbox', 'Billable?')
			        .setDisplayType('inline');
			subList.addField('p_enddate', 'date',
			        'Previous Allocation End Date').setDisplayType('inline');
			subList.addField('n_project', 'select', 'Project', 'job')
			        .setDisplayType('entry');
			subList.addField('n_startdate', 'date', 'Start Date')
			        .setDisplayType('entry');
			subList.addField('n_enddate', 'date', 'End Date').setDisplayType(
			        'entry');
			subList.addField('n_billable', 'checkbox', 'Billable?')
			        .setDisplayType('entry');
			subList.addField('n_billrate', 'currency', 'Bill Rate')
			        .setDisplayType('entry');
			subList.addField('n_otrate', 'currency', 'OT Rate').setDisplayType(
			        'entry');
			subList.addField('n_percent', 'percent', '% Allocation')
			        .setDisplayType('entry');
			subList.addField('n_site', 'select', 'Site',
			        'customlistonsiteoffsitelist').setDisplayType('entry');

			subList.setLineItemValues(allocationData);

			// add buttons and hidden fields
			getForm.addField('custpage_next_step', 'text', 'Next Step')
			        .setDisplayType('hidden').setDefaultValue(
			                'SubmitAllocationDetails');

			getForm.addSubmitButton('Submit Allocation');
			response.writePage(getForm);
		} else {
			renderProjectSelectionPage();
		}
	} catch (err) {
		nlapiLogExecution('ERROR', 'renderResourceDetailsPage', err);
		throw err;
	}
}

function renderResultPage(request) {
	try {
		// add buttons and hidden fields
		var getForm = nlapiCreateForm('Unbilled - Billed Allocation Movement',
		        false);
		getForm.addField('custpage_next_step', 'text', 'Next Step')
		        .setDisplayType('hidden').setDefaultValue('GoBack');
		getForm.addSubmitButton('Finish');
		response.writePage(getForm);
	} catch (err) {
		nlapiLogExecution('ERROR', 'renderResultPage', err);
		throw err;
	}
}