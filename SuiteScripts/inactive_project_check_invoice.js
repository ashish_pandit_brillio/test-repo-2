/**
  *@NApiVersion 2.0
  *@NScriptType UserEventScript
  *@NModuleScope SameAccount
  */

define(['N/ui/dialog','N/runtime','N/record','N/log','N/search'],
 function(dialog,runtime,record,log,search) {

function projectInactiveCheck(scriptContext) {
var cRecord =  scriptContext.newRecord
var action_type = scriptContext.type;

if(action_type == 'create' || action_type=='edit'){
  
var projectID = cRecord.getValue({fieldId: "job"});
log.debug("projectID",projectID);
//var projectText = cRecord.getText({fieldId: "project"});



var projectLookup = search.lookupFields({
 type: 'job',
 id: projectID,
 columns: ['isinactive']
});
  
log.debug("projectLookup",projectLookup);
log.debug("projectLookup.isinactive",projectLookup.isinactive);
  

var htmlText = '';
var tableData ='';
htmlText +="<h1> Can't create invoice for the below inactive project</h1>"
htmlText +="<br></br>"
tableData += "<table><tr><td> Project ID </td> <td> </td> <td> </td> <td>  Project Status </td></tr>"
tableData += "<tr>";
tableData += "<td>" + projectID +' ' + "</td>";
tableData += "<td>" + ' '+"</td>";
tableData += "<td>" + ' '+"</td>";
tableData += "<td>" + "Inactive" +' ' +"</td>";
tableData += "</tr>";

tableData += "</table>";

htmlText+=tableData

if(projectLookup.isinactive == true){
  throw htmlText;
  return false;

}


}
return true







}
  

  


return {

 beforeSubmit: projectInactiveCheck
};

});