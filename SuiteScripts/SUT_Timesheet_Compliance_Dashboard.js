//BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
    /*
     Script Name:SUT_RPT_NOT_SUBMITTED_FOR_MANAGERS.js
     Author: Sitaram Upadhya
     Company:Brillio Technologies Private Limited
     Date: 20-05-2021
     Description:
     Script Modification Log:
     -- Date --			-- Modified By --				--Requested By--				-- Description --
     20-05-2021
     Below is a summary of the process controls enforced by this script file.  The control logic is described
     more fully, below, in the appropriate function headers and code blocks.
     SUITELET
     - suiteletFunction(request, response )
     SUB-FUNCTIONS
     - The following sub-functions are called by the above core functions in order to maintain code
     modularization:
     - NOT USED
     */
}



function SUT_RPT_NOT_SUBMITTED_FOR_MANAGERS(request, response) 
{
    try 
    {
        
        if (request.getMethod() == 'GET')
        {


            var form = nlapiCreateForm('Time Sheet Compliance Report ');
            form.setScript('customscriptcli_notsubmitted')

            var contextObj = nlapiGetContext();

            var fromdate = form.addField('custpage_fromdate', 'Date', 'Period End Date :');

             form.addSubmitButton('Submit');
             response.writePage(form);

        }
        else // If method of request is Post
        {
            var date = request.getParameter('custpage_fromdate');
            var form = nlapiCreateForm('We are sending your Timesheet compliance report into your mailbox shortly.');
            response.writePage(form);

            var contextObj = nlapiGetContext();
            var currentuser = contextObj.getUser()
            nlapiLogExecution('DEBUG', 'currentuser',currentuser);

            var params = new Array();
            params['custscript_end_date_ts'] = date;
            params['custscript_user_email'] = currentuser;

            nlapiScheduleScript('customscript_ts_not_submitted','customdeploy_ts_not_submitted',params)
        }
    }
    catch (e)
    {
        nlapiLogExecution('ERROR', 'Try Catch Error ', 'Exception = ' + e)
    }
}

