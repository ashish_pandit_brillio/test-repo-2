/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       24 Dec 2018     Aazamali Khan    Create My Fulfillment Dashboard Main Suitelet 
 *
 */

//Global Variables
var projectCounter = 0;
var counter = 1;
var a_JSON = [];
var b_JSON = [];
var a_proj_array = new Array();
var a_proj_array_filter = new Array();
var a_sfdc_projects = [];
var total_frf = 0;
var total_rrf = 0; 
var values = new Array();
/*Image Links */ 
var calender = '<img src="https://system.netsuite.com/core/media/media.nl?id=1257247&c=3883006&h=ca132a145f59230970fd" class="calender_img">';
var team = '<img src="https://system.netsuite.com/core/media/media.nl?id=1257287&c=3883006&h=85ea67f2e30665186392" class="calender_img">';
var exiting = '<img src="https://system.na1.netsuite.com/core/media/media.nl?id=1257252&c=3883006&h=d124e4b81b5a369c8f0d">';
var rampup = '<img src="https://system.na1.netsuite.com/core/media/media.nl?id=1257243&c=3883006&h=c7d23397233058a5e902">';
var star = '<img src="https://system.na1.netsuite.com/core/media/media.nl?id=1257278&c=3883006&h=ac2e9cfe25078fa374d8">';
//Navigation URLs

var u_createFRF = nlapiResolveURL("SUITELET", "customscript_fuel_sut_create_frf", "customdeploy_fuel_sut_create_frf");
var u_peopleView = nlapiResolveURL("SUITELET", "customscript_sut_projectview", "customdeploy_sut_projectview");

//Main function 
function CreateDashboard(request, response){
	// Get Logged In User 
	var i_user = nlapiGetUser();
    var s_designation = nlapiLookupField('employee',i_user,'title');
  	nlapiLogExecution('AUDIT', 'before', i_user);
	/*if (i_user == 130541 || i_user== 1543 || i_user== 94862  ||i_user ==97265 || i_user == 3165 || i_user == 131458|| i_user ==948628 || i_user ==144836 || i_user == 36673 || i_user == 43693
) {
		i_user= '1646';
	}*/
  	nlapiLogExecution('AUDIT', 'after',i_user);
	// Get Logged In user's executing practice
	var spocSearch = GetPracticeSPOC(i_user);
  	nlapiLogExecution('AUDIT', 'spocSearch', JSON.stringify(spocSearch));
	if (spocSearch) {
		var i_practice = spocSearch[0].getValue('custrecord_practice');
		nlapiLogExecution('AUDIT', 'i_practice', i_practice);
		// Create JSON for dashboard table when logged in user in practice SPOC
		GetDashBoardDataPractice(i_practice);
		GetDashboardProjectManager(i_user);
	}else{
		// Create JSON for dashboard when logged in user is project manager
		GetDashboardProjectManager(i_user);
	}
  	values['{searchemployee}'] = nlapiResolveURL('SUITELET','customscript_sut_employee_search','customdeploy_sut_employee_search');
    values['{homeicon}']=nlapiResolveURL('SUITELET','customscript_fuel_sut_fulfillment_dash','customdeploy_fuel_sut_fulfillment_dash');
	values['{data1}'] = JSON.stringify(a_proj_array);
	values['{user}'] = nlapiGetContext().getName();
	values['{designation}'] = s_designation;
	values['{projectcount}'] = projectCounter;
	values['{openfrf}'] =  total_frf;
	values['{openrrf}'] =  total_rrf;
	values['{frfProgressPage}'] =  nlapiResolveURL('SUITELET','customscript_fuel_fulfillment_team_dashb','customdeploy_fuel_fulfillment_team_dashb');
	values['{filterData}'] = JSON.stringify(a_proj_array_filter);

	/*Getting User Gender for displaying Image*/
	if(nlapiLookupField('employee',i_user,'gender') == 'f'){
		values['{profilepic}'] = '/core/media/media.nl?id=1257303&c=3883006&h=b0e4f8408d1e0d948928';
	}else{
		values['{profilepic}'] = '/core/media/media.nl?id=1257271&c=3883006&h=ad48de7349910d7225a6';
	}
	/*Load HTML File*/
	var contents = nlapiLoadFile('1257137').getValue();
	// Replace with container 
	contents = replaceValues(contents, values);
	// write on page
	response.write(contents);
}
function GetPracticeSPOC(user) {
  	nlapiLogExecution('AUDIT', 'user', user);
	var spocSearch = nlapiSearchRecord("customrecord_practice_spoc",null,
			[
			 ["custrecord_spoc","anyof",user]
			 ], 
			 [
			  new nlobjSearchColumn("custrecord_practice")
			  ]
	);
	return spocSearch;
}
function GetDashBoardDataPractice(practiceId){
	var searchResult = nlapiSearchRecord("customrecord_fulfillment_dashboard_data",null,
			[
			 ["custrecord_fulfill_dashboard_practice","anyof",practiceId], 
			 "AND", 
			 ["custrecord_fulfill_dashboard_opp_id","noneof","@NONE@"],
      		 "AND",
      		 ["isinactive","is","F"]
			 ], 
			 [
			  new nlobjSearchColumn("scriptid"), 
			  new nlobjSearchColumn("custrecord_fulfill_dashboard_status"), 
			  new nlobjSearchColumn("custrecord_fulfill_dashboard_project"), 
			  new nlobjSearchColumn("custrecord_fulfill_dashboard_account"),
			  new nlobjSearchColumn("custrecord_fulfill_dashboard_rev_sta_pro"),
			  new nlobjSearchColumn("custrecord_fulfill_dashboard_reve_confid"), 
			  new nlobjSearchColumn("custrecord_fulfill_dashboard_manager"), 
			  new nlobjSearchColumn("custrecord_fulfill_dashboard_practice"), 
			  new nlobjSearchColumn("custrecord_fulfill_dashboard_pro_team"), 
			  new nlobjSearchColumn("custrecord_fulfill_dashboard_rampup"), 
			  new nlobjSearchColumn("custrecord_fulfill_dashboard_exiting"), 
			  new nlobjSearchColumn("custrecord_fulfill_dashboard_frf").setSort(false), 
			  new nlobjSearchColumn("custrecord_fulfill_dashboard_rrf"), 
			  new nlobjSearchColumn("custrecord_fulfill_dashboard_track"), 
			  new nlobjSearchColumn("custrecord_fulfill_dashboard_opp_id"), 
			  new nlobjSearchColumn("custrecord_fulfill_dashboard_last_update"), 
			  new nlobjSearchColumn("custrecord_fulfill_dashboard_start_date"),
			  new nlobjSearchColumn("custrecord_opportunity_name_sfdc","CUSTRECORD_FULFILL_DASHBOARD_OPP_ID",null),
			  new nlobjSearchColumn("custrecord_customer_internal_id_sfdc","CUSTRECORD_FULFILL_DASHBOARD_OPP_ID",null)
			  ]
	);
	if (searchResult) {
		for (var int = 0; int < searchResult.length; int++) {
			var n_frf;
			if (searchResult[int].getValue('custrecord_fulfill_dashboard_project')) {
				var projId = searchResult[int].getValue('custrecord_fulfill_dashboard_project');
				var oppId = searchResult[int].getValue('custrecord_fulfill_dashboard_opp_id');
				var s_project_link = '<span class="project_name" title="' +searchResult[int].getText('custrecord_fulfill_dashboard_project')+ '"><a class="project_link" href="'+u_peopleView+'&custpage_projectid='+projId+'&custpage_oppid='+oppId+'">'+searchResult[int].getText('custrecord_fulfill_dashboard_project')+'</a></span>';
				nlapiLogExecution("AUDIT", "projectName:"+searchResult[int].getValue("custrecord_opportunity_name_sfdc","CUSTRECORD_FULFILL_DASHBOARD_OPP_ID",null), "number of frf : "+ searchResult[int].getValue('custrecord_fulfill_dashboard_frf'));
				if (searchResult[int].getValue('custrecord_fulfill_dashboard_frf') == "0" && searchResult[int].getValue('custrecord_fulfill_dashboard_rrf') == "0" ) {
					n_frf = '<a class="prev-page" href="'+u_createFRF+'&s_project='+projId+'&s_opp='+oppId+'"><button class="btn create-frf-btn">Plan</button> </a>';
				}else{
					n_frf = searchResult[int].getValue('custrecord_fulfill_dashboard_frf');
				}
			}else{
				var oppId = searchResult[int].getValue('custrecord_fulfill_dashboard_opp_id');
				var projId = searchResult[int].getValue('custrecord_fulfill_dashboard_project');
				var s_project_link = '<span class="project_name" title="' +searchResult[int].getValue("custrecord_opportunity_name_sfdc","CUSTRECORD_FULFILL_DASHBOARD_OPP_ID",null)+ '"><a class="project_link" href="'+u_peopleView+'&custpage_projectid='+projId+'&custpage_oppid='+oppId+'">'+searchResult[int].getValue("custrecord_opportunity_name_sfdc","CUSTRECORD_FULFILL_DASHBOARD_OPP_ID",null)+'</a></span>';
				nlapiLogExecution("AUDIT", "projectName:"+searchResult[int].getValue("custrecord_opportunity_name_sfdc","CUSTRECORD_FULFILL_DASHBOARD_OPP_ID",null), "number of frf : "+ searchResult[int].getValue('custrecord_fulfill_dashboard_frf'))
				if (searchResult[int].getValue('custrecord_fulfill_dashboard_frf') == "0" && searchResult[int].getValue('custrecord_fulfill_dashboard_rrf') == "0" ) {
					n_frf = '<a class="prev-page" href="'+u_createFRF+'&s_project='+projId+'&s_opp='+oppId+'"><button class="btn create-frf-btn">Plan</button> </a>';
				}else{
					n_frf = searchResult[int].getValue('custrecord_fulfill_dashboard_frf');
				}
			}
			var s_projName='<br><span class="project_name" title="' +searchResult[int].getValue('custrecord_fulfill_dashboard_account')/*searchResult[int].getText("custrecord_customer_internal_id_sfdc","CUSTRECORD_FULFILL_DASHBOARD_OPP_ID",null)*/+ '">'+searchResult[int].getValue('custrecord_fulfill_dashboard_account')/*searchResult[int].getText("custrecord_customer_internal_id_sfdc","CUSTRECORD_FULFILL_DASHBOARD_OPP_ID",null)*/+'</span>'
			var s_startdate = searchResult[int].getValue('custrecord_fulfill_dashboard_start_date');
			s_startdate ='<p class="start_date">'+s_startdate+'</p>';
			var s_team = '<p class="team_size">'+Math.round(searchResult[int].getValue('custrecord_fulfill_dashboard_pro_team'))+'</p>';
			var s_pm = '<span class="project_name exec_pract" title="' +searchResult[int].getValue('custrecord_fulfill_dashboard_manager')+ '">'+searchResult[int].getValue('custrecord_fulfill_dashboard_manager')+'</span>';
			var s_exec_practice = '<span class="project_name" title="' +searchResult[int].getText('custrecord_fulfill_dashboard_practice')+ '">'+searchResult[int].getText('custrecord_fulfill_dashboard_practice')+'</span>';
			// Following JSON for render 
			a_JSON = {
					// pr_status: counter,In next MVP
					project_account: s_project_link + s_projName,//searchResult[int].getValue("custrecord_opportunity_name_sfdc","CUSTRECORD_FULFILL_DASHBOARD_OPP_ID",null),
					rev_status: searchResult[int].getValue('custrecord_fulfill_dashboard_rev_sta_pro')+'<br>'+searchResult[int].getValue('custrecord_fulfill_dashboard_reve_confid')+"%",
					pm_practice: s_pm+'<br>'+s_exec_practice,
					start_date: calender+s_startdate ,
					team: team + s_team,
					ramp_up: rampup + searchResult[int].getValue('custrecord_fulfill_dashboard_rampup'),
					exiting:exiting + Math.round(searchResult[int].getValue('custrecord_fulfill_dashboard_exiting')),
					frf:n_frf,
					rrf:searchResult[int].getValue('custrecord_fulfill_dashboard_rrf'),
					star:star
			}
			//Following JSON for filter 
			b_JSON = {
					// pr_status: counter,In next MVP
					project_account: searchResult[int].getValue('custrecord_fulfill_dashboard_account'),
					rev_status: searchResult[int].getValue('custrecord_fulfill_dashboard_rev_sta_pro'),
					pm_practice:searchResult[int].getText('custrecord_fulfill_dashboard_practice')
			}
			total_frf = total_frf + parseInt(searchResult[int].getValue('custrecord_fulfill_dashboard_frf'));
			total_rrf = total_rrf + parseInt(searchResult[int].getValue('custrecord_fulfill_dashboard_rrf'));
			a_proj_array.push(a_JSON);
			a_proj_array_filter.push(b_JSON);
			projectCounter++;
			counter++;
		}
	}
}
function GetDashboardProjectManager(i_user) {
	var searchResult = nlapiSearchRecord("customrecord_fulfillment_dashboard_data",null,
			[
			 [["custrecord_fulfill_dashboard_project.custentity_projectmanager","anyof",i_user],
			  "OR",
			  ["custrecord_fulfill_dashboard_project.custentity_deliverymanager","anyof",i_user]],
      		 "AND",
      		 ["isinactive","is","F"]
			 ], 
			 [
			  new nlobjSearchColumn("scriptid").setSort(false), 
			  new nlobjSearchColumn("custrecord_fulfill_dashboard_status"), 
			  new nlobjSearchColumn("custrecord_fulfill_dashboard_project"), 
			  new nlobjSearchColumn("custrecord_fulfill_dashboard_account"), 
			  new nlobjSearchColumn("custrecord_fulfill_dashboard_rev_sta_pro"), 
			  new nlobjSearchColumn("custrecord_fulfill_dashboard_reve_confid"), 
			  new nlobjSearchColumn("custrecord_fulfill_dashboard_manager"), 
			  new nlobjSearchColumn("custrecord_fulfill_dashboard_practice"), 
			  new nlobjSearchColumn("custrecord_fulfill_dashboard_pro_team"), 
			  new nlobjSearchColumn("custrecord_fulfill_dashboard_rampup"), 
			  new nlobjSearchColumn("custrecord_fulfill_dashboard_exiting"), 
			  new nlobjSearchColumn("custrecord_fulfill_dashboard_frf"), 
			  new nlobjSearchColumn("custrecord_fulfill_dashboard_rrf"), 
			  new nlobjSearchColumn("custrecord_fulfill_dashboard_track"), 
			  new nlobjSearchColumn("custrecord_fulfill_dashboard_opp_id"), 
			  new nlobjSearchColumn("custrecord_fulfill_dashboard_last_update"), 
			  new nlobjSearchColumn("custrecord_fulfill_dashboard_start_date"),
			  new nlobjSearchColumn("altname","CUSTRECORD_FULFILL_DASHBOARD_PROJECT",null)
			  ]
	);
	if (searchResult) {
		for (var int = 0; int < searchResult.length; int++) {
			var projId = searchResult[int].getValue('custrecord_fulfill_dashboard_project');
			var oppId = searchResult[int].getValue('custrecord_fulfill_dashboard_opp_id');
			var s_project_link = '<span class="project_name" title="' +searchResult[int].getValue("altname","CUSTRECORD_FULFILL_DASHBOARD_PROJECT",null)+ '"><a class="project_link" href="'+u_peopleView+'&custpage_projectid='+projId+'&custpage_oppid='+oppId+'">'+searchResult[int].getValue("altname","CUSTRECORD_FULFILL_DASHBOARD_PROJECT",null)+'</a></span>';
			var s_projName='<br><span class="project_name" title="' +searchResult[int].getValue('custrecord_fulfill_dashboard_account')+ '">'+searchResult[int].getValue('custrecord_fulfill_dashboard_account')+'</span>'
			var s_startdate = searchResult[int].getValue('custrecord_fulfill_dashboard_start_date');
			s_startdate ='<p class="start_date">'+s_startdate+'</p>';
			var s_pm = '<span class="project_name exec_pract" title="' +searchResult[int].getValue('custrecord_fulfill_dashboard_manager')+ '">'+searchResult[int].getValue('custrecord_fulfill_dashboard_manager')+'</span>';
			var s_exec_practice = '<span class="project_name" title="' +searchResult[int].getText('custrecord_fulfill_dashboard_practice')+ '">'+searchResult[int].getText('custrecord_fulfill_dashboard_practice')+'</span>';
			var n_frf = searchResult[int].getValue('custrecord_fulfill_dashboard_frf'); 
			if (searchResult[int].getValue('custrecord_fulfill_dashboard_frf') == "0" && searchResult[int].getValue('custrecord_fulfill_dashboard_rrf') == "0"&& _logValidation(oppId)) {
				n_frf = '<a class="prev-page" href='+u_createFRF+'&s_project='+projId+'&s_opp='+oppId+'><button class="btn create-frf-btn">Plan</button> </a>';
			}else{
				n_frf = searchResult[int].getValue('custrecord_fulfill_dashboard_frf');
			}
			// Following JSON for rendering 
			a_JSON = {
					// pr_status: counter, In next MVP
					project_account: s_project_link + s_projName,
					rev_status: searchResult[int].getValue('custrecord_fulfill_dashboard_reve_confid')+'<br>'+ searchResult[int].getValue('custrecord_fulfill_dashboard_rev_sta_pro'),
					pm_practice: s_pm + '<br>' + s_exec_practice,//searchResult[int].getValue('custrecord_fulfill_dashboard_manager')+'<br>'+searchResult[int].getText('custrecord_fulfill_dashboard_practice'),
					start_date: calender+ s_startdate,
					team: team+' '+ Math.round(searchResult[int].getValue('custrecord_fulfill_dashboard_pro_team')),
					ramp_up: rampup + searchResult[int].getValue('custrecord_fulfill_dashboard_rampup'),
					exiting:exiting + Math.round(searchResult[int].getValue('custrecord_fulfill_dashboard_exiting')),
					frf:n_frf,
					rrf:searchResult[int].getValue('custrecord_fulfill_dashboard_rrf'),
					star:star
			}
			// Following JSON for filter 
			b_JSON = {
					// pr_status: counter,In next MVP
					project_account: searchResult[int].getValue('custrecord_fulfill_dashboard_account'),
					rev_status: searchResult[int].getValue('custrecord_fulfill_dashboard_rev_sta_pro'),
					pm_practice:searchResult[int].getText('custrecord_fulfill_dashboard_practice')
					}
			total_frf = total_frf + parseInt(searchResult[int].getValue('custrecord_fulfill_dashboard_frf'));
			total_rrf = total_rrf + parseInt(searchResult[int].getValue('custrecord_fulfill_dashboard_rrf'));
			a_proj_array.push(a_JSON);
			a_proj_array_filter.push(b_JSON);
			projectCounter++;
			counter++;
		}
	}
}
function replaceValues(content, oValues)
{
	for(param in oValues)
	{
		// Replace null values with blank
		var s_value	=	(oValues[param] == null)?'':oValues[param];

		// Replace content
		//content = content.replace(new RegExp('{{' + param + '}}','gi'), s_value);
		content = content.replace(param, s_value);
	}

	return content;
}
function _logValidation(value){
	if (value != null && value != '' && value != undefined && value != 'NaN') {
		return true;
	}
	else {
		return false;
	}
}