function getdata(data){
var resourceallocationSearch = nlapiSearchRecord("resourceallocation",null,
[
   ["resource","anyof","157407"], 
   "AND", 
   ["startdate","notafter","08/22/2020"], 
   "AND", 
   ["enddate","notbefore","08/16/2020"]
], 
[
                new nlobjSearchColumn('project'),
                new nlobjSearchColumn('startdate'),
                new nlobjSearchColumn('jobtype', 'job'),
                new nlobjSearchColumn('jobbillingtype', 'job'),
                new nlobjSearchColumn('formulanumeric')
                        .setFormula('{percentoftime}'),
                new nlobjSearchColumn('enddate').setSort(false),//Praveena@17-07-2020- Added sort latest endate.
                new nlobjSearchColumn('percentoftime'),
                new nlobjSearchColumn('custevent1'),
                new nlobjSearchColumn('custevent_otserviceitem')
]
);

 

nlapiLogExecution('debug','4208 resourceallocationSearch',resourceallocationSearch.length);
  return resourceallocationSearch.length;
}