
function clientSaveRecord(){
try{	
		var type = '';
		var projectType = nlapiGetFieldText('jobtype');
	    var projectStatus = nlapiGetFieldText('entitystatus');
	    var lineCount = nlapiGetLineItemCount('mediaitem');
		var value = nlapiGetFieldValue('custentity_sold_margin_percent');
		 var recid = nlapiGetRecordId();
		 if(recid){
			type = 'edit'; 
		 }
		 else{
			 type = 'create';
		 }
		   if(!_logValidation(value) && projectType == 'External' && projectStatus == 'Active'){
			   alert('Sold Margin % cannot be blank  !');
		   	   return false;
			}
		   
		   if(type == 'create'){ 
		   if(projectType == 'External' && projectStatus == 'Active'){
			   if(parseFloat(value) < parseFloat(40)){
					   if(parseInt(lineCount) <= parseInt(0)){
					   alert('Please upload the approval document!!');
				   	   return false;
					   }
			   }
		   }
		   }
		   
		   if(type == 'edit'){
			   if(projectType == 'External' && projectStatus == 'Active'){
				   if(parseFloat(value) < parseFloat(40)){
					
						var filters_p = [];
						filters_p.push(new nlobjSearchFilter('internalid', null, 'anyof', recid));
						
						
						var searchRec = nlapiSearchRecord('job','customsearch1871',filters_p,[new nlobjSearchColumn('internalid'),
				                new nlobjSearchColumn('name','file'),
				                new nlobjSearchColumn('internalid','file')]);
						if(!_logValidation(searchRec)){
						   alert('Please upload the approval document!!');
					   	   return false;
						   }
				   }
			   }
			   }
		  
		  
		return true;
}
    
catch(e){
	nlapiLogExecution('debug','Validate Field Error',e);
	
}
}

function _logValidation(value) 
{
 if(value!='null' && value != null && value != null && value != '' && value != undefined && value != undefined && value != 'undefined' && value!= 'undefined'&& value!= 'NaN' && value != NaN) 
 {
  return true;
 }
 else 
 { 
  return false;
 }
}

