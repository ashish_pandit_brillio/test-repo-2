function pageInit() {
	document.getElementById('divTabDataInfo').style.display = "inline";

	var div = document.getElementById('divTabInfo');
	var para = div.children[0];
	para.style.backgroundColor = '#66ff33';
}

function displayTabData(dataId, tabId, url) {

	var tabsList = document.getElementsByClassName('pd-tab');

	for (var i = 0; i < tabsList.length; i++) {
		try {
			var para = tabsList[i].children[0];
			para.style.backgroundColor = '#99ffd6';
		} catch (er) {
			console.log(tabsList[i]);
		}
	}

	var div = document.getElementById(tabId);
	var para = div.children[0];
	para.style.backgroundColor = '#66ff33';

	var dataDivList = document.getElementsByClassName('pd-tab-data');

	for (var i = 0; i < dataDivList.length; i++) {
		try {
			dataDivList[i].style.display = "none";
		} catch (er) {
			console.log(dataDivList[i]);
		}
	}

	document.getElementById(dataId).style.display = "inline";
}