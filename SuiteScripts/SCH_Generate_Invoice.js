/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       04 Aug 2015     nitish.mishra
 *
 */

/**
 * @param {String}
 *            type Context Types: scheduled, ondemand, userinterface, aborted,
 *            skipped
 * @returns {Void}
 */
function scheduled(type) {
	try {
		nlapiLogExecution('debug', 'started');

		// get the data from parameters
		var context = nlapiGetContext();
		var data = context.getPreference('custscript_invoice_details');

		context.setPercentComplete(0);

		if (data) {
			var invoiceDetails = JSON.parse(data);
			nlapiLogExecution('debug', 'no. of records', invoiceDetails.length);

			var mailContent = "";

			mailContent += "Hi,<br/> Following invoices have been created by the auto invoice process : <br/><br/>";

			mailContent += "<table>";
			mailContent += "<tr>";
			mailContent += "<td>Invoice #</td>";
			mailContent += "<td>Project</td>";
			mailContent += "<td>Bill From</td>";
			mailContent += "<td>Bill To</td>";
			mailContent += "<td>Error</td>";
			mailContent += "</tr>";

			for (var i = 0; i < invoiceDetails.length; i++) {
				yieldScript(context);

				mailContent += generateInvoice(invoiceDetails[i].c,
				        invoiceDetails[i].p, invoiceDetails[i].e,
				        invoiceDetails[i].f, invoiceDetails[i].t);

				context
				        .setPercentComplete(((i + 1) / invoiceDetails.length) * 100);
			}

			mailContent += "</table>";

			// send email
			nlapiSendEmail(constant.Mail_Author.InformationSystems,
			        'billing@brillio.com;prashanth.s@BRILLIO.COM',
			        'Auto Invoice Completed', mailContent);

			nlapiLogExecution('debug', 'email send');
		} else {
			throw "No data in script parameter";
		}

		context.setPercentComplete(100);
		nlapiLogExecution('debug', 'ended');
	} catch (err) {
		nlapiLogExecution('ERROR', 'scheduled', err);
	}
}

function generateInvoice(customer, project, employee, billingFrom, billingTo) {
	var finalObject = {
	    Customer : customer,
	    Project : project,
	    Employee : employee,
	    BillingFrom : billingFrom,
	    BillingTo : billingTo,
	    Invoice : '',
	    Error : ''
	};

	try {
		nlapiLogExecution('debug', 'customer', customer);
		nlapiLogExecution('debug', 'project', project);
		nlapiLogExecution('debug', 'employee', employee);
		nlapiLogExecution('debug', 'billingFrom', billingFrom);
		nlapiLogExecution('debug', 'billingTo', billingTo);

		// create a new invoice record and set the fields
		var invoiceRecord = nlapiCreateRecord('invoice', {
		    recordmode : 'dynamic',
		    customform : 115,
		    entity : customer
		});
		nlapiLogExecution('debug', 'record object created');
		//Logic to find out group billing tm
		var project_group_tm = nlapiLookupField('job',parseInt(project),'custentity_group_tm_project');
		
		invoiceRecord.setFieldValue('job', project);
		invoiceRecord.setFieldValue('custbody_billfrom', billingFrom);
		invoiceRecord.setFieldValue('custbody_billto', billingTo);

		finalObject.Project = invoiceRecord.getFieldText('job');
		finalObject.Customer = invoiceRecord.getFieldText('customer');

		if (invoiceRecord.getFieldValue('subsidiary') == '2') {
			invoiceRecord.setFieldValue('location', 9);
		} else {
			invoiceRecord.setFieldValue('location', ''); // to be set for
			// BTPL
		}

		invoiceRecord.setFieldValue('custbody_layout_type', '2'); // to be
		// made
		// dynamic

		billingFrom = nlapiStringToDate(billingFrom);
		billingTo = nlapiStringToDate(billingTo);

		var checkStarted = false;
		var wasApplied = false;
		var lineCount =invoiceRecord.getLineItemCount('time');
		nlapiLogExecution('debug', 'record  lineCount',lineCount);
		// apply the timesheets for the billing from and to date
		for (var line = 1; line<=lineCount; line++) {

			var currentLineDate = nlapiStringToDate(invoiceRecord
			        .getLineItemValue('time', 'billeddate', line));
			var currentLineemployee = invoiceRecord
			        .getLineItemValue('time', 'employee', line);		
					
			//Logic For FrediMac Customer , Group Billing	
			//parseInt(project) == parseInt(25392) || parseInt(project) == parseInt(72256) || parseInt(project) == parseInt(94646)
			//|| parseInt(project) == parseInt(95916) || parseInt(project) == parseInt(97457)			
			if(project_group_tm == 'T'){	
				if (currentLineDate >= billingFrom && currentLineDate <= billingTo && parseInt(currentLineemployee) == parseInt(employee)) {
				invoiceRecord.selectLineItem('time', line);
				invoiceRecord.setCurrentLineItemValue('time', 'apply', 'T');
				invoiceRecord.commitLineItem('time');
				checkStarted = true;
				wasApplied = true;
				nlapiLogExecution('debug', 'record  currentLineDate',currentLineDate);
				nlapiLogExecution('debug', 'record  currentLineemployee',currentLineemployee);
			} else {
				wasApplied = false;
			}	
			}			
			else{
			if (currentLineDate >= billingFrom && currentLineDate <= billingTo) {
				invoiceRecord.selectLineItem('time', line);
				invoiceRecord.setCurrentLineItemValue('time', 'apply', 'T');
				invoiceRecord.commitLineItem('time');
				checkStarted = true;
				wasApplied = true;
			} else {
				wasApplied = false;
			}
		}
			//if (checkStarted && !wasApplied) {
			//	break;
			//}
		}

		// commit the invoice record
		var invoiceId = nlapiSubmitRecord(invoiceRecord, true, true);
		nlapiLogExecution('debug', 'invoice internal id', invoiceId);

		finalObject.Invoice = nlapiLookupField('invoice', invoiceId, 'tranid');
	} catch (err) {
		nlapiLogExecution('ERROR', 'generateInvoice', err);
		finalObject.Error = err;
	}

	var mailContent = "";
	mailContent += "<tr>";
	mailContent += "<td>" + finalObject.Invoice + "</td>";
	mailContent += "<td>" + finalObject.Project + "</td>";
	mailContent += "<td>" + finalObject.BillingFrom + "</td>";
	mailContent += "<td>" + finalObject.BillingTo + "</td>";
	mailContent += "<td>" + finalObject.Error + "</td>";
	mailContent += "</tr>";

	nlapiLogExecution('audit', 'DATA_FOR_SUITELET', JSON.stringify(finalObject));

	return mailContent;
}

function yieldScript(currentContext) {

	if (currentContext.getRemainingUsage() <= 100) {
		nlapiLogExecution('AUDIT', 'API Limit Exceeded');
		var state = nlapiYieldScript();

		if (state.status == "FAILURE") {
			nlapiLogExecution('ERROR', 'Yield Script Failed', 'Reason : '
			        + state.reason + ' / Size : ' + state.size);
			return false;
		} else if (state.status == "RESUME") {
			nlapiLogExecution('AUDIT', 'Script Resumed');
		}
	}
}