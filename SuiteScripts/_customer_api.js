    /**
     * @NApiVersion 2.x
     * @NScriptType Restlet
     * @NModuleScope SameAccount
     */
     define(['N/record', 'N/search', 'N/runtime'],
     function (obj_Record, obj_Search, obj_Runtime) {
         function Send_Customer_data(obj_Request) {
             try {
                 log.debug('requestBody ==', JSON.stringify(obj_Request));
                 var obj_Usage_Context = obj_Runtime.getCurrentScript();
                 var s_Request_Type = obj_Request.RequestType;
                 log.debug('s_Request_Type ==', s_Request_Type);
                 var s_Request_Data = obj_Request.RequestData;
                 log.debug('s_Request_Data ==', s_Request_Data);
                 var obj_Response_Return = {};
                 if (s_Request_Data == 'CUSTOMER') {
                     obj_Response_Return = get_Customer_Data(obj_Search);
                 }
             } 
             catch (s_Exception) {
                 log.debug('s_Exception==', s_Exception);
             } //// 
             log.debug('obj_Response_Return==', JSON.stringify(obj_Response_Return));
             log.debug('Final Remaining Usage  ==', obj_Usage_Context.getRemainingUsage());
             return obj_Response_Return;
         } ///// Function Send_Customer_data
 
 
         return {
             post: Send_Customer_data
         };
 
     }); //// End of Main Function
     /****************** Supporting Functions ***********************************/
     function get_Customer_Data(obj_Search) {
     try {
        var customerSearchObj = obj_Search.create({
            type: "customer",
            filters:
            [
            ],
            columns:
            [
                obj_Search.createColumn({name: "internalid", label: "Internal ID"}),
                obj_Search.createColumn({
                  name: "entityid",
                  sort: obj_Search.Sort.ASC,
                  label: "ID"
               }),
               obj_Search.createColumn({name: "altname", label: "Name"}),
               obj_Search.createColumn({name: "custentity_vertical", label: "Vertical"}),
               obj_Search.createColumn({name: "territory", label: "Territory"}),
               obj_Search.createColumn({
                name: "custentity_fusion_empid",
                join: "CUSTENTITY_CLIENTPARTNER",
                label: "Fusion Employee Id"
             }),
             obj_Search.createColumn({
                name: "custentity_fusion_empid",
                join: "CUSTENTITY_ACCOUNT_DELIVERY_OWNER",
                label: "Fusion Employee Id"
             })
            ]
         });
         var searchResultCount = customerSearchObj.runPaged().count;
         log.debug("customerSearchObj result count",searchResultCount);
         var myResults = getAllResults(customerSearchObj);
         var arr_Cutomer_json = [];
         myResults.forEach(function (result) {
             var obj_json_Container = {}
             obj_json_Container = {
                 'Internal_ID': result.getValue({ name: "internalid", label: "Internal ID" }),
                 'Customer_ID': result.getValue({ name: "entityid"}),
                 'Customer_Name': result.getValue({ name: "altname"}),
                 'Vertical':result.getText({name: 'custentity_vertical'}),
                 'Territory':result.getText({name: "territory"}),
                 'Client_Partner_Employee_Id':result.getValue({ name: "custentity_fusion_empid",join: "CUSTENTITY_CLIENTPARTNER"}),
                 'Account_Delivery_Owner_Employee_Id':result.getValue({ name: "custentity_fusion_empid",join: "CUSTENTITY_ACCOUNT_DELIVERY_OWNER"})
             };
             arr_Cutomer_json.push(obj_json_Container);
             return true;
         });
         return arr_Cutomer_json;
     } //// End of try
     catch (s_Exception) {
         log.debug('get_Customer_Data s_Exception== ', s_Exception);
     } //// End of catch 
     } ///// End of function get_Customer_Data()
 
 
     function getAllResults(s) {
     var result = s.run();
     var searchResults = [];
     var searchid = 0;
     do {
         var resultslice = result.getRange({ start: searchid, end: searchid + 1000 });
         resultslice.forEach(function (slice) {
             searchResults.push(slice);
             searchid++;
         }
         );
     } while (resultslice.length >= 1000);
     return searchResults;
     }