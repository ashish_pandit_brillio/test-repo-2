/**

 
 
 **/
/** Author Sai Saranya
	11 May 2017
 **/

function createApprovalForm()
{
	try{
	// vendorbill saves search1863
		var search = nlapiSearchRecord(
		        'vendorbill',null,[new nlobjSearchFilter('internalid',null,'anyof',
								[514139,511291,512754,517578,517577,511159,510675,515802,515645,515646,519352,514143,514146,
									514148,514147,519348,514165,514129,514160])],
						[ new nlobjSearchColumn('custbody_created_by_user'),
						new nlobjSearchColumn('internalid'),
		                new nlobjSearchColumn('altname','job'),
		                new nlobjSearchColumn('custbody_billfrom'),
		                new nlobjSearchColumn('approvalstatus')]);
			if(search)
				{
				for(var i=0;i<search.length;i++)
				{
					var bilid=search[i].getValue('internalid');
					var user=search[i].getValue('custbody_created_by_user');
					
					
					var rec=nlapiLoadRecord('vendorbill',bilid);
					
					nlapiSubmitRecord(rec);
					nlapiLogExecution('debug', 'bilid', bilid);
					nlapiLogExecution('debug', 'user', user);
				}
				}
			}
			catch(e)
			{
			nlapiLogExecution('error', 'Main', e);
			}
		}
					
			