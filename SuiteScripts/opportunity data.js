/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       16 March 2020     Prabhat Gupta
 *
 */

/**
 * @param {Object} dataIn Parameter object
 * @returns {Object} Output object
 */
 function getRESTlet(dataIn) {

	return {};
}

/**
 * @param {Object} dataIn Parameter object
 * @returns {Object} Output object
 */
function postRESTlet(dataIn) {

	try {
		
		//dataIn = {"lastUpdatedTimestamp":""}
        var response = new Response();
        //nlapiLogExecution('debug', 'dataIn', JSON.stringify(dataIn));
        var currentDateAndTime = sysDate();
		//Log for current date



		var receivedDate = dataIn.lastUpdatedTimestamp;
		//nlapiLogExecution('DEBUG', 'Current Date', 'receivedDate...' + receivedDate);
		//nlapiLogExecution('DEBUG', 'Current Date', 'currentDateAndTime...' + currentDateAndTime);

		//var receivedDate = '7/12/2019 1:00 am';
		//Check if the timestamp is empty
		if (!_logValidation(receivedDate)) {          // for empty timestanp in this we will give whole data as a response
			var filters = new Array();
			filters = [
				
				  ["isinactive","is","F"]			 
				
				];
		//	nlapiLogExecution('debug', 'filters ', filters);
		} else {
			var filters = new Array();                             // this filter will provide the result within the current date and given date     
			filters = [
				
				["lastmodified", "within", receivedDate, currentDateAndTime]				
				];
		//	nlapiLogExecution('debug', 'filters ', filters);
		}
		
		// created search by grouping 
		var searchResults = searchRecord("customrecord_sfdc_opportunity_record",null,
				filters, 
				[
				      
					  
					   new nlobjSearchColumn("custrecord_opportunity_name_sfdc"), 
					   new nlobjSearchColumn("custrecord_opportunity_id_sfdc"), 
                       new nlobjSearchColumn("custrecord_most_likely_date_sfdc"), //NIS-1329 prabhat gupta 19/05/2020
					   new nlobjSearchColumn("internalid","CUSTRECORD_CUSTOMER_INTERNAL_ID_SFDC",null), 
					   new nlobjSearchColumn("custrecord_projection_status_sfdc"), 
					   new nlobjSearchColumn("custrecord_stage_sfdc"),
					   new nlobjSearchColumn("custrecord_practice_internal_id_sfdc"), 
					   				   
					   new nlobjSearchColumn("isinactive"),
					   
					   
						new nlobjSearchColumn("custrecord_confidence_sfdc"),
						new nlobjSearchColumn("custrecord_opp_type_sfdc"),
						new nlobjSearchColumn("custrecord_account_type_sfdc"),
						new nlobjSearchColumn("custrecord_tcv_sfdc"),
						new nlobjSearchColumn("custrecord_projection_status_sfdc"),
						new nlobjSearchColumn("custrecord_end_date_sfdc"),
						
						
						new nlobjSearchColumn("custrecord_set_owner"),
                        new nlobjSearchColumn("custrecord_sfdc_opp_rec_rev_enddate"),
						new nlobjSearchColumn("custrecord_sfdc_opp_rec_rev_startdate"),
						new nlobjSearchColumn("custrecord_opportunity_id_sfdc","custrecord_sfdc_parent_opp_id",null),
						new nlobjSearchColumn("custrecord_sfdc_parent_opp_id"),
						new nlobjSearchColumn("custrecord_project_sfdc"), 
						new nlobjSearchColumn("custrecord_project_internal_id_sfdc"),
                        new nlobjSearchColumn("custrecord_fulfilment_plan_sfdc")
					
				]
				);	


						
		
		
		var oppData = [];
        
		if (searchResults) {
			
			var opp_detail = getOpportunityDetails();
			
			var total_position_data = getTotalPosition();
			
			for(var i=0; i<searchResults.length; i++){    
				
		    var opp = {};
                   
			var oppInternalId = searchResults[i].getId();
			var oppName = searchResults[i].getValue("custrecord_opportunity_name_sfdc");
			var oppId = searchResults[i].getValue("custrecord_opportunity_id_sfdc");
			var customerInternalId = searchResults[i].getValue("internalid","CUSTRECORD_CUSTOMER_INTERNAL_ID_SFDC",null);
			var oppStatusId =  searchResults[i].getValue("custrecord_projection_status_sfdc");
            var oppStatusName =  searchResults[i].getText("custrecord_projection_status_sfdc");
			var oppStageId =  searchResults[i].getValue("custrecord_stage_sfdc");
			var oppStageName =  searchResults[i].getText("custrecord_stage_sfdc");
			var practiceId = searchResults[i].getValue("custrecord_practice_internal_id_sfdc");	
            var mostLikelyDate = searchResults[i].getValue("custrecord_most_likely_date_sfdc"); //NIS-1329 prabhat gupta 19/05/2020
			var inactiveStatus = searchResults[i].getValue("isinactive")
			var projectId = searchResults[i].getValue("custrecord_project_sfdc")
			var projectName = searchResults[i].getText("custrecord_project_internal_id_sfdc");
			var practice = searchResults[i].getText("custrecord_practice_internal_id_sfdc");
            var fulfilmentPlan = searchResults[i].getValue("custrecord_fulfilment_plan_sfdc");
              
            
			
              var revisedStartDate =  searchResults[i].getValue("custrecord_sfdc_opp_rec_rev_startdate");
			var revisedEndDate = searchResults[i].getValue("custrecord_sfdc_opp_rec_rev_enddate");	
			
			opp.oppInternalId = oppInternalId;
			opp.oppName = oppName;
			opp.oppId = oppId;
			opp.customerInternalId = customerInternalId;
			opp.oppStatusId = oppStatusId;
            opp.oppStatusName = oppStatusName;
			opp.oppStageId = oppStageId;
			opp.oppStageName = oppStageName;
			opp.practiceId = practiceId;		
            opp.mostLikelyDate = mostLikelyDate; //NIS-1329 prabhat gupta 19/05/2020
			opp.inactiveStatus = inactiveStatus;
			opp.projectId  = projectId ;
			opp.projectName = projectName;
            opp.fulfilmentPlan = fulfilmentPlan;

			var participating_practice = [];
			
			var opp_data = opp_detail[oppInternalId] ;
			
			if(_logValidation(opp_data)){
			
				for(var j=0; j<opp_data.s_practice.length; j++){				
					
					var practiceName = 	opp_data.s_practice[j];			
					
					if ((practiceName != practice)) {
						
						if (_logValidation(practiceName)) {
							participating_practice.push(practiceName);
						}
					}
				}
			}
                
			opp.participatingpractice = participating_practice;

                opp.confidence = {
                    "name": searchResults[i].getValue("custrecord_confidence_sfdc")
                };
                opp.stage = {
                    "name": searchResults[i].getValue("custrecord_account_type_sfdc")
                };
                opp.tcv = {
                    "name": searchResults[i].getValue("custrecord_tcv_sfdc")
                };
                opp.opportunityType = {
                    "name": searchResults[i].getValue("custrecord_opp_type_sfdc")
                };
                opp.setOwner = {
                    "name": searchResults[i].getText("custrecord_set_owner"),
                    "id": searchResults[i].getValue("custrecord_set_owner")
                };	
				
				
				var parent_opp = searchResults[i].getValue("custrecord_opportunity_id_sfdc","custrecord_sfdc_parent_opp_id",null) || "";
				
				var parent_opp_id = searchResults[i].getValue("custrecord_sfdc_parent_opp_id") || "";
				
				opp.parentOpp = {
                    "name": parent_opp,
                    "id": parent_opp_id
                };
				
				var total_position = total_position_data[oppInternalId];
				   
				var total_pos = "";
				var start_date = "";
				var end_date = "";  
				
				   if(_logValidation(total_position)){
				
						 total_pos = total_position.total_position || "";
						 start_date = total_position.startDate || "";
						 end_date = total_position.endDate || "";
					
				   }
				
				
				
				opp.totalPosition = total_pos;
				opp.endDate = end_date;
				opp.startDate = start_date;
              
              opp.revisedStartDate = revisedStartDate || "";
              opp.revisedEndDate = revisedEndDate || "";
				
			oppData.push(opp);
			
		    }
		}
		

		response.timeStamp = currentDateAndTime;
		response.data = oppData;
		//response.data = dataRow;
		response.status = true;
	//	nlapiLogExecution('debug', 'response',JSON.stringify(response));
	} catch (err) {
		nlapiLogExecution('ERROR', 'postRESTlet', err);
		response.timeStamp = '';
		response.data = err;
		response.status = false;
	}
	return response;

}
//validate blank entries
function _logValidation(value) {
	if (value != null && value.toString() != null && value != '' && value != undefined && value.toString() != undefined && value != 'undefined' && value.toString() != 'undefined' && value.toString() != 'NaN' && value != NaN) {
		return true;
	} else {
		return false;
	}
}
//Get current date

//Returns timestamp
function timestamp() {
	var str = "";

	var currentTime = new Date();
	var hours = currentTime.getHours();
	var minutes = currentTime.getMinutes();
	var seconds = currentTime.getSeconds();
	var meridian = "";
	if (hours > 12) {
		meridian += "pm";
	} else {
		meridian += "am";
	}
	if (hours > 12) {

		hours = hours - 12;
	}
	if (minutes < 10) {
		minutes = "0" + minutes;
	}
	if (seconds < 10) {
		seconds = "0" + seconds;
	}
	//str += hours + ":" + minutes + ":" + seconds + " ";
	str += hours + ":" + minutes + " ";
	return str + meridian;
}

function Response() {
	this.status = "";
	this.timeStamp = "";
	this.data = "";

}

//Search function for results more than 1000
function searchRecord(recordType, savedSearch, arrFilters, arrColumns,
		filterExpression) {

	try {
		var search = null;

		// if a saved search is provided, load it and add the filters and
		// columns
		if (savedSearch) {
			search = nlapiLoadSearch(recordType, savedSearch);

			if (isArrayNotEmpty(arrFilters)) {
				search.addFilters(arrFilters);
			}

			if (isArrayNotEmpty(arrColumns)) {
				search.addColumns(arrColumns);
			}

			if (isArrayNotEmpty(filterExpression)) {
				search.setFilterExpression(filterExpression);
			}
		}
		// create a new search
		else {
			search = nlapiCreateSearch(recordType, arrFilters, arrColumns);
		}

		// run search
		var resultSet = search.runSearch();

		// iterate through the search and get all data 1000 at a time
		var searchResultCount = 0;
		var resultSlice = null;
		var searchResult = [];

		do {
			resultSlice = resultSet.getResults(searchResultCount,
					searchResultCount + 1000);

			if (resultSlice) {

				resultSlice.forEach(function(result) {

					searchResult.push(result);
					searchResultCount++;
				});
			}
		} while (isArrayNotEmpty(resultSlice) && resultSlice.length >= 1000);

		return searchResult;
	} catch (err) {
		nlapiLogExecution('ERROR', 'searchRecord', err);
		throw err;
	}
}



function isEmpty(value) {

	return value == null || value == "" || typeof(value) == undefined;
}

function isNotEmpty(value) {

	return !isEmpty(value);
}

function isArrayEmpty(argArray) {

	return !isArrayNotEmpty(argArray);
}

function isArrayNotEmpty(argArray) {

	return (isNotEmpty(argArray) && argArray.length > 0);
}
//function to format date object into NetSuite's mm/dd/yyyy format.
function formatNSDate(dateObj) {
	if (dateObj) {
		var nsFormatDate = dateObj.getFullYear() + '-' + addZeros(dateObj.getMonth() + 1, 2) + '-' + addZeros(dateObj.getDate(), 2);
		return nsFormatDate;
	}
	return null;
}

//function to add leading zeros on date parts.
function addZeros(num, len) {
	var str = num.toString();
	while (str.length < len) {
		str = '0' + str;
	}
	return str;
}

function getOpportunityDetails() {
    var tempArray = [];
	var data = {};
    var customrecord_sfdc_opportunity_recordSearch = searchRecord("customrecord_sfdc_opportunity_record", null,
        [
            ["isinactive", "is", "F"]
        ],
        [
            new nlobjSearchColumn("custrecord_fulfill_plan_practice", "CUSTRECORD_FULFILL_PLAN_OPP_ID", null)
        ]
    );
    if (customrecord_sfdc_opportunity_recordSearch) {
        for (var i = 0; i < customrecord_sfdc_opportunity_recordSearch.length; i++) {
			
			var opp_internal_id = customrecord_sfdc_opportunity_recordSearch[i].getId();
			var all_practice = [];
            var s_practice = customrecord_sfdc_opportunity_recordSearch[i].getValue("custrecord_fulfill_plan_practice", "CUSTRECORD_FULFILL_PLAN_OPP_ID", null);
			
			if(_logValidation(data[opp_internal_id])){
				
			    all_practice = data[opp_internal_id].s_practice ;
				all_practice.push(s_practice);
				
			    data[opp_internal_id] = {
					"id" : opp_internal_id,
					"s_practice" : all_practice
				}
				
			}else{
				all_practice.push(s_practice);
				
				data[opp_internal_id] = {
					"id" : opp_internal_id,
					"s_practice" : all_practice
				}
				
			}
            

        }
    }
    return data;
}

function getTotalPosition() {
    var data = {}
    var customrecord_fulfilmentDashboardSearch = searchRecord("customrecord_fulfillment_dashboard_data", null,
        [
            ["isinactive", "is", "F"]
        ],
        [
            new nlobjSearchColumn("custrecord_fulfill_dashboard_total_pos"), 
			new nlobjSearchColumn("custrecord_fulfill_dashboard_end_date"),
			new nlobjSearchColumn("custrecord_fulfill_dashboard_start_date"),
			new nlobjSearchColumn("custrecord_fulfill_dashboard_opp_id")
        ]
    );
    if (customrecord_fulfilmentDashboardSearch) {
        for (var i = 0; i < customrecord_fulfilmentDashboardSearch.length; i++) {
			
	  var opp_internal_id = customrecord_fulfilmentDashboardSearch[i].getValue("custrecord_fulfill_dashboard_opp_id");
      var total_position = customrecord_fulfilmentDashboardSearch[i].getValue("custrecord_fulfill_dashboard_total_pos") || "";
	  var startDate = customrecord_fulfilmentDashboardSearch[i].getValue("custrecord_fulfill_dashboard_start_date") || "";
	  var endDate = customrecord_fulfilmentDashboardSearch[i].getValue("custrecord_fulfill_dashboard_end_date") || "";
			
			if(_logValidation(opp_internal_id)){
				
			   data[opp_internal_id] = {
				"id" : opp_internal_id,
				"total_position" : total_position,
				"startDate": startDate,
				"endDate": endDate
				}  
			}			
            
        }
    }
    return data;
}