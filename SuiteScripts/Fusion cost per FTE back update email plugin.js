   function process(email) {

       nlapiLogExecution('DEBUG', 'EMAIL PLUGIN TRIGGERED');

       var attachments = email.getAttachments();

       if (attachments != null) {

           for (var a in attachments) {

               var attachment = attachments[a];

               attachment.setFolder(1949431); // 1805627 sandbox Folder Id for Fusion Cost per FTE Back Update in file cabinat

               var fileId = nlapiSubmitFile(attachment);

           }

       }

   }