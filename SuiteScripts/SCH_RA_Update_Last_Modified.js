/**
 * Module Description
 * 
 * Version Date Author Remarks 1.00 Apr 17 2015 Nitish Mishra
 * 
 */

/**
 * @param {String}
 *        type Context Types: scheduled, ondemand, userinterface, aborted,
 *        skipped
 * @returns {Void}
 */
function scheduled(type) {

	try {
		nlapiLogExecution('debug', 'start');
		var allocationSearch = searchRecord(null, 799);

		if (isNotEmpty(allocationSearch)) {
			var allocationId = null;
			var context = nlapiGetContext();
			var allocation = null;
			var previousAllocation = null;
			var currentAllocation = null;

			for (var i = 1; i < allocationSearch.length; i++) {

				previousAllocation = allocationSearch[i - 1].getId();
				currentAllocation = allocationSearch[i].getId();

				if (previousAllocation != currentAllocation) {
					allocation = allocationSearch[i];
					try {
						var record = nlapiLoadRecord('resourceallocation', currentAllocation);
						record.setFieldValue('custevent_ra_last_modified_by', allocation.getValue(
								'name', 'systemnotes'));

						var date1 = allocation.getValue('date', 'systemnotes');
						var t = date1.indexOf(' am') > -1 ? ' am' : ' pm';
						var k = date1.split(t);
						var date2 = k[0] + ":00" + t;

						record.setFieldValue('custevent_ra_last_modified_date', date2);
						nlapiSubmitRecord(record, false, true);
						nlapiLogExecution('debug', 'record', currentAllocation + '\n name : '
								+ allocation.getValue('name', 'systemnotes'));
					}
					catch (er) {
						nlapiLogExecution('error', 'allocation : ' + currentAllocation, er);
					}
				}
			}
		}
		nlapiLogExecution('debug', 'end', currentAllocation);
		return currentAllocation;
	}
	catch (err) {
		nlapiLogExecution('error', 'scheduled', err);
		return err.message;
	}
}

function formatDate(dates) {

	allocation.getValue('date', 'systemnotes')
}

function yieldScript(currentContext) {

	if (currentContext.getRemainingUsage() <= 100) {
		nlapiLogExecution('AUDIT', 'API Limit Exceeded');
		var state = nlapiYieldScript();

		if (state.status == constants.K_YIELD_FAILURE) {
			nlapiLogExecution('ERROR', 'Yield Script Failed', 'Reason : ' + state.reason
					+ ' / Size : ' + state.size);
			return false;
		}
		else if (state.status == constants.K_YIELD_RESUME) {
			nlapiLogExecution('AUDIT', 'Script Resumed');
		}
	}
}
