/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       31 March 2020     Gaurav Jaiswal
 *
 */

/**
 * @param {Object} dataIn Parameter object
 * @returns {Object} Output object
 */
 function getRESTlet(dataIn) {

	return {};
}

/**
 * @param {Object} dataIn Parameter object
 * @returns {Object} Output object
 */

function postRESTlet(dataIn) {

	try {
		
		//dataIn = {"lastUpdatedTimestamp":""}
        var response = new Response();
        //nlapiLogExecution('debug', 'dataIn', JSON.stringify(dataIn));
        var currentDateAndTime = sysDate();
		
		var receivedDate = dataIn.lastUpdatedTimestamp;
		//nlapiLogExecution('DEBUG', 'Current Date', 'receivedDate...' + receivedDate);
		nlapiLogExecution('DEBUG', 'Current Date', 'currentDateAndTime...' + currentDateAndTime);

		//var receivedDate = '7/12/2019 1:00 am';
		//Check if the timestamp is empty
		if (!_logValidation(receivedDate)) {          // for empty timestanp in this we will give whole data as a response
			var filters = new Array();
			filters = [
				
				["isinactive", "is", "F"]
				];
		//	nlapiLogExecution('debug', 'filters ', filters);
		} else {
			var filters = new Array();                             // this filter will provide the result within the current date and given date     
			filters = [
				
				
				["lastmodified", "within", receivedDate, currentDateAndTime]					
				];

		//	nlapiLogExecution('debug', 'filters ', filters);
		}
		
		// created search by grouping 
		var searchResults = searchRecord("customrecord_fuel_leadership",null,
				filters, 
				[
				  new nlobjSearchColumn("custrecord_fuel_leadership_employee"), 
               new nlobjSearchColumn("email","CUSTRECORD_FUEL_LEADERSHIP_EMPLOYEE",null),
               new nlobjSearchColumn("isinactive")
				]
				);		

		var employeeData = [];
        
        if (searchResults) {
			
			
			for(var i=0; i<searchResults.length; i++){    
				
				 var employee = {};
              employee.internalId = searchResults[i].getId();
                    employee.empInternalId = searchResults[i].getValue('custrecord_fuel_leadership_employee');
			        employee.empName = searchResults[i].getText('custrecord_fuel_leadership_employee');
                    employee.emailId = searchResults[i].getValue("email","CUSTRECORD_FUEL_LEADERSHIP_EMPLOYEE",null);
                    employee.isInactive = searchResults[i].getValue('isinactive');
			        employeeData.push(employee);
		}
		
		response.status = true;
	    response.timeStamp = currentDateAndTime;
		response.data = employeeData;

	   }
	} catch (err) {

		nlapiLogExecution('ERROR', 'postRESTlet', err);
		response.status = false;
		response.timeStamp = '';
		response.data = err;

	}
	return response;

}
//validate blank entries
function _logValidation(value) {
	if (value != null && value.toString() != null && value != '' && value != undefined && value.toString() != undefined && value != 'undefined' && value.toString() != 'undefined' && value.toString() != 'NaN' && value != NaN) {
		return true;
	} else {
		return false;
	}
}
//Get current date


//Returns timestamp
function timestamp() {
	var str = "";

	var currentTime = new Date();
	var hours = currentTime.getHours();
	var minutes = currentTime.getMinutes();
	var seconds = currentTime.getSeconds();
	var meridian = "";
	if (hours > 12) {
		meridian += "pm";
	} else {
		meridian += "am";
	}
	if (hours > 12) {

		hours = hours - 12;
	}
	if (minutes < 10) {
		minutes = "0" + minutes;
	}
	if (seconds < 10) {
		seconds = "0" + seconds;
	}
	//str += hours + ":" + minutes + ":" + seconds + " ";
	str += hours + ":" + minutes + " ";
	return str + meridian;
}

function Response() {
	this.status = "";
	this.timeStamp = "";
	this.data = "";

}

//Search function for results more than 1000
function searchRecord(recordType, savedSearch, arrFilters, arrColumns,
		filterExpression) {

	try {
		var search = null;

		// if a saved search is provided, load it and add the filters and
		// columns
		if (savedSearch) {
			search = nlapiLoadSearch(recordType, savedSearch);

			if (isArrayNotEmpty(arrFilters)) {
				search.addFilters(arrFilters);
			}

			if (isArrayNotEmpty(arrColumns)) {
				search.addColumns(arrColumns);
			}

			if (isArrayNotEmpty(filterExpression)) {
				search.setFilterExpression(filterExpression);
			}
		}
		// create a new search
		else {
			search = nlapiCreateSearch(recordType, arrFilters, arrColumns);
		}

		// run search
		var resultSet = search.runSearch();

		// iterate through the search and get all data 1000 at a time
		var searchResultCount = 0;
		var resultSlice = null;
		var searchResult = [];

		do {
			resultSlice = resultSet.getResults(searchResultCount,
					searchResultCount + 1000);

			if (resultSlice) {

				resultSlice.forEach(function(result) {

					searchResult.push(result);
					searchResultCount++;
				});
			}
		} while (isArrayNotEmpty(resultSlice) && resultSlice.length >= 1000);

		return searchResult;
	} catch (err) {
		nlapiLogExecution('ERROR', 'searchRecord', err);
		throw err;
	}
}

function isEmpty(value) {

	return value == null || value == "" || typeof(value) == undefined;
}

function isNotEmpty(value) {

	return !isEmpty(value);
}

function isArrayEmpty(argArray) {

	return !isArrayNotEmpty(argArray);
}

function isArrayNotEmpty(argArray) {

	return (isNotEmpty(argArray) && argArray.length > 0);
}
//function to format date object into NetSuite's mm/dd/yyyy format.
function formatNSDate(dateObj) {
	if (dateObj) {
		var nsFormatDate = dateObj.getFullYear() + '-' + addZeros(dateObj.getMonth() + 1, 2) + '-' + addZeros(dateObj.getDate(), 2);
		return nsFormatDate;
	}
	return null;
}

//function to add leading zeros on date parts.
function addZeros(num, len) {
	var str = num.toString();
	while (str.length < len) {
		str = '0' + str;
	}
	return str;
}