function beforeSubmit(type)
{
	try
	{
		var error_log = [];
		var project_id = nlapiGetFieldValue('entityid');
        if(type == 'create')
       {
		if(_logValidation(project_id))
		{
			var proID = project_id.split(' ');
			var projectID = proID[0];
			var len = projectID.length;
			if(len >= 9)
			{
				nlapiLogExecution('Debug','PROJECT ID VALUE',projectID);
				return true;
			}
			else
			{
				error_log.push({
					'Msg': "'Project ID is invalid. The ProjectID should be greater than or equal to 9 charcters.'"
				})
			}
		}
       }
		// if ((_logValidation(error_log)) && error_log.length > 0)
		if (error_log.length > 0)
		 {
        	   nlapiLogExecution('ERROR','We are in try id block');
			 throw nlapiCreateError('False', error_log, true);
		 }
	}
	catch(e)
	{
		nlapiLogExecution('ERROR','We are in catch block',e);
		throw e;
	}
}

function _logValidation(value) {
    if (value != null && value != '' && value != undefined && value.toString() != undefined && value.toString() != 'NaN' && value != NaN && value != 'undefined' && value != ' ') {
        return true;
    } else {
        return false;
    }
}