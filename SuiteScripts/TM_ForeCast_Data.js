/**
 * @param {nlobjRequest}
 *            request Request object
 * @param {nlobjResponse}
 *            response Response object
 * @returns {Void} Any output is written via response object
 */
function scheduled() {

	nlapiLogExecution('AUDIT', 'Report Run', '');

	try {
		

		 {
			 var context = nlapiGetContext();

				//var d_today = new Date();
			 	var d_today = '12/1/2017';
			 	d_today = nlapiStringToDate(d_today);
			 
				var error = false;
				var dataRow = [];
				var o_total_array = [];
				//Conversion Rates table
				var inr_to_usd = 0;
				var gbp_to_usd = 0;
				var eur_to_usd = 0;
				var a_conversion_rate_table = nlapiSearchRecord('customrecord_conversion_rate_sfdc',null,null,[new nlobjSearchColumn('custrecord_rate_inr_to_usd'),
				                                                              new nlobjSearchColumn('custrecord_rate_gbp_to_usd'),
				                                                              new nlobjSearchColumn('custrecord_rate_eur_to_usd'),
				                                                              new nlobjSearchColumn('internalid').setSort(true)]);
				if(a_conversion_rate_table)
					inr_to_usd = 1 / parseFloat(a_conversion_rate_table[0].getValue('custrecord_rate_inr_to_usd'));
					gbp_to_usd = a_conversion_rate_table[0].getValue('custrecord_rate_gbp_to_usd');
					eur_to_usd = a_conversion_rate_table[0].getValue('custrecord_rate_eur_to_usd');
				
				for (var i = 0; i < 2; i++) {
					var d_day = nlapiAddMonths(d_today, i);
					yieldScript(context);
					var d_month_start = nlapiAddDays(d_day, -1 * (d_day.getDate() - 1));
					var s_month_start = d_month_start.getMonth() + 1 + '/'
					        + d_month_start.getDate() + '/' + d_month_start.getFullYear();
					var d_month_end = nlapiAddDays(nlapiAddMonths(d_month_start, 1), -1);
					var s_month_end = d_month_end.getMonth() + 1 + '/'
					        + d_month_end.getDate() + '/' + d_month_end.getFullYear();
					try {
						var JSON_Obj = {};
						
						var a_data = getRevenueData(s_month_start, s_month_end,
						        d_month_start, d_month_end,inr_to_usd,gbp_to_usd,eur_to_usd);
						
						for(var indx=0;indx<a_data.length;indx++){
							var s_local = a_data[indx];
							o_total_array.push({
									
									 'Department': s_local.custpage_employee_department,
									 'ParentDepartment': s_local.custpage_employee_parent_department,
									 'ProjectID': s_local.custpage_project.split(' ')[0],
									 'ProjectName': s_local.custpage_project,
									 'CustomerName': s_local.custpage_customer,
									 'Amount': s_local.custpage_total_amount,
									 'ProjectCurrency': s_local.custpage_currency,
									 'MonthStart': s_month_start,
									 'MonthEnd': s_month_end
							});
							//o_total_array.push(JSON_Obj);
						}
						/*exportToXml(a_data, 'T', d_month_start.getMonth() + 1, d_month_end
						        .getFullYear());*/
						dataRow.push({
							'MonthStart': s_month_start,
							'MonthEnd': s_month_end,
							'Data': o_total_array
						});
					} catch (e) {
						nlapiLogExecution('ERROR', 'Error: ', e.message);
						error = true;
					}
				}
			/*var s_from_date = request.getParameter('custpage_from_date');
			var d_from_date = nlapiStringToDate(s_from_date, 'datetimetz');
			

			var s_to_date = request.getParameter('custpage_to_date');
			var d_to_date = nlapiStringToDate(s_to_date, 'datetimetz');

			var i_working_days = parseInt(calcBusinessDays(d_from_date,
			        d_to_date));


			var a_data = getRevenueData(s_from_date,
			        s_to_date, d_from_date, d_to_date);


			if (mode == 'excel') {
				exportToExcel(a_data, chk_show_ot_hours);
				return;// response.write('Test')
		}*/
		 }
	//	 return dataRow;
		down_excel_function(o_total_array);
		//nlapiLogExecution('debug', 'response', JSON.stringify(dataRow));

	} catch (err) {
		nlapiLogExecution('ERROR', 'main', err);
		 throw err;
	}
}
//Excel Function
function down_excel_function(dataRows)
{
	try
	{
		var strVar1= '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:ss="urn:schemas-microsoft-com:office:spreadsheet" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40">'+
		'<head>'+
		'<meta http-equiv=Content-Type content="text/html; charset=windows-1252"/>'+
        '<meta name=ProgId content=Excel.Sheet/>'+
        '<meta name=Generator content="Microsoft Excel 11"/>'+
        '<!--[if gte mso 9]><xml>'+
        '<x:excelworkbook>'+
        '<x:excelworksheets>'+
        '<x:excelworksheet=sheet1>'+
        '<x:name>** ESTIMATE FILE**</x:name>'+
        '<x:worksheetoptions>'+
        '<x:selected></x:selected>'+
        '<x:freezepanes></x:freezepanes>'+
        '<x:frozennosplit></x:frozennosplit>'+
        '<x:splithorizontal>** FROZEN ROWS + 1 **</x:splithorizontal>'+
        '<x:toprowbottompane>** FROZEN ROWS + 1 **</x:toprowbottompane>'+
        '<x:splitvertical>** FROZEN COLUMNS + 1 **</x:splitvertical>'+
        '<x:leftcolumnrightpane>** FROZEN COLUMNS + 1**</x:leftcolumnrightpane>'+
		    '<x:activepane>0</x:activepane>'+// 0
		    '<x:panes>'+
			'<x:pane>'+
			'<x:number>3</x:number>'+
			'</x:pane>'+
			'<x:pane>'+
		    '<x:number>1</x:number>'+
		    '</x:pane>'+
		    '<x:pane>'+
			'<x:number>2</x:number>'+
        '</x:pane>'+
        '<x:pane>'+
        '<x:number>0</x:number>'+//1
        '</x:pane>'+
        '</x:panes>'+
        '<x:protectcontents>False</x:protectcontents>'+
        '<x:protectobjects>False</x:protectobjects>'+
        '<x:protectscenarios>False</x:protectscenarios>'+
        '</x:worksheetoptions>'+
        '</x:excelworksheet>'+
		'</x:excelworksheets>'+
        '<x:protectstructure>False</x:protectstructure>'+
        '<x:protectwindows>False</x:protectwindows>'+
		'</x:excelworkbook>'+
        
        // '<style>'+
			
			//-------------------------------------
        '</x:excelworkbook>'+
        '</xml><![endif]-->'+
         '<style>'+
		'p.MsoFooter, li.MsoFooter, div.MsoFooter'+
		'{ margin:0in; margin-bottom:.0001pt; mso-pagination:widow-orphan; tab-stops:center 3.0in right 6.0in; font-size:12.0pt;}'+
		'<style>'+

		'<!-- /* Style Definitions */'+

		//'@page Section1'+
		//'{ size:8.5in 11.0in; margin:1.9cm 1.27cm 1.9cm 1.27cm; mso-header-margin:.5in; mso-footer-margin:.5in; mso-title-page:yes; mso-header: h1; mso-footer: f1; mso-first-header: fh1; mso-first-footer: ff1; mso-paper-source:0;}'+

		'div.Section1'+
		'{ page:Section1;}'+

		'table#hrdftrtbl'+
		'{ margin:0in 0in 0in 900in; width:1px; height:1px; overflow:hidden; font-family:Arial;}-->'+

		'</style>'+
		
		
		'</head>'+
		

		'<body>'+
		
		'<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">'
		
		
		var strVar2 = '';    
		strVar2 += "<table width=\"105%\">";
		strVar2 += "<tr>";
		strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \>Department</td>";
		strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \>ParentDepartment</td>";
		strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \>ProjectID</td>";
		strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \>ProjectName</td>";
		strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \>CustomerName</td>";
		strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \>Amount</td>";
		strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \>ProjectCurrency</td>";
		strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \>MonthStart</td>";
		strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \>MonthEnd</td>";
		
		strVar2 += "<\/tr>";
		
		if (dataRows)
		{
			nlapiLogExecution('audit','down_excel_function transcation length:- ',dataRows.length);
			for (var jk = 0; jk < dataRows.length; jk++)
			{
				
									
				nlapiLogExecution('audit','down_excel_function ProjectID:- ',dataRows[jk].ProjectID);
				strVar2 += "<tr>";
				strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \>" +dataRows[jk].Department+ "</td>";
				strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \>" +dataRows[jk].ParentDepartment+ "</td>";
				strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \>" +dataRows[jk].ProjectID+ "</td>";
				strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \>" +dataRows[jk].ProjectName+ "</td>";
				strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \>" +dataRows[jk].CustomerName+ "</td>";
				strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \>" +dataRows[jk].Amount+ "</td>";
				strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \>" +dataRows[jk].ProjectCurrency+ "</td>";
				strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \>" +dataRows[jk].MonthStart+ "</td>";
				strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \>" +dataRows[jk].MonthEnd+ "</td>";
				
				strVar2 += "<\/tr>";
			}
		}
		else
		{
			//NO RESULTS FOUND
		}
					
		strVar2 += "<\/table>";			
		strVar1 = strVar1 + strVar2;
		var file = nlapiCreateFile('TM_ForeCast_Report.xls', 'XMLDOC', strVar1);
		nlapiLogExecution('debug','file:- ',file);
		
		var curr_user = nlapiGetUser();
		var s_email = nlapiLookupField('employee', curr_user, 'email');
		//response.setContentType('XMLDOC','Vendor Bill Aging Report.xls');
		var a_emp_attachment = new Array();
		a_emp_attachment['entity'] = curr_user;
		nlapiSendEmail(442, 'deepak.srinivas@brillio.com', 'TM ForeCast Report', 'Please find the attachement of forecast report', null, null, a_emp_attachment, file, true, null, null);
		//response.write( file.getValue() );
		//window.close();
	}
	catch(err)
	{
		nlapiLogExecution('ERROR','ERROR MESSAGE:- ',err);
	}
}
//End
// Get Active Resource Allocations
function getResourceAllocations(d_start_date, d_end_date,
        o_holidays)
{

	nlapiLogExecution('AUDIT', 'getResourceAllocation Parameters', d_start_date
	        + ' - ' + d_end_date );

	var strExcludeHolidayEntries = '';
	if (o_holidays != null) {
		for ( var i_subsidiary in o_holidays) {
			for (var i = 0; i < o_holidays[i_subsidiary].length; i++) {
				var s_date = o_holidays[i_subsidiary][i].date.toJSON()
				        .substring(0, 10);
				strExcludeHolidayEntries += ' - CASE WHEN ({employee.subsidiary.id} = TO_NUMBER(\''
				        + i_subsidiary
				        + '\') AND TO_DATE(\''
				        + s_date
				        + '\', \'YYYY-MM-DD\') BETWEEN {startdate} AND {enddate}) THEN 1 ELSE 0 END ';
			}
		}
	}

	// Store resource allocations for project and employee
	var a_resource_allocations = new Array();

	// Get Resource allocations for this month
	var filters = new Array();
	filters[0] = new nlobjSearchFilter('custeventbstartdate', null,
	        'onorbefore', d_end_date);
	filters[1] = new nlobjSearchFilter('custeventbenddate', null, 'onorafter',
	        d_start_date);
	filters[2] = new nlobjSearchFilter('custeventrbillable', null, 'is', 'T');
	filters[3] = new nlobjSearchFilter('jobbillingtype', 'job', 'anyof', 'TM');
	//filters[4] = new nlobjSearchFilter('internalid', 'job', 'anyof', 71644);
	/*
	// department filtering
	if (isNotEmpty(a_department)) {
		filters[filters.length] = new nlobjSearchFilter('department',
		        'employee', 'anyof', a_department);
	}

	// project filtering
	if (isNotEmpty(i_project_filter)) {
		filters[filters.length] = new nlobjSearchFilter('project', null,
		        'anyof', i_project_filter);
	}

	// customer filtering
	if (isNotEmpty(i_customer_filter)) {
		filters[filters.length] = new nlobjSearchFilter('customer', 'job',
		        'anyof', i_customer_filter);
	}

	// vertical filtering
	if (isNotEmpty(i_vertical_filter)) {
		filters[filters.length] = new nlobjSearchFilter('custentity_vertical',
		        'job', 'anyof', i_vertical_filter);
	}
*/
	var columns = new Array();
	columns[0] = new nlobjSearchColumn('internalid', 'employee', 'group');
	columns[1] = new nlobjSearchColumn('project', null, 'group');
	columns[2] = new nlobjSearchColumn('custeventrbillable', null, 'group');
	columns[3] = new nlobjSearchColumn('custevent3', null, 'avg'); // st rate
	columns[4] = new nlobjSearchColumn('resource', null, 'group');
	columns[5] = new nlobjSearchColumn('percentoftime', null, 'avg');
	columns[6] = new nlobjSearchColumn('departmentnohierarchy', 'employee',
	        'group');
	columns[7] = new nlobjSearchColumn('subsidiary', 'employee', 'group');

	columns[8] = new nlobjSearchColumn('formulanumeric', null, 'sum');
	// Formula to restrict start date to current month
	var s_month_start_date = ' (CASE WHEN {custeventbstartdate} < TO_DATE(\''
	        + d_start_date.toJSON().substring(0, 10)
	        + '\', \'YYYY-MM-DD\') THEN TO_DATE(\''
	        + d_start_date.toJSON().substring(0, 10)
	        + '\', \'YYYY-MM-DD\') ELSE {custeventbstartdate} END) ';

	// Formula to restrict end date to current month
	var s_month_end_date = ' (CASE WHEN {custeventbenddate} > TO_DATE(\''
	        + d_end_date.toJSON().substring(0, 10)
	        + '\', \'YYYY-MM-DD\') THEN TO_DATE(\''
	        + d_end_date.toJSON().substring(0, 10)
	        + '\', \'YYYY-MM-DD\') ELSE {custeventbenddate} END) ';

	columns[8].setFormula('(' + s_month_end_date + ' - ' + s_month_start_date
	        + ') + 1 - 2 * FLOOR((' + s_month_end_date + ' - '
	        + s_month_start_date + ' + 1)/7) - (CASE WHEN TO_NUMBER(TO_CHAR('
	        + s_month_start_date + ', \'D\')) > TO_NUMBER(TO_CHAR('
	        + s_month_end_date
	        + ', \'D\')) THEN 2 ELSE 0 END) - (CASE WHEN TO_CHAR('
	        + s_month_start_date + ', \'D\') = \'1\' AND TO_CHAR('
	        + s_month_end_date
	        + ',\'D\') != \'7\' THEN 1 ELSE 0 END) - (CASE WHEN TO_CHAR('
	        + s_month_end_date + ', \'D\') = \'7\' AND TO_CHAR('
	        + s_month_start_date + ', \'D\') != \'1\' THEN 1 ELSE 0 END)'
	        + strExcludeHolidayEntries);// Calculate
	// Working
	// Days
	columns[9] = new nlobjSearchColumn('customer', 'job', 'group');
	columns[10] = new nlobjSearchColumn('internalid', null, 'count');
	columns[11] = new nlobjSearchColumn('custentity_project_currency', 'job',
	        'group');
	columns[12] = new nlobjSearchColumn('custentity_vertical', 'job', 'group');
	columns[13] = new nlobjSearchColumn('custentity_t_and_m_monthly', 'job',
	        'group');
	columns[14] = new nlobjSearchColumn('custevent_monthly_rate', null, 'group');
	columns[15] = new nlobjSearchColumn('formulatext', null, 'group');
	columns[15]
	        .setFormula('CASE WHEN INSTR({employee.department} , \' : \', 1) > 0 THEN SUBSTR({employee.department}, 1 , INSTR({employee.department} , \' : \', 1)) ELSE {employee.department} END');
	columns[16] = new nlobjSearchColumn('custentity_endcustomer', 'job',
	        'group');
	columns[0].setSort();
	columns[1].setSort();
	try {
		var search_results = searchRecord('resourceallocation', null, filters,
		        columns);
	} catch (e) {
		nlapiLogExecution('ERROR', 'Error', e.message);
	}

	if (search_results != null && search_results.length > 0) {
		for (var i_search_indx = 0; i_search_indx < search_results.length; i_search_indx++) {
			var i_project_id = search_results[i_search_indx]
			        .getValue(columns[1]);
			var s_project_name = search_results[i_search_indx]
			        .getText(columns[1]);
			var is_resource_billable = search_results[i_search_indx]
			        .getValue(columns[2]);
			var stRate = search_results[i_search_indx].getValue(columns[3]);
			var i_employee = search_results[i_search_indx].getValue(columns[0]);
			var s_employee_name = search_results[i_search_indx]
			        .getText(columns[4]);
			var i_percent_of_time = search_results[i_search_indx]
			        .getValue(columns[5]);
			var i_department_id = search_results[i_search_indx]
			        .getValue(columns[6]);
			var s_department = search_results[i_search_indx]
			        .getValue(columns[6]);
			var i_subsidiary = search_results[i_search_indx]
			        .getValue(columns[7]);
			var i_working_days = search_results[i_search_indx]
			        .getValue(columns[8]);
			var i_customer_id = search_results[i_search_indx]
			        .getValue(columns[9]);
			var s_customer = search_results[i_search_indx].getText(columns[9]);
			var i_count = search_results[i_search_indx].getValue(columns[10]);
			var i_currency = search_results[i_search_indx].getText(columns[11]);
			var i_vertical_id = search_results[i_search_indx]
			        .getValue(columns[11]);
			var s_vertical = search_results[i_search_indx].getText(columns[12]);
			var is_T_and_M_monthly = search_results[i_search_indx]
			        .getValue(columns[13]);
			var f_monthly_rate = search_results[i_search_indx]
			        .getValue(columns[14]);
			var s_parent_department = search_results[i_search_indx]
			        .getValue(columns[15]);
			var s_end_customer = search_results[i_search_indx]
			        .getText(columns[16]);
			a_resource_allocations[i_search_indx] = {
			    project_id : i_project_id,
			    'project_name' : s_project_name,
			    is_billable : is_resource_billable,
			    st_rate : stRate,
			    'employee' : i_employee,
			    'employee_name' : s_employee_name,
			    'percentoftime' : i_percent_of_time,
			    'department_id' : i_department_id,
			    'department' : s_department,
			    'holidays' : (o_holidays[i_subsidiary] == undefined ? []
			            : o_holidays[i_subsidiary]),
			    'workingdays' : i_working_days,
			    'customer_id' : i_customer_id,
			    'customer' : s_customer,
			    'count' : i_count,
			    'currency' : i_currency,
			    'vertical_id' : i_vertical_id,
			    'vertical' : s_vertical,
			    'monthly_billing' : is_T_and_M_monthly,
			    'monthly_rate' : f_monthly_rate,
			    'parent_department' : s_parent_department,
			    'end_customer' : s_end_customer == '- None -' ? ''
			            : s_end_customer
			};
		}
	} else {
		a_resource_allocations = null;
	}

	return a_resource_allocations;
}

// Return data from the search
function getData(d_start_date, d_end_date, a_billable_employees, o_holidays) {

	var not_submitted_array = getNotSubmitted(nlapiDateToString(d_start_date),
	        nlapiDateToString(d_end_date), a_billable_employees);

	//nlapiLogExecution('debug', 'not submitted data', JSON
	//        .stringify(not_submitted_array));

	var a_holidays = [];
	var strExcludeHolidayEntries = '';
	if (o_holidays != null) {
		for ( var i_subsidiary in o_holidays) {
			for (var i = 0; i < o_holidays[i_subsidiary].length; i++) {
				var s_date = o_holidays[i_subsidiary][i].date.toJSON()
				        .substring(0, 10);
				strExcludeHolidayEntries += ' OR ({employee.subsidiary.id} = TO_NUMBER(\''
				        + i_subsidiary
				        + '\') AND {date} = TO_DATE(\''
				        + s_date
				        + '\', \'YYYY-MM-DD\'))';
			}
		}
	}
	var filters = new Array();
	filters[0] = new nlobjSearchFilter('date', null, 'within', d_start_date,
	        d_end_date);
	if (a_billable_employees != null && a_billable_employees.length != 0) {
		filters[1] = new nlobjSearchFilter('employee', null, 'anyof',
		        a_billable_employees);
	}
	var columns = new Array();
	columns[0] = new nlobjSearchColumn('formuladate', null, 'count');// Calculate
	// submitted
	// days
	columns[0]
	        .setFormula('CASE WHEN TO_CHAR({date}, \'D\') = 1 OR TO_CHAR({date},\'D\') = 7 OR ({billable} = \'F\' AND {item.id} != \'Leave\' AND {item.id} != \'Holiday\' AND ({durationdecimal} != 0 OR {rate} = 0))'
	                + strExcludeHolidayEntries + ' THEN NULL ELSE {date} END');

	// Search Records
	var search_results = searchRecord('timeentry', 'customsearch_revenue_report_per_projec_2', filters, columns);

	var a_data = new Object();

	for (var i = 0; i < search_results.length; i++) {
		var o_data = new Object();

		columns = search_results[i].getAllColumns();

		o_data.employee_id = search_results[i].getValue(columns[0]);
		o_data.employee = search_results[i].getText(columns[0]);
		o_data.project_id = search_results[i].getValue(columns[1]);
		o_data.project = search_results[i].getText(columns[1]);
		o_data.billed_hours = search_results[i].getValue(columns[2]);
		o_data.approved_hours = search_results[i].getValue(columns[3]);
		o_data.submitted_hours = search_results[i].getValue(columns[4]);
		o_data.leave_hours = search_results[i].getValue(columns[5]);
		o_data.holiday_hours = search_results[i].getValue(columns[6]);
		o_data.billed_amount = search_results[i].getValue(columns[7]);
		o_data.approved_amount = search_results[i].getValue(columns[8]);
		o_data.submitted_amount = search_results[i].getValue(columns[9]);
		o_data.billed_hours_ot = search_results[i].getValue(columns[10]);
		o_data.approved_hours_ot = search_results[i].getValue(columns[11]);
		o_data.submitted_hours_ot = search_results[i].getValue(columns[12]);
		o_data.billed_amount_ot = search_results[i].getValue(columns[13]);
		o_data.approved_amount_ot = search_results[i].getValue(columns[14]);
		o_data.submitted_amount_ot = search_results[i].getValue(columns[15]);
		o_data.working_days_entered = parseInt(search_results[i].getValue(columns[16]));
		o_data.rejected_hours = search_results[i].getValue(columns[17]);//mani
		o_data.rejected_amount = search_results[i].getValue(columns[18]);//mani
		
		

		var job_id = search_results[i].getValue('internalid',
		        "customerProject", 'group');

		try {
			// nlapiLogExecution('debug', 'employee', 'E' + o_data.employee_id);
			// nlapiLogExecution('debug', 'project', 'P' + job_id);

			// nlapiLogExecution('debug', 'employee object', JSON
			// .stringify(not_submitted_array['E' + o_data.employee_id]));

			// nlapiLogExecution('debug', 'project object',
			// JSON
			// .stringify(not_submitted_array['E'
			// + o_data.employee_id]['P' + job_id]));

			var not_submitted_days = not_submitted_array['E'
			        + o_data.employee_id]['P' + job_id];

			// nlapiLogExecution('debug', 'not_submitted_days',
			// not_submitted_days);

			// if (!not_submitted_days || isNaN(not_submitted_days)) {
			// not_submitted_days = 0;
			// }

			o_data.custpage_not_submitted_hours = 8 * not_submitted_days;
			// nlapiLogExecution('debug', 'o_data.custpage_not_submitted_hours
			// ',
			// o_data.custpage_not_submitted_hours);
		} catch (e) {
			nlapiLogExecution('error', 'not submitted part', e);
		}

		if (a_data[o_data.employee_id] == undefined) {
			a_data[o_data.employee_id] = new Object();
		}

		a_data[o_data.employee_id][o_data.project_id] = o_data;

	}

	return a_data;
}

// Calculate Business Days
function calcBusinessDays(d_startDate, d_endDate) { // input given as Date

	// objects
	var startDate = new Date(d_startDate.getTime());
	var endDate = new Date(d_endDate.getTime());
	// Validate input
	if (endDate < startDate)
		return 0;

	// Calculate days between dates
	var millisecondsPerDay = 86400 * 1000; // Day in milliseconds
	startDate.setHours(0, 0, 0, 1); // Start just after midnight
	endDate.setHours(23, 59, 59, 999); // End just before midnight
	var diff = endDate - startDate; // Milliseconds between datetime objects
	var days = Math.ceil(diff / millisecondsPerDay);

	// Subtract two weekend days for every week in between
	var weeks = Math.floor(days / 7);
	var days = days - (weeks * 2);

	// Handle special cases
	var startDay = startDate.getDay();
	var endDay = endDate.getDay();

	// Remove weekend not previously removed.
	if (startDay - endDay > 1)
		days = days - 2;

	// Remove start day if span starts on Sunday but ends before Saturday
	if (startDay == 0 && endDay != 6)
		days = days - 1

		// Remove end day if span ends on Saturday but starts after Sunday
	if (endDay == 6 && startDay != 0)
		days = days - 1

	return days;

}
// Search Sub Departments
function searchSubDepartments(i_department_id) {

	var s_department_name = nlapiLookupField('department', i_department_id,
	        'name');

	var filters = new Array();
	if (s_department_name.indexOf(' : ') == -1) {
		filters[0] = new nlobjSearchFilter('formulatext', null, 'is',
		        s_department_name);
		filters[0]
		        .setFormula('TRIM(CASE WHEN INSTR({name} , \' : \', 1) > 0 THEN SUBSTR({name}, 1 , INSTR({name} , \' : \', 1)) ELSE {name} END)');
	} else {
		filters[0] = new nlobjSearchFilter('internalid', null, 'anyof',
		        i_department_id);
	}
	var columns = new Array();
	columns[0] = new nlobjSearchColumn('internalid');

	var search_sub_departments = nlapiSearchRecord('department', null, filters,
	        columns);

	var a_sub_departments = new Array();

	for (var i = 0; i < search_sub_departments.length; i++) {
		a_sub_departments
		        .push(search_sub_departments[i].getValue('internalid'));
	}

	return a_sub_departments;
}
// Get Holidays
function getHolidays(d_start_date, d_end_date) {

	var filters = new Array();
	filters[0] = new nlobjSearchFilter('custrecord_date', null, 'within',
	        d_start_date, d_end_date);

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_date',null,'group');
	columns[1] = new nlobjSearchColumn('custrecordsubsidiary',null,'group');

	var search_holidays = nlapiSearchRecord('customrecord_holiday', null,
	        filters, columns);

	var a_holidays = new Object();

	for (var i = 0; search_holidays != null && i < search_holidays.length; i++) {
		var i_subsidiary = search_holidays[i].getValue('custrecordsubsidiary',null,'group');
		var o_holiday = {
			'date' : nlapiStringToDate(search_holidays[i]
			        .getValue('custrecord_date',null,'group'))
		};
		if (a_holidays[i_subsidiary] == undefined) {
			a_holidays[i_subsidiary] = [ o_holiday ];
		} else {
			a_holidays[i_subsidiary].push(o_holiday);
		}
	}

	return a_holidays;
}
// Export To Excel
function exportToExcel(a_data, str_show_ot_hours) {

	var count = a_data.length;
	var globalArray = new Array();
	var o_columns = new Object();
	o_columns['custpage_employee'] = 'Employee';
	o_columns['custpage_employee_department'] = 'Department';
	o_columns['custpage_project'] = 'Project';
	o_columns['custpage_customer'] = 'Customer';
	o_columns['custpage_billed_hours'] = 'Billed Hours';
	o_columns['custpage_billed_amount'] = 'Billed Amount';
	o_columns['custpage_approved_hours'] = 'Approved Hours';
	o_columns['custpage_approved_amount'] = 'Approved Amount';
	o_columns['custpage_submitted_hours'] = 'Submitted Hours';
	o_columns['custpage_submitted_amount'] = 'Submitted Amount';
	o_columns['custpage_not_submitted_hours'] = 'Not Submitted Hours';
	o_columns['custpage_not_submitted_amount'] = 'Not Submitted Amount';
	o_columns['custpage_rejected_hours']        ='RejectedHours'; //mani 
	o_columns['custpage_rejected_amount']        ='RejectedAmount'; //mani 
	if (str_show_ot_hours == 'T') {
		o_columns['custpage_billed_hours_ot'] = 'Billed OT Hours';
		o_columns['custpage_billed_amount_ot'] = 'Billed OT Amount';
		o_columns['custpage_approved_hours_ot'] = 'Approved OT Hours';
		o_columns['custpage_approved_amount_ot'] = 'Approved OT Amount';
		o_columns['custpage_submitted_hours_ot'] = 'Submitted OT Hours';
		o_columns['custpage_submitted_amount_ot'] = 'Submitted OT Amount';
	}
	o_columns['custpage_total_amount'] = 'Total Amount';
	o_columns['custpage_leave_hours'] = 'Leave Hours';
	o_columns['custpage_holiday_hours'] = 'Holidays';
	o_columns['custpage_rate'] = 'Billable Rate';

	o_columns['custpage_percent'] = 'Percent Allocated';
	o_columns['custpage_end_customer'] = 'End Customer';

	// Enter the headings
	var s_line = '';
	for ( var s_column in o_columns) {
		s_line += o_columns[s_column] + ',';
	}
	globalArray.push(s_line);
	for (var i = 0; i < count; i++) {
		s_line = '\n';
		var a_line = new Array();
		for ( var s_column in o_columns) {
			//var s_value = a_data[i][s_column].toString();
			var s_value = a_data[i][s_column];
			//s_value = s_value.toString().replace(/[|]/g, " ");
			//s_value = s_value.replace(/[,]/g, " ");
			a_line.push(s_value);// o_sublist.getLineItemValue(s_column, i));
			// // [s_column]
			// + ',';
		}
		//s_line += a_line.toString();
		s_line += a_line;
		globalArray.push(s_line);
	}

	var fileName = 'Revenue report from TS'
	var Datetime = new Date();
	var CSVName = fileName + " - " + Datetime + '.csv';
	var file = nlapiCreateFile(CSVName, 'CSV', globalArray.toString());

	// nlapiLogExecution('DEBUG', 'suitlet_Po_ClosedStatus', ' PDF Name--> ');
	response.setContentType('CSV', CSVName);

	// nlapiLogExecution('DEBUG', 'suitlet_Po_ClosedStatus', ' PDF Name--> ');
	response.write(file.getValue());

	for ( var s_column in o_columns) {

	}
}
// Export to XML
function exportToXml(a_data, str_show_ot_hours, s_month, s_year) {

	var count = a_data.length;
	var globalArray = new Array();
	var o_columns = new Object();
	o_columns['custpage_employee'] = 'Employee';
	o_columns['custpage_employee_department'] = 'Department';
	o_columns['custpage_employee_parent_department'] = 'ParentDepartment';
	o_columns['custpage_project'] = 'Project';
	o_columns['custpage_customer'] = 'Customer';
	o_columns['custpage_billed_hours'] = 'BilledHours';
	o_columns['custpage_billed_amount'] = 'BilledAmount';
	o_columns['custpage_approved_hours'] = 'ApprovedHours';
	o_columns['custpage_approved_amount'] = 'ApprovedAmount';
	o_columns['custpage_submitted_hours'] = 'SubmittedHours';
	o_columns['custpage_submitted_amount'] = 'SubmittedAmount';
	o_columns['custpage_not_submitted_hours'] = 'NotSubmittedHours';
	o_columns['custpage_not_submitted_amount'] = 'NotSubmittedAmount';
	o_columns['custpage_rejected_hours'] = 'Rejected Hours';//mani
	o_columns['custpage_rejected_amount'] = 'Rejected Amount';//mani
	
	if (str_show_ot_hours == 'T') {
		o_columns['custpage_billed_hours_ot'] = 'BilledOTHours';
		o_columns['custpage_billed_amount_ot'] = 'BilledOTAmount';
		o_columns['custpage_approved_hours_ot'] = 'ApprovedOTHours';
		o_columns['custpage_approved_amount_ot'] = 'ApprovedOTAmount';
		o_columns['custpage_submitted_hours_ot'] = 'SubmittedOTHours';
		o_columns['custpage_submitted_amount_ot'] = 'SubmittedOTAmount';
	}
	o_columns['custpage_total_amount'] = 'TotalAmount';
	o_columns['custpage_leave_hours'] = 'LeaveHours';
	o_columns['custpage_holiday_hours'] = 'Holidays';
	o_columns['custpage_rate'] = 'BillableRate';
	o_columns['custpage_percent'] = 'PercentAllocated';
	o_columns['custpage_t_and_m_monthly'] = 'BillingType';
	o_columns['custpage_vertical'] = 'Vertical';
	o_columns['custpage_end_customer'] = 'EndCustomer';
	var xml = '<data>';
	// Enter the headings
	var s_line = '';
	for ( var s_column in o_columns) {
		s_line += o_columns[s_column] + ',';
	}
	globalArray.push(s_line);
	for (var i = 0; i < count; i++) {
		xml += '<record>';
		var a_line = new Array();
		for ( var s_column in o_columns) {
			var s_value = a_data[i][s_column].toString();// o_sublist.getLineItemValue(s_column,
			// i).toString();
			s_value = s_value.replace(/[|]/g, " ");
			s_value = s_value.replace(/[,]/g, " ");
			xml += '<' + o_columns[s_column] + '>' + nlapiEscapeXML(s_value)
			        + '</' + o_columns[s_column] + '>';
			// a_line.push(s_value);//o_sublist.getLineItemValue(s_column, i));
			// // [s_column]
			// + ',';
		}
		xml += '</record>';
		/*
		 * s_line += a_line.toString(); globalArray.push(s_line);
		 */
	}
	xml += '</data>';

	// Search if data already exists
	var filters = new Array();
	filters[0] = new nlobjSearchFilter('custrecord_rev_rep_xml_data_month',
	        null, 'anyof', s_month);
	filters[1] = new nlobjSearchFilter('custrecord_rev_rep_xml_data_year',
	        null, 'equalto', s_year);

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('internalid').setSort(true);
	var search_results = nlapiSearchRecord(
	        'customrecord_revenue_report_xml_data', null, filters, columns);

	var s_last_update_xml = '';

	if (search_results) {
		var i_last_update_internal_id = search_results[0]
		        .getValue('internalid');

		s_last_update_xml = nlapiLookupField(
		        'customrecord_revenue_report_xml_data',
		        i_last_update_internal_id,
		        'custrecord_rev_rep_xml_data_xml_data');
	}

	if (s_last_update_xml != xml) {
		nlapiLogExecution('AUDIT', 'i_last_update_internal_id: '
		        + i_last_update_internal_id + ', stored xml length: '
		        + s_last_update_xml.length + ', xml: ' + xml.length, s_month
		        + ' - ' + s_year);
		// Store only if there is any change from the last update
		var rec = nlapiCreateRecord('customrecord_revenue_report_xml_data');

		rec.setFieldValue('custrecord_rev_rep_xml_data_xml_data', xml);
		rec.setFieldValue('custrecord_rev_rep_xml_data_month', s_month);
		rec.setFieldValue('custrecord_rev_rep_xml_data_year', s_year);
		try {
			nlapiSubmitRecord(rec);
		} catch (e) {
			nlapiLogExecution('ERROR', 'Error:', e.message);
		}
	}

	return xml;

}
// Get Department List
function getDepartmentList() {

	try {
		var currentEmployeeId = nlapiGetUser();
		var permissionSearch = nlapiSearchRecord(
		        'customrecord_report_permission_list', null, [
		                new nlobjSearchFilter('custrecord_rpl_employee', null,
		                        'anyof', [ currentEmployeeId ]),
		                new nlobjSearchFilter('isinactive', null, 'is', 'F'),
		                new nlobjSearchFilter('custrecord_rpl_has_dept_access',
		                        null, 'is', 'T') ]);

		if (isArrayNotEmpty(permissionSearch)) {

			var permission_record = nlapiLoadRecord(
			        'customrecord_report_permission_list', permissionSearch[0]
			                .getId());
			var a_department_value_list = permission_record
			        .getFieldValues('custrecord_rpl_department');
			var a_department_text_list = permission_record
			        .getFieldTexts('custrecord_rpl_department');
			var a_department_list = [];

			if (isArrayEmpty(a_department_value_list)) {
				throw "No department specified";
			}

			var count_department = a_department_value_list.length;

			if (count_department > 1) {
				a_department_list.push({
				    Value : '',
				    Text : '',
				    IsSelected : true
				});
			}

			for (var i = 0; i < count_department; i++) {
				a_department_list.push({
				    Value : a_department_value_list[i],
				    Text : a_department_text_list[i],
				    IsSelected : false
				});
			}

			if (count_department == 1) {
				a_department_list[0].IsSelected = true;
			}

			return a_department_list;
		} else {
			throw "You are not authorized to access this page";
		}
	} catch (err) {
		nlapiLogExecution('ERROR', 'getDepartmentList', err);
		throw err;
	}
}
// Get Vertical List
function getVerticalList() {

	try {
		var currentEmployeeId = nlapiGetUser();
		var permissionSearch = nlapiSearchRecord(
		        'customrecord_report_permission_list', null, [
		                new nlobjSearchFilter('custrecord_rpl_employee', null,
		                        'anyof', [ currentEmployeeId ]),
		                new nlobjSearchFilter('isinactive', null, 'is', 'F'),
		                new nlobjSearchFilter(
		                        'custrecord__rpl_has_vertical_access', null,
		                        'is', 'T') ]);

		if (isArrayNotEmpty(permissionSearch)) {

			var permission_record = nlapiLoadRecord(
			        'customrecord_report_permission_list', permissionSearch[0]
			                .getId());
			var a_vertical_value_list = permission_record
			        .getFieldValues('custrecord_rpl_vertical');
			var a_vertical_text_list = permission_record
			        .getFieldTexts('custrecord_rpl_vertical');
			var a_vertical_list = [];

			if (isArrayEmpty(a_vertical_value_list)) {
				throw "No Verticals Specified";
			}

			var count_vertical = a_vertical_value_list.length;

			if (count_vertical > 1) {
				a_vertical_list.push({
				    Value : '',
				    Text : '',
				    IsSelected : true
				});
			}

			for (var i = 0; i < count_vertical; i++) {
				a_vertical_list.push({
				    Value : a_vertical_value_list[i],
				    Text : a_vertical_text_list[i],
				    IsSelected : false
				});
			}

			if (count_vertical == 1) {
				a_vertical_list[0].IsSelected = true;
			}

			return a_vertical_list;
		} else {
			throw "You are not authorized to access this page";
		}
	} catch (err) {
		nlapiLogExecution('ERROR', 'getVerticalList', err);
		throw err;
	}
}

function getRevenueData(s_from_date, s_to_date, d_from_date,
        d_to_date,inr_to_usd,gbp_to_usd,eur_to_usd)
{

	var o_holidays = getHolidays(d_from_date, d_to_date);
	var i_working_days = parseInt(calcBusinessDays(d_from_date, d_to_date));
	var chk_show_ot_hours = 'T';
	// Resource Allocation Data
	var a_resource_allocation = getResourceAllocations(d_from_date, d_to_date,
	       o_holidays);

	var i_current_employee = null;

	var a_res_alloc_for_each_employee = new Object();

	var a_exception_employees = new Array();

	var a_billable_employees = new Array();

	var a_currency_rate = new Object();

	for (var i_res_alloc_indx = 0; a_resource_allocation != null
	        && i_res_alloc_indx < a_resource_allocation.length; i_res_alloc_indx++) {
		var o_resource_allocation = a_resource_allocation[i_res_alloc_indx];
		var i_employee_id = o_resource_allocation.employee;
		var i_project_id = o_resource_allocation.project_id.toString();
		if (a_res_alloc_for_each_employee[i_employee_id] == undefined) {
			a_res_alloc_for_each_employee[i_employee_id] = new Object();
			a_res_alloc_for_each_employee[i_employee_id][i_project_id] = o_resource_allocation;
			a_billable_employees.push(i_employee_id);
		} else {
			a_res_alloc_for_each_employee[i_employee_id][i_project_id] = o_resource_allocation;

			// a_exception_employees.push(i_employee_id);
		}

		/*
		 * if(o_resource_allocation.start_date > d_from_date ||
		 * o_resource_allocation.end_date < d_to_date) {
		 * a_exception_employees.push(i_employee_id); }
		 */
	}

	var a_search_data = getData(d_from_date, d_to_date, a_billable_employees,
	        o_holidays);

	var a_data = new Array();
	for (var i_list_indx = 0; a_billable_employees != null
	        && i_list_indx < a_billable_employees.length; i_list_indx++) {
		var i_employee_id = a_billable_employees[i_list_indx];

		if (a_exception_employees.indexOf(i_employee_id) == -1) {
			var a_res_allocations_for_this_employee = a_res_alloc_for_each_employee[i_employee_id];
			for ( var i_project_id in a_res_allocations_for_this_employee) {
				var a_row_data = new Object();

				var o_search_result = null;

				if (a_search_data[i_employee_id] != undefined) {
					o_search_result = a_search_data[i_employee_id][i_project_id];
				}

				var o_resource_allocation = a_res_alloc_for_each_employee[i_employee_id][i_project_id];
				var s_conversion_rate_date = s_from_date;
				var d_today = new Date();
				if (d_today < d_from_date) {
					s_conversion_rate_date = null;
				}
				/*if (a_currency_rate[o_resource_allocation.currency] == undefined) {
					a_currency_rate[o_resource_allocation.currency] = nlapiExchangeRate(
					        o_resource_allocation.currency, 'USD',
					        s_conversion_rate_date);
				}*/
				//Conversion factor
				var f_currency_conversion = 1;
				if(o_resource_allocation.currency =='USD')
					f_currency_conversion = 1;
				if(o_resource_allocation.currency =='INR')
					f_currency_conversion = inr_to_usd;
				if(o_resource_allocation.currency =='EUR')
					f_currency_conversion.currency = eur_to_usd;
				if(o_resource_allocation.currency =='GBP')
					f_currency_conversion = gbp_to_usd;
					
				//var f_currency_conversion = parseFloat(a_currency_rate[o_resource_allocation.currency]);
				a_row_data['custpage_vertical'] = o_resource_allocation.vertical;
				a_row_data['custpage_vertical_id'] = o_resource_allocation.vertical_id;
				a_row_data['custpage_employee'] = o_resource_allocation.employee_name;
				a_row_data['custpage_employee_id'] = o_resource_allocation.employee;
				a_row_data['custpage_employee_department'] = o_resource_allocation.department;
				a_row_data['custpage_employee_department_id'] = o_resource_allocation.department_id;
				a_row_data['custpage_employee_parent_department'] = o_resource_allocation.parent_department;
				a_row_data['custpage_project'] = o_resource_allocation.project_name;
				a_row_data['custpage_project_id'] = o_resource_allocation.project_id;
				a_row_data['custpage_customer'] = o_resource_allocation.customer;
				a_row_data['custpage_customer_id'] = o_resource_allocation.customer_id;
				a_row_data['custpage_holiday_hours'] = o_resource_allocation.holidays.length
				        .toString();
				a_row_data['custpage_rate'] = (o_resource_allocation.monthly_billing == 'T') ? (o_resource_allocation.monthly_rate / (8.0 * i_working_days))
				        : o_resource_allocation.st_rate;
				if (o_search_result) {
					a_row_data['custpage_billed_hours'] = o_search_result.billed_hours;
					a_row_data['custpage_billed_hours_ot'] = o_search_result.billed_hours_ot;
					a_row_data['custpage_approved_hours'] = o_search_result.approved_hours;
					a_row_data['custpage_approved_hours_ot'] = o_search_result.approved_hours_ot;
					a_row_data['custpage_submitted_hours'] = o_search_result.submitted_hours;
					a_row_data['custpage_submitted_hours_ot'] = o_search_result.submitted_hours_ot;
					a_row_data['custpage_rejected_hours'] = o_search_result.rejected_hours; //Mani - Dated 20 MAR 17
					var i_not_submitted_hours = o_resource_allocation.workingdays
					        - o_search_result.working_days_entered;
					if (i_not_submitted_hours < 0) {
						i_not_submitted_hours = 0.0;
					}
					a_row_data['custpage_not_submitted_hours'] = o_search_result.custpage_not_submitted_hours;
					// (i_not_submitted_hours
					// * 8
					// * parseFloat(o_resource_allocation.percentoftime) /
					// 100.0)
					// .toString();
					a_row_data['custpage_leave_hours'] = o_search_result.leave_hours;
					a_row_data['custpage_billed_amount'] = parseFloat(
					        o_search_result.billed_amount
					                * f_currency_conversion).toFixed(2);
				a_row_data['custpage_billed_amount_ot'] = parseFloat(
					        o_search_result.billed_amount_ot
					                * f_currency_conversion).toFixed(2);
					a_row_data['custpage_approved_amount'] = parseFloat(
					        o_search_result.approved_amount
					                * f_currency_conversion).toFixed(2);
					a_row_data['custpage_approved_amount_ot'] = parseFloat(
					        o_search_result.approved_amount_ot
					                * f_currency_conversion).toFixed(2);
					a_row_data['custpage_submitted_amount'] = parseFloat(
					        o_search_result.submitted_amount
					                * f_currency_conversion).toFixed(2);
					a_row_data['custpage_submitted_amount_ot'] = parseFloat(
					        o_search_result.submitted_amount_ot
					                * f_currency_conversion).toFixed(2);
					a_row_data['custpage_rejected_amount'] = parseFloat(
					        o_search_result.rejected_amount
					                * f_currency_conversion).toFixed(2);	//Mani - Dated 20 MAR 17			
					 a_row_data['custpage_holiday_hours'] =
					 o_search_result.holiday_hours;

				} else {
					a_row_data['custpage_billed_hours'] = '0';
					a_row_data['custpage_approved_hours'] = '0';
					a_row_data['custpage_submitted_hours'] = '0';
					a_row_data['custpage_billed_hours_ot'] = '0';
					a_row_data['custpage_approved_hours_ot'] = '0';
					a_row_data['custpage_submitted_hours_ot'] = '0';
					a_row_data['custpage_not_submitted_hours'] = ((o_resource_allocation.workingdays)
					        * 8
					        * parseFloat(o_resource_allocation.percentoftime) / 100.0)
					        .toString();
					a_row_data['custpage_leave_hours'] = '0';
					// a_row_data['custpage_holiday_hours'] = '0';

					a_row_data['custpage_billed_amount'] = parseFloat(0.0)
					        .toFixed(2);// *
					// parseFloat(a_row_data['custpage_rate'])).toFixed(2);
					a_row_data['custpage_billed_amount_ot'] = parseFloat(0.0)
					        .toFixed(2);
					a_row_data['custpage_approved_amount'] = parseFloat(0.0)
					        .toFixed(2);// *
					// parseFloat(a_row_data['custpage_rate'])).toFixed(2);
					a_row_data['custpage_approved_amount_ot'] = parseFloat(0.0)
					        .toFixed(2);
					a_row_data['custpage_submitted_amount'] = parseFloat(0.0)
					        .toFixed(2);// *
					// parseFloat(a_row_data['custpage_rate'])).toFixed(2);
					a_row_data['custpage_submitted_amount_ot'] = parseFloat(0.0)
					        .toFixed(2);
				}
				a_row_data['custpage_count'] = parseFloat(o_resource_allocation.count);
				a_row_data['custpage_allocated_days'] = parseFloat(o_resource_allocation.workingdays);
				a_row_data['custpage_percent'] = o_resource_allocation.percentoftime
				        .toString();
				a_row_data['custpage_not_submitted_amount'] = parseFloat(
				        parseFloat(a_row_data['custpage_not_submitted_hours'])
				                * parseFloat(a_row_data['custpage_rate'])
				                * (parseFloat(a_row_data['custpage_percent']) / 100.0)
				                * f_currency_conversion).toFixed(2);
				a_row_data['custpage_total_amount'] = parseFloat(a_row_data['custpage_billed_amount'])
				        + parseFloat(a_row_data['custpage_approved_amount'])
				        + parseFloat(a_row_data['custpage_submitted_amount'])
				        + parseFloat(a_row_data['custpage_not_submitted_amount']);
				if (chk_show_ot_hours == 'T') {
					a_row_data['custpage_total_amount'] += parseFloat(a_row_data['custpage_billed_amount_ot'])
					        + parseFloat(a_row_data['custpage_approved_amount_ot'])
					        + parseFloat(a_row_data['custpage_submitted_amount_ot']);
				}
				a_row_data['custpage_total_amount'] = a_row_data['custpage_total_amount']
				        .toFixed(2);
				a_row_data['custpage_currency'] = o_resource_allocation.currency;
				a_row_data['custpage_t_and_m_monthly'] = o_resource_allocation.monthly_billing == 'T' ? 'T & M Monthly'
				        : 'T & M';
				a_row_data['custpage_end_customer'] = o_resource_allocation.end_customer;
				a_data.push(a_row_data);// [i_list_indx] = a_row_data;

			}

		}
	}
	nlapiLogExecution('AUDIT', 'Number of Records: ', a_data.length);
	return a_data;
}
// Restlet to store data
function getRESTlet(dataIn) {

	var o = new Object();
	nlapiLogExecution('AUDIT', 'Error: ', (nlapiGetContext()).getUser());
	var s_from_date = dataIn.custpage_from_date;
	var d_from_date = nlapiStringToDate(s_from_date, 'datetimetz');

	var s_to_date = dataIn.custpage_to_date;
	var d_to_date = nlapiStringToDate(s_to_date, 'datetimetz');
	var a_data = getRevenueData(s_from_date, s_to_date, d_from_date,
	        d_to_date, '', null, null, null);
	exportToXml(a_data, 'T', nlapiAddDays(d_from_date, 10).getMonth() + 1,
	        nlapiAddDays(d_from_date, 10).getFullYear())
	return o;
}
function storeDataForYear() {

	var context = nlapiGetContext();

	var d_today = new Date();

	var error = false;

	for (var i = -3; i < 12; i++) {
		var d_day = nlapiAddMonths(d_today, i);
		yieldScript(context);
		var d_month_start = nlapiAddDays(d_day, -1 * (d_day.getDate() - 1));
		var s_month_start = d_month_start.getMonth() + 1 + '/'
		        + d_month_start.getDate() + '/' + d_month_start.getFullYear();
		var d_month_end = nlapiAddDays(nlapiAddMonths(d_month_start, 1), -1);
		var s_month_end = d_month_end.getMonth() + 1 + '/'
		        + d_month_end.getDate() + '/' + d_month_end.getFullYear();
		try {
			var a_data = getRevenueData(s_month_start, s_month_end,
			        d_month_start, d_month_end);
			exportToXml(a_data, 'T', d_month_start.getMonth() + 1, d_month_end
			        .getFullYear());
		} catch (e) {
			nlapiLogExecution('ERROR', 'Error: ', e.message);
			error = true;
		}
		if (error == false) {
			return 'T';
		} else {
			return 'F';
		}
		nlapiLogExecution('AUDIT', 'Remaining usage: ', context
		        .getRemainingUsage());
	}
}

function yieldScript(currentContext) {

	if (currentContext.getRemainingUsage() <= 100) {
		nlapiLogExecution('AUDIT', 'API Limit Exceeded');
		var state = nlapiYieldScript();

		if (state.status == "FAILURE") {
			nlapiLogExecution('ERROR', 'Yield Script Failed', 'Reason : '
			        + state.reason + ' / Size : ' + state.size);
			return false;
		} else if (state.status == "RESUME") {
			nlapiLogExecution('AUDIT', 'Script Resumed');
		}
	}
}