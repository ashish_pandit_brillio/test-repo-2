/**
 * Send Email to all employees about timesheet block / unblock for month end
 * activity
 * 
 * Version Date Author Remarks 1.00 02 May 2016 Nitish Mishra
 * 
 */

function scheduled(type) {
	try {
		var context = nlapiGetContext();
		var deploymentId = context.getDeploymentId();

		var employeeSearch = searchRecord('employee', null,
		        [
		        // new nlobjSearchFilter('internalid', null, 'anyof', [ '9673',
		        // '5803' ]),
		        new nlobjSearchFilter('custentity_employee_inactive', null,
		                'is', 'F'),new nlobjSearchFilter('custentity_implementationteam', null, 'is', 'T') ], [ new nlobjSearchColumn('firstname'),
		                new nlobjSearchColumn('email') ]);

		if (isArrayNotEmpty(employeeSearch)) {
			nlapiLogExecution('debug', 'total emp', employeeSearch.length);
			var mailData = null, firstName = null, emailId = null, employeeId = null;

			for (var index = 0; index < employeeSearch.length; index++) {
				firstName = employeeSearch[index].getValue('firstname');
				emailId = employeeSearch[index].getValue('email');
				employeeId = employeeSearch[index].getId();
				yieldScript(context);

				if (isNotEmpty(firstName) && isNotEmpty(emailId)) {

					switch (deploymentId) {

						case "customdeploy_sch_mp_notify_block":
							mailData = getTimesheetBlockMailTemplate(firstName);
						break;

						case "customdeploy_sch_mp_notify_unblock":
							mailData = getTimesheetUnblockedMailTemplate(firstName);
						break;

						default:
							throw "Wrong deployment id";
					}

					nlapiSendEmail('442', emailId, mailData.MailSubject,
					        mailData.MailBody, null, null, {
						        entity : employeeId
					        });
				} else {
					nlapiLogExecution('debug', 'failed sending',
					        employeeSearch[index].getId());
				}
			}
		}
	} catch (err) {
		nlapiLogExecution('error', 'scheduled', err);
	}
}

function yieldScript(currentContext) {

	if (currentContext.getRemainingUsage() <= 100) {
		nlapiLogExecution('AUDIT', 'API Limit Exceeded');
		var state = nlapiYieldScript();

		if (state.status == "FAILURE") {
			nlapiLogExecution('ERROR', 'Yield Script Failed', 'Reason : '
			        + state.reason + ' / Size : ' + state.size);
			return false;
		} else if (state.status == "RESUME") {
			nlapiLogExecution('AUDIT', 'Script Resumed');
		}
	}
}