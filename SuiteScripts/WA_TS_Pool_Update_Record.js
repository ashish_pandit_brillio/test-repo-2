/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       01 Sep 2015     nitish.mishra
 *	2.00	  24 Mar 2020	  praveena			changed the sublist ids and fetch data from timesheets line level instead of fetching from subrecord
 */

/**
 * @returns {Void} Any or no return value
 */
function workflowAction() {
	try {
		var poolId = nlapiGetRecordId();
		var poolRecord = nlapiLoadRecord('customrecord_ts_quick_approval_pool',
		        poolId);

// Changed the structure TO FETCH THE VALUES FROM SUBLIST:  CHANGED BY PRAVEENA 
		// load the timesheet record
		var timesheetRecord = nlapiLoadRecord('timesheet', poolRecord.getFieldValue('custrecord_qap_timesheet'));
		var weekDays=['timebill0','timebill1','timebill2','timebill3','timebill4','timebill5','timebill6'];//replaced timebill array added by praveena
		var linecount = timesheetRecord.getLineItemCount('timeitem');
		var st = 0;
		var ot = 0;
		var leave = 0;
		var holiday = 0;
		var hours = null;
		var item = null;
		for (var linenum = 1; linenum <= linecount; linenum++) {
			timesheetRecord.selectLineItem('timeitem', linenum);

			// loop day wise
			for (var day_indx = 0; day_indx < 7; day_indx++) {
				var timeEntryView = timesheetRecord.getCurrentLineItemValue('timeitem',weekDays[day_indx]);

				if (timeEntryView) {
					hours = parseHoursToInt(timesheetRecord.getCurrentLineItemValue('timeitem','hours'+day_indx));
					item = timesheetRecord.getCurrentLineItemValue('timeitem','item');

					if (item == '2425') {
						ot += hours;
					} else if (item == '2479') {
						leave += hours;
					} else if (item == '2480') {
						holiday += hours;
					} else {
						st += hours;
					}
				}
			}
		}
//commented code by praveena

	/*	var weekDays = [ 'sunday', 'monday', 'tuesday', 'wednesday',
		        'thursday', 'friday', 'saturday' ];

		var linecount = timesheetRecord.getLineItemCount('timegrid');
		var st = 0;
		var ot = 0;
		var leave = 0;
		var holiday = 0;
		var hours = null;
		var item = null;

		for (var linenum = 1; linenum <= linecount; linenum++) {
			timesheetRecord.selectLineItem('timegrid', linenum);

			// loop day wise
			for (var day_indx = 0; day_indx < 7; day_indx++) {
				var timeEntryView = timesheetRecord
				        .viewCurrentLineItemSubrecord('timegrid',
				                weekDays[day_indx]);

				if (timeEntryView) {
					hours = parseHoursToInt(timeEntryView
					        .getFieldValue('hours'));
					item = timeEntryView.getFieldValue('item');

					if (item == '2425') {
						ot += hours;
					} else if (item == '2479') {
						leave += hours;
					} else if (item == '2480') {
						holiday += hours;
					} else {
						st += hours;
					}
				}
			}
		}
		*/

		poolRecord
		        .setFieldValue('custrecord_qap_st_hours', parseIntToHours(st));
		poolRecord
		        .setFieldValue('custrecord_qap_ot_hours', parseIntToHours(ot));
		poolRecord.setFieldValue('custrecord_qap_leave_hours',
		        parseIntToHours(leave));
		poolRecord.setFieldValue('custrecord_qap_holiday_hours',
		        parseIntToHours(holiday));

		nlapiSubmitRecord(poolRecord);
		nlapiLogExecution('debug', 'record updated');
	} catch (err) {
		nlapiLogExecution('error', 'workflowAction', err);
	}
}

function parseHoursToInt(hours) {
	var a = hours.split(":");
	var h = parseInt(a[0]);
	var m = parseInt(a[1]) / 60;
	return h + m;
}

function parseIntToHours(n) {
	n = parseFloat(n);
	var d = n % 1;
	var h = n / 1;
	var m = 0;
	if (d > 0) {
		m = d * 60;
		m = parseInt(m);

		if (m > 59) {
			h += (m / 60);
			m = m % 60;
		}

		m = m < 10 ? ('0' + m) : m;
	}
	m = m == '0' ? '00' : m;
	h = parseInt(h);
	h = h < 10 ? ('0' + h) : h;
	h = h == '0' ? '00' : h;

	return h + ":" + m;
}