/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       03 May 2017     deepak.srinivas
 *
 */

/**
 * @param {Object} dataIn Parameter object
 * @returns {Object} Output object
 */
function getRESTlet(dataIn) {
	
	return {};
}

/**
 * @param {Object} dataIn Parameter object
 * @returns {Object} Output object
 */
function postRESTlet(dataIn) {
	
	try{
		var response = new Response();
		//nlapiLogExecution('debug', 'dataIn', JSON.stringify(dataIn));
		var current_date = nlapiDateToString(new Date());
		var currentDate = sysDate();
		var currentTime = timestamp(); // returns the time stamp in HH:MM:SS
	    var currentDateAndTime = currentDate + ' ' + currentTime;
		//Log for current date
		nlapiLogExecution('DEBUG', 'Current Date', 'current_date...' + current_date);
	
		
		var receivedDate = dataIn.LastInstaceDate;
		nlapiLogExecution('DEBUG', 'Current Date', 'receivedDate...' + receivedDate);
		//var receivedDate = '7/12/2017 4:00 am';
		var filters = [];
		filters.push(new nlobjSearchFilter('lastmodifieddate',null,'within',receivedDate,currentDateAndTime));
		filters.push(new nlobjSearchFilter('jobtype',null,'anyof','2'));
		
		var cols = [];
		cols.push(new nlobjSearchColumn('internalid'));
		cols.push(new nlobjSearchColumn('entityid'));
		cols.push(new nlobjSearchColumn('jobname'));
		cols.push(new nlobjSearchColumn('companyname','customer'));
		cols.push(new nlobjSearchColumn('status'));
		cols.push(new nlobjSearchColumn('startdate'));
		cols.push(new nlobjSearchColumn('enddate'));
		cols.push(new nlobjSearchColumn('custentity_sowdate')); //SOW Date 
		cols.push(new nlobjSearchColumn('custentity_project_allocation_category'));
		cols.push(new nlobjSearchColumn('comments'));
		cols.push(new nlobjSearchColumn('custentity_deliverymodel'));
		cols.push(new nlobjSearchColumn('custentity_clientpartner'));
		cols.push(new nlobjSearchColumn('custentity_projectmanager'));
		cols.push(new nlobjSearchColumn('custentity_deliverymanager'));
		cols.push(new nlobjSearchColumn('custentity_verticalhead'));
		cols.push(new nlobjSearchColumn('subsidiary'));
		cols.push(new nlobjSearchColumn('custentity_location'));
		cols.push(new nlobjSearchColumn('custentity_region'));
		cols.push(new nlobjSearchColumn('custentity_endcustomer'));
		cols.push(new nlobjSearchColumn('custentity_practice'));
		cols.push(new nlobjSearchColumn('custentity_project_currency'));
		cols.push(new nlobjSearchColumn('custentity_entitygeo'));
		cols.push(new nlobjSearchColumn('custentity_worklocation'));
		cols.push(new nlobjSearchColumn('jobbillingtype'));
		cols.push(new nlobjSearchColumn('custentity_t_and_m_monthly'));
		cols.push(new nlobjSearchColumn('billingschedule')); //fxrate
		cols.push(new nlobjSearchColumn('fxrate'));
		cols.push(new nlobjSearchColumn('jobprice'));
		cols.push(new nlobjSearchColumn('estrevenue')); //Estimated Rev
		cols.push(new nlobjSearchColumn('estimatedtimeoverride')); //Estimated Work PM Email		
		cols.push(new nlobjSearchColumn('title','custentity_projectmanager'));
		cols.push(new nlobjSearchColumn('email','custentity_projectmanager'));
		cols.push(new nlobjSearchColumn('firstname','custentity_projectmanager'));
		cols.push(new nlobjSearchColumn('lastname','custentity_projectmanager'));
		cols.push(new nlobjSearchColumn('custentity_fusion_empid','custentity_projectmanager'));
		cols.push(new nlobjSearchColumn('email','custentity_deliverymanager'));
		cols.push(new nlobjSearchColumn('email','custentity_clientpartner'));
		cols.push(new nlobjSearchColumn('firstname','custentity_deliverymanager'));
		cols.push(new nlobjSearchColumn('lastname','custentity_deliverymanager'));
		cols.push(new nlobjSearchColumn('custrecord_practicehead','custentity_practice')); //custentity_project_currency
		cols.push(new nlobjSearchColumn('custentity_project_currency'));
		cols.push(new nlobjSearchColumn('custentity_projectcity'));
		cols.push(new nlobjSearchColumn('custentity_location')); //jobtype
		cols.push(new nlobjSearchColumn('jobtype'));
		cols.push(new nlobjSearchColumn("custentity_customerid")) /// added on 17 jan 2022 by Shravan
		
		var searchResults = searchRecord('job',null,filters,cols);
		var JSON = {};
		var dataRow = [];
		
	if(searchResults){
			
		for(var i=0;i<searchResults.length;i++){ //searchResults.length
		var i_practiceHead = searchResults[i].getValue('custrecord_practicehead','custentity_practice');
		
		var pratice_head_email = null;
		var pratice_head_firstname =null;
		var pratice_head_middlename = null;
		var pratice_head_lastname = null;
		var pratice_head_emp_ID = null;
		
		if(i_practiceHead){
		var filt = [];
		filt.push(new nlobjSearchFilter('internalid',null,'anyof',i_practiceHead));
		
		var col = [];
		col.push(new nlobjSearchColumn('email'));
		col.push(new nlobjSearchColumn('firstname'));
		col.push(new nlobjSearchColumn('middlename'));
		col.push(new nlobjSearchColumn('lastname'));
		col.push(new nlobjSearchColumn('custentity_fusion_empid'));
		
		var practiceHead_search = searchRecord('employee', null, filt, col);
		var pratice_head_full_name = null;
		if(_logValidation(practiceHead_search)){
			 pratice_head_email = practiceHead_search[0].getValue('email');
			 pratice_head_firstname = practiceHead_search[0].getValue('firstname');
			 pratice_head_middlename = practiceHead_search[0].getValue('middlename');
			 pratice_head_lastname = practiceHead_search[0].getValue('lastname');
			 pratice_head_emp_ID = practiceHead_search[0].getValue('custentity_fusion_empid');
			if(pratice_head_firstname)
				pratice_head_full_name = pratice_head_firstname;
				
			if(pratice_head_middlename)
				pratice_head_full_name = pratice_head_full_name + ' ' + pratice_head_middlename;
				
			if(pratice_head_lastname)
				pratice_head_full_name = pratice_head_full_name + ' ' + pratice_head_lastname;
			
		}
	}	
		var PM_Full_Name = null;
		var PM_firstname = searchResults[i].getValue('firstname','custentity_projectmanager');
		var PM_middlename = searchResults[i].getValue('middlename','custentity_projectmanager');
		var PM_lastname = searchResults[i].getValue('lastname','custentity_projectmanager');
		
		if(PM_firstname)
			PM_Full_Name = PM_firstname;
			
		if(PM_middlename)
			PM_Full_Name = PM_Full_Name + ' ' + PM_middlename;
			
		if(PM_lastname)
			PM_Full_Name = PM_Full_Name + ' ' + PM_lastname;
		//PM Full Name
		var DM_Full_Name = null;
		var DM_firstname = searchResults[i].getValue('firstname','custentity_deliverymanager');
		var DM_middlename = searchResults[i].getValue('middlename','custentity_deliverymanager');
		var DM_lastname = searchResults[i].getValue('lastname','custentity_deliverymanager');
		var DM_EMP_ID = searchResults[i].getText('custentity_deliverymanager');
		if(DM_EMP_ID){
			DM_EMP_ID = DM_EMP_ID.split('-');
			DM_EMP_ID = DM_EMP_ID[0];
		}
		
		if(PM_firstname)
			DM_Full_Name = DM_firstname;
			
		if(DM_middlename)
			DM_Full_Name = DM_Full_Name + ' ' + DM_middlename;
			
		if(DM_lastname)
			DM_Full_Name = DM_Full_Name + ' ' + DM_lastname;
		
		var startdate = searchResults[i].getValue('startdate');
		if(_logValidation(startdate)){
		startdate = nlapiStringToDate(startdate);
		startdate = formatNSDate(startdate);
		if(startdate)
		startdate = startdate +' '+'12:00:00';
	
		//nlapiLogExecution('DEBUG','startdate',startdate);
		}
		var endDate = searchResults[i].getValue('enddate');
		if(_logValidation(endDate)){
		endDate = nlapiStringToDate(endDate);
		endDate = formatNSDate(endDate);
		if(endDate)
		endDate = endDate +' '+'12:00:00';
		}
		var SowDate = searchResults[i].getValue('custentity_sowdate');
		if(_logValidation(SowDate)){
		SowDate = nlapiStringToDate(SowDate);
		SowDate = formatNSDate(SowDate);
		if(SowDate)
		SowDate = SowDate +' '+'12:00:00';
		}
		JSON = {
				InternalID: searchResults[i].getValue('internalid'),
				ProjectID: searchResults[i].getValue('entityid'),
				ProjectName: searchResults[i].getValue('jobname'),
				Customer: searchResults[i].getValue('companyname','customer'),
				Customer_ID:searchResults[i].getValue('custentity_customerid'),
				Status: searchResults[i].getText('status'),
				StartDate: startdate,
				EndDate: endDate,
				SowDate: SowDate,
				AllocationCategory: searchResults[i].getText('custentity_project_allocation_category'),
				ProjectCurrency: searchResults[i].getText('custentity_project_currency'),
				Comments: searchResults[i].getValue('comments'),
				DelieveryHead: searchResults[i].getText('custentity_deliverymodel'),
				ClientPartner: searchResults[i].getText('custentity_clientpartner'),
				ProjectManager: searchResults[i].getText('custentity_projectmanager'),
				DelieveryManager: searchResults[i].getText('custentity_deliverymanager'),
				VerticalHead: searchResults[i].getText('custentity_verticalhead'),
				Subsidiary: searchResults[i].getText('subsidiary'),
				Location: searchResults[i].getText('custentity_location'),
				Region: searchResults[i].getText('custentity_region'),
				EndCustomer: searchResults[i].getText('custentity_endcustomer'),
				ExecutingPractice: searchResults[i].getText('custentity_practice'),
				ProjectCurrency: searchResults[i].getText('custentity_project_currency'),
				Geo: searchResults[i].getText('custentity_entitygeo'),
				WorkLocation: searchResults[i].getText('custentity_worklocation'),
				BillingType: searchResults[i].getText('jobbillingtype'),
				TandM: searchResults[i].getValue('custentity_t_and_m_monthly'),
				BillingSchedule: searchResults[i].getText('billingschedule'),
				ExchangeRate: searchResults[i].getValue('fxrate'),
				ProjectValue: searchResults[i].getValue('jobprice'),
				ProjectCity: searchResults[i].getText('custentity_projectcity'),
				ProjectLocation: searchResults[i].getText('custentity_location'),
				EstimatedRevenue: searchResults[i].getValue('estrevenue'),
				EstimatedWork: searchResults[i].getValue('estimatedtimeoverride'),
				WorkLocation: searchResults[i].getText('custentity_worklocation'),
				PMDesignation: searchResults[i].getValue('title','custentity_projectmanager'),
				PMEmail: searchResults[i].getValue('email','custentity_projectmanager'),
				PMEMPID: searchResults[i].getValue('custentity_fusion_empid','custentity_projectmanager'),
				PMName: PM_Full_Name,
				DMName: DM_Full_Name,
				DMEmail: searchResults[i].getValue('email','custentity_deliverymanager'),
				DMEMPID: DM_EMP_ID,
				CPEmail: searchResults[i].getValue('email','custentity_clientpartner'),
				PracticeHead: searchResults[i].getText('custrecord_practicehead','custentity_practice'),
				PracticeHeadEmail: pratice_head_email,
				PracticeHeadFullName: pratice_head_full_name,
				PracticeHeadEmpID: pratice_head_emp_ID,
				ProjectType: searchResults[i].getText('jobtype')
				
		}
		dataRow.push(JSON);
			}
		}
		response.Data = dataRow;
		response.Status = true;
	}
	catch (err) {
		nlapiLogExecution('ERROR', 'postRESTlet', err);
		response.Data = err;
		response.Status = false;
	}
	//nlapiLogExecution('debug', 'response', JSON.stringify(response));
	return response;
	
}
//validate blank entries
function _logValidation(value) 
{
 if(value != null && value.toString() != null && value != '' && value != undefined && value.toString() != undefined && value != 'undefined' && value.toString() != 'undefined'&& value.toString() != 'NaN' && value != NaN) 
 {
  return true;
 }
 else 
 { 
  return false;
 }
}
//Get current date
function sysDate() {
	var date = new Date();
	var tdate = date.getDate();
	var month = date.getMonth() + 1; // jan = 0
	var year = date.getFullYear();
	return currentDate = month + '/' + tdate + '/' + year;
	}

//Returns timestamp
function timestamp() {
	var str = "";

	var currentTime = new Date();
	var hours = currentTime.getHours();
	var minutes = currentTime.getMinutes();
	var seconds = currentTime.getSeconds();
	var meridian = "";
	if (hours > 12) {
	    meridian += "pm";
	} else {
	    meridian += "am";
	}
	if (hours > 12) {

	    hours = hours - 12;
	}
	if (minutes < 10) {
	    minutes = "0" + minutes;
	}
	if (seconds < 10) {
	    seconds = "0" + seconds;
	}
	//str += hours + ":" + minutes + ":" + seconds + " ";
	str += hours + ":" + minutes +" ";
	return str + meridian;
	}

//Search function for results more than 1000
function searchRecord(recordType, savedSearch, arrFilters, arrColumns,
        filterExpression)
{

	try {
		var search = null;

		// if a saved search is provided, load it and add the filters and
		// columns
		if (savedSearch) {
			search = nlapiLoadSearch(recordType, savedSearch);

			if (isArrayNotEmpty(arrFilters)) {
				search.addFilters(arrFilters);
			}

			if (isArrayNotEmpty(arrColumns)) {
				search.addColumns(arrColumns);
			}

			if (isArrayNotEmpty(filterExpression)) {
				search.setFilterExpression(filterExpression);
			}
		}
		// create a new search
		else {
			search = nlapiCreateSearch(recordType, arrFilters, arrColumns);
		}

		// run search
		var resultSet = search.runSearch();

		// iterate through the search and get all data 1000 at a time
		var searchResultCount = 0;
		var resultSlice = null;
		var searchResult = [];

		do {
			resultSlice = resultSet.getResults(searchResultCount,
			        searchResultCount + 1000);

			if (resultSlice) {

				resultSlice.forEach(function(result) {

					searchResult.push(result);
					searchResultCount++;
				});
			}
		} while (isArrayNotEmpty(resultSlice) && resultSlice.length >= 1000);

		return searchResult;
	} catch (err) {
		nlapiLogExecution('ERROR', 'searchRecord', err);
		throw err;
	}
}
function isEmpty(value) {

	return value == null || value == "" || typeof (value) == undefined;
}

function isNotEmpty(value) {

	return !isEmpty(value);
}

function isArrayEmpty(argArray) {

	return !isArrayNotEmpty(argArray);
}

function isArrayNotEmpty(argArray) {

	return (isNotEmpty(argArray) && argArray.length > 0);
}
//function to format date object into NetSuite's mm/dd/yyyy format.
function formatNSDate(dateObj){
  if(dateObj){ 
    var nsFormatDate=dateObj.getFullYear()+'-'+addZeros(dateObj.getMonth()+1,2)+'-'+addZeros(dateObj.getDate(),2);
    return nsFormatDate;
  }
  return null;
}

//function to add leading zeros on date parts.
function addZeros(num,len){
  var str=num.toString();
  while(str.length<len){str='0'+str;}
  return str;
}