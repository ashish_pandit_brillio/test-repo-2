function getHourlyApprovedData(provisionMasterId, arrExchangeRate) {
	try {
		var provisionEntrySearch = searchRecord('customrecord_provision_entry',
		        null, [
		                new nlobjSearchFilter('isinactive', null, 'is', 'F'),
		                new nlobjSearchFilter('custrecord_pe_employee', null,
		                        'noneof', '@NONE@'),
		                new nlobjSearchFilter('custrecord_pe_provision_master',
		                        null, 'anyof', provisionMasterId),
		                new nlobjSearchFilter('custrecord_pe_is_monthly', null,
		                        'is', 'F') ], [
		                new nlobjSearchColumn(
		                        'custrecord_pe_provision_from_date', null,
		                        'group'),
		                new nlobjSearchColumn(
		                        'custrecord_pe_provision_to_date', null,
		                        'group'),
		                new nlobjSearchColumn('custrecord_pe_employee', null,
		                        'group'),
		                new nlobjSearchColumn('custrecord_pe_project', null,
		                        'group'),
		                new nlobjSearchColumn('custrecord_pe_project_name',
		                        null, 'group'),
		                new nlobjSearchColumn('custrecord_pe_customer', null,
		                        'group'),
		                new nlobjSearchColumn('custrecord_pe_currency', null,
		                        'group'),
		                new nlobjSearchColumn('custrecord_pe_subsidiary', null,
		                        'group'),
		                new nlobjSearchColumn('custrecord_pe_practice', null,
		                        'group'),
		                new nlobjSearchColumn('custrecord_pe_vertical', null,
		                        'group'),
		                new nlobjSearchColumn('custrecord_pe_region', null,
		                        'group'),
		                new nlobjSearchColumn('custrecord_pe_bill_rate', null,
		                        'group'),
		                new nlobjSearchColumn('custrecord_pe_hour_approved',
		                        null, 'sum'),
		                new nlobjSearchColumn(
		                        'custrecord_pe_hourly_amt_approved', null,
		                        'sum') ]);

		var data = [];

		if (provisionEntrySearch) {

			provisionEntrySearch
			        .forEach(function(entry) {
				        var amount = parseFloat(entry.getValue(
				                'custrecord_pe_hourly_amt_approved', null,
				                'sum'));
				        var exchangeRate = parseFloat((parseFloat(arrExchangeRate[entry
				                .getText('custrecord_pe_currency', null,
				                        'group')])).toFixed(2));
				        var convertedAmount = parseFloat((amount * exchangeRate)
				                .toFixed(2));

				        if (amount == 0) {
					        return;
				        }

				        data
				                .push({
				                    fromdate : entry
				                            .getValue(
				                                    'custrecord_pe_provision_from_date',
				                                    null, 'group'),
				                    todate : entry.getValue(
				                            'custrecord_pe_provision_to_date',
				                            null, 'group'),
				                    employee : entry.getText(
				                            'custrecord_pe_employee', null,
				                            'group'),
				                    project : entry.getValue(
				                            'custrecord_pe_project_name', null,
				                            'group'),
				                    customer : entry.getText(
				                            'custrecord_pe_customer', null,
				                            'group'),
				                    practice : entry.getText(
				                            'custrecord_pe_practice', null,
				                            'group'),
				                    vertical : entry.getText(
				                            'custrecord_pe_vertical', null,
				                            'group'),
				                    location : entry.getText(
				                            'custrecord_pe_region', null,
				                            'group'),
				                    employeeId : entry.getValue(
				                            'custrecord_pe_employee', null,
				                            'group'),
				                    projectId : entry.getValue(
				                            'custrecord_pe_project', null,
				                            'group'),
				                    customerId : entry.getValue(
				                            'custrecord_pe_customer', null,
				                            'group'),
				                    practiceId : entry.getValue(
				                            'custrecord_pe_practice', null,
				                            'group'),
				                    verticalId : entry.getValue(
				                            'custrecord_pe_vertical', null,
				                            'group'),
				                    locationId : entry.getValue(
				                            'custrecord_pe_region', null,
				                            'group'),
				                    billrate : entry.getValue(
				                            'custrecord_pe_bill_rate', null,
				                            'group'),
				                    hours : entry.getValue(
				                            'custrecord_pe_hour_approved',
				                            null, 'sum'),
				                    amount : amount,
				                    currency : entry.getText(
				                            'custrecord_pe_currency', null,
				                            'group'),
				                    currencyValue : entry.getValue(
				                            'custrecord_pe_currency', null,
				                            'group'),
				                    amountconverted : convertedAmount,
				                    newcurrency : 'INR' // <== remove this using
				                // master record
				                });
			        });
		}

		return data;
	} catch (err) {
		nlapiLogExecution('ERROR', 'getHourlyApprovedData', err);
		throw err;
	}
}

function getHourlySubmittedData(provisionMasterId, arrExchangeRate) {
	try {
		var provisionEntrySearch = searchRecord('customrecord_provision_entry',
		        null, [
		                new nlobjSearchFilter('isinactive', null, 'is', 'F'),
		                new nlobjSearchFilter('custrecord_pe_employee', null,
		                        'noneof', '@NONE@'),
		                new nlobjSearchFilter('custrecord_pe_provision_master',
		                        null, 'anyof', provisionMasterId),
		                new nlobjSearchFilter('custrecord_pe_is_monthly', null,
		                        'is', 'F') ], [
		                new nlobjSearchColumn(
		                        'custrecord_pe_provision_from_date', null,
		                        'group'),
		                new nlobjSearchColumn(
		                        'custrecord_pe_provision_to_date', null,
		                        'group'),
		                new nlobjSearchColumn('custrecord_pe_employee', null,
		                        'group'),
		                new nlobjSearchColumn('custrecord_pe_project', null,
		                        'group'),
		                new nlobjSearchColumn('custrecord_pe_project_name',
		                        null, 'group'),
		                new nlobjSearchColumn('custrecord_pe_customer', null,
		                        'group'),
		                new nlobjSearchColumn('custrecord_pe_currency', null,
		                        'group'),
		                new nlobjSearchColumn('custrecord_pe_subsidiary', null,
		                        'group'),
		                new nlobjSearchColumn('custrecord_pe_practice', null,
		                        'group'),
		                new nlobjSearchColumn('custrecord_pe_vertical', null,
		                        'group'),
		                new nlobjSearchColumn('custrecord_pe_region', null,
		                        'group'),
		                new nlobjSearchColumn('custrecord_pe_bill_rate', null,
		                        'group'),
		                new nlobjSearchColumn('custrecord_pe_hour_submitted',
		                        null, 'sum'),
		                new nlobjSearchColumn(
		                        'custrecord_pe_hourly_amt_submitted', null,
		                        'sum') ]);

		var data = [];

		if (provisionEntrySearch) {

			provisionEntrySearch.forEach(function(entry) {
				var amount = parseFloat(entry.getValue(
				        'custrecord_pe_hourly_amt_submitted', null, 'sum'));
				var exchangeRate = parseFloat((parseFloat(arrExchangeRate[entry
				        .getText('custrecord_pe_currency', null, 'group')]))
				        .toFixed(2));
				var convertedAmount = parseFloat((amount * exchangeRate)
				        .toFixed(2));

				if (amount == 0) {
					return;
				}

				data
				        .push({
				            fromdate : entry.getValue(
				                    'custrecord_pe_provision_from_date', null,
				                    'group'),
				            todate : entry.getValue(
				                    'custrecord_pe_provision_to_date', null,
				                    'group'),
				            employee : entry.getText('custrecord_pe_employee',
				                    null, 'group'),
				            project : entry
				                    .getValue('custrecord_pe_project_name',
				                            null, 'group'),
				            customer : entry.getText('custrecord_pe_customer',
				                    null, 'group'),
				            practice : entry.getText('custrecord_pe_practice',
				                    null, 'group'),
				            vertical : entry.getText('custrecord_pe_vertical',
				                    null, 'group'),
				            location : entry.getText('custrecord_pe_region',
				                    null, 'group'),
				            employeeId : entry.getValue(
				                    'custrecord_pe_employee', null, 'group'),
				            projectId : entry.getValue('custrecord_pe_project',
				                    null, 'group'),
				            customerId : entry.getValue(
				                    'custrecord_pe_customer', null, 'group'),
				            practiceId : entry.getValue(
				                    'custrecord_pe_practice', null, 'group'),
				            verticalId : entry.getValue(
				                    'custrecord_pe_vertical', null, 'group'),
				            locationId : entry.getValue('custrecord_pe_region',
				                    null, 'group'),
				            billrate : entry.getValue(
				                    'custrecord_pe_bill_rate', null, 'group'),
				            hours : entry
				                    .getValue('custrecord_pe_hour_submitted',
				                            null, 'sum'),
				            amount : amount,
				            currency : entry.getText('custrecord_pe_currency',
				                    null, 'group'),
				            currencyValue : entry.getValue(
				                    'custrecord_pe_currency', null, 'group'),
				            amountconverted : convertedAmount,
				            newcurrency : 'INR' // <== remove this using
				        // master record
				        });
			});
		}

		return data;
	} catch (err) {
		nlapiLogExecution('ERROR', 'getHourlySubmittedData', err);
		throw err;
	}
}

function getHourlyNotSubmittedData(provisionMasterId, arrExchangeRate) {
	try {
		var provisionEntrySearch = searchRecord(
		        'customrecord_provision_entry',
		        null,
		        [
		                new nlobjSearchFilter('isinactive', null, 'is', 'F'),
		                new nlobjSearchFilter('custrecord_pe_employee', null,
		                        'noneof', '@NONE@'),
		                new nlobjSearchFilter('custrecord_pe_provision_master',
		                        null, 'anyof', provisionMasterId),
		                new nlobjSearchFilter('custrecord_pe_is_monthly', null,
		                        'is', 'F') ],
		        [
		                new nlobjSearchColumn(
		                        'custrecord_pe_provision_from_date', null,
		                        'group'),
		                new nlobjSearchColumn(
		                        'custrecord_pe_provision_to_date', null,
		                        'group'),
		                new nlobjSearchColumn('custrecord_pe_employee', null,
		                        'group'),
		                new nlobjSearchColumn('custrecord_pe_project', null,
		                        'group'),
		                new nlobjSearchColumn('custrecord_pe_project_name',
		                        null, 'group'),
		                new nlobjSearchColumn('custrecord_pe_customer', null,
		                        'group'),
		                new nlobjSearchColumn('custrecord_pe_currency', null,
		                        'group'),
		                new nlobjSearchColumn('custrecord_pe_subsidiary', null,
		                        'group'),
		                new nlobjSearchColumn('custrecord_pe_practice', null,
		                        'group'),
		                new nlobjSearchColumn('custrecord_pe_vertical', null,
		                        'group'),
		                new nlobjSearchColumn('custrecord_pe_region', null,
		                        'group'),
		                new nlobjSearchColumn('custrecord_pe_bill_rate', null,
		                        'group'),
		                new nlobjSearchColumn(
		                        'custrecord_pe_hour_not_submitted', null, 'sum'),
		                new nlobjSearchColumn(
		                        'custrecord_pe_hourly_amt_not_submitted', null,
		                        'sum') ]);

		var data = [];

		if (provisionEntrySearch) {

			provisionEntrySearch
			        .forEach(function(entry) {
				        var amount = parseFloat(entry.getValue(
				                'custrecord_pe_hourly_amt_not_submitted', null,
				                'sum'));
				        var exchangeRate = parseFloat((parseFloat(arrExchangeRate[entry
				                .getText('custrecord_pe_currency', null,
				                        'group')])).toFixed(2));
				        var convertedAmount = parseFloat((amount * exchangeRate)
				                .toFixed(2));

				        if (amount == 0) {
					        return;
				        }

				        data.push({
				            fromdate : entry.getValue(
				                    'custrecord_pe_provision_from_date', null,
				                    'group'),
				            todate : entry.getValue(
				                    'custrecord_pe_provision_to_date', null,
				                    'group'),
				            employee : entry.getText('custrecord_pe_employee',
				                    null, 'group'),
				            project : entry
				                    .getValue('custrecord_pe_project_name',
				                            null, 'group'),
				            customer : entry.getText('custrecord_pe_customer',
				                    null, 'group'),
				            practice : entry.getText('custrecord_pe_practice',
				                    null, 'group'),
				            vertical : entry.getText('custrecord_pe_vertical',
				                    null, 'group'),
				            location : entry.getText('custrecord_pe_region',
				                    null, 'group'),
				            employeeId : entry.getValue(
				                    'custrecord_pe_employee', null, 'group'),
				            projectId : entry.getValue('custrecord_pe_project',
				                    null, 'group'),
				            customerId : entry.getValue(
				                    'custrecord_pe_customer', null, 'group'),
				            practiceId : entry.getValue(
				                    'custrecord_pe_practice', null, 'group'),
				            verticalId : entry.getValue(
				                    'custrecord_pe_vertical', null, 'group'),
				            locationId : entry.getValue('custrecord_pe_region',
				                    null, 'group'),
				            billrate : entry.getValue(
				                    'custrecord_pe_bill_rate', null, 'group'),
				            hours : entry.getValue(
				                    'custrecord_pe_hour_not_submitted', null,
				                    'sum'),
				            amount : amount,
				            currency : entry.getText('custrecord_pe_currency',
				                    null, 'group'),
				            currencyValue : entry.getValue(
				                    'custrecord_pe_currency', null, 'group'),
				            amountconverted : convertedAmount,
				            newcurrency : 'INR' // <== remove this using
				        // master record
				        });
			        });
		}

		return data;
	} catch (err) {
		nlapiLogExecution('ERROR', 'getHourlyNotSubmittedData', err);
		throw err;
	}
}

function getMonthlyApprovedData(provisionMasterId, arrExchangeRate) {
	try {
		var provisionEntrySearch = searchRecord(
		        'customrecord_provision_entry',
		        null,
		        [
		                new nlobjSearchFilter('isinactive', null, 'is', 'F'),
		                new nlobjSearchFilter('custrecord_pe_employee', null,
		                        'noneof', '@NONE@'),
		                new nlobjSearchFilter('custrecord_pe_provision_master',
		                        null, 'anyof', provisionMasterId),
		                new nlobjSearchFilter('custrecord_pe_is_monthly', null,
		                        'is', 'T') ],
		        [
		                new nlobjSearchColumn(
		                        'custrecord_pe_provision_from_date', null,
		                        'group'),
		                new nlobjSearchColumn(
		                        'custrecord_pe_provision_to_date', null,
		                        'group'),
		                new nlobjSearchColumn('custrecord_pe_employee', null,
		                        'group'),
		                new nlobjSearchColumn('custrecord_pe_project', null,
		                        'group'),
		                new nlobjSearchColumn('custrecord_pe_project_name',
		                        null, 'group'),
		                new nlobjSearchColumn('custrecord_pe_customer', null,
		                        'group'),
		                new nlobjSearchColumn('custrecord_pe_currency', null,
		                        'group'),
		                new nlobjSearchColumn('custrecord_pe_subsidiary', null,
		                        'group'),
		                new nlobjSearchColumn('custrecord_pe_practice', null,
		                        'group'),
		                new nlobjSearchColumn('custrecord_pe_vertical', null,
		                        'group'),
		                new nlobjSearchColumn('custrecord_pe_region', null,
		                        'group'),
		                new nlobjSearchColumn('custrecord_pe_daily_rate', null,
		                        'group'),
		                new nlobjSearchColumn('custrecord_pe_monthly_rate',
		                        null, 'group'),
		                new nlobjSearchColumn('custrecord_pe_day_approved',
		                        null, 'sum'),
		                new nlobjSearchColumn(
		                        'custrecord_pe_month_amt_approved', null, 'sum') ]);

		var data = [];

		if (provisionEntrySearch) {

			provisionEntrySearch.forEach(function(entry) {
				var amount = parseFloat(entry.getValue(
				        'custrecord_pe_month_amt_approved', null, 'sum'));
				var exchangeRate = parseFloat((parseFloat(arrExchangeRate[entry
				        .getText('custrecord_pe_currency', null, 'group')]))
				        .toFixed(2));
				var convertedAmount = parseFloat((amount * exchangeRate)
				        .toFixed(2));

				if (amount == 0) {
					return;
				}

				data
				        .push({
				            fromdate : entry.getValue(
				                    'custrecord_pe_provision_from_date', null,
				                    'group'),
				            todate : entry.getValue(
				                    'custrecord_pe_provision_to_date', null,
				                    'group'),
				            employee : entry.getText('custrecord_pe_employee',
				                    null, 'group'),
				            project : entry
				                    .getValue('custrecord_pe_project_name',
				                            null, 'group'),
				            customer : entry.getText('custrecord_pe_customer',
				                    null, 'group'),
				            practice : entry.getText('custrecord_pe_practice',
				                    null, 'group'),
				            vertical : entry.getText('custrecord_pe_vertical',
				                    null, 'group'),
				            location : entry.getText('custrecord_pe_region',
				                    null, 'group'),
				            employeeId : entry.getValue(
				                    'custrecord_pe_employee', null, 'group'),
				            projectId : entry.getValue('custrecord_pe_project',
				                    null, 'group'),
				            customerId : entry.getValue(
				                    'custrecord_pe_customer', null, 'group'),
				            practiceId : entry.getValue(
				                    'custrecord_pe_practice', null, 'group'),
				            verticalId : entry.getValue(
				                    'custrecord_pe_vertical', null, 'group'),
				            locationId : entry.getValue('custrecord_pe_region',
				                    null, 'group'),
				            billrate : entry.getValue(
				                    'custrecord_pe_daily_rate', null, 'group'),
				            monthlyrate : entry
				                    .getValue('custrecord_pe_monthly_rate',
				                            null, 'group'),
				            hours : entry.getValue(
				                    'custrecord_pe_day_approved', null, 'sum'),
				            amount : amount,
				            currency : entry.getText('custrecord_pe_currency',
				                    null, 'group'),
				            currencyValue : entry.getValue(
				                    'custrecord_pe_currency', null, 'group'),
				            amountconverted : convertedAmount,
				            newcurrency : 'INR' // <== remove this using
				        // master record
				        });
			});
		}

		return data;
	} catch (err) {
		nlapiLogExecution('ERROR', 'getMonthlyApprovedData', err);
		throw err;
	}
}

function getMonthlySubmittedData(provisionMasterId, arrExchangeRate) {
	try {
		var provisionEntrySearch = searchRecord('customrecord_provision_entry',
		        null, [
		                new nlobjSearchFilter('isinactive', null, 'is', 'F'),
		                new nlobjSearchFilter('custrecord_pe_employee', null,
		                        'noneof', '@NONE@'),
		                new nlobjSearchFilter('custrecord_pe_provision_master',
		                        null, 'anyof', provisionMasterId),
		                new nlobjSearchFilter('custrecord_pe_is_monthly', null,
		                        'is', 'T') ], [
		                new nlobjSearchColumn(
		                        'custrecord_pe_provision_from_date', null,
		                        'group'),
		                new nlobjSearchColumn(
		                        'custrecord_pe_provision_to_date', null,
		                        'group'),
		                new nlobjSearchColumn('custrecord_pe_employee', null,
		                        'group'),
		                new nlobjSearchColumn('custrecord_pe_project', null,
		                        'group'),
		                new nlobjSearchColumn('custrecord_pe_project_name',
		                        null, 'group'),
		                new nlobjSearchColumn('custrecord_pe_customer', null,
		                        'group'),
		                new nlobjSearchColumn('custrecord_pe_currency', null,
		                        'group'),
		                new nlobjSearchColumn('custrecord_pe_subsidiary', null,
		                        'group'),
		                new nlobjSearchColumn('custrecord_pe_practice', null,
		                        'group'),
		                new nlobjSearchColumn('custrecord_pe_vertical', null,
		                        'group'),
		                new nlobjSearchColumn('custrecord_pe_region', null,
		                        'group'),
		                new nlobjSearchColumn('custrecord_pe_daily_rate', null,
		                        'group'),
		                new nlobjSearchColumn('custrecord_pe_monthly_rate',
		                        null, 'group'),
		                new nlobjSearchColumn('custrecord_pe_day_submitted',
		                        null, 'sum'),
		                new nlobjSearchColumn(
		                        'custrecord_pe_month_amt_submitted', null,
		                        'sum') ]);

		var data = [];

		if (provisionEntrySearch) {

			provisionEntrySearch
			        .forEach(function(entry) {
				        var amount = parseFloat(entry.getValue(
				                'custrecord_pe_month_amt_submitted', null,
				                'sum'));
				        var exchangeRate = parseFloat((parseFloat(arrExchangeRate[entry
				                .getText('custrecord_pe_currency', null,
				                        'group')])).toFixed(2));
				        var convertedAmount = parseFloat((amount * exchangeRate)
				                .toFixed(2));

				        if (amount == 0) {
					        return;
				        }

				        data
				                .push({
				                    fromdate : entry
				                            .getValue(
				                                    'custrecord_pe_provision_from_date',
				                                    null, 'group'),
				                    todate : entry.getValue(
				                            'custrecord_pe_provision_to_date',
				                            null, 'group'),
				                    employee : entry.getText(
				                            'custrecord_pe_employee', null,
				                            'group'),
				                    project : entry.getValue(
				                            'custrecord_pe_project_name', null,
				                            'group'),
				                    customer : entry.getText(
				                            'custrecord_pe_customer', null,
				                            'group'),
				                    practice : entry.getText(
				                            'custrecord_pe_practice', null,
				                            'group'),
				                    vertical : entry.getText(
				                            'custrecord_pe_vertical', null,
				                            'group'),
				                    location : entry.getText(
				                            'custrecord_pe_region', null,
				                            'group'),
				                    employeeId : entry.getValue(
				                            'custrecord_pe_employee', null,
				                            'group'),
				                    projectId : entry.getValue(
				                            'custrecord_pe_project', null,
				                            'group'),
				                    customerId : entry.getValue(
				                            'custrecord_pe_customer', null,
				                            'group'),
				                    practiceId : entry.getValue(
				                            'custrecord_pe_practice', null,
				                            'group'),
				                    verticalId : entry.getValue(
				                            'custrecord_pe_vertical', null,
				                            'group'),
				                    locationId : entry.getValue(
				                            'custrecord_pe_region', null,
				                            'group'),
				                    billrate : entry.getValue(
				                            'custrecord_pe_daily_rate', null,
				                            'group'),
				                    monthlyrate : entry.getValue(
				                            'custrecord_pe_monthly_rate', null,
				                            'group'),
				                    hours : entry.getValue(
				                            'custrecord_pe_day_submitted',
				                            null, 'sum'),
				                    amount : amount,
				                    currency : entry.getText(
				                            'custrecord_pe_currency', null,
				                            'group'),
				                    currencyValue : entry.getValue(
				                            'custrecord_pe_currency', null,
				                            'group'),
				                    amountconverted : convertedAmount,
				                    newcurrency : 'INR' // <== remove this using
				                // master record
				                });
			        });
		}

		return data;
	} catch (err) {
		nlapiLogExecution('ERROR', 'getMonthlySubmittedData', err);
		throw err;
	}
}

function getMonthlyNotSubmittedData(provisionMasterId, arrExchangeRate) {
	try {
		var provisionEntrySearch = searchRecord(
		        'customrecord_provision_entry',
		        null,
		        [
		                new nlobjSearchFilter('isinactive', null, 'is', 'F'),
		                new nlobjSearchFilter('custrecord_pe_employee', null,
		                        'noneof', '@NONE@'),
		                new nlobjSearchFilter('custrecord_pe_provision_master',
		                        null, 'anyof', provisionMasterId),
		                new nlobjSearchFilter('custrecord_pe_is_monthly', null,
		                        'is', 'T') ],
		        [
		                new nlobjSearchColumn(
		                        'custrecord_pe_provision_from_date', null,
		                        'group'),
		                new nlobjSearchColumn(
		                        'custrecord_pe_provision_to_date', null,
		                        'group'),
		                new nlobjSearchColumn('custrecord_pe_employee', null,
		                        'group'),
		                new nlobjSearchColumn('custrecord_pe_project', null,
		                        'group'),
		                new nlobjSearchColumn('custrecord_pe_project_name',
		                        null, 'group'),
		                new nlobjSearchColumn('custrecord_pe_customer', null,
		                        'group'),
		                new nlobjSearchColumn('custrecord_pe_currency', null,
		                        'group'),
		                new nlobjSearchColumn('custrecord_pe_subsidiary', null,
		                        'group'),
		                new nlobjSearchColumn('custrecord_pe_practice', null,
		                        'group'),
		                new nlobjSearchColumn('custrecord_pe_vertical', null,
		                        'group'),
		                new nlobjSearchColumn('custrecord_pe_region', null,
		                        'group'),
		                new nlobjSearchColumn('custrecord_pe_daily_rate', null,
		                        'group'),
		                new nlobjSearchColumn('custrecord_pe_monthly_rate',
		                        null, 'group'),
		                new nlobjSearchColumn(
		                        'custrecord_pe_day_not_submitted', null, 'sum'),
		                new nlobjSearchColumn(
		                        'custrecord_pe_month_amt_not_submitted', null,
		                        'sum') ]);

		var data = [];

		if (provisionEntrySearch) {

			provisionEntrySearch.forEach(function(entry) {
				var amount = parseFloat(entry.getValue(
				        'custrecord_pe_month_amt_not_submitted', null, 'sum'));
				var exchangeRate = parseFloat((parseFloat(arrExchangeRate[entry
				        .getText('custrecord_pe_currency', null, 'group')]))
				        .toFixed(2));
				var convertedAmount = parseFloat((amount * exchangeRate)
				        .toFixed(2));

				if (amount == 0) {
					return;
				}

				data
				        .push({
				            fromdate : entry.getValue(
				                    'custrecord_pe_provision_from_date', null,
				                    'group'),
				            todate : entry.getValue(
				                    'custrecord_pe_provision_to_date', null,
				                    'group'),
				            employee : entry.getText('custrecord_pe_employee',
				                    null, 'group'),
				            project : entry
				                    .getValue('custrecord_pe_project_name',
				                            null, 'group'),
				            customer : entry.getText('custrecord_pe_customer',
				                    null, 'group'),
				            practice : entry.getText('custrecord_pe_practice',
				                    null, 'group'),
				            vertical : entry.getText('custrecord_pe_vertical',
				                    null, 'group'),
				            location : entry.getText('custrecord_pe_region',
				                    null, 'group'),
				            employeeId : entry.getValue(
				                    'custrecord_pe_employee', null, 'group'),
				            projectId : entry.getValue('custrecord_pe_project',
				                    null, 'group'),
				            customerId : entry.getValue(
				                    'custrecord_pe_customer', null, 'group'),
				            practiceId : entry.getValue(
				                    'custrecord_pe_practice', null, 'group'),
				            verticalId : entry.getValue(
				                    'custrecord_pe_vertical', null, 'group'),
				            locationId : entry.getValue('custrecord_pe_region',
				                    null, 'group'),
				            billrate : entry.getValue(
				                    'custrecord_pe_daily_rate', null, 'group'),
				            monthlyrate : entry
				                    .getValue('custrecord_pe_monthly_rate',
				                            null, 'group'),
				            hours : entry.getValue(
				                    'custrecord_pe_day_not_submitted', null,
				                    'sum'),
				            amount : amount,
				            currency : entry.getText('custrecord_pe_currency',
				                    null, 'group'),
				            currencyValue : entry.getValue(
				                    'custrecord_pe_currency', null, 'group'),
				            amountconverted : convertedAmount,
				            newcurrency : 'INR' // <== remove this using
				        // master record
				        });
			});
		}

		return data;
	} catch (err) {
		nlapiLogExecution('ERROR', 'getMonthlyNotSubmittedData', err);
		throw err;
	}
}

function getExchangeRate(effectiveDate, targetCurrency) {
	try {
		return {
		    'EUR' : nlapiExchangeRate('EUR', targetCurrency, effectiveDate),
		    'GBP' : nlapiExchangeRate('GBP', targetCurrency, effectiveDate),
		    'INR' : nlapiExchangeRate('INR', targetCurrency, effectiveDate),
		    'Peso' : nlapiExchangeRate('PHP', targetCurrency, effectiveDate),
		    'SGD' : nlapiExchangeRate('SGD', targetCurrency, effectiveDate),
		    'USD' : nlapiExchangeRate('USD', targetCurrency, effectiveDate)
		};
	} catch (err) {
		nlapiLogExecution('ERROR', 'getExchangeRate', err);
		throw err;
	}
}

function getRunningProvisionMasterId() {
	try {
		// get the currently active monthly provision
		var provisionMasterSearch = nlapiSearchRecord(
		        'customrecord_provision_master', null, [
		                new nlobjSearchFilter('isinactive', null, 'is', 'F'),
		                new nlobjSearchFilter(
		                        'custrecord_pm_is_currently_active', null,
		                        'is', 'T') ]);

		var provisionMasterId = null;
		if (provisionMasterSearch) {
			provisionMasterId = provisionMasterSearch[0].getId();
		}

		return provisionMasterId;
	} catch (err) {
		nlapiLogExecution('ERROR', 'getRunningProvisionMasterId', err);
		throw err;
	}
}

function noProvisionRunning() {
	try {
		var runningProvision = getRunningProvisionMasterId();
		return runningProvision == null;
	} catch (err) {
		nlapiLogExecution('ERROR', 'noProvisionRunning', err);
		throw err;
	}
}