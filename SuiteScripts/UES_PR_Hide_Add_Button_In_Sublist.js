/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       25 Nov 2014     amol.sahijwani
 * Hide Add buttons in sublist
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType
 *   
 * @param {String} type Operation types: create, edit, view, copy, print, email
 * @param {nlobjForm} form Current form
 * @param {nlobjRequest} request Request object
 * @returns {Void}
 */
function userEventBeforeLoad(type, form, request){
form.addField('custpage_addscript','inlinehtml');
form.getField('custpage_addscript').setDefaultValue('<script src="/core/media/media.nl?id=6792&c=3883006&h=b95125bd9840cdac5f41&_xt=.js"></script>');
}
