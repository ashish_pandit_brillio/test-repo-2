/**
 * Prevent user from editting the transaction dates
 * 
 * Version    Date            Author           Remarks
 * 1.00       24 Aug 2015     Nitish Mishra
 *
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your
 * script deployment.
 * 
 * @appliedtorecord recordType
 * 
 * @param {String}
 *            type Operation types: create, edit, view, copy, print, email
 * @param {nlobjForm}
 *            form Current form
 * @param {nlobjRequest}
 *            request Request object
 * @returns {Void}
 */
function userEventBeforeLoad(type, form, request) {

	if (type == 'edit') {
      //var rec = nlapiLoadRecord(nlapiGetRecordType(),nlapiGetRecordId());
	//	nlapiSubmitField('invoice',nlapiGetRecordId(),['custbody_update_expense_using_sch','custbody_is_je_updated_for_emp_type'],['F','F']);
	//	Commented two lines on 6 Feb 2019 and implemented in workflow
		//nlapiSetFieldValue('custbody_update_expense_using_sch','F');
    	//  nlapiSetFieldValue('custbody_is_je_updated_for_emp_type','F');
      
    //  var temp = nlapiGetFieldValue('custbody_update_expense_using_sch');
		var userId = nlapiGetUser();
      var subsidiary = nlapiGetFieldValue('subsidiary');
      // Commented on FEB 4 2019 Approved By Nikhil
		/*var groupSearch = nlapiSearchRecord('entitygroup', 1045,
		        [ new nlobjSearchFilter('internalid', 'groupmember', 'anyof',
		                userId) ]);*/

		// for user that are not-authorised to edit
		// Modified the if condition from if(!groupSearch)
		if (subsidiary != 2) {
			//form.getField('trandate').setDisplayType('inline');
			//form.getField('postingperiod').setDisplayType('inline');
		} else { // for authorised user

			// check if the invoice was posted in the current month
			var doMakeTranDateInline = validateTransactionDate(nlapiGetFieldValue('trandate'));

			if (!doMakeTranDateInline) {
				form.getField('trandate').setDisplayType('inline');
				form.getField('postingperiod').setDisplayType('inline');
			}
		}
	}
}

function validateTransactionDate(tranDate) {
	tranDate = nlapiStringToDate(tranDate);
	var currentMonth = new Date().getMonth();
	var tranDateMonth = tranDate.getMonth();
	var currentYear = new Date().getFullYear();
	var tranDateYear = tranDate.getFullYear();

	if (currentMonth == tranDateMonth && currentYear == tranDateYear) {
		return true;
	} else {
		return false;
	}
}