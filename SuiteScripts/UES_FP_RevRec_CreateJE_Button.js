/**
 * @author Jayesh
 */

function beforeLoad_displayButton(type)
{
	try
	{
		if (type == 'view')
		{
			form.setScript('customscript_cli_fp_revrec_invokesuitelt');
			form.setScript('customscript_ues_fp_revrec_createje_butt');
			
			var rcrd_id = nlapiGetRecordId();
			
			if (nlapiGetFieldValue('custrecord_status_fp_rev_rce') == 3 && nlapiGetFieldValue('custrecord_status_fp_rev_rce') != 5)
			{
				var filters = new Array();
				filters.push(new nlobjSearchFilter('status', null, 'anyof', ['PROCESSING', 'PENDING', 'COMPLETE']));
				
				if(nlapiGetFieldValue('custrecord_data_validated') != 'T')
				{
					filters.push(new nlobjSearchFilter('internalid', 'script', 'is', '1181'));
				}
				else
				{
					filters.push(new nlobjSearchFilter('internalid', 'script', 'is', '1343'));
				}
				
				var column = new Array();
				column.push(new nlobjSearchColumn('name', 'script'));
				column.push(new nlobjSearchColumn('internalid', 'scriptdeployment'));
				column.push(new nlobjSearchColumn('datecreated'));
				column.push(new nlobjSearchColumn('status'));
				column.push(new nlobjSearchColumn('startdate'));
				column.push(new nlobjSearchColumn('enddate'));
				column.push(new nlobjSearchColumn('queue'));
				column.push(new nlobjSearchColumn('percentcomplete'));
				column.push(new nlobjSearchColumn('queueposition'));
				column.push(new nlobjSearchColumn('percentcomplete'));
				
				var a_search_script_status = nlapiSearchRecord('scheduledscriptinstance', null, filters, column);
				
				if (a_search_script_status)
				{
				
					var s_script = a_search_script_status[0].getValue('name', 'script');
					var d_date_created = a_search_script_status[0].getValue('datecreated');
					var i_percent_complete = a_search_script_status[0].getValue('percentcomplete');
					var s_script_status = a_search_script_status[0].getValue('status');
					
					if (i_percent_complete == '' || i_percent_complete == null || i_percent_complete == undefined) {
						s_script_status = 'Not Started';
						i_percent_complete = '0%';
					}
					
					if (s_script_status == 'Complete') {
						i_percent_complete = '100%';
						s_script_status = 'Complete';
					}
					
					var message = '<html>';
					message += '<head>';
					message += "<meta http-equiv=\"refresh\" content=\"5\" \/>";
					message += '<meta charset="utf-8" />';
					message += '</head>';
					message += '<body>';
					var i_counter = '0%'
					message += "<div id=\"my-progressbar-container\">";
					message += "            ";
					message += "<div class=\"ajax-content\" id=\"test14\" name=\"test14\" style='height:50%;width:50%;' align='left' valign='top'>"; //opacity: .75;
					message += "<img src='https://system.sandbox.netsuite.com/core/media/media.nl?id=190162&c=3883006&h=1cf2a9f8bd8b922539c8' align='left' style='height:50%;width:50%;align=left;'/>";
					message += "        <\/div>";
					message += '</body>';
					message += '</html>';
					nlapiSetFieldValue('custrecord_progress_bar_auto_refresh', message);
					
					var a_fld_arr = new Array();
					a_fld_arr[0] = 'custrecord_processing_status_fp_rev_rec';
					a_fld_arr[1]= 'custrecord_prcnt_complt_fp_rev_rec';
					
					var a_fld_values = new Array();
					a_fld_values[0] = s_script_status;
					a_fld_values[1] = i_percent_complete;
			
					nlapiSubmitField('customrecord_fp_rev_rec_je_creation', rcrd_id, a_fld_arr, a_fld_values);
				}
			}
			else if(nlapiGetFieldValue('custrecord_data_validated') != 'T' && nlapiGetFieldValue('custrecord_validate_data_export') != 'T')
			{
				
              form.addButton('custpage_createJe', ' Before Validate', 'create_je(\'  ' + rcrd_id + '  \',\' ' + 'data' + ' \')');
			}
          else if(nlapiGetFieldValue('custrecord_status_fp_rev_rce') == 4 &&nlapiGetFieldValue('custrecord_validate_data_export') == 'T')
            {
              if(!nlapiGetFieldValue('custrecord_journal_entries_fp_rev_rec'))
					form.addButton('custpage_createJe', 'Validate Data', 'create_je(\'  ' + rcrd_id + '  \',\' ' + nlapiGetFieldValue('custrecord_data_validated') + ' \')');
               form.addButton('custpage_createJe', 'Export Validated Data', 'down_excel_fun()');
            }
			else if(nlapiGetFieldValue('custrecord_status_fp_rev_rce') == 5 && nlapiGetFieldValue('custrecord_data_validated') == 'T')
			{
				if(!nlapiGetFieldValue('custrecord_journal_entries_fp_rev_rec'))
					form.addButton('custpage_createJe', 'Create JE', 'create_je(\'  ' + rcrd_id + '  \',\' ' + nlapiGetFieldValue('custrecord_data_validated') + ' \')');
			        
					
					
					
			
			}
			form.addButton('custpage_export_excel_data','Export Data','down_excel_function()');
		}
	}
	catch(err)
	{
		nlapiLogExecution('ERROR','','ERROR:- '+err);
	}
}

function beforeSubmit()
{
	try{
		
		if(nlapiGetFieldValue('custrecord_status_fp_rev_rce') == '')
			nlapiSetFieldValue('custrecord_status_fp_rev_rce',4)
	}
	catch(err)
	{
		nlapiLogExecution('ERROR','ERROR MESSAGE:- '+err);
	}
}

function down_excel_function()
{
	try
	{
	
	 var create_excel = nlapiResolveURL('SUITELET', 'customscript_su_fp_rev_rec_je_excel_dwld', 'customdeploy1', false);
	 create_excel += '&recordType=' + nlapiGetRecordType();
	 create_excel += '&recordId=' + nlapiGetRecordId();
     window.open(create_excel,"_self");
	
	}
	catch(err)
	{
		nlapiLogExecution('ERROR','ERROR MESSAGE:- ',err);
	}
}
function down_excel_fun()
{
	try
	{
	
	 var create_excel = nlapiResolveURL('SUITELET', 'customscript_export_validate_data', 'customdeploy_validate_data_export', false);
	 create_excel += '&recordType=' + nlapiGetRecordType();
	 create_excel += '&recordId=' + nlapiGetRecordId();
     window.open(create_excel,"_self");
	
	}
	catch(err)
	{
		nlapiLogExecution('ERROR','ERROR MESSAGE:- ',err);
	}
}
