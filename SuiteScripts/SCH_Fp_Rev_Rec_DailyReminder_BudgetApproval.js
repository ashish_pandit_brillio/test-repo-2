/**
 * @author Jayesh
 */

function scheduled_approval_reminder()
{
	try
	{
		var a_project_filter = [['status', 'noneof', 1], 'and',
								['jobtype', 'anyof', 2], 'and',
								['jobbillingtype', 'anyof', 'FBM'], 'and',
								['custentity_fp_rev_rec_type', 'anyof', 1]];
		
		var a_columns_proj_srch = new Array();
		a_columns_proj_srch[0] = new nlobjSearchColumn('custentity_projectvalue');
		a_columns_proj_srch[1] = new nlobjSearchColumn('customer');
		a_columns_proj_srch[2] = new nlobjSearchColumn('custentity_fp_rev_rec_type');
		a_columns_proj_srch[3] = new nlobjSearchColumn('custrecord_practicehead','custentity_practice');
			
		var a_project_search_results = searchRecord('job', null, a_project_filter, a_columns_proj_srch);
		
		if (a_project_search_results)
		{
			for (var i_pro_index = 0; i_pro_index < a_project_search_results.length; i_pro_index++)
			{
				var a_revenue_cap_filter = [['custrecord_revenue_share_project', 'anyof', a_project_search_results[i_pro_index].getId()]];
				
				var a_columns_existing_cap_srch = new Array();
				a_columns_existing_cap_srch[0] = new nlobjSearchColumn('created').setSort(true);
				a_columns_existing_cap_srch[1] = new nlobjSearchColumn('custrecord_revenue_share_project');
				a_columns_existing_cap_srch[2] = new nlobjSearchColumn('custrecord_revenue_share_approval_status');
				a_columns_existing_cap_srch[3] = new nlobjSearchColumn('custrecord_revenue_share_total');
				a_columns_existing_cap_srch[4] = new nlobjSearchColumn('customer', 'custrecord_revenue_share_project');
				a_columns_existing_cap_srch[5] = new nlobjSearchColumn('custentity_fp_rev_rec_type', 'custrecord_revenue_share_project');
				
				var a_get_revenue_share = nlapiSearchRecord('customrecord_revenue_share', null, a_revenue_cap_filter, a_columns_existing_cap_srch);
				if(a_get_revenue_share)
				{
					var i_existing_rcrd_status = a_get_revenue_share[0].getValue('custrecord_revenue_share_approval_status');
					
					if (i_existing_rcrd_status == 2)
					{
						var s_execusting_prac_head_mail = nlapiLookupField('employee',a_project_search_results[i_pro_index].getValue('custrecord_practicehead','custentity_practice'),'email');
						
						var a_emp_attachment = new Array();
						a_emp_attachment['entity'] = a_project_search_results[i_pro_index].getValue('custrecord_practicehead','custentity_practice');
						
						var strVar = '';
						strVar += '<html>';
						strVar += '<body>';
						
						strVar += '<p>Hello Practice Head ,</p>';
						strVar += '<p>Kindly approve budget plan for Project:- '+a_get_revenue_share[0].getText('custrecord_revenue_share_project')+', where you are the practice head.</p>';
						
						strVar += '<p>Thanks & Regards,</p>';
						strVar += '<p>Team IS</p>';
							
						strVar += '</body>';
						strVar += '</html>';
						
						nlapiSendEmail(442, s_execusting_prac_head_mail, 'Approve Rev Rec Budget Plan', strVar,null,null,a_emp_attachment);
					
						nlapiLogExecution('audit','email attached to emp:- '+a_project_search_results[i_pro_index].getValue('custrecord_practicehead','custentity_practice'));
					}
				}
			}
		}
	}
	catch(err)
	{
		nlapiLogExecution('ERROR','scheduled_approval_reminder','ERROR MESSAGE:- '+err);
	}
}

function searchRecord(recordType, savedSearch, arrFilters, arrColumns,
        filterExpression)
{

	try {
		var search = null;

		// if a saved search is provided, load it and add the filters and
		// columns
		if (isNotEmpty(savedSearch)) {
			search = nlapiLoadSearch(recordType, savedSearch);

			if (isArrayNotEmpty(arrFilters)) {
				search.addFilters(arrFilters);
			}

			if (isArrayNotEmpty(arrColumns)) {
				search.addColumns(arrColumns);
			}

			if (isArrayNotEmpty(filterExpression)) {
				search.setFilterExpression(filterExpression);
			}
		}
		// create a new search
		else {
			search = nlapiCreateSearch(recordType, arrFilters, arrColumns);
		}

		// run search
		var resultSet = search.runSearch();

		// iterate through the search and get all data 1000 at a time
		var searchResultCount = 0;
		var resultSlice = null;
		var searchResult = [];

		do {
			resultSlice = resultSet.getResults(searchResultCount,
			        searchResultCount + 1000);

			if (resultSlice) {

				resultSlice.forEach(function(result) {

					searchResult.push(result);
					searchResultCount++;
				});
			}
		} while (isArrayNotEmpty(resultSlice) && resultSlice.length >= 1000);

		return searchResult;
	} catch (err) {
		nlapiLogExecution('ERROR', 'searchRecord', err);
		throw err;
	}
}

function isEmpty(value) {

	return value == null || value == "" || typeof (value) == undefined;
}

function isNotEmpty(value) {

	return !isEmpty(value);
}

function isArrayEmpty(argArray) {

	return !isArrayNotEmpty(argArray);
}

function isArrayNotEmpty(argArray) {

	return (isNotEmpty(argArray) && argArray.length > 0);
}