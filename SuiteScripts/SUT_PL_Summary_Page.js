/**
 * @author Deepak
 */

function suitelet(request,response)
{
	try
	{
		
		var form_obj = nlapiCreateForm("PL Summary Report");
		
		if (request.getMethod() == 'GET')
		{
			var i_current_user =  nlapiGetUser();
			var i_Month = form_obj.addField('custpage_month', 'select', 'Month','customlist_month').setMandatory(true);	
			var i_Year = form_obj.addField('custpage_year', 'select', 'Year','customlist_year').setMandatory(true);;
			var i_user = form_obj.addField('custpage_user', 'text', 'User').setDisplayType('hidden').setDefaultValue(i_current_user);
			//var Gbp_Inr = form_obj.addField('gbptoinr', 'currency', 'GBP TO INR');//.setMandatory(true);
			//var Usd_Inr = form_obj.addField('usdtoinr', 'currency', 'USD TO INR');//.setMandatory(true);
			form_obj.addSubmitButton('Export Report');
			
		}
		
		if (request.getMethod() == 'POST')
		{
			var bill_data_arr = new Array();
			var bill_unique_list = new Array();
			var sr_no = 0;
			
			var i_Month = request.getParameter('custpage_month');
			var i_Year = request.getParameter('custpage_year');
			var i_user = request.getParameter('custpage_user');
			//var exchangeamtusd = request.getParameter('usdtoinr');
			//var exchangeamtgbp = request.getParameter('gbptoinr');
			var name_ = nlapiLookupField('customlist_year',parseInt(i_Year),'name');
			nlapiLogExecution('DEBUG','i_Month,i_Year,i_user','i_Month:'+i_Month+ 'i_Year:'+i_Year+ 'i_user:'+i_user+ 'name:'+name_);	
			
			//if(context.getRemainingUsage()< 100)
			{
				var params = new Array();
				
				//var o_field = form_obj.addField('dispalymessage', 'inlinehtml', 'Message');
				//o_field.setDefaultValue(strVar);
				//var d_start_date = nlapiStringToDate(s_start_date, 'date');
				//var d_end_date = nlapiStringToDate(s_end_date, 'date');
				params['custscript_pl_month'] = i_Month;
				params['custscript_pl_year'] = i_Year;
              	params['custscript_pl_user'] = i_user;
				//params['custscript_gbp_inr'] = exchangeamtgbp;
				//params['custscript_usd_inr'] = exchangeamtusd;
				nlapiScheduleScript('customscript_sch_pl_summary_report', 'customdeploy_sch_pl_summary_report', params);
				
			}
			var field = form_obj.addField('dispalymessage', 'inlinehtml');
			var strVar="";
			strVar += "<html>";
			strVar += "<p>Email will be sent to you shortly.<\/p>";
			strVar += "<\/html>";
			field.setDefaultValue(strVar);

		}
		
		response.writePage(form_obj);
		
		
	}
	catch(err)
	{
		nlapiLogExecution('ERROR','ERROR MESSAGE :- ',err);
		
	}
}

