function getPersonUtilization(allocationStartDate, allocationEndDate,
        percentageAllocation, entryStartDate, entryEndDate, totalDays,
        hourPerDay)
{
	try {
		var percentageNumeric = (parseFloat(percentageAllocation.split("%")[0]) / 100);

		var d_allocationStartDate = nlapiStringToDate(allocationStartDate);
		var d_allocationEndDate = nlapiStringToDate(allocationEndDate);
		var d_entryStartDate = nlapiStringToDate(entryStartDate);
		var d_entryEndDate = nlapiStringToDate(entryEndDate);

		var entryMonthStartDate = nlapiStringToDate((d_entryStartDate
		        .getMonth() + 1)
		        + '/1/' + d_entryStartDate.getFullYear());
		var nextEntryMonthStartDate = nlapiAddMonths(entryMonthStartDate, 1);
		var entryMonthEndDate = nlapiAddDays(nextEntryMonthStartDate, -1);
		var startDate = d_allocationStartDate > d_entryStartDate ? allocationStartDate
		        : entryStartDate;
		var endDate = d_allocationEndDate < d_entryEndDate ? allocationEndDate
		        : entryEndDate;

		var noOfAllocatedDays = getDayDiff(startDate, endDate);
		var noOfAllocatedHours = noOfAllocatedDays * hourPerDay
		        * percentageNumeric;

		var personMonth = noOfAllocatedHours / (totalDays * hourPerDay);
		return parseFloat(personMonth.toFixed(2));
	} catch (err) {
		nlapiLogExecution('ERROR', 'getPersonUtilization', err);
		throw err;
	}
}

function getDayDiff(fromDate, toDate) {
	fromDate = nlapiStringToDate(fromDate);
	toDate = nlapiStringToDate(toDate);

	if (fromDate > toDate) {
		return -999;
	}

	var dayDiff = 1;
	for (;; dayDiff++) {
		var newDate = nlapiAddDays(fromDate, dayDiff);

		if (newDate > toDate) {
			break;
		}
	}

	return dayDiff;
}

function MaximumDate(date1, date2, date3) {
	return (date1 > date2) ? (date1 > date3 ? date1 : date3)
	        : (date2 > date3 ? date2 : date3);
}

function MinimumDate(date1, date2, date3) {
	return (date1 < date2) ? (date1 < date3 ? date1 : date3)
	        : (date2 < date3 ? date2 : date3);
}

function getWorkingDays(startDate, endDate) {
	try {
		var d_startDate = nlapiStringToDate(startDate);
		var d_endDate = nlapiStringToDate(endDate);
		var numberOfWorkingDays = 0;

		for (var i = 0;; i++) {
			var currentDate = nlapiAddDays(d_startDate, i);

			if (currentDate > d_endDate) {
				break;
			}

			if (currentDate.getDay() != 0 && currentDate.getDay() != 6) {
				numberOfWorkingDays += 1;
			}
		}

		return numberOfWorkingDays;
	} catch (err) {
		nlapiLogExecution('error', 'getWorkingDays', err);
		throw err;
	}
}

function removeComma(text) {
	return text.replace(/,/g, " ");
}

function getInactivePracticeMapping() {
	return {
	    282 : 324,
	    292 : 325,
	    307 : 325,
	    452 : 451,
	    305 : 320,
	    432 : 328,
	    339 : 320,
	    306 : 325,
	    308 : 316,
	    345 : 326,
	    314 : 458,
	    439 : 322,
	    341 : 322,
	    333 : 326,
	    302 : 326,
	    404 : 316,
	    289 : 316,
	    402 : 326,
	    293 : 316,
	    431 : 325,
	    408 : 407,
	    337 : 318,
	    285 : 326,
	    428 : 316,
	    294 : 316,
	    309 : 316,
	    429 : 316,
	    335 : 316,
	    310 : 316,
	    300 : 326,
	    303 : 326,
	    301 : 326,
	    286 : 326,
	    287 : 326,
	    304 : 316,
	    344 : 324,
	    440 : 327
	};
}