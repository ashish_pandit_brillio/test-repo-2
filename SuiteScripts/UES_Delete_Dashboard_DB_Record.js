/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       12 Feb 2019     Aazamali Khan
 *
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType
 * 
 * @param {String} type Operation types: create, edit, delete, xedit,
 *                      approve, cancel, reject (SO, ER, Time Bill, PO & RMA only)
 *                      pack, ship (IF only)
 *                      dropship, specialorder, orderitems (PO only) 
 *                      paybills (vendor payments)
 * @returns {Void}
 */
function userEventAfterSubmit(type){
	var newRec = nlapiLoadRecord(nlapiGetRecordType(), nlapiGetRecordId());
	var oldRec = nlapiGetOldRecord();
	if (newRec.getFieldValue("custrecord_stage_sfdc") != oldRec.getFieldValue("custrecord_stage_sfdc")) {
		if (newRec.getFieldValue("custrecord_stage_sfdc") == "Closed Lost" || newRec.getFieldValue("custrecord_stage_sfdc") == 'Closed Shelved') {
			var searchResult = nlapiSearchRecord("customrecord_fulfillment_dashboard_data",null,
					[
					 ["custrecord_fulfill_dashboard_opp_id","anyof",nlapiGetRecordId()]
					 ], 
					 [

					  ]
			);
			if (searchResult) {
				var dashboardDBId = searchResult[0].getId();
				nlapiSubmitField("customrecord_fulfillment_dashboard_data", dashboardDBId, "isinactive","T");
			}
		}
	}
}
