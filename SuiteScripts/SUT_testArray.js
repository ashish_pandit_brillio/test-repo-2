
// Global Array to hold employee id and details
var a_employee_id	=	new Array();
var a_employee_data = new Array();

function getArray(request, response) 
{
	try
	{
		var a_requiredSkills = [194,200,219,257,332,223];
		var family = 40;
		var resLocation = 'India : Bangalore';
		var searchResult = getEmployeeWithSkills(family,a_requiredSkills,resLocation);
		if(searchResult)
		{
			nlapiLogExecution('Debug','Search Result Length ',searchResult.length);
			nlapiLogExecution('Debug','Search Result Length ',JSON.stringify(searchResult));
			for(var i=0; i<searchResult.length;i++)
			{
				var a_EmpSkills = searchResult[i].getValue("custrecord_primary_updated","CUSTRECORD_EMPLOYEE_SKILL_UPDATED",null);
				a_EmpSkills = JSON.parse("[" + a_EmpSkills +"]");
				//nlapiLogExecution('Debug','a_EmpSkills ',a_EmpSkills);
				var employeeName = searchResult[i].getText("custrecord_employee_skill_updated");
				var empId = searchResult[i].getValue("custrecord_employee_skill_updated");
				if(a_EmpSkills)
				{
					var a_resultArray = common(a_requiredSkills,a_EmpSkills);
					//nlapiLogExecution('Debug',' a_resultArray ',a_resultArray);
					nlapiLogExecution('Debug','Maching Skill Length ',a_resultArray.length +'/'+a_requiredSkills.length);
				}
			}
		}
	}
	catch(error)
	{
		nlapiLogExecution('Debug','Error ',error);
	}
}

function common(arr1, arr2) {
  var newArr = [];
  newArr = arr1.filter(function(v){ return arr2.indexOf(v) >= 0;})
  newArr.concat(arr2.filter(function(v){ return newArr.indexOf(v) >= 0;}));

  return newArr;
}

// This function will return the employee details
function getEmployeeWithSkills(skillFamily, skills, resLocation) 
{
	try
	{
		var employee_filter	=	new Array();
		employee_filter[0]	=	new nlobjSearchFilter('custrecord_primary_updated', 'custrecord_employee_skill_updated', 'anyof', parseInt(skills));
		employee_filter[1]	=	new nlobjSearchFilter('custrecord_family_selected', 'custrecord_employee_skill_updated', 'anyof', skillFamily);
		employee_filter[2]	=	new nlobjSearchFilter('formulatext',null, 'startswith', resLocation);
		employee_filter[2].setFormula('{location}');
		employee_filter[3]	=	new nlobjSearchFilter('isinactive',null, 'is', false);
		
		
		var employee_columns =	new Array();
		employee_columns[0]	=	new nlobjSearchColumn('entityid');
		employee_columns[1]	=	new nlobjSearchColumn('internalid');
		employee_columns[2]	=	new nlobjSearchColumn("custrecord_family_selected","CUSTRECORD_EMPLOYEE_SKILL_UPDATED",null);
		employee_columns[3]	=	new nlobjSearchColumn("custrecord_primary_updated","CUSTRECORD_EMPLOYEE_SKILL_UPDATED",null);
		employee_columns[4]	=	new nlobjSearchColumn('department');
		employee_columns[5]	=	new nlobjSearchColumn('location');
		//var a_search_employee_details	=	nlapiSearchRecord('employee', null, employee_filter, employee_columns);
		var a_search_employee_details	=	nlapiSearchRecord(null, 'customsearch2635', null);
		
		var a_vendor_details	=	new Array();
		
		var i_count	= a_search_employee_details == null?0:a_search_employee_details.length;
		nlapiLogExecution('Debug','i_count Employee ',i_count);
		
		for(var i = 0; i < i_count; i++)
		{
			a_employee_id.push(a_search_employee_details[i].getValue('internalid'));
			//a_employee_data.push({'emp_id':a_search_employee_details[i].getValue('internalid'),'emp_name':a_search_employee_details[i].getValue('entityid'), 'skill_family':a_search_employee_details[i].getText("custrecord_family_selected","CUSTRECORD_EMPLOYEE_SKILL_UPDATED",null), 'skills':a_search_employee_details[i].getText("custrecord_primary_updated","CUSTRECORD_EMPLOYEE_SKILL_UPDATED",null), 'emp_practice':a_search_employee_details[i].getText('department'),'emp_location':a_search_employee_details[i].getText('location')});
			a_employee_data.push(a_search_employee_details[i].getValue("custrecord_primary_updated","CUSTRECORD_EMPLOYEE_SKILL_UPDATED",null));
		}
	
		return a_search_employee_details;
	}
	catch(e)
	{
		nlapiLogExecution('Debug','Error ',e);
	}
}


