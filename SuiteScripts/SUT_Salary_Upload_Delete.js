/**
 * @author Shweta
 */
// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
/*
    Script Name : SUT_Salary_Upload_Delete.js
	Author      : Shweta Chopde
	Date        : 6 Aug 2014
    Description : Delete the JEs created


	Script Modification Log:

	-- Date --			-- Modified By --				--Requested By--				-- Description --



Below is a summary of the process controls enforced by this script file.  The control logic is described
more fully, below, in the appropriate function headers and code blocks.


     SUITELET
		- suiteletFunction(request, response)


     SUB-FUNCTIONS
		- The following sub-functions are called by the above core functions in order to maintain code
            modularization:

               - NOT USED

*/
}
// END SCRIPT DESCRIPTION BLOCK  ====================================



// BEGIN GLOBAL VARIABLE BLOCK  =====================================
{
	//  Initialize any Global Variables, in particular, debugging variables...




}
// END GLOBAL VARIABLE BLOCK  =======================================





// BEGIN SUITELET ==================================================

function suiteletFunction(request, response)
{
	try 
	{
		var a_JE_IDs_arr = request.getParameter('custscript_je_array_sut');
		nlapiLogExecution('DEBUG', 'suiteletFunction', ' JE ID Array  -->' + a_JE_IDs_arr);
		
		var i_recordID = request.getParameter('custscript_record_id_delete_je');
		nlapiLogExecution('DEBUG', 'suiteletFunction', ' Record ID  -->' + i_recordID);
						
				
		// ================= Call Schedule Script ================
				
		 var params=new Array();
	     params['custscript_je_array'] = a_JE_IDs_arr
		 params['custscript_je_record_id_delete'] = i_recordID
		 
		 var status=nlapiScheduleScript('customscript_sch_salary_upload_delete',null,params);
		 nlapiLogExecution('DEBUG', 'suiteletFunction', ' Status -->' + status);
		 
		   if(status == 'QUEUED')
		 {
		 	if(_logValidation(i_recordID)) 
			{
				var o_recordOBJ = nlapiLoadRecord('customrecord_salary_upload_process',i_recordID)
				
				if(_logValidation(o_recordOBJ)) 
				{				
					o_recordOBJ.setFieldValue('custrecord_button_click_type','DELETE')					
					
					var i_submitID = nlapiSubmitRecord(o_recordOBJ,true,true);
					nlapiLogExecution('DEBUG', 'suiteletFunction', ' Submit ID -->' + i_submitID);
		 
					
				}//Record OBJ				
			}//Record ID			
		 }//QUEUED
		 	 
	     nlapiSetRedirectURL('RECORD', 'customrecord_salary_upload_process', i_recordID, null,null);
		 nlapiLogExecution('DEBUG', 'suiteletFunction', ' Suitelet Called ......... ' );	
	}
	catch(exception)
	{
	  nlapiLogExecution('DEBUG', 'ERROR',' Exception Caught -->' + exception);	
	}
}

// END SUITELET ====================================================

function _logValidation(value) 
{
 if(value!='null' && value != null && value != null && value != '' && value != undefined && value != undefined && value != 'undefined' && value!= 'undefined'&& value!= 'NaN' && value != NaN) 
 {
  return true;
 }
 else 
 { 
  return false;
 }
}


// BEGIN OBJECT CALLED/INVOKING FUNCTION ===================================================
{



}
// END OBJECT CALLED/INVOKING FUNCTION =====================================================
