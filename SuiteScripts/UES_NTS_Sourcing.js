// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
/*
   	Script Name:UES NTS Sourcing
	Author: Sachin K
	Company: Aashna Cloudtech - Brillio
	Date:12/23/2014
	Description:To source the Customer , Vertical , Project Description & Employee name in timesheet form.


	Script Modification Log:

	-- Date --			-- Modified By --				--Requested By--				-- Description --
     23/12/2014                                                                                 Shekar                                            


Below is a summary of the process controls enforced by this script file.  The control logic is described
more fully, below, in the appropriate function headers and code blocks.


     BEFORE LOAD
		- beforeLoadRecord(type)



     BEFORE SUBMIT
		- beforeSubmitRecord(type)


     AFTER SUBMIT
		- afterSubmitRecord(type)



     SUB-FUNCTIONS
		- The following sub-functions are called by the above core functions in order to maintain code
            modularization:

               - NOT USED

*/
}
// END SCRIPT DESCRIPTION BLOCK  ====================================



// BEGIN GLOBAL VARIABLE BLOCK  =====================================
{
	//  Initialize any Global Variables, in particular, debugging variables...




}
// END GLOBAL VARIABLE BLOCK  =======================================





// BEGIN BEFORE LOAD ==================================================

function beforeLoadRecord(type)
{

	/*  On before load:

          - EXPLAIN THE PURPOSE OF THIS FUNCTION

		-


		FIELDS USED:

          --Field Name--				--ID--


	*/



	//  LOCAL VARIABLES

    //  BEFORE LOAD CODE BODY


	return true;



}

// END BEFORE LOAD ====================================================





// BEGIN BEFORE SUBMIT ================================================

function beforeSubmitRecord(type)
{
    /*  On before submit:

          - PURPOSE
		-

          FIELDS USED:

          --Field Name--				--ID--


    */


    //  LOCAL VARIABLES


    //  BEFORE SUBMIT CODE BODY


	return true;

}

// END BEFORE SUBMIT ==================================================





// BEGIN AFTER SUBMIT =============================================

function afterSubmitRecord(type)
{
    /*  On after submit:

          - PURPOSE
		-

	FIELDS USED:

          --Field Name--				--ID--


    */


	//  LOCAL VARIABLES


if(type == 'delete')
{
    return;
}
var startDate = new Date();
	//  AFTER SUBMIT CODE BODY
	var a_days = ['sunday', 'monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday'];
	var recordId=nlapiGetRecordId()
	var recordType=nlapiGetRecordType()
	var recTS=nlapiLoadRecord(recordType,recordId)
	var lineCount = recTS.getLineItemCount('timegrid');

	//--------------------------------
	var i_employee = recTS.getFieldValue('employee');
	var i_employeeTxt = recTS.getFieldText('employee');

				if(_logValidation(i_employee))
				{
				   var o_employeeOBJ = nlapiLoadRecord('employee',i_employee);

					if (_logValidation(o_employeeOBJ))
					{
						i_practice = o_employeeOBJ.getFieldValue('department');
						nlapiLogExecution('DEBUG', 'afterSubmit_cross_project', ' Practice-->' + i_practice);
					}

				}//Employee
	//---------------------------------

	nlapiLogExecution('DEBUG', 'afterSubmit_NTS', ' lineCount->' + lineCount);
	
	// Store previous project id
	var i_previous_project_id	=	null; var o_previous_project	=	null;
	for(var i=1;i<=lineCount;i++)
    {
					/*o_recordOBJ.setFieldValue('custcolprj_name',s_project_description);
					o_recordOBJ.setFieldValue('custcolcustcol_temp_customer',i_customer);
					o_recordOBJ.setFieldValue('class',i_vertical);
					o_recordOBJ.setFieldValue('department',i_practice);*/

						recTS.selectLineItem('timegrid',i);

						for(var i_day_indx = 0; i_day_indx < 7; i_day_indx++)
						{
							var sub_record_name = a_days[i_day_indx];
							var o_sub_record_view = recTS.viewCurrentLineItemSubrecord('timegrid', sub_record_name);
							if(o_sub_record_view)
								{
									var approvalStatus = o_sub_record_view.getFieldValue('approvalstatus');
									//var isCurrentlyBillable = o_sub_record_view.getFieldValue('isbillable');

									if(approvalStatus == '1' || approvalStatus == '2')
									{
										var i_project_id = o_sub_record_view.getFieldValue('customer');//nlapiGetLineItemValue('timegrid', 'customer', i);
										
										nlapiLogExecution('DEBUG', 'afterSubmit_NTS', ' i_project_id->' + i_project_id);
										
										if(_logValidation(i_project_id))
 										{
						var o_projectOBJ = null;
						if (i_project_id != i_previous_project_id) {
							o_projectOBJ = nlapiLoadRecord('job', i_project_id);
							o_previous_project = o_projectOBJ;
							i_previous_project_id = i_project_id;
						} else {
							o_projectOBJ = o_previous_project;
						}

											if(_logValidation(o_projectOBJ))
											{
												var i_project_name_ID =  o_projectOBJ.getFieldValue('entityid');
												nlapiLogExecution('DEBUG', 'afterSubmit_cross_project', ' Project Name ID -->' + i_project_name_ID);

												var i_project_name =  o_projectOBJ.getFieldValue('companyname');
												nlapiLogExecution('DEBUG', 'afterSubmit_cross_project', ' Project Name -->' + i_project_name);

												var s_project_description = i_project_name_ID+' '+i_project_name;
												nlapiLogExecution('DEBUG', 'afterSubmit_cross_project', ' Project Description -->' + s_project_description);

												var i_customer =  o_projectOBJ.getFieldText('parent');
												nlapiLogExecution('DEBUG', 'afterSubmit_cross_project', ' Customer-->' + i_customer);

												 i_vertical =  o_projectOBJ.getFieldValue('custentity_vertical');
												 nlapiLogExecution('DEBUG', 'afterSubmit_cross_project', ' Vertical-->' + i_vertical);

													if(!_logValidation(i_customer))
													{
														i_customer = '';
													}
													if(!_logValidation(s_project_description))
													{
														s_project_description = '';
													}
													if(!_logValidation(i_vertical))
													{
														i_vertical = '';
													}
													if(!_logValidation(i_practice))
													{
														i_practice = '';
													}
											
													var o_sub_record = recTS.editCurrentLineItemSubrecord('timegrid', sub_record_name);
													o_sub_record.setFieldValue('class', i_vertical);
													o_sub_record.setFieldValue('department',i_practice );
													o_sub_record.setFieldValue('custrecordcustcol_temp_customer_ts', i_customer);
													o_sub_record.setFieldValue('custrecordprj_name_ts', s_project_description);//custrecord_employeenamecolumn_ts
													o_sub_record.setFieldValue('custrecord_employeenamecolumn_ts',i_employeeTxt );
													o_sub_record.commit();
													nlapiLogExecution('AUDIT', 'afterSubmit_NTS', 'Vertical-->'+i_vertical);
											
											}
										}
										
										
										
									}
											
								}

						}
						recTS.commitLineItem('timegrid');
				
				
    }

    var id=nlapiSubmitRecord(recTS);var endDate = new Date();
    nlapiLogExecution('DEBUG', 'Time: ' + ((endDate.getTime() - startDate.getTime())/1000.0) + ', afterSubmit_cross_project, type: ' + type, ' id-->' + id);
	return true;

}

// END AFTER SUBMIT ===============================================





// BEGIN FUNCTION ===================================================
{
function _logValidation(value)
{
 if(value != null && value.toString() != null && value != '' && value != undefined && value.toString() != undefined && value != 'undefined' && value.toString() != 'undefined'&& value.toString() != 'NaN' && value != NaN)
 {
  return true;
 }
 else
 {
  return false;
 }
}


}
// END FUNCTION =====================================================
