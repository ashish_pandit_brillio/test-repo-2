// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
/*
   	Script Name: SCH_Send_Ia_Report.js
	Author:     Rakesh K  
	Company:  	Brillio  
	Date:       03-01-2017 
	Version:    1.0

	Script Modification Log:

	-- Date --			-- Modified By --				--Requested By--				-- Description --




Below is a summary of the process controls enforced by this script file.  The control logic is described
more fully, below, in the appropriate function headers and code blocks.


     SCHEDULED FUNCTION
		- scheduledFunction(type)


     SUB-FUNCTIONS
		- The following sub-functions are called by the above core functions in order to maintain code
            modularization:

               - NOT USED

*/
}
// END SCRIPT DESCRIPTION BLOCK  ====================================


// BEGIN SCHEDULED FUNCTION =============================================

function schedulerFunction_sendEmailReport(type)
{
	try
	{
		
	var context = nlapiGetContext();
		var tds_account_name = '';
        nlapiLogExecution('DEBUG', 'schedulerFunctionsendemail', 'context ' + context);
        
        var s_start_date = context.getSetting('SCRIPT', 'custscript_start_date');
      //  var s_start_date = '12/1/2017';
        //nlapiLogExecution('DEBUG', 'schedulerFunctionsendemail', 'request==' + request);
        
        var s_end_date = context.getSetting('SCRIPT', 'custscript_end_date');
    //    var s_end_date = '12/31/2017';
		var i_vendor_id = context.getSetting('SCRIPT', 'custscript_vendor');
	//	i_vendor_id = 570;
		nlapiLogExecution('DEBUG', 'i_vendor_id', i_vendor_id);
        
        
        //var exchangeamtgbp = context.getSetting('SCRIPT', 'custscript_gbp_inr');
       
       // var exchangeamtusd = context.getSetting('SCRIPT', 'custscript_usd_inr');
       
		{
			var bill_data_arr = new Array();
			var bill_unique_list = new Array();
			var sr_no = 0;
			
			if(s_start_date)
			var d_start_date = nlapiStringToDate(s_start_date, 'date');
			if(s_end_date)
			var d_end_date = nlapiStringToDate(s_end_date, 'date');
			var vendor_actice_array = new Array();			
	
			
			
			if(_logValidation(i_vendor_id)){			  
			var filters_vendor_bill = new Array();
			
			filters_vendor_bill.push(new nlobjSearchFilter('type',null,'anyof',['VendBill']));
			filters_vendor_bill.push(new nlobjSearchFilter('subsidiary',null,'anyof',3));
			if(s_start_date && s_end_date){
			filters_vendor_bill.push(new nlobjSearchFilter('trandate',null,'within',[d_start_date, d_end_date]));
			}
			filters_vendor_bill.push(new nlobjSearchFilter('status',null,'anyof',['VendBill:A','VendBill:B']));
			filters_vendor_bill.push(new nlobjSearchFilter('internalid','vendor','anyof',parseInt(i_vendor_id)));
			}
			else{
			var filters_vendor_bill = [ 
								['type', 'anyof', [ 'VendBill']], 'and',
								['trandate', 'within', [d_start_date, d_end_date]], 'and',
								['status', 'anyof', [ 'VendBill:A','VendBill:B']], 'and',
								['subsidiary', 'anyof', 3]
							  ];
			
			}
			
			var columns_bill = new Array();
			columns_bill[0] = new nlobjSearchColumn('transactionnumber');
			columns_bill[1] = new nlobjSearchColumn('trandate');
			columns_bill[2] = new nlobjSearchColumn('duedate');
			columns_bill[3] = new nlobjSearchColumn('terms');
			columns_bill[4] = new nlobjSearchColumn('memo');
			columns_bill[5] = new nlobjSearchColumn('fxamount');
			columns_bill[6] = new nlobjSearchColumn('account');
			columns_bill[7] = new nlobjSearchColumn('entity');
			columns_bill[8] = new nlobjSearchColumn('tranid');
			columns_bill[9] = new nlobjSearchColumn('totalamount');
			columns_bill[10] = new nlobjSearchColumn('debitamount');
			columns_bill[11] = new nlobjSearchColumn('creditamount');
			columns_bill[12] = new nlobjSearchColumn('custcol_ait_swachh_cess_reverse');
			columns_bill[13] = new nlobjSearchColumn('custentity2', 'vendor');
			columns_bill[14] = new nlobjSearchColumn('custcol_tdsline');
			columns_bill[15] = new nlobjSearchColumn('custbody_billfrom');
			columns_bill[16] = new nlobjSearchColumn('custbody_billto');
			columns_bill[17] = new nlobjSearchColumn('custbody_vendorinvoicedate');
			columns_bill[18] = new nlobjSearchColumn('subsidiary');
			columns_bill[19] = new nlobjSearchColumn('currency');
			columns_bill[20] = new nlobjSearchColumn('type');	
			columns_bill[21] = new nlobjSearchColumn('exchangerate');	
			columns_bill[22] = new nlobjSearchColumn('taxamount');
			columns_bill[23] = new nlobjSearchColumn('custcol_baseamount');
			columns_bill[24] = new nlobjSearchColumn('location');
			//columns_bill[25] = new nlobjSearchColumn("custrecord_iit_address_gstn_uid","Address",null);
			//columns_bill[21] = new nlobjSearchColumn('name');
			//columns_bill[7].setSort(true);
			
			var vendor_bill_result = searchRecord('transaction', null, filters_vendor_bill, columns_bill);
			//var vendor_bill_result = searchRecord('transaction', 'customsearch2001', filters_vendor_bill, columns_bill);
			
			if (vendor_bill_result)
			{
				nlapiLogExecution('audit','srch len:- ',vendor_bill_result.length);
				//return;
				for (var index = 0; index < vendor_bill_result.length; index++)
				{
					var finalPartialamount = 0; 
					var previous_bill_id = 0;
					var st_amount = 0;
					var kkc_amount = 0;
					var sbc_amount = 0;
					var tds_amount = 0;
					var actual_amount = 0;
				//	var tds_account_name = '';
					var bill_internal_id = vendor_bill_result[index].getId();
					
					// code added by Rakesh K show partial paid amount at Excel
					//var filter_billPayment = new Array();
					
					if(context.getRemainingUsage() <= 1000)
					    {
					     yieldScript();
					    }
					var transaction_dueDate = vendor_bill_result[index].getValue('duedate');
					var fxamount = vendor_bill_result[index].getValue('fxamount');
					var account = vendor_bill_result[index].getValue('account');
					var account_text = vendor_bill_result[index].getText('account');
					var debit_amount = vendor_bill_result[index].getText('debitamount');
					var credit_amount = vendor_bill_result[index].getText('creditamount');
					var is_sbc_line = vendor_bill_result[index].getValue('custcol_ait_swachh_cess_reverse');
					var is_tds_line = vendor_bill_result[index].getValue('custcol_tdsline');
					
										
					var taxAmt = vendor_bill_result[index].getValue('taxamount');
					taxAmt = Math.abs(taxAmt);
					var baseAmt = vendor_bill_result[index].getValue('custcol_baseamount');
					baseAmt = Math.abs(baseAmt);
					fxamount = Math.abs(fxamount);
					var s_exchangerate = vendor_bill_result[index].getValue('exchangerate');
					fxamount = Math.abs(fxamount);
					//nlapiLogExecution('audit', 'account:- ' + account, fxamount);
					if (!transaction_dueDate) {
						if (account == 139 && is_sbc_line != 'T') {
							st_amount = fxamount;
						}
						else 
							if (account == 1047 && is_sbc_line != 'T') {
								kkc_amount = fxamount;
							}
							else 
								if (account == 1049 && is_sbc_line != 'T') {
									sbc_amount = fxamount;
								}
								else 
									if (is_tds_line == 'T') {
										tds_account_name = '';
										tds_amount = fxamount;
										tds_account_name = account_text;
									}
									else {
										if (is_sbc_line != 'T' && is_tds_line != 'T') //account == 564 && 
											actual_amount = fxamount;
									}
							if(taxAmt)
							taxAmt = taxAmt;
							if(baseAmt)
							baseAmt = baseAmt;
					}
					else {
						//nlapiLogExecution('audit','bill_internal_id:- ',bill_internal_id);
						var cloumns_billPayment = new Array();
						var filter_billPayment = [ 
													["type","anyof","VendPymt"], "AND",
													["appliedtotransaction.internalid","anyof",bill_internal_id]
												  ];
						cloumns_billPayment[0]= 	new nlobjSearchColumn("entity",null,null), 
						cloumns_billPayment[1]=    new nlobjSearchColumn("type",null,null), 
						cloumns_billPayment[2]=	new nlobjSearchColumn("fxamountpaid","appliedToTransaction",null);
						var searchBillPayment =  searchRecord('transaction', null, filter_billPayment, cloumns_billPayment);
						nlapiLogExecution('audit','searchBillPayment len:- ',searchBillPayment);
						
						
						if(context.getRemainingUsage() <= 1000)
					    {
					     yieldScript();
					    }
						if(searchBillPayment){
							for(var indexbill = 0; indexbill < searchBillPayment.length; indexbill++){
								
								var fxpartialamount = searchBillPayment[indexbill].getValue("fxamountpaid","appliedToTransaction");
								nlapiLogExecution('audit','fxpartialamount :- ',fxpartialamount);
								
								if(_logValidation(fxpartialamount)){
									finalPartialamount = parseFloat(finalPartialamount) + parseFloat(fxpartialamount);
									nlapiLogExecution('audit','finalPartialamount :- ',finalPartialamount);
								}
								if(context.getRemainingUsage() <= 1000)
							    {
							     yieldScript();
							    }
								
							}
							
						}
						
						var vendor_name = vendor_bill_result[index].getText('entity');
						var s_exchangerate = vendor_bill_result[index].getValue('exchangerate');
						//nlapiLogExecution('audit','vendor_name:- ',vendor_name);
						var i_vendor_id = vendor_bill_result[index].getValue('entity');
						var transaction_memo = vendor_bill_result[index].getValue('memo');
						var transaction_number = vendor_bill_result[index].getValue('transactionnumber');
						var transaction_date = vendor_bill_result[index].getValue('trandate');
						var terms = vendor_bill_result[index].getText('terms');
						var bill_ref_no = vendor_bill_result[index].getValue('tranid');
						var bill_subsidiary = vendor_bill_result[index].getText('subsidiary');
						var bill_currency = vendor_bill_result[index].getText('currency');
						var bill_from_date = vendor_bill_result[index].getValue('custbody_billfrom');
						var bill_to_date = vendor_bill_result[index].getValue('custbody_billto');
						var bill_vendor_invoice_date = vendor_bill_result[index].getValue('custbody_vendorinvoicedate');
						var o_vendor_rcrd_obj = nlapiLoadRecord('vendor', i_vendor_id);
						var vendor_open_balance = o_vendor_rcrd_obj.getFieldValue('balanceprimary');
						
						var transaction_type = vendor_bill_result[index].getValue('type');
						
					/*	//Search For GSTIN Number
						var filt = [];
						filt.push(new nlobjSearchFilter('internalid',null,'anyof',i_vendor_id));
						var cols = [];
						cols.push(new nlobjSearchColumn("custrecord_iit_address_gstn_uid","Address",null));
						
						var vendor_Search = nlapiSearchRecord('vendor',null,filt,cols);
						if(vendor_Search)
						var vendor_gstin = vendor_Search[0].getValue("custrecord_iit_address_gstn_uid","Address");
						else
						var vendor_gstin = ''; */
						//var addressCount = o_vendor_rcrd_obj.getLineItemCount('addressbook');
						//nlapiLogExecution('DEBUG','addressCount',addressCount);
					//	if(parseInt(addressCount)> parseInt(0)){
					//	var vendor_gstin = o_vendor_rcrd_obj.viewLineItemSubrecord('addressbook','custrecord_iit_address_gstn_uid',1);
						//nlapiLogExecution('DEBUG','vendor_gstin',vendor_gstin);
					//	}
					//	else{
					//	var vendor_gstin = '';
					//	}
						var bill_location = vendor_bill_result[index].getText('location');
						//var vendor_gstin = vendor_bill_result[index].getValue("custrecord_iit_address_gstn_uid","Address");
						//nlapiLogExecution('audit','transaction_type:- ',transaction_type);
						//nlapiLogExecution('audit','bill_from_date:- ',bill_from_date);
						//nlapiLogExecution('audit','bill_to_date:- ',bill_to_date);
						//nlapiLogExecution('audit','bill_currency:- ',bill_currency);
						
						// code added by Rakesh K 24-01-2017 multiply base currency 
						
						
						var bill_total = fxamount;
						
						if(bill_currency == 'USD'){
							var exchangeRate = nlapiExchangeRate('INR', 'USD');
							vendor_open_balance =parseFloat(vendor_open_balance) *parseFloat(exchangeRate);
							bill_total = parseFloat(bill_total) *parseFloat(exchangeRate);
						}
						if(bill_currency == 'EUR'){
							var exchangeRate = nlapiExchangeRate('INR', 'EUR');
							vendor_open_balance =parseFloat(vendor_open_balance) *parseFloat(exchangeRate);
							bill_total = parseFloat(bill_total) *parseFloat(exchangeRate);
						}
						if(bill_currency == 'GBP'){
							var exchangeRate = nlapiExchangeRate('INR', 'GBP');
							vendor_open_balance =parseFloat(vendor_open_balance) *parseFloat(exchangeRate);
							bill_total = parseFloat(bill_total) *parseFloat(exchangeRate);
						}
						var is_vendor_msme = vendor_bill_result[index].getText('custentity2', 'vendor');
						if (!is_vendor_msme) 
							is_vendor_msme = 'NO';
					}
					//nlapiLogExecution('audit','internal id:- ',bill_internal_id);
					if (bill_unique_list.indexOf(bill_internal_id) >= 0)
					{
						var index_exist = bill_unique_list.indexOf(bill_internal_id);
						var actual_amount_exist = bill_data_arr[index_exist].actual_amount;
						actual_amount_exist = parseFloat(actual_amount_exist) + parseFloat(actual_amount);
						var st_amount_exist = bill_data_arr[index_exist].st_amount;
						st_amount_exist = parseFloat(st_amount_exist) + parseFloat(st_amount);
						var sbc_amount_exist = bill_data_arr[index_exist].sbc_amount;
						sbc_amount_exist = parseFloat(sbc_amount_exist) + parseFloat(sbc_amount);
						var kkc_amount_exist = bill_data_arr[index_exist].kkc_amount;
						kkc_amount_exist = parseFloat(kkc_amount_exist) + parseFloat(kkc_amount);
						var tds_amount_exist = bill_data_arr[index_exist].tds_amount;
						tds_amount_exist = parseFloat(tds_amount_exist) + parseFloat(tds_amount);
						var tds_account_name_s = tds_account_name;
						//taxAmt
						var tax_amount_exist = bill_data_arr[index_exist].taxAmt;
						tax_amount_exist = parseFloat(tax_amount_exist) + parseFloat(taxAmt);
						//baseAmt
						var base_amount_exist = bill_data_arr[index_exist].baseAmt;
						base_amount_exist = parseFloat(base_amount_exist) + parseFloat(baseAmt);
						
						var vendor_name_exist = bill_data_arr[index_exist].vendor_name;
						var transaction_duedate_exist = bill_data_arr[index_exist].transaction_dueDate;
						var transaction_total_exist = bill_data_arr[index_exist].bill_total;
						var transaction_memo_exist = bill_data_arr[index_exist].transaction_memo;
						var bill_sub = bill_data_arr[index_exist].bill_subsidiary;
						var bill_exit_currency = bill_data_arr[index_exist].bill_currency;
						var bill_frm_date = bill_data_arr[index_exist].bill_from_date;
						var bill_exit_to_date = bill_data_arr[index_exist].bill_to_date;
						var bill_invoice_date = bill_data_arr[index_exist].bill_vendor_invoice_date;
						var vendor_open_bal = bill_data_arr[index_exist].vendor_open_balance;
						var tran_type = bill_data_arr[index_exist].transaction_type;
						
						var finalPartialamount =  bill_data_arr[index_exist].finalPartialamount; // final partail full columns addition 
						if(!transaction_duedate_exist){ // transaction Due Date 
							
							transaction_duedate_exist = transaction_dueDate;
							//nlapiLogExecution('audit','tran_type:- ',tran_type);
						}
						if(!tran_type){ // transaction type 
							
							tran_type = transaction_type;
							//nlapiLogExecution('audit','tran_type:- ',tran_type);
						}
						if(!vendor_open_bal){ // vendor opening balance
							
							vendor_open_bal = vendor_open_balance;
							//nlapiLogExecution('audit','vendor_open_bal:- ',vendor_open_bal);
						}
						if(!transaction_memo_exist){ // Transaction memo 
							
							transaction_memo_exist = transaction_memo;
							//nlapiLogExecution('audit','transaction_memo:- ',transaction_memo);
						}
						
						if(!transaction_total_exist){ // Net payable 
							
							transaction_total_exist = bill_total;
							//nlapiLogExecution('audit','transaction_total_exist:- ',transaction_total_exist);
						}
						if(!bill_sub){ // Subsidary  
							
							bill_sub = bill_subsidiary;
							//nlapiLogExecution('audit','bill_subsidiary:- ',bill_subsidiary);
						}
						if(!bill_exit_currency){ // Currency 
							
							bill_exit_currency = bill_currency;
							//nlapiLogExecution('audit','bill_currency:- ',bill_currency);
						}
						
						if(!bill_frm_date){ //Bill from date  
							
							bill_frm_date = bill_from_date;
							//nlapiLogExecution('audit','bill_frm_date:- ',bill_frm_date);
						}
						if(!bill_exit_to_date){ //Bill to date  
							
							bill_exit_to_date = bill_to_date;
							//nlapiLogExecution('audit','bill_to_date:- ',bill_to_date);
						}
						if(!bill_invoice_date){ //Invoice date   
							
							bill_invoice_date = bill_vendor_invoice_date;
							//nlapiLogExecution('audit','bill_invoice_date:- ',bill_invoice_date);
						}
						
							
							
						bill_data_arr[index_exist] = {
							'vendor_name': vendor_name,
							'bill_ref_no': bill_ref_no,
							'transaction_number': transaction_number,
							'transaction_date': transaction_date,
							'transaction_dueDate': transaction_duedate_exist,
							'terms': terms,
							'actual_amount': actual_amount_exist,
							'st_amount': st_amount_exist,
							'sbc_amount': sbc_amount_exist,
							'kkc_amount': kkc_amount_exist,
							'tds_amount': tds_amount_exist,
							'bill_total': transaction_total_exist,
							'transaction_memo': transaction_memo_exist,
							'is_vendor_msme': is_vendor_msme,
							'bill_subsidiary': bill_sub,
							'bill_currency': bill_exit_currency,
							'bill_from_date': bill_frm_date,
							'bill_to_date': bill_exit_to_date,
							'bill_vendor_invoice_date': bill_invoice_date,
							'vendor_open_balance': vendor_open_bal,
							'transaction_type': tran_type,
							'finalPartialamount':finalPartialamount,
							'tds_account_name_t': tds_account_name_s,
							'taxAmt':nlapiLookupField('vendorbill',bill_internal_id,'taxtotal'),
							'baseAmt':base_amount_exist,
							'exchangeRate':s_exchangerate,
							'location_': bill_location
					
						};
						
					}
					else
					{
						bill_unique_list.push(bill_internal_id);
						bill_data_arr[sr_no] = {
							'vendor_name': vendor_name,
							'bill_ref_no': bill_ref_no,
							'transaction_number': transaction_number,
							'transaction_date': transaction_date,
							'transaction_dueDate': transaction_dueDate,
							'terms': terms,
							'actual_amount': actual_amount,
							'st_amount': st_amount,
							'sbc_amount': sbc_amount,
							'kkc_amount': kkc_amount,
							'tds_amount': tds_amount,
							'bill_total': bill_total,
							'transaction_memo': transaction_memo,
							'is_vendor_msme': is_vendor_msme,
							'bill_subsidiary': bill_subsidiary,
							'bill_currency': bill_currency,
							'bill_from_date': bill_from_date,
							'bill_to_date': bill_to_date,
							'bill_vendor_invoice_date': bill_vendor_invoice_date,
							'vendor_open_balance': vendor_open_balance,
							'transaction_type': transaction_type,
							'finalPartialamount':finalPartialamount,
							'tds_account_name_t': tds_account_name_s,
							'taxAmt':nlapiLookupField('vendorbill',bill_internal_id,'taxtotal'),
							'baseAmt':baseAmt,
							'exchangeRate':s_exchangerate,
							'location_': bill_location
						};
						sr_no++;
						
					}
					
					previous_bill_id = bill_internal_id;
				}
				nlapiLogExecution('audit','tran len:- ',bill_data_arr.length);
				//return;
				down_excel_function(bill_data_arr);
			}
		}
	}
	catch(err)
	{
		nlapiLogExecution('ERROR','ERROR MESSAGE :- ',err);
	}

  
}

// END SCHEDULED FUNCTION ===============================================


function down_excel_function(bill_data_arr)
{
	try
	{
		var strVar1= '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:ss="urn:schemas-microsoft-com:office:spreadsheet" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40">'+
		'<head>'+
		'<meta http-equiv=Content-Type content="text/html; charset=windows-1252"/>'+
        '<meta name=ProgId content=Excel.Sheet/>'+
        '<meta name=Generator content="Microsoft Excel 11"/>'+
        '<!--[if gte mso 9]><xml>'+
        '<x:excelworkbook>'+
        '<x:excelworksheets>'+
        '<x:excelworksheet=sheet1>'+
        '<x:name>** ESTIMATE FILE**</x:name>'+
        '<x:worksheetoptions>'+
        '<x:selected></x:selected>'+
        '<x:freezepanes></x:freezepanes>'+
        '<x:frozennosplit></x:frozennosplit>'+
        '<x:splithorizontal>** FROZEN ROWS + 1 **</x:splithorizontal>'+
        '<x:toprowbottompane>** FROZEN ROWS + 1 **</x:toprowbottompane>'+
        '<x:splitvertical>** FROZEN COLUMNS + 1 **</x:splitvertical>'+
        '<x:leftcolumnrightpane>** FROZEN COLUMNS + 1**</x:leftcolumnrightpane>'+
		    '<x:activepane>0</x:activepane>'+// 0
		    '<x:panes>'+
			'<x:pane>'+
			'<x:number>3</x:number>'+
			'</x:pane>'+
			'<x:pane>'+
		    '<x:number>1</x:number>'+
		    '</x:pane>'+
		    '<x:pane>'+
			'<x:number>2</x:number>'+
        '</x:pane>'+
        '<x:pane>'+
        '<x:number>0</x:number>'+//1
        '</x:pane>'+
        '</x:panes>'+
        '<x:protectcontents>False</x:protectcontents>'+
        '<x:protectobjects>False</x:protectobjects>'+
        '<x:protectscenarios>False</x:protectscenarios>'+
        '</x:worksheetoptions>'+
        '</x:excelworksheet>'+
		'</x:excelworksheets>'+
        '<x:protectstructure>False</x:protectstructure>'+
        '<x:protectwindows>False</x:protectwindows>'+
		'</x:excelworkbook>'+
        
        // '<style>'+
			
			//-------------------------------------
        '</x:excelworkbook>'+
        '</xml><![endif]-->'+
         '<style>'+
		'p.MsoFooter, li.MsoFooter, div.MsoFooter'+
		'{ margin:0in; margin-bottom:.0001pt; mso-pagination:widow-orphan; tab-stops:center 3.0in right 6.0in; font-size:12.0pt;}'+
		'<style>'+

		'<!-- /* Style Definitions */'+

		//'@page Section1'+
		//'{ size:8.5in 11.0in; margin:1.9cm 1.27cm 1.9cm 1.27cm; mso-header-margin:.5in; mso-footer-margin:.5in; mso-title-page:yes; mso-header: h1; mso-footer: f1; mso-first-header: fh1; mso-first-footer: ff1; mso-paper-source:0;}'+

		'div.Section1'+
		'{ page:Section1;}'+

		'table#hrdftrtbl'+
		'{ margin:0in 0in 0in 900in; width:1px; height:1px; overflow:hidden; font-family:Arial;}-->'+

		'</style>'+
		
		
		'</head>'+
		

		'<body>'+
		
		'<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">'
		
		var strVar2 = '';    
		strVar2 += "<table width=\"105%\">";
		strVar2 += "<tr>";
		strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \>Vendor</td>";
		strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \>Transaction Type</td>";
		strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \>Ref No.</td>";
		strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \>Transaction Number</td>";
		strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \>Date</td>";
		strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \>Due Date</td>";
		strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \>Age</td>";
		strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \>Credit Terms</td>";
		strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \>Amount(Gross)</td>";
		strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \>ST</td>";
		strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \>SBC</td>";
		strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \>KKC</td>";
		strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \>TDS</td>";
		strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \>TDS Account</td>";
		strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \>Partial Paid Amount</td>";
		strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \>Net Payable</td>";
		strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \>Memo</td>";
		strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \>MSME Vendor</td>";
		strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \>Subsidiary</td>";
		strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \>Currency</td>";
		strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \>Billing From Date</td>";
		strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \>Billing To Date</td>";
		strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \>Vendor Invoice Date</td>";
		strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \>Vendor Open Balance</td>";
		strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \>Exchange Rate</td>";
		strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \>Tax Amount</td>";
		strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \>Base Amount</td>";
		strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \>Location</td>";
		
		strVar2 += "<\/tr>";
		
		
		if (bill_data_arr)
		{
			nlapiLogExecution('audit','down_excel_function transcation length:- ',bill_data_arr.length);
			for (var jk = 0; jk < bill_data_arr.length; jk++)
			{
				var age_in_days = getDatediffIndays(bill_data_arr[jk].transaction_date,nlapiDateToString(new Date()));
				var tds_ = bill_data_arr[jk].tds_amount;
				var tds_t = '';
				if(parseFloat(tds_)>0)
					tds_t = bill_data_arr[jk].tds_account_name_t;
									
				nlapiLogExecution('audit','down_excel_function vendor name:- ',bill_data_arr[jk].vendor_name);
				strVar2 += "<tr>";
				strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \>" +bill_data_arr[jk].vendor_name+ "</td>";
				strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \>" +bill_data_arr[jk].transaction_type+ "</td>";
				strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \>" +bill_data_arr[jk].bill_ref_no+ "</td>";
				strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \>" +bill_data_arr[jk].transaction_number+ "</td>";
				strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \>" +bill_data_arr[jk].transaction_date+ "</td>";
				strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \>" +bill_data_arr[jk].transaction_dueDate+ "</td>";
				strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \>" +age_in_days+ "</td>";
				strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \>" +bill_data_arr[jk].terms+ "</td>";
				strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \>" +bill_data_arr[jk].actual_amount+ "</td>";
				strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \>" +bill_data_arr[jk].st_amount+ "</td>";
				strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \>" +bill_data_arr[jk].sbc_amount+ "</td>";
				strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \>" +bill_data_arr[jk].kkc_amount+ "</td>";
				strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \>" +tds_+ "</td>";
				strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \>" +tds_t+ "</td>";
				strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \>" +bill_data_arr[jk].finalPartialamount+ "</td>";
				strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \>" +bill_data_arr[jk].bill_total+ "</td>";
				strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \>" +bill_data_arr[jk].transaction_memo+ "</td>";
				strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \>" +bill_data_arr[jk].is_vendor_msme+ "</td>";
				strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \>" +bill_data_arr[jk].bill_subsidiary+ "</td>";
				
				strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \>" +bill_data_arr[jk].bill_currency+ "</td>";
				strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \>" +bill_data_arr[jk].bill_from_date+ "</td>";
				strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \>" +bill_data_arr[jk].bill_to_date+ "</td>";
				strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \>" +bill_data_arr[jk].bill_vendor_invoice_date+ "</td>";
				strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \>" +bill_data_arr[jk].vendor_open_balance+ "</td>";
				
				strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \>" +bill_data_arr[jk].exchangeRate+ "</td>";
				strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \>" +bill_data_arr[jk].taxAmt+ "</td>";
				strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \>" +bill_data_arr[jk].baseAmt+ "</td>";
				strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \>" +bill_data_arr[jk].location_+ "</td>";
				
				strVar2 += "<\/tr>";
			}
		}
		else
		{
			//NO RESULTS FOUND
			
		}
					
		strVar2 += "<\/table>";			
		strVar1 = strVar1 + strVar2;
		var file = nlapiCreateFile('Vendor Bill Aging Report.xls', 'XMLDOC', strVar1);
		nlapiLogExecution('debug','file:- ',file);
		
		var curr_user = nlapiGetUser();
		var s_email = nlapiLookupField('employee', curr_user, 'email');
		//response.setContentType('XMLDOC','Vendor Bill Aging Report.xls');
		var a_emp_attachment = new Array();
		a_emp_attachment['entity'] = curr_user;
		nlapiSendEmail(442, s_email, 'Vendor Bill Aging Report', 'Please find the attachement of vendor bill aging report', null, null, a_emp_attachment, file, true, null, null);
		//response.write( file.getValue() );
		//window.close();
	}
	catch(err)
	{
		nlapiLogExecution('ERROR','ERROR MESSAGE:- ',err);
	}
}


// BEGIN FUNCTION ===================================================
{
 
	function getDatediffIndays(startDate, endDate)
	{
	    var one_day=1000*60*60*24;
	    var fromDate = startDate;
	    var toDate=endDate

		var date1 = nlapiStringToDate(fromDate);

	    var date2 = nlapiStringToDate(toDate);

	    var date3=Math.round((date2-date1)/one_day);

	    return (date3);
	}

	function searchRecord(recordType, savedSearch, arrFilters, arrColumns,  filterExpression){

		try {
			var search = null;

			// if a saved search is provided, load it and add the filters and
			// columns
			if (isNotEmpty(savedSearch)) {
				search = nlapiLoadSearch(recordType, savedSearch);

				if (isArrayNotEmpty(arrFilters)) {
					search.addFilters(arrFilters);
				}

				if (isArrayNotEmpty(arrColumns)) {
					search.addColumns(arrColumns);
				}

				if (isArrayNotEmpty(filterExpression)) {
					search.setFilterExpression(filterExpression);
				}
			}
			// create a new search
			else {
				search = nlapiCreateSearch(recordType, arrFilters, arrColumns);
			}

			// run search
			var resultSet = search.runSearch();

			// iterate through the search and get all data 1000 at a time
			var searchResultCount = 0;
			var resultSlice = null;
			var searchResult = [];

			do {
				resultSlice = resultSet.getResults(searchResultCount,
				        searchResultCount + 1000);

				if (resultSlice) {

					resultSlice.forEach(function(result) {

						searchResult.push(result);
						searchResultCount++;
					});
				}
			} while (isArrayNotEmpty(resultSlice) && resultSlice.length >= 1000);

			return searchResult;
		} catch (err) {
			nlapiLogExecution('ERROR', 'searchRecord', err);
			throw err;
		}
	}

	function isEmpty(value) {

		return value == null || value == "" || typeof (value) == undefined;
	}

	function isNotEmpty(value) {

		return !isEmpty(value);
	}

	function isArrayEmpty(argArray) {

		return !isArrayNotEmpty(argArray);
	}

	function isArrayNotEmpty(argArray) {

		return (isNotEmpty(argArray) && argArray.length > 0);
	}

}
function yieldScript() {
	 
	 nlapiLogExecution('AUDIT', 'API Limit Exceeded');
	 var state = nlapiYieldScript();

	 if (state.status == "FAILURE") {
	  nlapiLogExecution('ERROR', 'Yield Script Failed', 'Reason : '
	          + state.reason + ' / Size : ' + state.size);
	  return false;
	 } else if (state.status == "RESUME") {
	  nlapiLogExecution('AUDIT', 'Script Resumed');
	 }
	}
// END FUNCTION =====================================================
function _logValidation(value)
{
	if (value != null && value != '' && value != undefined && value != 'undefined' && value != 'NaN') 
	{
	    return true;
	}
	else 
	{
	    return false;
	}
 }
