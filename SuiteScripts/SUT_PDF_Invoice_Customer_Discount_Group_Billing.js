/**
 * Generate PDF for invoice with customer discount
 * 
 * Version Date Author Remarks 1.00 20 Aug 2015 Nitish Mishra
 * 
 */

function suitelet(request, response) {
	try {
		var invoiceId = request.getParameter('invoice');

		if (invoiceId) {
			var xmlContent = "";
			var cssFileUrl = nlapiEscapeXML("https://system.na1.netsuite.com/core/media/media.nl?id=38485&c=3883006&h=ff923e0edfab23fa6bf7&_xt=.css");

			var companyDetails = getCompanyDetails();
			var invoiceDetails = getInvoiceDetails(request
			        .getParameter('invoice'));

			nlapiLogExecution('debug', 'invoice data', JSON
			        .stringify(invoiceDetails));

			var customerDetails = getCustomerDetails(invoiceDetails.Customer);
			var projectDetails = getProjectDetails(invoiceDetails.Project);

			xmlContent += "<?xml version='1.0'?>";
			xmlContent += "<pdf><head>";
			xmlContent += "<link SRC='" + cssFileUrl + "' type='stylesheet'/>";
			xmlContent += "<meta name='title' value='Invoice # "
			        + invoiceDetails.InvoiceNumber + "'/>";
			xmlContent += "<macrolist>";
			xmlContent += "<macro id='pageFooter' footer-height='100mm'>";
			xmlContent += "<div style='footer-div'>";
			xmlContent += "<hr class='footer-line' width='100%'/>";
			xmlContent += "<p class='footer' align='center'>Brillio LLC, certifies that this invoice is correct, that payment has not been received and"
			        + " that it is presented with the<br/>"
			        + " understanding that the amount to be paid there under can be audited to verify.</p>";
			xmlContent += "</div>";
			xmlContent += "</macro>";
			xmlContent += "</macrolist>";

			xmlContent += "</head>";
			xmlContent += "<body footer='pageFooter' footer-height='20mm'>";

			for (var i = 0; i < invoiceDetails.GroupDiscountDetails.length; i++) {
				xmlContent += mainPage(invoiceDetails, customerDetails,
				        companyDetails, projectDetails, i);
			}

			xmlContent += "</body>";
			xmlContent += "</pdf>";

			response.renderPDF(xmlContent);
		} else {
			response.writePage('No Invoice Id Provided');
		}
	} catch (err) {
		nlapiLogExecution('ERROR', 'suitelet', err);
		// throw err;
	}
}

function mainPage(invoiceDetails, customerDetails, companyDetails,
        projectDetails, empIndex)
{
	try {
		var companyLogoUrl = nlapiEscapeXML(companyDetails.LogoUrl);

		var pdfText = "";
		pdfText += "<table width='100%' class='table1'>"; // T1

		// ---------------------------------------------------
		pdfText += "<tr class='underline-row main-header-row'>";

		pdfText += "<td width='50%'>";
		pdfText += "<img class='companyLogo' src='" + companyLogoUrl + "'/>";
		pdfText += "</td>";

		pdfText += "<td align='right'>";
		pdfText += "<div class='address-div'><p>";
		pdfText += companyDetails.Name
		        + "<br/>"
		        + companyDetails.AddressLine1
		        + "<br/>"
		        + companyDetails.AddressLine2
		        + "<br/>"
		        + companyDetails.City
		        + ", "
		        + companyDetails.State
		        + " "
		        + companyDetails.Zip
		        + "<br/>"
		        + (companyDetails.Phone ? ("Phone: " + companyDetails.Phone + "<br/>")
		                : "")
		        + (companyDetails.Fax ? ("Fax: " + companyDetails.Fax + "")
		                : "");
		pdfText += "</p></div>";
		pdfText += "</td>";

		pdfText += "</tr>";

		// ---------------------------------------------------

		// pdfText += "<tr><td colspan='2'>";
		// pdfText += "<hr width='100%'/>";
		// pdfText += "</td></tr>";

		// ---------------------------------------------------

		pdfText += "<tr class='add-row-padding'>";

		pdfText += "<td>";
		pdfText += "<span class='label'>Invoice Date : </span>";
		pdfText += "<span class='value'>"
		        + formatDateText(invoiceDetails.InvoiceDate) + "</span>";
		pdfText += "</td>";
		pdfText += "<td>";
		pdfText += "<span class='label'>Invoice Number : </span>";
		pdfText += "<span class='value'>" + invoiceDetails.InvoiceNumber
		        + "</span>";
		pdfText += "</td>";

		pdfText += "</tr>";

		// ---------------------------------------------------

		pdfText += "<tr>";

		pdfText += "<td>";
		pdfText += "<span class='label close-span'>Client Information : </span>";
		pdfText += "<div class='address-block'><p>" + customerDetails.Email
		        + "<br/>" + invoiceDetails.BillingAddressLine1 + "<br/>"
		        + invoiceDetails.BillingAddressLine2 + "<br/>"
		        + invoiceDetails.BillingAddressCity + " , "
		        + invoiceDetails.BillingAddressState + " "
		        + invoiceDetails.BillingAddressZip + "</p></div>";
		pdfText += "</td>";

		pdfText += "<td>";
		pdfText += "<span class='label close-span'>Remit Payment To : </span>";
		pdfText += "<div class='address-block'><p>" + companyDetails.Name
		        + "<br/>" + companyDetails.AddressLine1 + "<br/>"
		        + companyDetails.AddressLine2 + "<br/>" + companyDetails.City
		        + ", " + companyDetails.State + " " + companyDetails.Zip
		        + "</p></div>";
		pdfText += "</td>";

		pdfText += "</tr>";

		// ---------------------------------------------------
		pdfText += "<tr>";

		pdfText += "<td>";
		pdfText += "<span class='label'>Project : </span>";
		pdfText += "<span class='value'>" + projectDetails.ProjectId
		        + "</span>";
		pdfText += "</td>";
		pdfText += "<td>";
		pdfText += "<span class='label'>Terms : </span>";
		pdfText += "<span class='value'>" + invoiceDetails.Terms + "</span>";
		pdfText += "</td>";

		pdfText += "</tr>";

		// ---------------------------------------------------

		pdfText += "<tr>";

		pdfText += "<td>";
		pdfText += "<span class='label'>PO Number : </span>";
		pdfText += "<span class='value'>" + invoiceDetails.PONumber + "</span>";
		pdfText += "</td>";
		pdfText += "<td>";
		pdfText += "</td>";

		pdfText += "</tr>";

		// ---------------------------------------------------

		pdfText += "<tr>";

		pdfText += "<td>";
		pdfText += "<span class='label'>Period Beginning "
		        + formatDate(invoiceDetails.BillingFrom) + " through "
		        + formatDate(invoiceDetails.BillingTo) + "</span>";
		pdfText += "</td>";
		pdfText += "<td>";
		pdfText += "</td>";

		pdfText += "</tr>";

		// ---------------------------------------------

		pdfText += "</table>"; // T1

		// ---------------------------------------------------
		// ---------------------------------------------------

		pdfText += "<table width='100%' class='table2'>"; // T2

		// ---------------------------------------------------

		pdfText += "<tr>";

		pdfText += "<td>";
		pdfText += "<p><b>Invoice Comments : </b></p>";
		pdfText += insertInvoiceDetailsTable(invoiceDetails, empIndex);
		pdfText += "</td>";

		pdfText += "</tr>";

		// ---------------------------------------------------

		pdfText += "<tr>";

		pdfText += "<td class='blankDiv'>";
		pdfText += "";
		pdfText += "</td>";

		pdfText += "</tr>";

		// ---------------------------------------------------

		// pdfText += "<tr>";

		// pdfText += "<td align='center'>";

		// pdfText += "<hr width='100%'/>";

		// pdfText += "<div class='footer'>";
		// pdfText += "<p align='center'>Brillio LLC, certifies that this
		// invoice is correct, that payment has not been received and"
		// + " that it is presented with the<br/>"
		// + " understanding that the amount to be paid there under can be
		// audited to verify.</p>";
		// pdfText += "</div>";

		// pdfText += "</td>";

		// pdfText += "</tr>";

		// ---------------------------------------------------

		pdfText += "</table>"; // T2

		// ---------------------------------------------------
		// ---------------------------------------------------

		return pdfText;
	} catch (err) {
		nlapiLogExecution('ERROR', 'mainPage', err);
		throw err;
	}
}

function insertInvoiceDetailsTable(invoiceDetails, empIndex) {
	var table = "<table width='100%' class='table2'>";

	table += "<tr class='invoice-table-header'>";
	table += "<td>Details</td>";
	table += "<td>ST Rate</td>";
	table += "<td>ST Units</td>";
	table += "<td>OT Rate</td>";
	table += "<td>OT Units</td>";
	table += "<td>Amount</td>";
	table += "</tr>";

	table += "<tr class='invoice-table-cell'>";
	table += "<td><b>"
	        + invoiceDetails.GroupDiscountDetails[empIndex].EmployeeName
	        + "</b></td>";
	table += "<td></td>";
	table += "<td></td>";
	table += "<td></td>";
	table += "<td></td>";
	table += "</tr>";

	table += "<tr>";
	table += "<td></td>";
	table += "<td>"
	        + formatCurrency(invoiceDetails.GroupDiscountDetails[empIndex].ST_Rate)
	        + "</td>";
	table += "<td>" + invoiceDetails.GroupDiscountDetails[empIndex].ST_Unit
	        + "</td>";
	table += "<td>"
	        + formatCurrency(invoiceDetails.GroupDiscountDetails[empIndex].OT_Rate)
	        + "</td>";
	table += "<td>" + invoiceDetails.GroupDiscountDetails[empIndex].OT_Unit
	        + "</td>";
	table += "<td align='right'>"
	        + formatCurrency(invoiceDetails.GroupDiscountDetails[empIndex].GrossAmount)
	        + "</td>";
	table += "</tr>";

	table += "<tr>";
	table += "<td>Less : VMS Fee Discount</td>";
	table += "<td>" + invoiceDetails.FixedFeeDiscountPercent + "</td>";
	table += "<td></td>";
	table += "<td></td>";
	table += "<td></td>";
	table += "<td align='right'>( "
	        + formatCurrency(invoiceDetails.FixedFeeDiscountAmount) + " )</td>";
	table += "</tr>";

	table += "<tr>";
	table += "<td>Less : Tenure Discount</td>";
	table += "<td>"
	        + invoiceDetails.GroupDiscountDetails[empIndex].TenureDiscountPercent
	        + "</td>";
	table += "<td></td>";
	table += "<td></td>";
	table += "<td></td>";
	table += "<td align='right' class='underline-cell'>( "
	        + formatCurrency(invoiceDetails.GroupDiscountDetails[empIndex].TenureDiscountAmount)
	        + " )</td>";
	table += "</tr>";

	table += "<tr>";
	table += "<td><b>Total</b></td>";
	table += "<td></td>";
	table += "<td></td>";
	table += "<td></td>";
	table += "<td></td>";
	table += "<td align='right'><b>"
	        + formatCurrency(invoiceDetails.AmountLessVmsTenure) + "</b></td>";
	table += "</tr>";

	table += "<tr>";
	table += "<td>Less : Volume Discount</td>";
	table += "<td>" + invoiceDetails.VolumeDiscountPercent + "</td>";
	table += "<td></td>";
	table += "<td></td>";
	table += "<td></td>";
	table += "<td align='right' class='underline-cell'>( "
	        + formatCurrency(invoiceDetails.VolumeDiscountAmount) + " )</td>";
	table += "</tr>";

	table += "<tr class='invoice-table-footer'>";
	table += "<td></td>";
	table += "<td></td>";
	table += "<td></td>";
	table += "<td>Invoice Total</td>";
	table += "<td></td>";
	table += "<td align='right'>"
	        + formatCurrency(invoiceDetails.FinalInvoiceAmount) + "</td>";
	table += "</tr>";

	table += "</table>";

	return table;
}

function getCompanyDetails() {
	try {
		var subsidiaryId = 2;
		var subsidiaryRecord = nlapiLoadRecord('subsidiary', subsidiaryId);

		return {
		    LogoUrl : constant.Images.CompanyLogo,
		    Name : subsidiaryRecord.getFieldValue('addressee'),
		    AddressLine1 : subsidiaryRecord.getFieldValue('addr1'),
		    AddressLine2 : subsidiaryRecord.getFieldValue('addr2'),
		    City : subsidiaryRecord.getFieldValue('city'),
		    State : subsidiaryRecord.getFieldValue('state'),
		    Zip : subsidiaryRecord.getFieldValue('zip'),
		    Phone : subsidiaryRecord.getFieldValue('addrphone'),
		    Fax : subsidiaryRecord.getFieldValue('fax')
		};
	} catch (err) {
		nlapiLogExecution('error', 'getCompanyDetails', err);
		throw err;
	}
}

function getInvoiceDetails(invoiceId) {
	try {
		var invoiceRecord = nlapiLoadRecord('invoice', invoiceId);
		var discountRecordId = invoiceRecord
		        .getFieldValue('custbody_inv_cdd_temp_field');
		var discountRecord = nlapiLoadRecord(
		        'customrecord_customer_discount_details', discountRecordId);

		var employeeCount = discountRecord
		        .getLineItemCount('recmachcustrecord_tdr_customer_discount');

		var tenureDiscountDetails = [];
		var totalTenureDiscount = 0;

		for (var linenum = 1; linenum <= employeeCount; linenum++) {
			var employeeId = discountRecord.getLineItemValue(
			        'recmachcustrecord_tdr_customer_discount',
			        'custrecord_tdr_employee', linenum);
			var tenureDiscountAmount = discountRecord.getLineItemValue(
			        'recmachcustrecord_tdr_customer_discount',
			        "custrecord_tdr_final_discount_amt", linenum);
			var tenureDiscountPercent = discountRecord.getLineItemValue(
			        'recmachcustrecord_tdr_customer_discount',
			        "custrecord_tdr_final_discount_percent", linenum);
			var st_Hours = discountRecord.getLineItemValue(
			        'recmachcustrecord_tdr_customer_discount',
			        "custrecord_tdr_st_hours", linenum);
			var st_Rate = discountRecord.getLineItemValue(
			        'recmachcustrecord_tdr_customer_discount',
			        "custrecord_tdr_st_rate", linenum);
			var ot_Hours = discountRecord.getLineItemValue(
			        'recmachcustrecord_tdr_customer_discount',
			        "custrecord_tdr_ot_hours", linenum);
			var ot_Rate = discountRecord.getLineItemValue(
			        'recmachcustrecord_tdr_customer_discount',
			        "custrecord_tdr_ot_rate", linenum);

			st_Hours = st_Hours ? st_Hours : 0;
			st_Rate = st_Rate ? st_Rate : 0;
			ot_Hours = ot_Hours ? ot_Hours : 0;
			ot_Rate = ot_Rate ? ot_Rate : 0;

			var totalAmountWorked = (parseFloat(st_Hours) * parseFloat(st_Rate) + parseFloat(ot_Hours)
			        * parseFloat(ot_Rate)).toFixed(2);

			var employeeDetails = nlapiLookupField('employee', employeeId, [
			        'firstname', 'lastname' ]);

			tenureDiscountDetails.push({
			    EmployeeId : employeeId,
			    EmployeeName : employeeDetails.firstname + " "
			            + employeeDetails.lastname,
			    TenureDiscountPercent : tenureDiscountPercent,
			    TenureDiscountAmount : tenureDiscountAmount,
			    ST_Unit : st_Hours,
			    ST_Rate : st_Rate,
			    OT_Unit : ot_Hours,
			    OT_Rate : ot_Rate,
			    GrossAmount : totalAmountWorked
			});

			totalTenureDiscount += parseFloat(tenureDiscountAmount);
		}

		return {
		    Customer : invoiceRecord.getFieldValue('entity'),
		    Project : invoiceRecord.getFieldValue('job'),
		    InvoiceDate : invoiceRecord.getFieldValue('trandate'),
		    InvoiceNumber : invoiceRecord.getFieldValue('tranid'),
		    Terms : '1% 15 net 30',
		    PONumber : nlapiEscapeXML(invoiceRecord
		            .getFieldValue('otherrefnum')),
		    BillingFrom : invoiceRecord.getFieldValue('custbody_billfrom'),
		    BillingTo : invoiceRecord.getFieldValue('custbody_billto'),

		    FixedFeeDiscountPercent : discountRecord
		            .getFieldValue('custrecord_cdd_fixed_fee_percent'),
		    FixedFeeDiscountAmount : discountRecord
		            .getFieldValue('custrecord_cdd_fixed_fee_discount'),

		    GroupDiscountDetails : tenureDiscountDetails,

		    GrossAmount : discountRecord
		            .getFieldValue('custrecord_cdd_gross_amount'),

		    VolumeDiscountPercent : discountRecord
		            .getFieldValue('custrecord_cdd_volume_discount_percent'),
		    VolumeDiscountAmount : discountRecord
		            .getFieldValue('custrecord_cdd_volume_discount'),

		    AmountLessVmsTenure : discountRecord
		            .getFieldValue('custrecord_cdd_gross_amount')
		            - totalTenureDiscount,
		    FinalInvoiceAmount : discountRecord
		            .getFieldValue('custrecord_cdd_final_invoice_amount'),
		    BillingAddress : nlapiEscapeXML(invoiceRecord
		            .getFieldValue('billaddress')),
		    Terms : nlapiEscapeXML(invoiceRecord.getFieldText('terms')),
		    BillingAddressLine1 : nlapiEscapeXML(invoiceRecord
		            .getFieldValue('billaddr1')),
		    BillingAddressLine2 : nlapiEscapeXML(invoiceRecord
		            .getFieldValue('billaddr2')),
		    BillingAddressLine3 : nlapiEscapeXML(invoiceRecord
		            .getFieldValue('billaddr2')),
		    BillingAddressCity : nlapiEscapeXML(invoiceRecord
		            .getFieldValue('billcity')),
		    BillingAddressState : nlapiEscapeXML(invoiceRecord
		            .getFieldValue('billstate')),
		    BillingAddressZip : nlapiEscapeXML(invoiceRecord
		            .getFieldValue('billzip'))
		};
	} catch (err) {
		nlapiLogExecution('error', 'getInvoiceDetails', err);
		throw err;
	}
}

function getCustomerDetails(customerId) {
	try {
		var customerDetailValue = nlapiLookupField('customer', customerId, [
		        'altname', 'email' ]);

		return {
		    Name : customerDetailValue.altname,
		    Email : nlapiEscapeXML(customerDetailValue.email)
		};
	} catch (err) {
		nlapiLogExecution('error', 'getCustomerDetails', err);
		throw err;
	}
}

function getProjectDetails(projectId) {
	try {
		var projectDetailsValue = nlapiLookupField('job', projectId,
		        [ 'entityid' ]);

		return {
			ProjectId : projectDetailsValue.entityid
		};
	} catch (err) {
		nlapiLogExecution('error', 'getProjectDetails', err);
		throw err;
	}
}

function formatCurrency(value) {
	value = parseFloat(value);
	return nlapiEscapeXML("$" + " "
	        + value.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,"));
}

function formatDateText(value) {
	value = nlapiStringToDate(value);

	var month = new Array();
	month[0] = "January";
	month[1] = "February";
	month[2] = "March";
	month[3] = "April";
	month[4] = "May";
	month[5] = "June";
	month[6] = "July";
	month[7] = "August";
	month[8] = "September";
	month[9] = "October";
	month[10] = "November";
	month[11] = "December";
	var newDate = month[value.getMonth()] + " " + value.getDate() + ", "
	        + value.getFullYear();

	return newDate;
}

function formatDate(value) {
	return value;
	value = nlapiStringToDate(value);

	var mm = parseInt(value.getMonth()) + 1;
	if (mm < 9) {
		mm = "0" + mm;
	}

	var dd = parseInt(value.getDate());
	if (dd < 9) {
		dd = "0" + dd;
	}

	var yy = value.getFullYear();
	yy = yy.toString().substring(2, 4);

	var newDate = mm + "-" + dd + "-" + yy;

	return newDate;
}