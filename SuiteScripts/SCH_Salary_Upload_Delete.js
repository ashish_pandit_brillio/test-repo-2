/**
 * @author Shweta
 */
// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
/*
   	Script Name : SCH_Salary_Upload_Delete.js
	Author      : Shweta Chopde
	Date        : 6 Aug 2014
    Description : Delete the JEs created


	Script Modification Log:

	-- Date --			-- Modified By --				--Requested By--				-- Description --




Below is a summary of the process controls enforced by this script file.  The control logic is described
more fully, below, in the appropriate function headers and code blocks.


     SCHEDULED FUNCTION
		- scheduledFunction(type)


     SUB-FUNCTIONS
		- The following sub-functions are called by the above core functions in order to maintain code
            modularization:

               - NOT USED

*/
}
// END SCRIPT DESCRIPTION BLOCK  ====================================


// BEGIN SCHEDULED FUNCTION =============================================

function schedulerFunction(type) //
{
	var s_delete_status = '';
	var s_delete_record_status = '';
	
	try //
	{
		//------------------------------------------------------------------------
		if (_logValidation(i_recordID)) //
		{
			var o_recordOBJ = nlapiLoadRecord('customrecord_salary_upload_process', i_recordID)
			
			if (_logValidation(o_recordOBJ)) //
			{
				var i_CSV_File_ID = o_recordOBJ.getFieldValue('custrecord_csv_file_s_p')
				
				var s_notes = 'Created JEs & respective Monthly / Hourly have been deleted from Netsuite Account .<br> Please initiate the process from start if want to process again.';
				
				o_recordOBJ.setFieldValue('custrecord_notes', s_notes);
				o_recordOBJ.setFieldValue('custrecord_file_status', 3);
				
				
				var i_submitID = nlapiSubmitRecord(o_recordOBJ, true, true);
				nlapiLogExecution('DEBUG', 'schedulerFunction', ' ------------ Submit ID ----------->' + i_submitID);
				
				/*
				
				 var i_submit_fileID = nlapiDeleteFile(i_CSV_File_ID);
				
				 nlapiLogExecution('DEBUG', 'schedulerFunction',' ------------ File Submit ID ----------->' + i_submit_fileID);
				
				 */
				
			}
			
		}
		
		//------------------------------------------------------------------------
		var i_context = nlapiGetContext();
		
		var a_JE_IDs_arr = i_context.getSetting('SCRIPT', 'custscript_je_array');
		nlapiLogExecution('DEBUG', 'schedulerFunction', ' JE IDs Array -->' + a_JE_IDs_arr);
		nlapiLogExecution('DEBUG', 'schedulerFunction', ' JE IDs Array Length -->' + a_JE_IDs_arr.length);
		
		var i_recordID = i_context.getSetting('SCRIPT', 'custscript_je_record_id_delete');
		nlapiLogExecution('DEBUG', 'schedulerFunction', ' Record ID -->' + i_recordID);
		
		
		//---------------------------------------------------------------------------
		
		if (_logValidation(i_recordID)) //
		{
			var o_recordOBJ = nlapiLoadRecord('customrecord_salary_upload_process', i_recordID)
			
			if (_logValidation(o_recordOBJ)) //
			{
				var i_CSV_File_ID = o_recordOBJ.getFieldValue('custrecord_csv_file_s_p')
				
				var s_notes = 'Created JEs & respective Monthly / Hourly have been deleted from Netsuite Account .<br> Please initiate the process from start if want to process again.';
				
				o_recordOBJ.setFieldValue('custrecord_notes', s_notes);
				o_recordOBJ.setFieldValue('custrecord_file_status', 1);
				
				
				var i_submitID = nlapiSubmitRecord(o_recordOBJ, true, true);
				nlapiLogExecution('DEBUG', 'schedulerFunction', ' ------------ Submit ID ----------->' + i_submitID);
				
				/*
				
				 var i_submit_fileID = nlapiDeleteFile(i_CSV_File_ID);
				
				 nlapiLogExecution('DEBUG', 'schedulerFunction',' ------------ File Submit ID ----------->' + i_submit_fileID);
				
				 */
				
			}
			
		}
		//---------------------------------------------------------------------------
		
		var a_CRT_array_values = new Array()
		var i_data_CRT = new Array()
		i_data_CRT = a_JE_IDs_arr;
		
		for (var dct = 0; dct < i_data_CRT.length; dct++) //
		{
			a_CRT_array_values = i_data_CRT.split(',')
			break;
		}
		
		nlapiLogExecution('DEBUG', 'schedulerFunction', 'CRT Array Values-->' + a_CRT_array_values);
		nlapiLogExecution('DEBUG', 'schedulerFunction', ' CRT Array Values Length-->' + a_CRT_array_values.length);
		
		// ========================== Delete Journal Entries =======================
		
		if (_logValidation(a_CRT_array_values)) //
		{
			for (var i = 0; i < a_CRT_array_values.length; i++) //
			{
				if (_logValidation(a_CRT_array_values[i])) //
				{
				
					var o_journalOBJ = nlapiLoadRecord('journalentry', a_CRT_array_values[i])
					
					if (_logValidation(o_journalOBJ)) //
					{
						var a_monthly_record_arr = o_journalOBJ.getFieldValues('custbody_salary_monthly_records');
						nlapiLogExecution('DEBUG', 'schedulerFunction', 'Monthly Records Array -->' + a_monthly_record_arr);
						
						var a_hourly_record_arr = o_journalOBJ.getFieldValues('custbody_salary_hourly_records');
						nlapiLogExecution('DEBUG', 'schedulerFunction', 'Hourly Records Array -->' + a_hourly_record_arr);
						
						var i_delete_JE_ID = nlapiDeleteRecord('journalentry', a_CRT_array_values[i]);
						nlapiLogExecution('DEBUG', 'schedulerFunction', 'Delete JE ID-->' + i_delete_JE_ID);
						
						if (_logValidation(i_delete_JE_ID)) //
						{
							s_delete_status = 'DELETED'
						}//Delete JE	
						if (_logValidation(a_monthly_record_arr)) //
						{
							nlapiLogExecution('DEBUG', 'schedulerFunction', 'Monthly Records Array Length -->' + a_monthly_record_arr.length);
							
							for (var mt = 0; mt < a_monthly_record_arr.length; mt++) //
							{
								nlapiLogExecution('DEBUG', 'schedulerFunction', 'Monthly Record ID [' + mt + '] -->' + a_monthly_record_arr[mt]);
								
								var i_delete_MRD_ID = nlapiDeleteRecord('customrecord_salary_upload_monthly_file', a_monthly_record_arr[mt]);
								nlapiLogExecution('DEBUG', 'schedulerFunction', 'Delete Monthly Record ID -->' + i_delete_MRD_ID);
								
								if (_logValidation(i_delete_MRD_ID)) //
								{
									s_delete_record_status = 'DELETED'
								}//Delete JE	
							}//Loop Monthly Records	
						}//Monthly Records Array
						if (_logValidation(a_hourly_record_arr)) //
						{
							nlapiLogExecution('DEBUG', 'schedulerFunction', 'Hourly Records Array Length -->' + a_hourly_record_arr.length);
							
							for (var ht = 0; ht < a_hourly_record_arr.length; ht++) //
							{
								nlapiLogExecution('DEBUG', 'schedulerFunction', 'Hourly Record ID [' + ht + '] -->' + a_hourly_record_arr[ht]);
								
								var i_delete_HRD_ID = nlapiDeleteRecord('customrecord_salary_upload_hourly_file', a_hourly_record_arr[ht]);
								nlapiLogExecution('DEBUG', 'schedulerFunction', 'Delete Hourly Record ID -->' + i_delete_HRD_ID);
								
								if (_logValidation(i_delete_HRD_ID)) //
								{
									s_delete_record_status = 'DELETED'
								}//Delete JE	
							}//Loop Hourly Records				
						}//Hourly Records Array
					}//Journal OBJ		
				}//Journal OBJ			
			}//Delete Loop
		}
		
		var a_Filters = new Array();
		var a_Columns = new Array();
		
		a_Filters[0] = new nlobjSearchFilter('custrecord_sal_upload_rec_id', null, 'anyof', i_recordID);
		
		a_Columns[0] = new nlobjSearchColumn('internalid');
		
		var s_Search_Result_Salaray_upload_Data = nlapiSearchRecord('customrecord_salary_upload_ot_data', null, a_Filters, a_Columns);
		
		if (_logValidation(s_Search_Result_Salaray_upload_Data)) //
		{
			var a_All_columns =  s_Search_Result_Salaray_upload_Data[0].getAllColumns();
			
			for (var counter_I = 0; counter_I < s_Search_Result_Salaray_upload_Data.length; counter_I++) //
			{
				var i_internal_ID = s_Search_Result_Salaray_upload_Data[counter_I].getValue(a_All_columns[0]);
				nlapiLogExecution('DEBUG', 'Delete DIFF Data', 'i_internal_ID : ' + i_internal_ID);
				
				try //
				{
					var i_Delete_Flag = nlapiDeleteRecord('customrecord_salary_upload_ot_data', i_internal_ID);
					nlapiLogExecution('DEBUG', 'Delete DIFF Data', 'i_Delete_Flag : ' + i_Delete_Flag);
				} 
				catch (excp) //
				{
					nlapiLogExecution('ERROR', 'Exception while delete record', 'Exception : ' + excp);
				}
			}
		}
		else //
		{
			nlapiLogExecution('ERROR', 'No Data Found!', 'For Salary Upload Record : ' + i_recordID);
		}
		
		
		if (s_delete_status == 'DELETED' && s_delete_record_status == 'DELETED') //
		{
			if (_logValidation(i_recordID)) //
			{
				var o_recordOBJ = nlapiLoadRecord('customrecord_salary_upload_process', i_recordID)
				
				if (_logValidation(o_recordOBJ)) //
				{
					var i_CSV_File_ID = o_recordOBJ.getFieldValue('custrecord_csv_file_s_p')
					var s_notes = 'Created JEs & respective Monthly / Hourly have been deleted from Netsuite Account .<br> Please initiate the process from start if want to process again.';
					
					o_recordOBJ.setFieldValue('custrecord_notes', s_notes);
					o_recordOBJ.setFieldValue('custrecord_file_status', 5);
					
					var i_submitID = nlapiSubmitRecord(o_recordOBJ, true, true);
					nlapiLogExecution('DEBUG', 'schedulerFunction', ' ------------ Submit ID ----------->' + i_submitID);
					
					//var i_submit_fileID = nlapiDeleteFile(i_CSV_File_ID);
					//nlapiLogExecution('DEBUG', 'schedulerFunction', ' ------------ File Submit ID ----------->' + i_submit_fileID);
					
				}
			}
		}//DELETE
	} 
	catch (exception) //
	{
		nlapiLogExecution('DEBUG', 'ERROR', ' Exception Caught -->' + exception);
	}//CATCH
}

// END SCHEDULED FUNCTION ===============================================





// BEGIN FUNCTION ===================================================
/**
 * 
 * @param {Object} value
 * 
 * Description --> If the value is blank /null/undefined returns false else returns true
 */
function _logValidation(value) 
{
 if(value!='null' && value != null && value != null && value != '' && value != undefined && value != undefined && value != 'undefined' && value!= 'undefined'&& value!= 'NaN' && value != NaN) 
 {
  return true;
 }
 else 
 { 
  return false;
 }
}
// END FUNCTION =====================================================
