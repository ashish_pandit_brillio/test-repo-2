/*
    Constant file for Invoice PDFs
    Version :   1.00
    Author  :   Raghav Gupta
    Remarks :   24 Nov 2021
*/

/*
    InvoiceConstant variable structure 
    layout id: {
        "layoutname": A,
        "type": B,
        "address": ["Address_Line_1", "Address_Line_2", "Address_Line_3", "Address_Line_4", etc.]
    }
    note:
        1. Layoutname can be anything
                and is used for the pdf name while downloading.
        2. Type should be either "T&M" or "T&M Group" or "FP" or "FP with Tax details",
                and is used for all the conditional statements to differentiate the layouts.
        3. Address should have comma in the string itself if needed in the address body and can have any number of strings in it.
                and is used for Remittance and Header Address.

                Note: Header's first line is taken from subsidary.
*/

var InvoiceConstant = {
    1: {
        "layoutname": "BLLC T&M",
        "type": "T&M Group",
        "address": ["Brillio LLC", "399 Thornall St", "1st Floor", "Edison NJ 08837"],
        "Tax Details": "No"
    }
    ,
    2: {
        "layoutname": "BLLC T&M Group",
        "type": "T&M Group",
        "address": ["Brillio LLC", "399 Thornall St", "1st Floor", "Edison NJ 08837"],
        "Tax Details": "No"
    }
    ,
    3: {
        "layoutname": "BLLC FP",
        "type": "FP",
        "address": ["Brillio LLC", "399 Thornall St", "1st Floor", "Edison NJ 08837"],
        "Tax Details": "No"
    }
    ,
    4: {
        "layoutname": "BLLC Exp",
        "type": "EXP",
        "address": ["Brillio LLC", "399 Thornall St", "1st Floor", "Edison NJ 08837"],
        "Tax Details": "No"
    },
    17: {
        "layoutname": "UK FP",
        "type": "FP",
        "address": ["Brillio UK Ltd", "03-116, North West House", "119 Marylebone Road", "London, NW1 5PU", "United Kingdom"],
        "Tax Details": "Yes"
    }
    ,
    18: {
        "layoutname": "UK T&M",
        "type": "T&M Group",
        "address": ["Brillio UK Ltd", "03-116, North West House", "119 Marylebone Road", "London, NW1 5PU", "United Kingdom"],
        "Tax Details": "Yes"
    }
    ,
    19: {
        "layoutname": "Brillio CAD T&M",
        "type": "T&M Group",
        "address": ["Brillio Canada Inc", "11871 Horseshoe Inc", "Suite 1108 Richmond", "BC V7A 5H5", "Canada"],
        "Tax Details": "No"
    }
    ,
    20: {
        "layoutname": "Brillio CAD FP",
        "type": "FP",
        "address": ["Brillio Canada Inc", "11871 Horseshoe Inc", "Suite 1108 Richmond", "BC V7A 5H5", "Canada"],
        "Tax Details": "No"
    }
};


//FP with Tax details