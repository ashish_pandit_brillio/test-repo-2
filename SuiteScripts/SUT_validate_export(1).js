function su_excel_download(request, response)
{
	var s_record_type = request.getParameter('recordType');
	var i_record_id = request.getParameter('recordId');
  //var i_record_id = parseInt(11);
	nlapiLogExecution('DEBUG','Record id', i_record_id);
	if(!i_record_id)
	{
		return true;
	}
	
	var o_rev_rec_rcrd = nlapiLoadRecord(s_record_type,i_record_id)
	if(!o_rev_rec_rcrd)
	{
		return true;
	}
	
	var s_currentMonthName = o_rev_rec_rcrd.getFieldText('custrecord_month_fp_rev_rec');
	s_currentMonthName = s_currentMonthName.substr(0,3);
	s_currentMonthName = s_currentMonthName.toUpperCase();
	var s_currentYear = o_rev_rec_rcrd.getFieldText('custrecord_year_fp_rev_rec');
	var a_filter_ignore_proj_rev = [['custrecord_fp_othr_validate_mnth', 'contains', s_currentMonthName], 'and',
								  ['custrecord_fp_othr_validate_year', 'contains', s_currentYear]];
			nlapiLogExecution('DEBUG','year', s_currentYear);					  
		var a_columns_get_ytd_revenue_recognized = new Array();
		a_columns_get_ytd_revenue_recognized[0] = new nlobjSearchColumn('custrecord_fp_other_validate_project');
		a_columns_get_ytd_revenue_recognized[1] = new nlobjSearchColumn('custrecord_fp_other_validate_practice');
		a_columns_get_ytd_revenue_recognized[2] = new nlobjSearchColumn('custrecord_fp_othr_validate_sub_pract');
		a_columns_get_ytd_revenue_recognized[3] = new nlobjSearchColumn('custrecord_fp_othr_validate_role');
		a_columns_get_ytd_revenue_recognized[4] = new nlobjSearchColumn('custrecord_fp_othr_validate_level');
		a_columns_get_ytd_revenue_recognized[5] = new nlobjSearchColumn('custrecord_fp_othr_validate_rev_amnt');
		a_columns_get_ytd_revenue_recognized[6] = new nlobjSearchColumn('custrecord_fp_othr_validate_mnth');
		a_columns_get_ytd_revenue_recognized[7] = new nlobjSearchColumn('custrecord_fp_othr_validate_year');
		//a_columns_get_ytd_revenue_recognized[8] = new nlobjSearchColumn('custrecord_subtotal_amount');
		a_columns_get_ytd_revenue_recognized[8] = new nlobjSearchColumn('customer','custrecord_fp_other_validate_project');
		a_columns_get_ytd_revenue_recognized[9] = new nlobjSearchColumn('entityid','custrecord_fp_other_validate_project');
		a_columns_get_ytd_revenue_recognized[10] = new nlobjSearchColumn('altname','custrecord_fp_other_validate_project');
  a_columns_get_ytd_revenue_recognized[11] = new nlobjSearchColumn('custrecord_fp_othr_proj_type');
  a_columns_get_ytd_revenue_recognized[12] = new nlobjSearchColumn('custrecord_validate_subsi');

		var a_get_ytd_revenue_ignore_proj = nlapiSearchRecord('customrecord_fp_other_validate_excel_dat', null, a_filter_ignore_proj_rev, a_columns_get_ytd_revenue_recognized);
		if(a_get_ytd_revenue_ignore_proj)
		{
			down_excel_function(a_get_ytd_revenue_ignore_proj);
		}
}
function down_excel_function(a_get_ytd_revenue_ignore_proj)
{
	try
	{
		var strVar1= '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:ss="urn:schemas-microsoft-com:office:spreadsheet" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40">'+
		'<head>'+
		'<meta http-equiv=Content-Type content="text/html; charset=windows-1252"/>'+
        '<meta name=ProgId content=Excel.Sheet/>'+
        '<meta name=Generator content="Microsoft Excel 11"/>'+
        '<!--[if gte mso 9]><xml>'+
        '<x:excelworkbook>'+
        '<x:excelworksheets>'+
        '<x:excelworksheet=sheet1>'+
        '<x:name>** ESTIMATE FILE**</x:name>'+
        '<x:worksheetoptions>'+
        '<x:selected></x:selected>'+
        '<x:freezepanes></x:freezepanes>'+
        '<x:frozennosplit></x:frozennosplit>'+
        '<x:splithorizontal>** FROZEN ROWS + 1 **</x:splithorizontal>'+
        '<x:toprowbottompane>** FROZEN ROWS + 1 **</x:toprowbottompane>'+
        '<x:splitvertical>** FROZEN COLUMNS + 1 **</x:splitvertical>'+
        '<x:leftcolumnrightpane>** FROZEN COLUMNS + 1**</x:leftcolumnrightpane>'+
		    '<x:activepane>0</x:activepane>'+// 0
		    '<x:panes>'+
			'<x:pane>'+
			'<x:number>3</x:number>'+
			'</x:pane>'+
			'<x:pane>'+
		    '<x:number>1</x:number>'+
		    '</x:pane>'+
		    '<x:pane>'+
			'<x:number>2</x:number>'+
        '</x:pane>'+
        '<x:pane>'+
        '<x:number>0</x:number>'+//1
        '</x:pane>'+
        '</x:panes>'+
        '<x:protectcontents>False</x:protectcontents>'+
        '<x:protectobjects>False</x:protectobjects>'+
        '<x:protectscenarios>False</x:protectscenarios>'+
        '</x:worksheetoptions>'+
        '</x:excelworksheet>'+
		'</x:excelworksheets>'+
        '<x:protectstructure>False</x:protectstructure>'+
        '<x:protectwindows>False</x:protectwindows>'+
		'</x:excelworkbook>'+
        
        // '<style>'+
			
			//-------------------------------------
        '</x:excelworkbook>'+
        '</xml><![endif]-->'+
         '<style>'+
		'p.MsoFooter, li.MsoFooter, div.MsoFooter'+
		'{ margin:0in; margin-bottom:.0001pt; mso-pagination:widow-orphan; tab-stops:center 3.0in right 6.0in; font-size:12.0pt;}'+
		'<style>'+

		'<!-- /* Style Definitions */'+

		//'@page Section1'+
		//'{ size:8.5in 11.0in; margin:1.9cm 1.27cm 1.9cm 1.27cm; mso-header-margin:.5in; mso-footer-margin:.5in; mso-title-page:yes; mso-header: h1; mso-footer: f1; mso-first-header: fh1; mso-first-footer: ff1; mso-paper-source:0;}'+

		'div.Section1'+
		'{ page:Section1;}'+

		'table#hrdftrtbl'+
		'{ margin:0in 0in 0in 900in; width:1px; height:1px; overflow:hidden; font-family:Arial;}-->'+

		'</style>'+
		
		
		'</head>'+
		

		'<body>'+
		
		'<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">'
		
		var strVar2 = '';    
		strVar2 += "<table width=\"100%\">";
		strVar2 += "<tr>";
		strVar2 += "<td Width=\"10%\" style='text-align:left;font-weight:bold;' font-size=\"10\" \>Customer</td>";
		strVar2 += "<td Width=\"10%\" style='text-align:left;font-weight:bold;' font-size=\"10\" \>Project</td>";
		strVar2 += "<td Width=\"20%\" style='text-align:left;font-weight:bold;' font-size=\"10\" \>Sub Practice</td>";
		strVar2 += "<td Width=\"10%\" style='text-align:left;font-weight:bold;' font-size=\"10\" \>Revenue</td>";
		strVar2 += "<td Width=\"50%\" style='text-align:left;font-weight:bold;' font-size=\"10\" \>Month</td>";
		strVar2 += "<td Width=\"50%\" style='text-align:left;font-weight:bold;' font-size=\"10\" \>Year</td>";
		strVar2 += "<td Width=\"50%\" style='text-align:left;font-weight:bold;' font-size=\"10\" \>FP Type</td>";
		strVar2 += "<td Width=\"50%\" style='text-align:left;font-weight:bold;' font-size=\"10\" \>Subsidiary</td>";
		strVar2 += "<\/tr>";
		
		if ((a_get_ytd_revenue_ignore_proj)) //
		{
			for (var i = 0; i < a_get_ytd_revenue_ignore_proj.length; i++) //
			{
				var s_proj_cust = a_get_ytd_revenue_ignore_proj[i].getText('customer','custrecord_fp_other_validate_project');
				var s_project = a_get_ytd_revenue_ignore_proj[i].getText('custrecord_fp_other_validate_project');
				var s_sub_practice = a_get_ytd_revenue_ignore_proj[i].getText('custrecord_fp_othr_validate_sub_pract');
				var f_revenue_amount = a_get_ytd_revenue_ignore_proj[i].getValue('custrecord_fp_othr_validate_rev_amnt');
				var s_month = a_get_ytd_revenue_ignore_proj[i].getValue('custrecord_fp_othr_validate_mnth');
				var s_year = a_get_ytd_revenue_ignore_proj[i].getValue('custrecord_fp_othr_validate_year');
				var s_proj_entityid = a_get_ytd_revenue_ignore_proj[i].getValue('entityid','custrecord_fp_other_validate_project');
				var s_proj_name = a_get_ytd_revenue_ignore_proj[i].getValue('altname','custrecord_fp_other_validate_project');
				var s_type = a_get_ytd_revenue_ignore_proj[i].getText('custrecord_fp_othr_proj_type');
				var subsi=a_get_ytd_revenue_ignore_proj[i].getText('custrecord_validate_subsi');
				nlapiLogExecution('audit','s_proj_entityid:-'+s_proj_entityid,'s_proj_name:-'+s_proj_name);
				strVar2 += "<tr>";
				strVar2 += "<td Width=\"10%\" style='text-align:left;' font-size=\"10\" \>" + s_proj_cust + "</td>";
				strVar2 += "<td Width=\"10%\" style='text-align:left;' font-size=\"10\" \>" + s_proj_entityid + " " + s_proj_name + "</td>";
				strVar2 += "<td Width=\"20%\" style='text-align:left;' font-size=\"10\" \>" + s_sub_practice + "</td>";
				strVar2 += "<td Width=\"10%\" style='text-align:left;' font-size=\"10\" \>" + f_revenue_amount + "</td>";
				strVar2 += "<td Width=\"50%\" style='text-align:left;' font-size=\"10\" \>" + s_month + "</td>";
				strVar2 += "<td Width=\"50%\" style='text-align:left;' font-size=\"10\" \>" + s_year + "</td>";
          		strVar2 += "<td Width=\"50%\" style='text-align:left;' font-size=\"10\" \>" + s_type + "</td>";  
				strVar2 += "<td Width=\"50%\" style='text-align:left;' font-size=\"10\" \>" + subsi + "</td>";
				strVar2 += "<\/tr>";
			}
		}
		else
		{
			//NO RESULTS FOUND
		}
					
		strVar2 += "<\/table>";			
		strVar1 = strVar1 + strVar2;
		var file = nlapiCreateFile('Revenue Data.xls', 'XMLDOC', strVar1);
		nlapiLogExecution('debug','file:- ',file);
		response.setContentType('XMLDOC','Revenue Data.xls');
		response.write( file.getValue() );	 
		
	}
	catch(err)
	{
		nlapiLogExecution('ERROR','ERROR MESSAGE:- ',err);
	}
}
