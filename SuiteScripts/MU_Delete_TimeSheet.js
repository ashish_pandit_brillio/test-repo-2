/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       26 Jun 2015     amol.sahijwani
 *
 */

/**
 * @param {String} recType Record type internal id
 * @param {Number} recId Record internal id
 * @returns {Void}
 */
function massUpdate(recType, recId) {
	//nlapiDeleteRecord('internalid', 13863);
	nlapiDeleteRecord(recType, recId);
}
