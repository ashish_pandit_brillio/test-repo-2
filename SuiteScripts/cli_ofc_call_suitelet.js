/**
 * @NApiVersion 2.x
 * @NScriptType ClientScript
 * @NModuleScope SameAccount
 */
define(['N/currentRecord','N/url'],

function(currentRecord,urlMod) {
    
    /**
     * Function to be executed after page is initialized.
     *
     * @param {Object} scriptContext
     * @param {Record} scriptContext.currentRecord - Current form record
     * @param {string} scriptContext.mode - The mode in which the record is being accessed (create, copy, or edit)
     *
     * @since 2015.2
     */
    function pageInit(scriptContext) {
       alert('Inside page Init');
    }
  
    function existingRecord(){
      console.log('Triggered');
    }
  
    function s_suitelet_validate(){
      
    var i_rec_id = '';
	var s_rec_type = '';
	var i_mode = 'validate';
    

      var rec = currentRecord.get();

      i_rec_id = rec.id;
      s_rec_type = rec.type;

 
    	window.location.href =  urlMod.resolveScript({
             scriptId: 'customscript_sut_ofc_call_scheduled',
             deploymentId: 'customdeploy_sut_ofc_call_scheduled',
             params : {
				'i_rec_id' : i_rec_id,
				'i_mode': i_mode,
				's_rec_type': s_rec_type
			},
            returnExternalUrl: false,
           });

        return true;
	}

	function s_suitelet_create(){
      
		var i_rec_id = '';
		var s_rec_type = '';
		var i_mode = 'create';
		
	
		  var rec = currentRecord.get();
	
		  i_rec_id = rec.id;
		  s_rec_type = rec.type;
	
	 
			window.location.href =  urlMod.resolveScript({
				 scriptId: 'customscript_sut_ofc_call_scheduled',
				 deploymentId: 'customdeploy_sut_ofc_call_scheduled',
				 params : {
					'i_rec_id' : i_rec_id,
					'i_mode': i_mode,
					's_rec_type': s_rec_type
				},
				returnExternalUrl: false,
			   });
	
			return true;
		}

		function s_suitelet_delete(){
      
			var i_rec_id = '';
			var s_rec_type = '';
			var i_mode = 'delete';
			
		
			  var rec = currentRecord.get();
		
			  i_rec_id = rec.id;
			  s_rec_type = rec.type;
		
		 
				window.location.href =  urlMod.resolveScript({
					 scriptId: 'customscript_sut_ofc_call_scheduled',
					 deploymentId: 'customdeploy_sut_ofc_call_scheduled',
					 params : {
						'i_rec_id' : i_rec_id,
						'i_mode': i_mode,
						's_rec_type': s_rec_type
					},
					returnExternalUrl: false,
				   });
		
				return true;
			}
	


    return {
        pageInit: pageInit,
		s_suitelet_validate:s_suitelet_validate,
		s_suitelet_create:s_suitelet_create,
		s_suitelet_delete: s_suitelet_delete,
        existingRecord: existingRecord

        
    };
    
});