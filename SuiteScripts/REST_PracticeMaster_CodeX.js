/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       20 Sep 2017     deepak.srinivas
 *
 */

/**
 * @param {Object} dataIn Parameter object
 * @returns {Object} Output object
 */
function postRESTlet(dataIn) {
	try{
	var response = new Response(); 
	var startDate = '';
	var endDate = '';
	var current_date = nlapiDateToString(new Date());
	var requestType = dataIn.RequestType;
	//Log for current date
	nlapiLogExecution('DEBUG', 'Current Date', 'current_date...' + current_date);
	nlapiLogExecution('debug', 'dataIn', JSON.stringify(dataIn));
	
	switch(requestType) {
	
	case "PRACTICE_MASTER":

	{
	response.Data = getPracticeMasterData();
	response.Status = true;
	}
	break;
	
	}
	
}
	catch (err) {
		nlapiLogExecution('ERROR', 'postRESTlet', err);
		response.Data = err;
		response.Status = false;
	}
	//nlapiLogExecution('debug', 'response', JSON.stringify(response));
	return response;
}

function getPracticeMasterData(){
	try{
		var JSON ={};
		var dataRow = [];
		var search = searchRecord('department',null,[new nlobjSearchFilter('isinactive', null, 'is',
                                            						'F')], 
                                         				[
                                         				new nlobjSearchColumn('internalid'),
                                         				new nlobjSearchColumn('name'),
                                         				new nlobjSearchColumn('custrecord_practice_head_2'),
                                         				new nlobjSearchColumn('email','custrecord_practice_head_2'),
                                         				new nlobjSearchColumn('subsidiary'),
                                         				new nlobjSearchColumn('custrecord_practicehead'),
                                         				//new nlobjSearchColumn('custrecord_hrbusinesspartner'),
														// Commented by shravan for HRBP integration on 7-dec-2020
                                         				new nlobjSearchColumn('custrecord12'),
                                         				new nlobjSearchColumn('custrecord_parent_practice'),
                                         				new nlobjSearchColumn('custrecord_onsite_practice_head'),
                                         				new nlobjSearchColumn('email','custrecord_onsite_practice_head'),
                                         				new nlobjSearchColumn('email','custrecord_practicehead'),
                                         				//new nlobjSearchColumn('email','custrecord_hrbusinesspartner')
														// Commented by shravan for HRBP integration on 7-dec-2020
                                         				]);
		if(search){
			for(var i=0;i<search.length;i++){
				JSON ={
					ID: search[i].getValue('internalid'),
					Name: search[i].getValue('name'),
					PracticeDescription: search[i].getValue('custrecord12'),
					//Parent: search[i].getText('parent'),
					Subsidiary: search[i].getText('subsidiary'),
					PracticeHead: search[i].getText('custrecord_practicehead'),
					PracticeHeadEmail: search[i].getValue('email','custrecord_practicehead'),
					ParentPractice: search[i].getText('custrecord_parent_practice'),
                    //ParentPracticeId: search[i].getValue('custrecord_parent_practice'),
					OnSitePracticeHead: search[i].getText('custrecord_onsite_practice_head'),
					OnSitePracticeHeadEmail: search[i].getValue('email','custrecord_onsite_practice_head'),
					//HRBusinessPartner: search[i].getText('custrecord_hrbusinesspartner'),
					//HRBusinessPartnerEmail: search[i].getValue('email','custrecord_hrbusinesspartner'),
					// Commented by shravan for HRBP integration on 7-dec-2020
					PracticeHead2: search[i].getText('custrecord_practice_head_2'),
					PracticeHeadEmail2: search[i].getValue('email','custrecord_practice_head_2'),
				};
				dataRow.push(JSON);
		
			}
		}
		return dataRow;
	}
	catch (err) {
		nlapiLogExecution('ERROR', 'region master error', err);
		
	}
}