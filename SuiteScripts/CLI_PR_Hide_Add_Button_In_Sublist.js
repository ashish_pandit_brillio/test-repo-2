/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       25 Nov 2014     amol.sahijwani
 * 
 * Hide Add buttons in sublists
 *
 */
var css = '#tbl_newrec11, #tbl_newrec15, #tbl_newrecrecmachcustrecord_purchaserequest, #tbl_newrecrecmachcustrecord_prquote { display: none; }',
head = document.head || document.getElementsByTagName('head')[0],
style = document.createElement('style');

style.type = 'text/css';
if (style.styleSheet){
  style.styleSheet.cssText = css;
} else {
  style.appendChild(document.createTextNode(css));
}

head.appendChild(style);

