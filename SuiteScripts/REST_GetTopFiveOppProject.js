// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
/*
   	Script Name : REST_getTopFiveOppProject.js
	Author      : ASHISH PANDIT
	Date        : 02 APRIL 2019
    Description : RESTlet to show data of top five Opportunity and Projects


	Script Modification Log:

	-- Date --			-- Modified By --				--Requested By--				-- Description --
 


Below is a summary of the process controls enforced by this script file.  The control logic is described
more fully, below, in the appropriate function headers and code blocks.


     SCHEDULED FUNCTION
		- scheduledFunction(type)


     SUB-FUNCTIONS
		- The following sub-functions are called by the above core functions in order to maintain code
            modularization:

               - NOT USED

*/
}
//============================================================================================================
var projectArray = new Array();
var projectArrayProject = new Array();

/*RESTlet Main function*/
function REST_getTopFiveOppProj(dataIn)
{
	try
	{
		var user = dataIn.user;
		var dataOut = {};
		var values = {};
		nlapiLogExecution('Debug','Script Parameter user ',user);
		var startDate = getStartDate();
		nlapiLogExecution('Debug','startDate ',startDate);
		var endDate = getEndDate(1);
		var user = getUserUsingEmailId(user);
		var adminUser = GetAdmin(user);
		var spocSearch = GetPracticeSPOC(user);
		var region = GetRegion(user);
		
		var FirstMonthName = getMonthName(nlapiStringToDate(startDate).getMonth());
		nlapiLogExecution('Debug','FirstMonthName ',FirstMonthName);
		var SecondMonthName = getMonthName(nlapiAddMonths(nlapiStringToDate(startDate),1).getMonth());
		nlapiLogExecution('Debug','SecondMonthName ',SecondMonthName);
		var ThirdMonthName = getMonthName(nlapiAddMonths(nlapiStringToDate(startDate),2).getMonth());
		nlapiLogExecution('Debug','ThirdMonthName ',ThirdMonthName);
		var FourthMonthName = getMonthName(nlapiAddMonths(nlapiStringToDate(startDate),3).getMonth())
		nlapiLogExecution('debug','FourthMonthName ',FourthMonthName);
		nlapiLogExecution('debug','projectArray ',projectArray);
		nlapiLogExecution('debug','projectArrayProject ',projectArrayProject);
		
		
		values.project = getProjects(adminUser,spocSearch,region,user,startDate,endDate);
		values[FirstMonthName] = getMonthWiseFrf(projectArray,projectArrayProject,nlapiAddMonths(nlapiStringToDate(startDate),0),nlapiAddMonths(nlapiStringToDate(getEndDate(1)),0));
		values[SecondMonthName] = getMonthWiseFrf(projectArray,projectArrayProject,nlapiAddMonths(nlapiStringToDate(startDate),1),nlapiAddMonths(nlapiStringToDate(getEndDate(1)),1));
		values[ThirdMonthName] = getMonthWiseFrf(projectArray,projectArrayProject,nlapiAddMonths(nlapiStringToDate(startDate),2),nlapiAddMonths(nlapiStringToDate(getEndDate(1)),2));
		values[FourthMonthName] = getMonthWiseFrf(projectArray,projectArrayProject,nlapiAddMonths(nlapiStringToDate(startDate),3),nlapiAddMonths(nlapiStringToDate(getEndDate(1)),3));
		dataOut["dataOut"] = values;
		//nlapiLogExecution('debug','dataOut ',getProjects(adminUser,spocSearch,user,startDate,endDate));
		return dataOut;
	}
	catch(error)
	{
		nlapiLogExecution('Debug','Error ',error);
        return {
            "code": "error",
            "message": error
        }
	}
}

/****Function to get Start of Month****/
function getStartDate() 
{
    var date = new Date();
    var startDate =  new Date(date.getFullYear(),date.getMonth(),1);
    return nlapiDateToString(startDate);
}

/****Function to get End of Month****/
function getEndDate(data) 
{
    var date = new Date();
    var endDate =  new Date(date.getFullYear(), date.getMonth()+data,0);
    return nlapiDateToString(endDate);
}

function GetPracticeSPOC(user) 
{
  	var practiceArray = new Array();
	nlapiLogExecution('AUDIT', 'user', user);
	var spocSearch = nlapiSearchRecord("customrecord_practice_spoc",null,
			[
			 ["custrecord_spoc","anyof",user],
			  "AND",
			 ["isinactive","is","F"]
			], 
			[
			  new nlobjSearchColumn("custrecord_practice")
			]
	);
	if(spocSearch)
	{
		for(var i=0; i<spocSearch.length; i++)
		{
			practiceArray.push(spocSearch[i].getValue('custrecord_practice'));
		}
	}
	return practiceArray;
}

/*function getStartDate() {

    var date = new Date();

    var startDate =  new Date(date.getFullYear(),date.getMonth(),1);

    return nlapiDateToString(startDate);

}

function getEndDate() {

    var date = new Date();

    var endDate =  new Date(date.getFullYear(), date.getMonth()+2,0);

    return nlapiDateToString(endDate);
}*/

function GetAdmin(user) 
{
  	nlapiLogExecution('AUDIT', 'user GetAdmin ', user);
	var adminSearch = nlapiSearchRecord("customrecord_fuel_leadership",null,
			[
			 ["custrecord_fuel_leadership_employee","anyof",user],
			  "AND",
			 ["isinactive","is","F"]
			], 
			[
			  new nlobjSearchColumn("custrecord_fuel_leadership_employee")
			]
	);
	return adminSearch;
}

/****Search to get Month wise FRFs ******/

function getMonthWiseFrf(projectArray,projectArrayProject,startDate,endDate)
{
	nlapiLogExecution('Debug','startDate $$$ ',startDate);
	nlapiLogExecution('Debug','endDate $$$ ',endDate);
	nlapiLogExecution('Debug','projectArray $$$ ',projectArray);
	
	var filters = {};
	
	
		/*filters.push(new nlobjSearchFilter('custrecord_frf_details_start_date',null,'within',startDate,endDate));
		if(projectArray.length>0)
		{
			filters.push(new nlobjSearchFilter('custrecord_frf_details_opp_id',null,'anyof',startDate,endDate));
		}
		if(projectArrayProject.length>0)
		{
			filters.push(new nlobjSearchFilter('custrecord_frf_details_project',null,'anyof',startDate,endDate));
		}*/
		
		if(projectArray.length>0 && projectArrayProject.length>0)
		{
			filters = [["custrecord_frf_details_start_date","within",startDate, endDate], 
				  "AND", 
				   [["custrecord_frf_details_opp_id","anyof",projectArray],
				   "OR",
				   ["custrecord_frf_details_project","anyof",projectArrayProject]]]
		}
		else if(projectArray.length>0 && projectArrayProject.length==0)
		{
			filters = [["custrecord_frf_details_start_date","within",startDate, endDate], 
					  "AND", 
					   ["custrecord_frf_details_opp_id","anyof",projectArray]
					  ]	
		}
		else if(projectArray.length==0 && projectArrayProject.length>0)
		{
			filters = [["custrecord_frf_details_start_date","within",startDate, endDate], 
					  "AND", 
					   ["custrecord_frf_details_project","anyof",projectArrayProject]
					  ]	
		}
	    nlapiLogExecution('Debug','filters ',filters);
	var customrecord_frf_detailsSearch = nlapiSearchRecord("customrecord_frf_details",null,filters, 
	[
	   new nlobjSearchColumn("custrecord_frf_details_frf_number"), 
	   new nlobjSearchColumn("custrecord_frf_details_project"), 
	   new nlobjSearchColumn("custrecord_frf_details_opp_id"), 
	   new nlobjSearchColumn("custrecord_frf_details_availabledate"), 
	   new nlobjSearchColumn("custrecord_frf_details_allocated_date"),
	   new nlobjSearchColumn("custrecord_frf_details_sales_activity"),
	   new nlobjSearchColumn("custrecord_frf_details_start_date"),
	   new nlobjSearchColumn("custrecord_frf_details_account"),
	   new nlobjSearchColumn("custrecord_frf_details_external_hire"),
	   new nlobjSearchColumn("custrecord_opportunity_name_sfdc","custrecord_frf_details_opp_id")
	]
	);
	if(customrecord_frf_detailsSearch)
	{
		nlapiLogExecution('Debug','Available Date ',JSON.stringify(customrecord_frf_detailsSearch));
		var dataArray = new Array();
		for(var i=0; i<customrecord_frf_detailsSearch.length; i++)
		{
			var data = {};
			data.frfnumber = {"name":customrecord_frf_detailsSearch[i].getValue('custrecord_frf_details_frf_number')};
			//data.project = {"name":customrecord_frf_detailsSearch[i].getText('custrecord_frf_details_project'),"id":customrecord_frf_detailsSearch[i].getValue('custrecord_frf_details_project')};
			if(customrecord_frf_detailsSearch[i].getValue("custrecord_opportunity_name_sfdc","custrecord_frf_details_opp_id"))
			{
				data.project = {"name":customrecord_frf_detailsSearch[i].getValue("custrecord_opportunity_name_sfdc","custrecord_frf_details_opp_id"),"id":customrecord_frf_detailsSearch[i].getValue("custrecord_frf_details_opp_id")};
				data.salesactivity = {"name":customrecord_frf_detailsSearch[i].getValue('custrecord_frf_details_sales_activity')};
			}
			else
			{
				data.project = {"name":customrecord_frf_detailsSearch[i].getText("custrecord_frf_details_project"),"id":customrecord_frf_detailsSearch[i].getValue("custrecord_frf_details_project")};
				data.salesactivity = {"name":'Booked'};
			}
			var PrName = customrecord_frf_detailsSearch[i].getValue("custrecord_opportunity_name_sfdc","custrecord_frf_details_opp_id");
			//nlapiLogExecution('Debug','PrName ',PrName);
			data.account = {"name":customrecord_frf_detailsSearch[i].getText('custrecord_frf_details_account'),"id":customrecord_frf_detailsSearch[i].getValue('custrecord_frf_details_account')};
			data.availabledate = {"name":customrecord_frf_detailsSearch[i].getValue('custrecord_frf_details_availabledate')};
			data.allocateddate = {"name":customrecord_frf_detailsSearch[i].getValue('custrecord_frf_details_allocated_date')};
			data.externalhire = {"name":customrecord_frf_detailsSearch[i].getValue('custrecord_frf_details_external_hire')};
			data.startdate = {"name":customrecord_frf_detailsSearch[i].getValue('custrecord_frf_details_start_date')};
			dataArray.push(data);
		}
		nlapiLogExecution('debug','dataArray %%%% ',JSON.stringify(dataArray));
		return dataArray;
	}
	else
	{
		return [];
	}
	
}

/****Function to get Month Name*****/
function getMonthName(index)
{
	if(index==0)
	{
		return "January";
	}
	if(index==1)
	{
		return "February";
	}
	if(index==2)
	{
		return "March";
	}
	if(index==3)
	{
		return "April";
	}
	if(index==4)
	{
		return "May";
	}
	if(index==5)
	{
		return "June";
	}
	if(index==6)
	{
		return "July";
	}
	if(index==7)
	{
		return "August";
	}
	if(index==8)
	{
		return "September";
	}
	if(index==9)
	{
		return "October";
	}
	if(index==10)
	{
		return "November";
	}
	if(index==11)
	{
		return "December";
	}
}
 
function getProjects(adminUser,spocSearch,region,user,startDate,endDate)
{
	/*Search to get all Training List*/
	var filters = new Array();
	var dataArray = new Array();

	if(adminUser)
	{
		filters = [
		   [["custrecord_frf_details_project","noneof","@NONE@"],
		   "OR",
		   ["custrecord_frf_details_sales_activity","is","Most Likely"]], 
		   "AND", 
		   ["custrecord_frf_details_start_date","within",startDate,endDate],
		   "AND",
		   ["custrecord_frf_details_status","is","F"],
		   "AND", 
		   ["custrecord_frf_details_project.custentity_project_allocation_category","noneof","4"]
		] 
	}
	else if(spocSearch.length>0)
	{
		var practice = spocSearch;//[0].getValue('custrecord_practice');
		if(practice)
		{
			filters = [
					  ["custrecord_frf_details_res_practice","anyof",practice],
					  "AND", 
				      ["custrecord_frf_details_project.custentity_project_allocation_category","noneof","4"],
                      "AND",
					  ["custrecord_frf_details_status","is","F"],
					  "AND",
                      [["custrecord_frf_details_project","noneof","@NONE@"],
		  			   "OR",
		 			   ["custrecord_frf_details_sales_activity","is","Most Likely"]]
					  ];
		}
	}
	else if(region)
	{
		filters = [
					  ["custrecord_frf_details_account.custentity_region","anyof",region],
					  "AND", 
					  ["custrecord_frf_details_project.custentity_project_allocation_category","noneof","4"],
                      "AND",
					  ["custrecord_frf_details_status","is","F"],
					  "AND",
                      [["custrecord_frf_details_project","noneof","@NONE@"],
		  			   "OR",
		 			  ["custrecord_frf_details_sales_activity","is","Most Likely"]]
				  ];
	}
	else if(user) 
	{
		filters = [
		   [["custrecord_frf_details_project","noneof","@NONE@"],
		   "OR",
		   ["custrecord_frf_details_sales_activity","is","Most Likely"]], 
		   "AND", 
		   ["custrecord_frf_details_start_date","within",startDate,endDate],
		   "AND",
		   ["custrecord_frf_details_status","is","F"],
		   "AND", 
		   ["custrecord_frf_details_project.custentity_project_allocation_category","noneof","4"],
		   "AND",
		   [[["custrecord_frf_details_project.custentity_projectmanager","anyof",user],
		   "OR",
		   ["custrecord_frf_details_project.custentity_deliverymanager","anyof",user],
		    "OR",
		   ["custrecord_frf_details_project.custentity_clientpartner","anyof",user]],
		   "OR",
		   [["custrecord_frf_details_opp_id.custrecord_sfdc_opp_pm","anyof",user],
		   "OR",
		   ["custrecord_frf_details_opp_id.custrecord_sfdc_opportunity_dm","anyof",user],
		   "OR",
		   ["custrecord_frf_details_opp_id.custrecord_sfdc_client_partner","anyof",user]]]
		] 
	}
	nlapiLogExecution('debug','filters &&&&&&**** ',filters);
	var customrecord_frf_detailsSearch = nlapiSearchRecord("customrecord_frf_details",null, filters,
	[
	   new nlobjSearchColumn("internalid",null,"COUNT").setSort(true),
	   new nlobjSearchColumn("CUSTRECORD_FRF_DETAILS_OPP_ID",null,"GROUP"), 
	   new nlobjSearchColumn("custrecord_frf_details_project",null,"GROUP"), 
	   new nlobjSearchColumn("custrecord_opportunity_name_sfdc","CUSTRECORD_FRF_DETAILS_OPP_ID","GROUP"),
	]
	);
	if(customrecord_frf_detailsSearch)
	{
		nlapiLogExecution('Debug','customrecord_frf_detailsSearch Length $$$',customrecord_frf_detailsSearch.length);
		var index = customrecord_frf_detailsSearch.length;
		if(customrecord_frf_detailsSearch.length > 5)
		{
			index = 5;
		}
		for(var i=0; i< index; i++)
		{
			nlapiLogExecution('Debug','Project ',customrecord_frf_detailsSearch[i].getText("custrecord_frf_details_project",null,"GROUP"));
			nlapiLogExecution('Debug','Opportunity ',customrecord_frf_detailsSearch[i].getValue("custrecord_opportunity_name_sfdc","CUSTRECORD_FRF_DETAILS_OPP_ID","GROUP"));
			var data = {};
			if(_logValidation(customrecord_frf_detailsSearch[i].getValue("custrecord_opportunity_name_sfdc","CUSTRECORD_FRF_DETAILS_OPP_ID","GROUP")))
			{
				var i_oppId = customrecord_frf_detailsSearch[i].getValue("CUSTRECORD_FRF_DETAILS_OPP_ID",null,"GROUP");
				nlapiLogExecution('debug','i_oppId ',i_oppId);
				if(i_oppId)
					var i_account = nlapiLookupField('customrecord_sfdc_opportunity_record',i_oppId,'custrecord_customer_internal_id_sfdc',true);
				data.projectName = {"name":customrecord_frf_detailsSearch[i].getValue("custrecord_opportunity_name_sfdc","CUSTRECORD_FRF_DETAILS_OPP_ID","GROUP"),"id":customrecord_frf_detailsSearch[i].getValue("CUSTRECORD_FRF_DETAILS_OPP_ID",null,"GROUP")}
				data.salesactivity = {"name":'Most Likely'};
				data.accountName = {"name":i_account};
				projectArray.push(customrecord_frf_detailsSearch[i].getValue("CUSTRECORD_FRF_DETAILS_OPP_ID",null,"GROUP"));
			}
			else if(_logValidation(customrecord_frf_detailsSearch[i].getValue("custrecord_frf_details_project",null,"GROUP")))
			{
				var i_projectId = customrecord_frf_detailsSearch[i].getValue("custrecord_frf_details_project",null,"GROUP");
				if(i_projectId)
					var i_account = nlapiLookupField('job',i_projectId,'customer',true);
				data.projectName = {"name":customrecord_frf_detailsSearch[i].getText("custrecord_frf_details_project",null,"GROUP"),"id":customrecord_frf_detailsSearch[i].getValue("custrecord_frf_details_project",null,"GROUP")}
				data.salesactivity = {"name":'Booked'};
				data.accountName = {"name":i_account};
				projectArrayProject.push(customrecord_frf_detailsSearch[i].getValue("custrecord_frf_details_project",null,"GROUP"));
			}
			dataArray.push(data);
		}
		
		nlapiLogExecution('Debug','projectArray ',JSON.stringify(projectArray));
		nlapiLogExecution('Debug','projectArrayProject ',JSON.stringify(projectArrayProject));
		nlapiLogExecution('Debug','dataArray ',JSON.stringify(dataArray));
		return dataArray;
	}
}

function _logValidation(value) 
{
 if(value!='null' && value != null && value != null && value != '' && value != undefined && value != undefined && value != 'undefined' && value!= 'undefined'&& value!= 'NaN' && value != NaN && value != "- None -" && value != '- None -') 
 {
  return true;
 }
 else 
 { 
  return false;
 }
}
function GetRegion(user) 
{
  	try
	{
		nlapiLogExecution('AUDIT', 'user', user);
		var regionSearch = nlapiSearchRecord("customrecord_region",null,
				[
				 ["custrecord_region_head","anyof",user],
				  "AND",
				 ["isinactive","is","F"]
				], 
				[
				  new nlobjSearchColumn("name")
				]
		);
		if(regionSearch)
		{
			var region = regionSearch[0].getValue('name');
			return region;
		}
		else
		{
			return '';
		}
		
	}
	catch(e)
	{
		return e; 
	}
	
}