// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
/*
   	Script Name : SUT_CreateFRF.js
	Author      : ASHISH PANDIT
	Date        : 27 DEC 2018
    Description : Suitelet to display Create FRF form 


	Script Modification Log:

	-- Date --			-- Modified By --				--Requested By--				-- Description --
 


Below is a summary of the process controls enforced by this script file.  The control logic is described
more fully, below, in the appropriate function headers and code blocks.


     SCHEDULED FUNCTION
		- scheduledFunction(type)


     SUB-FUNCTIONS
		- The following sub-functions are called by the above core functions in order to maintain code
            modularization:

               - NOT USED

*/
}
// END SCRIPT DESCRIPTION BLOCK  ====================================

/*Global Variable*/
var projectCounter = 0;
var counter = 0;
var a_JSON = [];
var a_dataArray = new Array();
var a_frfArray = new Array();
var a_frfDataArray = new Array();
var i_resourceId;
var i_project;
var dataOut = {};
/*Image Links */ 
var calender = '<img src="https://system.netsuite.com/core/media/media.nl?id=1257247&c=3883006&h=ca132a145f59230970fd" class="calender_img">';
var team = '<img src="https://system.netsuite.com/core/media/media.nl?id=1257287&c=3883006&h=85ea67f2e30665186392" class="calender_img">';
var exiting = '<img src="https://system.na1.netsuite.com/core/media/media.nl?id=1257252&c=3883006&h=d124e4b81b5a369c8f0d">';
var rampup = '<img src="https://system.na1.netsuite.com/core/media/media.nl?id=1257243&c=3883006&h=c7d23397233058a5e902">';
var star = '<img src="https://system.na1.netsuite.com/core/media/media.nl?id=1257278&c=3883006&h=ac2e9cfe25078fa374d8">';

/*Suitlet Main function*/
function createFRFMain(request, response){
	try
	{
		var values = new Array();
		var user = nlapiGetUser();
		var i_resourceId = request.getParameter('s_resource'); 
		var i_project = request.getParameter('s_project'); 
		var i_opportunity = request.getParameter('s_opp');
		var i_account;
		var s_projectName;
		var s_accountDisplay;
		nlapiLogExecution('ERROR','Script Parameter i_resourceId ',i_resourceId);
		nlapiLogExecution('ERROR','Script Parameter i_project ',i_project);
		nlapiLogExecution('ERROR','Script Parameter i_opportunity ',i_opportunity);
		var tileData;
		/*Get Project / Opportunity Name*/
		
		if(i_project)
		{
			s_accountDisplay = nlapiLookupField('job',i_project,'customer',true);
			i_account = nlapiLookupField('job',i_project,'customer');
			s_projectName = nlapiLookupField('job',i_project,'companyname');
			tileData = getTileData(i_account,s_projectName,i_opportunity);	
			nlapiLogExecution('Debug','tileData ',tileData);
			var tempArray = new Array();
			tempArray.push(tileData);
			nlapiLogExecution('Debug','tempArray ',tileData);
			values["{project_role_options}"] = getProjectRoleValues(null,select,'Select Project Role',null,i_project);
			values["{tile_data}"] = tileData;
		}
		else if(i_opportunity)
		{
			s_projectName = nlapiLookupField('customrecord_sfdc_opportunity_record',i_opportunity,'custrecord_opportunity_name_sfdc');
			tileData = getTileData(i_account,null,i_opportunity);
            nlapiLogExecution('Debug','tileData ',tileData);
			values["{tile_data}"] = tileData;	
			var s_acc_type = nlapiLookupField('customrecord_sfdc_opportunity_record',i_opportunity,'custrecord_account_type_sfdc');
			nlapiLogExecution('Debug','s_acc_type ',s_acc_type);
			if(s_acc_type=='CUSTOMER')
			{
				s_accountDisplay = nlapiLookupField('customrecord_sfdc_opportunity_record',i_opportunity,'custrecord_customer_internal_id_sfdc',true);
			}
			else
			{
				s_accountDisplay = nlapiLookupField('customrecord_sfdc_opportunity_record',i_opportunity,'custrecord_customer_sfdc');
				nlapiLogExecution('Debug','s_accountDisplay Else',s_accountDisplay);
			}
			nlapiLogExecution('Debug','s_accountDisplay ',s_accountDisplay);
          	values["{project_role_options}"] = getProjectRoleValues(null,select,'Select Project Role',null,i_opportunity);
		}
		//Get the count of FRF
		var countFRF = getFRFCount(i_account,i_project,i_opportunity);
		nlapiLogExecution('AUDIT','FRF Count : ',countFRF);
		/*Get resource details*/
		var searchResult;
		if(i_resourceId)
		{
			searchResult = getResourceDetails(i_resourceId,i_project);
			nlapiLogExecution('Debug','JSON.stringify(a_dataArray) ',JSON.stringify(searchResult));
		}
		values['{homeicon}']=nlapiResolveURL('SUITELET','customscript_fuel_sut_fulfillment_dash','customdeploy_fuel_sut_fulfillment_dash');
      	values['{data}'] = JSON.stringify(searchResult);
		values['{Project_Id}'] = i_project;
		values['{Account_Id}'] = i_account;
		values['{project}'] = s_projectName;
		values['{account}'] = s_accountDisplay;
		values['{backButton}'] = nlapiResolveURL("SUITELET","customscript_sut_projectview","customdeploy_sut_projectview")+"&custpage_projectid="+i_project+"&custpage_oppid="+i_opportunity;
		values['{user}'] = nlapiGetContext().getName();
        var user_id = {"name": nlapiGetUser()};//nlapiGetContext().getName();};
    	values['{user1}'] = JSON.stringify(user_id);
		values['{designation}'] = nlapiLookupField('employee',user,'title');
		var select = "";
		values["{practice_options}"] = getPracticesSearchValues('customsearch2629',select,'Select Practice');//getListValues('department', select, 'Select Practice'); 
		values["{employee_level_options}"] = getEmpLevelValues('customsearch_employee_level_search',select,'Select Employee Level');
		
		values["{employee_search}"] = getSearchValues('customsearch_employee_search',select,'Select Employee');
		
		values["{location_options}"] = getListValues('location', select, 'Select Location');
		
		values["{employee_status}"] = getListValues('customlist_fuel_employee_status', select, 'Select Employee Status');
		values["{education_level}"] = getListValues('customlist_fuel_req_edu_level', select, 'Select Req. Education Level');
		values["{education_program}"] = getListValues('customlist_fuel_education_program', select, 'Select Education Program');
		
		if(s_projectName)
		{
			values['{project_name}'] =s_projectName;
			values['{project_name1}'] =s_projectName;
		}
		else
		{	
			values['{project_name}'] =nlapiLookupField('customrecord_fulfillment_dashboard_data',i_project,'custrecord_fulfill_dashboard_project');
		}
		
		values['{projectcount}'] = counter;
		
		nlapiLogExecution('debug','Gender',nlapiLookupField('employee',user,'gender'));
		
		/*Login user image*/
		if(nlapiLookupField('employee',user,'gender') == 'f'){
			values['{profilepic}'] = '/core/media/media.nl?id=1257303&c=3883006&h=b0e4f8408d1e0d948928';
		}else{
			values['{profilepic}'] = '/core/media/media.nl?id=1257271&c=3883006&h=ad48de7349910d7225a6';
		}
		
		var contents = nlapiLoadFile('1257136').getValue();
		contents = replaceValues(contents, values);
		//response.writeLine(JSON.stringify(arrayObj));
		response.write(contents);
	}
	catch(error)
	{
		nlapiLogExecution('Debug','error ',error);
	}
}

/** Function to get resource details**/

function getResourceDetails(i_resourceId,i_project)
{
	nlapiLogExecution('Debug','i_resourceId ',i_resourceId);
	nlapiLogExecution('Debug','i_project ',i_project);
	var resourceallocationSearch = nlapiSearchRecord("resourceallocation",null,
	[
	   ["resource","anyof",i_resourceId], 
	   "AND", 
	   ["project","anyof",i_project],
	   "AND",
	   ["enddate","onorafter","today"]
	], 
	[
	   new nlobjSearchColumn("custevent_practice"), 
	   new nlobjSearchColumn("level","employee",null), 
	   new nlobjSearchColumn("location","employee",null), 
	   new nlobjSearchColumn("custentity_lwd","employee",null),
	   new nlobjSearchColumn("title","employee",null),
	   new nlobjSearchColumn("internalid","employee",null),
	   new nlobjSearchColumn("employeestatus","employee",null),
	   new nlobjSearchColumn("custevent3"),
	   new nlobjSearchColumn("company"),
	]
	);
	if(resourceallocationSearch)
	{
		nlapiLogExecution('Debug','resourceallocationSearch.length ',resourceallocationSearch.length);
		var i_project = resourceallocationSearch[0].getValue("company");
		var i_resource = resourceallocationSearch[0].getValue("internalid","employee",null);
		var i_practice = resourceallocationSearch[0].getValue("custevent_practice");
		var d_lwd = resourceallocationSearch[0].getValue("custentity_lwd","employee",null);
		var s_frf_source;
		if(d_lwd)
			s_frf_source = "Attrition";
		else
			s_frf_source = "Rotation";
		var i_frf = '1';
		//var s_skillFamily = getTaggedFamilies(i_practice);
		
		
		//var dataOut = {};
		dataOut.project = {"name":resourceallocationSearch[0].getText("company"),"id":resourceallocationSearch[0].getValue("company")};
		dataOut.account ={"name" : nlapiLookupField('job',i_project,'customer',true),"id" :nlapiLookupField('job',i_project,'customer')} ;
		//dataOut.resource ={"name" : resourceallocationSearch[0].getText("internalid","employee",null),"id" : resourceallocationSearch[0].getValue("internalid","employee",null)} ;
		dataOut.practice = {"name" : resourceallocationSearch[0].getText("custevent_practice"),"id" : resourceallocationSearch[0].getValue("custevent_practice")};
		dataOut.emplevel = {"name" : resourceallocationSearch[0].getText("employeestatus","employee",null),"id" : resourceallocationSearch[0].getValue("employeestatus","employee",null)};
		dataOut.frfsource ={"name" : s_frf_source};
		dataOut.location ={"name" : resourceallocationSearch[0].getText("location","employee",null),"id":resourceallocationSearch[0].getValue("location","employee",null)};
		getEmployeeSkills(i_resource);
		//dataOut.family = s_skillFamily;
		//dataOut.primaryskills =s_skills;
		//dataOut.otherskills = {"name" : searchResult[int].getText('custrecord_frf_plan_details_other_skills'),"id" : searchResult[int].getText('custrecord_frf_plan_details_other_skills')};
		
		a_dataArray.push(dataOut);
	}
	return dataOut;
}
/*******************************Replace values Function***************************************************/
function replaceValues(content, oValues)
{
	for(param in oValues)
	{
		// Replace null values with blank
		var s_value	=	(oValues[param] == null)?'':oValues[param];

		// Replace content
		//content = content.replace(new RegExp('{{' + param + '}}','gi'), s_value);
		content = content.replace(param, s_value);
	}

	return content;
}

/************************************Function to get Employee Skills******************************************************/
function getEmployeeSkills(emp_id)
{
	try
	{
		nlapiLogExecution('Debug','Skill ',emp_id);
		var a_resultArray = new Array();
		var customrecord_employee_master_skill_dataSearch = nlapiSearchRecord("customrecord_employee_master_skill_data",null,
		[
		   ["custrecord_employee_skill_updated","anyof",emp_id]
		], 
		[
		   new nlobjSearchColumn("custrecord_employee_skill_updated"), 
		   new nlobjSearchColumn("custrecord_primary_updated"), 
		   new nlobjSearchColumn("custrecord_secondry_updated"),
		   new nlobjSearchColumn("custrecord_family_selected")
		]
		);
		if(customrecord_employee_master_skill_dataSearch)
		{
			dataOut.family = {"name": customrecord_employee_master_skill_dataSearch[0].getText("custrecord_family_selected"), "id": customrecord_employee_master_skill_dataSearch[0].getValue('custrecord_family_selected')};
			dataOut.primaryskills = {"name": customrecord_employee_master_skill_dataSearch[0].getText("custrecord_primary_updated"), "id": customrecord_employee_master_skill_dataSearch[0].getValue('custrecord_primary_updated')};
			dataOut.otherskills = {"name": customrecord_employee_master_skill_dataSearch[0].getText("custrecord_secondry_updated"), "id": customrecord_employee_master_skill_dataSearch[0].getValue('custrecord_secondry_updated')};
		}
		else
		{
			dataOut.family = {"name": "", "id": ""};
			dataOut.primaryskills = {"name": "", "id": ""};
			dataOut.otherskills = {"name": "", "id": ""};
		}
	}
	catch(error)
	{
		nlapiLogExecution('Debug','Error at getEmployeeSkills ',error);
	}
	
}
/******************************Function to get skill Families***********************************************/
function getTaggedFamilies(i_emp_dep)
{
	try{
		var a_resultArray = new Array();
		var parent_practice = nlapiLookupField('department',i_emp_dep,'custrecord_parent_practice');
		var practice_filter = [
					
		             [['custrecord_skill_emp_dep', 'anyOf', i_emp_dep ],
				     'or',
					 [ 'custrecord_skill_emp_dep', 'anyOf', parent_practice]],
		              'and',
		             [ 'isinactive','anyOf', 'F']];

		var practice_search_results = searchRecord('customrecord_employee_skill_family_list', null, practice_filter,
		        [ new nlobjSearchColumn("name"),
		          new nlobjSearchColumn("internalid")
				]);
				if(practice_search_results)
				{
					for(var i=0;i<practice_search_results.length;i++)
					{
						var dataOut2 = {};
						dataOut2 = {"name":practice_search_results[i].getValue("name"),"id":practice_search_results[i].getId()};
						a_resultArray.push(dataOut2);
					}
				}
		return a_resultArray;
	   }
	   catch(err)
	   {
			throw err;
	   }
}
/*********************************************Function to get List Values**********************************************/
function getListValues(s_list_name, strSelectedValue, s_placeholder, removeBlankOption)
{
	var columns = new Array();
	columns[0]	=	new nlobjSearchColumn('name');
	
	var a_search_results = searchRecord(s_list_name, null, null, columns);
	
	var list_values = new Array();
	
	for(var i = 0; a_search_results != null && i < a_search_results.length; i++)
		{
			list_values.push({'display':a_search_results[i].getValue('name'), 'value':a_search_results[i].id});
		}
	
	return getOptions(list_values, strSelectedValue, s_placeholder, removeBlankOption);
}
/*********************************************Function to Project Role Values**********************************************/
function getProjectRoleValues(s_list_name, strSelectedValue, s_placeholder, removeBlankOption,i_project){
	var a_search_results = nlapiSearchRecord("customrecord_frf_plan_details",null,
	[
	   ["custrecord_frf_plan_details_project","anyof",i_project],
       "OR",
       ["custrecord_frf_plan_details_opp_id","anyof",i_project]
	], 
	[
	   new nlobjSearchColumn("custrecord_frf_plan_details_project_role",null,"GROUP")
	]
	);
	var list_values = new Array();
	if(a_search_results)
	{
		for(var i = 0; a_search_results != null && i < a_search_results.length; i++)
		{
			list_values.push({'display':a_search_results[i].getValue("custrecord_frf_plan_details_project_role",null,"GROUP"), 'value':a_search_results[i].getValue("custrecord_frf_plan_details_project_role",null,"GROUP")});
		}
	}
	return getOptions(list_values, strSelectedValue, s_placeholder, removeBlankOption);
}

/*********************************************Function to get Search Values**********************************************/
function getSearchValues(s_list_name, strSelectedValue, s_placeholder, removeBlankOption)
{
	var columns = new Array();
	columns[0]	=	new nlobjSearchColumn('entityid');
	
	var a_search_results = searchRecord(null, s_list_name, null, columns);
	
	var list_values = new Array();
	
	for(var i = 0; a_search_results != null && i < a_search_results.length; i++)
		{
			list_values.push({'name':a_search_results[i].getValue('entityid'), 'id':a_search_results[i].id});
		}
	nlapiLogExecution('Debug','list_values ',JSON.stringify(list_values));
	return JSON.stringify(list_values);//getOptions(list_values, strSelectedValue, s_placeholder, removeBlankOption);
}
/**********************************************Function to get parent practices************************************/
function getPracticesSearchValues(s_list_name, strSelectedValue, s_placeholder, removeBlankOption)
{
	var columns = new Array();
	columns[0]	=	new nlobjSearchColumn('name');
	var a_search_results = searchRecord(null, s_list_name, null, columns);
	
	var list_values = new Array();
	
	for(var i = 0; a_search_results != null && i < a_search_results.length; i++)
		{
			list_values.push({'display':a_search_results[i].getValue('name'), 'value':a_search_results[i].getId()});
		}
	nlapiLogExecution('ERROR','list_values ',JSON.stringify(list_values));
	return getOptions(list_values, strSelectedValue, s_placeholder, removeBlankOption);
}
/******************************************END*********************************************************************/
/*********************************************Function to get Employee Level Values**********************************************/
function getEmpLevelValues(s_list_name, strSelectedValue, s_placeholder, removeBlankOption)
{
	var columns = new Array();
	columns[0]	=	new nlobjSearchColumn("employeestatus",null,"GROUP");
	
	var a_search_results = searchRecord(null, s_list_name, null, columns);
	
	var list_values = new Array();
	
	for(var i = 0; a_search_results != null && i < a_search_results.length; i++)
		{
			list_values.push({'display':a_search_results[i].getText("employeestatus",null,"GROUP"), 'value':a_search_results[i].getText("employeestatus",null,"GROUP")});
		}
	
	return getOptions(list_values, strSelectedValue, s_placeholder, removeBlankOption);
}

/*********************************************Function to get Search Results**********************************************/
function searchRecord(recordType, savedSearch, arrFilters, arrColumns,
        filterExpression)
{

	try {
		var search = null;

		// if a saved search is provided, load it and add the filters and
		// columns
		if (isNotEmpty(savedSearch)) {
			search = nlapiLoadSearch(recordType, savedSearch);

			if (isArrayNotEmpty(arrFilters)) {
				search.addFilters(arrFilters);
			}

			if (isArrayNotEmpty(arrColumns)) {
				search.addColumns(arrColumns);
			}

			if (isArrayNotEmpty(filterExpression)) {
				search.setFilterExpression(filterExpression);
			}
		}
		// create a new search
		else {
			search = nlapiCreateSearch(recordType, arrFilters, arrColumns);
		}

		// run search
		var resultSet = search.runSearch();

		// iterate through the search and get all data 1000 at a time
		var searchResultCount = 0;
		var resultSlice = null;
		var searchResult = [];

		do {
			resultSlice = resultSet.getResults(searchResultCount,
			        searchResultCount + 1000);

			if (resultSlice) {

				resultSlice.forEach(function(result) {

					searchResult.push(result);
					searchResultCount++;
				});
			}
		} while (isArrayNotEmpty(resultSlice) && resultSlice.length >= 1000);

		return searchResult;
	} catch (err) {
		nlapiLogExecution('ERROR', 'searchRecord', err);
		throw err;
	}
}

function isEmpty(value) {

	return value == null || value == "" || typeof (value) == undefined;
}

function isNotEmpty(value) {

	return !isEmpty(value);
}

function isArrayEmpty(argArray) {

	return !isArrayNotEmpty(argArray);
}

function isArrayNotEmpty(argArray) {

	return (isNotEmpty(argArray) && argArray.length > 0);
}

/*********************************************Function for adding selected options**********************************************/
function getOptions(a_data, i_selected_value, s_placeholder, removeBlankOptions)
{
	var strOptions	=	'';
	
	if(removeBlankOptions == true)
		{
		
		}
	else
		{
			strOptions = '<option value = "">' + (s_placeholder == undefined?'':s_placeholder) + '</option>';
		}

	var strSelected = '';
	
	for(var i = 0; i < a_data.length; i++)
	{
		if(i_selected_value == a_data[i].value)
			{
				strSelected = 'selected';
			}
		else
			{
				strSelected = '';
			}
		strOptions += '<option value = "'+a_data[i].value+'" ' + strSelected + ' >' + a_data[i].display + '</option>';
	}
//nlapiLogExecution('Debug', 'stringsss', strOptions);
	return strOptions;
}
/***********************************************Function to get tile data***********************************/
function getTileData(i_account,s_projectName,i_opportunity)
{
	nlapiLogExecution('Debug','i_account ',i_account);
	nlapiLogExecution('Debug','s_projectName ',s_projectName);
	nlapiLogExecution('Debug','i_opportunity ',i_opportunity);
	var filters = [];
			if(i_account)
				filters.push(new nlobjSearchFilter('custrecord_frf_plan_details_account',null,'anyof',i_account));
			if(s_projectName)
				filters.push(new nlobjSearchFilter('custrecord_frf_plan_details_project',null,'anyof',s_projectName));
			if(i_opportunity)
				filters.push(new nlobjSearchFilter('custrecord_frf_plan_details_opp_id',null,'anyof',i_opportunity));
			filters.push(new nlobjSearchFilter('custrecord_frf_plan_details_confirmed',null,'is','F'));
				
			var customrecord_frf_plan_detailsSearch = nlapiSearchRecord("customrecord_frf_plan_details",null, filters,
			[
			   new nlobjSearchColumn("internalid"), 
			   new nlobjSearchColumn("custrecord_frf_plan_details_project_role"), 
			   new nlobjSearchColumn("custrecord_frf_plan_details_startdate"), 
			   new nlobjSearchColumn("custrecord_frf_plan_details_positions"), 
			   new nlobjSearchColumn("custrecord_frf_plan_details"), 
			   new nlobjSearchColumn("custrecord_frf_plan_details_account"), 
			   new nlobjSearchColumn("custrecord_frf_plan_details_allocation"), 
			   new nlobjSearchColumn("custrecord_frf_plan_details_bill_rate"), 
			   new nlobjSearchColumn("custrecord_frf_plan_details_bill_role"), 
			   new nlobjSearchColumn("custrecord_frf_plan_details_criticalrole"), 
			   new nlobjSearchColumn("custrecord_frf_plan_details_emp_lvl"), 
			   new nlobjSearchColumn("custrecord_frf_plan_details_enddate"), 
			   new nlobjSearchColumn("custrecord_frf_plan_details_externalhire"), 
			   new nlobjSearchColumn("custrecord_frf_plan_details_location"), 
			   new nlobjSearchColumn("custrecord_frf_plan_details_opp_id"), 
			   new nlobjSearchColumn("custrecord_frf_plan_details_primary"), 
			   new nlobjSearchColumn("custrecord_frf_plan_details_practice"), 
			   new nlobjSearchColumn("custrecord_frf_plan_details_project"), 
			   new nlobjSearchColumn("custrecord_frf_plan_details_skill_family"), 
			   new nlobjSearchColumn("custrecord_frf_plan_details_special_req"), 
			   new nlobjSearchColumn("custrecord_frf_plan_details_suggest_res"), 
			   new nlobjSearchColumn("custrecord_frf_plan_details_other_skills"), 
			   new nlobjSearchColumn("custrecord_frf_plan_dollar_loss"),
			   new nlobjSearchColumn("custrecord_frf_plan_first_inte_rrf"), 
			   //=====================================================================
			   new nlobjSearchColumn("custrecord_frf_plan_sec_inte_rrf"), 
			   new nlobjSearchColumn("custrecord_frf_plan_reason_rrf"), 
			   new nlobjSearchColumn("custrecord_frf_pla_det_backup_require"), 
			   new nlobjSearchColumn("custrecord_frf_plan_desc_rrf"), 
			   new nlobjSearchColumn("custrecord_frf_plan_job_title_rrf"), 
			   new nlobjSearchColumn("custrecord_frf_plan_edu_lvl_rrf"), 
			   new nlobjSearchColumn("custrecord_frf_plan_edu_pro_rrf"), 
			   new nlobjSearchColumn("custrecord_frf_plan_emp_status_rrf"), 
			   new nlobjSearchColumn("custrecord_frf_plan_cus_inter_rrf"), 
			   new nlobjSearchColumn("custrecord_frf_plan_cust_inter_email_rrf"), 
			   new nlobjSearchColumn("custrecord_frf_plan_instruction_rrf"), 
			   new nlobjSearchColumn("custrecord_frf_plan_res_rrf")
			]
			);
	
	if(customrecord_frf_plan_detailsSearch)
	{
		var tileDataArray = new Array();
		nlapiLogExecution('Debug','customrecord_frf_plan_detailsSearch ',customrecord_frf_plan_detailsSearch.length);
		for(var ii = 0; ii<customrecord_frf_plan_detailsSearch.length; ii++)
		{
			var b_billRoll = customrecord_frf_plan_detailsSearch[ii].getValue("custrecord_frf_plan_details_bill_role");
			if(b_billRoll=='T')
			{
				b_billRoll=true;
			}
			else
			{
				b_billRoll = false;
			}
			
			var b_criticalRoll=customrecord_frf_plan_detailsSearch[ii].getValue("custrecord_frf_plan_details_criticalrole");
			if(b_criticalRoll=='T')
			{
				b_criticalRoll=true;
			}
			else
			{
				b_criticalRoll = false;
			}
			
			var b_externalHire =customrecord_frf_plan_detailsSearch[ii].getValue("custrecord_frf_plan_details_externalhire");
			if(b_externalHire=='T')
			{
				b_externalHire=true;
			}
			else
			{
				b_externalHire = false;
			}
			var tileData = {};
            tileData.recordid = {"name":customrecord_frf_plan_detailsSearch[ii].getId()};
			tileData.role = {"name":customrecord_frf_plan_detailsSearch[ii].getValue("custrecord_frf_plan_details_project_role")};
			tileData.startDate = {"name":customrecord_frf_plan_detailsSearch[ii].getValue("custrecord_frf_plan_details_startdate")};
			tileData.endDate = {"name":customrecord_frf_plan_detailsSearch[ii].getValue("custrecord_frf_plan_details_enddate")};
			tileData.positions = {"name":customrecord_frf_plan_detailsSearch[ii].getValue("custrecord_frf_plan_details_positions")};
			tileData.Allocation = {"name":customrecord_frf_plan_detailsSearch[ii].getValue("custrecord_frf_plan_details_allocation")};
			tileData.billrate = {"name":customrecord_frf_plan_detailsSearch[ii].getValue("custrecord_frf_plan_details_bill_rate")};
			tileData.billrole = {"name":b_billRoll};
			tileData.splrequirment = {"name":customrecord_frf_plan_detailsSearch[ii].getValue("custrecord_frf_plan_details_special_req")};
			tileData.dollarloss = {"name":customrecord_frf_plan_detailsSearch[ii].getValue("custrecord_frf_plan_dollar_loss")};
			tileData.externalhire = {"name":b_externalHire};
			tileData.criticalrole = {"name":b_criticalRoll};
			tileData.account = {"name":customrecord_frf_plan_detailsSearch[ii].getText("custrecord_frf_plan_details_account"),"id":customrecord_frf_plan_detailsSearch[ii].getValue("custrecord_frf_plan_details_account")};
			tileData.emplevel = {"name":customrecord_frf_plan_detailsSearch[ii].getText("custrecord_frf_plan_details_emp_lvl"),"id":customrecord_frf_plan_detailsSearch[ii].getValue("custrecord_frf_plan_details_emp_lvl")};
			tileData.location = {"name":customrecord_frf_plan_detailsSearch[ii].getText("custrecord_frf_plan_details_location"),"id":customrecord_frf_plan_detailsSearch[ii].getValue("custrecord_frf_plan_details_location")};
			tileData.primaryskills = {"name":customrecord_frf_plan_detailsSearch[ii].getText("custrecord_frf_plan_details_primary"),"id":customrecord_frf_plan_detailsSearch[ii].getValue("custrecord_frf_plan_details_primary")};
			tileData.practice = {"name":customrecord_frf_plan_detailsSearch[ii].getText("custrecord_frf_plan_details_practice"),"id":customrecord_frf_plan_detailsSearch[ii].getValue("custrecord_frf_plan_details_practice")};
			tileData.project = {"name":customrecord_frf_plan_detailsSearch[ii].getText("custrecord_frf_plan_details_project"),"id":customrecord_frf_plan_detailsSearch[ii].getValue("custrecord_frf_plan_details_project")};
			tileData.family = {"name":customrecord_frf_plan_detailsSearch[ii].getText("custrecord_frf_plan_details_skill_family"),"id":customrecord_frf_plan_detailsSearch[ii].getValue("custrecord_frf_plan_details_skill_family")};
			
			var suggestedPeopleData= {"name":customrecord_frf_plan_detailsSearch[0].getText("custrecord_frf_plan_details_suggest_res"),"id":customrecord_frf_plan_detailsSearch[0].getValue("custrecord_frf_plan_details_suggest_res")};
				var suggestedPeopleArray=[];
				for(var j=0;j<suggestedPeopleData.name.split(",").length;j++){

				suggestedPeopleArray.push({
				name:suggestedPeopleData.name.split(",")[j],
				id:suggestedPeopleData.id.split(",")[j]
				})
				}
				
			
			tileData.suggestedpeople = suggestedPeopleArray;//{"name":customrecord_frf_plan_detailsSearch[ii].getText("custrecord_frf_plan_details_suggest_res"),"id":customrecord_frf_plan_detailsSearch[ii].getValue("custrecord_frf_plan_details_suggest_res")};
			
			tileData.otherskills = {"name":customrecord_frf_plan_detailsSearch[ii].getText("custrecord_frf_plan_details_other_skills"),"id":customrecord_frf_plan_detailsSearch[ii].getValue("custrecord_frf_plan_details_other_skills")};
			tileData.frfsource = {"name":customrecord_frf_plan_detailsSearch[ii].getText("custrecord_frf_plan_details"),"id":customrecord_frf_plan_detailsSearch[ii].getValue("custrecord_frf_plan_details")};
			
			//===============================================================================================
			tileData.backuprequired = {"name":customrecord_frf_plan_detailsSearch[ii].getValue("custrecord_frf_pla_det_backup_require")};
			tileData.jobtitle = {"name":customrecord_frf_plan_detailsSearch[ii].getValue("custrecord_frf_plan_job_title_rrf")};
			tileData.jobdescription = {"name":customrecord_frf_plan_detailsSearch[ii].getValue("custrecord_frf_plan_desc_rrf")};
			tileData.instruction = {"name":customrecord_frf_plan_detailsSearch[ii].getValue("custrecord_frf_plan_instruction_rrf")};
			tileData.educationlevel = {"name":customrecord_frf_plan_detailsSearch[ii].getText("custrecord_frf_plan_edu_lvl_rrf"),"id":customrecord_frf_plan_detailsSearch[ii].getValue("custrecord_frf_plan_edu_lvl_rrf")};
			tileData.educationprogram = {"name":customrecord_frf_plan_detailsSearch[ii].getText("custrecord_frf_plan_edu_pro_rrf"),"id":customrecord_frf_plan_detailsSearch[ii].getValue("custrecord_frf_plan_edu_pro_rrf")};
			tileData.employeestatus = {"name":customrecord_frf_plan_detailsSearch[ii].getText("custrecord_frf_plan_emp_status_rrf"),"id":customrecord_frf_plan_detailsSearch[ii].getValue("custrecord_frf_plan_emp_status_rrf")};
			tileData.firstinterviewer = {"name":customrecord_frf_plan_detailsSearch[ii].getValue("custrecord_frf_plan_first_inte_rrf")};
			tileData.secondinterviewer = {"name":customrecord_frf_plan_detailsSearch[ii].getValue("custrecord_frf_plan_sec_inte_rrf")};
			tileData.custinterview = {"name":customrecord_frf_plan_detailsSearch[ii].getValue("custrecord_frf_plan_cus_inter_rrf")};
			tileData.custintervieweremail = {"name":customrecord_frf_plan_detailsSearch[ii].getValue("custrecord_frf_plan_cust_inter_email_rrf")};
			tileData.externalhirereason = {"name":customrecord_frf_plan_detailsSearch[ii].getValue("custrecord_frf_plan_reason_rrf")};
			//tileDataArray.push(tileData);
			tileDataArray.push(tileData);
		}
		nlapiLogExecution('Debug','tileDataArray$$$$$ ',JSON.stringify(tileDataArray));
		return JSON.stringify(tileDataArray);
	}
}
function getFRFCount(i_account,i_project,i_oppid)
{
		var filters = [];
		var count = 0;
		if(i_account)
			filters.push(new nlobjSearchFilter('custrecord_frf_plan_details_account',null,'anyof',i_account));
		if(i_project)
			filters.push(new nlobjSearchFilter('custrecord_frf_plan_details_project',null,'anyof',i_project));
		if(i_oppid)
			filters.push(new nlobjSearchFilter('custrecord_frf_plan_details_opp_id',null,'anyof',i_oppid));
		filters.push(new nlobjSearchFilter('custrecord_frf_plan_details_confirmed',null,'is','F'));
			
		var customrecord_frf_plan_detailsSearch = nlapiSearchRecord("customrecord_frf_plan_details",null, filters,
		[
		   new nlobjSearchColumn("custrecord_frf_plan_details_opp_id"),
		   new nlobjSearchColumn("custrecord_frf_plan_details_positions")
		]
		);
		if(customrecord_frf_plan_detailsSearch){
			for(var i = 0 ; i < customrecord_frf_plan_detailsSearch.length;i++){
				count += parseInt(customrecord_frf_plan_detailsSearch[i].getValue("custrecord_frf_plan_details_positions"));
			}
		}
		return count;
}