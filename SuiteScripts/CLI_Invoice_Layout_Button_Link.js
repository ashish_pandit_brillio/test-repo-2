/**
 * @author Shweta
 */
// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
/*
    Script Name : CLI_Invoice_Layout_Button_Link.js
	Author      : Shweta Chopde
	Date        : 14 July 2014
	Description : Open the suitlet window


	Script Modification Log:

	-- Date --			-- Modified By --				--Requested By--				-- Description --
   26 June 2014           Shweta Chopde                      Shekar                      Modified IDs as per the sandbox accounts
 


	Below is a summary of the process controls enforced by this script file.  The control logic is described
	more fully, below, in the appropriate function headers and code blocks.

     PAGE INIT
		- pageInit(type)


     SAVE RECORD
		- saveRecord()


     VALIDATE FIELD
		- validateField(type, name, linenum)


     FIELD CHANGED
		- fieldChanged(type, name, linenum)


     POST SOURCING
		- postSourcing(type, name)


	LINE INIT
		- lineInit(type)


     VALIDATE LINE
		- validateLine()


     RECALC
		- reCalc()


     SUB-FUNCTIONS
		- The following sub-functions are called by the above core functions in order to maintain code
            modularization:





*/
}
// END SCRIPT DESCRIPTION BLOCK  ====================================



// BEGIN SCRIPT UPDATION BLOCK  ====================================
/*


*/
// END SCRIPT UPDATION BLOCK  ====================================




// BEGIN GLOBAL VARIABLE BLOCK  =====================================
{

	//  Initialize any Global Variables, in particular, debugging variables...


}
// END GLOBAL VARIABLE BLOCK  =======================================





// BEGIN PAGE INIT ==================================================

function pageInit(type)
{
    /*  On page init:

	- PURPOSE

		FIELDS USED:

		--Field Name--				--ID--			--Line Item Name--

    */

    //  LOCAL VARIABLES


    //  PAGE INIT CODE BODY


}

// END PAGE INIT ====================================================





// BEGIN SAVE RECORD ================================================

function saveRecord()
{
    /*  On save record:

	- PURPOSE



	  FIELDS USED:

		--Field Name--			--ID--		--Line Item Name--


    */


    //  LOCAL VARIABLES



    //  SAVE RECORD CODE BODY


	return true;

}

// END SAVE RECORD ==================================================





// BEGIN VALIDATE FIELD =============================================

function validateField(type, name, linenum)
{

	/*  On validate field:

          - EXPLAIN THE PURPOSE OF THIS FUNCTION


          FIELDS USED:

          --Field Name--				--ID--


    */


	//  LOCAL VARIABLES



	//  VALIDATE FIELD CODE BODY


	return true;

}

// END VALIDATE FIELD ===============================================





// BEGIN FIELD CHANGED ==============================================

function fieldChanged(type, name, linenum)
{
    /*  On field changed:

          - PURPOSE


          FIELDS USED:

			--Field Name--				--ID--


    */


    //  LOCAL VARIABLES


    //  FIELD CHANGED CODE BODY

	//alert('Field Name='+name+'\nLine Item Type='+type+'\nField Value='+nlapiGetFieldValue(name)+'\nLine Item Value='+nlapiGetLineItemValue(type, name, 1));

}

// END FIELD CHANGED ================================================





// BEGIN POST SOURCING ==============================================

function postSourcing(type, name)
{

    /*  On post sourcing:

	- PURPOSE

          FIELDS USED:

		--Field Name--			--ID--		--Line Item Name--


    */

    //  LOCAL VARIABLES


    //  POST SOURCING CODE BODY



}

// END POST SOURCING ================================================



// BEGIN LINE INIT ==============================================

function lineInit(type)
{

    /*  On Line Init:

	- PURPOSE

          FIELDS USED:

		--Field Name--			--ID--		--Line Item Name--


    */

    //  LOCAL VARIABLES


    //  LINE INIT CODE BODY



}

// END LINE INIT ================================================


// BEGIN VALIDATE LINE ==============================================

function validateLine(type)
{

	/*  On validate line:

          - EXPLAIN THE PURPOSE OF THIS FUNCTION


          FIELDS USED:

          --Field Name--				--ID--


    */


	//  LOCAL VARIABLES


	//  VALIDATE LINE CODE BODY


	return true;

}

// END VALIDATE LINE ================================================




// BEGIN RECALC =====================================================

function recalc(type)
{

	/*  On recalc:

          - EXPLAIN THE PURPOSE OF THIS FUNCTION


          FIELDS USED:

          --Field Name--				--ID--


    */


	//  LOCAL VARIABLES


	//  RECALC CODE BODY


}

// END RECALC =======================================================





// BEGIN FUNCTION ===================================================

function open_BLLC_T_M_Invoice()
{	
   var i_recordID = nlapiGetRecordId();
   
   window.open('/app/site/hosting/scriptlet.nl?script=208&deploy=1&i_recordID='+i_recordID)
	
}//BLLC T&M

function open_BLLC_T_M_Group_Invoice()
{	
   var i_recordID = nlapiGetRecordId();
   
   window.open('/app/site/hosting/scriptlet.nl?script=209&deploy=1&i_recordID='+i_recordID)
	
}//BLLC T&M Group

function open_BLLC_T_M_Group_Dis_Invoice()
{	
   var i_recordID = nlapiGetRecordId();
   
   window.open('/app/site/hosting/scriptlet.nl?script=327&deploy=1&i_recordID='+i_recordID)
	
}//BLLC T&M Group Discount

function open_BLLC_T_M_Group_Move_Invoice()
{	
   var i_recordID = nlapiGetRecordId();
   
   window.open('/app/site/hosting/scriptlet.nl?script=329&deploy=1&i_recordID='+i_recordID)
	
}//BLLC T&M Move Discount

function open_BLLC_FP_Invoice()
{	
   var i_recordID = nlapiGetRecordId();
   
   window.open('/app/site/hosting/scriptlet.nl?script=210&deploy=1&i_recordID='+i_recordID)
	
}//BLLC FP

function open_BLLC_Exp_Invoice()
{	
   var i_recordID = nlapiGetRecordId();
   
   window.open('/app/site/hosting/scriptlet.nl?script=211&deploy=1&i_recordID='+i_recordID)
	
}//BLLC Exp

function open_Exports_BLR_US_Onsite_Invoice()
{	
   var i_recordID = nlapiGetRecordId();
   
   window.open('/app/site/hosting/scriptlet.nl?script=228&deploy=1&i_recordID='+i_recordID)
	
}//Exports- BLR US Onsite

function open_Exports_BLR_UK_Invoice()
{	
   var i_recordID = nlapiGetRecordId();
   
   window.open('/app/site/hosting/scriptlet.nl?script=226&deploy=1&i_recordID='+i_recordID)
	
}//Exports- BLR UK
function open_Domestic_BLR_Invoice()
{	
   var i_recordID = nlapiGetRecordId();
   
   window.open('/app/site/hosting/scriptlet.nl?script=224&deploy=1&i_recordID='+i_recordID)
	
}//Domestic- BLR
function open_Domestic_Tvm_T_M_Invoice()
{	
   var i_recordID = nlapiGetRecordId();
   
   window.open('/app/site/hosting/scriptlet.nl?script=225&deploy=1&i_recordID='+i_recordID)
	
}//Domestic- Tvm T&M
function open_Dom_Tvm_License_Invoice()
{	
   var i_recordID = nlapiGetRecordId();
   
   window.open('/app/site/hosting/scriptlet.nl?script=223&deploy=1&i_recordID='+i_recordID)
	
}//Dom- Tvm License
function open_Dom_Tvm_FP_Invoice()
{	
   var i_recordID = nlapiGetRecordId();
   
   window.open('/app/site/hosting/scriptlet.nl?script=222&deploy=1&i_recordID='+i_recordID)
	
}//Dom- Tvm FP

function open_Exports_BLR_US_Offshore_Invoice()
{	
   var i_recordID = nlapiGetRecordId();
   
   window.open('/app/site/hosting/scriptlet.nl?script=227&deploy=1&i_recordID='+i_recordID)
	
}//Exports - BLR US Offshore

function open_BLLC_TM_Discount()
{	
   var i_recordID = nlapiGetRecordId();
   
   window.open('/app/site/hosting/scriptlet.nl?script=629&deploy=1&invoice='+i_recordID)
	
}//TM Discount BLLC

function open_BLLC_Comity_TM()
{	
   var i_recordID = nlapiGetRecordId();
  // alert(i_recordID);
   window.open('/app/site/hosting/scriptlet.nl?script=1636&deploy=1&i_recordID='+i_recordID)
	
}//TM Comity BLLC

function open_BLLC_Comity_FP()
{	
   var i_recordID = nlapiGetRecordId();
   
   window.open('/app/site/hosting/scriptlet.nl?script=1637&deploy=1&i_recordID='+i_recordID)
	
}//FP Comity BLLC

function open_UK_FP()
{	
   var i_recordID = nlapiGetRecordId();
   
   window.open('/app/site/hosting/scriptlet.nl?script=1798&deploy=1&i_recordID='+i_recordID)
	
}//FP UK
function open_Brillio_Canada_TM() {
   var i_recordID = nlapiGetRecordId();

   window.open('/app/site/hosting/scriptlet.nl?script=2240&deploy=1&i_recordID=' + i_recordID)
}//TM Canada

function open_Brillio_Canada_FP() {
   var i_recordID = nlapiGetRecordId();
   window.open('/app/site/hosting/scriptlet.nl?script=2238&deploy=1&i_recordID=' + i_recordID)
}//




// END FUNCTION =====================================================
