/**
 * @author Deepak MS Date:20 Feb 2017
 */

function scheduled_fp_others_create_effort_plan(type)
{
	try
	{
		nlapiLogExecution('DEBUG','test','test');
		/*var a_get_revenue_cap = nlapiSearchRecord('customrecord_fp_revrec_month_end_effort');
		if (a_get_revenue_cap) {
			for(var i=0; i<a_get_revenue_cap.length; i++)
				nlapiDeleteRecord('customrecord_fp_revrec_month_end_effort',a_get_revenue_cap[i].getId());
		}
		
		var a_get_revenue_cap = nlapiSearchRecord('customrecord_fp_revrec_month_end_process');
		if (a_get_revenue_cap) {
			for(var i=0; i<a_get_revenue_cap.length; i++)
				nlapiDeleteRecord('customrecord_fp_revrec_month_end_process',a_get_revenue_cap[i].getId());
		}*/
		//return;
		var i_fp_others_plan_parent = '';
		var i_context = nlapiGetContext();
		var rev_rec_id = i_context.getSetting('SCRIPT', 'custscript_revenue_parent_recrd');
     // var rev_rec_id = parseInt(10);
      
		nlapiLogExecution('audit','project id:- ','revenue share id:- '+rev_rec_id);
		//var i_project_id = i_context.getSetting('SCRIPT', 'custscript_proj_id_to_send_mail');
		//var i_fp_others_plan_parent = i_context.getSetting('SCRIPT', 'custscript_revenue_share_id_to_process');
		//nlapiLogExecution('audit','project id:- '+i_project_id,'revenue share id:- '+i_fp_others_plan_parent);
		
		/*var a_revenue_cap_filter = [['custrecord_revenue_share_approval_status', 'anyof', 3], 'and',
									['custrecord_fp_rev_rec_others_month_end_e', 'is', 'F'], 'and',
									['internalid', 'anyof', parseInt(i_fp_others_plan_parent)]];*/
		if(rev_rec_id)
		{
			var a_revenue_cap_filter = [['internalid', 'anyof', rev_rec_id]];
					
		var a_column = new Array();
		a_column[0] = new nlobjSearchColumn('created').setSort(true);
		a_column[1] = new nlobjSearchColumn('custrecord_fp_rev_rec_pro_st_date');
		a_column[2] = new nlobjSearchColumn('custrecord_fp_rev_rec_proj_end_date');
		a_column[3] = new nlobjSearchColumn('custrecord_fp_rev_rec_others_projec');
		
		var a_get_revenue_cap = nlapiSearchRecord('customrecord_fp_rev_rec_others_parent', null, a_revenue_cap_filter, a_column);
		
		}
		else{
		var a_revenue_cap_filter = [['custrecord_fp_rev_rec_others_month_end_e', 'is', 'F']];
					
		var a_column = new Array();
		a_column[0] = new nlobjSearchColumn('created').setSort(true);
		a_column[1] = new nlobjSearchColumn('custrecord_fp_rev_rec_pro_st_date');
		a_column[2] = new nlobjSearchColumn('custrecord_fp_rev_rec_proj_end_date');
		a_column[3] = new nlobjSearchColumn('custrecord_fp_rev_rec_others_projec');
		
		var a_get_revenue_cap = nlapiSearchRecord('customrecord_fp_rev_rec_others_parent', null, a_revenue_cap_filter, a_column);
		}
		if (a_get_revenue_cap)
		{
			for(var i_index=0; i_index<a_get_revenue_cap.length; i_index++)
			{
				var monthBreakUp = getMonthsBreakup(
			        nlapiStringToDate(a_get_revenue_cap[i_index].getValue('custrecord_fp_rev_rec_pro_st_date')),
			        nlapiStringToDate(a_get_revenue_cap[i_index].getValue('custrecord_fp_rev_rec_proj_end_date')));
					
				i_fp_others_plan_parent = a_get_revenue_cap[i_index].getId();
				var i_revenue_share_project = a_get_revenue_cap[i_index].getValue('custrecord_fp_rev_rec_others_projec');
				
				var a_effort_plan_filter = [['custrecord_fp_rev_rec_others_parent_link.internalid', 'anyof', parseInt(i_fp_others_plan_parent)]];
			
				var a_columns_existing_effort_plan_srch = new Array();
				a_columns_existing_effort_plan_srch[0] = new nlobjSearchColumn('custrecord_fp_rev_rec_others_practice');
				a_columns_existing_effort_plan_srch[1] = new nlobjSearchColumn('custrecord_fp_rev_rec_others_sub_practic');
				a_columns_existing_effort_plan_srch[2] = new nlobjSearchColumn('custrecord_fp_rev_rec_others_role');
				a_columns_existing_effort_plan_srch[3] = new nlobjSearchColumn('custrecord_fp_rev_rec_others_level');
				a_columns_existing_effort_plan_srch[4] = new nlobjSearchColumn('custrecord_fp_rev_rec_others_no_of_resou');
				a_columns_existing_effort_plan_srch[5] = new nlobjSearchColumn('custrecord_fp_rev_rec_others_alloc_st_da');
				a_columns_existing_effort_plan_srch[6] = new nlobjSearchColumn('custrecord_fp_rev_rec_others_alloc_end_d');
				a_columns_existing_effort_plan_srch[7] = new nlobjSearchColumn('custrecord_fp_rev_rec_others_percent_all');
				a_columns_existing_effort_plan_srch[8] = new nlobjSearchColumn('custrecord_fp_rev_rec_others_location');
				a_columns_existing_effort_plan_srch[9] = new nlobjSearchColumn('custrecord_fp_rev_rec_others_cost');
					
				var a_get_exsiting_effort_plan = nlapiSearchRecord('customrecord_fp_rev_rec_others_eff_plan', null, a_effort_plan_filter, a_columns_existing_effort_plan_srch);
				if (a_get_exsiting_effort_plan)
				{
					create_record(i_fp_others_plan_parent,a_get_exsiting_effort_plan,monthBreakUp,i_revenue_share_project);
				}
				
				nlapiSubmitField('customrecord_fp_rev_rec_others_parent',parseInt(i_fp_others_plan_parent),'custrecord_fp_rev_rec_others_month_end_e','T');
			}
			
			/*var a_projectData = nlapiLookupField('job', i_project_id, [
				        'custentity_projectmanager', 'custentity_deliverymanager',
				        'customer', 'custentity_clientpartner', 'entityid', 'altname']);
						
			var s_recipient_mail_pm = nlapiLookupField('employee',a_projectData.custentity_projectmanager,'email');
			var s_recipient_mail_dm = nlapiLookupField('employee',a_projectData.custentity_deliverymanager,'email');
			var s_recipient_mail_cp = nlapiLookupField('employee',a_projectData.custentity_clientpartner,'email');
			var i_client_cp = nlapiLookupField('customer',a_projectData.customer,'custentity_clientpartner');
			var s_recipient_mail_acp = nlapiLookupField('employee',i_client_cp,'email');
			
			var a_recipient_mail = new Array();
			var s_project_name = '';
			s_project_name += a_projectData.entityid+' ';
			s_project_name += a_projectData.altname;
			
			if(s_recipient_mail_pm)
			{
				a_recipient_mail.push(s_recipient_mail_pm);
			}
			if(s_recipient_mail_dm)
			{
				a_recipient_mail.push(s_recipient_mail_dm);
			}
			if(s_recipient_mail_cp)
			{
				a_recipient_mail.push(s_recipient_mail_cp);
			}
			if(s_recipient_mail_acp)
			{
				a_recipient_mail.push(s_recipient_mail_acp);
			}
			
			var strVar = '';
			strVar += '<html>';
			strVar += '<body>';
			
			strVar += '<p>Hello All,</p>';
			strVar += '<p>Revenue Share for project :- '+s_project_name+', is approved by executing practice head.</p>';
			strVar += '<p>Please find below link to confirm Revenue share.</p>';
			
			strVar += '<a href=https://system.sandbox.netsuite.com/app/site/hosting/scriptlet.nl?script=1138&deploy=1>Confirm Revenue Share</a>';
			
			strVar += '<p>Thanks & Regards,</p>';
			strVar += '<p>Team IS</p>';
				
			strVar += '</body>';
			strVar += '</html>';
			
			var a_emp_attachment = new Array();
			a_emp_attachment['entity'] = '39108';
			
			nlapiSendEmail(442, a_recipient_mail, 'Revenue Share for Project :- '+s_project_name+' is Approved', strVar,null,null,a_emp_attachment);*/
			nlapiLogExecution('audit','mail sent for approval');
		}
	}
	catch(err)
	{
		nlapiLogExecution('ERROR','scheduled_create_effort_plan','ERROR MESSAGE :- '+err);
	}
}



function create_record(i_fp_others_plan_parent,a_get_exsiting_effort_plan,monthBreakUp,i_revenue_share_project)
{
	try
	{
		var o_mnth_end_parent = nlapiCreateRecord('customrecord_fp_rev_rec_others_mont_end');
		o_mnth_end_parent.setFieldValue('custrecord_fp_rev_rec_month_end_parent_l', i_fp_others_plan_parent);
		var i_mnth_end_parent_rcrd_id = nlapiSubmitRecord(o_mnth_end_parent, true, true);
		nlapiLogExecution('audit','mnth end rcrd id:- ',i_mnth_end_parent_rcrd_id);
				
		for (var i_effort_index = 0; i_effort_index < a_get_exsiting_effort_plan.length; i_effort_index++)
		{
			for (var j = 0; j < monthBreakUp.length; j++)
			{
				var months = monthBreakUp[j];
				var s_month_name = getMonthName(months.Start); // get month name
				var month_strt = nlapiStringToDate(months.Start);
				var month_end = nlapiStringToDate(months.End);
				var i_month = month_strt.getMonth();
				var i_year = month_strt.getFullYear();
				i_year = parseInt(i_year);
				
				var i_total_days_in_mnth = new Date(i_year, i_month + 1, 0).getDate();
				
				var parent_practice_month_end_effrt = a_get_exsiting_effort_plan[i_effort_index].getValue('custrecord_fp_rev_rec_others_practice');
				var sub_practice_month_end_effrt = a_get_exsiting_effort_plan[i_effort_index].getValue('custrecord_fp_rev_rec_others_sub_practic');
				var role_month_end_effrt = a_get_exsiting_effort_plan[i_effort_index].getValue('custrecord_fp_rev_rec_others_role');
				var level_month_end_effrt = a_get_exsiting_effort_plan[i_effort_index].getValue('custrecord_fp_rev_rec_others_level');
				var location_mnth_end_effrt = a_get_exsiting_effort_plan[i_effort_index].getValue('custrecord_fp_rev_rec_others_location');
				var i_no_of_resources = a_get_exsiting_effort_plan[i_effort_index].getValue('custrecord_fp_rev_rec_others_no_of_resou');
				var i_prcnt_allocated = a_get_exsiting_effort_plan[i_effort_index].getValue('custrecord_fp_rev_rec_others_percent_all');
				var f_cost_for_resource = a_get_exsiting_effort_plan[i_effort_index].getValue('custrecord_fp_rev_rec_others_cost');
				var s_allocation_strt_date = a_get_exsiting_effort_plan[i_effort_index].getValue('custrecord_fp_rev_rec_others_alloc_st_da');
				var s_allocation_end_date = a_get_exsiting_effort_plan[i_effort_index].getValue('custrecord_fp_rev_rec_others_alloc_end_d');
				
				i_prcnt_allocated = (parseFloat(i_prcnt_allocated) * parseFloat(i_no_of_resources)) / 100;
				
				var d_allocation_strt_date = nlapiStringToDate(s_allocation_strt_date);
				var d_allocation_end_date = nlapiStringToDate(s_allocation_end_date);
				
				var i_days_diff = 0;
				if (month_strt >= d_allocation_strt_date || month_end >= d_allocation_strt_date) {
					if (d_allocation_strt_date <= month_strt && d_allocation_end_date > month_strt) {
						d_allocation_strt_date = month_strt;
					}
					else 
						if (d_allocation_end_date < month_strt) 
							d_allocation_strt_date = '';
					
					if (d_allocation_end_date >= month_end && d_allocation_end_date >= month_strt) {
						d_allocation_end_date = month_end;
					}
					else 
						if (d_allocation_end_date < month_strt) {
							d_allocation_end_date = '';
						}
					
					if (d_allocation_strt_date) {
						if (d_allocation_end_date) {
							i_days_diff = getDatediffIndays(d_allocation_strt_date, d_allocation_end_date);
						}
					}
				}
				
				var f_prcnt_mnth_allo = parseFloat(i_days_diff) / parseFloat(i_total_days_in_mnth);
				f_prcnt_mnth_allo = parseFloat(f_prcnt_mnth_allo) * parseFloat(i_prcnt_allocated);
				f_prcnt_mnth_allo = parseFloat(f_prcnt_mnth_allo).toFixed(2);
				
				var o_actual_effrt_mnth_end = nlapiCreateRecord('customrecord_fp_rev_rec_othr_mnth_end_ef');
				o_actual_effrt_mnth_end.setFieldValue('custrecordfp_others_mnth_end_practice', parent_practice_month_end_effrt);
				o_actual_effrt_mnth_end.setFieldValue('custrecordfp_othr_month_end_sub_practice', sub_practice_month_end_effrt);
				o_actual_effrt_mnth_end.setFieldValue('custrecord_fp_month_end_role', role_month_end_effrt);
				o_actual_effrt_mnth_end.setFieldValue('custrecordfp_other_mnth_end_level', level_month_end_effrt);
				o_actual_effrt_mnth_end.setFieldValue('custrecord_fp_others__location_mnth_end', location_mnth_end_effrt);				
				o_actual_effrt_mnth_end.setFieldValue('custrecord_fp_others_mnth_end_year', s_month_name);
				o_actual_effrt_mnth_end.setFieldValue('custrecord_fp_others_mnth_end_year_y', i_year);
				o_actual_effrt_mnth_end.setFieldValue('custrecord_fp_others_mnth_end_st_date', months.Start);
				o_actual_effrt_mnth_end.setFieldValue('custrecord_fp_others_mnt_end_date', months.End);
				o_actual_effrt_mnth_end.setFieldValue('custrecord_fp_others_mnth_end_alloc_per', f_prcnt_mnth_allo);
				o_actual_effrt_mnth_end.setFieldValue('custrecord_fp_others__resource_cost', f_cost_for_resource);
				o_actual_effrt_mnth_end.setFieldValue('custrecord_fp_othr__mnth_end_project_pla', i_revenue_share_project);
				o_actual_effrt_mnth_end.setFieldValue('custrecord_fp_others_mnth_act_parent', i_mnth_end_parent_rcrd_id);
				var i_month_end_activity = nlapiSubmitRecord(o_actual_effrt_mnth_end);
				
			}
		}
	}
	catch(err)
	{
		nlapiLogExecution('ERROR','create_record','ERROR MESSAGE :- '+err);
	}
}

function getMonthsBreakup(d_startDate, d_endDate) {
	
	try
	{
		var dateBreakUp = [];
		var new_start_date = new Date(d_startDate.getFullYear(), d_startDate
		        .getMonth(), 1);
				
		var endDate = nlapiAddDays(nlapiAddMonths(d_startDate, 1), -1);
		endDate = nlapiDateToString(endDate, 'date');
	
		dateBreakUp.push({
		    Start : nlapiDateToString(new_start_date, 'date'),
		    End : endDate
		});
	
		for (var i = 1;; i++) {
			var new_date = nlapiAddMonths(new_start_date, i);
	
			if (new_date > d_endDate) {
				break;
			}
			
			endDate = nlapiAddDays(nlapiAddMonths(new_date, 1), -1);
			endDate = nlapiDateToString(endDate, 'date');
					
			dateBreakUp.push({
			    Start : nlapiDateToString(new_date, 'date'),
			    End : endDate
			});
		}
	}
	catch(err)
	{
		nlapiLogExecution('ERROR','getMonthsBreakup','ERROR MESSAGE :- '+err);
	}

	return dateBreakUp;
}

function getMonthName(currentDate) {
	currentDate = nlapiStringToDate(currentDate)
	var monthNames = [ "JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JUL", "AUG",
	        "SEP", "OCT", "NOV", "DEC" ];
	return monthNames[currentDate.getMonth()];
}

function getDatediffIndays(startDate, endDate)
{
    var one_day=1000*60*60*24;
    var fromDate = startDate;
    var toDate=endDate

	//var date1 = nlapiStringToDate(fromDate);

   //var date2 = nlapiStringToDate(toDate);
	
	var date1 = fromDate;
	var date2 = toDate;
	
    var date3=Math.round((date2-date1)/one_day);

    return (date3+1);
}

function MonthNames(yr) {
	var yr1 = Number(yr) + 1;
	
	var monthNames = [ "JAN_"+yr, "FEB_"+yr, "MAR_"+yr, "APR_"+yr, "MAY_"+yr, "JUN_"+yr, "JUL_"+yr, "AUG_"+yr,
	        "SEP_"+yr, "OCT_"+yr, "NOV_"+yr, "DEC_"+yr ];
	var month_nxt_yr = ["JAN_"+yr1, "FEB_"+yr1, "MAR_"+yr1, "APR_"+yr1, "MAY_"+yr1, "JUN_"+yr1, "JUL_"+yr1, "AUG_"+yr1,
	        "SEP_"+yr1, "OCT_"+yr1, "NOV_"+yr1, "DEC_"+yr1 ];
	monthNames = monthNames.concat(month_nxt_yr);
	
	return monthNames;
}
