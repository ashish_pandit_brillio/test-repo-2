// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
/*
   	Script Name : CLI_SET_AUTO_VENDOR_BILL_BLANK.js
	Author      : ASHISH PANDIT
	Date        : 20 NOV 2018
    Description : Client Script To set auto vendor bill field blank   


	Script Modification Log:

	-- Date --			-- Modified By --				--Requested By--				-- Description --
 


Below is a summary of the process controls enforced by this script file.  The control logic is described
more fully, below, in the appropriate function headers and code blocks.


     SCHEDULED FUNCTION
		- scheduledFunction(type)


     SUB-FUNCTIONS
		- The following sub-functions are called by the above core functions in order to maintain code
            modularization:

               - NOT USED

*/
}
// END SCRIPT DESCRIPTION BLOCK  ====================================


function pageInit_setvalue(type)
{
	if(type=='copy')
	{
		nlapiSetFieldValue('custbody_auto_invoice_bllc','');
	}
}
