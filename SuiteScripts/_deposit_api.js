/**
 * @NApiVersion 2.x
 * @NScriptType Restlet
 * @NModuleScope SameAccount
 */
 define(['N/record', 'N/search', 'N/runtime', 'N/format'],
 function (obj_Record, search, obj_Runtime, obj_Format) {
     function send_Deposit_data(obj_Request) {
         try {
             log.debug('requestBody ==', JSON.stringify(obj_Request));
             var obj_Usage_Context = obj_Runtime.getCurrentScript();
             var s_Request_Type = obj_Request.RequestType;
             log.debug('s_Request_Type ==', s_Request_Type);
             var s_Request_Data = obj_Request.RequestData;
             log.debug('s_Request_Data ==', s_Request_Data);
             var d_start_Date = obj_Request.start_date;
             d_start_Date = obj_Format.parse({ value: d_start_Date, type: obj_Format.Type.DATE });
             d_start_Date = obj_Format.format({ value: d_start_Date, type: obj_Format.Type.DATE });
             log.debug('d_start_Date ==', d_start_Date);
             var d_End_Date = obj_Request.end_date;
             d_End_Date = obj_Format.parse({ value: d_End_Date, type: obj_Format.Type.DATE });
             d_End_Date = obj_Format.format({ value: d_End_Date, type: obj_Format.Type.DATE });
             log.debug('d_End_Date ==', d_End_Date);
             if ((!s_Request_Type) || (s_Request_Type == null) || (s_Request_Data == null) || (!s_Request_Data) || (s_Request_Data != 'DEPOSIT')) {
                 var obj_Response_Return = new Response();
                 obj_Response_Return.Data = 'Please Enter Request Body';
                 obj_Response_Return.Status = false;

             } //if((!s_Request_Type) || (s_Request_Type == null) || (s_Request_Data == null) || (!s_Request_Data)|| (s_Request_Data!= 'INVOICE'))
             else {
                 var obj_Response_Return = new Response();
                 if ((!d_start_Date) || (!d_End_Date)) {
                     obj_Response_Return.Data = 'Please Enter Start Date and End date';
                     obj_Response_Return.Status = false;
                 } //// if((!d_start_Date)|| (!d_End_Date))
                 else {
                     obj_Response_Return.Status = true;
                     obj_Response_Return = fu_Send_Deposit_Data(search, d_start_Date, d_End_Date);
                 }// else of  if((!d_start_Date)|| (!d_End_Date))

             } //else of if if((!s_Request_Type) || (s_Request_Type == null) || (s_Request_Data == null) || (!s_Request_Data)|| (s_Request_Data!= 'INVOICE'))

         } //// end of try
         catch (s_Exception) {
             var obj_Response_Return = new Response();
             log.debug('s_Exception in send_Invoice_data ==', s_Exception);
             obj_Response_Return.Data = s_Exception;
             obj_Response_Return.Status = false;
         } //// End of Catch
         log.debug('obj_Response_Return==', JSON.stringify(obj_Response_Return));
         log.debug('Final Remaining Usage  ==', obj_Usage_Context.getRemainingUsage());
         return obj_Response_Return;
     } //// End of function send_Invoice_data
     return {
         post: send_Deposit_data,
     };

 }); /// End of main Function 
/************************ Supporting Functions ***************************************************/
function fu_Send_Deposit_Data(search, d_start_Date, d_End_Date) {
 try {
    var depositSearchObj = search.create({
        type: "deposit",
        filters:
        [
           ["type","anyof","Deposit"], 
           "AND", 
           ["posting","is","T"], 
           "AND", 
           ["fxamount","notequalto","0.00"], 
           "AND", 
           ["trandate","within",d_start_Date,d_End_Date],
            "AND",
            ["account.type","anyof","COGS","Expense","OthIncome","OthExpense","Income"]
        ],
        columns:
        [
           search.createColumn({
              name: "internalid",
              join: "account",
              label: "Account"
           }),
           search.createColumn({
              name: "formulatext",
              formula: "{type.id}",
              label: "Type"
           }),
           search.createColumn({name: "trandate", label: "Date"}),
           search.createColumn({name: "transactionnumber", label: "Transaction Number"}),
           search.createColumn({name: "tranid", label: "Document Number"}),
           search.createColumn({
              name: "formulatext",
              formula: "{mainname.id}",
              label: "Name"
           }),
           search.createColumn({name: "fxamount", label: "Amount"}),
           search.createColumn({name: "exchangerate", label: "Exchange Rate"}),
           search.createColumn({name: "currency", label: "Currency"}),
           search.createColumn({name: "custbody_billfrom", label: "Billing From"}),
           search.createColumn({name: "custbody_billto", label: "Billing To"}),
           search.createColumn({name: "custcolcustcol_temp_customer", label: "Customer"}),
           search.createColumn({name: "custcol_project_entity_id", label: "Project ID"}),
           search.createColumn({name: "custcol_proj_name_on_a_click_report", label: "Proj Name"}),
           search.createColumn({name: "custcol_employee_entity_id", label: "Employee ID"}),
           search.createColumn({name: "custcol_emp_name_on_a_click_report", label: "Emp Name"}),
           search.createColumn({name: "custcol_parent_executing_practice", label: "Parent Executing Practice"}),
           search.createColumn({
              name: "internalid",
              join: "department",
              label: "Practice"
           }),
           search.createColumn({
              name: "custrecord_parent_practice",
              join: "department",
              label: "Parent Practice"
           }),
           search.createColumn({
              name: "formulatext",
              formula: "{custcol_territory.id}",
              label: "Territory"
           }),
           search.createColumn({
              name: "internalid",
              join: "location",
              label: "Location"
           }),
           search.createColumn({name: "memomain", label: "Memo (Main)"}),
           search.createColumn({name: "memo", label: "Memo"}),
           search.createColumn({name: "custcol_memo_description", label: "Description"}),
           search.createColumn({
              name: "internalid",
              join: "item",
              label: "Item"
           }),
           search.createColumn({name: "custcol_units", label: "Units"}),
           search.createColumn({name: "custcol_employee_type", label: "Employee type"}),
           search.createColumn({name: "custcol_person_type", label: "Person type"}),
           search.createColumn({name: "custcol_onsite_offsite", label: "Onsite/Offsite"}),
           search.createColumn({name: "custcol_billing_type", label: "Billing type"}),
           search.createColumn({name: "custcol_proj_category_on_a_click", label: "Proj Category"}),
           search.createColumn({
              name: "internalid",
              join: "subsidiary",
              label: "Subsidiary"
           }),
           search.createColumn({name: "entity", label: "Name"}),
           search.createColumn({name: "type", label: "Type"}),
           search.createColumn({
              name: "internalid",
              join: "vendor",
              label: "Internal ID"
           }),
           search.createColumn({name: "custcol_project_region", label: "Project Region"}),
           search.createColumn({name: "postingperiod", label: "Period"})
        ]
     });
     var searchResultCount = depositSearchObj.runPaged().count;
     log.debug("depositSearchObj result count",searchResultCount);
     var myResults = getAllResults(depositSearchObj);
     var arr_Deposit_json = [];
     var amount;
     var exchangeRate;
     var finalAmount;
     myResults.forEach(function(result)  {
         amount = result.getValue({ name: "fxamount", label: "Amount" })
         exchangeRate = result.getValue({name: "exchangerate", label: "Exchange Rate"})
         finalAmount = (parseFloat(amount)*parseFloat(exchangeRate)).toFixed(2);
         var obj_json_Container = {}
         obj_json_Container = {
             "Account": result.getValue({ name: "internalid", join: "account", label: "Account" }),
             "Name": result.getValue({name: "internalid",join: "vendor"}),
             "Type" : result.getValue('type'),
             "Date": result.getValue({ name: "trandate", label: "Date" }),
             "Tran_Id": result.getValue({ name: "tranid", label: "Document Number" }),
             "Document_Number": result.getValue({name: "transactionnumber", label: "Transaction Number"}),
             "Base_Amount": finalAmount,
             "Currency": result.getValue({ name: "currency", label: "Currency" }),
             "Billing_From": result.getValue({ name: "custbody_billfrom", label: "Billing From" }),
             "Billing_To": result.getValue({ name: "custbody_billto", label: "Billing To" }),
             "Customer": result.getValue({ name: "custcolcustcol_temp_customer", label: "Customer" }),
             "Project_ID": result.getValue({ name: "custcol_project_entity_id", label: "Project ID" }),
             "Proj_Name": result.getValue({ name: "custcol_proj_name_on_a_click_report", label: "Proj Name" }),
             "Employee_ID": result.getValue({ name: "custcol_employee_entity_id", label: "Employee ID" }),
             "Emp_Name": result.getValue({ name: "custcol_emp_name_on_a_click_report", label: "Emp Name" }),
             "Parent_Executing_Practice": result.getValue({ name: "custcol_parent_executing_practice", label: "Parent Executing Practice" }),
             "Practice": result.getValue({ name: "internalid", join: "department", label: "Practice" }),
             "Parent_Practice": result.getValue({ name: "custrecord_parent_practice", join: "department", label: "Parent Practice" }),
             "Territory": result.getValue({ name: "formulatext", formula: "{custcol_territory.id}", label: "Territory" }),
             "Location": result.getValue({ name: "internalid", join: "location", label: "Location" }),
             "Memo_main": result.getValue({ name: "memomain", label: "Memo (Main)" }),
             "Memo": result.getValue({ name: "memo", label: "Memo" }),
             "Description": result.getValue({ name: "custcol_memo_description", label: "Description" }),
             "Item": result.getValue({ name: "internalid", join: "item", label: "Item" }),
             "Units": result.getValue({ name: "custcol_units", label: "Units" }),
             "Employee_type": result.getValue({ name: "custcol_employee_type", label: "Employee type" }),
             "Person_type": result.getValue({ name: "custcol_person_type", label: "Person type" }),
             "Onsite_Offsite": result.getValue({ name: "custcol_onsite_offsite", label: "Onsite/Offsite" }),
             "Billing_type": result.getValue({ name: "custcol_billing_type", label: "Billing type" }),
             "Proj_Category": result.getValue({ name: "custcol_proj_category_on_a_click", label: "Proj Category" }),
             "Subsidiary": result.getValue({ name: "internalid", join: "subsidiary", label: "Subsidiary" }),
             "Project_Region":result.getValue({name: "custcol_project_region"}),
             "Period":result.getText({name: "postingperiod"})
         };
         arr_Deposit_json.push(obj_json_Container);
         return true;
     });
     return arr_Deposit_json;
 }
 catch (s_Exception) {
     log.debug('Exception in fu_Send_Deposit_Data', s_Exception);
     return s_Exception;
 } //// End of catch
}///// function fu_Send_Deposit_Data(search)
/**************************************************************************************************/
function Response() {
 this.Status = false;
 this.Data = "";

} //// function Response() 
function getAllResults(s) {
 var result = s.run();
 var searchResults = [];
 var searchid = 0;
 do {
     var resultslice = result.getRange({ start: searchid, end: searchid + 1000 });
     resultslice.forEach(function (slice) {
         searchResults.push(slice);
         searchid++;
     }
     );
 } while (resultslice.length >= 1000);
 return searchResults;
}
/************************ Supporting Functions ***************************************************/