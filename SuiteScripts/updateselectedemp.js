/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       26 Sep 2019     Aazamali Khan	   update selected employee and joining date 	
 *
 */

/**
 * @param {String} type Context Types: scheduled, ondemand, userinterface, aborted, skipped
 * @returns {Void}
 */
function updateSelectedEmp(type) {
	var arrFilters = [
	                  ["custrecord_frf_details_status","is","F"], 
	                  "AND", 
	                  ["custrecord_frf_details_status_flag","anyof","1"], 
	                  "AND", 
	                  ["custrecord_frf_details_external_hire","is","T"], 
	                  "AND", 
	                  ["custrecord_frf_details_taleo_emp_select","anyof","@NONE@"], 
	                  "AND", 
	                  ["custrecord_frf_details_rrf_number.custrecord_offered_emp_email","isnotempty",""], 
	                  "AND", 
	                  ["custrecord_frf_details_rrf_number.custrecord_taleo_ext_hire_status","is","Filled"]
	               ];
	var arrColumns = [
	                  new nlobjSearchColumn("custrecord_frf_details_frf_number"), 
	                  new nlobjSearchColumn("custrecord_rrf_taleo"), 
	                  new nlobjSearchColumn("custrecord_frf_details_ns_rrf_number"), 
	                  new nlobjSearchColumn("custrecord_frf_details_status_flag"), 
	                  new nlobjSearchColumn("custrecord_frf_details_taleo_emp_select"), 
	                  new nlobjSearchColumn("custrecord_taleo_selected_employee","CUSTRECORD_FRF_DETAILS_RRF_NUMBER",null), 
	                  new nlobjSearchColumn("custrecord_taleo_external_selected_can","CUSTRECORD_FRF_DETAILS_RRF_NUMBER",null), 
	                  new nlobjSearchColumn("custrecord_offered_emp_email","CUSTRECORD_FRF_DETAILS_RRF_NUMBER",null), 
	                  new nlobjSearchColumn("custrecord_taleo_emp_joining_date","CUSTRECORD_FRF_DETAILS_RRF_NUMBER",null)
	               ];
	var searchResult = searchRecord("customrecord_frf_details", null, arrFilters, arrColumns);
	if (searchResult) {
		for (var i = 0; i < searchResult.length; i++) {
			var i_selectedEmp = "";
			var i_frfDetails = searchResult[i].getId();
			var s_selectedEmp = searchResult[i].getValue("custrecord_offered_emp_email","CUSTRECORD_FRF_DETAILS_RRF_NUMBER",null);
			var d_joiningDate = searchResult[i].getValue("custrecord_taleo_emp_joining_date","CUSTRECORD_FRF_DETAILS_RRF_NUMBER",null);
			var searchResultEmail = getEmployeeRec(s_selectedEmp);
			if(searchResultEmail){
              i_selectedEmp = searchResultEmail[0].getId();
            }
          	nlapiLogExecution("DEBUG", "s_selectedEmp : ", s_selectedEmp);
			var o_frfDetails = nlapiLoadRecord('customrecord_frf_details',i_frfDetails);
			if(i_selectedEmp)
			{
				o_frfDetails.setFieldValue('custrecord_frf_details_taleo_emp_select',i_selectedEmp);
				//Status closed code added by Ashish on 15-10-2019
				//o_frfDetails.setFieldValue('custrecord_frf_details_status_flag',2);
				//o_frfDetails.setFieldValue('custrecord_frf_details_open_close_status',2);
			}
			if(d_joiningDate)
				o_frfDetails.setFieldValue('custrecord_frf_details_emp_joining_date',d_joiningDate);
			var recObj = nlapiSubmitRecord(o_frfDetails);

		}
	}
}
function getEmployeeRec(personalEmail) {
	var employeeSearch = nlapiSearchRecord("employee", null,
		[
			["email", "is", personalEmail],"OR",["custentity_personalemailid", "is", personalEmail]
		],
		[]);
	return employeeSearch;
}