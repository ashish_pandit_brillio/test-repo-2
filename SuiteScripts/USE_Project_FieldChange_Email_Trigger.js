//BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
    /*
     Script Name: USE_Project_FieldChange_Email_Trigger.js
     Author:  	  Anukaran
     Company:     Inspirria Cloud tech
     Date:	      05-08-2020
     Description: This script will compare the value of fields with old record and new record fields. If the any field values are changed,Script will trigger a mail.
     
	 Script Modification Log:
     -- Date --			-- Modified By --				--Requested By--				-- Description --
    
	
     */
}

function projectFieldChange_afterSubmit(type) {
    try {
		if (type == 'create')
			{
				var id = nlapiGetRecordId();
				//nlapiLogExecution('DEBUG', 'Project_Log ', 'Record id =' + id);
				var rec_type = nlapiGetRecordType();
				//nlapiLogExecution('DEBUG', 'Project_Log ', 'Record type =' + rec_type);
				var newRec = nlapiLoadRecord(rec_type, id);
			
				var executing_practice = newRec.getFieldValue('custentity_practice');
				//nlapiLogExecution('DEBUG', 'Project_Log ', 'executing_practice =' + executing_practice);
				var region_txt = newRec.getFieldText('custentity_region');
				//nlapiLogExecution('DEBUG', 'Project_Log ', 'region_txt =' + region_txt);
				var customerName = newRec.getFieldValue('parent');
				var customerNameTxt = newRec.getFieldText('parent');
				//nlapiLogExecution('DEBUG', 'Project_Log ', 'customerName =' + customerNameTxt);
				var projectName = newRec.getFieldValue('companyname');
				//nlapiLogExecution('DEBUG', 'Project_Log ', 'projectName =' + projectName);
				var executing_practice_txt = newRec.getFieldText('custentity_practice');
				//nlapiLogExecution('DEBUG', 'Project_Log ', 'executing_practice_txt =' + executing_practice_txt);
				var project_Start_Date = newRec.getFieldValue('startdate');
				//nlapiLogExecution('DEBUG', 'Project_Log ', 'project_Start_Date =' + project_Start_Date);
				var project_End_Date = newRec.getFieldValue('enddate');
				//nlapiLogExecution('DEBUG', 'Project_Log ', 'project_End_Date =' + project_End_Date);
				var projectManager = newRec.getFieldValue('custentity_projectmanager');
				var projectManagerTxt = newRec.getFieldText('custentity_projectmanager');
				//nlapiLogExecution('DEBUG', 'Project_Log ', 'projectManagerTxt =' + projectManagerTxt);
				var billingTypeTxt = newRec.getFieldText('jobbillingtype');
				nlapiLogExecution('DEBUG', 'Project_Log ', 'billingTypeTxt =' + billingTypeTxt);

				
				if(executing_practice == 511 || executing_practice == 322)
				{
					var receiptant_3users = Array();
					//receiptant_3users[0] = 'Smitha.Thumbikkat@brillio.com';
					receiptant_3users[0] = 'archana.r@brillio.com';
					receiptant_3users[1] = 'senjuti.c1@brillio.com';
					//receiptant_3users[2] = 'anukaran@inspirria.com';

					//Table Creation

					var strVar = '';
					strVar += '<table width="100%" border="1">';
					strVar += '	<tr>';
					strVar += ' <td width="9%" font-size="11" align="center" bgcolor="#D3D3D3">Region</td>';
					strVar += ' <td width="9%" font-size="11" align="center" bgcolor="#D3D3D3">Customer Name</td>';
					strVar += ' <td width="9%" font-size="11" align="center" bgcolor="#D3D3D3">Project Name</td>';
					strVar += ' <td width="9%" font-size="11" align="center" bgcolor="#D3D3D3">Executing Practice</td>';
					strVar += ' <td width="9%" font-size="11" align="center" bgcolor="#D3D3D3">Project Start Date</td>';
					strVar += ' <td width="9%" font-size="11" align="center" bgcolor="#D3D3D3">Project End Date</td>';
					strVar += ' <td width="9%" font-size="11" align="center" bgcolor="#D3D3D3">PM</td>';
					strVar += ' <td width="9%" font-size="11" align="center" bgcolor="#D3D3D3">Billing Type</td>';
					strVar += '	</tr>';

					//Dymnamic Values
					strVar += '	<tr>';
					strVar += ' <td width="9%" font-size="11" align="center">' + region_txt + '</td>';
					strVar += ' <td width="9%" font-size="11" align="center">' + customerNameTxt + '</td>';
					strVar += ' <td width="9%" font-size="11" align="center">' + projectName + '</td>';
					strVar += ' <td width="9%" font-size="11" align="center">' + executing_practice_txt + '</td>';
					strVar += ' <td width="9%" font-size="11" align="center">' + project_Start_Date + '</td>';
					strVar += ' <td width="9%" font-size="11" align="center">' + project_End_Date + '</td>';
					strVar += ' <td width="9%" font-size="11" align="center">' + projectManagerTxt + '</td>';
					strVar += ' <td width="9%" font-size="11" align="center">' + billingTypeTxt + '</td>';
					strVar += '</tr>';
					strVar += '</table>';

					//Send Mail
					var strVar_ = '';
					strVar_ += '<html>';
					strVar_ += '<body>';
					strVar_ += '<p>Dear Archana/ Senjuti,</p>';
					//strVar_ += '<br/>';
					strVar_ += '<p>New Project is created for Practice Brillio Analytics</p>';
					strVar_ += '<br/>';
					strVar_ += strVar;
					strVar_ += '<br/>';
					strVar_ += '<p>Thanks & Regards,</p>';
					strVar_ += '<p>Information Systems Team</p>';
					strVar_ += '</body>';
					strVar_ += '</html>';

					nlapiSendEmail(442, receiptant_3users, 'Project Creation For Brillio Analytics Notification', strVar_, null, null, null);
					nlapiLogExecution('DEBUG', 'Creation Project Email Sent ', ' Process Completed Email Sent=>' + receiptant_3users)
				} //End of IF condition
				//rec_sub = nlapiSubmitRecord(newRec);
			} //End of if Type(Create)
		if (type == 'edit')
		{
            var id = nlapiGetRecordId();
            //nlapiLogExecution('DEBUG', 'Project_Log ', 'Record id =' + id);
            var rec_type = nlapiGetRecordType();
            //nlapiLogExecution('DEBUG', 'Project_Log ', 'Record type =' + rec_type);
            var newRec = nlapiLoadRecord(rec_type, id);
			
			var executing_practice = newRec.getFieldValue('custentity_practice');
            //nlapiLogExecution('DEBUG', 'Project_Log ', 'executing_practice =' + executing_practice);
			if(executing_practice == 511 || executing_practice == 322)
			{
				var region_txt = newRec.getFieldText('custentity_region');
				//nlapiLogExecution('DEBUG', 'Project_Log ', 'region_txt =' + region_txt);
				var customerName = newRec.getFieldValue('parent');
				var customerNameTxt = newRec.getFieldText('parent');
				//nlapiLogExecution('DEBUG', 'Project_Log ', 'customerName =' + customerNameTxt);
				var projectName = newRec.getFieldValue('companyname');
				//nlapiLogExecution('DEBUG', 'Project_Log ', 'projectName =' + projectName);
				var executing_practice_txt = newRec.getFieldText('custentity_practice');
				//nlapiLogExecution('DEBUG', 'Project_Log ', 'executing_practice_txt =' + executing_practice_txt);
				var project_Start_Date = newRec.getFieldValue('startdate');
				//nlapiLogExecution('DEBUG', 'Project_Log ', 'project_Start_Date =' + project_Start_Date);
				var project_End_Date = newRec.getFieldValue('enddate');
				//nlapiLogExecution('DEBUG', 'Project_Log ', 'project_End_Date =' + project_End_Date);
				var projectManager = newRec.getFieldValue('custentity_projectmanager');
				var projectManagerTxt = newRec.getFieldText('custentity_projectmanager');
				//nlapiLogExecution('DEBUG', 'Project_Log ', 'projectManagerTxt =' + projectManagerTxt);
				var billingTypeTxt = newRec.getFieldText('jobbillingtype');
				//nlapiLogExecution('DEBUG', 'Project_Log ', 'billingTypeTxt =' + billingTypeTxt);

				//old Recored Values
				var recObj = nlapiGetOldRecord();
				
				var projectManager_old = recObj.getFieldValue('custentity_projectmanager');
				var projectManager_oldTxt = recObj.getFieldText('custentity_projectmanager');
				//nlapiLogExecution('DEBUG', 'Project_Log ', 'projectManager_oldTxt =' + projectManager_oldTxt);
				
				if ( projectManager != projectManager_old)
				{
					var receiptant_3users = Array();
					//receiptant_3users[0] = 'Smitha.Thumbikkat@brillio.com';
					receiptant_3users[0] = 'archana.r@brillio.com';
					receiptant_3users[1] = 'senjuti.c1@brillio.com';
					//receiptant_3users[2] = 'anukaran@inspirria.com';

					//Table Creation

					var strVar = '';
					strVar += '<table width="100%" border="1">';
					strVar += '	<tr>';
					strVar += ' <td width="9%" font-size="11" align="center" bgcolor="#D3D3D3">Region</td>';
					strVar += ' <td width="9%" font-size="11" align="center" bgcolor="#D3D3D3">Customer Name</td>';
					strVar += ' <td width="9%" font-size="11" align="center" bgcolor="#D3D3D3">Project Name</td>';
					strVar += ' <td width="9%" font-size="11" align="center" bgcolor="#D3D3D3">Executing Practice</td>';
					strVar += ' <td width="9%" font-size="11" align="center" bgcolor="#D3D3D3">Project Start Date</td>';
					strVar += ' <td width="9%" font-size="11" align="center" bgcolor="#D3D3D3">Project End Date</td>';
					strVar += ' <td width="9%" font-size="11" align="center" bgcolor="#D3D3D3">PM</td>';
					strVar += ' <td width="9%" font-size="11" align="center" bgcolor="#D3D3D3">New PM</td>';
					strVar += ' <td width="9%" font-size="11" align="center" bgcolor="#D3D3D3">Billing Type</td>';
					strVar += '	</tr>';

					//Dymnamic Values
					strVar += '	<tr>';
					strVar += ' <td width="9%" font-size="11" align="center">' + region_txt + '</td>';
					strVar += ' <td width="9%" font-size="11" align="center">' + customerNameTxt + '</td>';
					strVar += ' <td width="9%" font-size="11" align="center">' + projectName + '</td>';
					strVar += ' <td width="9%" font-size="11" align="center">' + executing_practice_txt + '</td>';
					strVar += ' <td width="9%" font-size="11" align="center">' + project_Start_Date + '</td>';
					strVar += ' <td width="9%" font-size="11" align="center">' + project_End_Date + '</td>';
					strVar += ' <td width="9%" font-size="11" align="center">' + projectManager_oldTxt + '</td>';
					if(projectManager == projectManager_old)
					{
						strVar += ' <td width="9%" font-size="11" align="center">' + '' + '</td>';
					}else{
						strVar += ' <td width="9%" font-size="11" align="center">' + projectManagerTxt + '</td>';
					}
					
					strVar += ' <td width="9%" font-size="11" align="center">' + billingTypeTxt + '</td>';
					strVar += '</tr>';
					strVar += '</table>';

					//Send Mail
					var strVar_ = '';
					strVar_ += '<html>';
					strVar_ += '<body>';
					strVar_ += '<p>Dear Archana/ Senjuti,</p>';
					//strVar_ += '<br/>';
					strVar_ += '<p>Here is the details of fields those has changed on Project Record.</p>';
					strVar_ += '<br/>';
					strVar_ += strVar;
					strVar_ += '<br/>';
					strVar_ += '<p>Thanks & Regards,</p>';
					strVar_ += '<p>Information Systems Team</p>';
					strVar_ += '</body>';
					strVar_ += '</html>';

					nlapiSendEmail(442, receiptant_3users, 'Project Field Change Notification', strVar_, null, null, null);
					nlapiLogExecution('DEBUG', 'Email Edit Record Sent ', ' Process Completed Email Sent=>' + receiptant_3users)
				} //End of IF condition
				//rec_sub = nlapiSubmitRecord(newRec);
			}//End of Validation
            
        } //End of if Type
    } //End of Try
    catch (e) {
        nlapiLogExecution('DEBUG', 'Exception results ', ' value of e-====== **************** =' + e.toString())
    } //End of Catch

} //End of Function