/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       24 Mar 2015     amol.sahijwani
 *
 */

/**
 * @param {String}
 *            type Context Types: scheduled, ondemand, userinterface, aborted,
 *            skipped
 * @returns {Void}
 */
function scheduled(type) {
	var d_today = new Date();

	for (var i = -3; i < 12; i++) {
		var d_day = nlapiAddMonths(d_today, i);

		var d_month_start = nlapiAddDays(d_day, -1 * (d_day.getDate() - 1));
		var s_month_start = d_month_start.getMonth() + 1 + '/'
				+ d_month_start.getDate() + '/' + d_month_start.getFullYear();
		var d_month_end = nlapiAddDays(nlapiAddMonths(d_month_start, 1), -1);
		var s_month_end = d_month_end.getMonth() + 1 + '/'
				+ d_month_end.getDate() + '/' + d_month_end.getFullYear();
		try {
			
			var url = getRESTletURL() + '&mode=xml&custpage_from_date='
			+ s_month_start
			+ '&custpage_to_date='
			+ s_month_end
			+ '&custpage_department=&custpage_project_filter=&custpage_vertical_filter=&custpage_customer_filter=&custpage_show_ot_hours=null';
			
			var cred = new credentials();
			
			var headers = new Array();
			headers['User-Agent-x'] = 'SuiteScript-Call';
			headers['Authorization'] = 'NLAuth nlauth_account='+cred.account+', nlauth_email='+cred.email+', nlauth_signature='+cred.password+', nlauth_role='+cred.role;
			headers['Content-Type'] = 'application/json';
			
			var response = nlapiRequestURL( url, null, headers );
		 
		    nlapiLogExecution('AUDIT', 'url', response.getBody());
		    
		} catch (e) {
			nlapiLogExecution('ERROR', 'Error: ', e.message);
		}
		
	}
	
	// Update Log
	
	var today	=	new Date();
    var rec	=	nlapiCreateRecord('customrecord_rev_rep_data_update_log');
    rec.setFieldValue('custrecord_rev_rep_data_update_date', (today.getMonth() + 1) + '/' + today.getDate() + '/' + today.getFullYear());
    rec.setFieldValue('custrecord_rev_rep_data_upd_log_status', 'Done');
    nlapiSubmitRecord(rec);
}

function getAccount () { return '3883006'; }

function getRESTletURL()
{
	return 'https://rest.netsuite.com/app/site/hosting/restlet.nl?script=510&deploy=1&c='+getAccount(); // for phasing
}

function credentials()
{
    this.email='developer.brillio@brillio.com';
    this.account=getAccount();
    this.role='3';
    this.password='Amol123456';
}