/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       04 May 2019     Aazam Ali			
 *
 */
/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType
 * 
 * @param {String} type Operation types: create, edit, delete, xedit,
 *                      approve, cancel, reject (SO, ER, Time Bill, PO & RMA only)
 *                      pack, ship (IF only)
 *                      dropship, specialorder, orderitems (PO only) 
 *                      paybills (vendor payments)
 * @returns {Void}
 */

 function userEventAfterSubmit(type) {
    if (type == 'delete') {
        return true;
    }
    var payLoad = {};
    var headers = {
        "Content-Type": "application/json",
        "x-access-token": getTokens()
    };
    nlapiLogExecution("DEBUG", "Tokens : ", getTokens());
    var method = "POST";
    var recObj = nlapiLoadRecord(nlapiGetRecordType(), nlapiGetRecordId());
    var oldRec = nlapiGetOldRecord();
    var oldfrfStatus;
    var externalHireOld = "F";
    if (oldRec) {
        oldfrfStatus = oldRec.getFieldValue("custrecord_frf_details_status");
        externalHireOld = oldRec.getFieldValue("custrecord_frf_details_external_hire");
    }
    var statusField = recObj.getFieldValue("custrecord_frf_details_talep_created");
    var frfStatus = recObj.getFieldValue("custrecord_frf_details_status");
    var externalHire = recObj.getFieldValue("custrecord_frf_details_external_hire");
    var frfNumber = recObj.getFieldValue("custrecord_frf_details_ns_rrf_number");
    var taleoRRFNumber = recObj.getFieldValue("custrecord_rrf_taleo");
    var cancelReason = recObj.getFieldText("custrecord_frf_details_cancellation_reas");
    var taleoExternalHireId = recObj.getFieldValue("custrecord_frf_details_rrf_number");
    var rrfStatus = "";
    if (taleoExternalHireId)
        rrfStatus = nlapiLookupField("customrecord_taleo_external_hire", taleoExternalHireId, "custrecord_taleo_ext_hire_status");
    //nlapiLogExecution("ERROR","statusField : ",statusField);
    try {
        if (statusField == "F") {
            if (type == "create" || type == "edit") {
                var url = "https://netsuite-taleo-transformer.azurewebsites.net/createRRF";
                if (externalHire == "T" && !(_logValidation(taleoRRFNumber))) {
                    var frfLocation = recObj.getFieldText("custrecord_frf_details_res_location");
                    var frfCountry = (frfLocation.split(":"))[0];
                    var frfCity = (frfLocation.split(":"))[2];
                    nlapiLogExecution("DEBUG", "country", frfCountry);
                    nlapiLogExecution("DEBUG", "city", frfCity);
                    var TaleoConfigSearch = getBudgetCurrencyCode(frfCountry.trim());
                    nlapiLogExecution("DEBUG", "TaleoConfigSearch", JSON.stringify(TaleoConfigSearch));
                    var budgetCurrencyCode = "";
                    var cswWorkflow = "";
                    var group = "";
                    var organizationCode = "";
                    var approver = "";
                    var s_legal_employer = "";
                    if (TaleoConfigSearch) {
                        budgetCurrencyCode = TaleoConfigSearch[0].getValue("custrecord_taleo_config_currency");
                        cswWorkflow = TaleoConfigSearch[0].getValue("custrecord_taleo_config_csw_workflow");
                        group = TaleoConfigSearch[0].getValue("custrecord_taleo_cofig_group");
                        organizationCode = TaleoConfigSearch[0].getValue("custrecord_taleo_config_organisation");
                        s_legal_employer = TaleoConfigSearch[0].getValue("custrecord_legal_employer");
                    }
                    var department = recObj.getFieldText("custrecord_frf_details_res_practice");
					
					var departmentId = recObj.getFieldValue("custrecord_frf_details_res_practice");
					
					var taleo_approval_flow_record = getTaleoApprovalFlowRRF(departmentId);
					
                    if (frfCountry.trim() == "India") {
						
						if(_logValidation(taleo_approval_flow_record)){
							
							approver = taleo_approval_flow_record.indiaApprover || "";
							
						}
						
                        
                    } else {
						
						if(_logValidation(taleo_approval_flow_record)){
							
							approver = taleo_approval_flow_record.restApprover || "";
							
						}
                        
                    }
                    var i_practice = recObj.getFieldValue("custrecord_frf_details_res_practice");
                    var createdBy = recObj.getFieldValue("custrecord_frf_details_created_by");
                    var s_createdBy = recObj.getFieldText("custrecord_frf_details_created_by");
                    var createdByEmail = "";
                    if (createdBy) {
                        createdByEmail = nlapiLookupField("employee", createdBy, "email");
                    }
                    var jobTitle = recObj.getFieldValue("custrecord_frf_details_job_title");
                    var requestType = recObj.getFieldText("custrecord_frf_type");


                    var fitmentType = recObj.getFieldText("custrecord_frf_details_fitment_type"); // prabhat gupta NIS-1723 04/09/2020

                    var level = recObj.getFieldText("custrecord_frf_details_level"); // prabhat gupta NIS-1755 07/10/2020


                    var s_postitionType = recObj.getFieldText("custrecord_frf_details_position_type");
                    var s_projectName = recObj.getFieldText("custrecord_frf_details_project");
                    var s_projectDate = recObj.getFieldValue("custrecord_frf_details_start_date");
                    s_projectDate = moment(s_projectDate, "MM/DD/YYYY").format("YYYY-MM-DDTHH:MM:SS");
                    var desiredDate = recObj.getFieldValue("custrecord_frf_desired_start_date");
                    if (_logValidation(desiredDate))
                        desiredDate = moment(desiredDate, "MM/DD/YYYY").format("YYYY-MM-DDTHH:MM:SS");
                    var s_projectEndDate = recObj.getFieldValue("custrecord_frf_details_end_date");
                    s_projectEndDate = moment(s_projectEndDate, "MM/DD/YYYY").format("YYYY-MM-DDTHH:MM:SS");
                    var role = recObj.getFieldText("custrecord_frf_details_role");
                    var studyLevelCode = recObj.getFieldValue("custrecord_frf_details_edu_lvl");
                    if (studyLevelCode) {
                        studyLevelCode = config["educationLevel"][studyLevelCode]
                    } else {
                        studyLevelCode = "NONE";
                    }
                    var employeeStatus = recObj.getFieldText("custrecord_frf_details_emp_status");
                    var customer = recObj.getFieldText("custrecord_frf_details_account");
                    var priority = recObj.getFieldValue("custrecord_frf_details_critical_role");
                    var instructions = recObj.getFieldValue("custrecord_frf_details_instruction_team");
                    var eductionProgram = recObj.getFieldText("custrecord_frf_details_edu_program");
                    var hireTypeCode = recObj.getFieldValue("custrecord_frf_details_hire_type");
					payLoad.contestNumber = frfNumber; //rrfNumber;
                    payLoad.programSearchValue = eductionProgram; //rrfNumber;
                    payLoad.budgetCurrencyCode = budgetCurrencyCode;
                    payLoad.cswWorkflowSearchValue = cswWorkflow;
					if (hireTypeCode == 8) {	
                        payLoad.hireTypeCode = "9";	
                    }	
                    else {	
                        payLoad.hireTypeCode = hireTypeCode;	
                    }
                    payLoad.departmentCode = taleo_approval_flow_record.departmentCode || "";
                    payLoad.employeeStatus = config["employeeStatus"][employeeStatus]; //"2"; // Hardcoded "Hourly"
                    //payLoad.fusionGradesCode = "7"; // Hardcoded
                    payLoad.hiringManagerLoginname = createdByEmail;
                    payLoad.jobFieldSearchValue = "Specialist"; // Hardcoded
                    payLoad.jobInformationGroupSearchValue = group;
                    payLoad.jobTitle = jobTitle;
                    payLoad.requestTypeCode = config["requestType"][requestType]; //as per sridhar's comment removing

                    payLoad.fitmentTypeCode = config["fitmentType"][fitmentType]; // prabhat gupta NIS-1723 04/09/2020

                    payLoad.fusionGradesCode = config["level"][level]; // prabhat gupta NIS-1755 07/10/2020

                    payLoad.positionType = config["positionType"][s_postitionType];
                    nlapiLogExecution("AUDIT", "frfCity.trim() : ", frfCity);
                    var locationSearch = getPrimaryLocation(frfCity.trim(), frfCountry.trim());
                    nlapiLogExecution("AUDIT", "locationSearch : ", JSON.stringify(locationSearch));
                    if (locationSearch) {
                        payLoad.primaryLocationCode = locationSearch[0].getValue("custrecord_taleo_location_code");
                    }
                    payLoad.projectName = s_projectName;
                    payLoad.projectStartDate = s_projectDate;
                    payLoad.desiredStartDate = desiredDate;
                    var recuriterSearch = getFUELRecruiter(i_practice, frfCountry.trim());
                    if (recuriterSearch) {
                        var recruiterId = recuriterSearch[0].getValue("custrecord_fuel_recruiter_emp");
                    }
                    nlapiLogExecution("DEBUG", "recruiterId", recruiterId);
                    if (recruiterId) {
                        nlapiLogExecution("DEBUG", "recruiter email :", nlapiLookupField("employee", recruiterId, "email"));
                        payLoad.recruiterLoginName = nlapiLookupField("employee", recruiterId, "email");
                    }
                    //createdByEmail; Write a logic to get the value from practices vs recruiter
                    payLoad.resourceEndDate = s_projectEndDate;
                    payLoad.roles = role;
                    //Added the default value for function code, need to replace later once we have corporate into FUEL - 21st JAN 2020
                    payLoad.employeeFunctionCode = '2';
                    payLoad.studyLevelCode = studyLevelCode;
                    payLoad.organizationCode = organizationCode; //"BUCOD-0000000138265";
                    //added for legal employer, 21st JAN 2020
                    payLoad.fusionLegalEmployersCode = s_legal_employer;
                    payLoad.globalCustomerName = customer;
                    if (recObj.getFieldValue("custrecord_frf_details_opp_id")) {
                        var opp_id = nlapiLookupField('customrecord_sfdc_opportunity_record', parseInt(recObj.getFieldValue("custrecord_frf_details_opp_id")), 'custrecord_opportunity_id_sfdc');
                        payLoad.opportunityId = opp_id;
                    }
                    if (recObj.getFieldText("custrecord_frf_details_project")) {
                        payLoad.projectName = recObj.getFieldText("custrecord_frf_details_project");
                    }
                    var billiable = recObj.getFieldValue("custrecord_frf_details_billiable");
                    payLoad.billingCategorySearchValue = config["billingCategory"][billiable];
                    var s_secondarySkills = recObj.getFieldText("custrecord_frf_details_secondary_skills");
                    if (s_secondarySkills) {
                        s_secondarySkills = s_secondarySkills.replace(/\u0005/g, ",");
                        nlapiLogExecution("AUDIT", "s_secondarySkills", s_secondarySkills);
                    }
                    payLoad.secondarySkills = s_secondarySkills; //recObj.getFieldText("custrecord_frf_details_secondary_skills");
                    payLoad.allocation = recObj.getFieldValue("custrecord_frf_details_allocation");
                    var s_primary_skills = recObj.getFieldText("custrecord_frf_details_primary_skills");
                    if (s_primary_skills) {
                        s_primary_skills = s_primary_skills.replace(/\u0005/g, ",");
                        nlapiLogExecution("AUDIT", "s_primary_skills", s_primary_skills);
                    }
                    payLoad.primarySkill = s_primary_skills; //recObj.getFieldText("custrecord_frf_details_primary_skills");
                    //payLoad.primarySkill 	= recObj.getFieldValue("custrecord_frf_details_dollar_loss");
                    payLoad.clientBillRate = recObj.getFieldValue("custrecord_frf_details_bill_rate");
                    payLoad.customerInterviewer = recObj.getFieldValue("custrecord_frf_details_cust_inter_email");
                    payLoad.clientRequirementNumber = recObj.getFieldValue("custrecord_frf_details_cli_req_number");
                    payLoad.externalDescriptionHTML = recObj.getFieldValue("custrecord_frf_details_job_descriptions");
                    payLoad.internalDescriptionHTML = recObj.getFieldValue("custrecord_frf_details_job_descriptions");
                    payLoad.firstLevelInterviewer = recObj.getFieldValue("custrecord_frf_details_first_interviewer");
                    payLoad.secondLevelInterviewer = recObj.getFieldValue("custrecord_frf_details_sec_interviewer");
                    payLoad.secondLevelInterviewer = recObj.getFieldValue("custrecord_frf_details_sec_interviewer");
                    if (billiable == "F") {
                        payLoad.approversList = [{
                            "approverMailId": approver
                        }]
                        /*, {
                        							"approverMailId": config["nonBilliableApprover"][department]  // prabhat gupta 08/06/2020
                        						}]*/
                    }
                    if (billiable == "T") {
                        payLoad.approversList = [{
                            "approverMailId": approver
                        }]
                    }
                    payLoad.approvalComment = "Auto redirected to practice approver";
                    //payLoad.customerInterviewRequiredCode = recObj.getFieldValue("customerInterviewRequiredCode") ? "Yes" : "No";
                    if (priority == "T") {
                        payLoad.priorityCode = "1";
                    } else {
                        payLoad.priorityCode = "4";
                    }
                    var customerInterviewReq = recObj.getFieldValue("custrecord_frf_details_cust_interview");
                    if (customerInterviewReq == "T") {
                        payLoad.customerInterviewRequiredCode = "1";
                    } else {
                        payLoad.customerInterviewRequiredCode = "2";
                    }
                    payLoad.instructionsToHiringManager = instructions;
                    var backupReq = recObj.getFieldValue("custrecord_frf_details_backup_require");
                    if (backupReq == "T") {
                        payLoad.requisitionCode = "1";
                    } else {
                        payLoad.requisitionCode = "2";
                    }
                    var JSONBody = JSON.stringify(payLoad, replacer);
                    nlapiLogExecution("DEBUG", "JSONBody", JSONBody);
                    var responseObj = nlapiRequestURL(url, JSONBody, headers, null, method);
                    nlapiLogExecution("DEBUG", "Response :", responseObj.getCode());
                    nlapiSubmitField(nlapiGetRecordType(), nlapiGetRecordId(), "custrecord_frf_details_taleo_int_error", responseObj.body);
                    if (parseInt(responseObj.getCode()) == 200) {
                        nlapiSubmitField(nlapiGetRecordType(), nlapiGetRecordId(), "custrecord_frf_details_talep_created", "T");
                    } else {
                        // send Email to fuel
                        var file = nlapiCreateFile("payload.json", "PLAINTEXT", JSONBody);
                        var emailSub = "Error in RRF Creation #" + frfNumber;
                        var emailBoody = "Team," + "\r\n" + "Error while creating rrf to Taleo #" + frfNumber + "\r\n" + responseObj.body + "\r\n" + "Please find attached payLoad." + "\r\n";
                        nlapiSendEmail(154256, "team.fuel@brillio.com", emailSub, emailBoody, null, null, null, file);
                    }
                }
            }
        } else {
            if (type == "edit" && frfStatus == "F" && rrfStatus != "Filled") {
                var url = "https://netsuite-taleo-transformer.azurewebsites.net/updateRRF"; // update url 
                var recObj = nlapiLoadRecord(nlapiGetRecordType(), nlapiGetRecordId());
                if (externalHire == "T" && !(_logValidation(taleoRRFNumber))) {
                    var s_secondarySkills = recObj.getFieldText("custrecord_frf_details_secondary_skills");
                    if (s_secondarySkills) {
                        s_secondarySkills = s_secondarySkills.replace(/\u0005/g, ","); //.replace(/[^a-zA-Z ]/g, ",");
                        nlapiLogExecution("AUDIT", "s_secondarySkills", s_secondarySkills);
                    }
                    var s_primary_skills = recObj.getFieldText("custrecord_frf_details_primary_skills");
                    if (s_primary_skills) {
                        s_primary_skills = s_primary_skills.replace(/\u0005/g, ","); //.replace(/[^a-zA-Z ]/g, ",");
                        nlapiLogExecution("AUDIT", "s_primary_skills", s_primary_skills);
                    }
                    var frfLocation = recObj.getFieldText("custrecord_frf_details_res_location");
                    var frfCountry = (frfLocation.split(":"))[0];
                    var frfCity = (frfLocation.split(":"))[2];
                    nlapiLogExecution("DEBUG", "country", frfCountry);
                    nlapiLogExecution("DEBUG", "city", frfCity);
                    var TaleoConfigSearch = getBudgetCurrencyCode(frfCountry.trim());
                    nlapiLogExecution("DEBUG", "TaleoConfigSearch", JSON.stringify(TaleoConfigSearch));
                    var budgetCurrencyCode = "";
                    var cswWorkflow = "";
                    var group = "";
                    var organizationCode = "";
                    var approver = "";
                    var s_legal_employer = "";
                    if (TaleoConfigSearch) {
                        budgetCurrencyCode = TaleoConfigSearch[0].getValue("custrecord_taleo_config_currency");
                        cswWorkflow = TaleoConfigSearch[0].getValue("custrecord_taleo_config_csw_workflow");
                        group = TaleoConfigSearch[0].getValue("custrecord_taleo_cofig_group");
                        organizationCode = TaleoConfigSearch[0].getValue("custrecord_taleo_config_organisation");
                        s_legal_employer = TaleoConfigSearch[0].getValue("custrecord_legal_employer");
                    }
                    var department = recObj.getFieldText("custrecord_frf_details_res_practice");
					
					var departmentId = recObj.getFieldValue("custrecord_frf_details_res_practice");
					
					var taleo_approval_flow_record = getTaleoApprovalFlowRRF(departmentId);
					
                    if (frfCountry.trim() == "India") {
						
						if(_logValidation(taleo_approval_flow_record)){
							
							//approver = taleo_approval_flow_record.indiaApprover || "";
							
						}
						
                        
                    } else {
						
						if(_logValidation(taleo_approval_flow_record)){
							
							//approver = taleo_approval_flow_record.restApprover || "";
							
						}
                        
                    }
					
                    if (frfCountry.trim() == "India") {
                        //approver = config["IndiaApprover"][department]
                    } else {
                        //approver = config["restOfWorldApprover"][department]
                    }
                    var i_practice = recObj.getFieldValue("custrecord_frf_details_res_practice");
                    var createdBy = recObj.getFieldValue("custrecord_frf_details_created_by");
                    var s_createdBy = recObj.getFieldText("custrecord_frf_details_created_by");
                    var createdByEmail = "";
                    if (createdBy) {
                        createdByEmail = nlapiLookupField("employee", createdBy, "email");
                    }
                    var jobTitle = recObj.getFieldValue("custrecord_frf_details_job_title");
                    var requestType = recObj.getFieldText("custrecord_frf_type");

                    var fitmentType = recObj.getFieldText("custrecord_frf_details_fitment_type"); // prabhat gupta NIS-1723 04/09/2020

                    var level = recObj.getFieldText("custrecord_frf_details_level"); // prabhat gupta NIS-1755 07/10/2020

                    var s_postitionType = recObj.getFieldText("custrecord_frf_details_position_type");
                    var s_projectName = recObj.getFieldText("custrecord_frf_details_project");
                    var s_projectDate = recObj.getFieldValue("custrecord_frf_details_start_date");
                    s_projectDate = moment(s_projectDate, "MM/DD/YYYY").format("YYYY-MM-DDTHH:MM:SS");
                    var desiredDate = recObj.getFieldValue("custrecord_frf_desired_start_date");
                    if (_logValidation(desiredDate))
                        desiredDate = moment(desiredDate, "MM/DD/YYYY").format("YYYY-MM-DDTHH:MM:SS");
                    var s_projectEndDate = recObj.getFieldValue("custrecord_frf_details_end_date");
                    s_projectEndDate = moment(s_projectEndDate, "MM/DD/YYYY").format("YYYY-MM-DDTHH:MM:SS");
                    var role = recObj.getFieldText("custrecord_frf_details_role");
                    var studyLevelCode = recObj.getFieldValue("custrecord_frf_details_edu_lvl");
                    if (studyLevelCode) {
                        studyLevelCode = config["educationLevel"][studyLevelCode]
                    } else {
                        studyLevelCode = "NONE";
                    }
                    var employeeStatus = recObj.getFieldText("custrecord_frf_details_emp_status");
                    var customer = recObj.getFieldText("custrecord_frf_details_account");
                    var priority = recObj.getFieldValue("custrecord_frf_details_critical_role");
                    var instructions = recObj.getFieldValue("custrecord_frf_details_instruction_team");
                    var eductionProgram = recObj.getFieldText("custrecord_frf_details_edu_program");
					var hireTypeCode = recObj.getFieldValue("custrecord_frf_details_hire_type");
					payLoad.contestNumber = frfNumber; //rrfNumber;
                    payLoad.programSearchValue = eductionProgram; //rrfNumber;
                    payLoad.budgetCurrencyCode = budgetCurrencyCode;
                    payLoad.cswWorkflowSearchValue = cswWorkflow;
					
					if (hireTypeCode == 8) {	
                        payLoad.hireTypeCode = "9";	
                    }	
                    else {	
                        payLoad.hireTypeCode = hireTypeCode;	
                    }
					
                    payLoad.departmentCode = taleo_approval_flow_record.departmentCode || "";
                    payLoad.employeeStatus = config["employeeStatus"][employeeStatus]; // Hardcoded "Hourly"
                    //payLoad.fusionGradesCode = "7"; // Hardcoded
                    payLoad.hiringManagerLoginname = createdByEmail;
                    payLoad.jobFieldSearchValue = "Specialist"; // Hardcoded
                    payLoad.jobInformationGroupSearchValue = group;
                    //payLoad.jobTitle = jobTitle;
                    payLoad.requestTypeCode = config["requestType"][requestType];

                    payLoad.fitmentTypeCode = config["fitmentType"][fitmentType]; // prabhat gupta 04/09/2020

                    payLoad.fusionGradesCode = config["level"][level]; // prabhat gupta 04/09/2020

                    payLoad.positionType = config["positionType"][s_postitionType];
                    var locationSearch = getPrimaryLocation(frfCity.trim(), frfCountry.trim());
                    if (locationSearch) {
                        payLoad.primaryLocationCode = locationSearch[0].getValue("custrecord_taleo_location_code");
                    }
                    payLoad.projectName = s_projectName;
                    payLoad.projectStartDate = s_projectDate;
                    payLoad.desiredStartDate = desiredDate;
                    var recuriterSearch = getFUELRecruiter(i_practice, frfCountry.trim());
                    if (recuriterSearch) {
                        var recruiterId = recuriterSearch[0].getValue("custrecord_fuel_recruiter_emp");
                    }
                    if (recruiterId) {
                        nlapiLogExecution("DEBUG", "recruiter email :", nlapiLookupField("employee", recruiterId, "email"));
                        //payLoad.recruiterLoginName = nlapiLookupField("employee", recruiterId, "email");
                    }
                    //createdByEmail; Write a logic to get the value from practices vs recruiter
                    payLoad.resourceEndDate = s_projectEndDate;
                    payLoad.roles = role;
                    payLoad.studyLevelCode = studyLevelCode;
                    payLoad.organizationCode = organizationCode; //"BUCOD-0000000138265";
                    payLoad.globalCustomerName = customer;
                    //added for legal employer, 21st JAN 2020
                    payLoad.fusionLegalEmployersCode = s_legal_employer;
                    if (recObj.getFieldValue("custrecord_frf_details_opp_id")) {
                        var opp_id = nlapiLookupField('customrecord_sfdc_opportunity_record', parseInt(recObj.getFieldValue("custrecord_frf_details_opp_id")), 'custrecord_opportunity_id_sfdc');
                        payLoad.opportunityId = opp_id;
                    }
                    if (recObj.getFieldText("custrecord_frf_details_project")) {
                        payLoad.projectName = recObj.getFieldText("custrecord_frf_details_project");
                    }
                    var billiable = recObj.getFieldValue("custrecord_frf_details_billiable");
                    payLoad.billingCategorySearchValue = config["billingCategory"][billiable];
                    payLoad.secondarySkills = s_secondarySkills; //recObj.getFieldText("custrecord_frf_details_secondary_skills");
                    payLoad.allocation = recObj.getFieldValue("custrecord_frf_details_allocation");
                    payLoad.primarySkill = s_primary_skills; //recObj.getFieldText("custrecord_frf_details_primary_skills");
                    //payLoad.primarySkill = recObj.getFieldValue("custrecord_frf_details_dollar_loss");
                    payLoad.clientBillRate = recObj.getFieldValue("custrecord_frf_details_bill_rate");
                    payLoad.customerInterviewer = recObj.getFieldValue("custrecord_frf_details_cust_inter_email");
                    payLoad.clientRequirementNumber = recObj.getFieldValue("custrecord_frf_details_cli_req_number");
                    //payLoad.externalDescriptionHTML = recObj.getFieldValue("custrecord_frf_details_job_descriptions");
                    //payLoad.internalDescriptionHTML = recObj.getFieldValue("custrecord_frf_details_job_descriptions");
                    payLoad.firstLevelInterviewer = recObj.getFieldValue("custrecord_frf_details_first_interviewer");
                    payLoad.secondLevelInterviewer = recObj.getFieldValue("custrecord_frf_details_sec_interviewer");
                    payLoad.employeeFunctionCode = '2';
                    //payLoad.customerInterviewRequiredCode = recObj.getFieldValue("customerInterviewRequiredCode") ? "Yes" : "No";
                    /*if (billiable == "F") {
							//payLoad.approversList = [{
									"approverMailId": approver
								},{
									"approverMailId": config["nonBilliableApprover"][department]
								}]
							}
							if(billiable == "T"){
                     // payLoad.approversList = [{
								"approverMailId": approver
							}]
                    }*/
                    //payLoad.approvalComment = "Auto redirected to practice approver";
                    if (priority == "T") {
                        payLoad.priorityCode = "1";
                    } else {
                        payLoad.priorityCode = "4";
                    }
                    var customerInterviewReq = recObj.getFieldValue("custrecord_frf_details_cust_interview");
                    if (customerInterviewReq == "T") {
                        payLoad.customerInterviewRequiredCode = "1";
                    } else {
                        payLoad.customerInterviewRequiredCode = "2";
                    }
                    payLoad.instructionsToHiringManager = instructions;
                    var backupReq = recObj.getFieldValue("custrecord_frf_details_backup_require");
                    if (backupReq == "T") {
                        payLoad.requisitionCode = "1";
                    } else {
                        payLoad.requisitionCode = "2";
                    }
                    var JSONBody = JSON.stringify(payLoad, replacer);
                    nlapiLogExecution("ERROR", "Edit JSONBody", JSONBody);
                    var responseObj = nlapiRequestURL(url, JSONBody, headers, null, method);
                    //nlapiLogExecution("DEBUG", "Response :", responseObj.body);
                    nlapiSubmitField(nlapiGetRecordType(), nlapiGetRecordId(), "custrecord_frf_details_taleo_int_error", responseObj.body);
                    if (!(parseInt(responseObj.getCode()) == 200)) {
                        // send Email to fuel
                        // send Email to fuel
                        var rrf_no = recObj.getFieldValue("custrecord_frf_details_rrf_number"); // prabhat gupta 28/07/2020 NIS-1430

                        if (!rrf_no) { // prabhat gupta 28/07/2020 NIS-1430 check to validate if rrf is already created don't trigger a failure mail

                            var file = nlapiCreateFile("payload.json", "PLAINTEXT", JSONBody);
                            var emailSub = "Error in RRF Creation #" + frfNumber;
                            var emailBoody = "Team," + "\r\n" + "Error while creating rrf to Taleo #" + frfNumber + "\r\n" + responseObj.body + "\r\n" + "Please find attached payLoad." + "\r\n";
                            nlapiSendEmail(154256, "team.fuel@brillio.com", emailSub, emailBoody, null, null, null, file);
                        }
                    }
                }
            }
        }
        if (type == "edit" && frfStatus == "T" && externalHire == "T" && (oldfrfStatus != frfStatus)) {
            var url = "https://netsuite-taleo-transformer.azurewebsites.net/cancelRRF"
            if (_logValidation(taleoRRFNumber)) {
                payLoad.contestNumber = taleoRRFNumber;
            } else {
                payLoad.contestNumber = frfNumber;
            }
            if (cancelReason) {
                payLoad.comment = cancelReason;
            } else {
                payLoad.comment = "Cancelled";
            }
            var JSONBody = JSON.stringify(payLoad, replacer);
            var responseObj = nlapiRequestURL(url, JSONBody, headers, null, method);
            nlapiSubmitField(nlapiGetRecordType(), nlapiGetRecordId(), "custrecord_frf_details_taleo_int_error", responseObj.body);
        }
        if (type == "edit" && externalHireOld == "T" && externalHire == "F") {
            var url = "https://netsuite-taleo-transformer.azurewebsites.net/cancelRRF"
            //payLoad.contestNumber = frfNumber;
            if (_logValidation(taleoRRFNumber)) {
                payLoad.contestNumber = taleoRRFNumber;
            } else {
                payLoad.contestNumber = frfNumber;
            }
            if (cancelReason) {
                payLoad.comment = cancelReason;
            } else {
                payLoad.comment = "Cancelled";
            }
            var JSONBody = JSON.stringify(payLoad, replacer);
            var responseObj = nlapiRequestURL(url, JSONBody, headers, null, method);
            nlapiSubmitField(nlapiGetRecordType(), nlapiGetRecordId(), "custrecord_frf_details_taleo_int_error", responseObj.body);
            // notification for cancel rrf to created by  
            var reciverId = recObj.getFieldValue("custrecord_frf_details_created_by");
            var emailSub = "RRF #" + frfNumber + "cancelled";
            var emailBoody = "Hi " + nlapiLookupField("employee", reciverId, "firstname") + "\r\n" + responseObj.body;
            nlapiSendEmail(154256, reciverId, emailSub, emailBoody, null, null, null, null);
        }
    } catch (e) {
        nlapiLogExecution("DEBUG", "Error in script : ", e);
    }
}

function getBudgetCurrencyCode(locCountry) {
    nlapiLogExecution("DEBUG", "locCountry : 431 ", locCountry);
    var customrecord_taleo_config_fuelSearch = nlapiSearchRecord("customrecord_taleo_config_fuel", null,
        [
            ["custrecord_taleo_config_location", "is", locCountry]
        ],
        [
            new nlobjSearchColumn("scriptid").setSort(false),
            new nlobjSearchColumn("custrecord_taleo_config_location"),
            new nlobjSearchColumn("custrecord_taleo_cofig_group"),
            new nlobjSearchColumn("custrecord_taleo_config_csw_workflow"),
            new nlobjSearchColumn("custrecord_taleo_config_organisation"),
            new nlobjSearchColumn("custrecord_taleo_config_currency"),
            new nlobjSearchColumn("custrecord_legal_employer")
        ]);
    return customrecord_taleo_config_fuelSearch;
}

function getPrimaryLocation(city, country) {
    var customrecord_taleo_location_masterSearch = nlapiSearchRecord("customrecord_taleo_location_master", null,
        [
            ["custrecord_taleo_location_city", "is", city], "AND", ["custrecord_taleo_location_country", "is", country]
        ],
        [
            new nlobjSearchColumn("name").setSort(false),
            new nlobjSearchColumn("scriptid"),
            new nlobjSearchColumn("custrecord_taleo_location_country"),
            new nlobjSearchColumn("custrecord_taleo_location_city"),
            new nlobjSearchColumn("custrecord_taleo_location_state"),
            new nlobjSearchColumn("custrecord_taleo_location_code")
        ]);
    return customrecord_taleo_location_masterSearch;
}

function getFUELRecruiter(i_practice, i_location) {
    if (i_location == "India") {
        nlapiLogExecution("DEBUG", "i_location", i_location);
        i_location = 8; // India Location => NIS-1800 prabhat gupta 28/10/2020
    }else if(i_location == "US" || i_location == "United States" ) {
        i_location = 9; // US Location => NIS-1800 prabhat gupta 28/10/2020
    }else if(i_location == "UK" || i_location == "United Kingdom" ) {
        i_location = 2;  // United Kingdom Location => NIS-1800 prabhat gupta 28/10/2020
    }else if (i_location == "Australia") {
        i_location = 63;  // Australia Location added on 17-08-2021
    }
  else {// for other location => NIS-1800 prabhat gupta 28/10/2020
		var location_search = nlapiSearchRecord("location", null,
        [
            ["name", "is", i_location],
			"AND",
			["isinactive", "is", "F"]
        ],
        [
            new nlobjSearchColumn("internalid").setSort(true)
		]);
		
		var loc = location_search[0].getId();
		if(_logValidation(loc)){
			i_location = loc;
		}
		
	}
    var customrecord_fuel_recruiterSearch = nlapiSearchRecord("customrecord_fuel_recruiter", null,
        [
            ["custrecord_fuel_recruiter_practice", "anyof", i_practice], "AND",
            ["custrecord_fuel_recruiter_location", "anyof", i_location]
        ],
        [
            new nlobjSearchColumn("internalid").setSort(true),
            new nlobjSearchColumn("custrecord_fuel_recruiter_practice"),
            new nlobjSearchColumn("custrecord_fuel_recruiter_emp"),
            new nlobjSearchColumn("custrecord_fuel_recruiter_location")
        ]);
    return customrecord_fuel_recruiterSearch;
}

function getTaleoApprovalFlowRRF(departmentId){
	
	var taleo_approval_search = nlapiSearchRecord("customrecord_taleo_approval_flow_fuel", null,
        [
            ["custrecord_taleo_approval_practice", "is", departmentId],
			"AND",
			["isinactive", "is", "F"]
        ],
        [
            new nlobjSearchColumn("internalid").setSort(true),
			new nlobjSearchColumn("custrecord_taleo_approval_practice"), 
		   new nlobjSearchColumn("custrecord_parent_practice","CUSTRECORD_TALEO_APPROVAL_PRACTICE",null), 
		   new nlobjSearchColumn("custrecord_taleo_approval_departmentcode"),
		   new nlobjSearchColumn("email","CUSTRECORD_TALEO_APPROVAL_APPROVERINDIA",null), 
		   new nlobjSearchColumn("email","CUSTRECORD_TALEO_APPROVAL_APPROVERREST",null)
		  
		]);
		
		var data = {};
		
		if(_logValidation(taleo_approval_search)){
			
		data.practice = taleo_approval_search[0].getValue("custrecord_taleo_approval_practice");
		data.parentPractice = taleo_approval_search[0].getValue("custrecord_parent_practice","CUSTRECORD_TALEO_APPROVAL_PRACTICE",null);
		data.departmentCode = taleo_approval_search[0].getValue("custrecord_taleo_approval_departmentcode");
		data.indiaApprover = taleo_approval_search[0].getValue("email","CUSTRECORD_TALEO_APPROVAL_APPROVERINDIA",null);
		data.restApprover = taleo_approval_search[0].getValue("email","CUSTRECORD_TALEO_APPROVAL_APPROVERREST",null);
		
		
		}
		
		nlapiLogExecution("DEBUG", "Taleo approval flow fuel-rrf", JSON.stringify(data));
	
    
    return data;
	
	
	
}