/**
 * @author Shweta
 */
// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
/*
   	Script Name : SUT_Provision_Unbilled_Journal_Entries.js
	Author      : Shweta Chopde
	Date        : 12 May 2014
	Description : List down Unbilled Expense / Time form


	Script Modification Log:

	-- Date --			-- Modified By --				--Requested By--				-- Description --
    31-10-2014           swati kurariya                   nitin                          1.role wise project show
    

Below is a summary of the process controls enforced by this script file.  The control logic is described
more fully, below, in the appropriate function headers and code blocks.


     SUITELET
		- suiteletFunction(request, response)


     SUB-FUNCTIONS
		- The following sub-functions are called by the above core functions in order to maintain code
            modularization:

               - NOT USED

*/
}
// END SCRIPT DESCRIPTION BLOCK  ====================================



// BEGIN GLOBAL VARIABLE BLOCK  =====================================
{
	//  Initialize any Global Variables, in particular, debugging variables...




}
// END GLOBAL VARIABLE BLOCK  =======================================





// BEGIN SUITELET ==================================================

function suiteletFunction(request, response)
{						
	var i_internal_ID;
	var i_date;
	var i_employee;
	var i_project;
	var i_date;
	var i_hours;	
	var i_rate;	
	var i_projectID;
	var i_currency;	
	var i_employee;
	var i_currency;
	var i_amount;
	var i_date;
	var i_internal_ID;
	var i_cnt = 0;
	var i_ent = 0;
	var i_practice;
	var i_subsidiary;
	var i_project_name;
	var i_location;
	var i_employee_duplicate='';
	var i_employee_duplicate1='';
	var i_employee_duplicate2='';
	var i_customer;
	var rowCount=0;
	var sa1=0;
	var sa2=0;
	var sa3=0;
	var i_count=0;
	var test;
	try
	{
			var i_percent_complete = '';
		var s_script_status = '';
		var d_date_created = '';
		 nlapiLogExecution('DEBUG', 'suiteletFunction','request.getMethod() -->' + request.getMethod());	//		
		if (request.getMethod() == 'GET') 
		{
			 var i_context = nlapiGetContext();
			 
			 var i_usage_begin = i_context.getRemainingUsage();
			 nlapiLogExecution('DEBUG', 'suiteletFunction',' Usage Begin -->' + i_usage_begin);	//
						 			
			 var i_current_date =  request.getParameter('custscript_current_date');
			 //-----------------------------------------------------------------------
			 var i_from_date =  request.getParameter('custscript_from_date');
			 var i_to_date =  request.getParameter('custscript_to_date');
			
		
			 //nlapiLogExecution('DEBUG','suiteletFunction','i_count -->'+i_count);
			// nlapiLogExecution('DEBUG', 'suiteletFunction',' Current Date -->' + i_current_date);	
			 //nlapiLogExecution('DEBUG', 'suiteletFunction',' From Date -->' + i_from_date);	
			 //nlapiLogExecution('DEBUG', 'suiteletFunction',' To Date -->' + i_to_date);	 
			 //-----------------------------------------------------------------------
			 var i_unbilled_type =   request.getParameter('custscript_unbilled_type');
			 var i_unbilled_receivable_GL =  request.getParameter('custscript_unbilled_receivable_gl');
			 var i_unbilled_revenue_GL =  request.getParameter('custscript_unbilled_revenue_gl');
			 var i_reverse_date =  request.getParameter('custscript_reverse_date');
			 var i_criteria =  request.getParameter('custscript_provision_creation_criteria');
			 var i_user_role =  request.getParameter('custscript_user_role');
			 var i_user_subsidiary =  request.getParameter('custscript_user_subsidiary');
			 var d_created_script_date =  request.getParameter('custscript_date_created');
			 var i_month =  request.getParameter('custscript_year');
			 var i_year =  request.getParameter('custscript_month');
			 
			  var rowCountURL = request.getParameter('param_rowCount');
			  nlapiLogExecution('DEBUG', 'suiteletFunction',' rowCountURL -->' + rowCountURL);
			  
			  // var rowCountURL = request.getParameter('custpage_customer');
			  //nlapiLogExecution('DEBUG', 'suiteletFunction',' rowCountURL -->' + rowCountURL);
				
					
			 nlapiLogExecution('DEBUG', 'suiteletFunction',' User Role -->' + i_user_role);
			 nlapiLogExecution('DEBUG', 'suiteletFunction',' User Subsidiary -->' + i_user_subsidiary);				
			 nlapiLogExecution('DEBUG', 'suiteletFunction',' Criteria -->' + i_criteria);
			 
			 nlapiLogExecution('DEBUG', 'suiteletFunction',' Unbilled Type -->' + i_unbilled_type);
			 nlapiLogExecution('DEBUG', 'suiteletFunction',' Unbilled Receivable GL -->' + i_unbilled_receivable_GL);
			 nlapiLogExecution('DEBUG', 'suiteletFunction',' Unbilled Revenue GL -->' + i_unbilled_revenue_GL);
			 nlapiLogExecution('DEBUG', 'suiteletFunction',' Reverse Date -->' + i_reverse_date);	
			 nlapiLogExecution('DEBUG', 'suiteletFunction',' Created Script Date -->' + d_created_script_date);
			 nlapiLogExecution('DEBUG', 'suiteletFunction',' i_month -->' + i_month);	
			 nlapiLogExecution('DEBUG', 'suiteletFunction',' i_year -->' + i_year);	
			
			
			var i_current_user =  nlapiGetUser();		
			var i_current_role =  nlapiGetRole();
			
			nlapiLogExecution('DEBUG', 'suiteletFunction',' Current User -->' + i_current_user);	
			//nlapiLogExecution('DEBUG', 'suiteletFunction',' Current Role -->' + i_current_role);	
			
			
			
			 if(i_criteria == 3)
			 {
			 
			 }
			 else{
				    var i_data_s  = search_custom_recordID(i_current_user,i_criteria)
					nlapiLogExecution('DEBUG', 'suiteletFunction',' Data S -->' + i_data_s);
					i_data_s = i_data_s.toString()	
					nlapiLogExecution('DEBUG', 'suiteletFunction',' Data S Length-->' + i_data_s.length);	
					 var a_TR_array_values = new Array()	
					 var i_data_TR = new Array()
					 i_data_TR =  i_data_s;
						
		      for(var dt=0;dt<i_data_TR.length;dt++)
			  {
				 	a_TR_array_values = i_data_TR.split(',')
				
				    break;				
			  }	
			 }
		
		   nlapiLogExecution('DEBUG', 'schedulerFunction','TR Array Values-->' +a_TR_array_values);
		  // nlapiLogExecution('DEBUG', 'schedulerFunction',' TR Array Values Length-->' + a_TR_array_values.length);
			
		// --------------------- SCRIPT EXECUTION STATUS ---------------------
	    if(_logValidation(d_created_script_date))
		{									
		var column = new Array();
		var filters = new Array();
		filters.push(new nlobjSearchFilter('status', null, 'anyof', ['PROCESSING', 'PENDING','COMPLETE']));
				
		filters.push(new nlobjSearchFilter('internalid','script','is','236'));//250 sand box id//236 production
		
		column.push(new nlobjSearchColumn('name', 'script'));
		column.push(new nlobjSearchColumn('internalid', 'scriptdeployment'));
		column.push(new nlobjSearchColumn('datecreated'));
		column.push(new nlobjSearchColumn('status'));
		column.push(new nlobjSearchColumn('startdate'));
		column.push(new nlobjSearchColumn('enddate'));
		column.push(new nlobjSearchColumn('queue'));
		column.push(new nlobjSearchColumn('percentcomplete'));
		column.push(new nlobjSearchColumn('queueposition'));
		column.push(new nlobjSearchColumn('percentcomplete'));

		var a_search_results =  searchRecord('scheduledscriptinstance', null, filters, column);
	    
		if(_logValidation(a_search_results)) 
	    {
	    	nlapiLogExecution('DEBUG','suiteletFunction','Search Results Length -->'+a_search_results.length);
	    	
			for(var i=0;i<a_search_results.length;i++)
	    	{
	    		var s_script  = a_search_results[i].getValue('name', 'script');
				//nlapiLogExecution('DEBUG','suiteletFunction','Script -->'+s_script);

				d_date_created  = a_search_results[i].getValue('datecreated');
				//nlapiLogExecution('DEBUG','suiteletFunction','Date Created -->'+d_date_created);
       
	           	var i_percent_complete  = a_search_results[i].getValue('percentcomplete');
				//nlapiLogExecution('DEBUG','suiteletFunction','Percent Complete -->'+i_percent_complete);

				s_script_status  = a_search_results[i].getValue('status');
				nlapiLogExecution('DEBUG','suiteletFunction','Script Status -->'+s_script_status);
			
			    i_percent_complete = i_percent_complete;              
			
	    	}
	    }
		
		}
		    if (d_created_script_date == '' || d_created_script_date == null || d_created_script_date == undefined)
			{
				 d_date_created =  get_todays_date()		
			}
		 
		
			var f_form = nlapiCreateForm(" UNBILLED EXPENSE / TIME LIST");
			f_form.setScript('customscript_provision_time_expense_v')
			//----------------------------------------------------------------------------------
			//-------------customer field-----------------------------------------------------------------------//added by swati 31 oct 2014
			var customer_field = f_form.addField('custpage_customer','select', 'Customer');
			 var search = searchRecord('customer', null, [new nlobjSearchFilter('subsidiary', null, 'is',i_user_subsidiary)], [new nlobjSearchColumn('altname')]);
			 customer_field.addSelectOption('','');
              for (result in search){
                     customer_field.addSelectOption(search[result].id, search[result].getValue('altname'));
              }
			   var i_customer =  request.getParameter('custpage_customer');
			  // nlapiLogExecution('DEBUG','i_customer','i_customer-->'+i_customer);
			  customer_field.setDefaultValue(i_customer);
			//--------------------------------------------------------------------------------------------------
			var customer_field_ref = f_form.addField('custpage_customer_sec','select', 'Customer','customer').setDisplayType('hidden');
						  customer_field_ref.setDefaultValue(i_customer);
						  
			 var i_customer_ref =  request.getParameter('custpage_customer_sec');	
             if((i_customer == i_customer_ref) || i_customer_ref == '' || i_customer_ref == null)
			{			 
				i_count =  request.getParameter('custscript_count');
			}
			
			
			//--------------------------------------------------------------------------------
		
			 
			 
			 if(i_percent_complete =='' || i_percent_complete == null || i_percent_complete == undefined)
			 {
			 	s_script_status = 'Not Started'
				i_percent_complete = '0%'
			 }
			 
			 
			//if(i_percent_complete!='' && i_percent_complete!=null && i_percent_complete!=undefined)
			{				 
			var s_status_f = f_form.addField('custpage_status_script', 'text', 'Status').setDisplayType('inline');
			s_status_f.setDefaultValue(s_script_status)
			
			var s_d_date_created_f = f_form.addField('custpage_d_date_created', 'text', 'Date Created').setDisplayType('inline');
			s_d_date_created_f.setDefaultValue(d_date_created)
			
						
			var i_percent_complete_f = f_form.addField('custpage_percent_complete', 'text', 'Percent Complete').setDisplayType('inline');
			i_percent_complete_f.setDefaultValue(i_percent_complete)		 
							
			} 
			
			var i_unbilled_type_f = f_form.addField('custpage_unbilled_type', 'text', 'Unbilled Type').setDisplayType('hidden');
			i_unbilled_type_f.setDefaultValue(i_unbilled_type)
			
           //-----------------------------------------------------------------------------------------------------------------------------			
			var i_current_date_f =  f_form.addField('custpage_current_date', 'text', 'Current Date').setDisplayType('hidden');
			i_current_date_f.setDefaultValue(i_current_date)
			
			var i_from_date_f =  f_form.addField('custpage_from_date', 'text', 'From Date').setDisplayType('hidden');
			i_from_date_f.setDefaultValue(i_from_date)
			
			var i_to_date_f =  f_form.addField('custpage_to_date', 'text', 'To Date').setDisplayType('hidden');
			i_to_date_f.setDefaultValue(i_to_date);
			
			var d_month_f = f_form.addField('custpage_d_month', 'text', 'Month').setDisplayType('hidden');
			d_month_f.setDefaultValue(i_month);
			
			var d_year_f = f_form.addField('custpage_d_year', 'text', 'Year').setDisplayType('hidden');
			d_year_f.setDefaultValue(i_year);
			
			//---------------------------------------------------------------------------------------------------------------------------						
			var i_unbilled_receivable_GL_f = f_form.addField('custpage_unbilled_receivable_gl', 'text', 'Unbilled Rec GL').setDisplayType('hidden');
			i_unbilled_receivable_GL_f.setDefaultValue(i_unbilled_receivable_GL)  
			
			var i_unbilled_revenue_GL_f = f_form.addField('custpage_unbilled_revenue_gl', 'text', 'Unbilled ReEv GL').setDisplayType('hidden');
			i_unbilled_revenue_GL_f.setDefaultValue(i_unbilled_revenue_GL)
			
			var i_reverse_date_f =  f_form.addField('custpage_reverse_date', 'text', 'Reverse Date').setDisplayType('hidden');
			i_reverse_date_f.setDefaultValue(i_reverse_date)
					
			var i_data_process_f = f_form.addField('custpage_data_process', 'textarea', 'Data Processed').setDisplayType('hidden');
				
			var i_custom_rec_id = f_form.addField('custpage_custom_id', 'text', 'Custom ID').setDisplayType('hidden');
				
			var i_data_sel_criteria = f_form.addField('custpage_data_criteria', 'longtext', 'Data Cr').setDisplayType('hidden');			
												
			var i_data = f_form.addField('custpage_data','longtext', 'Data').setDisplayType('hidden');			
			
			var i_criteria_fld = f_form.addField('custpage_criteria_f','text', 'Criteria').setDisplayType('hidden');			
			i_criteria_fld.setDefaultValue(i_criteria);
			
			var i_time_expense_tab = f_form.addSubList('custpage_time_expense_sublist', 'list', 'Time / Expense Entries', 'custpage_time_expense_tab');
			
            var i_select = i_time_expense_tab.addField('custpage_select', 'checkbox', 'Select');	
			
			var i_S_No= i_time_expense_tab.addField('custpage_s_no', 'text', 'Sr. No');	
			
			var i_user_role_f= f_form.addField('custpage_user_role', 'text', 'Role').setDisplayType('hidden');				
			i_user_role_f.setDefaultValue(i_user_role)
			
			var i_user_subsidiary_f = f_form.addField('custpage_user_subsidiary', 'textarea', 'Subsidiary').setDisplayType('hidden');				
			i_user_subsidiary_f.setDefaultValue(i_user_subsidiary)	

		
			//-----------------------------------------------------------------------------			
			// ============================ UNBILLED EXPENSE ==================================
			nlapiLogExecution('DEBUG','i_unbilled_type','i_unbilled_type -->'+i_unbilled_type);
			
			if(i_unbilled_type == 1)
			{	
			    var i_internalID = i_time_expense_tab.addField('custpage_internal_id', 'select', 'ID','transaction').setDisplayType('inline');			
				var i_employee = i_time_expense_tab.addField('custpage_employee', 'select', 'Employee','employee').setDisplayType('inline');	
				var i_project_code = i_time_expense_tab.addField('custpage_project', 'select', 'Project Code','job').setDisplayType('inline');	
				var i_vertical = i_time_expense_tab.addField('custpage_vertical', 'select', 'Vertical','classification').setDisplayType('inline');	
				var i_practice = i_time_expense_tab.addField('custpage_practice', 'select', 'Practice','department').setDisplayType('inline');	
				var i_date = i_time_expense_tab.addField('custpage_date', 'date', 'Date');	
				var i_amount = i_time_expense_tab.addField('custpage_amount', 'text', 'Amount');	
				var i_currency = i_time_expense_tab.addField('custpage_currency', 'select', 'Currency','currency').setDisplayType('inline');	
				//var i_exchange_rate = i_time_expense_tab.addField('custpage_exchange_rate', 'text', 'Exchange Rate').setDisplayType('inline');	
				var i_subsidiary_f = i_time_expense_tab.addField('custpage_subsidiary', 'select', 'Subsidiary','subsidiary').setDisplayType('inline');	
				var i_location_f = i_time_expense_tab.addField('custpage_location', 'select', 'Location','location').setDisplayType('hidden');	
				var i_customer_val= i_time_expense_tab.addField('custpage_customer', 'select','Customer','customer').setDisplayType('inline');		
				
				
													
				if(_logValidation(i_from_date) && _logValidation(i_to_date))
				{
					
				if(parseInt(i_user_role) != 3 )
				{
					var filter = new Array();
					//filter[0] = new nlobjSearchFilter('trandate', null, 'onorbefore', i_current_date);
					filter[0] = new nlobjSearchFilter('trandate', null,'within',i_from_date,i_to_date);
					filter[1] = new nlobjSearchFilter('subsidiary','customer', 'is', parseInt(i_user_subsidiary));
					if(_logValidation(i_customer))
						{
						filter[2] = new nlobjSearchFilter('customersubof', null, 'is',parseInt(i_customer));
						}
				}
				else
				{
					var filter = new Array();
					//filter[0] = new nlobjSearchFilter('trandate', null, 'onorbefore', i_current_date);
					filter[0] = new nlobjSearchFilter('trandate', null,'within',i_from_date,i_to_date);
					filter[1] = new nlobjSearchFilter('subsidiary','customer','is',parseInt(i_user_subsidiary));
					filter[2] = new nlobjSearchFilter('custbody_is_provision_created',null,'is','F');
						if(_logValidation(i_customer))
						{
						filter[2] = new nlobjSearchFilter('customersubof', null,'is',parseInt(i_customer));
						}
				}	
					
			   
				var i_search_results = searchRecord('expensereport','customsearch_unbilled_expense_search_2_4',filter,null);
				
				 
				if(_logValidation(i_search_results))
				{
				  i_count=i_search_results.length;
				  nlapiLogExecution('DEBUG', 'suiteletFunction','expensereport Search Results Length  -->' + i_search_results.length);	
					//------------added by swati for testing--------------------------------------
				
					 
					//for(var c=(rowCount - rowsPerPage);c<rowCount;c++)//for(var c=0;c<90;c++)			
					for(var c=0;c<i_search_results.length;c++)
					{
					var a_search_transaction_result = i_search_results[c];
			        
					
					if(a_search_transaction_result ==null || a_search_transaction_result == '' || a_search_transaction_result == undefined)
					{
						break;
					}	
					var columns = a_search_transaction_result.getAllColumns();	
					var i_subsidiary_1=parseInt(i_user_subsidiary);
				   
				   var i_practice=a_search_transaction_result.getValue(columns[11]);
				   
				   var i_vertical=a_search_transaction_result.getValue(columns[12]);
				   var i_customer=a_search_transaction_result.getValue(columns[14]);
				   
				   //  var i_currency=a_search_transaction_result.getValue(columns[12]);
				   
			        var columnLen = columns.length;
			      
					for(var hg= 0 ;hg<columnLen ; hg++ )
					{
						var column = columns[hg];
						var label = column.getLabel();
						var value = a_search_transaction_result.getValue(column)
						var text = a_search_transaction_result.getText(column)
											
					 if(label =='Internal ID')
					 {
					 	i_internal_ID = value;
					 }
					 if(label =='Date')
					 {
					 	i_date = value;
					 }
					 if(label =='Amount(Debit)')
					 {
					 	i_amount = value;
					 }
					 if(label =='Currency')
					 {
					 	i_currency = value;						
					 }
					 if(label =='Employee')
					 {
					 	i_employee = value;
					 }		
					 if(label =='Project ID')
					 {
					 	i_projectID = value;
					 }
					 if(label =='Project Name')
					 {
					 	i_project_name = value;
					 }
					 if(label =='Location')
					 {
					 	i_location = value;
					 }
					
					 
					}//Column Loop				
					//nlapiLogExecution('DEBUG', 'suiteletFunction',' Expense ID  -->' + i_internal_ID);	
					
					var is_invoice_created = get_invoice_created_details(i_internal_ID);
					nlapiLogExecution('DEBUG', 'suiteletFunction',' Is Invoice Created  -->' + is_invoice_created);
					nlapiLogExecution('DEBUG', 'suiteletFunction','i_internal_ID  -->' + i_internal_ID);	
					 
					if(is_invoice_created)
					{
					}
					else{
					  	nlapiLogExecution('DEBUG', 'inside first if');	
					 
				  if (_logValidation(a_TR_array_values) && _logValidation(i_internal_ID)) 
				  {
				      nlapiLogExecution('DEBUG', 'inside second if');	
				  //	if (a_TR_array_values.indexOf(i_internal_ID) == -1) 
					{
				  	i_ent++;					
					
					 nlapiLogExecution('DEBUG', 'inside third if');	
					//var a_employee_details = get_practice(i_employee)
					
					//var a_split_array = new Array();
					
					
					
					if(!_logValidation(i_practice))
					{
						i_practice = ''
					}
					if(!_logValidation(i_vertical))
					{
						i_vertical = ''
					}
					if(!_logValidation(i_employee))
					{
						i_employee = ''
					}	
					if(!_logValidation(i_projectID))
					{
						i_projectID = ''
					}	
					if(!_logValidation(i_date))
					{
						i_date = ''
					}	
					if(!_logValidation(i_amount))
					{
						i_amount = ''
					}	
					if(!_logValidation(i_currency))
					{
						i_currency = ''
					}	
					if(!_logValidation(i_exchange_rate))
					{
						i_exchange_rate = ''
					}	
					if(!_logValidation(i_subsidiary_1))
					{
						i_subsidiary_1 = ''
					}	
						/*nlapiLogExecution('DEBUG', 'suiteletFunction',' i_ent  -->' + i_ent);	
						nlapiLogExecution('DEBUG', 'suiteletFunction',' i_internal_ID  -->' + i_internal_ID);	
						nlapiLogExecution('DEBUG', 'suiteletFunction',' i_employee  -->' + i_employee);	
						nlapiLogExecution('DEBUG', 'suiteletFunction',' i_projectID  -->' + i_projectID);	
						nlapiLogExecution('DEBUG', 'suiteletFunction','i_vertical  -->' + i_vertical);
						nlapiLogExecution('DEBUG', 'suiteletFunction','i_vertical  -->' + i_practice);	
						nlapiLogExecution('DEBUG', 'suiteletFunction','i_date  -->' + i_date);	
						nlapiLogExecution('DEBUG', 'suiteletFunction','i_amount  -->' + i_amount);	
						nlapiLogExecution('DEBUG', 'suiteletFunction','i_currency  -->' + i_currency);	
						//nlapiLogExecution('DEBUG', 'suiteletFunction','i_exchange_rate  -->' + i_exchange_rate);	
						nlapiLogExecution('DEBUG', 'suiteletFunction','i_subsidiary_1  -->' + i_subsidiary_1);	*/										
					// ========================= Set values ==========================
					i_time_expense_tab.setLineItemValue('custpage_internal_id',parseInt(i_ent), i_internal_ID);		     
	                i_time_expense_tab.setLineItemValue('custpage_s_no',parseInt(i_ent), parseInt(i_ent).toFixed(0));					
                    i_time_expense_tab.setLineItemValue('custpage_employee',parseInt(i_ent),i_employee);
					i_time_expense_tab.setLineItemValue('custpage_project',parseInt(i_ent),i_projectID);
					i_time_expense_tab.setLineItemValue('custpage_vertical',parseInt(i_ent),i_vertical);
				    i_time_expense_tab.setLineItemValue('custpage_practice',parseInt(i_ent), i_practice);
					i_time_expense_tab.setLineItemValue('custpage_date',parseInt(i_ent),i_date);
					i_time_expense_tab.setLineItemValue('custpage_amount',parseInt(i_ent),i_amount);
					i_time_expense_tab.setLineItemValue('custpage_currency',parseInt(i_ent),i_currency);
					//i_time_expense_tab.setLineItemValue('custpage_exchange_rate',parseInt(i_ent),i_exchange_rate);
					i_time_expense_tab.setLineItemValue('custpage_subsidiary',parseInt(i_ent),i_subsidiary_1);
					i_time_expense_tab.setLineItemValue('custpage_customer',parseInt(i_ent),i_customer);
				
					  	}
					  }
					  else
					  {	
						  
						  nlapiLogExecution('DEBUG', 'suiteletFunction',' i_ent  -->' + i_ent);	
							nlapiLogExecution('DEBUG', 'suiteletFunction',' i_internal_ID  -->' + i_internal_ID);	
							nlapiLogExecution('DEBUG', 'suiteletFunction',' i_employee  -->' + i_employee);	
							nlapiLogExecution('DEBUG', 'suiteletFunction',' i_projectID  -->' + i_projectID);	
							nlapiLogExecution('DEBUG', 'suiteletFunction','i_vertical  -->' + i_vertical);
							nlapiLogExecution('DEBUG', 'suiteletFunction','i_vertical  -->' + i_practice);	
							nlapiLogExecution('DEBUG', 'suiteletFunction','i_date  -->' + i_date);	
							nlapiLogExecution('DEBUG', 'suiteletFunction','i_amount  -->' + i_amount);	
							nlapiLogExecution('DEBUG', 'suiteletFunction','i_currency  -->' + i_currency);	
							//nlapiLogExecution('DEBUG', 'suiteletFunction','i_exchange_rate  -->' + i_exchange_rate);	
							nlapiLogExecution('DEBUG', 'suiteletFunction','i_subsidiary_1  -->' + i_subsidiary_1);
					  	i_ent++;					
						/*var a_employee_details = get_practice(i_employee)
					
						var a_split_array = new Array();
					    var i_subsidiary_1;
						if(_logValidation(a_employee_details))
						{
							a_split_array = a_employee_details[0].split('&&##&&')
							
							i_practice = a_split_array[0];
							
							i_subsidiary_1 = a_split_array[1];	
							
						}//Practice
								
						var a_split_array = new Array();
						var a_project_details = get_project_data(i_projectID)
						var i_vertical;
	                    var i_currency_k;
						var i_exchange_rate;
						
						if(_logValidation(a_project_details))
						{
							a_split_array = a_project_details[0].split('$$$$')
													
							i_vertical = a_split_array[0];
	                        i_currency_k = a_split_array[1];
							i_exchange_rate = a_split_array[2];
						
													
						}//Project Details*/
				
						if(!_logValidation(i_location))
						{
							i_location = ''
						}
						if(!_logValidation(i_practice))
						{
							i_practice = ''
						}
						if(!_logValidation(i_vertical))
						{
							i_vertical = ''
						}
						if(!_logValidation(i_employee))
						{
							i_employee = ''
						}	
						if(!_logValidation(i_projectID))
						{
							i_projectID = ''
						}	
						if(!_logValidation(i_date))
						{
							i_date = ''
						}	
						if(!_logValidation(i_amount))
						{
							i_amount = ''
						}	
						if(!_logValidation(i_currency))
						{
							i_currency = ''
						}	
						if(!_logValidation(i_exchange_rate))
						{
							i_exchange_rate = ''
						}	
						if(!_logValidation(i_subsidiary_1))
						{
							i_subsidiary_1 = ''
						}

						/*nlapiLogExecution('DEBUG', 'suiteletFunction',' i_ent  -->' + i_ent);	
						nlapiLogExecution('DEBUG', 'suiteletFunction',' i_internal_ID  -->' + i_internal_ID);	
						nlapiLogExecution('DEBUG', 'suiteletFunction',' i_employee  -->' + i_employee);	
						nlapiLogExecution('DEBUG', 'suiteletFunction',' i_projectID  -->' + i_projectID);	
						nlapiLogExecution('DEBUG', 'suiteletFunction','i_vertical  -->' + i_vertical);
						nlapiLogExecution('DEBUG', 'suiteletFunction','i_vertical  -->' + i_practice);	
						nlapiLogExecution('DEBUG', 'suiteletFunction','i_date  -->' + i_date);	
						nlapiLogExecution('DEBUG', 'suiteletFunction','i_amount  -->' + i_amount);	
						nlapiLogExecution('DEBUG', 'suiteletFunction','i_currency  -->' + i_currency);	
						nlapiLogExecution('DEBUG', 'suiteletFunction','i_exchange_rate  -->' + i_exchange_rate);	
						nlapiLogExecution('DEBUG', 'suiteletFunction','i_subsidiary_1  -->' + i_subsidiary_1);*/								
						// ========================= Set values ==========================
						i_time_expense_tab.setLineItemValue('custpage_internal_id', parseInt(i_ent), i_internal_ID);		     
		                i_time_expense_tab.setLineItemValue('custpage_s_no',parseInt(i_ent),parseInt(i_ent).toFixed(0));					
	                    i_time_expense_tab.setLineItemValue('custpage_employee',parseInt(i_ent),i_employee);
						i_time_expense_tab.setLineItemValue('custpage_project',parseInt(i_ent),i_projectID);
						i_time_expense_tab.setLineItemValue('custpage_vertical',parseInt(i_ent),i_vertical);
					    i_time_expense_tab.setLineItemValue('custpage_practice',parseInt(i_ent), i_practice);
						i_time_expense_tab.setLineItemValue('custpage_date',parseInt(i_ent),i_date);
						i_time_expense_tab.setLineItemValue('custpage_amount',parseInt(i_ent),i_amount);
						i_time_expense_tab.setLineItemValue('custpage_currency',parseInt(i_ent),i_currency);
						//i_time_expense_tab.setLineItemValue('custpage_exchange_rate',parseInt(i_ent),i_exchange_rate);
						i_time_expense_tab.setLineItemValue('custpage_subsidiary',parseInt(i_ent),i_subsidiary_1);
						i_time_expense_tab.setLineItemValue('custpage_location',parseInt(i_ent),i_location);
						i_time_expense_tab.setLineItemValue('custpage_customer',parseInt(i_ent),i_customer);
							
					  }						
						
					}// IS Invoice Created ? 
					 
					 
			
				
					var i_usage_end = i_context.getRemainingUsage();
	              // nlapiLogExecution('DEBUG', 'time_tracking_details','Usage End  -->' + i_usage_end);
			
					if(i_usage_end<30)
					{
						break;
					}	
												
				}//Loop Search Results	
					
			}//Search Results 
					
				}//Validation - IF Records , Item & Sales Order
				
				
			}//UNBILLED EXPENSE
					
			// ============================ UNBILLED TIME ==================================
			
			if(i_unbilled_type == 2)
			{
				//nlapiLogExecution('DEBUG', 'i_unbilled_type == 2');
				//if(i_criteria == 3)
					{
					var i_internalID = i_time_expense_tab.addField('custpage_internal_id','text','ID').setDisplayType('inline');
					}
				/*else
					{
				var i_internalID = i_time_expense_tab.addField('custpage_internal_id','select','ID','timebill').setDisplayType('inline');	
					}*/
				var i_month_of_date = i_time_expense_tab.addField('custpage_month_of_date', 'text','Month Of date','location').setDisplayType('inline');		
				var i_employee = i_time_expense_tab.addField('custpage_employee', 'select', 'Employee','employee').setDisplayType('inline');	
				var i_project_code = i_time_expense_tab.addField('custpage_project', 'select', 'Project Code','job').setDisplayType('inline');	
				var i_vertical = i_time_expense_tab.addField('custpage_vertical', 'select', 'Vertical','classification').setDisplayType('inline');	
				var i_practice = i_time_expense_tab.addField('custpage_practice', 'select', 'Practice','department').setDisplayType('inline');	
				var i_date = i_time_expense_tab.addField('custpage_date', 'date', 'Date');	
				var i_hrs = i_time_expense_tab.addField('custpage_hours', 'text', 'Hours');	
				var i_rate = i_time_expense_tab.addField('custpage_rate', 'text', 'Rate');	
				var i_amount = i_time_expense_tab.addField('custpage_amount', 'text', 'Amount');	
				var i_currency = i_time_expense_tab.addField('custpage_currency', 'select', 'Currency','currency').setDisplayType('inline');
				var i_item = i_time_expense_tab.addField('custpage_item', 'select', 'item','item').setDisplayType('inline');
					
				var i_subsidiary_f = i_time_expense_tab.addField('custpage_subsidiary', 'select', 'Subsidiary','subsidiary').setDisplayType('inline');	
			
				
				var i_customer_f = i_time_expense_tab.addField('custpage_customer', 'select','Customer','customer').setDisplayType('inline');
			//	var i_customer_f1 = i_time_expense_tab.addField('custpage_customer1','text','Customer').setDisplayType('inline');				
					var i_location = i_time_expense_tab.addField('custpage_location', 'select','Location','location').setDisplayType('inline');	
					


                if(i_criteria == 3)
					{	
                        var i_exchange_rate = i_time_expense_tab.addField('custpage_exchange_rate', 'text', 'Exchange Rate').setDisplayType('inline');			
						var d_billing_start_date = i_time_expense_tab.addField('custpage_billing_start_date', 'date', 'Billing Start Date').setDisplayType('inline');
						var d_billing_end_date = i_time_expense_tab.addField('custpage_billing_end_date', 'date', 'Billing End Date').setDisplayType('inline');
						var i_billed_days = i_time_expense_tab.addField('custpage_billed_days', 'text', 'Billed Days').setDisplayType('inline');
						var i_approved_days = i_time_expense_tab.addField('custpage_approved_days', 'text', 'Approved Days').setDisplayType('inline');
						var i_not_approved_days = i_time_expense_tab.addField('custpage_not_approved_days', 'text', 'Not Approved Days').setDisplayType('inline');
						var i_leave_days = i_time_expense_tab.addField('custpage_leave_days', 'text', 'Leave Days').setDisplayType('inline');
						var i_holiday_days = i_time_expense_tab.addField('custpage_holiday_days', 'text', 'Holiday Days').setDisplayType('inline');
						var i_floating_holiday_days = i_time_expense_tab.addField('custpage_floating_holiday_days', 'text', 'Floating Holiday Days').setDisplayType('inline');
						var i_not_submitted_holiday = i_time_expense_tab.addField('custpage_not_submitted_holiday', 'text', 'Not Submitted Holiday').setDisplayType('inline');
						var i_not_submitted_days = i_time_expense_tab.addField('custpage_not_submitted_days', 'text', 'Not Submitted Days').setDisplayType('inline');
						var i_not_submitted_hour = i_time_expense_tab.addField('custpage_not_submitted_hour', 'text', 'Not Submitted Hour').setDisplayType('inline');
						
						
					}
					
				if(_logValidation(i_from_date)  && _logValidation(i_to_date))
				{
				nlapiLogExecution('DEBUG', 'suiteletFunction','i_from_date -->' + i_from_date);
				nlapiLogExecution('DEBUG', 'suiteletFunction','i_to_date -->' + i_to_date);				
		     /* var columns = new Array();
		      columns[0] = new nlobjSearchColumn('internalid').setSort(); //include internal id in the returned columns and sort for reference
                var filter = new Array();
			    filter[0] = new nlobjSearchFilter('date', null, 'within', i_from_date,i_to_date);	
				filter[1] = new nlobjSearchFilter('subsidiary', 'customer', 'is', parseInt(i_user_subsidiary));
				var ij=2;
				if(i_customer == null || i_customer == '' || i_customer == 'null')
						{
						
						}
						else
						{
						//nlapiLogExecution('DEBUG', 'suiteletFunction','i_customer -->' + i_customer);	
						filter[ij] = new nlobjSearchFilter('customer','job','is',parseInt(i_customer));
						ij++
						}*/
				
				/*if(i_criteria == 1)
				{						
				   
							var i_search_results = searchRecord('timebill','customsearch_unbilled_approved_time_sear',filter,columns);
							var completeResultSet = i_search_results; //container of the complete result set
							if(completeResultSet != null)
							{
								while(i_search_results.length == 1000)
								{ //re-run the search if limit has been reached
									 var lastId = i_search_results[999].getValue('internalid'); //note the last record retrieved
									  // nlapiLogExecution('DEBUG', 'suiteletFunction lastId',lastId );
									 filter[ij] = new nlobjSearchFilter('internalidnumber',null,'greaterthan',lastId); //create new filter to restrict the next search based on the last record returned
									 i_search_results = searchRecord('timebill','customsearch_unbilled_approved_time_sear',filter,columns);
									 completeResultSet = completeResultSet.concat(i_search_results); //add the result to the complete result set 
								} 
								 i_search_results=completeResultSet;
							}
							// nlapiLogExecution('DEBUG', 'suiteletFunction',' Not submitted - Allocated -->completeResultSet' + completeResultSet.length);
			    
				}//Approved - Unbilled
				if(i_criteria == 2)
				{						
				 
							var i_search_results = searchRecord('timebill','customsearch_submitted_not_approved_time',filter,columns);
							var completeResultSet = i_search_results; //container of the complete result set
							if(completeResultSet != null)
							{
								while(i_search_results.length == 1000)
								{ //re-run the search if limit has been reached
									 var lastId = i_search_results[999].getValue('internalid'); //note the last record retrieved
									 filter[ij] = new nlobjSearchFilter('internalidnumber',null,'greaterthan',lastId); //create new filter to restrict the next search based on the last record returned
									 i_search_results = searchRecord('timebill','customsearch_submitted_not_approved_time',filter,columns);
									 completeResultSet = completeResultSet.concat(i_search_results); //add the result to the complete result set 
								} 
							}
							// nlapiLogExecution('DEBUG', 'suiteletFunction',' Not submitted - Allocated -->' + i_search_results);
							 i_search_results=completeResultSet;
 
				}//Submitted but Not approved - Unbilled*/
				
				//if(i_criteria == 3)
				{	
					//var columns = new Array();
					//columns[0] = new nlobjSearchColumn('internalid').setSort(); //include internal id in the returned columns and sort for reference
                    
					nlapiLogExecution('DEBUG', 'suiteletFunction',' i_from_date -->' + i_from_date);
					nlapiLogExecution('DEBUG', 'suiteletFunction',' i_to_date' + i_to_date);
					nlapiLogExecution('DEBUG', 'suiteletFunction',' i_user_subsidiary' + i_user_subsidiary);
					nlapiLogExecution('DEBUG', 'suiteletFunction',' i_criteria' + i_criteria);
					nlapiLogExecution('DEBUG', 'suiteletFunction',' i_criteria' + i_criteria);
					
					var filter = new Array();
				     var i=4;
				     filter[0] = new nlobjSearchFilter('custrecord_month_val',null,'is',i_month);
			    	  
	   			      filter[1] = new nlobjSearchFilter('custrecord_year_val',null,'is',i_year);
	   			      
                    //filter[0] = new nlobjSearchFilter('custrecord_from_date', null,'on',i_from_date);	
   			    	//filter[1] = new nlobjSearchFilter('custrecord_to_date', null,'on',i_to_date);
   			    	filter[2] = new nlobjSearchFilter('custrecord_subsidiary',null,'is', parseInt(i_user_subsidiary));
   			    	filter[3] = new nlobjSearchFilter('custrecord_criteria',null,'is', parseInt(i_criteria));
   			    	
					
					if(_logValidation(i_customer))
						{
						filter[i] = new nlobjSearchFilter('custrecord_customer',null,'is',parseInt(i_customer));
						i++;
						}	
						
						
   			    	if(i_criteria == 3)
   			    	{
   			    		
					     nlapiLogExecution('DEBUG', 'suiteletFunction',' i_criteria -->' + i_criteria);
   			    		filter[i] = new nlobjSearchFilter('custrecord_billable',null,'is','T');
   			    		i++;
   			    		filter[i] = new nlobjSearchFilter('custrecord_not_submitted_days',null,'greaterthan',0);
   			    		i++;
   			    	}
					
   			    
								   

					var i_search_results = searchRecord('customrecord_time_bill_rec','customsearch_time_bill_rec_search',filter,null);//customsearch_time_bill_rec_search//289
							/*var completeResultSet = i_search_results; //container of the complete result set
							while(i_search_results.length == 1000)
							{ //re-run the search if limit has been reached
								 var lastId = i_search_results[999].getValue('internalid'); //note the last record retrieved
								 filter[ij] = new nlobjSearchFilter('internalidnumber',null,'greaterthan',lastId); //create new filter to restrict the next search based on the last record returned
								 i_search_results = searchRecord('timebill',286,filter,columns);//customsearch_not_submitted_allocated
								 completeResultSet = completeResultSet.concat(i_search_results); //add the result to the complete result set 
							} 
							
						//	 i_search_results=completeResultSet;*/
					      //nlapiLogExecution('DEBUG', 'suiteletFunction',' Not submitted - Allocated -->' + i_search_results.length);
						 
                     //-------------------------------------------------------------------------------					 
				   

				}//Not submitted - Allocated
				
				
				if(i_criteria == 4)
				{
					
							var i_search_results_1 = searchRecord('timebill','customsearch_unbilled_approved_time_sear',filter,columns);
							var completeResultSet = i_search_results; //container of the complete result set
							while(i_search_results.length == 1000)
							{ //re-run the search if limit has been reached
								 var lastId = i_search_results[999].getValue('internalid'); //note the last record retrieved
								  // nlapiLogExecution('DEBUG', 'suiteletFunction lastId',lastId );
								 filter[ij] = new nlobjSearchFilter('internalidnumber',null,'greaterthan',lastId); //create new filter to restrict the next search based on the last record returned
								 i_search_results = searchRecord('timebill','customsearch_unbilled_approved_time_sear',filter,columns);
								 completeResultSet = completeResultSet.concat(i_search_results); //add the result to the complete result set 
							} 
							 i_search_results_1=completeResultSet;
							// nlapiLogExecution('DEBUG', 'suiteletFunction',' Not submitted - Allocated -->i_search_results_1' + i_search_results_1.length);
							
                     //-------------------------------------------------------------------------------					   
				   //-------------------------------------------------------------------------------------------------------------
				  
				  // var i_search_results_2 = searchRecord('timebill','customsearch_submitted_not_approved_time',filter,columns);
				 // nlapiLogExecution('DEBUG', 'suiteletFunction',' Submitted but Not approved - Unbilled -->' + i_search_results_2);	
				 	var i_search_results_2 = searchRecord('timebill','customsearch_submitted_not_approved_time',filter,columns);
							var completeResultSet = i_search_results; //container of the complete result set
							while(i_search_results.length == 1000)
							{ //re-run the search if limit has been reached
								 var lastId = i_search_results[999].getValue('internalid'); //note the last record retrieved
								 filter[ij] = new nlobjSearchFilter('internalidnumber',null,'greaterthan',lastId); //create new filter to restrict the next search based on the last record returned
								 i_search_results = searchRecord('timebill','customsearch_submitted_not_approved_time',filter,columns);
								 completeResultSet = completeResultSet.concat(i_search_results); //add the result to the complete result set 
							} 
							
							  i_search_results_2=completeResultSet;
							//nlapiLogExecution('DEBUG', 'suiteletFunction',' Not submitted - Allocated -->i_search_results_2' + i_search_results_2.length);
				  //--------------------------------------------------------------------------------------------------------------------------
				   
				   //var i_search_results_3 = searchRecord('timebill','customsearch_not_submitted_allocated',filter,columns);
				  //nlapiLogExecution('DEBUG', 'suiteletFunction',' Not submitted - Allocated -->' + i_search_results_3);
							var columns = new Array();
							columns[0] = new nlobjSearchColumn('internalid').setSort(); //include internal id in the returned columns and sort for reference
							var i_search_results_3 = searchRecord('timebill','customsearch_not_submitted_allocated',filter,columns);//customsearch_not_submitted_allocated
							var completeResultSet = i_search_results; //container of the complete result set
							while(i_search_results.length == 1000)
							{ //re-run the search if limit has been reached
								 var lastId = i_search_results[999].getValue('internalid'); //note the last record retrieved
								 filter[ij] = new nlobjSearchFilter('internalidnumber',null,'greaterthan',lastId); //create new filter to restrict the next search based on the last record returned
								 i_search_results = searchRecord('timebill','customsearch_not_submitted_allocated',filter,columns);//customsearch_not_submitted_allocated
								 completeResultSet = completeResultSet.concat(i_search_results); //add the result to the complete result set 
							} 
							
							 i_search_results_3=completeResultSet;
				//	nlapiLogExecution('DEBUG', 'suiteletFunction',' Not submitted - Allocated -->i_search_results_3' + i_search_results.length);							 
				  //-----------------------------------------------------------------------------------------------------------------------
				  //all_i_search_results=i_search_results_1.length+i_search_results_2.length+i_search_results_3.length;
				  i_count =  request.getParameter('custscript_count');
				//  nlapiLogExecution('DEBUG', 'suiteletFunction',' Not submitted - Allocated -->i_count' + i_count);	
				    
				}//Not submitted - Allocated
					
				var i_date1 = '' 
				if(_logValidation(i_search_results))
				{
					
					//--------------------------------------------------------------
					var verwrite_search=new Array();
					var matching_count=0;
					var without_matching=0;
					var mm=0;	
             
               if(i_criteria == 3)
                {//criteria check if start
				
					var i_usage_end = i_context.getRemainingUsage();
					//nlapiLogExecution('DEBUG','i_criteria == 3 before start if','Usage End  -->' + i_usage_end);
//=======================================================================================================================================================================
                 var i_cnt=1;
					for(var c=0;c<i_search_results.length;c++)	//for(var c=(rowCount - rowsPerPage);c<rowCount;c++)//for(var c=0;c<100;c++)	
					{  	  
						
						if(i_cnt == '1001')
						{
							//break;
						}
                	        var rec_id=i_search_results[c].getId();
                	        //nlapiLogExecution('DEBUG', 'columnLen', 'rec_id'+rec_id);
							var a_search_transaction_result = i_search_results[c];
							var columns=a_search_transaction_result.getAllColumns();
							// nlapiLogExecution('DEBUG', 'columnLen', 'columns= '+columns.length);
							var columnLen = columns.length;
                           // nlapiLogExecution('DEBUG', 'columnLen', 'columnLen= '+columnLen);
							
							var i_date=a_search_transaction_result.getValue(columns[0]);
							//nlapiLogExecution('DEBUG','i_date',i_date);
							//i_date1=i_date;
							
							var i_employee=a_search_transaction_result.getValue(columns[1]);
							//nlapiLogExecution('DEBUG','i_employee',i_employee);
								
							var i_project=a_search_transaction_result.getValue(columns[2]);
							//nlapiLogExecution('DEBUG','i_project',i_project);
								
							var i_practice=a_search_transaction_result.getValue(columns[8]);
							//nlapiLogExecution('DEBUG','i_practice',i_practice);
							
							var i_subsidiary=i_user_subsidiary;//a_search_transaction_result.getValue(columns[10]);
								
							//var i_hours=a_search_transaction_result.getValue(columns[6]);
							//nlapiLogExecution('DEBUG','i_hours',i_hours);
							
							 var i_hours=a_search_transaction_result.getValue(columns[33]);//not submitted hour
							//nlapiLogExecution('DEBUG','i_hours',i_hours);
							
							var i_customer=a_search_transaction_result.getValue(columns[9]);
							//nlapiLogExecution('DEBUG','i_customer',i_customer);
							
							var i_rate=a_search_transaction_result.getValue(columns[12]);
							//nlapiLogExecution('DEBUG','i_customer',i_customer);
							
							var i_currency=a_search_transaction_result.getValue(columns[13]);
							//nlapiLogExecution('DEBUG','i_customer',i_customer);
							
							var i_exchange_rate=a_search_transaction_result.getValue(columns[14]);
							//nlapiLogExecution('DEBUG','i_customer',i_customer);
							
							var i_vertical=a_search_transaction_result.getValue(columns[15]);
							//nlapiLogExecution('DEBUG','i_customer',i_customer);
							
							var i_location=a_search_transaction_result.getValue(columns[16]);
							//nlapiLogExecution('DEBUG','i_location',i_location);
							
							//-------------added 10 feb 2015----------------------
							var d_billing_start_date=a_search_transaction_result.getValue(columns[22]);
							//nlapiLogExecution('DEBUG','d_billing_start_date',d_billing_start_date);
							
							var d_billing_end_date=a_search_transaction_result.getValue(columns[23]);
							//nlapiLogExecution('DEBUG','d_billing_end_date',d_billing_end_date);
							
							var i_billed_days=a_search_transaction_result.getValue(columns[24]);
							//nlapiLogExecution('DEBUG','i_billed_days',i_billed_days);
							
							var i_approved_days=a_search_transaction_result.getValue(columns[25]);
							//nlapiLogExecution('DEBUG','i_approved_days',i_approved_days);
							
							var i_not_approved_days=a_search_transaction_result.getValue(columns[26]);
							//nlapiLogExecution('DEBUG','i_not_approved_days',i_not_approved_days);
							
							var i_leave_days=a_search_transaction_result.getValue(columns[27]);
							//nlapiLogExecution('DEBUG','i_leave_days',i_leave_days);
							
							var i_holidays_days=a_search_transaction_result.getValue(columns[28]);
							//nlapiLogExecution('DEBUG','i_holidays_days',i_holidays_days);
							
							var i_floating_holidays_days=a_search_transaction_result.getValue(columns[29]);
							//nlapiLogExecution('DEBUG','i_floating_holidays_days',i_floating_holidays_days);
							
							var i_not_submitted_days=a_search_transaction_result.getValue(columns[30]);
							//nlapiLogExecution('DEBUG','i_not_submitted_days',i_not_submitted_days);
							
							var i_not_submitted_hour=a_search_transaction_result.getValue(columns[31]);
							//nlapiLogExecution('DEBUG','i_not_submitted_hour',i_not_submitted_hour);
							
							var i_not_submitted_holiday=a_search_transaction_result.getValue(columns[32]);
							//nlapiLogExecution('DEBUG','i_not_submitted_holiday',i_not_submitted_holiday);
							
							var i_month_date=a_search_transaction_result.getValue(columns[34]);
							//nlapiLogExecution('DEBUG','i_month_date',i_month_date);
							//custpage_month_of_date
							//----------------------------------------------------
						 
							
							if(!_logValidation(a_search_transaction_result))
							{
								break;
							}
						
											/*var a_split_array = new Array();
											var a_project_details = get_project_data(i_project)
											var i_vertical;
											var i_currency;
											var i_exchange_rate;
											
											if(_logValidation(a_project_details))
											{
												a_split_array = a_project_details[0].split('$$$$')
																		
												i_vertical = a_split_array[0];
												i_currency = a_split_array[1];
												i_exchange_rate = a_split_array[2];
												
											}//Project Details*/
											
											if(!_logValidation(i_currency))
											{
												i_currency = ''
											}
											if(!_logValidation(i_practice))
											{
												i_practice = ''
											}
											if(!_logValidation(i_vertical))
											{
												i_vertical = ''
											}			
											if(!_logValidation(i_rate))
											{
												i_rate = 0
											}
											if(!_logValidation(i_hours))
											{
												i_hours = 0
											}
												if(!_logValidation(i_employee))
											{
												i_employee = ''
											}	
											if(!_logValidation(i_projectID))
											{
												i_projectID = ''
											}	
											if(!_logValidation(i_date))
											{
												i_date = ''
											}	
											if(!_logValidation(i_amount))
											{
												i_amount = ''
											}	
											if(!_logValidation(i_currency))
											{
												i_currency = ''
											}	
											if(!_logValidation(i_exchange_rate))
											{
												i_exchange_rate = ''
											}	
											if(!_logValidation(i_subsidiary))
											{
												i_subsidiary = ''
											}
											if(!_logValidation(i_location))
											{
												i_location = ''
											}
											
											if(!_logValidation(d_billing_start_date))
											{
												d_billing_start_date = ''
											}
											if(!_logValidation(d_billing_end_date))
											{
												d_billing_end_date = ''
											}
											if(!_logValidation(i_billed_days))
											{
												i_billed_days = ''
											}
											if(!_logValidation(i_approved_days))
											{
												i_approved_days = ''
											}
											if(!_logValidation(i_not_approved_days))
											{
												i_not_approved_days = ''
											}
											if(!_logValidation(i_leave_days))
											{
												i_leave_days = ''
											}
											if(!_logValidation(i_holidays_days))
											{
												i_holidays_days = ''
											}
											if(!_logValidation(i_floating_holidays_days))
											{
												i_floating_holidays_days = ''
											}
											if(!_logValidation(i_not_submitted_holiday))
											{
												i_not_submitted_holiday = ''
											}
											

											var i_amount = parseFloat(i_rate)*parseFloat(i_hours);
											i_amount = parseFloat(i_amount)
											i_amount = i_amount.toFixed(2);
																
											// ========================= Set values ==========================
											i_time_expense_tab.setLineItemValue('custpage_internal_id', (parseInt(i_cnt)),rec_id);
											i_time_expense_tab.setLineItemValue('custpage_s_no', (parseInt(i_cnt)), (parseInt(i_cnt)).toFixed(0));
											
										
											
											i_time_expense_tab.setLineItemValue('custpage_employee',(parseInt(i_cnt)),i_employee);
											i_time_expense_tab.setLineItemValue('custpage_project', (parseInt(i_cnt)),i_project);
											i_time_expense_tab.setLineItemValue('custpage_vertical', (parseInt(i_cnt)),i_vertical);
											i_time_expense_tab.setLineItemValue('custpage_practice', (parseInt(i_cnt)),i_practice);
											i_time_expense_tab.setLineItemValue('custpage_date',(parseInt(i_cnt)),i_date1);
											i_time_expense_tab.setLineItemValue('custpage_hours',(parseInt(i_cnt)),i_hours);
											i_time_expense_tab.setLineItemValue('custpage_rate',(parseInt(i_cnt)), i_rate);
											i_time_expense_tab.setLineItemValue('custpage_amount',(parseInt(i_cnt)), i_amount);
											i_time_expense_tab.setLineItemValue('custpage_currency',(parseInt(i_cnt)),i_currency);					
											i_time_expense_tab.setLineItemValue('custpage_exchange_rate',(parseInt(i_cnt)),i_exchange_rate);
											i_time_expense_tab.setLineItemValue('custpage_subsidiary',(parseInt(i_cnt)),i_subsidiary);
											i_time_expense_tab.setLineItemValue('custpage_customer',(parseInt(i_cnt)),i_customer);
											i_time_expense_tab.setLineItemValue('custpage_location',(parseInt(i_cnt)),i_location);
											
											//---------------------------added 10 feb 2015---------------------------------

											i_time_expense_tab.setLineItemValue('custpage_billing_start_date',(parseInt(i_cnt)),d_billing_start_date);
											i_time_expense_tab.setLineItemValue('custpage_billing_end_date',(parseInt(i_cnt)),d_billing_end_date);
											i_time_expense_tab.setLineItemValue('custpage_billed_days',(parseInt(i_cnt)),i_billed_days);
											i_time_expense_tab.setLineItemValue('custpage_approved_days',(parseInt(i_cnt)),i_approved_days);
											i_time_expense_tab.setLineItemValue('custpage_not_approved_days',(parseInt(i_cnt)),i_not_approved_days);
											i_time_expense_tab.setLineItemValue('custpage_leave_days',(parseInt(i_cnt)),i_leave_days);
											i_time_expense_tab.setLineItemValue('custpage_holiday_days',(parseInt(i_cnt)),i_holidays_days);
											i_time_expense_tab.setLineItemValue('custpage_floating_holiday_days',(parseInt(i_cnt)),i_floating_holidays_days);//
											i_time_expense_tab.setLineItemValue('custpage_not_submitted_days',(parseInt(i_cnt)),i_not_submitted_days);
											i_time_expense_tab.setLineItemValue('custpage_not_submitted_hour',(parseInt(i_cnt)),i_not_submitted_hour);
											i_time_expense_tab.setLineItemValue('custpage_not_submitted_holiday',(parseInt(i_cnt)),i_not_submitted_holiday);
											
											//-----------------------------------------------------------------------------
											i_time_expense_tab.setLineItemValue('custpage_month_of_date',(parseInt(i_cnt)),i_month_date);
										
											
											i_cnt++;		

								//}//TR Arrya if  close
								
								
							
							var i_usage_end = i_context.getRemainingUsage();
						   // nlapiLogExecution('DEBUG', 'time_tracking_details','Usage End  -->' + i_usage_end);
					
							if(i_usage_end<10)
							{
								break;
							}	
								
					}//Loop Search Results	

//=======================================================================================================================================================================
                }//criteria check if close
				
                else
				{//criteria check else start
					//nlapiLogExecution('DEBUG','else inside i_search_results',i_search_results.length);				
                    	var i_cnt=1;					  
                    	for(var c=0;c<i_search_results.length;c++)	//for(var c=(rowCount - rowsPerPage);c<rowCount;c++)//for(var c=0;c<100;c++)	
    					{  	  
    						
    						if(i_cnt == '1001')
    						{
    							//break;
    						}
                    	        var rec_id=i_search_results[c].getId();
                    	        nlapiLogExecution('DEBUG', 'columnLen', 'rec_id'+rec_id);
    							var a_search_transaction_result = i_search_results[c];
    							var columns=a_search_transaction_result.getAllColumns();
    							// nlapiLogExecution('DEBUG', 'columnLen', 'columns= '+columns.length);
    							var columnLen = columns.length;
                               // nlapiLogExecution('DEBUG', 'columnLen', 'columnLen= '+columnLen);
    							
    							var i_date=a_search_transaction_result.getValue(columns[0]);
    							//nlapiLogExecution('DEBUG','i_date',i_date);
    						//	i_date1=i_date;
    							
    							var i_employee=a_search_transaction_result.getValue(columns[1]);
    							//nlapiLogExecution('DEBUG','i_employee',i_employee);
    								
    							var i_project=a_search_transaction_result.getValue(columns[2]);
    							//nlapiLogExecution('DEBUG','i_project',i_project);
    								
    							var i_practice=a_search_transaction_result.getValue(columns[8]);
    							//nlapiLogExecution('DEBUG','i_practice',i_practice);
    							
    							var i_subsidiary=i_user_subsidiary;//a_search_transaction_result.getValue(columns[10]);
    								
    							var i_hours=a_search_transaction_result.getValue(columns[18]);
    							//nlapiLogExecution('DEBUG','i_hours',i_hours);
    							
    							var i_customer=a_search_transaction_result.getValue(columns[9]);
    							//nlapiLogExecution('DEBUG','i_customer',i_customer);
    							
    							var i_rate=a_search_transaction_result.getValue(columns[12]);
    							//nlapiLogExecution('DEBUG','i_customer',i_customer);
    							
    							var i_currency=a_search_transaction_result.getValue(columns[13]);
    							//nlapiLogExecution('DEBUG','i_customer',i_customer);
    							
    							var i_exchange_rate=a_search_transaction_result.getValue(columns[14]);
    							//nlapiLogExecution('DEBUG','i_customer',i_customer);
    							
    							var i_vertical=a_search_transaction_result.getValue(columns[15]);
    							//nlapiLogExecution('DEBUG','i_customer',i_customer);
    							
    							var i_location=a_search_transaction_result.getValue(columns[16]);
    							//nlapiLogExecution('DEBUG','i_location',i_location);
    							
    							var i_criteria=a_search_transaction_result.getValue(columns[17]);
    							//nlapiLogExecution('DEBUG','i_criteria',i_criteria);
    							
    							var i_item=a_search_transaction_result.getValue(columns[19]);
    							nlapiLogExecution('DEBUG','i_item',i_item);
    						 
							 	var i_month_date=a_search_transaction_result.getValue(columns[34]);
								nlapiLogExecution('DEBUG','i_month_date',i_month_date);
    							
    							if(!_logValidation(a_search_transaction_result))
    							{
    								break;
    							}
    						
    											/*var a_split_array = new Array();
    											var a_project_details = get_project_data(i_project)
    											var i_vertical;
    											var i_currency;
    											var i_exchange_rate;
    											
    											if(_logValidation(a_project_details))
    											{
    												a_split_array = a_project_details[0].split('$$$$')
    																		
    												i_vertical = a_split_array[0];
    												i_currency = a_split_array[1];
    												i_exchange_rate = a_split_array[2];
    												
    											}//Project Details*/
    											
    											if(!_logValidation(i_currency))
    											{
    												i_currency = ''
    											}
    											if(!_logValidation(i_practice))
    											{
    												i_practice = ''
    											}
    											if(!_logValidation(i_vertical))
    											{
    												i_vertical = ''
    											}			
    											if(!_logValidation(i_rate))
    											{
    												i_rate = 0
    											}
    											if(!_logValidation(i_hours))
    											{
    												i_hours = 0
    											}
    												if(!_logValidation(i_employee))
    											{
    												i_employee = ''
    											}	
    											if(!_logValidation(i_projectID))
    											{
    												i_projectID = ''
    											}	
    											if(!_logValidation(i_date))
    											{
    												i_date = ''
    											}	
    											if(!_logValidation(i_amount))
    											{
    												i_amount = ''
    											}	
    											if(!_logValidation(i_currency))
    											{
    												i_currency = ''
    											}	
    											if(!_logValidation(i_exchange_rate))
    											{
    												i_exchange_rate = ''
    											}	
    											if(!_logValidation(i_subsidiary))
    											{
    												i_subsidiary = ''
    											}
    											if(!_logValidation(i_location))
    											{
    												i_location = ''
    											}
    											

    											var i_amount = parseFloat(i_rate)*parseFloat(i_hours);
    											i_amount = parseFloat(i_amount)
    											i_amount = i_amount.toFixed(2);
    																
    											// ========================= Set values ==========================
    											i_time_expense_tab.setLineItemValue('custpage_internal_id', (parseInt(i_cnt)),rec_id);
    											i_time_expense_tab.setLineItemValue('custpage_s_no', (parseInt(i_cnt)), (parseInt(i_cnt)).toFixed(0));
    											
    										
    											
    											i_time_expense_tab.setLineItemValue('custpage_employee',(parseInt(i_cnt)),i_employee);
    											i_time_expense_tab.setLineItemValue('custpage_project', (parseInt(i_cnt)),i_project);
    											i_time_expense_tab.setLineItemValue('custpage_vertical', (parseInt(i_cnt)),i_vertical);
    											i_time_expense_tab.setLineItemValue('custpage_practice', (parseInt(i_cnt)),i_practice);
    											i_time_expense_tab.setLineItemValue('custpage_date',(parseInt(i_cnt)),i_date1);
    											i_time_expense_tab.setLineItemValue('custpage_hours',(parseInt(i_cnt)),i_hours);
    											i_time_expense_tab.setLineItemValue('custpage_rate',(parseInt(i_cnt)), i_rate);
    											i_time_expense_tab.setLineItemValue('custpage_amount',(parseInt(i_cnt)), i_amount);
    											i_time_expense_tab.setLineItemValue('custpage_currency',(parseInt(i_cnt)),i_currency);					
    											i_time_expense_tab.setLineItemValue('custpage_exchange_rate',(parseInt(i_cnt)),i_exchange_rate);
    											i_time_expense_tab.setLineItemValue('custpage_subsidiary',(parseInt(i_cnt)),i_subsidiary);
    											i_time_expense_tab.setLineItemValue('custpage_customer',(parseInt(i_cnt)),i_customer);
    											i_time_expense_tab.setLineItemValue('custpage_location',(parseInt(i_cnt)),i_location);
    											i_time_expense_tab.setLineItemValue('custpage_item',(parseInt(i_cnt)),i_item);
												
												i_time_expense_tab.setLineItemValue('custpage_month_of_date',(parseInt(i_cnt)),i_month_date);
    											
    											i_cnt++;		

    								//}//TR Arrya if  close
    								
    								
    							
    							var i_usage_end = i_context.getRemainingUsage();
    						   // nlapiLogExecution('DEBUG', 'time_tracking_details','Usage End  -->' + i_usage_end);
    					
    							if(i_usage_end<10)
    							{
    								break;
    							}	
    								
    					}//Loop Search Results	
					
				}//criteria check else close	
							
				}//Search Results 
						
						//var length_of_third_search= parseInt(i_search_results.length)-parseInt(sa1)-parseInt(sa2)-parseInt(sa3)	;
						//nlapiLogExecution('DEBUG', 'length_of_third_search  -->' + i_cnt);	
									
							
						// ===================== All ================================
			 if(i_criteria == 4)
			 {				
						//nlapiLogExecution('DEBUG','without_matching'+without_matching);
						//nlapiLogExecution('DEBUG','matching_count'+matching_count);
						      //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++
							 if(i_criteria == 4)  
							{  //i_count=i_cnt;
							 test = request.getParameter('test');
									 if(i_count >= 97)
									{//if start
										  var fld = f_form.addField('custpage_rows', 'select', 'View Row Numbers');
										 var count_array=new Array();
										  var sum=90;
										 for(var ss=0 ; ss<i_count ; ss++)
										  {
											count_array[ss]=i_count;
										 
											
											if(i_count < 97)
											{
											break;
											}
											else
											{
											   i_count=i_count-97
											   //nlapiLogExecution('DEBUG','i_count','i_count-->'+i_count);
											}
											
										  }
											//nlapiLogExecution('DEBUG','count_array','count_array-->'+count_array.length);
											
											for(var sk=0,k=1 ; sk<count_array.length ; sk++,k++)
											{
												

												if(k == count_array.length)
													{
													fld.addSelectOption(count_array[sk], '0'+'to'+count_array[sk]);
												}
												else{
												var previous=count_array[sk+1];
												fld.addSelectOption(count_array[sk], previous+'to'+count_array[sk]);
												}
												//}
												//nlapiLogExecution('DEBUG','sk','sk-->'+sk);
											}
										
										//////////////////////////////////////////
										 var rowsPerPage = 97;
										 rowCount = request.getParameter('custpage_rows');
										 if (null == rowCount || rowCount == '')
										 {
										  rowCount = rowsPerPage;
										 }
										 //......................................
										
										// nlapiLogExecution('DEBUG','param_rowCount----------->',rowCountURL);
										 if (rowCountURL != null && rowCountURL != '') 
										 {
										  rowCount = rowCountURL;
										  fld.setDefaultValue(rowCount);
										 }
									}//if close
								else{//else start
										rowCount=i_count;
									}//if close
							}				
							  //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++
						
						var i_search_count = 0;
						if(_logValidation(i_search_results_1)&&_logValidation(i_search_results_2)&&_logValidation(i_search_results_3))
						{
						   // nlapiLogExecution('DEBUG','sk','sk-->'+i_search_results_1.length); 
							// nlapiLogExecution('DEBUG','sk','sk-->'+i_search_results_2.length); 
							 // nlapiLogExecution('DEBUG','sk','sk-->'+i_search_results_1.length); 
							//i_search_count = 33
							if(test == 1)
							{
							 var row_count_row=rowCount+33;
							}
							else{
							 rowCount = 33
							 rowsPerPage=33;
							 var row_count_row=33;
							 }
							
						}
						if(_logValidation(i_search_results_1)&&_logValidation(i_search_results_2)&&!_logValidation(i_search_results_3))
						{
							if(test == 1)
							{
							 var row_count_row=rowCount+49;
							}
							else{
							 rowCount = 49
							 rowsPerPage=49;
							 var row_count_row=49;
							 }			
						}
						if(_logValidation(i_search_results_1)&&!_logValidation(i_search_results_2)&&_logValidation(i_search_results_3))
						{
							if(test == 1)
							{
							 var row_count_row=rowCount+49;
							}
							else{
							 rowCount = 49
							 rowsPerPage=49;
							 var row_count_row=49;
							 }	
							//i_search_count = 90					
						}
						if(!_logValidation(i_search_results_1)&&_logValidation(i_search_results_2)&&_logValidation(i_search_results_3))
						{
							if(test == 1)
							{
							 var row_count_row=rowCount+49;
							}
							else{
							 rowCount = 49
							 rowsPerPage=49;
							 var row_count_row=49;
							 }					
						}
						if(_logValidation(i_search_results_1)&&!_logValidation(i_search_results_2)&&!_logValidation(i_search_results_3))
						{
							
							 var row_count_row=rowCount;
												
						}
						if(!_logValidation(i_search_results_1)&&!_logValidation(i_search_results_2)&&_logValidation(i_search_results_3))
						{
							 var row_count_row=rowCount;					
						}
						if(!_logValidation(i_search_results_1)&&_logValidation(i_search_results_2)&&!_logValidation(i_search_results_3))
						{
							 var row_count_row=rowCount;				
						}
						//nlapiLogExecution('DEBUG', 'suiteletFunction',' Search Count -->' + i_search_count);	
										
						if(_logValidation(i_search_results_1))
						{
							// nlapiLogExecution('DEBUG', 'suiteletFunction',' Search Results 1 Length Time -->' + i_search_results_1.length);	
													
							//for(var c=0;c<i_search_count;c++)
							for(var c=(rowCount - rowsPerPage);c<row_count_row;c++)
							{															
							var a_search_transaction_result = i_search_results_1[c];
							if(!_logValidation(a_search_transaction_result))
							{
								break;
							}
							var columns = a_search_transaction_result.getAllColumns();		
							var columnLen = columns.length;
						  
							for(var hg= 0 ;hg<columnLen ; hg++ )
							{
							var column = columns[hg];
							var label = column.getLabel();
							var value = a_search_transaction_result.getValue(column)
							var text = a_search_transaction_result.getText(column)
													
							 if(label =='Internal ID')
							 {
								i_internal_ID = value;
							 }
							  if(label =='Date')
							 {
								i_date = value;
							 }
							  if(label =='Employee')
							 {
								i_employee = value;
							 }
							  if(label =='Project')
							 {
								i_project = value;						
							 }
							  if(label =='Date')
							 {
								i_date = value;
							 }		
							  if(label =='Hours')
							 {
								i_hours = value;
							 }	
							  if(label =='Rate')
							 {
								i_rate = value;
							 }
							}//Column Loop	
							
							  if (_logValidation(a_TR_array_values) && _logValidation(i_internal_ID)) 
							  {
								if (a_TR_array_values.indexOf(i_internal_ID) == -1) 
								{	
									
									var a_employee_details = get_practice_expense(i_employee)	
									
									var a_split_array = new Array();
									var i_practice;
									var i_subsidiary;
									if(_logValidation(a_employee_details))
									{
										a_split_array = a_employee_details[0].split('&&##&&')
										
										i_practice = a_split_array[0];
										
										i_subsidiary = a_split_array[1];						
										
									}//Practice
								
									var a_split_array = new Array();
									var a_project_details = get_project_data(i_project)
									var i_vertical;
									var i_currency;
									var i_exchange_rate;
									
									if(_logValidation(a_project_details))
									{
										a_split_array = a_project_details[0].split('$$$$')
																
										i_vertical = a_split_array[0];
										i_currency = a_split_array[1];
										i_exchange_rate = a_split_array[2];
										
									}//Project Details
									
									if(!_logValidation(i_currency))
									{
										i_currency = ''
									}
									if(!_logValidation(i_practice))
									{
										i_practice = ''
									}
									if(!_logValidation(i_vertical))
									{
										i_vertical = ''
									}			
									if(!_logValidation(i_rate))
									{
										i_rate = 0
									}
									if(!_logValidation(i_hours))
									{
										i_hours = 0
									}
										if(!_logValidation(i_employee))
									{
										i_employee = ''
									}	
									if(!_logValidation(i_projectID))
									{
										i_projectID = ''
									}	
									if(!_logValidation(i_date))
									{
										i_date = ''
									}	
									if(!_logValidation(i_amount))
									{
										i_amount = ''
									}	
									if(!_logValidation(i_currency))
									{
										i_currency = ''
									}	
									if(!_logValidation(i_exchange_rate))
									{
										i_exchange_rate = ''
									}	
									if(!_logValidation(i_subsidiary))
									{
										i_subsidiary = ''
									}
									
									var i_amount = parseFloat(i_rate)*parseFloat(i_hours);
									i_amount = parseFloat(i_amount)
									i_amount = i_amount.toFixed(2);
														
									// ========================= Set values ==========================
									
									if ((parseInt(i_user_role) != 3) && (i_user_subsidiary == i_subsidiary)) 
									{
											i_cnt++;	
									i_time_expense_tab.setLineItemValue('custpage_internal_id', (parseInt(i_cnt)), i_internal_ID);
									i_time_expense_tab.setLineItemValue('custpage_s_no', (parseInt(i_cnt)), (parseInt(i_cnt)).toFixed(0));
									i_time_expense_tab.setLineItemValue('custpage_employee',(parseInt(i_cnt)),i_employee);
									i_time_expense_tab.setLineItemValue('custpage_project', (parseInt(i_cnt)),i_project);
									i_time_expense_tab.setLineItemValue('custpage_vertical', (parseInt(i_cnt)),i_vertical);
									i_time_expense_tab.setLineItemValue('custpage_practice', (parseInt(i_cnt)),i_practice);
									i_time_expense_tab.setLineItemValue('custpage_date',(parseInt(i_cnt)),i_date1);
									i_time_expense_tab.setLineItemValue('custpage_hours',(parseInt(i_cnt)),i_hours);
									i_time_expense_tab.setLineItemValue('custpage_rate',(parseInt(i_cnt)), i_rate);
									i_time_expense_tab.setLineItemValue('custpage_amount',(parseInt(i_cnt)), i_amount);
									i_time_expense_tab.setLineItemValue('custpage_currency',(parseInt(i_cnt)),i_currency);					
									i_time_expense_tab.setLineItemValue('custpage_exchange_rate',(parseInt(i_cnt)),i_exchange_rate);
									i_time_expense_tab.setLineItemValue('custpage_subsidiary',(parseInt(i_cnt)),i_subsidiary);
									
										
										
									}
									else if((parseInt(i_user_role) == 3))
									{
									i_cnt++;	
									i_time_expense_tab.setLineItemValue('custpage_internal_id', (parseInt(i_cnt)), i_internal_ID);
									i_time_expense_tab.setLineItemValue('custpage_s_no', (parseInt(i_cnt)), (parseInt(i_cnt)).toFixed(0));
									i_time_expense_tab.setLineItemValue('custpage_employee',(parseInt(i_cnt)),i_employee);
									i_time_expense_tab.setLineItemValue('custpage_project', (parseInt(i_cnt)),i_project);
									i_time_expense_tab.setLineItemValue('custpage_vertical', (parseInt(i_cnt)),i_vertical);
									i_time_expense_tab.setLineItemValue('custpage_practice', (parseInt(i_cnt)),i_practice);
									i_time_expense_tab.setLineItemValue('custpage_date',(parseInt(i_cnt)),i_date1);
									i_time_expense_tab.setLineItemValue('custpage_hours',(parseInt(i_cnt)),i_hours);
									i_time_expense_tab.setLineItemValue('custpage_rate',(parseInt(i_cnt)), i_rate);
									i_time_expense_tab.setLineItemValue('custpage_amount',(parseInt(i_cnt)), i_amount);
									i_time_expense_tab.setLineItemValue('custpage_currency',(parseInt(i_cnt)),i_currency);					
									i_time_expense_tab.setLineItemValue('custpage_exchange_rate',(parseInt(i_cnt)),i_exchange_rate);
									i_time_expense_tab.setLineItemValue('custpage_subsidiary',(parseInt(i_cnt)),i_subsidiary);
									
										
									}
									
								  
								}
							  }
							  else
							  {
								
									var a_employee_details = get_practice_expense(i_employee)	
									
									var a_split_array = new Array();
									var i_practice;
									var i_subsidiary;
									if(_logValidation(a_employee_details))
									{
										a_split_array = a_employee_details[0].split('&&##&&')
										
										i_practice = a_split_array[0];
										
										i_subsidiary = a_split_array[1];						
										
									}//Practice
								
									var a_split_array = new Array();
									var a_project_details = get_project_data(i_project)
									var i_vertical;
									var i_currency;
									var i_exchange_rate;
								
									if(_logValidation(a_project_details))
									{
										a_split_array = a_project_details[0].split('$$$$')
																
										i_vertical = a_split_array[0];
										i_currency = a_split_array[1];
										i_exchange_rate = a_split_array[2];
										
									}//Project Details
									
									if(!_logValidation(i_currency))
									{
										i_currency = ''
									}
									if(!_logValidation(i_practice))
									{
										i_practice = ''
									}
									if(!_logValidation(i_vertical))
									{
										i_vertical = ''
									}			
									if(!_logValidation(i_rate))
									{
										i_rate = 0
									}
									if(!_logValidation(i_hours))
									{
										i_hours = 0
									}
										if(!_logValidation(i_employee))
									{
										i_employee = ''
									}	
									if(!_logValidation(i_projectID))
									{
										i_projectID = ''
									}	
									if(!_logValidation(i_date))
									{
										i_date = ''
									}	
									if(!_logValidation(i_amount))
									{
										i_amount = ''
									}	
									if(!_logValidation(i_currency))
									{
										i_currency = ''
									}	
									if(!_logValidation(i_exchange_rate))
									{
										i_exchange_rate = ''
									}	
									if(!_logValidation(i_subsidiary))
									{
										i_subsidiary = ''
									}
										
									var i_amount = parseFloat(i_rate)*parseFloat(i_hours);
									i_amount = parseFloat(i_amount)
									i_amount = i_amount.toFixed(2);
														
									// ========================= Set values ==========================
									
									if ((parseInt(i_user_role) != 3) && (i_user_subsidiary == i_subsidiary)) 
									{
									i_cnt++;
									i_time_expense_tab.setLineItemValue('custpage_internal_id', (parseInt(i_cnt)), i_internal_ID);
									i_time_expense_tab.setLineItemValue('custpage_s_no', (parseInt(i_cnt)), (parseInt(i_cnt)).toFixed(0));
									i_time_expense_tab.setLineItemValue('custpage_employee',(parseInt(i_cnt)),i_employee);
									i_time_expense_tab.setLineItemValue('custpage_project', (parseInt(i_cnt)),i_project);
									i_time_expense_tab.setLineItemValue('custpage_vertical',(parseInt(i_cnt)),i_vertical);
									i_time_expense_tab.setLineItemValue('custpage_practice',(parseInt(i_cnt)),i_practice);
									i_time_expense_tab.setLineItemValue('custpage_date',(parseInt(i_cnt)),i_date);
									i_time_expense_tab.setLineItemValue('custpage_hours',(parseInt(i_cnt)),i_hours);
									i_time_expense_tab.setLineItemValue('custpage_rate',(parseInt(i_cnt)), i_rate);
									i_time_expense_tab.setLineItemValue('custpage_amount',(parseInt(i_cnt)), i_amount);
									i_time_expense_tab.setLineItemValue('custpage_currency',(parseInt(i_cnt)),i_currency);					
									i_time_expense_tab.setLineItemValue('custpage_exchange_rate',(parseInt(i_cnt)),i_exchange_rate);
									i_time_expense_tab.setLineItemValue('custpage_subsidiary',(parseInt(i_cnt)),i_subsidiary);
																	
									}	
									else if((parseInt(i_user_role) == 3))
									{
									i_cnt++;
									i_time_expense_tab.setLineItemValue('custpage_internal_id', (parseInt(i_cnt)), i_internal_ID);
									i_time_expense_tab.setLineItemValue('custpage_s_no', (parseInt(i_cnt)), (parseInt(i_cnt)).toFixed(0));
									i_time_expense_tab.setLineItemValue('custpage_employee',(parseInt(i_cnt)),i_employee);
									i_time_expense_tab.setLineItemValue('custpage_project', (parseInt(i_cnt)),i_project);
									i_time_expense_tab.setLineItemValue('custpage_vertical',(parseInt(i_cnt)),i_vertical);
									i_time_expense_tab.setLineItemValue('custpage_practice',(parseInt(i_cnt)),i_practice);
									i_time_expense_tab.setLineItemValue('custpage_date',(parseInt(i_cnt)),i_date);
									i_time_expense_tab.setLineItemValue('custpage_hours',(parseInt(i_cnt)),i_hours);
									i_time_expense_tab.setLineItemValue('custpage_rate',(parseInt(i_cnt)), i_rate);
									i_time_expense_tab.setLineItemValue('custpage_amount',(parseInt(i_cnt)), i_amount);
									i_time_expense_tab.setLineItemValue('custpage_currency',(parseInt(i_cnt)),i_currency);					
									i_time_expense_tab.setLineItemValue('custpage_exchange_rate',(parseInt(i_cnt)),i_exchange_rate);
									i_time_expense_tab.setLineItemValue('custpage_subsidiary',(parseInt(i_cnt)),i_subsidiary);
																	
									}						
								   
								
							  }//Internal ID
								
							var i_usage_end = i_context.getRemainingUsage();
				  //          nlapiLogExecution('DEBUG', 'time_tracking_details','Usage End  -->' + i_usage_end);
					
							if(i_usage_end<30)
							{
								break;
							}	
							}//Loop Search Results	
							
						}//Search Results 	
						
						
						
						if(_logValidation(i_search_results_2))
						{
							//nlapiLogExecution('DEBUG', 'suiteletFunction',' Search Results 2 Length Time -->' + i_search_results_2.length);	
													
							//for(var c=0;c<i_search_count;c++)
								for(var c=(rowCount - rowsPerPage);c<row_count_row;c++)
							{
																	
							var a_search_transaction_result = i_search_results_2[c];
							if(!_logValidation(a_search_transaction_result))
							{
								break;
							}
							var columns = a_search_transaction_result.getAllColumns();		
							var columnLen = columns.length;
						  
							for(var hg= 0 ;hg<columnLen ; hg++ )
							{
							var column = columns[hg];
							var label = column.getLabel();
							var value = a_search_transaction_result.getValue(column)
							var text = a_search_transaction_result.getText(column)
													
							 if(label =='Internal ID')
							 {
								i_internal_ID = value;
							 }
							  if(label =='Date')
							 {
								i_date = value;
							 }
							  if(label =='Employee')
							 {
								i_employee = value;
							 }
							  if(label =='Project')
							 {
								i_project = value;						
							 }
							  if(label =='Date')
							 {
								i_date = value;
							 }		
							  if(label =='Hours')
							 {
								i_hours = value;
							 }	
							  if(label =='Rate')
							 {
								i_rate = value;
							 }
							}//Column Loop	
							
							  if (_logValidation(a_TR_array_values) && _logValidation(i_internal_ID)) 
							  {
								if (a_TR_array_values.indexOf(i_internal_ID) == -1) 
								{	
								i_cnt++;			
									var a_employee_details = get_practice_expense(i_employee)	
									
									var a_split_array = new Array();
									var i_practice;
									var i_subsidiary;
									if(_logValidation(a_employee_details))
									{
										a_split_array = a_employee_details[0].split('&&##&&')
										
										i_practice = a_split_array[0];
										
										i_subsidiary = a_split_array[1];						
										
									}//Practice
								
									var a_split_array = new Array();
									var a_project_details = get_project_data(i_project)
									var i_vertical;
									var i_currency;
									var i_exchange_rate;
									
									if(_logValidation(a_project_details))
									{
										a_split_array = a_project_details[0].split('$$$$')
																
										i_vertical = a_split_array[0];
										i_currency = a_split_array[1];
										i_exchange_rate = a_split_array[2];
										
									}//Project Details
									
									if(!_logValidation(i_currency))
									{
										i_currency = ''
									}
									if(!_logValidation(i_practice))
									{
										i_practice = ''
									}
									if(!_logValidation(i_vertical))
									{
										i_vertical = ''
									}			
									if(!_logValidation(i_rate))
									{
										i_rate = 0
									}
									if(!_logValidation(i_hours))
									{
										i_hours = 0
									}
										if(!_logValidation(i_employee))
									{
										i_employee = ''
									}	
									if(!_logValidation(i_projectID))
									{
										i_projectID = ''
									}	
									if(!_logValidation(i_date))
									{
										i_date = ''
									}	
									if(!_logValidation(i_amount))
									{
										i_amount = ''
									}	
									if(!_logValidation(i_currency))
									{
										i_currency = ''
									}	
									if(!_logValidation(i_exchange_rate))
									{
										i_exchange_rate = ''
									}	
									if(!_logValidation(i_subsidiary))
									{
										i_subsidiary = ''
									}
										
									var i_amount = parseFloat(i_rate)*parseFloat(i_hours);
									i_amount = parseFloat(i_amount)
									i_amount = i_amount.toFixed(2);
														
									// ========================= Set values ==========================
									
									if ((parseInt(i_user_role) != 3) && (i_user_subsidiary == i_subsidiary)) 
									{
									i_cnt++;
									i_time_expense_tab.setLineItemValue('custpage_internal_id', (parseInt(i_cnt)), i_internal_ID);
									i_time_expense_tab.setLineItemValue('custpage_s_no', (parseInt(i_cnt)), (parseInt(i_cnt)).toFixed(0));
									i_time_expense_tab.setLineItemValue('custpage_employee',(parseInt(i_cnt)),i_employee);
									i_time_expense_tab.setLineItemValue('custpage_project', (parseInt(i_cnt)),i_project);
									i_time_expense_tab.setLineItemValue('custpage_vertical', (parseInt(i_cnt)),i_vertical);
									i_time_expense_tab.setLineItemValue('custpage_practice', (parseInt(i_cnt)),i_practice);
									i_time_expense_tab.setLineItemValue('custpage_date',(parseInt(i_cnt)),i_date);
									i_time_expense_tab.setLineItemValue('custpage_hours',(parseInt(i_cnt)),i_hours);
									i_time_expense_tab.setLineItemValue('custpage_rate',(parseInt(i_cnt)), i_rate);
									i_time_expense_tab.setLineItemValue('custpage_amount',(parseInt(i_cnt)), i_amount);
									i_time_expense_tab.setLineItemValue('custpage_currency',(parseInt(i_cnt)),i_currency);					
									i_time_expense_tab.setLineItemValue('custpage_exchange_rate',(parseInt(i_cnt)),i_exchange_rate);
									i_time_expense_tab.setLineItemValue('custpage_subsidiary',(parseInt(i_cnt)),i_subsidiary);
									
									}
									else if ((parseInt(i_user_role) == 3)) 
									{
									i_cnt++;	
									i_time_expense_tab.setLineItemValue('custpage_internal_id', (parseInt(i_cnt)), i_internal_ID);
									i_time_expense_tab.setLineItemValue('custpage_s_no', (parseInt(i_cnt)), (parseInt(i_cnt)).toFixed(0));
									i_time_expense_tab.setLineItemValue('custpage_employee',(parseInt(i_cnt)),i_employee);
									i_time_expense_tab.setLineItemValue('custpage_project', (parseInt(i_cnt)),i_project);
									i_time_expense_tab.setLineItemValue('custpage_vertical', (parseInt(i_cnt)),i_vertical);
									i_time_expense_tab.setLineItemValue('custpage_practice', (parseInt(i_cnt)),i_practice);
									i_time_expense_tab.setLineItemValue('custpage_date',(parseInt(i_cnt)),i_date);
									i_time_expense_tab.setLineItemValue('custpage_hours',(parseInt(i_cnt)),i_hours);
									i_time_expense_tab.setLineItemValue('custpage_rate',(parseInt(i_cnt)), i_rate);
									i_time_expense_tab.setLineItemValue('custpage_amount',(parseInt(i_cnt)), i_amount);
									i_time_expense_tab.setLineItemValue('custpage_currency',(parseInt(i_cnt)),i_currency);					
									i_time_expense_tab.setLineItemValue('custpage_exchange_rate',(parseInt(i_cnt)),i_exchange_rate);
									i_time_expense_tab.setLineItemValue('custpage_subsidiary',(parseInt(i_cnt)),i_subsidiary);
									
										
									}
									
								  
								}
							  }
							  else
							  {
							  
									var a_employee_details = get_practice_expense(i_employee)	
									
									var a_split_array = new Array();
									var i_practice;
									var i_subsidiary;
									if(_logValidation(a_employee_details))
									{
										a_split_array = a_employee_details[0].split('&&##&&')
										
										i_practice = a_split_array[0];
										
										i_subsidiary = a_split_array[1];						
										
									}//Practice
								
									var a_split_array = new Array();
									var a_project_details = get_project_data(i_project)
									var i_vertical;
									var i_currency;
									var i_exchange_rate;
								
									if(_logValidation(a_project_details))
									{
										a_split_array = a_project_details[0].split('$$$$')
																
										i_vertical = a_split_array[0];
										i_currency = a_split_array[1];
										i_exchange_rate = a_split_array[2];
										
									}//Project Details
									
									if(!_logValidation(i_currency))
									{
										i_currency = ''
									}
									if(!_logValidation(i_practice))
									{
										i_practice = ''
									}
									if(!_logValidation(i_vertical))
									{
										i_vertical = ''
									}			
									if(!_logValidation(i_rate))
									{
										i_rate = 0
									}
									if(!_logValidation(i_hours))
									{
										i_hours = 0
									}
										if(!_logValidation(i_employee))
									{
										i_employee = ''
									}	
									if(!_logValidation(i_projectID))
									{
										i_projectID = ''
									}	
									if(!_logValidation(i_date))
									{
										i_date = ''
									}	
									if(!_logValidation(i_amount))
									{
										i_amount = ''
									}	
									if(!_logValidation(i_currency))
									{
										i_currency = ''
									}	
									if(!_logValidation(i_exchange_rate))
									{
										i_exchange_rate = ''
									}	
									if(!_logValidation(i_subsidiary))
									{
										i_subsidiary = ''
									}
										
									var i_amount = parseFloat(i_rate)*parseFloat(i_hours);
									i_amount = parseFloat(i_amount)
									i_amount = i_amount.toFixed(2);
														
									// ========================= Set values ==========================
									
									if ((parseInt(i_user_role) != 3) && (i_user_subsidiary == i_subsidiary)) 
									{
										i_cnt++;
										
									i_time_expense_tab.setLineItemValue('custpage_internal_id', (parseInt(i_cnt)), i_internal_ID);
									i_time_expense_tab.setLineItemValue('custpage_s_no', (parseInt(i_cnt)), (parseInt(i_cnt)).toFixed(0));
									i_time_expense_tab.setLineItemValue('custpage_employee',(parseInt(i_cnt)),i_employee);
									i_time_expense_tab.setLineItemValue('custpage_project', (parseInt(i_cnt)),i_project);
									i_time_expense_tab.setLineItemValue('custpage_vertical',(parseInt(i_cnt)),i_vertical);
									i_time_expense_tab.setLineItemValue('custpage_practice',(parseInt(i_cnt)),i_practice);
									i_time_expense_tab.setLineItemValue('custpage_date',(parseInt(i_cnt)),i_date);
									i_time_expense_tab.setLineItemValue('custpage_hours',(parseInt(i_cnt)),i_hours);
									i_time_expense_tab.setLineItemValue('custpage_rate',(parseInt(i_cnt)), i_rate);
									i_time_expense_tab.setLineItemValue('custpage_amount',(parseInt(i_cnt)), i_amount);
									i_time_expense_tab.setLineItemValue('custpage_currency',(parseInt(i_cnt)),i_currency);					
									i_time_expense_tab.setLineItemValue('custpage_exchange_rate',(parseInt(i_cnt)),i_exchange_rate);
									i_time_expense_tab.setLineItemValue('custpage_subsidiary',(parseInt(i_cnt)),i_subsidiary);
									
										
									}
									else 
										if ((parseInt(i_user_role) == 3)) 
										{
											i_cnt++;
											
									i_time_expense_tab.setLineItemValue('custpage_internal_id', (parseInt(i_cnt)), i_internal_ID);
									i_time_expense_tab.setLineItemValue('custpage_s_no', (parseInt(i_cnt)), (parseInt(i_cnt)).toFixed(0));
									i_time_expense_tab.setLineItemValue('custpage_employee',(parseInt(i_cnt)),i_employee);
									i_time_expense_tab.setLineItemValue('custpage_project', (parseInt(i_cnt)),i_project);
									i_time_expense_tab.setLineItemValue('custpage_vertical',(parseInt(i_cnt)),i_vertical);
									i_time_expense_tab.setLineItemValue('custpage_practice',(parseInt(i_cnt)),i_practice);
									i_time_expense_tab.setLineItemValue('custpage_date',(parseInt(i_cnt)),i_date);
									i_time_expense_tab.setLineItemValue('custpage_hours',(parseInt(i_cnt)),i_hours);
									i_time_expense_tab.setLineItemValue('custpage_rate',(parseInt(i_cnt)), i_rate);
									i_time_expense_tab.setLineItemValue('custpage_amount',(parseInt(i_cnt)), i_amount);
									i_time_expense_tab.setLineItemValue('custpage_currency',(parseInt(i_cnt)),i_currency);					
									i_time_expense_tab.setLineItemValue('custpage_exchange_rate',(parseInt(i_cnt)),i_exchange_rate);
									i_time_expense_tab.setLineItemValue('custpage_subsidiary',(parseInt(i_cnt)),i_subsidiary);
									
										}
									
								
							  }//Internal ID
								
							var i_usage_end = i_context.getRemainingUsage();
						//        nlapiLogExecution('DEBUG', 'time_tracking_details','Usage End  -->' + i_usage_end);
					
							if(i_usage_end<30)
							{
								break;
							}	
						
					}//Loop Search Results	
					
				}//Search Results 2
				
				if(_logValidation(i_search_results_3))
				{
					 //nlapiLogExecution('DEBUG', 'suiteletFunction',' Search Results 3 Length Time -->' + i_search_results_3.length);	
				    for(var c=(rowCount - rowsPerPage);c<row_count_row;c++)						
					//for(var c=0;c<i_search_count;c++)
					{
															
					var a_search_transaction_result = i_search_results_3[c];
					if(!_logValidation(a_search_transaction_result))
					{
						break;
					}
			        var columns = a_search_transaction_result.getAllColumns();		
			        var columnLen = columns.length;
			      
					for(var hg= 0 ;hg<columnLen ; hg++ )
					{
					var column = columns[hg];
			        var label = column.getLabel();
					var value = a_search_transaction_result.getValue(column)
					var text = a_search_transaction_result.getText(column)
											
					 if(label =='Internal ID')
					 {
					 	i_internal_ID = value;
					 }
					  if(label =='Date')
					 {
					 	i_date = value;
					 }
					  if(label =='Employee')
					 {
					 	i_employee = value;
					 }
					  if(label =='Project')
					 {
					 	i_project = value;						
					 }
					  if(label =='Date')
					 {
					 	i_date = value;
					 }		
					  if(label =='Hours')
					 {
					 	i_hours = value;
					 }	
					  if(label =='Rate')
					 {
					 	i_rate = value;
					 }
					}//Column Loop	
					
					  if (_logValidation(a_TR_array_values) && _logValidation(i_internal_ID)) 
					  {
					  	if (a_TR_array_values.indexOf(i_internal_ID) == -1) 
						{	
					
							var a_employee_details = get_practice_expense(i_employee)	
							
							var a_split_array = new Array();
							var i_practice;
							var i_subsidiary;
							if(_logValidation(a_employee_details))
							{
								a_split_array = a_employee_details[0].split('&&##&&')
								
								i_practice = a_split_array[0];
								
								i_subsidiary = a_split_array[1];						
								
							}//Practice
						
							var a_split_array = new Array();
							var a_project_details = get_project_data(i_project)
							var i_vertical;
		                    var i_currency;
							var i_exchange_rate;
							
							if(_logValidation(a_project_details))
							{
								a_split_array = a_project_details[0].split('$$$$')
														
								i_vertical = a_split_array[0];
		                        i_currency = a_split_array[1];
								i_exchange_rate = a_split_array[2];
								
							}//Project Details
							
							if(!_logValidation(i_currency))
							{
								i_currency = ''
							}
							if(!_logValidation(i_practice))
							{
								i_practice = ''
							}
							if(!_logValidation(i_vertical))
							{
								i_vertical = ''
							}			
							if(!_logValidation(i_rate))
							{
								i_rate = 0
							}
							if(!_logValidation(i_hours))
							{
								i_hours = 0
							}
								if(!_logValidation(i_employee))
							{
								i_employee = ''
							}	
							if(!_logValidation(i_projectID))
							{
								i_projectID = ''
							}	
							if(!_logValidation(i_date))
							{
								i_date = ''
							}	
							if(!_logValidation(i_amount))
							{
								i_amount = ''
							}	
							if(!_logValidation(i_currency))
							{
								i_currency = ''
							}	
							if(!_logValidation(i_exchange_rate))
							{
								i_exchange_rate = ''
							}	
							if(!_logValidation(i_subsidiary))
							{
								i_subsidiary = ''
							}
						//	nlapiLogExecution('DEBUG', 'schedulerFunction',' i_vertical-->' + i_vertical);
						//	nlapiLogExecution('DEBUG', 'schedulerFunction',' i_currency -->' + i_currency);
								
							var i_amount = parseFloat(i_rate)*parseFloat(i_hours);
							i_amount = parseFloat(i_amount)
							i_amount = i_amount.toFixed(2);
												
							// ========================= Set values ==========================
							
								if ((parseInt(i_user_role) != 3) && (i_user_subsidiary == i_subsidiary)) {
									i_cnt++;
									 i_time_expense_tab.setLineItemValue('custpage_internal_id', (parseInt(i_cnt)), i_internal_ID);
									 i_time_expense_tab.setLineItemValue('custpage_s_no', (parseInt(i_cnt)), (parseInt(i_cnt)).toFixed(0));
									 i_time_expense_tab.setLineItemValue('custpage_employee',(parseInt(i_cnt)),i_employee);
									 i_time_expense_tab.setLineItemValue('custpage_project', (parseInt(i_cnt)),i_project);
									 i_time_expense_tab.setLineItemValue('custpage_vertical', (parseInt(i_cnt)),i_vertical);
									 i_time_expense_tab.setLineItemValue('custpage_practice', (parseInt(i_cnt)),i_practice);
									 i_time_expense_tab.setLineItemValue('custpage_date',(parseInt(i_cnt)),i_date);
									 i_time_expense_tab.setLineItemValue('custpage_hours',(parseInt(i_cnt)),i_hours);
									 i_time_expense_tab.setLineItemValue('custpage_rate',(parseInt(i_cnt)), i_rate);
									 i_time_expense_tab.setLineItemValue('custpage_amount',(parseInt(i_cnt)), i_amount);
									 i_time_expense_tab.setLineItemValue('custpage_currency',(parseInt(i_cnt)),i_currency);					
									 i_time_expense_tab.setLineItemValue('custpage_exchange_rate',(parseInt(i_cnt)),i_exchange_rate);
									 i_time_expense_tab.setLineItemValue('custpage_subsidiary',(parseInt(i_cnt)),i_subsidiary);
							
								}
								else 
									if ((parseInt(i_user_role) == 3)) 
									{
										i_cnt++;
										 i_time_expense_tab.setLineItemValue('custpage_internal_id', (parseInt(i_cnt)), i_internal_ID);
										 i_time_expense_tab.setLineItemValue('custpage_s_no', (parseInt(i_cnt)), (parseInt(i_cnt)).toFixed(0));
										 i_time_expense_tab.setLineItemValue('custpage_employee',(parseInt(i_cnt)),i_employee);
										 i_time_expense_tab.setLineItemValue('custpage_project', (parseInt(i_cnt)),i_project);
										 i_time_expense_tab.setLineItemValue('custpage_vertical', (parseInt(i_cnt)),i_vertical);
										 i_time_expense_tab.setLineItemValue('custpage_practice', (parseInt(i_cnt)),i_practice);
										 i_time_expense_tab.setLineItemValue('custpage_date',(parseInt(i_cnt)),i_date);
										 i_time_expense_tab.setLineItemValue('custpage_hours',(parseInt(i_cnt)),i_hours);
										 i_time_expense_tab.setLineItemValue('custpage_rate',(parseInt(i_cnt)), i_rate);
										 i_time_expense_tab.setLineItemValue('custpage_amount',(parseInt(i_cnt)), i_amount);
										 i_time_expense_tab.setLineItemValue('custpage_currency',(parseInt(i_cnt)),i_currency);					
										 i_time_expense_tab.setLineItemValue('custpage_exchange_rate',(parseInt(i_cnt)),i_exchange_rate);
										 i_time_expense_tab.setLineItemValue('custpage_subsidiary',(parseInt(i_cnt)),i_subsidiary);
							
									}
						   
					  	}
					  }
					  else
					  {
					  	
					  		var a_employee_details = get_practice_expense(i_employee)	
							
							var a_split_array = new Array();
							var i_practice;
							var i_subsidiary;
							if(_logValidation(a_employee_details))
							{
								a_split_array = a_employee_details[0].split('&&##&&')
								
								i_practice = a_split_array[0];
								
								i_subsidiary = a_split_array[1];						
								
							}//Practice
						
							var a_split_array = new Array();
							var a_project_details = get_project_data(i_project)
							var i_vertical;
		                    var i_currency;
							var i_exchange_rate;
						
							if(_logValidation(a_project_details))
							{
								a_split_array = a_project_details[0].split('$$$$')
														
								i_vertical = a_split_array[0];
		                        i_currency = a_split_array[1];
								i_exchange_rate = a_split_array[2];
								
							}//Project Details
							
							if(!_logValidation(i_currency))
							{
								i_currency = ''
							}
							if(!_logValidation(i_practice))
							{
								i_practice = ''
							}
							if(!_logValidation(i_vertical))
							{
								i_vertical = ''
							}			
							if(!_logValidation(i_rate))
							{
								i_rate = 0
							}
							if(!_logValidation(i_hours))
							{
								i_hours = 0
							}
								if(!_logValidation(i_employee))
							{
								i_employee = ''
							}	
							if(!_logValidation(i_projectID))
							{
								i_projectID = ''
							}	
							if(!_logValidation(i_date))
							{
								i_date = ''
							}	
							if(!_logValidation(i_amount))
							{
								i_amount = ''
							}	
							if(!_logValidation(i_currency))
							{
								i_currency = ''
							}	
							if(!_logValidation(i_exchange_rate))
							{
								i_exchange_rate = ''
							}	
							if(!_logValidation(i_subsidiary))
							{
								i_subsidiary = ''
							}
							
							var i_amount = parseFloat(i_rate)*parseFloat(i_hours);
							i_amount = parseFloat(i_amount)
							i_amount = i_amount.toFixed(2);
												
							// ========================= Set values ==========================
							if ((parseInt(i_user_role) != 3) && (i_user_subsidiary == i_subsidiary)) 
							{
								i_cnt++;
								
							i_time_expense_tab.setLineItemValue('custpage_internal_id', (parseInt(i_cnt)), i_internal_ID);
				        	i_time_expense_tab.setLineItemValue('custpage_s_no', (parseInt(i_cnt)), (parseInt(i_cnt)).toFixed(0));
							i_time_expense_tab.setLineItemValue('custpage_employee',(parseInt(i_cnt)),i_employee);
							i_time_expense_tab.setLineItemValue('custpage_project', (parseInt(i_cnt)),i_project);
							i_time_expense_tab.setLineItemValue('custpage_vertical',(parseInt(i_cnt)),i_vertical);
							i_time_expense_tab.setLineItemValue('custpage_practice',(parseInt(i_cnt)),i_practice);
							i_time_expense_tab.setLineItemValue('custpage_date',(parseInt(i_cnt)),i_date);
							i_time_expense_tab.setLineItemValue('custpage_hours',(parseInt(i_cnt)),i_hours);
							i_time_expense_tab.setLineItemValue('custpage_rate',(parseInt(i_cnt)), i_rate);
							i_time_expense_tab.setLineItemValue('custpage_amount',(parseInt(i_cnt)), i_amount);
							i_time_expense_tab.setLineItemValue('custpage_currency',(parseInt(i_cnt)),i_currency);					
							i_time_expense_tab.setLineItemValue('custpage_exchange_rate',(parseInt(i_cnt)),i_exchange_rate);
							i_time_expense_tab.setLineItemValue('custpage_subsidiary',(parseInt(i_cnt)),i_subsidiary);
								
								
							}
							else 
								if ((parseInt(i_user_role) == 3)) 
								{
									i_cnt++;
									
							i_time_expense_tab.setLineItemValue('custpage_internal_id', (parseInt(i_cnt)), i_internal_ID);
				        	i_time_expense_tab.setLineItemValue('custpage_s_no', (parseInt(i_cnt)), (parseInt(i_cnt)).toFixed(0));
							i_time_expense_tab.setLineItemValue('custpage_employee',(parseInt(i_cnt)),i_employee);
							i_time_expense_tab.setLineItemValue('custpage_project', (parseInt(i_cnt)),i_project);
							i_time_expense_tab.setLineItemValue('custpage_vertical',(parseInt(i_cnt)),i_vertical);
							i_time_expense_tab.setLineItemValue('custpage_practice',(parseInt(i_cnt)),i_practice);
							i_time_expense_tab.setLineItemValue('custpage_date',(parseInt(i_cnt)),i_date);
							i_time_expense_tab.setLineItemValue('custpage_hours',(parseInt(i_cnt)),i_hours);
							i_time_expense_tab.setLineItemValue('custpage_rate',(parseInt(i_cnt)), i_rate);
							i_time_expense_tab.setLineItemValue('custpage_amount',(parseInt(i_cnt)), i_amount);
							i_time_expense_tab.setLineItemValue('custpage_currency',(parseInt(i_cnt)),i_currency);					
							i_time_expense_tab.setLineItemValue('custpage_exchange_rate',(parseInt(i_cnt)),i_exchange_rate);
							i_time_expense_tab.setLineItemValue('custpage_subsidiary',(parseInt(i_cnt)),i_subsidiary);
							
									
									
									
								}
						    
					  	
					  }//Internal ID
						
					var i_usage_end = i_context.getRemainingUsage();
	             // nlapiLogExecution('DEBUG', 'time_tracking_details','Usage End  -->' + i_usage_end);
			
					if(i_usage_end<30)
					{
						break;
					}	
						
					}//Loop Search Results	
					
				}//Search Results 3*/
			}
					
				}//Validation - IF Records , Item & Sales Order
				
			}//UNBILLED TIME						
          nlapiLogExecution('DEBUG', 'POST suiteletFunction');	
		   nlapiLogExecution('DEBUG', 'i_criteria','i_criteria' + i_criteria);
		   if(s_script_status == 'Complete')
		   {
		   
		   	   
		      //--------------------------------------------------------------------------
			   
			    var i_time_provision_f = f_form.addField('custpage_time_provision', 'textarea', 'Time Provision Records Created ').setDisplayType('inline');	 
			   
				
			   
		     // var i_deleteID = nlapiDeleteRecord('customrecord_provision_processed_records', i_custom_id);
	          nlapiLogExecution('DEBUG', 'i_criteria','i_criteria' + i_criteria);
			if(i_unbilled_type == 1)
				{
				   //var formSavedLabel = form.addField('custpage_time_provision_link', 'inlinehtml');
					//formSavedLabel.setDefaultValue("<html><body>Click <a href='https://system.sandbox.netsuite.com/app/common/custom/custrecordentrylist.nl?rectype=172'>here</a> to go to Time Bill Record.</body></html>");
			 
					//leadSourceField.setDisplayType('inline');
					
				 i_time_provision_f.setDefaultValue('https://system.na1.netsuite.com/app/common/custom/custrecordentrylist.nl?rectype=156')
				}
			else
				{
				 f_form.addButton('custpage_refresh','Show Journal Entry','Show_Journal_Entry()');
		     i_time_provision_f.setDefaultValue('https://system.na1.netsuite.com/app/common/custom/custrecordentrylist.nl?rectype=172')
				}
			
		   }
		    //-----------------------------------fill count field-----------------------------------------------------------------------
			
			//--------------------------------------------------------------------------------------------------------------------------
		   
		
			f_form.addButton('custpage_refresh','REFRESH','refresh()');
			f_form.addButton('custpage_export','EXPORT EXCEL','export_excel()');
		//	nlapiLogExecution('DEBUG', 's_script_status',s_script_status);	
			if(s_script_status != 'Pending' && s_script_status != 'Processing'&& s_script_status != 'Deferred')
			{	
				f_form.addSubmitButton('SUBMIT');
           //   nlapiLogExecution('DEBUG', 'inside if');				
			f_form.addButton('custpage_markall','MARK ALL','mark_all()');
			f_form.addButton('custpage_unmarkall','UNMARK ALL','unmark_all()');
			f_form.addButton('custpage_go_home_page','Go to Home Page','redirect_to_home_page()');
			
			}
			else
				{
				
				}
			
				
		
		    response.writePage(f_form);
		}//GET
		else if (request.getMethod() == 'POST') 	
		{		
			 var i_data_array =  request.getParameter('custpage_data');		
			 var i_current_date =  request.getParameter('custpage_current_date');
			 //----------------------------------------------------------------------------------
			 var i_from_date =  request.getParameter('custpage_from_date');
			 var i_to_date =  request.getParameter('custpage_to_date');
			 var i_month =  request.getParameter('custpage_d_month');
			 var i_year =  request.getParameter('custpage_d_year');
			 //--------------------------------------------------------------------------------------
			 var i_unbilled_type =   request.getParameter('custpage_unbilled_type');
			 var i_unbilled_receivable_GL =  request.getParameter('custpage_unbilled_receivable_gl');
			 var i_unbilled_revenue_GL =  request.getParameter('custpage_unbilled_revenue_gl');
			 var i_reverse_date =  request.getParameter('custpage_reverse_date');
			 var i_process_data =  request.getParameter('custpage_data_process');
			 var i_custom_id =  request.getParameter('custpage_custom_id');
			 var i_data_criteria =  request.getParameter('custpage_data_criteria');
			 var i_criteria =  request.getParameter('custpage_criteria_f');
			 var i_date_created =  request.getParameter('custpage_d_date_created');
						 
			 
			 var i_user_role =  request.getParameter('custpage_user_role');
			 var i_user_subsidiary =  request.getParameter('custpage_user_subsidiary');
				// nlapiLogExecution('DEBUG', 'POST suiteletFunction',' Criteria -->' + i_criteria);	
				  nlapiLogExecution('DEBUG', 'POST suiteletFunction',' Unbilled Type -->' + i_unbilled_type);
				 
			/* nlapiLogExecution('DEBUG', 'POST suiteletFunction',' Date Created-->' + i_date_created);																	
			 nlapiLogExecution('DEBUG', 'POST suiteletFunction',' Current Date -->' + i_current_date);

			 nlapiLogExecution('DEBUG', 'POST suiteletFunction',' From Date -->' + i_from_date);	
			 nlapiLogExecution('DEBUG', 'POST suiteletFunction',' To Date -->' + i_to_date);	
 
			
			 nlapiLogExecution('DEBUG', 'POST suiteletFunction',' Unbilled Receivable GL -->' + i_unbilled_receivable_GL);
			 nlapiLogExecution('DEBUG', 'POST suiteletFunction',' Unbilled Revenue GL -->' + i_unbilled_revenue_GL);
			 nlapiLogExecution('DEBUG', 'POST suiteletFunction',' Reverse Date -->' + i_reverse_date);	
			 nlapiLogExecution('DEBUG', 'POST suiteletFunction',' Data Array  -->' + i_data_array);	
			 nlapiLogExecution('DEBUG', 'POST suiteletFunction',' Data Array Length-->' + i_data_array.length);	
			 nlapiLogExecution('DEBUG', 'POST suiteletFunction',' Process Data -->' + i_process_data);	
			 nlapiLogExecution('DEBUG', 'POST suiteletFunction',' Custom ID -->' + i_custom_id);	
			 nlapiLogExecution('DEBUG', 'POST suiteletFunction',' Data Criteria -->' + i_data_criteria);	
			 nlapiLogExecution('DEBUG', 'POST suiteletFunction',' Criteria -->' + i_criteria);	
			 nlapiLogExecution('DEBUG', 'POST suiteletFunction',' User Role -->' + i_user_role);	
			 nlapiLogExecution('DEBUG', 'POST suiteletFunction',' User Subsidiary -->' + i_user_subsidiary);	*/
			
			
			
			 var params_1=new Array();
		     params_1['custscript_current_date'] = i_current_date
			 //------------------------------------------------------
			  params_1['custscript_from_date'] = i_from_date
			   params_1['custscript_to_date'] = i_to_date
			 //--------------------------------------------------------
			 params_1['custscript_unbilled_type'] =i_unbilled_type;
			 params_1['custscript_unbilled_receivable_gl'] = i_unbilled_receivable_GL
			 params_1['custscript_unbilled_revenue_gl'] = i_unbilled_revenue_GL
			 params_1['custscript_reverse_date'] = i_reverse_date
			 //params_1['custpage_data'] = i_data_array
			 params_1['custscript_user_role'] = i_user_role
			 params_1['custscript_user_subsidiary'] = i_user_subsidiary
			 params_1['custscript_date_created'] = i_date_created
			 params_1['custscript_provision_creation_criteria'] = i_criteria
			 params_1['custscript_month'] = i_month
			 params_1['custscript_year'] = i_year	
			 
			 
										 	
			 var params=new Array();
		 	 params['custscript_time_counter']='0';
		     params['custscript_current_date_sch'] = i_current_date
			//----------------------------------------------------------------
			  params['custscript_from_date_sch'] = i_from_date
			  params['custscript_to_date_sch'] = i_to_date
			 //----------------------------------------------------------------
			 params['custscript_unbilled_type_sch'] = i_unbilled_type
			 params['custscript_unbilled_receivable_sch'] = i_unbilled_receivable_GL
			 params['custscript_unbilled_revenue_sch'] = i_unbilled_revenue_GL
			 params['custscript_reverse_date_sch'] = i_reverse_date
			 params['custscript_data_array'] = i_data_array
			 params['custscript_data_process_provision'] = i_process_data
			 params['custscript_custom_id_process'] = i_custom_id
			 params['custscript_data_criteria'] = i_data_criteria
			 params['custscript_criteria_type_sch'] = i_criteria			 
			 	
			 var status=nlapiScheduleScript('customscriptsch_provision_unbilled_journ',null,params);
			 nlapiLogExecution('DEBUG', 'suiteletFunction', ' Status -->' + status);
										
			nlapiSetRedirectURL('suitelet','customscript_provision_unbilled_journal','customdeploy1', null,params_1 );//original
			
			//nlapiSetRedirectURL('suitelet','SUT_Show_Suitelet_Screen_Data','customdeploy1', null,params_1);
			
		   // nlapiLogExecution('DEBUG', 'suiteletFunction', ' Suitelet Called ........... ' );    
				
			//response.write('<p><br/><br/><br/> Selected enties are processed . <\/p>')	
								
		}//POST		
		
	}//TRY
	catch(exception)
	{
		nlapiLogExecution('DEBUG', 'ERROR',' Exception Caught -->' + exception);	
					
	}//CATCH

}

// END SUITELET ====================================================




// BEGIN OBJECT CALLED/INVOKING FUNCTION ===================================================
/**
 * 
 * @param {Object} value
 * 
 * Description --> If the value is blank /null/undefined returns false else returns true
 */
function _logValidation(value) 
{
 if(value != null && value.toString() != null && value != '' && value != undefined && value.toString() != undefined && value != 'undefined' && value.toString() != 'undefined'&& value.toString() != 'NaN' && value != NaN) 
 {
  return true;
 }
 else 
 { 
  return false;
 }
}
function get_vertical(i_projectID)
{
  var i_vertical;	
 	
  if(_logValidation(i_projectID))
  {
  	var filter = new Array();
    filter[0] = new nlobjSearchFilter('internalid', null, 'is',i_projectID);
	
	var columns = new Array();
    columns[0] = new nlobjSearchColumn('custentity_vertical');
		
	 var a_search_results = searchRecord('job',null,filter,columns);
	 
	 if(_logValidation(a_search_results))
	 {
	 	 i_vertical = a_search_results[0].getValue('custentity_vertical');
	
	 }//Search Results
		
  }//Employee
   
  return i_vertical;	
	
}
function get_project_data(i_projectID)
{
  var i_vertical;	
  var i_currency;
  var i_exchange_rate;
  var i_subsidiary;
  var a_return_array = new Array();
	
  if(_logValidation(i_projectID))
  {
  	 var o_projectOBJ = nlapiLoadRecord('job',i_projectID)
	
	 if(_logValidation(o_projectOBJ))
	 {
	 	 i_vertical = o_projectOBJ.getFieldValue('custentity_vertical');
		 i_currency = o_projectOBJ.getFieldValue('currency');
		// nlapiLogExecution('DEBUG','i_currency swati test',i_currency);
		 i_exchange_rate = o_projectOBJ.getFieldValue('fxrate');
		 i_subsidiary = o_projectOBJ.getFieldValue('subsidiary');		 
		 
	 }//Search Results
		
  }//Employee
  
  a_return_array[0] = i_vertical+'$$$$'+i_currency+'$$$$'+i_exchange_rate+'$$$$'+i_subsidiary
 		
  return a_return_array;	
	
}
function get_practice(i_employee)
{
	
  var i_practice;
  var i_subsidiary;	
  var a_return_array = new Array();
	
  if(_logValidation(i_employee))
  {
  	var filter = new Array();
    filter[0] = new nlobjSearchFilter('internalid', null, 'is',i_employee);
	
	var columns = new Array();
    columns[0] = new nlobjSearchColumn('internalid');
	columns[1] = new nlobjSearchColumn('department');
	columns[2] = new nlobjSearchColumn('subsidiary');
	
	 var a_search_results = searchRecord('employee',null,filter,columns);
	 
	 if(_logValidation(a_search_results))
	 {
	 	 i_practice = a_search_results[0].getValue('department');
		 
		 i_subsidiary = a_search_results[0].getValue('subsidiary');
		
		 a_return_array[0] = i_practice+'&&##&&'+i_subsidiary;
		 	
	 }//Search Results
	
  }//Employee
  return a_return_array;
}

function get_practice_expense(i_employee)
{
  var i_practice;
  var i_subsidiary;	
  var a_return_array = new Array();	
	
  if(_logValidation(i_employee))
  {
  	var filter = new Array();
    filter[0] = new nlobjSearchFilter('internalid', null, 'is',i_employee);
	
	var columns = new Array();
    columns[0] = new nlobjSearchColumn('internalid');
	columns[1] = new nlobjSearchColumn('department');
	columns[2] = new nlobjSearchColumn('subsidiary');
	
	 var a_search_results = searchRecord('employee',null,filter,columns);
	 
	 if(_logValidation(a_search_results))
	 {
	 	 i_practice = a_search_results[0].getValue('department');
		 
		 i_subsidiary = a_search_results[0].getValue('subsidiary');
		 
		 a_return_array[0] = i_practice+'&&##&&'+i_subsidiary;
			
	 }//Search Results
	
  }//Employee
  return a_return_array;
}

function search_custom_recordID(i_employee,i_criteria)
{
var a_rec_arr = new Array();
    if(i_criteria == '3')
	{
    var load_rec=nlapiLoadRecord('customrecord_provision_processed_records','84')
		  
	var data_pro_arr= load_rec.getFieldValue('custrecord_processed_records_provision');
	a_rec_arr.push(data_pro_arr);
	}
	else{
			//----------------------------------------------------
			
			 nlapiLogExecution('DEBUG', 'i_employee', 'i_employee ==' +i_employee);
			if(_logValidation(i_employee))
			{
			var filter = new Array();
			filter[0] = new nlobjSearchFilter('custrecord_employee_id_provision', null,'is',i_employee);
			
			var columns = new Array();
			columns[0] = new nlobjSearchColumn('internalid');
			columns[1] = new nlobjSearchColumn('custrecord_processed_records_provision');
			
			 var a_search_results = searchRecord('customrecord_provision_processed_records',null,filter,columns);
			 
			 if(_logValidation(a_search_results))
			 {
				for(var i=0;i<a_search_results.length;i++)
				{
					i_custom_recID = a_search_results[i].getValue('internalid');
					i_processed_records = a_search_results[i].getValue('custrecord_processed_records_provision');
					
					nlapiLogExecution('DEBUG', 'search_custom_recordID', 'Custom Rec ID ==' +i_custom_recID);
			 
					nlapiLogExecution('DEBUG', 'search_custom_recordID', 'Processed Records ==' +i_processed_records);
			 
					a_rec_arr.push(i_processed_records);
				}
				 
			 }//Search Results	
			
			}
	}
	return a_rec_arr;
}

function search_time_expense_provision_records()
{
	var a_recordID_arr = new Array();
	
	var filter = new Array();
    filter[0] = new nlobjSearchFilter('custrecord_criteria_type_t_e_p',null,'is',3);
	//filter[1] = new nlobjSearchFilter('custrecord_journal_entry_reverse_date',null,'isnot',get_todays_date());
	
	
	
//	filter[1] = new nlobjSearchFilter('custrecord_journalentry','reversaldate','isnot',get_todays_date());
	
	var columns = new Array();
    columns[0] = new nlobjSearchColumn('internalid');
	
	var a_search_results = searchRecord('customrecord_timeexpenseprovision',null,filter,columns);
		
	if(_logValidation(a_search_results))
	{
		for(var i=0;i<a_search_results.length;i++)
		{
			var i_recordID = a_search_results[i].getValue('internalid');			
			nlapiLogExecution('DEBUG', 'search_time_expense_provision_records', 'Record ID -->' +i_recordID);
	 
			a_recordID_arr.push(i_recordID);			
		}//Loop	
		nlapiLogExecution('DEBUG', 'search_time_expense_provision_records', 'Record ID Array -->' +a_recordID_arr);
	}//Search Results	
	return a_recordID_arr;	
}//Time n Expense Provision Records

function get_todays_date()
{
	var today;
	// ============================= Todays Date ==========================================
		
	  var date1 = new Date();
     // nlapiLogExecution('DEBUG', 'schedulerFunction', 'Date ==' + date1);

      var offsetIST = 5.5;

    //To convert to UTC datetime by subtracting the current Timezone offset
     var utcdate =  new Date(date1.getTime() + (date1.getTimezoneOffset()*60000));
     nlapiLogExecution('DEBUG', 'schedulerFunction', 'UTC Date' + utcdate);

    //Then cinver the UTS date to the required time zone offset like back to 5.5 for IST
     var istdate =  new Date(utcdate.getTime() - ((-offsetIST*60)*60000));
   nlapiLogExecution('DEBUG', 'schedulerFunction', 'IST Date ==' + istdate);
    
     var day = istdate.getDate();
    nlapiLogExecution('DEBUG', 'schedulerFunction', 'Day ==' + day);
	 
     var month = istdate.getMonth()+1;
     nlapiLogExecution('DEBUG', 'schedulerFunction', 'Month ==' + month);

	 var year=istdate.getFullYear();
	 nlapiLogExecution('DEBUG', 'schedulerFunction', 'Year ==' +year);
	  
	 var date_format = checkDateFormat();
	 nlapiLogExecution('DEBUG', 'schedulerFunction', 'Date Format ==' +date_format);
	 
	 if (date_format == 'YYYY-MM-DD')
	 {
	 	today = year + '-' + month + '-' + day;		
	 } 
	 if (date_format == 'DD/MM/YYYY') 
	 {
	  	today = day + '/' + month + '/' + year;
	 }
	 if (date_format == 'MM/DD/YYYY')
	 {
	  	today = month + '/' + day + '/' + year;
	 }	 
	 return today;
}
function checkDateFormat()
{
    var context = nlapiGetContext();

	var dateFormatPref = context.getPreference('dateformat');

    return dateFormatPref;
}

function get_invoice_created_details(i_expenseID)
{
	var i_invoice_created = false;
	if(_logValidation(i_expenseID))
	{
		var o_expenseOBJ = nlapiLoadRecord('expensereport',i_expenseID)
		
		if(_logValidation(o_expenseOBJ))
		{
			var i_reimbursements_count = o_expenseOBJ.getLineItemCount('reimbursements');
			nlapiLogExecution('DEBUG', 'get_invoice_created_details', 'Reimbursements Count --> ' + i_reimbursements_count);
	 
			if(_logValidation(i_reimbursements_count))
			{
				for(var i = 1;i<=i_reimbursements_count;i++)
				{
					var s_linkurl = o_expenseOBJ.getLineItemValue('reimbursements','linkurl',i)
					nlapiLogExecution('DEBUG', 'get_invoice_created_details', 'Link URL --> ' + s_linkurl);
	     
					if(s_linkurl.indexOf('custinvc')>-1)
					{
						nlapiLogExecution('DEBUG', 'get_invoice_created_details', 'Invoice has been created .....');
						i_invoice_created = true;
						break;
	     
					}//Invoice Created					
				}//Loop				
			}//Reimbursements Count			
		}//Expense OBJ		
	}//Expense ID	
	return i_invoice_created;
}//Invoice Created Details

function get_todays_date()
{
	var today;
	// ============================= Todays Date ==========================================
		
	  var date1 = new Date();
     // nlapiLogExecution('DEBUG', 'schedulerFunction', 'Date ==' + date1);

      var offsetIST = 5.5;

    //To convert to UTC datetime by subtracting the current Timezone offset
     var utcdate =  new Date(date1.getTime() + (date1.getTimezoneOffset()*60000));
     //nlapiLogExecution('DEBUG', 'schedulerFunction', 'UTC Date' + utcdate);

    //Then cinver the UTS date to the required time zone offset like back to 5.5 for IST
     var istdate =  new Date(utcdate.getTime() - ((-offsetIST*60)*60000));
    // nlapiLogExecution('DEBUG', 'schedulerFunction', 'IST Date ==' + istdate);
    
     var day = istdate.getDate();
     //nlapiLogExecution('DEBUG', 'schedulerFunction', 'Day ==' + day);
	 
     var month = istdate.getMonth()+1;
    // nlapiLogExecution('DEBUG', 'schedulerFunction', 'Month ==' + month);

	 var year=istdate.getFullYear();
	// nlapiLogExecution('DEBUG', 'schedulerFunction', 'Year ==' +year);
	  
	 var date_format = checkDateFormat();
	// nlapiLogExecution('DEBUG', 'schedulerFunction', 'Date Format ==' +date_format);
	 
	 if (date_format == 'YYYY-MM-DD')
	 {
	 	today = year + '-' + month + '-' + day;		
	 } 
	 if (date_format == 'DD/MM/YYYY') 
	 {
	  	today = day + '/' + month + '/' + year;
	 }
	 if (date_format == 'MM/DD/YYYY')
	 {
	  	today = month + '/' + day + '/' + year;
	 }	 
	 return today;
}
function checkDateFormat()
{
    var context = nlapiGetContext();

	var dateFormatPref = context.getPreference('dateformat');

    return dateFormatPref;
}

function get_duplicate_details(i_employee,i_date,i_project)
{
	var i_recordID;
	
	// nlapiLogExecution('DEBUG', 'get_duplicate_details', 'i_employee-->' +i_employee);
	 // nlapiLogExecution('DEBUG', 'get_duplicate_details', 'i_date -->' +i_date);
	 //  nlapiLogExecution('DEBUG', 'get_duplicate_details', ' i_project -->' +i_project);
	
	
  if(_logValidation(i_employee)&&_logValidation(i_date)&&_logValidation(i_project))
  {
  	var filter = new Array();
 //   filter[0] = new nlobjSearchFilter('date', null, 'is',i_date);
	filter[0] = new nlobjSearchFilter('employee', null, 'is',i_employee);
	filter[1] = new nlobjSearchFilter('customer', null, 'is',i_project);
	filter[2] = new nlobjSearchFilter('type', null, 'is','A');
		
	var columns = new Array();
    columns[0] = new nlobjSearchColumn('internalid');
	columns[1] = new nlobjSearchColumn('type');
	
	
	
	var a_search_results = searchRecord('timebill',null,filter,columns);
	// nlapiLogExecution('DEBUG', 'get_duplicate_details', ' Search Results Length -->' +a_search_results.length);
	 if(_logValidation(a_search_results))
	 {
	 	 i_recordID = a_search_results[0].getValue('internalid');
		// nlapiLogExecution('DEBUG', 'get_duplicate_details', ' Duplicate Details -->' +i_recordID);
		 
		 i_type = a_search_results[0].getValue('type');
		// nlapiLogExecution('DEBUG', 'get_duplicate_details', ' i_types -->' +i_type);
	 						
	 }//Search Results
	
  }//Employee
	
	
	
	
	
}//Duplicate Details



// END OBJECT CALLED/INVOKING FUNCTION =====================================================
//--------------------------------------------------------------------------------------------------------------
function count_val(f_form,i_count,fld)
{
				
				  var count_array=new Array();
				  var sum=100;
				 for(var ss=0 ; ss<i_count ; ss++)
				  {
					count_array[ss]=i_count;
				 
					
					if(i_count < 97)
					{
					break;
					}
					else
					{
					   i_count=i_count-97
					   nlapiLogExecution('DEBUG','i_count','i_count-->'+i_count);
					}
					
				  }
					//nlapiLogExecution('DEBUG','count_array','count_array-->'+count_array.length);
					
					for(var sk=0,k=1 ; sk<count_array.length ; sk++,k++)
					{
						

						if(k == count_array.length)
							{
							fld.addSelectOption(count_array[sk], '0'+'to'+count_array[sk]);
						}
						else{
						var previous=count_array[sk+1];
						fld.addSelectOption(count_array[sk], previous+'to'+count_array[sk]);
						}
						//}
						//nlapiLogExecution('DEBUG','sk','sk-->'+sk);
					}
}
