/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       04 Aug 2015     nitish.mishra
 *
 */

/**
 * @param {nlobjRequest}
 *            request Request object
 * @param {nlobjResponse}
 *            response Response object
 * @returns {Void} Any output is written via response object
 */
function suitelet(request, response) {
	var form = nlapiCreateForm('Auto Invoice');
	form.setScript('customscript_cs_sut_auto_invoice_status');

	var totalInvoice = request.getParameter('t');

	form.addField('custpage_total_invoice', 'integer', 'Total Invoice')
			.setDisplayType('inline').setDefaultValue(totalInvoice);

	var invoiceDoneField = form.addField('custpage_invoice_done', 'integer',
			'Invoice Done').setDisplayType('inline');

	var details = nlapiSearchRecord('scheduledscriptinstance', null, [
			new nlobjSearchFilter('internalid', 'script', 'anyof', '599'),
			new nlobjSearchFilter('internalid', 'scriptdeployment', 'anyof',
					'1864'),
			new nlobjSearchFilter('status', null, 'anyof', [ 'PENDING',
					'PROCESSING' ]) ], [
			new nlobjSearchColumn('percentcomplete'),
			new nlobjSearchColumn('status') ]);

	if (details) {
		nlapiLogExecution('debug', 'details.length', details.length);
	}

	var percentCompleteField = form.addField('custpage_percent_complete',
			'percent', "Percentage Complete");
	percentCompleteField.setDisplayType('inline');

	var statusField = form.addField('custpage_status', 'text', "Status");
	statusField.setDisplayType('inline');

	if (details) {
		var lastIndex = details.length - 1;
		var percentComplete = details[lastIndex].getValue('percentcomplete');
		percentCompleteField.setDefaultValue(percentComplete);
		statusField.setDefaultValue(details[lastIndex].getValue('status'));

		percentComplete = parseFloat(percentComplete.split('%')[0].trim());
		var invoicesDone = parseInt((percentComplete / 100) * totalInvoice);
		invoiceDoneField.setDefaultValue(invoicesDone);
	}

	form.addButton('custpage_btn_refresh', 'Refresh', "refresh");
	form.addButton('custpage_btn_go_back', 'Go Back', "goBack");

	response.writePage(form);
}

function refresh() {
	window.location.reload();
}

function goBack() {
	var url = nlapiResolveURL('SUITELET',
			'customscript_sut_auto_invoicing_weekly',
			'customdeploy_sut_auto_invoicing_weekly');
	window.location = url;
}
