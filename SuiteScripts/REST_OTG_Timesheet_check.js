/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       09 Aug 2017     deepak.srinivas
 *
 */	

/**
 * @param {Object} dataIn Parameter object
 * @returns {Object} Output object
 */
var days = ['sunday','monday','tuesday','wednesday','thursday','friday','saturday'];
var daysList = ['Su','Mo','Tu','We','Th','Fr','Sa'];
function postRESTlet(dataIn) {
	
	try{
		var response = new Response(); 
		var checkTS = null;
		var flag =false;
		var current_date = nlapiDateToString(new Date());
		//Log for current date
		nlapiLogExecution('DEBUG', 'Current Date', 'current_date...' + current_date)
		nlapiLogExecution('debug', 'dataIn', JSON.stringify(dataIn));
		
		/*var employeeId =  getUserUsingEmailId(dataIn.EmailId);
		var requestType = dataIn.RequestType;
		var weekStartDate = dataIn.Data ? dataIn.Data.WeekStartDate : ''; */

		var employeeId =  getUserUsingEmailId('uma.ramesh@brillio.com');
		var requestType = 'GET';
		var weekStartDate = '09/06/2019';
		
		//For Submit Function
		if(requestType == 'SUBMIT')
		{
		var timeSheet_Id = dataIn.Data[0].TimeSheetDetails[0].Body[0].TimeSheetID;
		var weekStartDate = dataIn.Data[0].TimeSheetDetails[0].Body[0].WeekStartDate;
		//var timeSheetDetails = dataIn.Data[0].TimeSheetDetails;
		//nlapiLogExecution('debug', 'timeSheetDetails', timeSheetDetails);
		//var timesheetDetails_t = timeSheetDetails[0].Body;
		//nlapiLogExecution('debug', 'timesheetDetails_t Body', timesheetDetails_t);
		//var timeSheet_Id = timesheetDetails_t[0].TimeSheetID;
		nlapiLogExecution('debug', 'timeSheet_Id', timeSheet_Id);
		}
		/*var employeeId =  30237;
		var requestType = 'NEXTWEEK';
		var weekStartDate = '9/3/2017';*/
		
		if(weekStartDate){
		//Get Week StartDate
		var weekStDate = nlapiStringToDate(weekStartDate);
		//var curr = new Date();
		/*var day = weekStDate.getDay();
		var firstday = new Date(weekStDate.getTime() - 60*60*24* day*1000); 
		nlapiLogExecution('DEBUG', 'firstday', 'firstday...' + firstday);
		weekStartDate = nlapiDateToString(firstday);
		nlapiLogExecution('DEBUG', 'firstday', 'weekstart_date...' + weekStartDate); */
		
		//
		//var date1 = new Date();
	//nlapiLogExecution('DEBUG', 'schedulerFunction', 'Date ==' + date1);
	
	date1 = weekStDate;
	
	var offsetIST = 5.5;
	
	//To convert to UTC datetime by subtracting the current Timezone offset
	var utcdate = new Date(date1.getTime() + (date1.getTimezoneOffset() * 60000));
	//nlapiLogExecution('DEBUG', 'schedulerFunction', 'UTC Date' + utcdate);
	
	//Then cinver the UTS date to the required time zone offset like back to 5.5 for IST
	var istdate = new Date(utcdate.getTime() - ((-offsetIST * 60) * 60000));
	//nlapiLogExecution('DEBUG', 'schedulerFunction', 'IST Date ==' + istdate);
		var day = istdate.getDay();
		var firstday = new Date(istdate.getTime() - 60*60*24* day*1000); 
		nlapiLogExecution('DEBUG', 'firstday', 'firstday...' + firstday);
		weekStartDate = nlapiDateToString(firstday);
		nlapiLogExecution('DEBUG', 'firstday', 'weekstart_date...' + weekStartDate);
		
		
		}
		
		var timesheet_iD = '';
		if(requestType == 'GET' || requestType == 'NEXTWEEK'){
			if(weekStartDate && employeeId){
			checkTS = validateTS(employeeId,weekStartDate);
			if(checkTS.length > 0){
				flag =true;
			}
			}
		}
		//Test Data
		/*var employeeId = 2685;
		var requestType = 'NEXTWEEK';
		var weekStartDate = '8/6/2017';*/
		
		var endDate = nlapiDateToString(nlapiAddDays(nlapiStringToDate(weekStartDate), 6), 'date');
		var timeSheetID = dataIn.timeSheetid;
		var autoFlag = dataIn.Data ? dataIn.Data.AutoFillFlag : '';
		switch (requestType) {

		case M_Constants.Request.Get:

			if (employeeId && weekStartDate && flag == false) {
				response.Data = allocationDetails(dataIn,employeeId,weekStartDate,endDate);
				response.Status = true;
			} 
			else if(employeeId && weekStartDate && flag == true ){
				response.Data = getTimesheetData(dataIn,employeeId,weekStartDate,endDate,checkTS);
				response.Status = true;
			}
			else {
				response.Data = "Some error with the data sent";
				response.Status = false;
		}
			break;
		case M_Constants.Request.Submit:

			if (timeSheet_Id && employeeId) {
				response.Data = submitTimeSheets(dataIn,timeSheet_Id,employeeId); 
				response.Status = true;
			} else {
				response.Data = "Some error with the data sent";
				response.Status = false;
		}
			break;	
		case M_Constants.Request.NextWeek:

			/*if (employeeId && flag == false) {
				response.Data = getNextWeekData(dataIn,employeeId,weekStartDate,endDate,autoFlag);
				response.Status = true;
			}*/
			if (employeeId && weekStartDate && flag == false)  {
			response.Data = allocationDetails(dataIn,employeeId,weekStartDate,endDate);
			response.Status = true;
			}
			else if(employeeId && weekStartDate && flag == true ){
				response.Data = getTimesheetData(dataIn,employeeId,weekStartDate,endDate,checkTS);
				response.Status = true;
			}
			else {
				response.Data = "Some error with the data sent";  
				response.Status = false;
		}
			break;	
	}
	}
	catch(err){
		nlapiLogExecution('error', 'Restlet Submit Function', err);
		response.Data = err;
		response.Status = false;
	}
	nlapiLogExecution('debug', 'response', JSON.stringify(response));
	return response;
}
//Submit on Timesheet
function submitTimeSheets(dataIn,timeSheetID,employee) {
	try{
		var status_ts= '';
		var timesheetID = dataIn.Data[0].TimeSheetDetails[0].Body[0].TimeSheetID;
		
		nlapiLogExecution('DEBUG','Update TimesheetID',timesheetID);
		var loadTS = nlapiLoadRecord('timesheet',timesheetID,{recordmode: 'dynamic'});
		var approvalstatus = loadTS.getFieldText('approvalstatus');
		nlapiLogExecution('DEBUG','Update Timesheet approvalstatus',approvalstatus);
		//Edit will work only for only 2-statuses
		if(approvalstatus == 'Open' || approvalstatus == 'Rejected'){
			
			//Removing Line Items
			var lineCount = loadTS.getLineItemCount('timegrid');
			nlapiLogExecution('DEBUG','Update Timesheet Line Length',lineCount);
			for(var n=lineCount ; n>=1; n--){
				loadTS.removeLineItem('timegrid',n);
				/*for(var day=0;day<7;day++){
					var subDay = days[day];
					var r_sub_record_view = loadTS.viewCurrentLineItemSubrecord('timegrid', subDay);
				if(r_sub_record_view)
				loadTS.removeCurrentLineItemSubrecord('timegrid', sub_day)
				}*/
			}
			nlapiLogExecution('DEBUG','TImeSheet Lines Removed');
			//Create New Sublists
			var serviceItemList_1 = dataIn.Data[0].TimeSheetDetails[0].Lines[0].ServiceItems_1;
			var serviceItemList_2 = dataIn.Data[0].TimeSheetDetails[0].Lines[0].ServiceItems_2; //ServiceItems_1
			var serviceItemList_3 = dataIn.Data[0].TimeSheetDetails[0].Lines[0].ServiceItems_3;
			var serviceItemList_4 = dataIn.Data[0].TimeSheetDetails[0].Lines[0].ServiceItems_4;
			var serviceItemList_5 = dataIn.Data[0].TimeSheetDetails[0].Lines[0].ServiceItems_5;
			var serviceItemList_6 = dataIn.Data[0].TimeSheetDetails[0].Lines[0].ServiceItems_6;
			//nlapiLogExecution('DEBUG','serviceItemList Length',serviceItemList);
			var daysOfWeek = [ 'sunday', 'monday', 'tuesday', 'wednesday',
			   				'thursday', 'friday', 'saturday' ];
			var otg_days = ['Su','Mo','Tu','We','Th','Fr','Sa'];
			var i_otg_days = ['0','1','2','3','4','5','6'];
			
			var holiday_list = dataIn.Data[0].TimeSheetDetails[0].Lines[0].Holiday_1;
			var holiday_list_2 = dataIn.Data[0].TimeSheetDetails[0].Lines[0].Holiday_2;
			var holiday_list_3 = dataIn.Data[0].TimeSheetDetails[0].Lines[0].Holiday_3;
			var holiday_list_4 = dataIn.Data[0].TimeSheetDetails[0].Lines[0].Holiday_4;
			var holiday_list_5 = dataIn.Data[0].TimeSheetDetails[0].Lines[0].Holiday_5;
			var holiday_list_6 = dataIn.Data[0].TimeSheetDetails[0].Lines[0].Holiday_6;
			if(holiday_list)
			nlapiLogExecution('DEBUG','holiday_list Length',holiday_list.length);
			var leave_list = dataIn.Data[0].TimeSheetDetails[0].Lines[0].Leave_1;
			var leave_list_2 = dataIn.Data[0].TimeSheetDetails[0].Lines[0].Leave_2;
			var leave_list_3 = dataIn.Data[0].TimeSheetDetails[0].Lines[0].Leave_3;
			var leave_list_4 = dataIn.Data[0].TimeSheetDetails[0].Lines[0].Leave_4;
			var leave_list_5 = dataIn.Data[0].TimeSheetDetails[0].Lines[0].Leave_5;
			var leave_list_6 = dataIn.Data[0].TimeSheetDetails[0].Lines[0].Leave_6;
			var ot_list = dataIn.Data[0].TimeSheetDetails[0].Lines[0].OT_1;
			var ot_list_2 = dataIn.Data[0].TimeSheetDetails[0].Lines[0].OT_2;
			var ot_list_3 = dataIn.Data[0].TimeSheetDetails[0].Lines[0].OT_3;
			var ot_list_4 = dataIn.Data[0].TimeSheetDetails[0].Lines[0].OT_4;
			var ot_list_5 = dataIn.Data[0].TimeSheetDetails[0].Lines[0].OT_5;
			
			
			//Service Item_1
			if(_logValidation(serviceItemList_1)){
			for(var len=0;len<serviceItemList_1.length;len++){
				var day_d_1 = serviceItemList_1[len].Days; 
				var project = serviceItemList_1[len].Project;
				var T_task = serviceItemList_1[len].Task;
				var H_Hrs = serviceItemList_1[len].Hrs;
				nlapiLogExecution('DEBUG','day_d',day_d);
				loadTS.selectNewLineItem('timegrid');

				//Get Ineteger no of array
				var days_d_text = otg_days.indexOf(day_d_1);
			//	for (var i = 0; i < 7; i++) {

					if (isNumber(days_d_text)) {
						// nlapiLogExecution('debug', 'project.Hours[i]',
						// project.Hours[i]);
						var newEntry = loadTS.createCurrentLineItemSubrecord(
								'timegrid', daysOfWeek[days_d_text]);
						// newEntry.setFieldValue('subsidiary',
						// employeeDetails.subsidiary);
						newEntry.setFieldValue('employee', employee);
						newEntry.setFieldText('customer', project);
						// newEntry.setFieldValue('location',
						// employeeDetails.location);
						 newEntry.setFieldValue('approvalstatus','2');
						newEntry.setFieldText('casetaskevent', T_task);
						// newEntry.setFieldValue('isbillable', 'T');
						newEntry.setFieldValue('hours', H_Hrs);
						newEntry.commit();
					}
					loadTS.commitLineItem('timegrid');
				//}
			}
			}
			
			//Service Item_2
			if(_logValidation(serviceItemList_2)){
			for(var len=0;len<serviceItemList_2.length;len++){
				var day_d_2 = serviceItemList_2[len].Days;
				var project = serviceItemList_2[len].Project;
				var T_task = serviceItemList_2[len].Task;
				var H_Hrs = serviceItemList_2[len].Hrs;
				nlapiLogExecution('DEBUG','day_d',day_d);
				loadTS.selectNewLineItem('timegrid');

				//Get Ineteger no of array
				var days_d_text = otg_days.indexOf(day_d_2);
			//	for (var i = 0; i < 7; i++) {

					if (isNumber(days_d_text)) {
						// nlapiLogExecution('debug', 'project.Hours[i]',
						// project.Hours[i]);
						var newEntry = loadTS.createCurrentLineItemSubrecord(
								'timegrid', daysOfWeek[days_d_text]);
						// newEntry.setFieldValue('subsidiary',
						// employeeDetails.subsidiary);
						newEntry.setFieldValue('employee', employee);
						newEntry.setFieldText('customer', project);
						// newEntry.setFieldValue('location',
						// employeeDetails.location);
						 newEntry.setFieldValue('approvalstatus','2');
						newEntry.setFieldText('casetaskevent', T_task);
						// newEntry.setFieldValue('isbillable', 'T');
						newEntry.setFieldValue('hours', H_Hrs);
						newEntry.commit();
					}
					loadTS.commitLineItem('timegrid');
				//}
			}
			}
			//Service Item_3
			if(_logValidation(serviceItemList_3)){
			for(var len=0;len<serviceItemList_3.length;len++){
				var day_d_3 = serviceItemList_3[len].Days;
				var project = serviceItemList_3[len].Project;
				var T_task = serviceItemList_3[len].Task;
				var H_Hrs = serviceItemList_3[len].Hrs;
				nlapiLogExecution('DEBUG','day_d',day_d);
				loadTS.selectNewLineItem('timegrid');

				//Get Ineteger no of array
				var days_d_text = otg_days.indexOf(day_d_3);
			//	for (var i = 0; i < 7; i++) {

					if (isNumber(days_d_text)) {
						// nlapiLogExecution('debug', 'project.Hours[i]',
						// project.Hours[i]);
						var newEntry = loadTS.createCurrentLineItemSubrecord(
								'timegrid', daysOfWeek[days_d_text]);
						// newEntry.setFieldValue('subsidiary',
						// employeeDetails.subsidiary);
						newEntry.setFieldValue('employee', employee);
						newEntry.setFieldText('customer', project);
						// newEntry.setFieldValue('location',
						// employeeDetails.location);
						 newEntry.setFieldValue('approvalstatus','2');
						newEntry.setFieldText('casetaskevent', T_task);
						// newEntry.setFieldValue('isbillable', 'T');
						newEntry.setFieldValue('hours', H_Hrs);
						newEntry.commit();
					}
					loadTS.commitLineItem('timegrid');
				//}
			}
			}
			//service Item 4
			if(_logValidation(serviceItemList_4)){
			for(var len=0;len<serviceItemList_4.length;len++){
				var day_d = serviceItemList_4[len].Days;
				var project = serviceItemList_4[len].Project;
				var T_task = serviceItemList_4[len].Task;
				var H_Hrs = serviceItemList_4[len].Hrs;
				nlapiLogExecution('DEBUG','day_d',day_d);
				loadTS.selectNewLineItem('timegrid');

				//Get Ineteger no of array
				var days_d_text = otg_days.indexOf(day_d);
			//	for (var i = 0; i < 7; i++) {

					if (isNumber(days_d_text)) {
						// nlapiLogExecution('debug', 'project.Hours[i]',
						// project.Hours[i]);
						var newEntry = loadTS.createCurrentLineItemSubrecord(
								'timegrid', daysOfWeek[days_d_text]);
						// newEntry.setFieldValue('subsidiary',
						// employeeDetails.subsidiary);
						newEntry.setFieldValue('employee', employee);
						newEntry.setFieldText('customer', project);
						// newEntry.setFieldValue('location',
						// employeeDetails.location);
						 newEntry.setFieldValue('approvalstatus','2');
						newEntry.setFieldText('casetaskevent', T_task);
						// newEntry.setFieldValue('isbillable', 'T');
						newEntry.setFieldValue('hours', H_Hrs);
						newEntry.commit();
					}
					loadTS.commitLineItem('timegrid');
				//}
			}
			}
			//service Item 5
			if(_logValidation(serviceItemList_5)){
			for(var len=0;len<serviceItemList_5.length;len++){
				var day_d = serviceItemList_5[len].Days;
				var project = serviceItemList_5[len].Project;
				var T_task = serviceItemList_5[len].Task;
				var H_Hrs = serviceItemList_5[len].Hrs;
				nlapiLogExecution('DEBUG','day_d',day_d);
				loadTS.selectNewLineItem('timegrid');

				//Get Ineteger no of array
				var days_d_text = otg_days.indexOf(day_d);
			//	for (var i = 0; i < 7; i++) {

					if (isNumber(days_d_text)) {
						// nlapiLogExecution('debug', 'project.Hours[i]',
						// project.Hours[i]);
						var newEntry = loadTS.createCurrentLineItemSubrecord(
								'timegrid', daysOfWeek[days_d_text]);
						// newEntry.setFieldValue('subsidiary',
						// employeeDetails.subsidiary);
						newEntry.setFieldValue('employee', employee);
						newEntry.setFieldText('customer', project);
						// newEntry.setFieldValue('location',
						// employeeDetails.location);
						 newEntry.setFieldValue('approvalstatus','2');
						newEntry.setFieldText('casetaskevent', T_task);
						// newEntry.setFieldValue('isbillable', 'T');
						newEntry.setFieldValue('hours', H_Hrs);
						newEntry.commit();
					}
					loadTS.commitLineItem('timegrid');
				//}
			}
			}
			//service Item 6
			if(_logValidation(serviceItemList_6)){
			for(var len=0;len<serviceItemList_6.length;len++){
				var day_d = serviceItemList_6[len].Days;
				var project = serviceItemList_6[len].Project;
				var T_task = serviceItemList_6[len].Task;
				var H_Hrs = serviceItemList_6[len].Hrs;
				nlapiLogExecution('DEBUG','day_d',day_d);
				loadTS.selectNewLineItem('timegrid');

				//Get Ineteger no of array
				var days_d_text = otg_days.indexOf(day_d);
			//	for (var i = 0; i < 7; i++) {

					if (isNumber(days_d_text)) {
						// nlapiLogExecution('debug', 'project.Hours[i]',
						// project.Hours[i]);
						var newEntry = loadTS.createCurrentLineItemSubrecord(
								'timegrid', daysOfWeek[days_d_text]);
						// newEntry.setFieldValue('subsidiary',
						// employeeDetails.subsidiary);
						newEntry.setFieldValue('employee', employee);
						newEntry.setFieldText('customer', project);
						// newEntry.setFieldValue('location',
						// employeeDetails.location);
						 newEntry.setFieldValue('approvalstatus','2');
						newEntry.setFieldText('casetaskevent', T_task);
						// newEntry.setFieldValue('isbillable', 'T');
						newEntry.setFieldValue('hours', H_Hrs);
						newEntry.commit();
					}
					loadTS.commitLineItem('timegrid');
				//}
			}
			}
			//Holiday Item
			if(_logValidation(holiday_list)){
			for(var len=0;len<holiday_list.length;len++){
				var day_d_h = holiday_list[len].Days;
				var project_h = holiday_list[len].Project;
				var T_task_h = holiday_list[len].Task;
				var H_Hrs_h = holiday_list[len].Hrs;
				nlapiLogExecution('DEBUG','day_d',day_d);
				
				
				loadTS.selectNewLineItem('timegrid');

				//Get Ineteger no of array
				var days_d_text = otg_days.indexOf(day_d_h);
			//	for (var i = 0; i < 7; i++) {

					if (isNumber(days_d_text)) {
						// nlapiLogExecution('debug', 'project.Hours[i]',
						// project.Hours[i]);
						var newEntry = loadTS.createCurrentLineItemSubrecord(
								'timegrid', daysOfWeek[days_d_text]);
						// newEntry.setFieldValue('subsidiary',
						// employeeDetails.subsidiary);
						newEntry.setFieldValue('employee', employee);
						newEntry.setFieldText('customer', project_h);
						// newEntry.setFieldValue('location',
						// employeeDetails.location);
						// newEntry.setFieldText('projecttaskassignment',
						// project.Event);
						newEntry.setFieldText('casetaskevent', T_task_h);
						newEntry.setFieldValue('approvalstatus','2');
						// newEntry.setFieldValue('isbillable', 'T');
						newEntry.setFieldValue('hours', H_Hrs_h);
						newEntry.commit();
					}
					loadTS.commitLineItem('timegrid');
				//}
			}
			}
			
			if(_logValidation(holiday_list_2)){
				for(var len=0;len<holiday_list_2.length;len++){
					var day_d_h = holiday_list_2[len].Days;
					var project_h = holiday_list_2[len].Project;
					var T_task_h = holiday_list_2[len].Task;
					var H_Hrs_h = holiday_list_2[len].Hrs;
					nlapiLogExecution('DEBUG','day_d',day_d);
					
					
					loadTS.selectNewLineItem('timegrid');

					//Get Ineteger no of array
					var days_d_text = otg_days.indexOf(day_d_h);
				//	for (var i = 0; i < 7; i++) {

						if (isNumber(days_d_text)) {
							// nlapiLogExecution('debug', 'project.Hours[i]',
							// project.Hours[i]);
							var newEntry = loadTS.createCurrentLineItemSubrecord(
									'timegrid', daysOfWeek[days_d_text]);
							// newEntry.setFieldValue('subsidiary',
							// employeeDetails.subsidiary);
							newEntry.setFieldValue('employee', employee);
							newEntry.setFieldText('customer', project_h);
							// newEntry.setFieldValue('location',
							// employeeDetails.location);
							// newEntry.setFieldText('projecttaskassignment',
							// project.Event);
							newEntry.setFieldText('casetaskevent', T_task_h);
							newEntry.setFieldValue('approvalstatus','2');
							// newEntry.setFieldValue('isbillable', 'T');
							newEntry.setFieldValue('hours', H_Hrs_h);
							newEntry.commit();
						}
						loadTS.commitLineItem('timegrid');
					//}
				}
				}
			if(_logValidation(holiday_list_3)){
				for(var len=0;len<holiday_list_3.length;len++){
					var day_d_h = holiday_list_3[len].Days;
					var project_h = holiday_list_3[len].Project;
					var T_task_h = holiday_list_3[len].Task;
					var H_Hrs_h = holiday_list_3[len].Hrs;
					nlapiLogExecution('DEBUG','day_d',day_d);
					
					
					loadTS.selectNewLineItem('timegrid');

					//Get Ineteger no of array
					var days_d_text = otg_days.indexOf(day_d_h);
				//	for (var i = 0; i < 7; i++) {

						if (isNumber(days_d_text)) {
							// nlapiLogExecution('debug', 'project.Hours[i]',
							// project.Hours[i]);
							var newEntry = loadTS.createCurrentLineItemSubrecord(
									'timegrid', daysOfWeek[days_d_text]);
							// newEntry.setFieldValue('subsidiary',
							// employeeDetails.subsidiary);
							newEntry.setFieldValue('employee', employee);
							newEntry.setFieldText('customer', project_h);
							// newEntry.setFieldValue('location',
							// employeeDetails.location);
							// newEntry.setFieldText('projecttaskassignment',
							// project.Event);
							newEntry.setFieldText('casetaskevent', T_task_h);
							newEntry.setFieldValue('approvalstatus','2');
							// newEntry.setFieldValue('isbillable', 'T');
							newEntry.setFieldValue('hours', H_Hrs_h);
							newEntry.commit();
						}
						loadTS.commitLineItem('timegrid');
					//}
				}
				}
			if(_logValidation(holiday_list_4)){
				for(var len=0;len<holiday_list_4.length;len++){
					var day_d_h = holiday_list_4[len].Days;
					var project_h = holiday_list_4[len].Project;
					var T_task_h = holiday_list_4[len].Task;
					var H_Hrs_h = holiday_list_4[len].Hrs;
					nlapiLogExecution('DEBUG','day_d',day_d);
					
					
					loadTS.selectNewLineItem('timegrid');

					//Get Ineteger no of array
					var days_d_text = otg_days.indexOf(day_d_h);
				//	for (var i = 0; i < 7; i++) {

						if (isNumber(days_d_text)) {
							// nlapiLogExecution('debug', 'project.Hours[i]',
							// project.Hours[i]);
							var newEntry = loadTS.createCurrentLineItemSubrecord(
									'timegrid', daysOfWeek[days_d_text]);
							// newEntry.setFieldValue('subsidiary',
							// employeeDetails.subsidiary);
							newEntry.setFieldValue('employee', employee);
							newEntry.setFieldText('customer', project_h);
							// newEntry.setFieldValue('location',
							// employeeDetails.location);
							// newEntry.setFieldText('projecttaskassignment',
							// project.Event);
							newEntry.setFieldText('casetaskevent', T_task_h);
							newEntry.setFieldValue('approvalstatus','2');
							// newEntry.setFieldValue('isbillable', 'T');
							newEntry.setFieldValue('hours', H_Hrs_h);
							newEntry.commit();
						}
						loadTS.commitLineItem('timegrid');
					//}
				}
				}
				if(_logValidation(holiday_list_5)){
				for(var len=0;len<holiday_list_5.length;len++){
					var day_d_h = holiday_list_5[len].Days;
					var project_h = holiday_list_5[len].Project;
					var T_task_h = holiday_list_5[len].Task;
					var H_Hrs_h = holiday_list_5[len].Hrs;
					nlapiLogExecution('DEBUG','day_d',day_d);
					
					
					loadTS.selectNewLineItem('timegrid');

					//Get Ineteger no of array
					var days_d_text = otg_days.indexOf(day_d_h);
				//	for (var i = 0; i < 7; i++) {

						if (isNumber(days_d_text)) {
							// nlapiLogExecution('debug', 'project.Hours[i]',
							// project.Hours[i]);
							var newEntry = loadTS.createCurrentLineItemSubrecord(
									'timegrid', daysOfWeek[days_d_text]);
							// newEntry.setFieldValue('subsidiary',
							// employeeDetails.subsidiary);
							newEntry.setFieldValue('employee', employee);
							newEntry.setFieldText('customer', project_h);
							// newEntry.setFieldValue('location',
							// employeeDetails.location);
							// newEntry.setFieldText('projecttaskassignment',
							// project.Event);
							newEntry.setFieldText('casetaskevent', T_task_h);
							newEntry.setFieldValue('approvalstatus','2');
							// newEntry.setFieldValue('isbillable', 'T');
							newEntry.setFieldValue('hours', H_Hrs_h);
							newEntry.commit();
						}
						loadTS.commitLineItem('timegrid');
					//}
				}
				}
				if(_logValidation(holiday_list_6)){
				for(var len=0;len<holiday_list_6.length;len++){
					var day_d_h = holiday_list_6[len].Days;
					var project_h = holiday_list_6[len].Project;
					var T_task_h = holiday_list_6[len].Task;
					var H_Hrs_h = holiday_list_6[len].Hrs;
					nlapiLogExecution('DEBUG','day_d',day_d);
					
					
					loadTS.selectNewLineItem('timegrid');

					//Get Ineteger no of array
					var days_d_text = otg_days.indexOf(day_d_h);
				//	for (var i = 0; i < 7; i++) {

						if (isNumber(days_d_text)) {
							// nlapiLogExecution('debug', 'project.Hours[i]',
							// project.Hours[i]);
							var newEntry = loadTS.createCurrentLineItemSubrecord(
									'timegrid', daysOfWeek[days_d_text]);
							// newEntry.setFieldValue('subsidiary',
							// employeeDetails.subsidiary);
							newEntry.setFieldValue('employee', employee);
							newEntry.setFieldText('customer', project_h);
							// newEntry.setFieldValue('location',
							// employeeDetails.location);
							// newEntry.setFieldText('projecttaskassignment',
							// project.Event);
							newEntry.setFieldText('casetaskevent', T_task_h);
							newEntry.setFieldValue('approvalstatus','2');
							// newEntry.setFieldValue('isbillable', 'T');
							newEntry.setFieldValue('hours', H_Hrs_h);
							newEntry.commit();
						}
						loadTS.commitLineItem('timegrid');
					//}
				}
				}
			//Leave Item
			if(_logValidation(leave_list)){
			for(var len=0;len<leave_list.length;len++){
				var day_d_p = leave_list[len].Days;
				var project_p = leave_list[len].Project;
				var T_task_p = leave_list[len].Task;
				var H_Hrs_p = leave_list[len].Hrs;
				nlapiLogExecution('DEBUG','day_d',day_d);
				
				loadTS.selectNewLineItem('timegrid');

				//Get Ineteger no of array
				var days_d_text = otg_days.indexOf(day_d_p);
			//	for (var i = 0; i < 7; i++) {

					if (isNumber(days_d_text)) {
						// nlapiLogExecution('debug', 'project.Hours[i]',
						// project.Hours[i]);
						var newEntry = loadTS.createCurrentLineItemSubrecord(
								'timegrid', daysOfWeek[days_d_text]);
						// newEntry.setFieldValue('subsidiary',
						// employeeDetails.subsidiary);
						newEntry.setFieldValue('employee', employee);
						newEntry.setFieldText('customer', project_p);
						newEntry.setFieldValue('approvalstatus','2');
						// newEntry.setFieldValue('location',
						// employeeDetails.location);
						// newEntry.setFieldText('projecttaskassignment',
						// project.Event);
						newEntry.setFieldText('casetaskevent', T_task_p);
						// newEntry.setFieldValue('isbillable', 'T');
						newEntry.setFieldValue('hours', H_Hrs_p);
						newEntry.commit();
					}
					loadTS.commitLineItem('timegrid');
				//}
			}
			}
			if(_logValidation(leave_list_2)){
				for(var len=0;len<leave_list_2.length;len++){
					var day_d_p = leave_list_2[len].Days;
					var project_p = leave_list_2[len].Project;
					var T_task_p = leave_list_2[len].Task;
					var H_Hrs_p = leave_list_2[len].Hrs;
					nlapiLogExecution('DEBUG','day_d',day_d);
					
					loadTS.selectNewLineItem('timegrid');

					//Get Ineteger no of array
					var days_d_text = otg_days.indexOf(day_d_p);
				//	for (var i = 0; i < 7; i++) {

						if (isNumber(days_d_text)) {
							// nlapiLogExecution('debug', 'project.Hours[i]',
							// project.Hours[i]);
							var newEntry = loadTS.createCurrentLineItemSubrecord(
									'timegrid', daysOfWeek[days_d_text]);
							// newEntry.setFieldValue('subsidiary',
							// employeeDetails.subsidiary);
							newEntry.setFieldValue('employee', employee);
							newEntry.setFieldText('customer', project_p);
							newEntry.setFieldValue('approvalstatus','2');
							// newEntry.setFieldValue('location',
							// employeeDetails.location);
							// newEntry.setFieldText('projecttaskassignment',
							// project.Event);
							newEntry.setFieldText('casetaskevent', T_task_p);
							// newEntry.setFieldValue('isbillable', 'T');
							newEntry.setFieldValue('hours', H_Hrs_p);
							newEntry.commit();
						}
						loadTS.commitLineItem('timegrid');
					//}
				}
				}
			if(_logValidation(leave_list_3)){
				for(var len=0;len<leave_list_3.length;len++){
					var day_d_p = leave_list_3[len].Days;
					var project_p = leave_list_3[len].Project;
					var T_task_p = leave_list_3[len].Task;
					var H_Hrs_p = leave_list_3[len].Hrs;
					nlapiLogExecution('DEBUG','day_d',day_d);
					
					loadTS.selectNewLineItem('timegrid');

					//Get Ineteger no of array
					var days_d_text = otg_days.indexOf(day_d_p);
				//	for (var i = 0; i < 7; i++) {

						if (isNumber(days_d_text)) {
							// nlapiLogExecution('debug', 'project.Hours[i]',
							// project.Hours[i]);
							var newEntry = loadTS.createCurrentLineItemSubrecord(
									'timegrid', daysOfWeek[days_d_text]);
							// newEntry.setFieldValue('subsidiary',
							// employeeDetails.subsidiary);
							newEntry.setFieldValue('employee', employee);
							newEntry.setFieldText('customer', project_p);
							newEntry.setFieldValue('approvalstatus','2');
							// newEntry.setFieldValue('location',
							// employeeDetails.location);
							// newEntry.setFieldText('projecttaskassignment',
							// project.Event);
							newEntry.setFieldText('casetaskevent', T_task_p);
							// newEntry.setFieldValue('isbillable', 'T');
							newEntry.setFieldValue('hours', H_Hrs_p);
							newEntry.commit();
						}
						loadTS.commitLineItem('timegrid');
					//}
				}
				}
			if(_logValidation(leave_list_4)){
				for(var len=0;len<leave_list_4.length;len++){
					var day_d_p = leave_list_4[len].Days;
					var project_p = leave_list_4[len].Project;
					var T_task_p = leave_list_4[len].Task;
					var H_Hrs_p = leave_list_4[len].Hrs;
					nlapiLogExecution('DEBUG','day_d',day_d);
					
					loadTS.selectNewLineItem('timegrid');

					//Get Ineteger no of array
					var days_d_text = otg_days.indexOf(day_d_p);
				//	for (var i = 0; i < 7; i++) {

						if (isNumber(days_d_text)) {
							// nlapiLogExecution('debug', 'project.Hours[i]',
							// project.Hours[i]);
							var newEntry = loadTS.createCurrentLineItemSubrecord(
									'timegrid', daysOfWeek[days_d_text]);
							// newEntry.setFieldValue('subsidiary',
							// employeeDetails.subsidiary);
							newEntry.setFieldValue('employee', employee);
							newEntry.setFieldText('customer', project_p);
							newEntry.setFieldValue('approvalstatus','2');
							// newEntry.setFieldValue('location',
							// employeeDetails.location);
							// newEntry.setFieldText('projecttaskassignment',
							// project.Event);
							newEntry.setFieldText('casetaskevent', T_task_p);
							// newEntry.setFieldValue('isbillable', 'T');
							newEntry.setFieldValue('hours', H_Hrs_p);
							newEntry.commit();
						}
						loadTS.commitLineItem('timegrid');
					//}
				}
				}
				if(_logValidation(leave_list_5)){
				for(var len=0;len<leave_list_5.length;len++){
					var day_d_p = leave_list_5[len].Days;
					var project_p = leave_list_5[len].Project;
					var T_task_p = leave_list_5[len].Task;
					var H_Hrs_p = leave_list_5[len].Hrs;
					nlapiLogExecution('DEBUG','day_d',day_d);
					
					loadTS.selectNewLineItem('timegrid');

					//Get Ineteger no of array
					var days_d_text = otg_days.indexOf(day_d_p);
				//	for (var i = 0; i < 7; i++) {

						if (isNumber(days_d_text)) {
							// nlapiLogExecution('debug', 'project.Hours[i]',
							// project.Hours[i]);
							var newEntry = loadTS.createCurrentLineItemSubrecord(
									'timegrid', daysOfWeek[days_d_text]);
							// newEntry.setFieldValue('subsidiary',
							// employeeDetails.subsidiary);
							newEntry.setFieldValue('employee', employee);
							newEntry.setFieldText('customer', project_p);
							newEntry.setFieldValue('approvalstatus','2');
							// newEntry.setFieldValue('location',
							// employeeDetails.location);
							// newEntry.setFieldText('projecttaskassignment',
							// project.Event);
							newEntry.setFieldText('casetaskevent', T_task_p);
							// newEntry.setFieldValue('isbillable', 'T');
							newEntry.setFieldValue('hours', H_Hrs_p);
							newEntry.commit();
						}
						loadTS.commitLineItem('timegrid');
					//}
				}
				}
				if(_logValidation(leave_list_6)){
				for(var len=0;len<leave_list_6.length;len++){
					var day_d_p = leave_list_6[len].Days;
					var project_p = leave_list_6[len].Project;
					var T_task_p = leave_list_6[len].Task;
					var H_Hrs_p = leave_list_6[len].Hrs;
					nlapiLogExecution('DEBUG','day_d',day_d);
					
					loadTS.selectNewLineItem('timegrid');

					//Get Ineteger no of array
					var days_d_text = otg_days.indexOf(day_d_p);
				//	for (var i = 0; i < 7; i++) {

						if (isNumber(days_d_text)) {
							// nlapiLogExecution('debug', 'project.Hours[i]',
							// project.Hours[i]);
							var newEntry = loadTS.createCurrentLineItemSubrecord(
									'timegrid', daysOfWeek[days_d_text]);
							// newEntry.setFieldValue('subsidiary',
							// employeeDetails.subsidiary);
							newEntry.setFieldValue('employee', employee);
							newEntry.setFieldText('customer', project_p);
							newEntry.setFieldValue('approvalstatus','2');
							// newEntry.setFieldValue('location',
							// employeeDetails.location);
							// newEntry.setFieldText('projecttaskassignment',
							// project.Event);
							newEntry.setFieldText('casetaskevent', T_task_p);
							// newEntry.setFieldValue('isbillable', 'T');
							newEntry.setFieldValue('hours', H_Hrs_p);
							newEntry.commit();
						}
						loadTS.commitLineItem('timegrid');
					//}
				}
				}
			//OT Item
			if(_logValidation(ot_list)){
			for(var len=0;len<ot_list.length;len++){
				var day_d_0 = ot_list[len].Days;
				var project_0 = ot_list[len].Project;
				var T_task_o = ot_list[len].Task;
				var H_Hrs_o = ot_list[len].OTHrs;
				nlapiLogExecution('DEBUG','day_d',day_d);
				
				loadTS.selectNewLineItem('timegrid');

				//Get Ineteger no of array
				var days_d_text = otg_days.indexOf(day_d_0);
			//	for (var i = 0; i < 7; i++) {

					if (isNumber(days_d_text)) {
						// nlapiLogExecution('debug', 'project.Hours[i]',
						// project.Hours[i]);
						var newEntry = loadTS.createCurrentLineItemSubrecord(
								'timegrid', daysOfWeek[days_d_text]);
						// newEntry.setFieldValue('subsidiary',
						// employeeDetails.subsidiary);
						newEntry.setFieldValue('employee', employee);
						newEntry.setFieldText('customer', project_0);
						newEntry.setFieldValue('approvalstatus','2');
						// newEntry.setFieldValue('location',
						// employeeDetails.location);
						// newEntry.setFieldText('projecttaskassignment',
						// project.Event);
						newEntry.setFieldText('casetaskevent', T_task_o);
						// newEntry.setFieldValue('isbillable', 'T');
						newEntry.setFieldValue('hours', H_Hrs_o);
						newEntry.commit();
					}
					loadTS.commitLineItem('timegrid');
				//}
			}
			}
			
			if(_logValidation(ot_list_2)){
				for(var len=0;len<ot_list_2.length;len++){
					var day_d_0 = ot_list_2[len].Days;
					var project_0 = ot_list_2[len].Project;
					var T_task_o = ot_list_2[len].Task;
					var H_Hrs_o = ot_list_2[len].OTHrs;
					nlapiLogExecution('DEBUG','day_d',day_d);
					
					loadTS.selectNewLineItem('timegrid');

					//Get Ineteger no of array
					var days_d_text = otg_days.indexOf(day_d_0);
				//	for (var i = 0; i < 7; i++) {

						if (isNumber(days_d_text)) {
							// nlapiLogExecution('debug', 'project.Hours[i]',
							// project.Hours[i]);
							var newEntry = loadTS.createCurrentLineItemSubrecord(
									'timegrid', daysOfWeek[days_d_text]);
							// newEntry.setFieldValue('subsidiary',
							// employeeDetails.subsidiary);
							newEntry.setFieldValue('employee', employee);
							newEntry.setFieldText('customer', project_0);
							newEntry.setFieldValue('approvalstatus','2');
							// newEntry.setFieldValue('location',
							// employeeDetails.location);
							// newEntry.setFieldText('projecttaskassignment',
							// project.Event);
							newEntry.setFieldText('casetaskevent', T_task_o);
							// newEntry.setFieldValue('isbillable', 'T');
							newEntry.setFieldValue('hours', H_Hrs_o);
							newEntry.commit();
						}
						loadTS.commitLineItem('timegrid');
					//}
				}
				}
			if(_logValidation(ot_list_3)){
				for(var len=0;len<ot_list_3.length;len++){
					var day_d_0 = ot_list_3[len].Days;
					var project_0 = ot_list_3[len].Project;
					var T_task_o = ot_list_3[len].Task;
					var H_Hrs_o = ot_list_3[len].OTHrs;
					nlapiLogExecution('DEBUG','day_d',day_d);
					
					loadTS.selectNewLineItem('timegrid');

					//Get Ineteger no of array
					var days_d_text = otg_days.indexOf(day_d_0);
				//	for (var i = 0; i < 7; i++) {

						if (isNumber(days_d_text)) {
							// nlapiLogExecution('debug', 'project.Hours[i]',
							// project.Hours[i]);
							var newEntry = loadTS.createCurrentLineItemSubrecord(
									'timegrid', daysOfWeek[days_d_text]);
							// newEntry.setFieldValue('subsidiary',
							// employeeDetails.subsidiary);
							newEntry.setFieldValue('employee', employee);
							newEntry.setFieldText('customer', project_0);
							newEntry.setFieldValue('approvalstatus','2');
							// newEntry.setFieldValue('location',
							// employeeDetails.location);
							// newEntry.setFieldText('projecttaskassignment',
							// project.Event);
							newEntry.setFieldText('casetaskevent', T_task_o);
							// newEntry.setFieldValue('isbillable', 'T');
							newEntry.setFieldValue('hours', H_Hrs_o);
							newEntry.commit();
						}
						loadTS.commitLineItem('timegrid');
					//}
				}
				}
			if(_logValidation(ot_list_4)){
				for(var len=0;len<ot_list_4.length;len++){
					var day_d_0 = ot_list_4[len].Days;
					var project_0 = ot_list_4[len].Project;
					var T_task_o = ot_list_4[len].Task;
					var H_Hrs_o = ot_list_4[len].OTHrs;
					nlapiLogExecution('DEBUG','day_d',day_d);
					
					loadTS.selectNewLineItem('timegrid');

					//Get Ineteger no of array
					var days_d_text = otg_days.indexOf(day_d_0);
				//	for (var i = 0; i < 7; i++) {

						if (isNumber(days_d_text)) {
							// nlapiLogExecution('debug', 'project.Hours[i]',
							// project.Hours[i]);
							var newEntry = loadTS.createCurrentLineItemSubrecord(
									'timegrid', daysOfWeek[days_d_text]);
							// newEntry.setFieldValue('subsidiary',
							// employeeDetails.subsidiary);
							newEntry.setFieldValue('employee', employee);
							newEntry.setFieldText('customer', project_0);
							newEntry.setFieldValue('approvalstatus','2');
							// newEntry.setFieldValue('location',
							// employeeDetails.location);
							// newEntry.setFieldText('projecttaskassignment',
							// project.Event);
							newEntry.setFieldText('casetaskevent', T_task_o);
							// newEntry.setFieldValue('isbillable', 'T');
							newEntry.setFieldValue('hours', H_Hrs_o);
							newEntry.commit();
						}
						loadTS.commitLineItem('timegrid');
					//}
				}
				}
				if(_logValidation(ot_list_5)){
				for(var len=0;len<ot_list_5.length;len++){
					var day_d_0 = ot_list_5[len].Days;
					var project_0 = ot_list_5[len].Project;
					var T_task_o = ot_list_5[len].Task;
					var H_Hrs_o = ot_list_5[len].OTHrs;
					nlapiLogExecution('DEBUG','day_d',day_d);
					
					loadTS.selectNewLineItem('timegrid');

					//Get Ineteger no of array
					var days_d_text = otg_days.indexOf(day_d_0);
				//	for (var i = 0; i < 7; i++) {

						if (isNumber(days_d_text)) {
							// nlapiLogExecution('debug', 'project.Hours[i]',
							// project.Hours[i]);
							var newEntry = loadTS.createCurrentLineItemSubrecord(
									'timegrid', daysOfWeek[days_d_text]);
							// newEntry.setFieldValue('subsidiary',
							// employeeDetails.subsidiary);
							newEntry.setFieldValue('employee', employee);
							newEntry.setFieldText('customer', project_0);
							newEntry.setFieldValue('approvalstatus','2');
							// newEntry.setFieldValue('location',
							// employeeDetails.location);
							// newEntry.setFieldText('projecttaskassignment',
							// project.Event);
							newEntry.setFieldText('casetaskevent', T_task_o);
							// newEntry.setFieldValue('isbillable', 'T');
							newEntry.setFieldValue('hours', H_Hrs_o);
							newEntry.commit();
						}
						loadTS.commitLineItem('timegrid');
					//}
				}
				}
			//set TS Status
			//loadTS.setFieldText('approvalstatus','Approved');
			loadTS.setFieldValue('approvalstatus', '2');
			loadTS.setFieldValue('iscomplete','T');
			var ts_submit_id = nlapiSubmitRecord(loadTS,true,true);
			//nlapiSubmitField('timesheet',ts_submit_id,'iscomplete','T');
			nlapiLogExecution('DEBUG', 'ts_submit_id-Submit', ts_submit_id);
			status_ts = nlapiLookupField('timesheet',ts_submit_id,'approvalstatus',true);
		}
		return status_ts;
	}
	catch(err){
		nlapiLogExecution('error', 'Restlet Main Function', err);
		throw err;
	}
}
//Get TS Data if  already filled
function getTimesheetData(dataIn,employeeId,startDate,endDate,TSObj){
	try{
		var JSON_Total = {};
		var dataRow_Total = [];
		var OT_Applicable = '';
		var ts_startDate = '';
		var ts_endDate = '';
		var ts_status = '';
		var task_t = '';
		var item_t ='';
		var OT_Applicable = [];
		var date_string = startDate;
		var date_string_Items = startDate;
		// get active allocations, hours ,etc.
		var allocationDetails = getActiveAllocations(startDate, endDate,
				employeeId);
		
		var allocationDetails_items = getActiveAllocations_Items(startDate, endDate,
				employeeId);
		
		//GET OT Flag From Allocation
		allocationDetails_items.forEach(function(project) {
			//OT_Applicable = [];
			OT_Applicable.push({
				'OT_Applicable': project.OT,
				'Project': project.Project
			});
		//	OT_Applicable = project.OT;
			//Project
		});
		allocationDetails.forEach(function(project) {
			task_t =project.Task;
		});
		var JSON_TS_Body = {};
		var dataRow_TS_Body =[];
		var dataRow_Items = [];
		var JSON_Items = {};
		var time_approver_email ='';
		if(TSObj){
			var timesheetID = TSObj[0].getId();
			var load_TS = nlapiLoadRecord('timesheet',timesheetID);
			var emp = load_TS.getFieldText('employee');
			var emp_id = load_TS.getFieldValue('employee');
			var emp_lookUP = nlapiLookupField('employee',emp_id,['email']);
			var emp_email = emp_lookUP.email;
			var emp_TS_approver = load_TS.getFieldText('custrecord_ts_time_approver');
			var weekStart = load_TS.getFieldValue('startdate');
			var weekEnd = load_TS.getFieldValue('enddate');
			var totalHrs = load_TS.getFieldValue('totalhours');
			var approvalstatus = load_TS.getFieldText('approvalstatus');
			
			var s_hireDate = nlapiLookupField('employee',parseInt(emp_id),['hiredate','timeapprover']);
			if(s_hireDate.timeapprover)
			time_approver_email = nlapiLookupField('employee',s_hireDate.timeapprover,'email');
			
			JSON_TS_Body = {
			TimeSheetID: timesheetID,
			Employee: emp,
			Email: emp_email,
			timesheetApprover :  _logValidation(time_approver_email) == false ? ' ' : time_approver_email.split('@')[0],
			HireDate: s_hireDate.hiredate,
			TSApprover: emp_TS_approver,
			WeekStartDate: weekStart,
			WeekEndDate: weekEnd,
			TotalHours: totalHrs,
			Status: approvalstatus,
			TimeSheetBlock: false,
			TaskHoliday: 'Holiday (Project Task)',
			TaskLeave: 'Leave (Project Task)',
			TaskOT: 'OT (Project Task)',
			ServiceItemHoliday: '2480',
			ServiceItemLeave: '2479',
			ServiceItemOT: '2425',
			DayLimit: 8,
			WeekLimit: 40			
			};
			dataRow_TS_Body.push(JSON_TS_Body);
			
			
				
			}
			//For Holiday Leave OT
		var JSON_Holiday_Lines_ = {};
		var dataRow_Holiday_Lines_ =[];
		var JSON_Leave_Lines_ = {};
		var dataRow_Leave_Lines_ =[];
		var JSON_OT_Lines_ = {};
		var dataRow_OT_Lines_ =[];
		
		var date_T_T = weekStart;
		date_string = nlapiStringToDate(date_string);
	
			
			
			
		//END
			var indx = 0;
			var dataRow_TS_Items_Su = [];
			var dataRow_TS_Items_Su_ = [];
			var dataRow_TS_Items_Mo = [];
			var dataRow_TS_Items_Mo_ = [];
			var dataRow_TS_Items_Tu = [];
			var dataRow_TS_Items_Tu_ = [];
			var dataRow_TS_Items_We = [];
			var dataRow_TS_Items_We_ = [];
			var dataRow_TS_Items_Th = [];
			var dataRow_TS_Items_Th_ = [];
			var dataRow_TS_Items_Fr = [];
			var dataRow_TS_Items_Fr_ = [];
			var dataRow_TS_Items_Sa = [];
			var dataRow_TS_Items_Sa_ = [];
			
			var dataRow_OT_Items_Su = [];
			var dataRow_OT_Items_Su_ = [];
			var dataRow_OT_Items_Mo = [];
			var dataRow_OT_Items_Mo_ = [];
			var dataRow_OT_Items_Tu = [];
			var dataRow_OT_Items_Tu_ = [];
			var dataRow_OT_Items_We = [];
			var dataRow_OT_Items_We_ = [];
			var dataRow_OT_Items_Th = [];
			var dataRow_OT_Items_Th_ = [];
			var dataRow_OT_Items_Fr = [];
			var dataRow_OT_Items_Fr_ = [];
			var dataRow_OT_Items_Sa = [];
			var dataRow_OT_Items_Sa_ = [];
			var hour_day = '';
			var hours_week = '';
			var dataRow_TS_Items = [];
			
			var emp_subsidiary = nlapiLookupField('employee',emp_id,'subsidiary');
			date_string_Items = nlapiStringToDate(date_string_Items);
			allocationDetails_items.forEach(function(project) {
				var iterate_flag = true;
			//	newTimesheet.selectNewLineItem('timegrid');
				if(project.Project && iterate_flag == true){
					proj_lookUp = nlapiLookupField('job',parseInt(project.ProjectID),['custentity_hoursperday','custentity_hoursperweek','custentity_onsite_hours_per_day','custentity_onsite_hours_per_week']);
					if(emp_subsidiary == 3 || emp_subsidiary == 19)
					{
						hour_day = proj_lookUp.custentity_hoursperday;
						if(!_logValidation(hour_day))
							hour_day = '';
					
						hours_week = proj_lookUp.custentity_hoursperweek;
						if(!_logValidation(hours_week))
							hours_week = 40;
					}
					else
					{
						hour_day = proj_lookUp.custentity_onsite_hours_per_day;
						if(!_logValidation(hour_day))
							hour_day = '';
					
						hours_week = proj_lookUp.custentity_onsite_hours_per_week;
						if(!_logValidation(hours_week))
							hours_week = 40;
					}
					
					iterate_flag = false;
				}
				var ot_flag_ = false;
				for(var ot_ind=0;ot_ind < OT_Applicable.length;ot_ind++){
					if(OT_Applicable[ot_ind].OT_Applicable == 'YES' && OT_Applicable[ot_ind].Project == project.Project)
					{	
						ot_flag_ = true;
					}
				}
				
				for (var i = 0; i < 7; i++) {
				
					if (isNumber(project.Hours[i])) 
					{
							
						
						//Change in structure
					if(parseInt(date_string_Items.getDay()) == parseInt(0)){  //Sunday
					dataRow_TS_Items_Su = [];
						dataRow_TS_Items_Su_.push({
					//	'Day':sub_day,
						'Date': nlapiDateToString(date_string_Items),
						'Project': project.Project,
						'Task': project.Task,
						'ServiceItem': project.Event,
						'Hrs': 0,
						'DayLimit': _logValidation(hour_day) ? parseFloat(project.Percent) * parseFloat(hour_day) : '',
						'WeekLimit': hours_week,
						'TaskPro': project.Task,
						'ServiceItemPro': project.Event,
						'HolidayCheck': false,
						'LeaveCheck': false,
						'OTHrs': 0,
						'Days': daysList[date_string_Items.getDay()],
						'isOtEligible': ot_flag_
					});
					dataRow_TS_Items_Su.push({
						'Task': 'Project Activities (Project Task)',
						'Date': nlapiDateToString(date_string_Items),
						'Days': daysList[date_string_Items.getDay()],
						'DayNo': date_string_Items.getDate(),
						'Projects': dataRow_TS_Items_Su_
					
					});
				}
				if(parseInt(date_string_Items.getDay()) == parseInt(1)){ //Monday
					dataRow_TS_Items_Mo = [];
						dataRow_TS_Items_Mo_.push({
					//	'Day':sub_day,
						'Date': nlapiDateToString(date_string_Items),
						'Project': project.Project,
						'Task': project.Task,
						'ServiceItem': project.Event,
						'Hrs': 0,
						'DayLimit': _logValidation(hour_day) ? parseFloat(project.Percent) * parseFloat(hour_day) : '',
						'WeekLimit': hours_week,	'TaskPro': project.Task,
						'ServiceItemPro': project.Event,
						'HolidayCheck': false,
						'LeaveCheck': false,
						'OTHrs': 0,
						'Days': daysList[date_string_Items.getDay()],
						'isOtEligible': ot_flag_
					});
					dataRow_TS_Items_Mo.push({
						'Task': 'Project Activities (Project Task)',
						'Date': nlapiDateToString(date_string_Items),
						'Days': daysList[date_string_Items.getDay()],
						'DayNo': date_string_Items.getDate(),
						'Projects': dataRow_TS_Items_Mo_
					
					});
				}
				if(parseInt(date_string_Items.getDay()) == parseInt(2)){ //Tuesday
					dataRow_TS_Items_Tu = [];
						dataRow_TS_Items_Tu_.push({
					//	'Day':sub_day,
						'Date': nlapiDateToString(date_string_Items),
						'Project': project.Project,
						'Task': project.Task,
						'ServiceItem': project.Event,
						'Hrs': 0,
						'DayLimit': _logValidation(hour_day) ? parseFloat(project.Percent) * parseFloat(hour_day) : '',
						'WeekLimit': hours_week,	'TaskPro': project.Task,
						'ServiceItemPro': project.Event,
						'HolidayCheck': false,
						'LeaveCheck': false,
						'OTHrs': 0,
						'Days': daysList[date_string_Items.getDay()],
						'isOtEligible': ot_flag_
					});
					dataRow_TS_Items_Tu.push({
						'Task': 'Project Activities (Project Task)',
						'Date': nlapiDateToString(date_string_Items),
						'Days': daysList[date_string_Items.getDay()],
						'DayNo': date_string_Items.getDate(),
						'Projects': dataRow_TS_Items_Tu_
					
					});
				}
				if(parseInt(date_string_Items.getDay()) == parseInt(3)){ //Wednesday
					dataRow_TS_Items_We = [];
						dataRow_TS_Items_We_.push({
					//	'Day':sub_day,
						'Date': nlapiDateToString(date_string_Items),
						'Project': project.Project,
						'Task': project.Task,
						'ServiceItem': project.Event,
						'Hrs': 0,
						'DayLimit': _logValidation(hour_day) ? parseFloat(project.Percent) * parseFloat(hour_day) : '',
						'WeekLimit': hours_week,	'TaskPro': project.Task,
						'ServiceItemPro': project.Event,
						'HolidayCheck': false,
						'LeaveCheck': false,
						'OTHrs': 0,
						'Days': daysList[date_string_Items.getDay()],
						'isOtEligible': ot_flag_
					});
					dataRow_TS_Items_We.push({
						'Task': 'Project Activities (Project Task)',
						'Date': nlapiDateToString(date_string_Items),
						'Days': daysList[date_string_Items.getDay()],
						'DayNo': date_string_Items.getDate(),
						'Projects': dataRow_TS_Items_We_
					
					});
				}
				if(parseInt(date_string_Items.getDay()) == parseInt(4)){ //Thursday
					dataRow_TS_Items_Th = [];
						dataRow_TS_Items_Th_.push({
					//	'Day':sub_day,
						'Date': nlapiDateToString(date_string_Items),
						'Project': project.Project,
						'Task': project.Task,
						'ServiceItem': project.Event,
						'Hrs': 0,
						'DayLimit': _logValidation(hour_day) ? parseFloat(project.Percent) * parseFloat(hour_day) : '',
						'WeekLimit': hours_week,	'TaskPro': project.Task,
						'ServiceItemPro': project.Event,
						'HolidayCheck': false,
						'LeaveCheck': false,
						'OTHrs': 0,
						'Days': daysList[date_string_Items.getDay()],
						'isOtEligible': ot_flag_
					});
					dataRow_TS_Items_Th.push({
						'Task': 'Project Activities (Project Task)',
						'Date': nlapiDateToString(date_string_Items),
						'Days': daysList[date_string_Items.getDay()],
						'DayNo': date_string_Items.getDate(),
						'Projects': dataRow_TS_Items_Th_
					
					});
				} 
				if(parseInt(date_string_Items.getDay()) == parseInt(5)){ //Friday
					dataRow_TS_Items_Fr = [];
						dataRow_TS_Items_Fr_.push({
					//	'Day':sub_day,
						'Date': nlapiDateToString(date_string_Items),
						'Project': project.Project,
						'Task': project.Task,
						'ServiceItem': project.Event,
						'Hrs': 0,
						'DayLimit': _logValidation(hour_day) ? parseFloat(project.Percent) * parseFloat(hour_day) : '',
						'WeekLimit': hours_week,	'TaskPro': project.Task,
						'ServiceItemPro': project.Event,
						'HolidayCheck': false,
						'LeaveCheck': false,
						'OTHrs': 0,
						'Days': daysList[date_string_Items.getDay()],
						'isOtEligible': ot_flag_
					});
					dataRow_TS_Items_Fr.push({
						'Task': 'Project Activities (Project Task)',
						'Date': nlapiDateToString(date_string_Items),
						'Days': daysList[date_string_Items.getDay()],
						'DayNo': date_string_Items.getDate(),
						'Projects': dataRow_TS_Items_Fr_
					
					});
				}
				if(parseInt(date_string_Items.getDay()) == parseInt(6)){ //Saturday
					dataRow_TS_Items_Sa = [];
						dataRow_TS_Items_Sa_.push({
					//	'Day':sub_day,
						'Date': nlapiDateToString(date_string_Items),
						'Project': project.Project,
						'Task': project.Task,
						'ServiceItem': project.Event,
						'Hrs': 0,
						'DayLimit': _logValidation(hour_day) ? parseFloat(project.Percent) * parseFloat(hour_day) : '',
						'WeekLimit': hours_week,	'TaskPro': project.Task,
						'ServiceItemPro': project.Event,
						'HolidayCheck': false,
						'LeaveCheck': false,
						'OTHrs': 0,
						'Days': daysList[date_string_Items.getDay()],
						'isOtEligible': ot_flag_
					});
					dataRow_TS_Items_Sa.push({
						'Task': 'Project Activities (Project Task)',
						'Date': nlapiDateToString(date_string_Items),
						'Days': daysList[date_string_Items.getDay()],
						'DayNo': date_string_Items.getDate(),
						'Projects': dataRow_TS_Items_Sa_
					
					});
				}
						
				
						
						
					}
					if(parseInt(i) == parseInt(6) && parseInt(indx) != parseInt(0))
							date_string_Items = date_string;
						else
						date_string_Items = nlapiAddDays(date_string_Items,1);
						indx ++;
					//newTimesheet.commitLineItem('timegrid');
				}
			});
				//Items
				dataRow_TS_Items.push(dataRow_TS_Items_Su);
				dataRow_TS_Items.push(dataRow_TS_Items_Mo);
				dataRow_TS_Items.push(dataRow_TS_Items_Tu);
				dataRow_TS_Items.push(dataRow_TS_Items_We);
				dataRow_TS_Items.push(dataRow_TS_Items_Th);
				dataRow_TS_Items.push(dataRow_TS_Items_Fr);
				dataRow_TS_Items.push(dataRow_TS_Items_Sa);		
				
				//OT
				/*dataRow_OT_Lines_.push(dataRow_OT_Items_Su);
				dataRow_OT_Lines_.push(dataRow_OT_Items_Mo);
				dataRow_OT_Lines_.push(dataRow_OT_Items_Tu);
				dataRow_OT_Lines_.push(dataRow_OT_Items_We);
				dataRow_OT_Lines_.push(dataRow_OT_Items_Th);
				dataRow_OT_Lines_.push(dataRow_OT_Items_Fr);
				dataRow_OT_Lines_.push(dataRow_OT_Items_Sa);	*/
				
			ts_startDate = weekStart;
			var d_ts_startDate = weekStart;
			var ts_startDate_ = weekStart;
			ts_endDate = weekEnd;
			if(ts_startDate)
				ts_startDate = nlapiStringToDate(ts_startDate);
			if(ts_startDate_)
				ts_startDate_ = nlapiStringToDate(ts_startDate_);
			if(ts_endDate)
				ts_endDate = nlapiStringToDate(ts_endDate);
			
			//ts_status = ts_LookUp.approvalstatus;
		//}
		var filters = [];
		/*filters[0] = new nlobjSearchFilter( 'formuladate', null, 'on', startDate);
		filters[0].setFormula('{startdate}');*/
		filters[0] = new nlobjSearchFilter('internalid', 'timesheet', 'anyof', timesheetID);
		
		var columns = [];
		columns.push(new nlobjSearchColumn('internalid'));
		columns.push(new nlobjSearchColumn('date').setSort(false)); //
		columns.push(new nlobjSearchColumn('timesheet'));
		columns.push(new nlobjSearchColumn('employee'));
		columns.push(new nlobjSearchColumn('customer').setSort(true));
		columns.push(new nlobjSearchColumn('internalid','item'));
		columns.push(new nlobjSearchColumn('casetaskevent'));
		columns.push(new nlobjSearchColumn('type'));
		columns.push(new nlobjSearchColumn('hours'));
		
		var timeEntrySearch = searchRecord('timeentry', null, filters , columns);
		
		if(timeEntrySearch){
			
			var JSON_TS_Lines = {};
			var dataRow_TS_Lines =[];
			var JSON_Holiday_Lines = {};
			var dataRow_Holiday_Lines =[];
			var JSON_Leave_Lines = {};
			var dataRow_Leave_Lines =[];
			var JSON_OT_Lines = {};
			var dataRow_OT_Lines =[];
			
			var dataRow_OverAll_TS = [];
			var JSON_ALL = {};
			var dataRow_ALL = [];
			var date_list =[];
			var item_list = [];
			var task_list = [];
			var dataRow_TS_Sun = [];
			var dataRow_TS_Sun_ = [];
			var dataRow_TS_Mon = [];
			var dataRow_TS_Mon_ = [];
			var dataRow_TS_Tue = [];
			var dataRow_TS_Tue_ = [];
			var dataRow_TS_Wed = [];
			var dataRow_TS_Wed_ = [];
			var dataRow_TS_Thur = [];
			var dataRow_TS_Thur_ = [];
			var dataRow_TS_Fri = [];
			var dataRow_TS_Fri_ = [];
			var dataRow_TS_Sat = [];
			var dataRow_TS_Sat_ = [];
				for(var inx=0; inx < timeEntrySearch.length; inx++){
				var task = '';
				var item_ = '';
				var date_T_ = '';
				var date_T = '';
				var customer = '';
				var hours = '';
				var i_customer = '';
				var proj_lookUp = '';
				var hour_day ='';
				var hours_week = '';
				
				task = timeEntrySearch[inx].getText('casetaskevent');
				item_ = timeEntrySearch[inx].getValue('internalid','item');
				date_T_ = timeEntrySearch[inx].getValue('date');
				var day = timeEntrySearch[inx].getValue('day');
				date_T = nlapiStringToDate(date_T_);
				var day_ = date_T.getDay();
				var getTodayDate = date_T.getDate();
				var s_day = daysList[day_];
				customer = timeEntrySearch[inx].getText('customer');
				i_customer = timeEntrySearch[inx].getValue('customer');
				var emp = timeEntrySearch[inx].getValue('employee');
				var emp_subsidiary = nlapiLookupField('employee',emp,'subsidiary');
				if(i_customer){
					proj_lookUp = nlapiLookupField('job',i_customer,['custentity_hoursperday','custentity_hoursperweek','custentity_onsite_hours_per_day','custentity_onsite_hours_per_week']);
					if(emp_subsidiary == 3 || emp_subsidiary == 19)
					{
						hour_day = proj_lookUp.custentity_hoursperday;
						hours_week = proj_lookUp.custentity_hoursperweek;
					}
					else
					{
						hour_day = proj_lookUp.custentity_onsite_hours_per_day;
						hours_week = proj_lookUp.custentity_onsite_hours_per_week;
					}
				}
				hours = timeEntrySearch[inx].getValue('hours');
				
				//Change in structure
				if(parseInt(day_) == parseInt(0)){  //Sunday
					dataRow_TS_Sun = [];
					dataRow_TS_Sun_.push({
					//	'Day':sub_day,
						'Date': date_T_,
						'Project':customer,
						'Task': task +' (Project Task)',
						'ServiceItem': item_,
						'Hrs': hours,
						'DayLimit': hour_day,
						'WeekLimit': hours_week,
						'TaskPro': task +' (Project Task)',
						'ServiceItemPro': item_,
						'HolidayCheck': false,
						'LeaveCheck': false,
						'OTHrs': 0,
						'Days': s_day,
						'isOtEligible': false
					});
					dataRow_TS_Sun.push({
						'Task': 'Project Activities (Project Task)',
						'Date': date_T_,
						'Days': s_day,
						'DayNo': getTodayDate,
						'Projects': dataRow_TS_Sun_
					
					});
				}
				if(parseInt(day_) == parseInt(1)){ //Monday
					dataRow_TS_Mon = [];
					dataRow_TS_Mon_.push({
					//	'Day':sub_day,
						'Date': date_T_,
						'Project':customer,
						'Task': task +' (Project Task)',
						'ServiceItem': item_,
						'Hrs': hours,
						'DayLimit': hour_day,
						'WeekLimit': hours_week,
						'TaskPro': task +' (Project Task)',
						'ServiceItemPro': item_,
						'HolidayCheck': false,
						'LeaveCheck': false,
						'OTHrs': 0,
						'Days': s_day,
						'isOtEligible': false
					});
					dataRow_TS_Mon.push({
						'Task': 'Project Activities (Project Task)',
						'Date': date_T_,
						'Days': s_day,
						'DayNo': getTodayDate,
						'Projects': dataRow_TS_Mon_
					
					});
				}
				if(parseInt(day_) == parseInt(2)){ //Tuesday
					dataRow_TS_Tue= [];
					dataRow_TS_Tue_.push({
					//	'Day':sub_day,
						'Date': date_T_,
						'Project':customer,
						'Task': task +' (Project Task)',
						'ServiceItem': item_,
						'Hrs': hours,
						'DayLimit': hour_day,
						'WeekLimit': hours_week,
						'TaskPro': task +' (Project Task)',
						'ServiceItemPro': item_,
						'HolidayCheck': false,
						'LeaveCheck': false,
						'OTHrs': 0,
						'Days': s_day,
						'isOtEligible': false
					});
					dataRow_TS_Tue.push({
						'Task': 'Project Activities (Project Task)',
						'Date': date_T_,
						'Days': s_day,
						'DayNo': getTodayDate,
						'Projects': dataRow_TS_Tue_
					
					});
				}
				if(parseInt(day_) == parseInt(3)){ //Wednesday
					dataRow_TS_Wed =[];
					dataRow_TS_Wed_.push({
					//	'Day':sub_day,
						'Date': date_T_,
						'Project':customer,
						'Task': task +' (Project Task)',
						'ServiceItem': item_,
						'Hrs': hours,
						'DayLimit': hour_day,
						'WeekLimit': hours_week,
						'TaskPro': task +' (Project Task)',
						'ServiceItemPro': item_,
						'HolidayCheck': false,
						'LeaveCheck': false,
						'OTHrs': 0,
						'Days': s_day,
						'isOtEligible': false
					});
					dataRow_TS_Wed.push({
						'Task': 'Project Activities (Project Task)',
						'Date': date_T_,
						'Days': s_day,
						'DayNo': getTodayDate,
						'Projects': dataRow_TS_Wed_
					
					});
				}
				if(parseInt(day_) == parseInt(4)){ //Thursday
					dataRow_TS_Thur = [];
					dataRow_TS_Thur_.push({
					//	'Day':sub_day,
						'Date': date_T_,
						'Project':customer,
						'Task': task +' (Project Task)',
						'ServiceItem': item_,
						'Hrs': hours,
						'DayLimit': hour_day,
						'WeekLimit': hours_week,
						'TaskPro': task +' (Project Task)',
						'ServiceItemPro': item_,
						'HolidayCheck': false,
						'LeaveCheck': false,
						'OTHrs': 0,
						'Days': s_day,
						'isOtEligible': false
					});
					dataRow_TS_Thur.push({
						'Task': 'Project Activities (Project Task)',
						'Date': date_T_,
						'Days': s_day,
						'DayNo': getTodayDate,
						'Projects': dataRow_TS_Thur_
					
					});
				} 
				if(parseInt(day_) == parseInt(5)){ //Friday
					dataRow_TS_Fri = [];
					dataRow_TS_Fri_.push({
					//	'Day':sub_day,
						'Date': date_T_,
						'Project':customer,
						'Task': task +' (Project Task)',
						'ServiceItem': item_,
						'Hrs': hours,
						'DayLimit': hour_day,
						'WeekLimit': hours_week,
						'TaskPro': task +' (Project Task)',
						'ServiceItemPro': item_,
						'HolidayCheck': false,
						'LeaveCheck': false,
						'OTHrs': 0,
						'Days': s_day,
						'isOtEligible': false
					});
					dataRow_TS_Fri.push({
						'Task': 'Project Activities (Project Task)',
						'Date': date_T_,
						'Days': s_day,
						'DayNo': getTodayDate,
						'Projects': dataRow_TS_Fri_
					
					});
				}
				if(parseInt(day_) == parseInt(6)){ //Saturday
					dataRow_TS_Sat = [];
					dataRow_TS_Sat_.push({
				//		'Day':sub_day,
						'Date': date_T_,
						'Project':customer,
						'Task': task +' (Project Task)',
						'ServiceItem': item_,
						'Hrs': hours,
						'DayLimit': hour_day,
						'WeekLimit': hours_week,
						'TaskPro': task +' (Project Task)',
						'ServiceItemPro': item_,
						'HolidayCheck': false,
						'LeaveCheck': false,
						'OTHrs': 0,
						'Days': s_day,
						'isOtEligible': false
					});
					dataRow_TS_Sat.push({
						'Task': 'Project Activities (Project Task)',
						'Date': date_T_,
						'Days': s_day,
						'DayNo': getTodayDate,
						'Projects': dataRow_TS_Sat_
					
					});
				}
				}	
		}//End of Search
		
				//Concat all TS
				dataRow_TS_Lines.push(dataRow_TS_Sun);
				dataRow_TS_Lines.push(dataRow_TS_Mon);
				dataRow_TS_Lines.push(dataRow_TS_Tue);
				dataRow_TS_Lines.push(dataRow_TS_Wed);
				dataRow_TS_Lines.push(dataRow_TS_Thur);
				dataRow_TS_Lines.push(dataRow_TS_Fri);
				dataRow_TS_Lines.push(dataRow_TS_Sat);
				
				
				JSON_ALL = {
				ServiceItem: dataRow_TS_Lines
				//Holiday: dataRow_Holiday_Lines_,
			//	Leave: dataRow_Leave_Lines_,
				
				//Items: dataRow_TS_Items
					
			};
			dataRow_ALL.push(JSON_ALL);
		
		
	var JSON_Total = {};
	var dataRow_Total = [];

	JSON_Total = {
			Body:dataRow_TS_Body,
			Lines:dataRow_ALL,
		//	OT: dataRow_OT_Lines_,
			Items: dataRow_TS_Items
	
	};
	dataRow_Total.push(JSON_Total);
	var JSON ={};
	var dataRow = [];
	
	//Customer Holiday List
	var customer_list = [];
	var holiday_list = [];
	//GET OT Flag From Allocation startDate, endDate,employeeId
				
		allocationDetails_items.forEach(function(project) {
			var project_internal_id = project.Project;
			var project_holiday = nlapiLookupField('job', project_internal_id,
	        'custentityproject_holiday');
			if(parseInt(project_holiday) != parseInt(1)) {
			customer_list = get_holidays(startDate,
					endDate, project.ProjectID,employeeId,project.Project);
			
			}
			else{
				//Get Holiday List in Week
				holiday_list = get_company_holidays(startDate,endDate,employeeId,project.Project);
			}
		});
	
	JSON ={
			TimeSheetDetails: dataRow_Total,
			HolidayList:holiday_list,
			CustomerHoliday: customer_list
			};
	
	dataRow.push(JSON);
	//}
	return dataRow;
	}		
	catch(err){
		nlapiLogExecution('error', 'Restlet Main Function', err);
		throw err;
	}
		
}


function get_holidays(start_date, end_date, project,employee,Project_ID) {

	var customer_val = nlapiLookupField('job', project,
	        'customer');
			
	var emp_subsidiary = nlapiLookupField('employee', employee, 'subsidiary');
	var cust_holiday_list = [];
	
	//start_date = nlapiDateToString(start_date, 'date');
	//end_date = nlapiDateToString(end_date, 'date');

	 nlapiLogExecution('debug', 'start_date', start_date);
	 nlapiLogExecution('debug', 'end_date', end_date);
	 nlapiLogExecution('debug', 'subsidiary', subsidiary);
	 nlapiLogExecution('debug', 'customer', customer);

	var search_customer_holiday = nlapiSearchRecord(
	        'customrecordcustomerholiday', 'customsearch_customer_holiday', [
	                new nlobjSearchFilter('custrecordholidaydate', null,
	                        'within', start_date, end_date),
	                new nlobjSearchFilter('custrecordcustomersubsidiary', null,
	                        'anyof', emp_subsidiary),
	                new nlobjSearchFilter('custrecord13', null, 'anyof',
	                        customer_val) ], [ new nlobjSearchColumn(
	                'custrecordholidaydate', null, 'group') ]);

	if (search_customer_holiday) {

		for (var i = 0; i < search_customer_holiday.length; i++) {
			cust_holiday_list.push({
			'ProjectID':	Project_ID,
			'Date': search_customer_holiday[i].getValue(
			        'custrecordholidaydate', null, 'group')
			
			});
		}
	}
}


function validateTS(employeeId,startDate){
	try{
		var filters = [];
		filters[0] = new nlobjSearchFilter( 'formuladate', null, 'on', startDate);
		filters[0].setFormula('{startdate}');
		filters[1] = new nlobjSearchFilter('employee', null, 'anyof', employeeId);
		
		var timeEntrySearch = searchRecord('timesheet', null, filters , 
			                                       		        [
			                                       		        new nlobjSearchColumn('internalid'),
			                                    		        ]);
			if(_logValidation(timeEntrySearch))   
			nlapiLogExecution('debug', 'timeEntrySearch',timeEntrySearch.length);
		return timeEntrySearch;
	}
	catch(err){
		nlapiLogExecution('error', 'Restlet TS Function', err);
		throw err;
	}
}
function allocationDetails(dataIn,employeeId,startDate,endDate){
	
try{
	var date_string = startDate;
	//var OT_Applicable = '';
	var OT_Applicable = [];
	var time_approver_email = '';
	// get active allocations, hours ,etc.
	var allocationDetails = getActiveAllocations(startDate, endDate,
			employeeId);
	
	var allocationDetails_items = getActiveAllocations_Items(startDate, endDate,
			employeeId);
	
		//Get Holiday List in Week
	//var holiday_list = get_company_holidays(startDate,endDate,employeeId);
	//Check if timesheet already Exist
	
	// create an open timesheet
	var timesheetId = autoGenerateTimesheet(startDate, employeeId,
			allocationDetails);
	if(timesheetId){
	//Load to Get TS Data
		var load_TS = nlapiLoadRecord('timesheet',timesheetId);
		var emp = load_TS.getFieldText('employee');
		var i_emp = load_TS.getFieldValue('employee');
		var emp_TS_approver = load_TS.getFieldText('custrecord_ts_time_approver');
		var weekStart = load_TS.getFieldValue('startdate');
		var weekEnd = load_TS.getFieldValue('enddate');
		var totalHrs = load_TS.getFieldValue('totalhours');
		var approvalstatus = load_TS.getFieldText('approvalstatus');
		//GET OT Flag From Allocation
		allocationDetails_items.forEach(function(project) {
			OT_Applicable.push({
				'OT_Applicable': project.OT,
				'Project': project.Project
			});
		//	OT_Applicable = project.OT;
			//Project
		});
		var s_hireDate = nlapiLookupField('employee',parseInt(i_emp),['hiredate','timeapprover']);
		if(s_hireDate.timeapprover)
		time_approver_email = nlapiLookupField('employee',s_hireDate.timeapprover,'email');
		
		var JSON_TS_Body = {};
		var dataRow_TS_Body =[];
		
		JSON_TS_Body = {
		TimeSheetID: timesheetId,		
		Employee:emp,
		timesheetApprover :  _logValidation(time_approver_email) == false ? ' ' : time_approver_email.split('@')[0],
		HireDate: s_hireDate.hiredate,
		TSApprover: emp_TS_approver,
		WeekStartDate: weekStart,
		WeekEndDate: weekEnd,
		TotalHours: totalHrs,
		Status: approvalstatus,
		TimeSheetBlock: false,
		TaskHoliday: 'Holiday (Project Task)',
			TaskLeave: 'Leave (Project Task)',
			TaskOT: 'OT (Project Task)',
			ServiceItemHoliday: '2480',
			ServiceItemLeave: '2479',
			ServiceItemOT: '2425',
			DayLimit: 8,
			WeekLimit: 40		
		};
		dataRow_TS_Body.push(JSON_TS_Body);
		
		
		//For Holiday Leave OT
		var JSON_Holiday_Lines_ = {};
		var dataRow_Holiday_Lines_ =[];
		var JSON_Leave_Lines_ = {};
		var dataRow_Leave_Lines_ =[];
		var JSON_OT_Lines_ = {};
		var dataRow_OT_Lines_ =[];
		var JSON_Items ={};
		var dataRow_Items = [];
		var date_T_T = weekStart;
		//var date_string = nlapiStringToDate(date_string);
		var date_string_Items = startDate;
			
		date_string = nlapiStringToDate(date_string);  	
			
		//END
			var indx = 0;
			var dataRow_TS_Items_Su = [];
			var dataRow_TS_Items_Su_ = [];
			var dataRow_TS_Items_Mo = [];
			var dataRow_TS_Items_Mo_ = [];
			var dataRow_TS_Items_Tu = [];
			var dataRow_TS_Items_Tu_ = [];
			var dataRow_TS_Items_We = [];
			var dataRow_TS_Items_We_ = [];
			var dataRow_TS_Items_Th = [];
			var dataRow_TS_Items_Th_ = [];
			var dataRow_TS_Items_Fr = [];
			var dataRow_TS_Items_Fr_ = [];
			var dataRow_TS_Items_Sa = [];
			var dataRow_TS_Items_Sa_ = [];
			
			var dataRow_OT_Items_Su_alloc = [];
			var dataRow_OT_Items_Su_alloc_ = [];
			var dataRow_OT_Items_Mo_alloc = [];
			var dataRow_OT_Items_Mo_alloc_ = [];
			var dataRow_OT_Items_Tu_alloc = [];
			var dataRow_OT_Items_Tu_alloc_ = [];
			var dataRow_OT_Items_We_alloc = [];
			var dataRow_OT_Items_We_alloc_ = [];
			var dataRow_OT_Items_Th_alloc = [];
			var dataRow_OT_Items_Th_alloc_ = [];
			var dataRow_OT_Items_Fr_alloc = [];
			var dataRow_OT_Items_Fr_alloc_ = [];
			var dataRow_OT_Items_Sa_alloc = [];
			var dataRow_OT_Items_Sa_alloc_ = [];
			var hour_day ='';
			var hours_week ='';
			var dataRow_TS_Items = [];
			date_string_Items = nlapiStringToDate(date_string_Items);
			allocationDetails_items.forEach(function(project) {
				var iterate_flag = true;
			//	newTimesheet.selectNewLineItem('timegrid');
				if(project.Project && iterate_flag == true){
					proj_lookUp = nlapiLookupField('job',parseInt(project.ProjectID),['custentity_hoursperday','custentity_hoursperweek','custentity_onsite_hours_per_day','custentity_onsite_hours_per_week']);
					var emp_subsidiary = nlapiLookupField('employee',parseInt(i_emp),'subsidiary');
					if(emp_subsidiary == 3)
					{
						hour_day = proj_lookUp.custentity_hoursperday;
						if(!_logValidation(hour_day))
							hour_day = '';
					
						hours_week = proj_lookUp.custentity_hoursperweek;
						if(!_logValidation(hours_week))
							hours_week = 40;
					}
					else
					{
						hour_day = proj_lookUp.custentity_onsite_hours_per_day;
						if(!_logValidation(hour_day))
							hour_day = '';
					
						hours_week = proj_lookUp.custentity_onsite_hours_per_week;
						if(!_logValidation(hours_week))
							hours_week = 40;
					}
					
					iterate_flag = false;
				}
				var ot_flag_ = false;
				for(var ot_ind=0;ot_ind < OT_Applicable.length;ot_ind++){
					if(OT_Applicable[ot_ind].OT_Applicable == 'YES' && OT_Applicable[ot_ind].Project == project.Project)
					{	
						ot_flag_ = true;
					}
				}
				
				for (var i = 0; i < 7; i++) {
				
					if (isNumber(project.Hours[i])) 
					{
							
						
						//Change in structure
					if(parseInt(date_string_Items.getDay()) == parseInt(0)){  //Sunday
					dataRow_TS_Items_Su = [];
						dataRow_TS_Items_Su_.push({
					//	'Day':sub_day,
						'Date': nlapiDateToString(date_string_Items),
						'Project': project.Project,
						'Task': project.Task,
						'ServiceItem': project.Event,
						'Hrs': 0,
						'DayLimit': _logValidation(hour_day) ? parseFloat(project.Percent) * parseFloat(hour_day) : '',
						'WeekLimit': hours_week,
						'TaskPro': project.Task,
						'ServiceItemPro': project.Event,
						'HolidayCheck': false,
						'LeaveCheck': false,
						'OTHrs': 0,
						'Days': daysList[date_string_Items.getDay()],
						'isOtEligible': ot_flag_
					});
					dataRow_TS_Items_Su.push({
						'Task': 'Project Activities (Project Task)',
						'Date': nlapiDateToString(date_string_Items),
						'Days': daysList[date_string_Items.getDay()],
						'DayNo': date_string_Items.getDate(),
						'Projects': dataRow_TS_Items_Su_
					
					});
				}
				if(parseInt(date_string_Items.getDay()) == parseInt(1)){ //Monday
					dataRow_TS_Items_Mo = [];
						dataRow_TS_Items_Mo_.push({
					//	'Day':sub_day,
						'Date': nlapiDateToString(date_string_Items),
						'Project': project.Project,
						'Task': project.Task,
						'ServiceItem': project.Event,
						'Hrs': 0,
						'DayLimit': _logValidation(hour_day) ? parseFloat(project.Percent) * parseFloat(hour_day) : '',
						'WeekLimit': hours_week,	'TaskPro': project.Task,
						'ServiceItemPro': project.Event,
						'HolidayCheck': false,
						'LeaveCheck': false,
						'OTHrs': 0,
						'Days': daysList[date_string_Items.getDay()],
						'isOtEligible': ot_flag_
					});
					dataRow_TS_Items_Mo.push({
						'Task': 'Project Activities (Project Task)',
						'Date': nlapiDateToString(date_string_Items),
						'Days': daysList[date_string_Items.getDay()],
						'DayNo': date_string_Items.getDate(),
						'Projects': dataRow_TS_Items_Mo_
					
					});
				}
				if(parseInt(date_string_Items.getDay()) == parseInt(2)){ //Tuesday
					dataRow_TS_Items_Tu = [];
						dataRow_TS_Items_Tu_.push({
					//	'Day':sub_day,
						'Date': nlapiDateToString(date_string_Items),
						'Project': project.Project,
						'Task': project.Task,
						'ServiceItem': project.Event,
						'Hrs': 0,
						'DayLimit': _logValidation(hour_day) ? parseFloat(project.Percent) * parseFloat(hour_day) : '',
						'WeekLimit': hours_week,	'TaskPro': project.Task,
						'ServiceItemPro': project.Event,
						'HolidayCheck': false,
						'LeaveCheck': false,
						'OTHrs': 0,
						'Days': daysList[date_string_Items.getDay()],
						'isOtEligible': ot_flag_
					});
					dataRow_TS_Items_Tu.push({
						'Task': 'Project Activities (Project Task)',
						'Date': nlapiDateToString(date_string_Items),
						'Days': daysList[date_string_Items.getDay()],
						'DayNo': date_string_Items.getDate(),
						'Projects': dataRow_TS_Items_Tu_
					
					});
				}
				if(parseInt(date_string_Items.getDay()) == parseInt(3)){ //Wednesday
					dataRow_TS_Items_We = [];
						dataRow_TS_Items_We_.push({
					//	'Day':sub_day,
						'Date': nlapiDateToString(date_string_Items),
						'Project': project.Project,
						'Task': project.Task,
						'ServiceItem': project.Event,
						'Hrs': 0,
						'DayLimit': _logValidation(hour_day) ? parseFloat(project.Percent) * parseFloat(hour_day) : '',
						'WeekLimit': hours_week,	'TaskPro': project.Task,
						'ServiceItemPro': project.Event,
						'HolidayCheck': false,
						'LeaveCheck': false,
						'OTHrs': 0,
						'Days': daysList[date_string_Items.getDay()],
						'isOtEligible': ot_flag_
					});
					dataRow_TS_Items_We.push({
						'Task': 'Project Activities (Project Task)',
						'Date': nlapiDateToString(date_string_Items),
						'Days': daysList[date_string_Items.getDay()],
						'DayNo': date_string_Items.getDate(),
						'Projects': dataRow_TS_Items_We_
					
					});
				}
				if(parseInt(date_string_Items.getDay()) == parseInt(4)){ //Thursday
					dataRow_TS_Items_Th = [];
						dataRow_TS_Items_Th_.push({
					//	'Day':sub_day,
						'Date': nlapiDateToString(date_string_Items),
						'Project': project.Project,
						'Task': project.Task,
						'ServiceItem': project.Event,
						'Hrs': 0,
						'DayLimit': _logValidation(hour_day) ? parseFloat(project.Percent) * parseFloat(hour_day) : '',
						'WeekLimit': hours_week,	'TaskPro': project.Task,
						'ServiceItemPro': project.Event,
						'HolidayCheck': false,
						'LeaveCheck': false,
						'OTHrs': 0,
						'Days': daysList[date_string_Items.getDay()],
						'isOtEligible': ot_flag_
					});
					dataRow_TS_Items_Th.push({
						'Task': 'Project Activities (Project Task)',
						'Date': nlapiDateToString(date_string_Items),
						'Days': daysList[date_string_Items.getDay()],
						'DayNo': date_string_Items.getDate(),
						'Projects': dataRow_TS_Items_Th_
					
					});
				} 
				if(parseInt(date_string_Items.getDay()) == parseInt(5)){ //Friday
					dataRow_TS_Items_Fr = [];
						dataRow_TS_Items_Fr_.push({
					//	'Day':sub_day,
						'Date': nlapiDateToString(date_string_Items),
						'Project': project.Project,
						'Task': project.Task,
						'ServiceItem': project.Event,
						'Hrs': 0,
						'DayLimit': _logValidation(hour_day) ? parseFloat(project.Percent) * parseFloat(hour_day) : '',
						'WeekLimit': hours_week,	'TaskPro': project.Task,
						'ServiceItemPro': project.Event,
						'HolidayCheck': false,
						'LeaveCheck': false,
						'OTHrs': 0,
						'Days': daysList[date_string_Items.getDay()],
						'isOtEligible': ot_flag_
					});
					dataRow_TS_Items_Fr.push({
						'Task': 'Project Activities (Project Task)',
						'Date': nlapiDateToString(date_string_Items),
						'Days': daysList[date_string_Items.getDay()],
						'DayNo': date_string_Items.getDate(),
						'Projects': dataRow_TS_Items_Fr_
					
					});
				}
				if(parseInt(date_string_Items.getDay()) == parseInt(6)){ //Saturday
					dataRow_TS_Items_Sa = [];
						dataRow_TS_Items_Sa_.push({
					//	'Day':sub_day,
						'Date': nlapiDateToString(date_string_Items),
						'Project': project.Project,
						'Task': project.Task,
						'ServiceItem': project.Event,
						'Hrs': 0,
						'DayLimit': _logValidation(hour_day) ? parseFloat(project.Percent) * parseFloat(hour_day) : '',
						'WeekLimit': hours_week,	'TaskPro': project.Task,
						'ServiceItemPro': project.Event,
						'HolidayCheck': false,
						'LeaveCheck': false,
						'OTHrs': 0,
						'Days': daysList[date_string_Items.getDay()],
						'isOtEligible': ot_flag_
					});
					dataRow_TS_Items_Sa.push({
						'Task': 'Project Activities (Project Task)',
						'Date': nlapiDateToString(date_string_Items),
						'Days': daysList[date_string_Items.getDay()],
						'DayNo': date_string_Items.getDate(),
						'Projects': dataRow_TS_Items_Sa_
					
					});
				}
						
				
						
						
					}
					if(parseInt(i) == parseInt(6) && parseInt(indx) != parseInt(0))
							date_string_Items = date_string;
						else
						date_string_Items = nlapiAddDays(date_string_Items,1);
						indx ++;
					//newTimesheet.commitLineItem('timegrid');
				}
			});
				dataRow_TS_Items.push(dataRow_TS_Items_Su);
				dataRow_TS_Items.push(dataRow_TS_Items_Mo);
				dataRow_TS_Items.push(dataRow_TS_Items_Tu);
				dataRow_TS_Items.push(dataRow_TS_Items_We);
				dataRow_TS_Items.push(dataRow_TS_Items_Th);
				dataRow_TS_Items.push(dataRow_TS_Items_Fr);
				dataRow_TS_Items.push(dataRow_TS_Items_Sa);		
			
				//OT
				/*dataRow_OT_Lines_.push(dataRow_OT_Items_Su_alloc);
				dataRow_OT_Lines_.push(dataRow_OT_Items_Mo_alloc);
				dataRow_OT_Lines_.push(dataRow_OT_Items_Tu_alloc);
				dataRow_OT_Lines_.push(dataRow_OT_Items_We_alloc);
				dataRow_OT_Lines_.push(dataRow_OT_Items_Th_alloc);
				dataRow_OT_Lines_.push(dataRow_OT_Items_Fr_alloc);
				dataRow_OT_Lines_.push(dataRow_OT_Items_Sa_alloc);*/
				
		var filters = [];
		/*filters[0] = new nlobjSearchFilter( 'formuladate', null, 'on', startDate);
		filters[0].setFormula('{startdate}');*/
		filters[0] = new nlobjSearchFilter('internalid', 'timesheet', 'anyof', timesheetId);
		
		var columns = [];
		columns.push(new nlobjSearchColumn('internalid'));
		columns.push(new nlobjSearchColumn('date').setSort(false)); //
		columns.push(new nlobjSearchColumn('timesheet'));
		columns.push(new nlobjSearchColumn('employee'));
		columns.push(new nlobjSearchColumn('customer').setSort(true));
		columns.push(new nlobjSearchColumn('internalid','item'));
		columns.push(new nlobjSearchColumn('casetaskevent'));
		columns.push(new nlobjSearchColumn('type'));
		columns.push(new nlobjSearchColumn('hours'));
		
		var timeEntrySearch = searchRecord('timeentry', null, filters , columns);
		
		if(timeEntrySearch){
			
			var JSON_TS_Lines = {};
			var dataRow_TS_Lines =[];
		//	var JSON_Holiday_Lines_ = {};
			//var dataRow_Holiday_Lines_ =[];
			//var JSON_Leave_Lines_ = {};
			//var dataRow_Leave_Lines_ =[];
			//var JSON_OT_Lines_ = {};
		//	var dataRow_OT_Lines_ =[];
			
			var dataRow_OverAll_TS = [];
			var JSON_ALL = {};
			var dataRow_ALL = [];
			var date_list =[];
			var item_list = [];
			var task_list = [];
			var dataRow_TS_Sun = [];
			var dataRow_TS_Sun_ = [];
			var dataRow_TS_Mon = [];
			var dataRow_TS_Mon_ = [];
			var dataRow_TS_Tue = [];
			var dataRow_TS_Tue_ = [];
			var dataRow_TS_Wed = [];
			var dataRow_TS_Wed_ = [];
			var dataRow_TS_Thur = [];
			var dataRow_TS_Thur_ = [];
			var dataRow_TS_Fri = [];
			var dataRow_TS_Fri_ = [];
			var dataRow_TS_Sat = [];
			var dataRow_TS_Sat_ = [];
			
			for(var inx=0; inx < timeEntrySearch.length; inx++){
				var task = '';
				var item_ = '';
				var date_T_ = '';
				var date_T = '';
				var customer = '';
				var hours = '';
				var i_customer = '';
				var proj_lookUp = '';
				var hour_day = '';
				var hours_week = '';
				task = timeEntrySearch[inx].getText('casetaskevent');
				item_ = timeEntrySearch[inx].getValue('internalid','item');
				date_T_ = timeEntrySearch[inx].getValue('date');
				//var day = timeEntrySearch[inx].getValue('day');
				//date_T_ = timeEntrySearch[inx].getValue('date');
				//var day = timeEntrySearch[inx].getValue('day');
				
				
				date_T = nlapiStringToDate(date_T_);
				var day_ = date_T.getDay();
				var getTodayDate = date_T.getDate();
				var s_day = daysList[day_];
				customer = timeEntrySearch[inx].getText('customer');
				hours = timeEntrySearch[inx].getValue('hours');
				i_customer = timeEntrySearch[inx].getValue('customer');
				
				if(i_customer){
					proj_lookUp = nlapiLookupField('job',i_customer,['custentity_hoursperday','custentity_hoursperweek','custentity_onsite_hours_per_day','custentity_onsite_hours_per_week']);
					var emp_subsidiary = nlapiLookupField('employee',parseInt(i_emp),'subsidiary');
					if(emp_subsidiary == 3)
					{
						hour_day = proj_lookUp.custentity_hoursperday;
						if(!_logValidation(hour_day))
							hour_day = '';
					
						hours_week = proj_lookUp.custentity_hoursperweek;
						if(!_logValidation(hours_week))
							hours_week = 40;
					}
					else
					{
						hour_day = proj_lookUp.custentity_onsite_hours_per_day;
						if(!_logValidation(hour_day))
							hour_day = '';
					
						hours_week = proj_lookUp.custentity_onsite_hours_per_week;
						if(!_logValidation(hours_week))
							hours_week = 40;
					}
				}
				//Change in structure
				if(parseInt(day_) == parseInt(0)){  //Sunday
					dataRow_TS_Sun = [];
					dataRow_TS_Sun_.push({
					//	'Day':sub_day,
						'Date': date_T_,
						'Project':customer,
						'Task': task +' (Project Task)',
						'ServiceItem': item_,
						'Hrs': hours,
						'DayLimit': hour_day,
						'WeekLimit': hours_week,
						'TaskPro': task +' (Project Task)',
						'ServiceItemPro': item_,
						'HolidayCheck': false,
						'LeaveCheck': false,
						'OTHrs': 0,
						'Days': s_day,
						'isOtEligible': false
					});
					dataRow_TS_Sun.push({
						'Task': 'Project Activities (Project Task)',
						'Date': date_T_,
						'Days': s_day,
						'DayNo': getTodayDate,
						'Projects': dataRow_TS_Sun_
					
					});
				}
				if(parseInt(day_) == parseInt(1)){ //Monday
					dataRow_TS_Mon = [];
					dataRow_TS_Mon_.push({
					//	'Day':sub_day,
						'Date': date_T_,
						'Project':customer,
						'Task': task +' (Project Task)',
						'ServiceItem': item_,
						'Hrs': hours,
						'DayLimit': hour_day,
						'WeekLimit': hours_week,
						'TaskPro': task +' (Project Task)',
						'ServiceItemPro': item_,
						'HolidayCheck': false,
						'LeaveCheck': false,
						'OTHrs': 0,
						'Days': s_day,
						'isOtEligible': false
					});
					dataRow_TS_Mon.push({
						'Task': 'Project Activities (Project Task)',
						'Date': date_T_,
						'Days': s_day,
						'DayNo': getTodayDate,
						'Projects': dataRow_TS_Mon_
					
					});
				}
				if(parseInt(day_) == parseInt(2)){ //Tuesday
					dataRow_TS_Tue= [];
					dataRow_TS_Tue_.push({
					//	'Day':sub_day,
						'Date': date_T_,
						'Project':customer,
						'Task': task +' (Project Task)',
						'ServiceItem': item_,
						'Hrs': hours,
						'DayLimit': hour_day,
						'WeekLimit': hours_week,
						'TaskPro': task +' (Project Task)',
						'ServiceItemPro': item_,
						'HolidayCheck': false,
						'LeaveCheck': false,
						'OTHrs': 0,
						'Days': s_day,
						'isOtEligible': false
					});
					dataRow_TS_Tue.push({
						'Task': 'Project Activities (Project Task)',
						'Date': date_T_,
						'Days': s_day,
						'DayNo': getTodayDate,
						'Projects': dataRow_TS_Tue_
					
					});
				}
				if(parseInt(day_) == parseInt(3)){ //Wednesday
					dataRow_TS_Wed =[];
					dataRow_TS_Wed_.push({
					//	'Day':sub_day,
						'Date': date_T_,
						'Project':customer,
						'Task': task +' (Project Task)',
						'ServiceItem': item_,
						'Hrs': hours,
						'DayLimit': hour_day,
						'WeekLimit': hours_week,
						'TaskPro': task +' (Project Task)',
						'ServiceItemPro': item_,
						'HolidayCheck': false,
						'LeaveCheck': false,
						'OTHrs': 0,
						'Days': s_day,
						'isOtEligible': false
					});
					dataRow_TS_Wed.push({
						'Task': 'Project Activities (Project Task)',
						'Date': date_T_,
						'Days': s_day,
						'DayNo': getTodayDate,
						'Projects': dataRow_TS_Wed_
					
					});
				}
				if(parseInt(day_) == parseInt(4)){ //Thursday
					dataRow_TS_Thur = [];
					dataRow_TS_Thur_.push({
					//	'Day':sub_day,
						'Date': date_T_,
						'Project':customer,
						'Task': task +' (Project Task)',
						'ServiceItem': item_,
						'Hrs': hours,
						'DayLimit': hour_day,
						'WeekLimit': hours_week,
						'TaskPro': task +' (Project Task)',
						'ServiceItemPro': item_,
						'HolidayCheck': false,
						'LeaveCheck': false,
						'OTHrs': 0,
						'Days': s_day,
						'isOtEligible': false
					});
					dataRow_TS_Thur.push({
						'Task': 'Project Activities (Project Task)',
						'Date': date_T_,
						'Days': s_day,
						'DayNo': getTodayDate,
						'Projects': dataRow_TS_Thur_
					
					});
				} 
				if(parseInt(day_) == parseInt(5)){ //Friday
					dataRow_TS_Fri = [];
					dataRow_TS_Fri_.push({
					//	'Day':sub_day,
						'Date': date_T_,
						'Project':customer,
						'Task': task +' (Project Task)',
						'ServiceItem': item_,
						'Hrs': hours,
						'DayLimit': hour_day,
						'WeekLimit': hours_week,
						'TaskPro': task +' (Project Task)',
						'ServiceItemPro': item_,
						'HolidayCheck': false,
						'LeaveCheck': false,
						'OTHrs': 0,
						'Days': s_day,
						'isOtEligible': false
					});
					dataRow_TS_Fri.push({
						'Task': 'Project Activities (Project Task)',
						'Date': date_T_,
						'Days': s_day,
						'DayNo': getTodayDate,
						'Projects': dataRow_TS_Fri_
					
					});
				}
				if(parseInt(day_) == parseInt(6)){ //Saturday
					dataRow_TS_Sat = [];
					dataRow_TS_Sat_.push({
				//		'Day':sub_day,
						'Date': date_T_,
						'Project':customer,
						'Task': task +' (Project Task)',
						'ServiceItem': item_,
						'Hrs': hours,
						'DayLimit': hour_day,
						'WeekLimit': hours_week,
						'TaskPro': task +' (Project Task)',
						'ServiceItemPro': item_,
						'HolidayCheck': false,
						'LeaveCheck': false,
						'OTHrs': 0,
						'Days': s_day,
						'isOtEligible': false
					});
					dataRow_TS_Sat.push({
						'Task': 'Project Activities (Project Task)',
						'Date': date_T_,
						'Days': s_day,
						'DayNo': getTodayDate,
						'Projects': dataRow_TS_Sat_
					
					});
				}
				
			}//End of For Loop
			
		}//End of search
		
		//Concat all TS
		dataRow_TS_Lines.push(dataRow_TS_Sun);
		dataRow_TS_Lines.push(dataRow_TS_Mon);
		dataRow_TS_Lines.push(dataRow_TS_Tue);
		dataRow_TS_Lines.push(dataRow_TS_Wed);
		dataRow_TS_Lines.push(dataRow_TS_Thur);
		dataRow_TS_Lines.push(dataRow_TS_Fri);
		dataRow_TS_Lines.push(dataRow_TS_Sat);
		
		/*var date_T_T = nlapiStringToDate(startDate);
		for(var ind =0;ind<7;ind++){
			//Holiday JSON
			JSON_Holiday_Lines_ = {
					
					
					Date: nlapiDateToString(date_T_T),
					Project:customer,
					Task: 'Holiday (Project Task)',
					ServiceItem: 2480,
					Hrs: '0:00'
					};
			dataRow_Holiday_Lines_.push(JSON_Holiday_Lines_);
			//Leave
			JSON_Leave_Lines_ = {
					
					
					Date: nlapiDateToString(date_T_T),
					Project:customer,
					Task: 'Leave (Project Task)',
					ServiceItem: 2479,
					Hrs: '0:00'
					};
			dataRow_Leave_Lines_.push(JSON_Leave_Lines_);
			
			if(OT_Applicable == 'YES'){
				JSON_OT_Lines_ = {
						
						
						Date: nlapiDateToString(date_T_T),
						Project:customer,
						Task: 'OT (Project Task)',
						ServiceItem: 2425,
						Hrs: '0:00'
						};
				dataRow_OT_Lines_.push(JSON_OT_Lines_);
			}
			
			date_T_T = nlapiAddDays(date_T_T,1);
			//nlapiLogExecution('DEBUG','date_T_T',date_T_T);
			
		}*/
				JSON_ALL = {
				ServiceItem: dataRow_TS_Lines
				//Holiday: dataRow_Holiday_Lines_,
				//Leave: dataRow_Leave_Lines_,
				
				//Items: dataRow_Items
				
					
			};
			dataRow_ALL.push(JSON_ALL);
		/*var timeEntrySearch = searchRecord('timesheet', null, filters , 
			                                       		        [
			                                       		        new nlobjSearchColumn('internalid'),
			                                       		        ]);
		if(timeEntrySearch) 
			nlapiLogExecution('debug', 'timeEntrySearch',timeEntrySearch.length);
		if(timeEntrySearch){
			var timesheetID = timeEntrySearch[0].getValue('internalid');
			
			if(timesheetID){
				//Load to Get TS Data
					var load_TS = nlapiLoadRecord('timesheet',timesheetID);
					var emp = load_TS.getFieldText('employee');
					var emp_id = load_TS.getFieldValue('employee');
					var emp_lookUP = nlapiLookupField('employee',emp_id,['email']);
					var emp_email = emp_lookUP.email;
					var emp_TS_approver = load_TS.getFieldText('custrecord_ts_time_approver');
					var weekStart = load_TS.getFieldValue('startdate');
					var weekEnd = load_TS.getFieldValue('enddate');
					var totalHrs = load_TS.getFieldValue('totalhours');
					var approvalstatus = load_TS.getFieldText('approvalstatus');
					
					//GET OT Flag From Allocation
					allocationDetails.forEach(function(project) {
						OT_Applicable = project.OT;
					});
					
					var JSON_TS_Body = {};
					var dataRow_TS_Body =[];
					
					JSON_TS_Body = {
					Employee: emp,
					Email: emp_email,
					TSApprover: emp_TS_approver,
					WeekStartDate: weekStart,
					WeekEndDate: weekEnd,
					TotalHours: totalHrs,
					Status: approvalstatus					
					};
					dataRow_TS_Body.push(JSON_TS_Body);
					
					var JSON_TS_Lines = {};
					var dataRow_TS_Lines =[];
					var JSON_Holiday_Lines = {};
					var dataRow_Holiday_Lines =[];
					var JSON_Leave_Lines = {};
					var dataRow_Leave_Lines =[];
					var JSON_OT_Lines = {};
					var dataRow_OT_Lines =[];
					
					var dataRow_OverAll_TS = [];
					var JSON_ALL = {};
					var dataRow_ALL = [];
					var lineCount = load_TS.getLineItemCount('timegrid');
					for(var inx=1;inx<=lineCount;inx++){
						
						var JSON_TS_Lines = {};
						var dataRow_TS_Lines =[];
						var JSON_Holiday_Lines = {};
						var dataRow_Holiday_Lines =[];
						var JSON_Leave_Lines = {};
						var dataRow_Leave_Lines =[];
						var JSON_OT_Lines = {};
						var dataRow_OT_Lines =[];
						
						
						
						load_TS.selectLineItem('timegrid',inx);	
					//var project = load_TS.getLineItemText('timegrid','customer',inx);
					//var task = load_TS.getLineItemValue('timegrid','casetaskevent',inx);
					//var service_item = load_TS.getLineItemValue('timegrid','item',inx);
					
					
					for(var n=0;n<7;n++){
						var o_sub_record_view = ''
						var sub_day = days[n];
						if(sub_day == 'saturday'){
							day_Date = nlapiStringToDate(day_Date);
							day_Date = nlapiAddDays(day_Date, 1);
							day_Date = nlapiDateToString(day_Date);
							//Service Item
							JSON_TS_Lines = {
									
								Day:sub_day,
								Date: day_Date,
								Project:project,
								Task: task,
								ServiceItem: service_item,
								Hrs: '0:00',
								Leave_Temp:'',
								Holiday_Temp:''
								};
							dataRow_TS_Lines.push(JSON_TS_Lines);
							//Holiday
							JSON_Holiday_Lines = {
									
									Day:sub_day,
									Date: day_Date,
									Project:project,
									Task: 'Holiday (Project Task)',
									ServiceItem: 2480,
									Hrs: '0:00'
									};
							dataRow_Holiday_Lines.push(JSON_Holiday_Lines);
							//Leave	
							JSON_Leave_Lines = {
										
										Day:sub_day,
										Date: day_Date,
										Project:project,
										Task: 'Leave (Project Task)',
										ServiceItem: 2479,
										Hrs: '0:00'
										};
							dataRow_Leave_Lines.push(JSON_Leave_Lines);
							//OT data
							if(OT_Applicable == 'YES'){
								JSON_OT_Lines = {
										
										Day:sub_day,
										Date: day_Date,
										Project:project,
										Task: 'OT (Project Task)',
										ServiceItem: 2425,
										Hrs: '0:00'
										};
								dataRow_OT_Lines.push(JSON_OT_Lines);
							}
						}
					
						
						
						if(sub_day == 'sunday'){
							o_sub_record_view = load_TS.viewCurrentLineItemSubrecord('timegrid', days[1]);
							if(o_sub_record_view)
								o_sub_record_view = load_TS.viewCurrentLineItemSubrecord('timegrid', days[2]);
							else if(o_sub_record_view)
								o_sub_record_view = load_TS.viewCurrentLineItemSubrecord('timegrid', days[3]);
							else if(o_sub_record_view)
								o_sub_record_view = load_TS.viewCurrentLineItemSubrecord('timegrid', days[4]);
							else if(o_sub_record_view)
								o_sub_record_view = load_TS.viewCurrentLineItemSubrecord('timegrid', days[5]);
							if(o_sub_record_view){
								var project = o_sub_record_view.getFieldText('customer');
								var task = o_sub_record_view.getFieldText('casetaskevent');
								var service_item = o_sub_record_view.getFieldValue('item');
								var hrs_sub = o_sub_record_view.getFieldValue('hours');  
  
								var day_Date = o_sub_record_view.getFieldValue('day');
								day_Date = nlapiStringToDate(day_Date);
								day_Date = nlapiAddDays(day_Date, -1);
								day_Date = nlapiDateToString(day_Date);
								//Service Item
								JSON_TS_Lines = {
										
									Day:sub_day,
									Date: day_Date,
									Project:project,
									Task: task,
									ServiceItem: service_item,
									Hrs: '0:00',
									Leave_Temp:'',
									Holiday_Temp:''
									};
								dataRow_TS_Lines.push(JSON_TS_Lines);
								//Holiday
								JSON_Holiday_Lines = {
										
										Day:sub_day,
										Date: day_Date,
										Project:project,
										Task: 'Holiday (Project Task)',
										ServiceItem: 2480,
										Hrs: '0:00'
										};
								dataRow_Holiday_Lines.push(JSON_Holiday_Lines);
								//Leave	
								JSON_Leave_Lines = {
											
											Day:sub_day,
											Date: day_Date,
											Project:project,
											Task: 'Leave (Project Task)',
											ServiceItem: 2479,
											Hrs: '0:00'
											};
								dataRow_Leave_Lines.push(JSON_Leave_Lines);
								//OT data
								if(OT_Applicable == 'YES'){
									JSON_OT_Lines = {
											
											Day:sub_day,
											Date: day_Date,
											Project:project,
											Task: 'OT (Project Task)',
											ServiceItem: 2425,
											Hrs: '0:00'
											};
									dataRow_OT_Lines.push(JSON_OT_Lines);
								}
							}
						}
						o_sub_record_view = load_TS.viewCurrentLineItemSubrecord('timegrid', sub_day);
						if(o_sub_record_view){
						var project = o_sub_record_view.getFieldText('customer');
						var task = o_sub_record_view.getFieldText('casetaskevent');
						var service_item = o_sub_record_view.getFieldValue('item');
						var hrs_sub = o_sub_record_view.getFieldValue('hours');
						var day_Date = o_sub_record_view.getFieldValue('day');
						//Service Item
						JSON_TS_Lines = {
								
							Day:sub_day,
							Date: day_Date,
							Project:project,
							Task: task,
							ServiceItem: service_item,
							Hrs: hrs_sub,
							Leave_Temp:'',
							Holiday_Temp:''
							};
						dataRow_TS_Lines.push(JSON_TS_Lines);
						//Holiday
						JSON_Holiday_Lines = {
								
								Day:sub_day,	
								Date: day_Date,
								Project:project,
								Task: 'Holiday (Project Task)',
								ServiceItem: 2480,
								Hrs: '0:00'
								};
						dataRow_Holiday_Lines.push(JSON_Holiday_Lines);
						//Leave	
						JSON_Leave_Lines = {
									
									Day:sub_day,
									Date: day_Date,
									Project:project,
									Task: 'Leave (Project Task)',
									ServiceItem: 2479,
									Hrs: '0:00'
									};
						dataRow_Leave_Lines.push(JSON_Leave_Lines);
						//OT data
						if(OT_Applicable == 'YES'){
							JSON_OT_Lines = {
									
									Day:sub_day,
									Date: day_Date,
									Project:project,
									Task: 'OT (Project Task)',
									ServiceItem: 2425,
									Hrs: '0:00'
									};
							dataRow_OT_Lines.push(JSON_OT_Lines);
						}
					}
						
						if(task == 'Holiday (Project Task)' && sub_day == 'saturday'){
							JSON_ALL = {
									
									Holiday: dataRow_TS_Lines
									
										
								};
								dataRow_ALL.push(JSON_ALL);
						}
						if(task == 'Leave (Project Task)' && sub_day == 'saturday'){
							JSON_ALL = {
									
									Leave: dataRow_TS_Lines
									
										
								};
								dataRow_ALL.push(JSON_ALL);
						}
						if(task == 'OT  (Project Task)' && sub_day == 'saturday'){
							JSON_ALL = {
									
									OT: dataRow_TS_Lines
										
								};
								dataRow_ALL.push(JSON_ALL);
						}
						if(task == 'Operations (Project Task)' ||  task == 'Project Activities (Project Task)'){
							if(sub_day == 'saturday'){
							JSON_ALL = {
									
									ServiceItem: dataRow_TS_Lines
									
										
								};
								dataRow_ALL.push(JSON_ALL);
							}
						}
				}
					
					JSON_ALL = {
						ServiceItem: dataRow_TS_Lines,
						Holiday: dataRow_Holiday_Lines,
						Leave: dataRow_Leave_Lines,
						OT: dataRow_OT_Lines
							
					};
					dataRow_ALL.push(JSON_ALL);
					dataRow_OverAll_TS.push(dataRow_TS_Lines);
					dataRow_OverAll_TS.push(dataRow_Holiday_Lines);
					dataRow_OverAll_TS.push(dataRow_Leave_Lines);
					if(dataRow_OT_Lines.length > 0)
						dataRow_OverAll_TS.push(dataRow_OT_Lines);
			}
					//Creating array object for holiday,Leave,OT task
					var holiday_flag = false;
					var leave_flag = false;
					var ot_flag = false;
					var st_flag = false;
					var JSON_TS_Lines = {};
					var dataRow_TS_Lines =[];
					var JSON_Holiday_Lines = {};
					var dataRow_Holiday_Lines =[];
					var JSON_Leave_Lines = {};
					var dataRow_Leave_Lines =[];
					var JSON_OT_Lines = {};
					var dataRow_OT_Lines =[];
					
					for(var inx=0;inx<dataRow_ALL.length;inx++){
						var data = dataRow_ALL[inx];
						nlapiLogExecution('DEBUG','DataRow Value',dataRow_ALL[0]);
						if((data.Holiday) && data.Holiday[inx].Task == 'Holiday (Project Task)'){
							holiday_flag = true;
						}
						if((data.Leave) && data.Leave[inx].Task == 'Leave (Project Task)'){
							leave_flag = true;
						}
						if((data.OT) && data.OT[inx].Task == 'OT (Project Task)'){
							ot_flag = true;
						}
						if(data.ServiceItem){
						if(data.ServiceItem[inx].Task == 'Operations (Project Task)' || data.ServiceItem[0].Task == 'ServiceItem' || data.ServiceItem[0].Task == 'Project Activities'){
							st_flag = true;
						}
						}
					}
					
					//Creatign the dummy data for HOLIDAY
					if(dataRow_ALL.length > 0){
						if(holiday_flag == false){
							var data = dataRow_ALL[0];
							if(data.Holiday)
								var project = data.Holiday[0].Project;
							if(data.Leave)
								var project = data.Leave[0].Project;
							if(data.OT)
								var project = data.OT[0].Project;
							if(data.ServiceItem)
								var project = data.ServiceItem[0].Project;
							var date_d = weekStart;
							for(var n=0;n<7;n++){
								
								var sub_day = days[n];
									
									day_Date = nlapiStringToDate(weekStart);
									day_Date = nlapiAddDays(day_Date, 1);
									day_Date = nlapiDateToString(day_Date);
									//Service Item
									JSON_Holiday_Lines = {
											
											Day:sub_day,
											Date: date_d,
											Project:project,
											Task: 'Holiday (Project Task)',
											ServiceItem: 2480,
											Hrs: '0:00'
											};
									dataRow_Holiday_Lines.push(JSON_Holiday_Lines);
									//Holiday
									JSON_Holiday_Lines = {
											
											Day:sub_day,
											Date: day_Date,
											Project:project,
											Task: 'Holiday (Project Task)',
											ServiceItem: 2480,
											Hrs: '0:00'
											};
									dataRow_Holiday_Lines.push(JSON_Holiday_Lines);
									//Leave	
									JSON_Leave_Lines = {
												
												Day:sub_day,
												Date: day_Date,
												Project:project,
												Task: 'Leave (Project Task)',
												ServiceItem: 2479,
												Hrs: '0:00'
												};
									dataRow_Leave_Lines.push(JSON_Leave_Lines);
									//OT data
									if(OT_Applicable == 'YES'){
										JSON_OT_Lines = {
												
												Day:sub_day,
												Date: day_Date,
												Project:project,
												Task: 'OT (Project Task)',
												ServiceItem: 2425,
												Hrs: '0:00'
												};
										dataRow_OT_Lines.push(JSON_OT_Lines);
									}
									date_d = nlapiStringToDate(date_d);
									date_d = nlapiAddDays(date_d, 1);
									date_d = nlapiDateToString(date_d);
								}
								JSON_ALL = {
									
									Holiday: dataRow_Holiday_Lines
									
										
								};
								dataRow_ALL.push(JSON_ALL);
						}
					}//Closing of dummy data
					//Creatign the dummy data Leave
					if(dataRow_ALL.length > 0){
						if(leave_flag == false){
							var data = dataRow_ALL[0];
							if(data.Holiday)
								var project = data.Holiday[0].Project;
							if(data.Leave)
								var project = data.Leave[0].Project;
							if(data.OT)
								var project = data.OT[0].Project;
							if(data.ServiceItem)
								var project = data.ServiceItem[0].Project;
							var date_d = weekStart;
							for(var n=0;n<7;n++){
								
								var sub_day = days[n];
									
								JSON_Leave_Lines = {
										
										Day:sub_day,
										Date: day_Date,
										Project:project,
										Task: 'Leave (Project Task)',
										ServiceItem: 2479,
										Hrs: '0:00'
										};
								dataRow_Leave_Lines.push(JSON_Leave_Lines);
									day_Date = nlapiStringToDate(weekStart);
									day_Date = nlapiAddDays(day_Date, 1);
									day_Date = nlapiDateToString(day_Date);
									//Service Item
									JSON_Holiday_Lines = {
											
											Day:sub_day,
											Date: date_d,
											Project:project,
											Task: 'Holiday (Project Task)',
											ServiceItem: 2480,
											Hrs: '0:00'
											};
									dataRow_Holiday_Lines.push(JSON_Holiday_Lines);
									//Holiday
									JSON_Holiday_Lines = {
											
											Day:sub_day,
											Date: day_Date,
											Project:project,
											Task: 'Holiday (Project Task)',
											ServiceItem: 2480,
											Hrs: '0:00'
											};
									dataRow_Holiday_Lines.push(JSON_Holiday_Lines);
									//Leave	
									JSON_Leave_Lines = {
												
												Day:sub_day,
												Date: day_Date,
												Project:project,
												Task: 'Leave (Project Task)',
												ServiceItem: 2479,
												Hrs: '0:00'
												};
									dataRow_Leave_Lines.push(JSON_Leave_Lines);
									//OT data
									if(OT_Applicable == 'YES'){
										JSON_OT_Lines = {
												
												Day:sub_day,
												Date: day_Date,
												Project:project,
												Task: 'OT (Project Task)',
												ServiceItem: 2425,
												Hrs: '0:00'
												};
										dataRow_OT_Lines.push(JSON_OT_Lines);
									}
									date_d = nlapiStringToDate(date_d);
									date_d = nlapiAddDays(date_d, 1);
									date_d = nlapiDateToString(date_d);
								}
								JSON_ALL = {
									
									Leave: dataRow_Leave_Lines
									
										
								};
								dataRow_ALL.push(JSON_ALL);
						}
					}
					//Closing of dummy data
					//Creatign the dummy data Leave
					if(dataRow_ALL.length > 0){
						if(ot_flag == false && OT_Applicable == 'Yes'){
							var data = dataRow_ALL[0];
							if(data.Holiday)
								var project = data.Holiday[0].Project;
							if(data.Leave)
								var project = data.Leave[0].Project;
							if(data.OT)
								var project = data.OT[0].Project;
							if(data.ServiceItem)
								var project = data.ServiceItem[0].Project;
							var date_d = weekStart;
							for(var n=0;n<7;n++){
								
								var sub_day = days[n];
								//OT data
								if(OT_Applicable == 'YES'){
									JSON_OT_Lines = {
											
											Day:sub_day,
											Date: date_d,
											Project:project,
											Task: 'OT (Project Task)',
											ServiceItem: 2425,
											Hrs: '0:00'
											};
									dataRow_OT_Lines.push(JSON_OT_Lines);
								}
								JSON_Leave_Lines = {
										
										Day:sub_day,
										Date: day_Date,
										Project:project,
										Task: 'Leave (Project Task)',
										ServiceItem: 2479,
										Hrs: '0:00'
										};
								dataRow_Leave_Lines.push(JSON_Leave_Lines);
									day_Date = nlapiStringToDate(weekStart);
									day_Date = nlapiAddDays(day_Date, 1);
									day_Date = nlapiDateToString(day_Date);
									//Service Item
									JSON_Holiday_Lines = {
											
											Day:sub_day,
											Date: date_d,
											Project:project,
											Task: 'Holiday (Project Task)',
											ServiceItem: 2480,
											Hrs: '0:00'
											};
									dataRow_Holiday_Lines.push(JSON_Holiday_Lines);
									//Holiday
									JSON_Holiday_Lines = {
											
											Day:sub_day,
											Date: day_Date,
											Project:project,
											Task: 'Holiday (Project Task)',
											ServiceItem: 2480,
											Hrs: '0:00'
											};
									dataRow_Holiday_Lines.push(JSON_Holiday_Lines);
									//Leave	
									JSON_Leave_Lines = {
												
												Day:sub_day,
												Date: day_Date,
												Project:project,
												Task: 'Leave (Project Task)',
												ServiceItem: 2479,
												Hrs: '0:00'
												};
									dataRow_Leave_Lines.push(JSON_Leave_Lines);
									//OT data
									if(OT_Applicable == 'YES'){
										JSON_OT_Lines = {
												
												Day:sub_day,
												Date: day_Date,
												Project:project,
												Task: 'OT (Project Task)',
												ServiceItem: 2425,
												Hrs: '0:00'
												};
										dataRow_OT_Lines.push(JSON_OT_Lines);
									}
									date_d = nlapiStringToDate(date_d);
									date_d = nlapiAddDays(date_d, 1);
									date_d = nlapiDateToString(date_d);
								}
								JSON_ALL = {
									
									OT: dataRow_OT_Lines
									
										
								};
								dataRow_ALL.push(JSON_ALL);
						}
					}
					JSON_Total = {
							Body:dataRow_TS_Body,
							Lines:dataRow_ALL
					
					};
					dataRow_Total.push(JSON_Total);
					var JSON ={};
					var dataRow = [];
					
					JSON ={
							TimeSheetDetails: dataRow_Total,
							HolidayList:holiday_list
					
							};
					
					dataRow.push(JSON);
					}*/
		//}	
	//}
		/*var JSON_TS_Lines = {};
		var dataRow_TS_Lines =[];
		var JSON_Holiday_Lines = {};
		var dataRow_Holiday_Lines =[];
		var JSON_Leave_Lines = {};
		var dataRow_Leave_Lines =[];
		var JSON_OT_Lines = {};
		var dataRow_OT_Lines =[];
		
		var dataRow_OverAll_TS = [];
		var JSON_ALL = {};
		var dataRow_ALL = [];
		var lineCount = load_TS.getLineItemCount('timegrid');
		for(var inx=1;inx<=lineCount;inx++){
			var JSON_TS_Lines = {};
			var dataRow_TS_Lines =[];
			var JSON_Holiday_Lines = {};
			var dataRow_Holiday_Lines =[];
			var JSON_Leave_Lines = {};
			var dataRow_Leave_Lines =[];
			var JSON_OT_Lines = {};
			var dataRow_OT_Lines =[];
			
			load_TS.selectLineItem('timegrid',inx);	
		//var project = load_TS.getLineItemText('timegrid','customer',inx);
		//var task = load_TS.getLineItemValue('timegrid','casetaskevent',inx);
		//var service_item = load_TS.getLineItemValue('timegrid','item',inx);
		
		
		for(var n=0;n<7;n++){
			
			var sub_day = days[n];
			if(sub_day == 'saturday'){
				day_Date = nlapiStringToDate(day_Date);
				day_Date = nlapiAddDays(day_Date, 1);
				day_Date = nlapiDateToString(day_Date);
				//Service Item
				JSON_TS_Lines = {
						
					Day:sub_day,
					Date: day_Date,
					Project:project,
					Task: task,
					ServiceItem: service_item,
					Hrs: '0:00',
					Leave_Temp:'',
					Holiday_Temp:''
					};
				dataRow_TS_Lines.push(JSON_TS_Lines);
				//Holiday
				JSON_Holiday_Lines = {
						
						Day:sub_day,
						Date: day_Date,
						Project:project,
						Task: 'Holiday (Project Task)',
						ServiceItem: 2480,
						Hrs: '0:00'
						};
				dataRow_Holiday_Lines.push(JSON_Holiday_Lines);
				//Leave	
				JSON_Leave_Lines = {
							
							Day:sub_day,
							Date: day_Date,
							Project:project,
							Task: 'Leave (Project Task)',
							ServiceItem: 2479,
							Hrs: '0:00'
							};
				dataRow_Leave_Lines.push(JSON_Leave_Lines);
				//OT data
				if(OT_Applicable == 'YES'){
					JSON_OT_Lines = {
							
							Day:sub_day,
							Date: day_Date,
							Project:project,
							Task: 'OT (Project Task)',
							ServiceItem: 2425,
							Hrs: '0:00'
							};
					dataRow_OT_Lines.push(JSON_OT_Lines);
				}
			}
		
			
			
			if(sub_day == 'sunday'){
				o_sub_record_view = load_TS.viewCurrentLineItemSubrecord('timegrid', days[1]);
				if(!o_sub_record_view)
					o_sub_record_view = load_TS.viewCurrentLineItemSubrecord('timegrid', days[2]);
				if(!o_sub_record_view)
					o_sub_record_view = load_TS.viewCurrentLineItemSubrecord('timegrid', days[3]);
				if(!o_sub_record_view)
					o_sub_record_view = load_TS.viewCurrentLineItemSubrecord('timegrid', days[4]);
				if(o_sub_record_view){
					var project = o_sub_record_view.getFieldText('customer');
					var task = o_sub_record_view.getFieldText('casetaskevent');
					var service_item = o_sub_record_view.getFieldValue('item');
					var hrs_sub = o_sub_record_view.getFieldValue('hours');
					var day_Date = o_sub_record_view.getFieldValue('day');
					day_Date = nlapiStringToDate(day_Date);
					day_Date = nlapiAddDays(day_Date, -1);
					day_Date = nlapiDateToString(day_Date);
					//Service Item
					JSON_TS_Lines = {
							
						Day:sub_day,
						Date: day_Date,
						Project:project,
						Task: task,
						ServiceItem: service_item,
						Hrs: '0:00',
						Leave_Temp:'',
						Holiday_Temp:''
						};
					dataRow_TS_Lines.push(JSON_TS_Lines);
					//Holiday
					JSON_Holiday_Lines = {
							
							Day:sub_day,
							Date: day_Date,
							Project:project,
							Task: 'Holiday (Project Task)',
							ServiceItem: 2480,
							Hrs: '0:00'
							};
					dataRow_Holiday_Lines.push(JSON_Holiday_Lines);
					//Leave	
					JSON_Leave_Lines = {
								
								Day:sub_day,
								Date: day_Date,
								Project:project,
								Task: 'Leave (Project Task)',
								ServiceItem: 2479,
								Hrs: '0:00'
								};
					dataRow_Leave_Lines.push(JSON_Leave_Lines);
					//OT data
					if(OT_Applicable == 'YES'){
						JSON_OT_Lines = {
								
								Day:sub_day,
								Date: day_Date,
								Project:project,
								Task: 'OT (Project Task)',
								ServiceItem: 2425,
								Hrs: '0:00'
								};
						dataRow_OT_Lines.push(JSON_OT_Lines);
					}
				}
			}
			var o_sub_record_view = load_TS.viewCurrentLineItemSubrecord('timegrid', sub_day);
			if(o_sub_record_view){
			var project = o_sub_record_view.getFieldText('customer');
			var task = o_sub_record_view.getFieldText('casetaskevent');
			var service_item = o_sub_record_view.getFieldValue('item');
			var hrs_sub = o_sub_record_view.getFieldValue('hours');
			var day_Date = o_sub_record_view.getFieldValue('day');
			//Service Item
			JSON_TS_Lines = {
					
				Day:sub_day,
				Date: day_Date,
				Project:project,
				Task: task,
				ServiceItem: service_item,
				Hrs: hrs_sub,
				Leave_Temp:'',
				Holiday_Temp:''
				};
			dataRow_TS_Lines.push(JSON_TS_Lines);
			//Holiday
			JSON_Holiday_Lines = {
					
					Day:sub_day,	
					Date: day_Date,
					Project:project,
					Task: 'Holiday (Project Task)',
					ServiceItem: 2480,
					Hrs: '0:00'
					};
			dataRow_Holiday_Lines.push(JSON_Holiday_Lines);
			//Leave	
			JSON_Leave_Lines = {
						
						Day:sub_day,
						Date: day_Date,
						Project:project,
						Task: 'Leave (Project Task)',
						ServiceItem: 2479,
						Hrs: '0:00'
						};
			dataRow_Leave_Lines.push(JSON_Leave_Lines);
			//OT data
			if(OT_Applicable == 'YES'){
				JSON_OT_Lines = {
						
						Day:sub_day,
						Date: day_Date,
						Project:project,
						Task: 'OT (Project Task)',
						ServiceItem: 2425,
						Hrs: '0:00'
						};
				dataRow_OT_Lines.push(JSON_OT_Lines);
			}
		}
			
		
	}
		JSON_ALL = {
				ServiceItem: dataRow_TS_Lines,
				Holiday: dataRow_Holiday_Lines,
				Leave: dataRow_Leave_Lines,
				OT: dataRow_OT_Lines
					
			};
			dataRow_ALL.push(JSON_ALL);
		dataRow_OverAll_TS.push(dataRow_TS_Lines);
		dataRow_OverAll_TS.push(dataRow_Holiday_Lines);
		dataRow_OverAll_TS.push(dataRow_Leave_Lines);
		if(dataRow_OT_Lines.length > 0)
			dataRow_OverAll_TS.push(dataRow_OT_Lines);
}*/
		
		var JSON_Total = {};
		var dataRow_Total = [];

		JSON_Total = {
				Body:dataRow_TS_Body,
				Lines:dataRow_ALL,
				//OT: dataRow_OT_Lines_,
				Items: dataRow_TS_Items	
		
		};
		dataRow_Total.push(JSON_Total);
		var JSON ={};
		var dataRow = [];
		//Customer Holiday List
		var customer_list = [];
		var holiday_list = [];
		//GET OT Flag From Allocation startDate, endDate,employeeId
				
		allocationDetails_items.forEach(function(project) {
			var project_internal_id = project.Project;
			var project_holiday = nlapiLookupField('job', project_internal_id,
	        'custentityproject_holiday');
			if(parseInt(project_holiday) != parseInt(1)) { //Project Marked as Customer Holiday
			customer_list = get_holidays(startDate,
					endDate, project.ProjectID,employeeId,project.Project);
			
			}
			else{
				//Get Holiday List in Week
				holiday_list = get_company_holidays(startDate,endDate,employeeId,project.Project);
			}
			});
	
		
	
	JSON ={
			TimeSheetDetails: dataRow_Total,
			HolidayList:holiday_list,
			CustomerHoliday: customer_list
			};
		
		
		dataRow.push(JSON);
		}
		return dataRow;
}
catch(e){
	nlapiLogExecution('error', 'allocation details', e);
	throw e;
}

}
//
//create a new timesheet record using the details retrieved
function autoGenerateTimesheet(weekStartDate, employee, allocationDetails) {

	try {
		// create a new timesheet record
		var newTimesheet = nlapiCreateRecord('timesheet', {
			recordmode : 'dynamic'
		});

		var daysOfWeek = [ 'sunday', 'monday', 'tuesday', 'wednesday',
				'thursday', 'friday', 'saturday' ];

		// var employeeDetails = nlapiLookupField('employee', employee, [
		// 'subsidiary', 'location'
		// ]);

		// set the main fields
		newTimesheet.setFieldValue('approvalstatus', '1');
		// newTimesheet.setFieldValue('customform', '');
		newTimesheet.setFieldValue('employee', employee);
		newTimesheet.setFieldValue('startdate', weekStartDate);
		// newTimesheet.setFieldValue('enddate', '');
		newTimesheet.setFieldValue('iscomplete', 'F');
		// newTimesheet.setFieldValue('subsidiary', employeeDetails.subsidiary);
		// newTimesheet.setFieldValue('totalhours', 40);

		// nlapiLogExecution('debug', 'allocationDetails',
		// JSON.stringify(allocationDetails));

		// fill in the timesheet hours
		allocationDetails.forEach(function(project) {

			newTimesheet.selectNewLineItem('timegrid');

			for (var i = 1; i < 6; i++) {

				if (isNumber(project.Hours[i])) {
					// nlapiLogExecution('debug', 'project.Hours[i]',
					// project.Hours[i]);
					var newEntry = newTimesheet.createCurrentLineItemSubrecord(
							'timegrid', daysOfWeek[i]);
					// newEntry.setFieldValue('subsidiary',
					// employeeDetails.subsidiary);
					newEntry.setFieldValue('employee', employee);
					newEntry.setFieldValue('customer', project.Project);
					// newEntry.setFieldValue('location',
					// employeeDetails.location);
					// newEntry.setFieldText('projecttaskassignment',
					// project.Event);
					newEntry.setFieldValue('casetaskevent', project.Task);
					// newEntry.setFieldValue('isbillable', 'T');
					newEntry.setFieldValue('hours', project.Hours[i]);
					newEntry.commit();
				}
				newTimesheet.commitLineItem('timegrid');
			}
		});

		var timesheetId = nlapiSubmitRecord(newTimesheet);
		return timesheetId;
	} catch (err) {
		nlapiLogExecution('ERROR', 'autoGenerateTimesheet', err);
		throw err;
	}
}

function isNumber(n) {

	return typeof n == 'number' && !isNaN(n) && isFinite(n);
}
//Get Active Real time alloaction data
function getActiveAllocations(weekStartDate, weekEndDate, employee) {

	try {
		// get active allocations
		var allocationSearch = nlapiSearchRecord('resourceallocation', null, [
				new nlobjSearchFilter('startdate', null, 'notafter',
						weekEndDate),
				new nlobjSearchFilter('enddate', null, 'notbefore',
						weekStartDate),
				new nlobjSearchFilter('resource', null, 'anyof', employee) ], [
				new nlobjSearchColumn('project'),
				new nlobjSearchColumn('customer', 'job'),
				new nlobjSearchColumn('subsidiary', 'employee'),
				new nlobjSearchColumn('startdate'),
				new nlobjSearchColumn('jobtype', 'job'),
				new nlobjSearchColumn('jobbillingtype', 'job'),
				new nlobjSearchColumn('formulanumeric')
						.setFormula('{percentoftime}'),
				new nlobjSearchColumn('enddate'),
				new nlobjSearchColumn('percentoftime'),
				new nlobjSearchColumn('custevent1'),
				new nlobjSearchColumn('custevent_otserviceitem')]);
		//Multiple Allocation
		/*if(allocationSearch){
		if(allocationSearch.length > 1) {
			if(parseFloat(allocationSearch[0].getValue('formulanumeric')) < parseFloat(1)){
			var percent_ = allocationSearch[0].getValue('formulanumeric');
			nlapiLogExecution('DEBUG','Mutiple Allocation Percent',percent_);
			nlapiLogExecution('DEBUG','Mutiple Allocation length',allocationSearch.length);
			throw 'You cannot submit the timesheet as you have multiple project allocations';
			}
		}
		}*/
		if(allocationSearch) {
			var weeks = [];
			var projects = [];
			var projectList = [];
			var activityNames = [];

			for (var j = 0; j < 7; j++) {
				weeks[j] = [];
				var currentDate = nlapiAddDays(
						nlapiStringToDate(weekStartDate), j);

				for (var i = 0; i < allocationSearch.length; i++) {
					var OTApplicable = '';
					var allocationStart = nlapiStringToDate(allocationSearch[i]
							.getValue('startdate'));
					var allocationEnd = nlapiStringToDate(allocationSearch[i]
							.getValue('enddate'));
					var project = allocationSearch[i].getValue('project');
					var i_customer = allocationSearch[i].getValue('customer', 'job');
					var i_emp_subsidiary = allocationSearch[i].getValue('subsidiary', 'employee');
					if(parseInt(i_customer) == parseInt(8381) && parseInt(i_emp_subsidiary) == parseInt(3)){
						throw 'You cannot submit the timesheet as you are allocated to MOVE-BTPL';
						return;
					}
					var otserviceitem = allocationSearch[i].getText('custevent_otserviceitem');
					if(otserviceitem == 'OT')
						OTApplicable = 'YES';
					else
						OTApplicable = 'NO';
					var projectName = allocationSearch[i].getText('project');
					var event = allocationSearch[i].getValue('custevent1');
					var projectType = allocationSearch[i].getValue('jobtype',
							'job');
					var billingType = allocationSearch[i].getValue(
							'jobbillingtype', 'job');
					var serviceItem = null;
					var percent = allocationSearch[i]
							.getValue('formulanumeric');
					var proj_lookUp = nlapiLookupField('job',parseInt(project),
						['custentity_hoursperday','custentity_hoursperweek','custentity_onsite_hours_per_day','custentity_onsite_hours_per_week'])
					if(i_emp_subsidiary == 3)
					{
						var hours_day = proj_lookUp.custentity_hoursperday;
						if(_logValidation(hours_day))
						{
							var hours = percent * hours_day;
						}
						else
						{
							var hours = percent * 8;
						}
					}
					else
					{
						var hours_day = proj_lookUp.custentity_onsite_hours_per_day;
						if(_logValidation(hours_day))
						{
							var hours = percent * hours_day;
						}
						else
						{
							var hours = percent * 8;
						}
					}
					hours = parseFloat(hours.toFixed(2));
					// nlapiLogExecution('debug', 'Hours', hours);

					activityNames = [ "Project Activites", "Project activites",
							"Project activities", "project activities",
							"Bench Task","Bench" ,"Project Activities-Offsite",
							"Project Activities-Onsite", "Operations",
							"Project Activities", "Project activity",
							"project activity", "Project Activity","Internal Project","Bench Activities"];

					// check project type to get the project activites name
					// will be used later to assign task
					// internal project
					/*
					 * if (projectType == '1') {
					 * 
					 * 
					 * serviceItem = '2426'; // Internal Projects } else { //
					 * external project
					 * 
					 * if (billingType == 'TM') { // T&M activityNames = [
					 * "Operations", "Project Activities", "Project activity" ];
					 * serviceItem = '2222'; // ST } else { // FBI & FBM
					 * activityNames = [ "Operations", "Project Activities",
					 * "Project activity" ]; serviceItem = '2221';// FP } }
					 */

					// create an array for days and hours to fill
					if (currentDate >= allocationStart
							&& currentDate <= allocationEnd) {
						weeks[j].push(project);

						var index = _.indexOf(projectList, project);

						if (index == -1) {
							projectList.push(project);
							projects.push({
								Project : project,
								Task : '',
								Event : serviceItem,
								OT: OTApplicable,
								// Days : [

								// ],
								Hours : [

								]
							});
							index = projects.length - 1;
						}

						// var date1 = currentDate);
						var day = currentDate.getDay();
						// projects[index].Days[day] = currentDate;
						projects[index].Hours[day] = hours;
					}
				}
			}

			// nlapiLogExecution('debug', 'weeks A', JSON.stringify(weeks));
			// nlapiLogExecution('debug', 'projects A',
			// JSON.stringify(projects));
			// nlapiLogExecution('debug', 'checkpoint', 1);
			// nlapiLogExecution('debug', 'activityNames',
			// JSON.stringify(activityNames));
			// nlapiLogExecution('debug', 'projects B',
			// JSON.stringify(projects));
			// nlapiLogExecution('debug', 'checkpoint', 2);
			// nlapiLogExecution('debug', 'activityNames',
			// JSON.stringify(activityNames));

			// creating a filter for project tasks
			var filter = [];
			var addOr = false;
			activityNames.forEach(function(name) {

				if (addOr) {
					filter.push('or');
				}

				filter.push([ 'title', 'is', name ]);

				addOr = true;
			});

			// nlapiLogExecution('debug', 'filter', JSON.stringify(filter));

			// searching the project task records for project activities
			var taskSearch = nlapiSearchRecord('projecttask', null, [ filter,
					'and', [ 'company', 'anyof', projectList ] ],
					[ new nlobjSearchColumn('company') ]);

			if (!taskSearch) {
				throw "Project Task not found";
			}

			taskSearch.forEach(function(task) {

				for (var i = 0; i < projects.length; i++) {

					if (projects[i].Project == task.getValue('company')) {
						projects[i].Task = task.getId();
					}
				}
			});
			

			 nlapiLogExecution('debug', 'projects', JSON.stringify(projects));
		} else {
			throw "No Active Allocation for this week. Kindly contact your manager/business.ops@BRILLIO.COM"; 
		}

		return projects;
	} catch (err) {
		nlapiLogExecution('ERROR', 'getActiveAllocations', err);
		throw err;
	}
}

//For Items Array
//Get Active Real time alloaction data
function getActiveAllocations_Items(weekStartDate, weekEndDate, employee) {

	try {
		// get active allocations
		var allocationSearch = nlapiSearchRecord('resourceallocation', null, [
				new nlobjSearchFilter('startdate', null, 'notafter',
						weekEndDate),
				new nlobjSearchFilter('enddate', null, 'notbefore',
						weekStartDate),
				new nlobjSearchFilter('resource', null, 'anyof', employee) ], [
				new nlobjSearchColumn('project'),
				new nlobjSearchColumn('startdate'),
				new nlobjSearchColumn('jobtype', 'job'),
				new nlobjSearchColumn('jobbillingtype', 'job'),
				new nlobjSearchColumn('formulanumeric')
						.setFormula('{percentoftime}'),
				new nlobjSearchColumn('enddate'),
				new nlobjSearchColumn('percentoftime'),
				new nlobjSearchColumn('custevent1'),
				new nlobjSearchColumn('custevent_otserviceitem')]);
		//Multiple Allocation
		/*if(allocationSearch){
		if(allocationSearch.length > 1 && (allocationSearch[0].getValue('formulanumeric') < 100)){ 
			nlapiLogExecution('DEBUG','Mutiple Allocation');
			throw 'Mutiple Allocation';
			
		}
		}*/
		if(allocationSearch) {
			var weeks = [];
			var projects = [];
			var projectList = [];
			var activityNames = [];
			var item_id = '';
			var proj_concat = '';
			for (var j = 0; j < 7; j++) {
				weeks[j] = [];
				var currentDate = nlapiAddDays(
						nlapiStringToDate(weekStartDate), j);

				for (var i = 0; i < allocationSearch.length; i++) {
					var OTApplicable = '';
					var event = '';
					var allocationStart = nlapiStringToDate(allocationSearch[i]
							.getValue('startdate'));
					var allocationEnd = nlapiStringToDate(allocationSearch[i]
							.getValue('enddate'));
					var project = allocationSearch[i].getValue('project');
					var otserviceitem = allocationSearch[i].getText('custevent_otserviceitem');
					if(otserviceitem == 'OT')
						OTApplicable = 'YES';
					else
						OTApplicable = 'NO';
					var projectName = allocationSearch[i].getText('project');
					event = allocationSearch[i].getValue('custevent1');
					
					//Find the internalID of service Item
					if(event){
						var serviceItemSearch = nlapiSearchRecord('item', null, [
						                                                      	new nlobjSearchFilter('itemid', null, 'is',
						                                                      						event),
						                                                      	new nlobjSearchFilter('isinactive', null, 'is',
								                                                      						'F')
						                                                      	], [new nlobjSearchColumn('internalid')]);
					
						if(serviceItemSearch)
							item_id = serviceItemSearch[0].getValue('internalid');
					}
					//Find the Project name
					if(project){
						var projectSearch = nlapiSearchRecord('job', null, [
						                                                      	new nlobjSearchFilter('internalid', null, 'is',
						                                                      			project),
						                                                      	], [new nlobjSearchColumn('entityid'),
						                                                      	    new nlobjSearchColumn('companyname'),
						                                                      	    new nlobjSearchColumn('companyname','customer')]);
					
						if(projectSearch)
							proj_concat = projectSearch[0].getValue('entityid')+' '+projectSearch[0].getValue('companyname');
					}
					
					//var serviceItem = allocationSearch[i].getValue('custevent1');
					var projectType = allocationSearch[i].getValue('jobtype',
							'job');
					var billingType = allocationSearch[i].getValue(
							'jobbillingtype', 'job');
					var serviceItem = null;
					var percent = allocationSearch[i]
							.getValue('formulanumeric');
					var hours = percent * 8;
					hours = parseFloat(hours.toFixed(2));
					// nlapiLogExecution('debug', 'Hours', hours);

					activityNames = [ "Project Activites", "Project activites",
										"Project activities", "project activities",
										"Bench Task","Bench" ,"Project Activities-Offsite",
										"Project Activities-Onsite", "Operations",
										"Project Activities", "Project activity",
										"project activity", "Project Activity","Internal Project","Bench Activities"];

					// check project type to get the project activites name
					// will be used later to assign task
					// internal project
					/*
					 * if (projectType == '1') {
					 * 
					 * 
					 * serviceItem = '2426'; // Internal Projects } else { //
					 * external project
					 * 
					 * if (billingType == 'TM') { // T&M activityNames = [
					 * "Operations", "Project Activities", "Project activity" ];
					 * serviceItem = '2222'; // ST } else { // FBI & FBM
					 * activityNames = [ "Operations", "Project Activities",
					 * "Project activity" ]; serviceItem = '2221';// FP } }
					 */

					// create an array for days and hours to fill
					if (currentDate >= allocationStart
							&& currentDate <= allocationEnd) {
						weeks[j].push(project);

						var index = _.indexOf(projectList, project);

						if (index == -1) {
							projectList.push(project);
							projects.push({
								Project : proj_concat,
								ProjectID : project,
								Task : '',
								//Task_Name : '',
								Event : item_id,
								OT: OTApplicable,
								Percent: percent,
								// Days : [

								// ],
								Hours : [

								]
							});
							index = projects.length - 1;
						}

						// var date1 = currentDate);
						var day = currentDate.getDay();
						// projects[index].Days[day] = currentDate;
						projects[index].Hours[day] = hours;
					}
				}
			}

			// nlapiLogExecution('debug', 'weeks A', JSON.stringify(weeks));
			// nlapiLogExecution('debug', 'projects A',
			// JSON.stringify(projects));
			// nlapiLogExecution('debug', 'checkpoint', 1);
			// nlapiLogExecution('debug', 'activityNames',
			// JSON.stringify(activityNames));
			// nlapiLogExecution('debug', 'projects B',
			// JSON.stringify(projects));
			// nlapiLogExecution('debug', 'checkpoint', 2);
			// nlapiLogExecution('debug', 'activityNames',
			// JSON.stringify(activityNames));

			// creating a filter for project tasks
			var filter = [];
			var addOr = false;
			activityNames.forEach(function(name) {

				if (addOr) {
					filter.push('or');
				}

				filter.push([ 'title', 'is', name ]);

				addOr = true;
			});

			// nlapiLogExecution('debug', 'filter', JSON.stringify(filter));

			// searching the project task records for project activities
			var taskSearch = nlapiSearchRecord('projecttask', null, [ filter,
					'and', [ 'company', 'anyof', projectList ] ],
					[ new nlobjSearchColumn('company'),
					  new nlobjSearchColumn('title')]);
			nlapiLogExecution('DEBUG','Task taskSearch',taskSearch.length);
			if (!taskSearch) {
				throw "Project Task not found";
			}

			taskSearch.forEach(function(task) {

				for (var i = 0; i < projects.length; i++) {

					if (projects[i].ProjectID == task.getValue('company')) 
					{
						var taskName = task.getValue('title');
						nlapiLogExecution('DEBUG','Task Name',taskName);
						//projects[i].Task = task.getId();
						projects[i].Task = taskName +' '+'(Project Task)';
					}
				}
			});
			

			// nlapiLogExecution('debug', 'projects', JSON.stringify(projects));
		} else {
			throw "No Active Allocation for this week. Kindly contact your manager/business.ops@BRILLIO.COM"; 
		}

		return projects;
	} catch (err) {
		nlapiLogExecution('ERROR', 'getActiveAllocations_Items', err);
		throw err;
	}
}

function get_company_holidays(start_date, end_date, emp,Project_ID) {
	var holiday_list = [];

	//start_date = nlapiDateToString(start_date, 'date');
	//end_date = nlapiDateToString(end_date, 'date');

	 nlapiLogExecution('debug', 'start_date', start_date);
	 nlapiLogExecution('debug', 'end_date', end_date);
	 nlapiLogExecution('debug', 'emp', emp);
	var empLook = nlapiLookupField('employee',parseInt(emp),['subsidiary']);
	var subsidiary = empLook.subsidiary;
	 nlapiLogExecution('debug', 'subsidiary', subsidiary);
	var search_company_holiday = nlapiSearchRecord('customrecord_holiday',
	        'customsearch_company_holiday_search', [
	                new nlobjSearchFilter('custrecord_date', null, 'within',
	                        start_date, end_date),
	                new nlobjSearchFilter('custrecordsubsidiary', null,
	                        'anyof', subsidiary) ], [ new nlobjSearchColumn(
	                'custrecord_date') ]);

	if (search_company_holiday) {

		for (var i = 0; i < search_company_holiday.length; i++) {
			holiday_list.push({
				'ProjectID':	Project_ID,
				'Date': search_company_holiday[i].getValue('custrecord_date')
			});
		}
		
	}

	return holiday_list;
}

function getNextWeekData(dataIn,employeeId,startDate, endDate,autoFill){
	try{
		var dataRow = [];
		var d_startDate = nlapiStringToDate(startDate);
		//Data For next Week data
		var allocationDetails = getActiveAllocations(startDate, endDate,
				employeeId);
			//Get Holiday List in Week
		var holiday_list = get_company_holidays(startDate,endDate,employeeId);
		// get active allocations, hours ,etc.
	/*	if(autoFill == 'T'){
		// create an open timesheet
		var timesheetId = autoGenerateTimesheet(startDate, employeeId,
				allocationDetails);
		if(timesheetId){
			//Load to Get TS Data
				var load_TS = nlapiLoadRecord('timesheet',timesheetId);
				var emp = load_TS.getFieldText('employee');
				var emp_TS_approver = load_TS.getFieldText('custrecord_ts_time_approver');
				var weekStart = load_TS.getFieldValue('startdate');
				var totalHrs = load_TS.getFieldValue('totalhours');
				
				//GET OT Flag From Allocation
				allocationDetails.forEach(function(project) {
					OT_Applicable = project.OT;
				});
				
				var JSON_TS_Body = {};
				var dataRow_TS_Body =[];
				
				JSON_TS_Body = {
				Employee:emp,
				TSApprover: emp_TS_approver,
				WeekStartDate: weekStart,
				TotalHours: totalHrs
				};
				dataRow_TS_Body.push(JSON_TS_Body);
				
				var JSON_TS_Lines = {};
				var dataRow_TS_Lines =[];
				var JSON_Holiday_Lines = {};
				var dataRow_Holiday_Lines =[];
				var JSON_Leave_Lines = {};
				var dataRow_Leave_Lines =[];
				var JSON_OT_Lines = {};
				var dataRow_OT_Lines =[];
				
				var dataRow_OverAll_TS = [];
				
				var lineCount = load_TS.getLineItemCount('timegrid');
				for(var inx=1;inx<=lineCount;inx++){
					
					load_TS.selectLineItem('timegrid',inx);	
				//var project = load_TS.getLineItemText('timegrid','customer',inx);
				//var task = load_TS.getLineItemValue('timegrid','casetaskevent',inx);
				//var service_item = load_TS.getLineItemValue('timegrid','item',inx);
				
				
				for(var n=0;n<7;n++){
					
					var sub_day = days[n];
					var o_sub_record_view = load_TS.viewCurrentLineItemSubrecord('timegrid', sub_day);
					
					if(o_sub_record_view){
					var project = o_sub_record_view.getFieldText('customer');
					var task = o_sub_record_view.getFieldText('casetaskevent');
					var service_item = o_sub_record_view.getFieldValue('item');
					var hrs_sub = o_sub_record_view.getFieldValue('hours');
					//Service Item
					JSON_TS_Lines = {
							
						Day:sub_day,	
						Project:project,
						Task: task,
						ServiceItem: service_item,
						Hrs: hrs_sub
						};
					dataRow_TS_Lines.push(JSON_TS_Lines);
					//Holiday
					JSON_Holiday_Lines = {
							
							Day:sub_day,	
							Project:project,
							Task: 'Holiday (Project Task)',
							ServiceItem: 2480,
							Hrs: 0
							};
					dataRow_Holiday_Lines.push(JSON_Holiday_Lines);
					//Leave	
					JSON_Leave_Lines = {
								
								Day:sub_day,	
								Project:project,
								Task: 'Leave (Project Task)',
								ServiceItem: 2479,
								Hrs: 0
								};
					dataRow_Leave_Lines.push(JSON_Leave_Lines);
					//OT data
					if(OT_Applicable == 'YES'){
						JSON_OT_Lines = {
								
								Day:sub_day,	
								Project:project,
								Task: 'OT (Project Task)',
								ServiceItem: 2425,
								Hrs: 0
								};
						dataRow_OT_Lines.push(JSON_OT_Lines);
					}
				}
					
				
			}
				dataRow_OverAll_TS.push(dataRow_TS_Lines);
				dataRow_OverAll_TS.push(dataRow_Holiday_Lines);
				dataRow_OverAll_TS.push(dataRow_Leave_Lines);
				if(dataRow_OT_Lines.length > 0)
					dataRow_OverAll_TS.push(dataRow_OT_Lines);
		}
				var JSON_Total = {};
				var dataRow_Total = [];

				JSON_Total = {
						Body:dataRow_TS_Body,
						Lines:dataRow_OverAll_TS
				
				};
				dataRow_Total.push(JSON_Total);
				var JSON ={};
				var dataRow = [];
				
				JSON ={
						TimeSheetDetails: dataRow_Total,
						HolidayList: holiday_list
				
						};
				
				dataRow.push(JSON);
				}
		}*/
		//If autofill is not true
		//else{
			var daysOfWeek = [ 'sunday', 'monday', 'tuesday', 'wednesday',
			   				'thursday', 'friday', 'saturday' ];
			var dataRow_N =[];
			var dataRow_TN =[];
			var JSON_N = {};
			var JSON_TN ={};
			var JSON_TS_Lines_N = {};
			var dataRow_TS_Lines_N =[];
			var JSON_Holiday_Lines_N = {};
			var dataRow_Holiday_Lines_N =[];
			var JSON_Leave_Lines_N = {};
			var dataRow_Leave_Lines_N =[];
			var JSON_OT_Lines_N = {};
			var dataRow_OT_Lines_N =[];
			
			var dataRow_OverAll_TS_N = [];
			
			allocationDetails.forEach(function(project) {
				
				var proj_id = project.Project;
				var proj_look = nlapiLookupField('job',proj_id,['entityid','altname']);
				var id = proj_look.entityid;
				var name = proj_look.altname;
				var customer = id + ' ' + name;
				
				var proj_Task = project.Task;
				var proj_lookTask = nlapiLookupField('projecttask',proj_Task,['title']);
				var id_Task = proj_lookTask.title;
				
				var name_Task = id_Task + ' (Project Task)';
				//newTimesheet.selectNewLineItem('timegrid');

				for (var i = 0; i < 7; i++) {

					//if (isNumber(project.Hours[i])) {
						// nlapiLogExecution('debug', 'project.Hours[i]',
						// project.Hours[i]);
						JSON_TS_Lines_N = {
						Day : nlapiDateToString(d_startDate),
						// newEntry.setFieldValue('subsidiary',
						// employeeDetails.subsidiary);
						//Employee: employeeId,
						Project: customer,
						ServiceItem: '',
						// newEntry.setFieldValue('location',
						// employeeDetails.location);
						// newEntry.setFieldText('projecttaskassignment',
						// project.Event);
						Task: name_Task,
						Hours: '0:00',
						//OT: project.OT
						// newEntry.setFieldValue('isbillable', 'T');
						
					
					};
						dataRow_TS_Lines_N.push(JSON_TS_Lines_N);
						
						JSON_Holiday_Lines_N = {
								Day : nlapiDateToString(d_startDate),
								// newEntry.setFieldValue('subsidiary',
								// employeeDetails.subsidiary);
								//Employee: employeeId,
								Project: customer,
								ServiceItem: 2480,
								// newEntry.setFieldValue('location',
								// employeeDetails.location);
								// newEntry.setFieldText('projecttaskassignment',
								// project.Event);
								Task: 'Holiday (Project Task)',
								Hours: '0:00',
								//OT: project.OT
								// newEntry.setFieldValue('isbillable', 'T');
								
							
							};
						dataRow_Holiday_Lines_N.push(JSON_Holiday_Lines_N);	
						
						JSON_Leave_Lines_N = {
								Day : nlapiDateToString(d_startDate),
								// newEntry.setFieldValue('subsidiary',
								// employeeDetails.subsidiary);
								//Employee: employeeId,
								Project: customer,
								ServiceItem: 2479,
								// newEntry.setFieldValue('location',
								// employeeDetails.location);
								// newEntry.setFieldText('projecttaskassignment',
								// project.Event);
								Task: 'Leave (Project Task)',
								Hours: '0:00',
								//OT: project.OT
								// newEntry.setFieldValue('isbillable', 'T');
								
							
							};
						dataRow_Leave_Lines_N.push(JSON_Leave_Lines_N);	
						if(project.OT == 'YES'){
							JSON_OT_Lines_N = {
									Day : nlapiDateToString(d_startDate),
									// newEntry.setFieldValue('subsidiary',
									// employeeDetails.subsidiary);
									//Employee: employeeId,
									Project: customer,
									ServiceItem: 2425,
									// newEntry.setFieldValue('location',
									// employeeDetails.location);
									// newEntry.setFieldText('projecttaskassignment',
									// project.Event);
									Task: 'OT (Project Task)',
									Hours: '0:00',
									//OT: project.OT
									// newEntry.setFieldValue('isbillable', 'T');
									
								
								};
							dataRow_OT_Lines_N.push(JSON_OT_Lines_N);	
						}
						d_startDate = nlapiAddDays(d_startDate,1);
				//}
				}
			});
			JSON_N = {
					ServiceItem: dataRow_TS_Lines_N,
					Holiday: dataRow_Holiday_Lines_N,
					Leave: dataRow_Leave_Lines_N,
					OT: dataRow_OT_Lines_N
			};
			dataRow_TN.push(JSON_N);
			JSON_TN ={
					TimeSheetDetails: dataRow_TN,
					HolidayList: holiday_list
			
					};
			
			dataRow_N.push(JSON_TN);
			//}
			
	//	}
		return dataRow_N;
	}
	catch (err) {
		nlapiLogExecution('ERROR', 'next week TS', err);
		throw err;
}
}

//validate blank entries
function _logValidation(value) 
{
if(value != null && value.toString() != null && value != '' && value != undefined && value.toString() != undefined && value != 'undefined' && value.toString() != 'undefined'&& value.toString() != 'NaN' && value != NaN) 
 {
  return true;
}
else 
 { 
  return false;
}
}
