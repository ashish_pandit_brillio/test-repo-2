/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       02 Jul 2015     nitish.mishra
 *
 */

/**
 * @param {String}
 *            type Context Types: scheduled, ondemand, userinterface, aborted,
 *            skipped
 * @returns {Void}
 */
function scheduled(type) {

}

function massUpdate(recType, recId) {
	try {

		var rec = nlapiLoadRecord(recType, recId, {
			recordmode : 'dynamic'
		});

		// get the resource
		var resource = rec.getFieldValue('allocationresource');
		nlapiLogExecution('debug', 'resource', rec
				.getFieldText('allocationresource'));

		// get all allocation of this resource excluding the current one sort as
		// per end date descending order
		var search = nlapiSearchRecord('resourceallocation', null, [
				new nlobjSearchFilter('internalid', null, 'noneof', recId),
				new nlobjSearchFilter('resource', null, 'anyof', resource),
				new nlobjSearchFilter('custeventwlocation', null, 'noneof',
						'@NONE@') ], [
				new nlobjSearchColumn('custeventwlocation', null, 'group'),
				new nlobjSearchColumn('resource', null, 'group'),
				new nlobjSearchColumn('formuladate', null, 'max')
						.setFormula('{enddate}') ]);

		if (search) {
			// new worklocation
			var worklocation = search[0].getValue('custeventwlocation', null,
					'group');
			nlapiLogExecution('debug', 'work location', search[0].getText(
					'custeventwlocation', null, 'group'))

			rec.setFieldValue('custeventwlocation', worklocation);
			nlapiSubmitRecord(rec, false, true);
		}
	} catch (err) {
		nlapiLogExecution('error', 'massUpdate', err);
	}
}
