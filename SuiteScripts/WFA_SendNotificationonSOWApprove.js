/**
 * @NApiVersion 2.x
 * @NScriptType WorkflowActionScript
 * @NModuleScope public
 *
 *
 * Script Name: WFA T&M Sow Approve Send Notification
 * Author: Praveenaa
 * Company:Inspirria Cloudtech Pvt.Ltd.
 * Date-17 JUN 2020
 * Description: This script used to send the notification to PM/DM/CM when sow approves for T&M Projects  
 * Script Modification Log:
 * -- Date --			-- Modified By --				--Requested By--				-- Description --
 *
 *  Below is a summary of the process controls enforced by this script file.  The control logic is described
 * more fully, below, in the appropriate function headers and code blocks.
 * USEREVENT 
 * -afterSubmit
 * 
 * SUB-FUNCTIONS
 *  - The following sub-functions are called by the above core functions in order to maintain code
 *   modularization:
 * - NOT USED
 *
 *
 *
 *
 */
 

define(['N/record', 'N/runtime', 'N/email', 'N/search'],function(record, runtime, email , search) {
 
 
		//Function used to send the notification to PM/DM/CM when sow approves for T&M Projects  
        function wfSendNotificationtoSetUpprojectrevenumodule(scriptContext) {
             
            var o_record = scriptContext.newRecord; //GET RECORD OBJECT
			
			var s_currency = o_record.getText({ //GET CURRENCY
                    fieldId: "currency",
                   }); 
				   
				var i_Sow_Value = o_record.getValue({ //GET SOW TOTAL AMOUNT
                    fieldId: "total",
                   }); 
				var i_Project = o_record.getValue({ //GET PROJECT INTERNAL ID
				fieldId: "job",
			   });
				var s_proj_name=o_record.getText({//GET PROJECT Name
				fieldId: "job",
			   });
			   var i_customer = o_record.getValue({ //GET customer INTERNAL ID
				fieldId: "entity",
			   });
			   
	
	
		//DECLARE VARIABLES
		
			var pm_email = '';
			var cp_email = '';
			var dm_email = '';
			var cust_cp_email = '';
				
		
			if(i_customer){
				var customer_look_up = search.lookupFields({ //GET Client Partner INTERNAL ID
				type: 'customer',
				id: parseInt(i_customer),
				columns: ['custentity_clientpartner']});
				
				var s_cust_cp = customer_look_up.custentity_clientpartner[0]["value"];
			}
			if(s_cust_cp){
				var emp_lookup_cust_cp = search.lookupFields({type: 'employee',
				id: parseInt(s_cust_cp),
				columns: ['firstname','email']
				});
				var cust_cp_firstname = emp_lookup_cust_cp.firstname[0].value;
				cust_cp_email = emp_lookup_cust_cp.email[0].value;
			}
			
		var o_pjct_look_up =search.lookupFields({ //GET the Project manager, Client partner and delivery Manager from project master
				type: 'job',
				id: parseInt(i_Project),
				columns: ['custentity_projectmanager','custentity_clientpartner','entityid','custentity_deliverymanager','custentity_region','startdate','enddate','custentity_practice']
			});
		
		
		
			var s_pm = o_pjct_look_up['custentity_projectmanager'][0]["value"]; //Project manager
			var s_cp = o_pjct_look_up['custentity_clientpartner'][0]["value"];//Client partner
			var s_dm = o_pjct_look_up['custentity_deliverymanager'][0]["value"];//delivery Manager
			if(s_pm){
				
			var emp_lookup = search.lookupFields({type:'employee',id:parseInt(s_pm),columns:['firstname','email']}); 
			var pm_firstname = emp_lookup.firstname;
			pm_email = emp_lookup.email;
			log.debug('PROJ MANAGER EMAIL',pm_email);
			
			}
			if(s_cp){
				var emp_lookup_s_cp = search.lookupFields({type:'employee',id:parseInt(s_cp),columns:['firstname','email']});
				var cp_firstname = emp_lookup_s_cp.firstname;
				cp_email = emp_lookup_s_cp.email;
				log.debug('Client Partner EMAIL',cp_email);
			}
			if(s_dm){
					var emp_lookup_s_dm = search.lookupFields({type:'employee',id:parseInt(s_dm),columns:['firstname','email']});
					var dm_firstname = emp_lookup_s_dm.firstname;
					dm_email = emp_lookup_s_dm.email;
					
					log.debug('delivery MANAGER EMAIL',dm_email);
			}
			
			var s_project_region = o_pjct_look_up['custentity_region'][0]["text"]; //PROJECT REGION
			var s_project_cust = o_record.getText({'fieldId':"entity"}); //GET CUSTOMER
			var s_project_strt = o_pjct_look_up['startdate']; //START DATE
			var s_project_end = o_pjct_look_up['enddate']; //END DATE
			var s_project_prac = o_pjct_look_up['custentity_practice'][0]["text"]; //PRACTICE
					
					var strVar = '';
					strVar += '<html>';
					strVar += '<body>';

					strVar += '<p>Hello PM,</p>';
					strVar += '<p>The TCV for the T&M project with the following details has been changed with the extension / renewal opportunity.';
					
					strVar += '<p>Please update and submit the revised revenue share for the extended duration immediately else update the allocation which will get the desired revenue for the future period.</p>';

					strVar += '<p><b>NOTE</b>: If this is not carried out in NS immediately, Salesforce forecast will be under stated to the tune of this opportunity amount!</p><br>';

					strVar += '<p>Region:- ' + s_project_region + '<br>';
					strVar += 'Customer:- ' + s_project_cust + '<br>';
					strVar += 'Project:- ' + s_proj_name + '<br>';
					strVar += 'Project Start Date ' + s_project_strt + ' and End Date ' + s_project_end + '<br>';
					strVar += 'Executing Practice:- ' + s_project_prac + '<br>';
					strVar += 'Project Currency:- ' + s_currency + '<br>';
          
          
					//strVar += 'Previous Project value:- ' + parseFloat(_search_previous_sowvalue(i_customer,i_Project,o_record.id)) + '<br>';

					strVar += 'Child Opportunity value:- '+ format_pra(parseFloat(i_Sow_Value))+ '</p>';
					strVar += "";
					strVar += '<a href=https://3883006.app.netsuite.com/app/site/hosting/scriptlet.nl?script=1625&deploy=1>Link to Project setup page in Netsuite</a>';

					strVar += '<p>Regards,</p>';
					strVar += '<p>Business Ops Team</p>';

					strVar += '</body>';
					strVar += '</html>';	


					var a_emp_attachment = new Array();
					a_emp_attachment['entity'] = '200580';

					var a_RECIPIENT = new Array();
					a_RECIPIENT[0] = pm_email;
					a_RECIPIENT[1] = cp_email;
					a_RECIPIENT[2] = dm_email;
					a_RECIPIENT[3] = cust_cp_email;	

					var a_group_mail_id = new Array();
					a_group_mail_id.push('billing@brillio.com');
					a_group_mail_id.push('team.fpa@brillio.com');
					email.send({
					author: 442,
					recipients: a_RECIPIENT,
					subject: 'T&M Project Value Changed: ' + o_pjct_look_up.entityid,
					body: strVar,
					cc:a_group_mail_id,
					RelatedRecords: {
					 entityId: a_emp_attachment
					}
					});

				
			
             
            return o_record.id; 
        }
		
//Added By Praveena 
function format_pra(n) {
    n = parseFloat(n).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,");
	n = n.toString().split('.');
	return n[0]+"."+n[1];
	//return n[0];
}

         
        return {
            onAction : wfSendNotificationtoSetUpprojectrevenumodule
        }
 
})