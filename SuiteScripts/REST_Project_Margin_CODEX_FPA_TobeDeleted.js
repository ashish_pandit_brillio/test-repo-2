/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       17 Nov 2017     deepak.srinivas
 *
 */
/**
 * @param {Object} dataIn Parameter object
 * @returns {Object} Output object
 */
 var mnth_array=new Array();
mnth_array=['JAN','FEB','MAR','APR','MAY','JUN','JUL','AUG','SEP','OCT','NOV','DEC'];
var fullmonth_array=['January','February','March','April','May','June','July','August','September','October','November','December'];
function postRESTlet(dataIn) {
    try {
    
        var response = new Response();
        var current_date = nlapiDateToString(new Date());
	/*dataIn = ({"RequestType":"GET",
				"Date":"",
				"Month":"12",
			"Year":"2019"
		});*/
        var requestType = 'GET';
        nlapiLogExecution('debug', 'dataIn', JSON.stringify(dataIn));

        switch (requestType) {

            case M_Constants.Request.Get:

                if (requestType) {
                    response.Data = actual_data_flow(dataIn);
                    response.Status = true;
                } else {
                    response.Data = "Some error with the data sent";
                    response.Status = false;
                }
                break;
        }

    } catch (err) {
        nlapiLogExecution('ERROR', 'postRESTlet', err);
        response.Data = err;
        response.Status = false;
    }
    nlapiLogExecution('debug', 'response', JSON.stringify(response));
    return response;

}

function actual_data_flow(dataIn) {
    try {
        var a_project_list = new Array();

        var s_selected_project_name = '';

        var d_today = nlapiDateToString(new Date());
       
        d_today = nlapiStringToDate(d_today);
        var s_month_start = '';
        var s_month_end = '';
        var showAll = false;

        var s_from = '';
        var s_to = '';

        var project_search_results = '';
     
        var s_d_flag = false;
        var filters = [];
     
      //Actual Delta
         //Specific month actuals
        if(_logValidation(dataIn.Month) && _logValidation(dataIn.Year)){
        	
        	var s_month_start = '';
        	var s_month_end = '';
        	//Make time stamp as blank
        	//currentDate_Time = '';
        	var project_search_results = getTaggedProjectList();
            var s_project_options = '';
            var a_selected_project_list = '';
            var s_project_number = '';
            var s_project_name = '';
            for (var i = 0; project_search_results != null && i < project_search_results.length; i++) {
                var i_project_id = project_search_results[i].getValue('internalid');
                s_project_number = project_search_results[i]
                    .getValue('entityid');
                s_project_name = project_search_results[i].getValue('companyname');



                if (_logValidation(s_project_number)) {
                    a_project_list.push(s_project_number);
                }



            }
        	
            s_month_start = dataIn.Month + '/' +
                1 + '/' + dataIn.Year;
            nlapiLogExecution('DEBUG','Specific month Run - Month Start Date',s_month_start);
            
            var nextMonthStartDate = nlapiAddMonths(nlapiStringToDate(s_month_start),1);
           //Logic to find out last date in month
            var last_date = nlapiAddDays(nextMonthStartDate, -1);
            
            s_month_end = dataIn.Month + '/' +
            last_date.getDate() + '/' + dataIn.Year;
            nlapiLogExecution('DEBUG','Specific month Run - Month ENd Date',s_month_end);
            var s_from = s_month_start;
            var s_to_ = s_month_end;
        }
       // var search = nlapiLoadSearch('transaction', 2209); // TO get Project List

        
		a_project_list = removearrayduplicate(a_project_list);
        //Search for exchange Rate
        var f_rev_curr = 0;
        var f_cost_curr = 0;
       

        //var filters = [];
        //filters.push(new nlobjSearchFilter('formulatext', null, 'is', s_mont).setFormula('{custrecord_pl_currency_exchange_month}')); //NOTE : Capital 'Y' in Year
        //filters.push(new nlobjSearchFilter('formulatext', null, 'is', s_year_).setFormula('{custrecord_pl_currency_exchange_year}'));
			var a_dataVal = {};	
			a_dataVal =PL_curencyexchange_rate();	



        var i_user_id = 1618;
        //nlapiLogExecution('debug', 'user', i_user_id);
        var project_search_results = '';
		
		
			var search = nlapiLoadSearch('transaction', 2209); 
            var filters = search.getFilters();
           
            filters = filters.concat([new nlobjSearchFilter('trandate', null,
                'within', s_from, s_to_)]);


        if (a_selected_project_list == null ||
            a_selected_project_list.length == 0) {
            if (a_project_list.length > 0) {
                a_selected_project_list = a_project_list;

                showAll = true;
            } else {
                a_selected_project_list = new Array();
            }
        }

        if (a_selected_project_list != null &&
            a_selected_project_list.length != 0) {
            var s_formula = '';

            for (var i = 0; i < a_selected_project_list.length; i++) {
                if (i != 0) {
                    s_formula += "OR";
                }
                var n = a_selected_project_list[i].length;
                /*s_formula += "{custcol_project_entity_id} = '" +
                a_selected_project_list[i] + "' ";*/
                if(parseInt(n)>parseInt(9)){
                s_formula += " SUBSTR({custcolprj_name}, 0,16) = '" +
                    a_selected_project_list[i] + "' ";
                }
                else{
                s_formula += " SUBSTR({custcolprj_name}, 0,9) = '" +
                a_selected_project_list[i] + "' ";
                }
               /*  s_formula += " SUBSTR({custcolprj_name}, 0,9)','startswith',"
                	 + a_selected_project_list[i] + "' ";*/
            }

            var projectFilter = new nlobjSearchFilter('formulanumeric', null,
                'equalto', 1);

            projectFilter.setFormula("CASE WHEN " + s_formula +
                " THEN 1 ELSE 0 END");

            filters = filters.concat([projectFilter]);
        } else {
            var projectFilter = new nlobjSearchFilter('formulanumeric', null,
                'equalto', 1);

            projectFilter.setFormula("0");

            filters = filters.concat([projectFilter]);
        }



       //Date filter pushed to above 


        var columns = search.getColumns();

        //columns[0].setSort(false);
        //columns[3].setSort(true);
        //	columns[9].setSort(false);
        filters.push(new nlobjSearchFilter('type', null, 'noneof', 'SalesOrd'));
        filters.push(new nlobjSearchFilter('transactionnumbernumber', null, 'isnotempty'));
        filters.push(new nlobjSearchFilter('type', null, 'noneof', 'PurchOrd'));
     //   filters.push(new nlobjSearchFilter('type', null, 'noneof', 'CustPymt'));
	//	filters.push(new nlobjSearchFilter('type','account','anyof',['Income','OthIncome']));
     // filters.push(new nlobjSearchFilter('internalid',null,'anyof',1533450));
        //  filters.push(new nlobjSearchFilter('postingperiod',null,'abs','140'));

        filters.push(new nlobjSearchFilter('status', null, 'noneof', ['ExpRept:A', 'ExpRept:B', 'ExpRept:C', 'ExpRept:D', 'ExpRept:E', 'ExpRept:H', 'Journal:A', 'VendBill:C',
            'VendBill:D', 'VendBill:E', , 'CustInvc:E', 'CustInvc:D'
        ]));

        var search_results = searchRecord('transaction', null, filters, [
            columns[2], columns[1], columns[0], columns[3], columns[4],
            columns[5], columns[8], columns[9], columns[10], columns[11], columns[12], columns[13], columns[14], columns[15], columns[16],
            columns[17], columns[18], columns[19], columns[20], columns[21],columns[22],columns[23]
        ]);

        // Get the facility cost

        var s_facility_cost_filter = '';

        for (var i = 0; i < a_selected_project_list.length; i++) {
            s_facility_cost_filter += "'" + a_selected_project_list[i] + "'";

            if (i != a_selected_project_list.length - 1) {
                s_facility_cost_filter += ",";
            }
        }

      

        var o_json = new Object();

        var o_data = {
            //'TimeStamp': currentDate_Time,
            'Revenue': [],
            'Discount' : []

        };

        var new_object = null;

        var s_period = '';

        var a_period_list = [];

        var a_category_list = [];
        a_category_list[0] = '';
        var a_group = [];

        var a_income_group = [];
        var j_other_list = {};
        var other_list = [];


        for (var i = 0; search_results != null && i < search_results.length; i++) {
            var period = search_results[i].getText(columns[0]);

            //Code updated by Deepak, Dated - 21 Mar 17
            var s_month_year = period.split(' ');
            var s_mont = s_month_year[0];
            s_mont = getMonthCompleteName(s_mont);
            var s_year_ = s_month_year[1];
            var f_revRate = 66.0;
            var f_costRate = 66.0;
            var data_indx=(s_mont+"_"+s_year_).toString();
        
            var transaction_type = search_results[i].getText(columns[8]);

            var transaction_date = search_results[i].getValue(columns[9]);

            var i_subsidiary = search_results[i].getValue(columns[10]);

            var amount = parseFloat(search_results[i].getValue(columns[1]));

            var category = search_results[i].getValue(columns[3]);

            var s_account_name = search_results[i].getValue(columns[11]);

            var currency = search_results[i].getValue(columns[12]);

            var exhangeRate = search_results[i].getValue(columns[13]);
            
          //check for mis practice
			var mis_sub_practice = '';
			var mis_parent_practice = '';
			var index_ = search_results[i].getValue(columns[23]).indexOf(':');
			if(parseInt(index_) > parseInt(0)){
				var a_mis_practice = search_results[i].getValue(columns[23]).split(':');
				mis_parent_practice = a_mis_practice[0];
				mis_sub_practice = search_results[i].getValue(columns[23]);
			}
			else{
			mis_parent_practice = search_results[i].getValue(columns[23]);
			mis_sub_practice = '';
			}
				
			nlapiLogExecution('AUDIT','mis_parent_practice',mis_parent_practice);	
				//Hardcoded for GDS practice
			if(mis_parent_practice.trim() == 'Global Design Studio'){ 	
			mis_parent_practice = 'Design & Experience Studio';
			mis_sub_practice = 'Design & Experience Studio : Experience Design';
			}	

			if(mis_parent_practice.trim() == 'Advanced Technology Group'){ 	
				mis_parent_practice = 'Technology Advisory &Consulting';
				mis_sub_practice = '';
			}				
			
            //	nlapiLogExecution('audit','exhangeRate:- ',exhangeRate);
            //	nlapiLogExecution('audit','exhangeRate:- ',exhangeRate);

            //Transaction blocking for discount GL's
            
            if(transaction_type != 'Journal' && (parseInt(s_account_name) == parseInt(515)|| parseInt(s_account_name) == parseInt(516))){
            	continue;
            }
            
            var s_practice = search_results[i].getText(columns[14]);

            var s_project_description = search_results[i].getValue(columns[16]);
            var project_ID = '';
            var projectName = '';
            var s_subPractice = '';
            var s_parentPractice = '';
            var projectSearch = '';
            if (_logValidation(s_project_description)) {
                project_ID = s_project_description.split(' ')[0];
                projectName = s_project_description.split(' ')[1];
                s_project_number = search_results[i].getValue(columns[21]);
				if(!_logValidation(s_project_number) && _logValidation(s_project_description))
						s_project_number =  project_ID;
                //s_project_number = project_ID;
                //s_project_name =  projectName;
            }
            if (!_logValidation(s_practice) && _logValidation(project_ID)) { //&& _logValidation(project_ID)
                projectSearch = getProjectInternalID(project_ID);
             
                if (projectSearch) {
                    var projectRES = projectSearch[0];
                    nlapiLogExecution('DEBUG', 'Project ', projectRES);
                    var departmentLookup = nlapiLookupField('department', parseInt(projectRES.Department), ['name']);
                    s_subPractice = departmentLookup.name;
                    var departmentLookup_parent = nlapiLookupField('department', parseInt(projectRES.Department), ['custrecord_parent_practice'], true);
                    s_parentPractice = departmentLookup_parent.custrecord_parent_practice;
                    s_project_number = search_results[i].getValue(columns[21]);
					if(!_logValidation(s_project_number) && _logValidation(s_project_description))
						s_project_number =  s_project_description.split(' ')[0]
                  //  s_project_number = project_ID;
                    //	s_project_name =  projectName;
                }
            } 
			else {
                s_subPractice = s_practice;
                s_parentPractice = search_results[i].getText(columns[15]);
            }
            //	nlapiLogExecution('audit','s_practice name:- '+s_practice);
            //	nlapiLogExecution('audit','accnt name:- '+s_account_name);
			if(!_logValidation(s_parentPractice)){
				s_parentPractice = s_subPractice;
			}
			
			
			amount=_conversionrateIntoUSD(i_subsidiary, amount, category, exhangeRate, a_dataVal[data_indx]);
           /*var flag = false;
			if((currency != parseInt(6)) && (parseInt(i_subsidiary) == parseInt(3)) && category == 'Revenue')
			{
				amount= parseFloat(amount)* parseFloat(exhangeRate);
				amount = parseFloat(amount) /parseFloat(f_revRate);
				flag = true;
				
			}
			var eur_to_inr = 77.25;
			
			//AMount Convertion Logic
			if((parseInt(i_subsidiary) != parseInt(2))&&(parseInt(i_subsidiary) != parseInt(7)) && (currency!= parseInt(1))){
			if (category != 'Revenue' && category != 'Other Income'
				&& category != 'Discount' && parseInt(i_subsidiary) != parseInt(2)) {
					if(currency== parseInt(9))
					{
						amount = parseFloat(amount) /parseFloat(f_crc_cost_rate);
					}
					else if(currency == parseInt(8)){
					amount = parseFloat(amount) /parseFloat(f_nok_cost_rate);
					}
					else if(currency == parseInt(10))
					{
                      amount= parseFloat(amount)* parseFloat(exhangeRate);
						amount = parseFloat(amount) / parseFloat(f_aud_conv_rate);
					}
					else{
			
				amount= parseFloat(amount)* parseFloat(exhangeRate);		
				amount = parseFloat(amount) /parseFloat(f_costRate);	
				}
				
			}
              	else if(flag == false && parseInt(currency) != parseInt(1) && parseInt(i_subsidiary) != parseInt(2)){
				amount= parseFloat(amount)* parseFloat(exhangeRate);
				amount = parseFloat(amount) /parseFloat(f_revRate);
				
			//f_total_revenue += o_data[s_category][j].amount;
			}
            }
          //Added for Exchange Revision logic 8/14/2019
          else if((currency != parseInt(2)) && (parseInt(i_subsidiary) == parseInt(7)))
			{
				amount= parseFloat(amount)* parseFloat(exhangeRate);
				amount = parseFloat(amount) /parseFloat(f_gbp_rev_rate);
			}
           else if((currency == parseInt(2)) && (parseInt(i_subsidiary) == parseInt(7)))
			{
				amount= parseFloat(amount)* parseFloat(exhangeRate);
				amount = parseFloat(amount) /parseFloat(f_gbp_rev_rate);
			}
          
		*/
		  //Code Commented
             /* else if(parseInt(currency) == parseInt(4) && parseInt(i_subsidiary) == parseInt(7)){  //UK Subsidiary
              amount= parseFloat(amount)* parseFloat(exhangeRate);
				amount= parseFloat(amount) / parseFloat(f_eur_conv_rate);
				//amount = parseFloat(amount) /parseFloat(f_revRate);
				
			//f_total_revenue += o_data[s_category][j].amount;
			}
			else if(parseInt(currency) == parseInt(2) && parseInt(i_subsidiary) == parseInt(7)){  //UK Subsidiary
              amount= parseFloat(amount)* parseFloat(exhangeRate);
				amount= parseFloat(amount) / parseFloat(f_gbp_rev_rate);
				//amount = parseFloat(amount) /parseFloat(f_revRate);
				
			//f_total_revenue += o_data[s_category][j].amount;
			}
			else if(parseInt(currency) == parseInt(10) && parseInt(i_subsidiary) == parseInt(7)){  //UK Subsidiary AUD currency
              amount= parseFloat(amount)* parseFloat(exhangeRate);
				amount= parseFloat(amount) / parseFloat(f_aud_conv_rate);
			}
			else if(flag == false && parseInt(currency) != parseInt(1) && parseInt(i_subsidiary) != parseInt(2)){
				amount= parseFloat(amount)* parseFloat(exhangeRate);
				amount = parseFloat(amount) /parseFloat(f_revRate);
				
			//f_total_revenue += o_data[s_category][j].amount;
			}*/
		//}	//END



            var isWithinDateRange = true;

            var d_transaction_date = nlapiStringToDate(transaction_date,
                'datetimetz');

            if (s_from != '') {
                var d_from = nlapiStringToDate(s_from, 'datetimetz');
               
            }

            if (s_to != '') {
                var d_to = nlapiStringToDate(s_to, 'datetimetz');
            }

         
         {
            	if (parseInt(s_account_name) == parseInt(580)) { // Bad Debts
                    if (parseFloat(amount) < 0) {
                        amount = Math.abs(parseFloat(amount));
                    } else {
                        amount = '-' + amount;
                    }
                }
                //avoiding discounts from account						  
                //if (category == 'Revenue' && i_subsidiary == parseInt(2)) {
				if (category == 'Revenue' && (i_subsidiary == parseInt(2) || i_subsidiary == parseInt(11) || i_subsidiary == parseInt(12) || i_subsidiary == parseInt(7) || i_subsidiary == parseInt(3))) {
                    if (parseInt(s_account_name) != parseInt(732)) {
                        o_data[category].push({
                            'project_id': s_project_number,
                          // 	'project_desc' : search_results[i].getValue(columns[16]),
                            'period': period,
							//'subsidiary' :search_results[i].getText(columns[10]),
							//'account' : search_results[i].getText(columns[11]),
                            'amount': amount,
                            'num': search_results[i].getValue(columns[4]),
                          // 	'customer' : search_results[i].getValue(columns[16]),
                            'sub_practice': mis_sub_practice,
                            'practice': mis_parent_practice,
                        //    'type': transaction_type,
                        //  'internalid': search_results[i].getValue(columns[22]),
                            'dt': transaction_date                       
                        });
                      

                    }
                  
                } else if (category == 'Revenue') {
                    o_data[category].push({
                        'project_id': s_project_number,
                        'period': period,
						//'subsidiary' :search_results[i].getText(columns[10]),
						//'account' : search_results[i].getText(columns[11]),
                        'amount': amount,
                        'num': search_results[i].getValue(columns[4]),
                        'sub_practice': mis_sub_practice,
                        'practice': mis_parent_practice,
                       // 'type': transaction_type,
                      // 'internalid': search_results[i].getValue(columns[22]),
                        'dt': transaction_date,
                     
                    //    's_account_name' : search_results[i].getText(columns[11])
                    });
                }
           if (category == 'Discount') {
                    o_data[category].push({
                        'project_id': s_project_number,
                         'period': period,
                        'amount': amount,

                        'num': search_results[i].getValue(columns[4]),
                        'sub_practice': mis_sub_practice,
                        'practice': mis_parent_practice,

                      //  'type': transaction_type,
                      // 'internalid': search_results[i].getValue(columns[22]),
                        'dt': transaction_date,
                        //    'include' : isWithinDateRange,
                  //      's_account_name' : search_results[i].getText(columns[11])
                    });
                }
	
            }

        }
		//
		var proj_list = getTaggedTMFPproject();
		var s_month_name = mnth_array[dataIn.Month-1];
      nlapiLogExecution('debug','mnth_array',JSON.stringify(mnth_array));
       var s_year_ = dataIn.Year;
	  
       nlapiLogExecution('debug','s_year_',s_year_);
	 
	  var data_indx=(fullmonth_array[dataIn.Month-1]+"_"+s_year_).toString();
	 nlapiLogExecution('debug','data_indx',JSON.stringify(a_dataVal));
	 
    /*
		var fp_rev_rec = nlapiSearchRecord("customrecord_fp_other_validate_excel_dat",null,
												[["custrecord_fp_othr_validate_mnth","is",s_month_name],"AND", 
												["custrecord_fp_othr_validate_year","is",s_year_],"AND",
												["custrecord_fp_other_validate_project","anyof",proj_list]],
												[new nlobjSearchColumn("custrecord_fp_other_validate_project"),
												new nlobjSearchColumn("custrecord_fp_other_validate_practice"), 
												new nlobjSearchColumn("custrecord_fp_othr_validate_sub_pract"), 
												new nlobjSearchColumn("custrecord_fp_othr_validate_rev_amnt"), 
												new nlobjSearchColumn("custrecord_fp_othr_proj_type"), 
												new nlobjSearchColumn("custrecord_validate_subsi"), 
												new nlobjSearchColumn("internalid"),
												new nlobjSearchColumn("created")]);
												*/
		var fp_rev_rec = nlapiSearchRecord("customrecord_fp_other_validate_excel_dat",null,
			[["custrecord_fp_othr_validate_mnth","is",s_month_name],"AND", 
			["custrecord_fp_othr_validate_year","is",s_year_],"AND",
			["custrecord_fp_other_validate_project","anyof",proj_list]],
			[new nlobjSearchColumn("custrecord_fp_other_validate_project"),
			new nlobjSearchColumn("custrecord_fp_other_validate_practice"), 
			new nlobjSearchColumn("custrecord_fp_othr_validate_sub_pract"), 
			new nlobjSearchColumn("custrecord_fp_othr_validate_rev_amnt"), 
			new nlobjSearchColumn("custrecord_fp_othr_proj_type"), 
			new nlobjSearchColumn("custrecord_validate_subsi"),
			new nlobjSearchColumn("custentity_project_currency","custrecord_fp_other_validate_project"),
			new nlobjSearchColumn("entityid","custrecord_fp_other_validate_project"),
			//new nlobjSearchColumn("subsidiary","custrecord_fp_other_validate_project"),
			new nlobjSearchColumn("internalid"),
			new nlobjSearchColumn("created")]);
			
					if(_logValidation(fp_rev_rec))
					{
						for(var fp_indx = 0 ; fp_indx<fp_rev_rec.length;fp_indx++)
						{
							//var proj_details = nlapiLookupField('job',fp_rev_rec[fp_indx].getValue('custrecord_fp_other_validate_project'),['entityid','subsidiary','currency']);
							var amount = fp_rev_rec[fp_indx].getValue('custrecord_fp_othr_validate_rev_amnt');
							//AMount Convertion Logic
							var s_proj_currency=fp_rev_rec[fp_indx].getText("custentity_project_currency","custrecord_fp_other_validate_project");
							
							var conversion_rate=_Currency_exchangeRate(s_proj_currency, a_dataVal[data_indx],"Revenue");
							
							amount=parseFloat(amount)/ parseFloat(conversion_rate);	
							
							
						/*	if(parseInt(proj_details.currency) == parseInt(4)){//EUR-USD
								amount= parseFloat(amount)/ parseFloat(f_eur_conv_rate);		
							}
							else if(parseInt(proj_details.currency) == parseInt(2)) //GBP-USD
							{
								amount= parseFloat(amount) / parseFloat(f_gbp_rev_rate);
							}
							else if(parseInt(proj_details.currency) == parseInt(6)) //GBP-USD
							{
								amount= parseFloat(amount) / parseFloat(f_revRate);
							}
							else if(parseInt(proj_details.currency) == parseInt(10)) //AUD-USD
							{
								amount= parseFloat(amount) / parseFloat(f_aud_conv_rate);
							}
							*/
							/*else if(parseInt(proj_details.currency) == parseInt(4) && parseInt(i_subsidiary) == parseInt(7)){  //UK Subsidiary EUR-USD
								amount= parseFloat(amount) / parseFloat(f_eur_conv_rate);
							}
							else if(parseInt(proj_details.currency) == parseInt(2) && parseInt(i_subsidiary) == parseInt(7)){  //UK Subsidiary GBP-USD
								amount= parseFloat(amount) / parseFloat(f_gbp_rev_rate);
							}
							else if(flag == false && parseInt(currency) != parseInt(1) && parseInt(i_subsidiary) != parseInt(2)){
						amount= parseFloat(amount)* parseFloat(exhangeRate);
							}*/
                          
                          //
							o_data['Revenue'].push({
							'project_id':fp_rev_rec[fp_indx].getValue("entityid","custrecord_fp_other_validate_project"),
							'period': period,
						//	'subsidiary' :fp_rev_rec[fp_indx].getText('custrecord_validate_subsi'),
						//	'account' : '',
							'amount': amount,
							'num': fp_rev_rec[fp_indx].getValue('internalid'),
							'sub_practice': fp_rev_rec[fp_indx].getText('custrecord_fp_othr_validate_sub_pract'),
							'practice': fp_rev_rec[fp_indx].getText('custrecord_fp_other_validate_practice'),
							//'type': fp_rev_rec[fp_indx].getText('custrecord_fp_othr_proj_type'),
							'dt': fp_rev_rec[fp_indx].getValue('created'),
							});
		
							
						}
					}
					//
        return o_data;
    } catch (e) {
        nlapiLogExecution('DEBUG', 'Actual Data Flow Error', e);
        throw e;
    }
}
//Used to display the html, by replacing the placeholders
function replaceValues(content, oValues) {
    for (param in oValues) {
        // Replace null values with blank
        var s_value = (oValues[param] == null) ? '' : oValues[param];

        // Replace content
        content = content.replace(new RegExp('{{' + param + '}}', 'gi'),
            s_value);
    }

    return content;
}

function createPeriodList(startDate, endDate) {
    var d_startDate = nlapiStringToDate(startDate);
    var d_endDate = nlapiStringToDate(endDate);

    var arrPeriod = [];

    for (var i = 0;; i++) {
        var currentDate = nlapiAddMonths(d_startDate, i);
        if ((_logValidation(getMonthName(currentDate))) && (_logValidation(getMonthStartDate(currentDate))) && (_logValidation(getMonthEndDate(currentDate)))) {} else {
            nlapiLogExecution('debug', 'error', '909');
        }
        arrPeriod.push({
            Name: getMonthName(currentDate),
            StartDate: getMonthStartDate(currentDate),
            EndDate: getMonthEndDate(currentDate)
        });

        if (getMonthEndDate(currentDate) >= d_endDate) {
            break;
        }
    }

    var today = new Date();

    // remove the current month
    if (d_endDate.getMonth() >= today.getMonth()) {
        arrPeriod.pop();
    }

    return arrPeriod;
}

function getMonthEndDate(currentDate) {
    return nlapiAddDays(nlapiAddMonths(currentDate, 1), -1);
}

function getMonthStartDate(currentDate) {
    var month = currentDate.getMonth() + 1;
    var year = currentDate.getFullYear();
    var s_startDate = month + "/1/" + year;
    var d_startDate = nlapiStringToDate(s_startDate);
    return d_startDate;
}

function getMonthName(currentDate) {
    var monthNames = ["JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JUL", "AUG",
        "SEP", "OCT", "NOV", "DEC"
    ];
    return monthNames[currentDate.getMonth()] + " " + currentDate.getFullYear();
}

function getLastDate(month, year) {
    var date = '';
    if (month == 'January')
        date = "1/31/" + year;
    if (month == 'February')
        date = "2/28/" + year;
    if (month == 'March')
        date = "3/31/" + year;
    if (month == 'April')
        date = "4/30/" + year;
    if (month == 'May')
        date = "5/31/" + year;
    if (month == 'June')
        date = "6/30/" + year;
    if (month == 'July')
        date = "7/31/" + year;
    if (month == 'August')
        date = "8/31/" + year;
    if (month == 'September')
        date = "9/30/" + year;
    if (month == 'October')
        date = "10/31/" + year;
    if (month == 'November')
        date = "11/30/" + year;
    if (month == 'December')
        date = "12/31/" + year;

    return date;
}

//Get the complete month name
function getMonthCompleteName(month) {
    var s_mont_complt_name = '';
    if (month == 'Jan')
        s_mont_complt_name = 'January';
    if (month == 'Feb')
        s_mont_complt_name = 'February';
    if (month == 'Mar')
        s_mont_complt_name = 'March';
    if (month == 'Apr')
        s_mont_complt_name = 'April';
    if (month == 'May')
        s_mont_complt_name = 'May';
    if (month == 'Jun')
        s_mont_complt_name = 'June';
    if (month == 'Jul')
        s_mont_complt_name = 'July';
    if (month == 'Aug')
        s_mont_complt_name = 'August';
    if (month == 'Sep')
        s_mont_complt_name = 'September';
    if (month == 'Oct')
        s_mont_complt_name = 'October';
    if (month == 'Nov')
        s_mont_complt_name = 'November';
    if (month == 'Dec')
        s_mont_complt_name = 'December';

    return s_mont_complt_name;
}

function _logValidation(value) {	
    if (value != '- None -' &&  value != null && value != '- None -' && value != null && value != '' && value != undefined && value != undefined && value != 'undefined' && value != 'undefined' && value != 'NaN' && value != NaN) {	
        return true;	
    } else {	
        return false;	
    }	
}
/*function _logValidation(value) {
    if (value != '- None -' && value.toString() != '- None -' && value != null && value.toString() != null && value != '' && value != undefined && value.toString() != undefined && value != 'undefined' && value.toString() != 'undefined' && value.toString() != 'NaN' && value != NaN) {
        return true;
    } else {
        return false;
    }
}
*/
function getSelectedProjects(project) {
    try {



        // get the list of project the user has access to
        var project_filter = [
            [
                ['internalid',
                    'anyOf', project
                ]
            ]
        ];

        var project_search_results = searchRecord('job', null, project_filter, [new nlobjSearchColumn("entityid"),
            new nlobjSearchColumn("altname"),
            new nlobjSearchColumn("companyname"),
            new nlobjSearchColumn("internalid")
        ]);

        nlapiLogExecution('debug', 'project count',
            project_search_results.length);

        if (project_search_results.length == 0) {
            throw "You didn't selected any projects.";
        }

        return project_search_results;
    } catch (err) {
        nlapiLogExecution('ERROR', 'getTaggedProjectList', err);
        throw err;
    }
}

function getTaggedProjectList() {
    try {
        var project_search_results = searchRecord('job', 'customsearch2430', null, [new nlobjSearchColumn("entityid"),
            new nlobjSearchColumn("altname"),
            new nlobjSearchColumn("internalid"),
            new nlobjSearchColumn("companyname")
        ]);



        if (project_search_results.length == 0) {
            throw "You don't have any projects under you.";
        }
        nlapiLogExecution('debug', 'project count',
            project_search_results.length);

        return project_search_results;
    } catch (err) {
        nlapiLogExecution('ERROR', 'getTaggedProjectList', err);
        throw err;
    }
}
function getTaggedTMFPproject() {
    try {
		var jobSearch = nlapiSearchRecord("job",null,
[
   ["custentity_project_allocation_category","anyof","1"], 
   "AND", 
   ["status","noneof","1"], 
   "AND", 
   ["jobbillingtype","anyof","FBM"], 
   "AND", 
   ["formuladate: {enddate}","onorafter","03/01/2019"]
], 
[
   new nlobjSearchColumn("subsidiary"), 
   new nlobjSearchColumn("jobbillingtype"),
   new nlobjSearchColumn("internalid"),
]
);


        if (jobSearch.length == 0) {
            throw "You don't have any projects under you.";
        }
        nlapiLogExecution('debug', 'project count',
            jobSearch.length);
	var job_array= new Array();
	for(var j_indx = 0; j_indx < jobSearch.length;j_indx++)
	{
		job_array.push(jobSearch[j_indx].getValue('internalid'));
	}
	
        return job_array;
    } catch (err) {
        nlapiLogExecution('ERROR', 'getTaggedProjectList', err);
        throw err;
    }
}

function getProjectInternalID(pro_id) {
    try {
        nlapiLogExecution('ERROR', 'pro_id', pro_id);
        var project_search_results = searchRecord('job', null, [new nlobjSearchFilter('entityid', null, 'is', pro_id)], [new nlobjSearchColumn("entityid"),
            new nlobjSearchColumn("custentity_practice"),
            new nlobjSearchColumn("internalid"),
            new nlobjSearchColumn("companyname")
        ]);
        var results = [];
        if (_logValidation(project_search_results)) {
            results.push({
                'ID': project_search_results[0].getValue('internalid'),
                'Department': project_search_results[0].getValue('custentity_practice')
            });
        }
        return results;
    } catch (err) {
        nlapiLogExecution('ERROR', 'getTaggedProjectList', err);
        throw err;
    }
}

function date_time() {
    var date = new Date();
    var date_ = nlapiDateToString(date);
    var am_pm = "am";
    var hour = date.getHours();

    var currentDateTime = new Date();
    var companyTimeZone = nlapiLoadConfiguration('companyinformation').getFieldText('timezone');
    var timeZoneOffSet = (companyTimeZone.indexOf('(GMT)') == 0) ? 0 : new Number(companyTimeZone.substr(4, 6).replace(/\+|:00/gi, '').replace(/:30/gi, '.5'));
    var UTC = currentDateTime.getTime() + (currentDateTime.getTimezoneOffset() * 60000);
    var companyDateTime = UTC + (timeZoneOffSet * 60 * 60 * 1000);

    if (hour >= 12) {
        am_pm = "pm";
    }
    if (hour > 12) {
        hour = hour - 12;
    }
    if (hour < 10) {
        hour = "0" + hour;
    }

    var minute = date.getMinutes();
    if (minute < 10) {
        minute = "0" + minute;
    }
    var sec = date.getSeconds();
    if (sec < 10) {
        sec = "0" + sec;
    }

    return date_ + ' ' + hour + ':' + minute + ' ' + am_pm;
}

function removearrayduplicate(array) {
    var newArray = new Array();
    label: for (var i = 0; i < array.length; i++) {
          for (var j = 0; j < array.length; j++) {
                if (newArray[j] == array[i])
                      continue label;
          }
          newArray[newArray.length] = array[i];
    }
    return newArray;
}