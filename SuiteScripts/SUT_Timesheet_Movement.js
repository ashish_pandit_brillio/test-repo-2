/**
 * Suitelet to move timesheet from billable to non-billable and vice-versa based
 * on resource allocation
 * 
 * Version Date Author Remarks 1.00 05 May 2016 Nitish Mishra
 * 
 */

function suitelet(request, response) {
	try {

		if (request.getMethod() == "GET") {
			createTimesheetMovementForm(request);
		} else {
			var mode = request.getParameter('custpage_mode');
			nlapiLogExecution('debug', 'mode', mode);

			if (mode == 'GetTimesheet') {
				createTimesheetMovementForm(request);
			} else if (mode == 'MoveTimesheet') {
				submitTimesheetMovementForm(request);
			}
		}
	} catch (err) {
		nlapiLogExecution('ERROR', 'suitelet', err);
		displayErrorForm(err, 'Timesheet Movement Page');
	}
}

function createTimesheetMovementForm(request) {
	try {
		var form = nlapiCreateForm('Timesheet Movement Page');

		var showAlert = request.getParameter('showalert');
		if (showAlert == 'T') {
			form.addFieldGroup('custpage_message', 'Message');
			form
			        .addField('custpage_message', 'inlinehtml', 'Message',
			                null, 'custpage_message')
			        .setDefaultValue(
			                'Request Submitted. Data is being processed. Once completed, you will receive an email.');
		}

		form.addFieldGroup('custpage_select', 'Select');
		var employeeField = form.addField('custpage_employee', 'select',
		        'Employee', 'employee', 'custpage_select');
		employeeField.setMandatory(true);

		// var projectField = form.addField('custpage_project', 'select',
		// 'Project', 'job', 'custpage_select');
		// projectField.setMandatory(true);

		var fromDateField = form.addField('custpage_date_from', 'date',
		        'From Date', null, 'custpage_select');
		fromDateField.setMandatory(true);

		var toDateField = form.addField('custpage_date_to', 'date', 'To Date',
		        null, 'custpage_select');
		toDateField.setMandatory(true);

		// var criteriaField = form.addField('custpage_criteria', 'select',
		// 'Criteria', null, 'custpage_select');
		// criteriaField.addSelectOption('1', 'Unbilled To Billed Movement');
		// criteriaField.addSelectOption('2', 'Billed To Unbilled Movement');
		// criteriaField.addSelectOption('', '', true);
		// criteriaField.setMandatory(true);

		var modeField = form.addField('custpage_mode', 'text', 'Mode', null,
		        'custpage_select');
		modeField.setDisplayType('hidden');

		// set default values
		var mode = request.getParameter('custpage_mode');
		var employee = request.getParameter('custpage_employee');
		// var project = request.getParameter('custpage_project');
		var fromDate = request.getParameter('custpage_date_from');
		var toDate = request.getParameter('custpage_date_to');
		// var criteria = request.getParameter('custpage_criteria');

		employeeField.setDefaultValue(employee);
		// projectField.setDefaultValue(project);
		fromDateField.setDefaultValue(fromDate);
		toDateField.setDefaultValue(toDate);
		// criteriaField.setDefaultValue(criteria);

		if (!mode) {
			modeField.setDefaultValue('GetTimesheet');
			form.addSubmitButton('Get Timesheets');
		} else if (mode == 'GetTimesheet' || mode == 'MoveTimesheet') {
			form.setScript('customscript_cs_sut_timesheet_movement');
			modeField.setDefaultValue('MoveTimesheet');

			employeeField.setDisplayType('inline');
			// projectField.setDisplayType('inline');
			fromDateField.setDisplayType('inline');
			toDateField.setDisplayType('inline');
			// criteriaField.setDisplayType('inline');

			form.addSubmitButton('Move Timesheets');
			form.addButton('custpage_btn_go_back', 'Go Back',
			        'goBackToSelectScreen()');

			// add a time entry sublist
			// create a sublist with all the time entry
			var timeList = form.addSubList('custpage_list_time', 'list',
			        'Time Entry');
			timeList.addRefreshButton();
			timeList
			        .addButton('custpage_btn_mark_all', 'Mark All', 'markAll()');
			timeList.addButton('custpage_btn_unmark_all', 'Unmark All',
			        'unmarkAll()');
			timeList.addField('internalid', 'text', 'Internal Id')
			        .setDisplayType('hidden');
			timeList.addField('timesheet', 'select', 'Timesheet', 'timesheet')
			        .setDisplayType('hidden');
			timeList.addField('employee', 'text', 'Employee').setDisplayType(
			        'inline');
			timeList.addField('project', 'text', 'Project').setDisplayType(
			        'inline');
			timeList.addField('s_date', 'text', 'Date')
			        .setDisplayType('inline');
			timeList.addField('event', 'text', 'Event')
			        .setDisplayType('inline');
			timeList.addField('item', 'text', 'Item').setDisplayType('inline');
			timeList.addField('duration', 'text', 'Duration').setDisplayType(
			        'inline');
			timeList.addField('billingstatus', 'text', 'Billing Status')
			        .setDisplayType('inline');
			timeList.addField('isbillable', 'text', 'Is Billable')
			        .setDisplayType('inline');
			timeList.addField('rate', 'text', 'Rate').setDisplayType('inline');
			timeList.addField('status', 'text', 'Approval Status')
			        .setDisplayType('inline');
			timeList.addField('new_project', 'select', 'Project', 'job')
			        .setDisplayType('inline');
			timeList.addField('new_is_billable', 'checkbox', 'New Is Billable')
			        .setDisplayType('inline');
			timeList.addField('new_rate', 'currency', 'New Bill Rate')
			        .setDisplayType('inline');
			timeList.addField('apply', 'checkbox', 'Apply');

			// get all timesheets (unbilled) during the specified period
			var timeEntrySearchFilter = [
			        new nlobjSearchFilter('employee', null, 'anyof', employee),
			        // new nlobjSearchFilter('customer', null, 'anyof',
			        // project),
			        new nlobjSearchFilter('type', null, 'anyof', 'A'),
			        new nlobjSearchFilter('date', null, 'within', fromDate,
			                toDate) ];

			// if (criteria == 1) {
			// timeEntrySearchFilter.push(new nlobjSearchFilter('billable',
			// null, 'is', 'F'));
			// } else if (criteria == 2) {
			// timeEntrySearchFilter.push(new nlobjSearchFilter('billable',
			// null, 'is', 'T'));
			// }

			var timeEntrySearch = searchRecord('timebill', null,
			        timeEntrySearchFilter, [ new nlobjSearchColumn('employee'),
			                new nlobjSearchColumn('customer'),
			                new nlobjSearchColumn('date'),
			                new nlobjSearchColumn('approvalstatus'),
			                new nlobjSearchColumn('casetaskevent'),
			                new nlobjSearchColumn('status'),
			                new nlobjSearchColumn('durationdecimal'),
			                new nlobjSearchColumn('internalid'),
			                new nlobjSearchColumn('item'),
			                new nlobjSearchColumn('type'),
			                new nlobjSearchColumn('isbillable'),
			                new nlobjSearchColumn('rate'),
			                new nlobjSearchColumn('timesheet') ]);

			var timeEntryList = [];

			if (timeEntrySearch) {
				timeEntrySearch
				        .forEach(function(timeEntry) {
					        timeEntryList
					                .push({
					                    internalid : timeEntry
					                            .getValue('internalid'),
					                    employee : timeEntry
					                            .getText('employee'),
					                    project : timeEntry.getText('customer'),
					                    s_date : timeEntry.getValue('date'),
					                    d_date : nlapiStringToDate(timeEntry
					                            .getValue('date')),
					                    event : timeEntry
					                            .getText('casetaskevent'),
					                    item : timeEntry.getValue('item'),
					                    duration : timeEntry
					                            .getValue('durationdecimal'),
					                    billingstatus : timeEntry
					                            .getText('billingstatus'),
					                    isbillable : timeEntry
					                            .getValue('isbillable') == 'T' ? 'Yes'
					                            : 'No',
					                    rate : timeEntry.getValue('rate'),
					                    status : timeEntry
					                            .getText('approvalstatus'),
					                    type : timeEntry.getText('type'),
					                    timesheet : timeEntry
					                            .getValue('timesheet'),
					                    new_rate : 0,
					                    new_is_billable : 'F'
					                });
				        });
			}

			// get all the employees allocation during this period
			var allocationList = getAllocation(employee, fromDate, toDate);

			// loop through the allocation and timesheet list and set the new
			// rates and billable status
			for (var i = 0; i < timeEntryList.length; i++) {

				for (var j = 0; j < allocationList.length; j++) {

					if (timeEntryList[i].d_date >= allocationList[j].StartDate
					        && timeEntryList[i].d_date <= allocationList[j].EndDate) {
						timeEntryList[i].new_rate = allocationList[j].Rate;
						timeEntryList[i].new_is_billable = allocationList[j].IsBillable;
						timeEntryList[i].new_project = allocationList[j].Project;
					}
				}
			}

			// set the values to the lines
			timeList.setLineItemValues(timeEntryList);
		}

		response.writePage(form);
	} catch (err) {
		nlapiLogExecution('ERROR', 'createTimesheetMovementForm', err);
		throw err;
	}
}

function submitTimesheetMovementForm(request) {
	try {
		var employee = request.getParameter('custpage_employee');
		var project = request.getParameter('custpage_project');
		var fromDate = request.getParameter('custpage_date_from');
		var toDate = request.getParameter('custpage_date_to');

		var linecount = request.getLineItemCount('custpage_list_time');
		var finalData = {};

		for (var linenum = 1; linenum <= linecount; linenum++) {
			var isApplied = request.getLineItemValue('custpage_list_time',
			        'apply', linenum);

			if (isApplied == 'T') {
				var timeEntry = request.getLineItemValue('custpage_list_time',
				        'internalid', linenum);
				var timeSheet = request.getLineItemValue('custpage_list_time',
				        'timesheet', linenum);
				var newIsBillable = request.getLineItemValue(
				        'custpage_list_time', 'new_is_billable', linenum);
				var newBillRate = request.getLineItemValue(
				        'custpage_list_time', 'new_rate', linenum);

				var finalDataTimesheet = finalData[timeSheet];

				if (!finalDataTimesheet) {
					finalData[timeSheet] = {
						List : []
					};
				}

				finalData[timeSheet].List.push({
				    Entry : timeEntry,
				    Billable : newIsBillable,
				    Rate : newBillRate
				});
			}
		}

		nlapiLogExecution('debug', 'final data', JSON.stringify(finalData));
		var requestObject = {
		    Employee : employee,
		    Project : project,
		    StartDate : fromDate,
		    EndDate : toDate,
		    TimeEntryList : finalData
		};

		nlapiScheduleScript('customscript_sch_timesheet_movement', null, {
			custscript_ts_movement_data : JSON.stringify(requestObject)
		});

		response.sendRedirect('SUITELET',
		        'customscript_sut_timesheet_movement',
		        'customdeploy_sut_timesheet_movement', null, {
		            showalert : 'T',
		            custpage_employee : employee,
		            custpage_date_to : toDate,
		            custpage_date_from : fromDate
		        });
	} catch (err) {
		nlapiLogExecution('ERROR', 'submitTimesheetMovementForm', err);
		throw err;
	}
}

function getAllocation(employee, startDate, endDate) {
	try {
		var filter1 = new nlobjSearchFilter('formuladate', null, 'notafter',
		        endDate);
		filter1.setFormula('{startdate}');

		var filter2 = new nlobjSearchFilter('formuladate', null, 'notbefore',
		        startDate);
		filter2.setFormula('{enddate}');

		var allocationSearch = searchRecord('resourceallocation', null, [
		        filter1, filter2,
		        new nlobjSearchFilter('resource', null, 'anyof', employee)
		// new nlobjSearchFilter('project', null, 'anyof', project)
		], [ new nlobjSearchColumn('company'),
		        new nlobjSearchColumn('resource'),
		        new nlobjSearchColumn('startdate'),
		        new nlobjSearchColumn('enddate'),
		        new nlobjSearchColumn('custeventrbillable'),
		        new nlobjSearchColumn('custevent3') ]);

		var allocationList = [];

		if (allocationSearch) {
			allocationSearch.forEach(function(allocation) {
				allocationList
				        .push({
				            Project : allocation.getValue('company'),
				            StartDate : nlapiStringToDate(allocation
				                    .getValue('startdate')),
				            EndDate : nlapiStringToDate(allocation
				                    .getValue('enddate')),
				            Rate : allocation.getValue('custevent3'),
				            IsBillable : allocation
				                    .getValue('custeventrbillable')
				        });
			});
		}

		return allocationList;
	} catch (err) {
		nlapiLogExecution('ERROR', 'getAllocation', err);
		throw err;
	}
}

// ----------------------- Client Script------------------------------------//

function goBackToSelectScreen() {
	var employee = nlapiGetFieldValue('custpage_employee');
	var project = nlapiGetFieldValue('custpage_project');
	var fromDate = nlapiGetFieldValue('custpage_date_from');
	var toDate = nlapiGetFieldValue('custpage_date_to');
	// var criteria = nlapiGetFieldValue('custpage_criteria');

	var url = nlapiResolveURL('SUITELET',
	        'customscript_sut_timesheet_movement',
	        'customdeploy_sut_timesheet_movement');

	window.location = url + "&custpage_employee=" + employee
	        + "&custpage_project=" + project + "&custpage_date_from="
	        + fromDate + "&custpage_date_to=" + toDate;
	// + "&custpage_criteria="
	// + criteria;
}

function markAll() {
	var linecount = nlapiGetLineItemCount('custpage_list_time');

	for (var linenum = 1; linenum <= linecount; linenum++) {
		nlapiSelectLineItem('custpage_list_time', linenum);
		nlapiSetCurrentLineItemValue('custpage_list_time', 'apply', 'T');
		nlapiCommitLineItem('custpage_list_time');
	}
}

function unmarkAll() {
	var linecount = nlapiGetLineItemCount('custpage_list_time');

	for (var linenum = 1; linenum <= linecount; linenum++) {
		nlapiSelectLineItem('custpage_list_time', linenum);
		nlapiSetCurrentLineItemValue('custpage_list_time', 'apply', 'F');
		nlapiCommitLineItem('custpage_list_time');
	}
}

function onSave() {
	var linecount = nlapiGetLineItemCount('custpage_list_time');
	var applied = 0;

	for (var linenum = 1; linenum <= linecount; linenum++) {
		var isApplied = nlapiGetLineItemValue('custpage_list_time', 'apply',
		        linenum);

		if (isApplied == 'T') {
			applied++;
			var isBillable = nlapiGetLineItemValue('custpage_list_time',
			        'new_is_billable', linenum);
			var billRate = parseFloat(nlapiGetLineItemValue(
			        'custpage_list_time', 'new_rate', linenum));

			if ((isBillable == 'T' && billRate <= 0)
			        || (isBillable == 'F' && parseInt(billRate) != 0)) {
				alert("Incorrect data at line " + linenum);
				return false;
			}
		}
	}

	if (applied == 0) {
		alert("No line has been applied.");
		return false;
	}

	return true;
}

function fieldChanged(type, name, linenum) {

	// if (type == 'custpage_list_time' && name == 'new_is_billable') {
	// console.log('type : ' + type + '\nname : ' + name + '\nlinenum : '
	// + linenum);
	// var isBillable = nlapiGetCurrentLineItemValue('custpage_list_time',
	// 'new_is_billable');
	// console.log('isBillable ' + isBillable);
	//
	// if (isBillable == 'F') {
	// console.log('Change bill rate');
	// nlapiSetCurrentLineItemValue('custpage_list_time', 'new_bill_rate',
	// 0.0);
	// nlapiCommitLineItem('custpage_list_time');
	// }
	// }
}