/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       07 Jun 2017     bidhi.raj
 *
 */

/**
 * @param {String} type Context Types: scheduled, ondemand, userinterface, aborted, skipped
 * @returns {Void}
 */
function Main() {
	try {

		var columns = new Array();
		columns[0] = new nlobjSearchColumn('firstname');
		columns[1] = new nlobjSearchColumn('email');

		// search for employee with 10 days of service as of today

		var emp_search = nlapiSearchRecord('employee', 'customsearch10th_day_mail',
				null, columns);

		nlapiLogExecution('debug', 'emp_search.length', emp_search.length);

		if (isNotEmpty(emp_search)) {

			for (var i = 0; i < emp_search.length; i++) {

				sendServiceMails(emp_search[i].getValue('firstname'),
						emp_search[i].getValue('email'), emp_search[i].getId());
			}
		}

	} catch (err) {
		nlapiLogExecution('error', 'Main', err);
	}
}

function sendServiceMails(firstName, email, emp_id) {

	try {
		var fileObj = nlapiLoadFile(485444);
		var mailTemplate = serviceTemplate(firstName);
		nlapiLogExecution('debug', 'chekpoint', email);
		nlapiSendEmail('442', email, mailTemplate.MailSubject,
				mailTemplate.MailBody, null, null, null, fileObj, {
					entity : emp_id
				});
	} catch (err) {
		nlapiLogExecution('error', 'sendServiceMails', err);
		throw err;
	}
}

function serviceTemplate(firstName) {
	var htmltext = '';

	htmltext += '<table border="0" width="100%"><tr>';
	htmltext += '<td colspan="4" valign="top">';
	htmltext += '<p>Dear ' + firstName + ',</p>';
	htmltext += '<p>Please find attached the User Guide on Taleo<p>';
	htmltext += '<p>Taleo is fully integrated talent acquisition solution including recruitment marketing and employee referrals to source talent, end to end automation and streamlined employee onboarding.</p>'
	htmltext += '<p>Thanks & Regards,</p>';
	htmltext += '<p>Information Systems</p>';

	htmltext += '</td></tr>';
	htmltext += '</table>';
	htmltext += '<hr width="100%" size="1" noshade color="#CCCCCC">';
	htmltext += '<table width="100%" border="0" cellspacing="0" cellpadding="0">';
	htmltext += '<tr>'; 
	htmltext += '<td align="right">';
	htmltext += '<font style="font-size:9px; font-family:Verdana,Arial,Helvetica,sans-serif; color:#999999;">Brillio is powered by <a href="http://www.netsuite.com/" style="color:#999999;">NetSuite</a> One System. No Limits.</font>';
	htmltext += '</td>';
	htmltext += '</tr>';
	htmltext += '</table>';
	htmltext += '</body>';
	htmltext += '</html>';

	return {
		MailBody : htmltext,
		MailSubject : "Taleo User Guide"
	};
}
