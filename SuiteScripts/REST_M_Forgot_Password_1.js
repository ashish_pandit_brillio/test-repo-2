/**
 * RESTlet for forgot password module
 * 
 * Version Date Author Remarks 1.00 29 Apr 2016 Nitish Mishra
 * 
 */

function postRESTlet(dataIn) {
	var response = new Response();
	try {
		nlapiLogExecution('debug', 'dataIn', JSON.stringify(dataIn));

		if (dataIn.EmailId) { // && dataIn.Data && dataIn.Data.EmployeeId
			var employeeId = getUserUsingEmailId(dataIn.EmailId);
			// var employeeEmpId = dataIn.Data.EmployeeId;
			var requestType = dataIn.RequestType;

			switch (requestType) {

				case M_Constants.Request.Get:
					var employeeValue = nlapiLookupField('employee',
					        employeeId, [ 'custentity_empid',
					                'custentity_personalemailid',
					                'mobilephone', 'phone','custentity_brillio_address_1','custentity_brillio_address_2','custentity_list_brillio_location_e']);
					var locationText = nlapiLookupField('employee', employeeId,
					        'location', true);
					
					var address = employeeValue.custentity_brillio_address_1 +' '+employeeValue.custentity_brillio_address_2;
					//var adress_2 = 	employeeValue
					// if (employeeEmpId == employeeValue.custentity_empid) {
					var phoneNumber = employeeValue.phone ? employeeValue.phone
					        : employeeValue.mobilephone;
					var alternateEmail = employeeValue.custentity_personalemailid;
                   var Baddress = employeeValue.custentity_list_brillio_location_e;
					response.Status = true;
					response.Data = {
					    Phone : phoneNumber,
					    Email : alternateEmail,
					    Location : Baddress,
						WorkAddress: address
					};
					// } else {
					// throw "Incorrect Details";
					// }
				break;
			}
		} else {
			throw "Email Id Not Provided";
		}

	} catch (err) {
		nlapiLogExecution('ERROR', 'postRESTlet', err);
		response.Data = err;
	}
	return response;
}