// create logs for mobility
function createLog(recordType, internalId, type, author, memo) {
	try {
		var logRecord = nlapiCreateRecord('customrecord_mobility_log');
		logRecord.setFieldValue('custrecord_m_log_record', recordType);
		logRecord.setFieldValue('custrecord_m_log_type', type);
		logRecord.setFieldValue('custrecord_m_log_internal_id', internalId);
		logRecord.setFieldValue('custrecord_m_log_author', author);
		logRecord.setFieldValue('custrecord_m_log_update_memo', memo);
		var logId = nlapiSubmitRecord(logRecord);
		nlapiLogExecution('debug', 'mobility log created', logId);
	} catch (err) {
		nlapiLogExecution('ERROR', 'createLog', err);
	}
}

function getUserUsingEmailId(emailId) {
	try {
		var employeeSearch = nlapiSearchRecord('employee', null, [
		        new nlobjSearchFilter('email', null, 'is', emailId),
		        new nlobjSearchFilter('custentity_employee_inactive', null, 'is', 'F') ]);

		if (employeeSearch) {
			return employeeSearch[0].getId();
		} else {
			throw "User does not exist";
		}
	} catch (err) {
		nlapiLogExecution('ERROR', 'getUserUsingEmailId', err);
		throw err;
	}
}
function ResultMorethanThousand(Rec_cnt,filt,col,SearchRecord)
{
nlapiLogExecution('debug', 'Rec_cnt',Rec_cnt.length);
nlapiLogExecution('debug', 'filt', JSON.stringify(filt));
nlapiLogExecution('debug', 'col', JSON.stringify(col));
nlapiLogExecution('debug', 'SearchRecord:',SearchRecord);
  var completeResultSet = Rec_cnt;
  var temp =0;
  while(Rec_cnt.length == 1000)
	{
	//nlapiLogExecution('debug', 'temp CNT:',temp++);
		var lastId = Rec_cnt[999].getId();
		filt.push( new nlobjSearchFilter('internalid',null,'greaterthan',lastId));
		Rec_cnt = nlapiSearchRecord(SearchRecord,null,filt,col);
		completeResultSet = completeResultSet.concat(Rec_cnt);
	}
  nlapiLogExecution('debug', 'ResultMorethanThousand for the'+SearchRecord+' is'+completeResultSet.length);
	return completeResultSet;
}