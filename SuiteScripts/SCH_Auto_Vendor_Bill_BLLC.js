/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       29 June 2017     Jayesh Dinde
 *
 */

function scheduled_createBill()
{
	try {
      
		var o_context = nlapiGetContext();
		//var i_invoice_id = o_context.getSetting('SCRIPT', 'custscript_invoice_to_process');
		
		var a_filter_get_approved_invoices = [['custbody_auto_vendor_bill_invoice', 'is', 'T'], 'AND',
											['mainline', 'is', 'T'], 'AND',
											['status','noneof','CustInvc:D'] ,'AND',
											[["trandate","within","lastmonth"],'OR',["trandate","within","thismonth"]]
                                             ];
		
		//var a_filter_get_approved_invoices = [['internalid', 'anyof', [1724116]]];
		
		var a_get_approved_invoices = nlapiSearchRecord('invoice',null,a_filter_get_approved_invoices);
		if (a_get_approved_invoices) {
			nlapiLogExecution('audit', 'invoice to process len:- ' + a_get_approved_invoices.length);
			for (var i_index = 0; i_index < a_get_approved_invoices.length; i_index++) {
				if (o_context.getRemainingUsage() < 1000) {
					nlapiYieldScript();
				}
				var i_invoice_id = a_get_approved_invoices[i_index].getId();
				nlapiLogExecution('audit', 'i_invoice_id:- ' + i_invoice_id);
				var o_invoice_rcrd = nlapiLoadRecord('invoice', i_invoice_id);
				if (o_invoice_rcrd) {
					var a_employee_list = new Array();
					var trandate = o_invoice_rcrd.getFieldValue('trandate');
					var s_billing_from = o_invoice_rcrd.getFieldValue('custbody_billfrom');
					var s_billing_to = o_invoice_rcrd.getFieldValue('custbody_billto');
					
					var d_billingFrom = nlapiStringToDate(s_billing_from);
					var d_billingTo = nlapiStringToDate(s_billing_to);
					
					var i_billable_time_count = o_invoice_rcrd.getLineItemCount('time');
					
					for (var i_line_num = 1; i_line_num <= i_billable_time_count; i_line_num++) {
						var b_timesheet_applied = o_invoice_rcrd.getLineItemValue('time', 'apply', i_line_num);
						if (b_timesheet_applied == 'T') {
							var i_employee_id = o_invoice_rcrd.getLineItemValue('time', 'employee', i_line_num);
							
							if (a_employee_list.indexOf(i_employee_id) == -1) {
								a_employee_list.push(i_employee_id);
							}
						}
					}
					
					for (var i_emp_to_bill = 0; i_emp_to_bill < a_employee_list.length; i_emp_to_bill++) {
						nlapiLogExecution('audit', 'emp id to create bill:- ' + a_employee_list[i_emp_to_bill], 'total emp length:- ' + a_employee_list.length);
						
						var i_emp_subsidiary = nlapiLookupField('employee', parseInt(a_employee_list[i_emp_to_bill]), ['subsidiary','custentity_employeetype']);
						
						if (parseInt(i_emp_subsidiary.subsidiary) == parseInt(2)) //&& parseInt(i_emp_subsidiary.custentity_employeetype) == parseInt(6) ) {
							//var url = nlapiResolveURL('SUITELET', 1357, 1, true);
							{	
							var strSelectedValues = JSON.stringify({
								'from': s_billing_from,
								'to': s_billing_to,
								'employee': a_employee_list[i_emp_to_bill],
								'i_invoice_id': i_invoice_id,
								'trandate':trandate
							});
							//nlapiLogExecution('audit', 'url:- ', url);
							
							/*var header = {
						 "User-Agent-x": "SuiteScript-Call"
						 };
						 var response = nlapiRequestURL(url, strSelectedValues, header);*/
						 if(o_context.getRemainingUsage() < 1000) {
								nlapiYieldScript();
							}
							var response = sutCreateVendorBill(strSelectedValues);
							nlapiLogExecution('audit', 'response:- ', response);
							if (response) {
								//var responseData = response.getBody();
								//nlapiLogExecution('audit', 'response:- ', responseData);
								var responseJson = JSON.parse(JSON.stringify(response));
								if (responseJson.status == 1) {
									nlapiLogExecution('audit', 'vendor bill created:- ' + responseJson.vendor_bill_id);
								}
							}
						}
						//else{
						//nlapiSubmitField('invoice', parseInt(i_invoice_id), 'custbody_auto_vendor_bill_invoice', 'F');
						//}
					}
					
				//nlapiSubmitField('invoice',i_invoice_id,'custbody_auto_vendor_bill_invoice','F');
				//o_invoice_rcrd.setFieldValue('custbody_auto_vendor_bill_invoice','F');
				//nlapiSubmitRecord(o_invoice_rcrd,true,true);
				}
			}
		}
	}
	catch(err)
	{
		nlapiLogExecution('ERROR','ERROR MSG SCHDL:- ',err);
	}
}