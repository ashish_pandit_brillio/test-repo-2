/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       23 May 2017     bidhi.raj
 *
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType
 *   
 * @param {String} type Operation types: create, edit, view, copy, print, email
 * @param {nlobjForm} form Current form
 * @param {nlobjRequest} request Request object
 * @returns {Void}
 */
function userEventBeforeLoad(){
 var filter=[];
   var search=new nlapiSearchRecord('transaction','customsearch2196',null,[new nlobjSearchColumn('internalid')]);
   for(var i=0;i<search.length;i++)
   {
		var id=search[i].getValue('internalid');
		var rec=nlapiLoadRecord('vendorbill',id);
		nlapiSubmitRecord(rec);
		
  }
	
  
}
//validate blank entries
function _logValidation(value) 
{
 if(value != null && value.toString() != null && value != '' && value != undefined && value.toString() != undefined && value != 'undefined' && value.toString() != 'undefined'&& value.toString() != 'NaN' && value != NaN) 
 {
  return true;
 }
 else 
 { 
  return false;
 }
}

