// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
    /*
     Script Name: Cli_validate_Customer_ID
     Author: Vikrant S. Adhav
     Company: Aashna CloudTech
     Date: 21-03-2014
     Description:
     
     Script Modification Log:
     -- Date --			-- Modified By --				--Requested By--				-- Description --


     Below is a summary of the process controls enforced by this script file.  The control logic is described
     more fully, below, in the appropriate function headers and code blocks.
     
     PAGE INIT
     - pageInit(type)
    
     SAVE RECORD
     - saveRecord()
    
     VALIDATE FIELD
     - validateField(type, name, linenum)
    
     FIELD CHANGED
     - fieldChanged(type, name, linenum)
     
     POST SOURCING
     - postSourcing(type, name)
     
     LINE INIT
     - lineInit(type)
     
     VALIDATE LINE
     - validateLine()
     
     RECALC
     - reCalc()
     
     SUB-FUNCTIONS
     - The following sub-functions are called by the above core functions in order to maintain code
     modularization:
     */
}
// END SCRIPT DESCRIPTION BLOCK  ====================================



// BEGIN SCRIPT UPDATION BLOCK  ====================================
/*

 */

// END SCRIPT UPDATION BLOCK  ====================================




// BEGIN GLOBAL VARIABLE BLOCK  =====================================
{

    //  Initialize any Global Variables, in particular, debugging variables...
	var s_type = '';


}
// END GLOBAL VARIABLE BLOCK  =======================================



// BEGIN PAGE INIT ==================================================

function pageInit_disable(type){
//alert('pageinit');
    /*  On page init:

     - PURPOSE

     FIELDS USED:

     --Field Name--				--ID--			--Line Item Name--


     */

    //  LOCAL VARIABLES


    //  PAGE INIT CODE BODY

    //if (type == 'edit')//
    {
       nlapiDisableField('entityid', true);
		////alert('Done');
    }
	s_type = type;
	 nlapiLogExecution('DEBUG', 'PAGE INIT', 's_type : ' + s_type);

}

// END PAGE INIT ====================================================

function saveRecord_validate_duplicate() //
{
    /*  On save record:
     - PURPOSE
     FIELDS USED:
     --Field Name--			--ID--		--Line Item Name--
     */
    //  LOCAL VARIABLES

    //  SAVE RECORD CODE BODY
    //alert('saveRecord_validate_duplicate');
    //alert('type : '+ s_type);

	 nlapiLogExecution('DEBUG', 'saveRecord_validate_duplicate', 's_type : ' + s_type);


   if (s_type == 'create' || s_type == 'copy') //
    {
        var s_email = nlapiGetFieldValue('email');
        nlapiLogExecution('DEBUG', 'saveRecord_validate_duplicate', 's_email : ' + s_email);
		s_email=s_email.trim()

        var s_name = nlapiGetFieldValue('companyname');
        s_name=s_name.trim()
        nlapiLogExecution('DEBUG', 'saveRecord_validate_duplicate', 's_name : ' + s_name);

        var s_url = nlapiGetFieldValue('url');
        s_url=s_url.trim()
        nlapiLogExecution('DEBUG', 'saveRecord_validate_duplicate', 's_url : ' + s_url);

        nlapiLogExecution('DEBUG', 'saveRecord_validate_duplicate', 'before   ......... s_url : ' + s_url);


        var a_zip_code = new Array();
        var a_addr_code = new Array();

        var addr_count = nlapiGetLineItemCount('addressbook');
        nlapiLogExecution('DEBUG', 'saveRecord_validate_duplicate', 'addr_count : ' + addr_count);
/*

        for (var i = 0; i < addr_count; i++) //
        {
            a_zip_code[i] = nlapiGetLineItemValue('addressbook', 'zip', parseInt(i) + 1);
			 nlapiLogExecution('DEBUG', 'saveRecord_validate_duplicate', 'a_zip_code[i] : ' + a_zip_code[i]);

			a_addr_code[i] = nlapiGetLineItemValue('addressbook', 'addr1', parseInt(i) + 1);
            nlapiLogExecution('DEBUG', 'saveRecord_validate_duplicate', 'Zip : ' + nlapiGetLineItemValue('addressbook', 'zip', parseInt(i) + 1));
            nlapiLogExecution('DEBUG', 'saveRecord_validate_duplicate', 'a_addr_code[i] : ' + a_addr_code[i]);

	   
	    }
*/

        var a_filters = new Array();
        var a_column = new Array();

		/*
if(s_email!=null && s_email!='' && s_email!=undefined)
		{
			s_email = nlapiEscapeXML(s_email)
		}
		if(s_name!=null && s_name!='' && s_name!=undefined)
		{
			s_name = nlapiEscapeXML(s_name)
		}
		if(s_url!=null && s_url!='' && s_url!=undefined)
		{
			s_url = nlapiEscapeXML(s_url)
		}
*/

		nlapiLogExecution('DEBUG', 'saveRecord_validate_duplicate', ' ************** s_email : ' + s_email+'-');
        nlapiLogExecution('DEBUG', 'saveRecord_validate_duplicate', ' ************** s_name : ' + s_name+'-');
        nlapiLogExecution('DEBUG', 'saveRecord_validate_duplicate', ' ********** s_url: ' + s_url+'-');


        //a_filters[0] = new nlobjSearchFilter('email', null, 'is', s_email);
        a_filters[0] = new nlobjSearchFilter('companyname', null, 'is', s_name);
        a_filters[1] = new nlobjSearchFilter('url', null, 'is', s_url);//contains

        a_column[0] = new nlobjSearchColumn('internalid');
        a_column[1] = new nlobjSearchColumn('entityid');
		a_column[2] = new nlobjSearchColumn('url');


        var a_searchResult = nlapiSearchRecord('customer', null, a_filters, a_column);

 	nlapiLogExecution('DEBUG', 'saveRecord_validate_duplicate', 'a_searchResult: ' + a_searchResult);
        if (a_searchResult != null && a_searchResult != undefined && a_searchResult != '')//
        {
            nlapiLogExecution('DEBUG', 'saveRecord_validate_duplicate', 'a_searchResult.length : ' + a_searchResult.length);

            for (var k = 0; k < a_searchResult.length; k++) //
            {
                var s_internalID = a_searchResult[k].getValue('internalid');
                nlapiLogExecution('DEBUG', 'saveRecord_validate_duplicate', 's_internalID : ' + s_internalID);

				var s_url_1 = a_searchResult[k].getValue('url');
                nlapiLogExecution('DEBUG', 'saveRecord_validate_duplicate', 's_url_1 : ' + s_url_1);


				if(s_url == s_url_1)
				{
					alert('The customer record is already available in system with enter email domain or customer name');
					return false;
				}


		/*
	            if (s_internalID != null && s_internalID != undefined && s_internalID != '')
				{
				var r_cust = nlapiLoadRecord('customer', s_internalID);
                nlapiLogExecution('DEBUG', 'saveRecord_validate_duplicate', 'r_cust : ' + r_cust);

                if (r_cust != null && r_cust != undefined && r_cust != '') //
                {
                    var a_zip = new Array();
					var a_addr = new Array();
                    var i_length = r_cust.getLineItemCount('addressbook');

                 
                    for (var i = 0; i < i_length; i++) //
                    {
                        a_zip[i] = r_cust.getLineItemValue('addressbook', 'zip', parseInt(i) + 1);
						a_addr[i] = nlapiGetLineItemValue('addressbook', 'addr1', parseInt(i) + 1);
                    }

                
                    for (var j = 0; j < a_zip.length; j++)//
                    {
						nlapiLogExecution('DEBUG', 'saveRecord_validate_duplicate', 'a_addr[j] : ' + a_addr[j]);
                        nlapiLogExecution('DEBUG', 'saveRecord_validate_duplicate', 'a_zip[j] : ' + a_zip[j]);

                        for (var t = 0; t < a_zip_code.length; t++) //
                        {
							nlapiLogExecution('DEBUG', 'saveRecord_validate_duplicate', 'a_addr_code[t] : ' + a_addr_code[t]);
                            nlapiLogExecution('DEBUG', 'saveRecord_validate_duplicate', 'a_zip_code[t] : ' + a_zip_code[t]);

                            if ((a_addr[j] == a_addr_code[t]))//
                            {
								if ((a_zip[j] == a_zip_code[t])) //
								{
									alert('The customer record is already available in system with enter email domain or customer name');
									return false;
								}
                            }
                        }
                    }

                }

				}
*/




            }
        }


	// ===================== Check for three letters ======================

	var i_customer_ID = nlapiGetFieldValue('entityid')
	nlapiLogExecution('DEBUG', 'saveRecord_validate_duplicate', 'Customer ID ' + i_customer_ID);

	if(i_customer_ID!=null && i_customer_ID!='' && i_customer_ID!=undefined)
	{
        var a_cust_arr = new Array();
	    a_cust_arr = i_customer_ID.split('-');
	    var s_cust_prefix_ID_text = a_cust_arr[0];
	    var i_seq_no  = a_cust_arr[1];

        nlapiLogExecution('DEBUG', 'saveRecord_validate_duplicate', 's_cust_prefix_ID_text ' + s_cust_prefix_ID_text);
        nlapiLogExecution('DEBUG', 'saveRecord_validate_duplicate', 'i_seq_no ID ' + i_seq_no);

		if(parseInt(s_cust_prefix_ID_text.toString().length) < 4)
		{
			alert(' You are not allowed to save customer record as Customer ID contains less than 4 characters .')
            return false;
		}

	// =================== Search Duplicates ========================

	   var filter = new Array();
	   filter[0] = new nlobjSearchFilter('entityid', null, 'startswith', s_cust_prefix_ID_text);
	   filter[1] = new nlobjSearchFilter('parent', null, 'isempty');

	   var columns = new Array();
	   columns[0] = new nlobjSearchColumn('internalid');
	   columns[1] = new nlobjSearchColumn('entityid');

	   var a_searchresults = nlapiSearchRecord('customer',null,filter,columns);
	   nlapiLogExecution('DEBUG', 'saveRecord_validate_duplicate', 'a_searchresults ' + a_searchresults);
	   if(a_searchresults!=null && a_searchresults!='' && a_searchresults!=undefined)
	   {
		 for(var ty=0 ;ty<a_searchresults.length;ty++)
		 {
		 	var i_customerID = a_searchresults[ty].getValue('internalid');
			nlapiLogExecution('DEBUG', 'search_sub_customers_ID', '  Customer ID -->' + i_customerID);

			var i_entityid = a_searchresults[ty].getValue('entityid');
			nlapiLogExecution('DEBUG', 'search_sub_customers_ID', '  Entity ID -->' + i_entityid);

			alert('The customer record is already available in system with enter email domain or customer name');
			return false;

		 }//Loop

	   }//Search Results

	}//Customer ID

    }


	// ===================== Check for Duplication =======================




    return true;
}

// END SAVE RECORD ==================================================

function fieldChanged_prefix(type, name, linenum)
{
	//alert("Test");
   //-----------------------------------------------------------------------------
   if (name == 'custentity_customeridprefix')
        {//1 if start
            try
            {
                var s_prefix = nlapiGetFieldValue('custentity_customeridprefix');
                s_prefix = s_prefix.toUpperCase();
                nlapiLogExecution('DEBUG', 'fieldChanged_prefix', 's_prefix : ' + s_prefix);

                for (var i = 0; i < s_prefix.length; i++)
                {
                    nlapiLogExecution('DEBUG', 'fieldChanged_prefix', 's_prefix.length : ' + s_prefix.length);
                    var alpha = '';
                    alpha = s_prefix.charAt(i);
                    nlapiLogExecution('DEBUG', 'fieldChanged_prefix', 'alpha : ' + alpha);

                    nlapiLogExecution('DEBUG', 'fieldChanged_prefix', 's_prefix.charAt(i) : ' + alpha.charCodeAt(0));
                    //nlapiLogExecution('DEBUG', 'fieldChanged_prefix', 's_prefix.charAt(i) parsing : ' + parseInt(s_prefix.charAt(i)));

                    //if (parseInt(s_prefix.charAt(i)) < 65 || parseInt(s_prefix.charAt(i)) > 90)//
                    if (alpha.charCodeAt(0) < 65 || alpha.charCodeAt(0) > 90)//
                    {
                        //alert('into invalid data loop');
                        nlapiLogExecution('DEBUG', 'fieldChanged_prefix', 'into invalid data loop');
                        //alert('Invalid prefix value !!! Please enter capital alphabets only ');
                        nlapiSetFieldValue('custentity_customeridprefix', '',false);
                        var inter_ID = document.getElementById('custentity_customeridprefix');
                        inter_ID.focus();
                        return false;
                    }
                }
                //alert('into valid data loop');
                if (s_prefix != '' && s_prefix!=null && s_prefix!=undefined)
				{
                    var filters1 = new Array();
                    var columns1 = new Array();
                    //alert('searchResult count');

                    filters1[0] = new nlobjSearchFilter('custentity_customeridprefix', null, 'is', s_prefix);
                    //alert('searchResult count 2');

                    columns1[0] = new nlobjSearchColumn('internalid');
                    columns1[1] = new nlobjSearchColumn('entityid');
                    //alert('searchResult count 3');

                    var a_searchresults = nlapiSearchRecord('customer', null, filters1, columns1);
                    //alert('searchResult count 4');
                    //alert('searchResult : ' + a_searchresults);

                    if (a_searchresults != null && a_searchresults != undefined && a_searchresults != '' )//
                    {
                        //alert('searchResult count : ' + a_searchresults.length);
                        alert('Entered value is already available in the system, please enter another value');
                        nlapiSetFieldValue('custentity_customeridprefix', '',false);
                        var inter_ID = document.getElementById('custentity_customeridprefix');
                        inter_ID.focus();
                    }
                    else //
                    {
                        //alert('Prefix is perfect.');
                        //nlapiSetFieldValue('custentity_customeridprefix', s_prefix);
                    }
                }
            }
            catch (err) //
            {
                txt = "There was an error on this page.\n\n";
                txt += "Error description: " + err.message + "\n\n";
                txt += "Click OK to continue.\n\n";
                alert(txt);
            }
        }//1 if close
        else
		{//1 else start
            if (name == 'parent') //
            {
                var s_parent = nlapiGetFieldValue('parent');
                //alert('parent ' + s_parent);

                if (s_parent != null && s_parent != '' && s_parent != undefined) //
                {
                    var r_parent = nlapiLoadRecord('customer', s_parent);
                    var s_parent_parent = r_parent.getFieldValue('parent');
                    //alert('s_parent_parent :' + s_parent_parent);
                    if (s_parent_parent != '' && s_parent_parent != undefined && s_parent_parent != null) //
                    {
                        alert('Please select Parent Customer.');
                        return false;
                    }
                }
            }
		}//1 else close
		
		//-----------------------------------------------------------------------------------
		if(name == 'autoname')
		{//2 if start
			var f_auto_name = nlapiGetFieldValue('autoname')

			if(f_auto_name == 'F')
			{
			 alert(' You are not allowed to enter Customer ID .')
			 nlapiSetFieldValue('autoname','T',false,false)
			}

		}//2 if close
		//-----------------------------------------------------------------------------------
		 if(s_type == 'create' || s_type =='copy')
		{//3 if start
		
		    if(name == 'companyname')
		    {//4 if start
			
			   var s_customer_prefix = ''
			   var s_company_name = nlapiGetFieldValue('companyname')
				s_company_name=s_company_name.trim()
				nlapiLogExecution('DEBUG', 'afterSubmit_auto_numbering', ' Company Name --> ' + s_company_name);
          
				var s_parent_customer = nlapiGetFieldValue('parent')
				nlapiLogExecution('DEBUG', 'afterSubmit_auto_numbering', ' Parent Customer --> ' + s_parent_customer);
				
					if(s_parent_customer == null || s_parent_customer == '' || s_parent_customer == undefined)
					{//5 if start
						if(s_company_name!=null && s_company_name!='' && s_company_name!=undefined)
						{//6 if start
						
						    a_split_array = s_company_name.split(' ')

							var s_first_name = a_split_array[0]

							var s_middle_name = a_split_array[1]
							if(_logValidation(s_middle_name)){
							if((s_middle_name.toString()=='OR')||(s_middle_name.toString()=='AND')||(s_middle_name.toString()=='Or')||(s_middle_name.toString()=='And')||(s_middle_name.toString()=='or')||(s_middle_name.toString()=='and'))
							{
								s_middle_name = a_split_array[2];
								var s_last_name = a_split_array[3]
							}
							else
							{
								var s_last_name = a_split_array[2]
							}
							}
							
							s_first_name = escape_special_chars(s_first_name)
							s_middle_name = escape_special_chars(s_middle_name)
							s_last_name = escape_special_chars(s_last_name)

							nlapiLogExecution('DEBUG', 'afterSubmit_auto_numbering', ' First Name --> ' + s_first_name);

							nlapiLogExecution('DEBUG', 'afterSubmit_auto_numbering', ' Middle Name --> ' + s_middle_name);

							nlapiLogExecution('DEBUG', 'afterSubmit_auto_numbering', ' Last Name --> ' + s_last_name);
							
								// ========= First Name , Middle Name & Last Name exists ===============
								
								if(s_first_name!=null && s_first_name!='' && s_first_name!=undefined)
								{
										if(s_middle_name!=null && s_middle_name!='' && s_middle_name!=undefined)
										{
											if(s_last_name!=null && s_last_name!='' && s_last_name!=undefined)
											{								
												
												if((s_first_name.toString()=='THE')||(s_first_name.toString()=='OR')||(s_first_name.toString()=='AND')||(s_first_name.toString()=='the')||(s_first_name.toString()=='The')||(s_first_name.toString()=='Or')||(s_first_name.toString()=='or')||(s_first_name.toString()=='and')||(s_first_name.toString()=='And'))
												{
													alert ("Company First Name has special characters AND THE OR \nThese are not allowed\n");
													nlapiSetFieldValue('companyname','',false)
													return false;
												}
												 if((s_middle_name.toString()=='THE')||(s_middle_name.toString()=='the')||(s_middle_name.toString()=='The'))
												{							
													alert ("Company Middle Name has special characters AND THE OR \nThese are not allowed\n");
													nlapiSetFieldValue('companyname','',false)
													return false;
												}
												
												if((s_last_name.toString()=='THE')||(s_last_name.toString()=='OR')||(s_last_name.toString()=='AND')||(s_last_name.toString()=='the')||(s_last_name.toString()=='The')||(s_last_name.toString()=='Or')||(s_last_name.toString()=='or')||(s_last_name.toString()=='and')||(s_last_name.toString()=='And'))
												{						
													 alert ("Company Last Name has special characters AND THE OR \nThese are not allowed\n");
													 nlapiSetFieldValue('companyname','',false)
													 return false;
												}						
												
												
												s_customer_prefix = s_first_name.substring(0,2)+''+s_middle_name.substring(0,1)+''+s_last_name.substring(0,1)
												s_customer_prefix = s_customer_prefix.toUpperCase()
												
												
												nlapiLogExecution('DEBUG', 'afterSubmit_auto_numbering', ' Customer Prefix --> ' + s_customer_prefix);

											}//Last Name

										}//Middle Name

								}//First Name
								
								// ========= First Name , Middle Name exists & Last Name does not exists ===============

									if(s_first_name!=null && s_first_name!='' && s_first_name!=undefined)
									{
										if(s_middle_name!=null && s_middle_name!='' && s_middle_name!=undefined)
										{
											if(s_last_name ==null || s_last_name=='' || s_last_name==undefined)
											{
												
											   if((s_first_name.toString()=='THE')||(s_first_name.toString()=='OR')||(s_first_name.toString()=='AND')||(s_first_name.toString()=='the')||(s_first_name.toString()=='The')||(s_first_name.toString()=='Or')||(s_first_name.toString()=='or')||(s_first_name.toString()=='and')||(s_first_name.toString()=='And'))
												{
													alert ("Company First Name has special characters AND THE OR \nThese are not allowed\n");
													nlapiSetFieldValue('companyname','',false)
													return false;
												}
												 if((s_middle_name.toString()=='THE')||(s_middle_name.toString()=='OR')||(s_middle_name.toString()=='AND')||(s_middle_name.toString()=='the')||(s_middle_name.toString()=='The')||(s_middle_name.toString()=='Or')||(s_middle_name.toString()=='or')||(s_middle_name.toString()=='and')||(s_middle_name.toString()=='And'))
												{							
													alert ("Company Middle Name has special characters AND THE OR \nThese are not allowed\n");
													nlapiSetFieldValue('companyname','',false)
													return false;
												}
																	
												s_customer_prefix = s_first_name.substring(0,2)+''+s_middle_name.substring(0,2)
												s_customer_prefix = s_customer_prefix.toUpperCase()
												nlapiLogExecution('DEBUG', 'afterSubmit_auto_numbering', ' Customer Prefix --> ' + s_customer_prefix);

											}//Last Name

									   }//Middle Name

									}//First Name
                                      // ========= First Name  exists &  Middle Name , Last Name does not exists ===============

										if(s_first_name!=null && s_first_name!='' && s_first_name!=undefined)
										{
											if(s_middle_name==null || s_middle_name =='' || s_middle_name ==undefined)
											{
												if(s_last_name ==null || s_last_name=='' || s_last_name==undefined)
												{								
												   if((s_first_name.toString()=='THE')||(s_first_name.toString()=='OR')||(s_first_name.toString()=='AND')||(s_first_name.toString()=='the')||(s_first_name.toString()=='The')||(s_first_name.toString()=='Or')||(s_first_name.toString()=='or')||(s_first_name.toString()=='and')||(s_first_name.toString()=='And'))
													{
														alert ("Company First Name has special characters AND THE OR \nThese are not allowed\n");
														nlapiSetFieldValue('companyname','',false)
														return false;
													}
													
																			
													s_customer_prefix = s_first_name.substring(0,4)
													s_customer_prefix = s_customer_prefix.toUpperCase()
													nlapiLogExecution('DEBUG', 'afterSubmit_auto_numbering', ' Customer Prefix --> ' + s_customer_prefix);

												}//Last Name

										   }//Middle Name

										}//First Name
										
										// =================== Search Duplicates ========================

											if(s_customer_prefix!=null && s_customer_prefix!='' && s_customer_prefix!=undefined)
											{
											   nlapiLogExecution('DEBUG', 'search_sub_customers_ID', ' Customer Prefix XXXXXXXXXX -->' + s_customer_prefix);
																
											   var filter = new Array();
											   filter[0] = new nlobjSearchFilter('entityid', null, 'startswith',s_customer_prefix);

											   var columns = new Array();
											   columns[0] = new nlobjSearchColumn('internalid');
											  
											   var a_searchresults = nlapiSearchRecord('customer',null,filter,columns);

											   if(a_searchresults!=null && a_searchresults!='' && a_searchresults!=undefined)
											   {
												f_flag = true
																		
												 if(s_first_name!=null && s_first_name!='' && s_first_name!=undefined)
												 {
														if(s_middle_name!=null && s_middle_name!='' && s_middle_name!=undefined)
														{
															//if(s_last_name!=null && s_last_name!='' && s_last_name!=undefined)
															{
																
															if((s_first_name.toString()=='THE')||(s_first_name.toString()=='OR')||(s_first_name.toString()=='AND')||(s_first_name.toString()=='the')||(s_first_name.toString()=='The')||(s_first_name.toString()=='Or')||(s_first_name.toString()=='or')||(s_first_name.toString()=='and')||(s_first_name.toString()=='And'))
															{
																alert ("Company First Name has special characters AND THE OR \nThese are not allowed\n");
																nlapiSetFieldValue('companyname','',false)
																return false;
															}
															if((s_middle_name.toString()=='THE')||(s_middle_name.toString()=='OR')||(s_middle_name.toString()=='AND')||(s_middle_name.toString()=='the')||(s_middle_name.toString()=='The')||(s_middle_name.toString()=='Or')||(s_middle_name.toString()=='or')||(s_middle_name.toString()=='and')||(s_middle_name.toString()=='And'))
															{							
																alert ("Company Middle Name has special characters AND THE OR \nThese are not allowed\n");
																nlapiSetFieldValue('companyname','',false)
																return false;
															}										
																s_customer_prefix = s_first_name.substring(0,3)+''+s_middle_name.substring(0,1)
																s_customer_prefix = s_customer_prefix.toUpperCase()
																nlapiLogExecution('DEBUG', 'afterSubmit_auto_numbering', ' Customer Prefix For Duplicates --> ' + s_customer_prefix);

															}//Last Name

													   }//Middle Name

												}//First Name

											   }//Search Results
											   else
											   {
												 f_flag = false
											   }

											}//Customer Prefix
											
											// =================== Search Sequence Number ========================

											   var filter = new Array();
											   filter[0] = new nlobjSearchFilter('entityid', null, 'startswith', s_customer_prefix);

											   var columns = new Array();
											   columns[0] = new nlobjSearchColumn('entityid');
											   columns[0].setSort()

											   var a_seq_searchresults = nlapiSearchRecord('customer',null,filter,columns);

											   if(a_seq_searchresults!=null && a_seq_searchresults!='' && a_seq_searchresults!=undefined)
											   {
												var i_customer_entity = a_seq_searchresults[0].getValue('entityid');
												nlapiLogExecution('DEBUG', 'search_sub_customers_ID', ' Entityyyyy -->' + i_customer_entity);

											   }//Search Results

											   i_current_number = 0
											   
											try
											{
												var i_new_cust_number = parseInt(i_current_number) + 1; // increment current customer id by 1
												i_current_number = i_new_cust_number;
												if (i_new_cust_number <= 9) //	Add 0 if numbers are between 0 to 9
												{
													i_new_cust_number = '0' + i_new_cust_number.toString();
												}

											   nlapiLogExecution('DEBUG', 'afterSubmit_auto_numbering', 'Customer ID -->' + i_new_cust_number);
											   nlapiLogExecution('DEBUG', 'afterSubmit_auto_numbering', 'sssssssssssssss-->' + s_customer_prefix +'-'+ i_new_cust_number);
											   nlapiSetFieldValue('entityid', s_customer_prefix +'-'+ i_new_cust_number,false)
																	
												var i_is_customer_prefix = isValid(s_customer_prefix) 
																			
												if(i_is_customer_prefix == false)
												{
													nlapiSetFieldValue('companyname','',false)
													nlapiSetFieldValue('entityid','',false)
													return false;
												}
															  
												if(s_customer_prefix.toString().length<4)
												{
													alert(' Less than four letters are not allowed in Customer Prefix ')
													nlapiSetFieldValue('entityid', '',false)
													nlapiSetFieldValue('companyname', '',false)
													return false;
												}
									
					
											}//TRY
											catch(exception)
											{
											   nlapiLogExecution('DEBUG', 'ERROR', ' Exception Caught --> ' + exception);
											}//CATCH
								
								
						
						}//6 if close
					}//5 if close
					else
				    {//5 else start
					     if (s_parent_customer != null && s_parent_customer != '' && s_parent_customer != undefined)
							{//7 if start
							
							   var o_parentOBJ = nlapiLoadRecord('customer',s_parent_customer)
							     if (o_parentOBJ != null && o_parentOBJ != '' && o_parentOBJ != undefined)
					              {//8 if start
								  
								    var i_customerID = o_parentOBJ.getFieldValue('entityid')
									nlapiLogExecution('DEBUG', 'afterSubmit_auto_numbering', 'Customer ID --> ' + i_customerID);

									var i_sub_customer_company_name =  search_sub_customers_ID(s_parent_customer)
									nlapiLogExecution('DEBUG', 'afterSubmit_auto_numbering', 'Sub Customer Company Name --> ' + i_sub_customer_company_name);
									
										//================== If Sub - Customer does not exists ==================

										if(i_sub_customer_company_name == null || i_sub_customer_company_name == '' || i_sub_customer_company_name == undefined)
										{
											i_customerID = i_customerID
											nlapiLogExecution('DEBUG', 'afterSubmit_auto_numbering', ' Prefix + Sequence Number (Parent) --> ' + i_customerID);

										}//No Sub - Customer
										else
										{
										  i_customerID = i_sub_customer_company_name
										  nlapiLogExecution('DEBUG', 'afterSubmit_auto_numbering', 'Prefix + Sequence Number (Sub - Customer) --> ' + i_customerID);

										}
										if (i_customerID != null && i_customerID != '' && i_customerID != undefined)
										{
												s_split_child_array = i_customerID.split('-')

												var s_prefix = s_split_child_array[0]
												nlapiLogExecution('DEBUG', 'afterSubmit_auto_numbering', 'Prefix --> ' + s_prefix);

												var i_sequence_number = s_split_child_array[1]
												nlapiLogExecution('DEBUG', 'afterSubmit_auto_numbering', 'Sequence Number --> ' + i_sequence_number);

												var i_new_cust_number = parseInt(i_sequence_number) + 1; // increment current customer id by 1
												i_sequence_number = i_new_cust_number;
												if (i_new_cust_number <= 9) //	Add 0 if numbers are between 0 to 9
												{
													i_new_cust_number = '0' + i_new_cust_number.toString();
												}

												nlapiLogExecution('DEBUG', 'afterSubmit_auto_numbering', 'Sub - Customer ID -->' + i_new_cust_number);
												nlapiLogExecution('DEBUG', 'afterSubmit_auto_numbering', 'Prefix -->' + s_prefix);

											   nlapiSetFieldValue('entityid', s_prefix +'-'+ i_new_cust_number)
																	   
												var i_is_customer_prefix = isValid(s_prefix) 
																		
												if(i_is_customer_prefix == false)
												{
													nlapiSetFieldValue('companyname','',false)
													nlapiSetFieldValue('entityid','',false)
													return false;
												}
											   
												if(s_prefix.toString().length<4)
												{
													alert(' Less than four letters are not allowed in Customer Prefix ')
													nlapiSetFieldValue('entityid', '',false)
													nlapiSetFieldValue('companyname', '',false)
													return false;
												}
					  
										}//Customer ID
								  
								  }// 8 if close
							}//7 if close
					}//5 else close
				
			}//4 if close
		
		}//3 if close
		
return true; 	
}
//-------------------------------------------
function postSourcing(type, name)
{

    /*  On post sourcing:

     - PURPOSE

     FIELDS USED:

     --Field Name--			--ID--		--Line Item Name--


     */

    //  LOCAL VARIABLES


    //  POST SOURCING CODE BODY

}

// END POST SOURCING ================================================



// BEGIN LINE INIT ==============================================

function lineInit(type){

    /*  On Line Init:

     - PURPOSE

     FIELDS USED:

     --Field Name--			--ID--		--Line Item Name--

     */

    //  LOCAL VARIABLES


    //  LINE INIT CODE BODY
}

// END LINE INIT ================================================


// BEGIN VALIDATE LINE ==============================================

function validateLine(type){

    /*  On validate line:
     - EXPLAIN THE PURPOSE OF THIS FUNCTION
     FIELDS USED:
     --Field Name--				--ID--
     */
    //  LOCAL VARIABLES


    //  VALIDATE LINE CODE BODY

    return true;

}

// END VALIDATE LINE ================================================





// BEGIN RECALC =====================================================

function recalc(type){

    /*  On recalc:

     - EXPLAIN THE PURPOSE OF THIS FUNCTION

     FIELDS USED:

     --Field Name--				--ID--

     */

    //  LOCAL VARIABLES

    //  RECALC CODE BODY


}
//---------------------------------------------
function search_sub_customers_ID(s_parent_customer)
{
	var i_company_name;

	 if (s_parent_customer != null && s_parent_customer != '' && s_parent_customer != undefined)
	 {
	 	   var filter = new Array();
		   filter[0] = new nlobjSearchFilter('parent', null, 'is', s_parent_customer);

		   var columns = new Array();
		   columns[0] = new nlobjSearchColumn('internalid');
		   columns[0].setSort(true);
		   columns[1] = new nlobjSearchColumn('entityid');

		   var a_searchresults = nlapiSearchRecord('customer',null,filter,columns);

		   if(a_searchresults!=null && a_searchresults!='' && a_searchresults!=undefined)
		   {
		   	nlapiLogExecution('DEBUG', 'search_sub_customers_ID', '  a_searchresults -->' + a_searchresults.length);

			if(a_searchresults.length > 1)
			{
				if(a_searchresults[1].getValue('internalid')!=null && a_searchresults[1].getValue('internalid')!='' && a_searchresults[1].getValue('internalid')!=undefined)
				{
					var i_customerID = a_searchresults[1].getValue('internalid');
					nlapiLogExecution('DEBUG', 'search_sub_customers_ID', '  Customer ID -->' + i_customerID);

					i_company_name = a_searchresults[1].getValue('entityid');
					nlapiLogExecution('DEBUG', 'search_sub_customers_ID', '  Company Name -->' + i_company_name);

				}

			}
			 else if(a_searchresults.length == 1)
			 {
			 	var i_customerID = a_searchresults[0].getValue('internalid');
				nlapiLogExecution('DEBUG', 'search_sub_customers_ID', '  Customer ID -->' + i_customerID);

				i_company_name = a_searchresults[0].getValue('entityid');
				nlapiLogExecution('DEBUG', 'search_sub_customers_ID', '  Company Name -->' + i_company_name);
			 }

		   }//Search Results

	 }//Parent Customer

	return i_company_name;

}//Sub Customer ID
//-------------------------------------
function isValid(str) 
{
  str = str.trim();
  var iChars = ".@()~`!#$%^&*+=-[]\\\';,/{}|\":<>?.";

    for (var i = 0; i < str.length; i++)
    {
       if (iChars.indexOf(str.charAt(i)) != -1)
       {
           alert ("Company Name has special characters .@()~`!#$%^&*+=-[]\\\';,/{}|\":<>?.AND THE OR \nThese are not allowed\n");
           return false;
       }
   
    }
	
    return true;
}

function escape_special_chars(text)
{
	var iChars = ".@()~`!#$%^&*+=-[]\\\';,/{}|\":<>?.";
	var text_new =''
		
	if(text!=null && text!='' && text!=undefined)
	{
		for(var y=0; y<text.toString().length; y++)
		{	
		  if(iChars.indexOf(text[y])== -1)
		  {		  
			text_new +=text[y]
		  }		
			
		}//Loop			
		
	}//Text Validation	
	
	return text_new;
}


function _logValidation(value)
{
	if (value != null && value != '' && value != undefined && value != 'undefined' && value != 'NaN') 
	{
	    return true;
	}
	else 
	{
	    return false;
	}
 }





// END RECALC =======================================================
// BEGIN FUNCTION ===================================================
// END FUNCTION =====================================================
