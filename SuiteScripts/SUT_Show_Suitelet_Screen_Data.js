/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       17 Nov 2014     Swati
 *
 */

/**
 * @param {nlobjRequest} request Request object
 * @param {nlobjResponse} response Response object
 * @returns {Void} Any output is written via response object
 */
function suitelet(request, response)
{//fun start
	 nlapiLogExecution('DEBUG','suiteletFunction','d_created_script_date-->'+d_created_script_date);
	 var i_unbilled_type =   request.getParameter('custscript_unbilled_type');
	 var i_unbilled_receivable_GL =  request.getParameter('custscript_unbilled_receivable_gl');
	 var i_unbilled_revenue_GL =  request.getParameter('custscript_unbilled_revenue_gl');
	 var i_reverse_date =  request.getParameter('custscript_reverse_date');
	 var i_criteria =  request.getParameter('custscript_provision_creation_criteria');
	 var i_user_role =  request.getParameter('custscript_user_role');
	 var i_user_subsidiary =  request.getParameter('custscript_user_subsidiary');
	 var d_created_script_date =  request.getParameter('custscript_date_created');
	 nlapiLogExecution('DEBUG','suiteletFunction','d_created_script_date-->'+d_created_script_date);
	 
	 if(_logValidation(d_created_script_date))
		{	
			 var column = new Array();
				var filters = new Array();
				filters.push(new nlobjSearchFilter('status', null, 'anyof', ['PROCESSING', 'PENDING','COMPLETE']));
						
				filters.push(new nlobjSearchFilter('internalid','script','is','250'));//250 sand box id
				
				column.push(new nlobjSearchColumn('name', 'script'));
				column.push(new nlobjSearchColumn('internalid', 'scriptdeployment'));
				column.push(new nlobjSearchColumn('datecreated'));
				column.push(new nlobjSearchColumn('status'));
				column.push(new nlobjSearchColumn('startdate'));
				column.push(new nlobjSearchColumn('enddate'));
				column.push(new nlobjSearchColumn('queue'));
				column.push(new nlobjSearchColumn('percentcomplete'));
				column.push(new nlobjSearchColumn('queueposition'));
				column.push(new nlobjSearchColumn('percentcomplete'));
		
				var a_search_results =  nlapiSearchRecord('scheduledscriptinstance', null, filters, column);
			    
						if(_logValidation(a_search_results)) 
					    {
					    	nlapiLogExecution('DEBUG','suiteletFunction','Search Results Length -->'+a_search_results.length);
					    	
							for(var i=0;i<a_search_results.length;i++)
					    	{
					    		var s_script  = a_search_results[i].getValue('name', 'script');
								//nlapiLogExecution('DEBUG','suiteletFunction','Script -->'+s_script);
				
								d_date_created  = a_search_results[i].getValue('datecreated');
								nlapiLogExecution('DEBUG','suiteletFunction','Date Created -->'+d_date_created);
				    
					           	var i_percent_complete  = a_search_results[i].getValue('percentcomplete');
								nlapiLogExecution('DEBUG','suiteletFunction','Percent Complete -->'+i_percent_complete);
				
								s_script_status  = a_search_results[i].getValue('status');
								nlapiLogExecution('DEBUG','suiteletFunction','Script Status -->'+s_script_status);
							
							    i_percent_complete = i_percent_complete;              
							
					    	}
					    }
		
		}
	 
	 var f_form = nlapiCreateForm(" UNBILLED EXPENSE / TIME LIST");
	 
	 
	 if(i_percent_complete =='' || i_percent_complete == null || i_percent_complete == undefined)
	 {
	 	s_script_status = 'Not Started'
		i_percent_complete = '0%'
	 }
	 
	 
					 
	var s_status_f = f_form.addField('custpage_status_script', 'text', 'Status').setDisplayType('inline');
	s_status_f.setDefaultValue(s_script_status)
	
	var s_d_date_created_f = f_form.addField('custpage_d_date_created', 'text', 'Date Created').setDisplayType('inline');
	s_d_date_created_f.setDefaultValue(d_date_created)
	
				
	var i_percent_complete_f = f_form.addField('custpage_percent_complete', 'text', 'Percent Complete').setDisplayType('inline');
	i_percent_complete_f.setDefaultValue(i_percent_complete)		 
					
	//------------------------------------------------------------------
	 if(s_script_status == 'Complete')
		   {
			   var i_time_provision_f = f_form.addField('custpage_time_provision', 'textarea', 'Time Provision Records Created ').setDisplayType('inline');
			   
			  
				
			   
		     // var i_deleteID = nlapiDeleteRecord('customrecord_provision_processed_records', i_custom_id);
	          //nlapiLogExecution('DEBUG', 'schedulerFunction',' ************ Delete Custom rec ID  ******** -->' + i_deleteID);
			if(i_criteria == 1 || i_criteria == 2)
				{
				   //var formSavedLabel = form.addField('custpage_time_provision_link', 'inlinehtml');
					//formSavedLabel.setDefaultValue("<html><body>Click <a href='https://system.sandbox.netsuite.com/app/common/custom/custrecordentrylist.nl?rectype=172'>here</a> to go to Time Bill Record.</body></html>");
			 
					//leadSourceField.setDisplayType('inline');
					
				 i_time_provision_f.setDefaultValue('https://system.sandbox.netsuite.com/app/common/custom/custrecordentrylist.nl?rectype=155')
				}
			else
				{
		   	
		     i_time_provision_f.setDefaultValue('https://system.sandbox.netsuite.com/app/common/custom/custrecordentrylist.nl?rectype=172')
				}
			
		   }
	
	//------------------------------------------------------------
	f_form.setScript('customscriptcli_time_bill_suitelet_ref_p');
	f_form.addSubmitButton('SUBMIT');
	f_form.addButton('custpage_refresh','REFRESH','refresh()');
	response.writePage(f_form);

}//fun close
