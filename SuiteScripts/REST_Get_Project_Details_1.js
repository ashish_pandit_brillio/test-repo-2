/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       08 Apr 2019     Aazamali Khan
 *
 */

/**
 * @param {Object} dataIn Parameter object
 * @returns {Object} Output object
 */
var oppDetailsArray = [];
var fufillmentDetailsArray = [];
var revenueArray = [];

function postRESTlet(dataIn) {
    try {
        var oppId = dataIn.oppId;
      nlapiLogExecution('Debug','oppId ',oppId);
        if (oppId) {
            var oppName = nlapiLookupField("customrecord_sfdc_opportunity_record", oppId, "custrecord_opportunity_id_sfdc");
            getOpportunityDetails(oppId); // Add opportunity record details into temp Array
            getRevenueDetails(oppName);
            return {
                "dataOut": {
                    "opportunityDetails": oppDetailsArray,
                    "fufillmentDetails": fufillmentDetailsArray,
                    "revenueDetails": revenueArray
                }
            };
        } else {
            return {
                "dataOut": {}
            }
        }
    } catch (e) {
        return {
            "code": "error",
            "message": e
        }
    }
}

function getOpportunityDetails(oppId) {
    var customrecord_sfdc_opportunity_recordSearch = nlapiSearchRecord("customrecord_sfdc_opportunity_record", null,
        [
            ["internalidnumber", "equalto", oppId]
        ],
        [
            new nlobjSearchColumn("custrecord_opportunity_id_sfdc"),
            new nlobjSearchColumn("custrecord_opportunity_name_sfdc"),
            new nlobjSearchColumn("custrecord_project_sfdc"),
            new nlobjSearchColumn("custrecord_customer_sfdc"),
            new nlobjSearchColumn("custrecord_start_date_sfdc"),
            new nlobjSearchColumn("custrecord_end_date_sfdc"),
            new nlobjSearchColumn("custrecord_practice_internal_id_sfdc"),
            new nlobjSearchColumn("custrecord_location_sfdc"),
            new nlobjSearchColumn("custrecord_sales_act_sfd"),
            new nlobjSearchColumn("custrecord_account_type_sfdc"),
            new nlobjSearchColumn("custrecord_opp_account_region"),
            new nlobjSearchColumn("custrecord_opp_service_line"),
            new nlobjSearchColumn("custrecord_account_name_sfdc"),
            new nlobjSearchColumn("custrecord_opp_fulfilment_status_sfdc"),
            new nlobjSearchColumn("custrecord_status_sfdc"),
            new nlobjSearchColumn("custrecord_projection_status_sfdc"),
            new nlobjSearchColumn("custrecord_stage_sfdc"),
            new nlobjSearchColumn("custrecord_tcv_sfdc"),
            new nlobjSearchColumn("custrecord_fulfill_plan_position", "CUSTRECORD_FULFILL_PLAN_OPP_ID", null),
            new nlobjSearchColumn("custrecord_fulfill_plan_practice", "CUSTRECORD_FULFILL_PLAN_OPP_ID", null),
            new nlobjSearchColumn("custrecord_fulfill_plan_skill", "CUSTRECORD_FULFILL_PLAN_OPP_ID", null),
            new nlobjSearchColumn("custrecord_fulfill_plan_opp_loc", "CUSTRECORD_FULFILL_PLAN_OPP_ID", null)
        ]
    );
    if (customrecord_sfdc_opportunity_recordSearch) {
        var jsonObject = {};
        var positions = 0;
        jsonObject.opportunityName = customrecord_sfdc_opportunity_recordSearch[0].getValue("custrecord_opportunity_name_sfdc");
        if (customrecord_sfdc_opportunity_recordSearch[0].getValue("custrecord_practice_internal_id_sfdc")) {
            jsonObject.parent = false;
        } else {
            jsonObject.parent = true;
        }
        jsonObject.practice = customrecord_sfdc_opportunity_recordSearch[0].getText("custrecord_practice_internal_id_sfdc");
        jsonObject.tcv = customrecord_sfdc_opportunity_recordSearch[0].getValue("custrecord_tcv_sfdc");
        jsonObject.startdate = customrecord_sfdc_opportunity_recordSearch[0].getValue("custrecord_start_date_sfdc");
        jsonObject.enddate = customrecord_sfdc_opportunity_recordSearch[0].getValue("custrecord_start_date_sfdc");
        jsonObject.status = customrecord_sfdc_opportunity_recordSearch[0].getText("custrecord_projection_status_sfdc");
        jsonObject.stage = customrecord_sfdc_opportunity_recordSearch[0].getText("custrecord_stage_sfdc");
        for (var i = 0; i < customrecord_sfdc_opportunity_recordSearch.length; i++) {
            var fulfillJSON = {};
            positions = positions + parseInt(customrecord_sfdc_opportunity_recordSearch[i].getValue("custrecord_fulfill_plan_position", "CUSTRECORD_FULFILL_PLAN_OPP_ID", null));
            fulfillJSON.practice = customrecord_sfdc_opportunity_recordSearch[i].getValue("custrecord_fulfill_plan_practice", "CUSTRECORD_FULFILL_PLAN_OPP_ID", null);
            fulfillJSON.positions = customrecord_sfdc_opportunity_recordSearch[i].getValue("custrecord_fulfill_plan_position", "CUSTRECORD_FULFILL_PLAN_OPP_ID", null);
            fulfillJSON.skills = customrecord_sfdc_opportunity_recordSearch[i].getValue("custrecord_fulfill_plan_skill", "CUSTRECORD_FULFILL_PLAN_OPP_ID", null);
            fulfillJSON.startdate = jsonObject.startdate;
            fulfillJSON.endddate = jsonObject.enddate;
            fulfillJSON.location = customrecord_sfdc_opportunity_recordSearch[i].getValue("custrecord_fulfill_plan_opp_loc", "CUSTRECORD_FULFILL_PLAN_OPP_ID", null);
			if(fulfillJSON.practice){
          	fufillmentDetailsArray.push(fulfillJSON);   
			}  
        }
        jsonObject.positions = positions ? positions : "0" ;
        oppDetailsArray.push(jsonObject);
    }
}

function getRevenueDetails(oppName) {
    var customrecord_fuel_sfdc_revenue_dataSearch = nlapiSearchRecord("customrecord_fuel_sfdc_revenue_data", null,
        [
            ["custrecord_sfdc_revenue_data_oppid", "is", oppName]
        ],
        [
            new nlobjSearchColumn("scriptid").setSort(false),
            new nlobjSearchColumn("custrecord_sfdc_revenue_data_stage"),
            new nlobjSearchColumn("custrecord_sfdc_revenue_data_sowid"),
            new nlobjSearchColumn("custrecord_sfdc_revenue_data_startdate"),
            new nlobjSearchColumn("custrecord_sfdc_revenue_data_prostatus"),
            new nlobjSearchColumn("custrecord_sfdc_revenue_data_projectid"),
            new nlobjSearchColumn("custrecord_sfdc_revenue_data_enddate"),
            new nlobjSearchColumn("custrecord_sfdc_revenue_data_oppid"),
            new nlobjSearchColumn("custrecord_sfdc_revenue_data_netcustid"),
            new nlobjSearchColumn("custrecord_sfdc_revenue_data_accregion"),
            new nlobjSearchColumn("custrecord_sfdc_revenue_data_accname"),
            new nlobjSearchColumn("custrecord_sfdc_fuel_forecast_rev_moyear", "CUSTRECORD_SFDC_FUEL_FORECAST_REV_EXT", null),
            new nlobjSearchColumn("custrecord_sfdc_fuel_forecast_rev_amount", "CUSTRECORD_SFDC_FUEL_FORECAST_REV_EXT", null),
            new nlobjSearchColumn("custrecord_sfdc_fuel_forecast_rev_pract", "CUSTRECORD_SFDC_FUEL_FORECAST_REV_EXT", null),
			new nlobjSearchColumn("custrecord_sfdc_fuel_fore_modayyear", "CUSTRECORD_SFDC_FUEL_FORECAST_REV_EXT", null)
        ]
    );
    if (customrecord_fuel_sfdc_revenue_dataSearch) {
        for (var int = 0; int < customrecord_fuel_sfdc_revenue_dataSearch.length; int++) {
            var jsonObject = {};
            jsonObject.monthyear = customrecord_fuel_sfdc_revenue_dataSearch[int].getValue("custrecord_sfdc_fuel_forecast_rev_moyear", "CUSTRECORD_SFDC_FUEL_FORECAST_REV_EXT", null);
            jsonObject.monthyeardateformat = customrecord_fuel_sfdc_revenue_dataSearch[int].getValue("custrecord_sfdc_fuel_fore_modayyear", "CUSTRECORD_SFDC_FUEL_FORECAST_REV_EXT", null);
            jsonObject.practice = customrecord_fuel_sfdc_revenue_dataSearch[int].getText("custrecord_sfdc_fuel_forecast_rev_pract", "CUSTRECORD_SFDC_FUEL_FORECAST_REV_EXT", null);
            jsonObject.amount = customrecord_fuel_sfdc_revenue_dataSearch[int].getValue("custrecord_sfdc_fuel_forecast_rev_amount", "CUSTRECORD_SFDC_FUEL_FORECAST_REV_EXT", null);
            revenueArray.push(jsonObject);
        }
    }
}