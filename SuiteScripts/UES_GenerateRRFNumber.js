function afterSubmitSetRRFNumber(type)
{
	if(type=='create' || type == 'edit')
	{
		try
		{
			var recId = nlapiGetRecordId();
			var recType = nlapiGetRecordType();
			var recordObject = nlapiLoadRecord(recType,recId);
			if(recId)
			{
				var frfNumber = recordObject.getFieldValue('custrecord_taleo_ext_hire_frf_details');
				var taleoId = nlapiLookupField("customrecord_frf_details",frfNumber,"custrecord_rrf_taleo");
              	if(taleoId){
                  nlapiSubmitField('customrecord_taleo_external_hire',recId,'custrecord_taleo_external_rrf_number',taleoId);
                }else{
                 	var submitId = nlapiSubmitField('customrecord_taleo_external_hire',recId,'custrecord_taleo_external_rrf_number',getFRFNumber(frfNumber)); 
                }
			}
		}
		catch(error)
		{
			nlapiLogExecution('Debug','Error ',error);
		}
		
	}
}

function getFRFNumber(recId) {
	var str = "" + recId;
	var pad = "00000000";
	var s_number = pad.substring(0, pad.length - str.length) + str;
	return "R"+s_number;
}