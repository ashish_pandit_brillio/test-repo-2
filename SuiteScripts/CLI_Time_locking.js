// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
/*
   	Script Name:	CLI_Time_locking
	Author:			Vikrant
	Company:		Aashna	
	Date:			02-09-2014
    Description:	Employee of certain projects will not allow to enter time before September 2014


	Script Modification Log:

	-- Date --			-- Modified By --				--Requested By--				-- Description --




	Below is a summary of the process controls enforced by this script file.  The control logic is described
	more fully, below, in the appropriate function headers and code blocks.

     PAGE INIT
		- pageInit_CLI_Time_locking(type)


     SAVE RECORD
		- saveRecord()


     VALIDATE FIELD
		- validateField(type, name, linenum)


     FIELD CHANGED
		- fieldChanged(type, name, linenum)


     POST SOURCING
		- postSourcing(type, name)


	LINE INIT
		- lineInit(type)


     VALIDATE LINE
		- validateLine()


     RECALC
		- reCalc()


     SUB-FUNCTIONS
		- The following sub-functions are called by the above core functions in order to maintain code
            modularization:





*/
}
// END SCRIPT DESCRIPTION BLOCK  ====================================



// BEGIN SCRIPT UPDATION BLOCK  ====================================
/*


*/
// END SCRIPT UPDATION BLOCK  ====================================




// BEGIN GLOBAL VARIABLE BLOCK  =====================================
{

	//  Initialize any Global Variables, in particular, debugging variables...


}
// END GLOBAL VARIABLE BLOCK  =======================================





// BEGIN PAGE INIT ==================================================

function pageInit_CLI_Time_locking(type) //
{
	/*  On page init:
	 - PURPOSE
	 FIELDS USED:
	 --Field Name--				--ID--			--Line Item Name--
	 */
	//  LOCAL VARIABLES
	
	
	//  PAGE INIT CODE BODY
	
	var weekly = nlapiGetFieldValue('weekly');
	//nlapiLogExecution('DEBUG', 'saveRecord_val_st_hrs', 'weekly : ' + weekly);
	
	
}

// END PAGE INIT ====================================================





// BEGIN SAVE RECORD ================================================

function saveRecord() //
{
	/*  On save record:
	 - PURPOSE
	 FIELDS USED:
	 --Field Name--			--ID--		--Line Item Name--
	 */
	//  LOCAL VARIABLES
	
	
	
	//  SAVE RECORD CODE BODY
	
}

// END SAVE RECORD ==================================================








// BEGIN VALIDATE FIELD =============================================

function fieldChanged(type, name, linenum)
{

	/*  On validate field:

          - EXPLAIN THE PURPOSE OF THIS FUNCTION


          FIELDS USED:

          --Field Name--				--ID--


    */


	//  LOCAL VARIABLES



	//  VALIDATE FIELD CODE BODY


	return true;

}

// END VALIDATE FIELD ===============================================




var g_hrs = 0;
// BEGIN FIELD CHANGED ==============================================

function fld_val__CLI_Time_locking(type, name, linenum)//
{
	/*  On field changed:
	 - PURPOSE
	 FIELDS USED:
	 --Field Name--				--ID--
	 */
	//  LOCAL VARIABLES
	
	if (name == 'customer' || name == 'trandate') //
	{
		var a = new Array();
		a['User-Agent-x'] = 'SuiteScript-Call';
		var resposeObject = '';
		
		var weekly = nlapiGetFieldValue('weekly');
		//nlapiLogExecution('DEBUG', 'saveRecord_val_st_hrs', 'weekly : ' + weekly);
		
		if (weekly == 'T') // If it is weekly time sheet form
		{
			var s_project_id = '';
			s_project_id = nlapiGetCurrentLineItemValue('timeitem', 'customer'); //nlapiGetFieldValue('customer');
			
			var d_date = nlapiGetFieldValue('trandate');
			
			if(!_validate(d_date)) //
			{
				return true;
			}
			
			if(!_validate(s_project_id)) //
			{
				return true;
			}
			
			d_date = nlapiStringToDate(d_date);
			nlapiLogExecution('DEBUG', 'fld_val__CLI_Time_locking', 'd_date : ' + d_date);
			
			var month = d_date.getMonth();
			nlapiLogExecution('DEBUG', 'pageInit_CLI_Time_locking', 'month : ' + month);
			
			var year = d_date.getFullYear();
			nlapiLogExecution('DEBUG', 'pageInit_CLI_Time_locking', 'month : ' + month);
			
			if (month < 8 && year == '2014') // if date selected is before September 2014
			{
				
				if(month < 6) // if month is less than July
				{
					/*nlapiDisableLineItemField('timeitem', 'customer', true);
					nlapiDisableLineItemField('timeitem', 'casetaskevent', true);
					
					nlapiDisableLineItemField('timeitem', 'hour0', true);
					nlapiDisableLineItemField('timeitem', 'hour1', true);
					nlapiDisableLineItemField('timeitem', 'hour2', true);
					nlapiDisableLineItemField('timeitem', 'hour3', true);
					nlapiDisableLineItemField('timeitem', 'hour4', true);
					nlapiDisableLineItemField('timeitem', 'hour5', true);
					nlapiDisableLineItemField('timeitem', 'hour6', true);*/
					
					nlapiSetCurrentLineItemValue('timeitem', 'customer', '', false);
					nlapiSetFieldValue('trandate', '', false, false);
					//nlapiSetCurrentLineItemValue('timeitem', 'customer', '', false);
					alert('Please do not enter time prior to July 2014 !!!');
					return false;
				}
				
				var resposeObject = nlapiRequestURL('/app/site/hosting/scriptlet.nl?script=289&deploy=1&custscript_pro_id=' + s_project_id +
				'&custscript_req_type=customer_id', null, a);
				
				s_project_id = resposeObject.getBody();
				nlapiLogExecution('DEBUG', 'fld_val__CLI_Time_locking', 's_project_id : ' + s_project_id);
				
				if (s_project_id == '4223') // if it is a Collabera customer time sheet
				{
					/*nlapiDisableLineItemField('timeitem', 'customer', true);
					nlapiDisableLineItemField('timeitem', 'casetaskevent', true);
					
					nlapiDisableLineItemField('timeitem', 'hour0', true);
					nlapiDisableLineItemField('timeitem', 'hour1', true);
					nlapiDisableLineItemField('timeitem', 'hour2', true);
					nlapiDisableLineItemField('timeitem', 'hour3', true);
					nlapiDisableLineItemField('timeitem', 'hour4', true);
					nlapiDisableLineItemField('timeitem', 'hour5', true);
					nlapiDisableLineItemField('timeitem', 'hour6', true);*/
					
					nlapiSetCurrentLineItemValue('timeitem', 'customer', '', false);
					nlapiSetFieldValue('trandate', '', false, false);
					//nlapiSetCurrentLineItemValue('timeitem', 'customer', '', false);
					alert('Please do not enter time for this project before 1st Sep 2014 !!!');
					return false;
				}
			}
			else //
			{
				/*nlapiDisableLineItemField('timeitem', 'customer', false);
				nlapiDisableLineItemField('timeitem', 'casetaskevent', false);
				
				nlapiDisableLineItemField('timeitem', 'hour0', false);
				nlapiDisableLineItemField('timeitem', 'hour1', false);
				nlapiDisableLineItemField('timeitem', 'hour2', false);
				nlapiDisableLineItemField('timeitem', 'hour3', false);
				nlapiDisableLineItemField('timeitem', 'hour4', false);
				nlapiDisableLineItemField('timeitem', 'hour5', false);
				nlapiDisableLineItemField('timeitem', 'hour6', false);*/
			}
		}
	}
	
	if (name == 'customer' || name == 'trandate') //
	{
		var weekly = nlapiGetFieldValue('weekly');
		//nlapiLogExecution('DEBUG', 'saveRecord_val_st_hrs', 'weekly : ' + weekly);
		
		if (weekly != 'T') // Means it is not a weekly time sheet form
		{
			var s_project_id = '';
			var d_date = '';
			d_date = nlapiGetFieldValue('trandate');
			//nlapiLogExecution('DEBUG', 'pageInit_CLI_Time_locking', '1 d_date : ' + d_date);
			
			s_project_id = nlapiGetFieldValue('customer');
			
			if (!_validate(s_project_id)) //
			{
				return true;
			}
			
			if (!_validate(d_date)) //
			{
				return true;
			}
			
			d_date = nlapiStringToDate(d_date);
			nlapiLogExecution('DEBUG', 'fld_val__CLI_Time_locking', 'd_date : ' + d_date);
			
			var month = d_date.getMonth();
			nlapiLogExecution('DEBUG', 'pageInit_CLI_Time_locking', 'month : ' + month);
			
			var year = d_date.getFullYear();
			nlapiLogExecution('DEBUG', 'pageInit_CLI_Time_locking', 'month : ' + month);
			
			if (month < 8 && year == '2014') // month is less than or equal to September
			{
				if(month < 6) // if month is less than July
				{
					/*nlapiDisableField('hours', true);
					nlapiDisableField('customer', true);
					nlapiDisableField('casetaskevent', true);
					nlapiDisableField('employee', true);
					nlapiDisableField('trandate', true);
					nlapiDisableField('customform', true);*/
					
					nlapiSetFieldValue('trandate', '', false, false);
					nlapiSetFieldValue('customer', '', false, false);
					//nlapiSetFieldValue('hours', '', false, false);
					
					alert('Please do not enter time prior to July 2014 !!!');
					return false;
				}
				
				var a = new Array();
				a['User-Agent-x'] = 'SuiteScript-Call';
				var resposeObject = '';
				
				var resposeObject = nlapiRequestURL('/app/site/hosting/scriptlet.nl?script=289&deploy=1&custscript_pro_id=' + s_project_id +
				'&custscript_req_type=customer_id', null, a);
				s_project_id = resposeObject.getBody();
				nlapiLogExecution('DEBUG', 'fld_val__CLI_Time_locking', 's_project_id : ' + s_project_id);
				
				if (s_project_id == '4223') // if it is a Collabera customer time sheet
				{
					/*nlapiDisableField('hours', true);
					nlapiDisableField('customer', true);
					nlapiDisableField('casetaskevent', true);
					nlapiDisableField('employee', true);
					nlapiDisableField('trandate', true);
					nlapiDisableField('customform', true);*/
					
					nlapiSetFieldValue('trandate', '', false, false);
					nlapiSetFieldValue('customer', '', false, false);
					//nlapiSetFieldValue('hours', '', false, false);
					
					alert('Please do not enter time for this project before 1st Sep 2014 !!!');
					return false;
				}
			}
			else //
			{
				/*nlapiDisableField('hours', false);
				nlapiDisableField('customer', false);
				nlapiDisableField('casetaskevent', false);
				nlapiDisableField('employee', false);
				nlapiDisableField('trandate', false);
				nlapiDisableField('customform', false);*/
			}
		//nlapiLogExecution('DEBUG', 'saveRecord_val_st_hrs', 'Execution stops as it is a weekly timesheet');
		//return true;
		}
		else // if it is not weekly time sheet form
		{
		
		}
	}
	
	return true;
	
	
	//  FIELD CHANGED CODE BODY

	//alert('Field Name='+name+'\nLine Item Type='+type+'\nField Value='+nlapiGetFieldValue(name)+'\nLine Item Value='+nlapiGetLineItemValue(type, name, 1));

}

// END FIELD CHANGED ================================================





// BEGIN POST SOURCING ==============================================

function postSourcing(type, name)
{

    /*  On post sourcing:

	- PURPOSE

          FIELDS USED:

		--Field Name--			--ID--		--Line Item Name--


    */

    //  LOCAL VARIABLES


    //  POST SOURCING CODE BODY



}

// END POST SOURCING ================================================



// BEGIN LINE INIT ==============================================

function lineInit(type)
{

    /*  On Line Init:

	- PURPOSE

          FIELDS USED:

		--Field Name--			--ID--		--Line Item Name--


    */

    //  LOCAL VARIABLES


    //  LINE INIT CODE BODY



}

// END LINE INIT ================================================


// BEGIN VALIDATE LINE ==============================================

function validateLine(type)
{

	/*  On validate line:

          - EXPLAIN THE PURPOSE OF THIS FUNCTION


          FIELDS USED:

          --Field Name--				--ID--


    */


	//  LOCAL VARIABLES


	//  VALIDATE LINE CODE BODY


	return true;

}

// END VALIDATE LINE ================================================





// BEGIN RECALC =====================================================

function recalc(type)
{

	/*  On recalc:

          - EXPLAIN THE PURPOSE OF THIS FUNCTION


          FIELDS USED:

          --Field Name--				--ID--


    */


	//  LOCAL VARIABLES


	//  RECALC CODE BODY


}

// END RECALC =======================================================





// BEGIN FUNCTION ===================================================

function get_ST_Task_ID(projectID) // search for ST task in project task list
{
	var filters = new Array();
	filters[filters.length] = new nlobjSearchFilter('internalid', 'job', 'anyof', projectID);
	
	// In case of any issue, please take a look at saved search "customsearch_task_search_for_st_task"
	// And confirm that Task names of "OT", "Leave" and "Holiday" has not been change in NetSuite 
	// You can check this by analyzing result if the saved search
	
	var search_result = nlapiSearchRecord('projecttask', 'customsearch_task_search_for_st_task', filters, null);
	
	if (_validate(search_result)) //
	{
		//nlapiLogExecution('DEBUG', 'get_Task_ID', 'search_result.length : ' + search_result.length);
		
		for (var i = 0; i < search_result.length; i++) //
		{
			var result = search_result[i];
			var all_columns = result.getAllColumns();
			
			var st_task_ID = result.getValue(all_columns[0]);
            //nlapiLogExecution('DEBUG', 'get_Task_ID', 'st_task_ID : ' + st_task_ID);
			
			return st_task_ID;
		}
	}
	return null;
}

function get_Holiday_Task_ID(projectID) // search for Holiday task in project task list
{
	var filters = new Array();
	filters[filters.length] = new nlobjSearchFilter('internalid', 'job', 'anyof', projectID);
	
	// In case of any issue, please take a look at saved search "customsearch_task_search_holiday_task"
	// And confirm that Task names of "Holiday" has not been change in NetSuite 
	// You can check this by analyzing result if the saved search
	
	var search_result = nlapiSearchRecord('projecttask', 'customsearch_task_search_holiday_task', filters, null);
	
	if (_validate(search_result)) //
	{
		//nlapiLogExecution('DEBUG', 'get_Holiday_Task_ID', 'search_result.length : ' + search_result.length);
		
		for (var i = 0; i < search_result.length; i++) //
		{
			var result = search_result[i];
			var all_columns = result.getAllColumns();
			
			var hday_task_ID = result.getValue(all_columns[0]);
            //nlapiLogExecution('DEBUG', 'get_Holiday_Task_ID', 'hday_task_ID : ' + hday_task_ID);
			
			return hday_task_ID;
		}
	}
	return null;
}

function get_FH_Task_ID(projectID) // search for Holiday task in project task list
{
	var filters = new Array();
	filters[filters.length] = new nlobjSearchFilter('internalid', 'job', 'anyof', projectID);
	
	// In case of any issue, please take a look at saved search "customsearch_task_search_for_fh_task"
	// And confirm that Task names of "Floating Holiday" has not been change in NetSuite 
	// You can check this by analyzing result if the saved search
	
	var search_result = nlapiSearchRecord('projecttask', 'customsearch_task_search_for_fh_task', filters, null);
	
	if (_validate(search_result)) //
	{
		//nlapiLogExecution('DEBUG', 'get_FH_Task_ID', 'search_result.length : ' + search_result.length);
		
		for (var i = 0; i < search_result.length; i++) //
		{
			var result = search_result[i];
			var all_columns = result.getAllColumns();
			
			var fh_task_ID = result.getValue(all_columns[0]);
            //nlapiLogExecution('DEBUG', 'get_FH_Task_ID', 'fh_task_ID : ' + fh_task_ID);
			
			return fh_task_ID;
		}
	}
	return null;
}

function get_Leave_Task_ID(projectID) // search for Holiday task in project task list
{
	var filters = new Array();
	filters[filters.length] = new nlobjSearchFilter('internalid', 'job', 'anyof', projectID);
	
	// In case of any issue, please take a look at saved search "customsearch_task_search_for_leave_task"
	// And confirm that Task names of "Leave" has not been change in NetSuite 
	// You can check this by analyzing result if the saved search
	
	var search_result = nlapiSearchRecord('projecttask', 'customsearch_task_search_for_leave_task', filters, null);
	
	if (_validate(search_result)) //
	{
		//nlapiLogExecution('DEBUG', 'get_Leave_Task_ID', 'search_result.length : ' + search_result.length);
		
		for (var i = 0; i < search_result.length; i++) //
		{
			var result = search_result[i];
			var all_columns = result.getAllColumns();
			
			var leave_task_ID = result.getValue(all_columns[0]);
            //nlapiLogExecution('DEBUG', 'get_Leave_Task_ID', 'leave_task_ID : ' + leave_task_ID);
			
			return leave_task_ID;
		}
	}
	return null;
}

function get_OT_Task_ID(projectID) // search for Holiday task in project task list
{
	var filters = new Array();
	filters[filters.length] = new nlobjSearchFilter('internalid', 'job', 'anyof', projectID);
	
	// In case of any issue, please take a look at saved search "customsearch_task_search_for_ot_task"
	// And confirm that Task names of "Leave" has not been change in NetSuite 
	// You can check this by analyzing result if the saved search
	
	var search_result = nlapiSearchRecord('projecttask', 'customsearch_task_search_for_ot_task', filters, null);
	
	if (_validate(search_result)) //
	{
		//nlapiLogExecution('DEBUG', 'get_Leave_Task_ID', 'search_result.length : ' + search_result.length);
		
		for (var i = 0; i < search_result.length; i++) //
		{
			var result = search_result[i];
			var all_columns = result.getAllColumns();
			
			var leave_task_ID = result.getValue(all_columns[0]);
            //nlapiLogExecution('DEBUG', 'get_Leave_Task_ID', 'leave_task_ID : ' + leave_task_ID);
			
			return leave_task_ID;
		}
	}
	return null;
}


function _validate(obj) //
{
	if(obj != null && obj != '' && obj != 'undefined' && obj != undefined)//
	{
		return true;
	}
	else //
	{
		return false;
	}
}

function _correct_time(t_time) //
{
	// function is used to correct the time 
	
	if(_validate(t_time))
	{
		//nlapiLogExecution('DEBUG', '_correct_time', 't_time : ' + t_time);
		
		var hrs = t_time.split(':')[0];	 	//nlapiLogExecution('DEBUG', '_correct_time', 'hrs : ' + hrs);
		var mins = t_time.split(':')[1];	//nlapiLogExecution('DEBUG', '_correct_time', 'mins : ' + mins);
		
		mins = parseFloat(mins) / parseFloat('60');  //nlapiLogExecution('DEBUG', '_correct_time', 'after correct mins : ' + mins);
		
		hrs = parseFloat(hrs) + parseFloat(mins);	//nlapiLogExecution('DEBUG', '_correct_time', 'after adding mins to hrs : ' + hrs);	
		
		return hrs;
	}
	else
	{
        //nlapiLogExecution('DEBUG', '_correct_time', 't_time is invalid : ' + t_time);
	}
	
}


// END FUNCTION =====================================================
