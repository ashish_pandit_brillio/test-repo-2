/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       04 Sep 2014     Swati
 *
 */

/**
 * @param {nlobjRequest} request Request object
 * @param {nlobjResponse} response Response object
 * @returns {Void} Any output is written via response object
 */
function suitelet_Status(request, response)
{
try
{
	
	nlapiLogExecution('DEBUG','working');
	var form=nlapiCreateForm("Journal Entry Creation Status");
	var sts=form.addField('custtxt1','text','Status').setDisplayType('disabled');
	var per=form.addField('custtxt2','text','Percentage Completed').setDisplayType('disabled');
	
	var status_rec_create=nlapiLoadRecord('customrecord_schedule_script_status','1');
	var s_Status=status_rec_create.getFieldValue('custrecord_status');
	var exception=status_rec_create.getFieldValue('custrecord_error');
	if(s_Status == 'Fail')
		{
	    var exe=form.addField('custtxt3','textarea','Exception').setDisplayType('disabled');
		sts.setDefaultValue(s_Status);
		per.setDefaultValue('0.0%');
		exe.setDefaultValue(exception);;
		status_rec_create.setFieldValue('custrecord_status','test');
		status_rec_create.setFieldValue('custrecord_error','');
		nlapiSubmitRecord(status_rec_create);
		var createNewReqLink = form.addField('custpage_new_req_link','inlinehtml', null, null,null);
		var linkURL = nlapiResolveURL('SUITELET', 'customscript_fixed_bid_project_screen','customdeploy1', null);
	 
		//Create a link to launch the Suitelet.
		createNewReqLink.setDefaultValue('<B>Click <A HREF="' + linkURL +'">Here</A>To Go Home Page.</B>');
		}
	else
		{
				//-------------------------------------------------------------------------
				
				var a_search_results =  nlapiSearchRecord('scheduledscriptinstance', 569, null,null);
				//nlapiLogExecution('DEBUG','suiteletFunction','Search Results Length -->'+a_search_results.length);
				
				if(_logValidation(a_search_results)) 
			    {
			    	nlapiLogExecution('DEBUG','suiteletFunction','Search Results Length -->'+a_search_results.length);
			    	
					for(var i=0;i<a_search_results.length;i++)
			    	{
						var result=a_search_results[i];
						var All_Column=result.getAllColumns();
						
			    		//var s_script  = a_search_results[i].getValue(All_Column[4]);
						//nlapiLogExecution('DEBUG','suiteletFunction','Script -->'+s_script);
			
						//var d_date_created  = a_search_results[i].getValue(All_Column[4]);
						//nlapiLogExecution('DEBUG','suiteletFunction','Date Created -->'+d_date_created);
			
						var i_percent_complete  = a_search_results[i].getValue(All_Column[6]);
						nlapiLogExecution('DEBUG','suiteletFunction','Percent Complete -->'+i_percent_complete);
			
						var s_script_status  = a_search_results[i].getValue(All_Column[4]);
						nlapiLogExecution('DEBUG','suiteletFunction','Script Status -->'+s_script_status);
					
					   i_percent_complete = i_percent_complete;
					
			    	}
					sts.setDefaultValue(s_script_status);
					per.setDefaultValue(i_percent_complete);
			    }
				else
				{
					var url='https://system.sandbox.netsuite.com/app/site/hosting/scriptlet.nl?script=263&deploy=1&compid=3883006&whence=';
					sts.setDefaultValue('complete');
					per.setDefaultValue('100%');
					var createNewReqLink = form.addField('custpage_new_req_link','inlinehtml', null, null,null);
					var linkURL = nlapiResolveURL('SUITELET', 'customscript_fixed_bid_project_screen','customdeploy1', null);
				 
					//Create a link to launch the Suitelet.
					createNewReqLink.setDefaultValue('<B>Click <A HREF="' + url +'">Here</A>To Go Home Page.</B>');
					
					
					var createNewReqLink1 = form.addField('custpage_new_req_link1','inlinehtml', null, null,null);
					//var linkURL = nlapiResolveURL('SUITELET', 'customscript_fixed_bid_project_screen','customdeploy1', null) + '&customerid=' + nlapiGetRecordId();
				 
					//Create a link to launch the Suitelet.
					createNewReqLink1.setDefaultValue('<B>Journel Entry Created ,Please Check The Selected Project .</B>');
				}
				//-------------------------------------------------------------------------
	
		}
	
	
	form.addSubmitButton('REFRESH');
	response.writePage( form );
}
catch(exception)
{
	  nlapiLogExecution('DEBUG', 'ERROR',' Exception Caught -->' + exception);	
}
	
}
//-------------------------------------------------------------------------------
function _logValidation(value) 
{
 if(value!='null' && value != null && value != null && value != '' && value != undefined && value != undefined && value != 'undefined' && value!= 'undefined'&& value!= 'NaN' && value != NaN) 
 {
  return true;
 }
 else 
 { 
  return false;
 }
}
