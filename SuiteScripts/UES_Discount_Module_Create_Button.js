// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
/*
   	Script Name : UES_Discount_Module_Create Button.js
	Author      : Jayesh Dinde
	Date        : 14 Sep 2016
    Description : Create Button for JE creation


	Script Modification Log:

	-- Date --			-- Modified By --				--Requested By--				-- Description --



Below is a summary of the process controls enforced by this script file.  The control logic is described
more fully, below, in the appropriate function headers and code blocks.


     BEFORE LOAD
		- beforeLoad_DiscountModule(type)



     BEFORE SUBMIT
		- beforeSubmitRecord(type)


     AFTER SUBMIT
		- afterSubmit_salary_upload(type)



     SUB-FUNCTIONS
		- The following sub-functions are called by the above core functions in order to maintain code
            modularization:

               - NOT USED

*/
}
// END SCRIPT DESCRIPTION BLOCK  ====================================



// BEGIN GLOBAL VARIABLE BLOCK  =====================================
{
	//  Initialize any Global Variables, in particular, debugging variables...




}
// END GLOBAL VARIABLE BLOCK  =======================================





// BEGIN BEFORE LOAD ==================================================

function beforeLoad_DiscountModule(type)
{
	try
	{
		if(type == 'view')
		{
			form.setScript('customscript_cli_discount_module');
			
			var i_recordID = nlapiGetRecordId();
			nlapiLogExecution('audit','stat:- '+nlapiGetFieldValue('custrecord_journal_created_id'),nlapiGetFieldValue('custrecord_je_creation_status'));
			
			if(!nlapiGetFieldValue('custrecord_journal_created_id') && !nlapiGetFieldValue('custrecord_je_creation_status'))
			{
				form.addButton('custpage_create_je', 'Create JE', 'Create_JE(\'  ' + i_recordID + '  \');');
			}
			
			if((nlapiGetFieldValue('custrecord_journal_created_id') == '' || nlapiGetFieldValue('custrecord_journal_created_id') == null) && ((nlapiGetFieldValue('custrecord_je_creation_status') != 'Processing' && nlapiGetFieldValue('custrecord_je_creation_status') != 'Pending' && nlapiGetFieldValue('custrecord_je_creation_status') != null)))
			{
				form.addButton('custpage_create_je', 'Create JE', 'Create_JE(\'  ' + i_recordID + '  \');');
			}
			
			if(nlapiGetFieldValue('custrecord_journal_created_id') != '' && nlapiGetFieldValue('custrecord_journal_created_id') != null)
			{
				form.addButton('custpage_export_data', 'Export CSV', 'Export_CSV(\'  ' + i_recordID + '  \');');
			}
				
			if (nlapiGetFieldValue('custrecord_je_creation_status') != 'Complete' && (nlapiGetFieldValue('custrecord_je_creation_status') != '' && nlapiGetFieldValue('custrecord_je_creation_status') != null))
			{
				var filters = new Array();
				filters.push(new nlobjSearchFilter('internalid', 'script', 'is', '1050'));
				
				var column = new Array();
				column.push(new nlobjSearchColumn('name', 'script'));
				column.push(new nlobjSearchColumn('internalid', 'scriptdeployment'));
				column.push(new nlobjSearchColumn('datecreated'));
				column.push(new nlobjSearchColumn('status'));
				column.push(new nlobjSearchColumn('startdate'));
				column.push(new nlobjSearchColumn('enddate'));
				column.push(new nlobjSearchColumn('queue'));
				column.push(new nlobjSearchColumn('percentcomplete'));
				column.push(new nlobjSearchColumn('queueposition'));
				column.push(new nlobjSearchColumn('percentcomplete'));
				
				var a_search_results = nlapiSearchRecord('scheduledscriptinstance', null, filters, column);
				if (_logValidation(a_search_results)) {
					for (var i = 0; i < a_search_results.length; i++) {
						var s_script = a_search_results[i].getValue('name', 'script');
						
						var d_date_created = a_search_results[i].getValue('datecreated');
						
						var i_percent_complete = a_search_results[i].getValue('percentcomplete');
						
						var s_script_status = a_search_results[i].getValue('status');
						
					}
				}
				
				if (i_percent_complete == '' || i_percent_complete == null || i_percent_complete == undefined) {
					s_script_status = 'Not Started';
					i_percent_complete = '0%';
				}
				
				if (s_script_status == 'Complete') {
					i_percent_complete = '100%';
					s_script_status = 'Complete';
				}
				
				var message = '<html>';
				message += '<head>';
				message += "<meta http-equiv=\"refresh\" content=\"5\" \/>";
				message += '<meta charset="utf-8" />';
				message += '</head>';
				message += '<body>';
				var i_counter = '0%'
				message += "<div id=\"my-progressbar-container\">";
				message += "            ";
				message += "<div class=\"ajax-content\" id=\"test14\" name=\"test14\" style='height:50%;width:50%;' align='left' valign='top'>"; //opacity: .75;
				message += "<img src='https://system.na1.netsuite.com/core/media/media.nl?id=190162&c=3883006&h=1cf2a9f8bd8b922539c8' align='left' style='height:30%;width:30%;align=left;'/>";
				message += "        <\/div>";
				message += '</body>';
				message += '</html>';
				
				nlapiSetFieldValue('custrecord_discount_module_progress_bar', message);
				
				nlapiSubmitField('customrecord_discount_module_for_cust', nlapiGetRecordId(), 'custrecord_je_creation_status', s_script_status);
			}
		}
	}
	catch(err)
	{
		nlapiLogExecution('ERROR','ERROR MESSAGE:-- ',err);
	}
}

function _logValidation(value) 
{
 if(value!='null' && value != null && value != null && value != '' && value != undefined && value != undefined && value != 'undefined' && value!= 'undefined'&& value!= 'NaN' && value != NaN) 
 {
  return true;
 }
 else 
 { 
  return false;
 }
}