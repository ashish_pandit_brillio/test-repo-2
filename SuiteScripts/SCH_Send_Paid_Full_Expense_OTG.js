/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       11 Jun 2021     shravan.k
 *
 */

/**
 * @param {String} type Context Types: scheduled, ondemand, userinterface, aborted, skipped
 * @returns {Void}
 */
function func_Send_OTG_Details(type)
{
	try
		{
			var obj_Usage_Context = nlapiGetContext();
			nlapiLogExecution('DEBUG', 'Begining Usage  ==',obj_Usage_Context.getRemainingUsage());
			var obj_Date = nlapiDateToString(new Date());
			var d_Today_Date = nlapiDateToString(nlapiAddDays(nlapiStringToDate(obj_Date),0), 'date');
			var d_Yeterday_Date = nlapiDateToString(nlapiAddDays(nlapiStringToDate(d_Today_Date),-1), 'date');
			nlapiLogExecution('DEBUG','d_Today_Date==' , d_Today_Date);
			nlapiLogExecution('DEBUG','d_Yeterday_Date==' , d_Yeterday_Date);
			var arr_Filters = [
							   ["type","anyof","ExpRept"], 
							   "AND", 
							   ["systemnotes.newvalue","contains","Paid In Full"], 
							   "AND", 
							   //["systemnotes.date","on","today"], 
							   ["systemnotes.date","within",d_Yeterday_Date,d_Today_Date],
							   "AND", 
							   ["mainline","is","T"]
							];
			var arr_Columns = [
							   new nlobjSearchColumn("transactionnumber"), 
							   new nlobjSearchColumn("statusref"),
							   new nlobjSearchColumn("status")
							];
			//var obj_Exp_Search = searchRecord('expensereport','customsearch3861',null,arr_Columns);
			var obj_Exp_Search = searchRecord('expensereport',null,arr_Filters,arr_Columns);
			var arr_Json_Response = [];
			var obj_Json_Resp = {};
			var s_Status = '';
			var s_Expense_Id = '';
			if(obj_Exp_Search)
				{
				nlapiLogExecution('DEBUG', ' obj_Exp_Search.length ==',obj_Exp_Search.length);
					for(var i_Index = 0; i_Index < obj_Exp_Search.length ;i_Index++)
						{
								s_Status = obj_Exp_Search[i_Index].getValue('status');
							 s_Expense_Id =obj_Exp_Search[i_Index].getValue('transactionnumber');
							obj_Json_Resp ={
									expId : s_Expense_Id,
									status : s_Status
							};
							arr_Json_Response.push(obj_Json_Resp);
						} /// for(var i_Index = 0; i_Index < obj_Exp_Search.length ;i_Index++)
				} /////if(obj_Exp_Search) 
			nlapiLogExecution('DEBUG', 'arr_Json_Response  ==',arr_Json_Response);
			nlapiLogExecution('DEBUG', 'arr_Json_Response  ==',JSON.stringify(arr_Json_Response));
			nlapiLogExecution('DEBUG', 'Mid Usage  ==',obj_Usage_Context.getRemainingUsage());
			
            //**************Authentication steps******************//
            var commonHeader = {
            		"Content-Type": "application/json",
           		    "accept": "application/json"
            };
          
            //************************Token Generation steps*****************************//
            var tokenURL = 'https://otgv2prod-fusion-and-performance-management-node.azurewebsites.net/api/otg-fusion-and-performance-management/leaves/get-access-and-refresh-tokens?userName=NSUser&password=NSOtgIntegrations'
            var tokenResponse = nlapiRequestURL(tokenURL,null, commonHeader, null,'GET');
            nlapiLogExecution("DEBUG", "tokenResponse Body :", (tokenResponse.body)); //JSON.stringify
            var tken = JSON.parse(tokenResponse.body);
            nlapiLogExecution("DEBUG", "tokenResponse accessToken :", (tken.data.accessToken));
            //********************END***********************************//
           



            //**************Authentication********************************//
            var dataRow = arr_Json_Response;
            nlapiLogExecution("DEBUG", "body create Mode: ", JSON.stringify(dataRow));
            var method = "POST";
            	var url = "https://onthegov2prodnetsuitemanagement.azurewebsites.net/api/otg-netsuite-management/expense-management/sync-paid-in-full-status"
            
            var headers = {
                	"Authorization": "Bearer "+tken.data.accessToken,
                    "x-api-key": "615679DD-ABEA-48CE-B916-28974A309F77",
                    "x-api-customer-key": "98F78573-3172-4A8D-A521-3DC619AF2BE1",
                    "x-api-token-key": "8D660F3B-7CA9-47D0-848A-E8D7BE1B2786",
                    "x.api.user": "113056",
                    "content-type": "application/json",
                    "Accept": "application/json"

                };

            var JSONBody = JSON.stringify(dataRow, replacer);

            var response = nlapiRequestURL(url, JSONBody, headers, method);
            nlapiLogExecution("DEBUG", "response Body ", JSON.stringify(response.body));

            //************************END*****************************// 	

            nlapiLogExecution('DEBUG', 'Final Remaining Usage  ==',obj_Usage_Context.getRemainingUsage());
            if (!(parseInt(JSON.parse(response.getCode())) == 200)) 
            {
								var arr_Emp_Attachment = new Array();
                arr_Emp_Attachment['entity'] = 83134;
                var obj_File = nlapiCreateFile("payload.json", "PLAINTEXT",JSON.stringify(arr_Json_Response));
                var s_Email_Sub = "Error in Expense Updation For Paid in Full";
                var s_Email_Body = "Team," + "\r\n" + "Error while updating the expense details into OTG server For Paid in Full"+ "\r\n" +"Please find attached payLoad." + "\r\n";
                nlapiSendEmail(442, "onthego@BRILLIO.COM", s_Email_Sub, s_Email_Body, 'information.systems@brillio.com', null, arr_Emp_Attachment, obj_File);
            } //// if (!(parseInt(JSON.parse(response.getCode())) == 200)) 
            
		
		} //// End of try
	catch (err) {
      	var a_emp_attachment = new Array();
		a_emp_attachment['entity'] = 83134;
		 var s_Email_Sub = "Error in Expense - NS Script Error for Paid in Full";
		 var s_Email_Body = "Team," + "\r\n" + "Error while sending the expense details into OTG server #" + "\r\n" + "Please find below NS error." + err + "\r\n";
        nlapiSendEmail(442, "onthego@BRILLIO.COM", s_Email_Sub, s_Email_Body, 'information.systems@brillio.com', null, a_emp_attachment);
        nlapiLogExecution('DEBUG', 'Expense Notification Error - OTG', err);
    }//// End of catch
} /// func_Send_OTG_Details(type)
function replacer(key, value) {
    if (typeof value == "number" && !isFinite(value)) {
        return String(value);
    }
    return value;
}
