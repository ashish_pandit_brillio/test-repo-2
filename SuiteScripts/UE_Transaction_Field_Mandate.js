/**
 * Module Description
 * 
 * Version Date Author Remarks 1.00 Apr 14 2015 Nitish Mishra
 * 
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your
 * script deployment.
 * 
 * @appliedtorecord recordType
 * 
 * @param {String}
 *        type Operation types: create, edit, view, copy, print, email
 * @param {nlobjForm}
 *        form Current form
 * @param {nlobjRequest}
 *        request Request object
 * @returns {Void}
 */
function userEventBeforeSubmit() {

	try {
			var department = nlapiGetFieldValue('department');
			var projectVal = nlapiGetFieldValue('job');
			if(projectVal){
						var projectLook = nlapiLookupField('job',projectVal,['custentity_practice']);
						var department_ = projectLook.custentity_practice;
						}
						
						
						if(!department)
						nlapiSetFieldValue('department',department_);
	
	}
	catch (err) {
		nlapiLogExecution('debug', 'userEventBeforeLoad', err);
	}
}
