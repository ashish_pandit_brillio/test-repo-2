/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       01 Apr 2019     Aazamali Khan
 *
 */

/**
 * @param {Object} dataIn Parameter object
 * @returns {Object} Output object
 */
function postRESTlet(dataIn) {
    try {
        var s_certificate = [];
        var tempAllocatedResources = [];
        var tempBenchResources = [];
        var employeeList = [];
        var benchemployeeList = [];

        // Get the user role and search the data accordingly 
        var s_user = dataIn.user;
        var i_user = getUserUsingEmailId(s_user);
        nlapiLogExecution("DEBUG", "i_user", i_user);
        // Get Logged In user's executing practice
        var spocSearch = GetPractice_SPOC_List(i_user);
        var regionSearch = GetRegion(i_user);
        var adminUser = GetAdmin(i_user) //true;
        var deliveryAnchore = GetDeliveryAnchore(i_user);
        nlapiLogExecution('debug', 'deliveryAnchore ', deliveryAnchore);
        var delivery_practice = getDeliveryPractices();

        /************************************************/
        // Get the Index for pagination
        //nlapiLogExecution("ERROR","mode",mode);
        var mode = 0;
        var firstIndex, secondIndex;
        if (mode != "0") {
            firstIndex = parseInt(mode) * 10;
            secondIndex = firstIndex + 10
        } else {
            firstIndex = 0;
            secondIndex = (firstIndex + 10) // - index;
        }
        /************************************************************************************/
        if (adminUser) {
            var searchResultBench = GetBenchResoucesProjection(null, null, true, null, delivery_practice, null); // Get the resources on bench for 3 months
            var searchResult = GetBenchProjection(null, null, true, null, delivery_practice, null); // Allocated resource bench projection
            var o_utilisation = getUtilisation(true, null, null);
        } else if (spocSearch.length > 0) {
            var i_practice = spocSearch;
            //var parentPractice = nlapiLookupField("department",i_practice,"custrecord_parent_practice");
            var practiceArray = getChildPractices(i_practice);
            nlapiLogExecution("AUDIT", "practiceArray : ", JSON.stringify(practiceArray))
            var searchResultBench = GetBenchResoucesProjection(i_user, practiceArray, null, null, delivery_practice, null); // Get the resources on bench for 3 months
            var searchResult = GetBenchProjection(i_user, practiceArray, null, null, delivery_practice, null); // Allocated resource bench projection
            var o_utilisation = getUtilisation(null, practiceArray, null);
        } else if (regionSearch) {
            var i_region = regionSearch;
            var searchResultBench = GetBenchResoucesProjection(i_user, null, null, i_region, delivery_practice, null); // Get the resources on bench for 3 months
            var searchResult = GetBenchProjection(i_user, null, null, i_region, delivery_practice, null); // Allocated resource bench projection
            var o_utilisation = getUtilisation(true, null, null);
        } else if (deliveryAnchore) {
            var searchResultBench = GetBenchResoucesProjection(i_user, null, null, null, delivery_practice, deliveryAnchore); // Get the resources on bench for 3 months
            var searchResult = GetBenchProjection(i_user, null, null, null, delivery_practice, deliveryAnchore); // Allocated resource bench projection
            var o_utilisation = getUtilisation(true, null, null);
        } else {
            var searchResultBench = GetBenchResoucesProjection(i_user, null, null, null, delivery_practice, null); // Get the resources on bench for 3 months
            var searchResult = GetBenchProjection(i_user, null, null, null, delivery_practice, null); // Allocated resource bench projection
            var o_utilisation = getUtilisation(null, null, i_user);
        }
        // From Allocation 
        if (searchResult) {
            for (var i = 0; i < searchResult.length; i++) {
                //checkMatring();
                var jsonObj = {};
                var empId = searchResult[i].getValue("resource", null, "GROUP");
                jsonObj.avaliable = searchResult[i].getValue("percentoftime", null, "GROUP");
                jsonObj.employeename = searchResult[i].getText("resource", null, "GROUP");
                jsonObj.employeeId = searchResult[i].getValue("resource", null, "GROUP");
                jsonObj.designation = searchResult[i].getValue("title", "employee", "GROUP")!="- None -"?searchResult[i].getValue("title", "employee", "GROUP") : ""; //nlapiLookupField("employee", empId, "title");
                jsonObj.practice = searchResult[i].getText("department", "employee", "GROUP");
                jsonObj.location = searchResult[i].getText("location", "employee", "GROUP");
                jsonObj.bench = searchResult[i].getValue("enddate", null, "MAX");
                jsonObj.days = "NA";
                jsonObj.futureTermDate = searchResult[i].getValue("custentity_future_term_date", "employee", "GROUP") || ''; //future term date added by prabhat gupta NIS-1295  13/4/2020

                //--------------------------------------------------------------------------------------------------
                //prabhat gupta 15/10/2020 NIS-1772				


                jsonObj.fusionId = searchResult[i].getValue("custentity_fusion_empid", "employee", "GROUP") || "";
                jsonObj.empLevel = searchResult[i].getText("employeestatus", "employee", "GROUP") || "";
                jsonObj.onsiteOffsite = searchResult[i].getText("custevent4", null, "GROUP");

                jsonObj.resignationNotifyDate = searchResult[i].getValue("custentity_resignation_notify_date", "employee", "GROUP") || '';

                //-------------------------------------------------------------------------------------------------

                //--------------------------------------------------------------------------------------------------
                //prabhat gupta 18/11/2020

                var inactiveReason = searchResult[i].getText("custentity_inactivereason", "employee", "GROUP") || "";
                var hireDate = searchResult[i].getValue("hiredate", "employee", "GROUP") || "";
                var actualHireDate = searchResult[i].getValue("custentity_actual_hire_date", "employee", "GROUP") || "";
                var projectAllocationCategoryId = searchResult[i].getValue("custentity_project_allocation_category", "job", "GROUP") || "";
                var projectAllocationCategory = searchResult[i].getText("custentity_project_allocation_category", "job", "GROUP") || "";

                jsonObj.localHireOrGT = inactiveReason != "- None -" ? inactiveReason : "";
                jsonObj.hireDate = hireDate;
                jsonObj.actualHireDate = actualHireDate;
				jsonObj.projectAllocationCategoryId = projectAllocationCategoryId;
				jsonObj.projectAllocationCategory = projectAllocationCategory;

                //----------------------------------------------------------------------------------------------------

                tempAllocatedResources.push(jsonObj);
                employeeList.push(searchResult[i].getValue("resource", null, "GROUP"));
            }
        }
        // From Bench 
        if (searchResultBench) {
            for (var i = 0; i < searchResultBench.length; i++) {
                //checkMatring();
                var jsonObj = {};
                var empId = searchResultBench[i].getValue("resource", null, "GROUP");
                jsonObj.avaliable = searchResultBench[i].getValue("percentoftime", null, "GROUP");
                jsonObj.employeename = searchResultBench[i].getText("resource", null, "GROUP");
                jsonObj.employeeId = searchResultBench[i].getValue("resource", null, "GROUP");
                jsonObj.designation = searchResultBench[i].getValue("title", "employee", "GROUP") //nlapiLookupField("employee", empId, "title");
                jsonObj.practice = searchResultBench[i].getText("department", "employee", "GROUP");
                jsonObj.location = searchResultBench[i].getText("location", "employee", "GROUP");
                var enddate = searchResultBench[i].getValue("enddate", null, "MAX");
                var startDate = searchResultBench[i].getValue("startdate", null, "MIN");
                enddate = nlapiStringToDate(enddate);
                var date = new Date();
                var milliseconds = date - enddate;
                var days = Math.round(milliseconds / (1000 * 60 * 60 * 24));
                jsonObj.bench = nlapiDateToString(enddate, "mm/dd/yyyy");
                jsonObj.days = Math.abs(days);
                jsonObj.benchStartDate = startDate;
                jsonObj.futureTermDate = searchResultBench[i].getValue("custentity_future_term_date", "employee", "GROUP") || ''; //future term date added by prabhat gupta NIS-1295  13/4/2020

                //--------------------------------------------------------------------------------------------------
                //prabhat gupta 15/10/2020 NIS-1772				


                jsonObj.fusionId = searchResultBench[i].getValue("custentity_fusion_empid", "employee", "GROUP") || "";
                jsonObj.empLevel = searchResultBench[i].getText("employeestatus", "employee", "GROUP") || "";
                jsonObj.onsiteOffsite = searchResultBench[i].getText("custevent4", null, "GROUP");

                jsonObj.resignationNotifyDate = searchResultBench[i].getValue("custentity_resignation_notify_date", "employee", "GROUP") || '';

                //-------------------------------------------------------------------------------------------------

                //--------------------------------------------------------------------------------------------------
                //prabhat gupta 18/11/2020

                var inactiveReason = searchResultBench[i].getText("custentity_inactivereason", "employee", "GROUP") || "";
                var hireDate = searchResultBench[i].getValue("hiredate", "employee", "GROUP") || "";
                var actualHireDate = searchResultBench[i].getValue("custentity_actual_hire_date", "employee", "GROUP") || "";



                jsonObj.localHireOrGT = inactiveReason != "- None -" ? inactiveReason : "";
                jsonObj.hireDate = hireDate;
                jsonObj.actualHireDate = actualHireDate;


                //----------------------------------------------------------------------------------------------------

                tempBenchResources.push(jsonObj);
                employeeList.push(searchResultBench[i].getValue("resource", null, "GROUP"));
                //nlapiLogExecution("ERROR","remaining usage : ",nlapiGetContext().getRemainingUsage());
            }
            benchemployeeList = employeeList;
        }
		
		         
		
        var skillArray = [];
        if (employeeList) {
            var skillSearchResult = getFamilySkill(employeeList); // Get the employees skills 
            if (skillSearchResult) {
                for (var j = 0; j < skillSearchResult.length; j++) {
                    //checkMatring();
                    var jsonObj = {};
                    /*jsonObj.skills = skillSearchResult[j].getText("custrecord_primary_updated") + "," + skillSearchResult[j].getValue("custrecord_secondry_updated");
                jsonObj.employeeId = skillSearchResult[j].getValue("custrecord_employee_skill_updated");
                jsonObj.certificates = skillSearchResult[j].getValue("custrecord_certifcate_det", "CUSTRECORD_EMP_SKILL_PARENT", null);*/
                    jsonObj.skills = skillSearchResult[j].getText("custrecord_resource_skillset");
                    jsonObj.employeeId = skillSearchResult[j].getValue("custrecord_skill_resource");
                    jsonObj.certificates = skillSearchResult[j].getValue("custrecord_res_certificate");
                    skillArray.push(jsonObj);
                }
            }
			
			
			var lastCustomerOnBench = getLastCustomer(employeeList);
			
        }

        //-----------------------------------------------------------------------------------------------------------------
        var pastDeliveryAllocation = getLastCustomer(employeeList);


        //-----------------------------------------------------------------------------------------------------------------


        var frfSearchResult = GetFRFSoftLock(employeeList); // Get the employees who are softlocked.
        var frfResult = [];
        if (frfSearchResult) {
            for (var j = 0; j < frfSearchResult.length; j++) {
                //checkMatring();
                //nlapiLogExecution("ERROR", "frfSearchResult.length", frfSearchResult.length)
                var jsonObj = {};
                jsonObj.status = frfSearchResult[j].getValue("custrecord_frf_details_frf_number");
                jsonObj.employeeId = frfSearchResult[j].getValue("custrecord_frf_details_selected_emp");
                jsonObj.fitmentType = frfSearchResult[j].getValue("custrecord_frf_details_fitment_type"); //prabhat gupta 08/09/2020 NIS-1723
                jsonObj.level = frfSearchResult[j].getValue("custrecord_frf_details_level"); //prabhat gupta 07/10/2020 NIS-1755

                //------------------------------------------------------------------------------------------------
                //prabhat gupta 15/10/2020 NIS-1772
                jsonObj.account = frfSearchResult[j].getText("custrecord_frf_details_account") || "";
                jsonObj.accountId = frfSearchResult[j].getValue("custrecord_frf_details_account") || "";

                jsonObj.startDate = frfSearchResult[j].getValue("custrecord_frf_details_start_date");
                jsonObj.desiredDate = frfSearchResult[j].getValue("custrecord_frf_desired_start_date");
                jsonObj.primarySkill = frfSearchResult[j].getText("custrecord_frf_details_primary_skills");
                jsonObj.secondarySkill = frfSearchResult[j].getText("custrecord_frf_details_secondary_skills");

                //--------------------------------------------------------------------------------------------------

                //------------------------------------------------------------------------------------------------
                //prabhat gupta 18/11/2020

                var frfStatus = frfSearchResult[j].getValue("custrecord_frf_details_status_flag") || "";
                var external = frfSearchResult[j].getValue("custrecord_frf_details_external_hire") || "";
                var requestType = frfSearchResult[j].getValue("custrecord_frf_type") || "";
                var resStartDate = frfSearchResult[j].getValue("custrecord_frf_details_start_date") || "";
                var opportunityRevenueStatus = frfSearchResult[j].getValue("custrecord_projection_status_sfdc", "CUSTRECORD_FRF_DETAILS_OPP_ID", null) || "";

                jsonObj.requestType = requestType;
                jsonObj.frfStatus = frfStatus;
                jsonObj.opportunityRevenueStatus = opportunityRevenueStatus;
                jsonObj.external = external;
                jsonObj.resStartDate = resStartDate


                //-------------------------------------------------------------------------------------------------

                frfResult.push(jsonObj);
            }
        }
        var trainingResult = GetEmployeeTrainings(employeeList); // Get the training assigned to employee.
        var trainingArray = [];
        if (trainingResult) {
            for (var i = 0; i < trainingResult.length; i++) {
                //checkMatring();
                var jsonObj = {};
                jsonObj.trainingassigned = trainingResult[i].getValue("internalid", "CUSTRECORD_TRAINING_EMPLOYEE", "COUNT");
                jsonObj.employeeId = trainingResult[i].getValue("internalid", null, "GROUP");
                trainingArray.push(jsonObj);
            }
        }
        var utiliArray = new Array();
        var utilizationSearchResult = nlapiLoadSearch(null, "customsearch2733"); // Get the utilization percent for this year.
        var filter = new nlobjSearchFilter('resource', null, 'anyof', employeeList);
        utilizationSearchResult.addFilter(filter);
        var utilizationArray = utilizationSearchResult.runSearch();
        var columns = utilizationArray.getColumns();
        var searchResultCount = 0;
        var resultSlice = null;
        var searchResult = [];

        do {
            resultSlice = utilizationArray.getResults(searchResultCount,
                searchResultCount + 1000);

            if (resultSlice) {

                resultSlice.forEach(function(result) {

                    searchResult.push(result);
                    searchResultCount++;
                });
            }
        } while (isArrayNotEmpty(resultSlice) && resultSlice.length >= 1000);

        if (searchResult) {
            for (var j = 0; j < searchResult.length; j++) {
                //checkMatring();
                var jsonObj = {};
                var resource = searchResult[j].getValue(columns[0]);
                jsonObj.employeeId = resource;
                jsonObj.utilizationpercent = searchResult[j].getValue(columns[2]);
                utiliArray.push(jsonObj);
            }

        }
        var visaDetailsResult = GetVisaDetails(employeeList); // Get the visa details 
        var visaDetailsArray = [];
        if (visaDetailsResult) {

            for (var i = 0; i < visaDetailsResult.length; i++) {
                //checkMatring()
                var jsonObj = {};
                jsonObj.visatype = visaDetailsResult[i].getText("custrecord_visa", "CUSTRECORD_VISAID", null);
                jsonObj.visavalidfrom = visaDetailsResult[i].getValue("custrecord27", "CUSTRECORD_VISAID", null);
                jsonObj.visavalidto = visaDetailsResult[i].getValue("custrecord_validtill", "CUSTRECORD_VISAID", null);
                jsonObj.employeeId = visaDetailsResult[i].getId();
                visaDetailsArray.push(jsonObj);
            }
        }
		
		
        var newJoinerSearch = getNewJoiners(benchemployeeList);
        var newJoinerArray = [];
		
        if (newJoinerSearch) {
            for (var i = 0; i < newJoinerSearch.length; i++) {
                var jsonObj = {};
                jsonObj.empName = newJoinerSearch[i].getValue("entityid");
                jsonObj.empId = newJoinerSearch[i].getId();
				jsonObj.customer = "New Hire";
                newJoinerArray.push(jsonObj);				
				
            }
			
			
			
			
        }
        var longLeaveSearch = getLongLeaveEmployees(employeeList);
        var longSearch = [];
        if (longLeaveSearch) {
            for (var i = 0; i < longLeaveSearch.length; i++) {
                var jsonObj = {};

                jsonObj.empName = longLeaveSearch[i].getText("custrecord_leave_emp_id");
                jsonObj.empId = longLeaveSearch[i].getValue("custrecord_leave_emp_id");
                //------------------------------------------------------------------------------------------------
                //prabhat gupta 15/10/2020 NIS-1772
                jsonObj.startDate = longLeaveSearch[i].getValue("custrecord_leave_st_date");
                jsonObj.endDate = longLeaveSearch[i].getValue("custrecord_leave_end_date");
                jsonObj.leaveType = longLeaveSearch[i].getValue("custrecord_leave_type");
                //------------------------------------------------------------------------------------------------
                longSearch.push(jsonObj);
            }
        }
		
		var allocationEnding = allocationEndingWithinOneMonth();
		
		var ramp_up_ramp_down = {
			"pastDeliveryAllocation" : pastDeliveryAllocation,
			"newJoinerAllocation" : newJoinerArray,
			"allocationEndingWithinOneMonth" : allocationEnding
			
		}
		
        var billable_non_billable = [];
        billable_non_billable = o_utilisation;
        return {
            "dataOut": {
                "allocatedresources": tempAllocatedResources,
                "benchresources": tempBenchResources,
                "skills": skillArray,
                "frfstatus": frfResult,
                "trainings": trainingArray,
                "utilization": utiliArray,
                "visadetails": visaDetailsArray,
                "newjoiners": newJoinerArray,
                "longleaves": longSearch,
                "billable_non_billable": billable_non_billable,
				"ramp_up_ramp_down" : ramp_up_ramp_down
            }
        }
    } catch (e) {
        return {
            "code": "error",
            "message": e
        }
    }
}

function GetPercentOfUtilization(resourcecStartDate, resourcecEndDate) {

    var todaysDate = GetTodaysDate();
    var startOfYear = getEndDate(); //"01/01/" + new Date().getFullYear();
    //nlapiLogExecution("ERROR","startOfYear : ",startOfYear);
    var diff1 = nlapiStringToDate(resourcecStartDate) - nlapiStringToDate(startOfYear);
    var diff1days = Math.round(diff1 / (1000 * 60 * 60 * 24));
    if (diff1days < 0) {
        diff1days = 0;
    }
    var diff2 = 0;
    var diff2days = 0;
    if (nlapiStringToDate(todaysDate) > nlapiStringToDate(resourcecEndDate)) {
        var diff2 = nlapiStringToDate(resourcecEndDate) - nlapiStringToDate(todaysDate);
        var diff2days = Math.round(diff1 / (1000 * 60 * 60 * 24));
        if (diff2days < 0) {
            diff2days = 0;
        }
    }
    // total days different 
    var totalDiffDays = parseInt(diff1days) + parseInt(diff2days);
    var denomnator = Math.round((nlapiStringToDate(todaysDate) - nlapiStringToDate(startOfYear)) / (1000 * 60 * 60 * 24))
    // nlapiLogExecution("ERROR", "denomnator : ", denomnator);
    var utilizationPercent = (totalDiffDays / denomnator) * 100;
    return utilizationPercent;
}

function GetBenchProjection(i_user, i_practice, isAdmin, i_region, delivery_practice, deliveryAnchore) {

    var todaysDate = GetNextDateBench();
    var nextDate = GetNextDateAllocation();
    //var nextDate = GetNextDateBench();
    var filters = [];
    var columns = [];
    nlapiLogExecution('debug', 'deliveryAnchore **&&^^%% ', deliveryAnchore);
    if (isAdmin == true) {
        filters = [
            ["enddate", "within", todaysDate, nextDate], "AND",
            ["job.custentity_project_allocation_category", "noneof", ["4", "16"]], "AND",
            ["employee.department", "anyof", delivery_practice], "AND",
            ["job.status", "anyof", "2"],
            "AND",
            ["employee.custentity_employee_inactive", "is", "F"]
        ] // Need to add closed filter
    } else if (i_user && i_practice) {
        //i_practice = getSkillIds(i_practice);
        //i_practice = JSON.stringify(i_practice);
        nlapiLogExecution("AUDIT", "i_practice", JSON.stringify(i_practice));
        filters = [

            ["enddate", "within", todaysDate, nextDate],
            "AND",
            ["job.custentity_project_allocation_category", "noneof", ["4", "16"]],
            "AND",
            ["employee.department", "anyof", i_practice],
            "AND",
            ["job.custentity_practice", "anyof", i_practice], "AND",
            ["job.status", "anyof", "2"],
            "AND",
            ["employee.custentity_employee_inactive", "is", "F"]
        ]
    } else if (deliveryAnchore) {
        //nlapiLogExecution('debug','i_practice deliveryAnchore**&&^^%% ',JSON.stringify(deliveryAnchore));
        var i_practiceOld = deliveryAnchore[0].practice;
        var i_practice = getChildPractices(i_practiceOld);
        nlapiLogExecution('debug', 'i_practice **&&^^%% ', i_practice);
        filters = [

            ["enddate", "within", todaysDate, nextDate],
            "AND",
            ["job.custentity_project_allocation_category", "noneof", ["4", "16"]],
            "AND",
            ["employee.department", "anyof", i_practice],
            "AND",
            ["job.custentity_practice", "anyof", i_practice], "AND",
            ["job.status", "anyof", "2"],
            "AND",
            ["employee.custentity_employee_inactive", "is", "F"]
        ]
    } else if (i_user) {
        var department = nlapiLookupField("employee", i_user, "department");
        var parentPractice = nlapiLookupField("department", department, "custrecord_parent_practice");
        var practiceArray = getSubPractices(parentPractice);
        nlapiLogExecution
        filters = [
            ["enddate", "within", todaysDate, nextDate],
            "AND",
            ["job.custentity_project_allocation_category", "noneof", ["4", "16"]],
            "AND",
            ["employee.department", "anyof", practiceArray], "AND",
            ["job.status", "anyof", "2"],
            "AND",
            ["employee.custentity_employee_inactive", "is", "F"],
            "AND",
            ["job.custentity_practice", "anyof", practiceArray]

        ]

    } else if (i_region) {
        filters = [
            ["enddate", "within", todaysDate, nextDate],
            "AND",
            ["job.custentity_project_allocation_category", "noneof", ["4", "16"]],
            "AND",
            ["employee.department", "anyof", delivery_practice], "AND",
            ["job.status", "anyof", "2"],
            "AND",
            ["employee.custentity_employee_inactive", "is", "F"]
        ];
    }
    columns = [
        new nlobjSearchColumn("resource", null, "GROUP"),
        new nlobjSearchColumn("enddate", null, "MAX"),
        new nlobjSearchColumn("percentoftime", null, "GROUP"),
        new nlobjSearchColumn("custentity_project_allocation_category", "job", "GROUP"),
        new nlobjSearchColumn("department", "employee", "GROUP"),
        new nlobjSearchColumn("title", "employee", "GROUP"),
        new nlobjSearchColumn("location", "employee", "GROUP"),
        new nlobjSearchColumn("custentity_future_term_date", "employee", "GROUP"), //future term date added by prabhat gupta NIS-1295  13/4/2020
        //--------------------------------------------------------------------------------------------------------
        //prabhat gupta 15/10/2020 NIS-1772
        new nlobjSearchColumn("custentity_fusion_empid", "employee", "GROUP"),
        new nlobjSearchColumn("employeestatus", "employee", "GROUP"),
        new nlobjSearchColumn("custevent4", null, "GROUP"),

        new nlobjSearchColumn("custentity_resignation_notify_date", "employee", "GROUP"),
        //----------------------------------------------------------------------------------------------------------

        //----------------------------------------------------------------------------------------------------------
        //prabhat gupta 18/11/2020
        new nlobjSearchColumn("custentity_inactivereason", "employee", "GROUP"),
        new nlobjSearchColumn("hiredate", "employee", "GROUP"),
        new nlobjSearchColumn("custentity_actual_hire_date", "employee", "GROUP"),
		new nlobjSearchColumn("custentity_project_allocation_category", "job", "GROUP"),


        //----------------------------------------------------------------------------------------------------------

    ]
    nlapiLogExecution("DEBUG", "Filters : ", filters);
    var resourceallocationSearch = searchRecord("resourceallocation", null, filters, columns, null);
    return resourceallocationSearch;
}

function GetFRFSoftLock(empIds) {
    var customrecord_frf_detailsSearch = nlapiSearchRecord("customrecord_frf_details", null,
        [
            ["custrecord_frf_details_selected_emp", "anyof", empIds], "AND",
            ["custrecord_frf_details_status_flag", "anyof", "3"]
        ],
        [
            new nlobjSearchColumn("custrecord_frf_details_frf_number"),
            new nlobjSearchColumn("custrecord_frf_details_selected_emp"),
            new nlobjSearchColumn("custrecord_frf_details_fitment_type"), //prabhat gupta 08/09/2020 NIS-1723
            new nlobjSearchColumn("custrecord_frf_details_level"), //prabhat gupta 07/10/2020 NIS-1755
            //--------------------------------------------------------------------------------------------
            //prabhat gupta 15/10/2020 NIS-1772
            new nlobjSearchColumn("custrecord_frf_details_account"),
            new nlobjSearchColumn("custrecord_frf_details_start_date"),
            new nlobjSearchColumn("custrecord_frf_desired_start_date"),
            new nlobjSearchColumn("custrecord_frf_details_primary_skills"),
            new nlobjSearchColumn("custrecord_frf_details_secondary_skills"),
            //------------------------------------------------------------------------------------------------------

            //-------------------------------------------------------------------------------------------------------
            //prabhat gupta 18/11/2020
            new nlobjSearchColumn("custrecord_frf_details_status_flag"),
            new nlobjSearchColumn("custrecord_frf_details_external_hire"),
            new nlobjSearchColumn("custrecord_frf_type"),
            new nlobjSearchColumn("custrecord_frf_details_start_date"),
            new nlobjSearchColumn("custrecord_projection_status_sfdc", "CUSTRECORD_FRF_DETAILS_OPP_ID", null)
            //-------------------------------------------------------------------------------------------------------
        ]
    );
    return customrecord_frf_detailsSearch;
}

function GetBenchResoucesProjection(i_user, i_practice, isAdmin, i_region, isDelivery, deliveryAnchore) {
    nlapiLogExecution('debug', 'deliveryAnchore in function ', deliveryAnchore)
    var todaysDate = GetTodaysDate();
    nlapiLogExecution("AUDIT", "todaysDate : ", todaysDate);
    var nextDate = GetNextDateBench();
    nlapiLogExecution("AUDIT", "nextDate : ", nextDate);
    var columns = [];
    var filters = [];
    if (isAdmin == true) {
        filters = [
            ["job.custentity_project_allocation_category", "anyof", ["4", "16"]],
            "AND",
            ["job.isinactive", "is", "F"],
            "AND",
            ["employee.department", "anyof", isDelivery],
            "AND",
            ["job.status", "anyof", "2"],
            "AND",
            ["employee.custentity_employee_inactive", "is", "F"],
            "AND",
            ["enddate", "onorafter", todaysDate]
        ];
    } else if (i_user && i_practice) {
        //i_practice = JSON.stringify(i_practice);
        //nlapiLogExecution("ERROR","i_practice : ",JSON.stringify(i_practice))
        filters = [
            ["job.custentity_project_allocation_category", "anyof", ["4", "16"]],
            "AND",
            ["job.isinactive", "is", "F"],
            "AND",
            ["employee.department", "anyof", i_practice],
            "AND",
            ["job.status", "anyof", "2"],
            "AND",
            ["employee.custentity_employee_inactive", "is", "F"],
            "AND",
            ["enddate", "onorafter", todaysDate],
            "AND", ["job.custentity_practice", "anyof", i_practice]
        ];
    } else if (i_user && deliveryAnchore) {
        var account = deliveryAnchore[0].customer;
        var i_practice = deliveryAnchore[0].practice;
        var i_practice = getChildPractices(i_practice)
        nlapiLogExecution('Debug', 'account &&& ', account);
        //i_practice = JSON.stringify(i_practice);
        //nlapiLogExecution("ERROR","i_practice : ",JSON.stringify(i_practice))
        filters = [
            ["job.custentity_project_allocation_category", "anyof", ["4", "16"]],
            "AND",
            ["job.isinactive", "is", "F"],
            "AND",
            ["employee.department", "anyof", i_practice],
            "AND",
            ["job.status", "anyof", "2"],
            "AND",
            ["employee.custentity_employee_inactive", "is", "F"],
            "AND",
            ["enddate", "onorafter", todaysDate],
            "AND", ["job.custentity_practice", "anyof", i_practice]
        ];
    } else if (i_user) {
        var department = nlapiLookupField("employee", i_user, "department");
        var parentPractice = nlapiLookupField("department", department, "custrecord_parent_practice");
        var practiceArray = getSubPractices(parentPractice);
        filters = [
            ["job.custentity_project_allocation_category", "anyof", ["4", "16"]],
            "AND",
            ["job.isinactive", "is", "F"],
            "AND",
            ["employee.department", "anyof", practiceArray],
            "AND",
            ["job.status", "anyof", "2"],
            "AND",
            ["employee.custentity_employee_inactive", "is", "F"],
            "AND",
            ["enddate", "onorafter", todaysDate], "AND",
            [
                "job.custentity_practice", "anyof", practiceArray
            ]
        ];
    } else if (i_region) {
        filters = [
            ["job.custentity_project_allocation_category", "anyof", ["4", "16"]],
            "AND",
            ["job.isinactive", "is", "F"],
            "AND",
            ["employee.department", "anyof", isDelivery],
            "AND",
            ["job.status", "anyof", "2"],
            "AND",
            ["employee.custentity_employee_inactive", "is", "F"],
            "AND",
            ["enddate", "onorafter", todaysDate]
        ];
    }
    var columns = [
        new nlobjSearchColumn("resource", null, "GROUP"),
        new nlobjSearchColumn("enddate", null, "MAX"),
        new nlobjSearchColumn("percentoftime", null, "GROUP"),
        new nlobjSearchColumn("department", "employee", "GROUP"),
        new nlobjSearchColumn("title", "employee", "GROUP"),
        new nlobjSearchColumn("location", "employee", "GROUP"),
        new nlobjSearchColumn("startdate", null, "MIN"),
        new nlobjSearchColumn("custentity_future_term_date", "employee", "GROUP"), //future term date added by prabhat gupta NIS-1295  13/4/2020

        //--------------------------------------------------------------------------------------------------------
        //prabhat gupta 15/10/2020 NIS-1772
        new nlobjSearchColumn("custentity_fusion_empid", "employee", "GROUP"),
        new nlobjSearchColumn("employeestatus", "employee", "GROUP"),
        new nlobjSearchColumn("custevent4", null, "GROUP"),

        new nlobjSearchColumn("custentity_resignation_notify_date", "employee", "GROUP"),
        //----------------------------------------------------------------------------------------------------------

        //----------------------------------------------------------------------------------------------------------
        //prabhat gupta 18/11/2020
        new nlobjSearchColumn("custentity_inactivereason", "employee", "GROUP"),
        new nlobjSearchColumn("hiredate", "employee", "GROUP"),
        new nlobjSearchColumn("custentity_actual_hire_date", "employee", "GROUP")


        //----------------------------------------------------------------------------------------------------------

    ];
    var resourceSearch = searchRecord("resourceallocation", null, filters, columns, null);
    return resourceSearch;
}

function getFamilySkill(employeeIds) {
    /*var customrecord_employee_master_skill_dataSearch = nlapiSearchRecord("customrecord_employee_master_skill_data", null,

        [

            ["custrecord_employee_skill_updated", "anyof", employeeIds]

        ],
        [
            new nlobjSearchColumn("internalid"),
            new nlobjSearchColumn("custrecord_primary_updated"),
            new nlobjSearchColumn("custrecord_secondry_updated"),
            new nlobjSearchColumn("custrecord_family_selected"),
            new nlobjSearchColumn("custrecord_employee_skill_updated"),
            new nlobjSearchColumn("custrecord_certifcate_det", "CUSTRECORD_EMP_SKILL_PARENT", null),
            new nlobjSearchColumn("created").setSort(true)
        ]

    );

    return customrecord_employee_master_skill_dataSearch;*/

    var customrecord_emp_skill_master_dataSearch = searchRecord("customrecord_emp_skill_master_data", null,
        [
            ["custrecord_skill_resource", "anyof", employeeIds]
        ],
        [
            new nlobjSearchColumn("scriptid").setSort(false),
            new nlobjSearchColumn("custrecord_skill_resource"),
            new nlobjSearchColumn("custrecord_res_dept"),
            new nlobjSearchColumn("custrecord_resource_skillset"),
            new nlobjSearchColumn("custrecord_resource_proficency"),
            new nlobjSearchColumn("custrecord_res_certificate")
        ]
    );
    return customrecord_emp_skill_master_dataSearch;
}

function GetTodaysDate() {
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth() + 1;
    var yyyy = today.getFullYear();
    return mm + "/" + dd + "/" + yyyy;
}

function GetNextDateBench() {
    var todaysDate = GetTodaysDate();
    var date = nlapiStringToDate(todaysDate, "mm/dd/yyyy");
    var nextDate = nlapiAddDays(date, -30)
    return nlapiDateToString(nextDate, "mm/dd/yyyy");
}

function endingDate() {
    var todaysDate = GetTodaysDate();
    var date = nlapiStringToDate(todaysDate, "mm/dd/yyyy");
    var nextDate = nlapiAddDays(date, 30)
    return nlapiDateToString(nextDate, "mm/dd/yyyy");
}

function GetNextDateAllocation() {
    var todaysDate = GetTodaysDate();
    var date = nlapiStringToDate(todaysDate, "mm/dd/yyyy");
    var nextDate = nlapiAddDays(date, 120)
    return nlapiDateToString(nextDate, "mm/dd/yyyy");
}

function GetEmployeeTrainings(empIds) {
    var employeeSearch = nlapiSearchRecord("employee", null,
        [
            ["internalid", "anyof", empIds]
        ],
        [
            new nlobjSearchColumn("internalid", "CUSTRECORD_TRAINING_EMPLOYEE", "COUNT").setSort(false),
            new nlobjSearchColumn("internalid", null, "GROUP")
        ]
    );
    return employeeSearch;
}

function GetUtilizationPercent(empIds) {
    /*
        var startofyear = "01/02/" + new Date().getFullYear();
        var endofyear = "12/31/" + new Date().getFullYear();
        var resourceallocationSearch = nlapiSearchRecord("resourceallocation", null,
            [
                ["custeventrbillable", "is", "T"],
                "AND",
                ["startdate", "notafter", "thisyear"],
                "AND",
                ["enddate", "notbefore", "startofthisyear"],
                "AND", ["resource", "anyof", empIds],
                "AND",
                ["job.custentity_project_allocation_category", "noneof", "4"]
            ],
            [
                new nlobjSearchColumn("resource", null, "GROUP"),
                new nlobjSearchColumn("enddate", null, "MAX"),
                new nlobjSearchColumn("percentoftime", null, "AVG"),
                new nlobjSearchColumn("startdate", null, "GROUP")
            ]
        );
        return resourceallocationSearch*/
    var resourceallocationSearch = nlapiSearchRecord("resourceallocation", null,
        [
            ["custeventrbillable", "is", "T"],
            "AND",
            ["startdate", "notafter", "today"],
            "AND",
            ["enddate", "notbefore", "startofthisyear"],
            "AND",
            ["formuladate: {enddate}", "notbefore", "startofthisyear"],
            "AND", ["resource", "anyof", empIds]
        ],
        [
            new nlobjSearchColumn("resource", null, "GROUP"),
            new nlobjSearchColumn("startdate", null, "GROUP"),
            new nlobjSearchColumn("enddate", null, "GROUP"),
            new nlobjSearchColumn("percentoftime", null, "GROUP"),
            new nlobjSearchColumn("formulanumeric", null, "SUM").setFormula("(CASE WHEN TO_CHAR({enddate},'YY')>TO_CHAR({today},'YY') AND TO_CHAR({startdate},'YY')-TO_CHAR({today},'YY')=0 THEN {today}-{startdate} WHEN {enddate} > {today} AND (TO_CHAR({startdate},'YY') < TO_CHAR({enddate},'YY')) THEN {today}-({today}-TO_CHAR({today},'DDD')-0) WHEN  {enddate} > {today} AND (TO_CHAR({startdate},'YY') - TO_CHAR({enddate},'YY'))=0 THEN {today}-{startdate}WHEN  {enddate} < {today} AND (TO_CHAR({startdate},'YY') - TO_CHAR({enddate},'YY'))=0 THEN {enddate}-{startdate}WHEN  {enddate} < {today} AND (TO_CHAR({startdate},'YY') < TO_CHAR({enddate},'YY')) THEN {enddate}-({today}-(TO_CHAR({today},'DDD')-0))ELSE 0 END)"),
            new nlobjSearchColumn("formulanumeric", null, "SUM").setFormula("Round(CASE WHEN {enddate} > {today} AND (TO_CHAR({startdate},'YY') < TO_CHAR({enddate},'YY')) THEN {today}-({today}-TO_CHAR({today},'DDD')-0) WHEN  {enddate} > {today} AND (TO_CHAR({startdate},'YY') - TO_CHAR({enddate},'YY'))=0 THEN {today}-{startdate}WHEN  {enddate} < {today} AND (TO_CHAR({startdate},'YY') - TO_CHAR({enddate},'YY'))=0 THEN {enddate}-{startdate}WHEN  {enddate} < {today} AND (TO_CHAR({startdate},'YY') < TO_CHAR({enddate},'YY')) THEN {enddate}-({today}-(TO_CHAR({today},'DDD')-0))ELSE 0 END,2)*{percentoftime}"),
            new nlobjSearchColumn("formulapercent", null, "AVG").setFormula("SUM(Round(CASE WHEN {enddate} > {today} AND (TO_CHAR({startdate},'YY') < TO_CHAR({enddate},'YY')) THEN {today}-({today}-TO_CHAR({today},'DDD')-0) WHEN  {enddate} > {today} AND (TO_CHAR({startdate},'YY') - TO_CHAR({enddate},'YY'))=0 THEN {today}-{startdate}WHEN  {enddate} < {today} AND (TO_CHAR({startdate},'YY') - TO_CHAR({enddate},'YY'))=0 THEN {enddate}-{startdate}WHEN  {enddate} < {today} AND (TO_CHAR({startdate},'YY') < TO_CHAR({enddate},'YY')) THEN {enddate}-({today}-(TO_CHAR({today},'DDD')-0))ELSE 0 END,2)*{percentoftime})/TO_CHAR({today},'DDD')"),
            new nlobjSearchColumn("formulanumeric", null, "GROUP").setFormula("TO_CHAR({today},'DDD')"),
            new nlobjSearchColumn("formuladate", null, "GROUP").setFormula("{today}-(TO_CHAR({today},'DDD')-1)")
        ]
    );
    return resourceallocationSearch;
}

function GetVisaDetails(empIds) {
    var employeeSearch = nlapiSearchRecord("employee", null,
        [
            ["internalid", "anyof", empIds]
        ],
        [
            new nlobjSearchColumn("custrecord_visa", "CUSTRECORD_VISAID", null),
            new nlobjSearchColumn("custrecord27", "CUSTRECORD_VISAID", null),
            new nlobjSearchColumn("custrecord_validtill", "CUSTRECORD_VISAID", null)
        ]
    );
    return employeeSearch;
}

function getEndDate() {
    var date = new Date();
    var endDate = new Date(date.getFullYear(), date.getMonth() - 12, 0);
    return nlapiDateToString(endDate);
}

function getChildPractices(praticeId) {
    var departmentSearch = nlapiSearchRecord("department", null,
        [
            ["isinactive", "is", "F"],
            "AND",
            ["custrecord_parent_practice", "anyof", praticeId]
        ],
        [
            new nlobjSearchColumn("name").setSort(false)
        ]
    );
    if (departmentSearch) {
        var arr = [];
        for (var i = 0; i < departmentSearch.length; i++) {
            arr.push(departmentSearch[i].getId());
        }
        return arr;
    } else {
        return praticeId;
    }
}
//Get Delivery Practices

function getDeliveryPractices() {
    var arr_list = [];
    var departmentSearch = nlapiSearchRecord("department", null,
        [
            ["isinactive", "is", "F"],
            "AND",
            ["custrecord_is_delivery_practice", "is", 'T']
        ],
        [
            new nlobjSearchColumn("name").setSort(false)
        ]
    );
    if (departmentSearch) {

        for (var i = 0; i < departmentSearch.length; i++) {
            arr_list.push(departmentSearch[i].getId());
        }

    }
    nlapiLogExecution('DEBUG', 'Delivery Practice', arr_list);
    nlapiLogExecution('DEBUG', 'Delivery Practice JSON', JSON.stringify(arr_list));
    nlapiLogExecution('DEBUG', 'type of arr_list', typeof(arr_list));
    return arr_list;
}

function getNewJoiners(empList) {
    var employeeSearch = nlapiSearchRecord("employee", null,
        [
            ["custentity_actual_hire_date", "within", "thismonth"],
            "AND",
            ["internalid", "anyof", empList]
        ],
        [
            new nlobjSearchColumn("entityid").setSort(false)
        ]
    );
    return employeeSearch;
}

function getLongLeaveEmployees(empList) {
    var customrecord_long_leave_dataSearch = nlapiSearchRecord("customrecord_long_leave_data", null,
        [
            ["custrecord_leave_st_date", "onorbefore", "today"],
            "AND",
            ["custrecord_leave_end_date", "onorafter", "today"], "AND", ["custrecord_leave_emp_id", "anyof", empList]
        ],
        [
            new nlobjSearchColumn("custrecord_leave_emp_id"),
            //-----------------------------------------------------------------------------------
            //prabhat gupta 15/10/2020 NIS-1772
            new nlobjSearchColumn("custrecord_leave_st_date"),
            new nlobjSearchColumn("custrecord_leave_end_date"),
            new nlobjSearchColumn("custrecord_leave_type")
            //-----------------------------------------------------------------------------------

        ]
    );
    return customrecord_long_leave_dataSearch;
}

function getSubPractices(i_practice) {
    var temp = [];
    var departmentSearch = nlapiSearchRecord("department", null,
        [
            ["isinactive", "is", "F"],
            "AND",
            ["custrecord_parent_practice", "anyof", i_practice]
        ],
        []
    );
    if (departmentSearch) {
        for (var i = 0; i < departmentSearch.length; i++) {
            temp.push(departmentSearch[i].getId());
        }
        return temp;
    } else {
        return i_practice
    }
}

function getUtilisation(isAdmin, practice, user) {
    var filters = new Array();
    var columns = new Array();
    if (isAdmin == true) {
        var practiceArray = getAdminPractice();
        filters = [
            ["startdate", "notonorafter", "today"],
            "AND",
            ["enddate", "notonorbefore", "today"],
            "AND",
            ["employee.custentity_employee_inactive", "is", "F"],
            "AND",
            ["employee.custentity_implementationteam", "is", "F"],
            "AND",
            ["employee.department", "anyof", practiceArray]
        ]
    } else if (practice) {
        filters = [
            ["startdate", "notonorafter", "today"],
            "AND",
            ["enddate", "notonorbefore", "today"],
            "AND",
            ["employee.custentity_employee_inactive", "is", "F"],
            "AND",
            ["employee.custentity_implementationteam", "is", "F"],
            "AND",
            ["employee.department", "anyof", practice]
        ]
    } else if (user) {
        var practice = nlapiLookupField('employee', user, 'department');
        var parentPractice = nlapiLookupField("department", practice, "custrecord_parent_practice");
        var a_subPractices = getSubPractices(parentPractice);
        filters = [
            ["startdate", "notonorafter", "today"],
            "AND",
            ["enddate", "notonorbefore", "today"],
            "AND",
            ["employee.custentity_employee_inactive", "is", "F"],
            "AND",
            ["employee.custentity_implementationteam", "is", "F"],
            "AND",
            ["employee.department", "anyof", a_subPractices]
        ]

    }
    var resourceallocationSearch = nlapiSearchRecord("resourceallocation", null, filters,
        [
            new nlobjSearchColumn("custeventrbillable", null, "GROUP"),
            new nlobjSearchColumn("internalid", null, "COUNT")
        ]
    );
    nlapiLogExecution('debug', 'filters $$$$ ', filters);
    if (resourceallocationSearch) {
        var dataArray = new Array();
        for (var i = 0; i < resourceallocationSearch.length; i++) {
            nlapiLogExecution('debug', '')
            var data = {};
            var isBillable = resourceallocationSearch[i].getValue("custeventrbillable", null, "GROUP");
            var count = resourceallocationSearch[i].getValue("internalid", null, "COUNT");
            nlapiLogExecution('Debug', 'isBillable ' + isBillable, 'count ' + count);
            if (isBillable == 'F') {
                data.nonBillable = {
                    "name": count
                };
            }
            if (isBillable == 'T') {
                data.billable = {
                    "name": count
                };
            }
            dataArray.push(data);
        }
        nlapiLogExecution('Debug', 'dataArray Utilisation ', dataArray);
        return dataArray;
    }
}

function getAdminPractice() {
    var departmentSearch = nlapiSearchRecord("department", null,
        [
            ["isinactive", "is", "F"],
            "AND",
            ["custrecord_is_delivery_practice", "is", "T"]
        ],
        [
            new nlobjSearchColumn("internalid")
        ]
    );
    if (departmentSearch) {
        var practiceArray = new Array();
        for (var i = 0; i < departmentSearch.length; i++) {
            practiceArray.push(departmentSearch[i].getValue('internalid'));
        }
        nlapiLogExecution('Debug', 'practiceArray ', practiceArray);
        return practiceArray;
    }
}

//Function to get Practices
function GetPractice_SPOC_List(user) {
    var practiceArray = new Array();
    nlapiLogExecution('AUDIT', 'user', user);
    var spocSearch = nlapiSearchRecord("customrecord_practice_spoc", null,
        [
            ["custrecord_spoc", "anyof", user],
            "AND",
            ["isinactive", "is", "F"]
        ],
        [
            new nlobjSearchColumn("custrecord_practice")
        ]
    );
    if (spocSearch) {
        for (var i = 0; i < spocSearch.length; i++) {
            practiceArray.push(spocSearch[i].getValue('custrecord_practice'));
        }
    }
    return practiceArray;
}

function GetDeliveryAnchore(user) {
    var temp = new Array();
    var customrecord_fuel_delivery_anchorSearch = nlapiSearchRecord("customrecord_fuel_delivery_anchor", null,
        [
            ["custrecord_fuel_anchor_employee", "anyof", user]
        ],
        [
            new nlobjSearchColumn("custrecord_fuel_anchor_customer"),
            new nlobjSearchColumn("custrecord_fuel_anchor_practice")
        ]
    );
    if (customrecord_fuel_delivery_anchorSearch) {
        for (var i = 0; i < customrecord_fuel_delivery_anchorSearch.length; i++) {
            temp.push({
                "customer": customrecord_fuel_delivery_anchorSearch[i].getValue('custrecord_fuel_anchor_customer'),
                "practice": customrecord_fuel_delivery_anchorSearch[i].getValue('custrecord_fuel_anchor_practice')
            });
        }
    }
    nlapiLogExecution('Debug', 'Delivery Anchor Array Length ', temp.length);
    return temp;
}


function allocationEndingWithinOneMonth() {
	
	var todaysDate = GetTodaysDate();
    var nextDate = endingDate();
   var allocation_Search = searchRecord("resourceallocation", null,
        [
		    ["enddate", "within", todaysDate, nextDate],
			"AND",
            ["employee.isinactive","is","F"], 
			   "AND", 
			   ["employee.custentity_implementationteam","is","F"],			  
			   "AND", 
				["custevent_practice.custrecord_is_delivery_practice","is","T"]
		],
        [
		    new nlobjSearchColumn("enddate").setSort(true),
            new nlobjSearchColumn("resource").setSort(false), 
			new nlobjSearchColumn("custentity_fusion_empid", "employee", null),
		   new nlobjSearchColumn("company"), 		    
		   new nlobjSearchColumn("startdate"), 
		   new nlobjSearchColumn("customer")
		   
        ]
    );
    
	var endingAllocation = [];
	for(var i=0; i<allocation_Search.length; i++){
		
		var resource = allocation_Search[i].getValue("resource");
		var temp_resource = "";
		
		
		if(resource != temp_resource){
			
			var data = {};
			
			var emp = allocation_Search[i].getText("resource") || "";
			var fusionId = allocation_Search[i].getValue("custentity_fusion_empid", "employee", null) || "";
			var project = allocation_Search[i].getValue("company") || "";
			var project_name = allocation_Search[i].getText("company") || "";
			var startDate = allocation_Search[i].getValue("startdate") || "";
			var endDate = allocation_Search[i].getValue("enddate") || "";
			var customer = allocation_Search[i].getText("customer") || "";
			var customer_id = allocation_Search[i].getValue("customer") || "";
			
			data.empInternalId = resource;
			data.emp = emp;
			data.fusionId = fusionId;
			data.project = project;
			data.projectName = project_name;
			data.startDate = startDate;
			data.endDate = endDate;
			data.customer_id = customer_id;
			data.customer = customer;
			
			endingAllocation.push(data);
			
			temp_resource = resource;
			
		}
		
	}

    return endingAllocation;

}

function getLastCustomer(empList) {
	
	var allocation_Search = searchRecord("resourceallocation", null,
        [
            ["employee.isinactive","is","F"], 
			   "AND", 
			   ["employee.custentity_implementationteam","is","F"], 
			   "AND", 
			   ["enddate","notbefore","lastyeartodate"],
			   "AND", 
				["custevent_practice.custrecord_is_delivery_practice","is","T"]
		],
        [
		    new nlobjSearchColumn("enddate").setSort(true), 
            new nlobjSearchColumn("resource").setSort(false), 
			new nlobjSearchColumn("custentity_fusion_empid", "employee", null),
		    new nlobjSearchColumn("company"), 
		    new nlobjSearchColumn("startdate"), 
		    new nlobjSearchColumn("customer")
		    
		   
        ]
    );
    
	var pastAllocation = [];
	for(var i=0; i<allocation_Search.length; i++){
		
		var resource = allocation_Search[i].getValue("resource");
		var temp_resource = "";
		
		
		if(resource != temp_resource){
			
			var data = {};
			
			var emp = allocation_Search[i].getText("resource") || "";
			var fusionId = allocation_Search[i].getValue("custentity_fusion_empid", "employee", null) || "";
			var project = allocation_Search[i].getValue("company") || "";
			var project_name = allocation_Search[i].getText("company") || "";
			var startDate = allocation_Search[i].getValue("startdate") || "";
			var endDate = allocation_Search[i].getValue("enddate") || "";
			var customer = allocation_Search[i].getText("customer") || "";
			var customer_id = allocation_Search[i].getValue("customer") || "";
			
			
			data.empInternalId = resource;
			data.emp = emp;
			data.fusionId = fusionId;
			data.project = project;
			data.projectName = project_name;
			data.startDate = startDate;
			data.endDate = endDate;
			data.customer_id = customer_id;
			data.customer = customer;
			
			pastAllocation.push(data);
			
			temp_resource = resource;
			
		}
		
	}

    return pastAllocation;

}