/**
 * @author Shweta
 */
// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
/*
   	Script Name : SUT_Call_Create_Processed_Records.js
	Author      : Shweta Chopde
	Date        : 3 July 2014
	Description : 


	Script Modification Log:

	-- Date --			-- Modified By --				--Requested By--				-- Description --



Below is a summary of the process controls enforced by this script file.  The control logic is described
more fully, below, in the appropriate function headers and code blocks.


     SUITELET
		- suiteletFunction(request, response)


     SUB-FUNCTIONS
		- The following sub-functions are called by the above core functions in order to maintain code
            modularization:

               - NOT USED

*/
}
// END SCRIPT DESCRIPTION BLOCK  ====================================



// BEGIN GLOBAL VARIABLE BLOCK  =====================================
{
	//  Initialize any Global Variables, in particular, debugging variables...




}
// END GLOBAL VARIABLE BLOCK  =======================================





// BEGIN SUITELET ==================================================

function suiteletFunction(request, response)
{
	var i_submitID;
	try
	{
 	var a_data_array=request.getParameter('custscript_data_array_pr');
    nlapiLogExecution('DEBUG', ' suiteletFunction',' Data Array -->' + a_data_array);
   	
	var i_employee =request.getParameter('custscript_employee_id_p');
    nlapiLogExecution('DEBUG', ' suiteletFunction',' Employee -->' + i_employee);
   		
	 if(request.getMethod()=='GET')
	 {
	 	if(_logValidation(a_data_array))
		{
		  var o_processOBJ = nlapiCreateRecord('customrecord_time_process_record');
		  	
		  o_processOBJ.setFieldValue('custrecord_data_p',a_data_array)
		  o_processOBJ.setFieldValue('custrecord_emp_p',i_employee)	
		  
		  i_submitID = nlapiSubmitRecord(o_processOBJ,true,true)	
		  nlapiLogExecution('DEBUG', 'ERROR',' Ei_submitID -->' + i_submitID);	
			
		}//Data Array 	
		
	 }
		
	}
	catch(exception)
	{
	  nlapiLogExecution('DEBUG', 'ERROR',' Exception Caught -->' + exception);	
	}//CATCH	
	response.write(i_submitID);	
}

// END SUITELET ====================================================




// BEGIN OBJECT CALLED/INVOKING FUNCTION ===================================================
function _logValidation(value) 
{
 if(value != null && value.toString() != null && value != '' && value != undefined && value.toString() != undefined && value != 'undefined' && value.toString() != 'undefined'&& value.toString() != 'NaN' && value != NaN) 
 {
  return true;
 }
 else 
 { 
  return false;
 }
}
// END OBJECT CALLED/INVOKING FUNCTION =====================================================
