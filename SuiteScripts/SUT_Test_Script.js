function getHolidayArray() {

var start_date = '2/1/2018';
var end_date = '2/28/2018';
start_date = nlapiStringToDate(start_date);
end_date = nlapiStringToDate(end_date);
	// Brillio Holiday
	var search_company_holiday = nlapiSearchRecord('customrecord_holiday', null,
	        [ new nlobjSearchFilter('custrecord_date', null, 'within',
	                start_date, end_date) ], [
	                new nlobjSearchColumn('custrecord_date'),
	                new nlobjSearchColumn('custrecordsubsidiary') ]);

	var brillio_holidays = {};

	if (search_company_holiday) {

		for (var i = 0; i < search_company_holiday.length; i++) {
			var tempA = brillio_holidays[search_company_holiday[i]
			        .getValue('custrecord_date')];

			if (!tempA) {
				brillio_holidays[search_company_holiday[i]
				        .getValue('custrecord_date')] = {};

				brillio_holidays[search_company_holiday[i]
				        .getValue('custrecord_date')][search_company_holiday[i]
				        .getValue('custrecordsubsidiary')] = [ 'Brillio' ];
			} else {
				var tempB = brillio_holidays[search_company_holiday[i]
				        .getValue('custrecord_date')][search_company_holiday[i]
				        .getValue('custrecordsubsidiary')];

				if (!tempB) {
					brillio_holidays[search_company_holiday[i]
					        .getValue('custrecord_date')][search_company_holiday[i]
					        .getValue('custrecordsubsidiary')] = [ 'Brillio' ];
				} else {
					brillio_holidays[search_company_holiday[i]
					        .getValue('custrecord_date')][search_company_holiday[i]
					        .getValue('custrecordsubsidiary')].push('Brillio');
				}
			}
		}
	}

	nlapiLogExecution('debug', 'Brillio Holiday Done');

	// customer holiday
	var search_customer_holiday = nlapiSearchRecord('customrecordcustomerholiday',
	        null, [ new nlobjSearchFilter('custrecordholidaydate', null,
	                'within', start_date, end_date) ], [
	                new nlobjSearchColumn('custrecordholidaydate'),
	                new nlobjSearchColumn('custrecordcustomersubsidiary'),
	                new nlobjSearchColumn('custrecord13') ]);

	if (search_customer_holiday) {

		for (var i = 0; i < search_customer_holiday.length; i++) {
			var tempA = brillio_holidays[search_customer_holiday[i]
			        .getValue('custrecordholidaydate')];

			if (!tempA) {
				brillio_holidays[search_customer_holiday[i]
				        .getValue('custrecordholidaydate')] = {};

				brillio_holidays[search_customer_holiday[i]
				        .getValue('custrecordholidaydate')][search_customer_holiday[i]
				        .getValue('custrecordcustomersubsidiary')] = [ search_customer_holiday[i]
				        .getValue('custrecord13') ];
			} else {
				var tempB = brillio_holidays[search_customer_holiday[i]
				        .getValue('custrecordholidaydate')][search_customer_holiday[i]
				        .getValue('custrecordcustomersubsidiary')];

				if (!tempB) {
					brillio_holidays[search_customer_holiday[i]
					        .getValue('custrecordholidaydate')][search_customer_holiday[i]
					        .getValue('custrecordcustomersubsidiary')] = [ search_customer_holiday[i]
					        .getValue('custrecord13') ];
				} else {
					brillio_holidays[search_customer_holiday[i]
					        .getValue('custrecordholidaydate')][search_customer_holiday[i]
					        .getValue('custrecordcustomersubsidiary')]
					        .push(search_customer_holiday[i]
					                .getValue('custrecord13'));
				}
			}
		}
	}

	return brillio_holidays;
}