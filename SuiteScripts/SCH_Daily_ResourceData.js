/**
 * @author Jayesh
 */

function schedule()
{
	try
	{
		var context = nlapiGetContext();
		//var billing_from_date = nlapiStringToDate(nlapiDateToString(new Date()));
		var billing_from_date = nlapiStringToDate(nlapiDateToString(nlapiAddDays(new Date(),-1)));
        nlapiLogExecution('audit','date for allocation:- ',billing_from_date);
		
		var filters_allocation = [ [ 'startdate', 'onorbefore', billing_from_date ], 'and',
									[ 'enddate', 'onorafter', billing_from_date ], 'and',
									[ 'custevent_practice.isinactive', 'is', 'F' ], 'and',
									[ 'resource.isinactive', 'is', 'F'], 'and',
									['job.isinactive', 'is', 'F']
								  ];
						
		var columns = new Array();
		columns[0] = new nlobjSearchColumn('custentity_region','customer');
		columns[1] = new nlobjSearchColumn('customer');
		columns[2] = new nlobjSearchColumn('company');
		columns[3] = new nlobjSearchColumn('jobbillingtype', 'job');
		columns[4] = new nlobjSearchColumn('custentity_project_allocation_category', 'job');
		columns[5] = new nlobjSearchColumn('resource');
		columns[6] = new nlobjSearchColumn('custevent_practice');
		columns[7] = new nlobjSearchColumn('subsidiary','employee');
		columns[8] = new nlobjSearchColumn('custrecord_parent_practice', 'custevent_practice');
		columns[9] = new nlobjSearchColumn('custentity_practice','job');
		columns[10] = new nlobjSearchColumn('percentoftime');
		columns[11] = new nlobjSearchColumn('custeventrbillable');
		columns[12] = new nlobjSearchColumn('custentity_clientpartner', 'job');
		columns[13] = new nlobjSearchColumn('custentity_projectmanager', 'job');
		columns[14] = new nlobjSearchColumn('custentity_deliverymanager', 'job');
		columns[15] = new nlobjSearchColumn('custentity_verticalhead', 'job');
		columns[16] = new nlobjSearchColumn('custentity_clientpartner', 'customer');
		columns[17] = new nlobjSearchColumn('startdate');
		columns[18] = new nlobjSearchColumn('enddate');
		
		var project_allocation_result = searchRecord('resourceallocation', null, filters_allocation, columns);
		if (project_allocation_result)
		{
			for(var i_allo_index=0; i_allo_index<project_allocation_result.length; i_allo_index++)
			{
				if(context.getRemainingUsage() <= 1000)
				{
					yieldScript();
				}
				
				var f_prcent_allocated = project_allocation_result[i_allo_index].getValue('percentoftime')
				f_prcent_allocated = f_prcent_allocated.toString();
				f_prcent_allocated = f_prcent_allocated.split('.');
				f_prcent_allocated = f_prcent_allocated[0].toString().split('%');
				var final_prcnt_allocation = parseFloat(f_prcent_allocated) / parseFloat(100);
				final_prcnt_allocation = parseFloat(final_prcnt_allocation);
				
				var o_daily_allocation_rcrd = nlapiCreateRecord('customrecord_daily_emp_head_count_detail'); 
				o_daily_allocation_rcrd.setFieldValue('custrecord_region_daily_ticker',project_allocation_result[i_allo_index].getValue('custentity_region','customer'));
				o_daily_allocation_rcrd.setFieldValue('custrecord_customer_daily_ticker',project_allocation_result[i_allo_index].getValue('customer'));
				o_daily_allocation_rcrd.setFieldValue('custrecord_project_daily_ticker',project_allocation_result[i_allo_index].getValue('company'));
				o_daily_allocation_rcrd.setFieldValue('custrecord_billing_type_daily_ticker',project_allocation_result[i_allo_index].getText('jobbillingtype', 'job'));
				o_daily_allocation_rcrd.setFieldValue('custrecord_proj_category_daily_ticker',project_allocation_result[i_allo_index].getText('custentity_project_allocation_category', 'job'));
				o_daily_allocation_rcrd.setFieldValue('custrecord_emp_daily_ticker',project_allocation_result[i_allo_index].getValue('resource'));
				o_daily_allocation_rcrd.setFieldValue('custrecord_emp_practice_daily_ticker',project_allocation_result[i_allo_index].getValue('custevent_practice'));
				o_daily_allocation_rcrd.setFieldValue('custrecord_emp_subsi_daily_ticker',project_allocation_result[i_allo_index].getValue('subsidiary','employee'));
				o_daily_allocation_rcrd.setFieldValue('custrecord_parent_practice_daily_ticker',project_allocation_result[i_allo_index].getValue('custrecord_parent_practice', 'custevent_practice'));
				o_daily_allocation_rcrd.setFieldValue('custrecord_executing_prac_daily_ticker',project_allocation_result[i_allo_index].getText('custentity_practice','job'));
				o_daily_allocation_rcrd.setFieldValue('custrecord_prcnt_allocated_daily_ticker',final_prcnt_allocation);
				o_daily_allocation_rcrd.setFieldValue('custrecord_resource_billabl_daily_ticker',project_allocation_result[i_allo_index].getValue('custeventrbillable'));
				o_daily_allocation_rcrd.setFieldValue('custrecord_clnt_partner_daily_ticker',project_allocation_result[i_allo_index].getText('custentity_clientpartner', 'job'));
				o_daily_allocation_rcrd.setFieldValue('custrecord_proj_manager_daily_ticker',project_allocation_result[i_allo_index].getText('custentity_projectmanager', 'job'));
				o_daily_allocation_rcrd.setFieldValue('custrecord_delivery_man_daily_ticker',project_allocation_result[i_allo_index].getText('custentity_deliverymanager', 'job'));
				o_daily_allocation_rcrd.setFieldValue('custrecord_vertical_head_daily_ticker',project_allocation_result[i_allo_index].getText('custentity_verticalhead', 'job'));
				o_daily_allocation_rcrd.setFieldValue('custrecord_cust_clnt_part_daily_ticker',project_allocation_result[i_allo_index].getText('custentity_clientpartner', 'customer'));
				o_daily_allocation_rcrd.setFieldValue('custrecord_allocation_strt_date_dailytic',project_allocation_result[i_allo_index].getValue('startdate'));
				o_daily_allocation_rcrd.setFieldValue('custrecord_allocation_end_date_dailytic',project_allocation_result[i_allo_index].getValue('enddate'));
				o_daily_allocation_rcrd.setFieldValue('custrecord_date_of_dump',nlapiDateToString(billing_from_date));
				var i_daily_resource_data = nlapiSubmitRecord(o_daily_allocation_rcrd,true,true);
			}
		}
	}
	catch(err)
	{
		nlapiLogExecution('ERROR','ERROR MESSAGE :- ',err);
	}
}

function searchRecord(recordType, savedSearch, arrFilters, arrColumns,
        filterExpression)
{

	try {
		var search = null;

		// if a saved search is provided, load it and add the filters and
		// columns
		if (isNotEmpty(savedSearch)) {
			search = nlapiLoadSearch(recordType, savedSearch);

			if (isArrayNotEmpty(arrFilters)) {
				search.addFilters(arrFilters);
			}

			if (isArrayNotEmpty(arrColumns)) {
				search.addColumns(arrColumns);
			}

			if (isArrayNotEmpty(filterExpression)) {
				search.setFilterExpression(filterExpression);
			}
		}
		// create a new search
		else {
			search = nlapiCreateSearch(recordType, arrFilters, arrColumns);
		}

		// run search
		var resultSet = search.runSearch();

		// iterate through the search and get all data 1000 at a time
		var searchResultCount = 0;
		var resultSlice = null;
		var searchResult = [];

		do {
			resultSlice = resultSet.getResults(searchResultCount,
			        searchResultCount + 1000);

			if (resultSlice) {

				resultSlice.forEach(function(result) {

					searchResult.push(result);
					searchResultCount++;
				});
			}
		} while (isArrayNotEmpty(resultSlice) && resultSlice.length >= 1000);

		return searchResult;
	} catch (err) {
		nlapiLogExecution('ERROR', 'searchRecord', err);
		throw err;
	}
}

function isEmpty(value) {

	return value == null || value == "" || typeof (value) == undefined;
}

function isNotEmpty(value) {

	return !isEmpty(value);
}

function isArrayEmpty(argArray) {

	return !isArrayNotEmpty(argArray);
}

function isArrayNotEmpty(argArray) {

	return (isNotEmpty(argArray) && argArray.length > 0);
}

function yieldScript() {
 
	nlapiLogExecution('AUDIT', 'API Limit Exceeded');
	var state = nlapiYieldScript();

	if (state.status == "FAILURE") {
		nlapiLogExecution('ERROR', 'Yield Script Failed', 'Reason : '
		        + state.reason + ' / Size : ' + state.size);
		return false;
	} else if (state.status == "RESUME") {
		nlapiLogExecution('AUDIT', 'Script Resumed');
	}
}