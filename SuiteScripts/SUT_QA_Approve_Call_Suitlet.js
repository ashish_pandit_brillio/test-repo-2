/**
 * @author Shweta
 */
// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
/*
    Script Name : SUT_QA_Approve_Call_Suitlet.js
	Author      : Shweta Chopde
	Date        : 22 July 2014
    Description : Approve the Quotation Analysis


	Script Modification Log:

	-- Date --			-- Modified By --				--Requested By--				-- Description --



Below is a summary of the process controls enforced by this script file.  The control logic is described
more fully, below, in the appropriate function headers and code blocks.


     SUITELET
		- suiteletFunction(request, response)


     SUB-FUNCTIONS
		- The following sub-functions are called by the above core functions in order to maintain code
            modularization:

               - NOT USED

*/
}
// END SCRIPT DESCRIPTION BLOCK  ====================================



// BEGIN GLOBAL VARIABLE BLOCK  =====================================
{
	//  Initialize any Global Variables, in particular, debugging variables...




}
// END GLOBAL VARIABLE BLOCK  =======================================





// BEGIN SUITELET ==================================================

function suiteletFunction(request, response)
{
	var i_preferred_vendor ;
	var flag = 0 ;
  	try 
	{
	 if (request.getMethod() == 'GET') 
	 {			
	  var i_recordID =  request.getParameter('custscript_approve_record_id');
      nlapiLogExecution('DEBUG', ' suiteletFunction',' QA ID -->' + i_recordID);	   
		
 
	if(_logValidation(i_recordID))
	{
		var i_author = nlapiGetUser(); 
		
		var o_QA_OBJ = nlapiLoadRecord('customrecord_quotationanalysis',i_recordID)
		
		if(_logValidation(o_QA_OBJ))
		{			
		var i_vendor_PO_selected = o_QA_OBJ.getFieldValue('custrecord_vendorforpo') 	
		
		if (i_vendor_PO_selected != null && i_vendor_PO_selected != '' && i_vendor_PO_selected != undefined) 
		{
		var i_SR_No_QA = o_QA_OBJ.getFieldValue('custrecord_srno')
    								
		var i_item_QA = o_QA_OBJ.getFieldValue('custrecord_item')
		
		var i_PR_ID = o_QA_OBJ.getFieldValue('custrecord_prquote')
    	
		var i_vertical_head = o_QA_OBJ.getFieldValue('custrecord_verticalheadforquotation')
    	
		var i_finance_head = o_QA_OBJ.getFieldValue('custrecord_financehead')
   	
		var i_approval_status = o_QA_OBJ.getFieldValue('custrecord_approvalstatusforquotation')
    	
		var i_vendor_1 = o_QA_OBJ.getFieldValue('custrecord_vendor1name')
    	
		var i_vendor_2 = o_QA_OBJ.getFieldValue('custrecord_vendor2name')
      
	  	var i_vendor_3= o_QA_OBJ.getFieldValue('custrecord_vendor3name')
   	
		var i_total_amt_1 = o_QA_OBJ.getFieldValue('custrecord_amount1')
   	
		var i_total_amt_2 = o_QA_OBJ.getFieldValue('custrecord_amount2')
   	
		var i_total_amt_3= o_QA_OBJ.getFieldValue('custrecord_amount3')
   	
		var i_vendor_PO= o_QA_OBJ.getFieldValue('custrecord_vendorforpo')
   	
		var i_preferred_vendor_selected= o_QA_OBJ.getFieldValue('custrecord_preferred_vendor')
     	
		var f_MIN = Math.min(i_total_amt_1 ,i_total_amt_2,i_total_amt_3)
		
		if(f_MIN == i_total_amt_1)
		{
			i_preferred_vendor = 1
		}
		if(f_MIN == i_total_amt_2)
		{
			i_preferred_vendor = 2
		}
		if(f_MIN == i_total_amt_3)
		{
			i_preferred_vendor = 3
		}
			
     var i_PR_Item_recordID = search_PR_Item_recordID_1(i_PR_ID,i_SR_No_QA,i_item_QA) 
	
	  if (_logValidation(i_PR_Item_recordID)) 
	  {
	  	var o_PR_Item_OBJ = nlapiLoadRecord('customrecord_pritem', i_PR_Item_recordID)
	  	
	  	if (_logValidation(o_PR_Item_OBJ)) 
		{
		     var i_PR_status = o_PR_Item_OBJ.getFieldValue('custrecord_prastatus')
			
				 // if(i_PR_status == 7)
				  {
				  	// ================== More Than 1 Vendor ====================
				
					if((i_vendor_1!=null && i_vendor_1!=undefined && i_vendor_1!='') && ((i_vendor_2!=null && i_vendor_2!=undefined && i_vendor_2!='')  || (i_vendor_3!=null && i_vendor_3!=undefined && i_vendor_3!='')))
					{
						if(i_preferred_vendor_selected!=i_vendor_PO)
						{
							if(i_approval_status == 1)
							{
								o_QA_OBJ.setFieldValue('custrecord_approvalstatusforquotation',2)
								o_QA_OBJ.setFieldValue('custrecord_vendorforpo',i_vendor_PO)
								o_PR_Item_OBJ.setFieldValue('custrecord_prastatus',25)
								o_PR_Item_OBJ.setFieldValue('custrecord_vendor_pr_item',i_vendor_PO)
							   
							}
							if(i_approval_status == 2)
							{
								o_QA_OBJ.setFieldValue('custrecord_approvalstatusforquotation',3)
								o_QA_OBJ.setFieldValue('custrecord_vendorforpo',i_vendor_PO)
								o_PR_Item_OBJ.setFieldValue('custrecord_prastatus',23)
							    o_PR_Item_OBJ.setFieldValue('custrecord_vendor_pr_item',i_vendor_PO)							}
							
						}//Not Selected Preferred Vendor
						
						if(i_preferred_vendor_selected == i_vendor_PO)
						{
							o_QA_OBJ.setFieldValue('custrecord_approvalstatusforquotation',3)
							o_QA_OBJ.setFieldValue('custrecord_vendorforpo',i_vendor_PO)
							o_PR_Item_OBJ.setFieldValue('custrecord_vendor_pr_item',i_vendor_PO)
							o_PR_Item_OBJ.setFieldValue('custrecord_prastatus',23)							
						}//Selected Preferred Vendor
									
					}//More than one vendor					
					if((i_vendor_1!=null && i_vendor_1!=undefined && i_vendor_1!='') && ((i_vendor_2 ==null || i_vendor_2==undefined || i_vendor_2=='')  && (i_vendor_3==null || i_vendor_3==undefined || i_vendor_3=='')))
					{
					    if(i_approval_status == 1)
						{
							o_QA_OBJ.setFieldValue('custrecord_approvalstatusforquotation',2)
							o_QA_OBJ.setFieldValue('custrecord_vendorforpo',i_vendor_PO)
							o_PR_Item_OBJ.setFieldValue('custrecord_prastatus',25)	
							o_PR_Item_OBJ.setFieldValue('custrecord_vendor_pr_item',i_vendor_PO)	
							 						 
						}
						if(i_approval_status == 2)
						{
							o_QA_OBJ.setFieldValue('custrecord_approvalstatusforquotation',3)
							o_QA_OBJ.setFieldValue('custrecord_vendorforpo',i_vendor_PO)
							o_PR_Item_OBJ.setFieldValue('custrecord_prastatus',23)
							o_PR_Item_OBJ.setFieldValue('custrecord_vendor_pr_item',i_vendor_PO)					 
						}
										
					}//Only One Vendor
					  var i_submitID_QA = nlapiSubmitRecord(o_QA_OBJ,true,true)							
					  var i_PR_submitID = nlapiSubmitRecord(o_PR_Item_OBJ,true,true)
					  nlapiLogExecution('DEBUG', 'afterSubmit_Item_Data_Sourcing',' ********************** PR Item Submit ID ********************** -->' + i_PR_submitID);	
		              
					   if(_logValidation(i_submitID_QA)&&_logValidation(i_PR_submitID))
					   {
					   	 flag = 1;
					   }
					
					  if(_logValidation(i_submitID_QA)&&_logValidation(i_vendor_PO)&& i_approval_status == 1)
					  {					  	  
					   sent_email(i_author,i_finance_head)							  	   
					  }
					  				  	
				  }//PR Approved 						
			  	}
			  }			  
						
		}//Vendor PO Selected			
		}//QA Object			
	}//Record ID	
	 }
	}
    catch(exception)
	{
	  nlapiLogExecution('DEBUG', 'ERROR',' Exception Caught -->' + exception);	
	}//CATCH
	
	 response.write(flag);





}

// END SUITELET ====================================================




// BEGIN OBJECT CALLED/INVOKING FUNCTION ===================================================
/**
 * 
 * @param {Object} value
 * 
 * Description --> If the value is blank /null/undefined returns false else returns true
 */
function _logValidation(value) 
{
 if(value != null && value.toString() != null && value != '' && value != undefined && value.toString() != undefined && value != 'undefined' && value.toString() != 'undefined'&& value.toString() != 'NaN' && value != NaN) 
 {
  return true;
 }
 else 
 { 
  return false;
 }
}
/**
 * 
 * @param {Object} i_recordID
 * @param {Object} i_SR_No_QA
 * @param {Object} i_item_QA
 * 
 * Description --> For the criteria Item, Serial No & Purchase Request search the PR ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬ÃƒÂ¢Ã¢â€šÂ¬Ã…â€œ Item record 
 *                 & returns the PR ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬ÃƒÂ¢Ã¢â€šÂ¬Ã…â€œ Item internal ID
 */
function search_PR_Item_recordID_1(i_recordID,i_SR_No_QA,i_item_QA)
{  
   nlapiLogExecution('DEBUG', 'search_PR_Item_recordID', 'i_recordID ==' +i_recordID);
   nlapiLogExecution('DEBUG', 'search_PR_Item_recordID', 'i_SR_No_QA ==' +i_SR_No_QA);
   nlapiLogExecution('DEBUG', 'search_PR_Item_recordID', 'i_item_QA ==' +i_item_QA);
	   	
   var i_internal_id;
   
   var filter = new Array();
   filter[0] = new nlobjSearchFilter('custrecord_purchaserequest', null, 'is',i_recordID);
   filter[1] = new nlobjSearchFilter('custrecord_prlineitemno', null, 'is',parseInt(i_SR_No_QA));
   filter[2] = new nlobjSearchFilter('custrecord_prmatgrpcategory', null, 'is',i_item_QA);
	
   var columns = new Array();
   columns[0] = new nlobjSearchColumn('internalid');
   columns[1] = new nlobjSearchColumn('custrecord_purchaserequest');
   columns[2] = new nlobjSearchColumn('custrecord_prlineitemno');
   columns[3] = new nlobjSearchColumn('custrecord_prmatgrpcategory');
   
   
   var a_seq_searchresults = nlapiSearchRecord('customrecord_pritem',null,filter,columns);
	
	if (a_seq_searchresults != null && a_seq_searchresults != '' && a_seq_searchresults != undefined)
	{	
	 nlapiLogExecution('DEBUG', 'search_PR_Item_recordID', 'a_seq_searchresults ==' +a_seq_searchresults.length);
	 
	 for(var ty=0;ty<a_seq_searchresults.length;ty++)
	 {
	 	i_internal_id = a_seq_searchresults[ty].getValue('internalid');
		nlapiLogExecution('DEBUG', 'search_PR_Item_recordID', ' Internal ID -->' + i_internal_id);
		
		var i_PR = a_seq_searchresults[ty].getValue('custrecord_purchaserequest');
		nlapiLogExecution('DEBUG', 'search_PR_Item_recordID', ' i_PR ID -->' + i_PR);
		
		var i_line_no = a_seq_searchresults[ty].getValue('custrecord_prlineitemno');
		nlapiLogExecution('DEBUG', 'search_PR_Item_recordID', ' i_line_no -->' + i_line_no);
		
		var i_item = a_seq_searchresults[ty].getValue('custrecord_prmatgrpcategory');
		nlapiLogExecution('DEBUG', 'search_PR_Item_recordID', ' i_item-->' + i_item);
		
		if((i_PR == i_recordID)&&(i_line_no == i_SR_No_QA)&&(i_item == i_item_QA))
		{
			i_internal_id = i_internal_id
			break
		}
		
	 }//LOOP
	 
		
		
	}
	
	return i_internal_id;	
}
/**
 * 
 * @param {Object} i_recordID
 * @param {Object} i_SR_No_QA
 * @param {Object} i_item_QA
 * 
 * Description --> For the criteria Item, Serial No & Purchase Request search Quotation Analysis record 
 *                 & returns the Quotation Analysis internal ID 
 */

function search_quotation_analysis_recordID(i_recordID,i_SR_No_QA,i_item_QA)
{
   var i_internal_id;
   
   var filter = new Array();
   filter[0] = new nlobjSearchFilter('custrecord_prquote', null, 'is',i_recordID);
   filter[1] = new nlobjSearchFilter('custrecord_srno', null, 'is',i_SR_No_QA);
   filter[2] = new nlobjSearchFilter('custrecord_item', null, 'is',i_item_QA);
	
   var columns = new Array();
   columns[0] = new nlobjSearchColumn('internalid');
   columns[1] = new nlobjSearchColumn('custrecord_item');
   columns[2] = new nlobjSearchColumn('custrecord_srno');
   columns[3] = new nlobjSearchColumn('custrecord_prquote');
   
   var a_seq_searchresults = nlapiSearchRecord('customrecord_quotationanalysis',null,filter,columns);
	
	if (a_seq_searchresults != null && a_seq_searchresults != '' && a_seq_searchresults != undefined)
	{	
		 for(var ty=0;ty<a_seq_searchresults.length;ty++)
		 {
		 	i_internal_id = a_seq_searchresults[ty].getValue('internalid');
			nlapiLogExecution('DEBUG', 'search_quotation_analysis_recordID', ' Internal ID -->' + i_internal_id);
			
			var i_PR = a_seq_searchresults[ty].getValue('custrecord_prquote');
			nlapiLogExecution('DEBUG', 'search_quotation_analysis_recordID', ' i_PR ID -->' + i_PR);
			
			var i_line_no = a_seq_searchresults[ty].getValue('custrecord_srno');
			nlapiLogExecution('DEBUG', 'search_quotation_analysis_recordID', ' i_line_no -->' + i_line_no);
			
			var i_item = a_seq_searchresults[ty].getValue('custrecord_item');
			nlapiLogExecution('DEBUG', 'search_quotation_analysis_recordID', ' i_item-->' + i_item);
			
			if((i_PR == i_recordID)&&(i_line_no == i_SR_No_QA)&&(i_item == i_item_QA))
			{
				i_internal_id = i_internal_id
				break
			}
			
		 }//LOOP
	}
	
	return i_internal_id;
}

function sent_email(i_author,i_recepientID)
{
	var i_recepient = get_employee_details(i_recepientID)
	
	var s_email_subject = ' Quotation analysis has been raised for your approval .'
			
	var s_email_body ='';	
	s_email_body+='Hi,\n\n';
	s_email_body+='Quotation analysis has been raised for your approval , please login to the Netsuite and go to reminder ˜Quotation pending for finance head approval to approve / reject.\n\n';
	s_email_body+='Regards\n';
	s_email_body+='Information systems\n';
				
	var records = new Object();
    records['entity'] = i_author	

	if(_logValidation(i_author)&&_logValidation(i_recepient)) 
	{		
		 nlapiSendEmail(i_author, i_recepient, s_email_subject, s_email_body, null , null, records, null);
		 nlapiLogExecution('DEBUG', 'sent_email', ' Email Sent ......');	
				
	}//Author & Recepient	
}//Email

function get_employee_details(i_employee)
{	
  var s_email;
  var s_name ;
  var a_return_array = new Array();
  
  if(_logValidation(i_employee))
  {
  	var o_employeeOBJ = nlapiLoadRecord('employee',i_employee)
	
	if(_logValidation(o_employeeOBJ))
	{
	  s_email = o_employeeOBJ.getFieldValue('email')  
	  nlapiLogExecution('DEBUG', 'get_employee_email',' Email -->' + s_email);
			
	}//Employee OBJ
		
  }//Employee
  
  return s_email ;
  	
}//Recipient Email

// END OBJECT CALLED/INVOKING FUNCTION =====================================================
