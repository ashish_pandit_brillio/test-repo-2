/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       18 Apr 2019     Aazamali Khan
 *
 */

/**
 * @param {Object} dataIn Parameter object
 * @returns {Object} Output object
 */
function postRESTlet(dataIn) {
    try {
        var empId = dataIn.empId;
        var oppId = dataIn.oppId;
        nlapiLogExecution("DEBUG", "oppId", oppId);
        var designation = dataIn.designation;
        nlapiLogExecution("DEBUG","designation",JSON.stringify(designation));
        var arrPM = [];
        var arrDM = [];
        if (oppId) {
            arrPM = designation.pm;
            arrDM = designation.dm;
            nlapiLogExecution("DEBUG", "PM Array : ", JSON.stringify(arrPM));
            nlapiLogExecution("DEBUG", "DM Array : ", JSON.stringify(arrDM));
            var recObj = nlapiLoadRecord("customrecord_sfdc_opportunity_record", oppId);
			
				recObj.setFieldValue("custrecord_sfdc_opp_pm", arrPM);	
			 
				recObj.setFieldValue("custrecord_sfdc_opportunity_dm", arrDM);	
			
            var id = nlapiSubmitRecord(recObj,"{disableTriggers : true}",false);
            if (id) {
                if (isArrayNotEmpty(arrPM)) {
                    nlapiLogExecution("AUDIT", "arrPM", JSON.stringify(arrPM));
                    var records = new Array();
                    var emailSub = 'FUEL Notification: You have been assigned as DM/PM';
                    var emailBoody = 'Dear User\nYou have been assigned as DM/PM for Opportunity: ' + s_oppName + 'for Account ' + s_account + 'by ' + s_PracticeHead + ' \n\nBrillio-FUEL\nThis email was sent from a notification only address.\nIf you need further assistance, please write to fuel.support@brillio.com';
                    records['entity'] = [94862, 41571, 144836];
                    nlapiSendEmail(154256, arrPM, emailSub, emailBoody, null, null, records);
                }
                if (isArrayNotEmpty(arrDM)) {
                    nlapiLogExecution("AUDIT", "arrDM", JSON.stringify(arrDM));
                    var records = new Array();
                    var emailSub = 'FUEL Notification: You have been assigned as DM/PM';
                    var emailBoody = 'Dear User\nYou have been assigned as DM/PM for Opportunity: ' + s_oppName + 'for Account ' + s_account + 'by ' + s_PracticeHead + ' \n\nBrillio-FUEL\nThis email was sent from a notification only address.\nIf you need further assistance, please write to fuel.support@brillio.com';
                    records['entity'] = [94862, 41571, 144836];
                    nlapiSendEmail(154256, arrDM, emailSub, emailBoody, null, null, records);
                }
            }
            return {
                "code": "success"
            }
        }
    } catch (e) {
        return {
            "code": "error",
            "message": e
        }
    }
}

function getSkillIds(s_skills) {
    var resultArray = new Array();
    if (_logValidation(s_skills)) {
        nlapiLogExecution('Debug', 's_skills in function', s_skills);
        var temp = s_skills.split(',');
        for (var i = 0; i < temp.length; i++) {
            resultArray.push(temp[i]);
        }
    }
    return resultArray;
}