//BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
    /*
     Script Name:SUT_RPT_NOT_SUBMITTED_FOR_MANAGERS.js
     Author:
     Company:Aashna Cloud tech
     Date:20-08-2014
     Description:
     Script Modification Log:
     -- Date --			-- Modified By --				--Requested By--				-- Description --
     20-08-2014
     Below is a summary of the process controls enforced by this script file.  The control logic is described
     more fully, below, in the appropriate function headers and code blocks.
     SUITELET
     - suiteletFunction(request, response )
     SUB-FUNCTIONS
     - The following sub-functions are called by the above core functions in order to maintain code
     modularization:
     - NOT USED
     */
}
//END SCRIPT DESCRIPTION BLOCK  ====================================



//END GLOBAL VARIABLE BLOCK  =======================================


//BEGIN SUITELET ==================================================
var d_start_wk_1 = null;
var d_start_wk_2 = null;
var d_start_wk_3 = null;
var d_start_wk_4 = null;
var d_start_wk_5 = null;

var d_end_wk_1 = null;
var d_end_wk_2 = null;
var d_end_wk_3 = null;
var d_end_wk_4 = null;
var d_end_wk_5 = null;

function SUT_RPT_NOT_SUBMITTED_CIENA(request, response) //
{
    try //
    {
        var a_employeearray = new Array();
        
        if (request.getMethod() == 'GET') //
        {
            //nlapiLogExecution('DEBUG', 'In GET Method', '')
            
            var s_period = '';
            var form = nlapiCreateForm('Time Sheet Report For Non Submitted Ciena');
            var oldvalues = false;
            var flag = 0;
            form.setScript('customscriptcli_notsubmitted')
            
            var contextObj = nlapiGetContext();
            //nlapiLogExecution('DEBUG', 'GET', ' contextObj===>' + contextObj)
            
            var beginUsage = contextObj.getRemainingUsage();
            //nlapiLogExecution('DEBUG', 'Search Results', 'beginUsage :' + beginUsage);
            
            var fromdate = form.addField('custpage_fromdate', 'Date', 'Period End Date :');
            
            var s_startdate = request.getParameter('custpage_fromdate');
            nlapiLogExecution('DEBUG', 'GET results ', ' s_startdate = ' + s_startdate);
			
           // var i_Subsidiary = request.getParameter('custpage_sub');
            //nlapiLogExecution('DEBUG', 'GET results ', 'i_Subsidiary : ' + i_Subsidiary);
            
            var sublist1 = form.addSubList('record', 'list', 'Time Sheet Report For Non Submitted');
            var id = request.getParameter('id');
            //nlapiLogExecution('DEBUG', 'GET results ', ' id=' + id)
            
            //var select_SUB = form.addField('custpage_sub', 'select', 'Select Subsidiary : ');
            
            /*var search_Result_Sub = '';
            var a_Filters = new Array();
            var a_Columns = new Array();
            
            a_Filters[a_Filters.length] = new nlobjSearchFilter('iselimination', null, 'is', 'F');
            
            a_Columns[a_Columns.length] = new nlobjSearchColumn('internalid');
            a_Columns[a_Columns.length] = new nlobjSearchColumn('name');
            
            search_Result_Sub = nlapiSearchRecord('subsidiary', null, a_Filters, a_Columns);
            
            if (_logValidation(search_Result_Sub)) //
            {
                var a_AllColumns = search_Result_Sub[0].getAllColumns();
                
                //nlapiLogExecution('DEBUG', 'GET results ', 'search_Result_Sub : ' + search_Result_Sub.length);
                
                //for (var counter_I = 0; counter_I < search_Result_Sub.length; counter_I++) //
                //{
					//nlapiLogExecution('DEBUG', 'GET results ', search_Result_Sub[counter_I].getValue(a_AllColumns[0]));
                   // select_SUB.addSelectOption(search_Result_Sub[counter_I].getValue(a_AllColumns[0]), search_Result_Sub[counter_I].getValue(a_AllColumns[1]));
               // }
            }*/
            
            if (id == 'Export') //
            {
                var param = request.getParameter('param');
                //nlapiLogExecution('DEBUG', 'GET results ', '1 param=' + param)
                
                var datesplit = param.toString().split('@')
                //nlapiLogExecution('DEBUG', 'GET results ', ' datesplit=' + datesplit)
                
                s_startdate = datesplit[0];
				
                //nlapiLogExecution('DEBUG', 'GET results ', ' s_startdate=' + s_startdate)
                //s_enddate = datesplit[1]
                //nlapiLogExecution('DEBUG', 'GET results ', ' s_enddate=' + s_enddate)
            }
            
            if (s_startdate == null) //
            {
                oldvalues = false;
            }
            else //
            {
                oldvalues = true;
            }
            
            //nlapiLogExecution('DEBUG', 'GET', ' oldvalues===>' + oldvalues)
            /////////////////////////////////////POST ON SUBMIT ////////////////////////////
            if (oldvalues) // Means start date is selected
            {
                // Add Export Button to form and set on click function to it
                form.addButton('custombutton', 'Export As CSV', 'fxn_generatePDF_For_managers(\'' + s_startdate + '\');');
                
                var id = request.getParameter('id');
                //nlapiLogExecution('DEBUG', 'GET results ', ' id=' + id)
                if (id == 'Export') //
				{
					var param = request.getParameter('param');
					//nlapiLogExecution('DEBUG', 'GET results ', '2 param=' + param)
					
					var datesplit = param.toString().split('@')
					//nlapiLogExecution('DEBUG', 'GET results ', ' datesplit=' + datesplit)
					
					s_startdate = datesplit[0]
					//s_enddate = datesplit[1]
					
					
				}
                //var param = request.getParameter('custpage_sub');
                //nlapiLogExecution('DEBUG', 'GET results ', '3 param=' + param);
                
				var s_startdate1 = s_startdate
                //nlapiLogExecution('DEBUG', 'GET results selected date', ' s_startdate1=' + s_startdate1)
                s_startdate1 = nlapiStringToDate(s_startdate1)
                //nlapiLogExecution('DEBUG', 'GET results ', ' s_startdate1=' + s_startdate1)
                
                //var enddate1 = nlapiAddMonths(s_startdate1, -1)
                var enddate1 = nlapiAddDays(s_startdate1, -34)
                //nlapiLogExecution('DEBUG', 'GET results ', ' enddate1=' + enddate1)
                
                s_startdate1 = nlapiDateToString(s_startdate1)
                enddate1 = nlapiDateToString(enddate1)
                
                d_start_wk_1 = enddate1;
                d_end_wk_1 = s_startdate1;
                
                //var calculateSundays_new = calculateSundays(enddate1, s_startdate1)
                
                //nlapiLogExecution('DEBUG', 'GET results ', ' calculateSundays_new=' + calculateSundays_new)
                //nlapiLogExecution('DEBUG', 'GET results ', ' calculateSundays_new=' + calculateSundays_new.length)
                
                //var calculate = calculateSundays(s_startdate, s_enddate)
                //nlapiLogExecution('DEBUG', 'GET results ', ' calculate=' + calculate.length)
                
                //var enddate_new = calculateSundays_new[0]
                //nlapiLogExecution('DEBUG', 'GET results ', ' enddate_new=' + enddate_new)
                
                
                //nlapiLogExecution('DEBUG', 'GET results ', ' enddate1=' + enddate1 + '===' + s_startdate1)
                
                
                // Set Week Start Date
                d_start_wk_1 = nlapiStringToDate(enddate1);
                //nlapiLogExecution('DEBUG', 'GET results', 'd_start_wk_1=' + d_start_wk_1);
                
                d_start_wk_2 = nlapiAddDays(d_start_wk_1, 7);
                //nlapiLogExecution('DEBUG', 'GET results', 'd_start_wk_2=' + d_start_wk_2);
                
                d_start_wk_3 = nlapiAddDays(d_start_wk_2, 7);
                //nlapiLogExecution('DEBUG', 'GET results', 'd_start_wk_3=' + d_start_wk_3);
                
                d_start_wk_4 = nlapiAddDays(d_start_wk_3, 7);
                //nlapiLogExecution('DEBUG', 'GET results', 'd_start_wk_4=' + d_start_wk_4);
				
                d_start_wk_5 = nlapiAddDays(d_start_wk_4, 7);
				//nlapiLogExecution('DEBUG', 'GET results', 'd_start_wk_4=' + d_start_wk_4);
                
                // Set Week End dates
                d_end_wk_5 = nlapiStringToDate(s_startdate1);
                //nlapiLogExecution('DEBUG', 'GET results', 'd_end_wk_4=' + d_end_wk_4);
				d_end_wk_4 = nlapiAddDays(d_end_wk_5, -7);
                
                d_end_wk_3 = nlapiAddDays(d_end_wk_4, -7);
                //nlapiLogExecution('DEBUG', 'GET results', 'd_end_wk_3=' + d_end_wk_3);
                
                d_end_wk_2 = nlapiAddDays(d_end_wk_3, -7);
                //nlapiLogExecution('DEBUG', 'GET results', 'd_end_wk_2=' + d_end_wk_2);
                
                d_end_wk_1 = nlapiAddDays(d_end_wk_2, -7);
                //nlapiLogExecution('DEBUG', 'GET results', 'd_end_wk_1=' + d_end_wk_1);
                
                
                d_start_wk_1 = nlapiDateToString(d_start_wk_1);
                d_start_wk_2 = nlapiDateToString(d_start_wk_2);
                d_start_wk_3 = nlapiDateToString(d_start_wk_3);
                d_start_wk_4 = nlapiDateToString(d_start_wk_4);
				d_start_wk_5 = nlapiDateToString(d_start_wk_5);
                
                d_end_wk_1 = nlapiDateToString(d_end_wk_1);
                d_end_wk_2 = nlapiDateToString(d_end_wk_2);
                d_end_wk_3 = nlapiDateToString(d_end_wk_3);
                d_end_wk_4 = nlapiDateToString(d_end_wk_4);
				d_end_wk_5 = nlapiDateToString(d_end_wk_5);
                
                var SearchEmployeeData1 = new Array()
                SearchEmployeeData1 = SearchEmployeeData(d_start_wk_1, d_end_wk_4);
                nlapiLogExecution('DEBUG', 'GET results ', ' SearchEmployeeData1=' + SearchEmployeeData1.length)
                
                var timesheetdata_WK_1 = SearchTimeData_New(d_start_wk_1, d_end_wk_1);
                nlapiLogExecution('DEBUG', 'GET results ', ' timesheetdata_WK_1=' + timesheetdata_WK_1.length)
                
                var timesheetdata_WK_2 = SearchTimeData_New(d_start_wk_2, d_end_wk_2);
                nlapiLogExecution('DEBUG', 'GET results ', ' timesheetdata_WK_2=' + timesheetdata_WK_2.length)
                
                var timesheetdata_WK_3 = SearchTimeData_New(d_start_wk_3, d_end_wk_3);
                nlapiLogExecution('DEBUG', 'GET results ', ' timesheetdata_WK_3=' + timesheetdata_WK_3.length)
                
                var timesheetdata_WK_4 = SearchTimeData_New(d_start_wk_4, d_end_wk_4);
                nlapiLogExecution('DEBUG', 'GET results ', ' timesheetdata_WK_4=' + timesheetdata_WK_4.length)
				   var timesheetdata_WK_5 = SearchTimeData_New(d_start_wk_5, d_end_wk_5);
                nlapiLogExecution('DEBUG', 'GET results ', ' timesheetdata_WK_5=' + timesheetdata_WK_5.length)
                
                //var setvalue = setValuesublist(sublist1, SearchEmployeeData1, timesheetdata, enddate_new, s_startdate1, calculateSundays_new, enddate1)
                
                var setvalue = setValuesublist_New(sublist1, SearchEmployeeData1, timesheetdata_WK_1, timesheetdata_WK_2, timesheetdata_WK_3, timesheetdata_WK_4,timesheetdata_WK_5);
                
                //Callsuitelet(sublist1, param);
            
            }
            
            
            if (oldvalues) //
            {
                fromdate.setDefaultValue(s_startdate);
                //Todate.setDefaultValue(s_enddate);
            }
            //nlapiLogExecution('DEBUG', 'GET results ', 'id=' + id)
            
            if (s_startdate == null) //
            {
                form.addSubmitButton('Submit');
                response.writePage(form);
            }
            /*
             else //
             {
             Callsuitelet(sublist1, param);
             }
             */
        }
        else // If method of request is Post
        {
            var date = request.getParameter('custpage_fromdate');
			
			//var subsidiary = request.getParameter('custpage_sub');
			//nlapiLogExecution('DEBUG', 'Post results ', 'subsidiary : ' + subsidiary);
			
            //nlapiLogExecution('DEBUG', 'Post results ', ' date =' + date)
            //var fromDate = request.getParameter('custpage_todate');
            //nlapiLogExecution('DEBUG', 'Post results ', ' fromDate =' + fromDate)
            //date = nlapiDateToString(date);
            //fromDate = nlapiDateToString(fromDate);
            
            var params = new Array();
            params['custpage_fromdate'] = date;
            //params['custpage_sub'] = subsidiary;
            //nlapiSetRedirectURL('SUITELET', 'customscriptnotsubmittedtsrptfromt', 'customdeploynotsubmittedtsrptfrom', false, params);
            nlapiSetRedirectURL('SUITELET', 'customscript_not_submitted_ciena_ts', 'customdeploy1', false, params);
        }
    } 
    catch (e) //
    {
        nlapiLogExecution('ERROR', 'Try Catch Error ', 'Exception = ' + e)
    }
}

function setValuesublist_New(sublist1, SearchEmployeeData, timesheetdata_WK_1, timesheetdata_WK_2, timesheetdata_WK_3, timesheetdata_WK_4,timesheetdata_WK_5) //
{
    get_Manager_Email();
    //nlapiLogExecution('DEBUG', 'get_Manager_Email', 'manager_email_Array count : ' + manager_email_Array.length);
    
    //nlapiLogExecution('DEBUG', 'setValuesublist', 'In Function setValuesublist' + SearchEmployeeData.length);
    var s_GCIID = sublist1.addField('custevent_gciid', 'text', 'GCI ID');
    var s_FusionId = sublist1.addField('custevent_fusid', 'text', 'Fusion Id');
    
    var s_firstName = sublist1.addField('custevent_firstname', 'text', 'First Name');
    var s_lastName = sublist1.addField('custevent_lastname', 'text', 'Last Name');
    
    var s_phone = sublist1.addField('custevent_phone', 'text', 'Phone');
    var s_Email = sublist1.addField('custevent_email', 'text', 'Email');
    
    var s_function = sublist1.addField('custevent_function', 'text', 'Function');
    //var s_Week1 = sublist1.addField('custevent_wk1', 'text', 'Week 1');
    var s_Week1 = sublist1.addField('custevent_wk1', 'text', d_end_wk_1);
    
    //var s_Week2 = sublist1.addField('custevent_wk2', 'text', 'Week 2');
    var s_Week2 = sublist1.addField('custevent_wk2', 'text', d_end_wk_2);
    
    //var s_Week3 = sublist1.addField('custevent_wk3', 'text', 'Week 3');
    var s_Week3 = sublist1.addField('custevent_wk3', 'text', d_end_wk_3);
    
    //var s_Week4 = sublist1.addField('custevent_wk4', 'text', 'Week 4');
    var s_Week4 = sublist1.addField('custevent_wk4', 'text', d_end_wk_4);
	 //var s_Week5 = sublist1.addField('custevent_wk5', 'text', 'Week 5');
    var s_Week5 = sublist1.addField('custevent_wk5', 'text', d_end_wk_5);
    
    var s_project = sublist1.addField('custevent_project', 'text', 'Project');
    var s_project_type = sublist1.addField('custevent_project_type', 'text', 'Project Type');
    
    var s_repo_man_nm = sublist1.addField('custevent_repo_man_nm', 'text', 'Reporting Manager');
    var s_repo_man_eml = sublist1.addField('custevent_repo_man_eml', 'text', 'Reporting Manager Email');
    
    var s_manager_nm = sublist1.addField('custevent_manager_nm', 'text', 'Project Manager');
    var s_manager_eml = sublist1.addField('custevent_manager_eml', 'text', 'Project Manager Email');
    
    var s_ts_approver_nm = sublist1.addField('custevent_ts_approver_nm', 'text', 'TS Approver Name');
    
    var s_ProjectState = sublist1.addField('custevent_projectstate', 'text', 'Project State');
    var s_dept1 = sublist1.addField('custevent_dept', 'text', 'Department');
    var s_dept2 = sublist1.addField('custevent_dept2', 'text', 'Executing Practice');
    
    var s_customer = sublist1.addField('custevent_customer', 'text', 'Client');
    var s_Employee_Type = sublist1.addField('custevent_employeetype', 'text', 'Employee_Type');
    
    var s_Employee_ID = sublist1.addField('custevent_employeeid', 'text', 'Employee_ID');
    var s_Person_Type = sublist1.addField('custevent_persontype', 'text', 'person_type');
    
    var s_term_date = sublist1.addField('custevent_term_date', 'text', 'term_date');
    var s_hire_date = sublist1.addField('custevent_hire_date', 'text', 'hire_date');
    s_Employee_ID.setDisplayType('hidden');
    
    var temp = 0;
    var s_id = ''
    var linenumber = 1;
    var tempInc = ''
    var currentLinenumber = 0;
    var tempdate = '';
    var middlecounteroftime = 0;
    var preveprojtocome = '';
    var preveprojtocomeline = 0;
    var prevcalposarray = new Array();
    var previcalenderweek = new Array();
    var a_lineItemarr = new Array();
    var a_result_array = new Array();
    var i_totalTime = 0;
    var Time_Bothweeksundays = new Array()
    var g_wk1 = 0;
    var g_wk2 = 0;
    var g_wk3 = 0;
    var g_wk4 = 0;
	var g_wk5 = 0;
    
    var globalArray = new Array();
    var arr = new Array();
    
    //nlapiLogExecution('DEBUG', 'setValuesublist', ' SearchEmployeeData.length =' + SearchEmployeeData.length)
    //var formatDate = formatdate()
    if (SearchEmployeeData.length != '' && SearchEmployeeData.length != null && SearchEmployeeData.length != 'undefined') //
    {
        /////////Loop through rows of search/////////////////////////////////////////////////////
        var i_empfound = '0';
        var temp_emp = '';
		var temp_project = '';
        
        var columns = SearchEmployeeData[0].getAllColumns();
        var columnLen = columns.length;
        
        for (var c = 0; c < SearchEmployeeData.length; c++) //for (var c = 0; c < 600; c++) 
        {
            var i_Calendar_Week = ''
            var s_FusionID = ''
            var i_Duration = ''
            var i_Phone = '';
            var i_Duration_decimal = 0;
            var i_First_Name = '';
            var i_Last_Name = '';
            var i_Inc_Id = '';
            var Employee_Type = ''
            var Person_Type = ''
            var Employment_Category = ''
            var e_Project = '';
            var e_Client = '';
            var i_Email = ''
            var s_ProjectState = ''
            var s_dept = ''
            var s_dept2 = ''
            var s_Customer = ''
            var i_week1 = ''
            var i_week2 = ''
            var i_week3 = ''
            var i_week4 = ''
			var i_week5 = ''
            var i_empid = ''
            var i_function = ''
            var i_project = ''
            var i_project_type = '';
            var s_repo_man_name = '';
            var s_repo_man_email = '';
            var s_manager_name = '';
            var s_manager_email = '';
            var s_ts_approver_name ='';
            var d_term_date = '';
            var d_hire_date = '';
            var column = '';
            var label = '';
            var value = '';
            var text = '';
            
            //nlapiLogExecution('DEBUG', 'c', 'c : ' + c);
            
            var a_search_employee = SearchEmployeeData[c];
            
            temp = 0; ///for every employee record looping counter is set 
            //nlapiLogExecution('DEBUG', 'setValuesublist', ' columnLen =' + columnLen)
            
            for (var hg = 0; hg < columnLen; hg++) //
            {
                //nlapiLogExecution('DEBUG', 'setValuesublist', ' hg =' + hg)
                column = columns[hg];
                //nlapiLogExecution('DEBUG', 'search_ComplaintCategory', 'column  -->' + column);
                
                label = column.getLabel();
                //nlapiLogExecution('DEBUG', 'search_ComplaintCategory', '*************************label********************  -->' + label);
                
                value = a_search_employee.getValue(column)
                text = a_search_employee.getText(column)
                //nlapiLogExecution('DEBUG', 'search_ComplaintCategory', 'value  -->' + value);
                
                if (label == 'Timesheet Date') {
                    i_Calendar_Week = value
                }
                if (label == 'Fusion ID') {
                    s_FusionID = value;
                }
                if (label == 'Phone') {
                    i_Phone = value;
                }
                if (label == 'First Name') {
                    i_First_Name = value;
                }
                if (label == 'Last Name') {
                    i_Last_Name = value;
                }
                if (label == 'IncID') {
                    i_Inc_Id = value;
                }
                if (label == 'Emp. Type') {
                    Employment_Category = text;
                }
                if (label == 'Email') {
                    i_Email = value
                }
                if (label == 'Project') {
                    e_Project = value
                }
                
                if (label == 'exe_practice') {
                    s_dept2 = text
                }
                
                if (label == 'Person Type') {
                    Person_Type = text
                }
                
                if (label == 'Department') {
                    s_Department = text;
                }
                if (label == 'Project State') {
                    s_ProjectState = text;
                    
                }
                if (label == 'Dept') {
                    s_dept = text;
                }
                
                if (label == 'Client') {
                    s_Customer = text;
                    e_Client = value;/////5205
                }
                if (label == 'Total hrs') {
                    i_Duration = value
                }
                if (label == 'Fusion ID') {
                    s_FusionID = value
                }
                if (label == 'EmpID') {
                    i_empid = value;
                }
                if (label == 'Function') {
                    i_function = text
                }
                if (label == 'Project') {
                    i_project = value;
                }
                if (label == 'project_type') {
                    i_project_type = text;
                }
                
                if (label == 'term_date') {
                    d_term_date = value;
                }
                if (label == 'hire_date') {
                    d_hire_date = value;
                }
                if (label == 'manager_name') {
                    s_manager_name = text;
                }
                if (label == 'manager_email') {
                    s_manager_email = text;
                }
                
                if (label == 'ts_approver_name') {
                	s_ts_approver_name = text;
                }
                
                if (label == 'reporting_manager') {
                    s_repo_man_name = text;
                }
                if (label == 'reporting_email') {
                    s_repo_man_email = text;
                }
                
                //nlapiLogExecution('DEBUG', 'test', 'd_term_date : ' + d_term_date);
                if (d_term_date != '' && d_term_date != null && d_term_date != 'undefined' && d_term_date != '- None -') {
                    d_term_date = d_term_date
                }
                else {
                    d_term_date = ''
                }
                
                if (d_hire_date != '' && d_hire_date != null && d_hire_date != 'undefined' && d_hire_date != '- None -') {
                    d_hire_date = d_hire_date
                }
                else {
                    d_hire_date = ''
                }
                
                if (s_repo_man_name != '' && s_repo_man_name != null && s_repo_man_name != 'undefined' && s_repo_man_name != '- None -') {
                    s_repo_man_name = s_repo_man_name
                }
                else {
                    s_repo_man_name = ''
                }
                
                if (s_repo_man_email != '' && s_repo_man_email != null && s_repo_man_email != 'undefined' && s_repo_man_email != '- None -') {
                    s_repo_man_email = s_repo_man_email
                }
                else {
                    s_repo_man_email = ''
                }
                
                if (s_manager_name != '' && s_manager_name != null && s_manager_name != 'undefined' && s_manager_name != '- None -') {
                    s_manager_name = s_manager_name
                }
                else {
                    s_manager_name = ''
                }
                
                if (s_manager_email != '' && s_manager_email != null && s_manager_email != 'undefined' && s_manager_email != '- None -') {
                    s_manager_email = s_manager_email
                }
                else {
                    s_manager_email = ''
                }             
                              

                if (s_ts_approver_name != '' && s_ts_approver_name != null && s_ts_approver_name != 'undefined' && s_ts_approver_name != '- None -') {
                	s_ts_approver_name = s_ts_approver_name
                }
                else {
                	s_ts_approver_name = ''
                }                
                
                if (s_FusionID != '' && s_FusionID != null && s_FusionID != 'undefined' && s_FusionID != '- None -') {
                    s_FusionID = s_FusionID
                }
                else {
                    s_FusionID = ''
                }
                if (i_function != '' && i_function != null && i_function != 'undefined' && i_function != '- None -') {
                    i_function = i_function
                }
                else {
                    i_function = ''
                }
                
                if (i_Phone != '' && i_Phone != null && i_Phone != 'undefined' && i_Phone != '- None -') {
                    i_Phone = i_Phone
                }
                else {
                    i_Phone = ''
                }
                
                if (i_First_Name != '' && i_First_Name != null && i_First_Name != 'undefined' && i_First_Name != '- None -') {
                    i_First_Name = i_First_Name
                }
                else {
                    i_First_Name = ''
                }
                
                if (s_dept != '' && s_dept != null && s_dept != 'undefined' && s_dept != '- None -') {
                    s_dept = s_dept
                }
                else {
                    s_dept = ''
                }
                if (s_dept2 != '' && s_dept2 != null && s_dept2 != 'undefined' && s_dept2 != '- None -') {
                    s_dept2 = s_dept2
                }
                else {
                    s_dept2 = ''
                }
                
                if (i_Inc_Id != '' && i_Inc_Id != null && i_Inc_Id != 'undefined' && i_Inc_Id != '- None -') {
                    i_Inc_Id = i_Inc_Id
                }
                else {
                    i_Inc_Id = ''
                }
                
                if (i_Email != '' && i_Email != null && i_Email != 'undefined' && i_Email != '- None -') {
                    i_Email = i_Email
                }
                else {
                    i_Email = ''
                }
                
                if (_logValidation(Employment_Category)) {
                    Employment_Category = Employment_Category
                }
                else {
                    Employment_Category = ''
                }
                
                if (s_ProjectState != '' && s_ProjectState != null && s_ProjectState != 'undefined' && s_ProjectState != '- None -') {
                    s_ProjectState = s_ProjectState
                }
                else {
                    s_ProjectState = ''
                }
                if (s_Customer != '' && s_Customer != null && s_Customer != 'undefined' && s_Customer != '- None -') {
                    s_Customer = s_Customer
                }
                else {
                    s_Customer = ''
                }
                if (i_week1 != '' && i_week1 != null && i_week1 != 'undefined' && i_week1 != '- None -') {
                    i_week1 = i_week1
                }
                else {
                    i_week1 = ''
                }
                if (i_week2 != '' && i_week2 != null && i_week2 != 'undefined' && i_week2 != '- None -') {
                    i_week2 = i_week2
                }
                else {
                    i_week2 = ''
                }
                if (i_week3 != '' && i_week3 != null && i_week3 != 'undefined') {
                    i_week3 = i_week3
                }
                else {
                    i_week3 = ''
                }
                if (i_week4 != '' && i_week4 != null && i_week4 != 'undefined') {
                    i_week4 = i_week4
                }
                else {
                    i_week4 = ''
                }
				
				  if (i_week5 != '' && i_week5 != null && i_week5 != 'undefined') {
                    i_week5 = i_week5
                }
                else {
                    i_week5 = ''
                }
                
            } /////for loop of columns of employee search......
            //nlapiLogExecution('DEBUG', 'i_empid', 'i_empid : ' + i_empid);
            
            //continue;
			
			if(i_empid == 140619)
			{
				nlapiLogExecution('DEBUG', 'temp_emp', 'temp_emp : ' + temp_emp);
				nlapiLogExecution('DEBUG', 'i_empid', 'i_empid : ' + i_empid);
				
			    nlapiLogExecution('DEBUG', 'temp_project', 'temp_project : ' + temp_project);
				nlapiLogExecution('DEBUG', 'i_project', 'i_project : ' + i_project);
			}
			
			
            
            if (temp_emp == i_empid && temp_project == i_project) // if employee is already entered in report
            {
                continue;
            }
            
            if (c == 0) //
            {
                temp_emp = i_empid;
				temp_project == i_project;
            }
            
            //var all_columns = timesheetdata_WK_1[0].getAllColumns();
            //nlapiLogExecution('DEBUG', 'i_empid', '*** Processing Data 1 ***');
            if (timesheetdata_WK_1 != null && timesheetdata_WK_1 != '' && timesheetdata_WK_1 != 'undefined') //
            {
                //nlapiLogExecution('DEBUG', 'i_empid', 'g_wk1 : ' + g_wk1);
                var result_wk1 = '';
                var wk1_emp = '';
                var wk1_duration = '' ;
				var wk1_project = '' ;
                for (var wk1 = g_wk1; wk1 < timesheetdata_WK_1.length; wk1++) //
                {
                    result_wk1 = timesheetdata_WK_1[wk1];
                    //var all_columns = result_wk1.getAllColumns();
                    wk1_emp = result_wk1.split('##')[0]; //result_wk1.getValue(all_columns[1]);
                    //nlapiLogExecution('DEBUG', 'wk4_emp', 'wk1_emp : ' + wk1_emp);
                    
					wk1_duration = result_wk1.split('##')[1];
					
					wk1_project = result_wk1.split('##')[2];
					
					

							if (wk1_duration != '' && wk1_duration != null && wk1_duration != 'undefined') {
							wk1_duration = wk1_duration
							 }
							else {
								wk1_duration = ''
							}
							
							if (wk1_project != '' && wk1_project != null && wk1_project != 'undefined') {
							wk1_project = wk1_project
							 }
							else {
								wk1_project = ''
							}
							
							
					
                    //if (i_empid >= wk1_emp) //
                    {
                        if (i_empid == wk1_emp && i_project == wk1_project) //
                        {
                            //nlapiLogExecution('DEBUG', 'wk4_emp', 'wk1_emp : ' + wk1_emp);
                            i_week1 = wk1_duration ;
                            //i_project = result_wk1.getValue(all_columns[3]);
                            //s_manager_name = result_wk1.getValue(all_columns[4]);
                            //s_manager_email = result_wk1.getValue(all_columns[5]);
                            g_wk1 = wk1;
                            break;
                        }
                    }
                    if (parseInt(wk1_emp) > parseInt(i_empid) && parseInt(wk1_project) > parseInt(i_project) ) //
                    {
                        //g_wk1 = wk1;
                        break;
                    }
                }
            }
            
            //nlapiLogExecution('DEBUG', 'i_empid', '*** Processing Data 2 ***');
            if (timesheetdata_WK_2 != null && timesheetdata_WK_2 != '' && timesheetdata_WK_2 != 'undefined') //
            {
				var wk2_duration = '' ;
				var wk2_project = '';
                //var all_columns = timesheetdata_WK_2[0].getAllColumns();
                //nlapiLogExecution('DEBUG', 'i_empid', 'g_wk2 : ' + g_wk2);
                
                for (var wk2 = g_wk2; wk2 < timesheetdata_WK_2.length; wk2++) //
                {
                    var result_wk2 = timesheetdata_WK_2[wk2];
                    //var all_columns = result_wk2.getAllColumns();
                    var wk2_emp = result_wk2.split('##')[0]; //result_wk2.getValue(all_columns[1]);
                    //nlapiLogExecution('DEBUG', 'wk4_emp', 'wk2_emp : ' + wk2_emp);
                    
					wk2_duration = result_wk2.split('##')[1];
					
					wk2_project = result_wk2.split('##')[2];
					
							if (wk2_duration != '' && wk2_duration != null && wk2_duration != 'undefined') {
							wk2_duration = wk2_duration
							 }
							else {
								wk2_duration = ''
							}
					
					
					if (wk2_project != '' && wk2_project != null && wk2_project != 'undefined') {
							wk2_project = wk2_project
							 }
							else {
								wk2_project = ''
							}
							
		/*
		if(i_empid == 140619)
		{
			nlapiLogExecution('DEBUG', 'i_empid', 'i_empid : ' + i_empid);
			nlapiLogExecution('DEBUG', 'i_project', 'i_project : ' + i_project);
			nlapiLogExecution('DEBUG', 'wk2_emp', 'wk2_emp : ' + wk2_emp);
			nlapiLogExecution('DEBUG', 'wk2_project', 'wk2_project : ' + wk2_project);
			//nlapiLogExecution('DEBUG', 'SearchTimeData_New', 'i : ' + i + ' temp_emp ==' + temp_emp + 'project ==' +a_search_empdata.getValue(all_columns[3]));
		}
		*/				
							
							
                    //if (i_empid >= wk2_emp) //
                    {
                        if (i_empid == wk2_emp && i_project == wk2_project) //
                        {
                            //nlapiLogExecution('DEBUG', 'wk4_emp', 'wk2_emp : ' + wk2_emp);
                            i_week2 = wk2_duration;
                            //i_project = result_wk2.getValue(all_columns[3]);
                            //s_manager_name = result_wk2.getValue(all_columns[4]);
                            //s_manager_email = result_wk2.getValue(all_columns[5]);
                            g_wk2 = wk2;
                            break;
                        }
                    }
                    if (parseInt(wk2_emp) > parseInt(i_empid)  && parseInt(wk2_project) > parseInt(i_project)) //
                    {
                        //g_wk2 = wk2;
                        break;
                    }
                }
            }
            
            //nlapiLogExecution('DEBUG', 'i_empid', '*** Processing Data 3 ***');						
            if (timesheetdata_WK_3 != null && timesheetdata_WK_3 != '' && timesheetdata_WK_3 != 'undefined') //
            {
				var wk3_duration = '' ;
				var wk3_project = '' ;
                //var all_columns = timesheetdata_WK_3[0].getAllColumns();
                //nlapiLogExecution('DEBUG', 'i_empid', 'g_wk3 : ' + g_wk3);
                
                for (var wk3 = g_wk3; wk3 < timesheetdata_WK_3.length; wk3++) //
                {
                    var result_wk3 = timesheetdata_WK_3[wk3];
                    
                    var wk3_emp = result_wk3.split('##')[0]; //result_wk3.getValue(all_columns[1]);
                    //nlapiLogExecution('DEBUG', 'wk4_emp', 'wk3_emp : ' + wk3_emp);
                    
					
					wk3_duration = result_wk3.split('##')[1]; 
					
					wk3_project = result_wk3.split('##')[2];
										
					
					if (wk3_duration != '' && wk3_duration != null && wk3_duration != 'undefined') {
							wk3_duration = wk3_duration
							 }
							else {
								wk3_duration = ''
							}
					
					
					if (wk3_project != '' && wk3_project != null && wk3_project != 'undefined') {
							wk3_project = wk3_project
							 }
							else {
								wk3_project = ''
							}
					
					
                    //if (i_empid >= wk3_emp) //
                    {
                        if (i_empid == wk3_emp && i_project == wk3_project) //
                        {
                            //nlapiLogExecution('DEBUG', 'wk4_emp', 'wk3_emp : ' + wk3_emp);
                            i_week3 = wk3_duration ;
                            //i_project = result_wk3.getValue(all_columns[3]);
                            //s_manager_name = result_wk3.getValue(all_columns[4]);
                            //s_manager_email = result_wk3.getValue(all_columns[5]);
                            g_wk3 = wk3;
                            break;
                        }
                    }
                    if (parseInt(wk3_emp) > parseInt(i_empid) && parseInt(wk3_project) > parseInt(i_project)) //
                    {
                        //g_wk3 = wk3;
                        break;
                    }
                }
            }
            
            //nlapiLogExecution('DEBUG', 'i_empid', '*** Processing Data 4 ***');
            if (timesheetdata_WK_4 != null && timesheetdata_WK_4 != '' && timesheetdata_WK_4 != 'undefined') //
            {
				var wk4_duration = '' ;
				var wk4_project = '' ;
                //var all_columns = timesheetdata_WK_4[0].getAllColumns();
                //nlapiLogExecution('DEBUG', 'i_empid', 'g_wk4 : ' + g_wk4);
                
                for (var wk4 = g_wk4; wk4 < timesheetdata_WK_4.length; wk4++) //
                {
                    var result_wk4 = timesheetdata_WK_4[wk4];
                    var wk4_emp = result_wk4.split('##')[0]; //result_wk4.getValue(all_columns[1]);
                    //nlapiLogExecution('DEBUG', 'wk4_emp', 'wk4_emp : ' + wk4_emp);
                    
					wk4_duration = result_wk4.split('##')[1]; 
					
					wk4_project = result_wk4.split('##')[2]; 
					
					if (wk4_duration != '' && wk4_duration != null && wk4_duration != 'undefined') {
					wk4_duration = wk4_duration
					 }
					else {
						wk4_duration = ''
					}
					
					if (wk4_project != '' && wk4_project != null && wk4_project != 'undefined') {
					wk4_project = wk4_project
					 }
					else {
						wk4_project = ''
					}
					
                    //if (i_empid >= wk4_emp) //
                    {
                        if (i_empid == wk4_emp && i_project == wk4_project) //
                        {
                            //nlapiLogExecution('DEBUG', 'wk4_emp', 'wk4_emp : ' + wk4_emp);
                            i_week4 = wk4_duration;
                            //i_project = result_wk4.getValue(all_columns[3]);
                            //s_manager_name = result_wk4.getValue(all_columns[4]);
                            //s_manager_email = result_wk4.getValue(all_columns[5]);
                            g_wk4 = wk4;
                            break;
                        }
                    }
                    if (parseInt(wk4_emp) > parseInt(i_empid) && parseInt(wk4_project) > parseInt(i_project)) //
                    {
                        //g_wk4 = wk4;
                        break;
                    }
                }
            }
              if (timesheetdata_WK_5 != null && timesheetdata_WK_5 != '' && timesheetdata_WK_5 != 'undefined') //
            {
				var wk5_duration = '' ;
				var wk5_project = '' ;
                //var all_columns = timesheetdata_WK_4[0].getAllColumns();
                //nlapiLogExecution('DEBUG', 'i_empid', 'g_wk4 : ' + g_wk4);
                
                for (var wk5 = g_wk5; wk5 < timesheetdata_WK_5.length; wk5++) //
                {
                    var result_wk5 = timesheetdata_WK_5[wk5];
                    var wk5_emp = result_wk5.split('##')[0]; //result_wk4.getValue(all_columns[1]);
                    //nlapiLogExecution('DEBUG', 'wk4_emp', 'wk4_emp : ' + wk4_emp);
                    
					wk5_duration = result_wk5.split('##')[1];

					wk5_project	= result_wk5.split('##')[2];				
					
							
							if (wk5_duration != '' && wk5_duration != null && wk5_duration != 'undefined') {
							wk5_duration = wk5_duration
							 }
							else {
								wk5_duration = ''
							}
							
							if (wk5_project != '' && wk5_project != null && wk5_project != 'undefined') {
							wk5_project = wk5_project
							 }
							else {
								wk5_project = ''
							}
	/*						
	if(i_empid == 140619)
	{
		nlapiLogExecution('DEBUG', 'i_empid', 'i_empid : ' + i_empid);
		nlapiLogExecution('DEBUG', 'i_project', 'i_project : ' + i_project);
		nlapiLogExecution('DEBUG', 'wk5_emp', 'wk5_emp : ' + wk5_emp);
		nlapiLogExecution('DEBUG', 'wk5_project', 'wk5_project : ' + wk5_project);
		//nlapiLogExecution('DEBUG', 'SearchTimeData_New', 'i : ' + i + ' temp_emp ==' + temp_emp + 'project ==' +a_search_empdata.getValue(all_columns[3]));
	}
	*/				
					
                    //if (i_empid >= wk4_emp) //
                    {
                        if (i_empid == wk5_emp && i_project == wk5_project) //
                        {
                            //nlapiLogExecution('DEBUG', 'wk4_emp', 'wk4_emp : ' + wk4_emp);
                            i_week5 = wk5_duration;
                            //i_project = result_wk4.getValue(all_columns[3]);
                            //s_manager_name = result_wk4.getValue(all_columns[4]);
                            //s_manager_email = result_wk4.getValue(all_columns[5]);
                            g_wk5 = wk5;
                            break;
                        }
                    }
                    if (parseInt(wk5_emp) > parseInt(i_empid) && parseInt(wk5_project) > parseInt(i_project)) //
                    {
                        //g_wk4 = wk4;
                        break;
                    }
                }
            }
			
			
			
            /*
             if (i_week1 == 'Filled' && i_week2 == 'Filled' && i_week3 == 'Filled' && i_week4 == 'Filled') // if employee has entered time in all weeks then skip it from report
             {
             continue;
             }
             */
            sublist1.setLineItemValue('custevent_gciid', linenumber, i_Inc_Id);
            sublist1.setLineItemValue('custevent_fusid', linenumber, s_FusionID);
            //nlapiLogExecution('DEBUG', 'Test', 's_FusionID : ' + s_FusionID);
            
            sublist1.setLineItemValue('custevent_employeeid', linenumber, i_empid);
            //nlapiLogExecution('DEBUG', 'Test', 'i_empid : ' + i_empid);
            
            sublist1.setLineItemValue('custevent_persontype', linenumber, Person_Type);
            
            sublist1.setLineItemValue('custevent_firstname', linenumber, i_First_Name);
            //nlapiLogExecution('DEBUG', 'Test', 'i_First_Name : ' + i_First_Name);
            
            sublist1.setLineItemValue('custevent_lastname', linenumber, i_Last_Name);
            sublist1.setLineItemValue('custevent_phone', linenumber, i_Phone);
            sublist1.setLineItemValue('custevent_email', linenumber, i_Email);
            sublist1.setLineItemValue('custevent_function', linenumber, i_function);
            
            sublist1.setLineItemValue('custevent_wk1', linenumber, i_week1);
            //nlapiLogExecution('DEBUG', 'Test', 'i_week1 : ' + i_week1);
            
            sublist1.setLineItemValue('custevent_wk2', linenumber, i_week2);
            //nlapiLogExecution('DEBUG', 'Test', 'i_week2 : ' + i_week2);
            
            sublist1.setLineItemValue('custevent_wk3', linenumber, i_week3);
            //nlapiLogExecution('DEBUG', 'Test', 'i_week3 : ' + i_week3);
            
            sublist1.setLineItemValue('custevent_wk4', linenumber, i_week4);
            //nlapiLogExecution('DEBUG', 'Test', 'i_week4 : ' + i_week4);
			    sublist1.setLineItemValue('custevent_wk5', linenumber, i_week5);
            //nlapiLogExecution('DEBUG', 'Test', 'i_week5 : ' + i_week5);
            
            sublist1.setLineItemValue('custevent_project', linenumber, i_project);
            sublist1.setLineItemValue('custevent_project_type', linenumber, i_project_type);
            
            for (var ii = 0; ii < manager_email_Array.length; ii++) //
            {
                var str = manager_email_Array[ii];
                
                if (str.indexOf(s_manager_name) >= 0) //
                {
                    s_manager_email = str.split("##")[1];
                    break;
                }
            }
            
            for (var ij = 0; ij < manager_email_Array.length; ij++) //
            {
                var str = manager_email_Array[ij];
                
                if (str.indexOf(s_repo_man_name) >= 0) //
                {
                    s_repo_man_email = str.split("##")[1];
                    break;
                }
            }
            
            sublist1.setLineItemValue('custevent_manager_nm', linenumber, s_manager_name);
            sublist1.setLineItemValue('custevent_manager_eml', linenumber, s_manager_email);
            sublist1.setLineItemValue('custevent_ts_approver_name', linenumber, s_ts_approver_name);
            
            sublist1.setLineItemValue('custevent_repo_man_nm', linenumber, s_repo_man_name);
            sublist1.setLineItemValue('custevent_repo_man_eml', linenumber, s_repo_man_email);
            sublist1.setLineItemValue('custevent_projectstate', linenumber, s_ProjectState);
            sublist1.setLineItemValue('custevent_dept', linenumber, s_dept);
            sublist1.setLineItemValue('custevent_dept2', linenumber, s_dept2);
            sublist1.setLineItemValue('custevent_customer', linenumber, s_Customer);
            sublist1.setLineItemValue('custevent_employeetype', linenumber, Employment_Category);
            sublist1.setLineItemValue('custevent_term_date', linenumber, d_term_date);
            sublist1.setLineItemValue('custevent_hire_date', linenumber, d_hire_date);
            
            linenumber++;
			
            temp_project = i_project;
            temp_emp = i_empid;
            //return;
            
            if (c == 0) //
            {
                //arr[i] = "GCI ID,Fusion Id,First Name,Last Name,Phone,Email,Function,Week1,Week2,Week3,Week4,Project,Manager Name,Manager Email,Project State,Department,Executing Practice,Client,Employee Type,Person Type,Termination Date" + "\n" 
                //+ gciid + "," + fusid + "," + firstname + "," + lastname + "," + phone + "," + email + "," + function1 + "," + week1 + "," + week2 + "," 
                //+ week3 + "," + week4 + "," + project + "," + projectstate + "," + dept + "," + customer + "," + employeeType + "," + personType + "," 
                //+ term_date;
                
                arr[c] = "GCI ID,Fusion Id,First Name,Last Name,Phone,Email,Function," + d_end_wk_1 + "," + d_end_wk_2 + "," + d_end_wk_3 + "," + d_end_wk_4 +"," + d_end_wk_5 +
                ",Project,Project Type,Project Manager Name,Project Manager Email,Reporting Manager Name,Reporting Manager Email,Time Sheet Approver,Project State,Department,Executing Practice,Client,Employee Type,Person Type,Termination Date,Hire Date" +
                "\n" +
                i_Inc_Id +
                "," +
                s_FusionID +
                "," +
                i_First_Name +
                "," +
                i_Last_Name +
                "," +
                i_Phone +
                "," +
                i_Email +
                "," +
                i_function +
                "," +
                i_week1 +
                "," +
                i_week2 +
                "," +
                i_week3 +
                "," +
                i_week4 +
                "," +
				i_week5 +
                "," +
                i_project +
                "," +
                i_project_type +
                "," +
                s_manager_name +
                "," +
                s_manager_email +
                "," +
                s_repo_man_name +
                "," +
                s_repo_man_email +
                "," +
                s_ts_approver_name +
                "," +
                s_ProjectState +
                "," +
                s_dept +
                "," +
                s_dept2 +
                "," +
                s_Customer +
                "," +
                Employment_Category +
                "," +
                Person_Type +
                "," +
                d_term_date +
                "," +
                d_hire_date;
            }
            else //
            {
                arr[c] = "\n" + i_Inc_Id + "," + s_FusionID + "," + i_First_Name + "," + i_Last_Name + "," + i_Phone + "," + i_Email + "," +
                i_function +
                "," +
                i_week1 +
                "," +
                i_week2 +
                "," +
                i_week3 +
                "," +
                i_week4 +
                "," +
			    i_week5 +
                "," +
                i_project +
                "," +
                i_project_type +
                "," +
                s_manager_name +
                "," +
                s_manager_email +
                "," +
                s_repo_man_name +
                "," +
                s_repo_man_email +
                "," +
                s_ts_approver_name +
                "," +
                s_ProjectState +
                "," +
                s_dept +
                "," +
                s_dept2 +
                "," +
                s_Customer +
                "," +
                Employment_Category +
                "," +
                Person_Type +
                "," +
                d_term_date +
                "," +
                d_hire_date;
            }
            //nlapiLogExecution('DEBUG', 'beforeLoad_CalculateWeek', 'arr[i]=' + arr[i]);
            
            globalArray[c] = (arr[c]);
            
            continue;
            
        }////end of for loop of employee
        var fileName = 'Not Submitted Report for GDM & Corporate'
        var Datetime = new Date();
        var CSVName = fileName + " - " + Datetime + '.csv';
        var file = nlapiCreateFile(CSVName, 'CSV', globalArray.toString());
        
        response.setContentType('CSV', CSVName);
        response.write(file.getValue());
        
    }///end of null values check for employee array
    //nlapiLogExecution('DEBUG', 'SearchData', 'a_result_array===' + a_result_array.length)
    return a_result_array;
}

function calculatepositionTimeweek(i_TimeCalendarWeek, calculateposition)//
{
    //i_TimeCalendarWeek = i_TimeCalendarWeek.toString();
    //declare flag
    //check if date is present in array
    //if equal to then set flag=1
    //check flag =1 return 0
    
    var i_nextweekdate = ''
    //i_TimeCalendarWeek = nlapiStringToDate(i_TimeCalendarWeek)
    //i_nextweekdate = nlapiAddDays(i_TimeCalendarWeek, 7)
    //i_nextweekdate = nlapiDateToString(i_nextweekdate)
    
    for (b = 0; calculateposition != null && b < calculateposition.length; b++) {
        var SplitPOSArray = new Array();
        SplitPOSArray = calculateposition[b].split('##');
        //nlapiLogExecution('DEBUG', 'SearchData', 'SplitPOSArray[0]===' + SplitPOSArray[0] + '==b==' + b)
        if (SplitPOSArray[0] == i_TimeCalendarWeek) {
            calculateposition.splice(b, 1);
            //return SplitPOSArray[1];
            //break;
        }
        else {
            //return null;
            //break;
        }
    }
    //nlapiLogExecution('DEBUG', 'calculatepositionTimeweek', 'calculateposition ===' + calculateposition)
}

function SearchEmployeeData(s_startdate, s_enddate) //
{
    var e_column = new Array();
    var e_filters = new Array();
    var LastfetchedInternalid_T = 0;
    var searchresultFinal_T = new Array();
    var searchresult_T = new Array();
    
    do //
    {
        // Filters for Employee search
        //e_filters[e_filters.length] = new nlobjSearchFilter('startdate', 'resourceallocation', 'notafter', s_enddate); ///is not after 9/28.14
        //e_filters[e_filters.length] = new nlobjSearchFilter('enddate', 'resourceallocation', 'notbefore', s_startdate); ////is not before 1sept
        //e_filters[e_filters.length] = new nlobjSearchFilter('internalidnumber', null, 'greaterthan', LastfetchedInternalid_T); ////is not before 1sept
        
        // Filters for resource allocation search
        
        e_filters[e_filters.length] = new nlobjSearchFilter('startdate', null, 'notafter', s_enddate);
        e_filters[e_filters.length] = new nlobjSearchFilter('enddate', null, 'notbefore', s_startdate);
		e_filters[e_filters.length] = new nlobjSearchFilter('customer', null, 'anyof', 116496);
		//e_filters[e_filters.length] = new nlobjSearchFilter('location', null, 'anyof', 45);
        e_filters[e_filters.length] = new nlobjSearchFilter('internalidnumber', 'employee', 'greaterthan', LastfetchedInternalid_T); ////is not before 1sept
      // e_filters[e_filters.length] = new nlobjSearchFilter('subsidiary', 'employee', 'anyof', i_Subsidiary); // setting subsidairy
     //   e_filters[e_filters.length] = new nlobjSearchFilter('internalid', 'employee', 'anyof', 140619); 
        //searchresult_T = nlapiSearchRecord('employee', 'customsearch_not_submitted_all_employe_2', e_filters, null);
        //searchresult_T = nlapiSearchRecord('resourceallocation', 'customsearch_non_submitted_ts_by_allocat', e_filters, null);
        
		searchresult_T = nlapiSearchRecord('resourceallocation', 'customsearch_non_subm_ts_for_mngr_g_corp', e_filters, null);//customsearch_non_subm_ts_for_mngr_g_corp
        
        if (searchresult_T != null && searchresult_T != undefined && searchresult_T != '') //
        {
            var temp_emp = '';
		//	var temp_project = '';
            var emp = '';
		//	var project = '';
			
            //nlapiLogExecution('DEBUG', 'researchcodes', 'searchresult_T==' + searchresult_T.length);
            
            for (var i = 0; i < searchresult_T.length; i++) //
            {
                var a_search_empdata = searchresult_T[i];
                var all_columns = a_search_empdata.getAllColumns();
                emp = a_search_empdata.getValue(all_columns[0]);
			//	project = a_search_empdata.getValue(all_columns[7]);
                //nlapiLogExecution('DEBUG', 'SearchTimeData', 'i : ' + i + ' emp ==' + emp);
                
                if (emp != null) //
                {
                    if (i == 0) //
                    {
                       // temp_emp = emp;
                    }
			/*		
					if(emp == 140619)
					{
						nlapiLogExecution('DEBUG', 'SearchData', 'i : ' + i + ' temp_emp ==' + temp_emp);
						nlapiLogExecution('DEBUG', 'SearchData', 'i : ' + i + ' emp ==' + emp);
						
						nlapiLogExecution('DEBUG', 'SearchData', 'i : ' + i + ' project ==' + temp_project);
						nlapiLogExecution('DEBUG', 'SearchData', 'i : ' + i + ' project ==' + project);
					}
            */        
                    LastfetchedInternalid_T = temp_emp;
                    //nlapiLogExecution('DEBUG', 'SearchData', 'i : ' + i + ' temp_emp ==' + temp_emp);
                    if (emp == temp_emp )//&& project == temp_project) //if employee id is same then add to final array
                    {
                        searchresultFinal_T.push(searchresult_T[i]);
                    }
                    else //else employee id does nt match
                    {
                        //nlapiLogExecution('DEBUG', 'SearchData', 'Emp change row number : ' + i);
                        if (i > 950)//
                        {
                            //nlapiLogExecution('DEBUG', 'SearchData', 'i==' + i);
                            //nlapiLogExecution('DEBUG', 'SearchData', 'LastfetchedInternalid_T==' + LastfetchedInternalid_T);
                            break;
                        }
                        else //
                        {
                            searchresultFinal_T.push(searchresult_T[i]);
                            temp_emp = emp;
						//	temp_project = project;
                            LastfetchedInternalid_T = temp_emp;
                        }
                    }
                }
                //searchresultFinal.push(searchresult[i]);
                //nlapiLogExecution('DEBUG', 'researchcodes', 'lastid==' + a_search_empdata.getValue('internalid', null, 'group'));
            }
            //nlapiLogExecution('DEBUG', 'researchcodes', 'lastid==' + a_search_empdata.getValue('internalid', null, 'group'));
            //LastfetchedInternalid_T = searchresult[searchresult.length - 1].getValue('internalid', null, 'group');
            //nlapiLogExecution('DEBUG', 'researchcodes', 'lastis==' + LastfetchedInternalid_T);
        }
    }
    while (searchresult_T != null && searchresult_T != undefined && searchresult_T != '');

    return searchresultFinal_T;
}

function SearchTimeData(s_startdate, s_enddate) //
{
    var LastfetchedInternalid_T = 0;
    var searchresultFinal_T = new Array();
    var searchresult_T = new Array();
    
    do //
    {
        var a_filters = new Array();
        a_filters[0] = new nlobjSearchFilter('date', null, 'onorbefore', s_enddate);
        a_filters[1] = new nlobjSearchFilter('date', null, 'onorafter', s_startdate);
        a_filters[2] = new nlobjSearchFilter('internalidnumber', 'employee', 'greaterthan', LastfetchedInternalid_T);////is not before 1sept
        var searchresult_T = nlapiSearchRecord('timeentry', 'customsearch_sal_non_std_rptts_gdm_corp', a_filters, null);
        
        if (searchresult_T != null && searchresult_T != undefined && searchresult_T != '') //
        {
            //nlapiLogExecution('DEBUG', 'researchcodes', 'searchresult_T == ' + searchresult_T.length);
            var temp_emp = '';
            var emp = '';
            
            for (var i = 0; i < searchresult_T.length; i++) //
            {
                var a_search_empdata = searchresult_T[i];
                var all_columns = a_search_empdata.getAllColumns();
                emp = a_search_empdata.getValue(all_columns[2]);
                //nlapiLogExecution('DEBUG', 'SearchTimeData', 'i : ' + i + ' emp ==' + emp);
                
                if (emp != null) //
                {
                    if (i == 0) //
                    {
                        temp_emp = emp;
                    }
                    
                    LastfetchedInternalid_T = temp_emp;
                    //nlapiLogExecution('DEBUG', 'SearchData', 'i : ' + i + ' temp_emp ==' + temp_emp);
                    if (emp == temp_emp) //if employee id is same then add to final array
                    {
                        searchresultFinal_T.push(searchresult_T[i]);
                    }
                    else //else employee id does nt match
                    {
                        //nlapiLogExecution('DEBUG', 'SearchData', 'Emp change row number : ' + i);
                        if (i > 950)//
                        {
                            //nlapiLogExecution('DEBUG', 'SearchData', 'i==' + i);
                            //nlapiLogExecution('DEBUG', 'SearchData', 'LastfetchedInternalid_T==' + LastfetchedInternalid_T);
                            break;
                        }
                        else //
                        {
                            searchresultFinal_T.push(searchresult_T[i]);
                            temp_emp = emp;
                            LastfetchedInternalid_T = temp_emp;
                        }
                    }
                }
                //searchresultFinal_T.push(searchresult_T[i]);
                //nlapiLogExecution('DEBUG', 'researchcodes', 'lastid==' + a_search_empdata.getValue('internalid', null, 'group'));
            }
            //nlapiLogExecution('DEBUG', 'researchcodes', 'lastid==' + a_search_empdata.getValue('internalid', null, 'group'));
            //LastfetchedInternalid_T = searchresult_T[searchresult_T.length - 1].getValue('internalid', 'employee', 'group');
            //nlapiLogExecution('DEBUG', 'researchcodes', 'LastfetchedInternalid_T==' + LastfetchedInternalid_T);
        }
        //var searchresult_T = nlapiSearchRecord('timebill', 'customsearch_salnonstdrptts_3_all', a_filters, null);
        //return searchresult_T
    }
    while (searchresult_T != null && searchresult_T != undefined && searchresult_T != '');

    return searchresultFinal_T;
    
}

function SearchTimeData_New(s_startdate, s_enddate) //
{
    var LastfetchedInternalid_T = 0;
    var searchresultFinal_T = new Array();
    var searchresult_T = new Array();
    
    do //
    {
        var a_filters = new Array();
        
        a_filters[a_filters.length] = new nlobjSearchFilter('date', null, 'onorafter', s_startdate);
        a_filters[a_filters.length] = new nlobjSearchFilter('date', null, 'onorbefore', s_enddate);
        
        a_filters[a_filters.length] = new nlobjSearchFilter('internalidnumber', 'employee', 'greaterthan', LastfetchedInternalid_T);////is not before 1sept
        //a_filters[a_filters.length] = new nlobjSearchFilter('subsidiary', null, 'anyof', i_Subsidiary); // selected subsidiary
        //nlapiLogExecution('DEBUG', 'SearchTimeData_New', 'LastfetchedInternalid_T == ' + LastfetchedInternalid_T);
        //nlapiLogExecution('DEBUG', 'SearchTimeData_New', 'i_Subsidiary == ' + i_Subsidiary);
        
        var searchresult_T = nlapiSearchRecord('timebill', 'customsearch_non_sub_ts_for_', a_filters, null);
        
        if (searchresult_T != null && searchresult_T != undefined && searchresult_T != '') //
        {
            //nlapiLogExecution('DEBUG', 'SearchTimeData_New', 'searchresult_T == ' + searchresult_T.length);
            
            var temp_emp = '';
            var emp = '';
            var a_search_empdata = searchresult_T[0];
            var all_columns = a_search_empdata.getAllColumns();
			var emp_total_hr = '';
            
            for (var i = 0; i < searchresult_T.length; i++) //
            {
                a_search_empdata = searchresult_T[i];
                emp = a_search_empdata.getValue(all_columns[1]); // fetch emp id from search result
              	emp_total_hr = a_search_empdata.getValue(all_columns[0]);

			  //nlapiLogExecution('DEBUG', 'SearchTimeData_New', 'i : ' + i + ' emp ==' + emp);
                
                if (emp != null) //
                {
                    if (i == 0) //
                    {
                        temp_emp = emp;
                    }
                    
                    var array_string = '';
                    LastfetchedInternalid_T = temp_emp;
                    //nlapiLogExecution('DEBUG', 'SearchTimeData_New', 'i : ' + i + ' temp_emp ==' + temp_emp);
                    if (emp == temp_emp) //if employee id is same then add to final array
                    {
                        array_string = emp + '##' + emp_total_hr + '##'+ a_search_empdata.getValue(all_columns[3]);
                        searchresultFinal_T.push(array_string);
                        /*
						if(emp == 140619)
						{
							nlapiLogExecution('DEBUG', 'SearchTimeData_New', 'i : ' + i + ' temp_emp ==' + temp_emp + 'project ==' +a_search_empdata.getValue(all_columns[3]));
						}
						*/
                        //searchresultFinal_T.push(searchresult_T[i]);
                    }
                    else //else employee id does nt match
                    {
                        //nlapiLogExecution('DEBUG', 'SearchTimeData_New', 'Emp change row number : ' + i);
                        if (i > 950)//
                        {
                            //nlapiLogExecution('DEBUG', 'SearchTimeData_New', 'i==' + i);
                            //nlapiLogExecution('DEBUG', 'SearchTimeData_New', 'LastfetchedInternalid_T==' + LastfetchedInternalid_T);
                            break;
                        }
                        else //
                        {
                            array_string = emp + '##' + emp_total_hr + '##'+ a_search_empdata.getValue(all_columns[3]);
                            searchresultFinal_T.push(array_string);
							/*
							if(emp == 140619)
							{
							nlapiLogExecution('DEBUG', 'SearchTimeData_New', 'i : ' + i + ' temp_emp ==' + temp_emp + 'project ==' +a_search_empdata.getValue(all_columns[3]));
							}
                            */
                            //searchresultFinal_T.push(searchresult_T[i]);
                            temp_emp = emp;
                            LastfetchedInternalid_T = temp_emp;
                        }
                    }
                }
                //searchresultFinal_T.push(searchresult_T[i]);
                //nlapiLogExecution('DEBUG', 'SearchTimeData_New', 'lastid==' + a_search_empdata.getValue('internalid', null, 'group'));
            }
            //nlapiLogExecution('DEBUG', 'SearchTimeData_New', 'lastid==' + a_search_empdata.getValue('internalid', null, 'group'));
            //LastfetchedInternalid_T = searchresult_T[searchresult_T.length - 1].getValue('internalid', 'employee', 'group');
            //nlapiLogExecution('DEBUG', 'SearchTimeData_New', 'LastfetchedInternalid_T==' + LastfetchedInternalid_T);
        }
        //var searchresult_T = nlapiSearchRecord('timebill', 'customsearch_salnonstdrptts_3_all', a_filters, null);
        //return searchresult_T
    }
    while (searchresult_T != null && searchresult_T != undefined && searchresult_T != '');

    
    return searchresultFinal_T;
}

var manager_email_Array = new Array();

function get_Manager_Email() //
{
    var i_initial_id = 0;
    var search_result = null;
    var i_array_counter = 0;
    
    do //
    {
        var filters = new Array();
        filters[filters.length] = new nlobjSearchFilter('internalidnumber', null, 'greaterthan', i_initial_id);
        //nlapiLogExecution('DEBUG', 'get_Manager_Email', 'i_initial_id : ' + i_initial_id);
        
        search_result = nlapiSearchRecord('job', 'customsearch_manger_with_email_gdm_corp', filters, null);
        
        if (_logValidation(search_result)) //
        {
            var all_columns = search_result[0].getAllColumns();
            var result = '';
            
            for (var i = 0; i < search_result.length; i++) //
            {
                result = search_result[i];
                
                manager_email_Array[i_array_counter] = result.getValue(all_columns[1]) + "##" + result.getValue(all_columns[2]);
                i_initial_id = result.getValue(all_columns[0]);
                i_array_counter++;
            }
        }
    }
    while (search_result != null && search_result != '' && search_result != 'undefined') //
    {
    
    }
}

/*
 Function name :-getWeekNumber()
 Parameter :- date
 return type :- returns week number of given year
 */
function getWeekNumber(d){
    //Copy date so don't modify original
    d = new Date(+d);
    d.setHours(0, 0, 0);
    //Set to nearest Thursday: current date + 4 - current day number
    //Make Sunday's day number 7
    d.setDate(d.getDate() + 4 - (d.getDay() || 7));
    //Get first day of year
    var yearStart = new Date(d.getFullYear(), 0, 1);
    //Calculate full weeks to nearest Thursday
    var weekNo = Math.ceil((((d - yearStart) / 86400000) + 1) / 7)
    //nlapiLogExecution('DEBUG', 'beforeLoad_CalculateWeek', 'weekNo=' + weekNo);
    //Return array of year and week number
    return weekNo;
}

function Callsuitelet(sublist1, param) //
{
    nlapiLogExecution('DEBUG', 'Callsuitelet', '*** Into Callsuitelet ***');
    var count = sublist1.getLineItemCount();
    //alert(count);
    //nlapiLogExecution('DEBUG', 'Callsuitelet', 'param' + param);
    nlapiLogExecution('DEBUG', 'Callsuitelet', 'count' + count);
    //nlapiLogExecution('DEBUG', 'Callsuitelet', 'linenumber' + linenumber);
    var globalArray = new Array();
    var arr = new Array();
    
    var gciid = '';
    var fusid = '';
    var firstname = '';
    var lastname = '';
    var phone = '';
    var email = '';
    var function1 = '';
    var week1 = '';
    var week2 = '';
    var week3 = '';
    var week4 = '';
	var week5 = '';
    var project = '';
    var project_type = '';
    var manager_name = '';
    var manager_email = '';
    var repo_man_name = '';
    var repo_man_email = '';
    var projectstate = '';
    var employeeType = '';
    var personType = '';
    var dept = '';
    var dept2 = '';
    var customer = '';
    var term_date = '';
    var hire_date = '';
    for (var i = 1; i <= count; i++) //
    {
        //nlapiLogExecution('DEBUG', 'Callsuitelet', '*** Into Callsuitelet loop (' + i + ') ***');
        
        gciid = sublist1.getLineItemValue('custevent_gciid', i);
        fusid = sublist1.getLineItemValue('custevent_fusid', i);
        firstname = sublist1.getLineItemValue('custevent_firstname', i);
        lastname = sublist1.getLineItemValue('custevent_lastname', i);
        phone = sublist1.getLineItemValue('custevent_phone', i);
        email = sublist1.getLineItemValue('custevent_email', i);
        function1 = sublist1.getLineItemValue('custevent_function', i);
        week1 = sublist1.getLineItemValue('custevent_wk1', i);
        week2 = sublist1.getLineItemValue('custevent_wk2', i);
        week3 = sublist1.getLineItemValue('custevent_wk3', i);
        week4 = sublist1.getLineItemValue('custevent_wk4', i);
		week5 = sublist1.getLineItemValue('custevent_wk5', i);
        project = sublist1.getLineItemValue('custevent_project', i);
        project_type = sublist1.getLineItemValue('custevent_project_type', i);
        manager_name = sublist1.getLineItemValue('custevent_manager_nm', i);
        manager_email = sublist1.getLineItemValue('custevent_manager_eml', i);
        repo_man_name = sublist1.getLineItemValue('custevent_repo_man_nm', i);
        repo_man_email = sublist1.getLineItemValue('custevent_repo_man_eml', i);
        projectstate = sublist1.getLineItemValue('custevent_projectstate', i);
        employeeType = sublist1.getLineItemValue('custevent_employeetype', i);
        personType = sublist1.getLineItemValue('custevent_persontype', i);
        dept = sublist1.getLineItemValue('custevent_dept', i);
        dept2 = sublist1.getLineItemValue('custevent_dept2', i);
        customer = sublist1.getLineItemValue('custevent_customer', i);
        term_date = sublist1.getLineItemValue('custevent_term_date', i);
        hire_date = sublist1.getLineItemValue('custevent_hire_date', i);
        customer = customer.toString().replace(/[,]/g, " ")
        if (gciid != null && gciid != '' && gciid != 'undefined') {
            gciid = gciid
        }
        else {
            gciid = ''
        }
        
        if (project != null && project != '' && project != 'undefined') {
            project = replace_Comma(project)
            project = project
            //nlapiLogExecution('DEBUG', 'Test', '0 project : ' + project);
        }
        else {
            project = ''
        }
        if (dept != null && dept != '' && dept != 'undefined') {
            dept = replace_Comma(dept)
            dept = dept
        }
        else {
            dept = ''
        }
        if (dept2 != null && dept2 != '' && dept2 != 'undefined') {
            dept2 = replace_Comma(dept2)
            dept2 = dept2
        }
        else {
            dept2 = ''
        }
        if (customer != null && customer != '' && customer != 'undefined') {
            customer = replace_Comma(customer)
            customer = customer
        }
        else {
            customer = ''
        }
        
        if (project_type != null && project_type != '' && project_type != 'undefined') {
            project_type = project_type
        }
        else {
            project_type = ''
        }
        
        if (repo_man_name != null && repo_man_name != '' && repo_man_name != 'undefined') {
            repo_man_name = repo_man_name
        }
        else {
            repo_man_name = ''
        }
        
        if (repo_man_email != null && repo_man_email != '' && repo_man_email != 'undefined') {
            repo_man_email = repo_man_email
        }
        else {
            repo_man_email = ''
        }
        
        if (function1 != null && function1 != '' && function1 != 'undefined') {
            function1 = function1
        }
        else {
            function1 = ''
        }
        if (fusid != null && fusid != '' && fusid != 'undefined') {
            fusid = fusid
        }
        else {
            fusid = ''
        }
        if (firstname != null && firstname != '' && firstname != 'undefined') {
            firstname = firstname
        }
        else {
            firstname = ''
        }
        if (lastname != null && lastname != '' && lastname != 'undefined') {
            lastname = lastname
        }
        else {
            lastname = ''
        }
        if (phone != null && phone != '' && phone != 'undefined') {
            phone = phone
        }
        else {
            phone = ''
        }
        if (email != null && email != '' && email != 'undefined') {
            email = email
        }
        else {
            email = ''
        }
        if (week1 != null && week1 != '' && week1 != 'undefined') {
            week1 = week1
        }
        else {
            week1 = ''
        }
        if (week2 != null && week2 != '' && week2 != 'undefined') {
            week2 = week2
        }
        else {
            week2 = ''
        }
        if (week3 != null && week3 != '' && week3 != 'undefined') {
            week3 = week3
        }
        else {
            week3 = ''
        }
        if (week4 != null && week4 != '' && week4 != 'undefined') {
            week4 = week4
        }
        else {
            week4 = ''
        }
		 if (week5 != null && week5 != '' && week5 != 'undefined') {
            week5 = week5
        }
        else {
            week5 = ''
        }
        if (projectstate != null && projectstate != '' && projectstate != 'undefined') {
            projectstate = projectstate
        }
        else {
            projectstate = ''
        }
        
        if (employeeType != null && employeeType != '' && employeeType != 'undefined') {
            employeeType = employeeType
        }
        else {
            employeeType = ''
        }
        if (personType != null && personType != '' && personType != 'undefined') {
            personType = personType
        }
        else {
            personType = ''
        }
        if (term_date != null && term_date != '' && term_date != 'undefined') {
            term_date = term_date
        }
        else {
            term_date = ''
        }
        if (hire_date != null && hire_date != '' && hire_date != 'undefined') {
            hire_date = hire_date
        }
        else {
            hire_date = ''
        }
        
        if (i == 1) //
        {
            //arr[i] = "GCI ID,Fusion Id,First Name,Last Name,Phone,Email,Function,Week1,Week2,Week3,Week4,Project,Manager Name,Manager Email,Project State,Department,Executing Practice,Client,Employee Type,Person Type,Termination Date" + "\n" + gciid + "," + fusid + "," + firstname + "," + lastname + "," + phone + "," + email + "," + function1 + "," + week1 + "," + week2 + "," + week3 + "," + week4 + "," + project + "," + projectstate + "," + dept + "," + customer + "," + employeeType + "," + personType + "," + term_date;
            arr[i] = "GCI ID,Fusion Id,First Name,Last Name,Phone,Email,Function," + d_end_wk_1 + "," + d_end_wk_2 + "," + d_end_wk_3 + "," + d_end_wk_4 +","+ d_end_wk_5+ ","+ ",Project,Project Type,Project Manager Name,Project Manager Email,Reporting Manager Name,Reporting Manager Email,Project State,Department,Executing Practice,Client,Employee Type,Person Type,Termination Date,Hire Date" + "\n" + gciid + "," + fusid + "," + firstname + "," + lastname + "," + phone + "," + email + "," + function1 + "," + week1 + "," + week2 + "," + week3 + "," + week4 + "," +","+ week5 +","+ project + "," + project_type + "," + manager_name + "," + manager_email + "," + repo_man_name + "," + repo_man_email + "," + projectstate + "," + dept + "," + dept2 + "," + customer + "," + employeeType + "," + personType + "," + term_date + "," + hire_date;
        }
        else //
        {
            arr[i] = "\n" + gciid + "," + fusid + "," + firstname + "," + lastname + "," + phone + "," + email + "," + function1 + "," + week1 + "," + week2 + "," + week3 + "," + week4 + "," +  week5 + "," + project + "," + project_type + "," + manager_name + "," + manager_email + "," + repo_man_name + "," + repo_man_email + "," + projectstate + "," + dept + "," + dept2 + "," + customer + "," + employeeType + "," + personType + "," + term_date + "," + hire_date;
        }
        //nlapiLogExecution('DEBUG', 'beforeLoad_CalculateWeek', 'arr[i]=' + arr[i]);
        
        globalArray[i - 1] = (arr[i]);
    }
    
    var fileName = 'Not Submitted Report'
    var Datetime = new Date();
    var CSVName = fileName + " - " + Datetime + '.csv';
    var file = nlapiCreateFile(CSVName, 'CSV', globalArray.toString());
    
    response.setContentType('CSV', CSVName);
    response.write(file.getValue());
    
}

function replace_Comma(value) //
{
    for (var i = 0; i < value.length; i++) //
    {
        if (value.charAt(i) == ',') //
        {
            value = value.replace(',', '.');
            //nlapiLogExecution('DEBUG', 'myIndexOf', 'Value : ' + value + ' i : ' + i + ' value.length : ' + value.length);
            //return i;
        }
    }
    return value;
}

function calculateSundays(s_startdate, s_enddate){
    var sundayarray = new Array();
    //nlapiLogExecution('DEBUG', 'suitlet_Po_ClosedStatus', ' s_startdate-->' + s_startdate);
    //////s_startdate= s_enddate
    var date = nlapiStringToDate(s_startdate)
    //nlapiLogExecution('DEBUG', 'suitlet_Po_ClosedStatus', ' date-->' + date);
    var day = date.getDay()
    //nlapiLogExecution('DEBUG', 'suitlet_Po_ClosedStatus', ' day-->' + day);
    var addday = getdays(day)
    //nlapiLogExecution('DEBUG', 'suitlet_Po_ClosedStatus', ' addday-->' + addday);
    
    for (var i = 0; i < 5; i++) {
        s_startdate = nlapiStringToDate(s_startdate)
        if (i == 0) {
        
            s_startdate = nlapiAddDays(s_startdate, addday)
            s_startdate = nlapiDateToString(s_startdate)
            sundayarray.push(s_startdate)
        }
        else {
            s_startdate = nlapiAddDays(s_startdate, 7)
            s_startdate = nlapiDateToString(s_startdate)
            sundayarray.push(s_startdate)
        }
    }
    
    //nlapiLogExecution('DEBUG', 'suitlet_Po_ClosedStatus', ' sundayarray-->' + sundayarray);
    return sundayarray;
}

function Cal_Timesheetdate(s_startdate, i_calenderweek){
    //nlapiLogExecution('DEBUG', 'suitlet_Po_ClosedStatus', ' s_startdate-->' + s_startdate);
    //nlapiLogExecution('DEBUG', 'suitlet_Po_ClosedStatus', ' i_calenderweek-->' + i_calenderweek);
    var i_timsheetdate = ''
    i_calenderweek = nlapiStringToDate(i_calenderweek)
    i_timsheetdate = nlapiAddDays(i_calenderweek, 7)
    i_timsheetdate = nlapiDateToString(i_timsheetdate)
    return i_timsheetdate;
    
    
}

function Sundayswithweekposition(s_startdate, s_enddate){
    //nlapiLogExecution('DEBUG', 'suitlet_Po_ClosedStatus', ' s_startdate-->' + s_startdate);
    var position = new Array();
    
    var date = nlapiStringToDate(s_startdate)
    //nlapiLogExecution('DEBUG', 'suitlet_Po_ClosedStatus', ' date-->' + date);
    var day = date.getDay()
    //nlapiLogExecution('DEBUG', 'suitlet_Po_ClosedStatus', ' day-->' + day);
    var addday = getdays(day)
    //nlapiLogExecution('DEBUG', 'suitlet_Po_ClosedStatus', ' addday-->' + addday);
    for (var i = 0; i < 5; i++) {
        s_startdate = nlapiStringToDate(s_startdate)
        if (i == 0) {
        
            s_startdate = nlapiAddDays(s_startdate, addday)
            s_startdate = nlapiDateToString(s_startdate)
            
            position.push(s_startdate + "##" + (i + 1))
            //position.push(s_startdate + "##" + (4 - i))
        }
        else {
            s_startdate = nlapiAddDays(s_startdate, 7)
            s_startdate = nlapiDateToString(s_startdate)
            position.push(s_startdate + "##" + (i + 1))
            //position.push(s_startdate + "##" + (4 - i))
        }
    }
    
    //nlapiLogExecution('DEBUG', 'suitlet_Po_ClosedStatus', ' position-->' + position);
    return position;
}

function getdays(day){
    //nlapiLogExecution('DEBUG', 'suitlet_Po_ClosedStatus', ' day-->' + day);
    var x = '';
    switch (day) {
        case 0:
            x = "7";
            break;
        case 1:
            x = "6";
            break;
        case 2:
            x = "5";
            break;
        case 3:
            x = "4";
            break;
        case 4:
            x = "3";
            break;
        case 5:
            x = "2";
            break;
        case 6:
            x = "1";
            break;
            
    }
    //nlapiLogExecution('DEBUG', 'suitlet_Po_ClosedStatus', ' x-->' + x);
    return x;
}

function EmployeeSearch(){


    var a_filters = new Array();
    
    
    
    var searchresult = nlapiSearchRecord('employee', 'customsearch_non_sub_emp_rpt_gdm_corp', a_filters, null);
    return searchresult
    
}

function _logValidation(value){
    if (value != null && value != '' && value != undefined && value.toString() != 'NaN' && value != NaN) {
        return true;
    }
    else {
        return false;
    }
}
