/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       09 Aug 2017     bidhi.raj
 *
 */

/**
 * @param {nlobjRequest} request Request object
 * @param {nlobjResponse} response Response object
 * @returns {Void} Any output is written via response object
 */

function GenerateHourlyReportnew(request, response){

    try {
        var a_employeearray = new Array();
        if (request.getMethod() == 'GET') {
            var s_period = '';
            var form = nlapiCreateForm('Hourly report from TS with No Control');
            var oldvalues = false;
            var flag = 0;
            form.setScript('customscript_cshourly_report')
            var contextObj = nlapiGetContext();
            //nlapiLogExecution('DEBUG', 'GET', ' contextObj===>' + contextObj)
            
            var beginUsage = contextObj.getRemainingUsage();
            //nlapiLogExecution('DEBUG', 'Search Results', 'beginUsage :' + beginUsage);
            
            //var select = form.addField('custpage_startdate', 'Date', 'Start Date :');  //Period End Date
            var select = form.addField('custpage_startdate', 'Date', 'Period End Date :'); //
            //var s_beforedate = request.getParameter('custpage_startdate');
          var s_beforedate = '8/12/2017';
            var s_afterdate = request.getParameter('custpage_fromdate');///why this code?
            //nlapiLogExecution('DEBUG', 'Search results ', ' s_afterdate =' + s_afterdate)
            var sublist1 = form.addSubList('record', 'list', 'Hourly report from TS');
            var id = request.getParameter('id');
            //nlapiLogExecution('DEBUG', 'Post results ', ' id=' + id)
            if (id == 'Export') {
                var param = request.getParameter('param');
                //nlapiLogExecution('DEBUG', 'Post results ', ' param=' + param)
                
                var datesplit = param.toString().split('@')
                //nlapiLogExecution('DEBUG', 'Post results ', ' datesplit=' + datesplit)
                
                s_beforedate = datesplit[0]
                //nlapiLogExecution('DEBUG', 'Post results ', ' s_beforedate=' + s_beforedate)
                s_afterdate = datesplit[1]
                //nlapiLogExecution('DEBUG', 'Post results ', ' s_beforedate=' + s_beforedate)
            
            }
            
            if (s_beforedate == null) {
                oldvalues = false;
            }
            else {
                oldvalues = true;
            }
            
            
            if (s_afterdate == null) {
                oldvalues = false;
            }
            else {
            
                oldvalues = true;
            }
            //nlapiLogExecution('DEBUG', 'GET', ' oldvalues===>' + oldvalues)
            /////////////////////////////////////POST ON SUBMIT ////////////////////////////
            if (oldvalues) {
                form.addButton('custombutton', 'Export As CSV', 'fxn_generateHourPDF_Dec(\'' + s_beforedate + "@" + s_afterdate + '\');');
                
                var id = request.getParameter('id');
                //nlapiLogExecution('DEBUG', 'Post results ', ' id=' + id)
                if (id == 'Export') {
                    var param = request.getParameter('param');
                    //nlapiLogExecution('DEBUG', 'Post results ', ' param=' + param)
                    
                    var datesplit = param.toString().split('@')
                    //nlapiLogExecution('DEBUG', 'Post results ', ' datesplit=' + datesplit)
                    
                    s_beforedate = datesplit[0]
                    s_afterdate = datesplit[1]
                }
                
                var searchData = SearchData(s_beforedate, s_afterdate)
                nlapiLogExecution('DEBUG', 'Post results ', ' searchData=' + searchData.length)
                
                var SearchDTDetailresult = SearchDTData(s_beforedate, s_afterdate)
                nlapiLogExecution('DEBUG', 'Post results ', ' SearchDTDetailresult=' + SearchDTDetailresult.length)
                
                var weeklyDTTotal = getDTTotalsweekly(sublist1, SearchDTDetailresult, s_afterdate)
                //nlapiLogExecution('DEBUG', 'Post results ', ' weeklyDTTotal=' + weeklyDTTotal.length)
                
                var data = setValuesublist(sublist1, searchData, s_afterdate, weeklyDTTotal, s_beforedate)
                
            }
            //nlapiLogExecution('DEBUG', 'Post results ', ' id' + id)
            if (oldvalues) {
                select.setDefaultValue(s_beforedate);
            }
            
            if (id != 'Export') {
                form.addSubmitButton('Submit');
                response.writePage(form);
            }
            else {
                Callsuitelet(sublist1, param);
            }
        }
        else {
            var date = request.getParameter('custpage_startdate');
            //nlapiLogExecution('DEBUG', 'Post results ', ' date =' + date)
            var date1 = nlapiStringToDate(date)
            //nlapiLogExecution('DEBUG', 'Post results ', ' date after string to date1 =' + date1)
            var fromDate = nlapiAddDays(date1, -13)
            //nlapiLogExecution('DEBUG', 'Post results ', ' fromDate =' + fromDate)
            fromDate = nlapiDateToString(fromDate)
            //nlapiLogExecution('DEBUG', 'Post results ', ' fromDate after conversion =' + fromDate)
            
            
            var params = new Array();
            params['custpage_startdate'] = date;
            params['custpage_fromdate'] = fromDate;
            nlapiSetRedirectURL('SUITELET', 'customscript_hourlyrprtfrmtmentry', 'customdeployhourlyreportfrmtimeentry', false, params);
            //nlapiSetRedirectURL('SUITELET', 'customscriptsut_report_salnonstdrptfrom', 'customdeploy1', false, params);
        }
    } 
    catch (e) {
        nlapiLogExecution('ERROR', 'Search results ', ' value of e-====== **************** =' + e)
    }
}

function setValuesublist(sublist1, searchData, s_afterdate1, weeklyDTTotal, s_beforedate) //
{
    try //
    {
    	// Get memo data
    	var a_memo_data = searchMemoFields(s_beforedate, s_afterdate1);
    	
        var s_file = sublist1.addField('custevent_file', 'text', 'File #');
		var s_GCI = sublist1.addField('custevent_gci', 'text', 'GCI ID');
        var s_firstName = sublist1.addField('custevent_firstname', 'text', 'First Name');
        var s_lastName = sublist1.addField('custevent_lastname', 'text', 'Last Name');
        var s_IfNonExempt = sublist1.addField('custevent_ifnonexempt', 'text', 'If Non Exempt');
        var s_ProjectCity = sublist1.addField('custevent_projectcity', 'text', 'Project_City');
        var s_ProjectLocation = sublist1.addField('custevent_projectloc', 'text', 'Location');
        var s_WK_ST = sublist1.addField('custevent_wkst1', 'text', 'WK1 ST');
        var s_WK_OT = sublist1.addField('custevent_wkot1', 'text', 'WK1 OT');
        var s_WK_DT = sublist1.addField('custevent_wkdt1', 'text', 'WK1 DT');
        var s_WK2_ST = sublist1.addField('custevent_wkst2', 'text', 'WK2 ST');
        var s_WK2_OT = sublist1.addField('custevent_wkot2', 'text', 'WK2 OT');
        var s_WK2_DT = sublist1.addField('custevent_wkdt2', 'text', 'WK2 DT');
        var s_reg_hours = sublist1.addField('custevent_reghours', 'text', 'Reg Hours');
        var s_ot_hours = sublist1.addField('custevent_othours', 'text', 'OT Hours');
        var s_ratecode = sublist1.addField('custevent_ratecode', 'text', 'Rate Code');
        var s_Employee_Type = sublist1.addField('custevent_employeetype', 'text', 'Employee_Type');
        var s_Department = sublist1.addField('custevent_department', 'textarea', 'Department');
        var s_User_Notes = sublist1.addField('custevent_usernotes', 'textarea', 'User_Notes');
        var s_id = ''
        var linenumber = 1;
        var tempInc = ''
        var currentLinenumber = 0;
        var tempdate = '';
        var includerecord = ''
        s_WK_ST.setDefaultValue('0');
        /////////////get stardateweekno
        s_afterdate1 = nlapiStringToDate(s_afterdate1);
        //************** pass the date as parameter to getWeekNumber function *****************//
        var startdateweek = getWeekNumber(s_afterdate1)
        //nlapiLogExecution('DEBUG', 'beforeLoad_CalculateWeek', 'startdateweek=' + startdateweek);
        //////////////////////////////////////
        
        
        
        var a_result_array = new Array();
        var i_totalTime = 0;
        var i_week1_st = 0;
        var i_week1_lv = 0;
        var i_week1_hl = 0;
        var i_week1_fl = 0;
        
        var i_week2_st = 0;
        var i_week2_lv = 0;
        var i_week2_hl = 0;
        var i_week2_fl = 0;
        var dtsetvalue = false;
        
        if (searchData.length != '' && searchData.length != null && searchData.length != 'undefined') //
        {
            /////////Loop through rows of search/////////////////////////////////////////////////////
            for (var c = 0; c < searchData.length; c++) //
            {
                var i_Calendar_Week = ''
                var i_Employee = ''
                var i_Duration = ''
                var i_Service_Item_Name = '';
                var i_Duration_decimal = 0;
                var i_EmpIntID = ''
				var i_emp_inc_id = ''
                var i_Employee_First_Name = '';
                var i_Employee_Last_Name = '';
                var i_EmployeeIFExempt = '';
                var Employee_Type = ''
                var Employment_Category = ''
                var i_note = ''
                var i_department = ''
                var i_projectcity = ''
                var i_ProjectLocation = ''
                var i_Dayofweek = '';
                var a_search_transaction_result = searchData[c];
                var columns = a_search_transaction_result.getAllColumns();
                var columnLen = columns.length;
                
                
                for (var hg = 0; hg < columnLen; hg++) //
                {
                    var column = columns[hg];
                    var label = column.getLabel();
                    var value = a_search_transaction_result.getValue(column)
                    var text = a_search_transaction_result.getText(column)
                    
                    if (label == 'Calendar Week') {
                        i_Calendar_Week = value
                    }
                    if (label == 'Employee') {
                        i_Employee = value;
                    }
                    if (label == 'Service Item') {
                        i_Service_Item_Name = text;
                    }
                    if (label == 'Employee : First Name') {
                        i_Employee_First_Name = value;
                    }
                    if (label == 'Employee : Last Name') {
                        i_Employee_Last_Name = value;
                    }
                    if (label == 'IF Exempt') {
                        i_EmployeeIFExempt = text;
                    }
                    if (label == 'Employee : Employment Category') {
                        Employment_Category = value;
                    }
                    if (label == 'User_Notes') {
                        i_note = value
                    }
                    if (label == 'Department') {
                        i_department = text;
                    }
                    if (label == 'Project Location') {
                        i_ProjectLocation = text;
                        
                    }
                    if (label == 'Project City') {
                        i_projectcity = text;
                    }
                    
                    if (label == 'Emp IntID') {
                        s_id = value;
                    }
                    if (label == 'Duration') {
                        i_Duration_decimal = value
                    }
                    if (label == 'Emp ID') {
                        i_EmpIntID = value
                    }
					if (label == 'incid') {
                        i_emp_inc_id = value
                    }
                    if (label == 'Dayofweek') {/////Added for sunday hrs
                        i_Dayofweek = value
                    }
                }////end of for loop of columns
                if (i_Service_Item_Name != '' && i_Service_Item_Name != null && i_Service_Item_Name != 'undefined') {
                    i_Service_Item_Name = i_Service_Item_Name
                }
                else {
                    i_Service_Item_Name = ''
                }
                if (i_Employee_First_Name != '' && i_Employee_First_Name != null && i_Employee_First_Name != 'undefined') {
                    i_Employee_First_Name = i_Employee_First_Name
                }
                else {
                    i_Employee_First_Name = ''
                }
                if (i_Employee_Last_Name != '' && i_Employee_Last_Name != null && i_Employee_Last_Name != 'undefined') {
                    i_Employee_Last_Name = i_Employee_Last_Name
                }
                else {
                    i_Employee_Last_Name = ''
                }
                
                if (i_EmployeeIFExempt != '' && i_EmployeeIFExempt != null && i_EmployeeIFExempt != 'undefined') {
                    i_EmployeeIFExempt = i_EmployeeIFExempt
                }
                else {
                    i_EmployeeIFExempt = ''
                }
                if (Employment_Category != '' && Employment_Category != null && Employment_Category != 'undefined' && Employment_Category != '- None -') {
                    Employment_Category = Employment_Category
                }
                else {
                    Employment_Category = ''
                }
                if (i_note != '' && i_note != null && i_note != 'undefined' && i_note != '- None -') {
                    i_note = i_note
                }
                else {
                    i_note = ''
                }
                if (Employee_Type != '' && Employee_Type != null && Employee_Type != 'undefined' && Employee_Type != '- None -') {
                    Employee_Type = Employee_Type
                }
                else {
                    Employee_Type = ''
                }
                
                if (i_EmpIntID != '' && i_EmpIntID != null && i_EmpIntID != 'undefined') {
                    i_EmpIntID = i_EmpIntID
                }
                else {
                    i_EmpIntID = ''
                }
                if (i_Duration_decimal != '' && i_Duration_decimal != null && i_Duration_decimal != 'undefined') {
                    i_Duration_decimal = i_Duration_decimal
                }
                else {
                    i_Duration_decimal = 0;
                }
                if (i_projectcity != '' && i_projectcity != null && i_projectcity != 'undefined' && i_projectcity != '- None -') {
                    i_projectcity = i_projectcity
                }
                else {
                    i_projectcity = ''
                }
                if (i_ProjectLocation != '' && i_ProjectLocation != null && i_ProjectLocation != 'undefined') {
                    i_ProjectLocation = i_ProjectLocation
                }
                else {
                    i_ProjectLocation = ''
                }
                
                if (i_department != '' && i_department != null && i_department != 'undefined') {
                    i_department = i_department
                }
                else {
                    i_department = ''
                }
                
                
                
                
                //////////////////Uppercase conversion of taksnames for comparison to get week hrs as all ids per project are different/////////////
                i_Service_Item_Name = i_Service_Item_Name.toString().toUpperCase();
                
                var s_OTTask = "OT"
                var a_STTask = [("ST").toUpperCase(), ("FP").toUpperCase(), ("Bench Project").toUpperCase(), ("Internal Projects").toUpperCase()];
                var s_LeaveTask = "Leave"
                var s_HolidayTask = "Holiday"
                var s_FHTask = "Floating Holiday"
                var s_Exempt = "Exempt"
                
                s_OTTask = s_OTTask.toString().toUpperCase();
                //s_STTask = s_STTask.toString().toUpperCase();
                s_LeaveTask = s_LeaveTask.toString().toUpperCase();
                s_HolidayTask = s_HolidayTask.toString().toUpperCase();
                s_FHTask = s_FHTask.toString().toUpperCase();
                s_Exempt = s_Exempt.toString().toUpperCase();
                
                /////calculate current date week/////////////////////
                if (i_Calendar_Week != s_beforedate) //
                {
                    i_Calendar_Week = nlapiStringToDate(i_Calendar_Week);
                    i_Calendar_Week = nlapiAddDays(i_Calendar_Week, 1)
                }
                //************** pass the date as parameter to getWeekNumber function *****************//
                var currentdateweek = getWeekNumber(i_Calendar_Week)
                
                
                
                ////**************Calculate the week of the Dayofweek column to get if there is any sunday filled time sheets with not matching weekno.
                /////Added for sunday hrs/////////////////////////////////////////////////
                var i_Calendar_Week_Dayofweekdate = nlapiStringToDate(i_Dayofweek);
                var Dayofweekno = getWeekNumber(i_Calendar_Week_Dayofweekdate);
                //nlapiLogExecution('DEBUG', 'SearchData', 'i_Duration_decimal===' + i_Duration_decimal)
                /////////////////////////////new code added to consider sunday also in same week missed in search/////////////////////////////////////
                
                
                var defaultvalue = 0.0;
                
                //nlapiLogExecution('DEBUG', 'SearchData', 'tempInc=== ' + tempInc + " ===sid===== " + s_id)
                
                if (tempInc != s_id) //if employee is diff
                {
                
                    i_totalTime = 0;
                    i_week1_st = 0;
                    i_week2_st = 0;
                    var excludeValues = searchexcludeValues(s_id, searchData, startdateweek, currentdateweek, s_beforedate, c)
                    //nlapiLogExecution('DEBUG', 'SearchData', 'excludeValues===' + excludeValues)
                    
                    /*
                     if (!excludeValues) //if this line is not to be included then take s_id
                     {
                     includerecord = s_id
                     }
                     */
                    ////////////////Line will be included according control added///
                    ///if (excludeValues) //
                    ///{
                    if (tempInc != '') //
                    {
                        var week1DT = sublist1.getLineItemValue('custevent_wkdt1', currentLinenumber);
                        if (isNaN(week1DT)) //
                        {
                            week1DT = 0;
                        }
                        
                        var week2DT = sublist1.getLineItemValue('custevent_wkdt2', currentLinenumber);
                        if (isNaN(week2DT)) //
                        {
                            week2DT = 0;
                        }
                        
                        var DTTotal = parseInt(week1DT) + parseInt(week2DT)
                        
                        var OTtotal = sublist1.getLineItemValue('custevent_othours', currentLinenumber);
                        if (isNaN(OTtotal) || OTtotal == null || OTtotal == '' || OTtotal == 'undefined') //
                        {
                            OTtotal = 0;
                        }
                        
                        var isnonexempt = sublist1.getLineItemValue('custevent_ifnonexempt', currentLinenumber);
                        var st1 = sublist1.getLineItemValue('custevent_wkst1', currentLinenumber);
                        if (isNaN(st1) || st1 == null || st1 == '' || st1 == 'undefined') //
                        {
                            st1 = 0;
                        }
                        
                        var st2 = sublist1.getLineItemValue('custevent_wkst2', currentLinenumber);
                        if (isNaN(st2) || st2 == null || st2 == '' || st2 == 'undefined') //
                        {
                            st2 = 0;
                        }
                        
                        var STTotal = parseFloat(st1) + parseFloat(st2)
                        //nlapiLogExecution('DEBUG', 'SearchData', 'tempInc : ' + tempInc + ' s_id : '+ s_id)
                        //nlapiLogExecution('DEBUG', 'SearchData', 'st1 : ' + st1+ ' st2 : '+ st2)
                        //nlapiLogExecution('DEBUG', 'SearchData', 'STTotal : ' + STTotal)
                        
                        var file = sublist1.getLineItemValue('custevent_file', currentLinenumber);
						var gci = sublist1.getLineItemValue('custevent_gci', currentLinenumber);
                        var firstname = sublist1.getLineItemValue('custevent_firstname', currentLinenumber);
                        var lastname = sublist1.getLineItemValue('custevent_lastname', currentLinenumber);
                        
                        if (isnonexempt == 'NonE') //None exempt conditions
                        {
                            if (OTtotal > 0) //
                            {
                                sublist1.setLineItemValue('custevent_othours', currentLinenumber, '');
                                sublist1.setLineItemValue('custevent_othours', linenumber, OTtotal);
                                sublist1.setLineItemValue('custevent_file', linenumber, file);
								sublist1.setLineItemValue('custevent_gci', linenumber, gci);
                                sublist1.setLineItemValue('custevent_firstname', linenumber, firstname);
                                sublist1.setLineItemValue('custevent_lastname', linenumber, lastname);
                                sublist1.setLineItemValue('custevent_ifnonexempt', linenumber, isnonexempt);
                                sublist1.setLineItemValue('custevent_ratecode', linenumber, '2');
                                
                                var reghours = parseFloat(STTotal) + parseFloat(OTtotal)
                                //nlapiLogExecution('DEBUG', 'SearchData', 'reghours ===' + reghours)
                                sublist1.setLineItemValue('custevent_othours', linenumber, OTtotal);
                                
                                if (isNaN(reghours)) //
                                {
                                    reghours = '';
                                }
                                
                                ///sublist1.setLineItemValue('custevent_reghours', linenumber, reghours);
                                
                                linenumber++;
                            }
                            else //OT total is zero
                            {
                                var REGtotal_1 = sublist1.getLineItemValue('custevent_reghours', linenumber);
                                if (isNaN(REGtotal_1) || REGtotal_1 == null || REGtotal_1 == '' || REGtotal_1 == 'undefined') //
                                {
                                    REGtotal_1 = 0;
                                }
                                
                                
                                var OTtotal_1 = sublist1.getLineItemValue('custevent_othours', linenumber);
                                if (isNaN(OTtotal_1) || OTtotal_1 == null || OTtotal_1 == '' || OTtotal_1 == 'undefined') //
                                {
                                    OTtotal_1 = 0;
                                }
                                
                                
                                //nlapiLogExecution('DEBUG', 'SearchData', 'else linenumber ===' + linenumber)
                                //nlapiLogExecution('DEBUG', 'SearchData', 'else currentLinenumber ===' + currentLinenumber)
                                
                                sublist1.setLineItemValue('custevent_othours', linenumber, '');
                                var reghours_1 = parseFloat(REGtotal_1) + parseFloat(OTtotal_1)
                                //nlapiLogExecution('DEBUG', 'SearchData', 'else reghours_1 ===' + reghours_1)
                                sublist1.setLineItemValue('custevent_reghours', linenumber, reghours_1);
                            }
                        }///end of non exempt block
                        else //For Exempt Employees -- we do not want Rate code 2 if employee work OT. We need record as below
                        {
                            if (OTtotal == null || OTtotal == '' || OTtotal == 'undefined') //isNaN(OTtotal) || 
                            {
                                OTtotal = 0;
                            }
                            if (STTotal == null || STTotal == '' || STTotal == 'undefined') //isNaN(STTotal) || 
                            {
                                STTotal = 0;
                            }
                            
                            //nlapiLogExecution('DEBUG', 'SearchData', 'STTotal : ' + STTotal + ' OTtotal : ' + OTtotal)
                            
                            //var reghours = parseFloat(STTotal) + parseFloat(OTtotal)
                            if (OTtotal > 0) //Add OT hrs in st1 +st2 reg hrs
                            {
                                var reghours = parseFloat(STTotal) + parseFloat(OTtotal)
                                //nlapiLogExecution('DEBUG', 'SearchData', 'reghours ===' + reghours)
                                sublist1.setLineItemValue('custevent_othours', currentLinenumber, '');
                                
                                if (isNaN(reghours)) //
                                {
                                    reghours = '';
                                }
                                sublist1.setLineItemValue('custevent_reghours', currentLinenumber, reghours);
                            }
                            
                        }
                        if (DTTotal > 0) //
                        {
                        
                            sublist1.setLineItemValue('custevent_othours', linenumber, DTTotal);
                            sublist1.setLineItemValue('custevent_ratecode', linenumber, '3');
                            linenumber++;
                        }
                    }/////end of tempinc!=
                    ////Set a new line for previous employee, if the next employee id is diff 
                    sublist1.setLineItemValue('custevent_file', linenumber, i_EmpIntID);
					sublist1.setLineItemValue('custevent_gci', linenumber, i_emp_inc_id);
                    sublist1.setLineItemValue('custevent_firstname', linenumber, i_Employee_First_Name);
                    sublist1.setLineItemValue('custevent_lastname', linenumber, i_Employee_Last_Name);
                    var i_nonExempt = Employment_Category.toString().search(/Non Exempt/);
                    if (i_nonExempt != -1) //
                    {
                        sublist1.setLineItemValue('custevent_ifnonexempt', linenumber, 'NonE');
                    }
                    else //
                    {
                        sublist1.setLineItemValue('custevent_ifnonexempt', linenumber, '');
                    }
                    sublist1.setLineItemValue('custevent_projectcity', linenumber, i_projectcity);
                    sublist1.setLineItemValue('custevent_projectloc', linenumber, i_ProjectLocation);
                    sublist1.setLineItemValue('custevent_employeetype', linenumber, Employment_Category);
                    sublist1.setLineItemValue('custevent_department', linenumber, i_department);
                    sublist1.setLineItemValue('custevent_usernotes', linenumber, a_memo_data[i_Employee] == undefined?'':a_memo_data[i_Employee]);//i_note.toString());
                    
                    
                    sublist1.setLineItemValue('custevent_wkst1', linenumber, '');
                    sublist1.setLineItemValue('custevent_wkot1', linenumber, '');
                    sublist1.setLineItemValue('custevent_wkot2', linenumber, '');
                    
                    sublist1.setLineItemValue('custevent_wkst2', linenumber, '');
                    sublist1.setLineItemValue('custevent_othours', linenumber, '');
                    sublist1.setLineItemValue('custevent_wkdt1', linenumber, '');
                    sublist1.setLineItemValue('custevent_wkdt2', linenumber, '');
                    
                    
                    if (a_STTask.indexOf(i_Service_Item_Name) != -1) //
                    {
                        if (startdateweek == currentdateweek) //1st week Added for sunday hrs
                        {
                            i_week1_st = parseFloat(i_week1_st) + parseFloat(i_Duration_decimal);
                            //nlapiLogExecution('DEBUG', 'Log', 'i_week1_st : ' + i_week1_st);
                            sublist1.setLineItemValue('custevent_wkst1', linenumber, parseFloat(i_week1_st));
                        }
                        else 
                            if (currentdateweek == startdateweek + 1 || (startdateweek != 1 && currentdateweek == 1)) //sec week , Added for sunday hrs
                            {
                                if (isNaN(i_Duration_decimal)) //
                                {
                                    i_Duration_decimal = 0;
                                }
                                i_week2_st = parseFloat(i_week2_st) + parseFloat(i_Duration_decimal);
                                //nlapiLogExecution('DEBUG', 'Log', 'i_week2_st : ' + i_week2_st);
                                sublist1.setLineItemValue('custevent_wkst2', linenumber, parseFloat(i_week2_st));
                            //sublist1.setLineItemValue('custevent_wkst2', linenumber, parseFloat(i_Duration_decimal));
                            }
                    }
                    else 
                        if (i_Service_Item_Name == s_LeaveTask || i_Service_Item_Name == s_HolidayTask) //
                        {
                            if (i_Service_Item_Name == s_LeaveTask) //
                            {
                                i_totalTime = parseFloat(i_totalTime) + parseFloat(i_Duration_decimal)
                            }
                            else 
                                if (i_Service_Item_Name == s_HolidayTask) //
                                {
                                    i_totalTime = parseFloat(i_totalTime) + parseFloat(i_Duration_decimal)
                                }
                            
                            
                        }
                    
                    var reghours = parseFloat(i_week1_st) + parseFloat(i_week2_st);
                    //nlapiLogExecution('DEBUG', 'if part of employee id mistmatch', 'else reghours ===' + reghours)
                    sublist1.setLineItemValue('custevent_reghours', linenumber, reghours);
                    currentLinenumber = linenumber;
                    
                    linenumber++;
                    ///}
                }
                else 
                    //if (includerecord != s_id) //
                    //{
                    
                    if (tempInc == s_id) /////if the employee records prev and next is same means check for the week
                    {
                    
                        if (a_STTask.indexOf(i_Service_Item_Name) != -1) //
                        {
                            if (startdateweek == currentdateweek) ////if it is 1st week.///Added for sunday hrs
                            {
                                i_week1_st = parseFloat(i_week1_st) + parseFloat(i_Duration_decimal);
                                //nlapiLogExecution('DEBUG', 'Log', 'i_week1_st : ' + i_week1_st);
                                sublist1.setLineItemValue('custevent_wkst1', currentLinenumber, parseFloat(i_week1_st));
                            ///sublist1.setLineItemValue('custevent_wkst1', currentLinenumber, parseFloat(i_Duration_decimal));
                            }
                            else 
                                if (currentdateweek == startdateweek + 1 || (startdateweek != 1 && currentdateweek == 1)) ////if it is 2nd week Added for sunday hrs
                                {
                                    i_week2_st = parseFloat(i_week2_st) + parseFloat(i_Duration_decimal);
                                    //nlapiLogExecution('DEBUG', 'Log', 'i_week2_st : ' + i_week2_st);
                                    sublist1.setLineItemValue('custevent_wkst2', currentLinenumber, parseFloat(i_week2_st));
                                ///sublist1.setLineItemValue('custevent_wkst2', currentLinenumber, parseFloat(i_Duration_decimal));
                                }
                        }
                        else 
                            if (i_Service_Item_Name == s_LeaveTask || i_Service_Item_Name == s_HolidayTask) {
                                if (i_Service_Item_Name == s_LeaveTask) {
                                    i_totalTime = parseFloat(i_totalTime) + parseFloat(i_Duration_decimal)
                                }
                                else 
                                    if (i_Service_Item_Name == s_HolidayTask) {
                                        i_totalTime = parseFloat(i_totalTime) + parseFloat(i_Duration_decimal)
                                    }
                                
                                if (startdateweek == currentdateweek) {
                                //sublist1.setLineItemValue('custevent_wk1timeoff1', currentLinenumber, i_totalTime);
                                }
                                else {
                                //sublist1.setLineItemValue('custevent_wk1timeoff2', currentLinenumber, i_totalTime);
                                }
                            }
                        
                        ////////////////DT Column set///////////////////////
                        
                        dtsetvalue = setDTvalue(sublist1, currentLinenumber, weeklyDTTotal, s_id, startdateweek, currentdateweek, i_ProjectLocation);
                        
                        //nlapiLogExecution('DEBUG', 'SearchData', 'Settiing DT value');
                        //////////////OT and Week off totals columns calculations//////////////////////////////////////
                        var week1OT = sublist1.getLineItemValue('custevent_wkot1', currentLinenumber);
                        var week2OT = sublist1.getLineItemValue('custevent_wkot2', currentLinenumber);
                        var week1ST = sublist1.getLineItemValue('custevent_wkst1', currentLinenumber);
                        var week2ST = sublist1.getLineItemValue('custevent_wkst2', currentLinenumber);
                        var week1DT = sublist1.getLineItemValue('custevent_wkdt1', currentLinenumber);
                        var week2DT = sublist1.getLineItemValue('custevent_wkdt2', currentLinenumber);
                        //nlapiLogExecution('DEBUG', 'SearchData', 'week1ST,week2ST===' + week1ST + ',,' + week2ST);
                        //nlapiLogExecution('DEBUG', 'SearchData', 'week1OT,week2OT===' + week1OT + ',,' + week2OT);
                        
                        if (week1OT == '' || week1OT == 'undefined' || week1OT == null || isNaN(week1OT)) {
                            week1OT = 0;
                        }
                        if (week2OT == '' || week2OT == 'undefined' || week2OT == null || isNaN(week2OT)) {
                            week2OT = 0;
                        }
                        
                        if (week1ST == '' || week1ST == 'undefined' || week1ST == null || isNaN(week1ST)) {
                            week1ST = 0;
                        }
                        if (week2ST == '' || week2ST == 'undefined' || week2ST == null || isNaN(week2ST)) {
                            week2ST = 0;
                        }
                        if (week1DT == '' || week1DT == 'undefined' || week1DT == null || isNaN(week1DT)) {
                            week1DT = 0;
                        }
                        if (week2DT == '' || week2DT == 'undefined' || week2DT == null || isNaN(week2DT)) {
                            week2DT = 0;
                        }
                        //var STTotal=parseInt(week1ST)+parseInt(week2ST)
                        //nlapiLogExecution('DEBUG', 'SearchData', 'week1ST===' + week1ST + ' === week2ST : ' + week2ST)
                        
                        var DTTotal = parseInt(week1DT) + parseInt(week2DT)
                        var OTweekstotals = parseFloat(week1OT) + parseFloat(week2OT);
                        
                        if (isNaN(week1ST)) //
                        {
                            week1ST = 0;
                        }
                        if (isNaN(week2ST)) //
                        {
                            week2ST = 0;
                        }
                        
                        var STweektotals = parseFloat(week1ST) + parseFloat(week2ST);
                        //nlapiLogExecution('DEBUG', 'SearchData', 'STweektotals===' + STweektotals)
                        
                        if (OTweekstotals == 0.0) //
                        {
                            sublist1.setLineItemValue('custevent_othours', currentLinenumber, '');
                        }
                        else //
                        {
                            sublist1.setLineItemValue('custevent_othours', currentLinenumber, parseFloat(OTweekstotals));
                        }
                        
                        if (isNaN(reghours)) //
                        {
                            reghours = '';
                        }
                        
                        sublist1.setLineItemValue('custevent_reghours', currentLinenumber, parseFloat(STweektotals));
                        
                    //nlapiLogExecution('DEBUG', 'SearchData', 'linenumber===' + linenumber)
                    //nlapiLogExecution('DEBUG', 'SearchData', 'currentLinenumber===' + currentLinenumber)
                    }
                ///}
                
                tempInc = s_id
                //nlapiLogExecution('DEBUG', 'SearchData', '******a_result_array******')
            }
        }
        //nlapiLogExecution('DEBUG', 'SearchData', 'a_result_array===' + a_result_array)
        //nlapiLogExecution('DEBUG', 'SearchData', 'a_result_array===' + a_result_array.length)
    } 
    catch (e) //
    {
        nlapiLogExecution('ERROR', 'ERROR', 'e === ' + e)
        nlapiLogExecution('ERROR', 'ERROR', 'e.message === ' + e.message)
    }
    return a_result_array;
}

function _SearchData(s_beforedate, s_afterdate){
    var a_filters = new Array();
    a_filters[0] = new nlobjSearchFilter('date', null, 'onorbefore', s_beforedate);
    a_filters[1] = new nlobjSearchFilter('date', null, 'onorafter', s_afterdate);
    var a_columns = new Array();
    a_columns[0] = new nlobjSearchColumn('employee');
    a_columns[1] = new nlobjSearchColumn('item');
    var searchresult = nlapiSearchRecord('timeentry', 'customsearch_salnonstdrptts_4_time_ent_2', a_filters, null);
    return searchresult
}

function SearchData(s_beforedate, s_afterdate) //
{
    var LastfetchedInternalid_T = 0;
    var searchresultFinal_T = new Array();
    var searchresult_T = new Array();
    
    do //
    {
        var a_filters = new Array();
        a_filters[0] = new nlobjSearchFilter('date', null, 'onorbefore', s_beforedate);
        a_filters[1] = new nlobjSearchFilter('date', null, 'onorafter', s_afterdate);
        a_filters[2] = new nlobjSearchFilter('internalidnumber', 'employee', 'greaterthan', LastfetchedInternalid_T);////is not before 1sept
        var a_columns = new Array();
        a_columns[0] = new nlobjSearchColumn('employee');
        a_columns[1] = new nlobjSearchColumn('item');
        
        var searchresult_T = nlapiSearchRecord('timeentry', 'customsearch_salnonstdrptts_4_time_ent_2', a_filters, null);
        
        if (searchresult_T != null && searchresult_T != undefined && searchresult_T != '') //
        {
            //nlapiLogExecution('DEBUG', 'SearchData', 'searchresult_T == ' + searchresult_T.length);
            var temp_emp = '';
            var emp = '';
            
            for (var i = 0; i < searchresult_T.length; i++) //
            {
                var a_search_empdata = searchresult_T[i];
                var all_columns = a_search_empdata.getAllColumns();
                emp = a_search_empdata.getValue(all_columns[13]);
                //nlapiLogExecution('DEBUG', 'SearchData', 'i : ' + i + ' emp ==' + emp);
				
                if (emp != null) //
                {
                    if (i == 0) //
                    {
                        temp_emp = emp;
                    }
                    
                    LastfetchedInternalid_T = temp_emp;
                    //nlapiLogExecution('DEBUG', 'SearchData', 'i : ' + i + ' temp_emp ==' + temp_emp);
                    if (emp == temp_emp) //if employee id is same then add to final array
                    {
                        searchresultFinal_T.push(searchresult_T[i]);
                    }
                    else //else employee id does nt match
                    {
                        //nlapiLogExecution('DEBUG', 'SearchData', 'Emp change row number : ' + i);
                        if (i > 950)//
                        {
                            //nlapiLogExecution('DEBUG', 'SearchData', 'LastfetchedInternalid_T==' + LastfetchedInternalid_T);
                            break;
                        }
                        else //
                        {
                            searchresultFinal_T.push(searchresult_T[i]);
                            temp_emp = emp;
							LastfetchedInternalid_T = temp_emp;
                        }
                    }
                }
                //searchresultFinal_T.push(searchresult_T[i]);
                //nlapiLogExecution('DEBUG','researchcodes','lastid=='+a_search_empdata.getValue('internalid',null,'group'));
            }
            //nlapiLogExecution('DEBUG','researchcodes','lastid=='+a_search_empdata.getValue('internalid',null,'group'));
            //LastfetchedInternalid_T = searchresult_T[searchresult_T.length - 1].getValue('internalid', 'employee', 'group');
            //nlapiLogExecution('DEBUG', 'SearchData', 'LastfetchedInternalid_T==' + LastfetchedInternalid_T);
        }
        
        //return searchresult
    }
    while (searchresult_T != null && searchresult_T != undefined && searchresult_T != '')
    {
    
    }
    //nlapiLogExecution('DEBUG', 'SearchData', 'searchresultFinal_T == ' + searchresultFinal_T.length);
    return searchresultFinal_T;
}

/*
 Function name :-getWeekNumber()
 Parameter :- date
 return type :- returns week number of given year
 */
function getWeekNumber(d){
    //Copy date so don't modify original
    d = new Date(+d);
    d.setHours(0, 0, 0);
    //Set to nearest Thursday: current date + 4 - current day number
    //Make Sunday's day number 7
    d.setDate(d.getDate() + 4 - (d.getDay()));
    //Get first day of year
    var yearStart = new Date(d.getFullYear(), 0, 1);
    //Calculate full weeks to nearest Thursday
    var weekNo = Math.ceil((((d - yearStart) / 86400000) + 1) / 7)
    //nlapiLogExecution('DEBUG', 'beforeLoad_CalculateWeek', 'weekNo=' + weekNo);
    //Return array of year and week number
    return weekNo;
}

function Callsuitelet(sublist1, param) //
{
    var count = sublist1.getLineItemCount();
    //nlapiLogExecution('DEBUG', 'SearchData', 'count===' + count)
    var globalArray = new Array();
    var arr = new Array();
    
    for (var i = 1; i <= count; i++) //
    {
        var file = sublist1.getLineItemValue('custevent_file', i);
		var gci = sublist1.getLineItemValue('custevent_gci', i);
        var firstname = sublist1.getLineItemValue('custevent_firstname', i);
        var lastname = sublist1.getLineItemValue('custevent_lastname', i);
        var wkst1 = sublist1.getLineItemValue('custevent_wkst1', i);
        var wkot1 = sublist1.getLineItemValue('custevent_wkot1', i);
        var wkdt1 = sublist1.getLineItemValue('custevent_wkdt1', i);
        var ifnonexempt = sublist1.getLineItemValue('custevent_ifnonexempt', i);
        var wkst2 = sublist1.getLineItemValue('custevent_wkst2', i);
        var wkot2 = sublist1.getLineItemValue('custevent_wkot2', i);
        var wkdt2 = sublist1.getLineItemValue('custevent_wkdt2', i);
        var reghours = sublist1.getLineItemValue('custevent_reghours', i);
        var regcode = sublist1.getLineItemValue('custevent_ratecode', i);
        var department = sublist1.getLineItemValue('custevent_department', i);
        var projectloc = sublist1.getLineItemValue('custevent_projectloc', i);
        var othhours = sublist1.getLineItemValue('custevent_othours', i);
        var projectcity = sublist1.getLineItemValue('custevent_projectcity', i);
        var employeetype = sublist1.getLineItemValue('custevent_employeetype', i);
        var usernotes = sublist1.getLineItemValue('custevent_usernotes', i);
        
        if (projectcity != '' && projectcity != null && projectcity != 'undefined') {
            projectcity = projectcity
        }
        else {
            projectcity = ''
        }
        if (department != '' && department != null && department != 'undefined') {
            department = department
        }
        else {
            department = ''
        }
        if (regcode != '' && regcode != null && regcode != 'undefined') {
            regcode = regcode
        }
        else {
            regcode = ''
        }
        
        if (wkst1 != '' && wkst1 != null && wkst1 != 'undefined') {
            wkst1 = wkst1
        }
        else {
            wkst1 = ''
        }
        if (wkot1 != '' && wkot1 != null && wkot1 != 'undefined') {
            wkot1 = wkot1
        }
        else {
            wkot1 = ''
        }
        if (wkdt1 != '' && wkdt1 != null && wkdt1 != 'undefined') {
            wkdt1 = wkdt1
        }
        else {
            wkdt1 = ''
        }
        if (wkst2 != '' && wkst2 != null && wkst2 != 'undefined') {
            wkst2 = wkst2
        }
        else {
            wkst2 = ''
        }
        if (wkot2 != '' && wkot2 != null && wkot2 != 'undefined') {
            wkot2 = wkot2
        }
        else {
            wkot2 = ''
        }
        if (wkdt2 != '' && wkdt2 != null && wkdt2 != 'undefined') {
            wkdt2 = wkdt2
        }
        else {
            wkdt2 = ''
        }
        if (reghours != '' && reghours != null && reghours != 'undefined' && (isNaN(reghours) == false)) {
            reghours = reghours
        }
        else {
            reghours = ''
        }
        if (file != '' && file != null && file != 'undefined') {
            file = file
        }
        else {
            file = ''
        }
		
		if (gci != '' && gci != null && gci != 'undefined') {
            gci = gci
        }
        else {
            gci = ''
        }
        
        if (firstname != '' && firstname != null && firstname != 'undefined') {
            firstname = firstname
        }
        else {
            firstname = ''
        }
        if (lastname != '' && lastname != null && lastname != 'undefined') {
            lastname = lastname
        }
        else {
            lastname = ''
        }
        
        if (regcode != '' && regcode != null && regcode != 'undefined') {
            regcode = regcode
        }
        else {
            regcode = ''
        }
        
        if (ifnonexempt != '' && ifnonexempt != null && ifnonexempt != 'undefined') {
            ifnonexempt = ifnonexempt
        }
        else {
            ifnonexempt = ''
        }
        if (reghours != '' && reghours != null && reghours != 'undefined' && (isNaN(reghours) == false)) {
            reghours = reghours
        }
        else {
            reghours = ''
        }
        
        if (projectloc != '' && projectloc != null && projectloc != 'undefined') {
            projectloc = projectloc
            projectloc = projectloc.toString().replace(/[,]/g, "; ")
        }
        else {
            projectloc = ''
        }
        if (usernotes != '' && usernotes != null && usernotes != 'undefined') {
            usernotes = usernotes
        }
        else {
            usernotes = ''
        }
        if (employeetype != '' && employeetype != null && employeetype != 'undefined') {
            employeetype = employeetype
        }
        else {
            employeetype = ''
        }
        usernotes = usernotes.toString().replace(/[|]/g, " ")
        usernotes = usernotes.toString().replace(/[,]/g, " ")
        //nlapiLogExecution('DEBUG', 'beforeLoad_CalculateWeek', 'usernotes-----' + usernotes);
        
        if (i == 1) //
		{
			//nlapiLogExecution('DEBUG', 'beforeLoad_CalculateWeek', 'arr[i]=' + arr);
			arr[i] = "File #,GCI(INC) ID,First Name,Last Name,If Non Exempt,Project_City,Project Location,WK1 ST,WK1 OT,WK1 DT,WK2 ST,WK2 OT,WK2 DT,Reg Hours,OT Hours,Rate Code,Employee_Type,Department,User Notes\n" + file + "," + gci + "," + firstname + "," + lastname + "," + ifnonexempt + "," + projectcity + "," + projectloc + "," + wkst1 + "," + wkot1 + "," + wkdt1 + "," + wkst2 + "," + wkot2 + "," + wkdt2 + "," + reghours + ',' + othhours + ',' + regcode + ',' + employeetype + ',' + department + "," + usernotes;
		}
		else //
		{
			arr[i] = "\n" + file + "," + gci + "," + firstname + "," + lastname + "," + ifnonexempt + "," + projectcity + "," + projectloc + "," + wkst1 + "," + wkot1 + "," + wkdt1 + "," + wkst2 + "," + wkot2 + "," + wkdt2 + "," + reghours + ',' + othhours + ',' + regcode + ',' + employeetype + ',' + department + "," + usernotes;
		}
        //nlapiLogExecution('DEBUG', 'beforeLoad_CalculateWeek', 'arr[i]=' + arr[i]);
        
        globalArray[i - 1] = (arr[i]);
        //nlapiLogExecution('DEBUG', 'beforeLoad_CalculateWeek', 'globalArray######' + globalArray);
    }
    
    var fileName = 'Hourly report from TS'
    var Datetime = new Date();
    var CSVName = fileName + " - " + Datetime + '.csv';
    var file = nlapiCreateFile(CSVName, 'CSV', globalArray.toString());
    
    //nlapiLogExecution('DEBUG', 'suitlet_Po_ClosedStatus', ' PDF Name-->  ');
    response.setContentType('CSV', CSVName);
    
    //nlapiLogExecution('DEBUG', 'suitlet_Po_ClosedStatus', ' PDF Name-->  ');
    response.write(file.getValue());
    
}

function setDTvalue(sublist1, currentLinenumber, weeklyDTTotal, s_id, startdateweek, currentdateweek, s_ProjectLocation){
    //nlapiLogExecution('DEBUG', 'SearchData', 'weeklyDTTotal===' + weeklyDTTotal)
    //nlapiLogExecution('DEBUG', 'SearchData', 'startdateweek===' + startdateweek)
    //nlapiLogExecution('DEBUG', 'SearchData', 's_id===' + s_id)
    if (weeklyDTTotal != null && weeklyDTTotal != '' && weeklyDTTotal != 'undefined') {
        for (var i = 0; i < weeklyDTTotal.length; i++) {
            var split = weeklyDTTotal[i].toString().split("##")
            //nlapiLogExecution('DEBUG', 'SearchData', 'split===' + split)
            var i_DTTime = split[0]
            
            var i_employeeid = split[1]
            //nlapiLogExecution('DEBUG', 'SearchData', 'i_employeeid===' + i_employeeid)
            var i_weekno = split[2]
            //nlapiLogExecution('DEBUG', 'SearchData', 'i_weekno===' + i_weekno)
            var i_othours = split[3]
            //nlapiLogExecution('DEBUG', 'SearchData', 'i_othours===' + i_othours)
            //nlapiLogExecution('DEBUG', 'SearchData', 's_id===' + s_id)
            
            if (i_employeeid == s_id) {
                if (startdateweek == i_weekno) {
                    //var week1OT = sublist1.getLineItemValue('custevent_wkot1', currentLinenumber);
                    //nlapiLogExecution('DEBUG', 'SearchData', 'week1OT===' + week1OT)
                    
                    if (s_ProjectLocation == 'California') {
                        sublist1.setLineItemValue('custevent_wkdt1', currentLinenumber, parseFloat(i_DTTime));
                    }
                    sublist1.setLineItemValue('custevent_wkot1', currentLinenumber, parseFloat(i_othours));
                    
                }
                else {
                    var week2OT = sublist1.getLineItemValue('custevent_wkot2', currentLinenumber);
                    //nlapiLogExecution('DEBUG', 'SearchData', 'in else===')
                    //nlapiLogExecution('DEBUG', 'SearchData', 'i_employeeid===' + i_employeeid)
                    //nlapiLogExecution('DEBUG', 'SearchData', 'i_weekno===' + i_weekno)
                    //nlapiLogExecution('DEBUG', 'SearchData', 'i_othours===' + i_othours)
                    //nlapiLogExecution('DEBUG', 'SearchData', 'startdateweek===' + startdateweek)
                    
                    if (s_ProjectLocation == 'California') {
                        sublist1.setLineItemValue('custevent_wkdt2', currentLinenumber, parseFloat(i_DTTime));
                    }
                    
                    sublist1.setLineItemValue('custevent_wkot2', currentLinenumber, parseFloat(i_othours));
                    //i_othours = parseFloat(i_othours) + parseFloat(4)
                
                    //week2OT = parseFloat(week2OT) - parseFloat(sublist1.getLineItemValue('custevent_wkdt2', currentLinenumber))
                    //nlapiLogExecution('DEBUG', 'SearchData', 'week2OT===' + week2OT)
                    //sublist1.setLineItemValue('custevent_wkot2', currentLinenumber, parseFloat(i_othours));
                    //nlapiLogExecution('DEBUG', 'SearchData', 'week2OT===' + week2OT)
                
                }
            }
        }
    }
    
    return true;
}

function getDTTotalsweekly(sublist1, searchData, s_afterdate1){








    /////////////get stardateweekno
    s_afterdate1 = nlapiStringToDate(s_afterdate1);
    //************** pass the date as parameter to getWeekNumber function *****************//
    var startdateweek = getWeekNumber(s_afterdate1)
    //nlapiLogExecution('DEBUG', 'beforeLoad_CalculateWeek', 'startdateweek=' + startdateweek);
    //////////////////////////////////////
    
    var othoursarray = new Array();
    var a_week1_array = new Array();
    var a_weekarray = new Array();
    var finalarray = new Array();
    if (searchData.length != '' && searchData.length != null && searchData.length != 'undefined') {
        /////////Loop through rows of search/////////////////////////////////////////////////////
        for (var x = 0; x < searchData.length; x++) {
            var a_search_transaction_result1 = searchData[x];
            var columns1 = a_search_transaction_result1.getAllColumns();
            
            var columnLen1 = columns1.length;
            
            var i_Calendar_Week = ''
            var i_Duration_decimal = 0;
            var i_Employee = '';
            var s_location = ''
            for (var hg1 = 0; hg1 < columnLen1; hg1++) {
            
            
                var column1 = columns1[hg1];
                //nlapiLogExecution('DEBUG', 'search_ComplaintCategory', 'column  -->' + column);
                var label1 = column1.getLabel();
                //nlapiLogExecution('DEBUG', 'search_ComplaintCategory', '*************************label********************  -->' + label1);
                var grpfieldName = column1.getName();
                //nlapiLogExecution('DEBUG', 'search_ComplaintCategory', '*************************grpfieldName********************  -->' + grpfieldName);
                
                var value = a_search_transaction_result1.getValue(column1)
                var text = a_search_transaction_result1.getText(column1)
                
                //nlapiLogExecution('DEBUG', 'search_ComplaintCategory', '*************************value********************  -->' + value);
                //nlapiLogExecution('DEBUG', 'search_ComplaintCategory', '*************************text********************  -->' + text);
                
                //nlapiLogExecution('DEBUG', 'search_ComplaintCategory', 'value  -->' + value);
                if (grpfieldName == 'date') {
                    var i_Calendar_Week = value;
                }
                if (grpfieldName == 'durationdecimal') {
                    var i_Duration = value;
                }
                if (grpfieldName == 'custentity_worklocation') {
                    var s_location = text;
                }
                if (grpfieldName == 'internalid') {
                    var i_Employee = value
                }
            }
            
            i_Duration_decimal = parseFloat(i_Duration_decimal)
            //nlapiLogExecution('DEBUG', 'SearchData', 'i_Employee===' + i_Employee)
            //nlapiLogExecution('DEBUG', 'SearchData', 'i_Calendar_Week===' + i_Calendar_Week)
            //nlapiLogExecution('DEBUG', 'SearchData', 'i_Duration===' + i_Duration)
            //nlapiLogExecution('DEBUG', 'SearchData', 's_location===' + s_location)
            var i_Calendar_Week1 = nlapiStringToDate(i_Calendar_Week);
            var currentdateweek = getWeekNumber(i_Calendar_Week1)
            var startdateweek = getWeekNumber(s_afterdate1)
            a_weekarray.push(i_Employee + "##" + currentdateweek)
            a_week1_array.push(i_Employee + "##" + i_Duration + "##" + currentdateweek + "##" + s_location)
            
        }
        var uniqueweekArray = removearrayduplicate(a_weekarray)
        
        for (var a = 0; a < uniqueweekArray.length; a++) {
            var i_totalTime = 0;
            var i_totalTime_1 = 0;
            var othhours = 0
            var i_split = uniqueweekArray[a].toString().split("##")
            //nlapiLogExecution('DEBUG', 'beforeLoad_CalculateWeek', 'i_split=' + i_split);
            
            var i_empid1 = i_split[0]
            //nlapiLogExecution('DEBUG', 'beforeLoad_CalculateWeek', 'i_empid1=' + i_empid1);
            
            var i_weekno1 = i_split[1]
            //nlapiLogExecution('DEBUG', 'beforeLoad_CalculateWeek', 'i_weekno1=' + i_weekno1);
            
            for (var b = 0; b < a_week1_array.length; b++) {
                var i_split_1 = a_week1_array[b].toString().split("##")
                //nlapiLogExecution('DEBUG', 'beforeLoad_CalculateWeek', 'i_split_1=' + i_split_1);
                
                var i_empid2 = i_split_1[0]
                
                
                var i_hours = i_split_1[1]
                //nlapiLogExecution('DEBUG', 'beforeLoad_CalculateWeek', 'i_hours=' + i_hours);
                
                var i_weekno2 = i_split_1[2]
                //nlapiLogExecution('DEBUG', 'beforeLoad_CalculateWeek', 'i_weekno2=' + i_weekno2);
                var i_location = i_split_1[3]
                //nlapiLogExecution('DEBUG', 'beforeLoad_CalculateWeek', 'i_location=' + i_location);
                
                if (i_empid1 == i_empid2 && i_weekno1 == i_weekno2) {
                    if (i_hours > 4) {
                        i_totalTime = parseFloat(i_hours) - parseFloat(4)
                        if (i_location == 'California') {
                            othhours = parseFloat(othhours) + parseFloat(4)
                        }
                        else {
                            othhours = parseFloat(othhours) + parseFloat(i_hours)
                        }
                        
                        
                        i_totalTime_1 = parseFloat(i_totalTime_1) + parseFloat(i_totalTime)
                    }
                    else {
                        othhours = parseFloat(othhours) + parseFloat(i_hours)
                    }
                    
                }
            }
            if (i_totalTime_1 != null && i_totalTime_1 != 'undefined' && i_totalTime_1 != '') {
                i_totalTime_1 = i_totalTime_1
            }
            else {
                i_totalTime_1 = 0
            }
            
            finalarray.push(i_totalTime_1 + "##" + i_empid1 + "##" + i_weekno1 + "##" + othhours)
        }
        //nlapiLogExecution('DEBUG', 'beforeLoad_CalculateWeek', 'finalarray=' + finalarray);
    }
    
    return finalarray;
}

function _SearchDTData(s_beforedate, s_afterdate) //
{
    //nlapiLogExecution('DEBUG', 'SearchDTData', 'In Search Data')
    //nlapiLogExecution('DEBUG', 'SearchDTData', 's_beforedate===' + s_beforedate)
    //nlapiLogExecution('DEBUG', 'SearchDTData', 's_afterdate===' + s_afterdate)
    
    var a_filters = new Array();
    //a_filters[0] = new nlobjSearchFilter('date', null, 'on', s_beforedate);
    a_filters[0] = new nlobjSearchFilter('date', null, 'onorbefore', s_beforedate);
    a_filters[1] = new nlobjSearchFilter('date', null, 'onorafter', s_afterdate);
    
    var a_columns = new Array();
    a_columns[0] = new nlobjSearchColumn('employee');
    a_columns[1] = new nlobjSearchColumn('item');
    a_columns[2] = new nlobjSearchColumn('date');
    a_columns[3] = new nlobjSearchColumn('hours');
    
    var searchresult = nlapiSearchRecord('timeentry', 'customsearchsalnonstdrptts_4_2_timeent_2', a_filters, null);
    //nlapiLogExecution('DEBUG', 'SearchDTData', 'searchresult===' + searchresult.length)
    return searchresult;
}

function SearchDTData(s_beforedate, s_afterdate) //
{
    var LastfetchedInternalid_T = 0;
    var searchresultFinal_T = new Array();
    var searchresult = new Array();
    
    do //
	{
		var a_filters = new Array();
		a_filters[0] = new nlobjSearchFilter('date', null, 'onorbefore', s_beforedate);
		a_filters[1] = new nlobjSearchFilter('date', null, 'onorafter', s_afterdate);
		a_filters[2] = new nlobjSearchFilter('internalidnumber', 'employee', 'greaterthan', LastfetchedInternalid_T);////is not before 1sept
		var a_columns = new Array();
		a_columns[0] = new nlobjSearchColumn('employee');
		a_columns[1] = new nlobjSearchColumn('item');
		a_columns[2] = new nlobjSearchColumn('date');
		a_columns[3] = new nlobjSearchColumn('hours');
		
		var searchresult = nlapiSearchRecord('timeentry', 'customsearchsalnonstdrptts_4_2_timeent_2', a_filters, null);
		//nlapiLogExecution('DEBUG', 'SearchDTData', 'searchresult===' + searchresult.length)
		
		if (searchresult != null && searchresult != undefined && searchresult != '') //
		{
			//nlapiLogExecution('DEBUG', '_SearchDTData', 'searchresult == ' + searchresult.length);
			var temp_emp = '';
			var emp = '';
			
			for (var i = 0; i < searchresult.length; i++) //
			{
				var a_search_empdata = searchresult[i];
				var all_columns = a_search_empdata.getAllColumns();
				emp = a_search_empdata.getValue(all_columns[5]);
				//nlapiLogExecution('DEBUG', 'SearchData', 'i : ' + i + ' emp ==' + emp);
				
				if (emp != null) //
				{
					if (i == 0) //
					{
						temp_emp = emp;
					}
					
					LastfetchedInternalid_T = temp_emp;
					//nlapiLogExecution('DEBUG', 'SearchData', 'i : ' + i + ' temp_emp ==' + temp_emp);
					
					if (emp == temp_emp) //if employee id is same then add to final array
					{
						searchresultFinal_T.push(searchresult[i]);
					}
					else //else employee id does not match
					{
						//nlapiLogExecution('DEBUG', 'SearchData', 'Emp change row number : ' + i);
						if (i > 950)//
						{
							//nlapiLogExecution('DEBUG', 'SearchData', 'LastfetchedInternalid_T==' + LastfetchedInternalid_T);
							break;
						}
						else //
						{
							searchresultFinal_T.push(searchresult[i]);
							temp_emp = emp;
							LastfetchedInternalid_T = temp_emp;
						}
					}
				}
			//searchresultFinal_T.push(searchresult[i]);
			//nlapiLogExecution('DEBUG','researchcodes','lastid=='+a_search_empdata.getValue('internalid',null,'group'));
			}
		//nlapiLogExecution('DEBUG','researchcodes','lastid=='+a_search_empdata.getValue('internalid',null,'group'));
		//LastfetchedInternalid_T = searchresult[searchresult.length - 1].getValue('internalid', 'employee', null);
		//nlapiLogExecution('DEBUG', '_SearchDTData', 'LastfetchedInternalid_T==' + LastfetchedInternalid_T);
		}
	}
	while (searchresult != null && searchresult != undefined && searchresult != '') //
    {
    
    }
    return searchresultFinal_T;
    
}

function SearchDTData(s_beforedate, s_afterdate) //
{
    var LastfetchedInternalid_T = 0;
    var searchresultFinal_T = new Array();
    var searchresult = new Array();
    
    do //
	{
		var a_filters = new Array();
		a_filters[0] = new nlobjSearchFilter('date', null, 'onorbefore', s_beforedate);
		a_filters[1] = new nlobjSearchFilter('date', null, 'onorafter', s_afterdate);
		a_filters[2] = new nlobjSearchFilter('internalidnumber', 'employee', 'greaterthan', LastfetchedInternalid_T);////is not before 1sept
		var a_columns = new Array();
		a_columns[0] = new nlobjSearchColumn('employee');
		a_columns[1] = new nlobjSearchColumn('item');
		a_columns[2] = new nlobjSearchColumn('date');
		a_columns[3] = new nlobjSearchColumn('hours');
		
		var searchresult = nlapiSearchRecord('timeentry', 'customsearchsalnonstdrptts_4_2_timeent_2', a_filters, null);
		//nlapiLogExecution('DEBUG', 'SearchDTData', 'searchresult===' + searchresult.length)
		
		if (searchresult != null && searchresult != undefined && searchresult != '') //
		{
			//nlapiLogExecution('DEBUG', '_SearchDTData', 'searchresult == ' + searchresult.length);
			var temp_emp = '';
			var emp = '';
			
			for (var i = 0; i < searchresult.length; i++) //
			{
				var a_search_empdata = searchresult[i];
				var all_columns = a_search_empdata.getAllColumns();
				emp = a_search_empdata.getValue(all_columns[5]);
				//nlapiLogExecution('DEBUG', 'SearchData', 'i : ' + i + ' emp ==' + emp);
				
				if (emp != null) //
				{
					if (i == 0) //
					{
						temp_emp = emp;
					}
					
					LastfetchedInternalid_T = temp_emp;
					//nlapiLogExecution('DEBUG', 'SearchData', 'i : ' + i + ' temp_emp ==' + temp_emp);
					
					if (emp == temp_emp) //if employee id is same then add to final array
					{
						searchresultFinal_T.push(searchresult[i]);
					}
					else //else employee id does not match
					{
						//nlapiLogExecution('DEBUG', 'SearchData', 'Emp change row number : ' + i);
						if (i > 950)//
						{
							//nlapiLogExecution('DEBUG', 'SearchData', 'LastfetchedInternalid_T==' + LastfetchedInternalid_T);
							break;
						}
						else //
						{
							searchresultFinal_T.push(searchresult[i]);
							temp_emp = emp;
							LastfetchedInternalid_T = temp_emp;
						}
					}
				}
			//searchresultFinal_T.push(searchresult[i]);
			//nlapiLogExecution('DEBUG','researchcodes','lastid=='+a_search_empdata.getValue('internalid',null,'group'));
			}
		//nlapiLogExecution('DEBUG','researchcodes','lastid=='+a_search_empdata.getValue('internalid',null,'group'));
		//LastfetchedInternalid_T = searchresult[searchresult.length - 1].getValue('internalid', 'employee', null);
		//nlapiLogExecution('DEBUG', '_SearchDTData', 'LastfetchedInternalid_T==' + LastfetchedInternalid_T);
		}
	}
	while (searchresult != null && searchresult != undefined && searchresult != '') //
    {
    
    }
    return searchresultFinal_T;
    
}

// Return memo data
function searchMemoFields(s_before_date, s_after_date)
{
	// Store data in an object
	var a_memo_data = new Object();
	
	//var o_searchMemos = nlapiLoadSearch('timeentry', 'customsearchsalnonstdrptts_4_3_timeentry');
	
	var a_filters = new Array();//o_searchMemos.getFilters();
	a_filters[0] = new nlobjSearchFilter('date', null, 'onorbefore', s_before_date);
	a_filters[1] = new nlobjSearchFilter('date', null, 'onorafter', s_after_date);

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('internalid').setSort(); //include internal id in the returned columns and sort for reference
	
	var resultSet = nlapiSearchRecord('timeentry','customsearchsalnonstdrptts_4_3_timeent_2',a_filters,columns); //perform search
	//var completeResultSet = results; //container of the complete result set
	
	do
	{    
	     
	     
	     if(resultSet != null && resultSet.length != 0)
	    	{
	    		for(var indx = 0; indx < resultSet.length; indx++)
	    			{
	    				var i_employee_id = resultSet[indx].getValue('employee')
	    				var s_date = resultSet[indx].getValue('date')
	    				var s_memo = resultSet[indx].getValue('memo');
	    				
	    				if(a_memo_data[i_employee_id] == undefined)
	    					{
	    						a_memo_data[i_employee_id] = s_date + ' - ' + s_memo + '; ';
	    					}
	    				else
	    					{
	    						a_memo_data[i_employee_id] += s_date + ' - ' + s_memo + '; ';
	    					}
	    			}
	    	}
	   //re-run the search if limit has been reached
	     var lastId = resultSet[resultSet.length - 1].getValue('internalid'); //note the last record retrieved
	     a_filters[2] = new nlobjSearchFilter('internalidnumber',null,'greaterthan',lastId); //create new filter to restrict the next search based on the last record returned
	     resultSet = nlapiSearchRecord('timeentry','customsearchsalnonstdrptts_4_3_timeent_2',a_filters,columns);
	     //completeResultSet = completeResultSet.concat(results); //add the result to the complete result set 
	}while(resultSet != null && resultSet.length == 1000);
		    
	
	
		
		return a_memo_data;
}
function removearrayduplicate(array){
    var newArray = new Array();
    label: for (var i = 0; i < array.length; i++) {
        for (var j = 0; j < array.length; j++) {
            if (newArray[j] == array[i]) 
                continue label;
        }
        newArray[newArray.length] = array[i];
    }
    return newArray;
}

function searchexcludeValues(s_id, searchData, startdateweek, currentdateweek, s_beforedate, c){
    var a_excludeArray = new Array();
    var s_OTTask = "OT"
    var a_STTask = ["ST".toUpperCase(), "FP".toUpperCase(), "Bench Project".toUpperCase(), "Internal Projects".toUpperCase()]
    var s_HolidayTask = "Holiday"
    var s_FHTask = "Floating Holiday"
    s_OTTask = s_OTTask.toString().toUpperCase();
    //s_STTask = s_STTask.toString().toUpperCase();
    s_HolidayTask = s_HolidayTask.toString().toUpperCase();
    s_FHTask = s_FHTask.toString().toUpperCase();
    
    var b_entryFlag = true;
    var i_Wk1OtHrs = 0
    var i_Wk2OtHrs = 0
    var i_Wk1STHrs = 0
    var i_Wk2STHrs = 0
    var i_Wk1HLHrs = 0
    var i_Wk2HLHrs = 0
    var i_Wk1FLHrs = 0;
    var i_Wk2FLHrs = 0;
    
    for (var d = c; d < searchData.length; d++) {
        var i_Calendar_Week = ''
        var i_Employee = ''
        var i_Duration = ''
        var i_Service_Item_Name = '';
        var i_Duration_decimal = 0;
        var i_empid = ''
        var i_Dayofweek = '';
        var i_Employee_First_Name = '';
        var i_Employee_Last_Name = '';
        var i_Employee_Inc_Id = '';
        var Employee_Type = ''
        var Employment_Category = ''
        var i_note = ''
        var i_Dayofweek = '';
        
        var a_search_transaction_result = searchData[d];
        //nlapiLogExecution('DEBUG', 'search_ComplaintCategory', 'a_search_transaction_result  -->' + a_search_transaction_result);
        //nlapiLogExecution('DEBUG', 'search_ComplaintCategory', 'c  -->' + c);
        var columns = a_search_transaction_result.getAllColumns();
        //nlapiLogExecution('DEBUG', 'search_ComplaintCategory', 'columns  -->' + columns);
        
        var columnLen = columns.length;
        //nlapiLogExecution('DEBUG', 'search_ComplaintCategory', 'columnLen  -->' + columnLen);
        
        
        for (var hg1 = 0; hg1 < columnLen; hg1++) {
        
            var column = columns[hg1];
            //nlapiLogExecution('DEBUG', 'search_ComplaintCategory', 'column  -->' + column);
            var label = column.getLabel();
            //nlapiLogExecution('DEBUG', 'search_ComplaintCategory', '*************************label********************  -->' + label);
            
            var value = a_search_transaction_result.getValue(column)
            var text = a_search_transaction_result.getText(column)
            //nlapiLogExecution('DEBUG', 'search_ComplaintCategory', 'value  -->' + value);
            if (label == 'Calendar Week') {
                i_Calendar_Week = value
            }
            if (label == 'Employee') {
                i_Employee = value;
            }
            if (label == 'Service Item') {
                i_Service_Item_Name = value;
            }
            if (label == 'Employee : First Name') {
                i_Employee_First_Name = value;
            }
            if (label == 'Employee : Last Name') {
                i_Employee_Last_Name = value;
            }
            if (label == 'IF Exempt') {
                i_EmployeeIFExempt = text;
            }
            if (label == 'Employee : Employment Category') {
                Employment_Category = text;
            }
            if (label == 'User_Notes') {
                i_note = value
            }
            if (label == 'Department') {
                i_department = text;
            }
            if (label == 'Project Location') {
                i_ProjectLocation = text;
                
            }
            if (label == 'Project City') {
                i_projectcity = text;
            }
            
            if (label == 'Emp IntID') {
                i_empid = value;
            }
            if (label == 'Duration') {
                i_Duration_decimal = value
            }
            if (label == 'Emp ID') {
                i_EmpIntID = value
            }
            
            if (label == 'Dayofweek') {/////Added for sunday hrs
                i_Dayofweek = value
            }
            //
        }
        if (i_Calendar_Week != s_beforedate) {
            i_Calendar_Week = nlapiStringToDate(i_Calendar_Week);
            i_Calendar_Week = nlapiAddDays(i_Calendar_Week, 1)
        }
        //************** pass the date as parameter to getWeekNumber function *****************//
        var currentdateweek = getWeekNumber(i_Calendar_Week)
        
        ////**************Calculate the week of the Dayofweek column to get if there is any sunday filled time sheets with not matching weekno. (Not required as logic changed to Sunday-Saturday)
        /////Added for sunday hrs/////////////////////////////////////////////////
        var i_Calendar_Week_Dayofweekdate = nlapiStringToDate(i_Dayofweek);
        var Dayofweekno = getWeekNumber(i_Calendar_Week_Dayofweekdate);
        //nlapiLogExecution('DEBUG', 'SearchData', 'Dayofweekno===' + Dayofweekno)
        /////////////////////////////new code added to consider sunday also in same week missed in search/////////////////////////////////////
        
        i_Service_Item_Name = i_Service_Item_Name.toString().toUpperCase();
        
        
        
        if (s_id == i_empid) //
        {
            //nlapiLogExecution('DEBUG', 'SearchData', 's_id===' + s_id)
            if (i_Service_Item_Name == s_OTTask) {
                //nlapiLogExecution('DEBUG', 'SearchData', 'i_Project_Task_Name===' + i_Project_Task_Name)
                //nlapiLogExecution('DEBUG', 'SearchData', 'i_Duration_decimal===' + i_Duration_decimal)
                if (startdateweek == currentdateweek) {
                    i_Wk1OtHrs = parseFloat(i_Wk1OtHrs) + parseFloat(i_Duration_decimal)
                    //i_Wk1OtHrs = i_Duration_decimal
                }
                else 
                    if ((currentdateweek == startdateweek + 1) || (startdateweek != 1 && currentdateweek == 1)) {
                        i_Wk2OtHrs = parseFloat(i_Wk2OtHrs) + parseFloat(i_Duration_decimal)
                    //i_Wk2OtHrs = i_Duration_decimal
                    }
            }
            else 
                if (a_STTask.indexOf(i_Service_Item_Name) != -1) {
                    //nlapiLogExecution('DEBUG', 'SearchData', 'i_Project_Task_Name===' + i_Project_Task_Name)
                    //nlapiLogExecution('DEBUG', 'SearchData', 'i_Duration_decimal===' + i_Duration_decimal)
                    if (startdateweek == currentdateweek) {
                        i_Wk1STHrs = parseFloat(i_Wk1STHrs) + parseFloat(i_Duration_decimal)
                    //i_Wk1STHrs = i_Duration_decimal
                    //nlapiLogExecution('DEBUG', 'SearchData', 'i_Wk1STHrs===' + i_Wk1STHrs)
                    }
                    else 
                        if ((currentdateweek == startdateweek + 1) || (startdateweek != 1 && currentdateweek == 1)) {
                            i_Wk2STHrs = parseFloat(i_Wk2STHrs) + parseFloat(i_Duration_decimal)
                        //i_Wk2STHrs = i_Duration_decimal
                        //nlapiLogExecution('DEBUG', 'SearchData', 'i_Wk2STHrs===' + i_Wk2STHrs)
                        }
                }
                else {
                    if (i_Service_Item_Name == s_HolidayTask) {
                        //nlapiLogExecution('DEBUG', 'SearchData', 'i_Project_Task_Name===' + i_Project_Task_Name)
                        if (startdateweek == currentdateweek) {
                            i_Wk1HLHrs = parseFloat(i_Wk1HLHrs) + parseFloat(i_Duration_decimal)
                        //i_Wk1HLHrs = i_Duration_decimal
                        }
                        else 
                            if ((currentdateweek == startdateweek + 1) || (startdateweek != 1 && currentdateweek == 1)) {
                                i_Wk2HLHrs = parseFloat(i_Wk2HLHrs) + parseFloat(i_Duration_decimal)
                            //i_Wk2HLHrs = i_Duration_decimal
                            }
                    }
                    else 
                        if (i_Service_Item_Name == s_FHTask) {
                            if (startdateweek == currentdateweek) {
                                i_Wk1FLHrs = parseFloat(i_Wk1FLHrs) + parseFloat(i_Duration_decimal)
                            //i_Wk1FLHrs = i_Duration_decimal
                            }
                            else 
                                if ((currentdateweek == startdateweek + 1) || (startdateweek != 1 && currentdateweek == 1)) {
                                    i_Wk2FLHrs = parseFloat(i_Wk2FLHrs) + parseFloat(i_Duration_decimal)
                                //i_Wk2FLHrs = i_Duration_decimal
                                }
                        }
                }
        }
        else {
            //nlapiLogExecution('DEBUG', 'SearchData', 's_id===' + s_id)
            
            //nlapiLogExecution('DEBUG', 'SearchData', 'i_Wk2STHrs===' + i_Wk2STHrs)
            //nlapiLogExecution('DEBUG', 'SearchData', 'i_Wk1STHrs===' + i_Wk1STHrs)
            
            //nlapiLogExecution('DEBUG', 'SearchData', 'i_Wk1OtHrs===' + i_Wk1OtHrs + "i_Wk1STHrs====" + i_Wk1STHrs + "i_Wk1HLHrs===" + i_Wk1HLHrs)
            if (i_Wk1OtHrs == 0 && (i_Wk1STHrs == 40 || i_Wk1STHrs == 0)) {
            
                //nlapiLogExecution('DEBUG', 'SearchData', 'b_entryFlag loop 1')
                b_entryFlag = false;
                
            }
            else {
                //nlapiLogExecution('DEBUG', 'SearchData', 'b_entryFlag loop 2 ')
                b_entryFlag = true;
            }
            
            if (b_entryFlag != true) //
            {
                //nlapiLogExecution('DEBUG', 'SearchData', 'i_Wk2OtHrs===' + i_Wk2OtHrs + "i_Wk2STHrs====" + i_Wk2STHrs + "i_Wk2HLHrs===" + i_Wk2HLHrs)
                if (i_Wk2OtHrs == 0 && (i_Wk2STHrs == 40 || i_Wk2STHrs == 0)) //
                {
                    //nlapiLogExecution('DEBUG', 'SearchData', 'b_entryFlag wk 2  loop 1 ')
                    b_entryFlag = false;
                }
                else //
                {
                    //nlapiLogExecution('DEBUG', 'SearchData', 'b_entryFlag wk 2  loop 2 ')
                    b_entryFlag = true;
                }
            }
            //nlapiLogExecution('DEBUG', 'SearchData', 'b_entryFlag===' + b_entryFlag)
            return b_entryFlag;
        }
    }
    //nlapiLogExecution('DEBUG', 'SearchData', 'a_excludeArray===' + a_excludeArray)
}