function update_VB()
{
	try
	{
		var current_date = nlapiDateToString(new Date());
		//Log for current date
		nlapiLogExecution('DEBUG', 'Execution Started Current Date', 'current_date...' + current_date);

		var timestp = timestamp();
		nlapiLogExecution('DEBUG', 'Execution Started Current Date', 'timestp...' + timestp);
		var counter ='';
		var context = nlapiGetContext();
		
		var je_list = new Array();
		var filter = new Array();
		var a_results_je = searchRecord('transaction', 'customsearch2232', null, null);
		if (_logValidation(a_results_je))
		{	
			for(var counter = 0; counter<a_results_je.length ; counter++)
			{
				nlapiLogExecution('audit','j:-- ',counter);
				nlapiLogExecution('audit','a_results_je:-- ',a_results_je.length);
				
					nlapiLogExecution('audit','a_results_je[i].getId():-- ',a_results_je[counter].getId());
					
					if(je_list.indexOf(a_results_je[counter].getId())>=0)
					{
						
					}
					else
					{
                    //  yieldScript(context);
						var o_je_rcrd = nlapiLoadRecord('vendorbill',a_results_je[counter].getId());//332853
						//var projectVal = o_je_rcrd.getFieldValue('job');
						//var projectLook = nlapiLookupField('job',projectVal,['custentity_practice']);
					//	var department_ = projectLook.custentity_practice;
						//o_je_rcrd.setFieldText('approvalstatus','Approved');
                    //  o_je_rcrd.setFieldText('currency','USD');//currency
						//o_je_rcrd.setFieldText('approvalstatus','Pending Approval');
						//var i_expense_count = o_je_rcrd.getLineItemCount('expense');
						
						
                  	//    o_je_rcrd.setFieldValue('currency','1');
					//	o_je_rcrd.setFieldValue('status','Approved');
					//	o_je_rcrd.setFieldValue('approvalstatus','2');
						//	o_je_rcrd.setFieldText('status','Approved');
						//o_je_rcrd.setFieldValue('approvalstatus','2');
					//	o_je_rcrd.setFieldValue('department',department_);
                   //   o_je_rcrd.setFieldValue('tranid','IN-UK00382370');
                    //  o_je_rcrd.setFieldValue('custbody_vendorinvoicedate','11/15/2017');
                   //   o_je_rcrd.setFieldValue('custbody_invoicenumber','IN-UK00382370');
                      
						var je_submit_id = nlapiSubmitRecord(o_je_rcrd,false,true);
						nlapiLogExecution('audit','je_submit_id:-- ',je_submit_id);
						je_list.push(je_submit_id);
					}
					
					yieldScript(context);
			}
			//nlapiLogExecution('DEBUG', 'usageEnd =' + usageEnd + '-->j-->' + counter);
			
		}
			var timestp_E = timestamp();
			nlapiLogExecution('DEBUG', 'Execution Ended Current Date', 'timestp_E...' + timestp_E);
	}
	catch(err)
	{
		nlapiLogExecution('ERROR','ERROR MESSAGE:-- ',err);
	}
}

function yieldScript(currentContext) {

		nlapiLogExecution('AUDIT', 'API Limit Exceeded');
		var state = nlapiYieldScript();

		if (state.status == "FAILURE") {
			nlapiLogExecution('ERROR', 'Yield Script Failed', 'Reason : '
			        + state.reason + ' / Size : ' + state.size);
			return false;
		} else if (state.status == "RESUME") {
			nlapiLogExecution('AUDIT', 'Script Resumed');
		}
}

function Schedulescriptafterusageexceeded()
{
    try
    {
        var params = new Array();
        var startDate = new Date();
        params['startdate'] = startDate.toUTCString();
        var status = nlapiScheduleScript(nlapiGetContext().getScriptId(), nlapiGetContext().getDeploymentId(), params);
        //nlapiLogExecution('DEBUG', 'After Scheduling', 'Script scheduled status=' + status);
    } 
    catch (e) 
    {
        nlapiLogExecution('DEBUG', 'In Scheduled Catch', 'e : ' + e.message);
    }
}

function _logValidation(value){
    if (value != null && value != '' && value != undefined && value.toString() != 'NaN' && value != NaN) {
        return true;
    }
    else {
        return false;
    }
}

function searchRecord(recordType, savedSearch, arrFilters, arrColumns,
        filterExpression)
{

	try {
		var search = null;

		// if a saved search is provided, load it and add the filters and
		// columns
		if (isNotEmpty(savedSearch)) {
			search = nlapiLoadSearch(recordType, savedSearch);

			if (isArrayNotEmpty(arrFilters)) {
				search.addFilters(arrFilters);
			}

			if (isArrayNotEmpty(arrColumns)) {
				search.addColumns(arrColumns);
			}

			if (isArrayNotEmpty(filterExpression)) {
				search.setFilterExpression(filterExpression);
			}
		}
		// create a new search
		else {
			search = nlapiCreateSearch(recordType, arrFilters, arrColumns);
		}

		// run search
		var resultSet = search.runSearch();

		// iterate through the search and get all data 1000 at a time
		var searchResultCount = 0;
		var resultSlice = null;
		var searchResult = [];

		do {
			resultSlice = resultSet.getResults(searchResultCount,
			        searchResultCount + 1000);

			if (resultSlice) {

				resultSlice.forEach(function(result) {

					searchResult.push(result);
					searchResultCount++;
				});
			}
		} while (isArrayNotEmpty(resultSlice) && resultSlice.length >= 1000);

		return searchResult;
	} catch (err) {
		nlapiLogExecution('ERROR', 'searchRecord', err);
		throw err;
	}
}

function isEmpty(value) {

	return value == null || value == "" || typeof (value) == undefined;
}

function isNotEmpty(value) {

	return !isEmpty(value);
}

function isArrayEmpty(argArray) {

	return !isArrayNotEmpty(argArray);
}

function isArrayNotEmpty(argArray) {

	return (isNotEmpty(argArray) && argArray.length > 0);
}
function timestamp() {
var str = "";

var currentTime = new Date();
var hours = currentTime.getHours();
var minutes = currentTime.getMinutes();
var seconds = currentTime.getSeconds();
var meridian = "";
if (hours > 12) {
    meridian += "pm";
} else {
    meridian += "am";
}
if (hours > 12) {

    hours = hours - 12;
}
if (minutes < 10) {
    minutes = "0" + minutes;
}
if (seconds < 10) {
    seconds = "0" + seconds;
}
str += hours + ":" + minutes + ":" + seconds + " ";

return str + meridian;
}