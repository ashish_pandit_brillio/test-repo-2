/**
 * @author Jayesh
 */

function before_load(type)
{
	if(type == 'view')
	{
		if(nlapiGetFieldValue('subsidiary') == 3 || nlapiGetFieldValue('subsidiary') == 9)
		{
			if(nlapiGetFieldValue('custbody_maker_checker_approval_status') == 1)
			{
				if(nlapiGetUser() == nlapiGetFieldValue('custbody_invoice_approver')) //39108
				{
					form.setScript('customscript_cli_sourceamnt_for_approval');
				
					form.addButton('custpage_approve', 'Approve', 'custom_approval(\'  ' + nlapiGetRecordId() + '  \');');
				}
			}
		}
	}
	if(type == 'create')
	{
		nlapiSetFieldValue('custbody_maker_checker_approval_status',1);
		
		if(nlapiGetUser() != 5764)
		{
			nlapiSetFieldValue('custbody_invoice_approver',5764);
		}
		else
		{
			nlapiSetFieldValue('custbody_invoice_approver',1615);
		}
	}
}

function before_submit()
{
	if(nlapiGetFieldValue('subsidiary') == 3 || nlapiGetFieldValue('subsidiary') == 9)
	{
		if(nlapiGetFieldValue('custbody_maker_checker_approval_status') == 1)
		{
			var i_expense_line_count = nlapiGetLineItemCount('expense');
		
			for(var i_index_expense=1; i_index_expense<=i_expense_line_count; i_index_expense++)
			{
				var f_line_amount = nlapiGetLineItemValue('expense','amount',i_index_expense);
				var f_tax_amount = nlapiGetLineItemValue('expense','tax1amt',i_index_expense);
				var f_approval_amount = nlapiGetLineItemValue('expense','custcol_maker_checker_approval_amount',i_index_expense);
				
				if(parseFloat(f_line_amount) != parseFloat(f_approval_amount))
				{
					nlapiSelectLineItem('expense',i_index_expense);
					nlapiSetCurrentLineItemValue('expense','amount',0);
					nlapiSetCurrentLineItemValue('expense','custcol_maker_checker_approval_amount',f_line_amount);
					nlapiSetCurrentLineItemValue('expense','custcol_makerchecker_approval_taxamnt',f_tax_amount);
					nlapiCommitLineItem('expense');
				}
			}
		
			var i_item_line_count = nlapiGetLineItemCount('item');
		
			for(var i_index_item=1; i_index_item<=i_item_line_count; i_index_item++)
			{
				var f_line_amount = nlapiGetLineItemValue('item','amount',i_index_item);
				var f_tax_amount = nlapiGetLineItemValue('item','tax1amt',i_index_item);
				var f_approval_amount = nlapiGetLineItemValue('item','custcol_maker_checker_approval_amount',i_index_expense);
				
				if(parseFloat(f_line_amount) != parseFloat(f_approval_amount))
				{
					nlapiSelectLineItem('item',i_index_item);
					nlapiSetCurrentLineItemValue('item','amount',0);
					nlapiSetCurrentLineItemValue('item','custcol_maker_checker_approval_amount',f_line_amount);
					nlapiSetCurrentLineItemValue('item','custcol_makerchecker_approval_taxamnt',f_tax_amount);
					nlapiCommitLineItem('item');
				}
			}
		}
	}
}
