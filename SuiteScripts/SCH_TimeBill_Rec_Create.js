// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
/*
   	Script Name:SCH_TimeBill_Rec_Create.js
	Author:Swati Kurariya
	Company:Aashna CloudTech Pvt. Ltd
	Date:
    Description:-Create/Delete Monthly_Provision_Allocation custom record based on condition.

	Script Modification Log:

	-- Date --			-- Modified By --				--Requested By--				-- Description --
    04-04-2015            swati Kurariya                  vaibhav                        Create the delete function to delete
                                                                                         Monthly_Provision_Allocation Custom record.



Below is a summary of the process controls enforced by this script file.  The control logic is described
more fully, below, in the appropriate function headers and code blocks.


     SCHEDULED FUNCTION
		- scheduledFunction(type)


     SUB-FUNCTIONS
		- The following sub-functions are called by the above core functions in order to maintain code
            modularization:

               - NOT USED

*/
}
// END SCRIPT DESCRIPTION BLOCK  ====================================


// BEGIN SCHEDULED FUNCTION =============================================
function time_bill_rec_create(type)
{//fun start
	try {
        var receipentsArray = new Array()
        receipentsArray.push('netsuite.support@brillio.com');
		nlapiLogExecution('DEBUG','working');
	var i_context = nlapiGetContext();
	//--------------------------get value---------------------------------------------------
	 var i_from_date = i_context.getSetting('SCRIPT','custscript_sch_from_date');
	 var i_to_date = i_context.getSetting('SCRIPT','custscript_sch_to_date');
	 var i_user_subsidiary = parseInt(i_context.getSetting('SCRIPT','custscript_sch_subsidiary'));
	 var i_criteria = parseInt(i_context.getSetting('SCRIPT','custscript_criteria'));
	 var i_count = parseInt(i_context.getSetting('SCRIPT','custscript_count'));
	 var i_month = parseInt(i_context.getSetting('SCRIPT','custscript_month_id'));
	 var i_year = parseInt(i_context.getSetting('SCRIPT','custscript_year_id'));
	 
	 if(i_count == null || i_count == '')
		 {
		 i_count=0;
		 }
	 if(isNaN(i_count))
		 {
		 i_count=0;
		 }
	 
	 nlapiLogExecution('DEBUG', 'suiteletFunction','i_from_date -->' + i_from_date);
	 nlapiLogExecution('DEBUG', 'suiteletFunction','i_to_date -->' + i_to_date);
	 nlapiLogExecution('DEBUG', 'suiteletFunction','i_user_subsidiary -->' + i_user_subsidiary);
	 nlapiLogExecution('DEBUG', 'suiteletFunction','i_criteria -->' + i_criteria);
	 nlapiLogExecution('DEBUG', 'suiteletFunction','i_month -->' + i_month);
	 nlapiLogExecution('DEBUG', 'suiteletFunction','i_year -->' + i_year);
	//-------------------------------------------------------------------------------------------
	if(i_criteria == 1)
	{//1 if start
		 nlapiLogExecution('DEBUG', 'suiteletFunction','1   i_criteria -->' + i_criteria);
	 //-----------------------------getting the value of search---------------------------------
	    var filter = new Array();
	    filter[0] = new nlobjSearchFilter('date', null, 'within', i_from_date,i_to_date);	
		filter[1] = new nlobjSearchFilter('subsidiary', 'customer', 'is', parseInt(i_user_subsidiary));
		var i_search_results = searchRecord('timebill','customsearch_unbilled_approved_new_tb',filter,null);
		//nlapiLogExecution('DEBUG', 'suiteletFunction',' Not submitted - Allocated -->' + i_search_results.length);
		
		var filter1 = new Array();
	   //filter1[0] = new nlobjSearchFilter('custrecord_from_date', null, 'within', i_from_date,i_to_date);	
	   //filter1[1] = new nlobjSearchFilter('custrecord_to_date', null, 'within', i_from_date,i_to_date);
	    filter1[0] = new nlobjSearchFilter('custrecord_subsidiary',null, 'is', parseInt(i_user_subsidiary));
		filter1[1] = new nlobjSearchFilter('custrecord_criteria',null, 'is', i_criteria);
		filter1[2] = new nlobjSearchFilter('custrecord_month_val',null, 'is', i_month);
		filter1[3] = new nlobjSearchFilter('custrecord_year_val',null, 'is', i_year);
		
		
		var search_time_bill_count=searchRecord('customrecord_time_bill_rec',null,filter1,null);
		//nlapiLogExecution('DEBUG', 'suiteletFunction',' search_time_bill_count -->' + search_time_bill_count.length);
	}//1 if close
	else if(i_criteria == 2)
	{//2 if start
		 nlapiLogExecution('DEBUG', 'suiteletFunction','2   i_criteria -->' + i_criteria);
	 //-----------------------------getting the value of search---------------------------------
	    var filter = new Array();
	    filter[0] = new nlobjSearchFilter('date', null, 'within', i_from_date,i_to_date);	
		filter[1] = new nlobjSearchFilter('subsidiary', 'customer', 'is', parseInt(i_user_subsidiary));
		var i_search_results = searchRecord('timebill','customsearch_submitted_not_appr_new_tb',filter,null);
		nlapiLogExecution('DEBUG', 'suiteletFunction',' Not submitted - Allocated -->' + i_search_results.length);
		
		var filter1 = new Array();
	    //filter1[0] = new nlobjSearchFilter('custrecord_from_date', null, 'on', i_from_date);	
	   // filter1[1] = new nlobjSearchFilter('custrecord_to_date', null, 'on', i_to_date);
	    filter1[0] = new nlobjSearchFilter('custrecord_subsidiary',null, 'is', parseInt(i_user_subsidiary));
		filter1[1] = new nlobjSearchFilter('custrecord_criteria',null, 'is', i_criteria);
		filter1[2] = new nlobjSearchFilter('custrecord_month_val',null, 'is', i_month);
		filter1[3] = new nlobjSearchFilter('custrecord_year_val',null, 'is', i_year);
		var search_time_bill_count=searchRecord('customrecord_time_bill_rec',null,filter1,null);
	}//2 if close
	else
	{//2 else start
		 nlapiLogExecution('DEBUG', 'suiteletFunction','3   i_criteria -->' + i_criteria);
	 //-----------------------------getting the value of search---------------------------------
	    var filter = new Array();
	    filter[0] = new nlobjSearchFilter('date', null, 'within', i_from_date,i_to_date);	
		filter[1] = new nlobjSearchFilter('subsidiary', 'customer', 'is', parseInt(i_user_subsidiary));
		var i_search_results = searchRecord('timebill','customsearch_notsubmitted_new_timesheet_',filter,null);
		nlapiLogExecution('DEBUG', 'suiteletFunction',' Not submitted - Allocated -->' + i_search_results.length);
		
		var filter1 = new Array();
	    //filter1[0] = new nlobjSearchFilter('custrecord_from_date', null, 'on', i_from_date);	
	    //filter1[1] = new nlobjSearchFilter('custrecord_to_date', null, 'on', i_to_date);
	    filter1[0] = new nlobjSearchFilter('custrecord_subsidiary',null, 'is', parseInt(i_user_subsidiary));
	    filter1[1] = new nlobjSearchFilter('custrecord_criteria',null, 'is', i_criteria);
	    filter1[2] = new nlobjSearchFilter('custrecord_month_val',null, 'is', i_month);
		filter1[3] = new nlobjSearchFilter('custrecord_year_val',null, 'is', i_year);
		var search_time_bill_count=searchRecord('customrecord_time_bill_rec',null,filter1,null);
	}//2 else close
		
	 //-----------------------------------------------------------------------------------------
	if(_logValidation(search_time_bill_count) && i_count == '0')
	{//1 if start
		
		Delete_Time_Bill_Record(i_from_date,i_to_date,i_month,i_year,i_user_subsidiary,i_criteria);
		
	}//1 if close
    //-----------------------------------------------------------------------------------------	
   if(!_logValidation(search_time_bill_count))
	 {//2 if start
		
	
		//nlapiLogExecution('DEBUG','search_time_bill_count=======================',i_criteria);
	    if(i_criteria == 1 || i_criteria == 2)
		{//2 if start
	    	
	    	nlapiLogExecution('DEBUG','i_criteria',i_criteria);
		   //------------------------------------------------------------------------------------------
		    if(i_search_results != null)
			{//1 if start
			   
				for(var c=i_count,ss=1;c<i_search_results.length;c++,ss++)		
				{//for start  
				
					var a_search_transaction_result = i_search_results[c];
					var columns=a_search_transaction_result.getAllColumns();
					
					var i_date=a_search_transaction_result.getValue(columns[0]);
					var i_employee=a_search_transaction_result.getValue(columns[1]);
					var i_project=a_search_transaction_result.getValue(columns[2]);
					
					var i_rate=a_search_transaction_result.getValue(columns[3]);
					var i_hour=a_search_transaction_result.getValue(columns[4]);
					
					var i_practice=a_search_transaction_result.getValue(columns[5]);
					var i_customer=a_search_transaction_result.getValue(columns[6]);
					var i_vertical=a_search_transaction_result.getValue(columns[7]);
					var i_subsidiary=i_user_subsidiary;//a_search_transaction_result.getValue(columns[10]);
					
					
					var i_currency=a_search_transaction_result.getValue(columns[8]);
					
					var i_location=a_search_transaction_result.getValue(columns[9]);
					
					var i_item=a_search_transaction_result.getValue(columns[10]);
				
					/*nlapiLogExecution('DEBUG','i_date==========>',i_date);
					nlapiLogExecution('DEBUG','i_employee===========>',i_employee);
					nlapiLogExecution('DEBUG','i_project===========>',i_project);
					nlapiLogExecution('DEBUG','i_rate==========>',i_rate);
					nlapiLogExecution('DEBUG','i_hour=========>',i_hour);
					nlapiLogExecution('DEBUG','i_practice========>',i_practice);
					nlapiLogExecution('DEBUG','i_customer========>',i_customer);
					nlapiLogExecution('DEBUG','i_vertical=========>',i_vertical);
					nlapiLogExecution('DEBUG','i_subsidiary==========>',i_subsidiary);
					nlapiLogExecution('DEBUG','i_location==========>',i_location);
					nlapiLogExecution('DEBUG','i_item==========>',i_item);*/
					//---------------------------create time bill custom record---------
					if(i_project == '4479' ||i_project == '4407' )
					{
					}
					else
					{
                      yieldScript(i_context);
					//bill rate set in user event script
					var time_bill_custom_rec=nlapiCreateRecord('customrecord_time_bill_rec');
					    
					time_bill_custom_rec.setFieldValue('custrecord_month_of_date',i_date);
					time_bill_custom_rec.setFieldValue('custrecord_employee_id',i_employee);
					time_bill_custom_rec.setFieldValue('custrecord_project_id',i_project);
					time_bill_custom_rec.setFieldValue('custrecord_practics',i_practice);
					time_bill_custom_rec.setFieldValue('custrecord_subsidiary',i_subsidiary);
					time_bill_custom_rec.setFieldValue('custrecord_hour',i_hour);
					time_bill_custom_rec.setFieldValue('custrecord_customer',i_customer);
					time_bill_custom_rec.setFieldValue('custrecord_from_date',i_from_date);
					time_bill_custom_rec.setFieldValue('custrecord_location',i_location);
					time_bill_custom_rec.setFieldValue('custrecord_to_date',i_to_date);
					time_bill_custom_rec.setFieldValue('custrecord_criteria',i_criteria);
					time_bill_custom_rec.setFieldValue('custrecord_item_list',i_item);
					time_bill_custom_rec.setFieldValue('custrecord_billable_rate',i_rate);
					time_bill_custom_rec.setFieldValue('custrecord_project_vertical',i_vertical);
					time_bill_custom_rec.setFieldValue('custrecord_project_currency',i_currency);
					time_bill_custom_rec.setFieldValue('custrecord_month_val',i_month);
					time_bill_custom_rec.setFieldValue('custrecord_year_val',i_year);
					
					//time_bill_custom_rec.setFieldValue('custrecord_billable',b_Resource_Billable);
					//time_bill_custom_rec.setFieldValue('custrecord_billable_rate',i_Billable_Rate);
					
					
					
					var time_bill_custome_rec_id=nlapiSubmitRecord(time_bill_custom_rec);
					nlapiLogExecution('DEBUG','time_bill_custome_rec_id======>',time_bill_custome_rec_id);
					}
					//------------------------------------------------------------------
					
					var count='';
					if(ss == '600')
						{
						nlapiLogExecution('DEBUG','script reschedule');
						nlapiLogExecution('DEBUG','c',c);
						    count=parseInt(c)+1;
							var params_1=new Array();
						   // params_1['custscript_current_date_sch'] = i_current_date
						    //----------------------------------------------------------------
						    params_1['custscript_sch_from_date'] = i_from_date
						    params_1['custscript_sch_to_date'] = i_to_date
						    params_1['custscript_count'] = count;
						    //----------------------------------------------------------------
						
						    params_1['custscript_sch_subsidiary'] = i_user_subsidiary
						    params_1['custscript_criteria'] = i_criteria;
						    params_1['custscript_month_id'] = i_month;
							params_1['custscript_year_id'] = i_year;
							
					        var status=nlapiScheduleScript('customscript_sch_timebill_rec_create',null,params_1);
					        if(status == 'QUEUED')
					        	{
					        	break;
					        	}
						}
					
					
				}//for close
			
			}//1 if close
		   //-----------------------------------------------------------------------------------------
		}
		else if(i_criteria == 3)
		{//2 if start
		   if(i_search_results != null)
			{//1 if start
			   
				for(var c=i_count;c<i_search_results.length;c++)		
				{//for start  
				
					var a_search_transaction_result = i_search_results[c];
					var columns=a_search_transaction_result.getAllColumns();
					
					var i_date=a_search_transaction_result.getValue(columns[0]);
					var i_employee=a_search_transaction_result.getValue(columns[12]);
					var i_project=a_search_transaction_result.getValue(columns[2]);
					var i_practice=a_search_transaction_result.getValue(columns[9]);
					var i_subsidiary=i_user_subsidiary;//a_search_transaction_result.getValue(columns[10]);
					var i_sun_of_sum_allocated=a_search_transaction_result.getValue(columns[3]);
					var i_sun_of_billed=a_search_transaction_result.getValue(columns[4]);
					var i_sun_of_unbilled=a_search_transaction_result.getValue(columns[5]);
					var i_sum_of_not_submitted=a_search_transaction_result.getValue(columns[6]);
					
					var i_customer=a_search_transaction_result.getValue(columns[11]);
					
					var i_billing_start_date=a_search_transaction_result.getValue(columns[15]);
					var i_billing_end_date=a_search_transaction_result.getValue(columns[16]);
					var i_percentage_of_time=a_search_transaction_result.getValue(columns[17]);
					
					var b_Resource_Billable=a_search_transaction_result.getValue(columns[18]);
					var i_Billable_Rate=a_search_transaction_result.getValue(columns[19]);
					
					var onsite_offsite =a_search_transaction_result.getValue(columns[20]);
					     var OnsiteHRS =a_search_transaction_result.getValue(columns[21]); //1
					    var OffsiteHrs =a_search_transaction_result.getValue(columns[22]); //2
					
					
					//-------------------------------net days calculate----------------------
					if(_logValidation(i_billing_start_date) && _logValidation(i_billing_end_date))
						{
					if((Date.parse(i_billing_start_date) <= Date.parse(i_to_date)) && (Date.parse(i_billing_start_date) >= Date.parse(i_from_date)))
					{
						flag_1 = 1 ;
						nlapiLogExecution('DEBUG', ' get_allocation_details',' Start Date is present ......');	
						var start_date=i_billing_start_date;
					} 
				
				    if((Date.parse(i_billing_end_date) >= Date.parse(i_from_date)) && (Date.parse(i_billing_end_date) <= Date.parse(i_to_date)))
					{
						flag_2 = 1;
						nlapiLogExecution('DEBUG', ' get_allocation_details',' End Date is present ......');	
						var end_date=i_billing_end_date;
					} 
				    if((Date.parse(i_billing_start_date) < Date.parse(i_from_date)) && (Date.parse(i_billing_start_date) < Date.parse(i_from_date)))
					{
						flag_2 = 1;
						nlapiLogExecution('DEBUG', ' get_allocation_details',' End Date is present ......');	
						var end_date=i_from_date;
					} 
				    if((Date.parse(i_billing_end_date) > Date.parse(i_from_date)) && (Date.parse(i_billing_start_date) > Date.parse(i_from_date)))
					{
						flag_2 = 1;
						nlapiLogExecution('DEBUG', ' get_allocation_details',' End Date is present ......');	
						var end_date=i_to_date;
					}
				    
				    
				    	var start_date=i_billing_start_date;
				    	var end_date=i_billing_end_date;
				   
				    nlapiLogExecution('DEBUG','b_Resource_Billable',b_Resource_Billable);
				    nlapiLogExecution('DEBUG','i_Billable_Rate',i_Billable_Rate);
				    
				    nlapiLogExecution('DEBUG','end_date',end_date);
				    nlapiLogExecution('DEBUG','end_date',end_date);
				    
				    var  i_total_days = getDatediffIndays(start_date,end_date);
					nlapiLogExecution('DEBUG','i_total_days',i_total_days);
					
					var  i_total_sat_sun = getWeekend(start_date,end_date);	
					nlapiLogExecution('DEBUG','i_total_days',i_total_days);
					
					 var net_billable_days=(parseInt(i_total_days) - parseInt(i_total_sat_sun));	
					nlapiLogExecution('DEBUG','net_billable_days',net_billable_days);
					
	                var allocated_billable_hrs="";
					if(onsite_offsite == "1")
					{
						allocated_billable_hrs=(parseInt(i_total_days) - parseInt(i_total_sat_sun))* parseInt(OnsiteHRS);	
						nlapiLogExecution('DEBUG','OnsiteHRS allocated_billable_hrs',allocated_billable_hrs);
					}
					else if(onsite_offsite == "2"){
						allocated_billable_hrs=(parseInt(i_total_days) - parseInt(i_total_sat_sun))*parseInt(OffsiteHrs);	
						nlapiLogExecution('DEBUG','OffsiteHrs allocated_billable_hrs',allocated_billable_hrs);
					}
						}
					else
						{
						i_billing_start_date='';
						i_billing_end_date='';
						}
				    
				    //------------------------------------------------------------------------
					nlapiLogExecution('DEBUG','i_date==========>',i_date);
					nlapiLogExecution('DEBUG','i_employee===========>',i_employee);
					nlapiLogExecution('DEBUG','i_project===========>',i_project);
					nlapiLogExecution('DEBUG','i_practice==========>',i_practice);
					nlapiLogExecution('DEBUG','i_subsidiary=========>',i_subsidiary);
					nlapiLogExecution('DEBUG','i_sun_of_billed========>',i_sun_of_billed);
					nlapiLogExecution('DEBUG','i_sun_of_unbilled========>',i_sun_of_unbilled);
					nlapiLogExecution('DEBUG','i_sum_of_not_submitted=========>',i_sum_of_not_submitted);
					nlapiLogExecution('DEBUG','i_customer==========>',i_customer);
					nlapiLogExecution('DEBUG','i_billing_start_date==========>',i_billing_start_date);
					nlapiLogExecution('DEBUG','i_billing_end_date==========>',i_billing_end_date);
					nlapiLogExecution('DEBUG','i_percentage_of_time==========>',i_percentage_of_time);
					//---------------------------create time bill custom record---------
					if(i_project == '4479' ||i_project == '4407' ||i_project == '4408')
					{
					}
					else
						{
						
						yieldScript(i_context);
					
					var time_bill_custom_rec=nlapiCreateRecord('customrecord_time_bill_rec');
					    
					time_bill_custom_rec.setFieldValue('custrecord_month_of_date',i_date);
					time_bill_custom_rec.setFieldValue('custrecord_employee_id',i_employee);
					time_bill_custom_rec.setFieldValue('custrecord_project_id',i_project);
					time_bill_custom_rec.setFieldValue('custrecord_practics',i_practice);
					time_bill_custom_rec.setFieldValue('custrecord_subsidiary',i_subsidiary);
					time_bill_custom_rec.setFieldValue('custrecord_sum_of_allocated',i_sun_of_sum_allocated);
					time_bill_custom_rec.setFieldValue('custrecord_sum_of_biiled',i_sun_of_billed);
					time_bill_custom_rec.setFieldValue('custrecord_sum_of_unbilled',i_sun_of_unbilled);
					time_bill_custom_rec.setFieldValue('custrecord_sum_of_not_submitted',i_sum_of_not_submitted);
					time_bill_custom_rec.setFieldValue('custrecord_customer',i_customer);
					time_bill_custom_rec.setFieldValue('custrecord_from_date',i_from_date);
					time_bill_custom_rec.setFieldValue('custrecord_to_date',i_to_date);
					time_bill_custom_rec.setFieldValue('custrecord_criteria',i_criteria);
					
					time_bill_custom_rec.setFieldValue('custrecord_billing_start_date',i_billing_start_date);
					time_bill_custom_rec.setFieldValue('custrecord_billing_end_date',i_billing_end_date);
					
					time_bill_custom_rec.setFieldValue('custrecord_percent_of_time',i_percentage_of_time);
					//time_bill_custom_rec.setFieldValue('custrecord_net_billable_days',net_billable_days);//
					//time_bill_custom_rec.setFieldValue('custrecord_allocated_billable_hours',allocated_billable_hrs);
					
					time_bill_custom_rec.setFieldValue('custrecord_billable',b_Resource_Billable);
					time_bill_custom_rec.setFieldValue('custrecord_billable_rate',i_Billable_Rate);
					
					time_bill_custom_rec.setFieldValue('custrecord_month_val',i_month);
					time_bill_custom_rec.setFieldValue('custrecord_year_val',i_year);
					
					
					
					var time_bill_custome_rec_id=nlapiSubmitRecord(time_bill_custom_rec);
					nlapiLogExecution('DEBUG','time_bill_custome_rec_id======>',time_bill_custome_rec_id);
						}
					//------------------------------------------------------------------
					var count='';
					if(ss == '600')
						{
						    count=parseInt(c)+1;
							var params_1=new Array();
						   // params_1['custscript_current_date_sch'] = i_current_date
						    //----------------------------------------------------------------
						    params_1['custscript_sch_from_date'] = i_from_date
						    params_1['custscript_sch_to_date'] = i_to_date
						    params_1['custscript_count'] = count;
						    //----------------------------------------------------------------
						
						    params_1['custscript_sch_subsidiary'] = i_subsidiary
						    params_1['custscript_criteria'] = i_criteria;
						    params_1['custscript_month_id'] = i_month;
							params_1['custscript_year_id'] = i_year;
							
					        var status=nlapiScheduleScript('customscript_sch_timebill_rec_create',null,params_1);
					        if(status == 'QUEUED')
					        	{
					        	break;
					        	}
						}
					
					
					
				}//for close
			
			}//1 if close
		}//3 if close
	}//2 if close
	 //-------------------
	} catch (error) {
		nlapiLogExecution('ERROR',error.code,error.message);	
		var mailTemplate = "";
		var emailBody = error
		mailTemplate += "<p> This is to inform that the below errror occurred while creation month provision allocation </p>";
		mailTemplate += "<br/>"
		mailTemplate += emailBody
		mailTemplate += "<p>Regards, <br/> Information Systems</p>";
		nlapiSendEmail( '442', receipentsArray, 'Error in creation month provision allocation',
		mailTemplate);       
	}
	}
//fun close
//-----------------------------------------------------------------

//----------------------------------------------------------------------
/*
 * Delete Monthly_Provision_Allocation custom record and respective journal entry,based on some criteria.
 */
function Delete_Time_Bill_Record(i_from_date,i_to_date,month,year,i_user_subsidiary,i_criteria)
{//fun start
	var i_context=nlapiGetContext();
	var i_usage_end = i_context.getRemainingUsage();
	var deleted_jv_ref='';
	
	try
	{//main try start
		
		var filter = new Array();
	    //filter[0] = new nlobjSearchFilter('custrecord_from_date', null, 'on', i_from_date);	
	   // filter[1] = new nlobjSearchFilter('custrecord_to_date', null, 'on', i_to_date);
	    filter[0] = new nlobjSearchFilter('custrecord_subsidiary',null, 'is', parseInt(i_user_subsidiary));
		filter[1] = new nlobjSearchFilter('custrecord_criteria',null, 'is', i_criteria);
		filter[2] = new nlobjSearchFilter('custrecord_month_val',null, 'is', month);
		filter[3] = new nlobjSearchFilter('custrecord_year_val',null, 'is', year);
		
		var column = new Array();
		column[0]=new nlobjSearchColumn('custrecord_journal_entry_ref');
		
		var search_time_bill=searchRecord('customrecord_time_bill_rec',null,filter,column);
		
		//---------------------------------------------------------
		
		var completeResultSet = search_time_bill; //container of the complete result set
		while(search_time_bill.length == 1000)
		{ //re-run the search if limit has been reached
			 var lastId = search_time_bill[999].getValue('internalid'); //note the last record retrieved
			 filter[ij] = new nlobjSearchFilter('internalidnumber',null,'greaterthan',lastId); //create new filter to restrict the next search based on the last record returned
			 search_time_bill = searchRecord('customrecord_time_bill_rec',null,filter,columns);
			 completeResultSet = completeResultSet.concat(search_time_bill); //add the result to the complete result set 
		} 
		
		search_time_bill=completeResultSet;
		//---------------------------------------------------------
		if(_logValidation(search_time_bill))
			{//if start
				nlapiLogExecution('DEBUG', 'DELETE RECORD','search_time_bill.length'+search_time_bill.length);
			   for(var ll=0 ; ll<search_time_bill.length ; ll++)
				   {//for start
				   
				   		var i_Time_Bill_Rec_Id=search_time_bill[ll].getId();
				   		nlapiLogExecution('DEBUG', 'DELETE RECORD','i_Time_Bill_Rec_Id===>'+i_Time_Bill_Rec_Id);
				   		
				   		var i_JV_Id=search_time_bill[ll].getValue('custrecord_journal_entry_ref');
				   		nlapiLogExecution('DEBUG', 'DELETE RECORD','i_JV_Id'+i_JV_Id);
				   		
				   		
						   		if(_logValidation(i_JV_Id))
						   			{//if start
						   			try
						   			   {//1 try start
						   					var Load_Jv_Rec=nlapiLoadRecord('journalentry',i_JV_Id);
						   					var b_Deffer_Entry=Load_Jv_Rec.getFieldValue('reversaldefer');
						   			
								   			if(b_Deffer_Entry == 'T')
								   				{//if start
								   					Load_Jv_Rec.setFieldValue('reversaldefer','F');//custbody_financehead
								   					Load_Jv_Rec.setFieldValue('custbody_financehead','9');//
								   					nlapiSubmitRecord(Load_Jv_Rec);
								   				}//if close
						   			
								   			   
								   				   deleted_jv_ref=deleted_jv_ref+'%%'+i_JV_Id;
								   				   nlapiLogExecution('DEBUG', 'DELETE RECORD','inside try');
								   				   nlapiDeleteRecord('journalentry',i_JV_Id);
								   				 
									   			   
								   			   }//1 try end
								   			   catch(e)
								   			   {//1 catch start
								   				 nlapiLogExecution('ERROR', 'Error', 'Transaction ID '+ i_JV_Id +': '+e.getCode());
								   			   }//1 catch end
						   				
						   			   
						   			
						   			}//if close
				   			
				   		   nlapiLogExecution('DEBUG', 'RECORD DELETED');
				   		   try
				   		   {//2 try start
				   			 nlapiDeleteRecord('customrecord_time_bill_rec',i_Time_Bill_Rec_Id);
				   		   }//2 try end
				   		  catch(ex)
			   			   {//2 catch start
					   			  if(parseInt(ll)+1 == search_time_bill.length)
					   				  {
						   				
								   				 var params_1=new Array();
								   			   // params_1['custscript_current_date_sch'] = i_current_date
								   			    //----------------------------------------------------------------
								   			    params_1['custscript_sch_from_date'] = i_from_date
								   			    params_1['custscript_sch_to_date'] = i_to_date
								   			    params_1['custscript_count'] = '';
								   			    //----------------------------------------------------------------
								   			
								   			    params_1['custscript_sch_subsidiary'] = i_user_subsidiary
								   			    params_1['custscript_criteria'] = i_criteria;
								   			    params_1['custscript_month_id'] = month;
								   				params_1['custscript_year_id'] = year;
								   				
								   				var status=nlapiScheduleScript('customscript_sch_timebill_rec_create','customdeploy1',params_1);
								   			   nlapiLogExecution('ERROR', 'status', status);
								   				if(status == 'QUEUED')
									        	{
									        	  break;
									        	}
								   			 nlapiLogExecution('ERROR', 'Error', 'Transaction ID '+ i_Time_Bill_Rec_Id +': '+ ex);
					   				  }
				   		         
			   			   }//2 catch end
				   		 
				   	  //  nlapiLogExecution('DEBUG', 'DELETE RECORD','i_Deleted_Time_Bill_Id'+i_Deleted_Time_Bill_Id);
				   		//-----------------------reschedule script------------------------
				   		if(i_usage_end < 100)
				   			{
				   			    var status=reschedule_script(i_from_date,i_to_date,i_user_subsidiary,i_criteria,month,year)
					   			if(status == 'QUEUED')
					   	     	{
					   	     	  break;
					   	     	}
				   			}
				   		//----------------------------------------------------------------  
				   }//for close
			
			
			}//if close
		
	}//main try close
	catch(exception)
	{
		nlapiLogExecution('DEBUG', 'ERROR',' Exception Caught -->' + exception);	
					
	}//MAIN CATCH
}//fun close
//------------------------------------------------
function _logValidation(value) 
{
 if(value != null && value.toString() != null && value != '' && value != undefined && value.toString() != undefined && value != 'undefined' && value.toString() != 'undefined'&& value.toString() != 'NaN' && value != NaN) 
 {
  return true;
 }
 else 
 { 
  return false;
 }
}
//-----------------------------------------
function reschedule_script(i_from_date,i_to_date,i_user_subsidiary,i_criteria,month,year)
{
	   var params_1=new Array();
	   // params_1['custscript_current_date_sch'] = i_current_date
	    //----------------------------------------------------------------
	    params_1['custscript_sch_from_date'] = i_from_date
	    params_1['custscript_sch_to_date'] = i_to_date
	    params_1['custscript_count'] = '';
	    //----------------------------------------------------------------
	
	    params_1['custscript_sch_subsidiary'] = i_user_subsidiary
	    params_1['custscript_criteria'] = i_criteria;
	    params_1['custscript_month_id'] = month;
		params_1['custscript_year_id'] = year;
		
		var status=nlapiScheduleScript('customscript_sch_timebill_rec_create',null,params_1);
		return status;
		
}
//------------------------------------------------------------
function getDatediffIndays(startDate, endDate)
{
    var one_day=1000*60*60*24;
    var fromDate = startDate;
    var toDate=endDate

	var date1 = nlapiStringToDate(fromDate);

    var date2 = nlapiStringToDate(toDate);

    var date3=(date2-date1)/one_day;

    return (date3+1);
}
//-----------------------------------------------------------------------
function getWeekend(startDate,endDate)
{
var i_no_of_sat = 0	;
var i_no_of_sun = 0	;

nlapiLogExecution('DEBUG','startDate',startDate);
nlapiLogExecution('DEBUG','endDate',endDate);
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 var date_format = checkDateFormat();

var startDate_1 = startDate
 var endDate_1 = endDate

 startDate = nlapiStringToDate(startDate);
 endDate = nlapiStringToDate(endDate);

 var i_count_day = startDate.getDate();

 var i_count_last_day = endDate.getDate();

 i_month = startDate.getMonth()+1;

 i_year = startDate.getFullYear();

var d_f = new Date();
var getTot = getDatediffIndays(startDate_1, endDate_1); //Get total days in a month

var sat = new Array();   //Declaring array for inserting Saturdays
var sun = new Array();   //Declaring array for inserting Sundays

for(var i=i_count_day;i<=i_count_last_day;i++)
{    //looping through days in month

	 if (date_format == 'YYYY-MM-DD')
	 {
	 	var newDate = i_year + '-' + i_month + '-' + i;
	 }
	 if (date_format == 'DD/MM/YYYY')
	 {
	  	var newDate = i + '/' + i_month + '/' + i_year;
	 }
	 if (date_format == 'MM/DD/YYYY')
	 {
	  	var newDate = i_month + '/' + i + '/' + i_year;
	 }

    newDate = nlapiStringToDate(newDate);

    if(newDate.getDay()==0)
	{   //if Sunday
        sat.push(i);
		i_no_of_sat++;
    }
    if(newDate.getDay()==6)
	{   //if Saturday
        sun.push(i);
		i_no_of_sun++;
    }


}

 var i_total_days_sat_sun = parseInt(i_no_of_sat)+parseInt(i_no_of_sun);
 return i_total_days_sat_sun ;
}
function checkDateFormat()
{
    var context = nlapiGetContext();

	var dateFormatPref = context.getPreference('dateformat');

    return dateFormatPref;
}


function yieldScript(currentContext) {

	if (currentContext.getRemainingUsage() <= 1000) {
		nlapiLogExecution('AUDIT', 'API Limit Exceeded');
		var state = nlapiYieldScript();

		if (state.status == "FAILURE") {
			nlapiLogExecution('ERROR', 'Yield Script Failed', 'Reason : '
			        + state.reason + ' / Size : ' + state.size);
			return false;
		} else if (state.status == "RESUME") {
			nlapiLogExecution('AUDIT', 'Script Resumed');
		}
	}
}