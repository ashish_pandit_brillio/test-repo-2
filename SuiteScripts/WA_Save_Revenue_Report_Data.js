/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       13 Mar 2015     amol.sahijwani
 *
 */

/**
 * @returns {Void} Any or no return value
 */
function workflowAction() {
	try {
		var i_record_id = nlapiGetRecordId();
		var rec = nlapiLoadRecord(nlapiGetRecordType(), i_record_id);

		var s_xml = rec.getFieldValue('custrecord_rev_rep_xml_data_xml_data');
		var s_month = rec.getFieldValue('custrecord_rev_rep_xml_data_month');
		var s_year = rec.getFieldValue('custrecord_rev_rep_xml_data_year');
		var o_xml = nlapiStringToXML(s_xml);

		var o_data = nlapiSelectNode(o_xml, '//data');

		var a_records = nlapiSelectNodes(o_data, 'record');

		var i_number_of_records = a_records.length;

		var context = nlapiGetContext();

		var i_last_updated_index = parseInt(context.getSetting('SCRIPT',
				'custscript_rev_rep_data_last_upd_index'));// rec.getFieldValue('custworkflow_wf_last_updated_index'));

		nlapiLogExecution('AUDIT', 'Last Updated Index', i_last_updated_index);
		var a_data = getData(s_month, s_year);

		var a_hash_data = new Array();
		for (var i = 0; i < a_data.length; i++) {
			a_hash_data[i] = getHashString(a_data[i].employee,
					a_data[i].customer, a_data[i].project,
					a_data[i].parent_practice, a_data[i].practice,
					a_data[i].vertical);// + '|' + + '|' + + '|' + + '|' + ;
		}
		nlapiLogExecution('AUDIT', 'a_hash_data', a_hash_data[0]);
		for (var i = 0; i < i_number_of_records; i++) {
			if (i >= i_last_updated_index) {

				var s_employee = nlapiSelectValue(a_records[i], 'Employee');
				var s_project = nlapiSelectValue(a_records[i], 'Project');
				var s_customer = nlapiSelectValue(a_records[i], 'Customer');
				var s_end_customer	=	nlapiSelectValue(a_records[i], 'EndCustomer');
				var s_vertical = nlapiSelectValue(a_records[i], 'Vertical');
				var s_practice = nlapiSelectValue(a_records[i], 'Department');
				var s_parent_practice = nlapiSelectValue(a_records[i],
						'ParentDepartment')
				var s_total = nlapiSelectValue(a_records[i], 'TotalAmount');
				var s_search_data = getHashString(s_employee, s_customer,
						s_project, s_parent_practice, s_practice, s_vertical);
				nlapiLogExecution('AUDIT', 'a_hash_data' + a_hash_data[0]
						+ ', s_search_data', s_search_data);
				var i_search_index = a_hash_data.indexOf(s_search_data);
				if (i_search_index == -1) {
					nlapiLogExecution('AUDIT', 'Record not found', i_record_id);
					var rec_rev_rep_data = nlapiCreateRecord('customrecord_revenue_report_data');
					rec_rev_rep_data.setFieldValue(
							'custrecord_rev_rep_data_employee', s_employee);
					rec_rev_rep_data.setFieldValue(
							'custrecord_rev_rep_data_customer', s_customer);
					rec_rev_rep_data.setFieldValue(
							'custrecord_rev_rep_data_project', s_project);
					rec_rev_rep_data.setFieldValue(
							'custrecord_rev_rep_data_practice', s_practice);
					rec_rev_rep_data.setFieldValue(
							'custrecord_rev_rep_data_parent_practice',
							s_parent_practice);
					rec_rev_rep_data.setFieldValue(
							'custrecord_rev_rep_data_vertical', s_vertical);
					rec_rev_rep_data.setFieldValue(
							'custrecord_rev_rep_data_total_amount', s_total);
					rec_rev_rep_data.setFieldValue(
							'custrecord_rev_rep_data_month', s_month);
					rec_rev_rep_data.setFieldValue(
							'custrecord_rev_rep_data_year', s_year);
					rec_rev_rep_data.setFieldValue(
							'custrecord_rev_rep_data_updated_by', i_record_id);
					rec_rev_rep_data.setFieldValue('custrecord_rev_rep_data_end_customer', s_end_customer);

					nlapiSubmitRecord(rec_rev_rep_data);
				} else {
					nlapiLogExecution('AUDIT', 'Record Found', i_record_id);
					if (s_total != a_data[i_search_index].tot_amt) {
						nlapiSubmitField('customrecord_revenue_report_data',
								a_data[i_search_index].internal_id, [
										'custrecord_rev_rep_data_total_amount',
										'custrecord_rev_rep_data_updated_by' ],
								[ s_total, i_record_id ]);
						nlapiLogExecution('AUDIT', 'Total Updated: '
								+ a_data[i_search_index].internal_id, s_total);
					} else {
						nlapiSubmitField('customrecord_revenue_report_data',
								a_data[i_search_index].internal_id,
								'custrecord_rev_rep_data_updated_by',
								i_record_id);
					}
				}
				nlapiLogExecution('AUDIT', i + ', Remaining Usage: ', context
						.getRemainingUsage());
				if (context.getRemainingUsage() < 10)
					return i;
			}
		}
	} catch (e) {
		nlapiLogExecution('ERROR', 'Error: ', e.message);
		return -1;
	}
	return -1;
	var a = 1;
}
function getData(s_month, s_year) {
	var filters = new Array();
	filters[0] = new nlobjSearchFilter('custrecord_rev_rep_data_month', null,
			'equalto', s_month);
	filters[1] = new nlobjSearchFilter('custrecord_rev_rep_data_year', null,
			'equalto', s_year);

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('internalid');
	columns[1] = new nlobjSearchColumn('custrecord_rev_rep_data_employee');
	columns[2] = new nlobjSearchColumn('custrecord_rev_rep_data_customer');
	columns[3] = new nlobjSearchColumn('custrecord_rev_rep_data_project');
	columns[4] = new nlobjSearchColumn('custrecord_rev_rep_data_practice');
	columns[5] = new nlobjSearchColumn('custrecord_rev_rep_data_vertical');
	columns[6] = new nlobjSearchColumn('custrecord_rev_rep_data_total_amount');
	columns[7] = new nlobjSearchColumn('custrecord_rev_rep_data_parent_practice');

	var a_search_results = searchRecord('customrecord_revenue_report_data',
			null, filters, columns);
	var a_data = new Array();
	for (var i = 0; i < a_search_results.length; i++) {
		var o_data = new Object();
		o_data.internal_id = a_search_results[i].getValue('internalid');
		o_data.employee = a_search_results[i]
				.getValue('custrecord_rev_rep_data_employee');
		o_data.customer = a_search_results[i]
				.getValue('custrecord_rev_rep_data_customer');
		o_data.project = a_search_results[i]
				.getValue('custrecord_rev_rep_data_project');
		o_data.practice = a_search_results[i]
				.getValue('custrecord_rev_rep_data_practice');
		o_data.parent_practice = a_search_results[i]
				.getValue('custrecord_rev_rep_data_parent_practice');
		o_data.vertical = a_search_results[i]
				.getValue('custrecord_rev_rep_data_vertical');
		o_data.tot_amt = a_search_results[i]
				.getValue('custrecord_rev_rep_data_total_amount');
		o_data.updated_by = a_search_results[i]
				.getValue('custrecord_rev_rep_data_updated_by');
		a_data.push(o_data);
	}

	return a_data;
}
// return hash string
function getHashString(s_employee, s_customer, s_project, s_practice,
		s_vertical) {
	return s_employee + '|' + s_customer + '|' + s_project + '|' + s_practice
			+ '|' + s_vertical;
}
/*
 * function searchRecord(recordType, savedSearch, arrFilters, arrColumns) {
 * 
 * try { var search = null; // if a saved search is provided, load it and add
 * the filters and // columns if (isNotEmpty(savedSearch)) { search =
 * nlapiLoadSearch(recordType, savedSearch); search.addFilters(arrFilters);
 * search.addColumns(arrColumns); } // create a new search else { search =
 * nlapiCreateSearch(recordType, arrFilters, arrColumns); } // run search var
 * resultSet = search.runSearch(); // iterate through the search and get all
 * data 1000 at a time var searchResultCount = 0; var resultSlice = null; var
 * searchResult = [];
 * 
 * do { resultSlice = resultSet.getResults(searchResultCount, searchResultCount +
 * 1000);
 * 
 * if (resultSlice) {
 * 
 * resultSlice.forEach(function(result) {
 * 
 * searchResult.push(result); searchResultCount++; }); } } while
 * (isArrayNotEmpty(resultSlice) && resultSlice.length >= 1000);
 * 
 * return searchResult; } catch (err) { nlapiLogExecution('ERROR',
 * 'searchRecord', err); throw err; } }
 */