/**
 * Show the details of the vendor bill to be generated for a contractor for a
 * period, and on submit, create the vendor bill
 * 
 * Version Date Author Remarks
 * 
 * 1.00 05 Jan 2016 amol.sahijwani
 * 
 * 2.00 09 August 2016 Jayesh Dinde refactoring and fixing for multiple subtier
 * record
 * 
 * Constraints :
 * 
 * Module works for monthly type only for the entire month
 */
var ii=0;
var file_constants = {
    CustomForm : 133, // post GST preferred form for BTPL changed, earlier form id : 123,prior to GST form id was 101
    FinanceHead : 1615,
    ItemCategory : 232,
    Account : 518,
    TaxCode : 2552
};

function suitelet(request, response) {
	try {
		var errorMessage = "";

		// get the inputs from request
		var vendorId = request.getParameter('custpage_vendor');
		var rateType = request.getParameter('custpage_rate_type');
		var employeeId = request.getParameter('custpage_contractor');
		var startDate = request.getParameter('custpage_start_date');
		var endDate = request.getParameter('custpage_end_date');

		// check all fields are present
		if (!(vendorId && rateType && employeeId && startDate && endDate)) {
			throw "Some field missing";
		}

		// get all the time-entries from the employee during this period
		var timeEntriesToBePaidSearch = nlapiSearchRecord('timebill', null, [
		        new nlobjSearchFilter('type', null, 'anyof', 'A'),
		        new nlobjSearchFilter('employee', null, 'anyof', employeeId),
		        new nlobjSearchFilter('date', null, 'within', startDate,
		                endDate) ]);

		var timeEntriesToBePaid = [];

		if (timeEntriesToBePaidSearch) {

			timeEntriesToBePaidSearch.forEach(function(timebill) {
				timeEntriesToBePaid.push(timebill.getId());
			});
		} else {
			throw "Employee has not filled any timesheet.";
		}

		// validate the allocation and timesheets
		validateTimeEntry(employeeId, startDate, endDate,
		        checkForProperAllocation(employeeId, startDate, endDate));

		// check if all the week timesheets are filled
		var notSubmittedTs = getNotSubmittedTimesheet(employeeId, startDate,
		        endDate);
				
		if(notSubmittedTs>0)
			throw "Timesheet not available for this contractor!!";

		// check if any of the time entry during this period is already paid
		var paidTimeEntrySearch = nlapiSearchRecord(
		        'customrecord_ts_extra_attributes', null, [
		                new nlobjSearchFilter('custrecord_time_entry_id_tb', null,
		                        'anyof', timeEntriesToBePaid),
		                new nlobjSearchFilter('isinactive', null, 'is', 'F'),
		                new nlobjSearchFilter('custrecord_vendor_bill', null,
		                        'noneof', '@NONE@') ], [
		                new nlobjSearchColumn('custrecord_vendor_bill'),
		                new nlobjSearchColumn('custrecord_time_entry_id_tb'),
		                new nlobjSearchColumn('durationdecimal',
		                        'custrecord_time_entry_id_tb'),
		                new nlobjSearchColumn('item',
		                        'custrecord_time_entry_id_tb'),
		                new nlobjSearchColumn('customer',
		                        'custrecord_time_entry_id_tb') ]);

		if (paidTimeEntrySearch) {
			throw "Some of the time entries are already paid";
		}

		var d_startDate = nlapiStringToDate(startDate);
		var d_endDate = nlapiStringToDate(endDate);

		// get the total no. of days in the month
		var noOfDaysInMonth = getDayDiff(startDate, endDate);

		// get the pay rate details
		var payRateDetails = getMonthlyPayRateDetails(startDate, endDate,
		        employeeId, vendorId, noOfDaysInMonth, rateType);
			
		if(parseInt(rateType) == 3)
		{
			// get the list of days on which leave was applied
			var leaveList = getAppliedLeaveList(employeeId, startDate, endDate);
	
			// get the day on which the leave is allowed (first leave)
			// - allowed day calculation if payrate is changing between the month
			// needs to be reconsidered
			var allowedLeaveDay = null;
			if (leaveList) {
				allowedLeaveDay = leaveList[0];
			}
		
			nlapiLogExecution('audit','allowed leave:-- ',allowedLeaveDay);
			// get day wise array of the payable days
			var payableDaysArray = getPayableDays(startDate, endDate, employeeId,
			        allowedLeaveDay, noOfDaysInMonth);
		
			var totalPayableAmount = calculateTotalPayableAmount(startDate,
		        endDate, payableDaysArray, payRateDetails);
				
			
		}
		else
		{
			var totalPayableAmount = calculateTotalPayableAmount_Hourly(startDate,
		        endDate, employeeId, payRateDetails);
			nlapiLogExecution('audit','totalPayableAmount:-- ',totalPayableAmount);
			
			var projectWiseBreakup_hourly = calculatePayableAmount_Hourly_projwise(startDate,endDate, employeeId, payRateDetails);
			nlapiLogExecution('audit','projectWiseBreakup_hourly:-- ',projectWiseBreakup_hourly.length);
		}
		
		var projectWiseBreakup = proportionateAmountProjectWise(startDate,
		        endDate, employeeId, allowedLeaveDay, payRateDetails, rateType);
		
		// proportionate the amount project wise
		
		
		
		var postResult = null;
		if (request.getMethod() == "POST") {
			postResult = createVendorBill(startDate, endDate, employeeId,
			        vendorId, projectWiseBreakup, postResult,
			        timeEntriesToBePaid, rateType, projectWiseBreakup_hourly);
		}

		// display the calculations
		createDisplayForm(employeeId, vendorId, startDate, endDate,
		        payRateDetails, projectWiseBreakup, noOfDaysInMonth,
		        totalPayableAmount, postResult, errorMessage, rateType);
	} catch (err) {
		nlapiLogExecution('ERROR', 'suitelet', err);
		displayErrorForm(err, "Create Vendor Bill");
	}
}

function createVendorBill(startDate, endDate, employeeId, vendorId,
        projectWiseBreakup, postResult, timeEntriesToBePaid, rateType, projectWiseBreakup_hourly)
{
	try {
		var vendorBill = nlapiCreateRecord('vendorbill', {
			'entity' : vendorId
		});

		var employeeDetails = nlapiLookupField('employee', employeeId, [
		        'entityid', 'department' ]);
		var employeePractice = employeeDetails.department;
		var contractorName = employeeDetails.entityid;

		vendorBill.setFieldValue('customform', file_constants.CustomForm);
		vendorBill.setFieldValue('custbody_financemanager',
		        file_constants.FinanceHead);
		vendorBill.setFieldValue('custbody_billfrom', startDate);
		vendorBill.setFieldValue('custbody_billto', endDate);
		vendorBill.setFieldValue('memo', 'Professional charges of '
		        + contractorName + ' for the period of ' + formatDate(nlapiStringToDate(startDate)) + ' to '
		        + formatDate(nlapiStringToDate(endDate)));
		vendorBill.setFieldValue('custbody_is_subtier_bill','T');
		vendorBill.setFieldValue('location',18);
		
		if(parseInt(rateType) == 3)
		{
			for ( var project in projectWiseBreakup)
			{
				var projectDetails = nlapiLookupField('job', project, [ 'entityid',
				        'altname', 'customer' ]);
				var customerDetails = nlapiLookupField('customer',
				        projectDetails.customer, [ 'territory',
				                'custentity_vertical' ]);
				var projectName = projectDetails.entityid + ' '
				        + projectDetails.altname;
				var proj_rcrd = nlapiLoadRecord('job',project);
				var proj_alt_name = proj_rcrd.getFieldValue('altname');
				var proj_entity_id = proj_rcrd.getFieldValue('entityid');
				projectName = proj_entity_id + ' ' + proj_alt_name;
				var vertical = customerDetails['custentity_vertical'];
				var customer = nlapiLookupField('job', project, 'customer', true);
				var territory = customerDetails.territory;
	
				for ( var monthlyRate in projectWiseBreakup[project])
				{
					// todo : memo date format is different
					// todo : memo date is not month date but project date
					var lineMemo = 'Professional charges of ' + contractorName
					        + ' for the period of ' + formatDate(nlapiStringToDate(startDate)) + ' to ' + formatDate(nlapiStringToDate(endDate))
					        + " for monthly rate " + monthlyRate;
	
					vendorBill.selectNewLineItem('expense');
					vendorBill.setCurrentLineItemValue('expense', 'category',
					        file_constants.ItemCategory);
					vendorBill.setCurrentLineItemValue('expense', 'account',
					        file_constants.Account);
					vendorBill.setCurrentLineItemValue('expense', 'amount',
					        projectWiseBreakup[project][monthlyRate]);
							
					//POST GST tax code will be auto populated based on transactino location and vendor state
					//vendorBill.setCurrentLineItemValue('expense', 'taxcode',file_constants.TaxCode);
					vendorBill.setCurrentLineItemValue('expense', 'department',
					        employeePractice);
					vendorBill
					        .setCurrentLineItemValue('expense', 'class', vertical);
					vendorBill.setCurrentLineItemValue('expense',
					        'custcol_territory', territory);
					vendorBill.setCurrentLineItemValue('expense',
					        'custcolcustcol_temp_customer', customer);
					vendorBill.setCurrentLineItemValue('expense',
					        'custcolprj_name', projectName);
					vendorBill.setCurrentLineItemValue('expense',
					        'custcol_employeenamecolumn', contractorName);
					vendorBill.setCurrentLineItemValue('expense', 'memo', lineMemo);
					vendorBill.commitLineItem('expense');
	
					// Swatch Bharat Cess
					/*vendorBill.selectNewLineItem('expense');
					vendorBill.setCurrentLineItemValue('expense', 'category',
					        file_constants.ItemCategory);
					vendorBill.setCurrentLineItemValue('expense', 'account',
					        file_constants.Account);
					vendorBill
					        .setCurrentLineItemValue(
					                'expense',
					                'amount',
					                calculateSwatchBharatCess(projectWiseBreakup[project][monthlyRate]));
					// vendorBill.setCurrentLineItemValue('expense', 'taxcode',
					// file_constants.TaxCode);
					vendorBill.setCurrentLineItemValue('expense', 'department',
					        employeePractice);
					vendorBill
					        .setCurrentLineItemValue('expense', 'class', vertical);
					vendorBill.setCurrentLineItemValue('expense',
					        'custcol_territory', territory);
					vendorBill.setCurrentLineItemValue('expense',
					        'custcolcustcol_temp_customer', customer);
					vendorBill.setCurrentLineItemValue('expense',
					        'custcolprj_name', projectName);
					vendorBill.setCurrentLineItemValue('expense',
					        'custcol_employeenamecolumn', contractorName);
					vendorBill.setCurrentLineItemValue('expense', 'memo', 'SBC-'
					        + lineMemo);
					vendorBill.commitLineItem('expense');*/
				}
			}
			
			// Check if time entries are already tagged
			var timeEntryCount = timeEntriesToBePaid.length;
			nlapiLogExecution('debug', 'timeEntryCount', timeEntryCount)
	
			// search for all the time entry in the TS Extra Attribute
			var search = nlapiSearchRecord('customrecord_ts_extra_attributes',
			        null, [ new nlobjSearchFilter('custrecord_time_entry_id_tb', null,
			                'anyof', timeEntriesToBePaid) ], [
			                new nlobjSearchColumn('custrecord_vendor_bill'),
			                new nlobjSearchColumn('custrecord_time_entry_id_tb'), ]);
	
			var allowCreation = true;
			var timeEntryProcess = [];
	
			timeEntriesToBePaid.forEach(function(timeEntryId) {
				var entryFound = false;

			// if no search data found, push for creation
			if (search) {

				// find if the time entry exists in the search result
				for (var i = 0; i < search.length; i++) {

					// if found, push for update
					if (timeEntryId == search[i]
					        .getValue('custrecord_time_entry_id_tb')) {
						entryFound = true;

						if (search[i].getValue('custrecord_vendor_bill')) {
							timeEntryProcess.push({
							    Type : 'Update',
							    timebill : timeEntryId,
							    RecId : search[i].getId()
							});
						} else {
							allowCreation = false;
							break;
						}
					}
				}
			}

			// not found, push for create
			if (!entryFound) {
				timeEntryProcess.push({
				    Type : 'Create',
				    timebill : timeEntryId,
				    RecId : ''
				});
			}
		});

		if (allowCreation) {
			var vendorBillId = nlapiSubmitRecord(vendorBill,true,true);

			nlapiLogExecution('DEBUG', 'Vendor Bill Created', vendorBillId);
			nlapiLogExecution('DEBUG', 'timeEntryProcess',
			        timeEntryProcess.length);

			timeEntryProcess.forEach(function(timebill) {

				        if (timebill.Type == 'Create') {
							nlapiLogExecution('DEBUG', 'ii',
			        ii++);
					        var newMappingRecord = nlapiCreateRecord('customrecord_ts_extra_attributes');
					        newMappingRecord.setFieldValue(
					                'custrecord_time_entry_id_tb',
					                timebill.timebill);
					        newMappingRecord.setFieldValue(
					                'custrecord_vendor_bill', vendorBillId);
									
					        nlapiSubmitRecord(newMappingRecord);
							
							
				        } else if (timebill.Type == 'Update') {
					        nlapiSubmitField(
					                'customrecord_ts_extra_attributes',
					                timebill.RecId, 'custrecord_vendor_bill',
					                vendorBillId);
				        }
			        });
nlapiLogExecution('DEBUG', 'ii',ii);
				nlapiLogExecution('AUDIT', 'Mapping Completed');
				return {
				    Status : true,
				    Data : vendorBillId
				};
			} else {
				return {
				    Status : false,
				    Data : "Some Time Entry Already Paid"
				};
			}
			
		}
		else
		{
			var lineMemo = 'Professional charges of ' + contractorName
			+ ' for the period of ' + formatDate(nlapiStringToDate(startDate)) + ' to ' + formatDate(nlapiStringToDate(endDate));
			
			//nlapiLogExecution('audit','inside else',projectWiseBreakup_hourly.length);
			for(var jk=0; jk<projectWiseBreakup_hourly.length; jk++)
			{
				var projectDetails = nlapiLookupField('job', projectWiseBreakup_hourly[jk].project, [ 'entityid',
				        'altname', 'customer' ]);
				var customerDetails = nlapiLookupField('customer',
				        projectDetails.customer, [ 'territory',
				                'custentity_vertical' ]);
				var projectName = projectDetails.entityid + ' '
				        + projectDetails.altname;
				var proj_rcrd = nlapiLoadRecord('job',projectWiseBreakup_hourly[jk].project);
				var proj_alt_name = proj_rcrd.getFieldValue('altname');
				var proj_entity_id = proj_rcrd.getFieldValue('entityid');
				projectName = proj_entity_id + ' ' + proj_alt_name;
				var vertical = customerDetails['custentity_vertical'];
				var customer = nlapiLookupField('job', projectWiseBreakup_hourly[jk].project, 'customer', true);
				var territory = customerDetails.territory;
				
				vendorBill.selectNewLineItem('expense');
				vendorBill.setCurrentLineItemValue('expense', 'category',
				        file_constants.ItemCategory);
				vendorBill.setCurrentLineItemValue('expense', 'account',
				        file_constants.Account);
				vendorBill.setCurrentLineItemValue('expense', 'amount',
				        projectWiseBreakup_hourly[jk].amount);
						
				//POST GST tax code will be auto populated based on transaction location and vendor state
				//vendorBill.setCurrentLineItemValue('expense', 'taxcode',file_constants.TaxCode);
				vendorBill.setCurrentLineItemValue('expense', 'department',
				        employeePractice);
				vendorBill
				        .setCurrentLineItemValue('expense', 'class', vertical);
				vendorBill.setCurrentLineItemValue('expense',
				        'custcol_territory', territory);
				vendorBill.setCurrentLineItemValue('expense',
				        'custcolcustcol_temp_customer', customer);
				vendorBill.setCurrentLineItemValue('expense',
				        'custcolprj_name', projectName);
				vendorBill.setCurrentLineItemValue('expense',
				        'custcol_employeenamecolumn', contractorName);
				vendorBill.setCurrentLineItemValue('expense', 'memo', lineMemo);
				vendorBill.commitLineItem('expense');
				//nlapiLogExecution('audit','line inserted',projectWiseBreakup_hourly[jk].amount);
			}
			
			var vendorBillId = nlapiSubmitRecord(vendorBill,true,true);
			nlapiLogExecution('audit','vendor bill id:-- ',vendorBillId);
			return {
				    Status : true,
				    Data : vendorBillId
				};
		}
		
	} catch (err) {
		nlapiLogExecution('ERROR', 'createVendorBill', err);
		throw err;
	}
}

function calculateSwatchBharatCess(amount) {
	return Math.round(amount * 0.005);
}

function createDisplayForm(employeeId, vendorId, startDate, endDate,
        payRateDetails, projectWiseBreakup, noOfDaysInMonth,
        totalPayableAmount, postResult, errorMessage, rateType)
{
	try {
		// create the display form
		var form = nlapiCreateForm('Create Vendor Bill');
		// form.setScript();
		form.addButton('custpage_go_back', 'Back', 'goBack()');

		if (request.getMethod() == "GET" && !errorMessage) {
			form.addSubmitButton('Create Vendor Bill');
		}

		form.addFieldGroup('custpage_1', 'Info');
		form.addField('custpage_contractor', 'select', 'Contractor',
		        'employee', 'custpage_1').setDisplayType('inline')
		        .setBreakType('startcol').setDefaultValue(employeeId);
		form.addField('custpage_vendor', 'select', 'Vendor', 'vendor',
		        'custpage_1').setDisplayType('inline').setBreakType('startcol')
		        .setDefaultValue(vendorId);
		form.addField('custpage_start_date', 'date', 'Start Date', null,
		        'custpage_1').setDisplayType('inline').setBreakType('startcol')
		        .setDefaultValue(startDate);
		form.addField('custpage_end_date', 'date', 'End Date', null,
		        'custpage_1').setDisplayType('inline').setBreakType('startcol')
		        .setDefaultValue(endDate);
		form.addField('custpage_rate_type', 'select', 'Rate Type',
		        'customlist_btpl_subtier_rate_type', 'custpage_1')
		        .setDisplayType('inline').setBreakType('startcol')
		        .setDefaultValue(rateType);

		if (postResult && postResult.Status) {
			form.addField('custpage_vendor_bill', 'select',
			        'New Vendor Bill Generated', 'vendorbill', 'custpage_1')
			        .setDisplayType('inline').setBreakType('startcol')
			        .setDefaultValue(postResult.Data);
		} else if (postResult && postResult.Data) {
			form.addField('custpage_vendor_bill', 'text', 'Some Error Occured',
			        null, 'custpage_1').setDisplayType('inline').setBreakType(
			        'startcol').setDefaultValue(postResult.Data);
		} else if (errorMessage) {
			form.addField('custpage_vendor_bill', 'text', 'Some Error Occured',
			        null, 'custpage_1').setDisplayType('inline').setBreakType(
			        'startcol').setDefaultValue(errorMessage);
		}

		// vendor bill table
		var paymentTableHtml = "<br/><div style='border:2px solid #0066ff;width: 600px !important;'>";
		for ( var project in projectWiseBreakup) {
			var projectDetails = nlapiLookupField('job', project, [ 'entityid',
			        'altname' ]);
			var projectName = projectDetails.entityid + " : "
			        + projectDetails.altname;
			paymentTableHtml += "<b><u>" + projectName + "</u></b><br/>";

			for ( var monthlyRate in projectWiseBreakup[project]) {
				if(parseInt(rateType) == 3)
				{
					paymentTableHtml += "Rate : " + monthlyRate;
					paymentTableHtml += "  Amount : "
					        + projectWiseBreakup[project][monthlyRate];
					paymentTableHtml += "<br/>";
				}
				else
				{
					paymentTableHtml += "Rate : " + monthlyRate;
					paymentTableHtml += "<br/>";
				}
			}
		}
		paymentTableHtml += "<b style='border-top:1px solid black;'>Total Vendor Bill Amount : "
		        + totalPayableAmount + "</b><br/>";
		paymentTableHtml += "</div><br/>";

		var monthTimesheetTable = createMonthTimesheetTable(employeeId,
		        startDate, endDate);

		// get the allocation during this period
		var allocationDetails = getAllocationDetails(startDate, endDate,
		        employeeId);

		var tableHtml = "";
		var projectObject = [];

		if (allocationDetails) {
			var dayList = [ 'SUN', 'MON', 'TUE', 'WED', 'THU', 'FRI', 'SAT' ];

			allocationDetails
			        .forEach(function(allocation) {
				        var projectId = allocation.getValue('company', null,
				                'group');
				        var projectText = allocation.getText('company', null,
				                'group');
				        var hoursSum = 0, leave = 0, holiday = 0, st = 0, ot = 0;

				        // get the time-entry for this employee in this period
				        // for this project
				        var enteredTimeEntry = getEnteredTimesheet(startDate,
				                endDate, employeeId, projectId);

				        // if (enteredTimeEntry) {
				        // nlapiLogExecution('debug', 'time entry found for '
				        // + projectText, enteredTimeEntry.length);
				        // }

				        var weekArray = {};

				        if (enteredTimeEntry) {

					        for (var i = 0; i < enteredTimeEntry.length; i++) {
						        var s_date = enteredTimeEntry[i]
						                .getValue('date');
						        var d_date = nlapiStringToDate(s_date);
						        var duration = enteredTimeEntry[i]
						                .getValue('durationdecimal');
						        hoursSum += parseInt(duration);
						        var timesheet = enteredTimeEntry[i]
						                .getValue('timesheet');
						        var itemText = enteredTimeEntry[i]
						                .getValue('item');
						        var dayOfWeek = d_date.getDay();
								
								if(timesheet)
								{
									switch (itemText) {
							        case "Leave":
								        leave += parseInt(duration);
							        break;
							        case "Holiday":
								        holiday += parseInt(duration);
							        break;
							        case "OT":
								        ot += parseInt(duration);
							        break;
							        default:
								        itemText = "ST";
								        st += parseInt(duration);
									}
								}

						        

						        var rate = getPayrateForDay(payRateDetails,
						                d_date, 3, noOfDaysInMonth);

						        if (!weekArray[timesheet]) {
							        weekArray[timesheet] = {
							            days : [ [], [], [], [], [], [], [] ],
							            startdate : enteredTimeEntry[i]
							                    .getValue('startdate',
							                            'timesheet'),
							            enddate : enteredTimeEntry[i].getValue(
							                    'enddate', 'timesheet')
							        };
						        }

						        weekArray[timesheet]['days'][dayOfWeek].push({
						            Duration : duration,
						            Rate : rate,
						            Item : itemText,
						            Type : ''
						        });
					        }
				        }

				        var timeEntryTable = "<br/>";
				        timeEntryTable += "<table class='timesheet-table'>";
				        timeEntryTable += "<tr style='background:#0066ff;color:white;font-weight:bold;'>";
				        timeEntryTable += "<td colspan=8>";
				        timeEntryTable += "<b>Project Name : " + projectText
				                + "</b>";
				        timeEntryTable += "</td>";
				        timeEntryTable += "</tr>";
				        timeEntryTable += "<tr style='background:#0066ff;color:white;font-weight:bold;'>";
				        timeEntryTable += "<td>";
				        timeEntryTable += "Week";
				        timeEntryTable += "</td>";
				        dayList.forEach(function(day) {
					        timeEntryTable += "<td>";
					        timeEntryTable += day;
					        timeEntryTable += "</td>";
				        });
				        timeEntryTable += "</tr>";

				        for ( var week in weekArray) {
							
							if(weekArray[week].startdate)
							{
								timeEntryTable += "<tr>";
								timeEntryTable += "<td style='background:#668cff;color:white;'>";
								timeEntryTable += weekArray[week].startdate + " - "
										+ weekArray[week].enddate;
								timeEntryTable += "</td>";
								weekArray[week]['days'].forEach(function(day) {
									timeEntryTable += "<td>";
									if (day.length > 0) {

										day.forEach(function(entry) {
											timeEntryTable += entry.Item;
											timeEntryTable += " ";
											timeEntryTable += entry.Duration;
											timeEntryTable += " ";
											timeEntryTable += entry.Rate;
											timeEntryTable += "<br/>";
										});
									} else {
										timeEntryTable += " ";
									}
									timeEntryTable += "</td>";
								});
								timeEntryTable += "</tr>";
							}
				        }
				        timeEntryTable += "</table>";

				        tableHtml += timeEntryTable;

				        tableHtml += "ST : " + st;
				        tableHtml += "<br/>";
				        tableHtml += "OT : " + ot;
				        tableHtml += "<br/>";
				        tableHtml += "Leave : " + leave;
				        tableHtml += "<br/>";
				        tableHtml += "Holiday : " + holiday;
				        tableHtml += "<br/>";
				        tableHtml += "<hr/>";

				        projectObject.push({
				            ProjectId : projectId,
				            Timesheet : weekArray,
				            Hours : hoursSum,
				        });
			        });
		}

		// nlapiLogExecution('debug', 'projectObject', JSON
		// .stringify(projectObject));

		var css = "<style>"
		        + ".timesheet-table {width:600px !important; border : 1px solid black;background: #f2f2f2;}"
		        + ".timesheet-table td { border : 1px solid black; }"
		        + "</style>";
		var projectWiseHtml = css + tableHtml;

		form.addFieldGroup('custpage_2', 'Vendor Bill Details');
		form.addField('custpage_fld_2', 'inlinehtml', '', null, 'custpage_2')
		        .setDefaultValue(paymentTableHtml);

		form.addFieldGroup('custpage_3', 'Month Timesheet View');
		form.addField('custpage_fld_3', 'inlinehtml', '', null, 'custpage_3')
		        .setDefaultValue(monthTimesheetTable);

		form.addFieldGroup('custpage_4', 'Project Wise Timesheet View');
		form.addField('custpage_fld_4', 'inlinehtml', '', null, 'custpage_4')
		        .setDefaultValue(projectWiseHtml);

		response.writePage(form);
	} catch (err) {
		nlapiLogExecution('ERROR', 'createDisplayForm', err);
		throw err;
	}
}

function getAllocationDetails(startDate, endDate, employee) {
	try {
		var allocationSearch = nlapiSearchRecord('resourceallocation', null,
		        [
		                new nlobjSearchFilter('resource', null, 'anyof',
		                        employee),
		                new nlobjSearchFilter('startdate', null, 'notafter',
		                        endDate),
		                new nlobjSearchFilter('enddate', null, 'notbefore',
		                        startDate) ], [ new nlobjSearchColumn(
		                'company', null, 'group').setSort() ]);

		return allocationSearch;
	} catch (err) {
		nlapiLogExecution('ERROR', 'getAllocationDetails', err);
		throw err;
	}
}

function proportionateAmountProjectWise(startDate, endDate, employeeId,
        allowedLeaveDay, payRateDetails, rateType)
{
	try {
		// var projectWiseBreakup = {};
		
		if(parseInt(rateType) == 3)
		{
			payRateDetails
		        .forEach(function(subtier) {
			        subtier.TotalDuration = 0;
			        subtier.ProjectWiseBreakUp = {};

			        var filters = [
			                [ 'type', 'anyof', 'A' ],
			                'and',
			                [ 'employee', 'anyof', employeeId ],
			                'and',
			                [ 'date', 'within', subtier.StartDate,
			                        subtier.EndDate ], 'and',
			                [ 'item', 'noneof', '2425' ], 'and' ];

			        if (allowedLeaveDay) {
				        filters.push([ [ 'item', 'noneof', '2479' ], 'or',
				                [ 'date', 'on', allowedLeaveDay ] ]);
			        } else {
				        filters.push([ 'item', 'noneof', '2479' ]);
			        }

			        // get the project wise timesheet details within this
			        // subtier
			        var timeEntrySearch = nlapiSearchRecord('timebill', null,
			                filters, [
			                        new nlobjSearchColumn('customer', null,
			                                'group').setSort(),
			                        new nlobjSearchColumn('durationdecimal',
			                                null, 'sum') ]);

			        if (timeEntrySearch) {

				        timeEntrySearch
				                .forEach(function(timebill) {
					                var duration = parseFloat(timebill
					                        .getValue('durationdecimal', null,
					                                'sum'));
					                var project = timebill.getValue(
					                        'customer', null, 'group');

					                if (!subtier.ProjectWiseBreakUp[project]) {
						                subtier.ProjectWiseBreakUp[project] = {
							                Hour : 0
						                };
					                }

					                subtier.ProjectWiseBreakUp[project].Hour += duration;
					                subtier.TotalDuration += duration;
				                });
			        }
		        });

				// calculate subtier project wise breakup
				payRateDetails.forEach(function(subtier) {
		
					for ( var project in subtier.ProjectWiseBreakUp) {
						subtier.ProjectWiseBreakUp[project].Amount = Math
						        .round((subtier.PayableAmount / subtier.TotalDuration)
						                * subtier.ProjectWiseBreakUp[project].Hour);
					}
				});

				var projectWiseBreakup = {};
				payRateDetails
				        .forEach(function(subtier) {
		
					        for ( var project in subtier.ProjectWiseBreakUp) {
		
						        if (!projectWiseBreakup[project]) {
							        projectWiseBreakup[project] = {};
						        }
		
						        if (!projectWiseBreakup[project][subtier.MonthlyRate]) {
							        projectWiseBreakup[project][subtier.MonthlyRate] = 0;
						        }
		
						        projectWiseBreakup[project][subtier.MonthlyRate] += subtier.ProjectWiseBreakUp[project].Amount;
					        }
				        });
		}
		else
		{
			payRateDetails
		        .forEach(function(subtier) {
			        subtier.TotalDuration = 0;
			        subtier.ProjectWiseBreakUp = {};

			        var filters = [
			                [ 'type', 'anyof', 'A' ],
			                'and',
			                [ 'employee', 'anyof', employeeId ],
			                'and',
			                [ 'date', 'within', subtier.StartDate,
			                        subtier.EndDate ], 'and',
			                [ 'item', 'noneof', '2479' ] ];

			        
			        // get the project wise timesheet details within this
			        // subtier
			        var timeEntrySearch = nlapiSearchRecord('timebill', null,
			                filters, [
			                        new nlobjSearchColumn('customer', null,
			                                'group').setSort(),
			                        new nlobjSearchColumn('durationdecimal',
			                                null, 'sum') ]);

			        if (timeEntrySearch) {

				        timeEntrySearch
				                .forEach(function(timebill) {
					                var duration = parseFloat(timebill
					                        .getValue('durationdecimal', null,
					                                'sum'));
					                var project = timebill.getValue(
					                        'customer', null, 'group');

					                if (!subtier.ProjectWiseBreakUp[project]) {
						                subtier.ProjectWiseBreakUp[project] = {
							                Hour : 0
						                };
					                }

					                subtier.ProjectWiseBreakUp[project].Hour += duration;
					                subtier.TotalDuration += duration;
				                });
			        }
		        });

				// calculate subtier project wise breakup
				payRateDetails.forEach(function(subtier) {
		
					for ( var project in subtier.ProjectWiseBreakUp) {
						nlapiLogExecution('audit','project id:-- '+subtier.ProjectWiseBreakUp[project],subtier.MonthlyRate);
						subtier.ProjectWiseBreakUp[project].Amount = Math
						        .round(subtier.MonthlyRate);
					}
				});
				
				
				var projectWiseBreakup = {};
				payRateDetails
				        .forEach(function(subtier) {
		
					        for ( var project in subtier.ProjectWiseBreakUp) {
		
						        if (!projectWiseBreakup[project]) {
							        projectWiseBreakup[project] = {};
						        }
		
						        if (!projectWiseBreakup[project][subtier.MonthlyRate]) {
							        projectWiseBreakup[project][subtier.MonthlyRate] = 0;
						        }
		
						        projectWiseBreakup[project][subtier.MonthlyRate] += subtier.ProjectWiseBreakUp[project].Amount;
					        }
				        });
		}
		
		//nlapiLogExecution('audit','proj lem:-- '+projectWiseBreakup.length,payRateDetails.length);
		return projectWiseBreakup;
	} catch (err) {
		nlapiLogExecution('ERROR', 'proportionateAmountProjectWise', err);
		throw err;
	}
}

function getPayableDays(startDate, endDate, employee, allowedLeaveDay,
        noOfDaysInMonth)
{
	try {
		var filters = [ [ 'type', 'anyof', 'A' ], 'and',
		        [ 'employee', 'anyof', employee ], 'and',
		        [ 'date', 'within', startDate, endDate ], 'and',
		        [ 'item', 'noneof', '2425' ], 'and' ];

		if (allowedLeaveDay) {
			filters.push([ [ 'item', 'noneof', '2479' ], 'or',
			        [ 'date', 'on', allowedLeaveDay ] ]);
		} else {
			filters.push([ 'item', 'noneof', '2479' ]);
		}

		var timeEntrySearch = nlapiSearchRecord('timebill', null, filters,
		        [ new nlobjSearchColumn('date', null, 'group').setSort() ]);

		var monthArray = [];
		var allow_frst_nt_submitted_entry = 0;
		var d_startDate = nlapiStringToDate(startDate);
		var d_endDate = nlapiStringToDate(endDate);

		// loop from start date to end date
		for (var i = 0; i < noOfDaysInMonth; i++) {
			var currentDate = nlapiAddDays(d_startDate, i);
			var s_currentDate = nlapiDateToString(currentDate, 'date');

			// check if weekend
			if (currentDate.getDay() == 0 || currentDate.getDay() == 6) {
				monthArray.push({
				    S_Date : s_currentDate,
				    D_Date : currentDate,
				    Payable : true
				});
				continue;
			}

			// check if the date exists in the timesheet search
			if (timeEntrySearch) {
				var datePushed = false;

				for (var j = 0; j < timeEntrySearch.length; j++) {
					var searchDate = timeEntrySearch[j].getValue('date', null,
					        'group');

					if (searchDate == s_currentDate) {
						datePushed = true;
						monthArray.push({
						    S_Date : s_currentDate,
						    D_Date : currentDate,
						    Payable : true
						});
						break;
					}
				}

				if (!datePushed) {
					if(allowedLeaveDay)
					{
						monthArray.push({
					    S_Date : s_currentDate,
					    D_Date : currentDate,
					    Payable : false
						});
					}
					else
					{
						var nt_submittd_day = nlapiStringToDate(s_currentDate);
						var filters = new Array();
						filters[0] = new nlobjSearchFilter('employee', null, 'anyof', employee);
						filters[1] = new nlobjSearchFilter('date', 'timebill', 'on', nt_submittd_day);
						var timeEntrySearch_chk_ntenteredday = nlapiSearchRecord('timesheet', null, filters,null);
						if(_logValidation(timeEntrySearch_chk_ntenteredday))
						{
							
						}
						else
						{
							if(allow_frst_nt_submitted_entry == 0)
							{
								monthArray.push({
							    S_Date : s_currentDate,
							    D_Date : currentDate,
							    Payable : true
								});
								allow_frst_nt_submitted_entry++;
							}
							else
							{
								monthArray.push({
							    S_Date : s_currentDate,
							    D_Date : currentDate,
							    Payable : false
								});
							}
						}
					}
				}
			} else {
				monthArray.push({
				    S_Date : s_currentDate,
				    D_Date : currentDate,
				    Payable : false
				});
			}
		}

		return monthArray;
	} catch (err) {
		nlapiLogExecution('ERROR', 'getPayableDays', err);
		throw err;
	}
}

function getDayDiff(fromDate, toDate) {
	fromDate = nlapiStringToDate(fromDate);
	toDate = nlapiStringToDate(toDate);

	if (fromDate > toDate) {
		return -999;
	}

	var dayDiff = 1;
	for (;; dayDiff++) {
		var newDate = nlapiAddDays(fromDate, dayDiff);

		if (newDate > toDate) {
			break;
		}
	}

	return dayDiff;
}

function checkForProperAllocation(employeeId, periodStartDate, periodEndDate) {
	try {
		var d_periodStartDate = nlapiStringToDate(periodStartDate);
		var d_periodEndDate = nlapiStringToDate(periodEndDate);
		nlapiLogExecution('audit','Period To process:- '+d_periodStartDate,d_periodEndDate);
		
		// get all the allocation during this period
		/*var allocationSearch = nlapiSearchRecord('resourceallocation', null, [
		        new nlobjSearchFilter('resource', null, 'anyof', employeeId),
		        new nlobjSearchFilter('formuladate', null, 'notafter',
		                periodEndDate).setFormula('{startdate}'),
		        new nlobjSearchFilter('formuladate', null, 'notbefore',
		                periodStartDate).setFormula('{enddate}') ], [
		        new nlobjSearchColumn('startdate'),
		        new nlobjSearchColumn('enddate'),
		        new nlobjSearchColumn('company'),
		        new nlobjSearchColumn('percentoftime') ]);*/
		
		var filters_allocation = [['startdate', 'onorbefore', periodEndDate], 'and',
								['enddate', 'onorafter', periodStartDate], 'and',
								['resource','anyof', employeeId]];
			
		var allocationSearch = nlapiSearchRecord('resourceallocation', null, filters_allocation, [
		        new nlobjSearchColumn('startdate'),
		        new nlobjSearchColumn('enddate'),
		        new nlobjSearchColumn('company'),
		        new nlobjSearchColumn('percentoftime') ]);
				
		// create a month array
		var monthDays = [];
		var totalDays = getDayDiff(periodStartDate, periodEndDate);
		for (var i = 0; i < totalDays; i++) {
			monthDays.push({
			    State : 0,
			    Sum : 0,
			    Project : []
			});
		}

		// mark days between hire and lwd
		var employeeDetails = nlapiLookupField('employee', employeeId, [
		        'hiredate', 'custentity_lwd' ]);
		var hireDate = employeeDetails.hiredate;
		var terminationDate = employeeDetails.custentity_lwd;
		var d_hireDate = nlapiStringToDate(hireDate);
		terminationDate = terminationDate ? terminationDate : periodEndDate;
		var d_terminationDate = nlapiStringToDate(terminationDate);
		var d_loopStartDate = d_hireDate > d_periodStartDate ? d_hireDate
		        : d_periodStartDate;
		var d_loopEndDate = d_terminationDate < d_periodEndDate ? d_terminationDate
		        : d_periodEndDate;
		var loopStartDate = nlapiDateToString(d_loopStartDate, 'date');
		var loopEndDate = nlapiDateToString(d_loopEndDate, 'date');
		var loopStartIndex = getDayDiff(periodStartDate, loopStartDate) - 1;
		var loopEndIndex = getDayDiff(periodStartDate, loopEndDate) - 1;
		
		nlapiLogExecution('audit','loopStartIndex:- '+loopStartIndex,loopEndIndex);
		for (var i = loopStartIndex; i <= loopEndIndex; i++) {
			monthDays[i].State = 1;
		}
	//	nlapiLogExecution('audit','allocationSearch.length:- '+allocationSearch.length);
		// mark the allocated days
		// loop and get this employees allocation
		if (allocationSearch) {

			for (var j = 0; j < allocationSearch.length; j++) {
				var allocationStartDate = allocationSearch[j]
				        .getValue('startdate');
				var d_allocationStartDate = nlapiStringToDate(allocationStartDate);
				var allocationEndDate = allocationSearch[j].getValue('enddate');
				var d_allocationEndDate = nlapiStringToDate(allocationEndDate);
				var allocatedProject = allocationSearch[j].getValue('company');
				var allocatedPercent = parseFloat(allocationSearch[j].getValue(
				        'percentoftime').split("%")[0]);
				var d_loopStartDate = d_allocationStartDate > d_periodStartDate ? d_allocationStartDate
				        : d_periodStartDate;
				var d_loopEndDate = d_allocationEndDate < d_periodEndDate ? d_allocationEndDate
				        : d_periodEndDate;
				var loopStartDate = nlapiDateToString(d_loopStartDate, 'date');
				var loopEndDate = nlapiDateToString(d_loopEndDate, 'date');
				var loopStartIndex = getDayDiff(periodStartDate, loopStartDate) - 1;
				var loopEndIndex = getDayDiff(periodStartDate, loopEndDate) - 1;
				
				nlapiLogExecution('audit','loopStartIndex:- '+loopStartIndex,loopEndIndex);
				
				for (var k = loopStartIndex; k <= loopEndIndex; k++) {
					monthDays[k].State = 2;
					monthDays[k].Sum += allocatedPercent;
					monthDays[k].Project.push(allocatedProject);
				}
			}
		}
		
		nlapiLogExecution('debug', 'month Day', JSON.stringify(monthDays));
		// check for over / under allocation
		for (var j = 0; j < monthDays.length; j++) {

			if ((monthDays[j].State == 2 && monthDays[j].Sum != 100)
			        || monthDays[j].State == 1) {
				throw "Employee not allocated properly";
			}
		}

		return monthDays;
	} catch (err) {
		nlapiLogExecution('ERROR', 'checkForProperAllocation', err);
		throw err;
	}
}

function validateTimeEntry(employeeId, periodStartDate, periodEndDate,
        allocationMaster)
{
	try {
		// get all the time entries between the given time period
		var timeEntrySearch = nlapiSearchRecord('timebill', null, [
		        new nlobjSearchFilter('employee', null, 'anyof', employeeId),
		        new nlobjSearchFilter('type', null, 'anyof', 'A'),
		        new nlobjSearchFilter('date', null, 'within', periodStartDate,
		                periodEndDate) ], [ new nlobjSearchColumn('employee'),
		        new nlobjSearchColumn('date'),
		        new nlobjSearchColumn('customer') ]);

		if (timeEntrySearch) {

			for (var i = 0; i < timeEntrySearch.length; i++) {
				var employeeId = timeEntrySearch[i].getValue('employee');
				var timeEntryDate = timeEntrySearch[i].getValue('date');
				var projectId = timeEntrySearch[i].getValue('customer');
				var dayDiff = getDayDiff(periodStartDate, timeEntryDate) - 1;
				//nlapiLogExecution('audit','allocationMaster[dayDiff].Project:-- '+allocationMaster[dayDiff].Project,projectId);
				if (_.indexOf(allocationMaster[dayDiff].Project, projectId) == -1) {
					throw "Timesheets are not filled properly on "
					        + timeEntryDate;
					break;
				}
			}
		}
	} catch (err) {
		nlapiLogExecution('ERROR', 'validateTimeEntry', err);
		throw err;
	}
}

function getMonthlyPayRateDetails(startDate, endDate, employee, vendor,
        noOfDaysInMonth, rateType)
{
	//rate type = 3
	try {
		var subtierSearch = nlapiSearchRecord(
		        'customrecord_subtier_vendor_data', null, [
		                new nlobjSearchFilter('custrecord_stvd_start_date',
		                        null, 'onorbefore', endDate),
		                new nlobjSearchFilter('custrecord_stvd_end_date', null,
		                        'onorafter', startDate),
		                new nlobjSearchFilter('custrecord_stvd_rate_type',
		                        null, 'anyof', parseInt(rateType)),
		                new nlobjSearchFilter('custrecord_stvd_vendor', null,
		                        'anyof', vendor),
		                new nlobjSearchFilter('custrecord_stvd_contractor',
		                        null, 'anyof', employee),
						new nlobjSearchFilter('isinactive', null, 'is', 'F')], [
		                new nlobjSearchColumn('custrecord_stvd_start_date'),
		                new nlobjSearchColumn('custrecord_stvd_end_date'),
		                new nlobjSearchColumn('custrecord_stvd_st_pay_rate'),
		                new nlobjSearchColumn('custrecord_stvd_ot_pay_rate') ]);

		var d_startDate = nlapiStringToDate(startDate);
		var d_endDate = nlapiStringToDate(endDate);
		var paymentDetails = [];

		if (subtierSearch) {
		
		if(parseInt(rateType) == 3)
		{
			subtierSearch
			        .forEach(function(subtier) {
				        var stRate = parseFloat(subtier
				                .getValue('custrecord_stvd_st_pay_rate'));
				        var dailyRate = stRate / noOfDaysInMonth;
				        // nlapiLogExecution('debug', 'stRate', stRate);

				        var subTierStartDate = nlapiStringToDate(subtier
				                .getValue('custrecord_stvd_start_date'));
				        var subTierEndDate = nlapiStringToDate(subtier
				                .getValue('custrecord_stvd_end_date'));
				        // nlapiLogExecution('debug', 'subTierStartDate',
				        // subTierStartDate);
				        // nlapiLogExecution('debug', 'subTierEndDate',
				        // subTierEndDate);

				        var newStart = subTierStartDate > d_startDate ? subTierStartDate
				                : d_startDate;
				        var newEnd = subTierEndDate < d_endDate ? subTierEndDate
				                : d_endDate;
				        // nlapiLogExecution('debug', 'newEnd', newEnd);
				        // nlapiLogExecution('debug', 'newStart', newStart);

				        paymentDetails.push({
				            StartDate : newStart,
				            EndDate : newEnd,
				            MonthlyRate : stRate,
				            DailyRate : dailyRate,
				            OT : subtier
				                    .getValue('custrecord_stvd_ot_pay_rate')
				        });
			        });
		}
		else
		{
			subtierSearch
			        .forEach(function(subtier) {
				        var stRate = parseFloat(subtier
				                .getValue('custrecord_stvd_st_pay_rate'));
				        //var dailyRate = stRate / noOfDaysInMonth;
				        // nlapiLogExecution('debug', 'stRate', stRate);

				        var subTierStartDate = nlapiStringToDate(subtier
				                .getValue('custrecord_stvd_start_date'));
				        var subTierEndDate = nlapiStringToDate(subtier
				                .getValue('custrecord_stvd_end_date'));
				        // nlapiLogExecution('debug', 'subTierStartDate',
				        // subTierStartDate);
				        // nlapiLogExecution('debug', 'subTierEndDate',
				        // subTierEndDate);

				        var newStart = subTierStartDate > d_startDate ? subTierStartDate
				                : d_startDate;
				        var newEnd = subTierEndDate < d_endDate ? subTierEndDate
				                : d_endDate;
				        // nlapiLogExecution('debug', 'newEnd', newEnd);
				        // nlapiLogExecution('debug', 'newStart', newStart);

				        paymentDetails.push({
				            StartDate : newStart,
				            EndDate : newEnd,
				            MonthlyRate : stRate,
				            DailyRate : 0,
				            OT : subtier
				                    .getValue('custrecord_stvd_ot_pay_rate')
				        });
			        });
		}
			
		}
		return paymentDetails;
	} catch (err) {
		nlapiLogExecution('ERROR', 'getPayRateDetails', err);
		throw err;
	}
}

function getAppliedLeaveList(employeeId, startDate, endDate) {
	//nlapiLogExecution('audit','inside get leaves list');
	var leaveSearch = nlapiSearchRecord('timebill', 'customsearch1470_2', [
	        new nlobjSearchFilter('employee', null, 'anyof', employeeId),
	        new nlobjSearchFilter('date', null, 'within', startDate, endDate) ]);
//nlapiLogExecution('audit','inside get leaves list after search');
	var leaveList = [];

	if (leaveSearch) {

		leaveSearch.forEach(function(leave) {
			//leaveList.push(leave.getValue('date', null, 'group'));
			leaveList.push(leave.getValue('date'));
		});
	}

	return leaveList;
}

function calculateTotalPayableAmount(startDate, endDate, payableDaysArray,
        payRateDetails)
{
	var totalPayableAmount = 0;

	payRateDetails.forEach(function(subtier) {
		var subtierPayableAmount = 0;

		payableDaysArray.forEach(function(day) {

			if (day.Payable && subtier.StartDate <= day.D_Date
			        && day.D_Date <= subtier.EndDate) {
				subtierPayableAmount += subtier.DailyRate;
			}
			//nlapiLogExecution('audit','day.Payable :- '+day.Payable,day.D_Date);
			//nlapiLogExecution('audit','subtier.DailyRate :- ',subtier.DailyRate);
		});

		totalPayableAmount += subtierPayableAmount;
		subtier.PayableAmount = subtierPayableAmount;
		
	});

	return Math.round(totalPayableAmount);
}

function calculateTotalPayableAmount_Hourly(startDate,endDate, employeeId, payRateDetails)
{
	var filters	=	new Array();	
	filters[0]	=	new nlobjSearchFilter('employee', null, 'is', parseInt(employeeId));
	filters[1] = new nlobjSearchFilter('date', null, 'within', startDate, endDate);
	var columns	=	new Array();
	columns[0]	=	new nlobjSearchColumn('durationdecimal');
	columns[1] =    new nlobjSearchColumn('date');
	
	
	var timeEntrySearch_hourly = nlapiSearchRecord('timebill','customsearch1481_2',filters,columns);
	var total_duration_hourly = 0;
	var subtierPayableAmount = 0;
	for(var time_srch=0; time_srch<timeEntrySearch_hourly.length; time_srch++)
	{
		total_duration_hourly = timeEntrySearch_hourly[time_srch].getValue('durationdecimal');
		var date = timeEntrySearch_hourly[time_srch].getValue('date');
		date = nlapiStringToDate(date);
		payRateDetails.forEach(function(subtier)
		{
			//nlapiLogExecution('audit','subtier.StartDate:- '+subtier.StartDate,subtier.EndDate);
			//nlapiLogExecution('audit','date:- '+date);
			if (subtier.StartDate <= date && date <= subtier.EndDate)
			{
				//nlapiLogExecution('audit','subtier.MonthlyRate:- '+subtier.MonthlyRate,total_duration_hourly);
				subtierPayableAmount += parseFloat(subtier.MonthlyRate) * parseFloat(total_duration_hourly);
			}
		});
	}
		
	//return Math.round(totalPayableAmount);
	return subtierPayableAmount;
}

function calculatePayableAmount_Hourly_projwise(startDate,endDate, employeeId, payRateDetails)
{
	var filters	=	new Array();	
	filters[0]	=	new nlobjSearchFilter('employee', null, 'is', parseInt(employeeId));
	filters[1] = new nlobjSearchFilter('date', null, 'within', startDate, endDate);
	var columns	=	new Array();
	columns[0]	=	new nlobjSearchColumn('durationdecimal');
	columns[1] =    new nlobjSearchColumn('date');
	columns[2] =    new nlobjSearchColumn('customer');
	
	
	var timeEntrySearch_hourly = nlapiSearchRecord('timebill','customsearch1481_2',filters,columns);
	var total_duration_hourly = 0;
	var subtierPayableAmount = 0;
	var projectWiseBreakup_list = new Array();
	var projectWiseBreakup_arr = new Array();
	var sr_no = 0;
	for(var time_srch=0; time_srch<timeEntrySearch_hourly.length; time_srch++)
	{
		total_duration_hourly = timeEntrySearch_hourly[time_srch].getValue('durationdecimal');
		var date = timeEntrySearch_hourly[time_srch].getValue('date');
		date = nlapiStringToDate(date);
		payRateDetails.forEach(function(subtier)
		{
			//nlapiLogExecution('audit','subtier.StartDate:- '+subtier.StartDate,subtier.EndDate);
			//nlapiLogExecution('audit','date:- '+date);
			if (subtier.StartDate <= date && date <= subtier.EndDate)
			{
				//nlapiLogExecution('audit','subtier.MonthlyRate:- '+subtier.MonthlyRate,total_duration_hourly);
				subtierPayableAmount = parseFloat(subtier.MonthlyRate) * parseFloat(total_duration_hourly);
				if (projectWiseBreakup_list.indexOf(timeEntrySearch_hourly[time_srch].getValue('customer')) >= 0)
				{
					var cuurnt_posi = projectWiseBreakup_list.indexOf(timeEntrySearch_hourly[time_srch].getValue('customer'));
					var amnt = parseFloat(projectWiseBreakup_arr[cuurnt_posi].amount) + parseFloat(subtierPayableAmount);
					projectWiseBreakup_arr[cuurnt_posi] = {
								'project':timeEntrySearch_hourly[time_srch].getValue('customer'),
								'amount':amnt
					};
				}
				else
				{
					projectWiseBreakup_arr[sr_no] = {
								'project':timeEntrySearch_hourly[time_srch].getValue('customer'),
								'amount':subtierPayableAmount
					};
					projectWiseBreakup_list.push(timeEntrySearch_hourly[time_srch].getValue('customer'));
					sr_no ++;
				}
				
			}
		});
	}
		
	//return Math.round(totalPayableAmount);
	return projectWiseBreakup_arr;
}

function createMonthTimesheetTable(employeeId, startDate, endDate) {
	try {
		// create a blank month table
		var table = {};
		var d_startDate = nlapiStringToDate(startDate);
		var d_endDate = nlapiStringToDate(endDate);
		var end_week = d_endDate.getWeekNumber();
	//var end_year = d_endDate.getFullYear();
		//var end_week = getWeek(end_year,d_endDate.getMonth(),d_endDate.getDay());

		for (var i = 0;; i++) {
			var currentDate = nlapiAddDays(d_startDate, i);
			var s_currentDate = nlapiDateToString(currentDate, 'date');
			var weekOfYear = currentDate.getWeekNumber();
			
			nlapiLogExecution('audit','week entry:- '+weekOfYear+' ::strt date:-'+d_startDate,currentDate);
			
			if (weekOfYear > end_week || currentDate > d_endDate) {
				break;
			}
			
			if (!table[weekOfYear]) {
				table[weekOfYear] = {
				    Week : [ '', '', '', '', '', '', '' ],
				    Name : currentDate.getWeekName()
				};
			}
		}

		// get all timesheets within the period
		var timeEntrySearch = nlapiSearchRecord('timebill', null, [
		        new nlobjSearchFilter('employee', null, 'anyof', employeeId),
		        new nlobjSearchFilter('type', null, 'anyof', 'A'),
		        new nlobjSearchFilter('date', null, 'within', startDate,
		                endDate) ], [ new nlobjSearchColumn('date', null,
		        'group') ]);

		if (timeEntrySearch) {

			timeEntrySearch.forEach(function(timebill) {
				var timeEntryDate = nlapiStringToDate(timebill.getValue(
				        'date', null, 'group'));
				var s_currentDate = nlapiDateToString(currentDate, 'date');
				var d_currentDate = new Date(s_currentDate);
				var weekOfYear = timeEntryDate.getWeekNumber();
				var dayOfWeek = timeEntryDate.getDay();
				
				nlapiLogExecution('audit','week of year:- '+weekOfYear,timeEntryDate);
				table[weekOfYear].Week[dayOfWeek] = 'Yes';
			});
		}

		var css = "<style>"
		        + ".month-timesheet-table { width: 600px !important; border : 1px solid black; background: #f2f2f2;} "
		        + ".month-timesheet-table td { border : 1px solid black; }"
		        + "</style>";

		var html = css;
		html += "<br/><table class='month-timesheet-table'>";
		var dayList = [ 'SUN', 'MON', 'TUE', 'WED', 'THU', 'FRI', 'SAT' ];
		html += "<tr style='background:#0066ff;color:white;font-weight:bold;'>";
		html += "<td>Week</td>";
		dayList.forEach(function(day) {
			html += "<td>" + day + "</td>";
		});
		html += "</tr>";
		for ( var week in table) {
			html += "<tr>";
			html += "<td style='background:#668cff;color:white;'>"
			        + table[week].Name + "</td>";
			table[week].Week.forEach(function(day) {
				html += "<td>" + day + "</td>";
			});
			html += "</tr>";
		}
		html += "</table><br/>";
		return html;
	} catch (err) {
		nlapiLogExecution('ERROR', 'createMonthTimesheetTable', err);
		throw err;
	}
}

function getPayrateForDay(payRateDetails, d_date, rateType, totalDays) {
	var rate = 0;

	for (var i = 0; i < payRateDetails.length; i++) {

		if (payRateDetails[i].StartDate <= d_date
		        && d_date <= payRateDetails[i].EndDate) {
			rate = payRateDetails[i].MonthlyRate;
			break;
		}
	}

	// nlapiLogExecution('debug', 'rate type', rateType);
	// nlapiLogExecution('debug', 'rate', rate);

	return rate;
}

function getNotSubmittedTimesheet(employee, d_start_date, d_end_date) {
	try {
		// Assuming employee is allocated 100% ( allocation is already checked
		// in a previous function )
		// Reference :
		// 1. Not Submitted
		// 3. Submitted

		var d_start_date = nlapiStringToDate(d_start_date);
		var d_end_date = nlapiStringToDate(d_end_date);

		// 1. create a array containing all days from start to end
		var main_array = [];

		// 2. loop from start to end date and mark saturday sunday as submitted
		for (var i = 0;; i++) {
			var d_current_date = nlapiAddDays(d_start_date, i);

			// if the date exceeds the end date, exit the loop
			if (d_current_date > d_end_date) {
				break;
			}

			// check if saturday / sunday
			var day = d_current_date.getDay();
			if (day == 0 || day == 6) {
				main_array[i] = 3;
			} else {
				main_array[i] = 1;
			}
		}

		nlapiLogExecution('debug', 'main_array', JSON.stringify(main_array));

		var employeeDetails = nlapiLookupField('employee', employee, [
		        'hiredate', 'custentity_lwd' ]);

		// 3a. Mark days before hire date as submitted
		var d_hire_date = nlapiStringToDate(employeeDetails.hiredate);

		if (d_hire_date >= d_start_date) {

			for (var i = 0;; i++) {
				var d_current_date = nlapiAddDays(d_start_date, i);

				// if the date exceeds the hire date, exit the loop
				if (d_current_date > d_hire_date) {
					break;
				}
				main_array[i] = 3;
			}
			nlapiLogExecution('debug', 'd_hire_date >= d_start_date', JSON.stringify(main_array));
		}

		nlapiLogExecution('debug', 'main_array', JSON.stringify(main_array));

		// 3b. Mark days after termination date as submitted
		var d_termination_date = null;

		if (employeeDetails.custentity_lwd) {
			d_termination_date = nlapiStringToDate(employeeDetails.custentity_lwd);

			if (d_termination_date <= d_end_date) {

				for (var i = main_array.length - 1;; i--) {
					var d_current_date = nlapiAddDays(d_start_date, i);

					// if the date exceeds the hire date, exit the loop
					if (d_current_date < d_termination_date) {
						break;
					}
					main_array[i] = 3;
				}
			}
		}

		nlapiLogExecution('debug', 'main_array', JSON.stringify(main_array));

		// 4. get the timesheet between the start date and end date
		var search_timesheet = nlapiSearchRecord('timesheet', null, [
		        new nlobjSearchFilter('employee', null, 'anyof', employee),
		        new nlobjSearchFilter('approvalstatus', null, 'noneof',
		                [ 1, 4 ]),
		        //new nlobjSearchFilter('timesheetdate', null, 'within',d_start_date, d_end_date) ], [ new nlobjSearchColumn('startdate')]);
		        new nlobjSearchFilter('date', 'timebill', 'within',d_start_date, d_end_date)],[ new nlobjSearchColumn('startdate')]);

		// 5. loop through the timesheets
		if (search_timesheet) {
			
			// 5a. if timesheet is submitted, mark entire week as submitted
			for (var i = 0; i < search_timesheet.length; i++) {
				var timesheet_date = search_timesheet[i].getValue('startdate');
				var n = getDatediffIndays(nlapiDateToString(d_start_date),
				        timesheet_date) - 1;
				
				if (n >= 0) {


					for (var j = n; j < n + 7; j++) {

						if (main_array[j] == 1) {
							main_array[j] = 3;
						}
					}
				} else {

nlapiLogExecution('debug', 'n ELSE', n);
					for (var j = 0; j < n + 7; j++) {

						if (main_array[j] == 1) {
							main_array[j] = 3;
						}
					}
				}
			}
		}

		nlapiLogExecution('debug', 'main_array', JSON.stringify(main_array));

		// 7. count the not submitted days in the main array
		var not_submitted_days = 0;
		var submitted = 0;

		for (var i = 0; i < main_array.length; i++) {

			if (main_array[i] == 1) {
				not_submitted_days = not_submitted_days + 1;
			} else {
				submitted = submitted + 1;
			}
		}

		nlapiLogExecution('debug', 'not submitted', not_submitted_days);
		return not_submitted_days;
	} catch (err) {

	}
}

function _logValidation(value){
	if (value != 'null' && value != null && value != null && value != '' && value != undefined && value != undefined && value != 'undefined' && value != 'undefined' && value != 'NaN' && value != NaN) {
		return true;
	}
	else {
		return false;
	}
}

function getDatediffIndays(startDate, endDate) {
	var one_day = 1000 * 60 * 60 * 24;
	var fromDate = startDate;
	var toDate = endDate;
	var date1 = nlapiStringToDate(fromDate);
	var date2 = nlapiStringToDate(toDate);
	var date3 = Math.round((date2 - date1) / one_day);
	return (date3 + 1);
}

function getEnteredTimesheet(startDate, endDate, employee, project) {
	try {
		var timeEntrySearch = nlapiSearchRecord('timebill', 'customsearch_1472_2', [
		        new nlobjSearchFilter('customer', null, 'anyof', project),
		        new nlobjSearchFilter('employee', null, 'anyof', employee),
		        new nlobjSearchFilter('date', null, 'within', startDate,
		                endDate) ]);

		return timeEntrySearch;
	} catch (err) {
		nlapiLogExecution('ERROR', 'getEnteredTimesheet', err);
		throw err;
	}
}

function formatDate(d_date)
{
	var a_months	=	['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
	
	return	d_date.getDate() + '-' + a_months[d_date.getMonth()] + '-' + d_date.getFullYear();
}

Date.prototype.getWeekName = function() {
	var d = new Date(+this);
	var days = d.getDay();
	var startDate = nlapiAddDays(d, -days);
	var endDate = nlapiAddDays(startDate, 6);
	return nlapiDateToString(startDate, 'date') + " - "
	        + nlapiDateToString(endDate, 'date');
};

Date.prototype.getWeekNumber = function() {
	var d = new Date(+this);
	var dd = new Date(+this);
	var end_year = d.getFullYear();
	d.setHours(0, 0, 0);
	d.setDate(d.getDate() + 4 - (d.getDay() || 7));
	var new_end_year = d.getFullYear();
	if(end_year != new_end_year)
	{
		return Math
	        .ceil((((dd - new Date(dd.getFullYear(), 0, 1)) / 8.64e7) + 1) / 7);
	}
	else
	{
	return Math
	        .ceil((((d - new Date(d.getFullYear(), 0, 1)) / 8.64e7) + 1) / 7);
	}
};
/*function getWeek(year,month,day){
    //lets calc weeknumber the cruel and hard way :D
    //Find JulianDay 
    month += 1; //use 1-12
    var a = Math.floor((14-(month))/12);
    var y = year+4800-a;
    var m = (month)+(12*a)-3;
    var jd = day + Math.floor(((153*m)+2)/5) + 
                 (365*y) + Math.floor(y/4) - Math.floor(y/100) + 
                 Math.floor(y/400) - 32045;      // (gregorian calendar)
    //var jd = (day+1)+Math.Round(((153*m)+2)/5)+(365+y) + 
    //                 Math.round(y/4)-32083;    // (julian calendar)
    
    //now calc weeknumber according to JD
    var d4 = (jd+31741-(jd%7))%146097%36524%1461;
    var L = Math.floor(d4/1460);
    var d1 = ((d4-L)%365)+L;
    NumberOfWeek = Math.floor(d1/7) + 1;
    return NumberOfWeek;        
}*/