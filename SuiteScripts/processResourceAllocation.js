/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       06 Mar 2019     Aazamali Khan    Update Allocation Name and Allocation Date
 * 1.01       08 Aug 2019 	  Aazamali Khan	   Add filter condition check for Taleo Employee Selected
 *
 */
function processFRFDetails(type) {
    try {
        // Get the resource allocation
        var searchResult = getResourceAllocations();
        // Get the Project of resource allocation 
        if (searchResult) {
            for (var i = 0; i < searchResult.length; i++) {
                CheckMetering();
				var resourceId = searchResult[i].getValue("resource");
                var projectId = searchResult[i].getValue("company");
                var startDate = searchResult[i].getValue("startdate");
                // find frf with same project and resource id 
                processFRFAllocations(resourceId, projectId, startDate);
            }
        }
    } catch (e) {
		nlapiLogExecution("DEBUG","error",e);
          var arr_cc = [];	
		  arr_cc.push("fuel.support@brillio.com");
		  arr_cc.push("deepak.srinivas@brillio.com");
		  arr_cc.push("information.systems@brillio.com");
		  var body = e;
		  var i_recipient = "aazamali@inspirria.com";
		  var subject = "FUEL Notification : Error in allocation script : 1888"
		  nlapiSendEmail(154256,i_recipient, subject, body,arr_cc);
    }
}

function getResourceAllocations() {
    var resourceallocationSearch = searchRecord("resourceallocation", null,
        [
            ["systemnotes.type", "is", "T"],
            "AND",
            ["systemnotes.date","after","thirtydaysago"] //thirtydaysago -- tendaysago
        ],
        [
            new nlobjSearchColumn("resource"),
            new nlobjSearchColumn("startdate"),
            new nlobjSearchColumn("company")
        ]
    );
    return resourceallocationSearch;
}

function processFRFAllocations(resourceId, projectId, startDate) {
    var customrecord_frf_detailsSearch = nlapiSearchRecord("customrecord_frf_details", null,
        [
            [["custrecord_frf_details_opp_id.custrecord_project_internal_id_sfdc", "anyof", projectId],"OR",["custrecord_frf_details_project", "anyof", projectId]],
            "AND",
            [["custrecord_frf_details_selected_emp", "anyof", resourceId],"OR",["custrecord_frf_details_taleo_emp_select","anyof",resourceId]],"AND",["custrecord_frf_details_open_close_status","noneof","2"] 
        ],
        [

        ]
    );
	if(customrecord_frf_detailsSearch){
		nlapiLogExecution("AUDIT","customrecord_frf_detailsSearch","Inside FRF search");
		for(var i = 0 ; i < customrecord_frf_detailsSearch.length ; i++){
			var recObj = nlapiLoadRecord("customrecord_frf_details",customrecord_frf_detailsSearch[i].getId());
			var externalHire = recObj.getFieldValue("custrecord_frf_details_external_hire");
			var allocationName = recObj.getFieldValue("custrecord_frf_details_allocated_emp");
            nlapiLogExecution("AUDIT","resourceId",resourceId);
			if(isEmpty(allocationName)){
				//nlapiLogExecution("AUDIT","resourceId 2",resourceId);
				recObj.setFieldValue("custrecord_frf_details_allocated_emp",resourceId);
				recObj.setFieldValue("custrecord_frf_details_allocated_date",startDate);
				recObj.setFieldValue("custrecord_frf_details_open_close_status","2");
				recObj.setFieldValue("custrecord_frf_details_status_flag","2");
	//----------------------------------------------------------------------------------------------------------
	// capturing close date  prabhat gupta NIS-1307			
				var currentDate = sysDate();
				currentDate = nlapiStringToDate(currentDate);
				recObj.setFieldValue("custrecord_frf_details_closed_date", currentDate);
	//------------------------------------------------------------------------------------------------------			
				nlapiSubmitRecord(recObj,{disabletriggers:true,enablesourcing:true},false);
			}
			if(externalHire == "T"){
			  	var taleoRecId = recObj.getFieldValue("custrecord_frf_details_rrf_number");
				var selectedEmployeeEmail = "";
				if(isNotEmpty(taleoRecId)){
					selectedEmployeeEmail = nlapiLookupField("customrecord_taleo_external_hire",taleoRecId,"custrecord_taleo_external_selected_can");	
				}
				// write a search 
				if(isNotEmpty(selectedEmployeeEmail)){
					var searchResult = getEmployeeRec(selectedEmployeeEmail);
					if(searchResult){
						var empId = searchResult[0].getId();
						// Get the resource allocation 
						var allocationSearch = getAllocationSearchForExternal(empId,projectId); 
						if(allocationSearch){
							var resourceId = allocationSearch[0].getValue("resource");
							var startDate = allocationSearch[0].getValue("startdate");
							recObj.setFieldValue("custrecord_frf_details_allocated_emp",resourceId);
							recObj.setFieldValue("custrecord_frf_details_allocated_date",startDate);
							recObj.setFieldValue("custrecord_frf_details_open_close_status","2");
							recObj.setFieldValue("custrecord_frf_details_status_flag","2");
							
							var currentDate = sysDate();
							currentDate = nlapiStringToDate(currentDate);
							recObj.setFieldValue("custrecord_frf_details_closed_date", currentDate);
							
							nlapiSubmitRecord(recObj,{disabletriggers:true,enablesourcing:true});
						}
					}
				}
			}
		}
	}else{
		nlapiLogExecution("AUDIT","customrecord_frf_detailsSearch else : ","Inside FRF search");
		    var customrecord_frf_detailsSearch = nlapiSearchRecord("customrecord_frf_details", null,
            [
                ["custrecord_frf_details_selected_emp", "anyof", resourceId]
            ],
            [
                new nlobjSearchColumn("custrecord_opportunity_id_sfdc", "CUSTRECORD_FRF_DETAILS_OPP_ID", null)
            ]);
        if (customrecord_frf_detailsSearch) {
            for (var i = 0; i < customrecord_frf_detailsSearch.length; i++) {
                var oppId = customrecord_frf_detailsSearch[i].getValue("custrecord_opportunity_id_sfdc", "CUSTRECORD_FRF_DETAILS_OPP_ID", null);
                var frfRecId = customrecord_frf_detailsSearch[i].getId();
                if (oppId) {
                    var salesorderSearch = nlapiSearchRecord("salesorder", null,
                        [
                            ["type", "anyof", "SalesOrd"],
                            "AND",
                            ["mainline", "is", "T"],
                            "AND",
                            ["custbody_opp_id_sfdc", "is", oppId.trim()],
							 "AND", 
							["status","noneof","SalesOrd:H"]		// Filter Added by Ashish on 25-10-2019 
                        ],
                        [
                            new nlobjSearchColumn("internalid", "jobMain", null)
                        ]
                    );
                    if (salesorderSearch) {
                        var projectIdOnSO = salesorderSearch[0].getValue("internalid", "jobMain", null);
                        if (projectId == projectIdOnSO) {
                            var recObj = nlapiLoadRecord("customrecord_frf_details", frfRecId);
                            var externalHire = recObj.getFieldValue("custrecord_frf_details_external_hire");
                            var allocationName = recObj.getFieldValue("custrecord_frf_details_allocated_emp");
                            if (isEmpty(allocationName)) {
                                nlapiLogExecution("AUDIT", "resourceId", resourceId);
                                recObj.setFieldValue("custrecord_frf_details_allocated_emp", resourceId);
                                recObj.setFieldValue("custrecord_frf_details_allocated_date", startDate);
                                recObj.setFieldValue("custrecord_frf_details_open_close_status", "2");
                                recObj.setFieldValue("custrecord_frf_details_status_flag", "2");
		//--------------------------------------------------------------------------------------------------
		// capturing close date  prabhat gupta NIS-1307						
								var currentDate = sysDate();
								currentDate = nlapiStringToDate(currentDate);
								recObj.setFieldValue("custrecord_frf_details_closed_date", currentDate);
		//-------------------------------------------------------------------------------------------------------------						
                                nlapiSubmitRecord(recObj,{disabletriggers:true,enablesourcing:true});
                            }
                        }

                    }
                }
            }
        }

    }
	}
function CheckMetering() {
    var CTX = nlapiGetContext();
    var START_TIME = new Date().getTime();
    //	want to try to only check metering every 15 seconds
    var remainingUsage = CTX.getRemainingUsage();
    var currentTime = new Date().getTime();
    var timeDifference = currentTime - START_TIME;
    //	changing to 15 minutes, should cause little if any impact, but willmake runaway scripts faster to kill
    if (remainingUsage < 800 || timeDifference > 900000) {
        START_TIME = new Date().getTime();
        var status = nlapiYieldScript();
        nlapiLogExecution('AUDIT', 'STATUS = ', JSON.stringify(status));
    }
}
function getEmployeeRec(personalEmail){
	
	var employeeSearch = nlapiSearchRecord("employee",null,
[
   ["custentity_personalemailid","is",personalEmail]
], 
[
]
);
return employeeSearch;
}
function getAllocationSearchForExternal(empId,projectId){
	var resourceallocationSearch = nlapiSearchRecord("resourceallocation",null,
[
   ["startdate","notafter","today"], 
   "AND", 
   ["enddate","notbefore","today"], 
   "AND", 
   ["resource","anyof",empId],"AND",
    ["project","anyof",projectId]   
], 
[
   new nlobjSearchColumn("resource"), 
   new nlobjSearchColumn("startdate"), 
   new nlobjSearchColumn("company")
]
);
return resourceallocationSearch;
}

//-------------------------------------------------------------------------------------------------------------------------------------
// capturing close date  prabhat gupta NIS-1307
function sysDate() {
	var date = new Date();
	var tdate = date.getDate();
	var month = date.getMonth() + 1; // jan = 0
	var year = date.getFullYear();
	return currentDate = month + '/' + tdate + '/' + year;
}