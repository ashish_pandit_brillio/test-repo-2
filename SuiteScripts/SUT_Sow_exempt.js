/*
Author: Deepak MS 5/9/2017 
Description: Validate Project Status
*/

function validateProject(request,response)
{
	try
	{
		var flag = true;
		var i_customer = request.getParameter('i_customer_interal_id');
		var customer_lookup = nlapiLookupField('customer',parseInt(i_customer),['custentity_sow_validation_exception']);
		var project_status = customer_lookup.custentity_sow_validation_exception;
		nlapiLogExecution('ERROR','project_status',project_status);
		/*if(parseInt(project_status) != 2 && parseInt(project_status) != 4 ){
			flag = false;

		}
		else{
			flag = true;
		}
		nlapiLogExecution('ERROR','flag',flag);*/
		response.write(project_status);
	}
	catch(err)
	{
		nlapiLogExecution('ERROR','validatePractice','ERROR MESSAGE :- '+err);
	}
	}