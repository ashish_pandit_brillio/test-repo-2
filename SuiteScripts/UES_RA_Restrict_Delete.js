/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       24 Nov 2016     deepak.srinivas
 *
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType
 * 
 * @param {String} type Operation types: create, edit, delete, xedit
 *                      approve, reject, cancel (SO, ER, Time Bill, PO & RMA only)
 *                      pack, ship (IF)
 *                      markcomplete (Call, Task)
 *                      reassign (Case)
 *                      editforecast (Opp, Estimate)
 * @returns {Void}
 */
function userEventBeforeSubmit(type){
	try{
		if(type == 'delete' ){
          //Added by praveena to fetch values when script is trying to delete the resource allocation
          var onj_load_rec=nlapiLoadRecord('resourceallocation',nlapiGetRecordId());
          	var startDate = onj_load_rec.getFieldValue('startdate');
			var endDate = onj_load_rec.getFieldValue('enddate');
			var employee = onj_load_rec.getFieldValue('allocationresource');
			
			/*var startDate = nlapiGetFieldValue('startdate');
			var endDate = nlapiGetFieldValue('enddate');
			var employee = nlapiGetFieldValue('allocationresource');
			*/var employee_Lookup = nlapiLookupField('employee',employee,['hiredate']);
			var emp_hire_date = employee_Lookup.hiredate;
			emp_hire_date = nlapiStringToDate(emp_hire_date);
			var resAlloc_stDate = nlapiStringToDate(startDate);

			if (employee && startDate && endDate) {

				if (checkIfTimesheetExists(employee, startDate, endDate)) {
					throw "Employee has already filled timesheets for this period for this allocation";
					 
				}
			}
			
		}
		if(type == 'edit' ){
			var startDate = nlapiGetFieldValue('startdate');
			var endDate = nlapiGetFieldValue('enddate');
			var employee = nlapiGetFieldValue('allocationresource');
			var project = nlapiGetFieldValue('project');
			var employee_Lookup = nlapiLookupField('employee',employee,['hiredate']);
			var emp_hire_date = employee_Lookup.hiredate;
			emp_hire_date = nlapiStringToDate(emp_hire_date);
			var resAlloc_stDate = nlapiStringToDate(startDate);

			if (employee && startDate && endDate) {

				if (checkIfTimesheetExists_stop_resource_edit(employee, startDate, endDate,project)) {
					throw "Employee has already filled timesheets for this period for this allocation";
					 
				}
			}
			
		}
	}
	catch(e){
		nlapiLogExecution('DEBUG','Error in RA Delete Restrict',e);
		throw e;
	}
 
}
function checkIfTimesheetExists_stop_resource_edit(employee, startDate, endDate,project) {
	try {
		//var timeEntrySearch = nlapiSearchRecord('timeentry', null, [//commnented by praveena on 11-03-2020 due to migration
		var timeEntrySearch = nlapiSearchRecord('timebill', null, [//search record changed after migration. logic added by praveena on 11-03-2020  
		        new nlobjSearchFilter('employee', null, 'anyof', employee),
		        new nlobjSearchFilter('type', null, 'anyof', 'A'),
		        new nlobjSearchFilter('date', null, 'after', endDate), 
				//new nlobjSearchFilter('internalid','customerproject','anyof',project)]);
                  //new nlobjSearchFilter('internalid','customer','anyof',project)]);
                  new nlobjSearchFilter('internalid','job','anyof',project)]);
		return timeEntrySearch != null;
	} catch (err) {
		nlapiLogExecution('ERROR', 'checkIfTimesheetExists', err);
		throw err;
	}
}
function checkIfTimesheetExists(employee, startDate, endDate) {
	try {
		//var timeEntrySearch = nlapiSearchRecord('timeentry', null, [//commnented by praveena on 11-03-2020 due to migration
		var timeEntrySearch = nlapiSearchRecord('timebill', null, [//search record changed after migration. logic added by praveena on 11-03-2020  
		        new nlobjSearchFilter('employee', null, 'anyof', employee),
		        new nlobjSearchFilter('type', null, 'anyof', 'A'),
		        new nlobjSearchFilter('date', null, 'within', startDate,
						endDate ) ]);

		return timeEntrySearch != null;
	} catch (err) {
		nlapiLogExecution('ERROR', 'checkIfTimesheetExists', err);
		throw err;
	}
}