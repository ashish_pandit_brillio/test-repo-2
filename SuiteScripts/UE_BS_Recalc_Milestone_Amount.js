/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       13 May 2015     nitish.mishra
 *
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your
 * script deployment.
 * 
 * @appliedtorecord recordType
 * 
 * @param {String}
 *            type Operation types: create, edit, delete, xedit approve, reject,
 *            cancel (SO, ER, Time Bill, PO & RMA only) pack, ship (IF)
 *            markcomplete (Call, Task) reassign (Case) editforecast (Opp,
 *            Estimate)
 * @returns {Void}
 */
function userEventBeforeSubmit(type) {
	try {
		// Note : all the amount should be in the same currency

		// check if sublist changed
		// if (true || nlapiIsLineItemChanged('milestone')) {
		var lineItemCount = nlapiGetLineItemCount('milestone');

		// check if a zero %age line has been added
		var isZeroAdded = false;
		for (var linenum = 1; linenum <= lineItemCount; linenum++) {
			var currentMileStonePercent = nlapiGetLineItemValue('milestone',
					'milestoneamount', linenum);

			// remove the %age sign
			var currentMileStonePercentNumeric = currentMileStonePercent
					.split('%')[0].trim();

			if (currentMileStonePercentNumeric == 0) {
				isZeroAdded = true;
				break;
			}

			nlapiLogExecution('debug', 'currentMileStonePercentNumeric',
					currentMileStonePercentNumeric);

			// check if any invalid amount was entered in comments
			var currentLineAmountText = nlapiGetLineItemValue('milestone',
					'comments', linenum);

			if (!currentLineAmountText) {
				throw "Amount entered in comments is not valid";
			}
		}

		// if zero entry has been made in the milestone, then adjust the
		// percentage as per the new amounts
		if (isZeroAdded) {
			// get the project value
			var projectId = nlapiGetFieldValue('project');
			var projectValue = nlapiLookupField('job', projectId,
					'custentity_projectvalue');
			var sum = 0;

			// loop through line item and calculate percentage
			for (var linenum = 1; linenum < lineItemCount; linenum++) {
				// read the amount from comments field
				var currentLineAmountText = nlapiGetLineItemValue('milestone',
						'comments', linenum);

				// extract the amount (number) part from the currency text
				var currentLineAmountNumeric = extractNumberFromComment(currentLineAmountText);
				nlapiLogExecution('debug', 'currentLineAmountNumeric',
						currentLineAmountNumeric);

				// calculate the %age milestone using the project value
				var percentage = (currentLineAmountNumeric / projectValue) * 100;

				// set the new milestone %age
				nlapiSetLineItemValue('milestone', 'milestoneamount', linenum,
						percentage);

				sum += parseFloat(percentage.toFixed(4));
			}

			// to calculate the last line %age, subtract the sum from 100
			var lastLinePercentage = 100 - sum;
			nlapiSetLineItemValue('milestone', 'milestoneamount',
					lineItemCount, lastLinePercentage);
		}

		nlapiLogExecution('debug', 'done', 'done');
	} catch (err) {
		nlapiLogExecution('ERROR', 'userEventBeforeSubmit', err);
		throw err;
	}
}

function extractNumberFromComment(textAmount) {
	var index = textAmount.indexOf('$');
	var index2 = textAmount.indexOf('INR');
	var index3 = textAmount.indexOf('GBP');
	var index4 = textAmount.indexOf('£'); //€ 
	var index5 = textAmount.indexOf('€');
	
	nlapiLogExecution('debug', 'index', index);
	nlapiLogExecution('debug', 'index2', index2);
	nlapiLogExecution('debug', 'index3', index3);
	nlapiLogExecution('debug', 'index4', index4);
	
	
	if (index == -1 && index2 == -1 && index3 == -1 && index4 == -1 && index5 == -1) {
		throw "Invalid Amount in comments - No $ or INR symbol found.";
	}

	if (index > 0 && index2 > 0 && index3 > 0 && index4> 0 && index5 > 0) {
		throw "Invalid Amount Format - should start with $ or INR sign";
	}

	//var splitIndex = index != -1 ? '$' : 'INR';
	
	var splitIndex = '';

	if(index >= 0)
	{
		splitIndex = '$';
	}
	else if(index2 >= 0)
	{
		splitIndex = 'INR';
	}
	else if(index3 >= 0)
	{
		splitIndex = 'GBP';
	}
 	 else if(index5 >= 0)
	{
		splitIndex = '€';
	}
	else
	{
		splitIndex= '£';
	}
		
	if(splitIndex)
	{
		var amount = textAmount.split(splitIndex)[1].trim();
	}

	nlapiLogExecution('debug', 'amount', parseFloat(amount));
	if(amount){
      amount = parseFloat(amount)
    }
	if (!isNumeric(amount)) {
		throw "Invalid Amount - enter only numbers";
	}

	return amount;
}

function isNumeric(value) {
	return !isNaN(value);
}