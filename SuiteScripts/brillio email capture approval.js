const reject_action = "workflowaction1013";
const reject_action_final = "workflowaction1015";
const approve_action = "workflowaction1012";
const final_action = "workflowaction1014";
const workflow_id = "customworkflow_brillio_vendorbill_workfl";

function process(email) {

    try {
        var from_address = email.getFrom();
        var msg_body = email.getTextBody().toUpperCase().slice(0,50);
        var subject = email.getSubject();

        nlapiLogExecution('DEBUG', 'process', 'from_address --> ' + from_address);
        nlapiLogExecution('DEBUG', 'process', 'msg_body --> ' + msg_body);
        nlapiLogExecution('DEBUG', 'process', 'subject --> ' + subject);

        var usrac;
        var record_id = subject.split(":")[2];
        var level = subject.split(":")[3];

        nlapiLogExecution('DEBUG', "record_id", record_id);

        nlapiLogExecution('DEBUG', "level", level);

        nlapiLogExecution('DEBUG', "approved check",  msg_body.indexOf("APPROVED"));

        nlapiLogExecution('DEBUG', "rejected check",  msg_body.indexOf("REJECTED"));

        if (record_id) {

            if (msg_body.indexOf("APPROVED") != -1) {
                nlapiLogExecution('DEBUG', "approved", true);
                if (level == 1)
                usrac = "approve"
                else if (level == 2)
                usrac = "approve1"

            } else if (msg_body.indexOf("REJECTED") != -1) {
                nlapiLogExecution('DEBUG', "rejected", true);
                usrac = "reject"
            }
nlapiLogExecution('DEBUG', "usrac", usrac);
            var loadrec = nlapiLoadRecord("vendorbill", record_id);

            var approver_email_list = [
               "deepa.manoharan@brillio.com",
              "deepa@concinnate.partners",
              "bhavani.sankargj@brillio.com",
              "krishna.d@brillio.com",
              "sathish2engineer@gmail.com"
                                      ];

            if (level == 1) {

                var approver = loadrec.getFieldValue("custbody_verticalhead");
                nlapiLogExecution('DEBUG', "APPROVER", approver);
                var app_email = nlapiLookupField('employee', approver, "email");
                approver_email_list.push(app_email)
            } else {
                var finance_manager = loadrec.getFieldValue("custbody_financemanager").split("\u0005");
                nlapiLogExecution('DEBUG', "finance_manager", finance_manager);
                if (finance_manager) {
                    for (var i = 0; i < finance_manager.length; i++) {

                        nlapiLogExecution('DEBUG', "finance_manager loop", finance_manager[i]);
                        if(finance_manager[i]!=null &&finance_manager[i]!="")
                        {
                            var fin_email = nlapiLookupField('employee', finance_manager[i], "email");
                            approver_email_list.push(fin_email);
                        }
                    }
                }

            }

            var flag = false;

           

            for(var i=0; i< approver_email_list.length; i++)
            {
                var str = new String(from_address);

                nlapiLogExecution('DEBUG', "approver_email_list[i]",approver_email_list[i]);

                if(approver_email_list[i].toUpperCase() == str.toUpperCase() || approver_email_list[i].toUpperCase().indexOf("BRILLIO.COM") != -1 )
                {
                    nlapiLogExecution('DEBUG', "inside:str",str);
                    flag = true
                }
            }


            var status = loadrec.getFieldValue("approvalstatus");
nlapiLogExecution('DEBUG', "inside status ", status);
            if (flag)
            {

                nlapiLogExecution('DEBUG', "inside approval ", "true");

                switch (usrac) {
                    case "approve":
                        
                nlapiLogExecution('DEBUG', "approve case", "true");

                            triggerWorkFlow(record_id, approve_action);
                        break;
    
                    case "reject":
                        
                nlapiLogExecution('DEBUG', "reject case", "true");

                        if (status == 1)
                        {
                            if(level==1)
                            {
                                triggerWorkFlow(record_id, reject_action);

                            }else if (level ==2)
                            {
                                triggerWorkFlow(record_id, reject_action_final);  
                            }
                        }
                            
                        break;
    
                    case "approve1":
                        triggerWorkFlow(record_id, final_action);
                        break
                }
            }
           
        }

    } catch (e) {
        nlapiLogExecution('DEBUG', 'process', e);
    }
    // var fromAddress = email.getFrom();
    // logAddress('from', fromAddress);

    // var to = email.getTo();
    // for (var indexTo in to)
    // {
    //    logAddress('to', to[indexTo]);
    // }
    // var cc = email.getCc();
    // for (var indexCc in cc)
    // {
    //    logAddress('cc', cc[indexCc]);
    // }

    // logAddress('replyTo', email.getReplyTo());
    // log('sent', email.getSentDate());
    // log('subject', email.getSubject());
    // log('text body', email.getTextBody());
    // log('html body', email.getHtmlBody());

    // var attachments = email.getAttachments();
    // for (var indexAtt in attachments)
    // {
    //    logAttachment('att', attachments[indexAtt]);
    // }


}


function triggerWorkFlow(record_id, action) {
    return nlapiTriggerWorkflow("vendorbill", record_id, workflow_id, action);
}