/**
 * Module Description
 * 
 * Version Date Author Remarks 1.00 19 Mar 2015 nitish.mishra
 * 
 */

function suiteletUtilization(request, response) {

	try {

		if (request.getMethod() == 'GET') {
			var mode = request.getParameter("mode");
			nlapiLogExecution('debug', 'start');

			if (mode == "EXCEL") {
				getUtilizationExcelWise();
			}
			else {
				createUtilizationForm();
			}

			nlapiLogExecution('debug', 'end');
		}
	}
	catch (err) {
		displayErrorForm(err);
	}
}

function generateUtilizationExcel() {

	try {
		var deployment = nlapiGetContext().getDeploymentId();
		var identifierColumn = null , tableColumnHeader = null;

		if (deployment == "customdeploy_sut_allocation_rep_vertical") {
			identifierColumn = "Vertical";
			tableColumnHeader = "Customer Vertical";
		}
		else if (deployment == "customdeploy_sut_allocation_rep_practice") {
			identifierColumn = "Practice";
			tableColumnHeader = "Employee Practice";
		}
		else if (deployment == "customdeploy_sut_allocation_rep_customer") {
			identifierColumn = "Customer";
			tableColumnHeader = "Project";
		}
		else {
			throw "Wrong deployment";
		}


		var classificationList_1 = getNormalClassification();
		var classificationList_2 = getProjectClassification();

		var allocationList =
				pullAllocation(allocationData, classificationList_1, classificationList_2,
						identifierColumn);

		var excelData = generateUtilizationExcel(allocationList.Report1, tableColumnHeader);

		var fileName = 'Utilization Report.csv';
		var file = nlapiCreateFile(fileName, 'CSV', excelData);
		response.setContentType('CSV', fileName);
		response.write(file.getValue());
	}
	catch (err) {
		nlapiLogExecution('ERROR', 'generateUtilizationExcel', err);
		throw err;
	}
}

function createGetFormUtilization(mode) {

	try {

		var deployementId = nlapiGetContext().getDeploymentId();

		switch (deployementId) {
			case "customdeploy_st_utilization_rep_vertical":
				if (mode == "EXCEL") {
					getUtilizationExcelVerticalWise();
				}
				else {
					createUtilizationVerticalForm();
				}
			break;
			case "customdeploy_st_utilization_rep_practice":

				if (mode == "EXCEL") {
					getUtilizationExcelPracticeWise();
				}
				else {
					createUtilizationPracticeForm();
				}
			break;
		}

	}
	catch (err) {
		nlapiLogExecution('ERROR', 'createGetFormUtilization', err);
		throw err;
	}
}


function createUtilizationForm() {

	try {
		var form = nlapiCreateForm('Utilization Report', false);
		var allocationData = getAllAllocation();

		var context = nlapiGetContext();
		var url = nlapiResolveURL('SUITELET', context.getScriptId(), context.getDeploymentId());
		url += "&mode=EXCEL";

		var html = "";
		html += getUtilization(allocationData);
		html += "<br/><br/><a href='" + url + "' target='_blank'>Export To Excel</a>";

		form.addField('custpage_0', 'inlinehtml', '').setDefaultValue(html);
		response.writePage(form);
	}
	catch (err) {
		nlapiLogExecution('ERROR', 'createUtilizationForm', err);
		throw err;
	}
}


function getUtilization(allocationData) {

	try {
		var deployment = nlapiGetContext().getDeploymentId();
		var identifierColumn = null , tableColumnHeader = null;

		if (deployment == "customdeploy_st_utilization_rep_vertical") {
			identifierColumn = "Vertical";
			tableColumnHeader = "Customer Vertical";
		}
		else if (deployment == "customdeploy_st_utilization_rep_practice") {
			identifierColumn = "Practice";
			tableColumnHeader = "Employee Practice";
		}
		else if (deployment == "customdeploy_st_utilization_rep_customer") {
			identifierColumn = "Customer";
			tableColumnHeader = "Project";
		}
		else {
			throw "Wrong deployment";
		}


		var classificationList_1 = getNormalClassification();
		var classificationList_2 = getProjectClassification();

		var allocationList =
				pullAllocation(allocationData, classificationList_1, classificationList_2,
						identifierColumn);

		return generateUtilizationTable(allocationList.Report1, tableColumnHeader);
	}
	catch (err) {
		nlapiLogExecution('ERROR', 'getUtilization', err);
		throw err;
	}
}

function generateUtilizationExcel(allocationData, headerName) {

	try {
		var utilizationData = generateUtilization(allocationData);

		var csvText = "";

		// First Row
		csvText +=
				headerName + "," + "Offsite (+) Trainee," + "Offsite (-) Trainee," + "Onsite,"
						+ "Overall (+) Trainee," + "Overall (-) Trainee" + "\n";

		var line = "";
		var data = "";
		var row = null;

		// add Rows from Utilization
		for (var i = 0; i < utilizationData.length - 1; i++) {
			row = utilizationData[i];
			line = '';
			line += row.NodeName + ",";
			line += row.OffsiteTraineePositive + ",";
			line += row.OffisteTraineeNegative + ",";
			line += row.Onsite + ",";
			line += row.OverallTraineePositive + ",";
			line += row.OverallTraineeNegative;
			line += "\n";
			data += line;
		}

		// add grand total row
		row = utilizationData[utilizationData.length - 1];
		line = '';
		line += row.NodeName + ",";
		line += row.OffsiteTraineePositive + ",";
		line += row.OffisteTraineeNegative + ",";
		line += row.Onsite + ",";
		line += row.OverallTraineePositive + ",";
		line += row.OverallTraineeNegative;
		line += "\n";
		data += line;

		csvText += data;

		return csvText;
	}
	catch (err) {
		nlapiLogExecution('ERROR', 'generateUtilizationExcel', err);
		throw err;
	}
}

function generateUtilizationTable(allocationData, headerName) {

	try {
		var utilizationData = generateUtilization(allocationData);

		// add the CSS
		var htmlText = getTableCss();

		htmlText += "<table class='tableCss' width='50%'>";

		// First Row
		htmlText +=
				"<tr class='headRow boldCell'>" + "<td>" + headerName + "</td>"
						+ "<td>Offsite (+) Trainee</td>" + "<td>Offsite (-) Trainee</td>"
						+ "<td>Onsite</td>" + "<td>Overall (+) Trainee</td>"
						+ "<td>Overall (-) Trainee</td>" + "</tr>";

		var line = "";
		var data = "";
		var row = null;

		// add Rows from Utilization
		for (var i = 1; i < utilizationData.length; i++) {
			row = utilizationData[i];
			line = "<tr>";
			line += "<td class='leftCell boldCell headColumn'>" + row.NodeName + "</td>";
			line += "<td class='utilization'>" + row.OffsiteTraineePositive + "</td>";
			line += "<td class='utilization'>" + row.OffisteTraineeNegative + "</td>";
			line += "<td class='utilization'>" + row.Onsite + "</td>";
			line += "<td class='utilization'>" + row.OverallTraineePositive + "</td>";
			line += "<td class='utilization'>" + row.OverallTraineeNegative + "</td>";
			line += "</tr>";
			data += line;
		}

		// add grand total row
		row = utilizationData[0];
		line = "<tr class='totalRow'>";
		line += "<td class='leftCell boldCell headColumn'>" + row.NodeName + "</td>";
		line += "<td class=''>" + row.OffsiteTraineePositive + "</td>";
		line += "<td class=''>" + row.OffisteTraineeNegative + "</td>";
		line += "<td class=''>" + row.Onsite + "</td>";
		line += "<td class=''>" + row.OverallTraineePositive + "</td>";
		line += "<td class=''>" + row.OverallTraineeNegative + "</td>";
		line += "</tr>";
		data += line;

		htmlText += data;

		htmlText += "</table>";

		return htmlText;
	}
	catch (err) {
		nlapiLogExecution('ERROR', 'generateUtilizationTable', err);
		throw err;
	}
}

function generateUtilization(allocationDetails) {

	try {
		var utilizationData = [];

		allocationDetails
				.forEach(function(allocation) {

					// get values
					var offsiteBilled = allocation.OffSite.Billed;
					var offsiteTrainee = allocation.OffSite.Trainee;
					var offsiteTotal = allocation.OffSite.Total;
					var onsiteBilled = allocation.OnSite.Billed;
					var onsiteTotal = allocation.OnSite.Total;

					// insert all blank
					var offsiteTraineePositive = '';
					var offsiteTraineeNegative = '';
					var onsite = '';
					var overallTraineePositive = '';
					var overallTraineeNegative = '';

					if (offsiteTotal != 0) {
						var offsiteTraineePositive =
								parseInt((offsiteBilled / offsiteTotal) * 100) + " %";
						var offsiteTraineeNegative =
								parseInt((offsiteBilled / (offsiteTotal - offsiteTrainee)) * 100)
										+ " %";
					}

					if (onsiteTotal != 0) {
						var onsite = parseInt((onsiteBilled / onsiteTotal) * 100) + " %";
					}

					if (onsiteTotal == 0 && onsiteTotal == 0) {
						var overallTraineePositive =
								parseInt(((offsiteBilled + onsiteBilled) / (offsiteTotal + onsiteTotal)) * 100)
										+ " %";
						var overallTraineeNegative =
								parseInt(((offsiteBilled + onsiteBilled) / (offsiteTotal
										+ onsiteTotal - offsiteTrainee)) * 100)
										+ " %";
					}

					// push to the resultant array
					utilizationData.push({
						NodeName : allocation.NodeName,
						OffsiteTraineePositive : offsiteTraineePositive,
						OffisteTraineeNegative : offsiteTraineeNegative,
						Onsite : onsite,
						OverallTraineePositive : overallTraineePositive,
						OverallTraineeNegative : overallTraineeNegative
					});
				});

		return utilizationData;
	}
	catch (err) {
		nlapiLogExecution('ERROR', 'generateUtilization', err);
	}
}
