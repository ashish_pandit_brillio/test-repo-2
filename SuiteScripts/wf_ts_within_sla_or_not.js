function workflowAction(type) {
    
    var flag = false;
      
    var startTime = new Date();
    nlapiLogExecution('DEBUG', 'current Date and Tiem',startTime);

    var currentContext = nlapiGetContext();

    var currentTSRecord = nlapiLoadRecord(nlapiGetRecordType(), nlapiGetRecordId());
    var currentEnd   = currentTSRecord.getFieldValue('enddate');
    var currentStart   = currentTSRecord.getFieldValue('startdate');
    var employeeSubsidiary = currentTSRecord.getFieldValue('subsidiary');
    var submissionTS = currentTSRecord.getFieldValue('custrecord_date_time_test');

    var actualSubmissionTime = startTime;
    
    //BTPL,Standav,UK,Cognetik
    if(employeeSubsidiary=='3' || employeeSubsidiary=='7' || employeeSubsidiary=='12' || employeeSubsidiary=='13'){
        var moment_date = moment(startTime).utcOffset("+05:30").format("MM/DD/YYYY");
        startTime = nlapiStringToDate(moment_date);
        nlapiLogExecution('DEBUG', 'startTime - moment',startTime);
    }
    //startTime = nlapiAddDays(startTime, 7); //test line
    
    
	startTime = nlapiDateToString(startTime,'date');
	startTime = nlapiStringToDate(startTime);
	
    
    //var currentYear = startTime.getFullYear();
    
    
    nlapiLogExecution('DEBUG', 'startTime - after subsidiary',startTime);
    

    
  
    /*if(employeeSubsidiary=='3'){
    var moment_date = moment(startTime).utcOffset("+05:30").format("MM/DD/YYYY");
    moment_date = new Date(moment_date);
      
    var moment_us = moment.utc().format("MM/DD/YYYY");
      
    nlapiLogExecution('DEBUG', 'moment_us Date',moment_us);
    nlapiLogExecution('DEBUG', 'moment_us new Date',new Date(moment_us));
    
    nlapiLogExecution('DEBUG', 'moment Date',moment_date);
    nlapiLogExecution('DEBUG', 'moment Date',new Date(moment_date));
    startTime = new Date(moment_date);
    }*/

    nlapiLogExecution('DEBUG', 'currentEnd',currentEnd);
    nlapiLogExecution('DEBUG', 'currentStart',currentStart);
        
    nlapiLogExecution('DEBUG', 'currentTSRecord',JSON.stringify(currentTSRecord));
        
    var i_employee_id = currentTSRecord.getFieldValue('employee');
 
    
    var d_end_date =	nlapiStringToDate(currentEnd);
    var start_DATE =    nlapiStringToDate(currentStart);
    //currentStart
	var currentYear = start_DATE.getFullYear();
    nlapiLogExecution('DEBUG', 'currentYear',currentYear);
    var endDateTS = '';

    var current_day = startTime.getDay();
    nlapiLogExecution('DEBUG', 'current_day',current_day);
       var end_day = '';
        if(current_day==1){
         end_day = 2;
         endDateTS = nlapiAddDays(startTime, -end_day);
        } else if(current_day==0){
		 end_day = 1;
         endDateTS = nlapiAddDays(startTime, -end_day);
			
		} else {
          endDateTS = startTime
        } 
    
    
    
    nlapiLogExecution('DEBUG', 'Submission endDateTS',endDateTS);
    nlapiLogExecution('DEBUG', 'TS d_end_date',d_end_date);
            if(d_end_date<endDateTS && parseInt(currentYear)>=2022 && !_logValidation(submissionTS)){
              nlapiLogExecution('DEBUG', 'entered 1','entered 1');
                flag = getResourceAllocationsForEmployee(i_employee_id, currentStart, currentEnd);
                if(flag == true){
                  nlapiLogExecution('DEBUG', 'entered 2','entered 2');
                  nlapiLogExecution('DEBUG', 'flag',flag);
                  
                  //currentTSRecord.setFieldValue('custrecord_ts_submission_timestamp', startTime.toString()); //
                  currentTSRecord.setFieldValue('custrecord_ts_outside_sla', "T");
                  currentTSRecord.setFieldValue('custrecord_date_time_test', actualSubmissionTime);
                  nlapiSubmitRecord(currentTSRecord);
                  
                  
                  

                } else {
					if(!_logValidation(submissionTS) && parseInt(currentYear)>=2022){
				currentTSRecord.setFieldValue('custrecord_date_time_test', actualSubmissionTime);
                 nlapiSubmitRecord(currentTSRecord);
				
			     }
					
				}

            } else {
				if(!_logValidation(submissionTS) && parseInt(currentYear)>=2022){
				currentTSRecord.setFieldValue('custrecord_date_time_test', actualSubmissionTime);
                 nlapiSubmitRecord(currentTSRecord);
				
			     }
				
				
			}
			
			
			
			

  

    

        return true;
    }

function getResourceAllocationsForEmployee(i_employee_id, d_start_date, d_end_date)
{

    var flag = false;
    
    var filters = new Array();
        filters[0] = new nlobjSearchFilter('custeventbstartdate', null,
            'notafter', d_end_date);
        filters[1] = new nlobjSearchFilter('custeventbenddate', null, 'notbefore',
        d_start_date);
        filters[2] = new nlobjSearchFilter('custeventrbillable', null, 'is', 'T');
        filters[3] = new nlobjSearchFilter('jobbillingtype', 'job', 'anyof', 'TM');
        filters[4] = new nlobjSearchFilter('resource', null,
            'anyof', i_employee_id); //
        filters[5] = new nlobjSearchFilter('customer', null, 'noneof',
        '5766') // SB - VEDS account
        

        var columns = new Array();

        columns[0] = new nlobjSearchColumn('custeventbstartdate', null, 'group');
        columns[1] = new nlobjSearchColumn('custeventbenddate', null, 'group');
        columns[2] = new nlobjSearchColumn('resource', null, 'group');
        columns[3] = new nlobjSearchColumn('custevent4', null, 'group');
        columns[4] = new nlobjSearchColumn("custentity_hoursperday","job","GROUP");
        columns[5] = new nlobjSearchColumn("custentity_onsite_hours_per_day","job","GROUP");
        columns[6] = new nlobjSearchColumn("percentoftime",null,"avg");
        columns[7] =new nlobjSearchColumn("custentityproject_holiday","job","GROUP")

        columns[8] = new nlobjSearchColumn("customer",null,"GROUP"), 
        columns[9] = new nlobjSearchColumn("subsidiary","employee","GROUP")

        try {
            var search_results = searchRecord('resourceallocation', null, filters,
                columns);
        } catch (e) {
            nlapiLogExecution('ERROR', 'Error', e.message);
        }

        nlapiLogExecution('debug', 'search_results.length', search_results.length);

        if(search_results.length>0){
            flag = true;

        }

        return flag;

       

        
    
}

function searchRecord(recordType, savedSearch, arrFilters, arrColumns,
    filterExpression) {

    try {
        var search = null;

        // if a saved search is provided, load it and add the filters and
        // columns
        if (isNotEmpty(savedSearch)) {
            search = nlapiLoadSearch(recordType, savedSearch);

            if (isArrayNotEmpty(arrFilters)) {
                search.addFilters(arrFilters);
            }

            if (isArrayNotEmpty(arrColumns)) {
                search.addColumns(arrColumns);
            }

            if (isArrayNotEmpty(filterExpression)) {
                search.setFilterExpression(filterExpression);
            }
        }
        // create a new search
        else {
            search = nlapiCreateSearch(recordType, arrFilters, arrColumns);
        }

        // run search
        var resultSet = search.runSearch();

        // iterate through the search and get all data 1000 at a time
        var searchResultCount = 0;
        var resultSlice = null;
        var searchResult = [];

        do {
            resultSlice = resultSet.getResults(searchResultCount,
                searchResultCount + 1000);

            if (resultSlice) {

                resultSlice.forEach(function(result) {

                    searchResult.push(result);
                    searchResultCount++;
                });
            }
        } while (isArrayNotEmpty(resultSlice) && resultSlice.length >= 1000);

        return searchResult;
    } catch (err) {
        nlapiLogExecution('ERROR', 'searchRecord', err);
        throw err;
    }
}

function isArrayEmpty(argArray) {

    return !isArrayNotEmpty(argArray);
}

function isArrayNotEmpty(argArray) {

    return (isNotEmpty(argArray) && argArray.length > 0);
}

function isEmpty(value) {

    return value == null || value == "" || typeof(value) == undefined;
}

function isNotEmpty(value) {

    return !isEmpty(value);
}

function isTrue(value) {

    return value == 'T';
}

function isFalse(value) {

    return value != 'T';
}


function _logValidation(value) {
    if (value != '- None -' && value != null && value.toString() != null && value != '' && value != undefined && value.toString() != undefined && value != 'undefined' && value.toString() != 'undefined' && value.toString() != 'NaN' && value != NaN) {
        return true;
    } else {
        return false;
    }
}

   