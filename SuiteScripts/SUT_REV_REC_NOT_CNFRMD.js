/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       03 Nov 2017     bidhi.raj
 *
 */

/**
 * @param {nlobjRequest} request Request object
 * @param {nlobjResponse} response Response object
 * @returns {Void} Any output is written via response object
 */
function suitelet(request, response) {
	if (request.getMethod() == 'GET') {
		var form = nlapiCreateForm('REV_REC_NOT_CONFIRMED_REPORT');
		var select = form.addField('custpage_field0', 'select', 'Month','null', 'month').setMandatory(true);
		select.addSelectOption('', '');
		select.addSelectOption('1', 'January');
		select.addSelectOption('2', 'February');
		select.addSelectOption('3', 'March');
		select.addSelectOption('4', 'April');
		select.addSelectOption('5', 'May');
		select.addSelectOption('6', 'June');
		select.addSelectOption('7', 'July');
		select.addSelectOption('8', 'August');
		select.addSelectOption('9', 'September');
		select.addSelectOption('10', 'October');
		select.addSelectOption('11', 'November');
		select.addSelectOption('12', 'December');

		form.addField('custpage_field1', 'text', 'Year').setMandatory(true).setLabel('year as YYYY');
		var rev_rec=form.addField('custpage_field2', 'select', 'FP Rev Rec Type').setMandatory(true);
		rev_rec.addSelectOption('1', 'Milestone');
		rev_rec.addSelectOption('4', 'FP Others');
		form.addSubmitButton('Get Report');
		response.writePage(form);
	}
	else {
		var month = request.getParameter('custpage_field0');
		var year = request.getParameter('custpage_field1');
		var rev_rec_type=request.getParameter('custpage_field2');
		nlapiLogExecution('DEBUG','RevRectype',rev_rec_type);
		if(rev_rec_type==1)
		{
				var filters = new Array();
				
				var _colz1 = new Array();
				_colz1.push(new nlobjSearchColumn('custrecord_revenue_share_project'));
				_colz1.push(new nlobjSearchColumn('custrecord_revenue_share_approval_status'));
				
				var str_date = month+'/'+'1'+'/'+year;
				var str_date_Date = new Date(str_date); 
				var end_dat = new Date(str_date_Date.getFullYear(), str_date_Date.getMonth() + 1, 0);
				
				var str_end_dat = nlapiDateToString(end_dat, 'mm/dd/yyyy');
				
				//filters.push(new nlobjSearchFilter('custrecord_proj_end_date_revenue_cap', null, 'onorafter',[str_date]));
				//filters.push(new nlobjSearchFilter('custrecord_revenue_share_project.enddate','custrecord_revenue_share_parent_json', 'onorafter',[str_date]));
				
				filters.push(new nlobjSearchFilter('custrecord_revenue_share_approval_status', null, 'is',['3']));
					
				var rev_rec = nlapiSearchRecord('customrecord_revenue_share', null, filters, _colz1);//415 -record id for rev cap
				var project_ids = Array();
				
			for(var i in rev_rec){
				if(rev_rec[i].getValue('custrecord_revenue_share_project')){
					var inid_Pro = rev_rec[i].getValue('custrecord_revenue_share_project');
					var proj_end_date = nlapiLookupField('job', inid_Pro, 'enddate', null);
					if(proj_end_date >= str_date){
						project_ids.push( rev_rec[i].getValue('custrecord_revenue_share_project'));
					}
					
			   }		
			}
			var filter2 = new Array();
			filter2.push(new nlobjSearchFilter('custrecord_current_mnth_no', null, 'equalto',[month-1]));
		   // filter2.push(new nlobjSearchFilter('custrecord_is_mnth_effrt_confirmed', null, 'is','F'));
			filter2.push(new nlobjSearchFilter('custrecord_revenue_share_project','custrecord_revenue_share_parent_json','anyof',project_ids));
			filter2.push(new nlobjSearchFilter('created', null, 'within', [str_date , str_end_dat]));
		   
			var colz = new Array();
			colz.push(new nlobjSearchColumn('custrecord_is_mnth_effrt_confirmed'));
			colz.push(new nlobjSearchColumn('custrecord_current_mnth_no'));
			colz.push(new nlobjSearchColumn('custrecord_revenue_share_project','custrecord_revenue_share_parent_json'));
			var rev_rec_mon_end_effrt_jsn = nlapiSearchRecord('customrecord_fp_rev_rec_mnth_end_json', null, filter2, colz);
			var projarray = new Array();
			var remArray = new Array();
			var html = "";
			html += "Project,";
			html += "Confirmed,";
			html += "\r\n";
			for(var i in rev_rec_mon_end_effrt_jsn){
				var prjt = rev_rec_mon_end_effrt_jsn[i].getText('custrecord_revenue_share_project','custrecord_revenue_share_parent_json');
				var status = rev_rec_mon_end_effrt_jsn[i].getValue('custrecord_is_mnth_effrt_confirmed');
				var monthOfREv = rev_rec_mon_end_effrt_jsn[i].getValue('custrecord_current_mnth_no');
				var l = status.length;
			  if(prjt == 'BRSC03085 Bristol-Myers Squibb Company : Corporate Analytics for Communications'){
			  nlapiLogExecution('Debug', 'len more', status);  
			  }
				if(status == 'F' && l == 1 && monthOfREv == month-1){
					projarray.push(prjt);	
				}
				if(status == 'T'){
					remArray.push(prjt);
				}
			}
		   
			projarray = removearrayduplicate(projarray);
			remArray = removearrayduplicate(remArray);
			
			const filteredArray = projarray.filter(function(x) { 
			  return remArray.indexOf(x) < 0;
			})
			for(var i in filteredArray){
				var tempro = filteredArray[i].replace (/,/g, "");
				html += tempro+",";
				html += 'F,';
				html += "\r\n";
			}
			var fileName = 'FP_REV_REC_NOT_CONFIRMED- month '+month+' year '+year+'.csv';
			var file = nlapiCreateFile(fileName, 'CSV', html);
			response.setContentType('CSV', fileName);
			response.write(file.getValue());
		}
	else{
			var filters = new Array();
			var _colz1 = new Array();
			_colz1.push(new nlobjSearchColumn('custrecord_fp_rev_rec_others_projec'));
			_colz1.push(new nlobjSearchColumn('custrecord_revenue_other_approval_status'));
				
			var str_date = month+'/'+'1'+'/'+year;
			var str_date_Date = new Date(str_date); 
			var end_dat = new Date(str_date_Date.getFullYear(), str_date_Date.getMonth() + 1, 0);
				
			var str_end_dat = nlapiDateToString(end_dat, 'mm/dd/yyyy');
				
			filters.push(new nlobjSearchFilter('custrecord_revenue_other_approval_status', null, 'is',['2']));
					
			var rev_rec = nlapiSearchRecord('customrecord_fp_rev_rec_others_parent', null, filters, _colz1);//415 -record id for rev cap
			var project_ids = Array();
				
			for(var i in rev_rec){
				if(rev_rec[i].getValue('custrecord_fp_rev_rec_others_projec')){
					var inid_Pro = rev_rec[i].getValue('custrecord_fp_rev_rec_others_projec');
					var proj_end_date = nlapiLookupField('job', inid_Pro, 'enddate', null);
					if(proj_end_date >= str_date){
						project_ids.push( rev_rec[i].getValue('custrecord_fp_rev_rec_others_projec'));
					}
					
			   }		
			}
			var filter2 = new Array();
			filter2.push(new nlobjSearchFilter('custrecord_other_current_mnth_no', null, 'equalto',[month-1]));
		   // filter2.push(new nlobjSearchFilter('custrecord_is_mnth_effrt_confirmed', null, 'is','F'));
			filter2.push(new nlobjSearchFilter('custrecord_fp_rev_rec_others_projec','custrecord_fp_others_mnth_end_fp_parent','anyof',project_ids));
			filter2.push(new nlobjSearchFilter('created', null, 'within', [str_date , str_end_dat]));
		   
			var colz = new Array();
			colz.push(new nlobjSearchColumn('custrecord_is_mnth_end_cfrmd'));
			colz.push(new nlobjSearchColumn('custrecord_other_current_mnth_no'));
			colz.push(new nlobjSearchColumn('custrecord_fp_rev_rec_others_projec','custrecord_fp_others_mnth_end_fp_parent'));
			var rev_rec_mon_end_effrt_jsn = nlapiSearchRecord('customrecord_fp_others_mnth_end_json', null, filter2, colz);
			var projarray = new Array();
			var remArray = new Array();
			var html = "";
			html += "Project,";
			html += "Confirmed,";
			html += "\r\n";
			for(var i in rev_rec_mon_end_effrt_jsn){
				var prjt = rev_rec_mon_end_effrt_jsn[i].getText('custrecord_fp_rev_rec_others_projec','custrecord_fp_others_mnth_end_fp_parent');
				var status = rev_rec_mon_end_effrt_jsn[i].getValue('custrecord_is_mnth_end_cfrmd');
				var monthOfREv = rev_rec_mon_end_effrt_jsn[i].getValue('custrecord_other_current_mnth_no');
				var l = status.length;
			  if(prjt == 'BRSC03085 Bristol-Myers Squibb Company : Corporate Analytics for Communications'){
			  nlapiLogExecution('Debug', 'len more', status);  
			  }
				if(status == 'F' && l == 1 && monthOfREv == month-1){
					projarray.push(prjt);	
				}
				if(status == 'T'){
					remArray.push(prjt);
				}
			}
		   
			projarray = removearrayduplicate(projarray);
			remArray = removearrayduplicate(remArray);
			
			const filteredArray1 = projarray.filter(function(x) { 
			  return remArray.indexOf(x) < 0;
			})
			for(var i in filteredArray1){
				var tempro = filteredArray1[i].replace (/,/g, "");
				html += tempro+",";
				html += 'F,';
				html += "\r\n";
			}
			var fileName = 'FP_REV_REC_NOT_CONFIRMED- month '+month+' year '+year+'.csv';
			var file = nlapiCreateFile(fileName, 'CSV', html);
			response.setContentType('CSV', fileName);
			response.write(file.getValue());
		}
   
	}
}
function removearrayduplicate(cp) {
	var newArray = new Array();
	label: for (var i = 0; i < cp.length; i++) {
		for (var j = 0; j < cp.length; j++) {
			if (newArray[j] == cp[i])
				continue label;
		}
		newArray[newArray.length] = cp[i];
	}
	return newArray;
}
