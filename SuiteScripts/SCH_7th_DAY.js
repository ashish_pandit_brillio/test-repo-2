/**
 * Module Description
 * 
 * Version      Date                          Author                          Remarks  
 * 1.00         29th Oct 2015             Manikandan v          Send 7 days completion mailer to the Employees scheduled at 9 a.m.
 */

function Main(){
try{

var columns = new Array();
columns[0]=new nlobjSearchColumn('firstname');
columns[1]=new nlobjSearchColumn('email');

// search for employee with 7th days of service as of today

var emp_search = nlapiSearchRecord('employee', 'customsearch_7th_day_completion_emailer', null,columns);

nlapiLogExecution('debug', 'emp_search.length',emp_search.length);

if(isNotEmpty(emp_search)){
	
	for(var i=0;i<emp_search.length;i++){
		
		
		sendServiceMails(
			emp_search[i].getValue('firstname'),
			emp_search[i].getValue('email'),
			emp_search[i].getId());
	}
}

}
catch(err){
nlapiLogExecution('error', 'Main', err);
}
}

function sendServiceMails(firstName, email, emp_id){
	
	try{
                //var AttachmentID=32680;
                var fileObj = nlapiLoadFile(220846);
		var mailTemplate = serviceTemplate(firstName);		
		nlapiLogExecution('debug', 'chekpoint',email);
		nlapiSendEmail('10730', email, mailTemplate.MailSubject, mailTemplate.MailBody,null,null,null,fileObj, {entity: emp_id});
	}
	catch(err){
nlapiLogExecution('error', 'sendServiceMails', err);
throw err;
     }
}

function serviceTemplate(firstName) {
    var htmltext = '';
    
    htmltext += '<table border="0" width="100%"><tr>';
    htmltext += '<td colspan="4" valign="top">';
    //htmltext += '<p>Hi ' + firstName + ',</p>';  
htmltext += '<p>Dear ' + firstName + ',</p>';
//htmltext += '&nbsp';
 
//htmltext += &nbsp;
htmltext += '<p> Please find attached the User Guide on medical insurance enrollment. </p>';
htmltext += '<p>You will receive a separate notification from the medimanage team with your login credentials</p>';

htmltext +='<p>Request you to log in and enroll yourself and your dependent family members for the medical insurance coverage, within 2 business days of receiving the email notification from the medimanage team.</p>'

htmltext += '<p>Do feel free to log a query in HR Hub for any clarification.</p>'


htmltext += '<p>Thanks & Regards,</p>';
htmltext += '<p>Team HR</p>';
 
 
    htmltext += '</td></tr>';
    htmltext += '</table>';
    htmltext += '<hr width="100%" size="1" noshade color="#CCCCCC">';
    htmltext += '<table width="100%" border="0" cellspacing="0" cellpadding="0">';
    htmltext += '<tr>';
    htmltext += '<td align="right">';
    htmltext += '<font style="font-size:9px; font-family:Verdana,Arial,Helvetica,sans-serif; color:#999999;">Brillio is powered by <a href="http://www.netsuite.com/" style="color:#999999;">NetSuite</a> One System. No Limits.</font>';
    htmltext += '</td>';
    htmltext += '</tr>';
    htmltext += '</table>';
    htmltext += '</body>';
    htmltext += '</html>';
 
    return {
        MailBody : htmltext,
        MailSubject : "Medical Insurance Enrollment User Guide "      
    };
}

