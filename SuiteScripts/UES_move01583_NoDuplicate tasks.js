/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       27 Oct 2016     shruthi.l
 *
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType
 * 
 * @param {String} type Operation types: create, edit, delete, xedit
 *                      approve, reject, cancel (SO, ER, Time Bill, PO & RMA only)
 *                      pack, ship (IF)
 *                      markcomplete (Call, Task)
 *                      reassign (Case)
 *                      editforecast (Opp, Estimate)
 * @returns {Void}
 */
 
 function task_status_before_load(type){
 if(type == 'create'){
 nlapiSetFieldValue('status','PROGRESS');
 }
 }
function userEventBeforeSubmit(type){
	if(type == 'create'){
	var project = nlapiGetFieldValue('company');
	nlapiLogExecution('Debug', 'company', project);
	
 //if(project == 15383)
	 {
	 var project_task_name = nlapiGetFieldValue('title');
	 
	 var filters = new Array();
	 filters[0] = new nlobjSearchFilter('title', null, 'is', project_task_name);
	 filters[1] = new nlobjSearchFilter('company', null, 'is', project);
	 
	 var columns = new Array();
	 columns[0] = new nlobjSearchColumn('company');
		
	 var results = nlapiSearchRecord('projecttask', null, filters, columns);
	 
	// nlapiLogExecution('Debug', 'results', results.length);
	 if(results){
		 if(results.length>0){
			 var err = "This Task already exists";
			 throw err;
		 }
	 	}
	 }
}
}