/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       05 Apr 2019     Aazamali Khan	   Project View 
 *
 */

/**
 * @param {Object} dataIn Parameter object
 * @returns {Object} Output object
 */
var projectArray = [];
var projectDetails = [];
var opportunityDetailsArray = [];
var salesOrdersArray = [];

function postRESTlet(dataIn) {
    try {
        var projectId = dataIn.project;
        // search the data into system to get the project details 
        getProjectDetails(projectId);
        getOpportunitySearchResult(projectId);
        getSalesOrders(projectId);
        return {
            project: projectDetails,
            salesorders: salesOrdersArray,
            opportunityDetails:opportunityDetailsArray
        };
    } catch (e) {
        return {
            "code": "error",
            "message": e
        }
    }
}

function getProjectDetails(projectId) {
    var jobSearch = nlapiSearchRecord("job", null,
        [
            ["internalidnumber", "equalto", projectId]
        ],
        [
            new nlobjSearchColumn("entityid").setSort(false),
            new nlobjSearchColumn("altname"),
            new nlobjSearchColumn("jobbillingtype"),
            new nlobjSearchColumn("email"),
            new nlobjSearchColumn("phone"),
            new nlobjSearchColumn("altphone"),
            new nlobjSearchColumn("fax"),
            new nlobjSearchColumn("customer"),
            new nlobjSearchColumn("entitystatus"),
            new nlobjSearchColumn("contact"),
            new nlobjSearchColumn("jobtype"),
            new nlobjSearchColumn("startdate"),
            new nlobjSearchColumn("enddate"),
            new nlobjSearchColumn("projectedenddate"),
            new nlobjSearchColumn("altemail"),
            new nlobjSearchColumn("custentity_clientpartner"),
            new nlobjSearchColumn("custentity_entitygeo"),
            new nlobjSearchColumn("custentity_vertical"),
            new nlobjSearchColumn("custentity_verticalhead"),
            new nlobjSearchColumn("custentity_customerid"),
            new nlobjSearchColumn("custentity_sowdate"),
            new nlobjSearchColumn("custentity_projectmanager"),
            new nlobjSearchColumn("custentity_location"),
            new nlobjSearchColumn("custentity_executingpractice"),
            new nlobjSearchColumn("custentity_deliverymodel"),
            new nlobjSearchColumn("custentity_customersubcategory"),
            new nlobjSearchColumn("custentity_deliverymanager"),
            new nlobjSearchColumn("custentity_customerlob"),
            new nlobjSearchColumn("custentity_projectclassification"),
            new nlobjSearchColumn("custentity_practice"),
            new nlobjSearchColumn("custentity_projectkeywords"),
            new nlobjSearchColumn("custentity_serviceline"),
            new nlobjSearchColumn("custentity_subserviceline"),
            new nlobjSearchColumn("custentity_hoursperday"),
            new nlobjSearchColumn("custentity_hoursperweek"),
            new nlobjSearchColumn("custentity_otapplicable"),
            new nlobjSearchColumn("custentity3"),
            new nlobjSearchColumn("custentity_sunday"),
            new nlobjSearchColumn("custentity_monday"),
            new nlobjSearchColumn("custentity_tuesday"),
            new nlobjSearchColumn("custentity_wednesday"),
            new nlobjSearchColumn("custentity_thursday"),
            new nlobjSearchColumn("custentity_fridaycustomerform"),
            new nlobjSearchColumn("custentity_saturday"),
            new nlobjSearchColumn("custentity_billstartdate"),
            new nlobjSearchColumn("custentity_billenddate"),
            new nlobjSearchColumn("custentity_startdate"),
            new nlobjSearchColumn("custentity_enddate"),
            new nlobjSearchColumn("custentity_worklocation"),
            new nlobjSearchColumn("custentity_oldrefnumber"),
            new nlobjSearchColumn("custentity_projectvalue"),
            new nlobjSearchColumn("custentity_endcustomer"),
            new nlobjSearchColumn("custentity_applydiscount"),
            new nlobjSearchColumn("custentity_cummulativehours"),
            new nlobjSearchColumn("custentity_projectcity"),
            new nlobjSearchColumn("custentity_region"),
            new nlobjSearchColumn("custentity_billable_expenses_to_customer"),
            new nlobjSearchColumn("custentity_sold_margin_percent"),
            new nlobjSearchColumn("custentity_margin_approval_status"),
            new nlobjSearchColumn("custentity_pro_closing_comments"),
            new nlobjSearchColumn("custentity_resource_revenue_cost"),
            new nlobjSearchColumn("custentity_project_id_update_sfdc"),
            new nlobjSearchColumn("custentity_exclude_rev_forecast"),
            new nlobjSearchColumn("custentity_project_head_count")
        ]
    );
    if (jobSearch) {
        for (var int = 0; int < jobSearch.length; int++) {
            var jsonObj = {};
            jsonObj.projectname = jobSearch[int].getValue("altname");
            jsonObj.accountname = jobSearch[int].getText("customer");
            jsonObj.practice = jobSearch[int].getText("custentity_practice");
            jsonObj.location = jobSearch[int].getText("custentity_location");
            jsonObj.startdate = jobSearch[int].getValue("startdate");
            jsonObj.enddate = jobSearch[int].getValue("enddate");
            jsonObj.billingtype = jobSearch[int].getValue("jobbillingtype"); //"FP";//jobSearch[int].getValue("enddate");
            jsonObj.status = "Booked";
            jsonObj.projectvalue = jobSearch[int].getValue("custentity_projectvalue");
            jsonObj.projectmanager = jobSearch[int].getText("custentity_projectmanager");
            jsonObj.deliverymanager = jobSearch[int].getText("custentity_deliverymanager");
            jsonObj.clientpartner = jobSearch[int].getText("custentity_clientpartner");
            jsonObj.projectcp = jobSearch[int].getText("custentity_clientpartner");
            projectDetails.push(jsonObj);
        }
    }
    //return jobSearch;
}

function getOpportunitySearchResult(projectId) {
    var searchResult = nlapiSearchRecord("customrecord_sfdc_opportunity_record", null,
        [
            ["custrecord_project_internal_id_sfdc", "anyof", projectId]/*,
            "AND",
            ["custrecord_project_internal_id_sfdc.mainline", "is", "T"]*/
        ],
        [
            new nlobjSearchColumn("custrecord_opportunity_id_sfdc"),
			new nlobjSearchColumn("custrecord_customer_internal_id_sfdc"),
            new nlobjSearchColumn("custrecord_opportunity_name_sfdc"),
            new nlobjSearchColumn("custrecord_project_sfdc"),
            new nlobjSearchColumn("custrecord_customer_sfdc"),
            new nlobjSearchColumn("custrecord_start_date_sfdc"),
            new nlobjSearchColumn("custrecord_end_date_sfdc"),
            new nlobjSearchColumn("custrecord_practice_internal_id_sfdc"),
            new nlobjSearchColumn("custrecord_location_sfdc"),
            new nlobjSearchColumn("custrecord_sales_act_sfd"),
            new nlobjSearchColumn("custrecord_account_type_sfdc"),
            new nlobjSearchColumn("custrecord_opp_account_region"),
            new nlobjSearchColumn("custrecord_opp_service_line"),
            new nlobjSearchColumn("custrecord_account_name_sfdc"),
            new nlobjSearchColumn("custrecord_opp_fulfilment_status_sfdc"),
            new nlobjSearchColumn("custrecord_status_sfdc"),
			new nlobjSearchColumn("custrecord_tcv_sfdc"),
            new nlobjSearchColumn("custrecord_fulfill_plan_position", "CUSTRECORD_FULFILL_PLAN_OPP_ID", null)
        ]
    );
    if (searchResult) {
		nlapiLogExecution("DEBUG","searchResult length",searchResult.length);
		nlapiLogExecution("DEBUG","searchResult length",JSON.stringify(searchResult));
        for (var i = 0; i < searchResult.length; i++) {
            var jsonObj = {};
            jsonObj.opportunityname = searchResult[i].getValue("custrecord_opportunity_name_sfdc");
			jsonObj.opportunityId = searchResult[i].getId();
			jsonObj.accountname = searchResult[i].getText("custrecord_customer_internal_id_sfdc");
			jsonObj.practice = searchResult[i].getText("custrecord_practice_internal_id_sfdc");
			jsonObj.tcv = searchResult[i].getValue("custrecord_tcv_sfdc");
			opportunityDetailsArray.push(jsonObj);
        }
    }
}

function getSalesOrders(projectId) {
    var salesorderSearch = nlapiSearchRecord("salesorder", null,
        [
            ["type", "anyof", "SalesOrd"],
            "AND",
            ["mainline", "is", "T"], "AND",
            ["jobmain.internalidnumber", "equalto", projectId]
        ],
        [
            new nlobjSearchColumn("mainline"),
            new nlobjSearchColumn("trandate").setSort(false),
            new nlobjSearchColumn("asofdate"),
            new nlobjSearchColumn("postingperiod"),
            new nlobjSearchColumn("taxperiod"),
            new nlobjSearchColumn("type"),
            new nlobjSearchColumn("tranid"),
            new nlobjSearchColumn("entity"),
            new nlobjSearchColumn("account"),
            new nlobjSearchColumn("memo"),
            new nlobjSearchColumn("amount"),
            new nlobjSearchColumn("statusref"),
            new nlobjSearchColumn("datecreated"),
            new nlobjSearchColumn("startdate"),
            new nlobjSearchColumn("enddate"),
            new nlobjSearchColumn("custbody_vendorinvoicedate"),
            new nlobjSearchColumn("custbody_dcrefnumber"),
            new nlobjSearchColumn("custbody_dcdate"),
            new nlobjSearchColumn("custbody_invoicenumber"),
            new nlobjSearchColumn("custbody_billinglocation"),
            new nlobjSearchColumn("custbody_worklocation"),
            new nlobjSearchColumn("custbody_accountmanager")
        ]
    );
    nlapiLogExecution("DEBUG", "salesordersearch : ", JSON.stringify(salesorderSearch));
    if (salesorderSearch) {
        for (var int = 0; int < salesorderSearch.length; int++) {
            var jsonObject = {};
            jsonObject.salesorder = salesorderSearch[int].getValue("tranid");
            jsonObject.salesordervalue = salesorderSearch[int].getValue("amount");
            jsonObject.salesorderstatus = salesorderSearch[int].getValue("statusref");
            jsonObject.createddate = salesorderSearch[int].getValue("datecreated");
            jsonObject.startdate = salesorderSearch[int].getValue("startdate");
            jsonObject.enddate = salesorderSearch[int].getValue("enddate");
            salesOrdersArray.push(jsonObject);
        }
    }
}