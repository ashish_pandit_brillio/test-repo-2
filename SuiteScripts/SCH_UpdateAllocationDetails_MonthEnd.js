/**
 * @author Jayesh
 */

function update_allocation()
{
	try
	{
		var i_context = nlapiGetContext();
		
		var a_project_filter = [['status', 'noneof', 1], 'and',
								['jobtype', 'anyof', 2], 'and',
								['jobbillingtype', 'anyof', 'FBM'], 'and',
								['custentity_fp_rev_rec_type', 'noneof', '@NONE@']];
								
		var a_project_search_results = searchRecord('job', null, a_project_filter);
		if(a_project_search_results)
		{
			for(var i_proj_index=0; i_proj_index<a_project_search_results.length; i_proj_index++)
			{
				var i_usage_end = i_context.getRemainingUsage();
				if (i_usage_end < 1000)
				{
					nlapiYieldScript();
				}
				
				var i_project_id = a_project_search_results[i_proj_index].getId();
            	//var i_project_id = 53098;
				var a_revenue_cap_filter = [['custrecord_revenue_share_project', 'anyof', i_project_id]];
				var a_columns_existing_cap_srch = new Array();
				a_columns_existing_cap_srch[0] = new nlobjSearchColumn('created').setSort(true);
				
				var a_get_exsiting_revenue_cap = nlapiSearchRecord('customrecord_revenue_share', null, a_revenue_cap_filter, a_columns_existing_cap_srch);
				if(a_get_exsiting_revenue_cap)
				{
					var i_revenue_share_id = a_get_exsiting_revenue_cap[0].getId();
					nlapiLogExecution('audit','existing revenue share id:- '+i_revenue_share_id);
					var a_effort_activity_mnth_end_filter = [['custrecord_revenue_share_parent', 'anyof', parseInt(i_revenue_share_id)]];
					
					var a_columns_mnth_end_effort_activity_srch = new Array();
					a_columns_mnth_end_effort_activity_srch[0] = new nlobjSearchColumn('created').setSort(true);
					
					var a_get_mnth_end_effrt_activity = nlapiSearchRecord('customrecord_fp_revrec_month_end_process', null, a_effort_activity_mnth_end_filter, a_columns_mnth_end_effort_activity_srch);
					if (a_get_mnth_end_effrt_activity)
					{
						var i_mnth_end_effrt_activity_parent = a_get_mnth_end_effrt_activity[0].getId();
						
						var a_effort_plan_mnth_end_filter = [['custrecord_month_end_parent', 'anyof', parseInt(i_mnth_end_effrt_activity_parent)]];
										
						var a_columns_mnth_end_effort_plan_srch = new Array();
						a_columns_mnth_end_effort_plan_srch[0] = new nlobjSearchColumn('custrecord_month_end_sub_practice');
							
						var a_get_mnth_end_effrt_plan_prac = searchRecord('customrecord_fp_revrec_month_end_effort', null, a_effort_plan_mnth_end_filter, a_columns_mnth_end_effort_plan_srch);
						if (a_get_mnth_end_effrt_plan_prac)
						{
							nlapiLogExecution('audit','effrt length(CUSTOM TABLE):- '+a_get_mnth_end_effrt_plan_prac.length);
							for(var i=0; i<a_get_mnth_end_effrt_plan_prac.length; i++)
							{
								var i_usage_end = i_context.getRemainingUsage();
								if (i_usage_end < 1000)
								{
									nlapiYieldScript();
								}
								var rcrd = nlapiLoadRecord('customrecord_fp_revrec_month_end_effort',a_get_mnth_end_effrt_plan_prac[i].getId());
								nlapiSubmitRecord(rcrd);
							}
						}
					}
				}
			}
		}
	}
	catch(err)
	{
		nlapiLogExecution('ERROR','ERROR MESSAGE:- '+err);
	}
}

function searchRecord(recordType, savedSearch, arrFilters, arrColumns,
        filterExpression)
{

	try {
		var search = null;

		// if a saved search is provided, load it and add the filters and
		// columns
		if (isNotEmpty(savedSearch)) {
			search = nlapiLoadSearch(recordType, savedSearch);

			if (isArrayNotEmpty(arrFilters)) {
				search.addFilters(arrFilters);
			}

			if (isArrayNotEmpty(arrColumns)) {
				search.addColumns(arrColumns);
			}

			if (isArrayNotEmpty(filterExpression)) {
				search.setFilterExpression(filterExpression);
			}
		}
		// create a new search
		else {
			search = nlapiCreateSearch(recordType, arrFilters, arrColumns);
		}

		// run search
		var resultSet = search.runSearch();

		// iterate through the search and get all data 1000 at a time
		var searchResultCount = 0;
		var resultSlice = null;
		var searchResult = [];

		do {
			resultSlice = resultSet.getResults(searchResultCount,
			        searchResultCount + 1000);

			if (resultSlice) {

				resultSlice.forEach(function(result) {

					searchResult.push(result);
					searchResultCount++;
				});
			}
		} while (isArrayNotEmpty(resultSlice) && resultSlice.length >= 1000);

		return searchResult;
	} catch (err) {
		nlapiLogExecution('ERROR', 'searchRecord', err);
		throw err;
	}
}

function isEmpty(value) {

	return value == null || value == "" || typeof (value) == undefined;
}

function isNotEmpty(value) {

	return !isEmpty(value);
}

function isArrayEmpty(argArray) {

	return !isArrayNotEmpty(argArray);
}

function isArrayNotEmpty(argArray) {

	return (isNotEmpty(argArray) && argArray.length > 0);
}