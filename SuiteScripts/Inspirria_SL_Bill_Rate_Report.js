/**
 * @NApiVersion 2.x
 * @NScriptType Suitelet
 */

var PAGE_SIZE = 1000;
var SEARCH_ID = 'customsearch2846';
var CLIENT_SCRIPT_FILE_ID = "";

define(['N/ui/serverWidget', 'N/search', 'N/redirect', 'N/file', 'N/encode','N/task','N/format'],
		function (serverWidget, search, redirect, file, encode,task,format) {
	function onRequest(context) {
		if (context.request.method == 'GET') {
			/*	var form = serverWidget.createForm({
				title: 'Bill Rate Report',
				hideNavBar: false
			});*/

			//	form.clientScriptFileId = CLIENT_SCRIPT_FILE_ID;

			var json = "";
			json = runSearch(SEARCH_ID);

			log.debug('allocation_Data',JSON.stringify(json));
			//	context.response.writePage(form);
			var glob_array = [];
			for (var key in json) {
				if(json[key].length>1)
				{
					for (var r=0;r<json[key].length;r++)
					{  
						var ss= json[key].length-1;
						var tempFrom=""; var toDate="";
						if(r == ss)
						{
							tempFrom = json[key][r].systemNotes_Date;
							toDate = json[key][r].enddate;
							json[key][r]["from"] = tempFrom;
							json[key][r]["to"] = toDate;
							glob_array.push(json[key][r]);
						}else if(r == 0){
							tempFrom = json[key][r].startdate;
							if(json[key][r+1].systemNotes_Date == json[key][r].systemNotes_Date)
							{
								toDate = json[key][r].systemNotes_Date;
							}
							else
							{
								toDate = calculateToDate(json[key][r+1].systemNotes_Date);
							}

							json[key][r]["from"] = tempFrom;
							json[key][r]["to"] = toDate;
							glob_array.push(json[key][r]);  
						} else{
							tempFrom = json[key][r].systemNotes_Date;
							if(json[key][r+1].systemNotes_Date == json[key][r].systemNotes_Date)
							{
								toDate = json[key][r].systemNotes_Date;
							}
							else
							{
								toDate = calculateToDate(json[key][r+1].systemNotes_Date);
							}
							//toDate = calculateToDate(json[key][r+1].systemNotes_Date);
							json[key][r]["from"] = tempFrom;
							json[key][r]["to"] = toDate;
							glob_array.push(json[key][r]);  
						}

					}
				}/*else
				{
					for (var r=0;r<json[key].length;r++)
					{  
						var tempFrom=""; var toDate="";
						tempFrom = json[key][r].startdate;
						toDate = json[key][r].enddate;
						json[key][r]["from"] = tempFrom;
						json[key][r]["to"] = toDate;
						glob_array.push(json[key][r]);  
					}
				}*/
			}

			var hardHeaders = ["ALLOCATION ID","EMPLOYEE","RATE","FROM","TO","PROJECT","ALLOCATION START DATE","ALLOCATION END DATE"];
			var CSVData = '<?xml version="1.0"?><?mso-application progid="Excel.Sheet"?>';
			CSVData += '<Workbook xmlns="urn:schemas-microsoft-com:office:spreadsheet" ';
			CSVData += 'xmlns:o="urn:schemas-microsoft-com:office:office" ';
			CSVData += 'xmlns:x="urn:schemas-microsoft-com:office:excel" ';
			CSVData += 'xmlns:ss="urn:schemas-microsoft-com:office:spreadsheet" ';
			CSVData += 'xmlns:html="http://www.w3.org/TR/REC-html40">';

			CSVData += '<Styles>';

			CSVData += '<Style ss:ID="s1">';
			CSVData += '<Font ss:Bold="1" ss:Underline="Single"/>';
			CSVData += '<Interior ss:Color="#CCFFFF" ss:Pattern="Solid"/>';
			CSVData += ' <Borders>';
			CSVData += ' <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>';
			CSVData += '</Borders>';
			CSVData += '</Style>';

			CSVData += '</Styles>';

			CSVData += '<Worksheet ss:Name="Report">';
			CSVData += '<Table><Row>';
			for (var k = 0; k < hardHeaders.length; k++) {
				CSVData += '<Cell ss:StyleID="s1"><Data ss:Type="String">' + hardHeaders[k] + '</Data></Cell>';
			}
			CSVData += '</Row>';
			for (var i = 0; i < glob_array.length; i++) {

				CSVData += '<Row>';
				CSVData += '<Cell><Data ss:Type="String">' + glob_array[i].id + '</Data></Cell>';
				CSVData += '<Cell><Data ss:Type="String">' + glob_array[i].resource_text + '</Data></Cell>';
				CSVData += '<Cell><Data ss:Type="String">' + glob_array[i].systemNotes_NewRate + '</Data></Cell>';
				CSVData += '<Cell><Data ss:Type="String">' + glob_array[i].from + '</Data></Cell>';
				CSVData += '<Cell><Data ss:Type="String">' + glob_array[i].to + '</Data></Cell>';
				CSVData += '<Cell><Data ss:Type="String">' + glob_array[i].Project_text + '</Data></Cell>';
				CSVData += '<Cell><Data ss:Type="String">' + glob_array[i].startdate + '</Data></Cell>';
				CSVData += '<Cell><Data ss:Type="String">' + glob_array[i].enddate + '</Data></Cell>';
				CSVData += '</Row>';
			}

			CSVData += '</Table></Worksheet></Workbook>';
			var strXmlEncoded = encode.convert({
				string: CSVData,
				inputEncoding: encode.Encoding.UTF_8,
				outputEncoding: encode.Encoding.BASE_64
			});

			var objXlsFile = file.create({
				name: 'Bill Rate Change Report.xls',
				fileType: file.Type.EXCEL,
				contents: strXmlEncoded
			});
			// Optional: you can choose to save it to file cabinet
			// objXlsFile.folder = -14;
			// var intFileId = objXlsFile.save();

			context.response.writeFile({
				file: objXlsFile
			});
		}
	}

	return {
		onRequest: onRequest
	};
	function calculateToDate(date)
	{
		var d = new Date(date);
		var rd  = new Date(d.setDate(d.getDate() - 1));

		var teml_Date = format.parse({
			value: rd,
			type: format.Type.DATE
		});

		var _Date = format.format({
			value: teml_Date,
			type: format.Type.DATE
		});
		return _Date;
	}
	function runSearch(searchId) {
		var searchObj = search.load({
			id: searchId
		});

		var results = [];
		var count = 0;
		var pageSize = 1000;
		var start = 0;
		do {
			var subresults = searchObj.run().getRange({
				start: start,
				end: start + pageSize
			});

			results = results.concat(subresults);
			count = subresults.length;
			start += pageSize;
		} while (count == pageSize);
		log.debug('resultsresultsresults', results.length);

		var resultsofData = new Array();
		for (var res = 0; res < results.length; res++) {

			var internalId = results[res].getValue({name: "id", label: "ID"});
			var Project_text = results[res].getText({name: "company", label: "Project"});
			var resource_text = results[res].getText({
				name: "resource",
				sort: search.Sort.ASC,
				label: "Resource"
			});
			var systemNotes_Date = results[res].getValue({
				name: "date",
				join: "systemNotes",
				sort: search.Sort.ASC,
				label: "Date"
			});
			var date = new Date(systemNotes_Date);
			var teml_Date = format.parse({
				value: date,
				type: format.Type.DATE
			});

			systemNotes_Date = format.format({
				value: teml_Date,
				type: format.Type.DATE
			});

			var startdate = results[res].getValue({name: "startdate", label: "Start Date"});
			var enddate =  results[res].getValue({name: "enddate", label: "End Date"});

			var systemNotes_OldRate =  results[res].getValue({
				name: "oldvalue",
				join: "systemNotes",
				label: "Old Rate"
			});
			var systemNotes_NewRate = results[res].getValue({
				name: "newvalue",
				join: "systemNotes",
				label: "New Rate"
			});

			resultsofData.push({
				"id": internalId,
				"resource_text":resource_text,
				"Project_text":Project_text,
				"startdate": startdate,
				"enddate": enddate,
				"systemNotes_Date": systemNotes_Date,
				"systemNotes_OldRate": systemNotes_OldRate,
				"systemNotes_NewRate": systemNotes_NewRate
			});
		}
		log.debug('All resultsofData is :', resultsofData.length);
		var groupResult_resource = resultsofData.reduce(function (r, a) {
			r[a.id] = r[a.id] || [];
			r[a.id].push(a);
			return r;
		}, Object.create(null));

		return groupResult_resource;

	}
});