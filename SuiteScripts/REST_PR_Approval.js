/**
 * RESTlet to get expenses for approval
 * 
 * Version    Date            Author           Remarks
 * 1.00       23 Nov 2015     nitish.mishra
 *
 */

/**
 * @param {Object}
 *            dataIn Parameter object
 * @returns {Object} Output object
 */
function postRESTlet(dataIn) {
	var response = new Response();
	try {
		var s_remarks = '';
		nlapiLogExecution('debug', 'dataIn', JSON.stringify(dataIn));
		var employeeId = getUserUsingEmailId(dataIn.EmailId);
		var requestType = dataIn.RequestType;
		
		var i_PrId = dataIn.Data ? dataIn.Data.PR_ID : ''; 
		if(requestType == 'REJECT'){
		s_remarks = dataIn.Data ? dataIn.Data.Remarks : '';
		nlapiLogExecution('debug', 'remarks', s_remarks);
		} 
		//nlapiLogExecution('debug', 'remarks', remarks);
		//Test
		//var employeeId = getUserUsingEmailId('maneesh.agarwal@brillio.com');
		//var requestType = 'APPROVE';
	//	var i_PrId = '390801';
	//	var i_PrId = '';
		//var s_remarks = 'Test';
		var List = [i_PrId];
		switch (requestType) {

			case M_Constants.Request.Get:

				if (i_PrId) {
					response.Data = getPRDetail(i_PrId, employeeId);
					response.Status = true;
				} else {
					response.Data = getPRPendingApproval(employeeId);
					response.Status = true;
				}
			break;

			case M_Constants.Request.Approve:
				response.Data = approve_PRs(List, //dataIn.Data.PR_ID,
				        employeeId);
				response.Status = true;
			break;

			case M_Constants.Request.Reject:
				response.Data = reject_PRs(dataIn.Data.PR_ID,s_remarks, //dataIn.Data.Remarks,
				        employeeId);
				response.Status = true;
			break;
		}

	} catch (err) {
		nlapiLogExecution('DEBUG', 'postRESTlet', err);
		response.Data = err;
	}
	return response;
}

function getPRDetail(i_PrId, firstLevelApprover) {
	try {
		var Pr_Details = [];
		if(_logValidation(i_PrId)){
			
			var a_load_PR_Rec = nlapiLoadRecord('customrecord_pritem',parseInt(i_PrId));
			
			Pr_Details.push({
				'PR_ID': a_load_PR_Rec.getId(),
				'PR_Number': a_load_PR_Rec.getFieldValue('custrecord_prno1'),
				'Employee': a_load_PR_Rec.getFieldText('custrecord_employee'),
				'Item': a_load_PR_Rec.getFieldText('custrecord_prmatgrpcategory'),
				'Description': a_load_PR_Rec.getFieldValue('custrecord_prmatldescription'),
				'Quantity': a_load_PR_Rec.getFieldValue('custrecord_quantity'),
				'TotalValue': a_load_PR_Rec.getFieldValue('custrecord_totalvalue'),
				'Project': a_load_PR_Rec.getFieldText('custrecord_project'),
				'Date': a_load_PR_Rec.getFieldValue('created'),
				'Currency': a_load_PR_Rec.getFieldText('custrecord_currencyforpr')  
			});
			
		}
		return Pr_Details;
	} catch (err) {
		nlapiLogExecution('error', 'getExpenseDetail', err);
		throw err;
	}
}

function getPRPendingApproval(firstLevelApprover) {
	try {
		var cols  = [];
		cols.push(new nlobjSearchColumn('custrecord_prno1'));
		cols.push(new nlobjSearchColumn('custrecord_purchaserequest'));
		cols.push(new nlobjSearchColumn('custrecord_employee'));
		cols.push(new nlobjSearchColumn('custrecord_prmatgrpcategory'));
		cols.push(new nlobjSearchColumn('custrecord_prmatldescription'));
		cols.push(new nlobjSearchColumn('custrecord_quantity'));
		cols.push(new nlobjSearchColumn('custrecord_totalvalue'));
		cols.push(new nlobjSearchColumn('custrecord_project'));
		cols.push(new nlobjSearchColumn('internalid')); //created
		cols.push(new nlobjSearchColumn('created'));
		cols.push(new nlobjSearchColumn('custrecord_currencyforpr'));
		var pending_PR_Search = nlapiSearchRecord('customrecord_pritem',null,
				[
				   [['custrecord_subpracticehead','anyof',firstLevelApprover], 
				   'AND', 
				   ['custrecord_prastatus','anyof',2]],
				   'OR', 
				  [['custrecord_pri_delivery_manager','anyof',firstLevelApprover], 
				   'AND', 
				   ['custrecord_prastatus','anyof',30]]
				], 
				cols
				);

		var prList = [];
		var time_Stamp = timestamp ();

		if (pending_PR_Search) {
			for (var i = 0; i < pending_PR_Search.length && i < 50; i++) {
				//Fetching project Info
				prList.push({
					'PR_ID': pending_PR_Search[i].getValue('internalid'),
					'PR_Number': pending_PR_Search[i].getValue('custrecord_prno1'),
					'Employee': pending_PR_Search[i].getText('custrecord_employee'),
					'Item': pending_PR_Search[i].getText('custrecord_prmatgrpcategory'),
					'Description': pending_PR_Search[i].getValue('custrecord_prmatldescription'),
					'Quantity': pending_PR_Search[i].getValue('custrecord_quantity'),
					'TotalValue': pending_PR_Search[i].getValue('custrecord_totalvalue'),
					'Project': pending_PR_Search[i].getText('custrecord_project'),
					'Date': pending_PR_Search[i].getValue('created'),
					'Currency': pending_PR_Search[i].getText('custrecord_currencyforpr')  
					
				});
				
			}
		}
		nlapiLogExecution('debug', 'dataIn', JSON.stringify(prList));
		return prList;
		
	} catch (err) {
		nlapiLogExecution('error', 'getExpensesPendingApproval', err);
		throw err;
	}
}

function approve_PRs(expenseList, firstApproverId) {
	try {

		if (expenseList) {
			var noOfApprovedExpenses = 0;

			for (var i = 0; i < expenseList.length; i++) {
				//var a_prRecord = nlapiLoadRecord('customrecord_pritem',
				//        parseInt(expenseList[i]));
				nlapiLogExecution('DEBUG','Approve ID',expenseList[i]);
				var pr_lookup = nlapiLookupField('customrecord_pritem',parseInt(expenseList[i]),['custrecord_project','custrecord_prastatus','custrecord_pritem_job_type']);
				var id_new = -1;
				// check access
				var projectVal = pr_lookup.custrecord_project;
				var status_ = pr_lookup.custrecord_prastatus;
				var projectLookUp_jobtype = pr_lookup.custrecord_pritem_job_type;
				//var itemCategory = pr_lookup.custrecord_itemcategory;
				
				if(parseInt(projectLookUp_jobtype) == parseInt(2)){
					var fildID = 'custrecord_pri_delivery_manager';
					var checkBox_ID  = 'custrecord_approved_by_dm';
				}
				else{
					var fildID = 'custrecord_subpracticehead';
					var checkBox_ID = 'custrecord_approve_subpractice';
				}
					var pr_approver = nlapiLookupField('customrecord_pritem',parseInt(expenseList[i]),fildID);
					if (parseInt(pr_approver) == parseInt(firstApproverId)) {

					//Intiate Workflow
						var instance_id = nlapiInitiateWorkflow('customrecord_pritem',parseInt(expenseList[i]),12);
						
					// check the status of the expense
					if (parseInt(status_)== parseInt(2) || parseInt(status_)== parseInt(1)|| parseInt(status_)== parseInt(30)) {
						
						if(parseInt(projectLookUp_jobtype) == parseInt(2)){
							var id = nlapiTriggerWorkflow('customrecord_pritem',parseInt(expenseList[i]),'customworkflow_prworkflow','workflowaction417','workflowstate181');
							
						}
						else{
							var id = nlapiTriggerWorkflow('customrecord_pritem',parseInt(expenseList[i]),'customworkflow_prworkflow','workflowaction90','workflowstate33');
							
						}
						nlapiLogExecution('DEBUG','Processed ID',id);
						//WorkAround if not appproved at first instance
						while (parseFloat(id) < parseFloat(0) && parseFloat(id_new) < parseFloat(0)){
							
							//Load the record
							var id_ = nlapiLoadRecord('customrecord_pritem',parseInt(expenseList[i]));
							var i_category = id_.getFieldValue('custrecord_itemcategory');
							nlapiLogExecution('DEBUG','i_category ID',i_category);
							if(!_logValidation(i_category)){
							id_.setFieldValue('custrecord_itemcategory',i_category);
							}
							var id_1 = nlapiSubmitRecord(id_,true,true);
							
							var pr_lookup = nlapiLookupField('customrecord_pritem',parseInt(expenseList[i]),['custrecord_project','custrecord_prastatus','custrecord_pritem_job_type']);
							var id_new = -1;
							// check access
							var projectVal = pr_lookup.custrecord_project;
							var status_ = pr_lookup.custrecord_prastatus;
							var projectLookUp_jobtype = pr_lookup.custrecord_pritem_job_type;
							//var itemCategory = pr_lookup.custrecord_itemcategory;
							
							if(parseInt(projectLookUp_jobtype) == parseInt(2)){
								var fildID = 'custrecord_pri_delivery_manager';
								var checkBox_ID  = 'custrecord_approved_by_dm';
							}
							else{
								var fildID = 'custrecord_subpracticehead';
								var checkBox_ID = 'custrecord_approve_subpractice';
							}
							var pr_approver = nlapiLookupField('customrecord_pritem',parseInt(expenseList[i]),fildID);
							//Approve the PR
							if(parseInt(projectLookUp_jobtype) == parseInt(2)){
							 id_new = nlapiTriggerWorkflow('customrecord_pritem',parseInt(expenseList[i]),'customworkflow_prworkflow','workflowaction417','workflowstate181');
								
							}
							else{
							 id_new =nlapiTriggerWorkflow('customrecord_pritem',parseInt(expenseList[i]),'customworkflow_prworkflow','workflowaction90','workflowstate33');
								
							}
							nlapiLogExecution('DEBUG','Processed ID - Not Processed at 1st Instance',id_new);
						}
						
						try {
						if(parseInt(id_new)!= -1 || parseInt(id)!= -1){
							
						nlapiSubmitField('customrecord_pritem',parseInt(expenseList[i]),'custrecord_pr_approver_name',firstApproverId);
						//	if(itemCategory == 'IT'){
						//		 a_prRecord.setFieldValue('custrecord_prastatus',3);
						//}
						//		else{
						//			a_prRecord.setFieldValue('custrecord_prastatus',4);	
						//		}
							//a_prRecord.setFieldText('custrecord_itemcategory',itemCategory);
							//var i_PrId = nlapiSubmitRecord(a_prRecord,true,true);
							createLog('customrecord_pritem', expenseList[i], 'Update',
							        firstApproverId, 'Approved');
							noOfApprovedExpenses += 1;
						}
						else{
							return 'Approval is Failed!!';
						}
						} catch (e) {
							nlapiLogExecution('error',
							        'failed to approve pr : '
							                + expenseList[i], e);

							if (expenseList.length == 1) {
								throw e;
							}
						}
						
					} else if (expenseList.length == 1) {
						throw "You cannot approve this PR.";
					}
				} else if (expenseList.length == 1) {
					throw "You are not authorized to access this record.";
				}
				
				
			}

			// check no. of expenses approved
			if (noOfApprovedExpenses == expenseList.length) {
				return "PR Approved Succesfully";
			} else {
				return "Done. Some approvals failed.";
			}
		} else {
			throw "No PR to approve.";
		}
	} catch (err) {
		nlapiLogExecution('ERROR', 'approvePRs', err);
		throw err;
	}
}

function reject_PRs(expenseList,remarks, firstApproverId) {
	try {
		var id_new = -1;
		if (expenseList) {
			var noOfApprovedExpenses = 0;

			for (var i = 0; i < expenseList.length; i++) {
				/*var expenseRecord = nlapiLoadRecord('expensereport',
				        expenseList[i], {
					        recordmode : 'dynamic'
				        });*/
				nlapiLogExecution('DEBUG','Reject ID',expenseList[i]);
				var pr_lookup = nlapiLookupField('customrecord_pritem',parseInt(expenseList[i]),['custrecord_project','custrecord_prastatus','custrecord_pritem_job_type']);
				
				// check access
				var projectVal = pr_lookup.custrecord_project;
				var status_ = pr_lookup.custrecord_prastatus;
				var projectLookUp_jobtype = pr_lookup.custrecord_pritem_job_type;
			//	var itemCategory = pr_lookup.custrecord_itemcategory;
				if(parseInt(projectLookUp_jobtype) == parseInt(2)){
					var fildID = 'custrecord_pri_delivery_manager';
					var checkBox_ID  = 'custrecord_rejected_by_dm';
				}
				else{
					var fildID = 'custrecord_subpracticehead';
					var checkBox_ID = 'custrecord_rejected_by_subpractice';
				}
				var pr_approver = nlapiLookupField('customrecord_pritem',parseInt(expenseList[i]),fildID);
				if (parseInt(status_)== parseInt(2) || parseInt(status_)== parseInt(1)|| parseInt(status_)== parseInt(30)) {
				if (parseInt(pr_approver) == parseInt(firstApproverId)) {	
					//Intiate Workflow
					var instance_id = nlapiInitiateWorkflow('customrecord_pritem',parseInt(expenseList[i]),12);
					
					if(parseInt(projectLookUp_jobtype) == parseInt(2)){
						
						var id =nlapiTriggerWorkflow('customrecord_pritem',parseInt(expenseList[i]),'customworkflow_prworkflow','workflowaction418','workflowstate181');
						  
					}
					else{
						var id =nlapiTriggerWorkflow('customrecord_pritem',parseInt(expenseList[i]),'customworkflow_prworkflow','workflowaction92','workflowstate33');
						
					}
					
					nlapiLogExecution('DEBUG','Processed ID',id);
					//WorkAround if not appproved at first instance
					while (parseFloat(id) < parseFloat(0) && parseFloat(id_new) < parseFloat(0)){
						
						//Load the record
						var id_ = nlapiLoadRecord('customrecord_pritem',parseInt(expenseList[i]));
						var i_category = id_.getFieldValue('custrecord_itemcategory');
						nlapiLogExecution('DEBUG','i_category ID',i_category);
						if(!_logValidation(i_category)){
						id_.setFieldValue('custrecord_itemcategory',i_category);
						}
						var id_1 = nlapiSubmitRecord(id_,true,true);
						
						var pr_lookup = nlapiLookupField('customrecord_pritem',parseInt(expenseList[i]),['custrecord_project','custrecord_prastatus','custrecord_pritem_job_type']);
						var id_new = -1;
						// check access
						var projectVal = pr_lookup.custrecord_project;
						var status_ = pr_lookup.custrecord_prastatus;
						var projectLookUp_jobtype = pr_lookup.custrecord_pritem_job_type;
						//var itemCategory = pr_lookup.custrecord_itemcategory;
						
						if(parseInt(projectLookUp_jobtype) == parseInt(2)){
							var fildID = 'custrecord_pri_delivery_manager';
							var checkBox_ID  = 'custrecord_approved_by_dm';
						}
						else{
							var fildID = 'custrecord_subpracticehead';
							var checkBox_ID = 'custrecord_approve_subpractice';
						}
						var pr_approver = nlapiLookupField('customrecord_pritem',parseInt(expenseList[i]),fildID);
						//Approve the PR
						if(parseInt(projectLookUp_jobtype) == parseInt(2)){
						 id_new = nlapiTriggerWorkflow('customrecord_pritem',parseInt(expenseList[i]),'customworkflow_prworkflow','workflowaction418','workflowstate181');
							
						}
						else{
						 id_new = nlapiTriggerWorkflow('customrecord_pritem',parseInt(expenseList[i]),'customworkflow_prworkflow','workflowaction92','workflowstate33');
							
						}
						nlapiLogExecution('DEBUG','Processed ID - Not Processed at 1st Instance',id_new);
					}
				//	a_prRecord.setFieldValue('custrecord_rejected', firstApproverId);
				//	a_prRecord.setFieldValue('custrecord_prastatus',6);	
				//	a_prRecord.setFieldText('custrecord_itemcategory',itemCategory);
					nlapiLogExecution('debug', 'Reject remarks', remarks);
					if(_logValidation(remarks)){
					nlapiSubmitField('customrecord_pritem',parseInt(expenseList[i]),'custrecord_rej_reason',remarks);
					//a_prRecord.setFieldValue('custrecord_subpracticeapproval', remarks);
					nlapiLogExecution('debug', 'Reject Set remarks', remarks);
					}
					try {
						//var i_PrId = nlapiSubmitRecord(a_prRecord,true,true);
						createLog('customrecord_pritem', expenseList[i], 'Update',
						        firstApproverId, 'Rejected');
						noOfApprovedExpenses += 1;
					} catch (e) {
						nlapiLogExecution('error',
						        'failed to reject pr : ' + expenseList[i],
						        e);

						if (expenseList.length == 1) {
							throw e;
						}
					}
				} else if (expenseList.length == 1) {
					throw "You are not authorized to access this record.";
				}
				}
			}

			// check no. of expenses approved
			if (noOfApprovedExpenses == expenseList.length) {
				return "PR Rejected Succesfully";
			} else {
				return "Done. Some rejections failed.";
			}
		} else {
			throw "No PR to reject.";
		}
	} catch (err) {
		nlapiLogExecution('ERROR', 'rejectPRs', err);
		throw err;
	}
}

function timestamp() {
var str = "";

var currentTime = new Date();
var hours = currentTime.getHours();
var minutes = currentTime.getMinutes();
var seconds = currentTime.getSeconds();
var meridian = "";
if (hours > 12) {
    meridian += "pm";
} else {
    meridian += "am";
}
if (hours > 12) {

    hours = hours - 12;
}
if (minutes < 10) {
    minutes = "0" + minutes;
}
if (seconds < 10) {
    seconds = "0" + seconds;
}
str += hours + ":" + minutes + ":" + seconds + " ";

return str + meridian;
}

function _logValidation(value) 
{
 if(value != null && value.toString() != null && value != '' && value != undefined && value.toString() != undefined && value != 'undefined' && value.toString() != 'undefined'&& value.toString() != 'NaN' && value != NaN) 
 {
  return true;
 }
 else 
 { 
  return false;
 }
}