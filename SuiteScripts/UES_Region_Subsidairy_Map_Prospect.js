//*** Map subsidiary based on region from SFDC **//

function assign_subsidiary(type){
	try{
		
	if(type == 'create' || type == 'edit'){	
		/************* Lookup Region *********************/
	var s_region = nlapiGetFieldValue('custentity_region_sfdc');
	if(s_region)
	{
		var customrecord_regionSearch = nlapiSearchRecord("customrecord_region",null,
		[
		   ["name","contains",regExReplacer(s_region)]
		], 
		[
		   new nlobjSearchColumn("internalid")
		]
		);
		if(customrecord_regionSearch)
		{
			nlapiSetFieldValue('custentity_region',customrecord_regionSearch[0].getValue('internalid'));
		}
	}
	var i_subsidairy = '';
	if(s_region == 'East' || s_region == 'West' || s_region == 'Central' || s_region == 'East'){
	i_subsidairy = 2;
	}
	else if(s_region == 'India'){
	i_subsidairy = 3;
	}
	else if(s_region == 'Europe' || s_region == 'UK'){
	i_subsidairy = 7;
	}
	else if(s_region == 'Canada'){
	i_subsidairy = 10;
	}
	nlapiSetFieldValue('subsidiary',i_subsidairy);
	}
	}
	catch(e){
		nlapiLogExecution('DEBUG','Assign Subsidiary Error',e);
		throw e;
	}
}

function regExReplacer(str)
{
	return str.replace(/\n|\r/g, "");
}