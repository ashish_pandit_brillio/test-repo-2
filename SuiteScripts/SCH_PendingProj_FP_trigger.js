function pendingMilestoneTrigger()
{
	try
	{
		var Sysdate = new Date()
		
		var filters = new Array();
		filters[0] = new nlobjSearchFilter("type",null,"anyof","FBM");
		filters[1] = new nlobjSearchFilter("isinactive",null,"is","F");
		
		var billingscheduleSearch = searchRecord('billingschedule', null, filters
		[
			new nlobjSearchColumn("id")
		]);

		if(billingscheduleSearch)
		{
			for(var count = 0 ; count < billingscheduleSearch.length; count++)
			{
				var id = billingscheduleSearch[count].getId();
				nlapiYieldScript();
				var loadRec = nlapiLoadRecord('billingschedule',id);
				var proj_id = loadRec.getFieldValue('project');
				if(_logValidation(proj_id))
				{
					var projectDetails = nlapiLookupField('job',proj_id,['entityid','altname','custentity_projectmanager','custentity_deliverymanager','entitystatus']);
					var projectStatus = projectDetails.entitystatus;
					if(projectStatus == 4)
					{
						var pm_email = nlapiLookupField('employee',projectDetails.custentity_projectmanager,['email','firstname']);
						var dm_email = nlapiLookupField('employee',projectDetails.custentity_deliverymanager,['email']);
					
						var lineNum = loadRec.getLineItemCount('milestone');
						for(var i = 1; i <= lineNum ; i++)
						{
							var milestoneDate = loadRec.getLineItemValue('milestone','milestonedate',i);
							var milestoneDesc = loadRec.getLineItemText('milestone','projecttask',i);
							var milestoneAmt = loadRec.getLineItemValue('milestone','comments',i);
							var milestoneCompleted = loadRec.getLineItemValue('milestone','milestonecompleted',i);
							var date = nlapiStringToDate(milestoneDate);
						
						
							if(milestoneCompleted != 'T')
							{
								if(date < Sysdate)
								{
									sendPendingReminderMails(
										pm_email.email,
										pm_email.firstname,
										dm_email.email,
										milestoneDate,
										milestoneDesc,
										milestoneAmt,
										projectDetails.entityid,
										projectDetails.altname
									);
								}
							}
						}
					}
				}
			}
		}
	}

	catch (e)
	{
		nlapiLogExecution('Debug','Error',e);
	}
}

function sendPendingReminderMails(pm_email, pm_name, dm_email, milestone_date, milestone_task,milestone_value,projID, projName)
{
	
	try{       
		var cc = [];
		//cc[0] = 'business.ops@brillio.com';
		cc[0] = dm_email;
		
		var mailTemplate = serviceTemplate(pm_email, pm_name, dm_email, milestone_date, milestone_task,milestone_value,projID, projName);
		nlapiLogExecution('debug', 'checkpoint',pm_email);
		nlapiLogExecution('debug', 'mailTemplate',mailTemplate.MailBody);
		nlapiSendEmail(176720, pm_email, mailTemplate.MailSubject, mailTemplate.MailBody,cc,'billing@brillio.com',null,null,null);
		
	}
	catch(err){
          nlapiLogExecution('error', 'sendServiceMails', err);
     }
}

function serviceTemplate(pm_email, pm_name, dm_email, milestone_date, milestone_task,milestone_value,projID, projName)
{
    var htmltext = '';
    
   htmltext += '<table border="0" width="100%"><tr>';
   htmltext += '<td colspan="4" valign="top">';
  
   htmltext += '<p>Hi  ' + pm_name + ',</p>';
   
   htmltext += '<p> We would like to inform you that below milestone is due for invoicing for project   '+projID + ' '+projName+' but project is in pending status due to missing document (such as missing signed SOW/PO) hence we are unable to raise invoice. If document are pending for more than 2 month then revenue recognition will be on hold. Pls expedite in getting document</p>';
 
   htmltext += '<p><b>Due Date :' + milestone_date + '</b></p>';
   htmltext += '<p><b>Milestone Description  :' + milestone_task + '</b></p>';
   htmltext += '<p><b>Amount :' + milestone_value + '</b></p>';
   
   htmltext += '<p>Thanks & Regards,</p>';
   htmltext += '<p>Brillio Billing Team</p>';
 
 
    htmltext += '</td></tr>';
    htmltext += '</table>';
    htmltext += '<hr width="100%" size="1" noshade color="#CCCCCC">';
    htmltext += '<table width="100%" border="0" cellspacing="0" cellpadding="0">';
    htmltext += '<tr>';
    htmltext += '<td align="right">';
    htmltext += '</td>';
    htmltext += '</tr>';
    htmltext += '</table>';
    htmltext += '</body>';
    htmltext += '</html>';
 
    return {
        MailBody : htmltext,
        MailSubject : "Reminder of milestone due for pending project "+projID + " " +projName + "- dated " +milestone_date
    };
}

function _logValidation(value) 
{
 if(value!='null' && value != null && value != null && value != '' && value != undefined && value != undefined && value != 'undefined' && value!= 'undefined'&& value!= 'NaN' && value != NaN) 
 {
  return true;
 }
 else 
 { 
  return false;
 }
 }
 
 function yieldScript(currentContext) {

	if (currentContext.getRemainingUsage() <= 2000) {
		nlapiLogExecution('AUDIT', 'API Limit Exceeded');
		var state = nlapiYieldScript();

		if (state.status == "FAILURE") {
			nlapiLogExecution('ERROR', 'Yield Script Failed', 'Reason : '
			        + state.reason + ' / Size : ' + state.size);
			return false;
		} else if (state.status == "RESUME") {
			nlapiLogExecution('AUDIT', 'Script Resumed');
		}
	}
}