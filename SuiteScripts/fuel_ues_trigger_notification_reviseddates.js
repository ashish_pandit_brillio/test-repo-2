/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       12 Sep 2019     Aazamali Khan
 *
 */
/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType
 * 
 * @param {String} type Operation types: create, edit, delete, xedit,
 *                      approve, cancel, reject (SO, ER, Time Bill, PO & RMA only)
 *                      pack, ship (IF only)
 *                      dropship, specialorder, orderitems (PO only) 
 *                      paybills (vendor payments)
 * @returns {Void}
 */
function uestriggerEmailForRevisedDates(type) {
	try {
		if (type == "edit") {
			var context = nlapiGetContext();
			var s_type = context.getExecutionContext();
			if(s_type == "scheduled"){
				var recObj = nlapiLoadRecord(nlapiGetRecordType(), nlapiGetRecordId());
				var body = "";
				var oldObj = nlapiGetOldRecord();
				var s_recipient = recObj.getFieldText("custrecord_frf_details_created_by");
				if (s_recipient) {
					name = s_recipient.split("-")[1];
				} else {
					name = s_recipient;
				}
				var i_recipient = recObj.getFieldValue("custrecord_frf_details_created_by");
				var originalStartDate = recObj.getFieldValue("custrecord_frf_details_ori_start_date");
				var originalStartDateOld = oldObj.getFieldValue("custrecord_frf_details_ori_start_date");
				var originalEndDate = recObj.getFieldValue("custrecord_frf_details_ori_end_date");
				var originalEndDateOld = oldObj.getFieldValue("custrecord_frf_details_ori_end_date");
				var reviseStartDate = "";
				var reviseEndDate = "";
				if (originalStartDate) {
					reviseStartDate = recObj.getFieldValue("custrecord_frf_details_start_date");
				}
				if (originalEndDate) {
					reviseEndDate = recObj.getFieldValue("custrecord_frf_details_end_date");
				}
				var frfNumber = recObj.getFieldValue("custrecord_frf_details_frf_number");
				var oppId = recObj.getFieldValue("custrecord_frf_details_opp_id");
				var OppName = nlapiLookupField("customrecord_sfdc_opportunity_record", oppId, "custrecord_opportunity_name_sfdc");
				var i_practice = recObj.getFieldValue("custrecord_frf_details_res_practice");
				var project = recObj.getFieldValue("custrecord_frf_details_project");
				var fl_externalHire = recObj.getFieldValue("custrecord_frf_details_external_hire");
				var searchResult = getPracticeSpocEmail(i_practice);
				var arr_cc = [];
				if (searchResult) {
					for (var i = 0; i < searchResult.length; i++) {
						arr_cc.push(searchResult[i].getValue("email", "CUSTRECORD_SPOC", null))
					}
				}
				if (project) {
					var i_projectManager = nlapiLookupField("job", project, "custentity_projectmanager");
					arr_cc.push(nlapiLookupField("employee", i_projectManager, "email"));
					var i_deliveryManager = nlapiLookupField("job", project, "custentity_deliverymanager");
					arr_cc.push(nlapiLookupField("employee", i_deliveryManager, "email"));
				}
				arr_cc.push("fuel.support@brillio.com");
				if(fl_externalHire == "F"){
					arr_cc.push("business.ops@brillio.com");	
				}
				nlapiLogExecution("DEBUG", "originalEndDate : ", originalEndDate);
				nlapiLogExecution("DEBUG", "originalStartDate : ", originalStartDate);
				nlapiLogExecution("DEBUG", "originalEndDateOld : ", originalEndDateOld);
				nlapiLogExecution("DEBUG", "originalStartDateOld : ", typeof(originalStartDateOld));
				if (originalEndDate != originalEndDateOld || originalStartDate != originalStartDateOld) {
					var body = "Dear " + name + " \n" + "# " + frfNumber + " has been modified as part the start/end date opportunity '" + OppName + "' which it is hired against has changed." + "\n" + "Please review the changes and do needful." + "\n\n\n";
					body += "Change Summary : " + "\n" + "\n";
					if (originalStartDate) {
						body += "Actual Start Date : " + originalStartDate + "                               Revised Start Date : " + reviseStartDate + "\n";
					}
					if (originalEndDate) {
						body += "Actual End Date : " + originalEndDate + "                                   Revised End Date : " + reviseEndDate + "\n";
					}
					body += "\n";
					body += "This email was sent from a notification only address.\n"
					body += "If you need further assistance, please write to fuel.support@brillio.com \n";
					var subject = frfNumber + ": FUEL Notification: Revised Start/End Date";
					nlapiSendEmail(154256, i_recipient, subject, body, arr_cc);
					//nlapiSendEmail(154256,"aazamali@inspirria.com", subject, body);
				}
			}
		}
	} catch (e) {
		// TODO: handle exception
		nlapiLogExecution("DEBUG", "Error in Script : ", e);
	}
}

function getPracticeSpocEmail(practice) {
	var customrecord_practice_spocSearch = nlapiSearchRecord("customrecord_practice_spoc", null,
		[
			["custrecord_practice", "anyof", practice], "AND",
			["custrecord_spoc.custentity_employee_inactive", "is", "F"]
		],
		[
			new nlobjSearchColumn("email", "CUSTRECORD_SPOC", null)
		]);
	return customrecord_practice_spocSearch;
}