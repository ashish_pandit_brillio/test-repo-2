/**
  *@NApiVersion 2.0
  *@NScriptType UserEventScript
  *@NModuleScope SameAccount
  */

 define(['N/ui/dialog','N/runtime','N/record','N/log','N/search','N/currentRecord','N/url','N/http','N/redirect'],
 function(dialog,runtime,record,log,search,currRecord,url,http,redirect) {

function ofcbeforejeupload(scriptContext) {
var cRecord =  scriptContext.newRecord
var form = scriptContext.form;

var action_type = scriptContext.type;
var currentRecord = currRecord.get();
//form.clientScriptModulePath = './cli_ofc_call_suitelet.js';
 form.clientScriptFileId=4319943;//SB: 4150245;
  
  log.debug('action_type',action_type);



if(action_type == 'create'){
  
    cRecord.setValue({
        fieldId: 'custrecord_ofc_current_status',
        value: 1
        });
    

}

if(action_type == 'view'){
    // deleteOFCRecord(6,8);
   //var creditAndJE = creditRecordAndJE(7,8);
   //log.debug("creditAndJE",creditAndJE);
   var i_status = cRecord.getValue({
     fieldId: 'custrecord_ofc_current_status',
   });
   
  
  
  var csvFile = cRecord.getValue({
    fieldId: 'custrecord_ofc_csv_file',
  });
 
 
  
if(i_status == 1)
{
    if(_logValidation(csvFile))
    {
       
        form.addButton({
            id: 'custpage_validate_process',
            label: 'Validate Process',
            functionName: 's_suitelet_validate()'
        });
      
		
    }
    
}

if(i_status == 4)
{
    if(_logValidation(csvFile))
    {
        
     form.addButton({
            id: 'custpage_create_je',
            label: 'Create JE',
            functionName:'s_suitelet_create()'
        });
       
       
    }
    
}

if(i_status == 3)
{
    if(_logValidation(csvFile))
    {
        
        form.addButton({
            id: 'custpage_validate_process',
            label: 'Delete File',
            functionName:'s_suitelet_delete()'
        });
        
    }
    
}

if(i_status==2)
		{
			var message = '<html>';
			message += '<head>';
			message += "<meta http-equiv=\"refresh\" content=\"5\" \/>";
			message += '<meta charset="utf-8" />';
			message += '</head>';
			message += '<body>';
			var i_counter = '0%'
			message += "<div id=\"my-progressbar-container\">";
			message += "            ";
			message += "<div class=\"ajax-content\" id=\"test14\" name=\"test14\" style='height:50%;width:50%;' align='left' valign='top'>"; //opacity: .75;
			message += "<img src='https://3883006-sb1.app.netsuite.com/core/media/media.nl?id=190162&c=3883006&h=1cf2a9f8bd8b922539c8' align='left' style='height:50%;width:50%;align=left;'/>"; // Added to take image from SB
			message += "        <\/div>";
			message += '</body>';
            message += '</html>';
            cRecord.setValue({
                fieldId: 'custrecord_ofc_progress_bar',
                value: message
              });
			
		}



    

}
return true







}

//    function triggerSuitelet(suiteletURL){
//     log.debug('triggered Suitelet',triggerSuitelet);
//   }

// function getSuiteletURL(i_mode,i_rec_id,s_rec_type){
 
//   var validateURL = '';
//     log.debug('i_mode',i_mode+','+i_rec_id+','+s_rec_type);
//     if(i_mode == 'validate'){
//         validateURL =  url.resolveScript({
// 			scriptId : 2329,
// 			deploymentId :'customdeploy_sut_ofc_call_scheduled',
// 			params : {
// 				'i_rec_id' : i_rec_id,
// 				'i_mode': i_mode,
// 				's_rec_type': s_rec_type
// 			}
//         });
        
        

//     }
//     if(i_mode == 'create'){
//          validateURL =  url.resolveScript({
// 			scriptId : 2329,
// 			deploymentId :'customdeploy_sut_ofc_call_scheduled',
// 			params : {
// 				'i_rec_id' : i_rec_id,
// 				'i_mode': i_mode,
// 				's_rec_type': s_rec_type
// 			}
//         });
        
        
        
//     }
//     if(i_mode == 'delete'){
//          validateURL =  url.resolveScript({
// 			scriptId : 2329,
// 			deploymentId :'customdeploy_sut_ofc_call_scheduled',
// 			params : {
// 				'i_rec_id' : i_rec_id,
// 				'i_mode': i_mode,
// 				's_rec_type': s_rec_type
// 			}
//         });
       
        
//     }
//     return validateURL;

// }

function _logValidation(value) 
{
 if(value != null && value!= 'null' && value != '' && value != undefined && value != undefined && value != 'undefined' && value.toString() != 'NaN' && value != NaN) 
 {
  return true;
 }
 else 
 { 
  return false;
 }
}
   
function beforeSub(context){
  //return true;
  var action_type = context.type;
  if(action_type == 'create' ){
    var cRecord =  context.newRecord;
    var i_month = cRecord.getValue({fieldId: 'custrecord_ofc_month'});
    var i_year = cRecord.getValue({fieldId: 'custrecord_ofc_year'});
    var creditandJE  =  creditRecordAndJE(i_month,i_year);
    var creditRec = creditandJE[0];
    var JERec     = creditandJE[1];
    log.debug("creditRec",creditRec);
    log.debug("JERec",JERec);
    if(creditRec.length> 0 && JERec.length>0){
      var htmlText = '';
      htmlText +="<h1> Following are the records which are already present in the system for the given month :- </h1>"
      var _id = creditRec[0];
      var urLpath = "https://3883006.app.netsuite.com/app/common/custom/custrecordentry.nl?rectype=641&id="+_id;
      var _blank = "_blank";
      htmlText += "<a href= "+urLpath+" target= "+_blank +">Link</a>"
      htmlText +="<br></br>"
      htmlText +="<h1> Do you want to re-run the module for given month ?";
      htmlText +="<h1> It will delete the Journal Entry, OFC Cost Custom Table created earlier for the given month";
      var button_type = "button";
      //htmlText +="<button type="+button_type+" onclick="+alert('Hello world!')+">Re-run Module !</button>"
      var i_mode = "re-run";
      var s_rec_type = "customrecord_ofc_cost_for_credit_amount";
      var destination_url = "https://3883006.app.netsuite.com/app/site/hosting/scriptlet.nl?script=2247&deploy=1&i_mode="+i_mode+"&i_rec_id="+_id+"&s_rec_type="+s_rec_type;
      htmlText +="<br></br>"
       htmlText += "<a href= "+destination_url+" >Re-run Module !</a>"
      //htmlText +="<button type="+button_type+" onclick= location.href = "+destination_url +">Re-run Module !</button>"

      throw htmlText;
      return false;
    }
  }

  return true;

}

function creditRecordAndJE(i_month,i_year){
  try{
    log.debug("i_month",i_month);
    log.debug("i_year",i_year);
var customrecord_ofc_cost_for_credit_amountSearchObj = search.create({
   type: "customrecord_ofc_cost_for_credit_amount",
   filters:
   [ 
      ["custrecord_ofc_month","anyof",i_month], 
      "AND", 
      ["custrecord_ofc_year","anyof",i_year]
   ],
   columns:
   [
      search.createColumn({name: "custrecord_ofc_journal_entry", label: "Journal Entry"}),
      search.createColumn({name: "internalid", label: "Internal ID"})
   ]
});
var searchResultCount = customrecord_ofc_cost_for_credit_amountSearchObj.runPaged().count;
var internalIDs = new Array();
var journalEntries = new Array();
log.debug("customrecord_ofc_cost_for_credit_amountSearchObj result count",searchResultCount);
customrecord_ofc_cost_for_credit_amountSearchObj.run().each(function(result){
  internalIDs.push(result.getValue({name: "internalid"}));
  journalEntries.push(result.getValue({name: "custrecord_ofc_journal_entry"}))
   return true;
});
    
log.debug('Internal IDs',internalIDs);
log.debug('journalEntries',journalEntries);
    return [internalIDs,journalEntries];

  } catch(e){
    log.debug("exception",e);
  }
  
        
    


}
   



return {

 beforeLoad: ofcbeforejeupload,
 beforeSubmit: beforeSub
};

});