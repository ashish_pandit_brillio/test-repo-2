// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
/*
   	Script Name : SUT_Salary_Upload_Validate_Process_BTPL.js
	Author      : Jayesh Dinde
	Date        : 11 April 2016
    Description : Suitlet Call for validate process


	Script Modification Log:

	-- Date --			-- Modified By --				--Requested By--				-- Description --



Below is a summary of the process controls enforced by this script file.  The control logic is described
more fully, below, in the appropriate function headers and code blocks.


     SUITELET
		- suiteletFunction(request, response)


     SUB-FUNCTIONS
		- The following sub-functions are called by the above core functions in order to maintain code
            modularization:

               - NOT USED

*/
}
// END SCRIPT DESCRIPTION BLOCK  ====================================



// BEGIN GLOBAL VARIABLE BLOCK  =====================================
{
	//  Initialize any Global Variables, in particular, debugging variables...




}
// END GLOBAL VARIABLE BLOCK  =======================================





// BEGIN SUITELET ==================================================

function suiteletFunction(request, response)
{
	try
	{
		var i_recordID =  request.getParameter('custscript_record_id_su');
		i_recordID = i_recordID.trim();
		var o_recordOBJ = nlapiLoadRecord('customrecord_salary_upload_process_btpl', i_recordID);
		var i_account_debit = o_recordOBJ.getFieldText('custrecord_account_debit_s_p_btpl');
		var i_account_credit = o_recordOBJ.getFieldText('custrecord_account_credit_s_p_btpl');
		var sal_upload_date_month = o_recordOBJ.getFieldText('custrecord_month_btpl');
		var sal_upload_date_year = o_recordOBJ.getFieldText('custrecord_year_btpl');
		var vendor_provision_subsidiary = o_recordOBJ.getFieldValue('custrecord_subsidiary_btpl');
		var current_status = o_recordOBJ.getFieldValue('custrecord_status_btpl');
		var rate_type = o_recordOBJ.getFieldValue('custrecord_rate_type');
		// ================= Call Schedule Script ================
		
		var array_fld = new Array();
		array_fld[0] = 'custrecord_status_btpl';
		array_fld[1] = 'custrecord_completion_status';
		array_fld[2] = 'custrecord_notes_btpl';
		array_fld[3] = 'custrecord_prcnt_complete';
		array_fld[4] = 'custrecord_which_bttn_clicked';
		array_fld[5] = 'custrecord_contractor_count';
		array_fld[6] = 'custrecord_total_processed';
		array_fld[7] = 'custrecord_not_processed';
		
		if(rate_type == 2)
		{
			var array_fld_values = new Array();
			array_fld_values[0] = 3;
			array_fld_values[1] = 'Not Started';
			array_fld_values[2] = '';
			array_fld_values[3] = '';
			array_fld_values[4] = 'VALIDATE';
			array_fld_values[5] = '';
			array_fld_values[6] = '';
			array_fld_values[7] = '';
		
			var params=new Array();
			params['custscript_sal_upload_date_month'] = sal_upload_date_month;
			params['custscript_sal_upload_date_year'] = sal_upload_date_year;
			params['custscript_account_debit_s_u_btpl'] = i_account_debit;
			params['custscript_account_credit_s_u_btpl'] = i_account_credit;
			params['custscriptrecord_id_s_u_btpl'] = i_recordID;
			params['custscript_vendor_pro_subsidiary'] = vendor_provision_subsidiary;
			params['custscript_vendor_pro_status'] = current_status;
		 
			var status=nlapiScheduleScript('customscript_sch_vendor_pro_vali_data',null,params);
		}
		else
		{
			var array_fld_values = new Array();
			array_fld_values[0] = 3;
			array_fld_values[1] = 'Not Started';
			array_fld_values[2] = '';
			array_fld_values[3] = '';
			array_fld_values[4] = 'VALIDATE_HOURLY';
			array_fld_values[5] = '';
			array_fld_values[6] = '';
			array_fld_values[7] = '';
		
			var params=new Array();
			params['custscript_sal_upload_month_btpl'] = sal_upload_date_month;
			params['custscript_sal_upload_year_btpl'] = sal_upload_date_year;
			params['custscript_account_debit_btpl'] = i_account_debit;
			params['custscript_account_credit_btpl'] = i_account_credit;
			params['custscript_record_id_btpl'] = i_recordID;
			params['custscript_vendor_subsidiary'] = vendor_provision_subsidiary;
			params['custscript_vendor_rcrd_status'] = current_status;
			
			var status=nlapiScheduleScript('customscript_sch_vendor_vali_data_hourly',null,params);
		}
		nlapiSubmitField('customrecord_salary_upload_process_btpl',i_recordID,array_fld,array_fld_values);			 
		
		nlapiSetRedirectURL('RECORD', 'customrecord_salary_upload_process_btpl', i_recordID, null,null);	

	}
	catch(exception)
	{
	  nlapiLogExecution('DEBUG', 'ERROR',' Exception Caught -->' + exception);	
	}
}

// END SUITELET ====================================================




// BEGIN OBJECT CALLED/INVOKING FUNCTION ===================================================
/**
 * 
 * @param {Object} value
 * 
 * Description --> If the value is blank /null/undefined returns false else returns true
 */
function _logValidation(value) 
{
 if(value.toString()!='null' && value != null && value.toString() != null && value != '' && value != undefined && value.toString() != undefined && value != 'undefined' && value.toString() != 'undefined'&& value.toString() != 'NaN' && value != NaN) 
 {
  return true;
 }
 else 
 { 
  return false;
 }
}
// END OBJECT CALLED/INVOKING FUNCTION =====================================================
