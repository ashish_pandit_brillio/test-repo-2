/* Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       17 Mar 2016     amol.sahijwani
 *
 */

/**
 * @param {String} type Context Types: scheduled, ondemand, userinterface, aborted, skipped
 * @returns {Void}
 */
function scheduled(type) {
	
	var context	=	nlapiGetContext();
	
	var d_today	=	new Date();
	
	var d_start_date	=	nlapiAddMonths(nlapiAddDays(d_today, -1 * d_today.getDate() + 1), -1);
	
	var d_end_date		=	nlapiAddDays(nlapiAddMonths(d_start_date, 1), -1);
	
	d_start_date		=	nlapiStringToDate(nlapiDateToString(d_start_date));
	
	d_end_date			=	nlapiStringToDate(nlapiDateToString(d_end_date));
	
	// Get Contractor List	
	var filters	=	new Array();
	filters[0]	=	new nlobjSearchFilter('custrecord_stvd_start_date', null, 'onorbefore', d_end_date);
	filters[1]	=	new nlobjSearchFilter('custrecord_stvd_end_date', null, 'onorafter', d_start_date);
	filters[2]	=	new nlobjSearchFilter('custrecord_stvd_subsidiary', null, 'anyof', 3);
	
	var columns	=	new Array();
	columns[0]	=	new nlobjSearchColumn('custrecord_stvd_vendor');
	columns[1]	=	new nlobjSearchColumn('custrecord_stvd_start_date');
	columns[2]	=	new nlobjSearchColumn('custrecord_stvd_end_date');
	columns[3]	=	new nlobjSearchColumn('custrecord_stvd_st_pay_rate');
	columns[4]	=	new nlobjSearchColumn('custrecord_stvd_contractor');
	columns[5]	=	new nlobjSearchColumn('department', 'custrecord_stvd_contractor');
	columns[6]	=	new nlobjSearchColumn('custrecord_stvd_rate_type');
	columns[7]	=	new nlobjSearchColumn('email', 'custrecord_stvd_vendor');
	
	var searchVendorData	=	searchRecord('customrecord_subtier_vendor_data', null, filters, columns);
	nlapiLogExecution('DEBUG', 'searchVendorData ', JSON.stringify(searchVendorData));
	var a_contractor_list	=	getContractorList(searchVendorData);
	nlapiLogExecution('DEBUG', 'a_contractor_list ');
	var objContractorData	=	groupContractorData(searchVendorData, a_contractor_list);
	nlapiLogExecution('DEBUG', 'objContractorData ');
	var objResourceAllocationData	=	getResourceAllocationsForEmployees(a_contractor_list, d_start_date, d_end_date);
	nlapiLogExecution('DEBUG', 'objResourceAllocationData ');
	var objTimesheetData	=	getTimesheetDataForEmployees(a_contractor_list, d_start_date, d_end_date);
	nlapiLogExecution('DEBUG', 'objTimesheetData ');
	var a_line_data	=	new Array();
	
	for(var contractor_indx = 0; contractor_indx < a_contractor_list.length; contractor_indx++)
		{
			yieldScript(context);
		
			nlapiLogExecution('DEBUG', 'Remaining Usage: ', context.getRemainingUsage());
			
			var i_contractor_id	=	a_contractor_list[contractor_indx];
		
			var objLineItem		=	{'vendor_id': '', 'vendor_name': '', 'vendor_email': '', 'contractor': i_contractor_id, 'status': '', 'wo_start_date': '', 'wo_end_date': '', 'rate': 0.0, 'allocated_days': 0, 'days_worked': 0, 'amount_to_be_paid': 0.0, rate_type: ''};
			
			if(objContractorData[i_contractor_id].length == 1)
				{
					var objWorkOrder	=	objContractorData[i_contractor_id][0];
					
					var d_period_start_date	=	objWorkOrder.wo_start_date > d_start_date?objWorkOrder.wo_start_date:d_start_date;
					var d_period_end_date	=	objWorkOrder.wo_end_date < d_end_date?objWorkOrder.wo_end_date:d_end_date;					
					
					var a_project_list	=	new Array();
					
					var a_holiday_list	=	new Array();
					
					// Get Allocations
					var o_allocation_data	=	objResourceAllocationData[i_contractor_id];
					
					d_period_start_date	=	d_period_start_date < o_allocation_data.startdate?o_allocation_data.startdate:d_period_start_date;
					d_period_end_date	=	d_period_end_date	> o_allocation_data.enddate?o_allocation_data.enddate:d_period_end_date;
					
					var a_allocations	=	o_allocation_data.allocation;
					
					var a_data	=	new Array();
					
					var o_date	=	d_period_start_date;
					
					while(o_date <= d_period_end_date)
						{
							// Get Allocation
							var o_allocation	=	getAllocationForDate(a_allocations, o_date);
							
							// Only one allocation per day is supported
							if(o_allocation.allocation_count == 1)
								{
									a_data.push({'date': o_date, 'allocation': o_allocation.allocation, 'timesheets': [], 'hours_worked': 0.0});
								
									o_date	=	nlapiAddDays(o_date, 1);
									
									// Add project to project list
									if(a_project_list.indexOf(o_allocation.allocation.project_id) == -1)
										{
											a_project_list.push(o_allocation.allocation.project_id);
										}
									
								}
							else
								{
									if(o_allocation.allocation_count > 1)
										{
											s_error	=	'Multiple Allocations For A Day';
										}
									else
										{
											s_error	=	'No Allocation found for some days in the range';
										}
									
									objLineItem.status = s_error;
									break;
								}
						}
					
					if(objLineItem.status != '')
						{
							continue;
						}
					
					// Get Timesheet Data
					var a_timesheet_data	=	new Array();
					
					a_timesheet_data	=	objTimesheetData[i_contractor_id];
					
					for(var i = 0; i < a_timesheet_data.length; i++)
						{
							for(var j = 0; j < a_data.length; j++)
								{
									if(a_data[j].date.getDate() == a_timesheet_data[i].date.getDate() && a_data[j].date.getMonth() == a_timesheet_data[i].date.getMonth() && a_data[j].date.getFullYear() == a_timesheet_data[i].date.getFullYear())
										{
											a_data[j].timesheets.push(a_timesheet_data[i]);
										}
								}
						}
					
					// Count the number of days per project
					var o_day_count	= new Object();
					
					for(var i = 0; i < a_project_list.length; i++)
						{
							// Calculated allocated days for project
							var i_allocated_days = 0;
							
							for(var j = 0; j < a_allocations.length; j++)
								{
									if(a_project_list[i] == a_allocations[j].project_id)
										{
											var oneDay = 24*60*60*1000; // hours*minutes*seconds*milliseconds
											
											var d_calculate_allocated_days_end_date = a_allocations[j].end_date > d_period_end_date?d_period_end_date:a_allocations[j].end_date;
											
											var d_calculate_allocated_days_start_date = a_allocations[j].start_date < d_period_start_date?d_period_start_date:a_allocations[j].start_date;
											
											i_allocated_days += Math.round(Math.abs((d_calculate_allocated_days_end_date.getTime() - d_calculate_allocated_days_start_date.getTime())/(oneDay))) + 1;
										}
								}
							
							o_day_count[a_project_list[i]]	=	{'working_hours': 0, 'working_day_count': 0, 'actual_days_worked': 0, 'allocated_days_count': i_allocated_days, 'holiday_count': 0, 'sat_sun_count': 0, 'one_day_leave_count': 0, 'vertical': null, 'customer': null, 'project_description': null, 'employee_name': null, 'start_date': d_calculate_allocated_days_start_date, 'end_date': d_calculate_allocated_days_end_date};
						}
					
					var a_billed_time_entries_id	=	new Array();
					
					var a_sat_sun_to_be_added	=	new Array();
					
					for(var i = 0; i < a_data.length; i++)
						{
							var f_total_duration_for_a_day	=	0.0;
							var i_project_id	=	null;
							
							for(var j = 0; j < a_data[i].timesheets.length; j++)
								{
									// Check if multiple projects for a day
									if(j > 0)
										{
											if(a_data[i].timesheets[j].project != a_data[i].timesheets[j - 1].project)
												{
													s_error	=	'Multiple projects on a single day';
													objLineItem.status	=	s_error;													
													break;
												}
										}
									
									if(a_data[i].timesheets[j].item != '2479' && a_data[i].timesheets[j].item != '2480' && a_data[i].timesheets[j].item != '2481')
										{
											f_total_duration_for_a_day	+=	parseInt(a_data[i].timesheets[j].duration);
											//a_billed_time_entries_id.push(a_data[i].timesheets[j].id);
										}
									
									i_project_id	=	a_data[i].timesheets[j].project;
								}
							
							if(objLineItem.status != '')
								{
									break;
								}
							
							try
							{
							if(f_total_duration_for_a_day > 0.0)
								{
									(o_day_count[i_project_id].working_day_count)++; // Add 1 to working days
									(o_day_count[i_project_id].actual_days_worked)++; // Add 1 to actual days worked
									o_day_count[i_project_id].working_hours	+=	f_total_duration_for_a_day;	// Add working hours
								}			
							}
							catch(e)
							{
								nlapiLogExecution('ERROR', 'data', JSON.stringify(a_data[i]));
								
								s_error	=	e.message;
								objLineItem.status	=	s_error;
								break;
							}
							if(o_day_count[a_data[i].allocation.project_id] != undefined && (a_data[i].date.getDay() == 0 || a_data[i].date.getDay() == 6))
								{
									a_sat_sun_to_be_added.push({'project_id': a_data[i].allocation.project_id, 'count': 1, 'date': a_data[i].date});
								}
						}
					
					if(objLineItem.status != '')
						{
							continue;
						}					
					
					// Calculate Holidays
					for(var i = 0; i < a_allocations.length; i++)
						{
							if(o_day_count[a_allocations[i].project_id] != undefined)
								{
									var d_allocation_start_date	=	a_allocations[i].start_date	< d_period_start_date?d_period_start_date:a_allocations[i].start_date;
									
									var d_allocation_end_date	=	a_allocations[i].end_date > d_period_end_date?d_period_end_date:a_allocations[i].end_date;
								
									// Set Vertical
									o_day_count[a_allocations[i].project_id].vertical	=	a_allocations[i].vertical;
									o_day_count[a_allocations[i].project_id].customer	=	a_allocations[i].customer;
									o_day_count[a_allocations[i].project_id].project_description	=	a_allocations[i].project_description;
									o_day_count[a_allocations[i].project_id].employee_name	=	objWorkOrder.contractor_name;
									
									// Add Holidays
									if(o_day_count[a_allocations[i].project_id].actual_days_worked > 0) // Should have worked 1 or more days 
										{
											var a_holiday_for_project	=	calculate_non_billable_holiday(d_allocation_start_date, d_allocation_end_date, {'contractor': i_contractor_id, 'subsidiary': a_allocations[i].employee_subsidiary, 'project_holiday_type': a_allocations[i].project_holiday_type, 'customer': a_allocations[i].customer_id}, a_allocations[i].project_id);
										
											for(var j = 0; j < a_holiday_for_project.length; j++)
												{
													a_holiday_list.push(nlapiStringToDate(a_holiday_for_project[j], 'date'));
												}
											
											//a_holiday_list	=	a_holiday_list.concat(a_holiday_for_project);
											
											var i_holiday_count	=	a_holiday_for_project.length;
											
											o_day_count[a_allocations[i].project_id].working_day_count += i_holiday_count;
											o_day_count[a_allocations[i].project_id].holiday_count	+=	i_holiday_count;
										}	
								}
						}
					
					var i_total_allocated_days	=	0;
					var i_total_days_worked	=	0;
					var i_total_actual_days_worked = 0;
					
					for(var s_project in o_day_count)
						{
							i_total_actual_days_worked += o_day_count[s_project].actual_days_worked;
						}
					
					// Add Saturday Sunday
					while(i_total_actual_days_worked > 0 && a_sat_sun_to_be_added.length > 0)
					{
						var o_sat_sun_to_be_added	=	a_sat_sun_to_be_added.pop();
							
						o_day_count[o_sat_sun_to_be_added.project_id].working_day_count	+=	o_sat_sun_to_be_added.count;
						o_day_count[o_sat_sun_to_be_added.project_id].sat_sun_count	+=	o_sat_sun_to_be_added.count;
					}
					
					// Calculation for 1 leave allowed
					var oneLeaveUsed	=	false;
					
					for(var s_project in o_day_count)
					{
						if(o_day_count[s_project].allocated_days_count > o_day_count[s_project].working_day_count && i_total_actual_days_worked > 0)
							{
								if(oneLeaveUsed == false)
									{
										o_day_count[s_project].working_day_count++;
										
										o_day_count[s_project].one_day_leave_count++;
										
										oneLeaveUsed	=	true;
									}
							}
					}
					
					for(var s_project in o_day_count)
					{
						i_total_allocated_days	+=	o_day_count[s_project].allocated_days_count;
						i_total_days_worked	+=	o_day_count[s_project].working_day_count;	
					}
					
					// If total days exceed allocated days deduct those days
					if(i_total_days_worked > i_total_allocated_days)
						{
							for(var s_project in o_day_count)
							{
								if(o_day_count[s_project].working_day_count > o_day_count[s_project].allocated_days_count)
									{
										o_day_count[s_project].working_day_count	=	o_day_count[s_project].allocated_days_count;					
									}
							}
						}
					
					// Display TimeSheet Data
					var s_days	=	'';
					
					var i_total_working_days	=	0;
					var i_total_working_hours	=	0;
					var f_total_amount	=	0.0;
					
					for(var s_project in o_day_count)
						{
							var f_amount	=	getAmount(objWorkOrder.rate_type.id, o_day_count[s_project].working_day_count, o_day_count[s_project].working_hours, d_start_date, d_end_date, objWorkOrder.pay_rate);
						
							i_total_working_hours	+=	o_day_count[s_project].working_hours;
							f_total_amount	+=	f_amount;
							
							s_days	+=	'<span>Project: </span>'	+	o_day_count[s_project].project_description	+	'<br /><span>Allocated Days: </span>'	+	o_day_count[s_project].allocated_days_count;
							s_days	+=	'<span><br />Days Worked: ' + o_day_count[s_project].actual_days_worked + ', Holidays: ' + o_day_count[s_project].holiday_count + ', Allowed Leaves: ' + o_day_count[s_project].one_day_leave_count + ', Saturday-Sunday: ' + o_day_count[s_project].sat_sun_count + '<br />Total Days: </span>'	+	o_day_count[s_project].working_day_count + '<br />';
							s_days	+=	'<b>Working Hours: ' + o_day_count[s_project].working_hours + '</b><br />';
							s_days	+=	'<b>Amount: ' + f_amount.toFixed(2) + '</b><br />';
							
							i_total_working_days	+=	o_day_count[s_project].working_day_count;
						}
					
					if(a_project_list.length > 1)
						{
							s_days	+=	'<span style="font-weight: bold;">Total Working Hours: '	+	i_total_working_hours	+	'</span><br />';
							s_days	+=	'<span style="font-weight: bold;">Total Amount: '	+	f_total_amount.toFixed(2)	+	'</span><br />';
						}
					objLineItem.vendor_id		=	objWorkOrder.vendor;
					objLineItem.vendor_name		=	objWorkOrder.vendor_name;
					objLineItem.vendor_email	=	objWorkOrder.vendor_email;
					objLineItem.allocated_days	=	i_total_allocated_days;
					objLineItem.rate			=	objWorkOrder.pay_rate;
					objLineItem.rate_type = objWorkOrder.rate_type.name;
					objLineItem.contractor		=	objWorkOrder.contractor_name;
					objLineItem.wo_start_date	=	objWorkOrder.wo_start_date;
					objLineItem.wo_end_date		=	objWorkOrder.wo_end_date;
					objLineItem.days_worked	=	i_total_working_days;
					objLineItem.amount_to_be_paid	=	f_total_amount.toFixed(2);
				}
			
			a_line_data.push(objLineItem);
			
		}
	
	var obj_vendor_email_data	=	new Object();
	
	for(var i = 0; i < a_line_data.length; i++)
		{
			if(obj_vendor_email_data[a_line_data[i].vendor_id] == undefined)
				{
					obj_vendor_email_data[a_line_data[i].vendor_id]	=	new Array();
				}
			
			obj_vendor_email_data[a_line_data[i].vendor_id].push(a_line_data[i])
			
		}
	
	for(var i_vendor_id in obj_vendor_email_data)
		{
			var s_email_text	=	'<table border="1" cellpadding="5" cellspacing="0" style="border-collapse: collapse" width="80%"><tr><th>Contractor</th><th>Work Order Start Date</th><th>Work Order End Date</th><th>Allocated Days</th><th>Days Worked</th><th>Rate</th><th>Amount To Be Paid</th><th>Rate Type</th></tr>';
			
			for(var i = 0; i < obj_vendor_email_data[i_vendor_id].length; i++)
				{
					s_email_text	+=	'<tr><td>' + obj_vendor_email_data[i_vendor_id][i].contractor + '</td>';
					s_email_text	+=	'<td>' + formatDate(obj_vendor_email_data[i_vendor_id][i].wo_start_date) + '</td>';
					s_email_text	+=	'<td>' + formatDate(obj_vendor_email_data[i_vendor_id][i].wo_end_date) + '</td>';
					s_email_text	+=	'<td>' + obj_vendor_email_data[i_vendor_id][i].allocated_days + '</td>';
					s_email_text	+=	'<td>' + obj_vendor_email_data[i_vendor_id][i].days_worked + '</td>';
					s_email_text	+=	'<td>' + obj_vendor_email_data[i_vendor_id][i].rate + '</td>';
					s_email_text	+=	'<td>' + obj_vendor_email_data[i_vendor_id][i].amount_to_be_paid + '</td>';
					s_email_text += '<td>' + obj_vendor_email_data[i_vendor_id][i].rate_type + '</td></tr>';
				}
			
			s_email_text	+=	'</table>';
			
			var a_to	=	['swetha.b@brillio.com'];
			
			if(obj_vendor_email_data[i_vendor_id][0].vendor_email != '')
				{
					a_to.push(obj_vendor_email_data[i_vendor_id][0].vendor_email);
				}
			
			nlapiSendEmail(442, a_to, obj_vendor_email_data[i_vendor_id][0].vendor_name + ', ' + formatDate(d_start_date) + ' to ' + formatDate(d_end_date), s_email_text, ['amol.sahijwani@brillio.com', 'lisa.joshy@brillio.com', 'jai.kumar@brillio.com', 'govind.kharayat@brillio.com'], null);
		
			//nlapiLogExecution('DEBUG', 'Test', s_email_text);

		}
}

function groupContractorData(searchVendorData, a_contractor_list)
{
	var objData	=	new Object();
	
	for(var i = 0; i < a_contractor_list.length; i++)
		{
			objData[a_contractor_list[i]]	=	new Array();
		}
	
	for(var i = 0; searchVendorData != null && i < searchVendorData.length; i++)
		{
			var i_contractor_id	=	searchVendorData[i].getValue('custrecord_stvd_contractor');
			
			var i_vendor_id	=	searchVendorData[i].getValue('custrecord_stvd_vendor');
			var s_vendor_name	=	searchVendorData[i].getText('custrecord_stvd_vendor');
			var f_pay_rate	=	searchVendorData[i].getValue('custrecord_stvd_st_pay_rate');
			var s_contractor_name	=	searchVendorData[i].getText('custrecord_stvd_contractor');
			var i_contractor_practice	=	searchVendorData[i].getValue('department', 'custrecord_stvd_contractor');
			var s_work_order_start_date	=	searchVendorData[i].getValue('custrecord_stvd_start_date');
			var s_work_order_end_date	=	searchVendorData[i].getValue('custrecord_stvd_end_date');
			var i_rate_type	=	searchVendorData[i].getValue('custrecord_stvd_rate_type');
			var s_rate_type	=	searchVendorData[i].getText('custrecord_stvd_rate_type');
			var d_work_order_start_date	=	nlapiStringToDate(s_work_order_start_date);
			var d_work_order_end_date	=	nlapiStringToDate(s_work_order_end_date);
			var s_email	= searchVendorData[i].getValue('email', 'custrecord_stvd_vendor');
			
			objWorkOrder	=	{'vendor': i_vendor_id, 'vendor_email': s_email, 'contractor_name': s_contractor_name, 'contractor_practice': i_contractor_practice, 'vendor_name': s_vendor_name, 'pay_rate': f_pay_rate, 'wo_start_date': d_work_order_start_date, 'wo_end_date': d_work_order_end_date, 'rate_type': {'id': i_rate_type, 'name': s_rate_type}};
			
			objData[i_contractor_id].push(objWorkOrder);
		}
	
	return objData;
}

function getContractorList(searchVendorData)
{
	var a_contractor_list	=	new Array();
	
	for(var i = 0; searchVendorData != null && i < searchVendorData.length; i++)
		{
			var i_contractor_id	=	searchVendorData[i].getValue('custrecord_stvd_contractor');
			
			if(a_contractor_list.indexOf(i_contractor_id) == -1)
				{
					a_contractor_list.push(i_contractor_id);
				}
		}

	return a_contractor_list;
}

//Get Allocation Data
function getResourceAllocationsForEmployees(a_employee_ids, d_start_date, d_end_date)
{
	// Store resource allocations for project and employee
	var a_resource_allocations = new Array();
	
	// Get Resource allocations for this week
	var filters = new Array();
	filters[0]	=	new nlobjSearchFilter('resource', null, 'anyof', a_employee_ids);
	//filters[1]	=	new nlobjSearchFilter('project', null, 'anyof', i_project_id);
	filters[1]	=	new nlobjSearchFilter('startdate', null, 'onorbefore', d_end_date);
	filters[2]	=	new nlobjSearchFilter('enddate', null, 'onorafter', d_start_date);
	//filters[3]	=	new nlobjSearchFilter('billable', null, 'is', 'T');

	var columns = new Array();
	columns[0]	= new nlobjSearchColumn('custeventbstartdate');
	columns[1]	= new nlobjSearchColumn('custeventbenddate');
	columns[2]	= new nlobjSearchColumn('custeventrbillable');
	columns[3]  = new nlobjSearchColumn('custevent3');
	columns[4]	= new nlobjSearchColumn('custevent_otrate');
	columns[5]  = new nlobjSearchColumn('project');
	columns[6]	= new nlobjSearchColumn('startdate');
	columns[7]	= new nlobjSearchColumn('enddate');
	columns[8]	= new nlobjSearchColumn('internalid');
	columns[9]	= new nlobjSearchColumn('custentity_t_and_m_monthly', 'job');
	columns[10]	= new nlobjSearchColumn('custevent_monthly_rate');
	columns[11] = new nlobjSearchColumn('custentity_vertical', 'job');
	columns[12] = new nlobjSearchColumn('entityid', 'job');
	columns[13] = new nlobjSearchColumn('jobname', 'job');
	columns[14] = new nlobjSearchColumn('department', 'employee');
	columns[15] = new nlobjSearchColumn('customer', 'job');
	columns[16] = new nlobjSearchColumn('resource');
	columns[17]	= new nlobjSearchColumn('subsidiary', 'employee');
	columns[18]	= new nlobjSearchColumn('custentityproject_holiday', 'job');
	
	var search_results = searchRecord('resourceallocation', null, filters, columns);
	
	var objData	=	new Object();
	
	for(var i = 0; i < a_employee_ids.length; i++)
		{
			objData[a_employee_ids[i]]	=	{'allocation': new Array(), 'startdate': null, 'enddate': null};
		}
	
	if(search_results != null && search_results.length > 0)
	{
		for(var i_search_indx = 0; i_search_indx < search_results.length; i_search_indx++)
			{
				var i_employee_id	=	search_results[i_search_indx].getValue('resource');
			
				var i_project_id = search_results[i_search_indx].getValue('project');
				
				//var recResourceAllocation = nlapiLoadRecord('resourceallocation', search_results[i_search_indx].getValue('internalid'));
				
				var resource_allocation_start_date = new Date(search_results[i_search_indx].getValue('startdate'));
				var resource_allocation_end_date   = new Date(search_results[i_search_indx].getValue('enddate'));
				var billing_start_date	=	nlapiStringToDate(search_results[i_search_indx].getValue('custeventbstartdate'), 'date');
				var billing_end_date	=	nlapiStringToDate(search_results[i_search_indx].getValue('custeventbenddate'), 'date');
				var is_resource_billable	= search_results[i_search_indx].getValue('custeventrbillable');
				var stRate					= search_results[i_search_indx].getValue('custevent3');
				var otRate					= search_results[i_search_indx].getValue('custevent_otrate');
				var is_monthly_billing	=	search_results[i_search_indx].getValue('custentity_t_and_m_monthly', 'job');
				var f_monthly_rate	=	parseFloat(search_results[i_search_indx].getValue('custevent_monthly_rate'));
				var i_project_vertical = search_results[i_search_indx].getValue('custentity_vertical', 'job');
				var i_project_name_id	=	search_results[i_search_indx].getValue('entityid', 'job');
				var s_project_name	=	search_results[i_search_indx].getValue('jobname', 'job');
				var i_customer_id	=	search_results[i_search_indx].getValue('customer', 'job');
				var s_customer_name	=	search_results[i_search_indx].getText('customer', 'job');
				var i_practice		=	search_results[i_search_indx].getValue('department', 'employee');
				var f_percent		=	search_results[i_search_indx].getValue('percentoftime');
				var i_subsidiary	=	search_results[i_search_indx].getValue('subsidiary', 'employee');
				var i_project_holiday_type	=	search_results[i_search_indx].getValue('custentityproject_holiday', 'job');
				
				objData[i_employee_id].allocation.push({
															'project_id': i_project_id,
															'start_date':resource_allocation_start_date,
															'end_date':resource_allocation_end_date,
															'billing_start_date':billing_start_date,
															'billing_end_date':billing_end_date,
															'is_billable': is_resource_billable,
															'st_rate': stRate,
															'ot_rate': otRate,
															'is_monthly_billing': is_monthly_billing,
															'monthly_rate': f_monthly_rate,
															'vertical': i_project_vertical,
															'project_description': getProjectDescription(i_project_name_id, s_project_name),
															'customer_id': i_customer_id,
															'customer': s_customer_name,
															'practice': i_practice,
															'percent': f_percent,
															'employee_subsidiary': i_subsidiary,
															'project_holiday_type': i_project_holiday_type
														});
				
				if(objData[i_employee_id].startdate == null)
					{
						objData[i_employee_id].startdate	=	resource_allocation_start_date;
					}
				else if(resource_allocation_start_date < objData[i_employee_id].startdate)
					{
						objData[i_employee_id].startdate	=	resource_allocation_start_date;
					}
				
				if(objData[i_employee_id].enddate == null)
					{
						objData[i_employee_id].enddate		=	resource_allocation_end_date;
					}
				else if(resource_allocation_end_date > objData[i_employee_id].enddate)
					{
						objData[i_employee_id].enddate		=	resource_allocation_end_date;
					}
			}
	}
	else
	{
		a_resource_allocations = new Array();
	}
	
	return objData;
}

//Get Project Description text
function getProjectDescription(s_project_id, s_project_name)
{
	return s_project_id + ' ' + s_project_name;
}

//Get Timesheet Data
function getTimesheetDataForEmployees(a_employee_ids, d_start_date, d_end_date)
{
	
	//var a_billed_time_entries	=	getBilledTimeEntriesArray(d_start_date, d_end_date, '', i_employee_id);
	
	var filters	=	new Array();
	filters[0]	=	new nlobjSearchFilter('employee', null, 'anyof', a_employee_ids);
	filters[1] = new nlobjSearchFilter('type', null, 'anyof', 'A');
	filters[2] = new nlobjSearchFilter('date', null, 'within', d_start_date, d_end_date);
	filters[3] = new nlobjSearchFilter('approvalstatus', null, 'anyof', '3');	
	
	var columns = new Array();
	columns[0]	= new nlobjSearchColumn('internalid');
	columns[1]	= new nlobjSearchColumn('employee');
	columns[2]	= new nlobjSearchColumn('customer','job');
	columns[3]	= new nlobjSearchColumn('custcolprj_name');
	columns[4]	= new nlobjSearchColumn('internalid', 'item');
	columns[5]	= new nlobjSearchColumn('durationdecimal');
	columns[6] = new nlobjSearchColumn('custentity_pofrom', 'employee');
	columns[7] = new nlobjSearchColumn('custentity_poto', 'employee');
	columns[8] = new nlobjSearchColumn('department');
	columns[9] = new nlobjSearchColumn('custentity_vertical', 'job');
	columns[10] = new nlobjSearchColumn('date');
	columns[11]	= new nlobjSearchColumn('internalid', 'job');
	columns[12]	= new nlobjSearchColumn('casetaskevent');
	
	var searchResults	=	searchRecord('timebill', null, filters, columns);
	
	var o_timesheet_data	=	new Object();
	
	for(var i = 0; i < a_employee_ids.length; i++)
		{
			o_timesheet_data[a_employee_ids[i]]	=	new Array();
		}
	
	for(var i = 0; searchResults != null && i < searchResults.length; i++)
		{			
			var i_time_entry_id	=	searchResults[i].getValue(columns[0]);
			var i_employee_id	=	searchResults[i].getValue(columns[1]);
			var i_project_id	=	searchResults[i].getValue(columns[11]);
			var i_item			=	searchResults[i].getValue(columns[4]);
			var f_duration		=	searchResults[i].getValue(columns[5]);
			var d_date			=	nlapiStringToDate(searchResults[i].getValue(columns[10]), 'date');
			var s_project_task	=	searchResults[i].getText(columns[12]);
			
			o_timesheet_data[i_employee_id].push({'id': i_time_entry_id, 'project': i_project_id, 'item': i_item, 'duration': f_duration, 'date': d_date, 'project_task': s_project_task});
		}
	
	return o_timesheet_data;
}

//Get project for date
function getAllocationForDate(a_allocations, o_date)
{
	var allocationsFound	=	0;
	var o_allocation	=	null;
	
	for(var i = 0; i < a_allocations.length; i++)
		{
			if(o_date >= a_allocations[i].start_date && o_date <= a_allocations[i].end_date)
				{
					allocationsFound	=	allocationsFound + 1;
					
					o_allocation	=	a_allocations[i];
				}
		}	
	
	return {'allocation_count': allocationsFound, 'allocation': o_allocation};
	
}

function getAmount(i_rate_type, i_working_day_count, i_working_hours, d_start_date, d_end_date, f_pay_rate)
{
	if(i_rate_type	==	3)
		{
			return Math.round(f_pay_rate	*	i_working_day_count/getDayCount(d_start_date, d_end_date));	
		}
	else
		{
			return Math.round(f_pay_rate	*	i_working_hours);
		}
}

function calculate_non_billable_holiday(d_Calculated_Start_Date,
        d_Calculated_End_Date, employee, project)
{// fun start

	var Holiday_Array = new Array();
	var i_Project_Holiday;
	
	var customer	=	null;	
	
	i_Project_Holiday = employee.project_holiday_type;
	customer	=	employee.customer;
		
	// company holiday=1
	// customer holiday=2;
	if (_logValidation(i_Project_Holiday)) {// 1 if start
		if (i_Project_Holiday == 1) {// if start
			// nlapiLogExecution('DEBUG','calculate_non_billable_holiday','d_Calculated_Start_Date',d_Calculated_Start_Date);
			// nlapiLogExecution('DEBUG','calculate_non_billable_holiday','d_Calculated_End_Date',d_Calculated_End_Date);
			// ---------------load company holiday
			// record------------------------
			var com_filter = new Array();
			com_filter[0] = new nlobjSearchFilter('custrecord_date', null,
			        'within', d_Calculated_Start_Date, d_Calculated_End_Date);
			com_filter[1] = new nlobjSearchFilter('custrecordsubsidiary', null,
			        'is', employee.subsidiary);

			var com_column = new Array();
			com_column[0] = new nlobjSearchColumn('custrecord_date');

			var o_Search_Company_Holiday = nlapiSearchRecord(
			        'customrecord_holiday',
			        'customsearch_company_holiday_search', com_filter, null);
			// nlapiLogExecution('DEBUG','calculate_non_billable_holiday','o_Search_Company_Holiday.length'+o_Search_Company_Holiday.length);

			if (_logValidation(o_Search_Company_Holiday)) {// if start
				// nlapiLogExecution('DEBUG','calculate_non_billable_holiday','o_Search_Company_Holiday.length'+o_Search_Company_Holiday.length);
				for (var j = 0; j < o_Search_Company_Holiday.length; j++) {// for
					// start

					var a_search_transaction_result = o_Search_Company_Holiday[j];
					var columns = a_search_transaction_result.getAllColumns();

					var columnLen = columns.length;
					var d_Company_Holiday_Date = a_search_transaction_result
					        .getValue(columns[0]);

					// var d_Company_Holiday_Date=
					// o_Search_Company_Holiday[j].getValue('custrecord_date');
					// nlapiLogExecution('DEBUG','calculate_non_billable_holiday','d_Company_Holiday_Date'+d_Company_Holiday_Date);
					Holiday_Array[j] = d_Company_Holiday_Date;

				}// for close

			}// if close
		}// if close
		else {// else start
			// ---------------load the customer holiday
			// record-------------------
			// nlapiLogExecution('DEBUG','calculate_non_billable_holiday','customer'+customer);
			var cust_filter = new Array();
			cust_filter[0] = new nlobjSearchFilter('custrecordholidaydate',
			        null, 'within', d_Calculated_Start_Date,
			        d_Calculated_End_Date);
			cust_filter[1] = new nlobjSearchFilter(
			        'custrecordcustomersubsidiary', null, 'is',
			        employee.subsidiary);
			cust_filter[2] = new nlobjSearchFilter('custrecord13', null, 'is',
			        customer);

			var cust_column = new Array();
			cust_column[0] = new nlobjSearchColumn('custrecordholidaydate');

			var o_Search_Customer_Holiday = nlapiSearchRecord(
			        'customrecordcustomerholiday',
			        'customsearch_customer_holiday', cust_filter, null);

			if (_logValidation(o_Search_Customer_Holiday)) {// if start
				// nlapiLogExecution('DEBUG','calculate_non_billable_holiday','o_Search_Customer_Holiday.length'+o_Search_Customer_Holiday.length);
				for (var j = 0; j < o_Search_Customer_Holiday.length; j++) {// for
					// start

					var a_search_transaction_result = o_Search_Customer_Holiday[j];
					var columns = a_search_transaction_result.getAllColumns();

					var columnLen = columns.length;
					var d_Customer_Holiday_Date = a_search_transaction_result
					        .getValue(columns[0]);

					// var d_Customer_Holiday_Date=
					// o_Search_Customer_Holiday[j].getValue('custrecordholidaydate');
					// nlapiLogExecution('DEBUG','calculate_non_billable_holiday','d_Customer_Holiday_Date'+d_Customer_Holiday_Date);
					Holiday_Array[j] = d_Customer_Holiday_Date;

				}// for close

			}// if close
			// ---------------------------------------------------------------
		}// else close
	}// 1 if close
	// --------------------------------------------------------------------------------
	return Holiday_Array;

}// fun close

function _logValidation(value) {
	if (value != 'null' && value != null && value != null && value != ''
	        && value != undefined && value != undefined && value != 'undefined'
	        && value != 'undefined' && value != 'NaN' && value != NaN) {
		return true;
	} else {
		return false;
	}
}

//Get Day Count
function getDayCount(d_start_date, d_end_date)
{
	var oneDay = 24*60*60*1000; // hours*minutes*seconds*milliseconds
	
	var d_calculate_allocated_days_end_date = d_end_date;
	
	var d_calculate_allocated_days_start_date = d_start_date;
	
	return (Math.round(Math.abs((d_calculate_allocated_days_end_date.getTime() - d_calculate_allocated_days_start_date.getTime())/(oneDay))) + 1);
}

function yieldScript(currentContext) {

	if (currentContext.getRemainingUsage() <= 900) {
		nlapiLogExecution('AUDIT', 'API Limit Exceeded');
		var state = nlapiYieldScript();

		if (state.status == "FAILURE") {
			nlapiLogExecution('ERROR', 'Yield Script Failed', 'Reason : '
			        + state.reason + ' / Size : ' + state.size);
			return false;
		} else if (state.status == "RESUME") {
			nlapiLogExecution('AUDIT', 'Script Resumed');
		}
	}
}

function formatDate(d_date)
{
	var a_months	=	['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
	
	return	d_date.getDate() + '-' + a_months[d_date.getMonth()] + '-' + d_date.getFullYear();
}