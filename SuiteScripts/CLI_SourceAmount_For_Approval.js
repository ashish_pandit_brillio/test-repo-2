/**
 * @author Jayesh
 */

function custom_approval(i_rcrd_id)
{
	try
	{
		var o_check_obj = nlapiLoadRecord('check',parseInt(i_rcrd_id),{recordmode: 'dynamic'});
		var i_line_count = o_check_obj.getLineItemCount('expense');
		for(var i_index=1; i_index<=i_line_count; i_index++)
		{
			var f_approval_amount = o_check_obj.getLineItemValue('expense','custcol_maker_checker_approval_amount',i_index);
			
			o_check_obj.selectLineItem('expense',i_index);
			o_check_obj.setCurrentLineItemValue('expense','amount',f_approval_amount);
			o_check_obj.setCurrentLineItemValue('expense','custcol_maker_checker_approval_amount',0);
			o_check_obj.setCurrentLineItemValue('expense','custcol_makerchecker_approval_taxamnt',0);
			o_check_obj.commitLineItem('expense');
		}
		
		var i_line_count_item = o_check_obj.getLineItemCount('item');
		for(var i_index=1; i_index<=i_line_count_item; i_index++)
		{
			var f_approval_amount = o_check_obj.getLineItemValue('item','custcol_maker_checker_approval_amount',i_index);
			
			o_check_obj.selectLineItem('item',i_index);
			o_check_obj.setCurrentLineItemValue('item','amount',f_approval_amount);
			o_check_obj.setCurrentLineItemValue('item','custcol_maker_checker_approval_amount',0);
			o_check_obj.setCurrentLineItemValue('item','custcol_makerchecker_approval_taxamnt',0);
			o_check_obj.commitLineItem('item');
		}
		
		o_check_obj.setFieldValue('custbody_maker_checker_approval_status',2);
		
		var i_submitted_recrd = nlapiSubmitRecord(o_check_obj,true,true);
		
		window.location = nlapiResolveURL('RECORD','check',parseInt(i_rcrd_id),'VIEW');
		
	}
	catch(err)
	{
		alert('ERROR MESSAGE:- '+err.message);
	}
}