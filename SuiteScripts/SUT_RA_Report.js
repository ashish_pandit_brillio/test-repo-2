// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
/*
   	Script Name:
	Author:
	Company:
	Date:
	Description:


	Script Modification Log:

	-- Date --			-- Modified By --				--Requested By--				-- Description --



Below is a summary of the process controls enforced by this script file.  The control logic is described
more fully, below, in the appropriate function headers and code blocks.


     SUITELET
		- suiteletFunction(request, response)


     SUB-FUNCTIONS
		- The following sub-functions are called by the above core functions in order to maintain code
            modularization:

               - NOT USED

*/
}
// END SCRIPT DESCRIPTION BLOCK  ====================================



// BEGIN GLOBAL VARIABLE BLOCK  =====================================
{
	//  Initialize any Global Variables, in particular, debugging variables...




}
// END GLOBAL VARIABLE BLOCK  =======================================





// BEGIN SUITELET ==================================================

function CreateReport(request, response)
{

	/*  Suitelet:

          - EXPLAIN THE PURPOSE OF THIS FUNCTION

		-


		FIELDS USED:

          --Field Name--				--ID--


	*/



	//  LOCAL VARIABLES



    //  SUITELET CODE BODY

           var fromDate=request.getParameter('fromdate');
			var toDate=request.getParameter('todate');
			nlapiLogExecution('DEBUG','In Post','fromDate=='+fromDate);
			nlapiLogExecution('DEBUG','In Post','toDate=='+toDate);

			var no_of_days=getDatediffIndays(fromDate,toDate)
			var no_of_weekdays=getWeekend(fromDate,toDate)
			var totalWorkingDay=parseFloat(no_of_days)-parseFloat(no_of_weekdays)
			nlapiLogExecution('DEBUG','In Post','no_of_days=='+no_of_days+'-no_of_weekdays=='+no_of_weekdays+'-totalWorkingDay=='+totalWorkingDay);

			var totalNoofhrs=parseFloat(totalWorkingDay)*8
			nlapiLogExecution('DEBUG','In Post','totalNoofhrs=='+totalNoofhrs);



			//====================================================================================================
			var WORDhtml= '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:ss="urn:schemas-microsoft-com:office:spreadsheet" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40">'+
	 				'<head>'+
	 				'<meta http-equiv=Content-Type content="text/html; charset=windows-1252"/>'+
                    '<meta name=ProgId content=Excel.Sheet/>'+
                    '<meta name=Generator content="Microsoft Excel 11"/>'+
                    '<!--[if gte mso 9]><xml>'+
                    '<x:excelworkbook>'+
                    '<x:excelworksheets>'+
                    '<x:excelworksheet=sheet1>'+
                    '<x:name>** GOLTAN FILE**</x:name>'+
                    '<x:worksheetoptions>'+
                    '<x:selected></x:selected>'+
                    '<x:freezepanes></x:freezepanes>'+
                    '<x:frozennosplit></x:frozennosplit>'+
                    '<x:splithorizontal>** FROZEN ROWS + 1 **</x:splithorizontal>'+
                    '<x:toprowbottompane>** FROZEN ROWS + 1 **</x:toprowbottompane>'+
                    '<x:splitvertical>** FROZEN COLUMNS + 1 **</x:splitvertical>'+
                    '<x:leftcolumnrightpane>** FROZEN COLUMNS + 1**</x:leftcolumnrightpane>'+
   				    '<x:activepane>0</x:activepane>'+// 0
   				    '<x:panes>'+
     				'<x:pane>'+
     				'<x:number>3</x:number>'+
      				'</x:pane>'+
     				'<x:pane>'+
     			    '<x:number>1</x:number>'+
     			    '</x:pane>'+
   				    '<x:pane>'+
     				'<x:number>2</x:number>'+
                    '</x:pane>'+
                    '<x:pane>'+
                    '<x:number>0</x:number>'+//1
                    '</x:pane>'+
                    '</x:panes>'+
                    '<x:protectcontents>False</x:protectcontents>'+
                    '<x:protectobjects>False</x:protectobjects>'+
                    '<x:protectscenarios>False</x:protectscenarios>'+
                    '</x:worksheetoptions>'+
                    '</x:excelworksheet>'+
					'</x:excelworksheets>'+
                    '<x:protectstructure>False</x:protectstructure>'+
                    '<x:protectwindows>False</x:protectwindows>'+
					'</x:excelworkbook>'+

                    // '<style>'+


					//-----------------------worksheet add

                    '<x:excelworkbook>'+
                    '<x:excelworksheets>'+
					'<x:excelworksheet=sheet2>'+
                    '<x:name>**FILE**</x:name>'+
                    '<x:worksheetoptions>'+
                    '<x:selected></x:selected>'+
                    '<x:freezepanes></x:freezepanes>'+
                    '<x:frozennosplit></x:frozennosplit>'+
                    '<x:splithorizontal>** FROZEN ROWS + 1 **</x:splithorizontal>'+
                    '<x:toprowbottompane>** FROZEN ROWS + 1 **</x:toprowbottompane>'+
                    '<x:splitvertical>** FROZEN COLUMNS + 1 **</x:splitvertical>'+
                    '<x:leftcolumnrightpane>** FROZEN COLUMNS + 1**</x:leftcolumnrightpane>'+
   				    '<x:activepane>0</x:activepane>'+// 0
   				    '<x:panes>'+
     				'<x:pane>'+
     				'<x:number>3</x:number>'+
      				'</x:pane>'+
     				'<x:pane>'+
     			    '<x:number>1</x:number>'+
     			    '</x:pane>'+
   				    '<x:pane>'+
     				'<x:number>2</x:number>'+
                    '</x:pane>'+
                    '<x:pane>'+
                    '<x:number>0</x:number>'+//1
                    '</x:pane>'+
                    '</x:panes>'+
                    '<x:protectcontents>False</x:protectcontents>'+
                    '<x:protectobjects>False</x:protectobjects>'+
                    '<x:protectscenarios>False</x:protectscenarios>'+
                    '</x:worksheetoptions>'+
                    '</x:excelworksheet>'+
					'</x:excelworksheets>'+
                    '<x:protectstructure>False</x:protectstructure>'+
                    '<x:protectwindows>False</x:protectwindows>'+
					//worksheett end

					//-------------------------------------new one




					'</x:Worksheet ss:Name="Sheet2">'+
					'</x:Table ss:ExpandedColumnCount="1" ss:ExpandedRowCount="1" x:FullColumns="1" x:FullRows="1">'+
					'</x:Row>'+
					'</x:Cell><Data ss:Type="Number" >1</Data></Cell>'+
					'</x:/Row>'+
					'</x:/Table>'+
					'</x:WorksheetOptions xmlns="urn:schemas-microsoft-com:office:excel">'+
					'</x:ProtectObjects>False</ProtectObjects>'+
					'</x:ProtectScenarios>False</ProtectScenarios>'+
					'</x:/WorksheetOptions>'+
					'</x:/Worksheet>'+
					//-------------------------------aad 2nd sheet
					 /*
					' var xls = new ActiveXObject ( "Excel.Application" )'
					 'var newBook = xls.Workbooks.Add;'
						'newBook.Worksheets.Add;'
						'newBook.Worksheets(1).Activate;'

						'ws1 = newBook.Worksheets(1);'
					*/

						//-------------------------------------
                    '</x:excelworkbook>'+
                    '</xml><![endif]-->'+
                     '<style>'+
					'p.MsoFooter, li.MsoFooter, div.MsoFooter'+
					'{ margin:0in; margin-bottom:.0001pt; mso-pagination:widow-orphan; tab-stops:center 3.0in right 6.0in; font-size:12.0pt;}'+
					'<style>'+

					'<!-- /* Style Definitions */'+

					'@page Section1'+
					'{ size:8.5in 11.0in; margin:1.9cm 1.27cm 1.9cm 1.27cm; mso-header-margin:.5in; mso-footer-margin:.5in; mso-title-page:yes; mso-header: h1; mso-footer: f1; mso-first-header: fh1; mso-first-footer: ff1; mso-paper-source:0;}'+

					'div.Section1'+
					'{ page:Section1;}'+

					'table#hrdftrtbl'+
					'{ margin:0in 0in 0in 900in; width:1px; height:1px; overflow:hidden; font-family:Arial;}-->'+

					'</style>'+


					'</head>'+


					'<body>'+

					'<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">'+

                     '<hr>'+
	                 '<p><center>'+
		             //'<h1>Overview</h1>'+
		//'<A HREF="#table0">Description (Detegasa)</A><br>'+

		//'<A HREF="#table3">T&amp;C</A><br>'+

	'</center></p>'+
'<hr>'+
//'<A NAME="table0"><h1>Sheet 1: </h1></A>'+
'<table align="left" cellspacing="0" border="1">'+

	'<tr>'+
    '<td  >'+
 	//'<img  alt="logo" src="" height="90" width="110"   ></img>'+
	'</td>'+
	'</tr>'+

	'<tr>'+
	    '<td height="20" align="left" valign=bottom><font color="#000000"><br></font></td>'+
		'<td align="left" valign=bottom><font color="#000000"><br></font></td>'+
		'<td align="left" valign=bottom><font color="#000000"><br></font></td>'+
		'<td align="left" valign=bottom><font color="#000000"><br></font></td>'+
		'<td align="right" valign=bottom sdnum="1033;0;_(* #,##0.00_);_(* (#,##0.00);_(* &quot;-&quot;??_);_(@_)"><b><font color="#00oo00"> From Date: </font></b></td>'+
		'<td align="right" valign=bottom sdval="41803" sdnum="1033;1033;[$-409]D-MMM-YYYY;@"><font color="#000000">From Date</font></td>'+
	'</tr>'+
	'<tr>'+
		'<td height="20" align="left" valign=bottom><font color="#000000"><br></font></td>'+
		'<td align="left" valign=bottom><font color="#000000"><br></font></td>'+
		'<td align="left" valign=bottom><font color="#000000"><br></font></td>'+
		'<td align="left" valign=bottom><font color="#000000"><br></font></td>'+
		'<td align="right" valign=bottom sdnum="1033;0;_(* #,##0.00_);_(* (#,##0.00);_(* &quot;-&quot;??_);_(@_)"><b><font color="#000000"> To Date: </font></b></td>'+
		'<td align="right" valign=bottom><font color="#000000">To Date</font></td>'+
	'</tr>'+

	/*
	'<tr>'+
		'<td height="20" align="left" valign=bottom><font color="#000000"><br></font></td>'+
		'<td align="left" valign=bottom><font color="#000000"><br></font></td>'+
		'<td align="left" valign=bottom><font color="#000000"><br></font></td>'+
		'<td align="left" valign=bottom><font color="#000000"><br></font></td>'+
		'<td align="right" valign=bottom sdnum="1033;0;_(* #,##0.00_);_(* (#,##0.00);_(* &quot;-&quot;??_);_(@_)"><b><font color="#000000"> Goltens Prod. Code:  </font></b></td>'+
		'<td align="right" valign=bottom sdval="3310" sdnum="1033;"><font color="#000000">3310</font></td>'+
	'</tr>'+ */

	'<tr>'+
		'<td height="20" align="left" valign=bottom><font color="#000000"><br></font></td>'+
		'<td align="left" valign=bottom><font color="#000000"><br></font></td>'+
		'<td align="left" valign=bottom><font color="#000000"><br></font></td>'+
		'<td align="left" valign=bottom><font color="#000000"><br></font></td>'+
		'<td align="right" valign=bottom sdnum="1033;0;_(* #,##0.00_);_(* (#,##0.00);_(* &quot;-&quot;??_);_(@_)"><b><font color="#000000"> Quoted By: </font></b></td>'+
		'<td align="right" valign=bottom><font color="#000000">quated_by</font></td>'+
	'</tr>'+
	/*'<tr>'+
		'<td height="20" align="left" valign=bottom><b><font color="#000000">Goltens Address:</font></b></td>'+
		'<td align="left" valign=bottom><font color="#000000">6A Benoi Road,</font></td>'+
		'<td align="left" valign=bottom><font color="#000000"><br></font></td>'+
		'<td align="left" valign=bottom><font color="#000000"><br></font></td>'+
		'<td align="right" valign=bottom sdnum="1033;0;_(* #,##0.00_);_(* (#,##0.00);_(* &quot;-&quot;??_);_(@_)"><b><font color="#000000"> Email: </font></b></td>'+
		'<td align="left" valign=bottom><u><font color="#0000FF"><a href="mailto:ron.chen@goltens.com">cust_email </a></font></u></td>'+
	'</tr>'+

	   '<tr>'+
		'<td height="20" align="left" valign=bottom><b><font color="#000000"></font></b></td>'+
		'<td align="left" valign=bottom><font color="#000000"></font></td>'+
		'<td align="left" valign=bottom><font color="#000000"><br></font></td>'+
		'<td align="left" valign=bottom><font color="#000000"><br></font></td>'+
		'<td align="right" valign=bottom sdnum="1033;0;_(* #,##0.00_);_(* (#,##0.00);_(* &quot;-&quot;??_);_(@_)"><b><font color="#000000"> Product No: </font></b></td>'+
		'<td align="left" valign=bottom><u><font color="#0000FF"></font></u></td>'+
	'</tr>'+
	    '<tr>'+
		'<td height="40" align="left" valign=bottom><font color="#000000"><br></font></td>'+
		'<td align="left" valign=bottom><font color="#000000"></font></td>'+
		'<td align="left" valign=bottom><font color="#000000"><br></font></td>'+
		'<td align="left" valign=bottom><font color="#000000"><br></font></td>'+
		'<td align="right" valign=bottom sdnum="1033;0;_(* #,##0.00_);_(* (#,##0.00);_(* &quot;-&quot;??_);_(@_)"><b><font color="#000000"> Vessel Name / Hull No.: </font></b></td>'+
		'<td align="left" valign=bottom><font color="#000000">vessel_name<br></font></td>'+
	'</tr>'+
	'<tr>'+
		'<td height="20" align="left" valign=bottom><font color="#000000"><br></font></td>'+
		'<td align="left" valign=bottom><font color="#000000"><br></font></td>'+
		'<td align="left" valign=bottom><font color="#000000"><br></font></td>'+
		'<td align="left" valign=bottom><font color="#000000"><br></font></td>'+
		'<td align="right" valign=bottom sdnum="1033;0;_(* #,##0.00_);_(* (#,##0.00);_(* &quot;-&quot;??_);_(@_)"><b><font color="#000000"> Customer Ref.: </font></b></td>'+
		'<td align="left" valign=bottom><font color="#000000"><br></font></td>'+
	'</tr>'+
	'<tr>'+
		'<td height="20" align="left" valign=bottom><b><font color="#000000">Customer Name:</font></b></td>'+
		'<td align="left" valign=bottom><font color="#000000">cust_name</font></td>'+
		'<td align="left" valign=bottom><font color="#000000"><br></font></td>'+
		'<td align="left" valign=bottom><font color="#000000"><br></font></td>'+
		'<td align="left" valign=bottom><font color="#000000"><br></font></td>'+
		'<td align="left" valign=bottom><font color="#000000"><br></font></td>'+
	'</tr>'+
	'<tr>'+
		'<td height="20" align="left" valign=bottom><b><font color="#000000">Customer Address: </font></b></td>'+
		'<td align="left" valign=bottom><font color="#000000">cust_add</font></td>'+
		'<td align="left" valign=bottom><font color="#000000"><br></font></td>'+
		'<td align="left" valign=bottom><font color="#000000"><br></font></td>'+
		'<td align="left" valign=bottom><font color="#000000"><br></font></td>'+
		'<td align="left" valign=bottom><font color="#000000"><br></font></td>'+
	'</tr>'+
	'<tr>'+
		'<td height="20" align="left" valign=bottom><font color="#000000"><br></font></td>'+
		'<td align="left" valign=bottom><font color="#000000"></font></td>'+
		'<td align="left" valign=bottom><font color="#000000"><br></font></td>'+
		'<td align="left" valign=bottom><font color="#000000"><br></font></td>'+
		'<td align="left" valign=bottom><font color="#000000"><br></font></td>'+
		'<td align="left" valign=bottom><font color="#000000"><br></font></td>'+
	'</tr>'+
	'<tr>'+
		'<td height="20" align="left" valign=bottom><font color="#000000"><br></font></td>'+
		'<td align="left" valign=bottom><font color="#000000"><br></font></td>'+
		'<td align="left" valign=bottom><font color="#000000"><br></font></td>'+
		'<td align="left" valign=bottom><font color="#000000"><br></font></td>'+
		'<td align="left" valign=bottom><font color="#000000"><br></font></td>'+
		'<td align="left" valign=bottom><font color="#000000"><br></font></td>'+
	'</tr>'+
	'<tr>'+
		'<td height="20" align="left" valign=bottom><b><font color="#000000">Attention To:</font></b></td>'+
		'<td align="left" valign=bottom><font color="#000000">cust_atten</font></td>'+
		'<td align="left" valign=bottom><font color="#000000"><br></font></td>'+
		'<td align="left" valign=bottom><font color="#000000"><br></font></td>'+
		'<td align="left" valign=bottom><font color="#000000"><br></font></td>'+
		'<td align="left" valign=bottom><font color="#000000"><br></font></td>'+
	'</tr>'+
	'<tr>'+
		'<td height="20" align="left" valign=bottom><b><font color="#000000">Contact No.:</font></b></td>'+
		'<td align="left" valign=bottom><font color="#000000">cust_ph</font></td>'+
		'<td align="left" valign=bottom><font color="#000000"><br></font></td>'+
		'<td align="left" valign=bottom><font color="#000000"><br></font></td>'+
		'<td align="left" valign=bottom><font color="#000000"><br></font></td>'+
		'<td align="left" valign=bottom><font color="#000000"><br></font></td>'+
	'</tr>'+

	'<tr>'+
		'<td height="20" align="left" valign=bottom><b><font color="#000000">Email :</font></b></td>'+
		'<td align="left" valign=bottom><u><font color="#0000FF">cust_conct_email</font></u></td>'+
		'<td align="left" valign=bottom><font color="#000000"><br></font></td>'+
		'<td align="left" valign=bottom><font color="#000000"><br></font></td>'+
		'<td align="left" valign=bottom><font color="#000000"><br></font></td>'+
		'<td align="left" valign=bottom><font color="#000000"><br></font></td>'+
	'</tr>'+

	'<tr>'+
		'<td height="20" align="left" valign=bottom><font color="#000000"><br></font></td>'+
		'<td align="left" valign=bottom><font color="#000000"><br></font></td>'+
		'<td align="left" valign=bottom><font color="#000000"><br></font></td>'+
		'<td align="left" valign=bottom><font color="#000000"><br></font></td>'+
		'<td align="left" valign=bottom><font color="#000000"><br></font></td>'+
		'<td align="left" valign=bottom><font color="#000000"><br></font></td>'+
	'</tr>'+
	'<tr>'+
		'<td height="20" align="left" valign=bottom><b><font color="#000000">Subject :</font></b></td>'+
		'<td align="left" valign=bottom><font color="#000000">excel_sub</font></td>'+
		'<td align="left" valign=bottom><font color="#000000"><br></font></td>'+
		'<td align="left" valign=bottom><font color="#000000"><br></font></td>'+
		'<td align="left" valign=bottom><font color="#000000"><br></font></td>'+
		'<td align="left" valign=bottom><font color="#000000"><br></font></td>'+
	'</tr>'+*/
	'<tr>'+
		'<td height="20" align="left" valign=bottom><font color="#000000"><br></font></td>'+
		'<td align="left" valign=bottom><font color="#000000"><br></font></td>'+
		'<td align="left" valign=bottom><font color="#000000"><br></font></td>'+
		'<td align="left" valign=bottom><font color="#000000"><br></font></td>'+
		'<td align="left" valign=bottom><font color="#000000"><br></font></td>'+
		'<td align="left" valign=bottom><font color="#000000"><br></font></td>'+
	'</tr>'+
	'<tr>'+
		'<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" height="20" align="left" valign=bottom bgcolor="#BFBFBF"><b><font color="#000000">Date</font></b></td>'+
		'<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=bottom bgcolor="#BFBFBF"><b><font color="#000000">Employee Name.</font></b></td>'+
		'<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=bottom bgcolor="#BFBFBF"><b><font color="#000000">Hrs</font></b></td>'+
		'<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=bottom bgcolor="#BFBFBF"><b><font color="#000000">Avalible Hrs</font></b></td>'+
		'<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=bottom bgcolor="#BFBFBF"><b><font color="#000000">Avalible Per</font></b></td>'+
	//	'<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=bottom bgcolor="#BFBFBF"><b><font color="#000000">Uom</font></b></td>'+
		'<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=bottom bgcolor="#BFBFBF" sdnum="1033;0;_(* #,##0.00_);_(* (#,##0.00);_(* &quot;-&quot;??_);_(@_)"><b><font color="#000000"> Release Date </font></b></td>'+
		'<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=bottom bgcolor="#BFBFBF" sdnum="1033;0;_(* #,##0.00_);_(* (#,##0.00);_(* &quot;-&quot;??_);_(@_)"><b><font color="#000000"> Hire Date </font></b></td>'+
		'<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=bottom bgcolor="#BFBFBF"><b><font color="#000000">Subsidairy</font></b></td>'+
		'<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=bottom bgcolor="#BFBFBF"><b><font color="#000000">Project</font></b></td>'+
		'<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=bottom bgcolor="#BFBFBF"><b><font color="#000000">Avalible Hrs</font></b></td>'+
	    '</tr>'





			//-----------------------------------------------------------------------------------------------------
			var report_Filters= new Array();
			var search = nlapiLoadSearch(null, 'customsearch_salaried_new_time_s_2_3_2');
			var columns = search.getColumns();
			var filters = search.getFilters();

			report_Filters[0]=new nlobjSearchFilter('date',null,'onorafter',fromDate)
			report_Filters[1]=new nlobjSearchFilter('date',null,'onorbefore',toDate)
			search.addFilters(report_Filters);
			nlapiLogExecution('DEBUG','In report','columns=='+columns);
			nlapiLogExecution('DEBUG','In report','filters=='+filters);
			if(search!=null)
			{
				nlapiLogExecution('DEBUG','In report','s=='+search.length);
				nlapiLogExecution('DEBUG','In report','s c=='+columns.length);
			}

			var resultSet = search.runSearch();
			nlapiLogExecution('DEBUG','In report','resultSet=='+resultSet);
			nlapiLogExecution('DEBUG','In report','resultSet legth=='+resultSet.length);

			var i=1
			resultSet.forEachResult(function(searchResult)
			{
			//sum += parseFloat(searchResult.getValue('total'));   // process the search result
			nlapiLogExecution('DEBUG','In report','Record Start==========================================='+i++)
			var employee=searchResult.getValue('employee',null,'group')
			var employee=searchResult.getText('employee',null,'group')
			//nlapiLogExecution('DEBUG','In report','employee=='+employee);
			var durationdecimal=searchResult.getValue('durationdecimal',null,'sum')
			//nlapiLogExecution('DEBUG','In report','durationdecimal=='+durationdecimal);

			var date=searchResult.getValue('date',null,'group')
			//nlapiLogExecution('DEBUG','In report','date=='+date);

			var releasedate=searchResult.getValue('releasedate','employee','group')
			//nlapiLogExecution('DEBUG','In report','releasedate=='+releasedate);
			var hiredate=searchResult.getValue('hiredate','employee','group')
			//nlapiLogExecution('DEBUG','In report','hiredate=='+hiredate);

			var subsidiary=searchResult.getValue('subsidiary','employee','group')
			var subsidiary=searchResult.getText('subsidiary','employee','group')

			var project=searchResult.getValue('altname','job','group')
			nlapiLogExecution('DEBUG','In report','employee=='+employee+'  durationdecimal=='+durationdecimal+ ' subsidiary=='+subsidiary+'-project-'+project+'---i---'+i++);
			nlapiLogExecution('DEBUG','In report','searchResult=='+searchResult.length)

			//------------------------------------------------------------------------------------
			    var fromDate=request.getParameter('fromdate');
				var toDate=request.getParameter('todate');
				nlapiLogExecution('DEBUG','In Post','fromDate=='+fromDate+'==toDate=='+toDate);
				nlapiLogExecution('DEBUG','In Post','hiredate=='+hiredate+'releasedate=='+releasedate);
				hiredateD=nlapiStringToDate(hiredate)
				releasedateD=nlapiStringToDate(releasedate)
				fromDateD=nlapiStringToDate(fromDate)
				toDateD=nlapiStringToDate(toDate)

				nlapiLogExecution('DEBUG','In Post','fromDate=='+fromDateD+'==toDate=='+toDateD);
				nlapiLogExecution('DEBUG','In Post','hiredate=='+hiredateD+'releasedate=='+releasedateD);
				if(hiredate!=null)
				{
					if(Date.parse(hiredateD)>=Date.parse(fromDateD)&&Date.parse(hiredateD)<=Date.parse(toDateD))
					{
						fromDate=hiredate
					}
					else
					{
						fromDate=fromDate
					}
				}
				if(releasedate!=null)
				{
					if(Date.parse(releasedateD)<=Date.parse(toDateD) && Date.parse(releasedateD)>=Date.parse(fromDateD))
					{
						toDate=releasedate
					}
					else
					{
						toDate=toDate
					}
				}
				nlapiLogExecution('DEBUG','In Post=====','fromDate=='+fromDate+'==toDate=='+toDate);
				var no_of_days=getDatediffIndays(fromDate,toDate)
				var no_of_weekdays=getWeekend(fromDate,toDate)
				var totalWorkingDay=parseFloat(no_of_days)-parseFloat(no_of_weekdays)
				nlapiLogExecution('DEBUG','In Post','no_of_days=='+no_of_days+'-no_of_weekdays=='+no_of_weekdays+'-totalWorkingDay=='+totalWorkingDay);

				var totalNoofhrs=parseFloat(totalWorkingDay)*8
				nlapiLogExecution('DEBUG','In Post','totalNoofhrs=='+totalNoofhrs);
			//------------------------------------------------------------------------------------

			/*var result = searchResult
					//nlapiLogExecution('DEBUG', 'getCOASalessalary data', 'results= '+results.length);
					var columns = result.getAllColumns();
					var columnLen = columns.length;
					// loop through all columns and pull UI labels, formulas, and functions that have
					// been specified for columns
					//nlapiLogExecution('DEBUG', 'getCOASalesExpense data', 'columnLen= '+columnLen);
					for (var j = 0; j < columnLen; j++)
					{
						var column = columns[j];
						var label = column.getLabel();
						var formula = column.getFormula();
						var functionName = column.getFunction();
						var value = result.getValue(column);
						nlapiLogExecution('DEBUG', 'getCOASalesExpense data', 'column= '+column+'-label-'+label+'-value-'+value);
					}*/

		WORDhtml=  WORDhtml+	'<tr>'+
		'<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" height="20" align="left" valign=bottom ><font color="#000000">'+date+'</font></td>'+
		'<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=bottom ><font color="#000000">'+employee+'</font></td>'+
		'<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=bottom ><font color="#000000">'+durationdecimal+'</font></td>'+
		'<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=bottom ><font color="#000000">'+totalNoofhrs+'</font></td>'+
		'<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=bottom ><font color="#000000">'+(parseFloat(durationdecimal)*100)/parseFloat(totalNoofhrs) +'</font></td>'+
	//	'<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=bottom ><b><font color="#000000">Uom</font></b></td>'+
		'<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=bottom  sdnum="1033;0;_(* #,##0.00_);_(* (#,##0.00);_(* &quot;-&quot;??_);_(@_)"><font color="#000000"> '+releasedate+' </font></td>'+
		'<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=bottom  sdnum="1033;0;_(* #,##0.00_);_(* (#,##0.00);_(* &quot;-&quot;??_);_(@_)"><font color="#000000"> '+hiredate+' </font></td>'+
		'<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=bottom ><font color="#000000">'+subsidiary+'</font></td>'+
		'<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=bottom ><font color="#000000">'+project+'</font></td>'+
	    '</tr>'

			return true;                // return true to keep iterating
			});


		//alert('Sum: ' + sum)
	 //------------------------------------------------------------------------------------------------------------------------------------
WORDhtml=  WORDhtml+
'</table>'+
'<br clear=left>'+
'<!-- ************************************************************************** -->'+


	      '</body></html>';

	      var Datetime = new Date();
		      var pdfName = 'RA DATA -' + Datetime+'.xls';
			  var id =  nlapiCreateFile(pdfName, 'PLAINTEXT',WORDhtml);
				  id.setFolder(5135);
			  var fileId=nlapiSubmitFile(id);
				  nlapiLogExecution('DEBUG', 'suitlet_excel', 'id of file'   + fileId);

     		  var fileobj=nlapiLoadFile(fileId);

		 	  var Datetime = new Date();
		      var pdfName = 'RA DATA -' + Datetime+'.xls';
		          response.setContentType('WORD', pdfName,'inline');
			      response.write(fileobj.getValue());
			      nlapiLogExecution('DEBUG', 'suitlet_excel', ' PDF Name-->  ' + pdfName);
			//====================================================================================================



}

// END SUITELET ====================================================




// BEGIN OBJECT CALLED/INVOKING FUNCTION ===================================================
{
function getDatediffIndays(startDate, endDate)
{
    var one_day=1000*60*60*24;
    var fromDate = startDate;
    var toDate=endDate

    var date1 = nlapiStringToDate(fromDate);

    var date2 = nlapiStringToDate(toDate);

    var date3=(date2-date1)/one_day;

    return (date3+1);
}

function getWeekend(startDate,endDate)
{
var i_no_of_sat = 0 ;
var i_no_of_sun = 0 ;

nlapiLogExecution('DEBUG','startDate',startDate);
nlapiLogExecution('DEBUG','endDate',endDate);
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 var date_format = checkDateFormat();

var startDate_1 = startDate
 var endDate_1 = endDate

 startDate = nlapiStringToDate(startDate);
 endDate = nlapiStringToDate(endDate);

 var i_count_day = startDate.getDate();

 var i_count_last_day = endDate.getDate();

 i_month = startDate.getMonth()+1;

 i_year = startDate.getFullYear();

var d_f = new Date();
var getTot = getDatediffIndays(startDate_1, endDate_1); //Get total days in a month

var sat = new Array();   //Declaring array for inserting Saturdays
var sun = new Array();   //Declaring array for inserting Sundays

for(var i=i_count_day;i<=i_count_last_day;i++)
{    //looping through days in month

  if (date_format == 'YYYY-MM-DD')
  {
   var newDate = i_year + '-' + i_month + '-' + i;
  }
  if (date_format == 'DD/MM/YYYY')
  {
    var newDate = i + '/' + i_month + '/' + i_year;
  }
  if (date_format == 'MM/DD/YYYY')
  {
    var newDate = i_month + '/' + i + '/' + i_year;
  }

    newDate = nlapiStringToDate(newDate);

    if(newDate.getDay()==0)
 {   //if Sunday
        sat.push(i);
  i_no_of_sat++;
    }
    if(newDate.getDay()==6)
 {   //if Saturday
        sun.push(i);
  i_no_of_sun++;
    }


}

 var i_total_days_sat_sun = parseInt(i_no_of_sat)+parseInt(i_no_of_sun);
 return i_total_days_sat_sun ;
}


function checkDateFormat()
{
    var context = nlapiGetContext();

	var dateFormatPref = context.getPreference('dateformat');

    return dateFormatPref;
}


}
// END OBJECT CALLED/INVOKING FUNCTION =====================================================
