/**
 * A screen for BO team to edit allocations related to any project
 * 
 * Version    Date            Author           Remarks
 * 1.00       13 Jul 2015     Nitish Mishra
 *
 */

/**
 * @param {nlobjRequest}
 *            request Request object
 * @param {nlobjResponse}
 *            response Response object
 * @returns {Void} Any output is written via response object
 */
function suitelet(request, response) {
	try {
		var method = request.getMethod();

		if (method == 'GET') {
			// var projectId = request.getParameter('job');
			var caseId = request.getParameter('case');

			if (caseId) {
				createCaseAllocationScreen(caseId);
			} else {
				createSelectCaseScreen(request);
			}
		} else if (method == 'POST') {
			submitRequest(request);
		} else {
			throw "Invalid Method";
		}
	} catch (err) {
		nlapiLogExecution('ERROR', 'suitelet', err);
		displayErrorForm(err, 'Resource Allocation');
	}
}

// --------------------------------------------Select Project Screen
function createSelectCaseScreen(request) {
	try {
		var form = nlapiCreateForm('Select any Case');

		var list = form.addSubList('custpage_case', 'staticlist', 'Cases');
		list.addField('supportcase', 'text', 'Case ID');
		list.addField('project', 'text', 'Project');
		list.addField('employee', 'text', 'Employee');
		list.addField('title', 'text', 'Title');
		list.addField('priority', 'text', 'Priority');
		list.addField('status', 'text', 'Status');
		list.addField('startdate', 'text', 'Incident Date');

		var searchResult = nlapiSearchRecord('supportcase',
		        constant.SavedSearch.BO_Open_Case_Search);

		var data = [];

		if (searchResult) {
			nlapiLogExecution('debug', 'no. of cases', searchResult.length);
			var url = nlapiResolveURL('SUITELET',
			        'customscript_sut_bo_allocation_changes',
			        'customdeploy_sut_bo_allocation_changes')
			        + "&unlayered=T&case=";

			searchResult.forEach(function(cases) {
				data.push({
				    supportcase : "<a href='" + url + cases.getId() + "'>"
				            + cases.getValue('casenumber') + "</a>",
				    project : cases.getText('custevent_case_project'),
				    employee : cases.getText('company'),
				    title : cases.getValue('title'),
				    priority : cases.getText('priority'),
				    status : cases.getText('status'),
				    startdate : cases.getValue('startdate')
				});
			});
		}

		list.setLineItemValues(data);
		response.writePage(form);
	} catch (err) {
		nlapiLogExecution('ERROR', 'createSelectCaseScreen', err);
		throw err;
	}
}

function csOpenCase() {
	try {
		var caseId = nlapiGetFieldValue('custpage_case');

		if (caseId) {
			var url = nlapiResolveURL('SUITELET',
			        'customscript_sut_bo_allocation_changes',
			        'customdeploy_sut_bo_allocation_changes')
			        + "&case=" + caseId;
			window.location = url;
		} else {
			alert('Please select any case');
		}
	} catch (err) {
		alert(err.message);
	}
}
// -------------------------------------------------

// ------------------------------------- Allocation Screen
function createCaseAllocationScreen(caseId) {
	try {
		var caseRecord = nlapiLoadRecord('supportcase', caseId);

		// case notes
		var caseNotes = caseRecord.getFieldValue('custevent_case_notes');
		var caseAllocations = JSON.parse(caseNotes);

		nlapiLogExecution('debug', 'project comment', caseAllocations.ac);
		nlapiLogExecution('debug', 'case length', caseAllocations.ar.length);

		var projectId = caseRecord.getFieldValue('custevent_case_project');

		var form = nlapiCreateForm('Resource Allocation Case Screen');

		form.addFieldGroup('custpage_group_1', 'Case');

		form.setScript('customscript_cs_sut_bo_allocation_case');
		form.addButton('custpage_btn_close', 'Close', 'closeCase');
		// form.addButton('custpage_btn_reject', 'Reject', 'rejectCase');
		form.addButton('custpage_btn_on_hold', 'On Hold', 'onHoldCase');

		form.addField('custpage_case', 'select', 'Case', 'supportcase',
		        'custpage_group_1').setDisplayType('inline').setDefaultValue(
		        caseId);

		form.addField('custpage_case_status', 'text', 'Status', null,
		        'custpage_group_1').setDisplayType('inline').setDefaultValue(
		        caseRecord.getFieldText('status'));

		form.addField('custpage_job', 'select', 'Project', 'job',
		        'custpage_group_1').setDisplayType('inline').setDefaultValue(
		        projectId);

		// get all project allocations
		var allocation_details = getProjectAllocationDetails(projectId,
		        caseAllocations.ar);

		form.addFieldGroup('custpage_group_2', 'Details');

		// present allocation
		var present_allocation_list = form.addSubList('custpage_present_list',
		        'list', 'Allocations');
		form.addField('custpage_pm_comment', 'textarea', 'User Comments', null,
		        'custpage_group_2').setDisplayType('inline').setDefaultValue(
		        caseAllocations.ac);
		form.addField('custpage_ops_comment', 'textarea', 'Your Comments',
		        null, 'custpage_group_2');

		addSublistColumns(present_allocation_list);
		present_allocation_list
		        .setLineItemValues(allocation_details.PresentAllocation);

		// past allocation
		// var past_allocation_list = form.addSubList('custpage_past_list',
		// 'list', 'Past Allocations');
		// addSublistColumns(past_allocation_list);
		// past_allocation_list
		// .setLineItemValues(allocation_details.PastAllocation);

		response.writePage(form);
	} catch (err) {
		nlapiLogExecution('ERROR', 'createCaseAllocationScreen', err);
		throw err;
	}
}

function addSublistColumns(sublist) {
	sublist.addField('view_edit', 'text', 'View/Edit').setDisplayType('inline');
	sublist.addField('resource', 'text', 'Resource').setDisplayType('inline');
	sublist.addField('internalid', 'text', 'Allocation').setDisplayType(
	        'hidden');
	sublist.addField('resourcename', 'text', 'Resource Name').setDisplayType(
	        'hidden');
	sublist.addField('startdate', 'date', 'Start Date')
	        .setDisplayType('inline');
	sublist.addField('enddate', 'date', 'End Date').setDisplayType('inline');
	sublist.addField('isbillable', 'checkbox', 'Billable').setDisplayType(
	        'inline');
	sublist.addField('billrate', 'currency', 'Bill Rate').setDisplayType(
	        'inline');
	sublist.addField('remarks', 'textarea', 'Project Managers Remarks');
	sublist.addField('boremarks', 'textarea', 'Ops Remarks').setDisplayType(
	        'entry').setDisplaySize(20, 2);
}

function getProjectAllocationDetails(projectIds, caseAllocations) {
	try {

		var allocationSearch = searchRecord('resourceallocation', null, [
		        new nlobjSearchFilter('project', null, 'anyof', projectIds),
		        new nlobjSearchFilter('enddate', null, 'notbefore', 'today') ],
		        [ new nlobjSearchColumn('resource'),
		                new nlobjSearchColumn('project'),
		                new nlobjSearchColumn('startdate'),
		                new nlobjSearchColumn('enddate'),
		                new nlobjSearchColumn('custeventrbillable'),
		                new nlobjSearchColumn('percentoftime'),
		                new nlobjSearchColumn('custeventbstartdate'),
		                new nlobjSearchColumn('custeventbenddate'),
		                new nlobjSearchColumn('custeventwlocation'),
		                new nlobjSearchColumn('custevent4'),
		                new nlobjSearchColumn('custevent_practice') ]);

		var presentAllocation = [];
		var pastAllocation = [];
		var futureAllocation = [];

		if (allocationSearch) {
			var currentDate = new Date();

			allocationSearch
			        .forEach(function(allocation) {
				        var endDate = nlapiStringToDate(allocation
				                .getValue('enddate'));
				        var startDate = nlapiStringToDate(allocation
				                .getValue('startdate'));
				        var currentAllocation = {
				            view_edit : "<a target='_blank' href='"
				                    + nlapiResolveURL('RECORD',
				                            'resourceallocation', allocation
				                                    .getId(), 'VIEW')
				                    + "'>View</a> | <a target='_blank' href='"
				                    + nlapiResolveURL('RECORD',
				                            'resourceallocation', allocation
				                                    .getId(), 'EDIT')
				                    + "'>Edit</a>",
				            resource : "<a target='_blank' href='"
				                    + nlapiResolveURL('RECORD', 'employee',
				                            allocation.getValue('resource'),
				                            'EDIT') + "'>"
				                    + allocation.getText('resource') + "</a>",
				            internalid : allocation.getId(),
				            resourcename : allocation.getText('resource'),
				            startdate : allocation.getValue('startdate'),
				            enddate : allocation.getValue('enddate'),
				            isbillable : allocation
				                    .getValue('custeventrbillable'),
				            billrate : allocation.getValue('custevent4'),
				            percent : allocation.getValue('percentoftime'),
				            location : allocation
				                    .getValue('custeventwlocation'),
				            billingstartdate : allocation
				                    .getValue('custeventbstartdate'),
				            billingenddate : allocation
				                    .getValue('custeventbenddate'),
				            practice : allocation
				                    .getValue('custevent_practice'),
				            remarks : ''
				        };

				        caseAllocations.forEach(function(rec) {
					        if (rec.i == allocation.getId()) {
						        currentAllocation.remarks = rec.r;
					        }
				        });

				        // if (currentDate > endDate) {
				        // pastAllocation.push(currentAllocation);
				        // } else if (currentDate < startDate) {
				        // futureAllocation.push(currentAllocation);
				        // } else {
				        presentAllocation.push(currentAllocation);
				        // }
			        });
		}

		return {
		    PresentAllocation : presentAllocation,
		    // FutureAllocation : futureAllocation,
		    PastAllocation : pastAllocation
		};
	} catch (err) {
		nlapiLogExecution('ERROR', 'getProjectAllocationDetails', err);
		throw err;
	}
}

// --------------- Client Script
function onHoldCase() {
	try {
		var caseCommentsArray = [];
		var project_comments = nlapiGetFieldValue('custpage_ops_comment');

		var mailContent = "";

		if (project_comments) {
			mailContent += "Comments : " + project_comments + "\n";
		}

		mailContent += getSublistRemarks('custpage_present_list', 'Allocation',
		        caseCommentsArray);
		if (mailContent) {
			mailContent += "\n";
		}

		if (mailContent) {

			var projectDetails = nlapiLookupField('job',
			        nlapiGetFieldValue('custpage_job'),
			        [ 'entityid', 'altname' ]);
			mailContent = "Project : " + projectDetails.entityid + " "
			        + projectDetails.altname + "<br/>" + mailContent;

			updateCaseRecord(nlapiGetFieldValue('custpage_case'), 6,
			        mailContent, {
			            ar : caseCommentsArray,
			            ac : project_comments
			        });
		}

	} catch (err) {
		alert(err.message);
	}
}

function getSublistRemarks(sublistId, allocationType, caseCommentsArray) {
	try {
		var lineCount = nlapiGetLineItemCount(sublistId);
		var anyRemarks = false;
		var text = "" + allocationType + " :- \n";

		for (var linenum = 1; linenum <= lineCount; linenum++) {
			var remarks = nlapiGetLineItemValue(sublistId, 'boremarks', linenum);

			if (remarks) {
				anyRemarks = true;
				text += nlapiGetLineItemValue(sublistId, 'resourcename',
				        linenum);
				text += " : " + remarks;
				text += "\n";
				caseCommentsArray.push({
				    i : nlapiGetLineItemValue(sublistId, 'resource', linenum),
				    r : remarks
				});
			}
		}

		if (!anyRemarks) {
			text = "";
		}

		return text;
	} catch (err) {
		throw err;
	}
}

function closeCase() {
	try {
		var caseCommentsArray = [];
		var project_comments = nlapiGetFieldValue('custpage_ops_comment');

		var mailContent = "";

		if (project_comments) {
			mailContent += "Comments : " + project_comments + "\n";
		}

		mailContent += getSublistRemarks('custpage_present_list', 'Allocation',
		        caseCommentsArray);
		if (mailContent) {
			mailContent += "\n";
		}

		if (mailContent) {

			var projectDetails = nlapiLookupField('job',
			        nlapiGetFieldValue('custpage_job'),
			        [ 'entityid', 'altname' ]);
			mailContent += "Case Closed\n ";
			mailContent = "Project : " + projectDetails.entityid + " "
			        + projectDetails.altname + "\n" + mailContent;

			updateCaseRecord(nlapiGetFieldValue('custpage_case'), 5,
			        mailContent, {
			            ar : caseCommentsArray,
			            ac : project_comments
			        });
		}

	} catch (err) {
		alert(err.message);
	}
}

function updateCaseRecord(caseId, newStatus, mailContent, caseCommentsObject) {
	try {
		var caseRecord = nlapiLoadRecord('supportcase', caseId);
		caseRecord.setFieldValue('status', newStatus);

		if (caseCommentsObject)
			caseRecord.setFieldValue('custevent_bo_remarks', JSON
			        .stringify(caseCommentsObject));

		if (mailContent)
			caseRecord.setFieldValue('outgoingmessage', mailContent);

		var caseId = nlapiSubmitRecord(caseRecord);
		alert('Case Record Submitted');

		window.location = nlapiResolveURL('SUITELET',
		        'customscript_sut_bo_allocation_changes',
		        'customdeploy_sut_bo_allocation_changes');
	} catch (err) {
		throw err;
	}
}