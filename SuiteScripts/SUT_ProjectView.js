// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
/*
   	Script Name : SUT_PROJECTVIEW.js
	Author      : ASHISH PANDIT
	Date        : 20 DEC 2018
    Description : Suitelet to display Project Details 


	Script Modification Log:

	-- Date --			-- Modified By --				--Requested By--				-- Description --
 


Below is a summary of the process controls enforced by this script file.  The control logic is described
more fully, below, in the appropriate function headers and code blocks.


     SCHEDULED FUNCTION
		- scheduledFunction(type)


     SUB-FUNCTIONS
		- The following sub-functions are called by the above core functions in order to maintain code
            modularization:

               - NOT USED

*/
}
// END SCRIPT DESCRIPTION BLOCK  ====================================

/*Global Variable*/
var projectCounter = 0;
var counter = 0;
var a_JSON = [];
var a_dataArray = new Array();
var a_frfArray = new Array();
var a_frfDataArray = new Array();
var i_servingNotice = 0;
var i_no_of_frf = 0;
var i_no_of_rrf = 0;
/*Image Links */ 

var calender = '<img src="https://system.netsuite.com/core/media/media.nl?id=1257247&c=3883006&h=ca132a145f59230970fd" class="calender_img">';
var team = '<img src="https://system.netsuite.com/core/media/media.nl?id=1257287&c=3883006&h=85ea67f2e30665186392" class="calender_img">';
var exiting = '<img src="https://system.na1.netsuite.com/core/media/media.nl?id=1257252&c=3883006&h=d124e4b81b5a369c8f0d">';
var rampup = '<img src="https://system.na1.netsuite.com/core/media/media.nl?id=1257243&c=3883006&h=c7d23397233058a5e902">';
var star = '<img src="https://system.na1.netsuite.com/core/media/media.nl?id=1257278&c=3883006&h=ac2e9cfe25078fa374d8">';


/*Suitlet Main function*/
function suiteletPeopleView(request, response)
{
	try
	{
		var user = nlapiGetUser();
		var projectId = request.getParameter('custpage_projectid'); 
		nlapiLogExecution('ERROR','Script Parameter ',projectId);
		var oppId = request.getParameter('custpage_oppid');
		nlapiLogExecution('Debug','oppId ',oppId);
		var frfSearch;
		if(projectId)
		{
			frfSearch = getFrfDetails(projectId);
			var searchResult = getAllocationDetails(projectId,oppId);
			nlapiLogExecution('Debug','projectId ',projectId);
			nlapiLogExecution('ERROR','frfSearch ',frfSearch);
			var s_projectName = nlapiLookupField('job',projectId,'companyname');
		}
		else if(oppId)
		{
			frfSearch = getFrfDetails(oppId);
		}
		
		var values = new Array();
        values['{homeicon}']=nlapiResolveURL('SUITELET','customscript_fuel_sut_fulfillment_dash','customdeploy_fuel_sut_fulfillment_dash');
		values['{data1}'] = JSON.stringify(a_dataArray);
      	//nlapiLogExecution('Debug','a_dataArray ',JSON.stringify(a_dataArray));
		values['{user}'] = nlapiGetContext().getName();
		values['{designation}'] = nlapiLookupField('employee',user,'title');
		values['{serving_notice_period}'] = i_servingNotice;
		values['{rrf_no}'] = i_no_of_rrf;
		nlapiLogExecution('Debug','i_no_of_frf '+i_no_of_frf,'i_no_of_rrf '+i_no_of_rrf);
		values['{frf_no}'] = i_no_of_frf; 
        values['{home_page}'] = nlapiResolveURL('SUITELET','customscript_fuel_sut_fulfillment_dash','customdeploy_fuel_sut_fulfillment_dash');
        values['{home_page1}'] = nlapiResolveURL('SUITELET','customscript_fuel_sut_fulfillment_dash','customdeploy_fuel_sut_fulfillment_dash');
		var createFrfUrl = nlapiResolveURL('SUITELET','customscript_fuel_sut_create_frf','customdeploy_fuel_sut_create_frf');
		values['{newFRF}'] = createFrfUrl+'&s_project='+projectId+'&s_opp='+oppId;
      //  values['{new_frf}'] ='<a class="create-frf" href="/app/site/hosting/scriptlet.nl?script=1747&deploy=1&s_project='+i_project+' class="prev-page">';
		if(s_projectName)
		{
			values['{project_name}'] =s_projectName;
		}
		else if(oppId)
		{	
			nlapiLogExecution('Debug','oppId ',oppId);
			values['{project_name}'] = nlapiLookupField('customrecord_sfdc_opportunity_record',oppId,'custrecord_opportunity_name_sfdc');
		}
		
		values['{projectcount}'] = counter;
		
		nlapiLogExecution('ERROR','Gender',nlapiLookupField('employee',user,'gender'));
		
		/*Login user image*/
		if(nlapiLookupField('employee',user,'gender') == 'f'){
		values['{profilepic}'] = '/core/media/media.nl?id=1257303&c=3883006&h=b0e4f8408d1e0d948928';
		}else{
		values['{profilepic}'] = '/core/media/media.nl?id=1257271&c=3883006&h=ad48de7349910d7225a6';
		}
		
		var contents = nlapiLoadFile('1257133').getValue();
		contents = replaceValues(contents, values);
		//response.writeLine(JSON.stringify(arrayObj));
		response.write(contents);
	}
	catch(error)
	{
		nlapiLogExecution('Debug','Error ',error);
	}
}

/** Function to get resource allocation details**/

function getAllocationDetails(projectId,oppId)
{
	var resourceallocationSearch = nlapiSearchRecord("resourceallocation",null,
	[
	   ["project","anyof",projectId], 
	   "AND", 
	   ["enddate","onorafter","today"]
	], 
	[
	   new nlobjSearchColumn("resource"),
	   new nlobjSearchColumn("internalid","employee",null),	   
	   new nlobjSearchColumn("title","employee",null), 
	   new nlobjSearchColumn("custentity_lwd","employee",null), 
	   new nlobjSearchColumn("company"), 
	   new nlobjSearchColumn("startdate"), 
	   new nlobjSearchColumn("enddate"), 
	   new nlobjSearchColumn("custevent_practice")
	]
	);
	if(resourceallocationSearch)
	{
		nlapiLogExecution('Debug','resourceallocationSearch.length ',resourceallocationSearch.length);
		var createFrfUrl = nlapiResolveURL('SUITELET','customscript_fuel_sut_create_frf','customdeploy_fuel_sut_create_frf');
		for(var i=0; i<resourceallocationSearch.length;i++)
		{
			var s_resource = resourceallocationSearch[i].getText('resource');
			//var i_resource = resourceallocationSearch[i].getValue('resource');
			var i_resource = resourceallocationSearch[i].getValue("internalid","employee",null);
			//nlapiLogExecution('Debug',' i_resource ',i_resource);
			var frfId = '';
			temp = s_resource.indexOf("-");
			if(temp>0)
			{
				var finalPm = s_resource.split("-")[1];
			}
			else{
				var finalPm = s_resource;
			}
			//nlapiLogExecution('AUDIT','temp:',temp);
			var s_designation = resourceallocationSearch[i].getValue("title","employee",null);
			var d_lwd = resourceallocationSearch[i].getValue("custentity_lwd","employee",null);
			var d_startDate = resourceallocationSearch[i].getValue("startdate");
			var d_endDate = resourceallocationSearch[i].getValue("enddate");
			var i_project = resourceallocationSearch[i].getValue("company");
			var n_frf = '';
			
			suggetion ='<a class="create-frf" href="'+createFrfUrl+'&s_resource='+i_resource+'&s_project='+i_project+'&s_opp='+oppId+'" class="prev-page"><button class="btn create-frf-btn">Create FRF</button> </a>';
			var s_criticality = '<label class="switch">'+
			'                      <input type="checkbox" unchecked="" disabled>'+
			'                      <span class="slider round"></span>'+
			'                    </label>';
	
			if(d_lwd)
			{
				d_lwd = "Notice";  
				
				if(a_frfArray.indexOf(i_resource)==-1)
				{
					i_servingNotice++;
				//suggetion ='<a href="/app/site/hosting/scriptlet.nl?script=1747&deploy=1&s_resource='+i_resource+'&s_project='+i_project+'" class="prev-page"><button class="btn create-frf-btn">Create FRF</button> </a>';
				s_criticality = '<label class="switch">'+
				'                      <input type="checkbox" checked="" disabled>'+
				'                      <span class="slider round"></span>'+
				'                    </label>';
				}
				else
				{
					nlapiLogExecution('Debug','a_frfDataArray '+a_frfDataArray,'a_frfDataArray length '+a_frfDataArray.length);
					for(var i_counter = 0; i_counter < a_frfDataArray.length; i_counter++) 
					{
					   if((a_frfDataArray[i_counter].resourceId == i_resource)&& !a_frfDataArray[i_counter].frfId) 
					   {
							suggetion ='<a href="/app/site/hosting/scriptlet.nl?script=1745&deploy=1&i_frfnumber='+a_frfDataArray[i_counter].internalId+'">'+a_frfDataArray[i_counter].frfId+'</a>';
					   }
					   else
					   {
						   suggetion = a_frfDataArray[i_counter].frfId;
						   i_no_of_frf++;
						   nlapiLogExecution('Debug','i_no_of_frf 1 ',i_no_of_frf);
					   }
					   
					}
					s_criticality = '<label class="switch">'+
					'                      <input type="checkbox" unchecked="" disabled>'+
					'                      <span class="slider round"></span>'+
					'                    </label>';
				}
			}
			else
			{
				//=================================================
				nlapiLogExecution('Debug','a_frfDataArray in show'+JSON.stringify(a_frfDataArray),'a_frfDataArray length '+a_frfDataArray.length);
				if(a_frfArray.indexOf(i_resource)!=-1)
				{
					nlapiLogExecution('Debug','a_frfDataArray.length ',a_frfDataArray.length);
					for(var i_counter = 0; i_counter < a_frfDataArray.length; i_counter++) 
					{
					   nlapiLogExecution('Debug','a_frfDataArray[i_counter].resourceId  '+a_frfDataArray[i_counter].resourceId,'i_resource '+i_resource);
					   if((a_frfDataArray[i_counter].resourceId == i_resource)) 
					   {
							if(a_frfDataArray[i_counter].exthire=='F')
							{
								suggetion = a_frfDataArray[i_counter].frfId;
								nlapiLogExecution('debug','suggetion  ',suggetion);
								i_no_of_frf++;
								nlapiLogExecution('Debug','a_frfDataArray[i_counter].frfId @@ '+a_frfDataArray[i_counter].frfId,'a_frfDataArray[i_counter].exthire '+a_frfDataArray[i_counter].exthire);
							}
							else 
							{
								suggetion = '';
								n_frf = a_frfDataArray[i_counter].frfId;
								i_no_of_rrf++;
								//i_no_of_frf--;
								nlapiLogExecution('Debug','n_frf ',n_frf);
							}
							
					    }
						
					}
					d_lwd = '<select class="select-option" id="mySelect" onchange="myFunction()" >'+
				'                      <option>Allocated</option>'+
				'                      <option selected>Rotation</option>'+
				'                    </select>';
				}
				else
				{
					d_lwd = '<select class="select-option" id="mySelect" onchange="myFunction()" >'+
				'                      <option>Allocated</option>'+
				'                      <option>Rotation</option>'+
				'                    </select>';
				}
				//=================================================
			}
			
			var s_skills = getEmployeeSkills(i_resource);
			a_JSON = {
						//pr_status: counter+1,
						resource: exiting +' '+ '<span title="'+finalPm+'" class= "team_member">'+finalPm+'</span>',
						designation: s_designation +'<br>'+'<span class="people_skills">'+s_skills+'</span>',
						criticality: s_criticality, 
						s_status: d_lwd,
						start_date: calender+d_startDate,
						end_date: calender+d_endDate,
						suggetion: suggetion,
						frf:n_frf,
						star:star
					}
			a_dataArray.push(a_JSON);
			counter++;
		}
	}
	return resourceallocationSearch;
}

function GetTeamSize(i_project) {
	var jobSearch = nlapiSearchRecord("job",null,
			[
			 ["internalid","anyof",i_project],"AND", 
			 ["resourceallocation.startdate","notafter","today"], 
			 "AND", 
			 ["resourceallocation.enddate","notbefore","today"]
			 ], 
			 [
			  new nlobjSearchColumn("resource","resourceAllocation",null)
			  ]
	);
	if (jobSearch) {
		return jobSearch.length;
	}else{
		return 0;
	}
}
function GetEmployeeExist(i_project) {
	Date.prototype.addDays = function(days) {
		var date = new Date(this.valueOf());
		date.setDate(date.getDate() + days);
		return date;
	}
	var date = new Date();
	date.addDays(30);
	var resourceallocationSearch = nlapiSearchRecord("resourceallocation",null,
			[
			 ["job.internalid","anyof",i_project], //5515 project internal ID
			 "AND", 
			 ["employee.custentity_lwd","before",date] // 01/05/2019 calculated dynamically using current date and adding 30 days to it. 
			 ], 
			 [
			  new nlobjSearchColumn("resource"), 
			  new nlobjSearchColumn("custentity_lwd","employee",null)
			  ]
	);
	if (resourceallocationSearch) {
		return resourceallocationSearch.length;
	}else{
      return "";
    }
}
function replaceValues(content, oValues)
{
	for(param in oValues)
	{
		// Replace null values with blank
		var s_value	=	(oValues[param] == null)?'':oValues[param];

		// Replace content
		//content = content.replace(new RegExp('{{' + param + '}}','gi'), s_value);
		content = content.replace(param, s_value);
	}

	return content;
}


/*Function to get FRF details*/
function GetFRF(i_project) {
	nlapiLogExecution('AUDIT', 'i_project ID ', i_project);
	var customrecord_frf_detailsSearch = nlapiSearchRecord("customrecord_frf_details",null,
	[
	   ["custrecord_frf_details_project","anyof",i_project]
	], 
	[
	   new nlobjSearchColumn("custrecord_frf_details_external_hire")
	]
	);
	if(customrecord_frf_detailsSearch){
		return customrecord_frf_detailsSearch.length;
	}
	else{
		return '00';
	}
	
}

function getEmployeeSkills(emp_id)
{
	var customrecord_employee_master_skill_dataSearch = nlapiSearchRecord("customrecord_employee_master_skill_data",null,
	[
	   ["custrecord_employee_skill_updated","anyof",emp_id]
	], 
	[
	   new nlobjSearchColumn("custrecord_employee_skill_updated"), 
	   new nlobjSearchColumn("custrecord_primary_updated"), 
	   new nlobjSearchColumn("custrecord_secondry_updated")
	]
	);
	if(customrecord_employee_master_skill_dataSearch)
	{
		var skills = customrecord_employee_master_skill_dataSearch[0].getText('custrecord_primary_updated');
		return skills;
	}
	else
	{
		return ' ';
	}
}


function getFrfDetails(oppId)
{
	nlapiLogExecution('Debug','oppId ',oppId);
	var customrecord_frf_Search = nlapiSearchRecord("customrecord_frf_details",null,
	[
	[["custrecord_frf_details_project","anyof",oppId],
	"OR",
	["custrecord_frf_details_opp_id","anyof",oppId]], 
    "AND", 
    ["custrecord_frf_details_status","is","F"]
	], 
	[
	   new nlobjSearchColumn("custrecord_frf_details_external_hire"), 
	   new nlobjSearchColumn("custrecord_frf_details_role"), 
	   new nlobjSearchColumn("custrecord_frf_details_critical_role"), 
	   new nlobjSearchColumn("custrecord_frf_details_start_date"), 
	   new nlobjSearchColumn("custrecord_frf_details_end_date"),
	   new nlobjSearchColumn("custrecord_frf_details_primary_skills"),
	   new nlobjSearchColumn("custrecord_frf_emp_notice_rotation"),
	   new nlobjSearchColumn("custrecord_frf_details_frf_number")
	]
	);
	if(customrecord_frf_Search)
	{
		nlapiLogExecution('Debug','customrecord_frf_Search Length ',customrecord_frf_Search.length);
		var frfProgressUrl = nlapiResolveURL('SUITELET','customscript_sut_frf_flow','customdeploysut_frf_flow');
		for(var i=0; i<customrecord_frf_Search.length; i++)
		{
			var internalId = customrecord_frf_Search[i].getId();
			var frfId = customrecord_frf_Search[i].getValue('custrecord_frf_details_frf_number');
			var b_extHire = customrecord_frf_Search[i].getValue('custrecord_frf_details_external_hire');
			var d_startDate = customrecord_frf_Search[i].getValue('custrecord_frf_details_start_date');
			var d_endDate = customrecord_frf_Search[i].getValue('custrecord_frf_details_end_date');
			var s_designation = customrecord_frf_Search[i].getValue('custrecord_frf_details_role');
			var s_skills = customrecord_frf_Search[i].getText('custrecord_frf_details_primary_skills');
			var s_resource = customrecord_frf_Search[i].getValue('custrecord_frf_emp_notice_rotation');
			//nlapiLogExecution('Debug','s_resource   ',s_resource);
			if(s_resource)
			{
				a_frfArray.push(s_resource);
				a_frfDataArray.push({
						resourceId: s_resource,
						frfId: '<a href="'+frfProgressUrl+'&custpage_frfid='+internalId+'">'+frfId+'</a>',
						internalId:internalId,
						exthire:b_extHire
						});
						//i_no_of_frf++;
						nlapiLogExecution('Debug','i_no_of_frf $$',i_no_of_frf);
			}	
			else
			{
				s_resource = 'New Member';
			
				var s_criticality = '<label class="switch">'+
					'                      <input type="checkbox" checked="" disabled>'+
					'                      <span class="slider round"></span>'+
					'                    </label>';
				if(b_extHire=='T')
				{
					a_JSON = {
							//pr_status: counter+1,
							resource: '<span title="'+s_resource+'" class= "team_member">'+s_resource+'</span>',
							designation: s_designation + '<br>'+'<span class="people_skills">'+s_skills+'</span>',
							criticality: s_criticality, 
							s_status: 'Opportunity',
							start_date: calender+d_startDate,
							end_date: calender+d_endDate,
							suggetion: '',
							frf:'<a href="'+frfProgressUrl+'&custpage_frfid='+internalId+'">'+frfId+'</a>',
							star:star
						}
						i_no_of_rrf++;
				}
				else 
				{
					a_JSON = {
							//pr_status: counter+1,
							resource: '<span title="'+s_resource+'" class= "team_member">'+s_resource+'</span>',
							designation: s_designation + '<br>'+'<span class="people_skills">'+s_skills+'</span>',
							criticality: s_criticality, 
							s_status: 'Opportunity',
							start_date: calender+d_startDate,
							end_date: calender+d_endDate,
							suggetion: '<a href="'+frfProgressUrl+'&custpage_frfid='+internalId+'">'+frfId+'</a>',
							frf:'',
							star:star
						}
						i_no_of_frf++;
						nlapiLogExecution('Debug','i_no_of_frf 3',i_no_of_frf);
				}
				
				a_dataArray.push(a_JSON);
				counter++;
			}
		}
		nlapiLogExecution('Debug','a_frfArray  ',a_frfArray);
		return customrecord_frf_Search;
	}
}