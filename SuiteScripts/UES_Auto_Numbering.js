// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
/*
   	Script Name:
	Author:
	Company:
	Date:
	Description:


	Script Modification Log:

	-- Date --			-- Modified By --				--Requested By--				-- Description --
	23-01-2020			Madem Praveena					Deepak/Nihal					Added logic on Before Record load to set WORKLOCATION field values as per "Project Default Location" workflow.


Below is a summary of the process controls enforced by this script file.  The control logic is described
more fully, below, in the appropriate function headers and code blocks.


     BEFORE LOAD
		- beforeLoadRecord_populateWorklocation(type,form)



     BEFORE SUBMIT
		- beforeSubmitRecord(type)


     AFTER SUBMIT
		- afterSubmitRecord(type)



     SUB-FUNCTIONS
		- The following sub-functions are called by the above core functions in order to maintain code
            modularization:

               - NOT USED

*/
}
// END SCRIPT DESCRIPTION BLOCK  ====================================



// BEGIN GLOBAL VARIABLE BLOCK  =====================================
{
	//  Initialize any Global Variables, in particular, debugging variables...




}
// END GLOBAL VARIABLE BLOCK  =======================================





// BEGIN BEFORE LOAD ==================================================

function beforeLoadRecord_populateWorklocation(type,form)
{



	/*  On before load:

          - THE PURPOSE OF THIS FUNCTION set WORKLOCATION field values as per "Project Default Location" workflow

		-


		FIELDS USED:

          --Field Name--				--ID--
		  -WORKLOCATION					custentity_worklocation 


	*/



	//  LOCAL VARIABLES

    //  BEFORE LOAD CODE BODY
if(nlapiGetRecordId()=="199066")// && type=='edit')
	try{
		var i_wrk_loc=nlapiGetFieldValues('custentity_worklocation');//custentity_worklocation//custentity_worklocation_state
		nlapiLogExecution('DEBUG','JSON i_wrk_loc',JSON.stringify(i_wrk_loc));
		if(_nullValidation(i_wrk_loc))//CHECK worklocation is having values or not
		{
			var a_listofworkLocations=["0","2","4","10","238","30000","30020","30","32","109","40","44","47","48"];//(Arizona : 0,British Columbia : 2,California : 4,Georgia : 10,Greater London : 238,Karnataka : 30000,Maharashtra : 30020,New Jersey : 30,New York : 32,Ontario : 109,Rhode Island : 40,Texas : 44,Virginia : 47,Washington : 48)
				try{
					nlapiSetFieldValues('custentity_worklocation',a_listofworkLocations);//custentity_worklocation
				}
				catch(e)
				{
					nlapiLogExecution('DEBUG','ERROR WHILING SEETING WORKLOCATION',e);
				}
		}
}
     catch(e)
				{
					nlapiLogExecution('DEBUG','ERROR WHILING SEETING WORKLOCATION',e);
				}
}

// END BEFORE LOAD ====================================================





// BEGIN BEFORE SUBMIT ================================================

function beforeSubmitRecord(type)
{
    /*  On before submit:

          - PURPOSE
		-

          FIELDS USED:

          --Field Name--				--ID--


    */


    //  LOCAL VARIABLES


    //  BEFORE SUBMIT CODE BODY


	return true;

}

// END BEFORE SUBMIT ==================================================





// BEGIN AFTER SUBMIT =============================================

function afterSubmitRecord_auto_numbering(type)
{
	if(type == 'create')
	{
	var i_record = nlapiGetRecordId();
	var s_recordtype=nlapiGetRecordType();
	nlapiLogExecution('DEBUG', 'afterSubmit_auto_numbering',' Record ID -->' + i_record);
	nlapiLogExecution('DEBUG', 'afterSubmit_auto_numbering',' Record Type -->' + s_recordtype);
	var record_obj = nlapiLoadRecord(s_recordtype,i_record);
	var customer=record_obj.getFieldText('parent');
	nlapiLogExecution('DEBUG', 'afterSubmit_auto_numbering', ' customer -->' + customer);
	
	if(customer!='COIN-01 Collabera Inc')
	{	
	if (customer != null && customer != undefined && customer != '') 
	{
		
		var a_split_array = new Array()
		var a_split_array = customer.split('-');
		var cust_prefix = a_split_array[0];
		if (cust_prefix != null && cust_prefix != undefined && cust_prefix != "") 
		{
			//search and load the serial number generation record object
				var o_serialrecord_obj = search_serial_number();
				if (o_serialrecord_obj != null && o_serialrecord_obj != '' && o_serialrecord_obj != undefined) 
				{
					var sequence_no = o_serialrecord_obj.getFieldValue('custrecord_current_sequence_number');
					if (sequence_no != null && sequence_no != undefined && sequence_no != '')
					{
						nlapiLogExecution('DEBUG', 'afterSubmit_auto_numbering', ' sequence no -->' + sequence_no);
						var i_next_sequence_no = parseInt(sequence_no) + 1;
						if (i_next_sequence_no.toString().length == 1) 
						{
							i_zero_number = '0000'
						}
						if (i_next_sequence_no.toString().length == 2) 
						{
							i_zero_number = '000'
						}
						if (i_next_sequence_no.toString().length == 3) 
						{
							i_zero_number = '00'
						}
						if (i_next_sequence_no.toString().length == 4) 
						{
							i_zero_number = 0
						}
						if (i_next_sequence_no.toString().length == 5) 
						{
							i_zero_number = ''
						}
						o_serialrecord_obj.setFieldValue('custrecord_current_sequence_number', i_next_sequence_no);
						var new_serialnumber = cust_prefix + i_zero_number + i_next_sequence_no;
						nlapiLogExecution('DEBUG', 'afterSubmit_auto_numbering', ' new serial number -->' + new_serialnumber);
						// set the new serial number on the serial number genration record
						o_serialrecord_obj.setFieldValue('custrecord_current_serial_number', new_serialnumber);
						nlapiSubmitRecord(o_serialrecord_obj,true,true);
						
						//set the project id of project record
						record_obj.setFieldValue('entityid', new_serialnumber);
						record_obj.setFieldValue('jobtype',2);
						nlapiSubmitRecord(record_obj,true,true);
					}//Sequence No.
				}//Serial OBJ
			
		}//Customer Prefix
		
	}//Customer
	
	}//Collebra Customer
		
	}//CREATE
	
  
	
  return true;
}


// END AFTER SUBMIT ===============================================





// BEGIN FUNCTION ===================================================
{
	function search_serial_number(){
		var filters = new Array();
		var columns = new Array();
		filters[0] = new nlobjSearchFilter('custrecord_purchase_request_type', null, 'is', 'Project');
		columns[0] = new nlobjSearchColumn('internalid');
		var searchresult = nlapiSearchRecord('customrecord_serial_number_generation', null, filters, columns);
		{
			if (searchresult != null && searchresult != '' && searchresult != undefined) {
				var i_recordID = searchresult[0].getValue('internalid');
				nlapiLogExecution('DEBUG', 'afterSubmit_auto_numbering',' Record ID -->' + i_recordID);
				var o_serialrecord = nlapiLoadRecord('customrecord_serial_number_generation', i_recordID);
				return o_serialrecord;
			}
		}
	}
}
// END FUNCTION =====================================================


// **************TO Check  NULL Values ***********************
function _nullValidation(value) {
    if (value == null || value == 'null' || value == 'NaN' || value == '' || value == undefined || value == '&nbsp;') {
        return true;
    } else {
        return false;
    }
}