//https://system.na1.netsuite.com/app/common/scripting/script.nl?id=869
// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
/*
   	Script Name : SCH_Set_proj_desc_cust_territory_Revenue_JE.js
	Author      : Jayesh Dinde
	Date        : 2 May 2016
    Description : Set project desc, customer name, vertical, practice and territory on JE created using Revenue Recognition option.


	Script Modification Log:

	-- Date --			-- Modified By --				--Requested By--				-- Description --




Below is a summary of the process controls enforced by this script file.  The control logic is described
more fully, below, in the appropriate function headers and code blocks.


     SCHEDULED FUNCTION
		- scheduledFunction(type)


     SUB-FUNCTIONS
		- The following sub-functions are called by the above core functions in order to maintain code
            modularization:

               - NOT USED

*/
}
// END SCRIPT DESCRIPTION BLOCK  ====================================


// BEGIN SCHEDULED FUNCTION =============================================

function set_prod_desc_revenue_je()
{
	try
	{	
		var column_proj_name = new Array();	
	    column_proj_name[0]= new nlobjSearchColumn('entity')
		var search_je = nlapiSearchRecord(null, 'customsearch_revenue_je_without_proj_des', null, column_proj_name);
		if (_logValidation(search_je))
		{
			for (var i = 0; i < search_je.length; i++)
			{
				var submit_flag = 0;
				var revenue_je_rcrdObj = nlapiLoadRecord('journalentry', search_je[i].getId());
				nlapiLogExecution('audit','je internal id:- '+search_je[i].getId());
				if (_logValidation(revenue_je_rcrdObj))
				{
					var is_je_already_processed = revenue_je_rcrdObj.getFieldValue('custbody_je_already_processed');
					if(is_je_already_processed == 'T')
					{
						
					}
					else
					{
						var line_count = revenue_je_rcrdObj.getLineItemCount('line');
						for (var j = 1; j <= line_count; j++)
						{
							var proj_ID = revenue_je_rcrdObj.getLineItemValue('line', 'entity', j);
							if (_logValidation(proj_ID))
							{
								try
								{
									var proj_rcrd_obj = nlapiLoadRecord('job', proj_ID);
									var proj_cust_name = proj_rcrd_obj.getFieldText('parent');
									var proj_cust_id = proj_rcrd_obj.getFieldValue('parent');
									var proj_desc = proj_rcrd_obj.getFieldValue('companyname');
									var proj_id = proj_rcrd_obj.getFieldValue('entityid');
									proj_desc = proj_id + ' ' + proj_desc;
									var proj_vertical = proj_rcrd_obj.getFieldValue('custentity_vertical');
									var proj_practice = proj_rcrd_obj.getFieldValue('custentity_practice');
									if (_logValidation(proj_cust_id)) {
										var proj_cust_territory = nlapiLookupField('customer', proj_cust_id, 'territory');
									}
									var proj_billing_type = proj_rcrd_obj.getFieldText('jobbillingtype');
									if (!_logValidation(proj_billing_type))
										proj_billing_type = '';
										
									var proj_onsite_offsite = proj_rcrd_obj.getFieldText('custentity_deliverymodel');
									if (!_logValidation(proj_onsite_offsite))
										proj_onsite_offsite = '';
									
									revenue_je_rcrdObj.selectLineItem('line', j);
									revenue_je_rcrdObj.setCurrentLineItemValue('line', 'custcolprj_name', proj_desc);
									revenue_je_rcrdObj.setCurrentLineItemValue('line', 'custcolcustcol_temp_customer', proj_cust_name);
									revenue_je_rcrdObj.setCurrentLineItemValue('line', 'custcol_je_region', proj_cust_territory);
									revenue_je_rcrdObj.setCurrentLineItemValue('line', 'class', proj_vertical);
									revenue_je_rcrdObj.setCurrentLineItemValue('line', 'department', proj_practice);
								}
								catch(err)
								{
									var cust_rcrd_obj = nlapiLoadRecord('customer', proj_ID);
									var cust_entityid = cust_rcrd_obj.getFieldValue('entityid');
									var cust_company_name = cust_rcrd_obj.getFieldValue('companyname');
									var cust_name = cust_entityid+' '+cust_company_name;
									var cust_territory = cust_rcrd_obj.getFieldValue('territory');
									
									revenue_je_rcrdObj.selectLineItem('line', j);
									revenue_je_rcrdObj.setCurrentLineItemValue('line', 'custcolcustcol_temp_customer', cust_name);
									revenue_je_rcrdObj.setCurrentLineItemValue('line', 'custcol_je_region', cust_territory);
								}
							
								revenue_je_rcrdObj.setCurrentLineItemValue('line', 'custcol_billing_type', proj_billing_type);
								revenue_je_rcrdObj.setCurrentLineItemValue('line', 'custcol_onsite_offsite', proj_onsite_offsite);
							}
							
							revenue_je_rcrdObj.commitLineItem('line');
							submit_flag = 1;
						}
					}
					
					if(submit_flag == 1)
					{
						revenue_je_rcrdObj.setFieldValue('custbody_je_already_processed','T');
						revenue_je_rcrdObj.setFieldValue('custbody_is_je_updated_for_emp_type','F');
						var je_submitted = nlapiSubmitRecord(revenue_je_rcrdObj, true, true);
						nlapiLogExecution('DEBUG', 'je_submitted:- ',je_submitted);
					}	
				}
			}
		}
	}
	catch(err)
	{
		nlapiLogExecution('ERROR','ERROR MESSAGE:-- ',err);
	}
}

// END SCHEDULED FUNCTION =============================================

function _logValidation(value) 
{
 if(value!='null' && value != null && value != null && value != '' && value != undefined && value != undefined && value != 'undefined' && value!= 'undefined'&& value!= 'NaN' && value != NaN) 
 {
  return true;
 }
 else 
 { 
  return false;
 }
}