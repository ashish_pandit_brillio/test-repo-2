//Nihal Mulani 
//It is Department
//SB - https://3883006-sb1.app.netsuite.com/app/common/scripting/script.nl?id=2018
function afterSubmitCheckFields(type) {
    nlapiLogExecution('Debug', 'type ', type);
    try {
        if (type == 'create') {
            var recordObject = nlapiLoadRecord(nlapiGetRecordType(), nlapiGetRecordId());
            var name = recordObject.getFieldValue('name');
            var custrecord_onsite_practice_head = recordObject.getFieldText('custrecord_onsite_practice_head');
            var custrecord_onsite_practice_head_internalId = recordObject.getFieldValue('custrecord_onsite_practice_head');
            var custrecord_practicehead = recordObject.getFieldText('custrecord_practicehead');
            var custrecord_practicehead_internalId = recordObject.getFieldValue('custrecord_practicehead');
            //var isinactive = recordObject.getFieldValue('isinactive');
            var custrecord_parent_practice = recordObject.getFieldValue('custrecord_parent_practice');

            if (custrecord_parent_practice == "" || custrecord_parent_practice == null) {
                var body = {};
                var method;
                method = "POST";
                body.internalId = nlapiGetRecordId();
                body.name = name || '';
                body.onsiteHead = custrecord_onsite_practice_head || '';

                if (custrecord_onsite_practice_head_internalId) {
                    var onsitepracticeheademail = nlapiLookupField("employee", custrecord_onsite_practice_head_internalId, "email"); //Parent Practice internal Id
                    if (onsitepracticeheademail) {
                        body.onsiteHeadEmailId = onsitepracticeheademail;
                    } else {
                        body.onsiteHeadEmailId = "";
                    }
                } else {
                    body.onsiteHeadEmailId = "";
                }
                body.practiceHead = custrecord_practicehead || '';
                if (custrecord_practicehead_internalId) {
                    var practiceheademail = nlapiLookupField("employee", custrecord_practicehead_internalId, "email"); //Parent Practice internal Id
                    if (practiceheademail) {
                        body.practiceHeadEmailId = practiceheademail;
                    } else {
                        body.practiceHeadEmailId = "";
                    }
                } else {
                    body.practiceHeadEmailId = "";
                }

                //body.isinactive = isinactive;
                nlapiLogExecution("DEBUG", "body create Mode: ", JSON.stringify(body));
                var url = "https://fuelnode1.azurewebsites.net/practice";
                nlapiLogExecution('debug', 'body  ', JSON.stringify(body));
                body = JSON.stringify(body);
                nlapiLogExecution("DEBUG", "response edit Mode:body ", body);
                var response = call_node(url, body, method);
                nlapiLogExecution("DEBUG", "response edit Mode: ", JSON.stringify(response));
            }
        } else if (type == 'edit' || type == 'xedit') {
            var recordObject = nlapiLoadRecord(nlapiGetRecordType(), nlapiGetRecordId());
            var name = recordObject.getFieldValue('name');
            var custrecord_onsite_practice_head = recordObject.getFieldText('custrecord_onsite_practice_head');
            var custrecord_onsite_practice_head_internalId = recordObject.getFieldValue('custrecord_onsite_practice_head');
            var custrecord_practicehead = recordObject.getFieldText('custrecord_practicehead');
            var custrecord_practicehead_internalId = recordObject.getFieldValue('custrecord_practicehead');
            //var isinactive = recordObject.getFieldValue('isinactive');
            var custrecord_parent_practice = recordObject.getFieldValue('custrecord_parent_practice');

            if (custrecord_parent_practice == "" || custrecord_parent_practice == null) {
                var oldRecord = nlapiGetOldRecord();
                var oldRecord_name = oldRecord.getFieldValue('name');
                var oldRecord_custrecord_onsite_practice_head = oldRecord.getFieldText('custrecord_onsite_practice_head');
                var oldRecord_custrecord_practicehead = oldRecord.getFieldText('custrecord_practicehead');
                var oldRecord_isinactive = oldRecord.getFieldValue('isinactive');



                if (name == oldRecord_name && custrecord_onsite_practice_head == oldRecord_custrecord_onsite_practice_head && custrecord_practicehead == oldRecord_custrecord_practicehead && isinactive == oldRecord_isinactive) {
                    nlapiLogExecution('Debug', 'Message', 'Record 4 fields are not updated ');
                } else {
                    var body = {};
                    var method;
                    method = "POST";
                    body.internalId = Number(nlapiGetRecordId());
                    body.name = name || '';
                    body.onsiteHead = custrecord_onsite_practice_head || '';
                    if (custrecord_onsite_practice_head_internalId) {
                        var onsitepracticeheademail = nlapiLookupField("employee", custrecord_onsite_practice_head_internalId, "email"); //Parent Practice internal Id
                        if (onsitepracticeheademail) {
                            body.onsiteHeadEmailId = onsitepracticeheademail;
                        } else {
                            body.onsiteHeadEmailId = "";
                        }
                    } else {
                        body.onsiteHeadEmailId = "";
                    }
                    body.practiceHead = custrecord_practicehead || '';
                    if (custrecord_practicehead_internalId) {
                        var practiceheademail = nlapiLookupField("employee", custrecord_practicehead_internalId, "email"); //Parent Practice internal Id
                        if (practiceheademail) {
                            body.practiceHeadEmailId = practiceheademail;
                        } else {
                            body.practiceHeadEmailId = "";
                        }
                    } else {
                        body.practiceHeadEmailId = "";
                    }
                    //body.isinactive = isinactive;
                    nlapiLogExecution("DEBUG", "body edit Mode: ", JSON.stringify(body));
                    var url = "https://fuelnode1.azurewebsites.net/practice";
                    nlapiLogExecution('debug', 'body  ', JSON.stringify(body));
                    body = JSON.stringify(body);
                    nlapiLogExecution("DEBUG", "response edit Mode:body ", body);
                    var response = call_node(url, body, method);
                    nlapiLogExecution("DEBUG", "response edit Mode: ", JSON.stringify(response));
                }
            }
        }
    } catch (error) {
        nlapiLogExecution('Debug', 'Error ', error);
    }
}

function beforeSubmitCheckFields(type) {
    if (type == 'delete') {
        try {
            var i_recordId = nlapiGetRecordId();
            var i_user = nlapiGetFieldValues('custrecord_fuel_anchor_employee');
            nlapiLogExecution('debug', 'i_user ', i_user);
            var body = {};
            var method = "DELETE";
            var url = " https://fuelnode1.azurewebsites.net/practice/" + i_recordId;
            var response = call_node(url, body, method);
            nlapiLogExecution('debug', 'Delete Mode: url  ', url);
            nlapiLogExecution("DEBUG", "response Delete Mode: ", JSON.stringify(response));
        } catch (error) {
            nlapiLogExecution('Debug', 'Error ', error);
        }
    }
}

//END BEFORE LOAD ====================================================


function _logValidation(value) {
    if (value != null && value != 'null' && value != '' && value != undefined && value != undefined && value != 'undefined' && value.toString() != 'NaN' && value != NaN) {
        return true;
    } else {
        return false;
    }
}
//END FUNCTION =====================================================