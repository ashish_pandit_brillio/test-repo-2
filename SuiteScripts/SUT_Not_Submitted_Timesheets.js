/**
 * Module Description
 * 
 * Version Date Author Remarks 1.00 12 May 2015 nitish.mishra
 * 
 */

var Timesheet_Approval_Status = {
	Approved : '3',
	PendingApproval : '2',
	Rejected : '4',
	Open : '1'
};

/**
 * @param {nlobjRequest}
 *            request Request object
 * @param {nlobjResponse}
 *            response Response object
 * @returns {Void} Any output is written via response object
 */
function suitelet(request, response) {
	try {

		// get employees under the current users
		var currentUser = '2050';
		var startDate = "4/26/2015";
		var endDate = "5/9/2015";

		if (startDate && endDate && currentUser) {
			var start_date_list = [];

			// get all submitted timesheets
			var search_employee_submitted_timesheet = searchRecord(
					'timesheet',
					null,
					[
							new nlobjSearchFilter('timeapprover', 'employee',
									'anyof', currentUser),
							new nlobjSearchFilter(
									'approvalstatus',
									null,
									'anyof',
									[
											Timesheet_Approval_Status.Approved,
											Timesheet_Approval_Status.Rejected,
											Timesheet_Approval_Status.PendingApproval,
											Timesheet_Approval_Status.Open ]),
							new nlobjSearchFilter('timesheetdate', null,
									'within', startDate, endDate) ], [
							new nlobjSearchColumn('internalid', 'employee',
									'group'),
							new nlobjSearchColumn('approvalstatus', null,
									'group'),
							new nlobjSearchColumn('startdate', null, 'group')
									.setSort() ]);

			var submitted_list = [];
			var open_list = [];
			var emp_list = [];
			var report_list = [];
			var emp_index = -1;

			search_employee_submitted_timesheet
					.forEach(function(ts) {
						var empId = ts.getValue('internalid', 'employee',
								'group');
						emp_list.push(empId);

						if (ts.getValue('approvalstatus', null, 'group') == Timesheet_Approval_Status.Open) {
							emp_index = _.indexOf(open_list, empId);
							nlapiLogExecution('debug', 'emp_index', emp_index);

							if (emp_index == -1) {
								open_list.push(empId);
								report_list.push({
									Employee : ts.getValue('internalid',
											'employee', 'group'),
									Date : []
								});
								emp_index = open_list.length - 1;
							}

							report_list[emp_index].Date.push(ts.getValue(
									'startdate', null, 'group'));

							start_date_list.push(ts.getValue('startdate', null,
									'group'));
						} else {
							submitted_list.push(ts.getValue('internalid',
									'employee', 'group'));
						}
					});

			emp_list = _.uniq(emp_list);

			nlapiLogExecution('debug', 'report_list', JSON
					.stringify(report_list));

			nlapiLogExecution(
					'debug',
					'Submitted Timesheets Employee Count During Selected Period',
					submitted_list.length);

			nlapiLogExecution('debug',
					'Open Timesheets Employee Count During Selected Period',
					open_list.length);

			nlapiLogExecution('debug',
					'Open Employee Count During Selected Period',
					report_list.length);

			start_date_list = _.uniq(start_date_list);
			nlapiLogExecution('debug', 'total weeks', start_date_list.length);

			var all_not_entered_employees = nlapiSearchRecord(
					'employee',
					null,
					[
							[ 'internalid', 'noneof', emp_list ],
							'and',
							[ 'timeapprover', 'anyof', currentUser ],
							'and',
							[
									[ 'custentity_lwd', 'after', endDate ],
									'or',
									[ 'custentity_employee_inactive', 'is', 'F' ] ] ]);

			var not_entered_ts = [];
			all_not_entered_employees.forEach(function(emp) {
				not_entered_ts.push(emp.getId());
			});

			nlapiLogExecution('debug', 'Not Entered', not_entered_ts.length);

		} else {
			throw "No Employee Found";
		}
	} catch (err) {
		nlapiLogExecution('debug', 'error:main', err);
	}
}
