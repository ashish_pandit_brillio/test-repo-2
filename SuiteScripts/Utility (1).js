/**
 * Module Description
 * 
 * Version Date Author Remarks 1.00 27 Jan 2015 Nitish Mishra
 * 
 */

function searchRecord(recordType, savedSearch, arrFilters, arrColumns,
        filterExpression)
{

	try {
		var search = null;

		// if a saved search is provided, load it and add the filters and
		// columns
		if (isNotEmpty(savedSearch)) {
			search = nlapiLoadSearch(recordType, savedSearch);

			if (isArrayNotEmpty(arrFilters)) {
				search.addFilters(arrFilters);
			}

			if (isArrayNotEmpty(arrColumns)) {
				search.addColumns(arrColumns);
			}

			if (isArrayNotEmpty(filterExpression)) {
				search.setFilterExpression(filterExpression);
			}
		}
		// create a new search
		else {
			search = nlapiCreateSearch(recordType, arrFilters, arrColumns);
		}

		// run search
		var resultSet = search.runSearch();

		// iterate through the search and get all data 1000 at a time
		var searchResultCount = 0;
		var resultSlice = null;
		var searchResult = [];

		do {
			resultSlice = resultSet.getResults(searchResultCount,
			        searchResultCount + 1000);

			if (resultSlice) {

				resultSlice.forEach(function(result) {

					searchResult.push(result);
					searchResultCount++;
				});
			}
		} while (isArrayNotEmpty(resultSlice) && resultSlice.length >= 1000);

		return searchResult;
	} catch (err) {
		nlapiLogExecution('ERROR', 'searchRecord', err);
		throw err;
	}
}

function isArrayEmpty(argArray) {

	return !isArrayNotEmpty(argArray);
}

function isArrayNotEmpty(argArray) {

	return (isNotEmpty(argArray) && argArray.length > 0);
}

function isEmpty(value) {

	return value == null || value == "" || typeof (value) == undefined;
}

function isNotEmpty(value) {

	return !isEmpty(value);
}

function isTrue(value) {

	return value == 'T';
}

function isFalse(value) {

	return value != 'T';
}

function addMailTemplate(mailBody) {

	var htmltext = '<table border="0" width="100%">';
	htmltext += '<tr>';
	htmltext += '<td colspan="4" valign="top">';
	htmltext += mailBody;
	htmltext += '</td></tr>';
	htmltext += '</table>';
	htmltext += '<hr width="100%" size="1" noshade color="#CCCCCC">';
	htmltext += '<table width="100%" border="0" cellspacing="0" cellpadding="0">';
	htmltext += '<tr>';
	htmltext += '<td align="right">';
	htmltext += '<font style="font-size:9px; font-family:Verdana,Arial,Helvetica,sans-serif; color:#999999;">Brillio is powered by <a href="http://www.netsuite.com/" style="color:#999999;">NetSuite</a> &#151; One System. No Limits.</font>';
	htmltext += '</td>';
	htmltext += '</tr>';
	htmltext += '</table>';
	htmltext += '</body>';
	htmltext += '</html>';

	return htmltext;
}

function getRelatedProjects(currentUser) {
	var filters = [
	        [ [ 'custentity_projectmanager', 'anyof', currentUser ], 'or',
	                [ 'custentity_deliverymanager', 'anyof', currentUser ],
	                'or', [ 'custentity_clientpartner', 'anyof', currentUser ] ],
	        'and',
	        [ [ 'status', 'anyof', '2' ], 'or', [ 'status', 'anyof', '4' ] ] ];

	var columns = [ new nlobjSearchColumn("entityid"),
	        new nlobjSearchColumn("altname"),
                  new nlobjSearchColumn("companyname")];

	var search = nlapiSearchRecord('job', null, filters, columns);
	return search;
}

function getTaggedProjectList(i_user_id) {
	try {
		if (i_user_id == 9673) {
			i_user_id = 1759;
		}

		// get the list of project the user has access to
		var project_filter = [
		        [
		                [ 'custentity_projectmanager', 'anyOf', i_user_id ],
		                'or',
		                [ 'custentity_deliverymanager', 'anyOf', i_user_id ],
		                'or',
		                [ 'custentity_clientpartner', 'anyOf', i_user_id ],
		                'or',
		                [ 'customer.custentity_clientpartner', 'anyOf',
		                        i_user_id ],
		                'or',
		                [ 'custentity_practice.custrecord_practicehead',
		                        'anyOf', i_user_id ] ], 'and',
		        [ 'status', 'anyOf', 2 ] ];

		var project_search_results = searchRecord('job', null, project_filter,
		        [ new nlobjSearchColumn("entityid"),
		                new nlobjSearchColumn("altname"),
                 new nlobjSearchColumn("internalid"),
                new nlobjSearchColumn("companyname")]);

		nlapiLogExecution('debug', 'project count',
		        project_search_results.length);

		if (project_search_results.length == 0) {
			throw "You don't have any projects under you.";
		}

		return project_search_results;
	} catch (err) {
		nlapiLogExecution('ERROR', 'getTaggedProjectList', err);
		throw err;
	}
}

function generateEmpDetailsUrl(empId, displayName) {
	return "<a href='/app/site/hosting/scriptlet.nl?script=817&deploy=1&emp="
	        + empId + "'>" + displayName + "</a>";
}
