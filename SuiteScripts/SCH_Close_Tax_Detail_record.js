// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
/*
   	Script Name:SCH_Close_tax_detail_record.js
	Author:		Nikhil jain
	Company:	Aashna cloudtech Pvt ltd.
	Date:		8 August 2015


	Script Modification Log:

	-- Date --			-- Modified By --				--Requested By--				-- Description --
	12 Jan 2016          Nikhil jain                                                    add an payment id on tax bill and invoice records



Below is a summary of the process controls enforced by this script file.  The control logic is described
more fully, below, in the appropriate function headers and code blocks.


     SCHEDULED FUNCTION
		- scheduledFunction(type)


     SUB-FUNCTIONS
		- The following sub-functions are called by the above core functions in order to maintain code
            modularization:

               - NOT USED

*/
}
// END SCRIPT DESCRIPTION BLOCK  ====================================


// BEGIN SCHEDULED FUNCTION =============================================

function schedulerFunction_update_tax_record(type){
	/*  On scheduled function:
	 - PURPOSE
	 -
	 FIELDS USED:
	 --Field Name--				--ID--
	 */
	//==== CODE FOR DESGNING POP UP XL ======
	
	var context = nlapiGetContext();
	var applied_taxcode = context.getSetting('SCRIPT', 'custscript_ait_pay_taxcode')
	nlapiLogExecution('DEBUG', 'Update Tax record', 'applied_taxcode' + applied_taxcode)
	
	var fromdate = context.getSetting('SCRIPT', 'custscript_ait_taxpay_fromdate')
	nlapiLogExecution('DEBUG', 'Update Tax record', 'fromdate' + fromdate)
	
	var todate = context.getSetting('SCRIPT', 'custscript_ait_taxpay_todate')
	nlapiLogExecution('DEBUG', 'Update Tax record', 'todate' + todate)
	
	var newpaymentid = context.getSetting('SCRIPT', 'custscript_ait_taxpay_paymentid')
	nlapiLogExecution('DEBUG', 'Update Tax record', 'Payment Id' + newpaymentid)
	
	var i_taxTypeID = context.getSetting('SCRIPT', 'custscript_ait_taxpay_id')
	nlapiLogExecution('DEBUG', 'Update Tax record', 'i_taxTypeID' + i_taxTypeID)
	
	var a_taxcode = applied_taxcode.split('#')
	
	for (var k = 1; k < a_taxcode.length; k++) {
		var taxcode = a_taxcode[k];
		nlapiLogExecution('DEBUG', 'Update Tax record', 'taxcode' + taxcode)
		var filters = new Array();
		filters.push(new nlobjSearchFilter('custrecord_vatbill_taxid', null, 'is', taxcode))
		filters.push(new nlobjSearchFilter('custrecord_vatbill_billdate', null, 'onorafter', fromdate))
		filters.push(new nlobjSearchFilter('custrecord_vatbill_billdate', null, 'onorbefore', todate))
		bill_filters.push(new nlobjSearchFilter('custrecord_vatbill_status', null, 'is', 'Open'))
		bill_filters.push(new nlobjSearchFilter('approvalstatus', 'custrecord_vatbill_tranno', 'is', 2))
		bill_filters.push(new nlobjSearchFilter('mainline', 'custrecord_vatbill_tranno', 'is', 'T'))
		bill_filters.push(new nlobjSearchFilter('voided', 'custrecord_vatbill_tranno', 'is', 'F'))
		bill_filters.push(new nlobjSearchFilter('recordtype', 'custrecord_vatbill_tranno', 'is', 'vendorbill'))
		if (i_taxTypeID != null && i_taxTypeID != '' && i_taxTypeID != undefined) {
			if (parseInt(i_taxTypeID) == parseInt(6)) {
				bill_filters.push(new nlobjSearchFilter('custrecord_vatbill_rcm', null, 'is', 'T'))
			}
			else {
			
				bill_filters.push(new nlobjSearchFilter('custrecord_vatbill_rcm', null, 'is', 'F'))
			}
		}
		var column = new Array();
		column.push(new nlobjSearchColumn('internalid'))
		
		var results = nlapiSearchRecord('customrecord_vatbilldetail', null, filters, column);
		if (results != null) {
			for (var i = 0; results != null && i < results.length; i++) {
				var billid = results[i].getValue('internalid')
				nlapiLogExecution('DEBUG', 'user', 'Updated bill Id  ' + billid)
				
				/*
				 updateBillrecord(billid)
				 
				 var record = nlapiLoadRecord('customrecord_vatbilldetail', billid);
				 record.setFieldValue('custrecord_vatbill_status', 'Close');
				 var updatedId = nlapiSubmitRecord(record, true);
				 nlapiLogExecution('DEBUG', 'user', 'updated bill ' + updatedId)
				 */
				var fields = new Array();
				var values = new Array();
				fields[0] = 'custrecord_vatbill_status';
				values[0] = "Close";
				fields[1] = 'custrecord_vatbill_tax_payment';
				values[1] = newpaymentid
				nlapiSubmitField('customrecord_vatbilldetail', billid, fields, values)
				
				//nlapiSubmitField('customrecord_vatbilldetail', billid, 'custrecord_vatbill_status', 'Close')
			}
			
		}
		var invfilters = new Array();
		invfilters.push(new nlobjSearchFilter('custrecord_vatinvoice_taxid', null, 'is', taxcode))
		invfilters.push(new nlobjSearchFilter('custrecord_vatinvoice_date', null, 'onorafter', fromdate))
		invfilters.push(new nlobjSearchFilter('custrecord_vatinvoice_date', null, 'onorbefore', todate))
		invfilters.push(new nlobjSearchFilter('custrecord_vatinvoice_status', null, 'is', 'Open'))
		invfilters.push(new nlobjSearchFilter('mainline', 'custrecord_vatinvoice_tranno', 'is', 'T'))
		invfilters.push(new nlobjSearchFilter('voided', 'custrecord_vatinvoice_tranno', 'is', 'F'))
		if (i_taxTypeID != null && i_taxTypeID != '' && i_taxTypeID != undefined) {
			if (parseInt(i_taxTypeID) == parseInt(6)) {
				invfilters.push(new nlobjSearchFilter('custrecord_vatinvoice_rcm', null, 'is', 'T'))
			}
			else {
			
				invfilters.push(new nlobjSearchFilter('custrecord_vatinvoice_rcm', null, 'is', 'F'))
			}
		}
		var invcolumn = new Array();
		invcolumn.push(new nlobjSearchColumn('internalid'))
		invcolumn.push(new nlobjSearchColumn('recordtype', 'custrecord_vatinvoice_tranno', null))
		invcolumn.push(new nlobjSearchColumn('approvalstatus', 'custrecord_vatinvoice_tranno', null))
		
		var invresults = nlapiSearchRecord('customrecord_vatinvoicedetail', null, invfilters, invcolumn);
		if (invresults != null && invresults != '' && invresults != undefined) {
			for (var j = 0; invresults != null && j < invresults.length; j++) {
				var i_rectype = invresults[j].getValue('recordtype', 'custrecord_vatinvoice_tranno', null)
				nlapiLogExecution('DEBUG', 'getSaletaxtotal', ' i_rectype-->' + i_rectype)
				
				var i_status = invresults[j].getValue('approvalstatus', 'custrecord_vatinvoice_tranno', null)
				nlapiLogExecution('DEBUG', 'getSaletaxtotal', ' i_status-->' + i_status)
				
				if (i_rectype == 'vendorbill') {
					if (i_status == '2') {
						var invoiceid = invresults[j].getValue('internalid')
						nlapiLogExecution('DEBUG', 'user', 'invoiceid ' + invoiceid)
						
						var fields = new Array();
						var values = new Array();
						fields[0] = 'custrecord_vatinvoice_status';
						values[0] = "Close";
						fields[1] = 'custrecord_vatinvoice_taxpayment';
						values[1] = newpaymentid
						
						nlapiSubmitField('customrecord_vatinvoicedetail', invoiceid, fields, values)
					}
				}
				else {
					var invoiceid = invresults[j].getValue('internalid')
					nlapiLogExecution('DEBUG', 'user', 'invoiceid ' + invoiceid)
					
					var fields = new Array();
					var values = new Array();
					fields[0] = 'custrecord_vatinvoice_status';
					values[0] = "Close";
					fields[1] = 'custrecord_vatinvoice_taxpayment';
					values[1] = newpaymentid
					
					nlapiSubmitField('customrecord_vatinvoicedetail', invoiceid, fields, values)
				}
				
			}
		}
	}
}
// END SCHEDULED FUNCTION ===============================================





// BEGIN FUNCTION ===================================================
{



}
// END FUNCTION =====================================================
