// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
/*
   	Script Name: CLI_Brillio_Validation_Exp
	Author: Vikrant 
	Company: Aashna CloudTech
	Date: 21-05-2014
    Description: Initial draft for multiple forms validations script


	Script Modification Log:

	-- Date --			-- Modified By --				--Requested By--				-- Description --
   29 July 2014          Shweta Chopde                   Shekar                         Disabled field Supervisor Approval



	Below is a summary of the process controls enforced by this script file.  The control logic is described
	more fully, below, in the appropriate function headers and code blocks.

     PAGE INIT
		- pageInit(type)


     SAVE RECORD
		- saveRecord()


     VALIDATE FIELD
		- validateField(type, name, linenum)


     FIELD CHANGED
		- fieldChanged_Brillio(type, name, linenum)


     POST SOURCING
		- postSourcing(type, name)


	LINE INIT
		- lineInit(type)


     VALIDATE LINE
		- validateLine()


     RECALC
		- reCalc()


     SUB-FUNCTIONS
		- The following sub-functions are called by the above core functions in order to maintain code
            modularization:





*/
}
// END SCRIPT DESCRIPTION BLOCK  ====================================



// BEGIN SCRIPT UPDATION BLOCK  ====================================
/*


*/
// END SCRIPT UPDATION BLOCK  ====================================




// BEGIN GLOBAL VARIABLE BLOCK  =====================================
{

	//  Initialize any Global Variables, in particular, debugging variables...


}
// END GLOBAL VARIABLE BLOCK  =======================================




// BEGIN PAGE INIT ==================================================

function pageInit_Brillio(type) //
{
	/*  On page init:
	 - PURPOSE
	 FIELDS USED:
	 --Field Name--				--ID--			--Line Item Name--
	 */
	//  LOCAL VARIABLES
	
	// nlapiDisableLineItemField('expense', 'custcolcustcol_temp_customer', true); // Customer
	// nlapiDisableLineItemField('expense', 'custcolprj_name', true); // Project Desc

	if(type == 'edit' || type == 'create'){
	nlapiSetFieldValue('custbody_is_je_updated_for_emp_type','F');
	}
	nlapiDisableField('supervisorapproval',true)
	
	
	//nlapiSelectNewLineItem('expense');
	nlapiDisableLineItemField('expense', 'location', true); // location
	nlapiDisableLineItemField('expense', 'department', true); // practice
	nlapiDisableLineItemField('expense', 'class', true); // vertical
	nlapiDisableLineItemField('expense', 'category', true); // Category
  	nlapiDisableLineItemField('expense', 'amount', true); // amount
	nlapiDisableLineItemField('expense', 'exchangerate', true); // exchangerate
  
	var cust_ = nlapiGetUser();
	//alert(cust_);
	var cust_ID = nlapiGetFieldValue('entity');
	nlapiLogExecution('DEBUG', 'pageInit_Brillio', 'cust_ID : ' + cust_ID);
	
	if (cust_ID != null && cust_ID != '' && cust_ID != 'undefined') //
	{
		lineInit_Brillio(type);
	}
	
	var approval_status = nlapiGetFieldValue('approvalstatus');
	//alert('approval_status : '+approval_status);
	if (approval_status == '2') //
	{
		//alert('into approval');
		nlapiDisableLineItemField('expense', 'refnumber', true); // vertical
		nlapiDisableLineItemField('expense', 'expensedate', true); // vertical
		nlapiDisableLineItemField('expense', 'category', true); // vertical
		nlapiDisableLineItemField('expense', 'currency', true); // vertical
		nlapiDisableLineItemField('expense', 'amount', true); // vertical
		nlapiDisableLineItemField('expense', 'foreignamount', true); // vertical
		nlapiDisableLineItemField('expense', 'memo', true); // vertical
		nlapiDisableLineItemField('expense', 'customer', true); // vertical
		nlapiDisableLineItemField('expense', 'custcol_projectmanager', true); // vertical
		nlapiDisableLineItemField('expense', 'isbillable', true); // vertical
		nlapiDisableLineItemField('expense', 'custcol_creditcard', true); // vertical
		nlapiDisableLineItemField('expense', 'expmediaitem', true); // vertical
		nlapiDisableLineItemField('expense', 'isnonreimbursable', true); // vertical
		nlapiDisableLineItemField('expense', 'receipt', true); // vertical
		
		nlapiDisableLineItemField('expense', 'tax1amt', true); // vertical
		nlapiDisableLineItemField('expense', 'taxcode', true); // vertical
		nlapiDisableLineItemField('expense', 'taxrate1', true); // grossamt
		nlapiDisableLineItemField('expense', 'grossamt', true); // grossamt
		
		var button4 = window.document.getElementById('total')
		button4.style.visibility = 'hidden';
		
	}
	//  PAGE INIT CODE BODY

}

// END PAGE INIT ====================================================





// BEGIN SAVE RECORD ================================================

function saveRecord()
{
    /*  On save record:

	- PURPOSE



	  FIELDS USED:

		--Field Name--			--ID--		--Line Item Name--


    */


    //  LOCAL VARIABLES



    //  SAVE RECORD CODE BODY


	return true;

}

// END SAVE RECORD ==================================================





// BEGIN VALIDATE FIELD =============================================

function validateField(type, name, linenum)
{

	/*  On validate field:

          - EXPLAIN THE PURPOSE OF THIS FUNCTION


          FIELDS USED:

          --Field Name--				--ID--


    */


	//  LOCAL VARIABLES



	//  VALIDATE FIELD CODE BODY


	return true;

}

// END VALIDATE FIELD ===============================================





// BEGIN FIELD CHANGED ==============================================

function fieldChngd_Exp_Rep(type, name, linenum)//
{
	/*  On field changed:
	 - PURPOSE
	 FIELDS USED:
	 --Field Name--				--ID--
	 */
	//  LOCAL VARIABLES
	
	
	//  FIELD CHANGED CODE BODY
	// nlapiDisableLineItemField('expense', 'custcolcustcol_temp_customer', true); // Customer
	// nlapiDisableLineItemField('expense', 'custcolprj_name', true); // Project Desc

	
	nlapiDisableField('supervisorapproval',true)
	
	
	if (name == 'amount' || name == 'foreignamount')//
	{
		var app_stat = nlapiGetFieldValue('approvalstatus');
		//alert('app-'+ app_stat);
		if (app_stat == '2') //
		{
			nlapiDisableLineItemField('expense', 'amount', true); // vertical
			nlapiDisableLineItemField('expense', 'foreignamount', true); // vertical
		}
	}
	
	
	if (name == 'customer') //
	{
		var project_ID = nlapiGetCurrentLineItemValue('expense', 'customer'); // get the value of project from line item
		nlapiLogExecution('DEBUG', 'fieldChanged_Brillio', 'project_ID : ' + project_ID);
		
		if (project_ID != null && project_ID != '' && project_ID != 'undefined') //
		{
			//var project_Rec = nlapiLoadRecord('job', project_ID);
			
			//if (project_Rec != null && project_Rec != '' && project_Rec != 'undefined') //
			{
				//var vertical_ID = project_Rec.getFieldValue('custentity_vertical');
				//alert('before geting vertical');
				
				var a = new Array();
				a['User-Agent-x'] = 'SuiteScript-Call';
				
				var resposeObject = nlapiRequestURL('/app/site/hosting/scriptlet.nl?script=174&deploy=1&custscript_project_id=' + project_ID, null, a);
				
				var vertical_ID = resposeObject.getBody();
				nlapiLogExecution('DEBUG', 'fieldChanged_Brillio', 'vertical_ID : ' + vertical_ID);
				
				if (vertical_ID == 'Vertical Not Selected on Project.') //
				{
                    //alert('Vertical Not Selected on Project.');
					//nlapiSetCurrentLineItemValue('expense', 'customer', '', false);
					nlapiSetCurrentLineItemValue('expense', 'class', '', false);
				}
				
				else if (vertical_ID != null && vertical_ID != '' && vertical_ID != 'undefined' && vertical_ID != 'Vertical Not Selected on Project.') //
				{
					nlapiSetCurrentLineItemValue('expense', 'class', vertical_ID, false);
				}
			}
		}
	}
	return true;
}

// END FIELD CHANGED ================================================





// BEGIN POST SOURCING ==============================================

function postSourcing(type, name)
{

    /*  On post sourcing:

	- PURPOSE

          FIELDS USED:

		--Field Name--			--ID--		--Line Item Name--


    */

    //  LOCAL VARIABLES


    //  POST SOURCING CODE BODY



}

// END POST SOURCING ================================================



// BEGIN LINE INIT ==============================================

function lineInit_Brillio(type) //
{
  //  nlapiDisableLineItemField('expense', 'custcolcustcol_temp_customer', true); // Customer
// nlapiDisableLineItemField('expense', 'custcolprj_name', true); // Project Desc

	/*  On Line Init:
	 - PURPOSE
	 FIELDS USED:
	 --Field Name--			--ID--		--Line Item Name--
	 */
	//  LOCAL VARIABLES
	
	
	//  LINE INIT CODE BODY
	nlapiLogExecution('DEBUG', 'fieldChanged_Brillio', 'lineInit_Brillio');
	
	var app_stat = nlapiGetFieldValue('approvalstatus');
	//alert('app-' + app_stat);
	if (app_stat == '2') //
	{
		nlapiDisableLineItemField('expense', 'amount', true); // vertical
		nlapiDisableLineItemField('expense', 'foreignamount', true); // vertical
	}
	
	var cust_ID = nlapiGetFieldValue('entity');
	nlapiLogExecution('DEBUG', 'fieldChanged_Brillio', 'cust_ID : ' + cust_ID);
	
	if (cust_ID != null && cust_ID != '' && cust_ID != 'undefined') //
	{
		//var practice = nlapiLookupField('employee', cust_ID, 'department');
		//nlapiLogExecution('DEBUG', 'fieldChanged_Brillio', 'practice : ' + practice);
		
		//var location = nlapiLookupField('employee', cust_ID, 'location');
		//nlapiLogExecution('DEBUG', 'fieldChanged_Brillio', 'location : ' + location);
		//===================================Code Modified on 16/04/2018==========================================================
			var linkURL = nlapiResolveURL('SUITELET', 'customscript_sut_getemployeedetails', 'customdeploy1', null);
			
            var param = '&i_employee=' + cust_ID;
            var finalURL = linkURL + param;
           // var response = new Array();
			//alert('Test Alert');
			var header = new Array();
			header['Accept'] = 'application/json';
			var response = nlapiRequestURL(finalURL, null, null, null);
			var status1 = response.getBody();
			status1 = JSON.parse(status1);
			//alert('status1'+status1);
          
			var practice = status1[0].i_practice;
			var location = status1[0].i_location;
			//alert('practice '+practice);
			//alert('i_location '+i_location);
		//=======================================================================================================================
		if (practice != null && practice != '' && practice != 'undefined') //
		{
			nlapiSetCurrentLineItemValue('expense', 'department', practice, false);
		}
		
		if (location != null && location != '' && location != 'undefined') //
		{
			nlapiSetCurrentLineItemValue('expense', 'location', location, false);
		}
	}
	
	return true;
}

// END LINE INIT ================================================


// BEGIN VALIDATE LINE ==============================================

function validateLine(type)
{

	/*  On validate line:

          - EXPLAIN THE PURPOSE OF THIS FUNCTION


          FIELDS USED:

          --Field Name--				--ID--


    */


	//  LOCAL VARIABLES


	//  VALIDATE LINE CODE BODY


	return true;

}

// END VALIDATE LINE ================================================





// BEGIN RECALC =====================================================

function recalc(type)
{

	/*  On recalc:

          - EXPLAIN THE PURPOSE OF THIS FUNCTION


          FIELDS USED:

          --Field Name--				--ID--


    */


	//  LOCAL VARIABLES


	//  RECALC CODE BODY


}

// END RECALC =======================================================





// BEGIN FUNCTION ===================================================




// END FUNCTION =====================================================
