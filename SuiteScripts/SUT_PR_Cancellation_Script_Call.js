/**
 * @author Shweta
 */
// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
/*
   	Script Name : SUT_PR_Cancellation_Script_Call.js
	Author      : Shweta Chopde
	Date        : 21 July 2014
	Description : Set the PR  - Item status as 'Cancelled'


	Script Modification Log:

	-- Date --			-- Modified By --				--Requested By--				-- Description --



Below is a summary of the process controls enforced by this script file.  The control logic is described
more fully, below, in the appropriate function headers and code blocks.


     SUITELET
		- suiteletFunction(request, response)


     SUB-FUNCTIONS
		- The following sub-functions are called by the above core functions in order to maintain code
            modularization:

               - NOT USED

*/
}
// END SCRIPT DESCRIPTION BLOCK  ====================================



// BEGIN GLOBAL VARIABLE BLOCK  =====================================
{
	//  Initialize any Global Variables, in particular, debugging variables...




}
// END GLOBAL VARIABLE BLOCK  =======================================





// BEGIN SUITELET ==================================================

function suiteletFunction(request, response)
{

	try 
	{
	 if (request.getMethod() == 'GET') 
	 {			
	  var i_PR_Req_ID =  request.getParameter('custscript_pr_req_id_c');
      nlapiLogExecution('DEBUG', ' suiteletFunction',' PR ID -->' + i_PR_Req_ID);	   
		
 	  var i_SR_No =  request.getParameter('custscript_s_no_cn');
      nlapiLogExecution('DEBUG', ' suiteletFunction',' S No -->' + i_SR_No);
	   
      var i_index_of = request.getParameter('custscript_index_of_c');
      nlapiLogExecution('DEBUG', ' suiteletFunction',' Index Of -->' + i_index_of);
	   
      var i_item = request.getParameter('custscript_item_c');
      nlapiLogExecution('DEBUG', ' suiteletFunction',' Item -->' + i_item);
		   
	  var i_PR_ID = search_PR_Item_recordID(i_PR_Req_ID,i_SR_No,i_item)
	  nlapiLogExecution('DEBUG', ' suiteletFunction',' PR Req ID -->' + i_PR_ID);	
		
	  if(_logValidation(i_PR_ID))
	  {					
	    var o_PR_OBJ = nlapiLoadRecord('customrecord_pritem',i_PR_ID)		
		if(_logValidation(o_PR_OBJ))
		{
		  o_PR_OBJ.setFieldValue('custrecord_prastatus',22)			
			
		   var i_PR_submitID = nlapiSubmitRecord(o_PR_OBJ,true,true)				 
		   nlapiLogExecution('DEBUG', ' suiteletFunction',' i_PR_submitID -->'+i_PR_submitID)				
		}//PR OBJ						
	  }	
	 }
	}
    catch(exception)
	{
	  nlapiLogExecution('DEBUG', 'ERROR',' Exception Caught -->' + exception);	
	}//CATCH
	
	 response.write(i_PR_submitID);	 
}

// END SUITELET ====================================================




// BEGIN OBJECT CALLED/INVOKING FUNCTION ===================================================
function search_PR_Item_recordID(i_recordID,i_SR_No_QA,i_item_QA)
{  
   var i_internal_id;
   
   var filter = new Array();
   filter[0] = new nlobjSearchFilter('custrecord_purchaserequest', null, 'is',i_recordID);
   filter[1] = new nlobjSearchFilter('custrecord_prlineitemno', null, 'is',parseInt(i_SR_No_QA));
   filter[2] = new nlobjSearchFilter('custrecord_prmatgrpcategory', null, 'is',i_item_QA);
	
   var columns = new Array();
   columns[0] = new nlobjSearchColumn('internalid');
   columns[1] = new nlobjSearchColumn('custrecord_purchaserequest');
   columns[2] = new nlobjSearchColumn('custrecord_prlineitemno');
   columns[3] = new nlobjSearchColumn('custrecord_prmatgrpcategory');
      
   var a_seq_searchresults = nlapiSearchRecord('customrecord_pritem',null,filter,columns);
	
	if (a_seq_searchresults != null && a_seq_searchresults != '' && a_seq_searchresults != undefined)
	{	
	 nlapiLogExecution('DEBUG', 'search_PR_Item_recordID', 'a_seq_searchresults ==' +a_seq_searchresults.length);
	 
	 for(var ty=0;ty<a_seq_searchresults.length;ty++)
	 {
	 	i_internal_id = a_seq_searchresults[ty].getValue('internalid');
		nlapiLogExecution('DEBUG', 'search_PR_Item_recordID', ' Internal ID -->' + i_internal_id);
		
		var i_PR = a_seq_searchresults[ty].getValue('custrecord_purchaserequest');
		nlapiLogExecution('DEBUG', 'search_PR_Item_recordID', ' i_PR ID -->' + i_PR);
		
		var i_line_no = a_seq_searchresults[ty].getValue('custrecord_prlineitemno');
		nlapiLogExecution('DEBUG', 'search_PR_Item_recordID', ' i_line_no -->' + i_line_no);
		
		var i_item = a_seq_searchresults[ty].getValue('custrecord_prmatgrpcategory');
		nlapiLogExecution('DEBUG', 'search_PR_Item_recordID', ' i_item-->' + i_item);
		
		if((i_PR == i_recordID)&&(i_line_no == i_SR_No_QA)&&(i_item == i_item_QA))
		{
			i_internal_id = i_internal_id
			break
		}
		
	 }//LOOP
	}	
	return i_internal_id;	
}

function _logValidation(value) 
{
 if(value != null && value.toString() != null && value != '' && value != undefined && value.toString() != undefined && value != 'undefined' && value.toString() != 'undefined'&& value.toString() != 'NaN' && value != NaN) 
 {
  return true;
 }
 else 
 { 
  return false;
 }
}
// END OBJECT CALLED/INVOKING FUNCTION =====================================================
