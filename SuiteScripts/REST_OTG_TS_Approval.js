/**
 * RESTlet for Quick Time Approval Pool
 * 
 * Version Date Author Remarks 1.00 03 Nov 2015 Nitish Mishra
 * Version Date Author Remarks 2.00 18 Mar 20202 praveena madem chaged timesheets'sublist id instaed of subrecords
 */

function postRESTlet(dataIn) {
	var response = new Response();
	try {
		
	/*	var dataIn = []
		dataIn = 
{
  "RequestType": "APPROVE",
  "EmailId": "deepak.srinivas@brillio.com",
  "Data": {
    "TimesheetList": [
      {
        "TimesheetId": "4783271",
        "EmployeeName": "103885-Kiran Kumar Y",
        "WeekStartDate": "05/24/2020",
        "WeekEndDate": "05/30/2020",
        "ST": "40:00",
        "OT": "00:00",
        "Leave": "00:00",
        "Holiday": "00:00",
        "TotalApprovable": "40:00",
        "Checked": true,
        "PoolId": "5767392",
        "Created": "05/31/2020 12:06 pm",
        "Project": "MAII03743 MasterCard International Incorporated : W3C Payment API Specification using Polyfill"
      }
    ]
  }
}*/
      	var current_date = nlapiDateToString(new Date());
		//Log for current date
		nlapiLogExecution('DEBUG', 'Current Date', 'current_date...' + current_date)
		nlapiLogExecution('debug', 'dataIn', JSON.stringify(dataIn));
		var employeeId = getUserUsingEmailId(dataIn.EmailId);
		var requestType = dataIn.RequestType;

		//Test 
		//var requestType = 'GET';
		//var employeeId = '30237';
      	switch (requestType) {

			case M_Constants.Request.Get:
				response.Data = getPendingTimesheets(employeeId);
				response.Status = true;
			break;

			case M_Constants.Request.Approve:
				response.Data = approveTimesheets(dataIn.Data.TimesheetList,
				        employeeId);
				response.Status = true;
			break;

			case M_Constants.Request.Reject:
				response.Data = rejectTimesheets(dataIn.Data.TimesheetList,
				        employeeId,dataIn);
				response.Status = true;
			break;
		}

	} catch (err) {
		nlapiLogExecution('ERROR', 'postRESTlet', err);
		response.Data = err;
		response.Status = false;
	}
	return response;
}

function getPendingTimesheets(employeeId) {
	try {
	var days= ['timebill0','timebill1','timebill2','timebill3','timebill4','timebill5','timebill6'];//Updated array as timebill values instaed of week--added by praveena on 18-03-2020
	
		var pendingTimesheetSearch = nlapiSearchRecord("timesheet",null,
		[
			["custrecord_ts_time_approver","anyof",employeeId], 
			"AND", 
			["approvalstatus","anyof","2"]//Pending Approval
		], 
		[
			new nlobjSearchColumn("id").setSort(false), 
			new nlobjSearchColumn("employee"), 
			new nlobjSearchColumn("startdate"), 
			new nlobjSearchColumn("enddate"), 
			new nlobjSearchColumn("totalhours"), 
			new nlobjSearchColumn("approvalstatus")
		]
		);

		var timesheetList = [];
		var projectList = [];
		var dataRow = [];
		var json = {};
		var main = {};
		if (pendingTimesheetSearch) {
			
			nlapiLogExecution('DEBUG','pendingTimesheetSearch',JSON.stringify(pendingTimesheetSearch));
			for (var i = 0; i < pendingTimesheetSearch.length && i < 50; i++) {
			 var timesheet_ID = pendingTimesheetSearch[i].getValue('id');
			 nlapiLogExecution('debug','TimeSheet ID',timesheet_ID);
				var loadRecord = nlapiLoadRecord('timesheet',timesheet_ID);
				var lineCount = loadRecord.getLineItemCount('timeitem');//changed sublist id by praveena 0n 18-03-2020
				
				for(var k=1;k<= lineCount ;k++){
				loadRecord.selectLineItem('timeitem',k);//changed sublist id by praveena 0n 18-03-2020
					
					
					for(var n=0;n<7;n++){
						
						var sub_day = days[n];
						var o_sub_record_view = loadRecord.getCurrentLineItemValue('timeitem', sub_day);
						if(o_sub_record_view){
						var i_item_id = loadRecord.getCurrentLineItemText('timeitem','customer');
						var d_submittedDate = loadRecord.getCurrentLineItemValue('timeitem','custcol_timestamp_tb');
						
						
						if(i_item_id )
						{
						var project_id = i_item_id;
						break;
						//json ={
					//	projectID :i_item_id
				
						//}
						
						}
												
				
				
					projectList.push(json);
				}
				}
				if(project_id){
				break;
				}
				loadRecord.commitLineItem('timeitem');
				}
				 nlapiLogExecution('debug','projectList',projectList);
				var timesheet = new TimesheetQuickApprovalPool();
		
				timesheet.TimesheetId = pendingTimesheetSearch[i]
				        .getValue('id');
				timesheet.EmployeeName = pendingTimesheetSearch[i]
				        .getText('employee');
				timesheet.WeekStartDate = pendingTimesheetSearch[i]
				        .getValue('startdate');
				timesheet.WeekEndDate = pendingTimesheetSearch[i]
				        .getValue('enddate');
				//timesheet.ST = pendingTimesheetSearch[i]
				//        .getValue('custrecord_qap_st_hours');
				//timesheet.OT = pendingTimesheetSearch[i]
				//        .getValue('custrecord_qap_ot_hours');
				//timesheet.Leave = pendingTimesheetSearch[i]
				//        .getValue('custrecord_qap_leave_hours');
				//timesheet.Holiday = pendingTimesheetSearch[i]
				//        .getValue('custrecord_qap_holiday_hours');
				timesheet.TotalApprovable = pendingTimesheetSearch[i].getValue(
				        'totalhours'); //.split(':')[0]
				//timesheet.Created = pendingTimesheetSearch[i]
				//        .getValue('created');
				//timesheet.Checked = false;
				timesheet.Project = project_id;
				timesheetList.push(timesheet);
				
			}
		}
		//main = {
		//	TimeSheet : timesheetList,
		//	Project : projectList
		//};

		return timesheetList;
	} catch (err) {
		nlapiLogExecution('error', 'getPendingTimesheets', err);
		throw err;
	}
}

function approveTimesheets(timesheetList, approverId) {
	try {
		var resultantList = [];
		nlapiLogExecution('DEBUG', 'timesheetList', timesheetList.length);
		
		//var days= ['timebill0','timebill1','timebill2','timebill3','timebill4','timebill5','timebill6'];

		for (var i = 0; i < timesheetList.length && i < 50; i++) {
			//var timesheet = timesheetList[i];

			//nlapiLogExecution('DEBUG', 'timesheet.Checked', timesheet.Checked);
			//if (timesheet.Checked) {
				var timesheetId = timesheetList[i].TimesheetId;

				try {
					
					var loadRecord = nlapiLoadRecord('timesheet',timesheetId);
					nlapiLogExecution('DEBUG', 'timesheet id', timesheetId);
					
					var status = loadRecord.getFieldValue('approvalstatus');
					nlapiLogExecution('DEBUG', 'TS status', status);
					
					var lineCount = loadRecord.getLineItemCount('timeitem');
					for(var k = 1;k <= lineCount ;k++)
					{
						for(var j = 0;j < 7;j++)
						{
							//var sub_day = days[j];
							var o_sub_record_view = loadRecord.getLineItemValue('timeitem','timebill'+j,k);
							if(o_sub_record_view)
							{
								nlapiSubmitField('timebill',o_sub_record_view,'approvalstatus',3);
							}
						}
					}
					//var id = nlapiSubmitRecord(loadRecord);
					nlapiLogExecution('DEBUG', 'approveTimesheets',
					        'RECORD UPDATED : ' + timesheetId);

					resultantList.push({
					    Id : timesheetId,
					    Message : 'TS Updated With Approved Status'
					});
				} catch (er) {
					nlapiLogExecution('ERROR', 'approveTimesheets',
					        'Failed To Approve : ' + timesheetId + ' : '
					                + er);
					resultantList.push({
					    Id : timesheetId,
					    Message : er
					});
				}
			}
		//}

		return resultantList;
	} catch (err) {
		nlapiLogExecution('error', 'approveTimesheets', err);
		throw err;
	}
}

function rejectTimesheets(timesheetList, approverId, dataIn) {
	try {
		var resultantList = [];
		var rejection_reson = '';
		var days= ['timebill0','timebill1','timebill2','timebill3','timebill4','timebill5','timebill6'];

		for (var i = 0; i < timesheetList.length && i < 50; i++) 
		{
				var timesheetId = timesheetList[i].TimesheetId;

				try {
					
					var loadRecord = nlapiLoadRecord('timesheet',timesheetId);
					nlapiLogExecution('DEBUG', 'timesheet id', timesheetId);
					
					var status = loadRecord.getFieldValue('approvalstatus');
					nlapiLogExecution('DEBUG', 'TS status', status);
					
					var lineCount = loadRecord.getLineItemCount('timeitem');
					for(var k = 1;k <= lineCount ;k++)
					{
						for(var j = 0;j < 7;j++)
						{
							var sub_day = days[j];
							var o_sub_record_view = loadRecord.getLineItemValue('timeitem','timebill'+j,k);
							if(o_sub_record_view)
							{
								nlapiSubmitField('timebill',o_sub_record_view,'approvalstatus',4);
							}
						}
					}



					resultantList.push({
					    Id : timesheetId,
					    Message : 'TS Updated With Rejection Status'
					});
				} 
				catch (er) {
					nlapiLogExecution('ERROR', 'rejectTimesheets',
					        'Failed To Reject : ' + timesheetId  + ' : '
					                + er);
					resultantList.push({
					    Id : timesheetId,
					    Message : er
					});
				}
		}
		

		return resultantList;
	} catch (err) {
		nlapiLogExecution('error', 'rejectTimesheets', err);
		throw err;
	}
}
function _logValidation(value) 
{
 if(value!='null' && value != null && value != null && value != '' && value != undefined && value != undefined && value != 'undefined' && value!= 'undefined'&& value!= 'NaN' && value != NaN) 
 {
  return true;
 }
 else 
 { 
  return false;
 }
}