/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       29 Aug 2017     deepak.srinivas
 *
 */

/**
 * @param {Object} dataIn Parameter object
 * @returns {Object} Output object
 */
function postRESTlet(dataIn) {
	try{
	var response = new Response(); 
	var startDate = '';
	var endDate = '';
	var current_date = nlapiDateToString(new Date());
	//Log for current date
	nlapiLogExecution('DEBUG', 'Current Date', 'current_date...' + current_date);
	nlapiLogExecution('debug', 'dataIn', JSON.stringify(dataIn));
	
	//var s_Date =  '8/11/2017';
	//var requestType = 'GET';
	var requestType = dataIn.RequestType;
	var s_Date = dataIn.Date;
	var date = nlapiStringToDate(s_Date);
	var endDate = nlapiAddDays(date,31);
	var s_endDate  = nlapiDateToString(endDate);
	var firstDay = new Date(date.getFullYear(), date.getMonth(), 1);
	var lastDay = new Date(date.getFullYear(), date.getMonth() + 1, 0);
	/*var d_Date = s_Date.split('/');
	var month = d_Date[0];
	if(year){
		startDate = '1/1/'+year
		endDate = '12/31/'+year
	}*/
	nlapiLogExecution('DEBUG', 'Current Date', 'endDate...' + s_endDate);
	nlapiLogExecution('DEBUG', 'Current Date', 'startDate...' + firstDay);
	nlapiLogExecution('DEBUG', 'Current Date', 'lastDay...' + lastDay);
	
	switch(requestType) {
	
	case M_Constants.Request.Get:
	if (s_Date) {
		response.Data = get_travel_requests(s_Date,s_endDate);
		response.Status = true;
	} else {
		response.Data = "Some error with the data sent";
		response.Status = false;
}
	break;
	case "PRACTICE_MASTER":

	{
	response.Data = getPracticeMasterData();
	response.Status = true;
	}
	break;
	
}
}
catch(err){
	nlapiLogExecution('error', 'Restlet Main Function', err);
	throw err;
}
nlapiLogExecution('debug', 'response', JSON.stringify(response));
return response;

}
function getPracticeMasterData(){
	try{
		var JSON ={};
		var dataRow = [];
		var search = searchRecord('department',null,[new nlobjSearchFilter('isinactive', null, 'is',
                                            						'F')], 
                                         				[
                                         				new nlobjSearchColumn('internalid'),
                                         				new nlobjSearchColumn('name'),
                                         				//new nlobjSearchColumn('parent'),
                                         				new nlobjSearchColumn('subsidiary'),
                                         				new nlobjSearchColumn('custrecord_practicehead'),
                                         				//new nlobjSearchColumn('custrecord_hrbusinesspartner'),  // Commented by shravan for HRBP integration on 7-dec-2020
                                         				new nlobjSearchColumn('custrecord12'),
                                         				new nlobjSearchColumn('custrecord_parent_practice'),
                                         				new nlobjSearchColumn('custrecord_onsite_practice_head'),
                                         				new nlobjSearchColumn('email','custrecord_onsite_practice_head'),
                                         				new nlobjSearchColumn('email','custrecord_practicehead'),
                                         			//	new nlobjSearchColumn('email','custrecord_hrbusinesspartner')// Commented by shravan for HRBP integration on 7-dec-2020
                                         				]);
		if(search){
			for(var i=0;i<search.length;i++){
				JSON ={
					ID: search[i].getValue('internalid'),
					Name: search[i].getValue('name'),
					PracticeDescription: search[i].getValue('custrecord12'),
					//Parent: search[i].getText('parent'),
					Subsidiary: search[i].getText('subsidiary'),
					PracticeHead: search[i].getText('custrecord_practicehead'),
					PracticeHeadEmail: search[i].getValue('email','custrecord_practicehead'),
					ParentPractice: search[i].getText('custrecord_parent_practice'),
					OnSitePracticeHead: search[i].getText('custrecord_onsite_practice_head'),
					OnSitePracticeHeadEmail: search[i].getValue('email','custrecord_onsite_practice_head'),
					//HRBusinessPartner: search[i].getText('custrecord_hrbusinesspartner'),
					// Commented by shravan for HRBP integration on 7-dec-2020
					//HRBusinessPartnerEmail: search[i].getValue('email','custrecord_hrbusinesspartner')
					// Commented by shravan for HRBP integration on 7-dec-2020
				};
				dataRow.push(JSON);
		
			}
		}
		return dataRow;
	}
	catch (err) {
		nlapiLogExecution('ERROR', 'region master error', err);
		
	}
}

function get_travel_requests(d_date,sd_endDate) {
	
	try{
	var holiday_list = [];
	var JSON ={};
	var JSON_List = {};
	var dataRow = [];
	var JSON_Total = {};
	var dataRow_All = [];
	
	//start_date = nlapiDateToString(start_date, 'date');
	//end_date = nlapiDateToString(end_date, 'date');

	// nlapiLogExecution('debug', 'start_date', start_date);
	// nlapiLogExecution('debug', 'end_date', end_date);
	// nlapiLogExecution('debug', 'subsidiary', subsidiary);
	
	var cols_T = [];
	cols_T.push(new nlobjSearchColumn('custrecord_trli_departure_date').setSort());
	//cols_.push(new nlobjSearchColumn('custrecord_trli_origin_city'));
	//cols_.push(new nlobjSearchColumn('custrecord_trli_destination_city'));
	cols_T.push(new nlobjSearchColumn('custrecord_trli_travel_request'));
	
	
	var search_travel_line_T = nlapiSearchRecord('customrecord_tr_travel_request_line_item',
	        null, [new nlobjSearchFilter('custrecord_trli_departure_date', null, 'onorafter',
	        		d_date)], cols_T);
	if(search_travel_line_T){
		
		for(var k=0;k<search_travel_line_T.length;k++){
			
		var i_travel_id = search_travel_line_T[k].getValue('custrecord_trli_travel_request');
		 nlapiLogExecution('debug', 'i_travel_id', i_travel_id);
	var cols = [];
	cols.push(new nlobjSearchColumn('name'));
	cols.push(new nlobjSearchColumn('internalid'));
	cols.push(new nlobjSearchColumn('custrecord_tr_travel_type'));
	cols.push(new nlobjSearchColumn('custrecord_tr_request_type'));
	cols.push(new nlobjSearchColumn('custrecord_tr_employee'));
	cols.push(new nlobjSearchColumn('email','custrecord_tr_employee'));
	cols.push(new nlobjSearchColumn('custrecord_tr_project'));
	cols.push(new nlobjSearchColumn('custrecord_tr_short_description'));
	cols.push(new nlobjSearchColumn('custrecord_tr_purpose_of_travel'));
	cols.push(new nlobjSearchColumn('custrecord_tr_status'));
	cols.push(new nlobjSearchColumn('custrecord_tr_employee_practice'));
	cols.push(new nlobjSearchColumn('custrecord_tr_departure_date').setSort());
	cols.push(new nlobjSearchColumn('custrecord_tr_gt_allocation_start_date'));
	cols.push(new nlobjSearchColumn('custrecord_tr_gt_allocation_end_date'));//custrecord_tr_visa_category
	cols.push(new nlobjSearchColumn('custrecord_tr_onsite_duration'));
	cols.push(new nlobjSearchColumn('custrecord_tr_visa_category'));
	
	var search_company_holiday = nlapiSearchRecord('customrecord_travel_request',
	        null, [new nlobjSearchFilter('internalid', null, 'anyof',i_travel_id),
	               new nlobjSearchFilter('custrecord_tr_status', null, 'noneof',
	    	                        ['1','7'])], cols);

	if (search_company_holiday) {

		for (var i = 0; i < search_company_holiday.length; i++) 
		{
			var practice = search_company_holiday[i].getValue('custrecord_tr_employee_practice');
			//var practiceLookUP = nlapiLookupField('department',practice,'custrecord_hrbusinesspartner',true);
			// Commented by shravan for HRBP integration on 7-dec-2020
			var i_travel_id = search_company_holiday[i].getValue('internalid');
			
			var cols_ = [];
			cols_.push(new nlobjSearchColumn('custrecord_trli_departure_date').setSort());
			cols_.push(new nlobjSearchColumn('custrecord_trli_origin_city'));
			cols_.push(new nlobjSearchColumn('custrecord_trli_destination_city'));
			cols_.push(new nlobjSearchColumn('custrecord_trli_travel_request'));
			
			
			var search_travel_line = nlapiSearchRecord('customrecord_tr_travel_request_line_item',
			        null, [new nlobjSearchFilter('custrecord_trli_travel_request', null, 'anyof',
			        		i_travel_id)], cols_);
			
			if(search_travel_line){
				JSON_List = {};
				dataRow = [];
				nlapiLogExecution('debug', 'search_travel_line', search_travel_line.length);
				for(var j=0;j<search_travel_line.length;j++){
					JSON_List = {
							Date: search_travel_line[j].getValue('custrecord_trli_departure_date'),
							Origin: search_travel_line[j].getText('custrecord_trli_origin_city'),
							Destination: search_travel_line[j].getText('custrecord_trli_destination_city'),
							TravelID: search_travel_line[j].getText('custrecord_trli_travel_request'),	
					};
					dataRow.push(JSON_List);					
				}
			}
			/* Added by Shravan on june 2021 for HRBP Changes */
			var i_Employee_id = search_company_holiday[i].getValue('custrecord_tr_employee');
			nlapiLogExecution('debug', 'i_Employee_id', i_Employee_id);
			var practiceLookUP = '';
			if(i_Employee_id)
			practiceLookUP = nlapiLookupField('employee',i_Employee_id,'custentity_emp_hrbp',true);
		/* Added by Shravan on june 2021 for HRBP Changes */
			JSON = {
				TravelID: search_company_holiday[i].getValue('name'),
				RequestType: search_company_holiday[i].getText('custrecord_tr_request_type'),
				TravelType:	search_company_holiday[i].getText('custrecord_tr_travel_type'),
				Employee: search_company_holiday[i].getText('custrecord_tr_employee'),
				EmployeeEmail: search_company_holiday[i].getValue('email','custrecord_tr_employee'),
				Project: search_company_holiday[i].getText('custrecord_tr_project'),
				TRDescription: search_company_holiday[i].getValue('custrecord_tr_short_description'),
				Purpose: search_company_holiday[i].getText('custrecord_tr_purpose_of_travel'),
				TravelStatus: search_company_holiday[i].getText('custrecord_tr_status'),
				EmployeePractice: search_company_holiday[i].getText('custrecord_tr_employee_practice'),
				HrBusinessPartner: practiceLookUP,
				DepartureDate: search_company_holiday[i].getValue('custrecord_tr_departure_date'),
				VisaCategory: search_company_holiday[i].getText('custrecord_tr_visa_category'),
				OnsiteDuration: search_company_holiday[i].getValue('custrecord_tr_onsite_duration'),
				GTAllocationStartDate: search_company_holiday[i].getValue('custrecord_tr_gt_allocation_start_date'),
				GTAllocationEndDate: search_company_holiday[i].getValue('custrecord_tr_gt_allocation_end_date'),
				TravelLines: dataRow
			};
			holiday_list.push(JSON);
		}
		/*JSON_Total = {
				TravelData: holiday_list,
				TravelLines: dataRow
		};
		dataRow_All.push(JSON_Total);*/
		}
		}
	}
	return holiday_list;
}
	catch(err){
		nlapiLogExecution('error', 'Restlet Holiday Function', err);
		throw err;
	}
}