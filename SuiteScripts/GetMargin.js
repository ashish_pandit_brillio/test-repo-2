/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       03 Jul 2019     Aazamali Khan
 *
 */

/**
 * @param {nlobjRequest} request Request object
 * @param {nlobjResponse} response Response object
 * @returns {Void} Any output is written via response object
 */
function getMarginDetails(request, response){
	var int_project_id = request.getParameter("projectId");
	var resultArray = getMargin(int_project_id);
	//nlapiLogExecution("DEBUG","resultArray : ",JSON.stringify(resultArray))
  	response.write(JSON.stringify(resultArray));
}