//https://debugger.na1.netsuite.com/app/common/scripting/script.nl?id=1109
function scheduled() {
    try {
		var excel_file_obj = '';	
		var err_row_excel = '';	
		var strVar_excel = '';	
         /*----------Added by Koushalya 28/12/2021---------*/
         var filters_customrecord_revenue_location_subsidiarySearch=["custrecord_offsite_onsite","anyof","2"];
         var obj_rev_loc_subSrch= Search_revenue_Location_subsdidary(filters_customrecord_revenue_location_subsidiarySearch);
         var obj_offsite_onsite=obj_rev_loc_subSrch["Onsite/Offsite"];
         var onsite_offsite = '';
          /*-------------------------------------------------*/
 
		strVar_excel += '<table>';	
		strVar_excel += '	<tr>';	
		strVar_excel += ' <td width="100%">';	
		strVar_excel += '<table width="100%" border="1">';	
		strVar_excel += '	<tr>';	
		strVar_excel += ' <td width="6%" font-size="11" align="center">Transaction Type</td>';	
		strVar_excel += ' <td width="6%" font-size="11" align="center">Transaction Number</td>';	
		strVar_excel += ' <td width="6%" font-size="11" align="center">Transaction Internal ID</td>';	
		strVar_excel += ' <td width="6%" font-size="11" align="center">Error Message</td>';	
		strVar_excel += '	</tr>';
        var currentContext_C = nlapiGetContext();
        //var inv_list = ['461534','461431','461535','461532','461533','463038','465828','464764','464763','464866','467024','467134','468145','468475','468499','468497'];
        var inv_list = ['483945'];
        var invList = [];
        //var search = nlapiSearchRecord('transaction','customsearch1878');
        //if(search){
        for (var i = 0; i < inv_list.length; i++) {
			
            if (invList.indexOf(inv_list[i]) < 0) {
				                    	

                //Test

                var record = nlapiLoadRecord('invoice', inv_list[i]);
				try{
				var transactionNum = record.getFieldValue('tranid');
                record.setFieldText('custbody_inv_tax_code', 'Service Tax @15%');
                var customer = record.getFieldText('entity');
                var region_id_cust = nlapiLookupField('customer', record.getFieldValue('entity'), 'custentity_region');

                var project = record.getFieldText('job');
                var proj_internal_id = record.getFieldValue('job');
                var proj_rcrd = nlapiLoadRecord('job', proj_internal_id);
                var billing_type = proj_rcrd.getFieldText('jobbillingtype');
                var region_id = proj_rcrd.getFieldValue('custentity_region');
                if (!region_id)
                    region_id = region_id_cust;

                var proj_name = proj_rcrd.getFieldValue('altname');
                var proj_category = proj_rcrd.getFieldText('custentity_project_allocation_category');

                var billing_from_date = record.getFieldValue('custbody_billfrom');
                var billing_to_date = record.getFieldValue('custbody_billto');
                var i_billable_time_count = record.getLineItemCount('time');
                nlapiLogExecution('AUDIT', 'i_billable_time_count', i_billable_time_count);
                var territory = nlapiLookupField('job', record.getFieldValue('job'), 'customer.territory');

                nlapiLogExecution('AUDIT', nlapiGetRecordId(), 'Project: ' + project + ', Customer: ' + customer);

                var a_employee_names = new Object();
                var a_resource_allocations = new Array();
                var emp_list = new Array();

                if (_logValidation(billing_from_date)) {
                    billing_from_date = nlapiStringToDate(billing_from_date);
                    billing_to_date = nlapiStringToDate(billing_to_date);

                    var filters_search_allocation = new Array();
                    filters_search_allocation[0] = new nlobjSearchFilter('project', null, 'anyof', proj_internal_id);
                    filters_search_allocation[1] = new nlobjSearchFilter('startdate', null, 'onorbefore', billing_to_date);
                    filters_search_allocation[2] = new nlobjSearchFilter('enddate', null, 'onorafter', billing_from_date);
                    var columns = new Array();
                    columns[0] = new nlobjSearchColumn('resource');
                    columns[1] = new nlobjSearchColumn('employeetype', 'employee');
                    columns[2] = new nlobjSearchColumn('custentity_persontype', 'employee');
                    columns[3] = new nlobjSearchColumn('subsidiary', 'employee');
                    columns[4] = new nlobjSearchColumn('custentity_fusion_empid', 'employee');
                    columns[5] = new nlobjSearchColumn('firstname', 'employee');
                    columns[6] = new nlobjSearchColumn('middlename', 'employee');
                    columns[7] = new nlobjSearchColumn('lastname', 'employee');
                    columns[8] = new nlobjSearchColumn('companyname', 'customer');
                    columns[9] = new nlobjSearchColumn('entityid', 'customer');
                    columns[10] = new nlobjSearchColumn('entityid', 'job');
                    columns[11] = new nlobjSearchColumn('custentity_legal_entity_fusion', 'employee');
                    var project_allocation_result = nlapiSearchRecord('resourceallocation', null, filters_search_allocation, columns);
                    if (_logValidation(project_allocation_result)) {
                        for (var i_search_indx = 0; i_search_indx < project_allocation_result.length; i_search_indx++) {
                            var i_employee_id = project_allocation_result[i_search_indx].getValue('resource');
                            var s_emp_type = project_allocation_result[i_search_indx].getText('employeetype', 'employee');
                            var s_emp_person_type = project_allocation_result[i_search_indx].getText('custentity_persontype', 'employee');
                            var i_emp_subsidiary = project_allocation_result[i_search_indx].getValue('subsidiary', 'employee');
                            var fusion_id = project_allocation_result[i_search_indx].getValue('custentity_fusion_empid', 'employee');
                            var first_name = project_allocation_result[i_search_indx].getValue('firstname', 'employee');
                            var middl_name = project_allocation_result[i_search_indx].getValue('middlename', 'employee');
                            var lst_name = project_allocation_result[i_search_indx].getValue('lastname', 'employee');
                            var cust_id = project_allocation_result[i_search_indx].getValue('entityid', 'customer');
                            var cust_name = project_allocation_result[i_search_indx].getValue('companyname', 'customer');
                            var proj_entity_id = project_allocation_result[i_search_indx].getValue('entityid', 'job');
                            var custentity_legal_entity_fusion = project_allocation_result[i_search_indx].getValue('custentity_legal_entity_fusion', 'employee');
                            a_resource_allocations[i_search_indx] = {
                                'emp_id': i_employee_id,
                                'emp_type': s_emp_type,
                                'person_type': s_emp_person_type,
                                'subsidiary': i_emp_subsidiary,
                                'fusion_id': fusion_id,
                                'frst_name': first_name,
                                'mddl_name': middl_name,
                                'lst_name': lst_name,
                                'cust_id': cust_id,
                                'cust_name': cust_name,
                                'proj_entity_id': proj_entity_id,
                                'custentity_legal_entity_fusion': custentity_legal_entity_fusion
                            };
                            emp_list.push(i_employee_id);
                        }
                    }
                }

                for (var i = 1; i <= i_billable_time_count; i++) {
                    var emp_full_name = '';
                    var isApply = record.getLineItemValue('time', 'apply', i);

                    var item = record.getLineItemValue('time', 'item', i);
                    if (currentContext_C.getRemainingUsage() <= 1000) {
                        nlapiYieldScript();
                    }
                    if (isApply == 'T' && (item == '2221' || item == '2222' || item == '2425')) {
                        var employeeId = record.getLineItemValue('time', 'employee', i);
                        if (a_employee_names[employeeId] == undefined) {
                            a_employee_names[employeeId] = nlapiLookupField('employee', employeeId, 'entityid');
                        }
                        var employeeName = a_employee_names[employeeId]; //

                        record.setLineItemValue('time', 'taxcode', i, 2552);
                        record.setLineItemValue('time', 'custcol_employeenamecolumn', i, employeeName); // Set Employee Service Tax:Service Tax @15%
                        record.setLineItemValue('time', 'custcolprj_name', i, project); // Set Project
                        record.setLineItemValue('time', 'custcolcustcol_temp_customer', i, customer); // Set Customer
                        record.setLineItemValue('time', 'custcol_territory', i, territory);

                        if (emp_list.indexOf(employeeId) >= 0) {
                            var emp_posi = emp_list.indexOf(employeeId);
                            var emp_subsidiary = a_resource_allocations[emp_posi].subsidiary;
                            var entity_fusion = a_resource_allocations[emp_posi].custentity_legal_entity_fusion;

                            if (emp_subsidiary == 3 && entity_fusion == 'Brillio Technologies Private Limited UK') {
                                onsite_offsite = 'Onsite';
                            }
                            else{
                                onsite_offsite = obj_offsite_onsite[emp_subsidiary]?'Offshore':'Onsite';    
                            }
                            /* else if (emp_subsidiary == 3 || emp_subsidiary == 12) {
                                var onsite_offsite = 'Offsite';
                            } else {
                                var onsite_offsite = 'Onsite';
                            }*/

                            record.setLineItemValue('time', 'custcol_employee_type', i, a_resource_allocations[emp_posi].emp_type);
                            record.setLineItemValue('time', 'custcol_person_type', i, a_resource_allocations[emp_posi].person_type);
                            record.setLineItemValue('time', 'custcol_onsite_offsite', i, onsite_offsite);
                            record.setLineItemValue('time', 'custcol_billing_type', i, billing_type);

                            record.setLineItemValue('time', 'custcol_customer_entityid', i, a_resource_allocations[emp_posi].cust_id);
                            record.setLineItemValue('time', 'custcol_cust_name_on_a_click_report', i, a_resource_allocations[emp_posi].cust_name);

                            record.setLineItemValue('time', 'custcol_project_entity_id', i, a_resource_allocations[emp_posi].proj_entity_id);
                            record.setLineItemValue('time', 'custcol_proj_name_on_a_click_report', i, proj_name);
                            record.setLineItemValue('time', 'custcol_region_master_setup', i, region_id);
                            record.setLineItemValue('time', 'custcol_proj_category_on_a_click', i, proj_category);


                            if (a_resource_allocations[emp_posi].frst_name)
                                emp_full_name = a_resource_allocations[emp_posi].frst_name;

                            if (a_resource_allocations[emp_posi].mddl_name)
                                emp_full_name = emp_full_name + ' ' + a_resource_allocations[emp_posi].mddl_name;

                            if (a_resource_allocations[emp_posi].lst_name)
                                emp_full_name = emp_full_name + ' ' + a_resource_allocations[emp_posi].lst_name;

                            record.setLineItemValue('time', 'custcol_employee_entity_id', i, a_resource_allocations[emp_posi].fusion_id);
                            record.setLineItemValue('time', 'custcol_emp_name_on_a_click_report', i, emp_full_name);

                            nlapiLogExecution('debug', 'Line Updated', i);

                            if (currentContext_C.getRemainingUsage() <= 1000) {
                                nlapiYieldScript();
                            }
                        }
                    }
                }

                //End 

                //var id = nlapiLoadRecord('invoice',inv_list[i]);
                //var status = id.getFieldValue('status');
                //nlapiLogExecution('debug','Record status',status);
                //id.setFieldText('custbody_inv_tax_code','Service Tax @15%');
                if (currentContext_C.getRemainingUsage() <= 1000) {
                    nlapiYieldScript();
                }

                var recid = nlapiSubmitRecord(record, false, true);
                if (currentContext_C.getRemainingUsage() <= 1000) {
                    nlapiYieldScript();
                }
                //nlapiSubmitField('journalentry',inv_list[i],'approved','T');
                nlapiLogExecution('debug', 'Record Updated', inv_list[i]);
                invList.push(recid);
				 } catch (err) {	
                    //Added try-catch logic by Sitaram 06/08/2021	
                    nlapiLogExecution('debug', 'err', err);	
                    err_row_excel += '	<tr>';	
                    err_row_excel += ' <td width="6%" font-size="11" align="center">' + 'invoice' + '</td>';	
                    err_row_excel += ' <td width="6%" font-size="11" align="center">' + transactionNum + '</td>';	
                    err_row_excel += ' <td width="6%" font-size="11" align="center">' + inv_list[i] + '</td>';	
                    err_row_excel += ' <td width="6%" font-size="11" align="center">' + err.message + '</td>';	
                    err_row_excel += '	</tr>';	
                    //continue;	
                }
            }
        } //for loop
		 if (_logValidation(err_row_excel)) {	
            var tailMail = '';	
            tailMail += '</table>';	
            tailMail += ' </td>';	
            tailMail += '</tr>';	
            tailMail += '</table>';	
            strVar_excel = strVar_excel + err_row_excel + tailMail	
            //excel_file_obj = generate_excel(strVar_excel);	
            var mailTemplate = "";	
            mailTemplate += '<html>';	
            mailTemplate += '<body>';	
            mailTemplate += "<p> This is to inform that following transactions are having Errors and therefore, system is not able to set the project/MIS mandatory fields</p>";	
            mailTemplate += "<p> Please make sure, transaction is updated with proper details else may not able to see the project/customer/employee details at Income Statement level.</p>";	
            mailTemplate += "<br/>"	
            mailTemplate += "<p><b> Script Id:- " + currentContext_C.getScriptId() + "</b></p>";	
            mailTemplate += "<p><b> Script Deployment Id:- " + currentContext_C.getDeploymentId() + "</b></p>";	
            mailTemplate += "<br/>"	
            mailTemplate += strVar_excel	
            mailTemplate += "<br/>"	
            mailTemplate += "<p>Regards, <br/> Information Systems</p>";	
            mailTemplate += '</body>';	
            mailTemplate += '</html>';	
            nlapiSendEmail(442, 'billing@brillio.com', 'Issue with updating the MIS data for the transactions', mailTemplate, 'netsuite.support@brillio.com', null, null, null);	
        }
		
        //}
        /* var search = nlapiSearchRecord('timesheet','customsearch1788',null,[new nlobjSearchColumn('internalid')]);
        nlapiLogExecution('debug','Record search',search.length);
        var msg = 'On Request from Nikhil';
        if(search){
        for(var i=0;i<search.length;i++){
        var id = search[i].getValue('internalid');
        if(id){
        var newTimesheet = nlapiLoadRecord('timesheet',parseInt(id));
        var employee = newTimesheet.getFieldText('employee');
        var employee = newTimesheet.setFieldValue('custrecord_ts_remark',msg);
        var status ;
        						var timesheetId = nlapiSubmitRecord(newTimesheet);
        						//nlapiDeleteRecord('timesheet',parseInt(timesheetId));
        						//nlapiLogExecution('debug','Record Deleted',id);
        						var timesheetId_Emp =timesheetId + employee+' , ';
        						nlapiLogExecution('debug', 'timesheetId + Date', timesheetId_Emp);

        					
        }
        nlapiDeleteRecord('timesheet',parseInt(id));
        nlapiLogExecution('debug','Record Deleted',i);
        }
        } */
    } catch (e) {
        nlapiLogExecution('debug', 'Error in Deletion', e);
    }
}

function yieldScript(currentContext) {

    nlapiLogExecution('AUDIT', 'API Limit Exceeded');
    var state = nlapiYieldScript();

    if (state.status == "FAILURE") {
        nlapiLogExecution('ERROR', 'Yield Script Failed', 'Reason : ' +
            state.reason + ' / Size : ' + state.size);
        return false;
    } else if (state.status == "RESUME") {
        nlapiLogExecution('AUDIT', 'Script Resumed');
    }
}

function _logValidation(value) {
    if (value != null && value.toString() != null && value != '' && value != undefined && value.toString() != undefined && value != 'undefined' && value.toString() != 'undefined' && value.toString() != 'NaN' && value != NaN) {
        return true;
    } else {
        return false;
    }
}