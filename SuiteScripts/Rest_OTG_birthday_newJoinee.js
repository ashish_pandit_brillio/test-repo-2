

var monthArray = [' ','Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
function postRESTlet_birthday_newJoinee(dataIn) {
	var response = new Response();
	var current_date = nlapiDateToString(new Date());
	//Log for current date
	nlapiLogExecution('DEBUG', 'Current Date', 'current_date...' + current_date)
	
	//nlapiLogExecution('debug', 'datain', dataIn);
	
	nlapiLogExecution('debug', 'dataIn', JSON.stringify(dataIn));
	nlapiLogExecution('debug', 'Email id', dataIn.EmailId);
	//var emailId  = 'arjun.r@brillio.com';
	var response = new Response();
	var currentDate_ = nlapiStringToDate(current_date);
	var currentDate = currentDate_.getDate();
	var currentMonth = currentDate_.getMonth();
	var currentYear = currentDate_.getFullYear();
	nlapiLogExecution('debug', 'currentYear', currentYear);
	try {
	//var employeeId = getUserUsingEmailId(emailId);
	
		var employeeId = getUserUsingEmailId(dataIn.EmailId);
		var flag_R = false;
		var flag_projectManager = false;
		//Search If logged user is reporting manager or not
		var filter = [];
		filter[0] = new nlobjSearchFilter('custentity_reportingmanager',null,'anyof',employeeId); //custentity_employee_inactive
		filter[1] = new nlobjSearchFilter('custentity_employee_inactive',null,'is','F');
		
		var col = [];
		col[0] = new nlobjSearchColumn('internalid');
		col[1] = new nlobjSearchColumn('entityid');
		col[2] = new nlobjSearchColumn('firstname');
		
		var reportingManager_Res = nlapiSearchRecord('employee',null,filter,col);
		if(reportingManager_Res){
			nlapiLogExecution('debug', 'Reportee Count', reportingManager_Res.length);
			if(reportingManager_Res.length > 0){
				flag_R = true;
			}
		}
		
		//Check if the logged in user is PM or not --- CODEX
		var statusList = [2,4]; //Active && Pending
		var filter_P = [];
		filter_P[0] = new nlobjSearchFilter('custentity_projectmanager',null,'anyof',employeeId); //custentity_employee_inactive
		filter_P[1] = new nlobjSearchFilter('status',null,'anyof',statusList); //jobtype
		filter_P[2] = new nlobjSearchFilter('jobtype',null,'anyof',2); 
		filter_P[3] = new nlobjSearchFilter('isinactive', null, 'is', 'F');
		
		var col_P = [];
		col_P[0] = new nlobjSearchColumn('internalid');
		col_P[1] = new nlobjSearchColumn('entityid');
		col_P[2] = new nlobjSearchColumn('custentity_projectmanager');
		
		
		var project_Manager_Res = nlapiSearchRecord('job',null,filter_P,col_P);
		if(project_Manager_Res){
			nlapiLogExecution('debug', 'PM Flag Count', project_Manager_Res.length);
			if(project_Manager_Res.length > 0){
				flag_projectManager = true;
			}
		}
		
		var emp_lookUp_Obj = nlapiLookupField('employee',parseInt(employeeId),['subsidiary','department','hiredate','entityid']);
		var i_subsidiary_V = emp_lookUp_Obj.subsidiary;
		var i_department_V = emp_lookUp_Obj.department;
		var i_hiredate_V = emp_lookUp_Obj.hiredate;
		var logged_user = emp_lookUp_Obj.entityid;
		nlapiLogExecution('debug','subsidiary',i_subsidiary_V);
		
		
		//-------------------------------------------------------------------------------
		
		if (employeeId) {
			
			//var employeeLookUp = nlapiLookupField('employee',employeeId,'department');
			//var i_department = employeeLookUp.department;
			
			
			//Get Birthday's
			var filters = [];
			filters.push(new nlobjSearchFilter('department',null,'anyof',i_department_V));
			filters.push(new nlobjSearchFilter('custentity_employee_inactive',null,'is','F'));
			//filters.push(new nlobjSearchFilter('birthdate',null,'onorafter','today'));
			
			var cols = [];
			cols.push(new nlobjSearchColumn('birthdate').setSort(true));
			cols.push(new nlobjSearchColumn('department'));
			cols.push(new nlobjSearchColumn('firstname'));
			cols.push(new nlobjSearchColumn('middlename'));
			cols.push(new nlobjSearchColumn('lastname'));
			cols.push(new nlobjSearchColumn('email'));
			cols.push(new nlobjSearchColumn('phone'));
			
			var dataRow_Bday = [];
			var JSON_Bday = {};
			var sort_JSON ={};
			var sort_Array = [];
			var counter_Bday = 0;
			var i_flagg = 0;
			var bday_list_ = new Array();
			//var searchEmployeeBthDay = nlapiSearchRecord('employee','customsearch1747',filters); //Sandbox
			var searchEmployeeBthDay = nlapiSearchRecord('employee','customsearch_employee_search_bday',filters);  //Prod
						
						if(searchEmployeeBthDay){
							for(var i=0;i<searchEmployeeBthDay.length;i++){
								var cols_R = searchEmployeeBthDay[i].getAllColumns();
								var s_resource = searchEmployeeBthDay[i].getValue(cols_R[0]);
								//var s_resource = searchEmployeeBthDay[i].getValue(cols_R[0]);
								var birthdate = searchEmployeeBthDay[i].getValue(cols_R[1]);
								var d_birthdate = nlapiStringToDate(birthdate);
								
								//var birthdate = searchEmployeeBthDay[i].getValue('email');
								
								
								if(d_birthdate && d_birthdate < currentDate_){
								var s_birthday = nlapiAddMonths(d_birthdate,12);
								s_birthday = nlapiDateToString(s_birthday);
							//	nlapiLogExecution('Debug','s_birthday',s_birthday);
								}
								else{
								var s_birthday = searchEmployeeBthDay[i].getValue(cols_R[1]);
								}
								if(birthdate){
								birthdate = nlapiStringToDate(birthdate);
							
								var b_date = birthdate.getDate();
								var b_month = birthdate.getMonth() + 1;
								var b_year = birthdate.getFullYear();
								var b_month_T = monthArray[b_month];
								var s_append_date = b_month + '/' + b_date + '/' + currentYear;
							//	var b_year = birthdate.getFullYear();
								
								var d_append_date = nlapiStringToDate(s_append_date);
								

							//	nlapiLogExecution('Debug','d_append_date',d_append_date);
								var diffDays = 0;
								if(d_append_date){
								//Logic To Sort Dates
								var timeDiff =  d_append_date.getTime() - new Date().getTime();
								diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
								}
								//var dateDiff = d_append_date - currentDate_ ; 
								sort_JSON = {
										Value: diffDays,
										Date : s_birthday,
										Month: b_month_T,
										BirthDate:b_date,	 
										Email: searchEmployeeBthDay[i].getValue(cols_R[3]),
										Name: searchEmployeeBthDay[i].getValue(cols_R[0]),
										Phone: searchEmployeeBthDay[i].getValue(cols_R[2]),
										IN: '',
										ImageURL :'img/image_10.jpg'
								};
								dataRow_Bday.push(sort_JSON);
								
						}
					}
					
				}
				dataRow_Bday.sort(predicateBy("Value"));
				
				//New Joinee's
			var filters = [];
			filters.push(new nlobjSearchFilter('department',null,'is',i_department_V));
			filters.push(new nlobjSearchFilter('custentity_employee_inactive',null,'is','F'));
			//filters.push(new nlobjSearchFilter('birthdate',null,'onorafter','today'));
			
			var cols = [];
			cols.push(new nlobjSearchColumn('hiredate').setSort(true));
			cols.push(new nlobjSearchColumn('department'));
			cols.push(new nlobjSearchColumn('firstname'));
			cols.push(new nlobjSearchColumn('lastname'));
			
			var dataRow_Joinee = [];
			var JSON_Joinee = {};
			var counter_Joinee = 0;
			var searchEmployee_Joinee = nlapiSearchRecord('employee',null,filters,cols);
			if(searchEmployee_Joinee){
				for(var i=0;i<searchEmployee_Joinee.length;i++){
					var s_resource = searchEmployee_Joinee[i].getValue('firstname');
					var hiredate = searchEmployee_Joinee[i].getValue('hiredate');
					if(hiredate){
						hiredate = nlapiStringToDate(hiredate);
				
					var b_date = hiredate.getDate();
					var b_month = hiredate.getMonth()+1;
					//if( counter_Joinee < 3 )
						{
							counter_Joinee ++;
							JSON_Joinee = {
									Name:searchEmployee_Joinee[i].getValue('firstname')+' '+searchEmployee_Joinee[i].getValue('lastname'),
									HireDate:searchEmployee_Joinee[i].getValue('hiredate'),
									IN: '',
									ImageURL :'img/image_10.jpg'
							};
							dataRow_Joinee.push(JSON_Joinee);
						}
				   }  
			    }
		    }
				
		
			
			var first_JSON = {};
			var first_Array = [];
			
			var second_JSON = {};
			var second_Array = [];
			var final_Array = [];
			
			first_JSON = {
         			
         			PeerBirthday: dataRow_Bday,
         			
         			NewJoinees: dataRow_Joinee,
         			
			};
			first_Array.push(first_JSON);
			
			
			
			
			//Check logged user is Expesne Approver
			var flag_E = false;
			var filter = [];
			filter[0] = new nlobjSearchFilter('approver',null,'anyof',employeeId); //custentity_employee_inactive
			filter[1] = new nlobjSearchFilter('custentity_employee_inactive',null,'is','F');
		
			var col = [];
			col[0] = new nlobjSearchColumn('internalid');
			col[1] = new nlobjSearchColumn('entityid');
			col[2] = new nlobjSearchColumn('firstname');
		
			var expenseApprover = nlapiSearchRecord('employee',null,filter,col);
			if(expenseApprover){
			nlapiLogExecution('debug', 'Reportee Count', expenseApprover.length);
			if(expenseApprover.length > 0){
				flag_E = true;
				}
			}
			//Check if logged user is Time Approver
			var flag_T = false;
			var filter = [];
			filter[0] = new nlobjSearchFilter('timeapprover',null,'anyof',employeeId); //custentity_employee_inactive
			filter[1] = new nlobjSearchFilter('custentity_employee_inactive',null,'is','F');
		
			var col = [];
			col[0] = new nlobjSearchColumn('internalid');
			col[1] = new nlobjSearchColumn('entityid');
			col[2] = new nlobjSearchColumn('firstname');
		
			var timeApprover = nlapiSearchRecord('employee',null,filter,col);
			if(timeApprover){
			nlapiLogExecution('debug', 'Reportee Count', timeApprover.length);
			if(timeApprover.length > 0){
				flag_T = true;
				}
			}
			//Check if logged user is DM/CP/Practice Head
			var flag_P = false;
			var jobSearch = nlapiSearchRecord("job",null,
			[
				[[["custentity_deliverymanager","anyof",employeeId]],
				"OR",
				[["custentity_clientpartner","anyof",employeeId]],
				"OR",
				[["custentity_practice.custrecord_practicehead","anyof",employeeId]],
				"AND",[["custentity_deliverymanager.custentity_employee_inactive","is","F"]],
				"AND",[["custentity_clientpartner.custentity_employee_inactive","is","F"]]]
			], 
			[
				new nlobjSearchColumn("custentity_deliverymanager"), 
				new nlobjSearchColumn("custentity_clientpartner"), 
				new nlobjSearchColumn("custrecord_practicehead","CUSTENTITY_PRACTICE",null)
			]
			);
			if(_logValidation(jobSearch))
			{
				if(jobSearch.length > 0)
				{
					flag_P = true;
				}
			}
			
			
			
			var final_JSON = {};
         
			// response
			response.Data = [
			                 	final_JSON = {
			                 			CurrentDate: current_date,
			                 			ProjectManagerFlag: flag_projectManager,
                                  		ReportingManager : flag_R,
			                 			RequestJSON: first_Array,
			                 			
			                 	}
			                	
			                 			
			                 			
			                 			
			                 	
		];
			
			nlapiLogExecution('DEBUG','response.Data',JSON.stringify(response.Data));
			response.Status = true;
		} else {
			throw "No Employee Id Provided";
		}
	}
	 catch (err) {
		nlapiLogExecution('ERROR', 'postRESTlet_birthday_newJoinee', err);
		response.Data = err;
	}
return response;
}

var sort_by = function(field, reverse, primer){

	   var key = primer ? 
	       function(x) {return primer(x[field])} : 
	       function(x) {return x[field]};

	   reverse = !reverse ? 1 : -1;

	   return function (a, b) {
	       return a = key(a), b = key(b), reverse * ((a > b) - (b > a));
	     } 
	}

function predicateBy(prop){
	   return function(a,b){
	      if( a[prop] > b[prop]){
	          return 1;
	      }else if( a[prop] < b[prop] ){
	          return -1;
	      }
	      return 0;
	   }
	}
//Function to remove duplicates
function removearrayduplicate(array) {
    var newArray = new Array();
    label: for (var i = 0; i < array.length; i++) {
          for (var j = 0; j < array.length; j++) {
                if (newArray[j] == array[i])
                      continue label;
          }
          newArray[newArray.length] = array[i];
    }
    return newArray;
}

function _logValidation(value) {
    if (value != null && value.toString() != null && value != '' && value != undefined && value.toString() != undefined && value != 'undefined' && value.toString() != 'undefined' && value.toString() != 'NaN' && value != NaN) {
        return true;
    } else {
        return false;
    }
}