/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       19 Jan 2018     bidhi.raj
 *
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your
 * script deployment.
 * 
 * @appliedtorecord recordType
 * 
 * @param {String}
 *            type Operation types: create, edit, view, copy, print, email
 * @param {nlobjForm}
 *            form Current form
 * @param {nlobjRequest}
 *            request Request object
 * @returns {Void}
 */
var status = '';
function userbefrsubmit(type, form, request) {
	if(type == 'create' ){

		var filter = new Array();
		var column = new Array();
		column.push(new nlobjSearchColumn('enddate'));
		column.push(new nlobjSearchColumn('startdate'));
		column.push(new nlobjSearchColumn('percentoftime'));
		filter.push(new nlobjSearchFilter('project', null, 'is',
				nlapiGetFieldValue('project')));
		
		//var resAllocation = nlapiSearchRecord('resourceallocation','customsearch2271', filter, column);//sandbox
		var resAllocation = nlapiSearchRecord('resourceallocation','customsearch2349', filter, column);//production
		var proj_headcount = nlapiLookupField('job',
				nlapiGetFieldValue('project'),
				[ 'custentity_project_head_count' ]);
		var currentDate = new Date();
		var count = 0;
		var array = [];
		for ( var i in resAllocation) {
			var edate = nlapiStringToDate(resAllocation[i].getValue('enddate'));
			var sdate = nlapiStringToDate(resAllocation[i].getValue('startdate'));
			var allocation_percent = resAllocation[i].getValue('percentoftime');
			allocation_percent = parseInt(allocation_percent);
			if (edate >= currentDate && sdate <=currentDate) {//taking active allocations excluding future allocation
				var end = resAllocation[i].getValue('enddate');
				var star = resAllocation[i].getValue('startdate');
				count += parseFloat(allocation_percent/100);
				array.push(edate);
			}
		}
		var maxDate = new Date(Math.max.apply(null, array));
		var minDate = new Date(Math.min.apply(null, array));//first end date 
		var currentrecdStrtDate = nlapiGetFieldValue('startdate');
		currentrecdStrtDate = nlapiStringToDate(currentrecdStrtDate);
		if(proj_headcount.custentity_project_head_count && status != 'edit'){
		if (count > proj_headcount.custentity_project_head_count) {//chkz if actv alloctn eql or grtr thn hdcnt
          nlapiLogExecution('Debug','Count',count);
          nlapiLogExecution('Debug','Headcount',proj_headcount.custentity_project_head_count);
			if(currentrecdStrtDate < minDate){//if end date entered is less than the firt end dt
				nlapiLogExecution('debug', 'exception', 'Resource allocation exceeding head count');
				throw 'Resource allocation exceeding head count';//thrw exptn
			}else{
				nlapiLogExecution('debug', 'exception', 'else loop');
			}
			
		}
	}
	}


}
function userbeforeload(type, form, request) {
	status = type;	
}
