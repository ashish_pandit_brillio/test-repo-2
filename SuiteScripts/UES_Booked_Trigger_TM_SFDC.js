/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       22 Dec 2017     deepak.srinivas
 * 1.01       17 Jan 2020	  praveena Madem   Added condition not execute when type is delete.
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType
 * 
 * @param {String} type Operation types: create, edit, delete, xedit,
 *                      approve, cancel, reject (SO, ER, Time Bill, PO & RMA only)
 *                      pack, ship (IF only)
 *                      dropship, specialorder, orderitems (PO only) 
 *                      paybills (vendor payments)
 * @returns {Void}
 */
function userEventAfterSubmit(type){
  if(type=="delete"){return;}
	try{
		var dataRow = [];
		var JSON_ = {};
		var current_date = nlapiDateToString(new Date());
		var d_currentDate = nlapiStringToDate(current_date);
		
		var loadRec = nlapiLoadRecord(nlapiGetRecordType(),nlapiGetRecordId());
		var endDate = loadRec.getFieldValue('custeventbenddate');
		var d_endDate = nlapiStringToDate(endDate);
		var is_billable = loadRec.getFieldValue('custeventrbillable');
		var i_project = loadRec.getFieldValue('project');
		if(i_project){
			var projectLookup_billingType = nlapiLookupField('job',parseInt(i_project),'jobbillingtype');
			var projectLookup_billingType_T = nlapiLookupField('job',parseInt(i_project),'jobbillingtype',true);
		}
		if(is_billable == 'T' && d_endDate && d_endDate >= d_currentDate && projectLookup_billingType == 'TM'){
			
			var s_project = loadRec.getFieldText('project');
			if(s_project){
				var project_V = s_project.split(' ');
				var project_id = project_V[0];
				var project_name = project_V[1];
			}
			var SOW_ID = searchSow(i_project);
			JSON_ ={
				'ProjectID': nlapiLookupField('job',parseInt(i_project),'entityid'),
				'BillingType': projectLookup_billingType_T,
				'Status':'Booked',
				'SOWList':SOW_ID
			};
			dataRow.push(JSON_);
		//	 nlapiLogExecution('DEBUG','JSON',JSON.stringify(dataRow)); 
			//Include SFDC Integration call
			{
				var accessURL = getAccessToken();
		        var method = 'POST';
				
		        var response = nlapiRequestURL(accessURL,null,null,method);
		        var temp = response.body;
		        nlapiLogExecution('DEBUG','JSON',temp);
		        var data = JSON.parse(response.body);
		        var access_token_SFDC = data.access_token;
		        var instance_URL_Sfdc = data.instance_url;
		        nlapiLogExecution('DEBUG','JSON',data);
			        
			        var dynamic_URL = instance_URL_Sfdc+'/services/apexrest/Sowbooked/v1.0/';
			       
			        var auth = 'Bearer'+' '+access_token_SFDC;
			        
			        var headers = {"Authorization": "Bearer "+access_token_SFDC,
			        		 "Content-Type": "application/json",
			        		 "accept": "application/json"};
			       
			        var method_ = 'POST';
			        var response_ = nlapiRequestURL(dynamic_URL,JSON.stringify(dataRow),headers,method_);
			        var data = JSON.parse(response_.body);
			        var message = data[0].message;
			        var details_ = data.details;
			        nlapiLogExecution('DEBUG','Response SFDC',data); 
			    //    nlapiLogExecution('DEBUG','details_',details_); 
			        nlapiLogExecution('DEBUG','JSON',JSON.stringify(dataRow)); 
				
			}
			
			//End
		}
		
	}
	catch (err) {
		nlapiLogExecution('ERROR', 'After Submit Trigger', err);
		throw err;
	}
	
}
function searchSow(pro_id){
	try{
		var searchRes = nlapiSearchRecord('salesorder',null,[new nlobjSearchFilter('internalid','jobmain','anyof',pro_id),
		                                                     new nlobjSearchFilter('mainline',null,'is','T'),
		                                                     new nlobjSearchFilter('status',null,'anyof',['SalesOrd:F','SalesOrd:E','SalesOrd:G'])],
															[new nlobjSearchColumn('internalid'),
															 new nlobjSearchColumn('tranid'), 
															 new nlobjSearchColumn('custbody_opp_id_sfdc')]);
		var Sow_array = [];	
		if(searchRes){
			for(var i=0;i<searchRes.length;i++){
			Sow_array.push({
				SOW_ID: searchRes[i].getValue('tranid'),
				Opp_id: searchRes[i].getValue('custbody_opp_id_sfdc')
			});
			}
		}
		return Sow_array;
	}
	catch(e){
		nlapiLogExecution('DEBUG','searchSow Error',e);
	}
}