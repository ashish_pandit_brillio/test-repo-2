/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       16 March 2020     Prabhat Gupta
 *
 */

/**
 * @param {Object} dataIn Parameter object
 * @returns {Object} Output object
 */
 function getRESTlet(dataIn) {

	return {};
}

/**
 * @param {Object} dataIn Parameter object
 * @returns {Object} Output object
 */
function postRESTlet(dataIn) {

	try {
		
		//dataIn = {"lastUpdatedTimestamp":""}
        var response = new Response();
        //nlapiLogExecution('debug', 'dataIn', JSON.stringify(dataIn));
        var currentDateAndTime = sysDate();
		//Log for current date



		var receivedDate = dataIn.lastUpdatedTimestamp;
		//nlapiLogExecution('DEBUG', 'Current Date', 'receivedDate...' + receivedDate);
		nlapiLogExecution('DEBUG', 'Current Date', 'currentDateAndTime...' + currentDateAndTime);

		//var receivedDate = '7/12/2019 1:00 am';
		//Check if the timestamp is empty
		if (!_logValidation(receivedDate)) {          // for empty timestanp in this we will give whole data as a response
			var filters = new Array();
			filters = [
				
				["isinactive", "is", "F"],
				
				];
		//	nlapiLogExecution('debug', 'filters ', filters);
		} else {
			var filters = new Array();                             // this filter will provide the result within the current date and given date     
			filters = [
				
				["lastmodified", "within", receivedDate, currentDateAndTime]					
				];
		//	nlapiLogExecution('debug', 'filters ', filters);
		}
		
		// created search by grouping 
		var costPerFte_Search = searchRecord("customrecord_cost_per_fte",null,
				filters, 
				[
				   new nlobjSearchColumn("custrecord_cost_p_fte_fusion_id").setSort(false),
				   new nlobjSearchColumn("custrecord_cost_p_fte_salary_id"), 
                   new nlobjSearchColumn("custrecord_cost_p_fte_salary_basis"),
                   new nlobjSearchColumn("custrecord_cost_p_fte_currency_code"), 
				   new nlobjSearchColumn("custrecord_cost_p_fte_start_date"), 
				   new nlobjSearchColumn("custrecord_cost_p_fte_end_date"),
                    new nlobjSearchColumn("custrecord_cost_p_fte_action_name"),
                   new nlobjSearchColumn("custrecord_cost_p_fte_legal_entity"), 
				   new nlobjSearchColumn("custrecord_cost_p_fte_ctc_encrypted"), 
				   new nlobjSearchColumn("custrecord_cost_p_fte_employee"),                     
					new nlobjSearchColumn("custrecord_cost_p_fte_salary_encrypted")		// prabhat gupta NIS-1397 29/06/2020			 
					
				]
				);		
		
		
		
        
		if (costPerFte_Search) {
			
			var emp = 0;
			var temp_emp_id = 0;
			var temp_fusion_id = 0;
			var employeeCostPerFteData = [];
			for(var i=0; i<costPerFte_Search.length; i++){    
				
				var emp_id = costPerFte_Search[i].getValue("custrecord_cost_p_fte_employee");
				
				var emp_name = costPerFte_Search[i].getText("custrecord_cost_p_fte_employee");	

                 var fusion_id = costPerFte_Search[i].getValue("custrecord_cost_p_fte_fusion_id");			
				
				
				
				if((emp_id != temp_emp_id) || (fusion_id != temp_fusion_id)){
					
					if(emp != 0){
						data.costPerFte = ctc;
						employeeCostPerFteData.push(data)
						
					}
					
					var ctc = [];
					var data = {
						employeeInternalId: '',	
						employeeName: '',
                        fusionId : '',						
						costPerFte: ''					
				    };
					
					data.employeeInternalId = emp_id;
				    data.employeeName = emp_name;
					data.fusionId = fusion_id;
					
					emp = 1;
					
				}
	//------------------------------------------------------------------------------------------------------------------
// prabhat gupta NIS-1397 29/06/2020	
				var legalEntityId = costPerFte_Search[i].getValue("custrecord_cost_p_fte_legal_entity") || '';
				var salaryBasis = costPerFte_Search[i].getValue("custrecord_cost_p_fte_salary_basis") || '';
		if((legalEntityId == 2 && salaryBasis == 'Hourly Salary') || (legalEntityId == 10 && salaryBasis == 'Hourly Salary Canada')){
			
			var salaryAmount = costPerFte_Search[i].getValue("custrecord_cost_p_fte_salary_encrypted") || '';
			var annualCtc = costPerFte_Search[i].getValue("custrecord_cost_p_fte_ctc_encrypted") || '';
		}else{
			var salaryAmount = costPerFte_Search[i].getValue("custrecord_cost_p_fte_ctc_encrypted") || '';
			
		}
//--------------------------------------------------------------------------------------------------------------------------		
				
				ctc.push({
					"internalId" : costPerFte_Search[i].getId(),
					"salaryId" : costPerFte_Search[i].getValue("custrecord_cost_p_fte_salary_id") || '' , 
					"salaryBasis" : salaryBasis ,
					"currencyCode" : costPerFte_Search[i].getValue("custrecord_cost_p_fte_currency_code") || '',
					"startDate" : costPerFte_Search[i].getValue("custrecord_cost_p_fte_start_date") || '' , 
					"endDate" : costPerFte_Search[i].getValue("custrecord_cost_p_fte_end_date") || '' ,
					"actionName" : costPerFte_Search[i].getValue("custrecord_cost_p_fte_action_name") || '',
					"legalEntityId" :  legalEntityId , 
					"legalEntity" : costPerFte_Search[i].getText("custrecord_cost_p_fte_legal_entity") || '' ,
					"ctc" : salaryAmount,
					"annualCtc" : annualCtc || salaryAmount
					
					
				});
				
				if(i == (costPerFte_Search.length - 1) ){
					
					data.costPerFte = ctc;
					employeeCostPerFteData.push(data)
					
				}
				
																			
					temp_emp_id = 	emp_id;
                    temp_fusion_id = fusion_id;					
				
			}
			
			
		}
		
	//------------------------------------------------taleo record search-------------------------------------------------	
		
			filters = [];
			filters.push( ["isinactive", "is", "F"],"AND",["custrecord_taleo_ext_hire_status","isnot","Filled"],"AND",["custrecord_taleo_ext_hire_status","isnot","Canceled"],"AND",["custrecord_taleo_ext_hire_status","isnot","Rejected"],"AND",
						["custrecord_rrf_ctc_offered","isnotempty",""]);
			
		
		
		var taleo_external_hireSearch = nlapiSearchRecord("customrecord_taleo_external_hire",null,
					filters,
				[
					new nlobjSearchColumn("custrecord_taleo_ext_hire_status"), 
					new nlobjSearchColumn("custrecord_rrf_encrypted_ctc"),
					new nlobjSearchColumn("custrecord_rrf_currency_code"), 
					new nlobjSearchColumn("custrecord_rrf_salary_basis"),
				    new nlobjSearchColumn("custrecord_frf_details_frf_number","CUSTRECORD_TALEO_EXT_HIRE_FRF_DETAILS",null), 
                   new nlobjSearchColumn("custrecord_frf_details_ns_rrf_number","CUSTRECORD_TALEO_EXT_HIRE_FRF_DETAILS",null), 
                   new nlobjSearchColumn("internalid","CUSTRECORD_TALEO_EXT_HIRE_FRF_DETAILS",null)
						
				]);
				
			var costPerFteTaleoData = [];			
			if(taleo_external_hireSearch){
				
				
				
				
				for(var i = 0; i < taleo_external_hireSearch.length; i++){
				
					var rrf = {};
					
					rrf.rrfInternalId = taleo_external_hireSearch[i].getId();
					rrf.rrfNumber = taleo_external_hireSearch[i].getValue("custrecord_frf_details_ns_rrf_number","CUSTRECORD_TALEO_EXT_HIRE_FRF_DETAILS",null);
					rrf.rrfStatus = taleo_external_hireSearch[i].getValue("custrecord_taleo_ext_hire_status");
					rrf.rrfEncryptedCtc = taleo_external_hireSearch[i].getValue("custrecord_rrf_encrypted_ctc");
					rrf.rrfCurrencyCode = taleo_external_hireSearch[i].getValue("custrecord_rrf_currency_code");
					rrf.rrfSalaryBasis = taleo_external_hireSearch[i].getValue("custrecord_rrf_salary_basis");
					rrf.frfNumberInternalId = taleo_external_hireSearch[i].getValue("internalid","CUSTRECORD_TALEO_EXT_HIRE_FRF_DETAILS",null);
					rrf.frfNumber = taleo_external_hireSearch[i].getValue("custrecord_frf_details_frf_number","CUSTRECORD_TALEO_EXT_HIRE_FRF_DETAILS",null);
				
				   costPerFteTaleoData.push(rrf);
				}
			}
			
	//------------------------------------------------------------------------------------------------------------------------	// prabhat gupta NIS-1403 01/07/2020	
		
             var subtier_vendor_Search = nlapiSearchRecord("customrecord_subtier_vendor_data",null,
															[
															   ["custrecord_stvd_end_date","notbefore","startoflastrollinghalf"], 
															   "AND", 
															   ["isinactive","is","F"]
															], 
															[
															   new nlobjSearchColumn("custrecord_stvd_contractor"), 
															   new nlobjSearchColumn("custrecord_stvd_subsidiary"), 
															   new nlobjSearchColumn("custrecord_stvd_start_date"), 
															   new nlobjSearchColumn("custrecord_stvd_end_date"), 
															   new nlobjSearchColumn("custrecord_stvd_rate_type"), 
															   new nlobjSearchColumn("custrecord_stvd_st_pay_rate"), 
															   new nlobjSearchColumn("custrecord_stvd_ot_pay_rate"), 
															   new nlobjSearchColumn("custrecord_stvd_vendor"),
									                           new nlobjSearchColumn("custrecord_subtier_currency"),	   
															   new nlobjSearchColumn("custentity_fusion_empid","CUSTRECORD_STVD_CONTRACTOR",null)
															]
															);
			var thirdPartyContractorData = [];												
		if (subtier_vendor_Search) {
			
			var emp = 0;
			var temp_emp_id = 0;
			var temp_fusion_id = 0;
			
			for(var i=0; i<subtier_vendor_Search.length; i++){    
				
				var emp_id = subtier_vendor_Search[i].getValue("custrecord_stvd_contractor");
				
				var emp_name = subtier_vendor_Search[i].getText("custrecord_stvd_contractor");	

            var fusion_id = subtier_vendor_Search[i].getValue("custentity_fusion_empid","CUSTRECORD_STVD_CONTRACTOR",null);			
				
				
				
				if((emp_id != temp_emp_id) || (fusion_id != temp_fusion_id)){
					
					if(emp != 0){
						data.costPerFte = ctc;
						thirdPartyContractorData.push(data)
						
					}
					
					var ctc = [];
					var data = {
						employeeInternalId: '',	
						employeeName: '',
                        fusionId : '',						
						costPerFte: ''					
				    };
					
					data.employeeInternalId = emp_id;
				    data.employeeName = emp_name;
					data.fusionId = fusion_id;
					
					emp = 1;
					
			     var st_pay_rate = subtier_vendor_Search[i].getValue("custrecord_stvd_st_pay_rate") || '';
				 
				 if(st_pay_rate){
					 st_pay_rate = encrypt(st_pay_rate);
				 }
				
				ctc.push({
					"internalId" : subtier_vendor_Search[i].getId(),					
					"startDate" : subtier_vendor_Search[i].getValue("custrecord_stvd_start_date") || '' , 
					"endDate" : subtier_vendor_Search[i].getValue("custrecord_stvd_end_date") || '' ,					
					"legalEntityId" :  subtier_vendor_Search[i].getValue("custrecord_stvd_subsidiary") || '',
					"legalEntity" : subtier_vendor_Search[i].getText("custrecord_stvd_subsidiary") || '',
					"currencyCode" : subtier_vendor_Search[i].getText("custrecord_subtier_currency") || '',
					"salaryBasis" : subtier_vendor_Search[i].getText("custrecord_stvd_rate_type") || '',
					"ctc" : st_pay_rate					
					
				});
				
				if(i == (subtier_vendor_Search.length - 1) ){
					
					data.costPerFte = ctc;
					thirdPartyContractorData.push(data)
					
				}
				
																			
					temp_emp_id = 	emp_id;
                    temp_fusion_id = fusion_id;					
				
			}
			
			
		   }											
				
		}

				
	//-----------------------------------------------------------------------------------------------------------------------	
		

		response.timeStamp = currentDateAndTime;
		response.data = {
			"employeeCostPerFteData" : employeeCostPerFteData,
			"costPerFteTaleoData" : costPerFteTaleoData,
			"thirdPartyContractorData" : thirdPartyContractorData
		}
		//response.data = dataRow;
		response.status = true;
	//	nlapiLogExecution('debug', 'response',JSON.stringify(response));
	} catch (err) {
		nlapiLogExecution('ERROR', 'postRESTlet', err);
		response.timeStamp = '';
		response.data = err;
		response.status = false;
	}
	return response;

}
//validate blank entries
function _logValidation(value) {
	if (value != null && value.toString() != null && value != '' && value != undefined && value.toString() != undefined && value != 'undefined' && value.toString() != 'undefined' && value.toString() != 'NaN' && value != NaN) {
		return true;
	} else {
		return false;
	}
}
//Get current date


//Returns timestamp
function timestamp() {
	var str = "";

	var currentTime = new Date();
	var hours = currentTime.getHours();
	var minutes = currentTime.getMinutes();
	var seconds = currentTime.getSeconds();
	var meridian = "";
	if (hours > 12) {
		meridian += "pm";
	} else {
		meridian += "am";
	}
	if (hours > 12) {

		hours = hours - 12;
	}
	if (minutes < 10) {
		minutes = "0" + minutes;
	}
	if (seconds < 10) {
		seconds = "0" + seconds;
	}
	//str += hours + ":" + minutes + ":" + seconds + " ";
	str += hours + ":" + minutes + " ";
	return str + meridian;
}

function Response() {
	this.status = "";
	this.timeStamp = "";
	this.data = "";

}

//Search function for results more than 1000
function searchRecord(recordType, savedSearch, arrFilters, arrColumns,
		filterExpression) {

	try {
		var search = null;

		// if a saved search is provided, load it and add the filters and
		// columns
		if (savedSearch) {
			search = nlapiLoadSearch(recordType, savedSearch);

			if (isArrayNotEmpty(arrFilters)) {
				search.addFilters(arrFilters);
			}

			if (isArrayNotEmpty(arrColumns)) {
				search.addColumns(arrColumns);
			}

			if (isArrayNotEmpty(filterExpression)) {
				search.setFilterExpression(filterExpression);
			}
		}
		// create a new search
		else {
			search = nlapiCreateSearch(recordType, arrFilters, arrColumns);
		}

		// run search
		var resultSet = search.runSearch();

		// iterate through the search and get all data 1000 at a time
		var searchResultCount = 0;
		var resultSlice = null;
		var searchResult = [];

		do {
			resultSlice = resultSet.getResults(searchResultCount,
					searchResultCount + 1000);

			if (resultSlice) {

				resultSlice.forEach(function(result) {

					searchResult.push(result);
					searchResultCount++;
				});
			}
		} while (isArrayNotEmpty(resultSlice) && resultSlice.length >= 1000);

		return searchResult;
	} catch (err) {
		nlapiLogExecution('ERROR', 'searchRecord', err);
		throw err;
	}
}

function isEmpty(value) {

	return value == null || value == "" || typeof(value) == undefined;
}

function isNotEmpty(value) {

	return !isEmpty(value);
}

function isArrayEmpty(argArray) {

	return !isArrayNotEmpty(argArray);
}

function isArrayNotEmpty(argArray) {

	return (isNotEmpty(argArray) && argArray.length > 0);
}
//function to format date object into NetSuite's mm/dd/yyyy format.
function formatNSDate(dateObj) {
	if (dateObj) {
		var nsFormatDate = dateObj.getFullYear() + '-' + addZeros(dateObj.getMonth() + 1, 2) + '-' + addZeros(dateObj.getDate(), 2);
		return nsFormatDate;
	}
	return null;
}

//function to add leading zeros on date parts.
function addZeros(num, len) {
	var str = num.toString();
	while (str.length < len) {
		str = '0' + str;
	}
	return str;
}

function encrypt(data){
	
	var encrypted_data = nlapiEncrypt(data, "aes", "7442A2900DA9A2C7E537BA94F939F191");//48656C6C6F0B0B0B0B0B0B0B0B0B0B0B  // prabhat gupta NIS-1396 29/06/2020
	nlapiLogExecution('debug', 'encrypted data: ', encrypted_data);                    
	return encrypted_data;
	
}