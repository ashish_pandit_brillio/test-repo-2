/**
 * @author Jayesh
 */

function suiteletFunction_FP_RevRec(request,rsponse)
{
	try
	{
		{
			if(request.getMethod() == 'GET')
			{
				var o_context = nlapiGetContext();
				var i_user_logegdIn_id = o_context.getUser();
		
				var i_projectId = request.getParameter('proj_id');
				var s_request_mode = request.getParameter('mode');
				if (s_request_mode == 'excel') {
					exportFormToExcel(request);
				}
				var i_existing_revenue_share_rcrd_id = request.getParameter('existing_rcrd_id');
				if(i_existing_revenue_share_rcrd_id)
					i_existing_revenue_share_rcrd_id = i_existing_revenue_share_rcrd_id.trim();
					
				var i_revenue_share_status = request.getParameter('revenue_share_status');
				if(i_revenue_share_status)
					i_revenue_share_status = parseInt(i_revenue_share_status);
					
				var b_proj_val_chngd = request.getParameter('proj_val_chngd');
			}
			else
			{
				var i_projectId = request.getParameter('proj_selected');
				
				var s_request_type = request.getParameter('share_type');
				
				var b_revenue_rcrd_updated = request.getParameter('change_triggered');
				
				var b_proj_val_chngd = 'F';
				
				var a_revenue_cap_filter = [['custrecord_revenue_share_project', 'anyof', parseInt(i_projectId)]];
					
				var a_column = new Array();
				a_column[0] = new nlobjSearchColumn('created').setSort(true);
				a_column[1] = new nlobjSearchColumn('custrecord_auto_number_counter');
				a_column[2] = new nlobjSearchColumn('custrecord_revenue_share_approval_status');
				
				var a_get_logged_in_user_exsiting_revenue_cap = nlapiSearchRecord('customrecord_revenue_share', null, a_revenue_cap_filter, a_column);
				if (a_get_logged_in_user_exsiting_revenue_cap)
				{
					var i_revenue_share_id = a_get_logged_in_user_exsiting_revenue_cap[0].getId();
					var i_revenue_share_status = a_get_logged_in_user_exsiting_revenue_cap[0].getValue('custrecord_revenue_share_approval_status');
				}
				
				if(s_request_type == 'View' || s_request_type == 'Update' || s_request_type == 'Create' || s_request_type == 'Submit')
				{
					var a_proj_participating_practice = new Array();
					
					var a_url_parameters = new Array();
					a_url_parameters['proj_id'] = i_projectId;
					
					if(s_request_type == 'View')
					{
						a_url_parameters['mode'] = 'Submit';
						nlapiSubmitField('customrecord_revenue_share',i_revenue_share_id,'custrecord_revenue_share_approval_status',1);
					}
						
					if(s_request_type == 'Create' || s_request_type == 'Update')
					{
						a_url_parameters['mode'] = 'View';
						a_url_parameters['revenue_rcrd_updated'] = b_revenue_rcrd_updated;
						a_url_parameters['revenue_rcrd_status'] = i_revenue_share_status;
					}
						
						
					if(s_request_type == 'Submit')
					{
						a_url_parameters['mode'] = 'PendingApproval';
						nlapiSubmitField('customrecord_revenue_share',i_revenue_share_id,'custrecord_revenue_share_approval_status',2);
					}
						
					a_url_parameters['existing_rcrd_id'] = i_revenue_share_id;
					
					nlapiSetRedirectURL('SUITELET', '1144', 'customdeploy_sut_fp_revrec_effrtcst_view', null, a_url_parameters);
				}
				else if(s_request_type == 'MonthEnd')
				{
					/*var a_effort_activity_mnth_end_filter = [['custrecord_revenue_share_parent', 'anyof', parseInt(i_revenue_share_id)]];
			
					var a_columns_mnth_end_effort_activity_srch = new Array();
					a_columns_mnth_end_effort_activity_srch[0] = new nlobjSearchColumn('created').setSort(true);
					
					var a_get_mnth_end_effrt_activity = nlapiSearchRecord('customrecord_fp_revrec_month_end_process', null, a_effort_activity_mnth_end_filter, a_columns_mnth_end_effort_activity_srch);
					if (a_get_mnth_end_effrt_activity)
					{
						var i_month_end_activity_id = a_get_mnth_end_effrt_activity[0].getId();
					}*/
					
					var a_effort_activity_mnth_end_filter = [['custrecord_revenue_share_parent_json', 'anyof', parseInt(i_revenue_share_id)]];
			
					var a_columns_mnth_end_effort_activity_srch = new Array();
					a_columns_mnth_end_effort_activity_srch[0] = new nlobjSearchColumn('created').setSort(true);
					
					var a_get_mnth_end_effrt_activity = nlapiSearchRecord('customrecord_fp_rev_rec_mnth_end_json', null, a_effort_activity_mnth_end_filter, a_columns_mnth_end_effort_activity_srch);
					if (a_get_mnth_end_effrt_activity)
					{
						var i_month_end_activity_id = a_get_mnth_end_effrt_activity[0].getId();
					}
							
					var a_url_parameters = new Array();
					a_url_parameters['proj_id'] = i_projectId;
					a_url_parameters['mode'] = 'View';
					a_url_parameters['mnth_end_activity_id'] = i_month_end_activity_id;
					a_url_parameters['revenue_rcrd_status'] = i_revenue_share_status;
					a_url_parameters['revenue_share_id'] = i_revenue_share_id;
					
					nlapiSetRedirectURL('SUITELET', '1146', 'customdeploy_sut_fp_revrec_mnth_end_cost', null, a_url_parameters);
				}
			}
			
			var o_rev_loc_sub=Search_revenue_Location_subsdidary();	
			var location_monthend=o_rev_loc_sub["Cost Location"];
			
			if(i_projectId) // check if project id is present
			{
				if(i_existing_revenue_share_rcrd_id)
					var s_reject_reason = nlapiLookupField('customrecord_revenue_share',i_existing_revenue_share_rcrd_id,'custrecord_rejection_reason');
				
				i_projectId = i_projectId.trim();
				
				var o_project_object = nlapiLoadRecord('job',i_projectId); // load project record object
				
				// get necessary information about project from project record
				var s_project_region = o_project_object.getFieldValue('custentity_region');
				var i_customer_name = o_project_object.getFieldValue('parent');
				var s_project_name = o_project_object.getFieldValue('companyname');
				var d_proj_start_date = o_project_object.getFieldValue('startdate');
				var d_proj_strt_date_old_proj = o_project_object.getFieldValue('custentity_rev_rec_new_strt_date');
				var d_proj_end_date = o_project_object.getFieldValue('enddate');
				var i_proj_executing_practice = o_project_object.getFieldValue('custentity_practice');
				var i_proj_manager = o_project_object.getFieldValue('custentity_projectmanager');
				var i_proj_manager_practice = nlapiLookupField('employee',i_proj_manager,'department');
				var i_project_sow_value = o_project_object.getFieldValue('custentity_projectvalue');
				var i_project_value_ytd = o_project_object.getFieldValue('custentity_ytd_rev_recognized');
				var i_proj_revenue_rec_type = o_project_object.getFieldValue('custentity_fp_rev_rec_type');
				var s_proj_currency = o_project_object.getFieldText('custentity_project_currency');
				var b_proj_inactive = o_project_object.getFieldValue('isinactive');
				
				if(d_proj_strt_date_old_proj)
				{
					d_proj_start_date = d_proj_strt_date_old_proj;
				}
			
				// design form which will be displayed to user
				var o_form_obj = nlapiCreateForm("FP Project Details Page");
				
				// call client script to perform validation check
				o_form_obj.setScript('customscript_cli_fp_revrec_revenue_vali');
				
					if(b_proj_inactive=='T'){	
               var fld_proj_inactive = o_form_obj.addField('proj_inactive', 'inlinehtml', null,null);	
              var s_inactive_msg = '<html>';	
               s_inactive_msg += '<p><font size="3" color="red">Project is inactive. System will not allow you to do the setup.Please work with business Operation Team to do the setup</font></p></html>';	
                fld_proj_inactive.setDefaultValue(s_inactive_msg);	
                }
				
				// create field to identify is proj val chngd
				var fld_proj_val_chngd = o_form_obj.addField('b_proj_val_chngd', 'text','b_proj_val_chngd').setDisplayType('hidden');
				fld_proj_val_chngd.setDefaultValue(b_proj_val_chngd);
				
				// create field to get total lines for revenue share
				var fld_revenue_share_lines = o_form_obj.addField('revenue_share_lines', 'integer','revenue_share_lines').setDisplayType('hidden');
				fld_revenue_share_lines.setDefaultValue(0);
				
				// create field to get total lines for budget effort
				var fld_budegt_effrt_lines = o_form_obj.addField('budegt_effrt_lines', 'integer','budegt_effrt_lines').setDisplayType('hidden');
				fld_budegt_effrt_lines.setDefaultValue(0);
				
				// create field to get total lines for month end effort
				var fld_mnth_end_effrt_lines = o_form_obj.addField('mnth_end_effrt_lines', 'integer','mnth_end_effrt_lines').setDisplayType('hidden');
				fld_mnth_end_effrt_lines.setDefaultValue(0);
				
				// create region field
				var fld_region_field = o_form_obj.addField('proj_region', 'select','Region','customrecord_region').setDisplayType('inline');
				fld_region_field.setDefaultValue(s_project_region);
				
				// create customer field
				var fld_customer_field = o_form_obj.addField('customer_selected', 'select','Customer','customer').setDisplayType('inline');
				fld_customer_field.setDefaultValue(i_customer_name);
				
				// create project field
				var fld_proj_field = o_form_obj.addField('proj_selected', 'select','Project','job').setDisplayType('inline');
				fld_proj_field.setDefaultValue(i_projectId);
				
				if(!i_project_value_ytd)
					i_project_value_ytd = 0;
					
				i_project_sow_value = parseFloat(i_project_sow_value) - parseFloat(i_project_value_ytd);
				i_project_sow_value = parseFloat(i_project_sow_value).toFixed(2);
				
				// create project sow value field
				var fld_proj_sow_value = o_form_obj.addField('proj_sow_value', 'text','Project SOW value').setDisplayType('inline');
				fld_proj_sow_value.setDefaultValue(i_project_sow_value);
				
				// create msg field for revenue share and effort plan
				if(i_revenue_share_status != 3)
				{
					var s_revenue_msg = '<html>';
					s_revenue_msg += '<p><font size="3" color="red">Please review "Revenue share & Effort plan" tab before generating project detailed view</font></p></html>';
					var fld_revenue_msg = o_form_obj.addField('rvenue_msg', 'inlinehtml',null,null);
					fld_revenue_msg.setDefaultValue(s_revenue_msg);
				}
				else
				{
					var s_revenue_msg = '<html>';
					s_revenue_msg += '<p><font size="3" color="red">Please review month end effort.</font></p></html>';
					s_revenue_msg += '<p><font size="3" color="red">Resource Allocation data will be considered as current month effort</font></p></html>';
					var fld_revenue_msg = o_form_obj.addField('rvenue_msg', 'inlinehtml',null,null);
					fld_revenue_msg.setDefaultValue(s_revenue_msg);
				}
				
				// create field for project start date
				var fld_proj_start_date = o_form_obj.addField('proj_strt_date', 'date','Project Start Date').setDisplayType('inline');
				fld_proj_start_date.setDefaultValue(d_proj_start_date);
				
				// create field for projet end date
				var fld_proj_end_date = o_form_obj.addField('proj_end_date', 'date','Project End Date').setDisplayType('inline');
				fld_proj_end_date.setDefaultValue(d_proj_end_date);
				
				// create field for projet currency
				var fld_proj_end_date = o_form_obj.addField('proj_currency', 'text','Project Currency').setDisplayType('inline');
				fld_proj_end_date.setDefaultValue(s_proj_currency);
				
				// create field for project practice
				var fld_proj_practice = o_form_obj.addField('proj_practice', 'select','Project Executing Practice','department').setDisplayType('inline');
				fld_proj_practice.setDefaultValue(i_proj_executing_practice);
				
				// create field for poject manager practice field
				var fld_proj_manager_practice = o_form_obj.addField('proj_manager_practice', 'select','Project Manager Practice','department').setDisplayType('inline');
				fld_proj_manager_practice.setDefaultValue(i_proj_manager_practice);
				
				// create field for project revenue rec type
				var fld_proj_revRec_type = o_form_obj.addField('rev_rec_type', 'select','Revenue Recognition Type','customlist_fp_rev_rec_project_type').setDisplayType('inline');
				fld_proj_revRec_type.setDefaultValue(i_proj_revenue_rec_type);
				
				// create field for rejection reason
				var fld_proj_rejection_reason = o_form_obj.addField('rejection_reason', 'text','Rejection Reason').setDisplayType('inline');
				fld_proj_rejection_reason.setDefaultValue(s_reject_reason);
				
				// create field for request type i.e create or update
				var fld_existing_revenue_share_status = o_form_obj.addField('share_type', 'text','Request Type').setDisplayType('inline');
				fld_existing_revenue_share_status.setDefaultValue(s_request_mode);
				
				//field to find is value changed or not
				var fld_existing_revenue_share_changed = o_form_obj.addField('change_triggered', 'checkbox','').setDisplayType('inline');
				fld_existing_revenue_share_changed.setDefaultValue('F');
				
				//field to find is value changed or not
				var fld_existing_revenue_share_record_id = o_form_obj.addField('revenue_share_id', 'integer','Revenue Share ID').setDisplayType('hidden');
				fld_existing_revenue_share_record_id.setDefaultValue(i_existing_revenue_share_rcrd_id);
				
				// create field for amount that is budgeted
				var fld_budgeted_value = o_form_obj.addField('proj_budgeted_value', 'text','Revenue Share Budgeted').setDisplayType('inline');

				//create field for download resouce allocation
			/*	var fld_resource_alloc = o_form_obj.addField('custpage_alloc', 'select',' Resouce Allocation Download').setDisplayType('inline');

				form.addField('custpage_export_' + jobIndex, 'url', '', null, tabId)
		        .setDisplayType('inline').setLinkText('Export To Excel')
		        .setDefaultValue(csvExportUrl);

				var linkUrl	= nlapiResolveURL('SUITELET', '619', 'customdeploy_sut_pm_weekly_allocation');
				linkUrl = linkUrl+'&proj_id='+i_projectId+'&mode=View&existing_rcrd_id='+i_revenue_share_id+'&revenue_rcrd_status=2&revenue_rcrd_updated=F';
			 
			o_form_obj.addField('custpage_alloc', 'Resouce Allocation Download');
			var fld_link_to_setup_plan = o_form_obj.addField('link_to_setup_plan', 'url','',null,'custpage_project_setup_link').setDisplayType('inline').setLinkText('Project Budget Link');
			fld_link_to_setup_plan.setDefaultValue(linkUrl);*/
			var csvExportUrl = nlapiResolveURL('SUITELET',
		        'customscript_sut_fp_rev_rec_proj_details',
		        'customdeploy_sut_fp_rev_rec_proj_details')
		        + "&s_request_mode=excel";
			//	&project=";
		      //  + (i_projectId ? i_projectId : '');
		      /*  + "&customer="
		        + (customer ? customer : '')
		        + "&startdate="
		        + (startDate ? startDate : '')
		        + "&enddate="
		        + (endDate ? endDate : '')
		        + "&status="
		        + (status ? status : '2');*/
				o_form_obj.addField('custpage_alloc', 'url', '', null)
		        .setDisplayType('inline').setLinkText('Resouce Allocation Download')
		        .setDefaultValue(csvExportUrl);

		/*	var link	=	o_form_obj.addField('custpage_alloc', 'url', "")
			var s_url	=	'https://' + request.getHeader('Host') + nlapiResolveURL('SUITELET', 1139, 1)
				
			link.setDisplayType('inline').setLinkText('Resouce Allocation Download').setDefaultValue();*/
			
				
				if (i_revenue_share_status == 3)
				{
					// Add subtab for month end activity effort plan
					var tab_effort_plan = o_form_obj.addSubTab('effort_plan_tab_month_end', 'Update Monthly Effort');
				}			
							
				// Add subtab for revenue share
				var tab_revenue_share = o_form_obj.addSubTab('revenue_share_tab', 'Plan Revenue Share');
				
				// Add subtab for effort plan
				var tab_effort_plan = o_form_obj.addSubTab('effort_plan_tab', 'Plan Effort Budget');
				
				if (s_request_mode != 'View' && s_request_mode != 'Submit' && s_request_mode != 'PendingApproval')
				{
					if(i_revenue_share_status == 2 || i_revenue_share_status == 3)
					{
						if(i_revenue_share_status == 3)
						{
							var d_today_date = new Date();
							var i_today_date = d_today_date.getDate();
							var i_today_month = d_today_date.getMonth();
							
							var o_freeze_date = nlapiLoadRecord('customrecord_fp_rev_rec_freeze_date',1);
							var s_freeze_date = o_freeze_date.getFieldValue('custrecord_freeze_date');
							var i_freeze_day = nlapiStringToDate(s_freeze_date).getDate();
							var i_freeze_month = nlapiStringToDate(s_freeze_date).getMonth();
							
							if(parseInt(i_freeze_month) != parseInt(i_today_month))
								i_freeze_day = 25;
							
							var o_Fp_Rev_Rec_admin_access = nlapiLoadRecord('customrecord_fp_rev_rec_admin_access',2);
							var a_Fp_Rev_Rec_admin_access_emp_list= o_Fp_Rev_Rec_admin_access.getFieldValues('custrecord36');
							var data_list = [];
							if(a_Fp_Rev_Rec_admin_access_emp_list){
							for(var list_index=0;list_index < a_Fp_Rev_Rec_admin_access_emp_list.length;list_index++ )
							{
							data_list.push(parseInt(a_Fp_Rev_Rec_admin_access_emp_list[list_index]));
							}
							}
							nlapiLogExecution('DEBUG','user',a_Fp_Rev_Rec_admin_access_emp_list);
							if(i_today_date >= i_freeze_day && data_list.indexOf(parseInt(i_user_logegdIn_id))<0) 
							{
								var s_revenue_msg = '<html>';
								s_revenue_msg += '<p><font size="3" color="red">Month End Effort is locked for this month.</font></p></html>';
								fld_revenue_msg.setDefaultValue(s_revenue_msg);
							}
							else
							{
								//o_form_obj.setScript('customscript_cli_fp_revrec_disable_remov');
								var projectWiseRevenue = {};
								
								var monthBreakUp = getMonthsBreakup(
						        nlapiStringToDate(d_proj_start_date),
						        nlapiStringToDate(d_proj_end_date));
								
								fld_existing_revenue_share_status.setDefaultValue('MonthEnd');
								
								o_form_obj.addSubmitButton('Generate Project Detail');
								
								var url_proj_detail_url = 'https://system.na1.netsuite.com/app/site/hosting/scriptlet.nl?script=1055&deploy=1&mode=View&revenue_rcrd_updated=F&existing_rcrd_id='+i_existing_revenue_share_rcrd_id+'&proj_id='+i_projectId+'&revenue_rcrd_status=3&whence=';
								//var fld_proj_detail = o_form_obj.addField('proj_detail_url', 'url','').setLinkText('Project Detail');
								//fld_proj_detail.setDefaultValue(url_proj_detail_url);
								
								// Add sublist on form to update effort plan
								var fld_sublist_effort_plan_mnth_end = o_form_obj.addSubList('effort_sublist_month_end', 'inlineeditor', 'Effort Plan Month End', 'effort_plan_tab_month_end');
								var fld_is_allocation_row = fld_sublist_effort_plan_mnth_end.addField('is_resource_allo_row', 'text','Is Allocation Row').setDisplayType('inline').setDefaultValue('<html><body bgcolor="red"></body></html>');
								var fld_practice = fld_sublist_effort_plan_mnth_end.addField('parent_practice_month_end', 'select', 'Practice', 'department').setDisplayType('disabled');
								var fld_sub_practice = fld_sublist_effort_plan_mnth_end.addField('sub_practice_month_end', 'select', 'Sub Practice');
								var fld_emp_level = fld_sublist_effort_plan_mnth_end.addField('level_month_end', 'select', 'Level'); //customlist_fp_rev_rec_level
								fld_sublist_effort_plan_mnth_end.addField('role_month_end', 'select', 'Role','customlist_fp_rev_rec_roles');
								var fld_location_mnth_end = fld_sublist_effort_plan_mnth_end.addField('location_month_end', 'select', 'Location');
								/*
								fld_location_mnth_end.addSelectOption('', '');
								fld_location_mnth_end.addSelectOption('8', 'India');
								fld_location_mnth_end.addSelectOption('2', 'UK/Europe/Nordics');
								fld_location_mnth_end.addSelectOption('9', 'US');
								fld_location_mnth_end.addSelectOption('57', 'Romania');
								*/
								
								fld_location_mnth_end.addSelectOption('', '');	
								for(var key in location_monthend){ 	
								fld_location_mnth_end.addSelectOption(key,location_monthend[key]);                          	
								}
								
								fld_emp_level.addSelectOption('', '');
								fld_emp_level.addSelectOption('31', '0');
								fld_emp_level.addSelectOption('1', '1');
								//fld_emp_level.addSelectOption('15', '1A');
								//fld_emp_level.addSelectOption('16', '1B');
								fld_emp_level.addSelectOption('2', '2');
								//fld_emp_level.addSelectOption('17', '2A');
								//fld_emp_level.addSelectOption('18', '2B');
								fld_emp_level.addSelectOption('3', '3');
								//fld_emp_level.addSelectOption('10', '3A');
								//fld_emp_level.addSelectOption('19', '3B');
								fld_emp_level.addSelectOption('4', '4');
								//fld_emp_level.addSelectOption('20', '4A');
								//fld_emp_level.addSelectOption('21', '4B');
								fld_emp_level.addSelectOption('5', '5');
								//fld_emp_level.addSelectOption('22', '5A');
								//fld_emp_level.addSelectOption('23', '5B');
								fld_emp_level.addSelectOption('6', '6');
								//fld_emp_level.addSelectOption('11', '6A');
								//fld_emp_level.addSelectOption('14', '6B');
								fld_emp_level.addSelectOption('7', '7');
								//fld_emp_level.addSelectOption('24', '7A');
								//fld_emp_level.addSelectOption('25', '7B');
								fld_emp_level.addSelectOption('8', '8');
								//fld_emp_level.addSelectOption('26', '8A');
								//fld_emp_level.addSelectOption('27', '8B');
								fld_emp_level.addSelectOption('9', '9');
								//fld_emp_level.addSelectOption('28', '9A');
								//fld_emp_level.addSelectOption('29', '9B');
								fld_emp_level.addSelectOption('13', 'CW');
								fld_emp_level.addSelectOption('30', 'PT');
								fld_emp_level.addSelectOption('12', 'TS');
								fld_emp_level.addSelectOption('33', 'L');
								fld_sub_practice.addSelectOption('', '');
								
								var a_filter_get_corporate_prac = [['custrecord_practice_type_ticker', 'anyof', 2], 'and',
													['isinactive', 'is', 'F']];
								
								var a_columns_get_corporate_prac = new Array();
								a_columns_get_corporate_prac[0] = new nlobjSearchColumn('name');
		
								var a_get_corporate_prac = nlapiSearchRecord('department', null, a_filter_get_corporate_prac, a_columns_get_corporate_prac);
								if (a_get_corporate_prac)
								{
									for(var i_prac_index=0; i_prac_index<a_get_corporate_prac.length; i_prac_index++)
									{
										fld_sub_practice.addSelectOption(a_get_corporate_prac[i_prac_index].getId(), a_get_corporate_prac[i_prac_index].getValue('name'));
									}
								}
								
								fld_sublist_effort_plan_mnth_end.addField('resource_cost_month_end', 'integer', 'Resource Cost(USD)').setDisplayType('disabled');
								fld_sublist_effort_plan_mnth_end.addField('revenue_share_mnth_end', 'integer', 'Revenue Share('+s_proj_currency+')').setDisplayType('disabled');
								
								for (var j = 0; j < monthBreakUp.length; j++)
								{
									var months = monthBreakUp[j];
									var s_month_name = getMonthName(months.Start); // get month name
									var month_strt = nlapiStringToDate(months.Start);
									var month_end = nlapiStringToDate(months.End);
									var i_month = month_strt.getMonth();
									var i_year = month_strt.getFullYear();
									s_month_name = s_month_name +'_'+ i_year;
									var d_today_date = new Date();
									
									if(month_strt <= d_today_date)
									{
										fld_sublist_effort_plan_mnth_end.addField(''+s_month_name.toLowerCase(), 'float', ''+s_month_name).setDisplayType('disabled');
									}
									else
									{
										fld_sublist_effort_plan_mnth_end.addField(''+s_month_name.toLowerCase(), 'float', ''+s_month_name);
									}
								}
							}
						}
						
						// Add sublist on form to update practice and revenue share
						var fld_form_sublist_revenue_share = o_form_obj.addSubList('practice_sublist', 'list', 'Participating Practice', 'revenue_share_tab');
						fld_form_sublist_revenue_share.addField('parent_practice', 'select', 'Practice', 'department').setDisplayType('inline');
						var fld_sub_practice = fld_form_sublist_revenue_share.addField('sub_practice', 'select', 'Sub Practice').setDisplayType('inline');
						fld_form_sublist_revenue_share.addField('share_amount', 'currency', 'Amount('+s_proj_currency+')');
					
						var a_filter_get_corporate_prac = [['custrecord_practice_type_ticker', 'anyof', 2], 'and',
													['isinactive', 'is', 'F']];
								
						var a_columns_get_corporate_prac = new Array();
						a_columns_get_corporate_prac[0] = new nlobjSearchColumn('name');

						var a_get_corporate_prac = nlapiSearchRecord('department', null, a_filter_get_corporate_prac, a_columns_get_corporate_prac);
						if (a_get_corporate_prac)
						{
							for(var i_prac_index=0; i_prac_index<a_get_corporate_prac.length; i_prac_index++)
							{
								fld_sub_practice.addSelectOption(a_get_corporate_prac[i_prac_index].getId(), a_get_corporate_prac[i_prac_index].getValue('name'));
							}
						}
								
						// Add sublist on form to update practice and revenue share
						var fld_form_sublist_effort_plan = o_form_obj.addSubList('effort_sublist', 'list', 'Effort Plan', 'effort_plan_tab');
						fld_form_sublist_effort_plan.addField('parent_practice_effort_plan', 'select', 'Practice', 'department').setDisplayType('inline');
						var fld_sub_practice = fld_form_sublist_effort_plan.addField('sub_practice_effort_plan', 'select', 'Sub Practice').setDisplayType('inline');
						var level_fld_effrt_pln = fld_form_sublist_effort_plan.addField('level_effort_plan', 'select', 'Level').setDisplayType('inline');
						var f_role_field = fld_form_sublist_effort_plan.addField('role_effort_plan', 'select', 'Role', 'customlist_fp_rev_rec_roles').setDisplayType('inline');
						fld_form_sublist_effort_plan.addField('no_resources_effort_plan', 'text', '# of resources');
						fld_form_sublist_effort_plan.addField('allocation_strt_date_effort_plan', 'date', 'Allocation Start Date(mm/dd/yyyy)');
						fld_form_sublist_effort_plan.addField('allocation_end_date_effort_plan', 'date', 'Allocation End Date(mm/dd/yyyy)');
						fld_form_sublist_effort_plan.addField('prcnt_allocation_effort_plan', 'integer', '% Allocation');
						fld_form_sublist_effort_plan.addField('location_effort_plan', 'select', 'Location', 'location').setDisplayType('inline');
						fld_form_sublist_effort_plan.addField('cost_effort_plan', 'currency', 'Resource Cost(USD)').setDisplayType('inline');
						
						var a_filter_get_corporate_prac = [['custrecord_practice_type_ticker', 'anyof', 2], 'and',
													['isinactive', 'is', 'F']];
								
						var a_columns_get_corporate_prac = new Array();
						a_columns_get_corporate_prac[0] = new nlobjSearchColumn('name');

						var a_get_corporate_prac = nlapiSearchRecord('department', null, a_filter_get_corporate_prac, a_columns_get_corporate_prac);
						if (a_get_corporate_prac)
						{
							for(var i_prac_index=0; i_prac_index<a_get_corporate_prac.length; i_prac_index++)
							{
								fld_sub_practice.addSelectOption(a_get_corporate_prac[i_prac_index].getId(), a_get_corporate_prac[i_prac_index].getValue('name'));
							}
						}
						
						level_fld_effrt_pln.addSelectOption('', '');
						level_fld_effrt_pln.addSelectOption('31', '0');
						level_fld_effrt_pln.addSelectOption('1', '1');
						//level_fld_effrt_pln.addSelectOption('15', '1A');
						//level_fld_effrt_pln.addSelectOption('16', '1B');
						level_fld_effrt_pln.addSelectOption('2', '2');
						//level_fld_effrt_pln.addSelectOption('17', '2A');
						//level_fld_effrt_pln.addSelectOption('18', '2B');
						level_fld_effrt_pln.addSelectOption('3', '3');
						//level_fld_effrt_pln.addSelectOption('10', '3A');
						//level_fld_effrt_pln.addSelectOption('19', '3B');
						level_fld_effrt_pln.addSelectOption('4', '4');
						//level_fld_effrt_pln.addSelectOption('20', '4A');
						//level_fld_effrt_pln.addSelectOption('21', '4B');
						level_fld_effrt_pln.addSelectOption('5', '5');
						//level_fld_effrt_pln.addSelectOption('22', '5A');
						//level_fld_effrt_pln.addSelectOption('23', '5B');
						level_fld_effrt_pln.addSelectOption('6', '6');
						//level_fld_effrt_pln.addSelectOption('11', '6A');
						//level_fld_effrt_pln.addSelectOption('14', '6B');
						level_fld_effrt_pln.addSelectOption('7', '7');
						//level_fld_effrt_pln.addSelectOption('24', '7A');
						//level_fld_effrt_pln.addSelectOption('25', '7B');
						level_fld_effrt_pln.addSelectOption('8', '8');
						//level_fld_effrt_pln.addSelectOption('26', '8A');
						//level_fld_effrt_pln.addSelectOption('27', '8B');
						level_fld_effrt_pln.addSelectOption('9', '9');
						//level_fld_effrt_pln.addSelectOption('28', '9A');
						//level_fld_effrt_pln.addSelectOption('29', '9B');
						level_fld_effrt_pln.addSelectOption('13', 'CW');
						level_fld_effrt_pln.addSelectOption('30', 'PT');
						level_fld_effrt_pln.addSelectOption('12', 'TS');
						level_fld_effrt_pln.addSelectOption('33', 'L');
						if(i_revenue_share_status == 2)
						{
							// add button to view project plan
							o_form_obj.addSubmitButton('Project Detail');
						}
					}
					else
					{
						// Add sublist on form to update practice and revenue share
						var fld_form_sublist_revenue_share = o_form_obj.addSubList('practice_sublist', 'inlineeditor', 'Participating Practice', 'revenue_share_tab');
						var fld_parent_prac_prac_sublist = fld_form_sublist_revenue_share.addField('parent_practice', 'select', 'Practice').setDisplayType('disabled');
						var fld_sub_prac_prac_sublist = fld_form_sublist_revenue_share.addField('sub_practice', 'select', 'Sub Practice');
						fld_form_sublist_revenue_share.addField('share_amount', 'currency', 'Amount('+s_proj_currency+')');
						
						// Add sublist on form to update practice and revenue share
						var fld_form_sublist_effort_plan = o_form_obj.addSubList('effort_sublist', 'inlineeditor', 'Effort Plan', 'effort_plan_tab');
						var fld_parent_prac_effort_sublist = fld_form_sublist_effort_plan.addField('parent_practice_effort_plan', 'select', 'Practice').setDisplayType('disabled');
						var fld_sub_prac_effort_sublist = fld_form_sublist_effort_plan.addField('sub_practice_effort_plan', 'select', 'Sub Practice');
						var level_fld_effrt_pln = fld_form_sublist_effort_plan.addField('level_effort_plan', 'select', 'Level');
						var f_role_field = fld_form_sublist_effort_plan.addField('role_effort_plan', 'select', 'Role','customlist_fp_rev_rec_roles');
						fld_form_sublist_effort_plan.addField('no_resources_effort_plan', 'integer', '# of resources');
						fld_form_sublist_effort_plan.addField('allocation_strt_date_effort_plan', 'date', 'Allocation Start Date(mm/dd/yyyy)');
						fld_form_sublist_effort_plan.addField('allocation_end_date_effort_plan', 'date', 'Allocation End Date(mm/dd/yyyy)');
						fld_form_sublist_effort_plan.addField('prcnt_allocation_effort_plan', 'integer', '% Allocation');
						var fld_location = fld_form_sublist_effort_plan.addField('location_effort_plan', 'select', 'Location');
						fld_form_sublist_effort_plan.addField('cost_effort_plan', 'currency', 'Resource Cost(USD)').setDisplayType('disabled');
						
						level_fld_effrt_pln.addSelectOption('', '');
						level_fld_effrt_pln.addSelectOption('31', '0');
						level_fld_effrt_pln.addSelectOption('1', '1');
						//level_fld_effrt_pln.addSelectOption('15', '1A');
						//level_fld_effrt_pln.addSelectOption('16', '1B');
						level_fld_effrt_pln.addSelectOption('2', '2');
						//level_fld_effrt_pln.addSelectOption('17', '2A');
						//level_fld_effrt_pln.addSelectOption('18', '2B');
						level_fld_effrt_pln.addSelectOption('3', '3');
						//level_fld_effrt_pln.addSelectOption('10', '3A');
						//level_fld_effrt_pln.addSelectOption('19', '3B');
						level_fld_effrt_pln.addSelectOption('4', '4');
						//level_fld_effrt_pln.addSelectOption('20', '4A');
						//level_fld_effrt_pln.addSelectOption('21', '4B');
						level_fld_effrt_pln.addSelectOption('5', '5');
						//level_fld_effrt_pln.addSelectOption('22', '5A');
						//level_fld_effrt_pln.addSelectOption('23', '5B');
						level_fld_effrt_pln.addSelectOption('6', '6');
						//level_fld_effrt_pln.addSelectOption('11', '6A');
						//level_fld_effrt_pln.addSelectOption('14', '6B');
						level_fld_effrt_pln.addSelectOption('7', '7');
						//level_fld_effrt_pln.addSelectOption('24', '7A');
						//level_fld_effrt_pln.addSelectOption('25', '7B');
						level_fld_effrt_pln.addSelectOption('8', '8');
						//level_fld_effrt_pln.addSelectOption('26', '8A');
						//level_fld_effrt_pln.addSelectOption('27', '8B');
						level_fld_effrt_pln.addSelectOption('9', '9');
						//level_fld_effrt_pln.addSelectOption('28', '9A');
						//level_fld_effrt_pln.addSelectOption('29', '9B');
						level_fld_effrt_pln.addSelectOption('13', 'CW');
						level_fld_effrt_pln.addSelectOption('30', 'PT');
						level_fld_effrt_pln.addSelectOption('12', 'TS');
						level_fld_effrt_pln.addSelectOption('33', 'L');
						/*
						fld_location.addSelectOption('', '');
						fld_location.addSelectOption('8', 'India');
						fld_location.addSelectOption('2', 'UK/Europe/Nordics');
						fld_location.addSelectOption('9', 'US');
					    fld_location.addSelectOption('57', 'Romania');
						*/
						
						fld_location.addSelectOption('', '');	
						for(var key in location_monthend){ 	
						fld_location.addSelectOption(key,location_monthend[key]);                          	
						}
						
						fld_parent_prac_prac_sublist.addSelectOption('', '');
						fld_parent_prac_effort_sublist.addSelectOption('', '');
						
						fld_sub_prac_prac_sublist.addSelectOption('', '');
						fld_sub_prac_effort_sublist.addSelectOption('', '');
								
						var a_practice_gdm = [['custrecord_practice_type_ticker', 'anyof', 2], 'and',
											['isinactive', 'is', 'F']];
											
						var a_columns_practice_gdm = new Array();
						a_columns_practice_gdm[0] = new nlobjSearchColumn('name');
							
						var a_practice_gdm_search_results = nlapiSearchRecord('department', null, a_practice_gdm, a_columns_practice_gdm);
						if(a_practice_gdm_search_results)
						{
							for(var i_gdm_prac_index = 0; i_gdm_prac_index<a_practice_gdm_search_results.length ; i_gdm_prac_index++)
							{
								fld_parent_prac_prac_sublist.addSelectOption(a_practice_gdm_search_results[i_gdm_prac_index].getId(), a_practice_gdm_search_results[i_gdm_prac_index].getValue('name'));
								fld_parent_prac_effort_sublist.addSelectOption(a_practice_gdm_search_results[i_gdm_prac_index].getId(), a_practice_gdm_search_results[i_gdm_prac_index].getValue('name'));
							
								fld_sub_prac_prac_sublist.addSelectOption(a_practice_gdm_search_results[i_gdm_prac_index].getId(), a_practice_gdm_search_results[i_gdm_prac_index].getValue('name'));
								fld_sub_prac_effort_sublist.addSelectOption(a_practice_gdm_search_results[i_gdm_prac_index].getId(), a_practice_gdm_search_results[i_gdm_prac_index].getValue('name'));
							}
						}
					}
						
					if(i_revenue_share_status != 2 && i_revenue_share_status != 3)
					{
						// add submit button to proceed further
						o_form_obj.addSubmitButton('Generate Project View');
					}
				}
				
				// If PM/DM wants to update revenue share and effort plan
				if(s_request_mode == 'Update')
				{
					var a_JSON = [];
					var a_proj_participating_practice = new Array();
					var a_JSON_effort_plan = [];
					var a_JSON_mnth_end = [];
					//var a_per_mnth_allocation = [];
					var a_proj_effort_plan = new Array();
					var a_proj_effort_plan_mnth_end = new Array();
					var a_proj_effrt_plan_mnth_prcnt = new Array();
					
					var a_revenue_cap_filter = [['custrecord_revenue_share_per_practice_ca.custrecord_revenue_share_project', 'anyof', parseInt(i_projectId)], 'and',
												['custrecord_revenue_share_per_practice_ca.internalid', 'anyof', parseInt(i_existing_revenue_share_rcrd_id)]];
			
					var a_columns_existing_cap_srch = new Array();
					a_columns_existing_cap_srch[0] = new nlobjSearchColumn('custrecord_revenue_share_per_practice_pr');
					a_columns_existing_cap_srch[1] = new nlobjSearchColumn('custrecord_revenue_share_per_practice_su');
					a_columns_existing_cap_srch[2] = new nlobjSearchColumn('custrecord_revenue_share_per_practice_re');
				
					var a_get_logged_in_user_exsiting_revenue_cap = nlapiSearchRecord('customrecord_revenue_share_per_practice', null, a_revenue_cap_filter, a_columns_existing_cap_srch);
					if (a_get_logged_in_user_exsiting_revenue_cap)
					{
						for(var i_revenue_index = 0; i_revenue_index < a_get_logged_in_user_exsiting_revenue_cap.length; i_revenue_index++)
						{
							a_JSON = {
									sub_practice: a_get_logged_in_user_exsiting_revenue_cap[i_revenue_index].getValue('custrecord_revenue_share_per_practice_su'),
									parent_practice: a_get_logged_in_user_exsiting_revenue_cap[i_revenue_index].getValue('custrecord_revenue_share_per_practice_pr'),
									share_amount: a_get_logged_in_user_exsiting_revenue_cap[i_revenue_index].getValue('custrecord_revenue_share_per_practice_re')
								}
							a_proj_participating_practice.push(a_JSON);
						}
					}
					
					var a_effort_plan_filter = [['custrecord_effort_plan_revenue_cap.internalid', 'anyof', parseInt(i_existing_revenue_share_rcrd_id)]];
			
					var a_columns_existing_effort_plan_srch = new Array();
					a_columns_existing_effort_plan_srch[0] = new nlobjSearchColumn('custrecord_effort_plan_practice');
					a_columns_existing_effort_plan_srch[1] = new nlobjSearchColumn('custrecord_effort_plan_sub_practice');
					a_columns_existing_effort_plan_srch[2] = new nlobjSearchColumn('custrecord_effort_plan_role');
					a_columns_existing_effort_plan_srch[3] = new nlobjSearchColumn('custrecord_effort_plan_level');
					a_columns_existing_effort_plan_srch[4] = new nlobjSearchColumn('custrecord_efort_plan_no_of_resources');
					a_columns_existing_effort_plan_srch[5] = new nlobjSearchColumn('custrecord_effort_plan_allo_strt_date');
					a_columns_existing_effort_plan_srch[6] = new nlobjSearchColumn('custrecord_effort_plan_allo_end_date');
					a_columns_existing_effort_plan_srch[7] = new nlobjSearchColumn('custrecord_effort_plan_percent_allocated');
					a_columns_existing_effort_plan_srch[8] = new nlobjSearchColumn('custrecord_effort_plan_location');
					a_columns_existing_effort_plan_srch[9] = new nlobjSearchColumn('custrecord_cost_for_resource');
				
					var a_get_exsiting_effort_plan = nlapiSearchRecord('customrecord_fp_rev_rec_effort_plan', null, a_effort_plan_filter, a_columns_existing_effort_plan_srch);
					if (a_get_exsiting_effort_plan)
					{
						for(var i_effort_index = 0; i_effort_index < a_get_exsiting_effort_plan.length; i_effort_index++)
						{
							a_JSON_effort_plan = {
									sub_practice_effort_plan: a_get_exsiting_effort_plan[i_effort_index].getValue('custrecord_effort_plan_sub_practice'),
									parent_practice_effort_plan: a_get_exsiting_effort_plan[i_effort_index].getValue('custrecord_effort_plan_practice'),
									role_effort_plan: a_get_exsiting_effort_plan[i_effort_index].getValue('custrecord_effort_plan_role'),
									level_effort_plan: a_get_exsiting_effort_plan[i_effort_index].getValue('custrecord_effort_plan_level'),
									no_resources_effort_plan: a_get_exsiting_effort_plan[i_effort_index].getValue('custrecord_efort_plan_no_of_resources'),
									allocation_strt_date_effort_plan: a_get_exsiting_effort_plan[i_effort_index].getValue('custrecord_effort_plan_allo_strt_date'),
									allocation_end_date_effort_plan: a_get_exsiting_effort_plan[i_effort_index].getValue('custrecord_effort_plan_allo_end_date'),
									prcnt_allocation_effort_plan: a_get_exsiting_effort_plan[i_effort_index].getValue('custrecord_effort_plan_percent_allocated'),
									location_effort_plan: a_get_exsiting_effort_plan[i_effort_index].getValue('custrecord_effort_plan_location'),
									cost_effort_plan: a_get_exsiting_effort_plan[i_effort_index].getValue('custrecord_cost_for_resource')
								}
								
							a_proj_effort_plan.push(a_JSON_effort_plan);
							
						}
					}
					
					fld_form_sublist_revenue_share.setLineItemValues(a_proj_participating_practice);
					fld_form_sublist_effort_plan.setLineItemValues(a_proj_effort_plan);
					
					fld_revenue_share_lines.setDefaultValue(a_proj_participating_practice.length);
					fld_budegt_effrt_lines.setDefaultValue(a_proj_effort_plan.length);
					
					if(fld_sublist_effort_plan_mnth_end)
					{
						var a_effort_activity_mnth_end_filter = [['custrecord_revenue_share_parent', 'anyof', parseInt(i_existing_revenue_share_rcrd_id)]];
			
						var a_columns_mnth_end_effort_activity_srch = new Array();
						a_columns_mnth_end_effort_activity_srch[0] = new nlobjSearchColumn('created').setSort(true);
						
						var a_get_mnth_end_effrt_activity = nlapiSearchRecord('customrecord_fp_revrec_month_end_process', null, a_effort_activity_mnth_end_filter, a_columns_mnth_end_effort_activity_srch);
						if (a_get_mnth_end_effrt_activity)
						{
							
							var obj_resource_location=o_rev_loc_sub["Location"];
							
							// Get resource allocation for current month
							var a_unique_sub_prac = new Array();
							var a_proj_prac_details = new Array();
							var a_current_pract = new Array();
							var a_current_pract_unique_list_sub_prac = new Array();
							var a_current_pract_unique_list_level = new Array();
							var a_current_pract_unique_list_role = new Array();
								
							var d_todays_date = nlapiStringToDate(nlapiDateToString(new Date()));
							var d_firstDay_mnth = new Date(d_todays_date.getFullYear(), d_todays_date.getMonth(), 1);
							var d_lastDay_mnth = new Date(d_todays_date.getFullYear(), d_todays_date.getMonth()+1, 0);
							var i_days_in_mnth = getDatediffIndays(d_firstDay_mnth,d_lastDay_mnth);
							
							var s_year_current = d_todays_date.getFullYear();
							var i_month_current = d_todays_date.getMonth();
							var s_month_current = getMonthName_FromMonthNumber(i_month_current);
									
							var a_filters_allocation = [ [ 'startdate', 'onorbefore', d_lastDay_mnth ], 'and',
								[ 'enddate', 'onorafter', d_firstDay_mnth ], 'and',
								['project', 'anyof', i_projectId]
							  ];
							 
							var a_columns = new Array();
							a_columns[0] = new nlobjSearchColumn('custevent_practice',null,'group').setSort(false);
							a_columns[1] = new nlobjSearchColumn('custrecord_parent_practice','custevent_practice','group');
							a_columns[2] = new nlobjSearchColumn('percentoftime',null,'sum');
							a_columns[3] = new nlobjSearchColumn('employeestatus','employee','group');
							a_columns[4] = new nlobjSearchColumn('startdate',null,'group');
							a_columns[5] = new nlobjSearchColumn('enddate',null,'group');
							a_columns[6] = new nlobjSearchColumn('subsidiary','employee','group');
							a_columns[7] = new nlobjSearchColumn('location','employee','group');
							
							var a_project_allocation_result = nlapiSearchRecord('resourceallocation', null, a_filters_allocation, a_columns);
							if (a_project_allocation_result)
							{
								for(var i_prac_count=0; i_prac_count<a_project_allocation_result.length; i_prac_count++)
								{
									var s_emp_level = a_project_allocation_result[i_prac_count].getText('employeestatus','employee','group');
									s_emp_level = s_emp_level.toString();
									var s_emp_level_sub_str = s_emp_level.substr(0,1);
									if(!isNaN(s_emp_level_sub_str))
									{
										s_emp_level = s_emp_level_sub_str;
									}
									else
									{
										s_emp_level = a_project_allocation_result[i_prac_count].getValue('employeestatus','employee','group');
									}
									
									var d_allocation_strt_date = a_project_allocation_result[i_prac_count].getValue('startdate',null,'group');
									var d_allocation_end_date = a_project_allocation_result[i_prac_count].getValue('enddate',null,'group');
									d_allocation_strt_date = nlapiStringToDate(d_allocation_strt_date);
									d_allocation_end_date = nlapiStringToDate(d_allocation_end_date);
									
									var s_prcnt_allocated = a_project_allocation_result[i_prac_count].getValue('percentoftime',null,'sum');
									s_prcnt_allocated = s_prcnt_allocated.toString();
									s_prcnt_allocated = s_prcnt_allocated.split('.');
									s_prcnt_allocated = s_prcnt_allocated[0].toString().split('%');
									
									if(d_allocation_strt_date < d_firstDay_mnth)
									{
										d_allocation_strt_date = d_firstDay_mnth;
									}
									
									if(d_allocation_end_date > d_lastDay_mnth)
									{
										d_allocation_end_date = d_lastDay_mnth;
									}
									
									var i_days_diff = getDatediffIndays(d_allocation_strt_date,d_allocation_end_date);
									//nlapiLogExecution('audit','days allocated:- '+i_days_diff,'days in month:- '+i_days_in_mnth);
									if(i_days_diff >= i_days_in_mnth)
									{
										var f_final_prcnt_allocation = parseFloat(s_prcnt_allocated) / parseFloat(100);
									}
									else
									{
										var i_total_allocated_days = parseFloat(i_days_diff) / parseFloat(i_days_in_mnth);
										s_prcnt_allocated = parseFloat(s_prcnt_allocated) * parseFloat(i_total_allocated_days);
										var f_final_prcnt_allocation = parseFloat(s_prcnt_allocated) / parseFloat(100);
									}
									f_final_prcnt_allocation = parseFloat(f_final_prcnt_allocation).toFixed(2);
									
									a_unique_sub_prac.push(a_project_allocation_result[i_prac_count].getValue('custevent_practice',null,'group'));
									
									var i_resource_location = a_project_allocation_result[i_prac_count].getValue('location','employee','group');
									
									i_resource_location=obj_resource_location[i_resource_location]?parseInt(obj_resource_location[i_resource_location]):parseInt(i_resource_location);
									
									//nlapiLogExecution('audit','resource location:- ',i_resource_location);
									var i_is_practice_alreday_present = findAllocationPracticePresent(a_proj_prac_details, a_project_allocation_result[i_prac_count].getValue('custevent_practice',null,'group'), s_emp_level, a_project_allocation_result[i_prac_count].getValue('subsidiary','employee','group'), i_resource_location);
									//nlapiLogExecution('audit','i_is_practice_alreday_present:- ',i_is_practice_alreday_present);
									
									if(i_is_practice_alreday_present >= 0)
									{
										var f_already_alloc_prcnt = a_proj_prac_details[i_is_practice_alreday_present].f_prcnt_allocated;
										f_already_alloc_prcnt = parseFloat(f_already_alloc_prcnt) + parseFloat(f_final_prcnt_allocation);
										f_already_alloc_prcnt = parseFloat(f_already_alloc_prcnt).toFixed(2);
										
										//nlapiLogExecution('audit','already present prcnt:- '+a_proj_prac_details[i_is_practice_alreday_present].f_prcnt_allocated,'f_already_alloc_prcnt:- '+f_already_alloc_prcnt);
										a_proj_prac_details[i_is_practice_alreday_present] = {
																'i_sub_practice': a_project_allocation_result[i_prac_count].getValue('custevent_practice',null,'group'),
																'i_practice': a_project_allocation_result[i_prac_count].getValue('custrecord_parent_practice','custevent_practice','group'),
																'f_prcnt_allocated': f_already_alloc_prcnt,
																's_emp_level': s_emp_level,
																'i_resource_location': i_resource_location,
																'subsidiary': a_project_allocation_result[i_prac_count].getValue('subsidiary','employee','group')
															};
									}
									else
									{
										a_proj_prac_details.push(
															{
																'i_sub_practice': a_project_allocation_result[i_prac_count].getValue('custevent_practice',null,'group'),
																'i_practice': a_project_allocation_result[i_prac_count].getValue('custrecord_parent_practice','custevent_practice','group'),
																'f_prcnt_allocated': f_final_prcnt_allocation,
																's_emp_level': s_emp_level,
																'i_resource_location': i_resource_location,
																'subsidiary': a_project_allocation_result[i_prac_count].getValue('subsidiary','employee','group')
															});
									}
								}
							}
							
							var i_mnth_end_effrt_activity_parent = a_get_mnth_end_effrt_activity[0].getId();
							nlapiLogExecution('audit','mnth end id:- ',i_mnth_end_effrt_activity_parent);
							
							/*var a_effort_plan_mnth_end_filter_1 = [['custrecord_month_end_parent', 'anyof', parseInt(i_mnth_end_effrt_activity_parent)]];
							
							var a_columns_mnth_end_effort_plan_srch_1 = new Array();
							a_columns_mnth_end_effort_plan_srch[0] = new nlobjSearchColumn('custrecord_month_end_sub_practice',null,'group').setSort(false);
							a_columns_mnth_end_effort_plan_srch[1] = new nlobjSearchColumn('custrecord_mnth_end_practice',null,'group');
							a_columns_mnth_end_effort_plan_srch[2] = new nlobjSearchColumn('custrecord_mnth_end_role',null,'group');
							a_columns_mnth_end_effort_plan_srch[3] = new nlobjSearchColumn('custrecord_mnth_end_level',null,'group');
							a_columns_mnth_end_effort_plan_srch[4] = new nlobjSearchColumn('custrecord_location_mnth_end',null,'group');
							
							var a_get_mnth_end_effrt_plan_prac_1 = nlapiSearchRecord('customrecord_fp_revrec_month_end_effort', null, a_effort_plan_mnth_end_filter, a_columns_mnth_end_effort_plan_srch);
							if (a_get_mnth_end_effrt_plan_prac_1)
							{
								var i_current_sublist_length = a_get_mnth_end_effrt_plan_prac_1.length;
							}
							, 'and',
																['custrecord_month_end_sub_practice', 'anyof', 320]*/
							
							var a_effort_plan_mnth_end_filter = [['custrecord_month_end_parent', 'anyof', parseInt(i_mnth_end_effrt_activity_parent)]];
							
							var a_columns_mnth_end_effort_plan_srch = new Array();
							a_columns_mnth_end_effort_plan_srch[0] = new nlobjSearchColumn('custrecord_month_end_sub_practice',null,'group').setSort(false);
							a_columns_mnth_end_effort_plan_srch[1] = new nlobjSearchColumn('custrecord_mnth_end_practice',null,'group');
							a_columns_mnth_end_effort_plan_srch[2] = new nlobjSearchColumn('custrecord_mnth_end_role',null,'group').setSort(false);
							a_columns_mnth_end_effort_plan_srch[3] = new nlobjSearchColumn('custrecord_mnth_end_level',null,'group').setSort(false);
							a_columns_mnth_end_effort_plan_srch[4] = new nlobjSearchColumn('custrecord_location_mnth_end',null,'group').setSort(false);
							a_columns_mnth_end_effort_plan_srch[5] = new nlobjSearchColumn('custrecord_resource_cost',null,'group').setSort(false);
							a_columns_mnth_end_effort_plan_srch[6] = new nlobjSearchColumn('custrecord_percent_allocated',null,'sum');
							a_columns_mnth_end_effort_plan_srch[7] = new nlobjSearchColumn('custrecord_month_end_month_name',null,'group');
							a_columns_mnth_end_effort_plan_srch[8] = new nlobjSearchColumn('custrecord_month_end_year_name',null,'group').setSort(false);
							a_columns_mnth_end_effort_plan_srch[9] = new nlobjSearchColumn('custrecord_revenue_share_mnth_end',null,'group');
							a_columns_mnth_end_effort_plan_srch[10] = new nlobjSearchColumn('custrecord_month_end_month_name',null,'group');
							
							var a_get_mnth_end_effrt_plan_prac = nlapiSearchRecord('customrecord_fp_revrec_month_end_effort', null, a_effort_plan_mnth_end_filter, a_columns_mnth_end_effort_plan_srch);
							if (a_get_mnth_end_effrt_plan_prac)
							{
								nlapiLogExecution('audit','effrt length(CUSTOM TABLE):- '+a_get_mnth_end_effrt_plan_prac.length);
								
								var i_previous_sub_prac = 0;
								var i_previous_role = 0;
								var i_previous_level = 0;
								var i_previous_location = 0;
								var s_prev_month = '';
								var f_revised_allocation = 0;
								var i_previous_cost = 0; //Added on 5th Aug'21
								var i_line_no = 1;
								var a_contractor_avearge = new Array();
								var i_frst_contractor = 0;
								var i_total_contractor_row_flag = 0;
								var f_total_contractor_resource_cost = 0;
								for(var i_mnth_end_plan=0; i_mnth_end_plan<a_get_mnth_end_effrt_plan_prac.length; i_mnth_end_plan++)
								{
									if(i_previous_sub_prac == 0)
									{
										i_previous_sub_prac = a_get_mnth_end_effrt_plan_prac[i_mnth_end_plan].getValue('custrecord_month_end_sub_practice',null,'group');
										i_previous_role = a_get_mnth_end_effrt_plan_prac[i_mnth_end_plan].getValue('custrecord_mnth_end_role',null,'group');
										i_previous_level = a_get_mnth_end_effrt_plan_prac[i_mnth_end_plan].getValue('custrecord_mnth_end_level',null,'group');
										i_previous_location = a_get_mnth_end_effrt_plan_prac[i_mnth_end_plan].getValue('custrecord_location_mnth_end',null,'group');
										i_previous_cost = a_get_mnth_end_effrt_plan_prac[i_mnth_end_plan].getValue('custrecord_resource_cost', null, 'group');
									}
									
									if(i_previous_sub_prac != a_get_mnth_end_effrt_plan_prac[i_mnth_end_plan].getValue('custrecord_month_end_sub_practice',null,'group'))
									{
										//i_current_sublist_length
										i_line_no++;
										i_previous_sub_prac = a_get_mnth_end_effrt_plan_prac[i_mnth_end_plan].getValue('custrecord_month_end_sub_practice',null,'group');
										i_previous_role = a_get_mnth_end_effrt_plan_prac[i_mnth_end_plan].getValue('custrecord_mnth_end_role',null,'group');
										i_previous_level = a_get_mnth_end_effrt_plan_prac[i_mnth_end_plan].getValue('custrecord_mnth_end_level',null,'group');
										i_previous_location = a_get_mnth_end_effrt_plan_prac[i_mnth_end_plan].getValue('custrecord_location_mnth_end',null,'group');
										i_previous_cost = a_get_mnth_end_effrt_plan_prac[i_mnth_end_plan].getValue('custrecord_resource_cost', null, 'group');
									}
									else if(i_previous_sub_prac == a_get_mnth_end_effrt_plan_prac[i_mnth_end_plan].getValue('custrecord_month_end_sub_practice',null,'group'))
									{
										if(i_previous_level != a_get_mnth_end_effrt_plan_prac[i_mnth_end_plan].getValue('custrecord_mnth_end_level',null,'group'))
										{
											if (a_get_mnth_end_effrt_plan_prac[i_mnth_end_plan].getValue('custrecord_mnth_end_level',null,'group')) {
												i_line_no++;
												i_previous_sub_prac = a_get_mnth_end_effrt_plan_prac[i_mnth_end_plan].getValue('custrecord_month_end_sub_practice',null,'group');
												i_previous_role = a_get_mnth_end_effrt_plan_prac[i_mnth_end_plan].getValue('custrecord_mnth_end_role',null,'group');
												i_previous_level = a_get_mnth_end_effrt_plan_prac[i_mnth_end_plan].getValue('custrecord_mnth_end_level',null,'group');
												i_previous_location = a_get_mnth_end_effrt_plan_prac[i_mnth_end_plan].getValue('custrecord_location_mnth_end',null,'group');
												i_previous_cost = a_get_mnth_end_effrt_plan_prac[i_mnth_end_plan].getValue('custrecord_resource_cost', null, 'group');
											}
										}
										else if(i_previous_role != a_get_mnth_end_effrt_plan_prac[i_mnth_end_plan].getValue('custrecord_mnth_end_role',null,'group'))
										{
											if (a_get_mnth_end_effrt_plan_prac[i_mnth_end_plan].getValue('custrecord_mnth_end_role',null,'group')) {
												i_line_no++;
												i_previous_sub_prac = a_get_mnth_end_effrt_plan_prac[i_mnth_end_plan].getValue('custrecord_month_end_sub_practice',null,'group');
												i_previous_role = a_get_mnth_end_effrt_plan_prac[i_mnth_end_plan].getValue('custrecord_mnth_end_role',null,'group');
												i_previous_level = a_get_mnth_end_effrt_plan_prac[i_mnth_end_plan].getValue('custrecord_mnth_end_level',null,'group');
												i_previous_location = a_get_mnth_end_effrt_plan_prac[i_mnth_end_plan].getValue('custrecord_location_mnth_end',null,'group');
												i_previous_cost = a_get_mnth_end_effrt_plan_prac[i_mnth_end_plan].getValue('custrecord_resource_cost', null, 'group');
											}
										}
										else if(i_previous_location != a_get_mnth_end_effrt_plan_prac[i_mnth_end_plan].getValue('custrecord_location_mnth_end',null,'group'))
										{
											i_line_no++;
											i_previous_sub_prac = a_get_mnth_end_effrt_plan_prac[i_mnth_end_plan].getValue('custrecord_month_end_sub_practice',null,'group');
											i_previous_role = a_get_mnth_end_effrt_plan_prac[i_mnth_end_plan].getValue('custrecord_mnth_end_role',null,'group');
											i_previous_level = a_get_mnth_end_effrt_plan_prac[i_mnth_end_plan].getValue('custrecord_mnth_end_level',null,'group');
											i_previous_location = a_get_mnth_end_effrt_plan_prac[i_mnth_end_plan].getValue('custrecord_location_mnth_end',null,'group');
											i_previous_cost = a_get_mnth_end_effrt_plan_prac[i_mnth_end_plan].getValue('custrecord_resource_cost', null, 'group');
											
										}
										else if (i_previous_cost != a_get_mnth_end_effrt_plan_prac[i_mnth_end_plan].getValue('custrecord_resource_cost', null, 'group')) {	
                                            i_line_no++;	
                                            i_previous_sub_prac = a_get_mnth_end_effrt_plan_prac[i_mnth_end_plan].getValue('custrecord_month_end_sub_practice', null, 'group');	
                                            i_previous_role = a_get_mnth_end_effrt_plan_prac[i_mnth_end_plan].getValue('custrecord_mnth_end_role', null, 'group');	
                                            i_previous_level = a_get_mnth_end_effrt_plan_prac[i_mnth_end_plan].getValue('custrecord_mnth_end_level', null, 'group');	
                                            i_previous_location = a_get_mnth_end_effrt_plan_prac[i_mnth_end_plan].getValue('custrecord_location_mnth_end', null, 'group');	
                                            i_previous_cost = a_get_mnth_end_effrt_plan_prac[i_mnth_end_plan].getValue('custrecord_resource_cost', null, 'group');	
                                        }
									}
									
									var i_is_prac_present_already = find_prac_exist(a_current_pract,a_get_mnth_end_effrt_plan_prac[i_mnth_end_plan].getValue('custrecord_month_end_sub_practice',null,'group'),a_get_mnth_end_effrt_plan_prac[i_mnth_end_plan].getValue('custrecord_mnth_end_level',null,'group'),a_get_mnth_end_effrt_plan_prac[i_mnth_end_plan].getValue('custrecord_location_mnth_end',null,'group'));
									
									if(i_is_prac_present_already == 0)
									{
										a_current_pract.push({
												'i_sub_practice': a_get_mnth_end_effrt_plan_prac[i_mnth_end_plan].getValue('custrecord_month_end_sub_practice',null,'group'),
												'i_level': a_get_mnth_end_effrt_plan_prac[i_mnth_end_plan].getValue('custrecord_mnth_end_level',null,'group'),
												'i_location': a_get_mnth_end_effrt_plan_prac[i_mnth_end_plan].getValue('custrecord_location_mnth_end',null,'group'),
												'i_line_no': i_line_no
											});
									}
									
									/*if ((a_current_pract_unique_list_sub_prac.indexOf(a_get_mnth_end_effrt_plan_prac[i_mnth_end_plan].getValue('custrecord_month_end_sub_practice')) < 0))
									{test
										if ((a_current_pract_unique_list_level.indexOf(a_get_mnth_end_effrt_plan_prac[i_mnth_end_plan].getValue('custrecord_mnth_end_level')) < 0))
										{
											a_current_pract.push({
												'i_sub_practice': a_get_mnth_end_effrt_plan_prac[i_mnth_end_plan].getValue('custrecord_month_end_sub_practice'),
												'i_level': a_get_mnth_end_effrt_plan_prac[i_mnth_end_plan].getValue('custrecord_mnth_end_level')
											});
										}
									}*/
									
									var s_mnth_year_1 = '';
									var s_mnth_name = a_get_mnth_end_effrt_plan_prac[i_mnth_end_plan].getValue('custrecord_month_end_month_name',null,'group');
									var s_year_name = a_get_mnth_end_effrt_plan_prac[i_mnth_end_plan].getValue('custrecord_month_end_year_name',null,'group');
									s_year_name = s_year_name.split('.');
									s_year_name = s_year_name[0];
									s_mnth_year_1 = s_mnth_name+'_'+s_year_name;
									
									var s_mnth_year = s_month_current+'_'+s_year_current;
									
									if(s_mnth_year_1 == s_mnth_year)
									{
										var i_allocation_to_display = 0;
									}
									else
									{
										var i_allocation_to_display = a_get_mnth_end_effrt_plan_prac[i_mnth_end_plan].getValue('custrecord_percent_allocated',null,'sum');
									}
									
									i_allocation_to_display = parseFloat(i_allocation_to_display);
									
									/*if(a_get_mnth_end_effrt_plan_prac[i_mnth_end_plan].getValue('custrecord_mnth_end_level',null,'group') == 13)
									{
										if(i_frst_contractor == 0)
										{
											a_contractor_avearge.push(s_mnth_name);
											i_frst_contractor = 1;
										}
										
										if(a_contractor_avearge.indexOf(s_mnth_name) >= 0)
										{
											nlapiLogExecution('audit','resource cost actual:- '+a_get_mnth_end_effrt_plan_prac[i_mnth_end_plan].getValue('custrecord_resource_cost',null,'group'));
											i_total_contractor_row_flag++;
											f_total_contractor_resource_cost = parseFloat(f_total_contractor_resource_cost) + parseFloat(a_get_mnth_end_effrt_plan_prac[i_mnth_end_plan].getValue('custrecord_resource_cost',null,'group'));
											nlapiLogExecution('audit','f_total_contractor_resource_cost:- '+f_total_contractor_resource_cost,'i_total_contractor_row_flag:- '+i_total_contractor_row_flag);
										}
										
										var f_total_contractor_avg_cost = parseFloat(f_total_contractor_resource_cost) / parseFloat(i_total_contractor_row_flag);
										fld_sublist_effort_plan_mnth_end.setLineItemValue('resource_cost_month_end',i_line_no,parseFloat(f_total_contractor_avg_cost).toFixed(0));
									}
									else*/
									{
										fld_sublist_effort_plan_mnth_end.setLineItemValue('resource_cost_month_end',i_line_no,parseFloat(a_get_mnth_end_effrt_plan_prac[i_mnth_end_plan].getValue('custrecord_resource_cost',null,'group')).toFixed(0));
									}
									
									fld_sublist_effort_plan_mnth_end.setLineItemValue('parent_practice_month_end',i_line_no,a_get_mnth_end_effrt_plan_prac[i_mnth_end_plan].getValue('custrecord_mnth_end_practice',null,'group'));
									fld_sublist_effort_plan_mnth_end.setLineItemValue('sub_practice_month_end',i_line_no,a_get_mnth_end_effrt_plan_prac[i_mnth_end_plan].getValue('custrecord_month_end_sub_practice',null,'group'));
									fld_sublist_effort_plan_mnth_end.setLineItemValue('role_month_end',i_line_no,a_get_mnth_end_effrt_plan_prac[i_mnth_end_plan].getValue('custrecord_mnth_end_role',null,'group'));
									fld_sublist_effort_plan_mnth_end.setLineItemValue('level_month_end',i_line_no,a_get_mnth_end_effrt_plan_prac[i_mnth_end_plan].getValue('custrecord_mnth_end_level',null,'group'));
									fld_sublist_effort_plan_mnth_end.setLineItemValue('location_month_end',i_line_no,a_get_mnth_end_effrt_plan_prac[i_mnth_end_plan].getValue('custrecord_location_mnth_end',null,'group'));
									fld_sublist_effort_plan_mnth_end.setLineItemValue('revenue_share_mnth_end',i_line_no,parseFloat(a_get_mnth_end_effrt_plan_prac[i_mnth_end_plan].getValue('custrecord_revenue_share_mnth_end',null,'group')).toFixed(0));
									
									// set effort from existing effort plan
									var s_current_mnth = a_get_mnth_end_effrt_plan_prac[i_mnth_end_plan].getValue('custrecord_month_end_month_name',null,'group');
									if(!s_prev_month)
									{
										s_prev_month = s_current_mnth;
									}
									
									//nlapiLogExecution('audit','s_current_mnth:- '+s_current_mnth,'s_prev_month:- '+s_prev_month);
									if(s_current_mnth == s_prev_month)
									{
										f_revised_allocation = parseFloat(f_revised_allocation) + parseFloat(i_allocation_to_display);
									}
									else
									{
										var f_revised_allocation = 0;
										s_prev_month = s_current_mnth;
									}
									
									fld_sublist_effort_plan_mnth_end.setLineItemValue(''+s_mnth_year_1.toLowerCase(),i_line_no,parseFloat(i_allocation_to_display).toFixed(2));
									
									/*if(a_current_pract_unique_list_sub_prac.indexOf(a_get_mnth_end_effrt_plan_prac[i_mnth_end_plan].getValue('custrecord_month_end_sub_practice')) < 0)
									{
										if(a_current_pract_unique_list_level.indexOf(a_get_mnth_end_effrt_plan_prac[i_mnth_end_plan].getValue('custrecord_mnth_end_level')) < 0)
										{
											if(a_current_pract_unique_list_role.indexOf(a_get_mnth_end_effrt_plan_prac[i_mnth_end_plan].getValue('custrecord_mnth_end_role')) < 0)
											{
												//nlapiLogExecution('audit','inside nothing found');
												//nlapiLogExecution('audit','sub prac:- ',a_get_mnth_end_effrt_plan_prac[i_mnth_end_plan].getText('custrecord_month_end_sub_practice'));
												//nlapiLogExecution('audit','level:- ',a_get_mnth_end_effrt_plan_prac[i_mnth_end_plan].getText('custrecord_mnth_end_level'));
												//nlapiLogExecution('audit','role:- ',a_get_mnth_end_effrt_plan_prac[i_mnth_end_plan].getText('custrecord_mnth_end_role'));
														
												fld_sublist_effort_plan_mnth_end.setLineItemValue('parent_practice_month_end',i_line_no,a_get_mnth_end_effrt_plan_prac[i_mnth_end_plan].getValue('custrecord_mnth_end_practice'));
												fld_sublist_effort_plan_mnth_end.setLineItemValue('sub_practice_month_end',i_line_no,a_get_mnth_end_effrt_plan_prac[i_mnth_end_plan].getValue('custrecord_month_end_sub_practice'));
												fld_sublist_effort_plan_mnth_end.setLineItemValue('role_month_end',i_line_no,a_get_mnth_end_effrt_plan_prac[i_mnth_end_plan].getValue('custrecord_mnth_end_role'));
												fld_sublist_effort_plan_mnth_end.setLineItemValue('level_month_end',i_line_no,a_get_mnth_end_effrt_plan_prac[i_mnth_end_plan].getValue('custrecord_mnth_end_level'));
												fld_sublist_effort_plan_mnth_end.setLineItemValue('location_month_end',i_line_no,a_get_mnth_end_effrt_plan_prac[i_mnth_end_plan].getValue('custrecord_location_mnth_end'));
												fld_sublist_effort_plan_mnth_end.setLineItemValue('resource_cost_month_end',i_line_no,a_get_mnth_end_effrt_plan_prac[i_mnth_end_plan].getValue('custrecord_resource_cost'));
												fld_sublist_effort_plan_mnth_end.setLineItemValue('revenue_share_mnth_end',i_line_no,a_get_mnth_end_effrt_plan_prac[i_mnth_end_plan].getValue('custrecord_revenue_share_mnth_end'));
												fld_sublist_effort_plan_mnth_end.setLineItemValue(''+s_mnth_year_1.toLowerCase(),i_line_no,i_allocation_to_display);
											
												a_current_pract_unique_list_sub_prac.push({
													'prac':a_get_mnth_end_effrt_plan_prac[i_mnth_end_plan].getValue('custrecord_month_end_sub_practice'),
													'level':a_get_mnth_end_effrt_plan_prac[i_mnth_end_plan].getValue('custrecord_mnth_end_level'),
													'role':a_get_mnth_end_effrt_plan_prac[i_mnth_end_plan].getValue('custrecord_mnth_end_role')});
												a_current_pract_unique_list_level.push(a_get_mnth_end_effrt_plan_prac[i_mnth_end_plan].getValue('custrecord_mnth_end_level'));
												a_current_pract_unique_list_role.push(a_get_mnth_end_effrt_plan_prac[i_mnth_end_plan].getValue('custrecord_mnth_end_role'));
										
											}
											else
											{
												//nlapiLogExecution('audit','inside sub prac n level nt fnd, role found');
												//nlapiLogExecution('audit','sub prac:- ',a_get_mnth_end_effrt_plan_prac[i_mnth_end_plan].getText('custrecord_month_end_sub_practice'));
												//nlapiLogExecution('audit','level:- ',a_get_mnth_end_effrt_plan_prac[i_mnth_end_plan].getText('custrecord_mnth_end_level'));
												//nlapiLogExecution('audit','role:- ',a_get_mnth_end_effrt_plan_prac[i_mnth_end_plan].getText('custrecord_mnth_end_role'));
														
												fld_sublist_effort_plan_mnth_end.setLineItemValue('parent_practice_month_end',i_line_no,a_get_mnth_end_effrt_plan_prac[i_mnth_end_plan].getValue('custrecord_mnth_end_practice'));
												fld_sublist_effort_plan_mnth_end.setLineItemValue('sub_practice_month_end',i_line_no,a_get_mnth_end_effrt_plan_prac[i_mnth_end_plan].getValue('custrecord_month_end_sub_practice'));
												fld_sublist_effort_plan_mnth_end.setLineItemValue('role_month_end',i_line_no,a_get_mnth_end_effrt_plan_prac[i_mnth_end_plan].getValue('custrecord_mnth_end_role'));
												fld_sublist_effort_plan_mnth_end.setLineItemValue('level_month_end',i_line_no,a_get_mnth_end_effrt_plan_prac[i_mnth_end_plan].getValue('custrecord_mnth_end_level'));
												fld_sublist_effort_plan_mnth_end.setLineItemValue('location_month_end',i_line_no,a_get_mnth_end_effrt_plan_prac[i_mnth_end_plan].getValue('custrecord_location_mnth_end'));
												fld_sublist_effort_plan_mnth_end.setLineItemValue('resource_cost_month_end',i_line_no,a_get_mnth_end_effrt_plan_prac[i_mnth_end_plan].getValue('custrecord_resource_cost'));
												fld_sublist_effort_plan_mnth_end.setLineItemValue('revenue_share_mnth_end',i_line_no,a_get_mnth_end_effrt_plan_prac[i_mnth_end_plan].getValue('custrecord_revenue_share_mnth_end'));
												fld_sublist_effort_plan_mnth_end.setLineItemValue(''+s_mnth_year_1.toLowerCase(),i_line_no,i_allocation_to_display);
											
												a_current_pract_unique_list_sub_prac.push({
													'prac':a_get_mnth_end_effrt_plan_prac[i_mnth_end_plan].getValue('custrecord_month_end_sub_practice'),
													'level':a_get_mnth_end_effrt_plan_prac[i_mnth_end_plan].getValue('custrecord_mnth_end_level'),
													'role':a_get_mnth_end_effrt_plan_prac[i_mnth_end_plan].getValue('custrecord_mnth_end_role')});
												a_current_pract_unique_list_level.push(a_get_mnth_end_effrt_plan_prac[i_mnth_end_plan].getValue('custrecord_mnth_end_level'));
												a_current_pract_unique_list_role.push(a_get_mnth_end_effrt_plan_prac[i_mnth_end_plan].getValue('custrecord_mnth_end_role'));
										
											}
										}
										else
										{
											//nlapiLogExecution('audit','inside sub prac nt found level found');
											//nlapiLogExecution('audit','sub prac:- ',a_get_mnth_end_effrt_plan_prac[i_mnth_end_plan].getText('custrecord_month_end_sub_practice'));
											//nlapiLogExecution('audit','level:- ',a_get_mnth_end_effrt_plan_prac[i_mnth_end_plan].getText('custrecord_mnth_end_level'));
											//nlapiLogExecution('audit','role:- ',a_get_mnth_end_effrt_plan_prac[i_mnth_end_plan].getText('custrecord_mnth_end_role'));
													
											fld_sublist_effort_plan_mnth_end.setLineItemValue('parent_practice_month_end',i_line_no,a_get_mnth_end_effrt_plan_prac[i_mnth_end_plan].getValue('custrecord_mnth_end_practice'));
											fld_sublist_effort_plan_mnth_end.setLineItemValue('sub_practice_month_end',i_line_no,a_get_mnth_end_effrt_plan_prac[i_mnth_end_plan].getValue('custrecord_month_end_sub_practice'));
											fld_sublist_effort_plan_mnth_end.setLineItemValue('role_month_end',i_line_no,a_get_mnth_end_effrt_plan_prac[i_mnth_end_plan].getValue('custrecord_mnth_end_role'));
											fld_sublist_effort_plan_mnth_end.setLineItemValue('level_month_end',i_line_no,a_get_mnth_end_effrt_plan_prac[i_mnth_end_plan].getValue('custrecord_mnth_end_level'));
											fld_sublist_effort_plan_mnth_end.setLineItemValue('location_month_end',i_line_no,a_get_mnth_end_effrt_plan_prac[i_mnth_end_plan].getValue('custrecord_location_mnth_end'));
											fld_sublist_effort_plan_mnth_end.setLineItemValue('resource_cost_month_end',i_line_no,a_get_mnth_end_effrt_plan_prac[i_mnth_end_plan].getValue('custrecord_resource_cost'));
											fld_sublist_effort_plan_mnth_end.setLineItemValue('revenue_share_mnth_end',i_line_no,a_get_mnth_end_effrt_plan_prac[i_mnth_end_plan].getValue('custrecord_revenue_share_mnth_end'));
											fld_sublist_effort_plan_mnth_end.setLineItemValue(''+s_mnth_year_1.toLowerCase(),i_line_no,i_allocation_to_display);
										
											a_current_pract_unique_list_sub_prac.push({
													'prac':a_get_mnth_end_effrt_plan_prac[i_mnth_end_plan].getValue('custrecord_month_end_sub_practice'),
													'level':a_get_mnth_end_effrt_plan_prac[i_mnth_end_plan].getValue('custrecord_mnth_end_level'),
													'role':a_get_mnth_end_effrt_plan_prac[i_mnth_end_plan].getValue('custrecord_mnth_end_role')});
											a_current_pract_unique_list_level.push(a_get_mnth_end_effrt_plan_prac[i_mnth_end_plan].getValue('custrecord_mnth_end_level'));
											a_current_pract_unique_list_role.push(a_get_mnth_end_effrt_plan_prac[i_mnth_end_plan].getValue('custrecord_mnth_end_role'));
										
										}
									}
									else
									{
										if(a_current_pract_unique_list_level.indexOf(a_get_mnth_end_effrt_plan_prac[i_mnth_end_plan].getValue('custrecord_mnth_end_level')) < 0)
										{
											//nlapiLogExecution('audit','inside sub prac found n level nt found');
											//nlapiLogExecution('audit','sub prac:- ',a_get_mnth_end_effrt_plan_prac[i_mnth_end_plan].getText('custrecord_month_end_sub_practice'));
											//nlapiLogExecution('audit','level:- ',a_get_mnth_end_effrt_plan_prac[i_mnth_end_plan].getText('custrecord_mnth_end_level'));
											//nlapiLogExecution('audit','role:- ',a_get_mnth_end_effrt_plan_prac[i_mnth_end_plan].getText('custrecord_mnth_end_role'));
													
											fld_sublist_effort_plan_mnth_end.setLineItemValue('parent_practice_month_end',i_line_no,a_get_mnth_end_effrt_plan_prac[i_mnth_end_plan].getValue('custrecord_mnth_end_practice'));
											fld_sublist_effort_plan_mnth_end.setLineItemValue('sub_practice_month_end',i_line_no,a_get_mnth_end_effrt_plan_prac[i_mnth_end_plan].getValue('custrecord_month_end_sub_practice'));
											fld_sublist_effort_plan_mnth_end.setLineItemValue('role_month_end',i_line_no,a_get_mnth_end_effrt_plan_prac[i_mnth_end_plan].getValue('custrecord_mnth_end_role'));
											fld_sublist_effort_plan_mnth_end.setLineItemValue('level_month_end',i_line_no,a_get_mnth_end_effrt_plan_prac[i_mnth_end_plan].getValue('custrecord_mnth_end_level'));
											fld_sublist_effort_plan_mnth_end.setLineItemValue('location_month_end',i_line_no,a_get_mnth_end_effrt_plan_prac[i_mnth_end_plan].getValue('custrecord_location_mnth_end'));
											fld_sublist_effort_plan_mnth_end.setLineItemValue('resource_cost_month_end',i_line_no,a_get_mnth_end_effrt_plan_prac[i_mnth_end_plan].getValue('custrecord_resource_cost'));
											fld_sublist_effort_plan_mnth_end.setLineItemValue('revenue_share_mnth_end',i_line_no,a_get_mnth_end_effrt_plan_prac[i_mnth_end_plan].getValue('custrecord_revenue_share_mnth_end'));
											fld_sublist_effort_plan_mnth_end.setLineItemValue(''+s_mnth_year_1.toLowerCase(),i_line_no,i_allocation_to_display);
										
											a_current_pract_unique_list_sub_prac.push({
													'prac':a_get_mnth_end_effrt_plan_prac[i_mnth_end_plan].getValue('custrecord_month_end_sub_practice'),
													'level':a_get_mnth_end_effrt_plan_prac[i_mnth_end_plan].getValue('custrecord_mnth_end_level'),
													'role':a_get_mnth_end_effrt_plan_prac[i_mnth_end_plan].getValue('custrecord_mnth_end_role')});
											a_current_pract_unique_list_level.push(a_get_mnth_end_effrt_plan_prac[i_mnth_end_plan].getValue('custrecord_mnth_end_level'));
											a_current_pract_unique_list_role.push(a_get_mnth_end_effrt_plan_prac[i_mnth_end_plan].getValue('custrecord_mnth_end_role'));
										
										}
										else
										{
											if(!a_get_mnth_end_effrt_plan_prac[i_mnth_end_plan].getValue('custrecord_mnth_end_role'))
											{
												//nlapiLogExecution('audit','inside sub prac found, level found n role nt found');
												//nlapiLogExecution('audit','sub prac:- ',a_get_mnth_end_effrt_plan_prac[i_mnth_end_plan].getText('custrecord_month_end_sub_practice'));
												//nlapiLogExecution('audit','level:- ',a_get_mnth_end_effrt_plan_prac[i_mnth_end_plan].getText('custrecord_mnth_end_level'));
												//nlapiLogExecution('audit','role:- ',a_get_mnth_end_effrt_plan_prac[i_mnth_end_plan].getText('custrecord_mnth_end_role'));
														
												fld_sublist_effort_plan_mnth_end.setLineItemValue('parent_practice_month_end',i_line_no,a_get_mnth_end_effrt_plan_prac[i_mnth_end_plan].getValue('custrecord_mnth_end_practice'));
												fld_sublist_effort_plan_mnth_end.setLineItemValue('sub_practice_month_end',i_line_no,a_get_mnth_end_effrt_plan_prac[i_mnth_end_plan].getValue('custrecord_month_end_sub_practice'));
												fld_sublist_effort_plan_mnth_end.setLineItemValue('role_month_end',i_line_no,a_get_mnth_end_effrt_plan_prac[i_mnth_end_plan].getValue('custrecord_mnth_end_role'));
												fld_sublist_effort_plan_mnth_end.setLineItemValue('level_month_end',i_line_no,a_get_mnth_end_effrt_plan_prac[i_mnth_end_plan].getValue('custrecord_mnth_end_level'));
												fld_sublist_effort_plan_mnth_end.setLineItemValue('location_month_end',i_line_no,a_get_mnth_end_effrt_plan_prac[i_mnth_end_plan].getValue('custrecord_location_mnth_end'));
												fld_sublist_effort_plan_mnth_end.setLineItemValue('resource_cost_month_end',i_line_no,a_get_mnth_end_effrt_plan_prac[i_mnth_end_plan].getValue('custrecord_resource_cost'));
												fld_sublist_effort_plan_mnth_end.setLineItemValue('revenue_share_mnth_end',i_line_no,a_get_mnth_end_effrt_plan_prac[i_mnth_end_plan].getValue('custrecord_revenue_share_mnth_end'));
												fld_sublist_effort_plan_mnth_end.setLineItemValue(''+s_mnth_year_1.toLowerCase(),i_line_no,i_allocation_to_display);
											
												a_current_pract_unique_list_sub_prac.push({
													'prac':a_get_mnth_end_effrt_plan_prac[i_mnth_end_plan].getValue('custrecord_month_end_sub_practice'),
													'level':a_get_mnth_end_effrt_plan_prac[i_mnth_end_plan].getValue('custrecord_mnth_end_level'),
													'role':a_get_mnth_end_effrt_plan_prac[i_mnth_end_plan].getValue('custrecord_mnth_end_role')});
												a_current_pract_unique_list_level.push(a_get_mnth_end_effrt_plan_prac[i_mnth_end_plan].getValue('custrecord_mnth_end_level'));
												a_current_pract_unique_list_role.push(a_get_mnth_end_effrt_plan_prac[i_mnth_end_plan].getValue('custrecord_mnth_end_role'));
										
											}
											
											if(a_current_pract_unique_list_role.indexOf(a_get_mnth_end_effrt_plan_prac[i_mnth_end_plan].getValue('custrecord_mnth_end_role')) < 0)
											{
												//nlapiLogExecution('audit','inside sub prac found, level found n role nt found');
												//nlapiLogExecution('audit','sub prac:- ',a_get_mnth_end_effrt_plan_prac[i_mnth_end_plan].getText('custrecord_month_end_sub_practice'));
												//nlapiLogExecution('audit','level:- ',a_get_mnth_end_effrt_plan_prac[i_mnth_end_plan].getText('custrecord_mnth_end_level'));
												//nlapiLogExecution('audit','role:- ',a_get_mnth_end_effrt_plan_prac[i_mnth_end_plan].getText('custrecord_mnth_end_role'));
														
												fld_sublist_effort_plan_mnth_end.setLineItemValue('parent_practice_month_end',i_line_no,a_get_mnth_end_effrt_plan_prac[i_mnth_end_plan].getValue('custrecord_mnth_end_practice'));
												fld_sublist_effort_plan_mnth_end.setLineItemValue('sub_practice_month_end',i_line_no,a_get_mnth_end_effrt_plan_prac[i_mnth_end_plan].getValue('custrecord_month_end_sub_practice'));
												fld_sublist_effort_plan_mnth_end.setLineItemValue('role_month_end',i_line_no,a_get_mnth_end_effrt_plan_prac[i_mnth_end_plan].getValue('custrecord_mnth_end_role'));
												fld_sublist_effort_plan_mnth_end.setLineItemValue('level_month_end',i_line_no,a_get_mnth_end_effrt_plan_prac[i_mnth_end_plan].getValue('custrecord_mnth_end_level'));
												fld_sublist_effort_plan_mnth_end.setLineItemValue('location_month_end',i_line_no,a_get_mnth_end_effrt_plan_prac[i_mnth_end_plan].getValue('custrecord_location_mnth_end'));
												fld_sublist_effort_plan_mnth_end.setLineItemValue('resource_cost_month_end',i_line_no,a_get_mnth_end_effrt_plan_prac[i_mnth_end_plan].getValue('custrecord_resource_cost'));
												fld_sublist_effort_plan_mnth_end.setLineItemValue('revenue_share_mnth_end',i_line_no,a_get_mnth_end_effrt_plan_prac[i_mnth_end_plan].getValue('custrecord_revenue_share_mnth_end'));
												fld_sublist_effort_plan_mnth_end.setLineItemValue(''+s_mnth_year_1.toLowerCase(),i_line_no,i_allocation_to_display);
											
												a_current_pract_unique_list_sub_prac.push({
													'prac':a_get_mnth_end_effrt_plan_prac[i_mnth_end_plan].getValue('custrecord_month_end_sub_practice'),
													'level':a_get_mnth_end_effrt_plan_prac[i_mnth_end_plan].getValue('custrecord_mnth_end_level'),
													'role':a_get_mnth_end_effrt_plan_prac[i_mnth_end_plan].getValue('custrecord_mnth_end_role')});
												a_current_pract_unique_list_level.push(a_get_mnth_end_effrt_plan_prac[i_mnth_end_plan].getValue('custrecord_mnth_end_level'));
												a_current_pract_unique_list_role.push(a_get_mnth_end_effrt_plan_prac[i_mnth_end_plan].getValue('custrecord_mnth_end_role'));
										
											}
											else
											{
												//nlapiLogExecution('audit','inside everything found:- ');
												//nlapiLogExecution('audit','month:- '+s_mnth_year_1.toLowerCase(),'allocation:- '+i_allocation_to_display);
												//nlapiLogExecution('audit','sub prac:- ',a_get_mnth_end_effrt_plan_prac[i_mnth_end_plan].getText('custrecord_month_end_sub_practice'));
												//nlapiLogExecution('audit','level:- ',a_get_mnth_end_effrt_plan_prac[i_mnth_end_plan].getText('custrecord_mnth_end_level'));
												//nlapiLogExecution('audit','role:- ',a_get_mnth_end_effrt_plan_prac[i_mnth_end_plan].getText('custrecord_mnth_end_role'));
												
												var f_existing_allo = fld_sublist_effort_plan_mnth_end.getLineItemValue(''+s_mnth_year_1.toLowerCase(),i_line_no);
												if(!f_existing_allo)
													f_existing_allo = 0;
													
												f_existing_allo = parseFloat(f_existing_allo) + parseFloat(i_allocation_to_display);
												
												// set effort from existing effort plan
												fld_sublist_effort_plan_mnth_end.setLineItemValue(''+s_mnth_year_1.toLowerCase(),i_line_no,f_existing_allo);
											}
										}
									}*/									
								}
							
								/*for(var i_index_current_prac=0; i_index_current_prac<a_current_pract.length; i_index_current_prac++)
								{
									var i_prac_found_flag = 0;
									var i_practice_index = -1;
									for (var i_index_current_allo = 0; i_index_current_allo < a_proj_prac_details.length; i_index_current_allo++)
									{
										if (a_current_pract[i_index_current_prac].i_sub_practice == a_proj_prac_details[i_index_current_allo].i_sub_practice && a_current_pract[i_index_current_prac].i_level == a_proj_prac_details[i_index_current_allo].s_emp_level)
										{
											i_prac_found_flag = 1;
											i_practice_index = i_index_current_allo;
										}
									}
									
									var i_allocation_prac = a_unique_sub_prac[i_practice_index];
									var i_index_allocation_prac = a_unique_sub_prac.indexOf(i_allocation_prac);
									
									if(i_prac_found_flag > 0)
									{
										var f_allocation_head = parseFloat(a_proj_prac_details[i_index_allocation_prac].f_prcnt_allocated);
										fld_sublist_effort_plan_mnth_end.setLineItemValue(''+s_mnth_year.toLowerCase(),i_index_current_prac+1,f_allocation_head);
										
										//if(a_proj_prac_details[i_index_allocation_prac].s_emp_level)
											//fld_sublist_effort_plan_mnth_end.setLineItemValue('level_month_end',i_index_current_prac+1,a_proj_prac_details[i_index_allocation_prac].s_emp_level);
										//i_mnth_end_plan++;
									}
								}*/
								
								// code for practice which is not thr in effrt plan but exist in allocation
								for (var i_allocation_index = 0; i_allocation_index < a_proj_prac_details.length; i_allocation_index++)
								{
									var i_practice_available = 0;
									//nlapiLogExecution('audit','current prac len:- ',a_current_pract.length);
									for (var i_current_index = 0; i_current_index < a_current_pract.length; i_current_index++)
									{
										if (a_current_pract[i_current_index].i_sub_practice == a_proj_prac_details[i_allocation_index].i_sub_practice && a_current_pract[i_current_index].i_level == a_proj_prac_details[i_allocation_index].s_emp_level && a_current_pract[i_current_index].i_location == a_proj_prac_details[i_allocation_index].i_resource_location) //i_location
										{
											i_practice_available = 1;
											
											nlapiLogExecution('audit','line no:- '+a_current_pract[i_current_index].i_line_no,'allocated percent:- '+a_proj_prac_details[i_allocation_index].f_prcnt_allocated);
											fld_sublist_effort_plan_mnth_end.setLineItemValue(''+s_mnth_year.toLowerCase(),a_current_pract[i_current_index].i_line_no,parseFloat(a_proj_prac_details[i_allocation_index].f_prcnt_allocated).toFixed(2));
										}
									}
									
									if(i_practice_available == 0)
									{
										i_line_no++;
										nlapiLogExecution('audit','level:- '+a_proj_prac_details[i_allocation_index].s_emp_level+':::i_line_no:- '+i_line_no,s_mnth_year);
										fld_sublist_effort_plan_mnth_end.setLineItemValue('parent_practice_month_end',i_line_no,a_proj_prac_details[i_allocation_index].i_practice);
										fld_sublist_effort_plan_mnth_end.setLineItemValue('sub_practice_month_end',i_line_no,a_proj_prac_details[i_allocation_index].i_sub_practice);
										fld_sublist_effort_plan_mnth_end.setLineItemValue(''+s_mnth_year.toLowerCase(),i_line_no,parseFloat(a_proj_prac_details[i_allocation_index].f_prcnt_allocated).toFixed(2));
										fld_sublist_effort_plan_mnth_end.setLineItemValue('level_month_end',i_line_no,a_proj_prac_details[i_allocation_index].s_emp_level);
										//fld_sublist_effort_plan_mnth_end.setLineItemValue('is_resource_allo_row',i_mnth_end_plan+1,'<html><body bgcolor="red"></body></html>');
										//i_line_no++;
										/*
										if(a_proj_prac_details[i_allocation_index].subsidiary != 3 && a_proj_prac_details[i_allocation_index].subsidiary != 12 && a_proj_prac_details[i_allocation_index].subsidiary != 7)
										{
											fld_sublist_effort_plan_mnth_end.setLineItemValue('location_month_end',i_line_no,'9');
										}
										else
										{
											fld_sublist_effort_plan_mnth_end.setLineItemValue('location_month_end',i_line_no,a_proj_prac_details[i_allocation_index].i_resource_location.toString());
										}
										*/
										
										var subsidiary_monthend=o_rev_loc_sub["Subsidairy"][a_proj_prac_details[i_allocation_index].subsidiary];	
										subsidiary_monthend=subsidiary_monthend?	
										fld_sublist_effort_plan_mnth_end.setLineItemValue('location_month_end', i_line_no, a_proj_prac_details[i_allocation_index].i_resource_location.toString())	
										:	
										fld_sublist_effort_plan_mnth_end.setLineItemValue('location_month_end', i_line_no, '9');
									}
								}
							}
							else
							{
								var s_nt_apprvd_msg = '<html>';
								s_nt_apprvd_msg += '<p><font size="3" color="red">Please wait system is generating month end effort as per budget plan, you will receive mail once process is completed.</font></p></html>';
								var fld_nt_apprvd_msg = o_form_obj.addField('s_nt_apprvd_msg', 'inlinehtml',null,null);
								fld_nt_apprvd_msg.setDefaultValue(s_nt_apprvd_msg);
							}
							
							/*var a_effort_plan_mnth_end_filter = [['custrecord_month_end_parent', 'anyof', parseInt(i_mnth_end_effrt_activity_parent)]];
									
							var a_columns_mnth_end_effort_plan_srch = new Array();
							a_columns_mnth_end_effort_plan_srch[0] = new nlobjSearchColumn('custrecord_month_end_sub_practice').setSort(false);
							a_columns_mnth_end_effort_plan_srch[1] = new nlobjSearchColumn('custrecord_percent_allocated');
							a_columns_mnth_end_effort_plan_srch[2] = new nlobjSearchColumn('custrecord_month_end_month_name');
							a_columns_mnth_end_effort_plan_srch[3] = new nlobjSearchColumn('custrecord_month_end_year_name');
							
							var a_get_mnth_end_effrt_plan = nlapiSearchRecord('customrecord_fp_revrec_month_end_effort', null, a_effort_plan_mnth_end_filter, a_columns_mnth_end_effort_plan_srch);
							if (a_get_mnth_end_effrt_plan)
							{
								var i_line_no = 0;
								var previous_prac = 0;
								
								for(var i_mnth_end_plan=0; i_mnth_end_plan<a_get_mnth_end_effrt_plan.length; i_mnth_end_plan++)
								{
									var s_month_name_effrt_plan = a_get_mnth_end_effrt_plan[i_mnth_end_plan].getValue('custrecord_month_end_month_name');
									var s_year_name_effrt_plan = a_get_mnth_end_effrt_plan[i_mnth_end_plan].getValue('custrecord_month_end_year_name');
									s_year_name_effrt_plan = s_year_name_effrt_plan.split('.');
									s_year_name_effrt_plan = s_year_name_effrt_plan[0];
									var s_month_name = s_month_name_effrt_plan+'_'+s_year_name_effrt_plan;
								
									if(previous_prac == 0)
									{
										previous_prac = a_get_mnth_end_effrt_plan[i_mnth_end_plan].getValue('custrecord_month_end_sub_practice');
									}
									
									if(previous_prac != a_get_mnth_end_effrt_plan[i_mnth_end_plan].getValue('custrecord_month_end_sub_practice'))
									{
										i_line_no++;
										previous_prac = a_get_mnth_end_effrt_plan[i_mnth_end_plan].getValue('custrecord_month_end_sub_practice');
									}
									
									var f_percent_allocated = a_get_mnth_end_effrt_plan[i_mnth_end_plan].getValue('custrecord_percent_allocated');
									if(!f_percent_allocated)
									{
										f_percent_allocated = 0;
									}
									
									if(s_mnth_year.toLowerCase() != s_month_name.toLowerCase())
										fld_sublist_effort_plan_mnth_end.setLineItemValue(''+s_month_name.toLowerCase(),i_line_no+1,f_percent_allocated);
								}								
							}*/
						}
						else
						{
							var s_nt_apprvd_msg = '<html>';
							s_nt_apprvd_msg += '<p><font size="3" color="red">Please wait system is generating month end effort as per budget plan, you will receive mail once process is completed.</font></p></html>';
							var fld_nt_apprvd_msg = o_form_obj.addField('s_nt_apprvd_msg', 'inlinehtml',null,null);
							fld_nt_apprvd_msg.setDefaultValue(s_nt_apprvd_msg);
						}
						
						nlapiLogExecution('audit','mnth end effrt total lines:- '+i_line_no);
						//fld_mnth_end_effrt_lines.setDefaultValue(i_line_no);
					}
				}
				else if(s_request_mode == 'View' || s_request_mode == 'Submit' || s_request_mode == 'PendingApproval') // Once Revenue share& effort is submitted it should not be editable
				{
					o_form_obj.setScript('customscript_cli_fp_revrec_revenue_vali');
					
					// Add sublist on form to display revenue share
					var fld_form_sublist_revenue_share = o_form_obj.addSubList('practice_sublist', 'list', 'Participating Practice', 'revenue_share_tab');
					fld_form_sublist_revenue_share.addField('parent_practice', 'text', 'Practice').setDisplayType('disabled');
					fld_form_sublist_revenue_share.addField('sub_practice', 'text', 'Sub Practice');
					fld_form_sublist_revenue_share.addField('share_amount', 'currency', 'Amount('+s_proj_currency+')');
					
					// Add sublist on form to display effort plan
					var fld_form_sublist_effort_plan = o_form_obj.addSubList('effort_sublist', 'list', 'Effort Plan', 'effort_plan_tab');
					fld_form_sublist_effort_plan.addField('parent_practice_effort_plan', 'text', 'Practice').setDisplayType('disabled');
					fld_form_sublist_effort_plan.addField('sub_practice_effort_plan', 'text', 'Sub Practice');
					fld_form_sublist_effort_plan.addField('level_effort_plan', 'text', 'Level');
					fld_form_sublist_effort_plan.addField('role_effort_plan', 'text', 'Role');
					fld_form_sublist_effort_plan.addField('no_resources_effort_plan', 'text', 'No. of resources');
					fld_form_sublist_effort_plan.addField('allocation_strt_date_effort_plan', 'date', 'Allocation Start Date(mm/dd/yyyy)');
					fld_form_sublist_effort_plan.addField('allocation_end_date_effort_plan', 'date', 'Allocation End Date(mm/dd/yyyy)');
					fld_form_sublist_effort_plan.addField('prcnt_allocation_effort_plan', 'text', 'Percent Allocated');
					fld_form_sublist_effort_plan.addField('location_effort_plan', 'text', 'Location');
					fld_form_sublist_effort_plan.addField('cost_effort_plan', 'currency', 'Resource Cost(USD)');
					
					var a_JSON = [];
					var a_proj_participating_practice = new Array();
					var a_JSON_effort_plan = [];
					var a_proj_effort_plan = new Array();
					
					var a_revenue_cap_filter = [['custrecord_revenue_share_per_practice_ca.internalid', 'anyof', parseInt(i_existing_revenue_share_rcrd_id)]];
			
					var a_columns_existing_cap_srch = new Array();
					a_columns_existing_cap_srch[0] = new nlobjSearchColumn('custrecord_revenue_share_per_practice_pr');
					a_columns_existing_cap_srch[1] = new nlobjSearchColumn('custrecord_revenue_share_per_practice_su');
					a_columns_existing_cap_srch[2] = new nlobjSearchColumn('custrecord_revenue_share_per_practice_re');
				
					var a_get_logged_in_user_exsiting_revenue_cap = nlapiSearchRecord('customrecord_revenue_share_per_practice', null, a_revenue_cap_filter, a_columns_existing_cap_srch);
					if (a_get_logged_in_user_exsiting_revenue_cap)
					{
						for(var i_revenue_index = 0; i_revenue_index < a_get_logged_in_user_exsiting_revenue_cap.length; i_revenue_index++)
						{
							a_JSON = {
									sub_practice: a_get_logged_in_user_exsiting_revenue_cap[i_revenue_index].getText('custrecord_revenue_share_per_practice_su'),
									parent_practice: a_get_logged_in_user_exsiting_revenue_cap[i_revenue_index].getText('custrecord_revenue_share_per_practice_pr'),
									share_amount: a_get_logged_in_user_exsiting_revenue_cap[i_revenue_index].getValue('custrecord_revenue_share_per_practice_re')
								}
							a_proj_participating_practice.push(a_JSON);
						}
					}
					
					var a_effort_plan_filter = [['custrecord_effort_plan_revenue_cap.internalid', 'anyof', parseInt(i_existing_revenue_share_rcrd_id)]];
			
					var a_columns_existing_effort_plan_srch = new Array();
					a_columns_existing_effort_plan_srch[0] = new nlobjSearchColumn('custrecord_effort_plan_practice');
					a_columns_existing_effort_plan_srch[1] = new nlobjSearchColumn('custrecord_effort_plan_sub_practice');
					a_columns_existing_effort_plan_srch[2] = new nlobjSearchColumn('custrecord_effort_plan_role');
					a_columns_existing_effort_plan_srch[3] = new nlobjSearchColumn('custrecord_effort_plan_level');
					a_columns_existing_effort_plan_srch[4] = new nlobjSearchColumn('custrecord_efort_plan_no_of_resources');
					a_columns_existing_effort_plan_srch[5] = new nlobjSearchColumn('custrecord_effort_plan_allo_strt_date');
					a_columns_existing_effort_plan_srch[6] = new nlobjSearchColumn('custrecord_effort_plan_allo_end_date');
					a_columns_existing_effort_plan_srch[7] = new nlobjSearchColumn('custrecord_effort_plan_percent_allocated');
					a_columns_existing_effort_plan_srch[8] = new nlobjSearchColumn('custrecord_effort_plan_location');
					a_columns_existing_effort_plan_srch[9] = new nlobjSearchColumn('custrecord_cost_for_resource');
				
					var a_get_exsiting_effort_plan = nlapiSearchRecord('customrecord_fp_rev_rec_effort_plan', null, a_effort_plan_filter, a_columns_existing_effort_plan_srch);
					if (a_get_exsiting_effort_plan)
					{
						for(var i_effort_index = 0; i_effort_index < a_get_exsiting_effort_plan.length; i_effort_index++)
						{
							a_JSON_effort_plan = {
									sub_practice_effort_plan: a_get_exsiting_effort_plan[i_effort_index].getText('custrecord_effort_plan_sub_practice'),
									parent_practice_effort_plan: a_get_exsiting_effort_plan[i_effort_index].getText('custrecord_effort_plan_practice'),
									role_effort_plan: a_get_exsiting_effort_plan[i_effort_index].getText('custrecord_effort_plan_role'),
									level_effort_plan: a_get_exsiting_effort_plan[i_effort_index].getText('custrecord_effort_plan_level'),
									no_resources_effort_plan: a_get_exsiting_effort_plan[i_effort_index].getValue('custrecord_efort_plan_no_of_resources'),
									allocation_strt_date_effort_plan: a_get_exsiting_effort_plan[i_effort_index].getValue('custrecord_effort_plan_allo_strt_date'),
									allocation_end_date_effort_plan: a_get_exsiting_effort_plan[i_effort_index].getValue('custrecord_effort_plan_allo_end_date'),
									prcnt_allocation_effort_plan: a_get_exsiting_effort_plan[i_effort_index].getValue('custrecord_effort_plan_percent_allocated'),
									location_effort_plan: a_get_exsiting_effort_plan[i_effort_index].getText('custrecord_effort_plan_location'),
									cost_effort_plan: a_get_exsiting_effort_plan[i_effort_index].getValue('custrecord_cost_for_resource')
								}
							a_proj_effort_plan.push(a_JSON_effort_plan);
						}
					}
						
					fld_form_sublist_revenue_share.setLineItemValues(a_proj_participating_practice);
					fld_form_sublist_effort_plan.setLineItemValues(a_proj_effort_plan);
					
					if (s_request_mode == 'View')
					{
						// add submit button to proceed further
						o_form_obj.addSubmitButton('Submit for Review');
					}
					else if(s_request_mode == 'Submit')
					{
						if(!i_revenue_share_status)
						{
							// add submit button to proceed further
							o_form_obj.addSubmitButton('Submit for Approval');
						}
					}
					else
					{		
						// if logged in user is executing practice head display approve & reject button
						var i_project_executing_practice = nlapiLookupField('job',i_projectId,'custentity_practice');
						var i_executing_practice_head = nlapiLookupField('department',i_project_executing_practice,'custrecord_practicehead');
						
						if(i_executing_practice_head == i_user_logegdIn_id)
						{
							//o_form_obj.setScript('customscript_sut_fp_rev_rec_proj_details');
							var fld_rejection_reason = o_form_obj.addField('rejection_reason', 'text','Rejection Reason');
							o_form_obj.addButton('approve_button', 'Approve', 'custom_Approve(\'  ' +i_existing_revenue_share_rcrd_id+ '  \')');
							o_form_obj.addButton('reject_button', 'Reject', 'custom_Reject(\'  ' +i_existing_revenue_share_rcrd_id+ '  \')');
						}
					}
				}
				else
				{
					fld_existing_revenue_share_status.setDefaultValue('Create');
				}
				
				//display form on suitelet window
		        response.writePage(o_form_obj);
			}
		}
	}
	catch(err)
	{
		nlapiLogExecution('ERROR','ERROR MESSAGE :- ',err);
	}
}

function find_prac_exist(a_current_pract,i_prac_to_find,i_level_to_find,i_location)
{
	var i_prac_present = 0;
	for(var i_prac_find=0; i_prac_find<a_current_pract.length; i_prac_find++)
	{
		if(a_current_pract[i_prac_find].i_sub_practice == i_prac_to_find && a_current_pract[i_prac_find].i_level == i_level_to_find && a_current_pract[i_prac_find].i_location == i_location)
		{
			i_prac_present = 1;
		}
	}
	
	return i_prac_present;
}

function getDatediffIndays(startDate, endDate)
{
    var one_day=1000*60*60*24;
    var fromDate = startDate;
    var toDate=endDate

	//var date1 = nlapiStringToDate(fromDate);

   //var date2 = nlapiStringToDate(toDate);
	
	var date1 = fromDate;
	var date2 = toDate;
	
    var date3=Math.round((date2-date1)/one_day);

    return (date3+1);
}

function generate_table_effort_view(projectWiseRevenue,monthBreakUp,i_year_project,i_project_mnth)
{
	var css_file_url = "";

	var context = nlapiGetContext();
	if (context.getEnvironment() == "SANDBOX") {
		css_file_url = "https://system.sandbox.netsuite.com/core/media/media.nl?id=199133&c=3883006&h=64fa194295cd4ab64d58&mv=iq3kjr6g&_xt=.css&whence=";
	} else if (context.getEnvironment() == "PRODUCTION") {
		css_file_url = "https://system.na1.netsuite.com/core/media/media.nl?id=199133&c=3883006&h=64fa194295cd4ab64d58&mv=iq3kjr6g&_xt=.css&whence=";
	}

	var html = "";
	html += "<link href='" + css_file_url + "'  rel='stylesheet'>";

	html += '<p><font size="2" color="red">Recent Effort Plan</font></p>';
	html += "<table class='projection-table'>";

	// header
	html += "<tr class='header-row'>";
	
	html += "<td>";
	html += "Practice";
	html += "</td>";
	
	html += "<td>";
	html += "Sub Practice";
	html += "</td>";

	html += "<td>";
	html += "Role";
	html += "</td>";
	
	var today = new Date();
	var currentMonthName = getMonthName(nlapiDateToString(today));
	
	for (var j = 0; j < monthBreakUp.length; j++)
	{
		var months = monthBreakUp[j];
		var s_month_name = getMonthName(months.Start); // get month name
		var s_year = nlapiStringToDate(months.Start)
		s_year = s_year.getFullYear();
		s_month_name = s_month_name+'_'+s_year;
				
		html += "<td>";
		html += s_month_name;
		html += "</td>";
	}
		
	html += "</tr>";
	
	var a_sub_prac_already_displayed = new Array();
	var i_subpractice_id = 0;
	for ( var emp_internal_id in projectWiseRevenue) {
		
		for ( var project_internal_id in projectWiseRevenue[emp_internal_id].RevenueData) {
			
			
			html += "<tr>";
			html += "<td class='label-name'>";
			html += projectWiseRevenue[emp_internal_id].practice_name;
			html += "</td>";
				
			html += "<td class='label-name'>";
			html += projectWiseRevenue[emp_internal_id].sub_prac_name;
			html += "</td>";
		
			html += "<td class='label-name'>";
			html += projectWiseRevenue[emp_internal_id].role_name;
			html += "</td>";
				
			var i_sub_practice_internal_id = 0;
			for (var month in projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id])
			{
				var a_MonthNames = MonthNames(i_year_project);
				var currentMonthPos = a_MonthNames.indexOf(month);
				
				if(currentMonthPos >= 0)
				{
					var s_current_month = a_MonthNames[currentMonthPos];
					var s_current_mnth_yr = s_current_month.substr(4, 8);
				}
				
				if (i_project_mnth <= currentMonthPos && parseInt(s_current_mnth_yr) >= i_year_project) {
					
					html += "<td class='monthly-amount'>";
					html += parseFloat(projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id][month].prcnt_allocated).toFixed(2);
					html += "</td>";
				}
			}
			html += "</tr>";			
		}
	}

	html += "</table>";
	
	return html;
}

function getMonthsBreakup(d_startDate, d_endDate) {
	
	try
	{
		var dateBreakUp = [];
		var new_start_date = new Date(d_startDate.getFullYear(), d_startDate
		        .getMonth(), 1);
				
		var endDate = nlapiAddDays(nlapiAddMonths(d_startDate, 1), -1);
		endDate = nlapiDateToString(endDate, 'date');
	
		dateBreakUp.push({
		    Start : nlapiDateToString(new_start_date, 'date'),
		    End : endDate
		});
	
		for (var i = 1;; i++) {
			var new_date = nlapiAddMonths(new_start_date, i);
	
			if (new_date > d_endDate) {
				break;
			}
			
			endDate = nlapiAddDays(nlapiAddMonths(new_date, 1), -1);
			endDate = nlapiDateToString(endDate, 'date');
					
			dateBreakUp.push({
			    Start : nlapiDateToString(new_date, 'date'),
			    End : endDate
			});
		}
	}
	catch(err)
	{
		nlapiLogExecution('ERROR','getMonthsBreakup','ERROR MESSAGE :- '+err);
	}

	return dateBreakUp;
}

function getMonthName(currentDate) {
	currentDate = nlapiStringToDate(currentDate)
	var monthNames = [ "JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JUL", "AUG",
	        "SEP", "OCT", "NOV", "DEC" ];
	return monthNames[currentDate.getMonth()];
}

function MonthNames(yr) {
	var yr1 = Number(yr) + 1;
	
	var monthNames = [ "JAN_"+yr, "FEB_"+yr, "MAR_"+yr, "APR_"+yr, "MAY_"+yr, "JUN_"+yr, "JUL_"+yr, "AUG_"+yr,
	        "SEP_"+yr, "OCT_"+yr, "NOV_"+yr, "DEC_"+yr ];
	var month_nxt_yr = ["JAN_"+yr1, "FEB_"+yr1, "MAR_"+yr1, "APR_"+yr1, "MAY_"+yr1, "JUN_"+yr1, "JUL_"+yr1, "AUG_"+yr1,
	        "SEP_"+yr1, "OCT_"+yr1, "NOV_"+yr1, "DEC_"+yr1 ];
	monthNames = monthNames.concat(month_nxt_yr);
	
	return monthNames;
}

function yearData(yr) {
	var yr1 = Number(yr) + 1;
	
	var monthNames = [ "JAN_"+yr, "FEB_"+yr, "MAR_"+yr, "APR_"+yr, "MAY_"+yr, "JUN_"+yr, "JUL_"+yr, "AUG_"+yr,
	        "SEP_"+yr, "OCT_"+yr, "NOV_"+yr, "DEC_"+yr ];
	var month_nxt_yr = ["JAN_"+yr1, "FEB_"+yr1, "MAR_"+yr1, "APR_"+yr1, "MAY_"+yr1, "JUN_"+yr1, "JUL_"+yr1, "AUG_"+yr1,
	        "SEP_"+yr1, "OCT_"+yr1, "NOV_"+yr1, "DEC_"+yr1 ];
	monthNames = monthNames.concat(month_nxt_yr);
	
	for (var i = 0; i < monthNames.length; i++) {
		this[monthNames[i]] = {
		    Amount : 0,
			prcnt_allocated : 0,
			total_revenue : 0
		};
	}
}
	
function searchForPractice(project)
{
	try
	{
		var a_JSON = {};
		var a_dataRows = [];
		var a_filters_project = new Array();
		a_filters_project.push(new nlobjSearchFilter('project',null,'anyof',project));
		
		var a_column_get_practice = new Array();
		a_column_get_practice.push(new nlobjSearchColumn('custevent_practice',null,'group'));
		a_column_get_practice.push(new nlobjSearchColumn('custrecord_parent_practice','custevent_practice','group'));
		
		var a_search_allocation = nlapiSearchRecord('resourceallocation',null,a_filters_project,a_column_get_practice);
		if(a_search_allocation){
			for(var i=0;i<a_search_allocation.length;i++){
				
				a_JSON = {
						sub_practice: a_search_allocation[i].getValue('custevent_practice',null,'group'),
						parent_practice: a_search_allocation[i].getValue('custrecord_parent_practice','custevent_practice','group')
				}
				a_dataRows.push(a_JSON);
			}
		}
		return a_dataRows;
	}
	catch(err)
	{
		nlapiLogExecution('ERROR','searchForPractice :- ',err);
	}

}

function findAllocationPracticePresent(a_proj_prac_details, subpractice, level, subsi, i_resource_location)
{
	for(var i_index_arr_srchng=0; i_index_arr_srchng<a_proj_prac_details.length; i_index_arr_srchng++)
	{
		//if(a_proj_prac_details[i_srch_arr].i_practice == practice)
		{
			//nlapiLogExecution('audit','prac:- '+practice);
			if(parseFloat(a_proj_prac_details[i_index_arr_srchng].i_sub_practice) == parseFloat(subpractice))
			{
				if(subpractice == 324)
				{
					nlapiLogExecution('audit','sub prac:- '+subpractice);
					nlapiLogExecution('audit','level before match:- '+level);
				}
				
				if(parseFloat(a_proj_prac_details[i_index_arr_srchng].s_emp_level) == parseFloat(level))
				{
					//nlapiLogExecution('audit','level:- '+level);
					if(parseFloat(a_proj_prac_details[i_index_arr_srchng].subsidiary) == parseFloat(subsi))
					{
						//nlapiLogExecution('audit','subsi:- '+subsi);
						if(parseFloat(a_proj_prac_details[i_index_arr_srchng].i_resource_location) == parseFloat(i_resource_location))
						{
							//nlapiLogExecution('audit','loc:- '+i_resource_location);
							return i_index_arr_srchng;
						}
					}
				}
			}
		}
	}
	
	return -1;
}

function getMonthName_FromMonthNumber(month_number)
{
	var a_month_name = [ "JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JUL", "AUG",
	        "SEP", "OCT", "NOV", "DEC" ];
								
	return a_month_name[month_number];
}
function exportToExcel1() {
	try {
	/*	var startdate = nlapiGetFieldValue('custpage_filter_start_date');
		var enddate = nlapiGetFieldValue('custpage_filter_end_date');
		var customer = nlapiGetFieldValue('custpage_filter_customer');
		var project = nlapiGetFieldValue('custpage_filter_project');
		var status = nlapiGetFieldValue('custpage_filter_status');*/

		var csvExportUrl = nlapiResolveURL('SUITELET',
		        'customscript_sut_fp_rev_rec_proj_details',
		        'customdeploy_sut_fp_rev_rec_proj_details')
		        + "&s_request_mode=excel";
				//&project="
		      //  + project;
		       /* + "&customer="
		        + customer
		        + "&startdate="
		        + startdate
		        + "&enddate="
		        + enddate
		        + "&status=" + status;*/

		var win = window.open(csvExportUrl);
		win.focus();
	} catch (err) {
		nlapiLogExecution('ERROR', 'exportToExcel1', err);
		alert(err.message);
	}
}