/* **************************************************************************************  

 *                            
 ** All Rights Reserved.                                                    
 **                                                                        
 ** This software is the confidential and proprietary information of          
 ** Olethia Labs Pvt. Ltd. ("Confidential Information"). You shall not              
 ** disclose such Confidential Information and shall use it only in          
 ** accordance with the terms of the license agreement you entered into    
 ** with .                      

 ************************************************************************************** */
function postRESTlet(request)
{

	var ret_arr = new Array();
	var St_Dttime =request.startDateTime;
	nlapiLogExecution('ERROR','St_Dttime',St_Dttime);
//	var endDate = GetEndDate(St_Dttime);
	var currentDate = sysDate(); // returns the date
	var currentTime = timestamp(); // returns the time stamp in HH:MM:SS
	var currentDateAndTime = currentDate + ' ' + currentTime;
	nlapiLogExecution('ERROR','currentDateAndTime',currentDateAndTime);
	var filters = new Array();
	var srchCST= nlapiLoadSearch(null,'customsearch3212');	
	if(St_Dttime!="")
	{
		filters.push(new nlobjSearchFilter("date","systemnotes","within",St_Dttime,currentDateAndTime));
		srchCST.addFilters(filters);
	}
	var resultSet = srchCST.runSearch();
	var totalRecords = 0;

	var searchResultCount = 0;
	var resultSlice = null;
	var searchResult = [];

	do {
		resultSlice = resultSet.getResults(searchResultCount,searchResultCount + 1000);
		if (resultSlice)
		{
			resultSlice.forEach(function(result) {

				searchResult.push(result);
				searchResultCount++;
			});

		}

	} while (resultSlice && resultSlice.length >= 1000);
//	nlapiLogExecution('ERROR','searchResult.length',searchResult.length);
	var returnData = [];
	if(searchResult){
		for(var t = 0;t<searchResult.length;t++)
		{
			var resultCols=searchResult[t].getAllColumns();
			//	var columns= searchResult[t];
			returnData.push({
				"internalId":searchResult[t].getValue(resultCols[0]),
				"EmployeeInternalId":searchResult[t].getValue(resultCols[1]),
				"ProjectInternalId":searchResult[t].getValue(resultCols[2]),
				"PmInternalId":searchResult[t].getValue(resultCols[3]),
				"accountInternalId":searchResult[t].getValue(resultCols[4]),
				"employeeLocation":searchResult[t].getValue(resultCols[5]),
				"employeeAllocationStartDate":searchResult[t].getValue(resultCols[6]),
				"employeeAllocationEndDate":searchResult[t].getValue(resultCols[7]),
				"isBillable":searchResult[t].getValue(resultCols[8]),
				"isBench":searchResult[t].getValue(resultCols[9]),
				"type":searchResult[t].getValue(resultCols[11])
			});
		}
	}
	return {"timestamp":currentDateAndTime,"records":returnData};
//	nlapiLogExecution('debug','returnData',JSON.stringify(returnData));
}
function GetEndDate(St_Dttime)
{
	var date = new Date();
	nlapiLogExecution('ERROR','date',date);
}
function sysDate() {
	var date = new Date();
	var tdate = date.getDate();
	var month = date.getMonth() + 1; // jan = 0
	var year = date.getFullYear();
	return currentDate = month + '/' + tdate + '/' + year;
}
function timestamp() {
	var str = "";

	var currentTime = new Date();
	var hours = currentTime.getHours();
	var minutes = currentTime.getMinutes();
	var seconds = currentTime.getSeconds();
	var meridian = "";
	if (hours > 12) {
		meridian += "pm";
	} else {
		meridian += "am";
	}
	if (hours > 12) {

		hours = hours - 12;
	}
	if (minutes < 10) {
		minutes = "0" + minutes;
	}
	if (seconds < 10) {
		seconds = "0" + seconds;
	}
	str += hours + ":" + minutes +" ";//":" + seconds + " ";

	return str + meridian;
}