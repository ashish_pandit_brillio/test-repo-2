/**
 * @author Shweta
 */
// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
/*
   	Script Name : CLI_Salary_Upload_Validations.js
	Author      : Shweta Chopde
	Date        : 29 July 2014
    Description : Validations on Salary Upload Process Records


	Script Modification Log:

	-- Date --			-- Modified By --				--Requested By--				-- Description --




	Below is a summary of the process controls enforced by this script file.  The control logic is described
	more fully, below, in the appropriate function headers and code blocks.

     PAGE INIT
		- pageInit(type)


     SAVE RECORD
		- saveRecord()


     VALIDATE FIELD
		- validateField(type, name, linenum)


     FIELD CHANGED
		- fieldChanged(type, name, linenum)


     POST SOURCING
		- postSourcing(type, name)


	LINE INIT
		- lineInit(type)


     VALIDATE LINE
		- validateLine()


     RECALC
		- reCalc()


     SUB-FUNCTIONS
		- The following sub-functions are called by the above core functions in order to maintain code
            modularization:





*/
}
// END SCRIPT DESCRIPTION BLOCK  ====================================



// BEGIN SCRIPT UPDATION BLOCK  ====================================
/*


*/
// END SCRIPT UPDATION BLOCK  ====================================




// BEGIN GLOBAL VARIABLE BLOCK  =====================================
{

	//  Initialize any Global Variables, in particular, debugging variables...


}
// END GLOBAL VARIABLE BLOCK  =======================================





// BEGIN PAGE INIT ==================================================

function pageInit_su_validations(type)
{
	
	var button = window.document.getElementById('tbl__cancel')
	button.style.visibility = "hidden";
	
    var button_1 = window.document.getElementById('custrecord_csv_file_s_p_popup_list')
	button_1.style.visibility = "hidden";
		
	var button_2 = window.document.getElementById('custrecord_csv_file_s_p_popup_link')
	button_2.style.visibility = "hidden";
	
	nlapiDisableField('custrecord_file_status',true)

    if(type == 'create')
	{
	 var i_file_status = nlapiGetFieldValue('custrecord_file_status')
    
    if(i_file_status == '' || i_file_status == undefined || i_file_status == null)
	{
		nlapiSetFieldValue('custrecord_file_status',4)
	}
		
	}//Create
	
}//Page Init V

// END PAGE INIT ====================================================





// BEGIN SAVE RECORD ================================================

function saveRecord_salary_upload() //
{
	//	if(_logValidation(i_recordID))
	{
		//  var o_recordOBJ = nlapiLoadRecord('customrecord_salary_upload_process',i_recordID)	
		
		//  if(_logValidation(o_recordOBJ))
		{
		
			var i_monthly_employee = nlapiGetFieldValue('custrecord_monthly_employee');
			
			var i_hourly_employee = nlapiGetFieldValue('custrecord_hourly_employee');
			
			// Code added to handle OT and Diff case
			var i_Salaried_OT = nlapiGetFieldValue('custrecord_sal_emp_ot_hrs');
			
			var i_Salaried_OT_Diff = nlapiGetFieldValue('custrecord_salaried_ot_diff');
			
			var i_Hourly_Diff = nlapiGetFieldValue('custrecord_hourly_emp_diff_hrs');
			
			var i_hourly_FP = nlapiGetFieldValue('custrecord_hourly_employee_fp_time');
		
			var i_hourly_FP_DIFF = nlapiGetFieldValue('custrecord_hourly_employee_fp_diff');
			
			var i_hourly_FP_Internal = nlapiGetFieldValue('custrecord_hourly_employee_fp_internal');
		
			var i_hourly_FP_Internal_Diff = nlapiGetFieldValue('custrecordhourly_employee_fp_internal_di');
			// End of Code added to handle OT and Diff case
			/*
			 alert(' Monthly -->'+i_monthly_employee)
			 alert(' Hourly -->'+i_hourly_employee)
			 
			 */
			if (i_monthly_employee == 'F' && i_hourly_employee == 'F' && i_Salaried_OT == 'F' && i_Salaried_OT_Diff == 'F' && i_Hourly_Diff == 'F' && i_hourly_FP == 'F' && i_hourly_FP_DIFF == 'F' && i_hourly_FP_Internal == 'F' && i_hourly_FP_Internal_Diff == 'F') //
			{
				//alert(' Please enter for Hourly / Salaried / Salaried OT / Salaried OT Diff / Hourly OT Diff Employee .')
				alert(' Please check any one process type check box on the form.');
				return false;
			}
			
		}//Record OBJ
	}//Record ID
	
	return true;
}

// END SAVE RECORD ==================================================





// BEGIN VALIDATE FIELD =============================================

function validateField(type, name, linenum)
{

	/*  On validate field:

          - EXPLAIN THE PURPOSE OF THIS FUNCTION


          FIELDS USED:

          --Field Name--				--ID--


    */


	//  LOCAL VARIABLES



	//  VALIDATE FIELD CODE BODY


	return true;

}

// END VALIDATE FIELD ===============================================





// BEGIN FIELD CHANGED ==============================================

function fieldChanged_salary_upload(type, name, linenum) //
{
	nlapiDisableField('custrecord_file_status', true)
	
	if (name == 'custrecord_monthly_employee' || name == 'custrecord_hourly_employee' || name == 'custrecord_sal_emp_ot_hrs' || name == 'custrecord_salaried_ot_diff' || name == 'custrecord_hourly_emp_diff_hrs' || name == 'custrecord_hourly_employee_fp_time' || name == 'custrecord_hourly_employee_fp_diff'  || name == 'custrecord_hourly_employee_fp_internal'  || name == 'custrecordhourly_employee_fp_internal_di') //
	{
		var i_monthly_employee = nlapiGetFieldValue('custrecord_monthly_employee');
		
		var i_hourly_employee = nlapiGetFieldValue('custrecord_hourly_employee');
		
		var i_Salaried_OT = nlapiGetFieldValue('custrecord_sal_emp_ot_hrs');
		
		var i_Salaried_OT_DIFF = nlapiGetFieldValue('custrecord_salaried_ot_diff');
		
		var i_hourly_DIFF = nlapiGetFieldValue('custrecord_hourly_emp_diff_hrs');
		
		var i_hourly_FP = nlapiGetFieldValue('custrecord_hourly_employee_fp_time');
		
		var i_hourly_FP_DIFF = nlapiGetFieldValue('custrecord_hourly_employee_fp_diff');
		
		var i_hourly_FP_Internal = nlapiGetFieldValue('custrecord_hourly_employee_fp_internal');
		
		var i_hourly_FP_Internal_Diff = nlapiGetFieldValue('custrecordhourly_employee_fp_internal_di');
		
		var a_Validation_Array = new Array();
		
		a_Validation_Array[0] = i_monthly_employee;
		a_Validation_Array[1] = i_hourly_employee;
		a_Validation_Array[2] = i_Salaried_OT;
		a_Validation_Array[3] = i_Salaried_OT_DIFF;
		a_Validation_Array[4] = i_hourly_DIFF;
		a_Validation_Array[5] = i_hourly_FP;
		a_Validation_Array[6] = i_hourly_FP_DIFF;		
		a_Validation_Array[7] = i_hourly_FP_Internal;
		a_Validation_Array[8] = i_hourly_FP_Internal_Diff;
		
		var i_counter = 0;
		
		for (var counter_I = 0; counter_I < a_Validation_Array.length; counter_I++) //
		{
			if (a_Validation_Array[counter_I] == 'T') //
			{
				i_counter++;
			}
		}
		
		if (i_counter > 1) //
		{
			alert('Please select one option only from given check boxes.');
			nlapiSetFieldValue(name, 'F');
			return false;
		}
	}
	
	if (name == 'custrecord_monthly_employee') //
	{
		if (i_hourly_employee == 'T') //
		{
			if (i_monthly_employee == 'T' || i_hourly_employee == 'T') //
			{
				//alert(' Please select either Salaried / Hourly Employee .')
				//nlapiSetFieldValue('custrecord_monthly_employee', 'F')
				//return false;
			}
		}
		
	}//Monthly 
	if (name == 'custrecord_hourly_employee') //
	{
		//var i_monthly_employee = nlapiGetFieldValue('custrecord_monthly_employee')
		
		//var i_hourly_employee = nlapiGetFieldValue('custrecord_hourly_employee')
		
		if (i_monthly_employee == 'T') //
		{
			if (i_hourly_employee == 'T') //
			{
				//alert(' Please select either Salaried / Hourly Employee .')
				//nlapiSetFieldValue('custrecord_hourly_employee', 'F')
				//return false;
			}
		}
		
	}//Hourly 
}//Field Change S U

// END FIELD CHANGED ================================================





// BEGIN POST SOURCING ==============================================

function postSourcing(type, name)
{

    /*  On post sourcing:

	- PURPOSE

          FIELDS USED:

		--Field Name--			--ID--		--Line Item Name--


    */

    //  LOCAL VARIABLES


    //  POST SOURCING CODE BODY



}

// END POST SOURCING ================================================



// BEGIN LINE INIT ==============================================

function lineInit(type)
{

    /*  On Line Init:

	- PURPOSE

          FIELDS USED:

		--Field Name--			--ID--		--Line Item Name--


    */

    //  LOCAL VARIABLES


    //  LINE INIT CODE BODY



}

// END LINE INIT ================================================


// BEGIN VALIDATE LINE ==============================================

function validateLine(type)
{

	/*  On validate line:

          - EXPLAIN THE PURPOSE OF THIS FUNCTION


          FIELDS USED:

          --Field Name--				--ID--


    */


	//  LOCAL VARIABLES


	//  VALIDATE LINE CODE BODY


	return true;

}

// END VALIDATE LINE ================================================




// BEGIN RECALC =====================================================

function recalc(type)
{

	/*  On recalc:

          - EXPLAIN THE PURPOSE OF THIS FUNCTION


          FIELDS USED:

          --Field Name--				--ID--


    */


	//  LOCAL VARIABLES


	//  RECALC CODE BODY


}

// END RECALC =======================================================





// BEGIN FUNCTION ===================================================

function validate_process() //
{
	var i_recordID = nlapiGetRecordId();
	
	if (_logValidation(i_recordID)) //
	{
		var o_recordOBJ = nlapiLoadRecord('customrecord_salary_upload_process', i_recordID)
		
		if (_logValidation(o_recordOBJ)) //
		{
			var i_CSV_File_ID = o_recordOBJ.getFieldValue('custrecord_csv_file_s_p');
			
			// var i_CSV_File_Name = o_recordOBJ.getFieldText('custrecord_csv_file_s_p');
			
			var i_monthly_employee = o_recordOBJ.getFieldValue('custrecord_monthly_employee');
			
			var i_hourly_employee = o_recordOBJ.getFieldValue('custrecord_hourly_employee');
			
			// *********  Code added by Vikrant for new 3 cases
			var i_Salaried_OT = o_recordOBJ.getFieldValue('custrecord_sal_emp_ot_hrs');
			
			var i_Salaried_OT_Diff = o_recordOBJ.getFieldValue('custrecord_salaried_ot_diff');
			
			var i_Hourly_Diff = o_recordOBJ.getFieldValue('custrecord_hourly_emp_diff_hrs');
			
			var i_hourly_FP = o_recordOBJ.getFieldValue('custrecord_hourly_employee_fp_time');
			
			var i_hourly_FP_DIFF = o_recordOBJ.getFieldValue('custrecord_hourly_employee_fp_diff');
			//  ********** End of Code added by Vikrant for new 3 cases
			
			var i_account_debit = o_recordOBJ.getFieldValue('custrecord_account_debit_s_p');
			
			var i_account_credit = o_recordOBJ.getFieldValue('custrecord_account_credit_s_p');
			
			
			var i_hourly_FP_Internal = o_recordOBJ.getFieldValue('custrecord_hourly_employee_fp_internal');
			
			var i_hourly_FP_Internal_Diff = o_recordOBJ.getFieldValue('custrecordhourly_employee_fp_internal_di');
			
			
			
			/*
			 alert('URL : ' + '/app/site/hosting/scriptlet.nl?script=241&deploy=1&custscript_csv_file_su=' + i_CSV_File_ID +
			'&custscript_monthly_employee_su=' +
			i_monthly_employee +
			'&custscript_hourly_employee_su=' +
			i_hourly_employee +
			'&custscript_account_debit_su=' +
			i_account_debit +
			'&custscript_account_credit_su=' +
			i_account_credit +
			'&custscript_record_id_su=' +
			i_recordID +
			'&custscript_sal_emp_ot_hrs=' +
			i_Salaried_OT +
			'&custscript_salaried_ot_diff=' +
			i_Salaried_OT_Diff +
			'&custscript_hourly_emp_diff_hrs=' +
			i_Hourly_Diff +
			'&custscript_hourly_emp_fp=' +
			i_hourly_FP +
			'&custscript_hourly_emp_fp_diff=' +
			i_hourly_FP_DIFF +
			'&custscript_hourly_employee_internal' +
			i_hourly_FP_Internal +
			'&custscripthourly_employee_internal_diff' +
			i_hourly_FP_Internal_Diff);
			*/
			
			
			window.open('/app/site/hosting/scriptlet.nl?script=241&deploy=1&custscript_csv_file_su=' + i_CSV_File_ID +
			'&custscript_monthly_employee_su=' +
			i_monthly_employee +
			'&custscript_hourly_employee_su=' +
			i_hourly_employee +
			'&custscript_account_debit_su=' +
			i_account_debit +
			'&custscript_account_credit_su=' +
			i_account_credit +
			'&custscript_record_id_su=' +
			i_recordID +
			'&custscript_sal_emp_ot_hrs=' +
			i_Salaried_OT +
			'&custscript_salaried_ot_diff=' +
			i_Salaried_OT_Diff +
			'&custscript_hourly_emp_diff_hrs=' +
			i_Hourly_Diff +
			'&custscript_hourly_emp_fp=' +
			i_hourly_FP +
			'&custscript_hourly_emp_fp_diff=' +
			i_hourly_FP_DIFF +
			'&custscript_hourly_employee_internal=' +
			i_hourly_FP_Internal +
			'&custscripthourly_employee_internal_diff=' +
			i_hourly_FP_Internal_Diff, '_self', null);
			
			//	window.open('/app/common/scripting/scriptstatus.nl?scripttype=242')
		
		}//Record OBJ
	}//Record ID
}//Validate/Process


function delete_JE() //
{
	var s_confirm = confirm("Do you really want to DELETE this record !?");
	
	if (s_confirm == false) // If user cancels to delete the record... terminate the script.
	{
		return false;
	}
	
	var i_recordID = nlapiGetRecordId();
	
	if (_logValidation(i_recordID)) //
	{
		var o_recordOBJ = nlapiLoadRecord('customrecord_salary_upload_process', i_recordID)
		
		if (_logValidation(o_recordOBJ)) //
		{
			var a_JE_array = o_recordOBJ.getFieldValues('custrecord_journal_entries_created');
			
			window.open('/app/site/hosting/scriptlet.nl?script=243&deploy=1&custscript_je_array_sut=' + a_JE_array + '&custscript_record_id_delete_je=' + i_recordID, '_self', null)
			
			//	window.open('/app/common/scripting/scriptstatus.nl?scripttype=246')
		}//Record OBJ
	}//Record ID
}//Delete JE

function update_JE()
{
	var i_recordID = nlapiGetRecordId();
	
	if(_logValidation(i_recordID))
	{
	  var o_recordOBJ = nlapiLoadRecord('customrecord_salary_upload_process',i_recordID)	
	  
	  if(_logValidation(o_recordOBJ))
	  {
		 var i_CSV_File_ID = o_recordOBJ.getFieldValue('custrecord_csv_file_s_p');
		 
		// var i_CSV_File_Name = o_recordOBJ.getFieldText('custrecord_csv_file_s_p');
						
		 var i_monthly_employee = o_recordOBJ.getFieldValue('custrecord_monthly_employee');
		
		 var i_hourly_employee = o_recordOBJ.getFieldValue('custrecord_hourly_employee');
		
		 var i_account_debit = o_recordOBJ.getFieldValue('custrecord_account_debit_s_p');
		
		 var i_account_credit = o_recordOBJ.getFieldValue('custrecord_account_credit_s_p');	
		 
		 var a_JE_array = o_recordOBJ.getFieldValues('custrecord_journal_entries_created');
		
				
		window.open('/app/site/hosting/scriptlet.nl?script=257&deploy=1&custscript_csv_file_id_update_sut='+i_CSV_File_ID+'&custscript_monthly_employee_update_sut='+i_monthly_employee+'&custscript_hourly_employee_update_sut='+i_hourly_employee+'&custscript_account_debit_update_sut='+i_account_debit+'&custscript_account_credit_update_sut='+i_account_credit+'&custscript_record_id_update_sut='+i_recordID+'&custscript_je_array_update_sut='+a_JE_array,'_self',null)
  		
	//	window.open('/app/common/scripting/scriptstatus.nl?scripttype=256')
		
	  }//Record OBJ
	
	}//Record ID
	
}//Update JE

function refresh_salary_upload()
{
	location.reload();	
}//Refresh Salary Upload

function create_journal_entry() //
{
	var i_recordID = nlapiGetRecordId();
	
	if (_logValidation(i_recordID)) //
	{
		var o_recordOBJ = nlapiLoadRecord('customrecord_salary_upload_process', i_recordID)
		
		if (_logValidation(o_recordOBJ)) //
		{
			var i_CSV_File_ID = o_recordOBJ.getFieldValue('custrecord_csv_file_s_p');
			
			var i_monthly_employee = o_recordOBJ.getFieldValue('custrecord_monthly_employee');
			
			var i_hourly_employee = o_recordOBJ.getFieldValue('custrecord_hourly_employee');
			
			var i_account_debit = o_recordOBJ.getFieldValue('custrecord_account_debit_s_p');
			
			var i_account_credit = o_recordOBJ.getFieldValue('custrecord_account_credit_s_p');
			
			// Added by Vikrant to handle OT and diff caes
			
			var i_Salaried_OT = o_recordOBJ.getFieldValue('custrecord_sal_emp_ot_hrs');
			var i_Salaried_OT_Diff = o_recordOBJ.getFieldValue('custrecord_salaried_ot_diff');
			var i_Hourly_Diff = o_recordOBJ.getFieldValue('custrecord_hourly_emp_diff_hrs');
			
			
			var i_hourly_FP = o_recordOBJ.getFieldValue('custrecord_hourly_employee_fp_time');
			var i_hourly_FP_DIFF = o_recordOBJ.getFieldValue('custrecord_hourly_employee_fp_diff');
			
			var i_hourly_FP_Internal = o_recordOBJ.getFieldValue('custrecord_hourly_employee_fp_internal');
			var i_hourly_FP_Internal_Diff = o_recordOBJ.getFieldValue('custrecordhourly_employee_fp_internal_di');
			
			// End of Added by Vikrant to handle OT and diff caes
			
			window.open('/app/site/hosting/scriptlet.nl?script=242&deploy=1&custscript_csv_file_s_je=' + i_CSV_File_ID +
			'&custscript_monthly_employee_s_je=' +
			i_monthly_employee +
			'&custscript_hourly_employee_s_je=' +
			i_hourly_employee +
			'&custscript_account_debit_s_je=' +
			i_account_debit +
			'&custscript_account_credit_s_je=' +
			i_account_credit +
			'&custscript_record_id_s_je=' +
			i_recordID +
			'&custrecord_sal_emp_ot_hrs=' +
			i_Salaried_OT +
			'&custrecord_salaried_ot_diff=' +
			i_Salaried_OT_Diff +
			'&custrecord_hourly_emp_diff_hrs=' +
			i_Hourly_Diff +
			'&custrecord_hourly_emp_fp_sch_1=' +
			i_hourly_FP +
			'&custrecord_hourly_emp_fp_diff_sch_1=' +
			i_hourly_FP_DIFF +
			'&custrecord_hourly_emp_internal=' +
			i_hourly_FP_Internal +
			'&custrecord_hourly_emp_internal_diff=' +
			i_hourly_FP_Internal_Diff, '_self', null)//243
			//	window.open('/app/common/scripting/scriptstatus.nl?scripttype=244')
		
		}//Record OBJ
	}//Record ID
}


/**
 * 
 * @param {Object} value
 * 
 * Description --> If the value is blank /null/undefined returns false else returns true
 */
function _logValidation(value) 
{
 if(value.toString()!='null' && value != null && value.toString() != null && value != '' && value != undefined && value.toString() != undefined && value != 'undefined' && value.toString() != 'undefined'&& value.toString() != 'NaN' && value != NaN) 
 {
  return true;
 }
 else 
 { 
  return false;
 }
}
// END FUNCTION =====================================================
