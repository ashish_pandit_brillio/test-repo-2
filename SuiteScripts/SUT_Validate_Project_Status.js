/*
Author: Deepak MS 5/9/2017 
Description: Validate Project Status
*/

function validateProject(request,response)
{
	try
	{
		var flag = true;
		var i_project = request.getParameter('i_project_interal_id');
		var project_lookUp = nlapiLookupField('job',parseInt(i_project),['entitystatus']);
		var project_status = project_lookUp.entitystatus;
		nlapiLogExecution('ERROR','project_status',project_status);
		if(parseInt(project_status) != 2 && parseInt(project_status) != 4 ){
			flag = false;

		}
		else{
			flag = true;
		}
		nlapiLogExecution('ERROR','flag',flag);
		response.write(flag);
	}
	catch(err)
	{
		nlapiLogExecution('ERROR','validatePractice','ERROR MESSAGE :- '+err);
	}
	}