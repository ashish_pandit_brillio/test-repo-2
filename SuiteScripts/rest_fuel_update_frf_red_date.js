//BEGIN SCRIPT DESCRIPTION BLOCK  ==================================

/*
		Script Name:	rest_fuel_update_frf_red_date.js
		Author: 		Prabhat Gupta
		Date:           04 feb 2021
		Description:	JSONObj.


		Script Modification Log:

		-- Date --		-- Modified By --			       --Requested By--				-- Description --

}*/
//END SCRIPT DESCRIPTION BLOCK  ====================================

/**
 * @NApiVersion 2.0
 * @NScriptType Restlet
 * @NModuleScope SameAccount
 */
 define(['N/record', 'N/search', 'N/format'], function (record, search, format) {
	function frfDetailsRedData(datain) {
		var frfData = datain;
		log.debug('date', JSON.stringify(frfData));
		if (frfData) {


			var loadSearch = getFrfDetailsUpdate(frfData);
			log.debug('loadSearch', loadSearch);
			return {
				"status": "success",
				"data": loadSearch
			}
		}
		else {
			return {
				"status": "error",
				"message": "Please provide FRF Details Array for RED."
			}
		}
	}
	function getFrfDetailsUpdate(frfDetailsData) {
		var data = [];
		log.debug({
			title: "frfDetailsData.length",
			details: frfDetailsData.length
		})
		for (var i = 0; i < frfDetailsData.length; i++) {
			var mode = frfDetailsData[i].mode;
			var rec_id = frfDetailsData[i].frfInternalId;
			//mode ecdUpdate 
			//mode bulkUpdate
			if (mode == 'ecdUpdate') {
				var redDate = frfDetailsData[i].redDate;
				var frfStartDate = frfDetailsData[i].startDate;
				if (_logValidation(rec_id)) {
					var record_id = record.submitFields({
						type: 'customrecord_frf_details',
						id: rec_id,
						values: {
							'custrecord_frf_details_red_date': redDate,
							'custrecord_frf_details_start_date': frfStartDate,
						}
					});
					data.push(record_id);
				}
			}
			else if (mode == 'bulkUpdate') {
				var frfEndDate = frfDetailsData[i].endDate;
				var frfStartDate = frfDetailsData[i].startDate;
				var projectInternalId = frfDetailsData[i].projectInternalId;
				var opportunityInternalId = frfDetailsData[i].opportunityInternalId;
				var accountInternalId = frfDetailsData[i].accountInternalId;
				var desiredStartDate = frfDetailsData[i].desiredStartDate;
                var loggedInUser = frfDetailsData[i].loggedInUser;
				if (_logValidation(rec_id)) {
					var record_id = record.submitFields({
						type: 'customrecord_frf_details',
						id: rec_id,
						values: {
							'custrecord_frf_details_end_date': frfEndDate,
							'custrecord_frf_details_start_date': frfStartDate,
							'custrecord_frf_details_project':projectInternalId,
							'custrecord_frf_details_opp_id':opportunityInternalId,
							'custrecord_frf_details_account':accountInternalId,
							'custrecord_frf_desired_start_date':desiredStartDate,
                            'custrecord_frf_details_last_updatedby':loggedInUser,
                            'custrecord_frf_details_last_updated_on':new Date()
						}
					});
					data.push(record_id);
				}
			}
		}
		return data;

	}
	function _logValidation(value) {
		if (value != null && value != '' && value != undefined && value.toString() != 'NaN' && value != NaN) {
			return true;
		} else {
			return false;
		}
	}

	return {
		'post': frfDetailsRedData
	};
});