function postRESTlet(dataIn)
{
	try
	{
			nlapiLogExecution('Debug','dataIn',JSON.stringify(dataIn));
			var request_Type = 'GET';
			var response = new Response();
			switch(request_Type)
			{
				case M_Constants.Request.Get:
				if(!dataIn instanceof Array)
				{
                    response.Data = "Some error with the data sent";
                    response.Status = false;
				}
			    else
				{
					response.Data = getProjectDetails(dataIn);
                    response.Status = true;
                }
			break;
			}
          return response;
	}

	catch(error)
	{
		nlapiLogExecution('debug','postRESTlet',error);
		response.Data = error;
		response.status = false;
	}

	nlapiLogExecution('debug', 'response', JSON.stringify(response));

}
var month_array = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
function getProjectDetails(dataIn)
{
	try{
	
//Added code
	var a_project_list = new Array();
	var requestData = dataIn.project;
//	var requestData = [138764,84486,88180,142968,65557,121898,126336];
	//var requestData = [138764];
	
	nlapiLogExecution('DEBUG','Data',requestData);
	
	var project_search_results = '';
	project_search_results = getSelectedProjects(requestData)
	var rev_pojected = 0;
	var s_selected_project_name = '';
	var a_selected_project_list = '';
	
	var showAll = false;
	var s_from = '';
	var s_to = '';
	var search = nlapiLoadSearch('transaction', 1897);
	var filters = search.getFilters();
	//Search for exchange Rate
		var f_rev_curr = 0;
		var f_cost_curr = 0;
		//var filters = [];
	
		//var filters = [];
		//filters.push(new nlobjSearchFilter('formulatext', null, 'is', s_mont).setFormula('{custrecord_pl_currency_exchange_month}')); //NOTE : Capital 'Y' in Year
		//filters.push(new nlobjSearchFilter('formulatext', null, 'is', s_year_).setFormula('{custrecord_pl_currency_exchange_year}'));
			
			var a_dataVal =PL_curencyexchange_rate();
			/*	var dataRows = [];

			var column = new Array();
			column[0] = new nlobjSearchColumn('custrecord_pl_currency_exchange_revnue_r');
			column[1] = new nlobjSearchColumn('custrecord_pl_currency_exchange_cost');
			column[2] = new nlobjSearchColumn('custrecord_pl_currency_exchange_year');
			column[3] = new nlobjSearchColumn('custrecord_pl_currency_exchange_month');
			//column[4] = new nlobjSearchColumn('custrecord_gbp_converstion_rate');
			column[4] = new nlobjSearchColumn('custrecord_pl_crc_cost_rate');
			column[5] = new nlobjSearchColumn('custrecord_pl_nok_cost_rate');
			column[6] = new nlobjSearchColumn('custrecord_eur_usd_conv_rate');
			column[7] = new nlobjSearchColumn('custrecord_gbp_converstion_rate');
			column[8] = new nlobjSearchColumn('custrecord_aud_to_usd');
			column[9] = new nlobjSearchColumn('custrecord_usd_ron_conversion_rate');
			column[10] = new nlobjSearchColumn('custrecord_usd_cad_conversion_rate');

			var currencySearch = nlapiSearchRecord('customrecord_pl_currency_exchange_rates', null, null, column);
			if (currencySearch) {
				for (var i_indx = 0; i_indx < currencySearch.length; i_indx++) {
					var indexvalue=currencySearch[i_indx].getText('custrecord_pl_currency_exchange_month')+"_"+currencySearch[i_indx].getText('custrecord_pl_currency_exchange_year');
					a_dataVal[indexvalue] = {
					//a_dataVal = {
						s_month: currencySearch[i_indx].getText('custrecord_pl_currency_exchange_month'),
						i_year: currencySearch[i_indx].getText('custrecord_pl_currency_exchange_year'),
						rev_rate: currencySearch[i_indx].getValue('custrecord_pl_currency_exchange_revnue_r'),
						cost_rate: currencySearch[i_indx].getValue('custrecord_pl_currency_exchange_cost'),
						gbp_rev_rate : currencySearch[i_indx].getValue('custrecord_gbp_converstion_rate'),
						crc_cost: currencySearch[i_indx].getValue('custrecord_pl_crc_cost_rate'),
						nok_cost_rate: currencySearch[i_indx].getValue('custrecord_pl_nok_cost_rate'),
						 eur_conv_rate : currencySearch[i_indx].getValue('custrecord_eur_usd_conv_rate'),
						 aud_conv_rate : currencySearch[i_indx].getValue('custrecord_aud_to_usd'),
						 cad_conv_rate : currencySearch[i_indx].getValue('custrecord_usd_cad_conversion_rate'), 
						 ron_conv_rate : currencySearch[i_indx].getValue('custrecord_usd_ron_conversion_rate')

					};
					//if (_logValidation(a_dataVal))
					//	dataRows.push(a_dataVal);


				}
			}
			*/
	//Exchange coded ended
	//   Filter for select using the project data
	var s_project_options = '';
	var current_date = new Date();
	current_date = nlapiDateToString(current_date);
	for (var i = 0; project_search_results != null
		        && i < project_search_results.length; i++) {
		var i_project_id = project_search_results[i].getValue('internalid');
		var s_project_number = project_search_results[i]
			        .getValue('entityid');
		var s_project_name = project_search_results[i].getValue('companyname');
		var s_proj_currency = project_search_results[i].getText('custentity_project_currency');
		var exchange_rate = nlapiExchangeRate(s_proj_currency,'USD',current_date);
       nlapiLogExecution('DEBUG','Proj_values',parseFloat(project_search_results[i].getValue('custentity_projectvalue')));
      nlapiLogExecution('DEBUG','rate',exchange_rate);
		var PO_values= parseFloat(project_search_results[i].getValue('custentity_projectvalue'))*exchange_rate;
        nlapiLogExecution('DEBUG','PO_values',PO_values);
		//var duplicate_rate = nlapiExchangeRate('INR','USD',current_date);
		//PO_values = parseFloat(PO_values ) * duplicate_rate;
		rev_pojected = parseFloat(rev_pojected) + parseFloat(PO_values);
		//var s_selected_project = request.getParameter('project');

		if(_logValidation(s_project_number)){
				a_project_list.push(s_project_number);
		}
		var s_selected = '';
		if ((a_selected_project_list != null && a_selected_project_list
			        .indexOf(s_project_number) != -1)
			        || showAll == true) {
			s_selected = ' selected="selected" ';

			if (s_selected_project_name != '') {
					s_selected_project_name += ', ';
			}

			s_selected_project_name += s_project_number + ' '
				        + s_project_name;
		}
		s_project_options += '<option value="' + s_project_number + '" '
			        + s_selected + '>' + s_project_number + ' '
			        + s_project_name + '</option>';
	}
	if (a_selected_project_list == null
		        || a_selected_project_list.length == 0) {
			if (a_project_list.length > 0) {
				a_selected_project_list = a_project_list;

				showAll = true;
			} else {
				a_selected_project_list = new Array();
			}
	}

	if (a_selected_project_list != null
		        && a_selected_project_list.length != 0) {
			var s_formula = '';

		for (var i = 0; i < a_selected_project_list.length; i++) {
			if (i != 0) {
					s_formula += " OR";
			}
			s_formula += " SUBSTR({custcolprj_name}, 0,9) = '"
				        + a_selected_project_list[i] + "' ";
		}
		var projectFilter = new nlobjSearchFilter('formulanumeric', null,
			        'equalto', 1);

			projectFilter.setFormula("CASE WHEN " + s_formula
			        + " THEN 1 ELSE 0 END");

			filters = filters.concat([ projectFilter ]);
	} else {
		var projectFilter = new nlobjSearchFilter('formulanumeric', null,
			        'equalto', 1);

		projectFilter.setFormula("0");

		filters = filters.concat([ projectFilter ]);
	}
	/*if (request.getParameter('from') != null
		        && request.getParameter('from') != '') {
			s_from = request.getParameter('from');

			// filters = filters.concat([new nlobjSearchFilter('trandate', null,
			// 'onorafter', s_from)]);
	}
	else
		s_from = '1/1/2017';	*/
		
	//var s_exclude =  request.getParameter('efc') ;
	//var s_exclude ;
	//nlapiLogExecution('debug','Exclude Value Parameter',request.getParameter('efc'));
	//s_exclude =  request.getParameter('efc') ;
	//nlapiLogExecution('debug','Exclude Value',s_exclude);
	/*if (request.getParameter('to') != null
		        && request.getParameter('to') != '') {
			s_to = request.getParameter('to');

			// filters = filters.concat([new nlobjSearchFilter('trandate', null,
			// 'onorbefore', s_to)]);
	}*/
	var columns = search.getColumns();
	columns[0].setSort(false);
	columns[3].setSort(true);
	columns[9].setSort(false);
	filters.push(new nlobjSearchFilter('type',null,'noneof',['SalesOrd','PurchOrd']));
	filters.push(new nlobjSearchFilter('transactionnumbernumber',null,'isnotempty'));
	
	//  filters.push(new nlobjSearchFilter('postingperiod',null,'abs','140'));
	filters.push(new nlobjSearchFilter('status',null,'noneof',
			['ExpRept:A','ExpRept:B','ExpRept:C','ExpRept:D','ExpRept:E','ExpRept:H','Journal:A','VendBill:C',
			'VendBill:D','VendBill:E',,'CustInvc:E','CustInvc:D']));
	var search_results = searchRecord('transaction', null, filters, [
		        columns[2], columns[1], columns[0], columns[3], columns[4],
		        columns[5], columns[8], columns[9] ,columns[10] ,columns[11],columns[12],columns[13]]);

	

	
	var o_json = new Object();

	var o_data = {
		    'Revenue' : [],
		   // 'Discount' : [],
		    'People Cost' : [],
		    'Facility Cost' : [],
			//'Other Cost - Travel':[],
			//'Other Cost - Immigration': [],
			//'Other Cost - Professional Fees' : [],
			//'Other Cost - Others' : [],
			'Other Costs' : [],
			'Revenue projected' : rev_pojected.toFixed(2)
		//	'fte' : []
			
	};
	var new_object = null;
	var s_period = '';
	var a_period_list = [];
	var a_category_list = [];
	a_category_list[0]='';
	var a_group = [];
	var a_income_group = [];
	var j_other_list = {};
	var other_list =[];
	var col = new Array();
	col[0] = new nlobjSearchColumn('custrecord_account_name');
	col[1] = new nlobjSearchColumn('custrecord_type_of_cost');
    var oth_fil=new Array();
    oth_fil[0]=new nlobjSearchFilter('isinactive',null,'is','F');
	var other_Search = nlapiSearchRecord('customrecord_pl_other_costs',null,oth_fil,col);
	if(other_Search){
		for(var i_ind=0;i_ind < other_Search.length;i_ind++){
			var acctname=other_Search[i_ind].getValue('custrecord_account_name');
			a_category_list.push(acctname);
			j_other_list = {
						o_acct : other_Search[i_ind].getValue('custrecord_account_name'),
						o_type : other_Search[i_ind].getText('custrecord_type_of_cost')					
			};
			other_list.push(j_other_list);
		}
	}
	for (var i = 0; search_results != null && i < search_results.length; i++) {
		var period = search_results[i].getText(columns[0]);
		var i_period=search_results[i].getValue(columns[0]);//Added by praveena on 28-05-2021
		
		//Code updated by Deepak, Dated - 21 Mar 17
		var s_month_year = period.split(' ');
		var s_mont = s_month_year[0];
		s_mont = getMonthCompleteName(s_mont);
		var s_year_ = s_month_year[1];
		var f_revRate = 66.0;
		var f_costRate = 66.0;	
		var f_gbp_rev_rate, f_crc_cost_rate, f_nok_cost_rate,f_eur_conv_rate,f_aud_conv_rate,f_ron_conv_rate,f_cad_conv_rate;
            //	var rate=nlapiExchangeRate('GBP', 'INR', mnt_lat_date);
            //	nlapiLogExecution('audit','accnt name:- '+mnt_lat_date);
            //nlapiLogExecution('audit','accnt name:- '+rate);

             //Fetch matching cost and rev rate convertion rate
				var data_indx=(s_mont+"_"+s_year_).toString();
					 
                /* 
				if (a_dataVal[data_indx]) {
					f_revRate = a_dataVal[data_indx].rev_rate;
					f_costRate = a_dataVal[data_indx].cost_rate;
					f_gbp_rev_rate = a_dataVal[data_indx].gbp_rev_rate;
					f_crc_cost_rate = a_dataVal[data_indx].crc_cost;
					f_nok_cost_rate = a_dataVal[data_indx].nok_cost_rate;
					f_eur_conv_rate = a_dataVal[data_indx].eur_conv_rate;
					f_aud_conv_rate = a_dataVal[data_indx].aud_conv_rate;
					f_ron_conv_rate = a_dataVal[data_indx].ron_conv_rate;
					f_cad_conv_rate = a_dataVal[data_indx].cad_conv_rate;
                }
				*/
		/*	if(f_gbp_rev_rate==0)
			{
			f_gbp_rev_rate=getExchangerates(s_mont,s_year_);
			}*/
			
		var transaction_type = search_results[i].getText(columns[8]);

		var transaction_date = search_results[i].getValue(columns[9]);
			
		var i_subsidiary = search_results[i].getValue(columns[10]);

		var amount = parseFloat(search_results[i].getValue(columns[1]));

		var category = search_results[i].getValue(columns[3]);
		
		var s_account_name = search_results[i].getValue(columns[11]);
		
		var currency=search_results[i].getValue(columns[12]);
			
		var exhangeRate=search_results[i].getValue(columns[13]);
		//	nlapiLogExecution('audit','exhangeRate:- ',exhangeRate);
			
		nlapiLogExecution('audit','accnt name:- '+s_account_name);
		//Calculate the amount into USD based on conversion rate
		amount=_conversionrateIntoUSD(i_subsidiary, amount, category, exhangeRate, a_dataVal[data_indx]);
		amount=amount? parseFloat(amount) : 0;
		
			/*	var flag = false;
			if((currency != parseInt(6)) && (parseInt(i_subsidiary) == parseInt(3)) && category == 'Revenue')
			{
				amount= parseFloat(amount)* parseFloat(exhangeRate);
				amount = parseFloat(amount) /parseFloat(f_revRate);
				flag = true;
				
			}
			var eur_to_inr = 77.25;
			
			//AMount Convertion Logic
			if((parseInt(i_subsidiary) != parseInt(2))&&(parseInt(i_subsidiary) != parseInt(7)) &&(currency!= parseInt(1))){
			if (category != 'Revenue' && category != 'Other Income'
				&& category != 'Discount' && parseInt(i_subsidiary) != parseInt(2)) {
					if(currency== parseInt(9))
					{
						amount = parseFloat(amount) /parseFloat(f_crc_cost_rate);
					}
					else if(currency == parseInt(8)){
					amount = parseFloat(amount) /parseFloat(f_nok_cost_rate);
					}
					else if(currency == parseInt(10))
					{
                      	amount= parseFloat(amount)* parseFloat(exhangeRate);
						amount = parseFloat(amount)/parseFloat(f_aud_conv_rate);
					}
					else if (currency == parseInt(11) && f_ron_conv_rate) {
					 amount = parseFloat(amount) * parseFloat(exhangeRate);
					 amount = parseFloat(amount) / parseFloat(f_ron_conv_rate);
					} 
					else if(currency == parseInt(3) && f_cad_conv_rate)
					{
						 amount = parseFloat(amount) * parseFloat(exhangeRate);
					 amount = parseFloat(amount) / parseFloat(f_cad_conv_rate);
					}
					
					else{
			
				amount= parseFloat(amount)* parseFloat(exhangeRate);		
				amount = parseFloat(amount) /parseFloat(f_costRate);	
				}
				
			}
              else if(flag == false && parseInt(currency) != parseInt(1) && parseInt(i_subsidiary) != parseInt(2)){
				amount= parseFloat(amount)* parseFloat(exhangeRate);
				amount = parseFloat(amount) /parseFloat(f_revRate);
				
			//f_total_revenue += o_data[s_category][j].amount;
			}
            }
			else if((currency != parseInt(2)) && (parseInt(i_subsidiary) == parseInt(7)))
			{
				amount= parseFloat(amount)* parseFloat(exhangeRate);
				amount = parseFloat(amount) /parseFloat(f_gbp_rev_rate);
			}
          else if((currency == parseInt(2)) && (parseInt(i_subsidiary) == parseInt(7)))
			{
				amount= parseFloat(amount)* parseFloat(exhangeRate);
				amount = parseFloat(amount) /parseFloat(f_gbp_rev_rate);
			}
			
			*/
		var isWithinDateRange = true;

		var d_transaction_date = nlapiStringToDate(transaction_date,
			        'datetimetz');

		if (s_from != '') {
				var d_from = nlapiStringToDate(s_from, 'datetimetz');

				if (d_transaction_date < d_from) {
					isWithinDateRange = false;
				}
		}

		if (s_to != '') {
				var d_to = nlapiStringToDate(s_to, 'datetimetz');

				if (d_transaction_date > d_to) {
					isWithinDateRange = false;
				}
		}
		if (isWithinDateRange == true) {
			var i_index = a_period_list.indexOf(period);
			if (i_index == -1) {
				if(_logValidation(period)){
					a_period_list.push(period);
				}
				else{
					//nlapiLogExecution('debug', 'error', '388');
				}
			
			}

			i_index = a_period_list.indexOf(period);
		}
		if(category != 'People Cost' && category != 'Discount'&&  category != 'Revenue' && category != 'Facility Cost')
		{
			var o_index=a_category_list.indexOf(s_account_name);
			if(o_index!=-1)
			{
			o_index=o_index-1;
			var acct=other_list[o_index].o_acct;
			var typeofact=other_list[o_index].o_type;
			
			
			if((_logValidation(period))&&(_logValidation(amount))&& (_logValidation(search_results[i].getValue(columns[4])))
			   && (_logValidation(search_results[i].getValue(columns[5]))) &&  (_logValidation(transaction_type))
			   &&(_logValidation(transaction_date))&&(_logValidation(isWithinDateRange))&& (_logValidation(s_account_name)))
			   {
			   }
			   else{
			 //  nlapiLogExecution('debug','error','408');
			   }
			if(typeofact == 'Other Direct Cost - Travel' || typeofact == 'Other Direct Cost - Immigration' || 
							typeofact == 'Other Direct Cost - Professional Fees' ||typeofact == 'Other Direct Cost - Others' ){
			o_data['Other Costs'].push({
			   'period' : period,
			   'amount' : amount,
			  // 'num' : search_results[i].getValue(columns[4]),
			  // 'memo' : search_results[i].getValue(columns[5]),
			   //'type' : transaction_type,
			   //'dt' : transaction_date,
			   //'include' : isWithinDateRange,
				//'s_account_name' : s_account_name
			});
			}
						
			}
			
			
		}
		else
		{
			
				
			//avoiding discounts from account                         
            //if(category == 'Revenue' && i_subsidiary == parseInt(2))
			if(category == 'Revenue' && (
                                    (i_subsidiary == parseInt(2)) ||
									(i_subsidiary == parseInt(13)) || (i_subsidiary == parseInt(14)) ||
                                    ((i_subsidiary == parseInt(11) || i_subsidiary == parseInt(12)) && i_period>307) ||
                                    ((i_subsidiary == parseInt(3)  || i_subsidiary == parseInt(7))  && i_period>309)
                                )
			)
			{
				if(s_account_name != parseInt(732))
				{
					o_data[category].push({
					'period' : period,
					'amount' : amount,
					//'num' : search_results[i].getValue(columns[4]),
					//'memo' : search_results[i].getValue(columns[5]),
					//'type' : transaction_type,
					//'dt' : transaction_date,
					//'include' : isWithinDateRange,
					//'s_account_name' : s_account_name
					});
                      nlapiLogExecution('audit','subs:- '+i_subsidiary);
					
                      
				}
					
			}
			else{
			   o_data[category].push({
			    'period' : period,
			    'amount' : amount,
			  //  'num' : search_results[i].getValue(columns[4]),
			  //  'memo' : search_results[i].getValue(columns[5]),
			   // 'type' : transaction_type,
			   // 'dt' : transaction_date,
			  //  'include' : isWithinDateRange,
				//'s_account_name' : s_account_name
			});
			}
			
				//nlapiLogExecution('audit','cat:- '+category);
		}
		}

	
//Added Code exit
	return o_data;
	}
catch (err)
{
	nlapiLogExecution('DEBUG','Error in getProjectDetails',err);
}	

}

function getSelectedProjects(requestData)
{
	
	
	if(_logValidation(requestData))
	{
		nlapiLogExecution('debug', 'projectlist',requestData);
		var project_filter = [
		[	             
		 'internalid','anyOf', requestData]
		];

		var project_search_results = searchRecord('job', null, project_filter,
		[ 	new nlobjSearchColumn("entityid"),
		    new nlobjSearchColumn("altname"),
			new nlobjSearchColumn("companyname"),
			new nlobjSearchColumn("internalid"),
			new nlobjSearchColumn("startdate"),
			new nlobjSearchColumn("custentity_projectvalue"),
			new nlobjSearchColumn("custentity_project_currency")
		]);

		nlapiLogExecution('debug', 'project count',project_search_results.length);
				
		return project_search_results;
	}
}

function createPeriodList(startDate, endDate) {
	var d_startDate = nlapiStringToDate(startDate);
	var d_endDate = nlapiStringToDate(endDate);

	var arrPeriod = [];

	for (var i = 0;; i++) {
		var currentDate = nlapiAddMonths(d_startDate, i);
		if((_logValidation(getMonthName(currentDate)))&& (_logValidation(getMonthStartDate(currentDate)))&&(_logValidation(getMonthEndDate(currentDate))))
		{
		}
		else{
		nlapiLogExecution('debug', 'error', '909');
		}
		arrPeriod.push({
		    Name : getMonthName(currentDate),
		    StartDate : getMonthStartDate(currentDate),
		    EndDate : getMonthEndDate(currentDate)
		});

		if (getMonthEndDate(currentDate) >= d_endDate) {
			break;
		}
	}

	var today = new Date();

	// remove the current month
	if (d_endDate.getMonth() >= today.getMonth()) {
		arrPeriod.pop();
	}

	return arrPeriod;
}

function getMonthEndDate(currentDate) {
	return nlapiAddDays(nlapiAddMonths(currentDate, 1), -1);
}

function getMonthStartDate(currentDate) {
	var month = currentDate.getMonth() + 1;
	var year = currentDate.getFullYear();
	var s_startDate = month + "/1/" + year;
	var d_startDate = nlapiStringToDate(s_startDate);
	return d_startDate;
}

function getMonthName(currentDate) {
	var monthNames = [ "JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JUL", "AUG",
	        "SEP", "OCT", "NOV", "DEC" ];
	return monthNames[currentDate.getMonth()] + " " + currentDate.getFullYear();
}

function getExchangerates(){
//var mnt_lat_date=getLastDate(s_mont,s_year_);
var amount=3910;
var rate=nlapiExchangeRate('USD', 'INR');
var con_amou=parseFloat(amount)*parseFloat(rate);
nlapiLogExecution('Debug','ExchangeValue'+rate,'amount'+con_amou);
var rate2=nlapiExchangeRate('INR', 'USD');
con_amou=parseFloat(con_amou)*parseFloat(rate2);
nlapiLogExecution('Debug','ExchangeValue'+rate2,'amount'+con_amou);
var column = new Array();
		column[0] = new nlobjSearchColumn('custrecord_pl_currency_exchange_year');
		column[1] = new nlobjSearchColumn('custrecord_pl_currency_exchange_month');
		column[2] = new nlobjSearchColumn('custrecord_pl_gbp_revenue');
		column[3]=new nlobjSearchColumn('internalid');	
		//var currencySearch = nlapiSearchRecord('customrecord_pl_currency_exchange_rates',null,null,column);
	//	if(currencySearch){
		//	for(var i_indx=0;i_indx < currencySearch.length;i_indx++){
		//	var id=currencySearch[i_indx].getValue('internalid');	
			//var rec=nlapiLoadRecord('customrecord_pl_currency_exchange_rates',id);	
			//	rec.setFieldValue('custrecord_pl_gbp_revenue',rate);
			//	nlapiSubmitRecord(rec);
			nlapiLogExecution('Debug','ExchangeValue',rate);
		//}
	//}					
	//return rate;
}

function getLastDate(month,year)
{
	var date='';
	if(month == 'January')
		date = "1/31/"+year;
	if(month == 'February')
		date = "2/28/"+year;
	if(month == 'March')
		date = "3/31/"+year;
	if(month == 'April')
		date = "4/30/"+year;
	if(month == 'May')
		date = "5/31/"+year;
	if(month == 'June')
		date = "6/30/"+year;
	if(month == 'July')
		date = "7/31/"+year;
	if(month == 'August')
		date = "8/31/"+year;
	if(month == 'September')
		date = "9/30/"+year;
	if(month == 'October')
		date = "10/31/"+year;
	if(month == 'November')
		date = "11/30/"+year;
	if(month == 'December')
		date = "12/31/"+year;
	
	return date;
}

//Get the complete month name
function getMonthCompleteName(month){
	var s_mont_complt_name = '';
	if(month == 'Jan')
		s_mont_complt_name = 'January';
	if(month == 'Feb')
		s_mont_complt_name = 'February';
	if(month == 'Mar')
		s_mont_complt_name = 'March';
	if(month == 'Apr')
		s_mont_complt_name = 'April';
	if(month == 'May')
		s_mont_complt_name = 'May';
	if(month == 'Jun')
		s_mont_complt_name = 'June';
	if(month == 'Jul')
		s_mont_complt_name = 'July';
	if(month == 'Aug')
		s_mont_complt_name = 'August';
	if(month == 'Sep')
		s_mont_complt_name = 'September';
	if(month == 'Oct')
		s_mont_complt_name = 'October';
	if(month == 'Nov')
		s_mont_complt_name = 'November';
	if(month == 'Dec')
		s_mont_complt_name = 'December';
	
	return s_mont_complt_name;
}
function _logValidation(value) 
{
 if(value != null && value.toString() != null && value != ''&& value != '- None -' && value != undefined && value.toString() != undefined && value != 'undefined' && value.toString() != 'undefined'&& value.toString() != 'NaN' && value != NaN) 
 {
  return true;
 }
 else 
 { 
  return false;
 }
}
function getResourceCount(proj_list)
{
	var resourceallocationSearch = nlapiSearchRecord("resourceallocation",null,
	[
		["project","anyof","138764","88180","142968","65557","121898","126336"]], 
		[new nlobjSearchColumn("id").setSort(false), 
		new nlobjSearchColumn("resource"), 
		new nlobjSearchColumn("company"), 
		new nlobjSearchColumn("startdate"), 
		new nlobjSearchColumn("percentoftime"), 
		new nlobjSearchColumn("enddate"), 
		new nlobjSearchColumn("employeestatus","employee",null), 
		new nlobjSearchColumn("subsidiary","employee",null)]);
		if(_logValidation(resourceallocationSearch))
		{
			return resourceallocationSearch;
		}
		else
		{
			
			throw "No Active Allocations";
		}
}