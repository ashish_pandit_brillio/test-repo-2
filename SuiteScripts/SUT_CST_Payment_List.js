// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
/*
   	Script Name: Service CST Payment List  
	Author:      Supriya Jadhav
	Company:     Aashna Cloudtech Pvt Ltd
	Date:      30 July 2014
	Version:     
	Description: Creat new Suitelet form for VAT Payment Deatil.


	Script Modification Log:

	-- Date --			-- Modified By --				--Requested By--				-- Description --
     26 Aug 2014             Supriya                       Kalpana                       Updated Group Tax Functionality for approved bills only
     3 Sep  2014             Supriya                       Mehul                         Added Mark All and UNMark All Button
     22 sep 2014         	 Supriya                       Kalpana                       Normal Account Validation
 	  5 feb 2014              nikhil                        sachin/kalpana                add a bill credit/voided functionality
Below is a summary of the process controls enforced by this script file.  The control logic is described
more fully, below, in the appropriate function headers and code blocks.


     SUITELET
		- suiteletFunction(request, response)
         CreatePaymentform(request, response)

     SUB-FUNCTIONS
		- The following sub-functions are called by the above core functions in order to maintain code
            modularization:
            addReturnButton(form)
			populatebankaccount(request, response,alist)
			popluattaxagency(request, response,taxagency)
			searchTax(request, response)
			storeCriteriaParameters(form, request)
			setSubList(sublist, form,isForm,results,request,response)
			getTaxaccount(taxcode)
			getPurchasetaxtotal(request,taxcode)
			getSaletaxtotal(request,taxcode)
			processRecord(name ,request, isDevice)
			getAccountid(accountname)
			searchBillrecord(taxcode,fromdate,todate)
			updateBillrecord(billid)
			searchInvoicerecord(taxcode,fromdate,todate)
			updateInvoicerecord(invoiceid)


               - NOT USED

*/
}
// END SCRIPT DESCRIPTION BLOCK  ====================================



// BEGIN GLOBAL VARIABLE BLOCK  =====================================
{
	//  Initialize any Global Variables, in particular, debugging variables...


	var purchasetaxtotal
	var fromdate
	var todate
	var a_subisidiary = new Array();
	var i_vatCode ;
	var i_taxcode ;
	var i_PayTaxcode ;
	var b_Mand_Class;
	var b_Mand_department;
	var b_Mand_location;

}
// END GLOBAL VARIABLE BLOCK  =======================================





// BEGIN SUITELET ==================================================

function CreatePaymentform(request, response)
{

	/*  Suitelet:

          - EXPLAIN THE PURPOSE OF THIS FUNCTION

		-


		FIELDS USED:

          --Field Name--				--ID--


	*/



	//  LOCAL VARIABLES



    //  SUITELET CODE BODY
    nlapiLogExecution('DEBUG','Request Method', " " + request.getMethod());
    if (request.getMethod() == 'GET' )
		{
			// ===== GET THE VALUES OF GLOBAL SUBSIDIARY PARAMETERS ====
			var i_AitGlobalRecId =  SearchGlobalParameter();
			nlapiLogExecution('DEBUG','Bill ', "i_AitGlobalRecId"+i_AitGlobalRecId);
			
			//==== CODE TO CHECK THE INDIAN SUBSIDIARY ====
			if(i_AitGlobalRecId != 0 )
			{
				var o_AitGloRec = nlapiLoadRecord('customrecord_indian_tax_glo_parameter',i_AitGlobalRecId);
				
				a_subisidiary= o_AitGloRec.getFieldValues('custrecord_ait_subsidiary');
				nlapiLogExecution('DEBUG','Bill ', "a_subisidiary->"+a_subisidiary);
				
				i_vatCode = o_AitGloRec.getFieldValue('custrecord_ait_vat_code');
				nlapiLogExecution('DEBUG','Bill ', "i_vatCode->"+i_vatCode);
				
				i_taxcode = o_AitGloRec.getFieldValue('custrecord_ait_taxcode');
				nlapiLogExecution('DEBUG','Bill ', "i_taxcode->"+i_taxcode);
				
				i_PayTaxcode = o_AitGloRec.getFieldValue('custrecord_pay_servicetax');
				nlapiLogExecution('DEBUG', 'Bill ', "i_PayTaxcode->" + i_PayTaxcode);
				
				b_Mand_location = o_AitGloRec.getFieldValue('custrecord_ait_location');
				nlapiLogExecution('DEBUG', 'Bill ', "b_Mand_location->" + b_Mand_location);
				
				b_Mand_Class = o_AitGloRec.getFieldValue('custrecord_ait_class');
				nlapiLogExecution('DEBUG', 'Bill ', "b_Mand_Class->" + b_Mand_Class);
				
				b_Mand_department = o_AitGloRec.getFieldValue('custrecord_ait_department');
				nlapiLogExecution('DEBUG', 'Bill ', "b_Mand_department->" + b_Mand_department);
				
				
				//===== FOR SINGLE SUBSIDARY ACCOUNT  =======
				var context = nlapiGetContext();
			    var i_subcontext = context.getFeature('SUBSIDIARIES');
				if(i_subcontext == false)
				{
					 a_subisidiary = null;
				}
				
				//=====CREATE FORM =====
			var form = nlapiCreateForm('CST Tax Payment List');

			//==== CALL CLIENT SCRIPT=
			form.setScript('customscript_vatpaymentlistclient');

			var fromdate=request.getParameter('fromdate')
	 	    var todate=request.getParameter('todate')

	  		//var grosssale= form.addField('grosssale','currency', 'Sale Gross Total');
	        ////grosssale.setDisplayType('inline')
	        //grosssale.setDefaultValue('0.00')

	        //var grosspurchase= form.addField('grosspurchase','currency', 'Purchase Gross Total');
	        //grosspurchase.setDisplayType('inline')
	        //grosspurchase.setDefaultValue('0.00')

	        //var saleamount= form.addField('totalsaleamount','currency', 'Sale amount Total');
	        //saleamount.setDisplayType('inline')
	        //saleamount.setDefaultValue('0.00')

	        //var purchaseamount= form.addField('totalpurchaseamont','currency', 'Purchase amount Total');
	        //purchaseamount.setDisplayType('inline')
	        //purchaseamount.setDefaultValue('0.00')

	  		var salebalance= form.addField('totalsaletax','currency', 'Sale Tax Account balance');
	        salebalance.setDisplayType('inline')
	        salebalance.setDefaultValue('0.00')

	        var purchasebalance= form.addField('totalpurchasetax','currency', 'Purchase Tax Account balance');
	        purchasebalance.setDisplayType('inline')
	        purchasebalance.setDefaultValue('0.00')

	        var vatpayable=form.addField('vatpayable','currency', 'Total Service Tax Payable');
	        vatpayable.setDisplayType('inline')
	        vatpayable.setDefaultValue('0.00')

	        var sysdate=new Date
			var date1=nlapiDateToString(sysdate)
			var pdate=form.addField('pdate','date', 'Date');
			pdate.setMandatory(true)
			pdate.setDefaultValue(date1)

			var acclist=form.addField('bankaccount','select','Bank A/C')
	  		acclist.setMandatory(true)
			acclist.addSelectOption('','');
			populatebankaccount(request, response,acclist , a_subisidiary);
			
            var classlist=form.addField('class','select','Class','classification')
           	if (b_Mand_Class == 'T') {
				classlist.setMandatory(true)
			}
			var lineaccount=form.addField('account','select','Tax Payable Account')
			lineaccount.setMandatory(true)
			lineaccount.addSelectOption('','');
			populatepaybleaccount(request, response,lineaccount , a_subisidiary)

	        var taxagency=form.addField('taxagency','select', 'Tax Agency');
		    taxagency.setMandatory(true)
		    taxagency.addSelectOption('','');
		    popluattaxagency(request, response,taxagency,a_subisidiary)
			
			var department = form.addField('department', 'select', 'Departement');
		   department.addSelectOption('', '');
			popluatDepartment(request, response, department, a_subisidiary)
			if (b_Mand_department == 'T') 
			{
				department.setMandatory(true)
			}
			if (i_subcontext != false) 
			{
				var subsidiary = form.addField('subsidiary', 'select', 'Subsidiary', 'subsidiary');
				subsidiary.setMandatory(true)
			}
			var loc = form.addField('location', 'select', 'Location');
			if (b_Mand_location == 'T') 
			{
			    loc.setMandatory(true)
			}
			loc.addSelectOption('', '');
            popluatlocation(request, response, loc , a_subisidiary);
         
	        form.addField('memo','text', 'Memo');
			form.addField('cheque','text', 'Cheque No');

	        storeCriteriaParameters(form, request)
			addReturnButton(form)
			form.addButton('custpage_printdoc', 'CST Tax Statement','PrintVat()');

	        // ===== ADD SUMIT BUTTON ====
			form.addSubmitButton('Submit');


	  		// ===== ADD TAB ======
			form.addTab("tab1", "Payment Detail");

	        //===CODE TO ADD LIST ==========
	       	var Result = searchTax(request, response);
	       	var sublist1 = form.addSubList('paymentlist','list', 'paymentlist', 'tab1');
			sublist1.addButton('custpage_markall','Mark All',"MarkAll();");
			sublist1.addButton('custpage_unmarkall','UnMark All',"UnMarkAll();");
			if(Result != null)
			{
					var rs=setSubList(sublist1,form,1, Result,request, response , i_taxcode , i_PayTaxcode);
			}
	       
	        response.writePage( form );
				
				
			}// END if(i_AitGlobalRecId != 0 )
			
			
	 		
	   } // END  if (request.getMethod() == 'GET' )
	else if(request.getMethod() == 'POST')
    {
   		nlapiLogExecution('DEBUG','user', 'Post method called ')
   		var i_paymentID = processRecord('paymentlist',request, 1);
		
		nlapiSetRedirectURL('RECORD','customrecord_vatpayment_main',i_paymentID,'View');
   		
   		//nlapiSetRedirectURL('SUITELET','customscript_stpayment_criteria', '1', false);
    }// END else if(request.getMethod() == 'POST')





}

// END SUITELET ====================================================




// BEGIN OBJECT CALLED/INVOKING FUNCTION ===================================================
{
//Begin:Add return to criteria button
function addReturnButton(form)
{
	/*
var url = nlapiResolveURL('SUITELET','customscript_sut_cst_tax_paymentcriteria', '1');
	var script = "{ window.location = '" + url + "'}";
	form.addButton('criteriabutton', 'Return to Criteria', script);
	form.addPageLink('breadcrumb','Payment Criteria',nlapiResolveURL('SUITELET','customscript_stpayment_criteria', '1'))
	
*/
	var url = nlapiResolveURL('SUITELET','customscript_stpayment_criteria', '1');
	var script = "{ window.location = '" + url + "'}";
	form.addButton('criteriabutton', 'Return to Criteria', script);
	form.addPageLink('breadcrumb','Payment Criteria',nlapiResolveURL('SUITELET','customscript_stpayment_criteria', '1'))
}
//End: Add return to criteria button


//Begin :function for  Populate Bank Account List
	function populatebankaccount(request, response,alist , a_subisidiary)
	{
		
				//==== SEARCH BANK ACCOUNT FOR SUBSIDIARY ====
				
				var filchk1= new Array();
				var columnsCheck1= new Array();
				filchk1.push( new nlobjSearchFilter('type',null,'is','Bank'));
				
				if(a_subisidiary != null && a_subisidiary != undefined && a_subisidiary != '' && a_subisidiary!='undefined')
				{
					filchk1.push(new nlobjSearchFilter('subsidiary',null,'anyOf',a_subisidiary));
				}
				
			    columnsCheck1.push( new nlobjSearchColumn('name'));
			    columnsCheck1.push( new nlobjSearchColumn('internalid'));
		
			    var searchResultsCheck1 = nlapiSearchRecord('account', null, filchk1, columnsCheck1);
			    for(i=0; searchResultsCheck1 != null && i < searchResultsCheck1.length; i++)
				 {
					alist.addSelectOption(searchResultsCheck1[i].getValue('internalid'),
					searchResultsCheck1[i].getValue('name'), false);
		
				 }// END   for(i=0; i < searchResultsCheck1 != null && i < searchResultsCheck1.length; i++)
				 
				
	   

	}
//END :function for  Populate Bank Account List



//Begin:populate the tax agency
	function popluattaxagency(request, response,taxagency,a_subisidiary)
	{
		
				
			    //==== SEARCH BANK ACCOUNT FOR SUBSIDIARY ====
				var ved_filchk1= new Array();
				var ved_columnsCheck1= new Array();
		
				ved_filchk1.push( new nlobjSearchFilter('category',null,'is',3));
		        ved_filchk1.push( new nlobjSearchFilter('isinactive',null,'is','F'));
				
				if(a_subisidiary != null && a_subisidiary != undefined && a_subisidiary != '' && a_subisidiary!='undefined')
				{
					ved_filchk1.push(new nlobjSearchFilter('subsidiary',null,'anyOf',a_subisidiary));
				}
				
		
			    ved_columnsCheck1.push( new nlobjSearchColumn('internalid'));
			    ved_columnsCheck1.push( new nlobjSearchColumn('entityid'));
			    ved_columnsCheck1.push( new nlobjSearchColumn('category'));
		
			    var ved_searchResultsCheck1 = nlapiSearchRecord('vendor', null, ved_filchk1, ved_columnsCheck1);
		
				for(i=0; ved_searchResultsCheck1 != null && i < ved_searchResultsCheck1.length; i++)
				{
					taxagency.addSelectOption(ved_searchResultsCheck1[i].getValue('internalid'),
					ved_searchResultsCheck1[i].getValue('entityid'), false);
		
				}// END for(i=0; i < ved_searchResultsCheck1 != null && i < ved_searchResultsCheck1.length; i++)
				
			

	}
//End: populate the tax agency

 //Begin:Search the service tax Payment Record
function searchTax(request, response)
{
    var filters = new Array();
    var column= new Array();
	
	column.push( new nlobjSearchColumn('internalid'))
	column.push( new nlobjSearchColumn('name'))
    //customsearch_sttax_group  taxgroup
	var results = nlapiSearchRecord('salestaxitem', 'customsearch_india_csttaxitem', null, column);
	//nlapiLogExecution('DEBUG','user', 'results:-'+results)
	return results;
}
//End:Search the service tax Payment Record


//Begin:Function to store the Criteria parameters to existin form that fiels is hidden
	function storeCriteriaParameters(form, request)
	{

		var fromdate=request.getParameter('fromdate')
	 	var todate=request.getParameter('todate')

    	var stfromdate = form.addField('fromdate', 'date', 'From Date');
		stfromdate.setDisplayType('inline');
		stfromdate.setDefaultValue(fromdate);
		var sttodate = form.addField('todate', 'date', 'To Date');
		sttodate.setDisplayType('inline');
		sttodate.setDefaultValue(todate);

	}

//End:Function to store the Criteria parameters to existin form that fiels is hidden


//Begin:Function for set the sublist

function setSubList(sublist, form,isForm,results,request,response,i_taxcode,i_PayTaxcode)
{

	    sublist.addField('apply','checkbox', 'Apply');
	    if(isForm==1)
	        	{

		          sublist.addField('internalid','text','Tax Id ')
		          sublist.addField('name','text', 'Tax Name');

				}


	    sublist.setLineItemValues(results);
	    sublist.addField('purchaseaccount','text', 'Purchase Account');
	    sublist.addField('purchasetaxtotal','currency', 'Purchase Tax Total');
	    //sublist.addField('purchaseamount','currency', 'Purchase Amount');
	    //sublist.addField('purchasegrossamount','currency', 'Purchase Gross Amount');

	    sublist.addField('saleaccount','text', 'Sale Account');
	    sublist.addField('saletaxtotal','currency', 'Sale Tax Total');
	    //sublist.addField('saleamount','currency', 'Sale Amount');
	    //sublist.addField('salegrossamount','currency', 'Sale Gross Amount');

	    var p=0
	    for(var i=1;i<=results.length && results != null;i++)
	    {
	      var taxid=results[p++].getValue('internalid')
	      var taxinfo=getTaxaccount(taxid)//i_PayTaxcode
	      //sublist.setLineItemValue('purchaseaccount',i,taxinfo[0])
	      //sublist.setLineItemValue('saleaccount',i,taxinfo[1])

	      var purtotal=getPurchasetaxtotal(request,taxid)
	      sublist.setLineItemValue('purchaseaccount',i,purtotal[3])
		  sublist.setLineItemValue('purchasetaxtotal',i,parseFloat(purtotal[0].toFixed(2)))
	      //sublist.setLineItemValue('purchaseamount',i,parseFloat(purtotal[1].toFixed(2)))
	      //sublist.setLineItemValue('purchasegrossamount',i,parseFloat(purtotal[2].toFixed(2)))

	      var saletotal=getSaletaxtotal(request,taxid)
	      sublist.setLineItemValue('saleaccount',i,saletotal[3])
		  sublist.setLineItemValue('saletaxtotal',i,parseFloat(saletotal[0].toFixed(2)))
	      //sublist.setLineItemValue('saleamount',i,parseFloat(saletotal[1].toFixed(2)))
	      //sublist.setLineItemValue('salegrossamount',i,parseFloat(saletotal[2].toFixed(2)))


	    }
		return sublist;
	}
//End:Function for set the sublist
function getTaxaccount(taxcode)
 {
 	   var taxdetail=new Array()
 	   var record=nlapiLoadRecord('salestaxitem',taxcode)

 	   //var name1 = record.getFieldValue('name')
 	   taxdetail[0]=record.getFieldValue('taxtype')
 	   taxdetail[1]=record.getFieldValue('acct1')
 	   taxdetail[2]=record.getFieldValue('acct2')

 	     return taxdetail
 }

//Begin:Function to get Purchase Tax total
 function getPurchasetaxtotal(request,taxcode)
 {
 	   if (taxcode != null && taxcode != undefined && taxcode != '') 	
	   {
		var o_taxcode = nlapiLoadRecord('salestaxitem',taxcode)
		nlapiLogExecution('DEBUG','user', 'o_taxcode'+o_taxcode)
		
		var purchaseaccount = o_taxcode.getFieldValue('acct1')
		nlapiLogExecution('DEBUG','user', 'purchaseaccount'+purchaseaccount)
		
		//nlapiLogExecution('DEBUG','user', 'tax code '+taxcode)
       var fromdate=request.getParameter('fromdate')
	   var todate=request.getParameter('todate')
 	   var purchasedetail= new Array()
 	   var purchasetaxtotal=0
 	   var purchaseamount=0
 	   var purchasegrossamount=0

 	   var filters = new Array();
	   filters.push( new nlobjSearchFilter('custrecord_vatbill_taxid', null, 'is',taxcode ))//custrecord_vatbill_taxcode
	   filters.push( new nlobjSearchFilter('custrecord_vatbill_billdate', null, 'onorafter',fromdate))
	   filters.push( new nlobjSearchFilter('custrecord_vatbill_billdate', null, 'onorbefore',todate))
	   filters.push( new nlobjSearchFilter('custrecord_vatbill_status', null, 'is','Open'))
	   //filters.push( new nlobjSearchFilter('approvalstatus', 'custrecord_vatbill_tranno', 'is',2))
	   filters.push( new nlobjSearchFilter('mainline', 'custrecord_vatbill_tranno', 'is','T'))
	   filters.push( new nlobjSearchFilter('voided', 'custrecord_vatbill_tranno', 'is','F'))
	   filters.push( new nlobjSearchFilter('recordtype', 'custrecord_vatbill_tranno', 'is','vendorcredit'))
	   
	   var bill_filters = new Array();
	   bill_filters.push( new nlobjSearchFilter('custrecord_vatbill_taxid', null, 'is',taxcode ))//custrecord_vatbill_taxcode
	   bill_filters.push( new nlobjSearchFilter('custrecord_vatbill_billdate', null, 'onorafter',fromdate))
	   bill_filters.push( new nlobjSearchFilter('custrecord_vatbill_billdate', null, 'onorbefore',todate))
	   bill_filters.push( new nlobjSearchFilter('custrecord_vatbill_status', null, 'is','Open'))
	   bill_filters.push( new nlobjSearchFilter('approvalstatus', 'custrecord_vatbill_tranno', 'is',2))
	   bill_filters.push( new nlobjSearchFilter('mainline', 'custrecord_vatbill_tranno', 'is','T'))
	   bill_filters.push( new nlobjSearchFilter('voided', 'custrecord_vatbill_tranno', 'is','F'))
	   bill_filters.push( new nlobjSearchFilter('recordtype', 'custrecord_vatbill_tranno', 'is','vendorbill'))
	   
	   var column= new Array();
	   column.push( new nlobjSearchColumn('custrecord_vatbill_taxamount'))
	   column.push( new nlobjSearchColumn('custrecord_vatbill_amount'))
	   column.push( new nlobjSearchColumn('custrecord_vatbill_grossamount'))
	   
	   var results = nlapiSearchRecord('customrecord_vatbilldetail', null, bill_filters, column);
	   nlapiLogExecution('DEBUG','getPurchasetaxtotal', 'results---->'+results)
	   
	   var credit_results = nlapiSearchRecord('customrecord_vatbilldetail', null, filters, column);
	   nlapiLogExecution('DEBUG','getPurchasetaxtotal', 'results---->'+credit_results)
	   
	   if(credit_results != null)
	   {
	   	if (results != null) 
		{
			results = credit_results.concat(results);
		}
		else
		{
			results = credit_results
		}
	   }
	   if( results != null)
	   {
		    nlapiLogExecution('DEBUG','getPurchasetaxtotal', 'results length---->'+results.length)
		   for(i=0; i< results != null && i < results.length ; i++)
			 {
			 	nlapiLogExecution('DEBUG','getPurchasetaxtotal', 'i-->'+i)
				var temptotal=results[i].getValue('custrecord_vatbill_taxamount')
				nlapiLogExecution('DEBUG','getPurchasetaxtotal', 'temptotal-->'+temptotal)
			 	var tempamount=results[i].getValue('custrecord_vatbill_amount')
			 	var tempgrossamount=results[i].getValue('custrecord_vatbill_grossamount')
                nlapiLogExecution('DEBUG','getPurchasetaxtotal', 'purchasetaxtotal-->'+purchasetaxtotal)
			 	purchasetaxtotal=purchasetaxtotal+parseFloat(temptotal)
				nlapiLogExecution('DEBUG','getPurchasetaxtotal', 'Post purchasetaxtotal-->'+purchasetaxtotal)
			 	//purchaseamount=purchaseamount+parseFloat(tempamount)
			 	//purchasegrossamount=purchasegrossamount+parseFloat(tempgrossamount)

			 }
	   }
 	   purchasedetail[0]=purchasetaxtotal
 	   purchasedetail[1]=purchaseamount
 	   purchasedetail[2]=purchasegrossamount
	   purchasedetail[3]=purchaseaccount

 	   return purchasedetail
	   }
	   
 }
//End:Function to get Purchase Tax total

//Begin:Function to get Sale Tax total
 function getSaletaxtotal(request,taxcode)
 {
 	  if (taxcode != null && taxcode != undefined && taxcode != '') 
	  {
	  	var o_taxcode = nlapiLoadRecord('salestaxitem', taxcode)
	  	
	  	var saleaccount = o_taxcode.getFieldValue('acct2')
	  	nlapiLogExecution('DEBUG','user', 'saleaccount'+saleaccount)
	  	// nlapiLogExecution('DEBUG','user', 'tax code '+taxcode)
				var fromdate = request.getParameter('fromdate')
				var todate = request.getParameter('todate')
				var saledetail = new Array()
				var saletaxtotal = 0.00
				var saleamount = 0.00
				var salegrosstotal = 0.00
				
				var filters = new Array();
				filters.push(new nlobjSearchFilter('custrecord_vatinvoice_taxid', null, 'is', taxcode))//custrecord_vatinvoice_taxcode
				filters.push(new nlobjSearchFilter('custrecord_vatinvoice_date', null, 'onorafter', fromdate))
				filters.push(new nlobjSearchFilter('custrecord_vatinvoice_date', null, 'onorbefore', todate))
				filters.push(new nlobjSearchFilter('custrecord_vatinvoice_status', null, 'is', 'Open'))
				filters.push( new nlobjSearchFilter('mainline', 'custrecord_vatinvoice_tranno', 'is','T'))
	   			filters.push( new nlobjSearchFilter('voided', 'custrecord_vatinvoice_tranno', 'is','F'))
				
				var column = new Array();
				column.push(new nlobjSearchColumn('custrecord_vatinvoice_taxamount'))
				column.push(new nlobjSearchColumn('custrecord_vatinvoice_amount'))
				column.push(new nlobjSearchColumn('custrecord_vatinvoice_grossamount'))
				
				var results = nlapiSearchRecord('customrecord_vatinvoicedetail', null, filters, column);
				nlapiLogExecution('DEBUG','getSaletaxtotal', 'results ---->'+results)
				if (results != null) 
				{
					nlapiLogExecution('DEBUG','getSaletaxtotal', 'results length---->'+results.length)
					for (i = 0; results != null && i < results.length; i++) 
					{
						nlapiLogExecution('DEBUG','getSaletaxtotal', ' i-->'+i)
						var temptotal = results[i].getValue('custrecord_vatinvoice_taxamount')
						nlapiLogExecution('DEBUG','getSaletaxtotal', ' temptotal-->'+temptotal)
						var tempamout = results[i].getValue('custrecord_vatinvoice_amount')
						var tempgrossamount = results[i].getValue('custrecord_vatinvoice_grossamount')
						nlapiLogExecution('DEBUG','getSaletaxtotal', ' saletaxtotal-->'+saletaxtotal)
						saletaxtotal = saletaxtotal + parseFloat(temptotal)
						nlapiLogExecution('DEBUG','getSaletaxtotal', 'Post saletaxtotal-->'+saletaxtotal)
						//saleamount = saleamount + parseFloat(tempamout)
						//salegrosstotal = salegrosstotal + parseFloat(tempgrossamount)
					}
				}
				
				saledetail[0] = saletaxtotal
				saledetail[1] = saleamount
				saledetail[2] = salegrosstotal
				saledetail[3] = saleaccount
				
				return saledetail
			}
 }
//End:Function to get Sale Tax total

//Begin:Process the Record
 	function processRecord(name, request, isDevice)
	{
		//===== FOR SINGLE SUBSIDARY ACCOUNT  =======
		var context = nlapiGetContext();
		var i_subcontext = context.getFeature('SUBSIDIARIES');
		
		var i_AitGlobalRecId = SearchGlobalParameter();
		nlapiLogExecution('DEBUG', 'Bill ', "i_AitGlobalRecId" + i_AitGlobalRecId);
		
		//==== CODE TO CHECK THE INDIAN SUBSIDIARY ====
		if (i_AitGlobalRecId != 0) {
			var o_AitGloRec = nlapiLoadRecord('customrecord_indian_tax_glo_parameter', i_AitGlobalRecId);
			
			a_subisidiary = o_AitGloRec.getFieldValues('custrecord_ait_subsidiary');
			nlapiLogExecution('DEBUG', 'Bill ', "a_subisidiary->" + a_subisidiary);
			
			i_vatCode = o_AitGloRec.getFieldValue('custrecord_ait_vat_code');
			nlapiLogExecution('DEBUG', 'Bill ', "i_vatCode->" + i_vatCode);
			
			i_taxcode = o_AitGloRec.getFieldValue('custrecord_ait_taxcode');
			nlapiLogExecution('DEBUG', 'Bill ', "i_taxcode->" + i_taxcode);
			
			i_PayTaxcode = o_AitGloRec.getFieldValue('custrecord_pay_servicetax');
			nlapiLogExecution('DEBUG', 'Bill ', "i_PayTaxcode->" + i_PayTaxcode);
			
		}
		
		var fromdate = request.getParameter('fromdate')
		var todate = request.getParameter('todate')
		
		//var salegrosstotal=request.getParameter('grosssale')
		//var purchasegrosstotal=request.getParameter('grosspurchase')
		//var totalsaleamount=request.getParameter('totalsaleamount')
		//var totalpurchaseamount=request.getParameter('totalpurchaseamont')
		var saletaxamount = request.getParameter('totalsaletax')
		var purchasetaxamount = request.getParameter('totalpurchasetax')
		var vatpayable = request.getParameter('vatpayable')
		var date = request.getParameter('pdate')
		var bankaccount = request.getParameter('bankaccount')
		var taxpayableaccount = request.getParameter('account')
		var taxagency = request.getParameter('taxagency')
		var memo = request.getParameter('memo')
		var chequeno = request.getParameter('cheque')
		var department = request.getParameter('department')
		var locationlist = request.getParameter('location')
		var subsidiary = request.getParameter('subsidiary')
		var classlist = request.getParameter('class')
		var subsidiary = request.getParameter('subsidiary')
		
		if (subsidiary == null || subsidiary == '' || subsidiary == 'undefined' || subsidiary == undefined) {
			subsidiary = null
		}
		
		var i_AitGlobalRecId = SearchGlobalParameter();
		nlapiLogExecution('DEBUG', 'Bill ', "i_AitGlobalRecId" + i_AitGlobalRecId);
		
		//==== CODE TO CHECK THE INDIAN SUBSIDIARY ====
		if (i_AitGlobalRecId != 0) {
			var o_AitGloRec = nlapiLoadRecord('customrecord_indian_tax_glo_parameter', i_AitGlobalRecId);
			
			a_subisidiary = o_AitGloRec.getFieldValues('custrecord_ait_subsidiary');
			
			i_vatCode = o_AitGloRec.getFieldValue('custrecord_ait_vat_code');
			
			i_taxcode = o_AitGloRec.getFieldValue('custrecord_ait_taxcode');
			
			i_PayTaxcode = o_AitGloRec.getFieldValue('custrecord_pay_servicetax');
			
			
			
			//Begin:Add Cheque record
			var chkrecord = nlapiCreateRecord('check')
			chkrecord.setFieldValue('account', bankaccount);
			chkrecord.setFieldValue('entity', taxagency);
			chkrecord.setFieldValue('trandate', date);
			chkrecord.setFieldValue('memo', memo);
			//chkrecord.setFieldValue('tranid', chequeno);
			
			if (department != null && department != 'undefined' && department != '') {
				chkrecord.setFieldValue('department', department);
			}
			
			chkrecord.setFieldValue('class', classlist)
			
			if (locationlist != null && locationlist != 'undefined' && locationlist != '') {
				chkrecord.setFieldValue('location', locationlist);
			}
			
			
			chkrecord.insertLineItem('expense', 1)
			chkrecord.setCurrentLineItemValue('expense', 'account', taxpayableaccount)
			chkrecord.setCurrentLineItemValue('expense', 'amount', vatpayable)
			if (department != null && department != 'undefined' && department != '') {
				chkrecord.setCurrentLineItemValue('expense', 'department', department)
			}
			
			if (locationlist != null && locationlist != 'undefined' && locationlist != '') {
				chkrecord.setCurrentLineItemValue('expense', 'location', locationlist);
			}
			if (classlist != null && classlist != 'undefined' && classlist != '') {
				chkrecord.setCurrentLineItemValue('expense', 'class', classlist);
			}
			chkrecord.setCurrentLineItemValue('expense', 'taxcode', i_vatCode)
			chkrecord.commitLineItem('expense')
			
			var chequeid = nlapiSubmitRecord(chkrecord, true, true);
			nlapiLogExecution('DEBUG', 'user', 'Newly Creted Check ID ' + chequeid)
			//End:Add cheque record
			
			//Begin:Add journal Entry
			var journalrecord = nlapiCreateRecord('journalentry');
			
			if (subsidiary != null || i_subcontext != false) {
				journalrecord.setFieldValue('subsidiary', subsidiary)
			}
			if (department != null && department != 'undefined' && department != '') {
				journalrecord.setFieldValue('department', department);
			}
			
			journalrecord.setFieldValue('class', classlist)
			
			if (locationlist != null && locationlist != 'undefined' && locationlist != '') {
				journalrecord.setFieldValue('location', locationlist);
			}
			journalrecord.setFieldValue('trandate', date)
			journalrecord.setLineItemValue('line', 'account', 1, taxpayableaccount)
			journalrecord.setLineItemValue('line', 'credit', 1, vatpayable)
			journalrecord.setLineItemValue('line', 'department', 1, department)
			journalrecord.setLineItemValue('line', 'class', 1, classlist)
			journalrecord.setLineItemValue('line', 'location', 1, locationlist)
			
			var count1 = request.getLineItemCount(name);
			var p = 2
			for (var i = 1; i <= count1; i++) {
				var isselect = request.getLineItemValue(name, 'apply', i)
				nlapiLogExecution('DEBUG', 'processLineItem', 'selected Item ' + isselect);
				
				if (isselect == 'T') {
					var taxcode = request.getLineItemValue(name, 'internalid', i)
					//searchBillrecord(taxcode, fromdate, todate)
					//searchInvoicerecord(taxcode, fromdate, todate)
					
					var saletaxaccount = request.getLineItemValue(name, 'saleaccount', i)
					var purchasetaxaccount = request.getLineItemValue(name, 'purchaseaccount', i)
					var saleamount = request.getLineItemValue(name, 'saletaxtotal', i)
					var purchaseamount = request.getLineItemValue(name, 'purchasetaxtotal', i)
					
					var saleaccountid = getAccountid(saletaxaccount)
					var purchaseaccountid = getAccountid(purchasetaxaccount)
					
					journalrecord.setLineItemValue('line', 'account', p, saleaccountid)
					journalrecord.setLineItemValue('line', 'debit', p, saleamount)
					journalrecord.setLineItemValue('line', 'department', p, department)
					journalrecord.setLineItemValue('line', 'class', p, classlist)
					journalrecord.setLineItemValue('line', 'location', p, locationlist)
					
					journalrecord.setLineItemValue('line', 'account', p + 1, purchaseaccountid)
					journalrecord.setLineItemValue('line', 'credit', p + 1, purchaseamount)
					journalrecord.setLineItemValue('line', 'department', p + 1, department)
					journalrecord.setLineItemValue('line', 'class', p + 1, classlist)
					journalrecord.setLineItemValue('line', 'location', p + 1, locationlist)
					p = p + 2
					
				}
			}
			
			
			var id = nlapiSubmitRecord(journalrecord, true);
			nlapiLogExecution('DEBUG', 'user', 'Newly Created journal id ' + id)
			
		}
		
		var s_taxcode = '';
		var a_taxcode = new Array();
		if (chequeid != null && chequeid != undefined && id != null && id != undefined && chequeid != 'undefined' && id != undefined) {
			var paymentrecord = nlapiCreateRecord('customrecord_vatpayment_main')
			paymentrecord.setFieldValue('custrecord_paymentvat_saletaxtotal', saletaxamount);
			paymentrecord.setFieldValue('custrecord_paymentvat_purchasetaxtotal', purchasetaxamount);
			paymentrecord.setFieldValue('custrecord_paymentvat_vatpayable', vatpayable);
			paymentrecord.setFieldValue('custrecord_paymentvat_date', date);
			paymentrecord.setFieldValue('custrecord_paymentvat_bankaccount', bankaccount);
			paymentrecord.setFieldValue('custrecord_paymentvat_taxpayableaccount', taxpayableaccount);
			paymentrecord.setFieldValue('custrecord_paymentvat_taxagency', taxagency);
			paymentrecord.setFieldValue('custrecord_paymentvat_memo', memo);
			paymentrecord.setFieldValue('custrecord_paymentvat_chequeno', chequeid);
			paymentrecord.setFieldValue('custrecord_paymentvat_journalentry', id);
			paymentrecord.setFieldValue('custrecord_paymentvat_fromdate', fromdate);
			paymentrecord.setFieldValue('custrecord_paymentvat_todate', todate);
			
			//End:To add VAT Payment main record
			
			//Begin:To add VAT Payment Child Record
			var count1 = request.getLineItemCount(name);
			
			for (var i = 1; i <= count1; i++) {
				var isselect = request.getLineItemValue(name, 'apply', i)
				
				if (isselect == 'T') {
					var taxcode = request.getLineItemValue(name, 'internalid', i)
					var taxname = request.getLineItemValue(name, 'name', i)
					var purchasetax = request.getLineItemValue(name, 'purchasetaxtotal', i)
					var saletax = request.getLineItemValue(name, 'saletaxtotal', i)
					
					s_taxcode = s_taxcode + '#' + taxcode;
					a_taxcode.push(taxcode);
					
					paymentrecord.selectNewLineItem('recmachcustrecord_paymentvat_taxpay_link')
					//paymentchildrecord.setCurrentLineItemValue('recmachcustrecord_paymentvat_taxpay_link','custrecord_paymentvat_vatpaymentno', newpaymentid);
					paymentrecord.setCurrentLineItemValue('recmachcustrecord_paymentvat_taxpay_link', 'custrecord_paymentvat_taxid', taxcode);
					paymentrecord.setCurrentLineItemValue('recmachcustrecord_paymentvat_taxpay_link', 'custrecord_paymentvat_taxname', taxname);
					paymentrecord.setCurrentLineItemValue('recmachcustrecord_paymentvat_taxpay_link', 'custrecord_paymentvat_purchasetax', purchasetax);
					paymentrecord.setCurrentLineItemValue('recmachcustrecord_paymentvat_taxpay_link', 'custrecord_paymentvat_saletax', saletax);
					paymentrecord.commitLineItem('recmachcustrecord_paymentvat_taxpay_link')
					
				}
			}
			var newpaymentid = nlapiSubmitRecord(paymentrecord, true, true);
			nlapiLogExecution('DEBUG', 'user', 'Newly Created VAT payment main record ' + newpaymentid)
			
			//==================================Begin Validate Bill record count ========================//
			
			validateTaxBillrecordCount(s_taxcode, a_taxcode, fromdate, todate, newpaymentid)
			
		//==================================End Validate Bill record count ========================//
		}
		return newpaymentid;
	}
//End:Process the Record

//Begin :function for  Populate  Account id
	function getAccountid(accountname)
	{
	    var filchk1= new Array();
		var columnsCheck1= new Array();
		filchk1.push( new nlobjSearchFilter('name',null,'contains',accountname));
		filchk1.push( new nlobjSearchFilter('isinactive',null,'is','F'));
	    columnsCheck1.push( new nlobjSearchColumn('internalid'));
 
	    var searchResultsCheck1 = nlapiSearchRecord('account', null, filchk1, columnsCheck1);
		
		if(searchResultsCheck1 != null)
		{
			var accountid=searchResultsCheck1[0].getValue('internalid')

     	    return accountid
			
		}

		  
	}
//END :function for  Populate Bank Account List

//Begin:Function to search the Bill records of particular tax code and with in given date range
	function searchBillrecord(taxcode,fromdate,todate)
	{
	   var filters = new Array();
	   filters.push( new nlobjSearchFilter('custrecord_vatbill_taxid', null, 'is',taxcode ))
	   filters.push( new nlobjSearchFilter('custrecord_vatbill_billdate', null, 'onorafter',fromdate))
	   filters.push( new nlobjSearchFilter('custrecord_vatbill_billdate', null, 'onorbefore',todate))

	   var column= new Array();
	   column.push( new nlobjSearchColumn('internalid'))

	   var results = nlapiSearchRecord('customrecord_vatbilldetail', null, filters, column);
	   if( results != null)
	   {
		   for(i=0; results != null && i < results.length; i++)
			 {
			 	var billid=results[i].getValue('internalid')
			 	nlapiLogExecution('DEBUG','user', 'Updated bill Id  '+billid)
			 	
				updateBillrecord(billid)
			 }

	   }

	}

//End::Function to search the Bill records of particular tax code and with in given date range

//Begin:Function to Update the Bill realation record
function updateBillrecord(billid)
{
	var record = nlapiLoadRecord('customrecord_vatbilldetail', billid);
  	record.setFieldValue('custrecord_vatbill_status', 'Close');
  	var updatedId = nlapiSubmitRecord(record, true);
  	nlapiLogExecution('DEBUG','user', 'updated bill '+updatedId)
}
//End:Function to Update the Bill realation record


//Begin:Function to search the Invoice records of particular tax code and with in given date range

function searchInvoicerecord(taxcode,fromdate,todate)
{
   var filters = new Array();
   filters.push( new nlobjSearchFilter('custrecord_vatinvoice_taxid', null, 'is',taxcode ))
   filters.push( new nlobjSearchFilter('custrecord_vatinvoice_date', null, 'onorafter',fromdate))
   filters.push( new nlobjSearchFilter('custrecord_vatinvoice_date', null, 'onorbefore',todate))

   var column= new Array();
   column.push( new nlobjSearchColumn('internalid'))

   var results = nlapiSearchRecord('customrecord_vatinvoicedetail', null, filters, column);
   if( results != null)
   {
	   for(i=0; results != null && i < results.length; i++)
		 {
		 	var invoiceid=results[i].getValue('internalid')
		 	updateInvoicerecord(invoiceid)
		 }
   }

}

//End:Function to search the Invoice records of particular tax code and with in given date range

//Begin:Function to Update the Invoice realation record
function updateInvoicerecord(invoiceid)
{
	var record = nlapiLoadRecord('customrecord_vatinvoicedetail', invoiceid);
  	record.setFieldValue('custrecord_vatinvoice_status', 'Close');
  	var updatedId = nlapiSubmitRecord(record, true);
  	nlapiLogExecution('DEBUG','user', 'updated invoice '+updatedId)
}
//End:Function to Update the Invoice relation record


//Begin:Function to get the global parameter
function SearchGlobalParameter()
{
	
	// == GET TDS GLOBAL PARAMETER ====
	var a_filters = new Array();
	var a_column = new Array();
	var i_globalRecId = 0;
	
    a_column.push(new nlobjSearchColumn('internalid'));
	
	var s_serchResult = nlapiSearchRecord('customrecord_indian_tax_glo_parameter',null,null,a_column)
	
	if(s_serchResult != null && s_serchResult != undefined && s_serchResult != '')
	{
		for (var i=0; i<s_serchResult.length && s_serchResult != null; i++) 	
		{
			i_globalRecId = s_serchResult[0].getValue('internalid');
			nlapiLogExecution('DEBUG','Bill ', "i_globalRecId"+i_globalRecId);
		
		}
		
	}
	
	
	return i_globalRecId;
}

function popluatlocation(request, response, taxagency, subsidiary)	
{
    var ved_filchk1 = new Array();
    var ved_columnsCheck1 = new Array();
	
	
    if(subsidiary != null && subsidiary != '' && subsidiary != undefined && subsidiary != 'undefined')
	{
		ved_filchk1.push( new nlobjSearchFilter('subsidiary', null, 'anyOf', subsidiary));
	}
    
    
    ved_columnsCheck1.push( new nlobjSearchColumn('internalid'));
    ved_columnsCheck1.push( new nlobjSearchColumn('name'));
    
    var ved_searchResultsCheck1 = nlapiSearchRecord('location', null, null, ved_columnsCheck1);
    if (ved_searchResultsCheck1 != null) 	
	{
        for (i = 0; ved_searchResultsCheck1 != null && i < ved_searchResultsCheck1.length; i++) 	
		{
		   taxagency.addSelectOption(ved_searchResultsCheck1[i].getValue('internalid'), ved_searchResultsCheck1[i].getValue('name'));
            
        }
    }
    
}
//End:Function to get the global parameter



function popluatDepartment(request, response, taxagency, subsidiary)	
{
    var ved_filchk1 = new Array();
    var ved_columnsCheck1 = new Array();
    
	if(subsidiary != null && subsidiary != '' && subsidiary != undefined && subsidiary != 'undefined')
	{
		ved_filchk1.push( new nlobjSearchFilter('subsidiary', null, 'anyOf', subsidiary));
	}
    
    
    ved_columnsCheck1.push( new nlobjSearchColumn('internalid'));
    ved_columnsCheck1.push( new nlobjSearchColumn('name'));
    												
    var ved_searchResultsCheck1 = nlapiSearchRecord('department', null, ved_filchk1, ved_columnsCheck1);
    if (ved_searchResultsCheck1 != null) 	
	{
		for (i = 0; ved_searchResultsCheck1 != null && i < ved_searchResultsCheck1.length; i++) 	
		{
			taxagency.addSelectOption(ved_searchResultsCheck1[i].getValue('internalid'), ved_searchResultsCheck1[i].getValue('name'));
            
        }
    }
    
}

}
//Begin :function for  Populate Bank Account List
function populatepaybleaccount(request, response,alist , a_subisidiary)
{
	
				
			//==== SEARCH BANK ACCOUNT FOR SUBSIDIARY ====
			
			var filchk1= new Array();
			var columnsCheck1= new Array();
 			
			if(a_subisidiary != null && a_subisidiary != '' && a_subisidiary != undefined && a_subisidiary != 'undefined')
			{
				filchk1.push(new nlobjSearchFilter('subsidiary',null,'anyOf',a_subisidiary));
			}
			
		    columnsCheck1.push( new nlobjSearchColumn('name'));
		    columnsCheck1.push( new nlobjSearchColumn('internalid'));
	
		    var searchResultsCheck1 = nlapiSearchRecord('account', null, filchk1, columnsCheck1);
		    for(i=0; searchResultsCheck1 != null && i < searchResultsCheck1.length; i++)
			 {
				alist.addSelectOption(searchResultsCheck1[i].getValue('internalid'),
				searchResultsCheck1[i].getValue('name'), false);
	
			 }// END   for(i=0; i < searchResultsCheck1 != null && i < searchResultsCheck1.length; i++)
			 
				
   

}
//END :function for  Populate Bank Account List

function validateTaxBillrecordCount(s_taxcode, a_taxcode, fromdate, todate,newpaymentid)
{

	var totalcount = 0;
	
	 var filters = new Array();
	 filters.push( new nlobjSearchFilter('custrecord_vatbill_taxid', null, 'anyOf',a_taxcode ))
	 filters.push( new nlobjSearchFilter('custrecord_vatbill_billdate', null, 'onorafter',fromdate))
	 filters.push( new nlobjSearchFilter('custrecord_vatbill_billdate', null, 'onorbefore',todate))
	 filters.push( new nlobjSearchFilter('custrecord_vatbill_status', null, 'is','Open'))
	 
	 var column= new Array();
	 column.push( new nlobjSearchColumn('internalid'))

	var results = nlapiSearchRecord('customrecord_vatbilldetail', null, filters, column);
	if (results != null) 
	{
		totalcount = results.length
		nlapiLogExecution('DEBUG', 'user', 'Bill count ' + totalcount)
	}
	
	var invfilters = new Array();
	invfilters.push(new nlobjSearchFilter('custrecord_vatinvoice_taxid', null, 'anyOf', a_taxcode))
	invfilters.push(new nlobjSearchFilter('custrecord_vatinvoice_date', null, 'onorafter', fromdate))
	invfilters.push(new nlobjSearchFilter('custrecord_vatinvoice_date', null, 'onorbefore', todate))
	invfilters.push( new nlobjSearchFilter('custrecord_vatinvoice_status', null, 'is','Open'))
	
	var invcolumn = new Array();
	invcolumn.push(new nlobjSearchColumn('internalid'))
	
	var invresults = nlapiSearchRecord('customrecord_vatinvoicedetail', null, invfilters, invcolumn);
	if (invresults != null) 
	{
	
		var TotalInvoiceCount = invresults.length
		nlapiLogExecution('DEBUG', 'user', 'TotalInvoiceCount ' + TotalInvoiceCount)
		totalcount = parseInt(totalcount) + parseInt(TotalInvoiceCount)
		nlapiLogExecution('DEBUG', 'user', 'Total invoice Bill count ' + totalcount)
	}
	
	if (parseInt(totalcount) < parseInt(350)) 
	{
	
		if (results != null && results != '' && results != undefined) 
		{
			for (var l = 0; l < results.length; l++) 
			{
				var billid = results[l].getValue('internalid')
				nlapiLogExecution('DEBUG', 'user', 'Updated bill Id  ' + billid)
				
				var fields = new Array();
				var values = new Array();
				fields[0] = 'custrecord_vatbill_status';
				values[0] = "Close";
				fields[1] = 'custrecord_vatbill_tax_payment';
				values[1] = newpaymentid
				nlapiSubmitField('customrecord_vatbilldetail', billid,fields,values)
				
			}
			
		}
		if (invresults != null && invresults != '' && invresults != undefined) 
		{
			for (var m = 0; m < invresults.length; m++) 
			{
				var invoiceid = invresults[m].getValue('internalid')
				nlapiLogExecution('DEBUG', 'user', 'invoiceid ' + invoiceid)
				
				var fields = new Array();
				var values = new Array();
				fields[0] = 'custrecord_vatinvoice_status';
				values[0] = "Close";
				fields[1] = 'custrecord_vatinvoice_taxpayment';
				values[1] = newpaymentid
				
				nlapiSubmitField('customrecord_vatinvoicedetail', invoiceid,fields,values)
			}
			
		}
		
	}
	else 
	{
		var params = new Array();
		params['custscript_ait_pay_taxcode'] = s_taxcode;
		params['custscript_ait_taxpay_fromdate'] = fromdate;
		params['custscript_ait_taxpay_todate'] = todate;
		params['custscript_ait_taxpay_paymentid'] = newpaymentid;
		
		var status = nlapiScheduleScript('customscript_ait_close_tax_bill_detail', null, params);
		nlapiLogExecution('DEBUG', 'Script Scheduled ', ' Script Status -->' + status);
	}
}
// END OBJECT CALLED/INVOKING FUNCTION =====================================================
