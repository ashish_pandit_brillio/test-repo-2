/**
 * @NApiVersion 2.x
 * @NScriptType Restlet
 * @NModuleScope SameAccount 
 */
/*
 * Created By : Shravan K 
 * Date : 07-may-2021
 * Purpose : API to provide Resource allocation summary to OTG Team
 */
define(['N/record', 'N/search','N/runtime'],
function(obj_Record, obj_Search,obj_Runtime) 
{
    function Post_OTG_Data(requestBody)
    {
    	try
    	{
    		log.debug('requestBody ==',JSON.stringify(requestBody));
    		var obj_Usage_Context = obj_Runtime.getCurrentScript();
    		var s_Request_Type = requestBody.RequestType;
    		log.debug('s_Request_Type ==',s_Request_Type);
    		if((!s_Request_Type) || (s_Request_Type == null))
    			{
    				var obj_Response_Return = new Response();
    				obj_Response_Return.Data ='Please Enter Request Body';
    				obj_Response_Return.Status = false;
    				return obj_Response_Return;	
    			} //// End of if((!s_Request_Type) || (s_Request_Type == null))
    		if(s_Request_Type == 'GETONE')
    			{
    				var s_User_Email =  requestBody.EmailId;
    				log.debug('s_User_Email ==',s_User_Email);
    				if(!s_User_Email)
    					{
    						var obj_Response_Return = new Response();
    						obj_Response_Return.Data ='Please Enter Email Id';
    						obj_Response_Return.Status = false;
    						return obj_Response_Return;
    					} //// if(!s_User_Email)
    				var i_Employee_Internal_Id = Get_Internal_Id_Employee(s_User_Email,obj_Search);
    				log.debug('i_Employee_Internal_Id ==',i_Employee_Internal_Id);
    				var obj_Response_Return = fu_Send_Indivual_Entity_Data(i_Employee_Internal_Id,obj_Search);
    				
    			} //// if(s_Request_Type == 'GETONE')
    		else if(s_Request_Type == 'GETALL')
    			{
    				var obj_Response_Return = {};
    				obj_Response_Return = fu_Send_Entire_Entity_Data(obj_Search);
    			} /// if(s_Request_Type == 'GETALL')
    		else
    			{
    				var obj_Response_Return = new Response();
    				obj_Response_Return.Data = 'Request Type violation';
    				obj_Response_Return.Status = false;
    			} //// final else
    	}//// End of try
    	catch(s_Exception)
    	{
    		var obj_Response_Return = new Response();
    		log.debug('s_Exception in Post_OTG_Data ==',s_Exception);
    		obj_Response_Return.Data = s_Exception;
    		obj_Response_Return.Status = false;
    	}//// End of catch
    	log.debug('obj_Response_Return==', JSON.stringify(obj_Response_Return));
    	log.debug('Final Remaining Usage  ==',obj_Usage_Context.getRemainingUsage());
    	return obj_Response_Return;	
    } ///// End of  function Post_OTG_Data(requestBody)
    return {
        post: Post_OTG_Data
    };
    
}); ////// End of Main Function 
/************************ Supporting Functions ****************************/
/******************************0 *************************************/
function Response() 
{
	this.Status = false;
  	//this.TimeStamp = "";
	this.Data = "";
  
} //// function Response()  
/******************************* 0 *************************************/
/********************************* 1 **********************************/ 
function Get_Internal_Id_Employee(s_Email,obj_Search)
{
	try
	{
		
		var obj_Emp_Search = obj_Search.create({
			   type: "employee",
			   filters:
			   [
			      ["email","is",s_Email],
			      "AND", 
			      ["custentity_employee_inactive","is","F"], 
			      "AND", 
			      ["custentity_implementationteam","is","F"]
			   ],
			   columns:
			   [
			      obj_Search.createColumn({name: "internalid", label: "Internal ID"})
			   ]
			});
			var i_Result_Count = obj_Emp_Search.runPaged().count;
			if(i_Result_Count > 0)
				{
					var i_Emp_Int_Id;
					obj_Emp_Search.run().each(function(result)
					{	
						i_Emp_Int_Id =  result.getValue({ name: "internalid", label: "Internal ID" });
						return true
					});
					return i_Emp_Int_Id;
				} //// if(obj_Emp_Search)
			else
				{
					throw 'User Doesnot Exist!!'
				} //// else of //// if(obj_Emp_Search)
		
	} //// End of Try
	catch(s_Exception)
	{
		log.debug('Exception in Get_Internal_Id_Employee ==', s_Exception )
		throw s_Exception;
	} ///// end of catch
} /// End of  function Get_Internal_Id_Employee(s_Email)
/********************************* 1 **********************************/ 

/************************** 2 *****************************************/
function fu_Send_Indivual_Entity_Data(i_Employee_Internal_Id,obj_Search)
{
	try
	{
		var obj_Response_Final = new Response();
		var obj_getStatusofOneRA_Response = {};
		var obj_getStatusofOneRA_Response = {};
		var arr_RA_Lines=[];
		var obj_Emp_Search = obj_Search.create({
			   type: "employee",
			   filters:
			   [
			      ["internalid","anyof",i_Employee_Internal_Id]
			   ],
			   columns:
			   [
			      obj_Search.createColumn({name: "custentity_fusion_empid", label: "Fusion Employee Id"}),
			      obj_Search.createColumn({name: "firstname", label: "First Name"}),
			      obj_Search.createColumn({name: "middlename", label: "Middle Name"}),
			      obj_Search.createColumn({name: "lastname", label: "Last Name"}),
			      obj_Search.createColumn({name: "email", label: "Email"}),
			      obj_Search.createColumn({name: "department", label: "Practice"}),
			      obj_Search.createColumn({name: "departmentnohierarchy", label: "Practice (no hierarchy)"}),
			      obj_Search.createColumn({name: "custentity_reportingmanager", label: "Reporting Manager"}),
			      obj_Search.createColumn({name: "email",join: "CUSTENTITY_REPORTINGMANAGER",label: "Email"})
			   ]
			});
			var i_Emp_Search_Count = obj_Emp_Search.runPaged().count;
			log.debug("i_Emp_Search_Count==",i_Emp_Search_Count);
			var obj_RA_Json_Body = {};
			obj_Emp_Search.run().each(function(result)
			{
				obj_RA_Json_Body = {
						  personNumber: result.getValue({name: "custentity_fusion_empid",label: "Fusion Employee Id"}) ,
					      firstName:result.getValue({name: "firstname",label: "First Name"}),
					      lastName:result.getValue({name: "lastname", label: "Last Name"}) ,
					      middleName:result.getValue({name: "middlename",label: "Middle Name"}),
					      workEmail: result.getValue({name: "email", label: "Email"}) ,
					      department: result.getText({name: "departmentnohierarchy",label: "Practice (no hierarchy)"})  ,
					      subDepartment: result.getText({name: "department", label: "Practice"})  ,
					      reportingManager:result.getText({name: "custentity_reportingmanager",label: "Reporting Manager"}) ,
					      reportingManagerEmail:result.getValue({name: "email",join: "CUSTENTITY_REPORTINGMANAGER",label: "Email"})												
				}

			   return true;
			}); /// End obj_Emp_Search.run().each(function(result)
			
			var obj_Resource_Allocation_Search = obj_Search.create({
				   type: "resourceallocation",
				   filters:
				   [
				      ["resource.internalid","anyof",i_Employee_Internal_Id],
				      "AND",
				      ["enddate","onorafter","today"]
				   ],
				   columns:
				   [
				      obj_Search.createColumn({name: "company", label: "Project"}),
				      obj_Search.createColumn({name: "percentoftime", label: "Percentage of Time"}),
				      obj_Search.createColumn({name: "startdate", label: "Start Date"}),
				      obj_Search.createColumn({name: "enddate", label: "End Date"}),
				      obj_Search.createColumn({name: "custentity_projectmanager",join: "job",label: "Project Manager"}),
				      obj_Search.createColumn({name: "custentity_deliverymanager",join: "job",label: "Delivery Manager"}),
				      obj_Search.createColumn({name: "custentity_vertical",join: "customer",label: "Vertical" }),
				      obj_Search.createColumn({name: "customer", label: "Customer"}),
				      obj_Search.createColumn({name: "territory",join: "customer",label: "Territory"}),
				      obj_Search.createColumn({name: "altname",join: "customer",label: "Name"})
				   ]
				});
			obj_Resource_Allocation_Search.run().each(function(result)
					{	
						var i_Proj_Manager_Internal_Id = result.getValue({name: "custentity_projectmanager",join: "job",label: "Project Manager"});
						var s_Proj_manager_Email = '';
						if(i_Proj_Manager_Internal_Id)
							{
								var obj_Proj_Manager_Email = obj_Search.lookupFields({
							    type: obj_Search.Type.EMPLOYEE,
							    id:i_Proj_Manager_Internal_Id ,
							    columns: ['email']
								});
								s_Proj_manager_Email = obj_Proj_Manager_Email.email;
							} ///// if(i_Proj_Manager_Internal_Id)
						var i_Delivery_Manager_Internal_Id = result.getValue({name: "custentity_deliverymanager",join: "job",label: "Delivery Manager"});
						var s_Delivery_Manager_Email = '';
						if(i_Delivery_Manager_Internal_Id)
							{
								var obj_Delivery_Manager_Email = obj_Search.lookupFields({
							    type: obj_Search.Type.EMPLOYEE,
							    id:i_Delivery_Manager_Internal_Id ,
							    columns: ['email']
								});
								s_Delivery_Manager_Email = obj_Delivery_Manager_Email.email;
							} //// if(i_Delivery_Manager_Internal_Id)
					
						obj_RA_JSON_Line = {
								  projectName:result.getText({name: "company", label: "Project"}) ,
						          allocationPercentage: result.getValue({name: "percentoftime", label: "Percentage of Time"}),
						          allocationStartDate: result.getValue({name: "startdate", label: "Start Date"}),
						          allocationEndDate: result.getValue({name: "enddate", label: "End Date"}),
						          projectManager: result.getText({name: "custentity_projectmanager",join: "job",label: "Project Manager"}),
						          projectManagerEmail:s_Proj_manager_Email,
						          deliveryManager: result.getText({name: "custentity_deliverymanager",join: "job",label: "Delivery Manager"}),
						          deliveryManagerEmail: s_Delivery_Manager_Email,
						          customerVertical: result.getText({name: "custentity_vertical",join: "customer",label: "Vertical" }),
						          customerName:result.getValue({name: "altname",join: "customer",label: "Name"}),
						          customerTerritory: result.getText({name: "territory",join: "customer",label: "Territory"})		
						}
						arr_RA_Lines.push(obj_RA_JSON_Line);
						return true;
					}); //// End of obj_Resource_Allocation_Search.run().each(function(result)
			obj_getStatusofOneRA_Response={
					EmployeeDetails : obj_RA_Json_Body,
					Projects :arr_RA_Lines 
			}
			obj_Response_Final.Data = obj_getStatusofOneRA_Response;
			obj_Response_Final.Status = true;
			return obj_Response_Final;
	} ///// End of try
	catch(s_Exception)
	{
		log.debug('Exception in fu_Send_Indivual_Entity_Data=== ',s_Exception);
		obj_Response_Final.Data = s_Exception;
		obj_Response_Final.Status = false;
		return obj_Response_Final;
	} //// catch(s_Exception)

} //// End of fu_Send_Indivual_Entity_Data()
/************************** 2 *****************************************/
/******************** 3 ************************************************/
function fu_Send_Entire_Entity_Data(obj_Search)
{
	try
	{
		var obj_Response_Final = new Response();
		
		var obj_Emp_Search = obj_Search.create({
			   type: "employee",
			   filters:
			   [
			      ["custentity_employee_inactive","is","F"], 
			    	"AND", 
			      ["custentity_implementationteam","is","F"]
			   ],
			   columns:
			   [
			      obj_Search.createColumn({name: "custentity_fusion_empid", label: "Fusion Employee Id"}),
			      obj_Search.createColumn({name: "firstname", label: "First Name"}),
			      obj_Search.createColumn({name: "middlename", label: "Middle Name"}),
			      obj_Search.createColumn({name: "lastname", label: "Last Name"}),
			      obj_Search.createColumn({name: "email", label: "Email"}),
			      obj_Search.createColumn({name: "department", label: "Practice"}),
			      obj_Search.createColumn({name: "departmentnohierarchy", label: "Practice (no hierarchy)"}),
			      obj_Search.createColumn({name: "custentity_reportingmanager", label: "Reporting Manager"}),
			      obj_Search.createColumn({name: "internalid", label: "Internal ID"}),
			      obj_Search.createColumn({name: "email",join: "CUSTENTITY_REPORTINGMANAGER",label: "Email"})
			   ]
			});
			var i_Emp_Search_Count = obj_Emp_Search.runPaged().count;
			log.debug("i_Emp_Search_Count==",i_Emp_Search_Count);
			var i_Employee_Internal_ID ;
			var arr_Emp_Int_ID = [];
			var arr_Emp_Fields_Details = [];
			var obj_RA_Json_Body = {};
			var arr_Emp_Emails = [];
			var obj_Emp_Search_result = getAllResults(obj_Emp_Search);
			obj_Emp_Search_result.forEach(function(result)
			{
				i_Employee_Internal_ID = result.getValue({name: "internalid", label: "Internal ID"});
				obj_RA_Json_Body = {};
				obj_RA_Json_Body = {
						  personNumber: result.getValue({name: "custentity_fusion_empid",label: "Fusion Employee Id"}) ,
					      firstName:result.getValue({name: "firstname",label: "First Name"}),
					      lastName:result.getValue({name: "lastname", label: "Last Name"}) ,
					      middleName:result.getValue({name: "middlename",label: "Middle Name"}),
					      workEmail: result.getValue({name: "email", label: "Email"}) ,
					      department: result.getText({name: "departmentnohierarchy",label: "Practice (no hierarchy)"})  ,
					      subDepartment: result.getText({name: "department", label: "Practice"})  ,
					      reportingManager:result.getText({name: "custentity_reportingmanager",label: "Reporting Manager"}) ,
					      reportingManagerEmail:result.getValue({name: "email",join: "CUSTENTITY_REPORTINGMANAGER",label: "Email"})												
				};
				arr_Emp_Int_ID.push(i_Employee_Internal_ID);
				arr_Emp_Emails.push(result.getValue({name: "email", label: "Email"})) 
				arr_Emp_Fields_Details.push(obj_RA_Json_Body);
			   return true;
			}); /// End obj_Emp_Search.run().each(function(result)
			log.debug("arr_Emp_Int_ID.length==",arr_Emp_Int_ID.length);

			
		var arr_RA_Lines = [];
		var obj_RA_JSON_Line = {};
		
		var obj_getStatusofOneRA_Response = {};
		var obj_Resource_Allocation_Search = obj_Search.create({
			   type: "resourceallocation",
			   filters:
			   [
			      ["enddate","onorafter","today"]
			   ],
			   columns:
			   [
			      obj_Search.createColumn({name: "firstname",join: "employee",label: "First Name"}),
			      obj_Search.createColumn({name: "custentity_reportingmanager",join: "employee",label: "Reporting Manager"}),
			      obj_Search.createColumn({name: "department",join: "employee", label: "Practice"}),
			      obj_Search.createColumn({name: "departmentnohierarchy",join: "employee",label: "Practice (no hierarchy)"}),
			      obj_Search.createColumn({name: "email",join: "employee", label: "Email"}),
			      obj_Search.createColumn({name: "lastname",join: "employee", label: "Last Name"}),
			      obj_Search.createColumn({name: "middlename", join: "employee",label: "Middle Name"}),
			      obj_Search.createColumn({name: "custentity_fusion_empid",join: "employee",  sort: obj_Search.Sort.ASC,label: "Fusion Employee Id"}),
			      obj_Search.createColumn({name: "company", label: "Project"}),
			      obj_Search.createColumn({name: "percentoftime", label: "Percentage of Time"}),
			      obj_Search.createColumn({name: "startdate", label: "Start Date"}),
			      obj_Search.createColumn({name: "enddate", label: "End Date"}),
			      obj_Search.createColumn({name: "custentity_projectmanager",join: "job",label: "Project Manager"}),
			      obj_Search.createColumn({name: "custentity_deliverymanager",join: "job",label: "Delivery Manager"}),
			      obj_Search.createColumn({name: "custentity_vertical",join: "customer",label: "Vertical" }),
			      obj_Search.createColumn({name: "customer", label: "Customer"}),
			      obj_Search.createColumn({name: "territory",join: "customer",label: "Territory"}),
			      obj_Search.createColumn({name: "altname",join: "customer",label: "Name"}),
			      obj_Search.createColumn({name: "internalid",join: "resource",label: "Internal ID" })
			   ]
			});
		var i_Initial_Search_Count  = obj_Resource_Allocation_Search.runPaged().count;
		log.debug('i_Initial_Search_Count==',i_Initial_Search_Count);
		
		if(i_Initial_Search_Count >= 1000)
			{
				/****** Below code is taken from SFDC script which has access to more than 1000 resut set ***/
				var results = [];
				var searchid = 0;
				do {
				var resultslice = obj_Resource_Allocation_Search.run().getRange({
					start: searchid,
					end: searchid + 1000
				});
				for (var rs in resultslice) 
				{
					results.push(resultslice[rs]);
					searchid++;
				}
			} while (resultslice.length >= 1000);
			/****** Above code is taken from SFDC script which has access to more than 1000 result set ***/
			} ///// if(i_Initial_Search_Count > 1000)
		else
			{
				var obj_Results = obj_Resource_Allocation_Search.run();
				 var results =  obj_Results.getRange({
			        start: 0,
			        end: parseInt(i_Initial_Search_Count)
			    });
				 
			} //// else of ///// if(i_Initial_Search_Count > 1000)
		
		var obj_RA_Json_Body = {};
		var obj_RA_JSON_Line = {};
		var arr_RA_Lines = [];
		
		var arr_RA_All_Data = [];
		var i_Emp_internal_Id_RA ;
		var arr_Emp_int_id_RA = [];
		var i_Loop_Total_Count = results.length;
		log.debug('i_Loop_Total_Count==',i_Loop_Total_Count);
		var s_Delivery_Manager_Email ;
		var s_Project_Manager_Email;
		var i_Proj_manager_id;
		var i_Delivery_manager_id ;
		for(var i_Index_loop = 0 ;i_Index_loop < i_Loop_Total_Count ;i_Index_loop++)
		{
			s_Delivery_Manager_Email = '';
			s_Project_Manager_Email = '';
			if(i_Index_loop == 0) //// first go fetch emp as well as project details
				{
						i_Emp_internal_Id_RA = results[i_Index_loop].getValue({name: "internalid",join: "resource",label: "Internal ID" });
						i_Proj_manager_id = results[i_Index_loop].getValue({name: "custentity_projectmanager",join: "job",label: "Project Manager"});
						if(i_Proj_manager_id)
							{
								i_Index_Array = arr_Emp_Int_ID.indexOf(i_Proj_manager_id)
								if(i_Index_Array >= 0)
									s_Project_Manager_Email = arr_Emp_Emails[i_Index_Array];
							} ////// if(i_Proj_manager_id)
						i_Delivery_manager_id = results[i_Index_loop].getValue({name: "custentity_deliverymanager",join: "job",label: "Project Manager"});
						if(i_Delivery_manager_id)
							{
								i_Index_Array = arr_Emp_Int_ID.indexOf(i_Delivery_manager_id)
								if(i_Index_Array >= 0)
									s_Delivery_Manager_Email = arr_Emp_Emails[i_Index_Array];
							} ////// if(i_Delivery_manager_id)
						
							obj_RA_JSON_Line = {
									  projectName:results[i_Index_loop].getText({name: "company", label: "Project"}) ,
							          allocationPercentage: results[i_Index_loop].getValue({name: "percentoftime", label: "Percentage of Time"}),
							          allocationStartDate: results[i_Index_loop].getValue({name: "startdate", label: "Start Date"}),
							          allocationEndDate: results[i_Index_loop].getValue({name: "enddate", label: "End Date"}),
							          projectManager: results[i_Index_loop].getText({name: "custentity_projectmanager",join: "job",label: "Project Manager"}),
							          projectManagerEmail:s_Project_Manager_Email,
							          deliveryManager: results[i_Index_loop].getText({name: "custentity_deliverymanager",join: "job",label: "Delivery Manager"}),
							          deliveryManagerEmail:s_Delivery_Manager_Email,
							          customerVertical: results[i_Index_loop].getText({name: "custentity_vertical",join: "customer",label: "Vertical" }),
							          customerName:results[i_Index_loop].getValue({name: "altname",join: "customer",label: "Name"}),
							          customerTerritory: results[i_Index_loop].getText({name: "territory",join: "customer",label: "Territory"})	
							}
							arr_RA_Lines.push(obj_RA_JSON_Line);
						} ////// if(i_Index_loop == 0)
			else
				{
						
						var i_Current_Emp_Number  = results[i_Index_loop].getValue({name: "custentity_fusion_empid",join: "employee",label: "Fusion Employee Id"});
						var i_Previous_Emp_Number = results[i_Index_loop - 1].getValue({name: "custentity_fusion_empid",join: "employee",label: "Fusion Employee Id"});
					if(parseInt(i_Current_Emp_Number) == parseInt(i_Previous_Emp_Number)) /// if same employee then no need of any Emp data
							{
						
						i_Proj_manager_id = results[i_Index_loop].getValue({name: "custentity_projectmanager",join: "job",label: "Project Manager"});
						if(i_Proj_manager_id)
							{
								i_Index_Array = arr_Emp_Int_ID.indexOf(i_Proj_manager_id)
								if(i_Index_Array >= 0)
									s_Project_Manager_Email = arr_Emp_Emails[i_Index_Array];
							} ////// if(i_Proj_manager_id)
						i_Delivery_manager_id = results[i_Index_loop].getValue({name: "custentity_deliverymanager",join: "job",label: "Project Manager"});
						if(i_Delivery_manager_id)
							{
								i_Index_Array = arr_Emp_Int_ID.indexOf(i_Delivery_manager_id)
								if(i_Index_Array >= 0)
									s_Delivery_Manager_Email = arr_Emp_Emails[i_Index_Array];
							} ////// if(i_Delivery_manager_id)
							obj_RA_JSON_Line = {
									  projectName:results[i_Index_loop].getText({name: "company", label: "Project"}) ,
							          allocationPercentage: results[i_Index_loop].getValue({name: "percentoftime", label: "Percentage of Time"}),
							          allocationStartDate: results[i_Index_loop].getValue({name: "startdate", label: "Start Date"}),
							          allocationEndDate: results[i_Index_loop].getValue({name: "enddate", label: "End Date"}),
							          projectManager: results[i_Index_loop].getText({name: "custentity_projectmanager",join: "job",label: "Project Manager"}),
							          projectManagerEmail: s_Project_Manager_Email,
							          deliveryManager: results[i_Index_loop].getText({name: "custentity_deliverymanager",join: "job",label: "Delivery Manager"}),
							          deliveryManagerEmail:s_Delivery_Manager_Email,
							          customerVertical: results[i_Index_loop].getText({name: "custentity_vertical",join: "customer",label: "Vertical" }),
							          customerName:results[i_Index_loop].getValue({name: "altname",join: "customer",label: "Name"}),
							          customerTerritory: results[i_Index_loop].getText({name: "territory",join: "customer",label: "Territory"})	
							}
							arr_RA_Lines.push(obj_RA_JSON_Line);
							} //// if(parseInt(i_Current_Emp_Number) == parseInt(i_Current_Emp_Number))
						else /// if new Emp arrives, then take all data
							{	
							arr_RA_All_Data.push(arr_RA_Lines);
							arr_Emp_int_id_RA.push(i_Emp_internal_Id_RA);
								arr_RA_Lines = [];
								i_Emp_internal_Id_RA = results[i_Index_loop].getValue({name: "internalid",join: "resource",label: "Internal ID" });
								i_Proj_manager_id = results[i_Index_loop].getValue({name: "custentity_projectmanager",join: "job",label: "Project Manager"});
								if(i_Proj_manager_id)
									{
										i_Index_Array = arr_Emp_Int_ID.indexOf(i_Proj_manager_id)
										if(i_Index_Array >= 0)
											s_Project_Manager_Email = arr_Emp_Emails[i_Index_Array];
									} ////// if(i_Proj_manager_id)
								i_Delivery_manager_id = results[i_Index_loop].getValue({name: "custentity_deliverymanager",join: "job",label: "Project Manager"});
								if(i_Delivery_manager_id)
									{
										i_Index_Array = arr_Emp_Int_ID.indexOf(i_Delivery_manager_id)
										if(i_Index_Array >= 0)
											s_Delivery_Manager_Email = arr_Emp_Emails[i_Index_Array];
									} ////// if(i_Delivery_manager_id)
								
								obj_RA_JSON_Line = {
										  projectName:results[i_Index_loop].getText({name: "company", label: "Project"}) ,
								          allocationPercentage: results[i_Index_loop].getValue({name: "percentoftime", label: "Percentage of Time"}),
								          allocationStartDate: results[i_Index_loop].getValue({name: "startdate", label: "Start Date"}),
								          allocationEndDate: results[i_Index_loop].getValue({name: "enddate", label: "End Date"}),
								          projectManager: results[i_Index_loop].getText({name: "custentity_projectmanager",join: "job",label: "Project Manager"}),
								          projectManagerEmail:s_Project_Manager_Email,
								          deliveryManager: results[i_Index_loop].getText({name: "custentity_deliverymanager",join: "job",label: "Delivery Manager"}),
								          deliveryManagerEmail:s_Delivery_Manager_Email,
								          customerVertical: results[i_Index_loop].getText({name: "custentity_vertical",join: "customer",label: "Vertical" }),
								          customerName:results[i_Index_loop].getValue({name: "altname",join: "customer",label: "Name"}),
								          customerTerritory: results[i_Index_loop].getText({name: "territory",join: "customer",label: "Territory"})	
								}
								arr_RA_Lines.push(obj_RA_JSON_Line);
						} ///// else of if if(parseInt(i_Current_Emp_Number) == parseInt(i_Current_Emp_Number))
				} ///// Else of if if(i_Index_loop == 0)
		} //// for(var i_Index_loop =0;i_Index_loop <results.length;i_Index_loop++)
		arr_RA_All_Data.push(arr_RA_Lines);	
		arr_Emp_int_id_RA.push(i_Emp_internal_Id_RA);
		log.debug("arr_RA_All_Data.length==",arr_RA_All_Data.length);
		log.debug("arr_Emp_int_id_RA.length==",arr_Emp_int_id_RA.length);
		log.debug('arr_Emp_int_id_RA[0]==', JSON.stringify(arr_Emp_int_id_RA));
		var obj_getStatusofOneRA_Response = {};
		var arr_Final_Response = [];
		for(var i_Final_index = 0; i_Final_index < arr_Emp_Int_ID.length ; i_Final_index++ )
			{
				var i_ref_Array_Index = arr_Emp_int_id_RA.indexOf(arr_Emp_Int_ID[i_Final_index]);
				if(i_ref_Array_Index >= 0)
					{
						obj_getStatusofOneRA_Response={
							EmployeeDetails : arr_Emp_Fields_Details[i_Final_index],
							Projects : arr_RA_All_Data[i_ref_Array_Index]
						};
						
						
					} ///// if(i_ref_Array_Index >= 0)
				else
				{
					obj_getStatusofOneRA_Response={
							EmployeeDetails : arr_Emp_Fields_Details[i_Final_index],
							Projects : ''
						};
					 	
				} ////else //// if(i_ref_Array_Index >= 0)
				arr_Final_Response.push(obj_getStatusofOneRA_Response);	
			} ///// for(var i_Final_index = 0; i_Final_index < arr_Emp_Int_ID.length ; i_Final_index++ )

		obj_Response_Final.Data = arr_Final_Response;
		obj_Response_Final.Status = true;
		return obj_Response_Final;
	} ///// End of try
	catch(s_Exception)
	{
		log.debug('Exception in fu_Send_Entire_Entity_Data',s_Exception);
		obj_Response_Final.Data = s_Exception;
		obj_Response_Final.Status = false;
		return obj_Response_Final;
	} //// End of catch

} ///// End of function fu_Send_Entire_Entity_Data(obj_Search)
/******************** 3 ************************************************/
/******************* 4 **************************************/
// Third party free source code to fetch results more than 1000 
function getAllResults(s) {
    var results = s.run();
    var searchResults = [];
    var searchid = 0;
    do {
        var resultslice = results.getRange({start:searchid,end:searchid+1000});
        resultslice.forEach(function(slice) {
            searchResults.push(slice);
            searchid++;
            }
        );
    } while (resultslice.length >=1000);
    return searchResults;
}  //////  function getAllResults(s)
/******************* 4 **************************************/