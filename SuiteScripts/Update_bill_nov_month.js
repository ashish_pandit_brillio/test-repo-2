function update_bill()
{
	try
	{
		var context = nlapiGetContext();
		var a_srch_rslt = nlapiSearchRecord('transaction','customsearch1687');
		if(a_srch_rslt)
		{
			for(var i_index = 0; i_index<a_srch_rslt.length; i_index++)
			{
				
				if (context.getRemainingUsage() <= 1000)
				{
					nlapiYieldScript();
				}
				
				var o_rcrd_obj = nlapiLoadRecord('vendorbill',a_srch_rslt[i_index].getId());
				if(o_rcrd_obj)
				{
					var i_count = o_rcrd_obj.getLineItemCount('expense');
					
					for(var i_line_no =1 ; i_line_no<=i_count; i_line_no++ )
					{
						var i_cust_id = o_rcrd_obj.getLineItemValue('expense','custcol_customer_entityid',i_line_no);
						var s_cust_name = o_rcrd_obj.getLineItemValue('expense','custcol_cust_name_on_a_click_report',i_line_no);
						
						var s_customer_whole_name = i_cust_id;
						s_customer_whole_name += ' ';
						s_customer_whole_name += s_cust_name;
						
						if(s_customer_whole_name)
							o_rcrd_obj.setLineItemValue('expense','custcolcustcol_temp_customer',i_line_no,s_customer_whole_name);
					}
					
					var i_count = o_rcrd_obj.getLineItemCount('item');
					
					for(var i_line_no =1 ; i_line_no<=i_count; i_line_no++ )
					{
						var i_cust_id = o_rcrd_obj.getLineItemValue('item','custcol_customer_entityid',i_line_no);
						var s_cust_name = o_rcrd_obj.getLineItemValue('item','custcol_cust_name_on_a_click_report',i_line_no);
						
						var s_customer_whole_name = i_cust_id;
						s_customer_whole_name += ' ';
						s_customer_whole_name += s_cust_name;
						
						if(s_customer_whole_name)
							o_rcrd_obj.setLineItemValue('item','custcolcustcol_temp_customer',i_line_no,s_customer_whole_name);
					} 
					
					var _submitted_id = nlapiSubmitRecord(o_rcrd_obj,true,true);
					nlapiLogExecution('audit','submitted id:- ',_submitted_id);
				}
				
				if (context.getRemainingUsage() <= 1000)
				{
					nlapiYieldScript();
				}	
			}
		}
	}
	catch(err)
	{
		nlapiLogExecution('ERROR','ERROR MESSAGE :- ',err);
	}
}