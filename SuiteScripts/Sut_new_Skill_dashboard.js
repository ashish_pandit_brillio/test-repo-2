// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
/*
   	Script Name : SUT_new_skill_dashboard.js
	Author      : Sai Saranya
	Date        : 04 JAN 2019
    Description : Suitelet to display available recourses 


	Script Modification Log:

	-- Date --			-- Modified By --				--Requested By--				-- Description --
 


Below is a summary of the process controls enforced by this script file.  The control logic is described
more fully, below, in the appropriate function headers and code blocks.


     SCHEDULED FUNCTION
		- scheduledFunction(type)


     SUB-FUNCTIONS
		- The following sub-functions are called by the above core functions in order to maintain code
            modularization:

               - NOT USED

*/
}
// END SCRIPT DESCRIPTION BLOCK  ====================================


// BEGIN SCHEDULED FUNCTION =============================================
// Global Array to hold employee id and details
var a_employee_id	=	new Array();
var a_employee_data = new Array();

function suiteletFunction_SelectResourse(request, response)
{
	var form = nlapiCreateForm('Employee Skill Update List');
	
	// Set Script
	form.setScript('customscript_newskill_dashboard_validate');
	var a_employee_id = nlapiGetUser();
	if(parseInt(a_employee_id) == parseInt(41571) || parseInt(a_employee_id) == parseInt(62082) || parseInt(a_employee_id)== parseInt(1543) || parseInt(a_employee_id) == parseInt(3165) || parseInt(a_employee_id)== parseInt(8231))
        a_employee_id = 442;
	var employee = nlapiLookupField('employee', a_employee_id,['internalid','department','custentity_reportingmanager','custentity_skill_updated']);
	var employee_name = employee.internalid;
	var employee_dep = employee.department;
	var reporting_manager = employee.custentity_reportingmanager;
	if(request.getMethod() == 'GET')
	{
		var o_context = nlapiGetContext();
		var i_user_logegdIn_id = o_context.getUser();
		var skill_updated = employee.custentity_skill_updated;
		var prev_skills_list = form.addField('custpage_prev_skill_length','text','Skills Length','employeegroup').setDisplayType('hidden');
		if(skill_updated == 'T')
		{
			//var form = nlapiCreateForm('New Skills Updation!!!" ');
			var group = form.addFieldGroup( 'employeegroup', 'Employee Information');
			var user_name = form.addField('custpage_employee','select','Employee','employee','employeegroup');
			user_name.setDefaultValue(employee_name);
			user_name.setDisplayType('inline');
			var user_name = form.addField('custpage_department','select','Department','department','employeegroup').setDisplayType('disabled');;
			user_name.setDefaultValue(employee_dep);
			var a_emp_list = new Array();
			var a_skill_list = new Array();
			var practice_list = getTaggedFamilies(employee_dep);
			// Add Fields
			var reporting_id=form.addField('reprtng_id','select','Employee','employee').setDisplayType('hidden');
			reporting_id.setDefaultValue(reporting_manager);	
			//o_sublist.setLineItemValues(a_data);
			var previous_skill_tab = form.addSubTab('previous_skill_details', 'Current Skills');
			var skill_tab = form.addSubTab('skill_details', 'New Skill & Certification Details');
			var skill_sublist = form.addSubList('skill_sublist', 'inlineeditor', 'Certification', 'skill_details');
			var family = skill_sublist.addField('custpage_family', 'select', 'Skill Family');
			family.addSelectOption('','');
			for(var i=0;i<practice_list.length;++i)
			{
				var n = practice_list[i].getValue('internalid');
				family.addSelectOption(n,practice_list[i].getValue('name'));
			}
			var skill_field = skill_sublist.addField('custpage_primary_skill', 'select', 'Skills');
			var skills_updated = skill_sublist.addField('custpage_skills_updated','text','Skills updated');
			skills_updated.setDisplayType('disabled');
			var radio_button = skill_sublist.addField('custpage_primary', 'checkbox', 'Pirimary');
			var radio_button_2 = skill_sublist.addField('custpage_secondary', 'checkbox', 'Secondary');
			var proficiency = skill_sublist.addField('custpage_proficncy', 'select', 'Proficiency','customlist_skill_proficiency');
			var certificate_field = skill_sublist.addField('custpage_certificate', 'text', 'Certification');
			//For previous Skill Details
			var skill_filters = new Array();
			skill_filters[0]= new nlobjSearchFilter('custrecord_skill_approval_status',null,'anyof',[1,2]);
			skill_filters[1]= new nlobjSearchFilter('isinactive',null,'is','F');
			skill_filters[2]= new nlobjSearchFilter('custrecord_skill_update_employee',null,'anyof',a_employee_id);
			var skill_col = new Array();
			skill_col[0] = new nlobjSearchColumn('custrecord_family_list_master');
			skill_col[1] = new nlobjSearchColumn('custrecord_skill_pri_list');
			skill_col[2] = new nlobjSearchColumn('custrecord_primary_check');
			skill_col[3] = new nlobjSearchColumn('custrecord_second_check');
			skill_col[4] = new nlobjSearchColumn('custrecord_proficieny_level');
			skill_col[5] = new nlobjSearchColumn('custrecord_certificate');
			skill_col[6] = new nlobjSearchColumn('internalid');
			var skill_search = nlapiSearchRecord('customrecord_employee_skill_details', null,skill_filters,skill_col);
			var a_data = {};
			if(_logValidation(skill_search))
			{
				for(var s_indx= 0; s_indx < skill_search.length ; s_indx++)
				{
                  var prima_select = skill_search[s_indx].getValue('custrecord_primary_check');
                  if(prima_select == 'T')
                    prima_select ='YES';
                  else
                    prima_select ='NO';
                  var sec_select =  skill_search[s_indx].getValue('custrecord_second_check');
                  if(sec_select == 'T')
                    sec_select ='YES';
                  else
                    sec_select = 'NO';
					a_data = {
					prev_custpage_family : skill_search[s_indx].getText('custrecord_family_list_master'),
					custpage_prev_skills_updated : skill_search[s_indx].getValue('custrecord_skill_pri_list'),
					custpage_prev_primary : prima_select,
					custpage_prev_secondary :sec_select,
					custpage_prev_proficncy : skill_search[s_indx].getValue('custrecord_proficieny_level'),
					custpage_prev_certificate : skill_search[s_indx].getValue('custrecord_certificate'),
					custpage_prev_recrd_id : skill_search[s_indx].getValue('internalid'),
					
					};
					a_skill_list.push(a_data);
				}
				prev_skills_list.setDefaultValue(parseInt(skill_search.length));
				var prev_skill_sublist = form.addSubList('prev_skill_sublist', 'inlineeditor', 'Current Skills', 'previous_skill_details');
				var prev_family = prev_skill_sublist.addField('prev_custpage_family', 'text', 'Skill Family').setDisplayType('disabled');		
				var skills_updated = prev_skill_sublist.addField('custpage_prev_skills_updated','text','Skills updated').setDisplayType('disabled');
				var radio_button = prev_skill_sublist.addField('custpage_prev_primary', 'text', 'Pirimary').setDisplayType('disabled');
				var radio_button_2 = prev_skill_sublist.addField('custpage_prev_secondary', 'text', 'Secondary').setDisplayType('disabled');
				var proficiency = prev_skill_sublist.addField('custpage_prev_proficncy', 'select', 'Proficiency','customlist_skill_proficiency');
				var certificate_field = prev_skill_sublist.addField('custpage_prev_certificate', 'text', 'Certification');
				var certificate_field = prev_skill_sublist.addField('custpage_prev_recrd_id', 'text', 'Recid').setDisplayType('hidden');
				var certificate_field = prev_skill_sublist.addField('custpage_updated_skill', 'checkbox', 'Updated').setDisplayType('hidden');
				prev_skill_sublist.setLineItemValues(a_skill_list);
				//var fld_sub_practice = skill_sublist.addField('custpage_cstart_date', 'date', 'Start Date');
				//var fld_emp_level = skill_sublist.addField('custpage_cend_date', 'date', 'End Date'); 
				form.addSubmitButton('Submit');
			}
		}
		else
		{
			if(parseInt(i_user_logegdIn_id) == parseInt(41571))
			i_user_logegdIn_id = 1806;
      
			var group = form.addFieldGroup( 'employeegroup', 'Employee Information');
			var user_name = form.addField('custpage_employee','select','Employee','employee','employeegroup');
			user_name.setDefaultValue(employee_name);
			user_name.setDisplayType('inline');
			var user_name = form.addField('custpage_department','select','Department','department','employeegroup').setDisplayType('disabled');;
			user_name.setDefaultValue(employee_dep);
			var a_emp_list = new Array();
			var practice_list = getTaggedFamilies(employee_dep);
			var a_data = new Array();
			// Add Fields
			var reporting_id=form.addField('reprtng_id','select','Employee','employee').setDisplayType('hidden');
			reporting_id.setDefaultValue(reporting_manager);	
			//o_sublist.setLineItemValues(a_data);
			var skill_tab = form.addSubTab('skill_details', 'Skill & Certification Details');
			var skill_sublist = form.addSubList('skill_sublist', 'inlineeditor', 'Certification', 'skill_details');
			var family = skill_sublist.addField('custpage_family', 'select', 'Skill Family');
			family.addSelectOption('','');
			for(var i=0;i<practice_list.length;++i)
			{
				var n = practice_list[i].getValue('internalid');
				family.addSelectOption(n,practice_list[i].getValue('name'));
			}
			var skill_field = skill_sublist.addField('custpage_primary_skill', 'select', 'Skills');
			var skills_updated = skill_sublist.addField('custpage_skills_updated','text','Skills updated');
			skills_updated.setDisplayType('disabled');
			var radio_button = skill_sublist.addField('custpage_primary', 'checkbox', 'Pirimary');
			var radio_button_2 = skill_sublist.addField('custpage_secondary', 'checkbox', 'Secondary');
			var proficiency = skill_sublist.addField('custpage_proficncy', 'select', 'Proficiency','customlist_skill_proficiency');
			var certificate_field = skill_sublist.addField('custpage_certificate', 'text', 'Certification');
			//var fld_sub_practice = skill_sublist.addField('custpage_cstart_date', 'date', 'Start Date');
			//var fld_emp_level = skill_sublist.addField('custpage_cend_date', 'date', 'End Date'); 
			form.addSubmitButton('Submit');
		}
	}
	response.writePage(form);
	if(request.getMethod() == 'POST')
	{
		var emp_id = nlapiGetUser();
		var post_form = nlapiCreateForm('Skills Updated Successfully!!!');
		response.writePage(post_form);
		nlapiSubmitField('employee',parseInt(emp_id), 'custentity_skill_updated', 'T');
	}
}


// This function will return the employee details
function getEmployeeIds(skillFamily, skills, resLocation) 
{
	try
	{
		var employee_filter	=	new Array();
		employee_filter[0]	=	new nlobjSearchFilter('custrecord_primary_updated', 'custrecord_employee_skill_updated', 'anyof', parseInt(skills));
		employee_filter[1]	=	new nlobjSearchFilter('custrecord_family_selected', 'custrecord_employee_skill_updated', 'anyof', skillFamily);
		employee_filter[2]	=	new nlobjSearchFilter('formulatext',null, 'startswith', resLocation);
		employee_filter[2].setFormula('{location}');
		employee_filter[3]	=	new nlobjSearchFilter('isinactive',null, 'is', false);
		
		
		var employee_columns =	new Array();
		employee_columns[0]	=	new nlobjSearchColumn('entityid');
		employee_columns[1]	=	new nlobjSearchColumn('internalid');
		employee_columns[2]	=	new nlobjSearchColumn("custrecord_family_selected","CUSTRECORD_EMPLOYEE_SKILL_UPDATED",null);
		employee_columns[3]	=	new nlobjSearchColumn("custrecord_primary_updated","CUSTRECORD_EMPLOYEE_SKILL_UPDATED",null);
		employee_columns[4]	=	new nlobjSearchColumn('department');
		employee_columns[5]	=	new nlobjSearchColumn('location');
		var a_search_employee_details	=	nlapiSearchRecord('employee', null, employee_filter, employee_columns);
		
		var a_vendor_details	=	new Array();
		
		var i_count	= a_search_employee_details == null?0:a_search_employee_details.length;
		nlapiLogExecution('Debug','i_count Employee ',i_count);
		
		for(var i = 0; i < i_count; i++)
		{
			a_employee_id.push(a_search_employee_details[i].getValue('internalid'));
			a_employee_data.push({'emp_id':a_search_employee_details[i].getValue('internalid'),'emp_name':a_search_employee_details[i].getValue('entityid'), 'skill_family':a_search_employee_details[i].getText("custrecord_family_selected","CUSTRECORD_EMPLOYEE_SKILL_UPDATED",null), 'skills':a_search_employee_details[i].getText("custrecord_primary_updated","CUSTRECORD_EMPLOYEE_SKILL_UPDATED",null), 'emp_practice':a_search_employee_details[i].getText('department'),'emp_location':a_search_employee_details[i].getText('location')});
		}
	
		return a_search_employee_details;
	}
	catch(e)
	{
		nlapiLogExecution('Debug','Error ',e);
	}
}

//This function will return the Resource Allocation Details
function resourceAllocationData(d_resStartDate, d_resEndDate)
{
	try
	{
		var resourceallocationSearch = nlapiSearchRecord("resourceallocation",null,
		[
		   [
			   [["startdate","within",d_resStartDate,d_resEndDate],
			   "AND",
			   ["enddate","within",d_resStartDate,d_resEndDate]],
			   "OR",
			   [["startdate","onorafter",d_resStartDate],
			   "AND",
			   ["enddate","onorbefore",d_resEndDate]],
			   "OR",
			   [["startdate","onorbefore",d_resStartDate],
			   "AND",
			   ["enddate","onorafter",d_resEndDate]]
		   ]
		], 
		[
		   new nlobjSearchColumn("resource"), 
		   new nlobjSearchColumn("startdate"), 
		   new nlobjSearchColumn("enddate")
		]
		);
		var i_count	= resourceallocationSearch == null?0:resourceallocationSearch.length;
		
		for(var i = 0; i < i_count; i++)
		{
			var resource = resourceallocationSearch[i].getValue('resource');
			if(resource.indexOf(a_employee_id) > 0)
			{
				a_employee_id.splice( a_employee_id.indexOf(resource), 1 );
			}
		}
	
		return resourceallocationSearch;
	}
	catch(error)
	{
		nlapiLogExecution('Debug','Error ',error);
	}
}

// This function will return Soft-Lockdetails
function getAssignResources(a_employee_id,d_resStartDate,d_resEndDate)
{
	try
	{
		nlapiLogExecution('Debug','a_employee_id ',a_employee_id);
		var customrecordosf_resource_recordSearch = nlapiSearchRecord("customrecordosf_resource_record",null,
		[
		   
			   ["custrecord_selected_resource","anyof",a_employee_id],
			   "AND",
			   [
				   [["custrecord_resource_start_date_osf","within",d_resStartDate,d_resEndDate],
				   "AND",
				   ["custrecord_resource_end_date_osf","within",d_resStartDate,d_resEndDate]],
				   "OR",
				   [["custrecord_resource_start_date_osf","onorafter",d_resStartDate],
				   "AND",
				   ["custrecord_resource_end_date_osf","onorbefore",d_resEndDate]],
				   "OR",
				   [["custrecord_resource_start_date_osf","onorbefore",d_resStartDate],
				   "AND",
				   ["custrecord_resource_end_date_osf","onorafter",d_resEndDate]]
			   ]
		], 
		[
		   new nlobjSearchColumn("custrecord_selected_resource"), 
		   new nlobjSearchColumn("custrecord_resource_start_date_osf"), 
		   new nlobjSearchColumn("custrecord_resource_end_date_osf")
		]
		);
		var i_count	= customrecordosf_resource_recordSearch == null?0:customrecordosf_resource_recordSearch.length;
		nlapiLogExecution('Debug','i_count ',i_count);
		for(var i = 0; i < i_count; i++)
		{
			var i_softLockResource = customrecordosf_resource_recordSearch[i].getValue('custrecord_selected_resource');
			nlapiLogExecution('Debug','i_softLockResource &&&&',a_employee_id.indexOf(i_softLockResource));
			if(a_employee_id.indexOf(i_softLockResource) > 0)
			{
				a_employee_id.splice( a_employee_id.indexOf(i_softLockResource), 1 );
				nlapiLogExecution('Debug','Updated Array ',a_employee_id);
			}
		}
	}
	catch(error)
	{
		nlapiLogExecution('Debug','Error ',error);
	}
}

function _logValidation(value) 
{
 if(value != null && value!= 'null' && value != '' && value != undefined && value != undefined && value != 'undefined' && value.toString() != 'NaN' && value != NaN) 
 {
  return true;
 }
 else 
 { 
  return false;
 }
}
function getTaggedFamilies(emp_dep)
{
try{
  var parent_practice = nlapiLookupField('department',emp_dep,'custrecord_parent_practice');
var practice_filter = [
		        
		           [ [ 'custrecord_skill_emp_dep', 'anyOf', emp_dep ],'or',[ 'custrecord_skill_emp_dep', 'anyOf', parent_practice]],
		                'and',
		                [ 'isinactive',
		                        'anyOf', 'F' ]
		         ];

		var practice_search_results = searchRecord('customrecord_employee_skill_family_list', null, practice_filter,
		        [ new nlobjSearchColumn("name"),
		               	new nlobjSearchColumn("internalid")]);


	return practice_search_results;
}catch(err){
	throw err;
	}
}
 function getSkills(primary_selected) {
	try {
			var skill_filter = 
		        [
		            [ 'custrecord_employee_skil_family', 'is', primary_selected ],               
		              'and',
		        [ 'isinactive', 'is', 'F' ] ];

		var skill_search_results = searchRecord('customrecord_employee_skills', null, skill_filter,
		        [ new nlobjSearchColumn("custrecord_emp_primary_skill"),
		               		new nlobjSearchColumn("internalid")]);

		nlapiLogExecution('debug', 'Results count',
		        skill_search_results.length);

		if (skill_search_results.length == 0) {
			throw "You don't have any Primary Skills under Family Selected.";
		}
	
		return skill_search_results;
	} catch (err) {
		nlapiLogExecution('ERROR', 'getTaggedProjectList', err);
		throw err;
	}
		
	
}