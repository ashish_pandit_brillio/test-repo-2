/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       22 May 2017     deepak.srinivas
 *
 */

/**
 * @param {Object} dataIn Parameter object
 * @returns {Object} Output object
 */
function postRESTlet(dataIn) {
	try{
		
		var response = new Response();
		var current_date = nlapiDateToString(new Date());
		var currentDate = sysDate();
		var currentTime = timestamp(); // returns the time stamp in HH:MM:SS
	    var currentDateAndTime = currentDate + ' ' + currentTime;
		//Log for current date
		nlapiLogExecution('DEBUG', 'Current Date', 'current_date...' + current_date)
		//nlapiLogExecution('debug', 'dataIn', JSON.stringify(dataIn));
		
		var receivedDate = dataIn.LastInstaceDate;
		//var receivedDate = '5/3/2017 4:00 am'
		var filters = [];
		filters.push(new nlobjSearchFilter('custevent_ra_last_modified_date',null,'within',receivedDate,currentDateAndTime));
		
		var cols = [];
		cols.push(new nlobjSearchColumn('internalid'));
		cols.push(new nlobjSearchColumn('resource'));
		cols.push(new nlobjSearchColumn('project'));
		cols.push(new nlobjSearchColumn('startdate'));
		cols.push(new nlobjSearchColumn('enddate'));//percentoftime
		cols.push(new nlobjSearchColumn('percentoftime'));
		cols.push(new nlobjSearchColumn('department', 'employee'));
		cols.push(new nlobjSearchColumn('custeventbstartdate'));
		cols.push(new nlobjSearchColumn('custeventbenddate'));
		cols.push(new nlobjSearchColumn('custevent4')); //Onsite/offiste
		cols.push(new nlobjSearchColumn('jobbillingtype','job')); //Onsite/offiste
		
		var searchObj = nlapiSearchRecord('resourceallocation','customsearch_codex_participating_practic',filters,cols);
		var JSON = {};
		var dataRow = [];
		if(_logValidation(searchObj)){
			for(var index=0;index<searchObj.length;index++){
				JSON = {
						InternalID : searchObj[index].getValue('internalid'),
						Resource : searchObj[index].getText('resource'),
						Project : searchObj[index].getText('project'),
						ProjectBillingType : searchObj[index].getText('jobbillingtype','job'),
						AllocationStartDate : searchObj[index].getValue('startdate'),
						AllcoationEndDate : searchObj[index].getValue('enddate'),
						PercentOfAllocation : searchObj[index].getValue('percentoftime'),
						ResourcePractice : searchObj[index].getText('department', 'employee'),
						BillingStartDate : searchObj[index].getValue('custeventbstartdate'),
						BillingEndDate : searchObj[index].getValue('custeventbenddate'),
						OnsiteOffshore : searchObj[index].getText('custevent4')
					
						
				};
				dataRow.push(JSON);
			}
			
		}
		response.Data = dataRow;
		response.Status = true;
		
	}
	catch (err) {
		nlapiLogExecution('ERROR', 'postRESTlet', err);
		response.Data = err;
		response.Status = false;
	}
	//nlapiLogExecution('debug', 'response', JSON.stringify(response));
	return response;
	
}

//validate blank entries
function _logValidation(value) 
{
 if(value != null && value.toString() != null && value != '' && value != undefined && value.toString() != undefined && value != 'undefined' && value.toString() != 'undefined'&& value.toString() != 'NaN' && value != NaN) 
 {
  return true;
 }
 else 
 { 
  return false;
 }
}
//Get current date
function sysDate() {
	var date = new Date();
	var tdate = date.getDate();
	var month = date.getMonth() + 1; // jan = 0
	var year = date.getFullYear();
	return currentDate = month + '/' + tdate + '/' + year;
	}

//Returns timestamp
function timestamp() {
	var str = "";

	var currentTime = new Date();
	var hours = currentTime.getHours();
	var minutes = currentTime.getMinutes();
	var seconds = currentTime.getSeconds();
	var meridian = "";
	if (hours > 12) {
	    meridian += "pm";
	} else {
	    meridian += "am";
	}
	if (hours > 12) {

	    hours = hours - 12;
	}
	if (minutes < 10) {
	    minutes = "0" + minutes;
	}
	if (seconds < 10) {
	    seconds = "0" + seconds;
	}
	//str += hours + ":" + minutes + ":" + seconds + " ";
	str += hours + ":" + minutes +" ";
	return str + meridian;
	}

//Search function for results more than 1000
function searchRecord(recordType, savedSearch, arrFilters, arrColumns,
        filterExpression)
{

	try {
		var search = null;

		// if a saved search is provided, load it and add the filters and
		// columns
		if (savedSearch) {
			search = nlapiLoadSearch(recordType, savedSearch);

			if (isArrayNotEmpty(arrFilters)) {
				search.addFilters(arrFilters);
			}

			if (isArrayNotEmpty(arrColumns)) {
				search.addColumns(arrColumns);
			}

			if (isArrayNotEmpty(filterExpression)) {
				search.setFilterExpression(filterExpression);
			}
		}
		// create a new search
		else {
			search = nlapiCreateSearch(recordType, arrFilters, arrColumns);
		}

		// run search
		var resultSet = search.runSearch();

		// iterate through the search and get all data 1000 at a time
		var searchResultCount = 0;
		var resultSlice = null;
		var searchResult = [];

		do {
			resultSlice = resultSet.getResults(searchResultCount,
			        searchResultCount + 1000);

			if (resultSlice) {

				resultSlice.forEach(function(result) {

					searchResult.push(result);
					searchResultCount++;
				});
			}
		} while (isArrayNotEmpty(resultSlice) && resultSlice.length >= 1000);

		return searchResult;
	} catch (err) {
		nlapiLogExecution('ERROR', 'searchRecord', err);
		throw err;
	}
}
function isEmpty(value) {

	return value == null || value == "" || typeof (value) == undefined;
}

function isNotEmpty(value) {

	return !isEmpty(value);
}

function isArrayEmpty(argArray) {

	return !isArrayNotEmpty(argArray);
}

function isArrayNotEmpty(argArray) {

	return (isNotEmpty(argArray) && argArray.length > 0);
}
