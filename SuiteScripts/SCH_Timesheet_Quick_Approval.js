/**
 * Scheduled script to approve timesheet that have been approved in the backend
 * 
 * Version Date Author Remarks 1.00 01 Sep 2015 nitish.mishra
 * Version Date Author Remarks 1.01 01 April 2020 nihal/praveena
 */

function scheduled(type) {
	nlapiLogExecution('debug', 'started');
	try {
		var a_table_data = new Array();
		a_table_data.push(["Timesheet ID", "Error Code", "Error Message"])
		var b_err_flag = false;

		// get the timesheets from the quick pool that are pending to te updated
		var timesheetSearch = getTimesheetsToBeApproved();
		var currentContext = nlapiGetContext();
		var statusToBeSet = "";
		nlapiLogExecution('DEBUG', 'timesheetSearch', timesheetSearch.length);
		for (var i = 0; i < timesheetSearch.length; i++) {
			try {
				// check the status to be set
				if (timesheetSearch[i].getValue('custrecord_qap_approved') == 'T') {
					statusToBeSet = '3';
				}
				else {
					statusToBeSet = '4';
				}
				// update the timesheet
				updateTimesheetStatus(timesheetSearch[i].getValue('custrecord_qap_timesheet'), statusToBeSet);

				// update the quick approval pool record
				nlapiSubmitField('customrecord_ts_quick_approval_pool', timesheetSearch[i].getId(), 'custrecord_qap_ts_updated', 'T');
				yieldScript(currentContext);
			}
			catch (err) {
				nlapiLogExecution('error', 'Main for', err);
				b_err_flag = true;
				var a_row = new Array();
				a_row.push(timesheetSearch[index].getId());
				a_row.push(err.code);
				a_row.push(err.message);
				a_table_data.push(a_row);
			}
		}

		if (b_err_flag) {
			Send_Exception_Mail(a_table_data);
		}

	}
	catch (err) {
		nlapiLogExecution('error', 'scheduled', err);

	}
	nlapiLogExecution('debug', 'ended');
}

function updateTimesheetStatus(timesheetId, newStatus) {
	try {
		nlapiLogExecution('debug', 'start');
		var weekDays = ['timebill0', 'timebill1', 'timebill2', 'timebill3', 'timebill4', 'timebill5', 'timebill6'];

		// load the timesheet record
		var timesheetRecord = nlapiLoadRecord('timesheet', timesheetId, { recordmode: 'dynamic' });
		nlapiLogExecution('debug', 'Timesheet id ');

		// loop and approve / reject the timesheet entries
		var linecount = timesheetRecord.getLineItemCount('timeitem');

		for (var linenum = 1; linenum <= linecount; linenum++) {
			timesheetRecord.selectLineItem('timeitem', linenum);
			nlapiLogExecution('debug', 'line');


			// loop day wise 

			for (var day_indx = 0; day_indx < 7; day_indx++) {
				//Added by Praveena 0n 07-04-2020
				var i_timebillView = timesheetRecord.getCurrentLineItemValue('timeitem', weekDays[day_indx]);
				if (i_timebillView) {
					//Added by Praveena 0n 07-04-2020
					var timebillEdit = nlapiSubmitField("timebill", i_timebillView, ['approvalstatus'], [newStatus]);
				}
			}

			timesheetRecord.commitLineItem('timeitem', false);
		}

		// //Added by Praveena 0n 07-04-2020 load the timesheet record
		var timesheetRecord = nlapiLoadRecord('timesheet', timesheetId, { recordmode: 'dynamic' });
		nlapiSubmitRecord(timesheetRecord, true, false);
		nlapiLogExecution('debug', 'Timesheet Updated : ' + timesheetId);
	}
	catch (err) {
		nlapiLogExecution('Error', 'Failed To Update Timesheet : ' + timesheetId, err);
		Send_Exception_Mail(err);
	}
}

function getTimesheetsToBeApproved() {
	try {
		var filters = [
			[['custrecord_qap_approved', 'is', 'T'], 'or',
			['custrecord_qap_rejected', 'is', 'T']], 'and',
			['isinactive', 'is', 'F'], 'and',
			['custrecord_qap_ts_updated', 'is', 'F']];

		var columns = [new nlobjSearchColumn('custrecord_qap_timesheet'),
		new nlobjSearchColumn('custrecord_qap_approved'),
		new nlobjSearchColumn('custrecord_qap_rejected')];

		return searchRecord('customrecord_ts_quick_approval_pool', null, filters, columns);
	}
	catch (err) {
		nlapiLogExecution('error', 'getTimesheetsToBeApproved', err);
		throw err;
	}
}

function yieldScript(currentContext) {

	if (currentContext.getRemainingUsage() <= 100) {
		nlapiLogExecution('AUDIT', 'API Limit Exceeded');
		var state = nlapiYieldScript();

		if (state.status == "FAILURE") {
			nlapiLogExecution('ERROR', 'Yield Script Failed', 'Reason : ' + state.reason + ' / Size : ' + state.size);
			return false;
		}
		else if (state.status == "RESUME") {
			nlapiLogExecution('AUDIT', 'Script Resumed');
		}
	}
}
function Send_Exception_Mail(a_table_data,err) {
	try {
		var context = nlapiGetContext();
		var s_DeploymentID = context.getDeploymentId();
		var i_ScriptID = context.getScriptId();
		var s_Subject = 'Issue on SCH Timesheet Quick Approval';

		var s_Body = 'This is to inform that SCH Timesheet Quick Approval is having an issue and System is not able to send the email notification to the employees.';
		s_Body += '<br/>Script: ' + i_ScriptID;
		s_Body += '<br/>Deployment: ' + s_DeploymentID;
		s_Body += '<br/><table width="100%" border="1" cellspacing="1" cellpadding="1">';
		for (var index = 0; index < a_table_data.length; index++) {
			s_Body += '<tr>';
			s_Body += '<td>' + a_table_data[index][0] + '</td>';
			s_Body += '<td>' + a_table_data[index][1] + '</td>';
			s_Body += '<td>' + a_table_data[index][2] + '</td>';
			s_Body += '</tr>';
		}
		s_Body += '</table>';
		s_Body += '<br/>Please have a look and do the needfull.';
		s_Body += '<br/><br/>Note: This is system genarated email, incase of any failure while sending the notification to employee(s)';

		s_Body += '<br/><br/>Regards,';
		s_Body += '<br/>Information Systems';
		nlapiSendEmail('422', 'netsuite.support@brillio.com', s_Subject, s_Body,null, null, null, null);

	}
	catch (err) {
		nlapiLogExecution('error', ' Send_Exeception_Mail', err.message);
	}
}
// END Send_Exception_Mail Function
