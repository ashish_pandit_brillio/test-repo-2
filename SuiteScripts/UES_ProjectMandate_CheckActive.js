/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       06 Aug 2018     deepak.srinivas
 *
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType
 *   
 * @param {String} type Operation types: create, edit, view, copy, print, email
 * @param {nlobjForm} form Current form
 * @param {nlobjRequest} request Request object
 * @returns {Void}
 */
function userEventBeforeLoad(type, form, request){
 
	try{
		 var currentContext = nlapiGetContext();
		 if(currentContext.getExecutionContext() == 'userinterface')
		 {
			// column field

			    var projectSublist = nlapiGetLineItemField('line','custcol_project_entity_id');

			    if (projectSublist){
			 
			    	projectSublist.setMandatory(true);

			    }
		 }
		
	}
	catch (err) {
		nlapiLogExecution('ERROR', 'postRESTlet', err);
		throw err;
	}
	
}

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType
 * 
 * @param {String} type Operation types: create, edit, delete, xedit
 *                      approve, reject, cancel (SO, ER, Time Bill, PO & RMA only)
 *                      pack, ship (IF)
 *                      markcomplete (Call, Task)
 *                      reassign (Case)
 *                      editforecast (Opp, Estimate)
 * @returns {Void}
 */
function userEventBeforeSubmit(type){
	try{
      if(type == 'delete')
        {
          return;
        }
		var error_log = [];
		var currentContext = nlapiGetContext();
		var void_value = nlapiGetFieldValue('createdfrom');
		if(_logValidation(void_value))
		{
			return true;
		}
		else {
			
		if(currentContext.getExecutionContext() == 'userinterface')
		{
			 var lineCount = nlapiGetLineItemCount('line');
			 if(type == 'edit')
			 {
			 for(var i=1; i <= lineCount; i++){
				 	var proj_desc_id = nlapiGetLineItemValue('line','custcol_project_entity_id',i);
				 	//Check Project ID is blank
					if(!_logValidation(proj_desc_id)){
						error_log.push({
							'Msg--': "'ProjectID is blank at line:-- '"+i
							});
					}
					var check_value = nlapiGetLineItemValue('line', 'custcol_project_id_check', i);
					if(check_value == 'T')
					{
					var s_employee_id = nlapiGetLineItemValue('line','custcol_employee_entity_id',i);
					var existing_practice = nlapiGetLineItemValue('line', 'department',i);
					//Check Project ID is blank
					if(!_logValidation(proj_desc_id)){
						error_log.push({
							'Msg--': "'ProjectID is blank at line:-- '"+i
							});
					}
					else{
						//Search For Project 
						//var filters_proj = new Array();
						//filters_proj[0] = new nlobjSearchFilter('entityid', null, 'is', proj_desc_id);
						
						var column_proj = new Array();
						column_proj[0] = new nlobjSearchColumn('jobbillingtype');
						column_proj[1] = new nlobjSearchColumn('customer');
						column_proj[2] = new nlobjSearchColumn('custentity_region');
						column_proj[3] = new nlobjSearchColumn('entityid');
						column_proj[4] = new nlobjSearchColumn('altname');
						column_proj[5] = new nlobjSearchColumn('custentity_project_allocation_category');
						column_proj[6] = new nlobjSearchColumn('companyname','customer');
						column_proj[7] = new nlobjSearchColumn('entityid','customer');
						column_proj[8] = new nlobjSearchColumn('custentity_vertical');
						column_proj[9] = new nlobjSearchColumn('territory','customer');
						column_proj[10] = new nlobjSearchColumn('custentity_practice');
						column_proj[11] = new nlobjSearchColumn('internalid','customer');
						column_proj[12] = new nlobjSearchColumn('internalid');
						column_proj[13] = new nlobjSearchColumn('custentity_project_services');
						
						
						var a_results_proj = nlapiSearchRecord('job', null, 
								[
								   ["entityid","is",proj_desc_id.trim()], 
								   "AND", 
								   [[["status","anyof","2","4"]],"OR",[["systemnotes.type","is","F"],"AND",["systemnotes.date","onorafter","threemonthsagotodate"],"AND",["systemnotes.newvalue","startswith","PROJECT-Closed"],"AND",["systemnotes.field","anyof","CUSTJOB.KENTITYSTATUS"]]]
								]
								,column_proj);
						if(_logValidation(a_results_proj)){
							
							var proj_practice = a_results_proj[0].getValue('custentity_practice');
							var s_proj_practice = a_results_proj[0].getText('custentity_practice');
							
							if(_logValidation(proj_practice)){
								var is_practice_active = nlapiLookupField('department',parseInt(proj_practice),['isinactive']);
								var isinactive_Practice = is_practice_active.isinactive;
								nlapiLogExecution('debug','isinactive_Practice',isinactive_Practice);
								if(!_logValidation(existing_practice) && isinactive_Practice =='T'){
									error_log.push({
										'Msg--':"'Practice '"+s_proj_practice+ "' is inactivated for project '"+proj_desc_id+ "' at line:-- '"+i
										});
								}
								}
						}
						//Project Would have closed/Incorrect Project ID
						else{
							error_log.push({
								'Msg--':"'Invalid ProjectID '"+proj_desc_id+"' at line:-- '"+i
								});
						}
					}
					//Check For Employee Data
					if(_logValidation(s_employee_id)){
					var filters_emp = new Array();
					filters_emp[0] = new nlobjSearchFilter('custentity_fusion_empid', null, 'is', s_employee_id);
					filters_emp[1] = new nlobjSearchFilter('isinactive', null, 'is', 'F');
					var column_emp = new Array();
					column_emp[0] = new nlobjSearchColumn('custentity_persontype');
					column_emp[1] = new nlobjSearchColumn('employeetype');
					column_emp[2] = new nlobjSearchColumn('subsidiary');
					column_emp[3] = new nlobjSearchColumn('custentity_fusion_empid');
					column_emp[4] = new nlobjSearchColumn('firstname');
					column_emp[5] = new nlobjSearchColumn('middlename');
					column_emp[6] = new nlobjSearchColumn('lastname');
					column_emp[7] = new nlobjSearchColumn('department');
					column_emp[8] = new nlobjSearchColumn('location');
					
					var a_results_emp_id = nlapiSearchRecord('employee', null, filters_emp, column_emp);
					if (_logValidation(a_results_emp_id))
					{
						var emp_practice = a_results_emp_id[0].getValue('department');
						var s_emp_practice = a_results_emp_id[0].getText('department');
						
						if(_logValidation(emp_practice)){
						var is_practice_active_e = nlapiLookupField('department',parseInt(emp_practice),['isinactive','custrecord_is_delivery_practice']);
						var isinactive_Practice_e = is_practice_active_e.isinactive;
						nlapiLogExecution('debug','isinactive_Practice_e',isinactive_Practice_e);
						if(!_logValidation(existing_practice) && isinactive_Practice_e =='T'){
							error_log.push({
								'Msg--':"'Practice '"+s_emp_practice+ "'is inactivated for employee '"+s_employee_id+ "' at line:-- '"+i
								});
						}
					}
					}
					else{
						error_log.push({
							'Msg--':"'Invalid EmployeeID '"+s_employee_id+"' at line:-- '"+i
							});
					}
					
				}
			 }
			 }	
			 }
			 else
			 {
				 	for(var i=1; i <= lineCount; i++){
					var proj_desc_id = nlapiGetLineItemValue('line','custcol_project_entity_id',i);
					var s_employee_id = nlapiGetLineItemValue('line','custcol_employee_entity_id',i);
					var existing_practice = nlapiGetLineItemValue('line', 'department',i);
					//Check Project ID is blank
					if(!_logValidation(proj_desc_id)){
						error_log.push({
							'Msg--': "'ProjectID is blank at line:-- '"+i
							});
					}
					else{
						//Search For Project 
						//var filters_proj = new Array();
						//filters_proj[0] = new nlobjSearchFilter('entityid', null, 'is', proj_desc_id);
						
						var column_proj = new Array();
						column_proj[0] = new nlobjSearchColumn('jobbillingtype');
						column_proj[1] = new nlobjSearchColumn('customer');
						column_proj[2] = new nlobjSearchColumn('custentity_region');
						column_proj[3] = new nlobjSearchColumn('entityid');
						column_proj[4] = new nlobjSearchColumn('altname');
						column_proj[5] = new nlobjSearchColumn('custentity_project_allocation_category');
						column_proj[6] = new nlobjSearchColumn('companyname','customer');
						column_proj[7] = new nlobjSearchColumn('entityid','customer');
						column_proj[8] = new nlobjSearchColumn('custentity_vertical');
						column_proj[9] = new nlobjSearchColumn('territory','customer');
						column_proj[10] = new nlobjSearchColumn('custentity_practice');
						column_proj[11] = new nlobjSearchColumn('internalid','customer');
						column_proj[12] = new nlobjSearchColumn('internalid');
						column_proj[13] = new nlobjSearchColumn('custentity_project_services');
						
						
						var a_results_proj = nlapiSearchRecord('job', null, 
								[
								   ["entityid","is",proj_desc_id.trim()], 
								   "AND", 
								   [[["status","anyof","2","4"]],"OR",[["systemnotes.type","is","F"],"AND",["systemnotes.date","onorafter","threemonthsagotodate"],"AND",["systemnotes.newvalue","startswith","PROJECT-Closed"],"AND",["systemnotes.field","anyof","CUSTJOB.KENTITYSTATUS"]]]
								]
								,column_proj);
						if(_logValidation(a_results_proj)){
							
							var proj_practice = a_results_proj[0].getValue('custentity_practice');
							var s_proj_practice = a_results_proj[0].getText('custentity_practice');
							
							if(_logValidation(proj_practice)){
								var is_practice_active = nlapiLookupField('department',parseInt(proj_practice),['isinactive']);
								var isinactive_Practice = is_practice_active.isinactive;
								nlapiLogExecution('debug','isinactive_Practice',isinactive_Practice);
								if(!_logValidation(existing_practice) && isinactive_Practice =='T'){
									error_log.push({
										'Msg--':"'Practice '"+s_proj_practice+ "' is inactivated for project '"+proj_desc_id+ "' at line:-- '"+i
										});
								}
								}
						}
						//Project Would have closed/Incorrect Project ID
						else{
							error_log.push({
								'Msg--':"'Invalid ProjectID '"+proj_desc_id+"' at line:-- '"+i
								});
						}
					}
					//Check For Employee Data
					if(_logValidation(s_employee_id)){
					var filters_emp = new Array();
					filters_emp[0] = new nlobjSearchFilter('custentity_fusion_empid', null, 'is', s_employee_id);
					filters_emp[1] = new nlobjSearchFilter('isinactive', null, 'is', 'F');
					var column_emp = new Array();
					column_emp[0] = new nlobjSearchColumn('custentity_persontype');
					column_emp[1] = new nlobjSearchColumn('employeetype');
					column_emp[2] = new nlobjSearchColumn('subsidiary');
					column_emp[3] = new nlobjSearchColumn('custentity_fusion_empid');
					column_emp[4] = new nlobjSearchColumn('firstname');
					column_emp[5] = new nlobjSearchColumn('middlename');
					column_emp[6] = new nlobjSearchColumn('lastname');
					column_emp[7] = new nlobjSearchColumn('department');
					column_emp[8] = new nlobjSearchColumn('location');
					
					var a_results_emp_id = nlapiSearchRecord('employee', null, filters_emp, column_emp);
					if (_logValidation(a_results_emp_id))
					{
						var emp_practice = a_results_emp_id[0].getValue('department');
						var s_emp_practice = a_results_emp_id[0].getText('department');
						
						if(_logValidation(emp_practice)){
						var is_practice_active_e = nlapiLookupField('department',parseInt(emp_practice),['isinactive','custrecord_is_delivery_practice']);
						var isinactive_Practice_e = is_practice_active_e.isinactive;
						nlapiLogExecution('debug','isinactive_Practice_e',isinactive_Practice_e);
						if(!_logValidation(existing_practice) && isinactive_Practice_e =='T'){
							error_log.push({
								'Msg--':"'Practice '"+s_emp_practice+ "'is inactivated for employee '"+s_employee_id+ "' at line:-- '"+i
								});
						}
					}
					}
					else{
						error_log.push({
							'Msg--':"'Invalid EmployeeID '"+s_employee_id+"' at line:-- '"+i
							});
					}
					
				}
					
			 }
			 }
			 //Check for error log
			 if(_logValidation(error_log) && error_log.length > 0){
				 var data = [];
				 var log_text = displayArrayObjects(error_log);
				 
				 nlapiLogExecution('DEBUG','JSON',JSON.stringify(log_text));
				throw nlapiCreateError('False',log_text,true);
				 
			 }
		 }
		} 
	}
	catch (err) {
		nlapiLogExecution('ERROR', 'postRESTlet', err);
		throw err;
	}
}
function displayArrayObjects(arrayObjects) {
    var len = arrayObjects.length;
    var text = "";

    for (var i = 0; i < len; i++) {
        var myObject = arrayObjects[i];
        
        for (var x in myObject) {
            text += ( x + ": " + myObject[x] + " ");
        }
        text += "<br/>";
    }

    return text;
}
function _logValidation(value) 
{
 if(value!='null' && value != null && value != null && value != '' && value != undefined && value != undefined && value != 'undefined' && value!= 'undefined'&& value!= 'NaN' && value != NaN) 
 {
  return true;
 }
 else 
 { 
  return false;
 }
}


/*var jobSearch = nlapiSearchRecord("job",null,
		[
		   ["entityid","is","BRTP00416"], 
		   "AND", 
		   [[["status","anyof","2","4"]],"OR",[["systemnotes.type","is","F"],"AND",["systemnotes.date","onorafter","threemonthsagotodate"],"AND",["systemnotes.newvalue","startswith","PROJECT-Closed"],"AND",["systemnotes.field","anyof","CUSTJOB.KENTITYSTATUS"]]]
		], 
		[
		   new nlobjSearchColumn("altname"), 
		   new nlobjSearchColumn("startdate"), 
		   new nlobjSearchColumn("entitystatus"), 
		   new nlobjSearchColumn("formuladate").setFormula("{enddate}"), 
		   new nlobjSearchColumn("entityid").setSort(false)
		]
		);*/