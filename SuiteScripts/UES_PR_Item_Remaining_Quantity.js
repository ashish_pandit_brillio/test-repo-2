/**
 * @author Shweta
 */
// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
/*
   	Script Name:
	Author:
	Company:
	Date:
	Description:


	Script Modification Log:

	-- Date --			-- Modified By --				--Requested By--				-- Description --



Below is a summary of the process controls enforced by this script file.  The control logic is described
more fully, below, in the appropriate function headers and code blocks.


     BEFORE LOAD
		- beforeLoadRecord(type)



     BEFORE SUBMIT
		- beforeSubmitRecord(type)


     AFTER SUBMIT
		- afterSubmitRecord(type)



     SUB-FUNCTIONS
		- The following sub-functions are called by the above core functions in order to maintain code
            modularization:

               - NOT USED

*/
}
// END SCRIPT DESCRIPTION BLOCK  ====================================



// BEGIN GLOBAL VARIABLE BLOCK  =====================================
{
	//  Initialize any Global Variables, in particular, debugging variables...




}
// END GLOBAL VARIABLE BLOCK  =======================================





// BEGIN BEFORE LOAD ==================================================

function beforeLoadRecord(type)
{

	/*  On before load:

          - EXPLAIN THE PURPOSE OF THIS FUNCTION

		-


		FIELDS USED:

          --Field Name--				--ID--


	*/



	//  LOCAL VARIABLES

    //  BEFORE LOAD CODE BODY


	return true;



}

// END BEFORE LOAD ====================================================





// BEGIN BEFORE SUBMIT ================================================

function beforeSubmitRecord(type)
{
    /*  On before submit:

          - PURPOSE
		-

          FIELDS USED:

          --Field Name--				--ID--


    */


    //  LOCAL VARIABLES


    //  BEFORE SUBMIT CODE BODY


	return true;

}

// END BEFORE SUBMIT ==================================================





// BEGIN AFTER SUBMIT =============================================

function afterSubmit_Item_Remaining_Qty(type)
{
  try
  {
  	var i_recordID = nlapiGetRecordId()
	
	var s_record_type = nlapiGetRecordType()
	
	var o_recordOBJ = nlapiLoadRecord(s_record_type,i_recordID)
	
	var o_old_recordOBJ = nlapiGetOldRecord();
	nlapiLogExecution('DEBUG', 'afterSubmit_Item_Remaining_Qty',' o_old_recordOBJ-->' + o_old_recordOBJ);	
  
   nlapiLogExecution('DEBUG', 'afterSubmit_Item_Remaining_Qty',' o_recordOBJ-->' + o_recordOBJ);	
  
	
	if(_logValidation(o_recordOBJ) && _logValidation(o_old_recordOBJ))
	{
		var i_line_item_count_old = o_old_recordOBJ.getLineItemCount('recmachcustrecord_purchaserequest')
		
		var i_line_item_count = o_recordOBJ.getLineItemCount('recmachcustrecord_purchaserequest')
		
		nlapiLogExecution('DEBUG', 'afterSubmit_Item_Remaining_Qty',' i_line_item_count_old-->' + i_line_item_count_old);	
  
      nlapiLogExecution('DEBUG', 'afterSubmit_Item_Remaining_Qty',' i_line_item_count-->' + i_line_item_count);	
  
	
		if(_logValidation(i_line_item_count_old) && _logValidation(i_line_item_count))
		{
		 
          for(var a=1;a<=i_line_item_count_old;a++)
		  {
		  	var i_SR_No_old = o_old_recordOBJ.getLineItemValue('recmachcustrecord_purchaserequest','custrecord_prlineitemno',a)
			nlapiLogExecution('DEBUG', 'afterSubmit_Item_Remaining_Qty',' SR No Old-->' + i_SR_No_old);	
  
			var i_item_old = o_old_recordOBJ.getLineItemValue('recmachcustrecord_purchaserequest','custrecord_prmatgrpcategory',a)
			nlapiLogExecution('DEBUG', 'afterSubmit_Item_Remaining_Qty',' Item Old -->' + i_item_old);	
  			
		  	 var i_quantity_old = o_old_recordOBJ.getLineItemValue('recmachcustrecord_purchaserequest','custrecord_quantity',a)
			 nlapiLogExecution('DEBUG', 'afterSubmit_Item_Remaining_Qty',' Quantity Old -->' + i_quantity_old);
		 
              for(var b=1;b<=i_line_item_count;b++)
			  {
			  	var i_SR_No= o_recordOBJ.getLineItemValue('recmachcustrecord_purchaserequest','custrecord_prlineitemno',b)
				nlapiLogExecution('DEBUG', 'afterSubmit_Item_Remaining_Qty',' SR No -->' + i_SR_No);	
	  
				var i_item = o_recordOBJ.getLineItemValue('recmachcustrecord_purchaserequest','custrecord_prmatgrpcategory',b)
				nlapiLogExecution('DEBUG', 'afterSubmit_Item_Remaining_Qty',' Item-->' + i_item);	
	  			
			  	var i_quantity = o_recordOBJ.getLineItemValue('recmachcustrecord_purchaserequest','custrecord_quantity',b)
				nlapiLogExecution('DEBUG', 'afterSubmit_Item_Remaining_Qty',' Quantity -->' + i_quantity);
		  	
			    if((i_SR_No_old == i_SR_No) && (i_item_old == i_item))
				{
				
				    i_remaining_quantity = parseInt(i_quantity_old)- parseInt(i_quantity)	
				//	o_recordOBJ.setLineItemValue('recmachcustrecord_purchaserequest','custrecord_qtyremain',i_remaining_quantity,b)
				    nlapiLogExecution('DEBUG', 'afterSubmit_Item_Remaining_Qty',' Quantity Remaining -->' + i_remaining_quantity);
		  		  
				    var i_PR_Item_recordID = search_PR_Item_recordID(i_recordID,i_SR_No,i_item)
				    nlapiLogExecution('DEBUG', 'afterSubmit_Item_Remaining_Qty',' PR Record ID -->' + i_PR_Item_recordID);
		  		  
				     if (_logValidation(i_PR_Item_recordID)) 
					  {
					  	var o_PR_Item_OBJ = nlapiLoadRecord('customrecord_pritem', i_PR_Item_recordID)
					  	
					  	if (_logValidation(o_PR_Item_OBJ)) 
						{
							o_PR_Item_OBJ.setFieldValue('custrecord_qtyremain',i_remaining_quantity,true)
							
							var i_PR_submitID = nlapiSubmitRecord(o_PR_Item_OBJ,true,true)
					        nlapiLogExecution('DEBUG', 'afterSubmit_Item_Remaining_Qty',' ********************** PR Item Submit ID ********************** -->' + i_PR_submitID);	
  	                 
						
					  	}
					  }
				  
				
				} //SR No & Item			 
			  	
			  }//Old Loop
				
			
		  }//Old Loop			
			
		}//Line Count		
		//var i_submitID = nlapiSubmitRecord(o_recordOBJ,true,true)
		 //nlapiLogExecution('DEBUG', 'afterSubmit_Item_Remaining_Qty',' ******************* Submit ID ************** -->' + i_submitID);
		  		
		
	}//Record OBJ  && Old Record OBJ	
  }//TRY
  catch(exception)
  {
  nlapiLogExecution('DEBUG', 'ERROR',' Exception Caught -->' + exception);	
  }//CATCH	
	return true;

}

// END AFTER SUBMIT ===============================================





// BEGIN FUNCTION ===================================================
function _logValidation(value) 
{
 if(value != null && value.toString() != null && value != '' && value != undefined && value.toString() != undefined && value != 'undefined' && value.toString() != 'undefined'&& value.toString() != 'NaN' && value != NaN) 
 {
  return true;
 }
 else 
 { 
  return false;
 }
}


function search_PR_Item_recordID(i_recordID,i_SR_No_QA,i_item_QA)
{
	
   var i_internal_id;
   
   var filter = new Array();
   filter[0] = new nlobjSearchFilter('custrecord_purchaserequest', null, 'is',i_recordID);
   filter[1] = new nlobjSearchFilter('custrecord_prlineitemno', null, 'is',i_SR_No_QA);
   filter[2] = new nlobjSearchFilter('custrecord_prmatgrpcategory', null, 'is',i_item_QA);
	
   var columns = new Array();
   columns[0] = new nlobjSearchColumn('internalid');
   
   var a_seq_searchresults = nlapiSearchRecord('customrecord_pritem',null,filter,columns);
	
	if (a_seq_searchresults != null && a_seq_searchresults != '' && a_seq_searchresults != undefined)
	{	
		i_internal_id = a_seq_searchresults[0].getValue('internalid');
		nlapiLogExecution('DEBUG', 'search_quotation_analysis_recordID', ' Internal ID -->' + i_internal_id);
	}
	
	return i_internal_id;
	
}
// END FUNCTION =====================================================
