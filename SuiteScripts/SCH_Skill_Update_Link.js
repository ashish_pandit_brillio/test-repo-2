/**
 * Module Description
 * 
 * Version Date Author Remarks 1.00 Apr 17 2015 Nitish Mishra
 * 
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your
 * script deployment.
 * 
 * @appliedtorecord recordType
 * 
 * @param {String}
 *        type Operation types: create, edit, delete, xedit, approve, cancel,
 *        reject (SO, ER, Time Bill, PO & RMA only) pack, ship (IF only)
 *        dropship, specialorder, orderitems (PO only) paybills (vendor
 *        payments)
 * @returns {Void}
 */
function scheduled() {

try {
		var cols = [];
		cols.push(new nlobjSearchColumn('internalid'));
		cols.push(new nlobjSearchColumn('email'));
		cols.push(new nlobjSearchColumn('firstname'));
		var searchResults = searchRecord('employee',2752,null,cols);
		
		if(searchResults){
			for(var i=0;i<searchResults.length;i++){
              if(i<498)
                {
                  
                }
              else
                {
				var emp_id  =searchResults[i].getValue('internalid');
				// get mail content as per the person type
				//if (doesEmailContainsBrillio(employeeDetails.email)) 
				{
					mailContent = getSalariedMailTemplate(searchResults[i].getValue('firstname'));
				}
				
				// send email to the new joinee
				if (isNotEmpty(mailContent)) {
					var file_obj = nlapiLoadFile('898070');
					nlapiSendEmail(129799, searchResults[i].getValue('email'),
							mailContent.Subject, mailContent.Body, null, 'sai.vannamareddy@brillio.com', {
								entity : parseInt(emp_id)
							},null);
                                        //sendNewJoineeEmail(employeeId);
				}
				else {
					throw "Invalid Person Type";
				}
				nlapiLogExecution('Debug','Processed Count',emp_id);
            }
				}
			}
		}
	
	catch (err) {
		nlapiLogExecution('error', 'userEventAfterSubmit', err);
		throw err;
	}
}

function doesEmailContainsBrillio(email) {

	email = email.toLowerCase();
	return email.indexOf("@brillio.com") > -1;
}

function getSalariedMailTemplate(employeeDetails) {

	var htmlText = '<style> p,li {font-size : 11; font-family : "verdana"}</style>';
	htmlText += '<p>Hi ' + employeeDetails + ',<p>';
	htmlText +=
			
					'<p>Please visit the below link to update your Skills and certification.If you have provided your skills in NetSuite earlier, we have migrated them to the new structure and request you to take couple of minutes to update the proficiency.</p>'
					+ '<p>This skill & certification information will be used to find the right matching positions for your future assignment.</br>'
					+ 'Please complete this activity by 19-Apr-19 without fail.</p>'
					+ '<a href=https://system.na1.netsuite.com/app/site/hosting/scriptlet.nl?script=1818&deploy=1>Skills and certification</a>'
					+ '<br/>'
					+'<p>Note: For any queries, reach out: talentmanagement@brillio.com </p>'
					+'<p>Thanks,<br/>' + 'Fulfillment Team</p>';

	return {
		Subject : 'Skill Update',
		Body : addMailTemplate(htmlText)
	};
}
