function afterSubmit(type)
{
	try
	{
		if(type == 'edit')
		{
			var Sysdate = new Date();
			var oldRec = nlapiGetOldRecord();
			var oldProjStatus = oldRec.getFieldValue('entitystatus');
			
			var projStatus = nlapiGetFieldValue('entitystatus');
			var projectManager  = nlapiGetFieldValue('custentity_projectmanager');
			var deliveryManager  = nlapiGetFieldValue('custentity_deliverymanager');
			var projectId  = nlapiGetFieldValue('entityid');
			var projectName  = nlapiGetFieldValue('altname');
			var billingType = nlapiGetFieldValue('jobbillingtype');
			
			var pm_email = nlapiLookupField('employee',projectManager,['email','firstname']);
			var dm_email = nlapiLookupField('employee',deliveryManager,'email');
			
			if(oldProjStatus == 4 && projStatus == 2 && billingType == 'FBM')
			{
				var billingschedule = nlapiGetFieldValue('billingschedule');
				nlapiLogExecution('Debug','Billing Schedule',billingschedule);
				if(_logValidation(billingschedule))
				{
					var loadRec = nlapiLoadRecord('billingschedule',billingschedule);
					var lineNum = loadRec.getLineItemCount('milestone');
					for(var i = 1; i <= lineNum ; i++)
					{
						var milestoneDate = loadRec.getLineItemValue('milestone','milestonedate',i);
						var milestoneDesc = loadRec.getLineItemText('milestone','projecttask',i);
						var milestoneAmt = loadRec.getLineItemValue('milestone','comments',i);
						var milestoneCompleted = loadRec.getLineItemValue('milestone','milestonecompleted',i);
						var date = nlapiStringToDate(milestoneDate);
						if(milestoneCompleted != 'T')
						{
							if(date < Sysdate)
							{
							sendActiveReminderMails(
								pm_email.email,
								pm_email.firstname,
								dm_email,
								milestoneDate,
								milestoneDesc,
								milestoneAmt,
								projectId,
								projectName);
							}			
						}
					}
				}
			}
		}
	}
	catch (e)
	{
		nlapiLogExecution('Debug','Error',e)
	}
}

function sendActiveReminderMails(pm_email, pm_name, dm_email, milestone_date, milestone_task,milestone_value,projID, projName)
{
	
	try{       
		//var cc = [];
		//cc[0] = 'billing@brillio.com';
		//cc[1] = dm_email;
		
		var mailTemplate = serviceTemplate(pm_email, pm_name, dm_email, milestone_date, milestone_task,milestone_value,projID, projName);
		nlapiLogExecution('debug', 'checkpoint',pm_email);
		nlapiLogExecution('debug', 'mailTemplate',mailTemplate.MailBody);
		nlapiSendEmail(175491, pm_email, mailTemplate.MailSubject, mailTemplate.MailBody,dm_email,['billing@brillio.com',"prabhat.gupta@brillio.com","sridhar.v@brillio.com"],null,null,null);
		nlapiLogExecution('debug', 'mailSent to ->',pm_email);
		
	}
catch(err){
          nlapiLogExecution('error', 'sendServiceMails', err);

     }
}

function _logValidation(value) 
{
 if(value != null && value.toString() != null && value != '' && value != undefined && value.toString() != undefined && value != 'undefined' && value.toString() != 'undefined'&& value.toString() != 'NaN' && value != NaN) 
 {
  return true;
 }
 else 
 { 
  return false;
 }
}

function serviceTemplate(pm_email, pm_name, dm_email, milestone_date, milestone_task, milestone_value,projID, projName)
{
    var htmltext = '';
    
   htmltext += '<table border="0" width="100%"><tr>';
   htmltext += '<td colspan="4" valign="top">';
  
   htmltext += '<p>Hi  ' + pm_name + ',</p>';
   
   htmltext += '<p> We would like to inform you that below milestone is due for invoicing for project  '+projID + ' '+projName+'. If milestone are achieved and services are rendered then pls provide approval by replying on same email so we can generate invoice and send you for final review and approval before sending it to customer</p>';
 
   htmltext += '<p><b>Due Date :' + milestone_date + '</b></p>';
   htmltext += '<p><b>Milestone Description  :' + milestone_task + '</b></p>';
   htmltext += '<p><b>Amount :' + milestone_value + '</b></p>';
   
   htmltext += '<p>Thanks & Regards,</p>';
   htmltext += '<p>Brillio Billing Team</p>';
 
 
    htmltext += '</td></tr>';
    htmltext += '</table>';
    htmltext += '<hr width="100%" size="1" noshade color="#CCCCCC">';
    htmltext += '<table width="100%" border="0" cellspacing="0" cellpadding="0">';
    htmltext += '<tr>';
    htmltext += '<td align="right">';
    htmltext += '</td>';
    htmltext += '</tr>';
    htmltext += '</table>';
    htmltext += '</body>';
    htmltext += '</html>';
 
    return {
        MailBody : htmltext,
        MailSubject : "Reminder of milestone due for project "+projID + " " +projName + "- dated " +milestone_date
    };
}