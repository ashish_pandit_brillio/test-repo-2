/**
 * @author Jayesh
 */

function autoCheckInvoiceTimesheets()
{
	try
	{	
		var i_context = nlapiGetContext();
		var i_recordID = i_context.getSetting('SCRIPT', 'custscript_invoice_id_to_update');
		nlapiLogExecution('audit','invoice id:- ',i_recordID);
		//nlapiSubmitField('invoice',i_recordID,'custbody_invoice_auto_check_unabled','F');
		//return;
		var o_record_obj = nlapiLoadRecord('invoice',parseInt(i_recordID));
		if(o_record_obj)
		{
			var d_billingFrom = o_record_obj.getFieldValue('custbody_billfrom');
			var d_billingTo = o_record_obj.getFieldValue('custbody_billto');
			
			d_billingFrom = nlapiStringToDate(d_billingFrom);
			d_billingTo = nlapiStringToDate(d_billingTo);
			
			var i_billable_time_count = o_record_obj.getLineItemCount('time');
			
			for (var i = 1; i < i_billable_time_count; i++)
			{
                
				var d_billedDate = nlapiStringToDate(o_record_obj.getLineItemValue('time', 'billeddate', i));
	
				if (d_billingFrom <= d_billedDate && d_billingTo >= d_billedDate)
				{
					var item_present = o_record_obj.getLineItemValue('time', 'item', i);
					if(item_present == 2222 || item_present == 2425)
					{
						o_record_obj.setLineItemValue('time','apply',i,'T');
						o_record_obj.setLineItemValue('time', 'taxcode', i, 2601);
					}
				}
			}
			nlapiLogExecution('audit','Auto Check Completed');
			o_record_obj.setFieldValue('custbody_invoice_auto_check_unabled','F');
			var id = nlapiSubmitRecord(o_record_obj,{"disableTriggers" : true},true);
			nlapiLogExecution('audit','invoice id submited:- ',id);
		}
	}
	catch(err)
	{
		nlapiLogExecution('ERROR','ERROR MESSAGE:-- ',err);
        var context=nlapiGetContext();

					var mailTemplate = "";
					mailTemplate += "<p> This is to inform that Invoice Auto Check Timesheets has been failed.And It is having Error - <b>["+err.code+" : "+err.message +"]</b> and therefore, system is not able to the send the information.</p>";

					mailTemplate += "<p> Kindly have a look on to the error message and Please do needfull.</p>";
					mailTemplate += "<p><b> Script Id:- "+context.getScriptId() +"</b></p>";
					mailTemplate += "<p><b> Script Deployment Id:- "+context.getDeploymentId() +"</b></p>";
					mailTemplate += "<p> Note: This is system genarated email, incase of any failure while sending the notification to employee(s).</p>";
					mailTemplate += "<br/>"
					mailTemplate += "<p>Regards, <br/> Information Systems</p>";
					nlapiSendEmail(442,'netsuite.support@brillio.com', 'Failure Notification on Invoice Auto Check Timesheets ',mailTemplate, null, null, null, null);
	}
}