/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       21 Jan 2021     shravan.k    Initial Development
 *
 */

/**
 * @param {String} type Context Types: scheduled, on-demand, userinterface, aborted, skipped
 * @returns {Void}
 */
function mail_Midweek_RA_Ending(type) 
{
	try /// Main Function
	{
		var obj_Usage_Context = nlapiGetContext();
		nlapiLogExecution('DEBUG', 'Begining Usage  ==',obj_Usage_Context.getRemainingUsage());
		var d_Start_Date = nlapiDateToString(new Date());
		nlapiLogExecution('DEBUG', 'd_Start_Date ==',d_Start_Date);
		var d_End_Date = nlapiDateToString(nlapiAddDays(nlapiStringToDate(d_Start_Date), 14), 'date');
		nlapiLogExecution('DEBUG', 'd_End_Date ==',d_End_Date);
		var arr_Resource = [];
		var arr_Resource_names = [];
		var arr_Mail_index_Exclude = [];
		var arr_Resource_List_Due = [];
		//// Below search takes the gap of 15 days and returns Resource allocations which are ending on mid week
		var obj_Resource_allocation_Search = nlapiLoadSearch('resourceallocation', 'customsearch3546');
		/*var obj_Resource_allocation_Search = nlapiSearchRecord("resourceallocation",null,
					[
					   ["enddate","on",d_End_Date], /// 14 Days from now
					   "AND", 
					   [["formulatext: TO_CHAR({enddate},'D')","doesnotstartwith","1"],"AND",["formulatext: TO_CHAR({enddate},'D')","doesnotstartwith","7"]]
					], 
					[
					   new nlobjSearchColumn("internalid"), 
					   new nlobjSearchColumn("resource")
					]
					);*/
			if(!obj_Resource_allocation_Search)
				{
					nlapiLogExecution('DEBUG', 'No Allocation ending midweek on this date ==','No Allocation ending midweek on this date');
				} /// End of if(!obj_Resource_allocation_Search)
		if(obj_Resource_allocation_Search)
			{
					nlapiLogExecution('DEBUG', 'obj_Resource_allocation_Search.length ==',obj_Resource_allocation_Search.length);
					var i_Resource_Id ;
					var s_Resource_Name;
					var i_Prev_Resource;
					var i_Current_resource;
					for(var i_Rs_index = 0; i_Rs_index < obj_Resource_allocation_Search.length ;  i_Rs_index++)	
						{
								if(obj_Usage_Context.getRemainingUsage() < 300)  //// Yield Script
									{
											nlapiLogExecution('DEBUG', 'Usage  ==',obj_Usage_Context.getRemainingUsage());	
						                     nlapiSetRecoveryPoint();
						                     nlapiYieldScript();   
									} ////// if(nlapiGetContext().getRemainingUsage()<300)
								if(i_Rs_index == 0)
									{
										i_Resource_Id = obj_Resource_allocation_Search[i_Rs_index].getValue('resource');
										//nlapiLogExecution('DEBUG', 'i_Resource_Id ==',i_Resource_Id);
										s_Resource_Name = obj_Resource_allocation_Search[i_Rs_index].getText('resource');
										arr_Resource.push(i_Resource_Id);
										arr_Resource_names.push(s_Resource_Name);
									} ///if(i_Rs_index == 0)
								else
									{
											i_Prev_Resource = obj_Resource_allocation_Search[i_Rs_index].getValue('resource');
											i_Current_resource = obj_Resource_allocation_Search[i_Rs_index - 1].getValue('resource');
											if(i_Prev_Resource != i_Current_resource)
												{
													i_Resource_Id = obj_Resource_allocation_Search[i_Rs_index].getValue('resource');
													s_Resource_Name = obj_Resource_allocation_Search[i_Rs_index].getText('resource');
													arr_Resource.push(i_Resource_Id);
													arr_Resource_names.push(s_Resource_Name);
												} ///if(obj_Resource_allocation_Search[i_Rs_index] != obj_Resource_allocation_Search[i_Rs_index - 1])
									} //// Else of  if(i_Rs_index == 0)
								
						} /// End of for(var i_Rs_index = 0; i_Rs_index < obj_Resource_allocation_Search.length ;  i_Rs_index++)	
					nlapiLogExecution('DEBUG', 'arr_Resource ==',arr_Resource);
			
		var d_Start_date_next_Search =  nlapiDateToString(nlapiAddDays(nlapiStringToDate(d_End_Date), 1), 'date');
		nlapiLogExecution('DEBUG', 'd_Start_date_next_Search ==',d_Start_date_next_Search);
		if(arr_Resource.length != 0)
			{
			var obj_RA_Search_New = nlapiSearchRecord("resourceallocation",null,
					[
					   ["startdate","on",d_Start_date_next_Search], 
					   "AND", 
					   ["resource","anyof",arr_Resource]
					],
					[
					   new nlobjSearchColumn("id").setSort(false), 
					   new nlobjSearchColumn("resource")
					]
					);
			if(obj_RA_Search_New)
				{
						var i_New_Allocation_Resource;
						for(var i_new_RA_index = 0 ; i_new_RA_index < obj_RA_Search_New.length ; i_new_RA_index++)
							{
									if(obj_Usage_Context.getRemainingUsage() < 300)  //// Yield Script
									{
										nlapiLogExecution('DEBUG', 'Usage  ==',obj_Usage_Context.getRemainingUsage());	
										nlapiSetRecoveryPoint();
										nlapiYieldScript();   
									} ////// if(nlapiGetContext().getRemainingUsage()<300)
								i_New_Allocation_Resource = obj_RA_Search_New[i_new_RA_index].getValue('resource');
								//nlapiLogExecution('DEBUG', 'i_New_Allocation_Resource ==',i_New_Allocation_Resource);
								var i_Index_of_Resource = arr_Resource.indexOf(i_New_Allocation_Resource);
								//nlapiLogExecution('DEBUG', 'i_Index_of_Resource ==',i_Index_of_Resource);
								if(i_Index_of_Resource != -1)
									{
										arr_Mail_index_Exclude.push(i_Index_of_Resource);		
									} ///End of if(i_Index_of_Resource != -1)
							} ////  for(var i_new_RA_index = 0 ; i_new_RA_index < obj_RA_Search_New.length ; i_new_RA_index++)
				} ////if(obj_RA_Search_New)
					var s_Subject_Mail = "Resource allocation Ending on Mid-Week Dated "	+ d_End_Date;
					var obj_Body_Mail = Resource_Allocation_Template(arr_Mail_index_Exclude,arr_Resource,arr_Resource_names,d_End_Date);
					
					/* mail details  Sender : 442 : Information systems, Receiver : 15279 Business operations , Entity : 442 Information systems, CC: 83134  is On the go */
					//nlapiSendEmail('442','15279',s_Subject_Mail,obj_Body_Mail.Body,'83134', null, {'entity':442});
					nlapiLogExecution('DEBUG', ' obj_Body_Mail==',obj_Body_Mail.Body);
					//nlapiSendEmail('233787','233787',s_Subject_Mail,obj_Body_Mail.Body,null, null,null); /// For Prod test
					//nlapiSendEmail('203169','203169',s_Subject_Mail,obj_Body_Mail.Body,null, null,{'entity':'203169'}); //// for sandbox only
					
			} //// if(arr_Resource)
			} /// End of if(obj_Resource_allocation_Search)
	} ///End of try in main Function
catch(e) //// Main Function
{
	nlapiLogExecution('DEBUG', 'Exception in Main==', e);
} //// End of catch(e)
} //// End of function mail_Midweek_RA_Ending(type) 

function Create_Employee_Table(arr_Mail_index_Exclude,arr_Resource,arr_Resource_names)
{
	try
	{
		nlapiLogExecution('DEBUG', 'arr_Mail_index_Exclude.length ==',arr_Mail_index_Exclude.length);
		nlapiLogExecution('DEBUG', 'arr_Resource.length ==',arr_Resource.length);
		var s_Table_Resource = "<table>";
		s_Table_Resource += "<tr>";
		s_Table_Resource += "<td style='background:#E3E1E0'>Resource Name</td>";
		s_Table_Resource += "</tr>";
		for(var i_Mail_index = 0 ;  i_Mail_index <  arr_Resource.length  ; i_Mail_index++)
			{
			       if(arr_Mail_index_Exclude.length == 0)
			    	   {
	    	   				s_Table_Resource += "<tr>";
	    	   				s_Table_Resource += "<td>" +arr_Resource_names[i_Mail_index]  + "</td>";
	    	   				s_Table_Resource += "</tr>";
			    	   			
			    	   } //// if(arr_Mail_index_Exclude.length == 0)
			       else
			    	   {
			    	   		if(arr_Mail_index_Exclude.indexOf(i_Mail_index) == -1)
			    	   		{
			    	   			s_Table_Resource += "<tr>";
			    	   			s_Table_Resource += "<td>" +arr_Resource_names[i_Mail_index]  + "</td>";
			    	   			s_Table_Resource += "</tr>";
			    	   		} ///// if(arr_Mail_index_Exclude.indexOf(i_Mail_index) == -1)
			    	   } //// End of else of //// if(arr_Mail_index_Exclude.length == 0)
			       		
			} /// for(var i_Mail_index = 0 ;  i_Mail_index <  arr_Resource.length  ; i_Mail_index++)
		s_Table_Resource += "</table>";
		return {
			Body : s_Table_Resource
		}
	} /// End of try in function Create_Employee_Table(arr_Mail_index_Exclude,arr_Resource)
	catch(e)
	{
		nlapiLogExecution('DEBUG', 'Exception in Create_Employee_Table== ', e);
	} //// /// End of catch in function Create_Employee_Table(arr_Mail_index_Exclude,arr_Resource)
} ///// End of function Create_Employee_Table(arr_Mail_index_Exclude,arr_Resource)


function Resource_Allocation_Template(arr_Mail_index_Exclude,arr_Resource,arr_Resource_names,d_End_Date)
{
	try
	{
		var s_html_Text = '<p>Hi  Business Operations Team,</p>';

		s_html_Text += "<p>This is to Inform that below resources are having allocation which are ending on mid week that is " + d_End_Date +"</p>";
		s_html_Text += "<p> Kindly extend the allocation or create a new allocation for resources to avoid issues on timesheet submissions. </p>"
		var Table_resource_Name = Create_Employee_Table(arr_Mail_index_Exclude,arr_Resource,arr_Resource_names);
		s_html_Text +=  Table_resource_Name.Body
		nlapiLogExecution('DEBUG', ' Table_resource_Name.Body==',  Table_resource_Name.Body);
		s_html_Text += '<p>Regards,<br/>';
		s_html_Text += 'Information Systems</p>';
		
		return {
			Body : s_html_Text
		}
	} ////// End of try in Resource_Allocation_Template
	catch(e)
	{
		nlapiLogExecution('DEBUG', 'Exception in Resource_Allocation_Template== ', e);
	} /// ////// End of catch in Resource_Allocation_Template

} /// End of function Resource_Allocation_Template(arr_Mail_index_Exclude,arr_Resource,arr_Resource_names,d_End_Date)