// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
/*
   	Script SUT_DisplayProjectDetailsT&M.js
	Author: Ashish Pandit	
	Date: 08/05/2018
    Description: Suitelet to show T&M project forecast   


	Script Modification Log:

	-- Date --			-- Modified By --				--Requested By--				-- Description --
	
	Below is a summary of the process controls enforced by this script file.  The control logic is described
	more fully, below, in the appropriate function headers and code blocks.

   
     SUB-FUNCTIONS
		- The following sub-functions are called by the above core functions in order to maintain code
            modularization:

		scheduled_SendEmailForApproval()



*/
}
// END SCRIPT DESCRIPTION BLOCK  ====================================


function suiteletFunction_t_nd_m(request,rsponse)
{
	try
	{
		if(request.getMethod() == 'GET')
			{
				var o_context = nlapiGetContext();
				var i_user_logegdIn_id = o_context.getUser();
				var i_projectId = request.getParameter('proj_id');
              var i_projectId = 128288;
				var practices_involved=new Array(); 
				var unique_list=new Array();
				var cost_array=new Array();
				
				if(i_projectId) // check if project id is present
				{
				
				//i_projectId = i_projectId.trim();
				
				var o_project_object = nlapiLoadRecord('job',i_projectId); // load project record object
				
				// get necessary information about project from project record
				var s_project_region = o_project_object.getFieldValue('custentity_region');
				var i_customer_name = o_project_object.getFieldValue('parent');
				var s_project_name = o_project_object.getFieldValue('companyname');
				var d_proj_start_date = o_project_object.getFieldValue('startdate');
				var original_start_date = d_proj_start_date;
				var d_todays_date = new Date();
						
				var d_proj_start_date = nlapiDateToString(d_todays_date);
				
				var d_proj_end_date = o_project_object.getFieldValue('enddate');
				var i_proj_executing_practice = o_project_object.getFieldValue('custentity_practice');
				var i_proj_manager = o_project_object.getFieldValue('custentity_projectmanager');
				var i_proj_manager_practice = nlapiLookupField('employee',i_proj_manager,'department');
				var i_project_sow_value = o_project_object.getFieldValue('custentity_projectvalue');
				var i_proj_revenue_rec_type = o_project_object.getFieldValue('custentity_fp_rev_rec_type');
				var s_proj_currency = o_project_object.getFieldText('custentity_project_currency');//entityid
				var s_project_id = o_project_object.getFieldValue('entityid');
				var billing_sch=o_project_object.getFieldValue('billingschedule');
				var d_proj_strt_date_old_proj = o_project_object.getFieldValue('custentity_rev_rec_new_strt_date');
				
				// design form which will be displayed to user
				var i_project_value_ytd = o_project_object.getFieldValue('custentity_ytd_rev_recognized');
				var d_proj_end_date_old_proj = o_project_object.getFieldValue('custentity_rev_rec_new_end_date');
				var charge_back=o_project_object.getFieldValue('custentity_chargeback');
				var ip_cost=o_project_object.getFieldValue('custentity_ip_cost');
				var withhold=o_project_object.getFieldValue('custentity_withhold');
				var other_costs=o_project_object.getFieldValue('custentity_other_costs');
				//nlapiLogExecution('Debug','Load Record');
				if(!i_project_value_ytd)
					i_project_value_ytd = 0;
				
				i_project_sow_value = parseFloat(i_project_sow_value) - parseFloat(i_project_value_ytd);
				i_project_sow_value = parseFloat(i_project_sow_value).toFixed(2);
				var o_form_obj = nlapiCreateForm("T&M Project Details Page");
				
				// call client script to perform validation check
				//o_form_obj.setScript('customscript_cli_tndm_validation');
				
				// create region field
				var fld_region_field = o_form_obj.addField('proj_region', 'select','Region','customrecord_region').setDisplayType('inline');
				fld_region_field.setDefaultValue(s_project_region);
				
				// create customer field
				var fld_customer_field = o_form_obj.addField('customer_selected', 'select','Customer','customer').setDisplayType('inline');
				fld_customer_field.setDefaultValue(i_customer_name);
				
				// create project field
				var fld_proj_field = o_form_obj.addField('proj_selected', 'select','Project','job').setDisplayType('inline');
				fld_proj_field.setDefaultValue(i_projectId);
				
				// create project sow value field
				var fld_proj_sow_value = o_form_obj.addField('proj_sow_value', 'text','Project SOW value').setDisplayType('inline');
				fld_proj_sow_value.setDefaultValue(i_project_sow_value);	
				
				// create field for project start date
				var fld_proj_start_date = o_form_obj.addField('proj_strt_date', 'date','Project Start Date').setDisplayType('inline');
				fld_proj_start_date.setDefaultValue(original_start_date);
				
				// create field for projet end date
				var fld_proj_end_date = o_form_obj.addField('proj_end_date', 'date','Project End Date').setDisplayType('inline');
				fld_proj_end_date.setDefaultValue(d_proj_end_date);
				
				// create field for projet currency
				var fld_proj_end_date = o_form_obj.addField('proj_currency', 'text','Project Currency').setDisplayType('inline');
				fld_proj_end_date.setDefaultValue(s_proj_currency);
				
				// create field for project practice
				var fld_proj_practice = o_form_obj.addField('proj_practice', 'select','Project Executing Practice','department').setDisplayType('inline');
				fld_proj_practice.setDefaultValue(i_proj_executing_practice);
				
				// create field for poject manager practice field
				var fld_proj_manager_practice = o_form_obj.addField('proj_manager_practice', 'select','Project Manager Practice','department').setDisplayType('inline');
				fld_proj_manager_practice.setDefaultValue(i_proj_manager_practice);
				
				// create custom_record field
				var fld_custom_record_id = o_form_obj.addField('custome_record', 'text','Custom Reocrd').setDisplayType('hidden');
				
				//field to find is value changed or not
				var fld_existing_revenue_share_changed = o_form_obj.addField('change_triggered', 'checkbox','').setDisplayType('inline');
				fld_existing_revenue_share_changed.setDefaultValue('F');
				//Fld to capture amount change
				var fld_subPractice_Total = o_form_obj.addField('sub_practice_total', 'textarea','Sub Practice Total').setDisplayType('hidden');
				var fld_amt_total = o_form_obj.addField('amt_total', 'text','Amount Total').setDisplayType('hidden');
				var fld_practices_involved = o_form_obj.addField('amt_practices_involved', 'text','practices_involved').setDisplayType('hidden');
				
				var practices_involved = new Array();		
					
					
					//nlapiLogExecution('debug','d_proj_start_date=',d_proj_start_date);
							
							
					var tab_revenue_share = o_form_obj.addSubTab('people_plan', 'Allocation Details');
					//Create 2nd Subtab
					var fld_sublist_effort_plan_mnth_end = o_form_obj.addSubList('people_plan_sublist', 'inlineeditor', 'Effort Plan Month End', 'people_plan');
					var fld_is_allocation_row = fld_sublist_effort_plan_mnth_end.addField('is_resource_allo_row', 'text','Is Allocation Row').setDisplayType('inline').setDefaultValue('<html><body bgcolor="red"></body></html>');
					var fld_practice = fld_sublist_effort_plan_mnth_end.addField('parent_practice_month_end', 'text', 'Practice').setDisplayType('disabled');
					fld_sublist_effort_plan_mnth_end.addField('sub_practice_month_end', 'select', 'Sub Practice', 'department').setDisplayType('disabled');
					fld_sublist_effort_plan_mnth_end.addField('role_month_end', 'text', 'Role').setDisplayType('disabled');
					var fld_emp_level = fld_sublist_effort_plan_mnth_end.addField('level_month_end', 'select', 'Level').setDisplayType('disabled'); //customlist_fp_rev_rec_level
										
								fld_emp_level.addSelectOption('', '');
								fld_emp_level.addSelectOption('31', '0');
								fld_emp_level.addSelectOption('1', '1');
								//fld_emp_level.addSelectOption('15', '1A');
								//fld_emp_level.addSelectOption('16', '1B');
								fld_emp_level.addSelectOption('2', '2');
								//fld_emp_level.addSelectOption('17', '2A');
								//fld_emp_level.addSelectOption('18', '2B');
								fld_emp_level.addSelectOption('3', '3');
								//fld_emp_level.addSelectOption('10', '3A');
								//fld_emp_level.addSelectOption('19', '3B');
								fld_emp_level.addSelectOption('4', '4');
								//fld_emp_level.addSelectOption('20', '4A');
								//fld_emp_level.addSelectOption('21', '4B');
								fld_emp_level.addSelectOption('5', '5');
								//fld_emp_level.addSelectOption('22', '5A');
								//fld_emp_level.addSelectOption('23', '5B');
								fld_emp_level.addSelectOption('6', '6');
								//fld_emp_level.addSelectOption('11', '6A');
								//fld_emp_level.addSelectOption('14', '6B');
								fld_emp_level.addSelectOption('7', '7');
								//fld_emp_level.addSelectOption('24', '7A');
								//fld_emp_level.addSelectOption('25', '7B');
								fld_emp_level.addSelectOption('8', '8');
								//fld_emp_level.addSelectOption('26', '8A');
								//fld_emp_level.addSelectOption('27', '8B');
								fld_emp_level.addSelectOption('9', '9');
								//fld_emp_level.addSelectOption('28', '9A');
								//fld_emp_level.addSelectOption('29', '9B');
								fld_emp_level.addSelectOption('13', 'CW');
								fld_emp_level.addSelectOption('30', 'PT');
								fld_emp_level.addSelectOption('12', 'TS');
							
					var fld_location_mnth_end = fld_sublist_effort_plan_mnth_end.addField('location_month_end', 'text', 'Location').setDisplayType('disabled');
					
					fld_sublist_effort_plan_mnth_end.addField('resource_cost_month_end', 'float', 'Bill Rate (USD)').setDisplayType('disabled');
					//fld_sublist_effort_plan_mnth_end.addField('current_month', 'float', 'Current Month').setDisplayType('disabled');
					
					// Add subtab for Ramp up/down
					var tab_revenue_share = o_form_obj.addSubTab('revenue_share_tab', 'Plan Revenue Share');
					var fld_sublist_effort_plan_mnth_end_next_tab = o_form_obj.addSubList('people_plan_sublist1', 'inlineeditor', 'Effort Plan Month End', 'revenue_share_tab');
				
					fld_sublist_effort_plan_mnth_end_next_tab.addField('ramp_list', 'select', 'Ramp List', 'customlist_ramp_up_down_list');
					
					var practices_selected=fld_sublist_effort_plan_mnth_end_next_tab.addField('select_practices','select', 'Practice');					
						
					fld_sublist_effort_plan_mnth_end_next_tab.addField('text_note', 'text', 'Note');
					
			
			//========================= Search To Get JSON Data =======================================================================
					var customrecord_tm_month_total_jsonSearch = nlapiSearchRecord("customrecord_tm_month_total_json",null,
					[
					   ["custrecord_project_json","anyof",i_projectId.toString()], 
					   "AND", 
					   ["custrecord_is_confirm","is","T"]
					], 
					[
					   new nlobjSearchColumn("custrecord_project_json"), 
					   new nlobjSearchColumn("custrecord_json1"), 
					   new nlobjSearchColumn("custrecord_json2"), 
					   new nlobjSearchColumn("custrecord_json3"), 
					   new nlobjSearchColumn("custrecord_json4"), 
					   new nlobjSearchColumn("custrecord_rampupjson1"), 
					   new nlobjSearchColumn("custrecord_rampupjson2"), 
					   new nlobjSearchColumn("custrecord_rampupjson3"), 
					   new nlobjSearchColumn("custrecord_rampupjson4"), 
					   new nlobjSearchColumn("created").setSort(true)
					]
					);
					
						var monthBreakUp = getMonthsBreakup(
						nlapiStringToDate(d_proj_start_date),
						nlapiStringToDate(d_proj_end_date));
					
					//================================================================================================
					
					nlapiLogExecution('Debug',' monthBreakUp.length', monthBreakUp.length);	
					for (var j = 0; j < monthBreakUp.length; j++)
					{
						var months = monthBreakUp[j];
						var s_month_name = getMonthName(months.Start); // get month name
						var month_strt = nlapiStringToDate(months.Start);
						var month_name_yr = month_strt.getMonth();
						var month_end = nlapiStringToDate(months.End);
						var i_month = month_strt.getMonth();
						var i_year = month_strt.getFullYear();
						s_month_name = s_month_name +'_'+ i_year;
						var d_today_date = new Date();
						var today_month_yr = d_today_date.getMonth();
						var prev_year = d_today_date.getFullYear();
					
						fld_sublist_effort_plan_mnth_end.addField(''+s_month_name.toLowerCase()+'_first', 'text', ' ').setDisplayType('disabled');//.setDisplayType('disabled');
						fld_sublist_effort_plan_mnth_end.addField(''+s_month_name.toLowerCase()+'_second', 'text', ''+s_month_name).setDisplayType('disabled');//.setDisplayType('disabled');
						fld_sublist_effort_plan_mnth_end.addField(''+s_month_name.toLowerCase()+'_third', 'text', ' ').setDisplayType('disabled');//.setDisplayType('disabled');
						fld_sublist_effort_plan_mnth_end.setLineItemValue(''+s_month_name.toLowerCase()+'_first',1,'FTE');
						fld_sublist_effort_plan_mnth_end.setLineItemValue(''+s_month_name.toLowerCase()+'_second',1,'Working Days');
						fld_sublist_effort_plan_mnth_end.setLineItemValue(''+s_month_name.toLowerCase()+'_third',1,'$');
						
						nlapiLogExecution('Debug','i_month '+i_month,'today_month_yr '+today_month_yr);
						nlapiLogExecution('Debug','i_year '+i_year,'prev_year '+prev_year);
						
						if(i_month < today_month_yr && i_year <= prev_year)
								{
									fld_sublist_effort_plan_mnth_end_next_tab.addField(''+s_month_name.toLowerCase()+'_second', 'float', ''+s_month_name).setDisplayType('disabled');
									
								}
								else
								{
									fld_sublist_effort_plan_mnth_end_next_tab.addField(''+s_month_name.toLowerCase()+'_second', 'float', ''+s_month_name);
								}
						//fld_sublist_effort_plan_mnth_end_next_tab.addField(''+s_month_name.toLowerCase()+'_second', 'text', ''+s_month_name);//.setDisplayType('disabled');
					}
					
					//===================================================Search for existing month data=====================================================================
					
					var d_today_date = new Date();
					var i_month = d_today_date.getMonth();
					//nlapiLogExecution('Debug','i_month'+i_month,'i_projectId'+i_projectId);
					
					if(_logValidation(customrecord_tm_month_total_jsonSearch))
					{
						//nlapiLogExecution('Debug','customrecord_tm_month_total_jsonSearch',customrecord_tm_month_total_jsonSearch.length);
						//nlapiLogExecution('Debug','customrecord_tm_month_total_jsonSearch[0].getId()',customrecord_tm_month_total_jsonSearch[0].getId());
						var o_mnth_end_effrt_prev_mnth = nlapiLoadRecord('customrecord_tm_month_total_json', customrecord_tm_month_total_jsonSearch[0].getId());
						var s_effrt_json_1_prev_mnth = o_mnth_end_effrt_prev_mnth.getFieldValue('custrecord_rampupjson1');
						var s_effrt_json_2_prev_mnth = o_mnth_end_effrt_prev_mnth.getFieldValue('custrecord_rampupjson2');
						var s_effrt_json_3_prev_mnth = o_mnth_end_effrt_prev_mnth.getFieldValue('custrecord_rampupjson3');
						var s_effrt_json_4_prev_mnth = o_mnth_end_effrt_prev_mnth.getFieldValue('custrecord_rampupjson4');
						// for printing next lines
						var i_Counter = 0;
						if(s_effrt_json_1_prev_mnth)
						{
							var s_entire_json_clubed = JSON.parse(s_effrt_json_1_prev_mnth);
							for (var i_mnth_end_plan = 0; i_mnth_end_plan < s_entire_json_clubed.length; i_mnth_end_plan++)
							{
								var a_row_json_data = JSON.parse(JSON.stringify(s_entire_json_clubed[i_mnth_end_plan]));
							
								for (var i_row_json_index = 0; i_row_json_index < a_row_json_data.length; i_row_json_index++)
								{
									var i_ramp_list = a_row_json_data[i_row_json_index].rampList;
									var i_sub_practice = a_row_json_data[i_row_json_index].subprac;
									var s_sub_practice = a_row_json_data[i_row_json_index].subprac_text;
									var s_note = a_row_json_data[i_row_json_index].note;
									var total_revenue = a_row_json_data[i_row_json_index].allo;
									var s_mnth = a_row_json_data[i_row_json_index].mnth;
									var s_year = a_row_json_data[i_row_json_index].year;
									var s_month_name = s_mnth + '_' + s_year;
									if(i_row_json_index==0)
									{
										//nlapiLogExecution('Debug','i_ramp_list'+i_ramp_list,'i_sub_practice'+i_sub_practice);
										fld_sublist_effort_plan_mnth_end_next_tab.setLineItemValue('ramp_list',i_mnth_end_plan+1,i_ramp_list);
										fld_sublist_effort_plan_mnth_end_next_tab.setLineItemValue('select_practices',i_mnth_end_plan+1,i_sub_practice);
										fld_sublist_effort_plan_mnth_end_next_tab.setLineItemValue('text_note',i_mnth_end_plan+1,s_note);
									}
									fld_sublist_effort_plan_mnth_end_next_tab.setLineItemValue(''+s_month_name.toLowerCase()+'_second',i_mnth_end_plan+1,parseFloat(total_revenue));
									nlapiSetLineItemDisabled('people_plan_sublist1', ''+s_month_name.toLowerCase()+'_second', true, 1);
									//nlapiLogExecution('Debug','+s_month_name.toLowerCase()+_second===='+''+s_month_name.toLowerCase()+'_second','total_revenue===='+total_revenue);
									
									//nlapiSetLineItemValue('people_plan_sublist1','ramp_list',i_row_json_index,total_revenue);
								}
								i_Counter = i_mnth_end_plan;
							}
							nlapiLogExecution('Debug','i_Counter',i_Counter);
						}
						if(s_effrt_json_2_prev_mnth)
						{
							nlapiLogExecution('Debug','s_effrt_json_2_prev_mnth',s_effrt_json_2_prev_mnth);
							var s_entire_json_clubed = JSON.parse(s_effrt_json_2_prev_mnth);
							for (var i_mnth_end_plan = 0; i_mnth_end_plan < s_entire_json_clubed.length; i_mnth_end_plan++)
							{
								var a_row_json_data = JSON.parse(JSON.stringify(s_entire_json_clubed[i_mnth_end_plan]));
								i_Counter++;
								for (var i_row_json_index = 0; i_row_json_index < a_row_json_data.length; i_row_json_index++)
								{
									var i_ramp_list = a_row_json_data[i_row_json_index].rampList;
									var i_sub_practice = a_row_json_data[i_row_json_index].subprac;
									var s_sub_practice = a_row_json_data[i_row_json_index].subprac_text;
									var s_note = a_row_json_data[i_row_json_index].note;
									var total_revenue = a_row_json_data[i_row_json_index].allo;
									var s_mnth = a_row_json_data[i_row_json_index].mnth;
									var s_year = a_row_json_data[i_row_json_index].year;
									var s_month_name = s_mnth + '_' + s_year;
									if(i_row_json_index==0)
									{
										//nlapiLogExecution('Debug','i_ramp_list'+i_ramp_list,'i_sub_practice'+i_sub_practice);
										fld_sublist_effort_plan_mnth_end_next_tab.setLineItemValue('ramp_list',i_Counter+1,i_ramp_list);
										fld_sublist_effort_plan_mnth_end_next_tab.setLineItemValue('select_practices',i_Counter+1,i_sub_practice);
										fld_sublist_effort_plan_mnth_end_next_tab.setLineItemValue('text_note',i_Counter+1,s_note);
									}
									fld_sublist_effort_plan_mnth_end_next_tab.setLineItemValue(''+s_month_name.toLowerCase()+'_second',i_Counter+1,parseFloat(total_revenue));
									//nlapiLogExecution('Debug','+s_month_name.toLowerCase()+_second===='+''+s_month_name.toLowerCase()+'_second','total_revenue===='+total_revenue);
									
									//nlapiSetLineItemValue('people_plan_sublist1','ramp_list',i_row_json_index,total_revenue);
								}
							
							}
							
						}
						if(s_effrt_json_3_prev_mnth)
						{
							var s_entire_json_clubed = JSON.parse(s_effrt_json_3_prev_mnth);
							for (var i_mnth_end_plan = 0; i_mnth_end_plan < s_entire_json_clubed.length; i_mnth_end_plan++)
							{
								var a_row_json_data = JSON.parse(JSON.stringify(s_entire_json_clubed[i_mnth_end_plan]));
								i_Counter++;
								for (var i_row_json_index = 0; i_row_json_index < a_row_json_data.length; i_row_json_index++)
								{
									var i_ramp_list = a_row_json_data[i_row_json_index].rampList;
									var i_sub_practice = a_row_json_data[i_row_json_index].subprac;
									var s_sub_practice = a_row_json_data[i_row_json_index].subprac_text;
									var s_note = a_row_json_data[i_row_json_index].note;
									var total_revenue = a_row_json_data[i_row_json_index].allo;
									var s_mnth = a_row_json_data[i_row_json_index].mnth;
									var s_year = a_row_json_data[i_row_json_index].year;
									var s_month_name = s_mnth + '_' + s_year;
									if(i_row_json_index==0)
									{
										//nlapiLogExecution('Debug','i_ramp_list'+i_ramp_list,'i_sub_practice'+i_sub_practice);
										fld_sublist_effort_plan_mnth_end_next_tab.setLineItemValue('ramp_list',i_Counter+1,i_ramp_list);
										fld_sublist_effort_plan_mnth_end_next_tab.setLineItemValue('select_practices',i_Counter+1,i_sub_practice);
										fld_sublist_effort_plan_mnth_end_next_tab.setLineItemValue('text_note',i_Counter+1,s_note);
									}
									fld_sublist_effort_plan_mnth_end_next_tab.setLineItemValue(''+s_month_name.toLowerCase()+'_second',i_Counter+1,parseFloat(total_revenue));
									//nlapiLogExecution('Debug','+s_month_name.toLowerCase()+_second===='+''+s_month_name.toLowerCase()+'_second','total_revenue===='+total_revenue);
									
									//nlapiSetLineItemValue('people_plan_sublist1','ramp_list',i_row_json_index,total_revenue);
								}
							
							}
							
						}
						if(s_effrt_json_4_prev_mnth)
						{
							var s_entire_json_clubed = JSON.parse(s_effrt_json_4_prev_mnth);
							for (var i_mnth_end_plan = 0; i_mnth_end_plan < s_entire_json_clubed.length; i_mnth_end_plan++)
							{
								var a_row_json_data = JSON.parse(JSON.stringify(s_entire_json_clubed[i_mnth_end_plan]));
								i_Counter++;
								for (var i_row_json_index = 0; i_row_json_index < a_row_json_data.length; i_row_json_index++)
								{
									var i_ramp_list = a_row_json_data[i_row_json_index].rampList;
									var i_sub_practice = a_row_json_data[i_row_json_index].subprac;
									var s_sub_practice = a_row_json_data[i_row_json_index].subprac_text;
									var s_note = a_row_json_data[i_row_json_index].note;
									var total_revenue = a_row_json_data[i_row_json_index].allo;
									var s_mnth = a_row_json_data[i_row_json_index].mnth;
									var s_year = a_row_json_data[i_row_json_index].year;
									var s_month_name = s_mnth + '_' + s_year;
									if(i_row_json_index==0)
									{
										//nlapiLogExecution('Debug','i_ramp_list'+i_ramp_list,'i_sub_practice'+i_sub_practice);
										fld_sublist_effort_plan_mnth_end_next_tab.setLineItemValue('ramp_list',i_Counter+1,i_ramp_list);
										fld_sublist_effort_plan_mnth_end_next_tab.setLineItemValue('select_practices',i_Counter+1,i_sub_practice);
										fld_sublist_effort_plan_mnth_end_next_tab.setLineItemValue('text_note',i_Counter+1,s_note);
									}
									fld_sublist_effort_plan_mnth_end_next_tab.setLineItemValue(''+s_month_name.toLowerCase()+'_second',i_Counter+1,parseFloat(total_revenue));
									//nlapiLogExecution('Debug','+s_month_name.toLowerCase()+_second===='+''+s_month_name.toLowerCase()+'_second','total_revenue===='+total_revenue);
									
									//nlapiSetLineItemValue('people_plan_sublist1','ramp_list',i_row_json_index,total_revenue);
								}
							
							}
							
						}
						
					}
					
					//===================================== Search To Get ForeCast Master Data ================================================================
					try
					{
					nlapiLogExecution('Debug','s_project_id%%%%%%% ',s_project_id);
					nlapiLogExecution('Debug','i_projectId%%%%%%%',i_projectId);
					var customrecord_forecast_master_dataSearch = nlapiSearchRecord("customrecord_forecast_master_data",null,
						[
						 //["custrecord_forecast_project_id","is",s_project_id] 
						 ["custrecord_forecast_project_name.internalid","anyof",i_projectId]
						
						], 
						[
						   new nlobjSearchColumn("custrecord_forecast_parent_department",null,"GROUP"), 
						   new nlobjSearchColumn("custrecord_forecast_department",null,"GROUP"), 
						   new nlobjSearchColumn("custrecord_forecast_emp_level",null,"GROUP"), 
						   new nlobjSearchColumn("custrecord_forecast_emp_role",null,"GROUP"), 
						   new nlobjSearchColumn("custrecord_forecast_emp_location",null,"GROUP"), 
						   new nlobjSearchColumn("custrecord40",null,"GROUP"), 
						   new nlobjSearchColumn("custrecord_forecast_amount",null,"SUM"), 
						   new nlobjSearchColumn("custrecord_forecast_employee",null,"COUNT"), 
						   new nlobjSearchColumn("custrecord38",null,"GROUP"),
						   new nlobjSearchColumn("custrecord_forecast_month_startdate",null,"GROUP").setSort(false)
						]
						);
						if(customrecord_forecast_master_dataSearch)
						{
							var j = 2;
							var i_Prev_Month = '';
							var i_Current_Month1 = '';
							var i_Prev_Year = '';
							var i_Current_Year = '';
							//nlapiLogExecution('Debug','customrecord_forecast_master_dataSearch.length=',customrecord_forecast_master_dataSearch.length);
							//nlapiLogExecution('Debug','monthBreakUp.length=',monthBreakUp.length);
							var i_Second_Month = parseInt(customrecord_forecast_master_dataSearch.length)/parseInt(monthBreakUp.length);
							//nlapiLogExecution('Debug','i_Second_Month',i_Second_Month);
							var i_fte_total = 0;
							var i_total = 0;
							var f_amount = 0;
							var dataRow = [];
							var sr = 0;
							var a_TotalArray= new Array();
							var data_total_array=new Array();
							var i_temp_count = 0;
							var i_temp = 0;
							var maxValue = 0;
							var forecastStartDate ='';
							//Added by Sai
							var uniuq_array = new Array();
							//
							for(var i=0; i<customrecord_forecast_master_dataSearch.length;i++)
							{
								var s_practice = customrecord_forecast_master_dataSearch[i].getValue("custrecord_forecast_parent_department",null,"GROUP");
								var s_subPractice = customrecord_forecast_master_dataSearch[i].getValue("custrecord_forecast_department",null,"GROUP");
								var s_subPracticeText = customrecord_forecast_master_dataSearch[i].getText("custrecord_forecast_department",null,"GROUP");
								practices_involved.push(s_subPracticeText);
								var s_role = customrecord_forecast_master_dataSearch[i].getValue("custrecord_forecast_emp_role",null,"GROUP");
								var s_level = customrecord_forecast_master_dataSearch[i].getValue("custrecord_forecast_emp_level",null,"GROUP");
								var s_location = customrecord_forecast_master_dataSearch[i].getValue("custrecord_forecast_emp_location",null,"GROUP");
								var i_resourcecost = customrecord_forecast_master_dataSearch[i].getValue("custrecord40",null,"GROUP");
								var d_monthStartDate = customrecord_forecast_master_dataSearch[i].getValue("custrecord_forecast_month_startdate",null,"GROUP");
								if(i==0)
								{
									forecastStartDate = d_monthStartDate;
								}
								var i_fte = customrecord_forecast_master_dataSearch[i].getValue("custrecord_forecast_employee",null,"COUNT");
								var i_workingdays = customrecord_forecast_master_dataSearch[i].getValue("custrecord38",null,"GROUP");
								var i_total_amt =  customrecord_forecast_master_dataSearch[i].getValue("custrecord_forecast_amount",null,"SUM");
								//nlapiLogExecution('debug','i_total_amt=',i_total_amt);
								var d_todays_date = nlapiStringToDate(nlapiDateToString(new Date()));
								var d_monthStartDate = nlapiStringToDate(d_monthStartDate);
								var i_month_current = d_todays_date.getMonth();
								var i_monthRec = d_monthStartDate.getMonth();
								var i_yearRec = d_monthStartDate.getFullYear();
								//nlapiLogExecution('debug','i_monthRec=',i_monthRec);
								var s_month_name1 = getMonthName_FromMonthNumber(i_monthRec);
								
								s_month_name1 = s_month_name1 +'_'+ i_yearRec;
								
								//Total Calculation
								if( i == 0)
								{
									i_Prev_Month = i_monthRec;
									i_Current_Month1 = i_monthRec;
									i_Prev_Month_Name = s_month_name1;
								}
								else
								{
									//i_Prev_Month = i_Prev_Month;
									i_Current_Month1 = i_monthRec;
								}
								if(i_Prev_Month==i_Current_Month1)
								{
									i_fte_total=parseFloat(i_fte_total)+ parseFloat(i_fte);
									i_total = parseFloat(i_total) + parseFloat(i_total_amt);
									//maxValue = i;
									
								}
								if(i_Prev_Month!=i_Current_Month1)
								{
									i_temp = i - i_temp_count;
									
									if(i_temp > maxValue)
									maxValue = i_temp;
									i_temp_count = i;
									var s_Month_Name_New = getMonthName_FromMonthNumber(i_Prev_Month);
									var s_Month_Name_New1 = s_Month_Name_New;
									
									if(i_Prev_Month == '11')
									{
										i_yearRec = i_yearRec - 1;
										s_Month_Name_New = s_Month_Name_New +'_'+ i_yearRec;
										i_yearRec = i_yearRec + 1;
										
									}
									else
									{
									s_Month_Name_New = s_Month_Name_New +'_'+ i_yearRec;
									}
									
									//fld_sublist_effort_plan_mnth_end.setLineItemValue('role_month_end',j,'Total');
									data_total_array.push(i_fte_total+'##'+i_total);
									//fld_sublist_effort_plan_mnth_end.setLineItemValue(''+s_Month_Name_New.toLowerCase()+'_first',j,i_fte_total);
									//fld_sublist_effort_plan_mnth_end.setLineItemValue(''+s_Month_Name_New.toLowerCase()+'_third',j,(i_total).toFixed(2));
									fld_sublist_effort_plan_mnth_end_next_tab.setLineItemValue(''+s_Month_Name_New.toLowerCase()+'_second',1,(i_total).toFixed(2));	
									a_TotalArray.push(i_total+'_'+s_subPractice);
								//	j = 2;
									i_Prev_Month = i_Current_Month1;
									i_Prev_Year = i_Current_Year;
									i_fte_total = parseFloat(i_fte);
									i_total = parseFloat(i_total_amt);
								}
								var subp_role_level = s_subPractice+'_'+s_role+'_'+s_level+'_'+s_location+'_'+i_resourcecost;
								if(uniuq_array.indexOf(subp_role_level)< 0)
								{
									uniuq_array.push(subp_role_level);
									fld_sublist_effort_plan_mnth_end.setLineItemValue('parent_practice_month_end',j,s_practice);
									fld_sublist_effort_plan_mnth_end.setLineItemValue('sub_practice_month_end',j,s_subPractice);
									fld_sublist_effort_plan_mnth_end.setLineItemValue('role_month_end',j,s_role);
									fld_sublist_effort_plan_mnth_end.setLineItemValue('resource_cost_month_end',j,i_resourcecost);
									fld_sublist_effort_plan_mnth_end.setLineItemValue('location_month_end',j,s_location);
									fld_sublist_effort_plan_mnth_end.setLineItemValue('level_month_end',j,s_level);
										
									fld_sublist_effort_plan_mnth_end.setLineItemValue(''+s_month_name1.toLowerCase()+'_first',j,i_fte);
									fld_sublist_effort_plan_mnth_end.setLineItemValue(''+s_month_name1.toLowerCase()+'_second',j,i_workingdays);
									var tempTotal = i_total_amt.split('.');
									tempTotal = format2(i_total_amt) + '.' + parseFloat(tempTotal[1]);
									fld_sublist_effort_plan_mnth_end.setLineItemValue(''+s_month_name1.toLowerCase()+'_third',j,tempTotal);
									j++;
								}
								else
									{
									j = uniuq_array.indexOf(subp_role_level);
									j=j+2;
									fld_sublist_effort_plan_mnth_end.setLineItemValue('parent_practice_month_end',j,s_practice);
									fld_sublist_effort_plan_mnth_end.setLineItemValue('sub_practice_month_end',j,s_subPractice);
									fld_sublist_effort_plan_mnth_end.setLineItemValue('role_month_end',j,s_role);
									fld_sublist_effort_plan_mnth_end.setLineItemValue('resource_cost_month_end',j,i_resourcecost);
									fld_sublist_effort_plan_mnth_end.setLineItemValue('location_month_end',j,s_location);
									fld_sublist_effort_plan_mnth_end.setLineItemValue('level_month_end',j,s_level);
										
									fld_sublist_effort_plan_mnth_end.setLineItemValue(''+s_month_name1.toLowerCase()+'_first',j,i_fte);
									fld_sublist_effort_plan_mnth_end.setLineItemValue(''+s_month_name1.toLowerCase()+'_second',j,i_workingdays);
									var tempTotal = i_total_amt.split('.');
									tempTotal = format2(i_total_amt) + '.' + parseFloat(tempTotal[1]);
									fld_sublist_effort_plan_mnth_end.setLineItemValue(''+s_month_name1.toLowerCase()+'_third',j,tempTotal);
									j++;
									}
								
								//nlapiLogExecution('debug','i=',i);
								if(i == (customrecord_forecast_master_dataSearch.length - 1))
								{
									var s_Month_Name_New = getMonthName_FromMonthNumber(i_Prev_Month);
									s_Month_Name_New = s_Month_Name_New +'_'+ i_yearRec;
									data_total_array.push(i_fte_total+'##'+i_total);
									fld_sublist_effort_plan_mnth_end_next_tab.setLineItemValue(''+s_Month_Name_New.toLowerCase()+'_second',1,(i_total).toFixed(2));	
									
									a_TotalArray.push(i_total+'_'+s_subPractice);
								}
							}
							//nlapiLogExecution('Debug','i_temp'+i_temp,'data_total_array'+data_total_array);
							var lineNum = nlapiGetLineItemCount('people_plan_sublist');
							//nlapiLogExecution('Debug','forecastStartDate',forecastStartDate);
							fld_amt_total.setDefaultValue(a_TotalArray);
							//============================================================
							var monthBreakUpForecast = getMonthsBreakup(
							nlapiStringToDate(forecastStartDate),
							nlapiStringToDate(d_proj_end_date));
							//nlapiLogExecution('Debug','i= '+i,'monthBreakUpForecast '+monthBreakUpForecast.length);
							if(monthBreakUpForecast.length==1)
							{
								maxValue = i-1;
							}
							nlapiLogExecution('Debug','maxValue ',maxValue);
							for (var j = 0; j < monthBreakUpForecast.length; j++)
							{
							var months = monthBreakUpForecast[j];
							var s_month_name = getMonthName(months.Start); // get month name
							var month_strt = nlapiStringToDate(months.Start);
							var month_name_yr = month_strt.getMonth();
							var month_end = nlapiStringToDate(months.End);
							var i_month = month_strt.getMonth();
							var i_year = month_strt.getFullYear();
							s_month_name = s_month_name +'_'+ i_year;
							var d_today_date = new Date();
							var today_month_yr = d_today_date.getMonth();
							var prev_year = d_today_date.getFullYear();
							//if(j==1)
							{
								for(var xx=0; xx<data_total_array.length;xx++)
								{
									var arrayElement = data_total_array[j];
									try
									{
										var arraTotal = arrayElement.split('##');
										var i_ftsTotal = arraTotal[0];
										var i_grandTotal = arraTotal[1];
										//nlapiLogExecution('Debug','i_ftsTotal'+i_ftsTotal,'maxValue=='+parseInt(maxValue));
										fld_sublist_effort_plan_mnth_end.setLineItemValue('role_month_end',parseInt(maxValue)+1,'Total');
										fld_sublist_effort_plan_mnth_end.setLineItemValue(''+s_month_name.toLowerCase()+'_first',parseInt(maxValue)+1,i_ftsTotal);
										//fld_sublist_effort_plan_mnth_end.setLineItemValue(''+s_month_name.toLowerCase()+'_second',1,value);
										//nlapiLogExecution('Debug','i_ftsTotal After'+i_ftsTotal,'maxValue=='+parseInt(maxValue));
										i_grandTotal = parseFloat(i_grandTotal).toFixed(2);
										var grandTotal = i_grandTotal.split('.');
										if(grandTotal[1])
										{
										grandTotal = format2(grandTotal) + '.'+parseFloat(grandTotal[1]);
										}
										else
										{
										grandTotal = format2(grandTotal)+'.'+'00'; 
										}
										fld_sublist_effort_plan_mnth_end.setLineItemValue(''+s_month_name.toLowerCase()+'_third',parseInt(maxValue)+1,grandTotal);
									}
									catch(err)
									{
										//nlapiLogExecution('Debug','error',err);
										fld_sublist_effort_plan_mnth_end.setLineItemValue(''+s_month_name.toLowerCase()+'_first',(maxValue+2),'');
										fld_sublist_effort_plan_mnth_end.setLineItemValue(''+s_month_name.toLowerCase()+'_third',(maxValue+2),'');
									}
									
									
								}
								
							}
							
							}
							//===========================================================================================================							
						}
					}
					catch(err)
					{
						nlapiLogExecution('Debug','Error=',err);
					}
						// Sub Practice List	
						var unique_list=remove_dulpi(practices_involved);
						fld_practices_involved.setDefaultValue(unique_list);
						practices_selected.addSelectOption('','');
						for(var n=0;n<unique_list.length;n++)
						{
							practices_selected.addSelectOption(n,unique_list[n]);
						}
						//===============================================================================
							nlapiLogExecution('Debug','s_project_id=',i_projectId);
							var customrecord_forecast_master_dataSearch1 = nlapiSearchRecord("customrecord_forecast_master_data",null,
							[
								["custrecord_forecast_project_name","anyof",i_projectId]
							], 
							[
								new nlobjSearchColumn("custrecord_forecast_department",null,"GROUP").setSort(false), 
								new nlobjSearchColumn("custrecord_forecast_month_startdate",null,"GROUP").setSort(false), 
								new nlobjSearchColumn("custrecord_forecast_amount",null,"SUM")
							]
							);	
							
							
							var a_sumOfPractice = new Array();
							//nlapiLogExecution('Debug','customrecord_forecast_master_dataSearch1=',customrecord_forecast_master_dataSearch1.length);
							
							if(customrecord_forecast_master_dataSearch1)
							{
								for(var month = 0;month <customrecord_forecast_master_dataSearch1.length;month++)
								{
									var i_monthwise_total = customrecord_forecast_master_dataSearch1[month].getValue("custrecord_forecast_amount",null,"SUM");
									a_sumOfPractice.push(i_monthwise_total);
								}
									
								
							}
							nlapiLogExecution('Debug','a_sumOfPractice',a_sumOfPractice);
							fld_subPractice_Total.setDefaultValue(a_sumOfPractice);
						//===============================================================================
						// Submit Button
						o_form_obj.addSubmitButton('Submit');
						o_form_obj.setScript('customscript_cli_tndm_validation');// Client script for validation	
					
				}
				
			   response.writePage(o_form_obj);
	}
	else
	{
			
		var o_form_obj_Final = nlapiCreateForm("T&M Project Details Page");
				
				// create region field
				var i_region = request.getParameter('proj_region');
				var i_practices_involved = request.getParameter('amt_practices_involved');
				//nlapiLogExecution('Debug','i_practices_involved',i_practices_involved.length);
				//unique Practice 
				var a_pract= new Array();
				a_pract = i_practices_involved.split('');
				
				var unique_list=remove_dulpi(a_pract);
				nlapiLogExecution('Debug','unique_list',unique_list);
				var fld_region_field = o_form_obj_Final.addField('proj_region', 'select','Region','customrecord_region').setDisplayType('inline');
				fld_region_field.setDefaultValue(i_region);
				
				// create customer field
				var i_customer = request.getParameter('customer_selected');
				var fld_customer_field = o_form_obj_Final.addField('customer_selected', 'select','Customer','customer').setDisplayType('inline');
				fld_customer_field.setDefaultValue(i_customer);
				
				// create project field
				var i_project = request.getParameter('proj_selected');
				var fld_proj_field = o_form_obj_Final.addField('proj_selected', 'select','Project','job').setDisplayType('inline');
				fld_proj_field.setDefaultValue(i_project);
				
				// create project sow value field
				var i_project_sowValue = request.getParameter('proj_sow_value');
				var fld_proj_sow_value = o_form_obj_Final.addField('proj_sow_value', 'text','Project SOW value').setDisplayType('inline');
				fld_proj_sow_value.setDefaultValue(i_project_sowValue);	
				
				// create field for project start date
				var i_projectStartDate = request.getParameter('proj_strt_date');
				var fld_proj_start_date = o_form_obj_Final.addField('proj_strt_date', 'date','Project Start Date').setDisplayType('inline');
				fld_proj_start_date.setDefaultValue(i_projectStartDate);
				
				// create field for projet end date
				var i_projectEndDate = request.getParameter('proj_end_date');
				var fld_proj_end_date = o_form_obj_Final.addField('proj_end_date', 'date','Project End Date').setDisplayType('inline');
				fld_proj_end_date.setDefaultValue(i_projectEndDate);
				
				// create field for project currency
				var i_projectCurrency = request.getParameter('proj_currency');
				var fld_proj_currency_date = o_form_obj_Final.addField('proj_currency', 'text','Project Currency').setDisplayType('inline');
				fld_proj_currency_date.setDefaultValue(i_projectCurrency);
				
				// create field for project practice
				var i_projectPractice = request.getParameter('proj_practice');
				var fld_proj_practice = o_form_obj_Final.addField('proj_practice', 'select','Project Executing Practice','department').setDisplayType('inline');
				fld_proj_practice.setDefaultValue(i_projectPractice);
				
				// create field for poject manager practice field
				var i_projectManagerPractice = request.getParameter('proj_manager_practice');
				var fld_proj_manager_practice = o_form_obj_Final.addField('proj_manager_practice', 'select','Project Manager Practice','department').setDisplayType('inline');
				fld_proj_manager_practice.setDefaultValue(i_projectManagerPractice);
				
				// create field for request type i.e create or update
				//var i_existing_revenue_share_status = request.getParameter('share_type');
				//var fld_existing_revenue_share_status = o_form_obj_Final.addField('share_type', 'text','Request Type').setDisplayType('inline');
				//fld_existing_revenue_share_status.setDefaultValue(i_existing_revenue_share_status);
				
				//field to find is value changed or not
				var fld_existing_revenue_share_changed = o_form_obj_Final.addField('change_triggered', 'checkbox','').setDisplayType('inline');
				//fld_existing_revenue_share_changed.setDefaultValue('F');
				//Fld to capture amount change
				//var i_amt_captured = request.getParameter('amt_captured');
				//var fld_amt_captured = o_form_obj_Final.addField('amt_captured', 'currency','Amount Captured').setDisplayType('inline');
				//fld_amt_captured.setDefaultValue(i_amt_captured);
				//var fld_sublist_effort_plan_mnth_end_next_tab = o_form_obj_Final.addSubList('people_plan_sublist1', 'inlineeditor', 'Effort Plan Month End', 'revenue_share_tab');
				var fld_custom_record = o_form_obj_Final.addField('cust_rec_id', 'text','Rec Id').setDisplayType('hidden');
				
				var a_totalAmount = request.getParameter('amt_total');
				//nlapiLogExecution('Debug','a_totalAmount',a_totalAmount);
				
				// create custom_record field
				var fld_custom_record_id = request.getParameter('custome_record');
				//nlapiLogExecution('Debug','fld_custom_record_id',fld_custom_record_id);
				var customrecord_tm_month_total_jsonSearch = nlapiSearchRecord("customrecord_tm_month_total_json",null,
					[
					   ["custrecord_project_json","anyof",i_project]
					], 
					[
					   new nlobjSearchColumn("custrecord_project_json"), 
					   new nlobjSearchColumn("custrecord_json1"), 
					   new nlobjSearchColumn("custrecord_json2"), 
					   new nlobjSearchColumn("custrecord_json3"), 
					   new nlobjSearchColumn("custrecord_json4"), 
					   new nlobjSearchColumn("custrecord_rampupjson1"), 
					   new nlobjSearchColumn("custrecord_rampupjson2"), 
					   new nlobjSearchColumn("custrecord_rampupjson3"), 
					   new nlobjSearchColumn("custrecord_rampupjson4"),
					   new nlobjSearchColumn("created").setSort(true)
					]
					);
			if(_logValidation(customrecord_tm_month_total_jsonSearch))
			{
				var context = nlapiGetContext();
				var	recId = customrecord_tm_month_total_jsonSearch[0].getId();
				fld_custom_record.setDefaultValue(recId);
				//nlapiLogExecution('debug','prev mnth effrt json id:- '+customrecord_tm_month_total_jsonSearch[0].getId());
				var projectWiseRevenue_previous_effrt = [];
				var a_prev_subprac_searched_once = new Array();
				var o_mnth_end_effrt_prev_mnth = nlapiLoadRecord('customrecord_tm_month_total_json', customrecord_tm_month_total_jsonSearch[0].getId());
				var s_effrt_json_1_prev_mnth = o_mnth_end_effrt_prev_mnth.getFieldValue('custrecord_rampupjson1');
				var s_effrt_json_2_prev_mnth = o_mnth_end_effrt_prev_mnth.getFieldValue('custrecord_rampupjson2');
				var s_effrt_json_3_prev_mnth = o_mnth_end_effrt_prev_mnth.getFieldValue('custrecord_rampupjson3');
				var s_effrt_json_4_prev_mnth = o_mnth_end_effrt_prev_mnth.getFieldValue('custrecord_rampupjson4');
				
				//var o_custRecord = nlapiLoadRecord('customrecord_tm_month_total_json',fld_custom_record_id);
				// Search to get total of sub-practices
				var customrecord_forecast_master_dataSearch = nlapiSearchRecord("customrecord_forecast_master_data",null,
				[
					["custrecord_forecast_project_name","anyof",i_project]
				], 
				[
					new nlobjSearchColumn("custrecord_forecast_department",null,"GROUP").setSort(false), 
					new nlobjSearchColumn("custrecord_forecast_month_startdate",null,"GROUP").setSort(false), 
					new nlobjSearchColumn("custrecord_forecast_amount",null,"SUM")
				]
				);
							
				var temp =0;
				var html1= "";
				var css_file_url = "";
					if (context.getEnvironment() == "SANDBOX") {
					css_file_url = "https://system.netsuite.com/core/media/media.nl?id=199133&c=3883006_SB1&h=c98b3547d8dad429c341&_xt=.css&whence=";
					} else if (context.getEnvironment() == "PRODUCTION") {
					css_file_url = "https://system.na1.netsuite.com/core/media/media.nl?id=199133&c=3883006&h=64fa194295cd4ab64d58&mv=iq3kjr6g&_xt=.css&whence=";
					}
				var total = 0;
				var totalArray =new Array();
				if(s_effrt_json_1_prev_mnth)
				{
				
				var a_practiceTotal = new Array();
				
				for(i_practice = 0;i_practice<a_pract.length;i_practice++)
				{
					var i_practice_previous = 0;
					var f_total_revenue_for_tenure = 0;
					var s_entire_json_clubed = JSON.parse(s_effrt_json_1_prev_mnth);
					var str = "";
					str += "<link href='" + css_file_url + "'  rel='stylesheet'>";
					str += "<table bgcolor =#e6e6ff width=100%><tr><td>";
					str += '<font size="2" color="#006699"><b>'+a_pract[i_practice]+'</b></font>';
					str += "</td></tr></table>"
					str += "<table class='projection-table' >";
					str += "<tr class='header-row'>";
					str += "<td>";
					str += "Ramp List";
					str += "</td>";
					str += "<td>";
					str += "Sub Practice";
					str += "</td>";
					str += "<td>";
					str += "Note";
					str += "</td>";
					for(var month = 0;month <s_entire_json_clubed.length;month++)
					{
						var a_row = JSON.parse(JSON.stringify(s_entire_json_clubed[month]));
						for(var i=0; i<a_row.length;i++)
						{
						var s_mnth = a_row[i].mnth;
							var s_year = a_row[i].year;
							var s_month_name = s_mnth + '_' + s_year;
							if(month==0)
							{
								str += "<td>";
								str += s_month_name;
								str += "</td>";
							}
						}
					}
					str += "<tr>";
					str += "<td>";
					str += "";
					str += "</td>";
					str += "<td>";
					str += "";
					str += "</td>";
					str += "<td>";
					str += "Total As per Actual Allocation";
					str += "</td>";
					//To get Practice wise total 
					if(customrecord_forecast_master_dataSearch)
					{
						//nlapiLogExecution('Debug','unique_list=',unique_list);
						for(var month = 0;month <s_entire_json_clubed.length;month++)
						{
							var a_row = JSON.parse(JSON.stringify(s_entire_json_clubed[month]));
							
							for(var i=0; i<a_row.length;i++)
							{
								var actualTotal = a_row[i].allo;
								if(month==0)
								{
									if(actualTotal>0)
									{
										try
										{
											var i_monthwise_total = customrecord_forecast_master_dataSearch[temp].getValue("custrecord_forecast_amount",null,"SUM");
											//nlapiLogExecution('Debug','i_monthwise_total=====',i_monthwise_total);
											//var i_department = customrecord_forecast_master_dataSearch[temp].getValue("custrecord_forecast_department",null,"group");
										}
										catch(error)
										{
											i_monthwise_total = 0;
											nlapiLogExecution('Debug','LengthError',error);
										}
										
										str += "<td class='monthly-amount'><b>";
										str += i_monthwise_total;
										str += "</b></td>";
										a_practiceTotal.push(i_monthwise_total);
									
										temp++;
										total = parseInt(total) + parseInt(i_monthwise_total); 
										totalArray.push(i_monthwise_total);
									}			
								}
								
							}
							
						}
						
					}
					//nlapiLogExecution('Debug','s_entire_json_clubed.length',s_entire_json_clubed.length);
					
					for (var i_mnth_end_plan = 1; i_mnth_end_plan < s_entire_json_clubed.length; i_mnth_end_plan++)
					{
						var a_row_json_data = JSON.parse(JSON.stringify(s_entire_json_clubed[i_mnth_end_plan]));
						//nlapiLogExecution('Debug','a_row_json_data.length',a_row_json_data.length);
						str += "<tr>"
						for (var i_row_json_index = 0; i_row_json_index < a_row_json_data.length; i_row_json_index++)
						{
							var i_ramp_list = a_row_json_data[i_row_json_index].rampList;
							var i_sub_practice = a_row_json_data[i_row_json_index].subprac;
							var s_sub_practice = a_row_json_data[i_row_json_index].subprac_text;
							var s_note = a_row_json_data[i_row_json_index].note;
							var total_revenue = a_row_json_data[i_row_json_index].allo;
							var s_mnth = a_row_json_data[i_row_json_index].mnth;
							var s_year = a_row_json_data[i_row_json_index].year;
							var s_month_name = s_mnth + '_' + s_year;
							total_revenue = parseFloat(total_revenue).toFixed(2);
							
							if(i_ramp_list == 1)
							{
								i_ramp_list = 'Ramp Up';	
							}
							else if(i_ramp_list == 2)
							{
								i_ramp_list = 'Leave';	
							}
							else if(i_ramp_list == 3)
							{
								i_ramp_list = 'Risk';	
							}
							else
							{ 
								i_ramp_list='';	
							}
							//nlapiLogExecution('Debug','total',total);
							if(i_sub_practice == i_practice)
							{
								if(i_row_json_index==0)
								{
								str += "<td class='label-name'>";
								str += i_ramp_list;
								str += "</td>";	
								str += "<td class='label-name'>";
								str += s_sub_practice;
								str += "</td>";
								str += "<td class='label-name'>";
								str += s_note;
								str += "</td>";
								}
								str += "<td class='monthly-amount'>";
								str += total_revenue;
								str += "</td>";
								var i_prev_total = totalArray[i_row_json_index];
								
								i_prev_total = parseFloat(i_prev_total) + parseFloat(total_revenue);
								totalArray[i_row_json_index] = i_prev_total;
								
							}
						}
					}
						
					if(s_effrt_json_2_prev_mnth)
					{
						var s_entire_json_clubed = JSON.parse(s_effrt_json_2_prev_mnth);
						for (var i_mnth_end_plan = 0; i_mnth_end_plan < s_entire_json_clubed.length; i_mnth_end_plan++)
						{
						var a_row_json_data = JSON.parse(JSON.stringify(s_entire_json_clubed[i_mnth_end_plan]));
						//nlapiLogExecution('Debug','a_row_json_data.length',a_row_json_data.length);
						str += "<tr>"
						for (var i_row_json_index = 0; i_row_json_index < a_row_json_data.length; i_row_json_index++)
						{
							var i_ramp_list = a_row_json_data[i_row_json_index].rampList;
							var i_sub_practice = a_row_json_data[i_row_json_index].subprac;
							var s_sub_practice = a_row_json_data[i_row_json_index].subprac_text;
							var s_note = a_row_json_data[i_row_json_index].note;
							var total_revenue = a_row_json_data[i_row_json_index].allo;
							var s_mnth = a_row_json_data[i_row_json_index].mnth;
							var s_year = a_row_json_data[i_row_json_index].year;
							var s_month_name = s_mnth + '_' + s_year;
							
							total_revenue = parseFloat(total_revenue).toFixed(2);
							if(i_ramp_list == 1)
							{
								i_ramp_list = 'Ramp Up';	
							}
							else if(i_ramp_list == 2)
							{
								i_ramp_list = 'Leave';	
							}
							else if(i_ramp_list == 3)
							{
								i_ramp_list = 'Risk';	
							}
							else
							{ 
								i_ramp_list='';	
							}
							//total = parseInt(total) + parseInt(total_revenue); 
							//nlapiLogExecution('Debug','total',total);
							if(i_sub_practice == i_practice)
							{
								if(i_row_json_index==0)
								{
								str += "<td class='label-name'>";
								str += i_ramp_list;
								str += "</td>";	
								str += "<td class='label-name'>";
								str += s_sub_practice;
								str += "</td>";
								str += "<td class='label-name'>";
								str += s_note;
								str += "</td>";
								}							
								str += "<td class='monthly-amount'>";
								str += total_revenue;
								str += "</td>";
								
								total = parseFloat(total) + parseFloat(total_revenue); 
								var i_prev_total = totalArray[i_row_json_index];
								
								i_prev_total = parseFloat(i_prev_total) + parseFloat(total_revenue);
								totalArray[i_row_json_index] = i_prev_total;
								
							}
						}
					
					}
						str += "</tr>"
							
					}
					if(s_effrt_json_3_prev_mnth)
					{
						var s_entire_json_clubed = JSON.parse(s_effrt_json_3_prev_mnth);
						for (var i_mnth_end_plan = 0; i_mnth_end_plan < s_entire_json_clubed.length; i_mnth_end_plan++)
						{
						var a_row_json_data = JSON.parse(JSON.stringify(s_entire_json_clubed[i_mnth_end_plan]));
						//nlapiLogExecution('Debug','a_row_json_data.length',a_row_json_data.length);
						str += "<tr>"
						for (var i_row_json_index = 0; i_row_json_index < a_row_json_data.length; i_row_json_index++)
						{
							var i_ramp_list = a_row_json_data[i_row_json_index].rampList;
							var i_sub_practice = a_row_json_data[i_row_json_index].subprac;
							var s_sub_practice = a_row_json_data[i_row_json_index].subprac_text;
							var s_note = a_row_json_data[i_row_json_index].note;
							var total_revenue = a_row_json_data[i_row_json_index].allo;
							var s_mnth = a_row_json_data[i_row_json_index].mnth;
							var s_year = a_row_json_data[i_row_json_index].year;
							var s_month_name = s_mnth + '_' + s_year;
							
							total_revenue = parseFloat(total_revenue).toFixed(2);
							if(i_ramp_list == 1)
							{
								i_ramp_list = 'Ramp Up';	
							}
							else if(i_ramp_list == 2)
							{
								i_ramp_list = 'Leave';	
							}
							else if(i_ramp_list == 3)
							{
								i_ramp_list = 'Risk';	
							}
							else
							{ 
								i_ramp_list='';	
							}
							//total = parseInt(total) + parseInt(total_revenue); 
							//nlapiLogExecution('Debug','total',total);
							if(i_sub_practice == i_practice)
							{
								if(i_row_json_index==0)
								{
								str += "<td class='label-name'>";
								str += i_ramp_list;
								str += "</td>";	
								str += "<td class='label-name'>";
								str += s_sub_practice;
								str += "</td>";
								str += "<td class='label-name'>";
								str += s_note;
								str += "</td>";
								}							
								str += "<td class='monthly-amount'>";
								str += total_revenue;
								str += "</td>";
							
								total = parseFloat(total) + parseFloat(total_revenue); 
								var i_prev_total = totalArray[i_row_json_index];
								
								i_prev_total = parseFloat(i_prev_total) + parseFloat(total_revenue);
								totalArray[i_row_json_index] = i_prev_total;
								
							}
						}
					
					}
						str += "</tr>"
						//nlapiLogExecution('Debug','totalArray',totalArray[i_mnth_end_plan]);	
					}
					if(s_effrt_json_4_prev_mnth)
					{
						var s_entire_json_clubed = JSON.parse(s_effrt_json_4_prev_mnth);
						for (var i_mnth_end_plan = 0; i_mnth_end_plan < s_entire_json_clubed.length; i_mnth_end_plan++)
						{
						var a_row_json_data = JSON.parse(JSON.stringify(s_entire_json_clubed[i_mnth_end_plan]));
						//nlapiLogExecution('Debug','a_row_json_data.length',a_row_json_data.length);
						str += "<tr>"
						for (var i_row_json_index = 0; i_row_json_index < a_row_json_data.length; i_row_json_index++)
						{
							var i_ramp_list = a_row_json_data[i_row_json_index].rampList;
							var i_sub_practice = a_row_json_data[i_row_json_index].subprac;
							var s_sub_practice = a_row_json_data[i_row_json_index].subprac_text;
							var s_note = a_row_json_data[i_row_json_index].note;
							var total_revenue = a_row_json_data[i_row_json_index].allo;
							var s_mnth = a_row_json_data[i_row_json_index].mnth;
							var s_year = a_row_json_data[i_row_json_index].year;
							var s_month_name = s_mnth + '_' + s_year;
							
							total_revenue = parseFloat(total_revenue).toFixed(2);
							if(i_ramp_list == 1)
							{
								i_ramp_list = 'Ramp Up';	
							}
							else if(i_ramp_list == 2)
							{
								i_ramp_list = 'Leave';	
							}
							else if(i_ramp_list == 3)
							{
								i_ramp_list = 'Risk';	
							}
							else
							{ 
								i_ramp_list='';	
							}
							
							//total = parseInt(total) + parseInt(total_revenue); 
							//nlapiLogExecution('Debug','total',total);
							if(i_sub_practice == i_practice)
							{
								if(i_row_json_index==0)
								{
								str += "<td class='label-name'>";
								str += i_ramp_list;
								str += "</td>";	
								str += "<td class='label-name'>";
								str += s_sub_practice;
								str += "</td>";
								str += "<td class='label-name'>";
								str += s_note;
								str += "</td>";
								}							
								str += "<td class='monthly-amount'>";
								str += total_revenue;
								str += "</td>";
								
								total = parseFloat(total) + parseFloat(total_revenue); 
								var i_prev_total = totalArray[i_row_json_index];
								
								i_prev_total = parseFloat(i_prev_total) + parseFloat(total_revenue);
								totalArray[i_row_json_index] = i_prev_total;
								
							}
						}
					
					}
					str += "</tr>"
						
					}
					//nlapiLogExecution('Debug','totalArray24234',totalArray);
					str += "</tr>";
					str +="<tr>";
					str += "<td>";
					str += "";
					str += "</td>";
					str += "<td>";
					str += "";
					str += "</td>";
					str += "<td><b><font color=blue>";
					str += "Grand Total";
					str += "</font></b></td>";
					for(var month = 0;month <s_entire_json_clubed.length;month++)
					{
						var a_row = JSON.parse(JSON.stringify(s_entire_json_clubed[month]));
						//nlapiLogExecution('Debug','a_row.length',a_row.length);
						for(var i=0; i<a_row.length;i++)
						{
						if(month==0)
							{
								
							str += "<td class='projected-amount'><b><font color =blue>";
							if(!totalArray[i])
							{
								totalArray[i] = 0;
							}
							str += parseFloat(totalArray[i]).toFixed(2);
							str += "</font></b></td>";
							}
						}
						totalArray  = [];
					}
					str +="</tr>";
					str +="</table>";
					str +="<br>";
					html1 = html1+ str;
				}
					
				}	
			}
				
			o_form_obj_Final.addField('cost_view_qw', 'inlinehtml', null, null, 'custpage_cost_view_qw').setDefaultValue(html1);
			o_form_obj_Final.setScript('customscript_cli_tndm_validation');// Client script for validation		
			o_form_obj_Final.addButton('confirm_button', 'Confirm', 'custom_plan()');			
			response.writePage(o_form_obj_Final);		
	}
	//}
	}catch(err)
	{
		nlapiLogExecution('ERROR','ERROR MESSAGE :- ',err);
	}	
	
}

function find_prac_exist(a_current_pract,i_prac_to_find,i_level_to_find)
{
	var i_prac_present = 0;
	for(var i_prac_find=0; i_prac_find<a_current_pract.length; i_prac_find++)
	{
		if(a_current_pract[i_prac_find].i_sub_practice == i_prac_to_find && a_current_pract[i_prac_find].i_level == i_level_to_find)
		{
			i_prac_present = 1;
		}
	}
	
	return i_prac_present;
}
function find_uniq_prac_exist(a_current_pract_un,i_prac_to_find)
{
	var i_prac_present_un = 0;
	for(var i_prac_find=0; i_prac_find<a_current_pract_un.length; i_prac_find++)
	{
		if(a_current_pract_un[i_prac_find].i_sub_practice == i_prac_to_find)
		{
			i_prac_present_un = 1;
		}
	}
	
	return i_prac_present_un;
}
function getDatediffIndays(startDate, endDate)
{
    var one_day=1000*60*60*24;
    var fromDate = startDate;
    var toDate=endDate

	//var date1 = nlapiStringToDate(fromDate);

   //var date2 = nlapiStringToDate(toDate);
	
	var date1 = fromDate;
	var date2 = toDate;
	
    var date3=Math.round((date2-date1)/one_day);

    return (date3+1);
}

function generate_table_effort_view(monthBreakUp,customrecord_forecast_master_dataSearch,i_project)//(projectWiseRevenue,monthBreakUp,i_year_project,i_project_mnth)
{
	var css_file_url = "";
	nlapiLogExecution('Debug','i_project',i_project);
	var context = nlapiGetContext();
	if (context.getEnvironment() == "SANDBOX") {
		css_file_url = "https://system.netsuite.com/core/media/media.nl?id=199133&c=3883006_SB1&h=c98b3547d8dad429c341&_xt=.css&whence=";
	} else if (context.getEnvironment() == "PRODUCTION") {
		css_file_url = "https://system.na1.netsuite.com/core/media/media.nl?id=199133&c=3883006&h=64fa194295cd4ab64d58&mv=iq3kjr6g&_xt=.css&whence=";
	}
   
	var html = "";
	html += "<link href='" + css_file_url + "'  rel='stylesheet'>";
	html += '<p><font size="2" color="red">Total</font></p>';
	html += "<table class='projection-table'>";

	// header
	html += "<tr class='header-row'>";
	html += "<td>";
	html += " ";
	html += "</td>";
	var today = new Date();
	var currentMonthName = getMonthName(nlapiDateToString(today));
	nlapiLogExecution('Debug','monthBreakUp',monthBreakUp);
	
	///Heading for loop
	for (var j = 0; j < monthBreakUp.length; j++)
	{
		var months = monthBreakUp[j];
		var s_month_name = getMonthName(months.Start); // get month name
		nlapiLogExecution('Debug','s_month_name',s_month_name);
		var s_year = nlapiStringToDate(months.Start)
		s_year = s_year.getFullYear();
		s_month_name = s_month_name+'_'+s_year;
				
		html += "<td>";
		html += s_month_name;
		html += "</td>";
	}
	html += "</tr>";
	
	var customrecord_forecast_master_dataSearch = nlapiSearchRecord("customrecord_forecast_master_data",null,
		[
			["custrecord_forecast_project_name","anyof",i_project]
		], 
		[
			new nlobjSearchColumn("custrecord_forecast_department",null,"GROUP"), 
			new nlobjSearchColumn("custrecord_forecast_month_startdate",null,"GROUP"), 
			new nlobjSearchColumn("custrecord_forecast_amount",null,"SUM")
		]
		);	
	var s_Prev_Practice = "";
	var s_Current_Practice = "";
	var s_Amounts = "";
	if(_logValidation(customrecord_forecast_master_dataSearch))
	{	
		for (var j = 0; j < customrecord_forecast_master_dataSearch.length; j++)
		{
			var flag = false;
			var i_subDept = customrecord_forecast_master_dataSearch[j].getText("custrecord_forecast_department",null,"GROUP");
			var i_totalAmt  = customrecord_forecast_master_dataSearch[j].getValue("custrecord_forecast_amount",null,"SUM");
			s_Current_Practice = i_subDept;
			if(j == 0)
			{
				s_Prev_Practice = s_Current_Practice;
			}
			if(s_Prev_Practice != s_Current_Practice || j == (customrecord_forecast_master_dataSearch.length - 1))
			{
			//	nlapiLogExecution('Debug','s_Amounts',s_Amounts);
				if(j == (customrecord_forecast_master_dataSearch.length - 1))
				{
					if(s_Amounts)
					{
						s_Amounts = s_Amounts + "@@"+i_totalAmt;
					}
					else
					{
						s_Amounts = i_totalAmt 
					}
				}
				flag = true;
				html += "<tr>";
				html += "<td>";
				html += s_Prev_Practice;
				html += "</td>";
				
				var arr_Amount = new Array();
				arr_Amount = s_Amounts.split("@@");
				for(o = 0 ; o < arr_Amount.length ; o++)
				{
					html += "<td>";
					html += arr_Amount[o];
					html += "</td>";
				}
				html += "</tr>";
				
				s_Amounts = "";
				//s_Amounts = i_totalAmt;
			
			}
			//else
			{
				if(s_Amounts)
				{
					s_Amounts = s_Amounts + "@@"+i_totalAmt;
				}
				else
				{
					s_Amounts = i_totalAmt 
				}
			}
			s_Prev_Practice = s_Current_Practice;
		}
	}
	html += "</tr>";
	html += "</table>";
	return html;
}

function getMonthsBreakup(d_startDate, d_endDate) {
	
	try
	{
		var dateBreakUp = [];
		var new_start_date = new Date(d_startDate.getFullYear(), d_startDate
		        .getMonth(), 1);
				
		var endDate = nlapiAddDays(nlapiAddMonths(d_startDate, 1), -1);
		endDate = nlapiDateToString(endDate, 'date');
	
		dateBreakUp.push({
		    Start : nlapiDateToString(new_start_date, 'date'),
		    End : endDate
		});
	
		for (var i = 1;; i++) {
			var new_date = nlapiAddMonths(new_start_date, i);
	
			if (new_date > d_endDate) {
				break;
			}
			
			endDate = nlapiAddDays(nlapiAddMonths(new_date, 1), -1);
			endDate = nlapiDateToString(endDate, 'date');
					
			dateBreakUp.push({
			    Start : nlapiDateToString(new_date, 'date'),
			    End : endDate
			});
		}
	}
	catch(err)
	{
		nlapiLogExecution('ERROR','getMonthsBreakup','ERROR MESSAGE :- '+err);
	}

	return dateBreakUp;
}

function getMonthName(currentDate) {
	currentDate = nlapiStringToDate(currentDate)
	var monthNames = [ "JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JUL", "AUG",
	        "SEP", "OCT", "NOV", "DEC" ];
	return monthNames[currentDate.getMonth()];
}

function MonthNames(yr) {
	var yr1 = Number(yr) + 1;
	
	var monthNames = [ "JAN_"+yr, "FEB_"+yr, "MAR_"+yr, "APR_"+yr, "MAY_"+yr, "JUN_"+yr, "JUL_"+yr, "AUG_"+yr,
	        "SEP_"+yr, "OCT_"+yr, "NOV_"+yr, "DEC_"+yr ];
	var month_nxt_yr = ["JAN_"+yr1, "FEB_"+yr1, "MAR_"+yr1, "APR_"+yr1, "MAY_"+yr1, "JUN_"+yr1, "JUL_"+yr1, "AUG_"+yr1,
	        "SEP_"+yr1, "OCT_"+yr1, "NOV_"+yr1, "DEC_"+yr1 ];
	monthNames = monthNames.concat(month_nxt_yr);
	
	return monthNames;
}

function yearData(yr) {
	var yr1 = Number(yr) + 1;
	
	var monthNames = [ "JAN_"+yr, "FEB_"+yr, "MAR_"+yr, "APR_"+yr, "MAY_"+yr, "JUN_"+yr, "JUL_"+yr, "AUG_"+yr,
	        "SEP_"+yr, "OCT_"+yr, "NOV_"+yr, "DEC_"+yr ];
	var month_nxt_yr = ["JAN_"+yr1, "FEB_"+yr1, "MAR_"+yr1, "APR_"+yr1, "MAY_"+yr1, "JUN_"+yr1, "JUL_"+yr1, "AUG_"+yr1,
	        "SEP_"+yr1, "OCT_"+yr1, "NOV_"+yr1, "DEC_"+yr1 ];
	monthNames = monthNames.concat(month_nxt_yr);
	
	for (var i = 0; i < monthNames.length; i++) {
		this[monthNames[i]] = {
		    Amount : 0,
			prcnt_allocated : 0,
			total_revenue : 0
		};
	}
}
	
function searchForPractice(project)
{
	try
	{
		var a_JSON = {};
		var a_dataRows = [];
		var a_filters_project = new Array();
		a_filters_project.push(new nlobjSearchFilter('project',null,'anyof',project));
		
		var a_column_get_practice = new Array();
		a_column_get_practice.push(new nlobjSearchColumn('custevent_practice',null,'group'));
		a_column_get_practice.push(new nlobjSearchColumn('custrecord_parent_practice','custevent_practice','group'));
		
		var a_search_allocation = nlapiSearchRecord('resourceallocation',null,a_filters_project,a_column_get_practice);
		if(a_search_allocation){
			for(var i=0;i<a_search_allocation.length;i++){
				
				a_JSON = {
						sub_practice: a_search_allocation[i].getValue('custevent_practice',null,'group'),
						parent_practice: a_search_allocation[i].getValue('custrecord_parent_practice','custevent_practice','group')
				}
				a_dataRows.push(a_JSON);
			}
		}
		return a_dataRows;
	}
	catch(err)
	{
		nlapiLogExecution('ERROR','searchForPractice :- ',err);
	}

}

function getMonthName_FromMonthNumber(month_number)
{
	var a_month_name = [ "JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JUL", "AUG",
	        "SEP", "OCT", "NOV", "DEC" ];
								
	return a_month_name[month_number];
}

function _logValidation(value) 
{
 if(value != null && value.toString() != null && value != '' && value != undefined && value.toString() != undefined && value != 'undefined' && value.toString() != 'undefined'&& value.toString() != 'NaN' && value != NaN) 
 {
  return true;
 }
 else 
 { 
  return false;
 }
}

function searchJournal(d_strtdate,d_endDate,s_project_id){
	
	var f_amt_captured = 0;
	var a_filters = [];
	a_filters.push(new nlobjSearchFilter('type', null, 'anyof', 'Journal'));
	a_filters.push(new nlobjSearchFilter('trandate', null, 'within', d_strtdate,d_endDate));
	a_filters.push(new nlobjSearchFilter('custcol_project_entity_id', null, 'is', s_project_id));
	
								  
		var a_columns = new Array();
		a_columns[0] = new nlobjSearchColumn('custcol_project_entity_id',null,'group');
		a_columns[1] = new nlobjSearchColumn('fxamount',null,'sum');
		
		var tran_search = nlapiSearchRecord('transaction',null,a_filters,a_columns);
		if(tran_search){
			f_amt_captured = tran_search[0].getValue('fxamount',null,'sum');
		}
	return f_amt_captured;
} 

function searchMilestone(billing_sch)
{
                var billing_milestone_id=nlapiLoadRecord('billingschedule',billing_sch);
                var lineItemCount=billing_milestone_id.getLineItemCount('milestone');
                var isZeroAdded = false;
                var billing_JSON=[];
                var billing_amount=[];
                for (var linenum = 1; linenum <= lineItemCount; linenum++) {
                                                var currentMileStonePercent = billing_milestone_id.getLineItemValue('milestone',
                                                                                'milestoneamount', linenum);
 
                                                // remove the %age sign
                                                var currentMileStonePercentNumeric = currentMileStonePercent
                                                                                .split('%')[0].trim();
 
                                                if (currentMileStonePercentNumeric == 0) {
                                                                isZeroAdded = true;
                                                                break;
                                                }
 
                                                nlapiLogExecution('debug', 'currentMileStonePercentNumeric',
                                                                                currentMileStonePercentNumeric);
                               
                                                var currentMileStoneMont = billing_milestone_id.getLineItemValue('milestone',
                                                                                'milestonedate', linenum);
                                                                //            currentMilestoneMonth=getMonthName(currentMilestoneMonth);
                                                                var current_mnth=getMonthName(currentMileStoneMont);
                                                               
                                                                                var month_strt = nlapiStringToDate(currentMileStoneMont);
                                                                                var i_year = month_strt.getFullYear();
                                                                                nlapiLogExecution('debug', 'i_year',
                                                                                i_year);
                                                                                month_strt=current_mnth+'_'+i_year;
                                                                                nlapiLogExecution('debug', 'currentMileStoneMonth',
                                                                                month_strt);     
                                                // check if any invalid amount was entered in comments
                                                var currentLineAmountText = billing_milestone_id.getLineItemValue('milestone',
                                                                                'comments', linenum);
                                                                                                var currentLineAmountNumeric = extractNumberFromComment(currentLineAmountText);
                                                                nlapiLogExecution('debug', 'currentLineAmountNumeric',
                                                                                                currentLineAmountNumeric);
                                                                                                billing_JSON={
                                                                                                                month:month_strt,
                                                                                                                amount:currentLineAmountNumeric
                                                                                                };
                                                                                                billing_amount.push(billing_JSON);
                                                nlapiLogExecution('debug', 'currentLineAmountText',
                                                                                currentLineAmountText);
                                                if (!currentLineAmountText) {
                                                                throw "Amount entered in comments is not valid";
                                                }
                                }
                                if (isZeroAdded) {
                                                // get the project value
                                                var projectId = nlapiGetFieldValue('project');
                                                var projectValue = nlapiLookupField('job', projectId,
                                                                                'custentity_projectvalue');
                                                var sum = 0;
 
                                                // loop through line item and calculate percentage
                                                for (var linenum = 1; linenum < lineItemCount; linenum++) {
                                                                // read the amount from comments field
                                                                var currentLineAmountText = billing_milestone_id.getLineItemValue('milestone',
                                                                                                'comments', linenum);
 
                                                                // extract the amount (number) part from the currency text
                                                                var currentLineAmountNumeric = extractNumberFromComment(currentLineAmountText);
                                                                nlapiLogExecution('debug', 'currentLineAmountNumeric',
                                                                                                currentLineAmountNumeric);
 
                                                                // calculate the %age milestone using the project value
                                                                var percentage = (currentLineAmountNumeric / projectValue) * 100;
 
                                                                // set the new milestone %age
                                                               
 
                                                                sum += parseFloat(percentage.toFixed(4));
                                                }
 
                                                // to calculate the last line %age, subtract the sum from 100
                                                var lastLinePercentage = 100 - sum;
                                                nlapiSetLineItemValue('milestone', 'milestoneamount',
                                                                                lineItemCount, lastLinePercentage);
                                }
                return billing_amount;
 
}

function findAllocationPracticePresent(a_proj_prac_details, subpractice, level, subsi, i_resource_location)
{
	for(var i_index_arr_srchng=0; i_index_arr_srchng<a_proj_prac_details.length; i_index_arr_srchng++)
	{
		//if(a_proj_prac_details[i_srch_arr].i_practice == practice)
		{
			//nlapiLogExecution('audit','prac:- '+practice);
			if(parseFloat(a_proj_prac_details[i_index_arr_srchng].i_sub_practice) == parseFloat(subpractice))
			{
				if(subpractice == 324)
				{
					nlapiLogExecution('audit','sub prac:- '+subpractice);
					nlapiLogExecution('audit','level before match:- '+level);
				}
				
				if(parseFloat(a_proj_prac_details[i_index_arr_srchng].s_emp_level) == parseFloat(level))
				{
					//nlapiLogExecution('audit','level:- '+level);
					if(parseFloat(a_proj_prac_details[i_index_arr_srchng].subsidiary) == parseFloat(subsi))
					{
						//nlapiLogExecution('audit','subsi:- '+subsi);
						if(parseFloat(a_proj_prac_details[i_index_arr_srchng].i_resource_location) == parseFloat(i_resource_location))
						{
							//nlapiLogExecution('audit','loc:- '+i_resource_location);
							return i_index_arr_srchng;
						}
					}
				}
			}
		}
	}
	
	return -1;
}
 
function extractNumberFromComment(textAmount) {
                var index = textAmount.indexOf('$');
                var index2 = textAmount.indexOf('INR');
                var index3 = textAmount.indexOf('GBP');
                var index4 = textAmount.indexOf('£');
               
               
/*           nlapiLogExecution('debug', 'index', index);
                nlapiLogExecution('debug', 'index2', index2);
                nlapiLogExecution('debug', 'index3', index3);
                nlapiLogExecution('debug', 'index4', index4);*/
               
               
                if (index == -1 && index2 == -1 && index3 == -1 && index4 == -1) {
                                throw "Invalid Amount in comments - No $ or INR symbol found.";
                }
 
                if (index > 0 && index2 > 0 && index3 > 0 && index4> 0) {
                                throw "Invalid Amount Format - should start with $ or INR sign";
                }
 
                //var splitIndex = index != -1 ? '$' : 'INR';
               
                var splitIndex = '';
 
                if(index >= 0)
                {
                                splitIndex = '$';
                }
                else if(index2 >= 0)
                {
                                splitIndex = 'INR';
                }
                else if(index3 >= 0)
                {
                                splitIndex = 'GBP';
                }
                else
                {
                                splitIndex= '£';
                }
                               
                if(splitIndex)
                {
                                var amount = textAmount.split(splitIndex)[1].trim();
                }
 
                nlapiLogExecution('debug', 'amount', amount);
 
                if (!isNumeric(amount)) {
                                throw "Invalid Amount - enter only numbers";
                }
 
                return amount;
}
function isNumeric(value) {
                return !isNaN(value);
}
function search_ytd_rev(s_currentMonthName,s_currentYear,s_project_id)
{
		var mnth_amount=0;
		var a_filter_get_ytd_revenue_recognized = [['custrecord_project_to_recognize_amount', 'anyof', s_project_id],'and',
															['custrecord_month_to_recognize_amount', 'is', s_currentMonthName], 'and',
															['custrecord_year_to_recognize_amount', 'is', (s_currentYear).toString()]];
					
				var a_columns_get_ytd_revenue_recognized = new Array();
				a_columns_get_ytd_revenue_recognized[0] = new nlobjSearchColumn('custrecord_project_to_recognize_amount');
				a_columns_get_ytd_revenue_recognized[1] = new nlobjSearchColumn('custrecord_practice_to_recognize_amount');
				a_columns_get_ytd_revenue_recognized[2] = new nlobjSearchColumn('custrecord_subparctice_to_recognize_amnt');
				a_columns_get_ytd_revenue_recognized[3] = new nlobjSearchColumn('custrecord_role_to_recognize_amount');
				a_columns_get_ytd_revenue_recognized[4] = new nlobjSearchColumn('custrecord_level_to_recognize_amount');
				a_columns_get_ytd_revenue_recognized[5] = new nlobjSearchColumn('custrecord_revenue_recognized');
				a_columns_get_ytd_revenue_recognized[6] = new nlobjSearchColumn('custrecord_month_to_recognize_amount');
				a_columns_get_ytd_revenue_recognized[7] = new nlobjSearchColumn('custrecord_year_to_recognize_amount');
				//a_columns_get_ytd_revenue_recognized[8] = new nlobjSearchColumn('custrecord_subtotal_amount');
				a_columns_get_ytd_revenue_recognized[8] = new nlobjSearchColumn('customer','custrecord_project_to_recognize_amount');
				
				var a_get_ytd_revenue = nlapiSearchRecord('customrecord_fp_rev_rec_recognized_amnt', null, a_filter_get_ytd_revenue_recognized, a_columns_get_ytd_revenue_recognized);
		if (a_get_ytd_revenue)
		{
				for(var i_rev_rec=0;i_rev_rec<a_get_ytd_revenue.length;i_rev_rec++)
				{
					var mnth_rec_amnt=a_get_ytd_revenue[i_rev_rec].getValue('custrecord_revenue_recognized');
					mnth_amount=parseFloat(mnth_amount)+parseFloat(mnth_rec_amnt);
				}
		}
		return mnth_amount;
}

function search_ytd_rev_pract(s_currentMonthName,s_currentYear,s_project_id,practice_line)
{
		var mnth_amount=0;
		var a_filter_get_ytd_revenue_recognized = [['custrecord_project_to_recognize_amount', 'anyof', s_project_id],'and',
															['custrecord_month_to_recognize_amount', 'is', s_currentMonthName], 'and',
															['custrecord_year_to_recognize_amount', 'is', (s_currentYear).toString()]];
					
				var a_columns_get_ytd_revenue_recognized = new Array();
				a_columns_get_ytd_revenue_recognized[0] = new nlobjSearchColumn('custrecord_project_to_recognize_amount');
				a_columns_get_ytd_revenue_recognized[1] = new nlobjSearchColumn('custrecord_practice_to_recognize_amount');
				a_columns_get_ytd_revenue_recognized[2] = new nlobjSearchColumn('custrecord_subparctice_to_recognize_amnt');
				a_columns_get_ytd_revenue_recognized[3] = new nlobjSearchColumn('custrecord_role_to_recognize_amount');
				a_columns_get_ytd_revenue_recognized[4] = new nlobjSearchColumn('custrecord_level_to_recognize_amount');
				a_columns_get_ytd_revenue_recognized[5] = new nlobjSearchColumn('custrecord_revenue_recognized');
				a_columns_get_ytd_revenue_recognized[6] = new nlobjSearchColumn('custrecord_month_to_recognize_amount');
				a_columns_get_ytd_revenue_recognized[7] = new nlobjSearchColumn('custrecord_year_to_recognize_amount');
				//a_columns_get_ytd_revenue_recognized[8] = new nlobjSearchColumn('custrecord_subtotal_amount');
				a_columns_get_ytd_revenue_recognized[8] = new nlobjSearchColumn('customer','custrecord_project_to_recognize_amount');
				
				var a_get_ytd_revenue = nlapiSearchRecord('customrecord_fp_rev_rec_recognized_amnt', null, a_filter_get_ytd_revenue_recognized, a_columns_get_ytd_revenue_recognized);
		if (a_get_ytd_revenue)
		{
				for(var i_rev_rec=0;i_rev_rec<a_get_ytd_revenue.length;i_rev_rec++)
				{
					var sub_pra_ytd=a_get_ytd_revenue[i_rev_rec].getText('custrecord_subparctice_to_recognize_amnt');
					var mnth_rec_amnt=a_get_ytd_revenue[i_rev_rec].getValue('custrecord_revenue_recognized');
					if(sub_pra_ytd==practice_line)
					{
						mnth_amount=parseFloat(mnth_amount)+parseFloat(mnth_rec_amnt);
					}
				}
		}
		return mnth_amount;
}
function remove_dulpi(practices_involved)
{
	var unique_array=new Array();
	for(var uni_prac=0;uni_prac<practices_involved.length;uni_prac++)
	{
		if(unique_array.indexOf(practices_involved[uni_prac])<0)
		{
			unique_array.push(practices_involved[uni_prac]);
		}
	}
	return unique_array;
}

function format2(n) {
    n = parseFloat(n).toFixed(1).replace(/(\d)(?=(\d{3})+\.)/g, "$1,");
	n = n.toString().split('.');
	return n[0];
}