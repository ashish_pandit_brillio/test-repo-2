/**
 * RESTlet to get expenses for approval
 * 
 * Version    Date            Author           Remarks
 * 1.00       23 Nov 2015     nitish.mishra
 *
 */

/**
 * @param {Object}
 *            dataIn Parameter object
 * @returns {Object} Output object
 */
function postRESTlet(dataIn) {
	var response = new Response();
	try {
		nlapiLogExecution('debug', 'dataIn', JSON.stringify(dataIn));
		var employeeId = getUserUsingEmailId(dataIn.EmailId);
		var requestType = dataIn.RequestType;
		var expenseId = dataIn.Data.ExpenseId;

		switch (requestType) {

			case M_Constants.Request.Get:

				if (expenseId) {
					response.Data = getExpenseDetail(expenseId, employeeId);
					response.Status = true;
				} else {
					response.Data = getExpensesPendingApproval(employeeId);
					response.Status = true;
				}
			break;

			case M_Constants.Request.Approve:
				response.Data = approveExpenses(dataIn.Data.ExpenseList,
				        employeeId);
				response.Status = true;
			break;

			case M_Constants.Request.Reject:
				response.Data = rejectExpenses(dataIn.Data.ExpenseList,
				        employeeId);
				response.Status = true;
			break;
		}

	} catch (err) {
		nlapiLogExecution('ERROR', 'postRESTlet', err);
		response.Data = err;
	}
	return response;
}

function getExpenseDetail(expenseId, firstLevelApprover) {
	try {
		var expenseReportRec = nlapiLoadRecord('expensereport', expenseId);

		// check access
		if (expenseReportRec.getFieldValue('custbody1stlevelapprover') == firstLevelApprover) {
			var expenseDetails = {};

			expenseDetails.Employee = expenseReportRec.getFieldText('entity');
			expenseDetails.FirstLevelApprover = expenseReportRec
			        .getFieldText('custbody1stlevelapprover');
			expenseDetails.SecondLevelApprover = expenseReportRec
			        .getFieldText('custbody_expenseapprover');

			expenseDetails.Expense = {
			    Reference : expenseReportRec.getFieldValue('tranid'),
			    Purpose : isEmpty(expenseReportRec.getFieldValue('memo')) ? ''
			            : expenseReportRec.getFieldValue('memo'),
			    Status : expenseReportRec.getFieldValue('status'),
			    InternalId : expenseReportRec.getId(),
			    AdvanceToApply : expenseReportRec.getFieldValue('advance'),
			    VAT : expenseReportRec.getFieldValue('tax1amt'),
			    PostingDate : expenseReportRec.getFieldValue('trandate'),
			    TransactionDate : expenseReportRec
			            .getFieldValue('custbody_transactiondate'),
			    AccountName : expenseReportRec.getFieldText('account'),
			    Account : expenseReportRec.getFieldValue('account'),
			    // Reason : '',
			    Currency : expenseReportRec.getFieldText('currency'),
			    DocumentReceived : expenseReportRec
			            .getFieldValue('custbody_exp_doc_received'),
			    DocumentReceivedDate : '',
			    ItemList : []
			// ApproverRemark : expenseReportRec.getFieldValue(''),
			};

			expenseDetails.Expense.Summary = {
			    Expenses : expenseReportRec.getFieldValue('total'),
			    NonReimbursableExpenses : expenseReportRec
			            .getFieldValue('nonreimbursable'),
			    ReimbursableExpenses : expenseReportRec
			            .getFieldValue('reimbursable'),
			    AdvanceToApply : expenseReportRec.getFieldValue('advance'),
			    TotalReimbursableAmount : expenseReportRec
			            .getFieldValue('amount')
			};

			// get all line items
			var lineItemCount = expenseReportRec.getLineItemCount('expense');
			var expenseList = [];
			var isReimbursable = null;
			var projectId = null;
			var projectIdList = [];

			for (var line = 1; line <= lineItemCount; line++) {
				isReimbursable = expenseReportRec.getLineItemValue('expense',
				        'isnonreimbursable', line);
				projectId = expenseReportRec.getLineItemValue('expense',
				        'customer', line);
				projectIdList.push(projectId);

				// if (isFalse(isReimbursable)) {

				if (!expenseDetails.ProjectText) {
					expenseDetails.ProjectText = expenseReportRec
					        .getLineItemValue('expense', 'custcolprj_name',
					                line);
				}
				expenseList.push({
				    ProjectText : expenseReportRec.getLineItemValue('expense',
				            'custcolprj_name', line),
				    ProjectId : projectId,
				    Vertical : expenseReportRec.getLineItemValue('expense',
				            'class_display', line),
				    CategoryText : expenseReportRec.getLineItemValue('expense',
				            'category_display', line),
				    Amount : expenseReportRec.getLineItemValue('expense',
				            'foreignamount', line),
				    ForeignAmount : expenseReportRec.getLineItemValue(
				            'expense', 'amount', line),
				    Currency : expenseReportRec.getLineItemText('expense',
				            'currency', line),
				    IsReimbursable : isReimbursable,
				    DeliveryManager : null
				});
				// }
			}

			expenseDetails.Expense.ItemList = expenseList;
			return expenseDetails;
		} else {
			throw "You are not authorised to access this record";
		}
	} catch (err) {
		nlapiLogExecution('error', 'getExpenseDetail', err);
		throw err;
	}
}

function getExpensesPendingApproval(firstLevelApprover) {
	try {
		var filters = [
		        new nlobjSearchFilter('custbody1stlevelapprover', null,
		                'anyof', firstLevelApprover),
		        new nlobjSearchFilter('status', null, 'anyof', [ 'ExpRept:B' ]),
		        new nlobjSearchFilter('mainline', null, 'is', 'T') ];

		var columns = [ new nlobjSearchColumn('transactionnumber'),
		        new nlobjSearchColumn('memo'),
		        new nlobjSearchColumn('trandate').setSort(true),
		        new nlobjSearchColumn('fxamount'),
		        new nlobjSearchColumn('currency'),
		        new nlobjSearchColumn('subsidiary'),
		        new nlobjSearchColumn('entity') ];

		var pendingExpenseSearch = nlapiSearchRecord('expensereport', null,
		        filters, columns);

		var expenseList = [];

		if (pendingExpenseSearch) {
			for (var i = 0; i < pendingExpenseSearch.length && i < 50; i++) {
				var expense = new ExpensesListItem();
				expense.InternalId = pendingExpenseSearch[i].getId();
				expense.ExpenseName = pendingExpenseSearch[i]
				        .getValue('transactionnumber');
				expense.EmployeeName = pendingExpenseSearch[i]
				        .getText('entity');
				expense.TotalAmount = pendingExpenseSearch[i]
				        .getValue('fxamount');
				expense.Currency = pendingExpenseSearch[i].getText('currency');
				expense.Subsidiary = pendingExpenseSearch[i]
				        .getText('subsidiary');
				expense.TransactionDate = pendingExpenseSearch[i]
				        .getValue('trandate');
				expense.Memo = pendingExpenseSearch[i].getValue('memo');

				expenseList.push(expense);
			}
		}

		return expenseList;
	} catch (err) {
		nlapiLogExecution('error', 'getExpensesPendingApproval', err);
		throw err;
	}
}

function approveExpenses(expenseList, firstApproverId) {
	try {

		if (expenseList) {
			var noOfApprovedExpenses = 0;

			for (var i = 0; i < expenseList.length; i++) {
				var expenseRecord = nlapiLoadRecord('expensereport',
				        expenseList[i], {
					        recordmode : 'dynamic'
				        });

				// check access
				if (expenseRecord.getFieldValue('custbody1stlevelapprover') == firstApproverId) {

					// check the status of the expense
					if (expenseRecord.getFieldValue('supervisorapproval') == 'F') {
						expenseRecord.setFieldValue('supervisorapproval', 'T');
						try {
							var expenseId = nlapiSubmitRecord(expenseRecord);
							createLog('expensereport', expenseId, 'Update',
							        firstApproverId, 'Approved');
							noOfApprovedExpenses += 1;
						} catch (e) {
							nlapiLogExecution('error',
							        'failed to approve expense : '
							                + expenseList[i], e);

							if (expenseList.length == 1) {
								throw e;
							}
						}
					} else if (expenseList.length == 1) {
						throw "You cannot approve this expense.";
					}
				} else if (expenseList.length == 1) {
					throw "You are not authorized to access this record.";
				}
			}

			// check no. of expenses approved
			if (noOfApprovedExpenses == expenseList.length) {
				return "Expenses Approved Succesfully";
			} else {
				return "Done. Some approvals failed.";
			}
		} else {
			throw "No Expenses to approve.";
		}
	} catch (err) {
		nlapiLogExecution('ERROR', 'approveExpenses', err);
		throw err;
	}
}

function rejectExpenses(expenseList, firstApproverId) {
	try {

		if (expenseList) {
			var noOfApprovedExpenses = 0;

			for (var i = 0; i < expenseList.length; i++) {
				var expenseRecord = nlapiLoadRecord('expensereport',
				        expenseList[i], {
					        recordmode : 'dynamic'
				        });

				// check access
				if (expenseRecord.getFieldValue('custbody1stlevelapprover') == firstApproverId) {

					// mark the expense as in progress
					expenseRecord.setFieldValue('complete', 'F');
					try {
						var expenseId = nlapiSubmitRecord(expenseRecord);
						createLog('expensereport', expenseId, 'Update',
						        firstApproverId, 'Rejected');
						noOfApprovedExpenses += 1;
					} catch (e) {
						nlapiLogExecution('error',
						        'failed to reject expense : ' + expenseList[i],
						        e);

						if (expenseList.length == 1) {
							throw e;
						}
					}
				} else if (expenseList.length == 1) {
					throw "You are not authorized to access this record.";
				}
			}

			// check no. of expenses approved
			if (noOfApprovedExpenses == expenseList.length) {
				return "Expenses Rejected Succesfully";
			} else {
				return "Done. Some rejections failed.";
			}
		} else {
			throw "No Expenses to reject.";
		}
	} catch (err) {
		nlapiLogExecution('ERROR', 'rejectExpenses', err);
		throw err;
	}
}