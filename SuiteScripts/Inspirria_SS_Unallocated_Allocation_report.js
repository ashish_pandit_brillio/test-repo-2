/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       18 Mar 2020     Nihal Mulani
 *
 */

/**
 * @param {String} type Context Types: scheduled, ondemand, userinterface, aborted, skipped
 * @returns {Void}
 */
var SEARCH_ID = 'customsearch3284';
var GlobData = [];
function scheduledReport() 
{
	var context = nlapiGetContext();
	var d_start_date = context.getSetting('script', 'custscript_start_date_un');
	var d_end_date = context.getSetting('script', 'custscript_end_date_un');
	nlapiLogExecution('DEBUG', 'd_start_date Para',d_start_date);
	nlapiLogExecution('DEBUG', 'd_end_date Para',d_end_date);
	var json = runSearch(SEARCH_ID, d_start_date, d_end_date);
	//nlapiLogExecution('DEBUG', 'GlobData',JSON.stringify(json));
	for(var t=0;t<json.length;t++)
	{
		check_emp_over_or_under_allocated(json[t].employee, d_start_date, d_end_date)
	}
	//nlapiLogExecution('DEBUG', 'GlobData',JSON.stringify(GlobData));

	var con = newunallocatedresource(d_start_date,d_end_date); 
	nlapiLogExecution('DEBUG', 'con',JSON.stringify(con));

	var hardHeaders = ["RESOURCE","SUBSIDIARY","DEPARTMENT","LOCATION","REPORTING MANAGER","HIRE DATE","STATUS"];
	var CSVData = '<?xml version="1.0"?><?mso-application progid="Excel.Sheet"?>';
	CSVData += '<Workbook xmlns="urn:schemas-microsoft-com:office:spreadsheet" ';
	CSVData += 'xmlns:o="urn:schemas-microsoft-com:office:office" ';
	CSVData += 'xmlns:x="urn:schemas-microsoft-com:office:excel" ';
	CSVData += 'xmlns:ss="urn:schemas-microsoft-com:office:spreadsheet" ';
	CSVData += 'xmlns:html="http://www.w3.org/TR/REC-html40">';

	CSVData += '<Styles>';

	CSVData += '<Style ss:ID="s1">';
	CSVData += '<Font ss:Bold="1" ss:Underline="Single"/>';
	CSVData += '<Interior ss:Color="#CCFFFF" ss:Pattern="Solid"/>';
	CSVData += ' <Borders>';
	CSVData += ' <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>';
	CSVData += '</Borders>';
	CSVData += '</Style>';

	CSVData += '</Styles>';

	CSVData += '<Worksheet ss:Name="Report">';
	CSVData += '<Table><Row>';
	for (var k = 0; k < hardHeaders.length; k++) {
		CSVData += '<Cell ss:StyleID="s1"><Data ss:Type="String">' + hardHeaders[k] + '</Data></Cell>';
	}
	CSVData += '</Row>';
	for (var i = 0; i < GlobData.length; i++) {

		CSVData += '<Row>';
		CSVData += '<Cell><Data ss:Type="String">' + GlobData[i].employee + '</Data></Cell>';
		CSVData += '<Cell><Data ss:Type="String">' + GlobData[i].subsidiary + '</Data></Cell>';
		CSVData += '<Cell><Data ss:Type="String">' + GlobData[i].department + '</Data></Cell>';
		CSVData += '<Cell><Data ss:Type="String">' + GlobData[i].location + '</Data></Cell>';
		CSVData += '<Cell><Data ss:Type="String">' + GlobData[i].reportingmanager + '</Data></Cell>';
		CSVData += '<Cell><Data ss:Type="String">' + GlobData[i].actual_hire_date + '</Data></Cell>';
		CSVData += '<Cell><Data ss:Type="String">' + GlobData[i].status + '</Data></Cell>';
		CSVData += '</Row>';
	}
	for (var iq = 0; iq < con.length; iq++) {

		CSVData += '<Row>';
		CSVData += '<Cell><Data ss:Type="String">' + con[iq].employee + '</Data></Cell>';
		CSVData += '<Cell><Data ss:Type="String">' + con[iq].subsidiary + '</Data></Cell>';
		CSVData += '<Cell><Data ss:Type="String">' + con[iq].department + '</Data></Cell>';
		CSVData += '<Cell><Data ss:Type="String">' + con[iq].location + '</Data></Cell>';
		CSVData += '<Cell><Data ss:Type="String">' + con[iq].reportingmanager + '</Data></Cell>';
		CSVData += '<Cell><Data ss:Type="String">' + con[iq].actual_hire_date + '</Data></Cell>';
		CSVData += '<Cell><Data ss:Type="String">' + con[iq].status + '</Data></Cell>';
		CSVData += '</Row>';
	}
	CSVData += '</Table></Worksheet></Workbook>';
	var xlsFile = nlapiCreateFile('Report.xls', 'EXCEL', nlapiEncrypt(CSVData, 'base64'));
	var body = "Hi "+nlapiGetContext().name +"<br/><br/>";
	body+= "Please find the attached Un-allocated Excel report.";
	var subject = "Un-Allocated resource report for the month "+d_start_date+" to "+d_end_date;
	nlapiSendEmail("442",nlapiGetContext().email, subject ,body,null,null,null,xlsFile);
}
function check_emp_over_or_under_allocated(i_employee, d_start_date, d_end_date)
{
	var i_internal_ID;

	var i_hours_per_week;
	var i_project_name;
	var i_allocated_hours;
	var flag = 0;

	var month_start_date = d_start_date;
	var month_end_date = d_end_date;

	//--------------------------employee load to search hire date and termination date-------------
	var o_empOBJ = nlapiLoadRecord('employee', i_employee) //
	if (_logValidation(o_empOBJ)) //
	{//Employee OBJ
		var i_hire_date = o_empOBJ.getFieldValue('hiredate');
		//	nlapiLogExecution('DEBUG', 'monthly_JE_creation', ' Hire Date  -->' + i_hire_date);

		//var i_resigned_date = o_empOBJ.getFieldValue('releasedate'); // This line is commented due to update in logic : we are now using custom field for validation.
		var i_resigned_date = o_empOBJ.getFieldValue('custentity_lwd');
		//nlapiLogExecution('DEBUG', 'monthly_JE_creation', ' Resigned Date -->' + i_resigned_date);

	}//Employee OBJ

	var i_hire_date_dt_format = null;
	if(i_hire_date!= null) //
	{
		i_hire_date_dt_format = nlapiStringToDate(i_hire_date);
	}

	var i_resigned_date_dt_format = null;
	if(_logValidation(i_resigned_date)) //
	{
		//nlapiLogExecution('DEBUG', ' monthly_JE_creation','*** Parsing Date ***');
		i_resigned_date_dt_format = nlapiStringToDate(i_resigned_date);
	}

	var s_month_start_date_dt_format = nlapiStringToDate(month_start_date);
	var s_month_end_date_dt_format = nlapiStringToDate(month_end_date);
	//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

	if (_logValidation(i_hire_date_dt_format) && !_logValidation(i_resigned_date_dt_format)) //i_month
	{
		//	nlapiLogExecution('DEBUG', ' monthly_JE_creation','*** Condition 1 ***');
		if ((Date.parse(i_hire_date_dt_format) <= Date.parse(s_month_end_date_dt_format) && Date.parse(i_hire_date_dt_format) >= Date.parse(s_month_start_date_dt_format))) //
		{
			//	nlapiLogExecution('DEBUG', ' monthly_JE_creation',' 1 Hire Date is current month .....');

			d_start_date = i_hire_date
			d_end_date = d_end_date
		}
		else //
		{
			//nlapiLogExecution('DEBUG', ' monthly_JE_creation',' 1 Hire Date is not current month .....');
			d_start_date = d_start_date
			d_end_date = d_end_date
		}

	}//Hire Date is not blank & Termination Date is blank
	if (_logValidation(i_hire_date_dt_format) && _logValidation(i_resigned_date_dt_format)) //
	{
		//	nlapiLogExecution('DEBUG', ' monthly_JE_creation','*** Condition 2 ***');
		//nlapiLogExecution('DEBUG', ' monthly_JE_creation',' both r present');
		if ((Date.parse(i_hire_date_dt_format) <= Date.parse(s_month_end_date_dt_format) && Date.parse(i_hire_date_dt_format) >= Date.parse(s_month_start_date_dt_format))) //
		{
			if ((Date.parse(i_resigned_date_dt_format) <= Date.parse(s_month_end_date_dt_format) && Date.parse(i_resigned_date_dt_format) >= Date.parse(s_month_start_date_dt_format))) //
			{
				//nlapiLogExecution('DEBUG', ' monthly_JE_creation',' 2 Hire Date is current month .....');
				d_start_date = i_hire_date;

				//nlapiLogExecution('DEBUG', ' monthly_JE_creation',' 2 Resigned Date is current month .....');
				d_end_date = i_resigned_date
			}
			else //
			{
				//	//nlapiLogExecution('DEBUG', ' monthly_JE_creation',' 2 A Hire Date is current month .....');
				d_start_date = i_hire_date;

				//	//nlapiLogExecution('DEBUG', ' monthly_JE_creation',' 2 A Resigned Date is current month .....');
				d_end_date = d_end_date
			}
		}
		else 
			if ((Date.parse(i_resigned_date_dt_format) <= Date.parse(s_month_end_date_dt_format) && Date.parse(i_resigned_date_dt_format) >= Date.parse(s_month_start_date_dt_format))) //
			{
				d_start_date = d_start_date
				d_end_date = i_resigned_date;
			}
			else //
			{
				d_start_date = d_start_date
				d_end_date = d_end_date
			}
	}//Hire Date is not blank & Termination Date is not blank

	if (!_logValidation(i_hire_date_dt_format) && _logValidation(i_resigned_date_dt_format)) //
	{
		//nlapiLogExecution('DEBUG', ' monthly_JE_creation','*** Condition 3 ***');
		if ((Date.parse(i_resigned_date_dt_format) <= Date.parse(s_month_end_date_dt_format) && Date.parse(i_resigned_date_dt_format) >= Date.parse(s_month_start_date_dt_format))) //
		{
			//nlapiLogExecution('DEBUG', ' monthly_JE_creation',' no hire date but resigned present .....');

			d_start_date = d_start_date
			d_end_date = i_resigned_date

		}
		else //
		{
			d_start_date = d_start_date
			d_end_date = d_end_date

		}

	}//Hire Date is blank & Termination Date is not blank
	if (!_logValidation(i_hire_date_dt_format) && !_logValidation(i_resigned_date_dt_format)) //
	{
		//	nlapiLogExecution('DEBUG', ' monthly_JE_creation','*** Condition 4 ***');
		//nlapiLogExecution('DEBUG', ' monthly_JE_creation',' No Hire Date & Resigned Date .....');

		d_start_date = d_start_date;
		d_end_date = d_start_date;

	}//Hire Date is blank & Termination Date is blank
	if (_logValidation(d_start_date)) //
	{
		d_start_date = nlapiStringToDate(d_start_date);
		d_start_date = nlapiDateToString(d_start_date);
	}

	if (_logValidation(d_end_date)) //
	{
		d_end_date = nlapiStringToDate(d_end_date);
		d_end_date = nlapiDateToString(d_end_date)

	}

//	nlapiLogExecution('DEBUG', 'Test', 'd_start_date : ' + d_start_date);
//	nlapiLogExecution('DEBUG', 'Test', 'd_end_date : ' + d_end_date);

	//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++	
	var i_total_days = getDatediffIndays(d_start_date, d_end_date);
	//nlapiLogExecution('DEBUG', 'i_total_days', i_total_days);

	var i_total_sat_sun = getWeekend(d_start_date, d_end_date);
//	nlapiLogExecution('DEBUG', 'i_total_sat_sun', i_total_sat_sun);

	var tot_hrs = (parseInt(i_total_days) - parseInt(i_total_sat_sun)) * 8
//	nlapiLogExecution('DEBUG', 'tot_hrs', tot_hrs); //i_total_days
	//---------------------------------------------------------------------------------------------

	if (_logValidation(i_employee) && _logValidation(d_start_date) && _logValidation(d_end_date)) //
	{
		var filters = new Array();
		filters[0] = new nlobjSearchFilter('employee', null, 'is', i_employee)
		//filters[1] = new nlobjSearchFilter('date', null, 'onorafter', month_start_date)
		//filters[2] = new nlobjSearchFilter('date', null, 'onorbefore', month_end_date)

		filters[1] = new nlobjSearchFilter('date', null, 'onorafter', d_start_date);
		filters[2] = new nlobjSearchFilter('date', null, 'onorbefore', d_end_date);

		var i_search_results = nlapiSearchRecord('timeentry', 'customsearch_salaried_new_time_s_2_3_3', filters, null);

		if (_logValidation(i_search_results)) //
		{
			//if start
			//nlapiLogExecution('DEBUG', 'monthly_JE_creation', ' ----------- Salaried Time Search Length --------->' + i_search_results.length);

			var a_search_transaction_result = i_search_results[0];

			if (a_search_transaction_result == null || a_search_transaction_result == '' || a_search_transaction_result == undefined) //
			{

			}
			var columns = a_search_transaction_result.getAllColumns();

			var columnLen = columns.length;
			var i_duration = a_search_transaction_result.getValue(columns[1]);

			/*for (var hg = 0; hg < columnLen; hg++)
				 {
				 var column = columns[hg];
				 var label = column.getLabel();
				 var value = a_search_transaction_result.getValue(column)
				 var text = a_search_transaction_result.getText(column)
				 if (label == 'Duration')
				 {
				 var i_duration = value;
				 }
				 }*/
//			nlapiLogExecution('DEBUG','i_duration',i_duration);

			if (parseFloat(i_duration) > parseFloat(tot_hrs)) //
			{
				GlobData.push({
					"employee":o_empOBJ.getFieldValue("entityid"),
					"actual_hire_date": o_empOBJ.getFieldValue("hiredate"),
					"subsidiary":o_empOBJ.getFieldText("subsidiary"),
					"department":o_empOBJ.getFieldText("department"),
					"location":o_empOBJ.getFieldText("location"),
					"reportingmanager":o_empOBJ.getFieldText("custentity_reportingmanager"),
					"status":"over allocation"
				});
			}else if (parseFloat(i_duration) < parseFloat(tot_hrs)) //
			{
				GlobData.push({
					"employee":o_empOBJ.getFieldValue("entityid"),
					"actual_hire_date": o_empOBJ.getFieldValue("hiredate"),
					"subsidiary":o_empOBJ.getFieldText("subsidiary"),
					"department":o_empOBJ.getFieldText("department"),
					"location":o_empOBJ.getFieldText("location"),
					"reportingmanager":o_empOBJ.getFieldText("custentity_reportingmanager"),
					"status":"under allocation"
				});
			}
		}
	}
} 
function getDatediffIndays(startDate, endDate)
{
	var one_day=1000*60*60*24;
	var fromDate = startDate;
	var toDate=endDate

	var date1 = nlapiStringToDate(fromDate);

	var date2 = nlapiStringToDate(toDate);

	var date3=Math.round((date2-date1)/one_day);

	return (date3+1);
}

function getWeekend(startDate, endDate) //
{
	var start = new Date(startDate),
	finish = new Date(endDate),
	dayMilliseconds = 1000 * 60 * 60 * 24;

	var weekendDays = 0;

	while (start <= finish) {
		var day = start.getDay()
		if (day == 0 || day == 6) {
			weekendDays++;
		}
		start = new Date(+start + dayMilliseconds);
	}
	return weekendDays;
}
function _logValidation(value) 
{
	if(value!='null' && value != null && value != null && value != '' && value != undefined && value != undefined && value != 'undefined' && value!= 'undefined'&& value!= 'NaN' && value != NaN) 
	{
		return true;
	}
	else 
	{ 
		return false;
	}
}
function runSearch(searchId, startDate, endDate) {
	var resourceallocationSearch = nlapiCreateSearch("resourceallocation",
			[
				["job.status","noneof","1"], 
				"AND", 
				["startdate","onorafter",startDate], 
				"AND", 
				["enddate","onorbefore",endDate]
				], 
				[
					new nlobjSearchColumn("resource",null,"GROUP").setSort(false)
					]
	);

	var resultSet = resourceallocationSearch.runSearch();
	var totalRecords = 0;

	var searchResultCount = 0;
	var resultSlice = null;
	var searchResult = [];

	do {
		resultSlice = resultSet.getResults(searchResultCount,searchResultCount + 1000);
		if (resultSlice)
		{
			resultSlice.forEach(function(result) {

				searchResult.push(result);
				searchResultCount++;
			});

		}

	} while (resultSlice && resultSlice.length >= 1000);
	nlapiLogExecution('debug','searchResult.length',searchResult.length);
	debugger
	var resultsofData = new Array();
	for(var i = 0; i < searchResult.length; i++) {

		var resource = searchResult[i].getValue("resource",null,"GROUP");

		resultsofData.push({
			"employee": resource
		});
	}
	return resultsofData;
}
function newunallocatedresource(sDate,eDate)
{
	var employeeSearch = nlapiSearchRecord("employee",null,
			[
				["hiredate","onorafter",sDate], 
				"AND", 
				["hiredate","onorbefore",eDate],
				"AND",
				["custentity_employee_inactive","is","F"], 
				"AND", 
				["resourceallocation.internalid","anyof","@NONE@"]
				], 
				[
					new nlobjSearchColumn("entityid").setSort(false), 
					new nlobjSearchColumn("subsidiary"), 
					new nlobjSearchColumn("location"), 
					new nlobjSearchColumn("department"), 
					new nlobjSearchColumn("custentity_reportingmanager"), 
					new nlobjSearchColumn("hiredate")
					]
	);
	var tempGlobData = [];
	for (var n=0;n<employeeSearch.length;n++)
	{

		tempGlobData.push({
			"employee":employeeSearch[n].getValue("entityid"),
			"actual_hire_date": employeeSearch[n].getValue("hiredate"),
			"subsidiary":employeeSearch[n].getText("subsidiary"),
			"department":employeeSearch[n].getText("department"),
			"location":employeeSearch[n].getText("location"),
			"reportingmanager":employeeSearch[n].getText("custentity_reportingmanager"),
			"status":"Allocation not available"
		});
	}
	return tempGlobData;
}