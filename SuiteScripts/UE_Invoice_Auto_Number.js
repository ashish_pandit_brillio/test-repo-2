/**
 * Set the invoice # as per the last invoice
 * 
 * Version Date Author Remarks 1.00 14 Aug 2015 nitish.mishra
 * 
 */

// clear the invoice number field on create before load
function userEventBeforeLoad(type, form) {

	// run only while invoice is being created
	if (type == 'create' || type == 'copy') {
//
		if (nlapiGetFieldValue('subsidiary') == '2' || nlapiGetFieldValue('subsidiary') == '11'|| nlapiGetFieldValue('subsidiary') == '10' || nlapiGetFieldValue('subsidiary') == '14') {
			nlapiSetFieldValue('tranid', '');
		}
	}
}

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your
 * script deployment.
 * 
 * @appliedtorecord recordType
 * 
 * @param {String}
 *            type Operation types: create, edit, delete, xedit approve, reject,
 *            cancel (SO, ER, Time Bill, PO & RMA only) pack, ship (IF)
 *            markcomplete (Call, Task) reassign (Case) editforecast (Opp,
 *            Estimate)
 * @returns {Void}
 */
function userEventBeforeSubmit(type) {
	try {

		// run only while invoice is being created
		if (type == 'create') {
          
          var subSidary = nlapiGetFieldValue('subsidiary');

			// only for LLC customer
			if (subSidary == '2' || subSidary == '11' || subSidary == '10' || subSidary == '14') {
              
              if(subSidary=='2'){
                
                	var invoiceSearch = nlapiSearchRecord(null, '1040');
				var lastInvoiceNumber = invoiceSearch[0].getValue(
				        'formulanumeric', null, 'max');
				nlapiLogExecution('debug', 'lastInvoiceNumber',
				        lastInvoiceNumber);

				// increment it by 1
				var newTransactionId = parseInt(lastInvoiceNumber) + 1;

				// make it of 5 digits
				var newTransactionIdString = "0" + newTransactionId;
				nlapiLogExecution('debug', 'newTransactionIdString',
				        newTransactionIdString);

				// set the new value in the field
				nlapiSetFieldValue('tranid', newTransactionIdString);
               
              }
              else  if(subSidary=='11') {
                
                var invoiceSearch = nlapiSearchRecord(null, '3574');
				var lastInvoiceNumber = invoiceSearch[0].getValue(
				        'formulanumeric', null, 'MAX');
				nlapiLogExecution('debug', 'lastInvoiceNumber',
				        lastInvoiceNumber);

				// increment it by 1
				var newTransactionId = parseInt(lastInvoiceNumber) + 1 ;

				// make it of 5 digits
				var newTransactionIdString = "0" + newTransactionId;
				nlapiLogExecution('debug', 'newTransactionIdString',
				        newTransactionIdString);

				// set the new value in the field
				nlapiSetFieldValue('tranid', newTransactionIdString);
                
              }
			  else if(subSidary=='10'){
                
                var invoiceSearch = nlapiSearchRecord(null, '3689');
				var lastInvoiceNumber = invoiceSearch[0].getValue(
				        'formulanumeric', null, 'MAX');
				nlapiLogExecution('debug', 'lastInvoiceNumber',
				        lastInvoiceNumber);
				
				// increment it by 1
				var newTransactionId = parseInt(lastInvoiceNumber) + 1 ;
				
				// make it of 5 digits
				var newTransactionIdString = "0" + newTransactionId;
				nlapiLogExecution('debug', 'newTransactionIdString',
				        newTransactionIdString);

				// set the new value in the field
				nlapiSetFieldValue('tranid', newTransactionIdString);
                
              }
			  
			  else if(subSidary=='14'){
                
                var invoiceSearch = nlapiSearchRecord(null, '4129');
				var lastInvoiceNumber = invoiceSearch[0].getValue(
				        'formulanumeric', null, 'MAX');
				nlapiLogExecution('debug', 'lastInvoiceNumber',
				        lastInvoiceNumber);
				
				// increment it by 1
				var newTransactionId = parseInt(lastInvoiceNumber) + 1 ;
				
				// make it of 5 digits
				var newTransactionIdString = "0" + newTransactionId;
				nlapiLogExecution('debug', 'newTransactionIdString',
				        newTransactionIdString);

				// set the new value in the field
				nlapiSetFieldValue('tranid', newTransactionIdString);
                
              }

				// get the invoice # from the latest submitted invoice
				
			}
         
		}
	} catch (err) {
		nlapiLogExecution('error', 'userEventBeforeSubmit', err);
		throw "ERROR IN AUTO-NUMBERING : \n<br/>" + err;
	}
}