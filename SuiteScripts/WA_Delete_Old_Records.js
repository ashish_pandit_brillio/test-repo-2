/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       23 Mar 2015     amol.sahijwani
 *
 */

/**
 * @returns {Void} Any or no return value
 */
function workflowAction() {
	try{var i_record_id	=	nlapiGetRecordId();
	var rec	=	nlapiLoadRecord(nlapiGetRecordType(), i_record_id);var s_month = rec.getFieldValue('custrecord_rev_rep_xml_data_month');
	var s_year = rec.getFieldValue('custrecord_rev_rep_xml_data_year');
	var context = nlapiGetContext();
	var a_data = getData(s_month, s_year, i_record_id);
	var i_deleted = 0;
	for (var i = 0; i < a_data.length; i++) {
		var i_internal_id = a_data[i].internal_id;nlapiLogExecution('AUDIT', 'Delete: ', a_data[i].internal_id);
		nlapiDeleteRecord('customrecord_revenue_report_data', i_internal_id);
		i_deleted++;
		if (context.getRemainingUsage() <= 10)
			return a_data.length - i_deleted;
	}
	return a_data.length - i_deleted;}catch(e){nlapiLogExecution('ERROR', 'Error: ', e.message); return 0;}
}
function getData(s_month, s_year, i_record_id) {
	var filters = new Array();
	filters[0] = new nlobjSearchFilter('custrecord_rev_rep_data_month', null,
			'equalto', s_month);
	filters[1] = new nlobjSearchFilter('custrecord_rev_rep_data_year', null,
			'equalto', s_year);
	filters[2]	=	new nlobjSearchFilter('custrecord_rev_rep_data_updated_by', null, 'noneof', i_record_id.toString());
	var columns = new Array();
	columns[0] = new nlobjSearchColumn('internalid');
	columns[1] = new nlobjSearchColumn('custrecord_rev_rep_data_employee');
	columns[2] = new nlobjSearchColumn('custrecord_rev_rep_data_customer');
	columns[3] = new nlobjSearchColumn('custrecord_rev_rep_data_project');
	columns[4] = new nlobjSearchColumn('custrecord_rev_rep_data_practice');
	columns[5] = new nlobjSearchColumn('custrecord_rev_rep_data_vertical');
	columns[6] = new nlobjSearchColumn('custrecord_rev_rep_data_total_amount');
columns[7]	=	new nlobjSearchColumn('custrecord_rev_rep_data_updated_by');
	var a_search_results = searchRecord('customrecord_revenue_report_data',
			null, filters, columns);
	var a_data = new Array();
	for (var i = 0; i < a_search_results.length; i++) {
		var o_data = new Object();
		o_data.internal_id = a_search_results[i].getValue('internalid');
		o_data.employee = a_search_results[i]
				.getValue('custrecord_rev_rep_data_employee');
		o_data.customer = a_search_results[i]
				.getValue('custrecord_rev_rep_data_customer');
		o_data.project = a_search_results[i]
				.getValue('custrecord_rev_rep_data_project');
		o_data.practice = a_search_results[i]
				.getValue('custrecord_rev_rep_data_practice');
		o_data.vertical = a_search_results[i]
				.getValue('custrecord_rev_rep_data_vertical');
		o_data.tot_amt = a_search_results[i]
				.getValue('custrecord_rev_rep_data_total_amount');
		o_data.updated_by = a_search_results[i]
				.getValue('custrecord_rev_rep_data_updated_by');
		a_data.push(o_data);
	}
nlapiLogExecution('AUDIT', 'a_data', JSON.stringify(a_data));
	return a_data;
}