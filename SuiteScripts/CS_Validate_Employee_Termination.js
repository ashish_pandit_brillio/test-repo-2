/**
 * Module Description
 * Client script to handle the field change and save record event for employee record
 * for employee termination
 * 
 * Version    Date            Author           Remarks
 * 1.00       30 Jan 2015     Nitish Mishra
 *
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your
 * script deployment.
 * 
 * @appliedtorecord employee
 * 
 * @returns {Boolean} True to continue save, false to abort save
 */
function clientSaveRecord () {

	try {
		var saveStatus = true;

		// employee is getting terminated
		var terminationDate = nlapiGetFieldValue('custentity_lwd'), 
			employeeInactive = nlapiGetFieldValue('custentity_employee_inactive'), 
			giveAccess = nlapiGetFieldValue('giveaccess');
			//isProjectResource = nlapiGetFieldValue('isjobresource');

		if (isNotEmpty(terminationDate) && isFalse(employeeInactive)) {
			alert('Please tick the employee inactive checkbox.');
			saveStatus = false;
		}
		else if (isEmpty(terminationDate) && isTrue(employeeInactive)) {
			alert('Please enter the termination date.');
			saveStatus = false;
		}

		return saveStatus;
	}
	catch (err) {
		alert("Some Error Occured : " + err.message);
		return false;
	}
}

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your
 * script deployment.
 * 
 * @appliedtorecord employee
 */
function clientFieldChanged (type, name, linenum) {

	try {

		if (name == 'custentity_lwd') {
			var terminationDate = nlapiGetFieldValue('custentity_lwd');

			if (isNotEmpty(terminationDate)) {
				nlapiSetFieldValue('custentity_employee_inactive', 'T', false, false);
				nlapiSetFieldValue('giveaccess', 'F', false, false);
				//nlapiSetFieldValue('isjobresource', 'F', false, false);
			}
			else {
				nlapiSetFieldValue('custentity_employee_inactive', 'F', false, false);
				nlapiSetFieldValue('giveaccess', 'T', false, false);
				//nlapiSetFieldValue('isjobresource', 'T', false, false);
			}
		}

		else if (name == 'custentity_employee_inactive') {
			var empInactive = isTrue(nlapiGetFieldValue('custentity_employee_inactive'));

			if (empInactive) {
				nlapiSetFieldValue('custentity_lwd', nlapiDateToString(new Date()), false, false);
				//nlapiSetFieldValue('isjobresource', 'F', false, false);
				nlapiSetFieldValue('giveaccess', 'F', false, false);
			}
			else {
				nlapiSetFieldValue('custentity_lwd', '', false, false);
				//nlapiSetFieldValue('isjobresource', 'T', false, false);
				nlapiSetFieldValue('giveaccess', 'T', false, false);
			}
		}
	}
	catch (err) {
		alert("Some Error Occured : " + err.message);
		nlapiLogExecution('ERROR', 'clientFieldChanged', err);
		return false;
	}
}
