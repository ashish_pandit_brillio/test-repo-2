/**
 * Module Description
 * 
 * Version      Date                          Author                          Remarks  
 * 1.00         17 Oct 2015             Manikandan V         Send 30 days completion mailer to the Employees scheduled at 9 a.m.
 */

function Main(){
try{

var columns = new Array();
columns[0]=new nlobjSearchColumn('firstname');
columns[1]=new nlobjSearchColumn('email');
columns[2]=new nlobjSearchColumn('email','custentity_reportingmanager');
//columns[3]=new nlobjSearchColumn('custrecord_hrbusinesspartner','department');

// search for employee with 30 days of service as of today

var emp_search = nlapiSearchRecord('employee', 'customsearch_30_days_complet_us', null,columns);

nlapiLogExecution('debug', 'emp_search.length',emp_search.length);

if(isNotEmpty(emp_search)){
	
	for(var i=0;i<emp_search.length;i++){
		
		// get the HR BP email id
		//var hr_bp_id = //emp_search[i].getValue('custrecord_hrbusinesspartner','department');
		//var hr_bp_email = null;
		
		//if(isNotEmpty(hr_bp_id)){
			//hr_bp_email = nlapiLookupField('employee',hr_bp_id, 'email');
			//hr_bp_firstname = nlapiLookupField('employee',hr_bp_id, 'firstname');
			//hr_bp_lastname = nlapiLookupField('employee',hr_bp_id, 'lastname');
		//}
		
		sendServiceMails(
			emp_search[i].getValue('firstname'),
			emp_search[i].getValue('email'),
			//hr_bp_email,
			emp_search[i].getId());
	}
}

}
catch(err){
nlapiLogExecution('error', 'Main', err);
}
}

function sendServiceMails(firstName, email, emp_id){
	
	try{
                //var AttachmentID=108692;
                var fileObj = nlapiLoadFile(108692);
		var mailTemplate = serviceTemplate(firstName,emp_id);		
		nlapiLogExecution('debug', 'chekpoint',email);
      var employe=nlapiLoadRecord('employee',emp_id);
		//employe.setFieldValue('var employe=nlapiLoadRecord('employee',emp_id;
		employe.setFieldValue('custentity9','T');
		nlapiSubmitRecord(employe);
		nlapiSendEmail('10730', email, mailTemplate.MailSubject, mailTemplate.MailBody,['preetham.suresh@brillio.com','abhishek.kaashyap@brillio.com'],null,null,null, {entity: emp_id});
	}
	catch(err){
nlapiLogExecution('error', 'sendServiceMails', err);
throw err;
     }
}

function serviceTemplate(firstName,emp_id) {
	nlapiLogExecution('audit','emp_id',emp_id);  
var feedbackFormUrl='https://forms.na1.netsuite.com/app/site/hosting/scriptlet.nl?script=1476&deploy=1&compid=3883006&h=208a9ee81d2fe1f2af1a';
	feedbackFormUrl +='&custparam_employee_id='+emp_id;
	
    var htmltext = '';
    
    htmltext += '<table border="0" width="100%"><tr>';
    htmltext += '<td colspan="4" valign="top">';
    //htmltext += '<p>Hi ' + firstName + ',</p>';  
htmltext += '<p>Dear ' + firstName + ',</p>';
//htmltext += '&nbsp';
 
htmltext += '<p>Congratulations on completion of 30 days with us.</p>';
//htmltext += &nbsp;
htmltext += '<p>Hope you are having a great time and the journey ahead looks exciting to you!.</p>';
htmltext += '<p>Please click on <a href  ="'+feedbackFormUrl+'"  >Turning 30</a> to complete the survey.Request you to please complete the survey and give your valuable feedback to help us improve</p>';

htmltext += '<p>Effective today </b>Abhishek Kaashyap</b> will be your HR Business Partner (HRBP).HRBP will be your single point of contact, please feel free to reach out to your HRBP for any assistance.</p>';
htmltext += '<p>You can reach out to </b>Abhishek Kaashyap</b> at <b>abhishek.kaashyap@brillio.com</b> </p>';
htmltext += '<p>Here’s wishing you a very successful year ahead and all the best for your future endeavors and contributions to Brillio Technologies Pvt. Ltd.</p>';

htmltext += '<p>Thanks & Regards,</p>';
htmltext += '<p>Team HR</p>';
 
 
    htmltext += '</td></tr>';
    htmltext += '</table>';
    htmltext += '<hr width="100%" size="1" noshade color="#CCCCCC">';
    htmltext += '<table width="100%" border="0" cellspacing="0" cellpadding="0">';
    htmltext += '<tr>';
    htmltext += '<td align="right">';
    htmltext += '<font style="font-size:9px; font-family:Verdana,Arial,Helvetica,sans-serif; color:#999999;">Brillio is powered by <a href="http://www.netsuite.com/" style="color:#999999;">NetSuite</a> One System. No Limits.</font>';
    htmltext += '</td>';
    htmltext += '</tr>';
    htmltext += '</table>';
    htmltext += '</body>';
    htmltext += '</html>';
 
    return {
        MailBody : htmltext,
        MailSubject : "Congratulations on completion of 30 days in Brillio!!"      
    };
}

