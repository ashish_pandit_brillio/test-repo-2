/**
 * Module Description
 * Scheduled script to handle employee record creation and updation from the XML file generated from Fusion
 * 
 * Version                      Date                                   Author                           Remarks
 * 1.00                            11th August 2015           Anuradha Sinha           This script runs at 8 a.m. and 8 p.m. respectively to save employee wise XML data into custom records
 *
 */

//Script to store new hire employee record into custom records

function Main(){
try{

//load an XML document from the file cabinet

//var folder = 23903 // the folder ID we want to extract the file from

var filters = new Array();
filters[0] = new nlobjSearchFilter('internalid', null, 'anyof', '29252' );

// get two columns so we can build a drop down list: file name and file internal ID
var columns = new Array();
columns[0] = new nlobjSearchColumn('name', 'file');
columns[1]  = new nlobjSearchColumn('internalid', 'file');
columns[2]  = new nlobjSearchColumn('modified', 'file').setSort(true);
var file_load;
var searchResult = nlapiSearchRecord('folder',null,filters,columns);
if(searchResult) {
        
                 file_load = searchResult[0].getValue('internalid','file');

                  }
nlapiLogExecution('debug', 'searchResult.length',searchResult.length);

var xmlFile = nlapiLoadFile(file_load);
//var xmlDocument = nlapiStringToXML(xmlFile.getValue());
var xmlDocument = xmlFile.getValue();


var xmlDoc=nlapiStringToXML(xmlDocument);
var xmlData=nlapiSelectNodes(xmlDoc, '//EmployeeData');
if(xmlData!=0 && xmlData.length>0)
{
for(var i = 0 ; i < xmlData.length;i++) 
{
var xmlValue=nlapiCreateRecord('customrecord_fi_employee_xml_master',{recordmode: 'dynamic'});


var regex = /<EmployeeData>/gi, result, firstIndices = [];
while ( (result = regex.exec(xmlDocument)) ) {
    firstIndices.push(result.index);
}
var regexs = /<\/EmployeeData>/gi, results, lastIndices = [];
while ( (results = regexs.exec(xmlDocument)) ) {
    lastIndices.push(results.index);
}
var res=xmlDocument.substring(firstIndices[i],lastIndices[i]);

xmlValue.setFieldValue('custrecord_fi_fusion_xml_data',res+'</EmployeeData>');
var id = nlapiSubmitRecord(xmlValue, false, true);
nlapiLogExecution('debug','XML data','Record Created:' +id);
}
}
}
catch(e){
nlapiSetFieldValue('custrecord_fi_error_log', e);
nlapiLogExecution('debug','Error','Record not Created'+e.message);
}
}