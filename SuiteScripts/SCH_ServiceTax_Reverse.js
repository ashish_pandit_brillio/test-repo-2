// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
/*
   	Script Name:SCH Reverse Service Tax
	Author:Nikhil jain
	Company:Aashna cloudtech
	Date:5-1-2015


	Script Modification Log:

	-- Date --			-- Modified By --				--Requested By--				-- Description --
	28 may 2014         Nikhil jain                                                   Remove the hard coded service tax
	17 July 2015         Nikhil                      	  satish D                      Add an Service tax round up method
	17 nov 2015         Nikhil                      	  sachink                      Add an swachh bahart tax 
	26 Feb 2016          Nikhil                         Vaibhav/Kalpana/sachin        add the expense account as Sbc purchase for reverse line 
	 23 May 2016          Nikhil                                                       remove hardcodings of tax types
Below is a summary of the process controls enforced by this script file.  The control logic is described
more fully, below, in the appropriate function headers and code blocks.


     SCHEDULED FUNCTION
		- scheduledFunction(type)


     SUB-FUNCTIONS
		- The following sub-functions are called by the above core functions in order to maintain code
            modularization:

               - NOT USED

*/
}
// END SCRIPT DESCRIPTION BLOCK  ====================================


// BEGIN SCHEDULED FUNCTION =============================================

function schedulerFunction_schreversetax(type){
	/*  On scheduled function:
	 - PURPOSE
	 -
	 FIELDS USED:
	 --Field Name--				--ID--
	 */
	//  LOCAL VARIABLES
	
	
	
	//  SCHEDULED FUNCTION CODE BODY
	
	//== GET GLOBAL SUBSIDIARY PARAMETERS ==
	var a_st_details = new Array();
	var st_details_obj = {};
	var st_serachRes;
	var a_subisidiary = new Array();
	
	var i_AitGlobalRecId = SearchGlobalParameter();
	nlapiLogExecution('DEBUG', 'Invoice ', "i_AitGlobalRecId" + i_AitGlobalRecId);
	
	if (i_AitGlobalRecId != 0) {
		var o_AitGloRec = nlapiLoadRecord('customrecord_indian_tax_glo_parameter', i_AitGlobalRecId);
		
		var a_subisidiary = o_AitGloRec.getFieldValues('custrecord_ait_subsidiary');
		nlapiLogExecution('DEBUG', 'Invoice ', "a_subisidiary->" + a_subisidiary);
		
		var i_vatCode = o_AitGloRec.getFieldValue('custrecord_ait_vat_code');
		nlapiLogExecution('DEBUG', 'Invoice ', "i_vatCode->" + i_vatCode);
		
		var i_taxcode = o_AitGloRec.getFieldValue('custrecord_ait_taxcode');
		nlapiLogExecution('DEBUG', 'Invoice ', "i_taxcode->" + i_taxcode);
		
		var i_servicetaxrate = o_AitGloRec.getFieldValue('custrecord_ait_servicetaxrate');
		
		var i_ecessrate = o_AitGloRec.getFieldValue('custrecord_ait_ecess_rate');
		
		var i_hecessrate = o_AitGloRec.getFieldValue('custrecord_ait_hecess_rate');
		
		var i_swachh_bharat_cess = o_AitGloRec.getFieldValue('custrecord_ait_swachh_cess');
		
		var i_total_service_tax = o_AitGloRec.getFieldValue('custrecord_ait_total_servicetax');
		
		var stRoundMethod = o_AitGloRec.getFieldValue('custrecord_ait_st_roundoff');
		
		var i_Rcm_inputAcc = o_AitGloRec.getFieldValue('custrecord_ait_rcm_st_input_acc');
		
		var i_Rcm_outputAcc = o_AitGloRec.getFieldValue('custrecord_ait_rcm_st_output_acc');
		
		//================================Begin:- Code to Get the Service tax Details=============================//
		
		
		var st_filters = new Array();
		var st_columns = new Array();
		
		st_filters.push(new nlobjSearchFilter('custrecord_ait_service_tax_global_link', null, 'anyOf', i_AitGlobalRecId))
		st_filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'))
		
		st_columns.push(new nlobjSearchColumn('custrecord_ait_service_tax_type'))
		st_columns.push(new nlobjSearchColumn('custrecord_ait_service_tax_rate'))
		st_columns.push(new nlobjSearchColumn('custrecord_ait_service_expense_out'))
		st_columns.push(new nlobjSearchColumn('custrecord_ait_taxdetail_rcm_st_input_ac'))
		st_columns.push(new nlobjSearchColumn('custrecord_ait_taxdetail_rcm_st_out_acc'))
		
		var st_serachRes = nlapiSearchRecord('customrecord_ait_service_tax_details', null, st_filters, st_columns)
		
		if (st_serachRes != null && st_serachRes != '' && st_serachRes != undefined) {
			for (var t = 0; t < st_serachRes.length; t++) {
				var st_taxtype = st_serachRes[t].getValue('custrecord_ait_service_tax_type')
				var st_rate = st_serachRes[t].getValue('custrecord_ait_service_tax_rate')
				var st_expense = st_serachRes[t].getValue('custrecord_ait_service_expense_out')
				var st_inputAcc = st_serachRes[t].getValue('custrecord_ait_taxdetail_rcm_st_input_ac')
				var st_outputAcc = st_serachRes[t].getValue('custrecord_ait_taxdetail_rcm_st_out_acc')
				st_details_obj[st_taxtype] = st_rate + '@' + st_expense + '@' + st_inputAcc + '@' + st_outputAcc
				
				a_st_details.push(st_details_obj);
			}
		}
		
		//===============================End:- Code to Get the Service tax Details=============================//
	
	
	}// end global record id
	var i_recordid = nlapiGetContext().getSetting('SCRIPT', 'custscript_strecordid')
	
	var executionCTX = nlapiGetContext().getSetting('SCRIPT', 'custscript_executionctx')
	
	if (i_recordid != null && i_recordid != '' && i_recordid != undefined) {
		var o_billrecord_obj = nlapiLoadRecord('vendorbill', i_recordid)
		
		if (o_billrecord_obj != null && o_billrecord_obj != '' && o_billrecord_obj != undefined) {
		
		
			// === GET THE LINE COUNT =====
			var exLinecount = o_billrecord_obj.getLineItemCount('expense')
			
			var vid = o_billrecord_obj.getFieldValue('entity');
			
			var vendor_type = nlapiLookupField('vendor', vid, 'isperson')
			//=== ITERATE THROUGHT EVERY EXPENSE LINE ===
			for (var l = 1; l <= exLinecount; l++) {
				var isReverseapply = o_billrecord_obj.getLineItemValue('expense', 'custcol_st_reverseapply', l)
				
				var Isreciviabl = o_billrecord_obj.getLineItemValue('expense', 'custcol_reciviableapply', l)
				
				//==== UPDATE THE BILL RECORD ====
				if (isReverseapply == 'T') {
					var paccouttobeset = o_billrecord_obj.getLineItemValue('expense', 'account', l) //if in case tax rate is 0.00
					if (executionCTX == 'csvimport' || executionCTX == 'webservices') {
					
						/*
						 if (vendor_type == 'F')
						 {
						 throw "You cannot apply the  Reverse Service tax to this vendor, The vendor is of type Company";
						 }
						 */
						var csv_taxrate = o_billrecord_obj.getLineItemValue('expense', 'taxrate1', l);
						
						var st_per = (parseFloat(csv_taxrate) / parseFloat(i_total_service_tax)) * 100;
						
						var stReverseper = parseInt(100) - parseInt(st_per);
						nlapiLogExecution('DEBUG', 'Bill After submit', '***********stReverseper->' + stReverseper)
						
						var streverse_per_id = get_STReversePerID(stReverseper);
						
						o_billrecord_obj.setLineItemValue('expense', 'custcol_st_recipientper', l, streverse_per_id);
						
						nlapiLogExecution('DEBUG', 'Bill After submit', 'After commit');
					}
					else {
						var stReverseper = o_billrecord_obj.getLineItemText('expense', 'custcol_st_recipientper', l)
						nlapiLogExecution('DEBUG', 'Bill After submit', '***********stReverseper->' + stReverseper)
					}
					var amount = o_billrecord_obj.getLineItemValue('expense', 'amount', l)
					
					if (parseFloat(stReverseper) > 0) {
						var taxcode = o_billrecord_obj.getLineItemValue('expense', 'taxcode', l)
						
						var location1 = o_billrecord_obj.getLineItemValue('expense', 'location', l);
						nlapiLogExecution('DEBUG', 'Bill After submit', 'location1->' + location1)
						var department1 = o_billrecord_obj.getLineItemValue('expense', 'department', l);
						nlapiLogExecution('DEBUG', 'Bill After submit', 'department1->' + department1)
						var class1 = o_billrecord_obj.getLineItemValue('expense', 'class', l);
						nlapiLogExecution('DEBUG', 'Bill After submit', 'class1->' + class1)
						
						//=======================Code to add swacchh cess expense account=====================//
						
						var sbc_expense_acc = o_billrecord_obj.getLineItemValue('expense', 'account', l)
						
						var taxAccounts = getTaxaccount(taxcode)
						
						for (var i = 0; i < taxAccounts.length; i = 5 + parseInt(i)) {
						
							nlapiLogExecution('DEBUG', 'Bill After submit', "taxAccounts->" + taxAccounts[i])
							nlapiLogExecution('DEBUG', 'Bill After submit', "tax Sales Accounts->" + taxAccounts[i + 1])
							nlapiLogExecution('DEBUG', 'Bill After submit', "taxRates->" + taxAccounts[i + 2])
							nlapiLogExecution('DEBUG', 'Bill After submit', "taxType->" + taxAccounts[i + 3])
							
							if (Isreciviabl == 'T') {
								var purchaseAccounts = paccouttobeset;
								nlapiLogExecution('DEBUG', 'Bill After submit', "Newly setfor checking account->" + purchaseAccounts)
								
							} // END if(Isreciviabl == 'T')       
							else {
								var purchaseAccounts = taxAccounts[i] //comment for testing
							} //end ELSE if for setting purchase account
							var salesAccount = taxAccounts[i + 1]
							var taxRate = taxAccounts[i + 2]
							var taxType = taxAccounts[i + 3]
							var taxID = taxAccounts[i + 4]
							
							//Setting for Tax rate if tax rate is 0.00%- Changed on 29-03-2013
							if (Isreciviabl == 'T') {
								if (parseFloat(taxRate) == parseFloat(0)) {
								
									/*
									 if (taxType == 'Service Tax' || taxType == 'ST' || taxType == 'India - Service Tax') {
									 var calTaxrate = parseFloat(i_servicetaxrate) - ((parseFloat(i_servicetaxrate) * (100 - parseInt(stReverseper))) / 100)
									 }
									 else
									 if (taxType == 'E Cess') {
									 var calTaxrate = parseFloat(i_ecessrate) - ((parseFloat(i_ecessrate) * (100 - parseInt(stReverseper))) / 100)
									 }
									 else
									 if (taxType == 'HE Cess' || taxType == 'H Cess') {
									 var calTaxrate = parseFloat(i_hecessrate) - ((parseFloat(i_hecessrate) * (100 - parseInt(stReverseper))) / 100)
									 }
									 else
									 if (taxType == 'Swachh Cess' || taxType == 'Swachh Bharat Cess') {
									 var calTaxrate = parseFloat(i_swachh_bharat_cess) - ((parseFloat(i_swachh_bharat_cess) * (100 - parseInt(stReverseper))) / 100)
									 }
									 */
									if (taxType in st_details_obj) {
										st_value = st_details_obj[taxType]
										nlapiLogExecution('DEBUG', 'Bill After submit', "taxID->" + st_value)
										
										var i_st_taxrate = st_value.split('@')[0]
										nlapiLogExecution('DEBUG', 'Bill After submit', "i_st_taxrate->" + i_st_taxrate)
										
										var st_expense = st_value.split('@')[1]
										nlapiLogExecution('DEBUG', 'Bill After submit', "st_expense->" + st_expense)
										
										var st_inputAcc = st_value.split('@')[2]
										nlapiLogExecution('DEBUG', 'Bill After submit', "st_inputAcc->" + st_inputAcc)
										
										var st_outputAcc = st_value.split('@')[3]
										nlapiLogExecution('DEBUG', 'Bill After submit', "st_outputAcc->" + st_outputAcc)
										
										var calTaxrate = parseFloat(i_st_taxrate) - parseFloat(taxRate)
									}
									
									
								}// end if
								else {
								
									/*
									 if (taxType == 'Service Tax' || taxType == 'ST' || taxType == 'India - Service Tax') {
									 var calTaxrate = parseFloat(i_servicetaxrate) - parseFloat(taxRate)
									 }
									 else
									 if (taxType == 'E Cess') {
									 var calTaxrate = parseFloat(i_ecessrate) - parseFloat(taxRate)
									 }
									 else
									 if (taxType == 'HE Cess' || taxType == 'H Cess') {
									 var calTaxrate = parseFloat(i_hecessrate) - parseFloat(taxRate)
									 }
									 else
									 if (taxType == 'Swachh Cess' || taxType == 'Swachh Bharat Cess') {
									 var calTaxrate = parseFloat(i_swachh_bharat_cess) - parseFloat(taxRate)
									 }
									 
									 */
									if (taxType in st_details_obj) {
										st_value = st_details_obj[taxType]
										nlapiLogExecution('DEBUG', 'Bill After submit', "taxID->" + st_value)
										
										var i_st_taxrate = st_value.split('@')[0]
										nlapiLogExecution('DEBUG', 'Bill After submit', "i_st_taxrate->" + i_st_taxrate)
										
										var st_expense = st_value.split('@')[1]
										nlapiLogExecution('DEBUG', 'Bill After submit', "st_expense->" + st_expense)
										
										var st_inputAcc = st_value.split('@')[2]
										nlapiLogExecution('DEBUG', 'Bill After submit', "st_inputAcc->" + st_inputAcc)
										
										var st_outputAcc = st_value.split('@')[3]
										nlapiLogExecution('DEBUG', 'Bill After submit', "st_outputAcc->" + st_outputAcc)
										
										var calTaxrate = parseFloat(i_st_taxrate) - parseFloat(taxRate)
									}
								} //end else- for txarate==0.00
							} // END if (Isreciviabl == 'T') 	
							else {
							
								/*
								 if (taxType == 'Service Tax' || taxType == 'ST' || taxType == 'India - Service Tax') {
								 var calTaxrate = parseFloat(i_servicetaxrate) - parseFloat(taxRate)
								 }
								 else
								 if (taxType == 'E Cess') {
								 var calTaxrate = parseFloat(i_ecessrate) - parseFloat(taxRate)
								 }
								 else
								 if (taxType == 'HE Cess' || taxType == 'H Cess') {
								 var calTaxrate = parseFloat(i_hecessrate) - parseFloat(taxRate)
								 }
								 else
								 if (taxType == 'Swachh Cess' || taxType == 'Swachh Bharat Cess') {
								 var calTaxrate = parseFloat(i_swachh_bharat_cess) - parseFloat(taxRate)
								 }
								 */
								if (taxType in st_details_obj) {
									st_value = st_details_obj[taxType]
									nlapiLogExecution('DEBUG', 'Bill After submit', "taxID->" + st_value)
									
									var i_st_taxrate = st_value.split('@')[0]
									nlapiLogExecution('DEBUG', 'Bill After submit', "i_st_taxrate->" + i_st_taxrate)
									
									var st_expense = st_value.split('@')[1]
									nlapiLogExecution('DEBUG', 'Bill After submit', "st_expense->" + st_expense)
									
									var st_inputAcc = st_value.split('@')[2]
									nlapiLogExecution('DEBUG', 'Bill After submit', "st_inputAcc->" + st_inputAcc)
									
									var st_outputAcc = st_value.split('@')[3]
									nlapiLogExecution('DEBUG', 'Bill After submit', "st_outputAcc->" + st_outputAcc)
									
									var calTaxrate = parseFloat(i_st_taxrate) - parseFloat(taxRate)
								}
							} //End -else for IS reciviable
							//End Setting for Tax rate if tax rate is 0.00%- Changed on 29-03-2013
							
							var taxAmount = parseFloat(amount) * (parseFloat(calTaxrate) / 100)
							taxAmount = applySTRoundMethod(stRoundMethod, taxAmount)
							o_billrecord_obj.selectNewLineItem('expense');
							
							/*
							 if (taxType == 'Swachh Cess' || taxType == 'Swachh Bharat Cess')
							 {
							 o_billrecord_obj.setCurrentLineItemValue('expense', 'account', sbc_expense_acc);
							 }
							 if (taxType == 'Service Tax' || taxType == 'ST' || taxType == 'India - Service Tax')
							 {
							 if (i_Rcm_inputAcc != null && i_Rcm_inputAcc != '' && i_Rcm_inputAcc != undefined) {
							 o_billrecord_obj.setCurrentLineItemValue('expense', 'account', i_Rcm_inputAcc);
							 }
							 else
							 {
							 o_billrecord_obj.setCurrentLineItemValue('expense', 'account', purchaseAccounts);
							 }
							 o_billrecord_obj.setCurrentLineItemValue('expense', 'custcol_st_code', taxID);
							 }
							 */
							if (st_expense == 'T') {
								o_billrecord_obj.setCurrentLineItemValue('expense', 'account', sbc_expense_acc);
							}
							else {
								if (st_inputAcc != null && st_inputAcc != '' && st_inputAcc != undefined) {
									o_billrecord_obj.setCurrentLineItemValue('expense', 'account', st_inputAcc);
								}
								else {
									o_billrecord_obj.setCurrentLineItemValue('expense', 'account', purchaseAccounts);
								}
								o_billrecord_obj.setCurrentLineItemValue('expense', 'custcol_st_code', taxID);
							}
							taxAmount = Math.round(taxAmount)
							o_billrecord_obj.setCurrentLineItemValue('expense', 'amount', taxAmount);
							o_billrecord_obj.setCurrentLineItemValue('expense', 'taxcode', i_taxcode);
							o_billrecord_obj.setCurrentLineItemValue('expense', 'custcol_st_reverseline', 'T');
							
							o_billrecord_obj.setCurrentLineItemValue('expense', 'location', location1);
							o_billrecord_obj.setCurrentLineItemValue('expense', 'department', department1);
							o_billrecord_obj.setCurrentLineItemValue('expense', 'class', class1);
							o_billrecord_obj.commitLineItem('expense');
							
							o_billrecord_obj.selectNewLineItem('expense');
							/*
							 if (taxType == 'Service Tax' || taxType == 'ST' || taxType == 'India - Service Tax')
							 {
							 if (st_inputAcc != null && st_inputAcc != '' && st_inputAcc != undefined) {
							 o_billrecord_obj.setCurrentLineItemValue('expense', 'account', st_inputAcc);
							 }
							 else
							 {
							 o_billrecord_obj.setCurrentLineItemValue('expense', 'account', salesAccount);
							 }
							 
							 }
							 else
							 {
							 o_billrecord_obj.setCurrentLineItemValue('expense', 'account', salesAccount);
							 }
							 */
							/*
							 if (st_expense == 'T') {
							 o_billrecord_obj.setCurrentLineItemValue('expense', 'account', salesAccount);
							 }
							 else
							 */
							{
								if (st_outputAcc != null && st_outputAcc != '' && st_outputAcc != undefined) {
									o_billrecord_obj.setCurrentLineItemValue('expense', 'account', st_outputAcc);
								}
								else {
									o_billrecord_obj.setCurrentLineItemValue('expense', 'account', salesAccount);
								}
							}
							//o_billrecord_obj.setCurrentLineItemValue('expense', 'account', salesAccount);
							taxAmount = Math.round(taxAmount)
							o_billrecord_obj.setCurrentLineItemValue('expense', 'amount', -taxAmount);
							o_billrecord_obj.setCurrentLineItemValue('expense', 'taxcode', i_taxcode);
							o_billrecord_obj.setCurrentLineItemValue('expense', 'custcol_st_reverseline', 'T');
							
							o_billrecord_obj.setCurrentLineItemValue('expense', 'location', location1);
							o_billrecord_obj.setCurrentLineItemValue('expense', 'department', department1);
							o_billrecord_obj.setCurrentLineItemValue('expense', 'class', class1);
							o_billrecord_obj.setCurrentLineItemValue('expense', 'custcol_st_code', taxID);
							o_billrecord_obj.commitLineItem('expense');
							
						}// END for(i=0;i<taxAccounts.length;i=4+parseInt(i))
					} // END if(parseFloat(stReverseper)>0)
				} // END if(isReverseapply=='T')
			} // END for(var i=1;i<=exLinecount;i++)
			//-----------------------------------------------Begin:code for Item--------------------------------------------------------------------------
			// === GET THE LINE COUNT =====
			var exLinecount = o_billrecord_obj.getLineItemCount('item')
			nlapiLogExecution('DEBUG', 'Bill After submit', "Item Line Count->" + exLinecount)
			//=== ITERATE THROUGHT EVERY EXPENSE LINE ===
			for (var j = 1; j <= exLinecount; j++) {
				var isReverseapply = o_billrecord_obj.getLineItemValue('item', 'custcol_st_reverseapply', j)
				
				var Isreciviabl = o_billrecord_obj.getLineItemValue('item', 'custcol_reciviableapply', j)
				
				//==== UPDATE THE BILL RECORD ====
				if (isReverseapply == 'T') {
					var paccouttobeset = ''
					// var paccouttobeset =  o_billrecord_obj.getLineItemValue('expense', 'account', i) //if in case tax rate is 0.00
					
					if (executionCTX == 'csvimport' || executionCTX == 'webservices') {
					
						/*
						 if (vendor_type == 'F')
						 {
						 throw "You cannot apply the  Reverse Service tax to this vendor, The vendor is of type Company";
						 }
						 */
						var csv_taxrate = o_billrecord_obj.getLineItemValue('item', 'taxrate1', j);
						
						var st_per = (parseFloat(csv_taxrate) / parseFloat(i_total_service_tax)) * 100;
						
						var stReverseper = parseInt(100) - parseInt(st_per);
						
						var streverse_per_id = get_STReversePerID(stReverseper);
						o_billrecord_obj.setLineItemValue('item', 'custcol_st_recipientper', j, streverse_per_id);
						
						
					}
					else {
						var stReverseper = o_billrecord_obj.getLineItemText('item', 'custcol_st_recipientper', j)
						nlapiLogExecution('DEBUG', 'Bill After submit', '***********stReverseper->' + stReverseper)
					}
					
					var amount = o_billrecord_obj.getLineItemValue('item', 'amount', j)
					
					if (parseFloat(stReverseper) > 0) {
						var taxcode = o_billrecord_obj.getLineItemValue('item', 'taxcode', j)
						
						var location1 = o_billrecord_obj.getLineItemValue('item', 'location', j);
						nlapiLogExecution('DEBUG', 'Bill After submit', 'location1->' + location1)
						var department1 = o_billrecord_obj.getLineItemValue('item', 'department', j);
						nlapiLogExecution('DEBUG', 'Bill After submit', 'department1->' + department1)
						var class1 = o_billrecord_obj.getLineItemValue('item', 'class', j);
						nlapiLogExecution('DEBUG', 'Bill After submit', 'class1->' + class1)
						
						//================================Begin code to add SBC Expense account====================//
						
						var sbc_expense_acc = o_billrecord_obj.getLineItemValue('item', 'custcol_ait_expense_acc', j);
						
						var taxAccounts = getTaxaccount(taxcode)
						
						for (var i = 0; i < taxAccounts.length; i = 5 + parseInt(i)) {
						
							nlapiLogExecution('DEBUG', 'Bill After submit', "taxAccounts->" + taxAccounts[i])
							nlapiLogExecution('DEBUG', 'Bill After submit', "tax Sales Accounts->" + taxAccounts[i + 1])
							nlapiLogExecution('DEBUG', 'Bill After submit', "taxRates->" + taxAccounts[i + 2])
							nlapiLogExecution('DEBUG', 'Bill After submit', "taxType->" + taxAccounts[i + 3])
							
							if (Isreciviabl == 'T') {
								var purchaseAccounts = paccouttobeset;
								nlapiLogExecution('DEBUG', 'Bill After submit', "Newly setfor checking account->" + purchaseAccounts)
								
							} // END if(Isreciviabl == 'T')
							else {
								var purchaseAccounts = taxAccounts[i] //comment for testing
							} //end ELSE if for setting purchase account
							var salesAccount = taxAccounts[i + 1]
							var taxRate = taxAccounts[i + 2]
							var taxType = taxAccounts[i + 3]
							var taxID = taxAccounts[i + 4]
							
							//Setting for Tax rate if tax rate is 0.00%- Changed on 29-03-2013
							if (Isreciviabl == 'T') {
								if (parseFloat(taxRate) == parseFloat(0)) {
								
									/*
									 if (taxType == 'Service Tax' || taxType == 'ST' || taxType == 'India - Service Tax') {
									 var calTaxrate = parseFloat(i_servicetaxrate) - ((parseFloat(i_servicetaxrate) * (100 - parseInt(stReverseper))) / 100)
									 }
									 else
									 if (taxType == 'E Cess') {
									 var calTaxrate = parseFloat(i_ecessrate) - ((parseFloat(i_ecessrate) * (100 - parseInt(stReverseper))) / 100)
									 }
									 else
									 if (taxType == 'HE Cess' || taxType == 'H Cess') {
									 var calTaxrate = parseFloat(i_hecessrate) - ((parseFloat(i_hecessrate) * (100 - parseInt(stReverseper))) / 100)
									 }
									 else
									 if (taxType == 'Swachh Cess' || taxType == 'Swachh Bharat Cess') {
									 var calTaxrate = parseFloat(i_swachh_bharat_cess) - ((parseFloat(i_swachh_bharat_cess) * (100 - parseInt(stReverseper))) / 100)
									 }
									 */
									if (taxType in st_details_obj) {
										st_value = st_details_obj[taxType]
										nlapiLogExecution('DEBUG', 'Bill After submit', "taxID->" + st_value)
										
										var i_st_taxrate = st_value.split('@')[0]
										nlapiLogExecution('DEBUG', 'Bill After submit', "i_st_taxrate->" + i_st_taxrate)
										
										var st_expense = st_value.split('@')[1]
										nlapiLogExecution('DEBUG', 'Bill After submit', "st_expense->" + st_expense)
										
										var st_inputAcc = st_value.split('@')[2]
										nlapiLogExecution('DEBUG', 'Bill After submit', "st_inputAcc->" + st_inputAcc)
										
										var st_outputAcc = st_value.split('@')[3]
										nlapiLogExecution('DEBUG', 'Bill After submit', "st_outputAcc->" + st_outputAcc)
										
										var calTaxrate = parseFloat(i_st_taxrate) - ((parseFloat(i_st_taxrate) * (100 - parseInt(stReverseper))) / 100)
									}
									
								}// end if
								else {
								
									/*
									 if (taxType == 'Service Tax' || taxType == 'ST') {
									 var calTaxrate = parseFloat(i_servicetaxrate) - parseFloat(taxRate)
									 }
									 else
									 if (taxType == 'E Cess') {
									 var calTaxrate = parseFloat(i_ecessrate) - parseFloat(taxRate)
									 }
									 else
									 if (taxType == 'HE Cess' || taxType == 'H Cess') {
									 var calTaxrate = parseFloat(i_hecessrate) - parseFloat(taxRate)
									 }
									 else
									 if (taxType == 'Swachh Cess' || taxType == 'Swachh Bharat Cess') {
									 var calTaxrate = parseFloat(i_swachh_bharat_cess) - parseFloat(taxRate)
									 }
									 */
									if (taxType in st_details_obj) {
										st_value = st_details_obj[taxType]
										nlapiLogExecution('DEBUG', 'Bill After submit', "taxID->" + st_value)
										
										var i_st_taxrate = st_value.split('@')[0]
										nlapiLogExecution('DEBUG', 'Bill After submit', "i_st_taxrate->" + i_st_taxrate)
										
										var st_expense = st_value.split('@')[1]
										nlapiLogExecution('DEBUG', 'Bill After submit', "st_expense->" + st_expense)
										
										var st_inputAcc = st_value.split('@')[2]
										nlapiLogExecution('DEBUG', 'Bill After submit', "st_inputAcc->" + st_inputAcc)
										
										var st_outputAcc = st_value.split('@')[3]
										nlapiLogExecution('DEBUG', 'Bill After submit', "st_outputAcc->" + st_outputAcc)
										
										var calTaxrate = parseFloat(i_st_taxrate) - parseFloat(taxRate)
									}
									
								} //end else- for txarate==0.00
							} // END if (Isreciviabl == 'T')
							else {
							
								/*
								 if (taxType == 'Service Tax' || taxType == 'ST') {
								 var calTaxrate = parseFloat(i_servicetaxrate) - parseFloat(taxRate)
								 }
								 else
								 if (taxType == 'E Cess') {
								 var calTaxrate = parseFloat(i_ecessrate) - parseFloat(taxRate)
								 }
								 else
								 if (taxType == 'HE Cess' || taxType == 'H Cess') {
								 var calTaxrate = parseFloat(i_hecessrate) - parseFloat(taxRate)
								 }
								 else
								 if (taxType == 'Swachh Cess' || taxType == 'Swachh Bharat Cess') {
								 var calTaxrate = parseFloat(i_swachh_bharat_cess) - parseFloat(taxRate)
								 }
								 */
								if (taxType in st_details_obj) {
									st_value = st_details_obj[taxType]
									nlapiLogExecution('DEBUG', 'Bill After submit', "taxID->" + st_value)
									
									var i_st_taxrate = st_value.split('@')[0]
									nlapiLogExecution('DEBUG', 'Bill After submit', "i_st_taxrate->" + i_st_taxrate)
									
									var st_expense = st_value.split('@')[1]
									nlapiLogExecution('DEBUG', 'Bill After submit', "st_expense->" + st_expense)
									
									var st_inputAcc = st_value.split('@')[2]
									nlapiLogExecution('DEBUG', 'Bill After submit', "st_inputAcc->" + st_inputAcc)
									
									var st_outputAcc = st_value.split('@')[3]
									nlapiLogExecution('DEBUG', 'Bill After submit', "st_outputAcc->" + st_outputAcc)
									
									var calTaxrate = parseFloat(i_st_taxrate) - parseFloat(taxRate)
								}
							}//End -else for IS reciviable
							//End Setting for Tax rate if tax rate is 0.00%- Changed on 29-03-2013
							
							var taxAmount = parseFloat(amount) * (parseFloat(calTaxrate) / 100)
							taxAmount = applySTRoundMethod(stRoundMethod, taxAmount)
							o_billrecord_obj.selectNewLineItem('expense');
							
							if (st_expense == 'T') {
								o_billrecord_obj.setCurrentLineItemValue('expense', 'account', sbc_expense_acc);
							}
							else {
								if (st_inputAcc != null && st_inputAcc != '' && st_inputAcc != undefined) {
									o_billrecord_obj.setCurrentLineItemValue('expense', 'account', st_inputAcc);
								}
								else {
									o_billrecord_obj.setCurrentLineItemValue('expense', 'account', purchaseAccounts);
								}
								o_billrecord_obj.setCurrentLineItemValue('expense', 'custcol_st_code', taxID);
							}
							//o_billrecord_obj.setCurrentLineItemValue('expense', 'account', purchaseAccounts);
							o_billrecord_obj.setCurrentLineItemValue('expense', 'amount', taxAmount);
							o_billrecord_obj.setCurrentLineItemValue('expense', 'taxcode', i_taxcode);
							o_billrecord_obj.setCurrentLineItemValue('expense', 'custcol_st_reverseline', 'T');
							
							o_billrecord_obj.setCurrentLineItemValue('expense', 'location', location1);
							o_billrecord_obj.setCurrentLineItemValue('expense', 'department', department1);
							o_billrecord_obj.setCurrentLineItemValue('expense', 'class', class1);
							o_billrecord_obj.commitLineItem('expense');
							
							o_billrecord_obj.selectNewLineItem('expense');
							/*
							 if (st_expense == 'T') {
							 o_billrecord_obj.setCurrentLineItemValue('expense', 'account', salesAccount);
							 }
							 else
							 */
							{
								if (st_outputAcc != null && st_outputAcc != '' && st_outputAcc != undefined) {
									o_billrecord_obj.setCurrentLineItemValue('expense', 'account', st_outputAcc);
								}
								else {
									o_billrecord_obj.setCurrentLineItemValue('expense', 'account', salesAccount);
								}
							}
							//o_billrecord_obj.setCurrentLineItemValue('expense', 'account', salesAccount);
							o_billrecord_obj.setCurrentLineItemValue('expense', 'amount', -taxAmount);
							o_billrecord_obj.setCurrentLineItemValue('expense', 'taxcode', i_taxcode);
							o_billrecord_obj.setCurrentLineItemValue('expense', 'custcol_st_reverseline', 'T');
							
							o_billrecord_obj.setCurrentLineItemValue('expense', 'location', location1);
							o_billrecord_obj.setCurrentLineItemValue('expense', 'department', department1);
							o_billrecord_obj.setCurrentLineItemValue('expense', 'class', class1);
							o_billrecord_obj.setCurrentLineItemValue('expense', 'custcol_st_code', taxID);
							o_billrecord_obj.commitLineItem('expense');
							
						}// END for(i=0;i<taxAccounts.length;i=4+parseInt(i))
					} // END if(parseFloat(stReverseper)>0)
				} // END if(isReverseapply=='T')
			} // END for(var i=1;i<=exLinecount;i++)
			//-----------------------------------------------End: code for Item--------------------------------------------------------------------------
			
			
			var updated_bill = nlapiSubmitRecord(o_billrecord_obj)
			nlapiLogExecution('DEBUG', 'Add revese service tax', "bill record id->" + updated_bill)
			
		}// end if (a_subisidiary[y] == i_subsidiary) 	
	}
}

// END SCHEDULED FUNCTION ===============================================





// BEGIN FUNCTION ===================================================
{
 function getTaxaccount(taxcode)	
	{
        var taxAccountarray = new Array()
        
        var taxgrpobj = nlapiLoadRecord('taxgroup', taxcode) //Load Tax group
        var taxgrplineitemcount = taxgrpobj.getLineItemCount('taxitem'); //Get line item on tax group
        nlapiLogExecution('DEBUG', 'getTaxaccount', "taxgrplineitemcount" + taxgrplineitemcount)
        var p = 0
        
        for (var j = 1; j <= taxgrplineitemcount; j++) 	
		{
            var taxname = taxgrpobj.getLineItemValue('taxitem', 'taxname', j) //Get Tax name
            nlapiLogExecution('DEBUG', 'getTaxaccount ', "taxname->" + taxname)
            
            if (taxname != null) 
			{
                var codeobj = nlapiLoadRecord('salestaxitem', taxname) //Get tax code object
                var accountcode = codeobj.getFieldValue('purchaseaccount') //Get account code
                nlapiLogExecution('DEBUG', 'getTaxaccount', "accountcode->" + accountcode)
                var salesaccount = codeobj.getFieldValue('saleaccount') //Get account code
                nlapiLogExecution('DEBUG', 'getTaxaccount', "salesaccount->" + salesaccount)
                var rate = codeobj.getFieldValue('rate') //Get account code
                nlapiLogExecution('DEBUG', 'getTaxaccount', "rate->" + rate)
                var taxtype = codeobj.getFieldValue('taxtype') //Get account code
                nlapiLogExecution('DEBUG', 'getTaxaccount', "taxtype->" + taxtype)
                taxAccountarray[p++] = accountcode
                taxAccountarray[p++] = salesaccount
                taxAccountarray[p++] = rate
                taxAccountarray[p++] = taxtype
                taxAccountarray[p++] = taxname
            } // END if (taxname != null)
        } // END for (var j = 1; j <= taxgrplineitemcount; j++)
        return taxAccountarray
    }// END getTaxaccount(taxcode)
	
    //FUNCTION FOR ROUDING AMOUNT
    function RoundUPServiceTAX(n)	
	{
        var newnumb = parseFloat(n).toString().split('.')
        
        ans = n * 1000
        ans = Math.round(ans / 10) + ""
        while (ans.length < 2) 	
		{
            ans = "0" + ans
        }
        
        len = ans.length
        ans = ans.substring(0, len - 2);
        
        
        
        if (newnumb[1] != '' || newnumb[1] != null || newnumb[1] != 'undefined') {
            if (newnumb[1] > 00) 	
			{
                ans = parseInt(newnumb[0]) + 1
                nlapiLogExecution('DEBUG', 'RoundUPServiceTAX', "Rounded up amount" + ans)
                return ans
            }
            else 	
			{
                return n
            }
            
        } // END if (newnumb[1] != '' || newnumb[1] != null || newnumb[1] != 'undefined')
        
    }// END  RoundUPServiceTAX(n)
	
    // FUNCTION FOR GETTING GLOBAL SUBSIDIARY PARAMETER 
 function SearchGlobalParameter()	
	{
    
        var a_filters = new Array();
        var a_column = new Array();
        var i_globalRecId = 0;
        
        a_column.push(new nlobjSearchColumn('internalid'));
        
        var s_serchResult = nlapiSearchRecord('customrecord_indian_tax_glo_parameter', null, null, a_column)
        
        if (s_serchResult != null && s_serchResult != undefined && s_serchResult != '') 	
		{
            for (var i = 0; i < s_serchResult.length; i++)	
			{
                i_globalRecId = s_serchResult[0].getValue('internalid');
                nlapiLogExecution('DEBUG', 'Bill ', "i_globalRecId" + i_globalRecId);
                
            }
            
        }
        
        
        return i_globalRecId;
    }
    

}
function get_STReversePerID(stReverseper)
{
	var st_filters = new Array();
	var st_column = new Array();
	var stper_internalID;
	
	st_column.push(new nlobjSearchColumn('internalid'));
	st_column.push(new nlobjSearchColumn('name'));
	
	var strecp_serchResult = nlapiSearchRecord('customlist_st_recipientper', null, null, st_column)
	
	if (strecp_serchResult != null && strecp_serchResult != undefined && strecp_serchResult != '') 
	{
		for (var k = 0; k < strecp_serchResult.length; k++) 
		{
			nlapiLogExecution('DEBUG', 'Bill ', "result length" + strecp_serchResult.length);
			var stper_name = strecp_serchResult[k].getValue('name');
			nlapiLogExecution('DEBUG', 'Bill ', "stper_name" + stper_name + " count -->"+k +"reverse --->"+ stReverseper);
			if (stReverseper == stper_name) 
			{
				var stper_internalID = strecp_serchResult[k].getValue('internalid');
				nlapiLogExecution('DEBUG', 'Bill ', "stper_internalID" + stper_internalID );
				
				break;
				
			}
			
		}
		
	}
	return stper_internalID;
}
function applySTRoundMethod(stRoundMethod,taxamount)
{
	var roundedSTAmount = taxamount;
	
	if(stRoundMethod == 2)
	{
		roundedSTAmount = Math.round(taxamount)
	}
	if(stRoundMethod == 3)
	{
		roundedSTAmount = Math.round(taxamount/10)*10;
	}
	if(stRoundMethod == 4)
	{
		roundedSTAmount = Math.round(taxamount/100)*100;
	}
	
	return roundedSTAmount;
}
// END FUNCTION =====================================================
