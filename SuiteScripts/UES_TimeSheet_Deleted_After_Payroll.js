/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       21 Apr 2016     amol.sahijwani
 *
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType
 * 
 * @param {String} type Operation types: create, edit, delete, xedit
 *                      approve, reject, cancel (SO, ER, Time Bill, PO & RMA only)
 *                      pack, ship (IF)
 *                      markcomplete (Call, Task)
 *                      reassign (Case)
 *                      editforecast (Opp, Estimate)
 * @returns {Void}
 */
function userEventBeforeSubmit(type){
	// Array of Subrecord names
	var a_days = ['sunday', 'monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday'];
	
	// Get Start Date
	var d_start_date	=	new Date(recTS.getFieldValue('startdate'));
			
	// Get End Date
	var d_end_date		=	new Date(recTS.getFieldValue('enddate'));
	
	// Get Line Count
	var i_line_count = recTS.getLineItemCount('timegrid');//recTS.getLineItemCount('timegrid');
	
	// Loop through Line Items
	for(var i_line_indx = 1; i_line_indx <= i_line_count; i_line_indx++)
		{
			
			//nlapiLogExecution('AUDIT', 'Test', JSON.stringify(a_days_data));		
						
			recTS.selectLineItem('timegrid',i_line_indx);
		
			for(var i_day_indx = 0; i_day_indx < 7; i_day_indx++)
			{
				var sub_record_name = a_days[i_day_indx];
				var o_sub_record_view = recTS.viewCurrentLineItemSubrecord('timegrid', sub_record_name);
				if(o_sub_record_view)
					{
						var d_current_date = nlapiStringToDate(o_sub_record_view.getFieldValue('day'), 'date');
						var i_project_id = o_sub_record_view.getFieldValue('customer');
						var isCurrentlyBillable = o_sub_record_view.getFieldValue('isbillable');
						var currentRate = o_sub_record_view.getFieldValue('rate');
						var i_item_id = o_sub_record_view.getFieldValue('item');
					}
			}
		}
}
