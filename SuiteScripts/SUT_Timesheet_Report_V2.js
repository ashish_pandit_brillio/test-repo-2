function suitelet(request, response) {
	try {
		var request_project_id = request.getParameter('custpage_project');
		var request_to_date = request.getParameter('custpage_to_date');
		var request_from_date = request.getParameter('custpage_from_date');

		// check the employee access level
		var user = nlapiGetUser();

		if (user == 41571 || user == 62082) {
			user = 1646;
		}
			if(user == 37827){
			user = 1720;
			}
      
      if(user == 200580)
        {
          user=3165;
        }
      

		// get all projects he is a PM / DM
		var projectList = getRelatedProjects(user);

		if (!projectList) {
			throw "You are not authorised to access this page. Please contact your account administrator for details";
		}

		var finalHtml = "";

		if (request_project_id) {
			// var projectList = [ request_project_id ]; // 10975

			if (request_to_date) {
				request_to_date = nlapiStringToDate(request_to_date);
			} else {
				request_to_date = new Date();
			}

			nlapiLogExecution('debug', 'request_to_date', request_to_date);

			// get the saturday of the week as the end date for the searchs
			var end_day = request_to_date.getDay();
			var sunday_of_end_week = nlapiAddDays(request_to_date, -end_day);
			var endDate = nlapiAddDays(sunday_of_end_week, 6);

			nlapiLogExecution('debug', 'enddate', endDate);

			// var endDate = nlapiStringToDate("9/30/2015"); // has to be a
			// saturday

			var startDate = null;
			if (request_from_date) {
				request_from_date = nlapiStringToDate(request_from_date);
				var end_day = request_from_date.getDay();
				startDate = nlapiAddDays(request_from_date, -end_day);
			} else {
				startDate = nlapiAddDays(endDate, -69); // 2 months back
			}

			nlapiLogExecution('debug', 'startDate', startDate);
			request_from_date = startDate;
			// get week start dates
			var weekStartDateList = [];
			for (var i = 0;; i += 7) {
				var d_current_date = nlapiAddDays(startDate, i);

				if (d_current_date > endDate) {
					break;
				}

				weekStartDateList
				        .push(nlapiDateToString(d_current_date, 'date'));
			}

			var itemList = [ 'ST', 'OT', 'Leave', 'Holiday' ];
			var statusList = [ 'Open', 'Pending Approval', 'Approved', 'Billed' ];
			var employeeList = {};

			// check the allocations of the project for the duration and create
			// a
			// master array
			var allocationSearch = nlapiSearchRecord('resourceallocation',
			        null, [
			                new nlobjSearchFilter('project', null, 'anyof',
			                        request_project_id),
			                new nlobjSearchFilter(
			                        'custentity_implementationteam',
			                        'employee', 'is', 'F'),
			                new nlobjSearchFilter('startdate', null,
			                        'onorbefore', endDate),
			                new nlobjSearchFilter('enddate', null, 'onorafter',
			                        startDate) ], [ new nlobjSearchColumn(
			                'resource', null, 'group') ]);

			nlapiLogExecution('debug', 'allocation done');

			if (allocationSearch) {
				nlapiLogExecution('debug', 'allocation count',
				        allocationSearch.length);

				for (var i = 0; i < allocationSearch.length; i++) {

					employeeList[allocationSearch[i].getText('resource', null,
					        'group')] = {
						ID : allocationSearch[i].getValue('resource', null,
						        'group')
					};
				}

				nlapiLogExecution('debug', 'allocation array', JSON
				        .stringify(employeeList));

				// create master array
				var masterArray = createMasterArray(employeeList, statusList,
				        itemList, weekStartDateList);
                //nlapiSendEmail("200580",'praveena@inspirria.com',"text_checkTimesheet Report - Weekly","Weekly",null,null,null,nlapiCreateFile('weekly.txt','PLAINTEXT',JSON.stringify(masterArray)));
				        
				nlapiLogExecution('debug', 'masterArray', JSON
				        .stringify(masterArray));

				// search the time entries for the project
				var timeEntrySearch = searchRecord('timebill', 'customsearch1102_2', [
				        new nlobjSearchFilter('customer', null, 'anyof',
				                request_project_id),
				        new nlobjSearchFilter('date', null, 'within',
				                startDate, endDate) ]);

				if (timeEntrySearch) {
					nlapiLogExecution('debug', 'time entry count',
					        timeEntrySearch.length);

					var timeentry_master = {};

					// create a JSON object employee wise
					timeEntrySearch
					        .forEach(function(timeentry) {
						        var empName = timeentry.getText('employee',
						                null, 'group');
						        var empId = timeentry.getValue('employee',
						                null, 'group');
						        var timeEntryDuration = parseFloat(timeentry
						                .getValue('durationdecimal', null,
						                        'sum'));
						        var timesheetStartDate = timeentry.getValue(
						                'startdate', 'timesheet', 'group');
						        var timeEntryStatusText = timeentry.getText(
						                'approvalstatus', 'timesheet', 'group');
						        var timeEntryItemText = timeentry.getText(
						                'item', null, 'group');

						        // nlapiLogExecution('debug', 'item',
						        // timeEntryItemText);

						        if (timeEntryItemText != "Leave"
						                && timeEntryItemText != "Holiday"
						                && timeEntryItemText != "OT") {
							        timeEntryItemText = "ST";
						        }

						        nlapiLogExecution('debug', 'emp name', empName);
						        
						        try{
						        	
						        	if(!masterArray[empName]["Weeks"]){
										nlapiLogExecution('debug', 'masterArray[empName]["Weeks"][timesheetStartDate]',masterArray[empName]["Weeks"][timesheetStartDate]);
						        
						        		return;
						        	}
						        }
						        catch(ex){
						        	nlapiLogExecution('debug', 'emp not found');
						        	return;
						        }
						        
								
									var d_timesheetStartDate=nlapiStringToDate(timesheetStartDate);
									if(d_timesheetStartDate.getDay()==1)//If week start of the day is MONDAY then append the values
									{
										timesheetStartDate=nlapiDateToString(nlapiAddDays(nlapiStringToDate(timesheetStartDate),-1));

									/*	masterArray[empName]["Weeks"][timesheetStartDate]={
										Status : 'Not-Submitted',
										Items : new Items()
									};
									*/
									}
									
									
								//nlapiLogExecution('debug', 'masterArray[empName]["Weeks"][timesheetStartDate]',masterArray[empName]["Weeks"][timesheetStartDate]);
						        switch (timeEntryStatusText) {
							        case "Pending Approval":
											masterArray[empName]["Weeks"][timesheetStartDate]["Status"] = "Submitted";
										break;

							        case "Approved":
								        masterArray[empName]["Weeks"][timesheetStartDate]["Status"] = "Approved";
							        break;
									
						        }

						       if (masterArray[empName]) {
									//masterArray[empName]["Weeks"][timesheetStartDate]["Items"][timeEntryItemText]=masterArray[empName]["Weeks"][timesheetStartDate]["Items"][timeEntryItemText]?masterArray[empName]["Weeks"][timesheetStartDate]["Items"][timeEntryItemText]+timeEntryDuration : timeEntryDuration;
										masterArray[empName]["Weeks"][timesheetStartDate]["Items"][timeEntryItemText] = timeEntryDuration;
										
									
								}




						        // nlapiLogExecution(
						        // 'debug',
						        // 'B ' + empName + " " + timesheetStartDate + "
						        // "
						        // + timeEntryItemText,
						        // masterArray[empName]["Weeks"][timesheetStartDate]["Items"][timeEntryItemText]);
					        });

					nlapiLogExecution('debug', 'master timeentry', JSON
					        .stringify(masterArray));
				}

				// create a HTML table

				// CSS file

				var css_file_url = "";

				var context = nlapiGetContext();
				if (context.getEnvironment() == "SANDBOX") {
					css_file_url = "https://system.na1.netsuite.com/core/media/media.nl?id=63885&c=3883006&h=1ab6e33882afec4c1d2b&_xt=.css";
				} else if (context.getEnvironment() == "PRODUCTION") {
					css_file_url = "https://system.na1.netsuite.com/core/media/media.nl?id=63885&c=3883006&h=1ab6e33882afec4c1d2b&_xt=.css";
				}

				var htmlText = "";
				htmlText += "<link href='" + css_file_url
				        + "'  rel='stylesheet'>";

				htmlText += "<table class='timesheet-master-table'>";
				htmlText += "<tr class='header-row'>";

				htmlText += "<td>";
				htmlText += "Employee Name";
				htmlText += "</td>";

				var statusCssArray = {
				    "Approved" : "ts-approved",
				    "Submitted" : "ts-submitted",
				    "Not-Submitted" : "ts-not-submitted"
				};

				for ( var weekDate in weekStartDateList) {
					htmlText += "<td>";
					htmlText += weekStartDateList[weekDate];
					htmlText += "</td>";
				}
				htmlText += "<td>";
				htmlText += "Total";
				htmlText += "</td>";
				htmlText += "</tr>";

				for ( var empName in masterArray) {
					var total_st = 0, total_ot = 0, total_leave = 0, total_holiday = 0;

					htmlText += "<tr>";
					htmlText += "<td class='emp-name'>" + empName + "</td>";

					for ( var weekDate in weekStartDateList) {
						var weekStatus = masterArray[empName]["Weeks"][weekStartDateList[weekDate]]["Status"];
						var weekData = masterArray[empName]["Weeks"][weekStartDateList[weekDate]]["Items"];
						
						//nlapiLogExecution("DEBUG",'weekStatus',weekStatus);
						//nlapiLogExecution("DEBUG",'weekData',weekData);
						
						if (weekStatus == "Not-Submitted") {
							htmlText += "<td class='"
							        + statusCssArray[weekStatus] + "'>";
							htmlText += "-";
							htmlText += "</td>";
						} else {
							htmlText += "<td class='"
							        + statusCssArray[weekStatus] + "'>";
							htmlText += "<p class='st'>" + weekData.ST + "</p>";
							htmlText += " / ";
							htmlText += "<p class='ot'>" + weekData.OT + "</p>";
							htmlText += " / ";
							htmlText += "<p class='leave'>" + weekData.Leave
							        + "</p>";
							htmlText += " / ";
							htmlText += "<p class='holiday'>"
							        + weekData.Holiday + "</p>";
							htmlText += "</td>";

							total_st += parseFloat(weekData.ST);
							total_ot += parseFloat(weekData.OT);
							total_leave += parseFloat(weekData.Leave);
							total_holiday += parseFloat(weekData.Holiday);
						}
					}

					// add the total row
					htmlText += "<td class='total-column'>";
					htmlText += "<p class='st'>" + total_st + "</p>";
					htmlText += " / ";
					htmlText += "<p class='ot'>" + total_ot + "</p>";
					htmlText += " / ";
					htmlText += "<p class='leave'>" + total_leave + "</p>";
					htmlText += " / ";
					htmlText += "<p class='holiday'>" + total_holiday + "</p>";
					htmlText += "</td>";

					htmlText += "</tr>";
				}

				htmlText += "</table>";

				finalHtml = "<table class='master-table'><tr><td>" + htmlText
				        + "</td><td class='legend-table-master-cell'>"
				        + legendsTable() + "" + hoursLegendTable()
				        + "</td></tr></table>";
			} else {
				finalHtml = "No Allocation Active For This Project";
			}
		} else {
			finalHtml = "Select any project and click refresh";
		}

		nlapiLogExecution('debug', 'lets create the page');
		var form = nlapiCreateForm("Timesheet Report - Weekly");

		form.addFieldGroup('custpage_grp_1', 'Select Details');

		var field_from_date = form.addField('custpage_from_date', 'date',
		        'From Date', null, 'custpage_grp_1');
		field_from_date.setBreakType('startcol');

		if (request_from_date) {

			field_from_date.setDefaultValue(nlapiDateToString(
			        request_from_date, 'date'));
		}

		var field_to_date = form.addField('custpage_to_date', 'date',
		        'To Date', null, 'custpage_grp_1');
		field_to_date.setBreakType('startcol');

		if (request_to_date) {

			field_to_date.setDefaultValue(nlapiDateToString(request_to_date,
			        'date'));
		}

		var field_project = form.addField('custpage_project', 'select',
		        'Project', null, 'custpage_grp_1');
		field_project.setBreakType('startcol');
		field_project.setMandatory(true);

		if (projectList) {

			for (var i = 0; i < projectList.length; i++) {
				field_project.addSelectOption(projectList[i].getId(),
				        projectList[i].getValue('entityid') + " "
				                + projectList[i].getValue('altname'));
			}
		}

		if (request_project_id) {
			field_project.setDefaultValue(request_project_id);
		}

		form.addFieldGroup('custpage_grp_2', 'Timesheet Details');
		form.addField('custpage_timesheet_html', 'inlinehtml', '', null,
		        'custpage_grp_2').setDefaultValue(finalHtml);

		form.addSubmitButton("Refresh");
		response.writePage(form);
	} catch (err) {
		nlapiLogExecution('error', 'suitelet', err);
		displayErrorForm(err, "Timesheet Report - Weekly");
	}
}

function createMasterArray(empList, statusList, itemList, weekList) {
	// create the employee array
	var employeeArray = {};
	for ( var empName in empList) {

		employeeArray[empName] = {
		    Id : empList[empName]["ID"],
		    Weeks : {}
		};

		for ( var weekStartDate in weekList) {
			employeeArray[empName]["Weeks"][weekList[weekStartDate]] = {
			    Status : 'Not-Submitted',
			    Items : new Items()
			};
		}

		// employeeArray[empName]["Weeks"]
	}

	return employeeArray;
}

function Items() {
	this.ST = 0;
	this.OT = 0;
	this.Leave = 0;
	this.Holiday = 0;
}

function legendsTable() {
	var htmlText = "";
	htmlText += "<table class='legend-table'>";
	htmlText += "<tr><td class='approved-circle'></td><td>Approved</td></tr>";
	htmlText += "<tr><td class='submitted-circle'></td><td>Pending Approval</td></tr>";
	htmlText += "<tr><td class='not-submitted-circle'></td><td>Not-Submitted</td></tr>";
	htmlText += "</table>";
	return htmlText;
}

function hoursLegendTable() {
	var htmlText = "";
	htmlText += "<table class='legend-table'>";
	htmlText += "<tr><td class='hours-legend'>ST / OT / Leave / Holiday</td></tr>";
	htmlText += "</table>";
	return htmlText;
}