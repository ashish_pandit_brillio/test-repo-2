/**
 * 
 * 
 * Version Date Author Remarks
 * 
 * 1.00 11 Jul 2016 Nitish Mishra
 * 
 */

function scheduled(type) {
	try {
		nlapiLogExecution('DEBUG', 'Start');

		var year_start_date = nlapiStringToDate('1/1/2015');
		var current_date = new Date();

		// delete all existing records
		deleteExistingUtilizationRecord();
		delete deleteExistingUtilizationRecord;
		
		// create new records
		for (var monthCounter = 0;; monthCounter++) {
			yieldScript(nlapiGetContext());

			var d_month_start = nlapiAddMonths(year_start_date, monthCounter);
			var d_month_end = nlapiAddDays(nlapiAddMonths(d_month_start, 1), -1);
			var s_month_start = nlapiDateToString(d_month_start, 'date');
			var s_month_end = nlapiDateToString(d_month_end, 'date');

			// don't run for current month
			if (d_month_end > current_date) {
				break;
			}

			// get all the projects active in this duration
			var jobSearch = searchRecord('job', null, [
			        new nlobjSearchFilter('status', null, 'anyof', [ 2, 4 ]),
			        new nlobjSearchFilter('formuladate', null, 'notafter',
			                s_month_end).setFormula('{startdate}'),
			        new nlobjSearchFilter('formuladate', null, 'notbefore',
			                s_month_start).setFormula('{enddate}'),
			        new nlobjSearchFilter('isinactive', null, 'is', 'F') ]);

			if (jobSearch) {

				for (var j = 0; j < jobSearch.length; j++) {

					try {
						var rec = nlapiCreateRecord('customrecord_utilization_report_trend');
						rec.setFieldValue('custrecord_urt_project',
						        jobSearch[j].getId());
						rec.setFieldValue('custrecord_urt_start_date',
						        s_month_start);
						rec.setFieldValue('custrecord_urt_end_date',
						        s_month_end);
						nlapiSubmitRecord(rec);
						yieldScript(nlapiGetContext());
					} catch (k) {
						nlapiLogExecution('ERROR', 'start : ' + s_month_start
						        + ' project : ' + jobSearch[j].getId(), k);
					}
				}
			} else {
				nlapiLogExecution('debug', 'jobSearch', 'No project found for '
				        + s_month_start);
			}

			nlapiLogExecution('DEBUG', 'Completed for ' + s_month_start);
		}

		nlapiLogExecution('DEBUG', 'End');
	} catch (err) {
		nlapiLogExecution('ERROR', 'scheduled', err);
		// throw err;
	}
}

function deleteExistingUtilizationRecord() {
	try {
		var utilSearch = searchRecord('customrecord_level_utilization_detail');

		for (var j = 0; j < utilSearch.length; j++) {
			try {
				nlapiDeleteRecord('customrecord_level_utilization_detail',
				        utilSearch[j].getId());
				yieldScript(nlapiGetContext());
			} catch (k) {
				nlapiLogExecution('ERROR',
				        'project : ' + utilSearch[j].getId(), k);
			}
		}

		nlapiLogExecution('DEBUG', 'Level Data Cleared');
		delete utilSearch;

		var utilSearch = searchRecord('customrecord_utilization_report_trend');

		for (var j = 0; j < utilSearch.length; j++) {
			try {
				nlapiDeleteRecord('customrecord_utilization_report_trend',
				        utilSearch[j].getId());
				yieldScript(nlapiGetContext());
			} catch (k) {
				nlapiLogExecution('ERROR',
				        'project : ' + utilSearch[j].getId(), k);
			}
		}
		delete utilSearch;
		
		nlapiLogExecution('DEBUG', 'Existing Data Cleared');
	} catch (err) {
		nlapiLogExecution('ERROR', 'deleteExistingUtilizationRecord', err);
		throw err;
	}
}

function yieldScript(currentContext) {

	if (currentContext.getRemainingUsage() <= 100) {
		nlapiLogExecution('AUDIT', 'API Limit Exceeded');
		var state = nlapiYieldScript();

		if (state.status == "FAILURE") {
			nlapiLogExecution('ERROR', 'Yield Script Failed', 'Reason : '
			        + state.reason + ' / Size : ' + state.size);
			return false;
		} else if (state.status == "RESUME") {
			nlapiLogExecution('AUDIT', 'Script Resumed');
		}
	}
}
