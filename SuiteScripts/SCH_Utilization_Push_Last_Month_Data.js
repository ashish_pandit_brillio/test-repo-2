/**
 * Create Utilization Record for the last month
 * 
 * Version Date Author Remarks 1.00 17 Jun 2016 Nitish Mishra
 * 
 */

function scheduled(type) {

	nlapiLogExecution('debug', 'Utilization Report Data Push', 'Start');
	try {

		var today = new Date();
		var sameDateLastMonth = nlapiAddMonths(today, -1);

		var lastMonthStartDate = getMonthStartDate(sameDateLastMonth);
		var lastMonthEndDate = getMonthEndDate(lastMonthStartDate);

		var s_lastMonthStartDate = nlapiDateToString(lastMonthStartDate, 'date');
		var s_lastMonthEndDate = nlapiDateToString(lastMonthEndDate, 'date');

		// get all the active projects that have end date after the last month
		// start date and start date before last month end date
		var jobSearch = searchRecord('job', null, [
		        new nlobjSearchFilter('status', null, 'anyof', [ 2, 4 ]),
		        new nlobjSearchFilter('formuladate', null, 'notafter',
		                lastMonthEndDate).setFormula('{startdate}'),
		        new nlobjSearchFilter('formuladate', null, 'notbefore',
		                lastMonthStartDate).setFormula('{enddate}'),
		        new nlobjSearchFilter('isinactive', null, 'is', 'F') ]);

		nlapiLogExecution('debug', 'start date', s_lastMonthStartDate);
		nlapiLogExecution('debug', 'end date', s_lastMonthEndDate);

		if (jobSearch) {
			nlapiLogExecution('debug', 'jobSearch', jobSearch.length);
		}

		if (jobSearch) {

			for (var j = 0; j < jobSearch.length; j++) {

				try {
					var rec = nlapiCreateRecord('customrecord_utilization_report_trend');
					rec.setFieldValue('custrecord_urt_project', jobSearch[j]
					        .getId());
					rec.setFieldValue('custrecord_urt_start_date',
					        s_lastMonthStartDate);
					rec.setFieldValue('custrecord_urt_end_date',
					        s_lastMonthEndDate);
					// nlapiSubmitRecord(rec);
					yieldScript(nlapiGetContext());
				} catch (k) {
					nlapiLogExecution('ERROR', 'project : '
					        + jobSearch[j].getId(), k);
				}
			}
		} else {
			nlapiLogExecution('debug', 'jobSearch', 'No project found');
		}

	} catch (err) {
		nlapiLogExecution('ERROR', 'Some error occured', err);
	}

	nlapiLogExecution('debug', 'Utilization Report Data Push', 'End');
}

function getMonthEndDate(currentDate) {
	return nlapiAddDays(nlapiAddMonths(currentDate, 1), -1);
}

function getMonthStartDate(currentDate) {
	var month = currentDate.getMonth() + 1;
	var year = currentDate.getFullYear();
	var s_startDate = month + "/1/" + year;
	var d_startDate = nlapiStringToDate(s_startDate);
	return d_startDate;
}

function yieldScript(currentContext) {

	if (currentContext.getRemainingUsage() <= 100) {
		nlapiLogExecution('AUDIT', 'API Limit Exceeded');
		var state = nlapiYieldScript();

		if (state.status == "FAILURE") {
			nlapiLogExecution('ERROR', 'Yield Script Failed', 'Reason : '
			        + state.reason + ' / Size : ' + state.size);
			return false;
		} else if (state.status == "RESUME") {
			nlapiLogExecution('AUDIT', 'Script Resumed');
		}
	}
}