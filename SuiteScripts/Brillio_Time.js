/*function Time_Before(type, form)
{
	if(form.getSubList('timeitem') != null)
	{
		form.addField('custpage_tslbl', 'label', '<b>Note - For Customer projects, standard hours cannot exceed 40 hrs(excluding OT hrs)<b>');
	
		var TimeTab = form.getSubList('timeitem');
		if(TimeTab.getField('rate') != null)
		{
			TimeTab.getField('rate').setDisplayType('hidden');
			TimeTab.getField('price').setDisplayType('hidden');
		}
	}
}

function Update_Roles()
{
	var cols = new Array();
	
	var filters = new Array();
	filters[filters.length] = new nlobjSearchFilter('email', null, 'doesnotcontain', '@brillio.com');
	filters[filters.length] = new nlobjSearchFilter('email', null, 'isnotempty');
	filters[filters.length] = new nlobjSearchFilter('subsidiary', null, 'is', 2);
	filters[filters.length] = new nlobjSearchFilter('internalid', null, 'is', 2885);
	
	var searchresult = nlapiSearchRecord('employee', null, filters, cols);
	nlapiLogExecution('DEBUG', 'count', searchresult.length);
	for(var i=0; i< searchresult.length; i++)
	{
		nlapiLogExecution('DEBUG', 'Employee Id', searchresult[i].getId());
		var record = nlapiLoadRecord('employee', searchresult[i].getId());
		nlapiLogExecution('DEBUG', 'Role Count', record.getLineItemCount('roles'));
		
		if(record.getLineItemCount('roles') > 1)
		{
			for(var i = 1; i <= record.getLineItemCount('roles'); i++)
			{
				nlapiLogExecution('DEBUG', 'Role Id', record.getLineItemValue('roles', 'selectedrole', i));
				if (record.getLineItemValue('roles', 'selectedrole', i) == 1005)
				{
					nlapiLogExecution('DEBUG', 'Removing Role Id', record.getLineItemValue('roles', 'selectedrole', i));
					record.selectLineItem('roles', i);
					record.removeLineItem('roles',i);
					record.commitLineItem('roles');
					nlapiLogExecution('DEBUG', 'Role Count after removal', record.getLineItemCount('roles'));
				}
			}
			var submitrecord = nlapiSubmitRecord(record);
			nlapiLogExecution('DEBUG', 'Record Submitted', submitrecord);
		}
	}
}*/
/*
function Vendor_BeforeLoad(type, form)
{
	//1. Check for type - edit & view
	//2. Show the tab only if resources are mapped
	
	nlapiLogExecution('DEBUG', 'ID', nlapiGetFieldValue('entityid'));
	
	if(type == 'edit' || type == 'view')
	{
     	 var filters = new Array();
      
     	 var projectDetails = nlapiLookupField('customrecord_subtier_vendor_data', 1581, [
			        'custrecord_stvd_vendor' ]);
     	 nlapiLogExecution('DEBUG', 'projectDetails', projectDetails.custrecord_stvd_vendor);
      
		 filters[filters.length] = new nlobjSearchFilter('custentity_vendoragency', null, 'anyof', nlapiGetFieldValue('id'));
      
    	
      
      
		var cols = new Array();
		cols[cols.length] = new nlobjSearchColumn('entityid');
		cols[cols.length] = new nlobjSearchColumn('hiredate');
		cols[cols.length] = new nlobjSearchColumn('releasedate');
		cols[cols.length] = new nlobjSearchColumn('custentity_stpayrate');
		cols[cols.length] = new nlobjSearchColumn('custentity_otpayrate');
		cols[cols.length] = new nlobjSearchColumn('custentity_referencevendor');
		cols[cols.length] = new nlobjSearchColumn('custentity_refvendorstrate');
		cols[cols.length] = new nlobjSearchColumn('custentity_refvendorotrate');
		
		
		
		var searchresult = nlapiSearchRecord('employee', null, filters, cols);
		if(searchresult != null)
		{
			form.addTab('custpage_empmap', 'Contract Resources');
			
			var Brillio_EmpMap = form.addSubList ('custpage_empmaplist', 'staticlist', 'Resources', 'custpage_empmap');
			Brillio_EmpMap.addField('custpage_view', 		'text', 	'View');
			Brillio_EmpMap.addField('custpage_empid', 		'text', 	'Employee Name');
			Brillio_EmpMap.addField('custpage_startdate', 	'date', 	'Start Date');
			Brillio_EmpMap.addField('custpage_enddate', 	'date', 	'End Date');
			Brillio_EmpMap.addField('custpage_strate', 		'currency', 'ST Rate');
			Brillio_EmpMap.addField('custpage_otrate', 		'currency', 'OT Rate');
			Brillio_EmpMap.addField('custpage_refvendor', 	'text', 	'Referral Vendor');
			Brillio_EmpMap.addField('custpage_refstrate', 	'currency', 'Referral ST Rate');
			Brillio_EmpMap.addField('custpage_refotrate', 	'currency', 'Referral OT Rate');
			
			for(var i=0; i<searchresult.length; i++)
			{
				var URL = nlapiResolveURL('RECORD', 'employee', searchresult[i].getId());

				var ViewURL = '<a href="' + URL + '" target="_blank">View</a>';
				
				Brillio_EmpMap.setLineItemValue('custpage_view', 	i+1, ViewURL); 
				Brillio_EmpMap.setLineItemValue('custpage_empid', 		i+1, searchresult[i].getValue('entityid'));
				Brillio_EmpMap.setLineItemValue('custpage_startdate', 	i+1, searchresult[i].getValue('hiredate'));
				Brillio_EmpMap.setLineItemValue('custpage_enddate', 	i+1, searchresult[i].getValue('releasedate'));
				Brillio_EmpMap.setLineItemValue('custpage_strate', 		i+1, searchresult[i].getValue('custentity_stpayrate'));
				Brillio_EmpMap.setLineItemValue('custpage_otrate', 		i+1, searchresult[i].getValue('custentity_otpayrate'));
				Brillio_EmpMap.setLineItemValue('custpage_refvendor', 	i+1, searchresult[i].getText('custentity_referencevendor'));
				Brillio_EmpMap.setLineItemValue('custpage_refstrate', 	i+1, searchresult[i].getValue('custentity_refvendorstrate'));
				Brillio_EmpMap.setLineItemValue('custpage_refotrate', 	i+1, searchresult[i].getValue('custentity_refvendorotrate'));
			}
		}
	}
}
*/

function ExpenseReport_BeforeLoad(form, type)
{
  if(type == 'create' || type == 'edit'){
    nlapiSetFieldValue('custbody_is_je_updated_for_emp_type','F');
  }
	if(nlapiGetLineItemField('expense', 'customer', 1) != null)
	{
		var Project = nlapiGetLineItemField('expense', 'customer', 1);
		var TaxCode = nlapiGetLineItemField('expense', 'taxcode', 1);
		var TaxRate1 = nlapiGetLineItemField('expense', 'taxrate1', 1);
		var TaxAmt1 = nlapiGetLineItemField('expense', 'tax1amt', 1);
		
		Project.setMandatory(true);
		if(TaxCode != null) TaxCode.setDisplayType('disabled');
		if(TaxRate1 != null) TaxRate1.setDisplayType('hidden');
		if(TaxAmt1 != null) TaxAmt1.setDisplayType('hidden');
	}
}

function beforeSubmit(type){
	try{
		if(type == 'create'|| type == 'edit'){
			var i_subsidiary = nlapiGetFieldValue('subsidiary');
			if(parseInt(i_subsidiary) == parseInt(10)){
				var lineCount = nlapiGetLineItemCount('expense');
				for(var line=1;line<lineCount;line++){
					nlapiSetLineItemValue('expense','taxcode',line,2905);
				}
			}
		}
	}
	catch(e){
	nlapiLogExecution('DEBUG','Before Load Error',e);
	}
	
}