/**
 * @author Jayesh
 */

function suitelet_auto_confirm_screen()
{
	try
	{
		var o_context = nlapiGetContext();
		var i_user_logegdIn_id = o_context.getUser();
		
		// design form which will be displayed to user
		var o_form_obj = nlapiCreateForm("Auto Confirm Month End Effort");
		
		if(i_user_logegdIn_id != 8177) // jayesh emp id fr testing:- 39108
		{
			var status = nlapiScheduleScript('customscript_sch_fp_revrec_auto_confirm');
			
			var s_result_not_found = "<table>";
			s_result_not_found += "<tr><td>";
			s_result_not_found += "You are not authorised to view this page !!!!";
			s_result_not_found += "</td></tr>";
			s_result_not_found += "</table>";
			
			var f_no_rslt_found = o_form_obj.addField('auto_approve', 'inlinehtml', 'Not Authorised');
			f_no_rslt_found.setDefaultValue(s_result_not_found);
			
			response.writePage(o_form_obj);
			
			return;
		}
		
		if(request.getMethod() == 'GET')
		{
			o_form_obj.addSubmitButton('Auto Approve Effort');
		}
		else
		{
			var status = nlapiScheduleScript('customscript_sch_fp_revrec_auto_confirm');
			
			var s_result_not_found = "<table>";
			s_result_not_found += "<tr><td>";
			s_result_not_found += "Auto Approval request is under process";
			s_result_not_found += "</td></tr>";
			s_result_not_found += "</table>";
			
			var f_no_rslt_found = o_form_obj.addField('auto_approve', 'inlinehtml', 'Auto Approval Under Process');
			f_no_rslt_found.setDefaultValue(s_result_not_found);
		}
		
		response.writePage(o_form_obj);
	}
	catch(err)
	{
		nlapiLogExecution('ERROR','suitelet_auto_confirm_screen','ERROR MESSAGE:- '+err);
	}
}
