/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       30 May 2017     deepak.srinivas
 *
 */

/**
 * @param {Object} dataIn Parameter object
 * @returns {Object} Output object
 */
function postRESTlet(dataIn) {
	var response = new Response();
	try{
		nlapiLogExecution('debug', 'dataIn', JSON.stringify(dataIn));
		var current_date = nlapiDateToString(new Date());
		nlapiLogExecution('debug', 'current_date', current_date);
		
		var requestType = dataIn.RequestType;
		
		switch (requestType) {

		case M_Constants.Request.Get:

			
				response.Data = getAllActiveEmployeeName();
				response.Status = true;
		}
		
	}
	catch (err) {
		nlapiLogExecution('ERROR', 'postRESTlet', err);
		response.Data = err;
	}
	return response;
}
	
function getAllActiveEmployeeName() {
		try {
			var employeeList = [];
			var emp_id ='';
			var emp_type ='';
			var person_type ='';
			var emp_fusion_id ='';
			var emp_frst_name ='';
			var emp_middl_name ='';
			var emp_lst_name ='';
			var emp_full_name ='';
			var emp_id ='';
			var emp_id ='';
			
			var cols = [];
			cols.push(new nlobjSearchColumn('firstname'));
			cols.push(new nlobjSearchColumn('middlename'));
			cols.push(new nlobjSearchColumn('lastname'));
			cols.push(new nlobjSearchColumn('custentity_fusion_empid'));
			cols.push(new nlobjSearchColumn('department'));
			cols.push(new nlobjSearchColumn('title'));
			cols.push(new nlobjSearchColumn('custentity_employeetype'));
			cols.push(new nlobjSearchColumn('custentity_persontype'));
			cols.push(new nlobjSearchColumn('email'));
			
			var employeeSearch = searchRecord('employee', null, [
			        new nlobjSearchFilter('custentity_employee_inactive', null,
			                'is', 'F'),
			        new nlobjSearchFilter('isinactive', null, 'is', 'F'),
			        new nlobjSearchFilter('custentity_implementationteam', null,
			                'is', 'F') ], cols);
			var dataRow = [];
			var JSON = {};
			if (employeeSearch) {
					for(var i=0;i<employeeSearch.length;i++){
					    emp_id = employeeSearch[i].getId();
						emp_type = employeeSearch[i].getText('custentity_employeetype');
					    person_type = employeeSearch[i].getText('custentity_persontype');
						emp_fusion_id = employeeSearch[i].getValue('custentity_fusion_empid');
						emp_frst_name = employeeSearch[i].getValue('firstname');
						emp_middl_name = employeeSearch[i].getValue('middlename');
						emp_lst_name = employeeSearch[i].getValue('lastname');
						if(emp_frst_name)
							emp_full_name = emp_frst_name;
							
						if(emp_middl_name)
							emp_full_name = emp_full_name + ' ' + emp_middl_name;
							
						if(emp_lst_name)
							emp_full_name = emp_full_name + ' ' + emp_lst_name;
						
						JSON = {
								ID: emp_id,
								EmployeeID:emp_fusion_id,
								Name: emp_full_name,
								EmployeeType: emp_type,
								PersonType: person_type,
								Designation: employeeSearch[i].getValue('title'),
								Department: employeeSearch[i].getText('department'),
								Email: employeeSearch[i].getValue('email')
						
								
						};
						dataRow.push(JSON);
						
					}
					
				
			}

			return dataRow;
		} catch (err) {
			nlapiLogExecution('error', 'getAllActiveEmployeeName', err);
			throw err;
		}
	}
