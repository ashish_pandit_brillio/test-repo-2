/**
 * Set a folder as private that is create / edited from employee center
 * 
 * Version    Date            Author           Remarks
 * 1.00       23 Jul 2015     nitish.mishra
 *
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your
 * script deployment.
 * 
 * @appliedtorecord recordType
 * 
 * @param {String}
 *            type Operation types: create, edit, delete, xedit, approve,
 *            cancel, reject (SO, ER, Time Bill, PO & RMA only) pack, ship (IF
 *            only) dropship, specialorder, orderitems (PO only) paybills
 *            (vendor payments)
 * @returns {Void}
 */
function userEventBeforeSubmit(type) {

	var context = nlapiGetContext();
	var roleCenter = context.getRoleCenter();

	if (roleCenter == 'EMPLOYEE') {
		nlapiSetFieldValue('group', 11363);
		// nlapiSetFieldValue('isprivate', 'T');
	}
}
