/**
 * @author Shweta
 */
// BEGIN SCRIPT DESCRIPTION BLOCK ==================================
{
	/*
	 * Script Name : CLI_Project_Validations.js Author : Shweta Chopde Date : 9
	 * May 2014 Description : Apply validations on Project Record
	 * 
	 * 
	 * Script Modification Log:
	 *  -- Date -- -- Modified By -- --Requested By-- -- Description --
	 *   2/2/2015     Nitish Mishra    Nitish Mishra     Customer and Project verticals can be different
	 * 
	 * 
	 * 
	 * Below is a summary of the process controls enforced by this script file.
	 * The control logic is described more fully, below, in the appropriate
	 * function headers and code blocks.
	 * 
	 * PAGE INIT - pageInit(type)
	 * 
	 * 
	 * SAVE RECORD - saveRecord()
	 * 
	 * 
	 * VALIDATE FIELD - validateField(type, name, linenum)
	 * 
	 * 
	 * FIELD CHANGED - fieldChanged(type, name, linenum)
	 * 
	 * 
	 * POST SOURCING - postSourcing(type, name)
	 * 
	 * 
	 * LINE INIT - lineInit(type)
	 * 
	 * 
	 * VALIDATE LINE - validateLine()
	 * 
	 * 
	 * RECALC - reCalc()
	 * 
	 * 
	 * SUB-FUNCTIONS - The following sub-functions are called by the above core
	 * functions in order to maintain code modularization:
	 * 
	 * 
	 * 
	 * 
	 * 
	 */
}
// END SCRIPT DESCRIPTION BLOCK ====================================

// BEGIN SCRIPT UPDATION BLOCK ====================================
/*
 * 
 * 
 */
// END SCRIPT UPDATION BLOCK ====================================

// BEGIN GLOBAL VARIABLE BLOCK =====================================
{

	// Initialize any Global Variables, in particular, debugging variables...

}
// END GLOBAL VARIABLE BLOCK =======================================

// BEGIN PAGE INIT ==================================================

function pageInit_project_validations (type) {

	var i_recordID = nlapiGetRecordId();

	var s_record_type = nlapiGetRecordType();

	var i_customer = nlapiGetFieldText('parent');

	if (_logValidation(i_customer)) {
		if (i_customer != 'COIN-01 Collabera Inc') {
			nlapiDisableField('entityid', true)
		}// COIN-01 Collabera Inc

	}// Record ID & Type

	// if(name == 'parent')
	{
		var i_customer = nlapiGetFieldValue('parent')

		var i_customer_txt = nlapiGetFieldText('parent')

		if (i_customer_txt != 'COIN-01 Collabera Inc') {
			if (_logValidation(i_customer)) {
				try {
					var o_custOBJ = nlapiLoadRecord('customer', i_customer)
				}
				catch (exc) {
					var o_custOBJ = nlapiLoadRecord('job', i_customer)
				}

				if (_logValidation(o_custOBJ)) {
					var i_customerLOB = o_custOBJ.getFieldValue('custentity_customerlob')

					var i_vertical = o_custOBJ.getFieldValue('custentity_vertical')

					var i_customer_sector = o_custOBJ.getFieldValue('category')

					var i_geo = o_custOBJ.getFieldValue('custentity_entitygeo')

					if (!_logValidation(i_customerLOB)) {
						i_customerLOB = ''
					}
					if (!_logValidation(i_vertical)) {
						i_vertical = ''
					}
					if (!_logValidation(i_customer_sector)) {
						i_customer_sector = ''
					}
					if (!_logValidation(i_geo)) {
						i_geo = ''
					}

					

					//added by Nitish Mishra (date : 2 Feb 2015)
					//Customer and Project Verticals can be different in case of end-customer
					//only to be sourced while create
					if (type != "edit") {
						nlapiSetFieldValue('custentity_vertical', i_vertical);
						nlapiSetFieldValue('custentity_customerlob', i_customerLOB);
					}
					
					nlapiSetFieldValue('category', i_customer_sector)
					nlapiSetFieldValue('custentity_entitygeo', i_geo)

				}// Customer OBJ

			}// Customer

		}// Non Collebra Customer

		if (i_customer_txt == 'COIN-01 Collabera Inc') {
			nlapiSetFieldValue('custentity_customerlob', '')
			nlapiSetFieldValue('custentity_vertical', '')
			nlapiSetFieldValue('category', '')
			nlapiSetFieldValue('custentity_entitygeo', '')
		}

	}// Customer

}// PAGE INIT

// END PAGE INIT ====================================================

// BEGIN SAVE RECORD ================================================

function saveRecord () {

	/*
	 * On save record:
	 *  - PURPOSE
	 * 
	 * 
	 * 
	 * FIELDS USED:
	 * 
	 * --Field Name-- --ID-- --Line Item Name--
	 * 
	 * 
	 */

	// LOCAL VARIABLES

	// SAVE RECORD CODE BODY

	return true;

}

// END SAVE RECORD ==================================================

// BEGIN VALIDATE FIELD =============================================

function validateField (type, name, linenum) {

	/*
	 * On validate field:
	 *  - EXPLAIN THE PURPOSE OF THIS FUNCTION
	 * 
	 * 
	 * FIELDS USED:
	 * 
	 * --Field Name-- --ID--
	 * 
	 * 
	 */

	// LOCAL VARIABLES

	// VALIDATE FIELD CODE BODY

	return true;

}

// END VALIDATE FIELD ===============================================

// BEGIN FIELD CHANGED ==============================================

function fieldChanged_source_project (type, name, linenum) {

	if (name == 'parent') {
		var i_customer = nlapiGetFieldValue('parent')
		// alert('i_customer->'+i_customer)
		var i_customer_txt = nlapiGetFieldText('parent')

		if (i_customer_txt != 'COIN-01 Collabera Inc') {
			if (_logValidation(i_customer)) {
				try {
					var o_custOBJ = nlapiLoadRecord('customer', i_customer)
				}
				catch (exc) {
					var o_custOBJ = nlapiLoadRecord('job', i_customer)
				}
				// alert('o_custOBJ->'+o_custOBJ)

				if (_logValidation(o_custOBJ)) {
					var i_customerLOB = o_custOBJ.getFieldValue('custentity_customerlob')

					var i_vertical = o_custOBJ.getFieldValue('custentity_vertical')

					var i_customer_sector = o_custOBJ.getFieldValue('category')

					var i_geo = o_custOBJ.getFieldValue('custentity_entitygeo')
					// alert('i_geo->'+i_geo)
					if (!_logValidation(i_customerLOB)) {
						i_customerLOB = ''
					}
					if (!_logValidation(i_vertical)) {
						i_vertical = ''
					}
					if (!_logValidation(i_customer_sector)) {
						i_customer_sector = ''
					}
					if (!_logValidation(i_geo)) {
						i_geo = ''
					}

					// alert('i_customerLOB->'+i_customerLOB)
					nlapiSetFieldValue('custentity_customerlob', i_customerLOB)
					// alert('i_vertical->'+i_vertical)
					nlapiSetFieldValue('custentity_vertical', i_vertical)
					// alert('i_customer_sector->'+i_customer_sector)
					nlapiSetFieldValue('category', i_customer_sector)
					// alert('i_geo->'+i_geo)
					nlapiSetFieldValue('custentity_entitygeo', i_geo)
					// alert('Final')
				}// Customer OBJ

			}// Customer

		}// Non Collebra Customer
		if (i_customer_txt == 'COIN-01 Collabera Inc') {
			nlapiSetFieldValue('custentity_customerlob', '')
			nlapiSetFieldValue('custentity_vertical', '')
			nlapiSetFieldValue('category', '')
			nlapiSetFieldValue('custentity_entitygeo', '')
		}
	}// Customer

}

// END FIELD CHANGED ================================================

// BEGIN POST SOURCING ==============================================

function postSourcing (type, name) {

	/*
	 * On post sourcing:
	 *  - PURPOSE
	 * 
	 * FIELDS USED:
	 * 
	 * --Field Name-- --ID-- --Line Item Name--
	 * 
	 * 
	 */

	// LOCAL VARIABLES

	// POST SOURCING CODE BODY

}

// END POST SOURCING ================================================

// BEGIN LINE INIT ==============================================

function lineInit (type) {

	/*
	 * On Line Init:
	 *  - PURPOSE
	 * 
	 * FIELDS USED:
	 * 
	 * --Field Name-- --ID-- --Line Item Name--
	 * 
	 * 
	 */

	// LOCAL VARIABLES

	// LINE INIT CODE BODY

}

// END LINE INIT ================================================

// BEGIN VALIDATE LINE ==============================================

function validateLine (type) {

	/*
	 * On validate line:
	 *  - EXPLAIN THE PURPOSE OF THIS FUNCTION
	 * 
	 * 
	 * FIELDS USED:
	 * 
	 * --Field Name-- --ID--
	 * 
	 * 
	 */

	// LOCAL VARIABLES

	// VALIDATE LINE CODE BODY

	return true;

}

// END VALIDATE LINE ================================================

// BEGIN RECALC =====================================================

function recalc (type) {

	/*
	 * On recalc:
	 *  - EXPLAIN THE PURPOSE OF THIS FUNCTION
	 * 
	 * 
	 * FIELDS USED:
	 * 
	 * --Field Name-- --ID--
	 * 
	 * 
	 */

	// LOCAL VARIABLES

	// RECALC CODE BODY

}

// END RECALC =======================================================

// BEGIN FUNCTION ===================================================

function _logValidation (value) {

	if (value != null && value.toString() != null && value != '' && value != undefined && value.toString() != undefined
			&& value != 'undefined' && value.toString() != 'undefined' && value.toString() != 'NaN' && value != NaN) {
		return true;
	}
	else {
		return false;
	}
}

// END FUNCTION =====================================================