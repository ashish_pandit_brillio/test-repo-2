/**
Purpose : Updation of TA and EA to PM and Dm for External allocations
Return Type : Null

**/
function extApproverChanges(type) {
	try {
		var jobidcol = new Array();
		jobidcol[0] = new nlobjSearchColumn('internalid');
		var search = nlapiSearchRecord('job', 'customsearch1771', null, jobidcol);
		for (var countid = 0; countid < search.length; countid++) {
			var pid = search[countid].getValue('internalid');
			nlapiLogExecution('AUDIT', 'Info', 'pid#' + pid);
			var col = new Array();
			col[0] = new nlobjSearchColumn('resource');
			col[1] = new nlobjSearchColumn('project');
			var resFilter = new Array();
			resFilter[0] = new nlobjSearchFilter('project', null, 'anyOf', pid);
			resFilter[1] = new nlobjSearchFilter('enddate', null, 'onorafter', 'today');
			resFilter[2] = new nlobjSearchFilter('startdate', null, 'onorbefore', 'today');
			resFilter[3] = new nlobjSearchFilter('percentoftime', null, 'notlessthan', '100');
			var s = searchRecord('resourceallocation', null, resFilter, col);
			nlapiLogExecution('AUDIT', 'Info', 'rsearch#' + s.length);
			for (var res_count = 0; s != null && res_count < s.length; res_count++) {
					var empid = s[res_count].getValue('resource');
					col = new Array();
					var pro = nlapiLookupField('job', pid, ['custentity_projectmanager','custentity_deliverymanager', 'jobtype']);
					var pm = pro.custentity_projectmanager;
					var dm = pro.custentity_deliverymanager;
					var jobt = pro.jobtype;

					var tapprover = search[countid].getValue('timeapprover');
					var approver = search[countid].getValue('approver');
					var r_mngr = search[countid].getValue('custentity_reportingmanager');
					var employe = nlapiLoadRecord('employee', empid);
					if (empid == pm || empid == dm) {

					}
					else {
						if (tapprover != pm) {
							employe.setFieldValue('timeapprover', pm);
						}
						if (approver != dm) {
							employe.setFieldValue('approver', dm);
						}
						var id = nlapiSubmitRecord(employe);
						nlapiLogExecution('AUDIT', 'Updates', 'empid#' + empid);

					}
			}
		}
		var a_emp_attachment = new Array();
		a_emp_attachment['entity'] = '62082';
		nlapiSendEmail(62082, 'sai.vannamareddy@brillio.com', 'External TA EA Script executed', 'Executed Sucessfully', null, null, a_emp_attachment);
	}
	catch (err) {
		nlapiLogExecution('error', 'Main', err);
		var a_emp_attachment = new Array();
		a_emp_attachment['entity'] = '62082';
		var context = nlapiGetContext();

		var mailTemplate = "";
		mailTemplate += "<p> This is to inform that External TA EA Script has been failed.And It is having Error - <b>[" + err.code + " : " + err.message + "]</b></p>";
		mailTemplate += "<p> Kindly have a look on to the error message and Please do needfull.</p>";
		mailTemplate += "<p><b> Script Id:- " + context.getScriptId() + "</b></p>";
		mailTemplate += "<p><b> Script Deployment Id:- " + context.getDeploymentId() + "</b></p>";

		mailTemplate += "<p><b> Note:</b> This is system genarated email, incase of any failure while sending the notification to employee(s).</p>";
		mailTemplate += "<br/>"
		mailTemplate += "<p>Regards, <br/> Digital Office</p>";
		nlapiSendEmail(442, 'netsuite.support@brillio.com', 'External project Script Execution Failed', mailTemplate, null, null, a_emp_attachment);
	}

}