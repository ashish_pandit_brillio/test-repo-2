// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
/*
   	Script Name:SUT_AIT_Pop_up_TDS_list.js
	Author:		Nikhil jain
	Company:	Aashna Cloudtech Pvt.Ltd.
	Date:		18 Nov 2015
	Description:Display an tds list to be apply on vendor


	Script Modification Log:

	-- Date --			-- Modified By --				--Requested By--				-- Description --



Below is a summary of the process controls enforced by this script file.  The control logic is described
more fully, below, in the appropriate function headers and code blocks.


     SUITELET
		- suiteletFunction(request, response)


     SUB-FUNCTIONS
		- The following sub-functions are called by the above core functions in order to maintain code
            modularization:

               - NOT USED

*/
}
// END SCRIPT DESCRIPTION BLOCK  ====================================



// BEGIN GLOBAL VARIABLE BLOCK  =====================================
{
	//  Initialize any Global Variables, in particular, debugging variables...




}
// END GLOBAL VARIABLE BLOCK  =======================================





// BEGIN SUITELET ==================================================

function suiteletFunction_tds_master_list(request, response){

	/*  Suitelet:
	 - EXPLAIN THE PURPOSE OF THIS FUNCTION
	 -
	 FIELDS USED:
	 --Field Name--				--ID--
	 */
	//  LOCAL VARIABLES
	
	
	//  SUITELET CODE BODY
	if (request.getMethod() == 'GET') 
	{
		
		var tdsList = 'tdslist';
		
		var ved_assessse_code = request.getParameter('ved_assessee_type')
		
		var a_tds_master = request.getParameter('a_tds_master')
		
		var form = nlapiCreateForm('TDS Master List');
		
		var attResult = searchTDS(ved_assessse_code,a_tds_master,request, response);
		
		var sublist1 = form.addSubList(tdsList, 'list',tdsList, 'tab1');
		
		sublist1.addMarkAllButtons()
		
		setSubList(sublist1, form, 1, attResult);
		
		form.addSubmitButton('Submit')
		
		response.writePage(form);
	}
	 else if(request.getMethod() == 'POST')
   	{
   		
		var a_tds_id = new Array();
		
		var i_line_count = request.getLineItemCount('tdslist');
		 nlapiLogExecution('DEBUG','set sublist',' i_line_count'+i_line_count)
		 
		for(var k =1;k<=i_line_count;k++)
		{
			
			var apply = request.getLineItemValue('tdslist','custrecord_apply',k)
			
			 nlapiLogExecution('DEBUG','set sublist','apply' + apply)
			if(apply == 'T') 
			{
				var internalid = request.getLineItemValue('tdslist','internalid',k)
				 nlapiLogExecution('DEBUG','set sublist',' internalid'+internalid)
		 
				a_tds_id.push(internalid)
			}
		}
   		response.write('<html><head><script>window.opener.setTDSRelationLine( "'+a_tds_id+'");self.close();</script></head><body></body></html>')
		 nlapiLogExecution('DEBUG','set sublist','after repsonse write')
   	}
}

// END SUITELET ====================================================




// BEGIN OBJECT CALLED/INVOKING FUNCTION ===================================================
function searchTDS(ved_assessse_code,a_tds_master,request, response)
{
	var filters = new Array();
	var column = new Array();
	
	 nlapiLogExecution('DEBUG','searchTDS','ved_assessse_code'+ved_assessse_code)
	 nlapiLogExecution('DEBUG','searchTDS','a_tds_master'+a_tds_master)
	  
	if (ved_assessse_code != null && ved_assessse_code != '' && ved_assessse_code != undefined) 
	{
		filters.push(new nlobjSearchFilter('custrecord_assessecode', null, 'is', ved_assessse_code))
	}
	if (a_tds_master != null && a_tds_master != '' && a_tds_master != undefined) 
	{
		var array_tds_master = a_tds_master.split(',');
		filters.push(new nlobjSearchFilter('internalid', null, 'noneOf',array_tds_master))
	}
	//=== Begin:Select the Fields from TDS Master Record. ===
	column.push(new nlobjSearchColumn('internalid'));
	column.push(new nlobjSearchColumn('name'));
	column.push(new nlobjSearchColumn('custrecord_tdstype'));
	column.push(new nlobjSearchColumn('custrecord_assessecode'));
	column.push(new nlobjSearchColumn('custrecord_section'));
	column.push(new nlobjSearchColumn('custrecord_paymentcode'));
	column.push(new nlobjSearchColumn('custrecord_tdsthreshold'));
	column.push(new nlobjSearchColumn('custrecord_cumulativethreshold'));
	column.push(new nlobjSearchColumn('custrecord_tdsaccount'));
	column.push(new nlobjSearchColumn('custrecord_netper'));
	column.push(new nlobjSearchColumn('custrecord_datetds'));
	column.push(new nlobjSearchColumn('custrecord_tdsitem'));
	
	column.push(new nlobjSearchColumn('custrecord_tdsrecievable_account'));
	column.push(new nlobjSearchColumn('custrecord_tdsrecievable_item'));
	
	column.push(new nlobjSearchColumn('custrecord_ao_concession'));
	column.push(new nlobjSearchColumn('custrecord_empty_pan_tdsper'));
	
	
	var results = nlapiSearchRecord('customrecord_tdsmaster', null, filters, column);
	if (results != null) 
	{
		return results;
	}
}
function setSubList(sublist, form,isForm,results,request,response)
{

              // === Add fields to subList === 
			 nlapiLogExecution('DEBUG','set sublist','Display list')
			 // sublist.addField('custrecord_tds_master_edit','text','EDIT')
			 
			  sublist.addField('custrecord_apply','checkbox','Apply')
			  
			  var internalidField = sublist.addField('internalid','text','InternalId')
			 internalidField.setDisplayType('hidden');
			 
	          sublist.addField('custrecord_datetds','date','Creation Date')
			  
			  sublist.addField('name','text','Name')
			  
	          var type = sublist.addField('custrecord_tdstype_display','select','Type','customrecord_tdstype')
			  type.setDisplayType('inline')
			  nlapiLogExecution('DEBUG','set sublist','Display list')
	          var assessee_code = sublist.addField('custrecord_assessecode_display','text','Assessee Code')
			  assessee_code.setDisplayType('inline')
			  
	          sublist.addField('custrecord_section','text','Section')
	          sublist.addField('custrecord_paymentcode','text','Payment Code')
	          sublist.addField('custrecord_tdsthreshold','currency','TDS Threshold')
	          sublist.addField('custrecord_cumulativethreshold','currency','Surcharge Threshold')
			  
	          var tds_payableac = sublist.addField('custrecord_tdsaccount_display','select','TDS Payable Account','account')
			  tds_payableac.setDisplayType('inline')
			  
	          var payable_item = sublist.addField('custrecord_tdsitem_display','select','TDS Payable Item','item')
			  
			  payable_item.setDisplayType('inline')
			  
	          sublist.addField('custrecord_netper','percent','Net%')
			  
			  var recivable_ac = sublist.addField('custrecord_tdsrecievable_account_display','select','TDS Receivable Account','account')
			  recivable_ac.setDisplayType('inline')
			  
			  var recivable_item = sublist.addField('custrecord_tdsrecievable_item_display','select','TDS Receivable Item','item')
			  recivable_item.setDisplayType('inline')
			  
			  var ao_conecssion_type = sublist.addField('custrecord_ao_concession','checkbox','AO Concession Type')
			  ao_conecssion_type.setDisplayType('Disabled')
			  sublist.addField('custrecord_empty_pan_tdsper','percent','PAN No. Empty TDS% Rate')



	         // === set the sublist ===
	         
			if (results != null && results != '' && results != undefined) 
			{
				var j = 0;
				for (var i = 0; i < results.length; i++) {
					j = j + 1;
					
					var internal_id = results[i].getValue('internalid')
					
					//var record_link = nlapiResolveURL('RECORD', 'customrecord_tdsmaster', internal_id, 'edit')
					
					//var edit_url = "<a href=" + record_link + " target='_blank'>Edit</a>"
					
					//sublist.setLineItemValue('custrecord_tds_master_edit', j, edit_url)
					
					sublist.setLineItemValue('internalid', j, internal_id)
					
					var custrecord_datetds = results[i].getValue('custrecord_datetds')
					sublist.setLineItemValue('custrecord_datetds', j, custrecord_datetds)
					
					var name = results[i].getValue('name')
					sublist.setLineItemValue('name', j, name)
					
					var custrecord_tdstype_display = results[i].getValue('custrecord_tdstype')
					sublist.setLineItemValue('custrecord_tdstype_display', j, custrecord_tdstype_display)
					
					var custrecord_assessecode_display = results[i].getText('custrecord_assessecode')
					sublist.setLineItemValue('custrecord_assessecode_display', j, custrecord_assessecode_display)
					
					var custrecord_section = results[i].getValue('custrecord_section')
					sublist.setLineItemValue('custrecord_section', j, custrecord_section)
					
					var custrecord_paymentcode = results[i].getValue('custrecord_paymentcode')
					sublist.setLineItemValue('custrecord_paymentcode', j, custrecord_paymentcode)
					
					var custrecord_tdsthreshold = results[i].getValue('custrecord_tdsthreshold')
					sublist.setLineItemValue('custrecord_tdsthreshold', j, custrecord_tdsthreshold)
					
					var custrecord_cumulativethreshold = results[i].getValue('custrecord_cumulativethreshold')
					sublist.setLineItemValue('custrecord_cumulativethreshold', j, custrecord_cumulativethreshold)
					
					var custrecord_tdsaccount_display = results[i].getValue('custrecord_tdsaccount')
					sublist.setLineItemValue('custrecord_tdsaccount_display', j, custrecord_tdsaccount_display)
					
					var custrecord_tdsitem_display = results[i].getValue('custrecord_tdsitem')
					sublist.setLineItemValue('custrecord_tdsitem_display', j, custrecord_tdsitem_display)
					
					var custrecord_netper = results[i].getValue('custrecord_netper')
					sublist.setLineItemValue('custrecord_netper', j, custrecord_netper)
					
					var custrecord_tdsrecievable_account_display = results[i].getValue('custrecord_tdsrecievable_account')
					sublist.setLineItemValue('custrecord_tdsrecievable_account_display', j, custrecord_tdsrecievable_account_display)
					
					var custrecord_tdsrecievable_item_display = results[i].getValue('custrecord_tdsrecievable_item')
					sublist.setLineItemValue('custrecord_tdsrecievable_item_display', j, custrecord_tdsrecievable_item_display)
					
					var custrecord_ao_concession = results[i].getValue('custrecord_ao_concession')
					sublist.setLineItemValue('custrecord_ao_concession', j, custrecord_ao_concession)
					
					var custrecord_empty_pan_tdsper = results[i].getValue('custrecord_empty_pan_tdsper')
					sublist.setLineItemValue('custrecord_empty_pan_tdsper', j, custrecord_empty_pan_tdsper)
						
				}
			}
			
	return sublist;
}
// END OBJECT CALLED/INVOKING FUNCTION =====================================================
