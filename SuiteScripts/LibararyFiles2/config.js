var config = {
  "admin" : {
    "information.systems@brillio.com":true,
      "deepak.srinivas@brillio.com" : true,
     "Ananda.Hirekerur@brillio.com" : true
  } 
}
function isArrayEmpty(argArray) {

	return !isArrayNotEmpty(argArray);
}
function isArrayNotEmpty(argArray) {

	return (isNotEmpty(argArray) && argArray.length > 0);
}
function isEmpty(value) {

	return value == null || value == "" || typeof (value) == undefined;
}

function isNotEmpty(value) {

	return !isEmpty(value);
}

function isTrue(value) {

	return value == 'T';
}

function isFalse(value) {

	return value != 'T';
}