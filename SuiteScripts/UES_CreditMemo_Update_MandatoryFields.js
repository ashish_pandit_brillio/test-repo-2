//https://system.na1.netsuite.com/app/common/scripting/script.nl?id=940
// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
    /*
       	Description: This script will update SBC lines MEMO field as per brillio standard memo on vendor bill.
    				 These are populated through AIT script. Need for this script was, we can't edit AIT script hence new script created.


    	Script Modification Log:

    	-- Date --			-- Modified By --				--Requested By--				-- Description --



    Below is a summary of the process controls enforced by this script file.  The control logic is described
    more fully, below, in the appropriate function headers and code blocks.


         BEFORE LOAD
    		- beforeLoadRecord(type)



         BEFORE SUBMIT
    		- beforeSubmitRecord(type)


         AFTER SUBMIT
    		- afterSubmit_UpdateMemo(type)



         SUB-FUNCTIONS
    		- The following sub-functions are called by the above core functions in order to maintain code
                modularization:

                   - NOT USED

    */
}
// END SCRIPT DESCRIPTION BLOCK  ====================================



// BEGIN GLOBAL VARIABLE BLOCK  =====================================
{
    //  Initialize any Global Variables, in particular, debugging variables...




}
// END GLOBAL VARIABLE BLOCK  =======================================




// BEGIN BEFORE LOAD ==================================================

function beforeLoadRecord(type) {

    /*  On before load:

          - EXPLAIN THE PURPOSE OF THIS FUNCTION

		-


		FIELDS USED:

          --Field Name--				--ID--


	*/



    //  LOCAL VARIABLES

    //  BEFORE LOAD CODE BODY


    return true;



}

// END BEFORE LOAD ====================================================


// BEGIN BEFORE SUBMIT ================================================

function beforeSubmitRecord(type) {
    /*  On before submit:

          - PURPOSE
		-

          FIELDS USED:

          --Field Name--				--ID--

    */

    //  LOCAL VARIABLES




    //  BEFORE SUBMIT CODE BODY


    return true;
}

// END BEFORE SUBMIT ==================================================


// BEGIN AFTER SUBMIT =============================================

function afterSubmit_UpdateMemo(type) {
    /*  On after submit:

          - PURPOSE
		-

	FIELDS USED:

          --Field Name--				--ID--

    */

    //  LOCAL VARIABLES

    var submit_record_flag = 0;
    var context = nlapiGetContext();

    //  AFTER SUBMIT CODE BODY

    try {
         /*----------Added by Koushalya 28/12/2021---------*/
         var filters_customrecord_revenue_location_subsidiarySearch=["custrecord_offsite_onsite","anyof","2"];
         var obj_rev_loc_subSrch= Search_revenue_Location_subsdidary(filters_customrecord_revenue_location_subsidiarySearch);
         var obj_offsite_onsite=obj_rev_loc_subSrch["Onsite/Offsite"];
          /*-------------------------------------------------*/
 
        if (type == 'create' || type == 'edit') {

            var vendor_bill_rcrd = nlapiLoadRecord(nlapiGetRecordType(), nlapiGetRecordId());
            try {
                var transactionNum = vendor_bill_rcrd.getFieldValue('tranid');
                var transaction_internal = nlapiGetRecordId();

                var item_line_count = vendor_bill_rcrd.getLineItemCount('item');
                var excel_file_obj = '';
                var err_row_excel = '';
                var strVar_excel = '';

                strVar_excel += '<table>';
                strVar_excel += '	<tr>';
                strVar_excel += ' <td width="100%">';
                strVar_excel += '<table width="100%" border="1">';
                strVar_excel += '	<tr>';
                strVar_excel += ' <td width="6%" font-size="11" align="center">Transaction Type</td>';
                strVar_excel += ' <td width="6%" font-size="11" align="center">Transaction Number</td>';
                strVar_excel += ' <td width="6%" font-size="11" align="center">Transaction Internal ID</td>';
                strVar_excel += ' <td width="6%" font-size="11" align="center">Error Message</td>';
                strVar_excel += '	</tr>';

                for (var d = 1; d <= item_line_count; d++) {
                    var emp_type = '';
                    var person_type = '';
                    var onsite_offsite = '';
                    var cust_full_name_with_id = '';
                    var core_practice = '';
                    var emp_fusion_id = '';
                    var emp_frst_name = '';
                    var emp_middl_name = '';
                    var emp_lst_name = '';
                    var emp_full_name = '';
                    var emp_practice = '';
                    var emp_practice_text = '';
                    var s_proj_desc_split = '';
                    var s_employee_name_split = '';
                    var proj_name_selected = '';
                    var s_employee_name = '';
                    var s_employee_id = '';
                    var s_employee_name_id = '';
                    var proj_name_selected = '';
                    var project_entity_id = '';
                    var s_proj_desc_id = '';
                    var existing_practice = '';
                    var proj_full_name_with_id = '';
                    var employee_with_id = '';
                    var proj_category_val = '';
                    var misc_practice = '';
                    var parent_practice = '';
                    var project_region = '';

                    existing_practice = vendor_bill_rcrd.getLineItemValue('item', 'department', d);
                    s_employee_name = vendor_bill_rcrd.getLineItemValue('item', 'custcol_employeenamecolumn', d);
                    s_employee_id = vendor_bill_rcrd.getLineItemValue('item', 'custcol_employee_entity_id', d);
                    if (_logValidation(s_employee_id)) {
                        s_employee_name_split = s_employee_id;
                        nlapiLogExecution('audit', 's_employee_name_split item', s_employee_name_split);
                    } else if (_logValidation(s_employee_name)) {
                        s_employee_name_id = s_employee_name.split('-');
                        s_employee_name_split = s_employee_name_id[0];
                        nlapiLogExecution('audit', 's_employee_name', s_employee_name);
                    }
                    if (_logValidation(s_employee_name) || _logValidation(s_employee_id)) {
                        //var s_employee_name_id = emp_name_selected.split('-');
                        //var s_employee_name_split = s_employee_name_id[0];

                        var filters_emp = new Array();
                        filters_emp[0] = new nlobjSearchFilter('entityid', null, 'contains', s_employee_name_split);
                        var column_emp = new Array();
                        column_emp[0] = new nlobjSearchColumn('custentity_persontype');
                        column_emp[1] = new nlobjSearchColumn('employeetype');
                        column_emp[2] = new nlobjSearchColumn('subsidiary');
                        column_emp[3] = new nlobjSearchColumn('custentity_fusion_empid');
                        column_emp[4] = new nlobjSearchColumn('firstname');
                        column_emp[5] = new nlobjSearchColumn('middlename');
                        column_emp[6] = new nlobjSearchColumn('lastname');
                        column_emp[7] = new nlobjSearchColumn('department');
                        column_emp[8] = new nlobjSearchColumn('location');
                        column_emp[9] = new nlobjSearchColumn('custentity_legal_entity_fusion');
                        var a_results_emp = nlapiSearchRecord('employee', null, filters_emp, column_emp);
                        nlapiLogExecution('audit', 'a_results_emp:-- ', a_results_emp);
                        if (_logValidation(a_results_emp)) {
                            var emp_id = a_results_emp[0].getId();
                            var emp_type = a_results_emp[0].getText('employeetype');
                            nlapiLogExecution('audit', 'emp_type:-- ', emp_type);
                            if (!_logValidation(emp_type)) {
                                emp_type = '';
                            }
                            var person_type = a_results_emp[0].getText('custentity_persontype');
                            nlapiLogExecution('audit', 'person_type:-- ', person_type);
                            if (!_logValidation(person_type)) {
                                person_type = '';
                            }

                            nlapiLogExecution('audit', 'person_type:-- ', person_type);
                            var emp_subsidiary = a_results_emp[0].getValue('subsidiary');
                            person_type = a_results_emp[0].getText('custentity_persontype');
                            emp_fusion_id = a_results_emp[0].getValue('custentity_fusion_empid');
                            emp_practice = a_results_emp[0].getValue('department');
                            emp_practice_text = a_results_emp[0].getText('department');
                            emp_frst_name = a_results_emp[0].getValue('firstname');
                            emp_middl_name = a_results_emp[0].getValue('middlename');
                            emp_lst_name = a_results_emp[0].getValue('lastname');
                            if (!_logValidation(emp_fusion_id)) {
                                emp_fusion_id = '';
                            }
                            nlapiLogExecution('audit', 'emp_fusion_id:-- ', emp_fusion_id);
                            if (emp_frst_name)
                                emp_full_name = emp_frst_name;

                            if (emp_middl_name)
                                emp_full_name = emp_full_name + ' ' + emp_middl_name;

                            if (emp_lst_name)
                                emp_full_name = emp_full_name + ' ' + emp_lst_name;
                            if (_logValidation(emp_subsidiary)) {
                                if (emp_subsidiary == 3 && a_results_emp[0].getValue('custentity_legal_entity_fusion') == 'Brillio Technologies Private Limited UK') {
                                    onsite_offsite = 'Onsite';
                                }
                                else{
                                    onsite_offsite = obj_offsite_onsite[emp_subsidiary]?'Offshore':'Onsite';    
                                }
                                /* else if (emp_subsidiary == 3 || emp_subsidiary == 9 || emp_subsidiary == 23) {
                                    onsite_offsite = 'Offsite';
                                } else {
                                    onsite_offsite = 'Onsite';
                                }*/
                            }
                            if (emp_practice) {
                                var is_practice_active_e = nlapiLookupField('department', parseInt(emp_practice), ['isinactive', 'custrecord_is_delivery_practice']);
                                var isinactive_Practice_e = is_practice_active_e.isinactive;
                                nlapiLogExecution('debug', 'isinactive_Practice_e', isinactive_Practice_e);
                                core_practice = is_practice_active_e.custrecord_is_delivery_practice;
                                nlapiLogExecution('debug', 'core_practice', core_practice);
                            }
                            if (emp_fusion_id)
                                employee_with_id = emp_fusion_id + '-' + emp_full_name;


                            vendor_bill_rcrd.selectLineItem('item', d);
                            vendor_bill_rcrd.setCurrentLineItemValue('item', 'custcol_employeenamecolumn', employee_with_id);

                            vendor_bill_rcrd.setCurrentLineItemValue('item', 'custcol_employee_type', emp_type);
                            vendor_bill_rcrd.setCurrentLineItemValue('item', 'custcol_person_type', person_type);
                            vendor_bill_rcrd.setCurrentLineItemValue('item', 'custcol_onsite_offsite', onsite_offsite);
                            vendor_bill_rcrd.setCurrentLineItemValue('item', 'custcol_employee_entity_id', emp_fusion_id);
                            vendor_bill_rcrd.setCurrentLineItemValue('item', 'custcol_emp_name_on_a_click_report', emp_full_name);
                            if (existing_practice) {} else {
                                vendor_bill_rcrd.setCurrentLineItemValue('item', 'department', emp_practice);
                            }
                            vendor_bill_rcrd.commitLineItem('item');

                            submit_record_flag = 1;
                        }

                    }

                    proj_name_selected = vendor_bill_rcrd.getLineItemValue('item', 'custcolprj_name', d);
                    project_entity_id = vendor_bill_rcrd.getLineItemValue('item', 'custcol_project_entity_id', d);
                    if (project_entity_id) {
                        s_proj_desc_split = project_entity_id;
                        nlapiLogExecution('audit', 's_proj_desc_split', s_proj_desc_split);
                    } else if (proj_name_selected) {
                        s_proj_desc_id = proj_name_selected.substr(0, 9);
                        s_proj_desc_split = s_proj_desc_id;
                        nlapiLogExecution('audit', 'proj_name_selected', proj_name_selected);
                    }
                    //if (_logValidation(proj_name_selected))
                    //{
                    //	proj_full_name = proj_name_selected.toString();
                    //	proj_id_array = proj_full_name.split(' ');
                    //	proj_id = proj_id_array[0];
                    if (_logValidation(s_proj_desc_split)) {
                        var filters_search_proj = new Array();
                        filters_search_proj[0] = new nlobjSearchFilter('entityid', null, 'is', s_proj_desc_split);
                        var column_search_proj = new Array();
                        column_search_proj[0] = new nlobjSearchColumn('jobbillingtype');
                        column_search_proj[1] = new nlobjSearchColumn('customer');
                        column_search_proj[2] = new nlobjSearchColumn('custentity_region', 'customer');
                        column_search_proj[3] = new nlobjSearchColumn('entityid');
                        column_search_proj[4] = new nlobjSearchColumn('altname');
                        column_search_proj[5] = new nlobjSearchColumn('custentity_project_allocation_category');
                        column_search_proj[6] = new nlobjSearchColumn('companyname', 'customer');
                        column_search_proj[7] = new nlobjSearchColumn('entityid', 'customer');
                        column_search_proj[8] = new nlobjSearchColumn('custentity_vertical');
                        column_search_proj[9] = new nlobjSearchColumn('territory', 'customer');
                        column_search_proj[10] = new nlobjSearchColumn('custentity_practice');
                        column_search_proj[11] = new nlobjSearchColumn('custentity_region');
                        column_search_proj[12] = new nlobjSearchColumn('custrecord_parent_practice', 'custentity_practice');

                        var search_proj_results = nlapiSearchRecord('job', null, filters_search_proj, column_search_proj);
                        if (_logValidation(search_proj_results)) {
                            var proj_billing_type = search_proj_results[0].getText('jobbillingtype');
                            var i_region_id = search_proj_results[0].getText('custentity_region');
                            var i_proj_rcrd = nlapiLoadRecord('job', search_proj_results[0].getId());
                            var i_proj_name = i_proj_rcrd.getFieldValue('altname');
                            var i_proj_entity_id = search_proj_results[0].getValue('entityid');
                            var i_proj_category = search_proj_results[0].getText('custentity_project_allocation_category');
                            var i_proj_vertical = search_proj_results[0].getValue('custentity_vertical');
                            var i_proj_practice = search_proj_results[0].getValue('custentity_practice');
                            proj_category_val = search_proj_results[0].getValue('custentity_project_allocation_category');

                            var i_cust_name = search_proj_results[0].getValue('companyname', 'customer');
                            var i_cust_entity_id = search_proj_results[0].getValue('entityid', 'customer');
                            var i_cust_territory = search_proj_results[0].getValue('territory', 'customer');
                            parent_practice = search_proj_results[0].getText('custrecord_parent_practice', 'custentity_practice');
                            if (!_logValidation(proj_billing_type))
                                proj_billing_type = '';

                            vendor_bill_rcrd.selectLineItem('item', d);
                            vendor_bill_rcrd.setCurrentLineItemValue('item', 'custcol_billing_type', proj_billing_type);
                            vendor_bill_rcrd.setCurrentLineItemValue('item', 'custcol_project_entity_id', s_proj_desc_split);

                            var proj_cust = search_proj_results[0].getText('customer');
                            if (_logValidation(proj_cust)) {
                                var cust_entity_id = proj_cust.split(' ');
                                cust_entity_id = cust_entity_id[0];
                                vendor_bill_rcrd.setCurrentLineItemValue('item', 'custcol_customer_entityid', cust_entity_id);
                            }
                            if (s_employee_name_split || existing_practice) {} else {
                                vendor_bill_rcrd.setCurrentLineItemValue('item', 'department', i_proj_practice);
                            }
                            if (i_proj_entity_id)
                                proj_full_name_with_id = i_proj_entity_id + ' ' + i_proj_name;
                            if (proj_name_selected) {} else {
                                vendor_bill_rcrd.setCurrentLineItemValue('item', 'custcolprj_name', proj_full_name_with_id);
                            }
                            if (cust_entity_id)
                                cust_full_name_with_id = cust_entity_id + ' ' + i_cust_name;

                            //
                            if (proj_category_val) {
                                if ((parseInt(proj_category_val) == parseInt(1)) && (core_practice == 'T')) {
                                    misc_practice = emp_practice;
                                    misc_practice = emp_practice_text;
                                } else {
                                    misc_practice = search_proj_results[0].getValue('custentity_practice');
                                    var is_practice_active = nlapiLookupField('department', parseInt(misc_practice), ['isinactive']);
                                    isinactive_Practice_e = is_practice_active.isinactive;
                                    misc_practice = search_proj_results[0].getText('custentity_practice');
                                }
                            }
                            nlapiLogExecution('audit', 'Pratice', 'misc_practice:' + misc_practice);
                            if (misc_practice && isinactive_Practice_e == 'F') {
                                vendor_bill_rcrd.setCurrentLineItemValue('item', 'custcol_mis_practice', misc_practice);
                            }
                            //
                            var cust_region = search_proj_results[0].getValue('custentity_region', 'customer');

                            vendor_bill_rcrd.setCurrentLineItemValue('item', 'custcolcustcol_temp_customer', cust_full_name_with_id);
                            vendor_bill_rcrd.setCurrentLineItemValue('item', 'custcol_region_master_setup', cust_region);
                            vendor_bill_rcrd.setCurrentLineItemValue('item', 'custcol_cust_name_on_a_click_report', i_cust_name);
                            vendor_bill_rcrd.setCurrentLineItemValue('item', 'custcol_proj_name_on_a_click_report', i_proj_name);
                            vendor_bill_rcrd.setCurrentLineItemValue('item', 'custcol_project_region', i_region_id);
                            vendor_bill_rcrd.setCurrentLineItemValue('item', 'custcol_proj_category_on_a_click', i_proj_category);
                            vendor_bill_rcrd.setCurrentLineItemValue('item', 'custcol_territory', i_cust_territory);
                            vendor_bill_rcrd.setCurrentLineItemValue('item', 'custcol_parent_executing_practice', parent_practice);
                            vendor_bill_rcrd.commitLineItem('item');

                            submit_record_flag = 1;
                        }
                    }
                    //}


                }

                if (submit_record_flag == 1) {
                    var vendor_bill_submitted_id = nlapiSubmitRecord(vendor_bill_rcrd, true, true);
                    nlapiLogExecution('debug', 'submitted bill id:-- ', vendor_bill_submitted_id);
                }
            } catch (err) {
                //Added try-catch logic by Sitaram 06/08/2021

                nlapiLogExecution('DEBUG', 'Error message', err);
                err_row_excel += '	<tr>';
                err_row_excel += ' <td width="6%" font-size="11" align="center">' + nlapiGetRecordType() + '</td>';
                err_row_excel += ' <td width="6%" font-size="11" align="center">' + transactionNum + '</td>';
                err_row_excel += ' <td width="6%" font-size="11" align="center">' + transaction_internal + '</td>';
                err_row_excel += ' <td width="6%" font-size="11" align="center">' + err.message + '</td>';

                err_row_excel += '	</tr>';


            }
        }
        if (_logValidation(err_row_excel)) {
            var tailMail = '';
            tailMail += '</table>';
            tailMail += ' </td>';
            tailMail += '</tr>';
            tailMail += '</table>';

            strVar_excel = strVar_excel + err_row_excel + tailMail
            //excel_file_obj = generate_excel(strVar_excel);
            var mailTemplate = "";
            mailTemplate += '<html>';
            mailTemplate += '<body>';
            mailTemplate += "<p> This is to inform that following transactions are having Errors and therefore, system is not able to set the project/MIS mandatory fields</p>";
            mailTemplate += "<p> Please make sure, transaction is updated with proper details else may not able to see the project/customer/employee details at Income Statement level.</p>";
            mailTemplate += "<br/>"
            mailTemplate += "<p><b> Script Id:- " + context.getScriptId() + "</b></p>";
            mailTemplate += "<p><b> Script Deployment Id:- " + context.getDeploymentId() + "</b></p>";
            mailTemplate += "<br/>"
            mailTemplate += strVar_excel
            mailTemplate += "<br/>"
            mailTemplate += "<p>Regards, <br/> Information Systems</p>";
            mailTemplate += '</body>';
            mailTemplate += '</html>';
            //nlapiSendEmail(442, 'billing@brillio.com', 'Issue with updating the MIS data for the transactions', mailTemplate, 'netsuite.support@brillio.com', null, null, null);

        }
    } catch (e) {
        nlapiLogExecution('ERROR', 'ERROR MESSAGE:-- ', e);
    }

    return true;

}

// END AFTER SUBMIT ===============================================



// BEGIN FUNCTION ===================================================
{

    function _logValidation(value) {
        if (value != null && value.toString() != null && value != '' && value != undefined && value.toString() != undefined && value != 'undefined' && value.toString() != 'undefined' && value.toString() != 'NaN' && value != NaN) {
            return true;
        } else {
            return false;
        }
    }

}


function generate_excel(strVar_excel) {
    var strVar1 = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:ss="urn:schemas-microsoft-com:office:spreadsheet" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40">' +
        '<head>' +
        '<meta http-equiv=Content-Type content="text/html; charset=windows-1252"/>' +
        '<meta name=ProgId content=Excel.Sheet/>' +
        '<meta name=Generator content="Microsoft Excel 11"/>' +
        '<!--[if gte mso 9]><xml>' +
        '<x:excelworkbook>' +
        '<x:excelworksheets>' +
        '<x:excelworksheet=sheet1>' +
        '<x:name>** ESTIMATE FILE**</x:name>' +
        '<x:worksheetoptions>' +
        '<x:selected></x:selected>' +
        '<x:freezepanes></x:freezepanes>' +
        '<x:frozennosplit></x:frozennosplit>' +
        '<x:splithorizontal>** FROZEN ROWS + 1 **</x:splithorizontal>' +
        '<x:toprowbottompane>** FROZEN ROWS + 1 **</x:toprowbottompane>' +
        '<x:splitvertical>** FROZEN COLUMNS + 1 **</x:splitvertical>' +
        '<x:leftcolumnrightpane>** FROZEN COLUMNS + 1**</x:leftcolumnrightpane>' +
        '<x:activepane>0</x:activepane>' + // 0
        '<x:panes>' +
        '<x:pane>' +
        '<x:number>3</x:number>' +
        '</x:pane>' +
        '<x:pane>' +
        '<x:number>1</x:number>' +
        '</x:pane>' +
        '<x:pane>' +
        '<x:number>2</x:number>' +
        '</x:pane>' +
        '<x:pane>' +
        '<x:number>0</x:number>' + //1
        '</x:pane>' +
        '</x:panes>' +
        '<x:protectcontents>False</x:protectcontents>' +
        '<x:protectobjects>False</x:protectobjects>' +
        '<x:protectscenarios>False</x:protectscenarios>' +
        '</x:worksheetoptions>' +
        '</x:excelworksheet>' +
        '</x:excelworksheets>' +
        '<x:protectstructure>False</x:protectstructure>' +
        '<x:protectwindows>False</x:protectwindows>' +
        '</x:excelworkbook>' +

        // '<style>'+

        //-------------------------------------
        '</x:excelworkbook>' +
        '</xml><![endif]-->' +
        '<style>' +
        'p.MsoFooter, li.MsoFooter, div.MsoFooter' +
        '{ margin:0in; margin-bottom:.0001pt; mso-pagination:widow-orphan; tab-stops:center 3.0in right 6.0in; font-size:12.0pt;}' +
        '<style>' +

        '<!-- /* Style Definitions */' +

        //'@page Section1'+
        //'{ size:8.5in 11.0in; margin:1.9cm 1.27cm 1.9cm 1.27cm; mso-header-margin:.5in; mso-footer-margin:.5in; mso-title-page:yes; mso-header: h1; mso-footer: f1; mso-first-header: fh1; mso-first-footer: ff1; mso-paper-source:0;}'+

        'div.Section1' +
        '{ page:Section1;}' +

        'table#hrdftrtbl' +
        '{ margin:0in 0in 0in 900in; width:1px; height:1px; overflow:hidden; font-family:Arial;}-->' +

        '</style>' +


        '</head>' +


        '<body>' +

        '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">'

    var strVar2 = strVar_excel;
    strVar1 = strVar1 + strVar2;
    var file = nlapiCreateFile('Resource Allocation Data.xls', 'XMLDOC', strVar1);
    return file;
}
// END FUNCTION =====================================================