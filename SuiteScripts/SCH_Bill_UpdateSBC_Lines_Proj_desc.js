// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
/*
   	Script Name: SCH_Bill_UpdateSBC_Lines_Proj_desc.js
	Author: Jayesh V Dinde
	Company: Aashna CLoudtech PVT LTD.
	Date: 22 July 2016
	Description: This script will update SBC lines Proj desc etc fields as per brillio standard memo on vendor bill.


	Script Modification Log:

	-- Date --			-- Modified By --				--Requested By--				-- Description --



Below is a summary of the process controls enforced by this script file.  The control logic is described
more fully, below, in the appropriate function headers and code blocks.


     SUB-FUNCTIONS
		- The following sub-functions are called by the above core functions in order to maintain code
            modularization:

               - NOT USED

*/
}
// END SCRIPT DESCRIPTION BLOCK  ====================================

function Sch_Update_Bill()
{
	try
	{
		var submit_record_flag = 0;
		var o_context = nlapiGetContext();
		
		var bill_already_processed_list = new Array();
		var a_results_bill = nlapiSearchRecord('transaction', 'customsearch1454', null, null);
		if (_logValidation(a_results_bill))
		{
			for (var index_count = 0; index_count < a_results_bill.length; index_count++)
			{
				if(o_context.getRemainingUsage() <= 2000)
				{
					nlapiYieldScript();
				}
				
				if(bill_already_processed_list.indexOf(a_results_bill[index_count].getId())>=0)
				{
					
				}
				else
				{
					var vendor_bill_rcrd = nlapiLoadRecord('vendorbill',a_results_bill[index_count].getId());
				
					var is_subtier_bill = vendor_bill_rcrd.getFieldValue('custbody_is_subtier_bill');
			nlapiLogExecution('debug','is_subtier_bill:-- '+is_subtier_bill,nlapiGetRecordId());
			if (is_subtier_bill == 'T')
			{
				var memo_body_level = vendor_bill_rcrd.getFieldValue('memo');
					
				var expense_line_count = vendor_bill_rcrd.getLineItemCount('expense');
				
				for (var j = 1; j <= expense_line_count; j++)
				{
					var amount = vendor_bill_rcrd.getLineItemValue('expense', 'amount', j);
					var taxAmount = parseFloat(amount) * (parseFloat(0.5) / 100);
					//taxAmount = applySTRoundMethod(stRoundMethod, taxAmount);
					var is_swach_cess_reverse_true_mainLine = vendor_bill_rcrd.getLineItemValue('expense', 'custcol_ait_swachh_cess_reverse', j);
					for(var q=j; q<= expense_line_count; q++)
					{
						var is_swach_cess_reverse_true = vendor_bill_rcrd.getLineItemValue('expense', 'custcol_ait_swachh_cess_reverse', q);
						if (is_swach_cess_reverse_true == 'T')
						{
							var taxAmount = parseFloat(amount) * (parseFloat(0.5) / 100);
							taxAmount = taxAmount.toString();
							var taxAmount_toCompare = taxAmount.split('.');
							taxAmount_toCompare = taxAmount_toCompare[0];
							//taxAmount = applySTRoundMethod(stRoundMethod, taxAmount);
							
							var amount_sbc = vendor_bill_rcrd.getLineItemValue('expense', 'amount', q);
							amount_sbc = amount_sbc.toString();
							var amount_sbc_toCompare = amount_sbc.split('.');
							amount_sbc_toCompare = amount_sbc_toCompare[0];
							
							if(parseFloat(amount_sbc_toCompare) == parseFloat(taxAmount_toCompare))
							{
								var emp_type = '';
								var person_type = '';
								var onsite_offsite = '';
								var proj_billing_type = '';
								var i_emp_full_name ='';
								var proj_id_V = ''; 
								
								var cust_id = vendor_bill_rcrd.getLineItemValue('expense', 'custcol_customer_entityid', j);
								var proj_id = vendor_bill_rcrd.getLineItemValue('expense', 'custcol_project_entity_id', j);
								var emp_id = vendor_bill_rcrd.getLineItemValue('expense', 'custcol_employee_entity_id', j);
								var proj_desc = vendor_bill_rcrd.getLineItemValue('expense', 'custcolprj_name', j);
								if(proj_desc){
								var pro_entity_id = proj_desc.split(' ');
								proj_id_V = pro_entity_id[0];
								}
								else if(proj_id){
								proj_id_V = proj_id;
								}

								if (!_logValidation(emp_id))
											{
												emp_id = '';
											}
								
								
								vendor_bill_rcrd.selectLineItem('expense', q);
								vendor_bill_rcrd.setCurrentLineItemValue('expense', 'memo', 'SBC-' + memo_body_level);
								vendor_bill_rcrd.setCurrentLineItemValue('expense', 'custcol_customer_entityid', cust_id);
								vendor_bill_rcrd.setCurrentLineItemValue('expense', 'custcol_project_entity_id', proj_id_V);
								vendor_bill_rcrd.setCurrentLineItemValue('expense', 'custcol_employee_entity_id', emp_id);	
								vendor_bill_rcrd.setCurrentLineItemValue('expense', 'custcolprj_name', proj_desc);									
								vendor_bill_rcrd.commitLineItem('expense');
								
								submit_record_flag = 1;
							}
							else
							{
								if(taxAmount_toCompare > parseFloat(0))
									taxAmount_toCompare = '-'+taxAmount_toCompare;
								
								if(parseFloat(amount_sbc_toCompare) == parseFloat(taxAmount_toCompare))
								{
									var emp_type = '';
								var person_type = '';
								var onsite_offsite = '';
								var proj_billing_type = '';
								var i_emp_full_name ='';
								var proj_id_V = ''; 
								
								var cust_id = vendor_bill_rcrd.getLineItemValue('expense', 'custcol_customer_entityid', j);
								var proj_id = vendor_bill_rcrd.getLineItemValue('expense', 'custcol_project_entity_id', j);
								var emp_id = vendor_bill_rcrd.getLineItemValue('expense', 'custcol_employee_entity_id', j);
								var proj_desc = vendor_bill_rcrd.getLineItemValue('expense', 'custcolprj_name', j);
								if(proj_desc){
								var pro_entity_id = proj_desc.split(' ');
								proj_id_V = pro_entity_id[0];
								}
								else if(proj_id){
								proj_id_V = proj_id;
								}

								if (!_logValidation(emp_id))
											{
												emp_id = '';
											}
								
								
								vendor_bill_rcrd.selectLineItem('expense', q);
								vendor_bill_rcrd.setCurrentLineItemValue('expense', 'memo', 'SBC-' + memo_body_level);
								vendor_bill_rcrd.setCurrentLineItemValue('expense', 'custcol_customer_entityid', cust_id);
								vendor_bill_rcrd.setCurrentLineItemValue('expense', 'custcol_project_entity_id', proj_id_V);
								vendor_bill_rcrd.setCurrentLineItemValue('expense', 'custcol_employee_entity_id', emp_id);	
								vendor_bill_rcrd.setCurrentLineItemValue('expense', 'custcolprj_name', proj_desc);									
								vendor_bill_rcrd.commitLineItem('expense');
									
									submit_record_flag = 1;
							
								}
								taxAmount_toCompare = Math.abs(taxAmount_toCompare);
							}
						}
						else
						{
							var is_tds_line_true = vendor_bill_rcrd.getLineItemValue('expense', 'custcol_tdsline', q);
							if (is_tds_line_true == 'T')
							{
								var taxAmount = parseFloat(amount) * (parseFloat(10) / 100);
								//taxAmount = applySTRoundMethod(stRoundMethod, taxAmount);
								taxAmount = '-'+taxAmount;
								var amount_tds = vendor_bill_rcrd.getLineItemValue('expense', 'amount', q);
								
								if(parseFloat(amount_tds) == parseFloat(taxAmount))
								{
									var cust_name = vendor_bill_rcrd.getLineItemValue('expense', 'custcolcustcol_temp_customer', j);
									var proj_desc = vendor_bill_rcrd.getLineItemValue('expense', 'custcolprj_name', j);
									var emp_name = vendor_bill_rcrd.getLineItemValue('expense', 'custcol_employeenamecolumn', j);
									var territory = vendor_bill_rcrd.getLineItemValue('expense', 'custcol_territory', j);
									
									vendor_bill_rcrd.selectLineItem('expense', q);
									vendor_bill_rcrd.setCurrentLineItemValue('expense', 'custcolcustcol_temp_customer', cust_name);
									vendor_bill_rcrd.setCurrentLineItemValue('expense', 'custcolprj_name', proj_desc);
									vendor_bill_rcrd.setCurrentLineItemValue('expense', 'custcol_employeenamecolumn', emp_name);
									vendor_bill_rcrd.setCurrentLineItemValue('expense', 'custcol_territory', territory);
									vendor_bill_rcrd.commitLineItem('expense');
									
									submit_record_flag = 1;
							
								}
							}							
						}
					}
					
					var tax_amount_at_line = vendor_bill_rcrd.getLineItemValue('expense', 'tax1amt', j);
					//nlapiLogExecution('audit','tax_amount_at_line:- ',tax_amount_at_line);
					if (_logValidation(tax_amount_at_line))
					{
						var tax_decimal = tax_amount_at_line.toString();
						tax_decimal = tax_decimal.split('.');
						//tax_decimal = tax_amount_at_line.split(',');
						//nlapiLogExecution('audit','tax amnt:- ',tax_decimal[1]);
						if (parseFloat(tax_decimal[1]) >= parseFloat(50)) {
							tax_amount_at_line = Math.round(tax_amount_at_line);
							//nlapiLogExecution('audit','tax_amount_at_line aftr roundoff:- ',tax_amount_at_line);
							vendor_bill_rcrd.selectLineItem('expense', j);
							vendor_bill_rcrd.setCurrentLineItemValue('expense', 'tax1amt', tax_amount_at_line);
							vendor_bill_rcrd.commitLineItem('expense');
						}
						else {
							tax_amount_at_line = Math.round(tax_amount_at_line);
							vendor_bill_rcrd.selectLineItem('expense', j);
							vendor_bill_rcrd.setCurrentLineItemValue('expense', 'tax1amt', tax_amount_at_line);
							vendor_bill_rcrd.commitLineItem('expense');
						}
					}
					var i_subsidiary = nlapiGetFieldValue('subsidiary');
					if (parseInt(i_subsidiary) == 3 && is_swach_cess_reverse_true_mainLine != 'T')
					{
						var amount_round_off = vendor_bill_rcrd.getLineItemValue('expense', 'amount', j);
						amount_round_off = Math.round(amount_round_off);
						//nlapiLogExecution('audit', 'main amount:- ', amount_round_off);
						vendor_bill_rcrd.setLineItemValue('expense', 'amount', j, amount_round_off);
					}
					
					if(o_context.getRemainingUsage() <= 2000)
					{
						nlapiYieldScript();
					}
				}
			}
			else
			{
				var item_line_count = vendor_bill_rcrd.getLineItemCount('item');
				
				var expense_line_count = vendor_bill_rcrd.getLineItemCount('expense');
				
				var vendor_attached = vendor_bill_rcrd.getFieldValue('entity');
				var vendor_name = nlapiLookupField('vendor', vendor_attached, 'altname');
						
				for (var j = 1; j <= expense_line_count; j++)
				{
					//var taxAmount = parseFloat(amount) * (parseFloat(0.5) / 100);
					//taxAmount = applySTRoundMethod(stRoundMethod, taxAmount);
					var is_swach_cess_reverse_true_mainLine = vendor_bill_rcrd.getLineItemValue('expense', 'custcol_ait_swachh_cess_reverse', j);
					for(var q=j; q<= expense_line_count; q++)
					{
						var is_swach_cess_reverse_true = vendor_bill_rcrd.getLineItemValue('expense', 'custcol_ait_swachh_cess_reverse', q);
						
						if (is_swach_cess_reverse_true == 'T')
						{
							var amount = vendor_bill_rcrd.getLineItemValue('expense', 'amount', j);
							
							var emp_type = '';
							var person_type = '';
							var onsite_offsite = '';
							var proj_billing_type = '';
							var i_emp_full_name = '';
							
							var taxAmount = parseFloat(amount) * (parseFloat(0.5) / 100);
							var rounded_amnt = Math.round(taxAmount);
							taxAmount = taxAmount.toString();
							var taxAmount_toCompare = taxAmount.split('.');
							taxAmount_toCompare = taxAmount_toCompare[0];
							//taxAmount = applySTRoundMethod(stRoundMethod, taxAmount);
							
							var amount_sbc = vendor_bill_rcrd.getLineItemValue('expense', 'amount', q);
							amount_sbc = amount_sbc.toString();
							var amount_sbc_toCompare = amount_sbc.split('.');
							amount_sbc_toCompare = amount_sbc_toCompare[0];
							
							nlapiLogExecution('audit','taxAmount_toCompare:- '+taxAmount_toCompare+':::rounded_amnt :- '+rounded_amnt,amount_sbc_toCompare);
							if(parseFloat(amount_sbc_toCompare) == parseFloat(taxAmount_toCompare) || parseFloat(amount_sbc_toCompare) == parseFloat(rounded_amnt))
							{
								var emp_type = '';
								var person_type = '';
								var onsite_offsite = '';
								var proj_billing_type = '';
								var i_emp_full_name ='';
								var proj_id_V = ''; 
								
								var cust_id = vendor_bill_rcrd.getLineItemValue('expense', 'custcol_customer_entityid', j);
								var proj_id = vendor_bill_rcrd.getLineItemValue('expense', 'custcol_project_entity_id', j);
								var emp_id = vendor_bill_rcrd.getLineItemValue('expense', 'custcol_employee_entity_id', j);
								var proj_desc = vendor_bill_rcrd.getLineItemValue('expense', 'custcolprj_name', j);
								if(proj_desc){
								var pro_entity_id = proj_desc.split(' ');
								proj_id_V = pro_entity_id[0];
								}
								else if(proj_id){
								proj_id_V = proj_id;
								}
								if (!_logValidation(emp_id))
											{
												emp_id = '';
											}
								
								
								vendor_bill_rcrd.selectLineItem('expense', q);
								vendor_bill_rcrd.setCurrentLineItemValue('expense', 'memo', 'SBC Charges-'+vendor_name);
								vendor_bill_rcrd.setCurrentLineItemValue('expense', 'custcol_customer_entityid', cust_id);
								vendor_bill_rcrd.setCurrentLineItemValue('expense', 'custcol_project_entity_id', proj_id_V);
								vendor_bill_rcrd.setCurrentLineItemValue('expense', 'custcol_employee_entity_id', emp_id);	
								vendor_bill_rcrd.setCurrentLineItemValue('expense', 'custcolprj_name', proj_desc);									
								vendor_bill_rcrd.commitLineItem('expense');
								
								submit_record_flag = 1;
							}
							else
							{
								var taxAmount = parseFloat(amount) * (parseFloat(0.5) / 100);
								var rounded_amnt = Math.round(taxAmount);
								taxAmount = taxAmount.toString();
								var taxAmount_toCompare = taxAmount.split('.');
								taxAmount_toCompare = taxAmount_toCompare[0];
							
								if(taxAmount_toCompare > parseFloat(0))
									taxAmount_toCompare = '-'+taxAmount_toCompare;
								if(rounded_amnt > parseFloat(0))	
								rounded_amnt = '-'+rounded_amnt;
								
								if(parseFloat(amount_sbc_toCompare) == parseFloat(taxAmount_toCompare) || parseFloat(amount_sbc_toCompare) == parseFloat(rounded_amnt))
								{
									var emp_type = '';
								var person_type = '';
								var onsite_offsite = '';
								var proj_billing_type = '';
								var i_emp_full_name ='';
								var proj_id_V = ''; 
								
								var cust_id = vendor_bill_rcrd.getLineItemValue('expense', 'custcol_customer_entityid', j);
								var proj_id = vendor_bill_rcrd.getLineItemValue('expense', 'custcol_project_entity_id', j);
								var emp_id = vendor_bill_rcrd.getLineItemValue('expense', 'custcol_employee_entity_id', j);
								var proj_desc = vendor_bill_rcrd.getLineItemValue('expense', 'custcolprj_name', j);
								if(proj_desc){
								var pro_entity_id = proj_desc.split(' ');
								proj_id_V = pro_entity_id[0];
								}
								else if(proj_id){
								proj_id_V = proj_id;
								}
								if (!_logValidation(emp_id))
											{
												emp_id = '';
											}
								
								
								vendor_bill_rcrd.selectLineItem('expense', q);
								vendor_bill_rcrd.setCurrentLineItemValue('expense', 'memo', 'SBC Charges-'+vendor_name);
								vendor_bill_rcrd.setCurrentLineItemValue('expense', 'custcol_customer_entityid', cust_id);
								vendor_bill_rcrd.setCurrentLineItemValue('expense', 'custcol_project_entity_id', proj_id_V);
								vendor_bill_rcrd.setCurrentLineItemValue('expense', 'custcol_employee_entity_id', emp_id);	
								vendor_bill_rcrd.setCurrentLineItemValue('expense', 'custcolprj_name', proj_desc);									
								vendor_bill_rcrd.commitLineItem('expense');
									
									submit_record_flag = 1;
							
								}
								taxAmount_toCompare = Math.abs(taxAmount_toCompare);
							}
						}
						else
						{
							var is_tds_line_true = vendor_bill_rcrd.getLineItemValue('expense', 'custcol_tdsline', q);
							if (is_tds_line_true == 'T')
							{
								var amount = vendor_bill_rcrd.getLineItemValue('expense', 'amount', j);
								var taxAmount = parseFloat(amount) * (parseFloat(10) / 100);
								//taxAmount = applySTRoundMethod(stRoundMethod, taxAmount);
								taxAmount = '-'+taxAmount;
								var amount_tds = vendor_bill_rcrd.getLineItemValue('expense', 'amount', q);
								
								if(parseFloat(amount_tds) == parseFloat(taxAmount))
								{
									var cust_name = vendor_bill_rcrd.getLineItemValue('expense', 'custcolcustcol_temp_customer', j);
									var proj_desc = vendor_bill_rcrd.getLineItemValue('expense', 'custcolprj_name', j);
									var emp_name = vendor_bill_rcrd.getLineItemValue('expense', 'custcol_employeenamecolumn', j);
									var territory = vendor_bill_rcrd.getLineItemValue('expense', 'custcol_territory', j);
									
									vendor_bill_rcrd.selectLineItem('expense', q);
									vendor_bill_rcrd.setCurrentLineItemValue('expense', 'custcolcustcol_temp_customer', cust_name);
									vendor_bill_rcrd.setCurrentLineItemValue('expense', 'custcolprj_name', proj_desc);
									vendor_bill_rcrd.setCurrentLineItemValue('expense', 'custcol_employeenamecolumn', emp_name);
									vendor_bill_rcrd.setCurrentLineItemValue('expense', 'custcol_territory', territory);
									vendor_bill_rcrd.commitLineItem('expense');
									
									submit_record_flag = 1;
							
								}
							}							
						}
						
						if(o_context.getRemainingUsage() <= 2000)
						{
							nlapiYieldScript();
						}
					
					}
					
					var tax_amount_at_line = vendor_bill_rcrd.getLineItemValue('expense', 'tax1amt', j);
					//nlapiLogExecution('audit','tax_amount_at_line:- ',tax_amount_at_line);
					if (tax_amount_at_line)
					{
						var tax_decimal = tax_amount_at_line.toString();
						tax_decimal = tax_decimal.split('.');
						//tax_decimal = tax_amount_at_line.split(',');
						//nlapiLogExecution('audit','tax amnt:- ',tax_decimal[1]);
						if (parseFloat(tax_decimal[1]) >= parseFloat(50)) {
							tax_amount_at_line = Math.round(tax_amount_at_line);
							//nlapiLogExecution('audit','tax_amount_at_line after rounding of',tax_amount_at_line);
							vendor_bill_rcrd.selectLineItem('expense', j);
							vendor_bill_rcrd.setCurrentLineItemValue('expense', 'tax1amt', tax_amount_at_line);
							vendor_bill_rcrd.commitLineItem('expense');
						}
						else {
							tax_amount_at_line = Math.round(tax_amount_at_line);
							vendor_bill_rcrd.selectLineItem('expense', j);
							vendor_bill_rcrd.setCurrentLineItemValue('expense', 'tax1amt', tax_amount_at_line);
							vendor_bill_rcrd.commitLineItem('expense');
						}
					}
					var i_subsidiary = nlapiGetFieldValue('subsidiary');
					if (parseInt(i_subsidiary) == 3 && is_swach_cess_reverse_true_mainLine != 'T')
					{
						var amount_round_off = vendor_bill_rcrd.getLineItemValue('expense', 'amount', j);
						amount_round_off = Math.round(amount_round_off);
						//nlapiLogExecution('audit', 'main amount:- ', amount_round_off);
						vendor_bill_rcrd.setLineItemValue('expense', 'amount', j, amount_round_off);
					}
					
					if (o_context.getRemainingUsage() <= 2000)
					{
						nlapiYieldScript();
					}
					
				}
				
				for(var m=1; m<= item_line_count; m++)
				{
					var amount = vendor_bill_rcrd.getLineItemValue('item', 'amount', m);
						
					for(var q=1; q<= expense_line_count; q++)
					{
						var is_swach_cess_reverse_true = vendor_bill_rcrd.getLineItemValue('expense', 'custcol_ait_swachh_cess_reverse', q);
						
						if (is_swach_cess_reverse_true == 'T')
						{
							var taxAmount = parseFloat(amount) * (parseFloat(0.5) / 100);
							
							taxAmount = taxAmount.toString();
							var taxAmount_toCompare = taxAmount.split('.');
							taxAmount_toCompare = taxAmount_toCompare[0];
							var temp_taxAmt = parseFloat(taxAmount_toCompare) + parseFloat(1.0);
							//taxAmount = applySTRoundMethod(stRoundMethod, taxAmount);
							
							var amount_sbc = vendor_bill_rcrd.getLineItemValue('expense', 'amount', q);
							amount_sbc = amount_sbc.toString();
							var amount_sbc_toCompare = amount_sbc.split('.');
							amount_sbc_toCompare = amount_sbc_toCompare[0];
							if(parseFloat(amount_sbc_toCompare) == parseFloat(taxAmount_toCompare) || parseFloat(temp_taxAmt) == parseFloat(amount_sbc_toCompare))
							{
								var emp_type = '';
								var person_type = '';
								var onsite_offsite = '';
								var proj_billing_type = '';
								var i_emp_full_name ='';
								var proj_id_V = ''; 
								
								var cust_id = vendor_bill_rcrd.getLineItemValue('item', 'custcol_customer_entityid', m);
								var proj_id = vendor_bill_rcrd.getLineItemValue('item', 'custcol_project_entity_id', m);
								var emp_id = vendor_bill_rcrd.getLineItemValue('item', 'custcol_employee_entity_id', m);
								var proj_desc = vendor_bill_rcrd.getLineItemValue('item', 'custcolprj_name', m);
								if(proj_desc){
								var pro_entity_id = proj_desc.split(' ');
								proj_id_V = pro_entity_id[0];
								}
								else if(proj_id){
								proj_id_V = proj_id;
								}


								if (!_logValidation(emp_id))
											{
												emp_id = '';
											}
								
								
								vendor_bill_rcrd.selectLineItem('expense', q);
								
								vendor_bill_rcrd.setCurrentLineItemValue('expense', 'custcol_customer_entityid', cust_id);
								vendor_bill_rcrd.setCurrentLineItemValue('expense', 'custcol_project_entity_id', proj_id_V);
								vendor_bill_rcrd.setCurrentLineItemValue('expense', 'custcol_employee_entity_id', emp_id);	
								vendor_bill_rcrd.setCurrentLineItemValue('expense', 'custcolprj_name', proj_desc);									
								vendor_bill_rcrd.commitLineItem('expense');
								
								submit_record_flag = 1;
							}
							else
							{
								if(taxAmount_toCompare > parseFloat(0))
									taxAmount_toCompare = '-'+taxAmount_toCompare;
									temp_taxAmt = '-'+temp_taxAmt;
								
								if(parseFloat(amount_sbc_toCompare) == parseFloat(taxAmount_toCompare) || parseFloat(temp_taxAmt) == parseFloat(amount_sbc_toCompare))
								{
									var emp_type = '';
								var person_type = '';
								var onsite_offsite = '';
								var proj_billing_type = '';
								var i_emp_full_name ='';
								var proj_id_V = ''; 
								
								var cust_id = vendor_bill_rcrd.getLineItemValue('item', 'custcol_customer_entityid', m);
								var proj_id = vendor_bill_rcrd.getLineItemValue('item', 'custcol_project_entity_id', m);
								var emp_id = vendor_bill_rcrd.getLineItemValue('item', 'custcol_employee_entity_id', m);
								var proj_desc = vendor_bill_rcrd.getLineItemValue('item', 'custcolprj_name', m);
								if(proj_desc){
								var pro_entity_id = proj_desc.split(' ');
								proj_id_V = pro_entity_id[0];
								}
								else if(proj_id){
								proj_id_V = proj_id;
								}

								if (!_logValidation(emp_id))
											{
												emp_id = '';
											}
								
								
								vendor_bill_rcrd.selectLineItem('expense', q);
								
								vendor_bill_rcrd.setCurrentLineItemValue('expense', 'custcol_customer_entityid', cust_id);
								vendor_bill_rcrd.setCurrentLineItemValue('expense', 'custcol_project_entity_id', proj_id_V);
								vendor_bill_rcrd.setCurrentLineItemValue('expense', 'custcol_employee_entity_id', emp_id);	
								vendor_bill_rcrd.setCurrentLineItemValue('expense', 'custcolprj_name', proj_desc);									
								vendor_bill_rcrd.commitLineItem('expense');
									
									submit_record_flag = 1;
							
								}
								taxAmount_toCompare = Math.abs(taxAmount_toCompare);
							}
						}
					}
					//Add
					var i_subsidiary = nlapiGetFieldValue('subsidiary');
					if (parseInt(i_subsidiary) == parseInt(3) )
					{
						var amount_round_off = vendor_bill_rcrd.getLineItemValue('item', 'grossamt', m);
						amount_round_off = Math.round(amount_round_off);
						//nlapiLogExecution('audit', 'main amount rounded:- ', amount_round_off);
						vendor_bill_rcrd.setLineItemValue('item', 'grossamt', m, amount_round_off);
					}
				}
			}
		
			//if(submit_record_flag == 1)
			{
				var vendor_bill_submitted_id = nlapiSubmitRecord(vendor_bill_rcrd,true,true);
				nlapiLogExecution('debug','submitted bill id:-- ',vendor_bill_submitted_id);
				bill_already_processed_list.push(vendor_bill_submitted_id);
			}
				}
			}
		}
	}
	catch(e)
	{
		nlapiLogExecution('ERROR','ERROR MESSAGE:-- ',e);
	}
}

function _logValidation(value) 
{
 if(value != null && value.toString() != null && value != '' && value != undefined && value.toString() != undefined && value != 'undefined' && value.toString() != 'undefined'&& value.toString() != 'NaN' && value != NaN) 
 {
  return true;
 }
 else 
 { 
  return false;
 }
}