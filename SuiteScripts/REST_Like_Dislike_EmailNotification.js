/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       28 Nov 2019     Nihal Mulani Cell: 9423591358
 *
 */

/**
 * @param {Object} dataIn Parameter object
 * @returns {Object} Output object
 */
function postRESTlet(dataIn) {
	try{
		nlapiLogExecution("debug","dataIn:",JSON.stringify(dataIn));
		var data =dataIn;// JSON.parse(dataIn);
		var FRF = data.frfInternalId;
		var emailId = data.userEmailId;
		var EmployeeID = getUserUsingEmailId(emailId)
		nlapiLogExecution("debug","FRF:",FRF);
		nlapiLogExecution("debug","EmployeeID",EmployeeID);
		var EmpFields = nlapiLookupField('employee',EmployeeID,['custentity_reportingmanager']);
		nlapiLogExecution("debug","EmpFields",JSON.stringify(EmpFields));

		var FRFFields = nlapiLookupField('customrecord_frf_details',FRF,['custrecord_frf_details_created_by','custrecord_frf_details_opp_id','custrecord_frf_details_project','custrecord_frf_details_res_practice','custrecord_frf_details_frf_number']);
		nlapiLogExecution("debug","FRFFields",JSON.stringify(FRFFields));
		
	/*	var reportingmanager_email =  NlapiLookupField('employee',EmpFields.custentity_reportingmanager,['email']);
		nlapiLogExecution("debug","reportingmanager_email",JSON.stringify(reportingmanager_email));*/
		var SendTo = [];//reportingmanager_email.email
		var FRFCreatedby_email='';
		if(FRFFields.custrecord_frf_details_created_by)
		{
			FRFCreatedby_email =  NlapiLookupField('employee',FRFFields.custrecord_frf_details_created_by,['email']);
			nlapiLogExecution("debug","FRFCreatedby_email",JSON.stringify(FRFCreatedby_email));
			SendTo.push(FRFCreatedby_email.email);
		}
		var fulfillmentSpocs = getAllFulfillmentSpocs(FRFFields.custrecord_frf_details_res_practice);
		SendTo = SendTo.concat(fulfillmentSpocs);
		var PracticeSpokes_CCinEmail = getAllPracticesCC(FRFFields.custrecord_frf_details_res_practice);
		PracticeSpokes_CCinEmail.push('fuel.support@BRILLIO.COM');
		PracticeSpokes_CCinEmail.push(data.userEmailId);

		nlapiLogExecution("debug","PracticeSpokes_CCinEmail",JSON.stringify(PracticeSpokes_CCinEmail));

		nlapiLogExecution("debug","SendTo 37",JSON.stringify(SendTo));
		if(FRFFields.custrecord_frf_details_opp_id)
		{
			var opp_PM = [];
			var arr = NlapiLookupField('customrecord_sfdc_opportunity_record',FRFFields.custrecord_frf_details_opp_id,['custrecord_sfdc_opp_pm']);
			nlapiLogExecution("debug","custrecord_sfdc_opp_pm",JSON.stringify(arr.custrecord_sfdc_opp_pm));
			if(arr.custrecord_sfdc_opp_pm){
				var spl = arr.custrecord_sfdc_opp_pm;
				var split = spl.split(",");
				for(var i=0;i<split.length;i++)
				{
					opp_PM.push(NlapiLookupField('employee',split[i],['email']).email);
				}
				SendTo = SendTo.concat(opp_PM);
			}

			var opp_DM = [];
			var arre = NlapiLookupField('customrecord_sfdc_opportunity_record',FRFFields.custrecord_frf_details_opp_id,['custrecord_sfdc_opportunity_dm']);
			nlapiLogExecution("debug","custrecord_sfdc_opp_pm",JSON.stringify(arre.custrecord_sfdc_opportunity_dm));
			if(arre.custrecord_sfdc_opportunity_dm)
			{
				var spli = arre.custrecord_sfdc_opportunity_dm;
				var split1 = spli.split(",");
				for(var i=0;i<split1.length;i++)
				{
					opp_DM.push(NlapiLookupField('employee',split1[i],['email']).email);
				}

				SendTo = SendTo.concat(opp_DM);
			}
		}
		else
		{
			var ProjectManager = NlapiLookupField('job',FRFFields.custrecord_frf_details_project,['custentity_projectmanager','custentity_deliverymanager']);
			var ProjectManagerEmail = NlapiLookupField('employee',ProjectManager.custentity_projectmanager,['email']);
			//var deliverymanager = NlapiLookupField('employee',ProjectManager.custentity_deliverymanager,['email']);
			//SendTo.push(ProjectManagerEmail.email);
			SendTo = SendTo.concat([ProjectManagerEmail.email]);//,deliverymanager.email
		}
		nlapiLogExecution("debug","SendTo",JSON.stringify(SendTo));
		var emailBody = getEmailBody(FRF,FRFFields.custrecord_frf_details_frf_number,EmployeeID);
		var subject = FRFFields.custrecord_frf_details_frf_number+' : FUEL Notification: Interest Shown';
		/*var to = ['nihal.mulani@gmail.com'];//SendTo
	var cc= ['nihal@inspirria.com','fuel.support@BRILLIO.COM'];//PracticeSpokes_CCinEmail*/
		nlapiLogExecution("debug","emailBody",emailBody);
		var emailSent = nlapiSendEmail('154256',SendTo,subject,emailBody,PracticeSpokes_CCinEmail);
		return {"sucess":true};
	}
	catch(err)
	{
		nlapiLogExecution('ERROR', 'ERROR', err);
		return {"ERROR":err};
	}
}
function getEmailBody(FRFinternalID,frfNumber,EMPID)
{
	var filter = new Array();
	var all_data= new Array();

	filter.push(new nlobjSearchFilter('internalid', null, 'is',FRFinternalID)); 
	filter.push(new nlobjSearchFilter('isinactive', null, 'is','F'));

	var columns = new Array();
	columns.push(new nlobjSearchColumn('custrecord_frf_details_opp_id'));
	columns.push(new nlobjSearchColumn('custrecord_frf_details_account'));
	columns.push(new nlobjSearchColumn('custrecord_frf_details_project'));
	columns.push(new nlobjSearchColumn('custrecord_frf_details_created_by'));
	var results= nlapiSearchRecord('customrecord_frf_details', null, filter, columns);
	var Name = '';
	if(results[0].getValue('custrecord_frf_details_opp_id'))
	{
		Name = results[0].getText('custrecord_frf_details_opp_id')
	}
	else
	{
		Name = results[0].getText('custrecord_frf_details_project')
	}
	var emailBody = '';
	emailBody+='Dear '+results[0].getText('custrecord_frf_details_created_by')+',<br/><br/>';//XYZ (Opportunity Manager/FRF creator);

	emailBody+= nlapiLookupField('employee',EMPID,'entityid')+' is interested in the open position #'+frfNumber+' Number. Opportunity/Project '+Name+' Account- '+results[0].getText('custrecord_frf_details_account')+'<br/><br/>';
	emailBody+='Kindly verify the fitment and do the needful..  <br/><br/>';

	emailBody+='Brillio-FUEL <br/><br/>';
	emailBody+='This email was sent from a notification only address. <br/><br/>';
	emailBody+='If you need further assistance, please write to fuel.support@brillio.com';
	return emailBody;
	
}
function getAllPracticesCC(practice)
{
	var filter = new Array();
	var all_data= new Array();

	filter.push(new nlobjSearchFilter('custrecord_practice', null, 'is',practice)); 
	filter.push(new nlobjSearchFilter('isinactive', null, 'is','F'));

	var columns = new Array();
	columns.push(new nlobjSearchColumn('email','custrecord_spoc'));


	var results= nlapiSearchRecord('customrecord_practice_spoc', null, filter, columns);
	for(var r = 0; r<results.length;r++)
	{
		all_data.push(results[r].getValue('email','custrecord_spoc'));
	}
	return all_data;
}
function getAllFulfillmentSpocs(practice)
{
	var filter = new Array();
	var all_data= new Array();

	filter.push(new nlobjSearchFilter('custrecord_resource_practice', null, 'is',practice)); 
	filter.push(new nlobjSearchFilter('isinactive', null, 'is','F'));

	var columns = new Array();
	columns.push(new nlobjSearchColumn('email','custrecord_fulfillment_team_members'));


	var results= nlapiSearchRecord('customrecord_fulfillment_spocs', null, filter, columns);
	for(var r = 0; r<results.length;r++)
	{
		all_data.push(results[r].getValue('email','custrecord_fulfillment_team_members'));
	}
	return all_data;
}
function NlapiLookupField(recordID,InternalID,FieldsArray)
{
	return nlapiLookupField(recordID,InternalID,FieldsArray);
}