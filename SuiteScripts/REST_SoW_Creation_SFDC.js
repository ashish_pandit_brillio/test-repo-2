/**
* Module Description
* 
 * Version    Date            Author           Remarks
* 1.00       05 May 2017     deepak.srinivas
*
*/

/**
* @param {Object} dataIn Parameter object
* @returns {Object} Output object
*/
                  
function postRESTlet(dataIn) {
      try{
            var response = new Response();
            var current_date = nlapiDateToString(new Date());
            //Log for current date
            nlapiLogExecution('DEBUG', 'Current Date', 'current_date...' + current_date)
            nlapiLogExecution('debug', 'dataIn', JSON.stringify(dataIn));
            
      //    var employeeId =  getUserUsingEmailId(dataIn.EmailId);
            var requestType = dataIn.RequestType; 
            var idExists = dataIn.Data ? dataIn.Data[0].Opportunity.Account.Account_ID__c : '';
            nlapiLogExecution('debug', 'idExists', idExists);         
                      
            
            
            switch (requestType) {

            case M_Constants.Request.Get:

                  if (idExists) 
                  {
                        response.Data = create_SoW(dataIn);
                        response.Status = true;
                  } else {
                        response.Data = "Some error with the data sent";
                        response.Status = false;
            }
                  break;
            }
      
      }
      catch (err) {
            nlapiLogExecution('ERROR', 'postRESTlet', err);
            response.Data = err;
            response.Status = false;
      }
      nlapiLogExecution('debug', 'response', JSON.stringify(response));
      return response;
      
}
function create_SoW(dataIn){
try{
      if(dataIn){
      var o_dataObj = dataIn.Data[0].Opportunity //? dataIn.Data.WeekStartDate : '';
      var account_Obj = o_dataObj.Account.Account_ID__c;
      var customer_id = searchCustomer(account_Obj);
	
      if(!_logValidation(customer_id)){
    	  throw 'No customer found in Netsuite for this SOW';
    	  return false;
      }
      var customer_lookup = nlapiLookupField('customer',parseInt(customer_id),'subsidiary');
	
      var account_id =account_Obj.Id;
      var Opportunity_Start_Date_temp = o_dataObj.Opportunity_Start_Date__c;
    //  var Opp_subsidiary = o_dataObj.Account_Subsidiary__c;
      var tax_code = 'GST:GST @0%';
      nlapiLogExecution('debug', 'customer_lookup', customer_lookup);
      
      if(parseInt(customer_lookup) == parseInt(2)){
            var customForm_sel = 107;
			
      }
      else{
            var customForm_sel = 128;
        if(_logValidation(o_dataObj.Tax_Code__c))
		  tax_code = o_dataObj.Tax_Code__c;
        
      }
      var Opportunity_Start_Date = dateFormat(Opportunity_Start_Date_temp);
      //Conversion
      var amount = o_dataObj.SOW_Amount__c;
     if(customer_id){
    	  var customer_lookup_currency = nlapiLookupField('customer',parseInt(customer_id),'currency',true);
    	  if(customer_lookup_currency != 'USD'){
    	  var f_rate = nlapiExchangeRate('USD',customer_lookup_currency,Opportunity_Start_Date);
    	  nlapiLogExecution('DEBUG','Exchange rate',f_rate);
    	  amount = (parseFloat(amount) * parseFloat(f_rate)).toFixed(2);
      }
     }

      var SOW_Start_Date__c = dateFormat(o_dataObj.SOW_Start_Date__c);
      nlapiLogExecution('debug', 'SOW_Start_Date__c', SOW_Start_Date__c);
      var SOW_End_Date__c = dateFormat(o_dataObj.SOW_End_Date__c);
      var delivery_model = o_dataObj.Delivery_Model__c;
      var SOW_Status__c = o_dataObj.SOW_Status__c;
      var Billing_Type__c = o_dataObj.Billing_Type__c;
      var Memo__c = o_dataObj.Memo__c;
      var project_val = o_dataObj.Project_Id__c;
      var project_int_id = '';
      if(project_val){
            project_int_id = searchProject(project_val);
            var s_billingType = nlapiLookupField('job',project_int_id,'jobbillingtype');
      }
      var s_workLocation = o_dataObj.Work_Location__c;
      if(s_workLocation)
            s_workLocation = s_workLocation.split(';');
      
      var record = nlapiCreateRecord('salesorder', {recordmode:'dynamic'});
      record.setFieldValue('customform',customForm_sel);
      record.setFieldValue('entity',customer_id);
      //record.setFieldText('subsidiary',Opp_subsidiary); Project_Id__c
      record.setFieldValue('trandate',Opportunity_Start_Date);
      if(parseInt(customer_lookup) == parseInt(2)){
            record.setFieldValue('custbody_projectstartdate',SOW_Start_Date__c);
            record.setFieldValue('custbody_projectenddate',SOW_End_Date__c);
      }
      else{
      record.setFieldValue('startdate',SOW_Start_Date__c);
      record.setFieldValue('enddate',SOW_End_Date__c);
      }
      if(_logValidation(project_int_id) && s_billingType != 'TM'){
      record.setFieldValue('job',project_int_id);
      }
      if(s_billingType == 'TM'){
    	  record.setFieldValue('custbody_sow_project_id_',project_val);
      }
     // record.setFieldValue('custbody7',o_dataObj.Opportunity_ID__c); //custbody_opp_id_sfdc
      record.setFieldValue('custbody_opp_id_sfdc',o_dataObj.Opportunity_ID__c); 
      record.setFieldText('custbody_projectmodel',delivery_model);
      record.setFieldText('custbody_sow',SOW_Status__c);
      record.setFieldText('custbody_billingtype',Billing_Type__c);
	  if(_logValidation(Memo__c))
      record.setFieldValue('memo',Memo__c);
      
      if(_logValidation(o_dataObj.Vertical__c))
      record.setFieldText('class',o_dataObj.Vertical__c); //otherrefnum
      
       if(_logValidation(o_dataObj.Project_Location__c))
      record.setFieldText('location',o_dataObj.Project_Location__c); //otherrefnum
        
      if(_logValidation(o_dataObj.PO__c))
      record.setFieldText('otherrefnum',o_dataObj.PO__c);
      
      if(_logValidation(s_workLocation))
      record.setFieldText('custbody_worklocation',s_workLocation);
      
      if(_logValidation(o_dataObj.Billing_Location__c))
      record.setFieldText('custbody_billinglocation',o_dataObj.Billing_Location__c);
      //custbody_billinglocation
      var lineCount = nlapiGetLineItemCount('item');
      var recLineCount = record.getLineItemCount('item');
      nlapiLogExecution('debug', 'lineCount', lineCount);
      nlapiLogExecution('debug', 'recLineCount', recLineCount);
      //get line count
     /* var lineCount = nlapiGetLineItemCount('item');
      for(var n=lineCount ; n>=1; n--){
			nlapiRemoveLineItem('item',n);
			
		}
      var recLineCount = record.getLineItemCount('item');
      for(var m=recLineCount ; m>=1; m--){
			record.removeLineItem('item',m);
			
		}*/
     // for(var i=1;i<=1;i++){
      //Creating Line Items
      record.selectNewLineItem('item');
      
      record.setCurrentLineItemText('item','item',o_dataObj.Item__c);
      //nlapiLogExecution('DEBUG','ID',obj[j].RefNo);             
      record.setCurrentLineItemValue('item','memo',o_dataObj.Memo__c); 
      record.setCurrentLineItemValue('item','quantity',1); //rate
      record.setCurrentLineItemValue('item','rate',amount);
      record.setCurrentLineItemValue('item','amount',amount);
	  
	  if(parseInt(customer_lookup) == parseInt(3)){
      //Tax code
            record.setCurrentLineItemText('item','taxcode',tax_code); 
	  }
     // record.setCurrentLineItemText('item','units','Number');
      //nlapiLogExecution('DEBUG','Memo',obj[j].Memo);      
      record.commitLineItem('item');
   //   }
      var Sow_ID = nlapiSubmitRecord(record,true,true);
      nlapiLogExecution('DEBUG',' creating the SOW Sow_ID','Sow_ID:'+Sow_ID);
      var JSON_ = {};
      var dataRow_ = [];
      if(Sow_ID){
            var lookUp_Expense = nlapiLookupField('salesorder',Sow_ID,['tranid','custbody_opp_id_sfdc']);
            JSON_ ={
                        'SOW_ID':lookUp_Expense.tranid,
                        'OPP_ID':lookUp_Expense.custbody_opp_id_sfdc
            };
            dataRow_.push(JSON_);
      }
      
     
      return dataRow_;
}
}
      catch(e){         
            nlapiLogExecution('DEBUG','Erron in creating SOW',e);
            throw e;
            
      }
}
//validate blank entries
function _logValidation(value) 
{
if(value != null && value.toString() != null && value != '' && value != undefined && value.toString() != undefined && value != 'undefined' && value.toString() != 'undefined'&& value.toString() != 'NaN' && value != NaN) 
 {
  return true;
}
else 
 { 
  return false;
}
}
function dateFormat(date){
      var date_return = '';
      if(date){
            var dateObj = date.split('-');
            var month = dateObj[1];
            var year = dateObj[0];
            var date_d = dateObj[2];
            
            date_return = month+'/'+date_d+'/'+year;
      }
      return date_return;
}

function searchCustomer(account_id){
      try{
            var customer_id = '';
            var filters = [];
            var s_account_customer = '"'+account_id+'"';
            nlapiLogExecution('DEBUG','Customer Filter ',s_account_customer);
            //filters.push(new nlobjSearchFilter('custentity22',null,'is',account_id.toString())); //custentity_sfdc_account_id
            filters.push(new nlobjSearchFilter('custentity_sfdc_account_id',null,'is',account_id.toString()));
            
            var cols = [];
            cols.push(new nlobjSearchColumn('internalid').setSort(true));
            //cols.push(new nlobjSearchColumn('name'));
            
            var searchRecord = nlapiSearchRecord('customer',null,filters,cols);
            if(searchRecord)
                  customer_id = searchRecord[0].getValue('internalid');
            nlapiLogExecution('DEBUG','Customer Search ',customer_id);
            
            return customer_id;
      }
      catch(e){
            nlapiLogExecution('DEBUG','Customer Search Error',e);
            throw e;
      }
}

function searchProject(pro_id){
      try{
            var proj_id = '';
            var filters = [];
            var s_account_customer = '"'+pro_id+'"';
            nlapiLogExecution('DEBUG','Project Filter ',pro_id);
            filters.push(new nlobjSearchFilter('entityid',null,'startswith',pro_id));
            
            var cols = [];
            cols.push(new nlobjSearchColumn('internalid').setSort(true));
            //cols.push(new nlobjSearchColumn('name'));
            
            var searchRecord = nlapiSearchRecord('job',null,filters,cols);
            if(searchRecord)
                  proj_id = searchRecord[0].getValue('internalid');
            nlapiLogExecution('DEBUG','Project Search ',proj_id);
            
            return proj_id;
      }
      catch(e){
            nlapiLogExecution('DEBUG','Customer Search Error',e);
            throw e;
      }
}

