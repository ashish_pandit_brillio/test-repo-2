/**
 * @author Jayesh
 */

function scheduled_ageing()
{
	try
	{
		var todays_date = new Date();
		
		var today = new Date();
		var dd = today.getDate();
		var mm = today.getMonth()+1;
		var yyyy = today.getFullYear();
		today = mm+'/'+dd+'/'+yyyy;
		
		var last_friday_date = nlapiAddDays(todays_date,-4);
		last_friday_date = nlapiStringToDate(nlapiDateToString(last_friday_date));
		nlapiLogExecution('audit','last friday date :- ',last_friday_date);

		var todays_date_d = nlapiStringToDate(nlapiDateToString(new Date()));
		
		var unique_practice_arr = new Array();
		var practice_data_arr = new Array();
		var sr_no = 0;
		
		var filters_allocation = new Array();
		filters_allocation[0] = new nlobjSearchFilter('startdate', null, 'onorbefore', last_friday_date);
		filters_allocation[1] = new nlobjSearchFilter('enddate', null, 'onorafter', last_friday_date);
		filters_allocation[2] = new nlobjSearchFilter('custentity_implementationteam', 'employee', 'is', 'F');
		filters_allocation[3] = new nlobjSearchFilter('custentity_project_allocation_category', 'job', 'noneof', 6);
						
		var columns = new Array();
		columns[0] = new nlobjSearchColumn('internalid').setSort(true);
		columns[1] = new nlobjSearchColumn('custeventrbillable');
		columns[2] = new nlobjSearchColumn('jobbillingtype', 'job');
		columns[3] = new nlobjSearchColumn('custentity_clientpartner', 'job');
		columns[4] = new nlobjSearchColumn('custentity_projectmanager', 'job');
		columns[5] = new nlobjSearchColumn('custentity_deliverymanager', 'job');
		columns[6] = new nlobjSearchColumn('custentity_verticalhead', 'job');
		columns[7] = new nlobjSearchColumn('custevent_practice');
		columns[8] = new nlobjSearchColumn('customer', 'job');
		columns[9] = new nlobjSearchColumn('company');
		columns[10] = new nlobjSearchColumn('custentity_project_allocation_category', 'job');
		columns[11] = new nlobjSearchColumn('subsidiary', 'employee');
		columns[12] = new nlobjSearchColumn('custentity_clientpartner', 'customer');
		columns[13] = new nlobjSearchColumn('custentity_region', 'customer');
		columns[14] = new nlobjSearchColumn('custentity_region', 'job');
		columns[15] = new nlobjSearchColumn('percentoftime');
		columns[16] = new nlobjSearchColumn('custrecord_parent_practice', 'custevent_practice');
		columns[17] = new nlobjSearchColumn('custentity_practice', 'job');
		columns[18] = new nlobjSearchColumn('department', 'employee');
		columns[19] = new nlobjSearchColumn('resource');
		columns[20] = new nlobjSearchColumn('startdate');
		columns[21] = new nlobjSearchColumn('enddate');
		columns[22] = new nlobjSearchColumn('custrecord_practice_type_ticker', 'custevent_practice');
		columns[23] = new nlobjSearchColumn('custrecord_is_region', 'custevent_practice');
		
		var project_allocation_result = searchRecord('resourceallocation', null, filters_allocation, columns);
		if (project_allocation_result)
		{
			var strVar_excel_overall = '';
			strVar_excel_overall += '<table>';
			strVar_excel_overall += '	<tr>';
			strVar_excel_overall += ' <td width="100%">';
			strVar_excel_overall += '<table width="100%" border="1">';
			
			strVar_excel_overall += '	<tr>';
			strVar_excel_overall += ' <td width="6%" font-size="11" align="center">Region</td>';
			strVar_excel_overall += ' <td width="6%" font-size="11" align="center">Customer Name</td>';
			strVar_excel_overall += ' <td width="6%" font-size="11" align="center">Project Name</td>';
			strVar_excel_overall += ' <td width="6%" font-size="11" align="center">Executing Practice</td>';
			strVar_excel_overall += ' <td width="6%" font-size="11" align="center">Billing Type</td>';
			strVar_excel_overall += ' <td width="10%" font-size="11" align="left">Employee Name</td>';
			strVar_excel_overall += ' <td width="6%" font-size="11" align="center">Employee Practice</td>';
			strVar_excel_overall += ' <td width="6%" font-size="11" align="center">Employeee Subsidiary</td>';
			strVar_excel_overall += ' <td width="6%" font-size="11" align="center">Percent Allocated</td>';
			strVar_excel_overall += ' <td width="6%" font-size="11" align="center">Resource Billable</td>';
			strVar_excel_overall += ' <td width="6%" font-size="11" align="center">Allocation Start Date</td>';
			strVar_excel_overall += ' <td width="6%" font-size="11" align="center">Allocation End Date</td>';
			strVar_excel_overall += ' <td width="6%" font-size="11" align="center">Client Partner</td>';
			strVar_excel_overall += ' <td width="6%" font-size="11" align="center">Project Manager</td>';
			strVar_excel_overall += ' <td width="6%" font-size="11" align="center">Delivery Manager</td>';			
			strVar_excel_overall += ' <td width="6%" font-size="11" align="center">Project Category</td>';
			strVar_excel_overall += '	</tr>';
			
			
			var strVar_excel_onsite_unbilled = '';
			strVar_excel_onsite_unbilled += '<table>';
			strVar_excel_onsite_unbilled += '	<tr>';
			strVar_excel_onsite_unbilled += ' <td width="100%">';
			strVar_excel_onsite_unbilled += '<table width="100%" border="1">';
			
			strVar_excel_onsite_unbilled += '	<tr>';
			strVar_excel_onsite_unbilled += ' <td width="6%" font-size="11" align="center">Region</td>';
			strVar_excel_onsite_unbilled += ' <td width="6%" font-size="11" align="center">Customer Name</td>';
			strVar_excel_onsite_unbilled += ' <td width="6%" font-size="11" align="center">Project Name</td>';
			strVar_excel_onsite_unbilled += ' <td width="6%" font-size="11" align="center">Executing Practice</td>';
			strVar_excel_onsite_unbilled += ' <td width="6%" font-size="11" align="center">Billing Type</td>';
			strVar_excel_onsite_unbilled += ' <td width="10%" font-size="11" align="left">Employee Name</td>';
			strVar_excel_onsite_unbilled += ' <td width="6%" font-size="11" align="center">Employee Practice</td>';
			strVar_excel_onsite_unbilled += ' <td width="6%" font-size="11" align="center">Employeee Subsidiary</td>';
			strVar_excel_onsite_unbilled += ' <td width="6%" font-size="11" align="center">Percent Allocated</td>';
			strVar_excel_onsite_unbilled += ' <td width="6%" font-size="11" align="center">Resource Billable</td>';
			strVar_excel_onsite_unbilled += ' <td width="6%" font-size="11" align="center">Allocation Start Date</td>';
			strVar_excel_onsite_unbilled += ' <td width="6%" font-size="11" align="center">Allocation End Date</td>';
			strVar_excel_onsite_unbilled += ' <td width="6%" font-size="11" align="center">Client Partner</td>';
			strVar_excel_onsite_unbilled += ' <td width="6%" font-size="11" align="center">Project Manager</td>';
			strVar_excel_onsite_unbilled += ' <td width="6%" font-size="11" align="center">Delivery Manager</td>';			
			strVar_excel_onsite_unbilled += ' <td width="6%" font-size="11" align="center">Project Category</td>';
			strVar_excel_onsite_unbilled += '	</tr>';
			
			for (var i_search_indx = 0; i_search_indx < project_allocation_result.length; i_search_indx++)
			{
				var region_dump = project_allocation_result[i_search_indx].getText('custentity_region','customer');
				var customer_dump = project_allocation_result[i_search_indx].getText('customer', 'job');
				var project_dump = project_allocation_result[i_search_indx].getText('company');
				var excuting_pract_proj_dump = project_allocation_result[i_search_indx].getText('custentity_practice', 'job');
				var proj_billing_type_dump = project_allocation_result[i_search_indx].getText('jobbillingtype', 'job');
				var employee_dump = project_allocation_result[i_search_indx].getText('resource');
				var emp_practice_dump = project_allocation_result[i_search_indx].getText('custevent_practice');
				var emp_subsidiary_dump = project_allocation_result[i_search_indx].getText('subsidiary', 'employee');
				var emp_subsidiary_dump_id = project_allocation_result[i_search_indx].getValue('subsidiary', 'employee');
				var prcnt_of_tym_dump = project_allocation_result[i_search_indx].getValue('percentoftime');
				prcnt_of_tym_dump = prcnt_of_tym_dump.toString();
				prcnt_of_tym_dump = prcnt_of_tym_dump.split('.');
				prcnt_of_tym_dump = prcnt_of_tym_dump[0].toString().split('%');
				var final_prcnt_allocation_dump = parseFloat(prcnt_of_tym_dump) / parseFloat(100);
				final_prcnt_allocation_dump = parseFloat(final_prcnt_allocation_dump);
				var is_resource_billable_dump = project_allocation_result[i_search_indx].getValue('custeventrbillable');
				var allo_strt_date_dump = project_allocation_result[i_search_indx].getValue('startdate');
				var allo_end_date_dump = project_allocation_result[i_search_indx].getValue('enddate');
				var client_partner_dump = project_allocation_result[i_search_indx].getText('custentity_clientpartner','customer');
				var prj_manager_dump = project_allocation_result[i_search_indx].getText('custentity_projectmanager', 'job');
				var delivery_manager_dump = project_allocation_result[i_search_indx].getText('custentity_deliverymanager', 'job');
				var proj_category_dump = project_allocation_result[i_search_indx].getText('custentity_project_allocation_category', 'job');

				if(is_resource_billable_dump == 'T')
				{
					is_resource_billable_dump = 'Yes';
				}
				else
				{
					is_resource_billable_dump = 'No';
					
					if(parseInt(emp_subsidiary_dump_id) != 3)
					{
						strVar_excel_onsite_unbilled += '	<tr>';
						strVar_excel_onsite_unbilled += ' <td width="6%" font-size="11" align="center">' +region_dump+ '</td>';
						strVar_excel_onsite_unbilled += ' <td width="6%" font-size="11" align="center">' +customer_dump+ '</td>';
						strVar_excel_onsite_unbilled += ' <td width="6%" font-size="11" align="center">' +project_dump+ '</td>';
						strVar_excel_onsite_unbilled += ' <td width="6%" font-size="11" align="center">' +excuting_pract_proj_dump+ '</td>';
						strVar_excel_onsite_unbilled += ' <td width="6%" font-size="11" align="center">' +proj_billing_type_dump+ '</td>';
						strVar_excel_onsite_unbilled += ' <td width="10%" font-size="11" align="left">' +employee_dump+ '</td>';
						strVar_excel_onsite_unbilled += ' <td width="6%" font-size="11" align="center">' +emp_practice_dump+ '</td>';
						strVar_excel_onsite_unbilled += ' <td width="6%" font-size="11" align="center">' +emp_subsidiary_dump+ '</td>';
						strVar_excel_onsite_unbilled += ' <td width="6%" font-size="11" align="center">' +final_prcnt_allocation_dump+ '</td>';
						strVar_excel_onsite_unbilled += ' <td width="6%" font-size="11" align="center">' +is_resource_billable_dump+ '</td>';
						strVar_excel_onsite_unbilled += ' <td width="6%" font-size="11" align="center">' +allo_strt_date_dump+ '</td>';
						strVar_excel_onsite_unbilled += ' <td width="6%" font-size="11" align="center">' +allo_end_date_dump+ '</td>';
						strVar_excel_onsite_unbilled += ' <td width="6%" font-size="11" align="center">' +client_partner_dump+ '</td>';
						strVar_excel_onsite_unbilled += ' <td width="6%" font-size="11" align="center">' +prj_manager_dump+ '</td>';
						strVar_excel_onsite_unbilled += ' <td width="6%" font-size="11" align="center">' +delivery_manager_dump+ '</td>';			
						strVar_excel_onsite_unbilled += ' <td width="6%" font-size="11" align="center">' +proj_category_dump+ '</td>';
						strVar_excel_onsite_unbilled += '	</tr>';
					}
				}
					
				strVar_excel_overall += '	<tr>';
				strVar_excel_overall += ' <td width="6%" font-size="11" align="center">' +region_dump+ '</td>';
				strVar_excel_overall += ' <td width="6%" font-size="11" align="center">' +customer_dump+ '</td>';
				strVar_excel_overall += ' <td width="6%" font-size="11" align="center">' +project_dump+ '</td>';
				strVar_excel_overall += ' <td width="6%" font-size="11" align="center">' +excuting_pract_proj_dump+ '</td>';
				strVar_excel_overall += ' <td width="6%" font-size="11" align="center">' +proj_billing_type_dump+ '</td>';
				strVar_excel_overall += ' <td width="10%" font-size="11" align="left">' +employee_dump+ '</td>';
				strVar_excel_overall += ' <td width="6%" font-size="11" align="center">' +emp_practice_dump+ '</td>';
				strVar_excel_overall += ' <td width="6%" font-size="11" align="center">' +emp_subsidiary_dump+ '</td>';
				strVar_excel_overall += ' <td width="6%" font-size="11" align="center">' +final_prcnt_allocation_dump+ '</td>';
				strVar_excel_overall += ' <td width="6%" font-size="11" align="center">' +is_resource_billable_dump+ '</td>';
				strVar_excel_overall += ' <td width="6%" font-size="11" align="center">' +allo_strt_date_dump+ '</td>';
				strVar_excel_overall += ' <td width="6%" font-size="11" align="center">' +allo_end_date_dump+ '</td>';
				strVar_excel_overall += ' <td width="6%" font-size="11" align="center">' +client_partner_dump+ '</td>';
				strVar_excel_overall += ' <td width="6%" font-size="11" align="center">' +prj_manager_dump+ '</td>';
				strVar_excel_overall += ' <td width="6%" font-size="11" align="center">' +delivery_manager_dump+ '</td>';			
				strVar_excel_overall += ' <td width="6%" font-size="11" align="center">' +proj_category_dump+ '</td>';
				strVar_excel_overall += '	</tr>';


				var day_0_15 = 0;
				var day_16_30 = 0;
				var mnth_1_2 = 0;
				var mnth_2_3 = 0;
				var greater_thn_3 = 0;
				
				var day_0_15_investmnt = 0;
				var day_16_30_investmnt = 0;
				var mnth_1_2_investmnt = 0;
				var mnth_2_3_investmnt = 0;
				var greater_thn_3_investmnt = 0;
				
				var billable_onsite = 0;
				var billable_offsite = 0;
				var unbillable_onsite = 0;
				var unbillable_offsite = 0;
				
				var management_count_offsite = 0;
				var bench_count_offsite = 0;
				var overhead_count_offsite = 0;
				var investment_count_offsite = 0;
				var unbilled_count_offsite = 0;
				
				var management_count_onsite = 0;
				var bench_count_onsite = 0;
				var overhead_count_onsite = 0;
				var investment_count_onsite = 0;
				var unbilled_count_onsite = 0;
				
				var is_resource_billable = project_allocation_result[i_search_indx].getValue('custeventrbillable');
				var emp_susidiary = project_allocation_result[i_search_indx].getValue('subsidiary', 'employee');
				
				var is_proj_bench_category = project_allocation_result[i_search_indx].getValue('custentity_project_allocation_category', 'job');
				
				var prcnt_allocated = project_allocation_result[i_search_indx].getValue('percentoftime');
				prcnt_allocated = prcnt_allocated.toString();
				prcnt_allocated = prcnt_allocated.split('.');
				prcnt_allocated = prcnt_allocated[0].toString().split('%');
				var final_prcnt_allocation = parseFloat(prcnt_allocated) / parseFloat(100);
				final_prcnt_allocation = parseFloat(final_prcnt_allocation);
				
				var alloacation_strt_date = project_allocation_result[i_search_indx].getValue('startdate');
				var alloacation_end_date = project_allocation_result[i_search_indx].getValue('enddate');
				
				var project_name = project_allocation_result[i_search_indx].getText('company');
				project_name = project_name.toString();
				
				var parent_pract_id = project_allocation_result[i_search_indx].getValue('custrecord_parent_practice', 'custevent_practice');
				var parent_pract_name = project_allocation_result[i_search_indx].getText('custrecord_parent_practice', 'custevent_practice');
				var parent_practice_type = project_allocation_result[i_search_indx].getValue('custrecord_practice_type_ticker', 'custevent_practice');
				var parent_practice_is_region = project_allocation_result[i_search_indx].getValue('custrecord_is_region', 'custevent_practice');
				
				if(is_proj_bench_category == 4 || is_proj_bench_category == 2) // Project Category = Bench || Trainee
				{
					var days_diff = getDatediffIndays(alloacation_strt_date,today);
				
					if(parseFloat(days_diff) <= 15)
					{
						day_0_15 = 1;
					}
					else if(parseFloat(days_diff) <= 30 && parseFloat(days_diff) > 15)
					{
						day_16_30 = 1;
					}
					else if(parseFloat(days_diff) <= 60 && parseFloat(days_diff) > 30)
					{
						mnth_1_2 = 1;
					}
					else if(parseFloat(days_diff) <= 90 && parseFloat(days_diff) > 60)
					{
						mnth_2_3 = 1;
					}
					else if(parseFloat(days_diff) > 90)
					{
						greater_thn_3 = 1;
					}
				}
				
				if(is_proj_bench_category == 5) // Project Category = Investment
				{
					var days_diff = getDatediffIndays(alloacation_strt_date,today);
				
					if(parseFloat(days_diff) <= 15)
					{
						day_0_15_investmnt = 1;
					}
					else if(parseFloat(days_diff) <= 30 && parseFloat(days_diff) > 15)
					{
						day_16_30_investmnt = 1;
					}
					else if(parseFloat(days_diff) <= 60 && parseFloat(days_diff) > 30)
					{
						mnth_1_2_investmnt = 1;
					}
					else if(parseFloat(days_diff) <= 90 && parseFloat(days_diff) > 60)
					{
						mnth_2_3_investmnt = 1;
					}
					else if(parseFloat(days_diff) > 90)
					{
						greater_thn_3_investmnt = 1;
					}
				}
				
				if (is_resource_billable == 'T')
				{
					if(emp_susidiary == 3) //offsite
					{
						billable_offsite = parseFloat(final_prcnt_allocation);
					}
					else
					{
						billable_onsite = parseFloat(final_prcnt_allocation);
					}
				}
				else
				{
					if(emp_susidiary == 3) //offsite
					{
						unbillable_offsite = parseFloat(final_prcnt_allocation);
						
						if(parseInt(is_proj_bench_category) == 4 || is_proj_bench_category == 2) // Project Category = Bench || Trainee
						{
							bench_count_offsite = parseFloat(final_prcnt_allocation);
						}
						else if(parseInt(is_proj_bench_category) == 5) // Project Category = Investment
						{
							investment_count_offsite = parseFloat(final_prcnt_allocation);
						}
						else if(parseInt(is_proj_bench_category) == 3) // Project Category = Management
						{
							if(project_name.indexOf("BU Management") >= 0)
							{
								overhead_count_offsite = parseFloat(final_prcnt_allocation);
							}
							else
							{
								management_count_offsite = parseFloat(final_prcnt_allocation);
							}
						}
						else if(parseInt(is_proj_bench_category) == 1) // Project Category = Revenue
						{
							unbilled_count_offsite = parseFloat(final_prcnt_allocation);
						}
					}
					else
					{
						unbillable_onsite = parseFloat(final_prcnt_allocation);
						
						if(parseInt(is_proj_bench_category) == 4 || parseInt(is_proj_bench_category) == 2) // Project Category = Bench || Trainee
						{
							bench_count_onsite = parseFloat(final_prcnt_allocation);
						}
						else if(parseInt(is_proj_bench_category) == 5) // Project Category = Investment
						{
							investment_count_onsite = parseFloat(final_prcnt_allocation);
						}
						else if(parseInt(is_proj_bench_category) == 3) // Project Category = Management
						{
							if(project_name.indexOf("BU Management") >= 0)
							{
								overhead_count_onsite = parseFloat(final_prcnt_allocation);
							}
							else
							{
								management_count_onsite = parseFloat(final_prcnt_allocation);
							}
						}
						else if(parseInt(is_proj_bench_category) == 1) // Project Category = Revenue
						{
							unbilled_count_onsite = parseFloat(final_prcnt_allocation);
						}
					}
				}
					
				// if practice already exist in array list
				if(unique_practice_arr.indexOf(parent_pract_id) >= 0)
				{
					var practice_already_exist_index = unique_practice_arr.indexOf(parent_pract_id);
					
					var day_0_15_update = parseFloat(practice_data_arr[practice_already_exist_index].day_0_15) + parseFloat(day_0_15);
					var day_16_30_update = parseFloat(practice_data_arr[practice_already_exist_index].day_16_30) + parseFloat(day_16_30);
					var mnth_1_2_update = parseFloat(practice_data_arr[practice_already_exist_index].mnth_1_2) + parseFloat(mnth_1_2);
					var mnth_2_3_update = parseFloat(practice_data_arr[practice_already_exist_index].mnth_2_3) + parseFloat(mnth_2_3);
					var greater_thn_3_update = parseFloat(practice_data_arr[practice_already_exist_index].greater_thn_3) + parseFloat(greater_thn_3);
					
					var day_0_15_investmnt_update = parseFloat(practice_data_arr[practice_already_exist_index].day_0_15_investmnt) + parseFloat(day_0_15_investmnt);
					var day_16_30_investmnt_update = parseFloat(practice_data_arr[practice_already_exist_index].day_16_30_investmnt) + parseFloat(day_16_30_investmnt);
					var mnth_1_2_investmnt_update = parseFloat(practice_data_arr[practice_already_exist_index].mnth_1_2_investmnt) + parseFloat(mnth_1_2_investmnt);
					var mnth_2_3_investmnt_update = parseFloat(practice_data_arr[practice_already_exist_index].mnth_2_3_investmnt) + parseFloat(mnth_2_3_investmnt);
					var greater_thn_3_investmnt_update = parseFloat(practice_data_arr[practice_already_exist_index].greater_thn_3_investmnt) + parseFloat(greater_thn_3_investmnt);
					
					var billable_onsite_update = parseFloat(practice_data_arr[practice_already_exist_index].billable_onsite) + parseFloat(billable_onsite);
					var billable_offsite_update = parseFloat(practice_data_arr[practice_already_exist_index].billable_offsite) + parseFloat(billable_offsite);
					var unbillable_onsite_update = parseFloat(practice_data_arr[practice_already_exist_index].unbillable_onsite) + parseFloat(unbillable_onsite);
					var unbillable_offsite_update = parseFloat(practice_data_arr[practice_already_exist_index].unbillable_offsite) + parseFloat(unbillable_offsite);
					 
					var management_count_onsite_update = parseFloat(practice_data_arr[practice_already_exist_index].management_count_onsite) + parseFloat(management_count_onsite);
					var bench_count_onsite_update = parseFloat(practice_data_arr[practice_already_exist_index].bench_count_onsite) + parseFloat(bench_count_onsite);
					var overhead_count_onsite_update = parseFloat(practice_data_arr[practice_already_exist_index].overhead_count_onsite) + parseFloat(overhead_count_onsite);
					var investment_count_onsite_update = parseFloat(practice_data_arr[practice_already_exist_index].investment_count_onsite) + parseFloat(investment_count_onsite);
					var unbilled_count_onsite_update = parseFloat(practice_data_arr[practice_already_exist_index].unbilled_count_onsite) + parseFloat(unbilled_count_onsite);
					
					var management_count_offsite_update = parseFloat(practice_data_arr[practice_already_exist_index].management_count_offsite) + parseFloat(management_count_offsite);
					var bench_count_offsite_update = parseFloat(practice_data_arr[practice_already_exist_index].bench_count_offsite) + parseFloat(bench_count_offsite);
					var overhead_count_offsite_update = parseFloat(practice_data_arr[practice_already_exist_index].overhead_count_offsite) + parseFloat(overhead_count_offsite);
					var investment_count_offsite_update = parseFloat(practice_data_arr[practice_already_exist_index].investment_count_offsite) + parseFloat(investment_count_offsite);
					var unbilled_count_offsite_update = parseFloat(practice_data_arr[practice_already_exist_index].unbilled_count_offsite) + parseFloat(unbilled_count_offsite);
						
					practice_data_arr[practice_already_exist_index] = {
										'parent_pract_name' : parent_pract_name,
										'parent_pracst_id' : parent_pract_id,
										'day_0_15' : day_0_15_update,
										'day_16_30': day_16_30_update,
										'mnth_1_2': mnth_1_2_update,
										'mnth_2_3': mnth_2_3_update,
										'greater_thn_3': greater_thn_3_update,
										'day_0_15_investmnt' : day_0_15_investmnt_update,
										'day_16_30_investmnt': day_16_30_investmnt_update,
										'mnth_1_2_investmnt': mnth_1_2_investmnt_update,
										'mnth_2_3_investmnt': mnth_2_3_investmnt_update,
										'greater_thn_3_investmnt': greater_thn_3_investmnt_update,
										'billable_onsite': billable_onsite_update,
										'billable_offsite': billable_offsite_update,
										'unbillable_onsite': unbillable_onsite_update,
										'unbillable_offsite': unbillable_offsite_update,
										'parent_practice_type': parent_practice_type,
										'parent_practice_is_region': parent_practice_is_region,
										'management_count_onsite': management_count_onsite_update,
										'bench_count_onsite': bench_count_onsite_update,
										'overhead_count_onsite': overhead_count_onsite_update,
										'investment_count_onsite': investment_count_onsite_update,
										'unbilled_count_onsite': unbilled_count_onsite_update,
										'management_count_offsite': management_count_offsite_update,
										'bench_count_offsite': bench_count_offsite_update,
										'overhead_count_offsite': overhead_count_offsite_update,
										'investment_count_offsite': investment_count_offsite_update,
										'unbilled_count_offsite': unbilled_count_offsite_update
										};
					
				}
				else
				{	
					practice_data_arr[sr_no] = {
										'parent_pract_name': parent_pract_name,
										'parent_pract_id': parent_pract_id,
										'day_0_15' : day_0_15,
										'day_16_30': day_16_30,
										'mnth_1_2': mnth_1_2,
										'mnth_2_3': mnth_2_3,
										'greater_thn_3': greater_thn_3,
										'day_0_15_investmnt' : day_0_15_investmnt,
										'day_16_30_investmnt': day_16_30_investmnt,
										'mnth_1_2_investmnt': mnth_1_2_investmnt,
										'mnth_2_3_investmnt': mnth_2_3_investmnt,
										'greater_thn_3_investmnt': greater_thn_3_investmnt,
										'billable_onsite': billable_onsite,
										'billable_offsite': billable_offsite,
										'unbillable_onsite': unbillable_onsite,
										'unbillable_offsite': unbillable_offsite,
										'parent_practice_type': parent_practice_type,
										'parent_practice_is_region': parent_practice_is_region,
										'management_count_onsite': management_count_onsite,
										'bench_count_onsite': bench_count_onsite,
										'overhead_count_onsite': overhead_count_onsite,
										'investment_count_onsite': investment_count_onsite,
										'unbilled_count_onsite': unbilled_count_onsite,
										'management_count_offsite': management_count_offsite,
										'bench_count_offsite': bench_count_offsite,
										'overhead_count_offsite': overhead_count_offsite,
										'investment_count_offsite': investment_count_offsite,
										'unbilled_count_offsite': unbilled_count_offsite
										};
					sr_no++;		
								
					unique_practice_arr.push(parent_pract_id);
				}
			}
			
			strVar_excel_onsite_unbilled += '</table>';
			strVar_excel_onsite_unbilled += ' </td>';	
			strVar_excel_onsite_unbilled += '	</tr>';
			strVar_excel_onsite_unbilled += '</table>';
			
			var excel_file_obj_onsite_unbilled = generate_excel(strVar_excel_onsite_unbilled,1);
							
			strVar_excel_overall += '</table>';
			strVar_excel_overall += ' </td>';	
			strVar_excel_overall += '	</tr>';
			strVar_excel_overall += '</table>';
			
			var excel_file_obj_overall = generate_excel(strVar_excel_overall,0);
			
			send_mail(practice_data_arr,excel_file_obj_onsite_unbilled,excel_file_obj_overall,last_friday_date);
		}
	}
	catch(err)
	{
		nlapiLogExecution('ERROR','ERROR MESSAGE :- ',err);
	}
}

function send_mail(practice_data_arr,excel_file_obj_onsite_unbilled,excel_file_obj_overall,last_friday_date)
{
	var today = new Date(last_friday_date);
	var dd = today.getDate();
	var mm = today.getMonth()+1;
	var yyyy = today.getFullYear();
	var hours = today.getHours();
	var minutes = today.getMinutes();
	
	if(dd<10){
	    dd='0'+dd
	} 
	if(mm<10){
	    mm='0'+mm
	} 
	today = mm+'/'+dd+'/'+yyyy;
	
	var strVar = '';
	strVar += '<html>';
	strVar += '<body>';
	
	strVar += '<p>Weekly Headcount and Utilization '+today+'</p>';
		
	strVar += '<table>';
	
		strVar += '	<tr>';
		strVar += ' <td width="100%">Summary Table';
		strVar += ' </td>';	
		strVar += '	</tr>';
		
		// Table For Practice Wise BreakUp for Onsite Billed and Unbilled
		strVar += '	<tr>';
		strVar += ' <td width="100%">';
		strVar += '<table width="100%" border="1">';
	
		strVar += '	<tr>';
		strVar += ' <td width="25%" font-size="11" align="center" bgcolor="#404040"><font color="#ffffff">Practice Name</font></td>';
		strVar += ' <td width="12.5%" font-size="11" align="center" bgcolor="#404040"><font color="#ffffff">Offsite Billed</font></td>';
		strVar += ' <td width="12.5%" font-size="11" align="center" bgcolor="#404040"><font color="#ffffff">Offsite Unbilled</font></td>';
		strVar += ' <td width="12.5%" font-size="11" align="center" bgcolor="#404040"><font color="#ffffff">Offsite Total</font></td>';
		strVar += ' <td width="12.5%" font-size="11" align="center" bgcolor="#404040"><font color="#ffffff">Onsite Billed</font></td>';
		strVar += ' <td width="12.5%" font-size="11" align="center" bgcolor="#404040"><font color="#ffffff">Onsite Unbilled</font></td>';
		strVar += ' <td width="12.5%" font-size="11" align="center" bgcolor="#404040"><font color="#ffffff">Onsite Total</font></td>';
		strVar += ' <td width="12.5%" font-size="11" align="center" bgcolor="#404040"><font color="#ffffff">Offsite Billed %</font></td>';
		strVar += ' <td width="12.5%" font-size="11" align="center" bgcolor="#404040"><font color="#ffffff">Onsite Billed %</font></td>';
		strVar += ' <td width="12.5%" font-size="11" align="center" bgcolor="#404040"><font color="#ffffff">Overall Billed %</font></td>';
		strVar += '	</tr>';
		
		for (var parent_index = 0; parent_index < practice_data_arr.length; parent_index++)
		{
			if(parseInt(practice_data_arr[parent_index].parent_practice_type) == 2 || practice_data_arr[parent_index].parent_practice_is_region == 'T')
			{
				var total_onsite_count = parseFloat(practice_data_arr[parent_index].billable_onsite) + parseFloat(practice_data_arr[parent_index].unbillable_onsite);
				var total_offsite_count = parseFloat(practice_data_arr[parent_index].billable_offsite) + parseFloat(practice_data_arr[parent_index].unbillable_offsite);
				
				var total_count = parseFloat(total_onsite_count) + parseFloat(total_offsite_count);
				if(total_onsite_count == 0)
				{
					var prcnt_onsite = 0;
				}
				else
				{
					var prcnt_onsite = (parseFloat(practice_data_arr[parent_index].billable_onsite) / parseFloat(total_onsite_count)) * 100;
				}
				
				if(total_offsite_count == 0)
				{
					var prcnt_offsite = 0;
				}
				else
				{
					var prcnt_offsite = (parseFloat(practice_data_arr[parent_index].billable_offsite) / parseFloat(total_offsite_count)) * 100;
				}
				
				var overall_prcnt = parseFloat(parseFloat(practice_data_arr[parent_index].billable_onsite) + parseFloat(practice_data_arr[parent_index].billable_offsite)) / parseFloat(total_count);
				overall_prcnt = overall_prcnt * 100;
				
				strVar += '	<tr>';
				strVar += ' <td width="25%" font-size="11" align="center" bgcolor="#F2DCDB">'+ practice_data_arr[parent_index].parent_pract_name +'</td>';
				strVar += ' <td width="12.5%" font-size="11" align="center">'+ parseFloat(practice_data_arr[parent_index].billable_offsite).toFixed(2) +'</td>';
				strVar += ' <td width="12.5%" font-size="11" align="center">'+ parseFloat(practice_data_arr[parent_index].unbillable_offsite).toFixed(2) +'</td>';
				strVar += ' <td width="12.5%" font-size="11" align="center" bgcolor="#F2DCDB">'+ total_offsite_count.toFixed(2) +'</td>';
				strVar += ' <td width="12.5%" font-size="11" align="center">'+ parseFloat(practice_data_arr[parent_index].billable_onsite).toFixed(2) +'</td>';
				strVar += ' <td width="12.5%" font-size="11" align="center">'+ parseFloat(practice_data_arr[parent_index].unbillable_onsite).toFixed(2) +'</td>';
				strVar += ' <td width="12.5%" font-size="11" align="center" bgcolor="#F2DCDB">'+ total_onsite_count.toFixed(2) +'</td>';
				strVar += ' <td width="12.5%" font-size="11" align="center" bgcolor="#F2DCDB">'+ prcnt_offsite.toFixed(2) +'</td>';
				strVar += ' <td width="12.5%" font-size="11" align="center" bgcolor="#F2DCDB">'+ prcnt_onsite.toFixed(2) +'</td>';
				strVar += ' <td width="12.5%" font-size="11" align="center" bgcolor="#F2DCDB">'+ overall_prcnt.toFixed(2) +'</td>';
				strVar += '	</tr>';
			}
		}
	
		strVar += '</table>';
		strVar += ' </td>';	
		strVar += '	</tr>';
	
		strVar += '	<tr height=10px>';
		strVar += ' <td width="100%">';
		strVar += ' </td>';	
		strVar += '	</tr>';
	
		strVar += '	<tr>';
		strVar += ' <td width="100%">Unbilled Summary Table';
		strVar += ' </td>';	
		strVar += '	</tr>';
		
		// Table For Practice Wise BreakUp for Unbilled bifurcation
		strVar += '	<tr>';
		strVar += ' <td width="100%">';
		strVar += '<table width="100%" border="1">';
	
		strVar += '	<tr>';
		strVar += ' <td width="10%" font-size="11" align="center" bgcolor="#404040"><font color="#ffffff">Practice Name</font></td>';
		strVar += ' <td width="6%" font-size="11" align="center" bgcolor="#404040"><font color="#ffffff">Offsite Unbilled</font></td>';
		strVar += ' <td width="6%" font-size="11" align="center" bgcolor="#404040"><font color="#ffffff">Offsite Management</font></td>';
		strVar += ' <td width="6%" font-size="11" align="center" bgcolor="#404040"><font color="#ffffff">Offsite Bench</font></td>';
		strVar += ' <td width="6%" font-size="11" align="center" bgcolor="#404040"><font color="#ffffff">Offsite Overhead</font></td>';
		strVar += ' <td width="6%" font-size="11" align="center" bgcolor="#404040"><font color="#ffffff">Offsite Investment</font></td>';
		strVar += ' <td width="6%" font-size="11" align="center" bgcolor="#404040"><font color="#ffffff">Offsite Unbilled</font></td>';
		strVar += ' <td width="6%" font-size="11" align="center" bgcolor="#404040"><font color="#ffffff">Offsite Total</font></td>';
		strVar += ' <td width="6%" font-size="11" align="center" bgcolor="#404040"><font color="#ffffff">Offsite Unbilled %</font></td>';
		strVar += ' <td width="6%" font-size="11" align="center" bgcolor="#404040"><font color="#ffffff">Onsite Unbilled</font></td>';
		strVar += ' <td width="6%" font-size="11" align="center" bgcolor="#404040"><font color="#ffffff">Onsite Management</font></td>';
		strVar += ' <td width="6%" font-size="11" align="center" bgcolor="#404040"><font color="#ffffff">Onsite Bench</font></td>';
		strVar += ' <td width="6%" font-size="11" align="center" bgcolor="#404040"><font color="#ffffff">Onsite Overhead</font></td>';
		strVar += ' <td width="6%" font-size="11" align="center" bgcolor="#404040"><font color="#ffffff">Onsite Investment</font></td>';
		strVar += ' <td width="6%" font-size="11" align="center" bgcolor="#404040"><font color="#ffffff">Onsite Unbilled</font></td>';
		strVar += ' <td width="6%" font-size="11" align="center" bgcolor="#404040"><font color="#ffffff">Onsite Total</font></td>';
		strVar += ' <td width="6%" font-size="11" align="center" bgcolor="#404040"><font color="#ffffff">Onsite Unbilled %</font></td>';
		strVar += '	</tr>';
		
		// Table for onsite unbilled bifurcation only
		var strVar_onsite_unbilled_separate_mail = '';
		strVar_onsite_unbilled_separate_mail += '<html>';
		strVar_onsite_unbilled_separate_mail += '<body>';
		
		strVar_onsite_unbilled_separate_mail += '<p>Weekly Headcount and Utilization '+today+'</p>';
		
		strVar_onsite_unbilled_separate_mail += '<p>Onsite Unbilled</p>';
		//strVar_onsite_unbilled_separate_mail += '<table width="100%" border="1">';
		
		var strVar_onsite_unbilled = '<table width="100%" border="1">';
		strVar_onsite_unbilled += '	<tr>';
		strVar_onsite_unbilled += ' <td width="10%" font-size="11" align="center" bgcolor="#404040"><font color="#ffffff">Practice Name</font></td>';
		strVar_onsite_unbilled += ' <td width="6%" font-size="11" align="center" bgcolor="#404040"><font color="#ffffff">Management</font></td>';
		strVar_onsite_unbilled += ' <td width="6%" font-size="11" align="center" bgcolor="#404040"><font color="#ffffff">Bench</font></td>';
		strVar_onsite_unbilled += ' <td width="6%" font-size="11" align="center" bgcolor="#404040"><font color="#ffffff">Overhead</font></td>';
		strVar_onsite_unbilled += ' <td width="6%" font-size="11" align="center" bgcolor="#404040"><font color="#ffffff">Investment</font></td>';
		strVar_onsite_unbilled += ' <td width="6%" font-size="11" align="center" bgcolor="#404040"><font color="#ffffff">Unbilled</font></td>';
		strVar_onsite_unbilled += ' <td width="6%" font-size="11" align="center" bgcolor="#404040"><font color="#ffffff">Grand Total</font></td>';
		strVar_onsite_unbilled += '	</tr>';
		
		for (var parent_index = 0; parent_index < practice_data_arr.length; parent_index++)
		{
			if(parseInt(practice_data_arr[parent_index].parent_practice_type) == 2 || practice_data_arr[parent_index].parent_practice_is_region == 'T')
			{
				var total_onsite_count = parseFloat(practice_data_arr[parent_index].billable_onsite) + parseFloat(practice_data_arr[parent_index].unbillable_onsite);
				var total_offsite_count = parseFloat(practice_data_arr[parent_index].billable_offsite) + parseFloat(practice_data_arr[parent_index].unbillable_offsite);
				
				if(total_onsite_count == 0)
				{
					var prcnt_onsite_unbillable = 0;
				}
				else
				{
					var prcnt_onsite_unbillable = (parseFloat(practice_data_arr[parent_index].unbillable_onsite) / parseFloat(total_onsite_count)) * 100;
				}
				
				if(total_offsite_count == 0)
				{
					var prcnt_offsite_unbillable = 0;
				}
				else
				{
					var prcnt_offsite_unbillable = (parseFloat(practice_data_arr[parent_index].unbillable_offsite) / parseFloat(total_offsite_count)) * 100;
				}
				
				strVar += '	<tr>';
				strVar += ' <td width="10%" font-size="11" align="center" bgcolor="#F2DCDB">'+ practice_data_arr[parent_index].parent_pract_name +'</td>';
				strVar += ' <td width="6%" font-size="11" align="center">'+ parseFloat(practice_data_arr[parent_index].unbillable_offsite).toFixed(2) +'</td>';
				strVar += ' <td width="6%" font-size="11" align="center">'+ parseFloat(practice_data_arr[parent_index].management_count_offsite).toFixed(2) +'</td>';
				strVar += ' <td width="6%" font-size="11" align="center">'+ parseFloat(practice_data_arr[parent_index].bench_count_offsite).toFixed(2) +'</td>';
				strVar += ' <td width="6%" font-size="11" align="center">'+ parseFloat(practice_data_arr[parent_index].overhead_count_offsite).toFixed(2) +'</td>';
				strVar += ' <td width="6%" font-size="11" align="center">'+ parseFloat(practice_data_arr[parent_index].investment_count_offsite).toFixed(2) +'</td>';
				strVar += ' <td width="6%" font-size="11" align="center">'+ parseFloat(practice_data_arr[parent_index].unbilled_count_offsite).toFixed(2) +'</td>';
				strVar += ' <td width="6%" font-size="11" align="center" bgcolor="#F2DCDB">'+ total_offsite_count.toFixed(2) +'</td>';
				strVar += ' <td width="6%" font-size="11" align="center" bgcolor="#F2DCDB">'+ prcnt_offsite_unbillable.toFixed(2) +'</td>';
				strVar += ' <td width="6%" font-size="11" align="center">'+ parseFloat(practice_data_arr[parent_index].unbillable_onsite).toFixed(2) +'</td>';
				strVar += ' <td width="6%" font-size="11" align="center">'+ parseFloat(practice_data_arr[parent_index].management_count_onsite).toFixed(2) +'</td>';
				strVar += ' <td width="6%" font-size="11" align="center">'+ parseFloat(practice_data_arr[parent_index].bench_count_onsite).toFixed(2) +'</td>';
				strVar += ' <td width="6%" font-size="11" align="center">'+ parseFloat(practice_data_arr[parent_index].overhead_count_onsite).toFixed(2) +'</td>';
				strVar += ' <td width="6%" font-size="11" align="center">'+ parseFloat(practice_data_arr[parent_index].investment_count_onsite).toFixed(2) +'</td>';
				strVar += ' <td width="6%" font-size="11" align="center">'+ parseFloat(practice_data_arr[parent_index].unbilled_count_onsite).toFixed(2) +'</td>';
				strVar += ' <td width="6%" font-size="11" align="center" bgcolor="#F2DCDB">'+ total_onsite_count.toFixed(2) +'</td>';
				strVar += ' <td width="6%" font-size="11" align="center" bgcolor="#F2DCDB">'+ prcnt_onsite_unbillable.toFixed(2) +'</td>';
				strVar += '	</tr>';
				
				var grand_total_onsite_unbilled = parseFloat(practice_data_arr[parent_index].management_count_onsite) + 
				parseFloat(practice_data_arr[parent_index].bench_count_onsite) + 
				parseFloat(practice_data_arr[parent_index].overhead_count_onsite) + 
				parseFloat(practice_data_arr[parent_index].investment_count_onsite) + 
				parseFloat(practice_data_arr[parent_index].unbilled_count_onsite);
							
				strVar_onsite_unbilled += '	<tr>';
				strVar_onsite_unbilled += ' <td width="10%" font-size="11" align="center" bgcolor="#F2DCDB">'+ practice_data_arr[parent_index].parent_pract_name +'</td>';
				strVar_onsite_unbilled += ' <td width="6%" font-size="11" align="center">'+ parseFloat(practice_data_arr[parent_index].management_count_onsite).toFixed(2) +'</td>';
				strVar_onsite_unbilled += ' <td width="6%" font-size="11" align="center">'+ parseFloat(practice_data_arr[parent_index].bench_count_onsite).toFixed(2) +'</td>';
				strVar_onsite_unbilled += ' <td width="6%" font-size="11" align="center">'+ parseFloat(practice_data_arr[parent_index].overhead_count_onsite).toFixed(2) +'</td>';
				strVar_onsite_unbilled += ' <td width="6%" font-size="11" align="center">'+ parseFloat(practice_data_arr[parent_index].investment_count_onsite).toFixed(2) +'</td>';
				strVar_onsite_unbilled += ' <td width="6%" font-size="11" align="center">'+ parseFloat(practice_data_arr[parent_index].unbilled_count_onsite).toFixed(2) +'</td>';
				strVar_onsite_unbilled += ' <td width="6%" font-size="11" align="center" bgcolor="#F2DCDB">'+ parseFloat(grand_total_onsite_unbilled).toFixed(2) +'</td>';
				strVar_onsite_unbilled += '	</tr>';
			}
		}
	
		strVar_onsite_unbilled += '</table>';
		strVar_onsite_unbilled_separate_mail += strVar_onsite_unbilled;
		strVar_onsite_unbilled_separate_mail += '<p>**For any discrepancy in data, please reach out to business.ops@brillio.com or chetan.barot@brillio.com </p>';	
		strVar_onsite_unbilled_separate_mail += '<p>Thanks & Regards,</p>';
		strVar_onsite_unbilled_separate_mail += '<p>Team IS</p>';
		strVar_onsite_unbilled_separate_mail += '</body>';
		strVar_onsite_unbilled_separate_mail += '</html>';
		
		var email_to_onsite_billed = new Array();
		email_to_onsite_billed[0] = 'Sajee.Sivaraman@brillio.com';
		email_to_onsite_billed[1] = 'Jayanth.Selvappullai@brillio.com';
		
		var cc_email_onsite_billed = new Array();
		cc_email_onsite_billed[0] = 'suchin.bhat@brillio.com';
		cc_email_onsite_billed[1] = 'Bindu.Pandya@brillio.com';
		cc_email_onsite_billed[2] = 'Chetan.barot@brillio.com';
		
		nlapiSendEmail(442, email_to_onsite_billed, 'Headcount report for onsite unbilled weekending dt. '+today, strVar_onsite_unbilled_separate_mail,cc_email_onsite_billed,'jayesh@inspirria.com',null,excel_file_obj_onsite_unbilled);
		
		strVar += '</table>';
		strVar += ' </td>';	
		strVar += '	</tr>';
		
		strVar += '	<tr height=10px>';
		strVar += ' <td width="100%">';
		strVar += ' </td>';	
		strVar += '	</tr>';
	
		strVar += '	<tr>';
		strVar += ' <td width="100%">Onsite Unbilled Table';
		strVar += ' </td>';	
		strVar += '	</tr>';
		
		// Table For Onsite Unbilled
		strVar += '	<tr>';
		strVar += ' <td width="100%">';
		//strVar += '<table width="100%" border="1">';
		
		strVar += strVar_onsite_unbilled;
		
		//strVar += '</table>';
		strVar += ' </td>';	
		strVar += '	</tr>';
		
		strVar += '	<tr height=10px>';
		strVar += ' <td width="100%">';
		strVar += ' </td>';	
		strVar += '	</tr>';
	
		strVar += '	<tr>';
		strVar += ' <td width="100%">Bench Aging';
		strVar += ' </td>';	
		strVar += '	</tr>';
			
		// Table For Practice Wise BreakUp Bench Aging
		strVar += '	<tr>';
		strVar += ' <td width="100%">';
		strVar += '<table width="100%" border="1">';
	
		strVar += '	<tr>';
		strVar += ' <td width="25%" font-size="11" align="center" bgcolor="#404040"><font color="#ffffff">Practice Name</font></td>';
		strVar += ' <td width="12.5%" font-size="11" align="center" bgcolor="#404040"><font color="#ffffff">0 to 15 Days</font></td>';
		strVar += ' <td width="12.5%" font-size="11" align="center" bgcolor="#404040"><font color="#ffffff">16 to 30 Days</font></td>';
		strVar += ' <td width="12.5%" font-size="11" align="center" bgcolor="#404040"><font color="#ffffff">1 to 2 Months</font></td>';
		strVar += ' <td width="12.5%" font-size="11" align="center" bgcolor="#404040"><font color="#ffffff">2 to 3 Months</font></td>';
		strVar += ' <td width="12.5%" font-size="11" align="center" bgcolor="#404040"><font color="#ffffff">> 3 Months</font></td>';
		strVar += ' <td width="12.5%" font-size="11" align="center" bgcolor="#404040"><font color="#ffffff">Grand Total</font></td>';
		strVar += '	</tr>';
		
		for (var parent_index = 0; parent_index < practice_data_arr.length; parent_index++)
		{
			var total_practice_count = parseFloat(practice_data_arr[parent_index].day_0_15) + parseFloat(practice_data_arr[parent_index].day_16_30)+ parseFloat(practice_data_arr[parent_index].mnth_1_2) + parseFloat(practice_data_arr[parent_index].mnth_2_3)+ parseFloat(practice_data_arr[parent_index].greater_thn_3);
			
			if(total_practice_count > 0)
			{
				strVar += '	<tr>';
				strVar += ' <td width="25%" font-size="11" align="center" bgcolor="#F2DCDB">'+ practice_data_arr[parent_index].parent_pract_name +'</td>';
				strVar += ' <td width="12.5%" font-size="11" align="center">'+ practice_data_arr[parent_index].day_0_15 +'</td>';
				strVar += ' <td width="12.5%" font-size="11" align="center">'+ practice_data_arr[parent_index].day_16_30 +'</td>';
				strVar += ' <td width="12.5%" font-size="11" align="center">'+ practice_data_arr[parent_index].mnth_1_2 +'</td>';
				strVar += ' <td width="12.5%" font-size="11" align="center">'+ practice_data_arr[parent_index].mnth_2_3 +'</td>';
				strVar += ' <td width="12.5%" font-size="11" align="center">'+ practice_data_arr[parent_index].greater_thn_3 +'</td>';
				strVar += ' <td width="12.5%" font-size="11" align="center" bgcolor="#F2DCDB">'+ total_practice_count +'</td>';
				strVar += '	</tr>';
			}
		}
	
		strVar += '</table>';
		strVar += ' </td>';	
		strVar += '	</tr>';
		
		strVar += '	<tr height=10px>';
		strVar += ' <td width="100%">';
		strVar += ' </td>';	
		strVar += '	</tr>';
		
		strVar += '	<tr>';
		strVar += ' <td width="100%">Investment Aging';
		strVar += ' </td>';	
		strVar += '	</tr>';
		
		// Table For Practice Wise BreakUp Investment Aging
		strVar += '	<tr>';
		strVar += ' <td width="100%">';
		strVar += '<table width="100%" border="1">';
	
		strVar += '	<tr>';
		strVar += ' <td width="25%" font-size="11" align="center" bgcolor="#404040"><font color="#ffffff">Practice Name</font></td>';
		strVar += ' <td width="12.5%" font-size="11" align="center" bgcolor="#404040"><font color="#ffffff">0 to 15 Days</font></td>';
		strVar += ' <td width="12.5%" font-size="11" align="center" bgcolor="#404040"><font color="#ffffff">16 to 30 Days</font></td>';
		strVar += ' <td width="12.5%" font-size="11" align="center" bgcolor="#404040"><font color="#ffffff">1 to 2 Months</font></td>';
		strVar += ' <td width="12.5%" font-size="11" align="center" bgcolor="#404040"><font color="#ffffff">2 to 3 Months</font></td>';
		strVar += ' <td width="12.5%" font-size="11" align="center" bgcolor="#404040"><font color="#ffffff">> 3 Months</font></td>';
		strVar += ' <td width="12.5%" font-size="11" align="center" bgcolor="#404040"><font color="#ffffff">Grand Total</font></td>';
		strVar += '	</tr>';
		
		for (var parent_index = 0; parent_index < practice_data_arr.length; parent_index++)
		{
			var total_practice_count_investmnt = parseFloat(practice_data_arr[parent_index].day_0_15_investmnt) + parseFloat(practice_data_arr[parent_index].day_16_30_investmnt)+ parseFloat(practice_data_arr[parent_index].mnth_1_2_investmnt) + parseFloat(practice_data_arr[parent_index].mnth_2_3_investmnt)+ parseFloat(practice_data_arr[parent_index].greater_thn_3_investmnt);
			
			if(total_practice_count_investmnt > 0 && parseInt(practice_data_arr[parent_index].parent_pract_id) != 430)
			{
				strVar += '	<tr>';
				strVar += ' <td width="25%" font-size="11" align="center" bgcolor="#F2DCDB">'+ practice_data_arr[parent_index].parent_pract_name +'</td>';
				strVar += ' <td width="12.5%" font-size="11" align="center">'+ practice_data_arr[parent_index].day_0_15_investmnt +'</td>';
				strVar += ' <td width="12.5%" font-size="11" align="center">'+ practice_data_arr[parent_index].day_16_30_investmnt +'</td>';
				strVar += ' <td width="12.5%" font-size="11" align="center">'+ practice_data_arr[parent_index].mnth_1_2_investmnt +'</td>';
				strVar += ' <td width="12.5%" font-size="11" align="center">'+ practice_data_arr[parent_index].mnth_2_3_investmnt +'</td>';
				strVar += ' <td width="12.5%" font-size="11" align="center">'+ practice_data_arr[parent_index].greater_thn_3_investmnt +'</td>';
				strVar += ' <td width="12.5%" font-size="11" align="center" bgcolor="#F2DCDB">'+ total_practice_count_investmnt +'</td>';
				strVar += '	</tr>';
			}
		}
	
		strVar += '</table>';
		strVar += ' </td>';	
		strVar += '	</tr>';
	
	strVar += '</table>';
			
	strVar += '<p>**For any discrepancy in data, please reach out to business.ops@brillio.com or chetan.barot@brillio.com </p>';
			
	strVar += '<p>Thanks & Regards,</p>';
	strVar += '<p>Team IS</p>';
			
	strVar += '</body>';
	strVar += '</html>';
	
	var file_attachment = new Array();
	file_attachment[0] = excel_file_obj_overall;
	file_attachment[1] = excel_file_obj_onsite_unbilled;
	
	var email_to_overall = new Array();
	email_to_overall[0] = 'manu.lavanya@brillio.com';
	email_to_overall[1] = 'atul.kumar@brillio.com';
	email_to_overall[2] = 'Sridhar.K@brillio.com';
	email_to_overall[3] = 'srinivas.guntur@brillio.com';
	email_to_overall[4] = 'amit.gautam@brillio.com';
	email_to_overall[5] = 'Sashidhar.CR@brillio.com';
	email_to_overall[6] = 'hemakumar.s@brillio.com';
	email_to_overall[7] = 'saisandesh.v@brillio.com';
	email_to_overall[8] = 'Kiran.Susarla@brillio.com';
	email_to_overall[9] = 'naresh.agarwal@brillio.com';
	email_to_overall[10] = 'vishal.anand@brillio.com';
	email_to_overall[11] = 'siva.p@brillio.com';
	email_to_overall[12] = 'athresh.krishnappa@brillio.com';
	email_to_overall[13] = 'sudeendra.p@brillio.com';
	email_to_overall[14] = 'Sharathkumar.s@brillio.com';
	
	var cc_email_overall = new Array();
	cc_email_overall[0] = 'Jayanth.Selvappullai@brillio.com';
	cc_email_overall[1] = 'Bindu.Pandya@brillio.com';
	cc_email_overall[2] = 'vimal.pandey@brillio.com';
	cc_email_overall[3] = 'suchin.bhat@brillio.com';
	cc_email_overall[4] = 'Ananda.Hirekerur@brillio.com';
	cc_email_overall[5] = 'maneesh.agarwal@brillio.com';
	cc_email_overall[6] = 'bhavanishankar.t@brillio.com';
	cc_email_overall[7] = 'vertica@brillio.com';
	cc_email_overall[8] = 'Chetan.barot@brillio.com';
		
	nlapiSendEmail(442, email_to_overall, 'Headcount report weekending dt. '+today, strVar,cc_email_overall,'jayesh@inspirria.com',null,file_attachment);
}

function generate_excel(strVar_excel,mode)
{
	var strVar1= '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:ss="urn:schemas-microsoft-com:office:spreadsheet" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40">'+
		'<head>'+
		'<meta http-equiv=Content-Type content="text/html; charset=windows-1252"/>'+
        '<meta name=ProgId content=Excel.Sheet/>'+
        '<meta name=Generator content="Microsoft Excel 11"/>'+
        '<!--[if gte mso 9]><xml>'+
        '<x:excelworkbook>'+
        '<x:excelworksheets>'+
        '<x:excelworksheet=sheet1>'+
        '<x:name>** ESTIMATE FILE**</x:name>'+
        '<x:worksheetoptions>'+
        '<x:selected></x:selected>'+
        '<x:freezepanes></x:freezepanes>'+
        '<x:frozennosplit></x:frozennosplit>'+
        '<x:splithorizontal>** FROZEN ROWS + 1 **</x:splithorizontal>'+
        '<x:toprowbottompane>** FROZEN ROWS + 1 **</x:toprowbottompane>'+
        '<x:splitvertical>** FROZEN COLUMNS + 1 **</x:splitvertical>'+
        '<x:leftcolumnrightpane>** FROZEN COLUMNS + 1**</x:leftcolumnrightpane>'+
		    '<x:activepane>0</x:activepane>'+// 0
		    '<x:panes>'+
			'<x:pane>'+
			'<x:number>3</x:number>'+
			'</x:pane>'+
			'<x:pane>'+
		    '<x:number>1</x:number>'+
		    '</x:pane>'+
		    '<x:pane>'+
			'<x:number>2</x:number>'+
        '</x:pane>'+
        '<x:pane>'+
        '<x:number>0</x:number>'+//1
        '</x:pane>'+
        '</x:panes>'+
        '<x:protectcontents>False</x:protectcontents>'+
        '<x:protectobjects>False</x:protectobjects>'+
        '<x:protectscenarios>False</x:protectscenarios>'+
        '</x:worksheetoptions>'+
        '</x:excelworksheet>'+
		'</x:excelworksheets>'+
        '<x:protectstructure>False</x:protectstructure>'+
        '<x:protectwindows>False</x:protectwindows>'+
		'</x:excelworkbook>'+
        
        // '<style>'+
			
			//-------------------------------------
        '</x:excelworkbook>'+
        '</xml><![endif]-->'+
         '<style>'+
		'p.MsoFooter, li.MsoFooter, div.MsoFooter'+
		'{ margin:0in; margin-bottom:.0001pt; mso-pagination:widow-orphan; tab-stops:center 3.0in right 6.0in; font-size:12.0pt;}'+
		'<style>'+

		'<!-- /* Style Definitions */'+

		//'@page Section1'+
		//'{ size:8.5in 11.0in; margin:1.9cm 1.27cm 1.9cm 1.27cm; mso-header-margin:.5in; mso-footer-margin:.5in; mso-title-page:yes; mso-header: h1; mso-footer: f1; mso-first-header: fh1; mso-first-footer: ff1; mso-paper-source:0;}'+

		'div.Section1'+
		'{ page:Section1;}'+

		'table#hrdftrtbl'+
		'{ margin:0in 0in 0in 900in; width:1px; height:1px; overflow:hidden; font-family:Arial;}-->'+

		'</style>'+
		
		
		'</head>'+
		

		'<body>'+
		
		'<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">'
		
		var strVar2 = strVar_excel;
		strVar1 = strVar1 + strVar2;
		if(mode == 0)
		{
			var file = nlapiCreateFile('Headcount Report.xls', 'XMLDOC', strVar1);
		}
		else
		{
			var file = nlapiCreateFile('Onsite Unbilled Data.xls', 'XMLDOC', strVar1);
		}
		return file;
}

function getDatediffIndays(startDate, endDate)
{
    var one_day=1000*60*60*24;
    var fromDate = startDate;
    var toDate = endDate;

	var date1 = nlapiStringToDate(fromDate);

    var date2 = nlapiStringToDate(toDate);

    var date3=Math.round((date2-date1)/one_day);

    return (date3+1);
}

function searchRecord(recordType, savedSearch, arrFilters, arrColumns,
        filterExpression)
{

	try {
		var search = null;

		// if a saved search is provided, load it and add the filters and
		// columns
		if (isNotEmpty(savedSearch)) {
			search = nlapiLoadSearch(recordType, savedSearch);

			if (isArrayNotEmpty(arrFilters)) {
				search.addFilters(arrFilters);
			}

			if (isArrayNotEmpty(arrColumns)) {
				search.addColumns(arrColumns);
			}

			if (isArrayNotEmpty(filterExpression)) {
				search.setFilterExpression(filterExpression);
			}
		}
		// create a new search
		else {
			search = nlapiCreateSearch(recordType, arrFilters, arrColumns);
		}

		// run search
		var resultSet = search.runSearch();

		// iterate through the search and get all data 1000 at a time
		var searchResultCount = 0;
		var resultSlice = null;
		var searchResult = [];

		do {
			resultSlice = resultSet.getResults(searchResultCount,
			        searchResultCount + 1000);

			if (resultSlice) {

				resultSlice.forEach(function(result) {

					searchResult.push(result);
					searchResultCount++;
				});
			}
		} while (isArrayNotEmpty(resultSlice) && resultSlice.length >= 1000);

		return searchResult;
	} catch (err) {
		nlapiLogExecution('ERROR', 'searchRecord', err);
		throw err;
	}
}

function isEmpty(value) {

	return value == null || value == "" || typeof (value) == undefined;
}

function isNotEmpty(value) {

	return !isEmpty(value);
}

function isArrayEmpty(argArray) {

	return !isArrayNotEmpty(argArray);
}

function isArrayNotEmpty(argArray) {

	return (isNotEmpty(argArray) && argArray.length > 0);
}