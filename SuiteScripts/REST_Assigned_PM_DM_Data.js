/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       19 Apr 2019     Aazamali Khan
 *
 */

/**
 * @param {Object} dataIn Parameter object
 * @returns {Object} Output object
 */
var data = [];
function getRESTlet(dataIn) {
	try {
		var s_user = dataIn.user;
		var i_user = getUserUsingEmailId(s_user);
		// Get Logged In user's executing practice
		var practiceHead = GetPracticeHead(i_user);
		var spocSearch = GetPracticeSPOC(i_user);
		//var regionSearch = GetRegion(i_user);
		var adminUser = GetAdmin(i_user);
		var spoc
		if (adminUser) {
			// Create JSON for dashboard table when logged in user in practice SPOC
			GetDashBoardDataOpportunities(null, null, true, null);
		} else if (practiceHead) {
			var i_practice = practiceHead;
			//nlapiLogExecution("DEBUG", "i_practice", i_practice);
			// Create JSON for dashboard table when logged in user in practice SPOC
			GetDashBoardDataOpportunities(i_user, i_practice, null, null);
		} else if(spocSearch){
			var i_practice = spocSearch;
			//nlapiLogExecution("DEBUG", "i_practice", i_practice);
			// Create JSON for dashboard table when logged in user in practice SPOC
			GetDashBoardDataOpportunities(i_user, i_practice, null, null);
		}
		return { "data" : data };
	} catch (e) {
		//nlapiLogExecution("DEBUG", "message : ", e);
		return {
			"code": "error",
			"message": e
		}
	}
}
function GetDashBoardDataOpportunities(i_user, i_practice, isAdmin,i_region) {
	var filters = new Array();
	var columns = new Array();
	if(isAdmin==true)
	{
		filters = [
		           ["isinactive","is","F"],"AND",["custrecord_fulfill_dashboard_opp_id","noneof","@NONE@"]
		           ,"AND",
		           ["custrecord_fulfill_dashboard_opp_id.custrecord_stage_sfdc","noneof","8","9"],"AND",["custrecord_fulfill_dashboard_opp_id.custrecord_projection_status_sfdc","noneof","3"]
		           ]; // Need to add closed filter
	}
	else if(i_user && i_practice)
	{
		var practiceArray = getSubPractices(i_practice)
		filters = [
		           ["custrecord_fulfill_dashboard_opp_id","noneof","@NONE@"],
		           "AND",
		           ["isinactive","is","F"]
		           ,"AND",
		           ["custrecord_fulfill_dashboard_opp_id.custrecord_stage_sfdc","noneof","8","9"],
		           "AND",
		           ["custrecord_fulfill_dashboard_opp_id.custrecord_practice_internal_id_sfdc","anyof",practiceArray]
		           ]
	}

	columns = [new nlobjSearchColumn("custrecord_fulfill_dashboard_project"),
	           new nlobjSearchColumn("custrecord_fulfill_dashboard_account"),
	           new nlobjSearchColumn("custrecord_fulfill_dashboard_rev_sta_pro"),
	           new nlobjSearchColumn("custrecord_fulfill_dashboard_reve_confid"),
	           new nlobjSearchColumn("custrecord_fulfill_dashboard_manager"),
	           new nlobjSearchColumn("custrecord_fulfill_dashboard_practice"),
	           new nlobjSearchColumn("custrecord_fulfill_dashboard_pro_team"),
	           new nlobjSearchColumn("custrecord_fulfill_dashboard_rampup"),
	           new nlobjSearchColumn("custrecord_fulfill_dashboard_exiting"),
	           new nlobjSearchColumn("custrecord_fulfill_dashboard_frf").setSort(false),
	           new nlobjSearchColumn("custrecord_fulfill_dashboard_rrf"),
	           // new nlobjSearchColumn("custrecord_fulfill_dashboard_track"),
	           new nlobjSearchColumn("custrecord_fulfill_dashboard_opp_id"),
	           new nlobjSearchColumn("custrecord_fulfill_dashboard_last_update"),
	           new nlobjSearchColumn("custrecord_fulfill_dashboard_start_date"),
	           new nlobjSearchColumn("custrecord_fulfill_dashboard_end_date"),       
	           new nlobjSearchColumn("custrecord_opportunity_name_sfdc", "CUSTRECORD_FULFILL_DASHBOARD_OPP_ID", null),
	           new nlobjSearchColumn("custrecord_opp_account_region", "CUSTRECORD_FULFILL_DASHBOARD_OPP_ID", null),
	           new nlobjSearchColumn("custrecord_customer_internal_id_sfdc", "CUSTRECORD_FULFILL_DASHBOARD_OPP_ID", null),
	           new nlobjSearchColumn("custentity_projectmanager", "CUSTRECORD_FULFILL_DASHBOARD_PROJECT", null),
	           new nlobjSearchColumn("custrecord_customer_internal_id_sfdc", "CUSTRECORD_FULFILL_DASHBOARD_OPP_ID", null),
	           new nlobjSearchColumn("customer", "CUSTRECORD_FULFILL_DASHBOARD_PROJECT", null),
	           new nlobjSearchColumn("custrecord_sfdc_opp_pm", "CUSTRECORD_FULFILL_DASHBOARD_OPP_ID", null),
	           new nlobjSearchColumn("custrecord_sfdc_opportunity_dm", "CUSTRECORD_FULFILL_DASHBOARD_OPP_ID", null),
	           new nlobjSearchColumn("custrecord_stage_sfdc", "CUSTRECORD_FULFILL_DASHBOARD_OPP_ID", null)
	]
	var searchResult = searchRecord("customrecord_fulfillment_dashboard_data", null, filters, columns, null);
	//nlapiLogExecution("DEBUG", "filters", filters);
	//nlapiLogExecution("DEBUG", "searchResult", searchResult.length);
	if(searchResult){
		for (var int = 0; int < searchResult.length; int++) {
			var jsonObj = {};
			jsonObj.name = searchResult[int].getValue("custrecord_opportunity_name_sfdc", "CUSTRECORD_FULFILL_DASHBOARD_OPP_ID", null);
			jsonObj.opportunityID = searchResult[int].getValue("CUSTRECORD_FULFILL_DASHBOARD_OPP_ID");
			jsonObj.type =  searchResult[int].getValue("custrecord_fulfill_dashboard_project") ? "Project" : "New Opportunity";
			jsonObj.project = searchResult[int].getText("custrecord_fulfill_dashboard_project") ? searchResult[int].getText("custrecord_fulfill_dashboard_project") : "";
			jsonObj.account = searchResult[int].getText("custrecord_fulfill_dashboard_account") ? searchResult[int].getText("custrecord_fulfill_dashboard_account") : "";
			jsonObj.practice = searchResult[int].getText("custrecord_fulfill_dashboard_practice");
			jsonObj.revenuestatus = searchResult[int].getValue("custrecord_fulfill_dashboard_rev_sta_pro");
			jsonObj.startDate = searchResult[int].getValue("custrecord_fulfill_dashboard_start_date");
			jsonObj.endDate = searchResult[int].getValue("custrecord_fulfill_dashboard_end_date");
			jsonObj.stage = searchResult[int].getText("custrecord_stage_sfdc", "CUSTRECORD_FULFILL_DASHBOARD_OPP_ID", null);
			var arrEmpPM = searchResult[int].getValue("custrecord_sfdc_opp_pm", "CUSTRECORD_FULFILL_DASHBOARD_OPP_ID", null);
			//nlapiLogExecution("ERROR","arrEmpPM",arrEmpPM);
			var arrEmpPMName = searchResult[int].getText("custrecord_sfdc_opp_pm", "CUSTRECORD_FULFILL_DASHBOARD_OPP_ID", null);
			//nlapiLogExecution("ERROR","arrEmpPMName",arrEmpPMName);
			var arrPM = [];
			if(arrEmpPM){
				arrEmpPM = getSkillIds(arrEmpPM);
				arrEmpPMName = getSkillIds(arrEmpPMName);
				if(isArrayNotEmpty(arrEmpPM)){
					if(arrEmpPM.length == 1){
						var obj = {};
						obj = {"name" : arrEmpPMName[0],"id" : arrEmpPM[0]}	
						arrPM.push(obj);
					}else{
						for(var i =0 ; i < arrEmpPM.length ; i++){
							var obj = {};	
								obj = {"name" : nlapiLookupField("employee", arrEmpPM[i], "entityid"),"id" : arrEmpPM[i]}	
								arrPM.push(obj);
						}
					}
				}
			}
			var arrEmpDM = searchResult[int].getValue("custrecord_sfdc_opportunity_dm", "CUSTRECORD_FULFILL_DASHBOARD_OPP_ID", null);
			nlapiLogExecution("ERROR","arrEmpDM",arrEmpDM);
			var arrEmpDMName = searchResult[int].getText("custrecord_sfdc_opportunity_dm", "CUSTRECORD_FULFILL_DASHBOARD_OPP_ID", null);
			nlapiLogExecution("ERROR","arrEmpDMName",arrEmpDMName);
			var arrDM = [];
			if(arrEmpDM){
				arrEmpDM = getSkillIds(arrEmpDM);
				arrEmpDMName = getSkillIds(arrEmpDMName);
				if(isArrayNotEmpty(arrEmpDM)){
					if(arrEmpDM.length == 1){
						var obj = {};
						obj = {"name" : arrEmpDMName[0],"id" : arrEmpDM[0]}	
						arrDM.push(obj);
					}else{
						for(var i =0 ; i < arrEmpDM.length ; i++){
							var obj = {};	
								obj = {"name" : nlapiLookupField("employee", arrEmpDM[i], "entityid"),"id" : arrEmpDM[i]}	
								arrDM.push(obj);
						}
					}
				}
				/*for(var i =0 ; i < arrEmpDM.length ; i++){
					var obj = {};
					if( i == 1){
						obj = {"name" : arrEmpDMName[i],"id" : arrEmpDM[i]}	
						arrDM.push(obj);
					}else{
						obj = {"name" : nlapiLookupField("employee", arrEmpDM[i], "entityid"),"id" : arrEmpDM[i]}	
						arrDM.push(obj);
					}
				}*/
			}
			jsonObj.projectmanager = arrPM ;
			jsonObj.deliverymanager = arrDM ;
			var opportunityID = searchResult[int].getValue("CUSTRECORD_FULFILL_DASHBOARD_OPP_ID");
			var s_practice = searchResult[int].getText("custrecord_fulfill_dashboard_practice");
			var practioner = getOpportunityDetails(opportunityID,s_practice);
			jsonObj.practionerPractice = practioner;
			data.push(jsonObj);
		}
	}
}
function getSkillIds(s_skills)
{

	var resultArray = new Array();
	if(s_skills)
	{
		//nlapiLogExecution('Debug','s_skills in function',s_skills);
		var temp = s_skills.split(',');
		for(var i=0; i<temp.length;i++)
		{

			resultArray.push(temp[i]);

		}
	}
	return resultArray;
}
function GetPracticeHead(user){

	var practiceArray = new Array();
	var searchRes  = nlapiSearchRecord('department',null,
			[[
			  ["custrecord_practicehead","anyof",user],"OR",
			  ["custrecord_onsite_practice_head","anyof",user]],"AND",
			  ["isinactive","is","F"],"AND",
			  ["custrecord_is_delivery_practice","is","T"],"AND",
			  ["custrecord_parent_practice","noneof","@NONE@"]],

			  [new nlobjSearchColumn('internalid')]);

	if(searchRes)
	{
		for(var i=0; i<searchRes.length;i++)
		{
			practiceArray.push(searchRes[i].getValue('internalid'));
		}
		return practiceArray
	}

	else 
		return '';
}
function getSubPractices(i_practice){
	var temp = [];
	var departmentSearch = nlapiSearchRecord("department",null,
			[
			 ["isinactive","is","F"], 
			 "AND", 
			 ["custrecord_parent_practice","anyof",i_practice]
			 ], 
			 [
			  ]
	);
	if(departmentSearch){
		for(var i = 0 ; i < departmentSearch.length ;i++){
			temp.push(departmentSearch[i].getId());
		}
		return temp;
	}else{
		return i_practice
	}
}
function getOpportunityDetails(oppId,practiceName) {
	var tempArray = [];
	var customrecord_sfdc_opportunity_recordSearch = nlapiSearchRecord("customrecord_sfdc_opportunity_record", null,
			[
			 ["internalidnumber", "equalto", oppId]
			 ],
			 [
			  new nlobjSearchColumn("custrecord_fulfill_plan_practice", "CUSTRECORD_FULFILL_PLAN_OPP_ID", null)
			  ]
	);
	if (customrecord_sfdc_opportunity_recordSearch) {
		for (var i = 0; i < customrecord_sfdc_opportunity_recordSearch.length; i++) {
			var s_practice = customrecord_sfdc_opportunity_recordSearch[i].getValue("custrecord_fulfill_plan_practice", "CUSTRECORD_FULFILL_PLAN_OPP_ID", null);
			if(practiceName != s_practice){
				if(s_practice != ""){
					tempArray.push(s_practice); 
				}	
			}

		}
	}
	return tempArray;
}