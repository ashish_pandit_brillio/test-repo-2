/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       25 Sep 2017     bidhi.raj
 *
 */

/**
 * @param {nlobjRequest}
 *            request Request object
 * @param {nlobjResponse}
 *            response Response object
 * @returns {Void} Any output is written via response object
 */
function suitelet(request, response) {
	//try {
		if (request.getMethod() == 'GET') {
			var form = nlapiCreateForm('PR - Item Approval History Template');
			 form.addField('custpage_startdate', 'Date',
			'PR_START Date :').setMandatory(true);
	        form.addField('custpage_enddate', 'Date',
			'PR_END Date :').setMandatory(true);
			form.addSubmitButton('Get Report');
			response.writePage(form);
		}

		else {
			var strt_select = request.getParameter('custpage_startdate');
			var end_select = request.getParameter('custpage_enddate');
			var params =[];
			var params = {
					custscriptstart_date :strt_select,
					custscriptend_select :end_select
					
					};
			
			nlapiScheduleScript('customscript1459', 'customdeploy1', params);
			var form2 = nlapiCreateForm('Report will be sent to your mail');
			response.writePage(form2);
		}
	
}
	
