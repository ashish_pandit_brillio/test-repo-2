// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
/*
   	Script Name : SCH_DailyTicker_ADO.js 
	Author      : Jayesh Dinde
	Date        : 15 Sep 2016
    Description : Send Daily ticker mail to ADO.


	Script Modification Log:

	-- Date --			-- Modified By --				--Requested By--				-- Description --




Below is a summary of the process controls enforced by this script file.  The control logic is described
more fully, below, in the appropriate function headers and code blocks.


     SCHEDULED FUNCTION
		- scheduledFunction(type)


     SUB-FUNCTIONS
		- The following sub-functions are called by the above core functions in order to maintain code
            modularization:

               - NOT USED

*/
}
// END SCRIPT DESCRIPTION BLOCK  ====================================


// BEGIN SCHEDULED FUNCTION =============================================

function CP_ticker()
{
	try
	{
		var filters_customrecord_revenue_location_subsidiarySearch = ["custrecord_offsite_onsite", "anyof", "2"];
        var obj_rev_loc_subSrch = Search_revenue_Location_subsdidary(filters_customrecord_revenue_location_subsidiarySearch);
        var obj_offsite_onsite = obj_rev_loc_subSrch["Onsite/Offsite"];

		var context = nlapiGetContext();
		var last_region_id = '';
		var icnt 	= context.getSetting('script', 'custscript_last_region_processed_id');
		nlapiLogExecution('audit','icnt:- ',icnt);
		if (icnt == null || icnt == '' || icnt == undefined) 
        {
             last_region_id = '';
        }
        else 
        {
             last_region_id = icnt;
        }
		
		var billing_from_date = nlapiStringToDate(nlapiDateToString(new Date()));
		
		var filters_search_regions = new Array();
		filters_search_regions[0] = new nlobjSearchFilter('isinactive', null, 'is', 'F'); //east 1484
		
		if(_logValidation(last_region_id))
		{
			filters_search_regions[1] = new nlobjSearchFilter('internalidnumber', null, 'greaterthan', last_region_id);
		}
		
		var column_search_region = new Array();
        column_search_region[0] = new nlobjSearchColumn('internalid').setSort(true);
		column_search_region[1] = new nlobjSearchColumn('email','custrecord_region_head');
		column_search_region[2] = new nlobjSearchColumn('name');
		
		var region_head_master_result = nlapiSearchRecord('customrecord_region', null, filters_search_regions, column_search_region);
		if (_logValidation(region_head_master_result))
		{
			//for(var region_index=0; region_index<region_head_master_result.length; region_index++)
			{	
				var cust_unique_list = new Array();
				var proj_unique_list = new Array();
				var proj_deatils_arr = new Array();
				var region_management_count = new Array();
				var region_list = new Array();
				var sr_no = 0;
				var region_sr_no = 0;
				
				//var region_id = region_head_master_result[region_index].getId();
				//var region_head_mail = region_head_master_result[region_index].getValue('email','custrecord_region_head');
			//	var region_name = region_head_master_result[region_index].getValue('name');
				
				yieldScript(context);
				
				var filters_search_allocation = new Array();
				//filters_search_allocation[0] = new nlobjSearchFilter('custentity_region', 'customer', 'anyof', parseInt(region_id));
				filters_search_allocation[0] = new nlobjSearchFilter('startdate', null, 'onorbefore', billing_from_date);
				filters_search_allocation[1] = new nlobjSearchFilter('enddate', null, 'onorafter', billing_from_date);
				filters_search_allocation[2] = new nlobjSearchFilter('custentity_employee_inactive', 'employee', 'is', 'F');
              filters_search_allocation[3] = new nlobjSearchFilter('custentity_account_delivery_owner', 'customer', 'isnotempty');
				
				
				var columns = new Array();
				columns[0] = new nlobjSearchColumn('custeventrbillable');
				columns[1] = new nlobjSearchColumn('jobbillingtype','job');
				columns[2] = new nlobjSearchColumn('custentity_clientpartner','job');
				columns[3] = new nlobjSearchColumn('custentity_projectmanager','job');
				columns[4] = new nlobjSearchColumn('custentity_deliverymanager','job');
				columns[5] = new nlobjSearchColumn('custentity_verticalhead','job');
				columns[6] = new nlobjSearchColumn('custevent_practice');
				columns[7] = new nlobjSearchColumn('customer','job');
				columns[8] = new nlobjSearchColumn('company');
				columns[9] = new nlobjSearchColumn('custentity_project_allocation_category','job');
				columns[10] = new nlobjSearchColumn('subsidiary','employee');
				//columns[11] = new nlobjSearchColumn('custentity_clientpartner','customer');
				columns[11] = new nlobjSearchColumn('custentity_account_delivery_owner','customer');
				columns[12] = new nlobjSearchColumn('custentity_region','customer');
				columns[13] = new nlobjSearchColumn('custentity_region','job');
				columns[14] = new nlobjSearchColumn('percentoftime');
				columns[15] = new nlobjSearchColumn('custrecord_parent_practice', 'custevent_practice');
				columns[16] = new nlobjSearchColumn('custentity_practice', 'job');
				columns[17] = new nlobjSearchColumn('department', 'employee');
				columns[18] = new nlobjSearchColumn('resource');
				columns[19] = new nlobjSearchColumn('startdate');
				columns[20] = new nlobjSearchColumn('enddate');
				columns[21] = new nlobjSearchColumn('employeestatus','employee');
				columns[22] = new nlobjSearchColumn('title','employee');
				columns[23] = new nlobjSearchColumn('custeventbstartdate');
				columns[24] = new nlobjSearchColumn('custeventbenddate');
				columns[25] = new nlobjSearchColumn('custentity_reportingmanager','employee');
				columns[26] = new nlobjSearchColumn('employeetype','employee');
				columns[27] = new nlobjSearchColumn('hiredate','employee');
				columns[28] = new nlobjSearchColumn('approver','employee');
				columns[29] = new nlobjSearchColumn('timeapprover','employee');
				columns[30] = new nlobjSearchColumn('custevent3');
				columns[31] = new nlobjSearchColumn('custevent_otrate');
				columns[32] = new nlobjSearchColumn('custevent_monthly_rate');
				
				var project_allocation_result = searchRecord('resourceallocation', null, filters_search_allocation, columns);
				if (_logValidation(project_allocation_result))
				{
					for (var i_search_indx = 0; i_search_indx < project_allocation_result.length; i_search_indx++)
					{
						var is_resource_billable = project_allocation_result[i_search_indx].getValue('custeventrbillable');
						var billing_type = project_allocation_result[i_search_indx].getText('jobbillingtype','job');
						var client_partner = project_allocation_result[i_search_indx].getText('custentity_account_delivery_owner','customer');//('custentity_clientpartner','job');
						var proj_manager = project_allocation_result[i_search_indx].getText('custentity_projectmanager','job');
						var delivery_manager = project_allocation_result[i_search_indx].getText('custentity_deliverymanager','job');
						var vertical_head = project_allocation_result[i_search_indx].getText('custentity_verticalhead','job');
						var parctice_id = project_allocation_result[i_search_indx].getValue('custevent_practice');
						var project_cust = project_allocation_result[i_search_indx].getValue('customer','job');
						var proj_cust_name = project_allocation_result[i_search_indx].getText('customer','job');
						var project_id = project_allocation_result[i_search_indx].getValue('company');
						var project_name = project_allocation_result[i_search_indx].getText('company');
						var is_proj_bench_category = project_allocation_result[i_search_indx].getValue('custentity_project_allocation_category','job');
						var emp_susidiary = project_allocation_result[i_search_indx].getValue('subsidiary','employee');
						var customer_CP_id = project_allocation_result[i_search_indx].getValue('custentity_account_delivery_owner','customer');//('custentity_clientpartner','customer');
						var customer_CP_name = project_allocation_result[i_search_indx].getText('custentity_account_delivery_owner','customer');//('custentity_clientpartner','customer');
						var customer_region = project_allocation_result[i_search_indx].getValue('custentity_region','customer');
						var project_region = project_allocation_result[i_search_indx].getValue('custentity_region','job');
						var prcnt_allocated = project_allocation_result[i_search_indx].getValue('percentoftime');
						prcnt_allocated = prcnt_allocated.toString();
						prcnt_allocated = prcnt_allocated.split('.');
						prcnt_allocated = prcnt_allocated[0].toString().split('%');
						var final_prcnt_allocation = parseFloat(prcnt_allocated) / parseFloat(100);
						final_prcnt_allocation = parseFloat(final_prcnt_allocation);
						
							if(is_resource_billable == 'T')
							{
								if(cust_unique_list.indexOf(project_cust)>=0)
								{
									if(parseInt(is_proj_bench_category) == 4)
									{
										var cust_already_index = cust_unique_list.indexOf(project_cust);
										var billable_count_onsite = proj_deatils_arr[cust_already_index].billable_count_onsite;
										var billable_count_offsite = proj_deatils_arr[cust_already_index].billable_count_offsite;
										var unbillabl_count_onsite = proj_deatils_arr[cust_already_index].unbillable_count_onsite;
										var unbillabl_count_offsite = proj_deatils_arr[cust_already_index].unbillable_count_offsite;
										
										var billable_allo_onsite = proj_deatils_arr[cust_already_index].billable_allocation_onsite;
										var billable_allo_offsite = proj_deatils_arr[cust_already_index].billable_allocation_offsite;
										var unbillabl_allo_onsite = proj_deatils_arr[cust_already_index].unbillabel_allo_onsite;
										var unbillabl_allo_offsite = proj_deatils_arr[cust_already_index].unbillable_allo_offsite;
										
										var bench_count_final = proj_deatils_arr[cust_already_index].bench_count;
										bench_count_final = parseInt(bench_count_final) + parseInt(1);
										
										proj_deatils_arr[cust_already_index] = {
															'project_id':project_id,
															'project_name':project_name,
															'cust_name':proj_cust_name,
															'cust_id':project_cust,
															'billable_count_onsite':billable_count_onsite,
															'billable_count_offsite':billable_count_offsite,
															'unbillable_count_onsite':unbillabl_count_onsite,
															'unbillable_count_offsite':unbillabl_count_offsite,
															'bench_count':bench_count_final,
															'billing_type':billing_type,
															'client_part':client_partner,
															'proj_manager':proj_manager,
															'delivery_mana':delivery_manager,
															'vertical_head':vertical_head,
															'customer_CP_name':customer_CP_name,
															'customer_CP_id':customer_CP_id,
															'billable_allocation_onsite':billable_allo_onsite,
															'billable_allocation_offsite':billable_allo_offsite,
															'unbillabel_allo_onsite':unbillabl_allo_onsite,
															'unbillable_allo_offsite':unbillabl_allo_offsite															
													};
									}
									else
									{
									//	if (parseInt(emp_susidiary) == 3)
									if (obj_offsite_onsite[emp_susidiary])
										{
											var cust_already_index = cust_unique_list.indexOf(project_cust);
											var billable_count_onsite = proj_deatils_arr[cust_already_index].billable_count_onsite;
											var billable_count_offsite = proj_deatils_arr[cust_already_index].billable_count_offsite;
											billable_count_offsite = parseInt(billable_count_offsite) + parseInt(final_prcnt_allocation);
											var unbillabl_count_onsite = proj_deatils_arr[cust_already_index].unbillable_count_onsite;
											var unbillabl_count_offsite = proj_deatils_arr[cust_already_index].unbillable_count_offsite;
											
											var billable_allo_onsite = proj_deatils_arr[cust_already_index].billable_allocation_onsite;
											var billable_allo_offsite = proj_deatils_arr[cust_already_index].billable_allocation_offsite;
											billable_allo_offsite = parseFloat(billable_allo_offsite) + parseFloat(final_prcnt_allocation);
											var unbillabl_allo_onsite = proj_deatils_arr[cust_already_index].unbillabel_allo_onsite;
											var unbillabl_allo_offsite = proj_deatils_arr[cust_already_index].unbillable_allo_offsite;
											
											var bench_count_final = proj_deatils_arr[cust_already_index].bench_count;
											
											proj_deatils_arr[cust_already_index] = {
																'project_id':project_id,
																'project_name':project_name,
																'cust_name':proj_cust_name,
																'cust_id':project_cust,
																'billable_count_onsite':billable_count_onsite,
																'billable_count_offsite':billable_count_offsite,
																'unbillable_count_onsite':unbillabl_count_onsite,
																'unbillable_count_offsite':unbillabl_count_offsite,
																'bench_count':bench_count_final,
																'billing_type':billing_type,
																'client_part':client_partner,
																'proj_manager':proj_manager,
																'delivery_mana':delivery_manager,
																'vertical_head':vertical_head,
																'customer_CP_name':customer_CP_name,
																'customer_CP_id':customer_CP_id,
																'billable_allocation_onsite':billable_allo_onsite,
																'billable_allocation_offsite':billable_allo_offsite,
																'unbillabel_allo_onsite':unbillabl_allo_onsite,
																'unbillable_allo_offsite':unbillabl_allo_offsite
														};
										}
										else
										{
											var cust_already_index = cust_unique_list.indexOf(project_cust);
											var billable_count_onsite = proj_deatils_arr[cust_already_index].billable_count_onsite;
											billable_count_onsite = parseInt(billable_count_onsite) + parseInt(1);
											var billable_count_offsite = proj_deatils_arr[cust_already_index].billable_count_offsite;
											var unbillabl_count_onsite = proj_deatils_arr[cust_already_index].unbillable_count_onsite;
											var unbillabl_count_offsite = proj_deatils_arr[cust_already_index].unbillable_count_offsite;
											
											var billable_allo_onsite = proj_deatils_arr[cust_already_index].billable_allocation_onsite;
											billable_allo_onsite = parseFloat(billable_allo_onsite) + parseFloat(final_prcnt_allocation);
											var billable_allo_offsite = proj_deatils_arr[cust_already_index].billable_allocation_offsite;
											var unbillabl_allo_onsite = proj_deatils_arr[cust_already_index].unbillabel_allo_onsite;
											var unbillabl_allo_offsite = proj_deatils_arr[cust_already_index].unbillable_allo_offsite;
											
											var bench_count_final = proj_deatils_arr[cust_already_index].bench_count;
											
											proj_deatils_arr[cust_already_index] = {
																'project_id':project_id,
																'project_name':project_name,
																'cust_name':proj_cust_name,
																'cust_id':project_cust,
																'billable_count_onsite':billable_count_onsite,
																'billable_count_offsite':billable_count_offsite,
																'unbillable_count_onsite':unbillabl_count_onsite,
																'unbillable_count_offsite':unbillabl_count_offsite,
																'bench_count':bench_count_final,
																'billing_type':billing_type,
																'client_part':client_partner,
																'proj_manager':proj_manager,
																'delivery_mana':delivery_manager,
																'vertical_head':vertical_head,
																'customer_CP_name':customer_CP_name,
																'customer_CP_id':customer_CP_id,
																'billable_allocation_onsite':billable_allo_onsite,
																'billable_allocation_offsite':billable_allo_offsite,
																'unbillabel_allo_onsite':unbillabl_allo_onsite,
																'unbillable_allo_offsite':unbillabl_allo_offsite
														};
										}
									
									}
								}
								else
								{
									if(parseInt(is_proj_bench_category) == 4)
									{
										cust_unique_list.push(project_cust);
										proj_unique_list.push(project_id);
										proj_deatils_arr[sr_no] = {
																'project_id':project_id,
																'project_name':project_name,
																'cust_name':proj_cust_name,
																'cust_id':project_cust,
																'billable_count_onsite':0,
																'billable_count_offsite':0,
																'unbillable_count_onsite':0,
																'unbillable_count_offsite':0,
																'bench_count':1,
																'billing_type':billing_type,
																'client_part':client_partner,
																'proj_manager':proj_manager,
																'delivery_mana':delivery_manager,
																'vertical_head':vertical_head,
																'customer_CP_name':customer_CP_name,
																'customer_CP_id':customer_CP_id,
																'billable_allocation_onsite':0,
																'billable_allocation_offsite':0,
																'unbillabel_allo_onsite':0,
																'unbillable_allo_offsite':0
														};
										sr_no++;
									}
									else
									{
									//	if(parseInt(emp_susidiary) == 3)
										if (obj_offsite_onsite[emp_susidiary])
										{
											cust_unique_list.push(project_cust);
											proj_unique_list.push(project_id);
											proj_deatils_arr[sr_no] = {
																	'project_id':project_id,
																	'project_name':project_name,
																	'cust_name':proj_cust_name,
																	'cust_id':project_cust,
																	'billable_count_onsite':0,
																	'billable_count_offsite':1,
																	'unbillable_count_onsite':0,
																	'unbillable_count_offsite':0,
																	'bench_count':0,
																	'billing_type':billing_type,
																	'client_part':client_partner,
																	'proj_manager':proj_manager,
																	'delivery_mana':delivery_manager,
																	'vertical_head':vertical_head,
																	'customer_CP_name':customer_CP_name,
																	'customer_CP_id':customer_CP_id,
																	'billable_allocation_onsite':0,
																	'billable_allocation_offsite':final_prcnt_allocation,
																	'unbillabel_allo_onsite':0,
																	'unbillable_allo_offsite':0
															};
											sr_no++;
										}
										else
										{
											cust_unique_list.push(project_cust);
											proj_unique_list.push(project_id);
											proj_deatils_arr[sr_no] = {
																	'project_id':project_id,
																	'project_name':project_name,
																	'cust_name':proj_cust_name,
																	'cust_id':project_cust,
																	'billable_count_onsite':1,
																	'billable_count_offsite':0,
																	'unbillable_count_onsite':0,
																	'unbillable_count_offsite':0,
																	'bench_count':0,
																	'billing_type':billing_type,
																	'client_part':client_partner,
																	'proj_manager':proj_manager,
																	'delivery_mana':delivery_manager,
																	'vertical_head':vertical_head,
																	'customer_CP_name':customer_CP_name,
																	'customer_CP_id':customer_CP_id,
																	'billable_allocation_onsite':final_prcnt_allocation,
																	'billable_allocation_offsite':0,
																	'unbillabel_allo_onsite':0,
																	'unbillable_allo_offsite':0
															};
											sr_no++;
										}
									}
								}
							}
							else
							{
								if(cust_unique_list.indexOf(project_cust)>=0)
								{
									if(parseInt(is_proj_bench_category) == 4)
									{
										var cust_already_index = cust_unique_list.indexOf(project_cust);
										var billable_count_onsite = proj_deatils_arr[cust_already_index].billable_count_onsite;
										var billable_count_offsite = proj_deatils_arr[cust_already_index].billable_count_offsite;
										var unbillabl_count_onsite = proj_deatils_arr[cust_already_index].unbillable_count_onsite;
										var unbillabl_count_offsite = proj_deatils_arr[cust_already_index].unbillable_count_offsite;
										
										var billable_allo_onsite = proj_deatils_arr[cust_already_index].billable_allocation_onsite;
										var billable_allo_offsite = proj_deatils_arr[cust_already_index].billable_allocation_offsite;
										var unbillabl_allo_onsite = proj_deatils_arr[cust_already_index].unbillabel_allo_onsite;
										var unbillabl_allo_offsite = proj_deatils_arr[cust_already_index].unbillable_allo_offsite;
										
										var bench_count_final = proj_deatils_arr[cust_already_index].bench_count;
										bench_count_final = parseInt(bench_count_final) + parseInt(1);
										
										proj_deatils_arr[cust_already_index] = {
															'project_id':project_id,
															'project_name':project_name,
															'cust_name':proj_cust_name,
															'cust_id':project_cust,
															'billable_count_onsite':billable_count_onsite,
															'billable_count_offsite':billable_count_offsite,
															'unbillable_count_onsite':unbillabl_count_onsite,
															'unbillable_count_offsite':unbillabl_count_offsite,
															'bench_count':bench_count_final,
															'billing_type':billing_type,
															'client_part':client_partner,
															'proj_manager':proj_manager,
															'delivery_mana':delivery_manager,
															'vertical_head':vertical_head,
															'customer_CP_name':customer_CP_name,
															'customer_CP_id':customer_CP_id,
															'billable_allocation_onsite':billable_allo_onsite,
															'billable_allocation_offsite':billable_allo_offsite,
															'unbillabel_allo_onsite':unbillabl_allo_onsite,
															'unbillable_allo_offsite':unbillabl_allo_offsite
													};
									}
									else
									{
									//	if (parseInt(emp_susidiary) == 3)
									if (obj_offsite_onsite[emp_susidiary])
										{
											var cust_already_index = cust_unique_list.indexOf(project_cust);
											var unbillable_count_onsite = proj_deatils_arr[cust_already_index].unbillable_count_onsite;
											var unbillable_count_offsite = proj_deatils_arr[cust_already_index].unbillable_count_offsite;
											unbillable_count_offsite = parseInt(unbillable_count_offsite) + parseInt(1);
											var billabe_count_onsite = proj_deatils_arr[cust_already_index].billable_count_onsite;
											var billabe_count_offsite = proj_deatils_arr[cust_already_index].billable_count_offsite;
											
											var unbillabl_allo_onsite = proj_deatils_arr[cust_already_index].unbillabel_allo_onsite;
											var unbillabl_allo_offsite = proj_deatils_arr[cust_already_index].unbillable_allo_offsite;
											unbillabl_allo_offsite = parseFloat(unbillabl_allo_offsite) + parseFloat(final_prcnt_allocation);
											var billable_allo_onsite = proj_deatils_arr[cust_already_index].billable_allocation_onsite;
											var billable_allo_offsite = proj_deatils_arr[cust_already_index].billable_allocation_offsite;
											
											var bench_count_final = proj_deatils_arr[cust_already_index].bench_count;
											
											proj_deatils_arr[cust_already_index] = {
																'project_id':project_id,
																'project_name':project_name,
																'cust_name':proj_cust_name,
																'cust_id':project_cust,
																'billable_count_onsite':billabe_count_onsite,
																'billable_count_offsite':billabe_count_offsite,
																'unbillable_count_onsite':unbillable_count_onsite,
																'unbillable_count_offsite':unbillable_count_offsite,
																'bench_count':bench_count_final,
																'billing_type':billing_type,
																'client_part':client_partner,
																'proj_manager':proj_manager,
																'delivery_mana':delivery_manager,
																'vertical_head':vertical_head,
																'customer_CP_name':customer_CP_name,
																'customer_CP_id':customer_CP_id,
																'billable_allocation_onsite':billable_allo_onsite,
																'billable_allocation_offsite':billable_allo_offsite,
																'unbillabel_allo_onsite':unbillabl_allo_onsite,
																'unbillable_allo_offsite':unbillabl_allo_offsite
														};
										}
										else
										{
											var cust_already_index = cust_unique_list.indexOf(project_cust);
											var unbillable_count_onsite = proj_deatils_arr[cust_already_index].unbillable_count_onsite;
											unbillable_count_onsite = parseInt(unbillable_count_onsite) + parseInt(1);
											var unbillable_count_offsite = proj_deatils_arr[cust_already_index].unbillable_count_offsite;
											var billabe_count_onsite = proj_deatils_arr[cust_already_index].billable_count_onsite;
											var billabe_count_offsite = proj_deatils_arr[cust_already_index].billable_count_offsite;
											
											var unbillabl_allo_onsite = proj_deatils_arr[cust_already_index].unbillabel_allo_onsite;
											unbillabl_allo_onsite = parseFloat(unbillabl_allo_onsite) + parseFloat(final_prcnt_allocation);
											var unbillabl_allo_offsite = proj_deatils_arr[cust_already_index].unbillable_allo_offsite;
											var billable_allo_onsite = proj_deatils_arr[cust_already_index].billable_allocation_onsite;
											var billable_allo_offsite = proj_deatils_arr[cust_already_index].billable_allocation_offsite;
											
											var bench_count_final = proj_deatils_arr[cust_already_index].bench_count;
											
											proj_deatils_arr[cust_already_index] = {
																'project_id':project_id,
																'project_name':project_name,
																'cust_name':proj_cust_name,
																'cust_id':project_cust,
																'billable_count_onsite':billabe_count_onsite,
																'billable_count_offsite':billabe_count_offsite,
																'unbillable_count_onsite':unbillable_count_onsite,
																'unbillable_count_offsite':unbillable_count_offsite,
																'bench_count':bench_count_final,
																'billing_type':billing_type,
																'client_part':client_partner,
																'proj_manager':proj_manager,
																'delivery_mana':delivery_manager,
																'vertical_head':vertical_head,
																'customer_CP_name':customer_CP_name,
																'customer_CP_id':customer_CP_id,
																'billable_allocation_onsite':billable_allo_onsite,
																'billable_allocation_offsite':billable_allo_offsite,
																'unbillabel_allo_onsite':unbillabl_allo_onsite,
																'unbillable_allo_offsite':unbillabl_allo_offsite
														};
										}
									}
								}
								else
								{
									if(parseInt(is_proj_bench_category) == 4)
									{
										cust_unique_list.push(project_cust);
										proj_unique_list.push(project_id);
										proj_deatils_arr[sr_no] = {
																'project_id':project_id,
																'project_name':project_name,
																'cust_name':proj_cust_name,
																'cust_id':project_cust,
																'billable_count_onsite':0,
																'billable_count_offsite':0,
																'unbillable_count_onsite':0,
																'unbillable_count_offsite':0,
																'bench_count':1,
																'billing_type':billing_type,
																'client_part':client_partner,
																'proj_manager':proj_manager,
																'delivery_mana':delivery_manager,
																'vertical_head':vertical_head,
																'customer_CP_name':customer_CP_name,
																'customer_CP_id':customer_CP_id,
																'billable_allocation_onsite':0,
																'billable_allocation_offsite':0,
																'unbillabel_allo_onsite':0,
																'unbillable_allo_offsite':0
														};
										sr_no++;
									}
									else
									{
									//	if (parseInt(emp_susidiary) == 3)
										if (obj_offsite_onsite[emp_susidiary])
										{
											cust_unique_list.push(project_cust);
											proj_unique_list.push(project_id);
											proj_deatils_arr[sr_no] = {
																	'project_id':project_id,
																	'project_name':project_name,
																	'cust_name':proj_cust_name,
																	'cust_id':project_cust,
																	'billable_count_onsite':0,
																	'billable_count_offsite':0,
																	'unbillable_count_onsite':0,
																	'unbillable_count_offsite':1,
																	'bench_count':0,
																	'billing_type':billing_type,
																	'client_part':client_partner,
																	'proj_manager':proj_manager,
																	'delivery_mana':delivery_manager,
																	'vertical_head':vertical_head,
																	'customer_CP_name':customer_CP_name,
																	'customer_CP_id':customer_CP_id,
																	'billable_allocation_onsite':0,
																	'billable_allocation_offsite':0,
																	'unbillabel_allo_onsite':0,
																	'unbillable_allo_offsite':final_prcnt_allocation
															};
											sr_no++;
										}
										else
										{
											cust_unique_list.push(project_cust);
											proj_unique_list.push(project_id);
											proj_deatils_arr[sr_no] = {
																	'project_id':project_id,
																	'project_name':project_name,
																	'cust_name':proj_cust_name,
																	'cust_id':project_cust,
																	'billable_count_onsite':0,
																	'billable_count_offsite':0,
																	'unbillable_count_onsite':1,
																	'unbillable_count_offsite':0,
																	'bench_count':0,
																	'billing_type':billing_type,
																	'client_part':client_partner,
																	'proj_manager':proj_manager,
																	'delivery_mana':delivery_manager,
																	'vertical_head':vertical_head,
																	'customer_CP_name':customer_CP_name,
																	'customer_CP_id':customer_CP_id,
																	'billable_allocation_onsite':0,
																	'billable_allocation_offsite':0,
																	'unbillabel_allo_onsite':final_prcnt_allocation,
																	'unbillable_allo_offsite':0
															};
											sr_no++;
										}
									}
								}
							}
					}
					
					nlapiLogExecution('audit','proj detail len:-- ',proj_deatils_arr.length);
					send_mail(proj_deatils_arr,billing_from_date,context,obj_offsite_onsite);
				
				}
			}
		}

	}
	catch(err)
	{
		nlapiLogExecution('ERROR','ERROR MESSAGE:-- ',err);
		//Schedulescriptafterusageexceeded(region_id);
	}
}

function send_mail(proj_deatils_arr,billing_from_date,context,obj_offsite_onsite)
{
	
	if (_logValidation(proj_deatils_arr))
	{
		var today = new Date();
		var dd = today.getDate();
		var mm = today.getMonth()+1;
		var yyyy = today.getFullYear();
		var hours = today.getHours();
		var minutes = today.getMinutes();
		
		if(dd<10){
		    dd='0'+dd
		} 
		if(mm<10){
		    mm='0'+mm
		} 
		var today = dd+'/'+mm+'/'+yyyy;
		
		var cp_mail = new Array();
		var strVar = '';
		for(var proj_detail_index_or=0; proj_detail_index_or<proj_deatils_arr.length; proj_detail_index_or++)
		{
			var cp_id_found = proj_deatils_arr[proj_detail_index_or].customer_CP_id;
			var total_billable_head_cp =0;
			if(cp_mail.indexOf(cp_id_found)<0)
			{
				cp_mail.push(cp_id_found);
				if(_logValidation(cp_id_found)) 
				{
					var mail = nlapiLookupField('employee',parseInt(cp_id_found),'email');
                  nlapiLogExecution("Debug",mail,mail);
					var strVar1 = '';
					strVar1 += '<html>';
					strVar1 += '<body>';
						
					strVar1 += '<p>Daily Ticker dated '+today+' '+hours+':'+minutes+' (24 Hour Format)</p>';
						
					strVar1 += '<table>';
			
					strVar1 += '	<tr>';
					strVar1 += ' <td width="100%">';
					strVar1 += '<table width="100%" border="1">';
								
					strVar1 += '	<tr>';
					strVar1 += ' <td width="20%" font-size="11" align="center">Customer Name</td>';
					strVar1 += ' <td width="10%" font-size="11" align="center">Oniste Billed Count</td>';
					strVar1 += ' <td width="10%" font-size="11" align="center">Offiste Billed Count</td>';
					strVar1 += ' <td width="10%" font-size="11" align="center">Total Billed Count</td>';
					strVar1 += ' <td width="10%" font-size="11" align="center">Onsite Unbilled Count</td>';
					strVar1 += ' <td width="10%" font-size="11" align="center">Offsite Unbilled Count</td>';
					strVar1 += ' <td width="10%" font-size="11" align="center">Total Unbilled Count</td>';
					strVar1 += ' <td width="20%" font-size="11" align="center">Account Delivery Owner</td>';
					strVar1 += '	</tr>';
					for(var proj_detail_index=0; proj_detail_index<proj_deatils_arr.length; proj_detail_index++)
					{
						var cp_id = proj_deatils_arr[proj_detail_index].customer_CP_id;	
						if(cp_id == cp_id_found)
						{
							var total_unbillabe_head_custWise = parseFloat(proj_deatils_arr[proj_detail_index].unbillable_allo_offsite) + parseFloat(proj_deatils_arr[proj_detail_index].unbillabel_allo_onsite);
							var total_billabe_head_custWise = parseFloat(proj_deatils_arr[proj_detail_index].billable_allocation_onsite) + parseFloat(proj_deatils_arr[proj_detail_index].billable_allocation_offsite);
							//nlapiLogExecution('audit','cp_id',cp_id);
							if(_logValidation(cp_id))  // CP mail content 
							{
								strVar1 += '	<tr>';
								strVar1 += ' <td width="20%" font-size="11">' + proj_deatils_arr[proj_detail_index].cust_name + '</td>';
								strVar1 += ' <td width="10%" font-size="11" align="center">' + parseFloat(proj_deatils_arr[proj_detail_index].billable_allocation_onsite).toFixed(2) + '</td>';
								strVar1 += ' <td width="10%" font-size="11" align="center">' + parseFloat(proj_deatils_arr[proj_detail_index].billable_allocation_offsite).toFixed(2) + '</td>';
								strVar1 += ' <td width="10%" font-size="11" align="center">' + parseFloat(total_billabe_head_custWise).toFixed(2) + '</td>';
								strVar1 += ' <td width="10%" font-size="11" align="center">' + parseFloat(proj_deatils_arr[proj_detail_index].unbillabel_allo_onsite).toFixed(2) + '</td>';
								strVar1 += ' <td width="10%" font-size="11" align="center">' + parseFloat(proj_deatils_arr[proj_detail_index].unbillable_allo_offsite).toFixed(2) + '</td>';
								strVar1 += ' <td width="10%" font-size="11" align="center">' + parseFloat(total_unbillabe_head_custWise).toFixed(2) + '</td>';
								strVar1 += ' <td width="20%" font-size="11">' + proj_deatils_arr[proj_detail_index].customer_CP_name + '</td>';
								strVar1 += '	</tr>';
						
								
								total_billable_head_cp = parseFloat(total_billable_head_cp)+ parseFloat(proj_deatils_arr[proj_detail_index].billable_allocation_onsite) + parseFloat(proj_deatils_arr[proj_detail_index].billable_allocation_offsite);
							}
						}
					}
					strVar1 += '</table>';
					strVar1 += ' </td>';	
					strVar1 += '	</tr>';
						
					strVar1 += '</table>';	
					//strVar1 += '<p>NOTE: Management Count is the management team allocated to this Region.</p>';
					strVar1 += '<p>**For any discrepancy in data, please reach out to business.ops@brillio.com or chetan.barot@brillio.com </p>';
						
					strVar1 += '<p>Thanks & Regards,</p>';
					strVar1 += '<p>Team IS</p>';
						
					strVar1 += '</body>';
					strVar1 += '</html>';
					var filters_allocation_dump = new Array();
					filters_allocation_dump[0] = new nlobjSearchFilter('startdate', null, 'onorbefore', billing_from_date);
					filters_allocation_dump[1] = new nlobjSearchFilter('enddate', null, 'onorafter', billing_from_date);
					filters_allocation_dump[2] = new nlobjSearchFilter('custentity_account_delivery_owner','customer','anyof', cp_id_found);//('custentity_clientpartner', 'customer', 'anyof', cp_id_found);
					filters_allocation_dump[3] = new nlobjSearchFilter('custentity_employee_inactive', 'employee', 'is', 'F');
						
					var columns_dump = new Array();
					columns_dump[0] = new nlobjSearchColumn('custentity_region','customer');
					columns_dump[1] = new nlobjSearchColumn('customer');
					columns_dump[2] = new nlobjSearchColumn('company');
					columns_dump[3] = new nlobjSearchColumn('jobbillingtype', 'job');
					columns_dump[4] = new nlobjSearchColumn('custentity_project_allocation_category', 'job');
					columns_dump[5] = new nlobjSearchColumn('resource');
					columns_dump[6] = new nlobjSearchColumn('custevent_practice');
					columns_dump[7] = new nlobjSearchColumn('subsidiary', 'employee');
					columns_dump[8] = new nlobjSearchColumn('custentity_practice', 'job');
					columns_dump[9] = new nlobjSearchColumn('percentoftime');
					columns_dump[10] = new nlobjSearchColumn('custeventrbillable');
					columns_dump[11] = new nlobjSearchColumn('custentity_account_delivery_owner','customer');//('custentity_clientpartner','customer');
					columns_dump[12] = new nlobjSearchColumn('custentity_projectmanager', 'job');
					columns_dump[13] = new nlobjSearchColumn('custentity_deliverymanager', 'job');
					columns_dump[14] = new nlobjSearchColumn('startdate');
					columns_dump[15] = new nlobjSearchColumn('enddate');
					columns_dump[16] = new nlobjSearchColumn('employeestatus','employee');
					columns_dump[17] = new nlobjSearchColumn('title','employee');
					columns_dump[18] = new nlobjSearchColumn('custeventbstartdate');
					columns_dump[19] = new nlobjSearchColumn('custeventbenddate');
					columns_dump[20] = new nlobjSearchColumn('custentity_reportingmanager','employee');
					columns_dump[21] = new nlobjSearchColumn('employeetype','employee');
					columns_dump[22] = new nlobjSearchColumn('hiredate','employee');
					columns_dump[23] = new nlobjSearchColumn('approver','employee');
					columns_dump[24] = new nlobjSearchColumn('timeapprover','employee');
					columns_dump[25] = new nlobjSearchColumn('custevent3');
					columns_dump[26] = new nlobjSearchColumn('custevent_otrate');
					columns_dump[27] = new nlobjSearchColumn('custevent_monthly_rate');
					
					var emp_dump_srch = nlapiSearchRecord('resourceallocation', null, filters_allocation_dump, columns_dump);
					if (emp_dump_srch)
					{
						var strVar_excel = '';
						
						strVar_excel += '<table>';
		
						strVar_excel += '	<tr>';
						strVar_excel += ' <td width="100%">';
						strVar_excel += '<table width="100%" border="1">';
						
						strVar_excel += '	<tr>';
						strVar_excel += ' <td width="6%" font-size="11" align="center">Region</td>';
						strVar_excel += ' <td width="6%" font-size="11" align="center">Customer Name</td>';
						strVar_excel += ' <td width="6%" font-size="11" align="center">Project Name</td>';
						strVar_excel += ' <td width="6%" font-size="11" align="center">Executing Practice</td>';
						strVar_excel += ' <td width="6%" font-size="11" align="center">Billing Type</td>';
						strVar_excel += ' <td width="10%" font-size="11" align="left">Employee Name</td>';
						strVar_excel += ' <td width="6%" font-size="11" align="center">Employee Practice</td>';
						strVar_excel += ' <td width="6%" font-size="11" align="center">Employeee Subsidiary</td>';
						strVar_excel += ' <td width="6%" font-size="11" align="center">Percent Allocated</td>';
						strVar_excel += ' <td width="6%" font-size="11" align="center">Billable Status</td>';
						strVar_excel += ' <td width="6%" font-size="11" align="center">Allocation Start Date</td>';
						strVar_excel += ' <td width="6%" font-size="11" align="center">Allocation End Date</td>';
						strVar_excel += ' <td width="6%" font-size="11" align="center">Employee Level</td>';
						strVar_excel += ' <td width="6%" font-size="11" align="center">Employee Designation</td>';
						strVar_excel += ' <td width="6%" font-size="11" align="center">Onsite/Offshore</td>';
						strVar_excel += ' <td width="6%" font-size="11" align="center">Billing Start Date</td>';
						strVar_excel += ' <td width="6%" font-size="11" align="center">Billing End Date</td>';
						strVar_excel += ' <td width="6%" font-size="11" align="center">Reporting Manager</td>';
						strVar_excel += ' <td width="6%" font-size="11" align="center">Expense Approver</td>';
						strVar_excel += ' <td width="6%" font-size="11" align="center">Time Approver</td>';
						strVar_excel += ' <td width="6%" font-size="11" align="center">Employee Type</td>';
						strVar_excel += ' <td width="6%" font-size="11" align="center">Hire Date</td>';
						strVar_excel += ' <td width="6%" font-size="11" align="center">Bill Rate</td>';
						strVar_excel += ' <td width="6%" font-size="11" align="center">OT Rate</td>';
						strVar_excel += ' <td width="6%" font-size="11" align="center">Monthly Rate</td>';
						
						strVar_excel += ' <td width="6%" font-size="11" align="center">Account Delivery Owner</td>';
						strVar_excel += ' <td width="6%" font-size="11" align="center">Project Manager</td>';
						strVar_excel += ' <td width="6%" font-size="11" align="center">Delivery Manager</td>';			
						strVar_excel += ' <td width="6%" font-size="11" align="center">Project Category</td>';
						strVar_excel += '	</tr>';
							
						for (var index_no_dump = 0; index_no_dump < emp_dump_srch.length; index_no_dump++)
						{
							var region = emp_dump_srch[index_no_dump].getText('custentity_region','customer');
							var customer = emp_dump_srch[index_no_dump].getText('customer');
							var project = emp_dump_srch[index_no_dump].getText('company');
							var excuting_pract_proj = emp_dump_srch[index_no_dump].getText('custentity_practice', 'job');
							var proj_billing_type = emp_dump_srch[index_no_dump].getText('jobbillingtype', 'job');
							var employee = emp_dump_srch[index_no_dump].getText('resource');
							var emp_practice = emp_dump_srch[index_no_dump].getText('custevent_practice');
							var emp_subsidiary = emp_dump_srch[index_no_dump].getText('subsidiary', 'employee');
							var emp_subsidiary_id_dump = emp_dump_srch[index_no_dump].getValue('subsidiary', 'employee');
							var onsite_offsite_dump = '';
					
							if (emp_subsidiary_id_dump == 3 && emp_dump_srch[index_no_dump].getValue('custentity_legal_entity_fusion', 'employee') == 'Brillio Technologies Private Limited UK') {
								onsite_offsite_dump = 'Onsite';
							}
							else {
								onsite_offsite_dump = obj_offsite_onsite[emp_subsidiary_id_dump] ? 'Offshore' : 'Onsite';
							}
								
							var prcnt_of_tym = emp_dump_srch[index_no_dump].getValue('percentoftime');
							prcnt_of_tym = prcnt_of_tym.toString();
							prcnt_of_tym = prcnt_of_tym.split('.');
							prcnt_of_tym = prcnt_of_tym[0].toString().split('%');
							var final_prcnt_allocation = parseFloat(prcnt_of_tym) / parseFloat(100);
							final_prcnt_allocation = parseFloat(final_prcnt_allocation);
							var is_resource_billable = emp_dump_srch[index_no_dump].getValue('custeventrbillable');
							var allo_strt_date = emp_dump_srch[index_no_dump].getValue('startdate');
							var allo_end_date = emp_dump_srch[index_no_dump].getValue('enddate');
							var client_partner = emp_dump_srch[index_no_dump].getText('custentity_account_delivery_owner','customer');//('custentity_clientpartner','customer');
							var prj_manager = emp_dump_srch[index_no_dump].getText('custentity_projectmanager', 'job');
							var delivery_manager = emp_dump_srch[index_no_dump].getText('custentity_deliverymanager', 'job');
							var proj_category = emp_dump_srch[index_no_dump].getText('custentity_project_allocation_category', 'job');
							var emp_level_dump = emp_dump_srch[index_no_dump].getText('employeestatus','employee');
							var emp_designation_dump = emp_dump_srch[index_no_dump].getValue('title','employee');
							var billing_strt_date_dump = emp_dump_srch[index_no_dump].getValue('custeventbstartdate');
							var billing_end_date_dump = emp_dump_srch[index_no_dump].getValue('custeventbenddate');
							var reporting_manager_dump = emp_dump_srch[index_no_dump].getText('custentity_reportingmanager','employee');
							var emp_type_dump = emp_dump_srch[index_no_dump].getText('employeetype','employee');
							var emp_hire_date_dump = emp_dump_srch[index_no_dump].getValue('hiredate','employee');
							var expense_approver_dump = emp_dump_srch[index_no_dump].getText('approver','employee');
							var time_approver_dump = emp_dump_srch[index_no_dump].getText('timeapprover','employee');
							var bill_rate_dump = emp_dump_srch[index_no_dump].getValue('custevent3');
							if(!bill_rate_dump)
								bill_rate_dump = 0;
								
							var ot_rate_dump = emp_dump_srch[index_no_dump].getValue('custevent_otrate');
							if(!ot_rate_dump)
								ot_rate_dump = 0;
								
							var monthly_rate_dump = emp_dump_srch[index_no_dump].getValue('custevent_monthly_rate');
							if(!monthly_rate_dump)
								monthly_rate_dump = 0;
							
							if(is_resource_billable == 'T')
							{
								is_resource_billable = 'Yes';
							}
							else
								is_resource_billable = 'No';
								
							strVar_excel += '	<tr>';
							strVar_excel += ' <td width="6%" font-size="11" align="center">' +region+ '</td>';
							strVar_excel += ' <td width="6%" font-size="11" align="center">' +customer+ '</td>';
							strVar_excel += ' <td width="6%" font-size="11" align="center">' +project+ '</td>';
							strVar_excel += ' <td width="6%" font-size="11" align="center">' +excuting_pract_proj+ '</td>';
							strVar_excel += ' <td width="6%" font-size="11" align="center">' +proj_billing_type+ '</td>';
							strVar_excel += ' <td width="10%" font-size="11" align="left">' +employee+ '</td>';
							strVar_excel += ' <td width="6%" font-size="11" align="center">' +emp_practice+ '</td>';
							strVar_excel += ' <td width="6%" font-size="11" align="center">' +emp_subsidiary+ '</td>';
							strVar_excel += ' <td width="6%" font-size="11" align="center">' +final_prcnt_allocation+ '</td>';
							strVar_excel += ' <td width="6%" font-size="11" align="center">' +is_resource_billable+ '</td>';
							strVar_excel += ' <td width="6%" font-size="11" align="center">' +allo_strt_date+ '</td>';
							strVar_excel += ' <td width="6%" font-size="11" align="center">' +allo_end_date+ '</td>';
							strVar_excel += ' <td width="6%" font-size="11" align="center">' +emp_level_dump+ '</td>';
							strVar_excel += ' <td width="6%" font-size="11" align="center">' +emp_designation_dump+ '</td>';
							strVar_excel += ' <td width="6%" font-size="11" align="center">' +onsite_offsite_dump+ '</td>';
							strVar_excel += ' <td width="6%" font-size="11" align="center">' +billing_strt_date_dump+ '</td>';
							strVar_excel += ' <td width="6%" font-size="11" align="center">' +billing_end_date_dump+ '</td>';
							strVar_excel += ' <td width="6%" font-size="11" align="center">' +reporting_manager_dump+ '</td>';
							strVar_excel += ' <td width="6%" font-size="11" align="center">' +expense_approver_dump+ '</td>';
							strVar_excel += ' <td width="6%" font-size="11" align="center">' +time_approver_dump+ '</td>';
							strVar_excel += ' <td width="6%" font-size="11" align="center">' +emp_type_dump+ '</td>';
							strVar_excel += ' <td width="6%" font-size="11" align="center">' +emp_hire_date_dump+ '</td>';
							strVar_excel += ' <td width="6%" font-size="11" align="center">' +bill_rate_dump+ '</td>';
							strVar_excel += ' <td width="6%" font-size="11" align="center">' +ot_rate_dump+ '</td>';
							strVar_excel += ' <td width="6%" font-size="11" align="center">' +monthly_rate_dump+ '</td>';
							
							strVar_excel += ' <td width="6%" font-size="11" align="center">' +client_partner+ '</td>';
							strVar_excel += ' <td width="6%" font-size="11" align="center">' +prj_manager+ '</td>';
							strVar_excel += ' <td width="6%" font-size="11" align="center">' +delivery_manager+ '</td>';			
							strVar_excel += ' <td width="6%" font-size="11" align="center">' +proj_category+ '</td>';
							strVar_excel += '	</tr>';
						}
							
						strVar_excel += '</table>';
						strVar_excel += ' </td>';	
						strVar_excel += '	</tr>';
						
						strVar_excel += '</table>';
						
						var excel_file_obj = generate_excel(strVar_excel);
                      nlapiLogExecution("Debug",excel_file_obj,excel_file_obj);
					}
					
					var bcc = new Array();
					bcc[0] = 'information.systems@brillio.com';
					bcc[1] = 'madem.praveena@brillio.com'
				/*	bcc[0] = 'business.ops@brillio.com';
					bcc[1] = 'Chetan.barot@brillio.com';
					bcc[2] = 'information.systems@brillio.com'; */
                  var a_emp_attachment = new Array();
					a_emp_attachment['entity'] = '41571';
                /*  if(proj_deatils_arr[proj_detail_index].cust_name == "MOVE-01 Move inc."){
						//nlapiSendEmail(442,mail, 'Your Billed Count: '+parseFloat(total_billable_head_cp).toFixed(2)+'  (Customer: '+proj_deatils_arr[proj_detail_index].cust_name+') dt. '+today, strVar1,'pritesh.sonu@brillio.com',bcc,null,excel_file_obj);
                    nlapiSendEmail(442,mail, 'Your Billed Count: dt. '+today, strVar1,'pritesh.sonu@brillio.com',bcc,a_emp_attachment,excel_file_obj);
						}
					else{*/
                   nlapiLogExecution("Debug","beore");
					nlapiSendEmail(442,mail, 'Your Billed Count: '+parseFloat(total_billable_head_cp).toFixed(2)+' as on: dt. '+today+' across accounts', strVar1,null,bcc,a_emp_attachment,excel_file_obj);	
                  //nlapiSendEmail(442,'deepak.srinivas@brillio.com', 'Your Billed Count: '+parseFloat(total_billable_head_cp).toFixed(2)+' as on: dt. '+today+' across accounts', strVar1,null,bcc,a_emp_attachment,excel_file_obj);
					
					//nlapiSendEmail(442,'praveena@inspirria.com', 'Your Billed Count: '+parseFloat(total_billable_head_cp).toFixed(2)+' as on: dt. '+today+' across accounts', strVar1,null,bcc,a_emp_attachment,excel_file_obj);
					
                  nlapiLogExecution("Debug","after");
					//}
					
					//total_billable_head_cp = parseInt(total_billable_head_cp);
					//total_billable_head_cp = Math.round(total_billable_head_cp);
					//nlapiSendEmail(442, 'jayesh@inspirria.com', 'Your Build Count: '+parseFloat(total_billable_head_cp).toFixed(2)+'  (Customer: '+proj_deatils_arr[proj_detail_index].cust_name+') dt. '+today, strVar1,null,null,null,excel_file_obj);
					//nlapiSendEmail(442,mail, 'Your Billed Count: '+parseFloat(total_billable_head_cp).toFixed(2)+'  (Customer: '+proj_deatils_arr[proj_detail_index].cust_name+') dt. '+today, strVar1,null,bcc,null,excel_file_obj);
					
					//cp_mail.push(mail);
				}
				}
				yieldScript(context);
		}	
	}
}

function yieldScript(currentContext) {

	if (currentContext.getRemainingUsage() <= 1000) {
		nlapiLogExecution('AUDIT', 'API Limit Exceeded');
		var state = nlapiYieldScript();

		if (state.status == "FAILURE") {
			nlapiLogExecution('ERROR', 'Yield Script Failed', 'Reason : '
			        + state.reason + ' / Size : ' + state.size);
			return false;
		} else if (state.status == "RESUME") {
			nlapiLogExecution('AUDIT', 'Script Resumed');
		}
	}
}

function _logValidation(value) 
{
 if(value!='null' && value != null && value != null && value != '' && value != undefined && value != undefined && value != 'undefined' && value!= 'undefined'&& value!= 'NaN' && value != NaN) 
 {
  return true;
 }
 else 
 { 
  return false;
 }
}

function Schedulescriptafterusageexceeded(region_id)
{
    try
    {
        var params = new Array();
        var startDate = new Date();
        params['startdate'] = startDate.toUTCString();
        params['custscript_last_region_processed_id'] = region_id;
        var status = nlapiScheduleScript(nlapiGetContext().getScriptId(), nlapiGetContext().getDeploymentId(), params);
        //nlapiLogExecution('DEBUG', 'After Scheduling', 'Script scheduled status=' + status);
    } 
    catch (e) 
    {
        nlapiLogExecution('DEBUG', 'In Scheduled Catch', 'e : ' + e.message);
    }
}

function generate_excel(strVar_excel)
{
	var strVar1= '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:ss="urn:schemas-microsoft-com:office:spreadsheet" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40">'+
		'<head>'+
		'<meta http-equiv=Content-Type content="text/html; charset=windows-1252"/>'+
        '<meta name=ProgId content=Excel.Sheet/>'+
        '<meta name=Generator content="Microsoft Excel 11"/>'+
        '<!--[if gte mso 9]><xml>'+
        '<x:excelworkbook>'+
        '<x:excelworksheets>'+
        '<x:excelworksheet=sheet1>'+
        '<x:name>** ESTIMATE FILE**</x:name>'+
        '<x:worksheetoptions>'+
        '<x:selected></x:selected>'+
        '<x:freezepanes></x:freezepanes>'+
        '<x:frozennosplit></x:frozennosplit>'+
        '<x:splithorizontal>** FROZEN ROWS + 1 **</x:splithorizontal>'+
        '<x:toprowbottompane>** FROZEN ROWS + 1 **</x:toprowbottompane>'+
        '<x:splitvertical>** FROZEN COLUMNS + 1 **</x:splitvertical>'+
        '<x:leftcolumnrightpane>** FROZEN COLUMNS + 1**</x:leftcolumnrightpane>'+
		    '<x:activepane>0</x:activepane>'+// 0
		    '<x:panes>'+
			'<x:pane>'+
			'<x:number>3</x:number>'+
			'</x:pane>'+
			'<x:pane>'+
		    '<x:number>1</x:number>'+
		    '</x:pane>'+
		    '<x:pane>'+
			'<x:number>2</x:number>'+
        '</x:pane>'+
        '<x:pane>'+
        '<x:number>0</x:number>'+//1
        '</x:pane>'+
        '</x:panes>'+
        '<x:protectcontents>False</x:protectcontents>'+
        '<x:protectobjects>False</x:protectobjects>'+
        '<x:protectscenarios>False</x:protectscenarios>'+
        '</x:worksheetoptions>'+
        '</x:excelworksheet>'+
		'</x:excelworksheets>'+
        '<x:protectstructure>False</x:protectstructure>'+
        '<x:protectwindows>False</x:protectwindows>'+
		'</x:excelworkbook>'+
        
        // '<style>'+
			
			//-------------------------------------
        '</x:excelworkbook>'+
        '</xml><![endif]-->'+
         '<style>'+
		'p.MsoFooter, li.MsoFooter, div.MsoFooter'+
		'{ margin:0in; margin-bottom:.0001pt; mso-pagination:widow-orphan; tab-stops:center 3.0in right 6.0in; font-size:12.0pt;}'+
		'<style>'+

		'<!-- /* Style Definitions */'+

		//'@page Section1'+
		//'{ size:8.5in 11.0in; margin:1.9cm 1.27cm 1.9cm 1.27cm; mso-header-margin:.5in; mso-footer-margin:.5in; mso-title-page:yes; mso-header: h1; mso-footer: f1; mso-first-header: fh1; mso-first-footer: ff1; mso-paper-source:0;}'+

		'div.Section1'+
		'{ page:Section1;}'+

		'table#hrdftrtbl'+
		'{ margin:0in 0in 0in 900in; width:1px; height:1px; overflow:hidden; font-family:Arial;}-->'+

		'</style>'+
		
		
		'</head>'+
		

		'<body>'+
		
		'<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">'
		
		var strVar2 = strVar_excel;
		strVar1 = strVar1 + strVar2;
		var file = nlapiCreateFile('Resource Allocation Data.xls', 'XMLDOC', strVar1);
		return file;
}