/**
@return Void
**/

/**

Buddy Assignment Trigger

**/ 
function buddyTrigger(type)
{
 
   if (type='edit' )
   {  
		var isBuddy=nlapiGetFieldValue('custentity_isbuddy');
		var empName=nlapiGetFieldValue('firstname');
		var email=nlapiGetFieldValue('email');
		var subsidiary = nlapiGetFieldValue('subsidiary');
		oldRecord = nlapiGetOldRecord();
        var oldValue =  oldRecord.getFieldValue('custentity_isbuddy');
		if(isBuddy== 'T' && oldValue == 'F' )
		{
		nlapiLogExecution('AUDIT', 'Info', 'buddy#' +isBuddy);
		nlapiLogExecution('AUDIT', 'Info', 'empname#' +empName);
		body=bodyContent(empName);
          var a_emp_attachment = new Array();
          a_emp_attachment['entity'] = '62082';
		  if(subsidiary== 3)
		  
          nlapiSendEmail(10730, [email], 'Assigned As Buddy',body.MailBody, ['pratibha.mishra@brillio.com'],'sai.vannamareddy@brillio.com',a_emp_attachment);
		  if(subsidiary== 9)
		
		nlapiSendEmail(10730, [email], 'Assigned As Buddy',body.MailBody, ['rupali@comitydesigns.com'],'manikandan.v@brillio.com',a_emp_attachment);
			  	
		}
   }
  
}
function bodyContent(empName)
{
     
	var htmltext = '';
    
   htmltext += '<table border="0" width="100%"><tr>';
   htmltext += '<td colspan="4" valign="top">';
  
   htmltext += '<p>Hi  ' + empName + ',</p>';
   
   htmltext += '<p> Congratulations , you have been assigned as buddy.</p>';
   htmltext +='<p>Hope you are having a great time !</p>';
   htmltext +='<p>Request you to please complete the survey and give your valuable feedback to help us improve.</p>';
   htmltext += '<p><b>Login To NetSuite DashBoard-> Buddy Program Feedback.</b></p>';
   htmltext += '<p style="color:#0A582A;">Thanks & Regards,<br/>';
   htmltext += 'Team HR</p>';
 
    htmltext += '</td></tr>';
    htmltext += '</table>';
    htmltext += '<hr width="100%" size="1" noshade color="#CCCCCC">';
    htmltext += '<table width="100%" border="0" cellspacing="0" cellpadding="0">';
    htmltext += '<tr>';
    htmltext += '<td align="right">';
    htmltext += '<font style="font-size:9px; font-family:Verdana,Arial,Helvetica,sans-serif; color:#999999;">Brillio is powered by <a href="http://www.netsuite.com/" style="color:#999999;">NetSuite</a> One System. No Limits.</font>';
    htmltext += '</td>';
    htmltext += '</tr>';
    htmltext += '</table>';
    htmltext += '</body>';
    htmltext += '</html>';
 
    return {
        MailBody : htmltext
             
    };
}