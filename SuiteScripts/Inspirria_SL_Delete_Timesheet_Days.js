/**
 * @NApiVersion 2.x
 * @NScriptType Suitelet
 */
var CLIENT_SCRIPT_FILE_ID = 2920049;//-: SB //prod : 2920049;//'customscript_cs_weekly_timesheet_delete';

define(['N/ui/serverWidget','N/format','N/search','N/record','N/redirect','N/error'], function(serverWidget,format,search,record,redirect,error) {
	function onRequest(context) {
		if (context.request.method === 'GET') {
		
			var weekstartdate = context.request.parameters.weekstartdate;
			var Customer = context.request.parameters.Customer;
			var timesheet = context.request.parameters.timesheet;
			var disp = context.request.parameters.disp;
			var to_be_update_obj = context.request.parameters.obj;
			log.debug("Data IS",to_be_update_obj);
			if(to_be_update_obj)
			{
				updateTimesheetData(to_be_update_obj);
			}
			var form = serverWidget.createForm({
				title: 'Delete Timesheet'
			});
			form.clientScriptFileId = CLIENT_SCRIPT_FILE_ID;
			var Empfield = form.addField({
				id: 'custpage_text',
				type: serverWidget.FieldType.SELECT,
				label: 'Employee',
				source: 'employee'
			});
			Empfield.layoutType = serverWidget.FieldLayoutType.NORMAL;
			Empfield.updateBreakType({
				breakType: serverWidget.FieldBreakType.STARTCOL
			});

			var weekDate = form.addField({
				id: 'custpage_date',
				type: serverWidget.FieldType.DATE,
				label: 'Week Start Date'
			});
			if(weekstartdate)
			{
				weekDate.defaultValue =weekstartdate;
			}
			if(Customer)
			{
				Empfield.defaultValue =Customer;
			}
			/*var tm = form.addField({
				id: 'custpage_timesheet',
				type: serverWidget.FieldType.TEXT,
				label: 'Timesheet'
			});*/

			var sublist = form.addSublist({
				id: 'sublist',
				type: serverWidget.SublistType.LIST,//EDITOR,//.INLINEEDITOR,//,LIST
				label: 'Timesheet'
			});

			sublist.addField({
				id: 'mark',
				type: serverWidget.FieldType.CHECKBOX,
				label: 'Mark'

			});
			var date = sublist.addField({
				id: 'timesheetdate',
				type: serverWidget.FieldType.DATE,
				label: 'Date'
			});
		//	date.updateDisplayType({displayType: serverWidget.FieldDisplayType.ENTRY});
			var status = sublist.addField({
				id: 'status',
				type: serverWidget.FieldType.SELECT,
				label: 'Status'
			});
			status.updateDisplayType({displayType: serverWidget.FieldDisplayType.ENTRY});
			status.addSelectOption({
                value: '1',
                text: 'Open'
            });
			status.addSelectOption({
                value: '2',
                text: 'Pending Approval'
            });
			status.addSelectOption({
                value: '3',
                text: 'Approved'
            });
			status.addSelectOption({
                value: '4',
                text: 'Rejected'
            });
						
			var rate = sublist.addField({
				id: 'rate',
				type: serverWidget.FieldType.TEXT,
				label: 'Rate'
			});
			rate.updateDisplayType({displayType: serverWidget.FieldDisplayType.ENTRY});
			var Duration = sublist.addField({
				id: 'durationdecimal',
				type: serverWidget.FieldType.TEXT,
				label: 'Duration Decimal'
			});
			Duration.updateDisplayType({displayType: serverWidget.FieldDisplayType.ENTRY});
			sublist.addField({
				id: 'billed_unbilled',
				type: serverWidget.FieldType.CHECKBOX,
				label: 'Billed'

			});
			var rec_id = sublist.addField({
				id: 'internal_id',
				type: serverWidget.FieldType.TEXT,
				label: 'Rec. ID'
			});
			rec_id.updateDisplayType({displayType: serverWidget.FieldDisplayType.ENTRY},{displayType: serverWidget.FieldDisplayType.DISABLED});
			var timesheet_Id = sublist.addField({
				id: 'timesheet_id',
				type: serverWidget.FieldType.TEXT,
				label: 'Timesheet Id'
			});
			timesheet_Id.updateDisplayType({displayType: serverWidget.FieldDisplayType.ENTRY},{displayType: serverWidget.FieldDisplayType.DISABLED});
			var data ='';
			if(weekstartdate && Customer)
			{
				data = loadTimesheetData(weekstartdate,Customer);
				//	tm = defaultValue =data[0].timesheet;
				for (var a=0;a<data.length;a++)
				{
					sublist.setSublistValue({
						id: 'timesheetdate',
						line: a,
						value: data[a].date
					});

					sublist.setSublistValue({
						id: 'status',
						line: a,
						value: data[a].approvalstatus
					});
					var chval="F";
					if(data[a].isbillable == false || data[a].isbillable == "false")
					{
						chval="F";
					}
					else
					{
						chval="T";
					}
					sublist.setSublistValue({
						id: 'billed_unbilled',
						line: a,
						value: chval
					});
					sublist.setSublistValue({
						id: 'rate',
						line: a,
						value: data[a].rate
					});
					sublist.setSublistValue({
						id: 'durationdecimal',
						line: a,
						value: data[a].durationdecimal
					});

					sublist.setSublistValue({
						id: 'internal_id',
						line: a,
						value: data[a].internalid
					});
					sublist.setSublistValue({
						id: 'timesheet_id',
						line: a,
						value: data[a].timesheet
					});
				}

			}
			if(disp == "yes")
			{
				form.addSubmitButton({
					label: 'Delete'
				});
				form.addButton({
					id : 'updatedata',
					label: 'Update Timesheet',
					functionName:"updateData()"
				});
				form.addButton({
					id : 'resetpage',
					label: 'Reset',
					functionName:"reset()"
				});
				form.addButton({
					id : 'loaddata',
					label: 'Load Data',
					functionName:"LoadData()"
				});
			}
			else{
				form.addButton({
					id : 'loaddata',
					label: 'Load Data',
					functionName:"LoadData()"
				});
			}
			context.response.writePage(form);
		} else {
			var delimiter1 = /\u0001/;
			var delimiter = /\u0002/;
			var timesheet_data = [];
			var timeheet_idIs= [];
			/* var timesheetId = context.request.parameters.custpage_timesheet;
			 log.debug('timesheetId', timesheetId);*/
			var sublistData = context.request.parameters.sublistdata.split(delimiter);

			//log.debug('sublistData', JSON.stringify(sublistData));
			for (var i = 0; i < sublistData.length; i++) {
				var sublistDataDetails = sublistData[i].split(delimiter1);
				var mark = sublistDataDetails[0];
				if(mark == "T" || mark == "true")
				{
					var rec_ID = sublistDataDetails[6];	
					timesheet_data.push(Number(rec_ID));
					timeheet_idIs.push(sublistDataDetails[7]);
				}
			}
			if(sublistData.length ==  timesheet_data.length)
			{
				var errObj = error.create({
					name: 'Can not select All ',
					message: 'You can not select all timesheet record. Click on button and correct those data.',
					notifyOff: true
				});
				throw errObj.message;
			}
			else
			{
				
				for(var v=0;v<timesheet_data.length;v++)
				{
					
					
						record.delete({
						type: 'timebill',
						id: timesheet_data[v],
					});
				}
						
				redirect.toSuitelet({
					scriptId : 'customscript_weekly_timesheet_delete',
					deploymentId :'customdeploy_weekly_timesheet_delete'
				});
			}
		}
	}
	function updateTimesheetData(data)
	{
		//log.debug('data', JSON.stringify(data));
		var Data = JSON.parse(data);
		
		
		for(var s=0;s<Data.length;s++)
		{
		if(Data[s].billed_unbilled == false || Data[s].billed_unbilled == "false")
			{
				b_isbillable=false;
			}
			else
			{
				b_isbillable=true;
			}

			var id = record.submitFields({
			type: 'timebill',
			id: Data[s].timetrack_id,
			values: {
				'approvalstatus': Data[s].approvalstatus,
				'hours':Data[s].durationdecimal,
				isbillable:b_isbillable,
				rate:Data[s].rate
			}
			
			});
		}
		redirect.toSuitelet({
			scriptId : 'customscript_weekly_timesheet_delete',
			deploymentId :'customdeploy_weekly_timesheet_delete'
		});
	}
	function loadTimesheetData(weekstartdate,Customer)
	{
		var dateis = calculateWeekStartDate(weekstartdate);

		var timeentrySearchObj = search.create({
			type: "timebill",
			filters:
				[
					["type","anyof","A"], 
					"AND", 
					["employee","anyof",Customer], 
					"AND", 
					["date","within",dateis[0].firstday,dateis[0].lastday]
					],
					columns:
						[
							search.createColumn({name: "internalid", label: "Internal ID"}),
							search.createColumn({
								name: "startdate",
								join: "timeSheet",
								label: "Start Date"
							}),
							search.createColumn({
								name: "date",
								sort: search.Sort.ASC,
								label: "Time Entry Date"
							}),
							search.createColumn({name: "employee", label: "Employee"}),
							search.createColumn({name: "casetaskevent", label: "Task"}),
							search.createColumn({name: "durationdecimal", label: "Duration:Decimal"}),
							search.createColumn({name: "isbillable", label: "Billable"}),
							search.createColumn({name: "approvalstatus", label: "Approval Status"}),
							search.createColumn({
								name: "internalid",
								join: "timeSheet",
								label: "Internal ID"
							}),
							search.createColumn({name: "rate", label: "Rate"})
							]
		});
		var searchResultCount =  timeentrySearchObj.run().getRange(0,999);

		var dataArr = [];
		for (var rr = 0 ; rr<searchResultCount.length ; rr++)
		{
			var internalid = searchResultCount[rr].getValue({name: "internalid"});
			var date = searchResultCount[rr].getValue({name: "date",sort: search.Sort.ASC});
			var isbillable = searchResultCount[rr].getValue({name: "isbillable"});
			var approvalstatus = searchResultCount[rr].getValue({name: "approvalstatus"});
			var timesheet = searchResultCount[rr].getValue({name: "internalid",join: "timeSheet"});
			var rate = searchResultCount[rr].getValue({name: "rate", label: "Rate"});
			var durationdecimal = searchResultCount[rr].getValue({name: "durationdecimal", label: "Duration:Decimal"})
			dataArr.push({
				"internalid":internalid,"date":date,
				"isbillable":isbillable,"approvalstatus":approvalstatus,
				"timesheet":timesheet,"rate":rate,"durationdecimal":durationdecimal
			});
		}

		return dataArr;
	}
	function calculateWeekStartDate(date)
	{
		var curr = new Date(date); // get current date
		var first = curr.getDate() - curr.getDay(); // First day is the day of the month - the day of the week
		var last = first + 6; // last day is the first day + 6
		var firstday = DateFormat(new Date(curr.setDate(first))); // 06-Jul-2014
		var lastday = DateFormat(new Date(curr.setDate(last))); //12-Jul-2014
		return [{"firstday":firstday,"lastday":lastday}];
	}
	function DateFormat(initialFormattedDateString)
	{
		var parsedDateStringAsRawDateObject = format.parse({
			value: initialFormattedDateString,
			type: format.Type.DATE
		});
		return format.format({
			value: parsedDateStringAsRawDateObject,
			type: format.Type.DATE
		});
	}
	return {
		onRequest: onRequest
	};
});