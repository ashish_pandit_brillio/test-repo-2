function suitelet(request, response) {
	try {
			nlapiLogExecution('debug', 'request.getMethod() ==', request.getMethod);
		if (request.getMethod() == "GET") {
			createProcessStartScreen(request);
		} else {
			startProvisionProcess(request);
		}
	} catch (err) {
		nlapiLogExecution('ERROR', 'suitelet', err);
		displayErrorForm(err, "Monthly Provision");
	}
}

function createProcessStartScreen(request) {
	try {
		var provisionMasterId = request.getParameter('master');

		if (!provisionMasterId) {
			provisionMasterId = getRunningProvisionMasterId();
		}

		if (provisionMasterId) {
			var rec = nlapiLoadRecord('customrecord_provision_master',
			        provisionMasterId);

			var form = nlapiCreateForm("Monthly Provision");
			form.setScript('customscript_cs_mp_process_status');

			form.addField('custpage_master', 'select', 'Master',
			        'customrecord_provision_master').setDisplayType('hidden')
			        .setDefaultValue(provisionMasterId);

			// General
			form.addFieldGroup('custpage_general', 'General');
			form.addField('custpage_month', 'text', 'Month', null,
			        'custpage_general').setDisplayType('inline')
			        .setDefaultValue(rec.getFieldText('custrecord_pm_month'));
			form.addField('custpage_year', 'text', 'Year', null,
			        'custpage_general').setDisplayType('inline')
			        .setDefaultValue(rec.getFieldText('custrecord_pm_year'));
			form.addField('custpage_subsidiary', 'text', 'Subsidiary', null,
			        'custpage_general').setDisplayType('inline')
			        .setDefaultValue(
			                rec.getFieldText('custrecord_pm_subsidiary'));
			form
			        .addField('custpage_currency', 'text', 'Currency', null,
			                'custpage_general')
			        .setDisplayType('inline')
			        .setDefaultValue(rec.getFieldText('custrecord_pm_currency'));
			form.addField('custpage_finance_head', 'text', 'Finance Head',
			        null, 'custpage_general').setDisplayType('inline')
			        .setDefaultValue(
			                rec.getFieldText('custrecord_mp_finance_head'));

			// Dates
			form.addFieldGroup('custpage_dates', 'Dates');
			form.addField('custpage_from_date', 'text', 'From Date', null,
			        'custpage_dates').setDisplayType('inline').setDefaultValue(
			        rec.getFieldValue('custrecord_pm_from_date'));
			form.addField('custpage_to_date', 'text', 'To Date', null,
			        'custpage_dates').setDisplayType('inline').setDefaultValue(
			        rec.getFieldValue('custrecord_pm_to_date'));
			form.addField('custpage_reversal_date', 'text', 'Reversal Date',
			        null, 'custpage_dates').setDisplayType('inline')
			        .setDefaultValue(
			                rec.getFieldValue('custrecord_pm_reversal_date'));
			form.addField('custpage_effective_date', 'text', 'Effective Date',
			        null, 'custpage_dates').setDisplayType('inline')
			        .setDefaultValue(
			                rec.getFieldValue('custrecord_pm_effective_date'));

			// Dates
			form.addFieldGroup('custpage_accounts', 'Account');
			form.addField('custpage_debit', 'text', 'Debit Account', null,
			        'custpage_accounts').setDisplayType('inline')
			        .setDefaultValue(
			                rec.getFieldText('custrecord_pm_acct_debit'));
			form.addField('custpage_credit', 'text', 'Credit Account', null,
			        'custpage_accounts').setDisplayType('inline')
			        .setDefaultValue(
			                rec.getFieldText('custrecord_pm_acct_credit'));

			// Status
			form.addFieldGroup('custpage_status', 'Status');
			var statusTable = getStatusTable(rec);
			var statusTableField = form.addField('custpage_validation',
			        'inlinehtml', '', null, 'custpage_status');
			statusTableField.setDisplayType('inline');
			statusTableField.setBreakType('startcol');
			statusTableField.setDefaultValue(statusTable);

			if (rec.getFieldValue('custrecord_pm_is_currently_active') == 'F'
			        && rec.getFieldValue('custrecord_pm_je_creation_done') == 'F'
			        && noProvisionRunning()) {
				form.addSubmitButton('Start Provision');
				form.addButton('custpage_go_back', 'Edit', 'editRecord()');
			}

			response.writePage(form);
		} else {
			throw "No Provision Is Active";
		}
	} catch (err) {
		nlapiLogExecution('ERROR', 'createProcessStartScreen', err);
		throw err;
	}
}

function getStatusTable(provisionMaster) {
	var html = "<table border=1>";
	html += generateColumn('custrecord_pm_validation_completed',
	        'customdeploy_sut_mp_validation_progress', 'Data Validation',
	        provisionMaster);

	var extraLink = nlapiResolveURL('SUITELET',
	        'customscript_sut_mp_display_entries',
	        'customdeploy_sut_mp_display_entries')
	        + "&master=" + provisionMaster.getId();
	var hyperLink = "<a target='_blank' href='" + extraLink
	        + "'>Show Entries</a>";

	html += generateColumn('custrecord_pm_provision_creation_done',
	        'customdeploy_sut_mp_provision_progress', 'Provision Creation',
	        provisionMaster, hyperLink);
	html += generateColumn('custrecord_pm_je_creation_done',
	        'customdeploy_sut_mp_je_creation_progress', 'JE Creation',
	        provisionMaster);
	html += "</table>";
	return html;
}

function generateColumn(fieldId, deploymentId, label, provisionMaster,
        hyperLink)
{
	var linkUrl = nlapiResolveURL('SUITELET',
	        'customscript_sut_mp_process_progress', deploymentId)
	        + "&master=" + provisionMaster.getId();
	var lastStepDone = provisionMaster.getFieldValue(fieldId) == 'T';

	var html = "<tr style='border:1px solid black;'>";
	html += "<td>" + label + "</td>";
	html += "<td>"
	        + getImage(provisionMaster.getFieldValue(fieldId), lastStepDone)
	        + "</td>";
	html += "<td><a target='_blank' href='" + linkUrl + "'>Open Page</a></td>";

	if (hyperLink) {
		html += "<td>" + hyperLink + "</td>";
	} else {
		html += "<td></td>";
	}
	html += "</tr>";
	nlapiLogExecution('debug', 'html', html);
	return html;
}

function getImage(value, lastStepDone) {
	var imgUrl = "";

	if (lastStepDone) {
		if (value == 'T') {
			imgUrl = "<img style='margin-top: 0px; height: 20px;' src='https://system.na1.netsuite.com/core/media/media.nl?id=166770&c=3883006&h=2476ab2d56865ea3716d&whence='/>";
		} else {
			imgUrl = "On going";
		}
	} else {
		imgUrl = "Not Started";
	}

	return imgUrl;
}

function startProvisionProcess(request) {
	try {
		var provisionMasterId = request.getParameter('custpage_master');

		if (provisionMasterId) {
			// set all provision master as not running
			var provisionMasterSearch = nlapiSearchRecord(
			        'customrecord_provision_master',
			        null,
			        [
			                new nlobjSearchFilter('isinactive', null, 'is', 'F'),
			                new nlobjSearchFilter(
			                        'custrecord_pm_is_currently_active', null,
			                        'is', 'T') ]);

			if (provisionMasterSearch) {

				provisionMasterSearch
				        .forEach(function(master) {
					        nlapiSubmitField('customrecord_provision_master',
					                master.getId(),
					                'custrecord_pm_is_currently_active', 'F');
				        });
			}
			nlapiLogExecution('debug', 'all provision closed');

			nlapiSubmitField('customrecord_provision_master',
			        provisionMasterId, 'custrecord_pm_is_currently_active', 'T');
			nlapiLogExecution('debug', 'new provision activated');

			// call the validate script
			nlapiScheduleScript('customscript_sch_mp_provision_validation',
			        'customdeploy_sch_mp_provision_validation');

			// redirect to the validation suitelet
			response.sendRedirect('SUITELET',
			        'customscript_sut_mp_process_progress',
			        'customdeploy_sut_mp_validation_progress');
		} else {
			throw "Provision Master Id Not Provided";
		}
	} catch (err) {
		nlapiLogExecution('ERROR', 'startProvisionProcess', err);
		throw err;
	}
}

// client script
function editRecord() {
	try {
		window.location = nlapiResolveURL('RECORD',
		        'customrecord_provision_master',
		        nlapiGetFieldValue('custpage_master'))
		        + "&e=T";
	} catch (er) {
		alert(er.message);
	}
}