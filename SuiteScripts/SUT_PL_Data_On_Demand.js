/**
 * P/L report for practice heads
 * 
 * Version Date Author Remarks
 * 
 * 1.00 31 Jul 2018 Deepak MS
 * 
 */

function suitelet(request, response) {
	try {
		var str = request.getBody();

		var context = nlapiGetContext();

		var i_user_id = nlapiGetUser();

		var a_project_list = new Array();

		var s_selected_project_name = '';

		var a_selected_project_list = request.getParameterValues('project');
		nlapiLogExecution('debug', 'selected Departments', JSON
		        .stringify(a_selected_project_list));
		
		var a_selected_customer_list = request.getParameterValues('customer');
		nlapiLogExecution('debug', 'selected Customers', JSON
		        .stringify(a_selected_customer_list));

		var showAll = false;

		var s_from = '';
		var s_to = '';

		if (a_selected_project_list == null
		        || a_selected_project_list.length == 0) {
			showAll = true;
		}

		if (request.getParameter('user') != null
		        && request.getParameter('user') != '' && i_user_id == '1582') {
			i_user_id = request.getParameter('user');
		}

		var search = nlapiLoadSearch('transaction', 2420);
		var filters = search.getFilters();
		
		//Search for exchange Rate
		var f_rev_curr = 0;
		var f_cost_curr = 0;
		var filters = [];
	
		//var filters = [];
		//filters.push(new nlobjSearchFilter('formulatext', null, 'is', s_mont).setFormula('{custrecord_pl_currency_exchange_month}')); //NOTE : Capital 'Y' in Year
		//filters.push(new nlobjSearchFilter('formulatext', null, 'is', s_year_).setFormula('{custrecord_pl_currency_exchange_year}'));
		var a_dataVal = {};
		var dataRows =[];
		var a_practice_List = [];
		var unique_practice = [];
		var column = new Array();
		column[0] = new nlobjSearchColumn('custrecord_pl_currency_exchange_revnue_r');
		column[1] = new nlobjSearchColumn('custrecord_pl_currency_exchange_cost');
		column[2] = new nlobjSearchColumn('custrecord_pl_currency_exchange_year');
		column[3] = new nlobjSearchColumn('custrecord_pl_currency_exchange_month');
		//column[4] = new nlobjSearchColumn('custrecord_pl_gbp_revenue');
		column[4]=new nlobjSearchColumn('custrecord_pl_crc_cost_rate');
		column[5]= new nlobjSearchColumn('custrecord_pl_nok_cost_rate');
		//column[5] = new nlobjSearchColumn('custrecord_pl_gbp_cost_rate');
		
		var currencySearch = nlapiSearchRecord('customrecord_pl_currency_exchange_rates',null,null,column);
		if(currencySearch){
			for(var i_indx=0;i_indx < currencySearch.length;i_indx++){
				
				a_dataVal = {
						s_month : currencySearch[i_indx].getText('custrecord_pl_currency_exchange_month'),
						i_year : currencySearch[i_indx].getText('custrecord_pl_currency_exchange_year'),
						rev_rate : currencySearch[i_indx].getValue('custrecord_pl_currency_exchange_revnue_r'),
						cost_rate : currencySearch[i_indx].getValue('custrecord_pl_currency_exchange_cost'),
						//gbp_rev_rate : currencySearch[i_indx].getValue('custrecord_pl_gbp_revenue'),
						crc_cost:currencySearch[i_indx].getValue('custrecord_pl_crc_cost_rate'),
						nok_cost_rate : currencySearch[i_indx].getValue('custrecord_pl_nok_cost_rate')
						//gbp_cost_rate : currencySearch[i_indx].getValue('custrecord_pl_gbp_cost_rate'),
						
						
				};
				if(_logValidation(a_dataVal))
				dataRows.push(a_dataVal);
				
				
			}
		}

		// get the project list the user has access to
		if (nlapiGetUser() == 32162 || nlapiGetUser() == 41571 ||  nlapiGetUser() ==  42305 || nlapiGetUser() == 62082 || nlapiGetUser() == 7905) {
			i_user_id = 3169;
		}
		
		//i_user_id = 1618;
		nlapiLogExecution('debug', 'user', i_user_id);
		var project_search_results = '';
		var fpna= request.getParameter('fpna');
		if(_logValidation(a_selected_project_list)){
		project_search_results = getSelectedProjects(a_selected_project_list);
		}
	else{
	 project_search_results = getTaggedPracticeList(i_user_id);
	}
		// create html for select using the project data
		
			
		var s_project_options = '';
		var s_customer_options = '';

		for (var i = 0; project_search_results != null
		        && i < project_search_results.length; i++) {
			var i_project_id = project_search_results[i].getValue('internalid');
			a_practice_List.push(i_project_id);
			var s_project_number = project_search_results[i]
			        .getValue('name');
			var s_project_name = project_search_results[i].getValue('name');

			var s_selected_project = request.getParameter('project');

				if(_logValidation(s_project_number)){
				a_project_list.push(s_project_number);
				}
				

			var s_selected = '';
			if ((a_selected_project_list != null && a_selected_project_list
			        .indexOf(s_project_number) != -1)
			        || showAll == true) {
				s_selected = ' selected="selected" ';

				if (s_selected_project_name != '') {
					s_selected_project_name += ', ';
				}

				s_selected_project_name += s_project_number ;
				        
			}

			s_project_options += '<option value="' + s_project_number + '" '
			        + s_selected + '>' + s_project_number + ' '
			         '</option>';
			
		}

		if (a_selected_project_list == null
		        || a_selected_project_list.length == 0) {
			if (a_project_list.length > 0) {
				a_selected_project_list = a_project_list;

				showAll = true;
			} else {
				a_selected_project_list = new Array();
			}
		}
		
		unique_practice = removearrayduplicate(a_practice_List);
		/*if (unique_practice != null
		        && unique_practice.length != 0) {
			var s_formula = '';

			for (var i = 0; i < unique_practice.length; i++) {
				if (i != 0) {
					s_formula += " OR";
				}

				s_formula += "{department} = '"
				        + unique_practice[i] + "' ";
			}

			var projectFilter = new nlobjSearchFilter('formulanumeric', null,
			        'equalto', 1);

			projectFilter.setFormula("CASE WHEN " + s_formula
			        + " THEN 1 ELSE 0 END");

			filters = filters.concat([ projectFilter ]);
		} else {
			var projectFilter = new nlobjSearchFilter('formulanumeric', null,
			        'equalto', 1);

			projectFilter.setFormula("0");

			filters = filters.concat([ projectFilter ]);
		}*/

		if (request.getParameter('from') != null
		        && request.getParameter('from') != '') {
			s_from = request.getParameter('from');

			// filters = filters.concat([new nlobjSearchFilter('trandate', null,
			// 'onorafter', s_from)]);
		}
		else
			s_from = '1/1/2018';	
		
		var s_exclude =  request.getParameter('efc') ;
		//var s_exclude ;
		//nlapiLogExecution('debug','Exclude Value Parameter',request.getParameter('efc'));
		//s_exclude =  request.getParameter('efc') ;
		nlapiLogExecution('debug','Exclude Value',s_exclude);
		if (request.getParameter('to') != null
		        && request.getParameter('to') != '') {
			s_to = request.getParameter('to');

			// filters = filters.concat([new nlobjSearchFilter('trandate', null,
			// 'onorbefore', s_to)]);
		}

		var columns = search.getColumns();

		columns[0].setSort(false);
		columns[3].setSort(true);
		columns[9].setSort(false);
		filters.push(new nlobjSearchFilter('type',null,'noneof','SalesOrd'));
		filters.push(new nlobjSearchFilter('transactionnumbernumber',null,'isnotempty'));
      filters.push(new nlobjSearchFilter('type',null,'noneof','PurchOrd'));
      filters.push(new nlobjSearchFilter('department',null,'anyof',unique_practice));
	//  filters.push(new nlobjSearchFilter('postingperiod',null,'abs','140'));
	  	
	  filters.push(new nlobjSearchFilter('status',null,'noneof',
			['ExpRept:A','ExpRept:B','ExpRept:C','ExpRept:D','ExpRept:E','ExpRept:H','Journal:A','VendBill:C',
			'VendBill:D','VendBill:E',,'CustInvc:E','CustInvc:D']));
	  
		var search_results = searchRecord('transaction', 2420, filters, [
		        columns[2], columns[1], columns[0], columns[3], columns[4],
		        columns[5], columns[8], columns[9] ,columns[10] ,columns[11],columns[12],columns[13],columns[14]
		        ,columns[15],columns[16],columns[17],columns[18]]);
		
		nlapiLogExecution('DEBUG','search_results Length',search_results.length);
		// Get the facility cost

		var s_facility_cost_filter = '';

		for (var i = 0; i < a_selected_project_list.length; i++) {
			s_facility_cost_filter += "'" + a_selected_project_list[i] + "'";

			if (i != a_selected_project_list.length - 1) {
				s_facility_cost_filter += ",";
			}
		}

		var facility_cost_filters = new Array();
		facility_cost_filters[0] = new nlobjSearchFilter('formulanumeric',
		        null, 'equalto', 1);
		if (s_facility_cost_filter != '') {
			facility_cost_filters[0]
			        .setFormula('CASE WHEN {custrecord_arpm_project.entityid} IN ('
			                + s_facility_cost_filter + ') THEN 1 ELSE 0 END');
		} else {
			facility_cost_filters[0].setFormula('0');
		}

		var facility_cost_columns = new Array();
		facility_cost_columns[0] = new nlobjSearchColumn(
		        'custrecord_arpm_period');
		facility_cost_columns[1] = new nlobjSearchColumn(
		        'custrecord_arpm_num_allocated_resources');
		facility_cost_columns[2] = new nlobjSearchColumn(
		        'custrecord_arpm_facility_cost_per_person');
		facility_cost_columns[3] = new nlobjSearchColumn(
		        'custrecord_arpm_location');

		var facility_cost_search_results = searchRecord(
		        'customrecord_allocated_resources_per_mon', null,
		        facility_cost_filters, facility_cost_columns);

		var o_json = new Object();

		var o_data = {
		    'Revenue' : [],
		    'Discount' : [],
		    'People Cost' : [],
		    'Facility Cost' : [],
			'Other Cost - Travel':[],
			'Other Cost - Immigration': [],
			'Other Cost - Professional Fees' : [],
			'Other Cost - Others' : []
			
		};

		var new_object = null;

		var s_period = '';

		var a_period_list = [];

		var a_category_list = [];
		a_category_list[0]='';
		var a_group = [];

		var a_income_group = [];
		var j_other_list = {};
		var other_list =[];
		
		var col = new Array();
		col[0] = new nlobjSearchColumn('custrecord_account_name');
		col[1] = new nlobjSearchColumn('custrecord_type_of_cost');
      var oth_fil=new Array();
      oth_fil[0]=new nlobjSearchFilter('isinactive',null,'is','F');
		var other_Search = nlapiSearchRecord('customrecord_pl_other_costs',null,oth_fil,col);
		if(other_Search){
			for(var i_ind=0;i_ind < other_Search.length;i_ind++){
				var acctname=other_Search[i_ind].getValue('custrecord_account_name');
				
				a_category_list.push(acctname);
				
				
				j_other_list = {
						o_acct : other_Search[i_ind].getValue('custrecord_account_name'),
						o_type : other_Search[i_ind].getText('custrecord_type_of_cost')					
				};
				
				other_list.push(j_other_list);
				
				
			}
		}
		var s_selected_ = '';
		var customer_list = [];
		a_category_list = filter_array(a_category_list);		
		for (var i = 0; search_results != null && i < search_results.length; i++) {
			var period = search_results[i].getText(columns[0]);
			
			nlapiLogExecution('DEBUG','LINE NUMB',i);
			
			//Code updated by Deepak, Dated - 21 Mar 17
			var s_month_year = period.split(' ');
			var s_mont = s_month_year[0];
			s_mont = getMonthCompleteName(s_mont);
			 var s_year_ = s_month_year[1];
	            var f_revRate = 65.65;
	            var f_costRate = 65.0;
	            var f_gbp_rev_rate;
	            var f_crc_cost_rate = 558.36;
	            var f_nok_cost_rate = 8.09;
		//	var rate=nlapiExchangeRate('GBP', 'INR', mnt_lat_date);
		//	nlapiLogExecution('audit','accnt name:- '+mnt_lat_date);
			//nlapiLogExecution('audit','accnt name:- '+rate);
			
			//Fetch matching cost and rev rate convertion rate
			for(var data_indx = 0; data_indx < dataRows.length ; data_indx ++){
				if(dataRows[data_indx].s_month == s_mont && dataRows[data_indx].i_year == s_year_){
					f_revRate = dataRows[data_indx].rev_rate;
					f_costRate = dataRows[data_indx].cost_rate;
					//f_gbp_rev_rate=dataRows[data_indx].gbp_rev_rate;
					f_crc_cost_rate=dataRows[data_indx].crc_cost;
					f_nok_cost_rate=dataRows[data_indx].nok_cost_rate;
				}
			}
		/*	if(f_gbp_rev_rate==0)
			{
			f_gbp_rev_rate=getExchangerates(s_mont,s_year_);
			}*/
			// Adding the Customer Filter
			
			s_selected = ' selected="selected" ';
			
			
			var transaction_type = search_results[i].getText(columns[8]);

			var transaction_date = search_results[i].getValue(columns[9]);
			
			var i_subsidiary = search_results[i].getValue(columns[10]);

			var amount = parseFloat(search_results[i].getValue(columns[1]));

			var category = search_results[i].getValue(columns[3]);
			
			var s_account_name = search_results[i].getValue(columns[11]);
			
			var currency=search_results[i].getValue(columns[12]);
			
			var exhangeRate=search_results[i].getValue(columns[13]);
			
			var s_customer = search_results[i].getValue(columns[17]);
			if(_logValidation(s_customer)){
				var temp = s_customer.split(' ');
				
				if(customer_list.indexOf(temp[0].trim()) == -1){
					customer_list.push(temp[0].trim());
					s_customer_options += '<option value="' + s_customer + '" '
			        + s_selected + '>' + s_customer + ' '
			         '</option>';
				}
				
				
			}
		//	nlapiLogExecution('audit','exhangeRate:- ',exhangeRate);
			
			nlapiLogExecution('audit','accnt name:- '+s_account_name);
			
			var flag = false;
			if((currency != parseInt(6)) && (parseInt(i_subsidiary) == parseInt(3)) && category == 'Revenue')
			{
				amount= parseFloat(amount)* parseFloat(exhangeRate);
				amount = parseFloat(amount) /parseFloat(f_revRate);
				flag = true;
				
			}
			var eur_to_inr = 77.25;
			
			//AMount Convertion Logic
			if((parseInt(i_subsidiary) != parseInt(2))&& (currency!= parseInt(1))){
			if (category != 'Revenue' && category != 'Other Income'
		        && category != 'Discount' && parseInt(i_subsidiary) != parseInt(2)) {
					if(currency== parseInt(9))
					{
						amount = parseFloat(amount) /parseFloat(f_crc_cost_rate);
					}
					else if(currency == parseInt(8)){
					amount = parseFloat(amount) /parseFloat(f_nok_cost_rate);
					}
					
					else{
			
				amount= parseFloat(amount)* parseFloat(exhangeRate);		
				amount = parseFloat(amount) /parseFloat(f_costRate);	
				}
				
			} else if(flag == false && parseInt(currency) != parseInt(1) && parseInt(i_subsidiary) != parseInt(2)){
				amount= parseFloat(amount)* parseFloat(exhangeRate);
				amount = parseFloat(amount) /parseFloat(f_revRate);
				
			//f_total_revenue += o_data[s_category][j].amount;
			}
		}	//END
			

			
			var isWithinDateRange = true;

			var d_transaction_date = nlapiStringToDate(transaction_date,
			        'datetimetz');

			if (s_from != '') {
				var d_from = nlapiStringToDate(s_from, 'datetimetz');

				if (d_transaction_date < d_from) {
					isWithinDateRange = false;
				}
			}

			if (s_to != '') {
				var d_to = nlapiStringToDate(s_to, 'datetimetz');

				if (d_transaction_date > d_to) {
					isWithinDateRange = false;
				}
			}

			if (isWithinDateRange == true) {
				var i_index = a_period_list.indexOf(period);

				if (i_index == -1) {
					if(_logValidation(period)){
					a_period_list.push(period);
					}
					else{
					//nlapiLogExecution('debug', 'error', '388');
					}
			
				}

				i_index = a_period_list.indexOf(period);
			}
			if(category != 'People Cost' && category != 'Discount' && category != 'Facility Cost' && category != 'Revenue')
			{
				
			var o_index=a_category_list.indexOf(s_account_name);
			
			if(o_index == 0)
				o_index = 1;
				
			if(o_index!=-1)
			{
			o_index=o_index-1;
			var acct=other_list[o_index].o_acct;
			var typeofact=other_list[o_index].o_type;
			
			
			 if ((_logValidation(period)) && (_logValidation(amount)) && (_logValidation(search_results[i].getValue(columns[4]))) &&
                     (_logValidation(search_results[i].getValue(columns[5]))) && (_logValidation(transaction_type)) &&
                     (_logValidation(transaction_date)) && (_logValidation(isWithinDateRange)) && (_logValidation(s_account_name))) {} else {
                     //  nlapiLogExecution('debug','error','408');
                 }
			if(typeofact == 'Other Direct Cost - Travel'){
			o_data['Other Cost - Travel'].push({
			    'period' : period,
			    'amount' : amount,
			    'num' : search_results[i].getValue(columns[4]),
			    'memo' : search_results[i].getValue(columns[5]),
			    'type' : transaction_type,
			    'dt' : transaction_date,
			    'include' : isWithinDateRange,
				's_account_name' : s_account_name
			});
			}
			else if(typeofact == 'Other Direct Cost - Immigration'){
			o_data['Other Cost - Immigration'].push({
			    'period' : period,
			    'amount' : amount,
			    'num' : search_results[i].getValue(columns[4]),
			    'memo' : search_results[i].getValue(columns[5]),
			    'type' : transaction_type,
			    'dt' : transaction_date,
			    'include' : isWithinDateRange,
				's_account_name' : s_account_name
			});
			}
			if(typeofact == 'Other Direct Cost - Professional Fees'){
			o_data['Other Cost - Professional Fees'].push({
			    'period' : period,
			    'amount' : amount,
			    'num' : search_results[i].getValue(columns[4]),
			    'memo' : search_results[i].getValue(columns[5]),
			    'type' : transaction_type,
			    'dt' : transaction_date,
			    'include' : isWithinDateRange,
				's_account_name' : s_account_name
			});
			}
			if(typeofact == 'Other Direct Cost - Others'){
			o_data['Other Cost - Others'].push({
			    'period' : period,
			    'amount' : amount,
			    'num' : search_results[i].getValue(columns[4]),
			    'memo' : search_results[i].getValue(columns[5]),
			    'type' : transaction_type,
			    'dt' : transaction_date,
			    'include' : isWithinDateRange,
				's_account_name' : s_account_name
			});
			}
			
			}
			
			
		}
		else
			{
			if (parseInt(s_account_name) == parseInt(580)) { // Bad Debts
                if (parseFloat(amount) < 0) {
                    amount = Math.abs(parseFloat(amount));
                } else {
                    amount = '-' + amount;
                }
            }
			//avoiding discounts from account						  
			   if(category == 'Revenue' && i_subsidiary == parseInt(2))
			   {
					if(s_account_name != parseInt(732))
					{
					o_data[category].push({
					'period' : period,
					'amount' : amount,
					'num' : search_results[i].getValue(columns[4]),
					'memo' : search_results[i].getValue(columns[5]),
					'type' : transaction_type,
					'dt' : transaction_date,
					'include' : isWithinDateRange,
					's_account_name' : s_account_name
					});
                      nlapiLogExecution('audit','subs:- '+i_subsidiary);
					
                      
					}
					
			   }
			   else if (category == 'Revenue') {
                   o_data[category].push({
                	  
       					'period' : period,
       					'amount' : amount,
       					'num' : search_results[i].getValue(columns[4]),
       					'memo' : search_results[i].getValue(columns[5]),
       					'type' : transaction_type,
       					'dt' : transaction_date,
       					'include' : isWithinDateRange,
       					's_account_name' : s_account_name
       					
                      });
              }
			   if (category == 'Discount') {

                   o_data[category].push({
                	    'period' : period,
      					'amount' : amount,
      					'num' : search_results[i].getValue(columns[4]),
      					'memo' : search_results[i].getValue(columns[5]),
      					'type' : transaction_type,
      					'dt' : transaction_date,
      					'include' : isWithinDateRange,
      					's_account_name' : s_account_name
                   });

               }
			   if (category == 'People Cost') {

                   o_data[category].push({
                	    'period' : period,
      					'amount' : amount,
      					'num' : search_results[i].getValue(columns[4]),
      					'memo' : search_results[i].getValue(columns[5]),
      					'type' : transaction_type,
      					'dt' : transaction_date,
      					'include' : isWithinDateRange,
      					's_account_name' : s_account_name
                   });

               }
			   if (category == 'Facility Cost' || parseInt(s_account_name) == parseInt(1058)) {
				   o_data['Facility Cost'].push({
                	    'period' : period,
      					'amount' : amount,
      					'num' : search_results[i].getValue(columns[4]),
      					'memo' : search_results[i].getValue(columns[5]),
      					'type' : transaction_type,
      					'dt' : transaction_date,
      					'include' : isWithinDateRange,
      					's_account_name' : s_account_name
                   });
               }
				//nlapiLogExecution('audit','cat:- '+category);
			}
		}

		// Add facility cost
		//o_data['Facility Cost'] = new Array();

		/*for (var i = 0; facility_cost_search_results != null
		        && i < facility_cost_search_results.length; i++) {
			var s_period = facility_cost_search_results[i]
			        .getValue(facility_cost_columns[0]);
			var f_allocated_resources = facility_cost_search_results[i]
			        .getValue(facility_cost_columns[1]);
			var f_cost_per_resource = facility_cost_search_results[i]
			        .getValue(facility_cost_columns[2]);
			var s_location = facility_cost_search_results[i]
			        .getText(facility_cost_columns[3]);

			if (f_cost_per_resource == '') {
				f_cost_per_resource = 0;
			}
			if(_logValidation(f_allocated_resources))
			{
			}
			else{
			f_allocated_resources=0;
			}
						   
			   
			//if(s_exclude == 'true'){
				o_data['Facility Cost'].push({
					'period' : s_period,
					'amount' : 0,
					'num' : '',
					'memo' : 'Facility cost for ' + f_allocated_resources
					+ ' resources at ' + s_location + '.',
					'type' : 'Facility Cost',
					'dt' : '',
					'include' : a_period_list.indexOf(s_period) != -1,
					's_account_name' : ''
				});
			//}
			else{
			o_data['Facility Cost'].push({
			    'period' : s_period,
			    'amount' : f_allocated_resources * f_cost_per_resource,
			    'num' : '',
			    'memo' : 'Facility cost for ' + f_allocated_resources
			            + ' resources at ' + s_location + '.',
			    'type' : 'Facility Cost',
			    'dt' : '',
			    'include' : a_period_list.indexOf(s_period) != -1,
				's_account_name' : ''
			});
			}
		}*/

		var o_total = new Array();
		var o_period_total = new Array();

		var f_total_revenue = 0.0;
		var f_total_expense = 0.0;
		var f_profit_percentage = 0.0;

		var isDateRangeSelected = false;

		if (s_from != '' || s_to != '') {
			isDateRangeSelected = true;
		}

		var a_chart = new Array();

		// a_chart.push(['x'].concat(a_period_list));

		for ( var s_category in o_data) {
			var a_temp = [];

			o_total[s_category] = 0.0;
			o_period_total[s_category] = 0.0;

			for (var i = 0; i < a_period_list.length; i++) {
				a_temp.push(0.0);
			}

			for (var j = 0; j < o_data[s_category].length; j++) {
				var i_index = a_period_list
				        .indexOf(o_data[s_category][j].period);

				a_temp[i_index] += o_data[s_category][j].amount;

				if (o_data[s_category][j].include == true) {
					o_period_total[s_category] += o_data[s_category][j].amount;
				}

				o_total[s_category] += o_data[s_category][j].amount;

				if (s_category != 'Revenue' && s_category != 'Other Income'
				        && s_category != 'Discount') {
					f_total_expense += o_data[s_category][j].amount;
				} else {
					f_total_revenue += o_data[s_category][j].amount;
				}
			}

			if (s_category != 'Revenue' && s_category != 'Other Income'
			        && s_category != 'Discount') {
					if(_logValidation(s_category)){
				a_group.push(s_category);
				}
				else{
				//nlapiLogExecution('debug', 'error', '580');
				}
				
			} else {
			if(_logValidation(s_category))
			{
				a_income_group.push(s_category);
				}
				else{
				//nlapiLogExecution('debug', 'error', '589');
				}
			}
	if(_logValidation(s_category))
			a_chart.push([ s_category ].concat(a_temp));
			else{
			
		//	nlapiLogExecution('debug', 'error', '596');
			}
		}

		var a_profit = [ 'Net Margin(%)', 0.0 ];

		var a_expense = [ '', 0.0 ];

		var a_revenue = [ '', 0.0 ];

		// Calculate Profit
		for (var i = 0; i < a_chart.length; i++) {

			if (i == 0) {
				for (j = 1; j < a_chart[i].length; j++) {
					a_profit.push(0.0);

					a_expense.push(0.0);

					a_revenue.push(0.0);
				}
			}

			for (var j = 1; j < a_chart[i].length; j++) {

				if (a_chart[i][0] == 'Revenue'
				        || a_chart[i][0] == 'Other Income'
				        || a_chart[i][0] == 'Discount') {
					a_profit[j] = a_profit[j] + a_chart[i][j];

					a_revenue[j] = a_revenue[j] + a_chart[i][j];

					a_profit[a_profit.length - 1] += a_chart[i][j];
					a_revenue[a_revenue.length - 1] += a_chart[i][j];
				} else {
					a_profit[j] = a_profit[j] - a_chart[i][j];

					a_expense[j] = a_expense[j] + a_chart[i][j];

					a_profit[a_profit.length - 1] -= a_chart[i][j];
					a_expense[a_expense.length - 1] += a_chart[i][j];
				}
			}
		}

		for (var i = 1; i < a_profit.length; i++) {
			if (a_revenue[i] != 0.0) {
				a_profit[i] = (a_profit[i] / a_revenue[i] * 100).toFixed(2)
				        + '%';
			} else {
				a_profit[i] = ' - ';
			}
		}

		if (f_total_revenue != 0) {
			f_profit_percentage = (((f_total_revenue - f_total_expense) / f_total_revenue) * 100)
			        .toFixed(2)
			        + '%';
		} else {
			f_profit_percentage = ' - ';
		}

		// a_chart.push(a_profit);

		// o_json.push(new_object);

		// Display the table
		var s_table = '<table>';
		// Header
		s_table += '<thead><tr><th></th>';
		for (var i = 0; i < a_period_list.length; i++) {
			s_table += '<th>' + a_period_list[i] + '</th>';
		}
		if (isDateRangeSelected == true) {
			s_table += '<th>Total</th>';
		}

		s_table += '<th>Overall</th>';

		s_table += '</thead></tr>';
		// Table
		for (var i = 0; i < a_chart.length; i++) {
			if (a_chart[i][0] == 'People Cost') {
				s_table += '<tr style="background-color:#CCC;"><td>Net Revenue</td>';
				for (var j = 1; j < a_revenue.length; j++) {
					s_table += '<td style="text-align:right;">'
					        + accounting.formatNumber(a_revenue[j]) + '</td>';
				}
				if (isDateRangeSelected == true) {
					s_table += '<td style="text-align:right;">'
					        + accounting.formatNumber(f_total_revenue)
					        + '</td>';
				}
				s_table += '</tr>';
			}

			s_table += '<tr>';
			for (var j = 0; j < a_chart[i].length; j++) {
				var strValue = '';
				if (j > 0) {
					if (a_chart[i][0] != 'People Cost') {
						strValue += '<a href="javascript:displayPopup(\''
						        + a_chart[i][0] + '\',\''
						        + a_period_list[j - 1] + '\');">';
					}
					else if(a_chart[i][0] == 'People Cost')
					{
						strValue += '<a href="javascript:displayPopup(\''
						        + a_chart[i][0] + '\',\''
						        + a_period_list[j - 1] + '\');">';
					}

					strValue += accounting.formatNumber(a_chart[i][j]);
					a_chart[i][j] = Math.round(a_chart[i][j]);

					if (a_chart[i][0] != 'People Cost') {
						strValue += '</a>';
					}
					else if(a_chart[i][0] == 'People Cost')
					{
						strValue += '</a>';
					}

					s_table += '<td style="text-align:right;">' + strValue
					        + '</td>';
				} else {
					
					strValue += a_chart[i][j];

					s_table += '<td>' + strValue + '</td>';
					
				}
			}
			s_table += '<td style="text-align:right;">'
			        + accounting.formatNumber(o_period_total[a_chart[i][0]])
			        + '</td>';
			if (isDateRangeSelected == true) {
				s_table += '<td style="text-align:right;">'
				        + accounting.formatNumber(o_total[a_chart[i][0]])
				        + '</td>';
			}
			s_table += '</tr>';

			if (i == a_chart.length - 1) {
				s_table += '<tr style="background-color:#CCC;"><td>Total Cost</td>';
				for (var j = 1; j < a_expense.length; j++) {
					s_table += '<td style="text-align:right;">'
					        + accounting.formatNumber(a_expense[j]) + '</td>';
				}
				if (isDateRangeSelected == true) {
					s_table += '<td style="text-align:right;">'
					        + accounting.formatNumber(f_total_expense)
					        + '</td>';
				}
				s_table += '</tr>';

				s_table += '<tr style="font-weight:bold;"><td>Net Margin</td>';
				for (var j = 1; j < a_revenue.length; j++) {
					s_table += '<td style="text-align:right;">'
					        + accounting.formatNumber(a_revenue[j]
					                - a_expense[j]) + '</td>';
				}
				if (isDateRangeSelected == true) {
					s_table += '<td style="text-align:right;">'
					        + accounting.formatNumber(f_total_revenue
					                - f_total_expense) + '</td>';
				}
				s_table += '</tr>';
			}
		}

		s_table += '<tfoot><tr style="font-weight:bold;">';

		for (var i = 0; i < a_profit.length; i++) {
			s_table += '<td>' + a_profit[i] + '</td>';
		}
		if (isDateRangeSelected == true) {
			s_table += '<td>' + f_profit_percentage + '</td>';
		}
		s_table += '</tr></tfoot>';

		s_table += '</table>';

		// Define colors
		var a_colors = [ '#7293cb', '#808585', '#ab6857', '#84ba5b', '#ccc210',
		        '#d35e60', '#9067a7', '#e1974c' ];

		var o_bar_colors = new Object();
		var indx = 0;
		for ( var s_category in o_data) {
			if (indx < a_colors.length) {
				o_bar_colors[s_category] = a_colors[indx];

				indx++;
			}
		}

		// added by Nitish
		// generate the HTML content for the lead indicator table
			/*	var margin_indicator_html = "";
		margin_indicator_html = generateSoldMarginTable(
		        a_selected_project_list, a_period_list, s_from, s_to);
		var lead_indicator_html = "";
		// if (nlapiGetUser() == 9673) {
		lead_indicator_html = generateLeadIndicatorTable(
		        a_selected_project_list, a_period_list, s_from, s_to);*/
		// }

				
		var objValues = new Object();
		objValues.a_chart = JSON.stringify(a_chart);
		objValues.a_group = JSON.stringify(a_group);
		objValues.a_income_group = JSON.stringify(a_income_group);
		objValues.a_period_list = JSON.stringify(a_period_list);
		objValues.s_project_options = s_project_options;
		objValues.s_customer_options = s_customer_options;
		objValues.s_table = s_table;
		//objValues.s_cost_indicator_table = lead_indicator_html;
		//objValues.s_margin_indicator_table = margin_indicator_html;
		objValues.s_table = s_table;
		objValues.s_from = s_from;
		objValues.s_to = s_to; 
		objValues.s_exclude = s_exclude;
		objValues.s_title = s_selected_project_name;
		objValues.json_data = JSON.stringify(o_data);
		objValues.url = nlapiResolveURL('SUITELET', context.getScriptId(),
		        context.getDeploymentId());
		objValues.i_user_id = i_user_id;
		objValues.i_script_id = request.getParameter('script');
		objValues.i_deployment_id = request.getParameter('deploy');
		objValues.colors = JSON.stringify(o_bar_colors);

		var s_html = displayHTML(864014, objValues);

		response.write(s_html);

		nlapiLogExecution('AUDIT', 'Run By', '');

		return;

		// Instantiate a report definition to work with
		var reportDefinition = nlapiCreateReportDefinition();

		reportDefinition.addRowHierarchy('custrecord_type', 'Type', 'TEXT');
		reportDefinition.addRowHierarchy('custrecord_category', 'Category',
		        'TEXT');
		reportDefinition.addRowHierarchy('custrecord_sub_category',
		        'Sub Category', 'TEXT');

		reportDefinition.addColumn('custrecord_transaction_number', false,
		        'Transaction Number', null, 'TEXT', null);
		reportDefinition.addColumn('custrecord_remarks', false, 'Remarks',
		        null, 'TEXT', null);

		var columnQuarter = reportDefinition.addColumnHierarchy(
		        'custrecord_quarter', 'Quarter', null, 'TEXT');
		var columnPeriod = reportDefinition.addColumnHierarchy(
		        'custrecord_period', 'Period', columnQuarter, 'TEXT');

		reportDefinition.addColumn('custrecord_amount', true, 'Amount',
		        columnPeriod, 'CURRENCY', null);

		reportDefinition.addSearchDataSource('transaction', null, filters,
		        columns, {
		            'custrecord_type' : columns[16],
		            'custrecord_quarter' : columns[15],
		            'custrecord_period' : columns[1],
		            'custrecord_category' : columns[17],
		            'custrecord_sub_category' : columns[11],
		            'custrecord_amount' : columns[6],
		            'custrecord_transaction_number' : columns[3],
		            'custrecord_remarks' : columns[5]
		        });

		// Create a form to build the report on
		var form = nlapiCreateReportForm('Revenue Trend Report');
		form.addPageLink('crosslink','BRTP','URL');
		// Build the form from the report definition
		var pvtTable = reportDefinition.executeReport(form);

		// Write the form to the browser
		response.writePage(form);
	} catch (err) {
		displayErrorForm(err);
	}
}

function displayHTML(i_file_id, objValues) {
	var file = nlapiLoadFile(i_file_id); // load the HTML file
	var contents = file.getValue(); // get the contents
	contents = replaceValues(contents, objValues);
	return contents;
}

// Used to display the html, by replacing the placeholders
function replaceValues(content, oValues) {
	for (param in oValues) {
		// Replace null values with blank
		var s_value = (oValues[param] == null) ? '' : oValues[param];

		// Replace content
		content = content.replace(new RegExp('{{' + param + '}}', 'gi'),
		        s_value);
	}

	return content;
}

function createPeriodList(startDate, endDate) {
	var d_startDate = nlapiStringToDate(startDate);
	var d_endDate = nlapiStringToDate(endDate);

	var arrPeriod = [];

	for (var i = 0;; i++) {
		var currentDate = nlapiAddMonths(d_startDate, i);
		if((_logValidation(getMonthName(currentDate)))&& (_logValidation(getMonthStartDate(currentDate)))&&(_logValidation(getMonthEndDate(currentDate))))
		{
		}
		else{
		nlapiLogExecution('debug', 'error', '909');
		}
		arrPeriod.push({
		    Name : getMonthName(currentDate),
		    StartDate : getMonthStartDate(currentDate),
		    EndDate : getMonthEndDate(currentDate)
		});

		if (getMonthEndDate(currentDate) >= d_endDate) {
			break;
		}
	}

	var today = new Date();

	// remove the current month
	if (d_endDate.getMonth() >= today.getMonth()) {
		arrPeriod.pop();
	}

	return arrPeriod;
}

function getMonthEndDate(currentDate) {
	return nlapiAddDays(nlapiAddMonths(currentDate, 1), -1);
}

function getMonthStartDate(currentDate) {
	var month = currentDate.getMonth() + 1;
	var year = currentDate.getFullYear();
	var s_startDate = month + "/1/" + year;
	var d_startDate = nlapiStringToDate(s_startDate);
	return d_startDate;
}

function getMonthName(currentDate) {
	var monthNames = [ "JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JUL", "AUG",
	        "SEP", "OCT", "NOV", "DEC" ];
	return monthNames[currentDate.getMonth()] + " " + currentDate.getFullYear();
}

function generateLeadIndicatorTable(a_selected_project_list, a_period_list,
        startDate, endDate)
{
	try {
		if (!startDate) {
			startDate = '1/1/2015';
		}

		if (!endDate) {
			endDate = nlapiDateToString(new Date(), 'date');
		}

		nlapiLogExecution('debug', 'a_period_list', JSON
		        .stringify(a_period_list));

		var periodList = createPeriodList(startDate, endDate);

		// get all the project related utilization data
		var filters = [
		        new nlobjSearchFilter('isinactive', null, 'is', 'F'),
		        new nlobjSearchFilter('custrecord_urt_start_date', null,
		                'within', startDate, endDate) ];

		var s_formula = '';
		for (var i = 0; i < a_selected_project_list.length; i++) {
			if (i > 0) {
				s_formula += " OR";
			}
			s_formula += " SUBSTR({custrecord_urt_project}, 0,9) = '"
			        + a_selected_project_list[i] + "' ";
		}
		var projectFilter = new nlobjSearchFilter('formulanumeric', null,
		        'equalto', 1);
		projectFilter.setFormula("CASE WHEN " + s_formula
		        + " THEN 1 ELSE 0 END");
				if(_logValidation(projectFilter))
		filters.push(projectFilter);
		else{
		nlapiLogExecution('debug', 'error', '991');
		}

		var utilizationSearch = nlapiSearchRecord(
		        'customrecord_utilization_report_trend',
		        null,
		        filters,
		        [
		                new nlobjSearchColumn('custrecord_urt_start_date',
		                        null, 'group').setSort(),
		                new nlobjSearchColumn('custrecord_urt_onsite_billed',
		                        null, 'sum'),
		                new nlobjSearchColumn('custrecord_urt_onsite_unbilled',
		                        null, 'sum'),
		                new nlobjSearchColumn('custrecord_urt_offsite_billed',
		                        null, 'sum'),
		                new nlobjSearchColumn(
		                        'custrecord_urt_offsite_unbilled', null, 'sum'),
		                new nlobjSearchColumn(
		                        'custrecord_urt_utilization_percent', null,
		                        'avg'),

		                new nlobjSearchColumn('custrecord_urt_salaried_count',
		                        null, 'sum'),
		                new nlobjSearchColumn('custrecord_urt_contract_count',
		                        null, 'sum'),
		                new nlobjSearchColumn('custrecord_urt_contract_cost',
		                        null, 'sum'),
		                new nlobjSearchColumn('custrecord_urt_salaried_cost',
		                        null, 'sum'),
		                new nlobjSearchColumn(
		                        'custrecord_urt_contractor_percent', null,
		                        'avg'),
		                new nlobjSearchColumn(
		                        'custrecord_urt_contractor_cost_percent', null,
		                        'avg') ]);

		// generate HTML for table
		var table_css = "";
		table_css += "<style>";

		table_css += "#cost_indicator tr:nth-child(even) {background: #e6e6ff}";
		table_css += "#cost_indicator tr:nth-child(odd) {background: white}";
		table_css += "#cost_indicator th {background: linear-gradient(#49708f, #293f50) !important}";
		table_css += "</style>";

		var s_table = '<table id="cost_indicator">';

		// Header
		s_table += '<thead><tr><th></th>';
		periodList.forEach(function(period) {
			s_table += '<th>' + period.Name + '</th>';
		});
		s_table += '</tr></thead>';

		// Data Rows
		var offSiteBilledRow = "<td>Offsite Billed (pm)</td>";
		var offSiteUnbilledRow = "<td>Offsite Unbilled (pm)</td>";
		var onSiteBilledRow = "<td>Onsite Billed (pm)</td>";
		var onSiteUnbilledRow = "<td>Onsite Unbilled (pm)</td>";
		var utilizationRow = "<td>% Utilization</td>";

		var salariedCount = "<td># Salaried (pm)</td>";
		var salariedCost = "<td>Salaried Cost ($)</td>";
		var contractCount = "<td># Contractor (pm)</td>";
		var contractCost = "<td>Contractor Cost ($)</td>";

		var salariedCountRatio = "<td>Contract Count Ratio</td>";
		var salariedCostRatio = "<td>Contract Cost Ratio</td>";

		periodList
		        .forEach(function(period) {

			        var columnFound = false;
			        for (var i = 0; i < utilizationSearch.length; i++) {
				        var searchDate = utilizationSearch[i].getValue(
				                'custrecord_urt_start_date', null, 'group');
				        var d_searchDate = nlapiStringToDate(searchDate);

				        if (period.StartDate <= d_searchDate
				                && d_searchDate <= period.EndDate) {
					        columnFound = true;
					        offSiteBilledRow += '<td style="text-align:right;">'
					                + parseFloat(
					                        utilizationSearch[i]
					                                .getValue(
					                                        'custrecord_urt_offsite_billed',
					                                        null, 'sum'))
					                        .toFixed(2) + '</td>';
					        offSiteUnbilledRow += '<td style="text-align:right;">'
					                + parseFloat(
					                        utilizationSearch[i]
					                                .getValue(
					                                        'custrecord_urt_offsite_unbilled',
					                                        null, 'sum'))
					                        .toFixed(2) + '</td>';
					        onSiteBilledRow += '<td style="text-align:right;">'
					                + parseFloat(
					                        utilizationSearch[i]
					                                .getValue(
					                                        'custrecord_urt_onsite_billed',
					                                        null, 'sum'))
					                        .toFixed(2) + '</td>';

					        onSiteUnbilledRow += '<td style="text-align:right;">'
					                + parseFloat(
					                        utilizationSearch[i]
					                                .getValue(
					                                        'custrecord_urt_onsite_unbilled',
					                                        null, 'sum'))
					                        .toFixed(2) + '</td>';
					        utilizationRow += '<td style="text-align:right;">'
					                + parseFloat(
					                        utilizationSearch[i]
					                                .getValue(
					                                        'custrecord_urt_utilization_percent',
					                                        null, 'avg'))
					                        .toFixed(2) + '%</td>';

					        salariedCountRatio += '<td style="text-align:right;">'
					                + parseFloat(
					                        utilizationSearch[i]
					                                .getValue(
					                                        'custrecord_urt_contractor_percent',
					                                        null, 'avg'))
					                        .toFixed(2) + '</td>';

					        salariedCostRatio += '<td style="text-align:right;">'
					                + parseFloat(
					                        utilizationSearch[i]
					                                .getValue(
					                                        'custrecord_urt_contractor_cost_percent',
					                                        null, 'avg'))
					                        .toFixed(2) + '</td>';

					        salariedCount += '<td style="text-align:right;">'
					                + parseFloat(
					                        utilizationSearch[i]
					                                .getValue(
					                                        'custrecord_urt_salaried_count',
					                                        null, 'sum'))
					                        .toFixed(2) + '</td>';

					        contractCount += '<td style="text-align:right;">'
					                + parseFloat(
					                        utilizationSearch[i]
					                                .getValue(
					                                        'custrecord_urt_contract_count',
					                                        null, 'sum'))
					                        .toFixed(2) + '</td>';

					        salariedCost += '<td style="text-align:right;">'
					                + Math.round(utilizationSearch[i].getValue(
					                        'custrecord_urt_salaried_cost',
					                        null, 'sum')) + '</td>';

					        contractCost += '<td style="text-align:right;">'
					                + Math.round(utilizationSearch[i].getValue(
					                        'custrecord_urt_contract_cost',
					                        null, 'sum')) + '</td>';

					        break;
				        }
			        }

			        // if column if not found, add 0
			        if (!columnFound) {
				        offSiteBilledRow += '<td style="text-align:right;">-</td>';
				        offSiteUnbilledRow += '<td style="text-align:right;">-</td>';
				        onSiteBilledRow += '<td style="text-align:right;">-</td>';
				        onSiteUnbilledRow += '<td style="text-align:right;">-</td>';
				        utilizationRow += '<td style="text-align:right;">-</td>';
				        salariedCount += '<td style="text-align:right;">-</td>';
				        salariedCost += '<td style="text-align:right;">-</td>';
				        contractCount += '<td style="text-align:right;">-</td>';
				        contractCost += '<td style="text-align:right;">-</td>';
				        salariedCountRatio += '<td style="text-align:right;">-</td>';
				        salariedCostRatio += '<td style="text-align:right;">-</td>';
			        }
		        });

		s_table += "<tr >" + onSiteBilledRow + "</tr>";
		s_table += "<tr>" + offSiteBilledRow + "</tr>";
		s_table += "<tr>" + onSiteUnbilledRow + "</tr>";
		s_table += "<tr>" + offSiteUnbilledRow + "</tr>";

		s_table += "<tr>" + utilizationRow + "</tr>";

		s_table += "<tr>" + salariedCount + "</tr>";
		s_table += "<tr>" + salariedCost + "</tr>";
		s_table += "<tr>" + contractCount + "</tr>";
		s_table += "<tr>" + contractCost + "</tr>";
		s_table += "<tr>" + salariedCostRatio + "</tr>";
		s_table += "<tr>" + salariedCountRatio + "</tr>";

		if (nlapiGetUser() == '9673') {
		}

		s_table += "</table>";

		return table_css + s_table;
	} catch (err) {
		nlapiLogExecution('ERROR', 'generateLeadIndicatorTable', err);
	}
}

//Generate SOLD Margin Table
function generateSoldMarginTable(a_selected_project_list, a_period_list,
        startDate, endDate)
{

	try {
	var projectOne = '';
	var searchObj = '';
		if (!startDate) {
			startDate = '1/1/2015';
		}

		if (!endDate) {
			endDate = nlapiDateToString(new Date(), 'date');
		}

		nlapiLogExecution('debug', 'a_selected_project_list', JSON
		        .stringify(a_selected_project_list));
 if(a_selected_project_list.length == 1){
 projectOne = a_selected_project_list[0];
 
 var filters = [];
 filters.push(new nlobjSearchFilter('entityid',null,'is',projectOne));
 
 var cols = [];
 cols.push(new nlobjSearchColumn('companyname'));
 cols.push(new nlobjSearchColumn('custentity_practice'));
 cols.push(new nlobjSearchColumn('custentity_sold_margin_percent'));
 cols.push(new nlobjSearchColumn('customer'));
 cols.push(new nlobjSearchColumn('custentity_region'));
 cols.push(new nlobjSearchColumn('startdate'));
 cols.push(new nlobjSearchColumn('enddate'));
 cols.push(new nlobjSearchColumn('jobbillingtype'));
 //cols.push(new nlobjSearchColumn('companyname'));
 //cols.push(new nlobjSearchColumn('companyname'));
 
 searchObj = nlapiSearchRecord('job',null,filters,cols);
 
 }
 var periodList = ['Details'];
	/*	var periodList = createPeriodList(startDate, endDate);
	

		// get all the project related utilization data
		var filters = [
		        new nlobjSearchFilter('isinactive', null, 'is', 'F'),
		        new nlobjSearchFilter('custrecord_urt_start_date', null,
		                'within', startDate, endDate) ];

		var s_formula = '';
		for (var i = 0; i < a_selected_project_list.length; i++) {
			if (i > 0) {
				s_formula += " OR";
			}
			s_formula += " SUBSTR({custrecord_urt_project}, 0,9) = '"
			        + a_selected_project_list[i] + "' ";
		}
		var projectFilter = new nlobjSearchFilter('formulanumeric', null,
		        'equalto', 1);
		projectFilter.setFormula("CASE WHEN " + s_formula
		        + " THEN 1 ELSE 0 END");
				if(_logValidation(projectFilter))
		filters.push(projectFilter);
		else{
		nlapiLogExecution('debug', 'error', '991');
		}

		var utilizationSearch = nlapiSearchRecord(
		        'customrecord_utilization_report_trend',
		        null,
		        filters,
		        [
		                new nlobjSearchColumn('custrecord_urt_start_date',
		                        null, 'group').setSort(),
		                new nlobjSearchColumn('custrecord_urt_onsite_billed',
		                        null, 'sum'),
		                new nlobjSearchColumn('custrecord_urt_onsite_unbilled',
		                        null, 'sum'),
		                new nlobjSearchColumn('custrecord_urt_offsite_billed',
		                        null, 'sum'),
		                new nlobjSearchColumn(
		                        'custrecord_urt_offsite_unbilled', null, 'sum'),
		                new nlobjSearchColumn(
		                        'custrecord_urt_utilization_percent', null,
		                        'avg'),

		                new nlobjSearchColumn('custrecord_urt_salaried_count',
		                        null, 'sum'),
		                new nlobjSearchColumn('custrecord_urt_contract_count',
		                        null, 'sum'),
		                new nlobjSearchColumn('custrecord_urt_contract_cost',
		                        null, 'sum'),
		                new nlobjSearchColumn('custrecord_urt_salaried_cost',
		                        null, 'sum'),
		                new nlobjSearchColumn(
		                        'custrecord_urt_contractor_percent', null,
		                        'avg'),
		                new nlobjSearchColumn(
		                        'custrecord_urt_contractor_cost_percent', null,
		                        'avg') ]); */

		// generate HTML for table
		var table_css = "";
		table_css += "<style>";

		table_css += "#sold_indicator td:nth-child(even) {background: white}";
		table_css += "#sold_indicator td:nth-child(odd) {background: #e6e6ff}";
		table_css += "#sold_indicator tr:nth-child(even) {background: white}";
		table_css += "#sold_indicator tr:nth-child(odd) {background: #e6e6ff}";
		table_css += "#sold_indicator th {background: linear-gradient(#49708f, #293f50) !important}";
		table_css += "</style>";

		var s_table = '<table id="sold_indicator">';

		// Header
		s_table += '<thead>';
		periodList.forEach(function(period) {
			s_table += '<th style="text-align:center;">' + 'Project Details' + '</th>';
		});
		s_table += '</thead>';

		// Data Rows
		var offSiteBilledRow = "Project Name";
		var offSiteUnbilledRow = "Executing Practice";
		var onSiteBilledRow = "Sold Margin %";
		var onSiteUnbilledRow = "Customer";
		var utilizationRow = "Region";

		var salariedCountRatio = "Start Date";
		var salariedCostRatio = "End Date";
		var salariedCount = "Project Type";
		//var contractCost = "<td>Contractor Cost ($)</td>";

		

		periodList
		        .forEach(function(period) {

			        var columnFound = false;
			        for (var i = 0; i < searchObj.length; i++) {
				       // var searchDate = searchObj[i].getValue(
				       //         'custrecord_urt_start_date', null, 'group');
				      //  var d_searchDate = nlapiStringToDate(searchDate);

				        //if (period.StartDate <= d_searchDate
				      //          && d_searchDate <= period.EndDate) {
					        //columnFound = true;
					        offSiteBilledRow += '<td style="text-align:center;">'
					                + (
					                        searchObj[i]
					                                .getValue(
					                                        'companyname'
					                                        )) + '</td>';
					        offSiteUnbilledRow += '<td style="text-align:center;">'
					               + (
					                        searchObj[i]
					                                .getText(
					                                        'custentity_practice'
					                                        )) + '</td>';
					        onSiteBilledRow += '<td style="text-align:center;">'
					               + (
					                        searchObj[i]
					                                .getValue(
					                                        'custentity_sold_margin_percent'
					                                        )) + '</td>';
															
					        onSiteUnbilledRow += '<td style="text-align:center;">'
					                + (
					                        searchObj[i]
					                                .getText(
					                                        'customer'
					                                        )) + '</td>';
															
					        utilizationRow += '<td style="text-align:center;">'
					                + (
					                        searchObj[i]
					                                .getText(
					                                        'custentity_region'
					                                        )) + '</td>';

					        salariedCountRatio += '<td style="text-align:center;">'
					               + (
					                        searchObj[i]
					                                .getValue(
					                                        'startdate'
					                                        )) + '</td>';

					        salariedCostRatio += '<td style="text-align:center;">'
					              + (
					                        searchObj[i]
					                                .getValue(
					                                        'enddate'
					                                        )) + '</td>';

					        salariedCount += '<td style="text-align:center;">'
					               + (
					                        searchObj[i]
					                                .getText(
					                                        'jobbillingtype'
					                                        )) + '</td>';


					        break;
				        }
			        });
        
		s_table += "<tr><td style='font-weight:bold;'>" + onSiteBilledRow + "</td>";
		s_table += "<td style='font-weight:bold;'>" + offSiteBilledRow + "</td>";
		s_table += "<td style='font-weight:bold;'>" + onSiteUnbilledRow + "</td>";
		s_table += "<td style='font-weight:bold;'>" + offSiteUnbilledRow + "</td></tr>";

		s_table += "<tr><td style='font-weight:bold;'>" + utilizationRow + "</td>";

		s_table += "<td style='font-weight:bold;'>" + salariedCount + "</td>";
		//s_table += "<tr>" + salariedCost + "</tr>";
		//s_table += "<tr>" + contractCount + "</tr>";
		//s_table += "<tr>" + contractCost + "</tr>";
		s_table += "<td style='font-weight:bold;'>" + salariedCostRatio + "</td>";
		s_table += "<td style='font-weight:bold;'>" + salariedCountRatio + "</td></tr>"; 

		if (nlapiGetUser() == '9673') {
		}

		s_table += "</table>";

		return table_css + s_table;
	} catch (err) {
		nlapiLogExecution('ERROR', 'soldMarginTable', err);
	}
}
function getExchangerates(){
//var mnt_lat_date=getLastDate(s_mont,s_year_);
var amount=3910;
var rate=nlapiExchangeRate('USD', 'INR');
var con_amou=parseFloat(amount)*parseFloat(rate);
nlapiLogExecution('Debug','ExchangeValue'+rate,'amount'+con_amou);
var rate2=nlapiExchangeRate('INR', 'USD');
con_amou=parseFloat(con_amou)*parseFloat(rate2);
nlapiLogExecution('Debug','ExchangeValue'+rate2,'amount'+con_amou);
var column = new Array();
		column[0] = new nlobjSearchColumn('custrecord_pl_currency_exchange_year');
		column[1] = new nlobjSearchColumn('custrecord_pl_currency_exchange_month');
		column[2] = new nlobjSearchColumn('custrecord_pl_gbp_revenue');
		column[3]=new nlobjSearchColumn('internalid');	
		//var currencySearch = nlapiSearchRecord('customrecord_pl_currency_exchange_rates',null,null,column);
	//	if(currencySearch){
		//	for(var i_indx=0;i_indx < currencySearch.length;i_indx++){
		//	var id=currencySearch[i_indx].getValue('internalid');	
			//var rec=nlapiLoadRecord('customrecord_pl_currency_exchange_rates',id);	
			//	rec.setFieldValue('custrecord_pl_gbp_revenue',rate);
			//	nlapiSubmitRecord(rec);
			nlapiLogExecution('Debug','ExchangeValue',rate);
		//}
	//}					
	//return rate;
}
function getLastDate(month,year)
{
	var date='';
	if(month == 'January')
		date = "1/31/"+year;
	if(month == 'February')
		date = "2/28/"+year;
	if(month == 'March')
		date = "3/31/"+year;
	if(month == 'April')
		date = "4/30/"+year;
	if(month == 'May')
		date = "5/31/"+year;
	if(month == 'June')
		date = "6/30/"+year;
	if(month == 'July')
		date = "7/31/"+year;
	if(month == 'August')
		date = "8/31/"+year;
	if(month == 'September')
		date = "9/30/"+year;
	if(month == 'October')
		date = "10/31/"+year;
	if(month == 'November')
		date = "11/30/"+year;
	if(month == 'December')
		date = "12/31/"+year;
	
	return date;
}

//Get the complete month name
function getMonthCompleteName(month){
	var s_mont_complt_name = '';
	if(month == 'Jan')
		s_mont_complt_name = 'January';
	if(month == 'Feb')
		s_mont_complt_name = 'February';
	if(month == 'Mar')
		s_mont_complt_name = 'March';
	if(month == 'Apr')
		s_mont_complt_name = 'April';
	if(month == 'May')
		s_mont_complt_name = 'May';
	if(month == 'Jun')
		s_mont_complt_name = 'June';
	if(month == 'Jul')
		s_mont_complt_name = 'July';
	if(month == 'Aug')
		s_mont_complt_name = 'August';
	if(month == 'Sep')
		s_mont_complt_name = 'September';
	if(month == 'Oct')
		s_mont_complt_name = 'October';
	if(month == 'Nov')
		s_mont_complt_name = 'November';
	if(month == 'Dec')
		s_mont_complt_name = 'December';
	
	return s_mont_complt_name;
}
function _logValidation(value) 
{
 if(value != null && value != '- None -' && value.toString() != '- None -' && value.toString() != null && value != '' && value != undefined && value.toString() != undefined && value != 'undefined' && value.toString() != 'undefined'&& value.toString() != 'NaN' && value != NaN) 
 {
  return true;
 }
 else 
 { 
  return false;
 }
}

function getSelectedProjects(project) {
	try {
		var practice_filter = '';
		var filters = [];
		if (project != null
        && project.length != 0) {
	var s_formula = '';

	for (var i = 0; i < project.length; i++) {
		if (i != 0) {
			s_formula += " OR";
		}

		s_formula += "{name} = '"
		        + project[i] + "' ";
	}

	var projectFilter = new nlobjSearchFilter('formulanumeric', null,
	        'equalto', 1);

	projectFilter.setFormula("CASE WHEN " + s_formula
	        + " THEN 1 ELSE 0 END");

	practice_filter = filters.concat([ projectFilter ]);
} else {
	var projectFilter = new nlobjSearchFilter('formulanumeric', null,
	        'equalto', 1);

	projectFilter.setFormula("0");

	practice_filter = filters.concat([ projectFilter ]);
}

		// get the list of project the user has access to
		/*var practice_filter = [
		        [	             
		                [ 'internalid',
		                        'anyOf', project ] ]
		        ];*/

		var project_search_results = searchRecord('department', null, practice_filter,
		        [ new nlobjSearchColumn("name"),
		              
						new nlobjSearchColumn("custrecord_parent_practice"),
						new nlobjSearchColumn("internalid")
						]);

		nlapiLogExecution('debug', 'project count',
		        project_search_results.length);

		if (project_search_results.length == 0) {
			throw "You didn't selected any projects.";
		}

		return project_search_results;
	} catch (err) {
		nlapiLogExecution('ERROR', 'getTaggedPracticeList', err);
		throw err;
	}
}

function getTaggedPracticeList(i_user_id) {
	try {
		if (i_user_id == 9673) {
			i_user_id = 1759;
		}

		// get the list of project the user has access to
		var practice_filter = [
		        [
		                [ 'custrecord_practicehead', 'anyOf', i_user_id ],
		                'or',
		                [ 'custrecord_parent_practice.custrecord_practicehead', 'anyOf',
		                        i_user_id ],
		                'or',
		                [ 'custrecord_practice_head_2',
		                        'anyOf', i_user_id ] ], 'and',
		        [ 'isinactive', 'is', 'F' ] ];

		var project_search_results = searchRecord('department', null, practice_filter,
		        [ new nlobjSearchColumn("name"),
		               
						new nlobjSearchColumn("custrecord_parent_practice"),
						new nlobjSearchColumn("internalid")
						]);

		nlapiLogExecution('debug', 'project count',
		        project_search_results.length);

		if (project_search_results.length == 0) {
			throw "You don't have any projects under you.";
		}

		return project_search_results;
	} catch (err) {
		nlapiLogExecution('ERROR', 'getTaggedPracticeList', err);
		throw err;
	}
}
function removearrayduplicate(array) {
    var newArray = new Array();
    label: for (var i = 0; i < array.length; i++) {
          for (var j = 0; j < array.length; j++) {
                if (newArray[j] == array[i])
                      continue label;
          }
          newArray[newArray.length] = array[i];
    }
    return newArray;
}
//removing blanks from array list
function filter_array(test_array) {
    var index = -1,
        arr_length = test_array ? test_array.length : 0,
        resIndex = -1,
        result = [];

    while (++index < arr_length) {
        var value = test_array[index];

        if (value) {
            result[++resIndex] = value;
        }
    }

    return result;
}