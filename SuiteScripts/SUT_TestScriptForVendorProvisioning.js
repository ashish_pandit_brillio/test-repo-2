// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
/*
   	Script Name : SCH_Vendor_Provision_Create_JE.js
	Author      : Jayesh Dinde
	Date        : 21 April 2016
    Description : Create JE based on vendor provision salary upload record entries.


	Script Modification Log:

	-- Date --			-- Modified By --				--Requested By--				-- Description --




Below is a summary of the process controls enforced by this script file.  The control logic is described
more fully, below, in the appropriate function headers and code blocks.


     SCHEDULED FUNCTION
		- scheduledFunction(type)


     SUB-FUNCTIONS
		- The following sub-functions are called by the above core functions in order to maintain code
            modularization:

               - NOT USED

*/
}
// END SCRIPT DESCRIPTION BLOCK  ====================================


// BEGIN SCHEDULED FUNCTION =============================================

function create_je_sch_func(type)
{
	try
	{
		var i_context = nlapiGetContext();
		//var rcrd_id = i_context.getSetting('SCRIPT', 'custscript_rcrd_id');
		var month = 'FEB'//i_context.getSetting('SCRIPT', 'custscript_vendor_provision_month');
		//month = month.trim();
		var year = '2018'//i_context.getSetting('SCRIPT', 'custscript_vendor_provision_year');
		year = year.trim();
		//var debit_account = i_context.getSetting('SCRIPT', 'custscript_debit_account');
		//debit_account = debit_account.trim();
		//debit_account = Number(debit_account);
		///var credit_account = i_context.getSetting('SCRIPT', 'custscript_credit_account');
		//credit_account = credit_account.trim();
		//credit_account = Number(credit_account);
		//var i_counter = i_context.getSetting('SCRIPT', 'custscript_i_counter');
		//var vendor_provision_subsidiary = i_context.getSetting('SCRIPT', 'custscript_ven_pro_subsidiary_id');
		//var je_reversal_date = nlapiLookupField('customrecord_salary_upload_process_btpl',rcrd_id,'custrecord_je_reversal_date');
		
		//if (i_counter != null && i_counter != '' && i_counter != undefined)
		{
			//var i = i_counter;
		}
		//else
		{
			//var i = 0;
		}
		
		var d_start_date = get_current_month_start_date(month, year);
		var d_end_date = get_current_month_end_date(month, year);
		
		d_start_date = nlapiStringToDate(d_start_date);
		d_end_date = nlapiStringToDate(d_end_date);
				
		var nt_submitted_sr_no = 1;
		var nt_apprvd_sr_no = 1;
		var apprvd_sr_no = 1;
		var past_mnth_sr_no = 1;
		
		var past_mnth_je = nlapiCreateRecord('journalentry');
		past_mnth_je.setFieldValue('reversaldate', je_reversal_date);
		past_mnth_je.setFieldValue('subsidiary', parseInt(vendor_provision_subsidiary));
		past_mnth_je.setFieldValue('approved', 'F');
		var cust_fin_head = new Array();
		cust_fin_head.push(1615);
		cust_fin_head.push(69145);
		past_mnth_je.setFieldValues('custbody_financehead',cust_fin_head);
		
		var total_cost_past_mnth = 0;
		var a_emp_considered = new Array();
		//code to create past month entry for termed emp
		var filters_past_mnth_termed = new Array();
		filters_past_mnth_termed[0] = new nlobjSearchFilter('custrecord_termed_emp', null, 'is', 'T');
		filters_past_mnth_termed[1] = new nlobjSearchFilter('custrecord_processed_rcrd', null, 'is', 'F');
		filters_past_mnth_termed[2] = new nlobjSearchFilter('custrecord_current_month', null, 'is', month);
		filters_past_mnth_termed[3] = new nlobjSearchFilter('custrecord_current_year', null, 'is', year);
		filters_past_mnth_termed[4] = new nlobjSearchFilter('custrecord_rate_type_lst_mnth', null, 'is', 2);
		filters_past_mnth_termed[5] = new nlobjSearchFilter('custrecord_subsidiary_processed', null, 'is', parseInt(vendor_provision_subsidiary));
					
		var column_past_mnth_termed = new Array();
		column_past_mnth_termed[0] = new nlobjSearchColumn('custrecord_tot_hours_nt_blld');
		column_past_mnth_termed[1] = new nlobjSearchColumn('custrecord_st_rate_emp');
		column_past_mnth_termed[2] = new nlobjSearchColumn('custrecord_proj_termed_emp');
		column_past_mnth_termed[3] = new nlobjSearchColumn('custrecord_customer_termed_emp');
		column_past_mnth_termed[4] = new nlobjSearchColumn('custentity_vertical','custrecord_proj_termed_emp');
		column_past_mnth_termed[5] = new nlobjSearchColumn('territory','custrecord_customer_termed_emp');
		column_past_mnth_termed[6] = new nlobjSearchColumn('department','custrecord_emp_name_lst_mnth');
		column_past_mnth_termed[7] = new nlobjSearchColumn('custrecord_emp_name_lst_mnth');
		column_past_mnth_termed[8] = new nlobjSearchColumn('custrecord_leaves_last_month'); 
		var lst_mnth_data_termed = nlapiSearchRecord('customrecord_vendor_pro_lst_mnth_data', null, filters_past_mnth_termed, column_past_mnth_termed);
		if(lst_mnth_data_termed)
		{
			for(var i_index_past=0; i_index_past<lst_mnth_data_termed.length; i_index_past++)
			{
				var no_leaves_allowed = 0;
				var tot_hrs_days_to_billed = 0;
				var no_of_leaves_cost_deducted_lst_month = 0;
				
				var s_Emp_name = lst_mnth_data_termed[i_index_past].getText('custrecord_emp_name_lst_mnth');
				var i_Emp_name = lst_mnth_data_termed[i_index_past].getValue('custrecord_emp_name_lst_mnth');
				var i_emp_practice = lst_mnth_data_termed[i_index_past].getValue('department','custrecord_emp_name_lst_mnth');
				var i_cust_territory = lst_mnth_data_termed[i_index_past].getValue('territory','custrecord_customer_termed_emp');
				var i_project_vertical = lst_mnth_data_termed[i_index_past].getValue('custentity_vertical','custrecord_proj_termed_emp');
				var s_project_name = lst_mnth_data_termed[i_index_past].getText('custrecord_proj_termed_emp');
				var i_project_id = lst_mnth_data_termed[i_index_past].getValue('custrecord_proj_termed_emp');
				var s_customer_name = lst_mnth_data_termed[i_index_past].getText('custrecord_customer_termed_emp');
				
				var rcrd_id_lst_mnth_data = lst_mnth_data_termed[i_index_past].getId();
				var tot_hours = lst_mnth_data_termed[i_index_past].getValue('custrecord_tot_hours_nt_blld');
				tot_hours = parseFloat(tot_hours);
				
				if(a_emp_considered.indexOf(parseInt(i_Emp_name)) < 0) 
				{
					if (_logValidation(tot_hours) && tot_hours != 0)
					{
						var no_of_leaves_taken_lst_mnth = lst_mnth_data_termed[i_index_past].getValue('custrecord_leaves_last_month');
						var monthly_rate_lst_mnth = lst_mnth_data_termed[i_index_past].getValue('custrecord_st_rate_emp');
						
						if (monthly_rate_lst_mnth) {
							var tot_hrs_days = tot_hours / parseInt(8);
							tot_hrs_days = parseFloat(tot_hrs_days);
							
							var previous_month = get_previous_month(month);
							previous_month = previous_month.toString();
							if (previous_month == 'December') {
								var i_year_temp = Number(year) - Number(1);
							}
							else {
								var i_year_temp = year;
							}
							
							var days_in_mnth_lst_mnth = daysInMonth(previous_month, i_year_temp);
							
							var monthly_rate_lst_mnth_daily = parseFloat(monthly_rate_lst_mnth) / parseInt(days_in_mnth_lst_mnth);
							monthly_rate_lst_mnth_daily = parseFloat(monthly_rate_lst_mnth_daily);
							
							monthly_rate_lst_mnth_daily = monthly_rate_lst_mnth_daily.toString();
							if (monthly_rate_lst_mnth_daily.indexOf(".") > 0) {
								var temp_index = monthly_rate_lst_mnth_daily.indexOf(".") + 3;
								monthly_rate_lst_mnth_daily = monthly_rate_lst_mnth_daily.substr(0, temp_index);
							}
							monthly_rate_lst_mnth_daily = parseFloat(monthly_rate_lst_mnth_daily);
							
							if (_logValidation(no_of_leaves_taken_lst_mnth) && no_of_leaves_taken_lst_mnth != 0) {
								no_leaves_allowed = parseFloat(no_of_leaves_taken_lst_mnth) - parseInt(1);
							}
							
							if (no_leaves_allowed == 0) {
							
							}
							else {
								no_of_leaves_cost_deducted_lst_month = parseFloat(no_leaves_allowed) * parseFloat(monthly_rate_lst_mnth_daily);
								no_of_leaves_cost_deducted_lst_month = no_of_leaves_cost_deducted_lst_month.toFixed(2);
								monthly_rate_lst_mnth = parseFloat(monthly_rate_lst_mnth) - parseFloat(no_of_leaves_cost_deducted_lst_month);
								//monthly_rate = monthly_rate.toFixed(2);
								
								monthly_rate_lst_mnth = monthly_rate_lst_mnth.toString();
								if (monthly_rate_lst_mnth.indexOf(".") > 0) {
									var temp_index = monthly_rate_lst_mnth.indexOf(".") + 3;
									monthly_rate_lst_mnth = monthly_rate_lst_mnth.substr(0, temp_index);
								}
								monthly_rate_lst_mnth = parseFloat(monthly_rate_lst_mnth);
								
							//nlapiLogExecution('audit','monthly rate deducted:---',monthly_rate);
							}
							
							var temp_strt_date = get_previous_month_start_date(previous_month, i_year_temp);
							var temp_end_date = get_previous_month_end_date(previous_month, i_year_temp);
							
							var total_sat_sun_lst_mnth = getWeekend(temp_strt_date, temp_end_date);
							
							days_in_mnth_lst_mnth = parseInt(days_in_mnth_lst_mnth) - parseInt(total_sat_sun_lst_mnth);
							monthly_rate_lst_mnth = parseFloat(monthly_rate_lst_mnth) / parseFloat(days_in_mnth_lst_mnth);
							monthly_rate_lst_mnth = monthly_rate_lst_mnth.toString();
							if (monthly_rate_lst_mnth.indexOf(".") > 0) {
								var temp_index = monthly_rate_lst_mnth.indexOf(".") + 3;
								monthly_rate_lst_mnth = monthly_rate_lst_mnth.substr(0, temp_index);
							}
							monthly_rate_lst_mnth = parseFloat(monthly_rate_lst_mnth);
							
							tot_hrs_days_to_billed = parseFloat(tot_hrs_days) * parseFloat(monthly_rate_lst_mnth);
							tot_hrs_days_to_billed = tot_hrs_days_to_billed.toString();
							if (tot_hrs_days_to_billed.indexOf(".") > 0) {
								var temp_index = tot_hrs_days_to_billed.indexOf(".") + 3;
								tot_hrs_days_to_billed = tot_hrs_days_to_billed.substr(0, temp_index);
							}
							tot_hrs_days_to_billed = parseFloat(tot_hrs_days_to_billed);
							
							nlapiLogExecution('debug', 's_Emp_name:- '+s_Emp_name+':::tot_hrs_days_to_billed:- '+tot_hrs_days_to_billed, 'tot_hrs_days:- ' + tot_hrs_days + ':: monthly_rate_lst_mnth:-' + monthly_rate_lst_mnth);
							
							past_mnth_je.selectNewLineItem('line');
							past_mnth_je.setCurrentLineItemValue('line', 'custcol_srnumber', parseInt(past_mnth_sr_no));
							past_mnth_sr_no++;
							past_mnth_je.setCurrentLineItemValue('line', 'account', debit_account);
							past_mnth_je.setCurrentLineItemValue('line', 'debit', tot_hrs_days_to_billed);
							past_mnth_je.setCurrentLineItemValue('line', 'department', i_emp_practice);
							past_mnth_je.setCurrentLineItemValue('line', 'class', i_project_vertical);
							past_mnth_je.setCurrentLineItemValue('line', 'custcol_project_name', s_project_name);
							past_mnth_je.setCurrentLineItemValue('line', 'custcolprj_name', s_project_name);
							past_mnth_je.setCurrentLineItemValue('line', 'custcol_sow_project', i_project_id);
							past_mnth_je.setCurrentLineItemValue('line', 'custcol_territory', i_cust_territory);
							past_mnth_je.setCurrentLineItemValue('line', 'custcol_employeenamecolumn', s_Emp_name);
							past_mnth_je.setCurrentLineItemValue('line', 'custcolcustcol_temp_customer', s_customer_name);
							past_mnth_je.setCurrentLineItemValue('line', 'memo', 'Contractor cost ' + previous_month + ' ' + i_year_temp + '/ Last Month Entries');
							past_mnth_je.commitLineItem('line');
							
							if (total_cost_past_mnth == 0) {
								total_cost_past_mnth = parseFloat(tot_hrs_days_to_billed);
								total_cost_past_mnth = total_cost_past_mnth.toFixed(2);
								
							}
							else {
								total_cost_past_mnth = parseFloat(total_cost_past_mnth) + parseFloat(tot_hrs_days_to_billed);
								total_cost_past_mnth = total_cost_past_mnth.toFixed(2);
								
							}
							
							nlapiSubmitField('customrecord_vendor_pro_lst_mnth_data', rcrd_id_lst_mnth_data, 'custrecord_processed_rcrd', 'T');
							a_emp_considered.push(parseInt(i_Emp_name));
						}
					}
				}// end of if loop for emp already exist
			}
		}
		nlapiLogExecution('audit','total_cost_past_mnth after termed:- '+total_cost_past_mnth);
				
		// code to fetch current month active contractors and process them
		var filters_emp_list = new Array();
		filters_emp_list[0] = new nlobjSearchFilter('startdate',null,'onorbefore',d_end_date);
		filters_emp_list[1] = new nlobjSearchFilter('enddate',null,'onorafter',d_start_date);
		filters_emp_list[2] = new nlobjSearchFilter('subsidiary','employee','is',parseInt(vendor_provision_subsidiary));
	
		var emp_list_result = nlapiSearchRecord(null, 'customsearch_emplist_for_je_create_btpl', filters_emp_list, null);
		if (_logValidation(emp_list_result))
		{
			nlapiLogExecution('debug', 'emp process length:-- ', emp_list_result.length);
			if (i == emp_list_result.length)
			{
				return true;
			}
			
			var je_nt_submittd = nlapiCreateRecord('journalentry');
			var je_nt_approved = nlapiCreateRecord('journalentry');
			var je_approved = nlapiCreateRecord('journalentry');
			
			je_nt_submittd.setFieldValue('reversaldate', je_reversal_date);
			je_nt_approved.setFieldValue('reversaldate', je_reversal_date);
			je_approved.setFieldValue('reversaldate', je_reversal_date);
			
			je_nt_submittd.setFieldValue('approved', 'F');
			je_nt_approved.setFieldValue('approved', 'F');
			je_approved.setFieldValue('approved', 'F');
			
			je_nt_submittd.setFieldValue('subsidiary', parseInt(vendor_provision_subsidiary));
			je_nt_approved.setFieldValue('subsidiary', parseInt(vendor_provision_subsidiary));
			je_approved.setFieldValue('subsidiary', parseInt(vendor_provision_subsidiary));
			
			je_nt_submittd.setFieldValues('custbody_financehead',cust_fin_head);
			je_nt_approved.setFieldValues('custbody_financehead',cust_fin_head);
			je_approved.setFieldValues('custbody_financehead',cust_fin_head);
			
			var total_credit_cost_approved = new Array();
			var total_credit_cost_nt_approved = 0;
			var total_credit_cost_nt_submitted = 0;
			
			var Emp_leave_list_arr = new Array();
			var approved_entry_exist_flag = 0;
			var nt_approved_entry_exist_flag = 0;
			var nt_submitted_entry_exist_flag = 0;
				
			for (; i < emp_list_result.length; i++)
			{
				var result_C = emp_list_result[0];
				var columns = result_C.getAllColumns();
				var columnLen = columns.length;
				
				for (var j = 0; j < columnLen; j++)
				{
					var column = columns[j];
					var label = column.getLabel();
					
					if (label == 'Employee Id')
					{
						var Emp_id = emp_list_result[i].getValue(column);
					}
					else if (label == 'Employee')
					{
						var Emp_name = emp_list_result[i].getText(column);
					}
				}
				
				//nlapiLogExecution('debug','employee being processed:- ',Emp_name);
				
				var filters_sal_upload = new Array();
				filters_sal_upload[0] = new nlobjSearchFilter('custrecord_month_sal_up_ven', null, 'is', month);
				filters_sal_upload[1] = new nlobjSearchFilter('custrecord_year_sal_up_ven', null, 'is', year);
				filters_sal_upload[2] = new nlobjSearchFilter('custrecord_is_processed', null, 'is', 'F');
				filters_sal_upload[3] = new nlobjSearchFilter('custrecord_employee_rec_id_sal_up_ven', null, 'is', Emp_id);
				filters_sal_upload[4] = new nlobjSearchFilter('custrecord_rate_type_sal_upload', null,'is',2);
				filters_sal_upload[5] = new nlobjSearchFilter('custrecord_subsidiary_sal_up_ven', null,'is',parseInt(vendor_provision_subsidiary));
				
				var column_sal_upload = new Array();
				column_sal_upload[0] = new nlobjSearchColumn('custrecord_project_assigned');
				column_sal_upload[1] = new nlobjSearchColumn('custrecord_no_of_leaves_taken');
				column_sal_upload[2] = new nlobjSearchColumn('custrecord_no_of_holidays');
				column_sal_upload[3] = new nlobjSearchColumn('custrecord_no_of_approved_days');
				column_sal_upload[4] = new nlobjSearchColumn('custrecord_no_of_sub_nt_approved_days');
				column_sal_upload[5] = new nlobjSearchColumn('custrecord_no_of_nt_submitted_days');
				column_sal_upload[6] = new nlobjSearchColumn('custrecord_cost_sal_up_ven');
				column_sal_upload[7] = new nlobjSearchColumn('custrecord_practice_sla_up_ven');
				column_sal_upload[8] = new nlobjSearchColumn('custrecord_location_sla_up_ven');
				column_sal_upload[9] = new nlobjSearchColumn('custrecord_subsidiary_sal_up_ven');
				column_sal_upload[10] = new nlobjSearchColumn('custrecord_project_assigned_id');
				column_sal_upload[11] = new nlobjSearchColumn('custrecord_allocated_time');
				
				var vendor_sal_upload_result = nlapiSearchRecord('customrecord_salary_upload_file', null, filters_sal_upload, column_sal_upload);
				
				if (_logValidation(vendor_sal_upload_result))
				{
					var monthly_rate_lst_mnth = vendor_sal_upload_result[0].getValue('custrecord_cost_sal_up_ven');
					var project = vendor_sal_upload_result[0].getValue('custrecord_project_assigned');
					var proj_id = vendor_sal_upload_result[0].getValue('custrecord_project_assigned_id');
					if (_logValidation(proj_id))
					{
						var proj_rcrd = nlapiLoadRecord('job',proj_id);
						var proj_cust_id = proj_rcrd.getFieldValue('parent');
						var proj_cust_name = proj_rcrd.getFieldText('parent');
						var vertical = proj_rcrd.getFieldValue('custentity_vertical');
						if (_logValidation(proj_cust_id))
						{
							var proj_cust_territory = nlapiLookupField('customer',proj_cust_id,'territory');
						}
					}
					
					var practice = vendor_sal_upload_result[0].getValue('custrecord_practice_sla_up_ven');
					//var vertical = vendor_sal_upload_result[0].getValue('custrecord_project_vertical_vendor_pro');
						
					var filters_past_mnth_data = new Array();
					filters_past_mnth_data[0] = new nlobjSearchFilter('custrecord_emp_name_lst_mnth', null, 'is', Emp_id);
					filters_past_mnth_data[1] = new nlobjSearchFilter('custrecord_processed_rcrd', null, 'is', 'F');
					filters_past_mnth_data[2] = new nlobjSearchFilter('custrecord_current_month', null, 'is', month);
					filters_past_mnth_data[3] = new nlobjSearchFilter('custrecord_current_year', null, 'is', year);
					filters_past_mnth_data[4] = new nlobjSearchFilter('custrecord_rate_type_lst_mnth', null, 'is', 2);
					filters_past_mnth_data[5] = new nlobjSearchFilter('custrecord_subsidiary_processed', null, 'is', parseInt(vendor_provision_subsidiary));
					
					var column_past_mnth_data = new Array();
					column_past_mnth_data[0] = new nlobjSearchColumn('custrecord_tot_hours_nt_blld');
					column_past_mnth_data[1] = new nlobjSearchColumn('custrecord_leaves_last_month');
					var lst_mnth_data = nlapiSearchRecord('customrecord_vendor_pro_lst_mnth_data', null, filters_past_mnth_data, column_past_mnth_data);
					if (_logValidation(lst_mnth_data))
					{
						var no_leaves_allowed = 0;
						var tot_hrs_days_to_billed = 0;
						var no_of_leaves_cost_deducted_lst_month = 0;
						var rcrd_id_lst_mnth_data = lst_mnth_data[0].getId();
						var tot_hours = lst_mnth_data[0].getValue('custrecord_tot_hours_nt_blld');
						tot_hours = parseFloat(tot_hours);
						if (_logValidation(tot_hours) && tot_hours != 0)
						{
							var no_of_leaves_taken_lst_mnth = lst_mnth_data[0].getValue('custrecord_leaves_last_month');
							var tot_hrs_days = tot_hours / parseInt(8);
							tot_hrs_days = parseFloat(tot_hrs_days);
							
							var previous_month = get_previous_month(month);
							previous_month = previous_month.toString();
							if(previous_month == 'December')
							{
								var i_year_temp = Number(year) - Number(1);
							}
							else
							{
								var i_year_temp = year;
							}
							
							var days_in_mnth_lst_mnth = daysInMonth(previous_month,i_year_temp);
							
							var monthly_rate_lst_mnth_daily = parseFloat(monthly_rate_lst_mnth) / parseInt(days_in_mnth_lst_mnth);
							monthly_rate_lst_mnth_daily = parseFloat(monthly_rate_lst_mnth_daily);
						
							monthly_rate_lst_mnth_daily = monthly_rate_lst_mnth_daily.toString();
							if(monthly_rate_lst_mnth_daily.indexOf(".")>0)
							{
								var temp_index = monthly_rate_lst_mnth_daily.indexOf(".")+3;
								monthly_rate_lst_mnth_daily = monthly_rate_lst_mnth_daily.substr(0,temp_index);
							}
							monthly_rate_lst_mnth_daily = parseFloat(monthly_rate_lst_mnth_daily);
							
							if (_logValidation(no_of_leaves_taken_lst_mnth) && no_of_leaves_taken_lst_mnth != 0)
							{
								no_leaves_allowed = parseFloat(no_of_leaves_taken_lst_mnth) - parseInt(1);
							}
							
							if(no_leaves_allowed == 0)
							{
								
							}
							else
							{
								no_of_leaves_cost_deducted_lst_month = parseFloat(no_leaves_allowed) * parseFloat(monthly_rate_lst_mnth_daily);
								no_of_leaves_cost_deducted_lst_month = no_of_leaves_cost_deducted_lst_month.toFixed(2);
								monthly_rate_lst_mnth = parseFloat(monthly_rate_lst_mnth) - parseFloat(no_of_leaves_cost_deducted_lst_month);
								//monthly_rate = monthly_rate.toFixed(2);
								
								monthly_rate_lst_mnth = monthly_rate_lst_mnth.toString();
								if(monthly_rate_lst_mnth.indexOf(".")>0)
								{
									var temp_index = monthly_rate_lst_mnth.indexOf(".")+3;
									monthly_rate_lst_mnth = monthly_rate_lst_mnth.substr(0,temp_index);
								}
								monthly_rate_lst_mnth = parseFloat(monthly_rate_lst_mnth);
								
								//nlapiLogExecution('audit','monthly rate deducted:---',monthly_rate);
							}
							
							nlapiLogExecution('debug','month previous:- ',previous_month);
							var temp_strt_date = get_previous_month_start_date(previous_month,i_year_temp);
							var temp_end_date = get_previous_month_end_date(previous_month,i_year_temp);
							
							var total_sat_sun_lst_mnth = getWeekend(temp_strt_date,temp_end_date);
							
							days_in_mnth_lst_mnth = parseInt(days_in_mnth_lst_mnth) - parseInt(total_sat_sun_lst_mnth);
							monthly_rate_lst_mnth = parseFloat(monthly_rate_lst_mnth) / parseFloat(days_in_mnth_lst_mnth);
							monthly_rate_lst_mnth = monthly_rate_lst_mnth.toString();
							if(monthly_rate_lst_mnth.indexOf(".")>0)
							{
								var temp_index = monthly_rate_lst_mnth.indexOf(".")+3;
								monthly_rate_lst_mnth = monthly_rate_lst_mnth.substr(0,temp_index);
							}
							monthly_rate_lst_mnth = parseFloat(monthly_rate_lst_mnth);
							
							tot_hrs_days_to_billed = parseFloat(tot_hrs_days) * parseFloat(monthly_rate_lst_mnth);
							tot_hrs_days_to_billed = tot_hrs_days_to_billed.toString();
							if(tot_hrs_days_to_billed.indexOf(".")>0)
							{
								var temp_index = tot_hrs_days_to_billed.indexOf(".")+3;
								tot_hrs_days_to_billed = tot_hrs_days_to_billed.substr(0,temp_index);
							}
							tot_hrs_days_to_billed = parseFloat(tot_hrs_days_to_billed);
							
							past_mnth_je.selectNewLineItem('line');
							past_mnth_je.setCurrentLineItemValue('line', 'custcol_srnumber', parseInt(past_mnth_sr_no));
							past_mnth_sr_no++;
							past_mnth_je.setCurrentLineItemValue('line', 'account', debit_account);
							past_mnth_je.setCurrentLineItemValue('line', 'debit', tot_hrs_days_to_billed);
							past_mnth_je.setCurrentLineItemValue('line', 'department', practice);
							past_mnth_je.setCurrentLineItemValue('line', 'class', vertical);
							past_mnth_je.setCurrentLineItemValue('line', 'custcol_project_name', project);
							past_mnth_je.setCurrentLineItemValue('line', 'custcolprj_name', project);
							past_mnth_je.setCurrentLineItemValue('line', 'custcol_sow_project', proj_id);
							past_mnth_je.setCurrentLineItemValue('line', 'custcol_territory', proj_cust_territory);
							past_mnth_je.setCurrentLineItemValue('line', 'custcol_employeenamecolumn', Emp_name);
							past_mnth_je.setCurrentLineItemValue('line', 'custcolcustcol_temp_customer', proj_cust_name);
							past_mnth_je.setCurrentLineItemValue('line', 'memo', 'Contractor cost '+previous_month+' '+i_year_temp+'/ Last Month Entries');
							past_mnth_je.commitLineItem('line');
							
							if(total_cost_past_mnth == 0)
							{
								total_cost_past_mnth = parseFloat(tot_hrs_days_to_billed);
								total_cost_past_mnth = total_cost_past_mnth.toFixed(2);
								
								/*total_cost_past_mnth = total_cost_past_mnth.toString();
								if(total_cost_past_mnth.indexOf(".")>0)
								{
									var temp_index = total_cost_past_mnth.indexOf(".")+3;
									total_cost_past_mnth = total_cost_past_mnth.substr(0,temp_index);
								}
								total_cost_past_mnth = parseFloat(total_cost_past_mnth);*/
							
							}
							else
							{
								total_cost_past_mnth = parseFloat(total_cost_past_mnth) + parseFloat(tot_hrs_days_to_billed);
								total_cost_past_mnth = total_cost_past_mnth.toFixed(2);
								
								/*total_cost_past_mnth = total_cost_past_mnth.toString();
								if(total_cost_past_mnth.indexOf(".")>0)
								{
									var temp_index = total_cost_past_mnth.indexOf(".")+3;
									total_cost_past_mnth = total_cost_past_mnth.substr(0,temp_index);
								}
								total_cost_past_mnth = parseFloat(total_cost_past_mnth);*/
							}
						}	
						nlapiSubmitField('customrecord_vendor_pro_lst_mnth_data',rcrd_id_lst_mnth_data,'custrecord_processed_rcrd','T');
					}
					
					for (var t=0; t < vendor_sal_upload_result.length; t++)
					{
						var no_of_leaves_cost = 0;
						var no_of_leaves_cost_deducted = 0;
						var no_of_holidays_cost = 0;
						var approved_days_cost = 0;
						var nt_approved_days_cost = 0;
						var nt_submitted_days_cost = 0;	
						var no_leaves_allowed = 0;	
						var leaves_flag = 0;	
						var total_approved_cost_to_push = 0;			
						
						var rcrd_id_sal_upload_file = vendor_sal_upload_result[t].getId();
						var project = vendor_sal_upload_result[t].getValue('custrecord_project_assigned');
						var proj_id = vendor_sal_upload_result[t].getValue('custrecord_project_assigned_id');
						if (_logValidation(proj_id))
						{
							var proj_rcrd = nlapiLoadRecord('job',proj_id);
							var proj_cust_id = proj_rcrd.getFieldValue('parent');
							var proj_cust_name = proj_rcrd.getFieldText('parent');
							var vertical = proj_rcrd.getFieldValue('custentity_vertical');
							if (_logValidation(proj_cust_id))
							{
								var proj_cust_territory = nlapiLookupField('customer',proj_cust_id,'territory');
							}
						}
						
						var total_allocated_tym = vendor_sal_upload_result[t].getValue('custrecord_allocated_time');
						var no_of_leaves_taken = vendor_sal_upload_result[t].getValue('custrecord_no_of_leaves_taken');
						var no_of_holidays = vendor_sal_upload_result[t].getValue('custrecord_no_of_holidays');
						var approved_days = vendor_sal_upload_result[t].getValue('custrecord_no_of_approved_days');
						var nt_approved_days = vendor_sal_upload_result[t].getValue('custrecord_no_of_sub_nt_approved_days');
						var nt_submitted_days = vendor_sal_upload_result[t].getValue('custrecord_no_of_nt_submitted_days');
						var practice = vendor_sal_upload_result[t].getValue('custrecord_practice_sla_up_ven');
						//var vertical = vendor_sal_upload_result[t].getValue('custrecord_project_vertical_vendor_pro');
						var location = vendor_sal_upload_result[t].getValue('custrecord_location_sla_up_ven');
						var monthly_rate = vendor_sal_upload_result[t].getValue('custrecord_cost_sal_up_ven');
						var monthly_rate_total_days = monthly_rate;
						var subsidiary = vendor_sal_upload_result[t].getValue('custrecord_subsidiary_sal_up_ven');
						//nlapiLogExecution('audit','monthly rate:---',monthly_rate);
						monthly_rate = parseFloat(monthly_rate);
						//monthly_rate = monthly_rate.toFixed(2);
						
						monthly_rate = monthly_rate.toString();
						if(monthly_rate.indexOf(".")>0)
						{
							var temp_index = monthly_rate.indexOf(".")+3;
							monthly_rate = monthly_rate.substr(0,temp_index);
						}
						monthly_rate = parseFloat(monthly_rate);
								
						
						var days_in_mnth = daysInMonth(month,year);
						var daily_rate = parseFloat(monthly_rate) / parseInt(days_in_mnth);
						daily_rate = parseFloat(daily_rate);
						//daily_rate = daily_rate.toFixed(2);
						
						daily_rate = daily_rate.toString();
						if(daily_rate.indexOf(".")>0)
						{
							var temp_index = daily_rate.indexOf(".")+3;
							daily_rate = daily_rate.substr(0,temp_index);
						}
						daily_rate = parseFloat(daily_rate);
						
						var temp_no_of_leaves = 0;
						
						approved_days = parseFloat(approved_days);
						nt_approved_days = parseFloat(nt_approved_days);
						nt_submitted_days = parseFloat(nt_submitted_days);
						
						if(vendor_sal_upload_result.length > 1)
						{
							for(var s=0; s<vendor_sal_upload_result.length; s++)
							{
								var no_of_leaves_taken_per_rcrd = vendor_sal_upload_result[s].getValue('custrecord_no_of_leaves_taken');
									
								if (_logValidation(no_of_leaves_taken_per_rcrd) && no_of_leaves_taken_per_rcrd != 0)
								{	
									leaves_flag = 1;
									if(temp_no_of_leaves == 0)
									{
										temp_no_of_leaves = no_of_leaves_taken_per_rcrd;
									}
									else
									{
										temp_no_of_leaves = parseFloat(temp_no_of_leaves) + parseFloat(no_of_leaves_taken_per_rcrd);
									}
								}
							}
							
							if(leaves_flag == 1)
							{
								no_leaves_allowed = parseFloat(temp_no_of_leaves) - parseInt(1);
							}
							
						}
						else
						{
							if (_logValidation(no_of_leaves_taken) && no_of_leaves_taken != 0)
							{
								no_leaves_allowed = parseFloat(no_of_leaves_taken) - parseInt(1);
							}
						}
						
						if(no_leaves_allowed == 0)
						{
							
						}
						else
						{
							no_of_leaves_cost_deducted = parseFloat(no_leaves_allowed) * parseFloat(daily_rate);
							no_of_leaves_cost_deducted = no_of_leaves_cost_deducted.toFixed(2);
							monthly_rate = parseFloat(monthly_rate) - parseFloat(no_of_leaves_cost_deducted);
							//monthly_rate = monthly_rate.toFixed(2);
							
							monthly_rate = monthly_rate.toString();
							if(monthly_rate.indexOf(".")>0)
							{
								var temp_index = monthly_rate.indexOf(".")+3;
								monthly_rate = monthly_rate.substr(0,temp_index);
							}
							monthly_rate = parseFloat(monthly_rate);
							
							//nlapiLogExecution('audit','monthly rate deducted:---',monthly_rate);
						}
						
						var temp_get_sat_sun_strt_date = get_current_month_start_date(month, year);
						var temp_get_sat_sun_end_date = get_current_month_end_date(month, year);
						var total_sat_sun = getWeekend(temp_get_sat_sun_strt_date,temp_get_sat_sun_end_date);
						
						days_in_mnth = parseInt(days_in_mnth) - parseInt(total_sat_sun);
						//nlapiLogExecution('audit','days in mnth:--',days_in_mnth);
						daily_rate = parseFloat(monthly_rate) / parseInt(days_in_mnth);
						//daily_rate = daily_rate.toFixed(2);
						
						daily_rate = daily_rate.toString();
						if(daily_rate.indexOf(".")>0)
						{
							var temp_index = daily_rate.indexOf(".")+3;
							daily_rate = daily_rate.substr(0,temp_index);
						}
						daily_rate = parseFloat(daily_rate);
						
						//nlapiLogExecution('debug','daily rate:-- '+Emp_name,daily_rate);
						
						if (_logValidation(no_of_leaves_taken) && no_of_leaves_taken != 0)
						{
							if(Emp_leave_list_arr.indexOf(Emp_id) > -1)
							{
								if (_logValidation(no_of_leaves_taken) && no_of_leaves_taken != 0)
								{
									no_of_leaves_cost = parseFloat(no_of_leaves_taken) * parseFloat(daily_rate);
									//no_of_leaves_cost = no_of_leaves_cost.toFixed(2);
									
									no_of_leaves_cost = no_of_leaves_cost.toString();
									if(no_of_leaves_cost.indexOf(".")>0)
									{
										var temp_index = no_of_leaves_cost.indexOf(".")+3;
										no_of_leaves_cost = no_of_leaves_cost.substr(0,temp_index);
									}
									no_of_leaves_cost = parseFloat(no_of_leaves_cost);
									
								}
							}
							else
							{
								/*Emp_leave_list_arr.push(Emp_id);
								no_of_leaves_taken = parseInt(no_of_leaves_taken) - parseInt(1);
								if (_logValidation(no_of_leaves_taken) && no_of_leaves_taken != 0)
								{
									no_of_leaves_cost = parseFloat(no_of_leaves_taken) * parseFloat(daily_rate);
									no_of_leaves_cost = no_of_leaves_cost.toFixed(2);
								}*/
								
								no_of_leaves_cost = parseFloat(no_of_leaves_taken) * parseFloat(daily_rate);
								//no_of_leaves_cost = no_of_leaves_cost.toFixed(2);
								
								no_of_leaves_cost = no_of_leaves_cost.toString();
								if(no_of_leaves_cost.indexOf(".")>0)
								{
									var temp_index = no_of_leaves_cost.indexOf(".")+3;
									no_of_leaves_cost = no_of_leaves_cost.substr(0,temp_index);
								}
								no_of_leaves_cost = parseFloat(no_of_leaves_cost);
							}
						}
						
						if (_logValidation(no_of_holidays) && no_of_holidays != 0)
						{
							no_of_holidays_cost = parseFloat(no_of_holidays) * parseFloat(daily_rate);
							//no_of_holidays_cost = no_of_holidays_cost.toFixed(2);
							
							no_of_holidays_cost = no_of_holidays_cost.toString();
							if(no_of_holidays_cost.indexOf(".")>0)
							{
								var temp_index = no_of_holidays_cost.indexOf(".")+3;
								no_of_holidays_cost = no_of_holidays_cost.substr(0,temp_index);
							}
							no_of_holidays_cost = parseFloat(no_of_holidays_cost); 
						}
						
						if (_logValidation(approved_days) && approved_days != 0)
						{
							approved_days_cost = parseFloat(approved_days) * parseFloat(daily_rate);
							approved_days_cost = parseFloat(approved_days_cost);
							//approved_days_cost = approved_days_cost.toFixed(2);
							
							approved_days_cost = approved_days_cost.toString();
							if(approved_days_cost.indexOf(".")>0)
							{
								var temp_index = approved_days_cost.indexOf(".")+3;
								approved_days_cost = approved_days_cost.substr(0,temp_index);
							}
							approved_days_cost = parseFloat(approved_days_cost);
							nlapiLogExecution('debug','approved_days:--',approved_days);
							nlapiLogExecution('debug','approved_days_cost:--'+Emp_name,approved_days_cost);
						}
						
						if (_logValidation(nt_approved_days) && nt_approved_days != 0)
						{
							nt_approved_days_cost = parseFloat(nt_approved_days) * parseFloat(daily_rate);
							nt_approved_days_cost = parseFloat(nt_approved_days_cost);
							//nt_approved_days_cost = nt_approved_days_cost.toFixed(2); 
							
							nt_approved_days_cost = nt_approved_days_cost.toString();
							if(nt_approved_days_cost.indexOf(".")>0)
							{
								var temp_index = nt_approved_days_cost.indexOf(".")+3;
								nt_approved_days_cost = nt_approved_days_cost.substr(0,temp_index);
							}
							nt_approved_days_cost = parseFloat(nt_approved_days_cost);
							//nlapiLogExecution('debug','nt_approved_days:--',nt_approved_days);
							//nlapiLogExecution('debug','nt_approved_days_cost:-- '+Emp_name,nt_approved_days_cost);
						}
						
						if (_logValidation(nt_submitted_days) && nt_submitted_days != 0)
						{
							nt_submitted_days_cost = parseFloat(nt_submitted_days) * parseFloat(daily_rate);
							nt_submitted_days_cost = parseFloat(nt_submitted_days_cost);
							//nt_submitted_days_cost = nt_submitted_days_cost.toFixed(2);
							
							nt_submitted_days_cost = nt_submitted_days_cost.toString();
							if(nt_submitted_days_cost.indexOf(".")>0)
							{
								var temp_index = nt_submitted_days_cost.indexOf(".")+3;
								nt_submitted_days_cost = nt_submitted_days_cost.substr(0,temp_index);
							}
							nt_submitted_days_cost = parseFloat(nt_submitted_days_cost);
							//nlapiLogExecution('debug','nt_submitted_days:--',nt_submitted_days);
							//nlapiLogExecution('debug','nt_submitted_days_cost:-- '+Emp_name,nt_submitted_days_cost);
						}
						
						var temp_allocated_days = parseFloat(total_allocated_tym) / parseInt(8);
						var total_days_cal = parseFloat(no_of_leaves_taken) + parseFloat(no_of_holidays) + parseFloat(approved_days) + parseFloat(nt_approved_days) + parseFloat(nt_submitted_days);
						if(temp_allocated_days < total_days_cal)
						{
							no_of_holidays_cost = 0;
							approved_days_cost = parseFloat(monthly_rate_total_days);
							nt_approved_days_cost = 0;
							nt_submitted_days_cost = 0;
						}
						
						nlapiSubmitField('customrecord_salary_upload_file',rcrd_id_sal_upload_file,'custrecord_is_processed','T');
																
						je_nt_submittd.selectNewLineItem('line');
						je_nt_approved.selectNewLineItem('line');
						//je_approved.selectNewLineItem('line');
						
						//je_approved.setCurrentLineItemValue('line', 'custcol_srnumber', parseInt(t) + parseInt(1));
						
						je_nt_submittd.setCurrentLineItemValue('line', 'account', debit_account);
						je_nt_approved.setCurrentLineItemValue('line', 'account', debit_account);
						//je_approved.setCurrentLineItemValue('line', 'account', debit_account);
						
						/*if(_logValidation(no_of_leaves_cost) && no_of_leaves_cost !=0)
						{
							nlapiLogExecution('audit','leave cost:-- ',no_of_leaves_cost);
							total_credit_cost_approved.push(no_of_leaves_cost);
							/*if(total_credit_cost_approved == 0)
							{
								total_credit_cost_approved = parseFloat(no_of_leaves_cost);
								total_credit_cost_approved = total_credit_cost_approved.toFixed(2);
							}
							else
							{
								total_credit_cost_approved = parseFloat(total_credit_cost_approved) + parseFloat(no_of_leaves_cost);
								total_credit_cost_approved = total_credit_cost_approved.toFixed(2);
							}*/
							
							/*approved_entry_exist_flag = 1;
							je_approved.selectNewLineItem('line');
							je_approved.setCurrentLineItemValue('line', 'custcol_srnumber', parseInt(apprvd_sr_no));
							apprvd_sr_no++;
							je_approved.setCurrentLineItemValue('line', 'account', debit_account);
							je_approved.setCurrentLineItemValue('line', 'debit', no_of_leaves_cost);
							je_approved.setCurrentLineItemValue('line', 'department', practice);
							je_approved.setCurrentLineItemValue('line', 'custcol_project_name', project);
							je_approved.setCurrentLineItemValue('line', 'custcolprj_name', project);
							je_approved.setCurrentLineItemValue('line', 'custcol_sow_project', proj_id);
							je_approved.setCurrentLineItemValue('line', 'custcol_territory', proj_cust_territory);
							je_approved.setCurrentLineItemValue('line', 'custcol_employeenamecolumn', Emp_name);
							je_approved.setCurrentLineItemValue('line', 'memo', 'Leave / Approved');
							je_approved.commitLineItem('line');
							
						}
						
						if(_logValidation(no_of_holidays_cost) && no_of_holidays_cost !=0)
						{
							total_credit_cost_approved.push(no_of_holidays_cost);
							/*if(total_credit_cost_approved == 0)
							{
								total_credit_cost_approved = parseFloat(no_of_holidays_cost);
								total_credit_cost_approved = total_credit_cost_approved.toFixed(2);
							}
							else
							{
								total_credit_cost_approved = parseFloat(total_credit_cost_approved) + parseFloat(no_of_holidays_cost);
								total_credit_cost_approved = total_credit_cost_approved.toFixed(2);
							}*/
							/*	
							approved_entry_exist_flag = 1;
							je_approved.selectNewLineItem('line');
							je_approved.setCurrentLineItemValue('line', 'custcol_srnumber', parseInt(apprvd_sr_no));
							apprvd_sr_no++;
							je_approved.setCurrentLineItemValue('line', 'account', debit_account);
							je_approved.setCurrentLineItemValue('line', 'debit', no_of_holidays_cost);
							je_approved.setCurrentLineItemValue('line', 'department', practice);
							je_approved.setCurrentLineItemValue('line', 'custcol_project_name', project);
							je_approved.setCurrentLineItemValue('line', 'custcolprj_name', project);
							je_approved.setCurrentLineItemValue('line', 'custcol_sow_project', proj_id);
							je_approved.setCurrentLineItemValue('line', 'custcol_territory', proj_cust_territory);
							je_approved.setCurrentLineItemValue('line', 'custcol_employeenamecolumn', Emp_name);
							je_approved.setCurrentLineItemValue('line', 'memo', 'Holiday / Approved');
							je_approved.commitLineItem('line');
							
							
						}*/
						
						//if(_logValidation(approved_days_cost) && approved_days_cost != 0 && approved_days_cost != 0.0)
						if(_logValidation(approved_days_cost) || _logValidation(no_of_holidays_cost) || _logValidation(no_of_leaves_cost))
						{
							//nlapiLogExecution('audit','no_of_holidays_cost:-- '+no_of_holidays_cost,no_of_leaves_cost);
							var total_approved_cost_to_push = parseFloat(approved_days_cost) + parseFloat(no_of_holidays_cost) + parseFloat(no_of_leaves_cost);
							//total_approved_cost_to_push = total_approved_cost_to_push.toFixed(2);
							
							total_approved_cost_to_push = total_approved_cost_to_push.toString();
							if(total_approved_cost_to_push.indexOf(".")>0)
							{
								var temp_index = total_approved_cost_to_push.indexOf(".")+3;
								total_approved_cost_to_push = total_approved_cost_to_push.substr(0,temp_index);
							}
							total_approved_cost_to_push = parseFloat(total_approved_cost_to_push);
							total_credit_cost_approved.push(total_approved_cost_to_push);
							/*if(total_credit_cost_approved == 0)
							{
								total_credit_cost_approved = parseFloat(approved_days_cost);
								total_credit_cost_approved = total_credit_cost_approved.toFixed(2);
							}
							else
							{
								total_credit_cost_approved = parseFloat(total_credit_cost_approved) + parseFloat(approved_days_cost);
								total_credit_cost_approved = total_credit_cost_approved.toFixed(2);
							}*/
								
							approved_entry_exist_flag = 1;
							je_approved.selectNewLineItem('line');
							je_approved.setCurrentLineItemValue('line', 'custcol_srnumber', parseInt(apprvd_sr_no));
							apprvd_sr_no++;
							je_approved.setCurrentLineItemValue('line', 'account', debit_account);
							je_approved.setCurrentLineItemValue('line', 'debit', total_approved_cost_to_push);
							je_approved.setCurrentLineItemValue('line', 'department', practice);
							je_approved.setCurrentLineItemValue('line', 'class', vertical);
							je_approved.setCurrentLineItemValue('line', 'custcol_project_name', project);
							je_approved.setCurrentLineItemValue('line', 'custcolprj_name', project);
							je_approved.setCurrentLineItemValue('line', 'custcol_sow_project', proj_id);
							je_approved.setCurrentLineItemValue('line', 'custcol_territory', proj_cust_territory);
							je_approved.setCurrentLineItemValue('line', 'custcol_employeenamecolumn', Emp_name);
							je_approved.setCurrentLineItemValue('line', 'custcolcustcol_temp_customer', proj_cust_name);
							je_approved.setCurrentLineItemValue('line', 'memo', 'Contractor Cost '+month+' '+year+'/ Approved');
							je_approved.commitLineItem('line');
							
						}
						
						if(_logValidation(nt_approved_days_cost) && nt_approved_days_cost !=0)
						{
							if(total_credit_cost_nt_approved == 0)
							{
								total_credit_cost_nt_approved = parseFloat(nt_approved_days_cost);
								total_credit_cost_nt_approved = total_credit_cost_nt_approved.toFixed(2);
								
								/*total_credit_cost_nt_approved = total_credit_cost_nt_approved.toString();
								if(total_credit_cost_nt_approved.indexOf(".")>0)
								{
									var temp_index = total_credit_cost_nt_approved.indexOf(".")+3;
									total_credit_cost_nt_approved = total_credit_cost_nt_approved.substr(0,temp_index);
								}
								total_credit_cost_nt_approved = parseFloat(total_credit_cost_nt_approved);*/
							
							}
							else
							{
								total_credit_cost_nt_approved = parseFloat(total_credit_cost_nt_approved) + parseFloat(nt_approved_days_cost);
								total_credit_cost_nt_approved = total_credit_cost_nt_approved.toFixed(2);
								
								/*total_credit_cost_nt_approved = total_credit_cost_nt_approved.toString();
								if(total_credit_cost_nt_approved.indexOf(".")>0)
								{
									var temp_index = total_credit_cost_nt_approved.indexOf(".")+3;
									total_credit_cost_nt_approved = total_credit_cost_nt_approved.substr(0,temp_index);
								}
								total_credit_cost_nt_approved = parseFloat(total_credit_cost_nt_approved);*/
							}
								
							nt_approved_entry_exist_flag = 1;
							je_nt_approved.setCurrentLineItemValue('line', 'custcol_srnumber', parseInt(nt_apprvd_sr_no));
							nt_apprvd_sr_no++;
							je_nt_approved.setCurrentLineItemValue('line', 'debit', nt_approved_days_cost);
							je_nt_approved.setCurrentLineItemValue('line', 'department', practice);
							je_nt_approved.setCurrentLineItemValue('line', 'class', vertical);
							je_nt_approved.setCurrentLineItemValue('line', 'custcol_project_name', project);
							je_nt_approved.setCurrentLineItemValue('line', 'custcolprj_name', project);
							je_nt_approved.setCurrentLineItemValue('line', 'custcol_sow_project', proj_id);
							je_nt_approved.setCurrentLineItemValue('line', 'custcol_territory', proj_cust_territory);
							je_nt_approved.setCurrentLineItemValue('line', 'custcol_employeenamecolumn', Emp_name);
							je_nt_approved.setCurrentLineItemValue('line', 'custcolcustcol_temp_customer', proj_cust_name);
							je_nt_approved.setCurrentLineItemValue('line', 'memo', 'Contractor Cost '+month+' '+year+'/ Not Approved');
							je_nt_approved.commitLineItem('line');
						}
						
						if(_logValidation(nt_submitted_days_cost) && nt_submitted_days_cost !=0)
						{
							if(total_credit_cost_nt_submitted == 0)
							{
								total_credit_cost_nt_submitted = parseFloat(nt_submitted_days_cost);
								total_credit_cost_nt_submitted = total_credit_cost_nt_submitted.toFixed(2);
								
								/*total_credit_cost_nt_submitted = total_credit_cost_nt_submitted.toString();
								if(total_credit_cost_nt_submitted.indexOf(".")>0)
								{
									var temp_index = total_credit_cost_nt_submitted.indexOf(".")+3;
									total_credit_cost_nt_submitted = total_credit_cost_nt_submitted.substr(0,temp_index);
								}
								total_credit_cost_nt_submitted = parseFloat(total_credit_cost_nt_submitted);*/
							}
							else
							{
								total_credit_cost_nt_submitted = parseFloat(total_credit_cost_nt_submitted) + parseFloat(nt_submitted_days_cost);
								total_credit_cost_nt_submitted = total_credit_cost_nt_submitted.toFixed(2);
								
								/*total_credit_cost_nt_submitted = total_credit_cost_nt_submitted.toString();
								if(total_credit_cost_nt_submitted.indexOf(".")>0)
								{
									var temp_index = total_credit_cost_nt_submitted.indexOf(".")+3;
									total_credit_cost_nt_submitted = total_credit_cost_nt_submitted.substr(0,temp_index);
								}
								total_credit_cost_nt_submitted = parseFloat(total_credit_cost_nt_submitted);*/
							}	
							
							nt_submitted_entry_exist_flag = 1;
							je_nt_submittd.setCurrentLineItemValue('line', 'custcol_srnumber', parseInt(nt_submitted_sr_no));
							nt_submitted_sr_no++;
							je_nt_submittd.setCurrentLineItemValue('line', 'debit', nt_submitted_days_cost);
							je_nt_submittd.setCurrentLineItemValue('line', 'department', practice);
							je_nt_submittd.setCurrentLineItemValue('line', 'class', vertical);
							je_nt_submittd.setCurrentLineItemValue('line', 'custcol_project_name', project);
							je_nt_submittd.setCurrentLineItemValue('line', 'custcolprj_name', project);
							je_nt_submittd.setCurrentLineItemValue('line', 'custcol_sow_project', proj_id);
							je_nt_submittd.setCurrentLineItemValue('line', 'custcol_territory', proj_cust_territory);
							je_nt_submittd.setCurrentLineItemValue('line', 'custcol_employeenamecolumn', Emp_name);
							je_nt_submittd.setCurrentLineItemValue('line', 'custcolcustcol_temp_customer', proj_cust_name);
							je_nt_submittd.setCurrentLineItemValue('line', 'memo', 'Contractor Cost '+month+' '+year+'/ Not Submitted');
							je_nt_submittd.commitLineItem('line');
						}
						
						var amounts_arr = new Array();
						amounts_arr[0] = total_approved_cost_to_push;
						amounts_arr[1] = nt_approved_days_cost;
						amounts_arr[2] = nt_submitted_days_cost;
						
						var amount_fields = new Array();
						amount_fields[0] = 'custrecord_approved_cost';
						amount_fields[1] = 'custrecord_nt_approved_cost';
						amount_fields[2] = 'custrecord_nt_submitted_cost';
						
						nlapiSubmitField('customrecord_salary_upload_file',rcrd_id_sal_upload_file,amount_fields,amounts_arr);
					}
				}
				
				var i_usage_end = i_context.getRemainingUsage();
				if(i_usage_end < 1000)
				{
					nlapiYieldScript();
				}
				
				/*if (i_usage_end < 600) //
				{
					var je_created_arr = new Array();
					if(total_cost_past_mnth != 0)
					{
						past_mnth_je.selectNewLineItem('line');
						past_mnth_je.setCurrentLineItemValue('line', 'custcol_srnumber', parseInt(past_mnth_sr_no));
						past_mnth_sr_no++;
						past_mnth_je.setCurrentLineItemValue('line', 'account', credit_account);
						past_mnth_je.setCurrentLineItemValue('line', 'credit', total_cost_past_mnth);
						past_mnth_je.commitLineItem('line');
						var je_id_past_mnth_data = nlapiSubmitRecord(past_mnth_je,true,true);
						nlapiLogExecution('audit','je_id_past_mnth_data:- ',je_id_past_mnth_data);
						je_created_arr.push(je_id_past_mnth_data);
					}
			
					//if(approved_entry_exist_flag == 1)
					//{
					var credit_amnt_apprvd = 0;
					for(var r=0; r<total_credit_cost_approved.length; r++)
					{
						credit_amnt_apprvd = Number(credit_amnt_apprvd) + Number(total_credit_cost_approved[r]);
					}
					credit_amnt_apprvd = credit_amnt_apprvd.toFixed(2);
						nlapiLogExecution('debug','total_credit_cost_approved:- ',credit_amnt_apprvd);
						je_approved.selectNewLineItem('line');
						je_approved.setCurrentLineItemValue('line', 'custcol_srnumber', parseInt(apprvd_sr_no));
						je_approved.setCurrentLineItemValue('line', 'account', credit_account);
						je_approved.setCurrentLineItemValue('line', 'credit', credit_amnt_apprvd);
						je_approved.commitLineItem('line');
												 
						var je_id_approved = nlapiSubmitRecord(je_approved,true,true);
						nlapiLogExecution('audit','je_id_approved:- ',je_id_approved);
						je_created_arr.push(je_id_approved);
					//}
							
					//if(nt_approved_entry_exist_flag == 1)
					//{
						nlapiLogExecution('debug','total_credit_cost_nt_approved:- ',total_credit_cost_nt_approved);
						je_nt_approved.selectNewLineItem('line');
						je_nt_approved.setCurrentLineItemValue('line', 'custcol_srnumber', parseInt(nt_apprvd_sr_no));
						je_nt_approved.setCurrentLineItemValue('line', 'account', credit_account);
						je_nt_approved.setCurrentLineItemValue('line', 'credit', total_credit_cost_nt_approved);
						je_nt_approved.commitLineItem('line');
				
						var je_id_nt_approved = nlapiSubmitRecord(je_nt_approved,true,true);
						nlapiLogExecution('audit','je_id_nt_approved:- ',je_id_nt_approved);
						je_created_arr.push(je_id_nt_approved);
					//}
					
					//if(nt_submitted_entry_exist_flag == 1)
					//{
						nlapiLogExecution('debug','total_credit_cost_nt_submitted:- ',total_credit_cost_nt_submitted);
						je_nt_submittd.selectNewLineItem('line');
						je_nt_submittd.setCurrentLineItemValue('line', 'custcol_srnumber', parseInt(nt_submitted_sr_no));
						je_nt_submittd.setCurrentLineItemValue('line', 'account', credit_account);
						je_nt_submittd.setCurrentLineItemValue('line', 'credit', total_credit_cost_nt_submitted);
						je_nt_submittd.commitLineItem('line');
						var je_id_nt_submitted = nlapiSubmitRecord(je_nt_submittd,true,true);
						nlapiLogExecution('audit','je_id_nt_submitted:- ',je_id_nt_submitted);
						je_created_arr.push(je_id_nt_submitted);
					//}
					
					nlapiSubmitField('customrecord_salary_upload_process_btpl',rcrd_id,'custrecord_journal_entries_created_btpl',je_created_arr);	
					//i_file_status = 'In Progress';
					schedule_script_after_usage_exceeded(i, rcrd_id, month, year, debit_account, credit_account, vendor_provision_subsidiary);
				}*/
				
			}
			
			var je_created_arr = new Array();
			var vendr_pro_rcrd = nlapiLoadRecord('customrecord_salary_upload_process_btpl',rcrd_id);
			var je_already_created_arr = vendr_pro_rcrd.getFieldValues('custrecord_journal_entries_created_btpl');
			if(_logValidation(je_already_created_arr))
			{
				for(var q=0; q<je_already_created_arr.length; q++)
				{
					je_created_arr.push(je_already_created_arr[q]);
				}
			}
			
			if(total_cost_past_mnth != 0)
			{
				nlapiLogExecution('debug','total_cost_past_mnth:- ',total_cost_past_mnth);
				past_mnth_je.selectNewLineItem('line');
				past_mnth_je.setCurrentLineItemValue('line', 'custcol_srnumber', parseInt(i) + parseInt(1));
				past_mnth_je.setCurrentLineItemValue('line', 'account', credit_account);
				past_mnth_je.setCurrentLineItemValue('line', 'credit', total_cost_past_mnth);
				past_mnth_je.commitLineItem('line');
				var je_id_past_mnth_data = nlapiSubmitRecord(past_mnth_je,true,true);
				nlapiLogExecution('audit','je_id_past_mnth_data:- ',je_id_past_mnth_data);
				je_created_arr.push(je_id_past_mnth_data);
			}
			
			try
			{
				var credit_amnt_apprvd = 0;
				for(var r=0; r<total_credit_cost_approved.length; r++)
				{
					credit_amnt_apprvd = Number(credit_amnt_apprvd) + Number(total_credit_cost_approved[r]);
				}
				credit_amnt_apprvd = credit_amnt_apprvd.toFixed(2);
				
				/*credit_amnt_apprvd = credit_amnt_apprvd.toString();
				if(credit_amnt_apprvd.indexOf(".")>0)
				{
					var temp_index = credit_amnt_apprvd.indexOf(".")+3;
					credit_amnt_apprvd = credit_amnt_apprvd.substr(0,temp_index);
				}
				credit_amnt_apprvd = parseFloat(credit_amnt_apprvd);*/
								
				nlapiLogExecution('debug','total_credit_cost_approved:- ',credit_amnt_apprvd);
				je_approved.selectNewLineItem('line');
				je_approved.setCurrentLineItemValue('line', 'custcol_srnumber', parseInt(apprvd_sr_no));
				je_approved.setCurrentLineItemValue('line', 'account', credit_account);
				je_approved.setCurrentLineItemValue('line', 'credit', credit_amnt_apprvd);
				je_approved.commitLineItem('line');
										 
				var je_id_approved = nlapiSubmitRecord(je_approved,true,true);
				nlapiLogExecution('audit','je_id_approved:- ',je_id_approved);
				je_created_arr.push(je_id_approved);
			}
			catch(err)
			{
				var je_notes = 'Error occurred while JE creation.';
				nlapiSubmitField('customrecord_salary_upload_process_btpl',rcrd_id,'custrecord_status_btpl',7);				
				nlapiSubmitField('customrecord_salary_upload_process_btpl',rcrd_id,'custrecord_notes_btpl',je_notes);
				
				var o_salary_pr_errorOBJ = nlapiCreateRecord('customrecord_error_logs_vendor_pro');
				
				o_salary_pr_errorOBJ.setFieldValue('custrecord_error_details', err);
				o_salary_pr_errorOBJ.setFieldValue('custrecord_employee_id_ven_pro', Emp_name);
				//o_salary_pr_errorOBJ.setFieldValue('custrecord_subsidiary_ven_pro', parseInt(vendor_provision_subsidiary));
				o_salary_pr_errorOBJ.setFieldValue('custrecord_month_ven_pro', month);
				o_salary_pr_errorOBJ.setFieldValue('custrecord_year_ven_pro', year);
				o_salary_pr_errorOBJ.setFieldValue('custrecord_vendor_pro_parent', rcrd_id);
				
				var i_submit_err_ID = nlapiSubmitRecord(o_salary_pr_errorOBJ, true, true);
			}
					
			try
			{
				nlapiLogExecution('debug','total_credit_cost_nt_approved:- ',total_credit_cost_nt_approved);
				je_nt_approved.selectNewLineItem('line');
				je_nt_approved.setCurrentLineItemValue('line', 'custcol_srnumber', parseInt(nt_apprvd_sr_no));
				je_nt_approved.setCurrentLineItemValue('line', 'account', credit_account);
				je_nt_approved.setCurrentLineItemValue('line', 'credit', total_credit_cost_nt_approved);
				je_nt_approved.commitLineItem('line');
				
				var je_id_nt_approved = nlapiSubmitRecord(je_nt_approved,true,true);
				nlapiLogExecution('audit','je_id_nt_approved:- ',je_id_nt_approved);
				je_created_arr.push(je_id_nt_approved);
			}
			catch(err)
			{
				var je_notes = 'Error occurred while JE creation.';
				nlapiSubmitField('customrecord_salary_upload_process_btpl',rcrd_id,'custrecord_status_btpl',7);				
				nlapiSubmitField('customrecord_salary_upload_process_btpl',rcrd_id,'custrecord_notes_btpl',je_notes);
				
				var o_salary_pr_errorOBJ = nlapiCreateRecord('customrecord_error_logs_vendor_pro');
				
				o_salary_pr_errorOBJ.setFieldValue('custrecord_error_details', err);
				o_salary_pr_errorOBJ.setFieldValue('custrecord_employee_id_ven_pro', Emp_name);
				//o_salary_pr_errorOBJ.setFieldValue('custrecord_subsidiary_ven_pro', parseInt(vendor_provision_subsidiary));
				o_salary_pr_errorOBJ.setFieldValue('custrecord_month_ven_pro', month);
				o_salary_pr_errorOBJ.setFieldValue('custrecord_year_ven_pro', year);
				o_salary_pr_errorOBJ.setFieldValue('custrecord_vendor_pro_parent', rcrd_id);
				
				var i_submit_err_ID = nlapiSubmitRecord(o_salary_pr_errorOBJ, true, true);
			}
			
			try
			{
				nlapiLogExecution('debug','total_credit_cost_nt_submitted:- ',total_credit_cost_nt_submitted);
				je_nt_submittd.selectNewLineItem('line');
				je_nt_submittd.setCurrentLineItemValue('line', 'custcol_srnumber', parseInt(nt_submitted_sr_no));
				je_nt_submittd.setCurrentLineItemValue('line', 'account', credit_account);
				je_nt_submittd.setCurrentLineItemValue('line', 'credit', total_credit_cost_nt_submitted);
				je_nt_submittd.commitLineItem('line');
				var je_id_nt_submitted = nlapiSubmitRecord(je_nt_submittd,true,true);
				nlapiLogExecution('audit','je_id_nt_submitted:- ',je_id_nt_submitted);
				je_created_arr.push(je_id_nt_submitted);
			}
			catch(err)
			{
				var je_notes = 'Error occurred while JE creation.';
				nlapiSubmitField('customrecord_salary_upload_process_btpl',rcrd_id,'custrecord_status_btpl',7);				
				nlapiSubmitField('customrecord_salary_upload_process_btpl',rcrd_id,'custrecord_notes_btpl',je_notes);
				
				var o_salary_pr_errorOBJ = nlapiCreateRecord('customrecord_error_logs_vendor_pro');
				
				o_salary_pr_errorOBJ.setFieldValue('custrecord_error_details', err);
				o_salary_pr_errorOBJ.setFieldValue('custrecord_employee_id_ven_pro', Emp_name);
				//o_salary_pr_errorOBJ.setFieldValue('custrecord_subsidiary_ven_pro', parseInt(vendor_provision_subsidiary));
				o_salary_pr_errorOBJ.setFieldValue('custrecord_month_ven_pro', month);
				o_salary_pr_errorOBJ.setFieldValue('custrecord_year_ven_pro', year);
				o_salary_pr_errorOBJ.setFieldValue('custrecord_vendor_pro_parent', rcrd_id);
				
				var i_submit_err_ID = nlapiSubmitRecord(o_salary_pr_errorOBJ, true, true);
			}
			
			var je_notes = 'JE created successfully.';
			nlapiSubmitField('customrecord_salary_upload_process_btpl',rcrd_id,'custrecord_status_btpl',5);				
			nlapiSubmitField('customrecord_salary_upload_process_btpl',rcrd_id,'custrecord_notes_btpl',je_notes);
			nlapiSubmitField('customrecord_salary_upload_process_btpl',rcrd_id,'custrecord_journal_entries_created_btpl',je_created_arr);
		}
		
	}
	catch(err)
	{
		var je_notes = 'Error occurred while JE creation.';
		nlapiSubmitField('customrecord_salary_upload_process_btpl',rcrd_id,'custrecord_status_btpl',7);				
		nlapiSubmitField('customrecord_salary_upload_process_btpl',rcrd_id,'custrecord_notes_btpl',je_notes);
		
		var o_salary_pr_errorOBJ = nlapiCreateRecord('customrecord_error_logs_vendor_pro');
		
		o_salary_pr_errorOBJ.setFieldValue('custrecord_error_details', err);
		o_salary_pr_errorOBJ.setFieldValue('custrecord_employee_id_ven_pro', Emp_name);
		//o_salary_pr_errorOBJ.setFieldValue('custrecord_subsidiary_ven_pro', vendor_provision_subsidiary);
		o_salary_pr_errorOBJ.setFieldValue('custrecord_month_ven_pro', month);
		o_salary_pr_errorOBJ.setFieldValue('custrecord_year_ven_pro', year);
		o_salary_pr_errorOBJ.setFieldValue('custrecord_vendor_pro_parent', rcrd_id);
		
		var i_submit_err_ID = nlapiSubmitRecord(o_salary_pr_errorOBJ, true, true);
		
		nlapiLogExecution('ERROR','ERROR MESSAGE:-- ',err);
	}
}

function schedule_script_after_usage_exceeded(i, rcrd_id, month, year, debit_account, credit_account, vendor_provision_subsidiary)
{
	////Define all parameters to schedule the script for voucher generation.
	 nlapiLogExecution('audit','Before Scheduling','i -->'+ i);
	 i = i++;
	 var params=new Array();
	 params['status']='scheduled';
 	 params['runasadmin']='T';
	 params['custscript_i_counter']=i;
	 params['custscript_rcrd_id']=rcrd_id;
	 params['custscript_vendor_provision_month']=month;
	 params['custscript_vendor_provision_year']=year;
	 params['custscript_debit_account']=debit_account;
	 params['custscript_credit_account']=credit_account;
	 params['custscript_ven_pro_subsidiary_id']=vendor_provision_subsidiary;
		 
	 var startDate = new Date();
 	 params['startdate']=startDate.toUTCString();
	
	 var status=nlapiScheduleScript(nlapiGetContext().getScriptId(), nlapiGetContext().getDeploymentId(),params);
	 //nlapiLogExecution('DEBUG','After Scheduling','Script Scheduled Status -->'+ status);
	 
	 ////If script is scheduled then successfuly then check for if status=queued
	 if (status == 'QUEUED') 
 	 {
		//nlapiLogExecution('DEBUG', ' SCHEDULED', ' Script Is Re - Scheduled for -->'+i);
 	 }
}//fun close


function daysInMonth(month,year) {
	month = month.trim();
	
	if(month == 'Jan' || month == 'January' || month == 'JAN' || month == 'JANUARY')
	{	  	
		month = 1;		
	}	
	else if(month == 'Feb' || month == 'February' || month == 'FEB' || month == 'FEBRUARY')
	{
		month = 2;	
	}		
	else if(month == 'Mar' || month == 'March' || month == 'MAR' || month == 'MARCH')
	{	  
		month = 3;
	}	
	else if(month == 'Apr' || month == 'April' || month == 'APR' || month == 'APRIL')
	{
		month = 4;
	}	
	else if(month == 'May' || month == 'MAY')
	{	  
		month = 5;
	}	  
	else if(month == 'June' || month == 'Jun' || month == 'JUN' || month == 'JUNE')
	{	 
		month = 6;
	}	
	else if(month == 'Jul' || month == 'July' || month == 'JUL' || month == 'JULY')
	{	  
		month = 7;
	}	
	else if(month == 'August' || month == 'Aug' || month == 'AUG' || month == 'AUGUST')
	{	  
		month = 8;
	}  
	else if(month == 'Sep' || month == 'September' || month == 'SEP' || month == 'SEPTEMBER')
	{  	
		month = 9;
	}	
	else if(month == 'Oct' || month == 'October' || month == 'OCT' || month == 'OCTOBER')
	{	 	
		month = 10;
	}	
	else if(month == 'Nov' || month == 'November' || month == 'NOV' || month == 'NOVEMBER')
	{	  
		month = 11;
	}	
	else if(month == 'Dec' || month == 'December' || month == 'DEC' || month == 'DECEMBER')
	{	 
		month = 12;
	}
	
	year = Number(year).toFixed(0);
    return new Date(year, month, 0).getDate();
}

function get_todays_date()
{
	var today;
	
	var date1 = new Date();
	
	var day = date1.getDate();
	
	var month = date1.getMonth()+1;
	
	var year_sd =date1.getYear();
	
	var year=date1.getFullYear();
	
	var date_format = checkDateFormat();
	
	if (date_format == 'YYYY-MM-DD')
	{
		today = year + '-' + month + '-' + day;
	}
	if (date_format == 'DD/MM/YYYY')
	{
		today = day + '/' + month + '/' + year;
	}
	if (date_format == 'MM/DD/YYYY')
	{
		today = month + '/' + day + '/' + year;
	}
	return today;
}


function get_current_month_start_date(i_month,i_year)
{
   var d_start_date = '';
   var d_end_date = '';
	
   var date_format = checkDateFormat();	 
	
 if (_logValidation(i_month) && _logValidation(i_year)) 
 {
 	  if(i_month == 'Jan' || i_month == 'January' || i_month == 'JAN' || i_month == 'JANUARY')
	  {	  	
		 if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 1 + '-' + 1;		
			d_end_date = i_year + '-' + 1 + '-' + 31;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 1 + '/' + i_year;
			d_end_date = 31 + '/' + 1 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 1 + '/' + 1 + '/' + i_year;
			d_end_date = 1 + '/' + 31 + '/' + i_year;
		 }	 
				
	  }	
	  else if(i_month == 'Feb' || i_month == 'February' || i_month == 'FEB' || i_month == 'FEBRUARY')
	  {
	  	if((i_year%2) == 0)
		{
			i_day = 29;
		}
		else
		{
			i_day = 28;
		}
			
		 if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 2 + '-' + 1;		
			d_end_date = i_year + '-' + 2 + '-' + i_day;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 2 + '/' + i_year;
			d_end_date = i_day + '/' + 2 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 2 + '/' + 1 + '/' + i_year;
			d_end_date = 2 + '/' + i_day + '/' + i_year;
		 }	 
			
	  }		
	  else if(i_month == 'Mar' || i_month == 'March' || i_month == 'MAR' || i_month == 'MARCH')
	  {	  
	     if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 3 + '-' + 1;		
			d_end_date = i_year + '-' + 3 + '-' + 31;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 3 + '/' + i_year;
			d_end_date = 31 + '/' + 3 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 3 + '/' + 1 + '/' + i_year;
			d_end_date = 3 + '/' + 31 + '/' + i_year;
		 }	 
	
	  }	
	  else if(i_month == 'Apr' || i_month == 'April' || i_month == 'APR' || i_month == 'APRIL')
	  {
	  	if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 4 + '-' + 1;		
			d_end_date = i_year + '-' + 4 + '-' + 30;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 4 + '/' + i_year;
			d_end_date = 30 + '/' + 4 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 4 + '/' + 1 + '/' + i_year;
			d_end_date = 4 + '/' + 30 + '/' + i_year;
	     }	 
	 
	
	  }	
	  else if(i_month == 'May' || i_month == 'MAY')
	  {	  
		 if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 5 + '-' + 1;		
			d_end_date = i_year + '-' + 5 + '-' + 31;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 5 + '/' + i_year;
			d_end_date = 31 + '/' + 5 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 5 + '/' + 1 + '/' + i_year;
			d_end_date = 5 + '/' + 31 + '/' + i_year;
	     }	 
	
	  }	  
	  else if(i_month == 'June' || i_month == 'Jun' || i_month == 'JUN' || i_month == 'JUNE')
	  {	 
		 if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 6 + '-' + 1;		
			d_end_date = i_year + '-' + 6 + '-' + 30;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 6 + '/' + i_year;
			d_end_date = 30 + '/' + 6 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 6 + '/' + 1 + '/' + i_year;
			d_end_date = 6 + '/' + 30 + '/' + i_year;
	     }	 
			
	  }	
	  else if(i_month == 'Jul' || i_month == 'July' || i_month == 'JUL' || i_month == 'JULY')
	  {	  
		 if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 7 + '-' + 1;		
			d_end_date = i_year + '-' + 7 + '-' + 31;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 7 + '/' + i_year;
			d_end_date = 31 + '/' + 7 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 7 + '/' + 1 + '/' + i_year;
			d_end_date = 7 + '/' + 31 + '/' + i_year;
	     }	 
		
	  }	
	  else if(i_month == 'August' || i_month == 'Aug' || i_month == 'AUG' || i_month == 'AUGUST')
	  {	  
		 if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 8 + '-' + 1;		
			d_end_date = i_year + '-' + 8 + '-' + 31;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 8 + '/' + i_year;
			d_end_date = 31 + '/' + 8 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 8 + '/' + 1 + '/' + i_year;
			d_end_date = 8 + '/' + 31 + '/' + i_year;
	     }	 
		
	  }  
	  else if(i_month == 'Sep' || i_month == 'September' || i_month == 'SEP' || i_month == 'SEPTEMBER')
	  {  	
	     if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 9 + '-' + 1;		
			d_end_date = i_year + '-' + 9 + '-' + 30;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 9 + '/' + i_year;
			d_end_date = 30 + '/' + 9 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 9 + '/' + 1 + '/' + i_year;
			d_end_date = 9 + '/' + 30 + '/' + i_year;
	     }	 
	
	  }	
	  else if(i_month == 'Oct' || i_month == 'October' || i_month == 'OCT' || i_month == 'OCTOBER')
	  {	 	
		if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 10 + '-' + 1;		
			d_end_date = i_year + '-' + 10 + '-' + 31;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 10 + '/' + i_year;
			d_end_date = 31 + '/' + 10 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 10 + '/' + 1 + '/' + i_year;
			d_end_date = 10 + '/' + 31 + '/' + i_year;
	     }		
	  }	
	  else if(i_month == 'Nov' || i_month == 'November' || i_month == 'NOV' || i_month == 'NOVEMBER')
	  {	  
		if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 11 + '-' + 1;		
			d_end_date = i_year + '-' + 11 + '-' + 30;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 11 + '/' + i_year;
			d_end_date = 30 + '/' + 11 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 11 + '/' + 1 + '/' + i_year;
			d_end_date = 11 + '/' + 30 + '/' + i_year;
	     }	 
			
	  }	
	  else if(i_month == 'Dec' || i_month == 'December' || i_month == 'DEC' || i_month == 'DECEMBER')
	  {	 
		if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 12 + '-' + 1;		
			d_end_date = i_year + '-' + 12 + '-' + 31;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 12 + '/' + i_year;
			d_end_date = 31 + '/' + 12 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 12 + '/' + 1 + '/' + i_year;
			d_end_date = 12 + '/' + 31 + '/' + i_year;
	     }	 
			
	  }	
 }//Month & Year
 return d_start_date;
}


function get_current_month_end_date(i_month,i_year)
{
	var d_start_date = '';
	var d_end_date = '';
	
	var date_format = checkDateFormat();
	 	
 if (_logValidation(i_month) && _logValidation(i_year)) 
 {
 	  if(i_month == 'Jan' || i_month == 'January' || i_month == 'JAN' || i_month == 'JANUARY')
	  {	  	
		 if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 1 + '-' + 1;		
			d_end_date = i_year + '-' + 1 + '-' + 31;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 1 + '/' + i_year;
			d_end_date = 31 + '/' + 1 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 1 + '/' + 1 + '/' + i_year;
			d_end_date = 1 + '/' + 31 + '/' + i_year;
		 }	 
				
	  }	
	  else if(i_month == 'Feb' || i_month == 'February' || i_month == 'FEB' || i_month == 'FEBRUARY')
	  {
	  	if((i_year%2) == 0)
		{
			i_day = 29;
		}
		else
		{
			i_day = 28;
		}
			
		 if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 2 + '-' + 1;		
			d_end_date = i_year + '-' + 2 + '-' + i_day;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 2 + '/' + i_year;
			d_end_date = i_day + '/' + 2 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 2 + '/' + 1 + '/' + i_year;
			d_end_date = 2 + '/' + i_day + '/' + i_year;
		 }	 
			
	  }		
	  else if(i_month == 'Mar' || i_month == 'March' || i_month == 'MAR' || i_month == 'MARCH')
	  {	  
	     if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 3 + '-' + 1;		
			d_end_date = i_year + '-' + 3 + '-' + 31;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 3 + '/' + i_year;
			d_end_date = 31 + '/' + 3 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 3 + '/' + 1 + '/' + i_year;
			d_end_date = 3 + '/' + 31 + '/' + i_year;
		 }	 
	
	  }	
	  else if(i_month == 'Apr' || i_month == 'April' || i_month == 'APR' || i_month == 'APRIL')
	  {
	  	if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 4 + '-' + 1;		
			d_end_date = i_year + '-' + 4 + '-' + 30;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 4 + '/' + i_year;
			d_end_date = 30 + '/' + 4 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 4 + '/' + 1 + '/' + i_year;
			d_end_date = 4 + '/' + 30 + '/' + i_year;
	     }	 
	 
	
	  }	
	  else if(i_month == 'May' || i_month == 'MAY')
	  {	  
		 if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 5 + '-' + 1;		
			d_end_date = i_year + '-' + 5 + '-' + 31;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 5 + '/' + i_year;
			d_end_date = 31 + '/' + 5 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 5 + '/' + 1 + '/' + i_year;
			d_end_date = 5 + '/' + 31 + '/' + i_year;
	     }	 
	
	  }	  
	  else if(i_month == 'June' || i_month == 'Jun' || i_month == 'JUN' || i_month == 'JUNE')
	  {	 
		 if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 6 + '-' + 1;		
			d_end_date = i_year + '-' + 6 + '-' + 30;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 6 + '/' + i_year;
			d_end_date = 30 + '/' + 6 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 6 + '/' + 1 + '/' + i_year;
			d_end_date = 6 + '/' + 30 + '/' + i_year;
	     }	 
			
	  }	
	  else if(i_month == 'Jul' || i_month == 'July' || i_month == 'JUL' || i_month == 'JULY')
	  {	  
		 if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 7 + '-' + 1;		
			d_end_date = i_year + '-' + 7 + '-' + 31;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 7 + '/' + i_year;
			d_end_date = 31 + '/' + 7 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 7 + '/' + 1 + '/' + i_year;
			d_end_date = 7 + '/' + 31 + '/' + i_year;
	     }	 
		
	  }	
	  else if(i_month == 'August' || i_month == 'Aug' || i_month == 'AUG' || i_month == 'AUGUST')
	  {	  
		 if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 8 + '-' + 1;		
			d_end_date = i_year + '-' + 8 + '-' + 31;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 8 + '/' + i_year;
			d_end_date = 31 + '/' + 8 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 8 + '/' + 1 + '/' + i_year;
			d_end_date = 8 + '/' + 31 + '/' + i_year;
	     }	 
		
	  }  
	  else if(i_month == 'Sep' || i_month == 'September' || i_month == 'SEP' || i_month == 'SEPTEMBER')
	  {  	
	     if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 9 + '-' + 1;		
			d_end_date = i_year + '-' + 9 + '-' + 30;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 9 + '/' + i_year;
			d_end_date = 30 + '/' + 9 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 9 + '/' + 1 + '/' + i_year;
			d_end_date = 9 + '/' + 30 + '/' + i_year;
	     }	 
	
	  }	
	  else if(i_month == 'Oct' || i_month == 'October' || i_month == 'OCT' || i_month == 'OCTOBER')
	  {	 	
		if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 10 + '-' + 1;		
			d_end_date = i_year + '-' + 10 + '-' + 31;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 10 + '/' + i_year;
			d_end_date = 31 + '/' + 10 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 10 + '/' + 1 + '/' + i_year;
			d_end_date = 10 + '/' + 31 + '/' + i_year;
	     }	 
	
		
	  }	
	  else if(i_month == 'Nov' || i_month == 'November' || i_month == 'NOV' || i_month == 'NOVEMBER')
	  {	  
		if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 11 + '-' + 1;		
			d_end_date = i_year + '-' + 11 + '-' + 30;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 11 + '/' + i_year;
			d_end_date = 30 + '/' + 11 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 11 + '/' + 1 + '/' + i_year;
			d_end_date = 11 + '/' + 30 + '/' + i_year;
	     }	 
			
	  }	
	  else if(i_month == 'Dec' || i_month == 'December' || i_month == 'DEC' || i_month == 'DECEMBER')
	  {	 
		if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 12 + '-' + 1;		
			d_end_date = i_year + '-' + 12 + '-' + 31;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 12 + '/' + i_year;
			d_end_date = 31 + '/' + 12 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 12 + '/' + 1 + '/' + i_year;
			d_end_date = 12 + '/' + 31 + '/' + i_year;
	     }	 
			
	  }	
 }//Month & Year
 return d_end_date;
}

function checkDateFormat()
{
    var context = nlapiGetContext();

	var dateFormatPref = context.getPreference('dateformat');

    return dateFormatPref;
}

function _logValidation(value) 
{
 if(value!='null' && value != null && value != null && value != '' && value != undefined && value != undefined && value != 'undefined' && value!= 'undefined'&& value!= 'NaN' && value != NaN) 
 {
  return true;
 }
 else 
 { 
  return false;
 }
}

function getWeekend(startDate, endDate) //
{
	var i_no_of_sat = 0;
	var i_no_of_sun = 0;
	
	//nlapiLogExecution('DEBUG', 'Check Date', 'startDate : ' + startDate);
	//nlapiLogExecution('DEBUG', 'Check Date', 'endDate : ' + endDate);
	
	var date_format = checkDateFormat();
	
	var startDate_1 = startDate
	var endDate_1 = endDate
	
	startDate = nlapiStringToDate(startDate);
	endDate = nlapiStringToDate(endDate);
	
	//nlapiLogExecution('DEBUG', 'Check Date', '2 startDate : ' + startDate);
	//nlapiLogExecution('DEBUG', 'Check Date', '2 endDate : ' + endDate);
	
	var i_count_day = startDate.getDate();
	
	var i_count_last_day = endDate.getDate();
	
	i_month = startDate.getMonth() + 1;
	
	i_year = startDate.getFullYear();
	
	//nlapiLogExecution('DEBUG', 'Check Date', '2 startDate_1 : ' + startDate_1);
	//nlapiLogExecution('DEBUG', 'Check Date', '2 endDate_1 : ' + endDate_1);
	
	var d_f = new Date();
	var getTot = getDatediffIndays(startDate_1, endDate_1); //Get total days in a month
	var sat = new Array(); //Declaring array for inserting Saturdays
	var sun = new Array(); //Declaring array for inserting Sundays
	
	for (var i = i_count_day; i <= i_count_last_day; i++) //
	{
		//looping through days in month
		if (date_format == 'YYYY-MM-DD') //
		{
			var newDate = i_year + '-' + i_month + '-' + i;
		}
		if (date_format == 'DD/MM/YYYY') //
		{
			var newDate = i + '/' + i_month + '/' + i_year;
		}
		if (date_format == 'MM/DD/YYYY') //
		{
			var newDate = i_month + '/' + i + '/' + i_year;
		}
		
		newDate = nlapiStringToDate(newDate);
		
		if (newDate.getDay() == 0) //
		{ //if Sunday
			sat.push(i);
			i_no_of_sat++;
		}
		if (newDate.getDay() == 6) //
		{ //if Saturday
			sun.push(i);
			i_no_of_sun++;
		}
	}
	
	var i_total_days_sat_sun = parseInt(i_no_of_sat) + parseInt(i_no_of_sun);
	//nlapiLogExecution('DEBUG', 'Check Date', '2 i_total_days_sat_sun : ' + i_total_days_sat_sun);
	
	return i_total_days_sat_sun;
}

function getDatediffIndays(startDate, endDate)
{
    var one_day=1000*60*60*24;
    var fromDate = startDate;
    var toDate=endDate

	var date1 = nlapiStringToDate(fromDate);

    var date2 = nlapiStringToDate(toDate);

    var date3=Math.round((date2-date1)/one_day);

    return (date3+1);
}

function get_previous_month(i_month)
{
	if (_logValidation(i_month)) 
 	{
 	  if(i_month == 'Jan' || i_month == 'January' || i_month == 'JAN' || i_month == 'JANUARY')
	  {	  	
	  		i_month = 'December';				
	  }	
	  else if(i_month == 'Feb' || i_month == 'February' || i_month == 'FEB' || i_month == 'FEBRUARY')
	  {
	  	i_month = 'January';
			
	  }		
	  else if(i_month == 'Mar' || i_month == 'March' || i_month == 'MAR' || i_month == 'MARCH')
	  {	  
	     i_month = 'February';
	
	  }	
	  else if(i_month == 'Apr' || i_month == 'April' || i_month == 'APR' || i_month == 'APRIL')
	  {
	  	i_month = 'March';
	
	  }	
	  else if(i_month == 'May' || i_month == 'MAY')
	  {	  
		  i_month = 'April';
	
	  }	  
	  else if(i_month == 'June' || i_month == 'Jun' || i_month == 'JUN' || i_month == 'JUNE')
	  {	 
		 i_month = 'May';	 
			
	  }	
	  else if(i_month == 'Jul' || i_month == 'July' || i_month == 'JUL' || i_month == 'JULY')
	  {	  
		i_month = 'June';
	  }	
	  else if(i_month == 'August' || i_month == 'Aug' || i_month == 'AUG' || i_month == 'AUGUST')
	  {	  
		 i_month = 'July';
	  }  
	  else if(i_month == 'Sep' || i_month == 'September' || i_month == 'SEP' || i_month == 'SEPTEMBER')
	  {  	
	      i_month = 'August';
	
	  }	
	  else if(i_month == 'Oct' || i_month == 'October' || i_month == 'OCT' || i_month == 'OCTOBER')
	  {	 	
			i_month = 'September';	
	  }	
	  else if(i_month == 'Nov' || i_month == 'November' || i_month == 'NOV' || i_month == 'NOVEMBER')
	  {	  
		 i_month = 'October';
			
	  }	
	  else if(i_month == 'Dec' || i_month == 'December' || i_month == 'DEC' || i_month == 'DECEMBER')
	  {	 
		 i_month = 'November';
			
	  }	
 }//Month & Year
	
	return i_month;
}

function get_previous_month_start_date(i_month,i_year)
{
	i_month = i_month.toString();
	i_year = i_year.toString();
   var d_start_date = '';
   var d_end_date = '';
	
   var date_format = checkDateFormat();	 
	
 if (_logValidation(i_month) && _logValidation(i_year)) 
 {
 	  if(i_month == 'Jan' || i_month == 'January' || i_month == 'JAN' || i_month == 'JANUARY')
	  {	  	
		 if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 1 + '-' + 1;		
			d_end_date = i_year + '-' + 1 + '-' + 31;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 1 + '/' + i_year;
			d_end_date = 31 + '/' + 1 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 1 + '/' + 1 + '/' + i_year;
			d_end_date = 1 + '/' + 31 + '/' + i_year;
		 }	 
				
	  }	
	  else if(i_month == 'Feb' || i_month == 'February' || i_month == 'FEB' || i_month == 'FEBRUARY')
	  {
	  	if((i_year%2) == 0)
		{
			i_day = 29;
		}
		else
		{
			i_day = 28;
		}
			
		 if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 2 + '-' + 1;		
			d_end_date = i_year + '-' + 2 + '-' + i_day;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 2 + '/' + i_year;
			d_end_date = i_day + '/' + 2 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 2 + '/' + 1 + '/' + i_year;
			d_end_date = 2 + '/' + i_day + '/' + i_year;
		 }	 
			
	  }		
	  else if(i_month == 'Mar' || i_month == 'March' || i_month == 'MAR' || i_month == 'MARCH')
	  {	  
	     if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 3 + '-' + 1;		
			d_end_date = i_year + '-' + 3 + '-' + 31;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 3 + '/' + i_year;
			d_end_date = 31 + '/' + 3 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 3 + '/' + 1 + '/' + i_year;
			d_end_date = 3 + '/' + 31 + '/' + i_year;
		 }	 
	
	  }	
	  else if(i_month == 'Apr' || i_month == 'April' || i_month == 'APR' || i_month == 'APRIL')
	  {
	  	if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 4 + '-' + 1;		
			d_end_date = i_year + '-' + 4 + '-' + 30;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 4 + '/' + i_year;
			d_end_date = 30 + '/' + 4 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 4 + '/' + 1 + '/' + i_year;
			d_end_date = 4 + '/' + 30 + '/' + i_year;
	     }	 
	 
	
	  }	
	  else if(i_month == 'May' || i_month == 'MAY')
	  {	  
		 if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 5 + '-' + 1;		
			d_end_date = i_year + '-' + 5 + '-' + 31;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 5 + '/' + i_year;
			d_end_date = 31 + '/' + 5 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 5 + '/' + 1 + '/' + i_year;
			d_end_date = 5 + '/' + 31 + '/' + i_year;
	     }	 
	
	  }	  
	  else if(i_month == 'June' || i_month == 'Jun' || i_month == 'JUN' || i_month == 'JUNE')
	  {	 
		 if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 6 + '-' + 1;		
			d_end_date = i_year + '-' + 6 + '-' + 30;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 6 + '/' + i_year;
			d_end_date = 30 + '/' + 6 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 6 + '/' + 1 + '/' + i_year;
			d_end_date = 6 + '/' + 30 + '/' + i_year;
	     }	 
			
	  }	
	  else if(i_month == 'Jul' || i_month == 'July' || i_month == 'JUL' || i_month == 'JULY')
	  {	  
		 if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 7 + '-' + 1;		
			d_end_date = i_year + '-' + 7 + '-' + 31;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 7 + '/' + i_year;
			d_end_date = 31 + '/' + 7 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 7 + '/' + 1 + '/' + i_year;
			d_end_date = 7 + '/' + 31 + '/' + i_year;
	     }	 
		
	  }	
	  else if(i_month == 'August' || i_month == 'Aug' || i_month == 'AUG' || i_month == 'AUGUST')
	  {	  
		 if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 8 + '-' + 1;		
			d_end_date = i_year + '-' + 8 + '-' + 31;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 8 + '/' + i_year;
			d_end_date = 31 + '/' + 8 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 8 + '/' + 1 + '/' + i_year;
			d_end_date = 8 + '/' + 31 + '/' + i_year;
	     }	 
		
	  }  
	  else if(i_month == 'Sep' || i_month == 'September' || i_month == 'SEP' || i_month == 'SEPTEMBER')
	  {  	
	     if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 9 + '-' + 1;		
			d_end_date = i_year + '-' + 9 + '-' + 30;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 9 + '/' + i_year;
			d_end_date = 30 + '/' + 9 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 9 + '/' + 1 + '/' + i_year;
			d_end_date = 9 + '/' + 30 + '/' + i_year;
	     }	 
	
	  }	
	  else if(i_month == 'Oct' || i_month == 'October' || i_month == 'OCT' || i_month == 'OCTOBER')
	  {	 	
		if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 10 + '-' + 1;		
			d_end_date = i_year + '-' + 10 + '-' + 31;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 10 + '/' + i_year;
			d_end_date = 31 + '/' + 10 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 10 + '/' + 1 + '/' + i_year;
			d_end_date = 10 + '/' + 31 + '/' + i_year;
	     }		
	  }	
	  else if(i_month == 'Nov' || i_month == 'November' || i_month == 'NOV' || i_month == 'NOVEMBER')
	  {	  
		if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 11 + '-' + 1;		
			d_end_date = i_year + '-' + 11 + '-' + 30;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 11 + '/' + i_year;
			d_end_date = 30 + '/' + 11 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 11 + '/' + 1 + '/' + i_year;
			d_end_date = 11 + '/' + 30 + '/' + i_year;
	     }	 
			
	  }	
	  else if(i_month == 'Dec' || i_month == 'December' || i_month == 'DEC' || i_month == 'DECEMBER')
	  {	 
		if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 12 + '-' + 1;		
			d_end_date = i_year + '-' + 12 + '-' + 31;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 12 + '/' + i_year;
			d_end_date = 31 + '/' + 12 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 12 + '/' + 1 + '/' + i_year;
			d_end_date = 12 + '/' + 31 + '/' + i_year;
	     }	 
			
	  }	
 }//Month & Year
 return d_start_date;
}

function get_previous_month_end_date(i_month,i_year)
{
	i_month = i_month.toString();
	i_year = i_year.toString();
   var d_start_date = '';
   var d_end_date = '';
	
   var date_format = checkDateFormat();	 
	
 if (_logValidation(i_month) && _logValidation(i_year)) 
 {
 	  if(i_month == 'Jan' || i_month == 'January' || i_month == 'JAN' || i_month == 'JANUARY')
	  {	  	
		 if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 1 + '-' + 1;		
			d_end_date = i_year + '-' + 1 + '-' + 31;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 1 + '/' + i_year;
			d_end_date = 31 + '/' + 1 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 1 + '/' + 1 + '/' + i_year;
			d_end_date = 1 + '/' + 31 + '/' + i_year;
		 }	 
				
	  }	
	  else if(i_month == 'Feb' || i_month == 'February' || i_month == 'FEB' || i_month == 'FEBRUARY')
	  {
	  	if((i_year%2) == 0)
		{
			i_day = 29;
		}
		else
		{
			i_day = 28;
		}
			
		 if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 2 + '-' + 1;		
			d_end_date = i_year + '-' + 2 + '-' + i_day;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 2 + '/' + i_year;
			d_end_date = i_day + '/' + 2 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 2 + '/' + 1 + '/' + i_year;
			d_end_date = 2 + '/' + i_day + '/' + i_year;
		 }	 
			
	  }		
	  else if(i_month == 'Mar' || i_month == 'March' || i_month == 'MAR' || i_month == 'MARCH')
	  {	  
	     if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 3 + '-' + 1;		
			d_end_date = i_year + '-' + 3 + '-' + 31;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 3 + '/' + i_year;
			d_end_date = 31 + '/' + 3 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 3 + '/' + 1 + '/' + i_year;
			d_end_date = 3 + '/' + 31 + '/' + i_year;
		 }	 
	
	  }	
	  else if(i_month == 'Apr' || i_month == 'April' || i_month == 'APR' || i_month == 'APRIL')
	  {
	  	if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 4 + '-' + 1;		
			d_end_date = i_year + '-' + 4 + '-' + 30;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 4 + '/' + i_year;
			d_end_date = 30 + '/' + 4 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 4 + '/' + 1 + '/' + i_year;
			d_end_date = 4 + '/' + 30 + '/' + i_year;
	     }	 
	 
	
	  }	
	  else if(i_month == 'May' || i_month == 'MAY')
	  {	  
		 if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 5 + '-' + 1;		
			d_end_date = i_year + '-' + 5 + '-' + 31;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 5 + '/' + i_year;
			d_end_date = 31 + '/' + 5 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 5 + '/' + 1 + '/' + i_year;
			d_end_date = 5 + '/' + 31 + '/' + i_year;
	     }	 
	
	  }	  
	  else if(i_month == 'June' || i_month == 'Jun' || i_month == 'JUN' || i_month == 'JUNE')
	  {	 
		 if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 6 + '-' + 1;		
			d_end_date = i_year + '-' + 6 + '-' + 30;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 6 + '/' + i_year;
			d_end_date = 30 + '/' + 6 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 6 + '/' + 1 + '/' + i_year;
			d_end_date = 6 + '/' + 30 + '/' + i_year;
	     }	 
			
	  }	
	  else if(i_month == 'Jul' || i_month == 'July' || i_month == 'JUL' || i_month == 'JULY')
	  {	  
		 if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 7 + '-' + 1;		
			d_end_date = i_year + '-' + 7 + '-' + 31;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 7 + '/' + i_year;
			d_end_date = 31 + '/' + 7 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 7 + '/' + 1 + '/' + i_year;
			d_end_date = 7 + '/' + 31 + '/' + i_year;
	     }	 
		
	  }	
	  else if(i_month == 'August' || i_month == 'Aug' || i_month == 'AUG' || i_month == 'AUGUST')
	  {	  
		 if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 8 + '-' + 1;		
			d_end_date = i_year + '-' + 8 + '-' + 31;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 8 + '/' + i_year;
			d_end_date = 31 + '/' + 8 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 8 + '/' + 1 + '/' + i_year;
			d_end_date = 8 + '/' + 31 + '/' + i_year;
	     }	 
		
	  }  
	  else if(i_month == 'Sep' || i_month == 'September' || i_month == 'SEP' || i_month == 'SEPTEMBER')
	  {  	
	     if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 9 + '-' + 1;		
			d_end_date = i_year + '-' + 9 + '-' + 30;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 9 + '/' + i_year;
			d_end_date = 30 + '/' + 9 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 9 + '/' + 1 + '/' + i_year;
			d_end_date = 9 + '/' + 30 + '/' + i_year;
	     }	 
	
	  }	
	  else if(i_month == 'Oct' || i_month == 'October' || i_month == 'OCT' || i_month == 'OCTOBER')
	  {	 	
		if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 10 + '-' + 1;		
			d_end_date = i_year + '-' + 10 + '-' + 31;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 10 + '/' + i_year;
			d_end_date = 31 + '/' + 10 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 10 + '/' + 1 + '/' + i_year;
			d_end_date = 10 + '/' + 31 + '/' + i_year;
	     }		
	  }	
	  else if(i_month == 'Nov' || i_month == 'November' || i_month == 'NOV' || i_month == 'NOVEMBER')
	  {	  
		if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 11 + '-' + 1;		
			d_end_date = i_year + '-' + 11 + '-' + 30;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 11 + '/' + i_year;
			d_end_date = 30 + '/' + 11 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 11 + '/' + 1 + '/' + i_year;
			d_end_date = 11 + '/' + 30 + '/' + i_year;
	     }	 
			
	  }	
	  else if(i_month == 'Dec' || i_month == 'December' || i_month == 'DEC' || i_month == 'DECEMBER')
	  {	 
		if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 12 + '-' + 1;		
			d_end_date = i_year + '-' + 12 + '-' + 31;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 12 + '/' + i_year;
			d_end_date = 31 + '/' + 12 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 12 + '/' + 1 + '/' + i_year;
			d_end_date = 12 + '/' + 31 + '/' + i_year;
	     }	 
			
	  }	
 }//Month & Year
 return d_end_date;
}