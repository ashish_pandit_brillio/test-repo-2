/**
@return Void
**/

/**

Buddy Assignment Trigger

**/ 
function sch_reminder_trigger()
{
	try
	{
		var date=new Date();
		nlapiLogExecution('DEBUG','date',date);
		date=nlapiDateToString(date);
		nlapiLogExecution('DEBUG','date',date);
		var fil=[];
		fil[0]=new nlobjSearchFilter('custrecord_validtill',null,'on','daysfromnow30');
		fil[1]=new nlobjSearchFilter('isinactive',null,'is','F');
		var col=[];
		col[0]=new nlobjSearchColumn('custrecord_visaid');
		col[1]=new nlobjSearchColumn('custrecord_validtill');
		var searchRes=searchRecord('customrecord_empvisadetails',null,fil,col);
		nlapiLogExecution('DEBUG', 'searchRes==', searchRes.length);
		for(var i=0;i<searchRes.length;i++)
		{
			var empid=searchRes[i].getValue('custrecord_visaid');
			var tilldate=searchRes[i].getValue('custrecord_validtill');
			var emp_search=searchRecord('employee',null,
					[
					new nlobjSearchFilter('custentity_employee_inactive',null,'is','F'),
					new nlobjSearchFilter('custentity_implementationteam',null, 'is', 'F'),
				    new nlobjSearchFilter('custentity_persontype',null,'anyof','1'),
					new nlobjSearchFilter('custentity_employeetype',null,'anyof','6'),
					new nlobjSearchFilter('internalid',null,'anyof',empid)
					],
					[
					new nlobjSearchColumn('entityid'),
					new nlobjSearchColumn('email')
					]);
			if(_logValidation(emp_search))
			{
				nlapiLogExecution('DEBUG', 'emp_search==', emp_search.length);
				for(var x=0;x<emp_search.length;x++)
				{
					var empName=emp_search[x].getValue('entityid');
					var split=empName.split('-');
					empName=split[1];
					var emp_email=emp_search[x].getValue('email');
					nlapiLogExecution('DEBUG','Emp',empid);
			
					var sub_fil=[];
					sub_fil[0]=new nlobjSearchFilter('custrecord_stvd_vendor_type',null,'is','1');
					sub_fil[1]=new nlobjSearchFilter('custrecord_stvd_contractor',null,'anyof',empid);
					sub_fil[2]=new nlobjSearchFilter('isinactive',null,'is','F');
					sub_fil[3]=new nlobjSearchFilter('custrecord_stvd_end_date',null,'notonorbefore',date);
					var sub_col=[];
					sub_col[0]=new nlobjSearchColumn('custrecord_stvd_vendor');
					var subtier_search=searchRecord('customrecord_subtier_vendor_data',null,sub_fil,sub_col);
					for( var j=0;j<subtier_search.length;j++)
					{			
						nlapiLogExecution('DEBUG', 'subtier_search==', subtier_search.length);
						var venid=subtier_search[j].getValue('custrecord_stvd_vendor');
						var vendor_det=nlapiLoadRecord('vendor',venid);
						var vend_email=vendor_det.getFieldValue('email');
						nlapiLogExecution('DEBUG','vend_email',vend_email);
						var ven_name=vendor_det.getFieldValue('altname');
					} ///for( var j=0;j<subtier_search.length;j++)
					var search_result = searchRecord('resourceallocation', null, [
						new nlobjSearchFilter('custentity_employee_inactive',
                            'employee', 'is', 'F'),
						new nlobjSearchFilter('resource',
                            null, 'anyof', empid),
						new nlobjSearchFilter('startdate', null, 'notafter', date),
						new nlobjSearchFilter('enddate', null, 'notbefore', date) ],
						[ new nlobjSearchColumn('project'),
						new nlobjSearchColumn('customer')]);
						for(var pro=0;pro<search_result.length;pro++)
						{
							nlapiLogExecution('DEBUG', 'search_result==', search_result.length);
							var project=search_result[pro].getValue('project');
							nlapiLogExecution('DEBUG','project',project);
							var custmr=search_result[pro].getValue('customer');
							nlapiLogExecution('DEBUG','custmr',custmr);
							var custmr_rec=nlapiLoadRecord('customer',custmr);
							var custmr_name=custmr_rec.getFieldValue('altname');
				
						} //// for(var pro=0;pro<search_result.length;pro++)
						var a_emp_attachment = new Array();
						a_emp_attachment['entity'] = '7935';
						var bcc=emp_email;
						nlapiLogExecution('DEBUG', 'ven_name==', ven_name);
						nlapiLogExecution('DEBUG', 'custmr_name==', custmr_name);
						 
						if(ven_name)
						{
									
							if(_logValidation(custmr_name))
                          {
						body=bodyContent(empName,custmr_name,ven_name,tilldate);
                          } //// end of if(_logValidation(custmr_name))
                  else
                    {
                      body=bodyfailContent(empName,custmr_name,ven_name,tilldate);
                    } /// End of else of if(_logValidation(custmr_name))
						//  nlapiSendEmail(7935, vend_email, 'I9 Expiring shortly -',body.MailBody, ['business.ops@brillio.com'], 'sai.vannamareddy@brillio.com' ,null,a_emp_attachment);
						var i_Email_Sender = 9701;
              if(_logValidation(custmr_name)) //// Change of From address on 02 march 2021 by shravan
                          {
					/*	nlapiSendEmail(132194, vend_email, 'I9 Expiring shortly- '+empName+' ', body.MailBody, ['pushpendra.kumar@BRILLIO.COM','business.ops@brillio.com'],
						['information.systems@brillio.com'], a_emp_attachment, null, true, null, null);*/
						nlapiSendEmail(i_Email_Sender, vend_email, 'I9 Expiring shortly- '+empName+' ', body.MailBody, ['business.ops@brillio.com','background.verification@brillio.com'],
						['information.systems@brillio.com'], a_emp_attachment, null, true, null, null); /// sender Gaurav
                         } //// End of   if(_logValidation(custmr_name))
                  else
                    {
                      nlapiSendEmail(442, 'pushpendra.kumar@brillio.com', 'I9 Expiring Email  Failed For- '+empName+' ', body.Mailfailbody, ['business.ops@brillio.com','background.verification@brillio.com'],
						['information.systems@brillio.com'], a_emp_attachment, null, true, null, null);
                    } /// End of else of  if(_logValidation(custmr_name))/// 
				} /// End of if(ven_name)
			
							else
							{
									
									var s_body =  bodyfail_vendor_unavilable(empName,tilldate);
									nlapiLogExecution('DEBUG', 'bodyfail_vendor_unavilable==', s_body.Vendor_Unavialble);
								  	nlapiSendEmail(442, 'pushpendra.kumar@brillio.com', 'I9 Expiring Email  Failed For- '+empName+' ',s_body.Vendor_Unavialble, ['business.ops@brillio.com','background.verification@brillio.com'],
											['information.systems@brillio.com'], a_emp_attachment, null, true, null, null);
							} ///  ///// End of else of if if(ven_name)
				} //// End of for(var x=0;x<emp_search.length;x++)
		} /// End for(var i=0;i<searchRes.length;i++)			
		} //if(_logValidation(emp_search))		
	}/// End of try
	catch(e)
	{
		nlapiLogExecution('DEBUG', 'Exception==', e);
	} //// End of catch
	
 } /// End of function sch_reminder_trigger()
 /// put try catch
 
  

function bodyContent(empName,custmr_name,ven_name,tilldate)
{
     
	var htmltext = '';
    
   htmltext += '<table border="0" width="100%"><tr>';
   htmltext += '<td colspan="4" valign="top">';
   htmltext += '<p>Hello,</p><br/>';
    htmltext += '<p> This is with reference to your resource '+ empName +' who is working at '+custmr_name+' through Brillio. As per the I9 provided by '+ ven_name +' ,'+ empName +'’s I9 is expiring on <b>'+tilldate+'.</b></p><br/>';
   htmltext +='<p>I am sure you must be aware that I9 is a federal document and it is also our client requirement that we ensure that each resource has valid work authorization. </p><br/>';
   htmltext +='<p>Please forward me an updated copy of ' + empName + ' I9 at the earliest. In case if you have applied for his work authorization and the work is still in progress, please forward some documentary proof supporting.</p><br/><br/>';
   htmltext += '<p>Please feel free to call me should you have any queries.</p><br/><br/>';
   htmltext += '<p style="color:#04122E;"><b>Thanks & Regards,<br/>';
  htmltext += 'Mit Trivedi<br/> Associate Manager - HR Operations  </b><br/> Email : mit.trivedi@BRILLIO.COM</p>';
 
    htmltext += '</td></tr>';
    htmltext += '</table>';
    htmltext += '<hr width="100%" size="1" noshade color="#CCCCCC">';
    htmltext += '<table width="100%" border="0" cellspacing="0" cellpadding="0">';
    htmltext += '<tr>';
    htmltext += '<td align="right">';
    htmltext += '<font style="font-size:9px; font-family:Verdana,Arial,Helvetica,sans-serif; color:#999999;">Brillio is powered by <a href="http://www.netsuite.com/" style="color:#999999;">NetSuite</a> One System. No Limits.</font>';
    htmltext += '</td>';
    htmltext += '</tr>';
    htmltext += '</table>';
    htmltext += '</body>';
    htmltext += '</html>';
 
    return {
        MailBody : htmltext
             
    };
}
function bodyfailContent(empName,custmr_name,ven_name,tilldate)
{
     
	var htmltext = '';
    
   htmltext += '<table border="0" width="100%"><tr>';
   htmltext += '<td colspan="4" valign="top">';
   htmltext += '<p>Hello,</p><br/>';
    htmltext += '<p> This is here to inform you that, for the resource '+ empName +' I9 is expiring on <b>'+tilldate+'.</b> The email notification trigger could not able  to sent to vendor due to unallocation of resource. Please do needful.</p><br/>';
   
   htmltext += '<p style="color:#04122E;"><b>Thanks & Regards,<br/>';
   htmltext += 'Information Systems </b><br/> information.systems@brillio.com </p>';
 
    htmltext += '</td></tr>';
    htmltext += '</table>';
    htmltext += '<hr width="100%" size="1" noshade color="#CCCCCC">';
    htmltext += '<table width="100%" border="0" cellspacing="0" cellpadding="0">';
    htmltext += '<tr>';
    htmltext += '<td align="right">';
    htmltext += '<font style="font-size:9px; font-family:Verdana,Arial,Helvetica,sans-serif; color:#999999;">Brillio is powered by <a href="http://www.netsuite.com/" style="color:#999999;">NetSuite</a> One System. No Limits.</font>';
    htmltext += '</td>';
    htmltext += '</tr>';
    htmltext += '</table>';
    htmltext += '</body>';
    htmltext += '</html>';
 
    return {
        Mailfailbody : htmltext
             
    };
}

function bodyfail_vendor_unavilable(empName,tilldate)
{
     
	var htmltext = '';
    
   htmltext += '<table border="0" width="100%"><tr>';
   htmltext += '<td colspan="4" valign="top">';
   htmltext += '<p>Hello,</p><br/>';
    htmltext += '<p> This is here to inform you that, for the resource '+ empName +' I9 is expiring on <b>'+tilldate+'.</b> The Vendor allocation date has expired. Please do needful.</p><br/>';
   
   htmltext += '<p style="color:#04122E;"><b>Thanks & Regards,<br/>';
   htmltext += 'Information Systems </b><br/> information.systems@brillio.com </p>';
 
    htmltext += '</td></tr>';
    htmltext += '</table>';
    htmltext += '<hr width="100%" size="1" noshade color="#CCCCCC">';
    htmltext += '<table width="100%" border="0" cellspacing="0" cellpadding="0">';
    htmltext += '<tr>';
    htmltext += '<td align="right">';
    htmltext += '<font style="font-size:9px; font-family:Verdana,Arial,Helvetica,sans-serif; color:#999999;">Brillio is powered by <a href="http://www.netsuite.com/" style="color:#999999;">NetSuite</a> One System. No Limits.</font>';
    htmltext += '</td>';
    htmltext += '</tr>';
    htmltext += '</table>';
    htmltext += '</body>';
    htmltext += '</html>';
 
    return {
        Vendor_Unavialble : htmltext
             
    };
}  ///// End of bodyfailContent

function _logValidation(value) 
{
if(value!='null' && value != null && value != null && value != '' && value != undefined && value != undefined && value != 'undefined' && value!= 'undefined'&& value!= 'NaN' && value != NaN) 
 {
  return true;
}
else 
 { 
  return false;
}
}