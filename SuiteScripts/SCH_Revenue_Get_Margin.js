var grade_actual_array = [];
var BillingType = "";
var finalArray = [];
var projectNameArray = [];
function getMargin() {
    var projectArray = getProjects();// [8387, 129563]; //
    for (var q = 0; q < projectArray.length; q++) {
        CheckMetering();
      	var int_project_id = projectArray[q]; //8387;//129563;//139893;
		//nlapiLogExecution("ERROR","int_project_id : ",int_project_id);
		var s_project_name = nlapiLookupField("job", int_project_id, "entityid");
		////nlapiLogExecution("ERROR","s_project_name : ",s_project_name);
		projectNameArray.push(s_project_name);	
        BillingType = nlapiLookupField("job", int_project_id, "jobbillingtype");
		var sr_no = 0;
        var bill_data_arr = new Array();
        var a_project_list = new Array();
        var a_selected_project_list = '';
        var i_user_id = nlapiGetUser();
        var search = nlapiLoadSearch('transaction', 2947);
        var filters = search.getFilters();
        //Search for exchange Rate
        var f_rev_curr = 0;
        var f_cost_curr = 0;
        var filters = [];
        var a_dataVal = {};
        var dataRows = [];
        var a_tempRevenue = [];
        var a_temp_amount = [];
        var s_from = '';
        dataRows = getCurrencyExchangeRate();
        var project_search_results = '';
        project_search_results = getSelectedProjects(int_project_id);
        var s_project_options = '';
        for (var i = 0; project_search_results != null && i < project_search_results.length; i++) {
            CheckMetering();
            var i_project_id = project_search_results[i].getValue('internalid');
            var s_project_number = project_search_results[i]
                .getValue('entityid');
            var s_project_name = project_search_results[i].getValue('companyname');
            //projectNameArray.push(s_project_name);
            var sold_margin = project_search_results[i].getValue('custentity_sold_margin_percent');
            var billing_type = project_search_results[i].getText('jobbillingtype');
            var st_date = project_search_results[i].getValue('startdate');
            //st_date=nlapiDateToString(st_date);
            var end_date = project_search_results[i].getValue('enddate');
            //end_date=nlapiDateToString(end_date);
            s_from = st_date;
            var project_name = project_search_results[i].getValue('companyname');

            var pro_internal_id = project_search_results[i].getId();


            if (_logValidation(s_project_number)) {
                a_project_list.push(s_project_number);
            }
            var s_selected = '';
            if ((a_selected_project_list != null && a_selected_project_list
                    .indexOf(s_project_number) != -1)) {
                s_selected = ' selected="selected" ';
                if (s_selected_project_name != '') {
                    s_selected_project_name += ', ';
                }
                s_selected_project_name += s_project_number + ' ' +
                    s_project_name;
            }
            s_project_options += '<option value="' + s_project_number + '" ' +
                s_selected + '>' + s_project_number + ' ' +
                s_project_name + '</option>';

        }
        if (a_selected_project_list == null ||
            a_selected_project_list.length == 0) {
            if (a_project_list.length > 0) {
                a_selected_project_list = a_project_list;


            } else {
                a_selected_project_list = new Array();
            }
        }

        if (a_selected_project_list != null &&
            a_selected_project_list.length != 0) {
            var s_formula = '';

            for (var i = 0; i < a_selected_project_list.length; i++) {
                if (i != 0) {
                    s_formula += " OR";
                }

                s_formula += " SUBSTR({custcolprj_name}, 0,9) = '" +
                    a_selected_project_list[i] + "' ";
            }

            var projectFilter = new nlobjSearchFilter('formulanumeric', null,
                'equalto', 1);

            projectFilter.setFormula("CASE WHEN " + s_formula +
                " THEN 1 ELSE 0 END");

            filters = filters.concat([projectFilter]);
        } else {
            var projectFilter = new nlobjSearchFilter('formulanumeric', null,
                'equalto', 1);

            projectFilter.setFormula("0");

            filters = filters.concat([projectFilter]);
        }

        var current_date = new Date();
        var columns = search.getColumns();
        var search_results = getTransactions(columns, filters);
        var o_json = new Object();

        // Get the facility cost

        var s_facility_cost_filter = '';
        for (var i = 0; i < a_selected_project_list.length; i++) {
            s_facility_cost_filter += "'" + a_selected_project_list[i] + "'";
            if (i != a_selected_project_list.length - 1) {
                s_facility_cost_filter += ",";
            }
        }

        var facility_cost_filters = new Array();
        facility_cost_filters[0] = new nlobjSearchFilter('formulanumeric',
            null, 'equalto', 1);
        if (s_facility_cost_filter != '') {
            facility_cost_filters[0]
                .setFormula('CASE WHEN {custrecord_arpm_project.entityid} IN (' +
                    s_facility_cost_filter + ') THEN 1 ELSE 0 END');
        } else {
            facility_cost_filters[0].setFormula('0');
        }

        var facility_cost_columns = new Array();
        facility_cost_columns[0] = new nlobjSearchColumn(
            'custrecord_arpm_period');
        facility_cost_columns[1] = new nlobjSearchColumn(
            'custrecord_arpm_num_allocated_resources');
        facility_cost_columns[2] = new nlobjSearchColumn(
            'custrecord_arpm_facility_cost_per_person');
        facility_cost_columns[3] = new nlobjSearchColumn(
            'custrecord_arpm_location');

        var facility_cost_search_results = searchRecord(
            'customrecord_allocated_resources_per_mon', null,
            facility_cost_filters, facility_cost_columns);
        var o_data = {
            'Revenue': [],
            'Discount': [],
            'People Cost': [],
            'Facility Cost': [],
            'Other Cost - Travel': [],
            'Other Cost - Immigration': [],
            'Other Cost - Professional Fees': [],
            'Other Cost - Others': []

        };
        var new_object = null;

        var s_period = '';

        var a_period_list = [];
        var a_category_list = [];
        a_category_list[0] = '';
        var a_group = [];
        var a_income_group = [];
        var j_other_list = {};
        var other_list = [];
        var col = new Array();
        col[0] = new nlobjSearchColumn('custrecord_account_name');
        col[1] = new nlobjSearchColumn('custrecord_type_of_cost');
        var oth_fil = new Array();
        oth_fil[0] = new nlobjSearchFilter('isinactive', null, 'is', 'F');
        var other_Search = nlapiSearchRecord('customrecord_pl_other_costs', null, oth_fil, col);
        if (other_Search) {
            for (var i_ind = 0; i_ind < other_Search.length; i_ind++) {
                var acctname = other_Search[i_ind].getValue('custrecord_account_name');
                a_category_list.push(acctname);
                j_other_list = {
                    o_acct: other_Search[i_ind].getValue('custrecord_account_name'),
                    o_type: other_Search[i_ind].getText('custrecord_type_of_cost')
                };
                other_list.push(j_other_list);
            }
        }
        for (var i = 0; search_results != null && i < search_results.length; i++) {
            CheckMetering();
            var period = search_results[i].getText(columns[0]);
            //Code updated by Deepak, Dated - 21 Mar 17
            var s_month_year = period.split(' ');
            var s_mont = s_month_year[0];
            s_mont = getMonthCompleteName(s_mont);
            var s_year_ = s_month_year[1];
            var f_revRate = 66.0;
            var f_costRate = 66.0;
            var f_gbp_rev_rate;
            //	var rate=nlapiExchangeRate('GBP', 'INR', mnt_lat_date);
            //	//////nlapiLogExecution('audit','accnt name:- '+mnt_lat_date);
            ////////nlapiLogExecution('audit','accnt name:- '+rate);

            //Fetch matching cost and rev rate convertion rate
            for (var data_indx = 0; data_indx < dataRows.length; data_indx++) {
                if (dataRows[data_indx].s_month == s_mont && dataRows[data_indx].i_year == s_year_) {
                    f_revRate = dataRows[data_indx].rev_rate;
                    f_costRate = dataRows[data_indx].cost_rate;
                    //f_gbp_rev_rate=dataRows[data_indx].gbp_rev_rate;
                    f_crc_cost_rate = dataRows[data_indx].crc_cost;
                    f_nok_cost_rate = dataRows[data_indx].nok_cost_rate;
                }
            }
            /*	if(f_gbp_rev_rate==0)
            	{
            	f_gbp_rev_rate=getExchangerates(s_mont,s_year_);
            	}*/

            var transaction_type = search_results[i].getText(columns[8]);

            var transaction_date = search_results[i].getValue(columns[9]);

            var i_subsidiary = search_results[i].getValue(columns[10]);

            var amount = parseFloat(search_results[i].getValue(columns[1]));

            var category = search_results[i].getValue(columns[3]);

            var s_account_name = search_results[i].getValue(columns[11]);

            var currency = search_results[i].getValue(columns[12]);

            var exhangeRate = search_results[i].getValue(columns[13]);
            var offsiteOnsite = search_results[i].getValue(columns[14]);
            var empName = search_results[i].getValue(columns[15]);
            ////////nlapiLogExecution('audit', 'offsiteOnsite:- ', offsiteOnsite);
            ////////nlapiLogExecution('audit', 'offsiteOnsite:- ', offsiteOnsite);
            ////////nlapiLogExecution('audit', 'empName:- ' + empName);

            if ((currency == parseInt(2)) && (parseInt(i_subsidiary) != parseInt(2))) {
                amount = parseFloat(amount) * parseFloat(exhangeRate);

            }
            //AMount Convertion Logic
            if ((parseInt(i_subsidiary) != parseInt(2)) && (currency != parseInt(1))) {
                if (category != 'Revenue' && category != 'Other Income' &&
                    category != 'Discount' && parseInt(i_subsidiary) != parseInt(2)) {
                    if (currency == parseInt(9)) {
                        amount = parseFloat(amount) / parseFloat(f_crc_cost_rate);
                    } else if (currency == parseInt(8)) {
                        amount = parseFloat(amount) / parseFloat(f_nok_cost_rate);
                    } else {
                        //f_total_expense += o_data[s_category][j].amount;
                        amount = parseFloat(amount) / parseFloat(f_costRate);
                    }

                } else {

                    amount = parseFloat(amount) / parseFloat(f_revRate);

                    //f_total_revenue += o_data[s_category][j].amount;
                }
            } //END



            var isWithinDateRange = true;

            var d_transaction_date = nlapiStringToDate(transaction_date,
                'datetimetz');

            if (s_from != '') {
                var d_from = nlapiStringToDate(s_from, 'datetimetz');

                if (d_transaction_date < d_from) {
                    isWithinDateRange = false;
                }
            }
            if (isWithinDateRange == true) {
                var i_index = a_period_list.indexOf(period);

                if (i_index == -1) {
                    if (_logValidation(period)) {
                        var date = period.split(" ");
                        var yearPeriod = date[1];
                        var currentDate = new Date();
                        var year = currentDate.getFullYear();
                        if (yearPeriod >= year) {
                            ////////nlapiLogExecution("ERROR", "year" + year, "yearPeriod" + yearPeriod);
                            a_period_list.push(period);
                        }
                    } else {
                        ////////nlapiLogExecution('debug', 'error', '388');
                    }

                }

                i_index = a_period_list.indexOf(period);
            }
            if (category != 'People Cost' && category != 'Discount' && category != 'Facility Cost' && category != 'Revenue') {
                var o_index = a_category_list.indexOf(s_account_name);
                if (o_index != -1) {
                    o_index = o_index - 1;
                    var acct = other_list[o_index].o_acct;
                    var typeofact = other_list[o_index].o_type;


                    if ((_logValidation(period)) && (_logValidation(amount)) && (_logValidation(search_results[i].getValue(columns[4]))) &&
                        (_logValidation(search_results[i].getValue(columns[5]))) && (_logValidation(transaction_type)) &&
                        (_logValidation(transaction_date)) && (_logValidation(isWithinDateRange)) && (_logValidation(s_account_name))) {} else {
                        //  //////nlapiLogExecution('debug','error','408');
                    }
                    if (typeofact == 'Other Direct Cost - Travel') {
                        //////nlapiLogExecution("DEBUG", "typeofact : ", typeofact);
                        o_data['Other Cost - Travel'].push({
                            'period': period,
                            'amount': amount,
                            'num': search_results[i].getValue(columns[4]),
                            'memo': search_results[i].getValue(columns[5]),
                            'type': transaction_type,
                            'dt': transaction_date,
                            'include': isWithinDateRange,
                            's_account_name': s_account_name,
                            'offsiteOnsite': offsiteOnsite,
                            'employee': empName
                        });
                    } else if (typeofact == 'Other Direct Cost - Immigration') {
                        //////nlapiLogExecution("DEBUG", "typeofact : ", typeofact);
                        o_data['Other Cost - Immigration'].push({
                            'period': period,
                            'amount': amount,
                            'num': search_results[i].getValue(columns[4]),
                            'memo': search_results[i].getValue(columns[5]),
                            'type': transaction_type,
                            'dt': transaction_date,
                            'include': isWithinDateRange,
                            's_account_name': s_account_name,
                            'offsiteOnsite': offsiteOnsite,
                            'employee': empName
                        });
                    }
                    if (typeofact == 'Other Direct Cost - Professional Fees') {
                        //////nlapiLogExecution("DEBUG", "typeofact : ", typeofact);
                        o_data['Other Cost - Professional Fees'].push({
                            'period': period,
                            'amount': amount,
                            'num': search_results[i].getValue(columns[4]),
                            'memo': search_results[i].getValue(columns[5]),
                            'type': transaction_type,
                            'dt': transaction_date,
                            'include': isWithinDateRange,
                            's_account_name': s_account_name,
                            'offsiteOnsite': offsiteOnsite,
                            'employee': empName
                        });
                    }
                    if (typeofact == 'Other Direct Cost - Others') {
                        //////nlapiLogExecution("DEBUG", "typeofact : ", typeofact);
                        o_data['Other Cost - Others'].push({
                            'period': period,
                            'amount': amount,
                            'num': search_results[i].getValue(columns[4]),
                            'memo': search_results[i].getValue(columns[5]),
                            'type': transaction_type,
                            'dt': transaction_date,
                            'include': isWithinDateRange,
                            's_account_name': s_account_name,
                            'offsiteOnsite': offsiteOnsite,
                            'employee': empName
                        });
                    }

                }


            } else {

                //avoiding discounts from account						  
                if (category == 'Revenue' && i_subsidiary == parseInt(2)) {
                    //////nlapiLogExecution("DEBUG", "typeofact : ", "Revenue");
                    if (s_account_name != parseInt(732)) {
                        o_data[category].push({
                            'period': period,
                            'amount': amount,
                            'num': search_results[i].getValue(columns[4]),
                            'memo': search_results[i].getValue(columns[5]),
                            'type': transaction_type,
                            'dt': transaction_date,
                            'include': isWithinDateRange,
                            's_account_name': s_account_name,
                            'offsiteOnsite': offsiteOnsite,
                            'employee': empName
                        });
                    }
                } else {
                    //////nlapiLogExecution("DEBUG", "typeofact : ", "Revenue else ");
                    o_data[category].push({
                        'period': period,
                        'amount': amount,
                        'num': search_results[i].getValue(columns[4]),
                        'memo': search_results[i].getValue(columns[5]),
                        'type': transaction_type,
                        'dt': transaction_date,
                        'include': isWithinDateRange,
                        's_account_name': s_account_name,
                        'offsiteOnsite': offsiteOnsite,
                        'employee': empName
                    });
                }
            }
        }
        for (var i = 0; facility_cost_search_results != null && i < facility_cost_search_results.length; i++) {

            var s_period = facility_cost_search_results[i]
                .getValue(facility_cost_columns[0]);
            var f_allocated_resources = facility_cost_search_results[i]
                .getValue(facility_cost_columns[1]);
            var f_cost_per_resource = facility_cost_search_results[i]
                .getValue(facility_cost_columns[2]);
            var s_location = facility_cost_search_results[i]
                .getText(facility_cost_columns[3]);

            if (f_cost_per_resource == '') {
                f_cost_per_resource = 0;
            }
            if (_logValidation(f_allocated_resources)) {} else {
                f_allocated_resources = 0;
            }


            //if(s_exclude == 'true'){
            o_data['Facility Cost'].push({
                'period': s_period,
                'amount': 0,
                'num': '',
                'memo': 'Facility cost for ' + f_allocated_resources +
                    ' resources at ' + s_location + '.',
                'type': 'Facility Cost',
                'dt': '',
                'include': a_period_list.indexOf(s_period) != -1,
                's_account_name': ''
            });

        }
        var o_total = new Array();
        var o_period_total = new Array();

        var f_total_revenue = 0.0;
        var f_total_expense = 0.0;
        var f_profit_percentage = 0.0;
        var a_chart = new Array();
        var a_tempRevenue = new Array();
        var a_offSiteOnsiteArray = new Array();
        var a_employees = new Array();

        //
        // a_chart.push(['x'].concat(a_period_list));
        ////////nlapiLogExecution("ERROR", "o_data", JSON.stringify(o_data));
        for (var s_category in o_data) {
            CheckMetering();
            var a_temp = [];
            o_total[s_category] = 0.0;
            o_period_total[s_category] = 0.0;
            for (var i = 0; i < a_period_list.length; i++) {
                a_temp.push(0.0);
            }
            if (s_category == "Revenue") {
                ////////nlapiLogExecution("Debug", "o_data[s_category]", JSON.stringify(o_data[s_category]));
                var groupBy = function(xs, key) {
                    return xs.reduce(function(rv, x) {
                        (rv[x[key]] = rv[x[key]] || []).push(x);
                        return rv;
                    }, {});
                };
                var groubedByTeam = groupBy(o_data[s_category], 'period');
                //////nlapiLogExecution("ERROR", "o_data", JSON.stringify(a_period_list));
                //////nlapiLogExecution("ERROR", "groubedByTeam", JSON.stringify(groubedByTeam));
                for (var j = 0; j < a_period_list.length; j++) {
                    var amount = 0;
                    var offSiteAmount = 0;
                    var onSiteAmount = 0;
                    var tempArray = groubedByTeam[a_period_list[j]];
                    //////nlapiLogExecution("ERROR", "tempArray : ", JSON.stringify(tempArray));
                    if (tempArray) {
                        for (var int = 0; int < tempArray.length; int++) {
                            amount += tempArray[int].amount;
                            if (tempArray[int].offsiteOnsite == "Offsite") {
                                offSiteAmount += tempArray[int].amount
                            } else {
                                onSiteAmount += tempArray[int].amount
                            }
                            a_employees.push(tempArray[int].employee)
                        }
                        a_tempRevenue.push({
                            "period": tempArray[0].period,
                            "amount": amount,
                            "onsite": onSiteAmount,
                            "offsite": offSiteAmount
                        })

                    }
                }
            }
        }
		var newObj = createJSONObj(a_employees, a_tempRevenue, int_project_id);
		var flag = _.isEmpty(newObj);
		if(!flag){
		finalArray.push(newObj);
		}
    }
    createXML(finalArray);
}

function createJSONObj(a_employees, a_tempRevenue, int_project_id) {
	var jsonObj = {};
jsonObj.months = {
		"Jan": { "total" : {
			"billiableDays" : "",
			"revenueFP" : "",
			"revenueTM" : ""
		},
		"Onsite" : {
			"billiableDays" : "",
			"revenueFP" : "",
			"revenueTM" : ""
		} ,
		"Offsite" : {
			"billiableDays" : "",
			"revenueFP" : "",
			"revenueTM" : ""
		}
		},
		"Feb": {
			"total" : {
				"billiableDays" : "",
				"revenueFP" : "",
				"revenueTM" : ""
			},
			"Onsite" : {
				"billiableDays" : "",
				"revenueFP" : "",
				"revenueTM" : "",
				"11" : ""
			} ,
			"Offsite" : {
				"billiableDays" : "",
				"revenueFP" : "",
				"revenueTM" : ""
			}
		},
		"Mar": {
			"total" : {
				"billiableDays" : "",
				"revenueFP" : "",
				"revenueTM" : ""
			},
			"Onsite" : {
				"billiableDays" : "",
				"revenueFP" : "",
				"revenueTM" : "",
				"11" : ""
			} ,
			"Offsite" : {
				"billiableDays" : "",
				"revenueFP" : "",
				"revenueTM" : ""
			}
		},
		"Apr": {
			"total" : {
				"billiableDays" : "",
				"revenueFP" : "",
				"revenueTM" : ""
			},
			"Onsite" : {
				"billiableDays" : "",
				"revenueFP" : "",
				"revenueTM" : ""
			} ,
			"Offsite" : {
				"billiableDays" : "",
				"revenueFP" : "",
				"revenueTM" : ""
			}
		},
		"May": {
			"total" : {
				"billiableDays" : "",
				"revenueFP" : "",
				"revenueTM" : ""
			},
			"Onsite" : {
				"billiableDays" : "",
				"revenueFP" : "",
				"revenueTM" : ""
			},
			"Offsite" : {
				"billiableDays" : "",
				"revenueFP" : "",
				"revenueTM" : ""
			}
		},
		"Jun": {
			"total" : {
				"billiableDays" : "",
				"revenueFP" : "",
				"revenueTM" : ""
			},
			"Onsite" : {
				"billiableDays" : "",
				"revenueFP" : "",
				"revenueTM" : ""
			},
			"Offsite" : {
				"billiableDays" : "",
				"revenueFP" : "",
				"revenueTM" : ""
			}
		},
		"Jul": {
			"total" : {
				"billiableDays" : "",
				"revenueFP" : "",
				"revenueTM" : ""
			},
			"Onsite" : {
				"billiableDays" : "",
				"revenueFP" : "",
				"revenueTM" : ""
			},
			"Offsite" : {
				"billiableDays" : "",
				"revenueFP" : "",
				"revenueTM" : ""
			}
		},
		"Aug": {
			"total" : {
				"billiableDays" : "",
				"revenueFP" : "",
				"revenueTM" : ""
			},
			"Onsite" : {
				"billiableDays" : "",
				"revenueFP" : "",
				"revenueTM" : ""
			} ,
			"Offsite" : {
				"billiableDays" : "",
				"revenueFP" : "",
				"revenueTM" : ""
            }
		},
		"Sep": {
			"total" : {
				"billiableDays" : "",
				"revenueFP" : "",
				"revenueTM" : ""
			},
			"Onsite" : {
				"billiableDays" : "",
				"revenueFP" : "",
				"revenueTM" : ""
			},
			"Offsite" : {
				"billiableDays" : "",
				"revenueFP" : "",
				"revenueTM" : ""
			}
		},
		"Oct": {
			"total" : {
				"billiableDays" : "",
				"revenueFP" : "",
				"revenueTM" : ""
			},
			"Onsite" : {
				"billiableDays" : "",
				"revenueFP" : "",
				"revenueTM" : ""
			},
			"Offsite" : {
				"billiableDays" : "",
				"revenueFP" : "",
				"revenueTM" : ""
			}
		},
		"Nov": {
			"total" : {
				"billiableDays" : "",
				"revenueFP" : "",
				"revenueTM" : ""
			},
			"Onsite" : {
				"billiableDays" : "",
				"revenueFP" : "",
				"revenueTM" : ""
			},
			"Offsite" : {
				"billiableDays" : "",
				"revenueFP" : "",
				"revenueTM" : ""
			}
		},
		"Dec": {
			"total" : {
				"billiableDays" : "",
				"revenueFP" : "",
				"revenueTM" : ""
			},
			"Onsite" : {
				"billiableDays" : "",
				"revenueFP" : "",
				"revenueTM" : ""
			} ,
			"Offsite" : {
				"billiableDays" : "",
				"revenueFP" : "",
				"revenueTM" : ""
			}
		}
}
	getBilliableDays(int_project_id,jsonObj);
    var a_emp_Ids = convertTextEmpToEmpId(a_employees);
    var employeeTandMArray = [];
    var temp = {};
    var secondTemp = {};
	var newObj = {}
    nlapiLogExecution("DEBUG", "a_tempRevenue : ",JSON.stringify(a_tempRevenue));
	for (var i = 0; i < a_tempRevenue.length; i++) {
        var periodMonth = a_tempRevenue[i].period;
        var periodAmount = a_tempRevenue[i].amount;
        var month = (periodMonth.split(" "))[0];
        var onsiteAmount = a_tempRevenue[i].onsite;
        var offsiteAmount = a_tempRevenue[i].offsite;
        var monthNumber = month_array.indexOf(month);
        ////////nlapiLogExecution("DEBUG", "monthNumber : ", monthNumber);
        if (monthNumber != -1) {
            var monthArray = getFilter(monthNumber);
            ////nlapiLogExecution("DEBUG", "Billing Type : ", BillingType);
            if (BillingType == "TM") {
                jsonObj.months[month].total.revenueTM = periodAmount;
                jsonObj.months[month].Onsite.revenueTM = onsiteAmount;
                jsonObj.months[month].Offsite.revenueTM = offsiteAmount;
				if(isArrayNotEmpty(a_emp_Ids)){
					var TandMHoursSearch = getBillableHoursTM(a_emp_Ids, monthArray, int_project_id);
					var onsiteOffsiteSearchResult = getResourcesAllocatedTime(a_emp_Ids, monthNumber, int_project_id);	
				}
                var obj = {};

                var groupBy = function(xs, key) {
                    return xs.reduce(function(rv, x) {
                        (rv[x[key]] = rv[x[key]] || []).push(x);
                        return rv;
                    }, {});
                };
                var groupedByTeam = groupBy(TandMHoursSearch, 'employeelevel');
                var groupByEmpLevel = groupBy(onsiteOffsiteSearchResult, 'employeelevel');

                for (var k = 0; k < grade_array.length; k++) {
                    var tempArray = groupedByTeam[grade_array[k]];
                    ////nlapiLogExecution("DEBUG", "tempArray : ", JSON.stringify(tempArray));
                    if (isArrayNotEmpty(tempArray)) {
                        grade_actual_array.push(grade_array[k]);
                        for (var q = 0; q < tempArray.length; q++) {
                            ////nlapiLogExecution("DEBUG", "element : ", jsonObj.months[month].total[grade_array[k]]);
                            jsonObj.months[month].total[grade_array[k]] = tempArray[q].hours;
                        }
                    }
                }
                // for onsite and offsite 
                for (var k = 0; k < grade_array.length; k++) {
                    var tempArray = groupByEmpLevel[grade_array[k]];
                    //////nlapiLogExecution("ERROR","tempArray onsiteOffsite: ",JSON.stringify(tempArray));
                    if (isArrayNotEmpty(tempArray)) {
                        grade_actual_array.push(grade_array[k]);
                        for (var q = 0; q < tempArray.length; q++) {
                            var offsiteHours = 0;
                            var onsiteHours = 0;
                            if (tempArray[q].onsiteOffsite == "Offsite") {
                                onsiteHours += parseInt(tempArray[q].billiableHours);
                                ////nlapiLogExecution("ERROR", "tempArray onsiteHours: ", onsiteHours);
                                //jsonObj.months[month].Offsite[grade_array[k]]= tempArray[q].hours;
                            } else {
                                offsiteHours += parseInt(tempArray[q].billiableHours);
                                ////nlapiLogExecution("ERROR", "tempArray offsiteHours: ", offsiteHours);
                                //jsonObj.months[month].Offsite[grade_array[k]]= tempArray[q].hours;
                            }
                            //////nlapiLogExecution("DEBUG","element : ",jsonObj.months[month].total[grade_array[k]]);	
                        }
                        jsonObj.months[month].Offsite[grade_array[k]] = offsiteHours;
                        jsonObj.months[month].Onsite[grade_array[k]] = onsiteHours;
                    }
                }
            } else {
                var resourceArray = [];
                jsonObj.months[month].total.revenueFP = periodAmount;
                jsonObj.months[month].Onsite.revenueFP = onsiteAmount;
                jsonObj.months[month].Offsite.revenueFP = offsiteAmount;
                // write code to get the details from resource allocation
                //var resourcesOnsiteOffsiteObj = getEmpDetailForFP(a_emp_Ids,monthNumber,int_project_id);
                ////////nlapiLogExecution("DEBUG","resourcesOnsiteOffsiteObj : ",JSON.stringify(resourcesOnsiteOffsiteObj))
                if (isArrayNotEmpty(a_emp_Ids)) {
                    resourceArray = getResourcesAllocatedTime(a_emp_Ids, monthNumber, int_project_id);
                }

                //////nlapiLogExecution("DEBUG","resourceArray : ",JSON.stringify(resourceArray));
                var groupBy = function(xs, key) {
                    return xs.reduce(function(rv, x) {
                        (rv[x[key]] = rv[x[key]] || []).push(x);
                        return rv;
                    }, {});
                };
                var groupedByEmployeeLevel = groupBy(resourceArray, 'employeelevel');
                ////nlapiLogExecution("Debug", "groupedByEmployeeLevel:", JSON.stringify(groupedByEmployeeLevel));
                for (var k = 0; k < grade_array.length; k++) {
                    var tempArray = groupedByEmployeeLevel[grade_array[k]];
                    if (isArrayNotEmpty(tempArray)) {
                        grade_actual_array.push(grade_array[k]);
                        for (var q = 0; q < tempArray.length; q++) {
                            jsonObj.months[month].total[grade_array[k]] = tempArray[q].hours;
                        }
                    }
                }
            }
        }

    }
    return jsonObj;
}

function JSONToCSVConvertor(JSONData, ReportTitle, ShowLabel) {
    //If JSONData is not an object then JSON.parse will parse the JSON string in an Object
    var arrData = typeof JSONData != 'object' ? JSON.parse(JSONData) : JSONData;

    var CSV = '';
    //Set Report title in first row or line

    CSV += ReportTitle + '\r\n\n';

    //This condition will generate the Label/Header
    if (ShowLabel) {
        var row = "";

        //This loop will extract the label from 1st index of on array
        for (var index in arrData[0]) {

            //Now convert each value to string and comma-seprated
            row += index + ',';
        }

        row = row.slice(0, -1);

        //append Label row with line break
        CSV += row + '\r\n';
    }

    //1st loop is to extract each row
    for (var i = 0; i < arrData.length; i++) {
        var row = "";

        //2nd loop will extract each column and convert it in string comma-seprated
        for (var index in arrData[i]) {
            row += '"' + arrData[i][index] + '",';
        }

        row.slice(0, row.length - 1);

        //add a line break after each row
        CSV += row + '\r\n';
    }

    return CSV;
}

function getSelectedProjects(requestData) {


    if (_logValidation(requestData)) {
        //////nlapiLogExecution('debug', 'projectlist', requestData);
        var project_filter = [
            [
                'internalid', 'anyOf', requestData
            ]
        ];

        var project_search_results = searchRecord('job', null, project_filter,
            [new nlobjSearchColumn("entityid"),
                new nlobjSearchColumn("altname"),
                new nlobjSearchColumn("companyname"),
                new nlobjSearchColumn("internalid"),
                new nlobjSearchColumn("startdate"),
                new nlobjSearchColumn("custentity_projectvalue"),
                new nlobjSearchColumn("custentity_project_currency"),
                new nlobjSearchColumn("custentity_sold_margin_percent"),
                new nlobjSearchColumn("jobbillingtype")

            ]);

        //////nlapiLogExecution('debug', 'project count', project_search_results.length);

        return project_search_results;
    }
}

function createPeriodList(startDate, endDate) {
    var d_startDate = nlapiStringToDate(startDate);
    var d_endDate = nlapiStringToDate(endDate);

    var arrPeriod = [];

    for (var i = 0;; i++) {
        var currentDate = nlapiAddMonths(d_startDate, i);
        if ((_logValidation(getMonthName(currentDate))) && (_logValidation(getMonthStartDate(currentDate))) && (_logValidation(getMonthEndDate(currentDate)))) {} else {
            //////nlapiLogExecution('debug', 'error', '909');
        }
        arrPeriod.push({
            Name: getMonthName(currentDate),
            StartDate: getMonthStartDate(currentDate),
            EndDate: getMonthEndDate(currentDate)
        });

        if (getMonthEndDate(currentDate) >= d_endDate) {
            break;
        }
    }

    var today = new Date();

    // remove the current month
    if (d_endDate.getMonth() >= today.getMonth()) {
        arrPeriod.pop();
    }

    return arrPeriod;
}

function getMonthEndDate(currentDate) {
    return nlapiAddDays(nlapiAddMonths(currentDate, 1), -1);
}

function getMonthStartDate(currentDate) {
    var month = currentDate.getMonth() + 1;
    var year = currentDate.getFullYear();
    var s_startDate = month + "/1/" + year;
    var d_startDate = nlapiStringToDate(s_startDate);
    return d_startDate;
}

function getMonthName(currentDate) {
    var monthNames = ["JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JUL", "AUG",
        "SEP", "OCT", "NOV", "DEC"
    ];
    return monthNames[currentDate.getMonth()] + " " + currentDate.getFullYear();
}
//Get the complete month name
function getMonthCompleteName(month) {
    var s_mont_complt_name = '';
    if (month == 'Jan')
        s_mont_complt_name = 'January';
    if (month == 'Feb')
        s_mont_complt_name = 'February';
    if (month == 'Mar')
        if (month == 'Mar')
            s_mont_complt_name = 'March';
    if (month == 'Apr')
        s_mont_complt_name = 'April';
    if (month == 'May')
        s_mont_complt_name = 'May';
    if (month == 'Jun')
        s_mont_complt_name = 'June';
    if (month == 'Jul')
        s_mont_complt_name = 'July';
    if (month == 'Aug')
        s_mont_complt_name = 'August';
    if (month == 'Sep')
        s_mont_complt_name = 'September';
    if (month == 'Oct')
        s_mont_complt_name = 'October';
    if (month == 'Nov')
        s_mont_complt_name = 'November';
    if (month == 'Dec')
        s_mont_complt_name = 'December';

    return s_mont_complt_name;
}

function _logValidation(value) {
    if (value != null && value.toString() != null && value != '' && value != undefined && value.toString() != undefined && value != 'undefined' && value.toString() != 'undefined' && value.toString() != 'NaN' && value != NaN) {
        return true;
    } else {
        return false;
    }
}

function getResourceCount(proj_list) {
    var resourceallocationSearch = nlapiSearchRecord("resourceallocation", null,
        [
            ["project", "anyof", "138764", "88180", "142968", "65557", "121898", "126336"]
        ],
        [new nlobjSearchColumn("id").setSort(false),
            new nlobjSearchColumn("resource"),
            new nlobjSearchColumn("company"),
            new nlobjSearchColumn("startdate"),
            new nlobjSearchColumn("percentoftime"),
            new nlobjSearchColumn("enddate"),
            new nlobjSearchColumn("employeestatus", "employee", null),
            new nlobjSearchColumn("subsidiary", "employee", null)
        ]);
    if (_logValidation(resourceallocationSearch)) {
        return resourceallocationSearch;
    } else {

        throw "No Active Allocations";
    }
}

function getBilliableDays(projectId,jsonObj) {
    var holidayType = nlapiLookupField("job", projectId, "custentityproject_holiday");
    var customerId = nlapiLookupField("job", projectId, "customer");
    var subsidiaryId = nlapiLookupField("job", projectId, "subsidiary");
    var BillingType = nlapiLookupField("job", projectId, "jobbillingtype");
	//////nlapiLogExecution("AUDIT", "holidayType", holidayType);
    //////nlapiLogExecution("AUDIT", "customerId", customerId);
    //////nlapiLogExecution("AUDIT", "subsidiaryId", subsidiaryId);
    var year = new Date().getFullYear();
    for (var i = 0; i < 12; i++) {
        var month = i + 1;
        //////nlapiLogExecution("ERROR", "month : ", month);
        var weekDays = getWeekdaysInMonth(month, year);
        if (holidayType == "2") {
            var dateArray = getFilter(i);
            var count = getCustomerHolidayCountMonth(customerId, dateArray);
            var billibalDays = parseInt(weekDays) - parseInt(count);
            if (BillingType == "FBM" || BillingType == "FBI") {
                jsonObj.months[month_array[i]].total.billiableDays = billibalDays;
            } else {
                jsonObj.months[month_array[i]].total.billiableDays = billibalDays;
            }
        } else {
            var dateArray = getFilter(i);
            var count = getCompanyHolidayCountMonth(subsidiaryId, dateArray)
            var billibalDays = parseInt(weekDays) - parseInt(count);
            if (BillingType == "FBM" || BillingType == "FBI") {
                jsonObj.months[month_array[i]].total.billiableDays = billibalDays;
                jsonObj.months[month_array[i]].total.billiableHours = (billibalDays * 8);
            } else {
                jsonObj.months[month_array[i]].total.billiableDays = billibalDays;
                jsonObj.months[month_array[i]].total.billiableHours = (billibalDays * 8);
            }
        }
    }
    //var weekDays = getWeekdaysInMonth(month, year);	
}

function weekdaysInYear(year) {
    return weekdayOfYear(new Date(year, 11, 31)) + 1;
}

//Returns the weekday number for the given date relative to the start of the year.
function weekdayOfYear(date) {
    var days = d3.time.dayOfYear(date),
        weeks = days / 7 | 0,
        day0 = (d3.time.year(date).getDay() + 6) % 7,
        day1 = day0 + days - weeks * 7;
    return Math.max(0, days - weeks * 2 -
        (day0 <= 5 && day1 >= 5 || day0 <= 12 && day1 >= 12) // extra saturday
        -
        (day0 <= 6 && day1 >= 6 || day0 <= 13 && day1 >= 13)); // extra sunday
}

function getCustomerHolidayCount(customerId) {
    var customrecordcustomerholidaySearch = nlapiSearchRecord("customrecordcustomerholiday", null,
        [
            ["custrecordholidaydate", "within", "thisyear"],
            "AND",
            ["custrecord13", "anyof", customerId]
        ],
        [

        ]
    );
    if (customrecordcustomerholidaySearch) {
        return customrecordcustomerholidaySearch.length;
    } else {
        return "0"
    }
}

function getCompanyHolidayCount(subsidiary) {
    var customrecord_holidaySearch = nlapiSearchRecord("customrecord_holiday", null,
        [
            ["custrecordsubsidiary", "anyof", subsidiary],
            "AND",
            ["custrecord_date", "within", "thisyear"]
        ],
        [

        ]
    );
    if (customrecord_holidaySearch) {
        return customrecord_holidaySearch.length;
    } else {
        return "0"
    }
}

function convertTextEmpToEmpId(arrEmp) {
    var resultArray = [];
    for (var i = 0; i < arrEmp.length; i++) {
        var employeeSearch = nlapiSearchRecord("employee", null,
            [
                ["entityid", "is", arrEmp[i]]
            ],
            []
        );
        if (employeeSearch) {
            resultArray.push(employeeSearch[0].getId())
        }
    }
    return resultArray;
}

function getBillableHoursTM(empList, month, project) {
    var timesheetSearch = nlapiSearchRecord("timesheet", null,
        [
            ["employee", "anyof", empList],
            "AND",
            ["timeentry.billable", "is", "T"],
            "AND",
            ["timesheetdate", "within", month],
            "AND",
            ["timeentry.customer", "anyof", project]
        ],
        [
            new nlobjSearchColumn("durationdecimal", "timeEntry", "SUM"),
            new nlobjSearchColumn("employee", null, "GROUP"),
            new nlobjSearchColumn("employeestatus", "employee", "GROUP")
        ]
    );
    var employeeTandMArray = [];
    if (timesheetSearch) {
        for (var int = 0; int < timesheetSearch.length; int++) {
            var hours = timesheetSearch[int].getValue("durationdecimal", "timeEntry", "SUM");
            var employee = timesheetSearch[int].getText("employee", null, "GROUP");
            var employeeLevel = timesheetSearch[int].getValue("employeestatus", "employee", "GROUP");
            employeeTandMArray.push({
                "employee": employee,
                "hours": hours,
                "employeelevel": employeeLevel
            });
        }
    }
    return employeeTandMArray
}

function getFilter(monthNumber) {
    var m = parseInt(monthNumber) + 1;
    var y = new Date().getFullYear();
    var lastday = getLastDay(y, monthNumber);
    /*if(m < 10){
    	m = "0"+m;
    }*/
    var returnArray = [];
    returnArray[0] = m + "/01/" + y;
    returnArray[1] = m + "/" + lastday + "/" + y;
    return returnArray;
}

function getLastDay(y, monthNumber) {
    return new Date(y, parseInt(monthNumber) + 1, 0).getDate();
}

function resourcecAllocation(resource, month, project) {
    var startOfMonth, endOfMonth, n_month;
    n_month = parseInt(month);
    var dateArray = getFilter(n_month);
    startOfMonth = dateArray[0];
    endOfMonth = dateArray[1];
    var temp = [];
    var resourceallocationSearch = nlapiSearchRecord("resourceallocation", null,
        [
            ["resource", "anyof", resource],
            "AND",
            ["enddate", "notbefore", startOfMonth],
            "AND",
            ["startdate", "notafter", endOfMonth],
            "AND",
            ["project", "anyof", project]
        ],
        [
            new nlobjSearchColumn("resource"),
            new nlobjSearchColumn("custevent4")
        ]
    );
    if (resourceallocationSearch) {
        for (var int = 0; int < resourceallocationSearch.length; int++) {
            temp.push({
                "employee": resourceallocationSearch[int].getText("resource"),
                "onsiteOffsite": resourceallocationSearch[int].getText("custevent4")
            })
        }
    }
    return temp;
}

function getEmpDetailForFP(resource, month, project) {
    var startOfMonth, endOfMonth, n_month;
    n_month = parseInt(month);
    var dateArray = getFilter(n_month);
    startOfMonth = dateArray[0];
    endOfMonth = dateArray[1];
    var temp = [];
    var resourceallocationSearch = nlapiSearchRecord("resourceallocation", null,
        [
            ["startdate", "notafter", startOfMonth],
            "AND",
            ["enddate", "notbefore", endOfMonth],
            "AND",
            ["resource", "anyof", resource],
            "AND",
            ["project", "anyof", project],
            "AND",
            ["custeventrbillable", "is", "T"]
        ],
        [
            new nlobjSearchColumn("custevent4"),
            new nlobjSearchColumn("resource"),
            new nlobjSearchColumn("employeestatus", "employee", null)
        ]
    );
    return resourceallocationSearch;
    if (resourceallocationSearch) {
        for (var j = 0; j < resourceallocationSearch.length; j++) {
            temp.push({
                "employee": resourceallocationSearch[j].getText("resource"),
                "employeelevel": resourceallocationSearch[j].getValue("employeestatus", "employee", null),
                "onsiteOffsite": resourceallocationSearch[int].getText("custevent4")
            })
        }
    }
    return temp;
}

function getResourcesAllocatedTime(resource, month, project) {
    var startOfMonth, endOfMonth, n_month;
    n_month = parseInt(month);
    var dateArray = getFilter(n_month);
    startOfMonth = dateArray[0];
    endOfMonth = dateArray[1];
    var temp = [];
    var d_todays_date = nlapiStringToDate(nlapiDateToString(new Date()));
    var d_firstDay_mnth = new Date(startOfMonth); //new Date(d_todays_date.getFullYear(), d_todays_date.getMonth(), 1);
    var d_lastDay_mnth = new Date(endOfMonth); //new Date(d_todays_date.getFullYear(), d_todays_date.getMonth() + 1, 0);
    var i_days_in_mnth = getDatediffIndays(d_firstDay_mnth, d_lastDay_mnth);

    /*var s_year_current = d_todays_date.getFullYear();
    var i_month_current = d_todays_date.getMonth();
    var s_month_current = getMonthName_FromMonthNumber(i_month_current);
     */
    var a_filters_allocation = [
        ['startdate', 'onorbefore', d_lastDay_mnth], 'and',
        ['enddate', 'onorafter', d_firstDay_mnth], 'and',
        ['project', 'anyof', project], "and",
        ["resource", "anyof", resource]
    ];

    var a_columns = [
        new nlobjSearchColumn("custevent4"),
        new nlobjSearchColumn("resource"),
        new nlobjSearchColumn("employeestatus", "employee", null),
        new nlobjSearchColumn("startdate"),
        new nlobjSearchColumn("enddate")
    ]

    var a_project_allocation_result = nlapiSearchRecord('resourceallocation', null, a_filters_allocation, a_columns);
    var tempArray = [];
    if (a_project_allocation_result) {
        for (var i_prac_count = 0; i_prac_count < a_project_allocation_result.length; i_prac_count++) {
            CheckMetering();
            var s_emp_level = a_project_allocation_result[i_prac_count].getText("employeestatus", "employee", null);
            var s_employee = a_project_allocation_result[i_prac_count].getText("resource");
            var s_onsiteOffsite = a_project_allocation_result[i_prac_count].getText("custevent4");
            s_emp_level = s_emp_level.toString();
            var s_emp_level_sub_str = s_emp_level.substr(0, 1);
            if (!isNaN(s_emp_level_sub_str)) {
                s_emp_level = s_emp_level_sub_str;
            } else {
                s_emp_level = a_project_allocation_result[i_prac_count].getValue("employeestatus", "employee", null);
            }

            var d_allocation_strt_date = a_project_allocation_result[i_prac_count].getValue('startdate');
            var d_allocation_end_date = a_project_allocation_result[i_prac_count].getValue('enddate');
            d_allocation_strt_date = nlapiStringToDate(d_allocation_strt_date);
            d_allocation_end_date = nlapiStringToDate(d_allocation_end_date);
            //////nlapiLogExecution("AUDIT", "before assignment : ", d_allocation_strt_date);
            //////nlapiLogExecution("AUDIT", "d_lastDay_mnth : ", d_lastDay_mnth);
            /*var s_prcnt_allocated = a_project_allocation_result[i_prac_count].getValue('percentoftime', null, 'sum');
            s_prcnt_allocated = s_prcnt_allocated.toString();
            s_prcnt_allocated = s_prcnt_allocated.split('.');
            s_prcnt_allocated = s_prcnt_allocated[0].toString().split('%');
             */
            if (d_allocation_strt_date < d_firstDay_mnth) {
                d_allocation_strt_date = d_firstDay_mnth;
            }

            if (d_allocation_end_date > d_lastDay_mnth) {
                d_allocation_end_date = d_lastDay_mnth;
            }
            //////nlapiLogExecution("AUDIT", "d_allocation_strt_date : ", d_allocation_strt_date);
            //////nlapiLogExecution("AUDIT", "d_allocation_end_date : ", d_allocation_end_date);
            var i_days_diff = getDatediffIndays(d_allocation_strt_date, d_allocation_end_date);
            //////nlapiLogExecution('audit', 'days allocated:- ' + i_days_diff, 'days in month:- ' + i_days_in_mnth);
            var holidayType = nlapiLookupField("job", project, "custentityproject_holiday");
            var customerId = nlapiLookupField("job", project, "customer");
            var subsidiaryId = nlapiLookupField("job", project, "subsidiary");
            var billiableDays = 0;
            if (holidayType == "2") {
                var count = getCustomerHolidayCountMonth(customerId, dateArray);
                billiableDays = parseFloat(i_days_diff) - parseFloat(count);
            } else {
                var count = getCompanyHolidayCountMonth(subsidiaryId, dateArray);
                billiableDays = parseFloat(i_days_diff) - parseFloat(count);
            }
            var billiableHours = billiableDays * 8;
            var obj = {};
            obj.employee = s_employee;
            obj.employeelevel = s_emp_level;
            obj.onsiteOffsite = s_onsiteOffsite;
            obj.billiableHours = billiableHours;
            tempArray.push(obj);
        }
    }
    return tempArray;
}

function getDatediffIndays(startDate, endDate) {
    var one_day = 1000 * 60 * 60 * 24;
    var fromDate = startDate;
    var toDate = endDate

    //var date1 = nlapiStringToDate(fromDate);

    //var date2 = nlapiStringToDate(toDate);

    var date1 = fromDate;
    var date2 = toDate;

    var date3 = Math.round((date2 - date1) / one_day);

    return (date3 + 1);
}

function getCustomerHolidayCountMonth(customerId, dateArray) {
    var customrecordcustomerholidaySearch = nlapiSearchRecord("customrecordcustomerholiday", null,
        [
            ["custrecordholidaydate", "within", dateArray],
            "AND",
            ["custrecord13", "anyof", customerId]
        ],
        [

        ]
    );
    if (customrecordcustomerholidaySearch) {
        return customrecordcustomerholidaySearch.length;
    } else {
        return "0"
    }
}

function getCompanyHolidayCountMonth(subsidiary, dateArray) {
    var customrecord_holidaySearch = nlapiSearchRecord("customrecord_holiday", null,
        [
            ["custrecordsubsidiary", "anyof", subsidiary],
            "AND",
            ["custrecord_date", "within", dateArray]
        ],
        []
    );
    if (customrecord_holidaySearch) {
        return customrecord_holidaySearch.length;
    } else {
        return "0"
    }
}

function CheckMetering() {
    var CTX = nlapiGetContext();
    var START_TIME = new Date().getTime();
    // want to try to only check metering every 15 seconds
    var remainingUsage = CTX.getRemainingUsage();
    var currentTime = new Date().getTime();
    var timeDifference = currentTime - START_TIME;
    // changing to 15 minutes, should cause little if any impact, but will make runaway scripts faster to kill
    if (remainingUsage < 800 || timeDifference > 900000) {
        START_TIME = new Date().getTime();
        var status = nlapiYieldScript();
        //////nlapiLogExecution('AUDIT', 'STATUS = ', JSON.stringify(status));
    }
}

function getCurrencyExchangeRate() {
    var dataRows = [];
    var column = new Array();
    column[0] = new nlobjSearchColumn('custrecord_pl_currency_exchange_revnue_r');
    column[1] = new nlobjSearchColumn('custrecord_pl_currency_exchange_cost');
    column[2] = new nlobjSearchColumn('custrecord_pl_currency_exchange_year');
    column[3] = new nlobjSearchColumn('custrecord_pl_currency_exchange_month');
    column[4] = new nlobjSearchColumn('custrecord_pl_crc_cost_rate');
    column[5] = new nlobjSearchColumn('custrecord_pl_nok_cost_rate');
    var currencySearch = nlapiSearchRecord('customrecord_pl_currency_exchange_rates', null, null, column);
    //////nlapiLogExecution("ERROR", "", JSON.stringify(currencySearch));
    if (currencySearch) {
        for (var i_indx = 0; i_indx < currencySearch.length; i_indx++) {
            a_dataVal = {
                s_month: currencySearch[i_indx].getText('custrecord_pl_currency_exchange_month'),
                i_year: currencySearch[i_indx].getText('custrecord_pl_currency_exchange_year'),
                rev_rate: currencySearch[i_indx].getValue('custrecord_pl_currency_exchange_revnue_r'),
                cost_rate: currencySearch[i_indx].getValue('custrecord_pl_currency_exchange_cost'),
                crc_cost: currencySearch[i_indx].getValue('custrecord_pl_crc_cost_rate'),
                nok_cost_rate: currencySearch[i_indx].getValue('custrecord_pl_nok_cost_rate')
                //gbp_cost_rate : currencySearch[i_indx].getValue('custrecord_pl_gbp_cost_rate'),
            };
            if (_logValidation(a_dataVal))
                dataRows.push(a_dataVal);
        }
    }
    return dataRows;
}

function getTransactions(columns, filters) {

    columns[0].setSort(false);
    columns[3].setSort(true);
    columns[9].setSort(false);
    filters.push(new nlobjSearchFilter('type', null, 'noneof', 'SalesOrd'));
    filters.push(new nlobjSearchFilter('transactionnumbernumber', null, 'isnotempty'));
    filters.push(new nlobjSearchFilter('type', null, 'noneof', 'PurchOrd'));
    filters.push(new nlobjSearchFilter('trandate', null, 'within', 'thisyear'));
    //filters.push(new nlobjSearchFilter('trandate',null,'onorafter','startofthisyear'));
    //  filters.push(new nlobjSearchFilter('postingperiod',null,'abs','140'));

    filters.push(new nlobjSearchFilter('status', null, 'noneof',
        ['ExpRept:A', 'ExpRept:B', 'ExpRept:C', 'ExpRept:D', 'ExpRept:E', 'ExpRept:H', 'Journal:A', 'VendBill:C',
            'VendBill:D', 'VendBill:E', , 'CustInvc:E', 'CustInvc:D'
        ]));

    var search_results = searchRecord('transaction', null, filters, [
        columns[2], columns[1], columns[0], columns[3], columns[4],
        columns[5], columns[8], columns[9], columns[10], columns[11], columns[12], columns[13], columns[14], columns[15]
    ]);
    return search_results;
}

function getWeekdaysInMonth(month, year) {
    var days = daysInMonth(month, year);
    var weekdays = 0;
    for (var i = 0; i < days; i++) {
        if (isWeekday(year, month, i + 1)) weekdays++;
    }
    return weekdays;
}

function daysInMonth(iMonth, iYear) {
    return 32 - new Date(iYear, iMonth, 32).getDate();
}

function isWeekday(year, month, day) {
    var day = new Date(year, month, day).getDay();
    return day != 0 && day != 6;
}

function createXML(jsonArray) {
    var xmlString = '<?xml version="1.0"?><?mso-application progid="Excel.Sheet"?>';
    xmlString += '<Workbook xmlns="urn:schemas-microsoft-com:office:spreadsheet" ';
    xmlString += 'xmlns:o="urn:schemas-microsoft-com:office:office" ';
    xmlString += 'xmlns:x="urn:schemas-microsoft-com:office:excel" ';
    xmlString += 'xmlns:ss="urn:schemas-microsoft-com:office:spreadsheet" ';
    xmlString += 'xmlns:html="http://www.w3.org/TR/REC-html40">';

    xmlString += '<Worksheet ss:Name="Sheet1">';
    //1st row header
    xmlString += '<Table>';
	     //2nd Row header
        xmlString += '<Row>' +
            '<Cell><Data ss:Type="String"><b></b></Data></Cell>' +
            '<Cell><Data ss:Type="String"><b></b></Data></Cell>';
        for (var i = 0; i < 12; i++) {
            xmlString += '<Cell><Data ss:Type="String"><b> </b></Data></Cell>' +
                '<Cell><Data ss:Type="String"><b></b></Data></Cell>' +
                '<Cell><Data ss:Type="String"><b> ' + month_array[i] + ' </b></Data></Cell>' +
                '<Cell><Data ss:Type="String"><b></b></Data></Cell>'
        }
        xmlString += '</Row>';
        //3rd Row header
        xmlString += '<Row>' +
            '<Cell><Data ss:Type="String"><b>Category</b></Data></Cell>' +
            '<Cell><Data ss:Type="String"><b>Particular</b></Data></Cell>' 
        for (var i = 0; i < 12; i++) {
            xmlString += '<Cell><Data ss:Type="String"><b> </b></Data></Cell>' +
                '<Cell><Data ss:Type="String"><b>  Total  </b></Data></Cell>' +
                '<Cell><Data ss:Type="String"><b>  Offsite  </b></Data></Cell>' +
                '<Cell><Data ss:Type="String"><b>  Onsite </b></Data></Cell>'
        }
        xmlString += '</Row>';
    for (var k = 0; k < jsonArray.length; k++) {
        var jsonObj = jsonArray[k];
        //4th Row header
        xmlString += '<Row>' +
            '<Cell><Data ss:Type="String"><b></b></Data></Cell>' +
            '<Cell><Data ss:Type="String"><b>FP Revenue</b></Data></Cell>' +
			'<Cell><Data ss:Type="String"><b> </b></Data></Cell>';
        for (var i = 0; i < 12; i++) {
            xmlString += '<Cell><Data ss:Type="Number"><b> ' + Math.round(jsonObj.months[month_array[i]].total.revenueFP) + ' </b></Data></Cell>' +
                '<Cell><Data ss:Type="Number"><b> ' + Math.round(jsonObj.months[month_array[i]].Offsite.revenueFP) + ' </b></Data></Cell>' +
                '<Cell><Data ss:Type="Number"><b> ' + Math.round(jsonObj.months[month_array[i]].Onsite.revenueFP) + ' </b></Data></Cell>' +
                '<Cell><Data ss:Type="String"> </Data></Cell>'
        }
        xmlString += '</Row>';
        //5th Row header
        xmlString += '<Row>' +
            '<Cell><Data ss:Type="String"><b></b></Data></Cell>' +
            '<Cell><Data ss:Type="String"><b>T&M Revenue</b></Data></Cell>' + '<Cell><Data ss:Type="String"><b> </b></Data></Cell>';
        for (var i = 0; i < 12; i++) {
            xmlString += '<Cell><Data ss:Type="Number"><b> ' + Math.round(jsonObj.months[month_array[i]].total.revenueTM) + ' </b></Data></Cell>' +
                '<Cell><Data ss:Type="Number"><b> ' + Math.round(jsonObj.months[month_array[i]].Offsite.revenueTM) + ' </b></Data></Cell>' +
                '<Cell><Data ss:Type="Number"><b> ' + Math.round(jsonObj.months[month_array[i]].Onsite.revenueTM) + ' </b></Data></Cell>' +
                '<Cell><Data ss:Type="String"> </Data></Cell>'
        }
        xmlString += '</Row>';
        //6th Row header
        xmlString += '<Row>' +
            '<Cell><Data ss:Type="String"><b></b></Data></Cell>' +
            '<Cell><Data ss:Type="String"><b>Billiable Days </b></Data></Cell>' + '<Cell><Data ss:Type="String"><b> </b></Data></Cell>';
        for (var i = 0; i < 12; i++) {
            xmlString += '<Cell><Data ss:Type="Number"><b> ' + jsonObj.months[month_array[i]].total.billiableDays + ' </b></Data></Cell>' +
                '<Cell><Data ss:Type="Number"><b> ' + jsonObj.months[month_array[i]].Offsite.billiableDays + ' </b></Data></Cell>' +
                '<Cell><Data ss:Type="Number"><b> ' + jsonObj.months[month_array[i]].Onsite.billiableDays + ' </b></Data></Cell>' +
                '<Cell><Data ss:Type="String"> </Data></Cell>'
        }
        xmlString += '</Row>';
        //7th Row header
        xmlString += '<Row>' +
            '<Cell><Data ss:Type="String"><b></b></Data></Cell>' +
            '<Cell><Data ss:Type="String"><b>Billiable Hours </b></Data></Cell>' + '<Cell><Data ss:Type="String"><b> </b></Data></Cell>';
        for (var i = 0; i < 12; i++) {
            xmlString += '<Cell><Data ss:Type="Number"><b> ' + jsonObj.months[month_array[i]].total.billiableHours + ' </b></Data></Cell>' +
                '<Cell><Data ss:Type="Number"><b> ' + jsonObj.months[month_array[i]].Offsite.billiableHours + ' </b></Data></Cell>' +
                '<Cell><Data ss:Type="Number"><b> ' + jsonObj.months[month_array[i]].Onsite.billiableHours + ' </b></Data></Cell>' +
                '<Cell><Data ss:Type="String"> </Data></Cell>'
        }
        xmlString += '</Row>';

        //8th Row 
        //////nlapiLogExecution("ERROR","Before unique",JSON.stringify(grade_actual_array))
        grade_actual_array = _.uniq(grade_actual_array);
        //////nlapiLogExecution("ERROR","After unique",JSON.stringify(grade_actual_array))
        for (var xy = 0; xy < grade_actual_array.length; xy++) {
            xmlString += '<Row>' +
                '<Cell><Data ss:Type="String"><b></b></Data></Cell>' +
                '<Cell><Data ss:Type="String"><b>Level' + grade_actual_array[xy] + '</b></Data></Cell>' + '<Cell><Data ss:Type="String"><b> </b></Data></Cell>';
            for (var i = 0; i < 12; i++) {
                xmlString += '<Cell><Data ss:Type="Number"><b> ' + jsonObj.months[month_array[i]].total[grade_actual_array[xy]] + ' </b></Data></Cell>' +
                    '<Cell><Data ss:Type="Number"><b>' + jsonObj.months[month_array[i]].Offsite[grade_actual_array[xy]] + ' </b></Data></Cell>' +
                    '<Cell><Data ss:Type="Number"><b> ' + jsonObj.months[month_array[i]].Onsite[grade_actual_array[xy]] + '</b></Data></Cell>' +
                    '<Cell><Data ss:Type="String"> </Data></Cell>'
            }
            xmlString += '</Row>';
        }
    }
	xmlString += '</Table></Worksheet></Workbook>';
var excelFile = nlapiCreateFile('KPIReport.xls', 'EXCEL', nlapiEncrypt(xmlString, 'base64'));
nlapiSendEmail(442, 144836, "Template", "Hello \n Please find attached KPI Report! \n Thanks & Regards \n Information System", null, null, null, excelFile);
}

function getProjects() {
    var temp = [];
    var jobSearch = nlapiSearchRecord("job", null,
        [
            ["status", "anyof", "2"],
            "AND",
            ["custentity_project_allocation_category", "anyof", "1"],
            "AND",
            ["type", "anyof", "2"],
            "AND",
            ["custentity_practice","anyof","528","532","529","535","534","533","527"]
        ],
        []
    );
    if (jobSearch) {
        for (var i = 0; i < jobSearch.length; i++) {
            temp[i] = jobSearch[i].getId();
        }
    }
    return temp;
}