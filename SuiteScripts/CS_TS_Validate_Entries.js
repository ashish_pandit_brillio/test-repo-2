/**
 * Module Description
 * 
 * Check if all the working days entries have been provided for a given week
 * while the timesheet is being submitted.
 * Test Issue
 * Version Date Author Remarks 1.00 03 Feb 2015 Nitish Mishra
 * Version Date Author Remarks 1.01 16 Mar 2020 Praveena Madem Changed Internalid of time sheet and timebill records,fields
 */




// prevent user from entering 0 in the timesheet
function validateTimeEntryForZero(type, name, linenum) {
	try {

		if (type == 'timeitem'
		        && (name == 'hours0' || name == 'hours1'
		                || name == 'hours2'
		                || name == 'hours3'
		                || name == 'hours4'
		                || name == 'hours5' || name == 'hours6')) {
			var hours = nlapiGetCurrentLineItemValue(type, name);

			if (hours) {
				hours = hours.replace(":", ".");
				hours = parseFloat(hours);

				if (hours > 0) {
					return true;
				} else {
					alert('You cannot enter 0 hours');
					nlapiSetCurrentLineItemValue(type, name, '', true, true);
				}
			}
		}
	} catch (err) {
		alert(err.message);
	}
}
/*
function clientPageInit(type){
   try{
	   if(type == 'create' || type == 'edit'){
		   alert('Timesheet submission through NetSuite will be blocked from 1st July 2020.Try submitting your timesheets in OnTheGO from NOW.');
	   }
	   	var main_div = document.getElementsByClassName("uir-page-title")[0];
		var newDiv = document.createElement("div");
		newDiv.innerHTML = "<span style='color: black;font-size: 14px;float: left;border: 1px solid;padding: 5px;background-color: beige;'><b>Timesheet submission through NetSuite will be blocked from 1st July 2020. Try submitting your timesheets in OnTheGO from NOW </b></span>";
		main_div.appendChild(newDiv);
	   
   }
   catch(e){
		nlapiLogExecution('ERROR','Process Error',e);
	}
}
*/
// check if all the entries are there from monday to friday
function validateMondayToFriday() {

	try {
		var i_line_count = nlapiGetLineItemCount('timeitem');
		var i_employee = nlapiGetFieldValue('employee');
		
		var linkURL = nlapiResolveURL('SUITELET', 'customscript_sut_getemployeedetails', 'customdeploy1', null);
            var param = '&i_employee=' + i_employee;
            var finalURL = linkURL + param;
            //var response = new Array();
			//alert('Test Alert');
            var response = nlapiRequestURL(finalURL, null, null, null);
            var status1 = response.getBody();
			status1 = JSON.parse(status1);
            var response1 = JSON.parse(JSON.stringify(status1));
			//alert('response1'+response1);
			var d_employee_hire_date = status1[0].d_employee_hire_date;
			var d_employee_last_working_date = status1[0].d_employee_last_working_date;
			
		// array for subrecords
		//var a_days = [ 'sunday', 'monday', 'tuesday', 'wednesday', 'thursday','friday', 'saturday' ];
       var a_days=['timebill0','timebill1','timebill2','timebill3','timebill4','timebill5','timebill6'];
		// array for validating the subrecords
		var a_days_filled = [ true, false, false, false, false, false, true ];

		var d_timesheet_start_date = nlapiStringToDate(nlapiGetFieldValue('startdate'));
		var d_timesheet_end_date = nlapiStringToDate(nlapiGetFieldValue('enddate'));
		

		if (d_employee_hire_date > d_timesheet_start_date) {
			var i_hire_day = d_employee_hire_date.getDay();

			for (var i = 0; i < i_hire_day; i++) {
				a_days_filled[i] = true;
			}
		}

		nlapiLogExecution('debug', 'validateMondayToFriday--a_days_filled', JSON
		        .stringify(a_days_filled));

		if (isNotEmpty(d_employee_last_working_date)) {

			if (d_timesheet_end_date > d_employee_last_working_date) {
				var i_termination_day = d_employee_last_working_date.getDay();

				for (var i = i_termination_day; i < 7; i++) {
					a_days_filled[i] = true;
				}
			}
		}

		for (var i_line_indx = 1; i_line_indx <= i_line_count; i_line_indx++) {
			nlapiSelectLineItem('timeitem', i_line_indx);

			for (var i_day_indx = 0; i_day_indx < a_days_filled.length; i_day_indx++) {
				var sub_record_name = a_days[i_day_indx];
				//var o_sub_record_view = nlapiViewCurrentLineItemSubrecord(
				var o_sub_record_view = nlapiGetCurrentLineItemValue(
				        'timeitem', sub_record_name);
               nlapiLogExecution('DEBUG','o_sub_record_view',o_sub_record_view);
				if (o_sub_record_view) {

					if (!a_days_filled[i_day_indx]) {
						a_days_filled[i_day_indx] = true;
					}
				}
			}
		}

		for (var i_day_indx = 0; i_day_indx < a_days_filled.length; i_day_indx++) {

			if (!a_days_filled[i_day_indx]) {
				return confirm('Some of the time entries are blank.\nDo you want to submit the timesheet ?');
			}
		}
	} catch (err) {
		alert(err.message);
		return false;
	}

	return true;
}

function saveRecordA() {
	try {
		
		var i_line_count = nlapiGetLineItemCount('timeitem');
nlapiLogExecution('debug', 'START saveRecordA', i_line_count);
		// array for subrecords
		var a_days=['timebill0','timebill1','timebill2','timebill3','timebill4','timebill5','timebill6'];
	  
		// array for validating the subrecords
		var a_days_filled = [ true, false, false, false, false, false, true ];

		var timesheet_temp = [];

		for (var i_line_indx = 1; i_line_indx <= i_line_count; i_line_indx++) {
			nlapiSelectLineItem('timeitem', i_line_indx);
			timesheet_temp.push({
			    Line : i_line_indx,
			    Project : null,
			    BillingType : null,
			    Days : []
			});

			for (var i_day_indx = 0; i_day_indx < a_days_filled.length; i_day_indx++) {
				var sub_record_name = a_days[i_day_indx];
				nlapiLogExecution('debug', 'sub_record_name', sub_record_name);
				var o_sub_record_view = nlapiViewCurrentLineItemSubrecord(
				        'timeitem', sub_record_name);

				if (o_sub_record_view) {

					if (!timesheet_temp[i_line_indx - 1].Project) {
						var projectId = o_sub_record_view
						        .getFieldValue('customer');
						var projectBillingType = nlapiLookupField('job',
						        projectId, 'jobbillingtype');
						timesheet_temp[i_line_indx - 1].Project = projectId;
						timesheet_temp[i_line_indx - 1].BillingType = projectBillingType;
						// alert(projectBillingType);

						if (projectBillingType == 'TM') {
							timesheet_temp[i_line_indx - 1].Days = [ null,
							        null, null, null, null, null, null ];
						} else if (projectBillingType == 'FBI') {
							timesheet_temp[i_line_indx - 1].Days = [ 1, 1, 1,
							        1, 1, 1, 1 ];
						} else if (projectBillingType == 'FBM') {
							timesheet_temp[i_line_indx - 1].Days = [ 1, 1, 1,
							        1, 1, 1, 1 ];
						}
					}

					// 1 means entry exists for that day
					timesheet_temp[i_line_indx - 1].Days[i_day_indx] = 2;
				}
			}
		}

		nlapiLogExecution('debug', 'JSON data', JSON.stringify(timesheet_temp));
		// alert(JSON.stringify(timesheet_temp));

		// ----------------

		var o_employee_details = nlapiLookupField('employee',
		        nlapiGetFieldValue('employee'),
		        [ 'hiredate', 'custentity_lwd' ]);

		// array for subrecords
		 var a_days=['timebill0','timebill1','timebill2','timebill3','timebill4','timebill5','timebill6'];
		  nlapiLogExecution('debug', 'JSON data', JSON.stringify(timesheet_temp));
		// array for validating the subrecords
		var a_days_filled = [ true, false, false, false, false, false, true ];
 nlapiLogExecution('debug', 'JSON data', JSON.stringify(timesheet_temp));
		var d_timesheet_start_date = nlapiStringToDate(nlapiGetFieldValue('startdate'));
		var d_timesheet_end_date = nlapiStringToDate(nlapiGetFieldValue('enddate'));
		var d_employee_hire_date = nlapiStringToDate(o_employee_details.hiredate);
		var d_employee_last_working_date = nlapiStringToDate(o_employee_details.custentity_lwd);

		if (d_employee_hire_date > d_timesheet_start_date) {
			var i_hire_day = d_employee_hire_date.getDay();

			for (var i = 0; i < i_hire_day; i++) {
				a_days_filled[i] = true;
			}
		}

		nlapiLogExecution('debug', 'a_days_filled', JSON.stringify(a_days_filled));

		if (isNotEmpty(d_employee_last_working_date)) {

			if (d_timesheet_end_date > d_employee_last_working_date) {
				var i_termination_day = d_employee_last_working_date.getDay();

				for (var i = i_termination_day; i < 7; i++) {
					a_days_filled[i] = true;
				}
			}
		}
nlapiLogExecution('debug', 'Stage2-------latstage', JSON.stringify(a_days_filled));
		for (var i_line_indx = 0; i_line_indx < i_line_count; i_line_indx++) {

			for (var i_day_indx = 0; i_day_indx < a_days_filled.length; i_day_indx++) {

				if (timesheet_temp[i_line_indx].Days[i_day_indx]) {

					if (!a_days_filled[i_day_indx]) {
						a_days_filled[i_day_indx] = true;
					}
				}
			}
		}
nlapiLogExecution('debug', 'before alert a_days_filled-------latstage', JSON.stringify(a_days_filled));
		for (var i_day_indx = 0; i_day_indx < a_days_filled.length; i_day_indx++) {

			if (!a_days_filled[i_day_indx]) {
				alert('Please provide timesheet entries from monday to friday.');
				return false;
			}
		}
	} catch (err) {
		alert(err.message);
		return false;
	}

	return true;
}







/*
// prevent user from entering 0 in the timesheet
function validateTimeEntryForZero(type, name, linenum) {
	try {

		if (type == 'timeitem'
		        && (name == 'timeentry_hours_0' || name == 'timeentry_hours_1'
		                || name == 'timeentry_hours_2'
		                || name == 'timeentry_hours_3'
		                || name == 'timeentry_hours_4'
		                || name == 'timeentry_hours_5' || name == 'timeentry_hours_6')) {
			var hours = nlapiGetCurrentLineItemValue(type, name);

			if (hours) {
				hours = hours.replace(":", ".");
				hours = parseFloat(hours);

				if (hours > 0) {
					return true;
				} else {
					alert('You cannot enter 0 hours');
					nlapiSetCurrentLineItemValue(type, name, '', true, true);
				}
			}
		}
	} catch (err) {
		alert(err.message);
	}
}

// check if all the entries are there from monday to friday
function validateMondayToFriday() {

	try {
		var i_line_count = nlapiGetLineItemCount('timeitem');
		var i_employee = nlapiGetFieldValue('employee');
		
		var linkURL = nlapiResolveURL('SUITELET', 'customscript_sut_getemployeedetails', 'customdeploy1', null);
            var param = '&i_employee=' + i_employee;
            var finalURL = linkURL + param;
            //var response = new Array();
			//alert('Test Alert');
            var response = nlapiRequestURL(finalURL, null, null, null);
            var status1 = response.getBody();
			status1 = JSON.parse(status1);
            var response1 = JSON.parse(JSON.stringify(status1));
			//alert('response1'+response1);
			var d_employee_hire_date = status1[0].d_employee_hire_date;
			var d_employee_last_working_date = status1[0].d_employee_last_working_date;
			
		// array for subrecords
		var a_days = [ 'sunday', 'monday', 'tuesday', 'wednesday', 'thursday',
		        'friday', 'saturday' ];

		// array for validating the subrecords
		var a_days_filled = [ true, false, false, false, false, false, true ];

		var d_timesheet_start_date = nlapiStringToDate(nlapiGetFieldValue('startdate'));
		var d_timesheet_end_date = nlapiStringToDate(nlapiGetFieldValue('enddate'));
		

		if (d_employee_hire_date > d_timesheet_start_date) {
			var i_hire_day = d_employee_hire_date.getDay();

			for (var i = 0; i < i_hire_day; i++) {
				a_days_filled[i] = true;
			}
		}

		nlapiLogExecution('debug', 'a_days_filled', JSON
		        .stringify(a_days_filled));

		if (isNotEmpty(d_employee_last_working_date)) {

			if (d_timesheet_end_date > d_employee_last_working_date) {
				var i_termination_day = d_employee_last_working_date.getDay();

				for (var i = i_termination_day; i < 7; i++) {
					a_days_filled[i] = true;
				}
			}
		}

		for (var i_line_indx = 1; i_line_indx <= i_line_count; i_line_indx++) {
			nlapiSelectLineItem('timeitem', i_line_indx);

			for (var i_day_indx = 0; i_day_indx < a_days_filled.length; i_day_indx++) {
				var sub_record_name = a_days[i_day_indx];
				var o_sub_record_view = nlapiViewCurrentLineItemSubrecord(
				        'timeitem', sub_record_name);

				if (o_sub_record_view) {

					if (!a_days_filled[i_day_indx]) {
						a_days_filled[i_day_indx] = true;
					}
				}
			}
		}

		for (var i_day_indx = 0; i_day_indx < a_days_filled.length; i_day_indx++) {

			if (!a_days_filled[i_day_indx]) {
				return confirm('Some of the time entries are blank.\nDo you want to submit the timesheet ?');
			}
		}
	} catch (err) {
		alert(err.message);
		return false;
	}

	return true;
}

function saveRecordA() {
	try {
		var i_line_count = nlapiGetLineItemCount('timeitem');

		// array for subrecords
		var a_days = [ 'sunday', 'monday', 'tuesday', 'wednesday', 'thursday',
		        'friday', 'saturday' ];

		// array for validating the subrecords
		var a_days_filled = [ true, false, false, false, false, false, true ];

		var timesheet_temp = [];

		for (var i_line_indx = 1; i_line_indx <= i_line_count; i_line_indx++) {
			nlapiSelectLineItem('timeitem', i_line_indx);
			timesheet_temp.push({
			    Line : i_line_indx,
			    Project : null,
			    BillingType : null,
			    Days : []
			});

			for (var i_day_indx = 0; i_day_indx < a_days_filled.length; i_day_indx++) {
				var sub_record_name = a_days[i_day_indx];
				var o_sub_record_view = nlapiViewCurrentLineItemSubrecord(
				        'timeitem', sub_record_name);

				if (o_sub_record_view) {

					if (!timesheet_temp[i_line_indx - 1].Project) {
						var projectId = o_sub_record_view
						        .getFieldValue('customer');
						var projectBillingType = nlapiLookupField('job',
						        projectId, 'jobbillingtype');
						timesheet_temp[i_line_indx - 1].Project = projectId;
						timesheet_temp[i_line_indx - 1].BillingType = projectBillingType;
						// alert(projectBillingType);

						if (projectBillingType == 'TM') {
							timesheet_temp[i_line_indx - 1].Days = [ null,
							        null, null, null, null, null, null ];
						} else if (projectBillingType == 'FBI') {
							timesheet_temp[i_line_indx - 1].Days = [ 1, 1, 1,
							        1, 1, 1, 1 ];
						} else if (projectBillingType == 'FBM') {
							timesheet_temp[i_line_indx - 1].Days = [ 1, 1, 1,
							        1, 1, 1, 1 ];
						}
					}

					// 1 means entry exists for that day
					timesheet_temp[i_line_indx - 1].Days[i_day_indx] = 2;
				}
			}
		}

		nlapiLogExecution('debug', 'JSON data', JSON.stringify(timesheet_temp));
		// alert(JSON.stringify(timesheet_temp));

		// ----------------

		var o_employee_details = nlapiLookupField('employee',
		        nlapiGetFieldValue('employee'),
		        [ 'hiredate', 'custentity_lwd' ]);

		// array for subrecords
		var a_days = [ 'sunday', 'monday', 'tuesday', 'wednesday', 'thursday',
		        'friday', 'saturday' ];

		// array for validating the subrecords
		var a_days_filled = [ true, false, false, false, false, false, true ];

		var d_timesheet_start_date = nlapiStringToDate(nlapiGetFieldValue('startdate'));
		var d_timesheet_end_date = nlapiStringToDate(nlapiGetFieldValue('enddate'));
		var d_employee_hire_date = nlapiStringToDate(o_employee_details.hiredate);
		var d_employee_last_working_date = nlapiStringToDate(o_employee_details.custentity_lwd);

		if (d_employee_hire_date > d_timesheet_start_date) {
			var i_hire_day = d_employee_hire_date.getDay();

			for (var i = 0; i < i_hire_day; i++) {
				a_days_filled[i] = true;
			}
		}

		nlapiLogExecution('debug', 'a_days_filled', JSON
		        .stringify(a_days_filled));

		if (isNotEmpty(d_employee_last_working_date)) {

			if (d_timesheet_end_date > d_employee_last_working_date) {
				var i_termination_day = d_employee_last_working_date.getDay();

				for (var i = i_termination_day; i < 7; i++) {
					a_days_filled[i] = true;
				}
			}
		}

		for (var i_line_indx = 0; i_line_indx < i_line_count; i_line_indx++) {

			for (var i_day_indx = 0; i_day_indx < a_days_filled.length; i_day_indx++) {

				if (timesheet_temp[i_line_indx].Days[i_day_indx]) {

					if (!a_days_filled[i_day_indx]) {
						a_days_filled[i_day_indx] = true;
					}
				}
			}
		}

		for (var i_day_indx = 0; i_day_indx < a_days_filled.length; i_day_indx++) {

			if (!a_days_filled[i_day_indx]) {
				alert('Please provide timesheet entries from monday to friday.');
				return false;
			}
		}
	} catch (err) {
		alert(err.message);
		return false;
	}

	return true;
}

*/