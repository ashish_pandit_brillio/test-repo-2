							
// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
/*
   	Script Name : FUEL | UES Delivery Anchor Notification 
	Author      : Ashish Pandit
	Date        : 09 Oct 2019
    Description : User Event to update MariaDB on Field change of Delivery Anchor   


	Script Modification Log:

	-- Date --			-- Modified By --				--Requested By--				-- Description --

Below is a summary of the process controls enforced by this script file.  The control logic is described
more fully, below, in the appropriate function headers and code blocks.


     BEFORE LOAD
		- beforeLoadRecord(type)



     BEFORE SUBMIT
		- beforeSubmitRecord(type)


     AFTER SUBMIT
		- afterSubmitRecord(type)



     SUB-FUNCTIONS
		- The following sub-functions are called by the above core functions in order to maintain code
            modularization:

               - NOT USED

*/
}
// END SCRIPT DESCRIPTION BLOCK  ====================================

function afterSubmitCheckFields(type)
{
	nlapiLogExecution('Debug','type ',type);
	if(type == 'create')
	{
		try
		{
			var recordObject = nlapiLoadRecord(nlapiGetRecordType(), nlapiGetRecordId());
			
			var emailId = nlapiLookupField('employee',recordObject.getFieldValue('custrecord_spoc'),'email');
			var practiceName = recordObject.getFieldText('custrecord_practice');
			var practiceInternalId = recordObject.getFieldValue('custrecord_practice');
			
			
			var body = {};
			var method;
			method = "POST";
			body.emailId = emailId;
			body.practiceName = practiceName;
			body.practiceInternalId = practiceInternalId;
						
			var url = "https://fuelnode1.azurewebsites.net/practice_spoc";
			nlapiLogExecution('debug','body  ',JSON.stringify(body));
			body = JSON.stringify(body);
			var response = call_node(url,body,method);
			nlapiLogExecution("DEBUG", "response Create Mode: ", JSON.stringify(response));
		}
		catch(error)
		{
			nlapiLogExecution('Debug','Error ',error);
		}
	}
	if(type == 'edit' || type == 'xedit')
	{
		try
		{
			var oldRecord = nlapiGetOldRecord();
			
			var Old_employee = oldRecord.getFieldValue('custrecord_spoc');
			var Old_practice = oldRecord.getFieldValue('custrecord_practice');
			
			
			var recordObject = nlapiLoadRecord(nlapiGetRecordType(), nlapiGetRecordId());
		
			var new_employee = recordObject.getFieldValue('custrecord_spoc');
			var emailId = nlapiLookupField('employee',new_employee,'email')
			var new_practiceText = recordObject.getFieldText('custrecord_practice');
			var new_practice = recordObject.getFieldValue('custrecord_practice');
			
			if(Old_employee != new_employee || Old_practice != new_practice)
			{
				var body = {};
				var method;
				method = "POST";
				body.emailId = emailId;
				body.practiceName = new_practiceText;
				body.practiceInternalId = new_practice;
				
				
				var url = "https://fuelnode1.azurewebsites.net/practice_spoc";
				nlapiLogExecution('debug','body  ',JSON.stringify(body));
				body = JSON.stringify(body);
				var response = call_node(url,body,method);
				nlapiLogExecution("DEBUG", "response Create Mode: ", JSON.stringify(response));
			}
		}
		catch(error)
		{
			nlapiLogExecution('Debug','Error ',error);
		}
	}
}
function beforeSubmitCheckFields(type)
{
	 if(type == 'delete')
	{
		try
		{
			var recordObject = nlapiLoadRecord(nlapiGetRecordType(), nlapiGetRecordId());
			
			var emailId = nlapiLookupField('employee',recordObject.getFieldValue('custrecord_spoc'),'email');
			var body = {};
			var method = "DELETE";
			/*
			var i_deliveryAnchor = getEmpIds(JSON.stringify(i_user));
			var emailArray = new Array();
			if(i_deliveryAnchor.length > 0)
			{
				for(var i = 0;i<i_deliveryAnchor.length;i++)
				{
					emailArray.push(nlapiLookupField("employee", parseInt(i_deliveryAnchor[i]), "email"));
				}
			}
			*/
			//body.regionName = nlapiGetFieldValue('name'); 
			var url = "https://fuelnode1.azurewebsites.net/practice_spoc/"+emailId;
			//body = JSON.stringify(body);
			var response = call_node(url,body,method);
			nlapiLogExecution('debug','body  ',JSON.stringify(body));
			nlapiLogExecution("DEBUG", "response Delete Mode: ", JSON.stringify(response));
		}
		catch(error)
		{
			nlapiLogExecution('Debug','Error ',error);
		}
	}
}
// END BEFORE LOAD ====================================================


function _logValidation(value) 
{
 if(value != null && value!= 'null' && value != '' && value != undefined && value != undefined && value != 'undefined' && value.toString() != 'NaN' && value != NaN) 
 {
  return true;
 }
 else 
 { 
  return false;
 }
}
// END FUNCTION =====================================================

/************************************************/
function getEmpIds(s_employee)
{
	var resultArray = new Array();
	if(_logValidation(s_employee))
	{
		nlapiLogExecution('Debug','s_employee in function',s_employee);
		//var temp = s_employee.split(',');
		for(var i=0; i<s_employee.length;i++)
		{
			resultArray.push(s_employee[i]);
		}
	}
	return resultArray;
}