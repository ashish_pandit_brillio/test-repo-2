/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       16 Apr 2015     amol.sahijwani
 *
 */

/**
 * @param {String} recType Record type internal id
 * @param {Number} recId Record internal id
 * @returns {Void}
 */
function massUpdate(recType, recId) {
	
	var rec	=	nlapiLoadRecord('invoice', recId);
	
	var invoice_number	=	rec.getFieldValue('tranid');
	var invoice_date	=	rec.getFieldValue('trandate');
	var invoice_billing_start_date	=	rec.getFieldValue('custbody_billfrom');
	var invoice_billing_end_date	=	rec.getFieldValue('custbody_billto');
	var i_billing_item_count	=	rec.getLineItemCount('time');
	var f_invoice_amount	=	rec.getFieldValue('total');
	
	var str_csv_line	=	'Invoice Internal ID, Invoice Number, Invoice Date, Billing Start Date, Billing End Date, Employee, Quantity, Bill Rate, TimeEntry Date, Invoice Amount\n';
	
	var arrExistingRecords	=	getExistingRecords(recId);
	
	var arrEmployeeList = [];
	
	var newRecords = [];
	
	for(var i = 1; i <= i_billing_item_count; i++)
		{
			var apply = rec.getLineItemValue('time', 'apply', i);
			if(apply == 'T')
				{
					var employee = rec.getLineItemValue('time', 'employee', i);
					var timeentry_id = rec.getLineItemValue('time', 'doc', i);
					
					if(arrExistingRecords.indexOf(timeentry_id) == -1)
						{
							newRecords.push(timeentry_id);
							
							if(arrEmployeeList.indexOf(employee) == -1)
							{
								arrEmployeeList.push(employee);
							}
						}
				}
		}
	
	// If no new records, return
	if(newRecords.length == 0)
		{
			return;
		}
	nlapiLogExecution('ERROR', 'New Records: ' + recId, newRecords.length);
	var raFilters = new Array();
	raFilters[0]	=	new nlobjSearchFilter('resource', null, 'anyof', arrEmployeeList);
	
	var raColumns = new Array();
	raColumns[0]	=	new nlobjSearchColumn('startdate');
	raColumns[1]	=	new nlobjSearchColumn('enddate');
	raColumns[2]	=	new nlobjSearchColumn('resource');
	raColumns[3]	=	new nlobjSearchColumn('internalid');
	raColumns[4]	=	new nlobjSearchColumn('company');
	
	var resource_allocations = nlapiSearchRecord('resourceallocation', null, raFilters, raColumns);
	
	var arrRa = new Array();
	for(var i = 0; resource_allocations != null && i < resource_allocations.length; i++)
		{
			var startDate = new Date(resource_allocations[i].getValue('startdate'));
			var endDate = new Date(resource_allocations[i].getValue('enddate'));
			var resource = resource_allocations[i].getValue('resource');
			var ra_id	= resource_allocations[i].getValue('internalid');
			var project = resource_allocations[i].getValue('company');
			
			arrRa.push({'start_date':startDate, 'end_date':endDate, 'resource':resource, 'id':ra_id, 'project': project});
		}
	
	for(var i = 1; i <= i_billing_item_count; i++)
		{
			var apply = rec.getLineItemValue('time', 'apply', i);
			if (apply == 'T') 
			{
				var qty = rec.getLineItemValue('time', 'qty', i);
				var employee = rec.getLineItemValue('time', 'employee', i);
				var billed_date = rec.getLineItemValue('time', 'billeddate', i);
				var bill_rate = rec.getLineItemValue('time', 'rate', i);
				var timeentry_id = rec.getLineItemValue('time', 'doc', i);
				var project = rec.getFieldValue('job');
				var resource_allocation_id = getResourceAllocationId(arrRa, new Date(billed_date), employee, project);
				
				if(newRecords.indexOf(timeentry_id) != -1)
					{
						try
						{
							var data_record	=	nlapiCreateRecord('customrecord_invoice_timesheet_data');
							data_record.setFieldValue('custrecord_temp_data_table_employee_id', employee);
							data_record.setFieldValue('custrecord_temp_data_table_duration', qty);
							data_record.setFieldValue('custrecord_temp_data_table_bill_rate', bill_rate);data_record.setFieldValue('custrecord_temp_data_table_entry_date', billed_date);
							data_record.setFieldValue('custrecord_tdt_invoice_id', recId);
							data_record.setFieldValue('custrecord_tdt_time_entry', timeentry_id);
							data_record.setFieldValue('custrecord_tdt_resource_allocation', resource_allocation_id);
							data_record.setFieldValue('custrecord_tdt_project', project);
							nlapiSubmitRecord(data_record);	
						}
						catch(e)
						{
							nlapiLogExecution('ERROR', 'Error: ' + recId, e.message);if(e.message == 'Script Execution Usage Limit Exceeded'){return;}
						}
					}
				
				str_csv_line += [recId, invoice_number, invoice_date,invoice_billing_start_date, invoice_billing_end_date,
					employee, qty, bill_rate, billed_date, f_invoice_amount].toString() + '\n';
			}
		}
	
	/*var objFile	=	nlapiLoadFile(20017);
	
	var newFile = nlapiCreateFile('invoice_data.csv', 'CSV', objFile.getValue() + str_csv_line); 
	newFile.setFolder(6213);//csvFolderId); 
	newFile.setEncoding('UTF-8'); 
	nlapiSubmitFile(newFile);*/
	
	
	//var newObjFile	=	new nlob
}

function getResourceAllocationId(arrAllocations, dtTimeEntry, i_employee_id, project_id)
{
	for(var i = 0; i < arrAllocations.length; i++)
		{
			if(dtTimeEntry >= arrAllocations[i].start_date && dtTimeEntry <= arrAllocations[i].end_date && arrAllocations[i].project == project_id && arrAllocations[i].resource == i_employee_id)
				{
					return arrAllocations[i].id;
				}
		}
	
	return null;
}

// Check if record exists
function getExistingRecords(invoice_id)
{
	var search_filter = new Array();
	search_filter[0] = new nlobjSearchFilter('custrecord_tdt_invoice_id', null, 'anyof', invoice_id);
	
	var search_columns = new Array();
	search_columns[0] = new nlobjSearchColumn('custrecord_tdt_time_entry');
	
	var search_results = searchRecord('customrecord_invoice_timesheet_data', null, search_filter, search_columns);
	
	var arrExistingData	=	new Array();
	for(var i = 0; search_results != null && i < search_results.length; i++)
		{
			arrExistingData.push(search_results[i].getValue('custrecord_tdt_time_entry'));
		}
	
	return arrExistingData;
}
