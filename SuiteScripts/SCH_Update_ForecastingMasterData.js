var SEARCHMODULE;
var FORMAT;
var RECORD;
var RUNTIME;
/**
 *@NApiVersion 2.x
 *@NScriptType ScheduledScript
 *@NModuleScope Public
 */
define(["N/search", "N/format", "N/record", 'N/runtime'], runScheduledScript);

//********************** MAIN FUNCTION **********************
function runScheduledScript(search, format, record, runtime) {
    SEARCHMODULE = search;
    FORMAT = format;
	RECORD=record;
	RUNTIME=runtime;
    var returnObj = {};
    returnObj.execute = execute;
    return returnObj;
}

function execute(context) {
var script = RUNTIME.getCurrentScript();
  
var requested_JSON =script.getParameter('custscript_request_json'); 
var d_startDate = new Date();

log.debug("ScheduledScript Triggered");
var holiday_date = script.getParameter('custscript_holiday_date');
var i_subsidiary =script.getParameter('custscript_i_subsidiary');
var i_customer = script.getParameter('custscript_i_customer');
var i_project_holiday_type = script.getParameter('custscript_project_holiday_type');
log.debug("holiday_date i_subsidiary i_customer i_project_holiday_type",holiday_date+" : "+i_subsidiary+" : "+i_customer+" : "+i_project_holiday_type);

if(i_project_holiday_type=='customrecord_holiday')
{
	i_project_holiday_type=1;
}
else if(i_project_holiday_type=='customrecordcustomerholiday')
{
	i_project_holiday_type=2;
	
}else{
return;
}

	var i_resource_id = null;//requested_JSON
	var holiday_date = FORMAT.format({
			value: holiday_date,
			type: FORMAT.Type.DATE
		});
		
    log.debug("holiday_date string ",holiday_date);
    var d_holiday_end_date = new Date(holiday_date);
    var d_montly_start_date = new Date(d_holiday_end_date.getFullYear(), d_holiday_end_date.getMonth() + 1, 0);
    d_montly_start_date.setDate(1);

    var d_montly_end_date = new Date(d_holiday_end_date.getFullYear(), d_holiday_end_date.getMonth() + 1, 0);//new Date(d_montly_start_date.getFullYear(), d_montly_start_date.getMonth() + 1, -1)
    var s_monthly_start_date = FORMAT.format({
        value: d_montly_start_date,
        type: FORMAT.Type.DATE
    });
    var s_montly_end_date = FORMAT.format({
        value: d_montly_end_date,
        type: FORMAT.Type.DATE
    });
    log.debug("d_montly_start_date : d_montly_end_date", d_montly_start_date + " : " + d_montly_end_date);
    log.debug("s_monthly_start_date : s_montly_end_date", s_monthly_start_date + " : " + s_montly_end_date);
    
    var d_today = new Date();
    var error = false;
    var dataRow = [];
    //Conversion Rates table
    var inr_to_usd = 0;
    var gbp_to_usd = 0;
    var eur_to_usd = 0;
    var aud_to_usd = 0;
    var column = new Array();


    var currencySearch = SEARCHMODULE.create({
        type: 'customrecord_pl_currency_exchange_rates',
        filters: [],
        columns: [
            SEARCHMODULE.createColumn({
                name: "custrecord_pl_currency_exchange_cost"
            }),
            SEARCHMODULE.createColumn({
                name: "custrecord_gbp_converstion_rate"
            }),
            SEARCHMODULE.createColumn({
                name: "custrecord_eur_usd_conv_rate"
            }),
            SEARCHMODULE.createColumn({
                name: "custrecord_aud_to_usd"
            }),
            SEARCHMODULE.createColumn({
                name: "internalid",
                sort: SEARCHMODULE.Sort.DESC
            })
        ]
    }).run().getRange({
        start: 0,
        end: 1
    });
    log.debug('currencySearch', currencySearch);

    if (currencySearch) {
        inr_to_usd = 1 / parseFloat(currencySearch[0].getValue({
            name: 'custrecord_pl_currency_exchange_cost'
        }));
        gbp_to_usd = 1 / parseFloat(currencySearch[0].getValue({
            name: 'custrecord_gbp_converstion_rate'
        }));
        eur_to_usd = 1 / parseFloat(currencySearch[0].getValue({
            name: 'custrecord_eur_usd_conv_rate'
        }));
        aud_to_usd = 1 / parseFloat(currencySearch[0].getValue({
            name: 'custrecord_aud_to_usd'
        }));
    }

    var holidayDetailsInMonth = get_holidays(s_monthly_start_date, s_montly_end_date, i_subsidiary, i_project_holiday_type, i_customer);

    log.debug("holidays ==" + i_project_holiday_type, holidayDetailsInMonth);

    var a_resulted_allocation_details = getResourceAllocations(s_monthly_start_date, s_montly_end_date, i_project_holiday_type, i_subsidiary);
    var a_data_=getRevenueData(s_monthly_start_date, s_montly_end_date, d_montly_start_date, d_montly_end_date, inr_to_usd, gbp_to_usd, eur_to_usd, null, aud_to_usd, null, a_resulted_allocation_details, holidayDetailsInMonth,i_subsidiary,i_project_holiday_type,i_customer)
    var o_forecast_master_ids = _fetch_forecasting_master_data(s_monthly_start_date, i_project_holiday_type, i_customer, i_subsidiary);
  
    if (a_resulted_allocation_details && o_forecast_master_ids) {
		var current_index = script.getParameter('custscript_current_index'); //Check for re-scheduled script
		current_index=current_index?current_index :0; //assign the count
        log.debug("o_forecast_master_ids", o_forecast_master_ids);
		
       for(var indx=current_index;indx<a_data_.length;indx++){
		   
		   // Check remaining usage and reschedule if necessary
            if (RUNTIME.getCurrentScript().getRemainingUsage() < 100) {

                var taskId = rescheduleCurrentScript(holiday_date,i_subsidiary,i_customer,i_project_holiday_type,current_index);
                log.audit("Rescheduling status: " + task.checkStatus(taskId));
                return;
            }
			
		   var s_local = a_data_[indx];
            var i_ra_internalid = s_local.i_ra_id;
            if (o_forecast_master_ids[i_ra_internalid]) {
				log.debug("FM_internalid",o_forecast_master_ids[i_ra_internalid].id);
				try{
				//Update the Forecasting master data
				var createRec=RECORD.load(
				{type:'customrecord_forecast_master_data',
				id:o_forecast_master_ids[i_ra_internalid].id,
				"isDynamic": true});
				createRec.setValue({fieldId:'custrecord41',value:s_local.custpage_holiday_hours});
				createRec.setValue({fieldId:'custrecord_forecast_percent_allocated',value:s_local.custpage_percent});
				createRec.setValue({fieldId:'custrecord38',value:s_local.custpage_allocated_days});
				createRec.setValue({fieldId:'custrecord40',value:s_local.custpage_rate});
				createRec.setValue({fieldId:'custrecord39',value:s_local.custpage_not_submitted_hours});
				createRec.setValue({fieldId:'custrecord_forecast_amount',value:s_local.custpage_total_amount});
				var id = createRec.save();
				current_index++;
				}
				catch(error)
				{
					log.debug('id',error);
				}
			
			}
        }
    }
	log.debug("Remaining Usage",RUNTIME.getCurrentScript().getRemainingUsage());
}


function _fetch_forecasting_master_data(s_month_start_date, project_holiday_type, i_customer, i_subsidiary) {

    var filters;
    if (_logValidation(i_customer)) {
        filters = [
            ["custrecord_forecast_month_startdate", "on", s_month_start_date],
            "AND",
            ["custrecord_forecast_project_name.custentityproject_holiday", "anyof", project_holiday_type],
            "AND",
            ["custrecord_forecast_employee.subsidiary", "anyof", i_subsidiary],
            "AND",["custrecord_forecast_customer", "anyof", i_customer]
        ];
    } else {
        filters = [
            ["custrecord_forecast_month_startdate", "on", s_month_start_date],
            "AND",
            ["custrecord_forecast_project_name.custentityproject_holiday", "anyof", project_holiday_type],
            "AND",
            ["custrecord_forecast_employee.subsidiary", "anyof", i_subsidiary]
        ];
    }

    var customrecord_forecast_master_dataSearchObj = SEARCHMODULE.create({
        type: "customrecord_forecast_master_data",
        filters: filters,
        columns: [
            SEARCHMODULE.createColumn({
                name: "custrecord_forecast_customer",
                sort: SEARCHMODULE.Sort.ASC,
                label: "Customer"
            }),
            SEARCHMODULE.createColumn({
                name: "custrecord_forecast_project_name",
                sort: SEARCHMODULE.Sort.ASC,
                label: "Project"
            }),
            SEARCHMODULE.createColumn({
                name: "custrecord_forecast_employee",
                sort: SEARCHMODULE.Sort.ASC,
                label: "Employee"
            }),
            SEARCHMODULE.createColumn({
                name: "internalid",
                label: "Internal ID"
            }),
            SEARCHMODULE.createColumn({
                name: "custrecord_forecast_month_startdate",
                label: "Month Start Date"
            }),
            SEARCHMODULE.createColumn({
                name: "custrecord_forecast_month_enddate",
                label: "Month End Date"
            }),
            SEARCHMODULE.createColumn({
                name: "custrecord_forecast_ra_id",
                label: "Resource allocation id"
            })
        ]
    });
    var searchResultCount = customrecord_forecast_master_dataSearchObj.runPaged().count;
    log.debug("customrecord_forecast_master_dataSearchObj result count", searchResultCount);
    /*
    customrecord_forecast_master_dataSearchObj.run().each(function(result){
       // .run().each has a limit of 4,000 results
       return true;
    });
    */
    var resultIndex = 0;
    var resultStep = 1000;
    var runSearch = customrecord_forecast_master_dataSearchObj.run();
    log.debug("runSearch", runSearch);
    var fm_data_values = {};
    if (runSearch) {
        var results;
        do {
            results = runSearch.getRange({
                start: parseInt(resultIndex),
                end: parseInt(resultIndex) + parseInt(resultStep)
            });

            for (var k = 0; results != null && k < results.length; k++) {
                var i_resource_allocation = results[k].getValue({
                    name: "custrecord_forecast_ra_id"
                });
                var i_fm_data = results[k].getValue({
                    name: "internalid"
                });
				if(i_resource_allocation){
                fm_data_values[i_resource_allocation] = {
                    'id': i_fm_data
                };
				}
            }
            resultIndex = parseInt(resultIndex) + parseInt(resultStep);
        } while (results.length > 0);

    }
    return fm_data_values;
}




function getRevenueData(s_from_date, s_to_date, d_from_date,
    d_to_date, inr_to_usd, gbp_to_usd, eur_to_usd, projectid, aud_to_usd, i_resource_id, a_resulted_allocation_details, holidayDetailsInMonth,i_subsidiary,i_project_holiday_type,i_customer) {

    var chk_show_ot_hours = 'T';
    // Resource Allocation Data
    var a_resource_allocation = a_resulted_allocation_details;
    var s_currentmonthyear = FORMAT.parse({
        value: s_from_date,
        type: FORMAT.Type.DATE
    }); //nlapiStringToDate(s_from_date, 'date');
    s_currentmonthyear = (s_currentmonthyear.getMonth() + 1) + "/" + s_currentmonthyear.getFullYear();
    var i_current_employee = null;

    var a_res_alloc_for_each_employee = new Object();

    var a_exception_employees = new Array();

    var a_billable_employees = new Array();

    var a_currency_rate = new Object();
    var a_data = new Array();

    for (var i_res_alloc_indx = 0; a_resource_allocation != null && i_res_alloc_indx < a_resource_allocation.length; i_res_alloc_indx++) {
        log.debug('a_resource_allocation '+i_res_alloc_indx,JSON.stringify(a_resource_allocation));

        var a_row_data = new Object();
        var o_resource_allocation = a_resource_allocation[i_res_alloc_indx];
        var i_employee_id = o_resource_allocation.employee;
        var i_project_id = o_resource_allocation.project_id.toString();
        var d_allocationStartDate = FORMAT.parse({
            value: o_resource_allocation.allocationStartDate,
            type: FORMAT.Type.DATE
        }); //nlapiStringToDate(o_resource_allocation.allocationStartDate);
        var d_allocationEndDate = FORMAT.parse({
            value: o_resource_allocation.allocationEndDate,
            type: FORMAT.Type.DATE
        }); //nlapiStringToDate(o_resource_allocation.allocationEndDate);
        var d_provisionStartDate = d_from_date;
        var d_provisionEndDate = d_to_date;
		log.debug( "d_provisionStartDate : d_provisionEndDate",d_provisionStartDate+" : "+d_provisionEndDate);//nlapiDateToString(d_to_date);
       
	   
	   
        var provisionStartDate = FORMAT.format({
            value: d_from_date,
            type: FORMAT.Type.DATE
        }); //nlapiDateToString(d_from_date);
        var provisionEndDate = FORMAT.format({
            value: d_to_date,
            type: FORMAT.Type.DATE
        }); log.debug( "provisionStartDate : provisionEndDate",provisionStartDate+" : "+provisionEndDate);//nlapiDateToString(d_to_date);
        var d_endofthemonthdate = FORMAT.format({
            value: provisionEndDate,
            type: FORMAT.Type.DATE
        }); //nlapiStringToDate(provisionEndDate);

        var lastDayOfMonth = new Date(d_allocationEndDate.getFullYear(), d_allocationEndDate.getMonth() + 1, 0);
        lastDayOfMonth = FORMAT.format({
            value: lastDayOfMonth,
            type: FORMAT.Type.DATE
        }); //nlapiDateToString(lastDayOfMonth);
        lastDayOfMonth = FORMAT.parse({
            value: lastDayOfMonth,
            type: FORMAT.Type.DATE
        }); //nlapiStringToDate(lastDayOfMonth);
        // log.debug( "lastDayOfMonth", lastDayOfMonth);
        // log.debug( "d_endofthemonthdate", d_endofthemonthdate);
        //if (d_endofthemonthdate <= lastDayOfMonth) //added logic to avoid multiple iteration with resource allocations 
        {
            log.debug(" i_res_alloc_indx d_endofthemonthdate : lastDayOfMonth", i_res_alloc_indx + ": " + d_endofthemonthdate + ": " + lastDayOfMonth);

            var provisionMonthStartDate = new Date(String((d_provisionStartDate.getMonth() + 1) + '/1/' + d_provisionStartDate.getFullYear()));
			
             var nextProvisionMonthStartDate = new Date(provisionMonthStartDate.setMonth(provisionMonthStartDate.getMonth() + 1));
			 log.debug("nextProvisionMonthStartDate",nextProvisionMonthStartDate);

            var provisionMonthEndDate =new Date(nextProvisionMonthStartDate.setDate(nextProvisionMonthStartDate.getDate()-1));
			log.debug("provisionMonthEndDate",provisionMonthEndDate);
			
			var startDate = d_allocationStartDate > d_provisionStartDate ? o_resource_allocation.allocationStartDate : provisionStartDate;
            var endDate = d_allocationEndDate < d_provisionEndDate ? o_resource_allocation.allocationEndDate : provisionEndDate;
	log.debug("startDate",startDate);
	log.debug("endDate",endDate);
		var holidays =0;
	   if((d_allocationStartDate.getMonth() == d_from_date.getMonth() && d_allocationStartDate.getFullYear() == d_from_date.getFullYear())
		   ||(d_provisionStartDate.getMonth() == d_to_date.getMonth() && d_provisionStartDate.getFullYear() == d_to_date.getFullYear())
		)
	   {
		   var samemonthholidayDetailsInMonth = get_holidays(startDate, endDate, i_subsidiary, i_project_holiday_type, i_customer);
		   holidays=samemonthholidayDetailsInMonth[s_currentmonthyear] ? samemonthholidayDetailsInMonth[s_currentmonthyear] : 0;
	   }
	   else{
		   holidays=holidayDetailsInMonth[s_currentmonthyear] ? holidayDetailsInMonth[s_currentmonthyear] : 0;
	   }
	
	
	
	
	
	
	
            // days calculation
            var workingDays = 0;
			log.debug("startDate",startDate);
			log.debug("endDate",endDate);
            workingDays = parseFloat(getWorkingDays(startDate, endDate));
            
            var s_conversion_rate_date = s_from_date;
            var d_today = new Date();
            if (d_today < d_from_date) {
                s_conversion_rate_date = null;
            }
           
            if (o_resource_allocation.currency == undefined) {
                a_currency_rate[o_resource_allocation.currency] = nlapiExchangeRate(
                    o_resource_allocation.currency, 'USD',
                    s_conversion_rate_date);
                log.debug('USAGE to isider coversion');
            }

            //Conversion factor
            var f_currency_conversion = 1;
            if (o_resource_allocation.currency == 'USD')
                f_currency_conversion = 1;
            if (o_resource_allocation.currency == 'INR')
                f_currency_conversion = inr_to_usd;
            if (o_resource_allocation.currency == 'EUR')
                f_currency_conversion.currency = eur_to_usd;
            if (o_resource_allocation.currency == 'GBP')
                f_currency_conversion = gbp_to_usd;
            if (o_resource_allocation.currency == 'AUD')
                f_currency_conversion = aud_to_usd;
            //	var f_currency_conversion = parseFloat(a_currency_rate[o_resource_allocation.currency]);
            a_row_data['custpage_vertical'] = o_resource_allocation.vertical;
            a_row_data['custpage_vertical_id'] = o_resource_allocation.vertical_id;
            a_row_data['custpage_employee'] = o_resource_allocation.employee_name;
            a_row_data['custpage_employee_id'] = o_resource_allocation.employee;
            a_row_data['custpage_employee_department'] = o_resource_allocation.department;
            a_row_data['custpage_employee_department_id'] = o_resource_allocation.department_id;
            a_row_data['custpage_employee_parent_department'] = o_resource_allocation.parent_department;
            a_row_data['custpage_project'] = o_resource_allocation.project_name;
            a_row_data['custpage_project_id'] = o_resource_allocation.project_id;
            a_row_data['custpage_customer'] = o_resource_allocation.customer;
            a_row_data['custpage_customer_id'] = o_resource_allocation.customer_id;
            //a_row_data['custpage_holiday_hours'] = o_resource_allocation.holidays;//commented by praveena on 04-02-2020
            a_row_data['custpage_holiday_hours'] = holidays;


            //  a_row_data['custpage_rate'] = (o_resource_allocation.monthly_billing == 'T') ? (o_resource_allocation.monthly_rate / (8.0 * workingDays)) :

            a_row_data['custpage_rate'] = (o_resource_allocation.monthly_billing == 'T') ? (o_resource_allocation.monthly_rate / (parseInt(o_resource_allocation.i_perday_hours) * workingDays)) :


                o_resource_allocation.st_rate; {
                a_row_data['custpage_billed_hours'] = '0';
                a_row_data['custpage_approved_hours'] = '0';
                a_row_data['custpage_submitted_hours'] = '0';
                a_row_data['custpage_billed_hours_ot'] = '0';
                a_row_data['custpage_approved_hours_ot'] = '0';
                a_row_data['custpage_submitted_hours_ot'] = '0';
                a_row_data['custpage_not_submitted_hours'] = (((parseFloat(workingDays)) - parseFloat(holidays)) *
                    //8* commented as per CR to provide the hours dynamically
                    parseInt(o_resource_allocation.i_perday_hours) * //Added no of hours dynamically ad a part of Onsite/Offsite>8 hrs revenue recognition requirement in T&M project
                    parseFloat(o_resource_allocation.percentoftime) / 100.0).toString();


                log.debug('workingDays', parseFloat(workingDays));
                log.debug('holidays', parseFloat(holidays));
                log.debug('o_resource_allocation.i_perday_hours hh', o_resource_allocation.i_perday_hours);
                log.debug('o_resource_allocation.i_perday_hours', parseInt(o_resource_allocation.i_perday_hours));
                log.debug('percentoftime', parseFloat(o_resource_allocation.percentoftime));
                log.debug('a_row_datacustpage_not_submitted_hours', (((parseFloat(workingDays)) - parseFloat(holidays)) * parseInt(o_resource_allocation.i_perday_hours) * parseFloat(o_resource_allocation.percentoftime) / 100.0).toString());


                a_row_data['custpage_leave_hours'] = '0';
                // a_row_data['custpage_holiday_hours'] = '0';

                a_row_data['custpage_billed_amount'] = parseFloat(0.0)
                    .toFixed(2); // *
                // parseFloat(a_row_data['custpage_rate'])).toFixed(2);
                a_row_data['custpage_billed_amount_ot'] = parseFloat(0.0)
                    .toFixed(2);
                a_row_data['custpage_approved_amount'] = parseFloat(0.0)
                    .toFixed(2); // *
                // parseFloat(a_row_data['custpage_rate'])).toFixed(2);
                a_row_data['custpage_approved_amount_ot'] = parseFloat(0.0)
                    .toFixed(2);
                a_row_data['custpage_submitted_amount'] = parseFloat(0.0)
                    .toFixed(2); // *
                // parseFloat(a_row_data['custpage_rate'])).toFixed(2);
                a_row_data['custpage_submitted_amount_ot'] = parseFloat(0.0)
                    .toFixed(2);
            }
            a_row_data['custpage_count'] = parseFloat(o_resource_allocation.count);
            a_row_data['custpage_allocated_days'] = parseFloat(workingDays) - parseFloat(holidays);
            a_row_data['custpage_percent'] = o_resource_allocation.percentoftime
                .toString();
            a_row_data['custpage_not_submitted_amount'] = parseFloat(
                parseFloat(a_row_data['custpage_not_submitted_hours']) *
                parseFloat(a_row_data['custpage_rate'])
                //    * (parseFloat(a_row_data['custpage_percent']) / 100.0)
                *
                f_currency_conversion).toFixed(2);
            a_row_data['custpage_total_amount'] = parseFloat(a_row_data['custpage_billed_amount']) +
                parseFloat(a_row_data['custpage_approved_amount']) +
                parseFloat(a_row_data['custpage_submitted_amount']) +
                parseFloat(a_row_data['custpage_not_submitted_amount']);
            if (chk_show_ot_hours == 'T') {
                a_row_data['custpage_total_amount'] += parseFloat(a_row_data['custpage_billed_amount_ot']) +
                    parseFloat(a_row_data['custpage_approved_amount_ot']) +
                    parseFloat(a_row_data['custpage_submitted_amount_ot']);
            }
            a_row_data['custpage_total_amount'] = a_row_data['custpage_total_amount']
                .toFixed(2);
            a_row_data['custpage_currency'] = o_resource_allocation.currency;
            a_row_data['custpage_t_and_m_monthly'] = o_resource_allocation.monthly_billing == 'T' ? 'T & M Monthly' :
                'T & M';
            a_row_data['custpage_end_customer'] = o_resource_allocation.end_customer;
            a_row_data['emp_location_'] = o_resource_allocation.emp_location;
            a_row_data['emp_level_'] = o_resource_allocation.emp_level;
            a_row_data['emp_role_'] = o_resource_allocation.emp_role;
            a_row_data['i_ra_id'] = o_resource_allocation.i_ra_id;
            log.audit('a_row_datacustpage_not_submitted_hours', a_row_data['custpage_not_submitted_hours']);
            if (Number(a_row_data['custpage_not_submitted_hours']) > 0) { //ADded condtion to avoid duplicate iterations
                a_data.push(a_row_data); // [i_list_indx] = a_row_data;
            }
        }
    }

    log.debug('Number of Records: ', a_data.length);
    return a_data;
}


function isArrayNotEmpty(argArray) {

    return (isNotEmpty(argArray) && argArray.length > 0);
}

function isEmpty(value) {

    return value == null || value == "" || typeof(value) == undefined;
}

function isNotEmpty(value) {

    return !isEmpty(value);
}

function isTrue(value) {

    return value == 'T';
}

function isFalse(value) {

    return value != 'T';
}


// Get Active Resource Allocations
function getResourceAllocations(s_month_start_date, s_month_end_date, i_project_holiday_type, i_subsidiary) {

    var resourceallocationSearchObj = SEARCHMODULE.create({
        type: "resourceallocation",
        filters: [
            ["job.jobbillingtype", "anyof", "TM"],
            "AND",
           ["startdate", "notafter", s_month_end_date ], // ["startdate", "onorbefore", s_month_start_date],
            "AND",
           ["enddate", "notbefore",s_month_start_date], // ["enddate", "onorafter", s_month_end_date],
            "AND",
            ["job.custentityproject_holiday", "anyof", i_project_holiday_type],
            "AND",
            ["custeventrbillable", "is", "T"],
            "AND",
            ["employee.subsidiary", "anyof", i_subsidiary],
        ],
        columns: [
            SEARCHMODULE.createColumn({ //0
                name: "percentoftime",
                summary: "AVG",
                label: "Percentage of Time"
            }),
            SEARCHMODULE.createColumn({ //1
                name: "custeventbstartdate",
                summary: "GROUP",
                label: "Start Date"
            }),
            SEARCHMODULE.createColumn({ //2
                name: "custeventbenddate",
                summary: "GROUP",
                label: "End Date"
            }),
            SEARCHMODULE.createColumn({ //3
                name: "resource",
                summary: "GROUP",
                sort: SEARCHMODULE.Sort.ASC,
                label: "Resource"
            }),
            SEARCHMODULE.createColumn({ //4
                name: "project",
                summary: "GROUP",
                sort: SEARCHMODULE.Sort.ASC,
                label: "Project"
            }),
            SEARCHMODULE.createColumn({ //5
                name: "custevent3",
                summary: "AVG",
                label: "Bill Rate"
            }),
            SEARCHMODULE.createColumn({ //6
                name: "custevent_monthly_rate",
                summary: "AVG",
                label: "Monthly Rate"
            }),
            SEARCHMODULE.createColumn({ //7
                name: "custeventrbillable",
                summary: "GROUP",
                label: "Resource Billable"
            }),

            SEARCHMODULE.createColumn({ //8
                name: "department",
                join: "employee",
                summary: "GROUP",
                label: "Practice"
            }),
            SEARCHMODULE.createColumn({ //9
                name: "subsidiary",
                join: "employee",
                summary: "GROUP",
                label: "Subsidiary"
            }),
            SEARCHMODULE.createColumn({ //10
                name: "customer",
                join: "job",
                summary: "GROUP",
                label: "Customer"
            }),
            SEARCHMODULE.createColumn({ //11
                name: "custentity_project_currency",
                join: "job",
                summary: "GROUP",
                label: "Currency"
            }),
            SEARCHMODULE.createColumn({ //12
                name: "custentity_vertical",
                join: "job",
                summary: "GROUP",
                label: "Vertical"
            }),
            SEARCHMODULE.createColumn({ //13
                name: "custentity_t_and_m_monthly",
                join: "job",
                summary: "GROUP",
                label: "T&M Monthly"
            }),
            SEARCHMODULE.createColumn({ //14
                name: "custevent_monthly_rate",
                summary: "GROUP",
                label: "Monthly Rate"
            }),
            SEARCHMODULE.createColumn({ //15
                name: "formulatext",
                summary: "GROUP",
                formula: "CASE WHEN INSTR({employee.department} , \' : \', 1) > 0 THEN SUBSTR({employee.department}, 1 , INSTR({employee.department} , \' : \', 1)) ELSE {employee.department} END",
                label: "Formula (Text)"
            }),
            SEARCHMODULE.createColumn({ //16
                name: "custentity_endcustomer",
                join: "job",
                summary: "GROUP",
                label: "End Customer"
            }),
            SEARCHMODULE.createColumn({ //17
                name: "internalid",
                summary: "COUNT",
                label: "Internal ID"
            }),
            SEARCHMODULE.createColumn({ //18
                name: "location",
                join: "employee",
                summary: "GROUP",
                label: "Location"
            }),
            SEARCHMODULE.createColumn({ //19
                name: "employeestatus",
                join: "employee",
                summary: "GROUP",
                label: "Employee Status"
            }),
            SEARCHMODULE.createColumn({ //20
                name: "title",
                join: "employee",
                summary: "GROUP",
                label: "Job Title"
            }),
            SEARCHMODULE.createColumn({ //21
                name: "entityid",
                join: "job",
                summary: "GROUP",
                label: "ID"
            }),
            SEARCHMODULE.createColumn({ //22
                name: "custevent4",
                summary: "GROUP",
                label: "Onsite/Offsite"
            }),
            SEARCHMODULE.createColumn({ //23
                name: "custentity_onsite_hours_per_day",
                join: "job",
                summary: "GROUP",
                label: "Onsite Hours per day"
            }),
            SEARCHMODULE.createColumn({ //24
                name: "custentity_hoursperday",
                join: "job",
                summary: "GROUP",
                label: "Hours Per Day"
            }),
            SEARCHMODULE.createColumn({ //25
                name: "internalid",
                summary: "GROUP",
            })
        ]
    });
    var searchResultCount = resourceallocationSearchObj.runPaged().count;
    log.debug("resourceallocationSearchObj result count", searchResultCount);

    var resultIndex = 0;
    var resultStep = 1000;
    var runSearch = resourceallocationSearchObj.run();
    log.debug("runSearch", runSearch);

    // iterate through the search and get all data 1000 at a time
    var searchResultCount = 0;
    var resultSlice = null;
    var a_search_results = [];
    // Store resource allocations for project and employee
    var a_resource_allocations = new Array();
    do {
        resultSlice = runSearch.getRange(searchResultCount, searchResultCount + 1000);

        if (resultSlice) {

            resultSlice.forEach(function(result) {

                a_search_results.push(result);
                searchResultCount++;
            });
        }
    } while (isArrayNotEmpty(resultSlice) && resultSlice.length >= 1000);


    for (var i_search_indx = 0; a_search_results != null && i_search_indx < a_search_results.length; i_search_indx++) {

        var search_results = a_search_results[i_search_indx];

        var i_project_id = search_results.getValue({
            name: "project",
            summary: "GROUP",
            sort: SEARCHMODULE.Sort.ASC
        });

        if (parseInt(i_project_id) == parseInt(120744)) {
            log.debug('Project Index', i_search_indx);
        }
        var s_project_name = search_results.getValue({
            name: "project",
            summary: "GROUP",
            sort: SEARCHMODULE.Sort.ASC
        });
        //log.debug("s_project_name",s_project_name);
        var s_project_id = search_results.getValue({
           name: "entityid",
         join: "job",
         summary: "GROUP",
        });

        //log.debug("s_project_id",s_project_id);

        var is_resource_billable = search_results.getValue({
            name: "custeventrbillable",
            summary: "GROUP"
        });

        //log.debug("is_resource_billable",is_resource_billable);


        var stRate = search_results.getValue({
            name: "custevent3",
            summary: "AVG"
        });

        //log.debug("stRate",stRate);

        stRate = stRate ? parseFloat(stRate) : 0;
        var i_employee = search_results.getValue({
            name: "resource",
            summary: "GROUP"
        });

        //log.debug("i_employee",i_employee);

        var s_employee_name = search_results.getValue({
            name: "resource",
            summary: "GROUP"
        });

        //log.debug("s_employee_name",s_employee_name);

        var i_percent_of_time = search_results.getValue({
            name: "percentoftime",
            summary: "AVG"
        });

        //log.debug("i_percent_of_time",i_percent_of_time);

        var i_department_id = search_results.getValue({
           name: "department",
         join: "employee",
         summary: "GROUP",
        });

        //log.debug("i_department_id",i_department_id);

        var s_department = search_results.getText({
            name: "department",
         join: "employee",
         summary: "GROUP",
        });

        //log.debug("s_department",s_department);

        var i_subsidiary = search_results.getValue({
            name: "subsidiary",
            join: "employee",
            summary: "GROUP"
        });

        //log.debug("i_subsidiary",i_subsidiary);

        //	var i_working_days = search_results.getValue(columns[8]);
        var allocationStartDate = search_results.getValue({
            name: "custeventbstartdate",
            summary: "GROUP"
        });

        //log.debug("allocationStartDate",allocationStartDate);

        var allocationEndDate = search_results.getValue({
            name: "custeventbenddate",
            summary: "GROUP"
        });

        //	log.debug("allocationEndDate",allocationEndDate);

        //var i_working_days = parseInt(calcBusinessDays(d_start_date, d_end_date));;

        var i_customer_id = search_results.getValue({
            name: "customer",
            join: "job",
            summary: "GROUP"
        });

        //log.debug("i_customer_id",i_customer_id);

        var s_customer = search_results.getText({
            name: "customer",
            join: "job",
            summary: "GROUP"
        });

        // log.debug("s_customer",s_customer);

        var i_count = search_results.getValue({
            name: "internalid",
            summary: "COUNT"
        });

        //log.debug("i_count",i_count);

        var i_currency = search_results.getText({
            name: "custentity_project_currency",
            join: "job",
            summary: "GROUP"
        });

        //log.debug("i_currency",i_currency);

        var i_vertical_id = search_results.getValue({
            name: "custentity_vertical",
            join: "job",
            summary: "GROUP"
        });
        var s_vertical = search_results.getValue({
            name: "custentity_vertical",
            join: "job",
            summary: "GROUP"
        });
        var is_T_and_M_monthly = search_results.getValue({
            name: "custentity_t_and_m_monthly",
            join: "job",
            summary: "GROUP",
        });
        var f_monthly_rate = search_results.getValue({
            name: "custevent_monthly_rate",
            summary: "GROUP"
        });
        f_monthly_rate = f_monthly_rate ? parseFloat(f_monthly_rate) : 0;
        var s_parent_department = search_results.getValue({
            name: "formulatext",
            summary: "GROUP",
            formula: "CASE WHEN INSTR({employee.department} , \' : \', 1) > 0 THEN SUBSTR({employee.department}, 1 , INSTR({employee.department} , \' : \', 1)) ELSE {employee.department} END",
        });
        var s_end_customer = search_results.getText({
            name: "custentity_endcustomer",
            join: "job",
            summary: "GROUP"
        });
        var s_emp_location = search_results.getText({
            name: "location",
            join: "employee",
            summary: "GROUP"
        });
        var s_emp_level = search_results.getValue({
            name: "employeestatus",
            join: "employee",
            summary: "GROUP"
        });
        var s_emp_role = search_results.getValue({
            name: "title",
            join: "employee",
            summary: "GROUP"
        });
        var s_pro_id = search_results.getText({
            name: "project",
            summary: "GROUP",
            sort: SEARCHMODULE.Sort.ASC
        });
        // days calculation
        if (_logValidation(s_emp_location))
            s_emp_location = s_emp_location.split(':')[0];

        var i_perday_hours = "";
        var i_offsite_onsite = search_results.getValue({
            name: "custevent4",
            summary: "GROUP"
        }); //CR : 8 hrs revenue recognition requirement in T&M project

        log.debug('i_offsite_onsite', i_offsite_onsite);

        if (i_offsite_onsite == "1") //Onsite
        {
            i_perday_hours = search_results.getValue({
                name: "custentity_onsite_hours_per_day",
                join: "job",
                summary: "GROUP"
            }); //CR : 8 hrs revenue recognition requirement in T&M project

        }

        if (i_offsite_onsite == "2") //Offsite
        {
            i_perday_hours = search_results.getValue({
                name: "custentity_hoursperday",
                join: "job",
                summary: "GROUP"
            }); //CR : 8 hrs revenue recognition requirement in T&M project
        }

        //log.debug( 'i_perday_hours', i_perday_hours);

        i_perday_hours = i_perday_hours > 0 ? i_perday_hours : 8; //Consider the default is 8 hours in case of the data
        log.debug('search i_perday_hours', i_perday_hours);

        //log.debug('holidayDetailsInMonth',JSON.stringify(holidayDetailsInMonth));
        //var holidayCountInMonth = holidayDetailsInMonth.length;
        a_resource_allocations[i_search_indx] = {
            'project_id': s_project_id,
            'project_name': s_project_name,
            is_billable: is_resource_billable,
            st_rate: stRate,
            'employee': i_employee,
            'employee_name': s_employee_name,
            'percentoftime': i_percent_of_time,
            'department_id': i_department_id,
            'department': s_department,
            //'holidays': holidayCountInMonth,
            //'holidayDetailsInMonth':holidayDetailsInMonth,
            'allocationEndDate': allocationEndDate,
            'allocationStartDate': allocationStartDate,
            // 'workingdays': workingDays,
            'customer_id': i_customer_id,
            'customer': i_customer_id,
            'count': i_count,
            'currency': i_currency,
            'vertical_id': i_vertical_id,
            'vertical': s_vertical,
            'monthly_billing': is_T_and_M_monthly,
            'monthly_rate': f_monthly_rate,
            'parent_department': s_parent_department,
            'emp_location': s_emp_location,
            'emp_level': s_emp_level,
            'emp_role': s_emp_role,
            'end_customer': s_end_customer == '- None -' ? '' : s_end_customer,
            'i_perday_hours': i_perday_hours,
            'i_ra_id': search_results.getValue({
                name: "internalid",
                summary: "GROUP"
            })
        };


    }
    log.debug("a_resource_allocations", a_resource_allocations);
    return a_resource_allocations;
}

function _logValidation(value) {
    if (value != '- None -' && value != null && value.toString() != null && value != '' && value != undefined && value.toString() != undefined && value != 'undefined' && value.toString() != 'undefined' && value.toString() != 'NaN' && value != NaN) {
        return true;
    } else {
        return false;
    }
}

function get_holidays(start_date, end_date, emp_subsidiary, project_holiday, customer) {
    if (project_holiday == 1) {
        return get_company_holidays(start_date, end_date, emp_subsidiary);
    } else {
        return get_customer_holidays(start_date, end_date, emp_subsidiary, customer);
    }
}

function get_company_holidays(start_date, end_date, subsidiary) {
    var holiday_list = {};
    var check_monthly_holidays = [];
    var search_company_holiday = SEARCHMODULE.load({
        id: 'customsearch_company_holiday_search'
    });
    //Add Filter to search

    search_company_holiday.filters.push(SEARCHMODULE.createFilter({
        name: 'custrecord_date',
        operator: 'within',
        values: [start_date, end_date]
    }));
    search_company_holiday.filters.push(SEARCHMODULE.createFilter({
        name: 'custrecordsubsidiary',
        operator: 'anyof',
        values: [subsidiary]
    }));
    search_company_holiday.columns.push(SEARCHMODULE.createColumn({
        name: 'custrecord_date'
    }));


    var resultset = search_company_holiday.run();
    var searchResultCount = search_company_holiday.runPaged().count;
    var results = resultset.getRange(0, searchResultCount);
    if (results) {
        for (var i = 0; i < results.length; i++) {
            var Holiday_Date = results[i].getValue({
                "name": 'custrecord_date'
            });
            Holiday_Date = FORMAT.parse({
                value: Holiday_Date,
                type: FORMAT.Type.DATE
            }); //nlapiStringToDate(Holiday_Date, 'date');
            holiday_list[Number(Holiday_Date.getMonth() + 1) + "/" + Holiday_Date.getFullYear()] = parseInt(holiday_list[Number(Holiday_Date.getMonth() + 1) + "/" + Holiday_Date.getFullYear()]) ? parseInt(holiday_list[Number(Holiday_Date.getMonth() + 1) + "/" + Holiday_Date.getFullYear()]) + 1 : 1;
        }
    }
    return holiday_list;
}

function get_customer_holidays(start_date, end_date, subsidiary, customer) {
    var holiday_list = {};
    var check_monthly_holidays = [];
    // var start_date = FORMAT.format({value:start_date, type: FORMAT.Type.DATE});//nlapiDateToString(start_date, 'date');
    // var end_date = FORMAT.format({value:end_date, type: FORMAT.Type.DATE});//nlapiDateToString(end_date, 'date');

    var search_customer_holiday = SEARCHMODULE.load({
        id: 'customsearch_customer_holiday'
    });
    //Add Filter to search
    search_customer_holiday.filters.push(SEARCHMODULE.createFilter({
        name: 'custrecordholidaydate',
        operator: 'within',
        values: [start_date, end_date]
    }));
    search_customer_holiday.filters.push(SEARCHMODULE.createFilter({
        name: 'custrecordcustomersubsidiary',
        operator: 'anyof',
        values: subsidiary
    }));
    search_customer_holiday.filters.push(SEARCHMODULE.createFilter({
        name: 'custrecord13',
        operator: 'anyof',
        values: customer
    }));
    search_customer_holiday.columns.push(SEARCHMODULE.createColumn({
        name: 'custrecordholidaydate',
        summary: 'group'
    }));
    var search_customer_holiday = search_customer_holiday.run().getRange(0, search_customer_holiday.runPaged().count);

    if (search_customer_holiday) {

        for (var i = 0; i < search_customer_holiday.length; i++) {
            var Holiday_Date = search_customer_holiday[i].getValue({
                name: 'custrecordholidaydate',
                summary: 'group'
            });
            Holiday_Date = FORMAT.parse({
                value: Holiday_Date,
                type: FORMAT.Type.DATE
            }); //nlapiStringToDate(Holiday_Date, 'date');         
            holiday_list[Number(Holiday_Date.getMonth() + 1) + "/" + Holiday_Date.getFullYear()] = parseInt(holiday_list[Number(Holiday_Date.getMonth() + 1) + "/" + Holiday_Date.getFullYear()]) ? parseInt(holiday_list[Number(Holiday_Date.getMonth() + 1) + "/" + Holiday_Date.getFullYear()]) + 1 : 1;
        }
    }

    return holiday_list;
}

function getWorkingDays(startDate, endDate) {
    try {
	/*var d_startDate =  FORMAT.parse({
        value: startDate,
        type: FORMAT.Type.DATETIMETZ
    });//nlapiStringToDate(startDate);
      var d_endDate = FORMAT.parse({
        value: endDate,
        type: FORMAT.Type.DATETIMETZ
    });//nlapiStringToDate(endDate);
	*/
	var d_startDate=new Date(startDate);
	var d_endDate=new Date(endDate);
        var numberOfWorkingDays = 0;

        for (var i = 0;; i++) {
            
			var currentDate=new Date(d_startDate.setDate(d_startDate.getDate() + 1));
			//var currentDate=new Date(d_startDate.setDate(d_startDate.getDate() + i));
			
		//	log.debug("currentDate",currentDate);
            if (currentDate > d_endDate) {
                break;
            }

            if (currentDate.getDay() != 0 && currentDate.getDay() != 6) {
                numberOfWorkingDays += 1;
            }
        }

        return numberOfWorkingDays;
    } catch (err) {
        log.error('getWorkingDays', err);
        throw err;
    }
}

function _logValidation(value) {
    if (value != '- None -' && value != null && value.toString() != null && value != '' && value != undefined && value.toString() != undefined && value != 'undefined' && value.toString() != 'undefined' && value.toString() != 'NaN' && value != NaN) {
        return true;
    } else {
        return false;
    }
}

function monthDiff(dateFrom, dateTo) {
    return dateTo.getMonth() - dateFrom.getMonth() +
        (12 * (dateTo.getFullYear() - dateFrom.getFullYear()))
}

/**
* Reschedules the current script and returns the ID of the reschedule task
*/
function rescheduleCurrentScript(holiday_date,i_subsidiary,i_customer,i_project_holiday_type,current_index) {
var scheduledScriptTask = task.create({
	taskType: task.TaskType.SCHEDULED_SCRIPT
});
		
 var data_parameters=
{'custscript_i_customer':i_customer,
'custscript_holiday_date':holiday_date,
'custscript_i_subsidiary':i_subsidiary,
'custscript_project_holiday_type':s_recordtype,
'custscript_current_index':current_index
};    
scheduledScriptTask.scriptId = runtime.getCurrentScript().id;
scheduledScriptTask.params = data_parameters;
// scheduledScriptTask.deploymentId = runtime.getCurrentScript().deploymentId;
return scheduledScriptTask.submit();
    
	
}

function getWorkingDays(startDate, endDate) {//getBusinessDatesCount(startDate, endDate) {
    var count = 0;
    var curDate = new Date(startDate);
  	var endDate=new Date(endDate);
  log.debug("curDate",curDate);
    while (curDate <= endDate) {
        var dayOfWeek = curDate.getDay();
        if(!((dayOfWeek == 6) || (dayOfWeek == 0)))
           count++;
        curDate.setDate(curDate.getDate() + 1);
    }
    //alert(count)
    return count;
}