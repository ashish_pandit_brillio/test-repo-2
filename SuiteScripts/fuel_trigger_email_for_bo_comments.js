/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       05 Sep 2019     Aazamali Khan
 *
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType
 * 
 * @param {String} type Operation types: create, edit, delete, xedit,
 *                      approve, cancel, reject (SO, ER, Time Bill, PO & RMA only)
 *                      pack, ship (IF only)
 *                      dropship, specialorder, orderitems (PO only) 
 *                      paybills (vendor payments)
 * @returns {Void}
 */
function uesTriggerEmail(type){
  if(type == "edit" || type == "xedit"){
	  var recObj = nlapiLoadRecord(nlapiGetRecordType(), nlapiGetRecordId());
	  nlapiLogExecution("DEBUG", "frfId", nlapiGetRecordId());
	  var oldRec = nlapiGetOldRecord();
	  var name = "";
	  var i_recipient;//new Array();
	  var arr_cc = new Array();
	  var status =  recObj.getFieldValue("custrecord_frf_details_status_flag");
	  var boCommentsNew = recObj.getFieldValue("custrecord_frf_details_bo_comments"); 
	  var boCommentsOld = oldRec.getFieldValue("custrecord_frf_details_bo_comments");
	  if(status == "3" && (boCommentsNew != boCommentsOld)){
		  var s_recipient = recObj.getFieldText("custrecord_frf_details_created_by");
		  if(s_recipient){
			  name = s_recipient.split("-")[1];  
		  }else{
			  name = s_recipient;  
		  }
		  var s_boCommentsNew = recObj.getFieldText("custrecord_frf_details_bo_comments");
		  var frfNumber = recObj.getFieldValue("custrecord_frf_details_frf_number");
		  i_recipient = recObj.getFieldValue("custrecord_frf_details_created_by");
		  var i_practice = recObj.getFieldValue("custrecord_frf_details_res_practice");
		  var project = recObj.getFieldValue("custrecord_frf_details_project");
//---------------------------------------------------------------------------------------------------------------------------	//prabhat gupta 16/6/2020 NIS-1376	  
		  var project_name = recObj.getFieldText("custrecord_frf_details_project");
		  var account_name = recObj.getFieldText("custrecord_frf_details_account");
		  
		  var opp_id = recObj.getFieldValue("custrecord_frf_details_opp_id");
	//------------------------------------------------------------------------------------------------------------------------	  
		  var searchResult = getPracticeSpocEmail(i_practice);
		  if(searchResult){
			  for (var i = 0; i < searchResult.length; i++) {
				arr_cc.push(searchResult[i].getValue("email","CUSTRECORD_SPOC",null))
			}
		  }
		  if(project){
			  var i_projectManager = nlapiLookupField("job", project, "custentity_projectmanager");
			  arr_cc.push(nlapiLookupField("employee", i_projectManager, "email")); 
			  var i_deliveryManager = nlapiLookupField("job", project, "custentity_deliverymanager");
			  arr_cc.push(nlapiLookupField("employee", i_deliveryManager, "email"));
		  }else{
			  if(opp_id){
				  var opp_name = nlapiLookupField("customrecord_sfdc_opportunity_record", opp_id, "custrecord_opportunity_name_sfdc");
				  
				  project_name = opp_name;
			  }
		  }
		  
		  
		  arr_cc.push("business.ops@BRILLIO.COM");
		  arr_cc.push("fuel.support@brillio.com");
		  nlapiLogExecution("DEBUG", "arr_cc", JSON.stringify(arr_cc));
	//-----------------------------------------------------------------------------------------------------------------------	 //prabhat gupta 16/6/2020 NIS-1376	  
		  var body = "Dear " + name + ", \n" + " Business Operations team has commented '" + s_boCommentsNew + "' for FRF # " + frfNumber + " created against Opportunity/Project: "+ project_name + " and Account: " + account_name + ".\n Kindly do the needful."+ "\n\n\n"
			body += "Brillio-FUEL \n"
			body += "This email was sent from a notification only address.\n"
			body += "If you need further assistance, please write to fuel.support@brillio.com \n";
			
	//---------------------------------------------------------------------------------------------------------------		
		  var subject = frfNumber + ": FUEL Notification: Bo Commnets";
		  nlapiLogExecution("DEBUG", "body", body);
        if(s_boCommentsNew!="No Action Required")
		  nlapiSendEmail(154256,i_recipient, subject, body,arr_cc);
		  //nlapiSendEmail(154256,"aazamali@inspirria.com", subject, body);
		  
	  }
  }
}
function getPracticeSpocEmail(practice){
	var customrecord_practice_spocSearch = nlapiSearchRecord("customrecord_practice_spoc",null,
			[
			   ["custrecord_practice","anyof",practice], 
			   "AND", 
			   ["custrecord_spoc.custentity_employee_inactive","is","F"]
			], 
			[
			   new nlobjSearchColumn("email","CUSTRECORD_SPOC",null)
			]
			);
	return customrecord_practice_spocSearch;
}