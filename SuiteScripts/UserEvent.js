/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       19 Jan 2018     bidhi.raj
 *
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType
 *   
 * @param {String} type Operation types: create, edit, view, copy, print, email
 * @param {nlobjForm} form Current form
 * @param {nlobjRequest} request Request object
 * @returns {Void}
 */
function userEventBeforeLoad(type, form, request){
 var headCount = 5;
 var currentRecord = nlapiLoadRecord(nlapiGetRecordType(), nlapiGetRecordId());
 var filter = new Array();
 filter.push(new  nlobjSearchFilter('job',null,'is',nlapiGetFieldValue('project')));
 var resAllocation = nlapiSearchRecord('resourceallocation', 'customsearch2271', filter, null);
 for(var i in resAllocation){
	 resAllocation[i].getFieldValue('');
 }
 
 if(resAllocation.length<=headCount){
	 
 }
}
