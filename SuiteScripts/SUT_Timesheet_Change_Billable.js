//https://debugger.na1.netsuite.com/app/common/scripting/script.nl?id=1109
function changeBillableFunction()
{
	try
	{
		var a_days = ['sunday', 'monday', 'tuesday', 'wednesday','thursday', 'friday', 'saturday'];
		var searchTimeSheet = nlapiSearchRecord('timesheet','customsearch2773',null,[new nlobjSearchColumn('internalid')]);
		nlapiLogExecution('debug','Record search',searchTimeSheet.length);
		
		if(searchTimeSheet)
		{
			for(var i=0;i<searchTimeSheet.length;i++)
			{
				var id = searchTimeSheet[i].getValue('internalid');
				if(id)
				{
					var rec = nlapiLoadRecord('timesheet',parseInt(id));
					var count = rec.getLineItemCount('timegrid');
					
					for(var i = 1; i <= count ; i++)
					{
						rec.selectLineItem('timegrid',i);
						
						for(var i_day_indx = 0; i_day_indx < 7; i_day_indx++)
						{
							var sub_record_name = a_days[i_day_indx];
							var obj_view = rec.viewCurrentLineItemSubrecord('timegrid',sub_record_name);
							var obj = rec.editCurrentLineItemSubrecord('timegrid', sub_record_name);
							if(_logValidation(obj))
							{
								var billable = obj.getField('isbillable');
								billable.setField('F');
								obj.commit(sub_record_name);
							}
						}
						nlapiCommitLineItem('timegrid',i)
					}
					//loadRec.setFieldValue('isbillable','F');
					var id = nlapiSubmitRecord(rec);
					nlapiLogExecution('debug', 'timesheet', id);	
				}

			}
		}
	}

	catch(e)
	{
		nlapiLogExecution('debug','Error in update',e);
	}
}

function _logValidation(value) 
{
 if(value!='null' && value != null && value != null && value != '' && value != undefined && value != undefined && value != 'undefined' && value!= 'undefined'&& value!= 'NaN' && value != NaN) 
 {
  return true;
 }
 else 
 { 
  return false;
 }
}