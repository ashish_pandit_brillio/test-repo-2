/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       16 Sep 2016     deepak.srinivas
 *
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType
 *   
 * @returns {Boolean} True to continue save, false to abort save
 */
function clientSaveRecord(){
try{
	var year = nlapiGetFieldValue('custrecord_year_ofc');
	var month = nlapiGetFieldValue('custrecord_month_ofc');
	
	var year_T = nlapiGetFieldText('custrecord_year_ofc');
	var month_T = nlapiGetFieldText('custrecord_month_ofc');
	
	var filters = Array();
	filters.push(new nlobjSearchFilter('custrecord_month_ofc', null , 'anyof', parseInt(month)));
	filters.push(new nlobjSearchFilter('custrecord_year_ofc', null , 'anyof', parseInt(year)));
	
	var cols =  Array();
	cols.push(new nlobjSearchColumn('internalid'));
	
	var exclude_ofc_cost_search = nlapiSearchRecord('customrecord_ofc_cost_against_customer',null,filters,cols);
	
	if(exclude_ofc_cost_search){
		var id = exclude_ofc_cost_search[0].getValue('internalid');
		if(id){
			alert('There is already a record exists with month ' + month_T +' & Year ' + year_T);
			return false;
		}
	}
	return true;
}

    catch(e){
    	nlapiLogExecution('DEBUG','Error in Validating Exclude OFC Cost Record',e);
    }
}
