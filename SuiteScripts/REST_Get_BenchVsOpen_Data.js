/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       15 Mar 2019     Aazamali Khan
 *
 Need to check if we are using open check for FRF details or it is based on softlock on frf details.
 
 */

/**
 * @param {Object} dataIn Parameter object
 * @returns {Object} Output object
 */
function postRESTlet(dataIn) {
    var frfdata = [];
    var benchdata = [];
    var revenuedata = [];
    var billiableData = [];
    var billedProjects = [];
    var billedOpp = [];
	var timestamp;
    try {
        var userEmail = dataIn.user;
        var userId = getUserUsingEmailId(userEmail);
		//if(userId == 94862)
			//userId = 148781;
        var spocSearch = GetPractice_SPOCList(userId);
        nlapiLogExecution("ERROR","spocSearch",JSON.stringify(spocSearch));
        var regionSearch = GetRegionId(userId);
		nlapiLogExecution("ERROR","regionSearch",JSON.stringify(regionSearch));
		var deliveryAnchore = GetDeliveryAnchore(userId);
        var adminUser = GetAdmin(userId);

        if (adminUser) {
            var frfSearchResult = GetOpenFRF(null, null, true, null,null);
            var benchSearchResult = GetBenchResource(null, null, true, null,null);
            var revenueSearchResult = GetRevenueResult(null, null, true, null,null);
        } else if (isArrayNotEmpty(spocSearch)) {
            nlapiLogExecution("ERROR","spocSearch","User is spoc");
            var i_practice = spocSearch;
            var frfSearchResult = GetOpenFRF(userId, i_practice, null, null,null);
            var benchSearchResult = GetBenchResource(userId, i_practice, null, null,null);
            var revenueSearchResult = GetRevenueResult(userId, i_practice, null, null,null);
        } else if (regionSearch) {
            nlapiLogExecution("ERROR","regionSearch","User is region head"); 
            var i_region = regionSearch;
            var frfSearchResult = GetOpenFRF(userId, null, null, i_region,null);
            var benchSearchResult = GetBenchResource(userId, null, null, i_region,null);
            var revenueSearchResult = GetRevenueResult(userId, null, null, i_region,null);
        }else if (deliveryAnchore) {
            nlapiLogExecution("ERROR","deliveryAnchoreSearch","User is deliveryAnchore"); 
            var i_deliveryAnchore = deliveryAnchore;
            var frfSearchResult = GetOpenFRF(userId, null, null, null,i_deliveryAnchore);
            var benchSearchResult = GetBenchResource(userId, null, null, null,i_deliveryAnchore);
            var revenueSearchResult = GetRevenueResult(userId, null, null, null,i_deliveryAnchore);
        } else {
            var frfSearchResult = GetOpenFRF(userId, null, null, null,null);
            var benchSearchResult = GetBenchResource(userId, null, null, null,null);
            var revenueSearchResult = GetRevenueResult(userId, null, null, null,null);
        }
        var practiceId = nlapiLookupField("employee", userId, "department", false);

        if (frfSearchResult) {
            for (var int = 0; int < frfSearchResult.length; int++) {
                var booked = false;
				var s_salesactivity = frfSearchResult[int].getText("custrecord_projection_status_sfdc", "CUSTRECORD_FRF_DETAILS_OPP_ID", 
				null);
				var i_opportunityStatus = frfSearchResult[int].getValue("custrecord_stage_sfdc","CUSTRECORD_FRF_DETAILS_OPP_ID",null);
				if(frfSearchResult[int].getValue("custrecord_frf_details_project") && !(frfSearchResult[int].getValue("custrecord_frf_details_opp_id"))){
					booked = true;
				}
                if ((s_salesactivity == "Most Likely" || booked == true) && (parseInt(i_opportunityStatus) != parseInt(8) && parseInt(i_opportunityStatus) != parseInt(9))) {
                    var frfData = {
                        "region": frfSearchResult[int].getText("custentity_region", "CUSTRECORD_FRF_DETAILS_ACCOUNT", null) ? frfSearchResult[int].getText("custentity_region", "CUSTRECORD_FRF_DETAILS_ACCOUNT", null) : "other",
                        "practice": frfSearchResult[int].getText("custrecord_frf_details_res_practice"),
                        "account":{ "name" : frfSearchResult[int].getText("custrecord_frf_details_account"), "id" : frfSearchResult[int].getValue("custrecord_frf_details_account") }, 
                        "startdate": frfSearchResult[int].getValue("custrecord_frf_details_start_date"),
                        "enddate": frfSearchResult[int].getValue("custrecord_frf_details_end_date"),
                        "salesactivity": s_salesactivity ? s_salesactivity : "Booked"
                    }
                    if (frfSearchResult[int].getValue("custrecord_opportunity_name_sfdc", "CUSTRECORD_FRF_DETAILS_OPP_ID", null))

                    {

                        frfData.project = {
                            "name": frfSearchResult[int].getValue("custrecord_opportunity_name_sfdc", "CUSTRECORD_FRF_DETAILS_OPP_ID", null),
                            "id": frfSearchResult[int].getValue("CUSTRECORD_FRF_DETAILS_OPP_ID")
                        };

                    } else

                    {

                        frfData.project = {
                            "name": frfSearchResult[int].getText("custrecord_frf_details_project"),
                            "id": frfSearchResult[int].getValue("custrecord_frf_details_project")
                        };

                    }
                    frfdata.push(frfData);
                }
            }
        }

        if (benchSearchResult) {
            for (var i = 0; i < benchSearchResult.length; i++) {
				//nlapiLogExecution("AUDIT","benchSearchResult length : ",benchSearchResult.length);
                var date = new Date();
                date = nlapiDateToString(date);
                var benchData = {
                    "region": "Brillio",
                    "practice": benchSearchResult[i].getText("custentity_practice", "job", "GROUP"),
                    "accout": {"name" : benchSearchResult[i].getValue("altname", "customer", "GROUP"),
					"id" : benchSearchResult[i].getValue("customer", "job", "GROUP")},
                    "project": {
                        "name": benchSearchResult[i].getText("project", null, "GROUP"),
                        "id": benchSearchResult[i].getValue("project", null, "GROUP")
                    },
		    "employee":benchSearchResult[i].getValue("resource", null, "GROUP"),
                    "todaysdate": date,
                    "allocationstartdate": benchSearchResult[i].getValue("startdate", null, "GROUP"),
                    "allocationenddate": benchSearchResult[i].getValue("enddate", null, "GROUP"),
                    "allocationpercent": benchSearchResult[i].getValue("percentoftime", null, "GROUP"),
                    "salesactivity": "Booked"
                }
                benchdata.push(benchData);
            }
        }

        if (revenueSearchResult) {
            for (var j = 0; j < revenueSearchResult.length; j++) {
                var revData = {
                    "recId": revenueSearchResult[j].getId(), 
                    "region": revenueSearchResult[j].getValue("custrecord_sfdc_revenue_data_accregion"),
                    "practice": revenueSearchResult[j].getText("custrecord_sfdc_fuel_forecast_rev_pract", "CUSTRECORD_SFDC_FUEL_FORECAST_REV_EXT", null),
                    "account":{"name" : revenueSearchResult[j].getValue("custrecord_sfdc_revenue_data_accname"),"id" : revenueSearchResult[j].getValue("customer", "custrecord_sfdc_revenue_data_proj_id_ns", null)},
                    "project": {
                        "name": revenueSearchResult[j].getValue("custrecord_sfdc_revenue_data_proj_id_ns") ? revenueSearchResult[j].getText("custrecord_sfdc_revenue_data_proj_id_ns") : "",
                        "id": revenueSearchResult[j].getValue("custrecord_sfdc_revenue_data_proj_id_ns") ? revenueSearchResult[j].getValue("custrecord_sfdc_revenue_data_proj_id_ns") : ""
                    },
                    "startdate": revenueSearchResult[j].getValue("custrecord_sfdc_revenue_data_startdate"),
                    "salesactivity": revenueSearchResult[j].getValue("custrecord_sfdc_revenue_data_prostatus"),
                    "forecastmonthandyear": revenueSearchResult[j].getValue("custrecord_sfdc_fuel_fore_modayyear", "CUSTRECORD_SFDC_FUEL_FORECAST_REV_EXT", null),
                    "Amount": revenueSearchResult[j].getValue("custrecord_sfdc_fuel_forecast_rev_amount", "CUSTRECORD_SFDC_FUEL_FORECAST_REV_EXT", null),
                    "oppid": revenueSearchResult[j].getValue("custrecord_sfdc_revenue_data_opp_id") ? revenueSearchResult[j].getValue("custrecord_sfdc_revenue_data_opp_id") : ""
                }
                revenuedata.push(revData);
                if (revenueSearchResult[j].getValue("custrecord_sfdc_revenue_data_proj_id_ns")) {
                    billedProjects.push(revenueSearchResult[j].getValue("custrecord_sfdc_revenue_data_proj_id_ns"));
                }
                if (revenueSearchResult[j].getValue("custrecord_sfdc_revenue_data_opp_id")) {
                    billedOpp.push(revenueSearchResult[j].getValue("custrecord_sfdc_revenue_data_opp_id"));
                }
            }
			timestamp = revenueSearchResult[0].getValue("created")
			//revenuedata.push({"revtimestamp" : timestamp})
        }
        // Processed on the basis of available projects
        if (isArrayNotEmpty(billedProjects)) {

            var billiableSearchResult = GetBillableResources(billedProjects);
            if (billiableSearchResult) {
                for (var i = 0; i < billiableSearchResult.length; i++) {
                    var jsonObj = {};
	                jsonObj.resourcename = billiableSearchResult[i].getValue("resource", null, "GROUP");
                    jsonObj.startdate = billiableSearchResult[i].getValue("startdate", null, "GROUP");
                    jsonObj.enddate = billiableSearchResult[i].getValue("enddate", null, "GROUP");
                    jsonObj.project = {
                        "name": billiableSearchResult[i].getText("project", null, "GROUP"),
                        "id": billiableSearchResult[i].getValue("project", null, "GROUP")
                    };
					jsonObj.account = {
                        "name": billiableSearchResult[i].getValue("altname", "customer", "GROUP"),
                        "id": billiableSearchResult[i].getValue("customer", "job", "GROUP")
                    };
                    billiableData.push(jsonObj);
                }
            }
        }
        if (isArrayNotEmpty(billedOpp)) {
            var billedFRFSearchResult = GetBilledFRFs(billedOpp);
            if (billedFRFSearchResult) {
                for (var i = 0; i < billedFRFSearchResult.length; i++) {
                    var jsonObj = {};
                    jsonObj.resourcename = billedFRFSearchResult[i].getValue("custrecord_frf_details_allocated_emp");
                    jsonObj.startdate = billedFRFSearchResult[i].getValue("custrecord_frf_details_start_date");
                    jsonObj.enddate = billedFRFSearchResult[i].getValue("custrecord_frf_details_end_date");
                    jsonObj.oppid = billedFRFSearchResult[i].getValue("custrecord_frf_details_opp_id");
                    billiableData.push(jsonObj);
                }
            }
        }
		
        return {
            "frfdata": frfdata,
            "benchdata": benchdata,
            "revenuedata": revenuedata,
            "billableresources": billiableData,
			"revtimestamp" : timestamp
        };
    } catch (e) {
        nlapiLogExecution("ERROR", "Bench vs Open Error : ", e);
        return {
            "code": "Error",
            "message": e
        }
    }
}

function GetOpenFRF(user, practice, isAdmin, region,i_deliveryAnchore) {
   
		var filters = new Array();
		var columns = new Array();
		if(isAdmin ==true)
		{
			filters = [["custrecord_frf_details_status","is","F"],
					  "AND",
					  [["isinactive","is","F"],
					  "OR",
					  ["custrecord_frf_details_opp_id.custrecord_stage_sfdc","noneof","9","8"]]/*,
					  "AND",
					  ["custrecord_frf_details_end_date","onorafter","today"],
					  "AND",
					  ["custrecord_frf_details_status_flag","noneof",2]*/]
		}
		else if(practice)
		{
			filters = [
					  /*["custrecord_frf_details_project.custentity_practice","anyof",practice],
					   "OR",
				      ["custrecord_frf_details_opp_id.custrecord_practice_internal_id_sfdc","anyof",practice],*/
					  ["custrecord_frf_details_res_practice","anyof",practice],
					  "AND",
					  ["custrecord_frf_details_status","is","F"],
					  "AND",
					  [["isinactive","is","F"],
					  "OR",
					  ["custrecord_frf_details_opp_id.custrecord_stage_sfdc","noneof","9","8"]]/*,
					  "AND",
					  ["custrecord_frf_details_end_date","onorafter","today"],
					  "AND",
					  ["custrecord_frf_details_status_flag","noneof",2]*/]
		}
		else if(region)
		{
			filters = [["custrecord_frf_details_account.custentity_region","anyof",region],
					  "AND",
					  ["custrecord_frf_details_status","is","F"],
					  "AND",
					  [["isinactive","is","F"],
					  "OR",
					  ["custrecord_frf_details_opp_id.custrecord_stage_sfdc","noneof","9","8"]]/*,
					  "AND",
					  ["custrecord_frf_details_end_date","onorafter","today"],
					  "AND",
					  ["custrecord_frf_details_status_flag","noneof",2]*/]
		}
		else if(i_deliveryAnchore)
		{
			var i_practice = i_deliveryAnchore[0].practice;
			filters = [
					  /*["custrecord_frf_details_project.custentity_practice","anyof",practice],
					   "OR",
				      ["custrecord_frf_details_opp_id.custrecord_practice_internal_id_sfdc","anyof",practice],*/
					  ["custrecord_frf_details_res_practice","anyof",i_practice],
					  "AND",
					  ["custrecord_frf_details_status","is","F"],
					  "AND",
					  [["isinactive","is","F"],
					  "OR",
					  ["custrecord_frf_details_opp_id.custrecord_stage_sfdc","noneof","9","8"]]/*,
					  "AND",
					  ["custrecord_frf_details_end_date","onorafter","today"],
					  "AND",
					  ["custrecord_frf_details_status_flag","noneof",2]*/]
		}
		else if(user)
		{
			filters = [[["custrecord_frf_details_project.custentity_projectmanager","anyof",user],
					   "OR",
					  ["custrecord_frf_details_project.custentity_deliverymanager","anyof",user],
					   "OR",
					  ["custrecord_frf_details_project.custentity_clientpartner","anyof",user]],
					  "OR",
					  [
					  ["custrecord_frf_details_opp_id.custrecord_sfdc_opp_pm","anyof",user],
					  "OR",
					  ["custrecord_frf_details_opp_id.custrecord_sfdc_opportunity_dm","anyof",user],
					   "OR",
					  ["custrecord_frf_details_opp_id.custrecord_sfdc_client_partner","anyof",user]],
					  "AND",
					  ["custrecord_frf_details_status","is","F"],
					  "AND",
					  [["isinactive","is","F"],
					  "OR",
					  ["custrecord_frf_details_opp_id.custrecord_stage_sfdc","noneof","9","8"]]/*,
					  "AND",
					  ["custrecord_frf_details_end_date","onorafter","today"],
					  "AND",
					  ["custrecord_frf_details_status_flag","noneof",2]*/
					  ]
		}
    var columns = [
        new nlobjSearchColumn("custrecord_frf_details_res_location"),
        new nlobjSearchColumn("custrecord_frf_details_res_practice"),
        new nlobjSearchColumn("custrecord_frf_details_account"),
        new nlobjSearchColumn("custrecord_frf_details_project"),
        new nlobjSearchColumn("custrecord_frf_details_start_date"),
        new nlobjSearchColumn("custrecord_frf_details_opp_id"),
        new nlobjSearchColumn("custrecord_frf_details_end_date"),
        new nlobjSearchColumn("custrecord_projection_status_sfdc", "CUSTRECORD_FRF_DETAILS_OPP_ID", null),
		new nlobjSearchColumn("custrecord_stage_sfdc","CUSTRECORD_FRF_DETAILS_OPP_ID",null), 
        new nlobjSearchColumn("custrecord_opportunity_name_sfdc", "CUSTRECORD_FRF_DETAILS_OPP_ID", null),
        new nlobjSearchColumn("custentity_region", "CUSTRECORD_FRF_DETAILS_ACCOUNT", null)
    ]
    var customrecord_frf_detailsSearch = searchRecord("customrecord_frf_details", null,
        filters, columns, null);
    return customrecord_frf_detailsSearch;
}

function GetBenchResource(i_user, i_practice, isAdmin, i_region,i_deliveryAnchore) {
    var filters = [];
    var startDate = getStartDate();
    nlapiLogExecution("AUDIT","GetBenchResource : startdate",startDate);
    var endDate = getEndDate();
    nlapiLogExecution("AUDIT","GetBenchResource : endDate",endDate);
    if (isAdmin) {
        filters = [
            ["job.custentity_project_allocation_category", "anyof", "4"],
            "AND",
            ["job.status", "anyof", "2"],
            "AND",
            ["employee.custentity_employee_inactive", "is", "F"],
            "AND",
            ["startdate", "notafter", endDate], //+ 12 month
            "AND",
            ["enddate", "notbefore", startDate]
        ]
    } else if (i_user && i_practice) {
		var department = nlapiLookupField("employee",i_user,"department");
	nlapiLogExecution("ERROR","department",department);
	var parentPractice = nlapiLookupField("department",department,"custrecord_parent_practice");
	nlapiLogExecution("ERROR","parent practice :",parentPractice);
	var practiceArray = getSubPractices(i_practice);
        filters = [
            ["job.custentity_project_allocation_category", "anyof", "4"],
            "AND",
            ["job.status", "anyof", "2"],
            "AND",
            ["employee.custentity_employee_inactive", "is", "F"],
            "AND",
            ["startdate", "notafter", endDate], //+ 12 month
            "AND",
            ["enddate", "notbefore", startDate], // start date of month 
            "AND", 
            ["job.custentity_practice", "anyof", practiceArray]
            
        ]
    } else if (i_user && i_region) {
        filters = [
            ["job.custentity_project_allocation_category", "anyof", "4"],
            "AND",
            ["job.status", "anyof", "2"],
            "AND",
            ["employee.custentity_employee_inactive", "is", "F"],
            "AND",
            ["startdate", "notafter", endDate], //+ 12 month
            "AND",
            ["enddate", "notbefore", startDate]
        ]
    }else if (i_user && i_deliveryAnchore) {
	  var i_practice = i_deliveryAnchore[0].practice;
      var practiceArray = getSubPractices(i_practice);
        filters = [
            ["job.custentity_project_allocation_category", "anyof", "4"],
            "AND",
            ["job.status", "anyof", "2"],
            "AND",
            ["employee.custentity_employee_inactive", "is", "F"],
            "AND",
            ["startdate", "notafter", endDate], //+ 12 month
            "AND",
            ["enddate", "notbefore", startDate], // start date of month 
            "AND", 
            ["job.custentity_practice", "anyof", practiceArray]
            
        ]
    }  else if(i_user){
		var department = nlapiLookupField("employee",i_user,"department");
	nlapiLogExecution("ERROR","department",department);
	var parentPractice = nlapiLookupField("department",department,"custrecord_parent_practice");
	nlapiLogExecution("ERROR","parent practice :",parentPractice);
	var practiceArray = getSubPractices(parentPractice);
        filters = [
            ["job.custentity_project_allocation_category", "anyof", "4"],
            "AND",
            ["job.status", "anyof", "2"],
            "AND",
            ["employee.custentity_employee_inactive", "is", "F"],
            "AND",
            ["startdate", "notafter", endDate], //+ 12 month
            "AND",
            ["enddate", "notbefore", startDate], // start date of month 
            "AND", 
            ["job.custentity_practice", "anyof", practiceArray]
            
        ]
    }
    var columns = [
        new nlobjSearchColumn("resource", null, "GROUP"),
        new nlobjSearchColumn("altname", "customer", "GROUP"),
		new nlobjSearchColumn("customer", "job", "GROUP"),
        new nlobjSearchColumn("project", null, "GROUP"),
        new nlobjSearchColumn("custentity_practice", "job", "GROUP"),
        new nlobjSearchColumn("startdate", null, "GROUP"),
        new nlobjSearchColumn("enddate", null, "GROUP"),
        new nlobjSearchColumn("percentoftime", null, "GROUP")
    ]
    var resourceallocationSearch = searchRecord("resourceallocation", null, filters, columns, null);
    return resourceallocationSearch;
}

function _logValidation(value) {
    if (value != 'null' && value != null && value != null && value != '' && value != undefined && value != undefined && value != 'undefined' && value != 'undefined' && value != 'NaN' && value != NaN) {
        return true;
    } else {
        return false;
    }
}

function GetRevenueResult(i_user, i_practice, isAdmin, i_region,deliveryAnchore) {
    
	// Add the i_user id to search filter.
    var filters = [];
    var columns = [
        new nlobjSearchColumn("custrecord_sfdc_revenue_data_stage"),
        new nlobjSearchColumn("custrecord_sfdc_revenue_data_sowid"),
        new nlobjSearchColumn("custrecord_sfdc_revenue_data_startdate"),
        new nlobjSearchColumn("custrecord_sfdc_revenue_data_prostatus"),
        new nlobjSearchColumn("custrecord_sfdc_revenue_data_projectid"),
        new nlobjSearchColumn("custrecord_sfdc_revenue_data_enddate"),
        new nlobjSearchColumn("custrecord_sfdc_revenue_data_oppid"),
        new nlobjSearchColumn("custrecord_sfdc_revenue_data_netcustid"),
        new nlobjSearchColumn("custrecord_sfdc_revenue_data_accregion"),
        new nlobjSearchColumn("custrecord_sfdc_revenue_data_accname"),
        new nlobjSearchColumn("custrecord_sfdc_revenue_data_proj_id_ns"),
		new nlobjSearchColumn("created"),
		new nlobjSearchColumn("customer", "custrecord_sfdc_revenue_data_proj_id_ns", null),
        new nlobjSearchColumn("custrecord_sfdc_fuel_forecast_rev_amount", "CUSTRECORD_SFDC_FUEL_FORECAST_REV_EXT", null),
        new nlobjSearchColumn("custrecord_sfdc_fuel_fore_modayyear", "CUSTRECORD_SFDC_FUEL_FORECAST_REV_EXT", null),
        new nlobjSearchColumn("custrecord_sfdc_fuel_forecast_rev_pract", "CUSTRECORD_SFDC_FUEL_FORECAST_REV_EXT", null)
    ]
    if (isAdmin) {
        filters = ["isinactive", "is", "F"];
    } else if (i_user && i_practice) {
		var practiceArray = getSubPractices(i_practice);
      var practiceName = nlapiLookupField("department",i_practice,"name");
		nlapiLogExecution("ERROR","practiceName : ",practiceName);
        filters = [
            /*["custrecord_sfdc_revenue_data_proj_id_ns.custentity_projectmanager", "anyof", i_user], 
			"OR", 
			["custrecord_sfdc_revenue_data_opp_id.custrecord_sfdc_opp_pm", "anyof", i_user], 
			"OR", 
			["custrecord_sfdc_revenue_data_opp_id.custrecord_sfdc_opportunity_dm", "anyof", i_user], 
			"OR", 
			["custrecord_sfdc_revenue_data_proj_id_ns.custentity_deliverymanager", "anyof", i_user], 
			"OR", 
			["custrecord_sfdc_revenue_data_opp_id.custrecord_practice_internal_id_sfdc", "anyof", practiceArray],
			"OR",
			["custrecord_sfdc_revenue_data_proj_id_ns.custentity_practice", "anyof", practiceArray]*/
          ["custrecord_sfdc_fuel_forecast_rev_ext.custrecord_sfdc_fuel_forecast_rev_pract", "anyof", i_practice]
        ]
    } else if (i_user && i_region) {
        filters = [
            /*["custrecord_sfdc_revenue_data_proj_id_ns.custentity_projectmanager", "anyof", i_user], 
			"OR", 
			["custrecord_sfdc_revenue_data_opp_id.custrecord_sfdc_opp_pm", "anyof", i_user], 
			"OR", 
			["custrecord_sfdc_revenue_data_opp_id.custrecord_sfdc_opportunity_dm", "anyof", i_user], 
			"OR", 
			["custrecord_sfdc_revenue_data_proj_id_ns.custentity_deliverymanager", "anyof", i_user], 
			"OR", */
			["custrecord_sfdc_revenue_data_opp_id.custrecord_sfdc_opp_account_region", "anyof", i_region],
			"OR",
			["custrecord_sfdc_revenue_data_proj_id_ns.custentity_region", "anyof", i_region], 
			"OR", 
			["custrecord_sfdc_rev_data_ns_region", "anyof", i_region]
        ]
    } else if (i_user && deliveryAnchore) {
       var i_practice = deliveryAnchore[0].practice;
	   var practiceArray = getSubPractices(i_practice);
        filters = [
            /*["custrecord_sfdc_revenue_data_proj_id_ns.custentity_projectmanager", "anyof", i_user], 
			"OR", 
			["custrecord_sfdc_revenue_data_opp_id.custrecord_sfdc_opp_pm", "anyof", i_user], 
			"OR", 
			["custrecord_sfdc_revenue_data_opp_id.custrecord_sfdc_opportunity_dm", "anyof", i_user], 
			"OR", 
			["custrecord_sfdc_revenue_data_proj_id_ns.custentity_deliverymanager", "anyof", i_user], 
			"OR", 
			["custrecord_sfdc_revenue_data_opp_id.custrecord_practice_internal_id_sfdc", "anyof", practiceArray],
			"OR",
			["custrecord_sfdc_revenue_data_proj_id_ns.custentity_practice", "anyof", practiceArray]*/
          ["custrecord_sfdc_fuel_forecast_rev_ext.custrecord_sfdc_fuel_forecast_rev_pract", "anyof", i_practice]
        ]
    }else {
        filters = [
            ["custrecord_sfdc_revenue_data_proj_id_ns.custentity_projectmanager", "anyof", i_user], 
			"OR", 
			["custrecord_sfdc_revenue_data_opp_id.custrecord_sfdc_opp_pm", "anyof", i_user], 
			"OR", 
			["custrecord_sfdc_revenue_data_opp_id.custrecord_sfdc_opportunity_dm", "anyof", i_user], 
			"OR", 
			["custrecord_sfdc_revenue_data_proj_id_ns.custentity_deliverymanager", "anyof", i_user]
        ]
    }
    var revenue_dataSearch = searchRecord("customrecord_fuel_sfdc_revenue_data", null, filters, columns, null);
    return revenue_dataSearch;
}

function getStartDate() {
    var date = new Date();
    var startDate = new Date(date.getFullYear(), date.getMonth(), 1);
    return nlapiDateToString(startDate);
}

function getEndDate() {
    var date = new Date();
    var endDate = new Date(date.getFullYear(), date.getMonth() + 12, 0);
    return nlapiDateToString(endDate);
}

function GetBillableResources(billedProjects) {
    nlapiLogExecution("ERROR", "billedProjects", JSON.stringify(billedProjects));
    var resourceallocationSearch = nlapiSearchRecord("resourceallocation", null,
        [
            ["startdate", "notafter", getEndDate()],
            "AND",
            ["enddate", "notbefore", getStartDate()],
            "AND",
            ["project", "anyof", billedProjects],
            "AND",
            ["employee.isinactive", "is", "F"],
            "AND",
            ["job.status", "anyof", "2"]
        ],
        [
            
            new nlobjSearchColumn("resource", null, "GROUP"),
            new nlobjSearchColumn("employeestatus", "employee", "GROUP"),
            new nlobjSearchColumn("title", "employee", "GROUP"),
            new nlobjSearchColumn("altname", "customer", "GROUP"),
            new nlobjSearchColumn("entityid", "job", "GROUP"),
			new nlobjSearchColumn("customer", "job", "GROUP"),
            new nlobjSearchColumn("companyname", "job", "GROUP"),
            new nlobjSearchColumn("jobtype", "job", "GROUP"),
            new nlobjSearchColumn("custentity_practice", "job", "GROUP"),
            new nlobjSearchColumn("formulatext", null, "GROUP").setFormula("case when {custevent_ra_is_shadow} = 'T' then 'Shadow' else ' ' end"),
            new nlobjSearchColumn("custevent4", null, "GROUP"),
            new nlobjSearchColumn("startdate", null, "GROUP"),
            new nlobjSearchColumn("enddate", null, "GROUP"),
            new nlobjSearchColumn("jobbillingtype", "job", "GROUP"),
            new nlobjSearchColumn("custentity_t_and_m_monthly", "job", "GROUP"),
            new nlobjSearchColumn("custeventwlocation", null, "GROUP"),
            new nlobjSearchColumn("custevent_workcityra", null, "GROUP"),
            new nlobjSearchColumn("location", "employee", "GROUP"),
            new nlobjSearchColumn("custentity_projectmanager", "job", "GROUP"),
            new nlobjSearchColumn("custentity_deliverymanager", "job", "GROUP"),
            new nlobjSearchColumn("custentity_reportingmanager", "employee", "GROUP"),
            new nlobjSearchColumn("employeetype", "employee", "GROUP"),
            new nlobjSearchColumn("hiredate", "employee", "GROUP"),
            new nlobjSearchColumn("project", null, "GROUP")
        ]
    );
    return resourceallocationSearch;
}

function GetBilledFRFs(oppIds) {
    nlapiLogExecution("ERROR", "oppIds", JSON.stringify(oppIds));
    var customrecord_frf_detailsSearch = nlapiSearchRecord("customrecord_frf_details", null,
        [
            //["custrecord_frf_details_opp_id","anyof",oppIds],"AND",["custrecord_frf_details_billiable","is","T"]
        ],
        [
            new nlobjSearchColumn("custrecord_frf_details_start_date"),
            new nlobjSearchColumn("custrecord_frf_details_end_date"),
            new nlobjSearchColumn("custrecord_frf_details_allocated_emp"),
            new nlobjSearchColumn("custrecord_frf_details_opp_id")
        ]
    );
    return customrecord_frf_detailsSearch;
}

function getStartDate() {
    var date = new Date();
    var startDate = new Date(date.getFullYear(), date.getMonth(), 1);
    return nlapiDateToString(startDate);
}

function getEndDate() {
    var date = new Date();
    var endDate = new Date(date.getFullYear(), date.getMonth() + 12, 0);
    return nlapiDateToString(endDate);
}
function getSubPractices(i_practice){
	var temp = [];
	var departmentSearch = nlapiSearchRecord("department",null,
[
   ["isinactive","is","F"], 
   "AND", 
   ["custrecord_parent_practice","anyof",i_practice]
], 
[
]
);
if(departmentSearch){
	for(var i = 0 ; i < departmentSearch.length ;i++){
		temp.push(departmentSearch[i].getId());
	}
	return temp;
}else{
	return i_practice
}
}
//FUnction to get practice from spoc table
function GetPractice_SPOCList(user)

{

    var practiceArray = new Array();

    nlapiLogExecution('AUDIT', 'user', user);

    var spocSearch = nlapiSearchRecord("customrecord_practice_spoc", null,

        [

            ["custrecord_spoc", "anyof", user],

            "AND",

            ["isinactive", "is", "F"]

        ],

        [

            new nlobjSearchColumn("custrecord_practice")

        ]

    );

    if (spocSearch)

    {

        for (var i = 0; i < spocSearch.length; i++)

        {

            practiceArray.push(spocSearch[i].getValue('custrecord_practice'));

        }

    }

    return practiceArray;

}
function GetRegionId(user) 
{
  	try
	{
		nlapiLogExecution('AUDIT', 'user', user);
		var regionSearch = nlapiSearchRecord("customrecord_region",null,
				[
				[["custrecord_region_head","anyof",user],"OR",["custrecord_region_head_2","anyof",user]],
				  "AND",
				 ["isinactive","is","F"]
				], 
				[
				  new nlobjSearchColumn("name")
				]
		);
		if(regionSearch)
		{
			var region = regionSearch[0].getId();
			return region;
		}
		else
		{
			return '';
		}
		
	}
	catch(e)
	{
		return e; 
	}
	
}
function GetDeliveryAnchore(user)
{
	var temp = new Array();
	var customrecord_fuel_delivery_anchorSearch = nlapiSearchRecord("customrecord_fuel_delivery_anchor",null,
			[
				["custrecord_fuel_anchor_employee","anyof",user]
				], 
				[
					new nlobjSearchColumn("custrecord_fuel_anchor_customer"),
					new nlobjSearchColumn("custrecord_fuel_anchor_practice")
				]
	);	
	if(customrecord_fuel_delivery_anchorSearch)
	{
		for(var i = 0 ; i < customrecord_fuel_delivery_anchorSearch.length;i++)
		{
			temp.push({"customer":customrecord_fuel_delivery_anchorSearch[i].getValue('custrecord_fuel_anchor_customer'),"practice":customrecord_fuel_delivery_anchorSearch[i].getValue('custrecord_fuel_anchor_practice')});
		}
	}
	nlapiLogExecution('Debug','Delivery Anchor Array Length ',temp.length);
	return temp;
}