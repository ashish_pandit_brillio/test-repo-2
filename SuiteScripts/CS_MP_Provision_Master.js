function pageInit(type) {

	if (type == 'create') {
		// set the effective date as the current date
		nlapiSetFieldValue('custrecord_pm_effective_date', nlapiDateToString(
		        new Date(), 'date'));
	}
}

function validateFieldChange(type, name, linenum) {

	// to date cannot be before from date
	if (name == 'custrecord_pm_from_date' || name == 'custrecord_pm_to_date') {
		var fromDate = nlapiGetFieldValue('custrecord_pm_from_date');
		var toDate = nlapiGetFieldValue('custrecord_pm_to_date');

		if (fromDate && toDate) {
			fromDate = nlapiStringToDate(fromDate);
			toDate = nlapiStringToDate(toDate);

			if (toDate <= fromDate) {
				alert("To Date has to be after from date");
				return false;
			}
		}
	}

	// JE reversal date is to after the from date
	if (name == 'custrecord_pm_from_date'
	        || name == 'custrecord_pm_reversal_date') {
		var fromDate = nlapiGetFieldValue('custrecord_pm_from_date');
		var reversalDate = nlapiGetFieldValue('custrecord_pm_reversal_date');

		if (fromDate && reversalDate) {
			fromDate = nlapiStringToDate(fromDate);
			reversalDate = nlapiStringToDate(reversalDate);

			if (reversalDate <= fromDate) {
				alert("JE Reversal Date has to be after from date");
				return false;
			}
		}
	}

	// Both debit and credit account cannot be same
	if (name == 'custrecord_pm_acct_debit'
	        || name == 'custrecord_pm_acct_credit') {
		var debitAccount = nlapiGetFieldValue('custrecord_pm_acct_debit');
		var creditAccount = nlapiGetFieldValue('custrecord_pm_acct_credit');

		if (debitAccount && creditAccount && debitAccount == creditAccount) {
			alert("Debit and Credit Accounts cannot be same");
			return false;
		}
	}

	// check if the master provision for the same month, year and subsidiary
	// already exists
	if (name == 'custrecord_pm_month' || name == 'custrecord_pm_year'
	        || name == 'custrecord_pm_subsidiary') {
		var month = nlapiGetFieldValue('custrecord_pm_month');
		var year = nlapiGetFieldValue('custrecord_pm_year');
		var subsidiary = nlapiGetFieldValue('custrecord_pm_subsidiary');

		if (month && year && subsidiary) {
			var duplicateSearch = nlapiSearchRecord(
			        'customrecord_provision_master',
			        null,
			        [
			                new nlobjSearchFilter('isinactive', null, 'is', 'F'),
			                new nlobjSearchFilter('custrecord_pm_month', null,
			                        'anyof', month),
			                new nlobjSearchFilter('custrecord_pm_year', null,
			                        'anyof', year),
			                new nlobjSearchFilter('custrecord_pm_subsidiary',
			                        null, 'anyof', subsidiary) ]);

			if (duplicateSearch) {
				alert("Another Master Provision Entry already exists for this general details.");
				return false;
			}
		}
	}

	return true;
}

function goToStatusPage() {
	try {
		var internalId = nlapiGetRecordId();
		var url = nlapiResolveURL('SUITELET',
		        'customscript_sut_mp_process_status',
		        'customdeploy_sut_mp_process_status')
		        + "&master=" + internalId;
		window.location = url;
	} catch (err) {
		alert(err.message);
	}
}