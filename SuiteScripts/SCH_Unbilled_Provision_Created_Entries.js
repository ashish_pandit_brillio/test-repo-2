/**
 * @author Shweta
 */
// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
/*
   	Script Name : SCH_Unbilled_Provision_Created_Entries.js
	Author      : Shweta Chopde
	Date        : 16 May 2014
	Description : Update Time Track / Expense Report entries as unchecked it its status is unbilled & already a provision is created


	Script Modification Log:

	-- Date --			-- Modified By --				--Requested By--				-- Description --




Below is a summary of the process controls enforced by this script file.  The control logic is described
more fully, below, in the appropriate function headers and code blocks.


     SCHEDULED FUNCTION
		- scheduledFunction(type)


     SUB-FUNCTIONS
		- The following sub-functions are called by the above core functions in order to maintain code
            modularization:

               - NOT USED

*/
}
// END SCRIPT DESCRIPTION BLOCK  ====================================


// BEGIN SCHEDULED FUNCTION =============================================

function schedulerFunction(type)
{
    try
	{
	 //======================= Time Track =========================	 
	
	// search_time_track_with_provision_created()
	
	 search_time_track_with_provision_created_3()	 
	
	//==================== Expense Report =========================
	
	search_expense_with_provision_created()
		
			
	}
    catch(err)
    {
	  nlapiLogExecution('DEBUG', 'ERROR',' Exception Caught -->' + err);	
	  Send_Exception_Mail(err);				
    }//CATCH 
}

// END SCHEDULED FUNCTION ===============================================



// BEGIN FUNCTION ===================================================

function search_time_track_with_provision_created()
{ 	
   nlapiLogExecution('DEBUG', 'search_time_track_with_provision_created',' In Time Block ................ ');	 
	
	try
	{
	    var filter = new Array();
    //   filter[0] = new nlobjSearchFilter('internalid', null, 'is',202);
						
		var columns = new Array();
        columns[0] = new nlobjSearchColumn('internalid');
		columns[1] = new nlobjSearchColumn('custrecord_time_track_pr');
		
	//	 var a_search_results = searchRecord('customrecord_timeexpenseprovision',null,filter,columns);
				
	   var a_search_results = searchRecord('customrecord_timeexpenseprovision','customsearch_jvreversedateontoday_2',filter,columns);
	   nlapiLogExecution('DEBUG', 'search_time_track_with_provision_created',' Time Track & Expense Search Length -->' +a_search_results.length);	 
		 if(_logValidation(a_search_results))
		 {
		 	 nlapiLogExecution('DEBUG', 'search_time_track_with_provision_created',' Time Track & Expense Search Length -->' +a_search_results.length);	 
			 
			 for(var i=0;i<a_search_results.length;i++)
			 {
			 	var i_internalID = a_search_results[i].getValue('internalid');
				
				var i_time_trackID = a_search_results[i].getValue('custrecord_time_track_pr');
				
				 if(_logValidation(i_internalID))
				 {
					 i_time_trackID=i_time_trackID.split(",");
					 for(var kk=0 ; kk<i_time_trackID.length ; kk++)
						 {
						   nlapiLogExecution('DEBUG', 'i_time_trackID[kk]',' i_time_trackID[kk]' +i_time_trackID[kk]);	 
						 	var timeOBJ = nlapiLoadRecord('timebill',i_time_trackID[kk]);
					
							 if(_logValidation(timeOBJ))
							 {
							 	timeOBJ.setFieldValue('custcol_is_provision_created_tr','F');
								
								var i_time_submitID = nlapiSubmitRecord(timeOBJ,true,true);
								nlapiLogExecution('DEBUG', 'search_time_track_with_provision_created',' ********************* Time Track Submit ID ********************* -->' +i_time_submitID);	 
					 											
							 }//Time Track OBJ
						 }
				 	
				 }//Internal ID
			 	
			 }//Loop			 	
		 }//Search Results
	}//TRY
	catch(exception)
	{
	  nlapiLogExecution('DEBUG', 'ERROR',' Exception Caught -->' + exception);					
	}//CATCH
	
}//Search time Track With Provision Created
//-------------------------------------added by swati----------------------------------------------
function search_time_track_with_provision_created_3()
{ //Search time Track With Provision Created criteria 3	
	
   nlapiLogExecution('DEBUG', 'search_time_track_with_provision_created_for_criteria_3',' In Custom Time Block ................ ');	 
	
	try
	{
	    var filter = new Array();
    //   filter[0] = new nlobjSearchFilter('internalid', null, 'is',202);
						
		var columns = new Array();
        columns[0] = new nlobjSearchColumn('internalid');
		columns[1] = new nlobjSearchColumn('custrecord_time_bill');
		
	//	 var a_search_results = searchRecord('customrecord_timeexpenseprovision',null,filter,columns);
				
	   var a_search_results = searchRecord('customrecord_time_bill_rec','customsearch_monthly_provision_allocatio',null,null);
		 
		 if(_logValidation(a_search_results))
		 {
		 	 nlapiLogExecution('DEBUG', 'search_time_track_with_provision_created',' Time Track & Expense Search Length -->' +a_search_results.length);	 
			 
			 for(var i=0;i<a_search_results.length;i++)
			 {
			 	var i_time_trackID = a_search_results[i].getId();
				
				//var i_time_trackID = a_search_results[i].getValue('custrecord_time_bill');
				
				 if(_logValidation(i_time_trackID))
				 {
				 	var timeOBJ = nlapiLoadRecord('customrecord_time_bill_rec',i_time_trackID);
					
					 if(_logValidation(timeOBJ))
					 {
					 	timeOBJ.setFieldValue('custrecord_processed','F');
						
						var i_time_submitID = nlapiSubmitRecord(timeOBJ,true,true);
						nlapiLogExecution('DEBUG', 'search_time_track_with_provision_created_for_criteria_3',' ********************* Time Track Submit ID ********************* -->' +i_time_submitID);	 
			 											
					 }//Time Track OBJ
				 	
				 }//Internal ID
			 	
			 }//Loop			 	
		 }//Search Results
	}//TRY
	catch(exception)
	{
	  nlapiLogExecution('DEBUG', 'ERROR',' Exception Caught -->' + exception);					
	}//CATCH
	
}//Search time Track With Provision Created criteria 3

//-------------------------------------------------------------------------------------------------

function search_expense_with_provision_created()
{
	var i_internalID;
	
	nlapiLogExecution('DEBUG', 'search_expense_with_provision_created',' In Expense Block ................ ');	 
		
	try
	{
	    var filter = new Array();
	   // filter[0] = new nlobjSearchFilter('internalid', null, 'is',301);
		
		var columns = new Array();
        columns[0] = new nlobjSearchColumn('internalid');
		columns[1] = new nlobjSearchColumn('custrecord_expense_report_pr');
		
	    var a_search_results = searchRecord('customrecord_timeexpenseprovision','customsearch_jvreversedateontoday',filter,columns);
		//   var a_search_results = searchRecord('customrecord_timeexpenseprovision',null,filter,columns);
		 
		 if(_logValidation(a_search_results))
		 {
		 	 nlapiLogExecution('DEBUG', 'search_expense_with_provision_created',' Expense Report Search Length -->' +a_search_results.length);	 
			 
			 for (var i = 0; i < a_search_results.length; i++) 
			 {
			   var i_internalID = a_search_results[i].getValue('internalid');
			   nlapiLogExecution('DEBUG', 'i_internalID',i_internalID);	 
				
				//var i_expense_reportID = a_search_results[i].getId();
			 	
			 	if (_logValidation(i_internalID)) 
				{
			 		var o_expenseOBJ = nlapiLoadRecord('customrecord_timeexpenseprovision',i_internalID);
			 		
			 		if (_logValidation(o_expenseOBJ)) 
					{
			 			var exp_val=o_expenseOBJ.getFieldValues('custrecord_expense_report_pr');
			 			for(var j=0 ; j<exp_val.length ; j++)
			 				{
			 				
			 				       var exp_load=nlapiLoadRecord('expensereport', exp_val[j]);
			 				      exp_load.setFieldValue('custbody_is_provision_created', 'F');
			 			
			 						var i_expense_submitID = nlapiSubmitRecord(exp_load,true,true);
			 						nlapiLogExecution('DEBUG', 'search_expense_with_provision_created',' ********************* Expense Report Submit ID ********************* -->' +i_expense_submitID);
			 				}
						
						}//Expense Report OBJ
					}//Internal ID
				}	
					 	
		 }//Search Results
	}//TRY
	catch(exception)
	{
	  nlapiLogExecution('DEBUG', 'ERROR',' Exception Caught -->' + exception);					
	}//CATCH
	
}//Search Expense With Provision Created

/**
 * 
 * @param {Object} value
 * 
 * Description --> If the value is blank /null/undefined returns false else returns true
 */
function _logValidation(value) 
{
 if(value != null && value.toString() != null && value != '' && value != undefined && value.toString() != undefined && value != 'undefined' && value.toString() != 'undefined'&& value.toString() != 'NaN' && value != NaN) 
 {
  return true;
 }
 else 
 { 
  return false;
 }
}
// END FUNCTION =====================================================

function Send_Exception_Mail(err)
{​​​​​
    try
    {​​​​​   
        var context = nlapiGetContext();
        var s_DeploymentID =context.getDeploymentId();
        var s_ScriptID =context.getScriptId();
        var s_Subject ='Issue on ';
        
        var s_Body = 'This is to inform that is having an issue and System is not able to send the email notification to employees.';
        s_Body += '<br/><b>Issue: Code:</b> '+err.code+' Message: '+err.message;
        s_Body += '<br/><b>Script: </b>'+s_ScriptID;
        s_Body += '<br/><b>Deployment:</b> '+s_DeploymentID;
        s_Body += '<br/>Please have a look and do the needfull.';
        s_Body += '<br/><br/>Note: This is system genarated email incase of any failure while sending the notification to employees';

        s_Body += '<br/><br/>Regards,';
        s_Body += '<br/>Digital Office';
        nlapiSendEmail(442,"netsuite.support@brillio.com",s_Subject,s_Body,null, null, null, null);
    }​​​​​
    catch(err)
    {​​​​​
        nlapiLogExecution('error',' Send_Exception_Mail', err.message); 
		
    }​​​​​
}​​​​​

