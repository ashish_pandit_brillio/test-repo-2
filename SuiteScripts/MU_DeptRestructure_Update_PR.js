/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       05 Feb 2015     amol.sahijwani
 *
 */

/**
 * @param {String} recType Record type internal id
 * @param {Number} recId Record internal id
 * @returns {Void}
 */
function massUpdate(recType, recId) {
	var record = nlapiLoadRecord(recType, recId);
	
	var i_employee_id = record.getFieldValue('custrecord_employee');
	
	var i_practice = nlapiLookupField('employee', i_employee_id, 'department');
	
	var i_project_id = record.getFieldValue('custrecord_project');
	
	var i_vertical = null;
	
	var i_end_customer = nlapiLookupField('job',i_project_id,'custentity_endcustomer');

	var i_vertical = null;

	if(i_end_customer != '' && i_end_customer != null && i_end_customer != undefined)
		{
			i_vertical = nlapiLookupField('job', i_project_id, 'custentity_vertical');
		}
	else
		{
			var i_customer = nlapiLookupField('job',i_project_id,'customer');
			i_vertical = nlapiLookupField('customer', i_customer, 'custentity_vertical');
		}
	
	record.setFieldValue('custrecord_prpractices', i_practice);
	record.setFieldValue('custrecord_verticals', i_vertical);
	
	try
	{
		nlapiSubmitRecord(record);// Record(record);
		nlapiLogExecution('AUDIT', 'Record Saved', recId);
	}
	catch(e)
	{
		nlapiLogExecution('ERROR', 'Error Saving Record: ' + recId, e.message);
	}
}
