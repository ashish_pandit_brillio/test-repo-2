/**
 * Vendor File Upload
 * 
 * Version Date Author Remarks 1.00 15 Apr 2016 Nitish Mishra
 * 
 */

function suitelet(request, response) {
	try {

		if (request.getMethod() == "GET") {
			createFileUploadScreen(request);
		} else {
			submitFileUploadScreen(request);
		}
	} catch (err) {
		nlapiLogExecution('ERROR', 'suitelet', err);
		displayErrorForm('File Upload', err);
	}
}

function createFileUploadScreen(request) {
	try {
		var vendorId = getVendorId(request);

		if (vendorId) {
			var vendorRec = nlapiLoadRecord('customrecord_contract_management',
			        vendorId);
			var form = nlapiCreateForm('File Upload : '
			        + vendorRec.getFieldValue('name'));

			form.addField('custpage_vendor_id', 'select', 'Vendor',
			        'customrecord_contract_management', null).setDisplayType(
			        'hidden').setDefaultValue(vendorId);

			form.addField('custpage_cms_file_csa', 'file',
			        'Consulting Service Agreement (CSA)', null, null);
			form.addField('custpage_cms_file_billing_guidelines', 'file',
			        'Billing Guidelines', null, null);
			form.addField('custpage_cms_file_addendum', 'file',
			        'Addendum to CSA', null, null);

			form.addField('custpage_cms_file_form_w_9', 'file', 'Form W 9',
			        null, null);
			form.addField('custpage_cms_file_minority_survey', 'file',
			        'Minority Survey Information', null, null);

			form.addField('custpage_csa_tax_form', 'select', 'CSA Tax Form',
			        'customrecord_csa_tax_forms', null);

			form.addField('custpage_cms_file_ach_form', 'file', 'ACH Form',
			        null, null);

			form.addResetButton('Reset');
			form.addSubmitButton('Submit');

			response.writePage(form);
		} else {
			throw "Vendor Id Not Found";
		}
	} catch (err) {
		nlapiLogExecution('ERROR', 'createFileUploadScreen', err);
		throw err;
	}
}

function getVendorId(request) {
	try {
		var email = request.getParameter("e");
		var requestAccessToken1 = request.getParameter("t1");
		var requestAccessToken2 = request.getParameter("t2");

		if (email && requestAccessToken1 && requestAccessToken2) {
			var vendorSearch = nlapiSearchRecord(
			        'customrecord_contract_management', null,
			        [ new nlobjSearchFilter('custrecord_cms_admin_email', null,
			                'is', email) ], [
			                new nlobjSearchColumn(
			                        'custrecord_cms_login_access_token_1'),
			                new nlobjSearchColumn(
			                        'custrecord_cms_login_access_token_2') ]);

			if (vendorSearch) {
				var storedAccessToken1 = vendorSearch[0]
				        .getValue('custrecord_cms_login_access_token_1');
				var storedAccessToken2 = vendorSearch[0]
				        .getValue('custrecord_cms_login_access_token_2');

				if (storedAccessToken1 == requestAccessToken1
				        && storedAccessToken2 == requestAccessToken2) {
					return vendorSearch[0].getId();
				} else {
					throw "Invalid Access";
				}
			}
		} else {
			throw "Access not allowed";
		}
	} catch (err) {
		nlapiLogExecution('ERROR', 'getVendorId', err);
		throw err;
	}
}

function submitFileUploadScreen(request) {
	try {
		var vendorId = request.getParameter('custpage_vendor_id');

		if (vendorId) {

			// Create a new vendor record
			var vendorRec = nlapiLoadRecord('customrecord_contract_management',
			        vendorId);

			var folderId = vendorRec.getFieldValue('custrecord_cms_folder_id');

			vendorRec.setFieldValue('custrecord_cms_file_csa',
			        saveFileToCabinet(request.getFile('custpage_cms_file_csa'),
			                folderId));
			vendorRec.setFieldValue('custrecord_cms_file_billing_guidelines',
			        saveFileToCabinet(request
			                .getFile('custpage_cms_file_billing_guidelines'),
			                folderId));
			vendorRec.setFieldValue('custrecord_cms_file_addendum',
			        saveFileToCabinet(request
			                .getFile('custpage_cms_file_addendum'), folderId));
			vendorRec.setFieldValue('custrecord_cms_file_page_1_incorporation',
			        saveFileToCabinet(request
			                .getFile('custpage_cms_file_page_1_incorporation'),
			                folderId));
			vendorRec.setFieldValue('custrecord_cms_file_form_w_9',
			        saveFileToCabinet(request
			                .getFile('custpage_cms_file_form_w_9'), folderId));
			vendorRec.setFieldValue('custrecord_cms_file_minority_survey',
			        saveFileToCabinet(request
			                .getFile('custpage_cms_file_minority_survey'),
			                folderId));
			vendorRec.setFieldValue('custrecord_csa_tax_form', request
			        .getParameter('custpage_csa_tax_form'));

			// vendorRec.setFieldValue('custrecord_cms_file_ca_tax_forms',
			// saveFileToCabinet(request
			// .getFile('custpage_cms_file_ca_tax_forms'),folderId ));

			vendorRec.setFieldValue('custrecord_cms_file_insurance_certificat',
			        saveFileToCabinet(request
			                .getFile('custpage_cms_file_insurance_certificat'),
			                folderId));
			vendorRec.setFieldValue('custrecord_cms_file_ach_form',
			        saveFileToCabinet(request
			                .getFile('custpage_cms_file_ach_form'), folderId));

			var vendorId = nlapiSubmitRecord(vendorRec);

			// send a notification mail to the contract admin

			// create a response form
			var form = nlapiCreateForm('Vendor File Upload - Contract Admin : '
			        + vendorRec.getFieldValue('name'));
			form.addField("custpage_1", "inlinehtml", "").setDefaultValue(
			        "Files Submitted");
			response.writePage(form);
		} else {
			throw "Vendor ID Not Provided";
		}
	} catch (err) {
		nlapiLogExecution('ERROR', 'submitFileUploadScreen', err);
		throw err;
	}
}

function saveFileToCabinet(fileObj, folderId) {
	try {
		if (fileObj) {
			fileObj.setFolder(folderId);
			var fileId = nlapiSubmitFile(fileObj);
			return fileId;
		} else {
			return null;
		}
	} catch (err) {
		nlapiLogExecution('ERROR', 'saveFileToCabinet', err);
		throw err;
	}
}

function displayErrorForm(title, err) {
	var form = nlapiCreateForm(title);
	form.addFieldGroup('custpage_message', 'Some error occured');
	form.addField('custpage_message', 'inlinehtml', 'Message', null,
	        'custpage_message').setDefaultValue(err);
	response.writePage(form);
}