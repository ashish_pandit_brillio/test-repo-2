function afterSubmitResourceAllocation(type){
	try
	{
		if(type == 'create')
		{
			var employee_internalid = nlapiGetFieldValue('allocationresource');
			var resourceallocationSearch = nlapiSearchRecord("resourceallocation",null,
		[
			["employee.internalid","anyof",employee_internalid]
		], 
		[
			new nlobjSearchColumn("entityid","job",null), 
			new nlobjSearchColumn("resource"), 
			new nlobjSearchColumn("altname","customer",null), 
			new nlobjSearchColumn("email","employee",null), 
			new nlobjSearchColumn("formulatext").setFormula("CASE WHEN INSTR({employee.department} , ' : ', 1) > 0 THEN SUBSTR({employee.department}, 1 , INSTR({employee.department} , ' : ', 1)) ELSE {employee.department} END"), 
			new nlobjSearchColumn("departmentnohierarchy","employee",null), 
			new nlobjSearchColumn("startdate").setSort(true), 
			new nlobjSearchColumn("enddate"),
			new nlobjSearchColumn("custentity_projectmanager","job",null)
		]
		);
			var employee_name = nlapiGetFieldText('allocationresource');
			var allocation_start_date = nlapiGetFieldValue('startdate');
			var allocation_end_date = nlapiGetFieldValue('enddate');
			var project = nlapiGetFieldText('project');
			var customer_name = resourceallocationSearch[0].getValue('altname','customer');
			var emp_email = resourceallocationSearch[0].getValue('email','employee');
			var emp_practice = resourceallocationSearch[0].getText('departmentnohierarchy','employee');
			var old_project_id = '';
			var old_project_name = '';
			if(resourceallocationSearch.length > 1)
			{
			 old_project_id = resourceallocationSearch[1].getValue('entityid','job');
			 old_project_name = resourceallocationSearch[1].getText('altname','job');
			
				/*var PM_internalid = resourceallocationSearch[0].getValue('custentity_projectmanager','job');
			
				var PM_mail_search = nlapiSearchRecord("employee",null,
				[
					["internalid","anyof",PM_internalid]
				], 
				[
					new nlobjSearchColumn("email")
				]
				);
			
				var PM_mail = PM_mail_search[0].getValue('email');*/
			
				/*var cc = new Array();
				cc[0] = 'Gopi.Krishnan@brillio.com';
				cc[1] = 'shamanth.k@brillio.com';*/
				var strVar = '';
				strVar += '<body>';
           
				strVar += '<p>Dear team,</p>';
				strVar += '<p>'+employee_name +' has been allocated to the project '+project+ ' from '+ old_project_id + ' '+ old_project_name + ' project as of ' + allocation_start_date + ' till '+ allocation_end_date + ' .';
				strVar += '<p>Customer Name : ' + customer_name + '</p>';
				strVar += '<p> Employee Email : ' + emp_email +'</p>';
				strVar += '<p> Employee Department : ' + emp_practice +'</p>';
				strVar += '</body>';
				strVar += '</html>';
				var email = ['sridhar.s@BRILLIO.COM','Gopi.Krishnan@brillio.com'];
			
				nlapiSendEmail(442,email,'New Project Allocation',strVar,null,null,null)
			}
		
		}
	}

	catch(err)
	{
		nlapiLogExecution('debug','Error in the main process',err);
		throw err;
	}
}

function _logValidation(value) {
    if (value != null && value != '' && value != undefined && value.toString() != undefined && value.toString() != 'NaN' && value != NaN && value != 'undefined' && value != ' ') {
        return true;
    } else {
        return false;
    }
}