function onclick_send_mail()
{
	try{
					  
				
				
				
				var line_count = nlapiGetLineItemCount('custpage_expense_report_detail');
					
                    var mailCount = 0;
																		
					  var searchresult_exp_rpt = nlapiSearchRecord('transaction', 'customsearch_in_progress_expense_report', null, null);
					
                    for(var k=1;k<=line_count;k++)						
					{			
										
						
						if(nlapiGetLineItemValue('custpage_expense_report_detail','custpage_check_box',k)== 'T')
						{    
					            var emp_name = nlapiGetLineItemValue('custpage_expense_report_detail','custpage_emp_name',k);
								
								var name = emp_name.split('-')[1];
								
								if(name == undefined || name == null)
								{
									var name = emp_name.split('-')[0];
                                }
								
						     for(var i=0;i<searchresult_exp_rpt.length;i++)
					         {
								 var sub_doc_internal_id = nlapiGetLineItemValue('custpage_expense_report_detail','custpage_doc_internal_id',k);
								 
								var a_search_exp_data = searchresult_exp_rpt[i];
								
								var all_columns = a_search_exp_data.getAllColumns();
								
								var doc_internal_id = a_search_exp_data.getValue(all_columns[8]);
								
								var src_doc_internal_id = doc_internal_id;
								
								if(sub_doc_internal_id == src_doc_internal_id)
								{								 
									 var a_search_exp_data = searchresult_exp_rpt[i];
									 var all_columns = a_search_exp_data.getAllColumns();
									 var emp_email = a_search_exp_data.getValue(all_columns[3]);			
			
						 
									var strVar = '';
									var j = 1;
									var subject ='Expense pending..';
									
									
									
									strVar += "<table>"; 
									strVar += "<tr>";
									strVar += "<th scope='col'>S.No.</th>";
									strVar += "<th scope='col'>Expense Id</th>";
									strVar += "<th scope='col'>Amount</th>";
									strVar += "<th scope='col'>Currency</th>";
									strVar += "</tr>";
									strVar += "<tr>";
									strVar += "<td>" + j + "</td>";
									strVar += "<td>" + nlapiGetLineItemValue('custpage_expense_report_detail','custpage_doc_no',k)+"</td>";
									strVar += "<td>" + nlapiGetLineItemValue('custpage_expense_report_detail','custpage_amount',k)+"</td>";
									strVar += "<td>" + nlapiGetLineItemValue('custpage_expense_report_detail','custpage_currency',k)+"</td>";
									strVar += "</tr>";
									strVar += "</table>";
									
									var body = 'Hi ' + name + ', <br /><br />' +  'We have noticed that the expense '+ nlapiGetLineItemValue('custpage_expense_report_detail','custpage_doc_no',k) +' claim  have been kept under in-progress status for more than two months.'+
									' We request you to submit the claim for approval at the earliest else it will be deleted from Netsuite by 25-Aug-2019.<br /> <br />'
									+ '<br /><br /><b>Note: Please reach out directly to Santosh Banadawar at santosh.banadawar@brillio.com should there be a clarification or concern on the above action.</b><br /><br /> Thanks & Regards,<br />Santosh Banadawar<br />Associate Manager Finance<br />Office +918040136157 Mobile +919481882041 ';  //60 days from creation date : '+nlapiGetLineItemValue('custpage_expense_report_detail','custpage_date',select_count)+ '.<br /><br />'+ strVar; 
									
									var a_emp_attachment = new Array();
									a_emp_attachment['entity'] =  '131052';//'140512';                                   

				 

									nlapiSendEmail(7074,emp_email,subject,body,'santosh.banadawar@brillio.com','prabhat.gupta@brillio.com',a_emp_attachment,null);	
						
									nlapiLogExecution('Debug','sent email '+ 'line no -> '+ k,emp_email);
									mailCount++;
									break;
								}
							 }
							 
							 
					
						}
						
						
					}
					alert(mailCount + " mail sent");
					
					
	    }
	
		catch(e)
		{
			nlapiLogExecution('Debug','process error',e);
		}
}

function onclick_select_all()
{
	line_count = nlapiGetLineItemCount('custpage_expense_report_detail');
	
	for(var i=1;i<=25;i++)
	{
		nlapiSetLineItemValue('custpage_expense_report_detail','custpage_check_box',i,'T');
	}
	
}

function onclick_delete()
{
	
	try
	{
			line_count = nlapiGetLineItemCount('custpage_expense_report_detail');
			
			var number_of_select = 0;
					for(var m=1;m<=25;m++)
					{
						
						if(nlapiGetLineItemValue('custpage_expense_report_detail','custpage_check_box',m)== 'T')
						{
							number_of_select++;
						}
					}
			
			
			var searchresult_exp_rpt = nlapiSearchRecord('transaction', 'customsearch_in_progress_expense_report', null, null);
			
			var n=0;
			for(var i=1;i<=line_count;i++)
			{
				if(nlapiGetLineItemValue('custpage_expense_report_detail','custpage_check_box',i) == 'T')
				{
					for(var j=0;j<searchresult_exp_rpt.length;j++)
					{
						var sub_doc_internal_id = nlapiGetLineItemValue('custpage_expense_report_detail','custpage_doc_internal_id',i);
						
						var a_search_exp_data = searchresult_exp_rpt[j];
						var all_columns = a_search_exp_data.getAllColumns();
						var expense_id = a_search_exp_data.getValue(all_columns[8]);
						var doc_no = a_search_exp_data.getValue(all_columns[1]);
						
						var src_doc_no = doc_no;
						
					    if(sub_doc_internal_id == expense_id)
						{
												
							nlapiLogExecution('Debug','deleted expenese id ',expense_id);
							nlapiLogExecution('Debug','deleted document number ',doc_no);						
							
							nlapiDeleteRecord('expensereport', expense_id);	
							break;
						}						
					}
					n++;
				}
				
				if(n==number_of_select)
				{
					
					break;
				}
				
			}
			
			//var url='https://3883006-sb1.app.netsuite.com/app/site/hosting/scriptlet.nl?script=1873&deploy=1';
			var url = 'https://3883006.app.netsuite.com/app/site/hosting/scriptlet.nl?script=1923&deploy=1';
		    window.open(url,'_self');
			
			
	}
	catch(e)
		{
			nlapiLogExecution('Debug','process error',e);
			
		}
	
}