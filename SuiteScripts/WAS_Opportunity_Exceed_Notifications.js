/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       20 Apr 2021     shravan.k
 *
 */

/**
 * @returns {Void} Any or no return value
 */
function Send_Mail_on_Project()
{
	try
	{
		var i_Record_id = nlapiGetRecordId();
		nlapiLogExecution('DEBUG',' i_Record_id==', i_Record_id);	
		var obj_Old_Record = nlapiGetOldRecord()
		var d_End_Date_Old = null;
		if(obj_Old_Record)
			{
				 d_End_Date_Old = obj_Old_Record.getFieldValue('enddate');
				d_End_Date_Old = nlapiStringToDate(d_End_Date_Old);
				nlapiLogExecution('DEBUG',' d_End_Date_Old==', d_End_Date_Old);
			}
		var d_End_Date_New = nlapiGetFieldValue('enddate');
		d_End_Date_New = nlapiStringToDate(d_End_Date_New);
		nlapiLogExecution('DEBUG',' d_End_Date_New==', d_End_Date_New);
		if(   ( (d_End_Date_Old)  && ( d_End_Date_New != d_End_Date_Old ) ) ||  (!d_End_Date_Old))
			{
					var d_Start_Date_project = nlapiGetFieldValue('startdate');
					d_Start_Date_project = nlapiStringToDate(d_Start_Date_project);
					nlapiLogExecution('DEBUG',' d_Start_Date_project==', d_Start_Date_project);
					var i_Year = d_Start_Date_project.getFullYear();
					nlapiLogExecution('DEBUG',' i_Year==', i_Year);
					var i_Year_Next = parseInt(i_Year) + 1 ;
					var d_Deadline_Date = '12/31/'+ i_Year_Next;
					d_Deadline_Date  = nlapiStringToDate(d_Deadline_Date);
					nlapiLogExecution('DEBUG',' d_Deadline_Date==', d_Deadline_Date);
					if(d_End_Date_New >d_Deadline_Date )
						{
								nlapiLogExecution('DEBUG', 'Sending Mail ==', null);
								var s_Customer_Name = nlapiGetFieldText('parent');
								var s_Project_Id = nlapiGetFieldValue('entityid');
								var s_Project_Name = nlapiGetFieldValue('companyname');
								var obj_Ref_File = nlapiLoadFile('3799137') /// 3086022 for sandbox
								func_Send_Mail(s_Customer_Name,s_Project_Id,s_Project_Name,obj_Ref_File)
						} /// if(d_End_Date_New >d_Deadline_Date )
			} /// 	if(   ( (d_End_Date_Old)  && ( d_End_Date_New != d_End_Date_Old ) ) ||  (!d_End_Date_Old))
	} //// End of try
	catch(s_Exception)
	{
		nlapiLogExecution('DEBUG', 's_Exception== in Send_Mail_on_Project ', s_Exception)
	} //// End of catch
} ///// End of function Send_Mail_on_Project()
function func_Send_Mail(s_Customer_Name,s_Project_Id,s_Project_Name,obj_Ref_File)
{
	try
	{
		var arr_records = new Array();
		arr_records['entity'] = 442; //// IS sytems
		var arr_Recipients = new Array();
		arr_Recipients.push('nishant.panda@brillio.com')
		arr_Recipients.push('krishna.d@brillio.com')
		arr_Recipients.push('Ananda.Hirekerur@brillio.com')
		arr_Recipients.push('sapan.shah@brillio.com')
		arr_Recipients.push('deepak.srinivas@brillio.com')
		arr_Recipients.push('chetan.barot@brillio.com')
		var s_Subject ='Opportunity with more than 24 months revenue Customer : ' + ' ' + s_Customer_Name;
		var s_Body = 'Please note that below opportunity is a closed win and total contract value is spread across more than 24 months.'
					s_Body +=  'Please take necessary action from your end and work with PMs to setup the project and revenue in the system.'
						s_Body += '<br/> Project ID :  ' + ' ' + s_Project_Id;
					s_Body += '<br/> Project Name :  ' + ' ' + s_Project_Name;
					s_Body += '<br/> Customer Name :  ' + ' ' + s_Customer_Name;
					s_Body += '<br/>Attached guidelines for your reference';
		nlapiSendEmail(442,arr_Recipients, s_Subject, s_Body, null, null, arr_records, obj_Ref_File);
	} /// end of try
	catch(s_Exception)
	{
		nlapiLogExecution('DEBUG', 's_Exception== in func_Send_Mail ', s_Exception)
	} //// End of catch

} /// function func_Send_Mail()