/**
 *
 * Script Name: SUT | T&M | forecast Master Creation
 * Author:Praveena Madem
 * Company:INSPIRRIA CLOUDTECH
 * Date: 04 - Jan - 2020
 * Description:1. This script will check the how many T&M forecase records will delete and Recreate the T & M forecast records.
				2. Call shecudle script if data is more to delete and re create
			
 			   
 * Script Modification Log:
 *  -- Date --			-- Modified By --				--Requested By--				-- Description --
*04-03-2020				Praveena Madem						Shamnath                      ADDED resource billable flag as Filter on 04.03.2020 to avoid the blank dates while sorting like (non billable)
*13-07-2020				Praveena Madem						Prateek/Deepak				 Updated logic to fetch INR conversion rate from USD-INR COST RATE instead of USD-INR REVENUE RATE value from P & L Currency Exchange Rates record
*28-07-2020				Praveena Madem						Vimal/Deepak				hours >8 hrs revenue recognition requirement in T&M project
*/
function _delete_create_forecast_master_data(request, response) {
    try {
        var context = nlapiGetContext();
        nlapiLogExecution('DEBUG', 'START TIME to delete', new Date());

        var projectid = request.getParameter('custscript_ra_project');
        var i_resource_id = request.getParameter('custscript_ra_resource');
        var s_start_date = request.getParameter('custscript_ra_startdate');
        var s_reallocation_end_date = request.getParameter('custscript_ra_enddate');
        var i_customer_id = request.getParameter('custscript_ra_customer');
      var i_ra_internalid=request.getParameter('custscript_ra_internalid');//added the internal id
        scheduled(projectid, i_resource_id, s_reallocation_end_date, i_customer_id,i_ra_internalid);
        nlapiLogExecution('DEBUG', 'USAGE', nlapiGetContext().getRemainingUsage());

        nlapiLogExecution('DEBUG', 'END TIME to Delete', new Date());
    } catch (e) {
        nlapiLogExecution('DEBUG', 'error in _delete_forecast_master_data', e);
    }

}




function scheduled(projectid, i_resource_id, s_reallocation_end_date, i_customer_id,i_ra_internalid) {

    nlapiLogExecution('AUDIT', 'Report Run', '');

    try {
        {
            var context = nlapiGetContext();
            var projectid = projectid;
            var i_resource_id = i_resource_id;
          var i_ra_internalid=i_ra_internalid;
            nlapiLogExecution('DEBUG', 'projectid : i_resource_id : i_ra_internalid ', projectid + " : " + i_resource_id+" : "+i_ra_internalid);
            //var projectid = 8385;
            if (!projectid || !i_resource_id)
                return;
            //  nlapiLogExecution('DEBUG', 'ProjectID', projectid);
            var forecast_fil = new Array();
            // forecast_fil[0] = new nlobjSearchFilter('custrecord_forecast_project_name', null, 'anyof', projectid);//comment the code 
			forecast_fil[0] = new nlobjSearchFilter('custrecord_forecast_ra_id', null, 'equalto',parseInt(i_ra_internalid));//Added the internal id on 02-11-2020
            forecast_fil[1] = new nlobjSearchFilter('custrecord_forecast_employee', null, 'anyof', i_resource_id); //ADDED EMPLOYEE as Filter on 03.02.2020
          
		   var record_del = searchRecord('customrecord_forecast_master_data', null, forecast_fil, [new nlobjSearchColumn('internalid'),new nlobjSearchColumn('custrecord_forecast_ra_id')]);
            if (record_del) {
               nlapiLogExecution('DEBUG', ' before Records are deleted Length', record_del.length);
                for (var del_ind = 0;record_del!=null && del_ind < record_del.length; del_ind++) {
                //  nlapiLogExecution('DEBUG', ' before rec intern', record_del[del_ind].getValue('custrecord_forecast_ra_id'));
                
                    nlapiDeleteRecord('customrecord_forecast_master_data', record_del[del_ind].getValue('internalid'));
                }

                // nlapiLogExecution('DEBUG', 'Records are deleted');
            }
            nlapiLogExecution('DEBUG', 'Records are deleted');


            var d_today = new Date();
            var error = false;
            var dataRow = [];
            //Conversion Rates table
			
			var o_PL_conversion_table=PL_curencyexchange_rate("T");
 

            var ra_filters = new Array();

            ra_filters[0] = new nlobjSearchFilter('resource', null, 'anyof', i_resource_id);
            ra_filters[1] = new nlobjSearchFilter('project', null, 'anyof', projectid);
			ra_filters[2] = new nlobjSearchFilter('custeventrbillable', null, 'is', 'T'); //ADDED resource billable flag as Filter on 04.03.2020 to avoid the blank dates while sorting like (non billable)
            ra_filters[3] = new nlobjSearchFilter('internalid',null, 'anyof', i_ra_internalid);//added the internal id on 02-11-2020
		   
            var ra_columns = new Array();
            ra_columns[0] = (new nlobjSearchColumn('custeventbenddate')).setSort(true);
			ra_columns[1] = (new nlobjSearchColumn('custeventbstartdate'));
			var s_start_date='';
            var srch_ra = nlapiSearchRecord('resourceallocation', null, ra_filters, ra_columns);
            if (srch_ra) {
               nlapiLogExecution('DEBUG', 'srch_ra', JSON.stringify(srch_ra));
                var d_date = nlapiStringToDate(s_reallocation_end_date);
                d_date = nlapiStringToDate(s_reallocation_end_date);
                nlapiLogExecution('DEBUG', 'd_date', d_date);
             
                var d_resulted_date = nlapiStringToDate(srch_ra[0].getValue('custeventbenddate'));
                nlapiLogExecution('DEBUG', 'd_resulted_date', d_resulted_date);
                if (d_date < d_resulted_date) {
                    s_reallocation_end_date = d_resulted_date;
                }
			 s_start_date=nlapiStringToDate(srch_ra[0].getValue('custeventbstartdate'));
			 nlapiLogExecution('DEBUG', 's_start_date', s_start_date);
           
            var d_reallocation_end_date = nlapiAddMonths(new Date(s_reallocation_end_date), 0);
            var d_reallocation_start_date = new Date(d_today.getFullYear(), d_today.getMonth() + 1, 0);
            d_reallocation_start_date.setDate(1);
            var d_reallocation_end_date = new Date(s_reallocation_end_date);
            nlapiLogExecution('DEBUG', 'd_reallocation_end_date', d_reallocation_end_date);
            nlapiLogExecution('DEBUG', 'd_reallocation_start_date', d_reallocation_start_date);

            //calcaulte how many months are required to create Forecast Record.
            var j = monthDiff(new Date(d_today.getFullYear(), d_today.getMonth()), new Date(d_reallocation_end_date.getFullYear(), d_reallocation_end_date.getMonth()));

            var a_resulted_allocation_details = getResourceAllocations(d_reallocation_start_date, d_reallocation_end_date, projectid, i_resource_id,i_ra_internalid,o_PL_conversion_table);
            
			d_reallocation_start_date=nlapiDateToString(d_reallocation_start_date);
		    d_reallocation_start_date=nlapiStringToDate(d_reallocation_start_date,'date');	
			var d_startDate=nlapiDateToString(s_start_date,'date');
			d_startDate=nlapiStringToDate(d_startDate,'date');
			 nlapiLogExecution('DEBUG', 'd_reallocation_start_date', d_reallocation_start_date);
			 nlapiLogExecution('DEBUG', 'd_startDate', d_startDate);
			if(d_reallocation_start_date<d_startDate)
			{
				d_reallocation_start_date=d_startDate;
			}
			
			var holidayDetailsInMonth = get_holidays(d_reallocation_start_date, d_reallocation_end_date, i_resource_id, projectid, i_customer_id);
			holidayDetailsInMonth = holidayDetailsInMonth ? holidayDetailsInMonth : {};//Added By praveena on 02-03-2020 to avaoid blank holidays data
           




		   nlapiLogExecution('DEBUG', 'Diff months : j', j);
            nlapiLogExecution('DEBUG', 'holidayDetailsInMonth : ', JSON.stringify(holidayDetailsInMonth));
            for (var i = 0; i <= Number(j); i++) {

                var d_day = nlapiAddMonths(d_today, i);

                var d_month_start = nlapiAddDays(d_day, -1 * (d_day.getDate() - 1));
              
              if(d_month_start.getDate()==2)/// @AUG 12 2021 : Logic is added sometimes Month end date is calcuating as 11/02/2021 instead of 11/01/2021 ADDED BY PRAVEENA
                    {
                      d_month_start=nlapiAddDays(d_month_start,-1);
                    }
              
                var s_month_start = d_month_start.getMonth() + 1 + '/' +
                    d_month_start.getDate() + '/' + d_month_start.getFullYear();
                var d_month_end = nlapiAddDays(nlapiAddMonths(d_month_start, 1), -1);
              
              if(d_month_end.getDate()==1)/// @April 03 20202 : Logic is added sometimes Month end date is calcauting as 01/11/2020 instead of 31/10/2020ADDED BY PRAVEENA
                {
                    d_month_end=nlapiAddDays(d_month_end,-1);
                }

              
                var s_month_end = d_month_end.getMonth() + 1 + '/' +
                    d_month_end.getDate() + '/' + d_month_end.getFullYear();
               try {
                    var a_data_ = getRevenueData(s_month_start, s_month_end,
						  d_month_start, d_month_end, projectid,i_resource_id, a_resulted_allocation_details, holidayDetailsInMonth);
                       
					   // nlapiLogExecution('DEBUG','USAGE to Create a_data_',JSON.stringify(a_data_));
                    for (var indx = 0; indx < a_data_.length; indx++) {
                        var s_local = a_data_[indx];
                        //nlapiLogExecution('DEBUG','USAGE to Create',nlapiGetContext().getRemainingUsage());
                        //nlapiLogExecution('DEBUG','BEFORE getRevenueDataUSAGE',nlapiGetContext().getRemainingUsage());
                        var createRec = nlapiCreateRecord('customrecord_forecast_master_data');
                        createRec.setFieldValue('custrecord_forecast_month_startdate', s_month_start);
                        createRec.setFieldValue('custrecord_forecast_month_enddate', s_month_end);
                        createRec.setFieldValue('custrecord_forecast_employee', s_local.custpage_employee);
                        createRec.setFieldValue('custrecord_forecast_vertical', s_local.custpage_vertical);
                        createRec.setFieldValue('custrecord_forecast_department', s_local.custpage_employee_department);
                        createRec.setFieldValue('custrecord_forecast_parent_department', s_local.custpage_employee_parent_department);
                        createRec.setFieldValue('custrecord_forecast_project_id', s_local.custpage_project_id);
                        createRec.setFieldValue('custrecord_forecast_project_name', s_local.custpage_project);
                        createRec.setFieldValue('custrecord_forecast_customer', s_local.custpage_customer);
                        createRec.setFieldValue('custrecord_forecast_amount', s_local.custpage_total_amount);
                        createRec.setFieldText('custrecord_forecast_project_currency', s_local.custpage_currency);

                        createRec.setFieldValue('custrecord41', s_local.custpage_holiday_hours);
                        createRec.setFieldValue('custrecord_forecast_percent_allocated', s_local.custpage_percent);
                        createRec.setFieldValue('custrecord38', s_local.custpage_allocated_days);
                        createRec.setFieldValue('custrecord40', s_local.custpage_rate);
                        createRec.setFieldValue('custrecord39', s_local.custpage_not_submitted_hours);
                        createRec.setFieldValue('custrecord_forecast_emp_location', s_local.emp_location_);
                        createRec.setFieldValue('custrecord_forecast_emp_level', s_local.emp_level_);
                        createRec.setFieldValue('custrecord_forecast_emp_role', s_local.emp_role_);
						createRec.setFieldValue('custrecord_forecast_ra_id',parseInt(s_local.i_ra_id));

                        var id = nlapiSubmitRecord(createRec);
                        //nlapiLogExecution('DEBUG','USAGE to submit',nlapiGetContext().getRemainingUsage());
                        nlapiLogExecution('DEBUG', 'Recordid:  ', +id);

                    }
                    //nlapiLogExecution('DEBUG', 'Record Created Count: ', indx);

                } catch (e) {
                    nlapiLogExecution('ERROR', 'Error: ', e.message);
                    error = true;
                }
            }
		}

        }
    } catch (err) {
        nlapiLogExecution('ERROR', 'main', err);
        throw err;
    }
}

// Get Active Resource Allocations
function getResourceAllocations(d_start_date, d_end_date, projectid, i_resource_id,i_ra_internalid,PL_curencyexchange_rate) {

    // Store resource allocations for project and employee
    var a_resource_allocations = new Array();

    // Get Resource allocations for this month
    var filters = new Array();

    filters[0] = new nlobjSearchFilter('custeventbstartdate', null,
        'notafter', d_end_date);
    filters[1] = new nlobjSearchFilter('custeventbenddate', null, 'notbefore',
        d_start_date);
    filters[2] = new nlobjSearchFilter('custeventrbillable', null, 'is', 'T');
    filters[3] = new nlobjSearchFilter('jobbillingtype', 'job', 'anyof', 'TM');
    filters[4] = new nlobjSearchFilter('internalid', 'job', 'anyof', projectid);
    filters[5] = new nlobjSearchFilter('resource', null, 'anyof', i_resource_id); //ADDED Resource as Filter on 03.02.2020
	filters[6] = new nlobjSearchFilter('internalid',null, 'anyof', i_ra_internalid);//added resource allocation id
    var columns = new Array();
    columns[0] = new nlobjSearchColumn('percentoftime', null, 'avg');
    columns[1] = new nlobjSearchColumn('custeventbstartdate', null, 'group');
    columns[2] = new nlobjSearchColumn('custeventbenddate', null, 'group');
    columns[3] = new nlobjSearchColumn('resource', null, 'group');
    columns[4] = new nlobjSearchColumn('project', null, 'group');
    columns[5] = new nlobjSearchColumn('custevent3', null, 'avg');
    columns[6] = new nlobjSearchColumn('custevent_monthly_rate', null, 'avg');
    columns[7] = new nlobjSearchColumn('custeventrbillable', null, 'group');
    columns[8] = new nlobjSearchColumn('department', 'employee',
        'group');
    columns[9] = new nlobjSearchColumn('subsidiary', 'employee', 'group');

    columns[10] = new nlobjSearchColumn('customer', 'job', 'group');
    columns[11] = new nlobjSearchColumn('custentity_project_currency', 'job',
        'group');
    columns[12] = new nlobjSearchColumn('custentity_vertical', 'job', 'group');
    columns[13] = new nlobjSearchColumn('custentity_t_and_m_monthly', 'job',
        'group');
    columns[14] = new nlobjSearchColumn('custevent_monthly_rate', null, 'group');

    columns[15] = new nlobjSearchColumn('formulatext', null, 'group');
    columns[15].setFormula('CASE WHEN INSTR({employee.department} , \' : \', 1) > 0 THEN SUBSTR({employee.department}, 1 , INSTR({employee.department} , \' : \', 1)) ELSE {employee.department} END');
    columns[16] = new nlobjSearchColumn('custentity_endcustomer', 'job', 'group');
    columns[17] = new nlobjSearchColumn('internalid', null, 'count');
    columns[18] = new nlobjSearchColumn('location', 'employee', 'group');
    columns[19] = new nlobjSearchColumn('employeestatus', 'employee', 'group');
    columns[20] = new nlobjSearchColumn('title', 'employee', 'group');
    columns[21] = new nlobjSearchColumn('entityid', 'job', 'group');
	columns[22] = new nlobjSearchColumn('custevent4',null, 'group'); //CR : 8 hrs revenue recognition requirement in T&M project
	columns[23] = new nlobjSearchColumn('custentity_onsite_hours_per_day', 'job', 'group');//CR : 8 hrs revenue recognition requirement in T&M project
	columns[24] = new nlobjSearchColumn('custentity_hoursperday', 'job', 'group');//CR : 8 hrs revenue recognition requirement in T&M project
	columns[25] = new nlobjSearchColumn('internalid', null, 'group');
    try {
        var search_results = searchRecord('resourceallocation', null, filters,
            columns);
    } catch (e) {
        nlapiLogExecution('ERROR', 'Error', e.message);
    }

    if (search_results != null && search_results.length > 0) {
        for (var i_search_indx = 0; i_search_indx < search_results.length; i_search_indx++) {
            var i_project_id = search_results[i_search_indx]
                .getValue(columns[4]);
            if (parseInt(i_project_id) == parseInt(120744)) {
                nlapiLogExecution('DEBUG', 'Project Index', i_search_indx);
            }
            var s_project_name = search_results[i_search_indx]
                .getValue(columns[4]);
            var s_project_id = search_results[i_search_indx]
                .getValue(columns[21]);
            var is_resource_billable = search_results[i_search_indx]
                .getValue(columns[7]);
            var stRate = search_results[i_search_indx].getValue(columns[5]);
            stRate = stRate ? parseFloat(stRate) : 0;
            var i_employee = search_results[i_search_indx].getValue(columns[3]);
            var s_employee_name = search_results[i_search_indx]
                .getValue(columns[3]);
            var i_percent_of_time = search_results[i_search_indx]
                .getValue(columns[0]);
            var i_department_id = search_results[i_search_indx]
                .getValue(columns[8]);
            var s_department = search_results[i_search_indx]
                .getValue(columns[8]);
            var i_subsidiary = search_results[i_search_indx]
                .getValue(columns[9]);
            //	var i_working_days = search_results[i_search_indx].getValue(columns[8]);
            var allocationStartDate = search_results[i_search_indx].getValue(columns[1]);
            var allocationEndDate = search_results[i_search_indx].getValue(columns[2]);

            //var i_working_days = parseInt(calcBusinessDays(d_start_date, d_end_date));;

            var i_customer_id = search_results[i_search_indx]
                .getValue(columns[10]);
            var s_customer = search_results[i_search_indx].getText(columns[10]);
            var i_count = search_results[i_search_indx].getValue(columns[17]);
            var i_currency = search_results[i_search_indx].getText(columns[11]);
            var i_vertical_id = search_results[i_search_indx]
                .getValue(columns[12]);
            var s_vertical = search_results[i_search_indx].getValue(columns[12]);
            var is_T_and_M_monthly = search_results[i_search_indx]
                .getValue(columns[13]);
            var f_monthly_rate = search_results[i_search_indx]
                .getValue(columns[14]);
            f_monthly_rate = f_monthly_rate ? parseFloat(f_monthly_rate) : 0;
            var s_parent_department = search_results[i_search_indx]
                .getValue(columns[15]);
            var s_end_customer = search_results[i_search_indx]
                .getText(columns[16]);
            var s_emp_location = search_results[i_search_indx]
                .getText(columns[18]);
            var s_emp_level = search_results[i_search_indx]
                .getValue(columns[19]);
            var s_emp_role = search_results[i_search_indx]
                .getValue(columns[20]);
            var s_pro_id = search_results[i_search_indx]
                .getText(columns[4]);
            // days calculation
            if (_logValidation(s_emp_location))
                s_emp_location = s_emp_location.split(':')[0];

			var i_perday_hours="";
			var i_offsite_onsite = search_results[i_search_indx]
                .getValue(columns[22]);//CR : 8 hrs revenue recognition requirement in T&M project
				
			nlapiLogExecution('DEBUG','i_offsite_onsite',i_offsite_onsite);
			
			if(i_offsite_onsite == "1")//Onsite
			{
				i_perday_hours = search_results[i_search_indx].getValue(columns[23]);//CR : 8 hrs revenue recognition requirement in T&M project
			
			}
			
			if(i_offsite_onsite == "2")//Offsite
			{
				i_perday_hours = search_results[i_search_indx].getValue(columns[24]);//CR : 8 hrs revenue recognition requirement in T&M project
			}
			
			nlapiLogExecution('DEBUG','i_perday_hours',i_perday_hours);
			
			i_perday_hours=i_perday_hours > 0 ?i_perday_hours : 8; //Consider the default is 8 hours in case of the data
			nlapiLogExecution('DEBUG','search i_perday_hours',i_perday_hours);
  
			
            a_resource_allocations[i_search_indx] = {
                'project_id': s_project_id,
                'project_name': s_project_name,
                is_billable: is_resource_billable,
                st_rate: stRate,
                'employee': i_employee,
                'employee_name': s_employee_name,
                'percentoftime': i_percent_of_time,
                'department_id': i_department_id,
                'department': s_department,
                //'holidays': holidayCountInMonth,
                //'holidayDetailsInMonth':holidayDetailsInMonth,
                'allocationEndDate': allocationEndDate,
                'allocationStartDate': allocationStartDate,
                // 'workingdays': workingDays,
                'customer_id': i_customer_id,
                'customer': i_customer_id,
                'count': i_count,
                'currency': i_currency,
                'vertical_id': i_vertical_id,
                'vertical': s_vertical,
                'monthly_billing': is_T_and_M_monthly,
                'monthly_rate': f_monthly_rate,
                'parent_department': s_parent_department,
                'emp_location': s_emp_location,
                'emp_level': s_emp_level,
                'emp_role': s_emp_role,
                'end_customer': s_end_customer == '- None -' ? '' : s_end_customer,
				'i_perday_hours':i_perday_hours,
				'i_ra_id':search_results[i_search_indx].getValue(columns[25]),
				'pl_coversion_rate':_Currency_exchangeRate(i_currency,PL_curencyexchange_rate)
            };
        }
    } else {
        a_resource_allocations = null;
    }

    return a_resource_allocations;
}

// Get Holidays
function getHolidays(d_start_date, d_end_date) {

    var filters = new Array();
    filters[0] = new nlobjSearchFilter('custrecord_date', null, 'within',
        d_start_date, d_end_date);

    var columns = new Array();
    columns[0] = new nlobjSearchColumn('custrecord_date', null, 'group');
    columns[1] = new nlobjSearchColumn('custrecordsubsidiary', null, 'group');

    var search_holidays = nlapiSearchRecord('customrecord_holiday', null,
        filters, columns);

    var a_holidays = new Object();

    for (var i = 0; search_holidays != null && i < search_holidays.length; i++) {
        var i_subsidiary = search_holidays[i].getValue('custrecordsubsidiary', null, 'group');
        var o_holiday = {
            'date': nlapiStringToDate(search_holidays[i]
                .getValue('custrecord_date', null, 'group'))
        };
        if (a_holidays[i_subsidiary] == undefined) {
            a_holidays[i_subsidiary] = [o_holiday];
        } else {
            a_holidays[i_subsidiary].push(o_holiday);
        }
    }

    return a_holidays;
}

function getRevenueData(s_from_date, s_to_date, d_from_date,
   d_to_date, projectid, i_resource_id, a_resulted_allocation_details, holidayDetailsInMonth) {

    var chk_show_ot_hours = 'T';
    // Resource Allocation Data
    var a_resource_allocation = a_resulted_allocation_details;
    var s_currentmonthyear = nlapiStringToDate(s_from_date, 'date');



    s_currentmonthyear = (s_currentmonthyear.getMonth() + 1) + "/" + s_currentmonthyear.getFullYear();
    var i_current_employee = null;

    var a_res_alloc_for_each_employee = new Object();

    var a_exception_employees = new Array();

    var a_billable_employees = new Array();

    var a_currency_rate = new Object();
    var a_data = new Array();

    for (var i_res_alloc_indx = 0; a_resource_allocation != null && i_res_alloc_indx < a_resource_allocation.length; i_res_alloc_indx++) {
        // nlapiLogExecution('DEBUG','a_resource_allocation '+i_res_alloc_indx,JSON.stringify(a_resource_allocation));

        var a_row_data = new Object();
        var o_resource_allocation = a_resource_allocation[i_res_alloc_indx];
        var i_employee_id = o_resource_allocation.employee;
        var i_project_id = o_resource_allocation.project_id.toString();
        var d_allocationStartDate = nlapiStringToDate(o_resource_allocation.allocationStartDate);
        var d_allocationEndDate = nlapiStringToDate(o_resource_allocation.allocationEndDate);
        var d_provisionStartDate = d_from_date;
        var d_provisionEndDate = d_to_date;
        var provisionStartDate = nlapiDateToString(d_from_date);
        var provisionEndDate = nlapiDateToString(d_to_date);
        var d_endofthemonthdate = nlapiStringToDate(provisionEndDate);
		
        var lastDayOfMonth = new Date(d_allocationEndDate.getFullYear(), d_allocationEndDate.getMonth() + 1, 0);
        lastDayOfMonth = nlapiDateToString(lastDayOfMonth);
        lastDayOfMonth = nlapiStringToDate(lastDayOfMonth);
       // nlapiLogExecution('DEBUG', "lastDayOfMonth", lastDayOfMonth);
       // nlapiLogExecution('DEBUG', "d_endofthemonthdate", d_endofthemonthdate);
        //if (d_endofthemonthdate <= lastDayOfMonth) //added logic to avoid multiple iteration with resource allocations 
        {
            nlapiLogExecution('DEBUG', " i_res_alloc_indx d_endofthemonthdate : lastDayOfMonth", i_res_alloc_indx+ ": " +d_endofthemonthdate + ": " + lastDayOfMonth);

            var provisionMonthStartDate = nlapiStringToDate((d_provisionStartDate
                    .getMonth() + 1) +
                '/1/' + d_provisionStartDate.getFullYear());
            var nextProvisionMonthStartDate = nlapiAddMonths(
                provisionMonthStartDate, 1);
            var provisionMonthEndDate = nlapiAddDays(
                nextProvisionMonthStartDate, -1);
            var startDate = d_allocationStartDate > d_provisionStartDate ? o_resource_allocation.allocationStartDate :
                provisionStartDate;
            var endDate = d_allocationEndDate < d_provisionEndDate ? o_resource_allocation.allocationEndDate :
                provisionEndDate;

            // days calculation
            var workingDays = 0;
            workingDays = parseFloat(getWorkingDays(startDate, endDate));
            var holidays = holidayDetailsInMonth[s_currentmonthyear] ? holidayDetailsInMonth[s_currentmonthyear] : 0;
            var s_conversion_rate_date = s_from_date;
            var d_today = new Date();
            if (d_today < d_from_date) {
                s_conversion_rate_date = null;
            }
			
             if (o_resource_allocation.currency == undefined) {
                a_currency_rate[o_resource_allocation.currency] = nlapiExchangeRate(
                    o_resource_allocation.currency, 'USD',
                    s_conversion_rate_date);
                nlapiLogExecution('DEBUG', 'USAGE to isider coversion');
            }

            //Conversion factor
			var f_currency_conversion=o_resource_allocation.pl_coversion_rate;
			f_currency_conversion=f_currency_conversion? f_currency_conversion : 1;

            //	var f_currency_conversion = parseFloat(a_currency_rate[o_resource_allocation.currency]);
            a_row_data['custpage_vertical'] = o_resource_allocation.vertical;
            a_row_data['custpage_vertical_id'] = o_resource_allocation.vertical_id;
            a_row_data['custpage_employee'] = o_resource_allocation.employee_name;
            a_row_data['custpage_employee_id'] = o_resource_allocation.employee;
            a_row_data['custpage_employee_department'] = o_resource_allocation.department;
            a_row_data['custpage_employee_department_id'] = o_resource_allocation.department_id;
            a_row_data['custpage_employee_parent_department'] = o_resource_allocation.parent_department;
            a_row_data['custpage_project'] = o_resource_allocation.project_name;
            a_row_data['custpage_project_id'] = o_resource_allocation.project_id;
            a_row_data['custpage_customer'] = o_resource_allocation.customer;
            a_row_data['custpage_customer_id'] = o_resource_allocation.customer_id;
            //a_row_data['custpage_holiday_hours'] = o_resource_allocation.holidays;//commented by praveena on 04-02-2020
            a_row_data['custpage_holiday_hours'] = holidays;
			
			
          //  a_row_data['custpage_rate'] = (o_resource_allocation.monthly_billing == 'T') ? (o_resource_allocation.monthly_rate / (8.0 * workingDays)) :
               
  a_row_data['custpage_rate'] = (o_resource_allocation.monthly_billing == 'T') ? (o_resource_allocation.monthly_rate / (parseInt(o_resource_allocation.i_perday_hours) * workingDays)) :
            

			   o_resource_allocation.st_rate; {
                a_row_data['custpage_billed_hours'] = '0';
                a_row_data['custpage_approved_hours'] = '0';
                a_row_data['custpage_submitted_hours'] = '0';
                a_row_data['custpage_billed_hours_ot'] = '0';
                a_row_data['custpage_approved_hours_ot'] = '0';
                a_row_data['custpage_submitted_hours_ot'] = '0';
                a_row_data['custpage_not_submitted_hours'] = (((parseFloat(workingDays)) - parseFloat(holidays)) *
						//8* commented as per CR to provide the hours dynamically
                        parseInt(o_resource_allocation.i_perday_hours) *//Added no of hours dynamically ad a part of Onsite/Offsite>8 hrs revenue recognition requirement in T&M project
                        parseFloat(o_resource_allocation.percentoftime) / 100.0).toString();
						
						
			nlapiLogExecution('AUDIT', 'workingDays',parseFloat(workingDays));
			nlapiLogExecution('AUDIT', 'holidays',parseFloat(holidays));
			nlapiLogExecution('AUDIT', 'o_resource_allocation.i_perday_hours hh',o_resource_allocation.i_perday_hours);
			nlapiLogExecution('AUDIT', 'o_resource_allocation.i_perday_hours',parseInt(o_resource_allocation.i_perday_hours));
			nlapiLogExecution('AUDIT', 'percentoftime',parseFloat(o_resource_allocation.percentoftime));
			
            nlapiLogExecution('AUDIT', 'a_row_datacustpage_not_submitted_hours',(((parseFloat(workingDays)) - parseFloat(holidays)) *parseInt(o_resource_allocation.i_perday_hours) * parseFloat(o_resource_allocation.percentoftime) / 100.0).toString());
          

		   a_row_data['custpage_leave_hours'] = '0';
                // a_row_data['custpage_holiday_hours'] = '0';

                a_row_data['custpage_billed_amount'] = parseFloat(0.0)
                    .toFixed(2); // *
                // parseFloat(a_row_data['custpage_rate'])).toFixed(2);
                a_row_data['custpage_billed_amount_ot'] = parseFloat(0.0)
                    .toFixed(2);
                a_row_data['custpage_approved_amount'] = parseFloat(0.0)
                    .toFixed(2); // *
                // parseFloat(a_row_data['custpage_rate'])).toFixed(2);
                a_row_data['custpage_approved_amount_ot'] = parseFloat(0.0)
                    .toFixed(2);
                a_row_data['custpage_submitted_amount'] = parseFloat(0.0)
                    .toFixed(2); // *
                // parseFloat(a_row_data['custpage_rate'])).toFixed(2);
                a_row_data['custpage_submitted_amount_ot'] = parseFloat(0.0)
                    .toFixed(2);
            }
            a_row_data['custpage_count'] = parseFloat(o_resource_allocation.count);
            a_row_data['custpage_allocated_days'] = parseFloat(workingDays) - parseFloat(holidays);
            a_row_data['custpage_percent'] = o_resource_allocation.percentoftime
                .toString();
            a_row_data['custpage_not_submitted_amount'] = parseFloat(
                parseFloat(a_row_data['custpage_not_submitted_hours']) *
                parseFloat(a_row_data['custpage_rate'])
                //    * (parseFloat(a_row_data['custpage_percent']) / 100.0)
                *
                f_currency_conversion).toFixed(2);
            a_row_data['custpage_total_amount'] = parseFloat(a_row_data['custpage_billed_amount']) +
                parseFloat(a_row_data['custpage_approved_amount']) +
                parseFloat(a_row_data['custpage_submitted_amount']) +
                parseFloat(a_row_data['custpage_not_submitted_amount']);
            if (chk_show_ot_hours == 'T') {
                a_row_data['custpage_total_amount'] += parseFloat(a_row_data['custpage_billed_amount_ot']) +
                    parseFloat(a_row_data['custpage_approved_amount_ot']) +
                    parseFloat(a_row_data['custpage_submitted_amount_ot']);
            }
            a_row_data['custpage_total_amount'] = a_row_data['custpage_total_amount']
                .toFixed(2);
            a_row_data['custpage_currency'] = o_resource_allocation.currency;
            a_row_data['custpage_t_and_m_monthly'] = o_resource_allocation.monthly_billing == 'T' ? 'T & M Monthly' :
                'T & M';
            a_row_data['custpage_end_customer'] = o_resource_allocation.end_customer;
            a_row_data['emp_location_'] = o_resource_allocation.emp_location;
            a_row_data['emp_level_'] = o_resource_allocation.emp_level;
            a_row_data['emp_role_'] = o_resource_allocation.emp_role;
			a_row_data['i_ra_id'] = o_resource_allocation.i_ra_id;
          nlapiLogExecution('AUDIT', 'a_row_datacustpage_not_submitted_hours',a_row_data['custpage_not_submitted_hours']);
            if(Number(a_row_data['custpage_not_submitted_hours'])>0)
			{//ADded condtion to avoid duplicate iterations
            a_data.push(a_row_data); // [i_list_indx] = a_row_data;
            }
        }
    }

    //nlapiLogExecution('AUDIT', 'Number of Records: ', a_data.length);
    return a_data;
}

function get_holidays(start_date, end_date, employee, project, customer) {

    var project_holiday = nlapiLookupField('job', project,
        'custentityproject_holiday');
    var emp_subsidiary = nlapiLookupField('employee', employee, 'subsidiary');

    if (project_holiday == 1) {
        return get_company_holidays(start_date, end_date, emp_subsidiary);
    } else {
        return get_customer_holidays(start_date, end_date, emp_subsidiary, customer);
    }
}

function get_company_holidays(start_date, end_date, subsidiary) {
    var holiday_list = {};
    var check_monthly_holidays = [];
    var search_company_holiday = nlapiSearchRecord('customrecord_holiday',
        'customsearch_company_holiday_search', [
            new nlobjSearchFilter('custrecord_date', null, 'within',
                start_date, end_date),
            new nlobjSearchFilter('custrecordsubsidiary', null,
                'anyof', subsidiary)
        ], [new nlobjSearchColumn(
            'custrecord_date')]);

    if (search_company_holiday) {
        for (var i = 0; i < search_company_holiday.length; i++) {
            var Holiday_Date = search_company_holiday[i].getValue('custrecord_date');
            Holiday_Date = nlapiStringToDate(Holiday_Date, 'date');
            holiday_list[Number(Holiday_Date.getMonth() + 1) + "/" + Holiday_Date.getFullYear()] = parseInt(holiday_list[Number(Holiday_Date.getMonth() + 1) + "/" + Holiday_Date.getFullYear()]) ? parseInt(holiday_list[Number(Holiday_Date.getMonth() + 1) + "/" + Holiday_Date.getFullYear()]) + 1 : 1;
        }
    }

    return holiday_list;
}

function get_customer_holidays(start_date, end_date, subsidiary, customer) {
    var holiday_list = {};
    var check_monthly_holidays = [];
    var start_date = nlapiDateToString(start_date, 'date');
    var end_date = nlapiDateToString(end_date, 'date');

    /*nlapiLogExecution('debug', 'start_date', start_date);
    nlapiLogExecution('debug', 'end_date', end_date);
    nlapiLogExecution('debug', 'subsidiary', subsidiary);
    nlapiLogExecution('debug', 'customer', customer);
*/
    var search_customer_holiday = nlapiSearchRecord(
        'customrecordcustomerholiday', 'customsearch_customer_holiday', [
            new nlobjSearchFilter('custrecordholidaydate', null,'within', start_date, end_date),
            new nlobjSearchFilter('custrecordcustomersubsidiary', null,'anyof', subsidiary),
            new nlobjSearchFilter('custrecord13', null, 'anyof',customer)
			]
		   ,[new nlobjSearchColumn('custrecordholidaydate', null, 'group')]
		
        /*, [new nlobjSearchColumn('custrecordholidaydate')]
			*/
			);

    if (search_customer_holiday) {

        for (var i = 0; i < search_customer_holiday.length; i++) {
           // var Holiday_Date = search_customer_holiday[i].getValue('custrecordholidaydate');
		   var Holiday_Date = search_customer_holiday[i].getValue('custrecordholidaydate', null, 'group');
            Holiday_Date = nlapiStringToDate(Holiday_Date, 'date');
            holiday_list[Number(Holiday_Date.getMonth() + 1) + "/" + Holiday_Date.getFullYear()] = parseInt(holiday_list[Number(Holiday_Date.getMonth() + 1) + "/" + Holiday_Date.getFullYear()]) ? parseInt(holiday_list[Number(Holiday_Date.getMonth() + 1) + "/" + Holiday_Date.getFullYear()]) + 1 : 1;
        }
    }

    return holiday_list;
}

function getWorkingDays(startDate, endDate) {
    try {
        var d_startDate = nlapiStringToDate(startDate);
        var d_endDate = nlapiStringToDate(endDate);
        var numberOfWorkingDays = 0;

        for (var i = 0;; i++) {
            var currentDate = nlapiAddDays(d_startDate, i);

            if (currentDate > d_endDate) {
                break;
            }

            if (currentDate.getDay() != 0 && currentDate.getDay() != 6) {
                numberOfWorkingDays += 1;
            }
        }

        return numberOfWorkingDays;
    } catch (err) {
        nlapiLogExecution('error', 'getWorkingDays', err);
        throw err;
    }
}

function _logValidation(value) {
    if (value != '- None -' && value != null && value.toString() != null && value != '' && value!= '- None -' && value != undefined && value.toString() != undefined && value != 'undefined' && value.toString() != 'undefined' && value.toString() != 'NaN' && value != NaN) {
        return true;
    } else {
        return false;
    }
}

function monthDiff(dateFrom, dateTo) {
    return dateTo.getMonth() - dateFrom.getMonth() +
        (12 * (dateTo.getFullYear() - dateFrom.getFullYear()))
}