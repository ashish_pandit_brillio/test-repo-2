/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       16 March 2020     Prabhat Gupta
 *	2.0 	   15-Sep-2020		Cloned on the part for OTG and Codex
 */

/**
 * @param {Object} dataIn Parameter object
 * @returns {Object} Output object
 */
function getRESTlet(dataIn) {

    return {};
}

/**
 * @param {Object} dataIn Parameter object
 * @returns {Object} Output object
 */
function postRESTlet(dataIn) {

    try {

        //dataIn = {"lastUpdatedTimestamp":""}
        var response = new Response();
        //nlapiLogExecution('debug', 'dataIn', JSON.stringify(dataIn));
        var current_date = nlapiDateToString(new Date());
        var currentDate = sysDate();
        var currentTime = timestamp(); // returns the time stamp in HH:MM:SS
        var currentDateAndTime = currentDate + ' ' + currentTime;
        //Log for current date



        var receivedDate = dataIn.lastUpdatedTimestamp;
        nlapiLogExecution('DEBUG', 'receivedDate', 'receivedDate...' + receivedDate);
        //nlapiLogExecution('DEBUG', 'Current Date', 'currentDateAndTime...' + currentDateAndTime);

        //var receivedDate = '7/12/2019 1:00 am';
        //Check if the timestamp is empty
        if (!_logValidation(receivedDate)) { 
	
		// for empty timestanp in this we will give whole data as a response
            var filters = new Array();
			nlapiLogExecution('DEBUG', 'receivedDate', 'receivedDate...null');
            filters = [

                ["custentity_employee_inactive", "is", "T"],
                "AND",
                ["custentity_implementationteam", "is", "F"]

            ];
            //	nlapiLogExecution('debug', 'filters ', filters);
        } else {
            var filters = new Array(); // this filter will provide the result within the current date and given date     
            filters = [

                ["custentity_implementationteam", "is", "F"],
                "AND",
				["custentity_employee_inactive", "is", "T"],
				"AND",
                ["lastmodifieddate", "within", receivedDate, currentDateAndTime] // prabhat gupta NIS-1349 28/5/2020				
            ];
            	
        }
			nlapiLogExecution('debug', 'filters ', filters);
        // created search by grouping 
		var employeeData = [];
		var employeeList = [];
			var emp_id ='';
			var emp_type ='';
			var person_type ='';
			var emp_fusion_id ='';
			var emp_frst_name ='';
			var emp_middl_name ='';
			var emp_lst_name ='';
			var emp_full_name ='';
			var emp_id ='';
			var emp_id ='';
			var emp_bday = '';
			
			//Manager
			var mgr_fusion_id ='';
			var mgr_frst_name ='';
			var mgr_middl_name ='';
			var mgr_lst_name ='';
			var mgr_full_name ='';
			var mgr_email ='';
			
			
			var cols = [];
			cols.push(new nlobjSearchColumn('firstname'));
			cols.push(new nlobjSearchColumn('middlename'));
			cols.push(new nlobjSearchColumn('lastname'));
			cols.push(new nlobjSearchColumn('custentity_fusion_empid'));
			cols.push(new nlobjSearchColumn('department'));
			cols.push(new nlobjSearchColumn('title'));
			cols.push(new nlobjSearchColumn('custentity_employeetype'));
			cols.push(new nlobjSearchColumn('custentity_persontype'));
			cols.push(new nlobjSearchColumn('email')); //custentity_emp_attendance_tracking
			cols.push(new nlobjSearchColumn('custentity_employee_inactive'));
			cols.push(new nlobjSearchColumn('custentity_emp_attendance_tracking'));
			cols.push(new nlobjSearchColumn('gender'));
			cols.push(new nlobjSearchColumn('employeetype'));
			cols.push(new nlobjSearchColumn('custentity_employee_inactive'));
			cols.push(new nlobjSearchColumn('email','custentity_reportingmanager'));
			cols.push(new nlobjSearchColumn('custentity_fusion_empid','custentity_reportingmanager'));
			cols.push(new nlobjSearchColumn('firstname','custentity_reportingmanager'));
			cols.push(new nlobjSearchColumn('middlename','custentity_reportingmanager'));
			cols.push(new nlobjSearchColumn('lastname','custentity_reportingmanager'));
			cols.push(new nlobjSearchColumn('subsidiary'));
			cols.push(new nlobjSearchColumn('custentity_lwd'));
			cols.push(new nlobjSearchColumn('employeestatus'));
			cols.push(new nlobjSearchColumn('phone'));
			cols.push(new nlobjSearchColumn('hiredate'));
           cols.push(new nlobjSearchColumn('custentity_actual_hire_date'));
			//Absence details:
			//
			cols.push(new nlobjSearchColumn('custentity_personid'));
			cols.push(new nlobjSearchColumn('custentity_employer_id'));
			cols.push(new nlobjSearchColumn('custentity_assignment_num'));
			
			//Brillio Location
			cols.push(new nlobjSearchColumn('custentity_list_brillio_location_e'));
			
			//Date of Birth
			cols.push(new nlobjSearchColumn('birthdate'));
			
			//Mobile Number
			cols.push(new nlobjSearchColumn('phone'));
          
          	//Legal Entity
          	cols.push(new nlobjSearchColumn('custentity_legal_entity_fusion'));
			
			var employeeSearch = searchRecord('employee', null,filters,cols);
					
			var dataRow = [];
			var JSON = {};
			if (employeeSearch) {
					for(var i=0;i<employeeSearch.length;i++){
					    emp_id = employeeSearch[i].getId();
						emp_type = employeeSearch[i].getText('custentity_employeetype');
					    person_type = employeeSearch[i].getText('custentity_persontype');
						emp_fusion_id = employeeSearch[i].getValue('custentity_fusion_empid');
						emp_frst_name = employeeSearch[i].getValue('firstname');
						emp_middl_name = employeeSearch[i].getValue('middlename');
						emp_lst_name = employeeSearch[i].getValue('lastname');
						emp_bday = employeeSearch[i].getValue('birthdate');
						
						
						if(emp_frst_name)
							emp_full_name = emp_frst_name;
							
						if(emp_middl_name)
							emp_full_name = emp_full_name + ' ' + emp_middl_name;
							
						if(emp_lst_name)
							emp_full_name = emp_full_name + ' ' + emp_lst_name;
						
						//manager
						mgr_fusion_id = employeeSearch[i].getValue('custentity_fusion_empid','custentity_reportingmanager');
						mgr_frst_name = employeeSearch[i].getValue('firstname','custentity_reportingmanager');
						mgr_middl_name = employeeSearch[i].getValue('middlename','custentity_reportingmanager');
						mgr_lst_name = employeeSearch[i].getValue('lastname','custentity_reportingmanager');
						
						if(mgr_frst_name)
							mgr_full_name = mgr_frst_name;
							
						if(mgr_middl_name)
							mgr_full_name = mgr_full_name + ' ' + mgr_middl_name;
							
						if(mgr_lst_name)
							mgr_full_name = mgr_full_name + ' ' + mgr_lst_name;
						
						
						
						JSON = {
								ID: emp_id,
								EmployeeID:emp_fusion_id,
								Emp_Display_Name: emp_full_name,
								EmployeeType: emp_type,
								PersonType: person_type,
								Designation: employeeSearch[i].getValue('title'),
								FirstName: employeeSearch[i].getValue('firstname'),
								MiddleName: employeeSearch[i].getValue('middlename'),
								LastName: employeeSearch[i].getValue('lastname'),
								Department: employeeSearch[i].getText('department'),
								LegalEntity: employeeSearch[i].getText('subsidiary'),
								Employee_InActive: employeeSearch[i].getValue('custentity_employee_inactive'),
								WorkEmail: employeeSearch[i].getValue('email'),
								Emp_Level: employeeSearch[i].getText('employeestatus'),
								AttendanceTracking: employeeSearch[i].getText('custentity_emp_attendance_tracking'),
								Phone: employeeSearch[i].getValue('phone'),
								HireDate: employeeSearch[i].getValue('custentity_actual_hire_date'),  // actual hire date
                                LegalHireDate: employeeSearch[i].getValue('hiredate'),               //hiredate       
								TerminationDate: employeeSearch[i].getValue('custentity_lwd'),
								Gender: employeeSearch[i].getValue('gender'),
								Hourly_Salaried: employeeSearch[i].getText('employeetype'),
								ManagerEmail: employeeSearch[i].getValue('email','custentity_reportingmanager'),
								ManagerPersonNumber: employeeSearch[i].getValue('custentity_fusion_empid','custentity_reportingmanager'),
								//JSON:
								PersonID: employeeSearch[i].getValue('custentity_personid'),
								EmployerId: employeeSearch[i].getValue('custentity_employer_id'),
								AssignmentNumber: employeeSearch[i].getValue('custentity_assignment_num'),
								ManagerDisplayName: mgr_full_name,
								//Location
								BrillioLocation: employeeSearch[i].getValue('custentity_list_brillio_location_e'),
								BirthDate : emp_bday,
								Phone : employeeSearch[i].getValue('phone'),
								LegalEntityFusion : employeeSearch[i].getValue('custentity_legal_entity_fusion')
								
						};
						dataRow.push(JSON);
						
					}
					
				
			}

       response.timeStamp = currentDateAndTime;
        response.data = dataRow;
        //response.data = dataRow;
        response.status = true;
        //	nlapiLogExecution('debug', 'response',JSON.stringify(response));
    } catch (err) {
        nlapiLogExecution('ERROR', 'postRESTlet', err);
        response.timeStamp = '';
        response.data = err;
        response.status = false;
    }
    return response;

}
//validate blank entries
function _logValidation(value) {
    if (value != null && value.toString() != null && value != '' && value != undefined && value.toString() != undefined && value != 'undefined' && value.toString() != 'undefined' && value.toString() != 'NaN' && value != NaN) {
        return true;
    } else {
        return false;
    }
}
//Get current date
function sysDate() {
    var date = new Date();
    var tdate = date.getDate();
    var month = date.getMonth() + 1; // jan = 0
    var year = date.getFullYear();
    return currentDate = month + '/' + tdate + '/' + year;
}

//Returns timestamp
function timestamp() {
    var str = "";

    var currentTime = new Date();
    var hours = currentTime.getHours();
    var minutes = currentTime.getMinutes();
    var seconds = currentTime.getSeconds();
    var meridian = "";
    if (hours > 12) {
        meridian += "pm";
    } else {
        meridian += "am";
    }
    if (hours > 12) {

        hours = hours - 12;
    }
    if (minutes < 10) {
        minutes = "0" + minutes;
    }
    if (seconds < 10) {
        seconds = "0" + seconds;
    }
    //str += hours + ":" + minutes + ":" + seconds + " ";
    str += hours + ":" + minutes + " ";
    return str + meridian;
}

function Response() {
    this.status = "";
    this.timeStamp = "";
    this.data = "";

}

//Search function for results more than 1000
function searchRecord(recordType, savedSearch, arrFilters, arrColumns,
    filterExpression) {

    try {
        var search = null;

        // if a saved search is provided, load it and add the filters and
        // columns
        if (savedSearch) {
            search = nlapiLoadSearch(recordType, savedSearch);

            if (isArrayNotEmpty(arrFilters)) {
                search.addFilters(arrFilters);
            }

            if (isArrayNotEmpty(arrColumns)) {
                search.addColumns(arrColumns);
            }

            if (isArrayNotEmpty(filterExpression)) {
                search.setFilterExpression(filterExpression);
            }
        }
        // create a new search
        else {
            search = nlapiCreateSearch(recordType, arrFilters, arrColumns);
        }

        // run search
        var resultSet = search.runSearch();

        // iterate through the search and get all data 1000 at a time
        var searchResultCount = 0;
        var resultSlice = null;
        var searchResult = [];

        do {
            resultSlice = resultSet.getResults(searchResultCount,
                searchResultCount + 1000);

            if (resultSlice) {

                resultSlice.forEach(function(result) {

                    searchResult.push(result);
                    searchResultCount++;
                });
            }
        } while (isArrayNotEmpty(resultSlice) && resultSlice.length >= 1000);

        return searchResult;
    } catch (err) {
        nlapiLogExecution('ERROR', 'searchRecord', err);
        throw err;
    }
}

function isEmpty(value) {

    return value == null || value == "" || typeof(value) == undefined;
}

function isNotEmpty(value) {

    return !isEmpty(value);
}

function isArrayEmpty(argArray) {

    return !isArrayNotEmpty(argArray);
}

function isArrayNotEmpty(argArray) {

    return (isNotEmpty(argArray) && argArray.length > 0);
}
//function to format date object into NetSuite's mm/dd/yyyy format.
function formatNSDate(dateObj) {
    if (dateObj) {
        var nsFormatDate = dateObj.getFullYear() + '-' + addZeros(dateObj.getMonth() + 1, 2) + '-' + addZeros(dateObj.getDate(), 2);
        return nsFormatDate;
    }
    return null;
}

//function to add leading zeros on date parts.
function addZeros(num, len) {
    var str = num.toString();
    while (str.length < len) {
        str = '0' + str;
    }
    return str;
}


function visaDetail() {

    var customrecord_visa_detail = nlapiSearchRecord("customrecord_empvisadetails", null,
        [
            ["isinactive", "is", "F"],
            "AND",
            ["custrecord_validtill", "notbefore", "today"]
        ],
        [
            new nlobjSearchColumn("internalid"),
            new nlobjSearchColumn("custrecord_visa"),
            new nlobjSearchColumn("custrecord_visaid"),
            new nlobjSearchColumn("custrecord_validtill")
        ]
    );

    var data = [];
    if (customrecord_visa_detail) {


        for (var i = 0; i < customrecord_visa_detail.length; i++) {
            data.push({
                "id": customrecord_visa_detail[i].getId(),
                "visaType": customrecord_visa_detail[i].getText("custrecord_visa"),
                "validTill": customrecord_visa_detail[i].getValue("custrecord_validtill"),
                "employeeInternalId": customrecord_visa_detail[i].getValue("custrecord_visaid"),
                "employee": customrecord_visa_detail[i].getText("custrecord_visaid")

            });
        }

    }
    return data;
}


function practice() {

    var practiceSearch = nlapiSearchRecord("department", null,
        [
            ["isinactive", "is", "F"]

        ],
        [
            new nlobjSearchColumn("name"),
            new nlobjSearchColumn("custrecord_parent_practice"),
         //   new nlobjSearchColumn("custrecord_hrbusinesspartner")
		 // Commented by shravan for HRBP integration on 7-dec-2020

        ]
    );

    var data = [];
    if (practiceSearch) {


        for (var i = 0; i < practiceSearch.length; i++) {
            data.push({
                "id": practiceSearch[i].getId(),
                "practice": practiceSearch[i].getValue("name"),
                "parentPracticeId": practiceSearch[i].getValue("custrecord_parent_practice"),
                "parentPractice": practiceSearch[i].getText("custrecord_parent_practice"),
              //  "hrbpId": practiceSearch[i].getValue("custrecord_hrbusinesspartner"),
             //   "hrbp": practiceSearch[i].getText("custrecord_hrbusinesspartner")
			 // Commented by shravan for HRBP integration on 7-dec-2020
            });
        }

    }
    return data;
}