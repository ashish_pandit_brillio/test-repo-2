/**
 *@NApiVersion 2.x
 *@NScriptType ClientScript
 *@NmoduleScope public
 */
define(['N/currentRecord'], function (record) {
    function pageInit(context)
    {
        var currentRecord = context.currentRecord;
        var territory = currentRecord.getText({ fieldId: 'territory' });
        if (territory == 'Brillio') {
            var sfdc_id = currentRecord.getField({ fieldId: 'custentity_sfdc_account_id' });
            sfdc_id.isMandatory = false;
        }

    }
    function fieldChanged(context) {

        if (context.fieldId === 'territory') {
            var currentRecord = context.currentRecord;
            var territory = currentRecord.getText({ fieldId: 'territory' });
            if (territory == 'Brillio') {
                var sfdc_id = currentRecord.getField({ fieldId: 'custentity_sfdc_account_id' });
                sfdc_id.isMandatory = false;
            }
            else{
                var sfdc_id = currentRecord.getField({ fieldId: 'custentity_sfdc_account_id' });
                sfdc_id.isMandatory = true;
            }
            return true;
        }
    }

    function saveRecord(context) {
        var currentRecord = context.currentRecord;
        var territory = currentRecord.getText({ fieldId: 'territory' });
        if (territory == 'Brillio') {
            var sfdc_id = currentRecord.getField({ fieldId: 'custentity_sfdc_account_id' });
            sfdc_id.isMandatory = false;
        }
        return true;
    }

    return {
        pageInit:pageInit,
        saveRecord: saveRecord,
        fieldChanged: fieldChanged
    };
});