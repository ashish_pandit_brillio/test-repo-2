/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       09 Aug 2017     deepak.srinivas
 * 2.00		  18 Mar 2020	  praveena Madem	changed subrecords array values , sublist ids,searched record type as timebill
 * 3.00		  19 Aug 2020	  praveena Madem	Added customer name (CustomerName) on GET api
 */

/**
 * @param {Object} dataIn Parameter object
 * @returns {Object} Output object
 */
/*var days = ['sunday','monday','tuesday','wednesday','thursday','friday','saturday'];
var daysList = ['Su','Mo','Tu','We','Th','Fr','Sa'];
*/
//var days= ['timebill0','timebill1','timebill2','timebill3','timebill4','timebill5','timebill6'];//Updated array as timebill values instaed of week--added by praveena on 18-03-2020
var days = ['hours0', 'hours1', 'hours2', 'hours3', 'hours4', 'hours5', 'hours6']; //Updated array as timebill values instaed of week--added by praveena on 18-03-2020

var daysList = ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'];

function postRESTlet(dataIn) {

    try {
		
	
        var response = new Response();
        var checkTS = null;
        var flag = false;
		var Timesheet_zero_hours=false;
		var checkTSzerohours=null;
        var current_date = nlapiDateToString(new Date());
        //Log for current date
        nlapiLogExecution('DEBUG', 'Current Date', 'current_date...' + current_date)
        nlapiLogExecution('debug', 'dataIn', JSON.stringify(dataIn));

        var employeeId = getUserUsingEmailId(dataIn.EmailId);
        var requestType = dataIn.RequestType;
		nlapiLogExecution('DEBUG', 'requestType', 'requestType...' + requestType)
        var weekStartDate = dataIn.Data ? dataIn.Data.WeekStartDate : '';

        /*var employeeId =  getUserUsingEmailId('deepak.srinivas@brillio.com');
        var requestType = 'GET';
        var weekStartDate = '10/27/2019';*/

        //For Submit Function
        if (requestType == 'SUBMIT') {
            var timeSheet_Id = dataIn.Data[0].TimeSheetDetails[0].Body[0].TimeSheetID;
            var weekStartDate = dataIn.Data[0].TimeSheetDetails[0].Body[0].WeekStartDate;

            nlapiLogExecution('debug', 'timeSheet_Id', timeSheet_Id);
        }
        if (weekStartDate) {
            //Get Week StartDate
           var weekStDate = nlapiStringToDate(weekStartDate);

            date1 = weekStDate;

            var offsetIST = 5.5;

            //To convert to UTC datetime by subtracting the current Timezone offset
            var utcdate = new Date(date1.getTime() + (date1.getTimezoneOffset() * 60000));
            //nlapiLogExecution('DEBUG', 'schedulerFunction', 'UTC Date' + utcdate);

            //Then cinver the UTS date to the required time zone offset like back to 5.5 for IST
            var istdate = new Date(utcdate.getTime() - ((-offsetIST * 60) * 60000));
            //nlapiLogExecution('DEBUG', 'schedulerFunction', 'IST Date ==' + istdate);
            var day = istdate.getDay();
            var firstday = new Date(istdate.getTime() - 60 * 60 * 24 * day * 1000);
            
			nlapiLogExecution('DEBUG', 'firstday', 'weekstart_date...' + weekStartDate);
            weekStartDate = nlapiDateToString(firstday);
		
            nlapiLogExecution('DEBUG', 'firstday', 'weekstart_date...' + weekStartDate);


        }

        var timesheet_iD = '';
		
        if (requestType == 'GET' || requestType == 'NEXTWEEK') {
            if (weekStartDate && employeeId) {
                checkTS = validateTS(employeeId, weekStartDate);
                if (checkTS.length > 0) {
                    flag = true;
					//nlapiLogExecution("debug","Timesheetzerohours",checkTS[0].getValue("totalhours"));
					if(checkTS[0].getValue("totalhours")=="0:00"){
					Timesheet_zero_hours=true;
					}
                }
            }
        }
        //Test Data
        var endDate = nlapiDateToString(nlapiAddDays(nlapiStringToDate(weekStartDate), 6), 'date');
        var timeSheetID = dataIn.timeSheetid;
        var autoFlag = dataIn.Data ? dataIn.Data.AutoFillFlag : '';
        switch (requestType) {

            case M_Constants.Request.Get:

                if (employeeId && weekStartDate && flag == false) {
                    response.Data = allocationDetails(dataIn, employeeId, weekStartDate, endDate);
                    response.Status = true;
                }
				else if (employeeId && weekStartDate && flag == true && Timesheet_zero_hours==true) {
                    response.Data = allocationDetails(dataIn, employeeId, weekStartDate, endDate, checkTS);
                    response.Status = true;
                } 
				else if (employeeId && weekStartDate && flag == true && Timesheet_zero_hours==false) {
                    response.Data = getTimesheetData(dataIn, employeeId, weekStartDate, endDate, checkTS);
                    response.Status = true;
                } else {
                    response.Data = "Some error with the data sent";
                    response.Status = false;
                }
				
                break;
            case M_Constants.Request.Submit:

                if (timeSheet_Id && employeeId) {
                    response.Data = submitTimeSheets(dataIn,timeSheet_Id,employeeId);//Replaced the structure with timebill and timeitem sublist on timesheet
					nlapiLogExecution('DEBUG',"RemaingUsage",nlapiGetContext().getRemainingUsage());
                    response.Status = true;
                } else {
                    response.Data = "Some error with the data sent";
                    response.Status = false;
                }
                break;
            case M_Constants.Request.NextWeek:

                /*if (employeeId && flag == false) {
                	response.Data = getNextWeekData(dataIn,employeeId,weekStartDate,endDate,autoFlag);
                	response.Status = true;
                }*/
                if (employeeId && weekStartDate && flag == false) {
                    response.Data = allocationDetails(dataIn, employeeId, weekStartDate, endDate);
                    response.Status = true;
                } else if (employeeId && weekStartDate && flag == true) {
                    response.Data = getTimesheetData(dataIn, employeeId, weekStartDate, endDate, checkTS);
                    response.Status = true;
                } else {
                    response.Data = "Some error with the data sent";
                    response.Status = false;
                }
                break;
        }
    } catch (err) {
        nlapiLogExecution('error', 'Restlet Submit Function', err);
        response.Data = err;
        response.Status = false;
    }
    nlapiLogExecution('debug', 'response', JSON.stringify(response));
	nlapiLogExecution("DEBUG","nlapiGetContext().getRemainingUsage()",nlapiGetContext().getRemainingUsage());
    return response;
}

//Submit on Timesheet
function submitTimeSheets(dataIn, timeSheetID, employee) {
    try {
        var status_ts = '';
        var timesheetID = dataIn.Data[0].TimeSheetDetails[0].Body[0].TimeSheetID;
		var weekStart=dataIn.Data[0].TimeSheetDetails[0].Body[0].WeekStartDate;
        nlapiLogExecution('DEBUG', 'Update TimesheetID', timesheetID);
        var loadTS = nlapiLoadRecord('timesheet', timesheetID, {
            recordmode: 'dynamic'
        });
        var approvalstatus = loadTS.getFieldText('approvalstatus');
        nlapiLogExecution('DEBUG', 'Update Timesheet approvalstatus', approvalstatus);
        //Edit will work only for only 2-statuses
        if (approvalstatus == 'Open' || approvalstatus == 'Rejected') {
				/// i know hire date, from eid lookup
				var i_Employee_id = loadTS.getFieldValue('employee');
					nlapiLogExecution('DEBUG', 'i_Employee_id ==', i_Employee_id);	
				var d_Employee_Hire_Date = nlapiLookupField('Employee',i_Employee_id,'hiredate');
					nlapiLogExecution('DEBUG', 'd_Employee_Hire_Date ==', d_Employee_Hire_Date);
					d_Employee_Hire_Date = nlapiStringToDate(d_Employee_Hire_Date);
					nlapiLogExecution('DEBUG', 'd_Employee_Hire_Date after conversion ==', d_Employee_Hire_Date);
            
           // var ts_submit_id = nlapiSubmitRecord(loadTS, false, true);

            nlapiLogExecution('DEBUG', 'TImeSheet Lines Removed');
            //Create New Sublists
            var serviceItemList_1 = dataIn.Data[0].TimeSheetDetails[0].Lines[0].ServiceItems_1;
            var serviceItemList_2 = dataIn.Data[0].TimeSheetDetails[0].Lines[0].ServiceItems_2; //ServiceItems_1
            var serviceItemList_3 = dataIn.Data[0].TimeSheetDetails[0].Lines[0].ServiceItems_3;
            var serviceItemList_4 = dataIn.Data[0].TimeSheetDetails[0].Lines[0].ServiceItems_4;
            var serviceItemList_5 = dataIn.Data[0].TimeSheetDetails[0].Lines[0].ServiceItems_5;
            var serviceItemList_6 = dataIn.Data[0].TimeSheetDetails[0].Lines[0].ServiceItems_6;
            var daysOfWeek = ['hours0', 'hours1', 'hours2', 'hours3', 'hours4', 'hours5', 'hours6']; //added by praveena

            var otg_days = ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'];
            var i_otg_days = ['0', '1', '2', '3', '4', '5', '6'];

            var holiday_list = dataIn.Data[0].TimeSheetDetails[0].Lines[0].Holiday_1;
            var holiday_list_2 = dataIn.Data[0].TimeSheetDetails[0].Lines[0].Holiday_2;
            var holiday_list_3 = dataIn.Data[0].TimeSheetDetails[0].Lines[0].Holiday_3;
            var holiday_list_4 = dataIn.Data[0].TimeSheetDetails[0].Lines[0].Holiday_4;
            var holiday_list_5 = dataIn.Data[0].TimeSheetDetails[0].Lines[0].Holiday_5;
            var holiday_list_6 = dataIn.Data[0].TimeSheetDetails[0].Lines[0].Holiday_6;
            if (holiday_list)
                nlapiLogExecution('DEBUG', 'holiday_list Length', holiday_list.length);
            var leave_list = dataIn.Data[0].TimeSheetDetails[0].Lines[0].Leave_1;
            var leave_list_2 = dataIn.Data[0].TimeSheetDetails[0].Lines[0].Leave_2;
            var leave_list_3 = dataIn.Data[0].TimeSheetDetails[0].Lines[0].Leave_3;
            var leave_list_4 = dataIn.Data[0].TimeSheetDetails[0].Lines[0].Leave_4;
            var leave_list_5 = dataIn.Data[0].TimeSheetDetails[0].Lines[0].Leave_5;
            var leave_list_6 = dataIn.Data[0].TimeSheetDetails[0].Lines[0].Leave_6;
            var ot_list = dataIn.Data[0].TimeSheetDetails[0].Lines[0].OT_1;
            var ot_list_2 = dataIn.Data[0].TimeSheetDetails[0].Lines[0].OT_2;
            var ot_list_3 = dataIn.Data[0].TimeSheetDetails[0].Lines[0].OT_3;
            var ot_list_4 = dataIn.Data[0].TimeSheetDetails[0].Lines[0].OT_4;
            var ot_list_5 = dataIn.Data[0].TimeSheetDetails[0].Lines[0].OT_5;
			
			if(!_logValidation(serviceItemList_1) && !_logValidation(serviceItemList_2) && !_logValidation(serviceItemList_3) && !_logValidation(serviceItemList_4) && !_logValidation(serviceItemList_5) && !_logValidation(serviceItemList_6) && !_logValidation(holiday_list) 
			&& !_logValidation(holiday_list_2) && !_logValidation(holiday_list_3) && !_logValidation(holiday_list_4) && !_logValidation(holiday_list_5) && !_logValidation(holiday_list_6) 
			&& !_logValidation(leave_list) && !_logValidation(leave_list_2) && !_logValidation(leave_list_3) && !_logValidation(leave_list_4) && !_logValidation(leave_list_5)&& !_logValidation(leave_list_6) && !_logValidation(ot_list)&& !_logValidation(ot_list_2)
			&& !_logValidation(ot_list_3) && !_logValidation(ot_list_4) && !_logValidation(ot_list_5) )
			{
				nlapiLogExecution('Audit','Data is blank');
				throw 'Data recieved for submission is blank from the app';
			}
				
				
			
			//Removing Line Items
            var lineCount = loadTS.getLineItemCount('timeitem');
            nlapiLogExecution('DEBUG', 'Update Timesheet Line Length', lineCount);
            for (var n = lineCount; n >= 1; n--) {
                loadTS.removeLineItem('timeitem', n);
                nlapiLogExecution('DEBUG', 'n', n);

            }

            //Service Item_1
            if (_logValidation(serviceItemList_1)) {
               
                for (var len = 0; len < serviceItemList_1.length; len++) {
                    var day_d_1 = serviceItemList_1[len].Days;
                    var project = serviceItemList_1[len].Project;
                    var T_task = serviceItemList_1[len].Task;
                    var H_Hrs = serviceItemList_1[len].Hrs;
                    var s_date = serviceItemList_1[len]["Date"];
					var d_Timesub_Date = nlapiStringToDate(s_date);
					nlapiLogExecution('DEBUG', 'd_Timesub_Date serviceItemList_1 ==', d_Timesub_Date);	/// added by shravan on 7 aug 2020 from Praveena Request
					if(d_Timesub_Date <  d_Employee_Hire_Date)
					{
						nlapiLogExecution('debug','Inside if of condition','on condition');
						throw('Timesheet cannot be submitted before the hire date');
					}///if(d_Timesub_Date >=  d_Employee_Hire_Date)
					/// compare here if throw  till here
					
                    nlapiLogExecution('DEBUG', 'day_d_1', day_d_1);

                    //Logic added by praveena
                    //Get Ineteger no of array
                    var days_d_text = otg_days.indexOf(day_d_1);
					///getting day like sun-0 , mon-1 
                    if (isNumber(days_d_text) && Number(H_Hrs) > 0)
						{
						nlapiLogExecution('DEBUG', 'H_Hrs', H_Hrs);
                        loadTS.setCurrentLineItemText('timeitem', 'customer', project);
                        loadTS.setCurrentLineItemText('timeitem', 'casetaskevent', T_task);
                        loadTS.setCurrentLineItemValue('timeitem', daysOfWeek[days_d_text], H_Hrs);
                        loadTS.setCurrentLineItemValue('timeitem', 'approvalstatus', '2');
                    }


                }
                loadTS.commitLineItem('timeitem');
            }

            //Service Item_2
            if (_logValidation(serviceItemList_2)) {
				 loadTS.selectNewLineItem('timeitem');
                for (var len = 0; len < serviceItemList_2.length; len++) {
                    var day_d_2 = serviceItemList_2[len].Days;
                    var project = serviceItemList_2[len].Project;
                    var T_task = serviceItemList_2[len].Task;
                    var H_Hrs = serviceItemList_2[len].Hrs;
                    nlapiLogExecution('DEBUG', 'day_d_2', day_d_2);
					                    var s_date = serviceItemList_2[len]["Date"];
					var d_Timesub_Date = nlapiStringToDate(s_date);
					nlapiLogExecution('DEBUG', 'd_Timesub_Date serviceItemList_2 ==', d_Timesub_Date);	/// added by shravan on 7 aug 2020 from Praveena Request
					if(d_Timesub_Date <  d_Employee_Hire_Date)
					{
						nlapiLogExecution('debug','Inside if of condition','on condition');
						throw('Timesheet cannot be submitted before the hire date');
					}///if(d_Timesub_Date >=  d_Employee_Hire_Date)
                    //Added by praveena for time tracking sublist 
                    //Get Ineteger no of array
                    var days_d_text = otg_days.indexOf(day_d_2);
                    if (isNumber(days_d_text) && Number(H_Hrs)>0) {
						 nlapiLogExecution('DEBUG', 'day_d_2', day_d_2);
                        loadTS.setCurrentLineItemText('timeitem', 'customer', project);
                        loadTS.setCurrentLineItemText('timeitem', 'casetaskevent', T_task);
                        loadTS.setCurrentLineItemValue('timeitem', daysOfWeek[days_d_text], H_Hrs);
                        loadTS.setCurrentLineItemValue('timeitem', 'approvalstatus', '2');
                    }
                }
				loadTS.commitLineItem('timeitem');
        }
        //Service Item_3
        if (_logValidation(serviceItemList_3)) {
			 loadTS.selectNewLineItem('timeitem');
            for (var len = 0; len < serviceItemList_3.length; len++) {
                var day_d_3 = serviceItemList_3[len].Days;
                var project = serviceItemList_3[len].Project;
                var T_task = serviceItemList_3[len].Task;
                var H_Hrs = serviceItemList_3[len].Hrs;
                nlapiLogExecution('DEBUG', 'day_d_3', day_d_3);
                                 var s_date = serviceItemList_3[len]["Date"];
					var d_Timesub_Date = nlapiStringToDate(s_date);
					nlapiLogExecution('DEBUG', 'd_Timesub_Date serviceItemList_3 ==', d_Timesub_Date);	/// added by shravan on 7 aug 2020 from Praveena Request
					if(d_Timesub_Date <  d_Employee_Hire_Date)
					{
						nlapiLogExecution('debug','Inside if of condition','on condition');
						throw('Timesheet cannot be submitted before the hire date');
					}///if(d_Timesub_Date >=  d_Employee_Hire_Date)
                //Added by praveena for time tracking sublist 
                //Get Ineteger no of array
                var days_d_text = otg_days.indexOf(day_d_3);
                if (isNumber(days_d_text) && Number(H_Hrs)>0) {

                    loadTS.setCurrentLineItemText('timeitem', 'customer', project);
                    loadTS.setCurrentLineItemText('timeitem', 'casetaskevent', T_task);
                    loadTS.setCurrentLineItemValue('timeitem', daysOfWeek[days_d_text], H_Hrs);
                    loadTS.setCurrentLineItemValue('timeitem', 'approvalstatus', '2');

                }
               }
			loadTS.commitLineItem('timeitem');
        }
        //service Item 4
        if (_logValidation(serviceItemList_4)) {
			loadTS.selectNewLineItem('timeitem');
            for (var len = 0; len < serviceItemList_4.length; len++) {
                var day_d = serviceItemList_4[len].Days;
                var project = serviceItemList_4[len].Project;
                var T_task = serviceItemList_4[len].Task;
                var H_Hrs = serviceItemList_4[len].Hrs;
                nlapiLogExecution('DEBUG', 'day_d', day_d);
				  var s_date = serviceItemList_4[len]["Date"];
					var d_Timesub_Date = nlapiStringToDate(s_date);
					nlapiLogExecution('DEBUG', 'd_Timesub_Date serviceItemList_4 ==', d_Timesub_Date);	/// added by shravan on 7 aug 2020 from Praveena Request
					if(d_Timesub_Date <  d_Employee_Hire_Date)
					{
						nlapiLogExecution('debug','Inside if of condition','on condition');
						throw('Timesheet cannot be submitted before the hire date');
					}///if(d_Timesub_Date >=  d_Employee_Hire_Date)
                //Added by praveena for time tracking sublist 
                //Get Ineteger no of array
                var days_d_text = otg_days.indexOf(day_d);
               // if (isNumber(days_d_text) && Number(H_Hrs)!=0) {
				    if (isNumber(days_d_text)) {

                    loadTS.setCurrentLineItemText('timeitem', 'customer', project);
                    loadTS.setCurrentLineItemText('timeitem', 'casetaskevent', T_task);
                    loadTS.setCurrentLineItemValue('timeitem', daysOfWeek[days_d_text], H_Hrs);
                    loadTS.setCurrentLineItemValue('timeitem', 'approvalstatus', '2');

                }
				
			}
                loadTS.commitLineItem('timeitem');

        }
        //service Item 5
        if (_logValidation(serviceItemList_5)) {
			loadTS.selectNewLineItem('timeitem');
                
            for (var len = 0; len < serviceItemList_5.length; len++) {
                var day_d = serviceItemList_5[len].Days;
                var project = serviceItemList_5[len].Project;
                var T_task = serviceItemList_5[len].Task;
                var H_Hrs = serviceItemList_5[len].Hrs;
                nlapiLogExecution('DEBUG', 'day_d', day_d);
				  var s_date = serviceItemList_5[len]["Date"];
					var d_Timesub_Date = nlapiStringToDate(s_date);
					nlapiLogExecution('DEBUG', 'd_Timesub_Date serviceItemList_5 ==', d_Timesub_Date);	/// added by shravan on 7 aug 2020 from Praveena Request
					if(d_Timesub_Date <  d_Employee_Hire_Date)
					{
						nlapiLogExecution('debug','Inside if of condition','on condition');
						throw('Timesheet cannot be submitted before the hire date');
					}///if(d_Timesub_Date >=  d_Employee_Hire_Date)
				
                //Added by praveena for time tracking sublist 
                //Get Ineteger no of array
                var days_d_text = otg_days.indexOf(day_d);
                if (isNumber(days_d_text) && Number(H_Hrs)!=0) {

                    loadTS.setCurrentLineItemText('timeitem', 'customer', project);
                    loadTS.setCurrentLineItemText('timeitem', 'casetaskevent', T_task);
                    loadTS.setCurrentLineItemValue('timeitem', daysOfWeek[days_d_text], H_Hrs);
                    loadTS.setCurrentLineItemValue('timeitem', 'approvalstatus', '2');

                }
                
            }
			loadTS.commitLineItem('timeitem'); 
        }
        //service Item 6
        if (_logValidation(serviceItemList_6)) {
			 loadTS.selectNewLineItem('timeitem');
            for (var len = 0; len < serviceItemList_6.length; len++) {
                var day_d = serviceItemList_6[len].Days;
                var project = serviceItemList_6[len].Project;
                var T_task = serviceItemList_6[len].Task;
                var H_Hrs = serviceItemList_6[len].Hrs;
                nlapiLogExecution('DEBUG', 'day_d', day_d);
                 var s_date = serviceItemList_6[len]["Date"];
					var d_Timesub_Date = nlapiStringToDate(s_date);
					nlapiLogExecution('DEBUG', 'd_Timesub_Date serviceItemList_6 ==', d_Timesub_Date);	/// added by shravan on 7 aug 2020 from Praveena Request
					if(d_Timesub_Date <  d_Employee_Hire_Date)
					{
						nlapiLogExecution('debug','Inside if of condition','on condition');
						throw('Timesheet cannot be submitted before the hire date');
					}///if(d_Timesub_Date >=  d_Employee_Hire_Date)
                //Added by praveena for time tracking sublist 
                //Get Ineteger no of array
                var days_d_text = otg_days.indexOf(day_d);
                if (isNumber(days_d_text) && Number(H_Hrs)>0) {

                    loadTS.setCurrentLineItemText('timeitem', 'customer', project);
                    loadTS.setCurrentLineItemText('timeitem', 'casetaskevent', T_task);
                    loadTS.setCurrentLineItemValue('timeitem', daysOfWeek[days_d_text], H_Hrs);
                    loadTS.setCurrentLineItemValue('timeitem', 'approvalstatus', '2');

                }
                
            }
			loadTS.commitLineItem('timeitem');
        }
        //Holiday Item
        if (_logValidation(holiday_list)) {
			 loadTS.selectNewLineItem('timeitem');
            for (var len = 0; len < holiday_list.length; len++) {
                var day_d_h = holiday_list[len].Days;
                var project_h = holiday_list[len].Project;
                var T_task_h = holiday_list[len].Task;
                var H_Hrs_h = holiday_list[len].Hrs;
                nlapiLogExecution('DEBUG', 'day_d_h', day_d_h);
					var s_date = holiday_list[len]["Date"];
					var d_Timesub_Date = nlapiStringToDate(s_date);
					nlapiLogExecution('DEBUG', 'd_Timesub_Date holiday_list ==', d_Timesub_Date);	/// added by shravan on 7 aug 2020 from Praveena Request
					if(d_Timesub_Date <  d_Employee_Hire_Date)
					{
						nlapiLogExecution('debug','Inside if of condition','on condition');
						throw('Timesheet cannot be submitted before the hire date');
					}///if(d_Timesub_Date >=  d_Employee_Hire_Date)

               
                //Added by praveena for time tracking sublist 
                //Get Ineteger no of array
                var days_d_text = otg_days.indexOf(day_d_h);
                if (isNumber(days_d_text) && Number(H_Hrs_h)!=0) {

                    loadTS.setCurrentLineItemText('timeitem', 'customer', project_h);
                    loadTS.setCurrentLineItemText('timeitem', 'casetaskevent', T_task_h);
                    loadTS.setCurrentLineItemValue('timeitem', daysOfWeek[days_d_text], H_Hrs_h);
                    loadTS.setCurrentLineItemValue('timeitem', 'approvalstatus', '2');
                }
               }
			   loadTS.commitLineItem('timeitem');
        }

        if (_logValidation(holiday_list_2)) {
			loadTS.selectNewLineItem('timeitem');
            for (var len = 0; len < holiday_list_2.length; len++) {
                var day_d_h = holiday_list_2[len].Days;
                var project_h = holiday_list_2[len].Project;
                var T_task_h = holiday_list_2[len].Task;
                var H_Hrs_h = holiday_list_2[len].Hrs;
                nlapiLogExecution('DEBUG', 'day_d_h', day_d_h);
					var s_date = holiday_list_2[len]["Date"];
					var d_Timesub_Date = nlapiStringToDate(s_date);
					nlapiLogExecution('DEBUG', 'd_Timesub_Date holiday_list_2 ==', d_Timesub_Date);	/// added by shravan on 7 aug 2020 from Praveena Request
					if(d_Timesub_Date <  d_Employee_Hire_Date)
					{
						nlapiLogExecution('debug','Inside if of condition','on condition');
						throw('Timesheet cannot be submitted before the hire date');
					}///if(d_Timesub_Date >=  d_Employee_Hire_Date)

                
                //Added by praveena for time tracking sublist 
                //Get Ineteger no of array
                var days_d_text = otg_days.indexOf(day_d_h);
                 if (isNumber(days_d_text) && Number(H_Hrs_h)>0){

                    loadTS.setCurrentLineItemText('timeitem', 'customer', project_h);
                    loadTS.setCurrentLineItemText('timeitem', 'casetaskevent', T_task_h);
                    loadTS.setCurrentLineItemValue('timeitem', daysOfWeek[days_d_text], H_Hrs_h);
                    loadTS.setCurrentLineItemValue('timeitem', 'approvalstatus', '2');
                }
          
            }
			loadTS.commitLineItem('timeitem');
        }
        if (_logValidation(holiday_list_3)) {
			loadTS.selectNewLineItem('timeitem');
            for (var len = 0; len < holiday_list_3.length; len++) {
                var day_d_h = holiday_list_3[len].Days;
                var project_h = holiday_list_3[len].Project;
                var T_task_h = holiday_list_3[len].Task;
                var H_Hrs_h = holiday_list_3[len].Hrs;
                nlapiLogExecution('DEBUG', 'day_d_h', day_d_h);
				//Added by praveena for time tracking sublist 
                //Get Ineteger no of array
					var s_date = holiday_list_3[len]["Date"];
					var d_Timesub_Date = nlapiStringToDate(s_date);
					nlapiLogExecution('DEBUG', 'd_Timesub_Date holiday_list_3 ==', d_Timesub_Date);	/// added by shravan on 7 aug 2020 from Praveena Request
					if(d_Timesub_Date <  d_Employee_Hire_Date)
					{
						nlapiLogExecution('debug','Inside if of condition','on condition');
						throw('Timesheet cannot be submitted before the hire date');
					}///if(d_Timesub_Date >=  d_Employee_Hire_Date)
                var days_d_text = otg_days.indexOf(day_d_h);
                 if (isNumber(days_d_text) && Number(H_Hrs_h)!=0){

                    loadTS.setCurrentLineItemText('timeitem', 'customer', project_h);
                    loadTS.setCurrentLineItemText('timeitem', 'casetaskevent', T_task_h);
                    loadTS.setCurrentLineItemValue('timeitem', daysOfWeek[days_d_text], H_Hrs_h);
                    loadTS.setCurrentLineItemValue('timeitem', 'approvalstatus', '2');
                }
               
            }
			loadTS.commitLineItem('timeitem');
        }
        if (_logValidation(holiday_list_4)) {
			loadTS.selectNewLineItem('timeitem');
            for (var len = 0; len < holiday_list_4.length; len++) {
                var day_d_h = holiday_list_4[len].Days;
                var project_h = holiday_list_4[len].Project;
                var T_task_h = holiday_list_4[len].Task;
                var H_Hrs_h = holiday_list_4[len].Hrs;
                nlapiLogExecution('DEBUG', 'day_d_h', day_d_h);
					var s_date = holiday_list_4[len]["Date"];
					var d_Timesub_Date = nlapiStringToDate(s_date);
					nlapiLogExecution('DEBUG', 'd_Timesub_Date holiday_list_4 ==', d_Timesub_Date);	/// added by shravan on 7 aug 2020 from Praveena Request
					if(d_Timesub_Date <  d_Employee_Hire_Date)
					{
						nlapiLogExecution('debug','Inside if of condition','on condition');
						throw('Timesheet cannot be submitted before the hire date');
					}///if(d_Timesub_Date >=  d_Employee_Hire_Date)

                
                //Added by praveena for time tracking sublist 
                //Get Ineteger no of array
                var days_d_text = otg_days.indexOf(day_d_h);
                if (isNumber(days_d_text) && Number(H_Hrs_h)>0) {

                    loadTS.setCurrentLineItemText('timeitem', 'customer', project_h);
                    loadTS.setCurrentLineItemText('timeitem', 'casetaskevent', T_task_h);
                    loadTS.setCurrentLineItemValue('timeitem', daysOfWeek[days_d_text], H_Hrs_h);
                    loadTS.setCurrentLineItemValue('timeitem', 'approvalstatus', '2');
                }
                
            }
			loadTS.commitLineItem('timeitem');
        }
        if (_logValidation(holiday_list_5)) {
			 loadTS.selectNewLineItem('timeitem');
            for (var len = 0; len < holiday_list_5.length; len++) {
                var day_d_h = holiday_list_5[len].Days;
                var project_h = holiday_list_5[len].Project;
                var T_task_h = holiday_list_5[len].Task;
                var H_Hrs_h = holiday_list_5[len].Hrs;
                nlapiLogExecution('DEBUG', 'day_d_h', day_d_h);
				
					var s_date = holiday_list_5[len]["Date"];
					var d_Timesub_Date = nlapiStringToDate(s_date);
					nlapiLogExecution('DEBUG', 'd_Timesub_Date holiday_list_5 ==', d_Timesub_Date);	/// added by shravan on 7 aug 2020 from Praveena Request
					if(d_Timesub_Date <  d_Employee_Hire_Date)
					{
						nlapiLogExecution('debug','Inside if of condition','on condition');
						throw('Timesheet cannot be submitted before the hire date');
					}///if(d_Timesub_Date >=  d_Employee_Hire_Date)
               


                //Added by praveena for time tracking sublist 
                //Get Ineteger no of array
                var days_d_text = otg_days.indexOf(day_d_h);
                 if (isNumber(days_d_text) && Number(H_Hrs_h)>0) {

                    loadTS.setCurrentLineItemText('timeitem', 'customer', project_h);
                    loadTS.setCurrentLineItemText('timeitem', 'casetaskevent', T_task_h);
                    loadTS.setCurrentLineItemValue('timeitem', daysOfWeek[days_d_text], H_Hrs_h);
                    loadTS.setCurrentLineItemValue('timeitem', 'approvalstatus', '2');
                }
		}
		loadTS.commitLineItem('timeitem');
        }
        if (_logValidation(holiday_list_6)) {
			loadTS.selectNewLineItem('timeitem');
            for (var len = 0; len < holiday_list_6.length; len++) {
                var day_d_h = holiday_list_6[len].Days;
                var project_h = holiday_list_6[len].Project;
                var T_task_h = holiday_list_6[len].Task;
                var H_Hrs_h = holiday_list_6[len].Hrs;
                nlapiLogExecution('DEBUG', 'day_d_h', day_d_h);
					var s_date = holiday_list_6[len]["Date"];
					var d_Timesub_Date = nlapiStringToDate(s_date);
					nlapiLogExecution('DEBUG', 'd_Timesub_Date holiday_list_6 ==', d_Timesub_Date);	/// added by shravan on 7 aug 2020 from Praveena Request
					if(d_Timesub_Date <  d_Employee_Hire_Date)
					{
						nlapiLogExecution('debug','Inside if of condition','on condition');
						throw('Timesheet cannot be submitted before the hire date');
					}///if(d_Timesub_Date >=  d_Employee_Hire_Date)

                
                //Added by praveena for time tracking sublist 
                //Get Ineteger no of array
                var days_d_text = otg_days.indexOf(day_d_h);
                 if (isNumber(days_d_text) && Number(H_Hrs_h)>0){

                    loadTS.setCurrentLineItemText('timeitem', 'customer', project_h);
                    loadTS.setCurrentLineItemText('timeitem', 'casetaskevent', T_task_h);
                    loadTS.setCurrentLineItemValue('timeitem', daysOfWeek[days_d_text], H_Hrs_h);
                    loadTS.setCurrentLineItemValue('timeitem', 'approvalstatus', '2');
                }
               }
			   loadTS.commitLineItem('timeitem');
        }
        //Leave Item
        if (_logValidation(leave_list)) {
			 loadTS.selectNewLineItem('timeitem');
            for (var len = 0; len < leave_list.length; len++) {
                var day_d_p = leave_list[len].Days;
                var project_p = leave_list[len].Project;
                var T_task_p = leave_list[len].Task;
                var H_Hrs_p = leave_list[len].Hrs;
                nlapiLogExecution('DEBUG', 'day_d_p', day_d_p);
					var s_date = leave_list[len]["Date"];
					var d_Timesub_Date = nlapiStringToDate(s_date);
					nlapiLogExecution('DEBUG', 'd_Timesub_Date leave_list ==', d_Timesub_Date);	/// added by shravan on 7 aug 2020 from Praveena Request
					if(d_Timesub_Date <  d_Employee_Hire_Date)
					{
						nlapiLogExecution('debug','Inside if of condition','on condition');
						throw('Timesheet cannot be submitted before the hire date');
					}///if(d_Timesub_Date >=  d_Employee_Hire_Date)
               
                //Added by praveena for time tracking sublist 
                //Get Ineteger no of array
                var days_d_text = otg_days.indexOf(day_d_p);
                 if (isNumber(days_d_text) && Number(H_Hrs_p)!=0){

                    loadTS.setCurrentLineItemText('timeitem', 'customer', project_p);
                    loadTS.setCurrentLineItemText('timeitem', 'casetaskevent', T_task_p);
                    loadTS.setCurrentLineItemValue('timeitem', daysOfWeek[days_d_text], H_Hrs_p);
                    loadTS.setCurrentLineItemValue('timeitem', 'approvalstatus', '2');
                }
                
			
            }
			loadTS.commitLineItem('timeitem');
        }
        if (_logValidation(leave_list_2)) {
			 loadTS.selectNewLineItem('timeitem');
            for (var len = 0; len < leave_list_2.length; len++) {
                var day_d_p = leave_list_2[len].Days;
                var project_p = leave_list_2[len].Project;
                var T_task_p = leave_list_2[len].Task;
                var H_Hrs_p = leave_list_2[len].Hrs;
                nlapiLogExecution('DEBUG', 'day_d_p', day_d_p);
	var s_date = leave_list_2[len]["Date"];
					var d_Timesub_Date = nlapiStringToDate(s_date);
					nlapiLogExecution('DEBUG', 'd_Timesub_Date leave_list_2 ==', d_Timesub_Date);	/// added by shravan on 7 aug 2020 from Praveena Request
					if(d_Timesub_Date <  d_Employee_Hire_Date)
					{
						nlapiLogExecution('debug','Inside if of condition','on condition');
						throw('Timesheet cannot be submitted before the hire date');
					}///if(d_Timesub_Date >=  d_Employee_Hire_Date)
               
                //Added by praveena for time tracking sublist 
                //Get Ineteger no of array
                var days_d_text = otg_days.indexOf(day_d_p);
                 if (isNumber(days_d_text) && Number(H_Hrs_p)!=0){

                    loadTS.setCurrentLineItemText('timeitem', 'customer', project_p);
                    loadTS.setCurrentLineItemText('timeitem', 'casetaskevent', T_task_p);
                    loadTS.setCurrentLineItemValue('timeitem', daysOfWeek[days_d_text], H_Hrs_p);
                    loadTS.setCurrentLineItemValue('timeitem', 'approvalstatus', '2');
                }
                

            }
			loadTS.commitLineItem('timeitem');
        }
        if (_logValidation(leave_list_3)) {
			 loadTS.selectNewLineItem('timeitem');
            for (var len = 0; len < leave_list_3.length; len++) {
                var day_d_p = leave_list_3[len].Days;
                var project_p = leave_list_3[len].Project;
                var T_task_p = leave_list_3[len].Task;
                var H_Hrs_p = leave_list_3[len].Hrs;
                nlapiLogExecution('DEBUG', 'day_d_p', day_d_p);
	var s_date = leave_list_3[len]["Date"];
					var d_Timesub_Date = nlapiStringToDate(s_date);
					nlapiLogExecution('DEBUG', 'd_Timesub_Date leave_list_3 ==', d_Timesub_Date);	/// added by shravan on 7 aug 2020 from Praveena Request
					if(d_Timesub_Date <  d_Employee_Hire_Date)
					{
						nlapiLogExecution('debug','Inside if of condition','on condition');
						throw('Timesheet cannot be submitted before the hire date');
					}///if(d_Timesub_Date >=  d_Employee_Hire_Date)
               
                //Added by praveena for time tracking sublist 
                //Get Ineteger no of array
                var days_d_text = otg_days.indexOf(day_d_p);
                if (isNumber(days_d_text) && Number(H_Hrs_p)!=0){

                    loadTS.setCurrentLineItemText('timeitem', 'customer', project_p);
                    loadTS.setCurrentLineItemText('timeitem', 'casetaskevent', T_task_p);
                    loadTS.setCurrentLineItemValue('timeitem', daysOfWeek[days_d_text], H_Hrs_p);
                    loadTS.setCurrentLineItemValue('timeitem', 'approvalstatus', '2');
                } 
               
            }
			 loadTS.commitLineItem('timeitem');
        }
        if (_logValidation(leave_list_4)) {
			loadTS.selectNewLineItem('timeitem');
            for (var len = 0; len < leave_list_4.length; len++) {
                var day_d_p = leave_list_4[len].Days;
                var project_p = leave_list_4[len].Project;
                var T_task_p = leave_list_4[len].Task;
                var H_Hrs_p = leave_list_4[len].Hrs;
                nlapiLogExecution('DEBUG', 'day_d_p', day_d_p);
					var s_date = leave_list_4[len]["Date"];
					var d_Timesub_Date = nlapiStringToDate(s_date);
					nlapiLogExecution('DEBUG', 'd_Timesub_Date leave_list_4 ==', d_Timesub_Date);	/// added by shravan on 7 aug 2020 from Praveena Request
					if(d_Timesub_Date <  d_Employee_Hire_Date)
					{
						nlapiLogExecution('debug','Inside if of condition','on condition');
						throw('Timesheet cannot be submitted before the hire date');
					}///if(d_Timesub_Date >=  d_Employee_Hire_Date)
                
                //Added by praveena for time tracking sublist 
                //Get Ineteger no of array
                var days_d_text = otg_days.indexOf(day_d_p);
                 if (isNumber(days_d_text) && Number(H_Hrs_p)!=0){

                    loadTS.setCurrentLineItemText('timeitem', 'customer', project_p);
                    loadTS.setCurrentLineItemText('timeitem', 'casetaskevent', T_task_p);
                    loadTS.setCurrentLineItemValue('timeitem', daysOfWeek[days_d_text], H_Hrs_p);
                    loadTS.setCurrentLineItemValue('timeitem', 'approvalstatus', '2');
                }
               
               
            }
			 loadTS.commitLineItem('timeitem');

        }
        if (_logValidation(leave_list_5)) {
			loadTS.selectNewLineItem('timeitem');
			
            for (var len = 0; len < leave_list_5.length; len++) {
                var day_d_p = leave_list_5[len].Days;
                var project_p = leave_list_5[len].Project;
                var T_task_p = leave_list_5[len].Task;
                var H_Hrs_p = leave_list_5[len].Hrs;
                nlapiLogExecution('DEBUG', 'day_d_p', day_d_p);
		var s_date = leave_list_5[len]["Date"];
					var d_Timesub_Date = nlapiStringToDate(s_date);
					nlapiLogExecution('DEBUG', 'd_Timesub_Date leave_list_5 ==', d_Timesub_Date);	/// added by shravan on 7 aug 2020 from Praveena Request
					if(d_Timesub_Date <  d_Employee_Hire_Date)
					{
						nlapiLogExecution('debug','Inside if of condition','on condition');
						throw('Timesheet cannot be submitted before the hire date');
					}///if(d_Timesub_Date >=  d_Employee_Hire_Date)
                
                //Added by praveena for time tracking sublist 
                //Get Ineteger no of array
                var days_d_text = otg_days.indexOf(day_d_p);
                 if (isNumber(days_d_text) && Number(H_Hrs_p)!=0){

                    loadTS.setCurrentLineItemText('timeitem', 'customer', project_p);
                    loadTS.setCurrentLineItemText('timeitem', 'casetaskevent', T_task_p);
                    loadTS.setCurrentLineItemValue('timeitem', daysOfWeek[days_d_text], H_Hrs_p);
                    loadTS.setCurrentLineItemValue('timeitem', 'approvalstatus', '2');
                }
            }
			loadTS.commitLineItem('timeitem');
        }
        if (_logValidation(leave_list_6)) {
			loadTS.selectNewLineItem('timeitem');
            for (var len = 0; len < leave_list_6.length; len++) {
                var day_d_p = leave_list_6[len].Days;
                var project_p = leave_list_6[len].Project;
                var T_task_p = leave_list_6[len].Task;
                var H_Hrs_p = leave_list_6[len].Hrs;
                nlapiLogExecution('DEBUG', 'day_d_p', day_d_p);
					var s_date = leave_list_6[len]["Date"];
					var d_Timesub_Date = nlapiStringToDate(s_date);
					nlapiLogExecution('DEBUG', 'd_Timesub_Date leave_list_6 ==', d_Timesub_Date);	/// added by shravan on 7 aug 2020 from Praveena Request
					if(d_Timesub_Date <  d_Employee_Hire_Date)
					{
						nlapiLogExecution('debug','Inside if of condition','on condition');
						throw('Timesheet cannot be submitted before the hire date');
					}///if(d_Timesub_Date >=  d_Employee_Hire_Date)
                
                //Added by praveena for time tracking sublist 
                //Get Ineteger no of array
                var days_d_text = otg_days.indexOf(day_d_p);
                 if (isNumber(days_d_text) && Number(H_Hrs_p)!=0){

                    loadTS.setCurrentLineItemText('timeitem', 'customer', project_p);
                    loadTS.setCurrentLineItemText('timeitem', 'casetaskevent', T_task_p);
                    loadTS.setCurrentLineItemValue('timeitem', daysOfWeek[days_d_text], H_Hrs_p);
                    loadTS.setCurrentLineItemValue('timeitem', 'approvalstatus', '2');
                }
               

            }
			loadTS.commitLineItem('timeitem');
        }
        //OT Item
        if (_logValidation(ot_list)) {
			 loadTS.selectNewLineItem('timeitem');
            for (var len = 0; len < ot_list.length; len++) {
                var day_d_0 = ot_list[len].Days;
                var project_0 = ot_list[len].Project;
                var T_task_o = ot_list[len].Task;
                var H_Hrs_o = ot_list[len].OTHrs;
                nlapiLogExecution('DEBUG', 'day_d_0', day_d_0);
				var s_date = ot_list[len]["Date"];
					var d_Timesub_Date = nlapiStringToDate(s_date);
					nlapiLogExecution('DEBUG', 'd_Timesub_Date ot_list ==', d_Timesub_Date);	/// added by shravan on 7 aug 2020 from Praveena Request
					if(d_Timesub_Date <  d_Employee_Hire_Date)
					{
						nlapiLogExecution('debug','Inside if of condition','on condition');
						throw('Timesheet cannot be submitted before the hire date');
					}///if(d_Timesub_Date >=  d_Employee_Hire_Date)
               
                //Added by praveena for time tracking sublist 
                //Get Ineteger no of array
                var days_d_text = otg_days.indexOf(day_d_0);
                 if (isNumber(days_d_text) && Number(H_Hrs_o)!=0){

                    loadTS.setCurrentLineItemText('timeitem', 'customer', project_0);
                    loadTS.setCurrentLineItemText('timeitem', 'casetaskevent', T_task_o);
                    loadTS.setCurrentLineItemValue('timeitem', daysOfWeek[days_d_text], H_Hrs_o);
                    loadTS.setCurrentLineItemValue('timeitem', 'approvalstatus', '2');
                }
                

            }
			loadTS.commitLineItem('timeitem');
        }

        if (_logValidation(ot_list_2)) {
			loadTS.selectNewLineItem('timeitem');
            for (var len = 0; len < ot_list_2.length; len++) {
                var day_d_0 = ot_list_2[len].Days;
                var project_0 = ot_list_2[len].Project;
                var T_task_o = ot_list_2[len].Task;
                var H_Hrs_o = ot_list_2[len].OTHrs;
                nlapiLogExecution('DEBUG', 'day_d_0', day_d_0);
				var s_date = ot_list_2[len]["Date"];
					var d_Timesub_Date = nlapiStringToDate(s_date);
					nlapiLogExecution('DEBUG', 'd_Timesub_Date ot_list_2 ==', d_Timesub_Date);	/// added by shravan on 7 aug 2020 from Praveena Request
					if(d_Timesub_Date <  d_Employee_Hire_Date)
					{
						nlapiLogExecution('debug','Inside if of condition','on condition');
						throw('Timesheet cannot be submitted before the hire date');
					}///if(d_Timesub_Date >=  d_Employee_Hire_Date)
                
                //Added by praveena for time tracking sublist 
                //Get Ineteger no of array
                var days_d_text = otg_days.indexOf(day_d_p);
                 if (isNumber(days_d_text) && Number(H_Hrs_o)!=0){

                    loadTS.setCurrentLineItemText('timeitem', 'customer', project_p);
                    loadTS.setCurrentLineItemText('timeitem', 'casetaskevent', T_task_p);
                    loadTS.setCurrentLineItemValue('timeitem', daysOfWeek[days_d_text], H_Hrs_p);
                    loadTS.setCurrentLineItemValue('timeitem', 'approvalstatus', '2');
                }
                

            }
			loadTS.commitLineItem('timeitem');
        }
        if (_logValidation(ot_list_3)) {
			loadTS.selectNewLineItem('timeitem');
            for (var len = 0; len < ot_list_3.length; len++) {
                var day_d_0 = ot_list_3[len].Days;
                var project_0 = ot_list_3[len].Project;
                var T_task_o = ot_list_3[len].Task;
                var H_Hrs_o = ot_list_3[len].OTHrs;
                nlapiLogExecution('DEBUG', 'day_d_0', day_d_0);
				var s_date = ot_list_3[len]["Date"];
					var d_Timesub_Date = nlapiStringToDate(s_date);
					nlapiLogExecution('DEBUG', 'd_Timesub_Date ot_list_3 ==', d_Timesub_Date);	/// added by shravan on 7 aug 2020 from Praveena Request
					if(d_Timesub_Date <  d_Employee_Hire_Date)
					{
						nlapiLogExecution('debug','Inside if of condition','on condition');
						throw('Timesheet cannot be submitted before the hire date');
					}///if(d_Timesub_Date >=  d_Employee_Hire_Date)
                
                //Added by praveena for time tracking sublist 
                //Get Ineteger no of array
                var days_d_text = otg_days.indexOf(day_d_0);
                 if (isNumber(days_d_text) && Number(H_Hrs_o)!=0){

                    loadTS.setCurrentLineItemText('timeitem', 'customer', project_0);
                    loadTS.setCurrentLineItemText('timeitem', 'casetaskevent', T_task_o);
                    loadTS.setCurrentLineItemValue('timeitem', daysOfWeek[days_d_text], H_Hrs_o);
                    loadTS.setCurrentLineItemValue('timeitem', 'approvalstatus', '2');
                }
                
            }
			loadTS.commitLineItem('timeitem');
        }
        if (_logValidation(ot_list_4)) {
			 loadTS.selectNewLineItem('timeitem');
            for (var len = 0; len < ot_list_4.length; len++) {
                var day_d_0 = ot_list_4[len].Days;
                var project_0 = ot_list_4[len].Project;
                var T_task_o = ot_list_4[len].Task;
                var H_Hrs_o = ot_list_4[len].OTHrs;
                nlapiLogExecution('DEBUG', 'day_d_0', day_d_0);
				var s_date = ot_list_4[len]["Date"];
					var d_Timesub_Date = nlapiStringToDate(s_date);
					nlapiLogExecution('DEBUG', 'd_Timesub_Date ot_list_4 ==', d_Timesub_Date);	/// added by shravan on 7 aug 2020 from Praveena Request
					if(d_Timesub_Date <  d_Employee_Hire_Date)
					{
						nlapiLogExecution('debug','Inside if of condition','on condition');
						throw('Timesheet cannot be submitted before the hire date');
					}///if(d_Timesub_Date >=  d_Employee_Hire_Date)
               

                //Added by praveena for time tracking sublist 
                //Get Ineteger no of array
                var days_d_text = otg_days.indexOf(day_d_0);
                if (isNumber(days_d_text) && Number(H_Hrs_o)!=0){

                    loadTS.setCurrentLineItemText('timeitem', 'customer', project_0);
                    loadTS.setCurrentLineItemText('timeitem', 'casetaskevent', T_task_o);
                    loadTS.setCurrentLineItemValue('timeitem', daysOfWeek[days_d_text], H_Hrs_o);
                    loadTS.setCurrentLineItemValue('timeitem', 'approvalstatus', '2');
                }
                

            }
			loadTS.commitLineItem('timeitem');
        }
        if (_logValidation(ot_list_5)) {
			 loadTS.selectNewLineItem('timeitem');
            for (var len = 0; len < ot_list_5.length; len++) {
                var day_d_0 = ot_list_5[len].Days;
                var project_0 = ot_list_5[len].Project;
                var T_task_o = ot_list_5[len].Task;
                var H_Hrs_o = ot_list_5[len].OTHrs;
                nlapiLogExecution('DEBUG', 'day_d_0', day_d_0);
	var s_date = ot_list_5[len]["Date"];
					var d_Timesub_Date = nlapiStringToDate(s_date);
					nlapiLogExecution('DEBUG', 'd_Timesub_Date ot_list_5 ==', d_Timesub_Date);	/// added by shravan on 7 aug 2020 from Praveena Request
					if(d_Timesub_Date <  d_Employee_Hire_Date)
					{
						nlapiLogExecution('debug','Inside if of condition','on condition');
						throw('Timesheet cannot be submitted before the hire date');
					}///if(d_Timesub_Date >=  d_Employee_Hire_Date)
               
                //Get Ineteger no of array
                var days_d_text = otg_days.indexOf(day_d_0);
                 if (isNumber(days_d_text) && Number(H_Hrs_o)!=0){

                    loadTS.setCurrentLineItemValue('timeitem', 'customer', project_0);
                    loadTS.setCurrentLineItemText('timeitem', 'casetaskevent', T_task_o);
                    loadTS.setCurrentLineItemValue('timeitem', daysOfWeek[days_d_text], H_Hrs_o);
                    loadTS.setCurrentLineItemValue('timeitem', 'approvalstatus', 2);
                }
               

            }
			 loadTS.commitLineItem('timeitem');
        }
		
        loadTS.setFieldValue('iscomplete', 'T');
		
		/// i know hire date, from eid lookup
		///if 
        var ts_submit_id = nlapiSubmitRecord(loadTS, true, true);
        var timesheetSearch = nlapiSearchRecord("timesheet", null,
            [	
				["internalid", "anyof", ts_submit_id]
            ],
            [
                new nlobjSearchColumn("internalid", "timeBill", null)
            ]
        );
        for (var j = 0;timesheetSearch!=null && timesheetSearch.length > j; j++) {
            var Obj_timebill_Rec = nlapiSubmitField('timebill', timesheetSearch[j].getValue("internalid", "timeBill"), ['approvalstatus'], ['2']);
       nlapiLogExecution('DEBUG', 'Obj_timebill_Rec-Submit', Obj_timebill_Rec);
	   }
	   
        nlapiLogExecution('DEBUG', 'ts_submit_id-Submit', ts_submit_id);
        status_ts = nlapiLookupField('timesheet', ts_submit_id, 'approvalstatus', true);
    }
    return status_ts;
} catch (err) {
    nlapiLogExecution('error', 'Restlet Main Function', err);
    throw err;
}
}



//Get TS Data if  already filled
function getTimesheetData(dataIn, employeeId, startDate, endDate, TSObj) {
    try {
        var JSON_Total = {};
        var dataRow_Total = [];
        var OT_Applicable = '';
        var ts_startDate = '';
        var ts_endDate = '';
        var ts_status = '';
        var task_t = '';
        var item_t = '';
        var OT_Applicable = [];
        var date_string = startDate;
        var date_string_Items = startDate;
        // get active allocations, hours ,etc.
        var allocationDetails = getActiveAllocations(startDate, endDate,
            employeeId);

	




        var allocationDetails_items = getActiveAllocations_Items(startDate, endDate,
            employeeId);
			
			
			
			
			
        //GET OT Flag From Allocation
        allocationDetails_items.forEach(function(project) {
            //OT_Applicable = [];
            OT_Applicable.push({
                'OT_Applicable': project.OT,
                'Project': project.Project
            });
            //	OT_Applicable = project.OT;
            //Project
        });
        allocationDetails.forEach(function(project) {
            task_t = project.Task;
        });
        var JSON_TS_Body = {};
        var dataRow_TS_Body = [];
        var dataRow_Items = [];
        var JSON_Items = {};
        var time_approver_email = '';
        if (TSObj) {
            var timesheetID = TSObj[0].getId();
            var load_TS = nlapiLoadRecord('timesheet', timesheetID);
			
			nlapiLogExecution('debug','timesheetID',timesheetID);//VNA19052020
            var emp = load_TS.getFieldText('employee');
            var emp_id = load_TS.getFieldValue('employee');
            var emp_lookUP = nlapiLookupField('employee', emp_id, ['email']);
            var emp_email = emp_lookUP.email;
            var emp_TS_approver = load_TS.getFieldText('custrecord_ts_time_approver');
            var weekStart = load_TS.getFieldValue('startdate');
            var weekEnd = load_TS.getFieldValue('enddate');
            var totalHrs = load_TS.getFieldValue('totalhours');
            var approvalstatus = load_TS.getFieldText('approvalstatus');

			nlapiLogExecution('debug','totalHrs',totalHrs);//VNA19052020
			nlapiLogExecution('debug','approvalstatus',approvalstatus);//VNA19052020
		
            var s_hireDate = nlapiLookupField('employee', parseInt(emp_id), ['hiredate', 'timeapprover']);
            if (s_hireDate.timeapprover)
                time_approver_email = nlapiLookupField('employee', s_hireDate.timeapprover, 'email');

            JSON_TS_Body = {
                TimeSheetID: timesheetID,
                Employee: emp,
                Email: emp_email,
                timesheetApprover: _logValidation(time_approver_email) == false ? ' ' : time_approver_email.split('@')[0],
                HireDate: s_hireDate.hiredate,
                TSApprover: emp_TS_approver,
                WeekStartDate: weekStart,
                WeekEndDate: weekEnd,
                TotalHours: totalHrs,
                Status: approvalstatus,
                TimeSheetBlock: false,
                TaskHoliday: 'Holiday (Project Task)',
                TaskLeave: 'Leave (Project Task)',
                TaskOT: 'OT (Project Task)',
                ServiceItemHoliday: '2480',
                ServiceItemLeave: '2479',
                ServiceItemOT: '2425',
                DayLimit: 8,
                WeekLimit: 40
            };
            dataRow_TS_Body.push(JSON_TS_Body);



        }
        //For Holiday Leave OT
        var JSON_Holiday_Lines_ = {};
        var dataRow_Holiday_Lines_ = [];
        var JSON_Leave_Lines_ = {};
        var dataRow_Leave_Lines_ = [];
        var JSON_OT_Lines_ = {};
        var dataRow_OT_Lines_ = [];

        var date_T_T = weekStart;
        date_string = nlapiStringToDate(date_string);




        //END
        var indx = 0;
        var dataRow_TS_Items_Su = [];
        var dataRow_TS_Items_Su_ = [];
        var dataRow_TS_Items_Mo = [];
        var dataRow_TS_Items_Mo_ = [];
        var dataRow_TS_Items_Tu = [];
        var dataRow_TS_Items_Tu_ = [];
        var dataRow_TS_Items_We = [];
        var dataRow_TS_Items_We_ = [];
        var dataRow_TS_Items_Th = [];
        var dataRow_TS_Items_Th_ = [];
        var dataRow_TS_Items_Fr = [];
        var dataRow_TS_Items_Fr_ = [];
        var dataRow_TS_Items_Sa = [];
        var dataRow_TS_Items_Sa_ = [];

        var dataRow_OT_Items_Su = [];
        var dataRow_OT_Items_Su_ = [];
        var dataRow_OT_Items_Mo = [];
        var dataRow_OT_Items_Mo_ = [];
        var dataRow_OT_Items_Tu = [];
        var dataRow_OT_Items_Tu_ = [];
        var dataRow_OT_Items_We = [];
        var dataRow_OT_Items_We_ = [];
        var dataRow_OT_Items_Th = [];
        var dataRow_OT_Items_Th_ = [];
        var dataRow_OT_Items_Fr = [];
        var dataRow_OT_Items_Fr_ = [];
        var dataRow_OT_Items_Sa = [];
        var dataRow_OT_Items_Sa_ = [];
        var hour_day = '';
        var hours_week = '';
        var dataRow_TS_Items = [];
		var i_max_Hrs_day = '';
		var i_Monthly_cap = '';
		var i_Yearly_Cap = '';
        var emp_subsidiary = nlapiLookupField('employee', emp_id, 'subsidiary');
        date_string_Items = nlapiStringToDate(date_string_Items);
        allocationDetails_items.forEach(function(project) 
		{
            var iterate_flag = true;
            //	newTimesheet.selectNewLineItem('timeitem');
            if (project.Project && iterate_flag == true) 
			{
                proj_lookUp = nlapiLookupField('job', parseInt(project.ProjectID), ['custentity_hoursperday', 'custentity_hoursperweek', 'custentity_onsite_hours_per_day', 'custentity_onsite_hours_per_week','custentity_max_hours_per_day','custentity_monthly_cap_hours','custentity_yearly_cap_hours']);
                if (emp_subsidiary == 3 || emp_subsidiary == 19) 
				{
                    hour_day = proj_lookUp.custentity_hoursperday;
                   // if (!_logValidation(hour_day))
                        //hour_day = 8;
					
					i_max_Hrs_day = proj_lookUp.custentity_max_hours_per_day;
					i_Monthly_cap  = proj_lookUp.custentity_monthly_cap_hours;
					i_Yearly_Cap = proj_lookUp.custentity_yearly_cap_hours;

                    hours_week = proj_lookUp.custentity_hoursperweek;
                    if ((!_logValidation(hours_week)) && (!_logValidation(i_max_Hrs_day)) )
					{
						 //hours_week = 40;
					} /////  if ((!_logValidation(hours_week)) && ((!_logValidation(i_max_Hrs_day)) )

                } else 
				{
                    hour_day = proj_lookUp.custentity_onsite_hours_per_day;
                    //if (!_logValidation(hour_day))
                        //hour_day = 8;

                    hours_week = proj_lookUp.custentity_onsite_hours_per_week;
                    //if (!_logValidation(hours_week))
                        //hours_week = 40;
                }

                iterate_flag = false;
            }
            var ot_flag_ = false;
            for (var ot_ind = 0; ot_ind < OT_Applicable.length; ot_ind++) {
                if (OT_Applicable[ot_ind].OT_Applicable == 'YES' && OT_Applicable[ot_ind].Project == project.Project) {
                    ot_flag_ = true;
                }
            }

            for (var i = 0; i < 7; i++) {

                if (isNumber(project.Hours[i])) {


                    //Change in structure
                    if (parseInt(date_string_Items.getDay()) == parseInt(0)) 
					{ //Sunday
                        dataRow_TS_Items_Su = [];
                        dataRow_TS_Items_Su_.push({
                            //	'Day':sub_day,
                            'Date': nlapiDateToString(date_string_Items),
                            'Project': project.Project,
                            'Task': project.Task,
                            'ServiceItem': project.Event,
                            'Hrs': 0,
							'DayLimit': _logValidation(hour_day) ? parseFloat(project.Percent) * parseFloat(hour_day) : '',
                            'WeekLimit': _logValidation(hours_week) ? parseFloat(project.Percent) * parseFloat(hours_week) : '',
							'Max_Day_limit':  _logValidation(i_max_Hrs_day) ? parseFloat(project.Percent) * parseFloat(i_max_Hrs_day) : '',
							'Max_Month_limit' :   _logValidation(i_Monthly_cap) ? parseFloat(project.Percent) * parseFloat(i_Monthly_cap) : '',
							'Max_year_limit':  _logValidation(i_Yearly_Cap) ? parseFloat(project.Percent) * parseFloat(i_Yearly_Cap) : '',
                            'TaskPro': project.Task,
                            'ServiceItemPro': project.Event,
                            'HolidayCheck': false,
                            'LeaveCheck': false,
                            'OTHrs': 0,
                            'Days': daysList[date_string_Items.getDay()],
                            'isOtEligible': ot_flag_,
							'CustomerName':project.s_customername,//18-09-2020
							'AllocationPercentage' : (project.Percent)* 100 /// Added on 11 june 2021
                        });
                        dataRow_TS_Items_Su.push({
                            'Task': 'Project Activities (Project Task)',
                            'Date': nlapiDateToString(date_string_Items),
                            'Days': daysList[date_string_Items.getDay()],
                            'DayNo': date_string_Items.getDate(),
                            'Projects': dataRow_TS_Items_Su_

                        });
                    }
                    if (parseInt(date_string_Items.getDay()) == parseInt(1)) { //Monday
                        dataRow_TS_Items_Mo = [];
						nlapiLogExecution('debug','project.Task===',project.Task);
                        dataRow_TS_Items_Mo_.push({
                            //	'Day':sub_day,
                            'Date': nlapiDateToString(date_string_Items),
                            'Project': project.Project,
                            'Task': project.Task,
                            'ServiceItem': project.Event,
                            'Hrs': project.Hours[i],
                 							'DayLimit': _logValidation(hour_day) ? parseFloat(project.Percent) * parseFloat(hour_day) : '',
                            'WeekLimit': _logValidation(hours_week) ? parseFloat(project.Percent) * parseFloat(hours_week) : '',
							'Max_Day_limit':  _logValidation(i_max_Hrs_day) ? parseFloat(project.Percent) * parseFloat(i_max_Hrs_day) : '',
							'Max_Month_limit' :   _logValidation(i_Monthly_cap) ? parseFloat(project.Percent) * parseFloat(i_Monthly_cap) : '',
							'Max_year_limit':  _logValidation(i_Yearly_Cap) ? parseFloat(project.Percent) * parseFloat(i_Yearly_Cap) : '',
                            'TaskPro': project.Task,
                            'ServiceItemPro': project.Event,
                            'HolidayCheck': false,
                            'LeaveCheck': false,
                            'OTHrs': 0,
                            'Days': daysList[date_string_Items.getDay()],
                            'isOtEligible': ot_flag_,
							'CustomerName':project.s_customername,//18-09-2020
							'AllocationPercentage' : (project.Percent)* 100 /// Added on 11 june 2021
                        });
                        dataRow_TS_Items_Mo.push({
                            'Task': 'Project Activities (Project Task)',
                            'Date': nlapiDateToString(date_string_Items),
                            'Days': daysList[date_string_Items.getDay()],
                            'DayNo': date_string_Items.getDate(),
                            'Projects': dataRow_TS_Items_Mo_

                        });
                    }
                    if (parseInt(date_string_Items.getDay()) == parseInt(2)) { //Tuesday
                        dataRow_TS_Items_Tu = [];
                        dataRow_TS_Items_Tu_.push({
                            //	'Day':sub_day,
                            'Date': nlapiDateToString(date_string_Items),
                            'Project': project.Project,
                            'Task': project.Task,
                            'ServiceItem': project.Event,
                            'Hrs': project.Hours[i],
                      							'DayLimit': _logValidation(hour_day) ? parseFloat(project.Percent) * parseFloat(hour_day) : '',
                            'WeekLimit': _logValidation(hours_week) ? parseFloat(project.Percent) * parseFloat(hours_week) : '',
							'Max_Day_limit':  _logValidation(i_max_Hrs_day) ? parseFloat(project.Percent) * parseFloat(i_max_Hrs_day) : '',
							'Max_Month_limit' :   _logValidation(i_Monthly_cap) ? parseFloat(project.Percent) * parseFloat(i_Monthly_cap) : '',
							'Max_year_limit':  _logValidation(i_Yearly_Cap) ? parseFloat(project.Percent) * parseFloat(i_Yearly_Cap) : '',
                            'TaskPro': project.Task,
                            'ServiceItemPro': project.Event,
                            'HolidayCheck': false,
                            'LeaveCheck': false,
                            'OTHrs': 0,
                            'Days': daysList[date_string_Items.getDay()],
                            'isOtEligible': ot_flag_,
							'CustomerName':project.s_customername,//18-09-2020
							'AllocationPercentage' : (project.Percent)* 100 /// Added on 11 june 2021
                        });
                        dataRow_TS_Items_Tu.push({
                            'Task': 'Project Activities (Project Task)',
                            'Date': nlapiDateToString(date_string_Items),
                            'Days': daysList[date_string_Items.getDay()],
                            'DayNo': date_string_Items.getDate(),
                            'Projects': dataRow_TS_Items_Tu_

                        });
                    }
                    if (parseInt(date_string_Items.getDay()) == parseInt(3)) { //Wednesday
                        dataRow_TS_Items_We = [];
                        dataRow_TS_Items_We_.push({
                            //	'Day':sub_day,
                            'Date': nlapiDateToString(date_string_Items),
                            'Project': project.Project,
                            'Task': project.Task,
                            'ServiceItem': project.Event,
                            'Hrs': project.Hours[i],
                         							'DayLimit': _logValidation(hour_day) ? parseFloat(project.Percent) * parseFloat(hour_day) : '',
                            'WeekLimit': _logValidation(hours_week) ? parseFloat(project.Percent) * parseFloat(hours_week) : '',
							'Max_Day_limit':  _logValidation(i_max_Hrs_day) ? parseFloat(project.Percent) * parseFloat(i_max_Hrs_day) : '',
							'Max_Month_limit' :   _logValidation(i_Monthly_cap) ? parseFloat(project.Percent) * parseFloat(i_Monthly_cap) : '',
							'Max_year_limit':  _logValidation(i_Yearly_Cap) ? parseFloat(project.Percent) * parseFloat(i_Yearly_Cap) : '',
                            'TaskPro': project.Task,
                            'ServiceItemPro': project.Event,
                            'HolidayCheck': false,
                            'LeaveCheck': false,
                            'OTHrs': 0,
                            'Days': daysList[date_string_Items.getDay()],
                            'isOtEligible': ot_flag_,
							'CustomerName':project.s_customername,//18-09-2020
							'AllocationPercentage' : (project.Percent)* 100/// Added on 11 june 2021
                        });
                        dataRow_TS_Items_We.push({
                            'Task': 'Project Activities (Project Task)',
                            'Date': nlapiDateToString(date_string_Items),
                            'Days': daysList[date_string_Items.getDay()],
                            'DayNo': date_string_Items.getDate(),
                            'Projects': dataRow_TS_Items_We_

                        });
                    }
                    if (parseInt(date_string_Items.getDay()) == parseInt(4)) { //Thursday
                        dataRow_TS_Items_Th = [];
                        dataRow_TS_Items_Th_.push({
                            //	'Day':sub_day,
                            'Date': nlapiDateToString(date_string_Items),
                            'Project': project.Project,
                            'Task': project.Task,
                            'ServiceItem': project.Event,
                            'Hrs': project.Hours[i],
                      							'DayLimit': _logValidation(hour_day) ? parseFloat(project.Percent) * parseFloat(hour_day) : '',
                            'WeekLimit': _logValidation(hours_week) ? parseFloat(project.Percent) * parseFloat(hours_week) : '',
							'Max_Day_limit':  _logValidation(i_max_Hrs_day) ? parseFloat(project.Percent) * parseFloat(i_max_Hrs_day) : '',
							'Max_Month_limit' :   _logValidation(i_Monthly_cap) ? parseFloat(project.Percent) * parseFloat(i_Monthly_cap) : '',
							'Max_year_limit':  _logValidation(i_Yearly_Cap) ? parseFloat(project.Percent) * parseFloat(i_Yearly_Cap) : '',
                            'TaskPro': project.Task,
                            'ServiceItemPro': project.Event,
                            'HolidayCheck': false,
                            'LeaveCheck': false,
                            'OTHrs': 0,
                            'Days': daysList[date_string_Items.getDay()],
                            'isOtEligible': ot_flag_,
							'CustomerName':project.s_customername,//18-09-2020
							'AllocationPercentage' : (project.Percent)* 100 /// Added on 11 june 2021
                        });
                        dataRow_TS_Items_Th.push({
                            'Task': 'Project Activities (Project Task)',
                            'Date': nlapiDateToString(date_string_Items),
                            'Days': daysList[date_string_Items.getDay()],
                            'DayNo': date_string_Items.getDate(),
                            'Projects': dataRow_TS_Items_Th_

                        });
                    }
                    if (parseInt(date_string_Items.getDay()) == parseInt(5)) { //Friday
                        dataRow_TS_Items_Fr = [];
                        dataRow_TS_Items_Fr_.push({
                            //	'Day':sub_day,
                            'Date': nlapiDateToString(date_string_Items),
                            'Project': project.Project,
                            'Task': project.Task,
                            'ServiceItem': project.Event,
                            'Hrs': project.Hours[i],
                          							'DayLimit': _logValidation(hour_day) ? parseFloat(project.Percent) * parseFloat(hour_day) : '',
                            'WeekLimit': _logValidation(hours_week) ? parseFloat(project.Percent) * parseFloat(hours_week) : '',
							'Max_Day_limit':  _logValidation(i_max_Hrs_day) ? parseFloat(project.Percent) * parseFloat(i_max_Hrs_day) : '',
							'Max_Month_limit' :   _logValidation(i_Monthly_cap) ? parseFloat(project.Percent) * parseFloat(i_Monthly_cap) : '',
							'Max_year_limit':  _logValidation(i_Yearly_Cap) ? parseFloat(project.Percent) * parseFloat(i_Yearly_Cap) : '',
                            'TaskPro': project.Task,
                            'ServiceItemPro': project.Event,
                            'HolidayCheck': false,
                            'LeaveCheck': false,
                            'OTHrs': 0,
                            'Days': daysList[date_string_Items.getDay()],
                            'isOtEligible': ot_flag_,
							'CustomerName':project.s_customername,//18-09-2020
							'AllocationPercentage' : (project.Percent)* 100 /// Added on 11 june 2021
                        });
                        dataRow_TS_Items_Fr.push({
                            'Task': 'Project Activities (Project Task)',
                            'Date': nlapiDateToString(date_string_Items),
                            'Days': daysList[date_string_Items.getDay()],
                            'DayNo': date_string_Items.getDate(),
                            'Projects': dataRow_TS_Items_Fr_

                        });
                    }
                    if (parseInt(date_string_Items.getDay()) == parseInt(6)) { //Saturday
                        dataRow_TS_Items_Sa = [];
                        dataRow_TS_Items_Sa_.push({
                            //	'Day':sub_day,
                            'Date': nlapiDateToString(date_string_Items),
                            'Project': project.Project,
                            'Task': project.Task,
                            'ServiceItem': project.Event,
                            'Hrs': 0,
                    							'DayLimit': _logValidation(hour_day) ? parseFloat(project.Percent) * parseFloat(hour_day) : '',
                            'WeekLimit': _logValidation(hours_week) ? parseFloat(project.Percent) * parseFloat(hours_week) : '',
							'Max_Day_limit':  _logValidation(i_max_Hrs_day) ? parseFloat(project.Percent) * parseFloat(i_max_Hrs_day) : '',
							'Max_Month_limit' :   _logValidation(i_Monthly_cap) ? parseFloat(project.Percent) * parseFloat(i_Monthly_cap) : '',
							'Max_year_limit':  _logValidation(i_Yearly_Cap) ? parseFloat(project.Percent) * parseFloat(i_Yearly_Cap) : '',
                            'TaskPro': project.Task,
                            'ServiceItemPro': project.Event,
                            'HolidayCheck': false,
                            'LeaveCheck': false,
                            'OTHrs': 0,
                            'Days': daysList[date_string_Items.getDay()],
                            'isOtEligible': ot_flag_,
							'CustomerName':project.s_customername,//18-09-2020
							'AllocationPercentage' : (project.Percent)* 100  /// Added on 11 june 2021
                        });
                        dataRow_TS_Items_Sa.push({
                            'Task': 'Project Activities (Project Task)',
                            'Date': nlapiDateToString(date_string_Items),
                            'Days': daysList[date_string_Items.getDay()],
                            'DayNo': date_string_Items.getDate(),
                            'Projects': dataRow_TS_Items_Sa_

                        });
                    }




                }
                if (parseInt(i) == parseInt(6) && parseInt(indx) != parseInt(0))
                    date_string_Items = date_string;
                else
                    date_string_Items = nlapiAddDays(date_string_Items, 1);
                indx++;
                //newTimesheet.commitLineItem('timeitem');
            }
        });
        //Items
        dataRow_TS_Items.push(dataRow_TS_Items_Su);
        dataRow_TS_Items.push(dataRow_TS_Items_Mo);
        dataRow_TS_Items.push(dataRow_TS_Items_Tu);
        dataRow_TS_Items.push(dataRow_TS_Items_We);
        dataRow_TS_Items.push(dataRow_TS_Items_Th);
        dataRow_TS_Items.push(dataRow_TS_Items_Fr);
        dataRow_TS_Items.push(dataRow_TS_Items_Sa);

        //OT
        /*dataRow_OT_Lines_.push(dataRow_OT_Items_Su);
        dataRow_OT_Lines_.push(dataRow_OT_Items_Mo);
        dataRow_OT_Lines_.push(dataRow_OT_Items_Tu);
        dataRow_OT_Lines_.push(dataRow_OT_Items_We);
        dataRow_OT_Lines_.push(dataRow_OT_Items_Th);
        dataRow_OT_Lines_.push(dataRow_OT_Items_Fr);
        dataRow_OT_Lines_.push(dataRow_OT_Items_Sa);	*/

        ts_startDate = weekStart;
        var d_ts_startDate = weekStart;
        var ts_startDate_ = weekStart;
        ts_endDate = weekEnd;
        if (ts_startDate)
            ts_startDate = nlapiStringToDate(ts_startDate);
        if (ts_startDate_)
            ts_startDate_ = nlapiStringToDate(ts_startDate_);
        if (ts_endDate)
            ts_endDate = nlapiStringToDate(ts_endDate);

        //ts_status = ts_LookUp.approvalstatus;
        //}
        var filters = [];
        /*filters[0] = new nlobjSearchFilter( 'formuladate', null, 'on', startDate);
        filters[0].setFormula('{startdate}');*/
        filters[0] = new nlobjSearchFilter('internalid', 'timesheet', 'anyof', timesheetID);

        var columns = [];
        columns.push(new nlobjSearchColumn('internalid'));
        columns.push(new nlobjSearchColumn('date').setSort(false)); //
        columns.push(new nlobjSearchColumn('timesheet'));
        columns.push(new nlobjSearchColumn('employee'));
        columns.push(new nlobjSearchColumn('customer').setSort(true));
        columns.push(new nlobjSearchColumn('internalid', 'item'));
        columns.push(new nlobjSearchColumn('casetaskevent'));
        columns.push(new nlobjSearchColumn('type'));
        columns.push(new nlobjSearchColumn('hours'));
		columns.push(new nlobjSearchColumn('customer','job'));//18-08-2020

        var timeTrackSearch = searchRecord('timebill', null, filters, columns); //Searched record type by praveena on 18-03-2020


			
        if (timeTrackSearch) 
		{



            var JSON_TS_Lines = {};
            var dataRow_TS_Lines = [];
            var JSON_Holiday_Lines = {};
            var dataRow_Holiday_Lines = [];
            var JSON_Leave_Lines = {};
            var dataRow_Leave_Lines = [];
            var JSON_OT_Lines = {};
            var dataRow_OT_Lines = [];

            var dataRow_OverAll_TS = [];
            var JSON_ALL = {};
            var dataRow_ALL = [];
            var date_list = [];
            var item_list = [];
            var task_list = [];
            var dataRow_TS_Sun = [];
            var dataRow_TS_Sun_ = [];
            var dataRow_TS_Mon = [];
            var dataRow_TS_Mon_ = [];
            var dataRow_TS_Tue = [];
            var dataRow_TS_Tue_ = [];
            var dataRow_TS_Wed = [];
            var dataRow_TS_Wed_ = [];
            var dataRow_TS_Thur = [];
            var dataRow_TS_Thur_ = [];
            var dataRow_TS_Fri = [];
            var dataRow_TS_Fri_ = [];
            var dataRow_TS_Sat = [];
            var dataRow_TS_Sat_ = [];
            for (var inx = 0; inx < timeTrackSearch.length; inx++) {
                var task = '';
                var item_ = '';
                var date_T_ = '';
                var date_T = '';
                var customer = '';
                var hours = '';
                var i_customer = '';
                var proj_lookUp = '';
                var hour_day = '';
                var hours_week = '';
				var s_customername='';
				
                task = timeTrackSearch[inx].getText('casetaskevent');
				nlapiLogExecution('DEBUG','task',task);//VNA19052020
                item_ = timeTrackSearch[inx].getValue('internalid', 'item');
				nlapiLogExecution('DEBUG','item_',item_);//VNA19052020
                date_T_ = timeTrackSearch[inx].getValue('date');
               // var day = timeTrackSearch[inx].getValue('day');
                date_T = nlapiStringToDate(date_T_);
               var day_ = date_T.getDay();
                var getTodayDate = date_T.getDate();
                var s_day = daysList[day_];
                customer = timeTrackSearch[inx].getText('customer');
                i_customer = timeTrackSearch[inx].getValue('customer');
                var emp = timeTrackSearch[inx].getValue('employee');
                var emp_subsidiary = nlapiLookupField('employee', emp, 'subsidiary');
				s_customername=timeTrackSearch[inx].getText('customer','job');//18-08-2020
				
					var i_max_Hrs_day = '';
					var i_Monthly_cap = '';
					var i_Yearly_Cap = '';
				
				nlapiLogExecution('DEBUG','item_',item_);//VNA19052020
				nlapiLogExecution('DEBUG','date_T',date_T);//VNA19052020
				nlapiLogExecution('DEBUG','day_',day_);//VNA19052020
				
                if (i_customer) {
                    proj_lookUp = nlapiLookupField('job', i_customer,['custentity_hoursperday', 'custentity_hoursperweek', 'custentity_onsite_hours_per_day', 'custentity_onsite_hours_per_week','custentity_max_hours_per_day','custentity_monthly_cap_hours','custentity_yearly_cap_hours'] );
                    if (emp_subsidiary == 3 || emp_subsidiary == 19)
						{
                        hour_day = proj_lookUp.custentity_hoursperday;
                        hours_week = proj_lookUp.custentity_hoursperweek;
						
					i_max_Hrs_day = proj_lookUp.custentity_max_hours_per_day;
					i_Monthly_cap  = proj_lookUp.custentity_monthly_cap_hours;
					i_Yearly_Cap = proj_lookUp.custentity_yearly_cap_hours;
                    if ((!_logValidation(hours_week)) && (!_logValidation(i_max_Hrs_day)) )
					{
						// hours_week = 40;
					} /////  if ((!_logValidation(hours_week)) && ((!_logValidation(i_max_Hrs_day)) )
						
						
                    } else {
                        hour_day = proj_lookUp.custentity_onsite_hours_per_day;
                        hours_week = proj_lookUp.custentity_onsite_hours_per_week;
                    }
                }
                hours = timeTrackSearch[inx].getValue('hours');
nlapiLogExecution('DEBUG','day_',day_);
nlapiLogExecution('DEBUG','hours',hours);              
              if(!hour_day)
				 // hour_day = parseInt(8);
			  if(!hours_week)
				 // hours_week = parseInt(40);
              
                //Change in structure
                if (parseInt(day_) == parseInt(0)) { //Sunday
                    dataRow_TS_Sun = [];
                    dataRow_TS_Sun_.push({
                        //	'Day':sub_day,
                        'Date': date_T_,
                        'Project': customer,
                        'Task': task + ' (Project Task)',
                        'ServiceItem': item_,
                        'Hrs': 0,
                        'DayLimit': hour_day,
                        'WeekLimit': hours_week,
						'Max_Day_limit': i_max_Hrs_day,
						'Max_Month_limit' : i_Monthly_cap,
						'Max_year_limit':i_Yearly_Cap,
                        'TaskPro': task + ' (Project Task)',
                        'ServiceItemPro': item_,
                        'HolidayCheck': false,
                        'LeaveCheck': false,
                        'OTHrs': 0,
                        'Days': s_day,
                        'isOtEligible': false,
						'CustomerName':s_customername//18-08-2020
                    });
                    dataRow_TS_Sun.push({
                        'Task': 'Project Activities (Project Task)',
                        'Date': date_T_,
                        'Days': s_day,
                        'DayNo': getTodayDate,
                        'Projects': dataRow_TS_Sun_

                    });
                }
                if (parseInt(day_) == parseInt(1)) { //Monday ///// Task changes here on 15 march for Sandbox
                    dataRow_TS_Mon = [];
                    dataRow_TS_Mon_.push({
                        //	'Day':sub_day,
                        'Date': date_T_,
                        'Project': customer,
                        'Task': task, //+ ' (Project Task)',
                        'ServiceItem': item_,
                        'Hrs': hours,
                        'DayLimit': hour_day,
                        'WeekLimit': hours_week,
							'Max_Day_limit': i_max_Hrs_day,
							'Max_Month_limit' : i_Monthly_cap,
							'Max_year_limit':i_Yearly_Cap,
                        'TaskPro': task ,//+ ' (Project Task)',
                        'ServiceItemPro': item_,
                        'HolidayCheck': false,
                        'LeaveCheck': false,
                        'OTHrs': 0,
                        'Days': s_day,
                        'isOtEligible': false,
						'CustomerName':s_customername//18-08-2020
                    });
                    dataRow_TS_Mon.push({
                        'Task': 'Project Activities (Project Task)',
                        'Date': date_T_,
                        'Days': s_day,
                        'DayNo': getTodayDate,
                        'Projects': dataRow_TS_Mon_

                    });
                }
                if (parseInt(day_) == parseInt(2)) { //Tuesday
                    dataRow_TS_Tue = [];
                    dataRow_TS_Tue_.push({
                        //	'Day':sub_day,
                        'Date': date_T_,
                        'Project': customer,
                        'Task': task , //+ ' (Project Task)',
                        'ServiceItem': item_,
                        'Hrs': hours,
                        'DayLimit': hour_day,
                        'WeekLimit': hours_week,
							'Max_Day_limit': i_max_Hrs_day,
							'Max_Month_limit' : i_Monthly_cap,
							'Max_year_limit':i_Yearly_Cap,
                        'TaskPro': task ,//+ ' (Project Task)',
                        'ServiceItemPro': item_,
                        'HolidayCheck': false,
                        'LeaveCheck': false,
                        'OTHrs': 0,
                        'Days': s_day,
                        'isOtEligible': false,
						'CustomerName':s_customername//18-08-2020
                    });
                    dataRow_TS_Tue.push({
                        'Task': 'Project Activities (Project Task)',
                        'Date': date_T_,
                        'Days': s_day,
                        'DayNo': getTodayDate,
                        'Projects': dataRow_TS_Tue_

                    });
                }
                if (parseInt(day_) == parseInt(3)) { //Wednesday
                    dataRow_TS_Wed = [];
                    dataRow_TS_Wed_.push({
                        //	'Day':sub_day,
                        'Date': date_T_,
                        'Project': customer,
                        'Task': task , //+ ' (Project Task)',
                        'ServiceItem': item_,
                        'Hrs': hours,
                        'DayLimit': hour_day,
                        'WeekLimit': hours_week,
							'Max_Day_limit': i_max_Hrs_day,
							'Max_Month_limit' : i_Monthly_cap,
							'Max_year_limit':i_Yearly_Cap,
                        'TaskPro': task, // + ' (Project Task)',
                        'ServiceItemPro': item_,
                        'HolidayCheck': false,
                        'LeaveCheck': false,
                        'OTHrs': 0,
                        'Days': s_day,
                        'isOtEligible': false,
						'CustomerName':s_customername//18-08-2020
                    });
                    dataRow_TS_Wed.push({
                        'Task': 'Project Activities (Project Task)',
                        'Date': date_T_,
                        'Days': s_day,
                        'DayNo': getTodayDate,
                        'Projects': dataRow_TS_Wed_

                    });
                }
                if (parseInt(day_) == parseInt(4)) { //Thursday
                    dataRow_TS_Thur = [];
                    dataRow_TS_Thur_.push({
                        //	'Day':sub_day,
                        'Date': date_T_,
                        'Project': customer,
                        'Task': task , //+ ' (Project Task)',
                        'ServiceItem': item_,
                        'Hrs': hours,
                        'DayLimit': hour_day,
                        'WeekLimit': hours_week,
							'Max_Day_limit': i_max_Hrs_day,
							'Max_Month_limit' : i_Monthly_cap,
							'Max_year_limit':i_Yearly_Cap,
                        'TaskPro': task ,//+ ' (Project Task)',
                        'ServiceItemPro': item_,
                        'HolidayCheck': false,
                        'LeaveCheck': false,
                        'OTHrs': 0,
                        'Days': s_day,
                        'isOtEligible': false,
						'CustomerName':s_customername//18-08-2020
                    });
                    dataRow_TS_Thur.push({
                        'Task': 'Project Activities (Project Task)',
                        'Date': date_T_,
                        'Days': s_day,
                        'DayNo': getTodayDate,
                        'Projects': dataRow_TS_Thur_

                    });
                }
                if (parseInt(day_) == parseInt(5)) { //Friday
                    dataRow_TS_Fri = [];
                    dataRow_TS_Fri_.push({
                        //	'Day':sub_day,
                        'Date': date_T_,
                        'Project': customer,
                        'Task': task , //+ ' (Project Task)',
                        'ServiceItem': item_,
                        'Hrs': hours,
                        'DayLimit': hour_day,
                        'WeekLimit': hours_week,
							'Max_Day_limit': i_max_Hrs_day,
							'Max_Month_limit' : i_Monthly_cap,
							'Max_year_limit':i_Yearly_Cap,
                        'TaskPro': task,// + ' (Project Task)',
                        'ServiceItemPro': item_,
                        'HolidayCheck': false,
                        'LeaveCheck': false,
                        'OTHrs': 0,
                        'Days': s_day,
                        'isOtEligible': false,
						'CustomerName':s_customername//18-08-2020
                    });
                    dataRow_TS_Fri.push({
                        'Task': 'Project Activities (Project Task)',
                        'Date': date_T_,
                        'Days': s_day,
                        'DayNo': getTodayDate,
                        'Projects': dataRow_TS_Fri_

                    });
                }
                if (parseInt(day_) == parseInt(6)) { //Saturday
                    dataRow_TS_Sat = [];
                    dataRow_TS_Sat_.push({
                        //		'Day':sub_day,
                        'Date': date_T_,
                        'Project': customer,
                        'Task': task + ' (Project Task)',
                        'ServiceItem': item_,
                        'Hrs': 0,
                        'DayLimit': hour_day,
                        'WeekLimit': hours_week,
							'Max_Day_limit': i_max_Hrs_day,
							'Max_Month_limit' : i_Monthly_cap,
							'Max_year_limit':i_Yearly_Cap,
                        'TaskPro': task + ' (Project Task)',
                        'ServiceItemPro': item_,
                        'HolidayCheck': false,
                        'LeaveCheck': false,
                        'OTHrs': 0,
                        'Days': s_day,
                        'isOtEligible': false,
						'CustomerName':s_customername//18-08-2020
                    });
                    dataRow_TS_Sat.push({
                        'Task': 'Project Activities (Project Task)',
                        'Date': date_T_,
                        'Days': s_day,
                        'DayNo': getTodayDate,
                        'Projects': dataRow_TS_Sat_

                    });
                }
            }
        } //End of Search

        //Concat all TS
        dataRow_TS_Lines.push(dataRow_TS_Sun);
        dataRow_TS_Lines.push(dataRow_TS_Mon);
        dataRow_TS_Lines.push(dataRow_TS_Tue);
        dataRow_TS_Lines.push(dataRow_TS_Wed);
        dataRow_TS_Lines.push(dataRow_TS_Thur);
        dataRow_TS_Lines.push(dataRow_TS_Fri);
        dataRow_TS_Lines.push(dataRow_TS_Sat);


        JSON_ALL = {
            ServiceItem: dataRow_TS_Lines
            //Holiday: dataRow_Holiday_Lines_,
            //	Leave: dataRow_Leave_Lines_,

            //Items: dataRow_TS_Items

        };
        dataRow_ALL.push(JSON_ALL);


        var JSON_Total = {};
        var dataRow_Total = [];

        JSON_Total = {
            Body: dataRow_TS_Body,
            Lines: dataRow_ALL,
            //	OT: dataRow_OT_Lines_,
            Items: dataRow_TS_Items

        };
        dataRow_Total.push(JSON_Total);
        var JSON = {};
        var dataRow = [];

        //Customer Holiday List
        var customer_list = [];
        var holiday_list = [];
        //GET OT Flag From Allocation startDate, endDate,employeeId

        allocationDetails_items.forEach(function(project) {
            //var project_internal_id = project.Project;//commented by praveena as its giving name instead of id
            var project_internal_id = project.ProjectID; //Added by praveena due
            nlapiLogExecution('debug', 'project_internal_id', project_internal_id);
            var project_holiday = nlapiLookupField('job', project_internal_id,
                'custentityproject_holiday');
            if (parseInt(project_holiday) != parseInt(1)) {
                customer_list = get_holidays(startDate,
                    endDate, project.ProjectID, employeeId, project.Project,project.s_customername);

            } else {
                //Get Holiday List in Week
                holiday_list = get_company_holidays(startDate, endDate, employeeId, project.Project,project.s_customername);
            }
        });

        JSON = {
            TimeSheetDetails: dataRow_Total,
            HolidayList: holiday_list,
            CustomerHoliday: customer_list
        };

        dataRow.push(JSON);
        //}
        return dataRow;
    } catch (err) {
        nlapiLogExecution('error', 'Restlet Main Function', err);
        throw err;
    }

}


function get_holidays(start_date, end_date, project, employee, Project_ID,s_customername) 
{

    var customer_val = nlapiLookupField('job', project,
        'customer');

    var emp_subsidiary = nlapiLookupField('employee', employee, 'subsidiary');
    var cust_holiday_list = [];

    //start_date = nlapiDateToString(start_date, 'date');
    //end_date = nlapiDateToString(end_date, 'date');

    nlapiLogExecution('debug', 'start_date', start_date);
    nlapiLogExecution('debug', 'end_date', end_date);
    nlapiLogExecution('debug', 'emp_subsidiary', emp_subsidiary);
    nlapiLogExecution('debug', 'customer_val', customer_val);

    var search_customer_holiday = nlapiSearchRecord(
        'customrecordcustomerholiday', 'customsearch_customer_holiday', [
            new nlobjSearchFilter('custrecordholidaydate', null,
                'within', start_date, end_date),
            new nlobjSearchFilter('custrecordcustomersubsidiary', null,
                'anyof', emp_subsidiary),
            new nlobjSearchFilter('custrecord13', null, 'anyof',
                customer_val)
        ], [new nlobjSearchColumn(
            'custrecordholidaydate', null, 'group')]);

    if (search_customer_holiday) 
	{

        for (var i = 0; i < search_customer_holiday.length; i++) {
            cust_holiday_list.push({
                'ProjectID': Project_ID,
                'Date': search_customer_holiday[i].getValue(
                    'custrecordholidaydate', null, 'group'),
					'CustomerName':s_customername

            });
        }
    }
	nlapiLogExecution('debug', 'cust_holiday_list', JSON.stringify(cust_holiday_list));
	return cust_holiday_list;
}


function validateTS(employeeId, startDate) {
    try {
        var filters = [];
        filters[0] = new nlobjSearchFilter('formuladate', null, 'on', startDate);
        filters[0].setFormula('{startdate}');
        filters[1] = new nlobjSearchFilter('employee', null, 'anyof', employeeId);

        var timeTrackSearch = searchRecord('timesheet', null, filters,
            [
                new nlobjSearchColumn('internalid'),
				new nlobjSearchColumn('totalhours')
            ]);
        if (_logValidation(timeTrackSearch))
            nlapiLogExecution('debug', 'timeTrackSearch', timeTrackSearch.length);
        return timeTrackSearch;
    } catch (err) {
        nlapiLogExecution('error', 'Restlet TS Function', err);
        throw err;
    }
}

function allocationDetails(dataIn, employeeId, startDate, endDate) {

    try {
        var date_string = startDate;
        //var OT_Applicable = '';
        var OT_Applicable = [];
        var time_approver_email = '';
        // get active allocations, hours ,etc.
        var allocationDetails = getActiveAllocations(startDate, endDate,
            employeeId);

        var allocationDetails_items = getActiveAllocations_Items(startDate, endDate,
            employeeId);

        //Get Holiday List in Week
        //var holiday_list = get_company_holidays(startDate,endDate,employeeId);
        //Check if timesheet already Exist

        // create an open timesheet
        var timesheetId = autoGenerateTimesheet(startDate, employeeId,
            allocationDetails);
        if (timesheetId) 
		{
            //Load to Get TS Data
            var load_TS = nlapiLoadRecord('timesheet', timesheetId);
            var emp = load_TS.getFieldText('employee');
            var i_emp = load_TS.getFieldValue('employee');
            var emp_TS_approver = load_TS.getFieldText('custrecord_ts_time_approver');
            var weekStart = load_TS.getFieldValue('startdate');
            var weekEnd = load_TS.getFieldValue('enddate');
            var totalHrs = load_TS.getFieldValue('totalhours');
            var approvalstatus = load_TS.getFieldText('approvalstatus');
            //GET OT Flag From Allocation
            allocationDetails_items.forEach(function(project) 
			{
                OT_Applicable.push({
                    'OT_Applicable': project.OT,
                    'Project': project.Project
                });
                //	OT_Applicable = project.OT;
                //Project
            });
            var s_hireDate = nlapiLookupField('employee', parseInt(i_emp), ['hiredate', 'timeapprover']);
            if (s_hireDate.timeapprover)
                time_approver_email = nlapiLookupField('employee', s_hireDate.timeapprover, 'email');

            var JSON_TS_Body = {};
            var dataRow_TS_Body = [];

            JSON_TS_Body = {
                TimeSheetID: timesheetId,
                Employee: emp,
                timesheetApprover: _logValidation(time_approver_email) == false ? ' ' : time_approver_email.split('@')[0],
                HireDate: s_hireDate.hiredate,
                TSApprover: emp_TS_approver,
                WeekStartDate: weekStart,
                WeekEndDate: weekEnd,
                TotalHours: totalHrs,
                Status: approvalstatus,
                TimeSheetBlock: false,
                TaskHoliday: 'Holiday (Project Task)',
                TaskLeave: 'Leave (Project Task)',
                TaskOT: 'OT (Project Task)',
                ServiceItemHoliday: '2480',
                ServiceItemLeave: '2479',
                ServiceItemOT: '2425',
                DayLimit: 8,
                WeekLimit: 40
            };
            dataRow_TS_Body.push(JSON_TS_Body);


            //For Holiday Leave OT
            var JSON_Holiday_Lines_ = {};
            var dataRow_Holiday_Lines_ = [];
            var JSON_Leave_Lines_ = {};
            var dataRow_Leave_Lines_ = [];
            var JSON_OT_Lines_ = {};
            var dataRow_OT_Lines_ = [];
            var JSON_Items = {};
            var dataRow_Items = [];
            var date_T_T = weekStart;
            //var date_string = nlapiStringToDate(date_string);
            var date_string_Items = startDate;

            date_string = nlapiStringToDate(date_string);

            //END
            var indx = 0;
            var dataRow_TS_Items_Su = [];
            var dataRow_TS_Items_Su_ = [];
            var dataRow_TS_Items_Mo = [];
            var dataRow_TS_Items_Mo_ = [];
            var dataRow_TS_Items_Tu = [];
            var dataRow_TS_Items_Tu_ = [];
            var dataRow_TS_Items_We = [];
            var dataRow_TS_Items_We_ = [];
            var dataRow_TS_Items_Th = [];
            var dataRow_TS_Items_Th_ = [];
            var dataRow_TS_Items_Fr = [];
            var dataRow_TS_Items_Fr_ = [];
            var dataRow_TS_Items_Sa = [];
            var dataRow_TS_Items_Sa_ = [];

            var dataRow_OT_Items_Su_alloc = [];
            var dataRow_OT_Items_Su_alloc_ = [];
            var dataRow_OT_Items_Mo_alloc = [];
            var dataRow_OT_Items_Mo_alloc_ = [];
            var dataRow_OT_Items_Tu_alloc = [];
            var dataRow_OT_Items_Tu_alloc_ = [];
            var dataRow_OT_Items_We_alloc = [];
            var dataRow_OT_Items_We_alloc_ = [];
            var dataRow_OT_Items_Th_alloc = [];
            var dataRow_OT_Items_Th_alloc_ = [];
            var dataRow_OT_Items_Fr_alloc = [];
            var dataRow_OT_Items_Fr_alloc_ = [];
            var dataRow_OT_Items_Sa_alloc = [];
            var dataRow_OT_Items_Sa_alloc_ = [];
            var hour_day = '';
            var hours_week = '';
				var i_max_Hrs_day = '';
		var i_Monthly_cap = '';
		var i_Yearly_Cap = '';
			
			
			
			
            var dataRow_TS_Items = [];
            date_string_Items = nlapiStringToDate(date_string_Items);
            allocationDetails_items.forEach(function(project) 
			{
                var iterate_flag = true;
                //	newTimesheet.selectNewLineItem('timeitem');
                if (project.Project && iterate_flag == true) 
				{

                    proj_lookUp = nlapiLookupField('job', parseInt(project.ProjectID), ['custentity_hoursperday', 'custentity_hoursperweek', 'custentity_onsite_hours_per_day', 'custentity_onsite_hours_per_week']);
                    var emp_subsidiary = nlapiLookupField('employee', parseInt(i_emp), 'subsidiary');
                    if (emp_subsidiary == 3) 
					{
						
						
						i_max_Hrs_day = proj_lookUp.custentity_max_hours_per_day;
						i_Monthly_cap  = proj_lookUp.custentity_monthly_cap_hours;
						i_Yearly_Cap = proj_lookUp.custentity_yearly_cap_hours;
					
                        hour_day = proj_lookUp.custentity_hoursperday;
						nlapiLogExecution('audit','before hour_day==',hour_day);
                       /// if (!_logValidation(hour_day))
                           // hour_day = 8; //// changed on 19 may 

                        hours_week = proj_lookUp.custentity_hoursperweek;
                         if ((!_logValidation(hours_week)) && (!_logValidation(i_max_Hrs_day)) )
						{
							//hours_week = 40;
						} /////  if ((!_logValidation(hours_week)) && ((!_logValidation(i_max_Hrs_day)) )
                    } else 
					{
                        hour_day = proj_lookUp.custentity_onsite_hours_per_day;
						nlapiLogExecution('audit','before hour_day==',hour_day);
                       // if (!_logValidation(hour_day))
                           // hour_day = 8; /// changed on 19 may 

                        hours_week = proj_lookUp.custentity_onsite_hours_per_week;
                        //if (!_logValidation(hours_week))
                           // hours_week = 40;
                    }

                    iterate_flag = false;
                }
                var ot_flag_ = false;
                for (var ot_ind = 0; ot_ind < OT_Applicable.length; ot_ind++) {
                    if (OT_Applicable[ot_ind].OT_Applicable == 'YES' && OT_Applicable[ot_ind].Project == project.Project) {
                        ot_flag_ = true;
                    }
                }
				nlapiLogExecution('audit','project.ProjectID==',project.ProjectID);
				
				nlapiLogExecution('audit','hour_day==',hour_day);
                for (var i = 0; i < 7; i++) 
				{

                    if (isNumber(project.Hours[i])) 
					{


                        //Change in structure
                        if (parseInt(date_string_Items.getDay()) == parseInt(0)) { //Sunday
                            dataRow_TS_Items_Su = [];
                            dataRow_TS_Items_Su_.push({
                                //	'Day':sub_day,
                                'Date': nlapiDateToString(date_string_Items),
                                'Project': project.Project,
                                'Task': project.Task,
                                'ServiceItem': project.Event,
                                'Hrs': 0,
                          							'DayLimit': _logValidation(hour_day) ? parseFloat(project.Percent) * parseFloat(hour_day) : '',
                            'WeekLimit': _logValidation(hours_week) ? parseFloat(project.Percent) * parseFloat(hours_week) : '',
							'Max_Day_limit':  _logValidation(i_max_Hrs_day) ? parseFloat(project.Percent) * parseFloat(i_max_Hrs_day) : '',
							'Max_Month_limit' :   _logValidation(i_Monthly_cap) ? parseFloat(project.Percent) * parseFloat(i_Monthly_cap) : '',
							'Max_year_limit':  _logValidation(i_Yearly_Cap) ? parseFloat(project.Percent) * parseFloat(i_Yearly_Cap) : '',
                                'TaskPro': project.Task,
                                'ServiceItemPro': project.Event,
                                'HolidayCheck': false,
                                'LeaveCheck': false,
                                'OTHrs': 0,
                                'Days': daysList[date_string_Items.getDay()],
                                'isOtEligible': ot_flag_,
								'CustomerName':project.s_customername//18-08-2020
                            });
                            dataRow_TS_Items_Su.push({
                                'Task': 'Project Activities (Project Task)',
                                'Date': nlapiDateToString(date_string_Items),
                                'Days': daysList[date_string_Items.getDay()],
                                'DayNo': date_string_Items.getDate(),
                                'Projects': dataRow_TS_Items_Su_

                            });
                        }
                        if (parseInt(date_string_Items.getDay()) == parseInt(1)) { //Monday
                            dataRow_TS_Items_Mo = [];
                            dataRow_TS_Items_Mo_.push({
                                //	'Day':sub_day,
                                'Date': nlapiDateToString(date_string_Items),
                                'Project': project.Project,
                                'Task': project.Task,
                                'ServiceItem': project.Event,
                                'Hrs': project.Hours[i],
                              							'DayLimit': _logValidation(hour_day) ? parseFloat(project.Percent) * parseFloat(hour_day) : '',
                            'WeekLimit': _logValidation(hours_week) ? parseFloat(project.Percent) * parseFloat(hours_week) : '',
							'Max_Day_limit':  _logValidation(i_max_Hrs_day) ? parseFloat(project.Percent) * parseFloat(i_max_Hrs_day) : '',
							'Max_Month_limit' :   _logValidation(i_Monthly_cap) ? parseFloat(project.Percent) * parseFloat(i_Monthly_cap) : '',
							'Max_year_limit':  _logValidation(i_Yearly_Cap) ? parseFloat(project.Percent) * parseFloat(i_Yearly_Cap) : '',
                                'TaskPro': project.Task,
                                'ServiceItemPro': project.Event,
                                'HolidayCheck': false,
                                'LeaveCheck': false,
                                'OTHrs': 0,
                                'Days': daysList[date_string_Items.getDay()],
                                'isOtEligible': ot_flag_,
								'CustomerName':project.s_customername//18-08-2020
                            });
                            dataRow_TS_Items_Mo.push({
                                'Task': 'Project Activities (Project Task)',
                                'Date': nlapiDateToString(date_string_Items),
                                'Days': daysList[date_string_Items.getDay()],
                                'DayNo': date_string_Items.getDate(),
                                'Projects': dataRow_TS_Items_Mo_

                            });
                        }
                        if (parseInt(date_string_Items.getDay()) == parseInt(2)) { //Tuesday
                            dataRow_TS_Items_Tu = [];
                            dataRow_TS_Items_Tu_.push({
                                //	'Day':sub_day,
                                'Date': nlapiDateToString(date_string_Items),
                                'Project': project.Project,
                                'Task': project.Task,
                                'ServiceItem': project.Event,
                                'Hrs': project.Hours[i],
                     							'DayLimit': _logValidation(hour_day) ? parseFloat(project.Percent) * parseFloat(hour_day) : '',
                            'WeekLimit': _logValidation(hours_week) ? parseFloat(project.Percent) * parseFloat(hours_week) : '',
							'Max_Day_limit':  _logValidation(i_max_Hrs_day) ? parseFloat(project.Percent) * parseFloat(i_max_Hrs_day) : '',
							'Max_Month_limit' :   _logValidation(i_Monthly_cap) ? parseFloat(project.Percent) * parseFloat(i_Monthly_cap) : '',
							'Max_year_limit':  _logValidation(i_Yearly_Cap) ? parseFloat(project.Percent) * parseFloat(i_Yearly_Cap) : '',
								'Max_year_limit':i_Yearly_Cap,
                                'TaskPro': project.Task,
                                'ServiceItemPro': project.Event,
                                'HolidayCheck': false,
                                'LeaveCheck': false,
                                'OTHrs': 0,
                                'Days': daysList[date_string_Items.getDay()],
                                'isOtEligible': ot_flag_,
								'CustomerName':project.s_customername//18-08-2020
                            });
                            dataRow_TS_Items_Tu.push({
                                'Task': 'Project Activities (Project Task)',
                                'Date': nlapiDateToString(date_string_Items),
                                'Days': daysList[date_string_Items.getDay()],
                                'DayNo': date_string_Items.getDate(),
                                'Projects': dataRow_TS_Items_Tu_

                            });
                        }
                        if (parseInt(date_string_Items.getDay()) == parseInt(3)) { //Wednesday
                            dataRow_TS_Items_We = [];
                            dataRow_TS_Items_We_.push({
                                //	'Day':sub_day,
                                'Date': nlapiDateToString(date_string_Items),
                                'Project': project.Project,
                                'Task': project.Task,
                                'ServiceItem': project.Event,
                                'Hrs': project.Hours[i],
												'DayLimit': _logValidation(hour_day) ? parseFloat(project.Percent) * parseFloat(hour_day) : '',
                            'WeekLimit': _logValidation(hours_week) ? parseFloat(project.Percent) * parseFloat(hours_week) : '',
							'Max_Day_limit':  _logValidation(i_max_Hrs_day) ? parseFloat(project.Percent) * parseFloat(i_max_Hrs_day) : '',
							'Max_Month_limit' :   _logValidation(i_Monthly_cap) ? parseFloat(project.Percent) * parseFloat(i_Monthly_cap) : '',
							'Max_year_limit':  _logValidation(i_Yearly_Cap) ? parseFloat(project.Percent) * parseFloat(i_Yearly_Cap) : '',
								'Max_year_limit':i_Yearly_Cap,
                                'TaskPro': project.Task,
                                'ServiceItemPro': project.Event,
                                'HolidayCheck': false,
                                'LeaveCheck': false,
                                'OTHrs': 0,
                                'Days': daysList[date_string_Items.getDay()],
                                'isOtEligible': ot_flag_,
								'CustomerName':project.s_customername//18-08-2020
                            });
                            dataRow_TS_Items_We.push({
                                'Task': 'Project Activities (Project Task)',
                                'Date': nlapiDateToString(date_string_Items),
                                'Days': daysList[date_string_Items.getDay()],
                                'DayNo': date_string_Items.getDate(),
                                'Projects': dataRow_TS_Items_We_

                            });
                        }
                        if (parseInt(date_string_Items.getDay()) == parseInt(4)) { //Thursday
                            dataRow_TS_Items_Th = [];
                            dataRow_TS_Items_Th_.push({
                                //	'Day':sub_day,
                                'Date': nlapiDateToString(date_string_Items),
                                'Project': project.Project,
                                'Task': project.Task,
                                'ServiceItem': project.Event,
                                'Hrs': project.Hours[i],
													'DayLimit': _logValidation(hour_day) ? parseFloat(project.Percent) * parseFloat(hour_day) : '',
                            'WeekLimit': _logValidation(hours_week) ? parseFloat(project.Percent) * parseFloat(hours_week) : '',
							'Max_Day_limit':  _logValidation(i_max_Hrs_day) ? parseFloat(project.Percent) * parseFloat(i_max_Hrs_day) : '',
							'Max_Month_limit' :   _logValidation(i_Monthly_cap) ? parseFloat(project.Percent) * parseFloat(i_Monthly_cap) : '',
							'Max_year_limit':  _logValidation(i_Yearly_Cap) ? parseFloat(project.Percent) * parseFloat(i_Yearly_Cap) : '',
								'Max_year_limit':i_Yearly_Cap,
                                'TaskPro': project.Task,
                                'ServiceItemPro': project.Event,
                                'HolidayCheck': false,
                                'LeaveCheck': false,
                                'OTHrs': 0,
                                'Days': daysList[date_string_Items.getDay()],
                                'isOtEligible': ot_flag_,
								'CustomerName':project.s_customername//18-08-2020
                            });
                            dataRow_TS_Items_Th.push({
                                'Task': 'Project Activities (Project Task)',
                                'Date': nlapiDateToString(date_string_Items),
                                'Days': daysList[date_string_Items.getDay()],
                                'DayNo': date_string_Items.getDate(),
                                'Projects': dataRow_TS_Items_Th_

                            });
                        }
                        if (parseInt(date_string_Items.getDay()) == parseInt(5)) { //Friday
                            dataRow_TS_Items_Fr = [];
                            dataRow_TS_Items_Fr_.push({
                                //	'Day':sub_day,
                                'Date': nlapiDateToString(date_string_Items),
                                'Project': project.Project,
                                'Task': project.Task,
                                'ServiceItem': project.Event,
                                'Hrs': project.Hours[i],
														'DayLimit': _logValidation(hour_day) ? parseFloat(project.Percent) * parseFloat(hour_day) : '',
                            'WeekLimit': _logValidation(hours_week) ? parseFloat(project.Percent) * parseFloat(hours_week) : '',
							'Max_Day_limit':  _logValidation(i_max_Hrs_day) ? parseFloat(project.Percent) * parseFloat(i_max_Hrs_day) : '',
							'Max_Month_limit' :   _logValidation(i_Monthly_cap) ? parseFloat(project.Percent) * parseFloat(i_Monthly_cap) : '',
							'Max_year_limit':  _logValidation(i_Yearly_Cap) ? parseFloat(project.Percent) * parseFloat(i_Yearly_Cap) : '',
                                'TaskPro': project.Task,
                                'ServiceItemPro': project.Event,
                                'HolidayCheck': false,
                                'LeaveCheck': false,
                                'OTHrs': 0,
                                'Days': daysList[date_string_Items.getDay()],
                                'isOtEligible': ot_flag_,
								'CustomerName':project.s_customername//18-08-2020
                            });
                            dataRow_TS_Items_Fr.push({
                                'Task': 'Project Activities (Project Task)',
                                'Date': nlapiDateToString(date_string_Items),
                                'Days': daysList[date_string_Items.getDay()],
                                'DayNo': date_string_Items.getDate(),
                                'Projects': dataRow_TS_Items_Fr_

                            });
                        }
                        if (parseInt(date_string_Items.getDay()) == parseInt(6)) { //Saturday
                            dataRow_TS_Items_Sa = [];
                            dataRow_TS_Items_Sa_.push({
                                //	'Day':sub_day,
                                'Date': nlapiDateToString(date_string_Items),
                                'Project': project.Project,
                                'Task': project.Task,
                                'ServiceItem': project.Event,
                                'Hrs': 0,
													'DayLimit': _logValidation(hour_day) ? parseFloat(project.Percent) * parseFloat(hour_day) : '',
                            'WeekLimit': _logValidation(hours_week) ? parseFloat(project.Percent) * parseFloat(hours_week) : '',
							'Max_Day_limit':  _logValidation(i_max_Hrs_day) ? parseFloat(project.Percent) * parseFloat(i_max_Hrs_day) : '',
							'Max_Month_limit' :   _logValidation(i_Monthly_cap) ? parseFloat(project.Percent) * parseFloat(i_Monthly_cap) : '',
							'Max_year_limit':  _logValidation(i_Yearly_Cap) ? parseFloat(project.Percent) * parseFloat(i_Yearly_Cap) : '',
                                'TaskPro': project.Task,
                                'ServiceItemPro': project.Event,
                                'HolidayCheck': false,
                                'LeaveCheck': false,
                                'OTHrs': 0,
                                'Days': daysList[date_string_Items.getDay()],
                                'isOtEligible': ot_flag_,
								'CustomerName':project.s_customername//18-08-2020
                            });
                            dataRow_TS_Items_Sa.push({
                                'Task': 'Project Activities (Project Task)',
                                'Date': nlapiDateToString(date_string_Items),
                                'Days': daysList[date_string_Items.getDay()],
                                'DayNo': date_string_Items.getDate(),
                                'Projects': dataRow_TS_Items_Sa_

                            });
                        }




                    }
                    if (parseInt(i) == parseInt(6) && parseInt(indx) != parseInt(0))
                        date_string_Items = date_string;
                    else
                        date_string_Items = nlapiAddDays(date_string_Items, 1);
                    indx++;
                    //newTimesheet.commitLineItem('timeitem');
                }
            });
            dataRow_TS_Items.push(dataRow_TS_Items_Su);
            dataRow_TS_Items.push(dataRow_TS_Items_Mo);
            dataRow_TS_Items.push(dataRow_TS_Items_Tu);
            dataRow_TS_Items.push(dataRow_TS_Items_We);
            dataRow_TS_Items.push(dataRow_TS_Items_Th);
            dataRow_TS_Items.push(dataRow_TS_Items_Fr);
            dataRow_TS_Items.push(dataRow_TS_Items_Sa);

            //OT
            /*dataRow_OT_Lines_.push(dataRow_OT_Items_Su_alloc);
            dataRow_OT_Lines_.push(dataRow_OT_Items_Mo_alloc);
            dataRow_OT_Lines_.push(dataRow_OT_Items_Tu_alloc);
            dataRow_OT_Lines_.push(dataRow_OT_Items_We_alloc);
            dataRow_OT_Lines_.push(dataRow_OT_Items_Th_alloc);
            dataRow_OT_Lines_.push(dataRow_OT_Items_Fr_alloc);
            dataRow_OT_Lines_.push(dataRow_OT_Items_Sa_alloc);*/

            var filters = [];
            /*filters[0] = new nlobjSearchFilter( 'formuladate', null, 'on', startDate);
            filters[0].setFormula('{startdate}');*/
            filters[0] = new nlobjSearchFilter('internalid', 'timesheet', 'anyof', timesheetId);

            var columns = [];
            columns.push(new nlobjSearchColumn('internalid'));
            columns.push(new nlobjSearchColumn('date').setSort(false)); //
            columns.push(new nlobjSearchColumn('timesheet'));
            columns.push(new nlobjSearchColumn('employee'));
            columns.push(new nlobjSearchColumn('customer').setSort(true));
            columns.push(new nlobjSearchColumn('internalid', 'item'));
            columns.push(new nlobjSearchColumn('casetaskevent'));
            columns.push(new nlobjSearchColumn('type'));
            columns.push(new nlobjSearchColumn('hours'));
			columns.push(new nlobjSearchColumn('customer','job'));

            var timeTrackSearch = searchRecord('timebill', null, filters, columns);

            if (timeTrackSearch) 
			{

                var JSON_TS_Lines = {};
                var dataRow_TS_Lines = [];
                //	var JSON_Holiday_Lines_ = {};
                //var dataRow_Holiday_Lines_ =[];
                //var JSON_Leave_Lines_ = {};
                //var dataRow_Leave_Lines_ =[];
                //var JSON_OT_Lines_ = {};
                //	var dataRow_OT_Lines_ =[];

                var dataRow_OverAll_TS = [];
                var JSON_ALL = {};
                var dataRow_ALL = [];
                var date_list = [];
                var item_list = [];
                var task_list = [];
                var dataRow_TS_Sun = [];
                var dataRow_TS_Sun_ = [];
                var dataRow_TS_Mon = [];
                var dataRow_TS_Mon_ = [];
                var dataRow_TS_Tue = [];
                var dataRow_TS_Tue_ = [];
                var dataRow_TS_Wed = [];
                var dataRow_TS_Wed_ = [];
                var dataRow_TS_Thur = [];
                var dataRow_TS_Thur_ = [];
                var dataRow_TS_Fri = [];
                var dataRow_TS_Fri_ = [];
                var dataRow_TS_Sat = [];
                var dataRow_TS_Sat_ = [];

                for (var inx = 0; inx < timeTrackSearch.length; inx++) {
                    var task = '';
                    var item_ = '';
                    var date_T_ = '';
                    var date_T = '';
                    var customer = '';
                    var hours = '';
                    var i_customer = '';
                    var proj_lookUp = '';
                    var hour_day = '';
                    var hours_week = '';
                    task = timeTrackSearch[inx].getText('casetaskevent');
                    item_ = timeTrackSearch[inx].getValue('internalid', 'item');
                    date_T_ = timeTrackSearch[inx].getValue('date');
                    //var day = timeTrackSearch[inx].getValue('day');
                    //date_T_ = timeTrackSearch[inx].getValue('date');
                    //var day = timeTrackSearch[inx].getValue('day');


                    date_T = nlapiStringToDate(date_T_);
                    var day_ = date_T.getDay();
                    var getTodayDate = date_T.getDate();
                    var s_day = daysList[day_];
                    customer = timeTrackSearch[inx].getText('customer');
                    hours = timeTrackSearch[inx].getValue('hours');
                    i_customer = timeTrackSearch[inx].getValue('customer');
					s_customername=timeTrackSearch[inx].getText('customer','job');//18-08-2020

                    if (i_customer) {
                        proj_lookUp = nlapiLookupField('job', i_customer, ['custentity_hoursperday', 'custentity_hoursperweek', 'custentity_onsite_hours_per_day', 'custentity_onsite_hours_per_week']);
                        var emp_subsidiary = nlapiLookupField('employee', parseInt(i_emp), 'subsidiary');
                        if (emp_subsidiary == 3) {
                            hour_day = proj_lookUp.custentity_hoursperday;
                           // if (!_logValidation(hour_day))
                               // hour_day = 8;

                            hours_week = proj_lookUp.custentity_hoursperweek;
                            //if (!_logValidation(hours_week))
                               // hours_week = 40;
                        } else {
                            hour_day = proj_lookUp.custentity_onsite_hours_per_day;
                           // if (!_logValidation(hour_day))
                               // hour_day = 8;

                            hours_week = proj_lookUp.custentity_onsite_hours_per_week;
                           // if (!_logValidation(hours_week))
                               // hours_week = 40;
                        }
                    }
                    //Change in structure
                    if (parseInt(day_) == parseInt(0)) { //Sunday
                        dataRow_TS_Sun = [];
                        dataRow_TS_Sun_.push({
                            //	'Day':sub_day,
                            'Date': date_T_,
                            'Project': customer,
                            'Task': task + ' (Project Task)',
                            'ServiceItem': item_,
                            'Hrs': 0,
                            'DayLimit': hour_day,
                            'WeekLimit': hours_week,
								'Max_Day_limit': i_max_Hrs_day,
							'Max_Month_limit' : i_Monthly_cap,
							'Max_year_limit':i_Yearly_Cap,	
                            'TaskPro': task + ' (Project Task)',
                            'ServiceItemPro': item_,
                            'HolidayCheck': false,
                            'LeaveCheck': false,
                            'OTHrs': 0,
                            'Days': s_day,
                            'isOtEligible': false,
							'CustomerName':s_customername//18-08-2020
                        });
                        dataRow_TS_Sun.push({
                            'Task': 'Project Activities (Project Task)',
                            'Date': date_T_,
                            'Days': s_day,
                            'DayNo': getTodayDate,
                            'Projects': dataRow_TS_Sun_

                        });
                    }
                    if (parseInt(day_) == parseInt(1)) { //Monday
                        dataRow_TS_Mon = [];
                        dataRow_TS_Mon_.push({
                            //	'Day':sub_day,
                            'Date': date_T_,
                            'Project': customer,
                            'Task': task + ' (Project Task)',
                            'ServiceItem': item_,
                            'Hrs': hours,
                            'DayLimit': hour_day,
                            'WeekLimit': hours_week,
								'Max_Day_limit': i_max_Hrs_day,
							'Max_Month_limit' : i_Monthly_cap,
							'Max_year_limit':i_Yearly_Cap,	
                            'TaskPro': task + ' (Project Task)',
                            'ServiceItemPro': item_,
                            'HolidayCheck': false,
                            'LeaveCheck': false,
                            'OTHrs': 0,
                            'Days': s_day,
                            'isOtEligible': false,
							'CustomerName':s_customername//18-08-2020
                        });
                        dataRow_TS_Mon.push({
                            'Task': 'Project Activities (Project Task)',
                            'Date': date_T_,
                            'Days': s_day,
                            'DayNo': getTodayDate,
                            'Projects': dataRow_TS_Mon_

                        });
						
                    }
					
					
                    if (parseInt(day_) == parseInt(2)) { //Tuesday
                        dataRow_TS_Tue = [];
                        dataRow_TS_Tue_.push({
                            //	'Day':sub_day,
                            'Date': date_T_,
                            'Project': customer,
                            'Task': task + ' (Project Task)',
                            'ServiceItem': item_,
                            'Hrs': hours,
                            'DayLimit': hour_day,
                            'WeekLimit': hours_week,
								'Max_Day_limit': i_max_Hrs_day,
							'Max_Month_limit' : i_Monthly_cap,
							'Max_year_limit':i_Yearly_Cap,	
                            'TaskPro': task + ' (Project Task)',
                            'ServiceItemPro': item_,
                            'HolidayCheck': false,
                            'LeaveCheck': false,
                            'OTHrs': 0,
                            'Days': s_day,
                            'isOtEligible': false,
							'CustomerName':s_customername//18-08-2020
                        });
                        dataRow_TS_Tue.push({
                            'Task': 'Project Activities (Project Task)',
                            'Date': date_T_,
                            'Days': s_day,
                            'DayNo': getTodayDate,
                            'Projects': dataRow_TS_Tue_

                        });
                    }
                    if (parseInt(day_) == parseInt(3)) { //Wednesday
                        dataRow_TS_Wed = [];
                        dataRow_TS_Wed_.push({
                            //	'Day':sub_day,
                            'Date': date_T_,
                            'Project': customer,
                            'Task': task + ' (Project Task)',
                            'ServiceItem': item_,
                            'Hrs': hours,
                            'DayLimit': hour_day,
                            'WeekLimit': hours_week,
								'Max_Day_limit': i_max_Hrs_day,
							'Max_Month_limit' : i_Monthly_cap,
							'Max_year_limit':i_Yearly_Cap,	
                            'TaskPro': task + ' (Project Task)',
                            'ServiceItemPro': item_,
                            'HolidayCheck': false,
                            'LeaveCheck': false,
                            'OTHrs': 0,
                            'Days': s_day,
                            'isOtEligible': false,
							'CustomerName':s_customername//18-08-2020
                        });
                        dataRow_TS_Wed.push({
                            'Task': 'Project Activities (Project Task)',
                            'Date': date_T_,
                            'Days': s_day,
                            'DayNo': getTodayDate,
                            'Projects': dataRow_TS_Wed_

                        });
                    }
                    if (parseInt(day_) == parseInt(4)) { //Thursday
                        dataRow_TS_Thur = [];
                        dataRow_TS_Thur_.push({
                            //	'Day':sub_day,
                            'Date': date_T_,
                            'Project': customer,
                            'Task': task + ' (Project Task)',
                            'ServiceItem': item_,
                            'Hrs': hours,
                            'DayLimit': hour_day,
                            'WeekLimit': hours_week,
								'Max_Day_limit': i_max_Hrs_day,
							'Max_Month_limit' : i_Monthly_cap,
							'Max_year_limit':i_Yearly_Cap,	
                            'TaskPro': task + ' (Project Task)',
                            'ServiceItemPro': item_,
                            'HolidayCheck': false,
                            'LeaveCheck': false,
                            'OTHrs': 0,
                            'Days': s_day,
                            'isOtEligible': false,
							'CustomerName':s_customername//18-08-2020
                        });
                        dataRow_TS_Thur.push({
                            'Task': 'Project Activities (Project Task)',
                            'Date': date_T_,
                            'Days': s_day,
                            'DayNo': getTodayDate,
                            'Projects': dataRow_TS_Thur_

                        });
                    }
                    if (parseInt(day_) == parseInt(5)) { //Friday
                        dataRow_TS_Fri = [];
                        dataRow_TS_Fri_.push({
                            //	'Day':sub_day,
                            'Date': date_T_,
                            'Project': customer,
                            'Task': task + ' (Project Task)',
                            'ServiceItem': item_,
                            'Hrs': hours,
                            'DayLimit': hour_day,
                            'WeekLimit': hours_week,
								'Max_Day_limit': i_max_Hrs_day,
							'Max_Month_limit' : i_Monthly_cap,
							'Max_year_limit':i_Yearly_Cap,	
                            'TaskPro': task + ' (Project Task)',
                            'ServiceItemPro': item_,
                            'HolidayCheck': false,
                            'LeaveCheck': false,
                            'OTHrs': 0,
                            'Days': s_day,
                            'isOtEligible': false,
							'CustomerName':s_customername//18-08-2020
                        });
                        dataRow_TS_Fri.push({
                            'Task': 'Project Activities (Project Task)',
                            'Date': date_T_,
                            'Days': s_day,
                            'DayNo': getTodayDate,
                            'Projects': dataRow_TS_Fri_

                        });
                    }
                    if (parseInt(day_) == parseInt(6)) { //Saturday
                        dataRow_TS_Sat = [];
                        dataRow_TS_Sat_.push({
                            //		'Day':sub_day,
                            'Date': date_T_,
                            'Project': customer,
                            'Task': task + ' (Project Task)',
                            'ServiceItem': item_,
                            'Hrs': 0,
                            'DayLimit': hour_day,
                            'WeekLimit': hours_week,
								'Max_Day_limit': i_max_Hrs_day,
							'Max_Month_limit' : i_Monthly_cap,
							'Max_year_limit':i_Yearly_Cap,	
                            'TaskPro': task + ' (Project Task)',
                            'ServiceItemPro': item_,
                            'HolidayCheck': false,
                            'LeaveCheck': false,
                            'OTHrs': 0,
                            'Days': s_day,
                            'isOtEligible': false,
							'CustomerName':s_customername//18-08-2020
                        });
                        dataRow_TS_Sat.push({
                            'Task': 'Project Activities (Project Task)',
                            'Date': date_T_,
                            'Days': s_day,
                            'DayNo': getTodayDate,
                            'Projects': dataRow_TS_Sat_

                        });
                    }

							
			

                } //End of For Loop
            } //End of search

            //Concat all TS
            dataRow_TS_Lines.push(dataRow_TS_Sun);
            dataRow_TS_Lines.push(dataRow_TS_Mon);
            dataRow_TS_Lines.push(dataRow_TS_Tue);
            dataRow_TS_Lines.push(dataRow_TS_Wed);
            dataRow_TS_Lines.push(dataRow_TS_Thur);
            dataRow_TS_Lines.push(dataRow_TS_Fri);
            dataRow_TS_Lines.push(dataRow_TS_Sat);


            JSON_ALL = {
                ServiceItem: dataRow_TS_Lines

            };
            dataRow_ALL.push(JSON_ALL);

            var JSON_Total = {};
            var dataRow_Total = [];

            JSON_Total = {
                Body: dataRow_TS_Body,
                Lines: dataRow_ALL,
                //OT: dataRow_OT_Lines_,
                Items: dataRow_TS_Items

            };
            dataRow_Total.push(JSON_Total);
            var JSON = {};
            var dataRow = [];
            //Customer Holiday List
            var customer_list = [];
            var holiday_list = [];
            //GET OT Flag From Allocation startDate, endDate,employeeId

            allocationDetails_items.forEach(function(project) {
                //var project_internal_id = project.Project;//CHanged Project as ProjectID to look up based on internalid
                var project_internal_id = project.ProjectID;
                var project_holiday = nlapiLookupField('job', project_internal_id,
                    'custentityproject_holiday');
                if (parseInt(project_holiday) != parseInt(1)) { //Project Marked as Customer Holiday
                    customer_list = get_holidays(startDate,
                        endDate, project.ProjectID, employeeId, project.Project,project.s_customername);
						

                } else {
                    //Get Holiday List in Week
                    holiday_list = get_company_holidays(startDate, endDate, employeeId, project.Project,project.s_customername);
					
                }
            });



            JSON = {
                TimeSheetDetails: dataRow_Total,
                HolidayList: holiday_list,
                CustomerHoliday: customer_list
            };


            dataRow.push(JSON);
        }
        return dataRow;
    } catch (e) {
        nlapiLogExecution('error', 'allocation details', e);
        throw e;
    }

}
//
//create a new timesheet record using the details retrieved
function autoGenerateTimesheet(weekStartDate, employee, allocationDetails) {

    try {
		
			var newTimesheet="";
		
			var timesheet_Search=nlapiSearchRecord("timesheet",null,
			[
			["employee","anyof",employee], 
			"AND", 
			["timesheetdate","on",weekStartDate]
			], 
			[
			new nlobjSearchColumn("internalid").setSort(false)
			]
			);
				if(_logValidation(timesheet_Search))
				{
					newTimesheet=nlapiLoadRecord('timesheet',timesheet_Search[0].getId(), {
					recordmode: 'dynamic'
					});
				}
				else
				{

					newTimesheet = nlapiCreateRecord('timesheet', {
					recordmode: 'dynamic'
					});
				}
		
		/*
        // create a new timesheet record
        var newTimesheet = nlapiCreateRecord('timesheet', {
            recordmode: 'dynamic'
        });
*/

        var daysOfWeek = ['hours0', 'hours1', 'hours2', 'hours3', 'hours4', 'hours5', 'hours6']; //Changed array by praveena 

        // var employeeDetails = nlapiLookupField('employee', employee, [
        // 'subsidiary', 'location'
        // ]);

        // set the main fields
        newTimesheet.setFieldValue('approvalstatus', '1');
        // newTimesheet.setFieldValue('customform', '');
        newTimesheet.setFieldValue('employee', employee);
        newTimesheet.setFieldValue('startdate', weekStartDate);
        // newTimesheet.setFieldValue('enddate', '');
        newTimesheet.setFieldValue('iscomplete', 'F');
        // newTimesheet.setFieldValue('subsidiary', employeeDetails.subsidiary);
        // newTimesheet.setFieldValue('totalhours', 40);

        // nlapiLogExecution('debug', 'allocationDetails',JSON.stringify(allocationDetails));

        // fill in the timesheet hours
        allocationDetails.forEach(function(project) {

            nlapiLogExecution('debug', 'allocationDetails', project.Project);
            nlapiLogExecution('debug', 'allocationDetails', project.Task);
			nlapiLogExecution('debug', 'project.Hours', project.Hours);
            //Added by praveena 0n 19-03-2020
            newTimesheet.selectNewLineItem('timeitem');
            newTimesheet.setCurrentLineItemValue('timeitem', 'customer', project.Project);
            newTimesheet.setCurrentLineItemValue('timeitem', 'casetaskevent', project.Task);
			var commit_flag=false;
            for (var i = 1; i < 6; i++) {

                if (_logValidation(project.Hours[i])) {
					commit_flag=true;
                    nlapiLogExecution('debug', 'project.Hours[i]', project.Hours[i]);

                    newTimesheet.setCurrentLineItemValue('timeitem', daysOfWeek[i], project.Hours[i]);
                    newTimesheet.setCurrentLineItemValue('timeitem','rate',project.BillRate);

                }


            }
			if(commit_flag==true){
            newTimesheet.commitLineItem('timeitem');
			}
        });

        var timesheetId = nlapiSubmitRecord(newTimesheet);
        return timesheetId;
    } catch (err) {
        nlapiLogExecution('ERROR', 'autoGenerateTimesheet', err);
        throw err;
    }
}

function isNumber(n) {

    return typeof n == 'number' && !isNaN(n) && isFinite(n);
}
//Get Active Real time alloaction data
function getActiveAllocations(weekStartDate, weekEndDate, employee) {

    try {
		
		nlapiLogExecution("Debug","weekStartDate : weekEndDate",weekStartDate+" : "+weekEndDate);
        // get active allocations
		//var allocationSearch=searchRecord('resourceallocation', 'customsearch3603',
      var allocationSearch = nlapiSearchRecord('resourceallocation',null, 
	  [
            new nlobjSearchFilter('startdate', null, 'notafter',
                weekEndDate),
            new nlobjSearchFilter('enddate', null, 'notbefore',
                weekStartDate),
            new nlobjSearchFilter('resource', null, 'anyof', employee)
        ], [
            new nlobjSearchColumn('project'),
            new nlobjSearchColumn('customer', 'job'),
            new nlobjSearchColumn('subsidiary', 'employee'),
            new nlobjSearchColumn('startdate'),
            new nlobjSearchColumn('jobtype', 'job'),
            new nlobjSearchColumn('jobbillingtype', 'job'),
            new nlobjSearchColumn('formulanumeric')
            .setFormula('{percentoftime}'),
            new nlobjSearchColumn('enddate'),
            new nlobjSearchColumn('percentoftime'),
            new nlobjSearchColumn('custevent1'),
            new nlobjSearchColumn('custevent_otserviceitem'),
            new nlobjSearchColumn('custevent3')
        ]);
		
		
        //Multiple Allocation
        /*if(allocationSearch){
        if(allocationSearch.length > 1) {
        	if(parseFloat(allocationSearch[0].getValue('formulanumeric')) < parseFloat(1)){
        	var percent_ = allocationSearch[0].getValue('formulanumeric');
        	nlapiLogExecution('DEBUG','Mutiple Allocation Percent',percent_);
        	nlapiLogExecution('DEBUG','Mutiple Allocation length',allocationSearch.length);
        	throw 'You cannot submit the timesheet as you have multiple project allocations';
        	}
        }
        }*/
        if (allocationSearch) {
			
			
            var weeks = [];
            var projects = [];
            var projectList = [];
            var activityNames = [];
				var a_srch_resource_allocation=[];
			for(var k=0;allocationSearch.length>k;k++)
			{
						//Added logic to reduce the timezone issue by praveena o 25-08-2020
							
						var load_record=nlapiLoadRecord('resourceallocation',allocationSearch[k].getId());
						nlapiLogExecution('debug','load_record',load_record.getFieldValue('enddate'));
						nlapiLogExecution('debug','load_record date',nlapiStringToDate(load_record.getFieldValue('enddate')));
						a_srch_resource_allocation.push({"allocationStart":load_record.getFieldValue('startdate'),
						"allocationEnd": load_record.getFieldValue('enddate')});
			}
            for (var j = 0; j < 7; j++) {
                weeks[j] = [];
                var currentDate = nlapiAddDays(
                    nlapiStringToDate(weekStartDate), j);

                for (var i = 0; i < allocationSearch.length; i++) {
                    var OTApplicable = '';
                  var allocationStart = nlapiStringToDate(allocationSearch[i]
                        .getValue('startdate'));
                    var allocationEnd = nlapiStringToDate(allocationSearch[i]
                        .getValue('enddate'));
					
						/*var load_record=nlapiLoadRecord('resourceallocation',allocationSearch[i].getId());
						nlapiLogExecution('debug','load_record',load_record.getFieldValue('enddate'));
						nlapiLogExecution('debug','load_record date',nlapiStringToDate(load_record.getFieldValue('enddate')));
						var allocationStart = nlapiStringToDate(load_record.getFieldValue('startdate'));
						var allocationEnd = nlapiStringToDate(load_record.getFieldValue('enddate'));
                        
                  
						*/
					nlapiLogExecution("Debug","AllocationStartDate : AllocationEndDate",allocationSearch[i].getValue('startdate')+" : "+allocationSearch[i].getValue('enddate'));
                  
                       var billRate = allocationSearch[i].getValue('custevent3');
                        nlapiLogExecution('debug', 'billRate Value ', billRate);
					
						var allocationStart = nlapiStringToDate(a_srch_resource_allocation[i]["allocationStart"]);
						var allocationEnd = nlapiStringToDate(a_srch_resource_allocation[i]["allocationEnd"]);
                      
					   nlapiLogExecution('debug','allocationStart',allocationStart);
						nlapiLogExecution('debug','allocationEnd date',allocationEnd);
						
                    var project = allocationSearch[i].getValue('project');
                    var i_customer = allocationSearch[i].getValue('customer', 'job');
                    var i_emp_subsidiary = allocationSearch[i].getValue('subsidiary', 'employee');
                    //if(parseInt(i_customer) == parseInt(8381) && parseInt(i_emp_subsidiary) == parseInt(3)){
                    //	throw 'You cannot submit the timesheet as you are allocated to MOVE-BTPL';
                    //	return;
                    //}
                    var otserviceitem = allocationSearch[i].getText('custevent_otserviceitem');
                    if (otserviceitem == 'OT')
                        OTApplicable = 'YES';
                    else
                        OTApplicable = 'NO';
                    var projectName = allocationSearch[i].getText('project');
                    var event = allocationSearch[i].getValue('custevent1');
                    var projectType = allocationSearch[i].getValue('jobtype',
                        'job');
                    var billingType = allocationSearch[i].getValue(
                        'jobbillingtype', 'job');
                    var serviceItem = null;
                    var percent = allocationSearch[i]
                        .getValue('formulanumeric');
                    var proj_lookUp = nlapiLookupField('job', parseInt(project),
                        ['custentity_hoursperday', 'custentity_hoursperweek', 'custentity_onsite_hours_per_day', 'custentity_onsite_hours_per_week'])
                    if (i_emp_subsidiary == 3) {
                        var hours_day = proj_lookUp.custentity_hoursperday;
                        if (_logValidation(hours_day)) {
                            var hours = percent * hours_day;
                        } else {
                            var hours = percent * 8;
                        }
                    } else {
                        var hours_day = proj_lookUp.custentity_onsite_hours_per_day;
                        if (_logValidation(hours_day)) {
                            var hours = percent * hours_day;
                        } else {
                            var hours = percent * 8;
                        }
                    }
                    hours = parseFloat(hours.toFixed(2));
                    // nlapiLogExecution('debug', 'Hours', hours);

                    activityNames = ["Project Activites", "Project activites",
                        "Project activities", "project activities",
                        "Bench Task", "Bench", "Project Activities-Offsite",
                        "Project Activities-Onsite", "Operations",
                        "Project Activities", "Project activity",
                        "project activity", "Project Activity", "Internal Project", "Bench Activities"
                    ];

                    // check project type to get the project activites name
                    // will be used later to assign task
                    // internal project
                    /*
                     * if (projectType == '1') {
                     * 
                     * 
                     * serviceItem = '2426'; // Internal Projects } else { //
                     * external project
                     * 
                     * if (billingType == 'TM') { // T&M activityNames = [
                     * "Operations", "Project Activities", "Project activity" ];
                     * serviceItem = '2222'; // ST } else { // FBI & FBM
                     * activityNames = [ "Operations", "Project Activities",
                     * "Project activity" ]; serviceItem = '2221';// FP } }
                     */

                    // create an array for days and hours to fill
                    if (currentDate >= allocationStart &&
                        currentDate <= allocationEnd) {
                        weeks[j].push(project);

                        var index = _.indexOf(projectList, project);

                        if (index == -1) {
                            projectList.push(project);
                            projects.push({
                                Project: project,
                                Task: '',
                                BillRate : billRate,
                                Event: serviceItem,
                                OT: OTApplicable,
                                // Days : [

                                // ],
                                Hours: [

                                ]
                            });
                            index = projects.length - 1;
                        }

                        // var date1 = currentDate);
                        var day = currentDate.getDay();
                        // projects[index].Days[day] = currentDate;
                        projects[index].Hours[day] = hours;
                    }
                }
            }

            // nlapiLogExecution('debug', 'weeks A', JSON.stringify(weeks));
            // nlapiLogExecution('debug', 'projects A',
            // JSON.stringify(projects));
            // nlapiLogExecution('debug', 'checkpoint', 1);
            // nlapiLogExecution('debug', 'activityNames',
            // JSON.stringify(activityNames));
            // nlapiLogExecution('debug', 'projects B',
            // JSON.stringify(projects));
            // nlapiLogExecution('debug', 'checkpoint', 2);
            // nlapiLogExecution('debug', 'activityNames',
            // JSON.stringify(activityNames));

            // creating a filter for project tasks
            var filter = [];
            var addOr = false;
            activityNames.forEach(function(name) {

                if (addOr) {
                    filter.push('or');
                }

                filter.push(['title', 'is', name]);

                addOr = true;
            });

            // nlapiLogExecution('debug', 'filter', JSON.stringify(filter));

            // searching the project task records for project activities
            var taskSearch = nlapiSearchRecord('projecttask', null, [filter,
                    'and', ['company', 'anyof', projectList]
                ],
                [new nlobjSearchColumn('company')]);

            if (!taskSearch) {
                throw "Project Task not found";
            }

            taskSearch.forEach(function(task) {

                for (var i = 0; i < projects.length; i++) {

                    if (projects[i].Project == task.getValue('company')) {
                        projects[i].Task = task.getId();
                    }
                }
            });


            nlapiLogExecution('debug', 'projects', JSON.stringify(projects));
        } else {
            throw "No Active Allocation for this week. Kindly contact your manager/business.ops@BRILLIO.COM";
        }

        return projects;
    } catch (err) {
        nlapiLogExecution('ERROR', 'getActiveAllocations', err);
        throw err;
    }
}

//For Items Array
//Get Active Real time alloaction data
function getActiveAllocations_Items(weekStartDate, weekEndDate, employee) {

    try {
        // get active allocations
        var allocationSearch = nlapiSearchRecord('resourceallocation', null, [
            new nlobjSearchFilter('startdate', null, 'notafter',
                weekEndDate),
            new nlobjSearchFilter('enddate', null, 'notbefore',
                weekStartDate),
            new nlobjSearchFilter('resource', null, 'anyof', employee)
        ], [
            new nlobjSearchColumn('project'),
            new nlobjSearchColumn('startdate'),
            new nlobjSearchColumn('jobtype', 'job'),
            new nlobjSearchColumn('jobbillingtype', 'job'),
            new nlobjSearchColumn('formulanumeric')
            .setFormula('{percentoftime}'),
            new nlobjSearchColumn('enddate').setSort(false),
            new nlobjSearchColumn('percentoftime'),
            new nlobjSearchColumn('custevent1'),
            new nlobjSearchColumn('custevent_otserviceitem'),
			new nlobjSearchColumn('customer', 'job')//added customer
        ]);
        //Multiple Allocation
        /*if(allocationSearch){
        if(allocationSearch.length > 1 && (allocationSearch[0].getValue('formulanumeric') < 100)){ 
        	nlapiLogExecution('DEBUG','Mutiple Allocation');
        	throw 'Mutiple Allocation';
        	
        }
        }*/
        if (allocationSearch) {
            var weeks = [];
            var projects = [];
            var projectList = [];
            var activityNames = [];
            var item_id = '';
            var proj_concat = '';
            var en_date = '';
			
			var a_srch_resource_allocation=[];
			for(var k=0;allocationSearch.length>k;k++)
			{
						//Added logic to reduce the timezone issue by praveena o 25-08-2020
							
						var load_record=nlapiLoadRecord('resourceallocation',allocationSearch[k].getId());
						nlapiLogExecution('debug','load_record',load_record.getFieldValue('enddate'));
						nlapiLogExecution('debug','load_record date',nlapiStringToDate(load_record.getFieldValue('enddate')));
						a_srch_resource_allocation.push({"allocationStart":load_record.getFieldValue('startdate'),
						"allocationEnd": load_record.getFieldValue('enddate')});
			}
			
            for (var j = 0; j < 7; j++) {
                weeks[j] = [];
                var currentDate = nlapiAddDays(
                    nlapiStringToDate(weekStartDate), j);

                for (var i = 0; i < allocationSearch.length; i++) {
                    var OTApplicable = '';
                    var event = '';
                    var allocationStart = nlapiStringToDate(allocationSearch[i]
                        .getValue('startdate'));
                    var allocationEnd = nlapiStringToDate(allocationSearch[i]
                        .getValue('enddate'));
						nlapiLogExecution("DEBUG","allocationEnd",allocationEnd);
						
						
						
						var allocationStart = nlapiStringToDate(a_srch_resource_allocation[i]["allocationStart"]);
						var allocationEnd = nlapiStringToDate(a_srch_resource_allocation[i]["allocationEnd"]);
                      
					   nlapiLogExecution('debug','allocationStart',allocationStart);
						nlapiLogExecution('debug','allocationEnd date',allocationEnd);
						
						
                    var project = allocationSearch[i].getValue('project');
                    var otserviceitem = allocationSearch[i].getText('custevent_otserviceitem');
					var s_customername=allocationSearch[i].getText('customer','job');
                    if (otserviceitem == 'OT')
                        OTApplicable = 'YES';
                    else
                        OTApplicable = 'NO';
                    var projectName = allocationSearch[i].getText('project');
                    event = allocationSearch[i].getValue('custevent1');
					
                     en_date = allocationEnd
					

                    //Find the internalID of service Item
                    if (event) {
                        var serviceItemSearch = nlapiSearchRecord('item', null, [
                            new nlobjSearchFilter('itemid', null, 'is',
                                event),
                            new nlobjSearchFilter('isinactive', null, 'is',
                                'F')
                        ], [new nlobjSearchColumn('internalid')]);

                        if (serviceItemSearch)
                            item_id = serviceItemSearch[0].getValue('internalid');
                    }
                    //Find the Project name
                    if (project) {
                        var projectSearch = nlapiSearchRecord('job', null, [
                            new nlobjSearchFilter('internalid', null, 'is',
                                project),
                        ], [new nlobjSearchColumn('entityid'),
                            new nlobjSearchColumn('companyname'),
                            new nlobjSearchColumn('companyname', 'customer')
                        ]);

                        if (projectSearch)
                            proj_concat = projectSearch[0].getValue('entityid') + ' ' + projectSearch[0].getValue('companyname');
                    }

                    //var serviceItem = allocationSearch[i].getValue('custevent1');
                    var projectType = allocationSearch[i].getValue('jobtype',
                        'job');
                    var billingType = allocationSearch[i].getValue(
                        'jobbillingtype', 'job');
                    var serviceItem = null;
                    var percent = allocationSearch[i]
                        .getValue('formulanumeric');
                    var hours = percent * 8;
                    hours = parseFloat(hours.toFixed(2));
                    // nlapiLogExecution('debug', 'Hours', hours);

                    activityNames = ["Project Activites", "Project activites",
                        "Project activities", "project activities",
                        "Bench Task", "Bench", "Project Activities-Offsite",
                        "Project Activities-Onsite", "Operations",
                        "Project Activities", "Project activity",
                        "project activity", "Project Activity", "Internal Project", "Bench Activities"
                    ];

                    // check project type to get the project activites name
                    // will be used later to assign task
                    // internal project
                    /*
                     * if (projectType == '1') {
                     * 
                     * 
                     * serviceItem = '2426'; // Internal Projects } else { //
                     * external project
                     * 
                     * if (billingType == 'TM') { // T&M activityNames = [
                     * "Operations", "Project Activities", "Project activity" ];
                     * serviceItem = '2222'; // ST } else { // FBI & FBM
                     * activityNames = [ "Operations", "Project Activities",
                     * "Project activity" ]; serviceItem = '2221';// FP } }
                     */

                    // create an array for days and hours to fill
                    if (currentDate >= allocationStart &&
                        currentDate <= allocationEnd) {
                        weeks[j].push(project);

                        var index = _.indexOf(projectList, project);

                        if (index == -1) {
                            projectList.push(project);
                            projects.push({
                                Project: proj_concat,
                                ProjectID: project,
                                Task: '',
                                //Task_Name : '',
                                Event: item_id,
                                OT: OTApplicable,
                                Percent: percent,
								s_customername:s_customername,//18-08-2020
                                // Days : [

                                // ],
                                Hours: [

                                ]
                            });
                            index = projects.length - 1;
                        }

                        // var date1 = currentDate);
                        var day = currentDate.getDay();
                        // projects[index].Days[day] = currentDate;
                        projects[index].Hours[day] = hours;
                    }
                }
              //Comment the code by praveena to check the exception in case of firday and added logic below code
              /*
              //To check Mid week ending allocations
					if(en_date < nlapiStringToDate(weekEndDate))
					{
						throw "Allocation ending in the middle of the week, hence restricting the submission of timesheet.Kindly contact your manager/business.ops@BRILLIO.COM";
					}
              */
              var day_endate=en_date.getDay();
              var d_weekEndDate=nlapiStringToDate(weekEndDate);
              nlapiLogExecution("DEBUG","day_endate",day_endate);
              nlapiLogExecution("DEBUG","d_weekEndDate",d_weekEndDate);
              nlapiLogExecution("DEBUG","en_date",en_date);
              //To check Mid week ending allocations
					//if(en_date < d_weekEndDate  && (day_endate!=5) && (day_endate)!=6)
					if(en_date < d_weekEndDate)
					{
						throw "Allocation ending in the middle of the week, hence restricting the submission of timesheet.Kindly contact your manager/business.ops@BRILLIO.COM";
					}
            }

            // nlapiLogExecution('debug', 'weeks A', JSON.stringify(weeks));
            // nlapiLogExecution('debug', 'projects A',
            // JSON.stringify(projects));
            // nlapiLogExecution('debug', 'checkpoint', 1);
            // nlapiLogExecution('debug', 'activityNames',
            // JSON.stringify(activityNames));
            // nlapiLogExecution('debug', 'projects B',
            // JSON.stringify(projects));
            // nlapiLogExecution('debug', 'checkpoint', 2);
            // nlapiLogExecution('debug', 'activityNames',
            // JSON.stringify(activityNames));

            // creating a filter for project tasks
            var filter = [];
            var addOr = false;
            activityNames.forEach(function(name) {

                if (addOr) {
                    filter.push('or');
                }

                filter.push(['title', 'is', name]);

                addOr = true;
            });

            // nlapiLogExecution('debug', 'filter', JSON.stringify(filter));

            // searching the project task records for project activities
            var taskSearch = nlapiSearchRecord('projecttask', null, [filter,
                    'and', ['company', 'anyof', projectList]
                ],
                [new nlobjSearchColumn('company'),
                    new nlobjSearchColumn('title')
                ]);
            nlapiLogExecution('DEBUG', 'Task taskSearch', taskSearch.length);
            if (!taskSearch) {
                throw "Project Task not found";
            }

            taskSearch.forEach(function(task) {

                for (var i = 0; i < projects.length; i++) {

                    if (projects[i].ProjectID == task.getValue('company')) {
                        var taskName = task.getValue('title');
                        nlapiLogExecution('DEBUG', 'Task Name', taskName);
                        //projects[i].Task = task.getId();
                        projects[i].Task = taskName + ' ' + '(Project Task)';
                    }
                }
            });


            // nlapiLogExecution('debug', 'projects', JSON.stringify(projects));
        } else {
            throw "No Active Allocation for this week. Kindly contact your manager/business.ops@BRILLIO.COM";
        }

        return projects;
    } catch (err) {
        nlapiLogExecution('ERROR', 'getActiveAllocations_Items', err);
        throw err;
    }
}

function get_company_holidays(start_date, end_date, emp, Project_ID,s_customername) {
    var holiday_list = [];

    //start_date = nlapiDateToString(start_date, 'date');
    //end_date = nlapiDateToString(end_date, 'date');

    nlapiLogExecution('debug', 'start_date', start_date);
    nlapiLogExecution('debug', 'end_date', end_date);
    nlapiLogExecution('debug', 'emp', emp);
    var employeeLocation = nlapiLookupField('employee', parseInt(emp), 'location');

    var locationArray = [employeeLocation];
    var currentLocation = employeeLocation;

    while (currentLocation) {
        var locationRec = nlapiLoadRecord('location', currentLocation);
        currentLocation = locationRec.getFieldValue('parent');

        if (currentLocation) {
            locationArray.push(currentLocation);
        }
    }

    //var subsidiary = empLook.subsidiary;
    nlapiLogExecution('debug', 'location', locationArray);

    var search_company_holiday = nlapiSearchRecord("customrecord_mobile_holiday_calendar", null,
        [
            ["custrecord_mhc_date", "within", start_date, end_date],
            "AND",
            ["isinactive", "is", "F"],
            "AND",
            ["custrecord_mhc_location", "anyof", locationArray]
        ],
        [
            new nlobjSearchColumn("custrecord_mhc_date"),
            new nlobjSearchColumn("custrecord_mhc_title"),
            new nlobjSearchColumn("custrecord_mhc_location"),
            new nlobjSearchColumn("custrecord_mhc_image")
        ]
    );


    if (search_company_holiday) {

        for (var i = 0; i < search_company_holiday.length; i++) {
            holiday_list.push({
                'ProjectID': Project_ID,
                'Date': search_company_holiday[i].getValue('custrecord_mhc_date'),
				'CustomerName':s_customername
            });
        }

    }

    return holiday_list;
}

function getNextWeekData(dataIn, employeeId, startDate, endDate, autoFill) {
    try {
        var dataRow = [];
        var d_startDate = nlapiStringToDate(startDate);
        //Data For next Week data
        var allocationDetails = getActiveAllocations(startDate, endDate,
            employeeId);
        //Get Holiday List in Week
        var holiday_list = get_company_holidays(startDate, endDate, employeeId);
        // get active allocations, hours ,etc.
        var daysOfWeek = ['timebill0', 'timebill1', 'timebill2', 'timebill3', 'timebill4', 'timebill5', 'timebill6']; //array values Changed by praveena on 18-03-2020
        var dataRow_N = [];
        var dataRow_TN = [];
        var JSON_N = {};
        var JSON_TN = {};
        var JSON_TS_Lines_N = {};
        var dataRow_TS_Lines_N = [];
        var JSON_Holiday_Lines_N = {};
        var dataRow_Holiday_Lines_N = [];
        var JSON_Leave_Lines_N = {};
        var dataRow_Leave_Lines_N = [];
        var JSON_OT_Lines_N = {};
        var dataRow_OT_Lines_N = [];

        var dataRow_OverAll_TS_N = [];

        allocationDetails.forEach(function(project) {

            var proj_id = project.Project;
            var proj_look = nlapiLookupField('job', proj_id, ['entityid', 'altname']);
            var id = proj_look.entityid;
            var name = proj_look.altname;
            var customer = id + ' ' + name;

            var proj_Task = project.Task;
            var proj_lookTask = nlapiLookupField('projecttask', proj_Task, ['title']);
            var id_Task = proj_lookTask.title;

            var name_Task = id_Task + ' (Project Task)';
            //newTimesheet.selectNewLineItem('timeitem');

            for (var i = 0; i < 7; i++) {

                //if (isNumber(project.Hours[i])) {
                // nlapiLogExecution('debug', 'project.Hours[i]',
                // project.Hours[i]);
                JSON_TS_Lines_N = {
                    Day: nlapiDateToString(d_startDate),
                    // newEntry.setFieldValue('subsidiary',
                    // employeeDetails.subsidiary);
                    //Employee: employeeId,
                    Project: customer,
                    ServiceItem: '',
                    // newEntry.setFieldValue('location',
                    // employeeDetails.location);
                    // newEntry.setFieldText('projecttaskassignment',
                    // project.Event);
                    Task: name_Task,
                    Hours: '0:00',
                    //OT: project.OT
                    // newEntry.setFieldValue('isbillable', 'T');


                };
                dataRow_TS_Lines_N.push(JSON_TS_Lines_N);

                JSON_Holiday_Lines_N = {
                    Day: nlapiDateToString(d_startDate),
                    // newEntry.setFieldValue('subsidiary',
                    // employeeDetails.subsidiary);
                    //Employee: employeeId,
                    Project: customer,
                    ServiceItem: 2480,
                    // newEntry.setFieldValue('location',
                    // employeeDetails.location);
                    // newEntry.setFieldText('projecttaskassignment',
                    // project.Event);
                    Task: 'Holiday (Project Task)',
                    Hours: '0:00',
                    //OT: project.OT
                    // newEntry.setFieldValue('isbillable', 'T');


                };
                dataRow_Holiday_Lines_N.push(JSON_Holiday_Lines_N);

                JSON_Leave_Lines_N = {
                    Day: nlapiDateToString(d_startDate),
                    // newEntry.setFieldValue('subsidiary',
                    // employeeDetails.subsidiary);
                    //Employee: employeeId,
                    Project: customer,
                    ServiceItem: 2479,
                    // newEntry.setFieldValue('location',
                    // employeeDetails.location);
                    // newEntry.setFieldText('projecttaskassignment',
                    // project.Event);
                    Task: 'Leave (Project Task)',
                    Hours: '0:00',
                    //OT: project.OT
                    // newEntry.setFieldValue('isbillable', 'T');


                };
                dataRow_Leave_Lines_N.push(JSON_Leave_Lines_N);
                if (project.OT == 'YES') {
                    JSON_OT_Lines_N = {
                        Day: nlapiDateToString(d_startDate),
                        // newEntry.setFieldValue('subsidiary',
                        // employeeDetails.subsidiary);
                        //Employee: employeeId,
                        Project: customer,
                        ServiceItem: 2425,
                        // newEntry.setFieldValue('location',
                        // employeeDetails.location);
                        // newEntry.setFieldText('projecttaskassignment',
                        // project.Event);
                        Task: 'OT (Project Task)',
                        Hours: '0:00',
                        //OT: project.OT
                        // newEntry.setFieldValue('isbillable', 'T');


                    };
                    dataRow_OT_Lines_N.push(JSON_OT_Lines_N);
                }
                d_startDate = nlapiAddDays(d_startDate, 1);
                //}
            }
        });
        JSON_N = {
            ServiceItem: dataRow_TS_Lines_N,
            Holiday: dataRow_Holiday_Lines_N,
            Leave: dataRow_Leave_Lines_N,
            OT: dataRow_OT_Lines_N
        };
        dataRow_TN.push(JSON_N);
        JSON_TN = {
            TimeSheetDetails: dataRow_TN,
            HolidayList: holiday_list

        };

        dataRow_N.push(JSON_TN);
        //}

        //	}
        return dataRow_N;
    } catch (err) {
        nlapiLogExecution('ERROR', 'next week TS', err);
        throw err;
    }
}

//validate blank entries
function _logValidation(value) {
    if (value != null && value.toString() != null && value != '' && value != undefined && value.toString() != undefined && value != 'undefined' && value.toString() != 'undefined' && value.toString() != 'NaN' && value != NaN) {
        return true;
    } else {
        return false;
    }
}


function getCompanyCurrentDateTime(UTCtime) { 
    var UTCtime = UTCtime; // Utc time is in milliseconds 
    var companyTimeZone = nlapiLoadConfiguration('companyinformation').getFieldText('timezone');
    nlapiLogExecution("DEBUG", "companyTimeZone",companyTimeZone );
    var timeZoneOffSet = (companyTimeZone.indexOf('(GMT)') == 0) ? 0 : new Number(companyTimeZone.substr(4, 6).replace(/\+|:00/gi, '').replace(/:30/gi, '.5'));
    nlapiLogExecution("DEBUG", "timeZoneOffSet",timeZoneOffSet ); // timezone offset is in hours and minutes 
    var timeZoneOffSetStr = timeZoneOffSet.toString();
    var timeZoneOffSetRes = timeZoneOffSetStr.split(".");
    var timeZoneOffSetMilli = Number(timeZoneOffSetRes[0])*3600*1000 + Number(timeZoneOffSetRes[1])*60*1000;
    nlapiLogExecution("DEBUG", "timeZoneOffSetMilli",timeZoneOffSetMilli );
    var NewcompanyDateTime = new Date(UTCtime + (timeZoneOffSetMilli));
    nlapiLogExecution("DEBUG", "NewcompanyDateTime",NewcompanyDateTime );
    return NewcompanyDateTime;
}