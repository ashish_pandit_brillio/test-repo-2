//Get post

function inProgressExpRep(request, response)
 {
	if (request.getMethod() == 'GET') 
	{

		  //Create the form and add fields to it 
		  var form = nlapiCreateForm("Suitelet - In Progress Expense Report");
		  
					  
		 
		 


			
			
		 var searchresult_exp_rpt = nlapiSearchRecord('transaction', 'customsearch_in_progress_expense_report', null, null);
		 
		  
		  
		  if(searchresult_exp_rpt==null){
			  var count = 0;
		  }else{
			  count = searchresult_exp_rpt.length;
		  }
		 
			var detail = form.addSubList('custpage_expense_report_detail', 'list', 'In Progress Expense ' + count, 'general');

			//add fields to the sublist
			detail.addField('custpage_check_box', 'checkbox', 'select');
			detail.addField('custpage_date', 'date', 'Date');
			detail.addField('custpage_doc_no', 'text', 'Document Number');
			detail.addField('custpage_emp_name', 'text', 'Emp Name');
			detail.addField('custpage_first_level_approver', 'text', '1st Level Approver');
			detail.addField('custpage_expense_approver', 'text', 'Expense Approver');
			detail.addField('custpage_amount', 'text', 'Amount');
			detail.addField('custpage_currency', 'text', 'Currency');
			detail.addField('custpage_doc_internal_id', 'text', 'Internal Id').setDisplayType('hidden');
	
              
		if (searchresult_exp_rpt != null && searchresult_exp_rpt != undefined && searchresult_exp_rpt != '') //
        {    
		    		
		
		     var exp_data = [];
			 
			 for (var i = 0; i < searchresult_exp_rpt.length; i++) //
             {
                var a_search_exp_data = searchresult_exp_rpt[i];
                var all_columns = a_search_exp_data.getAllColumns();
                var exp_date = a_search_exp_data.getValue(all_columns[0]);
				var doc_no = a_search_exp_data.getValue(all_columns[1]);
				var emp_name = a_search_exp_data.getValue(all_columns[2]);
				var first_approver = a_search_exp_data.getText(all_columns[4]);
				var exp_approver = a_search_exp_data.getText(all_columns[5]);
				var amount = a_search_exp_data.getValue(all_columns[6]);
				var currency = a_search_exp_data.getText(all_columns[7]);
				var internal_id = a_search_exp_data.getText(all_columns[8]);
			//	project = a_search_empdata.getValue(all_columns[7]);
                //nlapiLogExecution('DEBUG', 'SearchTimeData', 'i : ' + i + ' emp ==' + emp);
                
                if (doc_no != null) 
				{		  				  
					
					var  expense_data={
						   
						  'custpage_date'                   : exp_date,
						  'custpage_doc_no'      		    :  doc_no,
						  'custpage_emp_name'               :  emp_name,
						  'custpage_first_level_approver'	:  first_approver,
						  'custpage_expense_approver'		:  exp_approver,
						  'custpage_amount'                 :  amount,
						  'custpage_currency'               :  currency,
						  'custpage_doc_internal_id'        :  internal_id
						  
						};
						exp_data.push(expense_data);
				}   				  
				  
			}
			detail.setLineItemValues(exp_data);
		}
			 
		
	
  
		 form.setScript('customscript_cli_exp_report_pending');
		  
		  
		  form.addButton('custpage_button_send_mail', 'Send Mail','onclick_send_mail()');
		  
		  form.addButton('custpage_button_select_all' ,'Select All','onclick_select_all()');
		  
		  form.addButton('custpage_button_delete' ,'Delete','onclick_delete()');
		//  form.addButton('custpage_button1', 'Reject','onclick_reject()');
		//  form.addButton('custpage_button2', 'Check','onclick_approval_check()');
			
			
	
				
	response.writePage(form);

 }
 //POST call
	else 
	{
		  var form = nlapiCreateForm("Suitelet - POST call");

		  //create the fields on the form and populate them with values from the previous screen 
		  var resultField1 = form.addField('emp_res1', 'select', 'Emplolyee Name: ','employee');
		  resultField1.setDefaultValue(request.getParameter('employee_name'));
		  resultField1.setDisplayType('inline');


	 

	  response.writePage(form);
	}
}




