/**
 * Module Description
 * 
 * Version Date Author Remarks 1.00 09 Oct 2015 nitish.mishra
 * 
 */

function suitelet(request, response) {
	try {

		if (request.getMethod() == "GET") {

			var form = nlapiCreateForm('Not Submitted Timesheets');
			form.addField('custpage_startdate', 'date', 'Start Date')
			        .setMandatory(true);
			form.addField('custpage_enddate', 'date', 'End Date').setMandatory(
			        true);
			form.addField('custpage_emp', 'select', 'Employee', 'employee');
			form.addField('custpage_project', 'select', 'Project', 'job');
			form.addSubmitButton("Submit");
			response.writePage(form);

		} else {
			nlapiLogExecution('debug', 'started');
			var employee = request.getParameter('custpage_emp');
			var project = request.getParameter('custpage_project');
			var start_date = request.getParameter('custpage_startdate');
			var end_date = request.getParameter('custpage_enddate');
			var all_employee_list = [];

			if (project) {
				var allocationSearch = searchRecord('resourceallocation', null,
				        [
				                new nlobjSearchFilter(
				                        'custentity_employee_inactive',
				                        'employee', 'is', 'F'),
				                new nlobjSearchFilter('project', null, 'anyof',
				                        project),
				                new nlobjSearchFilter(
				                        'custentity_implementationteam',
				                        'employee', 'is', 'F') ],
				        [ new nlobjSearchColumn('resource') ]);

				if (allocationSearch) {

					allocationSearch
					        .forEach(function(allocation) {
						        all_employee_list.push(allocation
						                .getValue('resource'));
					        });
				}
			} else if (employee) {
				all_employee_list = [ employee ];
			} else {
				var empSearch = searchRecord('employee', null, [
				        new nlobjSearchFilter('custentity_employee_inactive',
				                null, 'is', 'F'),
				        new nlobjSearchFilter('custentity_implementationteam',
				                null, 'is', 'F') ]);

				for (var i = 0; i < empSearch.length; i++) {
					all_employee_list.push(empSearch[i].getId());
				}
			}

			var notSubmittedList = getNotSubmitted(start_date, end_date,
			        all_employee_list, true);

			var form = nlapiCreateForm('Not Submitted Timesheets');
			form.addField('custpage_startdate', 'date', 'Start Date')
			        .setMandatory(true).setDefaultValue(start_date);
			form.addField('custpage_enddate', 'date', 'End Date').setMandatory(
			        true).setDefaultValue(end_date);
			var empField = form.addField('custpage_emp', 'select', 'Employee',
			        'employee');
			empField.setDefaultValue(employee);
			var projectField = form.addField('custpage_project', 'select',
			        'Project', 'job');
			projectField.setDefaultValue(project);
			form.addSubmitButton("Submit");

			var sublist = form.addSubList('custpage_list', 'staticlist',
			        'Not Submitted Days Count');

			sublist.addField('employee', 'select', 'Employee', 'employee')
			        .setDisplayType('inline');
			sublist.addField('project', 'select', 'Project', 'job')
			        .setDisplayType('inline');
			sublist.addField('days', 'text', 'Days Not Submitted')
			        .setDisplayType('inline');

			var timesheetData = [];

			if (all_employee_list.length > 0) {
				timesheetData = formatNotSubmittedData(start_date,
				        notSubmittedList);
			}

			sublist.setLineItemValues(timesheetData);

			response.writePage(form);
		}
	} catch (err) {
		nlapiLogExecution('error', 'suitelet', err);
	}
}

function formatNotSubmittedData(startDate, notSubmittedList) {
	var finalList = [];
	var d_startDate = nlapiStringToDate(startDate);

	for ( var emp in notSubmittedList) {

		for ( var pro in notSubmittedList[emp]) {
			var obj = {
			    employee : emp.substring(1, emp.length),
			    project : pro.substring(1, pro.length),
			    days : ''
			};

			var dayList = notSubmittedList[emp][pro].DaysArray;
			nlapiLogExecution('debug', 'dayList', dayList.lenght);

			for (var i = 0; i < dayList.lenght; i++) {

				if (dayList[i] == 2) {
					obj.days += (nlapiDateToString(
					        nlapiAddDays(d_startDate, i), 'date'))
					        + " ; ";
				}
			}

			finalList.push(obj);
		}
	}

	return finalList;
}