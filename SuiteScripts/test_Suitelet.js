	 
	  function createSuitelet(request, response) {

         try {

            /* if (request.getMethod() == 'GET') {

                 var form = nlapiCreateForm("Test- Suitelet");	


                 form.addSubmitButton('PDF- GENERATOR');

                 response.writePage(form);
             }
          
            else*/ {
				
				try{
	
	var a_data_array = new Array();
	var a_hours_array = new Array();
	var a_employee_array = new Array();
	var a_line_array = new Array();
	var i_inc_cnt = 0;
	var i_cnt = 0;
	var i_hours_per_day;
	var i_hours_per_week;
	var i_days_for_month;
	var i_OT_applicable;
	var i_OT_rate_ST;
	var i_week_days;
	var a_location_data_array = new Array();
	var i_rate = 0;
	var otrVar;
	var ctrVar;
	var i_flag = 0;
	var i_project;
	var i_project_text;
	var i_total_amount_INV = 0;
	var a_split_f_array = new Array();
	var i_cnt = 0;
	var i_S_No_cnt = 0;
	var strVar = "";
	var a_employee_array = new Array();
	var a_employee_name_array = new Array();
	var i_cnt = 0;
	var linenum = '17';
	var pagening = 4;
	var pagecount = 0;

	var s_remittance_details = "Accounts Receivable, Brillio LLC;\n100 Town Square Place;\nSuite 308, Jersey City\n NJ 07310"

	var flag_name = '';
	var emp_name = '';
	var prev_emp_name = '';
	
	
	
	
	
	var address_1 = '';
	var address_2 = '';
	var concat_city = '';
	var concat_state = '';
	address_1 = '#58, 1st Main Road, Mini Forest';
	address_2 = 'J.P Nagar 3rd Phase';
	address_3 = 'Bangalore 560078';
	//retrieve the record id passed to the Suitelet
	var recId = '2433116'
	//2433116
	recId = parseInt(recId);
	if(recId){
		var recObj = nlapiLoadRecord('invoice',recId);
		var invoice_number = recObj.getFieldValue('tranid');
		var invoice_date = recObj.getFieldValue('trandate');
		var po_num = recObj.getFieldValue('otherrefnum');
	
		
		if(!_logValidation(po_num))
		{
			po_num='';
		}
		var split_Obj = invoice_date.split('/');
		var month = split_Obj[0];
		var date = split_Obj[1];
		var year = split_Obj[2];
		var year_sub = year.substring(2, 4);
		var month_T = getMonth(month);
		var invoice_formated_date = date +'-'+month_T+'-'+year_sub;
		//invoice_date = nlapiStringToDate(invoice_date);
		//Invoice template address
		var btpl_layout = recObj.getFieldValue('custbody_invoice_btpl_layout');
		var memo=recObj.getFieldValue('memo');
		if(!_logValidation(memo))
		{
		memo='';
		}
		var invoice_amt = recObj.getFieldValue('subtotal');
		var customer_Id = recObj.getFieldValue('entity');
		var project_Id =  recObj.getFieldValue('job');
        
        
        nlapiLogExecution('DEBUG','project_Id',project_Id);
		//var customername= recObj.getFieldText('entity');
		var service_category = recObj.getFieldText('custbody_servicetaxcategory');
		var billingdescription = recObj.getFieldValue('custbody_billingdescription');
		if(!_logValidation(billingdescription))
		{
		billingdescription='';
		}
		var billfrom = recObj.getFieldValue('custbody_billfrom');
		var billto = recObj.getFieldValue('custbody_billto');
		var filter=[];
		filter[0]=new nlobjSearchFilter('entity',null,'anyOf',customer_Id);
        
          filter[1]=new nlobjSearchFilter('internalid','job','anyOf',project_Id); //{jobmain.internalid}
        
		
		var col=[];
		col[0]=new nlobjSearchColumn('billaddressee');
		col[1]=new nlobjSearchColumn('billaddress1');
		col[2]=new nlobjSearchColumn('billcity');
		col[3]=new nlobjSearchColumn('billstate');
		col[4]=new nlobjSearchColumn('billzip');
		col[5]=new nlobjSearchColumn('billcountry');
		var search=nlapiSearchRecord('salesorder', null, filter,col);
		       for(var i=0;i<search.length;i++)
			   {
					var billadd=search[i].getValue('billaddressee');
					var billadd1=search[i].getValue('billaddress1');
					var billcity=search[i].getValue('billcity');
					var billstate=search[i].getValue('billstate');
					var billzip=search[i].getValue('billzip');
					var billcountry=search[i].getText('billcountry');
					
					nlapiLogExecution('DEBUG','Bill Adress',billadd);
			   }
			   if(!_logValidation(billadd1))
			   {
			   billadd1='';
			   }
			   else  { if(!_logValidation(billcity))
			   {
			   billcity='';
			   }
			   }
			   
			   
				   concat_city = billcity+' '+billzip;
				   concat_state= billstate+','+billcountry;
			   
			   
			   
		nlapiLogExecution('DEBUG','PONUM',po_num);
		var dataRow_description = [];
		var json = {};
		var lineCount = recObj.getLineItemCount('item');
		nlapiLogExecution('DEBUG','linecount',lineCount);
		for(var j=1;j<=lineCount;j++){
		json = {	
		 Description : recObj.getLineItemValue('item','description',j),
		 Amount : recObj.getLineItemValue('item','amount',j),
		 TaxRate : recObj.getLineItemValue('item','taxrate1',j),
		 TaxAmount : recObj.getLineItemValue('item','tax1amt',j),
		 rate:recObj.getLineItemValue('item','rate',j),
		 quantity: recObj.getLineItemValue('item','quantity',j)
		 
		};
		dataRow_description.push(json);
		}
		//Search For Invoice Address
			
		
		var taxamount;
		var taxrate;
		
		
		
		
		
		
		
		if(btpl_layout){
		var filters = [];
		filters.push(new nlobjSearchFilter('custrecord_location_1',null,'anyof',btpl_layout));
		var cols = [];
		cols.push(new nlobjSearchColumn('custrecord_btpl_address'));
		cols.push(new nlobjSearchColumn('custrecord_btpl_address_2'));
		
		var searchRec = nlapiSearchRecord('customrecord_btpl_invoice_address',null,filters,cols);
		if(searchRec){
			address_1 = searchRec[0].getValue('custrecord_btpl_address');
			//address_2 = searchRec[0].getValue('custrecord_btpl_address_2');
		}
	}
		//Getting Customer info
		if(customer_Id){
		var customerDetails = nlapiLookupField('customer',customer_Id,[ 'billaddressee','phone','altphone','fax','currency']);
		
		}
		//var data_bill = customerDetails.defaultaddress;
		var cu_phone = customerDetails.phone;
		var billTo_Telephone = cu_phone;
		var cu_altPhone = customerDetails.altphone;
		var cu_fax = customerDetails.fax;
		
		if(cu_altPhone){
			cu_phone = 'Tel#'+' '+cu_phone+' '+ '/' +' '+cu_altPhone;
			
		}
		var rec_Obj_1 = nlapiLoadRecord('customer',customer_Id);
		var customername=rec_Obj_1.getFieldValue('altname');
		var currency_ = rec_Obj_1.getFieldText('currency');
		var address_count = rec_Obj_1.getLineItemCount('addressbook');
		var payment_term=rec_Obj_1.getFieldText('terms');
		//if(address_C_2)
		//	concat_Address = address_C_1 +''+address_C_2+' '+city+','+state+'-'+zipcode+' '+country;
		//	else
		//	concat_Address = address_C_1+''+city+','+state+'-'+zipcode+' '+country;	
		
		
		for(var i=1; i<= 1; i++){
		var address_C_name = rec_Obj_1.getLineItemValue('addressbook','addressee',i);
		var attention_to = rec_Obj_1.getLineItemValue('addressbook','attention',i);
		if(!_logValidation(attention_to)){
			attention_to = '';
		}
		var addrtext = rec_Obj_1.getLineItemValue('addressbook','addrtext',i);
		var obj = addrtext.split('\n');
		var address_C_0 = obj[1];
		var address_C_1 = obj[2];
		var address_C_2 = obj[3];
		var address_C_3 = obj[4];
		if(!_logValidation(address_C_1))
		{
			address_C_1 = '';
		}
		if(!_logValidation(address_C_2))
		{
			address_C_2 = '';
		}
		if(!_logValidation(address_C_3))
		{
			address_C_3 = '';
		}
		
		/*var address_C_4 = obj[5];
		if(_logValidation(address_C_4)){
			concat_Address = address_C_0+','+address_C_1+','+address_C_2+','+address_C_3+','+address_C_4;
		}
		else if(_logValidation(address_C_3)){	
		concat_Address = address_C_0+','+address_C_1+','+address_C_2+','+address_C_3;
		}
		else if(_logValidation(address_C_2)){	
			concat_Address = address_C_0+','+address_C_1+','+address_C_2;
			}*/
		}
		/*if(address_C_1)
		address_C_1 = address_C_1.trim();
		var address_C_2 = rec_Obj_1.getLineItemValue('addressbook','addr2',i);
		if(address_C_2)
		address_C_2 = address_C_2.trim();
		var city = rec_Obj_1.getLineItemValue('addressbook','city',i);
		var dropdownstate = rec_Obj_1.getLineItemValue('addressbook','displaystate',i);
		var zip = rec_Obj_1.getLineItemValue('addressbook','zip',i);
		var attention_to = rec_Obj_1.getLineItemValue('addressbook','attention',i);
		if(address_C_2)
		concat_Address = address_C_1 +''+address_C_2+' '+city+','+dropdownstate+'-'+zip+' '+'India';
		else
		concat_Address = address_C_1+''+city+','+dropdownstate+'-'+zip+' '+'India';	
		}*/
		
		/*for(var i=1; i<= 1; i++){
			var address_cu = rec_Obj_1.getLineItemValue('addressbook','addrtext',i);
			var attention_to = rec_Obj_1.getLineItemValue('addressbook','attention',i);
			}*/
			nlapiLogExecution('DEBUG','Address ',concat_city);
		
		//Get Project name
		if(project_Id){
			var project_Name = nlapiLookupField('job',project_Id,[ 'companyname' ]);
		}
      
        nlapiLogExecution('AUDIT','REC ID ',recId);
		// get the Invoice details
		/*var recObj = nlapiLookupField('invoice', recId, [ 'name',
		        'trandate', 'total']);
		var invoice_number = recObj.name;
		var invoice_date = recObj.trandate;
		var invoice_amt = recObj.total;*/
		
		
			var IRN='';
		var qr_code='000';
		var transactionSearch = nlapiSearchRecord("transaction",null,
														[
														["internalid","anyof",recId], 
														"AND", 
														["mainline","is","T"]
														], 
														[
														new nlobjSearchColumn("custrecord_iit_e_inv_irn","CUSTRECORD_REF_NO",null), 
														new nlobjSearchColumn("custrecord_iit_e_inv_qr","CUSTRECORD_REF_NO",null)

														]
												 );

		if(transactionSearch)
				{
				IRN=transactionSearch[0].getValue("custrecord_iit_e_inv_irn","CUSTRECORD_REF_NO",null)
				nlapiLogExecution('DEBUG','IRN',IRN);

				if(_nullValidation(IRN))
				{
				IRN = '';
				}



				var qr_code= transactionSearch[0].getValue("custrecord_iit_e_inv_qr","CUSTRECORD_REF_NO",null)
				nlapiLogExecution('DEBUG','qr_code',qr_code);

				if(_nullValidation(qr_code))
				{
				qr_code = '000';
				}

		}
		var url="https://api.qrserver.com/v1/create-qr-code/?data="+qr_code+"&amp;size=202x170";
	
		
	//Generate PDF Starts here
	var comp_imgrsrc = 'https://system.na1.netsuite.com/core/media/media.nl?id=6237&c=3883006&h=f9681f3581e51644773b';
	comp_imgrsrc = nlapiEscapeXML(comp_imgrsrc);
	
	var xmlContent = "";
//	xmlContent += "<table width='100%' height='100%' >";

	//xmlContent += "<tr>";
	//xmlContent += "<td>";

/*	xmlContent += "<table  width='100%' height='100%'>";
	xmlContent += "<tr>";
	xmlContent += "<td  style='border:0px solid black;'>";
	xmlContent += "<img style='padding-top:0px; width:60px; height:60px;' class='companyLogo' src='"
	        + comp_imgrsrc + "'/>";
	xmlContent += "</td>";

	xmlContent += "<td  style='border:0px solid black;' font-size=\"9\">";
	xmlContent += "<p align='right'>5th Floor, Thejaswini, Technopark,<br/>"
		        + "Trivandrum, Kerala<br/>"
		        + "T +91 471 407 4000 <br/> "
				+ "PIN 695 581<br/>"
		        + "www.brillio.com<br/></p>";
	xmlContent += "</td>";
	xmlContent += "</tr>";
	
	
	xmlContent += "</table>";
	
	//var data_bill = customerDetails.defaultaddress;
	var cu_phone = customerDetails.phone;
	var billTo_Telephone = cu_phone;
	var cu_altPhone = customerDetails.altphone;
	var cu_fax = customerDetails.fax;
	if(cu_altPhone){
		cu_phone = 'Tel#'+' '+cu_phone+' '+ '/' +' '+cu_altPhone;
		
	}
	xmlContent += "</td>";
	xmlContent += "</tr>";
	<b>Sunil BalVally / A Balaji</b><br/>" 
			   + "<b>M/s. Piramal Enterprises Ltd.,</b><br/>"
			   + "<b>247 Business Park, A-Wing, 6th Floor,</b><br/>"
			   + "<b>LBS Marg, Vikhroli (West)</b><br/>"
			   + "<b>Mumbai, Maharastra 400089</b><br/>"
			   + "<b>Tel #022 - 39536955 / 9967554920</b><br/>
	
	xmlContent += "</table>";
	//NewLines
		
	xmlContent += "<table width='30%' >";
	xmlContent += "<tr>";
	xmlContent += "<td style='border:0px solid black;' >";
	xmlContent += "<p class='go-right' style='margin-left:10px;' font-size=\"12\"><b>  "+ nlapiEscapeXML(address_cu) +"</b><br/>"
			   +"<b>"+ cu_phone +"</b></p>";
			   
	xmlContent += "</td>";
	xmlContent += "</tr>";
	xmlContent += "</table>";
	//New Line-2
	xmlContent += "<table align='right'>";
	xmlContent += "<tr>";
	xmlContent += "<td style='border:0px solid black;'>";
	xmlContent += "<p class='go-right' style='margin-right:0px;padding-top:20px;' font-size=\"10\">BTPL/FA/134/2016-17<br/>"
			   + invoice_date+ "<br/></p>"
	xmlContent += "</td>";
	
	xmlContent += "<td style='border:0px solid black;'>";
	xmlContent += "<p class='go-right' style='margin-right:0px;padding-top:20px;'>"+ invoice_number +"<br/>" 
			   + invoice_date+ "<br/></p>"
			   
	xmlContent += "</td>";
	xmlContent += "</tr>";
	xmlContent += "</table>";
	
	//New Line-3
	xmlContent += "<table>";
	xmlContent += "<tr>";
	xmlContent += "<td style='border:0px solid black;'>";
	xmlContent += "<p class='go-right' style='margin-right:-110px;padding-top:20px;'>Dear Sir / Madam,<br/></p>";
			   
	xmlContent += "</td>";
	xmlContent += "</tr>";
	xmlContent += "</table>";
	
	
	//-4
	xmlContent += "<table>";
	xmlContent += "<tr>";
	xmlContent += "<td style='border:0px solid black;'>";
	xmlContent += "<p class='go-right' style='margin-right:-110px;padding-top:20px;'>We are enclosing herewith following invoice towards Product support and Consulting,<br/>" 
			   +"Contract charges for OptiSuite Modules <br/></p>";
			   
	xmlContent += "</td>";
	xmlContent += "</tr>";
	xmlContent += "</table>";
	
	xmlContent +="<br/>";
	
	xmlContent += "<TABLE BORDER='1' align='center'  WIDTH='70%'   >"; //CELLPADDING='4' CELLSPACING='3'
	xmlContent += "<TR>";
	xmlContent += "<TH border='0.5'>Invoice No.</TH>";
	xmlContent += "<TH border='0.5'>Date</TH>";
	xmlContent += "<TH border='0.5'>Amount(Rs)</TH>";
	xmlContent += "</TR>";
	xmlContent += "<TR>";
	xmlContent += "<TD border='0.5'>"+ invoice_number +"</TD>";
	xmlContent += "<TD border='0.5'>"+ invoice_date +"</TD>";
	xmlContent += "<TD border='0.5'>"+ invoice_amt +"</TD>";
	xmlContent += "</TR>";
	xmlContent += "</TABLE>";
	
	//-5
	xmlContent += "<table>";
	xmlContent += "<tr>";
	xmlContent += "<td style='border:0px solid black;'>";
	xmlContent += "<p class='go-right' style='margin-right:-110px;padding-top:20px;'>Please acknowledge the receipt and send us the payment at the earliest. Payment<br/>" 
			   +"may be made through elctronic transfer.<br/></p>";
			   
	xmlContent += "</td>";
	xmlContent += "</tr>";
	xmlContent += "</table>";
	
	//-6
	xmlContent += "<table>";
	xmlContent += "<tr>";
	xmlContent += "<td style='border:0px solid black;'>";
	xmlContent += "<p class='go-right' style='margin-right:-110px;padding-top:20px;'><b><u>Our RTGS / NEFT code is BOFA0BG3978 with Bank of America NA - Account</u></b><br/>" 
			   +"<b><u>Nummber. 70106035</u></b></p>";
			   
	xmlContent += "</td>";
	xmlContent += "</tr>";
	xmlContent += "</table>";
	
	//-7
	xmlContent += "<table>";
	xmlContent += "<tr>";
	xmlContent += "<td style='border:0px solid black;'>";
	xmlContent += "<p class='go-right' style='margin-right:-110px;padding-top:20px;'>Thank You,</p>";

	xmlContent += "</td>";
	xmlContent += "</tr>";
	xmlContent += "</table>";
	
	//-8 Yours Truly
	xmlContent += "<table>";
	xmlContent += "<tr>";
	xmlContent += "<td style='border:0px solid black;'>";
	xmlContent += "<p class='go-right' style='margin-right:-110px;padding-top:20px;'>Yours truly,</p>";

	xmlContent += "</td>";
	xmlContent += "</tr>";
	xmlContent += "</table>";
	
	
	
	//-9 
	xmlContent += "<table>";
	xmlContent += "<tr>";
	xmlContent += "<td style='border:0px solid black;'>";
	xmlContent += "<p class='go-right' style='margin-right:-110px;padding-top:20px;'>For <b>Brillio Technologies Pvt Ltd</b></p>";

	xmlContent += "</td>";
	xmlContent += "</tr>";
	xmlContent += "</table>";

	//Creating Blank
	xmlContent += "<table>";
	xmlContent += "<tr>";
	xmlContent += "<td style='border:0px solid black;'>";
	xmlContent +="<br/>";
	xmlContent +="<br/>";
	xmlContent += "</td>";
	xmlContent += "</tr>";
	xmlContent += "</table>";
	xmlContent += "<table>";
	xmlContent += "<tr>";
	xmlContent += "<td style='border:0px solid black;'>";
	xmlContent += "<p class='go-right' style='margin-right:-110px;padding-top:20px;'><b>Authorized Signatory</b></p>";

	xmlContent += "</td>";
	xmlContent += "</tr>";
	xmlContent += "</table>";
	
	//-8 For Brillio Technologies Pvt Ltd
	xmlContent += "<table>";
	xmlContent += "<tr>";
	xmlContent += "<td style='border:0px solid black;' font-size=\"9\">";
	xmlContent += "<p class='go-right' style='margin-right:-110px;padding-top:20px;'><b>NB: As per the Govt. of India Notification No.402/92/2006-MC (27 of 2010)/02nd June 2010, we request you to please</b><br/>"
				+"<b>issue the TDS Certificate along with the above amount and ensure correct credit in 26AS.</b></p>";
	xmlContent += "</td>";
	xmlContent += "</tr>";
	xmlContent += "</table>";
//
	xmlContent += "<table>";
	xmlContent += "<tr height=\"30px\">";
	xmlContent += "<td WIDTH=\"100%\" colspan='2'>";//border-right='0.5px' border-left='0.5px'
	
	xmlContent += "</td>";
	xmlContent += "</tr>";
	
	xmlContent +="</table>";
	//10
	xmlContent += "<table width='100%'>";
	xmlContent += "<tr>";
	xmlContent += "<td width='75%' align='center' font-size='7'>Regd. Office: Brillio Technologies Pvt. Ltd., 4A Block, 5th Floor, PECP Park, Bellandur Village, Marathahalli</td>"
	xmlContent += "</tr>";
	xmlContent += "<tr>";
	xmlContent += "<td width='100%' align='center' font-size='7'>Outer Ring Road, Varthur Hobli, Bangalore - 560037 CIN U22190KA199FTC022250</td>";
	//xmlContent += "</td>";
	xmlContent += "</tr>";
	xmlContent += "</table>";
	
	//xmlContent += "</td>";
	//xmlContent += "</tr>";
	//xmlContent += "</table>";
	xmlContent +="<br/>"; */
	
	//Next Page
	xmlContent +="<table border='0.5' width=\"100%\">";
	xmlContent +="<tr>";
	xmlContent += "<td width=\"50%\" style='border:0px solid black;' font-family=\"Helvetica\" font-size=\"9\">";
	xmlContent += "<p class='go-right' style='margin-left:10px;' font-size='9'><b>Brillio Technologies Pvt Ltd</b><br/>" 
			   + ""+address_1+"<br/>"
			   +" "+address_3+"<br/>"
			  // + "<br/>"
			   + "<b>GSTIN No : 29AABCP2354A1ZT</b></p>";
	xmlContent += "</td>";
	
	xmlContent += "<td align ='right' width =\"50%\" style='border:0px solid black;'>";
	xmlContent += "<img height=\"50\" width=\"60\" src='"
	        + comp_imgrsrc + "'/>"; //width:40px;  //class='companyLogo' style='padding-top:0px;
	xmlContent += "</td>";
	
	xmlContent += "</tr>";
	
	xmlContent +="<tr border-bottom='0.5'>";
	xmlContent +="<td font-family=\"Helvetica\" font-size=\"21\" colspan='2'><b>Invoice</b></td>"; //border-bottom='1px solid black'
	xmlContent += "</tr>";
	//xmlContent +="</table>";
	

	/*xmlContent += "<tr height=\"30px\">";
	xmlContent += "<td border-bottom='0.5px solid black' WIDTH=\"100%\" colspan='2'>";//border-right='0.5px' border-left='0.5px'
	
	xmlContent += "</td>";
	xmlContent += "</tr>";*/
	/*xmlContent += "<tr>";
	xmlContent += "<td>";
	//xmlContent += "<hr width='100%' border-bottom='1px solid black' />";
	xmlContent += "</td>";
	xmlContent += "</tr>";*/
	//xmlContent +="</table>";
	
	//xmlContent +="</table>";
	
	//xmlContent +="<table border='1'  WIDTH='100%'>";

	
	xmlContent +="</table>";
	
	
		xmlContent += "<table width=\"100%\" >";
     xmlContent += "<tr>";
	xmlContent +="<td style=\"border-left: 0.1px solid;border-bottom: 0.1px solid;border-top: 0.1px solid;border-right: 0.1px solid;\"  WIDTH=\"50%\" font-family=\"Helvetica\" font-size=\"8\"><b>Invoice No.:</b>"
			   + invoice_number +"</td>";
	//xmlContent += "<hr width='100%'/>";		
	
		
      //xmlContent += "	<tr>";
		//xml += "		<td align=\"left\" width=\"1%\"  style=\"border-left: 0.1px solid;border-bottom: 0.1px solid;border-top: 0.1px solid;border-right: 0.1px solid;\" font-size=\"0\">&nbsp;<\/td>";
		xmlContent += "		<td align=\"left\" width=\"52%\"  style=\"border-left: 0.1px solid;border-bottom: 0.1px solid;border-top: 0.1px solid;border-right: 0.1px solid;\" font-size=\"8\"><b>Invoice Date :</b>"+nlapiEscapeXML(invoice_formated_date)+"<\/td>";
		xmlContent += "	<\/tr>";
		
		xmlContent += "	<tr>";
      
      xmlContent +="<td rowspan=\"2\" style=\"border-left: 0.1px solid;border-bottom: 0.1px solid;border-top: 0.1px solid;border-right: 0.1px solid;\"  WIDTH=\"50%\" font-family=\"Helvetica\" font-size=\"8\"><b>Billing Description :</b>"
			   + billingdescription +"</td>";
		//xml += "		<td align=\"left\" width=\"1%\"  style=\"border-left: 0.1px solid;border-bottom: 0.1px solid;border-top: 0.1px solid;border-right: 0.1px solid;\" font-size=\"0\">&nbsp;<\/td>";
		//xml += "		<td align=\"left\" width=\"29%\"  style=\"border-left: 0.1px solid;border-bottom: 0.1px solid;border-top: 0.1px solid;border-right: 0.1px solid;\" font-size=\"6\">Duplicate for Supplier/Transporter<\/td>";
		xmlContent += "		<td align=\"left\" width=\"52%\"  style=\"border-left: 0.1px solid;border-bottom: 0.1px solid;border-top: 0.1px solid;border-right: 0.1px solid;\" font-size=\"8\"><b>IRN:</b>"+nlapiEscapeXML(IRN)+"<\/td>";
		xmlContent += "	<\/tr>";
		
		xmlContent += "	<tr>";
		//xml += "		<td align=\"left\" width=\"1%\"  style=\"border-left: 0.1px solid;border-bottom: 0.1px solid;border-top: 0.1px solid;border-right: 0.1px solid;\" font-size=\"0\">&nbsp;<\/td>";
		//xml += "		<td align=\"left\" width=\"29%\"  style=\"border-left: 0.1px solid;border-bottom: 0.1px solid;border-top: 0.1px solid;border-right: 0.1px solid;\" font-size=\"6\">Duplicate for Supplier/Transporter<\/td>";
		xmlContent += "		<td align=\"left\" width=\"52%\"  style=\"border-left: 0.1px solid;border-bottom: 0.1px solid;border-top: 0.1px solid;border-right: 0.1px solid;\" font-size=\"8\"><b>PO# :</b>"+nlapiEscapeXML(memo)+"<\/td>";
		xmlContent += "	<\/tr>";
		xmlContent += "<\/table>";
      
      
	
	//Transaction Details
	xmlContent +="<table border='0.5'  WIDTH=\"100%\">";
	xmlContent += "<tr>";
	/*xmlContent += "<td   WIDTH=\"30%\">";
	xmlContent += "<p class='go-right' style='margin-left:10px;' ><b>Bill To</b><br/><br/>" 	
				//+"<br/>"
				 +"<b> "+ nlapiEscapeXML(address_cu) +" </b><br/></p>";
	
	xmlContent += "<p align='left' font-size='11'  ><br/>" //width:15%;
		   +"<br/>"
		  // +"<br/>"
		   +"<b>Telephone No:"+ billTo_Telephone +"</b></p>";
	
	
	//xmlContent +="<p style='text-align: left;'>LEFT</p>";
	//xmlContent +="<p style='text-align: right; width='25%';>RIGHT</p>";
	//xmlContent += "<p align='center' ><b>Fax No:</b></p>" //width:15%;style='padding-right:50px;'
		  

	
	xmlContent += "</td>";
	
	xmlContent += "<td border-right='0.5'  WIDTH=\"30%\" font-size='11'>"; 
	xmlContent +="<p align='right' font-size='11' ><b></b><br/>" //class='go-right' style='margin-left:-30px;'
		+"<br/>"
		+"<br/>"
		+"<br/>"
		+"<br/>"
		+"<br/>"
		+"<br/>"
		+"<br/>"
		+""
		
		+"<b>Fax No:"+ cu_fax +"</b></p>";
	xmlContent +="</td>";*/
	
	//New Change
	xmlContent +="<td width=\"48.99%\" font-family=\"Helvetica\" font-size=\"9\">";
	//xmlContent +="<br></br>";
	xmlContent += "<p><b>Bill To:</b></p>" 
	
	//xmlContent +="</td>";
	//xmlContent +="<br/>";
	//xmlContent +="<td  WIDTH='25%' >"
	xmlContent += "<b>"+ nlapiEscapeXML(customername) +"</b><br/>";
	xmlContent +=  nlapiEscapeXML(billadd) +"<br/>";
	//xmlContent += "<p><b>"+ nlapiEscapeXML(address_C_name) +"</b></p>";
	xmlContent +=  nlapiEscapeXML(billadd1) +"<br/>";
if(_logValidation(concat_city))
{	
	nlapiLogExecution('DEBUG','CITY',concat_city);
	if(concat_city != ','){
	xmlContent +=  nlapiEscapeXML(concat_city)+"<br/>" ;
	}
}

if(concat_state != ','){
	xmlContent +=  nlapiEscapeXML(concat_state) ;
}
	//xmlContent +="<br/>";
	//xmlContent += "<p></p>";
	//xmlContent +="<br/>";
		xmlContent += "<p>Contact Person: "+ nlapiEscapeXML(attention_to) +"</p>";	
	//xmlContent += "<p><b>Telephone No:"+ billTo_Telephone +"</b></p>";
	xmlContent +="</td>";
	
	/*xmlContent +="<td width='45%' font-family=\"Helvetica\" font-size=\"9\" >";//width='52%'border=\"0.5\"
	xmlContent +="<br/>";
	xmlContent +="<br/>";
	xmlContent +="<br/>";
	xmlContent +="<br/>";
	xmlContent +="<br/>";
	xmlContent +="<br/>";
	xmlContent +="<br/>";
	xmlContent +="<br/>";
	//xmlContent +="<td  WIDTH='25%' >"
	//xmlContent += "<p><b>Fax No:"+ cu_fax +"</b></p>";
	xmlContent +="</td>";*/
	//End od new Change
	
	xmlContent +="<td width=\"20%\" font-family=\"Helvetica\" font-size=\"9\" border-left='0.5' align = 'left'>"; //border=\"0.5\"
	//xmlContent +="<br></br>";
	xmlContent += "<p><b>Remittance Details:</b></p>" 
	//xmlContent +="</td>";
	//xmlContent +="<br/>";
	//xmlContent +="<td  WIDTH='25%' >"
	xmlContent += "<p><b>Bank Name</b><br/>" //width:15%;
			   +"<b>Address</b><br/>"
			   +"<br/>"
			   +"<br/>"
			   +"<b>Bank Account No.</b><br/>"
			   +"SWIFT CODE<br/>"
			   +"CIN<br/></p>";
	xmlContent +="</td>";
	
	xmlContent +="<td width='30px' font-family=\"Helvetica\" font-size=\"9\" align = 'left'>";//width='52%'
	
	xmlContent +="<br/>";
	//xmlContent +="<td  WIDTH='25%' >"
	xmlContent += "<p><b>Kotak Mahindra Bank Ltd</b><br/>" 
				+"<b>#27, Santosh Towers,<br/> J.P.Nagar 4th Phase, 100ft Ring Road, <br/> Bangalore - 560078</b><br/>"
			   +"<b>1511696478</b><br/>"
			   +"KKBKINBBCPC<br/>"
			   +"U22190KA1997FTC022250<br/></p>";
	xmlContent +="</td>";
	xmlContent += "</tr>";
	
	xmlContent +="</table>";
	/*xmlContent +="<td  WIDTH='50%' >"
		xmlContent += "<p class='go-right' style='margin-left:10px;'><b>Bank Name</b><br/>" 
				   +"<b>Bank Account Number</b><br/>"
				   +"IFSC Code<br/>"
				   +"Service Tax Reg No<br/>"
				   +"Service Tax Category<br/>"
				   +"S T Classification u/s<br/>"
				   +"PAN NO<br/>"
				   +"CIN<br/></p>";
		xmlContent +="</td>";
	//xmlContent += "<hr width='100%'/>";		
	xmlContent += "</tr>";
	
	xmlContent +="</table>";*/

	xmlContent +="<table border='0.5' WIDTH='100%'>";
	xmlContent += "<tr>";
	xmlContent +="<td  border-bottom='0.5' WIDTH='30%' font-family=\"Helvetica\" font-size=\"9\"><b> Memo</b></td>";
	xmlContent +="<td border-bottom='0.5' WIDTH='70%' font-family=\"Helvetica\" font-size=\"9\">"+memo+"</td>";
	xmlContent += "</tr>";
	/*xmlContent += "<tr>";
	xmlContent +="<td border-right='0.5' border-bottom='0.5' WIDTH='30%' font-family=\"Helvetica\" font-size=\"9\"><b>Terms and Conditions</b></td>";
	xmlContent +="<td border-bottom='0.5' WIDTH='70%' font-family=\"Helvetica\" font-size=\"9\">"
	xmlContent += "<p class='go-right' style='margin-left:10px;'>1) Payment to be made within 15 days from the date of receipt of invoice<br/>"
		       +"2) Interest @18% will be charged on the bill if not paid with in due date<br/>"
		       +"3) All disputes subject to Bangalore Jurisdiction only.<br/></p>";
	
	xmlContent +="</td>";
	xmlContent += "</tr>";*/
	
	xmlContent += "<tr>";
	xmlContent +="<td  WIDTH='10%' font-family=\"Helvetica\" font-size=\"9\"><b>Payment Term </b></td>";
	xmlContent +="<td WIDTH='90%' font-family=\"Helvetica\" font-size=\"9\">"
	xmlContent +=  nlapiEscapeXML(payment_term) ;
	
	xmlContent +="</td>";
	xmlContent += "</tr>";
	
	/*xmlContent += "<tr height=\"30px\">";
	xmlContent += "<td border-bottom='0.5px solid black' WIDTH=\"100%\" colspan='2'>";//border-right='0.5px' border-left='0.5px'
	
	xmlContent += "</td>";
	xmlContent += "</tr>";*/
	
	xmlContent +="</table>";
	
//Creating Sub Table
	
	
	xmlContent +="<table border='0.5' width='100%' height='100%'>";
	xmlContent +="<tr>";
	xmlContent +="<td style='padding-top:10px;' border-right='0.5' border-bottom='0.5' width='50%' rowspan='2' align = 'center' font-family=\"Helvetica\" font-size=\"9\"><b>Name</b></td>";
	//xmlContent +="<td style='padding-top:10px;' border-right='0.5' border-bottom='0.5' width='35%' rowspan='2' align = 'center' font-family=\"Helvetica\"  font-size=\"9\"><b>Billing Description</b></td>";
	xmlContent +="<td border-right='0.5' border-bottom='0.5' width='20%' colspan='2' align = 'center' font-family=\"Helvetica\" font-size=\"9\"><b>Billing Period</b></td>";
	xmlContent +="<td style='padding-top:10px;' border-right='0.5' border-bottom='0.5' width='35%' rowspan='2' align = 'center' font-family=\"Helvetica\"  font-size=\"9\"><b>No. of<br/> Units Billed</b></td>";
	xmlContent +="<td style='padding-top:10px;' border-right='0.5' border-bottom='0.5' width='10%' rowspan='2' align = 'center' font-family=\"Helvetica\" font-size=\"9\"><b>Rate</b></td>";
	//xmlContent +="<td style='padding-top:10px;' border-right='0.5' border-bottom='0.5' width='35%' rowspan='2' align = 'center' font-family=\"Helvetica\"  font-size=\"9\"><p><b>Unit</b><br/><b>of Measures</b></p></td>";
	xmlContent +="<td border-bottom='0.5' width='35%' align='center' rowspan='2' font-family=\"Helvetica\" font-size=\"9\"><b>Amount</b></td>";
	
	//Breaks
	xmlContent +="</tr>";
	
	xmlContent +="<tr>";
	
	
	xmlContent +="<td  border-bottom='0.5' width='10%' font-family=\"Helvetica\" font-size=\"9\" ><b>From</b></td>";
	xmlContent +="<td  border-left='0.5' border-bottom='0.5' border-right='0.5' width='10%' font-family=\"Helvetica\" font-size=\"9\" ><b>To</b></td>";
	
	
	
	
	//Breaks
	xmlContent +="</tr>";
	
	//xmlContent +="</table>";
	//xmlContent +="<table border='1' width='100%'>";
	xmlContent +="<tr>";
	xmlContent +="<td  border-right='0.5' width='50%' align='left'></td>";
//	xmlContent +="<td  border-right='0.5' width='35%' align = 'center'></td>";
	xmlContent +="<td  border-right='0.5' width='20%'></td>";
	xmlContent +="<td  border-right='0.5' width='10%' ></td>";
	xmlContent +="<td  border-right='0.5' width='10%'></td>";
//	xmlContent +="<td  border-right='0.5' width='10%'></td>";
	xmlContent +="<td border-right='0.5' width='35%' align='right'></td>";
	
	xmlContent +="</tr>";
	
	xmlContent +="<tr>";
	xmlContent +="<td  border-right='0.5' width='50%' align='left'></td>";
	//xmlContent +="<td  border-right='0.5' width='35%' align = 'center'></td>";
	xmlContent +="<td  border-right='0.5' width='20%'></td>";
	xmlContent +="<td  border-right='0.5' width='10%' ></td>";
	xmlContent +="<td  border-right='0.5' width='10%'></td>";
//	xmlContent +="<td  border-right='0.5' width='10%'></td>";
	xmlContent +="<td border-right='0.5' width='35%' align='right'></td>";
	
	xmlContent +="</tr>";
		
	//xmlContent +="</tr>";
	//xmlContent +="</table>";
	//xmlContent +="<table border='1' width='100%'>";
	
//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------	
	var i_recordID = '2433116';
	//'2433116'
    var o_recordOBJ = nlapiLoadRecord('invoice', i_recordID);
	
	
	var i_item = o_recordOBJ.getLineItemValue('item',
						        'item', 1)

				var i_customer = o_recordOBJ.getFieldValue('entity');
				nlapiLogExecution('DEBUG', 'suiteletFunction', ' Customer -->'
				        + i_customer);

				if (_logValidation(i_customer)) {
					var o_customerOBJ = nlapiLoadRecord('customer', i_customer);

					if (_logValidation(o_customerOBJ)) {
						i_customer_name = o_customerOBJ
						        .getFieldValue('altname')
						// nlapiLogExecution('DEBUG', 'suiteletFunction','
						// Customer Name -->' + i_customer_name);

					}// Customer OBJ
				}// Customer

				var i_customer_txt = o_recordOBJ.getFieldText('entity');
				// nlapiLogExecution('DEBUG', 'suiteletFunction',' Customer Text
				// -->' + i_customer_txt);

				var i_tran_date = o_recordOBJ.getFieldValue('trandate')
				// nlapiLogExecution('DEBUG', 'suiteletFunction', ' Tran Date
				// --> ' + i_tran_date);

				var i_tran_id = o_recordOBJ.getFieldValue('tranid')
				// nlapiLogExecution('DEBUG', 'suiteletFunction', ' Invoice
				// Number --> ' + i_tran_id);

				if (!_logValidation(i_tran_id)) {
					i_tran_id = ''
				}

				var i_bill_address = nlapiEscapeXML(o_recordOBJ
				        .getFieldValue('billaddress'))
				// nlapiLogExecution('DEBUG', 'suiteletFunction', ' Bill Address
				// --> ' + i_bill_address);
				// i_bill_address =
				// formatAndReplaceSpacesofMessage(nlapiEscapeXML(i_bill_address));

				if (!_logValidation(i_bill_address)) {
					i_bill_address = ''
				}

				var i_PO_ID = o_recordOBJ.getFieldValue('otherrefnum')
				// nlapiLogExecution('DEBUG', 'suiteletFunction', ' PO # --> ' +
				// i_PO_ID);

				if (!_logValidation(i_PO_ID)) {
					i_PO_ID = ''
				}
				var i_terms = o_recordOBJ.getFieldText('terms')
				// nlapiLogExecution('DEBUG', 'suiteletFunction', ' Terms --> '
				// + i_terms);

				if (!_logValidation(i_terms)) {
					i_terms = ''
				}
				var s_memo = o_recordOBJ.getFieldValue('memo')
				// nlapiLogExecution('DEBUG', 'suiteletFunction', ' Memo --> ' +
				// s_memo);

				if (!_logValidation(s_memo)) {
					s_memo = ''
				}

				var i_discount = o_recordOBJ.getFieldValue('discounttotal')
				// nlapiLogExecution('DEBUG', 'suiteletFunction', ' Discount -->
				// ' + i_discount);

				if (!_logValidation(i_discount)) {
					i_discount = 0
				}

				var i_bill_to = o_recordOBJ.getFieldValue('custbody_billto')
				// nlapiLogExecution('DEBUG', 'suiteletFunction', ' Bill To -->
				// ' + i_bill_to);

				if (!_logValidation(i_bill_to)) {
					i_bill_to = ''
				}
				var i_bill_from = o_recordOBJ
				        .getFieldValue('custbody_billfrom')
				nlapiLogExecution('DEBUG', 'suiteletFunction', ' Bill From --> ' + i_bill_from);

				if (!_logValidation(i_bill_from)) {
					i_bill_from = ''
				}
				var i_billing_description = o_recordOBJ
				        .getFieldValue('custbody_billingdescription');
				 nlapiLogExecution('DEBUG', 'suiteletFunction', ' Bill Description --> ' + i_billing_description);

				if (!_logValidation(i_billing_description)) {
					i_billing_description = ''
				}

				var i_service_tax_category = o_recordOBJ
				        .getFieldText('custbody_servicetaxcategory')
				// nlapiLogExecution('DEBUG', 'suiteletFunction', ' Service Tax
				// Category --> ' + i_service_tax_category);

				if (!_logValidation(i_service_tax_category)) {
					i_service_tax_category = ''
				}
				var i_ST_Classification = o_recordOBJ
				        .getFieldText(' custbody_stclassificationus')
				// nlapiLogExecution('DEBUG', 'suiteletFunction', ' ST
				// Classification U/S --> ' + i_ST_Classification);

				if (!_logValidation(i_ST_Classification)) {
					i_ST_Classification = ''
				}
				var i_tax = o_recordOBJ.getFieldValue('taxtotal')
				// nlapiLogExecution('DEBUG', 'suiteletFunction', ' Tax --> ' +
				// i_tax);

				if (!_logValidation(i_tax)) {
					i_tax = 0
				}
	
	
	
	var i_discount_percent = parseFloat(i_discount) / 100;

				var i_tax_percent = parseFloat(i_tax) / 100;

				i_discount_percent = parseFloat(i_discount_percent).toFixed(2);
				i_tax_percent = parseFloat(i_tax_percent).toFixed(2);
	
	
	
	var i_billable_time_count = o_recordOBJ.getLineItemCount('time');
		
		var s_currency_symbol = '';
		var i_currency = o_recordOBJ.getFieldText('currency')
		var discount_count = o_recordOBJ.getLineItemCount('item');
		var discount = o_recordOBJ.getLineItemValue('item',
				        'description', 1);
				nlapiLogExecution('DEBUG', 'suiteletFunction', ' discount --> '
				        + discount);				
				var discount_amt = o_recordOBJ.getLineItemValue('item',
				        'amount', 1);
				nlapiLogExecution('DEBUG', 'suiteletFunction',
				        ' discount_amt --> ' + discount_amt);

				if (!_logValidation(i_currency)) {
					i_currency = '';
				}
				if (i_currency == 'USD') {
					s_currency_symbol = '$'
				}
				if (i_currency == 'INR') {
					s_currency_symbol = 'Rs.'
				}
				if (i_currency == 'GBP') {
					s_currency_symbol = '�'
				}
				if (i_currency == 'SGD') {
					s_currency_symbol = 'S$'
				}
				if (i_currency == 'Peso') {
					s_currency_symbol = 'P'
				}
				if (i_currency == 'EUR') {
					s_currency_symbol = '€'
				}
		
		
		
		if (_logValidation(i_billable_time_count)) {
					for (var i = 1; i <= i_billable_time_count; i++) {
						var i_date = o_recordOBJ.getLineItemValue('time',
						        'billeddate', i)

						var i_employee = o_recordOBJ.getLineItemValue('time',
						        'employeedisp', i)

						 i_project = o_recordOBJ.getLineItemValue('time',
						        'job', i)

						var i_item = o_recordOBJ.getLineItemValue('time',
						        'item', i)

						var i_rate = o_recordOBJ.getLineItemValue('time',
						        'rate', i)

						var i_hours = o_recordOBJ.getLineItemValue('time',
						        'quantity', i)

						var i_marked = o_recordOBJ.getLineItemValue('time',
						        'apply', i)

						/*
						 * nlapiLogExecution('DEBUG', 'suiteletFunction', ' Date
						 * --> ' + i_date);
						 * 
						 * nlapiLogExecution('DEBUG', 'suiteletFunction', '
						 * Employee --> ' + i_employee);
						 * 
						 * nlapiLogExecution('DEBUG', 'suiteletFunction', '
						 * Project --> ' + i_project);
						 * 
						 * nlapiLogExecution('DEBUG', 'suiteletFunction', ' Item
						 * --> ' + i_item);
						 * 
						 * nlapiLogExecution('DEBUG', 'suiteletFunction', ' Rate
						 * --> ' + i_rate);
						 */

						// nlapiLogExecution('DEBUG', 'suiteletFunction', '
						// Hours --> ' + i_hours);
						if (i_marked == 'T') {
							var d_start_of_week = get_start_of_week(i_date)
							var d_end_of_week = get_end_of_week(i_date)

							var d_SOW_date = nlapiStringToDate(d_start_of_week);
							var d_EOW_date = nlapiStringToDate(d_end_of_week);

							d_SOW_date = get_date(d_SOW_date)

							d_EOW_date = get_date(d_EOW_date)

							// nlapiLogExecution('DEBUG', 'suiteletFunction', '
							// d_SOW_date --> ' + d_SOW_date);
							// nlapiLogExecution('DEBUG', 'suiteletFunction', '
							// d_EOW_date --> ' + d_EOW_date);

							if (i_item == 2222 || i_item == 2425
							        || i_item == 2221) // Amol: added 2221(FP)
							// item
							{
								a_employee_array[i_cnt++] = i_employee + '&&&'
								        + d_EOW_date + '&&&' + d_SOW_date
								        + '&&&' + i_rate;
								a_employee_name_array.push(i_employee);
							}// Time & Material

							// nlapiLogExecution('DEBUG', 'suiteletFunction', '
							// *******************Start & End Of Week
							// *************** --> [' + d_SOW_date + ' , ' +
							// d_EOW_date + ']');
						}

					}// Billable Loop
				}// Billable Line Count Loop
				
				a_employee_array = removearrayduplicate(a_employee_array)
				a_employee_array = a_employee_array.sort();
				// nlapiLogExecution('DEBUG', 'suiteletFunction', '
				// ******************* Employee Array *************** -->'
				// +a_employee_array);

				a_employee_name_array = removearrayduplicate(a_employee_name_array)
				a_employee_name_array = a_employee_name_array.sort();
				// nlapiLogExecution('DEBUG', 'suiteletFunction', '
				// ******************* Employee Name Array *************** -->'
				// +a_employee_array);

				var i_employee_arr_n;
				var vb = 0;
				var emp_total_hr = 0;
				var emp =0;
				var emp_total_amt = 0;
				var emp_count = 0;
				for (var k = 0; k < a_employee_array.length; k++) {
					var i_ST_rate = 0;
					var i_OT_rate = 0;
					var i_ST_units = 0;
					var i_OT_units = 0;
					var i_hours = 0
					var i_minutes = 0;
					var i_hours_ST = 0;
					var i_minutes_ST = 0;
					var i_hours_OT = 0;
					var i_minutes_OT = 0;
					var flag = 0;
					
					

					a_split_array = a_employee_array[k].split('&&&')

					var i_employee_arr = a_split_array[0]

					var i_end_of_week_arr = a_split_array[1]

					var i_start_of_week_arr = a_split_array[2]

					var i_rate_emp = a_split_array[3]

					for (var lt = 1; lt <= i_billable_time_count; lt++) {
						var i_date_new = o_recordOBJ.getLineItemValue('time',
						        'billeddate', lt)

						var i_employee_new = o_recordOBJ.getLineItemValue(
						        'time', 'employeedisp', lt)

						var i_project_new = o_recordOBJ.getLineItemValue(
						        'time', 'job', lt)

						var i_item_new = o_recordOBJ.getLineItemValue('time',
						        'item', lt)

						var i_rate_new = o_recordOBJ.getLineItemValue('time',
						        'rate', lt)

						var i_hours_new = o_recordOBJ.getLineItemValue('time',
						        'quantity', lt)

						var i_marked = o_recordOBJ.getLineItemValue('time',
						        'apply', lt)

						if (i_marked == 'T')
						{
							if ((i_employee_arr == i_employee_new)) {
								var d_start_of_week_new = get_start_of_week(i_date_new)
								var d_end_of_week_new = get_end_of_week(i_date_new)

								/*
								 * nlapiLogExecution('DEBUG',
								 * '-----------------------------------------------------');
								 * nlapiLogExecution('DEBUG', 'i_date_new', ' 1
								 * --> ' + i_date_new);
								 * nlapiLogExecution('DEBUG',
								 * 'suiteletFunction', ' 1 --> ' +
								 * d_SOW_date_B); nlapiLogExecution('DEBUG',
								 * 'suiteletFunction', ' 2 --> ' +
								 * d_EOW_date_B); nlapiLogExecution('DEBUG',
								 * 'i_hours_new', ' 2 --> ' + i_hours_new);
								 * nlapiLogExecution('DEBUG',
								 * '-----------------------------------------------------');
								 */

								var d_SOW_date_B = nlapiStringToDate(d_start_of_week_new);
								var d_EOW_date_B = nlapiStringToDate(d_end_of_week_new);

								/*
								 * nlapiLogExecution('DEBUG',
								 * 'suiteletFunction', '3 --> ' + d_SOW_date_B);
								 * nlapiLogExecution('DEBUG',
								 * 'suiteletFunction', ' 4 --> ' +
								 * d_EOW_date_B);
								 * 
								 */
								d_SOW_date_B = get_date(d_SOW_date_B)
								d_EOW_date_B = get_date(d_EOW_date_B)

								/*
								 * nlapiLogExecution('DEBUG',
								 * 'suiteletFunction',' 5 --> ' + d_SOW_date_B);
								 * nlapiLogExecution('DEBUG',
								 * 'suiteletFunction', ' 6--> ' + d_EOW_date_B);
								 */

								if ((d_SOW_date_B == i_start_of_week_arr)
								        && (d_EOW_date_B == i_end_of_week_arr)
								        && (i_rate_emp == i_rate_new)) {

									/*
									 * nlapiLogExecution('DEBUG',
									 * '-----------------------------------------------------');
									 * nlapiLogExecution('DEBUG', 'i_date_new', '
									 * 1 --> ' + i_date_new);
									 * nlapiLogExecution('DEBUG',
									 * 'suiteletFunction', ' 1 --> ' +
									 * d_SOW_date_B); nlapiLogExecution('DEBUG',
									 * 'suiteletFunction', ' 2 --> ' +
									 * d_EOW_date_B); nlapiLogExecution('DEBUG',
									 * 'i_hours_new', ' 2 --> ' + i_hours_new);
									 * nlapiLogExecution('DEBUG', 'i_item_new', '
									 * 2 --> ' + i_item_new);
									 * nlapiLogExecution('DEBUG',
									 * '-----------------------------------------------------');
									 */

									if (flag != i_employee_arr) {
										i_employee_arr_n = i_employee_arr;
									} else {
										i_employee_arr_n = '';
									}
									var a_split_time_array = new Array();
									a_split_time_array = i_hours_new.split(':')
									var hours = a_split_time_array[0]
									var minutes = a_split_time_array[1]

									if (minutes == '0.00' || minutes == '0'
									        || minutes == 0) {
									} else {
										minutes = parseFloat(minutes) / 60;
										minutes = parseFloat(minutes)
										        .toFixed(2);
										// nlapiLogExecution('DEBUG', 'minutes',
										// ' ------ --> ' + minutes);
									}

									if (i_item_new == 2222
									        || i_item_new == 2221) // Amol:
									// added
									// 2221(FP)
									// item
									{
										i_hours_ST = parseFloat(i_hours_ST)
										        + parseFloat(hours)
										// i_minutes_ST =
										// parseFloat(i_minutes_ST) +
										// parseFloat(minutes);
										if (minutes == '0.75'
										        || minutes == '0.25') {
											// nlapiLogExecution('DEBUG',
											// 'i_minutes_ST', ' ------ --> ' +
											// i_minutes_ST);
											// nlapiLogExecution('DEBUG',
											// 'i_hours_ST', ' ------ --> ' +
											// i_hours_ST);
										}

										// i_minutes_ST=parseFloat(i_minutes_ST).toFixed(2);

										// i_minutes_ST=(parseFloat(i_minutes_ST)/60).toFixed(2);
										// nlapiLogExecution('DEBUG',
										// 'i_minutes_ST', ' ------ --> ' +
										// i_minutes_ST);

										i_hours_ST = parseFloat(i_hours_ST)
										        + parseFloat(minutes);
										i_hours_ST = parseFloat(i_hours_ST)
										        .toFixed(2);

										i_ST_rate = parseFloat(i_hours_ST)
										        * parseFloat(i_rate_new)
										i_ST_units = parseFloat(i_rate_new)
										        .toFixed(2);

									}// Time & Material

									if (i_item_new == 2425) {
										i_hours_OT = parseFloat(i_hours_OT)
										        + parseFloat(hours)
										// i_minutes_OT =
										// parseFloat(i_minutes_OT) +
										// parseFloat(minutes);

										// i_minutes_OT=parseFloat(i_minutes_OT).toFixed(2);

										// i_minutes_OT=(parseFloat(i_minutes_OT)/60).toFixed(2);

										i_hours_OT = parseFloat(i_hours_OT)
										        + parseFloat(minutes);

										i_hours_OT = parseFloat(i_hours_OT)
										        .toFixed(2);
										i_OT_rate = parseFloat(i_hours_OT);
										i_OT_units = parseFloat(i_rate_new)
										        .toFixed(2);

										/*
										 * nlapiLogExecution('DEBUG',
										 * '---------------------------------------------------');
										 * nlapiLogExecution('DEBUG', 'hours', ' -
										 * --> ' + hours);
										 * nlapiLogExecution('DEBUG',
										 * 'd_SOW_date_B', ' - --> ' +
										 * d_SOW_date_B);
										 * nlapiLogExecution('DEBUG',
										 * 'd_EOW_date_B', ' - --> ' +
										 * d_EOW_date_B);
										 * nlapiLogExecution('DEBUG',
										 * '---------------------------------------------------');
										 */

									}// OT Item

									// nlapiLogExecution('DEBUG',
									// 'suiteletFunction', ' 7 --> ' +
									// d_SOW_date_B);

								}// Start & End Date Validation
							}// Employee / Project / Item

						}// Marked T

					}// Billable Count

					i_amount = (parseFloat(i_hours_ST) * parseFloat(i_ST_units))
					        + (parseFloat(i_hours_OT) * parseFloat(i_OT_units));
					i_amount = parseFloat(i_amount);
					i_amount = i_amount.toFixed(2);

					i_total_amount_INV = parseFloat(i_total_amount_INV)
					        + parseFloat(i_amount);
					i_total_amount_INV = parseFloat(i_total_amount_INV);
					i_total_amount_INV = i_total_amount_INV.toFixed(2);

					var s_employee_addr_txt = '';
					s_employee_addr_txt = escape_special_chars(i_employee_arr);
					
					
					
					
					if(emp == 0 || emp_name == i_employee_arr ){
						if (a_employee_name_array.indexOf(i_employee_arr) != -1) {
							
							emp_total_hr = parseFloat(emp_total_hr) + parseFloat(i_hours_ST) + parseFloat(i_hours_OT);
							
							emp_total_amt = parseFloat(emp_total_amt) + parseFloat(i_amount);
							emp = 1;
							
							emp_count++;
							
							if(a_employee_array.length == emp_count){
								
								 xmlContent +="<tr>";
									xmlContent +="<td  border-right='0.5' width='50%' align = 'left' font-family=\"Helvetica\" font-size=\"9\">"+ prev_emp_name +" </td>";
								xmlContent +="<td  border-right='0.5' width='10%' align = 'center' font-family=\"Helvetica\" font-size=\"9\">"+ billfrom +"</td>";
								xmlContent +="<td  border-right='0.5' width='10%' align = 'center' font-family=\"Helvetica\" font-size=\"9\">"+ billto +"</td>";
								xmlContent +="<td  border-right='0.5' width='10%' align = 'center' font-family=\"Helvetica\" font-size=\"9\">"+ emp_total_hr+"</td>";
								xmlContent +="<td  border-right='0.5' width='10%' align = 'center' font-family=\"Helvetica\" font-size=\"9\">"+ parseFloat(i_ST_units)+"</td>";
							//	xmlContent +="<td  border-right='0.5' width='10%' ></td>";
								xmlContent +="<td  width='35%' align='right' font-family=\"Helvetica\" font-size=\"9\">"+ emp_total_amt + "</td>";
								
								xmlContent +="</tr>";						
								
								emp = 1;	
								
							}
							
							
							emp_name = i_employee_arr;
							prev_emp_name = s_employee_addr_txt;
							
						}
						
					}else{
						
						 xmlContent +="<tr>";
						xmlContent +="<td  border-right='0.5' width='50%' align = 'left' font-family=\"Helvetica\" font-size=\"9\">"+ prev_emp_name +" </td>";
					xmlContent +="<td  border-right='0.5' width='10%' align = 'center' font-family=\"Helvetica\" font-size=\"9\">"+ billfrom +"</td>";
					xmlContent +="<td  border-right='0.5' width='10%' align = 'center' font-family=\"Helvetica\" font-size=\"9\">"+ billto +"</td>";
					xmlContent +="<td  border-right='0.5' width='10%' align = 'center' font-family=\"Helvetica\" font-size=\"9\">"+ emp_total_hr+"</td>";
					xmlContent +="<td  border-right='0.5' width='10%' align = 'center' font-family=\"Helvetica\" font-size=\"9\">"+ parseFloat(i_ST_units)+"</td>";
				//	xmlContent +="<td  border-right='0.5' width='10%' ></td>";
					xmlContent +="<td  width='35%' align='right' font-family=\"Helvetica\" font-size=\"9\">"+ emp_total_amt + "</td>";
					
					xmlContent +="</tr>";		

						emp_total_hr = 0;	
						emp_total_amt = 0;
						
						emp_total_hr = parseFloat(emp_total_hr) + parseFloat(i_hours_ST) + parseFloat(i_hours_OT);
							
						emp_total_amt = parseFloat(emp_total_amt) + parseFloat(i_amount);
						
						emp_count++;
					 emp =0;	
					}
					
					
/*
                     xmlContent +="<tr>";
					strVar += "			<tr>";

					if (flag == 0 && flag_name != i_employee_arr) {
						if (a_employee_name_array.indexOf(i_employee_arr) != -1) {
							vb++;
							// strVar += " <td border-bottom=\"0\"
							// border-left=\"0\" font-family=\"Helvetica\"
							// align=\"left\"
							// font-size=\"8\">"+parseInt(vb)+"<\/td>";
							
							
							
							
                              
xmlContent +="<td  border-right='0.5' width='50%' align = 'center' font-family=\"Helvetica\" font-size=\"9\">"+ s_employee_addr_txt +" </td>";							  
								
								
							strVar += "				<td colspan=\"2\" border-bottom=\"0\" border-left=\"0\" font-family=\"Helvetica\" align=\"left\" font-size=\"8\">"
							        + s_employee_addr_txt + "<\/td>";
							flag = 1;
							flag_name = i_employee_arr
						}

					} else {
						// strVar += " <td border-bottom=\"0\" border-left=\"0\"
						// font-family=\"Helvetica\" align=\"left\"
						// font-size=\"8\"><\/td>";
						
						xmlContent +="<td  border-right='0.5' width='50%' align = 'center' font-family=\"Helvetica\" font-size=\"9\"></td>";							  

						
						
						strVar += "				<td colspan=\"2\" border-bottom=\"0\" border-left=\"0\" font-family=\"Helvetica\" align=\"left\" font-size=\"8\"><\/td>";
					}
	        
			         // nlapiLogExecution('DEBUG', 'i_hours_OT', ' - --> ' +
					// i_hours_OT);
					
					
				//	var m=n+1;
				
				//	xmlContent +="<td  border-right='0.5' width='10%' align='center' font-family=\"Helvetica\" font-size=\"9\"></td>";  //+parseInt(m)+
					
					xmlContent +="<td  border-right='0.5' width='10%' align = 'center' font-family=\"Helvetica\" font-size=\"9\">"+ i_start_of_week_arr +"</td>";
					xmlContent +="<td  border-right='0.5' width='10%' align = 'center' font-family=\"Helvetica\" font-size=\"9\">"+ i_end_of_week_arr +"</td>";
					xmlContent +="<td  border-right='0.5' width='10%' align = 'center' font-family=\"Helvetica\" font-size=\"9\">"+ parseFloat(i_hours_ST)+"</td>";
					xmlContent +="<td  border-right='0.5' width='10%' align = 'center' font-family=\"Helvetica\" font-size=\"9\">"+ parseFloat(i_ST_units)+"</td>";
				//	xmlContent +="<td  border-right='0.5' width='10%' ></td>";
					xmlContent +="<td  width='35%' align='right' font-family=\"Helvetica\" font-size=\"9\">"+ i_amount + "</td>";
					
					xmlContent +="</tr>";
					
					
*/
					strVar += "				<td colspan=\"1\" border-bottom=\"0\" border-left=\"0\" font-family=\"Helvetica\" align=\"left\" font-size=\"8\">"
					        + i_end_of_week_arr + "<\/td>";
					strVar += "				<td colspan=\"1\" border-bottom=\"0\" border-left=\"0\" font-family=\"Helvetica\" align=\"center\" font-size=\"8\">"
					        + parseFloat(i_ST_units) + "<\/td>";
					strVar += "				<td colspan=\"1\" border-bottom=\"0\" border-left=\"0\" font-family=\"Helvetica\" align=\"center\" font-size=\"8\">"
					        + parseFloat(i_hours_ST) + "<\/td>";
					strVar += "				<td colspan=\"1\" border-bottom=\"0\" border-left=\"0\" font-family=\"Helvetica\" align=\"center\" font-size=\"8\">"
					        + parseFloat(i_OT_units) + "<\/td>";// i_hours_OT//i_OT_rate
					strVar += "				<td colspan=\"1\" border-bottom=\"0\" border-left=\"0\" font-family=\"Helvetica\" align=\"center\" font-size=\"8\">"
					        + parseFloat(i_hours_OT) + "<\/td>";// i_OT_units
					strVar += "				<td colspan=\"1\" border-bottom=\"0\" border-left=\"0\" border-right=\"0\" font-family=\"Helvetica\" align=\"right\" font-size=\"8\">"
					        + i_amount + "<\/td>";
					strVar += "			<\/tr>";

					pagecount++;

					if (pagecount == linenum && a_employee_array.length != '17')// if(pagecount
					// ==
					// '17'
					// &&
					// a_employee_array.length!=pagecount)
					{
						pagecount = 0;
						linenum = 40;
						strVar += "<\/table>";
						strVar += " <p style=\"page-break-after: always\"><\/p>";

						strVar += "<table border=\"0\" width=\"100%\">";
						// ----------------------------------------------------------

						strVar += "<tr>";
						// strVar += "<td colspan=\"1\" border-left=\"0\"
						// border-top=\"0.3\" border-bottom=\"0\"
						// font-family=\"Helvetica\" align=\"center\"
						// font-size=\"9\"></td>";
						strVar += "<td colspan=\"2\" border-left=\"0\" border-top=\"0.3\" border-bottom=\"0\" font-family=\"Helvetica\" align=\"center\" font-size=\"9\"><b>Name<\/b></td>";
						strVar += "<td colspan=\"1\" border-left=\"0\" border-top=\"0.3\" border-bottom=\"0\" font-family=\"Helvetica\" align=\"center\" font-size=\"9\"><b>Week End<\/b></td>";
						strVar += "<td colspan=\"1\" border-left=\"0\" border-top=\"0.3\" border-bottom=\"0\" font-family=\"Helvetica\" align=\"center\" font-size=\"9\"><b>ST Rate<\/b></td>";
						strVar += "<td colspan=\"1\" border-left=\"0\" border-top=\"0.3\" border-bottom=\"0\" font-family=\"Helvetica\" align=\"center\" font-size=\"9\"><b>ST Units<\/b></td>";
						strVar += "<td colspan=\"1\" border-left=\"0\" border-top=\"0.3\" border-bottom=\"0\" font-family=\"Helvetica\" align=\"center\" font-size=\"9\"><b>OT Rate<\/b></td>";
						strVar += "<td colspan=\"1\" border-left=\"0\" border-top=\"0.3\" border-bottom=\"0\" font-family=\"Helvetica\" align=\"center\" font-size=\"9\"><b>OT Units<\/b></td>";
						strVar += "<td colspan=\"1\" border-left=\"0\" border-right=\"0\" border-top=\"0.3\" border-bottom=\"0\" align=\"right\" font-family=\"Helvetica\" font-size=\"9\"><b>Amount("
						        + s_currency_symbol + ")<\/b></td>";
						strVar += "</tr>";

						strVar += "<tr>";
						// strVar += "<td colspan=\"1\" border-left=\"0\"
						// border-top=\"0\" border-bottom=\"0.3\"
						// align=\"center\" font-family=\"Helvetica\"
						// font-size=\"9\"><b>SL#<\/b></td>";
						strVar += "<td colspan=\"2\" border-left=\"0\" border-top=\"0\" border-bottom=\"0.3\" align=\"left\" font-family=\"Helvetica\" font-size=\"9\"></td>";
						strVar += "<td colspan=\"1\" border-left=\"0\" border-top=\"0\" border-bottom=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"9\"></td>";
						strVar += "<td colspan=\"1\" border-left=\"0\" border-top=\"0\" border-bottom=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"9\">(Hourly)</td>";
						strVar += "<td colspan=\"1\" border-left=\"0\" border-top=\"0\" border-bottom=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"9\">(Hours)</td>";
						strVar += "<td colspan=\"1\" border-left=\"0\" border-top=\"0\" border-bottom=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"9\">(Hourly)</td>";
						strVar += "<td colspan=\"1\" border-left=\"0\" border-top=\"0\" border-bottom=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"9\">(Hours)</td>";
						strVar += "<td colspan=\"1\" border-left=\"0\" border-right=\"0\" border-top=\"0\" border-bottom=\"0.3\" align=\"right\" font-family=\"Helvetica\" font-size=\"9\"></td>";
						strVar += "</tr>";
						// ----------------------------------------------------------
					}
				}
	
	
	if (discount == null || discount == 'null') {
					discount = '';

				}
				if (discount_amt == null || discount_amt == 'null') {
					discount_amt = '';

				}
				for(var dis_ind=1;dis_ind<= discount_count;dis_ind++)
				{
				strVar += "			<tr>";
				strVar += "				<td colspan=\"2\" border-bottom=\"0\" border-left=\"0\" font-family=\"Helvetica\" align=\"left\" font-size=\"8\">"
				        + o_recordOBJ.getLineItemValue('item',
				        'description', dis_ind) + "<\/td>";
				strVar += "				<td colspan=\"1\" border-bottom=\"0\" border-left=\"0\" font-family=\"Helvetica\" align=\"left\" font-size=\"8\"><\/td>";
				strVar += "				<td colspan=\"1\" border-bottom=\"0\" border-left=\"0\" font-family=\"Helvetica\" align=\"center\" font-size=\"8\"><\/td>";
				strVar += "				<td colspan=\"1\" border-bottom=\"0\" border-left=\"0\" font-family=\"Helvetica\" align=\"center\" font-size=\"8\"><\/td>";
				strVar += "				<td colspan=\"1\" border-bottom=\"0\" border-left=\"0\" font-family=\"Helvetica\" align=\"center\" font-size=\"8\"><\/td>";// i_hours_OT//i_OT_rate
				strVar += "				<td colspan=\"1\" border-bottom=\"0\" border-left=\"0\" font-family=\"Helvetica\" align=\"center\" font-size=\"8\"><\/td>";// i_OT_units
				strVar += "				<td colspan=\"1\" border-bottom=\"0\" border-left=\"0\" border-right=\"0\" font-family=\"Helvetica\" align=\"right\" font-size=\"8\">"
				        +o_recordOBJ.getLineItemValue('item',
				        'amount',dis_ind ) + "<\/td>";
				strVar += "			<\/tr>";
				// -----------------------------------------------------------------------------------------------------------
				}
				if (_logValidation(i_discount_percent)
				        && i_discount_percent != 0) {
					strVar += "<tr>";
					// strVar += "<td colspan=\"1\" border-left=\"0\"
					// border-top=\"0\" border-bottom=\"0\" align=\"left\"
					// font-family=\"Helvetica\" font-size=\"9\"><b><\/b></td>";
					strVar += "<td colspan=\"2\" border-left=\"0\" border-top=\"0\" border-bottom=\"0\" align=\"left\" font-family=\"Helvetica\" font-size=\"9\"><b><\/b></td>";
					strVar += "<td colspan=\"1\" border-left=\"0\" border-top=\"0\" border-bottom=\"0\" align=\"left\" font-family=\"Helvetica\" font-size=\"9\">Less: Discount @"
					        + i_discount_percent + "</td>";
					strVar += "<td colspan=\"1\" border-left=\"0\" border-top=\"0\" border-bottom=\"0\" align=\"left\" font-family=\"Helvetica\" font-size=\"9\"><b><\/b></td>";
					strVar += "<td colspan=\"1\" border-left=\"0\" border-top=\"0\" border-bottom=\"0\" align=\"left\" font-family=\"Helvetica\" font-size=\"9\"><b><\/b></td>";
					strVar += "<td colspan=\"1\" border-left=\"0\" border-top=\"0\" border-bottom=\"0\" align=\"left\" font-family=\"Helvetica\" font-size=\"9\"><b><\/b></td>";
					strVar += "<td colspan=\"1\" border-left=\"0\" border-top=\"0\" border-bottom=\"0\" align=\"left\" font-family=\"Helvetica\" font-size=\"9\"><b><\/b></td>";
					strVar += "<td colspan=\"1\" border-left=\"0\" border-right=\"0\" border-top=\"0\" border-bottom=\"0\" align=\"left\" font-family=\"Helvetica\" font-size=\"9\"><b><\/b></td>";
					strVar += "</tr>";

				}
				if (_logValidation(i_tax_percent) && i_tax_percent != 0) {
					strVar += "<tr>";
					// strVar += "<td colspan=\"1\" border-left=\"0\"
					// border-top=\"0\" border-bottom=\"0\" align=\"left\"
					// font-family=\"Helvetica\" font-size=\"9\"><b><\/b></td>";
					strVar += "<td colspan=\"2\" border-left=\"0\" border-top=\"0\" border-bottom=\"0\" align=\"left\" font-family=\"Helvetica\" font-size=\"9\"><b><\/b></td>";
					strVar += "<td colspan=\"1\" border-left=\"0\" border-top=\"0\" border-bottom=\"0\" align=\"left\" font-family=\"Helvetica\" font-size=\"9\">Add: Sales Tax PA state @"
					        + i_tax_percent + "</td>";
					strVar += "<td colspan=\"1\" border-left=\"0\" border-top=\"0\" border-bottom=\"0\" align=\"left\" font-family=\"Helvetica\" font-size=\"9\"><b><\/b></td>";
					strVar += "<td colspan=\"1\" border-left=\"0\" border-top=\"0\" border-bottom=\"0\" align=\"left\" font-family=\"Helvetica\" font-size=\"9\"><b><\/b></td>";
					strVar += "<td colspan=\"1\" border-left=\"0\" border-top=\"0\" border-bottom=\"0\" align=\"left\" font-family=\"Helvetica\" font-size=\"9\"><b><\/b></td>";
					strVar += "<td colspan=\"1\" border-left=\"0\" border-top=\"0\" border-bottom=\"0\" align=\"left\" font-family=\"Helvetica\" font-size=\"9\"><b><\/b></td>";
					strVar += "<td colspan=\"1\" border-left=\"0\" border-right=\"0\" border-top=\"0\" border-bottom=\"0\" align=\"left\" font-family=\"Helvetica\" font-size=\"9\"><b><\/b></td>";
					strVar += "</tr>";

				}

				i_total_amount_INV = i_total_amount_INV
				        - parseInt(-discount_amt);
				i_total_amount_INV = i_total_amount_INV.toFixed(2);

				strVar += "<tr>";
				// strVar += "<td colspan=\"1\" border-left=\"0\"
				// border-top=\"0.3\" border-bottom=\"0.3\" align=\"center\"
				// font-family=\"Helvetica\" font-size=\"9\"><b><\/b></td>";
				strVar += "<td colspan=\"2\" border-left=\"0\" border-top=\"0.3\" border-bottom=\"0.3\" align=\"left\" font-family=\"Helvetica\" font-size=\"9\"><b><\/b></td>";
				strVar += "<td colspan=\"1\" border-left=\"0\" border-top=\"0.3\" border-bottom=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"9\"><b> <\/b></td>";
				strVar += "<td colspan=\"1\" border-left=\"0\" border-top=\"0.3\" border-bottom=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"9\"><b><\/b></td>";
				strVar += "<td colspan=\"1\" border-left=\"0\" border-top=\"0.3\" border-bottom=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"9\"></td>";
				strVar += "<td colspan=\"1\" border-left=\"0\" border-top=\"0.3\" border-bottom=\"0.3\" align=\"right\" font-family=\"Helvetica\" font-size=\"9\"><b>Invoice&nbps;&nbsp;Total :<\/b></td>";
				strVar += "<td colspan=\"1\" border-left=\"0\" border-top=\"0.3\" border-bottom=\"0.3\" align=\"right\" font-family=\"Helvetica\" font-size=\"9\"></td>";
				strVar += "<td colspan=\"1\" border-left=\"0\" border-right=\"0\" border-top=\"0.3\" border-bottom=\"0.3\" align=\"right\" font-family=\"Helvetica\" font-size=\"9\"><b>"
				        + s_currency_symbol
				        + "&nbsp;"
				        + formatDollar(parseFloat(i_total_amount_INV)
				                .toFixed(2)) + "<\/b></td>";
				strVar += "</tr>";

				strVar += "<tr>";
				strVar += "<td border-right=\"0\" border-left=\"0\"  font-family=\"Helvetica\" font-size=\"9\" colspan=\"8\"><b>Billing Currency&nbsp;&nbsp;&nbsp;:&nbsp;<\/b>"
				        + i_currency + "</td>";
				strVar += "</tr>";

				strVar += "<tr>";
				strVar += "<td border-right=\"0\" border-left=\"0\" border-top=\"0\" border-bottom=\"0.3\" font-family=\"Helvetica\" font-size=\"9\" colspan=\"8\"><b>Amount In Words&nbsp;:&nbsp;<\/b>"
				        + toWordsFunc(
				                parseFloat(i_total_amount_INV).toFixed(2),
				                i_currency) + "</td>";
				strVar += "</tr>";
								

				strVar += "</table>";
								
				
	

			
	
	
	
	
	
	if(i_item == 2221 || i_item == 2603 || i_item == 2612 ){
	
	
			for(var n=0;n<dataRow_description.length;n++){
		//dataRow = dataRow_description[n];
				var description = nlapiEscapeXML(dataRow_description[n].Description);
				var amount_ = dataRow_description[n].Amount;
				
                     				
				var taxrate=dataRow_description[n].TaxRate;
				var taxamount=dataRow_description[n].TaxAmount;
				var rate=dataRow_description[n].rate;
				var quantity=dataRow_description[n].quantity;
					if(!_logValidation(rate))
					{
						rate='';
					}
					if(!_logValidation(quantity))
					{
						quantity='';
					}
					
				if(!_logValidation(description)){
					description = '';
				}
					xmlContent +="<tr>";
					var m=n+1;
					
					xmlContent +="<td  border-right='0.5' width='50%' align = 'center' font-family=\"Helvetica\" font-size=\"9\">"+ description +" </td>";	
					xmlContent +="<td  border-right='0.5' width='10%' align = 'center' font-family=\"Helvetica\" font-size=\"9\">"+ billfrom +"</td>";
					xmlContent +="<td  border-right='0.5' width='10%' align = 'center' font-family=\"Helvetica\" font-size=\"9\">"+ billto +"</td>";
					xmlContent +="<td  border-right='0.5' width='10%' align = 'center' font-family=\"Helvetica\" font-size=\"9\">"+ quantity +"</td>";
					xmlContent +="<td  border-right='0.5' width='10%' align = 'center' font-family=\"Helvetica\" font-size=\"9\">"+ rate+"</td>";
				//	xmlContent +="<td  border-right='0.5' width='10%' ></td>";
					xmlContent +="<td  width='35%' align='right' font-family=\"Helvetica\" font-size=\"9\">"+ formatNumber(amount_) + "</td>";
					
				/*	
					xmlContent +="<td  border-right='0.5' width='10%' align='center' font-family=\"Helvetica\" font-size=\"9\">"+parseInt(m) +"</td>";
					xmlContent +="<td  border-right='0.5' width='35%' align = 'center' font-family=\"Helvetica\" font-size=\"9\">"+ description +" </td>";
					xmlContent +="<td  border-right='0.5' width='5%' align = 'center' font-family=\"Helvetica\" font-size=\"9\">"+ billfrom +"</td>";
					xmlContent +="<td  border-right='0.5' width='5%' align = 'center' font-family=\"Helvetica\" font-size=\"9\">"+ billto +"</td>";
					xmlContent +="<td  border-right='0.5' width='10%' align = 'center' font-family=\"Helvetica\" font-size=\"9\">"+quantity+"</td>";
					xmlContent +="<td  border-right='0.5' width='10%' align = 'center' font-family=\"Helvetica\" font-size=\"9\">"+rate+"</td>";
					xmlContent +="<td  border-right='0.5' width='10%' ></td>";
					xmlContent +="<td  width='15%' align='right' font-family=\"Helvetica\" font-size=\"9\">"+ formatNumber(amount_) + "</td>";
				*/	
					xmlContent +="</tr>";
	        }
	}
	//break

	xmlContent +="<tr>";
	xmlContent +="<td  border-right='0.5' width='40%' align='left'></td>";
//	xmlContent +="<td  border-right='0.5' width='35%' align = 'center'></td>";
	xmlContent +="<td  border-right='0.5' width='10%'></td>";
	xmlContent +="<td  border-right='0.5' width='10%' ></td>";
	xmlContent +="<td  border-right='0.5' width='10%'></td>";
//	xmlContent +="<td  border-right='0.5' width='10%'></td>";
	xmlContent +="<td border-right='0.5' width='15%' align='right'></td>";
	
	xmlContent +="</tr>";
	
	xmlContent +="<tr>";
	xmlContent +="<td  border-right='0.5' width='40%' align='left'></td>";
//	xmlContent +="<td  border-right='0.5' width='35%' align = 'center'></td>";
	xmlContent +="<td  border-right='0.5' width='10%'></td>";
	xmlContent +="<td  border-right='0.5' width='10%' ></td>";
	xmlContent +="<td  border-right='0.5' width='10%'></td>";
//	xmlContent +="<td  border-right='0.5' width='10%'></td>";
	xmlContent +="<td border-right='0.5' width='15%' align='right'></td>";
	
	xmlContent +="</tr>";
	
	xmlContent +="<tr>";
	xmlContent +="<td  border-right='0.5' width='40%' align='left'></td>";
//	xmlContent +="<td  border-right='0.5' width='35%' align = 'center'></td>";
	xmlContent +="<td  border-right='0.5' width='10%'></td>";
	xmlContent +="<td  border-right='0.5' width='10%' ></td>";
	xmlContent +="<td  border-right='0.5' width='10%'></td>";
//	xmlContent +="<td  border-right='0.5' width='10%'></td>";
	xmlContent +="<td border-right='0.5' width='15%' align='right'></td>";
	
	xmlContent +="</tr>";
	
	xmlContent +="<tr>";
	xmlContent +="<td  border-right='0.5' width='40%' align='left'></td>";
//	xmlContent +="<td  border-right='0.5' width='35%' align = 'center'></td>";
	xmlContent +="<td  border-right='0.5' width='10%'></td>";
	xmlContent +="<td  border-right='0.5' width='10%' ></td>";
	xmlContent +="<td  border-right='0.5' width='10%'></td>";
//	xmlContent +="<td  border-right='0.5' width='10%'></td>";
	xmlContent +="<td border-right='0.5' width='15%' align='right'></td>";
	
	xmlContent +="</tr>";
	
	xmlContent +="<tr>";
	xmlContent +="<td  border-right='0.5' width='40%' align='left'></td>";
//	xmlContent +="<td  border-right='0.5' width='35%' align = 'center'></td>";
	xmlContent +="<td  border-right='0.5' width='10%'></td>";
	xmlContent +="<td  border-right='0.5' width='10%' ></td>";
	xmlContent +="<td  border-right='0.5' width='10%'></td>";
//	xmlContent +="<td  border-right='0.5' width='10%'></td>";
	xmlContent +="<td border-right='0.5' width='15%' align='right'></td>";
	
	xmlContent +="</tr>";
	
	/*
	if(invoice_amt){
		var service_Tax = Math.round(invoice_amt * 0.14);
		var swachh_Bharat = Math.round(invoice_amt *0.005);
		var krishi_kalyan = Math.round(invoice_amt * 0.005);
		
	}*/
	var grand_Total = parseFloat(invoice_amt) + parseFloat(taxamount) ;
	
	var converted_words = number2text(grand_Total,currency_);
	//grand_Total = grand_Total +'.00';
	//xmlContent +="<br/>";
	xmlContent +="<tr>";
	xmlContent +="<td  border-right='0.5' width='40%' align='left'></td>";
//	xmlContent +="<td  border-right='0.5' width='35%' align = 'center'></td>";
	xmlContent +="<td  border-right='0.5' width='10%'></td>";
	xmlContent +="<td  border-right='0.5' width='10%' ></td>";
	xmlContent +="<td  border-right='0.5' width='10%'></td>";
//	xmlContent +="<td  border-right='0.5' width='10%'></td>";
	xmlContent +="<td border-right='0.5' width='15%' align='right'></td>";
	
	xmlContent +="</tr>";
	
	xmlContent +="<tr>";
	xmlContent +="<td  border-right='0.5' width='40%' align='left'></td>";
//	xmlContent +="<td  border-right='0.5' width='35%' align = 'center'></td>";
	xmlContent +="<td  border-right='0.5' width='10%'></td>";
	xmlContent +="<td  border-right='0.5' width='10%' ></td>";
	xmlContent +="<td  border-right='0.5' width='10%'></td>";
//	xmlContent +="<td  border-right='0.5' width='10%'></td>";
	xmlContent +="<td border-right='0.5' width='15%' align='right'></td>";
	
	xmlContent +="</tr>";
	
	xmlContent +="<tr>";
	xmlContent +="<td  border-right='0.5' width='40%' align='left'></td>";
//	xmlContent +="<td  border-right='0.5' width='35%' align = 'center'></td>";
	xmlContent +="<td  border-right='0.5' width='10%'></td>";
	xmlContent +="<td  border-right='0.5' width='10%' ></td>";
	xmlContent +="<td  border-right='0.5' width='10%'></td>";
//	xmlContent +="<td  border-right='0.5' width='10%'></td>";
	xmlContent +="<td border-right='0.5' width='15%' align='right'></td>";
	
	xmlContent +="</tr>";
	
	xmlContent +="<tr>";
	xmlContent +="<td  border-right='0.5' width='40%' align='left'></td>";
//	xmlContent +="<td  border-right='0.5' width='35%' align = 'center'></td>";
	xmlContent +="<td  border-right='0.5' width='10%'></td>";
	xmlContent +="<td  border-right='0.5' width='10%' ></td>";
	xmlContent +="<td  border-right='0.5' width='10%'></td>";
//	xmlContent +="<td  border-right='0.5' width='10%'></td>";
	xmlContent +="<td border-right='0.5' width='15%' align='right'></td>";
	
	xmlContent +="</tr>";
	
	xmlContent +="<tr>";
	xmlContent +="<td  border-right='0.5' width='40%' align='left'></td>";
//	xmlContent +="<td  border-right='0.5' width='35%' align = 'center'></td>";
	xmlContent +="<td  border-right='0.5' width='10%'></td>";
	xmlContent +="<td  border-right='0.5' width='10%' ></td>";
	xmlContent +="<td  border-right='0.5' width='10%'></td>";
//	xmlContent +="<td  border-right='0.5' width='10%'></td>";
	xmlContent +="<td border-right='0.5' width='15%' align='right'></td>";
	
	xmlContent +="</tr>";
	
	xmlContent +="<tr>";
	xmlContent +="<td  border-right='0.5' width='40%' align='left'></td>";
//	xmlContent +="<td  border-right='0.5' width='35%' align = 'center'></td>";
	xmlContent +="<td  border-right='0.5' width='10%'></td>";
	xmlContent +="<td  border-right='0.5' width='10%' ></td>";
	xmlContent +="<td  border-right='0.5' width='10%'></td>";
//	xmlContent +="<td  border-right='0.5' width='10%'></td>";
	xmlContent +="<td border-right='0.5' width='15%' align='right'></td>";
	
	xmlContent +="</tr>";
	
	xmlContent +="<tr>";
	xmlContent +="<td  border-right='0.5' width='40%' align='left'></td>";
//	xmlContent +="<td  border-right='0.5' width='35%' align = 'center'></td>";
	xmlContent +="<td  border-right='0.5' width='10%'></td>";
	xmlContent +="<td  border-right='0.5' width='10%' ></td>";
	xmlContent +="<td  border-right='0.5' width='10%'></td>";
//	xmlContent +="<td  border-right='0.5' width='10%'></td>";
	xmlContent +="<td border-right='0.5' width='15%' align='right'></td>";
	
	xmlContent +="</tr>";
	
	xmlContent +="<tr>";
	xmlContent +="<td  border-right='0.5' width='40%' align='left'></td>";
//	xmlContent +="<td  border-right='0.5' width='35%' align = 'center'></td>";
	xmlContent +="<td  border-right='0.5' width='10%'></td>";
	xmlContent +="<td  border-right='0.5' width='10%' ></td>";
	xmlContent +="<td  border-right='0.5' width='10%'></td>";
//	xmlContent +="<td  border-right='0.5' width='10%'></td>";
	xmlContent +="<td border-right='0.5' width='15%' align='right'></td>";
	
	xmlContent +="</tr>";
/*	
	xmlContent +="<tr>";
	xmlContent +="<td  border-right='0.5' width='40%' align='left'></td>";
//	xmlContent +="<td  border-right='0.5' width='35%' align = 'center'></td>";
	xmlContent +="<td  border-right='0.5' width='10%'></td>";
	xmlContent +="<td  border-right='0.5' width='10%' ></td>";
	xmlContent +="<td  border-right='0.5' width='10%'></td>";
//	xmlContent +="<td  border-right='0.5' width='10%'></td>";
	xmlContent +="<td border-right='0.5' width='15%' align='right'></td>";
	
	xmlContent +="</tr>";
	
	xmlContent +="<tr>";
	xmlContent +="<td  border-right='0.5' width='40%' align='left'></td>";
//	xmlContent +="<td  border-right='0.5' width='35%' align = 'center'></td>";
	xmlContent +="<td  border-right='0.5' width='10%'></td>";
	xmlContent +="<td  border-right='0.5' width='10%' ></td>";
	xmlContent +="<td  border-right='0.5' width='10%'></td>";
//	xmlContent +="<td  border-right='0.5' width='10%'></td>";
	xmlContent +="<td border-right='0.5' width='15%' align='right'></td>";
	
	xmlContent +="</tr>";
	
	xmlContent +="<tr>";
	xmlContent +="<td  border-right='0.5' width='40%' align='left'></td>";
//	xmlContent +="<td  border-right='0.5' width='35%' align = 'center'></td>";
	xmlContent +="<td  border-right='0.5' width='10%'></td>";
	xmlContent +="<td  border-right='0.5' width='10%' ></td>";
	xmlContent +="<td  border-right='0.5' width='10%'></td>";
//	xmlContent +="<td  border-right='0.5' width='10%'></td>";
	xmlContent +="<td border-right='0.5' width='15%' align='right'></td>";
	
	xmlContent +="</tr>";
	
	xmlContent +="<tr>";
	xmlContent +="<td  border-right='0.5' width='40%' align='left'></td>";
//	xmlContent +="<td  border-right='0.5' width='35%' align = 'center'></td>";
	xmlContent +="<td  border-right='0.5' width='10%'></td>";
	xmlContent +="<td  border-right='0.5' width='10%' ></td>";
	xmlContent +="<td  border-right='0.5' width='10%'></td>";
//	xmlContent +="<td  border-right='0.5' width='10%'></td>";
	xmlContent +="<td border-right='0.5' width='15%' align='right'></td>";
	
	xmlContent +="</tr>";
	
	xmlContent +="<tr>";
	xmlContent +="<td  border-right='0.5' width='40%' align='left'></td>";
//	xmlContent +="<td  border-right='0.5' width='35%' align = 'center'></td>";
	xmlContent +="<td  border-right='0.5' width='10%'></td>";
	xmlContent +="<td  border-right='0.5' width='10%' ></td>";
	xmlContent +="<td  border-right='0.5' width='10%'></td>";
//	xmlContent +="<td  border-right='0.5' width='10%'></td>";
	xmlContent +="<td border-right='0.5' width='15%' align='right'></td>";
	
	xmlContent +="</tr>";
	
	xmlContent +="<tr>";
	xmlContent +="<td  border-right='0.5' width='40%' align='left'></td>";
//	xmlContent +="<td  border-right='0.5' width='35%' align = 'center'></td>";
	xmlContent +="<td  border-right='0.5' width='10%'></td>";
	xmlContent +="<td  border-right='0.5' width='10%' ></td>";
	xmlContent +="<td  border-right='0.5' width='10%'></td>";
//	xmlContent +="<td  border-right='0.5' width='10%'></td>";
	xmlContent +="<td border-right='0.5' width='15%' align='right'></td>";
	
	xmlContent +="</tr>";
*/	
	xmlContent +="<tr>";
	xmlContent +="<td  border-right='0.5' width='40%' align='left'></td>";
//	xmlContent +="<td  border-right='0.5' width='35%' align = 'center'></td>";
	xmlContent +="<td  border-right='0.5' width='10%'></td>";
	xmlContent +="<td  border-right='0.5' width='10%' ></td>";
	xmlContent +="<td  border-right='0.5' width='10%'></td>";
//	xmlContent +="<td  border-right='0.5' width='10%'></td>";
	xmlContent +="<td border-right='0.5' width='15%' align='right'></td>";
	
	xmlContent +="</tr>";
	
xmlContent +="<tr>";
	xmlContent +="<td  border-right='0.5' width='40%' align='left'></td>";
//	xmlContent +="<td  border-right='0.5' width='35%' align = 'center'></td>";
	xmlContent +="<td  border-right='0.5' width='10%'></td>";
	xmlContent +="<td  border-right='0.5' width='10%' ></td>";
	xmlContent +="<td  border-right='0.5' width='10%'></td>";
//	xmlContent +="<td  border-right='0.5' width='10%'></td>";
	xmlContent +="<td border-right='0.5' width='15%' align='right'></td>";
	
	xmlContent +="</tr>";
	
	xmlContent +="<tr>";
	xmlContent +="<td  border-right='0.5' width='40%' align='left'></td>";
//	xmlContent +="<td  border-right='0.5' width='35%' align = 'center'></td>";
	xmlContent +="<td  border-right='0.5' width='10%'></td>";
	xmlContent +="<td  border-right='0.5' width='10%' ></td>";
	xmlContent +="<td  border-right='0.5' width='10%'></td>";
//	xmlContent +="<td  border-right='0.5' width='10%'></td>";
	xmlContent +="<td border-right='0.5' width='15%' align='right'></td>";
	
	xmlContent +="</tr>";
	
	xmlContent +="<tr>";
	xmlContent +="<td  border-right='0.5' width='40%' align='left'></td>";
//	xmlContent +="<td  border-right='0.5' width='35%' align = 'center'></td>";
	xmlContent +="<td  border-right='0.5' width='10%'></td>";
	xmlContent +="<td  border-right='0.5' width='10%' ></td>";
	xmlContent +="<td  border-right='0.5' width='10%'></td>";
//	xmlContent +="<td  border-right='0.5' width='10%'></td>";
	xmlContent +="<td border-right='0.5' width='15%' align='right'></td>";
	
	xmlContent +="</tr>";
	
	xmlContent +="<tr>";
	xmlContent +="<td  border-right='0.5' width='40%' align='left'></td>";
//	xmlContent +="<td  border-right='0.5' width='35%' align = 'center'></td>";
	xmlContent +="<td  border-right='0.5' width='10%'></td>";
	xmlContent +="<td  border-right='0.5' width='10%' ></td>";
	xmlContent +="<td  border-right='0.5' width='10%'></td>";
//	xmlContent +="<td  border-right='0.5' width='10%'></td>";
	xmlContent +="<td border-right='0.5' width='15%' align='right'></td>";
	
	xmlContent +="</tr>";
	
	xmlContent +="<tr>";
	xmlContent +="<td  border-right='0.5' width='40%' align='left'></td>";
//	xmlContent +="<td  border-right='0.5' width='35%' align = 'center'></td>";
	xmlContent +="<td  border-right='0.5' width='10%'></td>";
	xmlContent +="<td  border-right='0.5' width='10%' ></td>";
	xmlContent +="<td  border-right='0.5' width='10%'></td>";
//	xmlContent +="<td  border-right='0.5' width='10%'></td>";
	xmlContent +="<td border-right='0.5' width='15%' align='right'></td>";
	
	xmlContent +="</tr>";
	
xmlContent +="<tr>";
	xmlContent +="<td  border-right='0.5' width='40%' align='left'></td>";
//	xmlContent +="<td  border-right='0.5' width='35%' align = 'center'></td>";
	xmlContent +="<td  border-right='0.5' width='10%'></td>";
	xmlContent +="<td  border-right='0.5' width='10%' ></td>";
	xmlContent +="<td  border-right='0.5' width='10%'></td>";
//	xmlContent +="<td  border-right='0.5' width='10%'></td>";
	xmlContent +="<td border-right='0.5' width='15%' align='right'></td>";
	
	xmlContent +="</tr>";
	
	xmlContent +="<tr>";
	xmlContent +="<td  border-right='0.5' width='40%' align='left'></td>";
//	xmlContent +="<td  border-right='0.5' width='35%' align = 'center'></td>";
	xmlContent +="<td  border-right='0.5' width='10%'></td>";
	xmlContent +="<td  border-right='0.5' width='10%' ></td>";
	xmlContent +="<td  border-right='0.5' width='10%'></td>";
//	xmlContent +="<td  border-right='0.5' width='10%'></td>";
	xmlContent +="<td border-right='0.5' width='15%' align='right'></td>";
	
	xmlContent +="</tr>";
	
	xmlContent +="<tr>";
	xmlContent +="<td  border-right='0.5' width='40%' align='left'></td>";
//	xmlContent +="<td  border-right='0.5' width='35%' align = 'center'></td>";
	xmlContent +="<td  border-right='0.5' width='10%'></td>";
	xmlContent +="<td  border-right='0.5' width='10%' ></td>";
	xmlContent +="<td  border-right='0.5' width='10%'></td>";
//	xmlContent +="<td  border-right='0.5' width='10%'></td>";
	xmlContent +="<td border-right='0.5' width='15%' align='right'></td>";
	
	xmlContent +="</tr>";
	
	xmlContent +="<tr>";
	xmlContent +="<td  border-right='0.5' width='40%' align='left'></td>";
//	xmlContent +="<td  border-right='0.5' width='35%' align = 'center'></td>";
	xmlContent +="<td  border-right='0.5' width='10%'></td>";
	xmlContent +="<td  border-right='0.5' width='10%' ></td>";
	xmlContent +="<td  border-right='0.5' width='10%'></td>";
//	xmlContent +="<td  border-right='0.5' width='10%'></td>";
	xmlContent +="<td border-right='0.5' width='15%' align='right'></td>";
	
	xmlContent +="</tr>";
	
	xmlContent +="<tr>";
	xmlContent +="<td  border-right='0.5' width='40%' align='left'></td>";
//	xmlContent +="<td  border-right='0.5' width='35%' align = 'center'></td>";
	xmlContent +="<td  border-right='0.5' width='10%'></td>";
	xmlContent +="<td  border-right='0.5' width='10%' ></td>";
	xmlContent +="<td  border-right='0.5' width='10%'></td>";
//	xmlContent +="<td  border-right='0.5' width='10%'></td>";
	xmlContent +="<td border-right='0.5' width='15%' align='right'></td>";
	
	xmlContent +="</tr>";
	
	xmlContent +="<tr>";
	xmlContent +="<td  border-right='0.5' width='40%' align='left'></td>";
//	xmlContent +="<td  border-right='0.5' width='35%' align = 'center'></td>";
	xmlContent +="<td  border-right='0.5' width='10%'></td>";
	xmlContent +="<td  border-right='0.5' width='10%' ></td>";
	xmlContent +="<td  border-right='0.5' width='10%'></td>";
//	xmlContent +="<td  border-right='0.5' width='10%'></td>";
	xmlContent +="<td border-right='0.5' width='15%' align='right'></td>";
	
	xmlContent +="</tr>";
	
	xmlContent +="<tr>";
	xmlContent +="<td  border-right='0.5' width='40%' align='left'></td>";
//	xmlContent +="<td  border-right='0.5' width='35%' align = 'center'></td>";
	xmlContent +="<td  border-right='0.5' width='10%'></td>";
	xmlContent +="<td  border-right='0.5' width='10%' ></td>";
	xmlContent +="<td  border-right='0.5' width='10%'></td>";
//	xmlContent +="<td  border-right='0.5' width='10%'></td>";
	xmlContent +="<td border-right='0.5' width='15%' align='right'></td>";
	
	xmlContent +="</tr>";
	
	
/*	
	xmlContent +="<tr>";
	xmlContent +="<td  border-right='0.5' width='10%' align='left'></td>";
	if(taxamount!=0)
	{	
	xmlContent +="<td  border-right='0.5' width='35%' align = 'center' font-family=\"Helvetica\" font-size=\"9\"> TaxRate @"+taxrate+" </td>";
	xmlContent +="<td  border-right='0.5' width='5%'></td>";
	xmlContent +="<td  border-right='0.5' width='5%' ></td>";
	xmlContent +="<td  border-right='0.5' width='10%' ></td>";
	xmlContent +="<td  border-right='0.5' width='10%'></td>";
	xmlContent +="<td  border-right='0.5' width='10%'></td>";
	xmlContent +="<td  width='15%' align='right' font-family=\"Helvetica\" font-size=\"9\">"+ (taxamount) +"</td>";
	}
	else
	{
	xmlContent +="<td  border-right='0.5' width='35%' align = 'center'>  </td>";
	xmlContent +="<td  border-right='0.5' width='5%'></td>";
	xmlContent +="<td  border-right='0.5' width='5%' ></td>";
	xmlContent +="<td  border-right='0.5' width='10%' ></td>";
	xmlContent +="<td  border-right='0.5' width='10%'></td>";
	xmlContent +="<td  border-right='0.5' width='10%'></td>";
	xmlContent +="<td  width='15%' align='right' ></td>";
	}
	xmlContent +="</tr>";
	
*/
	/*
	xmlContent +="<tr>";
	xmlContent +="<td  border-right='0.5' width='10%' align='left'></td>";
	xmlContent +="<td  border-right='0.5' width='35%' align = 'center' font-family=\"Helvetica\" font-size=\"9\">Swachh Bharat Cess @ 0.5%</td>";
	xmlContent +="<td  border-right='0.5' width='5%'></td>";
	xmlContent +="<td  border-right='0.5' width='5%' ></td>";
	xmlContent +="<td  border-right='0.5' width='10%' ></td>";
	xmlContent +="<td  border-right='0.5' width='10%'></td>";
	xmlContent +="<td  border-right='0.5' width='10%'></td>";
	xmlContent +="<td  width='15%' align='right' font-family=\"Helvetica\" font-size=\"9\">"+ formatNumber(swachh_Bharat) +"</td>";
	xmlContent +="</tr>";
	
	xmlContent +="<tr>";
	xmlContent +="<td  border-right='0.5' width='10%' align='left'></td>";
	xmlContent +="<td  border-right='0.5' width='35%' align = 'center' font-family=\"Helvetica\" font-size=\"9\">Krishi Kalyan Cess @ 0.5%</td>";
	xmlContent +="<td  border-right='0.5' width='5%'></td>";
	xmlContent +="<td  border-right='0.5' width='5%' ></td>";
	xmlContent +="<td  border-right='0.5' width='10%' ></td>";
	xmlContent +="<td  border-right='0.5' width='10%'></td>";
	xmlContent +="<td  border-right='0.5' width='10%'></td>";
	xmlContent +="<td  width='15%' align='right' font-family=\"Helvetica\" font-size=\"9\">"+ formatNumber(krishi_kalyan) +"</td>";
	xmlContent +="</tr>";*/
	
	xmlContent +="<tr>";
	xmlContent +="<td  border-right='0.5' width='40%' align='left'></td>";
//	xmlContent +="<td  border-right='0.5' width='35%' align = 'center'></td>";
	xmlContent +="<td  border-right='0.5' width='10%'></td>";
	xmlContent +="<td  border-right='0.5' width='10%' ></td>";
	xmlContent +="<td  border-right='0.5' width='10%'></td>";
//	xmlContent +="<td  border-right='0.5' width='10%'></td>";
	xmlContent +="<td border-right='0.5' width='15%' align='right'></td>";
	
	xmlContent +="</tr>";
	
	xmlContent +="<tr>";
	xmlContent +="<td  border-right='0.5' width='40%' align='left'></td>";
//	xmlContent +="<td  border-right='0.5' width='35%' align = 'center'></td>";
	xmlContent +="<td  border-right='0.5' width='10%'></td>";
	xmlContent +="<td  border-right='0.5' width='10%' ></td>";
	xmlContent +="<td  border-right='0.5' width='10%'></td>";
//	xmlContent +="<td  border-right='0.5' width='10%'></td>";
	xmlContent +="<td border-right='0.5' width='15%' align='right'></td>";
	
	xmlContent +="</tr>";
	
xmlContent +="<tr>";
	xmlContent +="<td  border-right='0.5' width='40%' align='left'></td>";
//	xmlContent +="<td  border-right='0.5' width='35%' align = 'center'></td>";
	xmlContent +="<td  border-right='0.5' width='10%'></td>";
	xmlContent +="<td  border-right='0.5' width='10%' ></td>";
	xmlContent +="<td  border-right='0.5' width='10%'></td>";
//	xmlContent +="<td  border-right='0.5' width='10%'></td>";
	xmlContent +="<td border-right='0.5' width='15%' align='right'></td>";
	
	xmlContent +="</tr>";
	
	xmlContent +="<tr>";
	xmlContent +="<td  border-right='0.5' width='40%' align='left'></td>";
//	xmlContent +="<td  border-right='0.5' width='35%' align = 'center'></td>";
	xmlContent +="<td  border-right='0.5' width='10%'></td>";
	xmlContent +="<td  border-right='0.5' width='10%' ></td>";
	xmlContent +="<td  border-right='0.5' width='10%'></td>";
//	xmlContent +="<td  border-right='0.5' width='10%'></td>";
	xmlContent +="<td border-right='0.5' width='15%' align='right'></td>";
	
	xmlContent +="</tr>";
	
	var sub_total = o_recordOBJ.getFieldValue('subtotal')
				// nlapiLogExecution('DEBUG', 'suiteletFunction', ' Tax --> ' +
				// i_tax);

				if (!_logValidation(sub_total)) {
					sub_total = 0
				}
	
	var tax_percent = (i_tax * 100)/sub_total
	
	if(i_tax!=0){
	xmlContent +="<tr>";
	xmlContent +="<td  border-right='0.5' width='40%' align='left' font-family='Helvetica' font-size='9'>VAT@"+ parseFloat(tax_percent).toFixed(2) +"%</td>";
//	xmlContent +="<td  border-right='0.5' width='35%' align = 'center'></td>";
	xmlContent +="<td  border-right='0.5' width='10%'></td>";
	xmlContent +="<td  border-right='0.5' width='10%' ></td>";
	xmlContent +="<td  border-right='0.5' width='10%'></td>";
	xmlContent +="<td  border-right='0.5' width='10%'></td>";
	xmlContent +="<td border-right='0.5' width='15%' align='right' font-family='Helvetica' font-size='9'>"+ i_tax +"</td>";
	
	xmlContent +="</tr>";
	}
	xmlContent +="<tr>";
	xmlContent +="<td  border-right='0.5' width='40%' align='left'></td>";
//	xmlContent +="<td  border-right='0.5' width='35%' align = 'center'></td>";
	xmlContent +="<td  border-right='0.5' width='10%'></td>";
	xmlContent +="<td  border-right='0.5' width='10%' ></td>";
	xmlContent +="<td  border-right='0.5' width='10%'></td>";
//	xmlContent +="<td  border-right='0.5' width='10%'></td>";
	xmlContent +="<td border-right='0.5' width='15%' align='right'></td>";
	
	xmlContent +="</tr>";
	
	xmlContent +="<tr>";
	xmlContent +="<td  border-right='0.5' width='40%' align='left'></td>";
//	xmlContent +="<td  border-right='0.5' width='35%' align = 'center'></td>";
	xmlContent +="<td  border-right='0.5' width='10%'></td>";
	xmlContent +="<td  border-right='0.5' width='10%' ></td>";
	xmlContent +="<td  border-right='0.5' width='10%'></td>";
//	xmlContent +="<td  border-right='0.5' width='10%'></td>";
	xmlContent +="<td border-right='0.5' width='15%' align='right'></td>";
	
	xmlContent +="</tr>";
	
	xmlContent +="<tr>";
	xmlContent +="<td  border-right='0.5' width='40%' align='left'></td>";
//	xmlContent +="<td  border-right='0.5' width='35%' align = 'center'></td>";
	xmlContent +="<td  border-right='0.5' width='10%'></td>";
	xmlContent +="<td  border-right='0.5' width='10%' ></td>";
	xmlContent +="<td  border-right='0.5' width='10%'></td>";
//	xmlContent +="<td  border-right='0.5' width='10%'></td>";
	xmlContent +="<td border-right='0.5' width='15%' align='right'></td>";
	
	xmlContent +="</tr>";
	
	xmlContent +="<tr>";
	xmlContent +="<td  border-right='0.5' width='40%' align='left'></td>";
//	xmlContent +="<td  border-right='0.5' width='35%' align = 'center'></td>";
	xmlContent +="<td  border-right='0.5' width='10%'></td>";
	xmlContent +="<td  border-right='0.5' width='10%' ></td>";
	xmlContent +="<td  border-right='0.5' width='10%'></td>";
//	xmlContent +="<td  border-right='0.5' width='10%'></td>";
	xmlContent +="<td border-right='0.5' width='15%' align='right'></td>";
	
	xmlContent +="</tr>";
	
	xmlContent +="<tr>";
	xmlContent +="<td  border-right='0.5' width='40%' align='left'></td>";
//	xmlContent +="<td  border-right='0.5' width='35%' align = 'center'></td>";
	xmlContent +="<td  border-right='0.5' width='10%'></td>";
	xmlContent +="<td  border-right='0.5' width='10%' ></td>";
	xmlContent +="<td  border-right='0.5' width='10%'></td>";
//	xmlContent +="<td  border-right='0.5' width='10%'></td>";
	xmlContent +="<td border-right='0.5' width='15%' align='right'></td>";
	
	xmlContent +="</tr>";
	
	xmlContent +="<tr>";
	xmlContent +="<td  border-right='0.5' width='40%' align='left'></td>";
//	xmlContent +="<td  border-right='0.5' width='35%' align = 'center'></td>";
	xmlContent +="<td  border-right='0.5' width='10%'></td>";
	xmlContent +="<td  border-right='0.5' width='10%' ></td>";
	xmlContent +="<td  border-right='0.5' width='10%'></td>";
//	xmlContent +="<td  border-right='0.5' width='10%'></td>";
	xmlContent +="<td border-right='0.5' width='15%' align='right'></td>";
	
	xmlContent +="</tr>";
	
xmlContent +="<tr>";
	xmlContent +="<td  border-right='0.5' width='40%' align='left'></td>";
//	xmlContent +="<td  border-right='0.5' width='35%' align = 'center'></td>";
	xmlContent +="<td  border-right='0.5' width='10%'></td>";
	xmlContent +="<td  border-right='0.5' width='10%' ></td>";
	xmlContent +="<td  border-right='0.5' width='10%'></td>";
//	xmlContent +="<td  border-right='0.5' width='10%'></td>";
	xmlContent +="<td border-right='0.5' width='15%' align='right'></td>";
	
	xmlContent +="</tr>";
	

	
	//xmlContent +="</table>"
	//Billing Currency
	
	//xmlContent +="<table border='1' width='100%'>"	
		xmlContent +="<tr>";
	xmlContent +="<td  border-top='0.5' width='45%' colspan='2' align='left' font-family=\"Helvetica\" font-size=\"9\"><b>Billing Currency - "+i_currency+"</b></td>";
	//xmlContent +="<td  border-right='0.5' width='35%' align = 'center'></td>";
	xmlContent +="<td  border-top='0.5' width='15%'></td>";
	xmlContent +="<td  border-top='0.5' border-right='0.5' width='15%'></td>";
//	xmlContent +="<td border-top='0.5'  width='10%' ></td>";
	xmlContent +="<td border-top='0.5' border-right='0.5' width='5%' font-family=\"Helvetica\" font-size=\"9\" align='center' ><b>   Grand Total   </b></td>";
	//xmlContent +="<td border-top='0.5' border-right='0.5' width='10%' font-family=\"Helvetica\" font-size=\"9\" align='center' ><b>   Grand Total   </b></td>";
	
	if(i_item == 2221 || i_item == 2603 || i_item == 2612){
	
	xmlContent +="<td border-top='0.5' width='15%' align='right' font-family=\"Helvetica\" font-size=\"9\"><b>" + s_currency_symbol
				        + "&nbsp;"+ formatNumber(grand_Total) +"</b></td>";  
	
	}else{
		
		xmlContent +="<td border-top='0.5' width='15%' align='right' font-family=\"Helvetica\" font-size=\"9\"><b>" + s_currency_symbol
				        + "&nbsp;"+ formatDollar(parseFloat(i_total_amount_INV)
				                .toFixed(2)) +"</b></td>";
	}
	xmlContent +="</tr>";
	
	/*xmlContent +="<tr>";
	xmlContent +="<td  border-right='0.5' width='65%' align='left' font-size='9'><b>Billing Currency - "+currency_+"</b></td>";
	xmlContent +="<td  border-right='0.5' width='15%' align='center' font-size='9'><b>   Grand Total   </b></td>";
	xmlContent +="<td border-right='0.5' width='20%' align='right' font-size='9'><b>"+ grand_Total +"</b></td>";
	xmlContent +="</tr>";*/
	//xmlContent +="</table>"
		var currency_type;
		if(i_currency =='USD'){
			currency_type = 'Dollars';
			}
		else if(i_currency =='INR'){
			currency_type = 'Rupees';
			}
		else if(i_currency =='GBP'){
		currency_type = 'Pound';
		}
		else if(i_currency =='EUR'){
		currency_type = 'Euro';
		}
		//Amount in Words
	//	xmlContent +="<table border='1' width='100%'>"	
			xmlContent +="<tr border='0.5'>";
			xmlContent +="<td  border-right='0.5' width='45%'  font-family=\"Helvetica\" font-size=\"9\" colspan='2'><b>Amount in Words:</b></td>";
			
			if(i_item == 2221 || i_item == 2603 || i_item == 2612){
				
				xmlContent +="<td  width='55%' font-family=\"Helvetica\" font-size=\"9\" colspan='6'><b> "+ currency_type +' '+ converted_words +"</b></td>";
			}			
			else{
				
				//converted_words = number2text(parseFloat(i_total_amount_INV).toFixed(2),currency_);
				                
			xmlContent +="<td  width='55%' font-family=\"Helvetica\" font-size=\"9\" colspan='6'><b> "+ toWordsFunc(parseFloat(i_total_amount_INV).toFixed(2), i_currency) +"</b></td>";
			
			
				               
				                
			}
			xmlContent +="</tr>";
			xmlContent +="<tr border='0.5'>";
			xmlContent +="<td  width='45%'  font-family=\"Helvetica\" font-size=\"9\" colspan='2'><b>SAC Code: 998313 : </b></td>";
			xmlContent +="<td  width='55%' font-family=\"Helvetica\" font-size=\"9\" colspan='6'>Information technology (IT) consulting and support services</td>";
			
			xmlContent +="</tr>";
			
			//xmlContent +="</table>"
				
				//This is a computer generated email
				
				//xmlContent +="<table border='0.5' width='100%'>"	
				xmlContent +="<tr border='0.5'>";
      
      //From here
				xmlContent +="<td  border-right='0.5' width='50%' align='center' font-family=\"Helvetica\" font-size=\"9\" colspan='4'><br/>"
						
						xmlContent+="<img width=\"120\" height=\"120\"   src= '" + url + "'/>"
         
              xmlContent += "<br/>";
						
						
				xmlContent += "</td>";
              xmlContent +="<td  border-right='0.5' align='center' width='45%' colspan='4' font-family=\"Helvetica\" font-size=\"9\"><b>For Brillio Technologies Pvt Ltd</b><br/>"
      
       
		//colspan = \"2\"
		
      xmlContent += "<br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>";
		
		
		
              
         
         xmlContent +="<b align=\"center\" colspan = \"4\" font-size=\"8\">Authorised Signatory<\/b></td>";
					    
					     //   +"<b align='center'>Authorised Signature</b></td>";
     
  
				
				
					xmlContent +="</tr>";
					xmlContent +="</table>"	
						
						//Registered Office:
						
						xmlContent +="<table width='100%'>"	
							xmlContent +="<tr>";
							xmlContent +="<td border-left = '0.5' border-right = '0.5' align='center' font-size='7'><b>Registered office: Brillio Technologies Pvt. Ltd.,No. 58, 1st Main Road, Mini Forest,</b></td>";
							xmlContent +="</tr>";
							xmlContent +="<tr>";
							xmlContent +="<td border-left = '0.5' border-right = '0.5' border-bottom = '0.5' align='center' font-size='7'><b> J P Nagar 3rd Phase, Bangalore 560 078, Karnataka</b></td>";
								xmlContent +="</tr>";
								xmlContent +="</table>"	
	
	//xmlContent +="</table>";
		
		
		var xml = "<?xml version=\"1.0\"?>\n<!DOCTYPE pdf PUBLIC \"-//big.faceless.org//report\" \"report-1.1.dtd\">\n";
        xml += "<pdf>\n<body>"; //letter-landscape  portrait   ..size=\"letter-landscape\"
        xml += xmlContent;
        xml += "</body>\n</pdf>";
		
		var file = nlapiXMLToPDF(xml);
		var file_name = 'BTPL_' + invoice_number + '.pdf';
		file.setName(''+file_name)
		response.setContentType('PDF', ''+file_name, 'inline');
		response.write(file.getValue());
	
	}

}
catch(err){
nlapiLogExecution('ERROR','BTPL PDF Suitelet Error',err);
throw err;
}
				
				
				
				
				
				
				
				
				
				
				
				
	
	          
	
           }


         } catch (e) {
            nlapiLogExecution('ERROR',e);

         }



     }
	 
	 function _logValidation(value){
    if (value != null && value != '' && value != undefined && value.toString() != 'NaN' && value != NaN) {
        return true;
    }
    else {
        return false;
    }
}

function getMonth(month) {
	try{
		var month_Text = '';
		
		if(month =='01'){
			month_Text ='Jan';
		}
		if(month =='02'){
			month_Text ='Feb';
		}
		if(month =='03'){
			month_Text ='Mar';
		}
		if(month =='04'){
			month_Text ='Apr';
		}
		if(month =='05'){
			month_Text ='May';
		}
		if(month =='06'){
			month_Text ='Jun';
		}
		if(month =='07'){
			month_Text ='Jul';
		}
		if(month =='08'){
			month_Text ='Aug';
		}
		if(month =='09'){
			month_Text ='Sep';
		}
		if(month =='10'){
			month_Text ='Oct';
		}
		if(month =='11'){
			month_Text ='Nov';
		}
		if(month =='12'){
			month_Text ='Dec';
		}
		return month_Text;
	}
	catch(e){
		nlapiLogExecution('ERROR','BTPL PDF Suitelet Error',e);
	}
	
}

function formatNumber (num) {
    return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,")
}

function _logValidation(value) 
{
 if(value != null && value.toString() != null && value != '' && value != undefined && value.toString() != undefined && value != 'undefined' && value.toString() != 'undefined'&& value.toString() != 'NaN' && value != NaN) 
 {
  return true;
 }
 else 
 { 
  return false;
 }
}


//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



function get_start_of_week(date) {

	// nlapiLogExecution('DEBUG','date',' date -->'+date);
	// If no date object supplied, use current date
	// Copy date so don't modify supplied date
	var now = nlapiStringToDate(date)

	// set time to some convenient value
	now.setHours(0, 0, 0, 0);

	if (now.getDay() == '0') {

		// Get the previous Monday
		var monday = new Date(now);
		monday.setDate(monday.getDate() - 7 + 1);
		monday = nlapiDateToString(monday)
		// nlapiLogExecution('DEBUG','afterSubmit_calculate_OT', ' Monday --> '
		// + monday);

		// Get next Sunday
		var sunday = new Date(now);
		sunday.setDate(sunday.getDate() - sunday.getDay() + 7);
		sunday = nlapiDateToString(sunday)
		// nlapiLogExecution('DEBUG','afterSubmit_calculate_OT', ' Sunday --> '
		// + sunday);
		// Return array of date objects
		// nlapiLogExecution('DEBUG','date',' date -->'+date);
		// nlapiLogExecution('DEBUG','monday',' monday -->'+monday);

	} else {

		// Get the previous Monday
		var monday = new Date(now);
		monday.setDate(monday.getDate() - monday.getDay() + 1);
		monday = nlapiDateToString(monday)
		// nlapiLogExecution('DEBUG','afterSubmit_calculate_OT', ' Monday --> '
		// + monday);

		// Get next Sunday
		var sunday = new Date(now);
		sunday.setDate(sunday.getDate() - sunday.getDay() + 7);
		sunday = nlapiDateToString(sunday)

	}
	// nlapiLogExecution('DEBUG','date',' date -->'+date);
	// nlapiLogExecution('DEBUG','now.getDay()',now.getDay());
	return monday;
}

function get_end_of_week(date) {
	// If no date object supplied, use current date
	// Copy date so don't modify supplied date
	var now = nlapiStringToDate(date)

	// set time to some convenient value
	now.setHours(0, 0, 0, 0);
	var monday = new Date(now);
	var lastday = new Date(new Date(now).getFullYear(), new Date(now)
	        .getMonth() + 1, 0)
	// nlapiLogExecution('DEBUG','lastday',lastday.getDate());
	// nlapiLogExecution('DEBUG','monday.getDate()',monday.getDate());
	var f = parseInt(monday.getDate());
	var f1 = parseInt(lastday.getDate());
	// ----------------------------------------------------------------------
	if (f == f1) {
		var sunday = date;

		// nlapiLogExecution('DEBUG','inside if');
	} else {
		// nlapiLogExecution('DEBUG','now.getDay()', now.getDay());
		if (now.getDay() == '0') {
			// Get the previous Monday

			monday.setDate(monday.getDate() - monday.getDay() + 1);
			monday = nlapiDateToString(monday)

			// Get next Sunday
			var sunday = new Date(now);
			sunday.setDate(sunday.getDate() - 7 + 7);
			sunday = nlapiDateToString(sunday)

		} else {
			// Get the previous Monday
			// var monday = new Date(now);
			monday.setDate(monday.getDate() - monday.getDay() + 1);
			monday = nlapiDateToString(monday)
			// nlapiLogExecution('DEBUG','afterSubmit_calculate_OT', ' Monday
			// --> ' + monday);

			// Get next Sunday
			var sunday = new Date(now);
			sunday.setDate(sunday.getDate() - sunday.getDay() + 7);
			// nlapiLogExecution('DEBUG','sunday', ' sunday --> ' + sunday);
			// ------------------------------------------------------------------
			if (sunday > lastday) {
				// nlapiLogExecution('DEBUG','inside second if');
				sunday = lastday;

			}
			// -----------------------------------------------------------------
			sunday = nlapiDateToString(sunday)

		}
		// --------------------------------------------------------------------------------------

	}
	return sunday;
}

/**
 * 
 * @param {Object}
 *            value
 * 
 * Description --> If the value is blank /null/undefined returns false else
 * returns true
 */
function _logValidation(value) {
	if (value != 'null' && value != null && value.toString() != null
	        && value != '' && value != undefined
	        && value.toString() != undefined && value != 'undefined'
	        && value.toString() != 'undefined' && value.toString() != 'NaN'
	        && value != NaN) {
		return true;
	} else {
		return false;
	}
}
function get_customer_details(i_customer) {
	var i_address;
	var i_attention;
	var i_service_tax_reg_no;
	var i_CIN_No;
	var i_phone = '';
	var a_return_array = new Array();
	if (_logValidation(i_customer)) {
		var o_customerOBJ = nlapiLoadRecord('customer', i_customer)

		if (_logValidation(o_customerOBJ)) {
			i_address = o_customerOBJ.getFieldValue('defaultaddress')
			// nlapiLogExecution('DEBUG', 'get_customer_address', ' Address -->'
			// + i_address);

			i_service_tax_reg_no = o_customerOBJ.getFieldValue('vatregnumber')
			// nlapiLogExecution('DEBUG', 'get_customer_address', ' Service Tax
			// Reg No. -->' + i_service_tax_reg_no);

			i_CIN_No = o_customerOBJ.getFieldValue('custentity_cinnumber')
			// nlapiLogExecution('DEBUG', 'get_customer_address', ' CIN No .
			// -->' + i_CIN_No);

			i_phone = o_customerOBJ.getFieldValue('phone')
			// nlapiLogExecution('DEBUG', 'get_customer_address', ' Phone No .
			// -->' + i_phone);

			var i_addr_count = o_customerOBJ.getLineItemCount('addressbook')
			// nlapiLogExecution('DEBUG', 'get_customer_address', ' Adress Count
			// -->' + i_addr_count);

			if (_logValidation(i_addr_count)) {
				for (var r = 1; r <= i_addr_count; r++) {
					var i_default_billing = o_customerOBJ.getLineItemValue(
					        'addressbook', 'defaultbilling', r)
					// nlapiLogExecution('DEBUG', 'get_customer_address', '
					// Adress Count -->' + i_addr_count);

					if (i_default_billing == 'T') {
						i_attention = o_customerOBJ.getLineItemValue(
						        'addressbook', 'attention', r);
						nlapiLogExecution('DEBUG', 'beforeSubmitRecord',
						        ' Attention -->' + i_attention);

						i_addr1 = o_customerOBJ.getLineItemValue('addressbook',
						        'addr1', r);
						// nlapiLogExecution('DEBUG', 'beforeSubmitRecord', '
						// Addr1 -->' + i_addr1);

						i_addr2 = o_customerOBJ.getLineItemValue('addressbook',
						        'addr2', r);
						// nlapiLogExecution('DEBUG', 'beforeSubmitRecord', '
						// Addr2 -->' + i_addr2);

						i_city = o_customerOBJ.getLineItemValue('addressbook',
						        'city', r);
						// nlapiLogExecution('DEBUG', 'beforeSubmitRecord', '
						// City -->' + i_city);

						i_zip = o_customerOBJ.getLineItemValue('addressbook',
						        'zip', r);
						// nlapiLogExecution('DEBUG', 'beforeSubmitRecord', '
						// Zip -->' + i_zip);

						i_country = o_customerOBJ.getLineItemText(
						        'addressbook', 'country', r);
						// nlapiLogExecution('DEBUG', 'beforeSubmitRecord', '
						// Country -->' + i_country);

						i_state = o_customerOBJ.getLineItemValue('addressbook',
						        'displaystate', r);
						// nlapiLogExecution('DEBUG', 'beforeSubmitRecord', '
						// State -->' + i_state);

						break;
					}// Default Billing
				}// Address Loop
			}// Address Count
			a_return_array[0] = i_address + '######' + i_attention + '######'
			        + i_service_tax_reg_no + '######' + i_CIN_No + '######'
			        + i_phone + '######' + i_addr1 + '######' + i_addr2
			        + '######' + i_city + '######' + i_zip + '######'
			        + i_country + '######' + i_state

		}

	}

	return a_return_array;
}
function formatAndReplaceSpacesofMessage(messgaeToBeSendPara) {
	if (messgaeToBeSendPara != null && messgaeToBeSendPara != ''
	        && messgaeToBeSendPara != undefined) {
		messgaeToBeSendPara = messgaeToBeSendPara.toString();

		messgaeToBeSendPara = messgaeToBeSendPara.replace(/\n/g, "<br/>");// / /g
		return messgaeToBeSendPara;
	}

}

function get_address(i_subsidiary) {
	var i_address;
	if (_logValidation(i_subsidiary)) {
		var o_subsidiaryOBJ = nlapiLoadRecord('subsidiary', i_subsidiary)

		if (_logValidation(o_subsidiaryOBJ)) {
			i_address = o_subsidiaryOBJ.getFieldValue('addrtext')

		}
	}

	return i_address;
}

function get_address_rem(i_subsidiary) {
	var i_address;
	if (_logValidation(i_subsidiary)) {
		var o_subsidiaryOBJ = nlapiLoadRecord('subsidiary', i_subsidiary)

		if (_logValidation(o_subsidiaryOBJ)) {
			i_address_rem = o_subsidiaryOBJ.getFieldValue('mainaddress_text')
		}
	}
	return i_address_rem;
}

function removearrayduplicate(array) {
	var newArray = new Array();
	label: for (var i = 0; i < array.length; i++) {
		for (var j = 0; j < array.length; j++) {
			if (newArray[j] == array[i])
				continue label;
		}
		newArray[newArray.length] = array[i];
	}
	return newArray;
}
function get_employeeID(i_employee) {
	var i_internal_id;
	if (_logValidation(i_employee)) {
		var filter = new Array();
		filter[0] = new nlobjSearchFilter('entityid', null, 'is', i_employee);

		var columns = new Array();
		columns[0] = new nlobjSearchColumn('internalid');

		var a_search_results = nlapiSearchRecord('employee', null, filter,
		        columns);

		if (a_search_results != null && a_search_results != ''
		        && a_search_results != undefined) {
			i_internal_id = a_search_results[0].getValue('internalid');

		}

	}// Employee
	return i_internal_id;
}
// --------------------------------Function Begin - Amount in
// words----------------------------------------------------------------//
function toWordsFunc(s, i_currency) {
	var str = '';
	if (i_currency == 'USD') {
		str = '$' + ' '
	}
	if (i_currency == 'INR') {
		str = 'Rs.' + ' '
	}
	if (i_currency == 'GBP') {
		str = '�' + ' '
	}
	if (i_currency == 'SGD') {
		str = 'S$' + ' '
	}
	if (i_currency == 'Peso') {
		str = 'P' + ' '
	}if (i_currency == 'EUR') {
		str = '€'
				}

	return str + ' ' +toWords(s);
	var th = new Array('Billion ', 'Million ', 'Thousand ', 'Hundred ');
	var dg = new Array('1000000000', '1000000', '1000', '100');
	var dem = s.substr(s.lastIndexOf('.') + 1)
	s = parseInt(s)
	var d
	var n1, n2
	while (s >= 100) {
		for (var k = 0; k < 4; k++) {
			d = parseInt(s / dg[k])
			if (d > 0) {
				if (d >= 20) {
					n1 = parseInt(d / 10)
					n2 = d % 10
					printnum2(n1)
					printnum1(n2)
				} else
					printnum1(d)
				str = str + th[k]
			}
			s = s % dg[k]
		}
	}
	if (s >= 20) {
		n1 = parseInt(s / 10)
		n2 = s % 10
	} else {
		n1 = 0
		n2 = s
	}

	printnum2(n1)
	printnum1(n2)
	if (dem > 0) {
		decprint(dem)
	}
	return str

	function decprint(nm) {
		if (nm >= 20) {
			n1 = parseInt(nm / 10)
			n2 = nm % 10
		} else {
			n1 = 0
			n2 = parseInt(nm)
		}
		str = str + 'And '
		printnum2(n1)
		printnum1(n2)
	}

	function printnum1(num1) {
		switch (num1) {
			case 1:
				str = str + 'One '
			break;
			case 2:
				str = str + 'Two '
			break;
			case 3:
				str = str + 'Three '
			break;
			case 4:
				str = str + 'Four '
			break;
			case 5:
				str = str + 'Five '
			break;
			case 6:
				str = str + 'Six '
			break;
			case 7:
				str = str + 'Seven '
			break;
			case 8:
				str = str + 'Eight '
			break;
			case 9:
				str = str + 'Nine '
			break;
			case 10:
				str = str + 'Ten '
			break;
			case 11:
				str = str + 'Eleven '
			break;
			case 12:
				str = str + 'Twelve '
			break;
			case 13:
				str = str + 'Thirteen '
			break;
			case 14:
				str = str + 'Fourteen '
			break;
			case 15:
				str = str + 'Fifteen '
			break;
			case 16:
				str = str + 'Sixteen '
			break;
			case 17:
				str = str + 'Seventeen '
			break;
			case 18:
				str = str + 'Eighteen '
			break;
			case 19:
				str = str + 'Nineteen '
			break;
		}
	}

	function printnum2(num2) {
		switch (num2) {
			case 2:
				str = str + 'Twenty '
			break;
			case 3:
				str = str + 'Thirty '
			break;
			case 4:
				str = str + 'Forty '
			break;
			case 5:
				str = str + 'Fifty '
			break;
			case 6:
				str = str + 'Sixty '
			break;
			case 7:
				str = str + 'Seventy '
			break;
			case 8:
				str = str + 'Eighty '
			break;
			case 9:
				str = str + 'Ninety '
			break;
		}

	}
}
function toWords(s) {

	var th = [ '', 'thousand', 'million', 'billion', 'trillion' ];
	var dg = [ 'zero', 'one', 'two', 'three', 'four', 'five', 'six', 'seven',
	        'eight', 'nine' ];
	var tn = [ 'ten', 'eleven', 'twelve', 'thirteen', 'fourteen', 'fifteen',
	        'sixteen', 'seventeen', 'eighteen', 'nineteen' ];
	var tw = [ 'twenty', 'thirty', 'forty', 'fifty', 'sixty', 'seventy',
	        'eighty', 'ninety' ];

	s = s.toString();
	s = s.replace(/[\, ]/g, '');
	if (s != parseFloat(s))
		return 'not a number';
	var x = s.indexOf('.');
	if (x == -1)
		x = s.length;
	if (x > 15)
		return 'too big';
	var n = s.split('');
	var str = '';
	var sk = 0;
	for (var i = 0; i < x; i++) {
		if ((x - i) % 3 == 2) {
			if (n[i] == '1') {
				str += tn[Number(n[i + 1])] + ' ';
				i++;
				sk = 1;
			} else if (n[i] != 0) {
				str += tw[n[i] - 2] + ' ';
				sk = 1;
			}
		} else if (n[i] != 0) {
			str += dg[n[i]] + ' ';
			if ((x - i) % 3 == 0)
				str += 'hundred ';
			sk = 1;
		}

		if ((x - i) % 3 == 1) {
			if (sk)
				str += th[(x - i - 1) / 3] + ' ';
			sk = 0;
		}
	}
	if (x != s.length) {
		var y = s.length;
		str += 'point ';
		for (var i = x + 1; i < y; i++)
			str += dg[n[i]] + ' ';
	}
	return str.replace(/\s+/g, ' ');
}

// --------------------------------------------Function End - Amount in
// Words----------------------------------//

function formatDollar(somenum) {
	var split_arr = new Array()
	var i_no_before_comma;
	var i_no_before_comma_length;

	if (somenum != null && somenum != '' && somenum != undefined) {
		split_arr = somenum.toString().split('.')
		i_no_before_comma = split_arr[0]
		i_no_before_comma = Math.abs(i_no_before_comma)

		if (i_no_before_comma.toString().length <= 3) {
			return somenum;
		} else {
			var p = somenum.toString().split(".");

			if (p[1] != null && p[1] != '' && p[1] != undefined) {
				p[1] = p[1]
			} else {
				p[1] = '00'
			}
			return p[0].split("").reverse().reduce(
			        function(acc, somenum, i, orig) {
				        return somenum + (i && !(i % 3) ? "," : "") + acc;
			        }, "")
			        + "." + p[1];
		}
	}
}
function checkDateFormat() {
	var context = nlapiGetContext();

	var dateFormatPref = context.getPreference('dateformat');

	return dateFormatPref;
}

function get_date(date1) {
	var today;
	// ============================= Todays Date
	// ==========================================

	var offsetIST = 5.5;

	// To convert to UTC datetime by subtracting the current Timezone offset
	var utcdate = new Date(date1.getTime()
	        + (date1.getTimezoneOffset() * 60000));

	// Then cinver the UTS date to the required time zone offset like back to
	// 5.5 for IST
	//var istdate = new Date(utcdate.getTime() - ((-offsetIST * 60) * 60000));
	var istdate = date1;
	var day = istdate.getDate();

	var month = istdate.getMonth() + 1;

	var year = istdate.getFullYear();

	var date_format = checkDateFormat();
	// nlapiLogExecution('DEBUG', 'schedulerFunction', 'Date Format =='
	// +date_format);

	if (month == 1) {
		month = '01'
	} else if (month == 2) {
		month = '02'
	} else if (month == 3) {
		month = '03'
	}

	else if (month == 4) {
		month = '04'
	} else if (month == 5) {
		month = '05'
	} else if (month == 6) {
		month = '06'
	} else if (month == 7) {
		month = '07'
	} else if (month == 8) {
		month = '08'
	} else if (month == 9) {
		month = '09'
	}
	if (day == 1) {
		day = '01'
	} else if (day == 2) {
		day = '02'
	} else if (day == 3) {
		day = '03'
	}

	else if (day == 4) {
		day = '04'
	} else if (day == 5) {
		day = '05'
	} else if (day == 6) {
		day = '06'
	} else if (day == 7) {
		day = '07'
	} else if (day == 8) {
		day = '08'
	} else if (day == 9) {
		day = '09'
	}
	if (date_format == 'YYYY-MM-DD') {
		today = year + '-' + month + '-' + day;
	}
	if (date_format == 'DD/MM/YYYY') {
		today = day + '/' + month + '/' + year;
	}
	if (date_format == 'MM/DD/YYYY') {   //M/D/YYYY
		today = month + '/' + day + '/' + year;
	}
	if (date_format == 'M/D/YYYY') {   //M/D/YYYY
		today = month + '/' + day + '/' + year;
	}
	return today;
}
function header_adress(text, subsidiary) {
	var iChars = subsidiary;
	var text_new = ''

	if (text != null && text != '' && text != undefined) {
		for (var y = 0; y < text.toString().length; y++) {
			text_new += text[y]
			if (text_new == subsidiary) {
				text_new = '';
			}// Break
		}// Loop
	}// Text Validation

	nlapiLogExecution('DEBUG', 'header_adress', ' text_new -->' + text_new);
	return text_new;
}// Header Address

function escape_special_chars(text) {
	var iChars = ".@()~`!#$%^&*+=-[]\\\';,/{}|\":<>?.1234567890";
	var text_new = ''

	if (text != null && text != '' && text != undefined) {
		for (var y = 0; y < text.toString().length; y++) {
			if (iChars.indexOf(text[y]) == -1) {
				text_new += text[y]
			}

		}// Loop
	}// Text Validation
	return text_new;
}


//Added
function _nullValidation(value){
	 if (value == null || value == undefined || value == '')
	    {
	        return true;
	    }
	    else
	    {
	        return false;
	    }
}






function number2text(value,currency) {
    var fraction = Math.round(frac(value)*100);
    var f_text  = "";
	if(currency == 'INR')
	{
    if(fraction > 0) {
        f_text = "and "+convert_number(fraction)+" Paise";
    }
	}
	else if(currency =='USD')
	{
		if(fraction > 0) {
        f_text = "and "+convert_number(fraction)+" Cent";
    }
	}
    return convert_number(value,f_text)+" "+f_text+" Only";
}

function frac(f) {
    return f % 1;
}

function convert_number(number,f_text)
{
    if ((number < 0) || (number > 999999999)) 
    { 
        return "NUMBER OUT OF RANGE!";
    }
    var Gn = Math.floor(number / 10000000);  /* Crore */ 
    number -= Gn * 10000000; 
    var kn = Math.floor(number / 100000);     /* lakhs */ 
    number -= kn * 100000; 
    var Hn = Math.floor(number / 1000);      /* thousand */ 
    number -= Hn * 1000; 
    var Dn = Math.floor(number / 100);       /* Tens (deca) */ 
    number = number % 100;               /* Ones */ 
    var tn= Math.floor(number / 10); 
    var one=Math.floor(number % 10); 
    var res = ""; 

    if (Gn>0) 
    { 
        res += (convert_number(Gn) + " Crore"); 
    } 
    if (kn>0) 
    { 
            res += (((res=="") ? "" : " ") + 
            convert_number(kn) + " Lakh"); 
    } 
    if (Hn>0) 
    { 
        res += (((res=="") ? "" : " ") +
            convert_number(Hn) + " Thousand"); 
    } 

    if (Dn) 
    { 
        res += (((res=="") ? "" : " ") + 
            convert_number(Dn) + " Hundred "); 
    } 


    var ones = Array("", "One", "Two", "Three", "Four", "Five", "Six","Seven", "Eight", "Nine", "Ten", "Eleven", "Twelve", "Thirteen","Fourteen", "Fifteen", "Sixteen", "Seventeen", "Eighteen","Nineteen"); 
    var tens = Array("", "", "Twenty", "Thirty", "Fourty", "Fifty", "Sixty","Seventy", "Eighty", "Ninety"); 

    if (tn>0 || one>0) 
    { 
        if (!(res=="")) 
        { 
			if(f_text == "")
				res += " and ";
				
			else
				res += "";
        } 
        if (tn < 2) 
        { 
            res += ones[tn * 10 + one]; 
        } 
        else 
        { 

            res += tens[tn];
            if (one>0) 
            { 
                res += (" " + ones[one]); 
            } 
        } 
    }

    if (res=="")
    { 
        res = "zero"; 
    } 
    return res;
}



// END OBJECT CALLED/INVOKING FUNCTION
// =====================================================
	 
	 
	 
	 
	 
	 
  