/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       17 Jan 2019     Aazamali Khan	   This is backend suitelet to call schedule script
 *
 */

/**
 * @param {nlobjRequest} request Request object
 * @param {nlobjResponse} response Response object
 * @returns {Void} Any output is written via response object
 */
function callSchedule(request, response){
	var bodyData = request.getBody();
  	nlapiLogExecution("AUDIT","bodyData",bodyData);
	if (_logValidation(bodyData)) {
		var bodyJSON = JSON.parse(bodyData);
		var projectId = bodyJSON.projectId.id;
		nlapiLogExecution("DEBUG", "projectId", projectId);
		var oppId = bodyJSON.oppId.id;
		nlapiLogExecution("DEBUG", "oppId", oppId);
		var accId = bodyJSON.accountId.id;
		nlapiLogExecution("DEBUG", "accId", accId);
		var params = [];
		params["custscript_account"] = accId;
		params["custscript_project_id_create_frf"] = projectId;
		params["custscript_opp_id"] = oppId;
		nlapiScheduleScript("customscript_sch_createfrfdetails", "customdeploy_sch_createfrfdetails", params);
	}
	
}
function _logValidation(value){
	if (value != null && value != '' && value != undefined && value != 'NaN') {
		return true;
	}
	else {
		return false;
	}
}