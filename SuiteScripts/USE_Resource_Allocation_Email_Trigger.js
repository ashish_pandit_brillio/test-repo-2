//BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
    /*
     Script Name: USE_Resource_Allocation_Email_Trigger.js
     Author:  	  Anukaran
     Company:     Inspirria Cloud tech
     Date:	      05-08-2020
     Description: This script will compare the value of fields with old record and new record. If the any field values are changed,Script will trigger a mail.
				  Scritp will first check practice on record if Practice is Brillio Analytics internal id is 322 then it will go for compression.
	 Script Modification Log:
     -- Date --			-- Modified By --				--Requested By--				-- Description --
    
	
     */
}

function resourceAllocation_afterSubmit(type) {
    try {
		if (type == 'create') 
		{
            var id = nlapiGetRecordId();
            //nlapiLogExecution('DEBUG','ResourceAllocation_Log ','Record id ='+id);
            var rec_type = nlapiGetRecordType();
            //nlapiLogExecution('DEBUG','ResourceAllocation_Log ','Record type ='+rec_type);
            var newRec = nlapiLoadRecord(rec_type, id);

           
                var region = newRec.getFieldValue('custeventwlocation');
                //nlapiLogExecution('DEBUG','ResourceAllocation_Log ','Region ='+region);
                var region_txt = newRec.getFieldText('custeventwlocation');
                //nlapiLogExecution('DEBUG','ResourceAllocation_Log ','region_txt ='+region_txt);
                var customer_name_Details = newRec.getFieldValue('project');
                var customer_name_Details_txt = newRec.getFieldText('project');
                //nlapiLogExecution('DEBUG','ResourceAllocation_Log ','customer_name_Details_txt ='+customer_name_Details_txt);

                var split1 = new Array();
                var split1 = customer_name_Details_txt.toString().split(':');
                var customerName = split1[0];
                var projectName = split1[1];
                //nlapiLogExecution('DEBUG','Split_Log ','customerName ='+customerName);
                //nlapiLogExecution('DEBUG','Split_Log ','projectName ='+projectName);

                var executing_practice = newRec.getFieldValue('custevent_practice');
                var executing_practice_txt = newRec.getFieldText('custevent_practice');
                //nlapiLogExecution('DEBUG','ResourceAllocation_Log ','executing_practice_txt ='+executing_practice_txt);
                var employee_name = newRec.getFieldValue('allocationresource');
                var employee_name_txt = newRec.getFieldText('allocationresource');
                //nlapiLogExecution('DEBUG','ResourceAllocation_Log ','employee_name_txt ='+employee_name_txt);
                var percentageAllocated = newRec.getFieldValue('percentoftime');
                //nlapiLogExecution('DEBUG','ResourceAllocation_Log ','percentageAllocated ='+percentageAllocated);
                var allocation_Start_Date = newRec.getFieldValue('startdate');
                //nlapiLogExecution('DEBUG','ResourceAllocation_Log ','allocation_Start_Date ='+allocation_Start_Date);
                var allocation_End_Date = newRec.getFieldValue('enddate');
                //nlapiLogExecution('DEBUG','ResourceAllocation_Log ','allocation_End_Date ='+allocation_End_Date);
                var bill_rate = newRec.getFieldValue('custevent3');
                //nlapiLogExecution('DEBUG','ResourceAllocation_Log ','bill_rate ='+bill_rate);
	
          
          var employeePractise  = nlapiLookupField('employee',parseInt(employee_name),'department');
	nlapiLogExecution('audit','employee-Practise =====executing_practice',employeePractise+"====="+executing_practice);


	var di_department = nlapiGetContext().getSetting('SCRIPT', 'custscript_department')
	nlapiLogExecution('DEBUG','parameter_department',di_department);
          
          
	//Added by Sitaram on 02-02-2021 for mail trigger for resource=DI and project=non-DI
	if(executing_practice==di_department || employeePractise==di_department){
        
				var project_id_val = newRec.getFieldValue('project');
				var fields = ['custentity_projectmanager', 'custentity_deliverymanager','entityid','custentity_practice']
				var columns = nlapiLookupField('job', project_id_val, fields);
				var pm_id = columns.custentity_projectmanager
				var pm_email = nlapiLookupField('employee',parseInt(pm_id),'email')
				var dm_id = columns.custentity_deliverymanager
				var dm_email = nlapiLookupField('employee',parseInt(dm_id),'email');
				var entity_id = columns.entityid
				var project_practise = columns.custentity_practice
				  if(project_practise!=di_department){
					   DIDEMailTrigger(employee_name_txt,pm_email,dm_email,entity_id,"resourceallocation",projectName)//addded the proectname as parameter to Show the proect name also in the email
					nlapiLogExecution('audit','Allocation mail sent tagged under Non-DI project');
				  }
            
                }
               // var executing_practice = newRec.getFieldValue('custevent_practice');
				if (executing_practice == 322 || executing_practice == 511) // Brillio Analytics 1 || Brillio Analytics 1 : Brillio Analytics
				{
                    var receiptant_3users = Array();
                    //receiptant_3users[0] = 'Smitha.Thumbikkat@brillio.com';
                    receiptant_3users[0] = 'archana.r@brillio.com';
                    receiptant_3users[1] = 'senjuti.c1@brillio.com';
                    //receiptant_3users[3] = 'anukaran@inspirria.com';

                    //Table Creation

                    var strVar = '';
                    strVar += '<table width="100%" border="1">';
                    strVar += '	<tr>';
                    strVar += ' <td width="9%" font-size="11" align="center" bgcolor="#D3D3D3">Region</td>';
                    strVar += ' <td width="9%" font-size="11" align="center" bgcolor="#D3D3D3">Customer Name</td>';
                    strVar += ' <td width="9%" font-size="11" align="center" bgcolor="#D3D3D3">Project Name</td>';
                    strVar += ' <td width="9%" font-size="11" align="center" bgcolor="#D3D3D3">Executing Practice</td>';
                    strVar += ' <td width="9%" font-size="11" align="center" bgcolor="#D3D3D3">New Executing Practice</td>';
                    strVar += ' <td width="9%" font-size="11" align="center" bgcolor="#D3D3D3">Employee Name</td>';
                    strVar += ' <td width="9%" font-size="11" align="center" bgcolor="#D3D3D3">Percent Allocated</td>';
                    strVar += ' <td width="9%" font-size="11" align="center" bgcolor="#D3D3D3">New Percent Allocated</td>';
                    strVar += ' <td width="9%" font-size="11" align="center" bgcolor="#D3D3D3">Allocation Start Date</td>';
                    strVar += ' <td width="9%" font-size="11" align="center" bgcolor="#D3D3D3">Allocation End Date</td>';
                    strVar += ' <td width="9%" font-size="11" align="center" bgcolor="#D3D3D3">New Allocation Start Date</td>';
                    strVar += ' <td width="9%" font-size="11" align="center" bgcolor="#D3D3D3">New Allocation End Date</td>';
                    strVar += ' <td width="9%" font-size="11" align="center" bgcolor="#D3D3D3">Bill Rate</td>';
                    strVar += '	</tr>';

                    //Dymnamic Values
                    strVar += '	<tr>';
                    strVar += ' <td width="9%" font-size="11" align="center">' + region_txt + '</td>';
                    strVar += ' <td width="9%" font-size="11" align="center">' + customerName + '</td>';
                    strVar += ' <td width="9%" font-size="11" align="center">' + projectName + '</td>';
                    strVar += ' <td width="9%" font-size="11" align="center">' + '' + '</td>';
					strVar += ' <td width="9%" font-size="11" align="center">' + executing_practice_txt + '</td>';
                    strVar += ' <td width="9%" font-size="11" align="center">' + employee_name_txt + '</td>';
                    strVar += ' <td width="9%" font-size="11" align="center">' + '' + '</td>';
                    strVar += ' <td width="9%" font-size="11" align="center">' + percentageAllocated + '</td>';
                    strVar += ' <td width="9%" font-size="11" align="center">' + '' + '</td>';
                    strVar += ' <td width="9%" font-size="11" align="center">' + '' + '</td>';
					strVar += ' <td width="9%" font-size="11" align="center">' + allocation_Start_Date + '</td>';
					strVar += ' <td width="9%" font-size="11" align="center">' + allocation_End_Date + '</td>';
                    strVar += ' <td width="9%" font-size="11" align="center">' + bill_rate + '</td>';
                    strVar += '</tr>';
                    strVar += '</table>';

                    //Send Mail
                    var strVar_ = '';
                    strVar_ += '<html>';
                    strVar_ += '<body>';
                    strVar_ += '<p>Dear Archana/ Senjuti,</p>';
                    //strVar_ += '<br/>';
                    strVar_ += '<p>Resource allocation record is created for practice Brillio Analytics.</p>';
                    strVar_ += '<br/>';
                    strVar_ += strVar;
                    strVar_ += '<br/>';
                    strVar_ += '<p>Thanks & Regards,</p>';
                    strVar_ += '<p>Information Systems Team</p>';
                    strVar_ += '</body>';
                    strVar_ += '</html>';

                    nlapiSendEmail(442, receiptant_3users, 'Resource Allocation Creation Notification', strVar_, null, null, null);
                    nlapiLogExecution('DEBUG', 'Creation Email Sent ', 'Creation Process Completed Email Sent=>' + receiptant_3users)
                } //End of IF condition
                //rec_sub = nlapiSubmitRecord(newRec);
        } //End of if Type
		
        if (type == 'edit') 
		{
            var id = nlapiGetRecordId();
            //nlapiLogExecution('DEBUG','ResourceAllocation_Log ','Record id ='+id);
            var rec_type = nlapiGetRecordType();
            //nlapiLogExecution('DEBUG','ResourceAllocation_Log ','Record type ='+rec_type);
            var newRec = nlapiLoadRecord(rec_type, id);

            var executing_practice = newRec.getFieldValue('custevent_practice');
            if (executing_practice == 322 || executing_practice == 511) // Brillio Analytics 1 || Brillio Analytics 1 : Brillio Analytics
            {
                var region = newRec.getFieldValue('custeventwlocation');
                //nlapiLogExecution('DEBUG','ResourceAllocation_Log ','Region ='+region);
                var region_txt = newRec.getFieldText('custeventwlocation');
                //nlapiLogExecution('DEBUG','ResourceAllocation_Log ','region_txt ='+region_txt);
                var customer_name_Details = newRec.getFieldValue('project');
                var customer_name_Details_txt = newRec.getFieldText('project');
                //nlapiLogExecution('DEBUG','ResourceAllocation_Log ','customer_name_Details_txt ='+customer_name_Details_txt);

                var split1 = new Array();
                var split1 = customer_name_Details_txt.toString().split(':');
                var customerName = split1[0];
                var projectName = split1[1];
                //nlapiLogExecution('DEBUG','Split_Log ','customerName ='+customerName);
                //nlapiLogExecution('DEBUG','Split_Log ','projectName ='+projectName);

                var executing_practice = newRec.getFieldValue('custevent_practice');
                var executing_practice_txt = newRec.getFieldText('custevent_practice');
                //nlapiLogExecution('DEBUG','ResourceAllocation_Log ','executing_practice_txt ='+executing_practice_txt);
                var employee_name = newRec.getFieldValue('allocationresource');
                var employee_name_txt = newRec.getFieldText('allocationresource');
                //nlapiLogExecution('DEBUG','ResourceAllocation_Log ','employee_name_txt ='+employee_name_txt);
                var percentageAllocated = newRec.getFieldValue('percentoftime');
                //nlapiLogExecution('DEBUG','ResourceAllocation_Log ','percentageAllocated ='+percentageAllocated);
                var allocation_Start_Date = newRec.getFieldValue('startdate');
                //nlapiLogExecution('DEBUG','ResourceAllocation_Log ','allocation_Start_Date ='+allocation_Start_Date);
                var allocation_End_Date = newRec.getFieldValue('enddate');
                //nlapiLogExecution('DEBUG','ResourceAllocation_Log ','allocation_End_Date ='+allocation_End_Date);
                var bill_rate = newRec.getFieldValue('custevent3');
                //nlapiLogExecution('DEBUG','ResourceAllocation_Log ','bill_rate ='+bill_rate);

                //old Recored Values

                var recObj = nlapiGetOldRecord();

                var executing_practice_old = recObj.getFieldValue('custevent_practice');
                var executing_practice_old_txt = recObj.getFieldText('custevent_practice');
                //nlapiLogExecution('DEBUG','ResourceAllocation_OLD_Log ','executing_practice_old_txt ='+executing_practice_old_txt);
                var employee_name_old = recObj.getFieldValue('allocationresource');
                var employee_name_old_txt = recObj.getFieldText('allocationresource');
                //nlapiLogExecution('DEBUG','ResourceAllocation_OLD_Log ','employee_name_old_txt ='+employee_name_old_txt);
                var percentageAllocated_old = recObj.getFieldValue('percentoftime');
                //nlapiLogExecution('DEBUG','ResourceAllocation_OLD_Log ','percentageAllocated_old ='+percentageAllocated_old);
                var allocation_Start_Date_old = recObj.getFieldValue('startdate');
                //nlapiLogExecution('DEBUG','ResourceAllocation_OLD_Log ','allocation_Start_Date_old ='+allocation_Start_Date_old);
                var allocation_End_Date_old = recObj.getFieldValue('enddate');
                //nlapiLogExecution('DEBUG','ResourceAllocation_OLD_Log ','allocation_End_Date_old ='+allocation_End_Date_old);

                if (executing_practice != executing_practice_old || employee_name != employee_name_old ||
                    percentageAllocated != percentageAllocated_old || allocation_Start_Date != allocation_Start_Date_old ||
                    allocation_End_Date != allocation_End_Date_old) {
                    var receiptant_3users = Array();
                    //receiptant_3users[0] = 'Smitha.Thumbikkat@brillio.com';
                    receiptant_3users[0] = 'archana.r@brillio.com';
                    receiptant_3users[1] = 'senjuti.c1@brillio.com';
                    //receiptant_3users[3] = 'anukaran@inspirria.com';

                    //Table Creation

                    var strVar = '';
                    strVar += '<table width="100%" border="1">';
                    strVar += '	<tr>';
                    strVar += ' <td width="9%" font-size="11" align="center" bgcolor="#D3D3D3">Region</td>';
                    strVar += ' <td width="9%" font-size="11" align="center" bgcolor="#D3D3D3">Customer Name</td>';
                    strVar += ' <td width="9%" font-size="11" align="center" bgcolor="#D3D3D3">Project Name</td>';
                    strVar += ' <td width="9%" font-size="11" align="center" bgcolor="#D3D3D3">Executing Practice</td>';
                    strVar += ' <td width="9%" font-size="11" align="center" bgcolor="#D3D3D3">New Executing Practice</td>';
                    strVar += ' <td width="9%" font-size="11" align="center" bgcolor="#D3D3D3">Employee Name</td>';
                    strVar += ' <td width="9%" font-size="11" align="center" bgcolor="#D3D3D3">Percent Allocated</td>';
                    strVar += ' <td width="9%" font-size="11" align="center" bgcolor="#D3D3D3">New Percent Allocated</td>';
                    strVar += ' <td width="9%" font-size="11" align="center" bgcolor="#D3D3D3">Allocation Start Date</td>';
                    strVar += ' <td width="9%" font-size="11" align="center" bgcolor="#D3D3D3">Allocation End Date</td>';
                    strVar += ' <td width="9%" font-size="11" align="center" bgcolor="#D3D3D3">New Allocation Start Date</td>';
                    strVar += ' <td width="9%" font-size="11" align="center" bgcolor="#D3D3D3">New Allocation End Date</td>';
                    strVar += ' <td width="9%" font-size="11" align="center" bgcolor="#D3D3D3">Bill Rate</td>';
                    strVar += '	</tr>';

                    //Dymnamic Values
                    strVar += '	<tr>';
                    strVar += ' <td width="9%" font-size="11" align="center">' + region_txt + '</td>';
                    strVar += ' <td width="9%" font-size="11" align="center">' + customerName + '</td>';
                    strVar += ' <td width="9%" font-size="11" align="center">' + projectName + '</td>';
                    strVar += ' <td width="9%" font-size="11" align="center">' + executing_practice_old_txt + '</td>';

                    if (executing_practice_old == executing_practice) {
                        strVar += ' <td width="9%" font-size="11" align="center">' + '' + '</td>';
                    } else {
                        strVar += ' <td width="9%" font-size="11" align="center">' + executing_practice_txt + '</td>';
                    }

                    strVar += ' <td width="9%" font-size="11" align="center">' + employee_name_txt + '</td>';
                    strVar += ' <td width="9%" font-size="11" align="center">' + percentageAllocated_old + '</td>';

                    if (percentageAllocated == percentageAllocated_old) {
                        strVar += ' <td width="9%" font-size="11" align="center">' + '' + '</td>';
                    } else {
                        strVar += ' <td width="9%" font-size="11" align="center">' + percentageAllocated + '</td>';
                    }

                    strVar += ' <td width="9%" font-size="11" align="center">' + allocation_Start_Date_old + '</td>';
                    strVar += ' <td width="9%" font-size="11" align="center">' + allocation_End_Date_old + '</td>';

                    if (allocation_Start_Date == allocation_Start_Date_old) {
                        strVar += ' <td width="9%" font-size="11" align="center">' + '' + '</td>';
                    } else {
                        strVar += ' <td width="9%" font-size="11" align="center">' + allocation_Start_Date + '</td>';
                    }


                    if (allocation_End_Date == allocation_End_Date_old) {
                        strVar += ' <td width="9%" font-size="11" align="center">' + '' + '</td>';
                    } else {
                        strVar += ' <td width="9%" font-size="11" align="center">' + allocation_End_Date + '</td>';
                    }

                    strVar += ' <td width="9%" font-size="11" align="center">' + bill_rate + '</td>';
                    strVar += '</tr>';
                    strVar += '</table>';

                    //Send Mail
                    var strVar_ = '';
                    strVar_ += '<html>';
                    strVar_ += '<body>';
                    strVar_ += '<p>Dear Archana/ Senjuti,</p>';
                    //strVar_ += '<br/>';
                    strVar_ += '<p>Here is the details of fields those has changed on Resource Allocation Record.</p>';
                    strVar_ += '<br/>';
                    strVar_ += strVar;
                    strVar_ += '<br/>';
                    strVar_ += '<p>Thanks & Regards,</p>';
                    strVar_ += '<p>Information Systems Team</p>';
                    strVar_ += '</body>';
                    strVar_ += '</html>';

                    nlapiSendEmail(442, receiptant_3users, 'Resource Allocation Field Change Notification', strVar_, null, null, null);
                    nlapiLogExecution('DEBUG', 'Edit Record Email Sent ', ' Process Completed Email Sent=>' + receiptant_3users)
                } //End of IF condition
                //rec_sub = nlapiSubmitRecord(newRec);
            }
        } //End of if Type
    } //End of Try
    catch (e) {
        nlapiLogExecution('DEBUG', 'Exception results ', ' value of e-====== **************** =' + e.toString())
    } //End of Catch

} //End of Function