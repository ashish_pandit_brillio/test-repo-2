/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       03 Dec 2018     Sai Saranya
 *
 */

/**
 * @param {String} type Context Types: scheduled, ondemand, userinterface, aborted, skipped
 * @returns {Void}
 */
function scheduled(type) {
	
	try
	{
		var columns = new Array();
		columns = new nlobjSearchColumn('internalid');
		//var search = searchRecord('message','customsearch2595',null,columns);
		var search = searchRecord('deletedrecord','customsearch2617',null,columns);
		for(var index = 0; index < search.length ; index++)
		{
			var rec_id = search[index].getId();
			var context = nlapiGetContext();
			if(context.getRemainingUsage() > 1000)
			{
				nlapiYieldScript();
			}
			//var rec =nlapiLoadRecord('message',rec_id);
			nlapiDeleteRecord('message',rec_id);
          nlapiDeleteRecord('deletedrecord',rec_id);
          nlapiLogExecution('DEBUG','record',rec_id);
			
		}
	}
	catch(err)
	{
		nlapiLogExecution('Error','Error',err);
	}

}
