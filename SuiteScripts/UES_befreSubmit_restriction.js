/**
 * @author Jayesh
 */

function beforeSubmit_revenue_restriction(type)
{
	try
	{  
		var counter ='';
		var error_log = [];
		var context = nlapiGetContext();
		var currentContext = nlapiGetContext();
		var currency = nlapiGetFieldValue('currency');
		var currency_text=nlapiGetFieldText('currency');
		var subsidiary = nlapiGetFieldValue('subsidiary');
		var date = nlapiGetFieldValue('trandate');
		
		//Execute the logic only when creating a sales order with the browser UI
		if ( currentContext.getExecutionContext() == 'userinterface' || currentContext.getExecutionContext() == 'csvimport' )
		{
          	if(type == 'create')
            {
			var i_line_count = nlapiGetLineItemCount('line');
			var index_found = nlapiFindLineItemValue('line', 'account', '773');
			
			if(index_found > 0)
			{
				if(i_line_count < 30)
				{
					for (var i = 1; i <= i_line_count; i++)
					{
						var proj_name = '';
						var proj_entity_id = '';
						var proj_billing_type = '';
						var rev_rec_type = '';
						var project_id = '';
						var proj_desc_id = nlapiGetLineItemValue('line','custcol_project_entity_id',i);
						var gl_account = nlapiGetLineItemValue('line','account',i);
						var existing_practice = nlapiGetLineItemValue('line', 'department',i);
						var forgn_amount_credit = nlapiGetLineItemValue('line','credit',i);
						var forgn_amount_debit = nlapiGetLineItemValue('line','debit',i);
						if(_logValidation(forgn_amount_credit))
						{	
							var forgn_amount = forgn_amount_credit;
						}
						else if(_logValidation(forgn_amount_debit))
						{
							var forgn_amount = -(forgn_amount_debit);
						}
                      var proj_cat_fil = new Array();
					  proj_cat_fil[0] =new nlobjSearchFilter('entityid',null,'startswith',proj_desc_id);
                      var proj_cat_columns = new Array();
					  proj_cat_columns[0] = new nlobjSearchColumn('custentity_project_allocation_category');
                      var proj_cat= nlapiSearchRecord('job',null,proj_cat_fil,proj_cat_columns);
                      if(_logValidation(proj_cat))
                        {
                         var proj_cat_type = proj_cat[0].getValue('custentity_project_allocation_category');
                        }
						if(parseInt(gl_account) == parseInt(773) && (proj_desc_id) && (proj_cat_type != 6) )
						{
							s_proj_desc_split = proj_desc_id;
							s_proj_desc_split = s_proj_desc_split.trim();
							var filters_proj = new Array();
							filters_proj[0] = new nlobjSearchFilter('entityid', null, 'startswith', s_proj_desc_split);
							var column_proj = new Array();
							column_proj[0] = new nlobjSearchColumn('jobbillingtype');
							column_proj[1] = new nlobjSearchColumn('internalid');
							column_proj[2] = new nlobjSearchColumn('custentity_project_currency');
							column_proj[3] = new nlobjSearchColumn('custentity_fp_rev_rec_type');
							column_proj[4] = new nlobjSearchColumn('custentity_projectvalue');
							column_proj[5] = new nlobjSearchColumn('custentity_ytd_rev_recognized');
							column_proj[6] = new nlobjSearchColumn('custentity_sow_validation_exception','customer');
							column_proj[7]= new nlobjSearchColumn('entityid');
							var a_results_proj = nlapiSearchRecord('job', null, filters_proj, column_proj);
							if (a_results_proj)
							{
								var pro_currency=a_results_proj[0].getText('custentity_project_currency');
								if(parseInt(subsidiary) != parseInt(2) && parseInt(currency) != parseInt(1))
								{
									var rate=nlapiExchangeRate(currency_text,pro_currency);
									forgn_amount=parseFloat(forgn_amount)*parseFloat(rate);
									//forgn_amount_debit = parseFloat(f_credit_amount).toFixed(2);
								}
								project_id = a_results_proj[0].getValue('internalid');
								proj_billing_type = a_results_proj[0].getValue('jobbillingtype');
								rev_rec_type = a_results_proj[0].getValue('custentity_fp_rev_rec_type');
								var proj_name_missing=a_results_proj[0].getValue('entityid');
								if(parseInt(rev_rec_type) == parseInt(1))
								{
									var a_rev_cap_fil= [['custrecord_revenue_share_project','anyof',project_id], 'and',
														['isinactive','is','F']];
									var a_rev_col = new Array();
									a_rev_col[0] = new nlobjSearchColumn('custrecord_revenue_share_project');
									a_rev_col[1] = new nlobjSearchColumn('custrecord_revenue_share_total');
									a_rev_col[2] = new nlobjSearchColumn('created').setSort(true);
									a_rev_col[3] = new nlobjSearchColumn('custrecord_revenue_share_approval_status');
									var rev_cap_search = nlapiSearchRecord('customrecord_revenue_share', null, a_rev_cap_fil, a_rev_col);
									if(_logValidation(rev_cap_search))
									{
									var existing_rec = rev_cap_search[0].getValue('custrecord_revenue_share_project');
									var a_revenue_cap_filter = [['custrecord_revenue_share_per_practice_ca.custrecord_revenue_share_project', 'anyof', parseInt(existing_rec)], 'and',
																['custrecord_revenue_share_per_practice_su','anyof',parseInt(existing_practice)]];
					
									var a_columns_existing_cap_srch = new Array();
									a_columns_existing_cap_srch[0] = new nlobjSearchColumn('custrecord_revenue_share_per_practice_pr');
									a_columns_existing_cap_srch[1] = new nlobjSearchColumn('custrecord_revenue_share_per_practice_su');
									a_columns_existing_cap_srch[2] = new nlobjSearchColumn('custrecord_revenue_share_per_practice_re');

							a_columns_existing_cap_srch[3]=	new nlobjSearchColumn('created').setSort(true);
									var share_per_pract = nlapiSearchRecord('customrecord_revenue_share_per_practice', null, a_revenue_cap_filter, a_columns_existing_cap_srch);
									var pro_value = a_results_proj[0].getValue('custentity_projectvalue');
									var pro_ytd_value = a_results_proj[0].getValue('custentity_ytd_rev_recognized');
									if(!pro_ytd_value)
										pro_ytd_value = 0;
										
									pro_value = parseFloat(pro_value) - parseFloat(pro_ytd_value);
									if(_logValidation(share_per_pract))
									{
										var share_amount = share_per_pract[0].getValue('custrecord_revenue_share_per_practice_re');
									
										var a_filter_get_ytd_revenue_recognized = [['custrecord_project_to_recognize_amount', 'anyof', project_id],'and',
																['custrecord_subparctice_to_recognize_amnt', 'anyof' , existing_practice],'and',
																['custrecord_fp_rev_rec_type','anyof',[1]]];
								
										var a_columns_get_ytd_revenue_recognized = new Array();
										a_columns_get_ytd_revenue_recognized[0] = new nlobjSearchColumn('custrecord_project_to_recognize_amount');
										a_columns_get_ytd_revenue_recognized[1] = new nlobjSearchColumn('custrecord_practice_to_recognize_amount');
										a_columns_get_ytd_revenue_recognized[2] = new nlobjSearchColumn('custrecord_subparctice_to_recognize_amnt');
										a_columns_get_ytd_revenue_recognized[3] = new nlobjSearchColumn('custrecord_role_to_recognize_amount');
										a_columns_get_ytd_revenue_recognized[4] = new nlobjSearchColumn('custrecord_level_to_recognize_amount');
										a_columns_get_ytd_revenue_recognized[5] = new nlobjSearchColumn('custrecord_revenue_recognized');
										a_columns_get_ytd_revenue_recognized[6] = new nlobjSearchColumn('custrecord_month_to_recognize_amount');
										a_columns_get_ytd_revenue_recognized[7] = new nlobjSearchColumn('custrecord_year_to_recognize_amount');
										
										var a_get_ytd_revenue = nlapiSearchRecord('customrecord_fp_rev_rec_recognized_amnt', null, a_filter_get_ytd_revenue_recognized, a_columns_get_ytd_revenue_recognized);
										var amount_recognized='';
										var total_amount_recognized = 0;
										if(a_get_ytd_revenue)
										{
											for(var ytd_indx=0; ytd_indx < a_get_ytd_revenue.length ;ytd_indx++)
											{
												amount_recognized= a_get_ytd_revenue[ytd_indx].getValue('custrecord_revenue_recognized');
												total_amount_recognized = parseFloat(amount_recognized) + parseFloat(total_amount_recognized);
											}
										}
										/*if(parseFloat(forgn_amount) > parseFloat(share_amount))
										{
											throw "Revenue Recognized cannot be more than Revenue Share Amount Entered per practice";
											return false;
										}*/
										var total_amount= parseFloat(total_amount_recognized) + parseFloat(forgn_amount) ;
										if(parseFloat(total_amount) > parseFloat (pro_value))
										{
											//throw "Revenue Recognized cannot be more than Project PO Amount For "+proj_name_missing +"";
											//return false;
											error_log.push({
														'Msg--': "'Revenue recognition cannot be more than Project PO Amount for ptoject:-- '"+proj_name_missing
											});
										}
									}
									else
									{
										throw "Revenue Cannot be recognized against this practice";
									}
								}
							
								}
								else if(parseInt(rev_rec_type) == parseInt(4) || parseInt(rev_rec_type) == parseInt(2))
								{
									var pro_value = a_results_proj[0].getValue('custentity_projectvalue');
									var pro_ytd_value = a_results_proj[0].getValue('custentity_ytd_rev_recognized');
									if(!pro_ytd_value)
										pro_ytd_value = 0;
										
									pro_value = parseFloat(pro_value) - parseFloat(pro_ytd_value);
									
									var sow_custmr_exemption = a_results_proj[0].getValue('custentity_sow_validation_exception','customer');
									
									if(sow_custmr_exemption != 'T')
									{
										var a_filter_get_ytd_revenue_recognized = [['custrecord_project_to_recognize_amount', 'anyof', project_id],'and',
															['custrecord_subparctice_to_recognize_amnt', 'anyof' , existing_practice],'and',
															['custrecord_fp_rev_rec_type','anyof',[2,4]]];
							
										var a_columns_get_ytd_revenue_recognized = new Array();
										a_columns_get_ytd_revenue_recognized[0] = new nlobjSearchColumn('custrecord_project_to_recognize_amount');
										a_columns_get_ytd_revenue_recognized[1] = new nlobjSearchColumn('custrecord_practice_to_recognize_amount');
										a_columns_get_ytd_revenue_recognized[2] = new nlobjSearchColumn('custrecord_subparctice_to_recognize_amnt');
										a_columns_get_ytd_revenue_recognized[3] = new nlobjSearchColumn('custrecord_role_to_recognize_amount');
										a_columns_get_ytd_revenue_recognized[4] = new nlobjSearchColumn('custrecord_level_to_recognize_amount');
										a_columns_get_ytd_revenue_recognized[5] = new nlobjSearchColumn('custrecord_revenue_recognized');
										a_columns_get_ytd_revenue_recognized[6] = new nlobjSearchColumn('custrecord_month_to_recognize_amount');
										a_columns_get_ytd_revenue_recognized[7] = new nlobjSearchColumn('custrecord_year_to_recognize_amount');
										
										var a_get_ytd_revenue = nlapiSearchRecord('customrecord_fp_rev_rec_recognized_amnt', null, a_filter_get_ytd_revenue_recognized, a_columns_get_ytd_revenue_recognized);
										var amount_recognized='';
										var total_amount_recognized = 0;
										if(_logValidation(a_get_ytd_revenue))
										{
											for(var ytd_indx=0; ytd_indx < a_get_ytd_revenue.length ;ytd_indx++)
											{
												amount_recognized= a_get_ytd_revenue[ytd_indx].getValue('custrecord_revenue_recognized');
												total_amount_recognized = parseFloat(amount_recognized) + parseFloat(total_amount_recognized);
											}
										}
										if(parseFloat(forgn_amount) > parseFloat(pro_value))
										{
											//throw "Revenue Recognized cannot be more than  Project Amount   ";
											//return false;
											error_log.push({
											'Msg--':"'Revenue amount entered cannot be more than  Project Amount'"+pro_value+ "' for project '"+proj_name_missing+ "' at line:-- '"+i
											});
										}
										var total_amount= parseFloat(total_amount_recognized) + parseFloat(forgn_amount) ;
										if(parseFloat(total_amount) > parseFloat (pro_value))
										{
											//throw "Revenue Recognition cannot be more than PO value";
											//return false;
											error_log.push({
											'Msg--':"'Revenue Recognition cannot be more than  Project Amount'"+pro_value+ "' for project '"+proj_name_missing+ "' at line:-- '"+i
											});
										}
									}
								}
							}
						}
					}
				}
			}
			}
			 if(_logValidation(error_log) && error_log.length > 0){
				 var data = [];
				 var log_text = displayArrayObjects(error_log);
				 
				 nlapiLogExecution('DEBUG','JSON',JSON.stringify(log_text));
				throw nlapiCreateError('False',log_text,true);
				 
			 }
		}
	}
	catch(err)
	{
		nlapiLogExecution('ERROR','ERROR MESSAGE :- ',err);
		throw err;
	}
}
function after_submit_ytd_creation(type)
{
try
	{  
		var counter ='';
		var context = nlapiGetContext();
		var currentContext = nlapiGetContext();
		var currency = nlapiGetFieldValue('currency');
		var currency_text=nlapiGetFieldText('currency');
		var subsidiary = nlapiGetFieldValue('subsidiary');
		var date= nlapiGetFieldValue('trandate');
      	if(date)
        {
		s_currentMonthName = getMonthName(date);
		date = nlapiStringToDate(date);
		//var s_currentMonthName = date.getMonth();
		nlapiLogExecution('debug','date',date);
		var s_currentYear = date.getFullYear();
		s_currentYear =s_currentYear.toString();
		nlapiLogExecution('debug','year',s_currentYear);
        }
        var department_array = new Array();
		var a_proj_prac_details = new Array() ;
		//Execute the logic only when creating a sales order with the browser UI
		if ( currentContext.getExecutionContext() == 'userinterface' || currentContext.getExecutionContext() == 'csvimport' )
		{
			var index_found = nlapiFindLineItemValue('line', 'account', '773');
			var i_line_count = nlapiGetLineItemCount('line');
			if(index_found > 0)
			{
				if(i_line_count < 30)
				{
					for (var i = 1; i <= i_line_count; i++)
					{
						var proj_name = '';
						var proj_entity_id = '';
						var proj_billing_type = '';
						var rev_rec_type = '';
						var project_id = '';
						var gl_account = nlapiGetLineItemValue('line','account',i);
						//
						var proj_desc_id_temp = nlapiGetLineItemValue('line','custcol_project_entity_id',i);
						var proj_cat_fil = new Array();
						proj_cat_fil[0] = new nlobjSearchFilter('entityid',null,'startswith',proj_desc_id_temp);
						var proj_cat_columns = new Array();
						proj_cat_columns[0] = new nlobjSearchColumn('custentity_project_allocation_category');
						var proj_cat= nlapiSearchRecord('job',null,proj_cat_fil,proj_cat_columns);
						if(_logValidation(proj_cat))
                        {
                         var proj_cat_type = proj_cat[0].getValue('custentity_project_allocation_category');
                        }
						//
						if(parseInt(gl_account) == parseInt(773) && proj_cat_type != parseInt(6))
						{
							var existing_practice = nlapiGetLineItemValue('line', 'department',i);
							//var forgn_amount = nlapiGetLineItemValue('line','debit',i);
							var forgn_amount_credit = nlapiGetLineItemValue('line','credit',i);
							var forgn_amount_debit = nlapiGetLineItemValue('line','debit',i);
							if(_logValidation(forgn_amount_credit))
							{	
								var forgn_amount = forgn_amount_credit;
							}
							else if(_logValidation(forgn_amount_debit))
							{
								var forgn_amount = -(forgn_amount_debit);
							}
							var s_employee_id =nlapiGetLineItemValue('line','custcol_employee_entity_id',i);
							var parent_department = nlapiLookupField('department',existing_practice,'custrecord_parent_practice');
							var proj_desc_id = nlapiGetLineItemValue('line','custcol_project_entity_id',i);
							if(!forgn_amount)
								forgn_amount = 0;
								
							s_proj_desc_split = proj_desc_id;
							s_proj_desc_split = s_proj_desc_split.trim();
							var filters_proj = new Array();
							filters_proj[0] = new nlobjSearchFilter('entityid', null, 'startswith', s_proj_desc_split);
							var column_proj = new Array();
							column_proj[0] = new nlobjSearchColumn('jobbillingtype');
							column_proj[1] = new nlobjSearchColumn('internalid');
							column_proj[2] = new nlobjSearchColumn('custentity_project_currency');
							column_proj[3] = new nlobjSearchColumn('custentity_fp_rev_rec_type');
							column_proj[4] = new nlobjSearchColumn('custentity_projectvalue');
							column_proj[5] = new nlobjSearchColumn('custentity_ytd_rev_recognized');
							column_proj[6] = new nlobjSearchColumn('custentity_sow_validation_exception','customer');
							var a_results_proj = nlapiSearchRecord('job', null, filters_proj, column_proj);
							if (a_results_proj)
							{
								var pro_currency=a_results_proj[0].getText('custentity_project_currency');
								if(parseInt(subsidiary) != parseInt(2) && parseInt(currency) != parseInt(1))
								{
									var rate=nlapiExchangeRate(currency_text,pro_currency);
									forgn_amount=parseFloat(forgn_amount)*parseFloat(rate);
									//forgn_amount = parseFloat(f_credit_amount).toFixed(2);
								}
								project_id = a_results_proj[0].getValue('internalid');
								proj_billing_type = a_results_proj[0].getValue('jobbillingtype');
								rev_rec_type = a_results_proj[0].getValue('custentity_fp_rev_rec_type');
								
								var i_is_practice_alreday_present = findAllocationPracticePresent(a_proj_prac_details,existing_practice,project_id);
									
								if(i_is_practice_alreday_present >= 0)
								{
									var present_forgn_amount = a_proj_prac_details[i_is_practice_alreday_present].amount;
										present_forgn_amount = parseFloat(present_forgn_amount) +parseFloat(forgn_amount);
										
										a_proj_prac_details[i_is_practice_alreday_present]={
										'project' : project_id,
										'practice' : parent_department,
										'i_sub_practice' : existing_practice,
										'amount' : parseFloat(present_forgn_amount),
										'rev_rec_type' : rev_rec_type
										};
								}
								else
								{
										
									a_proj_prac_details.push({
										'project' : project_id,
										'practice' : parent_department,
										'i_sub_practice' : existing_practice,
										'amount' : parseFloat(forgn_amount),
										'rev_rec_type' : rev_rec_type
									});
								}
								
							}
						}
					}
					if(forgn_amount > 0)
					{
						var rec_id = nlapiGetRecordId();
						if(type == 'edit')
						{
							for(var creating_indx = 0; creating_indx < a_proj_prac_details.length; creating_indx++)
							{
								var fil=['custrecord_je_intenalid','is',rec_id];
								//new nlobjSearchFilter('custrecord_fp_othr_validate_mnth',null,'is',s_currentMonthName);
						
								var col=new Array();
								col[0] = new nlobjSearchColumn('internalid');
								col[1]= new nlobjSearchColumn('custrecord_project_to_recognize_amount');
								col[2]= new nlobjSearchColumn('custrecord_subparctice_to_recognize_amnt');
								col[3]= new nlobjSearchColumn('custrecord_revenue_recognized');
								col[4]= new nlobjSearchColumn('custrecord_month_to_recognize_amount');
								col[5]= new nlobjSearchColumn('custrecord_year_to_recognize_amount');
								var existing_ytd =  nlapiSearchRecord('customrecord_fp_rev_rec_recognized_amnt',null,fil,col);
								if(existing_ytd)
								{
									for(var del_indx=0;del_indx<existing_ytd.length;del_indx++)
									{
										var del_id=existing_ytd[del_indx].getValue('internalid');
										nlapiLogExecution('Debug','AfterSubmit',existing_ytd[del_indx].getValue('internalid'));
										nlapiLogExecution('Debug','AfterSubmit_proj',existing_ytd[del_indx].getText('custrecord_project_to_recognize_amount'));
										nlapiLogExecution('Debug','AfterSubmit_sub_prac',existing_ytd[del_indx].getText('custrecord_subparctice_to_recognize_amnt'));
										nlapiLogExecution('Debug','AfterSubmit_amnt',existing_ytd[del_indx].getValue('custrecord_revenue_recognized'));
										nlapiLogExecution('Debug','AfterSubmit_mnth_year',[existing_ytd[del_indx].getValue('custrecord_month_to_recognize_amount'),existing_ytd[del_indx].getValue('custrecord_year_to_recognize_amount')]);
									//nlapiLogExecution('Debug','AfterSubmit_amnt',existing_ytd[del_indx].getValue('custrecord_revenue_recognized'));
										var id=nlapiDeleteRecord('customrecord_fp_rev_rec_recognized_amnt',parseInt(del_id));
									}						
								}
							}
						}	
						for(var creating_indx = 0; creating_indx < a_proj_prac_details.length; creating_indx++)
						{
								var o_ytd_rcrd = nlapiCreateRecord('customrecord_fp_rev_rec_recognized_amnt');
								o_ytd_rcrd.setFieldValue('custrecord_project_to_recognize_amount', a_proj_prac_details[creating_indx].project);
								o_ytd_rcrd.setFieldValue('custrecord_practice_to_recognize_amount', a_proj_prac_details[creating_indx].practice);
								o_ytd_rcrd.setFieldValue('custrecord_subparctice_to_recognize_amnt', a_proj_prac_details[creating_indx].i_sub_practice);
								//o_ytd_rcrd.setFieldValue('custrecord_role_to_recognize_amount', a_proj_prac_details[creating_indx].emp_role);
								//o_ytd_rcrd.setFieldValue('custrecord_level_to_recognize_amount', a_proj_prac_details[creating_indx].emp_level);
								o_ytd_rcrd.setFieldValue('custrecord_revenue_recognized', a_proj_prac_details[creating_indx].amount);
								//o_ytd_rcrd.setFieldValue('custrecord_subtotal_amount', f_amount_sub_total);
								o_ytd_rcrd.setFieldValue('custrecord_month_to_recognize_amount', s_currentMonthName);
								s_currentYear = s_currentYear.split('.');
								s_currentYear = s_currentYear[0];
								o_ytd_rcrd.setFieldValue('custrecord_year_to_recognize_amount',s_currentYear);
								o_ytd_rcrd.setFieldValue('custrecord_fp_rev_rec_type',a_proj_prac_details[creating_indx].rev_rec_type);
								o_ytd_rcrd.setFieldValue('custrecord_je_intenalid', rec_id);
								//o_ytd_rcrd.setFieldValue('custrecord_resource_name',a_allocation_details[i_allo_index].i_resource_allo_text);
								var i_ytd_rcrd_id = nlapiSubmitRecord(o_ytd_rcrd, true, true);
						}
					}
				}
			}
		}
	}
	
	catch(err)
	{
		nlapiLogExecution('ERROR','ERROR MESSAGE :- ',err);
		throw err;
	}
	
}

function _logValidation(value){
    if (value != null && value != '' && value != undefined && value.toString() != 'NaN' && value != NaN) {
        return true;
    }
    else {
        return false;
    }
}

function isEmpty(value) {

	return value == null || value == "" || typeof (value) == undefined;
}

function isNotEmpty(value) {

	return !isEmpty(value);
}
function isArrayEmpty(argArray) {

	return !isArrayNotEmpty(argArray);
}

function isArrayNotEmpty(argArray) {

	return (isNotEmpty(argArray) && argArray.length > 0);
}
function findAllocationPracticePresent(a_proj_prac_details, existing_practice,project_id)
{
	for(var i_index_arr_srchng=0; i_index_arr_srchng<a_proj_prac_details.length; i_index_arr_srchng++)
	{
      if((a_proj_prac_details[i_index_arr_srchng].project) ==project_id)
        {
			if((a_proj_prac_details[i_index_arr_srchng].i_sub_practice) == (existing_practice))		
		{
			return i_index_arr_srchng;
		}
        }
	}
	return -1;
}
function getMonthName(currentDate) {
	currentDate = nlapiStringToDate(currentDate)
	var monthNames = [ "JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JUL", "AUG",
	        "SEP", "OCT", "NOV", "DEC" ];
	return monthNames[currentDate.getMonth()];
}
function displayArrayObjects(arrayObjects) {
    var len = arrayObjects.length;
    var text = "";

    for (var i = 0; i < len; i++) {
        var myObject = arrayObjects[i];
        
        for (var x in myObject) {
            text += ( x + ": " + myObject[x] + " ");
        }
        text += "<br/>";
    }

    return text;
}