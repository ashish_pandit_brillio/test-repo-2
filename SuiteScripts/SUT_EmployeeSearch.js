// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
/*
   	Script Name : SUT_EmployeeSearech.js
	Author      : ASHISH PANDIT
	Date        : 29 JAN 2019
    Description : Suitelet for Search by Employee and Search by Skills 


	Script Modification Log:

	-- Date --			-- Modified By --				--Requested By--				-- Description --
 


Below is a summary of the process controls enforced by this script file.  The control logic is described
more fully, below, in the appropriate function headers and code blocks.


     SCHEDULED FUNCTION
		- scheduledFunction(type)


     SUB-FUNCTIONS
		- The following sub-functions are called by the above core functions in order to maintain code
            modularization:

               - NOT USED

*/
}
// END SCRIPT DESCRIPTION BLOCK  ====================================
var values = new Array();
/*Suitlet Main function*/
function suiteletEmployeeSearch(request, response)
{
	try
	{
		var user = nlapiGetUser();
		var select;
		var employeeList = getEmployeeList('customsearch_employee_search',select,'Select Employee');
		//nlapiLogExecution('Debug','employeeList ',employeeList);
		
		var arrSkills = [];
		var searchResult = getSkillsList();
		if(searchResult){
			for (var int = 0; int < searchResult.length; int++) {
				var dataOut = {};
				dataOut.id = searchResult[int].getValue('custrecord_emp_primary_skill');
				dataOut.name = searchResult[int].getText('custrecord_emp_primary_skill');
				arrSkills.push(dataOut);
			}
		}
		//nlapiLogExecution('Debug','employeeList ',employeeList);
		//nlapiLogExecution('Debug','arrSkills ',arrSkills);
		values['{employeelist}'] = employeeList;
		values['{skill_list}'] = JSON.stringify(arrSkills);
		values['{user}'] = nlapiGetContext().getName();
		values['{designation}'] = nlapiLookupField('employee',user,'title');
		values['{homeicon}'] = nlapiResolveURL('SUITELET','customscript_fuel_sut_fulfillment_dash','customdeploy_fuel_sut_fulfillment_dash');
		var select = "";
		
		values["{employee_search}"] = getSearchValues('customsearch_employee_search',select,'Select Employee');
		values["{skill_search}"] = getSearchValues('customsearch_employee_search',select,'Select Employee');
		
		/*Login user image*/
		if(nlapiLookupField('employee',user,'gender') == 'f')
		{
			values['{profilepic}'] = '/core/media/media.nl?id=1257271&c=3883006&h=ad48de7349910d7225a6';
		}else{
			values['{profilepic}'] = '/core/media/media.nl?id=1257271&c=3883006&h=ad48de7349910d7225a6';
		}
		
		var contents = nlapiLoadFile('1269195').getValue();
		contents = replaceValues(contents, values);
		//response.writeLine(JSON.stringify(arrayObj));
		response.write(contents);
	}
	catch(error)
	{
		nlapiLogExecution('Debug','Error ',error);
	}
}

function replaceValues(content, oValues)
{
	for(param in oValues)
	{
		// Replace null values with blank
		var s_value	=	(oValues[param] == null)?'':oValues[param];

		// Replace content
		//content = content.replace(new RegExp('{{' + param + '}}','gi'), s_value);
		content = content.replace(param, s_value);
	}

	return content;
}

/*********************************************Function to get Employee Level Values**********************************************/
function getEmpLevelValues(s_list_name, strSelectedValue, s_placeholder, removeBlankOption)
{
	var columns = new Array();
	columns[0]	=	new nlobjSearchColumn("employeestatus",null,"GROUP");
	
	var a_search_results = searchRecord(null, s_list_name, null, columns);
	
	var list_values = new Array();
	
	for(var i = 0; a_search_results != null && i < a_search_results.length; i++)
		{
			list_values.push({'display':a_search_results[i].getText("employeestatus",null,"GROUP"), 'value':a_search_results[i].getText("employeestatus",null,"GROUP")});
		}
	
	return getOptions(list_values, strSelectedValue, s_placeholder, removeBlankOption);
}

/*********************************************Function to get List Values**********************************************/
function getListValues(s_list_name, strSelectedValue, s_placeholder, removeBlankOption)
{
	var columns = new Array();
	columns[0]	=	new nlobjSearchColumn('name');
	
	var a_search_results = searchRecord(s_list_name, null, null, columns);
	
	var list_values = new Array();
	
	for(var i = 0; a_search_results != null && i < a_search_results.length; i++)
		{
			list_values.push({'display':a_search_results[i].getValue('name'), 'value':a_search_results[i].id});
		}
	
	return getOptions(list_values, strSelectedValue, s_placeholder, removeBlankOption);
}

/*********************************************Function to get Search Results**********************************************/
function searchRecord(recordType, savedSearch, arrFilters, arrColumns,
        filterExpression)
{

	try {
		var search = null;

		// if a saved search is provided, load it and add the filters and
		// columns
		if (isNotEmpty(savedSearch)) {
			search = nlapiLoadSearch(recordType, savedSearch);

			if (isArrayNotEmpty(arrFilters)) {
				search.addFilters(arrFilters);
			}

			if (isArrayNotEmpty(arrColumns)) {
				search.addColumns(arrColumns);
			}

			if (isArrayNotEmpty(filterExpression)) {
				search.setFilterExpression(filterExpression);
			}
		}
		// create a new search
		else {
			search = nlapiCreateSearch(recordType, arrFilters, arrColumns);
		}

		// run search
		var resultSet = search.runSearch();

		// iterate through the search and get all data 1000 at a time
		var searchResultCount = 0;
		var resultSlice = null;
		var searchResult = [];

		do {
			resultSlice = resultSet.getResults(searchResultCount,
			        searchResultCount + 1000);

			if (resultSlice) {

				resultSlice.forEach(function(result) {

					searchResult.push(result);
					searchResultCount++;
				});
			}
		} while (isArrayNotEmpty(resultSlice) && resultSlice.length >= 1000);

		return searchResult;
	} catch (err) {
		nlapiLogExecution('ERROR', 'searchRecord', err);
		throw err;
	}
}

function isEmpty(value) {

	return value == null || value == "" || typeof (value) == undefined;
}

function isNotEmpty(value) {

	return !isEmpty(value);
}

function isArrayEmpty(argArray) {

	return !isArrayNotEmpty(argArray);
}

function isArrayNotEmpty(argArray) {

	return (isNotEmpty(argArray) && argArray.length > 0);
}
/*********************************************Function for adding selected options**********************************************/
function getOptions(a_data, i_selected_value, s_placeholder, removeBlankOptions)
{
	var strOptions	=	'';
	
	if(removeBlankOptions == true)
		{
		
		}
	else
		{
			strOptions = '<option value = "">' + (s_placeholder == undefined?'':s_placeholder) + '</option>';
		}

	var strSelected = '';
	
	for(var i = 0; i < a_data.length; i++)
	{
		if(i_selected_value == a_data[i].value)
			{
				strSelected = 'selected';
			}
		else
			{
				strSelected = '';
			}
		strOptions += '<option value = "'+a_data[i].value+'" ' + strSelected + ' >' + a_data[i].display + '</option>';
	}
//nlapiLogExecution('Debug', 'stringsss', strOptions);
	return strOptions;
}
/*********************************************Function to get Search Values**********************************************/
function getSearchValues(s_list_name, strSelectedValue, s_placeholder, removeBlankOption)
{
	var columns = new Array();
	columns[0]	=	new nlobjSearchColumn('entityid');
	
	var a_search_results = searchRecord(null, s_list_name, null, columns);
	
	var list_values = new Array();
	
	for(var i = 0; a_search_results != null && i < a_search_results.length; i++)
		{
			list_values.push({'name':a_search_results[i].getValue('entityid'), 'id':a_search_results[i].id});
		}
	//nlapiLogExecution('Debug','list_values ',JSON.stringify(list_values));
	return JSON.stringify(list_values);//getOptions(list_values, strSelectedValue, s_placeholder, removeBlankOption);
}
/*********************************************Function to get Search Values**********************************************/
function getEmployeeList(s_list_name, strSelectedValue, s_placeholder, removeBlankOption)
{
	var columns = new Array();
	columns[0]	=	new nlobjSearchColumn('entityid');
	
	var a_search_results = searchRecord(null, s_list_name, null, columns);
	
	var list_values = new Array();
	
	for(var i = 0; a_search_results != null && i < a_search_results.length; i++)
		{
				var s_empName = a_search_results[i].getValue('entityid'); 
				arr_name = s_empName.indexOf("-");
				if(arr_name>0)
				{
					var resourceName = s_empName.split("-")[1];
				}
				else{
					var resourceName = s_empName;
				}
				
			list_values.push({'name':resourceName, 'id':a_search_results[i].id});
		}
	//nlapiLogExecution('Debug','list_values ',JSON.stringify(list_values));
	return JSON.stringify(list_values);//getOptions(list_values, strSelectedValue, s_placeholder, removeBlankOption);
}
/******************************Search To Get Skill List************************************/	
function getSkillsList() {
	try {
			var skill_filter = 
		        [
		            //[ 'custrecord_employee_skil_family', 'is', i_skillFamily],               
		              //'and',
					[ 'isinactive', 'is', 'F' ] 
				];

		var skill_search_results = searchRecord('customrecord_employee_skills', null, skill_filter,
		        [ 
					new nlobjSearchColumn("custrecord_emp_primary_skill"),
					new nlobjSearchColumn("internalid")
				]);

		nlapiLogExecution('debug', 'Results count',skill_search_results.length);

		if (skill_search_results.length == 0) {
			throw "You don't have any Primary Skills under Family Selected.";
		}
	
		return skill_search_results;
	} catch (err) {
		nlapiLogExecution('ERROR', 'getTaggedProjectList', err);
		throw err;
	}
}
