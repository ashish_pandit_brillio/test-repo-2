/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       03 Dec 2018     Sai Saranya
 *
 */

/**
 * @param {String} recType Record type internal id
 * @param {Number} recId Record internal id
 * @returns {Void}
 */
	function massUpdate(recType, recId) {
	    
	    nlapiDeleteRecord('message', recId);

	}


