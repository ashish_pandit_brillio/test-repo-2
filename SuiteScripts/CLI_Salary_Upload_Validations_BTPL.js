// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
/*
   	Script Name : CLI_Salary_Upload_Validations_BTPL.js
	Author      : Jayesh Dinde
	Date        : 6 April 2016
    Description : Validations on Salary Upload Process for BTPL Record


	Script Modification Log:

	-- Date --			-- Modified By --				--Requested By--				-- Description --




	Below is a summary of the process controls enforced by this script file.  The control logic is described
	more fully, below, in the appropriate function headers and code blocks.

     PAGE INIT
		- pageInit(type)


     SAVE RECORD
		- saveRecord()


     VALIDATE FIELD
		- validateField(type, name, linenum)


     FIELD CHANGED
		- fieldChanged(type, name, linenum)


     POST SOURCING
		- postSourcing(type, name)


	LINE INIT
		- lineInit(type)


     VALIDATE LINE
		- validateLine()


     RECALC
		- reCalc()


     SUB-FUNCTIONS
		- The following sub-functions are called by the above core functions in order to maintain code
            modularization:



*/
}
// END SCRIPT DESCRIPTION BLOCK  ====================================



// BEGIN SCRIPT UPDATION BLOCK  ====================================
/*


*/
// END SCRIPT UPDATION BLOCK  ====================================




// BEGIN GLOBAL VARIABLE BLOCK  =====================================
{

	//  Initialize any Global Variables, in particular, debugging variables...


}
// END GLOBAL VARIABLE BLOCK  =======================================





// BEGIN PAGE INIT ==================================================

function pageInit_validations(type)
{
	
	/*var button = window.document.getElementById('tbl__cancel')
	button.style.visibility = "hidden";*/
	
    /*var button_1 = window.document.getElementById('custrecord_csv_file_s_p_popup_list')
	button_1.style.visibility = "hidden";
		
	var button_2 = window.document.getElementById('custrecord_csv_file_s_p_popup_link')
	button_2.style.visibility = "hidden";*/

    if(type == 'create')
	{
	 	var i_file_status = nlapiGetFieldValue('custrecord_status_btpl')
    
	    if(i_file_status == '' || i_file_status == undefined || i_file_status == null)
		{
			nlapiSetFieldValue('custrecord_status_btpl',4);
		}
		
	}//Create
	
	if(type == 'edit')
	{
		nlapiDisableField('custrecord_rate_type',true);
	}
	
}//Page Init V

// END PAGE INIT ====================================================

// BEGIN SAVE RECORD ================================================

function saveRecord_salary_upload() //
{
	return true;
}

// END SAVE RECORD ==================================================

// BEGIN VALIDATE FIELD =============================================

function validateField(type, name, linenum)
{

	/*  On validate field:

          - EXPLAIN THE PURPOSE OF THIS FUNCTION


          FIELDS USED:

          --Field Name--				--ID--


    */


	//  LOCAL VARIABLES



	//  VALIDATE FIELD CODE BODY


	return true;

}

// END VALIDATE FIELD ===============================================

// BEGIN FIELD CHANGED ==============================================

function fieldChanged_salary_upload(type, name, linenum) //
{
	//nlapiDisableField('custrecord_status_btpl', true);
	
	if(name == 'custrecord_subsidiary_btpl' || name == 'custrecord_month_btpl' || name == 'custrecord_year_btpl' || name == 'custrecord_rate_type')
	{
		var execute_err_flag = 0;
		var vendor_pro_mnth = nlapiGetFieldValue('custrecord_month_btpl');
		if (_logValidation(vendor_pro_mnth))
		{
			var vendor_pro_year = nlapiGetFieldValue('custrecord_year_btpl');
			if (_logValidation(vendor_pro_year))
			{
				var vendor_pro_subsidiary = nlapiGetFieldValue('custrecord_subsidiary_btpl');
				if (_logValidation(vendor_pro_subsidiary))
				{
					var rate_type = nlapiGetFieldValue('custrecord_rate_type');
					if (_logValidation(rate_type))
					{
						execute_err_flag = 1;
					}
				}
			}
			
		}
		
		if(execute_err_flag == 1)
		{
			var filters = new Array();
			filters[0] = new nlobjSearchFilter('custrecord_month_btpl', null, 'is', vendor_pro_mnth);
			filters[1] = new nlobjSearchFilter('custrecord_year_btpl', null, 'is', vendor_pro_year);
			filters[2] = new nlobjSearchFilter('custrecord_subsidiary_btpl', null, 'is', vendor_pro_subsidiary);
			filters[3] = new nlobjSearchFilter('custrecord_rate_type', null, 'is', rate_type);
			var a_results = nlapiSearchRecord('customrecord_salary_upload_process_btpl', null, filters, null);
			if (a_results != null)
			{
				if (_logValidation(a_results))
				{
					alert("Vendor Provision Record already exist for this month and year, kindly delete that record data first!");
					var rcrd_id = a_results[0].getId();
					var url = nlapiResolveURL('RECORD', 'customrecord_salary_upload_process_btpl', rcrd_id);
   					document.location=url;
					window.onbeforeunload = null;
					//nlapiSetRedirectURL('RECORD', 'customrecord_salary_upload_process_btpl', rcrd_id, true);
				}
			}
			return true;
		}
		
		return true;
	}
	
	return true;
		
}//Field Change S U

// END FIELD CHANGED ================================================





// BEGIN POST SOURCING ==============================================

function postSourcing(type, name)
{

    /*  On post sourcing:

	- PURPOSE

          FIELDS USED:

		--Field Name--			--ID--		--Line Item Name--


    */

    //  LOCAL VARIABLES


    //  POST SOURCING CODE BODY



}

// END POST SOURCING ================================================



// BEGIN LINE INIT ==============================================

function lineInit(type)
{

    /*  On Line Init:

	- PURPOSE

          FIELDS USED:

		--Field Name--			--ID--		--Line Item Name--


    */

    //  LOCAL VARIABLES


    //  LINE INIT CODE BODY



}

// END LINE INIT ================================================


// BEGIN VALIDATE LINE ==============================================

function validateLine(type)
{

	/*  On validate line:

          - EXPLAIN THE PURPOSE OF THIS FUNCTION


          FIELDS USED:

          --Field Name--				--ID--


    */


	//  LOCAL VARIABLES


	//  VALIDATE LINE CODE BODY


	return true;

}

// END VALIDATE LINE ================================================




// BEGIN RECALC =====================================================

function recalc(type)
{

	/*  On recalc:

          - EXPLAIN THE PURPOSE OF THIS FUNCTION


          FIELDS USED:

          --Field Name--				--ID--


    */


	//  LOCAL VARIABLES


	//  RECALC CODE BODY


}

// END RECALC =======================================================





// BEGIN FUNCTION ===================================================

function validate_process(rcrd_id) //
{	
	window.open('/app/site/hosting/scriptlet.nl?script=860&deploy=1&custscript_record_id_su=' +rcrd_id,'_self', null);
}//Validate/Process

function Down_error_logs(vendor_subsidiary_sub_id,vendor_provision_year,vendor_provision_subsidiary,vendor_provision_month,rate_type)
{
	window.open('/app/site/hosting/scriptlet.nl?script=863&deploy=1&vendor_provision_month=' +vendor_provision_month+ '&vendor_provision_year=' +vendor_provision_year+ '&vendor_provision_subsidiary=' +vendor_provision_subsidiary+ '&vendor_subsidiary_sub_id=' +vendor_subsidiary_sub_id+ '&rate_type=' +rate_type,'_self', null);
}

function Load_Data(vendor_subsidiary_sub_id,vendor_provision_year,vendor_provision_debit_acnt,vendor_provision_credit_acnt,vendor_provision_criteria,vendor_provision_subsidiary,vendor_provision_month,flag,rate_type)
{
	if (flag != 1)
	{
		var vendor_provision_month = nlapiGetFieldText('custrecord_month_btpl');
		var vendor_provision_year = nlapiGetFieldText('custrecord_year_btpl');
		var vendor_provision_debit_acnt = nlapiGetFieldText('custrecord_account_debit_s_p_btpl');
		var vendor_provision_credit_acnt = nlapiGetFieldText('custrecord_account_credit_s_p_btpl');
		var vendor_provision_criteria = nlapiGetFieldText('custrecord_criteria_btpl');
		var vendor_provision_subsidiary = nlapiGetFieldText('custrecord_subsidiary_btpl');
		var vendor_subsidiary_sub_id = nlapiGetFieldValue('custrecord_subsidiary_btpl');
		var rate_type = nlapiGetFieldValue('custrecord_rate_type');
	}
	
	var params=new Array();
	params['custscript_vendor_provision_month'] = vendor_provision_month;
	params['custscript_vendor_provision_year'] = vendor_provision_year;
	params['custscript_vendor_provision_debit_acnt'] = vendor_provision_debit_acnt;
	params['custscript_vendor_provision_credit_acnt'] = vendor_provision_credit_acnt;
	params['custscript_vendor_provision_criteria'] = vendor_provision_criteria;
	params['custscript_vendor_provision_subsidiary'] = vendor_provision_subsidiary;
	params['custscript_vendor_subsidiary_sub_id'] = vendor_subsidiary_sub_id;
	params['custscript_vendor_rate_type'] = rate_type;
			 
	window.open('/app/site/hosting/scriptlet.nl?script=862&deploy=1&custscript_vendor_provision_month=' +vendor_provision_month+ '&custscript_vendor_provision_year=' +vendor_provision_year+ '&custscript_vendor_provision_debit_acnt=' +vendor_provision_debit_acnt+ '&custscript_vendor_provision_credit_acnt=' +vendor_provision_credit_acnt+ '&custscript_vendor_provision_criteria=' +vendor_provision_criteria+ '&custscript_vendor_provision_subsidiary=' +vendor_provision_subsidiary+ '&custscript_vendor_subsidiary_sub_id=' +vendor_subsidiary_sub_id+ '&custscript_vendor_rate_type=' +rate_type);
	//nlapiSetRedirectURL('SUITELET', 'customscript_vendor_provision_data', 'customdeploy_vendor_provision_data', null,params);
}

function refresh_salary_upload(which_bttn_clickd)
{
	which_bttn_clickd = which_bttn_clickd.toString();
	which_bttn_clickd = which_bttn_clickd.trim();
	var i_percent_complete = '';
	var s_script_status = '';
	var column = new Array();
	var filters = new Array();
	filters.push(new nlobjSearchFilter('status', null, 'anyof', ['PROCESSING', 'PENDING','COMPLETE']));	
	if(which_bttn_clickd == 'VALIDATE')
	{
		filters.push(new nlobjSearchFilter('internalid','script','is','861')); //VALIDATE MONTHLY
	}
	else if(which_bttn_clickd == 'VALIDATE_HOURLY')
	{
		filters.push(new nlobjSearchFilter('internalid','script','is','909')); //VALIDATE HOURLY
	}
	else if(which_bttn_clickd == 'CREATE_JE')
	{
		filters.push(new nlobjSearchFilter('internalid','script','is','867')); //CREATE_JE MONTHLY
	}
	else if(which_bttn_clickd == 'CREATE_JE_HOURLY')
	{
		filters.push(new nlobjSearchFilter('internalid','script','is','910')); //CREATE_JE HOURLY
	}
	else if(which_bttn_clickd == 'DELETE_JE')
	{
		filters.push(new nlobjSearchFilter('internalid','script','is','865')); //DELETE_JE MONTHLY
	}
	else
	{
		filters.push(new nlobjSearchFilter('internalid','script','is','908')); // DELETE_JE HOURLY
	}
	
	column.push(new nlobjSearchColumn('name', 'script'));
	column.push(new nlobjSearchColumn('internalid', 'scriptdeployment'));
	column.push(new nlobjSearchColumn('datecreated'));
	column.push(new nlobjSearchColumn('status'));
	column.push(new nlobjSearchColumn('startdate'));
	column.push(new nlobjSearchColumn('enddate'));
	column.push(new nlobjSearchColumn('queue'));
	column.push(new nlobjSearchColumn('percentcomplete'));
	column.push(new nlobjSearchColumn('queueposition'));
	column.push(new nlobjSearchColumn('percentcomplete'));

	var a_search_results =  nlapiSearchRecord('scheduledscriptinstance', null, filters, column);
	if(_logValidation(a_search_results)) 
    {
		for(var i=0;i<a_search_results.length;i++)
    	{
    		s_script  = a_search_results[i].getValue('name', 'script');

			var d_date_created  = a_search_results[i].getValue('datecreated');

           	i_percent_complete  = a_search_results[i].getValue('percentcomplete');

			var s_script_status  = a_search_results[i].getValue('status');
		   
    	}
    }
	
	if(i_percent_complete =='' || i_percent_complete == null || i_percent_complete == undefined)
	{
		s_script_status = 'Not Started';
		i_percent_complete = '0%';
	}

	if(s_script_status == 'Complete')
	{
		i_percent_complete = '100%';
		s_script_status = 'Complete';
	}
		
	nlapiSubmitField('customrecord_salary_upload_process_btpl',nlapiGetRecordId(),'custrecord_completion_status',s_script_status);				
	nlapiSubmitField('customrecord_salary_upload_process_btpl',nlapiGetRecordId(),'custrecord_prcnt_complete',i_percent_complete);
	location.reload();
		
}//Refresh Salary Upload

function delete_vendor_sal_upload_rcrd(vendor_provision_month,vendor_provision_year,rcrd_id,rate_type,vendor_provision_subsidiary_id)
{	
	var confirm_del = confirm("Do you really want to DELETE this record !?");
	if (confirm_del == false) // If user cancels to delete the record... terminate the script.
	{
		return false;
	}
	
	window.open('/app/site/hosting/scriptlet.nl?script=864&deploy=1&custscript_vendor_provision_month=' +vendor_provision_month+ '&custscript_vendor_provision_year=' +vendor_provision_year+ '&rcrd_id=' +rcrd_id+ '&rate_type=' +rate_type+ '&vendor_provision_subsidiary=' +vendor_provision_subsidiary_id,'_self');

}

function create_journal_entry(rcrd_id,vendor_provision_month,vendor_provision_year,vendor_provision_debit_acnt,vendor_provision_credit_acnt,vendor_provision_subsidiary_id)
{
	window.open('/app/site/hosting/scriptlet.nl?script=866&deploy=1&rcrd_id=' +rcrd_id+ 
	'&vendor_provision_month=' +vendor_provision_month+
	'&vendor_provision_year=' +vendor_provision_year+
	'&vendor_provision_debit_acnt=' +vendor_provision_debit_acnt+
	'&vendor_provision_credit_acnt=' +vendor_provision_credit_acnt+
	'&vendor_provision_subsidiary=' +vendor_provision_subsidiary_id,'_self');
}

function Xport_JE_Data(vendor_provision_year,vendor_provision_month,vendor_provision_subsidiary_id,vendor_provision_rate_type)
{
	window.open('/app/site/hosting/scriptlet.nl?script=947&deploy=1&vendor_provision_month=' +vendor_provision_month+ '&vendor_provision_year=' +vendor_provision_year+ '&vendor_provision_subsidiary=' +vendor_provision_subsidiary_id+ '&vendor_provision_rate_type=' +vendor_provision_rate_type,'_self', null);
}


/**
 * 
 * @param {Object} value
 * 
 * Description --> If the value is blank /null/undefined returns false else returns true
 */
function _logValidation(value) 
{
 if(value.toString()!='null' && value != null && value.toString() != null && value != '' && value != undefined && value.toString() != undefined && value != 'undefined' && value.toString() != 'undefined'&& value.toString() != 'NaN' && value != NaN) 
 {
  return true;
 }
 else 
 { 
  return false;
 }
}
// END FUNCTION =====================================================
