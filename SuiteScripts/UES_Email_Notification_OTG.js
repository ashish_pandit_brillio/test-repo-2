/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       09 Apr 2020     deepak.srinivas
 *
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType
 * 
 * @param {String} type Operation types: create, edit, delete, xedit,
 *                      approve, cancel, reject (SO, ER, Time Bill, PO & RMA only)
 *                      pack, ship (IF only)
 *                      dropship, specialorder, orderitems (PO only) 
 *                      paybills (vendor payments)
 * @returns {Void}
 */
function userEventAfterSubmit(type){
  try{
	  nlapiLogExecution('debug', 'type', type);
	  var currentContext = nlapiGetContext();
	  nlapiLogExecution('debug', 'Context', currentContext.getExecutionContext());
		if (type == "create" && currentContext.getExecutionContext() == 'userinterface') {
			var expenseId = nlapiGetRecordId();
			if (nlapiLookupField('expensereport', expenseId, 'statusRef') == 'pendingSupApproval') {
				sendMailsForCreate(expenseId);
			}
		}

		if ((type == "edit" || type == "approve") && (currentContext.getExecutionContext() == 'userinterface' || currentContext.getExecutionContext() == 'userevent')) {
			nlapiLogExecution('DEBUG', 'Trigger On Approve','Trigger On Approve');
			var expenseId = nlapiGetRecordId();
			//ndMailsForCreate(expenseId);
			sendMailsForEdit(expenseId);
		}
	  
  }
  catch (err) {
		nlapiLogExecution('ERROR', 'userEventAfterSubmit', err);
	}
}

function sendMailsForEdit(expenseId) {

	try {

		var oldRecord = nlapiGetOldRecord();
		var newStatus = nlapiLookupField('expensereport', nlapiGetRecordId(),
		        'statusRef');
		var oldStatus = oldRecord.getFieldValue('statusRef');
		nlapiLogExecution('debug', 'old status', oldStatus);
		nlapiLogExecution('debug', 'new status', newStatus);

		// check if the status has changed
		if (newStatus != oldStatus) {

			switch (newStatus) {

				case "inProgress":
				break;
				case "pendingSupApproval":

					if (oldStatus == "inProgress") {
						sendMailsForCreate(expenseId);
					}
				break;
				case "pendingAcctApproval":

					if (oldStatus == "pendingSupApproval") {
						sendMailsForCreate(expenseId);
					}
				break;
				
			}
		}
	} catch (err) {
		nlapiLogExecution('ERROR', 'sendMailsForEdit', err);
	}
}


function sendMailsForCreate(expenseId){
	try{
		//Table Creation
		// Table For Practice total count
		//strVar += ' <tr><td width="20%" font-size="11" align="center">Total Head Count Detail</td></tr>';
		//strVar += '	<tr>';
		//strVar += ' <td width="100%">';
		if(expenseId){
		var expRec = nlapiLoadRecord('expensereport',parseInt(expenseId));
		var strVar = '';
		strVar += '<table width="100%" border="1">';
	
		strVar += '	<tr>';
		strVar += ' <td width="9%" font-size="11" align="center" bgcolor="#D3D3D3">Internal ID</td>';
		strVar += ' <td width="9%" font-size="11" align="center" bgcolor="#D3D3D3">Expense Number</td>';
		strVar += ' <td width="9%" font-size="11" align="center" bgcolor="#D3D3D3">Employee</td>';
		strVar += ' <td width="9%" font-size="11" align="center" bgcolor="#D3D3D3">Subsidiary</td>';
		strVar += ' <td width="9%" font-size="11" align="center" bgcolor="#D3D3D3">1st Level Approver</td>';
		strVar += ' <td width="9%" font-size="11" align="center" bgcolor="#D3D3D3">Status</td>';
		strVar += ' <td width="9%" font-size="11" align="center" bgcolor="#D3D3D3">Date</td>';
		
		strVar += '	</tr>';
		
		//Values
		strVar += '	<tr>';
		strVar += ' <td width="9%" font-size="11" align="center">' + expenseId + '</td>';
		strVar += ' <td width="9%" font-size="11" align="center">' + expRec.getFieldValue('tranid') + '</td>';
		strVar += ' <td width="9%" font-size="11" align="center">' + expRec.getFieldText('entity') + '</td>';
		strVar += ' <td width="9%" font-size="11" align="center">' + expRec.getFieldText('subsidiary') + '</td>';
		strVar += ' <td width="9%" font-size="11" align="center">' + expRec.getFieldText('custbody1stlevelapprover') + '</td>';
		strVar += ' <td width="9%" font-size="11" align="center">' + expRec.getFieldValue('status') + '</td>';
		strVar += ' <td width="9%" font-size="11" align="center">' + expRec.getFieldValue('trandate') + '</td>';
		
		strVar += '	</tr>';	
		
		strVar += '</table>';
		//Send Mail
		var strVar_ = '';
		strVar_ += '<html>';
		strVar_ += '<body>';
		
		strVar_ += '<p>Dear Team,</p>';
		//strVar_ += '<br/>';
		strVar_ += '<p>';
		//strVar_ += 'Please note that employees will get a system generated email, 4 weeks prior to the allocation end date notifying them about the project wind down. ';
		strVar_ += 'Following expense claim has been created in NetSuite. Please check and have it updated in OTG server.</p>';
		
		strVar_ += '<br/>';
		strVar_ += strVar;
	
		strVar_ += '<br/>';
		strVar_ += '<p>Thanks & Regards,</p>';
		strVar_ += '<p>Information Systems</p>';
			
		strVar_ += '</body>';
		strVar_ += '</html>';
		
		var a_emp_attachment = new Array();
		a_emp_attachment['entity'] = 83134;
		
		nlapiSendEmail(442, 'onthego@BRILLIO.COM', '[NetSuite] Expense Creation Notification', strVar_,'information.systems@brillio.com',null,a_emp_attachment);
		nlapiLogExecution('DEBUG', 'Mail Sent for exp:' , expRec.getFieldText('entity'));
		}
	}
	catch (err) {
		nlapiLogExecution('ERROR', 'sendMailsForEdit', err);
	}
	
}
function getExpenseDetails(expenseId) {

	try {
		var expenseReportRec = nlapiLoadRecord('expensereport', expenseId);

		var expenseDetails = {};

		// employee details
		var employeeDetails = nlapiLookupField('employee', expenseReportRec
		        .getFieldValue('entity'), [ 'firstname', 'lastname', 'email',
		        'subsidiary' ]);
		employeeDetails.id = expenseReportRec.getFieldText('entity');
		employeeDetails.internalid = expenseReportRec.getFieldValue('entity');

		var employeeDetailsText = nlapiLookupField('employee', expenseReportRec
		        .getFieldValue('entity'), [ 'department' ], true);

		// employeeDetails.vertical = employeeDetailsText;
		employeeDetails.department = employeeDetailsText.department;
		// employeeDetails.subsidiary = employeeDetailsText.subsidiary;

		expenseDetails.Employee = employeeDetails;
		nlapiLogExecution('debug', 'employee', JSON.stringify(employeeDetails));

		// 1st level approver details
		var firstApproverId = expenseReportRec
		        .getFieldValue(constant.ExpenseReport.FirstLevelApprover);

		if (isNotEmpty(firstApproverId)) {
			var firstApprover = nlapiLookupField('employee', firstApproverId, [
			        'firstname', 'lastname', 'email' ]);
			firstApprover.id = expenseReportRec
			        .getFieldText(constant.ExpenseReport.FirstLevelApprover);
			firstApprover.internalid = firstApproverId;

			expenseDetails.FirstLevelApprover = firstApprover;
			nlapiLogExecution('debug', 'first approver', JSON
			        .stringify(firstApprover));
		} else {
			// pull the from the employee record / reporting manager
		}

		// 2nd level approver details
		var secondLevelApprovalId = expenseReportRec
		        .getFieldValue('custbody_expenseapprover');

		if (isNotEmpty(secondLevelApprovalId)) {
			var secondApprover = nlapiLookupField('employee',
			        secondLevelApprovalId, [ 'firstname', 'lastname', 'email' ]);
			secondApprover.id = expenseReportRec
			        .getFieldText('custbody_expenseapprover');
			secondApprover.internalid = secondLevelApprovalId;

			expenseDetails.SecondLevelApprover = secondApprover;
			nlapiLogExecution('debug', 'second approver', JSON
			        .stringify(secondApprover));
		}

		expenseDetails.Expense = {
		    Reference : expenseReportRec.getFieldValue('tranid'),
		    Purpose : isEmpty(expenseReportRec.getFieldValue('memo')) ? ''
		            : expenseReportRec.getFieldValue('memo'),
		    Status : expenseReportRec.getFieldValue('status'),
		    InternalId : expenseReportRec.getId(),
		    AdvanceToApply : expenseReportRec.getFieldValue('advance'),
		    VAT : expenseReportRec.getFieldValue('tax1amt'),
		    PostingDate : expenseReportRec.getFieldValue('trandate'),
		    TransactionDate : expenseReportRec
		            .getFieldValue('custbody_transactiondate'),
		    AccountName : expenseReportRec.getFieldText('account'),
		    Account : expenseReportRec.getFieldValue('account'),
		    // Reason : '',
		    Currency : nlapiLookupField('expensereport', expenseId, 'currency',
		            true),
		    DocumentReceived : expenseReportRec
		            .getFieldValue('custbody_exp_doc_received'),
		    DocumentReceivedDate : '',
		    ItemList : []
		// ApproverRemark : expenseReportRec.getFieldValue(''),
		};

		expenseDetails.Expense.Summary = {
		    Expenses : expenseReportRec.getFieldValue('total'),
		    NonReimbursableExpenses : expenseReportRec
		            .getFieldValue('nonreimbursable'),
		    ReimbursableExpenses : expenseReportRec
		            .getFieldValue('reimbursable'),
		    AdvanceToApply : expenseReportRec.getFieldValue('advance'),
		    TotalReimbursableAmount : expenseReportRec.getFieldValue('amount')
		};

		// get all line items
		var lineItemCount = expenseReportRec.getLineItemCount('expense');
		var expenseList = [];
		var isReimbursable = null;
		var projectId = null;
		var projectIdList = [];

		for (var line = 1; line <= lineItemCount; line++) {
			isReimbursable = expenseReportRec.getLineItemValue('expense',
			        'isnonreimbursable', line);
			projectId = expenseReportRec.getLineItemValue('expense',
			        'customer', line);
			projectIdList.push(projectId);

			if (isFalse(isReimbursable)) {
				expenseList.push({
				    ProjectText : expenseReportRec.getLineItemValue('expense',
				            'custcolprj_name', line),
				    ProjectId : projectId,
				    Vertical : expenseReportRec.getLineItemValue('expense',
				            'class_display', line),
				    CategoryText : expenseReportRec.getLineItemValue('expense',
				            'category_display', line),
				    ForeignAmount : expenseReportRec.getLineItemValue(
				            'expense', 'foreignamount', line),
				    Amount : expenseReportRec.getLineItemValue('expense',
				            'amount', line),
				    Currency : expenseReportRec.getLineItemText('expense',
				            'currency', line),
				    DeliveryManager : null,
				    ClientPartner : null
				});
			}
		}

		// get all the delivery managers
		projectIdList = _.uniq(projectIdList);
		getDeliveryManager(projectIdList, expenseList, firstApprover,
		        expenseDetails);

		return expenseDetails;

	} catch (err) {
		nlapiLogExecution('ERROR', 'getExpenseDetails', err);
		throw err;
	}
}