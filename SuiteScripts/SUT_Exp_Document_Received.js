/**
 * Suitelet screen to allow the user to mark the expenses whose documents have been submitted.
 * 
 * Version    Date            Author           Remarks
 * 1.00       06 Jul 2015     Nitish Mishra
 *
 */

/**
 * @param {nlobjRequest}
 *            request Request object
 * @param {nlobjResponse}
 *            response Response object
 * @returns {Void} Any output is written via response object
 */
function suitelet(request, response) {
	try {
		var method = request.getMethod();
		nlapiLogExecution('debug', 'method', method);

		if (method == 'GET') {
			createDocumentReceivedScreen(request);
		} else if (method == 'POST') {
			submitExpenses(request);
		} else {
			throw "Invalid Method";
		}
	} catch (err) {
		nlapiLogExecution('ERROR', 'suitelet', err);
	}
}

function createDocumentReceivedScreen(request) {
	try {
		var form = nlapiCreateForm('Expense Report - Document Received');

		var sublist = form.addSubList('custpage_expense', 'list',
				'Expense Reports');

		sublist.addMarkAllButtons();

		sublist.addField('internalid', 'select', 'Expense Report Hidden',
				'expensereport').setDisplayType('hidden');
		sublist.addField('select', 'checkbox', 'Document Received');
		sublist.addField('expense', 'text', 'Expense');
		sublist.addField('status', 'text', 'Status');
		sublist.addField('employee', 'select', 'Employee', 'employee')
				.setDisplayType('inline');
		sublist.addField('first', 'select', 'First Approver', 'employee')
				.setDisplayType('inline');
		sublist.addField('second', 'select', 'Second Approver', 'employee')
				.setDisplayType('inline');
		sublist.addField('remark', 'textarea', 'Remarks').setDisplayType(
				'entry');

		sublist.setLineItemValues(getExpensePendingDocuments());
		form.addSubmitButton('Submit Expenses');

		response.writePage(form);
	} catch (err) {
		nlapiLogExecution('ERROR', 'createDocumentReceivedScreen', err);
		throw err;
	}
}

function getExpensePendingDocuments() {
	try {
		var searchResult = nlapiSearchRecord('expensereport', null, [
				new nlobjSearchFilter('status', null, 'anyof', 'ExpRept:C'),
				new nlobjSearchFilter('custbody_exp_doc_received', null, 'is',
						'F') ],
				[
						new nlobjSearchColumn('internalid', null, 'group')
								.setSort(),
						new nlobjSearchColumn('statusref', null, 'group'),
						new nlobjSearchColumn('tranid', null, 'group'),
						new nlobjSearchColumn('custbody_exp_doc_received',
								null, 'group'),
						new nlobjSearchColumn('custbody1stlevelapprover', null,
								'group'),
						new nlobjSearchColumn('custbody_expenseapprover', null,
								'group'),
						new nlobjSearchColumn('mainname', null, 'group') ]);

		var expenseList = [];

		if (searchResult) {

			searchResult.forEach(function(expense) {
				expenseList.push({
					internalid : expense.getValue('internalid', null, 'group'),
					expense : expense.getValue('tranid', null, 'group'),
					status : expense.getText('statusref', null, 'group'),
					documentrecevied : expense.getValue(
							'custbody_exp_doc_received', null, 'group'),
					employee : expense.getValue('mainname', null, 'group'),
					first : expense.getValue('custbody1stlevelapprover', null,
							'group'),
					second : expense.getValue('custbody_expenseapprover', null,
							'group')
				});
			});
		}

		return expenseList;
	} catch (err) {
		nlapiLogExecution('ERROR', 'getExpensePendingDocuments', err);
		throw err;
	}
}

function submitExpenses(request) {
	try {
		var expenseCount = request.getLineItemCount('custpage_expense');

		for (var linenum = 1; linenum <= expenseCount; linenum++) {
			var expenseId = request.getLineItemValue('custpage_expense',
					'internalid', linenum);
			var select = request.getLineItemValue('custpage_expense', 'select',
					linenum);

			if (select == 'T') {

				nlapiSubmitField('expensereport', expenseId,
						[ 'custbody_exp_doc_received',
								'custbody_exp_doc_remark' ], [
								'T',
								request.getLineItemValue('custpage_expense',
										'remark', linenum) ]);
			}
		}

		response.sendRedirect('SUITELET', 'customscript_sut_exp_doc_received',
				'customdeploy_sut_exp_doc_received');
	} catch (err) {
		nlapiLogExecution('ERROR', 'submitExpenses', err);
		throw err;
	}
}
