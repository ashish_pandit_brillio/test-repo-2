/**
 * Create JE using monthly provision master
 * 
 * Version Date Author Remarks 1.00 19 Apr 2016 Nitish Mishra
 * 
 */

function scheduled(type) {
	nlapiLogExecution('DEBUG', 'START');
	try {
		var tranDate = nlapiDateToString(new Date(), 'date');
		var context = nlapiGetContext();
		var provisionMasterId = getRunningProvisionMasterId();
		var provisionMaster = nlapiLoadRecord('customrecord_provision_master',
		        provisionMasterId);
		var arrExchangeRate = getExchangeRate(tranDate, provisionMaster
		        .getFieldText('custrecord_pm_currency'));

		nlapiSubmitField('customrecord_provision_master', provisionMasterId, [
		        'custrecord_pm_status', 'custrecord_pm_percent' ], [
		        'JE Creation', '0%' ]);

		createHourlyJournal(provisionMasterId, provisionMaster, tranDate,
		        arrExchangeRate);

		createMonthlyJournal(provisionMasterId, provisionMaster, tranDate,
		        arrExchangeRate);

		nlapiSubmitField('customrecord_provision_master', provisionMasterId, [
		        'custrecord_pm_status', 'custrecord_pm_percent' ], [
		        'JE Creation', '100%' ]);

		nlapiSubmitField('customrecord_provision_master', provisionMasterId, [
		        'custrecord_pm_je_creation_done',
		        'custrecord_pm_is_currently_active' ], [ 'T', 'F' ]);

		nlapiSendEmail('442', '8177', 'Monthly Provision - BTPL',
		        'Process completed');
	} catch (err) {
		nlapiLogExecution('ERROR', 'scheduled', err);
	}
	nlapiLogExecution('DEBUG', 'END');
}

function createHourlyJournal(provisionMasterId, provisionMasterRec, tranDate,
        arrExchangeRate)
{
	try {
		nlapiLogExecution('debug', 'Hourly Start');

		createHourlyJournalEntryB(provisionMasterId, provisionMasterRec,
		        tranDate, arrExchangeRate, 'Hourly - Approved',
		        getHourlyApprovedData(provisionMasterId, arrExchangeRate));

		nlapiSubmitField('customrecord_provision_master', provisionMasterId, [
		        'custrecord_pm_status', 'custrecord_pm_percent' ], [
		        'JE Creation', '15%' ]);

		createHourlyJournalEntryB(provisionMasterId, provisionMasterRec,
		        tranDate, arrExchangeRate, 'Hourly - Submitted, Not Approved',
		        getHourlySubmittedData(provisionMasterId, arrExchangeRate));

		nlapiSubmitField('customrecord_provision_master', provisionMasterId, [
		        'custrecord_pm_status', 'custrecord_pm_percent' ], [
		        'JE Creation', '30%' ]);

		createHourlyJournalEntryB(provisionMasterId, provisionMasterRec,
		        tranDate, arrExchangeRate, 'Hourly - Not Submitted',
		        getHourlyNotSubmittedData(provisionMasterId, arrExchangeRate));

		nlapiSubmitField('customrecord_provision_master', provisionMasterId, [
		        'custrecord_pm_status', 'custrecord_pm_percent' ], [
		        'JE Creation', '45%' ]);

		nlapiLogExecution('debug', 'Hourly End');
	} catch (err) {
		nlapiLogExecution('ERROR', 'createHourlyJournal', err);
		throw err;
	}
}

function createHourlyJournalEntryB(provisionMasterId, provisionMasterRec,
        tranDate, arrExchangeRate, memo, provisionEntrySearch)
{
	var vendorList = {};	
    var default_vendor_mappingSearch = nlapiSearchRecord("customrecord_default_vendor_mapping", null,	
        [],	
        [	
            new nlobjSearchColumn("custrecord_subsidiary_je"),	
            new nlobjSearchColumn("custrecord_default_vendor")	
        ]	
    );	
    for (var i_index = 0; i_index < default_vendor_mappingSearch.length; i_index++) {	
        var subsidiary_je = default_vendor_mappingSearch[i_index].getValue('custrecord_subsidiary_je');	
        var default_vendor_je = default_vendor_mappingSearch[i_index].getValue('custrecord_default_vendor');	
        vendorList[subsidiary_je] = default_vendor_je;	
    }
	
	
	
	try {
		nlapiLogExecution('debug', 'debit custrecord_mp_finance_head',
		        provisionMasterRec.getFieldValue('custrecord_mp_finance_head'));

		var count = provisionEntrySearch.length;
		nlapiLogExecution('debug', memo, 'Start');

		var creditAmountSum = 0;
		var itemsAdded = 0;
		var journalEntryRecord = null;

		for (var i = 0; i < count; i++) {

			if (!journalEntryRecord) {
				creditAmountSum = 0;
				journalEntryRecord = nlapiCreateRecord('journalentry');
				journalEntryRecord.setFieldValue(
				        'custbody_je_provision_master', provisionMasterId);
				journalEntryRecord.setFieldValue('memo', memo);
				journalEntryRecord.setFieldValue('currency', provisionMasterRec
				        .getFieldValue('custrecord_pm_currency'));
				journalEntryRecord.setFieldValue('subsidiary',
				        provisionMasterRec
				                .getFieldValue('custrecord_pm_subsidiary'));
				journalEntryRecord.setFieldValue('custbody_financehead',
				        provisionMasterRec
				                .getFieldValue('custrecord_mp_finance_head'));
				journalEntryRecord.setFieldValue('reversaldate',
				        provisionMasterRec
				                .getFieldValue('custrecord_pm_reversal_date'));
				journalEntryRecord.setFieldValue('trandate', tranDate);
			}

			var amount = parseFloat(provisionEntrySearch[i].amountconverted);

			if (amount <= 0) {
				nlapiLogExecution('debug', 'skipped');
			} else {
				creditAmountSum = parseFloat((creditAmountSum + provisionEntrySearch[i].amountconverted)
				        .toFixed(2));

				// added debit line
				journalEntryRecord.selectNewLineItem('line');

				journalEntryRecord.setCurrentLineItemValue('line', 'account',
				        provisionMasterRec
				                .getFieldValue('custrecord_pm_acct_credit'));

				journalEntryRecord.setCurrentLineItemValue('line', 'credit',
				        provisionEntrySearch[i].amountconverted);

				journalEntryRecord.setCurrentLineItemValue('line',
				        'custcol_je_foreign_amount',
				        provisionEntrySearch[i].amount);

				journalEntryRecord.setCurrentLineItemValue('line',
				        'custcol_je_foreign_currency',
				        provisionEntrySearch[i].currencyValue);

				journalEntryRecord.setCurrentLineItemValue('line',
				        'custcol_hour', provisionEntrySearch[i].hours);

				journalEntryRecord.setCurrentLineItemValue('line',
				        'custcol_billable_rate',
				        provisionEntrySearch[i].billrate);

				journalEntryRecord.setCurrentLineItemValue('line',
				        'custcol_itemlistinje', 2222);

				journalEntryRecord
				        .setCurrentLineItemValue('line', 'memo', memo);
						
				//Added by Sitaram 	
                if (_logValidation(vendorList[provisionMasterRec.getFieldValue('custrecord_pm_subsidiary')])) {	
                    journalEntryRecord.setCurrentLineItemValue('line', 'entity', vendorList[provisionMasterRec.getFieldValue('custrecord_pm_subsidiary')]);	
                }

				// Employee
				journalEntryRecord.setCurrentLineItemValue('line',
				        'custcol_employee_id',
				        provisionEntrySearch[i].employeeId);

				journalEntryRecord.setCurrentLineItemValue('line',
				        'custcol_employeenamecolumn',
				        provisionEntrySearch[i].employee);

				// Project
				journalEntryRecord.setCurrentLineItemValue('line',
				        'custcol_temp_project',
				        provisionEntrySearch[i].projectId);

				journalEntryRecord.setCurrentLineItemValue('line',
				        'custcolprj_name', provisionEntrySearch[i].project);

				journalEntryRecord
				        .setCurrentLineItemValue('line',
				                'custcol_project_name',
				                provisionEntrySearch[i].project);

				// Customer
				journalEntryRecord.setCurrentLineItemValue('line',
				        'custcolcustcol_temp_customer',
				        provisionEntrySearch[i].customer);

				journalEntryRecord.setCurrentLineItemValue('line',
				        'custcol_customer_list',
				        provisionEntrySearch[i].customerId);

				// Classification
				journalEntryRecord.setCurrentLineItemValue('line',
				        'department', provisionEntrySearch[i].practiceId);

				journalEntryRecord.setCurrentLineItemValue('line', 'class',
				        provisionEntrySearch[i].verticalId);

				journalEntryRecord
				        .setCurrentLineItemValue('line', 'custcol_je_region',
				                provisionEntrySearch[i].locationId);

				journalEntryRecord.commitLineItem('line');
				itemsAdded += 1;
			}

			// nlapiLogExecution('Debug', 'value i', i);
			if (itemsAdded == 249 || (i == count - 1)) {
				// adding a credit line
				journalEntryRecord.selectNewLineItem('line');
				journalEntryRecord.setCurrentLineItemValue('line', 'account',
				        provisionMasterRec
				                .getFieldValue('custrecord_pm_acct_debit'));
				journalEntryRecord.setCurrentLineItemValue('line', 'debit',
				        creditAmountSum);
				journalEntryRecord.setCurrentLineItemValue('line',
				        'custcol_itemlistinje', 2222);
				journalEntryRecord
				        .setCurrentLineItemValue('line', 'memo', memo);
						
				//Added by Sitaram 	
                if (_logValidation(vendorList[provisionMasterRec.getFieldValue('custrecord_pm_subsidiary')])) {	
                    journalEntryRecord.setCurrentLineItemValue('line', 'entity', vendorList[provisionMasterRec.getFieldValue('custrecord_pm_subsidiary')]);	
                }
				
				journalEntryRecord.commitLineItem('line');

				// Submitting the record
				var jeId = nlapiSubmitRecord(journalEntryRecord);
				nlapiLogExecution('Debug', 'JE Id', jeId);

				itemsAdded = 0;
				journalEntryRecord = null;
			}
		}

		nlapiLogExecution('debug', memo, 'Done');
	} catch (err) {
		nlapiLogExecution('ERROR', 'createHourlyJournalEntryB', err);
		throw err;
	}
}

function createMonthlyJournal(provisionMasterId, provisionMasterRec, tranDate,
        arrExchangeRate)
{
	try {
		nlapiLogExecution('debug', 'Monthly Start');

		createHourlyJournalEntryB(provisionMasterId, provisionMasterRec,
		        tranDate, arrExchangeRate, 'Monthly - Approved',
		        getMonthlyApprovedData(provisionMasterId, arrExchangeRate));

		nlapiSubmitField('customrecord_provision_master', provisionMasterId, [
		        'custrecord_pm_status', 'custrecord_pm_percent' ], [
		        'JE Creation', '60%' ]);

		createHourlyJournalEntryB(provisionMasterId, provisionMasterRec,
		        tranDate, arrExchangeRate, 'Monthly - Submitted, Not Approved',
		        getMonthlySubmittedData(provisionMasterId, arrExchangeRate));

		nlapiSubmitField('customrecord_provision_master', provisionMasterId, [
		        'custrecord_pm_status', 'custrecord_pm_percent' ], [
		        'JE Creation', '80%' ]);

		createHourlyJournalEntryB(provisionMasterId, provisionMasterRec,
		        tranDate, arrExchangeRate, 'Monthly - Not Submitted',
		        getMonthlyNotSubmittedData(provisionMasterId, arrExchangeRate));

		nlapiSubmitField('customrecord_provision_master', provisionMasterId, [
		        'custrecord_pm_status', 'custrecord_pm_percent' ], [
		        'JE Creation', '100%' ]);

		nlapiLogExecution('debug', 'Monthly End');
	} catch (err) {
		nlapiLogExecution('ERROR', 'createMonthlyJournal', err);
		throw err;
	}
}

function getExchangeRate(effectiveDate, targetCurrency) {
	try {
		return {
		    'EUR' : nlapiExchangeRate('EUR', targetCurrency, effectiveDate),
		    'GBP' : nlapiExchangeRate('GBP', targetCurrency, effectiveDate),
		    'INR' : nlapiExchangeRate('INR', targetCurrency, effectiveDate),
		    'Peso' : nlapiExchangeRate('PHP', targetCurrency, effectiveDate),
		    'SGD' : nlapiExchangeRate('SGD', targetCurrency, effectiveDate),
		    'USD' : nlapiExchangeRate('USD', targetCurrency, effectiveDate),
            'AUD' : nlapiExchangeRate('AUD', targetCurrency, effectiveDate)
		};
	} catch (err) {
		nlapiLogExecution('ERROR', 'getExchangeRate', err);
		throw err;
	}
}

function yieldScript(currentContext) {

	if (currentContext.getRemainingUsage() <= 100) {
		nlapiLogExecution('AUDIT', 'API Limit Exceeded');
		var state = nlapiYieldScript();

		if (state.status == "FAILURE") {
			nlapiLogExecution('ERROR', 'Yield Script Failed', 'Reason : '
			        + state.reason + ' / Size : ' + state.size);
			return false;
		} else if (state.status == "RESUME") {
			nlapiLogExecution('AUDIT', 'Script Resumed');
		}
	}
}

	function _logValidation(value) {	
    if (value != 'null' && value != null && value != null && value != '' && value != undefined && value != undefined && value != 'undefined' && value != 'undefined' && value != 'NaN' && value != NaN) {	
        return true;	
    } else {	
        return false;	
    }	
}