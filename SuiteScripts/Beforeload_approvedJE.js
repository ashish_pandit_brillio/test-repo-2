/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       23 May 2017     bidhi.raj
 *
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType
 *   
 * @param {String} type Operation types: create, edit, view, copy, print, email
 * @param {nlobjForm} form Current form
 * @param {nlobjRequest} request Request object
 * @returns {Void}
 */
function userEventBeforeLoad(type, form){
 if(type=='view'){
   nlapiLogExecution('error', 'entering view',null);
	 var objUser = nlapiGetContext();
	 var i_role_id = objUser.getRole();
	 var userID = nlapiGetUser();
	 userID = parseFloat(userID);
   nlapiLogExecution('error', 'i_role_id',i_role_id);
	 var financeMngr = nlapiGetFieldValues('custbody_financehead');
	 var status = nlapiGetFieldValue('status');
   var created_user = nlapiGetFieldValue('custbody_created_by_user');
   
   var filter=[];
   filter[0]=new nlobjSearchFilter('custrecord30',null,'anyof',nlapiGetUser());//custrecord30
   filter[1]=new nlobjSearchFilter('custrecord31',null,'is','3');//custrecord31
	var search=nlapiSearchRecord('customrecord437',null,filter,null);//customrecord437
	
   
   form.removeButton('approve');
   var dataRow = [];
   try{
   for(var j=0;j<financeMngr.length;++j){
	   dataRow.push(parseFloat(financeMngr[j]));
   }
   }catch(err){
     nlapiLogExecution('debug', 'finance mnger null',null);
   }
   var value = dataRow.indexOf(userID);
   
   if(value >= 0){
	   
   }
   else{
	   if(userID == parseFloat(created_user) && status == 'Pending Approval'){
		      
	    }
	   else{
         	if(parseFloat(i_role_id) != parseFloat(3)){
         		if(!_logValidation(search)){
         		form.removeButton('edit');
                  nlapiLogExecution('error', 'removed edit button for role_id-->',i_role_id);
         		}
         	}
         	else{
         		if(!_logValidation(search)){
                  if(parseFloat(i_role_id) != parseFloat(3)){
         			form.removeButton('edit');
              nlapiLogExecution('error', 'removed edit button at line 63',null);
                  }
                }
         	}
              
	   }
	   
   }
  //var fnm= financeMngr.split('');
  /*  for(var j=0;j<fnm.length;++j){
      if(fnm[j]==userID){
        nlapiLogExecution('debug', 'fnm',fnm);
      nlapiLogExecution('debug', 'userID',userID);
         if(status == 'Pending Approval'){
       if(parseInt(userID) != parseInt(fnm[j])){
          form.removeButton('edit');
		 nlapiLogExecution('debug', 'status',status);
       }
		
	 }
      }
  } */
	
   if(status == 'Approved for Posting'){
     if(parseFloat(i_role_id) != parseFloat(3)){
       form.removeButton('edit');
       nlapiLogExecution('error', 'removed edit button for role_id when approved-->',i_role_id);
     
     }
     
   }
   
	 /*if(i_role_id != '3' ||  parseInt(userID) != parseInt(financeMngr)){
		form.removeButton('edit');
		nlapiLogExecution('debug', 'iroleid',i_role_id);
  		
	 }*/
 }
}

function afterSubmit(type){
try{
var loadRecord = nlapiLoadRecord(nlapiGetRecordType(),nlapiGetRecordId());
var finance_mangr = loadRecord.getFieldValues('custbody_financehead');
var i_subsidiary = loadRecord.getFieldValue('subsidiary');
var values = [];
if(parseInt(i_subsidiary) == parseInt(3)){ //BTPL
values[0] = 69145; //Vikash
values[1] = 1615;  //Raghav
values[2] = 5764;  //Govind
}
else{
values[0] = 69145;
values[1] = 5764;
}
if(parseInt(finance_mangr.length) < parseInt(2)){
loadRecord.setFieldValues('custbody_financehead',values);
var id = nlapiSubmitRecord(loadRecord);
nlapiLogExecution('DEBUG','After Submit id',id);
}

}
catch(e){
nlapiLogExecution('DEBUG','After Submit Error',e);
}
}

function _logValidation(value) 
{
 if(value != null && value.toString() != null && value != '' && value != undefined && value.toString() != undefined && value != 'undefined' && value.toString() != 'undefined'&& value.toString() != 'NaN' && value != NaN) 
 {
  return true;
 }
 else 
 { 
  return false;
 }
}
