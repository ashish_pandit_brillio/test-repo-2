// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
/*
   	Script Name : REST_AssignTraining.js
	Author      : ASHISH PANDIT
	Date        : 02 APRIL 2019
    Description : RESTlet to show assign training data 


	Script Modification Log:

	-- Date --			-- Modified By --				--Requested By--				-- Description --
 


Below is a summary of the process controls enforced by this script file.  The control logic is described
more fully, below, in the appropriate function headers and code blocks.


     SCHEDULED FUNCTION
		- scheduledFunction(type)


     SUB-FUNCTIONS
		- The following sub-functions are called by the above core functions in order to maintain code
            modularization:

               - NOT USED

*/
}
//============================================================================================================


/*RESTlet Main function*/
function REST_assignTraining(dataIn)
{
	try
	{
		var current_date = nlapiDateToString(new Date());
		//Log for current date
		nlapiLogExecution('DEBUG', 'Current Date', 'current_date...' + current_date)
		nlapiLogExecution('debug', 'dataIn', JSON.stringify(dataIn));
		var requestType = dataIn.type;
		if(requestType == 'get')
		{
			var user = dataIn.user;
			var values = {};
			nlapiLogExecution('Debug','Script Parameter user ',user);
			
			/*Search to get all Training List*/
			var customrecord_fuel_training_catalogSearch = nlapiSearchRecord("customrecord_fuel_training_catalog",null,
			[
			], 
			[
			   new nlobjSearchColumn("internalid"), 
			   new nlobjSearchColumn("name")
			]
			);
			if(customrecord_fuel_training_catalogSearch)
			{
				values.trainingList = getTrainingList(customrecord_fuel_training_catalogSearch);
			}
			
			/*Search to get Users Assigned Training List*/
			values.AssignedTrainingList = getAssignedTrainingList(user);
			return values;
		}
		if(requestType == 'save')
		{
			nlapiLogExecution('Debug','dataIn.employee ',dataIn.employee);
			var i_employee = dataIn.employee;
			var i_trainingSkill = dataIn.skill;
			var i_assignby = dataIn.assignby;
			i_assignby = getUserUsingEmailId(i_assignby);
			
			for(var i=0; i<i_trainingSkill.length;i++)
			{
				var trainingRecord = nlapiCreateRecord('customrecord_fuel_training');
				trainingRecord.setFieldValue('custrecord_training_skill',i_trainingSkill[i]);
				trainingRecord.setFieldValue('custrecord_training_assigned_by',i_assignby);
				trainingRecord.setFieldValue('custrecord_training_employee',i_employee);
				var recId = nlapiSubmitRecord(trainingRecord);
				nlapiLogExecution('Debug','dataIn.recId ',recId);
				if(recId)
				{
					nlapiSubmitField('customrecord_fuel_training',recId,'custrecord_training_id',getTRNumber(recId));
				}
			}
			return "success";
		}
		if(requestType == 'completed')
		{
			var trainingId = dataIn.completed;
			//nlapiLogExecution('debug','Completed dataIn ',JSON.Strinify(dataIn));
			for(var i=0;i<trainingId.length;i++)
			{
				var customrecord_fuel_trainingSearch = nlapiSearchRecord("customrecord_fuel_training",null,
				[
				   ["custrecord_training_id","is",trainingId[i]]
				], 
				[
				   new nlobjSearchColumn("scriptid").setSort(false), 
				   new nlobjSearchColumn("custrecord_training_id"), 
				   new nlobjSearchColumn("custrecord_training_skill"), 
				   new nlobjSearchColumn("custrecord_training_assigned_date"), 
				   new nlobjSearchColumn("custrecord_training_assigned_by"), 
				   new nlobjSearchColumn("custrecord_training_completed"), 
				   new nlobjSearchColumn("custrecord_training_end_date"), 
				   new nlobjSearchColumn("custrecord_training_employee")
				]
				);
				if(customrecord_fuel_trainingSearch)
				{
					for(var j=0;j<customrecord_fuel_trainingSearch.length;j++)
					{
						var recId = customrecord_fuel_trainingSearch[j].getId();
						var trainingCompleted = nlapiSubmitField('customrecord_fuel_training',recId,'custrecord_training_completed','T');
						nlapiLogExecution('Debug','trainingCompleted ',trainingCompleted);
					}
				}
			}
			
			
		}
		
	}
	catch(error)
	{
		nlapiLogExecution('Debug','Error ',error);
		return {
			"code": "error",
			"message": error
		}
	}
}

/****Function to generate training id****/
function getTRNumber(recId) {
	var str = "" + recId;
	var pad = "00000000";
	var s_number = pad.substring(0, pad.length - str.length) + str;
	return "TR"+s_number;
}
/****Function to get Training List ****/
function getTrainingList(customrecord_fuel_training_catalogSearch)
{
	try
	{
		if(customrecord_fuel_training_catalogSearch)
		{
			var dataArray = new Array();
			nlapiLogExecution('Debug','customrecord_fuel_training_catalogSearch ',customrecord_fuel_training_catalogSearch.length);
			for(var i=0; i<customrecord_fuel_training_catalogSearch.length;i++)
			{
				var internalId = customrecord_fuel_training_catalogSearch[i].getValue("internalid");
				var skill = customrecord_fuel_training_catalogSearch[i].getValue("name");
				var data = {};
				data.skill = {"name":customrecord_fuel_training_catalogSearch[i].getValue("name"), "id":customrecord_fuel_training_catalogSearch[i].getId()};
				dataArray.push(data);
			}
			nlapiLogExecution('Debug','dataArray$$$$$ ',JSON.stringify(dataArray));
			return dataArray;
		}
	}
	catch(error)
	{
		nlapiLogExecution('Error','Exception ',error);
		return {
			"code": "error",
			"message": error
		}
	}
}


/****Function to get Assigned Training List ****/
function getAssignedTrainingList(user)
{
	try
	{
		if(user)
		{
			var customrecord_fuel_trainingSearch = nlapiSearchRecord("customrecord_fuel_training",null,
			[
			   ["custrecord_training_employee","anyof",user]
			], 
			[
			   new nlobjSearchColumn("custrecord_training_id"), 
			   new nlobjSearchColumn("custrecord_training_skill"), 
			   new nlobjSearchColumn("custrecord_training_assigned_date"), 
			   new nlobjSearchColumn("custrecord_training_assigned_by"), 
			   new nlobjSearchColumn("custrecord_training_completed"), 
			   new nlobjSearchColumn("custrecord_training_end_date"), 
			   new nlobjSearchColumn("custrecord_training_employee")
			]
			);
			if(customrecord_fuel_trainingSearch)
			{
				var dataArray = new Array();
				nlapiLogExecution('Debug','customrecord_fuel_trainingSearch ',customrecord_fuel_trainingSearch.length);
				for(var i=0; i<customrecord_fuel_trainingSearch.length;i++)
				{
					var internalId = customrecord_fuel_trainingSearch[i].getValue("internalid");
					var skill = customrecord_fuel_trainingSearch[i].getValue("custrecord_fuel_catalog_name");
					var data = {};
					data.trainingid = {"name":customrecord_fuel_trainingSearch[i].getValue("custrecord_training_id")};
					data.skill = {"name":customrecord_fuel_trainingSearch[i].getText("custrecord_training_skill"),"id":customrecord_fuel_trainingSearch[i].getValue("custrecord_training_skill")};
					data.assigndate = {"name":customrecord_fuel_trainingSearch[i].getValue("custrecord_training_assigned_date")};
					data.assignby = {"name":customrecord_fuel_trainingSearch[i].getText("custrecord_training_assigned_by"),"id":customrecord_fuel_trainingSearch[i].getValue("custrecord_training_assigned_by")};
					data.completed = {"name":customrecord_fuel_trainingSearch[i].getValue("custrecord_training_completed")};
					data.endDate = {"name":customrecord_fuel_trainingSearch[i].getValue("custrecord_training_end_date")};
					data.employee = {"name":customrecord_fuel_trainingSearch[i].getValue("custrecord_training_employee")};
					dataArray.push(data);
				}
				nlapiLogExecution('Debug','dataArray$$$$$ ',JSON.stringify(dataArray));
				return dataArray;
			}
		}
	}
	catch(error)
	{
		nlapiLogExecution('Error','Exception ',error);
		return {
			"code": "error",
			"message": error
		}
	}
}

