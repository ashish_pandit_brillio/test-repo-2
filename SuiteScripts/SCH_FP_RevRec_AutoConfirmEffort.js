/**
 * @author Jayesh
 */

function schedule_autoConfirm()
{
	try
	{
		var o_context = nlapiGetContext();
		var i_project_id = o_context.getSetting('SCRIPT', 'custscript_project_selected');
		
		if(i_project_id)
		{
			var a_revenue_cap_filter = [['custrecord_revenue_share_project', 'anyof', parseInt(i_project_id)]];
			search_revenue_share(a_revenue_cap_filter,i_project_id);
		}
		else
		{
			var a_project_filter = [['status', 'noneof', 1], 'and',
								['jobtype', 'anyof', 2], 'and',
								['jobbillingtype', 'anyof', 'FBM'], 'and',
								['custentity_fp_rev_rec_type', 'anyof', 1]];
		
			var a_columns_proj_srch = new Array();
			a_columns_proj_srch[0] = new nlobjSearchColumn('custentity_projectvalue');
			a_columns_proj_srch[1] = new nlobjSearchColumn('customer');
			a_columns_proj_srch[2] = new nlobjSearchColumn('custentity_fp_rev_rec_type');
				
			var a_project_search_results = searchRecord('job', null, a_project_filter, a_columns_proj_srch);
			
			if (a_project_search_results)
			{
				for (var i_pro_index = 0; i_pro_index < a_project_search_results.length; i_pro_index++)
				{
					if (o_context.getRemainingUsage() <= 2000)
					{
						nlapiYieldScript();
					}
					
					var i_project_id = a_project_search_results[i_pro_index].getId();
					
					var a_revenue_cap_filter = [['custrecord_revenue_share_project', 'anyof', parseInt(i_project_id)]];
					
					search_revenue_share(a_revenue_cap_filter, i_project_id);
				}
			}
		}
	}
	catch(err)
	{
		nlapiLogExecution('audit','schedule_autoConfirm','ERROR MESSGAE:- '+err);
	}
}

function search_revenue_share(a_revenue_cap_filter,i_project_id)
{
	var a_column = new Array();
	a_column[0] = new nlobjSearchColumn('created').setSort(true);
	a_column[1] = new nlobjSearchColumn('custrecord_auto_number_counter');
	a_column[2] = new nlobjSearchColumn('custrecord_revenue_share_approval_status');
	
	var a_get_logged_in_user_exsiting_revenue_cap = nlapiSearchRecord('customrecord_revenue_share', null, a_revenue_cap_filter, a_column);
	if (a_get_logged_in_user_exsiting_revenue_cap)
	{
		var i_revenue_share_id = a_get_logged_in_user_exsiting_revenue_cap[0].getId();
		var i_revenue_share_status = a_get_logged_in_user_exsiting_revenue_cap[0].getValue('custrecord_revenue_share_approval_status');
		
		var a_effort_activity_mnth_end_filter = [['custrecord_revenue_share_parent_json', 'anyof', parseInt(i_revenue_share_id)]];
		
		var a_columns_mnth_end_effort_activity_srch = new Array();
		a_columns_mnth_end_effort_activity_srch[0] = new nlobjSearchColumn('created').setSort(true);
		a_columns_mnth_end_effort_activity_srch[1] = new nlobjSearchColumn('custrecord_mnth_end_effrt_created_json');
		a_columns_mnth_end_effort_activity_srch[2] = new nlobjSearchColumn('custrecord_is_mnth_effrt_confirmed');
		a_columns_mnth_end_effort_activity_srch[3] = new nlobjSearchColumn('custrecord_is_mnth_effrt_auto_confirmed');
		
		var a_get_mnth_end_effrt_activity = nlapiSearchRecord('customrecord_fp_rev_rec_mnth_end_json', null, a_effort_activity_mnth_end_filter, a_columns_mnth_end_effort_activity_srch);
		if (a_get_mnth_end_effrt_activity)
		{
			var i_month_end_activity_id = a_get_mnth_end_effrt_activity[0].getId();
			nlapiLogExecution('audit','mnth end activity id:- '+i_month_end_activity_id);
			
			if(a_get_mnth_end_effrt_activity[0].getValue('custrecord_mnth_end_effrt_created_json') != 'T')
			{
				var s_effrt_json = nlapiLookupField('customrecord_fp_rev_rec_mnth_end_json',i_month_end_activity_id,'custrecord_json_mnth_end_effrt');
				var s_effrt_json_2 = nlapiLookupField('customrecord_fp_rev_rec_mnth_end_json',i_month_end_activity_id,'custrecord_json_mnth_end_effrt2');
				var s_effrt_json_3 = nlapiLookupField('customrecord_fp_rev_rec_mnth_end_json',i_month_end_activity_id,'custrecord_json_mnth_end_effrt3');
				var s_effrt_json_4 = nlapiLookupField('customrecord_fp_rev_rec_mnth_end_json',i_month_end_activity_id,'custrecord_json_mnth_end_effrt4');
				
				var o_mnth_end_parent = nlapiCreateRecord('customrecord_fp_revrec_month_end_process');
				o_mnth_end_parent.setFieldValue('custrecord_revenue_share_parent', i_revenue_share_id);
				var i_mnth_end_parent_rcrd_id = nlapiSubmitRecord(o_mnth_end_parent, true, true);
				nlapiLogExecution('audit','mnth end rcrd id:- ',i_mnth_end_parent_rcrd_id);
				
				if (s_effrt_json)
				{
					create_entry(i_mnth_end_parent_rcrd_id,s_effrt_json,i_project_id);
				}
				
				if (s_effrt_json_2)
				{
					create_entry(i_mnth_end_parent_rcrd_id,s_effrt_json_2,i_project_id);
				}
				
				if(s_effrt_json_3)
				{
					create_entry(i_mnth_end_parent_rcrd_id,s_effrt_json_3,i_project_id);
				}
				
				if(s_effrt_json_4)
				{
					create_entry(i_mnth_end_parent_rcrd_id,s_effrt_json_4,i_project_id);
				}
				
				//nlapiSubmitField('customrecord_fp_rev_rec_mnth_end_json',i_month_end_activity_id,'custrecord_mnth_end_effrt_created_json','T');
				var fprevrec_monthendjson=nlapiLoadRecord('customrecord_fp_rev_rec_mnth_end_json',i_month_end_activity_id);
             		fprevrec_monthendjson.setFieldValue('custrecord_mnth_end_effrt_created_json','T');
					nlapiSubmitRecord(fprevrec_monthendjson, {disabletriggers : true, enablesourcing : true});
			}
		}
	}
}

function create_entry(i_mnth_end_parent_rcrd_id,s_effrt_json,i_project_id)
{
	try
	{
		var s_entire_json_clubed = JSON.parse(s_effrt_json);
		for (var i_mnth_end_plan = 0; i_mnth_end_plan < s_entire_json_clubed.length; i_mnth_end_plan++)
		{
			var a_row_json_data = JSON.parse(JSON.stringify(s_entire_json_clubed[i_mnth_end_plan]));
			//nlapiLogExecution('audit', 'json len:- ' + a_row_json_data.length);
			for (var i_row_json_index = 0; i_row_json_index < a_row_json_data.length; i_row_json_index++)
			{
				var i_practice = a_row_json_data[i_row_json_index].prac;
				var i_sub_practice = a_row_json_data[i_row_json_index].subprac;
				var i_role = a_row_json_data[i_row_json_index].role;
				var i_level = a_row_json_data[i_row_json_index].level;
				var i_location = a_row_json_data[i_row_json_index].loc;
				var f_revenue = a_row_json_data[i_row_json_index].cost;
				if (!f_revenue) 
					f_revenue = 0;
				
				var f_revenue_share = a_row_json_data[i_row_json_index].share;
				if (!f_revenue_share) 
					f_revenue_share = 0;
				
				var i_no_of_resources = a_row_json_data[i_row_json_index].allo;
				var s_mnth = a_row_json_data[i_row_json_index].mnth;
				var s_year = a_row_json_data[i_row_json_index].year;
				
				var o_actual_effrt_mnth_end = nlapiCreateRecord('customrecord_fp_revrec_month_end_effort');
				 o_actual_effrt_mnth_end.setFieldValue('custrecord_mnth_end_practice', i_practice);
				 o_actual_effrt_mnth_end.setFieldValue('custrecord_month_end_sub_practice', i_sub_practice);
				 o_actual_effrt_mnth_end.setFieldValue('custrecord_mnth_end_role', i_role);
				 o_actual_effrt_mnth_end.setFieldValue('custrecord_mnth_end_level', i_level);
				 o_actual_effrt_mnth_end.setFieldValue('custrecord_location_mnth_end', i_location);
				 o_actual_effrt_mnth_end.setFieldValue('custrecord_month_end_month_name', s_mnth);
				 o_actual_effrt_mnth_end.setFieldValue('custrecord_month_end_year_name', s_year);
				 //o_actual_effrt_mnth_end.setFieldValue('custrecord_month_end_month_start', months.Start);
				 //o_actual_effrt_mnth_end.setFieldValue('custrecord_month_end_end_date', months.End);
				 o_actual_effrt_mnth_end.setFieldValue('custrecord_percent_allocated', i_no_of_resources);
				 o_actual_effrt_mnth_end.setFieldValue('custrecord_resource_cost', f_revenue);
				 o_actual_effrt_mnth_end.setFieldValue('custrecord_mnth_end_project_plan', i_project_id);
				 o_actual_effrt_mnth_end.setFieldValue('custrecord_month_end_parent', i_mnth_end_parent_rcrd_id);
				 var i_month_end_activity = nlapiSubmitRecord(o_actual_effrt_mnth_end);
			
			}
		}
	}
	catch(err)
	{
		nlapiLogExecution('ERROR','create_entry','ERROR:- '+err);
	}
}

function searchRecord(recordType, savedSearch, arrFilters, arrColumns,
        filterExpression)
{

	try {
		var search = null;

		// if a saved search is provided, load it and add the filters and
		// columns
		if (isNotEmpty(savedSearch)) {
			search = nlapiLoadSearch(recordType, savedSearch);

			if (isArrayNotEmpty(arrFilters)) {
				search.addFilters(arrFilters);
			}

			if (isArrayNotEmpty(arrColumns)) {
				search.addColumns(arrColumns);
			}

			if (isArrayNotEmpty(filterExpression)) {
				search.setFilterExpression(filterExpression);
			}
		}
		// create a new search
		else {
			search = nlapiCreateSearch(recordType, arrFilters, arrColumns);
		}

		// run search
		var resultSet = search.runSearch();

		// iterate through the search and get all data 1000 at a time
		var searchResultCount = 0;
		var resultSlice = null;
		var searchResult = [];

		do {
			resultSlice = resultSet.getResults(searchResultCount,
			        searchResultCount + 1000);

			if (resultSlice) {

				resultSlice.forEach(function(result) {

					searchResult.push(result);
					searchResultCount++;
				});
			}
		} while (isArrayNotEmpty(resultSlice) && resultSlice.length >= 1000);

		return searchResult;
	} catch (err) {
		nlapiLogExecution('ERROR', 'searchRecord', err);
		throw err;
	}
}

function isEmpty(value) {

	return value == null || value == "" || typeof (value) == undefined;
}

function isNotEmpty(value) {

	return !isEmpty(value);
}

function isArrayEmpty(argArray) {

	return !isArrayNotEmpty(argArray);
}

function isArrayNotEmpty(argArray) {

	return (isNotEmpty(argArray) && argArray.length > 0);
}