							
// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
/*
   	Script Name : UES_setAllocatedResourceFRF.js
	Author      : Ashish Pandit
	Date        : 03 June 2019
    Description : User Event to set allocated resource on FRF Details page   


	Script Modification Log:

	-- Date --			-- Modified By --				--Requested By--				-- Description --


Below is a summary of the process controls enforced by this script file.  The control logic is described
more fully, below, in the appropriate function headers and code blocks.


     BEFORE LOAD
		- beforeLoadRecord(type)



     BEFORE SUBMIT
		- beforeSubmitRecord(type)


     AFTER SUBMIT
		- afterSubmitRecord(type)



     SUB-FUNCTIONS
		- The following sub-functions are called by the above core functions in order to maintain code
            modularization:

               - NOT USED

*/
}
// END SCRIPT DESCRIPTION BLOCK  ====================================



// BEGIN GLOBAL VARIABLE BLOCK  =====================================
{
	//  Initialize any Global Variables, in particular, debugging variables...

}
// END GLOBAL VARIABLE BLOCK  =======================================



// BEGIN BEFORE LOAD ==================================================

function updateAllocationResource(type)
{
	try
	{
			if (type == 'create') 
		{
			var resourceObj = nlapiLoadRecord('resourceallocation', nlapiGetRecordId());
			var i_project = resourceObj.getFieldValue('project');
			var i_resource = resourceObj.getFieldValue('allocationresource');
			var d_startDate = resourceObj.getFieldValue('startdate');
			
			var customrecord_frf_detailsSearch = nlapiSearchRecord("customrecord_frf_details",null,
			[
			   ["custrecord_frf_details_selected_emp","anyof",i_resource], 
			   "AND", 
			   ["custrecord_frf_details_open_close_status","anyof","3"], 
			   "AND", 
			   ["custrecord_frf_details_status_flag","anyof","3"], 
			   "AND", 
			   ["isinactive","is","F"]
			], 
			[
			   new nlobjSearchColumn("custrecord_frf_details_opp_id"), 
			   new nlobjSearchColumn("custrecord_frf_details_project")
			]
			);
			if(customrecord_frf_detailsSearch)
			{
				nlapiLogExecution('debug','customrecord_frf_detailsSearch Length ',customrecord_frf_detailsSearch.length);
				for(var i=0; i<customrecord_frf_detailsSearch.length;i++)
				{
					var i_projectFRF = customrecord_frf_detailsSearch[i].getValue('custrecord_frf_details_project');
					var i_oppFRF = custrecord_frf_details_project[i].getValue('custrecord_frf_details_opp_id');
					if(i_project == i_projectFRF)
					{
						var obj_FrfDetails = nlapiLoadRecord('customrecord_frf_details',customrecord_frf_detailsSearch[i].getId());
						obj_FrfDetails.setFieldValue('custrecord_frf_details_allocated_emp',i_resource);
						obj_FrfDetails.setFieldValue('custrecord_frf_details_allocated_date',d_startDate);
						obj_FrfDetails.setFieldValue('custrecord_frf_details_status_flag',2); // Status Closed
						obj_FrfDetails.setFieldValue('custrecord_frf_details_open_close_status',2); // Status Closed
						var i_recId = nlapiSubmitRecord(obj_FrfDetails);
						nlapiLogExecution('Debug','i_recId ',i_recId);
					}
					else if(i_oppFRF)
					{
						var s_oppNo = nlapiLookupField('customrecord_sfdc_opportunity_record',i_oppFRF,'custrecord_opportunity_id_sfdc');
						if(s_oppNo)
						{
							var salesorderSearch = nlapiSearchRecord("salesorder",null,
							[
							   ["type","anyof","SalesOrd"], 
							   "AND", 
							   ["custbody_opp_id_sfdc","is",s_oppNo], 
							   "AND", 
							   ["mainline","is","T"]
							], 
							[
							   new nlobjSearchColumn("internalid","jobmain") 
							]
							);
							if(salesorderSearch)
							{
								var i_oppProject = salesorderSearch[0].getValue("internalid","jobmain");
								if(i_project == i_oppProject)
								{
									var obj_FrfDetails = nlapiLoadRecord('customrecord_frf_details',customrecord_frf_detailsSearch[i].getId());
									obj_FrfDetails.setFieldValue('custrecord_frf_details_allocated_emp',i_resource);
									obj_FrfDetails.setFieldValue('custrecord_frf_details_allocated_date',d_startDate);
									obj_FrfDetails.setFieldValue('custrecord_frf_details_status_flag',2); // Status Closed
									obj_FrfDetails.setFieldValue('custrecord_frf_details_open_close_status',2); // Status Closed
									var i_recId = nlapiSubmitRecord(obj_FrfDetails);
									nlapiLogExecution('Debug','i_recId ',i_recId);
								}
							}
						}
						
						
					}
				}
			}
		}
	}
	catch(error)
	{
		nlapiLogExecution('debug','Error ',error);
	}

}
