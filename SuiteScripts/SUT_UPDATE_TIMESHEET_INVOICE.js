/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       19 Sep 2019     Ravishanker		This script is use to apply timesheet of the selected date range 
 *
 */

/**
 * @param {nlobjRequest} request Request object
 * @param {nlobjResponse} response Response object
 * @returns {Void} Any output is written via response object
 */

function updateInvoicewithTimesheet(request, response){
	
	nlapiLogExecution('DEBUG', 'SALES INVOICE','Execution Start Here');
	
	if ( request.getMethod() == 'GET' )
	   {
	        var form = nlapiCreateForm('Apply Timesheet on Invoice');
	        var s_TimesheetFile = form.addField('custpage_timesheetfile','file', 'Select File');
	        s_TimesheetFile.setMandatory(true);
	        s_TimesheetFile.setLayoutType('normal','startcol')
	        var s_InvoiceId = form.addField('custpage_invoiceid','select', 'Invoice #','invoice');
	        s_InvoiceId.setLayoutType('normal','startcol')
	        s_InvoiceId.setMandatory(true);	        
	        s_InvoiceId.setDefaultValue('1499549');
	        
	        /*var dt_BillFrom = form.addField('custpage_billingfromdate','date', 'Billing From');
	        dt_BillFrom.setMandatory(true);
	        dt_BillFrom.setDisplaySize( 60, 10 );
	        var dt_BillTo = form.addField('custpage_billingtodate','date', 'Billing To');
	        dt_BillTo.setMandatory(true);
	        dt_BillTo.setDisplaySize( 60, 10 );*/
	        var i_taxCode = form.addField('custpage_taxcode','select', 'Tax Code');
	        i_taxCode.setMandatory(true);
	      
	        var taxGroupFilters = new Array();
	        taxGroupFilters[0] = new nlobjSearchFilter( 'isinactive', null, 'is', 'F' );
	        taxGroupFilters[1] = new nlobjSearchFilter( 'country', null, 'anyof', 'IN' );
	       
	        var taxGroupColumns = new Array();
	        taxGroupColumns[0] = new nlobjSearchColumn('internalid');
	        taxGroupColumns[1] = new nlobjSearchColumn('itemid');
	        
			var taxGroupList = nlapiSearchRecord('taxgroup', null, taxGroupFilters, taxGroupColumns); 

			for (var i = 0; i < taxGroupList.length; i++) {
				i_taxCode.addSelectOption(taxGroupList[i].getId(), taxGroupList[i].getValue('itemid'));
			}
			i_taxCode.setDefaultValue('2601');
				        
	        form.addSubmitButton("Submit");
	        form.addResetButton();	        
	        response.writePage(form);
	   }
	   else
	   {
	            var currentuser = nlapiGetUser();
	            var i_Rec_Id 	= request.getParameter('custpage_invoiceid');
	            var dt_billFrom = request.getParameter('custpage_billingfromdate');
	            var dt_billTo 	= request.getParameter('custpage_billingtodate');
	            var i_taxcode	= request.getParameter('custpage_taxcode');
	            var obj_csvfile	= request.getFile('custpage_timesheetfile');
	            
	            obj_csvfile.setFolder(1381832);
	            var fileID	=	nlapiSubmitFile(obj_csvfile);
	            
	            if(i_Rec_Id != null && i_Rec_Id != '')
				{
					var params = {				
			            'custscript_invoiceid' : i_Rec_Id,
			            'custscript_billingfromdate' : dt_billFrom,
			            'custscript_billingtodate' : dt_billTo,
			            'custscript_taxcode' : i_taxcode,
			            'custscript_fileid' : fileID
			        };
				
					var status = nlapiScheduleScript('customscript_sch_update_invoice_timeshet', null, params);
					nlapiLogExecution('DEBUG', 'SALES INVOICE','script Scheduled status:'+status);
				}	
	            
	            var redirectStatus	=	getNetSuiteDomainURL();
	            nlapiSetRedirectURL('EXTERNAL',redirectStatus+'/app/common/scripting/scriptstatus.nl?daterange=TODAY', null, null, null);
	   }
	
	nlapiLogExecution('DEBUG', 'SALES INVOICE','Execution End Here');
}

function getNetSuiteDomainURL()
{
  var obj_ConfigRecord		=	nlapiLoadConfiguration('companyinformation');
  return s_NetSuiteBaseURL	= 	obj_ConfigRecord.getFieldValue('appurl');    
  
}
