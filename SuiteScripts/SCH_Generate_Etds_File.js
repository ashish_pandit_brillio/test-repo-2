// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
/*
   	Script Name:SCH Generate Etds return File
	Author:Nikhil jain
	Company:Aashna cloudtech Pvt. Ltd
	Date:


	Script Modification Log:

	-- Date --			-- Modified By --				--Requested By--				-- Description --




Below is a summary of the process controls enforced by this script file.  The control logic is described
more fully, below, in the appropriate function headers and code blocks.


     SCHEDULED FUNCTION
		- scheduledFunction(type)


     SUB-FUNCTIONS
		- The following sub-functions are called by the above core functions in order to maintain code
            modularization:

               - NOT USED

*/
}
// END SCRIPT DESCRIPTION BLOCK  ====================================


// BEGIN SCHEDULED FUNCTION =============================================

function schedulerFunction_generate_tds(type)
{
    /*  On scheduled function:

          - PURPOSE
		-

	FIELDS USED:

          --Field Name--				--ID--


    */


	//  LOCAL VARIABLES



	//  SCHEDULED FUNCTION CODE BODY
	try {
		var context = nlapiGetContext();
		var usageRemaining = context.getRemainingUsage();
		nlapiLogExecution('DEBUG', 'usage', 'usageRemaining obj->' + usageRemaining)
		
		var i_recordid = context.getSetting('SCRIPT', 'custscript_recordid')
		
		if (i_recordid != null && i_recordid != '' && i_recordid != undefined) 
		{
			var o_quarterly_recordobj = nlapiLoadRecord('customrecord_quarterly_return_details', i_recordid)
			
			if (o_quarterly_recordobj != null && o_quarterly_recordobj != '' && o_quarterly_recordobj != undefined) 
			{
				var quarter = o_quarterly_recordobj.getFieldValue('custrecord_quater');
				
				var quarter_text = o_quarterly_recordobj.getFieldText('custrecord_quater');
				
				var financial_id = o_quarterly_recordobj.getFieldValue('custrecord_financial_year');
				
				var subsidiary = o_quarterly_recordobj.getFieldValue('custrecord_quarterlyreturn_subsidiary');
				
				var email = o_quarterly_recordobj.getFieldValue('custrecord_email_id');
				
				//-----------------------------------Begin Function to calculate quarter start date and quarter end date----------------------------------//
				
				var financial_year = nlapiLookupField('customrecord_financial_year', financial_id, 'custrecord_fy_code')
				
				var quarter_start_date = nlapiLookupField('customrecord_financial_year', financial_id, 'custrecord_fy_start_date')
				
				var quarter_date = get_quarter_date(quarter_start_date, quarter)
				
				var quarter_date_split = quarter_date.split('#');
				
				var quarter_startdate = quarter_date_split[0];
				//nlapiLogExecution('DEBUG', 'ETDS REtun File', 'quarter_startdate' + quarter_startdate)
				
				var quarter_enddate = quarter_date_split[1];
				//nlapiLogExecution('DEBUG', 'ETDS REtun File', 'quarter_enddate' + quarter_enddate)
				
				//-----------------------------------End Function to calculate quarter start date and quarter end date------------------------------------//
				
				var file_header = create_file_header(subsidiary, quarter, quarter_startdate, quarter_enddate, o_quarterly_recordobj)
				
				var challan_header = create_challan_header(subsidiary, quarter, quarter_startdate, quarter_enddate, o_quarterly_recordobj)
				
				var header = file_header + '\r\n' + challan_header;
				
				//--------------------------------Function to search Folder ID---------------------------------------//
				
				var folder_id = get_folderID()
				
				//--------------------------------Function to search Folder ID---------------------------------------//
				var file_name = quarter_text + '_' + financial_year;
				
				var file_obj = nlapiCreateFile(file_name + '.txt', 'PLAINTEXT', header);
				file_obj.setFolder(folder_id);
				var file_id = nlapiSubmitFile(file_obj);
				
				if (file_id != null && file_id != '' && file_id != undefined) {
				
					o_quarterly_recordobj.setFieldValue('custrecord_tds_file_status', 'Completed');
					o_quarterly_recordobj.setFieldValue('custrecord_etds_file', file_id);
					
					var submit_id = nlapiSubmitRecord(o_quarterly_recordobj)
					nlapiLogExecution('DEBUG', 'ETDS REtun File', 'Updated quarterly detail record ID' + submit_id)
				}
				
				var sent_mail = context.getSetting('SCRIPT', 'custscript_sent_etdsmail')
				nlapiLogExecution('DEBUG', 'mail', 'sent_mail obj->' + sent_mail)
				
				if (sent_mail == 'T') {
					var user = nlapiGetUser();
					var mail = nlapiSendEmail(user, email, 'TDS file', 'tds return file', null, null, null, file_obj, null, null)
					
				}
				var usageRemaining = context.getRemainingUsage();
				nlapiLogExecution('DEBUG', 'end usage', 'usageRemaining obj->' + usageRemaining)
			}
		}
		
	}
	catch(Execption)
	{
		nlapiLogExecution('DEBUG', 'etds file', 'Etds file error->' + Execption)
		nlapiSubmitField('customrecord_quarterly_return_details', i_recordid,'custrecord_tds_file_status','Failed:-'+ Execption)
	}

}

// END SCHEDULED FUNCTION ===============================================





// BEGIN FUNCTION ===================================================
{
//----------------------------------------------------Begin function to create a file header-------------------------------------------------//	
	
function create_file_header(subsidiary,quarter,quarter_startdate,quarter_enddate,o_quarterly_recordobj)
{
	var context = nlapiGetContext();
	var i_subcontext = context.getFeature('SUBSIDIARIES');
	var company_filters = new Array();
	
	var company_column = new Array();
	company_column.push( new nlobjSearchColumn('internalid'));    
	
	var company_results = nlapiSearchRecord('customrecord_taxcompanyinfo', null, company_filters, company_column);
	if (company_results != null && company_results != '' && company_results != undefined) 
	{
		var i_internalid = company_results[0].getValue('internalid')
		nlapiLogExecution('DEBUG', 'company info record', 'internalid->' + i_internalid)
		
	//------------------------------------------------Begin Load company info record ----------------------------------------------------------//
		
		var o_company_obj = nlapiLoadRecord('customrecord_taxcompanyinfo',i_internalid);
		nlapiLogExecution('DEBUG', 'company info record', 'company obj->' + o_company_obj)
		
		var deductor_tan = o_company_obj.getFieldValue('custrecord_tax_deduction_accountno')
		
		
		deductor_tan = log_validation(deductor_tan)
		
		var deductor_pan = o_company_obj.getFieldValue('custrecord_permanent_accountno')
		
		
		deductor_pan = log_validation(deductor_pan)
		
		var deductor_name = o_company_obj.getFieldValue('name')
		
		
		deductor_name = log_validation(deductor_name)
		
		var deductor_branch = o_company_obj.getFieldValue('custrecord_branch')
		
		
		deductor_branch = log_validation(deductor_branch)
		
		var deductor_add_1 = o_company_obj.getFieldValue('custrecord_nameofthe_premises_buildi')
	
		
		deductor_add_1 = log_validation(deductor_add_1)
		
		var deductor_add_2 = o_company_obj.getFieldValue('custrecord_road_street_lane')
	
		
		deductor_add_2 = log_validation(deductor_add_2)
		
		var deductor_add_3 = o_company_obj.getFieldValue('custrecord_area_location')
		
		
		deductor_add_3 = log_validation(deductor_add_3)
		
		var deductor_add_4 = o_company_obj.getFieldValue('custrecord_town_city_district')
	
		
		deductor_add_4 = log_validation(deductor_add_4)
		
		var deductor_state = o_company_obj.getFieldValue('custrecord_state')
		
		
		deductor_state = log_validation(deductor_state)
		
		var deductor_state_code = nlapiLookupField('customrecord_tds_state_master',deductor_state,'custrecord_tdscode')
		
		
		var deductor_pincode = o_company_obj.getFieldValue('custrecord_pincode')
		
		
		deductor_pincode = log_validation(deductor_pincode)
		
		var deductor_email = o_company_obj.getFieldValue('custrecord_email')
		
		
		deductor_email = log_validation(deductor_email)
		
		var deductor_std_code = o_company_obj.getFieldValue('custrecord_std_code')
		
		
		deductor_std_code = log_validation(deductor_std_code)
		
		var deductor_tel_no = o_company_obj.getFieldValue('custrecord_telephone_no')
		
		
		deductor_tel_no = log_validation(deductor_tel_no)
		
		var deductor_type = o_company_obj.getFieldValue('custrecord_typeof_deductor')
		
		deductor_type = log_validation(deductor_type)
		
		var pao_code = o_company_obj.getFieldValue('custrecord_pao_code')
		
		pao_code = log_validation(pao_code)
		
		var ddo_code = o_company_obj.getFieldValue('custrecord_ddo_code')
		
		ddo_code = log_validation(ddo_code)
		
		var ministry_name = o_company_obj.getFieldValue('custrecord_ministry_name')
		
		if(ministry_name != null && ministry_name != '' && ministry_name != undefined)
		{
			ministry_name = ministry_name
		}
		else
		{
			ministry_name = 'N'
		}
		
		var ministry_name_other = o_company_obj.getFieldValue('custrecord_ministry_name_other')
		
		if(ministry_name_other != null && ministry_name_other != '' && ministry_name_other != undefined)
		{
			ministry_name_other = ministry_name_other
		}
		else
		{
			ministry_name_other = 'N'
		}
		
		var pao_registration_no = o_company_obj.getFieldValue('custrecord_pao_registration_no')
		
		if(pao_registration_no != null && pao_registration_no != '' && pao_registration_no != undefined)
		{
			pao_registration_no = pao_registration_no
		}
		else
		{
			pao_registration_no = 'N'
		}
		
		var ddo_registration_no = o_company_obj.getFieldValue('custrecord_ddo_registration_no')
	
		if(ddo_registration_no != null && ddo_registration_no != '' && ddo_registration_no != undefined)
		{
			ddo_registration_no = ddo_registration_no
		}
		else
		{
			ddo_registration_no = 'N'
		}
		
		var tax_deduct_acc = o_company_obj.getFieldValue('custrecord_tax_deduction_accountno')
	
		tax_deduct_acc = log_validation(tax_deduct_acc)
		
		var ain = o_company_obj.getFieldValue('custrecord_ain')
		
		
		ain = log_validation(ain)
		
	//---------------------------------------------------End Load company info record ---------------------------------------------------------------//	
	
		
	//---------------------------------------------------Begin Load an employee record details ------------------------------------------------------//
		
		var resp_person_id = o_quarterly_recordobj.getFieldValue('custrecord_person_resposible')
		nlapiLogExecution('DEBUG', 'company info record', 'resposible person->' + resp_person);
		
		var resp_person = o_quarterly_recordobj.getFieldText('custrecord_person_resposible')
		
		
		var resp_designation = o_quarterly_recordobj.getFieldValue('custrecord_person_resp_designation')
		
		
		var o_emp_obj = nlapiLoadRecord('employee',resp_person_id)
		nlapiLogExecution('DEBUG', 'employee record', 'employee obj->' + o_emp_obj);
		
		o_emp_obj.selectLineItem('addressbook',1)
		
		var subrecord = o_emp_obj.viewCurrentLineItemSubrecord('addressbook', 'addressbookaddress');
		nlapiLogExecution('DEBUG', 'employee record', 'subrecord->' + subrecord);
		
		var emp_addr_1 = subrecord.getFieldValue('addr1');
		
		
		emp_addr_1 = log_validation(emp_addr_1)

		var emp_addr_2 = subrecord.getFieldValue('addr2');
	
		
		emp_addr_2 = log_validation(emp_addr_2)
		/*
		var emp_state = subrecord.getFieldValue('state');
		nlapiLogExecution('DEBUG', 'employee record', 'emp state->' + emp_state);
		
		emp_state = log_validation(emp_state)
		
		var emp_state_code = get_employee_state_code(emp_state);
		nlapiLogExecution('DEBUG', 'employee record', 'emp state code->' + emp_state_code);
		*/
		
		var emp_state = o_company_obj.getFieldValue('custrecord_person_resp_state')
		nlapiLogExecution('DEBUG', 'employee record', 'emp state->' + emp_state);
		
		var emp_state_code = nlapiLookupField('customrecord_tds_state_master',emp_state,'custrecord_tdscode')

		var emp_zip = subrecord.getFieldValue('zip');
	
		
		emp_zip = log_validation(emp_zip)
		
		var resp_email = o_emp_obj.getFieldValue('email')
		
		
		resp_email = log_validation(resp_email)
		
		var resp_mobilephone = o_emp_obj.getFieldValue('mobilephone')
	
		
		resp_mobilephone = log_validation(resp_mobilephone)
		
		var resp_phone = o_emp_obj.getFieldValue('phone')
		
		
		resp_phone = log_validation(resp_phone)
		
	//---------------------------------------------------END Load an employee record Details------------------------------------------------------//
	}
		
	var file_creation_date = o_quarterly_recordobj.getFieldValue('custrecord_quarterlyreturn_filecreated')
	nlapiLogExecution('DEBUG', 'quarterly detail record', 'file creation date->' + file_creation_date);
	
	var creation_date = formatdate(file_creation_date)
	
	var total_batches = get_total_batches(subsidiary,quarter_startdate,quarter_enddate,i_subcontext);
	
	var total_challans = get_total_challans(subsidiary,quarter_startdate,quarter_enddate,i_subcontext);
		
	var total_amount = get_total_amount(subsidiary,quarter_startdate,quarter_enddate,i_subcontext);
	
	//--------------------------------------------------- Begin File header section-----------------------------------------------------------------//
				
	//var file_header = '1^FH^NS1^R^' + creation_date +'^1^D^'+deductor_tan+'^'+total_batches+'^AASHNA Taxation^^^^^^^^';
	//change the batch header logic on 10 feb2015 as discussed with satish and chaitnya :- (total batches == 1 always)
	
	var file_header = '1^FH^NS1^R^' + creation_date +'^1^D^'+deductor_tan+'^1^AASHNA Taxation^^^^^^^^';
	
	//--------------------------------------------------- End File header section-------------------------------------------------------------------//
	
	var return_type = o_quarterly_recordobj.getFieldText('custrecord_return_type')
	
	var quarter_text = o_quarterly_recordobj.getFieldText('custrecord_quater')
	
	var i_finacial_id = o_quarterly_recordobj.getFieldValue('custrecord_financial_year')
	nlapiLogExecution('DEBUG', 'quarterly detail record', 'fiancial internal id->' + i_finacial_id);
	
	var o_financial_obj = nlapiLoadRecord('customrecord_financial_year',i_finacial_id)
	nlapiLogExecution('DEBUG', 'Financial  record', 'fiancial record obj->' + o_financial_obj);
	
	var financialyr = o_financial_obj.getFieldValue('custrecord_fy_code')
	
	
	var assesmentyr = o_financial_obj.getFieldValue('custrecord_assessment_code')
	
  	var preivous_token_number = get_previous_token(subsidiary,i_finacial_id,quarter,financialyr,i_subcontext)
	
	var token_array = preivous_token_number.split('##');
	
	var token_number = token_array[0];
	
	var token_date = token_array[1];
	
	var chk_previousform26 = token_array[2];
	
	if(chk_previousform26 == parseInt(1))
	{
		chk_previousform26 = 'Y';
	}
	else
	{
		chk_previousform26 = 'N';
	}
	token_date = formatdate(token_date)
	token_date = log_validation(token_date)
	//-------------------------------------------------------------Begin Single File header section-------------------------------------------------------------------------//

	//var single_file_header_1 = '2^BH^'+quarter+'^'+total_challans+'^'+return_type+'^^^^'+token_number+'^^'+token_date+'^^'+deductor_tan+'^^'+deductor_pan
	//change the batch header logic on 10 feb2015 as discussed with satish and chaitnya :- (quarter as batches == 1 always)
	var single_file_header_1 = '2^BH^1^'+total_challans+'^'+return_type+'^^^^'+token_number+'^^'+token_date+'^^'+deductor_tan+'^^'+deductor_pan
	
	var single_file_header_2 = '^'+assesmentyr+'^'+financialyr+'^'+quarter_text+'^'+deductor_name+'^'+deductor_branch+'^'+deductor_add_1+'^'+deductor_add_2+'^'+deductor_add_3+'^'+deductor_add_4+'^^'+deductor_state_code+'^'+deductor_pincode+'^'+deductor_email+'^'+deductor_std_code+'^'+deductor_tel_no
	
	var single_file_header_3 = '^N^'+deductor_type+'^'+resp_person+'^'+resp_designation+'^'+emp_addr_1+'^'+emp_addr_2+'^^^^'+emp_state_code+'^'+emp_zip+'^'+resp_email+'^'+resp_mobilephone+'^^'+resp_phone
	
	var single_file_header_4 = '^N^'+total_amount+'^^^^N^'+chk_previousform26+'^^N^'+pao_code+'^'+ddo_code+'^'+ministry_name+'^'+ministry_name_other+'^'+tax_deduct_acc+'^'+pao_registration_no
	
	var single_file_header_5 = '^'+ddo_registration_no+'^^^^^^^'+ain+'^';
	
	//-----------------------------------------------------------End single file header section--------------------------------------------------------------------------------//
	
	
	//----------------------------------------------------------Begin Final output of file header and single file header-----------------------------------//
	
	var output = file_header+'\r\n'+single_file_header_1+single_file_header_2+single_file_header_3+single_file_header_4+single_file_header_5
	
	//----------------------------------------------------------End Final output of file header and single file header-----------------------------------//
	return output;
	
}
//----------------------------------------------------End function to create a file header-------------------------------------------------//	


//----------------------------------------------------Begin function to get total batches ---------------------------------------------------//

function get_total_batches(subsidiary,quarter_startdate,quarter_enddate,i_subcontext)
{		
	var total_batches = 0;
	var filters = new Array();
	filters.push( new nlobjSearchFilter('custrecord_billdate', null, 'onorafter',quarter_startdate));
	filters.push( new nlobjSearchFilter('custrecord_billdate', null, 'onorbefore', quarter_enddate));
	if (i_subcontext != false) 
	{
		filters.push(new nlobjSearchFilter('custrecord_billsubsidiary', null, 'is', subsidiary));
	}
	filters.push( new nlobjSearchFilter('custrecord_billstatus', null, 'is', 'Close'));
	filters.push( new nlobjSearchFilter('custrecord_billchallanno', null, 'isnotempty')); 
	filters.push( new nlobjSearchFilter('isinactive', null, 'is','F')); 
	    
	var column = new Array();
	column.push( new nlobjSearchColumn('internalid'));    
	
	var results = nlapiSearchRecord('customrecord_tdsbillrelation', null, filters, column);
	if (results != null && results != '' && results != undefined) 
	{
		total_batches = parseInt(results.length)
		nlapiLogExecution('DEBUG', 'create_quartely_detailrecord', 'total_batches->' + total_batches)
		
	}
	return total_batches;
}
//----------------------------------------------------Begin function to get total batches ---------------------------------------------------//

//----------------------------------------------------Begin function to get total no. of challans--------------------------------------------//
function get_total_challans(subsidiary,quarter_startdate,quarter_enddate,i_subcontext)
{		
	var total_challan = 0;
	var filters = new Array();
	filters.push( new nlobjSearchFilter('custrecord_billdate', null, 'onorafter',quarter_startdate));
	filters.push( new nlobjSearchFilter('custrecord_billdate', null, 'onorbefore', quarter_enddate));
	if (i_subcontext != false) 
	{
		filters.push(new nlobjSearchFilter('custrecord_billsubsidiary', null, 'is', subsidiary));
	}
	filters.push( new nlobjSearchFilter('custrecord_billstatus', null, 'is', 'Close'));
	filters.push( new nlobjSearchFilter('custrecord_billchallanno', null, 'isnotempty')); 
	filters.push( new nlobjSearchFilter('isinactive', null, 'is','F')); 
	    
	var column = new Array();
	column.push(  new nlobjSearchColumn('custrecord_billchallanno',null,'group'));   
	
	var results = nlapiSearchRecord('customrecord_tdsbillrelation', null, filters, column);
	if (results != null && results != '' && results != undefined) 
	{
		total_challan = parseInt(results.length)
		nlapiLogExecution('DEBUG', 'create_quartely_detailrecord', 'total_challan->' + total_challan)
		
	}
	return total_challan;
}
//----------------------------------------------------End function to get total no. of challans--------------------------------------------//


//----------------------------------------------------Begin function to get total challan amount-------------------------------------------//

function get_total_amount(subsidiary,quarter_startdate,quarter_enddate,i_subcontext)
{
	var total_amount = parseFloat(0);
	var totalamt_filters = new Array();
	totalamt_filters.push( new nlobjSearchFilter('custrecord_billdate', null, 'onorafter',quarter_startdate));
	totalamt_filters.push( new nlobjSearchFilter('custrecord_billdate', null, 'onorbefore', quarter_enddate));
	if (i_subcontext != false) 
	{
		totalamt_filters.push(new nlobjSearchFilter('custrecord_billsubsidiary', null, 'is', subsidiary));
	}
	totalamt_filters.push( new nlobjSearchFilter('custrecord_billstatus', null, 'is', 'Close'));
	totalamt_filters.push( new nlobjSearchFilter('custrecord_billchallanno', null, 'isnotempty')); 
	totalamt_filters.push( new nlobjSearchFilter('isinactive', null, 'is','F')); 
	    
	var total_amt_column = new Array();
	total_amt_column[0] =  new nlobjSearchColumn('custrecord_billtdsamount',null,'sum');    
	
	var results = nlapiSearchRecord('customrecord_tdsbillrelation', null, totalamt_filters, total_amt_column);
	if (results != null && results != '' && results != undefined) 
	{
		total_amount = results[0].getValue(total_amt_column[0])
		nlapiLogExecution('DEBUG', 'create_quartely_detailrecord', 'total_amount->' + total_amount)
		
	}	
	return total_amount;
}
//----------------------------------------------------End function to get total challan amount-------------------------------------------//


//----------------------------------------------------Begin function to get previous token number and date of quarterly detail record-------------------------------------------//

function get_previous_token(subsidiary,i_finacial_id,quarter,financialyr,i_subcontext)
{
	
	var token_no= '';
	var token_date = '';
	var chk_previous_form26 = ''; 
	
	if (parseInt(quarter) == parseInt(1)) 
	{
		var quarter = parseInt(4)
		var start_yr = financialyr.substring(0, 4)
		var end_yr = financialyr.substring(4, 6)
		
		var start_yr = (parseInt(start_yr) - parseInt(1));
		var end_yr = (parseInt(end_yr) - parseInt(1));
		
		var year = start_yr+''+end_yr
		nlapiLogExecution('DEBUG', 'get previous token', 'year->' + year)
		
		var year = parseInt(year);
		
		var financil_filters = new Array();
		financil_filters.push( new nlobjSearchFilter('custrecord_fy_code', null, 'is',year));
	
		var financial_column = new Array();
		financial_column.push( new nlobjSearchColumn('internalid'));    
		   
		var financial_results = nlapiSearchRecord('customrecord_financial_year', null, financil_filters, financial_column);
		if (financial_results != null && financial_results != '' && financial_results != undefined) 
		{
			var i_finacial_id = financial_results[0].getValue('internalid')
			nlapiLogExecution('DEBUG', 'financial year record', 'i_finacial_id->' + i_finacial_id)
			
		}	
	}
	else 
	{
		var quarter = parseInt(quarter) - parseInt(1)
	}
	
		var token_filters = new Array();
		token_filters.push(new nlobjSearchFilter('custrecord_financial_year', null, 'is', i_finacial_id));
		token_filters.push(new nlobjSearchFilter('custrecord_quater', null, 'is', quarter));
		if (i_subcontext != false) 
		{
			token_filters.push(new nlobjSearchFilter('custrecord_quarterlyreturn_subsidiary', null, 'is', subsidiary));
		}
		var token_column = new Array();
		token_column[0] = new nlobjSearchColumn('custrecord_token_number');
		token_column[1] = new nlobjSearchColumn('custrecord_token_date');
		
		var results = nlapiSearchRecord('customrecord_quarterly_return_details', null, token_filters, token_column);
		if (results != null && results != '' && results != undefined) 
		{
			token_no = results[0].getValue('custrecord_token_number')
			
			
			token_date = results[0].getValue('custrecord_token_date')
			
			chk_previous_form26 = results[0].getValue('custrecord_return_type')
		
			
		}
	var token = token_no+'##'+token_date+'##'+chk_previous_form26
	nlapiLogExecution('DEBUG', 'generate token number', 'token->' + token)
	return token;
	
}
//===================================================End function to get previous token number and date of quarterly detail record-------------------------------------------//


//===================================================Begin function to get employee state code======================================
function get_employee_state_code(emp_state)
{
	var state_filters = new Array();
	state_filters.push( new nlobjSearchFilter('name', null, 'is',emp_state));
	   
	var state_column = new Array();
	state_column[0] =  new nlobjSearchColumn('custrecord_tdscode');    
	
	var results = nlapiSearchRecord('customrecord_tds_state_master', null, state_filters, state_column);
	if (results != null && results != '' && results != undefined) 
	{
		var state_code = results[0].getValue('custrecord_tdscode')
		nlapiLogExecution('DEBUG', 'State code', 'state_code->' + state_code)
	}	
	else
	{
		//For other state
		var state_code = '99';
	}
	return state_code;
	
}
//===================================================End function to get employee state code======================================

//===================================================Function to create an challan header and deductee details======================

function create_challan_header(subsidiary,quarter,quarter_startdate,quarter_enddate,o_quarterly_recordobj)
{
	//nlapiLogExecution('DEBUG', 'create_challan_header', 'inchallan ->')
	
	//nlapiLogExecution('DEBUG', 'create_challan_header', 'start date inchallan ->'+quarter_startdate)
	
	//nlapiLogExecution('DEBUG', 'create_challan_header', 'end date inchallan ->'+quarter_enddate)
	
	var context = nlapiGetContext();
	var i_subcontext = context.getFeature('SUBSIDIARIES');
	
	var final_header = '';
	
	var p = parseInt(2);
	
	//add a minor head on 10 feb 2015
	var minor_head = o_quarterly_recordobj.getFieldText('custrecord_minor_head');
	
	var book_entry = o_quarterly_recordobj.getFieldValue('custrecord_bookentry');
	if(parseInt(book_entry) == parseInt(1))
	{
		book_entry = 'Y'
	}
	else
	{
		book_entry = 'N'
	}
	var challan_filters = new Array();
	challan_filters.push( new nlobjSearchFilter('custrecord_billdate', null, 'onorafter',quarter_startdate));
	challan_filters.push( new nlobjSearchFilter('custrecord_billdate', null, 'onorbefore', quarter_enddate));
	
	if (i_subcontext != false) 
	{
		challan_filters.push(new nlobjSearchFilter('custrecord_billsubsidiary', null, 'is', subsidiary));
	}
	challan_filters.push( new nlobjSearchFilter('custrecord_billstatus', null, 'is', 'Close'));
	challan_filters.push( new nlobjSearchFilter('custrecord_billchallanno', null, 'isnotempty')); 
	challan_filters.push( new nlobjSearchFilter('isinactive', null, 'is','F')); 
	 
	 
	var challan_column = new Array();
	challan_column[0] =  new nlobjSearchColumn('custrecord_billchallanno',null,'group');
	challan_column[1] =  new nlobjSearchColumn('custrecord_billtdsamount',null,'sum');
	challan_column[2] =  new nlobjSearchColumn('custrecord_bill_paymentdate',null,'group');
	challan_column[2].setSort(false);  
	
	var challan_results = nlapiSearchRecord('customrecord_tdsbillrelation', null, challan_filters, challan_column);
	
	if (challan_results != null && challan_results != '' && challan_results != undefined) 
	{
		nlapiLogExecution('DEBUG', 'create_quartely_detailrecord', 'challan_results.length' + challan_results.length)
		for (var i = 0; i < challan_results.length; i++) 
		{
			var challan_number = challan_results[i].getValue(challan_column[0])
			nlapiLogExecution('DEBUG', 'create_quartely_detailrecord', 'challan_number->' + challan_number)
			
			if (challan_number != '' && challan_number != null && challan_number != 'undefined') 
			{
				var cd_count ; 
				if(parseInt(i) == parseInt(0))
				{
						cd_count = 1;
				}
				else
				{
					cd_count = (parseInt(cd_count) + parseInt(1))
				}
				
				p = (parseInt(p) + parseInt(1))
			 
				var challan_amount = challan_results[i].getValue(challan_column[1])
				
				challan_amount = parseFloat(challan_amount);
				challan_amount = challan_amount.toFixed(2);
				
				var challan_details = get_challan_details(subsidiary, challan_number, quarter, quarter_startdate, quarter_enddate,i_subcontext)
				
				
				var deductee_count_filters = new Array();
				deductee_count_filters.push(new nlobjSearchFilter('custrecord_billdate', null, 'onorafter', quarter_startdate));
				deductee_count_filters.push(new nlobjSearchFilter('custrecord_billdate', null, 'onorbefore', quarter_enddate));
				deductee_count_filters.push(new nlobjSearchFilter('custrecord_billchallanno', null, 'is', challan_number));
				deductee_count_filters.push(new nlobjSearchFilter('isinactive', null, 'is','F')); 
				if (i_subcontext != false) 
				{
					deductee_count_filters.push(new nlobjSearchFilter('custrecord_billsubsidiary', null, 'is', subsidiary));
				}
				deductee_count_filters.push(new nlobjSearchFilter('custrecord_billstatus', null, 'is', 'Close'));
				
				var deductee_count_column = new Array();
				deductee_count_column[0] = new nlobjSearchColumn('custrecord_billvendorrel', null, 'group');
				deductee_count_column[1] = new nlobjSearchColumn('custrecord_billtdsamount', null, 'sum');
				
				var deductee_results = nlapiSearchRecord('customrecord_tdsbillrelation', null, deductee_count_filters, deductee_count_column);
				
				if (deductee_results != null && deductee_results != '' && deductee_results != 'undefined') 
				{
					//----------------------------------------------------------------------Below are the total challan amount details of field----------------------------------------------------------------------------//
				//field 27 Total of DepositAmount as per Challan/Transfer Voucher Number ('Oltas TDS/TCS -Income Tax ' + 'Oltas TDS/ TCS - Surcharge '+ 'Oltas TDS/ TCS - Cess' + Oltas TDS/ TCS -Interest Amount + Oltas TDS/ TCS -Others (amount)
				//field 30 :-Total sum of column no. 421/721 for the respective Challan
				//field 29 - Total Tax Deposit Amount as per deductee annexure  (Total Sum of column no. 421)
				//Field 33 :- Sum of 'Total Income Tax Deducted at Source' (TDS   - Income Tax ) 	
				//----------------------------------------------------------------------above are the total challan amount-------------------------------------------------------------------------//
				
				//-----------------------------------------------------------Begin challan Header ----------------------------------------------------------------//
					var fees = '0.00';
					var interest_amt = '0.00';
					var others = '0.00';
					//var challan_header_1 = p + '^CD^' + quarter + '^' + cd_count + '^' + deductee_results.length + '^N^^^^^^' + challan_number + '^^^^' + challan_details[0] + '^^' + challan_details[1] + '^^^'
					//change the batch header logic on 10 feb2015 as discussed with satish and chaitnya :- (quarter as batches == 1 always)
					//remove the tds section as it is not applicable after 2013-14
					
					var challan_header_1 = p + '^CD^1^' + cd_count + '^' + challan_details[4] + '^N^^^^^^' + challan_number + '^^^^' + challan_details[0] + '^^' + challan_details[1] + '^^^'
					
					var challan_header_2 = '^' + challan_amount + '^0.00^0.00^0.00^0.00^' + challan_amount + '^^' + challan_amount + '^' + challan_amount + '^0.00^0.00^' + challan_amount + '^'+interest_amt+'^'+others+'^' + challan_details[3] + '^' + book_entry + '^^'+fees+'^'+minor_head+'^'
					
					var challan_header_3 = challan_header_1 + challan_header_2 + '\r\n'
					
					
				//----------------------------------------------------------End challan header---------------------------------------------------------------------//
				
					var deductee_details_2 = '';
					var dd_count = 0;
					for (var k = 0; k < deductee_results.length; k++) 
					{
						
						var deductee_name = deductee_results[k].getValue(deductee_count_column[0])
						
						if (deductee_name != '' && deductee_name != null && deductee_name != undefined) 
						{
							
						var deductee_amount = deductee_results[k].getValue(deductee_count_column[1])
						
						deductee_amount = parseFloat(deductee_amount)
						deductee_amount = deductee_amount.toFixed(2);
						
						//-----------------------------------------Begin load the vendor record---------------------------------------------------------------//
						
						
							var vendor_obj = nlapiLoadRecord('vendor', deductee_name)
							nlapiLogExecution('DEBUG', 'vendor', 'vendor recrod object->' + vendor_obj)
							
							var dd_code = vendor_obj.getFieldValue('isperson')
							
							if (dd_code == 'F') 
							{
								dd_code = 1; // for companies
							}
							else 
							{
								dd_code = 2;// for other
							}
							
							var dd_pan = vendor_obj.getFieldValue('custentity_vendor_panno')
							
							dd_pan = log_validation(dd_pan)
							
							var dd_referenceno = vendor_obj.getFieldValue('custentity_deductee_refno')
							
							dd_referenceno = log_validation(dd_referenceno)
							
							var dd_name = vendor_obj.getFieldValue('entityid')
							
							dd_name = log_validation(dd_name)
							
							var dd_certificate = vendor_obj.getFieldValue('custentity_ao_certificate_number')
						    dd_certificate = log_validation(dd_certificate)
							
							//----------------------------------------------------END load the vendor record ---------------------------------------------------------//
							
							//----------------------------------------------------Begin create Deductee detail ------------------------------------------------//		
							var deductee_details = '';
							
							var dd_filters = new Array();
							dd_filters.push(new nlobjSearchFilter('custrecord_billdate', null, 'onorafter', quarter_startdate));
							dd_filters.push(new nlobjSearchFilter('custrecord_billdate', null, 'onorbefore', quarter_enddate));
							dd_filters.push(new nlobjSearchFilter('custrecord_billchallanno', null, 'is', challan_number));
							dd_filters.push(new nlobjSearchFilter('custrecord_billvendorrel', null, 'is', deductee_name));
							dd_filters.push(new nlobjSearchFilter('custrecord_billstatus', null, 'is', 'Close'));
							dd_filters.push( new nlobjSearchFilter('isinactive', null, 'is','F')); 
							if (i_subcontext != false) 
							{
								dd_filters.push(new nlobjSearchFilter('custrecord_billsubsidiary', null, 'is', subsidiary));
							}
							
							var dd_column = new Array();
							dd_column[0] = new nlobjSearchColumn('custrecord_billbillamount');
							dd_column[1] = new nlobjSearchColumn('custrecord_billdate');
							dd_column[2] = new nlobjSearchColumn('custrecord_tdsperrate');
							dd_column[3] = new nlobjSearchColumn('custrecord_reasonfornondeductionl');
							dd_column[4] = new nlobjSearchColumn('custrecord_billtdstype');
							dd_column[5] = new nlobjSearchColumn('custrecord_billtdsamount');
							
							var dd_results = nlapiSearchRecord('customrecord_tdsbillrelation', null, dd_filters, dd_column);
							
							if (dd_results != null && dd_results != '' && dd_results != 'undefined') 
							{
								for (var l = 0; l < dd_results.length; l++) 
								{
									
									dd_count = (parseInt(dd_count) + parseInt(1))
									

									var bill_amount = dd_results[l].getValue('custrecord_billbillamount')
									
									bill_amount = parseFloat(bill_amount)
									bill_amount = bill_amount.toFixed(2);
									
									var tds_bill_amount = parseFloat(0);
									var tds_bill_amount = dd_results[l].getValue('custrecord_billtdsamount')
									
									tds_bill_amount = parseFloat(tds_bill_amount)
									tds_bill_amount = tds_bill_amount.toFixed(2);
									
									
									
									var bill_date = dd_results[l].getValue('custrecord_billdate')
									
									bill_date = formatdate(bill_date);
								
									var tds_rate = dd_results[l].getValue('custrecord_tdsperrate')
									
									var tds_per = tds_rate.split('%')
									
									tds_rate = tds_per[0];
									tds_rate = parseFloat(tds_rate);
									tds_rate = tds_rate.toFixed(4);
									
									var reason_for_deduction = dd_results[l].getValue('custrecord_reasonfornondeductionl')
									
									var tds_type = dd_results[l].getValue('custrecord_billtdstype')
									
									var payment_code = nlapiLookupField('customrecord_tdsmaster', tds_type, 'custrecord_paymentcode')
									
									p = (parseInt(p) + parseInt(1))
									
									//total_tax = Total Income Tax Deducted at Source (TDS / TCS IncomeTax+ TDS /TCS Surcharge+ TDS/TCS Cess) I.e. (421+ 422 + 423 )
									//17 field :- var total_tax = deductee_amount + tds_surchage + tds_cess
									
									//----------------------------------------------------------Begin deductee detail record string -----------------------------------------------------------------//
									
									//var dd_header_1 = p + '^DD^' + quarter + '^' + cd_count + '^' + dd_count + '^O^^' + dd_code + '^^' + dd_pan + '^^' + dd_referenceno + '^' + dd_name + '^' + tds_bill_amount + '^0.00^0.00^' + tds_bill_amount + '^^' + tds_bill_amount + '^^'
									
									//change the batch header logic on 10 feb2015 as discussed with satish and chaitnya :- (quarter as batches == 1 always)
									
									var dd_header_1 = p + '^DD^1^' + cd_count + '^' + dd_count + '^O^^' + dd_code + '^^' + dd_pan + '^^' + dd_referenceno + '^' + dd_name + '^' + tds_bill_amount + '^0.00^0.00^' + tds_bill_amount + '^^' + tds_bill_amount + '^^'
									
									var dd_header_2 = '^' + bill_amount + '^' + bill_date + '^' + bill_date + '^^' + tds_rate + '^^' + book_entry + '^^' + reason_for_deduction + '^^^' + payment_code + '^' + dd_certificate + '^^^^^' + '\r\n'
									
									
									deductee_details = deductee_details + dd_header_1 + dd_header_2;
									
									//-----------------------------------------------------------End Deductee detail record string -------------------------------------------------------------//
								}
							}
							deductee_details_2 = deductee_details_2 + deductee_details
							
						//--------------------------------------------------------End create dedudtee detail --------------------------------------------//
						}
					}
				}
				final_header = final_header + challan_header_3 + deductee_details_2
			}
		}
	}
	return final_header;
}
function get_challan_details(subsidiary,challan_number,quarter,quarter_startdate,quarter_enddate,i_subcontext)
{
	var challan_details = new Array();
	
	var challan_details_filters = new Array();
	challan_details_filters.push( new nlobjSearchFilter('custrecord_billdate', null, 'onorafter',quarter_startdate));
	challan_details_filters.push( new nlobjSearchFilter('custrecord_billdate', null, 'onorbefore', quarter_enddate));
	if (i_subcontext != false) 
	{
		challan_details_filters.push(new nlobjSearchFilter('custrecord_billsubsidiary', null, 'is', subsidiary));
	}
	challan_details_filters.push( new nlobjSearchFilter('custrecord_billstatus', null, 'is', 'Close'));	
	challan_details_filters.push( new nlobjSearchFilter('custrecord_billchallanno', null, 'isnotempty')); 
	challan_details_filters.push( new nlobjSearchFilter('custrecord_billvendorrel', null, 'isnotempty')); 
	challan_details_filters.push( new nlobjSearchFilter('custrecord_billchallanno', null, 'is',challan_number));  
	challan_details_filters.push( new nlobjSearchFilter('isinactive', null, 'is','F'));  
	    
	var challan_details_column = new Array();
	challan_details_column.push(new nlobjSearchColumn('custrecord_bill_bsrcode'));
	challan_details_column.push(new nlobjSearchColumn('custrecord_bill_paymentdate'));
	challan_details_column.push(new nlobjSearchColumn('custrecord_billtdssection'));
	challan_details_column.push(new nlobjSearchColumn('custrecord_tds_cheque_no'));
	
	var challan_detail_results = nlapiSearchRecord('customrecord_tdsbillrelation', null, challan_details_filters, challan_details_column);
	
	if (challan_detail_results != null && challan_detail_results != '' && challan_detail_results != undefined) 
	{
		challan_details[0] = challan_detail_results[0].getValue('custrecord_bill_bsrcode')
	
		challan_details[1] = challan_detail_results[0].getValue('custrecord_bill_paymentdate')
		
		challan_details[1] = formatdate(challan_details[1])
		
		challan_details[2] = challan_detail_results[0].getValue('custrecord_billtdssection')
		
		challan_details[3] = challan_detail_results[0].getValue('custrecord_tds_cheque_no')
		
		challan_details[4] = challan_detail_results.length
	}
	
	return challan_details;
	
}
function get_quarter_date(quarter_start_date,quarter)
{
    var ctx = nlapiGetContext();
    var date_format = ctx.getPreference('DATEFORMAT');
   
    
    var d_string = quarter_start_date; //nlapiDateToString(date_s);
   
    
    if (date_format == 'MM/DD/YYYY') {
	
		var date_split = d_string.split('/');
		
		var mm = date_split[0];
		var dd = date_split[1];
		var yy = date_split[2];
		
		
		if (parseInt(quarter) == parseInt(1)) {
		
			var quarter_start_date = 4 + '/' + 1 + '/' + yy
			var quarter_end_date = 6 + '/' + 30 + '/' + yy
			
			var final_date = quarter_start_date + '#' + quarter_end_date
		}
		
		if (parseInt(quarter) == parseInt(2)) {
		
			var quarter_start_date = 7 + '/' + 1 + '/' + yy
			var quarter_end_date = 9 + '/' + 30 + '/' + yy
			
			var final_date = quarter_start_date + '#' + quarter_end_date
		}
		if (parseInt(quarter) == parseInt(3)) {
		
			var quarter_start_date = 10 + '/' + 1 + '/' + yy
			var quarter_end_date = 12 + '/' + 31 + '/' + yy
			
			var final_date = quarter_start_date + '#' + quarter_end_date
		}
		if (parseInt(quarter) == parseInt(4)) {
			yy = parseInt(yy) + parseInt(1)
			var quarter_start_date = 1 + '/' + 1 + '/' + yy
			var quarter_end_date = 3 + '/' + 31 + '/' + yy
			
			var final_date = quarter_start_date + '#' + quarter_end_date
		}
		
		
		return final_date;
	}
	
	else 
	
		if (date_format == 'DD/MM/YYYY') {
		
			var date_split = d_string.split('/');
			
			
			var dd = date_split[0];
			var mm = date_split[1];
			var yy = date_split[2];
			
			if (parseInt(quarter) == parseInt(1)) {
				var quarter_start_date = 1 + '/' + 4 + '/' + yy
				var quarter_end_date = 30 + '/' + 6 + '/' + yy
				
				var final_date = quarter_start_date + '#' + quarter_end_date
			}
			
			if (parseInt(quarter) == parseInt(2)) {
			
				var quarter_start_date = 1 + '/' + 7 + '/' + yy
				var quarter_end_date = 30 + '/' + 9 + '/' + yy
				
				var final_date = quarter_start_date + '#' + quarter_end_date
			}
			if (parseInt(quarter) == parseInt(3)) {
			
				var quarter_start_date = 1 + '/' + 10 + '/' + yy
				var quarter_end_date = 31 + '/' + 12 + '/' + yy
				
				var final_date = quarter_start_date + '#' + quarter_end_date
			}
			if (parseInt(quarter) == parseInt(4)) {
				yy = parseInt(yy) + parseInt(1)
				var quarter_start_date = 1 + '/' + 1 + '/' + yy
				var quarter_end_date = 31 + '/' + 3 + '/' + yy
				
				var final_date = quarter_start_date + '#' + quarter_end_date
			}
			
			return final_date;
			
		}
		else 
		
			if (date_format == 'DD-Mon-YYYY') {
				var date_split = d_string.split('-');
				
				
				var dd = date_split[0];
				var mm = date_split[1];
			//	nlapiLogExecution('DEBUG', 'date_split', 'mm !!!!!!!!!!!!!!!!!1=' + mm);
				var yy = date_split[2];
				
				if (parseInt(quarter) == parseInt(1)) {
				
					var quarter_start_date = 1 + '-' + 'Apr' + '-' + yy
					var quarter_end_date = 30 + '-' + 'Jun' + '-' + yy
					
					var final_date = quarter_start_date + '#' + quarter_end_date
				}
				
				if (parseInt(quarter) == parseInt(2)) {
				
					var quarter_start_date = 1 + '-' + 'Jul' + '-' + yy
					var quarter_end_date = 30 + '-' + 'Sep' + '-' + yy
					
					var final_date = quarter_start_date + '#' + quarter_end_date
				}
				if (parseInt(quarter) == parseInt(3)) {
				
					var quarter_start_date = 1 + '-' + 'Oct' + '-' + yy
					var quarter_end_date = 31 + '-' + 'Dec' + '-' + yy
					
					var final_date = quarter_start_date + '#' + quarter_end_date
				}
				if (parseInt(quarter) == parseInt(4)) {
					yy = parseInt(yy) + parseInt(1)
					var quarter_start_date = 1 + '-' + 'Jan' + '-' + yy
					var quarter_end_date = 31 + '-' + 'Mar' + '-' + yy
					
					var final_date = quarter_start_date + '#' + quarter_end_date
				}
				
				
				return final_date;
				
			}
			
			else 
			
				if (date_format == 'DD.MM.YYYY') {
				
					var date_split = d_string.split('.');
				
					
					var dd = date_split[0];
					var mm = date_split[1];
					var yy = date_split[2];
					
					if (parseInt(quarter) == parseInt(1)) {
					
						var quarter_start_date = 1 + '.' +4+ '.' + yy
						var quarter_end_date = 30 + '.' + '6' + '.' + yy
						
						var final_date = quarter_start_date + '#' + quarter_end_date
					}
					
					if (parseInt(quarter) == parseInt(2)) {
					
						var quarter_start_date = 1 + '.' + 7 + '.' + yy
						var quarter_end_date = 30 + '.' + 9 + '.' + yy
						
						var final_date = quarter_start_date + '#' + quarter_end_date
					}
					if (parseInt(quarter) == parseInt(3)) {
					
						var quarter_start_date = 1 + '.' + 10 + '.' + yy
						var quarter_end_date = 31 + '.' + 12 + '.' + yy
						
						var final_date = quarter_start_date + '#' + quarter_end_date
					}
					if (parseInt(quarter) == parseInt(4)) {
						yy = parseInt(yy) + parseInt(1)
						var quarter_start_date = 1 + '.' + 1 + '.' + yy
						var quarter_end_date = 31 + '.' + 31 + '.' + yy
						
						var final_date = quarter_start_date + '#' + quarter_end_date
					}
					
					
					return final_date;
					
				}
				
				else 
				
					if (date_format == 'DD-MONTH-YYYY') {
					
						var date_split = d_string.split('-');
					
						
						var dd = date_split[0];
						var mm = date_split[1];
						
					
						var yy = date_split[2];
						
						if (parseInt(quarter) == parseInt(1)) {
						
							var quarter_start_date = 1 + '-' + 'April' + '-' + yy
							var quarter_end_date = 30 + '-' + 'June' + '-' + yy
							
							var final_date = quarter_start_date + '#' + quarter_end_date
						}
						
						if (parseInt(quarter) == parseInt(2)) {
						
							var quarter_start_date = 1 + '-' + 'July' + '-' + yy
							var quarter_end_date = 30 + '-' + 'September' + '-' + yy
							
							var final_date = quarter_start_date + '#' + quarter_end_date
						}
						if (parseInt(quarter) == parseInt(3)) {
						
							var quarter_start_date = 1 + '-' + 'October' + '-' + yy
							var quarter_end_date = 31 + '-' + 'December' + '-' + yy
							
							var final_date = quarter_start_date + '#' + quarter_end_date
						}
						if (parseInt(quarter) == parseInt(4)) {
							yy = parseInt(yy) + parseInt(1)
							var quarter_start_date = 1 + '-' + 'January' + '-' + yy
							var quarter_end_date = 31 + '-' + 'March' + '-' + yy
							
							var final_date = quarter_start_date + '#' + quarter_end_date
						}
						
						
						return final_date;
						
					}
					
					else 
					
						if (date_format == 'YYYY/MM/DD') {
						
							var date_split = d_string.split('/');
						
							var yy = date_split[0];
							var mm = date_split[1];
							var dd = date_split[2];
							
							if (parseInt(quarter) == parseInt(1)) {
							
								var quarter_start_date = yy + '/' + 4 + '/' + 1
								var quarter_end_date = yy + '/' + 6 + '/' + 30
								
								var final_date = quarter_start_date + '#' + quarter_end_date
							}
							
							if (parseInt(quarter) == parseInt(2)) {
							
								var quarter_start_date = yy + '/' + 7 + '/' + 1
								var quarter_end_date = yy + '/' + 9 + '/' + 30
								
								var final_date = quarter_start_date + '#' + quarter_end_date
							}
							if (parseInt(quarter) == parseInt(3)) {
							
								var quarter_start_date = yy + '/' + 10 + '/' + 1
								var quarter_end_date = yy + '/' + 12 + '/' + 31
								
								var final_date = quarter_start_date + '#' + quarter_end_date
							}
							if (parseInt(quarter) == parseInt(4)) {
								yy = parseInt(yy) + parseInt(1)
								var quarter_start_date = yy + '/' + 1 + '/' + 1
								var quarter_end_date = yy + '/' + 3 + '/' + 31
								
								var final_date = quarter_start_date + '#' + quarter_end_date
							}
							
							return final_date;
							
							
						}
						
						else 
						
							if (date_format == 'YYYY-MM-DD') {
								var date_split = d_string.split('-');
							
								var yy = date_split[0];
								var mm = date_split[1];
								var dd = date_split[2];
								
								
								if (parseInt(quarter) == parseInt(1)) {
								
									var quarter_start_date = yy + '-' + 4 + '-' + 1
									var quarter_end_date = yy + '-' + 6 + '-' + 30
									
									var final_date = quarter_start_date + '#' + quarter_end_date
								}
								
								if (parseInt(quarter) == parseInt(2)) {
								
									var quarter_start_date = yy + '-' + 7 + '-' + 1
									var quarter_end_date = yy + '-' + 9 + '-' + 30
									
									var final_date = quarter_start_date + '#' + quarter_end_date
								}
								if (parseInt(quarter) == parseInt(3)) {
								
									var quarter_start_date = yy + '-' + 10 + '-' + 1
									var quarter_end_date = yy + '-' + 12 + '-' + 31
									
									var final_date = quarter_start_date + '#' + quarter_end_date
								}
								if (parseInt(quarter) == parseInt(4)) {
									yy = parseInt(yy) + parseInt(1)
									var quarter_start_date = yy + '-' + 1 + '-' + 1
									var quarter_end_date = yy + '-' + 3 + '-' + 31
									
									var final_date = quarter_start_date + '#' + quarter_end_date
								}
								
								return final_date;
								
								
							}
							else 
							
								if (date_format == 'DD MONTH, YYYY') {
									var date_split = d_string.split(',');
								
									var ddmm = date_split[0];
									var yy = date_split[1];
									
									var dd_mm = ddmm.split(' ');
									var dd = dd_mm[0];
									var mm = dd_mm[1];
									
									if (parseInt(quarter) == parseInt(1)) {
									
										var quarter_start_date = '1 April' + ',' + yy
										var quarter_end_date = '30 June' + ',' + yy
										
										var final_date = quarter_start_date + '#' + quarter_end_date
									}
									
									if (parseInt(quarter) == parseInt(2)) {
									
										var quarter_start_date = '1 July' + '-' + yy
										var quarter_end_date = '30 September' + '-' + yy
										
										var final_date = quarter_start_date + '#' + quarter_end_date
									}
									if (parseInt(quarter) == parseInt(3)) {
									
										var quarter_start_date = '1 October' + '-' + yy
										var quarter_end_date = '31 December' + '-' + yy
										
										var final_date = quarter_start_date + '#' + quarter_end_date
									}
									if (parseInt(quarter) == parseInt(4)) {
										yy = parseInt(yy) + parseInt(1)
										var quarter_start_date = '1 January' + '-' + yy
										var quarter_end_date = '31 March' + '-' + yy
										
										var final_date = quarter_start_date + '#' + quarter_end_date
									}
									
									return final_date;
									
									
								}
								else {
									return null;
									
								}
    
    
}
function formatdate(date_value)
	{

	var ctx = nlapiGetContext();
	var date_format = ctx.getPreference('DATEFORMAT');
	
	var d_string = date_value; //nlapiDateToString(date_s);
	
	
	if (date_value != '' && date_value != null && date_value != undefined) 
	{
		if (date_format == 'MM/DD/YYYY') {
		
		
			var date_split = d_string.split('/');
			//nlapiLogExecution('DEBUG', 'date_split', 'date_split =' + date_split);
			
			var mm = date_split[0];
			var dd = date_split[1];
			var yy = date_split[2];
			if (mm == '1' || mm == '2' || mm == '3' || mm == '4' || mm == '5' || mm == '6' || mm == '7' || mm == '8' || mm == '9') {
			
				mm = '0' + mm;
				
			}
			if (dd == '1' || dd == '2' || dd == '3' || dd == '4' || dd == '5' || dd == '6' || dd == '7' || dd == '8' || dd == '9') {
			
				dd = '0' + dd;
				
			}
			var final_date = dd + '' + mm + '' + yy
			return final_date;
		}
		
		else 
		
			if (date_format == 'DD/MM/YYYY') {
			
				var date_split = d_string.split('/');
			
				var dd = date_split[0];
				var mm = date_split[1];
				var yy = date_split[2];
				if (mm == '1' || mm == '2' || mm == '3' || mm == '4' || mm == '5' || mm == '6' || mm == '7' || mm == '8' || mm == '9') {
				
					mm = '0' + mm;
					
				}
				
				if (dd == '1' || dd == '2' || dd == '3' || dd == '4' || dd == '5' || dd == '6' || dd == '7' || dd == '8' || dd == '9') {
				
					dd = '0' + dd;
					
				}
				var final_date = dd + '' + mm + '' + yy
				return final_date;
				
			}
			
			else 
			
				if (date_format == 'DD-Mon-YYYY') {
					var date_split = d_string.split('-');
					//nlapiLogExecution('DEBUG', 'date_split', 'date_split =' + date_split);
					
					var dd = date_split[0];
					var mm = date_split[1];
					mm = SearchNumber(mm)
					nlapiLogExecution('DEBUG', 'date_split', 'mm !!!!!!!!!!!!!!!!!1=' + mm);
					var yy = date_split[2];
					/*
		 if ( mm == '1'|| mm == '2'|| mm == '3'|| mm == '4'|| mm == '5'|| mm == '6'|| mm == '7'|| mm == '8'|| mm == '9' )
		 {
		 
		 mm =  '0' + mm ;
		 }
		 */
					if (dd == '1' || dd == '2' || dd == '3' || dd == '4' || dd == '5' || dd == '6' || dd == '7' || dd == '8' || dd == '9') {
					
						dd = '0' + dd;
						
					}
					var final_date = dd + '' + mm + '' + yy
					return final_date;
					
				}
				
				else 
				
					if (date_format == 'DD.MM.YYYY') {
					
						var date_split = d_string.split('.');
						//nlapiLogExecution('DEBUG', 'date_split', 'date_split =' + date_split);
						
						var dd = date_split[0];
						var mm = date_split[1];
						var yy = date_split[2];
						if (mm == '1' || mm == '2' || mm == '3' || mm == '4' || mm == '5' || mm == '6' || mm == '7' || mm == '8' || mm == '9') {
						
							mm = '0' + mm;
							
						}
						
						if (dd == '1' || dd == '2' || dd == '3' || dd == '4' || dd == '5' || dd == '6' || dd == '7' || dd == '8' || dd == '9') {
						
							dd = '0' + dd;
							
						}
						var final_date = dd + '' + mm + '' + yy
						return final_date;
						
					}
					
					else 
					
						if (date_format == 'DD-MONTH-YYYY') {
						
							var date_split = d_string.split('-');
							//nlapiLogExecution('DEBUG', 'date_split', 'date_split =' + date_split);
							
							var dd = date_split[0];
							var mm = date_split[1];
							mm = SearchNumber(mm)
							nlapiLogExecution('DEBUG', 'date_split', 'mm =' + mm);
							var yy = date_split[2];
							/*
		 if ( mm == '1'|| mm == '2'|| mm == '3'|| mm == '4'|| mm == '5'|| mm == '6'|| mm == '7'|| mm == '8'|| mm == '9' )
		 {
		 
		 mm =  '0' + mm ;
		 }
		 */
							if (dd == '1' || dd == '2' || dd == '3' || dd == '4' || dd == '5' || dd == '6' || dd == '7' || dd == '8' || dd == '9') {
							
								dd = '0' + dd;
								
							}
							var final_date = dd + '' + mm + '' + yy
							return final_date;
							
						}
						
						else 
						
							if (date_format == 'YYYY/MM/DD') {
							
								var date_split = d_string.split('/');
								//nlapiLogExecution('DEBUG', 'date_split', 'date_split =' + date_split);
								
								var yy = date_split[0];
								var mm = date_split[1];
								var dd = date_split[2];
								if (mm == '1' || mm == '2' || mm == '3' || mm == '4' || mm == '5' || mm == '6' || mm == '7' || mm == '8' || mm == '9') {
								
									mm = '0' + mm;
									
								}
								if (dd == '1' || dd == '2' || dd == '3' || dd == '4' || dd == '5' || dd == '6' || dd == '7' || dd == '8' || dd == '9') {
								
									dd = '0' + dd;
									
								}
								var final_date = dd + '' + mm + '' + yy
								return final_date;
								
								
							}
							
							else 
							
								if (date_format == 'YYYY-MM-DD') {
									var date_split = d_string.split('-');
									//nlapiLogExecution('DEBUG', 'date_split', 'date_split =' + date_split);
									
									var yy = date_split[0];
									var mm = date_split[1];
									var dd = date_split[2];
									if (mm == '1' || mm == '2' || mm == '3' || mm == '4' || mm == '5' || mm == '6' || mm == '7' || mm == '8' || mm == '9') {
									
										mm = '0' + mm;
										
									}
									
									if (dd == '1' || dd == '2' || dd == '3' || dd == '4' || dd == '5' || dd == '6' || dd == '7' || dd == '8' || dd == '9') {
									
										dd = '0' + dd;
										
									}
									var final_date = dd + '' + mm + '' + yy
									return final_date;
									
									
								}
								
								else 
								
									if (date_format == 'DD MONTH, YYYY') {
										var date_split = d_string.split(',');
										//nlapiLogExecution('DEBUG', 'date_split', 'date_split =' + date_split);
										
										var ddmm = date_split[0];
										var yy = date_split[1];
										
										var dd_mm = ddmm.split(' ');
										var dd = dd_mm[0];
										var mm = dd_mm[1];
										mm = SearchNumber(mm)
										if (dd == '1' || dd == '2' || dd == '3' || dd == '4' || dd == '5' || dd == '6' || dd == '7' || dd == '8' || dd == '9') {
										
											dd = '0' + dd;
											
										}
										
										var final_date = dd + '' + mm + '' + yy
										return final_date;
										
										
									}
									else {
										return null;
										
									}
	}
	else
	{
		return '';
	}
}

function SearchNumber(monthvalue){


    var x = '';
    switch (monthvalue) {
        case 'Jan':
            x = "1";
            break;
        case 'Feb':
            x = "2";
            break;
        case 'Mar':
            x = "3";
            break;
        case 'Apr':
            x = "4";
            break;
        case 'May':
            x = "5";
            break;
        case 'Jun':
            x = "6";
            break;
        case 'Jul':
            x = "7";
            break;
        case 'Aug':
            x = "8";
            break;
        case 'Sep':
            x = "9";
            break;
        case 'Oct':
            x = "10";
            break;
        case 'Nov':
            x = "11";
            break;
        case 'Dec':
            x = "12";
            break;
    }
    return x;
}
function log_validation(value)
{	
	if(value != null && value != '' && value != 'undefined')
	{
		value = value;
	}
	else
	{
		value = '';
	}
	return value;
}
function get_folderID()
{
			var folder_filters = new Array();
			folder_filters.push(new nlobjSearchFilter('name', null, 'is', 'ETDS_Return'));
	
			var folder_column = new Array();
			folder_column.push( new nlobjSearchColumn('internalid'));    
	
			var folder_results = nlapiSearchRecord('folder', null, folder_filters, folder_column);
			if (folder_results != null && folder_results != '' && folder_results != undefined) 
			{
				var folderId = folder_results[0].getId();
				
			}
			else
			{
				var o_folder_obj = nlapiCreateRecord('folder')
				o_folder_obj.setFieldValue('name','ETDS_Return')
				var folderId = nlapiSubmitRecord(o_folder_obj)
				
			}
			return folderId;
		
	
}
}
// END FUNCTION =====================================================
