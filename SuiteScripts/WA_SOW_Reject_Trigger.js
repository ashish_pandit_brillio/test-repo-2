/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       29 Mar 2018     deepak.srinivas
 *
 */

/**
 * @returns {Void} Any or no return value
 */
function workflowAction() {
try{
	nlapiLogExecution('AUDIT', 'SOW ID: ', nlapiGetRecordId());
	var dataRow = [];
	var a_loadRec = nlapiLoadRecord(nlapiGetRecordType(),nlapiGetRecordId());
	var s_status = a_loadRec.getFieldText('custbody_invoicestatus')
	if(s_status == 'Rejected'){
		dataRow = ({
			'SOW_ID': a_loadRec.getFieldValue('tranid'),
			'Opp_ID': a_loadRec.getFieldValue('custbody_opp_id_sfdc'),
			'Status': 'Rejected'
		});
		
	}
	nlapiLogExecution('DEBUG','Response',JSON.stringify(dataRow));
	
}
catch(e){
	
	nlapiLogExecution('DEBUG','Rejection Trigger Error',e);
}
}
