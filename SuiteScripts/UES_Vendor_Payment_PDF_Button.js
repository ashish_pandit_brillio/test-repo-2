// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
/*
   	Script Name: UES_Vendor_Payment_PDF_Button.js
	Author: Jayesh V Dinde
	Company: Aashna Cloudtech PVT LTD
	Date: 9 May 2016
	Description: This script is used to create button named PRINT at view mode which would be used to design pdf layout.


	Script Modification Log:

	-- Date --			-- Modified By --				--Requested By--				-- Description --



Below is a summary of the process controls enforced by this script file.  The control logic is described
more fully, below, in the appropriate function headers and code blocks.


     BEFORE LOAD
		- before_load_display_button(type)


     BEFORE SUBMIT
		- beforeSubmitRecord(type)


     AFTER SUBMIT
		- afterSubmitRecord(type)



     SUB-FUNCTIONS
		- The following sub-functions are called by the above core functions in order to maintain code
            modularization:

               - NOT USED

*/
}
// END SCRIPT DESCRIPTION BLOCK  ====================================

function before_load_display_button(type)
{
	if(type == 'view')
	{
		var user_id = nlapiGetUser();
		if (user_id != 9185 && user_id != 5764 && user_id != 39108) 
		{
			return;
		}
				
		var rcrd_id = nlapiGetRecordId();
		var recrd_type = nlapiGetRecordType();
		form.setScript('customscript_vendor_payment_invoke_sut');
		form.addButton('custpage_print_vendor_pay','Print','Print_Vendor_Payment(\'  ' + rcrd_id + '  \',\'  ' + recrd_type + '  \')');
	}
	
	return true;
}
