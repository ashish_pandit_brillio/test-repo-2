/**
 * CSV Download For Employee TIme Sheet
 * 
 * Version Date Author Remarks 1.00
 * 
 * 12 Aug 2016 Deepak MS
 * 
 */
var days = ['sunday','monday','tuesday','wednesday','thursday','friday','saturday'];
function suitelet(request, response) {
	try {
	
	
	
	var projectID = request.getParameter('custpage_project');
	
	
	
	var projectName = searchProjectObj(projectID);
	var st_date = request.getParameter('custpage_startdate');
	var mode = request.getParameter('mode')	
					
				
	var IMG_currentDate = nlapiDateToString(new Date());
	var resource_List  = searchProjects(projectID,st_date);
	
	if (mode == 'CSV') {
		var csvText = generateTableCsv(resource_List,st_date,projectName);
		var fileName = projectName +' '+st_date
		        + '.csv';
		var file = nlapiCreateFile(fileName, 'CSV', csvText);
		response.setContentType('CSV', fileName);
		response.write(file.getValue());	
		
	}
	}
	
catch(e){
nlapiLogExecution('DEBUG','Process Error',e);
}	
}

function generateTableCsv(res_list,d_stDate,projectVal){
	try{
var html = "";
		
		
		html += "Project,";
		html += "Employee,";
		html += "Task,";
		html += "Service Item,";
		html += "Days Of Week,";
		html += "Start Date,";
		html += "Duration,";
		html += "Approval Status,";
		html += "\r\n";

		// table header
		for(var j=0;j<res_list.length;j++){
			var projectData = res_list[j];
			
			var employee = nlapiLookupField('employee',projectData.Resource,['entityid']);
			
			for(var index=0;index<=6;index++){
			var day = days[index];
			
				
					html += projectVal;
					html += ",";
					html += employee.entityid;
					html += ",";
					html += "Project Activities (Project Task)";
					html += ",";
					html += "ST";
					html += ",";
					html += day;
					html += ",";
					html += d_stDate;
					html += ",";
					html += "";
					html += ",";
					html += "";
					html += "\r\n";
			}
		}
		/*for(var v=0;v<timeEntryList_1.length;v++){
			var employeeData = timeEntryList_1[v];
			var Amt = employeeData.Bill_rate * employeeData.TotalHrs;
			
			
			html += employeeData.Project;
			html += ",";
			html += employeeData.Employee;
			html += ",";
			html += employeeData.PracticeR;
			html += ",";
			html += employeeData.VericalR;
			html += ",";
			html += employeeData.Bill_rate;
			html += ",";
			html += employeeData.TotalHrs;
			html += ",";
			html += Amt;
			html += "\r\n";
		}*/
		
		return html;
		
	}
	catch(e){
		nlapiLogExecution('DEBUG','Process Error',e);	
	}
	
}
function searchProjectObj(proID){
	try{
		var project_Concat;
		var filter = Array();
		filter.push(new nlobjSearchFilter('internalid', null, 'is', proID));
		
		var cols = Array();
		cols.push(new nlobjSearchColumn('entityid'));
		cols.push(new nlobjSearchColumn('companyname','customer'));
		cols.push(new nlobjSearchColumn('companyname'));
		
		var searchRes = nlapiSearchRecord('job',null,filter,cols);
		
		if(searchRes){
			var projectID = searchRes[0].getValue('entityid');
			var customer_Name = searchRes[0].getValue('companyname','customer');
			var projectName = searchRes[0].getValue('companyname');
			
			  //project_Concat = projectID+' '+customer_Name+' '+':'+' '+projectName;
			    project_Concat = projectID+' '+''+''+projectName;
		}
		return project_Concat;
	}
	catch(e){
		nlapiLogExecution('debug','Project Search Error',e);
	}
}

function searchProjects(proj_ID,d_StartDate){
try{
var d_st_date = nlapiStringToDate(d_StartDate);
var filters = Array();
filters.push(new nlobjSearchFilter('internalid', null, 'is', proj_ID));
filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
filters.push(new nlobjSearchFilter('startdate','resourceallocation', 'onorbefore',d_st_date));
filters.push(new nlobjSearchFilter('enddate','resourceallocation', 'onorafter',d_st_date));

var cols = Array();
cols.push(new nlobjSearchColumn('resource','resourceallocation')); //parent

var projectSearchObj = nlapiSearchRecord('job',null,filters,cols);
var dataRows =[];
var resourceList = {};
if(projectSearchObj){
for(var i=0;i<projectSearchObj.length;i++){
resourceList = {
Resource: projectSearchObj[i].getValue('resource','resourceallocation')
};
dataRows.push(resourceList);
}
}

return dataRows;
}
catch(e){
nlapiLogExecution('DEBUG',' Project Search Error',e);
}

}


