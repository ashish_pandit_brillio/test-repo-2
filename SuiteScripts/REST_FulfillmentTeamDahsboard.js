// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
    /*
       	Script Name : REST_FulfillmentTeamDashboard.js
    	Author      : Sai
    	Date        : 06 MARCH 2019
        Description : RESTlet for Fulfillment team dashboard 


    	Script Modification Log:

    	-- Date --			-- Modified By --				--Requested By--				-- Description --
     


    Below is a summary of the process controls enforced by this script file.  The control logic is described
    more fully, below, in the appropriate function headers and code blocks.


         SCHEDULED FUNCTION
    		- scheduledFunction(type)


         SUB-FUNCTIONS
    		- The following sub-functions are called by the above core functions in order to maintain code
                modularization:

                   - NOT USED

    */
}
//============================================================================================================

/*Suitlet Main function*/
function REST_CreateDashboardData(dataIn) {
    try {
        nlapiLogExecution('Debug', 'dataIn', dataIn);
        var i_userEmail = dataIn.user;
        var user = getUserUsingEmailId(i_userEmail);
        var values = {};
        //nlapiLogExecution('Debug','Script Parameter user ',user);
        var spocSearch = GetPracticeSPOC(user);
        var region = GetRegion(user);
        var deliveryAnchore = GetDeliveryAnchore(user);
        //nlapiLogExecution('Debug','region ',region);
        var customrecord_fuel_adminSearch = nlapiSearchRecord("customrecord_fuel_leadership", null,
            [
                ["custrecord_fuel_leadership_employee", "anyof", user],
                "AND",
                ['isinactive', 'is', 'F']
            ],
            [
                new nlobjSearchColumn("custrecord_fuel_leadership_employee")
            ]
        );
        if (customrecord_fuel_adminSearch) {
            values.dashboardData = getFrfDashboardData(null, null, true, null);
            //nlapiLogExecution('Debug','Script Parameter user Admin'); Get all admin data
        } else if (spocSearch.length > 0) {
            values.dashboardData = getFrfDashboardData(spocSearch, null, null, null);
        } else if (region) {
            values.dashboardData = getFrfDashboardData(null, null, null, region);
        } else if (deliveryAnchore) {
            values.dashboardData = getFrfDashboardData(null, null, null, null, deliveryAnchore);
        } else {
            values.dashboardData = getFrfDashboardData(null, user, null, null);
        }
        return values;
    } catch (error) {
        nlapiLogExecution('Debug', 'Error ', error);
        return {
            "code": "error",
            "message": error
        }
    }
}
var onHold = GetOnholdStatusOfAllRRF(); // Nihal
var reduceOnHoldID = meargeDate(onHold); // Nihal
/****Function to get FRF details ****/
function getFrfDashboardData(practice, user, isAdmin, region, deliveryAnchore) {
    try {
        //nlapiLogExecution('Debug','In Function getFrfDetails');
        var filters = new Array();
        var columns = new Array();
        if (isAdmin == true) {
            filters = [
                ["custrecord_frf_details_status", "is", "F"],
                "AND",
                ["isinactive", "is", "F"],
                "AND",
                ["custrecord_frf_details_opp_id.custrecord_stage_sfdc", "noneof", "9", "8"],
				"AND",
				["custrecord_frf_details_status_flag","noneof","2","4","5"], 
			   "AND", 
			   ["custrecord_frf_details_status","is","F"]
                /*,
                					  "AND",
                					  ["custrecord_frf_details_end_date","onorafter","today"],
                					  "AND",
                					  ["custrecord_frf_details_status_flag","noneof",2]*/
            ]
        } else if (practice) {
            filters = [
                ["custrecord_frf_details_res_practice", "anyof", practice],
                "AND",
                ["custrecord_frf_details_status", "is", "F"],
                "AND",
                ["isinactive", "is", "F"],
                "AND",
                ["custrecord_frf_details_opp_id.custrecord_stage_sfdc", "noneof", "9", "8"],
				"AND",
				["custrecord_frf_details_status_flag","noneof","2","4","5"], 
			   "AND", 
			   ["custrecord_frf_details_status","is","F"]
                /*,
                					  "AND",
                					  ["custrecord_frf_details_end_date","onorafter","today"],
                					  "AND",
                					  ["custrecord_frf_details_status_flag","noneof",2]*/
            ]
        } else if (region) {
            filters = [
                ["custrecord_frf_details_account.custentity_region", "anyof", region],
                "AND",
                ["custrecord_frf_details_status", "is", "F"],
                "AND",
                ["isinactive", "is", "F"],
                "AND",
                ["custrecord_frf_details_opp_id.custrecord_stage_sfdc", "noneof", "9", "8"],
				"AND",
				["custrecord_frf_details_status_flag","noneof","2","4","5"], 
			   "AND", 
			   ["custrecord_frf_details_status","is","F"]
                /*,
                					  "AND",
                					  ["custrecord_frf_details_end_date","onorafter","today"],
                					  "AND",
                					  ["custrecord_frf_details_status_flag","noneof",2]*/
            ]
        } else if (deliveryAnchore) {
            filters = [
                ["custrecord_frf_details_account", "anyof", deliveryAnchore],
                "AND",
                ["custrecord_frf_details_status", "is", "F"],
                "AND",
                ["isinactive", "is", "F"],
                "AND",
                ["custrecord_frf_details_opp_id.custrecord_stage_sfdc", "noneof", "9", "8"],
				"AND",
				["custrecord_frf_details_status_flag","noneof","2","4","5"], 
			   "AND", 
			   ["custrecord_frf_details_status","is","F"]
                /*,
                					  "AND",
                					  ["custrecord_frf_details_end_date","onorafter","today"],
                					  "AND",
                					  ["custrecord_frf_details_status_flag","noneof",2]*/
            ]
        } else if (user) {
            filters = [
                [
                    [
                        ["custrecord_frf_details_project.custentity_projectmanager", "anyof", user],
                        "OR",
                        ["custrecord_frf_details_project.custentity_deliverymanager", "anyof", user],
                        "OR",
                        ["custrecord_frf_details_project.custentity_clientpartner", "anyof", user]
                    ],
                    "AND",
                    ["custrecord_frf_details_status", "is", "F"],
                    "AND",
                    ["isinactive", "is", "F"],
					"AND",
				    ["custrecord_frf_details_status_flag","noneof","2","4","5"] 
			   
                ],
                "OR",
                [
                    [
                        ["custrecord_frf_details_opp_id.custrecord_sfdc_opp_pm", "anyof", user],
                        "OR",
                        ["custrecord_frf_details_opp_id.custrecord_sfdc_opportunity_dm", "anyof", user],
                        "OR",
                        ["custrecord_frf_details_opp_id.custrecord_sfdc_client_partner", "anyof", user]
                    ],
                    "AND",
                    ["custrecord_frf_details_status", "is", "F"],
                    "AND",
                    ["isinactive", "is", "F"],
                    "AND",
                    ["custrecord_frf_details_opp_id.custrecord_stage_sfdc", "noneof", "9", "8"],
					"AND",
					["custrecord_frf_details_status_flag","noneof","2","4","5"]
				   
                ]
                /*,
                					  "AND",
                					  ["custrecord_frf_details_end_date","onorafter","today"],
                					  "AND",
                					  ["custrecord_frf_details_status_flag","noneof",2]*/
            ]
        }
        nlapiLogExecution('Debug', 'Filters = ', filters);
        columns = [
            new nlobjSearchColumn("custrecord_frf_details_created_date").setSort(true),
            new nlobjSearchColumn("custrecord_frf_details_opp_id"),
            new nlobjSearchColumn("custrecord_frf_details_res_location"),
            new nlobjSearchColumn("custrecord_frf_details_res_practice"),
            new nlobjSearchColumn("custrecord_frf_details_emp_level"),
            new nlobjSearchColumn("custrecord_frf_details_external_hire"),
            new nlobjSearchColumn("custrecord_frf_details_role"),
            new nlobjSearchColumn("custrecord_frf_details_bill_rate"),
            new nlobjSearchColumn("custrecord_frf_details_critical_role"),
            new nlobjSearchColumn("custrecord_frf_details_skill_family"),
            new nlobjSearchColumn("custrecord_frf_details_allocated_emp"),
            new nlobjSearchColumn("custrecord_frf_details_allocated_date"),
            new nlobjSearchColumn("custrecord_frf_details_start_date"),
            new nlobjSearchColumn("custrecord_frf_details_end_date"),
            new nlobjSearchColumn("custrecord_frf_details_selected_emp"),
            new nlobjSearchColumn("custrecord_frf_details_taleo_emp_select"),
            new nlobjSearchColumn("custrecord_frf_details_internal_fulfill"),
            new nlobjSearchColumn("custrecord_frf_details_primary_skills"),
            new nlobjSearchColumn("custrecord_frf_details_frf_number"),
            new nlobjSearchColumn("custrecord_frf_details_project"),
            new nlobjSearchColumn("custrecord_frf_details_suggestion"),
            new nlobjSearchColumn("custrecord_frf_type"),
            new nlobjSearchColumn("custrecord_frf_emp_notice_rotation"),
            new nlobjSearchColumn("custrecord_frf_details_billiable"),
            new nlobjSearchColumn("custrecord_frf_details_source"),
            new nlobjSearchColumn("custrecord_frf_details_secondary_skills"),
            new nlobjSearchColumn("custrecord_frf_details_allocation"),
            new nlobjSearchColumn("custrecord_frf_details_dollar_loss"),
            //new nlobjSearchColumn("custrecord_frf_details_suggest_pref_mat"), 
            new nlobjSearchColumn("custrecord_frf_details_special_req"),
            new nlobjSearchColumn("custrecord_frf_details_personal_email"),
            new nlobjSearchColumn("custrecord_frf_details_backup_require"),
            new nlobjSearchColumn("custrecord_frf_details_job_title"),
            new nlobjSearchColumn("custrecord_frf_details_edu_lvl"),
            new nlobjSearchColumn("custrecord_frf_details_edu_program"),
            new nlobjSearchColumn("custrecord_frf_details_region"),
            new nlobjSearchColumn("custrecord_frf_details_status_flag"),
            new nlobjSearchColumn("custrecord_frf_details_emp_status"),
            new nlobjSearchColumn("custrecord_frf_details_first_interviewer"),
            new nlobjSearchColumn("custrecord_frf_details_sec_interviewer"),
            new nlobjSearchColumn("custrecord_frf_details_cust_interview"),
            new nlobjSearchColumn("custrecord_frf_details_cust_inter_email"),
            new nlobjSearchColumn("custrecord_frf_details_ta_manager"),
            new nlobjSearchColumn("custrecord_frf_details_req_type"),
            new nlobjSearchColumn("custrecord_frf_details_created_by"),
            new nlobjSearchColumn("custrecord_frf_details_parent"),
            new nlobjSearchColumn("custrecord_frf_details_account"),
            new nlobjSearchColumn("custrecord_frf_details_reason_external"),
            new nlobjSearchColumn("custrecord_frf_details_availabledate"),
            new nlobjSearchColumn("custrecord_frf_details_rrf_number"), // Added By Nihal
            new nlobjSearchColumn("custrecord_taleo_external_selected_can", "custrecord_frf_details_rrf_number", null), // Added By Nihal
            new nlobjSearchColumn("custrecord_opportunity_name_sfdc", "CUSTRECORD_FRF_DETAILS_OPP_ID", null),
            new nlobjSearchColumn("custrecord_customer_sfdc", "CUSTRECORD_FRF_DETAILS_OPP_ID", null),
            new nlobjSearchColumn("custrecord_customer_internal_id_sfdc", "CUSTRECORD_FRF_DETAILS_OPP_ID", null),
            new nlobjSearchColumn("custrecord_projection_status_sfdc", "CUSTRECORD_FRF_DETAILS_OPP_ID", null),
            new nlobjSearchColumn("custrecord_taleo_ext_hire_status", "custrecord_frf_details_rrf_number", null),
            new nlobjSearchColumn("custentity_region", "CUSTRECORD_FRF_DETAILS_ACCOUNT", null),
            new nlobjSearchColumn("custentity_region", "CUSTRECORD_FRF_DETAILS_PROJECT", null),
            new nlobjSearchColumn("jobtype", "CUSTRECORD_FRF_DETAILS_PROJECT", null),
            new nlobjSearchColumn("created"), //custrecord_frf_details_ns_rrf_number custrecord_stage_sfdc
            new nlobjSearchColumn("companyname", "CUSTRECORD_FRF_DETAILS_ACCOUNT", null),
            new nlobjSearchColumn("custrecord_frf_details_ns_rrf_number"), //Added By Deepak 20-03-20
            new nlobjSearchColumn("custrecord_opportunity_id_sfdc", "CUSTRECORD_FRF_DETAILS_OPP_ID", null),
            new nlobjSearchColumn("custrecord_stage_sfdc", "CUSTRECORD_FRF_DETAILS_OPP_ID", null),
            new nlobjSearchColumn("custrecord_frf_details_softlock_date"),
            new nlobjSearchColumn("custrecord_frf_details_desoftlock_date"), //prabhat gupta 07/07/2020
            new nlobjSearchColumn("custrecord_frf_details_closed_date"),
            new nlobjSearchColumn("custrecord_rrf_filled_date", "custrecord_frf_details_rrf_number", null),
            new nlobjSearchColumn("custrecord_frf_details_fitment_type"), //prabhat gupta 04/09/2020 NIS-1723
            new nlobjSearchColumn("custrecord_frf_details_level") //prabhat gupta 07/10/2020 NIS-1755
            //new nlobjSearchColumn("custrecord_frf_details_softlock_date")

        ]
        var customrecord_frf_detailsSearch = searchRecord("customrecord_frf_details", null, filters, columns, null);
        nlapiLogExecution('Debug', 'customrecord_frf_detailsSearch 245 ', customrecord_frf_detailsSearch.length);
        if (customrecord_frf_detailsSearch) {
            var dataArray = new Array();
            nlapiLogExecution('Debug', 'customrecord_frf_detailsSearch 249 ', customrecord_frf_detailsSearch.length);
            for (var i = 0; i < customrecord_frf_detailsSearch.length; i++) {
                var b_billRoll = customrecord_frf_detailsSearch[i].getValue("custrecord_frf_details_billiable");
                var RRF_Number = customrecord_frf_detailsSearch[i].getValue("custrecord_frf_details_rrf_number");

                if (b_billRoll == 'T') {
                    b_billRoll = true;
                } else {
                    b_billRoll = false;
                }

                var b_criticalRoll = customrecord_frf_detailsSearch[i].getValue("custrecord_frf_details_critical_role");
                if (b_criticalRoll == 'T') {
                    b_criticalRoll = true;
                } else {
                    b_criticalRoll = false;
                }

                var b_externalHire = customrecord_frf_detailsSearch[i].getValue("custrecord_frf_details_external_hire");
                if (b_externalHire == 'T') {
                    b_externalHire = true;
                } else {
                    b_externalHire = false;
                }
                var b_backupRequired = customrecord_frf_detailsSearch[i].getValue("custrecord_frf_details_backup_require");
                if (b_backupRequired == 'T') {
                    b_backupRequired = true;
                } else {
                    b_backupRequired = false;
                }
                var b_cust_interview = customrecord_frf_detailsSearch[i].getValue("custrecord_frf_details_cust_interview");
                if (b_cust_interview == 'T') {
                    b_cust_interview = true;
                } else {
                    b_cust_interview = false;
                }

                var s_account = customrecord_frf_detailsSearch[i].getText("custrecord_frf_details_account");
                if (!s_account) {
                    s_account = customrecord_frf_detailsSearch[i].getText("custrecord_customer_internal_id_sfdc", "CUSTRECORD_FRF_DETAILS_OPP_ID", null);
                }
                var s_project = customrecord_frf_detailsSearch[i].getText("custrecord_frf_details_project");
                if (!s_project) {
                    s_project = customrecord_frf_detailsSearch[i].getValue("custrecord_opportunity_name_sfdc", "CUSTRECORD_FRF_DETAILS_OPP_ID", null);
                }
                var i_account = customrecord_frf_detailsSearch[i].getValue("custrecord_frf_details_account");

                var i_project = customrecord_frf_detailsSearch[i].getValue("custrecord_frf_details_project");
                var s_projection_status = customrecord_frf_detailsSearch[i].getValue("custrecord_projection_status_sfdc", "CUSTRECORD_FRF_DETAILS_OPP_ID", null);
                var allocatedResource = customrecord_frf_detailsSearch[i].getValue("custrecord_frf_details_allocated_emp");
                var softlock = customrecord_frf_detailsSearch[i].getValue("custrecord_frf_details_selected_emp");

                if (s_projection_status == "Booked" && (!softlock)) {
                    totalFRF_Booked++;
                } else if (s_projection_status == "Most Likely" && (!softlock)) {
                    totalFRF_MostLikely++;
                } else if (s_projection_status == "stretched" && (!softlock)) {
                    totalFRF_Stretached++;
                }
                var data = {};
                if (customrecord_frf_detailsSearch[i].getValue("custrecord_opportunity_name_sfdc", "CUSTRECORD_FRF_DETAILS_OPP_ID", null)) {
                    data.project = {
                        "name": customrecord_frf_detailsSearch[i].getValue("custrecord_opportunity_name_sfdc", "CUSTRECORD_FRF_DETAILS_OPP_ID", null),
                        "id": customrecord_frf_detailsSearch[i].getValue("CUSTRECORD_FRF_DETAILS_OPP_ID")
                    };
                } else {
                    data.project = {
                        "name": customrecord_frf_detailsSearch[i].getText("custrecord_frf_details_project"),
                        "id": customrecord_frf_detailsSearch[i].getValue("custrecord_frf_details_project")
                    };
                }

                if (customrecord_frf_detailsSearch[i].getValue("custrecord_customer_internal_id_sfdc", "CUSTRECORD_FRF_DETAILS_OPP_ID", null)) {
                    data.account = {
                        "name": customrecord_frf_detailsSearch[i].getText("custrecord_customer_internal_id_sfdc", "CUSTRECORD_FRF_DETAILS_OPP_ID", null),
                        "id": customrecord_frf_detailsSearch[i].getValue("custrecord_customer_internal_id_sfdc", "CUSTRECORD_FRF_DETAILS_OPP_ID", null)
                    };
                } else {
                    data.account = {
                        "name": customrecord_frf_detailsSearch[i].getText("custrecord_frf_details_account"),
                        "id": customrecord_frf_detailsSearch[i].getValue("custrecord_frf_details_account")
                    };
                }

                /*
                if(customrecord_frf_detailsSearch[i].getValue("custrecord_frf_details_account"))
                {
                	nlapiLogExecution('Debug','Account ',customrecord_frf_detailsSearch[i].getValue("custrecord_frf_details_account"))
                	data.account = {"name":customrecord_frf_detailsSearch[i].getText("custrecord_frf_details_account"),"id":customrecord_frf_detailsSearch[i].getValue("custrecord_frf_details_account")};
                }
                else
                {
                	data.account = {"name":customrecord_frf_detailsSearch[i].getValue("custrecord_customer_sfdc","CUSTRECORD_FRF_DETAILS_OPP_ID",null)};
                }
                */
                data.internalid = {
                    "name": customrecord_frf_detailsSearch[i].getId()
                };
                data.frfnumber = {
                    "name": customrecord_frf_detailsSearch[i].getValue("custrecord_frf_details_frf_number")
                };
                data.rrfnumber = {
                    "name": customrecord_frf_detailsSearch[i].getValue("custrecord_frf_details_ns_rrf_number")
                };
                data.frfdatecreated = {
                    "name": customrecord_frf_detailsSearch[i].getValue("created")
                };
                data.role = {
                    "name": customrecord_frf_detailsSearch[i].getText("custrecord_frf_details_role"),
                    "id": customrecord_frf_detailsSearch[i].getValue("custrecord_frf_details_role")
                };
                data.practice = {
                    "name": customrecord_frf_detailsSearch[i].getText("custrecord_frf_details_res_practice"),
                    "id": customrecord_frf_detailsSearch[i].getValue("custrecord_frf_details_res_practice")
                };
                data.skillfamily = {
                    "name": customrecord_frf_detailsSearch[i].getText("custrecord_frf_details_skill_family"),
                    "id": customrecord_frf_detailsSearch[i].getValue("custrecord_frf_details_skill_family")
                };
                data.primaryskills = {
                    "name": customrecord_frf_detailsSearch[i].getText("custrecord_frf_details_primary_skills"),
                    "id": customrecord_frf_detailsSearch[i].getValue("custrecord_frf_details_primary_skills")
                };
                data.location = {
                    "name": customrecord_frf_detailsSearch[i].getText("custrecord_frf_details_res_location"),
                    "id": customrecord_frf_detailsSearch[i].getValue("custrecord_frf_details_res_location")
                };
                data.startDate = {
                    "name": customrecord_frf_detailsSearch[i].getValue("custrecord_frf_details_start_date")
                };

                data.endDate = {
                    "name": customrecord_frf_detailsSearch[i].getValue("custrecord_frf_details_end_date")
                }; //NIS-1265 prabhat gupta

                data.availabledate = {
                    "name": customrecord_frf_detailsSearch[i].getValue("custrecord_frf_details_availabledate")
                };
                data.billRate = {
                    "name": customrecord_frf_detailsSearch[i].getValue("custrecord_frf_details_bill_rate")
                };

                data.frfSoftLockDate = {
                    "name": customrecord_frf_detailsSearch[i].getValue("custrecord_frf_details_softlock_date")
                };

                data.frfDeSoftLockDate = {
                    "name": customrecord_frf_detailsSearch[i].getValue("custrecord_frf_details_desoftlock_date")
                }; // prabhat gupta 07/07/2020	


                data.frfClosedDate = {
                    "name": customrecord_frf_detailsSearch[i].getValue("custrecord_frf_details_closed_date")
                };
                data.rrfFilledDate = {
                    "name": customrecord_frf_detailsSearch[i].getValue("custrecord_rrf_filled_date", "custrecord_frf_details_rrf_number", null)
                };

                data.taleoStatus = {
                    "name": customrecord_frf_detailsSearch[i].getValue("custrecord_taleo_ext_hire_status", "custrecord_frf_details_rrf_number", null)
                };
                var startDateCalculate = customrecord_frf_detailsSearch[i].getValue("custrecord_frf_details_start_date");
                var d_todaysDate = new Date();
                var d_startDate = new Date(startDateCalculate);
                data.frfage = {
                    "name": datediff(d_startDate, d_todaysDate)
                };
                if (customrecord_frf_detailsSearch[i].getText("custrecord_frf_details_allocated_emp"))
                    data.softlockresource = {
                        "name": customrecord_frf_detailsSearch[i].getText("custrecord_frf_details_allocated_emp"),
                        "id": customrecord_frf_detailsSearch[i].getValue("custrecord_frf_details_allocated_emp")
                    };
                else
                    data.softlockresource = {
                        "name": customrecord_frf_detailsSearch[i].getText("custrecord_frf_details_selected_emp"),
                        "id": customrecord_frf_detailsSearch[i].getValue("custrecord_frf_details_selected_emp")
                    };
                var prjStatus = customrecord_frf_detailsSearch[i].getValue("custrecord_projection_status_sfdc", "CUSTRECORD_FRF_DETAILS_OPP_ID", null);
                data.OppID = customrecord_frf_detailsSearch[i].getValue("custrecord_opportunity_id_sfdc", "CUSTRECORD_FRF_DETAILS_OPP_ID", null);
                data.OpportunityStage = {
                    "name": customrecord_frf_detailsSearch[i].getText("custrecord_stage_sfdc", "CUSTRECORD_FRF_DETAILS_OPP_ID", null),
                    "id": customrecord_frf_detailsSearch[i].getValue("custrecord_stage_sfdc", "CUSTRECORD_FRF_DETAILS_OPP_ID", null)
                };


                //nlapiLogExecution('Debug','prjStatus ',prjStatus);
                var projection = '';
                if (prjStatus) {
                    //nlapiLogExecution('Debug','prjStatus &&&&',prjStatus);
                    projection = {
                        "name": customrecord_frf_detailsSearch[i].getText("custrecord_projection_status_sfdc", "CUSTRECORD_FRF_DETAILS_OPP_ID", null),
                        "id": customrecord_frf_detailsSearch[i].getValue("custrecord_projection_status_sfdc", "CUSTRECORD_FRF_DETAILS_OPP_ID", null)
                    };
                    //data.projectionstatus = {"name":customrecord_frf_detailsSearch[i].getText("custrecord_projection_status_sfdc","CUSTRECORD_FRF_DETAILS_OPP_ID",null),"id":customrecord_frf_detailsSearch[i].getValue("custrecord_projection_status_sfdc","CUSTRECORD_FRF_DETAILS_OPP_ID",null)};
                } else {

                    //data.projectionstatus = {"name":"Booked","id": "3"};
                    projection = {
                        "name": "Booked",
                        "id": "3"
                    };
                }
                data.projectionstatus = projection
                data.frfstatus = {
                    "name": customrecord_frf_detailsSearch[i].getText("custrecord_frf_details_status_flag")
                }; // corrected as per new id


                if (customrecord_frf_detailsSearch[i].getValue("custentity_region", "CUSTRECORD_FRF_DETAILS_ACCOUNT", null)) {
                    data.region = {
                        "name": customrecord_frf_detailsSearch[i].getText("custentity_region", "CUSTRECORD_FRF_DETAILS_ACCOUNT", null),
                        "id": customrecord_frf_detailsSearch[i].getValue("custentity_region", "CUSTRECORD_FRF_DETAILS_ACCOUNT", null)
                    };
                } else if (customrecord_frf_detailsSearch[i].getValue("custentity_region", "CUSTRECORD_FRF_DETAILS_PROJECT", null)) {
                    data.region = {
                        "name": customrecord_frf_detailsSearch[i].getText("custentity_region", "CUSTRECORD_FRF_DETAILS_PROJECT", null),
                        "id": customrecord_frf_detailsSearch[i].getValue("custentity_region", "CUSTRECORD_FRF_DETAILS_PROJECT", null)
                    };
                } else {
                    data.region = {
                        "name": "other",
                        "id": ""
                    };
                }
                data.allocationdate = {
                    "name": customrecord_frf_detailsSearch[i].getValue("custrecord_frf_details_allocated_date")
                };
                if (customrecord_frf_detailsSearch[i].getValue("custrecord_frf_details_external_hire") == 'T') {
                    /*
                    					data.externalhire =  {"name":true};
                    					var s_selectedEmployee = "";
                    					var taleoRecId = nlapiLookupField("customrecord_frf_details",customrecord_frf_detailsSearch[i].getId(),"custrecord_frf_details_rrf_number");
                    					//nlapiLogExecution("AUDIT","Taleo Rec : ",taleoRecId);
                    					if(taleoRecId)
                    						s_selectedEmployee = nlapiLookupField("customrecord_taleo_external_hire",taleoRecId,"custrecord_taleo_external_selected_can");
                    					data.selectedEmployee =  {"name":s_selectedEmployee};*/
                    // Adding Nihal 
                    data.externalhire = {
                        "name": true
                    };
                    var s_selectedEmployee = "";
                    s_selectedEmployee = customrecord_frf_detailsSearch[i].getValue("custrecord_taleo_external_selected_can", "custrecord_frf_details_rrf_number", null)
                    data.selectedEmployee = {
                        "name": s_selectedEmployee
                    };
                } else {
                    data.externalhire = {
                        "name": false
                    };
                    data.selectedEmployee = {
                        "name": ""
                    };
                }
                if (customrecord_frf_detailsSearch[i].getValue("custrecord_frf_details_selected_emp") ||
                    customrecord_frf_detailsSearch[i].getValue("custrecord_frf_details_taleo_emp_select")) {
                    data.allocationstatus = {
                        "name": customrecord_frf_detailsSearch[i].getText("custrecord_frf_details_status_flag")
                    };
                } else {
                    data.allocationstatus = {
                        "name": 'In Progress'
                    };
                }
                data.projectType = {
                    "name": customrecord_frf_detailsSearch[i].getText("jobtype", "CUSTRECORD_FRF_DETAILS_PROJECT", null)
                };
                data.requestType = {
                    "name": customrecord_frf_detailsSearch[i].getText("custrecord_frf_type")
                };

                //--------------------------------------------------------------------------------------------------------

                //prabhat gupta 07/09/2020 NIS-1723

                data.fitmentType = {
                    "name": customrecord_frf_detailsSearch[i].getText("custrecord_frf_details_fitment_type"),
                    "id": customrecord_frf_detailsSearch[i].getValue("custrecord_frf_details_fitment_type")
                };


                //---------------------------------------------------------------------------------------------------------

                //--------------------------------------------------------------------------------------------------------

                //prabhat gupta 07/10/2020 NIS-1755

                data.level = {
                    "name": customrecord_frf_detailsSearch[i].getText("custrecord_frf_details_level"),
                    "id": customrecord_frf_detailsSearch[i].getValue("custrecord_frf_details_level")
                };


                //---------------------------------------------------------------------------------------------------------

                if (RRF_Number) {
                    data.OnHoldDetails = reduceOnHoldID[RRF_Number] ? reduceOnHoldID[RRF_Number] : "";
                } else {
                    data.OnHoldDetails = "";
                }
                dataArray.push(data);
            }

            //	nlapiLogExecution('Debug','dataArray$$$$$ ',JSON.stringify(dataArray));
            return dataArray;
        }
    } catch (error) {
        nlapiLogExecution('Error', 'Exception ', error);
    }
}


function GetOnholdStatusOfAllRRF() {
    // prabhat gupta 27/08/2020  replacing nlapiSearchRecord with searchRecord which is custom function
    var customrecord_taleo_external_hireSearch = searchRecord("customrecord_taleo_external_hire", null,
        [
            [
                ["systemnotes.newvalue", "is", "On hold"], "OR", ["systemnotes.oldvalue", "is", "On hold"]
            ],
            "AND",
            ["isinactive", "is", "F"]
        ],
        [
            new nlobjSearchColumn("oldvalue", "systemNotes", null),
            new nlobjSearchColumn("newvalue", "systemNotes", null),
            new nlobjSearchColumn("date", "systemNotes", null)
        ]
    );
    var completeResultSet = customrecord_taleo_external_hireSearch;

    /*
	//prabhat gupta 27/08/2020  commenting this code because of production issue in fuel
		while(customrecord_taleo_external_hireSearch.length == 1000)
		{
			var lastId = customrecord_taleo_external_hireSearch[999].getId();
			filter.push(new nlobjSearchFilter('internalidnumber',null,'greaterthan',lastId));
			customrecord_taleo_external_hireSearch = nlapiSearchRecord("customrecord_frf_details",null,filter,column);
			completeResultSet = completeResultSet.concat(customrecord_taleo_external_hireSearch); 
		} 
		customrecord_taleo_external_hireSearch = completeResultSet;
		
*/
    var returnData = [];
    if (customrecord_taleo_external_hireSearch) {
        for (var t = 0; t < customrecord_taleo_external_hireSearch.length; t++) {
            returnData.push({
                "internalId": customrecord_taleo_external_hireSearch[t].getId(),
                "oldStatus": customrecord_taleo_external_hireSearch[t].getValue("oldvalue", "systemNotes", null),
                "newStatus": customrecord_taleo_external_hireSearch[t].getValue("newvalue", "systemNotes", null),
                "Date": customrecord_taleo_external_hireSearch[t].getValue("date", "systemNotes", null)
            });
        }
    }
    return returnData;

}

function meargeDate(all_data) {
    return groupResult = all_data.reduce(function(r, a) {
        r[a.internalId] = r[a.internalId] || [];
        r[a.internalId].push(a);
        return r;
    }, Object.create(null));
}

/*********************************************Function to get Search Results**********************************************/
function searchRecord(recordType, savedSearch, arrFilters, arrColumns,
    filterExpression) {

    try {
        var search = null;

        // if a saved search is provided, load it and add the filters and
        // columns
        if (isNotEmpty(savedSearch)) {
            search = nlapiLoadSearch(recordType, savedSearch);

            if (isArrayNotEmpty(arrFilters)) {
                search.addFilters(arrFilters);
            }

            if (isArrayNotEmpty(arrColumns)) {
                search.addColumns(arrColumns);
            }

            if (isArrayNotEmpty(filterExpression)) {
                search.setFilterExpression(filterExpression);
            }
        }
        // create a new search
        else {
            search = nlapiCreateSearch(recordType, arrFilters, arrColumns);
        }

        // run search
        var resultSet = search.runSearch();

        // iterate through the search and get all data 1000 at a time
        var searchResultCount = 0;
        var resultSlice = null;
        var searchResult = [];

        do {
            resultSlice = resultSet.getResults(searchResultCount,
                searchResultCount + 1000);

            if (resultSlice) {

                resultSlice.forEach(function(result) {

                    searchResult.push(result);
                    searchResultCount++;
                });
            }
        } while (isArrayNotEmpty(resultSlice) && resultSlice.length >= 1000);

        return searchResult;
    } catch (err) {
        nlapiLogExecution('ERROR', 'searchRecord', err);
        throw err;
    }
}

function isEmpty(value) {

    return value == null || value == "" || typeof(value) == undefined;
}

function isNotEmpty(value) {

    return !isEmpty(value);
}

function isArrayEmpty(argArray) {

    return !isArrayNotEmpty(argArray);
}

function isArrayNotEmpty(argArray) {

    return (isNotEmpty(argArray) && argArray.length > 0);
}


function datediff(first, second) {
    // Take the difference between the dates and divide by milliseconds per day.
    // Round to nearest whole number to deal with DST.
    return Math.round((second - first) / (1000 * 60 * 60 * 24));
}

function GetPracticeSPOC(user) {
    var practiceArray = new Array();
    //nlapiLogExecution('AUDIT', 'user', user);
    var spocSearch = nlapiSearchRecord("customrecord_practice_spoc", null,
        [
            ["custrecord_spoc", "anyof", user],
            "AND",
            ["isinactive", "is", "F"]
        ],
        [
            new nlobjSearchColumn("custrecord_practice")
        ]
    );
    if (spocSearch) {
        for (var i = 0; i < spocSearch.length; i++) {
            practiceArray.push(spocSearch[i].getValue('custrecord_practice'));
        }
    }
    return practiceArray;
}



function GetRegion(user) {
    try {
        //nlapiLogExecution('AUDIT', 'user', user);
        var regionSearch = nlapiSearchRecord("customrecord_region", null,
            [
                ["custrecord_region_head", "anyof", user],
                "AND",
                ["isinactive", "is", "F"]
            ],
            [
                new nlobjSearchColumn("name")
            ]
        );
        if (regionSearch) {
            var region = regionSearch[0].getId();
            return region;
        } else {
            return '';
        }

    } catch (e) {
        return e;
    }

}

function GetDeliveryAnchore(user) {
    var temp = [];
    var customrecord_fuel_delivery_anchorSearch = nlapiSearchRecord("customrecord_fuel_delivery_anchor", null,
        [
            ["custrecord_fuel_anchor_employee", "anyof", user]
        ],
        [
            new nlobjSearchColumn("custrecord_fuel_anchor_customer")
        ]
    );
    if (customrecord_fuel_delivery_anchorSearch) {
        for (var i = 0; i < customrecord_fuel_delivery_anchorSearch.length; i++) {
            temp.push(customrecord_fuel_delivery_anchorSearch[i].getValue('custrecord_fuel_anchor_customer'));
        }
        return temp;
    } else {
        return false;
    }
}