/**
 * @author Shweta
 */
// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
/*
   	Script Name : CLI_Journal_Entry_Auto_No_Validations.js
	Author      : Shweta Chopde
	Date        : 5 May 2014
    Description : Disable the fields on Page Load


	Script Modification Log:

	-- Date --			-- Modified By --				--Requested By--				-- Description --




	Below is a summary of the process controls enforced by this script file.  The control logic is described
	more fully, below, in the appropriate function headers and code blocks.

     PAGE INIT
		- pageInit(type)


     SAVE RECORD
		- saveRecord()


     VALIDATE FIELD
		- validateField(type, name, linenum)


     FIELD CHANGED
		- fieldChanged(type, name, linenum)


     POST SOURCING
		- postSourcing(type, name)


	LINE INIT
		- lineInit(type)


     VALIDATE LINE
		- validateLine()


     RECALC
		- reCalc()


     SUB-FUNCTIONS
		- The following sub-functions are called by the above core functions in order to maintain code
            modularization:





*/
}
// END SCRIPT DESCRIPTION BLOCK  ====================================



// BEGIN SCRIPT UPDATION BLOCK  ====================================
/*


*/
// END SCRIPT UPDATION BLOCK  ====================================




// BEGIN GLOBAL VARIABLE BLOCK  =====================================
{

	//  Initialize any Global Variables, in particular, debugging variables...


}
// END GLOBAL VARIABLE BLOCK  =======================================





// BEGIN PAGE INIT ==================================================

function pageInit_JE_validations(type)
{
   nlapiDisableField('custbody_refnumber',true)
}

// END PAGE INIT ====================================================





// BEGIN SAVE RECORD ================================================

function saveRecord()
{
    /*  On save record:

	- PURPOSE



	  FIELDS USED:

		--Field Name--			--ID--		--Line Item Name--


    */


    //  LOCAL VARIABLES



    //  SAVE RECORD CODE BODY


	return true;

}

// END SAVE RECORD ==================================================





// BEGIN VALIDATE FIELD =============================================

function validateField(type, name, linenum)
{

	/*  On validate field:

          - EXPLAIN THE PURPOSE OF THIS FUNCTION


          FIELDS USED:

          --Field Name--				--ID--


    */


	//  LOCAL VARIABLES



	//  VALIDATE FIELD CODE BODY


	return true;

}

// END VALIDATE FIELD ===============================================





// BEGIN FIELD CHANGED ==============================================

function fieldChanged_JE_validate(type, name, linenum)
{
    nlapiDisableField('custbody_refnumber',true)

}

// END FIELD CHANGED ================================================





// BEGIN POST SOURCING ==============================================

function postSourcing(type, name)
{

    /*  On post sourcing:

	- PURPOSE

          FIELDS USED:

		--Field Name--			--ID--		--Line Item Name--


    */

    //  LOCAL VARIABLES


    //  POST SOURCING CODE BODY



}

// END POST SOURCING ================================================



// BEGIN LINE INIT ==============================================

function lineInit(type)
{

    /*  On Line Init:

	- PURPOSE

          FIELDS USED:

		--Field Name--			--ID--		--Line Item Name--


    */

    //  LOCAL VARIABLES


    //  LINE INIT CODE BODY



}

// END LINE INIT ================================================


// BEGIN VALIDATE LINE ==============================================

function validateLine(type)
{

	/*  On validate line:

          - EXPLAIN THE PURPOSE OF THIS FUNCTION


          FIELDS USED:

          --Field Name--				--ID--


    */


	//  LOCAL VARIABLES


	//  VALIDATE LINE CODE BODY


	return true;

}

// END VALIDATE LINE ================================================




// BEGIN RECALC =====================================================

function recalc(type)
{

	/*  On recalc:

          - EXPLAIN THE PURPOSE OF THIS FUNCTION


          FIELDS USED:

          --Field Name--				--ID--


    */


	//  LOCAL VARIABLES


	//  RECALC CODE BODY


}

// END RECALC =======================================================





// BEGIN FUNCTION ===================================================

function approve_je(i_recordID)
{	
	if(_logValidation(i_recordID))
	{
		var s_record_type = nlapiGetRecordType();
		var o_recordOBJ = nlapiLoadRecord(s_record_type,i_recordID )
		
		if(_logValidation(o_recordOBJ))
		{
			var i_approval_status = o_recordOBJ.getFieldValue('custbodyapprovalstatusonjv')
			
			if(i_approval_status == 1)
			{
				o_recordOBJ.setFieldValue('custbodyapprovalstatusonjv',2)
				o_recordOBJ.setFieldValue('approved','T')
				
			}//Pending Approval
			
			var i_submitID = nlapiSubmitRecord(o_recordOBJ,true ,true)
			
		}//Record OBJ
		
	}//Record ID
	location.reload();	
}//Approve

function reject_je(i_recordID)
{
	if(_logValidation(i_recordID))
	{
		var s_record_type = nlapiGetRecordType();
		var o_recordOBJ = nlapiLoadRecord(s_record_type,i_recordID )
		
		if(_logValidation(o_recordOBJ))
		{
			var i_approval_status = o_recordOBJ.getFieldValue('custbodyapprovalstatusonjv')
			
			if(i_approval_status == 1)
			{
				o_recordOBJ.setFieldValue('custbodyapprovalstatusonjv',3)
				
			}//Pending Approval
			
			var i_submitID = nlapiSubmitRecord(o_recordOBJ,true ,true)
			
		}//Record OBJ
		
	}//Record ID
	location.reload();	
}//Reject

function _logValidation(value) 
{
 if(value != null && value.toString() != null && value != '' && value != undefined && value.toString() != undefined && value != 'undefined' && value.toString() != 'undefined'&& value.toString() != 'NaN' && value != NaN) 
 {
  return true;
 }
 else 
 { 
  return false;
 }
}
// END FUNCTION =====================================================
