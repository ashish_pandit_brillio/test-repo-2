/**
 * Generate a zero allocation report and send it to business OPS
 * 
 * Version    Date            Author           Remarks
 * 1.00       01 Jun 2015     Nitish Mishra
 *
 */

/**
 * @param {String}
 *            type Context Types: scheduled, ondemand, userinterface, aborted,
 *            skipped
 * @returns {Void}
 */
function scheduled(type) {
	try {
		generateAndSendZeroAllocationReport();
	} catch (err) {
		nlapiLogExecution('error', 'scheduled', err);
	}
}

function getAllocatedEmployeeList(date) {
	try {
		var allocated_employees = [];

		date = 'today';
		nlapiLogExecution('debug', 'date', date);

		// get all active allocation for the current date
		var search_result = searchRecord('resourceallocation', null, [
		        new nlobjSearchFilter('custentity_employee_inactive',
		                'employee', 'is', 'F'),
		        new nlobjSearchFilter('startdate', null, 'notafter', date),
		        new nlobjSearchFilter('enddate', null, 'notbefore', date) ],
		        [ new nlobjSearchColumn('resource', null, 'group') ]);

		nlapiLogExecution('debug', 'search_result', search_result.length);

		search_result.forEach(function(allocation) {
			// nlapiLogExecution('debug', 'testing', allocation.getValue(
			// 'resource', null, 'group'));
			allocated_employees.push(allocation.getValue('resource', null,
			        'group'));
		});

		nlapiLogExecution('debug', 'allocated resources',
		        allocated_employees.length);

		return allocated_employees;
	} catch (err) {
		nlapiLogExecution('error', 'getAllocatedEmployeeList', err);
		throw err;
	}
}

function generateAndSendZeroAllocationReport(date) {
	try {
		var zero_allocated_list = getZeroAllocationEmployee(date);
		// nlapiLogExecution('debug', 'zero_allocated_list', JSON
		// .stringify(zero_allocated_list));

		if (isArrayNotEmpty(zero_allocated_list)) {
			// nlapiLogExecution('debug', 'send emails');
		}
	} catch (err) {
		nlapiLogExecution('error', 'generateAndSendZeroAllocationReport', err);
		throw err;
	}
}

function getZeroAllocationEmployee(date) {
	try {
		var allocated_employees = getAllocatedEmployeeList(date);
		// nlapiLogExecution('debug', 'allocated resources', JSON
		// .stringify(allocated_employees));

		if (isArrayNotEmpty(allocated_employees)) {
			// pull the left out active employees
			var unallocated_employees_search = searchRecord('employee', null, [
			        new nlobjSearchFilter('custentity_employee_inactive', null,
			                'is', 'F'),
			        // new nlobjSearchFilter('custentity_lwd', null,
			        // 'notbefore',
			        // date),
			        new nlobjSearchFilter('custentity_implementationteam',
			                null, 'is', 'F'),
			              //  new nlobjSearchFilter('formulanumeric', null, 'between',
			    	        	//	50000,60000).setFormula('{internalid}'),
			        new nlobjSearchFilter('internalid', null, 'noneof',
			                allocated_employees) ], [
			        new nlobjSearchColumn('entityid'),
			        new nlobjSearchColumn('department'),
			        new nlobjSearchColumn('location'),
			        new nlobjSearchColumn('hiredate'),
			        new nlobjSearchColumn('subsidiary'),
			        new nlobjSearchColumn('title'),
			        new nlobjSearchColumn('custentity_persontype'),
			        new nlobjSearchColumn('custentity_employeetype') ]);

			 nlapiLogExecution('debug', 'unallocated_employees_search less than 40000',
			 unallocated_employees_search.length);

			var unallocated_employees = [];
			var employee_list = [];

			unallocated_employees_search
			        .forEach(function(employee) {
				        unallocated_employees
				                .push({
				                    entityid : employee.getValue('entityid'),
				                    internalid : employee.getId(),
				                    viewlink : '<a target="_blank" style="color:#255599;text-decoration:none;"'
				                            + ' href="/app/common/entity/employee.nl?id='
				                            + employee.getId() + '">View</a>',
				                    practice : employee.getText('department'),
				                    location : employee.getText('location'),
				                    jobtitle : employee.getValue('title'),
				                    hiredate : employee.getValue('hiredate'),
				                    subsidiary : employee.getText('subsidiary'),
				                    employeetype : employee
				                            .getText('custentity_persontype'),
				                    persontype : employee
				                            .getText('custentity_employeetype'),
				                    last_allocation : '',
				                    last_allocation_customer : '',
				                    last_allocation_customer_name : '',
				                    last_allocation_name : '',
				                    last_allocation_end_date : '',
				                    last_allocation_start_date : '',
				                    open_allocation : ''
				                });

				        employee_list.push(employee.getId());
			        });
			
		  /* unallocated_employees_search = searchRecord('employee', null, [
	        new nlobjSearchFilter('custentity_employee_inactive', null,
	                'is', 'F'),
	        // new nlobjSearchFilter('custentity_lwd', null,
	        // 'notbefore',
	        // date),
	        new nlobjSearchFilter('custentity_implementationteam',
	                null, 'is', 'F'),
	                new nlobjSearchFilter('formulanumeric', null, 'lessthanorequalto',
	    	        		50000).setFormula('{internalid}'),
	       
	        new nlobjSearchFilter('internalid', null, 'noneof',
	                allocated_employees) ], [
	        new nlobjSearchColumn('entityid'),
	        new nlobjSearchColumn('department'),
	        new nlobjSearchColumn('location'),
	        new nlobjSearchColumn('hiredate'),
	        new nlobjSearchColumn('subsidiary'),
	        new nlobjSearchColumn('title'),
	        new nlobjSearchColumn('custentity_persontype'),
	        new nlobjSearchColumn('custentity_employeetype') ]);
		   
		   nlapiLogExecution('debug', 'unallocated_employees_search greater than 40000',
					 unallocated_employees_search.length);
			unallocated_employees_search
	        .forEach(function(employee) {
		        unallocated_employees
		                .push({
		                    entityid : employee.getValue('entityid'),
		                    internalid : employee.getId(),
		                    viewlink : '<a target="_blank" style="color:#255599;text-decoration:none;"'
		                            + ' href="/app/common/entity/employee.nl?id='
		                            + employee.getId() + '">View</a>',
		                    practice : employee.getText('department'),
		                    location : employee.getText('location'),
		                    jobtitle : employee.getValue('title'),
		                    hiredate : employee.getValue('hiredate'),
		                    subsidiary : employee.getText('subsidiary'),
		                    employeetype : employee
		                            .getText('custentity_persontype'),
		                    persontype : employee
		                            .getText('custentity_employeetype'),
		                    last_allocation : '',
		                    last_allocation_customer : '',
		                    last_allocation_customer_name : '',
		                    last_allocation_name : '',
		                    last_allocation_end_date : '',
		                    last_allocation_start_date : '',
		                    open_allocation : ''
		                });

		        employee_list.push(employee.getId());
	        });
			*/
			
    			
			// get the last allocation of all the employee
			try {

				var allocationSearchResult = nlapiSearchRecord(
				        'resourceallocation', 1076, [ new nlobjSearchFilter(
				                'resource', null, 'anyof', employee_list) ]);

				if (allocationSearchResult) {

					for (var i = 0; i < unallocated_employees.length; i++) {

						for (var j = 0; j < allocationSearchResult.length; j++) {

							if (unallocated_employees[i].internalid == allocationSearchResult[j]
							        .getValue('resource')) {
								unallocated_employees[i].last_allocation_end_date = allocationSearchResult[j]
								        .getValue('enddate');
								unallocated_employees[i].last_allocation_start_date = allocationSearchResult[j]
								        .getValue('startdate');
								unallocated_employees[i].last_allocation = allocationSearchResult[j]
								        .getValue('company');
								unallocated_employees[i].last_allocation_name = allocationSearchResult[j]
								        .getText('company');
								unallocated_employees[i].last_allocation_customer = allocationSearchResult[j]
								        .getValue('customer');
								unallocated_employees[i].last_allocation_customer_name = allocationSearchResult[j]
								        .getText('customer');
								unallocated_employees[i].open_allocation = '<a target="_blank" style="color:#255599;'
								        + 'text-decoration:none;" href="/app/accounting/project/allocation.nl?id='
								        + allocationSearchResult[j].getId()
								        + '">Open</a>';

								break;
							}
						}
					}
				}
			} catch (ex) {
				nlapiLogExecution('error', 'allocationSearchResult', ex);
			}

			nlapiLogExecution('debug', 'final data', JSON
			        .stringify(unallocated_employees));
			return unallocated_employees;
		}
		return [];
	} catch (err) {
		nlapiLogExecution('error', 'getZeroAllocationEmployee', err);
		throw err;
	}
}

function suitelet(request, response) {
	try {
		var zero_allocated_list = getZeroAllocationEmployee(request
		        .getParameter('custpage_date'));

		var mode = request.getParameter('mode');

		if (mode != "export") {
			// creating a form to display the result
			var form = nlapiCreateForm('Unallocated Employees');
			var context = nlapiGetContext();

			var url = "<a href='"
			        + nlapiResolveURL('SUITELET', context.getScriptId(),
			                context.getDeploymentId())
			        + "&mode=export'> CSV Export </a>";
			form.addField("custpage_export_link", "inlinehtml", "")
			        .setDisplayType('inline').setDefaultValue(url);

			form.addTab('custpage_tab_1', 'Resource List');

			var list = form.addSubList('custpage_unallocated_emp_list',
			        'staticlist', 'List', 'custpage_tab_1');
			list.addField('viewlink', 'text', '').setDisplayType('inline');
			list.addField('entityid', 'text', 'Employee').setDisplayType(
			        'inline');
			list.addField('hiredate', 'date', 'Hire Date').setDisplayType(
			        'inline');
			list.addField('practice', 'text', 'Practice').setDisplayType(
			        'inline');
			list.addField('subsidiary', 'text', 'Subsidiary').setDisplayType(
			        'inline');
			list.addField('employeetype', 'text', 'Employee Type')
			        .setDisplayType('inline');
			// list.addField('persontype', 'text', 'Person
			// Type').setDisplayType(
			// 'inline');
			list.addField('last_allocation', 'select', 'Last Project', 'job')
			        .setDisplayType('inline');
			list.addField('last_allocation_customer', 'select',
			        'Last Customer', 'customer').setDisplayType('inline');
			list.addField('last_allocation_start_date', 'date', 'Start Date')
			        .setDisplayType('inline');
			list.addField('last_allocation_end_date', 'date', 'End Date')
			        .setDisplayType('inline');
			list.addField('open_allocation', 'text', 'Last Allocation')
			        .setDisplayType('inline');
			list.setLineItemValues(zero_allocated_list);
			response.writePage(form);
		} else {
			var csvText = "";
			csvText += "Employee,";
			csvText += "Hire Date,";
			csvText += "Practice,";
			csvText += "Subsidiary,";
			csvText += "Employee Type,";
			csvText += "Last Allocated Project,";
			csvText += "Last Allocated Customer,";
			csvText += "Last Allocation Start Date,";
			csvText += "Last Allocation End Date,\n";

			zero_allocated_list.forEach(function(emp) {
				csvText += emp.entityid + ",";
				csvText += emp.hiredate + ",";
				csvText += emp.practice + ",";
				csvText += emp.subsidiary + ",";
				csvText += emp.employeetype + ",";
				csvText += emp.last_allocation_name + ",";
				csvText += emp.last_allocation_customer_name + ",";
				csvText += emp.last_allocation_start_date + ",";
				csvText += emp.last_allocation_end_date + ",\n";
			});

			var fileName = 'Unallocated Resource - '
			        + nlapiDateToString(new Date(), 'date') + '.csv';
			var csvFile = nlapiCreateFile(fileName, 'CSV', csvText);
			response.setContentType('CSV', fileName);
			response.write(csvFile.getValue());
		}
	} catch (err) {
		nlapiLogExecution('error', 'suitelet', err);
		displayErrorForm(err);
	}
}

function exportToExcel() {
	try {

	} catch (err) {

	}
}