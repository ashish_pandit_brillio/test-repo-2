/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       01 Dec 2014     amol.sahijwani
 * Display custom message
 */

/**
 * @param {nlobjRequest} request Request object
 * @param {nlobjResponse} response Response object
 * @returns {Void} Any output is written via response object
 */
function suitelet(request, response){
	
	var showTillTime = Date.UTC(2018,1,9,17,0,0);
	
	var currentTime = new Date();
	
	var currentMilliseconds = currentTime.getTime(); // - currentTime.getTimezoneOffset() * 60 * 1000;
	
	var remainingMilliseconds = showTillTime - currentMilliseconds;
	
	var remainingSeconds = Math.round(remainingMilliseconds/1000); 
	
	var remainingMinutes = Math.round(remainingSeconds/60.0);

	var strHeading = 'Timesheet is Unavailable';
	
	var isAvailable = false;
	
	if(!(remainingMilliseconds > 0))
		{
			strHeading = 'Timesheet is available now';
			
			isAvailable = true;
		}
	
	var hours = Math.floor(remainingMinutes/60.0);
	
	var minutes = remainingMinutes - hours * 60;
	
	var form = nlapiCreateForm(strHeading, false);
	
	var myInlineHtml = form.addField('custpage_text', 'inlinehtml');
	//myInlineHtml.setDefaultValue('<span style="font-size:150%; color:red;">Due to testing of Month end closure, timesheets have been blocked. Please enter the timesheets after 4 hours!!</span>');
	if(!isAvailable)
		{
			myInlineHtml.setDefaultValue('<span style="font-size:150%; color:red;">Due to Month end closure, timesheets have been blocked. Please enter the timesheets after 4 hours!!</span>');	
		}
	else
		{
			myInlineHtml.setDefaultValue('<span style="font-size:150%; color:green;">Timesheets are available now!!</span>');
		}
	nlapiLogExecution('AUDIT', 'AccessedBy: ' + nlapiGetContext().getUser() + ' at ' + currentTime.toString(),hours + ' hours, ' + minutes + ' minutes');
	response.writePage(form);
}

function enableTimeSheet()
{
	
}