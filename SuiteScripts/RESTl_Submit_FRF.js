// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
/*
   	Script Name : REST_SubmitAllCallSch.js
	Author      : ASHISH PANDIT
	Date        : 22 APRIL 2018
    Description : RESTlet to call Scheduled Script for creating FRF Details Record 


	Script Modification Log:

	-- Date --			-- Modified By --				--Requested By--				-- Description --
 


Below is a summary of the process controls enforced by this script file.  The control logic is described
more fully, below, in the appropriate function headers and code blocks.


     SCHEDULED FUNCTION
		- scheduledFunction(type)


     SUB-FUNCTIONS
		- The following sub-functions are called by the above core functions in order to maintain code
            modularization:

               - NOT USED

*/
}
// END SCRIPT DESCRIPTION BLOCK  ====================================

function REST_callSchedule(dataIn)
{
	var response = new Response();
	try
	{
		if(_logValidation(dataIn)) 
		{
			var bodyJSON = dataIn;
            nlapiLogExecution('debug','bodyJSON ',JSON.stringify(bodyJSON));
			var projectId = bodyJSON.projectId;
			nlapiLogExecution("DEBUG", "projectId", projectId);
			var oppId = bodyJSON.oppId;
			nlapiLogExecution("DEBUG", "oppId", oppId);
			var accId = bodyJSON.accountId;
			nlapiLogExecution("DEBUG", "accId", accId);
			var i_user = bodyJSON.user;
			nlapiLogExecution("DEBUG", "i_user", i_user);
			if(projectId)
			{
				var jobSearch = nlapiSearchRecord("job",null,
				[
				   ["internalid","anyof",projectId]
				], 
				[
				   new nlobjSearchColumn("customer"), 
				   new nlobjSearchColumn("altname")
				]
				);
			}
			if(jobSearch)
			{
				nlapiLogExecution('Debug','jobSearch length ',jobSearch.length);
			}
			if(oppId)
			{
				var customrecord_sfdc_opportunity_recordSearch = nlapiSearchRecord("customrecord_sfdc_opportunity_record",null,
				[
					["internalid","anyof",oppId]
				], 
				[
				   new nlobjSearchColumn("scriptid").setSort(false), 
				   new nlobjSearchColumn("custrecord_opportunity_id_sfdc")
				]
				);
			}
			if(customrecord_sfdc_opportunity_recordSearch)
			{
				nlapiLogExecution('Debug','customrecord_sfdc_opportunity_recordSearch length ',customrecord_sfdc_opportunity_recordSearch.length);
			}
			if(accId)
			{
				var customerSearch = nlapiSearchRecord("customer",null,
				[
				   ["internalid","anyof",accId]
				], 
				[
				   new nlobjSearchColumn("entityid").setSort(false), 
				   new nlobjSearchColumn("altname") 
				]
				);
			}
			if(customerSearch)
			{
				nlapiLogExecution('Debug','customerSearch length ',customerSearch.length);
			}
			if((jobSearch)||(customrecord_sfdc_opportunity_recordSearch) && (customerSearch))
			{
				var params = [];
				params["custscript_account"] = accId;
				params["custscript_project_id_create_frf"] = projectId;
				params["custscript_opp_id"] = oppId;
				params["custscript_user_id"] = i_user;
				response.Data = nlapiScheduleScript("customscript_sch_createfrfdetails", "customdeploy_sch_createfrfdetails", params);
				response.Status = true;
				return response;
			}
			else
			{
				throw 'Account OR Project OR Opportunity Cannot be blank!!';
				response.Status = false;
				return response;
			}
			
		}
	}
	catch (err) 
	{
		nlapiLogExecution('ERROR', 'postRESTlet', err);
		response.Data = err;
		response.Status = false;
		return response;
	}
}
function _logValidation(value){
	if (value != null && value != '' && value != undefined && value != 'NaN') {
		return true;
	}
	else {
		return false;
	}
}