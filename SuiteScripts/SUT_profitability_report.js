
function suitelet_function(request, response){

	var str = request.getBody();
	
	var context		=	nlapiGetContext();
	
	var i_user_id	=	1732;
	
	var a_project_list	=	new Array();
	
	var a_project_status = new Array();
	
	var s_selected_project_name = '';
	
	var s_selected_project_status = '';
	
	var a_selected_project_list	=	request.getParameterValues('project');
	
	var a_selected_project_status = request.getParameterValues('project_stat');
		
	var showAll	=	false;
	
	var s_from	=	'';
	var s_to	=	'';	
	
	if(a_selected_project_list == null || a_selected_project_list.length == 0)
		{
			showAll	=	true;			
		}
		
	if(a_selected_project_status == null || a_selected_project_status.length == 0)
		{
			showAll	=	true;			
		}
	
	if(request.getParameter('user') != null && request.getParameter('user') != '' && i_user_id == '9122')
	{
		i_user_id	=	request.getParameter('user');
		
		//filters = filters.concat([new nlobjSearchFilter('trandate', null, 'onorafter', s_from)]);
	}
	
	var s_project_status = '';
	
	var search	=	nlapiLoadSearch('transaction', 1261);
	
	var filters	=	search.getFilters();	
	
	if(a_selected_project_status == null || a_selected_project_status.length == 0)
	{
		var project_filter	=	[[['custentity_projectmanager', 'anyOf', i_user_id], 'or', ['custentity_deliverymanager', 'anyOf', i_user_id]]];			
	}
	else
	{
		var s_selected	=	'';
		s_selected	=	' selected="selected" ';
		
		/*if (a_selected_project_status.length > 1)
		{
			var project_filter = [[['custentity_projectmanager', 'anyOf', i_user_id], 'or', ['custentity_deliverymanager', 'anyOf', i_user_id]], 'and', ['status', 'anyOf', 2]];
			s_project_status += '<option value="Active" ' + s_selected + '>Active</option>';
			s_project_status += '<option value="Closed" >Closed</option>';
			s_project_status += '<option value="Hold" >Hold</option>';
			s_project_status += '<option value="Pending" >Pending</option>';
		}
		else
		{*/
			if(a_selected_project_status[0] == 'Active')
			{
				var project_filter = [[['custentity_projectmanager', 'anyOf', i_user_id], 'or', ['custentity_deliverymanager', 'anyOf', i_user_id]], 'and', ['status', 'anyOf', 2]];
				s_project_status += '<option value="Active" ' + s_selected + '>Active</option>';
				/*s_project_status += '<option value="Closed" >Closed</option>';
				s_project_status += '<option value="Hold" >Hold</option>';
				s_project_status += '<option value="Pending" >Pending</option>';*/
			}
			else if(a_selected_project_status[0] == 'Closed')
			{
				var project_filter = [[['custentity_projectmanager', 'anyOf', i_user_id], 'or', ['custentity_deliverymanager', 'anyOf', i_user_id]], 'and', ['status', 'anyOf', 1]];
				s_project_status += '<option value="Closed" ' + s_selected + '>Closed</option>';
				/*s_project_status += '<option value="Active" >Active</option>';
				s_project_status += '<option value="Hold" >Hold</option>';
				s_project_status += '<option value="Pending" >Pending</option>';*/
			}
			else if(a_selected_project_status[0] == 'Hold')
			{
				var project_filter = [[['custentity_projectmanager', 'anyOf', i_user_id], 'or', ['custentity_deliverymanager', 'anyOf', i_user_id]], 'and', ['status', 'anyOf', 17]];
				s_project_status += '<option value="Hold" ' + s_selected + '>Hold</option>';
				/*s_project_status += '<option value="Active" >Active</option>';
				s_project_status += '<option value="Closed" >Closed</option>';
				s_project_status += '<option value="Pending" >Pending</option>';*/
			}
			else
			{
				var project_filter = [[['custentity_projectmanager', 'anyOf', i_user_id], 'or', ['custentity_deliverymanager', 'anyOf', i_user_id]], 'and', ['status', 'anyOf', 4]];
				s_project_status += '<option value="Pending" ' + s_selected + '>Pending</option>';
				/*s_project_status += '<option value="Active" >Active</option>';
				s_project_status += '<option value="Closed" >Closed</option>';
				s_project_status += '<option value="Hold" >Hold</option>';*/
			}
		//}
	}
	
	var project_search_results	=	searchRecord('job', null, project_filter, [new nlobjSearchColumn('internalid'), new nlobjSearchColumn('entityid'), new nlobjSearchColumn('jobname'), new nlobjSearchColumn('entitystatus')]);
	
	var s_project_options	=	'';
	
	
	var s_project_status_array = new Array();
	
	for(var i = 0; project_search_results != null && i < project_search_results.length; i++)
		{
			var i_project_id	=	project_search_results[i].getValue('internalid');
			var s_project_number	=	project_search_results[i].getValue('entityid');
			var s_project_name	=	project_search_results[i].getValue('jobname');
			var s_project_stat = project_search_results[i].getText('entitystatus');
			
			var s_selected_project	=	request.getParameter('project');
			
			a_project_list.push(s_project_number);
			a_project_status.push(s_project_stat);
			
			if(a_project_list.length == project_search_results.length && a_selected_project_list != null)
			{
				if(a_selected_project_list.length >= a_project_list.length)
				{
					a_selected_project_list	=	a_project_list;
				
					showAll	=	true;
				} 
			}
			
			var s_selected	=	'';
			if((a_selected_project_list != null && a_selected_project_list.indexOf(s_project_number) != -1) || showAll == true)
				{
					s_selected	=	' selected="selected" ';
					
					if(s_selected_project_name != '')
						{
							s_selected_project_name	+=	', ';
						}
					
					s_selected_project_name += s_project_number + ' ' + s_project_name;
				}
			
			/*if(a_selected_project_list == null)
			{
				s_project_options	+=	'<option value=' + s_project_number +'>' + s_project_number + ' ' + s_project_name + '</option>';
			}
			else
			{*/
				s_project_options	+=	'<option value="' + s_project_number + '" ' + s_selected + '>' + s_project_number + ' ' + s_project_name + '</option>';
			//}
			
		}
		
	
	
	if (a_selected_project_status != null)
	{
		if (a_selected_project_status.length > 1)
		{
			a_selected_project_status += 'Active';
		}
	}
	
	if(a_project_list.length == 0)
	{
		displayErrorForm("You don't have any projects under you.", "No Data To Display");	
	}
	else
	{
		if (a_selected_project_status == null)
		{
			var s_selected = '';
			s_selected	=	' selected="selected" ';
			s_project_status += '<option value="Active" ' + s_selected + '>Active</option>';
			s_project_status += '<option value="Closed" ' + s_selected + '>Closed</option>';
			s_project_status += '<option value="Hold" ' + s_selected + '>Hold</option>';
			s_project_status += '<option value="Pending" ' + s_selected + '>Pending</option>';
		}
	}
	
	if(a_selected_project_list == null || a_selected_project_list.length == 0)
	{
		if(a_project_list.length > 0)
			{
				a_selected_project_list	=	a_project_list;
				
				showAll	=	true;
			}
		else
			{
				a_selected_project_list	=	new Array();
			}
	}
	/*else
	{
		if(a_selected_project_list.length != a_project_list.length)
		{
			a_selected_project_list = a_project_list;
		}
	}*/
	
	if(a_selected_project_status == null || a_selected_project_status.length == 0)
	{
		if(a_project_status.length > 0)
			{
				a_selected_project_status	=	a_project_status;
				
				showAll	=	true;
			}
		else
			{
				a_selected_project_status	=	new Array();
			}
	}
	
	if(a_selected_project_list != null && a_selected_project_list.length != 0)
		{			
			var s_formula	=	'';
			
			for(var i = 0; i < a_selected_project_list.length; i++)
				{
					if(i != 0)
						{
							s_formula	+=	" OR";
						}
					s_formula	+=	" SUBSTR({custcolprj_name}, 0,9) = '" + a_selected_project_list[i] + "' ";
				}
			
			var projectFilter = new nlobjSearchFilter('formulanumeric', null, 'equalto', 1);
			
			projectFilter.setFormula("CASE WHEN " + s_formula + " THEN 1 ELSE 0 END");
			
			//var projectFilter = new nlobjSearchFilter('custcolprj_name', null, 'contains', a_selected_project_list);
			filters = filters.concat([projectFilter]);
		}
	else
		{		
			var projectFilter = new nlobjSearchFilter('formulanumeric', null, 'equalto', 1);
					
			projectFilter.setFormula("0");
					
			filters = filters.concat([projectFilter]);						
		}
	
	if(request.getParameter('from') != null && request.getParameter('from') != '')
		{
			s_from	=	request.getParameter('from');
			
			// filters = filters.concat([new nlobjSearchFilter('trandate', null, 'onorafter', s_from)]);
		}
	
	if(request.getParameter('to') != null && request.getParameter('to') != '')
	{
		s_to	=	request.getParameter('to');
		
		// filters = filters.concat([new nlobjSearchFilter('trandate', null, 'onorbefore', s_to)]);
	}
	
	var columns	=	search.getColumns();
	
	columns[0].setSort(false);
	columns[3].setSort(true);
	columns[9].setSort(false);
	
	var search_results	=	searchRecord('transaction', null, filters, [columns[2], columns[1], columns[0], columns[3], columns[4], columns[5], columns[8], columns[9]]);
	
	// Get the facility cost
	
	var s_facility_cost_filter	=	'';
	
	for(var i = 0; i < a_selected_project_list.length; i++)
		{
			s_facility_cost_filter += "'" + a_selected_project_list[i] + "'";
			
			if(i != a_selected_project_list.length - 1)
				{
					s_facility_cost_filter += ",";
				}
		}
	
	var facility_cost_filters	=	new Array();
	facility_cost_filters[0]	=	new nlobjSearchFilter('formulanumeric', null, 'equalto', 1);
	if(s_facility_cost_filter != '')
		{
			facility_cost_filters[0].setFormula('CASE WHEN {custrecord_arpm_project.entityid} IN (' + s_facility_cost_filter + ') THEN 1 ELSE 0 END');
		}
	else
		{
			facility_cost_filters[0].setFormula('0');
		}
	
	var facility_cost_columns	=	new Array();
	facility_cost_columns[0]	=	new nlobjSearchColumn('custrecord_arpm_period');
	facility_cost_columns[1]	=	new nlobjSearchColumn('custrecord_arpm_num_allocated_resources');
	facility_cost_columns[2]	=	new nlobjSearchColumn('custrecord_arpm_facility_cost_per_person');
	facility_cost_columns[3]	=	new nlobjSearchColumn('custrecord_arpm_location');
	
	var facility_cost_search_results	=	searchRecord('customrecord_allocated_resources_per_mon', null, facility_cost_filters, facility_cost_columns);
	
	var o_json	=	new Object();

	var o_data	=	{'Revenue': [], 'Discounts': [], 'People Cost': [], 'Facility Cost': [], 'Travel': [], 'Expense': []};
	
	var new_object = null;

	var s_period = '';

	var a_period_list	=	[];
	
	var a_category_list	=	[];
	
	var a_group	=	[];
	
	var a_income_group	=	[];
	
	for(var i = 0; search_results != null && i < search_results.length; i++)
	{
		var period = search_results[i].getText(columns[0]);

		var transaction_type = search_results[i].getText(columns[8]);		

		var transaction_date = search_results[i].getValue(columns[9]);
		
		var amount = parseFloat(search_results[i].getValue(columns[1]));
		
		var category = search_results[i].getValue(columns[3]);
		
		/*if(period == 'Feb 2015')
		{
			nlapiLogExecution('audit','period:- ',period);
			nlapiLogExecution('audit','amount:- ',amount);
			nlapiLogExecution('audit','category:- ',category);
			nlapiLogExecution('audit','transaction_type:- ',transaction_type);
			nlapiLogExecution('audit','transaction_num:- ',search_results[i].getValue(columns[4]));
		}*/
		
		if(!(category in o_data))
			{
				o_data[category]	=	new Array();
			}
		
		var isWithinDateRange	=	true;
		
		var d_transaction_date	=	nlapiStringToDate(transaction_date, 'datetimetz');
		
		if(s_from != '')
			{
				var d_from	=	nlapiStringToDate(s_from, 'datetimetz');				
				
				if(d_transaction_date < d_from)
					{
						isWithinDateRange	=	false;
					}
			}
		
		if(s_to != '')
			{
				var d_to	=	nlapiStringToDate(s_to, 'datetimetz');
				
				if(d_transaction_date > d_to)
				{
					isWithinDateRange	=	false;
				}
			}
		
		if(isWithinDateRange == true)
			{
				var i_index	=	a_period_list.indexOf(period);
			
				if(i_index == -1)
					{
						a_period_list.push(period);
					}
			
				i_index	=	a_period_list.indexOf(period);	
			}
		
		o_data[category].push({'period': period, 'amount': amount, 'num': search_results[i].getValue(columns[4]), 'memo': search_results[i].getValue(columns[5]), 'type': transaction_type, 'dt': transaction_date, 'include': isWithinDateRange});		
	}

	// Add facility cost	
	o_data['Facility Cost']	=	new Array();
	
	for(var i = 0;facility_cost_search_results != null && i < facility_cost_search_results.length; i++)
		{
			var s_period	=	facility_cost_search_results[i].getValue(facility_cost_columns[0]);
			var f_allocated_resources	=	facility_cost_search_results[i].getValue(facility_cost_columns[1]);
			var f_cost_per_resource		=	facility_cost_search_results[i].getValue(facility_cost_columns[2]);
			var s_location				=	facility_cost_search_results[i].getText(facility_cost_columns[3]);
			
			if(f_cost_per_resource == '')
				{
					f_cost_per_resource	=	0;
				}
			
			o_data['Facility Cost'].push({'period': s_period, 'amount': f_allocated_resources * f_cost_per_resource, 'num': '', 'memo': 'Facility cost for ' + f_allocated_resources + ' resources at ' + s_location + '.', 'type': 'Facility Cost', 'dt': '', 'include': a_period_list.indexOf(s_period) != -1});
		}
	
	var o_total	=	new Array();
	var o_period_total	=	new Array();
	
	var f_total_revenue	=	0.0;
	var f_total_expense	=	0.0;
	var f_profit_percentage	=	0.0;
	
	var isDateRangeSelected	=	false;
	
	if(s_from != '' || s_to != '')
		{
			isDateRangeSelected	=	true;
		}
	
		
	var a_chart	=	new Array();
	
	//a_chart.push(['x'].concat(a_period_list));
			
	for(var s_category in o_data)
		{		
			var a_temp	=	[];
		
			o_total[s_category]	=	0.0;
			o_period_total[s_category]	=	0.0;
			
			for(var i = 0; i < a_period_list.length; i++)
				{
					a_temp.push(0.0);					
				}
		
			for(var j = 0; j < o_data[s_category].length; j++)
				{
					var i_index	=	a_period_list.indexOf(o_data[s_category][j].period);
					
					a_temp[i_index]	+=	o_data[s_category][j].amount;
					
					if(o_data[s_category][j].include == true)
						{
							o_period_total[s_category]	+=	o_data[s_category][j].amount;
						}
					
					o_total[s_category]	+=	o_data[s_category][j].amount;
					
					if(s_category != 'Revenue' && s_category != 'Other Income' && s_category != 'Discounts')
					{
						f_total_expense	+=	o_data[s_category][j].amount;
					}
				else
					{
						f_total_revenue	+=	o_data[s_category][j].amount;
					}
				}
		
			if(s_category != 'Revenue' && s_category != 'Other Income' && s_category != 'Discounts')
				{
					a_group.push(s_category);
				}
			else
				{
					a_income_group.push(s_category);
				}
			
			a_chart.push([s_category].concat(a_temp));
		}
	
	var a_profit	=	['Net Margin(%)', 0.0];
	
	var a_expense	=	['', 0.0];
	
	var a_revenue	=	['', 0.0];
	
	// Calculate Profit
	for(var i = 0; i < a_chart.length; i++)
		{
			
			if(i == 0)
			{
				for(j = 1; j < a_chart[i].length; j++)
					{
						a_profit.push(0.0);
						
						a_expense.push(0.0);
						
						a_revenue.push(0.0);
					}				
			}
			
			for(var j = 1; j < a_chart[i].length; j++)
				{
				
					if(a_chart[i][0] == 'Revenue' || a_chart[i][0] == 'Other Income' || a_chart[i][0] == 'Discounts')
						{
							a_profit[j]	=	a_profit[j] + a_chart[i][j];
							
							a_revenue[j] = a_revenue[j] + a_chart[i][j];
							
							a_profit[a_profit.length - 1]	+=	a_chart[i][j];
							a_revenue[a_revenue.length - 1]	+=	a_chart[i][j];
						}
					else
						{
							a_profit[j]	=	a_profit[j] - a_chart[i][j];
							
							a_expense[j] = a_expense[j] + a_chart[i][j];
							
							a_profit[a_profit.length - 1]	-=	a_chart[i][j];
							a_expense[a_expense.length - 1]	+=	a_chart[i][j];
						}					
				}
		}
	
	for(var i = 1; i < a_profit.length; i++)
		{
			if(a_revenue[i] != 0.0)
				{
					a_profit[i] = (a_profit[i]/a_revenue[i] * 100).toFixed(2) + '%';
				}
			else
				{
					a_profit[i] = ' - ';
				}
		}

	if(f_total_revenue != 0)
		{
			f_profit_percentage	= (((f_total_revenue - f_total_expense)/f_total_revenue) * 100).toFixed(2) + '%';
		}
	else
		{
			f_profit_percentage	=	' - ';
		}
	
	//a_chart.push(a_profit);	
	
	//o_json.push(new_object);
	
	// Display the table
	var s_table	=	'<table>';
	// Header
	s_table	+=	'<thead><tr><th></th>';
	for(var i = 0; i < a_period_list.length; i++)
		{
			s_table += '<th>' + a_period_list[i] + '</th>';
		}
	if(isDateRangeSelected == true)
		{
			s_table	+=	'<th>Total</th>';
		}
	
	s_table	+=	'<th>Overall</th>';
	
	s_table	+=	'</thead></tr>';
	// Table
	for(var i = 0; i < a_chart.length; i++)
		{
			if(a_chart[i][0] == 'People Cost')
			{
				s_table += '<tr style="background-color:#CCC;"><td>Net Revenue</td>';
				for(var j = 1; j < a_revenue.length; j++)
					{
						s_table += '<td style="text-align:right;">' + accounting.formatNumber(a_revenue[j]) + '</td>';						
					}
				if(isDateRangeSelected == true){s_table	+=	'<td style="text-align:right;">'	+	accounting.formatNumber(f_total_revenue) + '</td>';}
				s_table += '</tr>';
			}
		
			s_table += '<tr>';
			for(var j = 0; j < a_chart[i].length; j++)
				{
					var strValue	=	'';
					if(j>0)
						{
							if(a_chart[i][0] != 'People Cost')
								{
									strValue	+=	'<a href="javascript:displayPopup(\'' + a_chart[i][0] + '\',\'' + a_period_list[j-1] + '\');">';
								}
							
							strValue	+=	accounting.formatNumber(a_chart[i][j]);
							a_chart[i][j]	=	Math.round(a_chart[i][j]);
							
							if(a_chart[i][0] != 'People Cost')
								{
									strValue	+=	'</a>';
								}
							
							s_table	+=	'<td style="text-align:right;">' + strValue + '</td>';
						}
					else
						{
							strValue	+=	a_chart[i][j];
							
							s_table	+=	'<td>' + strValue + '</td>';
						}					
				}
			s_table	+=	'<td style="text-align:right;">' + accounting.formatNumber(o_period_total[a_chart[i][0]]) + '</td>';
			if(isDateRangeSelected == true){s_table	+=	'<td style="text-align:right;">' + accounting.formatNumber(o_total[a_chart[i][0]]) + '</td>';}
			s_table += '</tr>';			
			
			if(i == a_chart.length - 1)
			{				
				s_table += '<tr style="background-color:#CCC;"><td>Total Cost</td>';
				for(var j = 1; j < a_expense.length; j++)
					{
						s_table += '<td style="text-align:right;">' + accounting.formatNumber(a_expense[j]) + '</td>';						
					}
				if(isDateRangeSelected == true){s_table	+=	'<td style="text-align:right;">'	+	accounting.formatNumber(f_total_expense) + '</td>';}
				s_table += '</tr>';
				
				s_table += '<tr style="font-weight:bold;"><td>Net Margin</td>';
				for(var j = 1; j < a_revenue.length; j++)
					{
						s_table += '<td style="text-align:right;">' + accounting.formatNumber(a_revenue[j] - a_expense[j]) + '</td>';
					}
				if(isDateRangeSelected == true){s_table	+= '<td style="text-align:right;">' + accounting.formatNumber(f_total_revenue - f_total_expense) + '</td>';}
				s_table += '</tr>';
			}
		}
	
	s_table += '<tfoot><tr style="font-weight:bold;">';
	
	for(var i = 0; i < a_profit.length; i++)
		{
			s_table += '<td>' + a_profit[i] + '</td>';
		}
	if(isDateRangeSelected == true){s_table	+= '<td>' + f_profit_percentage + '</td>';}
	s_table += '</tr></tfoot>';
	
	s_table += '</table>';
	
	// Define colors
	var a_colors = ['#7293cb', '#808585', '#ab6857', '#84ba5b', '#ccc210', '#d35e60','#9067a7', '#e1974c'];
	
	var o_bar_colors	=	new Object();
	var indx = 0;
	for(var s_category in o_data)
		{
			if(indx < a_colors.length)
				{
					o_bar_colors[s_category]	=	a_colors[indx];
					
					indx++;
				}			
		}
	
	var objValues	=	new Object();
	objValues.a_chart	=	JSON.stringify(a_chart);
	objValues.a_group	=	JSON.stringify(a_group);
	objValues.a_income_group	=	JSON.stringify(a_income_group);
	objValues.a_period_list	=	JSON.stringify(a_period_list);
	objValues.s_project_options	=	s_project_options;
	objValues.s_project_status	=	s_project_status;
	objValues.s_table	=	s_table;
	objValues.s_from	=	s_from;
	objValues.s_to		=	s_to;
	objValues.s_title	=	s_selected_project_name;
	objValues.json_data	=	JSON.stringify(o_data);
	objValues.url		=	nlapiResolveURL('SUITELET', context.getScriptId(), context.getDeploymentId());
	objValues.i_user_id	=	i_user_id;
	objValues.i_script_id	=	request.getParameter('script');
	objValues.i_deployment_id	=	request.getParameter('deploy');
	objValues.colors	=	JSON.stringify(o_bar_colors);
	
	var s_html	=	displayHTML(172628, objValues);	

	response.write(s_html);

	nlapiLogExecution('AUDIT', 'Run By', '');
	
	return;

	// Instantiate a report definition to work with
	var reportDefinition = nlapiCreateReportDefinition();
	
	reportDefinition.addRowHierarchy('custrecord_type', 'Type', 'TEXT');
	reportDefinition.addRowHierarchy('custrecord_category', 'Category', 'TEXT');
	reportDefinition.addRowHierarchy('custrecord_sub_category', 'Sub Category', 'TEXT');
	
	reportDefinition.addColumn('custrecord_transaction_number', false, 'Transaction Number', null, 'TEXT', null);
	reportDefinition.addColumn('custrecord_remarks', false, 'Remarks', null, 'TEXT', null);
	
	var columnQuarter	=	reportDefinition.addColumnHierarchy('custrecord_quarter', 'Quarter', null, 'TEXT');
	var columnPeriod	=	reportDefinition.addColumnHierarchy('custrecord_period', 'Period', columnQuarter, 'TEXT');
	
	reportDefinition.addColumn('custrecord_amount', true, 'Amount', columnPeriod, 'CURRENCY', null);
	
	reportDefinition.addSearchDataSource('transaction', null, filters, columns, 
																		{
																			'custrecord_type': columns[16], 
																			'custrecord_quarter': columns[15],
																			'custrecord_period': columns[1],
																			'custrecord_category': columns[17],
																			'custrecord_sub_category': columns[11],
																			'custrecord_amount': columns[6],
																			'custrecord_transaction_number': columns[3],
																			'custrecord_remarks': columns[5]});
	
	// Create a form to build the report on
	var form =
			nlapiCreateReportForm('Revenue Trend Report');

	// Build the form from the report definition
	var pvtTable = reportDefinition.executeReport(form);

	// Write the form to the browser
	response.writePage(form);
	
}

function displayHTML(i_file_id, objValues)
{
	var file = nlapiLoadFile(i_file_id); //load the HTML file
	var contents = file.getValue(); //get the contents
	
	contents = replaceValues(contents, objValues);
	
	return contents;
}

//Used to display the html, by replacing the placeholders
function replaceValues(content, oValues)
{
	for(param in oValues)
		{
			// Replace null values with blank
			var s_value	=	(oValues[param] == null)?'':oValues[param];
			
			// Replace content
			content = content.replace(new RegExp('{{' + param + '}}','gi'), s_value);
		}
	
	return content;
}