/**
* Module Description
* User Event script to handle employee record creation and updation from the XML file generated from Fusion
* 
 * Version               Date                      Author                           Remarks
* 1.00             18th Feb, 2016         Manikandan V           
 *
*/

function feedbackSuitelet(request, response)
{
	var method=request.getMethod();
	var employee_id= request.getParameter('custparam_employee_id'); 
  
	// Get the employee details
  
	var filters = new Array();
	filters[0]  = new nlobjSearchFilter('internalid',null,'anyof',employee_id);
   
	var columns   = new Array();
	columns[0]    = new nlobjSearchColumn('firstname');
	columns[1]    = new nlobjSearchColumn('custentity_fusion_empid');
	columns[2]    = new nlobjSearchColumn('department');
	columns[3]    = new nlobjSearchColumn('title');
	columns[4]    = new nlobjSearchColumn('custentity_actual_hire_date');
	columns[5]    = new nlobjSearchColumn('custentity_probationenddate');
	columns[6]    = new nlobjSearchColumn('custentity_reportingmanager');
	columns[7]    = new nlobjSearchColumn('location');
	columns[8]    = new nlobjSearchColumn('middlename');
	columns[9]    = new nlobjSearchColumn('lastname');
	columns[10]	  = new nlobjSearchColumn('email', 'custentity_reportingmanager');
   
   var emp_search = nlapiSearchRecord('employee',null,filters,columns);
  
	if (method == 'GET' )
	{
		var file = nlapiLoadFile(131759);   //load the HTML file
		var contents = file.getValue();    //get the contents
  
		if(isNotEmpty(emp_search))
		{
			var objReplaceValues=new Object();
			objReplaceValues['employee_name']=emp_search[0].getValue('firstname');
            objReplaceValues['employee_number']=emp_search[0].getValue('custentity_fusion_empid');
            objReplaceValues['employee_pratcice']=emp_search[0].getText('department');
			objReplaceValues['employee_designation']=emp_search[0].getValue('title');
            objReplaceValues['date_of_joining']=emp_search[0].getValue('custentity_actual_hire_date');
            objReplaceValues['review_period']=emp_search[0].getValue('custentity_probationenddate');
            objReplaceValues['employee_reporting_manager']=emp_search[0].getText('custentity_reportingmanager');
            objReplaceValues['employee_location']=emp_search[0].getText('location');
            
			contents = replaceValues(contents, objReplaceValues);
            
			response.write(contents);          //render it on  suitlet
		}
	}
	else
	{
		//var form = nlapiCreateForm("Suitelet - POST call");
		var feedback = new Object();
		feedback['employee_id'] =  request.getParameter('custparam_employee_id');
		feedback['feedback_record_id'] =request.getParameter('feedback_record_id');
		feedback['Response1'] = request.getParameter('OptionsGroup1');
		feedback['Response2'] = request.getParameter('OptionsGroup2');
		feedback['Response3'] = request.getParameter('OptionsGroup3');
		feedback['Response4'] = request.getParameter('OptionsGroup4');
		feedback['Response5'] = request.getParameter('OptionsGroup5');
		feedback['Response6'] = request.getParameter('OptionsGroup6');
		feedback['Response7'] = request.getParameter('OptionsGroup7');
		feedback['Response8'] = request.getParameter('OptionsGroup8');
		feedback['Response9'] = request.getParameter('OptionsGroup9');
		feedback['Comments1'] = request.getParameter('txtComments1');
		feedback['Comments3'] = request.getParameter('txtComments3');
		feedback['Comments4'] = request.getParameter('txtComments4');
		feedback['Comments5'] = request.getParameter('txtComments5');
  
		var status = saveRequest(feedback);
		
		if(isNotEmpty(emp_search))
		{
			var firstName = emp_search[0].getValue('firstname');
			var middleName = emp_search[0].getValue('middlename');
			var lastName = emp_search[0].getValue('lastname');
			var employeeNumber=emp_search[0].getValue('custentity_fusion_empid');
			var title=emp_search[0].getValue('title');
			var actual_hire_date=emp_search[0].getValue('custentity_actual_hire_date');
			var probation_end_date=emp_search[0].getValue('custentity_probationenddate');
			var overallRating = request.getParameter('OptionsGroup9');
			var reporting_manager_email = emp_search[0].getValue('email', 'custentity_reportingmanager');
		 
			sendEmail(firstName, employee_id,reporting_manager_email);
		 
			//generatePDF(firstName,middleName,lastName,title,employeeNumber,overallRating,actual_hire_date,probation_end_date);
		}
   
		var thanks_note = nlapiLoadFile(63444);   //load the HTML file
		var thanks_contents = thanks_note.getValue();    //get the contents
		response.write(thanks_contents);     //render it on  suitlet
	}
}

// Used to display the html, by replacing the placeholders
function replaceValues(content, oValues)
{
	for(param in oValues)
    {
		// Replace null values with blank
		var s_value	=	(oValues[param] == null)?'':oValues[param];

		// Replace content
        content = content.replace(new RegExp('{{' + param + '}}','gi'), s_value);
    }
                
    return content;
}   
   
function saveRequest(feedback)
{
	try
    {
        var feedback_form = nlapiLoadRecord('customrecord_confirmation_process',feedback.feedback_record_id);
                         
        feedback_form.setFieldValue('custrecord_new_question',"State in brief, achievements of the appraise during the review period against set KRA’s");
		feedback_form.setFieldValue('custrecord_new_question_2', "Select the appropriate rating, citing critical incidents if any, to validate your rating");
		feedback_form.setFieldValue('custrecord_new_question_2a', "(a) Job Knowledge(to meet demands of job)");
										  
		feedback_form.setFieldValue('custrecord_new_question_2b',"(b) Quality Of Work(consistent in view of time, cost etc)");
		feedback_form.setFieldValue('custrecord_new_question_2c',"(c) Problem solving & Decision making");
		feedback_form.setFieldValue('custrecord_new_question_2d',"(d) Management of resources(physical, financial or human)");
		feedback_form.setFieldValue('custrecord_new_question_2e',"(e) Ability for sustained hard(Work/energy level)");
		feedback_form.setFieldValue('custrecord_new_question_2f',"(f) Communication(verbally & written)");
		feedback_form.setFieldValue('custrecord_new_question_2g',"(g) Ability to work in a team");
		feedback_form.setFieldValue('custrecord_new_question_2h',"(h) Attendance & Discipline");
		feedback_form.setFieldValue('custrecord_new_question_3',"Factors that enabled / constrained performance in achieving results");
		feedback_form.setFieldValue('custrecord_new_question_4',"Any development needs for the employee in terms of training, job knowledge etc.");
		feedback_form.setFieldValue('custrecord_new_question_5',"Strengths / Weaknesses");
		feedback_form.setFieldValue('custrecord_new_question_6',"Overall Rating");

		feedback_form.setFieldValue('custrecord_new_answer_2a', feedback.Response1);
		feedback_form.setFieldValue('custrecord_new_answer_2b', feedback.Response2);
		feedback_form.setFieldValue('custrecord_new_answer_2c', feedback.Response3);
                                                   
        feedback_form.setFieldValue('custrecord_new_answer_2d', feedback.Response4);
		feedback_form.setFieldValue('custrecord_new_answer_2e', feedback.Response5);
		feedback_form.setFieldValue('custrecord_new_answer_2f', feedback.Response6);
		feedback_form.setFieldValue('custrecord_new_answer_2g', feedback.Response7);
		feedback_form.setFieldValue('custrecord_new_answer_2h', feedback.Response8);
		feedback_form.setFieldValue('custrecord_new_answer_6', feedback.Response9);

		feedback_form.setFieldValue('custrecord_new_comments1', feedback.Comments1);
		feedback_form.setFieldValue('custrecord_new_comments3', feedback.Comments3);
		feedback_form.setFieldValue('custrecord_new_comments4', feedback.Comments4);
		feedback_form.setFieldValue('custrecord_new_comments5', feedback.Comments5);
		feedback_form.setFieldValue('custrecord_njcp_employee_name', feedback.employee_id);
                                                   
        var id = nlapiSubmitRecord(feedback_form, true,true);
        nlapiLogExecution('debug', 'Record Saved', id);
	}
	catch(err)
	{
		nlapiLogExecution('error', 'Record not saved', err);
	}
}


function sendEmail(firstName, emp_id,custentity_reportingmanager_email)
{
	try
	{	
		var mailTemplate =serviceTemplate(firstName);
		nlapiSendEmail('10730', emp_id, mailTemplate.MailSubject, mailTemplate.MailBody,custentity_reportingmanager_email,null,{entity: emp_id});
	}
	catch(err)
	{
		nlapiLogExecution('error', 'sendEmail', err);
		throw err;
	}
}

function serviceTemplate(firstName)
{
	var htmltext = '';
     
    htmltext += '<table border="0" width="100%"><tr>';
	htmltext += '<td colspan="4" valign="top">';
	//htmltext += '<p>Hi ' + firstName + ',</p>';  
	htmltext += '<p>Dear' + firstName + ',</p>';
	//htmltext += '&nbsp';
		 
	//htmltext += &nbsp;
	htmltext +='<p></p>';
	htmltext +='<p>This is confirm you that you had been disscusion with your Reporting Manager about your Employeement Confirmation</p>';
	htmltext +='<p>You will receive a information soon</p>'; 
	htmltext += '<p>Thanks & Regards,</p>';
    htmltext += '<p>Team HR</p>';
			
	return {
        MailBody : htmltext,
        MailSubject : "Employment Confirmation"      
    };
}


function generatePDF(firstName,middleName,lastName,title,employeeNumber,overallRating,custentity_actual_hire_date,probation_end_date)
{
	var htmltext = '';
	
	htmltext += '<pdf><body>';
	//Search the rating value if rating is Outstanding,Exceed Expectations, Met Expectations Confirmation Form will be Generated

	if (overallRating == 'Outstanding'|| overallRating == 'Exceeded Expectation'|| overallRating == 'Met Expectation')
	{
		htmltext += '<p style="margin-right:380px;">Mr.'+ firstName +''+ middleName + ''+ lastName +'  </p>';
		htmltext += '<p style="margin-right:380px;"> '+title+'</p>';
		htmltext += '<p style="margin-left:380px;">Employee Number : '+employeeNumber+'</p>';
		
		htmltext += '<table border="0" width="100%"><tr>';
		htmltext += '<td colspan="4" valign="top">';
		//htmltext += '<p>Hi ' + firstName + ',</p>';  
		htmltext += '<p>Dear ' + firstName + ',</p>';
		//htmltext += '&nbsp';
		
		//htmltext += &nbsp;
		htmltext += '<p> Your performance for the period  ' + custentity_actual_hire_date + ' to ' + probation_end_date + '  has been assessed as "<b>'+overallRating+' </b>" Consequently, we are pleased to inform you that with effect from ' + probation_end_date + ' your employment with the Company is confirmed All other terms and conditions of your employment as stated in the Appointment Letter remain the same.We would like to express our sincere appreciation for your efforts and look forward to your continued contribution to the success of the company.</p>';
		
		htmltext +='<p>Wishing you the very best in the coming years</p>';
        htmltext += '<p>Yours sincerely</p>';
		htmltext += '<p>for Brillio Technologies Pvt. Ltd.,</p>';


		htmltext +='<p><b>Hemavathi J</b></p>';

		htmltext +='<p><img src="https://system.na1.netsuite.com/core/media/media.nl?id=131760&amp;c=3883006&amp;h=888054e2a84780788144"/></p>';
										  
		htmltext +='<p><b>Senior Manager - Human Resources</b></p>';


		htmltext += '</td></tr>';
		htmltext += '</table>';
	}
	else if (overallRating == 'Short of Expectation')
	{
		htmltext += '<p style="margin-right:380px;">Mr.'+ firstName +''+ middleName + ' '+ lastName +'  </p>';
		htmltext += '<p style="margin-right:380px;"> '+title+'</p>';
		htmltext += '<p style="margin-left:380px;">Employee Number : '+employeeNumber+'</p>';
		
		htmltext += '<table border="0" width="100%"><tr>';
		htmltext += '<td colspan="4" valign="top">';
		//htmltext += '<p>Hi ' + firstName + ',</p>';  
		htmltext += '<p>Dear '+ firstName +', </p>';
		//htmltext += &nbsp;
		htmltext += '<p> Your performance for the period  '+custentity_actual_hire_date+' to '+probation_end_date+' has been assessed as "<b>'+overallRating+'</b>" Consequently, your probation period has been extended for further Months with effect from '+probation_end_date+' </p>';
					
					
					
		htmltext += '<p>Yours sincerely</p>';
		htmltext += '<p>for Brillio Technologies Pvt. Ltd.,</p>';

		htmltext +='<p><b>Hemavathi J</b></p>';
		htmltext +='<p><img src="https://system.na1.netsuite.com/core/media/media.nl?id=131760&amp;c=3883006&amp;h=888054e2a84780788144"/></p>';
		htmltext +='<p><b>Senior Manager - Human Resources</b></p>';
	 
		htmltext += '</td></tr>';
		htmltext += '</table>';
	}
	
	htmltext += "</body></pdf>";

	var filePDF = nlapiXMLToPDF(htmltext);
	
	filePDF.setFolder(42090);
	
	nlapiSubmitFile(filePDF);
}