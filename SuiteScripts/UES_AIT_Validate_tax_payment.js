// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
/*
   	Script Name:UES_Validate_tax_payment.js
	Author:		Nikhil jain
	Company:	Aashna cloudtech Pvt. Ltd.
	Date:		12 Jan 2016
	Description:validate the tax payment while deleting the journal entry and check record 


	Script Modification Log:

	-- Date --			-- Modified By --				--Requested By--				-- Description --



Below is a summary of the process controls enforced by this script file.  The control logic is described
more fully, below, in the appropriate function headers and code blocks.


     BEFORE LOAD
		- beforeLoadRecord(type)



     BEFORE SUBMIT
		- beforeSubmitRecord(type)


     AFTER SUBMIT
		- afterSubmitRecord(type)



     SUB-FUNCTIONS
		- The following sub-functions are called by the above core functions in order to maintain code
            modularization:

               - NOT USED

*/
}
// END SCRIPT DESCRIPTION BLOCK  ====================================



// BEGIN GLOBAL VARIABLE BLOCK  =====================================
{
	//  Initialize any Global Variables, in particular, debugging variables...




}
// END GLOBAL VARIABLE BLOCK  =======================================





// BEGIN BEFORE LOAD ==================================================

function beforeLoadRecord(type)
{

	/*  On before load:

          - EXPLAIN THE PURPOSE OF THIS FUNCTION

		-


		FIELDS USED:

          --Field Name--				--ID--


	*/



	//  LOCAL VARIABLES

    //  BEFORE LOAD CODE BODY


	return true;



}

// END BEFORE LOAD ====================================================





// BEGIN BEFORE SUBMIT ================================================

function beforeSubmitRecord_ait_validate_taxpay(type){
	/*  On before submit:
	 - PURPOSE
	 -
	 FIELDS USED:
	 --Field Name--				--ID--
	 */
	//  LOCAL VARIABLES
	
	
	//  BEFORE SUBMIT CODE BODY
	var Flag = 0;
	var a_subisidiary = new Array()
	var s_pass_code;
	
	
	if (type == 'delete') {
		var s_recordType = nlapiGetRecordType();
		var i_recordID = nlapiGetRecordId();
		
		if (s_recordType == 'check') {
			var i_AitGlobalRecId = SearchGlobalParameter();
			nlapiLogExecution('DEBUG', 'Bill ', "i_AitGlobalRecId" + i_AitGlobalRecId);
			
			
			if (i_AitGlobalRecId != 0) {
				var o_AitGloRec = nlapiLoadRecord('customrecord_indian_tax_glo_parameter', i_AitGlobalRecId);
				
				a_subisidiary = o_AitGloRec.getFieldValues('custrecord_ait_subsidiary');
				nlapiLogExecution('DEBUG', 'Bill ', "a_subisidiary->" + a_subisidiary);
				
				var s_pass_code = o_AitGloRec.getFieldValue('custrecord_encpwdstring');
				nlapiLogExecution('DEBUG', 'Bill ', "s_pass_code->" + s_pass_code);
				
				
			}// end if(i_AitGlobalRecId != 0 )
			var i_subsidiary = nlapiGetFieldValue('subsidiary');
			
			Flag = isindia_subsidiary(a_subisidiary, s_pass_code, i_subsidiary)
			
			//==================================Begin :-Search to Display an Adjust Advance Button===========================//
			
			if (Flag == 1) {
				var taxpay_filter = new Array();
				taxpay_filter.push(new nlobjSearchFilter('custrecord_paymentvat_chequeno', null, 'anyOf', i_recordID))
				taxpay_filter.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'))
				
				var taxpay_column = new Array();
				taxpay_column.push(new nlobjSearchColumn('internalid'))
				
				var taxpay_results = nlapiSearchRecord('customrecord_vatpayment_main', null, taxpay_filter, taxpay_column);
				if (taxpay_results != null && taxpay_results != '' && taxpay_results != undefined) {
					var i_taxpayid = taxpay_results[0].getValue('internalid')
					nlapiLogExecution('DEBUG', 'validate tax payment', 'Tax payment' + i_taxpayid)
					
					throw "You are not allowed to delete the Tax payment Tansaction"
					
				}
			}
			
		}
		if (s_recordType == 'journalentry') {
			var i_AitGlobalRecId = SearchGlobalParameter();
			nlapiLogExecution('DEBUG', 'Bill ', "i_AitGlobalRecId" + i_AitGlobalRecId);
			
			
			if (i_AitGlobalRecId != 0) {
				var o_AitGloRec = nlapiLoadRecord('customrecord_indian_tax_glo_parameter', i_AitGlobalRecId);
				
				a_subisidiary = o_AitGloRec.getFieldValues('custrecord_ait_subsidiary');
				nlapiLogExecution('DEBUG', 'Bill ', "a_subisidiary->" + a_subisidiary);
				
				var s_pass_code = o_AitGloRec.getFieldValue('custrecord_encpwdstring');
				nlapiLogExecution('DEBUG', 'Bill ', "s_pass_code->" + s_pass_code);
				
				
			}// end if(i_AitGlobalRecId != 0 )
			var i_subsidiary = nlapiGetFieldValue('subsidiary');
			
			Flag = isindia_subsidiary(a_subisidiary, s_pass_code, i_subsidiary)
			
			//==================================Begin :-Search to Display an Adjust Advance Button===========================//
			
			if (Flag == 1) {
				var taxpay_filter = new Array();
				taxpay_filter.push(new nlobjSearchFilter('custrecord_paymentvat_journalentry', null, 'anyOf', i_recordID))
				taxpay_filter.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'))
				
				var taxpay_column = new Array();
				taxpay_column.push(new nlobjSearchColumn('internalid'))
				
				var taxpay_results = nlapiSearchRecord('customrecord_vatpayment_main', null, taxpay_filter, taxpay_column);
				if (taxpay_results != null && taxpay_results != '' && taxpay_results != undefined) {
					var i_taxpayid = taxpay_results[0].getValue('internalid')
					nlapiLogExecution('DEBUG', 'validate tax payment', 'Tax payment' + i_taxpayid)
					
					throw "You are not allowed to delete the Tax payment Tansaction"
					
				}
			}
		}
	}
	return true;
}

// END BEFORE SUBMIT ==================================================





// BEGIN AFTER SUBMIT =============================================

function afterSubmitRecord(type)
{
    /*  On after submit:

          - PURPOSE
		-

	FIELDS USED:

          --Field Name--				--ID--


    */


	//  LOCAL VARIABLES
	

	//  AFTER SUBMIT CODE BODY

	return true;

}

// END AFTER SUBMIT ===============================================





// BEGIN FUNCTION ===================================================
 function SearchGlobalParameter()	
	{
    
        var a_filters = new Array();
        var a_column = new Array();
        var i_globalRecId = 0;
        
        a_column.push(new nlobjSearchColumn('internalid'));
        
        var s_serchResult = nlapiSearchRecord('customrecord_indian_tax_glo_parameter', null, null, a_column)
        
        if (s_serchResult != null && s_serchResult != undefined && s_serchResult != '') 
		{
            for (var i = 0; i < s_serchResult.length; i++) 
			{
                i_globalRecId = s_serchResult[0].getValue('internalid');
                nlapiLogExecution('DEBUG', 'Bill ', "i_globalRecId" + i_globalRecId);
                
            }
            
        }
        
        
        return i_globalRecId;
    }
// END FUNCTION =====================================================
