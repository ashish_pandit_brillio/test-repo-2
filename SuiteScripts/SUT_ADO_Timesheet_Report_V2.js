function suitelet(request, response) {
	try {
		var request_project_id = request.getParameter('custpage_project');
		var request_to_date = request.getParameter('custpage_to_date');
		var request_from_date = request.getParameter('custpage_from_date');
		var request_customer_id = request.getParameter('custpage_customer');
        nlapiLogExecution('debug','request_customer_id',request_customer_id);
		 nlapiLogExecution('debug','request_project_id',request_project_id);
		// check the employee access level
		var user = nlapiGetUser();
		var context=nlapiGetContext();

		if (user == 41571 || user == 62082) {
		user = 1646;
		}
		if(user == 37827){
		user = 1720;
		}

		if(user == user)
		{
		//user=3165;
		}
         if(user==278398)
        {
          user =240746;
        }
		var s_currentusertype='';
		var s_currentusertypeprojects='';
		var allocation_filters=[];
		if(context.getDeploymentId()=='customdeploy_sut_ado_timesheet_report_v2')
		{
			s_currentusertype='custentity_account_delivery_owner';
			s_currentusertypeprojects='customer.custentity_account_delivery_owner';
			allocation_filters[allocation_filters.length]=new nlobjSearchFilter('custentity_account_delivery_owner', 'customer','anyof', user);
		}
		else if(context.getDeploymentId()=='customdeploy_sut_dm_timesheet_report_v2')
		{
			s_currentusertype='job.custentity_projectmanager';
			s_currentusertypeprojects='custentity_projectmanager';
			allocation_filters[allocation_filters.length]=new nlobjSearchFilter('custentity_projectmanager', 'job','anyof', user);
		}
		else if (context.getDeploymentId()=='customdeploy_sut_pm_timesheet_report_v2')
		{
			s_currentusertype='job.custentity_deliverymanager';
			s_currentusertypeprojects='custentity_deliverymanager';
			allocation_filters[allocation_filters.length]=new nlobjSearchFilter('custentity_deliverymanager', 'job','anyof', user);
		}
		// get all projects he is a ADO
		var customerList = getRelatedCustomers(user,s_currentusertype);
		nlapiLogExecution('debug', 'customerList', 'customerList');

		if (!customerList) {
			throw "You are not authorised to access this page. Please contact your account administrator for details";
		}
		

		// get all projects he is a PM / DM
		var projectList = getRelatedProjects(user,request_customer_id,s_currentusertypeprojects);
nlapiLogExecution('debug', 'projectList', projectList);
		if (!projectList) {
			throw "You are not authorised to access this page. Please contact your account administrator for details";
		}
		


		var finalHtml = "";

		if (request_project_id) {
			// var projectList = [ request_project_id ]; // 10975

			if (request_to_date) {
				request_to_date = nlapiStringToDate(request_to_date);
			} else {
				request_to_date = new Date();
			}

			nlapiLogExecution('debug', 'request_to_date', request_to_date);

			// get the saturday of the week as the end date for the searchs
			var end_day = request_to_date.getDay();
			var sunday_of_end_week = nlapiAddDays(request_to_date, -end_day);
			var endDate = nlapiAddDays(sunday_of_end_week, 6);

			nlapiLogExecution('debug', 'enddate', endDate);

			// var endDate = nlapiStringToDate("9/30/2015"); // has to be a
			// saturday

			var startDate = null;
			if (request_from_date) {
				request_from_date = nlapiStringToDate(request_from_date);
				var end_day = request_from_date.getDay();
				startDate = nlapiAddDays(request_from_date, -end_day);
			} else {
				startDate = nlapiAddDays(endDate, -69); // 2 months back
			}

			nlapiLogExecution('debug', 'startDate', startDate);
			request_from_date = startDate;
			// get week start dates
			var weekStartDateList = [];
			for (var i = 0;; i += 7) {
				var d_current_date = nlapiAddDays(startDate, i);

				if (d_current_date > endDate) {
					break;
				}

				weekStartDateList
				        .push(nlapiDateToString(d_current_date, 'date'));
			}

			var itemList = [ 'ST', 'OT', 'Leave', 'Holiday' ];
			var statusList = [ 'Open', 'Pending Approval', 'Approved', 'Billed' ];
			var employeeList = {};
			var a_customer_array=[];
			// check the allocations of the project for the duration and create
			// a
			// master array
			nlapiLogExecution('debug', 'before allocation done');
			
			allocation_filters[allocation_filters.length]=new nlobjSearchFilter('custentity_implementationteam','employee', 'is', 'F');
			allocation_filters[allocation_filters.length]=new nlobjSearchFilter('startdate', null,'onorbefore', endDate);
			allocation_filters[allocation_filters.length]=new nlobjSearchFilter('enddate', null, 'onorafter',startDate);
									
					if(request_customer_id&&request_customer_id!='All')
					{
						allocation_filters[allocation_filters.length]=new nlobjSearchFilter('customer', null, 'anyof',request_customer_id);
					}
					if(request_project_id&&request_project_id!='All')
					{
						allocation_filters[allocation_filters.length]=new nlobjSearchFilter('project', null, 'anyof',request_project_id);
					}
			nlapiLogExecution('debug', 'allocation_filters.length',allocation_filters.length);
			
			var allocationSearch = nlapiSearchRecord('resourceallocation',
			        null,allocation_filters, [ new nlobjSearchColumn(
			                'resource', null, 'group'), new nlobjSearchColumn(
			                'customer', null, 'group')]);

			nlapiLogExecution('debug', 'allocation done');

			if (allocationSearch) {
				nlapiLogExecution('debug', 'allocation count',
				        allocationSearch.length);

				for (var i = 0; i < allocationSearch.length; i++) {

					employeeList[allocationSearch[i].getText('resource', null,
					        'group')] = {
						ID : allocationSearch[i].getValue('resource', null,
						        'group')
					};
					if(a_customer_array.indexOf(allocationSearch[i].getValue('customer', null,'group'))==-1){
					a_customer_array.push(allocationSearch[i].getValue('customer', null,'group'));
					}
				}

				nlapiLogExecution('debug', 'allocation array', JSON
				        .stringify(employeeList));

				// create master array
				var masterArray = createMasterArray(employeeList, statusList,
				        itemList, weekStartDateList);
               nlapiLogExecution('debug', 'masterArray', JSON.stringify(masterArray));

				  var timeentry_filters=[];
				   timeentry_filters.push(new nlobjSearchFilter('date', null, 'within',startDate, endDate));
					if(request_customer_id=='All')
					{
						timeentry_filters.push(new nlobjSearchFilter('customer', 'job', 'anyof',a_customer_array));
					
					}
					if(request_customer_id && request_customer_id!='All')
					{
						timeentry_filters.push(new nlobjSearchFilter('customer', 'job', 'anyof',request_customer_id));
					}
					if(request_project_id && request_project_id!='All')
					{
						allocation_filters.push(new nlobjSearchFilter('customer', null, 'anyof',request_project_id));
					}
					
					

				// search the time entries for the project
				var timeEntrySearch = searchRecord('timebill', 'customsearch1102_2', timeentry_filters);
				//var timeEntrySearch = searchRecord('timeentry', 'customsearch1102', timeentry_filters);

				if (timeEntrySearch) {
					nlapiLogExecution('debug', 'time entry count',
					        timeEntrySearch.length);

					var timeentry_master = {};

					// create a JSON object employee wise
					timeEntrySearch
					        .forEach(function(timeentry) {
						        var empName = timeentry.getText('employee',
						                null, 'group');
						        var empId = timeentry.getValue('employee',
						                null, 'group');
						        var timeEntryDuration = parseFloat(timeentry
						                .getValue('durationdecimal', null,
						                        'sum'));
						        var timesheetStartDate = timeentry.getValue(
						                'startdate', 'timesheet', 'group');
						        var timeEntryStatusText = timeentry.getText(
						                'approvalstatus', 'timesheet', 'group');
						        var timeEntryItemText = timeentry.getText(
						                'item', null, 'group');

						        // nlapiLogExecution('debug', 'item',
						        // timeEntryItemText);

						        if (timeEntryItemText != "Leave"
						                && timeEntryItemText != "Holiday"
						                && timeEntryItemText != "OT") {
							        timeEntryItemText = "ST";
						        }

						        nlapiLogExecution('debug', 'emp name', empName);
								//nlapiLogExecution('debug', 'timeEntryStatusText',timeEntryStatusText)
								//nlapiLogExecution('debug', 'timesheetStartDate',timesheetStartDate)
						        
						        try{
						        	
						        	if(!masterArray[empName]["Weeks"]){
										nlapiLogExecution('debug', 'masterArray[empName]["Weeks"][timesheetStartDate]',masterArray[empName]["Weeks"][timesheetStartDate]);
						        
						        		return;
						        	}
						        }
						        catch(ex){
						        	nlapiLogExecution('debug', 'emp not found',ex);
						        	return;
						        }
						        
								
									var d_timesheetStartDate=nlapiStringToDate(timesheetStartDate);
									if(d_timesheetStartDate.getDay()==1)//If week start of the day is MONDAY then append the values
									{
										timesheetStartDate=nlapiDateToString(nlapiAddDays(nlapiStringToDate(timesheetStartDate),-1));

									/*	masterArray[empName]["Weeks"][timesheetStartDate]={
										Status : 'Not-Submitted',
										Items : new Items()
									};
									*/
									}
									
									
								//nlapiLogExecution('debug', 'masterArray[empName]["Weeks"][timesheetStartDate]',masterArray[empName]["Weeks"][timesheetStartDate]);
						        switch (timeEntryStatusText) {
							        case "Pending Approval":
											masterArray[empName]["Weeks"][timesheetStartDate]["Status"] = "Submitted";
										break;

							        case "Approved":
								        masterArray[empName]["Weeks"][timesheetStartDate]["Status"] = "Approved";
							        break;
									
						        }

						        // nlapiLogExecution(
						        // / 'debug',
						        // 'A ' + empName + " " + timesheetStartDate + "
						        // "
						        // + timeEntryItemText,
						        // masterArray[empName]["Weeks"][timesheetStartDate]["Items"][timeEntryItemText]);
								
								if (masterArray[empName]) {
									//masterArray[empName]["Weeks"][timesheetStartDate]["Items"][timeEntryItemText]=masterArray[empName]["Weeks"][timesheetStartDate]["Items"][timeEntryItemText]?masterArray[empName]["Weeks"][timesheetStartDate]["Items"][timeEntryItemText]+timeEntryDuration : timeEntryDuration;
										masterArray[empName]["Weeks"][timesheetStartDate]["Items"][timeEntryItemText] = timeEntryDuration;
										
									
								}




						        // nlapiLogExecution(
						        // 'debug',
						        // 'B ' + empName + " " + timesheetStartDate + "
						        // "
						        // + timeEntryItemText,
						        // masterArray[empName]["Weeks"][timesheetStartDate]["Items"][timeEntryItemText]);
					        });

					nlapiLogExecution('debug', 'master timeentry', JSON
					        .stringify(masterArray));
				}

				// create a HTML table

				// CSS file

				var css_file_url = "";

				var context = nlapiGetContext();
				if (context.getEnvironment() == "SANDBOX") {
					css_file_url = "https://system.na1.netsuite.com/core/media/media.nl?id=63885&c=3883006&h=1ab6e33882afec4c1d2b&_xt=.css";
				} else if (context.getEnvironment() == "PRODUCTION") {
					css_file_url = "https://system.na1.netsuite.com/core/media/media.nl?id=63885&c=3883006&h=1ab6e33882afec4c1d2b&_xt=.css";
				}

				var htmlText = "";
				htmlText += "<link href='" + css_file_url
				        + "'  rel='stylesheet'>";

				htmlText += "<table class='timesheet-master-table'>";
				htmlText += "<tr class='header-row'>";

				htmlText += "<td>";
				htmlText += "Employee Name";
				htmlText += "</td>";

				var statusCssArray = {
				    "Approved" : "ts-approved",
				    "Submitted" : "ts-submitted",
				    "Not-Submitted" : "ts-not-submitted"
				};

				for ( var weekDate in weekStartDateList) {
					htmlText += "<td>";
					htmlText += weekStartDateList[weekDate];
					htmlText += "</td>";
				}
				htmlText += "<td>";
				htmlText += "Total";
				htmlText += "</td>";
				htmlText += "</tr>";

				for ( var empName in masterArray) {
					var total_st = 0, total_ot = 0, total_leave = 0, total_holiday = 0;

					htmlText += "<tr>";
					htmlText += "<td class='emp-name'>" + empName + "</td>";

					for ( var weekDate in weekStartDateList) {
						var weekStatus = masterArray[empName]["Weeks"][weekStartDateList[weekDate]]["Status"];
						var weekData = masterArray[empName]["Weeks"][weekStartDateList[weekDate]]["Items"];
						
						//nlapiLogExecution("DEBUG",'weekStatus',weekStatus);
						//nlapiLogExecution("DEBUG",'weekData',weekData);
						
						if (weekStatus == "Not-Submitted") {
							htmlText += "<td class='"
							        + statusCssArray[weekStatus] + "'>";
							htmlText += "-";
							htmlText += "</td>";
						} else {
							htmlText += "<td class='"
							        + statusCssArray[weekStatus] + "'>";
							htmlText += "<p class='st'>" + weekData.ST + "</p>";
							htmlText += " / ";
							htmlText += "<p class='ot'>" + weekData.OT + "</p>";
							htmlText += " / ";
							htmlText += "<p class='leave'>" + weekData.Leave
							        + "</p>";
							htmlText += " / ";
							htmlText += "<p class='holiday'>"
							        + weekData.Holiday + "</p>";
							htmlText += "</td>";

							total_st += parseFloat(weekData.ST);
							total_ot += parseFloat(weekData.OT);
							total_leave += parseFloat(weekData.Leave);
							total_holiday += parseFloat(weekData.Holiday);
						}
					}

					// add the total row
					htmlText += "<td class='total-column'>";
					htmlText += "<p class='st'>" + total_st + "</p>";
					htmlText += " / ";
					htmlText += "<p class='ot'>" + total_ot + "</p>";
					htmlText += " / ";
					htmlText += "<p class='leave'>" + total_leave + "</p>";
					htmlText += " / ";
					htmlText += "<p class='holiday'>" + total_holiday + "</p>";
					htmlText += "</td>";

					htmlText += "</tr>";
				}

				htmlText += "</table>";

				finalHtml = "<table class='master-table'><tr><td>" + htmlText
				        + "</td><td class='legend-table-master-cell'>"
				        + legendsTable() + "" + hoursLegendTable()
				        + "</td></tr></table>";
			} else {
				finalHtml = "No Allocation Active For This Selected Customer/Project";
			}
		} else {
			finalHtml = "Select any Customer and click refresh";
		}

		nlapiLogExecution('debug', 'lets create the page');
		var form = nlapiCreateForm("Timesheet Report - Weekly");

		form.addFieldGroup('custpage_grp_1', 'Select Details');
       
		var field_from_date = form.addField('custpage_from_date', 'date',
		        'From Date', null, 'custpage_grp_1');
		field_from_date.setBreakType('startcol');

		if (request_from_date) {

			field_from_date.setDefaultValue(nlapiDateToString(
			        request_from_date, 'date'));
		}

		var field_to_date = form.addField('custpage_to_date', 'date',
		        'To Date', null, 'custpage_grp_1');
		field_to_date.setBreakType('startcol');

		if (request_to_date) {

			field_to_date.setDefaultValue(nlapiDateToString(request_to_date,'date'));
		}
		var field_customer = form.addField('custpage_customer', 'select',
		        'Customer', null, 'custpage_grp_1');
		field_customer.setBreakType('startcol');
		field_customer.setMandatory(true);
		var field_project = form.addField('custpage_project', 'select',
		        'Project', null, 'custpage_grp_1');
		field_project.setBreakType('startcol');
		field_project.setMandatory(true);
		
		var o_current_usertype = form.addField('custpage_currentusertype',
                    'text', 'Current User type').setDisplayType('hidden');

                o_current_usertype.setDefaultValue(s_currentusertypeprojects);
		form.setScript('customscript_cli_ado_timesheet_report_v2');
		
		if (!request_customer_id) {
			field_customer.addSelectOption('', '', true);
			field_customer.addSelectOption('All', 'All');
		}
		else{
			field_customer.addSelectOption('', '');
			field_customer.addSelectOption('All', 'All');
		}

		if (customerList) {

			for (var i = 0; i < customerList.length; i++) {
				field_customer.addSelectOption(customerList[i].getId(),
				        customerList[i].getValue('entityid') + " "
				                + customerList[i].getValue('altname'));
			}
		}
		
		if (request_customer_id) 
		{
		field_customer.setDefaultValue(request_customer_id);
		}
/*
		if(request_customer_id=="All")
		{
			field_project.addSelectOption('All','All');
		}
		*/
		
		
		if (projectList) {
			//field_project.addSelectOption('','',true);
			field_project.addSelectOption('All','All');
				
			for (var i = 0; i < projectList.length; i++) {
				field_project.addSelectOption(projectList[i].getId(),
						projectList[i].getValue('entityid') + " "
								+ projectList[i].getValue('altname'));
			}
		}
		
		if (request_project_id) 
		{
		field_project.setDefaultValue(request_project_id);
		}
		
		form.addFieldGroup('custpage_grp_2', 'Timesheet Details');
		form.addField('custpage_timesheet_html', 'inlinehtml', '', null,
		        'custpage_grp_2').setDefaultValue(finalHtml);
	
		form.addSubmitButton("Refresh");
		
		response.writePage(form);
	} catch (err) {
		nlapiLogExecution('error', 'suitelet', err);
		displayErrorForm(err, "Timesheet Report - Weekly");
	}
}

function createMasterArray(empList, statusList, itemList, weekList) {
	// create the employee array
	var employeeArray = {};
	for ( var empName in empList) {

		employeeArray[empName] = {
		    Id : empList[empName]["ID"],
		    Weeks : {}
		};

		for ( var weekStartDate in weekList) {
			employeeArray[empName]["Weeks"][weekList[weekStartDate]] = {
			    Status : 'Not-Submitted',
			    Items : new Items()
			};
		}

		// employeeArray[empName]["Weeks"]
	}

	return employeeArray;
}

function Items() {
	this.ST = 0;
	this.OT = 0;
	this.Leave = 0;
	this.Holiday = 0;
}

function legendsTable() {
	var htmlText = "";
	htmlText += "<table class='legend-table'>";
	htmlText += "<tr><td class='approved-circle'></td><td>Approved</td></tr>";
	htmlText += "<tr><td class='submitted-circle'></td><td>Pending Approval</td></tr>";
	htmlText += "<tr><td class='not-submitted-circle'></td><td>Not-Submitted</td></tr>";
	htmlText += "</table>";
	return htmlText;
}

function hoursLegendTable() {
	var htmlText = "";
	htmlText += "<table class='legend-table'>";
	htmlText += "<tr><td class='hours-legend'>ST / OT / Leave / Holiday</td></tr>";
	htmlText += "</table>";
	return htmlText;
}

function getRelatedProjects(currentUser,request_customer_id,s_currentusertypeprojects) {
	var filters = [
	        [s_currentusertypeprojects,"anyof",currentUser],
	        'and',
	        [ [ 'status', 'anyof', '2' ], 'or', [ 'status', 'anyof', '4' ] ] ];
			if(request_customer_id && request_customer_id!='All')
			{
				filters.push('and');
				filters.push(["customer","anyof",request_customer_id]);
			}

	var columns = [ new nlobjSearchColumn("entityid"),
	        new nlobjSearchColumn("altname") ];

	var search = nlapiSearchRecord('job', null, filters, columns);
	return search;
}
function getRelatedCustomers(currentUser,s_currentusertype) {
		var filters = [
		//["custentity_account_delivery_owner","anyof",currentUser],
		[s_currentusertype,"anyof",currentUser],
		'and',
		[ [ 'job.status', 'anyof', '2' ], 'or', [ 'job.status', 'anyof', '4' ] ] ];

		var columns = [ new nlobjSearchColumn("entityid"),
		new nlobjSearchColumn("altname") ];

		var search = nlapiSearchRecord('customer', null, filters, columns);
		return search;
		}
		
		
	