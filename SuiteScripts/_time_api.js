/**
 * @NApiVersion 2.x
 * @NScriptType Restlet
 * @NModuleScope SameAccount
 */
 define(['N/record', 'N/search', 'N/runtime', 'N/format'],
 function (obj_Record, search, obj_Runtime, obj_Format) {
     function send_Time_data(obj_Request) {
         try {
             log.debug('requestBody ==', JSON.stringify(obj_Request));
             var obj_Usage_Context = obj_Runtime.getCurrentScript();
             var s_Request_Type = obj_Request.RequestType;
             log.debug('s_Request_Type ==', s_Request_Type);
             var s_Request_Data = obj_Request.RequestData;
             log.debug('s_Request_Data ==', s_Request_Data);
             var d_start_Date = obj_Request.start_date;
             d_start_Date = obj_Format.parse({ value: d_start_Date, type: obj_Format.Type.DATE });
             d_start_Date = obj_Format.format({ value: d_start_Date, type: obj_Format.Type.DATE });
             log.debug('d_start_Date ==', d_start_Date);
             var d_End_Date = obj_Request.end_date;
             d_End_Date = obj_Format.parse({ value: d_End_Date, type: obj_Format.Type.DATE });
             d_End_Date = obj_Format.format({ value: d_End_Date, type: obj_Format.Type.DATE });
             log.debug('d_End_Date ==', d_End_Date);
             if ((!s_Request_Type) || (s_Request_Type == null) || (s_Request_Data == null) || (!s_Request_Data) || (s_Request_Data != 'TIME')) {
                 var obj_Response_Return = new Response();
                 obj_Response_Return.Data = 'Please Enter Request Body';
                 obj_Response_Return.Status = false;

             } //if((!s_Request_Type) || (s_Request_Type == null) || (s_Request_Data == null) || (!s_Request_Data)|| (s_Request_Data!= 'INVOICE'))
             else {
                 var obj_Response_Return = new Response();
                 if ((!d_start_Date) || (!d_End_Date)) {
                     obj_Response_Return.Data = 'Please Enter Start Date and End date';
                     obj_Response_Return.Status = false;
                 } //// if((!d_start_Date)|| (!d_End_Date))
                 else {
                     obj_Response_Return.Status = true;
                     obj_Response_Return = fu_Send_Time_Report_Data(search, d_start_Date, d_End_Date);
                 }// else of  if((!d_start_Date)|| (!d_End_Date))

             } //else of if if((!s_Request_Type) || (s_Request_Type == null) || (s_Request_Data == null) || (!s_Request_Data)|| (s_Request_Data!= 'INVOICE'))

         } //// end of try
         catch (s_Exception) {
             var obj_Response_Return = new Response();
             log.debug('s_Exception in send_Invoice_data ==', s_Exception);
             obj_Response_Return.Data = s_Exception;
             obj_Response_Return.Status = false;
         } //// End of Catch
         log.debug('obj_Response_Return==', JSON.stringify(obj_Response_Return));
         log.debug('Final Remaining Usage  ==', obj_Usage_Context.getRemainingUsage());
         return obj_Response_Return;
     } //// End of function send_Time_data
     return {
         post: send_Time_data,
     };

 }); /// End of main Function 
/************************ Supporting Functions ***************************************************/
function fu_Send_Time_Report_Data(search, d_start_Date, d_End_Date) {
 try {
    var timebillSearchObj = search.create({
        type: "timebill",
        filters:
        [
           ["type","anyof","A"], 
           "AND", 
           ["date","within",d_start_Date,d_End_Date]
        ],
        columns:
        [
           search.createColumn({name: "date", label: "Date"}),
           search.createColumn({name: "employee", label: "Employee"}),
           search.createColumn({
              name: "firstname",
              join: "employee",
              label: "First Name"
           }),
           search.createColumn({
              name: "middlename",
              join: "employee",
              label: "Middle Name"
           }),
           search.createColumn({
              name: "lastname",
              join: "employee",
              label: "Last Name"
           }),
           search.createColumn({
              name: "custentity_empid",
              join: "employee",
              label: "Emp ID"
           }),
           search.createColumn({
              name: "custentity_incid",
              join: "employee",
              label: "Inc ID"
           }),
           search.createColumn({
              name: "customer",
              join: "job",
              label: "Customer"
           }),
           search.createColumn({
              name: "subsidiary",
              join: "customer",
              label: "Customer Subsidiary "
           }),
           search.createColumn({
              name: "entityid",
              join: "job",
              label: "Project ID"
           }),
           search.createColumn({
              name: "altname",
              join: "job",
              label: "Project Name"
           }),
           search.createColumn({
              name: "title",
              join: "projectTask",
              label: "Task"
           }),
           search.createColumn({name: "item", label: "Item"}),
           search.createColumn({name: "durationdecimal", label: "Duration"}),
           search.createColumn({name: "isbillable", label: "Billable"}),
           search.createColumn({name: "location", label: "Location"}),
           search.createColumn({name: "rate", label: "Rate"}),
           search.createColumn({
              name: "custcol_projectmanager",
              sort: search.Sort.ASC,
              label: "Project Manager"
           }),
           search.createColumn({name: "subsidiary", label: "Employee Subsidiary "}),
           search.createColumn({
              name: "custentity_endcustomer",
              join: "job",
              label: "End Customer"
           }),
           search.createColumn({
              name: "jobbillingtype",
              join: "job",
              label: "Billing Type"
           }),
           search.createColumn({
              name: "jobtype",
              join: "job",
              label: "Project Type"
           }),
           search.createColumn({name: "approvalstatus", label: "Approval Status"}),
           search.createColumn({name: "custcol_employeenamecolumn", label: "Employee Name"}),
           search.createColumn({name: "custcolprj_name", label: "Project Description"}),
           search.createColumn({
              name: "timeapprover",
              join: "employee",
              label: "Time Approver"
           }),
           search.createColumn({name: "status", label: "Status"}),
           search.createColumn({
              name: "custentity_persontype",
              join: "employee",
              label: "Person Type"
           }),
           search.createColumn({
              name: "employeetype",
              join: "employee",
              label: "Employee Type"
           }),
           search.createColumn({
              name: "department",
              join: "employee",
              label: "Practice"
           }),
           search.createColumn({name: "department", label: "Practice"}),
           search.createColumn({name: "custcol_territory_tb", label: "TE Territory"}),
           search.createColumn({name: "timesheet", label: "Timesheet"}),
           search.createColumn({name: "datecreated", label: "Date Created"})
        ]
     });
     var searchResultCount = timebillSearchObj.runPaged().count;
     log.debug("timebillSearchObj result count",searchResultCount);
     var myResults = getAllResults(timebillSearchObj);
     var arr_Time_json = [];
     myResults.forEach(function(result)  {
         var obj_json_Container = {}
         obj_json_Container = {
             "Date": result.getValue({ name: "date"}),
             "Employee": result.getValue({name: "employee"}),
             "Employee_First_Name" : result.getValue({name: "firstname",join: "employee"}),
             "Employee_Middle_Name" : result.getValue({name: "middlename",join: "employee"}),
             "Employee_Last_Name" : result.getValue({name: "lastname",join: "employee"}),
             "Employee_Id": result.getValue({name: "custentity_empid",join: "employee"}),
             "Employee_Inc_Id": result.getValue({name: "custentity_incid",join: "employee" }),
             "Customer": result.getValue({name: "customer",join: "job",}),
             "Customer_Subsidiary ": result.getValue({name: "subsidiary", join: "customer"}),
             "Project_Id": result.getValue({name: "entityid",join: "job"}),
             "Customer": result.getValue({ name: "custcolcustcol_temp_customer", label: "Customer" }),
             "Project_ID": result.getValue({ name: "custcol_project_entity_id", label: "Project ID" }),
             "Project_Name": result.getValue({name: "altname",join: "job"}),
             "Task" : result.getValue({name: "title", join: "projectTask"}),
             "Item": result.getValue({ name: "item"}),
             "Duration": result.getValue({ name: "durationdecimal"}),
             "Billable": result.getValue({ name: "isbillable" }),
             "Location": result.getValue({ name: "location"}),
             "Rate": result.getValue({ name: "rate"}),
             "Employee_Subsidiary":result.getValue({name: "subsidiary", label: "Employee Subsidiary "}),
             "End_Customer":result.getValue({
                name: "custentity_endcustomer",
                join: "job",
                label: "End Customer"
             }),
             "Billing_Type":result.getValue({
                name: "jobbillingtype",
                join: "job",
                label: "Billing Type"
             }),
             "Project_Type":result.getValue({
                name: "jobtype",
                join: "job",
                label: "Project Type"
             }),
             "Approval_Status":result.getText({name: "approvalstatus", label: "Approval Status"}),
             "Employee_Name":result.getValue({name: "custcol_employeenamecolumn", label: "Employee Name"}),
             "Project_Description":result.getValue({name: "custcolprj_name", label: "Project Description"}),
             "Time_Approver":result.getValue({
                name: "timeapprover",
                join: "employee",
                label: "Time Approver"
             }),
             "Status":result.getValue({name: "status", label: "Status"}),
             "Person_Type":result.getValue({
                name: "custentity_persontype",
                join: "employee",
                label: "Person Type"
             }),
             "Employee_Type":result.getValue({
                name: "employeetype",
                join: "employee",
                label: "Employee Type"
             }),
             "Employee_Practice":result.getValue({
                name: "department",
                join: "employee",
                label: "Practice"
             }),
             "Practice":result.getValue({name: "department", label: "Practice"}),
             "TE Territory":result.getValue({name: "custcol_territory_tb", label: "TE Territory"}),
             "Timesheet":result.getValue({name: "timesheet", label: "Timesheet"}),
             "Date_Created":result.getValue({name: "datecreated", label: "Date Created"})
         };
         arr_Time_json.push(obj_json_Container);
         return true;
     });
     return arr_Time_json;
 }
 catch (s_Exception) {
     log.debug('Exception in fu_Send_Time_Report_Data', s_Exception);
     return s_Exception;
 } //// End of catch
}///// function fu_Send_Time_Report_Data(search)
/**************************************************************************************************/
function Response() {
 this.Status = false;
 this.Data = "";

} //// function Response() 
function getAllResults(s) {
 var result = s.run();
 var searchResults = [];
 var searchid = 0;
 do {
     var resultslice = result.getRange({ start: searchid, end: searchid + 1000 });
     resultslice.forEach(function (slice) {
         searchResults.push(slice);
         searchid++;
     }
     );
 } while (resultslice.length >= 1000);
 return searchResults;
}
/************************ Supporting Functions ***************************************************/