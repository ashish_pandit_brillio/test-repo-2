/**
 * @author Shweta
 */
// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
/*
   	Script Name :CLI_Create_PO.js
	Author      : Shweta Chopde
	Date        : 14 April 2014
	Description : Validate the PR - Item slections


	Script Modification Log:

	-- Date --			-- Modified By --				--Requested By--				-- Description --




	Below is a summary of the process controls enforced by this script file.  The control logic is described
	more fully, below, in the appropriate function headers and code blocks.

     PAGE INIT
		- pageInit(type)


     SAVE RECORD
		- saveRecord()


     VALIDATE FIELD
		- validateField(type, name, linenum)


     FIELD CHANGED
		- fieldChanged(type, name, linenum)


     POST SOURCING
		- postSourcing(type, name)


	LINE INIT
		- lineInit(type)


     VALIDATE LINE
		- validateLine()


     RECALC
		- reCalc()


     SUB-FUNCTIONS
		- The following sub-functions are called by the above core functions in order to maintain code
            modularization:





*/
}
// END SCRIPT DESCRIPTION BLOCK  ====================================



// BEGIN SCRIPT UPDATION BLOCK  ====================================
/*


*/
// END SCRIPT UPDATION BLOCK  ====================================




// BEGIN GLOBAL VARIABLE BLOCK  =====================================
{

	//  Initialize any Global Variables, in particular, debugging variables...


}
// END GLOBAL VARIABLE BLOCK  =======================================





// BEGIN PAGE INIT ==================================================

function pageInit(type)
{
    /*  On page init:

	- PURPOSE

		FIELDS USED:

		--Field Name--				--ID--			--Line Item Name--

    */

    //  LOCAL VARIABLES


    //  PAGE INIT CODE BODY


}

// END PAGE INIT ====================================================





// BEGIN SAVE RECORD ================================================

function saveRecord()
{
    /*  On save record:

	- PURPOSE



	  FIELDS USED:

		--Field Name--			--ID--		--Line Item Name--


    */


    //  LOCAL VARIABLES



    //  SAVE RECORD CODE BODY


	return true;

}

// END SAVE RECORD ==================================================





// BEGIN VALIDATE FIELD =============================================

function validateField(type, name, linenum)
{

	/*  On validate field:

          - EXPLAIN THE PURPOSE OF THIS FUNCTION


          FIELDS USED:

          --Field Name--				--ID--


    */


	//  LOCAL VARIABLES



	//  VALIDATE FIELD CODE BODY


	return true;

}

// END VALIDATE FIELD ===============================================


var a_create_PO_array = new Array()
var a_vendor_array = new Array()
var a_do_not_create_PO_array = new Array()


// BEGIN FIELD CHANGED ==============================================
/**
 * 
 * @param {Object} type
 * @param {Object} name
 * @param {Object} linenum
 * 
 * Description --> On selection of Create PO get the selected Vendor & Quantity in an array
 */
function fieldChanged_create_PO(type, name, linenum)
{
	if(type =='recmachcustrecord_purchaserequest' && name == 'custrecord_select_create_po')
	{
	   var i_select_create_PO =  nlapiGetCurrentLineItemValue('recmachcustrecord_purchaserequest','custrecord_select_create_po')		
	   var i_SR_No =  nlapiGetCurrentLineItemValue('recmachcustrecord_purchaserequest','custrecord_prlineitemno')		
	   var i_index_of =  nlapiGetCurrentLineItemIndex('recmachcustrecord_purchaserequest')	
	   
	   nlapiSelectLineItem('recmachcustrecord_purchaserequest',i_index_of)
	   var i_quantity =  nlapiGetLineItemValue('recmachcustrecord_purchaserequest','custrecord_quantity',i_index_of)		
	   var i_vendor =  nlapiGetLineItemValue('recmachcustrecord_purchaserequest','custrecord_vendor_pr_item',i_index_of)		
	 	 
	 
	   if(i_select_create_PO == 'T')
	   {
	   	  a_create_PO_array.push(i_SR_No)  
		  
		  a_vendor_array.push(i_vendor)
	   }
	 /*
  else if(i_select_create_PO == 'F')
	   {	
	   	 a_create_PO_array.splice((parseInt(i_index_of)-1),1)
		 a_quantity_array.splice((parseInt(i_index_of)-1),1)
		 a_vendor_array.splice((parseInt(i_index_of)-1),1)
		 
	   }
*/
	   	
	}
		 
 return true;  
}

// END FIELD CHANGED ================================================





// BEGIN POST SOURCING ==============================================

function postSourcing(type, name)
{

    /*  On post sourcing:

	- PURPOSE

          FIELDS USED:

		--Field Name--			--ID--		--Line Item Name--


    */

    //  LOCAL VARIABLES


    //  POST SOURCING CODE BODY



}

// END POST SOURCING ================================================



// BEGIN LINE INIT ==============================================

function lineInit(type)
{

    /*  On Line Init:

	- PURPOSE

          FIELDS USED:

		--Field Name--			--ID--		--Line Item Name--


    */

    //  LOCAL VARIABLES


    //  LINE INIT CODE BODY



}

// END LINE INIT ================================================


// BEGIN VALIDATE LINE ==============================================

function validateLine(type)
{

	/*  On validate line:

          - EXPLAIN THE PURPOSE OF THIS FUNCTION


          FIELDS USED:

          --Field Name--				--ID--


    */


	//  LOCAL VARIABLES


	//  VALIDATE LINE CODE BODY


	return true;

}

// END VALIDATE LINE ================================================




// BEGIN RECALC =====================================================

function recalc(type)
{

	/*  On recalc:

          - EXPLAIN THE PURPOSE OF THIS FUNCTION


          FIELDS USED:

          --Field Name--				--ID--


    */


	//  LOCAL VARIABLES


	//  RECALC CODE BODY


}

// END RECALC =======================================================





// BEGIN FUNCTION ===================================================
/**
 * 
 * @param {Object} i_recordID
 * 
 * Description --> If the vendor array contains more than one vendor, 
 *                throw an alert ÃƒÂ¢Ã¢â€šÂ¬Ã…â€œPlease select only single vendor. \n You are not allowed to select multiple vendors while creating Purchase Order.ÃƒÂ¢Ã¢â€šÂ¬Ã¯Â¿Â½ 
 *                & reload the page again. Call the suitlet & get the Purchase Order as a response.
	              Open the new window for Purchase Order
*/
function create_PO(i_recordID)
{
	var cnt = 0
	var s_data_serial=''
	var finalValues = new Array()
	var quant_array = new Array()
	var a_vendor_array =  new Array()
	var a_create_PO_array = new Array()
	var flag = 0;
	if (_logValidation(i_recordID))
	{	 
	 var i_line_count = nlapiGetLineItemCount('recmachcustrecord_purchaserequest')
			
	if (_logValidation(i_line_count)) 
	{
		for (var yt = 1; yt <= i_line_count; yt++) 
		{
			var i_select_create_PO = nlapiGetLineItemValue('recmachcustrecord_purchaserequest', 'custrecord_select_create_po', yt)
			
			var i_quant = nlapiGetLineItemValue('recmachcustrecord_purchaserequest', 'custrecord_quantity', yt)
			
			var i_SR_No = nlapiGetLineItemValue('recmachcustrecord_purchaserequest', 'custrecord_prlineitemno', yt)
			
			var i_vendor =  nlapiGetLineItemValue('recmachcustrecord_purchaserequest','custrecord_vendor_pr_item',yt)		
	 			
				
					
			   if(i_select_create_PO == 'T')
			   {
			   	  a_create_PO_array.push(i_SR_No)  
				  
				  a_vendor_array.push(i_vendor)
				  
				  quant_array.push(i_quant)
			   }
			
		}
	}
	
	     var vendor_n =  a_vendor_array[0]
		 
		 var vendor_length = a_vendor_array.length
		 
		 for(var bt= 0;bt<a_vendor_array.length;bt++)
		 {		 	
		 	if(a_vendor_array[bt]!=vendor_n)
			{
				flag = 1;
				break;
			}
		 }		 
		
		   if (vendor_length != null && vendor_length != undefined && vendor_length != '' && vendor_length!= 0)
		   {		  
		   	if (flag == 1) 
			{
		   		alert(' Please select only single vendor . \n You are not allowed to select multiple vendors while creating Purchase Order.')
		   		location.reload();
              return false;
		   	}
		   }
	
	     /*
if(a_vendor_array.length!=null && a_vendor_array.length!=undefined && a_vendor_array.length!='' &&a_vendor_array.length!=0)
		 { 
		    if(a_vendor_array.length!=1)
			{
				 alert(' Please select only single vendor . \n You are not allowed to select multiple vendors while creating Purchase Order.')
			     location.reload();	
				return false;
			}
		 }
*/
      
		 if(a_create_PO_array.length == 0 || a_create_PO_array == null || a_create_PO_array == '' || a_create_PO_array == undefined)
		 {
		 	alert(' Please select Create PO for creating Purchase Order.')
			location.reload();
           	return false;
		 }
		
		
		
if(quant_array.indexOf('0')>-1)
		{
			alert(' You are not allowed to create Purchase Order for zero Quantity.')
		    location.reload();	
			return false;			
		} 

		


		   var a = new Array();
           a['User-Agent-x'] = 'SuiteScript-Call';
		  
		  var resposeObject = nlapiRequestURL('/app/site/hosting/scriptlet.nl?script=119&deploy=1&custscript_record_id_c_po=' + i_recordID+'&custscript_create_po_array='+a_create_PO_array+'&custscript_q_array='+quant_array, null, a);
       	  
		  var a_PO_ID_array = resposeObject.getBody();
		 
		  for(var po=0;po<a_PO_ID_array.length;po++)
		  {
			 	finalValues = a_PO_ID_array.split(',')
			    break;				
		  }
				
		 for(var po=0;po<finalValues.length;po++)
		{		
		   window.open('/app/accounting/transactions/purchord.nl?id='+finalValues[po]+'&e=T&whence=')			
		}		
		  location.reload();	
		 
	}	
}//Create PO
/**
 * 
 * @param {Object} value
 * 
 * Description --> If the value is blank /null/undefined returns false else returns true
 */
function _logValidation(value) 
{
 if(value != null && value.toString() != null && value != '' && value != undefined && value.toString() != undefined && value != 'undefined' && value.toString() != 'undefined'&& value.toString() != 'NaN' && value != NaN) 
 {
  return true;
 }
 else 
 { 
  return false;
 }
}
/**
 * 
 * @param {Object} array
 * 
 * Description --> Returns the unique elements of the array
 */
function removearrayduplicate(array)
{
   var newArray = new Array();
   label:for(var i=0; i<array.length;i++ )
     {  
     for(var j=0; j<array.length;j++ )
      {
      if(newArray[j]==array[i]) 
      continue label;
     }
     newArray[newArray.length] = array[i];
      }
   return newArray;
}

// END FUNCTION =====================================================
