/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       03 Apr 2015     amol.sahijwani
 *
 */

/**
 * @param {String} type Context Types: scheduled, ondemand, userinterface, aborted, skipped
 * @returns {Void}
 */
function scheduled(type) {
var rec	=	nlapiCreateRecord('customrecord_rev_rep_data_update_log');

nlapiSubmitRecord(rec);
}
