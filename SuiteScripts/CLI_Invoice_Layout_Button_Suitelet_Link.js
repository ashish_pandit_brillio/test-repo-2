// BEGIN SCRIPT DESCRIPTION BLOCK ==================================
/*
   Date		   : 24/11/2021
   Author		: Raghav Gupta
   Remarks		: Client script to open suitelet that generates Invoice PDF when button is clicked.
   Script Name	: CLI_Invoice_Layout_Button_Suitelet_Link.js
   Script Type	: Client Script (1.0)

   Script Modification Log:s
   -- Date --			-- Modified By --				--Requested By--				-- Description --

   Function used and their Descriptions:

   open_Invoice FUNCTION         : Function to generate the PDF for Invoices based on layout types.
   open_BLLC_Comity_TM FUNCTION  : Function to generate comity T&M PDF.     Note: Didn't merge it with the other layouts because its not in use anymore.
   open_BLLC_Comity_FP FUNCTION  : Function to generate comity FP PDF.      Note: Didn't merge it with the other layouts because its not in use anymore.
   open_BTPL_Invoice             : Function to generate BTPL PDF for Invoices having BTPL subsidary.
*/
// END SCRIPT DESCRIPTION BLOCK ==================================

// BEGIN open_Invoice
function open_Invoice() {
   try {
      var i_recordID = nlapiGetRecordId();
      window.open('/app/site/hosting/scriptlet.nl?script=2295&deploy=1&i_recordID=' + i_recordID)
   }
   catch (error) {
      nlapiLogExecution('ERROR', 'Invoice PDF Generation Error', error);
   }
}
// END open_Invoice

// BEGIN open_BLLC_Comity_TM
function open_BLLC_Comity_TM() {
   try {
      var i_recordID = nlapiGetRecordId();
      window.open('/app/site/hosting/scriptlet.nl?script=1636&deploy=1&i_recordID=' + i_recordID)
   }
   catch (error) {
      nlapiLogExecution('ERROR', 'Comity T&M Invoice PDF Generation Error', error);
   }

}
// END open_BLLC_Comity_TM

// BEGIN open_BLLC_Comity_FP
function open_BLLC_Comity_FP() {
   try {
      var i_recordID = nlapiGetRecordId();
      window.open('/app/site/hosting/scriptlet.nl?script=1637&deploy=1&i_recordID=' + i_recordID)
   }
   catch (error) {
      nlapiLogExecution('ERROR', 'Comity FP Invoice PDF Generation Error', error);
   }

}
// END open_BLLC_Comity_FP

// BEGIN open_BTPL_Invoice
function open_BTPL_Invoice(internalid) {
   try {
      window.open("/app/site/hosting/scriptlet.nl?script=1135&deploy=1&id=" + internalid);
   }
   catch (error) {
      nlapiLogExecution('ERROR', 'BTPL PDF Generation Error', error);
   }
}
// END open_BTPL_Invoice