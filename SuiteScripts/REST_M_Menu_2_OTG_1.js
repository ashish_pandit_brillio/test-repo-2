/**
 * Menu RESTlet
 * 
 * Version    Date            Author           Remarks
 * 1.00       25 Nov 2015     nitish.mishra
 *
 */

/**
 * @param {Object}
 *            dataIn Parameter object
 * @returns {Object} Output object
 */
var monthArray = [' ','Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];

function postRESTlet(dataIn) {
	var response = new Response();
	var current_date = nlapiDateToString(new Date());
	//Log for current date
	nlapiLogExecution('DEBUG', 'Current Date', 'current_date...' + current_date)
	
	//nlapiLogExecution('debug', 'datain', dataIn);
	
	nlapiLogExecution('debug', 'dataIn', JSON.stringify(dataIn));
	nlapiLogExecution('debug', 'Email id', dataIn.EmailId);
	//var emailId  = 'rahul.murali@brillio.com';
	var response = new Response();
	var currentDate_ = nlapiStringToDate(current_date);
	var currentDate = currentDate_.getDate();
	var currentMonth = currentDate_.getMonth();
	var currentYear = currentDate_.getFullYear();
	nlapiLogExecution('debug', 'currentYear', currentYear);
	try {
	//	var employeeId = getUserUsingEmailId(emailId);
	
		var employeeId = getUserUsingEmailId(dataIn.EmailId);
		var flag_R = false;
		var flag_projectManager = false;
		//Search If logged user is reporting manager or not
		var filter = [];
		filter[0] = new nlobjSearchFilter('custentity_reportingmanager',null,'anyof',employeeId); //custentity_employee_inactive
		filter[1] = new nlobjSearchFilter('custentity_employee_inactive',null,'is','F');
		
		var col = [];
		col[0] = new nlobjSearchColumn('internalid');
		col[1] = new nlobjSearchColumn('entityid');
		col[2] = new nlobjSearchColumn('firstname');
		
		var reportingManager_Res = nlapiSearchRecord('employee',null,filter,col);
		if(reportingManager_Res){
			nlapiLogExecution('debug', 'Reportee Count', reportingManager_Res.length);
			if(reportingManager_Res.length > 0){
				flag_R = true;
			}
		}
		//Check if the logged in user is PM or not --- CODEX
		var statusList = [2,4]; //Active && Pending
		var filter_P = [];
		filter_P[0] = new nlobjSearchFilter('custentity_projectmanager',null,'anyof',employeeId); //custentity_employee_inactive
		filter_P[1] = new nlobjSearchFilter('status',null,'anyof',statusList); //jobtype
		filter_P[2] = new nlobjSearchFilter('jobtype',null,'anyof',2); 
		filter_P[3] = new nlobjSearchFilter('isinactive', null, 'is', 'F');
		
		var col_P = [];
		col_P[0] = new nlobjSearchColumn('internalid');
		col_P[1] = new nlobjSearchColumn('entityid');
		col_P[2] = new nlobjSearchColumn('custentity_projectmanager');
		
		
		var project_Manager_Res = nlapiSearchRecord('job',null,filter_P,col_P);
		if(project_Manager_Res){
			nlapiLogExecution('debug', 'PM Flag Count', project_Manager_Res.length);
			if(project_Manager_Res.length > 0){
				flag_projectManager = true;
			}
		}
		
		var emp_lookUp_Obj = nlapiLookupField('employee',parseInt(employeeId),['subsidiary','department','hiredate','entityid']);
		var i_subsidiary_V = emp_lookUp_Obj.subsidiary;
		var i_department_V = emp_lookUp_Obj.department;
		var i_hiredate_V = emp_lookUp_Obj.hiredate;
		var logged_user = emp_lookUp_Obj.entityid;
		nlapiLogExecution('debug','subsidiary',i_subsidiary_V);
		
		//Hiredate Json
		var JSON_hireDate = {};
		var dataRow_HireDate = [];
		
		JSON_hireDate = {
				LoggedUser : logged_user,
				HireDate : i_hiredate_V
		};
		dataRow_HireDate.push(JSON_hireDate);
		//Get Holidays List
		/*var holiday_list_ = new Array();
		var s_startDate = '1/1/'+currentYear;
		var s_endDate = '12/31/'+currentYear;
		nlapiLogExecution('debug', 's_startDate', s_startDate);
		nlapiLogExecution('debug', 's_endDate', s_endDate);
		var filters = [];
		filters.push(new nlobjSearchFilter('custrecordsubsidiary',null,'anyof',i_subsidiary_V));
		filters.push(new nlobjSearchFilter('custrecord_date',null,'within',s_startDate,s_endDate));
		filters.push(new nlobjSearchFilter('custrecord_date',null,'onorafter','today'));
		filters.push( new nlobjSearchFilter('isinactive', null, 'is', 'F'));
		
				
		var cols = [];
		cols.push(new nlobjSearchColumn('custrecord_date').setSort());
		cols.push(new nlobjSearchColumn('custrecord_holiday_description'));
		cols.push(new nlobjSearchColumn('internalid'));*/
		
		var employeeLocation = nlapiLookupField('employee', parseInt(employeeId),
        'location');

		var locationArray = [ employeeLocation ];
		var currentLocation = employeeLocation;

		while (currentLocation) {
		var locationRec = nlapiLoadRecord('location', currentLocation);
		currentLocation = locationRec.getFieldValue('parent');
	
		if (currentLocation) {
			locationArray.push(currentLocation);
		}
	}

		nlapiLogExecution('debug', 'location array', JSON
        .stringify(locationArray));
		
		var dataRow_Holiday = [];
		var holiday_list_ = [];
		var JSON_Holiday = {};
		var counter_H = 0;
		//var searchHolidayList = nlapiSearchRecord('customrecord_holiday',null,filters,cols);
		
		var searchHolidayList = nlapiSearchRecord(
		        'customrecord_mobile_holiday_calendar', null, [
		                new nlobjSearchFilter('custrecord_mhc_location', null,
		                        'anyof', locationArray),
		                new nlobjSearchFilter('custrecord_mhc_date', null,
		                        'onorafter', 'startofthisyear'),
		                new nlobjSearchFilter('custrecord_mhc_date', null,
		                        'onorbefore', 'nextfiscalyear'),
		                new nlobjSearchFilter('isinactive', null, 'is', 'F') ],
		        [ new nlobjSearchColumn('custrecord_mhc_date').setSort(),
		                new nlobjSearchColumn('custrecord_mhc_title'),
		                new nlobjSearchColumn('custrecord_mhc_image') ]);
		
		if(searchHolidayList){
			for(var indx=0; indx < searchHolidayList.length; indx++){
				var title = searchHolidayList[indx].getValue('custrecord_mhc_title');
				var holiday_date_ = searchHolidayList[indx].getValue('custrecord_mhc_date');
				var holiday_date = nlapiStringToDate(holiday_date_);
				var h_date = holiday_date.getDate();
				var h_month = holiday_date.getMonth() + 1;
				h_month = monthArray[h_month];
				var h_year = holiday_date.getFullYear();
				//if(counter_H < 3 && holiday_list_.indexOf(title)<0)
				{ //currentMonth >=b_month &&
					counter_H ++;
					JSON_Holiday = {
							Holiday_Desc : searchHolidayList[indx].getValue('custrecord_mhc_title'),
							Holiday_Date : h_date,
							IN:'',
							Holiday_Month: h_month,
							Holiday_Year: h_year,
							HolidayDate: holiday_date_
					};
					holiday_list_.push(searchHolidayList[indx].getValue('custrecord_mhc_title'));
					dataRow_Holiday.push(JSON_Holiday);
				}
			}
		}
		//Removing Duplicates from Holiday
		//dataRow_Holiday = removearrayduplicate(dataRow_Holiday);
		if (employeeId) {
			
			//var employeeLookUp = nlapiLookupField('employee',employeeId,'department');
			//var i_department = employeeLookUp.department;
			
			
			//Get Birthday's
			var filters = [];
			filters.push(new nlobjSearchFilter('department',null,'anyof',i_department_V));
			filters.push(new nlobjSearchFilter('custentity_employee_inactive',null,'is','F'));
			//filters.push(new nlobjSearchFilter('birthdate',null,'onorafter','today'));
			
			var cols = [];
			cols.push(new nlobjSearchColumn('birthdate').setSort(true));
			cols.push(new nlobjSearchColumn('department'));
			cols.push(new nlobjSearchColumn('firstname'));
			cols.push(new nlobjSearchColumn('middlename'));
			cols.push(new nlobjSearchColumn('lastname'));
			cols.push(new nlobjSearchColumn('email'));
			cols.push(new nlobjSearchColumn('phone'));
			
			var dataRow_Bday = [];
			var JSON_Bday = {};
			var sort_JSON ={};
			var sort_Array = [];
			var counter_Bday = 0;
			var i_flagg = 0;
			var bday_list_ = new Array();
			//var searchEmployeeBthDay = nlapiSearchRecord('employee','customsearch1747',filters); //Sandbox
			var searchEmployeeBthDay = nlapiSearchRecord('employee','customsearch_employee_search_bday',filters);  //Prod
			
			if(searchEmployeeBthDay){
				for(var i=0;i<searchEmployeeBthDay.length;i++){
					var cols_R = searchEmployeeBthDay[i].getAllColumns();
					var s_resource = searchEmployeeBthDay[i].getValue(cols_R[0]);
					//var s_resource = searchEmployeeBthDay[i].getValue(cols_R[0]);
					var birthdate = searchEmployeeBthDay[i].getValue(cols_R[1]);
					var d_birthdate = nlapiStringToDate(birthdate);
					
					//var birthdate = searchEmployeeBthDay[i].getValue('email');
					
					
					if(d_birthdate && d_birthdate < currentDate_){
					var s_birthday = nlapiAddMonths(d_birthdate,12);
					s_birthday = nlapiDateToString(s_birthday);
				//	nlapiLogExecution('Debug','s_birthday',s_birthday);
					}
					else{
					var s_birthday = searchEmployeeBthDay[i].getValue(cols_R[1]);
					}
					if(birthdate){
					birthdate = nlapiStringToDate(birthdate);
				
					var b_date = birthdate.getDate();
					var b_month = birthdate.getMonth() + 1;
					var b_year = birthdate.getFullYear();
					var b_month_T = monthArray[b_month];
					var s_append_date = b_month + '/' + b_date + '/' + currentYear;
				//	var b_year = birthdate.getFullYear();
					
					var d_append_date = nlapiStringToDate(s_append_date);
					

				//	nlapiLogExecution('Debug','d_append_date',d_append_date);
					var diffDays = 0;
					if(d_append_date){
					//Logic To Sort Dates
					var timeDiff =  d_append_date.getTime() - new Date().getTime();
					diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
					}
					//var dateDiff = d_append_date - currentDate_ ; 
					sort_JSON = {
							Value: diffDays,
							Date : s_birthday,
							Month: b_month_T,
							BirthDate:b_date,	 
							Email: searchEmployeeBthDay[i].getValue(cols_R[3]),
							Name: searchEmployeeBthDay[i].getValue(cols_R[0]),
							Phone: searchEmployeeBthDay[i].getValue(cols_R[2]),
							IN: '',
							ImageURL :'img/image_10.jpg'
					};
					dataRow_Bday.push(sort_JSON);
					/*sort_JSON = {
							Name:searchEmployeeBthDay[i].getValue('firstname') + ' '+searchEmployeeBthDay[i].getValue('middlename')+' '+searchEmployeeBthDay[i].getValue('lastname') ,
							Email:searchEmployeeBthDay[i].getValue('email'),
							Phone:searchEmployeeBthDay[i].getValue('phone'),
							ImageURL :'img/image_10.jpg',
							Date:b_date,
							Month:b_month
					};
					sort_Array.push(sort_Array);*/
					//if( counter_Bday < 3 && b_month >= currentMonth && b_year >= currentYear && b_date >= currentDate)
					/*if( b_month >= currentMonth && b_year >= currentYear && b_date >= currentDate )
					{
						counter_Bday ++;
						JSON_Bday = {
								Name: searchEmployeeBthDay[i].getValue(cols_R[0]),
								BirthDate:b_date,
								Month: b_month_T,
								Email: searchEmployeeBthDay[i].getValue(cols_R[3]),
								Phone: searchEmployeeBthDay[i].getValue(cols_R[2]),
								IN: '',
								ImageURL :'img/image_10.jpg'
						};
						bday_list_.push(searchEmployeeBthDay[i].getId());
						dataRow_Bday.push(JSON_Bday);
					}*/
				}
			}
		}
			//Sorting BDays
			// Sort by price high to low
			dataRow_Bday.sort(predicateBy("Value"));
			//dataRow_Bday.push(bday_list_);
			//bday_list_.sort(sort_by('Value', true, parseInt));
			//My Profile
			var JSON_my_profile = {};
			var dataRow_myProfile = [];
			var emp_profile_Obj = nlapiLookupField('employee',parseInt(employeeId),['subsidiary','department','title','entityid','gender','birthdate']);
			var emp_profile_Obj_T = nlapiLookupField('employee',parseInt(employeeId),['subsidiary'],true);
			var emp_profile_Obj_I = nlapiLookupField('employee',parseInt(employeeId),['subsidiary']);
			JSON_my_profile = {
					Name: emp_profile_Obj.entityid,
					Subsidiary: emp_profile_Obj_T.subsidiary,
					SubsidiaryIndex: emp_profile_Obj_I.subsidiary,
					Designation: emp_profile_Obj.title,
					Gender: emp_profile_Obj.gender,
					BirthDate: emp_profile_Obj.birthdate,
					imagepath: 'img/image_10.jpg'
			}
			dataRow_myProfile.push(JSON_my_profile);
			//New Joinee's
			var filters = [];
			filters.push(new nlobjSearchFilter('department',null,'is',i_department_V));
			filters.push(new nlobjSearchFilter('custentity_employee_inactive',null,'is','F'));
			//filters.push(new nlobjSearchFilter('birthdate',null,'onorafter','today'));
			
			var cols = [];
			cols.push(new nlobjSearchColumn('hiredate').setSort(true));
			cols.push(new nlobjSearchColumn('department'));
			cols.push(new nlobjSearchColumn('firstname'));
			cols.push(new nlobjSearchColumn('lastname'));
			
			var dataRow_Joinee = [];
			var JSON_Joinee = {};
			var counter_Joinee = 0;
			var searchEmployee_Joinee = nlapiSearchRecord('employee',null,filters,cols);
			if(searchEmployee_Joinee){
				for(var i=0;i<searchEmployee_Joinee.length;i++){
					var s_resource = searchEmployee_Joinee[i].getValue('firstname');
					var hiredate = searchEmployee_Joinee[i].getValue('hiredate');
					if(hiredate){
						hiredate = nlapiStringToDate(hiredate);
				
					var b_date = hiredate.getDate();
					var b_month = hiredate.getMonth()+1;
					//if( counter_Joinee < 3 )
					{
						counter_Joinee ++;
						JSON_Joinee = {
								Name:searchEmployee_Joinee[i].getValue('firstname')+' '+searchEmployee_Joinee[i].getValue('lastname'),
								HireDate:searchEmployee_Joinee[i].getValue('hiredate'),
								IN: '',
								ImageURL :'img/image_10.jpg'
						};
						dataRow_Joinee.push(JSON_Joinee);
					}
				}
			}
		}
		
			//Expense Raised By Logged User
			var filters = [];
			filters.push(new nlobjSearchFilter('entity',null,'anyof',employeeId));
			//filters.push(new nlobjSearchFilter('birthdate',null,'onorafter','today'));
			
			var cols = [];
			cols.push(new nlobjSearchColumn('datecreated').setSort());
			cols.push(new nlobjSearchColumn('status'));
			cols.push(new nlobjSearchColumn('total'));
		
			
		/*	var pendingExpenseSearch = nlapiSearchRecord('transaction',
			        'customsearch1773',filters,cols);      */               //Sandbox
			
			var pendingExpenseSearch = nlapiSearchRecord('transaction',
			        'customsearch_expense_rasied_by_emp',filters,cols);  //Prod

			var pendingExpenseCount = 0;
			var JSON_expense_Mobile = {};
			var dataRow_Mobile = [];
			var totalExpensePendingAmount = 0;
			if (pendingExpenseSearch) {
				pendingExpenseID = pendingExpenseSearch[0].getId();
				
				var loadExpense = nlapiLoadRecord('expensereport',parseInt(pendingExpenseID));
				var employee = loadExpense.getFieldText('entity');
				var expense_createdDate = loadExpense.getFieldValue('createddate');
				var expense_id = loadExpense.getFieldValue('tranid');
				var expense_total = loadExpense.getFieldValue('total');
				var expense_status = loadExpense.getFieldValue('status');
				var lineCount = loadExpense.getLineItemCount('expense');
				for(var inx = 1 ; inx <=1; inx++){
					var project = loadExpense.getLineItemValue('expense','custcolprj_name',inx);
				}
				JSON_expense_Mobile = {
						Employee: employee,
						DateRaised: expense_createdDate,
						ExpenseID: expense_id,
						Amount:expense_total,
						Project: project,
						Status: expense_status
						
				};
				dataRow_Mobile.push(JSON_expense_Mobile);
			}
			// Timesheet
			var pendingTimesheetSearch = nlapiSearchRecord(
			        'customrecord_ts_quick_approval_pool',
			        'customsearch_m_pending_timesheet',
			        [ new nlobjSearchFilter('timeapprover',
			                'custrecord_qap_employee', 'anyof', employeeId) ]);

			var pendingTimesheetCount = 0;
			if (pendingTimesheetSearch) {
				pendingTimesheetCount = pendingTimesheetSearch[0].getValue(
				        'internalid', null, 'count');
			}

			// Expenses
			var pendingExpenseSearch = nlapiSearchRecord('expensereport',
			        'customsearch_m_pending_expenses', [ new nlobjSearchFilter(
			                'custbody1stlevelapprover', null, 'anyof',
			                employeeId) ]);

			var pendingExpenseCount = 0;
			var totalExpensePendingAmount = 0;
			if (pendingExpenseSearch) {
				pendingExpenseCount = pendingExpenseSearch[0].getValue(
				        'internalid', null, 'count');
				totalExpensePendingAmount = pendingExpenseSearch[0].getValue(
				        'netamount', null, 'sum');
			}

			// PR Item
			
			  var pendingPRSearch = nlapiSearchRecord('customrecord_pritem',
			  null, [ [ [ 'custrecord_prastatus',
			  'anyof', '2' ], 'and', [ 'custrecord_subpracticehead', 'anyof',
			  employeeId ] ], 'or', [ [ 'custrecord_prastatus', 'anyof', '30' ],
			  'and', [ 'custrecord_pri_delivery_manager', 'anyof', employeeId ] ]
			  ],[new nlobjSearchColumn('internalid',null,'count')]);
			  
			  var pendingPrCount = 0; if (pendingPRSearch) { pendingPrCount =
			  pendingPRSearch[0].getValue('internalid', null, 'count'); }
			 nlapiLogExecution('DEBUG','pendingPrCount',pendingPrCount);
			//Enter Expense
			var enterExpenseCount = '';
			
			var emp_lookUP = nlapiLookupField('employee',employeeId,['subsidiary']);
			var subsidiary_V = emp_lookUP.subsidiary;
			nlapiLogExecution('debug','subsidiary',subsidiary_V);
			if(parseInt(subsidiary_V) == 2){
			enterExpenseCount = '$';
			}
			else if(parseInt(subsidiary_V) == 3){
			enterExpenseCount = 'â‚¹';
			}
			nlapiLogExecution('debug','symbol',enterExpenseCount);

			// travel
			var pendingTravelSearch = nlapiSearchRecord(
			        'customrecord_travel_request',
			        'customsearch_m_pending_travel', [ new nlobjSearchFilter(
			                'custrecord_tr_approving_manager_id', null, 'is',
			                employeeId) ]);
			var pendingTravelCount = 0;
			if (pendingTravelSearch) {
				pendingTravelCount = pendingTravelSearch[0].getValue(
				        'internalid', null, 'count');
			}
			
				      
			

			// reportee count
			var reporteeSearch = nlapiSearchRecord('employee', null, [
			        new nlobjSearchFilter('custentity_implementationteam',
			                null, 'is', 'F'),
			        new nlobjSearchFilter('custentity_employee_inactive', null,
			                'is', 'F'),
			        new nlobjSearchFilter('custentity_reportingmanager', null,
			                'anyof', employeeId) ], [ new nlobjSearchColumn(
			        'internalid', null, 'count') ]);

			var reporteeCount = 0;
			if (reporteeSearch) {
				reporteeCount = reporteeSearch[0].getValue('internalid', null,
				        'count');
			}

			// get count of active allocation
			var allocationSearch = nlapiSearchRecord('resourceallocation',
			        null, [
			                new nlobjSearchFilter('resource', null, 'anyof',
			                        employeeId),
			                new nlobjSearchFilter('enddate', null, 'notbefore',
			                        'today'),
			                new nlobjSearchFilter('startdate', null,
			                        'notafter', 'today') ],
			        [ new nlobjSearchColumn('internalid', null, 'count') ]);

			var allocationCount = 0;
			if (allocationSearch) {
				allocationCount = allocationSearch[0].getValue('internalid',
				        null, 'count');
			}

			// get project count
			var projectSearch = nlapiSearchRecord('job', null,
			        [
			                [ 'isinactive', 'is', 'F' ],
			                'and',
			                [ [ 'status', 'anyof', 2 ], 'or',
			                        [ 'status', 'anyof', 4 ] ],
			                'and',
			                [
			                        [ 'custentity_projectmanager', 'anyof',
			                                employeeId ],
			                        'or',
			                        [ 'custentity_deliverymanager', 'anyof',
			                                employeeId ] ] ],
			        [ new nlobjSearchColumn('internalid', null, 'count') ]);

			var projectCount = 0;
			var pendingLeaveCount = 0;
			var pendingPRCount = 0;
			var hasProjectListAccess = false;
			if (projectSearch) {
				projectCount = projectSearch[0].getValue('internalid', null,
				        'count');
				hasProjectListAccess = true;
			}

			// Holiday Calendar (Get the next holiday)
			var employeeLocation = nlapiLookupField('employee', employeeId,
			        'location');

			var locationArray = [ employeeLocation ];
			var currentLocation = employeeLocation;

			while (currentLocation) {
				var locationRec = nlapiLoadRecord('location', currentLocation);
				currentLocation = locationRec.getFieldValue('parent');

				if (currentLocation) {
					locationArray.push(currentLocation);
				}
			}

			var searchHoliday = nlapiSearchRecord(
			        'customrecord_mobile_holiday_calendar',
			        null,
			        [
			                new nlobjSearchFilter('custrecord_mhc_location',
			                        null, 'anyof', locationArray),
			                new nlobjSearchFilter('custrecord_mhc_date', null,
			                        'onorafter', 'today'),
			                new nlobjSearchFilter('isinactive', null, 'is', 'F') ],
			        [ new nlobjSearchColumn('custrecord_mhc_date').setSort(),
			                new nlobjSearchColumn('custrecord_mhc_title') ]);

			var nextHolidayDate = "";
			var nextHolidayTitle = "";
			var pendingHolidayCount = 0;
			if (searchHoliday) {
				pendingHolidayCount = searchHoliday.length;
				nextHolidayDate = searchHoliday[0]
				        .getValue('custrecord_mhc_date');
				nextHolidayTitle = searchHoliday[0]
				        .getValue('custrecord_mhc_title');
			}
			var first_JSON = {};
			var first_Array = [];
			var second_JSON = {};
			var second_Array = [];
			var final_Array = [];
			first_JSON = {
         			MyProfile: dataRow_myProfile,
         			PeerBirthday: dataRow_Bday,
         			HolidayList: dataRow_Holiday,
         			NewJoinees: dataRow_Joinee,
         			HireDate: dataRow_HireDate,
         			ExpenseRaised: dataRow_Mobile
			};
			first_Array.push(first_JSON);
			second_JSON = {
					//Leave Approval
					 PendingLeave: new AccessAllowed(pendingLeaveCount, true, "",
					                'img/Leave.svg', 'Leave',
					                '/leaveApproval') ,
					PendingTimeSheet: new AccessAllowed(pendingTimesheetCount, true, "",
			                'img/Timesheet.svg', 'Timesheet',
			                 '/timesheet'),
			        PendingExpense: new AccessAllowed(pendingExpenseCount, true,
			                totalExpensePendingAmount,
			                'img/Expense.svg', 'Expense',
			                '/expense'),
			        
			        PendingTravel: new AccessAllowed(pendingTravelCount, true, "",
			                'img/Travel.svg', 'Travel',
			                 '/trapproval'),
							 
					
					
			};
			//Check if logged user is reporting manager
			if(flag_R == true){
				
			second_Array.push(//Leave Approval
					  new AccessAllowed(pendingLeaveCount, true, "",
				                'img/Leave.svg', 'Leave',
				                '/leaveApproval'),
				      new AccessAllowed(pendingTimesheetCount, true, "",
			                'img/Timesheet.svg', 'Timesheet',
		            '/timesheet'),
		            new AccessAllowed(pendingExpenseCount, true,
		           totalExpensePendingAmount,
		           'img/Expense.svg', 'Expense',
		           '/expense'),
		   
		           new AccessAllowed(pendingTravelCount, true, "",
		           'img/Travel.svg', 'Travel',
		            '/trapproval'),
				   new AccessAllowed(pendingPrCount, true, "",
						                'img/PR.svg', 'PR',
						                 '/trapproval'),
				   new AccessAllowed('', true, "",
									                '', '',
									                 '')	  
			 );
			}
			var final_JSON = {};
			// response
			response.Data = [
			                 	final_JSON = {
			                 			CurrentDate: current_date,
			                 			ProjectManagerFlag: flag_projectManager,
			                 			RequestJSON: first_Array,
			                 			PendingApproval: second_Array
			                 	}
			                	
			                 			
			                 			
			                 			
			                 	
		];
			        /* //Peer Birthday
			         new AccessAllowed(dataRow_Bday, true, "",
			                'img/timesheet-approval.png', 'Peer Birthday',
			                'PeerBirthday', 'Birthday', '/birthday'),
			         
			                //Holiday List
			                
			         new AccessAllowed(dataRow_Holiday, true, "",
					                'img/timesheet-approval.png', 'Holiday List',
					                'HolidayList', 'Holidays', '/holidays'),
					                
					 //New Joinee's
					  new AccessAllowed(dataRow_Joinee, true, "",
							                'img/timesheet-approval.png', 'Joinee List',
							                'JoineesList', 'Joinee', '/joinees'), 
					//WorkAnniversary i_hiredate_V
					if(i_hiredate_V)
						i_hiredate_V = i_hiredate_V;
						else
							i_hiredate_V = '';
			         
				    new AccessAllowed(i_hiredate_V, true, "",
									                'img/timesheet-approval.png', 'LoggedUser HireDate',
									                'WorkAnniversary', 'Anniversary', '/anniversary'), 		                
			                
			        new AccessAllowed(pendingTimesheetCount, true, "",
			                'img/timesheet-approval.png', 'Timesheet Approval',
			                'TimesheetApproval', 'Pending', '/timesheet'),
			        new AccessAllowed(pendingExpenseCount, true,
			                totalExpensePendingAmount,
			                'img/expense-approval.png', 'Expense Approval',
			                'ExpenseApproval', 'Pending', '/expense'),
			        // new AccessAllowed(pendingPrCount, true, "",
			        // 'img/Procurement-approval.png', 'PR Approval',
			        // 'ProcurementApproval', 'Pending', '/prapproval'),
			        new AccessAllowed(pendingTravelCount, true, "",
			                'img/travel-approval.png', 'Travel Approval',
			                'TravelApproval', 'Pending', '/trapproval'),
					//Leave Approval
					 new AccessAllowed(enterExpenseCount, true, "",
					                'img/enterExpense.png', 'Leave Approval',
					                'HolidayCalender', '', '/leaveApproval'), 
									
			        //Enter Expense Mobility
			        new AccessAllowed(enterExpenseCount, true, "",
					                'img/enterExpense.png', 'Enter Expense',
					                'EnterExpense', '', '/expenseList'), 
									
					//Leave Entry
					new AccessAllowed(enterExpenseCount, true, "",
					                'img/enterExpense.png', 'Enter Leave',
					                'HolidayCalender', '', '/leaveEntry'), 		
					                
			        new AccessAllowed(reporteeCount, true, "",
			                'img/active-person.png', 'Hierarchy',
			                'EmployeeHierarchy', 'Reportee',
			                '/employeehierarchy'),
			        new AccessAllowed('', true, "",
			                'img/conferencecall-white.png', 'Dial-In Bridge',
			                'WebConference', '', '/conference'),

			        // April
			        new AccessAllowed(allocationCount, true, "",
			                'img/Allocation-details.png', 'My Allocation',
			                'Allocation', 'active', 'allocationHistory'),

			        new AccessAllowed(projectCount, hasProjectListAccess, "",
			                'img/project.png', 'Project Details',
			                'ProjectDetails', 'projects', '/projectDetails'),

			        new AccessAllowed(pendingHolidayCount, true, "",
			                'img/holiday-calander.png', 'Holiday Calendar',
			                'HolidayCalender', 'remaining',
			                '/holidayCalender') ];*/

			/*
			 * ,
			 * 
			 * new AccessAllowed('', true, "", 'img/conferencecall-white.png',
			 * 'Change AD Password', 'WebConference', '', '/conference')
			 */
			nlapiLogExecution('DEBUG','response.Data',JSON.stringify(response.Data));
			response.Status = true;
		} else {
			throw "No Employee Id Provided";
		}
	} catch (err) {
		nlapiLogExecution('ERROR', 'postRESTlet', err);
		response.Data = err;
	}
	return response;
}

var sort_by = function(field, reverse, primer){

	   var key = primer ? 
	       function(x) {return primer(x[field])} : 
	       function(x) {return x[field]};

	   reverse = !reverse ? 1 : -1;

	   return function (a, b) {
	       return a = key(a), b = key(b), reverse * ((a > b) - (b > a));
	     } 
	}

function predicateBy(prop){
	   return function(a,b){
	      if( a[prop] > b[prop]){
	          return 1;
	      }else if( a[prop] < b[prop] ){
	          return -1;
	      }
	      return 0;
	   }
	}
//Function to remove duplicates
function removearrayduplicate(array) {
    var newArray = new Array();
    label: for (var i = 0; i < array.length; i++) {
          for (var j = 0; j < array.length; j++) {
                if (newArray[j] == array[i])
                      continue label;
          }
          newArray[newArray.length] = array[i];
    }
    return newArray;
}
