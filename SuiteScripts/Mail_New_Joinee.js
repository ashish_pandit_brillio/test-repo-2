function sendNewJoineeEmail(empId) {
    try {
        var email = nlapiLookupField('employee', empId, 'email');

        if (email) {
            var emailMerger = nlapiCreateEmailMerger(22);
            emailMerger.setEntity("employee", empId);
            var mergeResult = emailMerger.merge();
            var emailSubject = mergeResult.getSubject();
            var emailBody = mergeResult.getBody();
            //var attachment = [
               // nlapiLoadFile(52744),
                //nlapiLoadFile(52743) ];

            nlapiSendEmail(16808, email, emailSubject, emailBody, null, null, { 'entity': empId }, null);

        }
        else {
            throw "Employee Email Missing";
        }
    }
    catch (err) {
        nlapiLogExecution('error', 'sendNewJoineeEmail', err);
        throw err;
    }
}