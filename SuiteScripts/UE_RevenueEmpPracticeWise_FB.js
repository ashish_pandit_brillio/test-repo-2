/**
 * @author Jayesh
 */

function userEventBeforeSubmit(type) {
	try {
		if (type == 'create' || type == 'edit' || type == 'xedit') {
			
			var is_fb_project = nlapiGetFieldValue('custrecord_fb_project');
			if(is_fb_project != 'T')
			{
				return;
			}
			
			var projectId = nlapiGetFieldValue('custrecord_project_name_revenue');
			var proj_prac_id = nlapiGetFieldValue('custrecord_practice_id');
			
			if (projectId) {
				var startDate = nlapiGetFieldValue('custrecord_strt_date_revenue');
				var d_startDate = nlapiStringToDate(startDate);
				var d_endDate = nlapiAddDays(nlapiAddMonths(d_startDate, 1), -1);
				var endDate = nlapiDateToString(d_endDate, 'date');
				var today = new Date();

				nlapiSetFieldValue('custrecord_end_date_revenue', endDate);
				nlapiSetFieldValue('custrecord_month_revenue',getMonthName(startDate));
				nlapiSetFieldValue('custrecord_year_revenue', getYear(d_startDate));
				
				var cust_territory = nlapiLookupField('customer', nlapiGetFieldValue('custrecord_cust_revenue'),'territory');
				nlapiSetFieldValue('custrecord_region_revenue',cust_territory);
				
				var projectData = nlapiLookupField('job', projectId, [
				        'jobbillingtype', 'custentity_t_and_m_monthly',
				        'customer', 'custentity_projectvalue',
				        'custentity_practice', 'startdate', 'enddate',
				        'subsidiary', 'entityid', 'custentity_project_currency', 'entitystatus' ]);
				
				var proj_entitystatus = nlapiLookupField('job', projectId, 'entitystatus', true);
				
				nlapiSetFieldValue('custrecord_currency_revenue',projectData.custentity_project_currency);
				nlapiSetFieldValue('custrecord_fp_proj_status',proj_entitystatus);
				
				var i_RemainingMonths = getMonthDiff_reamining_mnth(new Date(),
		        nlapiStringToDate(projectData.enddate));
				nlapiSetFieldValue('custrecord_fp_remaining_mnths',i_RemainingMonths);
				
				var filters = new Array();
				filters[0] = new nlobjSearchFilter('field', 'systemnotes', 'anyof', 'CUSTJOB.DJOBACTUALEND');
				filters[1] = new nlobjSearchFilter('internalid', null, 'anyof', projectId);
				
				var column_proj = new Array();	
				column_proj[0] = new nlobjSearchColumn('oldvalue','systemNotes');
				column_proj[1] = new nlobjSearchColumn('date','systemNotes').setSort('true');
				
				var searchResult = nlapiSearchRecord('job', null, filters, column_proj);
				if (searchResult)
				{
					var d_recent_rpj_end_date = nlapiStringToDate(searchResult[0].getValue('oldvalue','systemNotes'));
					if(d_recent_rpj_end_date)
					{
						d_recent_rpj_end_date = nlapiDateToString(d_recent_rpj_end_date, 'date');
						nlapiSetFieldValue('custrecord_fp_previous_enddate',d_recent_rpj_end_date);
						nlapiSetFieldValue('custrecord_fp_extended','T');
					}
				}
		
				var projectCurrencyValue = nlapiGetFieldValue('custrecord_currency_revenue');
				var projectCurrencyText = nlapiGetFieldText('custrecord_currency_revenue');
				
				var usd_inr_rate = nlapiGetFieldValue('custrecord_usd_inr_rate_to_use');
				var usd_gbp_rate = nlapiGetFieldValue('custrecord_usd_gbp_rate_to_use');
				
				//if (projectData.jobbillingtype == 'FBM') {

					// if its a past month, do revenue recognition
					if (d_endDate < today) {
						nlapiSetFieldValue('custrecord_prp_is_recognised', 'T');
						revenueRecognition(projectData, projectId, startDate,
						        endDate, d_startDate, d_endDate,
						        projectCurrencyText, projectCurrencyValue,proj_prac_id,usd_inr_rate,usd_gbp_rate);
					} else { // else projection
						/*revenueProjection(projectData, projectId, startDate,
						        endDate, d_startDate, d_endDate,
						        projectCurrencyText, projectCurrencyValue,proj_prac_id,today,usd_inr_rate,usd_gbp_rate);*/
					}
				//}
			}
		}
	} catch (err) {
		nlapiLogExecution('ERROR', 'userEventBeforeSubmit', err);
	}
}

function revenueRecognition(projectData, projectId, startDate, endDate,
        d_startDate, d_endDate, projectCurrencyText, projectCurrencyValue,proj_prac_id,usd_inr_rate,usd_gbp_rate)
{
	try {
		
		var projectName = projectData.entityid;
		
		var filters = [
		        new nlobjSearchFilter('formuladate', null, 'within', startDate,
		                endDate).setFormula('{trandate}'),
		        new nlobjSearchFilter('custcolprj_name', null, 'startswith',
		                projectName)
					];
		
		if(proj_prac_id)
		{
			filters.push(new nlobjSearchFilter('department', null, 'anyof',parseInt(proj_prac_id)));
		}
		else
		{
			filters.push(new nlobjSearchFilter('department', null, 'anyof','@NONE@'));
		}
		
		// if LLC
		if (projectData.subsidiary == 2) {
			filters.push(new nlobjSearchFilter('account', null, 'anyof',
			        [ '773' ]));
		} else { // BTPL
			filters.push(new nlobjSearchFilter('account', null, 'anyof', ['773', '732' ]));
		}
		
		filters.push(new nlobjSearchFilter('type', null, 'noneof','SalesOrd'));
		
		var jeSearch = nlapiSearchRecord('transaction', null, filters, [
		        new nlobjSearchColumn('fxamount', null, 'sum'),
		        new nlobjSearchColumn('currency', null, 'group') ]);
		
		nlapiLogExecution('debug', 'search data', JSON.stringify(jeSearch));
		
		var recognizedAmount = 0;

		if (jeSearch) {
			
			usd_inr_rate = 1/parseFloat(usd_inr_rate);
			usd_gbp_rate = 1/parseFloat(usd_gbp_rate);

			jeSearch.forEach(function(je) {
				var currency = je.getText('currency', null, 'group');
				var amount = je.getValue('fxamount', null, 'sum');
				
				if(currency == 'GBP')
				{
					amount = parseFloat(amount) * parseFloat(usd_gbp_rate); 
				}
				else if(currency == 'USD')
				{
					amount = parseFloat(amount) * parseFloat(1);
				}
				else if(currency == 'INR')
				{
					amount = parseFloat(amount) * parseFloat(usd_inr_rate);
				}
				
				recognizedAmount = parseFloat(amount) + parseFloat(recognizedAmount);
			});
		}
		
		nlapiSetFieldValue('custrecord_usd_cost_revenue',recognizedAmount);

	} catch (err) {
		nlapiLogExecution('ERROR', 'revenueRecognition', err);
		throw err;
	}
}

function revenueProjection(projectData, projectId, startDate, endDate,
        d_startDate, d_endDate, projectCurrencyText, projectCurrencyValue,proj_prac_id,today,usd_inr_rate,usd_gbp_rate)
{
	try {
		
		var last_practice = nlapiGetFieldValue('custrecord_lst_participating_practice');
		if(last_practice != 'T')
		{
			return;
		}
		
		usd_inr_rate = 1/parseFloat(usd_inr_rate);
		usd_gbp_rate = 1/parseFloat(usd_gbp_rate);
		
		// get the total project value
		var totalProjectValue = parseFloat(projectData.custentity_projectvalue);
		if(projectCurrencyText == 'GBP')
		{
			totalProjectValue = parseFloat(totalProjectValue) * parseFloat(usd_gbp_rate); 
		}
		else if(projectCurrencyText == 'USD')
		{
			totalProjectValue = parseFloat(totalProjectValue) * parseFloat(1);
		}
		else if(projectCurrencyText == 'INR')
		{
			totalProjectValue = parseFloat(totalProjectValue) * parseFloat(usd_inr_rate);
		}
		nlapiLogExecution('debug', 'total project value', totalProjectValue);

		// get the recognized amount
		var totalRecognizedAmount = getTotalRecognizedAmount(projectId,
		        projectData.subsidiary, projectData.entityid, startDate,
		        projectCurrencyText,today,usd_inr_rate,usd_gbp_rate);
		nlapiLogExecution('debug', 'total recognized value for 2016',totalRecognizedAmount);
			
		var amount_till_2015 = 0;
			
		var filters_proj_revenue2015 = new Array();
		filters_proj_revenue2015[0] = new nlobjSearchFilter('entityid', 'custrecord_fp_project_link', 'is', projectData.entityid);
		
		var proj_recoginsed_amnt_srch = nlapiSearchRecord('customrecord_fp_recognised_amount_2015', null, filters_proj_revenue2015, [
        new nlobjSearchColumn('custrecord_amount_recognised_till_2015')]);
		
		if(proj_recoginsed_amnt_srch)
		{
			amount_till_2015 = proj_recoginsed_amnt_srch[0].getValue('custrecord_amount_recognised_till_2015');
		}
		nlapiLogExecution('debug', 'total recognized value for 2015',amount_till_2015);
		
		var total_recognized_amount_till_date = parseFloat(amount_till_2015) + parseFloat(totalRecognizedAmount);
		
		nlapiSetFieldValue('custrecord_total_recognised_amount',total_recognized_amount_till_date);
		
		// get the remaining amount
		var totalRemainingAmount = totalProjectValue - total_recognized_amount_till_date;
		nlapiLogExecution('debug', 'total remaining value',
		        totalRemainingAmount);

		// total monthly duration of the project
		var countProjectMonths = getMonthDiff(
		        nlapiStringToDate(projectData.startdate),
		        nlapiStringToDate(projectData.enddate));
		nlapiLogExecution('debug', 'project duration', countProjectMonths);

		// remaining months
		var countRemainingMonths = getMonthDiff(new Date(),
		        nlapiStringToDate(projectData.enddate));
		nlapiLogExecution('debug', 'remaining months', countRemainingMonths);
		nlapiSetFieldValue('custrecord_fp_remaining_mnths',countRemainingMonths);

		// calculate per month amount
		var projectedPerMonthAmount = Math.round(totalRemainingAmount
		        / countRemainingMonths);
		nlapiLogExecution('debug', 'per month amount', projectedPerMonthAmount);

		nlapiSetFieldValue('custrecord_usd_cost_revenue',projectedPerMonthAmount);

		
	} catch (err) {
		nlapiLogExecution('ERROR', 'revenueProjection', err);
		throw err;
	}
}

function getTotalRecognizedAmount(projectId, subsidiary, projectName,
        monthStartDate, projectCurrency,today,usd_inr_rate,usd_gbp_rate)
{
	try {
		var recognizedAmount = 0;

		nlapiLogExecution('debug', 'monthStartDate', monthStartDate);
		nlapiLogExecution('debug', 'projectName', projectName);
	
		var previous_mnth_lst_date = new Date(today.getFullYear(), today.getMonth(), 1);
		var previous_mnth_lst_date = new Date(previous_mnth_lst_date);
		previous_mnth_lst_date.setDate(-1);
		previous_mnth_lst_date = nlapiStringToDate(previous_mnth_lst_date);
		
		var filters = [
		        new nlobjSearchFilter('formuladate', null, 'within', '1/1/2016',
		                previous_mnth_lst_date).setFormula('{trandate}'),
		        new nlobjSearchFilter('custcolprj_name', null, 'startswith',
		                projectName) ];

		// if LLC
		if (subsidiary == 2) {
			filters.push(new nlobjSearchFilter('account', null, 'anyof',
			        [ '773' ]));
		} else { // BTPL
			filters.push(new nlobjSearchFilter('account', null, 'anyof', [
			        '773', '732' ]));
		}

		var jeSearch = nlapiSearchRecord('transaction', null, filters, [
		        new nlobjSearchColumn('fxamount', null, 'sum'),
		        new nlobjSearchColumn('currency', null, 'group') ]);

		nlapiLogExecution('debug', 'search data', JSON.stringify(jeSearch));

		if (jeSearch) {

			jeSearch.forEach(function(je) {
				var currency = je.getText('currency', null, 'group');
				var amount = je.getValue('fxamount', null, 'sum');
				
				if(currency == 'GBP')
				{
					amount = parseFloat(amount) * parseFloat(usd_gbp_rate); 
				}
				else if(currency == 'USD')
				{
					amount = parseFloat(amount) * parseFloat(1);
				}
				else if(currency == 'INR')
				{
					amount = parseFloat(amount) * parseFloat(usd_inr_rate);
				}
				
				recognizedAmount = parseFloat(recognizedAmount) + parseFloat(amount);
			});
		}

		return recognizedAmount;
	} catch (err) {
		nlapiLogExecution('ERROR', 'getTotalRecognizedAmount', err);
		throw err;
	}
}

function getMonthDiff(fromDate, toDate) {
	var month = 1;
	for (;; month++) {
		var newDate = nlapiAddMonths(fromDate, month);

		if (newDate >= toDate) {
			break;
		}
	}

	return month;
}

function getMonthDiff_reamining_mnth(fromDate, toDate) {
	
	var month = 0;
	if (fromDate < toDate)
	{
		month = 1;
		for (;; month++) {
			var newDate = nlapiAddMonths(fromDate, month);
	
			if (newDate >= toDate) {
				break;
			}
		}
	}
	
	return month;
}

function getMonthName(currentDate) {
	currentDate = nlapiStringToDate(currentDate)
	var monthNames = [ "JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JUL", "AUG",
	        "SEP", "OCT", "NOV", "DEC" ];
	return monthNames[currentDate.getMonth()];
}

function getYear(currentDate) {
	return Math.round(currentDate.getFullYear()).toString();
}
