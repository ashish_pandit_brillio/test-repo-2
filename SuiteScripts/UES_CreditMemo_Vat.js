// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
    /*
     * 
     Script Name:UES Credit Memo VAT
     Author:      Supriya Jadhav
     Company:    Aashna Cloudtech Pvt Ltd.
     Date: 
     Description:
     
     Script Modification Log:
     
     -- Date --			-- Modified By --				--Requested By--				-- Description --
      22 sep 2014         Supriya                         Kalpana                       Normal Account Validation
      30 July 2015        Nikhil                          sachin k                     add a reneweal process
      21 oct 2015         Nikhil j                        Sachin k                     currency other than Inr (convert to INR)   
     Below is a summary of the process controls enforced by this script file.  The control logic is described
     more fully, below, in the appropriate function headers and code blocks.
     
     BEFORE LOAD
     - beforeLoadRecord(type)
     Not Used
     BEFORE SUBMIT
     - beforeSubmitRecord(type)
     Not Used
     AFTER SUBMIT
     - afterSubmitRecord(type)
     afterSubmitRecord(type)
     SUB-FUNCTIONS
     - The following sub-functions are called by the above core functions in order to maintain code
     modularization:
     - NOT USED
     */
}
// END SCRIPT DESCRIPTION BLOCK  ====================================



// BEGIN GLOBAL VARIABLE BLOCK  =====================================
{
    // Initialize any Global Variables, in particular, debugging variables...
    var a_subisidiary = new Array;
    var i_vatCode;
    var i_taxcode;
	var s_pass_code;
    
    
    
}
// END GLOBAL VARIABLE BLOCK  =======================================





// BEGIN BEFORE LOAD ==================================================

function beforeLoadRecord(type)	
{

    /*  On before load:
     - EXPLAIN THE PURPOSE OF THIS FUNCTION
     -
     FIELDS USED:
     --Field Name--				--ID--
     */
    //  LOCAL VARIABLES
    
    //  BEFORE LOAD CODE BODY
    
    
    return true;
    
    
    
}

// END BEFORE LOAD ====================================================





// BEGIN BEFORE SUBMIT ================================================

function beforeSubmitRecord(type)	
{
    /*  On before submit:
     - PURPOSE
     -
     FIELDS USED:
     --Field Name--				--ID--
     */
    //  LOCAL VARIABLES
    
    
    //  BEFORE SUBMIT CODE BODY
	//try
	{
	var i_Flagcheck = 0;
	var Flag = 0;
	var a_subisidiary = new Array();
	var s_pass_code;
	
	nlapiLogExecution('DEBUG', 'beforeSubmitRecord Credit Memo ', " i_Flagcheck " + i_Flagcheck)
	nlapiLogExecution('DEBUG', 'beforeSubmitRecord Credit Memo', " type " + type)
    if (type == 'delete') 	
		{
			var i_creditmemo = nlapiGetRecordId();
			nlapiLogExecution('DEBUG', 'beforeSubmitRecord Credit Memo ', " i_creditmemo " + i_creditmemo)
			
			var recordType = nlapiGetRecordType()
			nlapiLogExecution('DEBUG', 'beforeSubmitRecord Invoice', " recordType " + recordType)
			
			var o_creditmemoobj = nlapiLoadRecord(recordType,i_creditmemo)
			
			var i_AitGlobalRecId = SearchGlobalParameter();
		    nlapiLogExecution('DEBUG', 'Invoice', "i_AitGlobalRecId" + i_AitGlobalRecId);
		    
		    if (i_AitGlobalRecId != 0) 	
			{
		        var o_AitGloRec = nlapiLoadRecord('customrecord_indian_tax_glo_parameter', i_AitGlobalRecId);
		        
		       
		        a_subisidiary = o_AitGloRec.getFieldValues('custrecord_ait_subsidiary');
		        nlapiLogExecution('DEBUG', 'Invoice ', "a_subisidiary->" + a_subisidiary);
		        
		        var i_vatCode = o_AitGloRec.getFieldValue('custrecord_ait_vat_code');
		        nlapiLogExecution('DEBUG', 'Invoice ', "i_vatCode->" + i_vatCode);
		        
		        var i_taxcode = o_AitGloRec.getFieldValue('custrecord_ait_taxcode');
		        nlapiLogExecution('DEBUG', 'Invoice', "i_taxcode->" + i_taxcode);
				
				var s_pass_code = o_AitGloRec.getFieldValue('custrecord_encpwdstring');
				nlapiLogExecution('DEBUG','Bill ', "s_pass_code->"+s_pass_code);
		        
		        
		    }// end if(i_AitGlobalRecId != 0 )
			//var a_subsidiary1 = a_subisidiary.toString();
			
			var i_subsidiary = nlapiGetFieldValue('subsidiary')
		    Flag = isindia_subsidiary(a_subisidiary, s_pass_code,i_subsidiary)      
		        
		       /*
 if (a_subisidiary[1] != undefined && a_subisidiary[1] != null && a_subisidiary[1] != '') 	
				{
		        
		            for (var y = 0; y < a_subisidiary.length; y++) 	
					{
		                var i_subsidiary = o_creditmemoobj.getFieldValue('subsidiary')
		                
		                //==== IF SUBSIDIARY IS INDIAN SUBSIDIARY ====
		                if (a_subisidiary[y] == i_subsidiary) 	
						{
		                    Flag = 1;
		                    break;
		                }// END if (a_subisidiary[y] == i_subsidiary)
						
		            }// END for (var y = 0; y < a_subisidiary.length; y++)
					
		        }// END  if (a_subsidiary1.indexOf(',') > -1)
		        else 	
				{
		            // === IF COMMA NOT FOUND ===
		            var i_subsidiary = o_creditmemoobj.getFieldValue('subsidiary')
		            
		            //==== IF SUBSIDIARY IS INDIAN SUBSIDIARY ====
		            if (a_subisidiary[0] == i_subsidiary) 	
					{
		                Flag = 1;
		            }// END  if (a_subsidiary1 == i_subsidiary)
					
		        }// END  if (a_subsidiary1 == i_subsidiary)
				
			//===== FOR SINGLE SUBSIDARY ACCOUNT  =======
			var context = nlapiGetContext();
		    var i_subcontext = context.getFeature('SUBSIDIARIES');
			if(i_subcontext == false)
			{
				 Flag = 1;
			}	

*/				
			
			if(Flag == parseInt(1))
			{
			var column = new Array();
            var filters = new Array();
            filters.push(new nlobjSearchFilter('custrecord_vatinvoice_invoiceno', null, 'is', i_creditmemo));
            column.push(new nlobjSearchColumn('internalid'))
            column.push(new nlobjSearchColumn('custrecord_vatinvoice_status'))
            var results = nlapiSearchRecord('customrecord_vatinvoicedetail', null, filters, column);
            
            if (results != null) 	
			{
            
                for (var i = 0; results != null && i < results.length; i++) 	
				{
                    var vatInvoicedetailId = results[i].getValue('internalid')
                    var vatInvoiceStatus = results[i].getValue('custrecord_vatinvoice_status')
                    nlapiLogExecution('DEBUG', 'beforeSubmitRecord Credit Memo ', " Vat Credit Memo Detail ID " + vatInvoicedetailId)
                    nlapiLogExecution('DEBUG', 'beforeSubmitRecord Credit Memo ', " Vat Credit Memo Status " + vatInvoiceStatus)
                    if (vatInvoicedetailId != null && vatInvoicedetailId != undefined && vatInvoicedetailId != '')	
					 {
                        if (vatInvoiceStatus == 'Close') 	
						{
                            i_Flagcheck = 1;
							break;
                        }// invoice status end
						
                    }// end of the Vat Credit Memo Id
                    
                }// result for loop end.
				
				nlapiLogExecution('DEBUG', 'beforeSubmitRecord Credit Memo ', " i_Flagcheck " + i_Flagcheck)
				if(i_Flagcheck == parseInt(1))
				{
					throw("You can not delete this record as payment has been applied for this record..")
				}
				else
				{
					for (var i = 0; results != null && i < results.length; i++) 	
					{
	                    var vatInvoicedetailId = results[i].getValue('internalid')
	                    var vatInvoiceStatus = results[i].getValue('custrecord_vatinvoice_status')
	                    nlapiLogExecution('DEBUG', 'beforeSubmitRecord Credit Memo ', " Vat Credit Memo Detail ID " + vatInvoicedetailId)
	                    nlapiLogExecution('DEBUG', 'beforeSubmitRecord Credit Memo ', " Vat Credit Memo Status " + vatInvoiceStatus)
	                    if (vatInvoicedetailId != null && vatInvoicedetailId != undefined && vatInvoicedetailId != '')	
						 {
	                        //if (vatInvoiceStatus == 'Open') 	
							{
	                            nlapiLogExecution('DEBUG', 'beforeSubmitRecord Credit Memo ', " if(vatInvoicedetailId == 'Open')")
	                            nlapiDeleteRecord('customrecord_vatinvoicedetail', vatInvoicedetailId);
	                        }// invoice status end
							
	                    }// end of the Vat Credit Memo Id
	                    
	                }// result for loop end.
				}
				
            }// result not null check.
			}// End Flag Check
			
		}//End Delete Record
	}
	//catch(EX)
	//{
	//	 nlapiLogExecution('DEBUG', 'Credit Memo beforeSubmitRecord ', "Exception Thrown" + EX);
	//}
	
    
    return true;
    
}

// END BEFORE SUBMIT ==================================================





// BEGIN AFTER SUBMIT =============================================

function afterSubmitRecord(type)	
{
    /*  On after submit:
     - PURPOSE
     -
     FIELDS USED:
     --Field Name--				--ID--
     */
    //  LOCAL VARIABLES
    
    
    
    //  AFTER SUBMIT CODE BODY
    
    
    // === CODE TO GET THE GLOBAL SUBSIDIARY PARAMETER ===
	if (type != 'delete') 	
	{
		try
	{
		
	var stRoundMethod;
	
	var i_AitGlobalRecId = SearchGlobalParameter();
    nlapiLogExecution('DEBUG', 'Credit Memo ', "i_AitGlobalRecId" + i_AitGlobalRecId);
    
    if (i_AitGlobalRecId != 0) 	
	{
        var o_AitGloRec = nlapiLoadRecord('customrecord_indian_tax_glo_parameter', i_AitGlobalRecId);
        
        a_subisidiary = o_AitGloRec.getFieldValues('custrecord_ait_subsidiary');
        nlapiLogExecution('DEBUG', 'Credit Memo ', "a_subisidiary->" + a_subisidiary);
        
        i_vatCode = o_AitGloRec.getFieldValue('custrecord_ait_vat_code');
        nlapiLogExecution('DEBUG', 'Credit Memo ', "i_vatCode->" + i_vatCode);
        
        i_taxcode = o_AitGloRec.getFieldValue('custrecord_ait_taxcode');
        nlapiLogExecution('DEBUG', 'Credit Memo ', "i_taxcode->" + i_taxcode);
		
		s_pass_code = o_AitGloRec.getFieldValue('custrecord_encpwdstring');
		nlapiLogExecution('DEBUG','Bill ', "s_pass_code->"+s_pass_code);
		
		stRoundMethod = o_AitGloRec.getFieldValue('custrecord_ait_st_roundoff');
        
    }// end global record id
    
   
    
    //==== Begin :Variable Declaration ====
    var i_creditmemo;
    var customer;
	var currentRectype;
    var date;
	var usertotal;
	var account = new Array();
    var creditamount= new Array();
	var debitamount= new Array();
	var posting= new Array();
    var memo= new Array();
	var entity= new Array();
	var subsidiary = new Array();
	var department= new Array();
	var class1= new Array();
	var locationArray= new Array();
	var fxamount= new Array();
	var rectype= new Array();
	var taxcode = new Array();
	 
    // ====  CODE BODY  =====
    i_creditmemo = nlapiGetRecordId()
	currentRectype = nlapiGetRecordType();
    
    if (i_creditmemo != null && i_creditmemo != 'undefined' && i_creditmemo != '') 	
	{
    
        var o_creditmemoobj = nlapiLoadRecord(currentRectype, i_creditmemo);
        
        if (o_creditmemoobj != null && o_creditmemoobj != undefined && o_creditmemoobj != '') 	
		{
             var a_subsidiary1 = a_subisidiary.toString();
    		 var Flag = 0;
             nlapiLogExecution('DEBUG', 'Credit Memo ', "a_subsidiary1->" + a_subsidiary1);
			 var i_subsidiary = o_creditmemoobj.getFieldValue('subsidiary')
			 Flag = isindia_subsidiary(a_subisidiary, s_pass_code,i_subsidiary)      
    
		   /*
 if (a_subisidiary[1] != undefined && a_subisidiary[1] != null && a_subisidiary[1] != '') 	
			{
		    
		        for (var y = 0; y < a_subisidiary.length; y++) 	
				{
		            var i_subsidiary = o_creditmemoobj.getFieldValue('subsidiary')
		            
		            //==== IF SUBSIDIARY IS INDIAN SUBSIDIARY ====
		            if (a_subisidiary[y] == i_subsidiary) 	
					{
		                Flag = 1;
		                break;
		            }// END if (a_subisidiary[y] == i_subsidiary)
					
		        }// END for (var y = 0; y < a_subisidiary.length; y++)
				
		    }// END  if (a_subsidiary1.indexOf(',') > -1)
		    else 	
			{
		        // === IF COMMA NOT FOUND ===
		        var i_subsidiary = o_creditmemoobj.getFieldValue('subsidiary')
		        
		        //==== IF SUBSIDIARY IS INDIAN SUBSIDIARY ====
		        if (a_subisidiary[0] == i_subsidiary) 	
				{
		            Flag = 1;
		        }// END  if (a_subsidiary1 == i_subsidiary)
				
		    }// END  if (a_subsidiary1 == i_subsidiary)
			
			
*/
		    //======VAT CODE ====
            var formid = nlapiGetFieldValue('customform')
			customer = o_creditmemoobj.getFieldValue('entity')
            date = o_creditmemoobj.getFieldValue('trandate')
			usertotal = o_creditmemoobj.getFieldValue('total')
			
			//==========================Get indian Currency internalD and Exchange rate =====================//
			
			var currency = o_creditmemoobj.getFieldValue('currency')
			
			var INRcurrencyID = getAITIndianCurrency()
			
			var exchangerate = nlapiExchangeRate(currency, INRcurrencyID,date)
			
			if(currency != INRcurrencyID)
			{
				usertotal = (parseFloat(usertotal) * parseFloat(exchangerate)) 
				
				usertotal = applySTRoundMethod(stRoundMethod,usertotal)
				
				//subtotal = (parseFloat(subtotal) * parseFloat(exchangerate)) 
			}
			
			
		/*
	//===== FOR SINGLE SUBSIDARY ACCOUNT  =======
			var context = nlapiGetContext();
		    var i_subcontext = context.getFeature('SUBSIDIARIES');
			if(i_subcontext == false)
			{
				 Flag = 1;
			}
            
*/
            if (Flag == 1) 	
			{
                //  CODE TO GET THE DATA ONE BY ONE FROM THE SAVED SEARCH
                var filter = new Array();
                filter.push(new nlobjSearchFilter('internalid', null, 'is', i_creditmemo))
				//filter.push(new nlobjSearchFilter('type', null, 'is', ' CustCred'))
                
                
                var columns = new Array();
                columns[0] = new nlobjSearchColumn('internalid');
                var search_results = nlapiSearchRecord('transaction', 'customsearch_tax_invoice_info', filter, null);
                
                if (search_results != null && search_results != '' && search_results != 'undefined') 	
				{
                    nlapiLogExecution('DEBUG', 'Credit Memo', ' bill searchresult -->' + search_results.length);
                    for (k = 0; k < search_results.length; k++) 	
					{
                        var grpresult = search_results[k];
                        var grpcolumns = grpresult.getAllColumns();
                        var grpcolumnLen = grpcolumns.length;
                        
                        for (var jcnt = 0; jcnt < grpcolumnLen; jcnt++) 	
						{
                        
                            var grpcolumn = grpcolumns[jcnt];
                            var grpfieldName = grpcolumn.getName();
                            var grpvalue = grpresult.getValue(grpcolumn);
                            var Txtvalue = grpresult.getText(grpcolumn);
                            nlapiLogExecution('DEBUG', 'Credit Memo ', " k ->  " + k)
							
                            if (grpfieldName == 'account') 	
							{
                               
								account[k] = grpvalue;
                                nlapiLogExecution('DEBUG', 'Credit Memo ', " account ->  " + account[k])
                            }
                            
                            if (grpfieldName == 'creditamount') 	
							{
                                creditamount[k] = grpvalue;
                                nlapiLogExecution('DEBUG', 'Credit Memo ', " creditamount ->  " + creditamount[k])
                            }
                            if (grpfieldName == 'debitamount') 	
							{
                                debitamount[k] = grpvalue;
                                nlapiLogExecution('DEBUG', 'Credit Memo ', " debitamount ->  " + debitamount[k])
                            }
                            if (grpfieldName == 'postingperiod')	
							{
                                posting[k] = grpvalue;
                                nlapiLogExecution('DEBUG', 'Credit Memo ', " posting ->  " + posting[k])
                            }
                            if (grpfieldName == 'memo') 	
							{
                                memo[k] = grpvalue;
                                nlapiLogExecution('DEBUG', 'Credit Memo ', " memo ->  " + memo[k])
                            }
                            if (grpfieldName == 'entity') 	
							{
                                entity[k] = grpvalue;
                                nlapiLogExecution('DEBUG', 'Credit Memo ', " entity ->  " + entity[k])
                            }
                            if (grpfieldName == 'subsidiary') 	
							{
                                subsidiary[k] = grpvalue;
                                nlapiLogExecution('DEBUG', 'Credit Memo ', " subsidiary ->  " + subsidiary[k])
                            }
                            if (grpfieldName == 'department') 	
							{
                                department[k] = grpvalue;
                                nlapiLogExecution('DEBUG', 'Credit Memo ', " department ->  " + department[k])
                            }
                            if (grpfieldName == 'class') 	
							{
                                class1[k] = grpvalue;
                                nlapiLogExecution('DEBUG', 'Credit Memo ', " class1 ->  " + class1[k])
                                
                            }
                            if (grpfieldName == 'location') 	
							{
                                locationArray[k] = grpvalue;
                                nlapiLogExecution('DEBUG', 'Credit Memo ', " locationArray ->  " + locationArray[k])
                            }
                            if (grpfieldName == 'fxamount') 	
							{
								if (INRcurrencyID != currency) 
								{
									var i_INR_amt = (parseFloat(grpvalue) * parseFloat(exchangerate));
									nlapiLogExecution('DEBUG', 'Bill ', " i_INR_amt ->  " + i_INR_amt)
									
									i_INR_amt = applySTRoundMethod(stRoundMethod,i_INR_amt)
									
									fxamount[k] = i_INR_amt;
								}
								else 
								{
									fxamount[k] = parseFloat(grpvalue);
									nlapiLogExecution('DEBUG', 'Credit Memo ', " fxamount ->  " + fxamount[k])
								}
                            }
                            if (grpfieldName == 'taxcode') 	
							{
                                taxcode[k] = grpvalue;
                                nlapiLogExecution('DEBUG', 'Credit Memo ', " taxcode ->  " + taxcode[k])
                            }
                            if (grpfieldName == 'type') 	
							{
                                 nlapiLogExecution('DEBUG', 'Credit Memo ', " grpvalue 1->  " + grpvalue)
								if(grpvalue == 'CustCred')
								{
									grpvalue = 'Credit Memo'
								}
								rectype[k] = grpvalue;
                                nlapiLogExecution('DEBUG', 'Credit Memo ', " rectype ->  " + rectype[k])
								
                            }// group value end
                            
                        }// column check end
                        
                    }//search result end
                
				}// search result not null check
				
				
				// ======== Code  when New vat bill is created we create on custom record.of the type VAT Bill Detail with all the detail  ====
                if (type == 'create') 	
				{
                    if (search_results != null && search_results != '' && search_results != 'undefined') 
					{
						for (var k = 0; k < search_results.length; k++) 	
						{
	                        nlapiLogExecution('DEBUG', 'Credit Memo ', " fxamount[k] ->  " + fxamount[k])
	                        record = nlapiCreateRecord('customrecord_vatinvoicedetail');
	                        record.setFieldValue('custrecord_vatinvoice_invoiceno', i_creditmemo)
							record.setFieldValue('custrecord_vatinvoice_tranno', i_creditmemo)
	                        record.setFieldValue('custrecord_vatinvoice_account', account[k])
	                        record.setFieldValue('custrecord_vatinvoice_customer', customer);
	                        record.setFieldValue('custrecord_vatinvoice_date', date);
	                        record.setFieldValue('custrecord_vatinvoice_grossamount', usertotal);
	                        record.setFieldValue('custrecord_vatinvoice_taxamount', fxamount[k]);
	                        record.setFieldValue('custrecord_vatinvoice_status', 'Open');
	                        record.setFieldValue('custrecord_vatinvoice_type', rectype[k]);
	                        record.setFieldValue('custrecord_vatinvoice_taxid', taxcode[k]);
	                        record.setFieldValue('custrecord_vatinvoice_postingperiod', posting[k]);
	                        record.setFieldValue('custrecord_vatbill_location', locationArray[k]);
	                        var id = nlapiSubmitRecord(record, true);
	                        nlapiLogExecution('DEBUG', 'Credit Memo ', " New Vat Credit Memo Detail Record ID " + id)
	                        
	                    } // END for(var k=1;k<=rowcount;k++)
	                    
					}// ENd if (search_results != null && search_results != '' && search_results != 'undefined') 	
					
                } // END if(type=='create')
                
                // ====  End :Code for create New VAT Bill Detail custom Record ======
				
                if (type == 'edit') 	
				{
                    var column = new Array();
                    var filters = new Array();
                    filters.push(new nlobjSearchFilter('custrecord_vatinvoice_invoiceno', null, 'is', i_creditmemo));
                    column.push(new nlobjSearchColumn('internalid'))
                    column.push(new nlobjSearchColumn('custrecord_vatinvoice_status'))
                    var results = nlapiSearchRecord('customrecord_vatinvoicedetail', null, filters, column);
                    
                    if (results != null) 	
					{
                    
                        for (var i = 0; results != null && i < results.length; i++) 	
						{
                            var vatInvoicedetailId = results[i].getValue('internalid')
                            var vatInvoiceStatus = results[i].getValue('custrecord_vatinvoice_status')
                            nlapiLogExecution('DEBUG', 'Credit Memo ', " Vat Credit Memo Detail ID " + vatInvoicedetailId)
                            nlapiLogExecution('DEBUG', 'Credit Memo ', " Vat Credit Memo Status " + vatInvoiceStatus)
                            if (vatInvoicedetailId != null && vatInvoicedetailId != undefined && vatInvoicedetailId != '')	
							 {
                                if (vatInvoiceStatus == 'Open') 	
								{
                                    nlapiLogExecution('DEBUG', 'Credit Memo ', " if(vatInvoicedetailId == 'Open')")
                                    nlapiDeleteRecord('customrecord_vatinvoicedetail', vatInvoicedetailId);
                                }// invoice status end
								
                            }// end of the Vat Credit Memo Id
                            
                        }// result for loop end.
						
                    }// result not null check.
                    
                    if(search_results != null && search_results != undefined && search_results != '')
					{
						for (var k = 0; k < search_results.length; k++) 	
						{
	                    
	                        nlapiLogExecution('DEBUG', 'Credit Memo ', " fxamount[k] ->  " + fxamount[k])
	                        record = nlapiCreateRecord('customrecord_vatinvoicedetail');
	                        
	                        record.setFieldValue('custrecord_vatinvoice_invoiceno', i_creditmemo)
							record.setFieldValue('custrecord_vatinvoice_tranno', i_creditmemo)
	                        record.setFieldValue('custrecord_vatinvoice_account', account[k])
	                        record.setFieldValue('custrecord_vatinvoice_customer', customer);
	                        record.setFieldValue('custrecord_vatinvoice_date', date);
	                        record.setFieldValue('custrecord_vatinvoice_grossamount', usertotal);
	                        record.setFieldValue('custrecord_vatinvoice_taxamount', fxamount[k]);
	                        record.setFieldValue('custrecord_vatinvoice_status', 'Open');
	                        record.setFieldValue('custrecord_vatinvoice_type', rectype[k]);
	                        record.setFieldValue('custrecord_vatinvoice_taxid', taxcode[k]);
	                        record.setFieldValue('custrecord_vatinvoice_postingperiod', posting[k]);
	                        record.setFieldValue('custrecord_vatbill_location', locationArray[k]);
	                        var id = nlapiSubmitRecord(record, true);
	                        nlapiLogExecution('DEBUG', 'Credit Memo ', " New Vat Credit Memo Detail Record ID " + id)
	                        
	                    } // END for(var k=1;k<=rowcount;k++)
						
					}// end  if(search_results != null && search_results != undefined && search_results != '')
					
					
                }// type edit end
				
				
				
                
            }// end if (a_subisidiary[y] == i_subsidiary)
			
        }// end if (o_creditmemoobj != null && o_creditmemoobj != undefined && o_creditmemoobj != '') 
        
    }// end  if (i_creditmemo != null && i_creditmemo != 'undefined' && i_creditmemo != '') 
	}
	catch(EX)
	{
		nlapiLogExecution('DEBUG', 'Credit Memo ', "Exception thrown->" + EX);
	}// catch end 
	}
	
	
    
    
    
    return true;
    //End Code for if you Edit the VAT Invoice
}

// END AFTER SUBMIT ===============================================





// BEGIN FUNCTION ===================================================
{
    function SearchGlobalParameter()	
	{
    
        // == GET TDS GLOBAL PARAMETER ====
        var a_filters = new Array();
        var a_column = new Array();
        var i_globalRecId = 0;
        
        a_column.push(new nlobjSearchColumn('internalid'));
        
        var s_serchResult = nlapiSearchRecord('customrecord_indian_tax_glo_parameter', null, null, a_column)
        
        if (s_serchResult != null && s_serchResult != undefined && s_serchResult != '')	
		 {
            for (var i = 0; i < s_serchResult.length; i++) 	
			{
                i_globalRecId = s_serchResult[0].getValue('internalid');
                nlapiLogExecution('DEBUG', 'Invoice ', "i_globalRecId" + i_globalRecId);
                
            }
            
        }
        
        
        return i_globalRecId;
    }
    
}
function applySTRoundMethod(stRoundMethod,taxamount)
{
	var roundedSTAmount = taxamount;
	
	if(stRoundMethod == 2)
	{
		roundedSTAmount = Math.round(taxamount)
	}
	if(stRoundMethod == 3)
	{
		roundedSTAmount = Math.round(taxamount/10)*10;
	}
	if(stRoundMethod == 4)
	{
		roundedSTAmount = Math.round(taxamount/100)*100;
	}
	
	return roundedSTAmount;
}
// END FUNCTION =====================================================
