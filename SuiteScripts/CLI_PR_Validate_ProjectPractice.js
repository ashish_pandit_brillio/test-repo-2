function fieldChanged_PR_validation_project(type, name, linenum)
{
try{
 if(name == 'custrecord_project')
	{		
		var i_project = nlapiGetCurrentLineItemValue('recmachcustrecord_purchaserequest', 'custrecord_project');
		nlapiLogExecution('DEBUG', 'i_project', ' i_project --> ' + i_project);  
		var project_Practice = nlapiLookupField('job',parseInt(i_project),'custentity_practice');
		var practice_lookup = nlapiLookupField('department',parseInt(project_Practice),'isinactive');
		if(practice_lookup == 'T'){
		alert('Selected Project Executing Practice is Inactivated. Please select the different project Or Please drop an email to BuOps team!!');
		
		//nlapiRemoveLineItem('recmachcustrecord_purchaserequest',nlapiGetCurrentLineItemIndex('recmachcustrecord_purchaserequest'));
		nlapiSetCurrentLineItemValue('recmachcustrecord_purchaserequest','custrecord_project','',false,false);
		nlapiSetCurrentLineItemValue('recmachcustrecord_purchaserequest','custrecord_customerforpritem','',false,false);
		//nlapiSetCurrentLineItemValue('recmachcustrecord_purchaserequest','custrecord_prastatus','',false,false);
		nlapiSetCurrentLineItemValue('recmachcustrecord_purchaserequest','custrecord_subpracticehead','',false,false);
		}
		
					
	}
}
catch(e){
nlapiLogExecution('DEBUG','Process Error',e);
}
}