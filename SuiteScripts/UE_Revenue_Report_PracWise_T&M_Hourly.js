/**
 * @author Jayesh
 * Script Modification Log:
     -- Date --               -- Modified By --                  --Requested By--                   -- Description --
      16-03-2020				Praveena madem					  Deepak Ms						 Changed Join id Filters of timesheet.
	 
 */

function userEventBeforeSubmit(type) {
	try {

		if (type == 'create' || type == 'edit' || type == 'xedit') {
			
			var is_monthly_project = nlapiGetFieldValue('custrecord_monthly_project');
			var is_fb_project = nlapiGetFieldValue('custrecord_fb_project');
			if(is_monthly_project == 'T' || is_fb_project == 'T')
			{
				return;
			}
			
			var projectId = nlapiGetFieldValue('custrecord_project_name_revenue');

			if (projectId) {
				
				var emp_id = nlapiGetFieldValue('custrecord_resource_revenue');
				var sub_id = nlapiLookupField('employee',emp_id,'subsidiary');
				nlapiSetFieldValue('custrecord_emp_subsidiary_revenue', sub_id);
				var onsite_offsite = '';
				if(sub_id == 3)
				{
					onsite_offsite = 'Offsite';
				}
				else
				{
					onsite_offsite = 'Onsite';
				}
				nlapiSetFieldValue('custrecord_emp_onsiite_offsite_revenue', onsite_offsite);
				
				var startDate = nlapiGetFieldValue('custrecord_strt_date_revenue');
				var d_startDate = nlapiStringToDate(startDate);
				var d_endDate = nlapiAddDays(nlapiAddMonths(d_startDate, 1), -1);
				var endDate = nlapiDateToString(d_endDate, 'date');
				var today = new Date();
				
				var emp_id = nlapiGetFieldValue('custrecord_resource_revenue');
				var employee_name = nlapiGetFieldText('custrecord_resource_revenue');
				employee_name = employee_name.split('-');
				employee_name = employee_name[0];
				nlapiSetFieldValue('custrecord_end_date_revenue', endDate);
				nlapiSetFieldValue('custrecord_month_revenue',getMonthName(startDate));
				nlapiSetFieldValue('custrecord_year_revenue', getYear(d_startDate));

				var cust_territory = nlapiLookupField('customer', nlapiGetFieldValue('custrecord_cust_revenue'),'territory');
				nlapiSetFieldValue('custrecord_region_revenue',cust_territory);
				
				var projectData = nlapiLookupField('job', projectId, [
				        'jobbillingtype', 'custentity_t_and_m_monthly',
				        'customer', 'entityid', 'custentity_project_currency' ]);

				nlapiSetFieldValue('custrecord_currency_revenue',projectData.custentity_project_currency);

				var projectCurrencyValue = nlapiGetFieldValue('custrecord_currency_revenue');
				var projectCurrencyText = nlapiGetFieldText('custrecord_currency_revenue');

				//if (projectData.jobbillingtype == 'TM'
				//        && projectData.custentity_t_and_m_monthly == 'F') {

					// if its a past month, do revenue recognition
					if (d_endDate < today) {
						nlapiSetFieldValue('custrecord_recognised', 'T');
						revenueRecognition(projectData, projectId, startDate,
						        endDate, d_startDate, d_endDate,
						        projectCurrencyText, projectCurrencyValue,employee_name,emp_id);
					} else { // else projection
						revenueProjection(projectData, projectId, startDate,
						        endDate, d_startDate, d_endDate,
						        projectCurrencyText, projectCurrencyValue,employee_name,emp_id);
					}
				//}
			}
		}
	} catch (err) {
		nlapiLogExecution('ERROR', 'userEventBeforeSubmit', err);
	}
}

function revenueRecognition(projectData, projectId, startDate, endDate,
        d_startDate, d_endDate, projectCurrencyText, projectCurrencyValue,employee_name,emp_id)
{
	try {
		var recognizedAmount = 0;
		var projectName = projectData.entityid;

		
		//nlapiLogExecution('debug', 'employee_name', employee_name);

		/*var filters = [
		        new nlobjSearchFilter('formulanumeric', null, 'equalto', 0)
		                .setFormula("CASE WHEN {number} = 'Memorized' THEN 1 ELSE 0 END"),
		        new nlobjSearchFilter('formuladate', null, 'within', startDate,
		                endDate).setFormula('{trandate}'),
		        new nlobjSearchFilter('custcol_employeenamecolumn', null, 'startswith',
		                employee_name),
		        new nlobjSearchFilter('account', null, 'anyof',
		                [ '647', '644' ]) ];*/
						
		var filters = new Array();
		filters[0] = new nlobjSearchFilter('employee', null, 'anyof', emp_id);
		filters[1] = new nlobjSearchFilter('date', 'timebill', 'within', startDate, endDate);//Changed Join id by praveena on 16-03-2020
		filters[2] = new nlobjSearchFilter('customer', 'timebill', 'anyof', projectId);//Changed Join id by praveena on 16-03-2020
		
		var jeSearch = nlapiSearchRecord('timesheet', 'customsearch1520', filters, null);//Updated UI columns and filters in UI saved search
		
		nlapiLogExecution('debug', 'search data', JSON.stringify(jeSearch));
		
		var practiceWiseBreakup = {};
		//var inactivePracticeMapping = getInactivePracticeMapping();

		if (jeSearch) {
			
			var result_C 	= jeSearch[0];
			var columns 	= result_C.getAllColumns();
			var columnLen 	= columns.length;
			var total_amount = 0;
			
			for(var srch_indx=0; srch_indx<jeSearch.length; srch_indx++)
			{
				for (var j = 0; j < columnLen; j++) 
				{
					var column 		= columns[j];
					var label 		= column.getLabel();
					if (label == 'Billable Amount') 
					{
						total_amount = jeSearch[srch_indx].getValue(column);
					}
					if (label == 'Rate') 
					{
						var rate_assigend = jeSearch[srch_indx].getValue(column);
					}
					
				}//for (var j = 0; j < columnLen; j++)

			
				/*var department = je.getValue('department', null, 'group');

				if (inactivePracticeMapping[department]) {
					department = inactivePracticeMapping[department];
				}*/

				//var currency = je.getText('currency', null, 'group');
				//var amount = je.getValue('fxamount', null, 'sum');
				//var exchangeRate = nlapiExchangeRate(currency,projectCurrencyText, startDate);
				//var convertedAmount = exchangeRate * amount;
				
				if(rate_assigend)
					recognizedAmount = parseFloat(recognizedAmount) + parseFloat(total_amount);
				
				//nlapiLogExecution('debug', 'search data amount', amount);
			}
			nlapiLogExecution('debug', 'search data recognizedAmount',recognizedAmount);
			var usdExchangeRate = nlapiExchangeRate(projectCurrencyText, 'USD',endDate);
			nlapiLogExecution('debug', 'usdExchangeRate:- '+projectCurrencyText+' : USD', usdExchangeRate);
			var convertedAmount = usdExchangeRate * recognizedAmount;
			nlapiSetFieldValue('custrecord_usd_cost_revenue', recognizedAmount);
		
		}
		
	} catch (err) {
		nlapiLogExecution('ERROR', 'revenueRecognition', err);
		throw err;
	}
}

function isNumeric(n) {
	return !isNaN(parseFloat(n)) && isFinite(n);
}

function revenueProjection(projectData, projectId, startDate, endDate,
        d_startDate, d_endDate, projectCurrencyText, projectCurrencyValue,employee_name,emp_id)
{
	try {
		// get project holiday list (only working days holiday)
		var holidayList = getWorkingDayHolidays(startDate, endDate, projectId,
		        projectData.customer);

		// get practice wise revenue projection
		var practiceWiseBreakUp = getDepartmentWiseBreakup(projectId,
		        startDate, endDate, holidayList, d_startDate, d_endDate,emp_id);

		var usdExchangeRate = nlapiExchangeRate(projectCurrencyText, 'USD',
		        endDate);

		// add new lines
		var convertedAmount = 0;
		for ( var practice in practiceWiseBreakUp) {
			if (practiceWiseBreakUp[practice] < 0) {
				practiceWiseBreakUp[practice] = 0;
			}
			//convertedAmount += usdExchangeRate * practiceWiseBreakUp[practice];
			convertedAmount += practiceWiseBreakUp[practice];
		}
		nlapiLogExecution('audit','projected amount:- ',convertedAmount);
		nlapiSetFieldValue('custrecord_usd_cost_revenue', convertedAmount);
		
	} catch (err) {
		nlapiLogExecution('ERROR', 'revenueProjection', err);
		throw err;
	}
}

function getDepartmentWiseBreakup(project, startDate, endDate, holidayList,
        d_startDate, d_endDate,emp_id)
{
	try {
		var allocationSearch = nlapiSearchRecord(
		        'resourceallocation',
		        null,
		        [
		                new nlobjSearchFilter('project', null, 'anyof', project),
		                new nlobjSearchFilter('custeventrbillable', null, 'is',
		                        'T'),
		                new nlobjSearchFilter('startdate', null, 'notafter',
		                        endDate),
		                new nlobjSearchFilter('enddate', null, 'notbefore',
		                        startDate),
						new nlobjSearchFilter('resource', null, 'anyof',
		                        emp_id) ],
		        [ new nlobjSearchColumn('startdate'),
		                new nlobjSearchColumn('enddate'),
		                new nlobjSearchColumn('percentoftime'),
		                new nlobjSearchColumn('department', 'employee'),
		                new nlobjSearchColumn('subsidiary', 'employee'),
		                new nlobjSearchColumn('custevent3'),
		                new nlobjSearchColumn('custentity_hoursperday', 'job') ]);

		var departmentMainObject = {};
		var inactivePracticeMapping = getInactivePracticeMapping();

		if (allocationSearch) {

			allocationSearch
			        .forEach(function(allocation) {
				        var department = allocation.getValue('department',
				                'employee');

				        if (inactivePracticeMapping[department]) {
					        department = inactivePracticeMapping[department];
				        }

				        if (!departmentMainObject[department]) {
					        departmentMainObject[department] = 0;
				        }

				        var allocationStartDate = nlapiStringToDate(allocation
				                .getValue('startdate'));
				        var allocationEndDate = nlapiStringToDate(allocation
				                .getValue('enddate'));

				        var newStartDate = allocationStartDate > d_startDate ? allocationStartDate
				                : d_startDate;
				        var newEndDate = allocationEndDate < d_endDate ? allocationEndDate
				                : d_endDate;

				        departmentMainObject[department] += getEmployeeRevenueBetweenDates(
				                newStartDate, newEndDate, parseFloat(allocation
				                        .getValue('custevent3')), allocation
				                        .getValue('custentity_hoursperday',
				                                'job'), allocation
				                        .getValue('percentoftime'),
				                holidayList, allocation.getValue('subsidiary',
				                        'employee'));
			        });
		}

		//nlapiLogExecution('debug', 'departmentMainObject', JSON
		//        .stringify(departmentMainObject));
		return departmentMainObject;
	} catch (err) {
		nlapiLogExecution('ERROR', 'getDepartmentWiseBreakup', err);
		throw err;
	}
}

function getEmployeeRevenueBetweenDates(d_startDate, d_endDate, hourlyRate,
        hoursPerDay, percentAllocation, holidayList, subsidiary)
{
	try {
		//nlapiLogExecution('debug', 'hourlyRate', hourlyRate);
		//nlapiLogExecution('debug', 'hoursPerDay', hoursPerDay);

		if (!hoursPerDay) {
			hoursPerDay = 8;
		}

		var noOfWorkingsDays = getWorkingDays(d_startDate, d_endDate);
		//nlapiLogExecution('debug', 'noOfWorkingsDays', noOfWorkingsDays);

		// holidays between allocation dates
		var holidayCount = 0;

		holidayList.forEach(function(holiday) {

			if (d_startDate <= holiday.Date && holiday.Date <= d_endDate
			        && subsidiary == holiday.Subsidiary) {
				holidayCount += 1;
			}
		});

		// final working days
		noOfWorkingsDays -= holidayCount;
		//nlapiLogExecution('debug', 'holidayCount', holidayCount);

		percentAllocation = parseFloat((percentAllocation.split('%')[0].trim())) / 100;
		//nlapiLogExecution('debug', 'percentAllocation', percentAllocation);

		var noOfWorkingHours = noOfWorkingsDays * percentAllocation
		        * hoursPerDay;
		//nlapiLogExecution('debug', 'noOfWorkingHours', noOfWorkingHours);

		var revenue = noOfWorkingHours * hourlyRate;
		//nlapiLogExecution('debug', 'revenue', revenue);
		return revenue;
	} catch (err) {
		nlapiLogExecution('ERROR', 'getEmployeeRevenueBetweenDates', err);
		throw err;
	}
}

function getWorkingDays(d_startDate, d_endDate) {
	try {
		var numberOfWorkingDays = 0;

		for (var i = 0;; i++) {
			var currentDate = nlapiAddDays(d_startDate, i);

			if (currentDate > d_endDate) {
				break;
			}

			if (currentDate.getDay() != 0 && currentDate.getDay() != 6) {
				numberOfWorkingDays += 1;
			}
		}

		return numberOfWorkingDays;
	} catch (err) {
		nlapiLogExecution('ERROR', 'getWorkingDays', err);
		throw err;
	}
}

function getWorkingDayHolidays(start_date, end_date, project, customer) {
	var project_holiday = nlapiLookupField('job', project,
	        'custentityproject_holiday');

	if (project_holiday == 1) {
		return get_company_holidays(start_date, end_date);
	} else {
		return get_customer_holidays(start_date, end_date, customer);
	}
}

function get_company_holidays(start_date, end_date) {
	try {
		var holiday_list = [];

		var search_company_holiday = nlapiSearchRecord('customrecord_holiday',
		        'customsearch_company_holiday_search',
		        [ new nlobjSearchFilter('custrecord_date', null, 'within',
		                start_date, end_date) ], [
		                new nlobjSearchColumn('custrecord_date'),
		                new nlobjSearchColumn('custrecordsubsidiary') ]);

		if (search_company_holiday) {

			for (var i = 0; i < search_company_holiday.length; i++) {
				var holidayDate = nlapiStringToDate(search_company_holiday[i]
				        .getValue('custrecord_date'));

				if (holidayDate.getDay() != 0 && holidayDate.getDay() != 6) {
					holiday_list.push({
					    Subsidiary : search_company_holiday[i]
					            .getValue('custrecordsubsidiary'),
					    Date : holidayDate
					});
				}
			}
		}

		return holiday_list;
	} catch (err) {
		nlapiLogExecution('ERROR', 'get_company_holidays', err);
		throw err;
	}
}

function get_customer_holidays(start_date, end_date, customer) {
	var holiday_list = [];

	var search_customer_holiday = nlapiSearchRecord(
	        'customrecordcustomerholiday', 'customsearch_customer_holiday', [
	                new nlobjSearchFilter('custrecordholidaydate', null,
	                        'within', start_date, end_date),
	                new nlobjSearchFilter('custrecord13', null, 'anyof',
	                        customer) ], [
	                new nlobjSearchColumn('custrecordholidaydate', null,
	                        'group'),
	                new nlobjSearchColumn('custrecordcustomersubsidiary', null,
	                        'group') ]);

	if (search_customer_holiday) {

		for (var i = 0; i < search_customer_holiday.length; i++) {
			var holidayDate = nlapiStringToDate(search_customer_holiday[i]
			        .getValue('custrecordholidaydate', null, 'group'));

			if (holidayDate.getDay() != 0 && holidayDate.getDay() != 6) {
				holiday_list.push({
				    Subsidiary : search_customer_holiday[i].getValue(
				            'custrecordcustomersubsidiary', null, 'group'),
				    Date : holidayDate
				});
			}
		}
	}

	return holiday_list;
}

function getMonthName(currentDate) {
	currentDate = nlapiStringToDate(currentDate)
	var monthNames = [ "JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JUL", "AUG",
	        "SEP", "OCT", "NOV", "DEC" ];
	return monthNames[currentDate.getMonth()];
}

function getYear(currentDate) {
	return Math.round(currentDate.getFullYear()).toString();
}

function getInactivePracticeMapping() {
	return {
	    282 : 324,
	    292 : 325,
	    307 : 325,
	    452 : 451,
	    305 : 320,
	    432 : 328,
	    339 : 320,
	    306 : 325,
	    308 : 316,
	    345 : 326,
	    314 : 458,
	    439 : 322,
	    341 : 322,
	    333 : 326,
	    302 : 326,
	    404 : 316,
	    289 : 316,
	    402 : 326,
	    293 : 316,
	    431 : 325,
	    408 : 407,
	    337 : 318,
	    285 : 326,
	    428 : 316,
	    294 : 316,
	    309 : 316,
	    429 : 316,
	    335 : 316,
	    310 : 316,
	    300 : 326,
	    303 : 326,
	    301 : 326,
	    286 : 326,
	    287 : 326,
	    304 : 316,
	    344 : 324,
	    440 : 327
	};
}