/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 *1.00       10 Dec 2019     Prabhat
 *
 */

/**
 * @param {nlobjRequest} request Request object
 * @param {nlobjResponse} response Response object
 * @returns {Void} Any output is written via response object
 */

function suitelet(request, response){

	var method	=	request.getMethod();

	var s_step_1 = 'step-1';
	var s_step_2 = 'step-2';	
	// Get logged-in user
	var objUser = nlapiGetContext();
	var i_employee_id = objUser.getUser();
	
	var s_role	=	getRoleName(i_employee_id, objUser.getRole());
	
	var s_listing_page_url	=	nlapiResolveURL('SUITELET', 'customscript_emp_domain_mapping', 'customdeploy_emp_domain_mapping');
	
	var s_new_request_url	=	nlapiResolveURL('SUITELET', 'customscript_emp_domain_mapping', 'customdeploy_emp_domain_mapping');
	
	var o_employee_data	=	nlapiLookupField('employee', i_employee_id, ['firstname', 'lastname', 'employeestatus' , 'subsidiary','department','title']);
	var o_employee_data_subsidiary	=	nlapiLookupField('employee', i_employee_id, 'subsidiary',true);
	var emp_department = o_employee_data.department;
	var status = 1;	// Used to display success or failure at top message box
	var message = request.getParameter('custparam_message'); // Display the message at the top	
	var i_tri_id	=	request.getParameter('custparam_tri_id'); // Travel Segment ID(for edit mode)
	var i_file_id	=	request.getParameter('custparam_file_id');
	var next_step	=	request.getParameter('custparam_next_step');
	var mode = request.getParameter('custparam_mode'); // Edit mode for editing the segment or travel details
	
	var s_next_step	=	s_step_1;
	
	var s_current_step = s_next_step == null?s_step_1:s_next_step;
	
	
	if(mode == 'edit_header')
	{
		s_current_step	=	s_step_1;
	}
	else if(mode == 'edit')
	{
		s_current_step	=	s_step_2;
	}
	
	var values = new Object();
	
	values['username']	=	o_employee_data.firstname + ' ' + o_employee_data.lastname;
	values['emp_subsidiary'] = o_employee_data_subsidiary;
	values['role']		=	s_role;
	values['empdepartment'] = nlapiLookupField('employee',i_employee_id,'department',true);
	values['title'] = o_employee_data.title;
	values['new_request_url']	=	s_new_request_url;
	values['my_requests_url']	=	s_listing_page_url + '&mode=my-requests';
	values['approve_requests_url']	=	s_listing_page_url;
	values['step_1']	=	s_step_1;
	values['step_2']	=	s_step_2;
	values['enter-tr']	=	'hide';
	values['edit-tr']	=	'hide';
	values['preview_and_submit'] =	'hide';
	
	// Travel Segment Display Options
	if(mode == 'edit')
		{
			values['tri-id']	=	i_tri_id;
			values['add-edit-segment-heading']	=	'<span class="glyphicon glyphicon-pencil"></span> Add';
			values['add-edit-skill']	=	'<span class="glyphicon glyphicon-pencil"></span> Add';
			values['add-edit-segment']	=	'Update';
			values['show-hide-cancel-button']	=	'';
			values['show-hide-save'] = 'hide';
	
		}
	else
		{
			values['add-tech-role'] = '';
			values['tri-id']	=	'';
			values['add-edit-segment-heading']	=	'<span class="glyphicon glyphicon-plus"></span> Add';
			values['add-edit-skill']	=	'<span class="glyphicon glyphicon-plus"></span> Add';
			values['add-edit-segment']	=	'Save';
			values['show-hide-cancel-button']	=	'hide';
			values['show-hide-update-button'] = 'hide';
			values['step_2']= '';
		}
	
	var view_mode	=	false; // Allow editing by the user who entered the travel request
	
	var i_status	=	null;
	var i_approver_id = null;
	var i_travel_type	=	null;
	var i_visa_type	=	null;
	var i_tr_employee_id	=	null;
	if(method == 'POST')
	{
		// Check which button was clicked
		var s_button_clicked = '';
		if(next_step == 'skilladded')
		{
			s_button_clicked = 'ADD_SKILL';
		}
		else if(request.getParameter('add_segment') != null && view_mode == false)
		{
			s_button_clicked	=	'ADD_SEGMENT';
		}
		else if(request.getParameter('update_edit_segment')!= null && view_mode == false)
		{
			s_button_clicked = 'EDIT_SEGMENT';
		}
		else if(request.getParameter('submit_travel_request') != null && view_mode == false)
		{
			s_button_clicked	=	'SUBMIT_TRAVEL_REQUEST';
		}
		var o_status = saveTravelRequestDetails(request, s_button_clicked, i_status, i_approver_id,  i_travel_type, i_visa_type);
		if(o_status.status == 1)
		{
				message = o_status.message;
				
				if(s_button_clicked == 'SAVE_TRAVEL_REQUEST')
					{
						//i_tr_id	=	o_status.tr_id;
						
						mode = '';
						
						s_current_step	=	s_step_2;
						
						response.sendRedirect('SUITELET', 'customscript_emp_domain_mapping', 'customdeploy_emp_domain_mapping', false, {'custparam_next_step':s_current_step, 'custparam_message':o_status.message});
					}
				else if(s_button_clicked == 'EDIT_TRAVEL_REQUEST')
					{
						mode = '';
						
						s_current_step	=	s_step_2;
					}
		}
		else
			{
				message = o_status.message;
			
			}
	}
	else
	{	
		
		if(mode == 'delete' && view_mode == false)
		{
			// Delete travel segment
				o_status = deleteTravelRequestDetails(i_tri_id);
				message = o_status.message;
				status	=	o_status.status;	
			
		}
		
	}
	
	var file = nlapiLoadFile(2191897); //load the HTML file
	var contents = file.getValue(); //get the contents
	
	if(message == null)
	{
		values['message-display']	=	'display:none;';
	}
	else
	{
		values['message']	=	message;
		if(status == 1)
			{
				values['message-status']	=	'success';
			}
		else
			{
				values['message-status']	=	'danger';
				
				nlapiLogExecution('ERROR', 'Error: ', message);
			}
	}
	
	var recTR	=	null;
	
	if(mode != 'new_tr')
	{
		// Load the data and populate it
		values['customer-project-json']	=	JSON.stringify(o_json);
		values['script-id']				=	1990;	
		// Travel Details
		var i_travel_details_count = 0;
		var skill_fil = new Array();
		skill_fil[0] = new nlobjSearchFilter('custrecord_domian_resource',null,'anyof',i_employee_id);
		var skill_columns= new Array();
		skill_columns[0]= new nlobjSearchColumn('internalid');
		var i_travel_details_count = nlapiSearchRecord('customrecord_emp_domain_mapping',null,skill_fil,skill_columns);
		var a_travel_details	=	new Array();
		if(_logValidation(i_travel_details_count))
		{
			//values['show-role-fields'] = 'none';
			var o_travel_details_heading	=	{'highlight': '', 'edit': '<span class="glyphicon glyphicon-pencil"></span>',  'industry':'Industry Name',  'experience': 'Relavent Industry Experience', 'total_experience': 'Total Year of Experience', 'certificate':'Certification Name', 'certificate_id':'Certificate Id', 'certificate_date':'Certification Date','delete':'<span class="glyphicon glyphicon-trash"></span>'};
		
		for(var i = 1; i <= i_travel_details_count.length; i++)
				{
					var load_rec_id = i_travel_details_count[i-1].getValue('internalid');
					a_travel_details[i - 1]	=	new Object();
					var recid= nlapiLoadRecord('customrecord_emp_domain_mapping',load_rec_id);
					
					if(i_tri_id == load_rec_id)
						{
							a_travel_details[i - 1]['highlight']	=	'info';	
						}
					else
						{
							a_travel_details[i - 1]['highlight']	=	'';
						}		
					a_travel_details[i - 1]['edit']	=	'<a href="' + nlapiResolveURL('SUITELET', 'customscript_emp_domain_mapping', 'customdeploy_emp_domain_mapping') +  '&custparam_tri_id=' + load_rec_id +'&custparam_mode=edit&custparam_next_step=">Edit</a>';
					a_travel_details[i - 1]['industry']	=	recid.getFieldText('custrecord_domain_industry_name');
					a_travel_details[i - 1]['experience']	=	recid.getFieldText('custrecord_domain_experience');
					a_travel_details[i - 1]['total_experience']	=	recid.getFieldText('custrecord_domain_total_experience');
					a_travel_details[i - 1]['certificate']	=	recid.getFieldValue('custrecord_domain_certificate_name');
					a_travel_details[i - 1]['certificate_id']	=	recid.getFieldValue('custrecord_domain_certificate_id');
					a_travel_details[i - 1]['certificate_date']	=	recid.getFieldValue('custrecord_domain_certificate_date');
					a_travel_details[i - 1]['delete']	=	'<a href="' + nlapiResolveURL('SUITELET', 'customscript_emp_domain_mapping', 'customdeploy_emp_domain_mapping') + '&custparam_tri_id=' + load_rec_id +'&custparam_mode=delete&custparam_next_step='+s_current_step+'">Delete</a>';
				}
		}
		else
		{
			//values['show-role-fields'] = '';
			//values['technicalrole'] = getTechnicalRole(emp_department);
			
		}
			var strTableHtml = displayTable(o_travel_details_heading, a_travel_details, 'datetime'); // '<thead><tr><th class="edit-mode">Edit</th><th>Departure Date</th><th>Origin</th><th>Destinatin</th><th>Mode of Travel</th><th>Meal/Seat Preference</th><th>Hotel Required</th><th class="edit-mode">Delete</th></tr></thead>';
		  
			values['list'] = i_travel_details_count == 0?'<tr><td>Please Enter Technical Role and Skills</td></tr>':strTableHtml;					
			// Display Segment Form
			var entered_family =	'';
			var industry_name	=	'';
			var entered_skill_type	=	'';
			var selected_secondary	=	'';
			var selected_primary	=	'';
			var selected_origin			=	'';
			var experience	=	'';
			var total_experience = '';
			var certificate_name = '';
			var final_skill_id = '';
			var certificate_id = '';
			var certificate_date = '';
			var domain_name = '';
			
			if(mode == 'edit' && i_tri_id != null)
			{
					var recTR_Item	  = 	nlapiLoadRecord('customrecord_emp_domain_mapping', i_tri_id);
					industry_name	  =	    recTR_Item.getFieldValue('custrecord_domain_industry_name');
					domain_name       =     recTR_Item.getFieldText('custrecord_emp_domain');
					
					//Added'
				//	var customlistSearch = nlapiSearchRecord("customrecord_skills",null,[["name","is",entered_skill]], [
										//	new nlobjSearchColumn("name").setSort(false)]);
					//final_skill_id    =    customlistSearch[0].getId();	
					experience =	recTR_Item.getFieldValue('custrecord_domain_experience');
					total_experience = recTR_Item.getFieldValue('custrecord_domain_total_experience');
				  certificate_name	=	recTR_Item.getFieldValue('custrecord_domain_certificate_name');
				  certificate_id	=	recTR_Item.getFieldValue('custrecord_domain_certificate_id');
				  certificate_date	=	recTR_Item.getFieldValue('custrecord_domain_certificate_date');
				  
				  var edit_date = certificate_date;
				  
				 // edit_date = edit_date.toLocaleDateString();
				  
				   nlapiLogExecution('Debug','Wihtout certificate_date',certificate_date);
					var frmt = edit_date.split('/');
					var edit_day = frmt[1];
					var edit_month = frmt[0];
					var edit_year = frmt[2];
					
					 //certificate_date = new Date(edit_year+'-'+edit_month+'-'+edit_day);
					// var certificate_dateObj = new Date(certificate_date)
					// certificate_date = certificate_dateObj.getFullYear()+'-'+(certificate_dateObj.getMonth()+1)+'-'+certificate_dateObj.getDate()
					certificate_date = edit_year+'-'+edit_month+'-'+edit_day;
					 nlapiLogExecution('Debug','With certificate_date',certificate_date);
				  
				  
					//
			}
			// Add Segment Form Drop-downs
			values['experience']		=	getListValues('customlist_dom_allow_exp', experience, '--Please Select Relavent Experience--');
			values['total_experience']		=	getListValues('customlist_dom_allow_exp', total_experience, '--Please Select Total Experience--');
			//values['total_experience']   =  total_experience;
			values['origin'] = getSkills(industry_name, 'Select Industry', '');
		//	values['editorigin']	= getListValues('custrecord_industry_name', final_skill_id, '', true);
		    values['domain']            =    domain_name
			values['certificate_name']	=	certificate_name;
			values['certificate_id']	=	certificate_id;
			values['certificate_date']	=	certificate_date;
		}
	else
	{
		var o_json	=	getfamilySkillJSON();
		//nlapiLogExecution('Debug', 'o_json', JSON.stringify(o_json));
		// Display Input form
		//values['header-data-input']		=	displayHeaderForm(mode, recTR, i_tr_id, s_step_2, o_json);
	}

	// Display Sections based on Current Step
	{
			values['edit-tr']	=	'show';
	}	
	contents = replaceValues(contents, values);
	response.write(contents); //render it on the suitelet
	
	var context	=	nlapiGetContext();
	nlapiLogExecution('AUDIT', 'Page: ' + s_current_step + ', Mode: ' + mode + ', Remaining Usage: ', context.getRemainingUsage());
	
}

function saveTravelRequestDetails(request, s_button, i_status, i_approving_manager,  i_travel_type, i_visa_type)
{
	
	if(s_button == 'ADD_SEGMENT')
	{
		try
		{
			var objUser = nlapiGetContext();
			nlapiLogExecution('DEBUG','SAVE ACTION',request.getParameter('custparam_next_step'));
			var i_employee_id = objUser.getUser();
			var i_employee_subsidiary = nlapiLookupField('employee', i_employee_id, ['department']);
			var o_travel_data = new Object();
			var s_next_step	=	request.getParameter('custparam_next_step');
			var s_mode	=	'';
			var skill_value = request.getParameter('project');
		//	var skill_selected = nlapiLookupField('customrecord_employee_skills',skill_value,'custrecord_emp_primary_skill',true);
			o_travel_data['industry'] = request.getParameter('industry');
			o_travel_data['domain'] = request.getParameter('domain');
			o_travel_data['experience'] = request.getParameter('experience');	
			o_travel_data['total_experience'] = request.getParameter('total_experience');
			o_travel_data['certificate_name']	=	request.getParameter('certificate_name');
			o_travel_data['certificate_id']	=	request.getParameter('certificate_id');
			
			var date = request.getParameter('certificate_date');
			var frmt = date.split('-');
			var day = frmt[2];
			var month = frmt[1];
			var year = frmt[0];
			
			var ns_date = month+'/'+day+'/'+year;
			
			o_travel_data['certificate_date']	= ns_date;
		//	o_travel_data['certificate_date']	=	request.getParameter('certificate_date');
			o_travel_data['tri_id']	=	request.getParameter('tri_id');
			//o_travel_data['technicalrole']	=	request.getParameter('technicalrole');
			 
			var recTR	=	null;
			//if(o_travel_data.tri_id != 'null' && o_travel_data.tri_id != '')
			if(_logValidation(o_travel_data.tri_id))
			{
				recTR	=	nlapiLoadRecord('customrecord_emp_domain_mapping', o_travel_data.tri_id);
				s_mode	=	'Updated';
			}
			else
			{
				recTR	=	nlapiCreateRecord('customrecord_emp_domain_mapping');
				s_mode	=	'Added';
			}
			recTR.setFieldValue('custrecord_domian_resource',i_employee_id);
			nlapiLogExecution('AUDIT','o_travel_data.industry',o_travel_data.industry);
			recTR.setFieldValue('custrecord_domain_industry_name', parseInt(o_travel_data.industry));
			recTR.setFieldValue('custrecord_domain_experience', o_travel_data.experience);
			recTR.setFieldValue('custrecord_domain_total_experience', o_travel_data.total_experience);
			recTR.setFieldValue('custrecord_domain_certificate_name', o_travel_data.certificate_name);
			recTR.setFieldValue('custrecord_domain_certificate_id', o_travel_data.certificate_id);
			recTR.setFieldValue('custrecord_domain_certificate_date', o_travel_data.certificate_date);
			
			
								var filters = new Array();
								
								filters[0] = new nlobjSearchFilter( 'internalid', null, 'is', parseInt(o_travel_data.industry));
								  
								var columns = new Array();
								columns[0] = new nlobjSearchColumn( 'name' );
								columns[1] = new nlobjSearchColumn( 'custrecord_domain' ); 
								
								
								var searchresults = nlapiSearchRecord( 'customrecord_emp_industry', null, filters, columns );	
			
			
				
				var industry_domain = '';
			
			for(var i=0;i<searchresults.length;i++){
				
				industry_domain = searchresults[i].getValue('custrecord_domain');
				
			}		
			
			
			recTR.setFieldValue('custrecord_emp_domain', industry_domain);
			//recTR.setFieldValue('custrecord_resource_technical_role',o_travel_data.technicalrole);
			var i_rec_id = nlapiSubmitRecord(recTR);	
			
				return ({'status': 1, 'message': 'Domain ' + s_mode, 'tr_id': i_rec_id});
			}
		catch(e)
		{
				return ({'status': -1, 'message': 'Error: ' + e.message, 'tr_id': null});
		}
	}
	else if(s_button = 'ADD_SKILL')
	{
		var new_skill = request.getParameter('newskill');
		var n_skill_filter = new nlobjSearchFilter('name',null,'is',new_skill);
		var skillSearch = nlapiSearchRecord('customrecord_skills',null,n_skill_filter,null);
		if(_logValidation(skillSearch))
		{
			return({'status' : -1,'message': 'Skill Already Existed In The System.','tr_id':null});
		}
		else
		{
			var new_skill_recrd = nlapiCreateRecord('customrecord_skills');
			new_skill_recrd.setFieldValue('name',new_skill);
			new_skill_recrd.setFieldValue('custrecord_created_byuser','T');
			var i_skill_rec_id = nlapiSubmitRecord(new_skill_recrd,true);
			return ({'status': 1, 'message': 'New Skill Added' , 'skill_id': i_skill_rec_id});
		}
	}
	else if(s_button = 'EDIT_SEGMENT')
	{
		//
		try
		{
			var objUser = nlapiGetContext();
			nlapiLogExecution('DEBUG','SAVE ACTION',request.getParameter('custparam_next_step'));
			var i_employee_id = objUser.getUser();
			var i_employee_subsidiary = nlapiLookupField('employee', i_employee_id, ['department','custentity_reportingmanager']);
			var o_travel_data = new Object();
			var s_next_step	=	request.getParameter('custparam_next_step');
			var s_mode	=	'';
			var skill_value = request.getParameter('editproject');
			if(!isNaN(skill_value))
			{
				var skill_value = nlapiLookupField('customrecord_employee_skills',skill_value,'custrecord_emp_primary_skill',true);
			}
			o_travel_data['family'] = request.getParameter('editfamily');
			o_travel_data['proficiency'] = request.getParameter('editproficiency');
			o_travel_data['skill'] = skill_value//skill details
			o_travel_data['skilltype']	=	request.getParameter('skilltype');
			o_travel_data['secondary'] = request.getParameter('skilltype');
			o_travel_data['certificate']	=	request.getParameter('editcertificate');
			o_travel_data['tri_id']	=	request.getParameter('custparam_tri_id');
			var recTR	=	null;
			//if(o_travel_data.tri_id != 'null' && o_travel_data.tri_id != '')
			if(_logValidation(o_travel_data.tri_id))
			{
				recTR	=	nlapiLoadRecord('customrecord_emp_skill_master_data', o_travel_data.tri_id);
				s_mode	=	'Updated';
			}
			recTR.setFieldValue('custrecord_family_list_master', o_travel_data.family);
			recTR.setFieldValue('custrecord_skill_pri_list', o_travel_data.skill);
			if(o_travel_data.skilltype == 'primary')
			{
				recTR.setFieldValue('custrecord_primary_check', 'T');
				recTR.setFieldValue('custrecord_second_check', 'F');
			}
			else 
			{
				recTR.setFieldValue('custrecord_second_check', 'T');
				recTR.setFieldValue('custrecord_primary_check', 'F');
			}
			recTR.setFieldValue('custrecord_proficieny_level', o_travel_data.proficiency);
			recTR.setFieldValue('custrecord_certificate', o_travel_data.certificate);
			recTR.setFieldValue('custrecord_skill_resource', i_employee_id);
			recTR.setFieldValue('custrecord_skill_emp_dept', i_employee_subsidiary.department);
			recTR.setFieldValue('custrecord_skill_approver', i_employee_subsidiary.custentity_reportingmanager);
			recTR.setFieldValue('custrecord_skill_approval_status', 1);
			var i_rec_id = nlapiSubmitRecord(recTR, true);	
			
				return ({'status': 1, 'message': 'Skills' + s_mode, 'tr_id': i_rec_id});
		}
		catch(e)
		{
				return ({'status': -1, 'message': 'Error: ' + e.message, 'tr_id': null});
		}
		//
	}
		
	
}

// Delete Travel Request line item
function deleteTravelRequestDetails(i_tri_id)
{
	try
	{
		nlapiDeleteRecord('customrecord_emp_domain_mapping', i_tri_id);
		
		return ({'status': 1, 'message': 'Domain Record Deleted', 'tr_id': i_tri_id});
	}
	catch(e)
	{
		return ({'status': -1, 'message': 'Error: ' + e.message, 'tr_id': null});
	}
}

// Used to display the html, by replacing the placeholders
function replaceValues(content, oValues)
{
	for(param in oValues)
		{
			// Replace null values with blank
			var s_value	=	(oValues[param] == null)?'':oValues[param];
			
			// Replace content
			content = content.replace(new RegExp('{{' + param + '}}','gi'), s_value);
		}
	
	return content;
}

// Get Options for select dropdown
// a_data: Array of options to be added
// i_selected_value: The option to be selected
// s_placeholder: Text to be displayed for empty value
// removeBlankOptions: If no empty value required
function getOptions(a_data, i_selected_value, s_placeholder, removeBlankOptions)
{
	var strOptions	=	'';
	
	if(removeBlankOptions == true)
		{
		
		}
	else
		{
			strOptions = '<option value = "">' + (s_placeholder == undefined?'':s_placeholder) + '</option>';
		}

	var strSelected = '';
	
	for(var i = 0; i < a_data.length; i++)
	{
		if(i_selected_value == a_data[i].value)
			{
				strSelected = 'selected';
			}
		else
			{
				strSelected = '';
			}
		strOptions += '<option value = "'+a_data[i].value+'" ' + strSelected + ' >' + a_data[i].display + '</option>';
	}
//nlapiLogExecution('Debug', 'stringsss', strOptions);
	return strOptions;
}

/**
 * @param a_data - data to generate the radio buttons
 * @param s_control_name - name and id for the generated control
 * @param s_selected_value - the selected option
 * @returns - the HTML for the radio buttons
 */

function getCheckBoxOptions(a_data, s_control_name, s_selected_value)
{
	var strOptions = '';

	for(var i = 0; i < a_data.length; i++)
	{
		var isSelected = '';
		if(a_data[i].value == s_selected_value)
			{
				isSelected = 'checked';
			}
		strOptions += ' <input type="radio" ' + isSelected + ' id="' + s_control_name + '" name="' + s_control_name + '" value = "'+a_data[i].value+'"><span> ' + a_data[i].display + '</span>';
	}

	return strOptions;
}
function getProjects(i_employee_id)
{
	var columns = new Array();
	columns[0] = new nlobjSearchColumn('company');
	
	var resource_allocations = nlapiSearchRecord('resourceallocation', null, [new nlobjSearchFilter('resource', null, 'anyof', i_employee_id)], columns);
	
	var project_list = new Array();
	
	for(var i = 0; resource_allocations != null && i < resource_allocations.length; i++)
		{
			project_list.push({'display':resource_allocations[i].getText(columns[0]), 'value':resource_allocations[i].getValue(columns[0])});
		}
	
	return project_list;
}

function getCustomerProjectList()
{
	var filters =	new Array();
	filters[0]	=	new nlobjSearchFilter('status',null,'noneof',1);
	var columns	=	new Array();
	columns[0]	=	new nlobjSearchColumn('internalid');
	columns[1]	=	new nlobjSearchColumn('jobname');
	columns[2]	=	new nlobjSearchColumn('internalid', 'customer');
	columns[3]	=	new nlobjSearchColumn('altname', 'customer');
	columns[4]	=	new nlobjSearchColumn('entityid');
	
	var searchResults	=	searchRecord('job', null, filters, columns);
	
	var a_data	=	new Array();
	
	for(var i = 0; i < searchResults.length; i++)
		{
			var s_project_id	=	searchResults[i].getValue(columns[0]);
			var s_project_name	=	searchResults[i].getValue(columns[1]);
			var s_customer_id	=	searchResults[i].getValue(columns[2]);
			var s_customer_name	=	searchResults[i].getValue(columns[3]);
			var s_project_entityid =searchResults[i].getValue(columns[4]);
			
			
			a_data.push({'project_id': s_project_id, 'project_name': s_project_name, 'customer_id': s_customer_id, 'customer_name': s_customer_name,'entityid' : s_project_entityid});
		}
	
	return a_data;
}
function getFamilySkillList()
{
	var filters =	new Array();
	filters[0]	=	new nlobjSearchFilter('isinactive',null,'is','F');
	var columns	=	new Array();
	columns[0]	=	new nlobjSearchColumn('internalid');
	columns[1]	=	new nlobjSearchColumn('custrecord_emp_primary_skill');
	columns[2]	=	new nlobjSearchColumn('internalid', 'custrecord_employee_skil_family');
	columns[3]	=	new nlobjSearchColumn('custrecord_employee_skil_family');
	//columns[4]	=	new nlobjSearchColumn('entityid');
	
	var searchResults	=	searchRecord('customrecord_employee_skills', null, filters, columns);
	
	var a_data	=	new Array();
	
	for(var i = 0; i < searchResults.length; i++)
		{
			var s_skill_id	=	searchResults[i].getValue(columns[0]);
			var s_skill_name	=	searchResults[i].getText(columns[1]);
			var s_family_id	=	searchResults[i].getValue(columns[2]);
			var s_family_name	=	searchResults[i].getValue(columns[3]);
			//var s_project_entityid =searchResults[i].getValue(columns[4]);
			
			
			a_data.push({'skill_id': s_skill_id, 'skill_name': s_skill_name, 'family_id': s_family_id, 'family_name': s_family_name});
		}
	
	return a_data;
}

function getFamilyDropdown(o_json, i_project_id)
{
	var list	=	new Array();
	
	var i_selected_value	=	'';
	
	for(var x in o_json)
	{
			list.push({'display': o_json[x].name, 'value': x});
			
			for(var i = 0; i < o_json[x].list.length; i++)
				{
					if(o_json[x].list[i].id	==	i_project_id)
						{
							i_selected_value	=	x;
						}
				}
		
	}
	
	return getOptions(list, i_selected_value);
}

function getSkillDropdown(o_json, i_project_id)
{
	
	var list	=	new Array();
	
	var i_customer_id	=	null;
	
	for(var x in o_json)
		{	
			for(var i = 0; i < o_json[x].list.length; i++)
			{
				if(o_json[x].list[i].id	==	i_project_id)
				{
					i_customer_id	=	x;
				}
			}
				
		}
	
	if(i_customer_id != null)
		{
		var display_project = new Array();
			for(i = 0; i < o_json[i_customer_id].list.length; i++)
			{
				nlapiLogExecution('Debug', 'getProjectDropdown',JSON.stringify(o_json) ); 
				display_project[i]= o_json[i_customer_id].list[i].value;
				//nlapiLogExecution('Audit', 'projects', display_project[i]);
				list.push({'display':display_project[i], 'value':o_json[i_customer_id].list[i].id});
			}
		}
	
	return getOptions(list, i_project_id);
}
function getfamilySkillJSON()
{
	var a_data	=	getFamilySkillList();
	
	var o_json	=	new Object();
	
	for(var i = 0; i < a_data.length; i++)
		{
			
			var o_project	=	{'id':a_data[i].skill_id, 'value':a_data[i].skill_name};
			
			if(o_json[a_data[i].family_id] != undefined)
				{
					o_json[a_data[i].family_id].list.push(o_project);
				}
			else
				{
					o_json[a_data[i].family_id]	=	{'name': a_data[i].family_name, 'list': [o_project]};
				}
		}
	
	return o_json;
	
}

// Get Employee List
function getEmployeeList()
{
	var filters = new Array();
	filters[0]	=	new nlobjSearchFilter('isinactive', null, 'is', 'F');	
	
	var columns = new Array();
	columns[0]	=	new nlobjSearchColumn('entityid').setSort();
	//columns[1]	=	new nlobjSearchColumn('custrecord_trc_country');
	
	var a_search_results = searchRecord('employee', null, filters, columns);
	
	var list_values = new Array();
	
	for(var i = 0; a_search_results != null && i < a_search_results.length; i++)
		{
			list_values.push({'display':a_search_results[i].getValue('entityid'), 'value':a_search_results[i].id});
		}
	
	return getOptions(list_values, null, '--Please Select--');
}


function mySort(obj_array) {
  obj_array.sort(function(a, b){return a.id - b.id});
 // return obj_array;
}




function getListValues(s_list_name, strSelectedValue, s_placeholder, removeBlankOption)
{
	var columns = new Array();
	columns[0]	=	new nlobjSearchColumn('name');
	
	var a_search_results = searchRecord(s_list_name, null, null, columns);
	
	 mySort(a_search_results);
	
	var list_values = new Array();
	
	for(var i = 0; a_search_results != null && i < a_search_results.length; i++)
		{
			list_values.push({'display':a_search_results[i].getValue('name'), 'value':a_search_results[i].id});
		}
	
	return getOptions(list_values, strSelectedValue, s_placeholder, removeBlankOption);
}

function getListValuesArray(s_list_name)
{
	var columns = new Array();
	columns[0]	=	new nlobjSearchColumn('name');
	
	var a_search_results = searchRecord(s_list_name, null, null, columns);
	
	var list_values = new Array();
	
	for(var i = 0; a_search_results != null && i < a_search_results.length; i++)
		{
			list_values.push({'display':a_search_results[i].getValue('name').setSort(), 'value':a_search_results[i].id});
		}
		
	//	list_values.sort();
	
	return list_values;
}

function getCheckBoxes(s_list_name, s_control_name, s_selected_value)
{
	var columns = new Array();
	columns[0]	=	new nlobjSearchColumn('name');
	
	var a_search_results = searchRecord(s_list_name, null, null, columns);
	
	var list_values = new Array();
	
	for(var i = 0; a_search_results != null && i < a_search_results.length; i++)
		{
			list_values.push({'display':a_search_results[i].getValue('name'), 'value':a_search_results[i].id});
		}
	
	return getCheckBoxOptions(list_values, s_control_name, s_selected_value);
}

function updateChecklist(a_list, i_tr_id)
{
	var recTR	=	nlapiLoadRecord('customrecord_travel_request', i_tr_id);
	
	var i_checklist_count	=	recTR.getLineItemCount('recmachcustrecord_trmc_travel_request');
	
	for(var i = 0; i < a_list.length; i++)
		{
			// Loop through checklist
			for(var j = 1; j <= i_checklist_count; j++)
				{
					if(recTR.getLineItemValue('recmachcustrecord_trmc_travel_request', 'custrecord_trmc_item', j) == a_list[i].id)
						{
							// Update Checklist Record
							recTR.setLineItemValue('recmachcustrecord_trmc_travel_request', 'custrecord_trmc_value', j, a_list[i].checked);
						}
				}	
		}
	
	nlapiSubmitRecord(recTR);
}

function displayHeaderForm(edit_mode, recTR, i_tr_id, s_next_step, o_json)
{
	var file = nlapiLoadFile(57171); //load the HTML file
	var contents = file.getValue(); //get the contents
  
	var o_values	=	new Object();
	
	if(edit_mode == 'edit_header' && recTR != null)
	{
		o_values.i_tr_id		=	i_tr_id;
		o_values.travel_type	=	recTR.getFieldValue('custrecord_tr_travel_type');
		o_values.request_type	=	recTR.getFieldValue('custrecord_tr_request_type');
		o_values.travel_purpose	=	recTR.getFieldValue('custrecord_tr_purpose_of_travel');
		o_values.country		=	recTR.getFieldValue('custrecord_tr_country');
		o_values.short_desc		=	recTR.getFieldValue('custrecord_tr_short_description');
		o_values.project		=	recTR.getFieldValue('custrecord_tr_project');
		o_values.work_address	=	recTR.getFieldValue('custrecord_tr_work_address');
		o_values.zip_code		=	recTR.getFieldValue('custrecord_tr_zip_code');
		o_values.onsite_duration	=	recTR.getFieldValue('custrecord_tr_onsite_duration');
		o_values.onsite_contact_number	=	recTR.getFieldValue('custrecord_tr_onsite_contact_number');
		o_values.per_diem		=	recTR.getFieldValue('custrecord_tr_per_diem');
		o_values.travel_advance_currency	=	recTR.getFieldValue('custrecord_tr_advance_currency');
		o_values.forex_or_advance	=	recTR.getFieldValue('custrecord_tr_forex_or_advance');
		o_values.tr_remarks		=	recTR.getFieldValue('custrecord_tr_remarks');
		o_values.visa_category		=	recTR.getFieldValue('custrecord_tr_visa_category');
		o_values.mobile_number	=	recTR.getFieldValue('custrecord_tr_mobile_number');
		o_values.subsidiary  	= 	recTR.getFieldText('custrecord_tr_subsidiary');
	}
	else
	{
		o_values.travel_type	=	1;
		o_values.request_type	=	1;
		// o_values.travel_advance_currency	=	1;
	}
	
	if(edit_mode == 'edit_header')
	{
		o_values.button_name	=	'edit_tr';
	}
	else
	{
		o_values.button_name	=	'save_tr';
		s_next_step				=	'step-2';
	}
	
	// Get logged-in user
	var objUser = nlapiGetContext();
  
	var i_employee_id = objUser.getUser();
  
	var o_employee_data	=	nlapiLookupField('employee', i_employee_id, ['firstname', 'lastname', 'Subsidiary','title']);
    var o_employee_data_subsidiary	=	nlapiLookupField('employee', i_employee_id, 'subsidiary',true);
	//var a_project_data = getProjects(i_employee_id);
	var strFamilyOptions = getFamilyDropdown(o_json , o_values.project);
  

	var strSkillOptions = getSkillDropdown(o_json , o_values.project);
	//nlapiLogExecution('Debug', 'proj list', JSON.stringify(strProjectOptions));
	//nlapiLogExecution('Audit', 'proj list', strProjectOptions);
	
	var values	=	new Object();
	values['script-id']			=	1990;
	values['tr-id']				=	o_values.i_tr_id;
	values['next-step']			=	s_next_step;
	values['button_name']		=	o_values.button_name;
	
	values['family']	=	strFamilyOptions;
	//values['project_options']	=	strProjectOptions;
	values['origin']	=	strSkillOptions;

	
	contents	=	replaceValues(contents, values);
	
	return contents;
}

function saveTravelRequest(o_travel, mode)
{
	
}

// Display Table
function displayTable(o_headings, a_data, date_format)
{
	var strHtml = '';
	
	var enableHighlight	=	false;
	
	var s_highlight	=	'';
	
	// Display Header
	strHtml += '<thead><tr>';
	for(var s_field in o_headings)
		{
			if(s_field != 'highlight')
				{
					strHtml += '<th class="' + ((s_field == 'edit' || s_field == 'delete')?'edit-mode':'') + '" style="' + ((s_field == 'edit' || s_field == 'delete')?'text-align:center;':'') + '">' + o_headings[s_field] + '</th>';				
				}
			else
				{
					enableHighlight	=	true;
				}
		}
	strHtml += '</tr></thead>';
	
	for(var i = 0; i < a_data.length; i++)
		{
			
			if(enableHighlight == true)
				{
					s_highlight	=	a_data[i]['highlight'];
				}
			else
				{
					s_highlight	=	'';
				}
			
			strHtml	+=	'<tr class="' + s_highlight + '">';
			for(var s_field in o_headings)
				{
					if(s_field != 'highlight')
					{
						var s_value	=	a_data[i][s_field];
						if(s_value == null)
							{
								s_value	=	'';
							}
						else if(isDate(s_value))
							{
								s_value	=	nlapiDateToString(s_value, date_format);
							}
						
						strHtml	+=	'<td class="' + ((s_field == 'edit' || s_field == 'delete')?'edit-mode':'') + '" style="' + ((s_field == 'edit' || s_field == 'delete')?'text-align:center;':'') + '">' + s_value + '</td>';						
					}	
				}
			strHtml	+=	'</tr>';
		}
	
	return strHtml;
}

// Sort Data
function compare(a,b)
{
	if (a.sort_value < b.sort_value)
		return -1;
	if (a.sort_value > b.sort_value)
		return 1;
	return 0;
}

// Dynamic sort
function dynamicSort(property) {
    var sortOrder = 1;
    if(property[0] === "-") {
        sortOrder = -1;
        property = property.substr(1);
    }
    return function (a,b) {
        var result = (a[property] < b[property]) ? -1 : (a[property] > b[property]) ? 1 : 0;
        return result * sortOrder;
    };
}

// Change from dd-MMM-yyyy to mm/dd/yyyy format
function convertToDateFormat(input) {
    var pattern = /(.*?)-(.*?)-(.*?)$/;
    var result = input.replace(pattern,function(match,p1,p2,p3){
        var months = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
        return (months.indexOf(p2) + 1) + "/" + (p1<10?p1.substring(1):p1) + "/" + p3;
    });

    return result;
}

// Change from mm/dd/yyyy format to dd-MMM-yyyy
function convertToDateFormat1(input) {
    var pattern = /(.*?)\/(.*?)\/(.*?)$/;
    var result = input.replace(pattern,function(match,p1,p2,p3){
        var months = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
        return (p2<10?"0"+p2:p2) + "-" + months[(p1-1)] + "-" + p3;
    });

    return result;
}

// Format Date
function GetFormattedDate(d_date) {
    var todayTime = d_date;
    var month = format(todayTime .getMonth() + 1);
    var day = format(todayTime .getDate());
    var year = format(todayTime .getFullYear());
    return month + "/" + day + "/" + year;
}

// Check if Date object
function isDate(myDate) {
    return myDate.constructor.toString().indexOf("Date") > -1;
}



// Get Role Name
function getRoleName(i_user_id, i_role_id)
{
	var a_search_results	=	nlapiSearchRecord('employee', null, [new nlobjSearchFilter('internalid', null, 'anyof', i_user_id), new nlobjSearchFilter('role', null, 'anyof', i_role_id)], new nlobjSearchColumn('role'));
	
	if(a_search_results != null && a_search_results.length == 1)
		{
			return a_search_results[0].getText('role');
		}
	else
		{
			return '';
		}
}
function getSkills(strSelectedValue, s_placeholder, removeBlankOption)
{
try{//
 
var skill_filter =['isinactive','anyOf', 'F' ];

		var practice_search_results = searchRecord('customrecord_emp_industry', null, skill_filter,
		        [ new nlobjSearchColumn("name"), new nlobjSearchColumn("custrecord_domain"),
		               	new nlobjSearchColumn("internalid")]);
	var list_values = new Array();
	
	for(var i = 0; practice_search_results != null && i < practice_search_results.length; i++)
		{ 
						list_values.push({'display':practice_search_results[i].getValue('name'), 'value':practice_search_results[i].id, 'domain':practice_search_results[i].getText('custrecord_domain')});
		}
	
	return getOptions(list_values,strSelectedValue,s_placeholder);

}catch(err){
	throw err;
	}
}

function getTechnicalRole(emp_dep,strSelectedValue, s_placeholder, removeBlankOption)
{
try{
		//var parent_practice = nlapiLookupField('department',emp_dep,'custrecord_parent_practice');
		var parent_dep = nlapiLookupField('department',emp_dep,'custrecord_parent_practice');
		var role_fil = new Array();
		role_fil[0] = new nlobjSearchFilter('custrecord_fuel_role_mapping_practice',null,'anyof',parent_dep);
		var role_columns= new Array();
		role_columns[0]= new nlobjSearchColumn('custrecord_fuel_role_mapping_name');
		var role_details = nlapiSearchRecord('customrecord_fuel_role_mapping_skill',null,role_fil,role_columns);
		var list_values = new Array();
	
		for(var i = 0; role_details != null && i < role_details.length; i++)
		{ 
			list_values.push({'display':role_details[i].getValue('custrecord_fuel_role_mapping_name'), 'value':role_details[i].id});
		}
	return getOptions(list_values,strSelectedValue);

}catch(err){
	throw err;
	}
}
function getTaggedSkills(emp_family,strSelectedValue, s_placeholder, removeBlankOption)
{
try{
	
	var filters =	new Array();
	filters[0]	=	new nlobjSearchFilter('isinactive',null,'is','F');
	filters[1]=   new nlobjSearchFilter('custrecord_employee_skil_family',null,'anyof',emp_family);
	var columns	=	new Array();
	columns[0]	=	new nlobjSearchColumn('internalid');
	columns[1]	=	new nlobjSearchColumn('custrecord_emp_primary_skill');
	columns[2]	=	new nlobjSearchColumn('internalid', 'custrecord_employee_skil_family');
	columns[3]	=	new nlobjSearchColumn('custrecord_employee_skil_family');
	//columns[4]	=	new nlobjSearchColumn('entityid');
	
	var searchResults	=	searchRecord('customrecord_employee_skills', null, filters, columns);
	
	var list_values = new Array();
	
	for(var i = 0; searchResults != null && i < searchResults.length; i++)
		{ 
				list_values.push({'display':searchResults[i].getText('custrecord_emp_primary_skill'), 'value':searchResults[i].getText('custrecord_emp_primary_skill')});
		}
	
	return getOptions(list_values,strSelectedValue,s_placeholder, removeBlankOption);

}catch(err){
	throw err;
	}
}
function _logValidation(value) 
{
 if(value!='null' && value != null && value != null && value != '' && value != undefined && value != undefined && value != 'undefined' && value!= 'undefined'&& value!= 'NaN' && value != NaN) 
 {
  return true;
 }
 else 
 { 
  return false;
 }
 }