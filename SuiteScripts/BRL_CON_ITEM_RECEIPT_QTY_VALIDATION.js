/**
 *@NApiVersion 2.x
 *@NScriptType ClientScript
 */
define(['N/record', 'N/currentRecord'], function (record, currentRecord) {

    var item_arr = [];

    function pageInit(context) {

        setTimeout(function () {

            var rec = context.currentRecord;

            if (context.mode == 'create' || context.mode == 'copy' || context.mode == "edit") {

                var line_count = rec.getLineCount({
                    sublistId: 'item'
                });

                for (i = 0; i < line_count; i++) {

                    var item = rec.getSublistValue({
                        sublistId: 'item',
                        fieldId: 'item',
                        line: i
                    });

                    // console.log("item " + item);

                    var qty = rec.getSublistValue({
                        sublistId: "item",
                        fieldId: "quantity",
                        line: i
                    });

                    // console.log("qty " + qty);

                    item_arr.push({
                        item: item,
                        qty: qty,
                        line: i
                    });

                    rec.selectLine({
                        sublistId: "item",
                        line: i
                    });

                    rec.setCurrentSublistValue({
                        sublistId: "item",
                        fieldId: "custcol_custom_qty",
                        value: qty
                    });


                }

                // console.log(item_arr);

            }
        }, 1000)
    }

    function fieldChanged(context) {

        if(context.fieldId == "itemreceive"){
            var check_rec = context.currentRecord;

            var receive_box = check_rec.getCurrentSublistValue({
                sublistId: "item",
                fieldId: "itemreceive"
            });

            var line = context.line;

            if(receive_box == false){
                check_rec.setCurrentSublistValue({
                    sublistId: "item",
                    fieldId: 'custcol_custom_qty',
                    value: ''
                });
            }else {
                check_rec.setCurrentSublistValue({
                    sublistId: "item",
                    fieldId: 'custcol_custom_qty',
                    value: item_arr[line].qty
                });
            }
        }

        if (context.fieldId == "custcol_custom_qty") {

            if (item_arr.length > 0) {
                var fld_rec = context.currentRecord;
                // console.log("fld_rec " + fld_rec);

                var fld_qty = fld_rec.getCurrentSublistValue({
                    sublistId: "item",
                    fieldId: 'custcol_custom_qty'
                });

                // console.log("fld_qty " + fld_qty);

                // console.log("line " + context.line);

                for (var i = 0; i < item_arr.length; i++) {
                    // console.log("test");
                    // console.log(item_arr[i].item);
                    // console.log(item_arr[i].line);
                    if (item_arr[i].line == context.line) {
                        // console.log("test1");
                        if (item_arr[i].qty < fld_qty) {
                            alert('THE VALUE SHOULD NOT MORE THEN ' + item_arr[i].qty);

                            fld_rec.setCurrentSublistValue({
                                sublistId: "item",
                                fieldId: 'custcol_custom_qty',
                                value: item_arr[i].qty
                            });

                            break;
                        }else if(fld_qty <= item_arr[i].qty){
                            // console.log("test1");
                            fld_rec.setCurrentSublistValue({
                                sublistId: "item",
                                fieldId: 'quantity',
                                value: fld_qty
                            });

                            break;
                        }
                    }

                }
            }


        }

    }

    return {
        pageInit: pageInit,
        fieldChanged: fieldChanged
    }
});