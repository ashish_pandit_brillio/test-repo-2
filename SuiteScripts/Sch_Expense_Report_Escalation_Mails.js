/**
 * Module Description
 * 
 * Version Date Author Remarks 1.00 12 Feb 2015 nitish.mishra
 * 
 */

var Expense_Report_Status = {
	// OnGoing : '1',
	PendingSupervisorApproval : 'ExpRept:B',
	PendingAccountingApproval : 'ExpRept:C'
};

/**
 * @param {String}
 *            type Context Types: scheduled, ondemand, userinterface, aborted,
 *            skipped
 * @returns {Void}
 */
function scheduled(type) {
	nlapiLogExecution('DEBUG', 'SCRIPT', 'STARTED');

	nlapiLogExecution('debug', 'first level escalation', 'started');
	// var failedRecord1 = firstLevelEscalation();
	nlapiLogExecution('debug', 'first level escalation', 'ended');

	nlapiLogExecution('debug', 'second level first escalation', 'started');
	// var failedRecord2 = secondLevelFirstEscalation();
	nlapiLogExecution('debug', 'second level first escalation', 'ended');

	nlapiLogExecution('debug', 'second level second escalation', 'started');
	var failedRecord3 = secondLevelSecondEscalation();
	nlapiLogExecution('debug', 'second level second escalation', 'ended');

	// if (failedRecord1.length > 0) {
	// nlapiSendEmail(constant.Employee.InformationSystem,
	// 'nitish.mishra@brillio.com', "Failed Emails 1st 6days", JSON
	// .stringify(failedRecord1));
	// }

	// if (failedRecord2.length > 0) {
	// nlapiSendEmail(constant.Employee.InformationSystem,
	// 'nitish.mishra@brillio.com',
	// "Failed Emails 2nd 5days", JSON.stringify(failedRecord2));
	// }

	if (failedRecord3.length > 0) {
		nlapiSendEmail(constant.Employee.InformationSystem,
				'nitish.mishra@brillio.com', "Failed Emails 2nd 8days", JSON
						.stringify(failedRecord3));
	}

	nlapiLogExecution('DEBUG', 'SCRIPT', 'ENDED');
}

function firstLevelEscalation() {
	try {
		var expenseRecords = getExpenseForFirstLevelEscalation();
		var expenseId = null;
		var failedMails = [];
		var context = nlapiGetContext();
		var hrHead = getHumanResourceHead();

		for (var index = 0; index < expenseRecords.length; index++) {
			expenseId = expenseRecords[index];
			yieldScript(context);
			try {
				var expenseDetails = getExpenseDetails(expenseId);
				var mailContent = firstLevelEscalationMailTemplate(expenseDetails);
				var departmentHeadEmail = getDepartmentHeadEmail(expenseDetails.Employee.internalid);
				nlapiLogExecution('debug', 'expense details', JSON
						.stringify(expenseDetails));

				nlapiSendEmail(
						constant.Mail_Author.InformationSystem,
						expenseDetails.FirstLevelApprover.email,
						mailContent.Subject,
						mailContent.Body,
						[ expenseDetails.Employee.email, departmentHeadEmail,
								hrHead ],
						null,
						{
							entity : expenseDetails.FirstLevelApprover.internalid,
							transaction : expenseId
						});
			} catch (er) {
				nlapiLogExecution('Error', 'First Level Escalation Failed',
						'expense id : ' + expenseId + " ; Error " + er);
				failedMails.push({
					State : 'First Escalation Level',
					Id : expenseId,
					ErrorMessage : er
				});
			}
		}

		return failedMails;
	} catch (err) {
		nlapiLogExecution('ERROR', 'firstLevelEscalation', err);
		throw err;
	}
}

function getExpenseForFirstLevelEscalation() {
	try {
		var expenseRecords = [];

		// get all expense reports in Pending Supervisor Approval State
		searchRecord(
				'expensereport',
				null,
				[
						new nlobjSearchFilter('status', null, 'anyof',
								Expense_Report_Status.PendingSupervisorApproval),
						new nlobjSearchFilter('custentity_implementationteam',
								'employee', 'is', 'F'),
						new nlobjSearchFilter('field', 'systemnotes', 'anyof',
								'TRANDOC.KSTATUS'),
						new nlobjSearchFilter('date', 'systemnotes', 'on',
								'daysAgo6'),
						new nlobjSearchFilter('newvalue', 'systemnotes', 'is',
								'Pending Supervisor Approval') ],
				[
						new nlobjSearchColumn('internalid', null, 'group'),
						new nlobjSearchColumn('date', 'systemnotes', 'max')
								.setSort() ]).forEach(function(expense) {
			expenseRecords.push(expense.getValue('internalid', null, 'group'));
		});

		nlapiLogExecution('debug', 'expense report search',
				expenseRecords.length);

		return expenseRecords;
	} catch (err) {
		nlapiLogExecution('ERROR', 'getExpenseForFirstLevelEscalation', err);
		throw err;
	}
}

function firstLevelEscalationMailTemplate(expenseDetails) {

	var htmltext = '<p>Hi ' + expenseDetails.FirstLevelApprover.firstname
			+ ',</p>';
	htmltext += '<p>A gentle reminder !</p>'
			+ '<p>You have not approved the expense submitted by '
			+ expenseDetails.Employee.firstname
			+ ' '
			+ expenseDetails.Employee.lastname
			+ ' . Please view the record and kindly take necessary action. Please find the details below : </p>';
	htmltext += getExpenseDetailsTemplate(expenseDetails);
	htmltext += '<p>Regards,<br/>';
	htmltext += 'NetSuite Support Team</p>';

	return {
		Subject : 'Expense : ' + expenseDetails.Expense.Reference
				+ ' - Pending Supervisor Approval',
		Body : addMailTemplate(htmltext)
	};
}

function secondLevelFirstEscalation() {
	try {
		var expenseRecords = getExpenseForSecondLevelFirstEscalation();
		var expenseId = null;
		var failedMails = [];
		var context = nlapiGetContext();

		for (var index = 0; index < expenseRecords.length; index++) {
			expenseId = expenseRecords[index];
			yieldScript(context);
			try {
				var expenseDetails = getExpenseDetails(expenseId);
				var mailContent = secondLevelFirstEscalationMailTemplate(expenseDetails);

				var departmentHeadEmail = getDepartmentHeadEmail(expenseDetails.Employee.internalid);

				nlapiLogExecution('debug', 'expense details', JSON
						.stringify(expenseDetails));

				nlapiSendEmail(
						constant.Mail_Author.InformationSystem,
						expenseDetails.SecondLevelApprover.email,
						mailContent.Subject,
						mailContent.Body,
						[ expenseDetails.Employee.email, departmentHeadEmail ],
						null,
						{
							entity : expenseDetails.SecondLevelApprover.internalid,
							transaction : expenseId
						});
			} catch (er) {
				nlapiLogExecution('Error',
						'Second Level First Escalation Failed', 'expense id : '
								+ expenseId + " ; Error " + er);
				failedMails.push({
					State : 'Second Escalation First Level',
					Id : expenseId,
					ErrorMessage : er
				});
			}
		}

		return failedMails;
	} catch (err) {
		nlapiLogExecution('ERROR', 'secondLevelFirstEscalation', err);
		throw err;
	}
}
function getExpenseForSecondLevelFirstEscalation() {
	try {
		var expenseRecords = [];

		searchRecord(
				'expensereport',
				null,
				[
						new nlobjSearchFilter('status', null, 'anyof',
								Expense_Report_Status.PendingAccountingApproval),
						new nlobjSearchFilter('custentity_implementationteam',
								'employee', 'is', 'F'),
						new nlobjSearchFilter('field', 'systemnotes', 'anyof',
								'TRANDOC.KSTATUS'),
						new nlobjSearchFilter('date', 'systemnotes', 'on',
								'daysAgo5'),
						new nlobjSearchFilter('newvalue', 'systemnotes', 'is',
								'Pending Accounting Approval') ],
				[
						new nlobjSearchColumn('internalid', null, 'group'),
						new nlobjSearchColumn('date', 'systemnotes', 'max')
								.setSort() ]).forEach(function(expense) {
			expenseRecords.push(expense.getValue('internalid', null, 'group'));
		});

		nlapiLogExecution('debug', 'expense report search',
				expenseRecords.length);

		return expenseRecords;
	} catch (err) {
		nlapiLogExecution('ERROR', 'getExpenseForSecondLevelFirstEscalation',
				err);
		throw err;
	}
}
function secondLevelFirstEscalationMailTemplate(expenseDetails) {

	var htmltext = '<p>Hi ' + expenseDetails.SecondLevelApprover.firstname
			+ ',</p>';
	htmltext += '<p>A gentle reminder !</p>'
			+ '<p>You have not approved the expense submitted by '
			+ expenseDetails.Employee.firstname
			+ ' '
			+ expenseDetails.Employee.lastname
			+ ' . Please view the record and kindly take necessary action. Please find the details below : </p>';
	htmltext += getExpenseDetailsTemplate(expenseDetails);

	htmltext += '<p>Regards,<br/>';
	htmltext += 'NetSuite Support Team</p>';

	return {
		Subject : 'Expense : ' + expenseDetails.Expense.Reference
				+ ' - Pending Finance Approval',
		Body : addMailTemplate(htmltext)
	};
}

function secondLevelSecondEscalation() {
	try {
		var expenseRecords = getExpenseForSecondLevelSecondEscalation();
		var expenseId = null;
		var failedMails = [];
		var context = nlapiGetContext();

		// get finance Head Details details
		var financeHeadDetails_BTPL = nlapiLookupField('employee',
				1615, [ 'firstname', 'lastname',
						'email' ]);
						
		var financeHeadDetails_BLLC = nlapiLookupField('employee',
				5764, [ 'firstname', 'lastname',
						'email' ]);				

		// get other finance approvers
		var financeApprovers = getFinanceApprovers();
		var cc = [];

		for (var index = 0; index < expenseRecords.length; index++) {
			yieldScript(context);
			expenseId = expenseRecords[index];
			try {
				cc = null;

				var expenseDetails = getExpenseDetails(expenseId);
				var departmentHeadEmail = getDepartmentHeadEmail(expenseDetails.Employee.internalid);
				nlapiLogExecution('debug', 'expense details', JSON
						.stringify(expenseDetails));

				if (expenseDetails.Employee.subsidiary == constant.Subsidiary.US) {
					cc = [ financeApprovers.US[0], financeApprovers.US[1],
							expenseDetails.Employee.email, departmentHeadEmail, 'kaushal.thakar@brillio.com','bhavik.pandya@brillio.com'];
				} else if (expenseDetails.Employee.subsidiary == constant.Subsidiary.IN) {
					cc = [ financeApprovers.IN[0], financeApprovers.IN[1],
							expenseDetails.Employee.email, departmentHeadEmail,'amarana.r@BRILLIO.COM','srinivasa.pesappa@brillio.com'];
				}

				if (cc.length > 6) {
					nlapiSendEmail(constant.Mail_Author.InformationSystem,
							'nitish.mishra@brillio.com', 'Escalation Mails',
							'More than 6 cc');
					continue;
				}

				if (expenseDetails.Employee.subsidiary == constant.Subsidiary.US)
				{
					var mailContent = secondLevelSecondEscalationMailTemplate(
						expenseDetails, financeHeadDetails_BLLC);
						nlapiSendEmail(constant.Mail_Author.InformationSystem,
						financeHeadDetails_BLLC.email, mailContent.Subject,
						mailContent.Body, cc, null, {
							entity : constant.Employee.FinanceHead,
							transaction : expenseId
							});
					cc = null;
				}
				else if (expenseDetails.Employee.subsidiary == constant.Subsidiary.IN)
				{
					var mailContent = secondLevelSecondEscalationMailTemplate(
						expenseDetails, financeHeadDetails_BTPL);
						nlapiSendEmail(constant.Mail_Author.InformationSystem,
						financeHeadDetails_BTPL.email, mailContent.Subject,
						mailContent.Body, cc, null, {
							entity : constant.Employee.FinanceHead,
							transaction : expenseId
							});
					cc = null;
				}
			} catch (er) {
				nlapiLogExecution('Error',
						'Second Level Second Escalation Failed',
						'expense id : ' + expenseId + " ; Error " + er);
				failedMails.push({
					State : 'Second Escalation Second Level',
					Id : expenseId,
					ErrorMessage : er
				});
			}
		}

		return failedMails;
	} catch (err) {
		nlapiLogExecution('ERROR', 'secondLevelSecondEscalation', err);
		throw err;
	}
}

function getExpenseForSecondLevelSecondEscalation() {
	try {
		var expenseRecords = [];

		searchRecord(
				'expensereport',
				null,
				[
						new nlobjSearchFilter('status', null, 'anyof',
								Expense_Report_Status.PendingAccountingApproval),
						new nlobjSearchFilter('field', 'systemnotes', 'anyof',
								'TRANDOC.KSTATUS'),
						new nlobjSearchFilter('custentity_implementationteam',
								'employee', 'is', 'F'),
						new nlobjSearchFilter('date', 'systemnotes', 'on',
								'daysAgo8'),
						new nlobjSearchFilter('newvalue', 'systemnotes', 'is',
								'Pending Accounting Approval') ],
				[
						new nlobjSearchColumn('internalid', null, 'group'),
						new nlobjSearchColumn('date', 'systemnotes', 'max')
								.setSort() ]).forEach(function(expense) {
			expenseRecords.push(expense.getValue('internalid', null, 'group'));
		});

		nlapiLogExecution('debug', 'expense report search',
				expenseRecords.length);

		return expenseRecords;
	} catch (err) {
		nlapiLogExecution('ERROR', 'getExpenseForSecondLevelSecondEscalation',
				err);
		throw err;
	}
}

function secondLevelSecondEscalationMailTemplate(expenseDetails,
		financeHeadDetails) {

	// add the document received date
	expenseDetails.Expense.DocumentReceivedDate = getDocumentReceivedDate(expenseDetails.Expense.InternalId);

	var htmltext = '<p>Hi ' + financeHeadDetails.firstname + ',</p>';
	htmltext += '<p>The below expenses have not been approved by the Finance approver.'
			+' Via this message, this matter is being auto-escalated to Head of Finance.'
			+' No action is needed on behalf of the claimant or his supervisor.</p>';
	htmltext += '<p>Finance Approver and Head of Finance, please view the record and kindly take necessary actions.</p>';		
	htmltext += getExpenseDetailsTemplate(expenseDetails, null, null, true);
	htmltext += '<p>Regards,<br/>';
	htmltext += 'NetSuite Support Team</p>';

	return {
		Subject : 'Expense : ' + expenseDetails.Expense.Reference
				+ ' - Pending Finance Approval',
		Body : addMailTemplate(htmltext)
	};
}

function getDepartmentHeadEmail(employeeId) {
	try {
		var emailId = [];
		var departmentHead = nlapiLookupField('employee', employeeId,
				'department.custrecord_practicehead');
		var onsiteDepartmentHead = nlapiLookupField('employee', employeeId,
				'department.custrecord_onsite_practice_head');
		if (departmentHead) {
			var email = nlapiLookupField('employee', departmentHead, 'email');
			emailId.push(email);
		}
		if (_logValidation(onsiteDepartmentHead))
		{
			var emailonsite = nlapiLookupField('employee', onsiteDepartmentHead, 'email');
			emailId.push(emailonsite);
		}
		return emailId;
	} catch (err) {
		nlapiLogExecution('ERROR', 'getDepartmentHeadEmail', details)
	}
}

function getHumanResourceHead() {
	try {
		var emailId = nlapiLookupField('department',
				constant.Practice.Human_Resource,
				'custrecord_practicehead.email');
		return emailId;
	} catch (err) {
		nlapiLogExecution('ERROR', 'getHumanResourceHead', details)
	}
}

function getFinanceApprovers() {
	try {
		var usa = [], india = [];

		nlapiSearchRecord(
				'employee',
				null,
				[ new nlobjSearchFilter('internalid', null, 'anyof',
						constant.Finance_Approver.US) ],
				[ new nlobjSearchColumn('email') ]).forEach(function(emp) {
			usa.push(emp.getValue('email'));
		});

		nlapiSearchRecord(
				'employee',
				null,
				[ new nlobjSearchFilter('internalid', null, 'anyof',
						constant.Finance_Approver.IN) ],
				[ new nlobjSearchColumn('email') ]).forEach(function(emp) {
			india.push(emp.getValue('email'));
		});

		return {
			US : usa,
			IN : india
		};
	} catch (err) {
		nlapiLogExecution('ERROR', 'getFinanceApprovers', details)
	}
}

function yieldScript(currentContext) {

	if (currentContext.getRemainingUsage() <= 100) {
		nlapiLogExecution('AUDIT', 'API Limit Exceeded');
		var state = nlapiYieldScript();

		if (state.status == "FAILURE") {
			nlapiLogExecution('ERROR', 'Yield Script Failed', 'Reason : '
					+ state.reason + ' / Size : ' + state.size);
			return false;
		} else if (state.status == "RESUME") {
			nlapiLogExecution('AUDIT', 'Script Resumed');
		}
	}
}

function _logValidation(value) {
    if (value != null && value != '' && value != undefined && value.toString() != undefined && value.toString() != 'NaN' && value != NaN && value != 'undefined' && value != ' ') {
        return true;
    } else {
        return false;
    }
}