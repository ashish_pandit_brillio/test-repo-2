/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       06 Nov 2019     Jyoti Shinde
 *
 */
/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType
 * 
 * @param {String} type Operation types: create, edit, delete, xedit,
 *                      approve, cancel, reject (SO, ER, Time Bill, PO & RMA only)
 *                      pack, ship (IF only)
 *                      dropship, specialorder, orderitems (PO only) 
 *                      paybills (vendor payments)
 * @returns {Void}
 */

//SB: https://3883006-sb1.app.netsuite.com/app/common/scripting/script.nl?id=2019
function uesNotificationEmployee(type) {
    try {
        // create employee into mariaDb
        if (type == "create") {
            var method = "POST";
            var recObj = nlapiLoadRecord(nlapiGetRecordType(), nlapiGetRecordId());
            var obj = {};

            var emp_inactive = recObj.getFieldValue("custentity_employee_inactive");
            var implem_team = recObj.getFieldValue("custentity_implementationteam");

            if (emp_inactive == 'F' && implem_team == 'F') {

                obj.internalId = Number(nlapiGetRecordId());
                obj.employeeId = Number(recObj.getFieldValue("custentity_fusion_empid")) || '';

                var emp_frst_name = recObj.getFieldValue('firstname') || '';
                var emp_middl_name = recObj.getFieldValue('middlename') || '';
                var emp_lst_name = recObj.getFieldValue('lastname') || '';
                var emp_full_name = '';
                if (emp_frst_name)
                    emp_full_name = emp_frst_name;

                if (emp_middl_name)
                    emp_full_name = emp_full_name + ' ' + emp_middl_name;

                if (emp_lst_name)
                    emp_full_name = emp_full_name + ' ' + emp_lst_name;
//-------------------------------------------------------------------------------------------------------------------------------------------------------				
				var recId = nlapiGetRecordId();                         //NIS-1231  prabhat Gupta 27/02/2020
				
				var emp_visaRecord = visaDetail(recId);
				
				obj.visaDetail = emp_visaRecord;
                obj.actualHireDate = recObj.getFieldValue('custentity_actual_hire_date') || '';
				obj.reportingManager = recObj.getFieldValue('custentity_reportingmanager') || '';
				obj.level = recObj.getFieldValue('employeestatus') || '';
				
				
//-----------------------------------------------------------------------------------------------------------------------------------------------------
                obj.firstName = emp_frst_name || '';
                obj.middleName = emp_middl_name || '';
                obj.lastName = emp_lst_name || '';
                obj.fullName = emp_full_name || '';
                obj.designation = recObj.getFieldValue("title") || '';
                obj.emailId = recObj.getFieldValue("email") || '';
                var department = recObj.getFieldValue("department");
                if (department) 
				{
                    var practice = nlapiLookupField("department", recObj.getFieldValue("department"), "custrecord_parent_practice"); //Parent Practice internal Id
                    if (practice) {
                        obj.practiceInternalId = Number(practice);
                    } else {
                        obj.practiceInternalId = Number(recObj.getFieldValue("department"));
                    }
//-------------------------------------------------------------------------------------------------------------------------------------------------------------
//NIS-1231  prabhat Gupta 10/03/2020					
				/*	if (practice) {
						var practiceHrbp = nlapiLookupField("department", practice, "custrecord_hrbusinesspartner");
						if (practiceHrbp) {
							obj.hrbpInternalId = Number(practiceHrbp);
							obj.hrbpName =  nlapiLookupField("employee", practiceHrbp, "entityid");
						
						}
					}else{
							var practiceHrbp = nlapiLookupField("department", recObj.getFieldValue("department"), "custrecord_hrbusinesspartner");
							if (practiceHrbp) {
								obj.hrbpInternalId = Number(practiceHrbp);
								obj.hrbpName = nlapiLookupField("employee", practiceHrbp, "entityid");
							}
					}
					
                      Commented by Shravan on HRBP integration part*/
//------------------------------------------------------------------------------------------------------------------------------------------------------------				
				}
				var practiceHrbp = Number(recObj.getFieldValue("custentity_emp_hrbp"));
				obj.hrbpInternalId = practiceHrbp;
				if(practiceHrbp)
				obj.hrbpName = nlapiLookupField("employee", practiceHrbp, "entityid"); // 96,97,98 added by Shravan
                obj.isEmployeeInactive = recObj.getFieldValue("custentity_employee_inactive") || '';
                obj = JSON.stringify(obj);
                nlapiLogExecution("DEBUG", "body create Mode: ", obj);
              //  var url = "https://fuelnode1.azurewebsites.net/employee";
			  var url = "https://fuelnodesandbox.azurewebsites.net/employee";
                var response = call_node(url, obj, method);
                nlapiLogExecution("DEBUG", "response : create :  ", JSON.stringify(response));
            }
        }
        // update employee into mariaDb
        if (type == "edit" || type == 'xedit') {
            var method = "POST";
            var recObj = nlapiLoadRecord(nlapiGetRecordType(), nlapiGetRecordId());
            var oldRec = nlapiGetOldRecord();

            // var emp_inactive = recObj.getFieldValue("custentity_employee_inactive");
            var implem_team = recObj.getFieldValue("custentity_implementationteam");
            //emp_inactive == 'F' && 
            if (implem_team == 'F') {

                var employeeId = Number(recObj.getFieldValue("custentity_fusion_empid")) || '';
                var oldemployeeId = Number(oldRec.getFieldValue("custentity_fusion_empid")) || '';
				
				
				

                var frstname = recObj.getFieldValue("firstname") || '';
                var oldfrstname = oldRec.getFieldValue("firstname") || '';
                var middlname = recObj.getFieldValue("middlename") || '';
                var oldmiddlname = oldRec.getFieldValue("middlename") || '';
                var lastname = recObj.getFieldValue("lastname") || '';
                var oldlastname = oldRec.getFieldValue("lastname") || '';

                var designation = recObj.getFieldValue("title") || '';
                var olddesignation = oldRec.getFieldValue("title") || '';
                var emailId = recObj.getFieldValue("email") || '';
                var oldemailId = oldRec.getFieldValue("email") || '';
                var department = recObj.getFieldValue("department");
                if (department) {
                    var practice = nlapiLookupField("department", recObj.getFieldValue("department"), "custrecord_parent_practice"); //Parent Practice internal Id
                    if (practice) {
                        var practiceInternalId = Number(practice);
                    } else {
                        var practiceInternalId = Number(recObj.getFieldValue("department"));
                    }
                }
                var olddepartment = oldRec.getFieldValue("department");
                if (olddepartment) {
                    var oldpractice = nlapiLookupField("department", oldRec.getFieldValue("department"), "custrecord_parent_practice"); //Parent Practice internal Id
                    if (oldpractice) {
                        var oldpracticeInternalId = Number(oldpractice);
                    } else {
                        var oldpracticeInternalId = Number(recObj.getFieldValue("department"));
                    }
                }
                var Inactive = recObj.getFieldValue("custentity_employee_inactive") || '';
                var oldInactive = oldRec.getFieldValue("custentity_employee_inactive") || '';
                if (employeeId != oldemployeeId || frstname != oldfrstname || middlname != oldmiddlname || lastname != oldlastname || designation != olddesignation || emailId != oldemailId || practiceInternalId != oldpracticeInternalId || Inactive != oldInactive) {
                    var obj = {};
                    obj.internalId = Number(nlapiGetRecordId());
                    obj.employeeId = Number(recObj.getFieldValue("custentity_fusion_empid")) || '';

                    var emp_frst_name = recObj.getFieldValue('firstname') || '';
                    var emp_middl_name = recObj.getFieldValue('middlename') || '';
                    var emp_lst_name = recObj.getFieldValue('lastname') || '';
                    var emp_full_name = '';
                    if (emp_frst_name)
                        emp_full_name = emp_frst_name;

                    if (emp_middl_name)
                        emp_full_name = emp_full_name + ' ' + emp_middl_name;

                    if (emp_lst_name)
                        emp_full_name = emp_full_name + ' ' + emp_lst_name;
//---------------------------------------------------------------------------------------------------------------------------------------------					
					var recId = nlapiGetRecordId();  ////NIS-1231  prabhat Gupta 27/02/2020
					
					var emp_visaRecord = visaDetail(recId);
				
					obj.visaDetail = emp_visaRecord;
					obj.actualHireDate = recObj.getFieldValue('custentity_actual_hire_date') || '';
					obj.reportingManager = recObj.getFieldValue('custentity_reportingmanager') || '';
					obj.level = recObj.getFieldValue('employeestatus') || '';
//---------------------------------------------------------------------------------------------------------------------------------------------
                    obj.firstName = emp_frst_name || '';
                    obj.middleName = emp_middl_name || '';
                    obj.lastName = emp_lst_name || '';
                    obj.fullName = emp_full_name || '';
                    obj.designation = recObj.getFieldValue("title") || '';
                    obj.emailId = recObj.getFieldValue("email") || '';
                    var department = recObj.getFieldValue("department");
                    if (department) {
                        var practice = nlapiLookupField("department", recObj.getFieldValue("department"), "custrecord_parent_practice"); //Parent Practice internal Id
                        if (practice) {
                            obj.practiceInternalId = Number(practice);
                        } else {
                            obj.practiceInternalId = Number(recObj.getFieldValue("department"));
                        }
						
//-------------------------------------------------------------------------------------------------------------------------------------------------------------
//NIS-1231  prabhat Gupta 10/03/2020					
						/*if (practice) {
							var practiceHrbp = nlapiLookupField("department", practice, "custrecord_hrbusinesspartner");
							if (practiceHrbp) {
								obj.hrbpInternalId = Number(practiceHrbp);
								obj.hrbpName =  nlapiLookupField("employee", practiceHrbp, "entityid");
							
							}
						}else{
								var practiceHrbp = nlapiLookupField("department", recObj.getFieldValue("department"), "custrecord_hrbusinesspartner");
								if (practiceHrbp) {
									obj.hrbpInternalId = Number(practiceHrbp);
									obj.hrbpName = nlapiLookupField("employee", practiceHrbp, "entityid");
								}
							}
						
						*/
						
//------------------------------------------------------------------------------------------------------------------------------------------------------------				
						var practiceHrbp = Number(recObj.getFieldValue("custentity_emp_hrbp"));
						obj.hrbpInternalId = practiceHrbp;
						if(practiceHrbp)
						obj.hrbpName = nlapiLookupField("employee", practiceHrbp, "entityid"); // 96,97,98 added by Shravan
                    }
                    obj.isEmployeeInactive = recObj.getFieldValue("custentity_employee_inactive") || '';
                    obj = JSON.stringify(obj);
                    nlapiLogExecution("DEBUG", "body edit Mode: ", obj);
                   // var url = "https://fuelnode1.azurewebsites.net/employee";
				   var url = "https://fuelnodesandbox.azurewebsites.net/employee";
                    var response = call_node(url, obj, method);
                    nlapiLogExecution("DEBUG", "response : Edit :  ", JSON.stringify(response));
                }
            }
        }
    } catch (e) {
        // TODO: handle exception
        nlapiLogExecution("DEBUG", "Error in script : ", e);
    }
}

function beforeSubmitDelete(type) {
    // delete employee into mariaDb
    if (type == 'delete') {
        var body = {};
        var method = "DELETE";
       // var url = "https://fuelnode1.azurewebsites.net/employee/" + nlapiGetRecordId();
	   var url = "https://fuelnodesandbox.azurewebsites.net/employee" + nlapiGetRecordId();
        var response = call_node(url, body, method);
        nlapiLogExecution("DEBUG", "response Delete Mode: ", JSON.stringify(response));
    }
}

//--------------------------------------------------------------------------------------------------------------------------------------------------------------------
////NIS-1231  prabhat Gupta 27/02/2020
function visaDetail(empId){
	
	var customrecord_visa_detail = nlapiSearchRecord("customrecord_empvisadetails",null,
	[
	   ["isinactive","is","F"],"AND",["custrecord_visaid","is",empId]
	], 
	[
	   new nlobjSearchColumn("internalid"), 
	   new nlobjSearchColumn("custrecord_visa"), 
	   new nlobjSearchColumn("custrecord_visaid")
	]
	);
	if(customrecord_visa_detail)
	{
	var data = [];
		var dataArray = new Array();
		for(var i=0; i<customrecord_visa_detail.length;i++)
		{
			data.push({
			"id":customrecord_visa_detail[i].getId(),
			"visaType":customrecord_visa_detail[i].getText("custrecord_visa"),
			"employee":customrecord_visa_detail[i].getText("custrecord_visaid"),			
			});
		}
		return data;
	}
}
//---------------------------------------------------------------------------------------------------------------------------------------------------