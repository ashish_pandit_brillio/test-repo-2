/**
 * Module Description
 * 
 * Version Date Author Remarks 1.00 30 Jun 2015 nitish.mishra
 * 
 */

function main() {
	var rec = nlapiSearchRecord(null, 975);

	for (var i = 0; i < rec.length; i++) {
		try {
			moveTimesheet(rec[i].getId());
		} catch (err) {
			nlapiLogExecution('error', rec[i].getId(), err);
		}

	}
}

/**
 * @param {String}
 *            type Context Types: scheduled, ondemand, userinterface, aborted,
 *            skipped
 * @returns {Void}
 */
function moveTimesheet(recId) {

	var ab = [ recId ];

	for (var j = 0; j < ab.length; j++) {
		nlapiLogExecution('debug', 'ID: ', ab[j]);
		var a_days = [ 'sunday', 'monday', 'tuesday', 'wednesday', 'thursday',
				'friday', 'saturday' ];

		var a;
		try {
			a = nlapiLoadRecord('timesheet', ab[j]);
		} catch (e) {
			nlapiLogExecution('ERROR', 'e', e.message);
			if (e.message == 'That record does not exist.') {
				continue;
			}
			break;
		}

		doSubmit = false;
		for (var i = 1; i <= a.getLineItemCount('timegrid'); i++) {

			a.selectLineItem('timegrid', i);
			for (var i_day_indx = 0; i_day_indx < 7; i_day_indx++) {
				var o_sub_record_view = a.viewCurrentLineItemSubrecord(
						'timegrid', a_days[i_day_indx]);

				if (o_sub_record_view) {
					var customer = o_sub_record_view.getFieldValue('customer');

					if (customer == 10929 || customer == 11008
							|| customer == 11014 || customer == 11021
							|| customer == 11132 || customer == 11188
							|| customer == 11135 || customer == 11148
							|| customer == 11168 || customer == 11176
							|| customer == 11187 || customer == 11133) {

						if (o_sub_record_view.getFieldValue('item') == 2222) {
							var o_sub_record = a.editCurrentLineItemSubrecord(
									'timegrid', a_days[i_day_indx]);

							doSubmit = true;
							o_sub_record.setFieldValue('item', 2221);
							o_sub_record.commit();
						}
					}
				}
			}

			a.commitLineItem('timegrid');
		}

		if (doSubmit)
			nlapiSubmitRecord(a);
	}
}
