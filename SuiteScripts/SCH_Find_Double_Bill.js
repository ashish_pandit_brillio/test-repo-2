function find_vednor_bill()
{
	try
	{
		
		var srch_inv = nlapiSearchRecord('transaction',2161);
		if(srch_inv)
		{
			nlapiLogExecution('debug','inc srch len:- '+srch_inv.length);
			for(var i=0; i<srch_inv.length; i++)
			{
				nlapiLogExecution('audit','inc id:- '+srch_inv[i].getId());
				var tranid = nlapiLookupField('invoice',srch_inv[i].getId(),'tranid');
				var inv_rcrd = nlapiLoadRecord('invoice',srch_inv[i].getId());
				var a_bill = inv_rcrd.getFieldValues('custbody_auto_invoice_bllc');
				
				var a_filter_get_approved_invoices = [['custbody_oldinvoicenumber', 'is', tranid], 'and', ['mainline', 'is', 'T']];
		
				var a_get_bills = nlapiSearchRecord('vendorbill', null, a_filter_get_approved_invoices);
				if (a_get_bills) {
					
					for(var j=0; j<a_get_bills.length; j++)
					{
						if(a_bill.length > 0)
						{
							if(a_bill[j] == a_get_bills[j].getId())
							{
								nlapiLogExecution('debug','bill id:- '+a_get_bills[j].getId());
							}
							else
							{
								nlapiLogExecution('audit','bill not match:- '+a_get_bills[j].getId());
								nlapiDeleteRecord('vendorbill',a_get_bills[j].getId());
							}
						}
					}
				}
			}
		}
	}
	catch(err)
	{
		nlapiLogExecution('ERROR','SCHLD:- ',err);
	}
}