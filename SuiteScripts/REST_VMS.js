/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       8 April 2019     shamanth.k
 *
 */

/**
 * @param {Object} dataIn Parameter object
 * @returns {Object} Output object
 */
function postRESTlet(dataIn) {
	var response = new Response();
	try
	{
		nlapiLogExecution('debug', 'dataIn', JSON.stringify(dataIn));
		var current_date = nlapiDateToString(new Date());
		nlapiLogExecution('debug', 'current_date', current_date);
		
		if(_logValidation(dataIn))
		{
			var requestType = dataIn.RequestType;
			var temp = dataIn.name;
		
			switch (requestType) {

			case M_Constants.Request.Get:
				response.Data = getAllActiveEmployeeName(temp);
				response.Status = true;
			}
		}
	}
	catch (err) {
		nlapiLogExecution('ERROR', 'postRESTlet', err);
		response.Data = err;
	}
	return response;
}
	
function getAllActiveEmployeeName(temp) {
		try {

			var emp_fusion_id = '';
			var emp_first_name = '';
			var emp_middle_name = '';
			var emp_last_name = '';
			var emp_full_name = '';
			
			var employeeSearch = nlapiSearchRecord("employee",null,
				[
					[[[["firstname","contains",temp]],"OR",
					[["middlename","contains",temp]],"OR",
					[["lastname","contains",temp]]],"AND",
					[["custentity_implementationteam","is","F"]],"AND",
					[["custentity_employee_inactive","is","F"]],"AND",
					[["isinactive","is","F"]],"AND",
					[["subsidiary","anyof","3","9"]]]
				], 
				[
					new nlobjSearchColumn("firstname"), 
					new nlobjSearchColumn("middlename"), 
					new nlobjSearchColumn("lastname"), 
					new nlobjSearchColumn("custentity_fusion_empid"), 
					new nlobjSearchColumn("department"), 
					new nlobjSearchColumn("phone"), 
					new nlobjSearchColumn("custentity_list_brillio_location_e"), 
					new nlobjSearchColumn("email")
				]
			);
			var dataRow = [];
			var JSON = {};
			if (employeeSearch) {
					for(var i=0;i<employeeSearch.length;i++){
					    
						emp_fusion_id = employeeSearch[i].getValue('custentity_fusion_empid');
						emp_first_name = employeeSearch[i].getValue('firstname');
						emp_middle_name = employeeSearch[i].getValue('middlename');
						emp_last_name = employeeSearch[i].getValue('lastname');
						if(emp_first_name)
							emp_full_name = emp_first_name;
							
						if(emp_middle_name)
							emp_full_name = emp_full_name + ' ' + emp_middle_name;
							
						if(emp_last_name)
							emp_full_name = emp_full_name + ' ' + emp_last_name;
						
						JSON = {
								EmployeeID:emp_fusion_id,
								Name: emp_full_name,
								Phone: employeeSearch[i].getValue('phone'),
								Department: employeeSearch[i].getText('department'),
								Brilliolocation: employeeSearch[i].getValue('custentity_list_brillio_location_e'),
								Email: employeeSearch[i].getValue('email')
						
								
						};
						dataRow.push(JSON);
						
					}
					
				
			}

			return dataRow;
		} 
		catch (err) {
			nlapiLogExecution('error', 'getAllActiveEmployeeName', err);
			throw err;
		}
	}


function _logValidation(value) 
{
 if(value != null && value.toString() != null && value != '' && value != undefined && value.toString() != undefined && value != 'undefined' && value.toString() != 'undefined'&& value.toString() != 'NaN' && value != NaN) 
 {
  return true;
 }
 else 
 { 
  return false;
 }
}