// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
/*
   	Script Name : REST_FRF_Flow.js
	Author      : ASHISH PANDIT
	Date        : 06 MARCH 2019
    Description : RESTlet for FRF Flow 


	Script Modification Log:

	-- Date --			-- Modified By --				--Requested By--				-- Description --
 


Below is a summary of the process controls enforced by this script file.  The control logic is described
more fully, below, in the appropriate function headers and code blocks.


     SCHEDULED FUNCTION
		- scheduledFunction(type)


     SUB-FUNCTIONS
		- The following sub-functions are called by the above core functions in order to maintain code
            modularization:

               - NOT USED

*/
}
// END SCRIPT DESCRIPTION BLOCK  ====================================
var values = {};
/*Suitlet Main function*/
function RESTlet_FRF_Flow(dataIn)
{
	try
	{
		var user = dataIn.user;
		var i_frf_id = dataIn.frfid; 
		nlapiLogExecution('ERROR','Script Parameter i_frf_id ',i_frf_id);
		if(i_frf_id)
		{
			var frfSearch = getFrfDetails(i_frf_id);
			values['{data}'] = frfSearch;
		}
		values.designation = nlapiLookupField('employee',user,'title');
		values.homeicon = nlapiResolveURL('SUITELET','customscript_fuel_sut_fulfillment_dash','customdeploy_fuel_sut_fulfillment_dash');
		var select = "";
		values.practice_options = getListValues('department', select, 'Select Practice'); 
		values.employee_level_options = getEmpLevelValues('customsearch_employee_level_search',select,'Select Employee Level');
		values.location_options = getListValues('location', select, 'Select Location');
		
		values.employee_search = getSearchValues('customsearch_employee_search',select,'Select Employee');
		values.employee_status = getListValues('customlist_fuel_employee_status', select, 'Select Employee Status');
		values.education_level = getListValues('customlist_fuel_req_edu_level', select, 'Select Req. Education Level');
		values.education_program = getListValues('customlist_fuel_education_program', select, 'Select Education Program');
		return values;
	}
	catch(error)
	{
		nlapiLogExecution('Debug','Error ',error);
	}
}
/****Function to get FRF details ****/
function getFrfDetails(i_frf_id)
{
	try
	{
		nlapiLogExecution('Debug','In Function getFrfDetails');
		if(i_frf_id)
		{
			nlapiLogExecution('Debug','i_frf_id type',typeof(i_frf_id));
			var customrecord_frf_detailsSearch = nlapiSearchRecord("customrecord_frf_details",null,
			[
			   ["internalid","anyof",i_frf_id]
			], 
			[
			   new nlobjSearchColumn("scriptid").setSort(false), 
			   new nlobjSearchColumn("custrecord_frf_details_opp_id"), 
			   new nlobjSearchColumn("custrecord_frf_details_res_location"), 
			   new nlobjSearchColumn("custrecord_frf_details_res_practice"), 
			   new nlobjSearchColumn("custrecord_frf_details_emp_level"), 
			   new nlobjSearchColumn("custrecord_frf_details_external_hire"), 
			   new nlobjSearchColumn("custrecord_frf_details_role"), 
			   new nlobjSearchColumn("custrecord_frf_details_bill_rate"), 
			   new nlobjSearchColumn("custrecord_frf_details_critical_role"), 
			   new nlobjSearchColumn("custrecord_frf_details_skill_family"), 
			   new nlobjSearchColumn("custrecord_frf_details_start_date"), 
			   new nlobjSearchColumn("custrecord_frf_details_end_date"), 
			   new nlobjSearchColumn("custrecord_frf_details_account"), 
			  // new nlobjSearchColumn("custrecord_frf_details_selected_emp"), 
			  // new nlobjSearchColumn("custrecord_frf_details_internal_fulfill"), 
			   new nlobjSearchColumn("custrecord_frf_details_primary_skills"), 
			   new nlobjSearchColumn("custrecord_frf_details_frf_number"), 
			   new nlobjSearchColumn("custrecord_frf_details_project"), 
			  // new nlobjSearchColumn("custrecord_frf_details_suggestion"), 
			  // new nlobjSearchColumn("custrecord_frf_type"), 
			  // new nlobjSearchColumn("custrecord_frf_emp_notice_rotation"), 
			   new nlobjSearchColumn("custrecord_frf_details_billiable"), 
			   new nlobjSearchColumn("custrecord_frf_details_source"), 
			   new nlobjSearchColumn("custrecord_frf_details_secondary_skills"), 
			   new nlobjSearchColumn("custrecord_frf_details_allocation"), 
			   new nlobjSearchColumn("custrecord_frf_details_dollar_loss"), 
			   new nlobjSearchColumn("custrecord_frf_details_suggest_pref_mat"), 
			   new nlobjSearchColumn("custrecord_frf_details_special_req"), 
			  // new nlobjSearchColumn("custrecord_frf_details_personal_email"), 
			   new nlobjSearchColumn("custrecord_frf_details_backup_require"), 
			   new nlobjSearchColumn("custrecord_frf_details_job_title"), 
			   new nlobjSearchColumn("custrecord_frf_details_edu_lvl"), 
			   new nlobjSearchColumn("custrecord_frf_details_edu_program"), 
			   new nlobjSearchColumn("custrecord_frf_details_emp_status"), 
			   new nlobjSearchColumn("custrecord_frf_details_first_interviewer"), 
			   new nlobjSearchColumn("custrecord_frf_details_sec_interviewer"), 
			   new nlobjSearchColumn("custrecord_frf_details_cust_interview"), 
			   new nlobjSearchColumn("custrecord_frf_details_cust_inter_email"), 
			   new nlobjSearchColumn("custrecord_frf_details_job_descriptions"), 
			   new nlobjSearchColumn("custrecord_frf_details_instruction_team"), 
			   new nlobjSearchColumn("custrecord_frf_details_reason_external"),
			  // new nlobjSearchColumn("custrecord_frf_details_ta_manager"), 
			  // new nlobjSearchColumn("custrecord_frf_details_req_type"), 
			   new nlobjSearchColumn("custrecord_frf_details_created_by"),
			   new nlobjSearchColumn("custrecord_frf_details_created_date"),
			   new nlobjSearchColumn("custrecord_project_internal_id_sfdc","CUSTRECORD_FRF_DETAILS_OPP_ID",null), 
			   new nlobjSearchColumn("custrecord_opportunity_name_sfdc","CUSTRECORD_FRF_DETAILS_OPP_ID",null), 
			   new nlobjSearchColumn("custrecord_customer_internal_id_sfdc","CUSTRECORD_FRF_DETAILS_OPP_ID",null), 
			   new nlobjSearchColumn("custrecord_customer_sfdc","CUSTRECORD_FRF_DETAILS_OPP_ID",null), 
			   new nlobjSearchColumn("custrecord_project_sfdc","CUSTRECORD_FRF_DETAILS_OPP_ID",null)
			]
			);
			if(customrecord_frf_detailsSearch)
			{
				var dataArray = new Array();
				nlapiLogExecution('Debug','customrecord_frf_detailsSearch ',customrecord_frf_detailsSearch.length);
				
				var b_billRoll = customrecord_frf_detailsSearch[0].getValue("custrecord_frf_details_billiable");
				if(b_billRoll=='T')
				{
					b_billRoll=true;
				}
				else
				{
					b_billRoll = false;
				}
				
				var b_criticalRoll=customrecord_frf_detailsSearch[0].getValue("custrecord_frf_details_critical_role");
				if(b_criticalRoll=='T')
				{
					b_criticalRoll=true;
				}
				else
				{
					b_criticalRoll = false;
				}
				
				var b_externalHire =customrecord_frf_detailsSearch[0].getValue("custrecord_frf_details_external_hire");
				if(b_externalHire=='T')
				{
					b_externalHire=true;
				}
				else
				{
					b_externalHire = false;
				}
				var b_backupRequired = customrecord_frf_detailsSearch[0].getValue("custrecord_frf_details_backup_require");
				if(b_backupRequired=='T')
				{
					b_backupRequired=true;
				}
				else
				{
					b_backupRequired=false;
				}
				var b_cust_interview = customrecord_frf_detailsSearch[0].getValue("custrecord_frf_details_cust_interview");
				if(b_cust_interview=='T')
				{
					b_cust_interview=true;
				}
				else
				{
					b_cust_interview=false;
				}
				
				var s_account = customrecord_frf_detailsSearch[0].getText("custrecord_frf_details_account");
				if(!s_account)
				{
					s_account =  customrecord_frf_detailsSearch[0].getValue("custrecord_customer_sfdc","CUSTRECORD_FRF_DETAILS_OPP_ID",null);
					nlapiLogExecution('Debug','s_account ',s_account);
				}
				var s_project = customrecord_frf_detailsSearch[0].getText("custrecord_frf_details_project");
				if(!s_project)
				{
					s_project = customrecord_frf_detailsSearch[0].getValue("custrecord_opportunity_name_sfdc","CUSTRECORD_FRF_DETAILS_OPP_ID",null);
					nlapiLogExecution('Debug','s_project ',s_project);
				}
				var i_account = customrecord_frf_detailsSearch[0].getValue("custrecord_frf_details_account");
				
				var i_project = customrecord_frf_detailsSearch[0].getValue("custrecord_frf_details_project");
				values.frf_number =customrecord_frf_detailsSearch[0].getValue("custrecord_frf_details_frf_number");
				values.account = s_account;//customrecord_frf_detailsSearch[0].getText("custrecord_frf_details_account");
				values.project = s_project;//customrecord_frf_detailsSearch[0].getText("custrecord_frf_details_project");
				values.account_Id = i_account;
                var homepage_url = nlapiResolveURL('SUITELET','customscript_fuel_sut_fulfillment_dash','customdeploy_fuel_sut_fulfillment_dash');
				nlapiLogExecution('debug','homepage_url ',homepage_url);
				values.backButton =homepage_url;// "/app/site/hosting/scriptlet.nl?script=1745&deploy=1";
				var data = {};
				data.recordid = {"name":customrecord_frf_detailsSearch[0].getId()};
				data.frfnumber = {"name":customrecord_frf_detailsSearch[0].getValue("custrecord_frf_details_frf_number")};
				data.role = {"name":customrecord_frf_detailsSearch[0].getValue("custrecord_frf_details_role")};
				data.startDate = {"name":customrecord_frf_detailsSearch[0].getValue("custrecord_frf_details_start_date")};
				data.endDate = {"name":customrecord_frf_detailsSearch[0].getValue("custrecord_frf_details_end_date")};
				data.Allocation = {"name":customrecord_frf_detailsSearch[0].getValue("custrecord_frf_details_allocation")};
				data.billrate = {"name":customrecord_frf_detailsSearch[0].getValue("custrecord_frf_details_bill_rate")};
				data.billrole = {"name":b_billRoll};
				data.splrequirment = {"name":customrecord_frf_detailsSearch[0].getValue("custrecord_frf_details_special_req")};
				data.dollarloss = {"name":customrecord_frf_detailsSearch[0].getValue("custrecord_frf_details_dollar_loss")};
				data.externalhire = {"name":b_externalHire};
				data.criticalrole = {"name":b_criticalRoll};
				data.account = {"name":customrecord_frf_detailsSearch[0].getText("custrecord_frf_details_account"),"id":customrecord_frf_detailsSearch[0].getValue("custrecord_frf_details_account")};
				data.emplevel = {"name":customrecord_frf_detailsSearch[0].getText("custrecord_frf_details_emp_level"),"id":customrecord_frf_detailsSearch[0].getValue("custrecord_frf_details_emp_level")};
				data.location = {"name":customrecord_frf_detailsSearch[0].getText("custrecord_frf_details_res_location"),"id":customrecord_frf_detailsSearch[0].getValue("custrecord_frf_details_res_location")};
				data.primaryskills = {"name":customrecord_frf_detailsSearch[0].getText("custrecord_frf_details_primary_skills"),"id":customrecord_frf_detailsSearch[0].getValue("custrecord_frf_details_primary_skills")};
				data.practice = {"name":customrecord_frf_detailsSearch[0].getText("custrecord_frf_details_res_practice"),"id":customrecord_frf_detailsSearch[0].getValue("custrecord_frf_details_res_practice")};
				data.project = {"name":customrecord_frf_detailsSearch[0].getText("custrecord_frf_details_project"),"id":customrecord_frf_detailsSearch[0].getValue("custrecord_frf_details_project")};
				data.family = {"name":customrecord_frf_detailsSearch[0].getText("custrecord_frf_details_skill_family"),"id":customrecord_frf_detailsSearch[0].getValue("custrecord_frf_details_skill_family")};
				
				
				var suggestedPeopleData= {"name":customrecord_frf_detailsSearch[0].getText("custrecord_frf_details_suggest_pref_mat"),"id":customrecord_frf_detailsSearch[0].getValue("custrecord_frf_details_suggest_pref_mat")};
				var suggestedPeopleArray=[];
				for(var j=0;j<suggestedPeopleData.name.split(",").length;j++){

				suggestedPeopleArray.push({
				name:suggestedPeopleData.name.split(",")[j],
				id:suggestedPeopleData.id.split(",")[j]
				})
				}
				data.suggestedpeople = suggestedPeopleArray;
				nlapiLogExecution('Debug','data.suggestedpeople  ',JSON.stringify(data.suggestedpeople));
				
				data.otherskills = {"name":customrecord_frf_detailsSearch[0].getText("custrecord_frf_details_secondary_skills"),"id":customrecord_frf_detailsSearch[0].getValue("custrecord_frf_details_secondary_skills")};
				data.frfsource = {"name":customrecord_frf_detailsSearch[0].getText("custrecord_frf_details_source"),"id":customrecord_frf_detailsSearch[0].getValue("custrecord_frf_details_source")};
				data.loggedby = {"name":customrecord_frf_detailsSearch[0].getText("custrecord_frf_details_created_by"),"id":customrecord_frf_detailsSearch[0].getValue("custrecord_frf_details_created_by")};
				data.loggedon = {"name":customrecord_frf_detailsSearch[0].getValue("custrecord_frf_details_created_date")};
				//======================================================================================================================
				data.backuprequired = {"name":b_backupRequired};
				data.jobtitle = {"name":customrecord_frf_detailsSearch[0].getValue("custrecord_frf_details_job_title")};
				data.jobdescription = {"name":customrecord_frf_detailsSearch[0].getValue("custrecord_frf_details_job_descriptions")};
				data.instruction = {"name":customrecord_frf_detailsSearch[0].getValue("custrecord_frf_details_instruction_team")};
				data.educationlevel = {"name":customrecord_frf_detailsSearch[0].getText("custrecord_frf_details_edu_lvl"),"id":customrecord_frf_detailsSearch[0].getValue("custrecord_frf_details_edu_lvl")};
				data.educationprogram = {"name":customrecord_frf_detailsSearch[0].getText("custrecord_frf_details_edu_program"),"id":customrecord_frf_detailsSearch[0].getValue("custrecord_frf_details_edu_program")};
				data.employeestatus = {"name":customrecord_frf_detailsSearch[0].getText("custrecord_frf_details_emp_status"),"id":customrecord_frf_detailsSearch[0].getValue("custrecord_frf_details_emp_status")};
				data.firstinterviewer = {"name":customrecord_frf_detailsSearch[0].getValue("custrecord_frf_details_first_interviewer")};
				data.secondinterviewer = {"name":customrecord_frf_detailsSearch[0].getValue("custrecord_frf_details_sec_interviewer")};
				data.custinterview = {"name":b_cust_interview};
				data.custintervieweremail = {"name":customrecord_frf_detailsSearch[0].getValue('custrecord_frf_details_cust_inter_email')};
				data.externalhirereason = {"name":customrecord_frf_detailsSearch[0].getValue('custrecord_frf_details_reason_external')};
				
				//======================================================================================================================
				//tileDataArray.push(data);
				dataArray.push(data);
				
				nlapiLogExecution('Debug','dataArray$$$$$ ',JSON.stringify(dataArray));
				return dataArray;
			}
		}
	}
	catch(error)
	{
		nlapiLogExecution('Error','Exception ',error);
	}
}


/*********************************************Function to get Employee Level Values**********************************************/
function getEmpLevelValues(s_list_name, strSelectedValue, s_placeholder, removeBlankOption)
{
	var columns = new Array();
	columns[0]	=	new nlobjSearchColumn("employeestatus",null,"GROUP");
	
	var a_search_results = searchRecord(null, s_list_name, null, columns);
	
	var list_values = new Array();
	
	for(var i = 0; a_search_results != null && i < a_search_results.length; i++)
		{
			list_values.push({'display':a_search_results[i].getText("employeestatus",null,"GROUP"), 'value':a_search_results[i].getText("employeestatus",null,"GROUP")});
		}
	return list_values;
	//return getOptions(list_values, strSelectedValue, s_placeholder, removeBlankOption);
}

/*********************************************Function to get List Values**********************************************/
function getListValues(s_list_name, strSelectedValue, s_placeholder, removeBlankOption)
{
	var columns = new Array();
	columns[0]	=	new nlobjSearchColumn('name');
	
	var a_search_results = searchRecord(s_list_name, null, null, columns);
	
	var list_values = new Array();
	
	for(var i = 0; a_search_results != null && i < a_search_results.length; i++)
		{
			list_values.push({'display':a_search_results[i].getValue('name'), 'value':a_search_results[i].id});
		}
	return list_values;
	//return getOptions(list_values, strSelectedValue, s_placeholder, removeBlankOption);
}

/*********************************************Function to get Search Results**********************************************/
function searchRecord(recordType, savedSearch, arrFilters, arrColumns,
        filterExpression)
{

	try {
		var search = null;

		// if a saved search is provided, load it and add the filters and
		// columns
		if (isNotEmpty(savedSearch)) {
			search = nlapiLoadSearch(recordType, savedSearch);

			if (isArrayNotEmpty(arrFilters)) {
				search.addFilters(arrFilters);
			}

			if (isArrayNotEmpty(arrColumns)) {
				search.addColumns(arrColumns);
			}

			if (isArrayNotEmpty(filterExpression)) {
				search.setFilterExpression(filterExpression);
			}
		}
		// create a new search
		else {
			search = nlapiCreateSearch(recordType, arrFilters, arrColumns);
		}

		// run search
		var resultSet = search.runSearch();

		// iterate through the search and get all data 1000 at a time
		var searchResultCount = 0;
		var resultSlice = null;
		var searchResult = [];

		do {
			resultSlice = resultSet.getResults(searchResultCount,
			        searchResultCount + 1000);

			if (resultSlice) {

				resultSlice.forEach(function(result) {

					searchResult.push(result);
					searchResultCount++;
				});
			}
		} while (isArrayNotEmpty(resultSlice) && resultSlice.length >= 1000);

		return searchResult;
	} catch (err) {
		nlapiLogExecution('ERROR', 'searchRecord', err);
		throw err;
	}
}

function isEmpty(value) {

	return value == null || value == "" || typeof (value) == undefined;
}

function isNotEmpty(value) {

	return !isEmpty(value);
}

function isArrayEmpty(argArray) {

	return !isArrayNotEmpty(argArray);
}

function isArrayNotEmpty(argArray) {

	return (isNotEmpty(argArray) && argArray.length > 0);
}
/*********************************************Function for adding selected options**********************************************/
function getOptions(a_data, i_selected_value, s_placeholder, removeBlankOptions)
{
	var strOptions	=	'';
	
	if(removeBlankOptions == true)
		{
		
		}
	else
		{
			strOptions = (s_placeholder == undefined?'':s_placeholder);
		}

	var strSelected = '';
	
	for(var i = 0; i < a_data.length; i++)
	{
		if(i_selected_value == a_data[i].value)
			{
				strSelected = 'selected';
			}
		else
			{
				strSelected = '';
			}
		strOptions += a_data[i].value+ strSelected +  a_data[i].display ;
	}
//nlapiLogExecution('Debug', 'stringsss', strOptions);
	return strOptions;
}
/*********************************************Function to get Search Values**********************************************/
function getSearchValues(s_list_name, strSelectedValue, s_placeholder, removeBlankOption)
{
	var columns = new Array();
	columns[0]	=	new nlobjSearchColumn('entityid');
	
	var a_search_results = searchRecord(null, s_list_name, null, columns);
	
	var list_values = new Array();
	
	for(var i = 0; a_search_results != null && i < a_search_results.length; i++)
		{
			list_values.push({'name':a_search_results[i].getValue('entityid'), 'id':a_search_results[i].id});
		}
	//nlapiLogExecution('Debug','list_values ',JSON.stringify(list_values));
	return list_values;//getOptions(list_values, strSelectedValue, s_placeholder, removeBlankOption);
}
