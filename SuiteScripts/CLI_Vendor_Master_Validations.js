/**
 * @author Shweta
 */
// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
/*
   	Script Name : CLI_Vendor_Master_Validations.js
	Author      : Shweta Chopde
	Date        : 9 May 2014
    Description : Apply validations on Address Line


	Script Modification Log:

	-- Date --			-- Modified By --				--Requested By--				-- Description --




	Below is a summary of the process controls enforced by this script file.  The control logic is described
	more fully, below, in the appropriate function headers and code blocks.

     PAGE INIT
		- pageInit(type)


     SAVE RECORD
		- saveRecord()


     VALIDATE FIELD
		- validateField(type, name, linenum)


     FIELD CHANGED
		- fieldChanged(type, name, linenum)


     POST SOURCING
		- postSourcing(type, name)


	LINE INIT
		- lineInit(type)


     VALIDATE LINE
		- validateLine()


     RECALC
		- reCalc()


     SUB-FUNCTIONS
		- The following sub-functions are called by the above core functions in order to maintain code
            modularization:





*/
}
// END SCRIPT DESCRIPTION BLOCK  ====================================



// BEGIN SCRIPT UPDATION BLOCK  ====================================
/*


*/
// END SCRIPT UPDATION BLOCK  ====================================




// BEGIN GLOBAL VARIABLE BLOCK  =====================================
{

	//  Initialize any Global Variables, in particular, debugging variables...


}
// END GLOBAL VARIABLE BLOCK  =======================================





// BEGIN PAGE INIT ==================================================

function pageInit(type)
{
    /*  On page init:

	- PURPOSE

		FIELDS USED:

		--Field Name--				--ID--			--Line Item Name--

    */

    //  LOCAL VARIABLES


    //  PAGE INIT CODE BODY


}

// END PAGE INIT ====================================================





// BEGIN SAVE RECORD ================================================

function saveRecord_vendor_master()
{
	
	var i_form = nlapiGetFieldValue('customform')
	alert(' Form -->'+i_form)			
				
	
	if(i_form == 4)
	{
	var i_country ;
    var i_recordID = nlapiGetRecordId();
	var i_PAN_No = ''
	var s_record_type = nlapiGetRecordType();
	
	//if(_logValidation(i_recordID)&&_logValidation(s_record_type))
	{
	//	var o_recordOBJ = nlapiLoadRecord(s_record_type,i_recordID)
		
	//	if(_logValidation(o_recordOBJ))
		{
			var i_address_line_count =  nlapiGetLineItemCount('addressbook')
			 nlapiLogExecution('DEBUG','saveRecord_vendor_master', ' i_address_line_count --> ' + i_address_line_count);
			//alert(' i_address_line_count'+i_address_line_count)
			
			 i_PAN_No = nlapiGetFieldValue('custentity_vendor_panno')
			 nlapiLogExecution('DEBUG','saveRecord_vendor_master', ' i_PAN_No --> ' + i_PAN_No);
			//alert(' PAN No.  -->'+i_PAN_No)		
				
			i_country = nlapiGetFieldValue('billcountry')
			nlapiLogExecution('DEBUG','saveRecord_vendor_master', ' i_country --> ' + i_country);
			//alert(' Country -->'+i_country)			
				
				
	/*
	//	if(_logValidation(i_address_line_count))
			{
				for(var i=1;i<=i_address_line_count;i++)
				{
					var i_default_billing =  nlapiGetLineItemValue('addressbook','defaultbilling',i) 
					nlapiLogExecution('DEBUG','saveRecord_vendor_master', ' i_default_billing --> ' + i_default_billing);
				
				    if(i_default_billing == 'T')
					{
					  i_country = 	 nlapiGetLineItemValue('addressbook','country',i) 
					  nlapiLogExecution('DEBUG','saveRecord_vendor_master', ' i_country --> ' + i_country);
				
				      break;
						
						
					}//Default Billing Checked				
				
				}//Loop Address Count				
				
			}//Address Line Count
*/
			
			if(i_country=='IN')
			{
				if(i_PAN_No==null || i_PAN_No==''||i_PAN_No==undefined || i_PAN_No.toString() ==null || i_PAN_No.toString()==undefined)
				{
					alert(' Please enter PAN No.')
					return false;
				}
			}
			if (i_address_line_count == null || i_address_line_count == '' || i_address_line_count == undefined || i_address_line_count.toString() == null || i_address_line_count.toString() == undefined) 
			{
				alert(' Please enter Address .')
				return false;
			}
			
			
		}//Record Object
		
	}//Record ID & Record Type
	}
	


	return true;

}

// END SAVE RECORD ==================================================





// BEGIN VALIDATE FIELD =============================================

function validateField(type, name, linenum)
{

	/*  On validate field:

          - EXPLAIN THE PURPOSE OF THIS FUNCTION


          FIELDS USED:

          --Field Name--				--ID--


    */


	//  LOCAL VARIABLES



	//  VALIDATE FIELD CODE BODY


	return true;

}

// END VALIDATE FIELD ===============================================


// BEGIN FIELD CHANGED ==============================================

function fieldChanged_vendor_master(type, name, linenum)
{
	var i_form = nlapiGetFieldValue('customform')
	
	if (i_form == 4) 
	{
		
   if(name == 'custentity_vendor_panno')
   {
   	var i_PAN_No = nlapiGetFieldValue('custentity_vendor_panno')
	nlapiLogExecution('DEBUG','fieldChanged_vendor_master', ' i_PAN_No --> ' + i_PAN_No);
		
	if(_logValidation(i_PAN_No))
	{
		if(i_PAN_No.toString().length!=10)
		{
			alert(' Only 10 characters are allowed for PAN No. ')
			nlapiSetFieldValue('custentity_vendor_panno','')
			return false;
		}		
	}	
   }//PAN No
   
   if(name == 'custentity_tinno')
   {
   	var i_Tin_No = nlapiGetFieldValue('custentity_tinno')
	nlapiLogExecution('DEBUG','fieldChanged_vendor_master', ' i_Tin_No --> ' + i_Tin_No);
	
	if(_logValidation(i_Tin_No))
	{
		if(i_Tin_No.toString().length!=11)
		{
			alert(' Only 11 characters are allowed for Tin No. ')
			nlapiSetFieldValue('custentity_tinno','')
			return false;
		}
	}			   	
   }//Tin No		
	}
  
}

// END FIELD CHANGED ================================================





// BEGIN POST SOURCING ==============================================

function postSourcing(type, name)
{

    /*  On post sourcing:

	- PURPOSE

          FIELDS USED:

		--Field Name--			--ID--		--Line Item Name--


    */

    //  LOCAL VARIABLES


    //  POST SOURCING CODE BODY



}

// END POST SOURCING ================================================



// BEGIN LINE INIT ==============================================

function lineInit(type)
{

    /*  On Line Init:

	- PURPOSE

          FIELDS USED:

		--Field Name--			--ID--		--Line Item Name--


    */

    //  LOCAL VARIABLES


    //  LINE INIT CODE BODY



}

// END LINE INIT ================================================


// BEGIN VALIDATE LINE ==============================================

function validateLine(type)
{

	/*  On validate line:

          - EXPLAIN THE PURPOSE OF THIS FUNCTION


          FIELDS USED:

          --Field Name--				--ID--


    */


	//  LOCAL VARIABLES


	//  VALIDATE LINE CODE BODY


	return true;

}

// END VALIDATE LINE ================================================




// BEGIN RECALC =====================================================

function recalc(type)
{

	/*  On recalc:

          - EXPLAIN THE PURPOSE OF THIS FUNCTION


          FIELDS USED:

          --Field Name--				--ID--


    */


	//  LOCAL VARIABLES


	//  RECALC CODE BODY


}

// END RECALC =======================================================





// BEGIN FUNCTION ===================================================
function _logValidation(value) 
{
 if(value != null && value.toString() != null && value != '' && value != undefined && value.toString() != undefined && value != 'undefined' && value.toString() != 'undefined'&& value.toString() != 'NaN' && value != NaN) 
 {
  return true;
 }
 else 
 { 
  return false;
 }
}




// END FUNCTION =====================================================
