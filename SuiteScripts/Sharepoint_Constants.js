/**
 * Constants for sharepoint
 * 
 * Version Date Author Remarks
 * 
 * 1.00 20 May 2016 Nitish Mishra
 * 
 */

var Sharepoint_Constants = {
	Request : {
	    Get : 'GET',
	    Post : 'POST',
	    Approve : 'APPROVE',
	    Reject : 'REJECT',
	    GetAll : 'GETALL'
	}
};

function Response() {
	this.Status = false;
	this.Data = "";
}

function getUserUsingEmailId(emailId) {
	try {
		var employeeSearch = nlapiSearchRecord('employee', null, [
		        new nlobjSearchFilter('email', null, 'is', emailId),
		        new nlobjSearchFilter('isinactive', null, 'is', 'F') ]);

		if (employeeSearch) {
			return employeeSearch[0].getId();
		} else {
			throw "User does not exist";
		}
	} catch (err) {
		nlapiLogExecution('ERROR', 'getUserUsingEmailId', err);
		throw err;
	}
}