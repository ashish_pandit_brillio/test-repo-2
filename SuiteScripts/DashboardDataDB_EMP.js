/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       19 Dec 2018     Aazamali Khan    Script will update employee existing on fulfillment dashboard data table			
 *
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType
 * 
 * @param {String} type Operation types: create, edit, delete, xedit,
 *                      approve, cancel, reject (SO, ER, Time Bill, PO & RMA only)
 *                      pack, ship (IF only)
 *                      dropship, specialorder, orderitems (PO only) 
 *                      paybills (vendor payments)
 * @returns {Void}
 */
function updateFulfillmentDashboardDB(type){
	var dashboardDBType = 'customrecord_fulfillment_dashboard_data';
	if (type == 'edit') {
		var emplRec = nlapiLoadRecord('employee', nlapiGetRecordId());
		var empExit = emplRec.getFieldValue('custentity_lwd');
		nlapiLogExecution('DEBUG', 'emplRec', emplRec.getFieldValue('custentity_lwd'));
		if (_logValidation(empExit)) {
			var searchResultRA = GetResourceAllocation(nlapiGetRecordId());
			nlapiLogExecution('DEBUG', 'searchResultRA', JSON.stringify(searchResultRA));
			if (searchResultRA != null) {
				for (var int = 0; int < searchResultRA.length; int++) {
					var resourceAllocationID = searchResultRA[int].getId();
					nlapiLogExecution('DEBUG', 'resource allocation ID :', resourceAllocationID);
					var projectId = nlapiLookupField('resourceallocation', resourceAllocationID, 'project');
					nlapiLogExecution('DEBUG', 'resource allocation ID :', projectId);
					var peopleExiting = GetEmployeeExist(projectId);
					var dashboardDBSearch = GetDashboardDB(projectId);
					if (dashboardDBSearch != null) {
						for (var int2 = 0; int2 < dashboardDBSearch.length; int2++) {
							var i_dashboardDB = dashboardDBSearch[int2].getId();
							nlapiSubmitField(dashboardDBType, i_dashboardDB, 'custrecord_fulfill_dashboard_exiting', peopleExiting);
                            nlapiLogExecution('DEBUG', 'i_dashboardDB', i_dashboardDB);
						}
					}
				}
			}
		}
	}
}
function GetResourceAllocation(i_empId) {
	var resourceallocationSearch = nlapiSearchRecord("resourceallocation",null,
			[
			 ["startdate","notafter","today"], 
			 "AND", 
			 ["enddate","notbefore","today"], 
			 "AND", 
			 ["resource","anyof",i_empId]
			 ], 
			 [
			  ]
	);
	return resourceallocationSearch;
}
function GetEmployeeExist(i_project) {
	Date.prototype.addDays = function(days) {
		var date = new Date(this.valueOf());
		date.setDate(date.getDate() + days);
		return date;
	}
	var date = new Date();
	var nextDate = date.addDays(30);
	nlapiLogExecution("AUDIT", "exiting date", date);
	var resourceallocationSearch = nlapiSearchRecord("resourceallocation",null,
			[
			 ["job.internalid","anyof",i_project], //5515 project internal ID
			 "AND", 
			 ["employee.custentity_lwd","within",date,nextDate] // 01/05/2019 calculated dynamically using current date and adding 30 days to it. 
			 ], 
			 [
			  new nlobjSearchColumn("resource",null,'GROUP')
			  ]
	);
	if (resourceallocationSearch) {
		return resourceallocationSearch.length;
	}else{
		return "0";
	}
}
function GetDashboardDB(i_project) {
	var customrecord_fulfillment_dashboard_dataSearch = nlapiSearchRecord("customrecord_fulfillment_dashboard_data",null,
			[
			 ["custrecord_fulfill_dashboard_opp_id","anyof","@NONE@"], 
			 "AND", 
			 ["custrecord_fulfill_dashboard_project","anyof",i_project]
			 ], 
			 [
			  ]
	);
	return customrecord_fulfillment_dashboard_dataSearch;
}
function _logValidation(value) 
{
	if(value!='null' && value != null && value != null && value != '' && value != undefined && value != undefined && value != 'undefined' && value!= 'undefined'&& value!= 'NaN' && value != NaN) 
	{
		return true;
	}
	else 
	{ 
		return false;
	}
}