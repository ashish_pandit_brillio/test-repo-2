function deleteTimesheet()
{
	try
	{
		//var timesheetSearch = nlapiSearchRecord("timesheet",3271,null,
		var timesheetSearch = searchRecord("timesheet",3731,null,
[
   new nlobjSearchColumn("id").setSort(false), 
   new nlobjSearchColumn("employee"), 
   new nlobjSearchColumn("startdate"), 
   new nlobjSearchColumn("totalhours"), 
   new nlobjSearchColumn("approvalstatus")
]
);
nlapiLogExecution('Debug','timesheetSearch length ',timesheetSearch.length);
		for(var count = 0; count < timesheetSearch.length; count++)
		{
          var context=nlapiGetContext();
            yieldScript(context);
			var id = timesheetSearch[count].getValue('id');
			nlapiDeleteRecord('timesheet',parseInt(id));
			nlapiLogExecution('Debug','Count',count);
		}
	}
	catch (err)
	{
		nlapiLogExecution('Debug','Delete Timesheet',err);
		//throw err;
	}
  finally {
  var timesheetSearch = searchRecord("timesheet",3731,
null,
[
   new nlobjSearchColumn("id").setSort(false), 
   new nlobjSearchColumn("employee"), 
   new nlobjSearchColumn("startdate"), 
   new nlobjSearchColumn("totalhours"), 
   new nlobjSearchColumn("approvalstatus")
]
);
		nlapiLogExecution('Debug','timesheetSearch length ',timesheetSearch.length);
		for(var count = 0; count < timesheetSearch.length; count++)
		{
             var context=nlapiGetContext();
            yieldScript(context);
			var id = timesheetSearch[count].getValue('id');
			nlapiDeleteRecord('timesheet',parseInt(id));
			nlapiLogExecution('Debug','Count',count);
		}
	}
}


function yieldScript(currentContext) {

	if (currentContext.getRemainingUsage() <= 2000) {
		nlapiLogExecution('AUDIT', 'API Limit Exceeded');
		var state = nlapiYieldScript();

		if (state.status == "FAILURE") {
			nlapiLogExecution('ERROR', 'Yield Script Failed', 'Reason : '
			        + state.reason + ' / Size : ' + state.size);
			return false;
		} else if (state.status == "RESUME") {
			nlapiLogExecution('AUDIT', 'Script Resumed');
		}
	}
}


function searchRecord(recordType, savedSearch, arrFilters, arrColumns,
        filterExpression)
{

	try {
		var search = null;

		// if a saved search is provided, load it and add the filters and
		// columns
		if (isNotEmpty(savedSearch)) {
			search = nlapiLoadSearch(recordType, savedSearch);

			if (isArrayNotEmpty(arrFilters)) {
				search.addFilters(arrFilters);
			}

			if (isArrayNotEmpty(arrColumns)) {
				search.addColumns(arrColumns);
			}

			if (isArrayNotEmpty(filterExpression)) {
				search.setFilterExpression(filterExpression);
			}
		}
		// create a new search
		else {
			search = nlapiCreateSearch(recordType, arrFilters, arrColumns);
		}

		// run search
		var resultSet = search.runSearch();

		// iterate through the search and get all data 1000 at a time
		var searchResultCount = 0;
		var resultSlice = null;
		var searchResult = [];

		do {
			resultSlice = resultSet.getResults(searchResultCount,
			        searchResultCount + 1000);

			if (resultSlice) {

				resultSlice.forEach(function(result) {

					searchResult.push(result);
					searchResultCount++;
				});
			}
		} while (isArrayNotEmpty(resultSlice) && resultSlice.length >= 1000);

		return searchResult;
	} catch (err) {
		nlapiLogExecution('ERROR', 'searchRecord', err);
		throw err;
	}
}

function isEmpty(value) {

	return value == null || value == "" || typeof (value) == undefined;
}

function isNotEmpty(value) {

	return !isEmpty(value);
}

function isArrayEmpty(argArray) {

	return !isArrayNotEmpty(argArray);
}

function isArrayNotEmpty(argArray) {

	return (isNotEmpty(argArray) && argArray.length > 0);
}
