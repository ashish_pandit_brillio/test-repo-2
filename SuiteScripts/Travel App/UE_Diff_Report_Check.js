/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       03 May 2016     amol.sahijwani
 *
 */

/**
 * @returns {Void} Any or no return value
 */
function workflowAction(type) {
	try
	{
		var context	=	nlapiGetContext();
		
		var oldRecord	=	nlapiGetOldRecord();
		var newRecord	=	nlapiGetNewRecord();
		
		var f_old_ST_hours_total	=	0.0;
		var f_old_OT_hours_total	=	0.0;
		var f_old_leave_hours_total =	0.0;
		var f_old_holiday_hours_total = 0.0;
		
		var f_new_ST_hours_total	=	0.0;
		var f_new_OT_hours_total	=	0.0;
		var f_new_leave_hours_total =	0.0;
		var f_new_holiday_hours_total = 0.0;
		
		// Array of Subrecord names
		var a_days = ['sunday', 'monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday'];
		
		//var event_type = nlapiGetContext().getSetting('SCRIPT', 'custscript_wa_payroll_diff_rep_event_typ');
		
		if(oldRecord != null && type != 'DELETE')
			{
				// Get Line Count
				var i_line_count_new = newRecord.getLineItemCount('timegrid');
				
				for(var i_line_indx = 1; i_line_indx <= i_line_count_new; i_line_indx++)
					{
						newRecord.selectLineItem('timegrid', i_line_indx);
						
						var emp_id = newRecord.getFieldValue('employee');
						var emp_type = nlapiLookupField('employee',emp_id,'employeetype');
						nlapiLogExecution('Debug','emp_id',emp_id);
						
						for(var i_day_indx = 0; i_day_indx < 7; i_day_indx++)
						{
							var sub_record_name = a_days[i_day_indx];
							var o_sub_record_view = newRecord.viewCurrentLineItemSubrecord('timegrid', sub_record_name);
							if(o_sub_record_view)
								{
									var i_item_id = o_sub_record_view.getFieldValue('item');
									
									var i_approval_status = o_sub_record_view.getFieldValue('approvalstatus');
									
									var f_duration	=	hoursToDuration(o_sub_record_view.getFieldValue('hours'));
									
									if(i_approval_status == 1 || i_approval_status == 2 || i_approval_status == 3)
										{
											if(emp_type == 3)
											{
												if(i_item_id == '2222' || i_item_id == '2221' || i_item_id == '2224' || i_item_id == '2426')
												{
													f_new_ST_hours_total += f_duration;
												}	 
												else if(i_item_id == '2425')
												{
													f_new_OT_hours_total += f_duration;
												}
												else if(i_item_id == '2479')
												{
													f_new_leave_hours_total += f_duration;
												}
												else if(i_item_id == '2480')
												{
													f_new_holiday_hours_total += f_duration;
												}
											}
											else
											{
												if(i_item_id == '2222' || i_item_id == '2221')
												{
													f_new_ST_hours_total += f_duration;
												}	 
												else if(i_item_id == '2425')
												{
													f_new_OT_hours_total += f_duration;
												}
												else if(i_item_id == '2479')
												{
													f_new_leave_hours_total += f_duration;
												}
												else if(i_item_id == '2480')
												{
													f_new_holiday_hours_total += f_duration;
												}
											}
										}									
									//nlapiLogExecution('AUDIT', 'New Record', 'Day: ' + sub_record_name + ', Item ID: ' + i_item_id + ', Approval Status: ' + i_approval_status + ', Duration: ' + f_duration);
								}
						}
					}
					
					//Old Timesheet data
					
					var i_line_count_old = oldRecord.getLineItemCount('timegrid');
					for(var i_line_indx = 1; i_line_indx <= i_line_count_old; i_line_indx++)
					{
						oldRecord.selectLineItem('timegrid', i_line_indx);
						
						var emp_id = oldRecord.getFieldValue('employee');
						var emp_type = nlapiLookupField('employee',emp_id,'employeetype');
						nlapiLogExecution('Debug','emp_id',emp_id);
						
						for(var i_day_indx = 0; i_day_indx < 7; i_day_indx++)
						{
							var sub_record_name = a_days[i_day_indx];
							var o_sub_record_view = oldRecord.viewCurrentLineItemSubrecord('timegrid', sub_record_name);
							if(o_sub_record_view)
								{
									var i_item_id = o_sub_record_view.getFieldValue('item');
									
									var i_approval_status = o_sub_record_view.getFieldValue('approvalstatus');
									
									var f_duration	=	hoursToDuration(o_sub_record_view.getFieldValue('hours'));
									
									if(i_approval_status == 1 || i_approval_status == 2 || i_approval_status == 3)
										{
											if(emp_type == 3)
											{
												if(i_item_id == '2222' || i_item_id == '2221' || i_item_id == '2224' || i_item_id == '2426')
												{
													f_old_ST_hours_total += f_duration;
												}	 
												else if(i_item_id == '2425')
												{
													f_old_OT_hours_total += f_duration;
												}
												else if(i_item_id == '2479')
												{
													f_old_leave_hours_total += f_duration;
												}
												else if(i_item_id == '2480')
												{
													f_old_holiday_hours_total += f_duration;
												}
											}
											else
											{
												if(i_item_id == '2222' || i_item_id == '2221')
												{
													f_old_ST_hours_total += f_duration;
												}	 
												else if(i_item_id == '2425')
												{
													f_old_OT_hours_total += f_duration;
												}
												else if(i_item_id == '2479')
												{
													f_old_leave_hours_total += f_duration;
												}
												else if(i_item_id == '2480')
												{
													f_old_holiday_hours_total += f_duration;
												}
											}
										}									
									//nlapiLogExecution('AUDIT', 'New Record', 'Day: ' + sub_record_name + ', Item ID: ' + i_item_id + ', Approval Status: ' + i_approval_status + ', Duration: ' + f_duration);
								}
						}
					}
			}		
		
	/*	if(type != 'DELETE')
			{
				// Get Line Count
				var i_line_count_new = newRecord.getLineItemCount('timegrid');
				
				for(var i_line_indx = 1; i_line_indx <= i_line_count_new; i_line_indx++)
					{
						newRecord.selectLineItem('timegrid', i_line_indx);
						
						var emp_id = newRecord.getFieldValue('employee');
						var emp_type = nlapiLookupField('employee',emp_id,'employeetype');
						nlapiLogExecution('Debug','emp_id',emp_id);
						
						for(var i_day_indx = 0; i_day_indx < 7; i_day_indx++)
						{
							var sub_record_name = a_days[i_day_indx];
							var o_sub_record_view = newRecord.viewCurrentLineItemSubrecord('timegrid', sub_record_name);
							if(o_sub_record_view)
								{
									var i_item_id = o_sub_record_view.getFieldValue('item');
									
									var i_approval_status = o_sub_record_view.getFieldValue('approvalstatus');
									
									var f_duration	=	hoursToDuration(o_sub_record_view.getFieldValue('hours'));
									
									if(i_approval_status == 1 || i_approval_status == 2 || i_approval_status == 3)
										{
											if(emp_type == 3)
											{
												if(i_item_id == '2222' || i_item_id == '2221' || i_item_id == '2224' || i_item_id == '2426')
												{
													f_new_ST_hours_total += f_duration;
												}	 
												else if(i_item_id == '2425')
												{
													f_new_OT_hours_total += f_duration;
												}
												else if(i_item_id == '2479')
												{
													f_new_leave_hours_total += f_duration;
												}
												else if(i_item_id == '2480')
												{
													f_new_holiday_hours_total += f_duration;
												}
											}
											else
											{
												if(i_item_id == '2222' || i_item_id == '2221')
												{
													f_new_ST_hours_total += f_duration;
												}	 
												else if(i_item_id == '2425')
												{
													f_new_OT_hours_total += f_duration;
												}
												else if(i_item_id == '2479')
												{
													f_new_leave_hours_total += f_duration;
												}
												else if(i_item_id == '2480')
												{
													f_new_holiday_hours_total += f_duration;
												}
											}
										}									
									//nlapiLogExecution('AUDIT', 'New Record', 'Day: ' + sub_record_name + ', Item ID: ' + i_item_id + ', Approval Status: ' + i_approval_status + ', Duration: ' + f_duration);
								}
						}
					}
					
					//Old Timesheet data
					
					var i_line_count_old = oldRecord.getLineItemCount('timegrid');
					for(var i_line_indx = 1; i_line_indx <= i_line_count_old; i_line_indx++)
					{
						oldRecord.selectLineItem('timegrid', i_line_indx);
						
						var emp_id = oldRecord.getFieldValue('employee');
						var emp_type = nlapiLookupField('employee',emp_id,'employeetype');
						nlapiLogExecution('Debug','emp_id',emp_id);
						
						for(var i_day_indx = 0; i_day_indx < 7; i_day_indx++)
						{
							var sub_record_name = a_days[i_day_indx];
							var o_sub_record_view = oldRecord.viewCurrentLineItemSubrecord('timegrid', sub_record_name);
							if(o_sub_record_view)
								{
									var i_item_id = o_sub_record_view.getFieldValue('item');
									
									var i_approval_status = o_sub_record_view.getFieldValue('approvalstatus');
									
									var f_duration	=	hoursToDuration(o_sub_record_view.getFieldValue('hours'));
									
									if(i_approval_status == 1 || i_approval_status == 2 || i_approval_status == 3)
										{
											if(emp_type == 3)
											{
												if(i_item_id == '2222' || i_item_id == '2221' || i_item_id == '2224' || i_item_id == '2426')
												{
													f_old_ST_hours_total += f_duration;
												}	 
												else if(i_item_id == '2425')
												{
													f_old_OT_hours_total += f_duration;
												}
												else if(i_item_id == '2479')
												{
													f_old_leave_hours_total += f_duration;
												}
												else if(i_item_id == '2480')
												{
													f_old_holiday_hours_total += f_duration;
												}
											}
											else
											{
												if(i_item_id == '2222' || i_item_id == '2221')
												{
													f_old_ST_hours_total += f_duration;
												}	 
												else if(i_item_id == '2425')
												{
													f_old_OT_hours_total += f_duration;
												}
												else if(i_item_id == '2479')
												{
													f_old_leave_hours_total += f_duration;
												}
												else if(i_item_id == '2480')
												{
													f_old_holiday_hours_total += f_duration;
												}
											}
										}									
									//nlapiLogExecution('AUDIT', 'New Record', 'Day: ' + sub_record_name + ', Item ID: ' + i_item_id + ', Approval Status: ' + i_approval_status + ', Duration: ' + f_duration);
								}
						}
					}
			}	*/	
		
		nlapiLogExecution('AUDIT', 'Hours', type + ', ST: ' + f_old_ST_hours_total + ' -> ' + f_new_ST_hours_total + ', OT: ' + f_old_OT_hours_total + ' -> ' + f_new_OT_hours_total + ', Leave: ' + f_old_leave_hours_total + ' -> ' + f_new_leave_hours_total + ', Holiday: ' + f_old_holiday_hours_total + ' -> ' + f_new_holiday_hours_total);
		//nlapiLogExecution('AUDIT', 'Old Record', JSON.stringify(oldRecord));
		//nlapiLogExecution('AUDIT', 'New Record', JSON.stringify(newRecord));
		nlapiLogExecution('AUDIT', 'f_new_ST_hours_total, f_old_ST_hours_total ,f_new_OT_hours_total,f_old_OT_hours_total, f_new_leave_hours_total,f_old_leave_hours_total,f_new_holiday_hours_total,f_old_holiday_hours_total', [f_new_ST_hours_total, f_old_ST_hours_total ,f_new_OT_hours_total,f_old_OT_hours_total, f_new_leave_hours_total,f_old_leave_hours_total,f_new_holiday_hours_total,f_old_holiday_hours_total]);
		
		if(f_new_ST_hours_total != f_old_ST_hours_total || f_new_OT_hours_total != f_old_OT_hours_total || f_new_leave_hours_total != f_old_leave_hours_total || f_new_holiday_hours_total != f_old_holiday_hours_total)
			{
				var i_employee_id	=	newRecord.getFieldValue('employee');
				var s_end_date		=	newRecord.getFieldValue('enddate');
				
				var newLogRecord	=	nlapiCreateRecord('customrecord_payroll_ts_diff_rep_data');
				newLogRecord.setFieldValue('custrecord_ptdr_action', event_type);
				newLogRecord.setFieldValue('custrecord_ptdr_employee', i_employee_id);
				newLogRecord.setFieldValue('custrecord_ptdr_weekending', s_end_date);
				newLogRecord.setFieldValue('custrecord_ptdr_old_st_hours', f_old_ST_hours_total);
				newLogRecord.setFieldValue('custrecord_ptdr_old_ot_hours', f_old_OT_hours_total);
				newLogRecord.setFieldValue('custrecord_ptdr_old_leave_hours', f_old_leave_hours_total);
				newLogRecord.setFieldValue('custrecord_ptdr_old_holiday_hours', f_old_holiday_hours_total);
				newLogRecord.setFieldValue('custrecord_ptdr_new_st_hours', f_new_ST_hours_total);
				newLogRecord.setFieldValue('custrecord_ptdr_new_ot_hours', f_new_OT_hours_total);
				newLogRecord.setFieldValue('custrecord_ptdr_new_leave_hours', f_new_leave_hours_total);
				newLogRecord.setFieldValue('custrecord_ptdr_new_holiday_hours', f_new_holiday_hours_total);
				newLogRecord.setFieldValue('custrecord_ptdr_updated_by', i_employee_id);
				nlapiSubmitRecord(newLogRecord);
			}	
	}
	catch(e)
	{
		nlapiLogExecution('AUDIT', 'Error', e.message);
		nlapiSendEmail(442, 'information.systems@brillio.com', 'Timesheet hours changed', e.message, null, null, null, null, null);	
	}
	
}

function hoursToDuration(s_hours)
{
	var a_duration = s_hours.split(":");
	
	return parseFloat(a_duration[0]) + parseFloat(a_duration[1])/60.0;
}