/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       16 Oct 2015     amol.sahijwani
 *
 */

// Define constants
var K_STATUS	=	{'OPEN':'1', 'PENDING_APPROVAL':'2', 'IMMIGRATION':'3', 'TRAVEL':'4', 'APPROVED':'5', 'REJECTED':'6'};
var K_EMAIL_TEMPLATES	=	{'SUBMITTED': 23, 'APPROVED': 24, 'REJECTED': 25, 'IMMIGRATION_APPROVED': 26, 'TRAVEL_APPROVED': 27};
var K_EMPLOYEE_IDS		=	{'AMOL_SAHIJWANI': 9122, 'ANIRUDH_BANDI': 2128, 'PAAYAL_ARUN_ASHOK': 9419, 'NITISH_MISHRA': 9673, 'SRIDHAR': 8871, 'LAVANYA': 8020, 'PALLAVI': 137228, 'SANTOSH': 7074, 'HARSH_KUMAR': 9629, 'AGNEL' : 149390};
var K_ROLES		=	{'TRAVEL': [K_EMPLOYEE_IDS.PALLAVI, K_EMPLOYEE_IDS.SANTOSH], 'IMMIGRATION': [K_EMPLOYEE_IDS.ALFRED, K_EMPLOYEE_IDS.AMOL_SAHIJWANI, K_EMPLOYEE_IDS.HARSH_KUMAR]};
var K_DOMESTIC_INTERNATIONAL	=	{'DOMESTIC': 1, 'INTERNATIONAL': 2};
var K_TRAVEL_APPROVER_ROLE	=	{'APPROVER': 1, 'IMMIGRATION': 2, 'TRAVEL': 3};
var K_VISA_TYPE	=	{'WORK': 1, 'BUSINESS': 2};
var K_DISABLE_CUSTOMERS = {'CUSTOMER': []};
var K_DISABLE_PROJECTS = {'PROJECT': ['196823']};

var email_configuration	=	
{
	'IMMIGRATION':['usimmigration@brillio.com', 'harsh.kumar@brillio.com','mandeep.d@BRILLIO.COM','tilak.s@brillio.com','business.ops@brillio.com'],
	'TRAVEL':['traveldesk@brillio.com', 'pallavi.g@brillio.com','agnel.joseph@brillio.com','rashmi.l@brillio.com'],
	'TRAVELDESK':['traveldesk@brillio.com'],
	'FINANCE':['santosh.banadawar@BRILLIO.COM','govind.kharayat@brillio.com','priyanka.s3@BRILLIO.COM'],
	'FMG':['fmg@brillio.com'],
	'OPS':['business.ops@brillio.com'],
	'HR_US':['tilak.s@BRILLIO.COM','paulina.b@BRILLIO.COM'],
	'TA':['talentacquisitionus@brillio.com'],
	'INDIA_HR':['Jayarajan.c@brillio.com','aditi.sood@brillio.com','soubhik.chandaa@BRILLIO.COM'],
	//'INDIA_COMP_TEAM':['satyanarayan.kb@BRILLIO.COM'],
	'RMG_US':['fulfilment.us@brillio.com'],
	'RMG_INDIA':['team.fulfilment@brillio.com']
};