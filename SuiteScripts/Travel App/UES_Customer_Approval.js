function afterSubmit() 
{
	
	try
	{
		if(type == 'edit')
		{
			var oldRec = nlapiGetOldRecord();
			var status = oldRec.getFieldValue('isinactive');
			
			var value = nlapiGetFieldValue('isinactive');
			
			if(status == 'T' && value == 'F')
			{
				nlapiLogExecution('AUDIT', 'Test', nlapiGetRecordId());
				var rec = nlapiLoadRecord('customer', nlapiGetRecordId());
	
				//var NS_Id = rec.getFieldValue('entityid');
				//var SFDC_Id = rec.getFieldValue('custentity_sfdc_account_id');
	
				var dataRow = [];
	
				dataRow = 
				{
					'NetsuiteCustomerID' : nlapiGetFieldValue('entityid'),
					'AccountID' : nlapiGetFieldValue('custentity_sfdc_account_id')
				}
				var request =  new RequestData()
				var arr = [];
				arr.push(dataRow);
				request.Data = arr;
				
				var accessURL = getAccessToken();
				var method = 'POST'
				
				var response = nlapiRequestURL(accessURL,null,null,method);
				var temp = response.body;
				nlapiLogExecution('DEBUG','JSON',temp);
				var data = JSON.parse(response.body);
				var access_token_SFDC = data.access_token;
				var instance_URL_Sfdc = data.instance_url;
				nlapiLogExecution('DEBUG','JSON',data);
		
				var dynamic_URL = instance_URL_Sfdc+'/services/apexrest/AccountApprovedBackUpdate/v1.0/';
				var auth = 'Bearer'+' '+access_token_SFDC;
		
				var headers = {"Authorization": "Bearer "+access_token_SFDC,
				"Content-Type": "application/json",
	        		 "accept": "application/json"};
					 
				var method_ = 'POST';
				var response_ = nlapiRequestURL(dynamic_URL,JSON.stringify(request),headers,method_);
				var data = (response_.body);
				var message = data[0].message;
				var details_ = data.details;
				nlapiLogExecution('DEBUG','Body',data); 
				nlapiLogExecution('DEBUG','message',message); 
				nlapiLogExecution('DEBUG','details_',details_); 
		
				nlapiLogExecution('DEBUG','JSON',JSON.stringify(request)); 
			}
		}		
	}
	catch(e)
	{
		nlapiLogExecution('Debug','Error',e);
	}	
	
}

function RequestData() {
	//this.Status = false;
  	//this.TimeStamp = "";
	this.Data = "";
  
}