function postRestlet(dataIn) {
    try {


        //var requestType = 'CREATION';
        //var Data_Array; data array
        /*var dataIn = [];
        dataIn = {
        	"RequestType" : "UPDATE",
        	"Data" : [
        	{
        		"AccountID" : "Acc-2548",
        		"Account_Region" : "India",
        		"Account_Name" : "	Elite brillio"
        	}
        ]
        }*/

        var requestType = dataIn.RequestType;
        var response = new Response()
        nlapiLogExecution('DEBUG', 'Request', requestType);
        nlapiLogExecution('DEBUG', 'Data', dataIn);

        switch (requestType) {

            case M_Constants.Request.Creation:
                response.Data = createProspect(dataIn);
                response.Status = true;
                break;

            case M_Constants.Request.Update:
                response.Data = updateProspect(dataIn);
                response.Status = true;
                break;

        }

    } catch (error) {
        nlapiLogExecution('ERROR', 'postRESTlet', error);
        response.Data = error;
        response.Status = false;

        var file = nlapiCreateFile("prospect.json", "PLAINTEXT", dataIn);
        var emailSub = "Error in Prospect Creation";
        var emailBody = "Hi," + "\r\n" + "Error while creating prospect to netsuite" + "\r\n" + "Please find attached payLoad." + "\r\n";
        nlapiSendEmail(442, ["prabhat.gupta@brillio.com", "sridhar.v@brillio.com"], emailSub, emailBody, null, null, null, file);
        nlapiLogExecution('ERROR', 'mail sent to prabhat.gupta@brillio.com and sridhar.v@brillio.com');
    }
    nlapiLogExecution('debug', 'response', JSON.stringify(response));
    return response;
}

function createProspect(dataIn) {
    try {
        var o_prospect_data = dataIn.Data;
        //if (isArray(o_prospect_data))
        //{
        nlapiLogExecution('debug', 'Length is ', o_prospect_data.length);
        for (var count = 0; count < o_prospect_data.length; count++) {
            var SFDC_account = o_prospect_data[count].AccountID;
            nlapiLogExecution('DEBUG', 'Account ID', SFDC_account);

            var SFDC_region = o_prospect_data[count].Account_Region;
            nlapiLogExecution('DEBUG', 'Account Region', SFDC_region);

            var SFDC_account_name = o_prospect_data[count].Account_Name;
            nlapiLogExecution('DEBUG', 'Account Name', SFDC_account_name);

            var rec = nlapiCreateRecord('prospect', {
                recordmode: 'dynamic'
            });
            rec.setFieldValue('customform', -8); //Custom Lead Form
            rec.setFieldValue('entitystatus', 8); //Stage: Prospect-In Discussion
            rec.setFieldValue('companyname', SFDC_account_name);
            rec.setFieldValue('custentity_region_sfdc', SFDC_region);

            if (SFDC_region) {
                var customrecord_regionSearch = nlapiSearchRecord("customrecord_region", null,
                    [
                        ["name", "contains", regExReplacer(SFDC_region)]
                    ],
                    [
                        new nlobjSearchColumn("internalid")
                    ]
                );
                if (customrecord_regionSearch) {
                    rec.setFieldValue('custentity_region', customrecord_regionSearch[0].getValue('internalid'));
                }
            }

            var i_subsidairy = '';
            if (SFDC_region == 'East' || SFDC_region == 'West' || SFDC_region == 'Central' || SFDC_region == 'East' || SFDC_region == 'House') {
                i_subsidairy = 2;
            } else if (SFDC_region == 'India') {
                i_subsidairy = 3;
            } else if (SFDC_region == 'Europe' || SFDC_region == 'UK') {
                i_subsidairy = 7;
            } else if (SFDC_region == 'Canada') {
                i_subsidairy = 10;
            } else if (SFDC_region == 'Cognetik') { // NIS-1691 prabhat gupta 21/8/2020 added subsidiary cognetik
                i_subsidairy = 21; // NIS-1691 prabhat gupta 21/8/2020 subsidiary is 21 for sandbox and 11 for production
            }


            rec.setFieldValue('subsidiary', i_subsidairy);
            rec.setFieldValue('custentity_sfdc_account_id', SFDC_account);
            var id = nlapiSubmitRecord(rec, false, false);

            var account_list = []; //JSON Response
            var loadRec = nlapiLoadRecord('prospect', id)
            account_list.push({
                AccountID: loadRec.getFieldValue('custentity_sfdc_account_id'),
                NetsuiteCustomerID: loadRec.getFieldValue('entityid')
            });

        }
        return account_list;
    } catch (err) {
        nlapiLogExecution('Error', 'Error in Prospect creation', err);
        throw err;
    }
}

function updateProspect(dataIn) {
    try {
        var o_prospect_data = dataIn.Data;
        var account_list = []; //JSON Response
        //if (isArray(o_prospect_data))
        //{
        nlapiLogExecution('debug', 'Length is ', o_prospect_data.length);
        for (var count = 0; count < o_prospect_data.length; count++) {
            var SFDC_account = o_prospect_data[count].AccountID;
            nlapiLogExecution('DEBUG', 'Account ID', SFDC_account);

            var SFDC_region = o_prospect_data[count].Account_Region;
            nlapiLogExecution('DEBUG', 'Account Region', SFDC_region);

            var SFDC_account_name = o_prospect_data[count].Account_Name;
            nlapiLogExecution('DEBUG', 'Account Name', SFDC_account_name);

            var prospectSearch = nlapiSearchRecord("prospect", null,
                [
                    ["stage", "anyof", "PROSPECT"],
                    "AND",
                    ["custentity_sfdc_account_id", "is", SFDC_account],
                    "AND",
                    ["isinactive", "is", "F"]
                ],
                [
                    new nlobjSearchColumn("entityid").setSort(false),
                    new nlobjSearchColumn("altname"),
                    new nlobjSearchColumn("internalid"),
                    new nlobjSearchColumn("custentity_sfdc_account_id"),
                    new nlobjSearchColumn("custentity_region"),
                    new nlobjSearchColumn("custentity_region_sfdc"),
                    new nlobjSearchColumn("subsidiary")
                ]);
            if (prospectSearch) {

                var subsidiary = prospectSearch[count].getValue('subsidiary');
                var NS_CustId = prospectSearch[count].getValue('entityid');
                var oldrec = prospectSearch[count].getValue('internalid');

                var i_subsidairy = '';
                if (SFDC_region == 'East' || SFDC_region == 'West' || SFDC_region == 'Central' || SFDC_region == 'East' || SFDC_region == 'House') {
                    i_subsidairy = 2;
                } else if (SFDC_region == 'India') {
                    i_subsidairy = 3;
                } else if (SFDC_region == 'Europe' || SFDC_region == 'UK') {
                    i_subsidairy = 7;
                } else if (SFDC_region == 'Canada') {
                    i_subsidairy = 10;
                }else if (SFDC_region == 'Cognetik') { // NIS-1691 prabhat gupta 21/8/2020 added subsidiary cognetik
                i_subsidairy = 21; // NIS-1691 prabhat gupta 21/8/2020 subsidiary is 21 for sandbox and 11 for production
                }

                if (subsidiary == i_subsidairy) {
                    if (SFDC_region) {
                        var customrecord_regionSearch = nlapiSearchRecord("customrecord_region", null,
                            [
                                ["name", "contains", regExReplacer(SFDC_region)]
                            ],
                            [
                                new nlobjSearchColumn("internalid")
                            ]
                        );
                        if (customrecord_regionSearch) {
                            var rec = nlapiLoadRecord('prospect', oldrec);
                            rec.setFieldValue('custentity_region', customrecord_regionSearch[0].getValue('internalid'));
                            rec.setFieldValue('custentity_region_sfdc', SFDC_region);
                            rec.setFieldValue('companyname', SFDC_account_name);
                            var id = nlapiSubmitRecord(rec, true, true);


                            var loadRec = nlapiLoadRecord('prospect', id)
                            account_list.push({
                                AccountID: loadRec.getFieldValue('custentity_sfdc_account_id'),
                                NetsuiteCustomerID: loadRec.getFieldValue('entityid')
                            });


                        }

                    }
                } else {


                    var rec = nlapiCreateRecord('prospect', {
                        recordmode: 'dynamic'
                    });
                    rec.setFieldValue('customform', -8); //Custom Lead Form
                    rec.setFieldValue('entitystatus', 8); //Stage: Prospect-In Discussion
                    rec.setFieldValue('companyname', SFDC_account_name);
                    rec.setFieldValue('custentity_region_sfdc', SFDC_region);

                    if (SFDC_region) {
                        var customrecord_regionSearch = nlapiSearchRecord("customrecord_region", null,
                            [
                                ["name", "contains", regExReplacer(SFDC_region)]
                            ],
                            [
                                new nlobjSearchColumn("internalid")
                            ]
                        );
                        if (customrecord_regionSearch) {
                            rec.setFieldValue('custentity_region', customrecord_regionSearch[0].getValue('internalid'));
                        }
                    }

                    rec.setFieldValue('subsidiary', i_subsidairy);
                    rec.setFieldValue('custentity_sfdc_account_id', SFDC_account);
                    var new_id = nlapiSubmitRecord(rec, false, false);
                    nlapiLogExecution('Debug', 'New InternalId', new_id);


                    var new_prospectid = nlapiLookupField('prospect', new_id, ['entityid', 'custentity_region_sfdc', 'custentity_region', 'custentity_sfdc_account_id']);

                    //Response
                    account_list.push({
                        AccountID: new_prospectid.custentity_sfdc_account_id,
                        NetsuiteCustomerID: new_prospectid.entityid
                    });

                    //search sfdc opportunity record for existing propsect
                    var update_oppRec = UpdateSFDC_Record_With_NewID(oldrec, new_prospectid, new_id);

                    //search Fulfillment Dashboard
                    var update_FulfillmentRec = UpdateFulfillmentDashboard_Rec_with_NewID(oldrec, new_prospectid, new_id);


                    //FRF details Search
                    nlapiLogExecution('debug', 'Old Id', oldrec);
                    var customrecord_frf_detailsSearch = nlapiSearchRecord("customrecord_frf_details", null,
                        [
                            ["custrecord_frf_details_account", "anyof", parseInt(oldrec)]
                        ],
                        [
                            new nlobjSearchColumn("custrecord_frf_details_account"),
                            new nlobjSearchColumn("internalid")
                        ]
                    );

                    if (customrecord_frf_detailsSearch) {
                        for (var i = 0; i < customrecord_frf_detailsSearch.length; i++) {
                            var id = customrecord_frf_detailsSearch[i].getValue('internalid');
                            nlapiLogExecution('Debug', 'FRF id', id);
                            nlapiSubmitField('customrecord_frf_details', parseInt(id),
                                'custrecord_frf_details_account', parseInt(new_id))
                        }
                    }

                    //FRF Plan details search
                    var customrecord_frf_plandetailsSearch = nlapiSearchRecord("customrecord_frf_plan_details", null,
                        [
                            ["custrecord_frf_plan_details_ns_account", "anyof", oldrec]
                        ],
                        [
                            new nlobjSearchColumn("custrecord_frf_plan_details_ns_account"),
                            new nlobjSearchColumn("internalid")
                        ]
                    );
                    if (customrecord_frf_plandetailsSearch) {
                        for (var j = 0; j < customrecord_frf_plandetailsSearch.length; j++) {
                            var frf_plan_id = customrecord_frf_plandetailsSearch[j].getValue('internalid');
                            //var frf_plan_id = customrecord_frf_plandetailsSearch[j].getId()
                            nlapiLogExecution('Debug', 'FRF Plan id', frf_plan_id);
                            nlapiSubmitField('customrecord_frf_plan_details', parseInt(frf_plan_id),
                                'custrecord_frf_plan_details_ns_account', parseInt(new_id));
                        }
                    }

                    //Old record Inactive
                    var old_rec = nlapiLoadRecord('prospect', oldrec);
                    old_rec.setFieldValue('isinactive', 'T');
                    nlapiSubmitRecord(old_rec, true, true);
                }


            }


        }
        return account_list;
        //}
    } catch (err) {
        nlapiLogExecution('Debug', 'Error in updating the Prospect', err);
        throw err;
    }
}


function regExReplacer(str) {
    return str.replace(/\n|\r/g, "");
}

function UpdateSFDC_Record_With_NewID(old_id, new_prospectid, new_id) {

    try {

        var customrecord_sfdc_opportunity_recordSearch = nlapiSearchRecord("customrecord_sfdc_opportunity_record", null,
            [
                ["custrecord_customer_internal_id_sfdc", "is", parseInt(old_id)]
            ],
            [
                new nlobjSearchColumn("custrecord_customer_internal_id_sfdc"),
                new nlobjSearchColumn("custrecord_opp_account_region"),
                new nlobjSearchColumn("custrecord_sfdc_opp_account_region"),
                new nlobjSearchColumn("internalid")
            ]
        );


        if (customrecord_sfdc_opportunity_recordSearch) {

            for (var i = 0; i < customrecord_sfdc_opportunity_recordSearch.length; i++) {
                var opp_id = customrecord_sfdc_opportunity_recordSearch[i].getValue('internalid');
                /*var Record = nlapiLoadRecord('customrecord_sfdc_opportunity_record',parseInt(opp_id));
                Record.setFieldValue('custrecord_customer_internal_id_sfdc',new_id);
                Record.setFieldValue('custrecord_sfdc_opp_account_region',new_prospectid.custentity_region);
                Record.setFieldValue('custrecord_opp_account_region',new_prospectid.custentity_region_sfdc);
                Record.setFieldValue('custrecord_customer_sfdc',new_prospectid.entityid);
                nlapiSubmitRecord(Record);*/

                nlapiSubmitField('customrecord_sfdc_opportunity_record', parseInt(opp_id),
                    ['custrecord_customer_internal_id_sfdc', 'custrecord_sfdc_opp_account_region',
                        'custrecord_opp_account_region', 'custrecord_customer_sfdc'
                    ],
                    [parseInt(new_id), new_prospectid.custentity_region, new_prospectid.custentity_region_sfdc, new_prospectid.entityid])
            }
        }

    } catch (e) {
        nlapiLogExecution('Debug', 'Error in updating the SFDC record', e);
        throw e;
    }

}

function UpdateFulfillmentDashboard_Rec_with_NewID(oldrec, new_prospectid, new_id) {
    try {
        var customrecord_fulfillment_dashboard_recordSearch = nlapiSearchRecord("customrecord_fulfillment_dashboard_data", null,
            [
                ["custrecord_fulfill_dashboard_account", "is", parseInt(oldrec)]
            ],
            [
                new nlobjSearchColumn("custrecord_fulfill_dashboard_account"),
                new nlobjSearchColumn("internalid")
            ]
        );

        nlapiLogExecution('Debug', 'Fulfillment Record Length', customrecord_fulfillment_dashboard_recordSearch.length)
        if (customrecord_fulfillment_dashboard_recordSearch) {
            for (var i = 0; i < customrecord_fulfillment_dashboard_recordSearch.length; i++) {
                var recId = customrecord_fulfillment_dashboard_recordSearch[i].getValue('internalid');
                nlapiLogExecution('Debug', 'Fulfillment Record ID', parseInt(recId));

                nlapiSubmitField('customrecord_fulfillment_dashboard_data', parseInt(recId), 'custrecord_fulfill_dashboard_account', parseInt(new_id));
            }
        }
    } catch (error) {
        nlapiLogExecution('Debug', 'Error in updating Fulfillment Dashboard record', error);
        throw error;
    }
}