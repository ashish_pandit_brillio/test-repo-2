function beforeLoad(type,form)
{
	try
	{
		if(type == 'create' || type == 'edit' || type == 'delete' || type == 'view')
		{
          
          	var i_user_id = nlapiGetUser();
          
			if(i_user_id != parseInt(5748) || i_user_id != parseInt(97260))
			{
              	nlapiLogExecution('Audit','User -> ',i_user_id);
				form.getField('exchangerate').setDisplayType('disabled');;
				//nlapiLogExecution('Debug','Exchange Rate',ExchangeRate);
				//if(ExchangeRate != null)
				//ExchangeRate.setDisplayType('disabled');
			}
		}	
		
	}
	catch (err)
	{
			nlapiLogExecution('Debug','Error',err);
			throw err;
	}	
}