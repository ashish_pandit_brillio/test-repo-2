/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       05 Aug 2016     shruthi.l
 *
 */

/**
 * @param {Object} dataIn Parameter object
 * @returns {Object} Output object
 */
function postRESTlet(dataIn) {
	var response = new Response();
	try {
		nlapiLogExecution('debug', 'dataIn', JSON.stringify(dataIn));
		var employeeId = getUserUsingEmailId(dataIn.EmailId);
		nlapiLogExecution('debug', 'dataIn employeeId', employeeId);
		
		var transactionDate = dataIn.Data.Date;
		nlapiLogExecution('debug', 'dataIn transactionDate', transactionDate);
		var T_date = dateSplit(transactionDate);
		nlapiLogExecution('debug', 'T_date transactionDate', T_date); 
		
		//Testign Data
		/*var employeeId = 30237;
		var transactionDate = '22/8/2016';
		var T_date = dateSplit(transactionDate); */
		
		//var requestType = dataIn.RequestType;
		//var expenseId = dataIn.Data.ExpenseId;
	//	var project_ID = dataIn.ProjectName;
	//	switch (requestType) {

	//	case M_Constants.Request.Get:

			if (employeeId) {
				response.Data = getEmployeeDetail(employeeId, T_date);
				response.Status = true;
			} 
			
		
		
	} catch (err) {
		nlapiLogExecution('ERROR', 'postRESTlet', err);
		response.Data = err;
		response.Status = false;
	}
	return response;
}

function dateSplit(tranDate){
	
	try{
		var date = tranDate.split('/');
		var day = date[0];
		var month = date[1];
		var year = date[2];
		var tran_Date = month+'/'+day+'/'+year;
		return tran_Date;
	}
	catch(e){
		nlapiLogExecution('ERROR', 'Date Splitting Error', e);
		throw e;
	}
}

function getEmployeeDetail(employeeId , T_date) {
	try {
		//var employeeRec = nlapiLoadRecord('employee', employeeId);
		var employeeDetails = {};
		var exchangeRateTable ={};
		var json = {};
		
		var C_date = nlapiStringToDate(T_date);
		
		//Get Subsidiary
		var employeeLookup = nlapiLookupField('employee',employeeId,['subsidiary'],true);
		var subsidiary_E = employeeLookup.subsidiary;
		nlapiLogExecution('Debug','Subsidiary ->', subsidiary_E);
		var is_Subsidiary_US = '';

			
		
		var employeeLookup_S  = nlapiLookupField('employee',employeeId,['subsidiary','approver']); 
		var subsidiary_V = employeeLookup_S.subsidiary;
		
		if(subsidiary_V == parseInt(3) || subsidiary_V == parseInt(18))
		{
			is_Subsidiary_US = false;
		}
		else
		{
			is_Subsidiary_US = true;
		}
		
		//Get Approver
		if(employeeId){
			var emp_lookUp = nlapiLookupField('employee',employeeId,['approver'],true);
			var first_Approver = emp_lookUp.approver;
			nlapiLogExecution('debug', 'first_Approver', first_Approver);
			var exp_approver_email = nlapiLookupField('employee',employeeLookup_S.approver,'email');
			nlapiLogExecution('debug', 'exp_approver_email', exp_approver_email);
			}
		
		var filters = new Array();
		filters[filters.length] = new nlobjSearchFilter('internalid', 'employee', 'anyof', employeeId);
		var current_date = nlapiStringToDate(nlapiDateToString(new Date()));
		nlapiLogExecution('DEBUG', 'getEmployeeDetail', 'current_date...' + current_date)
		filters[filters.length] = new nlobjSearchFilter('enddate', null, 'onorafter', 'ninetydaysago');
		//filters[filters.length] = new nlobjSearchFilter('startdate', null, 'onorbefore', current_date);

		var returncols = new Array();
		returncols[0] = new nlobjSearchColumn('company');
		returncols[1] = new nlobjSearchColumn('jobtype','job');
	    returncols[2] = new nlobjSearchColumn('enddate').setSort(true);
		returncols[3] = new nlobjSearchColumn("formulatext").setFormula("case when TO_DATE({enddate})<TO_DATE((sysdate - 59)) then 1 else 0 end");
		
		var _date = new Date();
		nlapiLogExecution('DEBUG', 'getEmployeeDetail', '2 Before Search...');
		nlapiLogExecution('DEBUG', 'CurrentDate --->',_date);
		
		//_date = dateSplit(_date);
		//var curr_date = nlapiStringToDate(_date)
		//nlapiLogExecution('DEBUG', 'CurrDate --->',curr_date);
		var employeeAllocatedProjects = nlapiSearchRecord('resourceallocation', null, filters, returncols);
		employeeDetails.projects = employeeAllocatedProjects;
		if(!_logValidation(employeeDetails.projects))
		{
			throw ('Currently you are not allocated to any project. Kindly contact your Manager/business.ops@brillio.com');
		}

	

		//employeeDetails.projects = employeeAllocatedProjects;
		
		//Get Expense Categories
		var expCatFilter =  new Array();;
		expCatFilter[0]= new nlobjSearchFilter('isinactive', null, 'is', 'F');
		expCatFilter[1]= new nlobjSearchFilter('custrecord_exp_category_applicable_emp', null, 'anyof', 1);
		expCatFilter[2]= new nlobjSearchFilter('subsidiary', null, 'anyof', subsidiary_V);
		var expCatColumns = new Array();
		expCatColumns[0] = new nlobjSearchColumn('name');
		var expCategories = nlapiSearchRecord('expensecategory', null, expCatFilter, expCatColumns);
		employeeDetails.expenseCategories = expCategories;
		//employeeDetails.firstLevelApprover = first_Approver;
		/*if(parseInt(subsidiary_V) == parseInt(3) || parseInt(subsidiary_V) == parseInt(19))
		{ //BTPL
		var inr = nlapiExchangeRate('USD', 'INR', T_date);
		 exchangeRateTable = {
			   // INR : nlapiExchangeRate('INR', 'USD', C_date),
			    USD : nlapiExchangeRate('USD', 'INR', T_date),
			    GBP : nlapiExchangeRate('GBP', 'INR', T_date),
			    EUR : nlapiExchangeRate('EUR', 'INR', T_date),
			    SGD : nlapiExchangeRate('SGD', 'INR', T_date),
			    INR: nlapiExchangeRate('INR', 'INR', T_date),
			    Peso : nlapiExchangeRate('PHP', 'INR', T_date),
				CAD : nlapiExchangeRate('CAD', 'INR', T_date),
				RON : nlapiExchangeRate('RON', 'INR', T_date)
			};
		}
		var a_currencyList = ['USD','GBP','EUR','SGD','INR','Peso','CAD','RON'];
		if(parseInt(subsidiary_V) == parseInt(2) || parseInt(subsidiary_V) == parseInt(18))
		{   //BLLC
			var inr = nlapiExchangeRate('INR', 'USD', T_date);
			 exchangeRateTable = {
				   // INR : nlapiExchangeRate('INR', 'USD', C_date),
				    USD : nlapiExchangeRate('USD', 'USD', T_date),
				    GBP : nlapiExchangeRate('GBP', 'USD', T_date),
				    EUR : nlapiExchangeRate('EUR', 'USD', T_date),
				    SGD : nlapiExchangeRate('SGD', 'USD', T_date),
				    INR: nlapiExchangeRate('INR', 'USD', T_date),
				    Peso : nlapiExchangeRate('PHP', 'USD', T_date),
					CAD : nlapiExchangeRate('CAD', 'USD', T_date),
					RON : nlapiExchangeRate('RON', 'USD', T_date)
				};
		}
		//In case of UK
		if(parseInt(subsidiary_V) == parseInt(7))
		{   //BLLC
			var inr = nlapiExchangeRate('INR', 'GBP', T_date);
			 exchangeRateTable = {
				    INR : nlapiExchangeRate('INR', 'GBP', T_date),
				    USD : nlapiExchangeRate('USD', 'GBP', T_date),
				    GBP : nlapiExchangeRate('GBP', 'GBP', T_date),
				    EUR : nlapiExchangeRate('EUR', 'GBP', T_date),
				    SGD : nlapiExchangeRate('SGD', 'GBP', T_date),
				    Peso : nlapiExchangeRate('PHP', 'GBP', T_date),
					CAD : nlapiExchangeRate('CAD', 'GBP', T_date),
					RON : nlapiExchangeRate('RON', 'GBP', T_date)
				};
		}
		//Canada
		//In case of UK
		if(parseInt(subsidiary_V) == parseInt(10))
		{   //BLLC
			var inr = nlapiExchangeRate('INR', 'CAD', T_date);
			 exchangeRateTable = {
				    INR : nlapiExchangeRate('INR', 'CAD', T_date),
				    USD : nlapiExchangeRate('USD', 'CAD', T_date),
				    GBP : nlapiExchangeRate('GBP', 'CAD', T_date),
				    EUR : nlapiExchangeRate('EUR', 'CAD', T_date),
				    SGD : nlapiExchangeRate('SGD', 'CAD', T_date),
				    Peso : nlapiExchangeRate('PHP', 'CAD', T_date),
					CAD : nlapiExchangeRate('CAD', 'CAD', T_date),
					RON : nlapiExchangeRate('RON', 'CAD', T_date)
				};
		}
		if(parseInt(subsidiary_V) == parseInt(12)) /// Cognitek Srl
		{   //BLLC
			
			 exchangeRateTable = {
				    INR : nlapiExchangeRate('INR', 'RON', T_date),
				    USD : nlapiExchangeRate('USD', 'RON', T_date),
				    GBP : nlapiExchangeRate('GBP', 'RON', T_date),
				    EUR : nlapiExchangeRate('EUR', 'RON', T_date),
				    SGD : nlapiExchangeRate('SGD', 'RON', T_date),
				    Peso : nlapiExchangeRate('PHP', 'RON', T_date),
					CAD : nlapiExchangeRate('CAD', 'RON', T_date),
					RON : nlapiExchangeRate('RON', 'RON', T_date)
				};
		}*/
		
					/**  New added Logic ***/	 //added by shravan on 3 Aug 2021 
		var arr_All_Currencies = [];
		nlapiLogExecution('debug', 'subsidiary_V==',subsidiary_V);
		var s_Base_Currency = null;
		s_Base_Currency = nlapiLookupField('subsidiary',subsidiary_V , 'currency',true)
		nlapiLogExecution('debug', 's_Base_Currency==',s_Base_Currency);
		var arr_Exchange_Rates = [];
		var obj_Currency_Search = nlapiSearchRecord("currency",null,
				[
				   ["isinactive","is","F"]
				], 
				[
				   new nlobjSearchColumn("name").setSort(false), 
				   new nlobjSearchColumn("symbol"), 
				   new nlobjSearchColumn("exchangerate")
				]
				); 
		if(obj_Currency_Search)
			{
				for(var i_Index_Currency = 0 ; i_Index_Currency < obj_Currency_Search.length ; i_Index_Currency++)
					{
						var s_Currency_Symbol = obj_Currency_Search[i_Index_Currency].getValue('symbol')
						arr_All_Currencies.push(s_Currency_Symbol);
						if(s_Base_Currency)
							{
								var s_Exchange_Rate = s_Currency_Symbol + ' ' +  ':' + ' ' + nlapiExchangeRate(s_Currency_Symbol, s_Base_Currency , T_date)
								arr_Exchange_Rates.push(s_Exchange_Rate)
							} ////if(s_Base_Currency)
						
					} //// for(var i_Index_Currency = 0 ; i_Index_Currency <= obj_Currency_Search.length ; i_Index_Currency++)
			} ///if(obj_Currency_Search)
			/**  New added Logic ***/	
		nlapiLogExecution('debug', 'arr_All_Currencies==', JSON.stringify(arr_All_Currencies));
		nlapiLogExecution('debug', 'arr_Exchange_Rates==', JSON.stringify(arr_Exchange_Rates));
		
		
			json ={
					Details:employeeDetails,
					FirstLevelApprover: first_Approver,
					expenseApprover:  _logValidation(exp_approver_email) == false ? ' ' : exp_approver_email.split('@')[0],
					Subsidiary: subsidiary_E,
					Subsidiary_ID: subsidiary_V,
					Is_Subsidiary: is_Subsidiary_US,
					Base_Currency :  s_Base_Currency,
					Currency: arr_All_Currencies,
					isBillableCheck: false,
					ExchangeRate: arr_Exchange_Rates
					
			};
			
		nlapiLogExecution('debug','Response',JSON.stringify(json));
		return json;
	
	} catch (err) {
		nlapiLogExecution('error', 'getExpenseDetail', err);
		throw err;
	}
}

function _logValidation(value) {
    if (value != '- None -' && value != null && value.toString() != null && value != '' && value != undefined && value.toString() != undefined && value != 'undefined' && value.toString() != 'undefined' && value.toString() != 'NaN' && value != NaN) {
        return true;
    } else {
        return false;
    }
}