



function rm_mail(){
	
	//custentity_reportingmanager
	var context = nlapiGetContext();
	
	// list of all active employees
	var all_employee_list = [];
	searchRecord(
	        'employee',
	        null,
	       [ new nlobjSearchFilter('custentity_employee_inactive', null, 'is',
	                'F'),new nlobjSearchFilter('custentity_implementationteam', null, 'is',
	                'F')],null).forEach(function(employee) {

		all_employee_list.push(employee.getId());
	});
	
	var all_reporting_manager_list = []; 
	  //new nlobjSearchFilter('isinactive', null, 'is','F'),
	                              
	searchRecord(
	        'employee',
	        null,
	       [ new nlobjSearchFilter('custentity_employee_inactive', null, 'is',
	                'F'),new nlobjSearchFilter('custentity_implementationteam', null, 'is',
	                'F')],[ new nlobjSearchColumn('custentity_reportingmanager')] ).forEach(function(employee){
						
                if(all_reporting_manager_list.indexOf(employee.getValue('custentity_reportingmanager')) == -1){
						all_reporting_manager_list.push(employee.getValue('custentity_reportingmanager'));
				}
	});
	
	
	var active_reporting_manager_list = [];
	
	for(var i=0;i<all_employee_list.length;i++){
		
		if(all_employee_list.indexOf(all_reporting_manager_list[i]) != -1){
						active_reporting_manager_list.push(all_reporting_manager_list[i]);
				}
		
	}
	
	//active_reporting_manager_list = [3165,139683,1807]
	
	
	for(var i=0;i<active_reporting_manager_list.length;i++){
		
		yieldScript(context);
		
		nlapiLogExecution('Debug','reporting_manager -> '+i + ' -> ',active_reporting_manager_list[i]);
		
		  var employeeSearch = nlapiSearchRecord("employee",null,
														[
														//   ["isinactive","is","F"], 
														 //  "AND", 
														   ["custentity_employee_inactive","is","F"], 
														   "AND", 
														   ["custentity_implementationteam","is","F"], 
														   "AND", 
														    ["custentity_employeetype","anyof","1"], 
														   "AND",
														   ["custentity_actual_hire_date","notafter","09/30/2019"],
														   "AND",
														   ["custentity_reportingmanager","anyof",active_reporting_manager_list[i]]
														], 
														[
														   new nlobjSearchColumn("entityid"), 
														   new nlobjSearchColumn("custentity_actual_hire_date"), 
														   new nlobjSearchColumn("department"), 
														   new nlobjSearchColumn("location"),
														   new nlobjSearchColumn("subsidiary"), 
														   new nlobjSearchColumn("employeestatus")									   
														   
														]
														);
																

			var str = [];
			var strVar = '';	
			var n =1;								
			if(employeeSearch!=null){

							for(var j=0;j<employeeSearch.length;j++){
								
								var emp = employeeSearch[j].getValue("entityid");
								
								var actual_date = employeeSearch[j].getValue("custentity_actual_hire_date");
								
								var practice = employeeSearch[j].getText("department");
								
								var level =  employeeSearch[j].getText("employeestatus");
								
							//	var subs  =  employeeSearch[j].getText("subsidiary");
								
								var loc = employeeSearch[j].getText("location");


												strVar += "<tr>";
												strVar += "<td>" + n + "</td>";
												strVar += "<td>" + emp+"</td>";
												strVar += "<td>" + actual_date +"</td>";
												strVar += "<td>" + practice +"</td>";
												strVar += "<td>" + level+"</td>";
										//		strVar += "<td>" + subs+"</td>";
												strVar += "<td>" + loc +"</td>";								
												strVar += "</tr>";	
												
												
												str.push(strVar);   
												strVar = "";	
												
									 n++;
							}
							
							
						
						var subject ='Review your Reportee list ';	
												
														var reporting_manager_emp_detail = '';
														
														for(var l=0; l<str.length; l++)
														{
															reporting_manager_emp_detail += str[l];
														}
														
														strVar += "<table border=\"1\"  align=\"center\" width=\"100%\">"; 
														strVar += "<tr>";
														strVar += "<th border-bottom=\"0.3\" width=\"5%\" scope='col'><b>S.No.</b></th>";
														strVar += "<th border-bottom=\"0.3\" width=\"10%\" scope='col'><b>Employee Name</b></th>";
														strVar += "<th border-bottom=\"0.3\" width=\"10%\" scope='col'><b>Hire Date<b/></th>";											
														strVar += "<th border-bottom=\"0.3\" width=\"10%\" scope='col'><b>Practice</b></th>";
														strVar += "<th border-bottom=\"0.3\" width=\"5%\" scope='col'><b>Level</b></th>";
													//	strVar += "<th border-bottom=\"0.3\" width=\"20%\" scope='col'><b>Entity</b></th>";
														strVar += "<th border-bottom=\"0.3\" width=\"10%\" scope='col'><b>Location</b></th>";											
														strVar += "</tr>";	
														strVar += reporting_manager_emp_detail;
														strVar += "</table>";
														
														var reporting_manager_name = nlapiLookupField('employee', active_reporting_manager_list[i], 'entityid');
														
														   var reporting_manager_full_name = reporting_manager_name.split('-')[1];
														   
														   if(reporting_manager_full_name == undefined || reporting_manager_full_name == null)
															{
																var reporting_manager_full_name = reporting_manager_name.split('-')[0];
															}
														
														var reporting_manager_mail = nlapiLookupField('employee', active_reporting_manager_list[i], 'email');
														
														var date = new Date();
														var year = date.getFullYear();
														
														var body = 'Dear ' + reporting_manager_full_name + ', <br /><br />' +  'As we approach the end of 2019, we would like you to review your reportee list: <br /><br />'+ strVar + '<br />We would be initiating Performance Development process for the above mentioned employees in OnTheGo App by early January 2020.<br /> <br />If you wish to nominate a different manager as their performance manager, you may initiate reporting changes in Fusion. Kindly refer to the Manager self-service guide(Attached) for the same.<br /><br /><b>Will appreciate if you make these changes before 20-Dec-2019</b><br /><br /><b>Please Note: Full time employees who have joined Brillio on or before 30-Sept-2019 will be a part of Performance development 2019 </b> <br /><br />For any Queries, Log a request in BRIA under “Performance Management”  <br /><br /> Regards,<br /> Brillio HR'; 
													
													var attachment = nlapiLoadFile(2185558);
													
													 nlapiSendEmail(442, reporting_manager_mail , subject,body, null, null, null, attachment);
													
													 nlapiLogExecution('Debug','Email Sent to ->',reporting_manager_mail);
														
													//'prabhat.gupta@brillio.com'
														strVar = '';
														str =[];
													/*	
														if(i==7){
															break;
														}
						*/
																		
			}
	
	}
}




function yieldScript(currentContext) {

	if (currentContext.getRemainingUsage() <= 100) {
		nlapiLogExecution('AUDIT', 'API Limit Exceeded');
		var state = nlapiYieldScript();

		if (state.status == "FAILURE") {
			nlapiLogExecution('ERROR', 'Yield Script Failed', 'Reason : '
			        + state.reason + ' / Size : ' + state.size);
			return false;
		} else if (state.status == "RESUME") {
			nlapiLogExecution('AUDIT', 'Script Resumed');
		}
	}
}