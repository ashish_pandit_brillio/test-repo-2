function suiteletFunction(request, response)
{
	try
	{
		var i_recordID =  request.getParameter('fp_revrec_rcrd_id');
		i_recordID = i_recordID.trim();
		
		var b_data_validated =  request.getParameter('data_validated');
		b_data_validated = b_data_validated.trim();
		nlapiLogExecution('audit','rcrd id:- '+i_recordID,'b_data_validated:- '+b_data_validated);
		
		if(b_data_validated == 'T')
		{
			var param_je=new Array();
			param_je['custscript_rev_rec_id'] = i_recordID;
			nlapiLogExecution('DEBUG','record_id',i_recordID);
			var status = nlapiScheduleScript('customscript_fp_revrec_createje_resource','customdeploy_fp_revrec_createje_resource',param_je);
		}
     else if(b_data_validated == 'data')
		{
			var param_va=new Array();
			param_va['custscript_validate_recrd_id'] = i_recordID;
			nlapiLogExecution('DEBUG','record_id',i_recordID);
			var status = nlapiScheduleScript('customscript_export_data_to_validate','customdeploy_export_data_validate',param_va);
		}
		else
		{
			var params=new Array();
			params['custscript_fp_rev_rec_rcrd_id'] = i_recordID;
			
			var status = nlapiScheduleScript('customscript_fp_revrec_validate_data','customdeploy_fp_revrec_validate_data',params);
		}
		
		var o_fp_rev_rec = nlapiLoadRecord('customrecord_fp_rev_rec_je_creation',parseInt(i_recordID));
		o_fp_rev_rec.setFieldValue('custrecord_status_fp_rev_rce',3);
		nlapiSubmitRecord(o_fp_rev_rec,true,true);
		
		nlapiSetRedirectURL('RECORD', 'customrecord_fp_rev_rec_je_creation', i_recordID, null,null);
	}
	catch(exception)
	{
	  nlapiLogExecution('ERROR', 'suiteletFunction','ERROR:- '+exception);	
	}
}