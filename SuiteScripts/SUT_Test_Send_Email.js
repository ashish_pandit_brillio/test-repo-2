/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.https://system.na1.netsuite.com/app/common/scripting/script.nl?id=557#00       15 May 2015     amol.sahijwani
 *
 */

/**
 * @param {nlobjRequest} request Request object
 * @param {nlobjResponse} response Response object
 * @returns {Void} Any output is written via response object
 */
	
function getBirthday()
 {
var searchresults = nlapiSearchRecord('employee', 'customsearch_birthdaymailer', null, null);
var emailAddress = '';
var  ReportingManagerEmail='';
	for ( var i = 0; searchresults != null && i < searchresults.length; i++ ) 
	{
		var searchresult = searchresults[ i ];
                emailAddress  = searchresult.getValue( 'email' );
                ReportingManagerEmail= searchresult.getValue('email','custentity_reportingmanager',null);
                suitelet(request, response);
        }
}
function suitelet(request, response){
	var mailData = timeSheetNotificationMailTemplate('Anuradha');
	try
	{
response.write(mailData.MailBody);
		nlapiSendEmail('10730', 'anuradha.sinha@brillio.com', mailData.MailSubject, mailData.MailBody,null,
							null, {entity : 2673});
response.write('Email Sent');
	}
	catch(e)
	{
                response.write('Error: ' + e.message);
		nlapiLogExecution('ERROR', 'Error: ', e.message);
	}
}

function timeSheetNotificationMailTemplate(firstName) {
    var htmltext = '';
    
    htmltext += '<table border="0" width="100%"><tr>';
    htmltext += '<td colspan="4" valign="top">';
    //htmltext += '<p>Hi ' + firstName + ',</p>';  
htmltext += '<p>Dear ' + firstName + ',</p>';
 
htmltext += '<p>MANY HAPPY RETURNS OF THE DAY!</p>';
 
htmltext += '<p>Wishing you a day filled with happiness and a life filled with joy</p>';
htmltext += '<p><img src="https://system.na1.netsuite.com/core/media/media.nl?id=23876&c=3883006&h=b4c21d3880ddf62470ed" alt="Right click to download image." /></p>';
htmltext += '<p>With Best Wishes,</p>';
htmltext += '<p>HR & MANAGEMENT TEAM @ BRILLIO</p>';
 
 
    htmltext += '</td></tr>';
    htmltext += '</table>';
    htmltext += '<hr width="100%" size="1" noshade color="#CCCCCC">';
    htmltext += '<table width="100%" border="0" cellspacing="0" cellpadding="0">';
    htmltext += '<tr>';
    htmltext += '<td align="right">';
    htmltext += '<font style="font-size:9px; font-family:Verdana,Arial,Helvetica,sans-serif; color:#999999;">Brillio is powered by <a href="http://www.netsuite.com/" style="color:#999999;">NetSuite</a> One System. No Limits.</font>';
    htmltext += '</td>';
    htmltext += '</tr>';
    htmltext += '</table>';
    htmltext += '</body>';
    htmltext += '</html>';
 
    return {
        MailBody : htmltext,
        MailSubject : "Happy Birthday"
      
    };
}
