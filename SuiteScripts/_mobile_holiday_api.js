/**
 * @NApiVersion 2.x
 * @NScriptType Restlet
 * @NModuleScope SameAccount
 */
 define(['N/record', 'N/search', 'N/runtime'],
 function (obj_Record, obj_Search, obj_Runtime) {
     function Send_Holiday_data(obj_Request) {

         try {
             log.debug('requestBody ==', JSON.stringify(obj_Request));
             var obj_Usage_Context = obj_Runtime.getCurrentScript();
             var s_Request_Type = obj_Request.RequestType;
             log.debug('s_Request_Type ==', s_Request_Type);
             var s_Request_Data = obj_Request.RequestData;
             log.debug('s_Request_Data ==', s_Request_Data);
             var s_Request_Date = obj_Request.RequestDate;
             log.debug('s_Request_Date ==', s_Request_Date);
             var obj_Response_Return = {};
             if (s_Request_Data == 'MOBILEHOLIDAY') {
                 log.debug({
                     title: 's_Request_Data at line 29',
                     details: s_Request_Data
                 })
                 obj_Response_Return = get_Holiday_Data(obj_Search,s_Request_Date);

             } //// if(s_Request_Data == 'Employee')


         } /// End of try
         catch (s_Exception) {
             log.debug('s_Exception==', s_Exception);
         } //// 
         log.debug('obj_Response_Return==', JSON.stringify(obj_Response_Return));
         log.debug('Final Remaining Usage  ==', obj_Usage_Context.getRemainingUsage());
         return obj_Response_Return;
     } ///// Function Send_Holiday_data


     return {
         post: Send_Holiday_data
     };

 }); //// End of Main Function
/****************** Supporting Functions ***********************************/
function get_Holiday_Data(obj_Search,s_Request_Date) {
 try {
     var customrecord_mobile_holiday_calendarSearchObj = obj_Search.create({
        type: "customrecord_mobile_holiday_calendar",
        filters:
        [
           ["custrecord_mhc_date","onorafter",s_Request_Date]
        ],
        columns:
        [
           obj_Search.createColumn({name: "custrecord_mhc_date", label: "Date"}),
           obj_Search.createColumn({name: "custrecord_mhc_title", label: "Title"}),
           obj_Search.createColumn({name: "custrecord_mhc_location", label: "Location"})
        ]
     });
     var searchResultCount = customrecord_mobile_holiday_calendarSearchObj.runPaged().count;
     log.debug("customrecord_mobile_holiday_calendarSearchObj result count",searchResultCount);
     var myResults = getAllResults(customrecord_mobile_holiday_calendarSearchObj);
     var arr_Holiday_json = [];
     myResults.forEach(function (result) {
         var obj_json_Container = {}
         obj_json_Container = {
             'Internal_ID': result.id,
             'Name':result.getValue({name: "custrecord_mhc_title"}),
             'Date':result.getValue({name: "custrecord_mhc_date"}),
             'Location':result.getValue({name: "custrecord_mhc_location"}),
         };
         arr_Holiday_json.push(obj_json_Container);
         return true;
     });
     return arr_Holiday_json;
 } //// End of try
 catch (s_Exception) {
     log.debug('get_Holiday_Data s_Exception== ', s_Exception);
 } //// End of catch 
} ///// End of function get_Holiday_Data()


function getAllResults(s) {
 var result = s.run();
 var searchResults = [];
 var searchid = 0;
 do {
     var resultslice = result.getRange({ start: searchid, end: searchid + 1000 });
     resultslice.forEach(function (slice) {
         searchResults.push(slice);
         searchid++;
     }
     );
 } while (resultslice.length >= 1000);
 return searchResults;
}