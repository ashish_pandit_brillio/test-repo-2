/**
 *@NApiVersion 2.0
 *@NScriptType UserEventScript
 *@NModuleScope SameAccount
 */

// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
    /*
       	Script Name: UES_ADO_Validation
    	Author: Praveena Madem
    	Company: Inspirria Cloud Tech
    	Date: 06/04/2021
		Description : Script is used vaidate the ADO records should have unique customers and populate the account deleivery manager on the customer master when user selects the Account deleivery manager on ADO record.
    	Script Modification Log:

    	-- Date --			-- Modified By --				--Requested By--				-- Description --
    	



    Below is a summary of the process controls enforced by this script file.  The control logic is described
    more fully, below, in the appropriate function headers and code blocks.


         UESEREVENT FUNCTION
    		- beforeSubmit()
    		


         SUB-FUNCTIONS
    		- The following sub-functions are called by the above core functions in order to maintain code
                modularization:

                   -NA
    			   

    */
}
// END SCRIPT DESCRIPTION BLOCK  ====================================


define(['N/ui/dialog', 'N/runtime', 'N/record', 'N/log', 'N/error', 'N/search', 'N/format'],
    function(dialog, runtime, record, log, error, search, format) {

        function beforeSubmit(scriptContext) {
            var cRecord = scriptContext.newRecord
            var action_type = scriptContext.type;
            var filters = [];
            var i_customer = cRecord.getValue({
                fieldId: "custrecord_ado_customer"
            });
            log.debug('i_customer', i_customer);
            if (action_type == 'create') {
                filters = [
                    ["custrecord_ado_customer", "anyof", i_customer]
                ]
            }
            if (action_type == 'edit' || action_type == 'xedit') {
                log.debug('cRecord.id', cRecord.id);

                filters = [
                    ["custrecord_ado_customer", "anyof", i_customer],
                    "AND",
                    ["internalid", "noneof", cRecord.id]
                ]
            }

            if (filters) {
                var customrecord_adoSearchObj = search.create({
                    type: "customrecord_ado",
                    filters: filters,
                    columns: []

                });
                var searchResultCount = customrecord_adoSearchObj.runPaged().count;
                log.debug("customrecord_adoSearchObj result count", searchResultCount);

                if (searchResultCount > 0) {
                    throw "Duplicate ADO record with customer";
                    return false;
                } else {
                    record.submitFields({
                        "type": record.Type.CUSTOMER,
                        "id": i_customer,
                        "values": {
                            "custentity_account_delivery_owner": cRecord.getValue({
                                fieldId: "custrecord_account_delivery_owner"
                            })
                        }
                    });
                }


            }


            return true;




        }



        return {

            beforeSubmit: beforeSubmit
        };

    });