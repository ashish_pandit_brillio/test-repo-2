function Time_Before(type, form)
{
	var Collabera_Cust = 'F', Brillio_Cust = 'F';
	var cols = new Array();
	cols[cols.length] = new nlobjSearchColumn('customer');
	cols[cols.length] = new nlobjSearchColumn('company');
	
	var filters = new Array();
	filters[filters.length] = new nlobjSearchFilter('resource', null, 'anyof', nlapiGetContext().getUser());
	
	var searchResult = nlapiSearchRecord('resourceallocation', null, filters, cols);
	if(searchResult != null)
	{
		for (var i=0; i < searchResult.length; i++)
		{
			if(searchResult[i].getValue('customer') == 4223)
			{
				Collabera_Cust = 'T';
			}
			else
			{
				Brillio_Cust = 'T';
			}
		}
	}

	//Restrict based on the flag;
	if(Collabera_Cust == 'T' && Brillio_Cust == 'F')
	{
		var e1 = nlapiCreateError('Aborting Save', 'You do not have access to this functionality. We will keep you notified when you need to enter timsheet in NetSuite.');
		throw e1.getDetails();
	}
	
	if(form.getSubList('timeitem') != null)
	{
		form.setScript('customscript_timesheet_restrict');
		form.addField('custpage_tslbl', 'label', '<b>Note - For Customer projects, standard hours cannot exceed 40 hrs(excluding OT hrs) per week<b>');
		
		var IsInternal = form.addField('custpage_internalproj', 'checkbox', 'Is Internal');
		IsInternal.setDefaultValue('F');
		IsInternal.setDisplayType('hidden');

		var IsExternal = form.addField('custpage_externalproj', 'checkbox', 'Is External');
		IsExternal.setDefaultValue('F');
		IsExternal.setDisplayType('hidden');
		
		var ClientExecuted = form.addField('custpage_clientexecuted', 'checkbox', 'Client Executed');
		ClientExecuted.setDefaultValue('F');
		ClientExecuted.setDisplayType('hidden');

		var FH_Availed = form.addField('custpage_fhavailed', 'checkbox', 'FH Availed');
		FH_Availed.setDefaultValue('F');
		//FH_Availed.setDisplayType('hidden');

		var FH_DateAvailed = form.addField('custpage_fhdateavailed', 'date', 'FH Date Availed');
		//FH_Availed.setDisplayType('hidden');
		
		var TimeTab = form.getSubList('timeitem');
		if(TimeTab.getField('rate') != null)
		{
			TimeTab.getField('rate').setDisplayType('hidden');
			TimeTab.getField('price').setDisplayType('hidden');
		}
	}
}

function Error_NoticePage(request, response)
{
	var Error_Message = 'You do not have access to this functionality. We will keep you notified when you need to enter timsheet in NetSuite.';
	
	var redirecttype = 'history.back()';
	var NoticeError = '<html><link rel="stylesheet" href="/core/styles/pagestyles.nl?ct=-142&bglt=EEEEEE&bgmd=FFEBC2&bgdk=787878&bgon=211F5E&bgoff=FFCC66&bgbar=211F5E&tasktitletext=FFCC66&crumbtext=FFD88A&headertext=B39163&ontab=FFFFFF&offtab=000033&text=000000&link=000000&bgbody=FFFFFF&bghead=FFFFFF&portlet=D3D2DF&portletlabel=000000&bgbutton=FFE599&bgrequiredfld=FFFFE5&font=Verdana%2CHelvetica%2Csans-serif&size_site_content=8pt&size_site_title=8pt&size=1.0&nlinputstyles=T&NS_VER=2007.0.5">' +
								'<body bgcolor="#FFFFFF" link="#000000" vlink="#000000" alink="#330099" text="#000000" topmargin=0 marginheight=1>' +
								'<img src="/images/nav/stretch.gif" width="10">' +
								'<img src="/images/logos/netsuite30.gif" border=0>' +
								'<TABLE border=0 cellPadding=0 cellSpacing=0 width=100%>' +
									'<tr><td class=bglt>' +
										'<table border=0 cellspacing=0 cellpadding=5 width=100%>' +
											'<tr><td class=textboldnolink>Notice</td></tr>' +
											'<tr><td vAlign=top><table border=0 cellspacing=0 cellpadding=0 width=100%>' +
												'<TR><TD class=text>&nbsp;</TD></TR>' + 
												'<tr><td class=text><img src="/images/5square.gif" width=5 height=5>\t' + Error_Message + '</td></tr>' +
												'<TR><TD class=text>&nbsp;</TD></TR>' +
												'<TR><TD class=text>&nbsp;</TD></TR>' + 
												'</table></td>' +
											'</tr>' +
										'</TABLE></TD>' +
									'<tr><TD style="" >' +
										'<table id="tbl_goback" cellpadding=0 cellspacing=0 border=0 style="cursor:hand;">' +
											'<TR><TD class=text>&nbsp;</TD></TR>' +
											'<tr>' +
												'<td nowrap class="rndbuttoncaps" background="/images/buttons/upper_left_cap.gif"><img src="/images/nav/stretch.gif" border=0 width=4></td>' +
													'<TD height=20 valign="bottom" nowrap class="rndbuttonbody" background="/images/buttons/upper_body.gif" style="padding-top:2">' +
													'<INPUT type="button" style="vertical-align:middle; " class="rndbuttoninpt" value="Go Back" id="goback" name="goback" onclick="' + redirecttype + ';return false;" ></TD>' +
													'<td nowrap class="rndbuttoncaps" background="/images/buttons/upper_right_cap.gif"><img src="/images/nav/stretch.gif" border=0 width=4></td>' +
											'</tr>' + 
										'</table></TD>' +
									'</tr>' +
								'</TABLE></HTML>';
	response.write(NoticeError); 
}

function ValidateLine(type)
{
	if(type == 'timeitem')
	{
		var TotalTime = 0;
		
		//Update timeentries in the line to blank if entered/updated as 0
		for(var i=0; i<7; i++)
		{
			if(parseInt(nlapiGetCurrentLineItemValue('timeitem', 'hours'+i)) == 0)
			{
				nlapiSetCurrentLineItemValue('timeitem', 'hours'+i, ''); 
				nlapiLogExecution('DEBUG', 'LOG', 'terminate hrs : '+parseInt(nlapiGetCurrentLineItemValue('timeitem', 'hours'+i)));
			}
		}
		
		nlapiSetCurrentLineItemValue('timeitem', 'custcol_projecttask', nlapiGetCurrentLineItemText('timeitem', 'casetaskevent')); 
		
		var ProjType = nlapiGetCurrentLineItemValue('timeitem', 'custcol_projecttype');
		if(ProjType == 'External')
		{
			var ProjTask = nlapiGetCurrentLineItemText('timeitem', 'casetaskevent');
			if(ProjTask == 'Floating Holiday (Project Task)')
			{
				//Check if entered more than one column
				var TranYear = new Date(nlapiStringToDate(nlapiGetFieldValue('trandate')));
				var FH_Exists = 'F', FH_Break = 'F';
				for(var i=0; i<7; i++)
				{
					if(parseInt(nlapiGetCurrentLineItemValue('timeitem', 'hours'+i)) > 0)
					{
						if(FH_Exists == 'F')
						{
							FH_Exists = 'T';
							var month, day, year;

							var dt = new Date(nlapiGetFieldValue('trandate'));
							var lastdt= nlapiAddDays(dt, i); 							

							FH_Date = lastdt;
							month = parseInt(lastdt.getMonth() + 1);
							day = lastdt.getDate();
							year = lastdt.getFullYear();
							
							FH_Date = month + '/' + day + '/' + year;

							nlapiSetFieldValue('custpage_fhavailed', 'T');
							nlapiSetFieldValue('custpage_fhdateavailed', FH_Date);
						}
						else
						{
							FH_Break = 'T';
							nlapiSetFieldValue('custpage_fhavailed', 'F');
							nlapiSetFieldValue('custpage_fhdateavailed', '');
						}
					}
				}

				if(FH_Break == 'T')
				{
					alert('You can avail only one Floating Holiday for the year ' + TranYear.getFullYear());
					nlapiSetFieldValue('custpage_fhavailed', 'F');
					nlapiSetFieldValue('custpage_fhdateavailed', '');
					return false;
				}
				
				var response = nlapiRequestURL(nlapiResolveURL('SUITELET', 'customscript_fetchvalue_timesheet', 'customdeploy_fetchvalue_timesheet') + '&type=FH&trandate=' + TranYear.getFullYear() + '&empid=' + nlapiGetFieldValue('employee') , null, null );
				if(response.getBody() == 'T')
				{
					alert('Floating Holiday for the year ' + TranYear.getFullYear() + ' is already availed.');
					nlapiSetFieldValue('custpage_fhavailed', 'F');
					nlapiSetFieldValue('custpage_fhdateavailed', '');
					return false;
				}
				else if(response.getBody() == -1)
				{
					alert('You are not eligible to avail Floating Holiday.');
					nlapiSetFieldValue('custpage_fhavailed', 'F');
					nlapiSetFieldValue('custpage_fhdateavailed', '');
					return false;
				}
			}

			if(ProjTask != 'OT (Project Task)')
			{
				nlapiSetCurrentLineItemValue('timeitem', 'custcol_projecttask', nlapiGetCurrentLineItemText('timeitem', 'casetaskevent')); 
				for(var i=0; i< 7; i++)
				{
					if(nlapiGetCurrentLineItemValue('timeitem', 'hours' + i) != null && nlapiGetCurrentLineItemValue('timeitem', 'hours' + i).length > 0)
					{
						var TimePerDay = nlapiGetCurrentLineItemValue('timeitem', 'hours' + i).split(':');
						TotalTime += parseInt(TimePerDay[0] * 60) + parseInt(TimePerDay[1]);

						if(parseInt(TimePerDay[0] * 60) + parseInt(TimePerDay[1]) > 600)
						{
							alert('Standard hours cannot exceed 8 hrs per day');
							return false;
						}
					}
				}
				
				if(TotalTime > 2400)
				{
					alert('Standard hours for the week cannot exceed 40 hrs(excluding OT)');
					return false;
				}
			}
			else
			{
				nlapiSetCurrentLineItemValue('timeitem', 'custcol_projecttask', nlapiGetCurrentLineItemText('timeitem', 'casetaskevent')); 
				
			}
			nlapiSetFieldValue('custpage_externalproj', 'T');
		}
		else
		{
			nlapiSetFieldValue('custpage_internalproj', 'T');
		}
	}
	return true;
}

function Timesheet_SaveCheck()
{
	var CurrDate = new Date(nlapiGetFieldValue('trandate'));
	var CutofDate = new Date("June 29, 2014");
	
	if(CurrDate < CutofDate)
	{
		alert('Due to data entry issues, timesheet entry for the month of June 2014 has been blocked');
		return false;
	}

	if(nlapiGetFieldValue('employee') != null && nlapiGetFieldValue('employee').length <= 0)
	{
		alert('Please select Employee');
		return false;
	}
	
	if(nlapiGetFieldValue('customer') != null && nlapiGetFieldValue('customer').length <= 0)
	{
		alert('Please select Project');
		return false;
	}

	if(nlapiGetFieldValue('casetaskevent') != null && nlapiGetFieldValue('casetaskevent').length <= 0)
	{
		alert('Please select Project task');
		return false;
	}
	
	if(nlapiGetFieldValue('custpage_internalproj') == 'T' && nlapiGetFieldValue('custpage_externalproj') == 'T')
	{
		alert('Please submit separate timesheet for Internal and External projects');
		return false;
	}
	
	if(nlapiGetField('weekly') != null && nlapiGetFieldValue('weekly') == 'T')
	{
		var totalTime = 0;
		var OTTime = 0;
		for(var i=0; i < nlapiGetLineItemCount('timeitem'); i++)
		{
			var duration = nlapiGetLineItemValue('timeitem', 'hourstotal', i+1).split(':');
			if(nlapiGetLineItemValue('timeitem', 'custcol_projecttask', i+1) != 'OT (Project Task)')
			{
				totalTime += parseInt(duration[0] * 60) + parseInt(duration[1]);
			}
			else
			{
				OTTime += parseInt(duration[0] * 60) + parseInt(duration[1]);
				
			}
		}

		if(nlapiGetFieldValue('custpage_internalproj') != 'T')
		{
			if(totalTime > 2400)
			{
				alert('Standard hours for the week cannot exceed 40 hrs(excluding OT)');
				return false;
			}
			else
			{
				if(totalTime > 0)
				{
					//Call Suitelet to evaluate the hours filled for week
					var response = nlapiRequestURL(nlapiResolveURL('SUITELET', 'customscript_fetchvalue_timesheet', 'customdeploy_fetchvalue_timesheet') + '&type=W&trandate=' + nlapiGetFieldValue('trandate') + '&employee=' + nlapiGetFieldValue('employee') , null, null );
					if (parseInt(response.getBody()) >= 2400) //Returns ST hrs
					{
						if (OTTime != 0)
						{
							alert('You need to submit OT hours separately for the week as ST hours is already submitted for the week');
							return false;
						}
						else
						{
							alert('You have already submitted  timesheet for the week');
							return false;
						}
					}
					else
					{
						if (totalTime + parseInt(response.getBody()) > 2400)
						{
							if (OTTime != 0)
							{
								alert('You need to submit OT hours separately for the week as ST hours is already submitted for the week');
								return false;
							}
							else
							{
								alert('Standard hours for the week cannot exceed 40 hrs(excluding OT)');
								return false;
							}
						}
					}
				}
			}
		}
	}
	else
	{
		var CustProj = nlapiGetFieldValue('customer');
		var ProjType = nlapiGetFieldValue('custcol_projecttype');
		if(CustProj != null && CustProj.length > 0)
		{
			if(ProjType != null && ProjType.length <= 0)
			{
				var response = nlapiRequestURL(nlapiResolveURL('SUITELET', 'customscript_fetchvalue_timesheet', 'customdeploy_fetchvalue_timesheet') + '&type=P&projid=' + CustProj, null, null );
				nlapiSetFieldValue('custcol_projecttype', response.getBody()); 
			}
		}
		
		if(ProjType == 'External')
		{
			if(nlapiGetFieldValue('hours') != null && nlapiGetFieldValue('hours').length > 0)
			{
				var duration = nlapiGetFieldValue('hours').split(':');
				var totaltime = parseInt(duration[0] * 60) + parseInt(duration[1]);

				if(nlapiGetFieldText('casetaskevent') == 'Floating Holiday (Project Task)')
				{
					var TranYear = new Date(nlapiStringToDate(nlapiGetFieldValue('trandate')));
					var response = nlapiRequestURL(nlapiResolveURL('SUITELET', 'customscript_fetchvalue_timesheet', 'customdeploy_fetchvalue_timesheet') + '&type=FH&trandate=' + TranYear.getFullYear() + '&empid=' + nlapiGetFieldValue('employee') , null, null );
					if(response.getBody() == 'T')
					{
						alert('Floating Holiday for the year ' + TranYear.getFullYear() + ' is already availed.');
						return false;
					}
				}

				if(nlapiGetFieldText('casetaskevent') != 'OT (Project Task)')
				{
					if(totaltime > 600)			
					{
						alert('Standard hours for the day cannot exceed 8 hrs(excluding OT)');
						return false;
					}
					else
					{
						//Call Suitelet to evaluate the hours filled for day
						var response = nlapiRequestURL(nlapiResolveURL('SUITELET', 'customscript_fetchvalue_timesheet', 'customdeploy_fetchvalue_timesheet') + '&type=D&trandate=' + nlapiGetFieldValue('trandate') + '&employee=' + nlapiGetFieldValue('employee') , null, null );
						if (parseInt(response.getBody()) >= 600)
						{
							alert('You have already submitted  timesheet for the selected day');
							return false;
						}
						else
						{
							if (totaltime + parseInt(response.getBody()) > 600)
							{
								alert('Standard hours for the day cannot exceed 8 hrs(excluding OT)');
								return false;
							}
						}
					}
				}
			}
		}
	}
	nlapiSetFieldValue('custpage_clientexecuted', 'T');
	return true;
}


function Time_BeforeSubmit(type)
{
	if(nlapiGetFieldValue('custpage_clientexecuted') == null && nlapiGetFieldValue('custpage_clientexecuted') != 'T')
	{
		//Client side scripting failed...so perform validations
		var InternalProjects = 'F', ExternalProjects = 'F';
		var TotalHours = 0;
		
		var CurrDate = new Date(nlapiGetFieldValue('trandate'));
		var CutofDate = new Date("June 29, 2014");
		
		if(CurrDate < CutofDate)
		{
			var e1 = nlapiCreateError('Aborting Save', 'Due to data entry issues, timesheet entry for the month of June 2014 has been blocked');
			throw e1.getDetails();
		}

		if(nlapiGetField('weekly') != null && nlapiGetFieldValue('weekly') == 'T')
		{
			/*for(var i=0; i < nlapiGetLineItemCount('timeitem'); i++)
			{
				if(nlapiGetLineItemValue('timeitem', 'hours0', i+1) == '0.00')
				{
					nlapiSetLineItemValue('timeitem', 'hours0', i+1, ''); 
				}

				if(nlapiGetLineItemValue('timeitem', 'hours1', i+1) == '0.00')
				{
					nlapiSetLineItemValue('timeitem', 'hours1', i+1, ''); 
				}

				if(nlapiGetLineItemValue('timeitem', 'hours2', i+1) == '0.00')
				{
					nlapiSetLineItemValue('timeitem', 'hours2', i+1, ''); 
				}

				if(nlapiGetLineItemValue('timeitem', 'hours3', i+1) == '0.00')
				{
					nlapiSetLineItemValue('timeitem', 'hours3', i+1, ''); 
				}

				if(nlapiGetLineItemValue('timeitem', 'hours4', i+1) == '0.00')
				{
					nlapiSetLineItemValue('timeitem', 'hours4', i+1, ''); 
				}

				if(nlapiGetLineItemValue('timeitem', 'hours5', i+1) == '0.00')
				{
					nlapiSetLineItemValue('timeitem', 'hours5', i+1, ''); 
				}

				if(nlapiGetLineItemValue('timeitem', 'hours6', i+1) == '0.00')
				{
					nlapiSetLineItemValue('timeitem', 'hours6', i+1, ''); 
				}
			}*/
			
			//Get Project Types
			for(var i=0; i < nlapiGetLineItemCount('timeitem'); i++)
			{
				if(nlapiGetLineItemValue('timeitem', 'hourstotal', i+1) != null)
				{
					var duration = nlapiGetLineItemValue('timeitem', 'hourstotal', i+1).split(':');
					TotalHours += parseInt(duration[0] * 60) + parseInt(duration[1]);
				}

				var CustProj = nlapiGetLineItemValue('timeitem', 'customer',i+1);
				if(CustProj != null && CustProj.length > 0)
				{
					var ProjType = FetchProjectType(CustProj).split(":");
					nlapiSetLineItemValue('timeitem', 'custcol_projecttype', i+1, ProjType[0]); 
					if(ProjType[0] == 'External')
					{
						ExternalProjects = 'T';
					}
					else
					{
						InternalProjects = 'T';
					}
				}
			}

			if(TotalHours != null && TotalHours > 0)
			{
				if(ExternalProjects == 'T' && InternalProjects == 'T')
				{
					var e1 = nlapiCreateError('Aborting Save', 'Please submit separate timesheet for Internal and External projects');
					throw e1.getDetails();
				}

				var totalTime = 0;
				var OTTime = 0;
				for(var i=0; i < nlapiGetLineItemCount('timeitem'); i++)
				{
					var duration = nlapiGetLineItemValue('timeitem', 'hourstotal', i+1).split(':');
					if(nlapiGetLineItemText('timeitem', 'casetaskevent', i+1) != 'OT (Project Task)')
					{
						totalTime += parseInt(duration[0] * 60) + parseInt(duration[1]);
					}
					else
					{
						OTTime += parseInt(duration[0] * 60) + parseInt(duration[1]);
						
					}
				}
				
				if(nlapiGetFieldValue('custpage_internalproj') != 'T')
				{
					if(totalTime > 2400)
					{
						var e1 = nlapiCreateError('Aborting Save', 'Standard hours for the week cannot exceed 40 hrs(excluding OT)');
						throw e1.getDetails();
					}
					else
					{
						if(totalTime > 0)
						{
							//Call Suitelet to evaluate the hours filled for week
							var STHours = FetchTime(nlapiGetFieldValue('trandate'), 0, nlapiGetFieldValue('employee'));
							if (parseInt(STHours) >= 2400) //Returns ST hrs
							{
								if (OTTime != 0)
								{
									var e1 = nlapiCreateError('Aborting Save', 'You need to submit OT hours separately for the week as ST hours is already submitted for the week');
									throw e1.getDetails();
								}
								else
								{
									var e1 = nlapiCreateError('Aborting Save', 'You have already submitted  timesheet for the week');
									throw e1.getDetails();
								}
							}
							else
							{
								if (totalTime + parseInt(STHours) > 2400)
								{
									if (OTTime != 0)
									{
										var e1 = nlapiCreateError('Aborting Save', 'You need to submit OT hours separately for the week as ST hours is already submitted for the week');
										throw e1.getDetails();
									}
									else
									{
										var e1 = nlapiCreateError('Aborting Save', 'Standard hours for the week cannot exceed 40 hrs(excluding OT)');
										throw e1.getDetails();
									}
								}
							}
						}
					}
				}
			}
			else
			{
				var e1 = nlapiCreateError('Aborting Save', 'Zero hours cannot be submitted');
				throw e1.getDetails();
			}
		}
		else
		{
			var CustProj = nlapiGetFieldValue('customer');
			var ProjType = nlapiGetFieldValue('custcol_projecttype');
			if(CustProj != null && CustProj.length > 0)
			{
				if(ProjType != null && ProjType.length <= 0)
				{
					var ProjType = FetchProjectType(CustProj).split(":");
					nlapiSetFieldValue('custcol_projecttype', ProjType[0]); 
				}
			}
			
			if(ProjType == 'External')
			{
				if(nlapiGetFieldValue('hours') != null && nlapiGetFieldValue('hours').length > 0)
				{
					var duration = nlapiGetFieldValue('hours').split(':');
					var totaltime = parseInt(duration[0] * 60) + parseInt(duration[1]);
					
					if(nlapiGetFieldText('casetaskevent') != 'OT (Project Task)')
					{
						if(totaltime > 600)			
						{
							var e1 = nlapiCreateError('Aborting Save', 'Standard hours for the day cannot exceed 8 hrs(excluding OT)');
							throw e1.getDetails();
						}
						else
						{
							//Call Suitelet to evaluate the hours filled for day
							var STHours = FetchTime(nlapiGetFieldValue('trandate'), 1, nlapiGetFieldValue('employee'));
							if (parseInt(STHours) >= 600)
							{
								var e1 = nlapiCreateError('Aborting Save', 'You have already submitted  timesheet for the selected day');
								throw e1.getDetails();
							}
							else
							{
								if (totaltime + parseInt(STHours) >= 600)
								{
									var e1 = nlapiCreateError('Aborting Save', 'Standard hours for the day cannot exceed 8 hrs(excluding OT)');
									throw e1.getDetails();
								}
							}
						}
					}
				}
			}
		}
	}
}


function Time_AfterSubmit(type)
{
	//Update floating holiday custom record
	if(nlapiGetFieldValue('weekly') != null && nlapiGetFieldValue('weekly') == 'T')
	{
		if(type == 'create')
		{
			if(nlapiGetFieldValue('custpage_fhavailed') != null && nlapiGetFieldValue('custpage_fhavailed') == 'T')
			{
				UpdateFH_Record(nlapiGetFieldValue('employee'), nlapiGetFieldValue('custpage_fhdateavailed'));
			}
		}
	}
	else
	{
		if(type != 'delete')
		{
			if(nlapiGetFieldText('casetaskevent') == 'Floating Holiday (Project Task)')
			{
				var FH_date = new Date(nlapiGetFieldValue('trandate'));

				UpdateFH_Record(nlapiGetFieldValue('employee'), nlapiGetFieldValue('trandate'));
			}
		}
	}
}

function Employee_AfterSubmit(type)
{
	if(type == 'create')
	{
		if(nlapiGetFieldValue('subsidiary') == 2 && nlapiGetFieldValue('custentity_persontype') == 2 && nlapiGetFieldValue('employeetype') == 3)
		{
			//Only if employee belongs to LLC unit, Person Type is Employee and Type is Salaried
			var record = nlapiCreateRecord('customrecord_floating_holiday');
			record.setFieldValue('custrecord_empid', nlapiGetRecordId());
			var FH_date = new Date();
			record.setFieldValue('custrecord_fh_year', FH_date.getFullYear());
			var recid = nlapiSubmitRecord(record);
		}
	}
	else if(type == 'edit')
	{
	
	
	}
}

function UpdateFH_Record(EmpId, FH_AvailedDate)
{
	var FH_date = new Date(FH_AvailedDate);
	
	var cols = new Array();
	cols[cols.length] = new nlobjSearchColumn('custrecord_fh_year');
	
	var filters = new Array();
	filters[filters.length] = new nlobjSearchFilter('custrecord_empid', null, 'anyof', EmpId);
	//filters[filters.length] = new nlobjSearchFilter('custrecord_fh_year', null, 'is', FH_date.getFullYear());
	
	var searchResult = nlapiSearchRecord('customrecord_floating_holiday', null, filters, cols);
	for(var i=0; searchResult != null && i < searchResult.length; i++)
	{
		if(searchResult[i].getValue('custrecord_fh_year') == FH_date.getFullYear())
		{
			var record = nlapiLoadRecord('customrecord_floating_holiday', searchResult[i].getId());
			record.setFieldValue('custrecord_fh_date_availed', FH_AvailedDate);
			record.setFieldValue('custrecord_fh_availed', 'T');
			var Recid = nlapiSubmitRecord(record);
		}
	}
}

function Evaluate_Time(request, response)
{
	var returnValues = '';
	try
	{
		switch(request.getParameter('type'))
		{
			case 'W':
				returnValues = FetchTime(request.getParameter('trandate'), 0, request.getParameter('employee'));
				break;
				
			case 'D':
				returnValues = FetchTime(request.getParameter('trandate'), 1, request.getParameter('employee'));
				break;

			case 'P':
				returnValues = FetchProjectType(request.getParameter('projid'));
				break;
			
			case 'FH':
				returnValues = CheckFloatingHoliday(request.getParameter('trandate'), request.getParameter('empid'));
				break;
				
			default:
				break;
		}
		response.write(returnValues);
	}
	catch(err)
	{
		nlapiLogExecution('DEBUG', 'Catch Error', err.getDetails());
	}
}

function FetchTime(TranDate, Type, EmpId)
{
	var TotalTime = 0;
	var cols = new Array();
	cols[cols.length] = new nlobjSearchColumn('hours');
	cols[cols.length] = new nlobjSearchColumn('item');

	nlapiLogExecution('DEBUG', '1', '1');
	var filters = new Array();
	if(Type == 0)
	{
		nlapiLogExecution('DEBUG', '1', '2');
		filters[filters.length] = new nlobjSearchFilter('date', null, 'within', TranDate, nlapiAddDays(new Date(TranDate), 6));
	}
	else
	{
		nlapiLogExecution('DEBUG', '1', '3');
		filters[filters.length] = new nlobjSearchFilter('date', null, 'within', TranDate, TranDate);
	}
	filters[filters.length] = new nlobjSearchFilter('type', null, 'is', 'A');
	filters[filters.length] = new nlobjSearchFilter('employee', null, 'anyof', EmpId);

		nlapiLogExecution('DEBUG', '1', '4');

	var searchResult = nlapiSearchRecord('timebill', null, filters, cols);
	nlapiLogExecution('DEBUG', '1', '5');
	
	if(searchResult != null)
	{
		nlapiLogExecution('DEBUG', '1', '6');

		for(var j=0; j< searchResult.length; j++)
		{
				nlapiLogExecution('DEBUG', '1', '7');

			if(searchResult[j].getValue('item') != 'OT')
			{
					nlapiLogExecution('DEBUG', '1', '8');

				var FilledHrs = searchResult[j].getValue('hours').split(':'); 
				TotalTime += parseInt(FilledHrs[0] * 60) + parseInt(FilledHrs[1]);
			}
				nlapiLogExecution('DEBUG', '1', '9');

		}
		FetchTime = TotalTime;
			nlapiLogExecution('DEBUG', '1', '10');
			nlapiLogExecution('DEBUG', 'FetchTime', 'FetchTime');


	}
	else
	{
		FetchTime = parseInt(TotalTime);
			nlapiLogExecution('DEBUG', '1', '11');
			nlapiLogExecution('DEBUG', 'FetchTime', FetchTime);
	}
				nlapiLogExecution('DEBUG', '1', '12');

	return FetchTime;
}

function TimeSheet_FieldChanged(type, name, linenum)
{
	if(type == 'timeitem' && name == 'customer')
	{
		var CustProj = nlapiGetCurrentLineItemValue('timeitem', 'customer');
		if(CustProj != null && CustProj.length > 0)
		{
			var response = nlapiRequestURL(nlapiResolveURL('SUITELET', 'customscript_fetchvalue_timesheet', 'customdeploy_fetchvalue_timesheet') + '&type=P&projid=' + nlapiGetCurrentLineItemValue('timeitem', 'customer'), null, null );
			var splitvalues = response.getBody().split(":");			
			nlapiSetCurrentLineItemValue('timeitem', 'custcol_projecttype', splitvalues[0]); 
		}
		else
		{
			nlapiSetCurrentLineItemValue('timeitem', 'custcol_projecttype', ''); 
		}
	}
	else if(name == 'customer')
	{
		var CustProj = nlapiGetFieldValue('customer');
		if(CustProj != null && CustProj.length > 0)
		{
			var response = nlapiRequestURL(nlapiResolveURL('SUITELET', 'customscript_fetchvalue_timesheet', 'customdeploy_fetchvalue_timesheet') + '&type=P&projid=' + CustProj, null, null );
			var splitvalues = response.getBody().split(":");			
			nlapiSetFieldValue('custcol_projecttype', splitvalues[0]); 
		}
		else
		{
			nlapiSetFieldValue('custcol_projecttype', ''); 
		}
	}
	return true;
}

function FetchProjectType(ProjId)
{
	var returnValues = '';
	var cols = new Array();
	cols[cols.length] = new nlobjSearchColumn('jobtype');
	cols[cols.length] = new nlobjSearchColumn('custentity_practice');
	cols[cols.length] = new nlobjSearchColumn('custentity_vertical');
	
	var filters = new Array();
	filters[filters.length] = new nlobjSearchFilter('internalid', null, 'anyof', ProjId);
	
	var searchResult = nlapiSearchRecord('job', null, filters, cols);
	for(var i=0; searchResult != null && i < searchResult.length; i++)
	{
		returnValues += searchResult[i].getText('jobtype') + ':' + searchResult[i].getValue('custentity_practice') + ':' + searchResult[i].getValue('custentity_vertical');
		//FetchProjectType = searchResult[i].getText('jobtype');
	}
	
	return returnValues;
}

function CheckFloatingHoliday(TranYear, EmpId)
{
	var returnValues = '';
	var cols = new Array();
	cols[cols.length] = new nlobjSearchColumn('custrecord_fh_availed');
	
	var filters = new Array();
	filters[filters.length] = new nlobjSearchFilter('custrecord_empid', null, 'anyof', EmpId);
	filters[filters.length] = new nlobjSearchFilter('custrecord_fh_year', null, 'is', TranYear);
	
	var searchResult = nlapiSearchRecord('customrecord_floating_holiday', null, filters, cols);
	if(searchResult != null)
	{
		for(var i=0; searchResult != null && i < searchResult.length; i++)
		{
			returnValues = searchResult[i].getValue('custrecord_fh_availed');
		}
	}
	else
	{
		returnValues = '-1';
	}
	return returnValues;
}

function UpdateOTRates()
{
	//'1458;Yes;No;25.17',
	//'2884;No;No;0.01',
	//'3187;No;No;0.01',
	//'3287;Yes;No;20.15',
	var OTFields = ['3288;Yes;No;23.17',
'3289;Yes;No;25',
'4467;No;No;0.01',
'4469;No;No;0'];


	nlapiLogExecution('DEBUG', 'Total Count', OTFields.length);
	for(var i=0; i < OTFields.length; i++)
	{
		var SplitOTField = OTFields[i].split(';');
		/*nlapiLogExecution('DEBUG', 'Internal Id', SplitOTField[0]);
		nlapiLogExecution('DEBUG', 'OT Billable', SplitOTField[1]);
		nlapiLogExecution('DEBUG', 'OT Payable', SplitOTField[2]);
		nlapiLogExecution('DEBUG', 'OT Rate', SplitOTField[3]);*/
		
		var record = nlapiLoadRecord('resourceallocation', SplitOTField[0]);
		nlapiLogExecution('DEBUG', 'Record Loaded', SplitOTField[0]);
		
		if(SplitOTField[1] == 'Yes') //OT Billable
		{
			record.setFieldValue('custevent_otbillable', 'T');
		}
		else
		{
			record.setFieldValue('custevent_otbillable', 'F');
		}
		if(SplitOTField[2] == 'Yes') //OT Payable
		{
			record.setFieldValue('custevent_otpayable', 'T');
		}
		else
		{
			record.setFieldValue('custevent_otpayable', 'F');
		}
		
		record.setFieldValue('custevent_otserviceitem', 2425);
		record.setFieldValue('custevent_otrate', SplitOTField[3]);
		var recSubmit = nlapiSubmitRecord(record, false);
		nlapiLogExecution('DEBUG', 'Record Updated', recSubmit);
	}
}


function Update_ServiceItems()
{
	var SrvcField = ['366;ST','367;ST','368;ST'];
	
	for(var i=0; i < SrvcField.length; i++)
	{
		var SplitSrvcField = SrvcField[i].split(';');
		try
		{
			//nlapiLogExecution('DEBUG', 'Internal Id', SplitSrvcField[0]);
			//nlapiLogExecution('DEBUG', 'SplitSrvcField', SplitSrvcField[1]);
			
			var record = nlapiLoadRecord('resourceallocation', SplitSrvcField[0]);
			nlapiLogExecution('DEBUG', 'Record Loaded', SplitSrvcField[0]);
			
			record.setFieldValue('custevent1', SplitSrvcField[1]);
			var recSubmit = nlapiSubmitRecord(record, false);
			nlapiLogExecution('DEBUG', 'Record Updated', recSubmit);
		}
		catch(err)
		{
			nlapiLogExecution('ERROR', 'Not Processed',  SplitSrvcField[0]);
		}
	}
}

function Map_EmptoFHTask()
{
	//Map employee to floating task
	var SrvcField = [4448,3121];
	
	for(var i=0; i < SrvcField.length; i++)
	{
		try
		{
			var record = nlapiLoadRecord('resourceallocation', SrvcField[i]);
			nlapiLogExecution('DEBUG', 'Record Loaded', SrvcField[i]);
			
			var recSubmit = nlapiSubmitRecord(record, false);
			nlapiLogExecution('DEBUG', 'Record Updated', recSubmit);
		}
		catch(err)
		{
			nlapiLogExecution('ERROR', 'Not Processed',  SrvcField[i]);
		}
	}
}
