// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
/*
   	Script Name:
	Author:
	Company:
	Date:


	Script Modification Log:

	-- Date --			-- Modified By --				--Requested By--				-- Description --




Below is a summary of the process controls enforced by this script file.  The control logic is described
more fully, below, in the appropriate function headers and code blocks.


     SCHEDULED FUNCTION
		- scheduledFunction(type)


     SUB-FUNCTIONS
		- The following sub-functions are called by the above core functions in order to maintain code
            modularization:

               - NOT USED

*/
}
// END SCRIPT DESCRIPTION BLOCK  ====================================


// BEGIN SCHEDULED FUNCTION =============================================

function schedulerFunction(type)
{
    /*  On scheduled function:

          - PURPOSE
		-

	FIELDS USED:

          --Field Name--				--ID--


    */


	//  LOCAL VARIABLES



	//  SCHEDULED FUNCTION CODE BODY
	var context = nlapiGetContext();
	var fromDate =nlapiGetContext().getSetting('SCRIPT', 'custscript_tpfromdate')
	var toDate =nlapiGetContext().getSetting('SCRIPT', 'custscript_tptodate')
	var fromSubsidiary =nlapiGetContext().getSetting('SCRIPT', 'custscript_tpfromsubsidiary')
	var toSubsidiary =nlapiGetContext().getSetting('SCRIPT', 'custscript_tptosubsidiary')
	var recordId =nlapiGetContext().getSetting('SCRIPT', 'custscript_tprecordid')
    nlapiLogExecution('DEBUG','Tp ', " fromDate->"+fromDate)
    nlapiLogExecution('DEBUG','Tp ', " toDate->"+toDate)
    nlapiLogExecution('DEBUG','Tp ', " fromSubsidiary->"+fromSubsidiary)
    nlapiLogExecution('DEBUG','Tp ', " toSubsidiary->"+toSubsidiary)
    nlapiLogExecution('DEBUG','Tp ', " recordId->"+recordId)

    //===================================Delete Existing Records=====================================================
    	   var Td_Filters= new Array();
		    var Td_Column  = new Array();

			Td_Filters[0]=new nlobjSearchFilter('custrecord_tp_main',null,'is',recordId)
			//rent_Filters[1]=new nlobjSearchFilter('trandate',null,'onorbefore',toDate)

			Td_Column[0]= new nlobjSearchColumn('internalid')

	        var tpdResult = nlapiSearchRecord('customrecord_transferprice_data',null,Td_Filters,Td_Column);
			//nlapiLogExecution('DEBUG','In bench','benchResult=='+benchResult);

			if (tpdResult != null)
			 {
				//nlapiLogExecution('DEBUG','In bench','search=='+benchResult.length);
			 	for (var k = 0; k < tpdResult.length; k++)
				{
					   var id= tpdResult[k].getValue('internalid');
					   var delId = nlapiDeleteRecord('customrecord_transferprice_data', id);
				}

			}
    //===================================Delete Existing Records=====================================================
	//----------------------Begin:Code to COGS Value and create the Record------------------------
	       var cogs_Filters= new Array();
	       var cogs_Column  = new Array();

			cogs_Filters[0]=new nlobjSearchFilter('trandate',null,'onorafter',fromDate)
			cogs_Filters[1]=new nlobjSearchFilter('trandate',null,'onorbefore',toDate)

			cogs_Column[0]= new nlobjSearchColumn('internalid','job','group');//
			cogs_Column[1]= new nlobjSearchColumn('amount',null,'sum');
			cogs_Column[2]= new nlobjSearchColumn('account',null,'group');
			cogs_Column[3]= new nlobjSearchColumn('custentity_location','job','group');

			var cogsResult = nlapiSearchRecord('transaction', 'customsearch_cogos',cogs_Filters,cogs_Column);
			nlapiLogExecution('DEBUG','In COGS','search=='+cogsResult);
			if (cogsResult != null)
			{
				nlapiLogExecution('DEBUG','In COGS','search=='+cogsResult.length);
				for (var j = 0; j < cogsResult.length; j++)
				{
					var location = cogsResult[j].getValue('custentity_location','job','group');
					var project  = cogsResult[j].getValue('internalid','job','group');
					var amount   = cogsResult[j].getValue('amount',null,'sum');
					var account  = cogsResult[j].getValue('account',null,'group');
					//*nlapiLogExecution('DEBUG','In COGS','location=='+location);
					//nlapiLogExecution('DEBUG','In COGS','project=='+project);
					//nlapiLogExecution('DEBUG','In COGS','amount=='+amount);
					//nlapiLogExecution('DEBUG','In COGS','account=='+account);
					var newReportRec=nlapiCreateRecord('customrecord_transferprice_data')
					newReportRec.setFieldValue('custrecord_tp_main',recordId)
					newReportRec.setFieldValue('custrecord_tp_category','Direct')
					newReportRec.setFieldValue('custrecord_tp_subcategory','COGS')
					newReportRec.setFieldValue('custrecord_tp_particulars',account)
					newReportRec.setFieldValue('custrecord_tp_project',project)
					newReportRec.setFieldValue('custrecord_tp_amount',amount)
					newReportRec.setFieldValue('custrecord_tp_location',location)
					var id=nlapiSubmitRecord(newReportRec,true,true)
					nlapiLogExecution('DEBUG','In COGS','id============================='+id);

				}
			}
	//----------------------End:Code to COGS Value and create the Record------------------------
	//----------------------Begin: Code Bench Cost--------------------------------------------
		   var bench_Filters= new Array();
	       var bench_Column  = new Array();

			bench_Filters[0]=new nlobjSearchFilter('trandate',null,'onorafter',fromDate)
			bench_Filters[1]=new nlobjSearchFilter('trandate',null,'onorbefore',toDate)

     		bench_Column[0]= new nlobjSearchColumn('internalid','job','group');//
			bench_Column[1]= new nlobjSearchColumn('amount',null,'sum');
			bench_Column[2]= new nlobjSearchColumn('account',null,'group');
			bench_Column[3]= new nlobjSearchColumn('custentity_location','job','group');

			var benchResult = nlapiSearchRecord('transaction', 'customsearch_tp_bench',bench_Filters,bench_Column);
			nlapiLogExecution('DEBUG','In bench','benchResult=='+benchResult);
			var totalBenchcost=0
			if (benchResult != null)
			{
				nlapiLogExecution('DEBUG','In bench','search=='+benchResult.length);
				for (var j = 0; j < benchResult.length; j++)
				{
					var amount   = benchResult[j].getValue('amount',null,'sum');
					totalBenchcost=parseFloat(totalBenchcost)+parseFloat(amount)
				}

			}
		nlapiLogExecution('DEBUG','In bench','totalBenchcost=='+totalBenchcost);

		  //--------------------Begin:Seqmentwise employee count--------------------
		   var ec_Filters= new Array();
	       var ec_Column  = new Array();

			//ec_Filters[0]=new nlobjSearchFilter('trandate',null,'onorafter',fromDate)
			ec_Filters[0]=new nlobjSearchFilter('enddate',null,'onorbefore',toDate)

     		ec_Column[0]= new nlobjSearchColumn('internalid',null,'count');//
			ec_Column[1]= new nlobjSearchColumn('custentity_location','job','group');

			var ecResult = nlapiSearchRecord('resourceallocation', 'customsearch_tp_resourcecount',ec_Filters,ec_Column);
			nlapiLogExecution('DEBUG','In employeecount','ecResult=='+ecResult);
			var totalCount=0
			if (ecResult != null)
			{
				nlapiLogExecution('DEBUG','In employeecount','search=='+ecResult.length);
				for (var m = 0; m < ecResult.length; m++)
				{
					var count   = ecResult[m].getValue('internalid',null,'count');
					var location   = ecResult[m].getValue('custentity_location','job','group');

					totalCount=parseFloat(totalCount)+parseFloat(count)
				}
				nlapiLogExecution('DEBUG','In employeecount','totalCount=='+totalCount);
				for (var k = 0; k < ecResult.length; k++)
				{
					var count   = ecResult[k].getValue('internalid',null,'count');
					var location   = ecResult[k].getValue('custentity_location','job','group');
					nlapiLogExecution('DEBUG','In employeecount','count=='+count);
					nlapiLogExecution('DEBUG','In employeecount','location=='+location);
					//totalCount=parseFloat(totalCount)+parseFloat(count)
					//------------------------------------------------------
					var locationCost=(totalBenchcost/totalCount)*parseFloat(count)
					nlapiLogExecution('DEBUG','In employeecount------------','locationCost=='+locationCost);
					var newReportRec=nlapiCreateRecord('customrecord_transferprice_data')
					newReportRec.setFieldValue('custrecord_tp_main',recordId)
					newReportRec.setFieldValue('custrecord_tp_category','Direct')
					newReportRec.setFieldValue('custrecord_tp_subcategory','Bench')
					//newReportRec.setFieldValue('custrecord_tp_particulars',account)
					//newReportRec.setFieldValue('custrecord_tp_project',project)
					newReportRec.setFieldValue('custrecord_tp_amount',locationCost)
					newReportRec.setFieldValue('custrecord_tp_location',location)
					var id=nlapiSubmitRecord(newReportRec,true,true)
					nlapiLogExecution('DEBUG','In COGS','id============================='+id);
					//------------------------------------------------------
				}


			}


		  //--------------------End:Seqmentwise employee count----------------------
	//----------------------End: Code Bench Cost----------------------------------------------


    //-----------------------Begin:Delivery OH/ - Support------------------------------------------------
    		//---Total Benchcount-------------------------
    		var ebc_Filters= new Array();
	        var ebc_Column  = new Array();

			//ec_Filters[0]=new nlobjSearchFilter('trandate',null,'onorafter',fromDate)
			ebc_Filters[0]=new nlobjSearchFilter('enddate',null,'onorbefore',toDate)

     		ebc_Column[0]= new nlobjSearchColumn('internalid',null,'count');//
			ebc_Column[1]= new nlobjSearchColumn('custentity_location','job','group');

			var ebcResult = nlapiSearchRecord('resourceallocation', 'customsearch_tp_benchresource',ebc_Filters,ebc_Column);
			//nlapiLogExecution('DEBUG','In employeecount','ecResult=='+ecResult);
			var totalbenchCount=0
			if (ebcResult != null)
			{
				//nlapiLogExecution('DEBUG','In employeecount','search=='+ecResult.length);
				for (var m = 0; m < ebcResult.length; m++)
				{
					var count   = ebcResult[m].getValue('internalid',null,'count');
					var location   = ebcResult[m].getValue('custentity_location','job','group');

					totalbenchCount=parseFloat(totalbenchCount)+parseFloat(count)
				}
			}
			nlapiLogExecution('DEBUG','In bench','totalbenchCount=='+totalbenchCount);
			//---End:Total Benchcount-------------------------


			//----------------------Begin: Code total overhead Cost--------------------------------------------
		   var ohCost_Filters= new Array();
	       var ohCost_Column  = new Array();

			ohCost_Filters[0]=new nlobjSearchFilter('trandate',null,'onorafter',fromDate)
			ohCost_Filters[1]=new nlobjSearchFilter('trandate',null,'onorbefore',toDate)

     		ohCost_Column[0]= new nlobjSearchColumn('internalid','job','group');//
			ohCost_Column[1]= new nlobjSearchColumn('amount',null,'sum');
			ohCost_Column[2]= new nlobjSearchColumn('account',null,'group');
			ohCost_Column[3]= new nlobjSearchColumn('custentity_location','job','group');

			var ohCostResult = nlapiSearchRecord('transaction', 'customsearch_tp_deliveryohsupport',ohCost_Filters,ohCost_Column);
			//nlapiLogExecution('DEBUG','In bench','benchResult=='+benchResult);
			var totalohCostcost=0
			if (ohCostResult != null)
			{
				//nlapiLogExecution('DEBUG','In bench','search=='+benchResult.length);
				for (var j = 0; j < ohCostResult.length; j++)
				{
					var amount   = ohCostResult[j].getValue('amount',null,'sum');
					totalohCostcost=parseFloat(totalohCostcost)+parseFloat(amount)
				}

			}
			nlapiLogExecution('DEBUG','In bench','totalohCostcost=='+totalohCostcost);

			//----------------------end: Code total overhead Cost--------------------------------------------
			//------------------------begin:percentage calclation of resource--------------------------------
			//totalCount
			var totaldelohOCount=0
			for (var k = 0; k < ecResult.length; k++)
				{
					var count   = ecResult[k].getValue('internalid',null,'count');
					var location   = ecResult[k].getValue('custentity_location','job','group');
					nlapiLogExecution('DEBUG','In employeecount','count=='+count);
					nlapiLogExecution('DEBUG','In employeecount','totalCount=='+totalCount);
					//totalCount=parseFloat(totalCount)+parseFloat(count)
					//------------------------------------------------------
					var employeePer=(parseFloat(count)/parseFloat(totalCount))*100
				    nlapiLogExecution('DEBUG','In Delivery oh','employeePer=='+employeePer);
					//totalbenchCount
					var delohOCount=(parseFloat(totalbenchCount)*parseFloat(employeePer)/100)+parseFloat(count)
					nlapiLogExecution('DEBUG','In Delivery oh','delohOCount=='+delohOCount);
					totaldelohOCount=parseFloat(totaldelohOCount)+parseFloat(delohOCount)
				}
					nlapiLogExecution('DEBUG','In Delivery oh','totaldelohOCount=='+totaldelohOCount);

			//========================================================================
			//totalohCostcost
			for (var k = 0; k < ecResult.length; k++)
				{
					var count   = ecResult[k].getValue('internalid',null,'count');
					var location   = ecResult[k].getValue('custentity_location','job','group');
					nlapiLogExecution('DEBUG','In employeecount','count=='+count);
					nlapiLogExecution('DEBUG','In employeecount','totalCount=='+totalCount);
					//totalCount=parseFloat(totalCount)+parseFloat(count)
					//------------------------------------------------------
					var employeePer=(parseFloat(count)/parseFloat(totalCount))*100
				    nlapiLogExecution('DEBUG','In Delivery oh','employeePer=='+employeePer);
					//totalbenchCount
					var delohOCount=(parseFloat(totalbenchCount)*parseFloat(employeePer)/100)+parseFloat(count)
					nlapiLogExecution('DEBUG','In Delivery oh','delohOCount=='+delohOCount);
					var delOhcost=(parseFloat(totalohCostcost)/parseFloat(totaldelohOCount))*parseFloat(delohOCount)
					nlapiLogExecution('DEBUG','In Delivery oh','delOhcost=='+delOhcost);

					var newReportRec=nlapiCreateRecord('customrecord_transferprice_data')
					newReportRec.setFieldValue('custrecord_tp_main',recordId)
					newReportRec.setFieldValue('custrecord_tp_category','Direct')
					newReportRec.setFieldValue('custrecord_tp_subcategory','Delivery OH-Support')
					//newReportRec.setFieldValue('custrecord_tp_particulars',account)
					//newReportRec.setFieldValue('custrecord_tp_project',project)
					newReportRec.setFieldValue('custrecord_tp_amount',delOhcost)
					newReportRec.setFieldValue('custrecord_tp_location',location)
				    var id=nlapiSubmitRecord(newReportRec,true,true)
					nlapiLogExecution('DEBUG','In COGS','id============================='+id);
				}


			// =======================================================================
			//------------------------end:percentage calclation of resource--------------------------------
    //-----------------------End: Delivery OH/ - Support-------------------------------------------------


    //------------------------Begin:Rent Calculation------------------------------------------------------
    	//----------------------Begin: Code total rent Cost--------------------------------------------
		   var rent_Filters= new Array();
	       var rent_Column  = new Array();

			rent_Filters[0]=new nlobjSearchFilter('trandate',null,'onorafter',fromDate)
			rent_Filters[1]=new nlobjSearchFilter('trandate',null,'onorbefore',toDate)

     		rent_Column[0]= new nlobjSearchColumn('internalid','job','group');//
			rent_Column[1]= new nlobjSearchColumn('amount',null,'sum');
			rent_Column[2]= new nlobjSearchColumn('account',null,'group');
			rent_Column[3]= new nlobjSearchColumn('custentity_location','job','group');

			var rentResult = nlapiSearchRecord('transaction', 'customsearch_tp_rent',rent_Filters,rent_Column);
			//nlapiLogExecution('DEBUG','In bench','benchResult=='+benchResult);
			var totalRentcost=0
			if (rentResult != null)
			{
				//nlapiLogExecution('DEBUG','In bench','search=='+benchResult.length);
				for (var j = 0; j < rentResult.length; j++)
				{
					var amount   = rentResult[j].getValue('amount',null,'sum');
					totalRentcost=parseFloat(totalRentcost)+parseFloat(amount)
				}

			}
			nlapiLogExecution('DEBUG','In Rent','totalohRentcost=='+totalRentcost);

			//----------------------end: Code total rent Cost--------------------------------------------

			//========================Locatinwise Sqft===============================================
			//totalohCostcost
			var totalSqftOccupied=0
			for (var k = 0; k < ecResult.length; k++)
				{
					var count   = ecResult[k].getValue('internalid',null,'count');
					var location   = ecResult[k].getValue('custentity_location','job','group');

					//totalCount=parseFloat(totalCount)+parseFloat(count)
					//------------------------------------------------------
					var employeePer=(parseFloat(count)/parseFloat(totalCount))*100
				    nlapiLogExecution('DEBUG','In Rent','employeePer=='+employeePer);
					//totalbenchCount
					var delohOCount=(parseFloat(totalbenchCount)*parseFloat(employeePer)/100)+parseFloat(count)
					nlapiLogExecution('DEBUG','In Rent','delohOCount=='+delohOCount);
                    var locationPersqft=getLoactionSqft(location)
                    var sqftOccupied=parseFloat(locationPersqft)*parseFloat(delohOCount)
                    nlapiLogExecution('DEBUG','In Rent','sqftOccupied=='+sqftOccupied);
                    totalSqftOccupied=parseFloat(totalSqftOccupied)+parseFloat(sqftOccupied)

				}
				nlapiLogExecution('DEBUG','In Rent','totalSqftOccupied=='+totalSqftOccupied);

			// =======================================================================
			//totalRentcost
			 	for (var k = 0; k < ecResult.length; k++)
				{
					var count   = ecResult[k].getValue('internalid',null,'count');
					var location   = ecResult[k].getValue('custentity_location','job','group');

					//totalCount=parseFloat(totalCount)+parseFloat(count)
					//------------------------------------------------------
					var employeePer=(parseFloat(count)/parseFloat(totalCount))*100
				    nlapiLogExecution('DEBUG','In Rent','employeePer=='+employeePer);
					//totalbenchCount
					var delohOCount=(parseFloat(totalbenchCount)*parseFloat(employeePer)/100)+parseFloat(count)
					nlapiLogExecution('DEBUG','In Rent','delohOCount=='+delohOCount);
                    var locationPersqft=getLoactionSqft(location)
                    var sqftOccupied=parseFloat(locationPersqft)*parseFloat(delohOCount)
                    var rentAmout=parseFloat(totalRentcost)*parseFloat(sqftOccupied)/parseFloat(totalSqftOccupied)

					var newReportRec=nlapiCreateRecord('customrecord_transferprice_data')
					newReportRec.setFieldValue('custrecord_tp_main',recordId)
					newReportRec.setFieldValue('custrecord_tp_category','Indirect')
					newReportRec.setFieldValue('custrecord_tp_subcategory','Rent')
					//newReportRec.setFieldValue('custrecord_tp_particulars',account)
					//newReportRec.setFieldValue('custrecord_tp_project',project)
					newReportRec.setFieldValue('custrecord_tp_amount',rentAmout)
					newReportRec.setFieldValue('custrecord_tp_location',location)
				     var id=nlapiSubmitRecord(newReportRec,true,true)
				     nlapiLogExecution('DEBUG','In rent','id============================='+id);

				}
    //------------------------End:Rent Calculation------------------------------------------------------

    //-----------------------Begin:SG & Administration Expenses-----------------------------------------
    		//----------------------Begin: Code total sg and admin Cost--------------------------------------------
		   var sgadmin_Filters= new Array();
	       var sgadmin_Column  = new Array();

			sgadmin_Filters[0]=new nlobjSearchFilter('trandate',null,'onorafter',fromDate)
			sgadmin_Filters[1]=new nlobjSearchFilter('trandate',null,'onorbefore',toDate)

     		sgadmin_Column[0]= new nlobjSearchColumn('internalid','job','group');//
			sgadmin_Column[1]= new nlobjSearchColumn('amount',null,'sum');
			sgadmin_Column[2]= new nlobjSearchColumn('account',null,'group');
			sgadmin_Column[3]= new nlobjSearchColumn('custentity_location','job','group');

			var sgadminResult = nlapiSearchRecord('transaction', 'customsearch_tp_sgadminexpense',sgadmin_Filters,sgadmin_Column);
			//nlapiLogExecution('DEBUG','In bench','benchResult=='+benchResult);
			var totalSgadmincost=0
			if (sgadminResult != null)
			{
				//nlapiLogExecution('DEBUG','In bench','search=='+benchResult.length);
				for (var j = 0; j < sgadminResult.length; j++)
				{
					var amount   = sgadminResult[j].getValue('amount',null,'sum');
					totalSgadmincost=parseFloat(totalSgadmincost)+parseFloat(amount)
				}

			}
			nlapiLogExecution('DEBUG','In Rent','totalSgadmincost=='+totalSgadmincost);

			//----------------------end: Code total sg and admin Cost--------------------------------------------

			for (var k = 0; k < ecResult.length; k++)
				{
					var count   = ecResult[k].getValue('internalid',null,'count');
					var location   = ecResult[k].getValue('custentity_location','job','group');
					nlapiLogExecution('DEBUG','In employeecount','count=='+count);
					nlapiLogExecution('DEBUG','In employeecount','totalCount=='+totalCount);
					//totalCount=parseFloat(totalCount)+parseFloat(count)
					//------------------------------------------------------
					var employeePer=(parseFloat(count)/parseFloat(totalCount))*100
				    nlapiLogExecution('DEBUG','In sgAdmincost ','employeePer=='+employeePer);
					//totalbenchCount
					var delohOCount=(parseFloat(totalbenchCount)*parseFloat(employeePer)/100)+parseFloat(count)
					nlapiLogExecution('DEBUG','In sgAdmincost ','delohOCount=='+delohOCount);
					var sgAdmincost=(parseFloat(totalSgadmincost)*parseFloat(delohOCount))/parseFloat(totaldelohOCount)
					nlapiLogExecution('DEBUG','In sgAdmincost ','sgAdmincost=='+sgAdmincost);

					var newReportRec=nlapiCreateRecord('customrecord_transferprice_data')
					newReportRec.setFieldValue('custrecord_tp_main',recordId)
					newReportRec.setFieldValue('custrecord_tp_category','Indirect')
					newReportRec.setFieldValue('custrecord_tp_subcategory','SG & Administration  Expenses')
					//newReportRec.setFieldValue('custrecord_tp_particulars',account)
					//newReportRec.setFieldValue('custrecord_tp_project',project)
					newReportRec.setFieldValue('custrecord_tp_amount',sgAdmincost)
					newReportRec.setFieldValue('custrecord_tp_location',location)
				    var id=nlapiSubmitRecord(newReportRec,true,true)
					nlapiLogExecution('DEBUG','In sgAdmincost','id============================='+id);
				}
    //-----------------------End:SG & Administration Expenses-----------------------------------------

    //-----------------------Begin:depcerication cost-------------------------------------------------
    	  var dep_Filters= new Array();
	       var dep_Column  = new Array();

			dep_Filters[0]=new nlobjSearchFilter('trandate',null,'onorafter',fromDate)
			dep_Filters[1]=new nlobjSearchFilter('trandate',null,'onorbefore',toDate)

     		dep_Column[0]= new nlobjSearchColumn('internalid','job','group');//
			dep_Column[1]= new nlobjSearchColumn('amount',null,'sum');
			dep_Column[2]= new nlobjSearchColumn('account',null,'group');
			dep_Column[3]= new nlobjSearchColumn('custentity_location','job','group');

			var depResult = nlapiSearchRecord('transaction', 'customsearch_tp_depcerication',dep_Filters,dep_Column);
			//nlapiLogExecution('DEBUG','In bench','benchResult=='+benchResult);
			var totalDepcal=0
			if (depResult != null)
			{
				//nlapiLogExecution('DEBUG','In bench','search=='+benchResult.length);
				for (var j = 0; j < depResult.length; j++)
				{
					var amount   = depResult[j].getValue('amount',null,'sum');
					totalDepcal=parseFloat(totalDepcal)+parseFloat(amount)
				}

			}
			else
			{
			    var newReportRec=nlapiCreateRecord('customrecord_transferprice_data')
				newReportRec.setFieldValue('custrecord_tp_main',recordId)
				newReportRec.setFieldValue('custrecord_tp_category','Indirect')
				newReportRec.setFieldValue('custrecord_tp_subcategory','Depcerication')
				//newReportRec.setFieldValue('custrecord_tp_particulars',account)
				//newReportRec.setFieldValue('custrecord_tp_project',project)
				newReportRec.setFieldValue('custrecord_tp_amount',0)
				//newReportRec.setFieldValue('custrecord_tp_location',location)
				var id=nlapiSubmitRecord(newReportRec,true,true)
				nlapiLogExecution('DEBUG','In sgAdmincost','id============================='+id);
			}
			nlapiLogExecution('DEBUG','In depcerication','totalDepcal=='+totalDepcal);
    //-----------------------End:depcerication cost-------------------------------------------------

	  var i_usage_begin = context.getRemainingUsage();
        nlapiLogExecution('DEBUG', 'schedulerFunction','Remaining Usage  -->' + i_usage_begin);
}

// END SCHEDULED FUNCTION ===============================================





// BEGIN FUNCTION ===================================================
{

function getLoactionSqft(location)
{
	   var rate=0
	   if(location!=null&&location!='')
	   {
		   var loc_Filters= new Array();
		   var loc_Column  = new Array();

			loc_Filters[0]=new nlobjSearchFilter('custrecord_tp_rentdetail',null,'is',location)
			//rent_Filters[1]=new nlobjSearchFilter('trandate',null,'onorbefore',toDate)

			loc_Column[0]= new nlobjSearchColumn('custrecord_tp_rentpersqftrate')

	        var locResult = nlapiSearchRecord('customrecord_tp_rentdetail',null,loc_Filters,loc_Column);
			//nlapiLogExecution('DEBUG','In bench','benchResult=='+benchResult);

			if (locResult != null)
			 {
				//nlapiLogExecution('DEBUG','In bench','search=='+benchResult.length);
			 	for (var j = 0; j < locResult.length; j++)
				{
					   rate= locResult[0].getValue('custrecord_tp_rentpersqftrate');

				}

			}

	   }
	   nlapiLogExecution('DEBUG','In getLoactionSqft','rate=='+rate);
		return rate
}

}
// END FUNCTION =====================================================
