// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
/*
   	Script Name:Sut Generate Vendor payable report
	Author:		Nikhil jain
	Company:	Aashna cloudtech Pvt. Ltd.
	Date:		01/07/2015
	Description:Genrate an vendor paybale report 


	Script Modification Log:

	-- Date --			-- Modified By --				--Requested By--				-- Description --



Below is a summary of the process controls enforced by this script file.  The control logic is described
more fully, below, in the appropriate function headers and code blocks.


     SUITELET
		- suiteletFunction(request, response)


     SUB-FUNCTIONS
		- The following sub-functions are called by the above core functions in order to maintain code
            modularization:

               - NOT USED

*/
}
// END SCRIPT DESCRIPTION BLOCK  ====================================



// BEGIN GLOBAL VARIABLE BLOCK  =====================================
{
	//  Initialize any Global Variables, in particular, debugging variables...




}
// END GLOBAL VARIABLE BLOCK  =======================================





// BEGIN SUITELET ==================================================

function suiteletFunction_GenVenPayableReport(request, response)
{

	/*  Suitelet:

          - EXPLAIN THE PURPOSE OF THIS FUNCTION

		-

		FIELDS USED:

          --Field Name--				--ID--


	*/


	//  LOCAL VARIABLES


    //  SUITELET CODE BODY
	
	if (request.getMethod() == 'GET') 
	{
	
		// == SC : CREATE FORM ===
		var form = nlapiCreateForm('Vendor Payable Report');
		
		
		// === SC:Add fields in the newly created form ===
		 form.setScript('customscript_cli_vendor_payable_report');
		  
		var vendor = form.addField('custpage_vendor', 'select', 'Vendor', 'Vendor');
		vendor.setMandatory(true);
		
		var from_date = form.addField('custpage_fromdate', 'date', 'From Date');
		from_date.setMandatory(true);
		
		var to_date = form.addField('custpage_todate', 'date', 'To Date');
		to_date.setMandatory(true);
		
		form.addButton('custombutton', 'Generate Report', 'generate_VendorPayableReport()');
		//form.addSubmitButton('Generate Report');
		
		  response.writePage(form);
	}
	
}
function suiteletFunction_downloadreport(request, response)
{

	/*  Suitelet:

          - EXPLAIN THE PURPOSE OF THIS FUNCTION

		-

		FIELDS USED:

          --Field Name--				--ID--


	*/


	//  LOCAL VARIABLES


    //  SUITELET CODE BODY
	
	
	var SearchxmlString = '';
	
	var vendor = request.getParameter('vendor');
	var fromdate = request.getParameter('fromdate');
	var todate = request.getParameter('todate');
	
	var a_filters = new Array();
	var a_column = new Array();
	
	a_filters.push(new nlobjSearchFilter('entity', null, 'is', vendor));
	a_filters.push(new nlobjSearchFilter('trandate', null, 'onorafter', fromdate));
	a_filters.push(new nlobjSearchFilter('trandate', null, 'onorbefore', todate));
	a_filters.push(new nlobjSearchFilter('mainline', null, 'is','T'));
	a_column.push(new nlobjSearchColumn('internalid'));
	a_column.push(new nlobjSearchColumn('trandate'));
	a_column.push(new nlobjSearchColumn('transactionnumber'));
	a_column.push(new nlobjSearchColumn('custbody_invoicenumber'));
	a_column.push(new nlobjSearchColumn('custbody_vendorinvoicedate'));
	a_column.push(new nlobjSearchColumn('custbody_oldinvoicenumber'));
	a_column.push(new nlobjSearchColumn('entity'));
	a_column.push(new nlobjSearchColumn('memo'));
	a_column.push(new nlobjSearchColumn('amount'));
	a_column.push(new nlobjSearchColumn('taxtotal'));
	a_column.push(new nlobjSearchColumn('currency'));
	a_column.push(new nlobjSearchColumn('fxamount'));
	a_column.push(new nlobjSearchColumn('duedate'));
	a_column.push(new nlobjSearchColumn('subsidiarynohierarchy'));
	
	var s_serchResult = nlapiSearchRecord('vendorbill', 'customsearch_vendor_payable_report', a_filters, a_column)
	
	if (s_serchResult != null && s_serchResult != undefined && s_serchResult != '') 
	{
		for (var i = 0; i < s_serchResult.length; i++) 
		{
			var internal_id = s_serchResult[i].getValue('internalid');
			nlapiLogExecution('DEBUG', 'generate vendor payable report', "internal_id" + internal_id);
			
			
			//=====================Begin : Function to get the tds amount==========================================// 
			
			var tdsamount = getTDSamount(internal_id);
			
			//=====================Begin : Function to get the tds amount==========================================//
			
			var date = s_serchResult[i].getValue('trandate');
			//nlapiLogExecution('DEBUG', 'generate vendor payable report', "date" + date);
			
			var billnumber = s_serchResult[i].getValue('transactionnumber');
			//nlapiLogExecution('DEBUG', 'generate vendor payable report', "billnumber" + billnumber);
			
			var invoice_no = s_serchResult[i].getValue('custbody_invoicenumber');
			//nlapiLogExecution('DEBUG', 'generate vendor payable report', "invoice_no" + invoice_no);
			
			var invoice_date = s_serchResult[i].getValue('custbody_vendorinvoicedate');
			//nlapiLogExecution('DEBUG', 'generate vendor payable report', "invoice_date" + invoice_date);
			
			var cust_invoice_no = s_serchResult[i].getValue('custbody_oldinvoicenumber');
			//nlapiLogExecution('DEBUG', 'generate vendor payable report', "cust_invoice_no" + cust_invoice_no);
			
			var vendor_name = s_serchResult[i].getText('entity');
			//nlapiLogExecution('DEBUG', 'generate vendor payable report', "vendor_name" + vendor_name);
			
			var memo = s_serchResult[i].getValue('memo');
			//nlapiLogExecution('DEBUG', 'generate vendor payable report', "memo" + memo);
			
			var user_total = s_serchResult[i].getValue('amount');
			//nlapiLogExecution('DEBUG', 'generate vendor payable report', "user_total" + user_total);
			
			var taxtotal = s_serchResult[i].getValue('taxtotal');
			//nlapiLogExecution('DEBUG', 'generate vendor payable report', "taxtotal" + taxtotal);
			
			var currency = s_serchResult[i].getText('currency');
			//nlapiLogExecution('DEBUG', 'generate vendor payable report', "currency" + currency);
			
			var fxamount = s_serchResult[i].getValue('fxamount');
			//nlapiLogExecution('DEBUG', 'generate vendor payable report', "fxamount" + fxamount);
			
			var duedate = s_serchResult[i].getValue('duedate');
			//nlapiLogExecution('DEBUG', 'generate vendor payable report', "duedate" + duedate);
			
			var subsidiary = s_serchResult[i].getText('subsidiarynohierarchy');
			//nlapiLogExecution('DEBUG', 'generate vendor payable report', "subsidiary" + subsidiary);
			
			var grossamount = parseFloat(user_total) + parseFloat(taxtotal)
			//nlapiLogExecution('DEBUG', 'generate vendor payable report', "grossamount" + grossamount);
			
			 if(tdsamount != null && tdsamount != '' && tdsamount != undefined)
			 {
			 	grossamount = parseFloat(grossamount) + parseFloat(tdsamount);
			 }
			SearchxmlString += '<Row>' +
			'<Cell><Data ss:Type="String">' +
			date +
			'</Data></Cell>' +
			'<Cell><Data ss:Type="String">' +
			billnumber +
			'</Data></Cell>' +
			'<Cell><Data ss:Type="String">' +
			invoice_no +
			'</Data></Cell>' +
			'<Cell><Data ss:Type="String">' +
			invoice_date +
			'</Data></Cell>' +
			'<Cell><Data ss:Type="String">' +
			cust_invoice_no +
			'</Data></Cell>' +
			'<Cell><Data ss:Type="String">' +
			vendor_name +
			'</Data></Cell>' +
			'<Cell><Data ss:Type="String">' +
			memo +
			'</Data></Cell>' +
			'<Cell><Data ss:Type="Number">' +
			grossamount +
			'</Data></Cell>' +
			'<Cell><Data ss:Type="Number">' +
			(-(taxtotal)) +
			'</Data></Cell>' +
			'<Cell><Data ss:Type="Number">' +
			tdsamount +
			'</Data></Cell>' +
			'<Cell><Data ss:Type="Number">' +
			user_total +
			'</Data></Cell>' +
			'<Cell><Data ss:Type="String">' +
			currency +
			'</Data></Cell>' +
			'<Cell><Data ss:Type="Number">' +
			fxamount +
			'</Data></Cell>' +
			'<Cell><Data ss:Type="String">' +
			duedate +
			'</Data></Cell>' +
			'<Cell><Data ss:Type="String">' +
			subsidiary +
			'</Data></Cell>' +
			'</Row>';
		}
		
		generate_excel_file(SearchxmlString,request, response)
	}
		
	
}

// END SUITELET ====================================================

function generate_excel_file(SearchxmlString,request, response)
{
	nlapiLogExecution('DEBUG', 'generate vendor payable report', "Generate file");
	var xmlString = '<?xml version="1.0"?><?mso-application progid="Excel.Sheet"?>';
	xmlString += '<Workbook xmlns="urn:schemas-microsoft-com:office:spreadsheet" ';
	xmlString += 'xmlns:o="urn:schemas-microsoft-com:office:office" ';
	xmlString += 'xmlns:x="urn:schemas-microsoft-com:office:excel" ';
	xmlString += 'xmlns:ss="urn:schemas-microsoft-com:office:spreadsheet" ';
	xmlString += 'xmlns:html="http://www.w3.org/TR/REC-html40">';
	
	xmlString += '<Worksheet ss:Name="Sheet1">';
	xmlString += '<Table>' +
	'<Row>' +
	'<Cell><Data ss:Type="String">Date</Data></Cell>' +
	'<Cell><Data ss:Type="String"> Bill# </Data></Cell>' +
	'<Cell><Data ss:Type="String"> Vendor Invoice# </Data></Cell>' +
	'<Cell><Data ss:Type="String"> Vendor Invoice Date</Data></Cell>' +
	'<Cell><Data ss:Type="String"> Customer Invoice #</Data></Cell>' +
	'<Cell><Data ss:Type="String"> Name </Data></Cell>' +
	'<Cell><Data ss:Type="String"> Memo </Data></Cell>' +
	'<Cell><Data ss:Type="String"> Gross Amount(Basic)</Data></Cell>' +
	'<Cell><Data ss:Type="String"> Service Tax</Data></Cell>' +
	'<Cell><Data ss:Type="String"> TDS </Data></Cell>' +
	'<Cell><Data ss:Type="String"> Net Amount </Data></Cell>' +
	'<Cell><Data ss:Type="String"> Currency </Data></Cell>' +
	'<Cell><Data ss:Type="String"> Amount(Foriegn Currency)</Data></Cell>' +
	'<Cell><Data ss:Type="String"> Due Date/RecivedBy</Data></Cell>' +
	'<Cell><Data ss:Type="String"> Subsidiary </Data></Cell>' +
	'</Row>';
	
	xmlString += SearchxmlString;
	
	xmlString += '</Table></Worksheet></Workbook>';
	
	//create file
	var xlsFile = nlapiCreateFile('Vendor_Payable_report.xls', 'EXCEL', nlapiEncrypt(xmlString, 'base64'));
	
	var folederid = get_folderID();
	xlsFile.setFolder(folederid);
	xlsFile.setIsOnline(true);
	//window.open(xlsFile)
	//save file 
	var fileID = nlapiSubmitFile(xlsFile);
	var fileobj = nlapiLoadFile(fileID);
	nlapiLogExecution('DEBUG', 'Script Scheduled ', 'fileobj -->' + fileobj);
			
	var file_url = fileobj.getURL();
	nlapiLogExecution('DEBUG', 'Script Scheduled ', 'file_url -->' + file_url);
	
	var html = '<html>' 

              + '<script type="text/javascript">'

              + 'window.location = https://system.na1.netsuite.com'+file_url;

              + '</script>'

              + '</html>';

              response.write(file_url);
			  

}

function getTDSamount(internal_id)
{
	
	var tds_amount = parseFloat('0');
	var tdsamt_filters = new Array();
	tdsamt_filters.push( new nlobjSearchFilter('custrecord_billbillno', null, 'is',internal_id));
	tdsamt_filters.push( new nlobjSearchFilter('isinactive', null, 'is','F'));
	
	var tds_amt_column = new Array();
	tds_amt_column[0] =  new nlobjSearchColumn('custrecord_billtdsamount',null,'sum');    
	
	var results = nlapiSearchRecord('customrecord_tdsbillrelation', null, tdsamt_filters, tds_amt_column);
	if (results != null && results != '' && results != undefined) 
	{
		tds_amount = results[0].getValue(tds_amt_column[0])
		nlapiLogExecution('DEBUG', 'create_quartely_detailrecord', 'tds_amount->' + tds_amount)
		
	}	
	return tds_amount;
	
}
function get_folderID()
{
			var folder_filters = new Array();
			folder_filters.push(new nlobjSearchFilter('name', null, 'is', 'Vendor_Payable_report'));
	
			var folder_column = new Array();
			folder_column.push( new nlobjSearchColumn('internalid'));    
	
			var folder_results = nlapiSearchRecord('folder', null, folder_filters, folder_column);
			if (folder_results != null && folder_results != '' && folder_results != undefined) 
			{
				var folderId = folder_results[0].getId();
				
			}
			else
			{
				var o_folder_obj = nlapiCreateRecord('folder')
				o_folder_obj.setFieldValue('name','Vendor_Payable_report')
				var folderId = nlapiSubmitRecord(o_folder_obj)
				
			}
			return folderId;
		
	
}



// BEGIN OBJECT CALLED/INVOKING FUNCTION ===================================================
{
	
}
// END OBJECT CALLED/INVOKING FUNCTION =====================================================
