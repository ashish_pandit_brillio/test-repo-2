/**
 * Validate all allocation, timesheet and projects before monthly provision
 * 
 * Version Date Author Remarks 1.00 19 Apr 2016 Nitish Mishra
 * 
 */  

 function scheduled(type) {
	nlapiLogExecution('DEBUG', 'START');
	var context = nlapiGetContext();
    var currentUser = context.getUser();
    var currentUserEmail = context.getEmail();
    var receipentsArray = new Array()
	receipentsArray.push(currentUserEmail);
   receipentsArray.push('netsuite.support@brillio.com');
   receipentsArray.push('billing@brillio.com');
	try {
		var provisionMasterId = getRunningProvisionMasterId();

		if (provisionMasterId) {
			var currentContext = nlapiGetContext();
			nlapiSubmitField('customrecord_provision_master',
			        provisionMasterId, [ 'custrecord_pm_status',
			                'custrecord_pm_percent' ], [ 'Clearing Old Logs',
			                '0%' ]);

			var provisionMasterRecord = nlapiLookupField(
			        'customrecord_provision_master', provisionMasterId, [
			                'custrecord_pm_from_date', 'custrecord_pm_to_date',
			                'custrecord_pm_subsidiary' ]);

			nlapiSubmitField('customrecord_provision_master',
			        provisionMasterId, [ 'custrecord_pm_status',
			                'custrecord_pm_percent' ], [ 'Clearing old logs',
			                '0%' ]);

			// clear all previous logs
			clearErrorLogs(provisionMasterId);
			delete clearErrorLogs;
			yieldScript(currentContext);

			nlapiSubmitField('customrecord_provision_master',
			        provisionMasterId, [ 'custrecord_pm_status',
			                'custrecord_pm_percent' ], [ 'In Progress', '10%' ]);

			// check all the inactive dependent records
			nlapiLogExecution('debug', 'inactive dependent record check',
			        'start');
			checkInactiveRecords(provisionMasterId,
			        provisionMasterRecord.custrecord_pm_from_date,
			        provisionMasterRecord.custrecord_pm_to_date,
			        provisionMasterRecord.custrecord_pm_subsidiary);
			delete checkInactiveRecords;
			nlapiSubmitField('customrecord_provision_master',
			        provisionMasterId, [ 'custrecord_pm_status',
			                'custrecord_pm_percent' ], [ 'In Progress', '30%' ]);
			nlapiLogExecution('debug', 'inactive dependent record check', 'end');
			yieldScript(currentContext);

			// Validate Over / Under Allocation
			nlapiLogExecution('debug', 'proper allocation check', 'start');
			var data = checkForProperAllocation(provisionMasterId,
			        provisionMasterRecord.custrecord_pm_from_date,
			        provisionMasterRecord.custrecord_pm_to_date,
			        provisionMasterRecord.custrecord_pm_subsidiary);
			nlapiSubmitField('customrecord_provision_master',
			        provisionMasterId, [ 'custrecord_pm_status',
			                'custrecord_pm_percent' ], [ 'In Progress', '60%' ]);
			nlapiLogExecution('debug', 'proper allocation check', 'end');
			yieldScript(currentContext);

			// Validate Time Entry
			nlapiLogExecution('debug', 'time entry validation', 'start');
			validateTimeEntry(provisionMasterId,
			        provisionMasterRecord.custrecord_pm_from_date,
			        provisionMasterRecord.custrecord_pm_to_date,
			        provisionMasterRecord.custrecord_pm_subsidiary,
			        data.AllocationMaster, data.EmployeeList);
			nlapiSubmitField('customrecord_provision_master',
			        provisionMasterId, [ 'custrecord_pm_status',
			                'custrecord_pm_percent' ], [ 'In Progress', '80%' ]);
			nlapiLogExecution('debug', 'time entry validation', 'end');
			delete validateTimeEntry;
			delete checkForProperAllocation;
			delete data;
			yieldScript(currentContext);

			// Validate Monthly Rate / Bill Rate
			nlapiLogExecution('debug', 'bill rate check', 'start');
			checkAllocationRates(provisionMasterId,
			        provisionMasterRecord.custrecord_pm_from_date,
			        provisionMasterRecord.custrecord_pm_to_date,
			        provisionMasterRecord.custrecord_pm_subsidiary);
			delete checkAllocationRates;

			nlapiSubmitField('customrecord_provision_master',
			        provisionMasterId, [ 'custrecord_pm_validation_completed',
			                'custrecord_pm_status', 'custrecord_pm_percent' ],
			        [ 'T', 'Completed', '100%' ]);
			nlapiLogExecution('debug', 'bill rate check', 'end');

		} else {
			nlapiLogExecution('ERROR', 'No Active Provision Master');
		}
	} catch (err) {
		nlapiLogExecution('ERROR', 'scheduled', err);
        var mailTemplate = "";
        var emailBody = err
        mailTemplate += "<p> This is to inform that the below errror occurred while running Provision Validation</p>";
        mailTemplate += "<br/>"
        mailTemplate += emailBody
        mailTemplate += "<p>Regards, <br/> Information Systems</p>";
        nlapiSendEmail( '442', receipentsArray, 'Error in Provision Validation',
        mailTemplate);
	}
	nlapiLogExecution('DEBUG', 'END');
}

function checkInactiveRecords(provisionMasterId, startDate, endDate, subsidiary)
{
	try {
		var currentContext = nlapiGetContext();

		// get all the allocations between the specified period
		var allocationSearch = searchRecord('resourceallocation', null,
		        [
		                new nlobjSearchFilter('jobbillingtype', 'job', 'anyof',
		                        'TM'),
		                new nlobjSearchFilter('startdate', null, 'notafter',
		                        endDate),
		                new nlobjSearchFilter('enddate', null, 'notbefore',
		                        startDate),
		                new nlobjSearchFilter('subsidiary', 'job', 'anyof',
		                        subsidiary),
		                new nlobjSearchFilter('custeventrbillable', null, 'is',
		                        'T') ], [ new nlobjSearchColumn('company'),
		                new nlobjSearchColumn('resource'),
		                new nlobjSearchColumn('custentity_vertical', 'job'),
		                new nlobjSearchColumn('custentity_location', 'job'),
		                new nlobjSearchColumn('customer', 'job'),
		                new nlobjSearchColumn('department', 'employee'), ]);

		var employeeList = [], projectList = [], locationList = [];
		var verticalList = [], practiceList = [], allocationList = [], customerList = [];

		if (allocationSearch) {

			allocationSearch.forEach(function(allocation) {
				employeeList.push(allocation.getValue('resource'));
				projectList.push(allocation.getValue('company'));
				verticalList.push(allocation.getValue('custentity_vertical',
				        'job'));
				locationList.push(allocation.getValue('custentity_location',
				        'job'));
				customerList.push(allocation.getValue('customer', 'job'));
				practiceList
				        .push(allocation.getValue('department', 'employee'));
				allocationList.push(allocation.getId());
			});
			delete allocationSearch;
			yieldScript(currentContext);

			// check if all the employees are system active
			employeeList = _.uniq(employeeList);
			var inactiveRecordSearch = searchRecord('employee', null, [
			        new nlobjSearchFilter('isinactive', null, 'is', 'T'),
			        new nlobjSearchFilter('internalid', null, 'anyof',
			                employeeList) ],
			        [ new nlobjSearchColumn('entityid') ]);

			if (inactiveRecordSearch) {

				for (var i = 0; i < inactiveRecordSearch.length; i++) {
					var link = nlapiResolveURL('RECORD', 'employee',
					        inactiveRecordSearch[i].getId(), 'VIEW');

					createErrorLog(provisionMasterId, "Employee : "
					        + inactiveRecordSearch[i].getValue('entityid')
					        + " is inactive", link);

					yieldScript(currentContext);
				}
			}
			delete employeeList;
			delete inactiveRecordSearch;

			// check if all projects are active
			projectList = _.uniq(projectList);
			inactiveRecordSearch = searchRecord('job', null, [
			        new nlobjSearchFilter('isinactive', null, 'is', 'T'),
			        new nlobjSearchFilter('internalid', null, 'anyof',
			                projectList) ],
			        [ new nlobjSearchColumn('entityid') ]);

			if (inactiveRecordSearch) {

				for (var i = 0; i < inactiveRecordSearch.length; i++) {
					var link = nlapiResolveURL('RECORD', 'job',
					        inactiveRecordSearch[i].getId(), 'VIEW');
					createErrorLog(provisionMasterId, "Project : "
					        + inactiveRecordSearch[i].getValue('entityid')
					        + " is inactive", link);
					yieldScript(currentContext);
				}
			}
			delete projectList;
			delete inactiveRecordSearch;

			// check if all customer are active
			customerList = _.uniq(customerList);
			inactiveCustomerSearch = searchRecord('customer', null, [
			        new nlobjSearchFilter('isinactive', null, 'is', 'T'),
			        new nlobjSearchFilter('internalid', null, 'anyof',
			                customerList) ],
			        [ new nlobjSearchColumn('entityid') ]);

			if (inactiveRecordSearch) {

				for (var i = 0; i < inactiveRecordSearch.length; i++) {
					var link = nlapiResolveURL('RECORD', 'customer',
					        inactiveRecordSearch[i].getId(), 'VIEW');
					createErrorLog(provisionMasterId, "Customer : "
					        + inactiveRecordSearch[i].getValue('entityid')
					        + " is inactive", link);
					yieldScript(currentContext);
				}
			}
			delete customerList;
			delete inactiveRecordSearch;

			// check if all the practices are active
			practiceList = _.uniq(practiceList);
			inactiveRecordSearch = searchRecord('department', null, [
			        new nlobjSearchFilter('isinactive', null, 'is', 'T'),
			        new nlobjSearchFilter('internalid', null, 'anyof',
			                practiceList) ], [ new nlobjSearchColumn('name') ]);

			if (inactiveRecordSearch) {

				for (var i = 0; i < inactiveRecordSearch.length; i++) {
					var link = nlapiResolveURL('RECORD', 'department',
					        inactiveRecordSearch[i].getId(), 'VIEW');

					// should list all tagged employees / allocation
					createErrorLog(provisionMasterId, "Practice : "
					        + inactiveRecordSearch[i].getValue('name')
					        + " is inactive", link);
					yieldScript(currentContext);
				}
			}
			delete practiceList;
			delete inactiveRecordSearch;

			// check if all the verticals are active
			verticalList = _.uniq(verticalList);
			inactiveRecordSearch = searchRecord('classification', null, [
			        new nlobjSearchFilter('isinactive', null, 'is', 'T'),
			        new nlobjSearchFilter('internalid', null, 'anyof',
			                verticalList) ], [ new nlobjSearchColumn('name') ]);

			if (inactiveRecordSearch) {

				for (var i = 0; i < inactiveRecordSearch.length; i++) {
					var link = nlapiResolveURL('RECORD', 'classification',
					        inactiveRecordSearch[i].getId(), 'VIEW');

					// should list all tagged project / allocation
					createErrorLog(provisionMasterId, "Vertical : "
					        + inactiveRecordSearch[i].getValue('name')
					        + " is inactive", link);
					yieldScript(currentContext);
				}
			}
			delete verticalList;
			delete inactiveRecordSearch;

			// check if all the regions are active
			locationList = _.uniq(locationList);
			inactiveRecordSearch = searchRecord('location', null, [
			        new nlobjSearchFilter('isinactive', null, 'is', 'T'),
			        new nlobjSearchFilter('internalid', null, 'anyof',
			                locationList) ], [ new nlobjSearchColumn('name') ]);

			if (inactiveRecordSearch) {

				for (var i = 0; i < inactiveRecordSearch.length; i++) {
					var link = nlapiResolveURL('RECORD', 'location',
					        inactiveRecordSearch[i].getId(), 'VIEW');

					// should list all tagged project / allocation
					createErrorLog(provisionMasterId, "Location : "
					        + inactiveRecordSearch[i].getValue('name')
					        + " is inactive", link);
					yieldScript(currentContext);
				}
			}
			delete locationList;
			delete inactiveRecordSearch;
		}
	} catch (err) {
		nlapiLogExecution('ERROR', 'checkInactiveRecords', err);
		throw err;
	}
}

function checkAllocationRates(provisionMasterId, startDate, endDate, subsidiary)
{
	try {
		var currentContext = nlapiGetContext();
		yieldScript(currentContext);
		var allocationSearch = searchRecord(
		        'resourceallocation',
		        null,
		        [
		                [ 'job.jobbillingtype', 'anyof', 'TM' ],
		                'and',
		                [ 'startdate', 'notafter', endDate ],
		                'and',
		                [ 'enddate', 'notbefore', startDate ],
		                'and',
		                [ 'job.subsidiary', 'anyof', subsidiary ],
		                'and',
		                [ 'custeventrbillable', 'is', 'T' ],
		                'and',
		                [
		                        [
		                                [ 'job.custentity_t_and_m_monthly',
		                                        'is', 'F' ], 'and',
		                                [ 'custevent3', 'notgreaterthan', 0 ] ],
		                        'or',
		                        [
		                                [ 'job.custentity_t_and_m_monthly',
		                                        'is', 'T' ],
		                                'and',
		                                [ 'custevent_monthly_rate',
		                                        'notgreaterthan', 0 ] ] ] ], [
		                new nlobjSearchColumn('company'),
		                new nlobjSearchColumn('resource'),
		                new nlobjSearchColumn('startdate'),
		                new nlobjSearchColumn('enddate'),
		                new nlobjSearchColumn('custentity_t_and_m_monthly',
		                        'job') ]);

		if (allocationSearch) {

			for (var i = 0; i < allocationSearch.length; i++) {
				var isMonthly = allocationSearch[i].getValue(
				        'custentity_t_and_m_monthly', 'job') == 'T';

				var errorMessage = "Allocation : "
				        + allocationSearch[i].getText('resource') + ", "
				        + allocationSearch[i].getText('company') + ", "
				        + allocationSearch[i].getValue('startdate') + " - "
				        + allocationSearch[i].getValue('enddate')
				        + " is missing ";

				if (isMonthly) {
					errorMessage += "monthly rate.";
				} else {
					errorMessage += "bill rate.";
				}

				var link = nlapiResolveURL('RECORD', 'resourceallocation',
				        allocationSearch[i].getId(), 'VIEW');

				createErrorLog(provisionMasterId, errorMessage, link);
				yieldScript(currentContext);
			}
		}

		delete allocationSearch;
	} catch (err) {
		nlapiLogExecution('ERROR', 'checkAllocationRates', err);
		throw err;
	}
}

function test(request, response) {
	var provisionMasterId = 7;
	var provisionMasterRecord = nlapiLookupField(
	        'customrecord_provision_master', provisionMasterId, [
	                'custrecord_pm_from_date', 'custrecord_pm_to_date',
	                'custrecord_pm_subsidiary' ]);
	var data = checkForProperAllocation(provisionMasterId,
	        provisionMasterRecord.custrecord_pm_from_date,
	        provisionMasterRecord.custrecord_pm_to_date,
	        provisionMasterRecord.custrecord_pm_subsidiary);

	validateTimeEntry(provisionMasterId,
	        provisionMasterRecord.custrecord_pm_from_date,
	        provisionMasterRecord.custrecord_pm_to_date,
	        provisionMasterRecord.custrecord_pm_subsidiary,
	        data.AllocationMaster, data.EmployeeList);
}

function checkForProperAllocation(provisionMasterId, provisionStartDate,
        provisionEndDate, subsidiary)
{
	try {
		var currentContext = nlapiGetContext();
		yieldScript(currentContext);
		var d_provisionStartDate = nlapiStringToDate(provisionStartDate);
		var d_provisionEndDate = nlapiStringToDate(provisionEndDate);

		// search all the allocation active during the specified period and
		// belonging to the subsidiary
		var filter1 = new nlobjSearchFilter('formuladate', null, 'notafter',
		        provisionEndDate);
		filter1.setFormula('{startdate}');

		var filter2 = new nlobjSearchFilter('formuladate', null, 'notbefore',
		        provisionStartDate);
		filter2.setFormula('{enddate}');

		var allocationSearch = searchRecord('resourceallocation', null,
		        [
		                // new nlobjSearchFilter('resource', null, 'anyof',
		                // '10012'),
		                filter1,
		                filter2,
		                new nlobjSearchFilter('jobbillingtype', 'job', 'anyof',
		                        'TM'),
		                new nlobjSearchFilter('subsidiary', 'job', 'anyof',
		                        subsidiary),
		                new nlobjSearchFilter('custeventrbillable', null, 'is',
		                        'T') ], [ new nlobjSearchColumn('company'),
		                new nlobjSearchColumn('resource'),
		                new nlobjSearchColumn('startdate'),
		                new nlobjSearchColumn('endDate'),
		                new nlobjSearchColumn('percentoftime') ]);

		var employeeList = [];
		if (allocationSearch) {
			var allocationCount = allocationSearch.length;

			// get employee list
			for (var i = 0; i < allocationCount; i++) {
				employeeList.push(allocationSearch[i].getValue('resource'));
			}
			employeeList = _.uniq(employeeList);
		}

		var allocationSearch = searchRecord('resourceallocation', null, [
		        new nlobjSearchFilter('resource', null, 'anyof', employeeList),
		        filter1, filter2 ], [ new nlobjSearchColumn('company'),
		        new nlobjSearchColumn('resource'),
		        new nlobjSearchColumn('startdate'),
		        new nlobjSearchColumn('endDate'),
		        new nlobjSearchColumn('percentoftime') ]);

		var allocationMaster = [];
		var employeeList = [];

		if (allocationSearch) {
			var allocationCount = allocationSearch.length;
			nlapiLogExecution('debug', 'allocation count', allocationCount);
			var totalProvisionDay = getDayDifference(provisionStartDate,
			        provisionEndDate) + 1;
			nlapiLogExecution('debug', 'totalProvisionDay', totalProvisionDay);

			// get employee list
			for (var i = 0; i < allocationCount; i++) {
				employeeList.push(allocationSearch[i].getValue('resource'));
			}
			employeeList = _.uniq(employeeList);
			nlapiLogExecution('debug', 'employeeList count',
			        employeeList.length);

			// push all in master allocation list
			for (var i = 0; i < employeeList.length; i++) {
				var entry = {
				    Employee : employeeList[i],
				    Day : []
				};

				// insert an entry for each day of provision
				for (var j = 0; j < totalProvisionDay; j++) {
					entry.Day.push({
					    State : 0,
					    Sum : 0,
					    Project : []
					});
				}

				allocationMaster.push(entry);
			}
			nlapiLogExecution('debug', 'initial entry allocationMaster count',
			        allocationMaster.length);
			// delete employeeList;

			// add hire and termination date check
			// add a day wise allocation entry
			for (var i = 0; i < allocationMaster.length; i++) {
				var employeeId = allocationMaster[i].Employee;
				var employeeDetails = nlapiLookupField('employee', employeeId,
				        [ 'hiredate', 'custentity_lwd' ]);
				var hireDate = employeeDetails.hiredate;
				var terminationDate = employeeDetails.custentity_lwd;
				var d_hireDate = nlapiStringToDate(hireDate);
				terminationDate = terminationDate ? terminationDate
				        : provisionEndDate;
				var d_terminationDate = nlapiStringToDate(terminationDate);

				// change all days between allocation start and end date
				// as allocated and add the percent to the sum of those
				// days
				var d_loopStartDate = d_hireDate > d_provisionStartDate ? d_hireDate
				        : d_provisionStartDate;
				var d_loopEndDate = d_terminationDate < d_provisionEndDate ? d_terminationDate
				        : d_provisionEndDate;
				var loopStartDate = nlapiDateToString(d_loopStartDate, 'date');
				var loopEndDate = nlapiDateToString(d_loopEndDate, 'date');
				var loopStartIndex = getDayDifference(provisionStartDate,
				        loopStartDate);
				var loopEndIndex = getDayDifference(provisionStartDate,
				        loopEndDate);

				nlapiLogExecution('debug', 'loop start date', loopStartDate
				        + " index " + loopStartIndex);
				nlapiLogExecution('debug', 'loop end date', loopEndDate
				        + " index " + loopEndIndex);

				// marking days as active
				for (var k = loopStartIndex; k <= loopEndIndex; k++) {
					allocationMaster[i].Day[k].State = 1;
				}
			}

			// add a day wise allocation entry
			for (var i = 0; i < allocationMaster.length; i++) {
				var employeeId = allocationMaster[i].Employee;

				// loop and get this employees allocation
				for (var j = 0; j < allocationCount; j++) {

					// allocation found
					if (employeeId == allocationSearch[j].getValue('resource')) {
						var allocationStartDate = allocationSearch[j]
						        .getValue('startdate');
						var d_allocationStartDate = nlapiStringToDate(allocationStartDate);
						nlapiLogExecution('debug', 'allocationStartDate',
						        allocationStartDate);
						var allocationEndDate = allocationSearch[j]
						        .getValue('enddate');
						var d_allocationEndDate = nlapiStringToDate(allocationEndDate);
						nlapiLogExecution('debug', 'allocationEndDate',
						        allocationEndDate);
						var allocatedProject = allocationSearch[j]
						        .getValue('company');
						var allocatedPercent = parseFloat(allocationSearch[j]
						        .getValue('percentoftime').split("%")[0]);

						// change all days between allocation start and end date
						// as allocated and add the percent to the sum of those
						// days
						var d_loopStartDate = d_allocationStartDate > d_provisionStartDate ? d_allocationStartDate
						        : d_provisionStartDate;
						var d_loopEndDate = d_allocationEndDate < d_provisionEndDate ? d_allocationEndDate
						        : d_provisionEndDate;
						var loopStartDate = nlapiDateToString(d_loopStartDate,
						        'date');
						var loopEndDate = nlapiDateToString(d_loopEndDate,
						        'date');
						var loopStartIndex = getDayDifference(
						        provisionStartDate, loopStartDate);
						var loopEndIndex = getDayDifference(provisionStartDate,
						        loopEndDate);

						nlapiLogExecution('debug', 'loop start date',
						        loopStartDate + " index " + loopStartIndex);
						nlapiLogExecution('debug', 'loop end date', loopEndDate
						        + " index " + loopEndIndex);
						// marking days as allocated
						for (var k = loopStartIndex; k <= loopEndIndex; k++) {
							allocationMaster[i].Day[k].State = 2;
							allocationMaster[i].Day[k].Sum += allocatedPercent;
							allocationMaster[i].Day[k].Project
							        .push(allocatedProject);
						}
					}
				}
			}

			// nlapiLogExecution('DEBUG', 'allocationMaster', JSON
			// .stringify(allocationMaster));

			// check for over / under allocation
			for (var i = 0; i < allocationMaster.length; i++) {
				var employeeId = allocationMaster[i].Employee;
				var employeeName = nlapiLookupField('employee', employeeId,
				        'entityid');

				for (var j = 0; j < allocationMaster[i].Day.length; j++) {

					if ((allocationMaster[i].Day[j].State == 2 && allocationMaster[i].Day[j].Sum != 100)
					        || allocationMaster[i].Day[j].State == 1) {
						nlapiLogExecution('DEBUG', 'Employee ' + employeeName
						        + ' not allocated properly at ' + j, JSON
						        .stringify(allocationMaster[i]));
						createErrorLog(provisionMasterId, "Employee : "
						        + employeeName + " not allocated properly",
						        null);
						yieldScript(currentContext);
						break;
					}
				}
			}
		} // allocation null check

		delete allocationSearch;

		return {
		    AllocationMaster : allocationMaster,
		    EmployeeList : employeeList
		};
	} catch (err) {
		nlapiLogExecution('ERROR', 'checkForProperAllocation', err);
		throw err;
	}
}

function validateTimeEntry(provisionMasterId, provisionStartDate,
        provisionEndDate, subsidiary, allocationMaster, employeeMaster)
{
	try {
		var currentContext = nlapiGetContext();
		var d_provisionStartDate = nlapiStringToDate(provisionStartDate);
		var d_provisionEndDate = nlapiStringToDate(provisionEndDate);

		// get all the time entries between the given time period
		var timeEntrySearch = searchRecord('timebill', null,
		        [
		                new nlobjSearchFilter('employee', null, 'anyof',
		                        employeeMaster),
		                new nlobjSearchFilter('type', null, 'anyof', 'A'),
		                new nlobjSearchFilter('date', null, 'within',
		                        provisionStartDate, provisionEndDate) ], [
		                new nlobjSearchColumn('employee'),
		                new nlobjSearchColumn('date'),
		                new nlobjSearchColumn('customer') ]);

		if (timeEntrySearch) {

			for (var i = 0; i < timeEntrySearch.length; i++) {
				var employeeId = timeEntrySearch[i].getValue('employee');
				var timeEntryDate = timeEntrySearch[i].getValue('date');
				var projectId = timeEntrySearch[i].getValue('customer');
				var dayDiff = getDayDifference(provisionStartDate,
				        timeEntryDate);

				for (var j = 0; j < allocationMaster.length; j++) {

					if (allocationMaster[j].Employee == employeeId) {
						// var allocatedProjectList =
						// allocationMaster[j].Day.Project;
						var projectIndex = _.indexOf(
						        allocationMaster[j].Day[dayDiff].Project,
						        projectId);

						if (projectIndex == -1) {
							createErrorLog(provisionMasterId, "Employee : "
							        + timeEntrySearch[i].getText('employee')
							        + " has wrong time entry for date : "
							        + timeEntryDate, null);
							nlapiLogExecution('debug', 'Wrong Timesheet Entry',
							        'employeeId : ' + employeeId + " date : "
							                + timeEntryDate);
							yieldScript(currentContext);
						}
						break;
					}
				}

			}
		}
	} catch (err) {
		nlapiLogExecution('ERROR', 'validateTimeEntry', err);
		throw err;
	}
}

function createErrorLog(provisionMasterId, errorComment, link) {
	try {
		var rec = nlapiCreateRecord('customrecord_provision_error_log');
		rec.setFieldValue('custrecord_pel_provision_master', provisionMasterId);
		rec.setFieldValue('custrecord_pel_error_comment', errorComment);

		if (link) {
			rec.setFieldValue('custrecord_pel_record_link', link);
		}
		var id = nlapiSubmitRecord(rec);
		nlapiLogExecution('DEBUG', 'Error Log Created', id);
	} catch (err) {
		nlapiLogExecution('ERROR', 'createErrorLog', err);
		throw err;
	}
}

function clearErrorLogs(provisionMasterId) {
	try {
		nlapiLogExecution('DEBUG', 'Clearing Error Log', 'Start');
		var logSearch = searchRecord('customrecord_provision_error_log', null,
		        [
		                new nlobjSearchFilter('isinactive', null, 'is', 'F'),
		                new nlobjSearchFilter(
		                        'custrecord_pel_provision_master', null,
		                        'anyof', provisionMasterId) ]);

		if (logSearch) {

			for (var i = 0; i < logSearch.length; i++) {
				nlapiSubmitField('customrecord_provision_error_log',
				        logSearch[i].getId(), 'isinactive', 'T');
			}
		}

		delete logSearch;
		delete i;
		nlapiLogExecution('DEBUG', 'Clearing Error Log', 'End');
	} catch (err) {
		nlapiLogExecution('ERROR', 'clearErrorLogs', err);
		throw err;
	}
}

function yieldScript(currentContext) {

	if (currentContext.getRemainingUsage() <= 100) {
		nlapiLogExecution('AUDIT', 'API Limit Exceeded');
		var state = nlapiYieldScript();

		if (state.status == "FAILURE") {
			nlapiLogExecution('ERROR', 'Yield Script Failed', 'Reason : '
			        + state.reason + ' / Size : ' + state.size);
			return false;
		} else if (state.status == "RESUME") {
			nlapiLogExecution('AUDIT', 'Script Resumed');
		}
	}
}

function getRunningProvisionMasterId() {
	try {
		// get the currently active monthly provision
		var provisionMasterSearch = nlapiSearchRecord(
		        'customrecord_provision_master', null, [
		                new nlobjSearchFilter('isinactive', null, 'is', 'F'),
		                new nlobjSearchFilter(
		                        'custrecord_pm_is_currently_active', null,
		                        'is', 'T') ]);

		var provisionMasterId = null;
		if (provisionMasterSearch) {
			provisionMasterId = provisionMasterSearch[0].getId();
		}

		return provisionMasterId;
	} catch (err) {
		nlapiLogExecution('ERROR', 'getRunningProvisionMasterId', err);
		throw err;
	}
}

function getDayDifference(startDate, endDate) {
	try {
		var d_startDate = nlapiStringToDate(startDate);
		var d_endDate = nlapiStringToDate(endDate);
		var count = 0;
		for (;;) {
			var newDate = nlapiAddDays(d_startDate, count);

			if (newDate >= d_endDate) {
				break;
			}

			count += 1;
		}

		return count;
	} catch (err) {
		nlapiLogExecution('ERROR', 'getDayDifference', err);
		throw err;
	}
}