/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       02 Jul 2015     nitish.mishra
 *
 */

/**
 * @param {String}
 *            type Context Types: scheduled, ondemand, userinterface, aborted,
 *            skipped
 * @returns {Void}
 */
function scheduled(type) {

}

function massUpdate(recType, recId) {
	try {

		var rec = nlapiLoadRecord(recType, recId, {
			recordmode : 'dynamic'
		});
		rec.setFieldValue('custevent_otpayable', 'T');
		rec.setFieldValue('custevent_otbillable', 'T');
		rec.setFieldValue('custevent_otrate', 0.01);
		nlapiSubmitRecord(rec, false, true);		
	} catch (err) {
		nlapiLogExecution('error', 'massUpdate', err);
	}
}
