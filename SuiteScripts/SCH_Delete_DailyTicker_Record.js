function deleteRecord()
{
	try
	{
		var search = nlapiLoadSearch('customrecord_daily_emp_head_count_detail','customsearch_daily_ticker_record_delete');
		var count = 0;
		var filters = search.getFilters();
		var columns = search.getColumns();
		
		 var search_results = searchRecord('customrecord_daily_emp_head_count_detail', null, filters, columns);
      nlapiLogExecution('Debug','Search Result Length',search_results.length);
		if(search_results)
		{
			for(var i = 0; i < search_results.length; i++)
			{
				nlapiYieldScript();
				var id = search_results[i].getId();
				nlapiDeleteRecord('customrecord_daily_emp_head_count_detail',id);
              	count++
				
			}
          nlapiLogExecution('Debug','Records deleted ->',count);
		}
	}
	catch (err)
	{
		nlapiLogExecution('Debug','Error in deleting record',err);
	}
}

function yieldScript(currentContext) {

	if (currentContext.getRemainingUsage() <= 2000) {
		nlapiLogExecution('AUDIT', 'API Limit Exceeded');
		var state = nlapiYieldScript();

		if (state.status == "FAILURE") {
			nlapiLogExecution('ERROR', 'Yield Script Failed', 'Reason : '
			        + state.reason + ' / Size : ' + state.size);
			return false;
		} else if (state.status == "RESUME") {
			nlapiLogExecution('AUDIT', 'Script Resumed');
		}
	}
}