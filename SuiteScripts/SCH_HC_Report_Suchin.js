/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       17 Sept 2018    shamanth.k
 *
 */

/**
 * @param {String} type Context Types: scheduled, ondemand, userinterface, aborted, skipped
 * @returns {Void}
 */
function scheduled(type) {
    try {
        var context = nlapiGetContext();
        var current_date = new Date();
        var a_data = searchTransactions(current_date);

        var excel_result = createExcel(a_data);

        //Returns File ID
        var newAttachment = nlapiLoadFile(excel_result);
        // var a_emp_attachment = new Array();
        //a_emp_attachment['entity'] = 97260;
        var emp_email = ['suchin.bhat@brillio.com'];
        nlapiSendEmail(442, emp_email, 'Verizon & Honeywell HC Report as on ' + nlapiDateToString(new Date(), 'date'), 'Please find the attachement of head count report', null, 'shamanth.k@brillio.com', null, newAttachment);
        nlapiLogExecution('DEBUG', 'Mail Sent to -> ', emp_email);
    }
    catch (err) {
        nlapiLogExecution('DEBUG', 'Process Error - Main', err);
        Send_Exception_Mail(err);
        
    
    }
}
//Create Excel logic
function createExcel(a_results) {
    try {
        /*var strVar1= '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:ss="urn:schemas-microsoft-com:office:spreadsheet" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40">'+
        '<head>'+
        '<meta http-equiv=Content-Type content="text/html; charset=windows-1252"/>'+
                '<meta name=ProgId content=Excel.Sheet/>'+
                '<meta name=Generator content="Microsoft Excel 11"/>'+
                '<!--[if gte mso 9]><xml>'+
                '<x:excelworkbook>'+
                '<x:excelworksheets>'+
                '<x:excelworksheet=sheet1>'+
                '<x:name>** ESTIMATE FILE**</x:name>'+
                '<x:worksheetoptions>'+
                '<x:selected></x:selected>'+
                '<x:freezepanes></x:freezepanes>'+
                '<x:frozennosplit></x:frozennosplit>'+
                '<x:splithorizontal>** FROZEN ROWS + 1 **</x:splithorizontal>'+
                '<x:toprowbottompane>** FROZEN ROWS + 1 **</x:toprowbottompane>'+
                '<x:splitvertical>** FROZEN COLUMNS + 1 **</x:splitvertical>'+
                '<x:leftcolumnrightpane>** FROZEN COLUMNS + 1**</x:leftcolumnrightpane>'+
            '<x:activepane>0</x:activepane>'+// 0
            '<x:panes>'+
        '<x:pane>'+
        '<x:number>3</x:number>'+
        '</x:pane>'+
        '<x:pane>'+
              '<x:number>1</x:number>'+
            '</x:pane>'+
            '<x:pane>'+
        '<x:number>2</x:number>'+
                '</x:pane>'+
                '<x:pane>'+
                '<x:number>0</x:number>'+//1
                '</x:pane>'+
                '</x:panes>'+
                '<x:protectcontents>False</x:protectcontents>'+
                '<x:protectobjects>False</x:protectobjects>'+
                '<x:protectscenarios>False</x:protectscenarios>'+
                '</x:worksheetoptions>'+
                '</x:excelworksheet>'+
        '</x:excelworksheets>'+
                '<x:protectstructure>False</x:protectstructure>'+
                '<x:protectwindows>False</x:protectwindows>'+
        '</x:excelworkbook>'+
                
                // '<style>'+
        //-------------------------------------
                '</x:excelworkbook>'+
                '</xml><![endif]-->'+
                 '<style>'+
        'p.MsoFooter, li.MsoFooter, div.MsoFooter'+
        '{ margin:0in; margin-bottom:.0001pt; mso-pagination:widow-orphan; tab-stops:center 3.0in right 6.0in; font-size:12.0pt;}'+
        '<style>'+

        '<!-- /* Style Definitions */

        //'@page Section1'+
        //'{ size:8.5in 11.0in; margin:1.9cm 1.27cm 1.9cm 1.27cm; mso-header-margin:.5in; mso-footer-margin:.5in; mso-title-page:yes; mso-header: h1; mso-footer: f1; mso-first-header: fh1; mso-first-footer: ff1; mso-paper-source:0;}'+

        /*'div.Section1'+
        '{ page:Section1;}'+

        'table#hrdftrtbl'+
        '{ margin:0in 0in 0in 900in; width:1px; height:1px; overflow:hidden; font-family:Arial;}-->'+

        '</style>'+
        '</head>'+

        '<body>'+
        '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">'*/
        var current_date = nlapiDateToString(new Date());
        var strVar_excel = '';
        var html = "";

        html += "Resource Name,";
        html += "Customer,";
        html += "Job Title,";
        html += "Person Type,";
        html += "Employee Type,";
        html += "Project,";
        //  html += "Project Task,";
        html += "Number of hours,";
        html += "Start Date,";
        html += "Percentage of time,";
        html += "Allocate by,";
        html += "Notes,";
        html += "End Date,";
        html += "Allocation Type,";
        html += "Billing Start Date,";
        html += "Billing End Date,";
        html += "Resource billable,";
        html += "Work Location,";
        html += "Practice,";
        html += "Service Item,";
        html += "Unit Cost,";
        html += "Bill Rate,";
        html += "Onsite/Offsite,";
        html += "OT Billable,";
        html += "OT Payable,";
        html += "OT Rate,";
        html += "Tenure Discount Available,";
        html += "Work City,";
        html += "Work State,";
        html += "Holiday Service Item,"; //
        html += "Leave Service Item,";
        html += "IS Shadow,";
        html += "30 days prior notification,";
        html += "Hire Date,";
        html += "Pay Rate,";
        html += "Vendor Name,"
        html += "\r\n";

        for (var k = 0; k < a_results.length; k++) {
            var temp = a_results[k];
            html += temp.Resource;
            html += ",";
            html += temp.Customer;
            html += ",";
            html += temp.Designation;
            html += ",";
            html += temp.Person_type;
            html += ",";
            html += temp.Employee_type;
            html += ",";
            html += temp.Project;
            html += ",";
            // html += temp.Project_task; //replace(/[^a-zA-Z0-9_]/g, ' '); 
            //html += ",";
            html += temp.Number_of_hours;
            html += ",";
            html += temp.start_date;
            html += ",";
            html += temp.percentage_of_time;
            html += ",";
            html += temp.allocate_by;
            html += ",";
            html += _logValidation(temp.notes) == false ? ' ' : temp.notes.replace(/[,.\t\n\r\f\v]/g, '');
            html += ",";
            html += temp.end_date;
            html += ",";
            html += temp.allocation_type;
            html += ",";
            html += _logValidation(temp.billing_start_date) == false ? ' ' : temp.billing_start_date;
            html += ",";
            html += _logValidation(temp.billing_end_date) == false ? ' ' : temp.billing_end_date.replace(/[,.\t\n\r\f\v]/g, '');
            html += ",";
            html += _logValidation(temp.resource_billable) == false ? ' ' : temp.resource_billable;
            html += ",";
            html += _logValidation(temp.work_location) == false ? ' ' : temp.work_location.replace(/[,.\t\n\r\f\v]/g, '');
            html += ",";
            html += _logValidation(temp.practice) == false ? ' ' : temp.practice.replace(/[,.\t\n\r\f\v]/g, '');
            html += ",";
            html += _logValidation(temp.service_item) == false ? ' ' : temp.service_item.replace(/[,.\t\n\r\f\v]/g, '');
            html += ",";
            html += _logValidation(temp.unit_cost) == false ? ' ' : temp.unit_cost.replace(/[,.\t\n\r\f\v]/g, '');
            html += ",";
            html += _logValidation(temp.bill_rate) == false ? ' ' : temp.bill_rate.replace(/[,.\t\n\r\f\v]/g, '');
            html += ",";
            html += _logValidation(temp.onsite_offsite) == false ? ' ' : temp.onsite_offsite.replace(/[,.\t\n\r\f\v]/g, '');
            html += ",";
            html += _logValidation(temp.ot_billable) == false ? ' ' : temp.ot_billable.replace(/[,.\t\n\r\f\v]/g, '');
            html += ",";
            html += _logValidation(temp.ot_payable) == false ? ' ' : temp.ot_payable.replace(/[,.\t\n\r\f\v]/g, '');
            html += ",";
            html += _logValidation(temp.ot_rate) == false ? ' ' : temp.ot_rate.replace(/[,.\t\n\r\f\v]/g, '');
            html += ",";
            html += _logValidation(temp.discount_applicable) == false ? ' ' : temp.discount_applicable.replace(/[,.\t\n\r\f\v]/g, '');
            html += ",";
            html += _logValidation(temp.work_city) == false ? ' ' : temp.work_city.replace(/[,.\t\n\r\f\v]/g, '');
            html += ",";
            html += _logValidation(temp.work_state) == false ? ' ' : temp.work_state.replace(/[,.\t\n\r\f\v]/g, '');
            html += ",";
            html += _logValidation(temp.holiday_item) == false ? ' ' : temp.holiday_item.replace(/[,.\t\n\r\f\v]/g, '');
            html += ",";
            html += _logValidation(temp.leave_item) == false ? ' ' : temp.leave_item.replace(/[,.\t\n\r\f\v]/g, '');
            html += ",";
            html += _logValidation(temp.is_shadow) == false ? ' ' : temp.is_shadow.replace(/[,.\t\n\r\f\v]/g, '');
            html += ",";
            html += _logValidation(temp.prior_notification) == false ? ' ' : temp.prior_notification.replace(/[,.\t\n\r\f\v]/g, '');
            html += ",";
            html += _logValidation(temp.Hire_date) == false ? ' ' : temp.Hire_date.replace(/[,.\t\n\r\f\v]/g, '');
            html += ",";
            html += temp.payrate;
            html += ",";
            html += temp.vendor_name;
            html += "\r\n";
        }

        /*strVar_excel += '</table>';
        strVar_excel += ' </td>';

        strVar_excel += '
        </tr>';
        strVar_excel += '</table>';*/
        var current_date = nlapiDateToString(new Date());
        //var _date = nlapiStringToDate(current_date);
        var fileName = 'Verizon and Honeywell Head Count Report ' + nlapiDateToString(new Date()) + ' .csv';

        var file = nlapiCreateFile(fileName, 'CSV', html);
        file.setFolder('354237'); //Prod
        var fileID = nlapiSubmitFile(file);
        nlapiLogExecution('debug', 'File ID', fileID);

        return fileID;
    } catch (err) {
        nlapiLogExecution('DEBUG', 'Excel creation error', err);
    }
}
//Search for transactions
function searchTransactions(d_date) {

    try {
        var context = nlapiGetContext();
        var search_ = nlapiLoadSearch('resourceallocation', 'customsearch2507');
        var filters = search_.getFilters();
        var columns = search_.getColumns();

        var o_data = new Array();
        //var columns = new Array();


        var search_results = searchRecord('resourceallocation', null, filters, [
            columns[0], columns[1], columns[2], columns[3], columns[4], columns[5],
            columns[6], columns[7], columns[8], columns[9], columns[10], columns[11], columns[12], columns[13], columns[14], columns[15],
            columns[16], columns[17], columns[18], columns[19], columns[20], columns[21], columns[22], columns[23], columns[24], columns[25],
            columns[26], columns[27], columns[28], columns[29], columns[30], columns[31], columns[32], columns[33]
        ]);

        if (search_results) {
            for (var i = 0; i < search_results.length; i++) { //search_results.length

                var employee = search_results[i].getValue(columns[0]);

                var pay_rate_search = searchPayRate(employee);

                var rate = _logValidation(pay_rate_search) ? pay_rate_search[0].getValue('custrecord_stvd_st_pay_rate', 'CUSTRECORD_STVD_CONTRACTOR', null) : ' ';

                var vendor = _logValidation(pay_rate_search) ? pay_rate_search[0].getText('custrecord_stvd_vendor', 'CUSTRECORD_STVD_CONTRACTOR', null) : ' ';

                o_data.push({
                    'Resource': search_results[i].getText(columns[0]), //2
                    'Customer': search_results[i].getText(columns[1]), //3
                    'Designation': search_results[i].getValue(columns[2]), //4
                    'Person_type': search_results[i].getText(columns[3]), //6
                    'Employee_type': search_results[i].getText(columns[4]), //7
                    'Project': search_results[i].getText(columns[5]), //8
                    //'Project_task':search_results[i].getValue(columns[6]), //9
                    'Number_of_hours': search_results[i].getValue(columns[7]), //10
                    'start_date': search_results[i].getValue(columns[8]), //11
                    'percentage_of_time': search_results[i].getValue(columns[9]), //12
                    'allocate_by': search_results[i].getText(columns[10]),
                    'notes': search_results[i].getValue(columns[11]),
                    'end_date': search_results[i].getValue(columns[12]),
                    'allocation_type': search_results[i].getText(columns[13]), //13
                    'billing_start_date': search_results[i].getValue(columns[14]), //14
                    'billing_end_date': search_results[i].getValue(columns[15]), //15
                    'resource_billable': search_results[i].getValue(columns[16]), //16
                    'work_location': search_results[i].getText(columns[17]), //17
                    'practice': search_results[i].getText(columns[18]), //18
                    'service_item': search_results[i].getValue(columns[19]), //19
                    'unit_cost': search_results[i].getValue(columns[20]), //20
                    'bill_rate': search_results[i].getValue(columns[21]), //21
                    'onsite_offsite': search_results[i].getText(columns[22]), //22
                    'ot_billable': search_results[i].getValue(columns[23]), //23
                    'ot_payable': search_results[i].getValue(columns[24]), //24
                    'ot_rate': search_results[i].getValue(columns[25]), //25
                    'discount_applicable': search_results[i].getValue(columns[26]), //26
                    'work_city': search_results[i].getValue(columns[27]), //27
                    'work_state': search_results[i].getText(columns[28]), //28
                    'holiday_item': search_results[i].getText(columns[29]), //29
                    'leave_item': search_results[i].getText(columns[30]), //30
                    'is_shadow': search_results[i].getValue(columns[31]), //31
                    'prior_notification': search_results[i].getValue(columns[32]), //32
                    'Hire_date': search_results[i].getValue(columns[33]),
                    'payrate': rate,
                    'vendor_name': vendor,

                });
            }
        }


        return o_data;
    } catch (ex) {
        nlapiLogExecution('DEBUG', 'Summary Error', ex);

    }
}


//Blank Values
function _logValidation(value) {
    if (value != 'null' && value != null && value != '- None -' && value != null && value != '' && value != undefined && value != undefined && value != 'undefined' && value != 'undefined' && value != 'NaN' && value != NaN) {
        return true;
    } else {
        return false;
    }
}


//Employee Level Search
function searchPayRate(emp) {
    try {

        var employeeSearch = nlapiSearchRecord("employee", null,
            [
                ["internalid", "anyof", emp],
                "AND",
                ["custentity_persontype", "anyof", "1"],
                "AND",
                ["custentity_employeetype", "anyof", "2", "6"],
                "AND",
                ["custrecord_stvd_contractor.custrecord_stvd_start_date", "notafter", "today"],
                "AND",
                ["custrecord_stvd_contractor.custrecord_stvd_end_date", "notonorbefore", "today"],
                "AND",
                ["custentity_employee_inactive", "is", "F"]
            ],
            [
                new nlobjSearchColumn("entityid").setSort(false),
                new nlobjSearchColumn("custentity_fusion_empid"),
                new nlobjSearchColumn("custrecord_stvd_st_pay_rate", "CUSTRECORD_STVD_CONTRACTOR", null),
                new nlobjSearchColumn("custrecord_stvd_vendor", "CUSTRECORD_STVD_CONTRACTOR", null)
            ]
        );
        return employeeSearch;
    }
    catch (err) {
        nlapiLogExecution('DEBUG', 'Pay Rate Error', err);
    }
}

function Send_Exception_Mail(err)
{​​​​​
    try
    {​​​​​   
        var context = nlapiGetContext();
        var s_DeploymentID =context.getDeploymentId();
        var s_ScriptID =context.getScriptId();
        var s_Subject ='Issue on Verizon & Honeywell HC Report';
        
        var s_Body = 'This is to inform that Verizon & Honeywell HC Report is having an issue and System is not able to send the details.';
        s_Body += '<br/><b>Issue: Code:</b> '+err.code+' Message: '+err.message;
        s_Body += '<br/><b>Script: </b>'+s_ScriptID;
        s_Body += '<br/><b>Deployment:</b> '+s_DeploymentID;
        s_Body += '<br/>Please have a look and do the needfull.';
        s_Body += '<br/><br/>Note: This is system genarated email incase of any failure while sending the notification to employees';

        s_Body += '<br/><br/>Regards,';
        s_Body += '<br/>Digital Office';
        nlapiSendEmail(442,"netsuite.support@brillio.com",s_Subject,s_Body,null, null, null, null);
    }​​​​​
    catch(err)
    {​​​​​
        nlapiLogExecution('error',' Send_Exception_Mail', err.message); 
		
    }​​​​​
}​​​​​




