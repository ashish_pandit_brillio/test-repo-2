/*
 * Client script for customer discount
 */

// Global Variable
var project_Id = '';
var total_inc_aftr_dsc = 0;
var original_invoice_total = 0;
var net_tenure_discount = 0;
var emp_name = '';
var emp_practice = '';
var customer_vertical = '';
var tenureDiscountReportDetails = [];

function page_Init_Custdesc_Desccal(type) //
{
	if (type == 'edit') {

		if (nlapiGetFieldValue('custbody_inv_is_discount_added') == 'T') {
			var i_Item_Line_Count = nlapiGetLineItemCount('item');

			for (var i_Counter_II = i_Item_Line_Count; i_Counter_II >= 1; i_Counter_II--) {

				if (nlapiGetLineItemValue('item', 'itemtype', i_Counter_II) == 'Discount') {
					nlapiRemoveLineItem('item', i_Counter_II);
				}
			}
		}
	}
}

var customerDiscountDetails = new CustomerDiscountObject();

function saveRecord_Custdesc_Desccal(type) //
{
	var wasDiscountAdded = false;
	var layout = nlapiGetFieldValue('custbody_layout_type');

	if (!layout) {
		alert("PDF layout not selected");
		return false;
	}

	/*
	 * On save record: - PURPOSE FIELDS USED: --Field Name-- --ID-- --Line Item
	 * Name--
	 */
	// LOCAL VARIABLES
	nlapiLogExecution('DEBUG', 'Start', '********* Execution Started ********');
	try //
	{
		var g_Inv_Amt_Sum = 0;
		var b_counter = false;
		var d_date;
		var n_Hours_Sum = 0;
		var i_Fixed_Discount_Rate_Sum = 0;
		var b_Annual_counter = false;
		var b_Quater_counter = false;
		var b_validation_count_t = false;
		var b_validation_count_v = false;
		var b_validation_count_f = false;
		var b_td_count = false;
		var emp_id = '';
		var total_inc = 0;
		var subtotal_exec = 0;
		var cumulative_sale = 0;
		// SAVE RECORD CODE BODY
		// ------------------------------------------------------------------------------------
		// -----------------------------------------------------------------------------------

		var rec_type = nlapiGetRecordType();
		// alert('rec_type'+rec_type);
		nlapiLogExecution('DEBUG', 'saveRecord_Custdesc_Desccal', 'rec_type : '
		        + rec_type);

		var i_Inv_Count = nlapiGetLineItemCount('item');
		// alert('i_Inv_Count'+i_Inv_Count);
		nlapiLogExecution('DEBUG', 'saveRecord_Custdesc_Desccal',
		        'i_Inv_Count : ' + i_Inv_Count);

		original_invoice_total = nlapiGetFieldValue('total');
		customerDiscountDetails.TotalAmount = original_invoice_total;

		var total_inc = nlapiGetFieldValue('total');
		nlapiLogExecution('DEBUG', 'saveRecord_Custdesc_Desccal',
		        'total_inc : ' + total_inc);

		total_inc_aftr_dsc = total_inc; // setting invoice total for cumulative
		// sales calculations.
		nlapiLogExecution('DEBUG', 'saveRecord_Custdesc_Desccal',
		        'total_inc_aftr_dsc : ' + total_inc_aftr_dsc);

		var subtotal_exec = nlapiGetFieldValue('subtotal');
		nlapiLogExecution('DEBUG', 'saveRecord_Custdesc_Desccal',
		        'subtotal_exec : ' + subtotal_exec);

		var project_rec_id = nlapiGetFieldValue('job');
		nlapiLogExecution('DEBUG', 'saveRecord_Custdesc_Desccal',
		        'project_rec_id : ' + project_rec_id);
		customerDiscountDetails.Project = project_rec_id;

		project_Id = project_rec_id;
		nlapiLogExecution('DEBUG', 'saveRecord_Custdesc_Desccal',
		        'project_Id : ' + project_Id);

		var pro_blank = validate(project_rec_id);
		nlapiLogExecution('DEBUG', 'saveRecord_Custdesc_Desccal',
		        'pro_blank : ' + pro_blank);

		// if (project_rec_id != null) // COMMENTED BY VIKRANT
		if (validate(project_rec_id)) //
		{// project id null check if start
			var project_load = nlapiLoadRecord('job', project_rec_id);

			var i_Project_Type = project_load.getFieldValue('jobbillingtype');
			nlapiLogExecution('DEBUG', 'saveRecord_Custdesc_Desccal',
			        'i_Project_Type : ' + i_Project_Type);

			if (i_Project_Type != 'TM') {
				nlapiLogExecution('DEBUG', 'Exit', 'Project is not T&M');
				return true;
			}

			var S_Customer_Id = nlapiGetFieldValue('entity');
			customerDiscountDetails.Customer = S_Customer_Id;
			nlapiLogExecution('DEBUG', 'saveRecord_Custdesc_Desccal',
			        'S_Customer_Id : ' + S_Customer_Id);

			// if (S_Customer_Id != '4297') {
			// nlapiLogExecution('DEBUG', 'Exit',
			// 'Customer is not Freddie Mac');
			// return true;
			// }

			var d_Project_Start_Date = project_load.getFieldValue('startdate');
			nlapiLogExecution('DEBUG', 'saveRecord_Custdesc_Desccal',
			        'd_Project_Start_Date : ' + d_Project_Start_Date);

			var d_Project_End_Date = project_load.getFieldValue('enddate');
			nlapiLogExecution('DEBUG', 'saveRecord_Custdesc_Desccal',
			        'd_Project_End_Date : ' + d_Project_End_Date);

			// check if group billing T&M project or not
			var res_filter = new Array();
			res_filter[0] = new nlobjSearchFilter('project', null, 'is',
			        project_rec_id);

			var res_column = new Array();
			res_column[0] = new nlobjSearchColumn('resource');
			res_column[1] = new nlobjSearchColumn('custevent1');

			var resource_search = nlapiSearchRecord('resourceallocation', 368,
			        res_filter, null);

			if (resource_search) {

				if (resource_search.length == 1) {
					emp_id = resource_search[0].getValue('resource', null,
					        'group');
					customerDiscountDetails.Employee = emp_id;
					nlapiLogExecution('DEBUG', 'Sinlge Employee Project',
					        'emp_id : ' + emp_id);
					b_td_count = true;
				} else {
					nlapiLogExecution('DEBUG', 'Group Billing Project');
					b_td_count = true;
				}
			} else {
				nlapiLogExecution('DEBUG', 'Exit',
				        'No Allocations for the project');
				return true;
			}

			var s_Apply_Dis = project_load
			        .getFieldValue('custentity_applydiscount');
			nlapiLogExecution('DEBUG', 'saveRecord_Custdesc_Desccal',
			        's_Apply_Dis : ' + s_Apply_Dis);

			var s_Apply_Dis_VD = project_load
			        .getFieldValue('custentity_apply_vd');
			nlapiLogExecution('DEBUG', 'saveRecord_Custdesc_Desccal',
			        's_Apply_Dis_VD : ' + s_Apply_Dis_VD);

			var s_Apply_Dis_TD = project_load
			        .getFieldValue('custentity_apply_td');
			nlapiLogExecution('DEBUG', 'saveRecord_Custdesc_Desccal',
			        's_Apply_Dis_TD : ' + s_Apply_Dis_TD);

			// ===================================================================================================================
			// if (s_Apply_Dis == 'T') //
			{// 1 if start
				// ---111111111
				// block------------------------------------------------------------------------------------------------
				// var i_Subtotal_Value=nlapiGetFieldValue('subtotal');
				// alert('i_Subtotal_Value'+i_Subtotal_Value);

				// var S_Customer_Id = nlapiGetFieldValue('entity');
				// nlapiLogExecution('DEBUG', 'saveRecord_Custdesc_Desccal',
				// 'S_Customer_Id : ' + S_Customer_Id);

				var cust_blank = validate(S_Customer_Id);
				var o_Customer_Load = nlapiLoadRecord('customer', S_Customer_Id);
				var d_MSA_Start_Date = o_Customer_Load
				        .getFieldValue('startdate');
				var d_MSA_End_Date = o_Customer_Load.getFieldValue('enddate');
				// alert('S_Customer_Id'+S_Customer_Id);

				var cumulative_sale_F = o_Customer_Load
				        .getFieldValue('custentity_cummulative_sales_final');

				if (cumulative_sale_F) {
					cumulative_sale = parseFloat(cumulative_sale_F);
				} else {
					// get the opening balance
					cumulative_sale_F = o_Customer_Load
					        .getFieldValue('custentity_cummulativesales');

					if (cumulative_sale_F) {
						cumulative_sale_F = parseFloat(cumulative_sale_F);
					} else {
						cumulative_sale_F = 0;
					}
				}

				nlapiLogExecution('DEBUG', 'saveRecord_Custdesc_Desccal',
				        'cumulative_sale_F : ' + cumulative_sale_F);

				// -----------------------search invoice to check already
				// discount applied or not---------------------------
				if (rec_type == 'invoice') //
				{
					// if(s_Remittance_Method == '1') //
					// {//2 if start
					var Filter = new Array();
					Filter[0] = new nlobjSearchFilter('entity', null, 'is',
					        S_Customer_Id);
					// Filter[1]=new nlobjSearchFilter('job', null,
					// 'is',project_rec_id);
					var o_Annual_Des_invoice_search = nlapiSearchRecord(
					        'transaction', 215, Filter, null);
					// alert('o_Annual_Des_invoice_search'+o_Annual_Des_invoice_search.length);

					if (o_Annual_Des_invoice_search != null) //
					{
						nlapiLogExecution('DEBUG',
						        'o_Annual_Des_invoice_search',
						        o_Annual_Des_invoice_search.length);
						b_Annual_counter = true;
					}

					var Filter = new Array();
					Filter[0] = new nlobjSearchFilter('entity', null, 'is',
					        S_Customer_Id);
					// Filter[1]=new nlobjSearchFilter('job', null,
					// 'is',project_rec_id);
					var o_Quater_Des_invoice_search = nlapiSearchRecord(
					        'transaction', 214, Filter, null);
					// alert('o_Quater_Des_invoice_search'+o_Quater_Des_invoice_search.length);

					if (o_Quater_Des_invoice_search != null) //
					{
						nlapiLogExecution('DEBUG',
						        'o_Quater_Des_invoice_search',
						        o_Quater_Des_invoice_search.length);
						b_Quater_counter = true;
					}
					// }// 2 else close
				}

				// get the employee, practice and vertical
				customer_vertical = nlapiLookupField('customer', S_Customer_Id,
				        'custentity_vertical');

				var lineCount = nlapiGetLineItemCount('time');
				for (var linenum = 1; linenum <= lineCount; linenum++) {

					var isApplied = nlapiGetLineItemValue('time', 'apply',
					        linenum) == 'T';

					if (isApplied) {
						a_emp_id = nlapiGetLineItemValue('time', 'employee',
						        linenum);

						var look = nlapiLookupField('employee', a_emp_id, [
						        'entityid', 'department' ]);
						emp_name = look.entityid;
						emp_practice = look.department;

						// nlapiSearchRecord(
						// 'employee',
						// null,
						// [ new nlobjSearchFilter('internalid', null,
						// 'anyof', a_emp_id) ],
						// [ new nlobjSearchColumn('department'),
						// new nlobjSearchColumn('entityid') ])
						// .forEach(function(emp) {
						// emp_name = emp.getValue('entityid');
						// emp_practice = emp.getValue('department');
						// });

						break;
					}
				}

				// ---111111111
				// block------------------------------------------------------------------------------------------------

				if (b_counter == false) //
				{
					var s_Apply_Method = 'invoice';// o_Customer_Load.getFieldValue('custentity_applymethod');
					nlapiLogExecution('DEBUG', 'o_Quater_Des_invoice_search',
					        's_Apply_Method : ' + s_Apply_Method);
					nlapiLogExecution('DEBUG', 'o_Quater_Des_invoice_search',
					        'rec_type : ' + rec_type);

					if (s_Apply_Method == rec_type) //
					{// 3 if start
						// ====================check all mandatory field fill or
						// not===========================
						var d_Date = nlapiGetFieldValue('trandate');
						var date_blank = validate(d_Date);
						// //alert('date_blank'+date_blank);

						var posting_Period = nlapiGetFieldValue('postingperiod');
						var posting_Period_blank = validate(posting_Period);
						// //alert('posting_Period_blank'+posting_Period_blank);

						var location = nlapiGetFieldValue('location');
						var location_blank = validate(location);
						// //alert('location_blank'+location_blank);

						var Draft_Invoice_Approver = nlapiGetFieldValue('custbody_draft_invoice_app');
						var Draft_Invoice_Approver_blank = validate(Draft_Invoice_Approver);

						// alert('cust_blank'+cust_blank);
						// alert('Draft_Invoice_Approver_blank'+Draft_Invoice_Approver_blank);
						// alert('pro_blank'+pro_blank);

						if (date_blank == false || posting_Period == false
						        || location_blank == false
						        || cust_blank == false) //
						{
							alert('Please fill All The Mandatory Fields');
							return false;
						}

						// ======================================Process For
						// Fixed Free
						// Discount===============================================

						if (s_Apply_Dis == 'T') //
						{
							nlapiLogExecution('DEBUG',
							        'saveRecord_Custdesc_Desccal',
							        'Into Fixed discount...');
							nlapiLogExecution('DEBUG',
							        'saveRecord_Custdesc_Desccal',
							        'total_inc_aftr_dsc : '
							                + total_inc_aftr_dsc);

							var fixed_discount = Fixed_Discount_Cal(
							        S_Customer_Id, d_MSA_Start_Date,
							        d_MSA_End_Date, d_Project_Start_Date,
							        d_Project_End_Date, rec_type,
							        o_Customer_Load, b_Annual_counter,
							        b_Quater_counter, total_inc, subtotal_exec,
							        customerDiscountDetails);

							wasDiscountAdded = true;

							nlapiLogExecution('DEBUG', 'fixed_discount',
							        fixed_discount);
						}

						// added by Nitish
						// if both TD and VD enabled, add the invoice amount to
						// cumulative sales final
						if (s_Apply_Dis_TD == 'T' && s_Apply_Dis_VD == 'T') {

							// Tenure Discount Part
							if (s_Apply_Dis_TD == 'T') //
							{
								nlapiLogExecution('DEBUG', 'Save Record',
								        'Tenure Discount Enabled');

								nlapiLogExecution('DEBUG',
								        'saveRecord_Custdesc_Desccal',
								        'i_Project_Type : ' + i_Project_Type);

								nlapiLogExecution('DEBUG',
								        'saveRecord_Custdesc_Desccal',
								        'b_td_count : ' + b_td_count);

								if (i_Project_Type == 'TM') {

									if (S_Customer_Id == '4297') // take
									// total
									// after fixed
									// dscnt applied
									// only for
									// Freddy Mac
									{
										nlapiLogExecution('DEBUG',
										        'Tenure Discount',
										        '--------------------Start');

										total_inc = nlapiGetFieldValue('total');

										nlapiLogExecution('DEBUG',
										        'Save Record',
										        'Invoice Total : ' + total_inc);

										var tenureDiscount = tenure_discount(
										        S_Customer_Id,
										        d_MSA_Start_Date,
										        d_MSA_End_Date,
										        d_Project_Start_Date,
										        d_Project_End_Date, rec_type,
										        o_Customer_Load,
										        b_Annual_counter,
										        b_Quater_counter, emp_id,
										        project_rec_id, total_inc,
										        subtotal_exec);

										wasDiscountAdded = true;

										nlapiLogExecution('DEBUG',
										        'Tenure Discount',
										        'Total Tenure Discount : '
										                + tenureDiscount);
									}
								}
							}

							nlapiLogExecution('DEBUG',
							        'saveRecord_Custdesc_Desccal',
							        'total_inc_aftr_dsc after TD : '
							                + total_inc_aftr_dsc);

							nlapiLogExecution('DEBUG',
							        'saveRecord_Custdesc_Desccal',
							        'cumulative_sale_F : ' + cumulative_sale_F);

							// if invoice is being editted, reduce the last
							// invoice details
							var last_cumulative_sales_addition = parseFloat(nlapiGetFieldValue('custbody_inv_last_sales_addition'));

							nlapiLogExecution('DEBUG',
							        'last_cumulative_sales_addition',
							        last_cumulative_sales_addition);

							cumulative_sale_F = cumulative_sale_F
							        - last_cumulative_sales_addition;

							nlapiLogExecution('DEBUG', 'Correct Sales Value',
							        cumulative_sale_F);
							customerDiscountDetails.CumulativeSalesOld = cumulative_sale_F;

							nlapiLogExecution('DEBUG',
							        'original_invoice_total',
							        original_invoice_total);

							nlapiLogExecution('DEBUG', 'tenureDiscount',
							        tenureDiscount);

							nlapiLogExecution('DEBUG',
							        'saveRecord_Custdesc_Desccal',
							        'cumulative_sale_F : ' + cumulative_sale_F);

							cumulative_sale_F = parseFloat(cumulative_sale_F)
							        + parseFloat(original_invoice_total)
							        - parseFloat(tenureDiscount);

							nlapiLogExecution('DEBUG',
							        'saveRecord_Custdesc_Desccal',
							        'Final Cumulative Sales Final : '
							                + cumulative_sale_F);

							customerDiscountDetails.PurchaseOrderNumber = nlapiGetFieldValue('otherrefnum');
							customerDiscountDetails.CumulativeSalesNew = cumulative_sale_F;
							o_Customer_Load.setFieldValue(
							        'custentity_cummulative_sales_final',
							        cumulative_sale_F);

							// set the amount added, to the custom field
							nlapiSetFieldValue(
							        'custbody_inv_last_sales_addition',
							        parseFloat(original_invoice_total)
							                - parseFloat(tenureDiscount));

							nlapiSubmitRecord(o_Customer_Load, true);

							// nlapiSubmitField('customer', S_Customer_Id,
							// 'custentity_cummulative_sales_final',
							// cumulative_sale_F);

							nlapiLogExecution('DEBUG',
							        'saveRecord_Custdesc_Desccal',
							        'Final Cummulative amount is set on customer');

							nlapiLogExecution('DEBUG', 'Tenure Discount',
							        '--------------------End');

							// Volume Discount Part
							if (s_Apply_Dis_VD == 'T') //
							{
								nlapiLogExecution('DEBUG', 'Save Record',
								        'Volume Discount Enabled');

								if (S_Customer_Id == '4297') // take total
								// after
								// tenure dscnt
								// applied only for
								// Freddy Mac
								{
									total_inc = nlapiGetFieldValue('total');
									nlapiLogExecution('DEBUG', 'Save Record',
									        'total_inc : ' + total_inc);

									var first_desc = Volume_desc(S_Customer_Id,
									        d_MSA_Start_Date, d_MSA_End_Date,
									        d_Project_Start_Date,
									        d_Project_End_Date, rec_type,
									        o_Customer_Load, b_Annual_counter,
									        b_Quater_counter, total_inc,
									        subtotal_exec);

									wasDiscountAdded = true;

									nlapiLogExecution('DEBUG', 'Save Record',
									        first_desc);
								}

							}
						}
					}
				}
			}
		}

		if (wasDiscountAdded) {
			nlapiSetFieldValue('custbody_inv_is_discount_added', 'T');

			customerDiscountDetails.TotalDiscount = customerDiscountDetails.VolumeDiscount
			        + customerDiscountDetails.TenureDiscount
			        + customerDiscountDetails.FixedFeeDiscount;
			customerDiscountDetails.NetInvoiceAmount = customerDiscountDetails.TotalAmount
			        - customerDiscountDetails.TotalDiscount;

			saveCustomerDiscountDetails();
		}

		return true;
	} catch (ex) {
		nlapiLogExecution('ERROR', 'At main', 'Ex ' + ex);
		nlapiLogExecution('ERROR', 'At main', 'Ex.message ' + ex.message);

		// removed the added discount items
		var i_Item_Line_Count = nlapiGetLineItemCount('item');

		for (var i_Counter_II = i_Item_Line_Count; i_Counter_II >= 1; i_Counter_II--) {

			if (nlapiGetLineItemValue('item', 'itemtype', i_Counter_II) == 'Discount') {
				nlapiRemoveLineItem('item', i_Counter_II);
			}
		}

		return false;
	}
}

function GetVolumeDiscount(customerId) {
	try {
		nlapiLogExecution('DEBUG', 'Customer Id', customerId);

		// load the customer record
		var customerRecord = nlapiLoadRecord('customer', customerId);

		// get the current cummulative sales amount from the customer
		var cumulativeSales = customerRecord
		        .getFieldValue('custentity_cummulative_sales_final');
		var cumulativeSalesInCustomer = cumulativeSales;

		if (cumulativeSales) {
			cumulativeSales = parseFloat(cumulativeSales);
		} else {
			// get the opening balance
			cumulativeSales = customerRecord
			        .getFieldValue('custentity_cummulativesales');

			if (cumulativeSales) {
				cumulativeSales = parseFloat(cumulativeSales);
			} else {
				cumulativeSales = 0;
			}
		}

		nlapiLogExecution('DEBUG', 'Cumulative Sales Final', cumulativeSales);

		var lastAddition = parseFloat(nlapiGetFieldValue('custbody_inv_last_sales_addition'));
		cumulativeSales = cumulativeSales - lastAddition;
		var previousCumulativeSales = parseFloat(cumulativeSales);
		nlapiLogExecution('DEBUG', 'previousCumulativeSales',
		        previousCumulativeSales);

		var currentInvoiceAmount = parseFloat(nlapiGetFieldValue('total'));
		nlapiLogExecution('DEBUG', 'Current Invoice Amount',
		        currentInvoiceAmount);
		nlapiLogExecution('DEBUG', 'cumulativeSales', cumulativeSales);

		var netCumulativeSales = parseFloat(cumulativeSales)
		        + parseFloat(currentInvoiceAmount);
		nlapiLogExecution('DEBUG', 'netCumulativeSales ', netCumulativeSales);

		if (customerId) {

			// get all the volume discounts slabs for this customer
			var volumeDiscountSearch = nlapiSearchRecord(
			        'customrecord_vdcustparent', null, [ new nlobjSearchFilter(
			                'custrecord_customer_parent', null, 'is',
			                customerId) ], [
			                new nlobjSearchColumn('custrecord_vddiscountrate'),
			                new nlobjSearchColumn('custrecord_vdslabfrom')
			                        .setSort(false),
			                new nlobjSearchColumn('custrecord_vdslabto') ]);

			if (volumeDiscountSearch) {
				nlapiLogExecution('DEBUG', 'Cumulative Sales Slab Count',
				        volumeDiscountSearch.length);

				var totalVolumeDiscount = 0;
				var discount = 0;
				var amountDifference = 0;
				var slabFrom = 0;
				var slabTo = 0;
				var volumeDiscount = 0;
				var previousSlabTo = volumeDiscountSearch[0]
				        .getValue('custrecord_vdslabfrom');

				for (var i = 0; i < volumeDiscountSearch.length; i++) {
					volumeDiscount = volumeDiscountSearch[i];
					discount = volumeDiscount
					        .getValue('custrecord_vddiscountrate');

					discount = parseFloat((discount.split('%')[0]).trim());
					slabFrom = parseFloat(volumeDiscount
					        .getValue('custrecord_vdslabfrom'));
					slabTo = parseFloat(volumeDiscount
					        .getValue('custrecord_vdslabto'));

					nlapiLogExecution('DEBUG', 'slabTo', slabTo);
					nlapiLogExecution('DEBUG', 'slabFrom', slabFrom);

					if (slabFrom <= cumulativeSales
					        && slabTo >= cumulativeSales) {

						// entire amount lies in the current slab
						if (slabFrom <= netCumulativeSales
						        && slabTo >= netCumulativeSales) {
							totalVolumeDiscount = (currentInvoiceAmount * discount) / 100;

							customerDiscountDetails.VolumeDiscountAmountA = totalVolumeDiscount;
							customerDiscountDetails.VolumeDiscountPercentA = discount;
							customerDiscountDetails.VolumeDiscount = totalVolumeDiscount;
							nlapiLogExecution('DEBUG', 'volume discount',
							        'no split');
							break;
						} else { // slab changing
							nlapiLogExecution('DEBUG', 'volume discount',
							        'slab split');

							// consider the amount without fixed fee
							// netCumulativeSales = original_invoice_total
							// -
							// parseFloat(nlapiGetFieldValue('custbody_inv_tenure_discount'))
							// + previousCumulativeSales;

							netCumulativeSales = cumulativeSalesInCustomer;

							// netCumulativeSales = currentInvoiceAmount
							// + previousCumulativeSales;

							nlapiLogExecution('DEBUG',
							        'new netCumulativeSales',
							        netCumulativeSales);

							// get the percentage from the next slab
							var nextSlabDiscount = volumeDiscountSearch[i + 1]
							        .getValue('custrecord_vddiscountrate');
							nextSlabDiscount = parseFloat((nextSlabDiscount
							        .split('%')[0]).trim());
							nlapiLogExecution('DEBUG', 'slabTo', slabTo);

							var upperSlabAmount = parseFloat(netCumulativeSales)
							        - parseFloat(slabTo);
							var lowerSlabAmount = parseFloat(currentInvoiceAmount)
							        - parseFloat(upperSlabAmount);
							nlapiLogExecution('DEBUG', 'upper slab amount',
							        upperSlabAmount);
							nlapiLogExecution('DEBUG', 'lower slab amount',
							        lowerSlabAmount);

							var upperSlabDiscount = (upperSlabAmount * nextSlabDiscount) / 100;
							var lowerSlabDiscount = (lowerSlabAmount * discount) / 100;

							nlapiLogExecution('DEBUG', 'upper slab discount',
							        upperSlabDiscount);
							nlapiLogExecution('DEBUG', 'lower slab discount',
							        lowerSlabDiscount);

							totalVolumeDiscount = upperSlabDiscount
							        + lowerSlabDiscount;

							customerDiscountDetails.VolumeDiscountAmountA = upperSlabDiscount;
							customerDiscountDetails.VolumeDiscountPercentA = nextSlabDiscount;
							customerDiscountDetails.VolumeDiscountAmountB = lowerSlabDiscount;
							customerDiscountDetails.VolumeDiscountPercentB = discount;
							customerDiscountDetails.VolumeDiscount = totalVolumeDiscount;

							// ---

							if (false) {
								// get the amount lying in the current slab
								var currentSlabAmount = slabTo
								        - cumulativeSales;
								totalVolumeDiscount = (currentSlabAmount * discount) / 100;

								// get the percentage from the next slab
								var nextSlabDiscount = volumeDiscountSearch[i + 1]
								        .getValue('custrecord_vddiscountrate');
								nextSlabDiscount = parseFloat((nextSlabDiscount
								        .split('%')[0]).trim());
								totalVolumeDiscount += ((currentInvoiceAmount - currentSlabAmount) * nextSlabDiscount) / 100;
							}
							break;
						}
					}

					previousSlabTo = slabTo;
				}

				nlapiLogExecution('DEBUG', 'totalVolumeDiscount',
				        totalVolumeDiscount);
				return totalVolumeDiscount;
			} else {
				throw "Volume Discount Slab Not Found For This Customer";
			}

		} else {
			throw "No Customer Id Provided";
		}
	} catch (err) {
		nlapiLogExecution('error', 'GetVolumeDiscount', err);
		throw err;
	}
}

function Volume_desc(S_Customer_Id, d_MSA_Start_Date, d_MSA_End_Date,
        d_Project_Start_Date, d_Project_End_Date, rec_type, o_Customer_Load,
        b_Annual_counter, b_Quater_counter, total_inc, subtotal_exec)//
{
	nlapiLogExecution('DEBUG', 'Volume_desc', 'Started');

	// load the customer record
	var cust_rec = nlapiLoadRecord('customer', S_Customer_Id);

	// get the current cummulative sales amount from the customer
	var cumulative_sale = cust_rec
	        .getFieldValue('custentity_cummulative_sales_final');

	if (cumulative_sale) {
		cumulative_sale = parseFloat(cumulative_sale);
	} else {
		// get the opening balance
		cumulative_sale = cust_rec.getFieldValue('custentity_cummulativesales');

		if (cumulative_sale) {
			cumulative_sale = parseFloat(cumulative_sale);
		} else {
			cumulative_sale = 0;
		}
	}

	nlapiLogExecution('DEBUG', 'Volume Discount',
	        'cumulative_sale (set in customer ): ' + cumulative_sale);

	var invoice_total = nlapiGetFieldValue('total');

	var invoice_tax_total = nlapiGetFieldValue('subtotal'); // ?? why
	// subtotal
	// and not tax

	// var invoice_id = nlapiGetRecordId();
	// if (invoice_id) {
	// var last_tenure_discount =
	// nlapiGetFieldValue('custbody_inv_tenure_discount');

	// var previous_invoice_total = parseFloat(nlapiLookupField('invoice',
	// invoice_id, 'total'));
	// cumulative_sale = cumulative_sale - last_tenure_discount;
	// }

	// read all the volume discount slabs for this customer, get the first one
	// for calculating the MSA , etc.
	var Filter = new Array();
	Filter[0] = new nlobjSearchFilter('custrecord_customer_parent', null, 'is',
	        S_Customer_Id);

	var o_Search_Volume_Rec = nlapiSearchRecord('customrecord_vdcustparent',
	        219, Filter);

	if (o_Search_Volume_Rec) {
		nlapiLogExecution('DEBUG', 'Volume_desc', 'No. of slabs '
		        + o_Search_Volume_Rec.length);

		var new_total = 0;

		var discount_validity = o_Search_Volume_Rec[0]
		        .getValue('custrecord_vddiscountvalidity');
		var base_amount = o_Search_Volume_Rec[0]
		        .getValue('custrecord_vdbasedamount');
		var remmitance = o_Search_Volume_Rec[0]
		        .getValue('custrecord_vdremittancemethod');
		// var total_amount = parseFloat(total_invoice_amount)
		// + parseFloat(nlapiGetFieldValue('total'));

		// nlapiLogExecution('DEBUG', 'Volume_desc', 'total 1'
		// + total_invoice_amount);
		// nlapiLogExecution('DEBUG', 'Volume_desc', 'total 2' + total_amount);

		// discount validity
		var start_date, end_date;
		var d_Invoice_Date = new Date(nlapiGetFieldValue('trandate'));

		if (discount_validity == 1) { // MSA
			start_date = nlapiStringToDate(d_MSA_Start_Date);
			end_date = nlapiStringToDate(d_MSA_End_Date);
		} else if (discount_validity == 2) { // Project Date
			start_date = nlapiStringToDate(d_Project_Start_Date);
			end_date = nlapiStringToDate(d_Project_End_Date);
		} else {
			alert("Discount not valid A " + discount_validity);
			return;
		}

		var discount_valid = false;

		if (start_date <= d_Invoice_Date && d_Invoice_Date <= end_date) {
			d_date = nlapiGetFieldValue('trandate');
			var o_Inv_Date = nlapiStringToDate(d_date);
			var d_Month = o_Inv_Date.getMonth() + 1;

			if (remmitance == '1') {
				discount_valid = d_Month == '12';
			} else if (remmitance == '2') {
				discount_valid = d_Month == '3' || d_Month == '6'
				        || d_Month == '9' || d_Month == '12';
			} else if (remmitance == '3') {
				discount_valid = true;
			}
		}

		if (discount_valid) {
			// Including Expenses and Taxes
			if (base_amount == '1') {
				nlapiLogExecution('DEBUG', 'Volume_desc',
				        'Into include expense');
				new_total = parseFloat(invoice_total);
			} else { // Excluding Taxes and Expenses
				nlapiLogExecution('DEBUG', 'Volume_desc',
				        'into exclude expense');
				// var total_tax = parseFloat(total_invoice_tax_amount)
				// + parseFloat(nlapiGetFieldValue('subtotal')); // ??
				// new_total = total_amount - total_tax;
			}

			new_total = parseFloat(new_total) + parseFloat(cumulative_sale);
			nlapiLogExecution('DEBUG', 'Volume_desc',
			        'After Adding Cumulative Sales : ' + new_total);

			// calculate the total volume discount
			var totalVolumeDiscount = GetVolumeDiscount(S_Customer_Id);

			// add the volume discount line
			nlapiSelectNewLineItem('item');
			nlapiSetCurrentLineItemValue('item', 'item',
			        constant.Item.VolumeDiscount, true, true);
			nlapiSetCurrentLineItemValue('item', 'custcol_discouint_amt',
			        totalVolumeDiscount, true, true);
			nlapiSetCurrentLineItemValue('item', 'rate', -totalVolumeDiscount,
			        true, true);
			nlapiSetCurrentLineItemValue('item', 'price_display', 'Custom',
			        true, true);
			nlapiSetCurrentLineItemValue('item', 'department', emp_practice);
			nlapiSetCurrentLineItemValue('item', 'custcol_employeenamecolumn',
			        emp_name);
			nlapiSetCurrentLineItemValue('item', 'class', customer_vertical);

			nlapiCommitLineItem('item');
			nlapiLogExecution('DEBUG', 'volume discount added');
		} else {
			alert("Discount not valid B " + discount_valid);
		}

		return true;
	}
}

function GetTenureDiscount(customerId, cumulativeHours, invoiceHours, billRate,
        currentInvoiceTotal)
{
	try {
		nlapiLogExecution('DEBUG', 'Customer Id', customerId);
		nlapiLogExecution('DEBUG', 'Cumulative Hours', cumulativeHours);
		nlapiLogExecution('DEBUG', 'Invoice Hours', invoiceHours);

		if (customerId) {

			// get all the tenure discounts slabs for this customer
			var tenureDiscountSearch = nlapiSearchRecord(
			        'customrecord_tenurediscount', null,
			        [ new nlobjSearchFilter('custrecord_tdcustparent', null,
			                'is', customerId) ], [
			                new nlobjSearchColumn('custrecord_tddiscountrate'),
			                new nlobjSearchColumn('custrecord_tdslabfrom')
			                        .setSort(false),
			                new nlobjSearchColumn('custrecord_tdslabto') ]);

			if (tenureDiscountSearch) {
				var totalTenureDiscount = 0;
				var discount = 0;
				var hoursDifference = 0;
				var slabFrom = 0;
				var slabTo = 0;
				var tenureDiscount = 0;
				var previousSlabTo = tenureDiscountSearch[0]
				        .getValue('custrecord_tdslabfrom');

				for (var i = 0; i < tenureDiscountSearch.length; i++) {
					tenureDiscount = tenureDiscountSearch[i];
					discount = tenureDiscount
					        .getValue('custrecord_tddiscountrate');

					discount = parseFloat((discount.split('%')[0]).trim());
					slabFrom = parseFloat(tenureDiscount
					        .getValue('custrecord_tdslabfrom'));
					slabTo = parseFloat(tenureDiscount
					        .getValue('custrecord_tdslabto'));
					var netCumulativeHours = parseFloat(cumulativeHours)
					        + parseFloat(invoiceHours);

					nlapiLogExecution('DEBUG', 'slabfrom', slabFrom);
					nlapiLogExecution('DEBUG', 'slabTo', slabTo);
					nlapiLogExecution('DEBUG', 'cumulativeHours',
					        cumulativeHours);
					nlapiLogExecution('DEBUG', 'netCumulativeHours',
					        netCumulativeHours);

					if (slabFrom <= cumulativeHours
					        && slabTo >= cumulativeHours) {

						// entire hour lies in the current slab
						if (slabFrom <= netCumulativeHours
						        && slabTo >= netCumulativeHours) {
							nlapiLogExecution('DEBUG', 'normal slab');
							nlapiLogExecution('DEBUG', 'tenure slab',
							        'normal slab');
							totalTenureDiscount = (currentInvoiceTotal * discount) / 100;
							customerDiscountDetails.TenureDiscountPercentA = discount;
							customerDiscountDetails.TenureDiscountAmountA = totalTenureDiscount;
							customerDiscountDetails.TenureDiscount = totalTenureDiscount;
							break;
						} else { // slab changing
							// alert('slab split');
							nlapiLogExecution('DEBUG', 'tenure slab',
							        'slab split');

							// get the percentage from the next slab
							var nextSlabDiscount = tenureDiscountSearch[i + 1]
							        .getValue('custrecord_tddiscountrate');
							nextSlabDiscount = parseFloat((nextSlabDiscount
							        .split('%')[0]).trim());
							// totalTenureDiscount += ((invoiceHours -
							// currentSlabHour)
							// * billRate * nextSlabDiscount) / 100;
							nlapiLogExecution('DEBUG', 'tenure slab slabTo',
							        slabTo);

							var upperSlabHour = parseFloat(netCumulativeHours)
							        - parseFloat(slabTo);
							var lowerSlabHour = parseFloat(invoiceHours)
							        - parseFloat(upperSlabHour);
							nlapiLogExecution('DEBUG',
							        'tenure slab upper slab hours',
							        upperSlabHour);
							nlapiLogExecution('DEBUG',
							        'tenure slab lower slab hours',
							        lowerSlabHour);

							var upperSlabDiscount = (upperSlabHour * billRate * nextSlabDiscount) / 100;
							var lowerSlabDiscount = (lowerSlabHour * billRate * discount) / 100;

							nlapiLogExecution('DEBUG',
							        'tenure slab upper slab discount',
							        upperSlabDiscount);
							nlapiLogExecution('DEBUG',
							        'tenure slab lower slab discount',
							        lowerSlabDiscount);

							totalTenureDiscount = upperSlabDiscount
							        + lowerSlabDiscount;

							customerDiscountDetails.TenureDiscountPercentA = nextSlabDiscount;
							customerDiscountDetails.TenureDiscountAmountA = upperSlabDiscount;
							customerDiscountDetails.TenureDiscountPercentB = discount;
							customerDiscountDetails.TenureDiscountAmountB = lowerSlabDiscount;
							customerDiscountDetails.TenureDiscount = totalTenureDiscount;

							// get the amount lying in the current slab
							// var currentSlabHour = slabTo - cumulativeHours;
							// totalTenureDiscount = (currentSlabHour * billRate
							// * discount) / 100;

							break;
						}
					}

					previousSlabTo = slabTo;
				}

				nlapiLogExecution('DEBUG', 'totalTenureDiscount',
				        totalTenureDiscount);
				return totalTenureDiscount;
			} else {
				throw "Tenure Discount Slab Not Found For This Customer";
			}

		} else {
			throw "No Customer Id Provided";
		}
	} catch (err) {
		nlapiLogExecution('error', 'GetTenureDiscount', err);
		throw err;
	}
}

function GetEmpTenureDiscount(cumulativeHours, invoiceHours, billRate,
        currentInvoiceTotal, tenureDiscountSearch, employeeId,
        lastCumulativeTotal, stHours, otHours, stRate, otRate)
{
	try {
		// //nlapiLogExecution('DEBUG', 'Cumulative Hours', cumulativeHours);
		// nlapiLogExecution('DEBUG', 'Invoice Hours', invoiceHours);

		// if (customerId) {

		// if (tenureDiscountSearch) {
		var totalTenureDiscount = 0;
		var discount = 0;
		var hoursDifference = 0;
		var slabFrom = 0;
		var slabTo = 0;
		var tenureDiscount = 0;
		var previousSlabTo = tenureDiscountSearch[0]
		        .getValue('custrecord_tdslabfrom');

		for (var i = 0; i < tenureDiscountSearch.length; i++) {
			tenureDiscount = tenureDiscountSearch[i];
			discount = tenureDiscount.getValue('custrecord_tddiscountrate');

			discount = parseFloat((discount.split('%')[0]).trim());
			slabFrom = parseFloat(tenureDiscount
			        .getValue('custrecord_tdslabfrom'));
			slabTo = parseFloat(tenureDiscount.getValue('custrecord_tdslabto'));
			var netCumulativeHours = parseFloat(cumulativeHours)
			        + parseFloat(invoiceHours);

			// nlapiLogExecution('DEBUG', 'slabfrom', slabFrom);
			// nlapiLogExecution('DEBUG', 'slabTo', slabTo);
			// nlapiLogExecution('DEBUG', 'cumulativeHours', cumulativeHours);
			// nlapiLogExecution('DEBUG', 'netCumulativeHours',
			// netCumulativeHours);

			if (slabFrom <= cumulativeHours && slabTo >= cumulativeHours) {

				// entire hour lies in the current slab
				if (slabFrom <= netCumulativeHours
				        && slabTo >= netCumulativeHours) {
					// nlapiLogExecution('DEBUG', 'normal slab');
					// nlapiLogExecution('DEBUG', 'tenure slab', 'normal slab');
					totalTenureDiscount = (invoiceHours * billRate * discount) / 100;
					tenureDiscountReportDetails.push({
					    Employee : employeeId,
					    PercentA : discount,
					    PercentB : 0,
					    FinalPercent : discount,
					    AmountA : totalTenureDiscount,
					    AmountB : 0,
					    FinalAmount : totalTenureDiscount,
					    OldHours : lastCumulativeTotal,
					    NewHours : cumulativeHours,
					    ST_Hours : stHours,
					    OT_Hours : otHours,
					    ST_Rate : stRate,
					    OT_Rate : otRate
					});

					// customerDiscountDetails.TenureDiscountPercentA =
					// discount;
					// customerDiscountDetails.TenureDiscountAmountA =
					// totalTenureDiscount;
					// customerDiscountDetails.TenureDiscount =
					// totalTenureDiscount;
					break;
				} else { // slab changing
					// alert('slab split');
					// nlapiLogExecution('DEBUG', 'tenure slab', 'slab split');

					// get the percentage from the next slab
					var nextSlabDiscount = tenureDiscountSearch[i + 1]
					        .getValue('custrecord_tddiscountrate');
					nextSlabDiscount = parseFloat((nextSlabDiscount.split('%')[0])
					        .trim());
					// totalTenureDiscount += ((invoiceHours -
					// currentSlabHour)
					// * billRate * nextSlabDiscount) / 100;
					// nlapiLogExecution('DEBUG', 'tenure slab slabTo', slabTo);

					var upperSlabHour = parseFloat(netCumulativeHours)
					        - parseFloat(slabTo);
					var lowerSlabHour = parseFloat(invoiceHours)
					        - parseFloat(upperSlabHour);
					// nlapiLogExecution('DEBUG', 'tenure slab upper slab
					// hours',
					// upperSlabHour);
					// nlapiLogExecution('DEBUG', 'tenure slab lower slab
					// hours',
					// lowerSlabHour);

					var upperSlabDiscount = (upperSlabHour * billRate * nextSlabDiscount) / 100;
					var lowerSlabDiscount = (lowerSlabHour * billRate * discount) / 100;

					// nlapiLogExecution('DEBUG',
					// 'tenure slab upper slab discount',
					// upperSlabDiscount);
					// nlapiLogExecution('DEBUG',
					// 'tenure slab lower slab discount',
					// lowerSlabDiscount);

					totalTenureDiscount = upperSlabDiscount + lowerSlabDiscount;

					customerDiscountDetails.TenureDiscountPercentA = nextSlabDiscount;
					customerDiscountDetails.TenureDiscountAmountA = upperSlabDiscount;
					customerDiscountDetails.TenureDiscountPercentB = discount;
					customerDiscountDetails.TenureDiscountAmountB = lowerSlabDiscount;
					customerDiscountDetails.TenureDiscount = totalTenureDiscount;

					tenureDiscountReportDetails.push({
					    Employee : employeeId,
					    PercentA : nextSlabDiscount,
					    PercentB : discount,
					    FinalPercent : discount,
					    AmountA : upperSlabDiscount,
					    AmountB : lowerSlabDiscount,
					    FinalAmount : totalTenureDiscount,
					    OldHours : lastCumulativeTotal,
					    NewHours : cumulativeHours,
					    ST_Hours : stHours,
					    OT_Hours : otHours,
					    ST_Rate : stRate,
					    OT_Rate : otRate
					});

					// get the amount lying in the current slab
					// var currentSlabHour = slabTo - cumulativeHours;
					// totalTenureDiscount = (currentSlabHour * billRate
					// * discount) / 100;

					break;
				}
			}

			previousSlabTo = slabTo;
		}

		nlapiLogExecution('DEBUG', 'totalTenureDiscount', totalTenureDiscount);
		return totalTenureDiscount;
		// } else {
		// throw "Tenure Discount Slab Not Found For This Customer";
		// }

		// } else {
		// throw "No Customer Id Provided";
		// }
	} catch (err) {
		nlapiLogExecution('error', 'GetEmpTenureDiscount', err);
		throw err;
	}
}

// /////////////////////////////////////////////////
function tenure_discount(S_Customer_Id, d_MSA_Start_Date, d_MSA_End_Date,
        d_Project_Start_Date, d_Project_End_Date, rec_type, o_Customer_Load,
        b_Annual_counter, b_Quater_counter, emp_id, project_rec_id, total_inc,
        subtotal_exec, rec_project) //
{
	nlapiLogExecution('DEBUG', 'tenure discount', 'started');
	var inEditMode = nlapiGetFieldValue('custbody_inv_is_discount_added') == 'T';

	var totalTenureDiscount = 0;

	if (!project_rec_id) {
		throw "Project Id Not Provided";
	}

	rec_project = nlapiLoadRecord('job', project_rec_id);

	// creating an array with hours being invoiced for each employee
	var empHoursDetailObject = {};
	var lineCount = nlapiGetLineItemCount('time');
	var item, stRate, otRate, stHours, otHours, billRate, hours, empId;

	for (var linenum = 1; linenum <= lineCount; linenum++) {

		var isApplied = nlapiGetLineItemValue('time', 'apply', linenum) == 'T';
		if (isApplied) {
			empId = nlapiGetLineItemValue('time', 'employee', linenum);

			// ST OT breakup
			billRate = parseFloat(nlapiGetLineItemValue('time', 'rate', linenum));
			hours = parseFloat(nlapiGetLineItemValue('time', 'qty', linenum));

			stRate = 0, otRate = 0, stHours = 0, otHours = 0;
			item = nlapiGetLineItemValue('time', 'item', linenum);
			if (item == '2222') { // ST
				st_hours = hours;
				st_rate = billRate;
			} else if (item == '2425') { // OT
				ot_hours = hours;
				ot_rate = billRate;
			}

			if (!empHoursDetailObject[empId]) {
				empHoursDetailObject[empId] = {
				    EmployeeId : empId,
				    Name : nlapiLookupField('employee', empId, 'entityid'),
				    Practice : nlapiLookupField('employee', empId,
				            'department', true),
				    HoursBeingInvoiced : hours,
				    LastInvoicedHours : 0,
				    BillRate : billRate,
				    NewTotal : 0,
				    ST_Hours : st_hours,
				    OT_Hours : ot_hours,
				    ST_Rate : st_rate,
				    OT_Rate : ot_rate
				};
			} else {
				empHoursDetailObject[empId].HoursBeingInvoiced += hours;
				empHoursDetailObject[empId].ST_Hours += st_hours;
				empHoursDetailObject[empId].OT_Hours += ot_hours;
				empHoursDetailObject[empId].ST_Rate += st_rate;
				empHoursDetailObject[empId].OT_Rate += ot_rate;
			}
		}
	}

	nlapiLogExecution('DEBUG', 'empHoursDetailObject', JSON
	        .stringify(empHoursDetailObject));

	var customer_vertical = nlapiLookupField('customer', S_Customer_Id,
	        'custentity_vertical');

	var empCount = rec_project
	        .getLineItemCount('recmachcustrecord_ech_project');

	nlapiLogExecution('debug', 'employees in project', empCount);

	// get all the tenure discounts slabs for this customer
	var tenureDiscountSearch = nlapiSearchRecord('customrecord_tenurediscount',
	        null, [ new nlobjSearchFilter('custrecord_tdcustparent', null,
	                'is', S_Customer_Id) ], [
	                new nlobjSearchColumn('custrecord_tddiscountrate'),
	                new nlobjSearchColumn('custrecord_tdslabfrom')
	                        .setSort(false),
	                new nlobjSearchColumn('custrecord_tdslabto') ]);

	for (var line = 1; line <= empCount; line++) {
		var currentEmpId = rec_project.getLineItemValue(
		        'recmachcustrecord_ech_project', 'custrecord_ech_employee',
		        line);
		nlapiLogExecution('debug', 'loop in tenure emp id', currentEmpId);
		var lastEmpCumulativeHours = parseFloat(rec_project.getLineItemValue(
		        'recmachcustrecord_ech_project',
		        'custrecord_ech_cumulative_hours', line));
		lastEmpCumulativeHours = lastEmpCumulativeHours ? lastEmpCumulativeHours
		        : 0;
		var lastEmpInvoicedHours = parseFloat(rec_project.getLineItemValue(
		        'recmachcustrecord_ech_project',
		        'custrecord_ech_last_invoiced_hours', line));
		lastEmpInvoicedHours = lastEmpInvoicedHours ? lastEmpInvoicedHours : 0;

		// reduce the last invoiced hours if the invoice is being editted
		if (inEditMode) {
			lastEmpCumulativeHours -= lastEmpInvoicedHours;
		}

		if (empHoursDetailObject[currentEmpId]) {
			empHoursDetailObject[currentEmpId].LineNumber = line;

			var empHoursBeingInvoiced = parseFloat(empHoursDetailObject[currentEmpId].HoursBeingInvoiced);
			empHoursDetailObject[currentEmpId].NewTotal = empHoursBeingInvoiced
			        + lastEmpCumulativeHours;

			var empTenureDiscount = GetEmpTenureDiscount(
			        lastEmpCumulativeHours, empHoursBeingInvoiced,
			        parseFloat(empHoursDetailObject[currentEmpId].BillRate),
			        parseFloat(nlapiGetFieldValue('total')),
			        tenureDiscountSearch, currentEmpId, lastEmpCumulativeHours,
			        parseFloat(empHoursDetailObject[currentEmpId].ST_Hours),
			        parseFloat(empHoursDetailObject[currentEmpId].OT_hours),
			        parseFloat(empHoursDetailObject[currentEmpId].ST_Rate),
			        parseFloat(empHoursDetailObject[currentEmpId].OT_Rate));

			nlapiLogExecution('debug', 'Tenure Discount for '
			        + empHoursDetailObject[currentEmpId].Name,
			        empTenureDiscount);

			totalTenureDiscount += empTenureDiscount;

			try {
				// add the tenure discount line
				nlapiSelectNewLineItem('item');
				nlapiSetCurrentLineItemValue('item', 'item',
				        constant.Item.TenureDiscount, true, true);
				nlapiSetCurrentLineItemValue('item', 'custcol_discouint_amt',
				        empTenureDiscount, true, true);
				nlapiSetCurrentLineItemValue('item', 'rate',
				        -empTenureDiscount, true, true);
				nlapiSetCurrentLineItemValue('item', 'price_display', 'Custom',
				        true, true);
				nlapiSetCurrentLineItemValue('item', 'department',
				        empHoursDetailObject[currentEmpId].Practice);
				nlapiSetCurrentLineItemValue('item',
				        'custcol_employeenamecolumn',
				        empHoursDetailObject[currentEmpId].Name);
				nlapiSetCurrentLineItemValue('item', 'class', customer_vertical);
				nlapiCommitLineItem('item');
				nlapiLogExecution('DEBUG', 'tenure discount added');

				// alert('emp : ' + currentEmpId);
				// continue;

				// update in project
				var empProjectLine = empHoursDetailObject[currentEmpId].LineNumber;

				alert(empHoursDetailObject[currentEmpId].NewTotal + " @ "
				        + empProjectLine);

				nlapiLogExecution('debug', 'project line', empProjectLine);

				rec_project.setLineItemValue('recmachcustrecord_ech_project',
				        'custrecord_ech_last_invoiced_hours', empProjectLine,
				        parseFloat(empHoursDetailObject[currentEmpId].NewTotal)
				                - lastEmpCumulativeHours);

				rec_project
				        .setLineItemValue(
				                'recmachcustrecord_ech_project',
				                'custrecord_ech_cumulative_hours',
				                empProjectLine,
				                parseFloat(empHoursDetailObject[currentEmpId].NewTotal));

			} catch (errt) {
				nlapiLogExecution('error', 'error in td', errt);
				alert(JSON.stringify(errt));
			}
		} else {
			continue;
		}
	}

	nlapiSubmitRecord(rec_project);
	nlapiLogExecution('debug', 'project updated with the hours');
	nlapiLogExecution('debug', 'total tenure discount', totalTenureDiscount);
	return totalTenureDiscount;

	// old code

	var cumulative_hrs_sum = 0;

	// get the cumulative hours from project
	cumulative_hrs_sum = nlapiLookupField('job', project_Id,
	        'custentity_cummulativehours');

	if (!cumulative_hrs_sum) {
		cumulative_hrs_sum = 0;
	}

	nlapiLogExecution('DEBUG', 'tenure_discount', 'cumulative_hrs_sum : '
	        + cumulative_hrs_sum);

	// get the first tenure discount slab to do MSA, discount validity, etc.
	var Filter = new Array();
	Filter[0] = new nlobjSearchFilter('custrecord_tdcustparent', null, 'is',
	        S_Customer_Id);
	o_Search_Tenure_Rec = nlapiSearchRecord('customrecord_tenurediscount', 212,
	        Filter, null);

	if (o_Search_Tenure_Rec) {
		nlapiLogExecution('DEBUG', 'tenure_discount',
		        'o_Search_Tenure_Rec.length : ' + o_Search_Tenure_Rec.length);

		var discount_validity = o_Search_Tenure_Rec[0]
		        .getValue('custrecord_tddiscountvalidity');
		var base_amount = o_Search_Tenure_Rec[0]
		        .getValue('custrecord_tdbasedamount');
		var remmitance = o_Search_Tenure_Rec[0]
		        .getValue('custrecord_tdremittancemethod');

		// get the total hours in the invoice
		var timeLineCount = nlapiGetLineItemCount('time');
		var billingTo = nlapiGetFieldValue('custbody_billto');
		billTo = nlapiStringToDate(billingTo);

		var totalHours = 0;
		var st_hours = 0;
		var ot_hours = 0;
		var ot_rate = 0;
		var st_rate = 0;
		var billRate = 0;
		var index = 0;
		nlapiLogExecution('DEBUG', 'timeLineCount', timeLineCount);

		for (var linenum = 1; linenum <= timeLineCount; linenum++) {
			var billedDate = nlapiStringToDate(nlapiGetLineItemValue('time',
			        'billeddate', linenum));
			var isApplied = nlapiGetLineItemValue('time', 'apply', linenum) == 'T';
			// nlapiLogExecution('DEBUG', 'field', nlapiGetLineItemValue('time',
			// 'apply', linenum));
			// nlapiLogExecution('DEBUG', 'isApplied', isApplied);

			if (isApplied) {
				index++;
				// nlapiLogExecution('DEBUG', 'applied', index);

				billRate = parseFloat(nlapiGetLineItemValue('time', 'rate',
				        linenum));
				var hours = parseFloat(nlapiGetLineItemValue('time', 'qty',
				        linenum));
				totalHours += hours;

				var item = nlapiGetLineItemValue('time', 'item', linenum);
				if (item == '2222') { // ST
					st_hours += hours;
					st_rate = billRate;
				} else if (item == '2425') { // OT
					ot_hours += hours;
					ot_rate = billRate;
				}
			}
		}

		customerDiscountDetails.ST_Hours = st_hours;
		customerDiscountDetails.OT_Hours = ot_hours;
		customerDiscountDetails.ST_Rate = st_rate;
		customerDiscountDetails.OT_Rate = ot_rate;
		customerDiscountDetails.TotalHours = totalHours;

		nlapiLogExecution('DEBUG', 'tenure discount', 'total invoice hours :'
		        + totalHours);

		// if a previous invoice, then reduce the number of hours from existing
		// invoice from cumulative hour
		var invoice_id = nlapiGetRecordId();
		if (invoice_id) {
			var previous_invoice_hr_total = parseFloat(nlapiGetFieldValue('custbody_inv_last_invoiced_hours'));

			if (false) {
				var previous_invoice_rec = nlapiLoadRecord('invoice',
				        invoice_id);

				var timeLineCount = previous_invoice_rec
				        .getLineItemCount('time');
				var billingTo = previous_invoice_rec
				        .getFieldValue('custbody_billto');
				billTo = nlapiStringToDate(billingTo);

				for (var linenum = 1; linenum <= timeLineCount; linenum++) {
					var billedDate = nlapiStringToDate(previous_invoice_rec
					        .getLineItemValue('time', 'billeddate', linenum));
					var isApplied = previous_invoice_rec.getLineItemValue(
					        'time', 'apply', linenum) == 'T';

					if (isApplied) {
						var hours = parseFloat(previous_invoice_rec
						        .getLineItemValue('time', 'qty', linenum));
						previous_invoice_hr_total += hours;
					}

					if (billedDate > billTo) {
						break;
					}
				}
			}

			nlapiLogExecution('DEBUG', 'tenure discount',
			        'hours in the existing invoice : '
			                + previous_invoice_hr_total);

			cumulative_hrs_sum -= previous_invoice_hr_total;

			nlapiLogExecution('DEBUG', 'tenure discount',
			        'cumulative hours sum after reducing old invoice hr : '
			                + cumulative_hrs_sum);
		}

		customerDiscountDetails.CumulativeHoursOld = cumulative_hrs_sum;

		// var i_Subtotal = 0;
		// if (base_method == '1') {
		// i_Subtotal = parseFloat(nlapiGetFieldValue('total'));
		// } else {
		// i_Subtotal = parseFloat(nlapiGetFieldValue('subtotal'));
		// }

		// discount validity
		var start_date, end_date;
		var d_Invoice_Date = new Date(nlapiGetFieldValue('trandate'));

		if (discount_validity == '1') { // MSA
			start_date = nlapiStringToDate(d_MSA_Start_Date);
			end_date = nlapiStringToDate(d_MSA_End_Date);
		} else if (discount_validity == '2') { // Project Date
			start_date = nlapiStringToDate(d_Project_Start_Date);
			end_date = nlapiStringToDate(d_Project_End_Date);
		} else {
			alert("Discount not valid");
			return;
		}

		var discount_valid = false;

		if (start_date <= d_Invoice_Date && d_Invoice_Date <= end_date) {
			d_date = nlapiGetFieldValue('trandate');
			var o_Inv_Date = nlapiStringToDate(d_date);
			var d_Month = o_Inv_Date.getMonth() + 1;

			if (remmitance == '1') {
				discount_valid = d_Month == '12';
			} else if (remmitance == '2') {
				discount_valid = d_Month == '3' || d_Month == '6'
				        || d_Month == '9' || d_Month == '12';
			} else if (remmitance == '3') {
				discount_valid = true;
			}
		}

		if (discount_valid) {
			// Including Expenses and Taxes
			if (base_amount == '1') {
				nlapiLogExecution('DEBUG', 'tenure discount',
				        'Into include expense');
				// new_total = parseFloat(invoice_total);
			} else { // Excluding Taxes and Expenses
				nlapiLogExecution('DEBUG', 'tenure discount',
				        'into exclude expense');
				// var total_tax = parseFloat(total_invoice_tax_amount)
				// + parseFloat(nlapiGetFieldValue('subtotal')); // ??
				// new_total = total_amount - total_tax;
			}

			var final_total_hours = parseFloat(totalHours)
			        + parseFloat(cumulative_hrs_sum);
			nlapiLogExecution('DEBUG', 'tenure discount',
			        'After Adding Cumulative Hours : ' + final_total_hours);

			// set invoiced hours to the custom field
			nlapiSetFieldValue('custbody_inv_last_invoiced_hours', totalHours);

			// calculate the total tenure discount
			var totalTenureDiscount = GetTenureDiscount(S_Customer_Id,
			        cumulative_hrs_sum, totalHours, billRate,
			        parseFloat(nlapiGetFieldValue('total')));

			// add the tenure discount line
			nlapiSelectNewLineItem('item');
			nlapiSetCurrentLineItemValue('item', 'item',
			        constant.Item.TenureDiscount, true, true);
			nlapiSetCurrentLineItemValue('item', 'custcol_discouint_amt',
			        totalTenureDiscount, true, true);
			nlapiSetCurrentLineItemValue('item', 'rate', -totalTenureDiscount,
			        true, true);
			nlapiSetCurrentLineItemValue('item', 'price_display', 'Custom',
			        true, true);
			nlapiSetCurrentLineItemValue('item', 'department', emp_practice);
			nlapiSetCurrentLineItemValue('item', 'custcol_employeenamecolumn',
			        emp_name);
			nlapiSetCurrentLineItemValue('item', 'class', customer_vertical);

			nlapiCommitLineItem('item');
			nlapiLogExecution('DEBUG', 'tenure discount added');

			total_inc_aftr_dsc = parseFloat(total_inc_aftr_dsc)
			        - parseFloat(totalTenureDiscount);

			// tenure discount set to the custom field
			nlapiSetFieldValue('custbody_inv_tenure_discount',
			        totalTenureDiscount);

			nlapiLogExecution('DEBUG', 'tenure discount',
			        'Tenure Discount added to the custom field');

			// update the project's cumulative hours
			nlapiSubmitField('job', project_Id, 'custentity_cummulativehours',
			        final_total_hours);
			customerDiscountDetails.CumulativeHoursNew = final_total_hours;

			nlapiLogExecution('DEBUG', 'tenure discount',
			        'Project Cumulative Hours Updated');

			return totalTenureDiscount;
		} else {
			alert("Discount not valid");
		}
	}
}

// ////////////////////////////////////////////////
function Fixed_Discount_Cal(S_Customer_Id, d_MSA_Start_Date, d_MSA_End_Date,
        d_Project_Start_Date, d_Project_End_Date, rec_type, o_Customer_Load,
        b_Annual_counter, b_Quater_counter, total_inc, subtotal_exec) //
{
	// nlapiLogExecution('DEBUG', 'Fixed_Discount_Cal',
	// '...Fixed Free Discount Processing...');

	// alert('total_inc'+total_inc);
	var b_validation_count_f = false;
	var b_result = false;
	var i_Fixed_Discount_Rate_Sum = 0;
	var g_Fixed_Inv_Amt_Sum = 0;
	var i_fixed_free_count = o_Customer_Load
	        .getLineItemCount('recmachcustrecord_fpcustparent');
	// nlapiLogExecution('DEBUG', 'Fixed_Discount_Cal', 'i_fixed_free_count : '
	// + i_fixed_free_count);

	// alert('i_fixed_free_count'+i_fixed_free_count);

	if (i_fixed_free_count == '-1' || i_fixed_free_count == 0) //
	{// count check if start
	}// count check if close
	else //
	{// count check else start
		var i_Fixed_Apply_Method = parseFloat(o_Customer_Load.getLineItemValue(
		        'recmachcustrecord_fpcustparent', 'custrecord_applymethod123',
		        1));
		nlapiLogExecution('DEBUG', 'Fixed_Discount_Cal',
		        'i_Fixed_Apply_Method : ' + i_Fixed_Apply_Method);

		var i_Fixed_Discount_Base_Amt = parseFloat(o_Customer_Load
		        .getLineItemValue('recmachcustrecord_fpcustparent',
		                'custrecord_fdbasedamount', 1));
		nlapiLogExecution('DEBUG', 'Fixed_Discount_Cal',
		        'i_Fixed_Discount_Base_Amt : ' + i_Fixed_Discount_Base_Amt);

		if (i_Fixed_Discount_Base_Amt == '1') //
		{
			// /include===
			var i_Subtotal_Val = total_inc;// parseFloat(nlapiGetFieldValue('total'));
			// //
			// alert('i_Subtotal_Val'+i_Subtotal_Val);
		} else {// excluding===
			var i_Subtotal_Val = subtotal_exec;// parseFloat(nlapiGetFieldValue('subtotal'))//;//
			// alert('i_Subtotal_Val'+i_Subtotal_Val);
		}
		nlapiLogExecution('DEBUG', 'Fixed_Discount_Cal', 'i_Subtotal_Val : '
		        + i_Subtotal_Val);
		// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
		if (i_Fixed_Apply_Method == '1') // If invoice is apply method
		{
			var b_R = false;
			var b_fixed = false;

			for (var ss = 1; ss <= i_fixed_free_count; ss++) //
			{
				// var i_Fixed_Discount_Item =
				// parseFloat(o_Customer_Load.getLineItemValue('recmachcustrecord_fpcustparent',
				// 'custrecord_fpdiscountitem', ss));
				var i_Fixed_Discount_Item = o_Customer_Load.getLineItemValue(
				        'recmachcustrecord_fpcustparent',
				        'custrecord_fpdiscountitem', ss);
				nlapiLogExecution('DEBUG', 'Fixed_Discount_Cal',
				        'i_Fixed_Discount_Item : ' + i_Fixed_Discount_Item);
				// alert('i_Fixed_Discount_Item'+i_Fixed_Discount_Item);
				/*
				 * if(i_Fixed_Discount_Item == '5') {
				 * i_Fixed_Discount_Item='2435'; } else {
				 * i_Fixed_Discount_Item='1570'; }
				 */
				var i_Fixed_Discount_Rate = parseFloat(o_Customer_Load
				        .getLineItemValue('recmachcustrecord_fpcustparent',
				                'custrecord_fpdiscountrate', ss));
				// alert('i_Fixed_Discount_Rate' + i_Fixed_Discount_Rate);

				customerDiscountDetails.FixedFeeDiscountPercent = i_Fixed_Discount_Rate;
				nlapiLogExecution('DEBUG', 'Fixed_Discount_Cal',
				        'i_Fixed_Discount_Rate : ' + i_Fixed_Discount_Rate);
				// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

				var i_Fixed_Remittence = (o_Customer_Load.getLineItemValue(
				        'recmachcustrecord_fpcustparent',
				        'custrecord_fpremittancemethod', ss));
				// alert('i_Fixed_Remittence'+i_Fixed_Remittence);
				nlapiLogExecution('DEBUG', 'Fixed_Discount_Cal',
				        'i_Fixed_Remittence : ' + i_Fixed_Remittence);
				if (i_Fixed_Remittence == '1' && b_Annual_counter == true) //
				{
					b_result = true;
				} else if (i_Fixed_Remittence == '2'
				        && b_Quater_counter == true) //
				{
					b_result = true;
				}

				var s_Fixed_Discount_validity = (o_Customer_Load
				        .getLineItemValue('recmachcustrecord_fpcustparent',
				                'custrecord_fddiscountvalidity', ss));
				nlapiLogExecution('DEBUG', 'Fixed_Discount_Cal',
				        's_Fixed_Discount_validity : '
				                + s_Fixed_Discount_validity);
				nlapiLogExecution('DEBUG', 'Fixed_Discount_Cal', 'b_result : '
				        + b_result);
				nlapiLogExecution('DEBUG', 'Fixed_Discount_Cal',
				        'i_Fixed_Remittence : ' + i_Fixed_Remittence);

				if (b_result == false) //
				{
					if (i_Fixed_Remittence == '1' || i_Fixed_Remittence == '2') //
					{
						g_Fixed_Inv_Amt_Sum = 0;
						// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
						// var
						// s_Fixed_Discount_validity=(o_Customer_Load.getLineItemText('recmachcustrecord_fpcustparent','custrecord_fddiscountvalidity',ss));
						// alert('s_Fixed_Discount_validity'+s_Fixed_Discount_validity);
						if (s_Fixed_Discount_validity == '1') //
						{// 6 if start
							// ---pick date from invoice record
							var d_Fixed_Inv_Date = nlapiGetFieldValue('trandate');
							// alert('d_Fixed_Inv_Date'+d_Fixed_Inv_Date);
							var d_Fixed_Date = d_Fixed_Inv_Date;
						}// 6 if close
						else //
						{// 6 else start
							var d_Project_Date = nlapiGetFieldValue('trandate');// d_Project_End_Date;//project_load.getFieldValue('enddate');
							// alert('d_Project_Date'+d_Project_Date);
							var d_Fixed_Date = d_Project_Date;
							// alert('d_date'+d_date);
						}// 6 else close
						var o_Fixed_Inv_Date = nlapiStringToDate(d_Fixed_Date);
						var d_Fixed_Month = o_Fixed_Inv_Date.getMonth() + 1;
						// alert('d_Fixed_Month' + d_Fixed_Month);
						nlapiLogExecution('DEBUG', 'd_Fixed_Month',
						        d_Fixed_Month);
						// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
						if (i_Fixed_Remittence == '1') //
						{
							var s_condition = d_Fixed_Month == '12';
						} else if (i_Fixed_Remittence == '2') //
						{
							// alert('inside else of second method');
							var s_condition = d_Fixed_Month == '3'
							        || d_Fixed_Month == '6'
							        || d_Fixed_Month == '9'
							        || d_Fixed_Month == '12';
						}

						nlapiLogExecution('debug', 'check', 'a');
						nlapiLogExecution('debug', 'i_Fixed_Remittence',
						        i_Fixed_Remittence);

						// alert('s_condition'+s_condition);
						if (s_condition) //
						{// check month if start
							if (rec_type == 'invoice') //
							{
								if (i_Fixed_Remittence == '1') //
								{
									// -------search all invoice created in this
									// year with same customer
									var Inv_Filter = new Array();
									Inv_Filter[0] = new nlobjSearchFilter(
									        'entity', null, 'is', S_Customer_Id);
									// Inv_Filter[1]=new
									// nlobjSearchFilter('job', null,
									// 'is',project_rec_id);
									var o_Invoice_Search = nlapiSearchRecord(
									        'transaction', 217, Inv_Filter,
									        null);
									// alert('o_Invoice_Search.length===='+o_Invoice_Search.length);
									nlapiLogExecution('DEBUG',
									        'o_Invoice_Search.length',
									        o_Invoice_Search.length);
								} else if (i_Fixed_Remittence == '2') //
								{
									// -------search all invoice created in this
									// quater year with same customer
									var Inv_Filter = new nlobjSearchFilter(
									        'entity', null, 'is', S_Customer_Id);
									var o_Invoice_Search = nlapiSearchRecord(
									        'transaction', 216, Inv_Filter,
									        null);
									// alert('o_Invoice_Search.length===='+o_Invoice_Search.length);
									nlapiLogExecution('DEBUG',
									        'o_Invoice_Search.length',
									        o_Invoice_Search.length);
								}
							}

							if (o_Invoice_Search != null) //
							{// 7 if start
								// alert('inside if');
								for (var ks = 0; ks < o_Invoice_Search.length; ks++) // o_Invoice_Search.length
								{// for start
									var o_Inv_Result = o_Invoice_Search[ks];
									var columns = o_Inv_Result.getAllColumns();

									var i_Inv_Tot_Amt = o_Inv_Result
									        .getValue(columns[0]);
									// alert('i_Inv_Tot_Amt' + i_Inv_Tot_Amt);

									g_Fixed_Inv_Amt_Sum = g_Fixed_Inv_Amt_Sum
									        + parseFloat(i_Inv_Tot_Amt);
									// alert('g_Fixed_Inv_Amt_Sum' +
									// g_Fixed_Inv_Amt_Sum);
									nlapiLogExecution('DEBUG',
									        'g_Fixed_Inv_Amt_Sum',
									        g_Fixed_Inv_Amt_Sum);
									//				   
								}// for close
							}// 7 if close
							else //
							{
								g_Fixed_Inv_Amt_Sum = i_Subtotal_Val;
							}
							// ------------------------------------------------------------------

							if (ss == 1)//
							{
								b_R = confirm("Do you really want to apply Fixed Fee Discount ?");
							}

							if (b_R == true) //
							{// confirm box if start
								b_validation_count_f = true;
								// alert('g_Fixed_Inv_Amt_Sum' +
								// g_Fixed_Inv_Amt_Sum);
								var i_Fixed_Amt_After_Dsc_Cal = (g_Fixed_Inv_Amt_Sum)
								        * (i_Fixed_Discount_Rate / 100);

								customerDiscountDetails.FixedFeeDiscount = i_Fixed_Amt_After_Dsc_Cal;

								nlapiLogExecution('DEBUG',
								        'i_Fixed_Amt_After_Dsc_Cal',
								        i_Fixed_Amt_After_Dsc_Cal);
								// alert('i_Fixed_Amt_After_Dsc_Cal' +
								// i_Fixed_Amt_After_Dsc_Cal);

								// -----------add Volume discount in column
								// level---------------------------------------------
								nlapiSelectNewLineItem('item');
								nlapiSetCurrentLineItemValue('item', 'item',
								        constant.Item.FixedFee, true, true);
								nlapiSetCurrentLineItemValue('item',
								        'custcol_discouint_amt',
								        i_Fixed_Amt_After_Dsc_Cal, true, true);
								nlapiSetCurrentLineItemValue('item', 'rate',
								        -i_Fixed_Amt_After_Dsc_Cal, true, true);
								nlapiSetCurrentLineItemValue('item',
								        'price_display', 'Custom', true, true);

								nlapiSetCurrentLineItemValue('item',
								        'department', emp_practice);
								nlapiSetCurrentLineItemValue('item',
								        'custcol_employeenamecolumn', emp_name);
								nlapiSetCurrentLineItemValue('item', 'class',
								        customer_vertical);

								// nlapiSetCurrentLineItemValue('item',
								// 'price','-1',true,true);
								nlapiCommitLineItem('item');
								// --------------------------------------------------------------------------------------------
							}// confirm box if close
							else //
							{
								return true;
							}
						}
					} else if (i_Fixed_Remittence == '3') // transactional
					{

						nlapiLogExecution('debug', 'fixed fee', 'transactional');
						var d_Invoice_Date = new Date(
						        nlapiGetFieldValue('trandate'));

						if (s_Fixed_Discount_validity == '1') //
						{// 6 if start
							// ---pick date from invoice record

							var Start_Date_Tenure = new Date(d_MSA_Start_Date);
							var End_Date_Tenure = new Date(d_MSA_End_Date);
						}// 6 if close
						else //
						{// 6 else start
							var Start_Date_Tenure = new Date(
							        d_Project_Start_Date);
							var End_Date_Tenure = new Date(d_Project_End_Date);
						}// 6 else close
						// alert('Start_Date_Tenure'+Start_Date_Tenure);
						// alert('End_Date_Tenure'+End_Date_Tenure);
						// alert('d_Invoice_Date'+d_Invoice_Date);

						nlapiLogExecution('debug', 'fixed fee', 'a');

						if (Start_Date_Tenure <= d_Invoice_Date
						        && d_Invoice_Date <= End_Date_Tenure) //
						{

							if (ss == 1)//
							{
								nlapiLogExecution('debug', 'fixed fee', 'b');

								b_fixed = true; // confirm("Do really you Want
								// To Update Fixed Fee Discount
								// ?");
							}
							if (b_fixed == true) //
							{
								nlapiLogExecution('debug', 'fixed fee', 'e');
								b_validation_count_f = true;
								var i_Amt_After_Dsc_Cal_Fixed = (i_Subtotal_Val)
								        * (i_Fixed_Discount_Rate / 100);

								customerDiscountDetails.FixedFeeDiscount = i_Amt_After_Dsc_Cal_Fixed;
								// -----------add Volume discount in column
								// level---------------------------------------------
								nlapiSelectNewLineItem('item');
								nlapiSetCurrentLineItemValue('item', 'item',
								        constant.Item.FixedFee, true, true);
								nlapiSetCurrentLineItemValue('item',
								        'custcol_discouint_amt',
								        i_Amt_After_Dsc_Cal_Fixed, true, true);
								nlapiSetCurrentLineItemValue('item', 'rate',
								        -i_Amt_After_Dsc_Cal_Fixed, true, true);
								nlapiSetCurrentLineItemValue('item',
								        'price_display', 'Custom', true, true);
								nlapiSetCurrentLineItemValue('item',
								        'department', emp_practice);
								nlapiSetCurrentLineItemValue('item',
								        'custcol_employeenamecolumn', emp_name);
								nlapiSetCurrentLineItemValue('item', 'class',
								        customer_vertical);

								nlapiLogExecution('debug', 'fixed fee',
								        'item fetched');

								// nlapiSetCurrentLineItemValue('item',
								// 'price','-1',true,true);
								nlapiCommitLineItem('item');

								nlapiLogExecution('debug', 'fixed fee',
								        'item added');
							} else //
							{
								return true;
							}
						} else //
						{
							nlapiLogExecution('debug', 'fixed fee', 'd');

							if (s_Fixed_Discount_validity == '1') //
							{// 6 if start
								alert('Invoice Date Is Not Between MSA Start Date And MSA End Date');

							}// 6 if close
							else //
							{// 6 else start
								alert('Invoice Date Is Not Between Project Start Date And Project End Date');
							}// 6 else close
						}
					}
				}
			}
			if (b_validation_count_f == false) //
			{
				alert('No discount applied due to total amount is not between slabs defined.');
			}
		}// applied method if start
		else //
		{
			alert('Discount not applied as "Apply Discount" checkbox on project is not checked.');
		}
		/*
		 * else
		 * 
		 * {//applied method else start
		 * 
		 * //----------------------if apply method is receipt(credit
		 * memo)-------------------
		 * 
		 * if(i_fixed_free_count != '0')
		 * 
		 * {//applied method if start
		 * 
		 * var
		 * i_Fixed_Remittence=(o_Customer_Load.getLineItemValue('recmachcustrecord_fpcustparent','custrecord_fpremittancemethod',1));
		 * 
		 * nlapiLogExecution('DEBUG','i_Fixed_Remittence',i_Fixed_Remittence);
		 * 
		 * 
		 * 
		 * if(i_Fixed_Remittence == '1') {
		 * 
		 * //-------search all invoice created in this year with same customer
		 * 
		 * var Credit_Memo_Filter=new Array();
		 * 
		 * Credit_Memo_Filter[0]=new nlobjSearchFilter('entity',
		 * null,'is',S_Customer_Id);
		 * 
		 * //Inv_Filter[1]=new nlobjSearchFilter('job',
		 * null,'anyof',project_rec_id);
		 * 
		 * var
		 * o_Credit_Memo_Search=nlapiSearchRecord('transaction',159,Credit_Memo_Filter,null); //
		 * //alert('o_Credit_Memo_Search.length===='+o_Credit_Memo_Search.length);
		 * 
		 * if(o_Credit_Memo_Search != null) {
		 * 
		 * b_result=true; } }
		 * 
		 * else if(i_Fixed_Remittence == '2') {
		 * 
		 * //-------search all invoice created in this quater with same customer
		 * 
		 * var Credit_Memo_Filter=new Array();
		 * 
		 * Credit_Memo_Filter[0]=new nlobjSearchFilter('entity',
		 * null,'is',S_Customer_Id);
		 * 
		 * //Inv_Filter[1]=new nlobjSearchFilter('job',
		 * null,'anyof',project_rec_id);
		 * 
		 * var
		 * o_Credit_Memo_Search=nlapiSearchRecord('transaction',160,Credit_Memo_Filter,null); //
		 * //alert('o_Credit_Memo_Search.length===='+o_Credit_Memo_Search.length);
		 * 
		 * if(o_Credit_Memo_Search != null) {
		 * 
		 * b_result=true; } }
		 * 
		 * if(b_result== false) {
		 * 
		 * for(var ss=1 ; ss<=i_fixed_free_count ; ss++) {
		 * 
		 * var
		 * i_Fixed_Remittence=(o_Customer_Load.getLineItemValue('recmachcustrecord_fpcustparent','custrecord_fpremittancemethod',ss));
		 * 
		 * //alert('i_Fixed_Remittence'+i_Fixed_Remittence);
		 * 
		 * 
		 * 
		 * var
		 * s_Fixed_Discount_validity=(o_Customer_Load.getLineItemValue('recmachcustrecord_fpcustparent','custrecord_fddiscountvalidity',ss));
		 * 
		 * //alert('s_Fixed_Discount_validity'+s_Fixed_Discount_validity); }
		 * 
		 * 
		 * 
		 * if(i_Fixed_Remittence == '1' || i_Fixed_Remittence == '2') {
		 * 
		 * 
		 * 
		 * if(s_Fixed_Discount_validity == '1')
		 * 
		 * {//6 if start
		 * 
		 * //---pick date from invoice record
		 * 
		 * var d_Fixed_Inv_Date=nlapiGetFieldValue('trandate');
		 * 
		 * //alert('d_Fixed_Inv_Date'+d_Fixed_Inv_Date);
		 * 
		 * var d_Fixed_Date=d_Fixed_Inv_Date;
		 * 
		 * 
		 * 
		 * 
		 * 
		 * }//6 if close
		 * 
		 * else
		 * 
		 * {//6 else start
		 * 
		 * 
		 * 
		 * var
		 * d_Project_Date=nlapiGetFieldValue('trandate');//d_Project_End_Date;//project_load.getFieldValue('enddate');
		 * 
		 * //alert('d_Project_Date'+d_Project_Date);
		 * 
		 * var d_Fixed_Date=d_Project_Date;
		 * 
		 * //alert('d_date'+d_date);
		 * 
		 * }//6 else close
		 * 
		 * 
		 * 
		 * var o_Fixed_Inv_Date= new Date(d_Fixed_Date);
		 * 
		 * var d_Fixed_Month=o_Fixed_Inv_Date.getMonth()+1;
		 * 
		 * //alert('d_Fixed_Month'+d_Fixed_Month);
		 * 
		 * //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
		 * 
		 * if(i_Fixed_Remittence == '1') {
		 * 
		 * var s_condition=d_Fixed_Month == '12'; }
		 * 
		 * else if(i_Fixed_Remittence == '2') {
		 * 
		 * //alert('inside else of second method');
		 * 
		 * var s_condition=d_Fixed_Month == '3' || d_Fixed_Month == '6' ||
		 * d_Fixed_Month == '9' || d_Fixed_Month == '12'; }
		 * 
		 * //alert('s_condition'+s_condition);
		 * 
		 * if(s_condition)
		 * 
		 * {//check month if start
		 * 
		 * 
		 * 
		 * var b_R = confirm("Confirm You Want To Apply Fixed Free Discount");
		 * 
		 * if (b_R == true)
		 * 
		 * {//confirm box if start
		 * 
		 * nlapiSetFieldValue('custbody_fixed_free_discount','T');
		 * 
		 * }//confirm box if close } }
		 * 
		 * else if(i_Fixed_Remittence == '3') {
		 * 
		 * 
		 * 
		 * var d_Invoice_Date=new Date(nlapiGetFieldValue('trandate'));
		 * 
		 * 
		 * 
		 * if(s_Fixed_Discount_validity == '1')
		 * 
		 * {//6 if start
		 * 
		 * //---pick date from invoice record
		 * 
		 * 
		 * 
		 * var Start_Date_Tenure=new Date(d_MSA_Start_Date);
		 * 
		 * var End_Date_Tenure=new Date(d_MSA_End_Date);
		 * 
		 * }//6 if close
		 * 
		 * else
		 * 
		 * {//6 else start
		 * 
		 * 
		 * 
		 * var Start_Date_Tenure=new Date(d_Project_Start_Date);
		 * 
		 * var End_Date_Tenure= new Date(d_Project_End_Date);
		 * 
		 * }//6 else close
		 * 
		 * 
		 * 
		 * if(Start_Date_Tenure<=d_Invoice_Date && d_Invoice_Date<=End_Date_Tenure) {
		 * 
		 * //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
		 * 
		 * 
		 * 
		 * 
		 * 
		 * var b_fixed = confirm("Confirm you Want To Update Fixed Free
		 * Discount");
		 * 
		 * if (b_fixed == true) {
		 * 
		 * nlapiSetFieldValue('custbody_fixed_free_discount','T'); } }
		 * 
		 * else {
		 * 
		 * if(s_Fixed_Discount_validity == '1')
		 * 
		 * {//6 if start
		 * 
		 * //alert('Invoice Date Is Not Between MSA Start Date And MSA End
		 * Date');
		 * 
		 * 
		 * 
		 * }//6 if close
		 * 
		 * else
		 * 
		 * {//6 else start
		 * 
		 * 
		 * 
		 * //alert('Invoice Date Is Not Between Project Start Date And Project
		 * End Date');
		 * 
		 * }//6 else close } } }
		 * 
		 * }//applied method if start
		 * 
		 * //--------------------------------------------------------------------------------
		 * 
		 * }//applied method else close
		 */
	}
	// }//count check else close
	nlapiLogExecution('DEBUG', 'Fixed_Discount_Cal',
	        '...Fixed Free Discount completed...');
	return true;
}

// //////////////////////////////////////////////
function validate(value) //
{
	if (value != null && value != '' && value != 'undefined') // 
	{
		return true;
	}
	return false;
}

function getInvoicedHours() {
	try {
		var totalHours = 0;
		var st_hours = 0;
		var ot_hours = 0;
		var ot_rate = 0;
		var st_rate = 0;
		var billRate = 0;
		var index = 0;
		var timeLineCount = nlapiGetLineItemCount('time');
		nlapiLogExecution('DEBUG', 'timeLineCount', timeLineCount);

		for (var linenum = 1; linenum <= timeLineCount; linenum++) {
			var billedDate = nlapiStringToDate(nlapiGetLineItemValue('time',
			        'billeddate', linenum));
			var isApplied = nlapiGetLineItemValue('time', 'apply', linenum) == 'T';
			// nlapiLogExecution('DEBUG', 'field', nlapiGetLineItemValue('time',
			// 'apply', linenum));
			// nlapiLogExecution('DEBUG', 'isApplied', isApplied);

			if (isApplied) {
				// index++;
				// nlapiLogExecution('DEBUG', 'applied', index);

				billRate = parseFloat(nlapiGetLineItemValue('time', 'rate',
				        linenum));
				var hours = parseFloat(nlapiGetLineItemValue('time', 'qty',
				        linenum));
				totalHours += hours;

				var item = nlapiGetLineItemValue('time', 'item', linenum);
				if (item == '2222') { // ST
					st_hours += hours;
					st_rate = billRate;
				} else if (item == '2425') { // OT
					ot_hours += hours;
					ot_rate = billRate;
				}
			}
		}

		customerDiscountDetails.ST_Hours = st_hours;
		customerDiscountDetails.OT_Hours = ot_hours;
		customerDiscountDetails.ST_Rate = st_rate;
		customerDiscountDetails.OT_Rate = ot_rate;
		customerDiscountDetails.TotalHours = totalHours;
	} catch (err) {
		nlapiLogExecution('ERROR', 'getInvoicedHours', err);
		alert(err.message);
		throw err;
	}
}

function saveCustomerDiscountDetails() {
	try {
		var customerDiscountId = nlapiGetFieldValue('custbody_inv_cdd_temp_field');
		var discountDetails = null;

		if (customerDiscountId) {
			discountDetails = nlapiLoadRecord(
			        'customrecord_customer_discount_details',
			        customerDiscountId, {
				        recordmode : 'dynamic'
			        });

			// remove all previous TD lines
			for (var linenum = 1; linenum <= discountDetails
			        .getLineItemCount('recmachcustrecord_tdr_customer_discount'); linenum++) {
				discountDetails.removeLineItem(
				        'recmachcustrecord_tdr_customer_discount', 1);
			}
		} else {
			discountDetails = nlapiCreateRecord(
			        'customrecord_customer_discount_details', {
				        recordmode : 'dynamic'
			        });
		}

		discountDetails.setFieldValue('custrecord_cdd_customer',
		        customerDiscountDetails.Customer);
		discountDetails.setFieldValue('custrecord_cdd_project',
		        customerDiscountDetails.Project);
		discountDetails.setFieldValue('custrecord_cdd_employee',
		        customerDiscountDetails.Employee);

		discountDetails.setFieldValue('custrecord_cdd_po_number',
		        customerDiscountDetails.PurchaseOrderNumber);

		// if (customerDiscountDetails.TotalHours == 0) {
		// getInvoicedHours();
		// }

		// discountDetails.setFieldValue('custrecord_cdd_st_hours',
		// customerDiscountDetails.ST_Hours);
		// discountDetails.setFieldValue('custrecord_cdd_st_amount',
		// customerDiscountDetails.ST_Rate);
		// discountDetails.setFieldValue('custrecord_cdd_ot_hours',
		// customerDiscountDetails.OT_Hours);
		// discountDetails.setFieldValue('custrecord_cdd_ot_amount',
		// customerDiscountDetails.OT_Rate);
		// discountDetails.setFieldValue('custrecord_cdd_total_hours',
		// customerDiscountDetails.TotalHours);
		discountDetails.setFieldValue('custrecord_cdd_gross_amount',
		        customerDiscountDetails.TotalAmount);

		discountDetails.setFieldValue('custrecord_cdd_fixed_fee_percent',
		        customerDiscountDetails.FixedFeeDiscountPercent);
		discountDetails.setFieldValue('custrecord_cdd_fixed_fee_discount',
		        customerDiscountDetails.FixedFeeDiscount);

		// discountDetails.setFieldValue('custrecord_cdd_tenure_disc_percent',
		// // customerDiscountDetails.TenureDiscountPercentA);
		// discountDetails.setFieldValue('custrecord_cdd_tenure_disc_percent_b',
		// customerDiscountDetails.TenureDiscountPercentB);
		// discountDetails.setFieldValue('custrecord_cdd_tenure_disc_amt_a',
		// customerDiscountDetails.TenureDiscountAmountA);
		// discountDetails.setFieldValue('custrecord_cdd_tenure_disc_amt_b',
		// customerDiscountDetails.TenureDiscountAmountB);
		// discountDetails.setFieldValue('custrecord_cdd_tenure_discount',
		// customerDiscountDetails.TenureDiscount);

		discountDetails.setFieldValue('custrecord_cdd_volume_discount_percent',
		        customerDiscountDetails.VolumeDiscountPercentA);
		discountDetails.setFieldValue(
		        'custrecord_cdd_volume_discount_percent_b',
		        customerDiscountDetails.VolumeDiscountPercentB);
		discountDetails.setFieldValue('custrecord_cdd_volume_discount_amt_a',
		        customerDiscountDetails.VolumeDiscountAmountA);
		discountDetails.setFieldValue('custrecord_cdd_volume_discount_amt_b',
		        customerDiscountDetails.VolumeDiscountAmountB);
		discountDetails.setFieldValue('custrecord_cdd_volume_discount',
		        customerDiscountDetails.VolumeDiscount);

		discountDetails.setFieldValue('custrecord_cdd_old_cumulative_hours',
		        customerDiscountDetails.CumulativeHoursOld);
		discountDetails.setFieldValue('custrecord_cdd_new_cumulative_hours',
		        customerDiscountDetails.CumulativeHoursNew);
		discountDetails.setFieldValue('custrecord_cdd_old_cumulative_sales',
		        customerDiscountDetails.CumulativeSalesOld);
		discountDetails.setFieldValue('custrecord_cdd_new_cumulative_sales',
		        customerDiscountDetails.CumulativeSalesNew);

		discountDetails.setFieldValue('custrecord_cdd_net_discount',
		        customerDiscountDetails.TotalDiscount);
		discountDetails.setFieldValue('custrecord_cdd_final_invoice_amount',
		        customerDiscountDetails.NetInvoiceAmount);

		// add the tenure discount lines
		for (var i = 0; i < tenureDiscountReportDetails.length; i++) {
			discountDetails
			        .selectNewLineItem("recmachcustrecord_tdr_customer_discount");
			discountDetails.setCurrentLineItemValue(
			        "recmachcustrecord_tdr_customer_discount",
			        "custrecord_tdr_employee",
			        tenureDiscountReportDetails[i].Employee);
			discountDetails.setCurrentLineItemValue(
			        "recmachcustrecord_tdr_customer_discount",
			        "custrecord_tdr_percent_a",
			        tenureDiscountReportDetails[i].PercentA);
			discountDetails.setCurrentLineItemValue(
			        "recmachcustrecord_tdr_customer_discount",
			        "custrecord_tdr_amount_a",
			        tenureDiscountReportDetails[i].AmountA);
			discountDetails.setCurrentLineItemValue(
			        "recmachcustrecord_tdr_customer_discount",
			        "custrecord_tdr_percent_b",
			        tenureDiscountReportDetails[i].PercentB);
			discountDetails.setCurrentLineItemValue(
			        "recmachcustrecord_tdr_customer_discount",
			        "custrecord_tdr_amount_b",
			        tenureDiscountReportDetails[i].AmountB);
			discountDetails.setCurrentLineItemValue(
			        "recmachcustrecord_tdr_customer_discount",
			        "custrecord_tdr_final_discount_percent",
			        tenureDiscountReportDetails[i].FinalPercent);
			discountDetails.setCurrentLineItemValue(
			        "recmachcustrecord_tdr_customer_discount",
			        "custrecord_tdr_final_discount_amt",
			        tenureDiscountReportDetails[i].FinalAmount);
			discountDetails.setCurrentLineItemValue(
			        "recmachcustrecord_tdr_customer_discount",
			        "custrecord_tdr_old_hours",
			        tenureDiscountReportDetails[i].OldHours);
			discountDetails.setCurrentLineItemValue(
			        "recmachcustrecord_tdr_customer_discount",
			        "custrecord_tdr_new_hours",
			        tenureDiscountReportDetails[i].NewHours);
			discountDetails.setCurrentLineItemValue(
			        "recmachcustrecord_tdr_customer_discount",
			        "custrecord_tdr_st_hours",
			        tenureDiscountReportDetails[i].ST_Hours);
			discountDetails.setCurrentLineItemValue(
			        "recmachcustrecord_tdr_customer_discount",
			        "custrecord_tdr_ot_hours",
			        tenureDiscountReportDetails[i].OT_Hours);
			discountDetails.setCurrentLineItemValue(
			        "recmachcustrecord_tdr_customer_discount",
			        "custrecord_tdr_st_rate",
			        tenureDiscountReportDetails[i].ST_Rate);
			discountDetails.setCurrentLineItemValue(
			        "recmachcustrecord_tdr_customer_discount",
			        "custrecord_tdr_ot_rate",
			        tenureDiscountReportDetails[i].OT_Rate);

			discountDetails
			        .commitLineItem("recmachcustrecord_tdr_customer_discount");
		}

		var id = nlapiSubmitRecord(discountDetails);
		nlapiLogExecution('debug',
		        'customer discount record created / updated', id);

		nlapiSetFieldValue('custbody_inv_cdd_temp_field', id);
	} catch (err) {
		nlapiLogExecution('ERROR', 'saveCustomerDiscountDetails', err);
		alert("Discount Report : " + JSON.stringify(err));
		throw err;
	}
}

function CustomerDiscountObject() {
	this.Invoice = '';
	this.Customer = '';
	this.Project = '';
	this.Employee = '';
	this.ST_Hours = 0;
	this.ST_Rate = 0;
	this.OT_Hours = 0;
	this.OT_Rate = 0;
	this.TotalHours = 0;
	this.TotalAmount = 0;
	this.PreviousInvoiceNumber = '';
	this.PurchaseOrderNumber = '';
	this.TotalDiscount = 0;
	this.NetInvoiceAmount = 0;

	this.CumulativeHoursOld = 0;
	this.CumulativeHoursNew = 0;

	this.CumulativeSalesOld = 0;
	this.CumulativeSalesNew = 0;

	this.FixedFeeDiscountPercent = 0;
	this.FixedFeeDiscount = 0;

	this.TenureDiscountPercentA = 0;
	this.TenureDiscountPercentB = 0;
	this.TenureDiscountAmountA = 0;
	this.TenureDiscountAmountB = 0;
	this.TenureDiscount = 0;

	this.VolumeDiscountPercentA = 0;
	this.VolumeDiscountPercentB = 0;
	this.VolumeDiscountAmountA = 0;
	this.VolumeDiscountAmountB = 0;
	this.VolumeDiscount = 0;
}