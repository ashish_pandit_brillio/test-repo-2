// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
/*
   	Script Name:Sut_Vendor_Statment_updated
	Author:		Nikhil jain
	Company:	Aashna Cloudtech Pvt Ltd
	Date:		21-04-2015
	Description:create an vendor statment Criteria form


	Script Modification Log:

	-- Date --			-- Modified By --				--Requested By--				-- Description --



Below is a summary of the process controls enforced by this script file.  The control logic is described
more fully, below, in the appropriate function headers and code blocks.


     SUITELET
		- suiteletFunction(request, response)


     SUB-FUNCTIONS
		- The following sub-functions are called by the above core functions in order to maintain code
            modularization:

               - NOT USED

*/
}
// END SCRIPT DESCRIPTION BLOCK  ====================================



// BEGIN GLOBAL VARIABLE BLOCK  =====================================
{
	//  Initialize any Global Variables, in particular, debugging variables...




}
// END GLOBAL VARIABLE BLOCK  =======================================





// BEGIN SUITELET ==================================================

function prepareVendorStatementCriteria()
{
	nlapiLogExecution('DEBUG','prepareVendorStatementCriteria', "Request Method = " +request.getMethod());

	if(request.getMethod() == 'GET')
	{
		//============== CREATE FORM ============
		var form = nlapiCreateForm("Print Vendor Statement");
		var vendorName = form.addField('vendorname', 'multiselect', 'Vendor', 'vendor');
		vendorName.setMandatory(true);
		
		// ======= ADD FIELDS ========
		
		var subsidiary = form.addField('subsidiary', 'select', 'subsidiary', 'subsidiary');
		subsidiary.setMandatory(true);
		
		var email = form.addField('email', 'email', 'Email ID');
		//email.setMandatory(true);
		
		//var locationField = form.addField('locationname', 'select', 'Location', 'location');
		
		var startDateField = form.addField('startdate', 'date', 'Start Date');
		startDateField.setMandatory(true);
		startDateField.setLayoutType('normal','startcol')
		
		var endDateField = form.addField('enddate', 'date', 'End Date');
		endDateField.setMandatory(true);
		
		var currency = form.addField('currency', 'select', 'Currency');
		currency.addSelectOption('1','INR');
		currency.addSelectOption('2','MultiCurrency');

		// ==== CALL A CLIENT SCRIPT ====
		form.setScript('customscript_cli_vendorstatement');
       
	    // ==== ADD A BUTTON =====
	   	form.addButton('custombutton', 'Generate statement in PDF Format', 'clientScriptPDF_createrecord()');
		
		form.addButton('custombutton', 'Generate statement in CSV Format', 'clientScriptCSV_createrecord()');
		
		form.addButton('custombutton', 'Search Previous Report', 'Search_last_report()');
		
		//form.addButton('custombutton', 'Download CSV', 'download_csv()');
		
      	response.writePage(form);

      	
		

	}// if(request.getMethod() == 'GET')
	else if(request.getMethod() == 'POST')
	{
		
	} 
	else
	{
	    //===WRITE A RESPONSE ======
		var pageNotFound = '<html><body>404-Page Not Found</body></html>';
	    response.write(pageNotFound);
	} // END else
}// END prepareVendorStatementCriteria()


function  HTMLDesign_Vendor_statPDF_Callschedule(request, response)
{	
	if (request.getMethod() == 'GET') 
	{
	
		var params = new Array();
		var record_id = request.getParameter('venstatsubmitid');
		if (record_id != null && record_id != undefined && record_id != '') 
		{
		params['custscript_ait_venstatid'] = record_id;
		var status = nlapiScheduleScript('customscript_genrate_ved_statment', null, params);
		nlapiLogExecution('DEBUG', 'Script Scheduled ', ' Script Status -->' + status);
		
		nlapiSetRedirectURL('RECORD', 'customrecord_vendor_statement_detail_rec', record_id, 'VIEW');
	}
	}
	
}// END prepareVendorStatementCriteria()
function  HTMLDesign_Vendor_statXL_Callschedule(request, response)
{	
	if(request.getMethod() == 'GET')
	{
		var params = new Array();
		var record_id = request.getParameter('venstatsubmitid');
		
		if (record_id != null && record_id != undefined && record_id != '') 
		{
			params['custscript_ait_csvvenstatid'] = record_id;
			var status = nlapiScheduleScript('customscript_genrate_ved_statment_xl', null, params);
			nlapiLogExecution('DEBUG', 'Script Scheduled ', ' Script Status -->' + status);
			
			nlapiSetRedirectURL('RECORD', 'customrecord_vendor_statement_detail_rec', record_id, 'VIEW');
		}
	}// if(request.getMethod() == 'GET')
	
	
}// END prepareVendorStatementCriteria()



// END SUITELET ====================================================




// BEGIN OBJECT CALLED/INVOKING FUNCTION ===================================================
{


}
// END OBJECT CALLED/INVOKING FUNCTION =====================================================
