// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
/*
   	Script Name:
	Author:
	Company:
	Date:
	Description:


	Script Modification Log:

	-- Date --			-- Modified By --				--Requested By--				-- Description --



Below is a summary of the process controls enforced by this script file.  The control logic is described
more fully, below, in the appropriate function headers and code blocks.


     SUITELET
		- suiteletFunction(request, response)


     SUB-FUNCTIONS
		- The following sub-functions are called by the above core functions in order to maintain code
            modularization:

               - NOT USED

*/
}
// END SCRIPT DESCRIPTION BLOCK  ====================================



// BEGIN GLOBAL VARIABLE BLOCK  =====================================
{
	//  Initialize any Global Variables, in particular, debugging variables...




}
// END GLOBAL VARIABLE BLOCK  =======================================





// BEGIN SUITELET ==================================================

function suiteletFunction_genrate_email_etds(request, response)
{

	/*  Suitelet:

          - EXPLAIN THE PURPOSE OF THIS FUNCTION

		-

		FIELDS USED:

          --Field Name--				--ID--


	*/


	//  LOCAL VARIABLES


    //  SUITELET CODE BODY
	
	var record_id = request.getParameter('record_id')
	nlapiLogExecution('DEBUG', 'create_quartely_detailrecord', 'record_id->' + record_id)
	
	if (record_id != null && record_id != '' && record_id != undefined) 
	{
		var params = new Array();
		params['runasadmin'] = 'T';
		params['custscript_recordid'] = record_id;
		params['custscript_sent_etdsmail'] = 'T';
		
		var status = nlapiScheduleScript('customscript_genrate_etds', null, params);
		nlapiLogExecution('DEBUG', 'Script Scheduled ', ' Script Status -->' + status);
		
		nlapiSetRedirectURL('RECORD', 'customrecord_quarterly_return_details',record_id,'VIEW');
		//nlapiSetRedirectURL('SUITELET', 'customscript_sut_genrate_etds', '1', false);
	}
}

// END SUITELET ====================================================




// BEGIN OBJECT CALLED/INVOKING FUNCTION ===================================================
{



}
// END OBJECT CALLED/INVOKING FUNCTION =====================================================
