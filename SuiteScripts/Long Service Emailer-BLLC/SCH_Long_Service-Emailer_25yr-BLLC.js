/**
 * Module Description
 * 
 * Version      Date                Author                          Remarks  
 * 1.00         27th Nov 2015       ManiKandan V        Send Long Service mailer-25year to BLLC Employees scheduled at 9 a.m. U.S. Time Zone
 */

function Main(){
try{

var columns = new Array();
columns[0]=new nlobjSearchColumn('firstname');
columns[1]=new nlobjSearchColumn('email');
columns[2]=new nlobjSearchColumn('email','custentity_reportingmanager');
//columns[3]=new nlobjSearchColumn('custrecord_hrbusinesspartner','department');

// search for employee with 1st year of service as of today

var emp_Oneyr_search = nlapiSearchRecord('employee', 'customsearch_longservicemailer_25yr_bllc', null,columns);

nlapiLogExecution('debug', 'emp_Oneyr_search.length',emp_Oneyr_search.length);

if(isNotEmpty(emp_Oneyr_search)){
	
	for(var i=0;i<emp_Oneyr_search.length;i++){
		
		// get the HR BP email id
		/*var hr_bp_id = emp_Oneyr_search[i].getValue('custrecord_hrbusinesspartner','department');
		var hr_bp_email = null;
		
		if(isNotEmpty(hr_bp_id)){
			hr_bp_email = nlapiLookupField('employee',hr_bp_id, 'email');
		}*/
		
		sendServiceMails(
			emp_Oneyr_search[i].getValue('firstname'),
			emp_Oneyr_search[i].getValue('email'),
			emp_Oneyr_search[i].getValue('email','custentity_reportingmanager'),
			emp_Oneyr_search[i].getId());
	}
}

}
catch(err){
nlapiLogExecution('error', 'Main', err);
}
}

function sendServiceMails(firstName, email, reporting_mng_email,emp_id){
	
	try{
		var mailTemplate = serviceTemplate(firstName);		
		nlapiLogExecution('debug', 'chekpoint',email);
		nlapiSendEmail('10730', email, mailTemplate.MailSubject, mailTemplate.MailBody,[reporting_mng_email,'unni.kondathil@brillio.com'],null, {entity: emp_id});
	}
	catch(err){
nlapiLogExecution('error', 'sendServiceMails', err);
throw err;
     }
}

function serviceTemplate(firstName) {
    var htmltext = '';
    
    htmltext += '<table border="0" width="100%"><tr>';
    htmltext += '<td colspan="4" valign="top">';
    //htmltext += '<p>Hi ' + firstName + ',</p>';  
htmltext += '<p>Dear ' + firstName + ',</p>';
//htmltext += '&nbsp';
 
//htmltext += '<p>Congratulations on completion of'+" "+1+" "+ 'year in Brillio!!</p>';
//htmltext += &nbsp;
//htmltext += '<p>Wishing you a day filled with happiness and a life filled with joy</p>';
htmltext += '<p><img src="https://system.na1.netsuite.com/core/media/media.nl?id=83795&c=3883006&h=3ea675e14fff938bac95&whence=" alt="Right click to download image." /></p>';
htmltext += '<p>With Best Wishes,</p>';
htmltext += '<p>HR & MANAGEMENT TEAM @ BRILLIO</p>';
 
 
    htmltext += '</td></tr>';
    htmltext += '</table>';
    htmltext += '<hr width="100%" size="1" noshade color="#CCCCCC">';
    htmltext += '<table width="100%" border="0" cellspacing="0" cellpadding="0">';
    htmltext += '<tr>';
    htmltext += '<td align="right">';
    htmltext += '<font style="font-size:9px; font-family:Verdana,Arial,Helvetica,sans-serif; color:#999999;">Brillio is powered by <a href="http://www.netsuite.com/" style="color:#999999;">NetSuite</a> One System. No Limits.</font>';
    htmltext += '</td>';
    htmltext += '</tr>';
    htmltext += '</table>';
    htmltext += '</body>';
    htmltext += '</html>';
 
    return {
        MailBody : htmltext,
        MailSubject : "Congrats on achieving a new milestone!"      
    };
}

