/**
 * Operation Excellence Metrics
 * 
 * Version Date Author Remarks 1.00
 * 
 * 12 Aug 2016 Deepak MS
 * 14 Apr 2020	Praveena changed searched record type as timebill and changed url triggering to display export data button
 */

function suitelet(request, response) {
	try {
		
	var current_date = nlapiDateToString(new Date());
	nlapiLogExecution('DEBUG', 'Current Date', 'current_date...' + current_date);
	var form = nlapiCreateForm('Invoicing Report');
	
	form.addFieldGroup('custpage_grp_1', 'Link');
	form.addFieldGroup('custpage_grp_2', 'Table');
	
	//var mode = request.getParameter('mode');
	
	var projectID = request.getParameter('custpage_project_id');
	var startdate = request.getParameter('custpage_start_date');
	var enddate = request.getParameter('custpage_end_date');
	var mode = request.getParameter('mode');
	
	nlapiLogExecution('DEBUG', 'Current Date', 'projectID...' + projectID);
	nlapiLogExecution('DEBUG', 'Current Date', 'startdate...' + startdate);
	nlapiLogExecution('DEBUG', 'Current Date', 'enddate...' + enddate);
	nlapiLogExecution('DEBUG', 'mode', 'mode...' + mode);
/*	var projectID = '11171';
	var mode = 'Excel';
	var startdate = '6/1/2014';
	var enddate = '6/31/2014'*/
	// Add Generate Excel Field
	/*var csvLinkField = form.addField('custpage_download', 'inlinehtml', '',
	        null, 'custpage_grp_3');
	csvLinkField.setBreakType('startcol');
	//csvLinkField.setDefaultValue(csvLink);//var enddate =  '8/19/2014';
	
	
	var s_parameter = "&custpage_project_id=" + projectID
    + "&custpage_start_date=" + startdate
    + "&custpage_end_date=" + enddate;
    

	var excelImportUrl = nlapiResolveURL('SUITELET',
    'customscript_sut_invoice_employee_report', 'customdeploy_sut_invoice_employee_report');
	csvLinkField.setDefaultValue('<a href="' + excelImportUrl
    + '&mode=excel' + s_parameter
    + '" target="_blank"></a>');*/

	var csvLink = "<a href='/app/site/hosting/scriptlet.nl?script=994&deploy=1"
        + "&custpage_project_id="
        + projectID
        + "&custpage_start_date="
        + startdate
        + "&custpage_end_date="
        + enddate
        + "&mode=CSV' target='_blank'>"
        
		//commented by praveena
		//+ "<img style='height: 40px; margin-top: 20px;' src='https://debugger.sandbox.netsuite.com/core/media/media.nl?id=145877&c=3883006&h=db69121d869e7d0ba460'></a>"; //https://system.na1.netsuite.com/core/media/media.nl?id=145877&c=3883006&h=db69121d869e7d0ba460

	+ "<img style='height: 40px; margin-top: 20px;' src='https://system.na1.netsuite.com/core/media/media.nl?id=145877&c=3883006&h=db69121d869e7d0ba460'></a>"; //
	
	var csvLinkField = form.addField('custpage_download', 'inlinehtml', '',
        null, 'custpage_grp_1');
	csvLinkField.setBreakType('startcol');
	csvLinkField.setDefaultValue(csvLink);
	/*if (mode == 'excel') {
	exportToExcel(a_data, chk_show_ot_hours);
	return;// response.write('Test')
	}*/
	
	 var projectName = nlapiLookupField('job',projectID,['companyname']);
	 var pro_Name = projectName.companyname;
	var resourcesList =  searchResourceAllocation(projectID,startdate,enddate);
	var timeEntryList = searchTimeCardEntry(resourcesList,startdate,enddate,projectID);
	if (mode == 'CSV') {
		var csvText = generateTableCsv(timeEntryList);
		var fileName = 'Invoice For Project' +' '+ pro_Name
		        + '.csv';
		var file = nlapiCreateFile(fileName, 'CSV', csvText);
		response.setContentType('CSV', fileName);
		response.write(file.getValue());	
		
	}
	else{
	var html = createHtmlTable(timeEntryList);
	var tableField = form.addField('custpage_html1', 'inlinehtml', null,null, 'custpage_grp_2')
			tableField.setDefaultValue(html);
			tableField.setBreakType('startcol');
			response.writePage(form);
	}
}
	
	
	catch(e){
	
	nlapiLogExecution('DEBUG','Process Error',e);
	}
}

function generateTableCsv(timeEntryList_1){
	try{
		var html = "";
		
		html += "Project,";
		html += "Employee,";
		html += "Practice,";
		html += "Vertical,";
		html += "Rate,";
		html += "Total TimeSheet Hrs,";
		html += "Invoice Amount,";
		html += "\r\n";

		// table header
		
		for(var v=0;v<timeEntryList_1.length;v++){
			var employeeData = timeEntryList_1[v];
			var Amt = employeeData.Bill_rate * employeeData.TotalHrs;
			
			
			html += employeeData.Project;
			html += ",";
			html += employeeData.Employee;
			html += ",";
			html += employeeData.PracticeR;
			html += ",";
			html += employeeData.VericalR;
			html += ",";
			html += employeeData.Bill_rate;
			html += ",";
			html += employeeData.TotalHrs;
			html += ",";
			html += Amt;
			html += "\r\n";
		}
		
		return html;
	}
	catch(e){
		nlapiLogExecution('DEBUG','Excel Process Error',e);	
		
	}
	
}
	function searchResourceAllocation(projectID,start_date, enddate){
	try{
	var st_Date = nlapiStringToDate(start_date);
	var en_Date = nlapiStringToDate(enddate);
	var filters = Array();
	filters.push(new nlobjSearchFilter('internalid', null, 'is', projectID));
	filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
	filters.push(new nlobjSearchFilter('enddate', 'resourceallocation', 'onorafter',en_Date));
	/*var filters = [
	               	new nlobjSearchFilter('internalid', null, 'is', projectID),
			        new nlobjSearchFilter('formuladate', 'resourceallocation', 'notafter',
			        		en_Date).setFormula('{startdate}'),
			        new nlobjSearchFilter('formuladate', 'resourceallocation', 'notbefore',
			        		st_Date).setFormula('{enddate}')
			         ];*/

	var cols = Array();
	cols.push(new nlobjSearchColumn('custentity_vertical'));
	cols.push(new nlobjSearchColumn('resource','resourceallocation'));
	cols.push(new nlobjSearchColumn('custevent3','resourceallocation'));//custevent_practice
	cols.push(new nlobjSearchColumn('custevent_practice','resourceallocation'));
	//cols.push(new nlobjSearchColumn('custevent3','resourceallocation'));
	

	var projectObj = nlapiSearchRecord('job',null,filters,cols);
	var dataRows =[];
	var resourceList = {};
	if(projectObj){
	for(var i=0;i<projectObj.length;i++){
	resourceList = {
	Vertical: projectObj[i].getText('custentity_vertical'),
	Resource: projectObj[i].getValue('resource','resourceallocation'),
	BillRate: projectObj[i].getValue('custevent3','resourceallocation'),
	Practice: projectObj[i].getText('custevent_practice','resourceallocation')
	
	
	
	};
	dataRows.push(resourceList);
	}
	}
	
	return dataRows;
	}
	catch(e){
	nlapiLogExecution('DEBUG','Resource Process Error',e);
	
	}
	}
	
	function searchTimeCardEntry(resourceList,startdate,enddate, project_ID){
	try{
	var jsonObj ={};
	var stDate = nlapiStringToDate(startdate);
	var enDate = nlapiStringToDate(enddate);
	var dataObj =[];
	if(resourceList){
	for(j=0;j<resourceList.length;j++){
	var resources = resourceList[j];
	
	var employeeID = resources.Resource;
	//var employee =5055;
	 if(employeeID =='' || employeeID == null){
	 throw "No Resource Tagged to Project";
	 
	 }
	 var projectName = nlapiLookupField('job',project_ID,['companyname']);
	 var lookUp = nlapiLookupField('employee',employeeID,['entityid']);
	var emp = lookUp.entityid;
	var filters_1 = Array();
	filters_1[0] = new nlobjSearchFilter('employee', null, 'anyof', employeeID) ;
	filters_1[1] = new nlobjSearchFilter('date', null, 'within', stDate , enDate );

	var cols_1 = Array();
	cols_1[0] = new nlobjSearchColumn('durationdecimal');
	cols_1[1] = new nlobjSearchColumn('rate');
	
	
	
	
	var timeentered = 0;
	var counter = 0;
	var total_hrs = 0 ;
	var count = 0.00;
	var timeEntryObj = nlapiSearchRecord('timebill','customsearch1495_2',filters_1,cols_1);//Repalced record type and ids by praveena
	//timeentered = timeEntryObj[0].getValue('durationdecimal');
	if(timeEntryObj){
	//for(var n=0;n<timeEntryObj.length;n++){
	//var approvalstatus = timeEntryObj[n].getValue('');
//}
//}
	for(var n=0;n<timeEntryObj.length;n++){
	
	
	timeentered = timeEntryObj[n].getValue(cols_1[0]);
	count = parseFloat(timeentered);
	var rate_1  = timeEntryObj[n].getValue(cols_1[1]);
	//var date  = timeEntryObj[n].getValue(cols_1[2]);
	//var billingstatus  = timeEntryObj[n].getValue(cols_1[3]);//2753
	//if(approvalstatus =='Approved' && billingstatus =='Unbilled'  ) //&&  date >= startdate  &&  date <= enddate
	//{
	total_hrs +=   count;
	counter ++;
	//}
	//}
	//}
	//if(timeEntryObj){
	//for(var n=0;n<timeEntryObj.length;n++){
	//var time  = timeEntryObj[n].getValue(cols_1[0]);
	//totalHrs = + ParseInt(time);
		}
	}
	//var timecard = timecardSearch(employee,stDate,enDate);
	jsonObj ={
	Project: projectName.companyname,
	Employee:	emp,
	Bill_rate:  rate_1,
	PracticeR: resources.Practice,
	VericalR: resources.Vertical,
	TotalHrs:	total_hrs
	};
	dataObj.push(jsonObj);
	nlapiLogExecution('DEBUG','jsonObj',JSON.stringify(jsonObj));
	nlapiLogExecution('DEBUG','jsonObj',JSON.stringify(jsonObj));
	}
	}
	
	return dataObj;
	}
	catch(e){
	nlapiLogExecution('DEBUG','Resource Process Error',e);
	}
	
	}
	
	function timecardSearch(empID,d_st_date,d_end_date){
	
	try{
	var filters = Array();
	filters.push(new nlobjSearchFilter('employee', null, 'is', empID));
	filters.push(new nlobjSearchFilter('approvalstatus', null, 'is', 3));
	filters.push(new nlobjSearchFilter('date', null, 'within', d_st_date , d_end_date ));

	var cols = Array();
	//cols.push(new nlobjSearchColumn('durationdecimal',null,'count'));
	
	

	var timeEntryObj = nlapiSearchRecord('timebill','customsearch1494_2',filters,null);//Repalced search record type and internal id
	//var timeCount = timeEntryObj[0].getValue('durationdecimal',null,'count');
	return timeEntryObj;
	}
catch(e){
nlapiLogExecution('DEBUG','Time Process Error',e);
}	
	}
	
	function createHtmlTable(resouceList){
	
	try{
	var html = "";
	// table css
	html += "<style>";
	html += ".ops-metrics-table td {border:1px solid black;}";
	html += ".ops-metrics-table tr:nth-child(even) {background: #ffffe6}";
	html += ".ops-metrics-table tr:nth-child(odd) {background: #e6ffe6}";
	html += ".ops-metrics-table tr:nth-child(even):hover {background: #ffff99}";
	html += ".ops-metrics-table tr:nth-child(odd):hover {background: #80ff80}";
	html += ".ops-metrics-table .header td {background-color: #001a33 !important; color : white !important;}";
	html += "</style>";

	// table
	html += "<table class='ops-metrics-table' width='200%' >";

	html += "<tr class='header'>";

	html += "<td font-size='20'>";
	html += "Project ID";
	html += "</td>";
	
	html += "<td font-size='20'>";
	html += "Employee";
	html += "</td>";
	
	html += "<td font-size='20'>";
	html += "Practice";
	html += "</td>";
	
	html += "<td font-size='20'>";
	html += "Vertical";
	html += "</td>";

	html += "<td  font-size='20'>";
	html += "Rate";
	html += "</td>";
	
	html += "<td  font-size='20'>";
	html += "Total Time Sheet Hrs";
	html += "</td>";
	
	html += "<td font-size='20'>";
	html += "Total Invoice Amount";
	html += "</td>";
	
	html += "</tr>";	
	// table header
	
	
		for(var k=0;k<resouceList.length;k++){
		var employeeData = resouceList[k];
		
		var Amt = employeeData.Bill_rate * employeeData.TotalHrs;
		
		html += "<tr>";
		html += "<td font-size='14'>";
				html += employeeData.Project;
				html += "</td>";
				
				html += "<td font-size='14'>";
				html += employeeData.Employee;
				html += "</td>";
				
				html += "<td font-size='14'>";
				html += employeeData.PracticeR;
				html += "</td>";
				
				html += "<td font-size='14'>";
				html += employeeData.VericalR;
				html += "</td>";

				html += "<td font-size='14'>";
				html += employeeData.Bill_rate;
			//	html += "<p>View<a href  ="https://forms.na1.netsuite.com/app/site/hosting/scriptlet.nl?script=681&deploy=1&compid=3883006&h=889d72db68c85c6ca6a0" ></p>";
				html += "</td>";
				
				html += "<td font-size='14'>";
				html += employeeData.TotalHrs;
				html += "</td>";
				
				html += "<td font-size='14'>";
				html += Amt;
				html += "</td>";
				
		html += "</tr>";
		}
	html += "</table>";

	return html;
	}
	catch(e){
	nlapiLogExecution('DEBUG','Time Process Error',e);
	}
	
	}
	
	