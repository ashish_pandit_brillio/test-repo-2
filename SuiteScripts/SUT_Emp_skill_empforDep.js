/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       19 Jul 2017     bidhi.raj
 *
 */

/**
 * @param {nlobjRequest} request Request object
 * @param {nlobjResponse} response Response object
 * @returns {Void} Any output is written via response object
 */
function suitelet(request, response){
	
	
	try
	{
		var i_department = request.getParameter('i_department_interal_id');
		var filters =[];
		filters.push(new nlobjSearchFilter('department',null,'anyof',parseInt(i_department)));
		filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
		
		var cols = [];
		cols.push(new nlobjSearchColumn('internalid'));
		cols.push(new nlobjSearchColumn('entityid'));
		cols.push(new nlobjSearchColumn('custentity_employee_primary_skill'));
		cols.push(new nlobjSearchColumn('custentity_employee_secondary_skill'));
		cols.push(new nlobjSearchColumn('custentity_course_status'));
      cols.push(new nlobjSearchColumn('custentity_competency'));
      
		
		var searchRes = nlapiSearchRecord('employee',null,filters,cols);
		var emp_array = [];
		for(var i=0;i<searchRes.length;i++){
			emp_array[i] = searchRes[i];
		}
		nlapiLogExecution('ERROR','searchRes length',searchRes.length);
		response.write(JSON.stringify(emp_array));
	}
	catch(err)
	{
		nlapiLogExecution('ERROR','EmpforDepartment','ERROR MESSAGE :- '+err);
	}
	}
	

