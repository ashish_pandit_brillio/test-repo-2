/**
 * Check if the calculations should be done for this project for this month
 * 
 * Version Date Author Remarks
 * 
 * 1.00 17 Jun 2016 Nitish Mishra
 * 
 */

function userEventAfterSubmit(type) {
	try {
		if (type == 'create' || type == 'edit' || type == 'xedit') {

			var startDate = nlapiGetFieldValue('custrecord_urt_start_date');
			var endDate = nlapiGetFieldValue('custrecord_urt_end_date');
			var project = nlapiGetFieldValue('custrecord_urt_project');

			if (startDate && endDate && project) {
				var projectDetails = nlapiLookupField('job', project, [
				        'startdate', 'enddate' ]);

				var recordStartDate = nlapiStringToDate(startDate);
				var recordEndDate = nlapiStringToDate(endDate);
				var projectStartDate = nlapiStringToDate(projectDetails.startdate);
				var projectEndDate = nlapiStringToDate(projectDetails.enddate);

				// if project is not started or active in this month
				if (projectStartDate > recordEndDate) {
					nlapiSubmitField(nlapiGetRecordType(), nlapiGetRecordId(),
					        'isinactive', 'T');
					nlapiLogExecution('debug', 'inactivate', nlapiGetRecordId());
				}
			} else {
				throw 'Mandatory fields missing';
			}
		}
	} catch (err) {
		nlapiLogExecution('ERROR', 'userEventAfterSubmit', err);
	}
}
