/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       12 Dec 2014     amol.sahijwani
 *
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType 
 * 
 * @param {String} type Access mode: create, copy, edit
 * @returns {Void}
 */
function cliPageInitDisableFutureDates(type){
var d_start_date = new Date(nlapiGetFieldValue('startdate'));
	
	var d_today = new Date();
	
	for(var i_day_indx = 0; i_day_indx < 7; i_day_indx++)
		{
			if(nlapiAddDays(d_today, 7) < nlapiAddDays(d_start_date, i_day_indx))
			{
				nlapiSelectLineItem('timegrid', 1);
				nlapiDisableLineItemField('timegrid', 'timeentry_hours_' + i_day_indx, false);
			}
		}
}

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType
 *   
 * @param {String} type Sublist internal id
 * @returns {Void}
 */
function clientLineInit(type) {
     
}
