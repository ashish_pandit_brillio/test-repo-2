function beforeLoad(type, form) {
	try {
		var role = nlapiGetRole();
		nlapiLogExecution('Debug','role==',role);	
		if (type == 'view' && role != 3) {
			nlapiSetRedirectURL('SUITELET',
			        'customscript_sut_mp_process_status',
			        'customdeploy_sut_mp_process_status', null, {
				        master : nlapiGetRecordId()
			        });
		}

		if (type == 'edit') {
			form.addButton('custpage_open_status', 'Open Status',
			        "goToStatusPage()");
		}
	} catch (err) {
		nlapiLogExecution('ERROR', 'beforeLoad', err);
		throw err;
	}
}

function beforeSubmit(type) {
	try {

		if (type == 'create') {
			nlapiSetFieldValue('custrecord_pm_status', 'Not Started');
			nlapiSetFieldValue('custrecord_pm_percent', '0%');
		}

		if (type == 'edit') {
			var isRunning = nlapiGetFieldValue('custrecord_pm_is_currently_active');

			if (isRunning == 'T') {
				var role = nlapiGetRole();

				if (role != '3') {
					throw "This provision is currently running. Only Administrator can edit this";
				}
			}
		}
	} catch (err) {
		nlapiLogExecution('ERROR', 'beforeSubmit', err);
		throw err;
	}
}

function afterSubmit(type) {
	try {

		if (type == 'create') {
			nlapiSetRedirectURL('SUITELET',
			        'customscript_sut_mp_process_status',
			        'customdeploy_sut_mp_process_status', null, {
				        master : nlapiGetRecordId()
			        });
		}
	} catch (err) {
		nlapiLogExecution('ERROR', 'afterSubmit', err);
		throw err;
	}
}
