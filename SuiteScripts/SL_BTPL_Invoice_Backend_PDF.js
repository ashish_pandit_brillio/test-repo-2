/**
* 
*/
/**
 * Definition of the Suitelet script trigger point.
 *
 * @param {Object} context
 * @param {ServerRequest} context.request - Encapsulation of the incoming request
 * @param {ServerResponse} context.response - Encapsulation of the Suitelet response
 * @Since 2015.2
 */
function createBTPL_Invoice_PDF(request, response) {
    try {
        nlapiLogExecution("debug", "Script Started");
        var a_data_array = new Array();
        var a_hours_array = new Array();
        var a_employee_array = new Array();
        var a_line_array = new Array();
        var i_inc_cnt = 0;
        var i_cnt = 0;
        var i_hours_per_day;
        var i_hours_per_week;
        var i_days_for_month;
        var i_OT_applicable;
        var i_OT_rate_ST;
        var i_week_days;
        var a_location_data_array = new Array();
        var i_rate = 0;
        var otrVar;
        var ctrVar;
        var i_flag = 0;
        var i_project;
        var i_project_text;
        var i_total_amount_INV = 0;
        var a_split_f_array = new Array();
        var i_cnt = 0;
        var i_S_No_cnt = 0;
        var strVar = "";
        var a_employee_array = new Array();
        var a_employee_name_array = new Array();
        var i_cnt = 0;
        var linenum = '17';
        var pagening = 4;
        var pagecount = 0;
        var s_remittance_details = "Accounts Receivable, Brillio LLC;\n100 Town Square Place;\nSuite 308, Jersey City\n NJ 07310"
        var flag_name = '';
        var emp_name = '';
        var prev_emp_name = '';
        var prev_i_ST_units = '';
        var address_1 = '';
        var address_2 = '';
        var concat_city = '';
        var concat_state = '';
        /*address_1 = '#58, 1st Main Road, Mini Forest';
        address_2 = 'J.P Nagar 3rd Phase';
        address_3 = 'Bangalore 560078';
        */
        address_1 = 'No. 8/2, 4th Floor, Bren Optimus';
        address_2 = 'Dr. M H Marigowda Road, Bangalore';
        address_3 = 'Bengaluru (Bangalore) Urban,Karnataka, 560029';

        //retrieve the record id passed to the Suitelet
        var recId = request.getParameter('id');
        recId = parseInt(recId);

        // first object
        nlapiLogExecution("debug", "recId", recId);
        if (recId) {
            var recObj = nlapiLoadRecord('invoice', recId);
            var invoice_number = recObj.getFieldValue('tranid');
            var invoice_date = recObj.getFieldValue('trandate');
            var po_num = recObj.getFieldValue('otherrefnum');
            if (!_logValidation(po_num)) {
                po_num = '';
            }
            var split_Obj = invoice_date.split('/');
            var month = split_Obj[0];
            var date = split_Obj[1];
            var year = split_Obj[2];
            var year_sub = year.substring(2, 4);
            var month_T = getMonth(month);
            var invoice_formated_date = date + '-' + month_T + '-' + year_sub;
            var btpl_layout = recObj.getFieldValue('custbody_invoice_btpl_layout');
            var memo = recObj.getFieldValue('memo');
            if (!_logValidation(memo)) {
                memo = '';
            }
            var invoice_amt = recObj.getFieldValue('subtotal');
            var customer_Id = recObj.getFieldValue('entity');
            var project_Id = recObj.getFieldValue('job');
            var service_category = recObj.getFieldText('custbody_servicetaxcategory');
            var billingdescription = recObj.getFieldValue('custbody_billingdescription');
            if (!_logValidation(billingdescription)) {
                billingdescription = '';
            }
            var billfrom = recObj.getFieldValue('custbody_billfrom');
            var billto = recObj.getFieldValue('custbody_billto');

            var filter = [];
            filter[0] = new nlobjSearchFilter('entity', null, 'anyOf', customer_Id);
            filter[1] = new nlobjSearchFilter('internalid', 'job', 'anyOf', project_Id);
            var col = [];
            col[0] = new nlobjSearchColumn('billaddressee');
            col[1] = new nlobjSearchColumn('billaddress1');
            col[2] = new nlobjSearchColumn('billcity');
            col[3] = new nlobjSearchColumn('billstate');
            col[4] = new nlobjSearchColumn('billzip');
            col[5] = new nlobjSearchColumn('billcountry');

            var search = nlapiSearchRecord('salesorder', null, filter, col);
            for (var i = 0; i < search.length; i++) {
                var billadd = search[i].getValue('billaddressee');
                var billadd1 = search[i].getValue('billaddress1');
                var billcity = search[i].getValue('billcity');
                var billstate = search[i].getValue('billstate');
                var billzip = search[i].getValue('billzip');
                var billcountry = search[i].getText('billcountry');
            }
            if (!_logValidation(billadd1)) {
                billadd1 = '';
            }
            else {
                if (!_logValidation(billcity)) {
                    billcity = '';
                }
            }

            concat_city = billcity + ' ' + billzip;
            concat_state = billstate + ',' + billcountry;

            var dataRow_description = [];
            var json = {};
            var lineCount = recObj.getLineItemCount('item');
            // nlapiLogExecution('DEBUG', 'linecount', lineCount);

            for (var j = 1; j <= lineCount; j++) {
                json = {
                    Description: recObj.getLineItemValue('item', 'description', j),
                    Amount: recObj.getLineItemValue('item', 'amount', j),
                    TaxRate: recObj.getLineItemValue('item', 'taxrate1', j),
                    TaxAmount: recObj.getLineItemValue('item', 'tax1amt', j),
                    rate: recObj.getLineItemValue('item', 'rate', j),
                    quantity: recObj.getLineItemValue('item', 'quantity', j)
                };
                dataRow_description.push(json);
            }

            //Search For Invoice Address

            var taxamount;
            var taxrate;

            if (btpl_layout) {
                var filters = [];
                filters.push(new nlobjSearchFilter('custrecord_location_1', null, 'anyof', btpl_layout));
                var cols = [];
                cols.push(new nlobjSearchColumn('custrecord_btpl_address'));
                cols.push(new nlobjSearchColumn('custrecord_btpl_address_2'));
                var searchRec = nlapiSearchRecord('customrecord_btpl_invoice_address', null, filters, cols);
                if (searchRec) {
                    address_1 = searchRec[0].getValue('custrecord_btpl_address');
                    //address_2 = searchRec[0].getValue('custrecord_btpl_address_2');
                }
            }
            //Getting Customer info
            if (customer_Id) {
                var customerDetails = nlapiLookupField('customer', customer_Id, ['billaddressee', 'phone', 'altphone', 'fax', 'currency']);
            }

            //var data_bill = customerDetails.defaultaddress;
            var cu_phone = customerDetails.phone;
            var billTo_Telephone = cu_phone;
            var cu_altPhone = customerDetails.altphone;
            var cu_fax = customerDetails.fax;

            if (cu_altPhone) {
                cu_phone = 'Tel#' + ' ' + cu_phone + ' ' + '/' + ' ' + cu_altPhone;
            }

            var rec_Obj_1 = nlapiLoadRecord('customer', customer_Id);
            var customername = rec_Obj_1.getFieldValue('altname');
            var currency_ = rec_Obj_1.getFieldText('currency');
            var address_count = rec_Obj_1.getLineItemCount('addressbook');
            var payment_term = rec_Obj_1.getFieldText('terms');

            for (var i = 1; i <= 1; i++) {
                var address_C_name = rec_Obj_1.getLineItemValue('addressbook', 'addressee', i);
                var attention_to = rec_Obj_1.getLineItemValue('addressbook', 'attention', i);
                if (!_logValidation(attention_to)) {
                    attention_to = '';
                }

                //start of block not using code
                var addrtext = rec_Obj_1.getLineItemValue('addressbook', 'addrtext', i);
                var obj = addrtext.split('\n');
                var address_C_0 = obj[1];
                var address_C_1 = obj[2];
                var address_C_2 = obj[3];
                var address_C_3 = obj[4];
                if (!_logValidation(address_C_1)) {
                    address_C_1 = '';
                }
                if (!_logValidation(address_C_2)) {
                    address_C_2 = '';
                }
                if (!_logValidation(address_C_3)) {
                    address_C_3 = '';
                }
                //end of block not using code
            }

            //Get Project name
            if (project_Id) {
                var project_Name = nlapiLookupField('job', project_Id, ['companyname']);
            }

            var IRN = '';
            var qr_code = '000';
            var transactionSearch = nlapiSearchRecord("transaction", null,
                [
                    ["internalid", "anyof", recId],
                    "AND",
                    ["mainline", "is", "T"]
                ],
                [
                    new nlobjSearchColumn("custrecord_iit_e_inv_irn", "CUSTRECORD_REF_NO", null),
                    new nlobjSearchColumn("custrecord_iit_e_inv_qr", "CUSTRECORD_REF_NO", null)
                ]
            );

            if (transactionSearch) {
                IRN = transactionSearch[0].getValue("custrecord_iit_e_inv_irn", "CUSTRECORD_REF_NO", null)
                if (_nullValidation(IRN)) {
                    IRN = '';
                }
                var qr_code = transactionSearch[0].getValue("custrecord_iit_e_inv_qr", "CUSTRECORD_REF_NO", null)
                if (_nullValidation(qr_code)) {
                    qr_code = '000';
                }
            }
            var url = "https://api.qrserver.com/v1/create-qr-code/?data=" + qr_code + "&amp;size=202x170";

            //Generate PDF Starts here
            var comp_imgrsrc = 'https://system.na1.netsuite.com/core/media/media.nl?id=6237&c=3883006&h=f9681f3581e51644773b';
            comp_imgrsrc = nlapiEscapeXML(comp_imgrsrc);

            var xmlContent = "";
            //Next Page
            xmlContent += "<table border='0.5' width=\"100%\">";
            xmlContent += "<tr>";
            xmlContent += "<td width=\"50%\" style='border:0px solid black;' font-family=\"Helvetica\" font-size=\"9\">";
            xmlContent += "<p class='go-right' style='margin-left:10px;' font-size='9'><b>Brillio Technologies Pvt Ltd</b><br/>"
                + "" + address_1 + "<br/>" + " " + address_3 + "<br/>" + "<b>GSTIN No : 29AABCP2354A1ZT</b></p>";
            xmlContent += "</td>";
            xmlContent += "<td align ='right' width =\"50%\" style='border:0px solid black;'>";
            xmlContent += "<img height=\"50\" width=\"60\" src='" + comp_imgrsrc + "'/>";
            xmlContent += "</td>";
            xmlContent += "</tr>";
            xmlContent += "<tr border-bottom='0.5'>";
            xmlContent += "<td font-family=\"Helvetica\" font-size=\"21\" colspan='2'><b>Invoice</b></td>"; //border-bottom='1px solid black'
            xmlContent += "</tr>";
            xmlContent += "</table>";

            xmlContent += "<table width=\"100%\" >";
            xmlContent += "<tr>";
            xmlContent += "<td style=\"border-left: 0.1px solid;border-bottom: 0.1px solid;border-top: 0.1px solid;border-right: 0.1px solid;\"  WIDTH=\"50%\" font-family=\"Helvetica\" font-size=\"8\"><b>Invoice No.:</b>" + invoice_number + "</td>";
            xmlContent += "<td align=\"left\" width=\"52%\"  style=\"border-left: 0.1px solid;border-bottom: 0.1px solid;border-top: 0.1px solid;border-right: 0.1px solid;\" font-size=\"8\"><b>Invoice Date :</b>" + nlapiEscapeXML(invoice_formated_date) + "<\/td>";
            xmlContent += "<\/tr>";
            xmlContent += "	<tr>";
            xmlContent += "<td rowspan=\"2\" style=\"border-left: 0.1px solid;border-bottom: 0.1px solid;border-top: 0.1px solid;border-right: 0.1px solid;\"  WIDTH=\"50%\" font-family=\"Helvetica\" font-size=\"8\"><b>Billing Description :</b>" + billingdescription + "</td>";
            xmlContent += "<td align=\"left\" width=\"52%\"  style=\"border-left: 0.1px solid;border-bottom: 0.1px solid;border-top: 0.1px solid;border-right: 0.1px solid;\" font-size=\"8\"><b>IRN:</b>" + nlapiEscapeXML(IRN) + "<\/td>";
            xmlContent += "<\/tr>";
            xmlContent += "<tr>";
            xmlContent += "<td align=\"left\" width=\"52%\"  style=\"border-left: 0.1px solid;border-bottom: 0.1px solid;border-top: 0.1px solid;border-right: 0.1px solid;\" font-size=\"8\"><b>PO# :</b>" + nlapiEscapeXML(po_num) + "<\/td>";
            xmlContent += "<\/tr>";
            xmlContent += "<\/table>";

            //Transaction Details
            xmlContent += "<table border='0.5'  WIDTH=\"100%\">";
            xmlContent += "<tr>";

            //New Change
            xmlContent += "<td width=\"48.99%\" font-family=\"Helvetica\" font-size=\"9\">";
            xmlContent += "<p><b>Bill To:</b></p>"
            xmlContent += "<b>" + nlapiEscapeXML(customername) + "</b><br/>";
            xmlContent += nlapiEscapeXML(billadd) + "<br/>";
            xmlContent += nlapiEscapeXML(billadd1) + "<br/>";
            if (_logValidation(concat_city)) {
                if (concat_city != ',') {
                    xmlContent += nlapiEscapeXML(concat_city) + "<br/>";
                }
            }
            if (concat_state != ',') {
                xmlContent += nlapiEscapeXML(concat_state);
            }

            xmlContent += "<p>Contact Person: " + nlapiEscapeXML(attention_to) + "</p>";
            xmlContent += "</td>";

            xmlContent += "<td width=\"20%\" font-family=\"Helvetica\" font-size=\"9\" border-left='0.5' align = 'left'>"; //border=\"0.5\"
            xmlContent += "<p><b>Remittance Details:</b></p>"
            xmlContent += "<p><b>Bank Name</b><br/>"
                + "<b>Address</b><br/>"
                + "<br/>"
                + "<br/>"
                + "<b>Bank Account No.</b><br/>"
                + "SWIFT CODE<br/>"
                + "CIN<br/></p>";
            xmlContent += "</td>";
            xmlContent += "<td width='30px' font-family=\"Helvetica\" font-size=\"9\" align = 'left'>";//width='52%'
            xmlContent += "<br/>";
            xmlContent += "<p><b>Kotak Mahindra Bank Ltd</b><br/>"
                + "<b>#27, Santosh Towers,<br/> J.P.Nagar 4th Phase, 100ft Ring Road, <br/> Bangalore - 560078</b><br/>"
                + "<b>1511696478</b><br/>"
                + "KKBKINBBCPC<br/>"
                + "U22190KA1997FTC022250<br/></p>";
            xmlContent += "</td>";
            xmlContent += "</tr>";
            xmlContent += "</table>";

            xmlContent += "<table border='0.5' WIDTH='100%'>";
            xmlContent += "<tr>";
            xmlContent += "<td  border-bottom='0.5' WIDTH='30%' font-family=\"Helvetica\" font-size=\"9\"><b> Memo</b></td>";
            xmlContent += "<td border-bottom='0.5' WIDTH='70%' font-family=\"Helvetica\" font-size=\"9\">" + memo + "</td>";
            xmlContent += "</tr>";

            xmlContent += "<tr>";
            xmlContent += "<td  WIDTH='10%' font-family=\"Helvetica\" font-size=\"9\"><b>Payment Term </b></td>";
            xmlContent += "<td WIDTH='90%' font-family=\"Helvetica\" font-size=\"9\">"
            xmlContent += nlapiEscapeXML(payment_term);
            xmlContent += "</td>";
            xmlContent += "</tr>";

            xmlContent += "</table>";

            //Creating Sub Table
            xmlContent += "<table border='0.5' width='100%' height='100%'>";
            xmlContent += "<tr>";
            xmlContent += "<td style='padding-top:10px;' border-right='0.5' border-bottom='0.5' width='50%' rowspan='2' align = 'center' font-family=\"Helvetica\" font-size=\"9\"><b>Name</b></td>";
            xmlContent += "<td border-right='0.5' border-bottom='0.5' width='20%' colspan='2' align = 'center' font-family=\"Helvetica\" font-size=\"9\"><b>Billing Period</b></td>";
            xmlContent += "<td style='padding-top:10px;' border-right='0.5' border-bottom='0.5' width='35%' rowspan='2' align = 'center' font-family=\"Helvetica\"  font-size=\"9\"><b>No. of<br/> Units Billed</b></td>";
            xmlContent += "<td style='padding-top:10px;' border-right='0.5' border-bottom='0.5' width='10%' rowspan='2' align = 'center' font-family=\"Helvetica\" font-size=\"9\"><b>Rate</b></td>";
            xmlContent += "<td border-bottom='0.5' width='35%' align='center' rowspan='2' font-family=\"Helvetica\" font-size=\"9\"><b>Amount</b></td>";

            //Breaks
            xmlContent += "</tr>";
            xmlContent += "<tr>";

            xmlContent += "<td  border-bottom='0.5' width='10%' font-family=\"Helvetica\" font-size=\"9\" ><b>From</b></td>";
            xmlContent += "<td  border-left='0.5' border-bottom='0.5' border-right='0.5' width='10%' font-family=\"Helvetica\" font-size=\"9\" ><b>To</b></td>";
            //Breaks
            xmlContent += "</tr>";

            xmlContent += "<tr>";
            xmlContent += "<td  border-right='0.5' width='50%' align='left'></td>";
            xmlContent += "<td  border-right='0.5' width='20%'></td>";
            xmlContent += "<td  border-right='0.5' width='10%' ></td>";
            xmlContent += "<td  border-right='0.5' width='10%'></td>";
            xmlContent += "<td border-right='0.5' width='35%' align='right'></td>";
            xmlContent += "</tr>";

            xmlContent += "<tr>";
            xmlContent += "<td  border-right='0.5' width='50%' align='left'></td>";
            xmlContent += "<td  border-right='0.5' width='20%'></td>";
            xmlContent += "<td  border-right='0.5' width='10%' ></td>";
            xmlContent += "<td  border-right='0.5' width='10%'></td>";
            xmlContent += "<td border-right='0.5' width='35%' align='right'></td>";
            xmlContent += "</tr>";

            //------------------------
            var i_recordID = request.getParameter('id');
            var o_recordOBJ = nlapiLoadRecord('invoice', i_recordID);
            var i_item = o_recordOBJ.getLineItemValue('item', 'item', 1)
            var i_customer = o_recordOBJ.getFieldValue('entity');

            if (_logValidation(i_customer)) {
                var o_customerOBJ = nlapiLoadRecord('customer', i_customer);
                if (_logValidation(o_customerOBJ)) {
                    i_customer_name = o_customerOBJ.getFieldValue('altname')
                }
            }

            var i_customer_txt = o_recordOBJ.getFieldText('entity');
            var i_tran_date = o_recordOBJ.getFieldValue('trandate')
            var i_tran_id = o_recordOBJ.getFieldValue('tranid')

            if (!_logValidation(i_tran_id)) {
                i_tran_id = ''
            }
            var i_bill_address = nlapiEscapeXML(o_recordOBJ.getFieldValue('billaddress'))

            if (!_logValidation(i_bill_address)) {
                i_bill_address = ''
            }
            var i_PO_ID = o_recordOBJ.getFieldValue('otherrefnum')

            if (!_logValidation(i_PO_ID)) {
                i_PO_ID = ''
            }
            var i_terms = o_recordOBJ.getFieldText('terms')

            if (!_logValidation(i_terms)) {
                i_terms = ''
            }
            var s_memo = o_recordOBJ.getFieldValue('memo')

            if (!_logValidation(s_memo)) {
                s_memo = ''
            }

            var i_discount = o_recordOBJ.getFieldValue('discounttotal')

            if (!_logValidation(i_discount)) {
                i_discount = 0
            }

            var i_bill_to = o_recordOBJ.getFieldValue('custbody_billto')

            if (!_logValidation(i_bill_to)) {
                i_bill_to = ''
            }
            var i_bill_from = o_recordOBJ.getFieldValue('custbody_billfrom')

            if (!_logValidation(i_bill_from)) {
                i_bill_from = ''
            }
            var i_billing_description = o_recordOBJ.getFieldValue('custbody_billingdescription');

            if (!_logValidation(i_billing_description)) {
                i_billing_description = ''
            }

            var i_service_tax_category = o_recordOBJ.getFieldText('custbody_servicetaxcategory')

            if (!_logValidation(i_service_tax_category)) {
                i_service_tax_category = ''
            }
            var i_ST_Classification = o_recordOBJ.getFieldText(' custbody_stclassificationus')

            if (!_logValidation(i_ST_Classification)) {
                i_ST_Classification = ''
            }
            var i_tax = o_recordOBJ.getFieldValue('taxtotal')

            if (!_logValidation(i_tax)) {
                i_tax = 0
            }

            var i_discount_percent = parseFloat(i_discount) / 100;
            var i_tax_percent = parseFloat(i_tax) / 100;
            i_discount_percent = parseFloat(i_discount_percent).toFixed(2);
            i_tax_percent = parseFloat(i_tax_percent).toFixed(2);
            var i_billable_time_count = o_recordOBJ.getLineItemCount('time');
            var s_currency_symbol = '';
            var i_currency = o_recordOBJ.getFieldText('currency')
            var discount_count = o_recordOBJ.getLineItemCount('item');
            var discount = o_recordOBJ.getLineItemValue('item', 'description', 1);
            var discount_amt = o_recordOBJ.getLineItemValue('item', 'amount', 1);

            if (!_logValidation(i_currency)) {
                i_currency = '';
            }
            if (i_currency == 'USD') {
                s_currency_symbol = '$'
            }
            if (i_currency == 'INR') {
                s_currency_symbol = 'Rs.'
            }
            if (i_currency == 'GBP') {
                s_currency_symbol = '�'
            }
            if (i_currency == 'SGD') {
                s_currency_symbol = 'S$'
            }
            if (i_currency == 'Peso') {
                s_currency_symbol = 'P'
            }
            if (i_currency == 'EUR') {
                s_currency_symbol = '€'
            }

            if (_logValidation(i_billable_time_count)) {
                for (var i = 1; i <= i_billable_time_count; i++) {
                    var i_date = o_recordOBJ.getLineItemValue('time', 'billeddate', i)
                    var i_employee = o_recordOBJ.getLineItemValue('time', 'employeedisp', i)
                    i_project = o_recordOBJ.getLineItemValue('time', 'job', i)
                    var i_item = o_recordOBJ.getLineItemValue('time', 'item', i)
                    var i_rate = o_recordOBJ.getLineItemValue('time', 'rate', i)
                    var i_hours = o_recordOBJ.getLineItemValue('time', 'quantity', i)
                    var i_marked = o_recordOBJ.getLineItemValue('time', 'apply', i)

                    if (i_marked == 'T') {
                        var d_start_of_week = get_start_of_week(i_date)
                        var d_end_of_week = get_end_of_week(i_date)
                        var d_SOW_date = nlapiStringToDate(d_start_of_week);
                        var d_EOW_date = nlapiStringToDate(d_end_of_week);
                        d_SOW_date = get_date(d_SOW_date)
                        d_EOW_date = get_date(d_EOW_date)

                        if (i_item == 2222 || i_item == 2425 || i_item == 2221 || i_item == 2633) // Amol: added 2221(FP) item
                        {
                            a_employee_array[i_cnt++] = i_employee + '&&&' + d_EOW_date + '&&&' + d_SOW_date + '&&&' + i_rate;
                            a_employee_name_array.push(i_employee);
                        }// Time & Material
                    }
                }// Billable Loop
            }// Billable Line Count Loop

            a_employee_array = removearrayduplicate(a_employee_array)
            a_employee_array = a_employee_array.sort();
            // nlapiLogExecution("debug", "a_employee_array", a_employee_array);
            // nlapiLogExecution("debug", "a_employee_array.length", a_employee_array.length);

            a_employee_name_array = removearrayduplicate(a_employee_name_array)
            a_employee_name_array = a_employee_name_array.sort();
            // nlapiLogExecution("debug", "a_employee_name_array", a_employee_name_array);
            // nlapiLogExecution("debug", "a_employee_name_array.length", a_employee_name_array.length);


            var i_employee_arr_n;
            var vb = 0;
            var emp_total_hr = 0;
            var emp = 0;
            var emp_total_amt = 0;
            var emp_count = 0;

            // start of for loop
            for (var k = 0; k < a_employee_array.length; k++) {
                var i_ST_rate = 0;
                var i_OT_rate = 0;
                var i_ST_units = 0;
                var i_OT_units = 0;
                var i_hours = 0
                var i_minutes = 0;
                var i_hours_ST = 0;
                var i_minutes_ST = 0;
                var i_hours_OT = 0;
                var i_minutes_OT = 0;
                var flag = 0;

                a_split_array = a_employee_array[k].split('&&&')
                var i_employee_arr = a_split_array[0]
                var i_end_of_week_arr = a_split_array[1]
                var i_start_of_week_arr = a_split_array[2]
                var i_rate_emp = a_split_array[3]

                for (var lt = 1; lt <= i_billable_time_count; lt++) {
                    var i_date_new = o_recordOBJ.getLineItemValue('time', 'billeddate', lt)
                    var i_employee_new = o_recordOBJ.getLineItemValue('time', 'employeedisp', lt)
                    var i_project_new = o_recordOBJ.getLineItemValue('time', 'job', lt)
                    var i_item_new = o_recordOBJ.getLineItemValue('time', 'item', lt)
                    var i_rate_new = o_recordOBJ.getLineItemValue('time', 'rate', lt)
                    var i_hours_new = o_recordOBJ.getLineItemValue('time', 'quantity', lt)
                    var i_marked = o_recordOBJ.getLineItemValue('time', 'apply', lt)

                    if (i_marked == 'T') {
                        if ((i_employee_arr == i_employee_new)) {
                            var d_start_of_week_new = get_start_of_week(i_date_new)
                            var d_end_of_week_new = get_end_of_week(i_date_new)
                            var d_SOW_date_B = nlapiStringToDate(d_start_of_week_new);
                            var d_EOW_date_B = nlapiStringToDate(d_end_of_week_new);

                            d_SOW_date_B = get_date(d_SOW_date_B)
                            d_EOW_date_B = get_date(d_EOW_date_B)

                            if ((d_SOW_date_B == i_start_of_week_arr) && (d_EOW_date_B == i_end_of_week_arr) && (i_rate_emp == i_rate_new)) {
                                if (flag != i_employee_arr) {
                                    i_employee_arr_n = i_employee_arr;
                                }
                                else {
                                    i_employee_arr_n = '';
                                }
                                var a_split_time_array = new Array();
                                a_split_time_array = i_hours_new.split(':')
                                var hours = a_split_time_array[0]
                                var minutes = a_split_time_array[1]

                                if (minutes == '0.00' || minutes == '0'
                                    || minutes == 0) {
                                }
                                else {
                                    minutes = parseFloat(minutes) / 60;
                                    minutes = parseFloat(minutes).toFixed(2);
                                }
                                if (i_item_new == 2222 || i_item_new == 2221 || i_item_new == 2633) // Amol: added 2221(FP) item
                                {
                                    i_hours_ST = parseFloat(i_hours_ST) + parseFloat(hours)
                                    if (minutes == '0.75'
                                        || minutes == '0.25') {
                                    }
                                    i_hours_ST = parseFloat(i_hours_ST) + parseFloat(minutes);
                                    i_hours_ST = parseFloat(i_hours_ST).toFixed(2);
                                    i_ST_rate = parseFloat(i_hours_ST) * parseFloat(i_rate_new)
                                    i_ST_units = parseFloat(i_rate_new).toFixed(2);
                                }// Time & Material

                                if (i_item_new == 2425) {
                                    i_hours_OT = parseFloat(i_hours_OT) + parseFloat(hours)
                                    i_hours_OT = parseFloat(i_hours_OT) + parseFloat(minutes);
                                    i_hours_OT = parseFloat(i_hours_OT).toFixed(2);
                                    i_OT_rate = parseFloat(i_hours_OT);
                                    i_OT_units = parseFloat(i_rate_new).toFixed(2);
                                }// OT Item
                            }// Start & End Date Validation
                        }// Employee / Project / Item
                    }// Marked T
                }// Billable Count
                // nlapiLogExecution("debug", "details4","k: " + k + ", i_ST_units: " + i_ST_units);
                i_amount = (parseFloat(i_hours_ST) * parseFloat(i_ST_units)) + (parseFloat(i_hours_OT) * parseFloat(i_OT_units));
                i_amount = parseFloat(i_amount);
                i_amount = i_amount.toFixed(2);
                i_total_amount_INV = parseFloat(i_total_amount_INV) + parseFloat(i_amount);
                i_total_amount_INV = parseFloat(i_total_amount_INV);
                i_total_amount_INV = i_total_amount_INV.toFixed(2);
                var s_employee_addr_txt = '';
                s_employee_addr_txt = escape_special_chars(i_employee_arr);

                // nlapiLogExecution("debug", "details1","k: " + k + ", emp: " + emp + ", prev_emp_name:" + prev_emp_name + ", emp_name: " + emp_name + ", i_employee_arr: " + i_employee_arr + ", parseFloat(i_ST_units)" + parseFloat(i_ST_units) + ", emp_total_amt: " + emp_total_amt);
                if (emp == 0 || emp_name == i_employee_arr) {
                    if (a_employee_name_array.indexOf(i_employee_arr) != -1) {
                        emp_total_hr = parseFloat(emp_total_hr) + parseFloat(i_hours_ST) + parseFloat(i_hours_OT);
                        emp_total_amt = parseFloat(emp_total_amt) + parseFloat(i_amount);
                        emp = 1;
                        emp_count++;
                        // nlapiLogExecution("debug", "details2","k: " + k + ", emp_count: " + emp_count);
                        if (a_employee_array.length == emp_count) {
                            if (emp_total_amt != 0) {
                                xmlContent += "<tr>";
                                xmlContent += "<td  border-right='0.5' width='50%' align = 'left' font-family=\"Helvetica\" font-size=\"9\">" + escape_special_chars(emp_name) + " </td>";
                                xmlContent += "<td  border-right='0.5' width='10%' align = 'center' font-family=\"Helvetica\" font-size=\"9\">" + billfrom + "</td>";
                                xmlContent += "<td  border-right='0.5' width='10%' align = 'center' font-family=\"Helvetica\" font-size=\"9\">" + billto + "</td>";
                                xmlContent += "<td  border-right='0.5' width='10%' align = 'center' font-family=\"Helvetica\" font-size=\"9\">" + emp_total_hr + "</td>";
                                xmlContent += "<td  border-right='0.5' width='10%' align = 'center' font-family=\"Helvetica\" font-size=\"9\">" + prev_i_ST_units + "</td>";
                                xmlContent += "<td  width='35%' align='right' font-family=\"Helvetica\" font-size=\"9\">" + emp_total_amt + "</td>";
                                xmlContent += "</tr>";
                            }
                            emp = 1;
                        }
                        emp_name = i_employee_arr;
                        prev_emp_name = s_employee_addr_txt;
                        prev_i_ST_units = parseFloat(i_ST_units);
                    }
                }
                else {
                    if (emp_total_amt != 0) {
                        xmlContent += "<tr>";
                        xmlContent += "<td  border-right='0.5' width='50%' align = 'left' font-family=\"Helvetica\" font-size=\"9\">" + escape_special_chars(emp_name) + " </td>";
                        xmlContent += "<td  border-right='0.5' width='10%' align = 'center' font-family=\"Helvetica\" font-size=\"9\">" + billfrom + "</td>";
                        xmlContent += "<td  border-right='0.5' width='10%' align = 'center' font-family=\"Helvetica\" font-size=\"9\">" + billto + "</td>";
                        xmlContent += "<td  border-right='0.5' width='10%' align = 'center' font-family=\"Helvetica\" font-size=\"9\">" + emp_total_hr + "</td>";
                        xmlContent += "<td  border-right='0.5' width='10%' align = 'center' font-family=\"Helvetica\" font-size=\"9\">" + prev_i_ST_units + "</td>";
                        xmlContent += "<td  width='35%' align='right' font-family=\"Helvetica\" font-size=\"9\">" + emp_total_amt + "</td>";
                        xmlContent += "</tr>";
                    }

                    emp_total_hr = 0;
                    emp_total_amt = 0;
                    emp_total_hr = parseFloat(emp_total_hr) + parseFloat(i_hours_ST) + parseFloat(i_hours_OT);
                    emp_total_amt = parseFloat(emp_total_amt) + parseFloat(i_amount);

                    emp_name = i_employee_arr;
                    prev_emp_name = s_employee_addr_txt;
                    prev_i_ST_units = parseFloat(i_ST_units);
                    emp_count++;
                    // emp = 0;

                    // if previous employee name is not equal to current employee and are at the end of for loop, the below if condition will print the current line item.
                    if (a_employee_array.length == emp_count) {
                        if (emp_total_amt != 0) {
                            xmlContent += "<tr>";
                            xmlContent += "<td  border-right='0.5' width='50%' align = 'left' font-family=\"Helvetica\" font-size=\"9\">" + escape_special_chars(emp_name) + " </td>";
                            xmlContent += "<td  border-right='0.5' width='10%' align = 'center' font-family=\"Helvetica\" font-size=\"9\">" + billfrom + "</td>";
                            xmlContent += "<td  border-right='0.5' width='10%' align = 'center' font-family=\"Helvetica\" font-size=\"9\">" + billto + "</td>";
                            xmlContent += "<td  border-right='0.5' width='10%' align = 'center' font-family=\"Helvetica\" font-size=\"9\">" + emp_total_hr + "</td>";
                            xmlContent += "<td  border-right='0.5' width='10%' align = 'center' font-family=\"Helvetica\" font-size=\"9\">" + prev_i_ST_units + "</td>";
                            xmlContent += "<td  width='35%' align='right' font-family=\"Helvetica\" font-size=\"9\">" + emp_total_amt + "</td>";
                            xmlContent += "</tr>";
                        }
                        emp = 1;
                    }


                }
                // nlapiLogExecution("debug", "details3","k: " + k + ", emp: " + emp + ", prev_emp_name:" + prev_emp_name + ", emp_name: " + emp_name + ", i_employee_arr: " + i_employee_arr + ", parseFloat(i_ST_units)" + parseFloat(i_ST_units) + ", emp_total_amt: " + emp_total_amt);

                strVar += "				<td colspan=\"1\" border-bottom=\"0\" border-left=\"0\" font-family=\"Helvetica\" align=\"left\" font-size=\"8\">" + i_end_of_week_arr + "<\/td>";
                strVar += "				<td colspan=\"1\" border-bottom=\"0\" border-left=\"0\" font-family=\"Helvetica\" align=\"center\" font-size=\"8\">" + parseFloat(i_ST_units) + "<\/td>";
                strVar += "				<td colspan=\"1\" border-bottom=\"0\" border-left=\"0\" font-family=\"Helvetica\" align=\"center\" font-size=\"8\">" + parseFloat(i_hours_ST) + "<\/td>";
                strVar += "				<td colspan=\"1\" border-bottom=\"0\" border-left=\"0\" font-family=\"Helvetica\" align=\"center\" font-size=\"8\">" + parseFloat(i_OT_units) + "<\/td>";// i_hours_OT//i_OT_rate
                strVar += "				<td colspan=\"1\" border-bottom=\"0\" border-left=\"0\" font-family=\"Helvetica\" align=\"center\" font-size=\"8\">" + parseFloat(i_hours_OT) + "<\/td>";// i_OT_units
                strVar += "				<td colspan=\"1\" border-bottom=\"0\" border-left=\"0\" border-right=\"0\" font-family=\"Helvetica\" align=\"right\" font-size=\"8\">" + i_amount + "<\/td>";
                strVar += "			<\/tr>";

                pagecount++;
                if (pagecount == linenum && a_employee_array.length != '17') {
                    pagecount = 0;
                    linenum = 40;
                    strVar += "<\/table>";
                    strVar += " <p style=\"page-break-after: always\"><\/p>";
                    strVar += "<table border=\"0\" width=\"100%\">";
                    // ----------------------------------------------------------

                    strVar += "<tr>";
                    strVar += "<td colspan=\"2\" border-left=\"0\" border-top=\"0.3\" border-bottom=\"0\" font-family=\"Helvetica\" align=\"center\" font-size=\"9\"><b>Name<\/b></td>";
                    strVar += "<td colspan=\"1\" border-left=\"0\" border-top=\"0.3\" border-bottom=\"0\" font-family=\"Helvetica\" align=\"center\" font-size=\"9\"><b>Week End<\/b></td>";
                    strVar += "<td colspan=\"1\" border-left=\"0\" border-top=\"0.3\" border-bottom=\"0\" font-family=\"Helvetica\" align=\"center\" font-size=\"9\"><b>ST Rate<\/b></td>";
                    strVar += "<td colspan=\"1\" border-left=\"0\" border-top=\"0.3\" border-bottom=\"0\" font-family=\"Helvetica\" align=\"center\" font-size=\"9\"><b>ST Units<\/b></td>";
                    strVar += "<td colspan=\"1\" border-left=\"0\" border-top=\"0.3\" border-bottom=\"0\" font-family=\"Helvetica\" align=\"center\" font-size=\"9\"><b>OT Rate<\/b></td>";
                    strVar += "<td colspan=\"1\" border-left=\"0\" border-top=\"0.3\" border-bottom=\"0\" font-family=\"Helvetica\" align=\"center\" font-size=\"9\"><b>OT Units<\/b></td>";
                    strVar += "<td colspan=\"1\" border-left=\"0\" border-right=\"0\" border-top=\"0.3\" border-bottom=\"0\" align=\"right\" font-family=\"Helvetica\" font-size=\"9\"><b>Amount(" + s_currency_symbol + ")<\/b></td>";
                    strVar += "</tr>";

                    strVar += "<tr>";
                    strVar += "<td colspan=\"2\" border-left=\"0\" border-top=\"0\" border-bottom=\"0.3\" align=\"left\" font-family=\"Helvetica\" font-size=\"9\"></td>";
                    strVar += "<td colspan=\"1\" border-left=\"0\" border-top=\"0\" border-bottom=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"9\"></td>";
                    strVar += "<td colspan=\"1\" border-left=\"0\" border-top=\"0\" border-bottom=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"9\">(Hourly)</td>";
                    strVar += "<td colspan=\"1\" border-left=\"0\" border-top=\"0\" border-bottom=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"9\">(Hours)</td>";
                    strVar += "<td colspan=\"1\" border-left=\"0\" border-top=\"0\" border-bottom=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"9\">(Hourly)</td>";
                    strVar += "<td colspan=\"1\" border-left=\"0\" border-top=\"0\" border-bottom=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"9\">(Hours)</td>";
                    strVar += "<td colspan=\"1\" border-left=\"0\" border-right=\"0\" border-top=\"0\" border-bottom=\"0.3\" align=\"right\" font-family=\"Helvetica\" font-size=\"9\"></td>";
                    strVar += "</tr>";
                    // ----------------------------------------------------------
                }
            }
            //end of for loop

            if (discount == null || discount == 'null') {
                discount = '';
            }
            if (discount_amt == null || discount_amt == 'null') {
                discount_amt = '';
            }
            for (var dis_ind = 1; dis_ind <= discount_count; dis_ind++) {
                strVar += "			<tr>";
                strVar += "				<td colspan=\"2\" border-bottom=\"0\" border-left=\"0\" font-family=\"Helvetica\" align=\"left\" font-size=\"8\">" + o_recordOBJ.getLineItemValue('item', 'description', dis_ind) + "<\/td>";
                strVar += "				<td colspan=\"1\" border-bottom=\"0\" border-left=\"0\" font-family=\"Helvetica\" align=\"left\" font-size=\"8\"><\/td>";
                strVar += "				<td colspan=\"1\" border-bottom=\"0\" border-left=\"0\" font-family=\"Helvetica\" align=\"center\" font-size=\"8\"><\/td>";
                strVar += "				<td colspan=\"1\" border-bottom=\"0\" border-left=\"0\" font-family=\"Helvetica\" align=\"center\" font-size=\"8\"><\/td>";
                strVar += "				<td colspan=\"1\" border-bottom=\"0\" border-left=\"0\" font-family=\"Helvetica\" align=\"center\" font-size=\"8\"><\/td>";// i_hours_OT//i_OT_rate
                strVar += "				<td colspan=\"1\" border-bottom=\"0\" border-left=\"0\" font-family=\"Helvetica\" align=\"center\" font-size=\"8\"><\/td>";// i_OT_units
                strVar += "				<td colspan=\"1\" border-bottom=\"0\" border-left=\"0\" border-right=\"0\" font-family=\"Helvetica\" align=\"right\" font-size=\"8\">" + o_recordOBJ.getLineItemValue('item', 'amount', dis_ind) + "<\/td>";
                strVar += "			<\/tr>";
                // -----------------------------------------------------------------------------------------------------------
            }
            if (_logValidation(i_discount_percent)
                && i_discount_percent != 0) {
                strVar += "<tr>";

                strVar += "<td colspan=\"2\" border-left=\"0\" border-top=\"0\" border-bottom=\"0\" align=\"left\" font-family=\"Helvetica\" font-size=\"9\"><b><\/b></td>";
                strVar += "<td colspan=\"1\" border-left=\"0\" border-top=\"0\" border-bottom=\"0\" align=\"left\" font-family=\"Helvetica\" font-size=\"9\">Less: Discount @" + i_discount_percent + "</td>";
                strVar += "<td colspan=\"1\" border-left=\"0\" border-top=\"0\" border-bottom=\"0\" align=\"left\" font-family=\"Helvetica\" font-size=\"9\"><b><\/b></td>";
                strVar += "<td colspan=\"1\" border-left=\"0\" border-top=\"0\" border-bottom=\"0\" align=\"left\" font-family=\"Helvetica\" font-size=\"9\"><b><\/b></td>";
                strVar += "<td colspan=\"1\" border-left=\"0\" border-top=\"0\" border-bottom=\"0\" align=\"left\" font-family=\"Helvetica\" font-size=\"9\"><b><\/b></td>";
                strVar += "<td colspan=\"1\" border-left=\"0\" border-top=\"0\" border-bottom=\"0\" align=\"left\" font-family=\"Helvetica\" font-size=\"9\"><b><\/b></td>";
                strVar += "<td colspan=\"1\" border-left=\"0\" border-right=\"0\" border-top=\"0\" border-bottom=\"0\" align=\"left\" font-family=\"Helvetica\" font-size=\"9\"><b><\/b></td>";
                strVar += "</tr>";

            }
            if (_logValidation(i_tax_percent) && i_tax_percent != 0) {
                strVar += "<tr>";
                strVar += "<td colspan=\"2\" border-left=\"0\" border-top=\"0\" border-bottom=\"0\" align=\"left\" font-family=\"Helvetica\" font-size=\"9\"><b><\/b></td>";
                strVar += "<td colspan=\"1\" border-left=\"0\" border-top=\"0\" border-bottom=\"0\" align=\"left\" font-family=\"Helvetica\" font-size=\"9\">Add: Sales Tax PA state @" + i_tax_percent + "</td>";
                strVar += "<td colspan=\"1\" border-left=\"0\" border-top=\"0\" border-bottom=\"0\" align=\"left\" font-family=\"Helvetica\" font-size=\"9\"><b><\/b></td>";
                strVar += "<td colspan=\"1\" border-left=\"0\" border-top=\"0\" border-bottom=\"0\" align=\"left\" font-family=\"Helvetica\" font-size=\"9\"><b><\/b></td>";
                strVar += "<td colspan=\"1\" border-left=\"0\" border-top=\"0\" border-bottom=\"0\" align=\"left\" font-family=\"Helvetica\" font-size=\"9\"><b><\/b></td>";
                strVar += "<td colspan=\"1\" border-left=\"0\" border-top=\"0\" border-bottom=\"0\" align=\"left\" font-family=\"Helvetica\" font-size=\"9\"><b><\/b></td>";
                strVar += "<td colspan=\"1\" border-left=\"0\" border-right=\"0\" border-top=\"0\" border-bottom=\"0\" align=\"left\" font-family=\"Helvetica\" font-size=\"9\"><b><\/b></td>";
                strVar += "</tr>";

            }

            i_total_amount_INV = i_total_amount_INV - parseInt(-discount_amt);
            i_total_amount_INV = i_total_amount_INV.toFixed(2);

            strVar += "<tr>";
            strVar += "<td colspan=\"2\" border-left=\"0\" border-top=\"0.3\" border-bottom=\"0.3\" align=\"left\" font-family=\"Helvetica\" font-size=\"9\"><b><\/b></td>";
            strVar += "<td colspan=\"1\" border-left=\"0\" border-top=\"0.3\" border-bottom=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"9\"><b> <\/b></td>";
            strVar += "<td colspan=\"1\" border-left=\"0\" border-top=\"0.3\" border-bottom=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"9\"><b><\/b></td>";
            strVar += "<td colspan=\"1\" border-left=\"0\" border-top=\"0.3\" border-bottom=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"9\"></td>";
            strVar += "<td colspan=\"1\" border-left=\"0\" border-top=\"0.3\" border-bottom=\"0.3\" align=\"right\" font-family=\"Helvetica\" font-size=\"9\"><b>Invoice&nbps;&nbsp;Total :<\/b></td>";
            strVar += "<td colspan=\"1\" border-left=\"0\" border-top=\"0.3\" border-bottom=\"0.3\" align=\"right\" font-family=\"Helvetica\" font-size=\"9\"></td>";
            strVar += "<td colspan=\"1\" border-left=\"0\" border-right=\"0\" border-top=\"0.3\" border-bottom=\"0.3\" align=\"right\" font-family=\"Helvetica\" font-size=\"9\"><b>"
                + s_currency_symbol + "&nbsp;" + formatDollar(parseFloat(i_total_amount_INV).toFixed(2)) + "<\/b></td>";
            strVar += "</tr>";

            strVar += "<tr>";
            strVar += "<td border-right=\"0\" border-left=\"0\"  font-family=\"Helvetica\" font-size=\"9\" colspan=\"8\"><b>Billing Currency&nbsp;&nbsp;&nbsp;:&nbsp;<\/b>" + i_currency + "</td>";
            strVar += "</tr>";

            strVar += "<tr>";
            strVar += "<td border-right=\"0\" border-left=\"0\" border-top=\"0\" border-bottom=\"0.3\" font-family=\"Helvetica\" font-size=\"9\" colspan=\"8\"><b>Amount In Words&nbsp;:&nbsp;<\/b>"
                + toWordsFunc(parseFloat(i_total_amount_INV).toFixed(2), i_currency) + "</td>";
            strVar += "</tr>";

            strVar += "</table>";

            if (i_item == 2221 || i_item == 2603 || i_item == 2612) {
                for (var n = 0; n < dataRow_description.length; n++) {
                    var description = nlapiEscapeXML(dataRow_description[n].Description);
                    var amount_ = dataRow_description[n].Amount;
                    var taxrate = dataRow_description[n].TaxRate;
                    var taxamount = dataRow_description[n].TaxAmount;
                    var rate = dataRow_description[n].rate;
                    var quantity = dataRow_description[n].quantity;
                    if (!_logValidation(rate)) {
                        rate = '';
                    }
                    if (!_logValidation(quantity)) {
                        quantity = '';
                    }
                    if (!_logValidation(description)) {
                        description = '';
                    }
                    xmlContent += "<tr>";
                    var m = n + 1;
                    xmlContent += "<td  border-right='0.5' width='50%' align = 'center' font-family=\"Helvetica\" font-size=\"9\">" + description + " </td>";
                    xmlContent += "<td  border-right='0.5' width='10%' align = 'center' font-family=\"Helvetica\" font-size=\"9\">" + billfrom + "</td>";
                    xmlContent += "<td  border-right='0.5' width='10%' align = 'center' font-family=\"Helvetica\" font-size=\"9\">" + billto + "</td>";
                    xmlContent += "<td  border-right='0.5' width='10%' align = 'center' font-family=\"Helvetica\" font-size=\"9\">" + quantity + "</td>";
                    xmlContent += "<td  border-right='0.5' width='10%' align = 'center' font-family=\"Helvetica\" font-size=\"9\">" + rate + "</td>";
                    xmlContent += "<td  width='35%' align='right' font-family=\"Helvetica\" font-size=\"9\">" + formatNumber(amount_) + "</td>";
                    xmlContent += "</tr>";
                }
            }
            //break

            xmlContent += "<tr>";
            xmlContent += "<td  border-right='0.5' width='40%' align='left'></td>";
            xmlContent += "<td  border-right='0.5' width='10%'></td>";
            xmlContent += "<td  border-right='0.5' width='10%' ></td>";
            xmlContent += "<td  border-right='0.5' width='10%'></td>";
            xmlContent += "<td border-right='0.5' width='15%' align='right'></td>";
            xmlContent += "</tr>";

            xmlContent += "<tr>";
            xmlContent += "<td  border-right='0.5' width='40%' align='left'></td>";
            xmlContent += "<td  border-right='0.5' width='10%'></td>";
            xmlContent += "<td  border-right='0.5' width='10%' ></td>";
            xmlContent += "<td  border-right='0.5' width='10%'></td>";
            xmlContent += "<td border-right='0.5' width='15%' align='right'></td>";
            xmlContent += "</tr>";

            xmlContent += "<tr>";
            xmlContent += "<td  border-right='0.5' width='40%' align='left'></td>";
            xmlContent += "<td  border-right='0.5' width='10%'></td>";
            xmlContent += "<td  border-right='0.5' width='10%' ></td>";
            xmlContent += "<td  border-right='0.5' width='10%'></td>";
            xmlContent += "<td border-right='0.5' width='15%' align='right'></td>";

            xmlContent += "</tr>";

            xmlContent += "<tr>";
            xmlContent += "<td  border-right='0.5' width='40%' align='left'></td>";
            xmlContent += "<td  border-right='0.5' width='10%'></td>";
            xmlContent += "<td  border-right='0.5' width='10%' ></td>";
            xmlContent += "<td  border-right='0.5' width='10%'></td>";
            xmlContent += "<td border-right='0.5' width='15%' align='right'></td>";
            xmlContent += "</tr>";

            xmlContent += "<tr>";
            xmlContent += "<td  border-right='0.5' width='40%' align='left'></td>";
            xmlContent += "<td  border-right='0.5' width='10%'></td>";
            xmlContent += "<td  border-right='0.5' width='10%' ></td>";
            xmlContent += "<td  border-right='0.5' width='10%'></td>";
            xmlContent += "<td border-right='0.5' width='15%' align='right'></td>";
            xmlContent += "</tr>";

            var grand_Total = parseFloat(invoice_amt) + parseFloat(taxamount);
            var converted_words = number2text(grand_Total, currency_);
            xmlContent += "<tr>";
            xmlContent += "<td  border-right='0.5' width='40%' align='left'></td>";
            xmlContent += "<td  border-right='0.5' width='10%'></td>";
            xmlContent += "<td  border-right='0.5' width='10%' ></td>";
            xmlContent += "<td  border-right='0.5' width='10%'></td>";
            xmlContent += "<td border-right='0.5' width='15%' align='right'></td>";
            xmlContent += "</tr>";

            xmlContent += "<tr>";
            xmlContent += "<td  border-right='0.5' width='40%' align='left'></td>";
            xmlContent += "<td  border-right='0.5' width='10%'></td>";
            xmlContent += "<td  border-right='0.5' width='10%' ></td>";
            xmlContent += "<td  border-right='0.5' width='10%'></td>";
            xmlContent += "<td border-right='0.5' width='15%' align='right'></td>";

            xmlContent += "</tr>";

            xmlContent += "<tr>";
            xmlContent += "<td  border-right='0.5' width='40%' align='left'></td>";
            xmlContent += "<td  border-right='0.5' width='10%'></td>";
            xmlContent += "<td  border-right='0.5' width='10%' ></td>";
            xmlContent += "<td  border-right='0.5' width='10%'></td>";
            xmlContent += "<td border-right='0.5' width='15%' align='right'></td>";

            xmlContent += "</tr>";

            xmlContent += "<tr>";
            xmlContent += "<td  border-right='0.5' width='40%' align='left'></td>";
            xmlContent += "<td  border-right='0.5' width='10%'></td>";
            xmlContent += "<td  border-right='0.5' width='10%' ></td>";
            xmlContent += "<td  border-right='0.5' width='10%'></td>";
            xmlContent += "<td border-right='0.5' width='15%' align='right'></td>";
            xmlContent += "</tr>";

            xmlContent += "<tr>";
            xmlContent += "<td  border-right='0.5' width='40%' align='left'></td>";
            xmlContent += "<td  border-right='0.5' width='10%'></td>";
            xmlContent += "<td  border-right='0.5' width='10%' ></td>";
            xmlContent += "<td  border-right='0.5' width='10%'></td>";
            xmlContent += "<td border-right='0.5' width='15%' align='right'></td>";
            xmlContent += "</tr>";

            xmlContent += "<tr>";
            xmlContent += "<td  border-right='0.5' width='40%' align='left'></td>";
            xmlContent += "<td  border-right='0.5' width='10%'></td>";
            xmlContent += "<td  border-right='0.5' width='10%' ></td>";
            xmlContent += "<td  border-right='0.5' width='10%'></td>";
            xmlContent += "<td border-right='0.5' width='15%' align='right'></td>";

            xmlContent += "</tr>";
            xmlContent += "<tr>";
            xmlContent += "<td  border-right='0.5' width='40%' align='left'></td>";
            xmlContent += "<td  border-right='0.5' width='10%'></td>";
            xmlContent += "<td  border-right='0.5' width='10%' ></td>";
            xmlContent += "<td  border-right='0.5' width='10%'></td>";
            xmlContent += "<td border-right='0.5' width='15%' align='right'></td>";
            xmlContent += "</tr>";

            xmlContent += "<tr>";
            xmlContent += "<td  border-right='0.5' width='40%' align='left'></td>";
            xmlContent += "<td  border-right='0.5' width='10%'></td>";
            xmlContent += "<td  border-right='0.5' width='10%' ></td>";
            xmlContent += "<td  border-right='0.5' width='10%'></td>";
            xmlContent += "<td border-right='0.5' width='15%' align='right'></td>";
            xmlContent += "</tr>";

            xmlContent += "<tr>";
            xmlContent += "<td  border-right='0.5' width='40%' align='left'></td>";
            xmlContent += "<td  border-right='0.5' width='10%'></td>";
            xmlContent += "<td  border-right='0.5' width='10%' ></td>";
            xmlContent += "<td  border-right='0.5' width='10%'></td>";
            xmlContent += "<td border-right='0.5' width='15%' align='right'></td>";
            xmlContent += "</tr>";

            xmlContent += "<tr>";
            xmlContent += "<td  border-right='0.5' width='40%' align='left'></td>";
            xmlContent += "<td  border-right='0.5' width='10%'></td>";
            xmlContent += "<td  border-right='0.5' width='10%' ></td>";
            xmlContent += "<td  border-right='0.5' width='10%'></td>";
            xmlContent += "<td border-right='0.5' width='15%' align='right'></td>";
            xmlContent += "</tr>";

            xmlContent += "<tr>";
            xmlContent += "<td  border-right='0.5' width='40%' align='left'></td>";
            xmlContent += "<td  border-right='0.5' width='10%'></td>";
            xmlContent += "<td  border-right='0.5' width='10%' ></td>";
            xmlContent += "<td  border-right='0.5' width='10%'></td>";
            xmlContent += "<td border-right='0.5' width='15%' align='right'></td>";
            xmlContent += "</tr>";

            xmlContent += "<tr>";
            xmlContent += "<td  border-right='0.5' width='40%' align='left'></td>";
            xmlContent += "<td  border-right='0.5' width='10%'></td>";
            xmlContent += "<td  border-right='0.5' width='10%' ></td>";
            xmlContent += "<td  border-right='0.5' width='10%'></td>";
            xmlContent += "<td border-right='0.5' width='15%' align='right'></td>";
            xmlContent += "</tr>";

            xmlContent += "<tr>";
            xmlContent += "<td  border-right='0.5' width='40%' align='left'></td>";
            xmlContent += "<td  border-right='0.5' width='10%'></td>";
            xmlContent += "<td  border-right='0.5' width='10%' ></td>";
            xmlContent += "<td  border-right='0.5' width='10%'></td>";
            xmlContent += "<td border-right='0.5' width='15%' align='right'></td>";
            xmlContent += "</tr>";

            xmlContent += "<tr>";
            xmlContent += "<td  border-right='0.5' width='40%' align='left'></td>";
            xmlContent += "<td  border-right='0.5' width='10%'></td>";
            xmlContent += "<td  border-right='0.5' width='10%' ></td>";
            xmlContent += "<td  border-right='0.5' width='10%'></td>";
            xmlContent += "<td border-right='0.5' width='15%' align='right'></td>";
            xmlContent += "</tr>";

            xmlContent += "<tr>";
            xmlContent += "<td  border-right='0.5' width='40%' align='left'></td>";
            xmlContent += "<td  border-right='0.5' width='10%'></td>";
            xmlContent += "<td  border-right='0.5' width='10%' ></td>";
            xmlContent += "<td  border-right='0.5' width='10%'></td>";
            xmlContent += "<td border-right='0.5' width='15%' align='right'></td>";
            xmlContent += "</tr>";

            xmlContent += "<tr>";
            xmlContent += "<td  border-right='0.5' width='40%' align='left'></td>";
            xmlContent += "<td  border-right='0.5' width='10%'></td>";
            xmlContent += "<td  border-right='0.5' width='10%' ></td>";
            xmlContent += "<td  border-right='0.5' width='10%'></td>";
            xmlContent += "<td border-right='0.5' width='15%' align='right'></td>";
            xmlContent += "</tr>";

            xmlContent += "<tr>";
            xmlContent += "<td  border-right='0.5' width='40%' align='left'></td>";
            xmlContent += "<td  border-right='0.5' width='10%'></td>";
            xmlContent += "<td  border-right='0.5' width='10%' ></td>";
            xmlContent += "<td  border-right='0.5' width='10%'></td>";
            xmlContent += "<td border-right='0.5' width='15%' align='right'></td>";
            xmlContent += "</tr>";

            xmlContent += "<tr>";
            xmlContent += "<td  border-right='0.5' width='40%' align='left'></td>";
            xmlContent += "<td  border-right='0.5' width='10%'></td>";
            xmlContent += "<td  border-right='0.5' width='10%' ></td>";
            xmlContent += "<td  border-right='0.5' width='10%'></td>";
            xmlContent += "<td border-right='0.5' width='15%' align='right'></td>";
            xmlContent += "</tr>";

            xmlContent += "<tr>";
            xmlContent += "<td  border-right='0.5' width='40%' align='left'></td>";
            xmlContent += "<td  border-right='0.5' width='10%'></td>";
            xmlContent += "<td  border-right='0.5' width='10%' ></td>";
            xmlContent += "<td  border-right='0.5' width='10%'></td>";
            xmlContent += "<td border-right='0.5' width='15%' align='right'></td>";
            xmlContent += "</tr>";

            xmlContent += "<tr>";
            xmlContent += "<td  border-right='0.5' width='40%' align='left'></td>";
            xmlContent += "<td  border-right='0.5' width='10%'></td>";
            xmlContent += "<td  border-right='0.5' width='10%' ></td>";
            xmlContent += "<td  border-right='0.5' width='10%'></td>";
            xmlContent += "<td border-right='0.5' width='15%' align='right'></td>";
            xmlContent += "</tr>";

            xmlContent += "<tr>";
            xmlContent += "<td  border-right='0.5' width='40%' align='left'></td>";
            xmlContent += "<td  border-right='0.5' width='10%'></td>";
            xmlContent += "<td  border-right='0.5' width='10%' ></td>";
            xmlContent += "<td  border-right='0.5' width='10%'></td>";
            xmlContent += "<td border-right='0.5' width='15%' align='right'></td>";
            xmlContent += "</tr>";

            xmlContent += "<tr>";
            xmlContent += "<td  border-right='0.5' width='40%' align='left'></td>";
            xmlContent += "<td  border-right='0.5' width='10%'></td>";
            xmlContent += "<td  border-right='0.5' width='10%' ></td>";
            xmlContent += "<td  border-right='0.5' width='10%'></td>";
            xmlContent += "<td border-right='0.5' width='15%' align='right'></td>";
            xmlContent += "</tr>";

            xmlContent += "<tr>";
            xmlContent += "<td  border-right='0.5' width='40%' align='left'></td>";
            xmlContent += "<td  border-right='0.5' width='10%'></td>";
            xmlContent += "<td  border-right='0.5' width='10%' ></td>";
            xmlContent += "<td  border-right='0.5' width='10%'></td>";
            xmlContent += "<td border-right='0.5' width='15%' align='right'></td>";
            xmlContent += "</tr>";

            xmlContent += "<tr>";
            xmlContent += "<td  border-right='0.5' width='40%' align='left'></td>";
            xmlContent += "<td  border-right='0.5' width='10%'></td>";
            xmlContent += "<td  border-right='0.5' width='10%' ></td>";
            xmlContent += "<td  border-right='0.5' width='10%'></td>";
            xmlContent += "<td border-right='0.5' width='15%' align='right'></td>";
            xmlContent += "</tr>";

            xmlContent += "<tr>";
            xmlContent += "<td  border-right='0.5' width='40%' align='left'></td>";
            xmlContent += "<td  border-right='0.5' width='10%'></td>";
            xmlContent += "<td  border-right='0.5' width='10%' ></td>";
            xmlContent += "<td  border-right='0.5' width='10%'></td>";
            xmlContent += "<td border-right='0.5' width='15%' align='right'></td>";
            xmlContent += "</tr>";

            var sub_total = o_recordOBJ.getFieldValue('subtotal')

            if (!_logValidation(sub_total)) {
                sub_total = 0
            }
            var tax_percent = (i_tax * 100) / sub_total

            if (i_tax != 0) {
                xmlContent += "<tr>";
                xmlContent += "<td  border-right='0.5' width='40%' align='left' font-family='Helvetica' font-size='9'>VAT@" + parseFloat(tax_percent).toFixed(2) + "%</td>";
                xmlContent += "<td  border-right='0.5' width='10%'></td>";
                xmlContent += "<td  border-right='0.5' width='10%' ></td>";
                xmlContent += "<td  border-right='0.5' width='10%'></td>";
                xmlContent += "<td  border-right='0.5' width='10%'></td>";
                xmlContent += "<td border-right='0.5' width='15%' align='right' font-family='Helvetica' font-size='9'>" + i_tax + "</td>";

                xmlContent += "</tr>";
            }
            xmlContent += "<tr>";
            xmlContent += "<td  border-right='0.5' width='40%' align='left'></td>";
            xmlContent += "<td  border-right='0.5' width='10%'></td>";
            xmlContent += "<td  border-right='0.5' width='10%' ></td>";
            xmlContent += "<td  border-right='0.5' width='10%'></td>";
            xmlContent += "<td border-right='0.5' width='15%' align='right'></td>";
            xmlContent += "</tr>";

            xmlContent += "<tr>";
            xmlContent += "<td  border-right='0.5' width='40%' align='left'></td>";
            xmlContent += "<td  border-right='0.5' width='10%'></td>";
            xmlContent += "<td  border-right='0.5' width='10%' ></td>";
            xmlContent += "<td  border-right='0.5' width='10%'></td>";
            xmlContent += "<td border-right='0.5' width='15%' align='right'></td>";
            xmlContent += "</tr>";

            xmlContent += "<tr>";
            xmlContent += "<td  border-right='0.5' width='40%' align='left'></td>";
            xmlContent += "<td  border-right='0.5' width='10%'></td>";
            xmlContent += "<td  border-right='0.5' width='10%' ></td>";
            xmlContent += "<td  border-right='0.5' width='10%'></td>";
            xmlContent += "<td border-right='0.5' width='15%' align='right'></td>";
            xmlContent += "</tr>";

            xmlContent += "<tr>";
            xmlContent += "<td  border-right='0.5' width='40%' align='left'></td>";
            xmlContent += "<td  border-right='0.5' width='10%'></td>";
            xmlContent += "<td  border-right='0.5' width='10%' ></td>";
            xmlContent += "<td  border-right='0.5' width='10%'></td>";
            xmlContent += "<td border-right='0.5' width='15%' align='right'></td>";
            xmlContent += "</tr>";

            xmlContent += "<tr>";
            xmlContent += "<td  border-right='0.5' width='40%' align='left'></td>";
            xmlContent += "<td  border-right='0.5' width='10%'></td>";
            xmlContent += "<td  border-right='0.5' width='10%' ></td>";
            xmlContent += "<td  border-right='0.5' width='10%'></td>";
            xmlContent += "<td border-right='0.5' width='15%' align='right'></td>";
            xmlContent += "</tr>";

            xmlContent += "<tr>";
            xmlContent += "<td  border-right='0.5' width='40%' align='left'></td>";
            xmlContent += "<td  border-right='0.5' width='10%'></td>";
            xmlContent += "<td  border-right='0.5' width='10%' ></td>";
            xmlContent += "<td  border-right='0.5' width='10%'></td>";
            xmlContent += "<td border-right='0.5' width='15%' align='right'></td>";
            xmlContent += "</tr>";

            xmlContent += "<tr>";
            xmlContent += "<td  border-right='0.5' width='40%' align='left'></td>";
            xmlContent += "<td  border-right='0.5' width='10%'></td>";
            xmlContent += "<td  border-right='0.5' width='10%' ></td>";
            xmlContent += "<td  border-right='0.5' width='10%'></td>";
            xmlContent += "<td border-right='0.5' width='15%' align='right'></td>";
            xmlContent += "</tr>";

            xmlContent += "<tr>";
            xmlContent += "<td  border-top='0.5' width='45%' colspan='2' align='left' font-family=\"Helvetica\" font-size=\"9\"><b>Billing Currency - " + i_currency + "</b></td>";
            xmlContent += "<td  border-top='0.5' width='15%'></td>";
            xmlContent += "<td  border-top='0.5' border-right='0.5' width='15%'></td>";
            xmlContent += "<td border-top='0.5' border-right='0.5' width='5%' font-family=\"Helvetica\" font-size=\"9\" align='center' ><b>   Grand Total   </b></td>";

            if (i_item == 2221 || i_item == 2603 || i_item == 2612) {
                xmlContent += "<td border-top='0.5' width='15%' align='right' font-family=\"Helvetica\" font-size=\"9\"><b>"
                    + s_currency_symbol + "&nbsp;" + formatNumber(grand_Total) + "</b></td>";

            }
            else {
                xmlContent += "<td border-top='0.5' width='15%' align='right' font-family=\"Helvetica\" font-size=\"9\"><b>"
                    + s_currency_symbol + "&nbsp;" + formatDollar(parseFloat(i_total_amount_INV).toFixed(2)) + "</b></td>";
            }
            xmlContent += "</tr>";

            var currency_type;
            if (i_currency == 'USD') {
                currency_type = 'Dollars';
            }
            else if (i_currency == 'INR') {
                currency_type = 'Rupees';
            }
            else if (i_currency == 'GBP') {
                currency_type = 'Pound';
            }
            else if (i_currency == 'EUR') {
                currency_type = 'Euro';
            }
            xmlContent += "<tr border='0.5'>";
            xmlContent += "<td  border-right='0.5' width='45%'  font-family=\"Helvetica\" font-size=\"9\" colspan='2'><b>Amount in Words:</b></td>";

            if (i_item == 2221 || i_item == 2603 || i_item == 2612) {

                xmlContent += "<td  width='55%' font-family=\"Helvetica\" font-size=\"9\" colspan='6'><b> " + currency_type + ' ' + converted_words + "</b></td>";
            }
            else {
                xmlContent += "<td  width='55%' font-family=\"Helvetica\" font-size=\"9\" colspan='6'><b> " + toWordsFunc(parseFloat(i_total_amount_INV).toFixed(2), i_currency) + "</b></td>";
            }
            xmlContent += "</tr>";
            xmlContent += "<tr border='0.5'>";
            xmlContent += "<td  width='45%'  font-family=\"Helvetica\" font-size=\"9\" colspan='2'><b>SAC Code: 998313 : </b></td>";
            xmlContent += "<td  width='55%' font-family=\"Helvetica\" font-size=\"9\" colspan='6'>Information technology (IT) consulting and support services</td>";
            xmlContent += "</tr>";

            xmlContent += "<tr border='0.5'>";
            xmlContent += "<td  border-right='0.5' width='50%' align='center' font-family=\"Helvetica\" font-size=\"9\" colspan='4'><br/>"
            xmlContent += "<img width=\"120\" height=\"120\"   src= '" + url + "'/>"
            xmlContent += "<br/>";
            xmlContent += "</td>";
            xmlContent += "<td  border-right='0.5' align='center' width='45%' colspan='4' font-family=\"Helvetica\" font-size=\"9\"><b>For Brillio Technologies Pvt Ltd</b><br/>"
            xmlContent += "<br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>";
            xmlContent += "<b align=\"center\" colspan = \"4\" font-size=\"8\">Authorised Signatory<\/b></td>";
            xmlContent += "</tr>";

            xmlContent += "</table>"

            //Commented the Old office address and added the new office address
            /*
            //Registered Office:
            xmlContent += "<table width='100%'>"
            xmlContent += "<tr>";
            xmlContent += "<td border-left = '0.5' border-right = '0.5' align='center' font-size='7'><b>Registered office: Brillio Technologies Pvt. Ltd.,No. 58, 1st Main Road, Mini Forest,</b></td>";
            xmlContent += "</tr>";
            xmlContent += "<tr>";
            xmlContent += "<td border-left = '0.5' border-right = '0.5' border-bottom = '0.5' align='center' font-size='7'><b> J P Nagar 3rd Phase, Bangalore 560 078, Karnataka</b></td>";
            xmlContent += "</tr>";
            xmlContent += "</table>"
            */
            //Registered Office:
            xmlContent += "<table width='100%'>"
            xmlContent += "<tr>";
            xmlContent += "<td border-left = '0.5' border-right = '0.5' align='center' font-size='7'><b>Registered office: Brillio Technologies Pvt. Ltd.,No. 8/2, 4th Floor, Bren Optimus,</b></td>";
            xmlContent += "</tr>";
            xmlContent += "<tr>";
            xmlContent += "<td border-left = '0.5' border-right = '0.5' border-bottom = '0.5' align='center' font-size='7'><b> Dr. M H Marigowda Road, Bangalore, Bengaluru (Bangalore) Urban,Karnataka, 560029</b></td>";
            xmlContent += "</tr>";
            xmlContent += "</table>"

            var xml = "<?xml version=\"1.0\"?>\n<!DOCTYPE pdf PUBLIC \"-//big.faceless.org//report\" \"report-1.1.dtd\">\n";
            xml += "<pdf>\n<body>";
            xml += xmlContent;
            xml += "</body>\n</pdf>";

            var file = nlapiXMLToPDF(xml);
            var file_name = 'BTPL_' + invoice_number + '.pdf';
            file.setName('' + file_name)
            response.setContentType('PDF', '' + file_name, 'inline');
            response.write(file.getValue());
        }
        nlapiLogExecution("debug", "Script Ended");
    }
    catch (err) {
        nlapiLogExecution('ERROR', 'BTPL PDF Suitelet Error', err);
        throw err;
    }
}
function _logValidation(value) {
    if (value != null && value != '' && value != undefined && value.toString() != 'NaN' && value != NaN) {
        return true;
    }
    else {
        return false;
    }
}

function getMonth(month) {
    try {
        var month_Text = '';

        if (month == '01') {
            month_Text = 'Jan';
        }
        if (month == '02') {
            month_Text = 'Feb';
        }
        if (month == '03') {
            month_Text = 'Mar';
        }
        if (month == '04') {
            month_Text = 'Apr';
        }
        if (month == '05') {
            month_Text = 'May';
        }
        if (month == '06') {
            month_Text = 'Jun';
        }
        if (month == '07') {
            month_Text = 'Jul';
        }
        if (month == '08') {
            month_Text = 'Aug';
        }
        if (month == '09') {
            month_Text = 'Sep';
        }
        if (month == '10') {
            month_Text = 'Oct';
        }
        if (month == '11') {
            month_Text = 'Nov';
        }
        if (month == '12') {
            month_Text = 'Dec';
        }
        return month_Text;
    }
    catch (e) {
        nlapiLogExecution('ERROR', 'BTPL PDF Suitelet Error', e);
    }

}

function formatNumber(num) {
    return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,")
}

function _logValidation(value) {
    if (value != null && value.toString() != null && value != '' && value != undefined && value.toString() != undefined && value != 'undefined' && value.toString() != 'undefined' && value.toString() != 'NaN' && value != NaN) {
        return true;
    }
    else {
        return false;
    }
}

function get_start_of_week(date) {
    var now = nlapiStringToDate(date)
    now.setHours(0, 0, 0, 0);

    if (now.getDay() == '0') {
        // Get the previous Monday
        var monday = new Date(now);
        monday.setDate(monday.getDate() - 7 + 1);
        monday = nlapiDateToString(monday)

        // Get next Sunday
        var sunday = new Date(now);
        sunday.setDate(sunday.getDate() - sunday.getDay() + 7);
        sunday = nlapiDateToString(sunday)
    }
    else {
        var monday = new Date(now);
        monday.setDate(monday.getDate() - monday.getDay() + 1);
        monday = nlapiDateToString(monday)
        var sunday = new Date(now);
        sunday.setDate(sunday.getDate() - sunday.getDay() + 7);
        sunday = nlapiDateToString(sunday)

    }
    return monday;
}

function get_end_of_week(date) {
    var now = nlapiStringToDate(date)
    now.setHours(0, 0, 0, 0);
    var monday = new Date(now);
    var lastday = new Date(new Date(now).getFullYear(), new Date(now).getMonth() + 1, 0)
    var f = parseInt(monday.getDate());
    var f1 = parseInt(lastday.getDate());

    if (f == f1) {
        var sunday = date;
    }
    else {
        if (now.getDay() == '0') {
            // Get the previous Monday
            monday.setDate(monday.getDate() - monday.getDay() + 1);
            monday = nlapiDateToString(monday)
            // Get next Sunday
            var sunday = new Date(now);
            sunday.setDate(sunday.getDate() - 7 + 7);
            sunday = nlapiDateToString(sunday)
        }
        else {
            monday.setDate(monday.getDate() - monday.getDay() + 1);
            monday = nlapiDateToString(monday)
            // Get next Sunday
            var sunday = new Date(now);
            sunday.setDate(sunday.getDate() - sunday.getDay() + 7);
            if (sunday > lastday) {
                sunday = lastday;
            }
            sunday = nlapiDateToString(sunday)
        }
    }
    return sunday;
}

/**
* 
* @param {Object}
*            value
* 
* Description --> If the value is blank /null/undefined returns false else
* returns true
*/
function _logValidation(value) {
    if (value != 'null' && value != null && value.toString() != null && value != '' && value != undefined
        && value.toString() != undefined && value != 'undefined' && value.toString() != 'undefined'
        && value.toString() != 'NaN' && value != NaN) {
        return true;
    }
    else {
        return false;
    }
}

function get_customer_details(i_customer) {
    var i_address;
    var i_attention;
    var i_service_tax_reg_no;
    var i_CIN_No;
    var i_phone = '';
    var a_return_array = new Array();
    if (_logValidation(i_customer)) {
        var o_customerOBJ = nlapiLoadRecord('customer', i_customer)
        if (_logValidation(o_customerOBJ)) {
            i_address = o_customerOBJ.getFieldValue('defaultaddress')
            i_service_tax_reg_no = o_customerOBJ.getFieldValue('vatregnumber')
            i_CIN_No = o_customerOBJ.getFieldValue('custentity_cinnumber')
            i_phone = o_customerOBJ.getFieldValue('phone')
            var i_addr_count = o_customerOBJ.getLineItemCount('addressbook')

            if (_logValidation(i_addr_count)) {
                for (var r = 1; r <= i_addr_count; r++) {
                    var i_default_billing = o_customerOBJ.getLineItemValue('addressbook', 'defaultbilling', r)
                    if (i_default_billing == 'T') {
                        i_attention = o_customerOBJ.getLineItemValue('addressbook', 'attention', r);

                        i_addr1 = o_customerOBJ.getLineItemValue('addressbook', 'addr1', r);
                        i_addr2 = o_customerOBJ.getLineItemValue('addressbook', 'addr2', r);
                        i_city = o_customerOBJ.getLineItemValue('addressbook', 'city', r);
                        i_zip = o_customerOBJ.getLineItemValue('addressbook', 'zip', r);
                        i_country = o_customerOBJ.getLineItemText('addressbook', 'country', r);
                        i_state = o_customerOBJ.getLineItemValue('addressbook', 'displaystate', r);
                        break;
                    }// Default Billing
                }// Address Loop
            }// Address Count
            a_return_array[0] = i_address + '######' + i_attention + '######' + i_service_tax_reg_no + '######'
                + i_CIN_No + '######' + i_phone + '######' + i_addr1 + '######' + i_addr2 + '######'
                + i_city + '######' + i_zip + '######' + i_country + '######' + i_state
        }
    }
    return a_return_array;
}

function formatAndReplaceSpacesofMessage(messgaeToBeSendPara) {
    if (messgaeToBeSendPara != null && messgaeToBeSendPara != ''
        && messgaeToBeSendPara != undefined) {
        messgaeToBeSendPara = messgaeToBeSendPara.toString();
        messgaeToBeSendPara = messgaeToBeSendPara.replace(/\n/g, "<br/>");// / /g
        return messgaeToBeSendPara;
    }
}

function get_address(i_subsidiary) {
    var i_address;
    if (_logValidation(i_subsidiary)) {
        var o_subsidiaryOBJ = nlapiLoadRecord('subsidiary', i_subsidiary)
        if (_logValidation(o_subsidiaryOBJ)) {
            i_address = o_subsidiaryOBJ.getFieldValue('addrtext')
        }
    }
    return i_address;
}

function get_address_rem(i_subsidiary) {
    var i_address;
    if (_logValidation(i_subsidiary)) {
        var o_subsidiaryOBJ = nlapiLoadRecord('subsidiary', i_subsidiary)
        if (_logValidation(o_subsidiaryOBJ)) {
            i_address_rem = o_subsidiaryOBJ.getFieldValue('mainaddress_text')
        }
    }
    return i_address_rem;
}

function removearrayduplicate(array) {
    var newArray = new Array();
    label: for (var i = 0; i < array.length; i++) {
        for (var j = 0; j < array.length; j++) {
            if (newArray[j] == array[i])
                continue label;
        }
        newArray[newArray.length] = array[i];
    }
    return newArray;
}
function get_employeeID(i_employee) {
    var i_internal_id;
    if (_logValidation(i_employee)) {
        var filter = new Array();
        filter[0] = new nlobjSearchFilter('entityid', null, 'is', i_employee);

        var columns = new Array();
        columns[0] = new nlobjSearchColumn('internalid');

        var a_search_results = nlapiSearchRecord('employee', null, filter, columns);

        if (a_search_results != null && a_search_results != '' && a_search_results != undefined) {
            i_internal_id = a_search_results[0].getValue('internalid');
        }

    }// Employee
    return i_internal_id;
}

function toWordsFunc(s, i_currency) {
    var str = '';
    if (i_currency == 'USD') {
        str = '$' + ' '
    }
    if (i_currency == 'INR') {
        str = 'Rs.' + ' '
    }
    if (i_currency == 'GBP') {
        str = '�' + ' '
    }
    if (i_currency == 'SGD') {
        str = 'S$' + ' '
    }
    if (i_currency == 'Peso') {
        str = 'P' + ' '
    } if (i_currency == 'EUR') {
        str = '€'
    }
    return str + ' ' + toWords(s);

    var th = new Array('Billion ', 'Million ', 'Thousand ', 'Hundred ');
    var dg = new Array('1000000000', '1000000', '1000', '100');
    var dem = s.substr(s.lastIndexOf('.') + 1)
    s = parseInt(s)
    var d
    var n1, n2
    while (s >= 100) {
        for (var k = 0; k < 4; k++) {
            d = parseInt(s / dg[k])
            if (d > 0) {
                if (d >= 20) {
                    n1 = parseInt(d / 10)
                    n2 = d % 10
                    printnum2(n1)
                    printnum1(n2)
                } else
                    printnum1(d)
                str = str + th[k]
            }
            s = s % dg[k]
        }
    }
    if (s >= 20) {
        n1 = parseInt(s / 10)
        n2 = s % 10
    } else {
        n1 = 0
        n2 = s
    }

    printnum2(n1)
    printnum1(n2)
    if (dem > 0) {
        decprint(dem)
    }
    return str

    function decprint(nm) {
        if (nm >= 20) {
            n1 = parseInt(nm / 10)
            n2 = nm % 10
        }
        else {
            n1 = 0
            n2 = parseInt(nm)
        }
        str = str + 'And '
        printnum2(n1)
        printnum1(n2)
    }

    function printnum1(num1) {
        switch (num1) {
            case 1:
                str = str + 'One '
                break;
            case 2:
                str = str + 'Two '
                break;
            case 3:
                str = str + 'Three '
                break;
            case 4:
                str = str + 'Four '
                break;
            case 5:
                str = str + 'Five '
                break;
            case 6:
                str = str + 'Six '
                break;
            case 7:
                str = str + 'Seven '
                break;
            case 8:
                str = str + 'Eight '
                break;
            case 9:
                str = str + 'Nine '
                break;
            case 10:
                str = str + 'Ten '
                break;
            case 11:
                str = str + 'Eleven '
                break;
            case 12:
                str = str + 'Twelve '
                break;
            case 13:
                str = str + 'Thirteen '
                break;
            case 14:
                str = str + 'Fourteen '
                break;
            case 15:
                str = str + 'Fifteen '
                break;
            case 16:
                str = str + 'Sixteen '
                break;
            case 17:
                str = str + 'Seventeen '
                break;
            case 18:
                str = str + 'Eighteen '
                break;
            case 19:
                str = str + 'Nineteen '
                break;
        }
    }

    function printnum2(num2) {
        switch (num2) {
            case 2:
                str = str + 'Twenty '
                break;
            case 3:
                str = str + 'Thirty '
                break;
            case 4:
                str = str + 'Forty '
                break;
            case 5:
                str = str + 'Fifty '
                break;
            case 6:
                str = str + 'Sixty '
                break;
            case 7:
                str = str + 'Seventy '
                break;
            case 8:
                str = str + 'Eighty '
                break;
            case 9:
                str = str + 'Ninety '
                break;
        }
    }
}

function toWords(s) {
    var th = ['', 'thousand', 'million', 'billion', 'trillion'];
    var dg = ['zero', 'one', 'two', 'three', 'four', 'five', 'six', 'seven', 'eight', 'nine'];
    var tn = ['ten', 'eleven', 'twelve', 'thirteen', 'fourteen', 'fifteen', 'sixteen', 'seventeen', 'eighteen', 'nineteen'];
    var tw = ['twenty', 'thirty', 'forty', 'fifty', 'sixty', 'seventy', 'eighty', 'ninety'];

    s = s.toString();
    s = s.replace(/[\, ]/g, '');
    if (s != parseFloat(s))
        return 'not a number';
    var x = s.indexOf('.');
    if (x == -1)
        x = s.length;
    if (x > 15)
        return 'too big';
    var n = s.split('');
    var str = '';
    var sk = 0;
    for (var i = 0; i < x; i++) {
        if ((x - i) % 3 == 2) {
            if (n[i] == '1') {
                str += tn[Number(n[i + 1])] + ' ';
                i++;
                sk = 1;
            }
            else if (n[i] != 0) {
                str += tw[n[i] - 2] + ' ';
                sk = 1;
            }
        }
        else if (n[i] != 0) {
            str += dg[n[i]] + ' ';
            if ((x - i) % 3 == 0)
                str += 'hundred ';
            sk = 1;
        }
        if ((x - i) % 3 == 1) {
            if (sk) {
                str += th[(x - i - 1) / 3] + ' ';
            }
            sk = 0;
        }
    }
    if (x != s.length) {
        var y = s.length;
        str += 'point ';
        for (var i = x + 1; i < y; i++) {
            str += dg[n[i]] + ' ';
        }
    }
    return str.replace(/\s+/g, ' ');
}

function formatDollar(somenum) {
    var split_arr = new Array()
    var i_no_before_comma;
    var i_no_before_comma_length;

    if (somenum != null && somenum != '' && somenum != undefined) {
        split_arr = somenum.toString().split('.')
        i_no_before_comma = split_arr[0]
        i_no_before_comma = Math.abs(i_no_before_comma)

        if (i_no_before_comma.toString().length <= 3) {
            return somenum;
        }
        else {
            var p = somenum.toString().split(".");
            if (p[1] != null && p[1] != '' && p[1] != undefined) {
                p[1] = p[1]
            }
            else {
                p[1] = '00'
            }
            return p[0].split("").reverse().reduce(
                function (acc, somenum, i, orig) {
                    return somenum + (i && !(i % 3) ? "," : "") + acc;
                }
                , "") + "." + p[1];
        }
    }
}
function checkDateFormat() {
    var context = nlapiGetContext();
    var dateFormatPref = context.getPreference('dateformat');
    return dateFormatPref;
}

function get_date(date1) {
    var today;
    var offsetIST = 5.5;

    // To convert to UTC datetime by subtracting the current Timezone offset
    var utcdate = new Date(date1.getTime() + (date1.getTimezoneOffset() * 60000));
    var istdate = date1;
    var day = istdate.getDate();
    var month = istdate.getMonth() + 1;
    var year = istdate.getFullYear();
    var date_format = checkDateFormat();

    if (month == 1) {
        month = '01'
    }
    else if (month == 2) {
        month = '02'
    }
    else if (month == 3) {
        month = '03'
    }
    else if (month == 4) {
        month = '04'
    }
    else if (month == 5) {
        month = '05'
    } else if (month == 6) {
        month = '06'
    } else if (month == 7) {
        month = '07'
    } else if (month == 8) {
        month = '08'
    } else if (month == 9) {
        month = '09'
    }
    if (day == 1) {
        day = '01'
    }
    else if (day == 2) {
        day = '02'
    }
    else if (day == 3) {
        day = '03'
    }

    else if (day == 4) {
        day = '04'
    }
    else if (day == 5) {
        day = '05'
    }
    else if (day == 6) {
        day = '06'
    }
    else if (day == 7) {
        day = '07'
    }
    else if (day == 8) {
        day = '08'
    }
    else if (day == 9) {
        day = '09'
    }
    if (date_format == 'YYYY-MM-DD') {
        today = year + '-' + month + '-' + day;
    }
    if (date_format == 'DD/MM/YYYY') {
        today = day + '/' + month + '/' + year;
    }
    if (date_format == 'MM/DD/YYYY') {
        today = month + '/' + day + '/' + year;
    }
    if (date_format == 'M/D/YYYY') {
        today = month + '/' + day + '/' + year;
    }
    return today;
}

function header_adress(text, subsidiary) {
    var iChars = subsidiary;
    var text_new = ''

    if (text != null && text != '' && text != undefined) {
        for (var y = 0; y < text.toString().length; y++) {
            text_new += text[y]
            if (text_new == subsidiary) {
                text_new = '';
            }// Break
        }// Loop
    }// Text Validation
    return text_new;
}// Header Address

function escape_special_chars(text) {
    var iChars = ".@()~`!#$%^&*+=-[]\\\';,/{}|\":<>?.1234567890";
    var text_new = ''
    if (text != null && text != '' && text != undefined) {
        for (var y = 0; y < text.toString().length; y++) {
            if (iChars.indexOf(text[y]) == -1) {
                text_new += text[y]
            }
        }// Loop
    }// Text Validation
    return text_new;
}

function _nullValidation(value) {
    if (value == null || value == undefined || value == '') {
        return true;
    }
    else {
        return false;
    }
}