/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       19 Jul 2017     deepak.srinivas
 *
 */

/**
 * @param {String} type Context Types: scheduled, ondemand, userinterface, aborted, skipped
 * @returns {Void}
 */
function scheduled(type){
	
	try{
		var context = nlapiGetContext();
		var je_list = new Array();
		//var inv_id = '450963';
		//var closingComments = 'Project status updated as closed as it has been more than 90 days past the end date of the project.';
		var searchObj = nlapiSearchRecord('timesheet','customsearch1992');
		if(searchObj){
			for(var i=0;i<searchObj.length;i++){
				var result = searchObj[i];
				var columns = result.getAllColumns();
				
				if(result){
					var employee = result.getValue(columns[0]);
					
					var emp_lookUP = nlapiLookupField('employee',parseInt(employee),['firstname','email']);
					var emp_name = emp_lookUP.firstname;
					var emp_email = emp_lookUP.email;
				
					var strVar = '';
					strVar += '<html>';
					strVar += '<body>';
					
					strVar += '<p>Dear '+ emp_name +',</p>';
					//strVar += '<br/>';
					strVar += '<p>We are very excited to announce that the TimeSheet Approval feature is now live on the Brillio On The Go app !';
					strVar += ' You can approve your timesheets within seconds.  It is as simple as that! </p>';
					//strVar += '<br/>';
					strVar += '<p> Its available in Andriod, IOS,  url: https://onthegoprod.blob.core.windows.net/onthego/index.html</p>';
			
					//strVar += '<br/>';
					//strVar += '<a href="https://system.sandbox.netsuite.com/app/site/hosting/scriptlet.nl?script=1044&deploy=1&proj_id='+nlapiGetRecordId()+'>Project Setup</a>';
					
					strVar += '<p>Thanks & Regards,</p>';
					strVar += '<p>Team IS</p>';
						
					strVar += '</body>';
					strVar += '</html>';
					
					var bcc = [];
					bcc[0] = 'deepak.srinivas@brillio.com'
						
					var a_emp_attachment = new Array();
					a_emp_attachment['entity'] = employee;
					//nlapiSendEmail(author, recipient, subject, body, cc, bcc, records, attachments)
					nlapiLogExecution('DEBUG','emp_email',emp_email);
					nlapiLogExecution('DEBUG','emp_user',employee);
					nlapiSendEmail(442, bcc , 'On The Go - TimeSheet Approval', strVar,null,'deepak.srinivas@brillio.com',a_emp_attachment);
					//nlapiSendEmail(442, emp_email , 'Brillio On The Go - Expense Entry', strVar,null,'deepak.srinivas@brillio.com',a_emp_attachment);
					nlapiLogExecution('audit','mail sent');
					
					/*var lineCount = loadRec.getLineItemCount('item');
					for(var j=1;j<=lineCount;j++){
						
						var practiceVal = loadRec.getLineItemValue('item','department',j);
						if(parseInt(practiceVal) == parseInt(460) || parseInt(practiceVal) == parseInt(458) || parseInt(practiceVal) == parseInt(326) ||
								parseInt(practiceVal) == parseInt(324) || parseInt(practiceVal) == parseInt(479) || parseInt(practiceVal) == parseInt(323)	|| parseInt(practiceVal) == parseInt(437) 
								|| parseInt(practiceVal) == parseInt(457)){
							loadRec.setLineItemValue('item','department',j,497);
						}
					}
					
					var lineCount_time = loadRec.getLineItemCount('expense');
					for(var k=1;k<=lineCount_time;k++){
						//var applyfld = loadRec.getLineItemValue('time','apply',k);
						//if(applyfld == 'T'){
						var practiceVal = loadRec.getLineItemValue('expense','department',k);
						if(parseInt(practiceVal) == parseInt(460) || parseInt(practiceVal) == parseInt(458) || parseInt(practiceVal) == parseInt(326) ||
								parseInt(practiceVal) == parseInt(324) || parseInt(practiceVal) == parseInt(479) || parseInt(practiceVal) == parseInt(323)	|| parseInt(practiceVal) == parseInt(437) 
								|| parseInt(practiceVal) == parseInt(457)){
							loadRec.setLineItemValue('expense','department',k,497);
						}
						//}
					}*/
					//update the fields
				//	var finalComments = closingComments + '\n' +comments;
					//loadRec.setFieldValue('comments',finalComments);
					//loadRec.setFieldValue('status',1);
					
					yieldScript(context);
				
				}
			}
		}
		/*var appro = ['5764','69145'];
		var loadInv = nlapiLoadRecord('vendorbill',parseInt(470688));
		loadInv.setFieldValue('custbody_financemanager',appro);
		var id = nlapiSubmitRecord(loadInv);
		var loadInv = nlapiLoadRecord('invoice',parseInt(inv_id))
		var billingFrom = loadInv.getFieldValue('custbody_billfrom');
		var billingTo = loadInv.getFieldValue('custbody_billto');
		billingFrom = nlapiStringToDate(billingFrom);
		billingTo = nlapiStringToDate(billingTo);
		
		var getLineCount = loadInv.getLineItemCount('time');
		for(var i=1;i<=getLineCount;i++){
			
			yieldScript(context);
			nlapiLogExecution('DEBUG','Processed Line :---',i);
			var applyfld = loadInv.getLineItemValue('time','apply',i);
			var billeddate = loadInv.getLineItemValue('time','billeddate',i);
			billeddate = nlapiStringToDate(billeddate);
			var rate = loadInv.getLineItemValue('time','rate',i);
			//var applyfld = loadInv.getLineItemValue('time','apply',i);
			
			if(billeddate >= billingFrom && billeddate <= billingTo){
				loadInv.setLineItemValue('time','apply',i,'T');
				loadInv.setLineItemValue('time','rate',i,0);
				//loadInv.setLineItemValue('time','apply',i,'T');
			}
		}*/
		//yieldScript(context);
		//var id = nlapiSubmitRecord(loadInv);
	}
	catch (err) {
		nlapiLogExecution('error', 'getZeroAllocationEmployee', err);
		throw err;
	}
}

function yieldScript(currentContext) {

	if (currentContext.getRemainingUsage() <= 2000) {
		nlapiLogExecution('AUDIT', 'API Limit Exceeded');
		var state = nlapiYieldScript();

		if (state.status == "FAILURE") {
			nlapiLogExecution('ERROR', 'Yield Script Failed', 'Reason : '
			        + state.reason + ' / Size : ' + state.size);
			return false;
		} else if (state.status == "RESUME") {
			nlapiLogExecution('AUDIT', 'Script Resumed');
		}
	}
}