function trigger_email()
{
try{
var cols = [];
cols.push(new nlobjSearchColumn('firstname'));
cols.push(new nlobjSearchColumn('email'));
cols.push(new nlobjSearchColumn('custentity_personalemailid'));
cols.push(new nlobjSearchColumn('internalid'));
cols.push(new nlobjSearchColumn('timeapprover'));

var search_res = nlapiSearchRecord('employee','customsearch_payroll_timesheet_reminder',null,cols);
                 nlapiLogExecution('DEBUG','Length',search_res.length);
if(search_res){
for(var i=0;i<search_res.length;i++){

var timeapprover = nlapiLookupField('employee',search_res[i].getValue('timeapprover'),['email']);
var timeapprover_email = timeapprover.email;
var cc_array = [];
cc_array[0] = timeapprover_email;
cc_array[1] = 'timesheet@brillio.com';


var htmltext = '<table border="0" width="100%">';
	htmltext += '<tr>';
	htmltext += '<td colspan="4" valign="top">';
	htmltext += '<p>Hi ' + search_res[i].getValue('firstname') + ',</p>';
	htmltext += '<p></p>';

	htmltext += '<p>We are missing your hours either of the following weekending 10/7/2017, 10/14/2017, 10/21/2017 or 10/28/2017 in NetSuite.</p>';
	
	htmltext += '<p>Please update your hours in Netsuite before 3:00 PM EST Monday.</p>';

	htmltext += '<p><b>Note :</b>Any hours submitted after 3:00 PM EST Monday will be processed in Next payroll</p>';
	//htmltext += '<p>You have not submitted the timesheet for the week <b>'
	        //+ week_start_date + " - " + week_end_date + '</b>. </p>';

	htmltext += '<p><b>If you have already submitted your hours, you can ignore this emai.</b></p>';

	// htmltext +=
	// '<p>For any system related issues, please write to <a
	// href="mailto:information.systems@brillio.com">information.systems@brillio.com</a>.</p>';
	htmltext += '<p>Regards,<br/>';
	htmltext += 'Information Systems</p>';	

	htmltext += '</td></tr>';
	htmltext += '</table>';
	htmltext += '<hr width="100%" size="1" noshade color="#CCCCCC">';
	htmltext += '<table width="100%" border="0" cellspacing="0" cellpadding="0">';
	htmltext += '<tr>';
	htmltext += '<td align="right">';
	htmltext += '<font style="font-size:9px; font-family:Verdana,Arial,Helvetica,sans-serif; color:#999999;">Brillio is powered by <a href="http://www.netsuite.com/" style="color:#999999;">NetSuite</a> &#151; One System. No Limits.</font>';
	htmltext += '</td>';
	htmltext += '</tr>';
	htmltext += '</table>';
	htmltext += '</body>';
	htmltext += '</html>';


	  var  subject = 'VERY IMP : Payyroll Reminbder for Timesheet Submission.';
		
		//Subject : 'Weekly Timesheet Reminder : ' + week_start_date + " - "+ week_end_date,
	  


nlapiSendEmail(442,[search_res[i].getValue('email'),search_res[i].getValue('custentity_personalemailid')],subject, htmltext, cc_array, null, {
				        'entity' : search_res[i].getValue('internalid')
			        });
					
nlapiLogExecution('DEBUG','Mail Triggered',search_res[i].getValue('firstname'));										
}
}
}
catch(e){
nlapiLogExecution('DEBUG','Main',e);
}
}