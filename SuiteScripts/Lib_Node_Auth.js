function call_node(url,body,callmethod) {
    try {
        //Start of SuiteScript

        var accessURL = 'https://fuelnode1.azurewebsites.net/nsauth';

        var method = 'POST';
        //var tokenbody = new Object();
		var tokenbody = {
                "username": "nsuser@fuel.com",
                "password": "fuelPoc@1"
		};
		tokenbody = JSON.stringify(tokenbody);
        var tokenheaders = {
            "Content-Type": "application/json"
        };
		//tokenheaders = JSON.stringify(tokenheaders);
        var response = nlapiRequestURL(accessURL, tokenbody, tokenheaders, method);
        var data = JSON.parse(response.body);
        var access_token_node = data.nsJWTToken;
        nlapiLogExecution("DEBUG","access_token_node",access_token_node)
        var dynamic_URL = url;

        var headers = {
            "Authorization": "Bearer " +access_token_node,
            "Content-Type": "application/json",
            "accept": "application/json"
        };
        //
        var method_ = callmethod;//'POST';//callmethod;
        var dataRows = body;//{}
        nlapiLogExecution('DEBUG', 'dynamic_URL', dynamic_URL);
        nlapiLogExecution('DEBUG', 'dataRows', JSON.stringify(dataRows));
        nlapiLogExecution('DEBUG', 'headers', JSON.stringify(headers));
        nlapiLogExecution('DEBUG', 'method_',method_);
      	var response_ = nlapiRequestURL(dynamic_URL, dataRows, headers, method_);
        var data = JSON.parse(response_.body);
       nlapiLogExecution('DEBUG', 'data',data.message);
        return data;
    } catch (e) {
        nlapiLogExecution('DEBUG', 'SFDC Call error', e);

    }
}