/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       19 Aug 2015     nitish.mishra
 *
 */

/**
 * @param {Object}
 *            dataIn Parameter object
 * @returns {Object} Output object
 */
function getRESTlet(dataIn) {

	return {};
}

/**
 * @param {Object}
 *            dataIn Parameter object
 * @returns {Object} Output object
 */
function postRESTlet(dataIn) {

	var data = saveCustomerDiscountDetails(dataIn.CustomerDiscountId,
			dataIn.CustomerDiscountDetails);
	return {
		CustomerDiscountRecord : data
	};
}

function saveCustomerDiscountDetails(customerDiscountId,
		customerDiscountDetails) {
	try {

		var discountDetails = null;

		if (customerDiscountId) {
			discountDetails = nlapiLoadRecord(
					'customrecord_customer_discount_details',
					customerDiscountId, {
						recordmode : 'dynamic'
					});
		} else {
			discountDetails = nlapiCreateRecord(
					'customrecord_customer_discount_details', {
						recordmode : 'dynamic'
					});
		}

		discountDetails.setFieldValue('custrecord_cdd_customer',
				customerDiscountDetails.Customer);
		discountDetails.setFieldValue('custrecord_cdd_project',
				customerDiscountDetails.Project);
		discountDetails.setFieldValue('custrecord_cdd_employee',
				customerDiscountDetails.Employee);

		discountDetails.setFieldValue('custrecord_cdd_po_number',
				customerDiscountDetails.PurchaseOrderNumber);

		discountDetails.setFieldValue('custrecord_cdd_st_hours',
				customerDiscountDetails.ST_Hours);
		discountDetails.setFieldValue('custrecord_cdd_st_amount',
				customerDiscountDetails.ST_Rate);
		discountDetails.setFieldValue('custrecord_cdd_ot_hours',
				customerDiscountDetails.OT_Hours);
		discountDetails.setFieldValue('custrecord_cdd_ot_amount',
				customerDiscountDetails.OT_Rate);
		discountDetails.setFieldValue('custrecord_cdd_total_hours',
				customerDiscountDetails.TotalHours);
		discountDetails.setFieldValue('custrecord_cdd_gross_amount',
				customerDiscountDetails.TotalAmount);

		discountDetails.setFieldValue('custrecord_cdd_fixed_fee_percent',
				customerDiscountDetails.FixedFeeDiscountPercent);
		discountDetails.setFieldValue('custrecord_cdd_fixed_fee_discount',
				customerDiscountDetails.FixedFeeDiscount);

		discountDetails.setFieldValue('custrecord_cdd_tenure_disc_percent',
				customerDiscountDetails.TenureDiscountPercentA);
		discountDetails.setFieldValue('custrecord_cdd_tenure_disc_percent_b',
				customerDiscountDetails.TenureDiscountPercentB);
		discountDetails.setFieldValue('custrecord_cdd_tenure_disc_amt_a',
				customerDiscountDetails.TenureDiscountAmountA);
		discountDetails.setFieldValue('custrecord_cdd_tenure_disc_amt_b',
				customerDiscountDetails.TenureDiscountAmountB);
		discountDetails.setFieldValue('custrecord_cdd_tenure_discount',
				customerDiscountDetails.TenureDiscount);

		discountDetails.setFieldValue('custrecord_cdd_volume_discount_percent',
				customerDiscountDetails.VolumeDiscountPercentA);
		discountDetails.setFieldValue(
				'custrecord_cdd_volume_discount_percent_b',
				customerDiscountDetails.VolumeDiscountPercentB);
		discountDetails.setFieldValue('custrecord_cdd_volume_discount_amt_a',
				customerDiscountDetails.VolumeDiscountAmountA);
		discountDetails.setFieldValue('custrecord_cdd_volume_discount_amt_b',
				customerDiscountDetails.VolumeDiscountAmountB);
		discountDetails.setFieldValue('custrecord_cdd_volume_discount',
				customerDiscountDetails.VolumeDiscount);

		discountDetails.setFieldValue('custrecord_cdd_old_cumulative_hours',
				customerDiscountDetails.CumulativeHoursOld);
		discountDetails.setFieldValue('custrecord_cdd_new_cumulative_hours',
				customerDiscountDetails.CumulativeHoursNew);
		discountDetails.setFieldValue('custrecord_cdd_old_cumulative_sales',
				customerDiscountDetails.CumulativeSalesOld);
		discountDetails.setFieldValue('custrecord_cdd_new_cumulative_sales',
				customerDiscountDetails.CumulativeSalesNew);

		discountDetails.setFieldValue('custrecord_cdd_net_discount',
				customerDiscountDetails.TotalDiscount);
		discountDetails.setFieldValue('custrecord_cdd_final_invoice_amount',
				customerDiscountDetails.NetInvoiceAmount);

		var id = nlapiSubmitRecord(discountDetails);
		nlapiLogExecution('debug',
				'customer discount record created / updated', id);

		return id;
	} catch (err) {
		nlapiLogExecution('ERROR', 'saveCustomerDiscountDetails', err);
		throw err;
	}
}
