// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
/*
   	Script Name : SUT_getSkillFamily.js
	Author      : ASHISH PANDIT
	Date        : 04 JAN 2018
    Description : Suitelet to get skill family according to Practice selected 


	Script Modification Log:

	-- Date --			-- Modified By --				--Requested By--				-- Description --
 


Below is a summary of the process controls enforced by this script file.  The control logic is described
more fully, below, in the appropriate function headers and code blocks.


     SCHEDULED FUNCTION
		- scheduledFunction(type)


     SUB-FUNCTIONS
		- The following sub-functions are called by the above core functions in order to maintain code
            modularization:

               - NOT USED

*/
}
// END SCRIPT DESCRIPTION BLOCK  ====================================


function getSkillFamily(request, response){
	try
	{
		var i_emp_dep = request.getParameter('i_practice');   
		nlapiLogExecution('DEBUG','i_emp_dep',i_emp_dep);
		var arrSkillFamily = [];
		var searchResult = getTaggedFamilies(i_emp_dep);
		if(searchResult){
			for (var int = 0; int < searchResult.length; int++) {
                var dataOut = {};
				dataOut.id = searchResult[int].getId();
				dataOut.name = searchResult[int].getValue('name');
				arrSkillFamily.push(dataOut);
			}
		}
		response.write(JSON.stringify(arrSkillFamily));
	}
	catch(error)
	{
		nlapiLogExecution('Debug','error ',error);
	}
}

function getTaggedFamilies(i_emp_dep)
{
try{
  var parent_practice = nlapiLookupField('department',i_emp_dep,'custrecord_parent_practice');
var practice_filter = [
		        
		           [ [ 'custrecord_skill_emp_dep', 'anyOf', i_emp_dep ],'or',[ 'custrecord_skill_emp_dep', 'anyOf', parent_practice]],
		                'and',
		                [ 'isinactive',
		                        'anyOf', 'F' ]
		         ];

		var practice_search_results = searchRecord('customrecord_employee_skill_family_list', null, practice_filter,
		        [ new nlobjSearchColumn("name"),
		               	new nlobjSearchColumn("internalid")]);


	return practice_search_results;
}catch(err){
	throw err;
	}
}

function searchRecord(recordType, savedSearch, arrFilters, arrColumns,
        filterExpression)
{

	try {
		var search = null;

		// if a saved search is provided, load it and add the filters and
		// columns
		if (isNotEmpty(savedSearch)) {
			search = nlapiLoadSearch(recordType, savedSearch);

			if (isArrayNotEmpty(arrFilters)) {
				search.addFilters(arrFilters);
			}

			if (isArrayNotEmpty(arrColumns)) {
				search.addColumns(arrColumns);
			}

			if (isArrayNotEmpty(filterExpression)) {
				search.setFilterExpression(filterExpression);
			}
		}
		// create a new search
		else {
			search = nlapiCreateSearch(recordType, arrFilters, arrColumns);
		}

		// run search
		var resultSet = search.runSearch();

		// iterate through the search and get all data 1000 at a time
		var searchResultCount = 0;
		var resultSlice = null;
		var searchResult = [];

		do {
			resultSlice = resultSet.getResults(searchResultCount,
			        searchResultCount + 1000);

			if (resultSlice) {

				resultSlice.forEach(function(result) {

					searchResult.push(result);
					searchResultCount++;
				});
			}
		} while (isArrayNotEmpty(resultSlice) && resultSlice.length >= 1000);

		return searchResult;
	} catch (err) {
		nlapiLogExecution('ERROR', 'searchRecord', err);
		throw err;
	}
}

function isEmpty(value) {

	return value == null || value == "" || typeof (value) == undefined;
}

function isNotEmpty(value) {

	return !isEmpty(value);
}

function isArrayEmpty(argArray) {

	return !isArrayNotEmpty(argArray);
}

function isArrayNotEmpty(argArray) {

	return (isNotEmpty(argArray) && argArray.length > 0);
}