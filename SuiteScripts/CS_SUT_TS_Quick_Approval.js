/**
 * Module Description
 * 
 * Version Date Author Remarks 1.00 31 Aug 2015 nitish.mishra
 * 
 */

function clientFieldChanged(type, name, linenum) {

}

// button click - approve
function approveTimeSheet() {
	var timesheets = getAppliedTimesheet();

	if (timesheets.length > 0) {
		makeSuiteletCall('approve', timesheets);
	} else {
		alert('No Timesheets Selected');
	}
}

// button click - reject
function rejectTimeSheet() {
	var timesheets = getAppliedTimesheet();

	if (timesheets.length > 0) {
		makeSuiteletCall('reject', timesheets);
	} else {
		alert('No Timesheets Selected');
	}
}

function getAppliedTimesheet() {

	var records = [];
	var timesheetCount = nlapiGetLineItemCount('custpage_timesheets');

	for (var linenum = 1; linenum <= timesheetCount; linenum++) {

		if (nlapiGetLineItemValue('custpage_timesheets', 'apply', linenum) == 'T') {
			records.push(nlapiGetLineItemValue('custpage_timesheets',
			        'internalid', linenum));
		}
	}

	return records;
}

function makeSuiteletCall(requestType, record) {
	try {
		var dataReceived = nlapiRequestURL(
		        "/app/site/hosting/scriptlet.nl?script=643&deploy=1", {
		            requesttype : requestType,
		            records : record
		        });

		dataReceived = dataReceived.getBody();
		dataReceived = JSON.parse(dataReceived);

		if (dataReceived.Status) {
			alert(dataReceived.Message);
			window.location.href = window.location;
		} else {
			alert(dataReceived.Message);
		}
	} catch (err) {
		alert(err.message);
	}
}