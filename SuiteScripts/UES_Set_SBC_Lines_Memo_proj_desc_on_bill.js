//https://system.na1.netsuite.com/app/common/scripting/script.nl?id=948
// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
	/*
	 Script Name: UES_Set_SBC_Lines_Memo_on_bill.js
	 Author: Jayesh V Dinde
	 Company: Aashna CLoudtech PVT LTD.
	 Date: 23 June 2016
	 Description: This script will update SBC lines MEMO field as per brillio standard memo on vendor bill.
	 These are populated through AIT script. Need for this script was, we can't edit AIT script hence new script created.


	 Script Modification Log:

	 -- Date --                                              -- Modified By --                                                               --Requested By--                                                             -- Description --



	 Below is a summary of the process controls enforced by this script file.  The control logic is described
	 more fully, below, in the appropriate function headers and code blocks.


	 BEFORE LOAD
	 - beforeLoadRecord(type)



	 BEFORE SUBMIT
	 - beforeSubmitRecord(type)


	 AFTER SUBMIT
	 - afterSubmit_UpdateMemo(type)



	 SUB-FUNCTIONS
	 - The following sub-functions are called by the above core functions in order to maintain code
	 modularization:

	 - NOT USED

	 */
}
// END SCRIPT DESCRIPTION BLOCK  ====================================

// BEGIN GLOBAL VARIABLE BLOCK  =====================================
{
	//  Initialize any Global Variables, in particular, debugging variables...

}
// END GLOBAL VARIABLE BLOCK  =======================================

// BEGIN BEFORE LOAD ==================================================

function beforeLoadRecord(type) {

	/*  On before load:

	- EXPLAIN THE PURPOSE OF THIS FUNCTION

					-


					FIELDS USED:

	--Field Name--                                                        --ID--


	 */

	//  LOCAL VARIABLES
	//  BEFORE LOAD CODE BODY
	try {

		var i_vendor = nlapiGetFieldValue('entity');



		if (_logValidation(i_vendor)) {
			var s_billaddress = nlapiGetFieldValue('billaddress'); //billzip
			var i_billzip = nlapiGetFieldValue('billzip');
			var vendorSearch = nlapiSearchRecord("vendor", null,
				[
					["internalid", "anyof", i_vendor]
				],
				[
					new nlobjSearchColumn("entityid").setSort(false),
					new nlobjSearchColumn("custrecord_iit_address_gstn_uid", "Address", null),
					new nlobjSearchColumn("zipcode", "Address", null)
				]
			);
			if (vendorSearch) {
				for (var i = 0; i < vendorSearch.length; i++) {
					var line_zipCode = vendorSearch[i].getValue("zipcode", "Address");
					if (line_zipCode == i_billzip) {
						var gstin_num = vendorSearch[i].getValue("custrecord_iit_address_gstn_uid", "Address");
						nlapiSetFieldValue('custbody_vendor_gstin', gstin_num);
					}
				}
			}
		}

		return true;


	}
	catch (e) {
		nlapiLogExecution('debug', 'Process Error - Before Load', e);
		throw e;
	}
}

// END BEFORE LOAD ====================================================

// BEGIN BEFORE SUBMIT ================================================

function beforeSubmitRecord(type) {
	/*  On before submit:

		  - PURPOSE
								-

		  FIELDS USED:

		  --Field Name--                                                        --ID--

	 */

	//  LOCAL VARIABLES
	//  BEFORE SUBMIT CODE BODY
	try {
		var o_context = nlapiGetContext();
		if (o_context.getExecutionContext() == 'scheduled') {
			return true;
		}

		if (type == 'create' || type == 'edit') {

			var is_subtier_bill = nlapiGetFieldValue('custbody_is_subtier_bill');
			var i_currency = nlapiGetFieldValue('currency');

			//nlapiLogExecution('debug','is_subtier_bill:-- '+is_subtier_bill,nlapiGetRecordId());
			//nlapiLogExecution('debug','is_subtier_bill:-- '+i_currency,i_currency);
			//nlapiLogExecution('debug','is_subtier_bill:-- '+is_subtier_bill,is_subtier_bill);

			{
				var a_lines_commited = new Array();
				var item_line_count = nlapiGetLineItemCount('item');
				var expense_line_count = nlapiGetLineItemCount('expense');
				var i_vendor = nlapiGetFieldValue('entity');
				var b_vendorType = nlapiLookupField('vendor', i_vendor, 'custentity_iit_gst_liable');
				//nlapiLogExecution('DEBUG','b_vendorType=',b_vendorType);
				for (var j = 1; j <= expense_line_count; j++) {
					var total_updated_line = 0;

					var is_swach_cess_reverse_true_mainLine = nlapiGetLineItemValue(
						'expense', 'custcol_ait_swachh_cess_reverse', j);
					for (var q = j; q <= expense_line_count; q++) {
						var is_swach_cess_reverse_true = nlapiGetLineItemValue(
							'expense', 'custcol_st_reverseline', q);
						//nlapiLogExecution('Audit', 'swachh_reverse_line', is_swach_cess_reverse_true);                                        
						if (is_swach_cess_reverse_true == 'T') {
							var tax_amount_original_line = nlapiGetLineItemValue(
								'expense', 'tax1amt', j);
							//nlapiLogExecution('Audit','Tax Amount--->', tax_amount_original_line);

							var i_tax_code_selected = nlapiGetLineItemValue(
								'expense', 'taxcode_display', j);
							//i_tax_code_selected = i_tax_code_selected.trim();
							//nlapiLogExecution('Debug','Tax group selected--->', i_tax_code_selected);

							var i_item_amount = nlapiGetLineItemValue('expense',
								'amount', j);

							var i_item_tax_rate = nlapiGetLineItemValue('expense',
								'taxrate1', j);

							var i_tax_percent = (parseFloat(i_item_amount)
								* parseFloat(i_item_tax_rate) / 100);

							nlapiLogExecution('Debug', 'i_tax_percent--->',
								i_tax_percent);

							var amount = 0;
							if (i_tax_code_selected == 'GST:GST @18%'
								|| i_tax_code_selected == 'GST:GST @5%' || i_tax_code_selected == 'GST:GST @28%' || i_tax_code_selected == 'GST:GST @12%' || i_tax_code_selected == 'GST:GST @0%') {
								amount = (parseFloat(i_tax_percent)) / 2;
								amount = parseInt(amount);
								nlapiLogExecution('debug', 'amount --->',
									amount);
							} else if (i_tax_code_selected == 'GST:IGST @18%' || i_tax_code_selected == 'GST:IGST @5%' || i_tax_code_selected == 'GST:IGST @28%' || i_tax_code_selected == 'GST:IGST @12%' || i_tax_code_selected == 'GST:IGST @0%') {
								amount = (parseFloat(i_tax_percent));
								amount = parseInt(amount);
								nlapiLogExecution('debug', 'amount --->',
									amount);
							}

							tax_amount_original_line = parseInt(tax_amount_original_line);
							amount = '-' + amount;
							var emp_type = '';
							var person_type = '';
							var onsite_offsite = '';
							var proj_billing_type = '';
							var i_emp_full_name = '';

							/*var taxAmount = parseFloat(amount) * (parseFloat(0.5) / 100);
							var rounded_amnt = Math.round(taxAmount);
							taxAmount = taxAmount.toString();
							var taxAmount_toCompare = taxAmount.split('.');
							taxAmount_toCompare = taxAmount_toCompare[0];*/
							//taxAmount = applySTRoundMethod(stRoundMethod, taxAmount);
							var amount_sbc = nlapiGetLineItemValue('expense',
								'amount', q);
							amount_sbc = parseInt(amount_sbc);
							var negative_amount_sbc = '-' + amount_sbc;
							/*amount_sbc = amount_sbc.toString();
							var amount_sbc_toCompare = amount_sbc.split('.');
							amount_sbc_toCompare = amount_sbc_toCompare[0];*/

							nlapiLogExecution('debug', 'amount:- ' + amount
								+ ':::amount_sbc :- ' + amount_sbc);
							if (parseFloat(amount) == parseFloat(amount_sbc)
								|| parseFloat(tax_amount_original_line) == parseFloat(amount_sbc)) {
								var emp_type = '';
								var person_type = '';
								var onsite_offsite = '';
								var proj_billing_type = '';
								var i_emp_full_name = '';
								var proj_id_V = '';

								var cust_id = nlapiGetLineItemValue('expense',
									'custcol_customer_entityid', j);
								
								var proj_id = nlapiGetLineItemValue('expense',
									'custcol_project_entity_id', j);
								nlapiLogExecution('debug', 'prj_id--->', proj_id);
								var emp_id = nlapiGetLineItemValue('expense',
									'custcol_employee_entity_id', j);
								
								var proj_desc = nlapiGetLineItemValue(
									'expense', 'custcolprj_name', j);
								if (proj_desc) {
									var pro_entity_id = proj_desc.split(' ');
									proj_id_V = pro_entity_id[0];
								}

								else if (proj_id) {
									proj_id_V = proj_id;
								}
								if (!_logValidation(emp_id)) {
									emp_id = '';
								}

								if (a_lines_commited.indexOf(q) < 0) {
									nlapiLogExecution('debug', 'line no:- ' + q);
									nlapiSetLineItemValue('expense',
										'custcol_customer_entityid', q,
										cust_id);
									nlapiSetLineItemValue('expense',
										'custcol_project_entity_id', q,
										proj_id_V);
									nlapiSetLineItemValue('expense',
										'custcol_employee_entity_id', q,
										emp_id);
									nlapiSetLineItemValue('expense',
										'custcolprj_name', q, proj_desc);
									nlapiLogExecution('debug', 'customer id', cust_id);
									nlapiLogExecution('debug', 'proj id', proj_id_V);
									nlapiLogExecution('debug', 'emp id', emp_id);
									nlapiLogExecution('debug', 'proj des', proj_desc);
									a_lines_commited.push(q);
									total_updated_line++;
								}
							}

							if (i_tax_code_selected == 'GST:GST @18%' || i_tax_code_selected == 'GST:GST @5%' || i_tax_code_selected == 'GST:GST @12%' || i_tax_code_selected == 'GST:GST @28%' || i_tax_code_selected == 'GST:GST @0%') {
								if (b_vendorType == 'T') {
									if (total_updated_line == 3)
										break;
								}
								else {
									if (total_updated_line == 5)
										break;
								}

							}
							else if (i_tax_code_selected == 'GST:IGST @18%' || i_tax_code_selected == 'GST:IGST @28%' || i_tax_code_selected == 'GST:IGST @5%' || i_tax_code_selected == 'GST:IGST @12%' || i_tax_code_selected == 'GST:IGST @0%') {
								if (b_vendorType == 'T') {
									if (total_updated_line == 2)
										break;
								}
								else {
									if (total_updated_line == 3)
										break;
								}
							}


							/*else
							{
											var taxAmount = parseFloat(amount) * (parseFloat(0.5) / 100);
											var rounded_amnt = Math.round(taxAmount);
											taxAmount = taxAmount.toString();
											var taxAmount_toCompare = taxAmount.split('.');
											taxAmount_toCompare = taxAmount_toCompare[0];
							
											if(taxAmount_toCompare > parseFloat(0))
															taxAmount_toCompare = '-'+taxAmount_toCompare;
											if(rounded_amnt > parseFloat(0))           
											rounded_amnt = '-'+rounded_amnt;
										    
											if(parseFloat(amount_sbc_toCompare) == parseFloat(taxAmount_toCompare) || parseFloat(amount_sbc_toCompare) == parseFloat(rounded_amnt))
											{
															var emp_type = '';
											var person_type = '';
											var onsite_offsite = '';
											var proj_billing_type = '';
											var i_emp_full_name ='';
											var proj_id_V = ''; 
										    
											var cust_id = nlapiGetLineItemValue('expense', 'custcol_customer_entityid', j);
											var proj_id = nlapiGetLineItemValue('expense', 'custcol_project_entity_id', j);
											var emp_id = nlapiGetLineItemValue('expense', 'custcol_employee_entity_id', j);
											var proj_desc = nlapiGetLineItemValue('expense', 'custcolprj_name', j);
											if(proj_desc){
											var pro_entity_id = proj_desc.split(' ');
											proj_id_V = pro_entity_id[0];
											}
											else if(proj_id){
											proj_id_V = proj_id;
											}
											if (!_logValidation(emp_id))
																							{
																											emp_id = '';
																							}
										    
											if (a_lines_commited.indexOf(q) < 0)
											{
															//nlapiLogExecution('audit','line no:- '+q);
															nlapiSetLineItemValue('expense', 'custcol_customer_entityid', q, cust_id);
															nlapiSetLineItemValue('expense', 'custcol_project_entity_id', q, proj_id_V);
															nlapiSetLineItemValue('expense', 'custcol_employee_entity_id', q, emp_id);
															nlapiSetLineItemValue('expense', 'custcolprj_name', q, proj_desc);
														    
															a_lines_commited.push(q);
															break;
											}
							
											}
											taxAmount_toCompare = Math.abs(taxAmount_toCompare);
							}*/
						}
					}

					/*var tax_amount_at_line = nlapiGetLineItemValue('expense', 'tax1amt', j);
					//nlapiLogExecution('audit','tax_amount_at_line:- ',tax_amount_at_line);
					if (tax_amount_at_line)
					{
									var tax_decimal = tax_amount_at_line.toString();
									tax_decimal = tax_decimal.split('.');
									//tax_decimal = tax_amount_at_line.split(',');
									//nlapiLogExecution('audit','tax amnt:- ',tax_decimal[1]);
									if (parseFloat(tax_decimal[1]) >= parseFloat(50)) {
													tax_amount_at_line = Math.round(tax_amount_at_line);
													//nlapiLogExecution('audit','tax_amount_at_line after rounding of',tax_amount_at_line);
												    
													nlapiSetLineItemValue('expense', 'tax1amt', j, tax_amount_at_line);
									}
									else {
													tax_amount_at_line = Math.round(tax_amount_at_line);
												    
													nlapiSetLineItemValue('expense', 'tax1amt', j, tax_amount_at_line);
												    
									}
					}
					var i_subsidiary = nlapiGetFieldValue('subsidiary');
					if (parseInt(i_subsidiary) == 3 && is_swach_cess_reverse_true_mainLine != 'T' && parseInt(i_currency) != parseInt(1))
					{
									var amount_round_off = nlapiGetLineItemValue('expense', 'grossamt', j);
									amount_round_off = Math.round(amount_round_off);
									//nlapiLogExecution('audit', 'main amount:- ', amount_round_off);
									nlapiSetLineItemValue('expense', 'grossamt', j, amount_round_off);
					}*/

				}

				// code to traverse item sublist
				for (var j = 1; j <= item_line_count; j++) {
					var total_updated_line = 0;
					for (var q = j; q <= expense_line_count; q++) {
						var is_swach_cess_reverse_true = nlapiGetLineItemValue(
							'expense', 'custcol_st_reverseline', q);

						//nlapiLogExecution('Audit', 'swachh_reverse_line', is_swach_cess_reverse_true);                                        
						if (is_swach_cess_reverse_true == 'T') {
							var tax_amount_original_line = nlapiGetLineItemValue(
								'item', 'tax1amt', j);
							//nlapiLogExecution('Audit','Tax Amount--->', tax_amount_original_line);

							var i_tax_code_selected = nlapiGetLineItemValue(
								'item', 'taxcode_display', j);
							//i_tax_code_selected = i_tax_code_selected.trim();
							//nlapiLogExecution('Audit','Tax group selected--->', i_tax_code_selected);

							var i_item_amount = nlapiGetLineItemValue('item',
								'amount', j);

							var i_item_tax_rate = nlapiGetLineItemValue('item',
								'taxrate1', j);

							var i_tax_percent = (parseFloat(i_item_amount)
								* parseFloat(i_item_tax_rate) / 100);

							nlapiLogExecution('Audit', 'i_tax_percent--->',
								i_tax_percent);

							var amount = 0;
							if (i_tax_code_selected == 'GST:GST @18%'
								|| i_tax_code_selected == 'GST:GST @5%') {
								amount = (parseFloat(i_tax_percent)) / 2;
								amount = parseInt(amount);
								nlapiLogExecution('Audit', 'amount --->',
									amount);
							}

							tax_amount_original_line = parseInt(tax_amount_original_line);
							amount = '-' + amount;
							var emp_type = '';
							var person_type = '';
							var onsite_offsite = '';
							var proj_billing_type = '';
							var i_emp_full_name = '';

							/*var taxAmount = parseFloat(amount) * (parseFloat(0.5) / 100);
							var rounded_amnt = Math.round(taxAmount);
							taxAmount = taxAmount.toString();
							var taxAmount_toCompare = taxAmount.split('.');
							taxAmount_toCompare = taxAmount_toCompare[0];*/
							//taxAmount = applySTRoundMethod(stRoundMethod, taxAmount);
							var amount_sbc = nlapiGetLineItemValue('expense',
								'amount', q);
							amount_sbc = parseInt(amount_sbc);
							var negative_amount_sbc = '-' + amount_sbc;
							/*amount_sbc = amount_sbc.toString();
							var amount_sbc_toCompare = amount_sbc.split('.');
							amount_sbc_toCompare = amount_sbc_toCompare[0];*/

							nlapiLogExecution('audit', 'amount:- ' + amount
								+ ':::amount_sbc :- ' + amount_sbc);
							if (parseFloat(amount) == parseFloat(amount_sbc)
								|| parseFloat(tax_amount_original_line) == parseFloat(amount_sbc)) {
								var emp_type = '';
								var person_type = '';
								var onsite_offsite = '';
								var proj_billing_type = '';
								var i_emp_full_name = '';
								var proj_id_V = '';

								var cust_id = nlapiGetLineItemValue('item',
									'custcol_customer_entityid', j);
								//nlapiLogExecution('Audit','Cust_id--->', cust_id);
								var proj_id = nlapiGetLineItemValue('item',
									'custcol_project_entity_id', j);
								//nlapiLogExecution('Audit','prj_id--->', proj_id);
								var emp_id = nlapiGetLineItemValue('item',
									'custcol_employee_entity_id', j);
								//nlapiLogExecution('Audit','Expense--->', emp_id);
								var proj_desc = nlapiGetLineItemValue('item',
									'custcolprj_name', j);
								if (proj_desc) {
									var pro_entity_id = proj_desc.split(' ');
									proj_id_V = pro_entity_id[0];
								}

								else if (proj_id) {
									proj_id_V = proj_id;
								}
								if (!_logValidation(emp_id)) {
									emp_id = '';
								}

								if (a_lines_commited.indexOf(q) < 0) {
									//nlapiLogExecution('audit','line no:- '+q);
									nlapiSetLineItemValue('expense',
										'custcol_customer_entityid', q,
										cust_id);
									nlapiSetLineItemValue('expense',
										'custcol_project_entity_id', q,
										proj_id_V);
									nlapiSetLineItemValue('expense',
										'custcol_employee_entity_id', q,
										emp_id);
									nlapiSetLineItemValue('expense',
										'custcolprj_name', q, proj_desc);
									//            nlapiLogExecution('Audit', 'customer id',cust_id);
									//            nlapiLogExecution('Audit', 'proj id',proj_id_V);
									//            nlapiLogExecution('Audit', 'emp id',emp_id);
									//            nlapiLogExecution('Audit', 'proj des',proj_desc);
									a_lines_commited.push(q);
									total_updated_line++;
								}
							}

							if (total_updated_line == 7)
								break;

						}
					}
				}
			}
		}
	} catch (e) {
		nlapiLogExecution('ERROR', 'ERROR MESSAGE:-- ', e);
	}

	return true;
}

// END BEFORE SUBMIT ==================================================

// BEGIN AFTER SUBMIT =============================================

function afterSubmit_UpdateProjDesc(type) {
	/*  On after submit:

		  - PURPOSE
								-

				FIELDS USED:

		  --Field Name--                                                        --ID--

	 */

	//  LOCAL VARIABLES
	var submit_record_flag = 0;

	//  AFTER SUBMIT CODE BODY

	try {
		var o_context = nlapiGetContext();
		if (o_context.getExecutionContext() == 'scheduled') {
			return true;
		}
        var filters_customrecord_revenue_location_subsidiarySearch=["custrecord_offsite_onsite","anyof","2"];
        var obj_rev_loc_subSrch= Search_revenue_Location_subsdidary(filters_customrecord_revenue_location_subsidiarySearch);
        var obj_offsite_onsite=obj_rev_loc_subSrch["Onsite/Offsite"];

		if (type == 'create' || type == 'edit') {

			var vendor_bill_rcrd = nlapiLoadRecord(nlapiGetRecordType(),
				nlapiGetRecordId());
			var a_lines_commited = new Array();
			var gst_not_credit_flag = false;
			var item_line_count = vendor_bill_rcrd.getLineItemCount('item');
			var i_vendor = vendor_bill_rcrd.getFieldValue('entity');
			var expense_line_count = vendor_bill_rcrd
				.getLineItemCount('expense');
			var i_subsidiary = vendor_bill_rcrd.getFieldValue('subsidiary');
			var i_currency = vendor_bill_rcrd.getFieldValue('currency');

			//Logic for expense out
			/*for (var jb = 1; jb <= expense_line_count; jb++)
			{
			//            vendor_bill_rcrd.getLineItemValue('expense', 'custcol_st_notcredit', jb); //custcol_st_notcredit
			var st_notcredit = vendor_bill_rcrd.getLineItemValue('expense', 'custcol_st_notcredit', jb); //custcol_st_notcredit
			if(st_notcredit == 'T')
			gst_not_credit_flag = true;
			nlapiLogExecution('audit', 'gst_not_credit_flag:- ', gst_not_credit_flag);
			}*/

			for (var j = 1; j <= expense_line_count; j++) {

				var taxtotal_exp = vendor_bill_rcrd.getLineItemValue('expense',
					'tax1amt', j);
				var i_account = vendor_bill_rcrd.getLineItemValue('expense',
					'account', j); //custcol_st_notcredit
				var amount_round_off = vendor_bill_rcrd.getLineItemValue(
					'expense', 'grossamt', j);
			//	if ((parseInt(i_subsidiary) == parseInt(3) || parseInt(i_subsidiary) == parseInt(9))

					if((obj_offsite_onsite[i_subsidiary]) && parseInt(i_currency) == parseInt(6)) {
					var tds_line = vendor_bill_rcrd.getLineItemValue('expense',
						'custcol_tdsline', j);
					var apply_tds = vendor_bill_rcrd.getLineItemValue('expense',
						'custcol_tdsapply', j);
					if (apply_tds == 'T') {

						var actl_tds_amt = vendor_bill_rcrd.getLineItemValue(
							'expense', 'custcol_tdsamount', j);
						var actl_tds_amount_round_off_tds = Math.round(actl_tds_amt);
						//nlapiLogExecution('audit', 'main amount rounded:- ', amount_round_off);
						vendor_bill_rcrd.setLineItemValue('expense',
							'custcol_tdsamount', j, actl_tds_amount_round_off_tds);
					}
					if (tds_line == 'T') {
						var tds_amt = vendor_bill_rcrd.getLineItemValue(
							'expense', 'grossamt', j);
						var amount_round_off_tds = Math.round(tds_amt);
						//nlapiLogExecution('audit', 'main amount rounded:- ', amount_round_off);
						vendor_bill_rcrd.setLineItemValue('expense',
							'grossamt', j, amount_round_off_tds);
					}
					//if(gst_not_credit_flag == true && (i_account == 1134 || i_account == 1135 || i_account ==  1136 )){
					//nlapiLogExecution('audit', 'gst_not_credit_flag amount_round_off:- ', amount_round_off);  custcol_tdsline
					//}
					//else{

					var amount_round_off_tax = Math.round(taxtotal_exp);
					/* if(submit_record_flag == 1)
					{
						var vendor_bill_submitted_id = nlapiSubmitRecord(
								vendor_bill_rcrd, true, true);
						nlapiLogExecution('debug', 'submitted bill id:-- ',
								vendor_bill_submitted_id);
					}*/
					//nlapiLogExecution('audit', 'main amount rounded:- ', amount_round_off);
					//vendor_bill_rcrd.setLineItemValue('expense', 'tax1amt', j,amount_round_off_tax);
					//            }
				}

			}
			var vendor_bill_submitted_id = nlapiSubmitRecord(
				vendor_bill_rcrd, true, true);
			nlapiLogExecution('debug', 'submitted bill id:-- ',
				vendor_bill_submitted_id);

		}

		if (type == 'create' || type == 'edit') {
			var vendor_bill_rcrd_ = nlapiLoadRecord(nlapiGetRecordType(),
				nlapiGetRecordId());
			var flag_diff = false;
			var a_lines_commited = new Array();
			var gst_not_credit_flag = false;
			var item_line_count = vendor_bill_rcrd_.getLineItemCount('item');
			var i_vendor = vendor_bill_rcrd_.getFieldValue('entity');
			var expense_line_count = vendor_bill_rcrd_
				.getLineItemCount('expense');
			var i_subsidiary = vendor_bill_rcrd_.getFieldValue('subsidiary');
			var i_currency = vendor_bill_rcrd_.getFieldValue('currency');
			var expense_line_count = vendor_bill_rcrd_
				.getLineItemCount('expense');

		//	if ((parseInt(i_subsidiary) == parseInt(3) || parseInt(i_subsidiary) == parseInt(9))
        if((obj_offsite_onsite[i_subsidiary])	&& parseInt(i_currency) == parseInt(6)) {
				var main_Amount = vendor_bill_rcrd_.getFieldValue('usertotal');
				
				var amount_round_off_MainAmt = Math.round(main_Amount);

				var diff = amount_round_off_MainAmt
					- parseFloat(main_Amount).toFixed(2);
				diff = diff.toFixed(2);
				
				if (expense_line_count >= 1) {
					if (diff < 1 && parseFloat(diff) != parseFloat(0)) { // &&
						// parseFloat(diff)
						// !=
						// parseFloat(0)
						/*
						 * if(diff <= .5){ var diff = Math.abs(diff); }
						 */
						var account_tofill = vendor_bill_rcrd_
							.getLineItemValue('expense', 'account', 1);
						vendor_bill_rcrd_.setLineItemValue('expense',
							'account', expense_line_count + 1,
							account_tofill);
						vendor_bill_rcrd_.setLineItemValue('expense',
							'grossamt', expense_line_count + 1, diff);
						vendor_bill_rcrd_.setLineItemValue('expense',
							'taxcode', expense_line_count + 1, 2602);
						vendor_bill_rcrd_.setLineItemValue('expense', 'memo',
							expense_line_count + 1,
							'Amount Round off value');
						var cust_id_tofill = vendor_bill_rcrd_
							.getLineItemValue('expense',
								'custcol_customer_entityid', 1);
						var proj_id_tofill = vendor_bill_rcrd_
							.getLineItemValue('expense',
								'custcol_project_entity_id', 1);
						vendor_bill_rcrd_.setLineItemValue('expense',
							'custcol_project_entity_id',
							expense_line_count + 1, proj_id_tofill);
						
						vendor_bill_rcrd_.setLineItemValue('expense',
							'custcol_customer_entityid',
							expense_line_count + 1, cust_id_tofill);
						
						flag_diff = true;

					}

				}
			}

			var item_gst_not_credit_flag = false;
			// Logic for expense out
			/*
			 * for (var jv = 1; jv <= item_line_count; jv++) { var st_notcredit =
			 * vendor_bill_rcrd.getLineItemValue('item', 'custcol_st_notcredit',
			 * jv); //custcol_st_notcredit if(st_notcredit == 'T')
			 * item_gst_not_credit_flag = true; //nlapiLogExecution('audit',
			 * 'item_gst_not_credit_flag:- ', item_gst_not_credit_flag); }
			 */
			for (var jk = 1; jk <= item_line_count; jk++) {
				// var i_subsidiary =
				// vendor_bill_rcrd.getFieldValue('subsidiary');
				var taxtotal_item = vendor_bill_rcrd_.getLineItemValue('item',
					'taxtotal', jk);
				var i_account_ = vendor_bill_rcrd_.getLineItemValue('item',
					'account', jk); // custcol_st_notcredit
				var amount_round_off = vendor_bill_rcrd_.getLineItemValue(
					'item', 'grossamt', jk);
			//	if ((parseInt(i_subsidiary) == parseInt(3) || parseInt(i_subsidiary) == parseInt(9))
            if((obj_offsite_onsite[i_subsidiary])	&& parseInt(i_currency) == parseInt(6)) {
					// if(item_gst_not_credit_flag == true && (i_account_ ==
					// 1134 || i_account_ == 1135 || i_account_ == 1136 )){
					// nlapiLogExecution('audit', 'gst_not_credit_flag
					// amount_round_off:- ', amount_round_off_);
					// }
					// else{
					var tds_line_item = vendor_bill_rcrd_.getLineItemValue(
						'item', 'custcol_tdsline', jk);

					var s_tdsapply = vendor_bill_rcrd.getLineItemValue('item',
						'custcol_tdsapply', jk);
					var s_tdsAmt_ = vendor_bill_rcrd.getLineItemValue('item',
						'custcol_tdsamount', jk);
					if (s_tdsapply == 'T') {
						var s_round_tdsAmt_ = Math.round(s_tdsAmt_);
						vendor_bill_rcrd.setLineItemValue('item',
							'custcol_tdsamount', jk, s_round_tdsAmt_);
					}
					if (tds_line_item == 'T') {
						var tds_amt = vendor_bill_rcrd_.getLineItemValue(
							'item', 'grossamt', jk);
						var amount_round_off_tds = Math.round(tds_amt);
						// nlapiLogExecution('audit', 'main amount rounded:- ',
						// amount_round_off);
						vendor_bill_rcrd_.setLineItemValue('item', 'grossamt',
							jk, amount_round_off_tds);
					}

					var tax_item_amount_round_off = Math.round(taxtotal_item);
					// nlapiLogExecution('audit', 'main amount rounded:- ',
					// amount_round_off);
					// vendor_bill_rcrd.setLineItemValue('item', 'taxtotal', jk,
					// tax_item_amount_round_off);
					// }
				}

			}
			if (item_line_count >= 1) {
				if (diff < 1 && parseFloat(diff) != parseFloat(0) && flag_diff == false) { // &&
					// parseFloat(diff)
					// !=
					// parseFloat(0)
					var account_tofill_item = vendor_bill_rcrd_
						.getLineItemValue('item', 'item', 1);
					vendor_bill_rcrd_.setLineItemValue('item', 'item',
						item_line_count + 1, account_tofill_item);
					vendor_bill_rcrd_.setLineItemValue('item', 'grossamt',
						item_line_count + 1, diff);
					vendor_bill_rcrd_.setLineItemValue('item', 'rate',
						item_line_count + 1, diff);
					vendor_bill_rcrd_.setLineItemValue('item', 'taxcode',
						item_line_count + 1, 2602);
					var cust_id_tofill_l = vendor_bill_rcrd_.getLineItemValue(
						'item', 'custcol_customer_entityid', 1);
					var proj_id_tofill_l = vendor_bill_rcrd_.getLineItemValue(
						'item', 'custcol_project_entity_id', 1);
					vendor_bill_rcrd_.setLineItemValue('item',
						'custcol_project_entity_id', item_line_count + 1,
						proj_id_tofill_l);
					vendor_bill_rcrd_.setLineItemValue('item',
						'custcol_customer_entityid', item_line_count + 1,
						cust_id_tofill_l);

				}
			}




			var item_gst_not_credit_flag = false;
			//Logic for expense out
			/*for (var jv = 1; jv <= item_line_count; jv++)
			{
			var st_notcredit = vendor_bill_rcrd.getLineItemValue('item', 'custcol_st_notcredit', jv); //custcol_st_notcredit
			if(st_notcredit == 'T')
			item_gst_not_credit_flag = true;
			//nlapiLogExecution('audit', 'item_gst_not_credit_flag:- ', item_gst_not_credit_flag);
			}*/
			for (var jk = 1; jk <= item_line_count; jk++) {
				//var i_subsidiary = vendor_bill_rcrd.getFieldValue('subsidiary');
				var taxtotal_item = vendor_bill_rcrd.getLineItemValue('item',
					'taxtotal', jk);
				var i_account_ = vendor_bill_rcrd.getLineItemValue('item',
					'account', jk); //custcol_st_notcredit
				var amount_round_off = vendor_bill_rcrd.getLineItemValue(
					'item', 'grossamt', jk);
			//	if ((parseInt(i_subsidiary) == parseInt(3) || parseInt(i_subsidiary) == parseInt(9))
            if((obj_offsite_onsite[i_subsidiary])	&& parseInt(i_currency) == parseInt(6)) {
					//            if(item_gst_not_credit_flag == true && (i_account_ == 1134 || i_account_ == 1135 || i_account_ ==  1136 )){
					//            nlapiLogExecution('audit', 'gst_not_credit_flag amount_round_off:- ', amount_round_off_);
					//            }
					//            else{
					var tds_line_item = vendor_bill_rcrd.getLineItemValue(
						'item', 'custcol_tdsline', jk);
					if (tds_line_item == 'T') {
						var tds_amt = vendor_bill_rcrd.getLineItemValue('item',
							'grossamt', jk);
						var amount_round_off_tds = Math.round(tds_amt);
						//nlapiLogExecution('audit', 'main amount rounded:- ', amount_round_off);
						vendor_bill_rcrd.setLineItemValue('item', 'grossamt',
							jk, amount_round_off_tds);
					}

					var tax_item_amount_round_off = Math.round(taxtotal_item);
					//nlapiLogExecution('audit', 'main amount rounded:- ', amount_round_off);
					//vendor_bill_rcrd.setLineItemValue('item', 'taxtotal', jk,tax_item_amount_round_off);
					//            }
				}

			}

			//if(submit_record_flag == 1)

			var vendor_bill_submitted_id = nlapiSubmitRecord(
				vendor_bill_rcrd_, true, true);
			nlapiLogExecution('debug', 'submitted bill id:-- ',
				vendor_bill_submitted_id);


		}


		/*var vendor_bill_submitted_id = nlapiSubmitRecord(
				vendor_bill_rcrd, true, true);
		nlapiLogExecution('debug', 'submitted bill id:-- ',
				vendor_bill_submitted_id);
	*/

		//}

	} catch (e) {
		nlapiLogExecution('ERROR', 'ERROR MESSAGE:-- ', e);
	}

	return true;

}

// END AFTER SUBMIT ===============================================

// BEGIN FUNCTION ===================================================
{

	function applySTRoundMethod(stRoundMethod, taxamount) {
		var roundedSTAmount = taxamount;

		if (stRoundMethod == 2) {
			roundedSTAmount = Math.round(taxamount)
		}
		if (stRoundMethod == 3) {
			roundedSTAmount = Math.round(taxamount / 10) * 10;
		}
		if (stRoundMethod == 4) {
			roundedSTAmount = Math.round(taxamount / 100) * 100;
		}

		return roundedSTAmount;
	}

	function _logValidation(value) {
		if (value != null && value.toString() != null && value != ''
			&& value != undefined && value.toString() != undefined
			&& value != 'undefined' && value.toString() != 'undefined'
			&& value.toString() != 'NaN' && value != NaN) {
			return true;
		} else {
			return false;
		}
	}

}
// END FUNCTION =====================================================

