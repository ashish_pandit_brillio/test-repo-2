/**
 *@NApiVersion 2.x
 *@NScriptType Restlet
 *@author Prabhat Gupta
 */
define(['N/record', 'N/search', 'N/format'],
    function(record, search, format) {

        function _logValidation(value) {
            if (value != null && value != '' && value != undefined && value.toString() != 'NaN' && value != NaN) {
                return true;
            } else {
                return false;
            }
        }

        function _nullValidation(value) {
            if (value == null || value == 'null' || value == 'NaN' || value == '' || value == undefined || value == '&nbsp;') {
                return true;
            } else {
                return false;
            }
        }

        function REST_BenchOrRevenue(context) {
            log.debug("request body", context.benchDataRequest)
            log.debug("request body", context.benchDataRequest)

            var benchDataRequest = context.benchDataRequest;

            var revenueDataRequest = context.revenueDataRequest;

            if (_logValidation(benchDataRequest)) {

                return GetBenchResource();

            } else if (_logValidation(revenueDataRequest)) {


                var revenueSearchResult = GetRevenueResult();

                return {
                    "revenueSearchResult": revenueSearchResult,
                    "status": true
                }

            } else {

                throw new Error("Request should be either for Bench data or Revenue data with key as benchDataRequest or revenueSearchResult")

            }
        }

        function GetBenchResource() {

            try {
                var startDate = getStartDate();
                log.debug("GetBenchResource : startdate", startDate);
                var endDate = getEndDate();
                log.debug("GetBenchResource : endDate", endDate);

                var filters = [
                    ["job.custentity_project_allocation_category","anyof","16","4"],
                    "AND",
                    ["job.status", "anyof", "2"],
                    "AND",
                    ["employee.custentity_employee_inactive", "is", "F"],
                    "AND",
                    ["startdate", "notafter", endDate], //+ 12 month
                    "AND",
                    ["enddate", "notbefore", startDate]
                ]
                var columns = [
                    search.createColumn({
                        name: "resource",
                        sort: search.Sort.ASC,
                        summary: "group"
                    }),
					search.createColumn({
                        name: "internalid",                       
                        summary: "group"
                    }),
                    search.createColumn({
                        name: "altname",
                        join: "customer",
                        summary: "group"
                    }),
                    search.createColumn({
                        name: 'customer',
                        join: "job",
                        summary: "group"
                    }),
                    search.createColumn({
                        name: 'project',
                        summary: "group"
                    }),
                    search.createColumn({
                        name: 'custentity_practice',
                        join: "job",
                        summary: "group"
                    }),
					 search.createColumn({
						 name: "custrecord_parent_practice",
						 join: "CUSTEVENT_PRACTICE",
						 label: "Parent Practice",
						 summary: "group"
					  }),
                    search.createColumn({
                        name: 'startdate',
                        summary: "group"
                    }),
                    search.createColumn({
                        name: 'enddate',
                        summary: "group"
                    }),
                    search.createColumn({
                        name: 'percentoftime',
                        summary: "group"
                    })
                ]

                var benchData = [];
                var resourceallocationSearchObj = search.create({
                    type: "resourceallocation",
                    filters: filters,
                    columns: columns

                });
                var searchResultCount = resourceallocationSearchObj.runPaged().count;
                log.debug("resourceallocationSearchObj result count", searchResultCount);

                var resultIndex = 0;
                var resultStep = 1000;
                var runSearch = resourceallocationSearchObj.run();
                log.debug("runSearch", runSearch);
                if (runSearch) {

                    var results;
                    do {
                        results = runSearch.getRange({
                            start: parseInt(resultIndex),
                            end: parseInt(resultIndex) + parseInt(resultStep)
                        });


                        for (var i = 0; results != null && i < results.length; i++) {

                            var date = getTodayDate();
                            var bench_Data = {};
//log.debug("recordId", results[i].id);
                            bench_Data = {

                                "recordId": results[i].getValue({
                                        name: "internalid",
                                        summary: "group"
                                    }),
                                "region": "Brillio",
                                "practice": results[i].getText({
                                    name: "custentity_practice",
                                    join: "job",
                                    summary: "group"
                                }),
								"parentPractice": results[i].getText({
                                    name: "custrecord_parent_practice",
									 join: "CUSTEVENT_PRACTICE",
									 label: "Parent Practice",
									 summary: "group"
                                }),
                                "accout": results[i].getValue({
                                    name: "altname",
                                    join: "customer",
                                    summary: "group"
                                }),
                                "id": results[i].getValue({
                                    name: "customer",
                                    join: "job",
                                    summary: "group"
                                }),
                                "project": {
                                    "name": results[i].getText({
                                        name: "project",
                                        summary: "group"
                                    }),
                                    "id": results[i].getValue({
                                        name: "project",
                                        summary: "group"
                                    })
                                },
                                "employee": results[i].getValue({
                                    name: "resource",
                                    summary: "group"
                                }),
                                "todaysdate": date,
                                "allocationstartdate": results[i].getValue({
                                    name: "startdate",
                                    summary: "group"
                                }),
                                "allocationenddate": results[i].getValue({
                                    name: "enddate",
                                    summary: "group"
                                }),
                                "allocationpercent": results[i].getValue({
                                    name: "percentoftime",
                                    summary: "group"
                                }),
                                "salesactivity": "Booked"
                            }

                            benchData.push(bench_Data);


                        }
                        resultIndex = parseInt(resultIndex) + parseInt(resultStep);
                    } while (results.length > 0);

                }
                return benchData;

            } catch (e) {

                return {
                    "benchData": e.message,
                    "status": false
                }
            }
        }

        function GetRevenueResult() {
            try {
                var filters = [
                    ["isinactive", "is", "F"]
                ]
                var columns = [
                    search.createColumn({
                        name: "custrecord_sfdc_revenue_data_stage"

                    }),
                    search.createColumn({
                        name: "custrecord_sfdc_revenue_data_sowid"

                    }),
                    search.createColumn({
                        name: 'custrecord_sfdc_revenue_data_startdate'

                    }),
                    search.createColumn({
                        name: 'custrecord_sfdc_revenue_data_prostatus'

                    }),
                    search.createColumn({
                        name: 'custrecord_sfdc_revenue_data_projectid'
                    }),
                    search.createColumn({
                        name: 'custrecord_sfdc_revenue_data_enddate'
                    }),
                    search.createColumn({
                        name: 'custrecord_sfdc_revenue_data_oppid'
                    }),
                    search.createColumn({
                        name: 'custrecord_sfdc_revenue_data_netcustid'
                    }),
                    search.createColumn({
                        name: 'custrecord_sfdc_revenue_data_accregion'
                    }),
                    search.createColumn({
                        name: 'custrecord_sfdc_revenue_data_accname'
                    }),
                    search.createColumn({
                        name: 'custrecord_sfdc_revenue_data_proj_id_ns'
                    }),
                    search.createColumn({
                        name: 'created',

                    }),
                    search.createColumn({
                        name: 'customer',
                        join: 'custrecord_sfdc_revenue_data_proj_id_ns'
                    }),
                    search.createColumn({
                        name: 'custrecord_sfdc_fuel_forecast_rev_amount',
                        join: 'CUSTRECORD_SFDC_FUEL_FORECAST_REV_EXT'
                    }),
                    search.createColumn({
                        name: 'custrecord_sfdc_fuel_fore_modayyear',
                        join: 'CUSTRECORD_SFDC_FUEL_FORECAST_REV_EXT'
                    }),
                    search.createColumn({
                        name: 'custrecord_sfdc_fuel_forecast_rev_pract',
                        join: 'CUSTRECORD_SFDC_FUEL_FORECAST_REV_EXT'
                    })
                ]

                var revenueData = [];
                var revenueSearchObj = search.create({
                    type: "customrecord_fuel_sfdc_revenue_data",
                    filters: filters,
                    columns: columns

                });
                var searchResultCount = revenueSearchObj.runPaged().count;
                log.debug("revenueSearchObj result count", searchResultCount);

                var resultIndex = 0;
                var resultStep = 1000;
                var runSearch = revenueSearchObj.run();
                log.debug("runSearch", runSearch);
                if (runSearch) {

                    var results;
                    var record_id = []

                    do {
                        results = runSearch.getRange({
                            start: parseInt(resultIndex),
                            end: parseInt(resultIndex) + parseInt(resultStep)
                        });


                        for (var j = 0; results != null && j < results.length; j++) {

                            

                                var revData = {};

                                revData = {
                                    "recordId": results[j].id,
                                    "region": results[j].getValue({
                                        name: "custrecord_sfdc_revenue_data_accregion"
                                    }),
                                    "practice": {
                                        "name": results[j].getText({
                                            name: "custrecord_sfdc_fuel_forecast_rev_pract",
                                            join: "CUSTRECORD_SFDC_FUEL_FORECAST_REV_EXT"
                                        }),
                                        "id": results[j].getValue({
                                            name: "custrecord_sfdc_fuel_forecast_rev_pract",
                                            join: "CUSTRECORD_SFDC_FUEL_FORECAST_REV_EXT"
                                        })
                                    },
                                    "account": {
                                        "name": results[j].getValue({
                                            name: "custrecord_sfdc_revenue_data_accname"
                                        }),
                                        "id": results[j].getValue({
                                            name: "customer",
                                            join: "custrecord_sfdc_revenue_data_proj_id_ns"
                                        })
                                    },
                                    "project": {
                                        "name": results[j].getValue({
                                            name: "custrecord_sfdc_revenue_data_proj_id_ns"
                                        }) ? results[j].getText({
                                            name: "custrecord_sfdc_revenue_data_proj_id_ns"
                                        }) : "",
                                        "id": results[j].getValue({
                                            name: "custrecord_sfdc_revenue_data_proj_id_ns"
                                        }) ? results[j].getValue({
                                            name: "custrecord_sfdc_revenue_data_proj_id_ns"
                                        }) : ""
                                    },
                                    "startdate": results[j].getValue({
                                        name: "custrecord_sfdc_revenue_data_startdate"
                                    }),
                                    "salesactivity": results[j].getValue({
                                        name: "custrecord_sfdc_revenue_data_prostatus"
                                    }),
                                    "forecastmonthandyear": results[j].getValue({
                                        name: "custrecord_sfdc_fuel_fore_modayyear",
                                        join: "CUSTRECORD_SFDC_FUEL_FORECAST_REV_EXT"
                                    }),
                                    "Amount": results[j].getValue({
                                        name: "custrecord_sfdc_fuel_forecast_rev_amount",
                                        join: "CUSTRECORD_SFDC_FUEL_FORECAST_REV_EXT"
                                    }),
                                    "oppid": results[j].getValue({
                                        name: "custrecord_sfdc_revenue_data_oppid"
                                    }) ? results[j].getValue({
                                        name: "custrecord_sfdc_revenue_data_oppid"
                                    }) : "",
                                    "created": results[j].getValue({
                                        name: "created"
                                    })
                                }


                                revenueData.push(revData);

                            


                        }
                        resultIndex = parseInt(resultIndex) + parseInt(resultStep);
                    } while (results.length > 0);

                }
                return revenueData;

            } catch (e) {

                return {
                    "revenueData": e.message,
                    "status": false
                }
            }


        }




        return {
            post: REST_BenchOrRevenue
        };



        function getStartDate() {
            var date = new Date();
            var startDate = new Date(date.getFullYear(), date.getMonth(), 1);
            var start_date = format.parse({
                value: startDate,
                type: format.Type.DATE
            });
            return dateToString(start_date);
        }

        function getEndDate() {
            var date = new Date();
            var endDate = new Date(date.getFullYear(), date.getMonth() + 12, 0);
            var end_date = format.parse({
                value: endDate,
                type: format.Type.DATE
            });
            return dateToString(end_date);

        }




        function getTodayDate() {
            var date = new Date();

            var todayDate = format.parse({
                value: date,
                type: format.Type.DATE
            });

            return dateToString(todayDate);

        }

        function dateToString(date) {
            var d = new Date(date);

            var date = d.getDate();
            var month = d.getMonth() + 1; // Since getMonth() returns month from 0-11 not 1-12
            var year = d.getFullYear();

            if (date < 10) {
                date = '0' + date;
            }

            if (month < 10) {
                month = '0' + month;
            }

            var dateStr = month + "/" + date + "/" + year; //mm/dd/yyyy
            return dateStr;
            //"2021-08-31T07:00:00.000Z"

        }



    }
);