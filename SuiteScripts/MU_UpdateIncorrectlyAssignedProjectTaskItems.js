/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       15 Jan 2015     amol.sahijwani
 *
 */

/**
 * @param {String} recType Record type internal id
 * @param {Number} recId Record internal id
 * @returns {Void}
 */
function massUpdate(recType, recId) {
	var recProjectTask = nlapiLoadRecord(recType, recId);
	var isRecordChanged = false;
	var s_name = recProjectTask.getFieldValue('title');
	var is_non_billable = recProjectTask.getFieldValue('nonbillabletask');
	var numAssignees = recProjectTask.getLineItemCount('assignee');
	var s_change = 'Project Task ID: ' + recId;
	s_change += ', Project Task Name: ' + s_name;
	s_change += ', Non Billable :' + is_non_billable;
	if(s_name == 'Leave' || s_name == 'Holiday' || s_name == 'Floating Holiday')
		{
			if(is_non_billable == 'F')
				{
					recProjectTask.setFieldValue('nonbillabletask', 'T');
					s_change += ', Non Billable F -> T, ';
					isRecordChanged = true;
				}
			for(var i = 1; i <= numAssignees; i++)
			{
				var i_service_item = recProjectTask.getLineItemValue('assignee', 'serviceitem', i);
			
				if(s_name == 'Leave' && (i_service_item == '2222' || i_service_item == '2221'))
					{
						recProjectTask.setLineItemValue('assignee', 'serviceitem', i, 2479);
						isRecordChanged = true;
						s_change += recProjectTask.getLineItemText('assignee', 'resource', i) + ': ' + i_service_item + ' -> ' + '2479' + '<br />';
					}
				else if(s_name == 'Holiday' && (i_service_item == '2222' || i_service_item == '2221'))
					{
						recProjectTask.setLineItemValue('assignee', 'serviceitem', i, 2480);
						isRecordChanged = true;
						s_change += recProjectTask.getLineItemText('assignee', 'resource', i) + ': ' + i_service_item + ' -> ' + '2480' + '<br />';
					}
				else if(s_name == 'Floating Holiday' && (i_service_item == '2222' || i_service_item == '2221'))
					{
						recProjectTask.setLineItemValue('assignee', 'serviceitem', i, 2481);
						isRecordChanged = true;
						s_change += recProjectTask.getLineItemText('assignee', 'resource', i) + ': ' + i_service_item + ' -> ' + '2481' + '<br />';
					}
			}
		}
	
	if(isRecordChanged == true)
		{
			nlapiSubmitRecord(recProjectTask);
			nlapiLogExecution('AUDIT', recId, s_change);
		}
	else
		{
			nlapiLogExecution('DEBUG', recId, s_change);
		}
	
	
}
