/**
 * @author Jayesh
 */

function validatePractice(request,response)
{
	try
	{
		var i_mode = request.getParameter('mode');
		if(i_mode == 1)
		{
			var i_sub_practice_available = request.getParameter('i_sub_practice_available');
			if(i_sub_practice_available)
			{
				var a_practice_filter = [['internalid', 'anyof', parseInt(i_sub_practice_available)]];
					
				var a_column_practice = new Array();
				a_column_practice[0] = new nlobjSearchColumn('custrecord_parent_practice');
				
				var a_practice_gdm = nlapiSearchRecord('department', null, a_practice_filter, a_column_practice);
				if (a_practice_gdm)
				{
					i_parent_prac_id = a_practice_gdm[0].getValue('custrecord_parent_practice');
				}
				
				response.write(i_parent_prac_id);
			}
		}
		else if(i_mode == 2)
		{
			var i_revenue_share_id = request.getParameter('i_revenue_share_id');
			var a_practice_involved = new Array();
			nlapiLogExecution('audit','i_revenue_share_id:- ',i_revenue_share_id);
			
			var a_revenue_cap_filter = [['custrecord_revenue_share_per_practice_ca.internalid', 'anyof', parseInt(i_revenue_share_id)]];
			
			var a_columns_existing_cap_srch = new Array();
			a_columns_existing_cap_srch[0] = new nlobjSearchColumn('custrecord_revenue_share_per_practice_pr');
			a_columns_existing_cap_srch[1] = new nlobjSearchColumn('custrecord_revenue_share_per_practice_su');
		
			var a_exsiting_revenue_cap = nlapiSearchRecord('customrecord_revenue_share_per_practice', null, a_revenue_cap_filter, a_columns_existing_cap_srch);
			if (a_exsiting_revenue_cap)
			{
				for(var i_revenue_index = 0; i_revenue_index < a_exsiting_revenue_cap.length; i_revenue_index++)
				{
					var i_parent_parctice_id = a_exsiting_revenue_cap[i_revenue_index].getValue('custrecord_revenue_share_per_practice_pr');
					var i_sub_parctice_id = a_exsiting_revenue_cap[i_revenue_index].getValue('custrecord_revenue_share_per_practice_su');
					
					if(a_practice_involved.indexOf(i_parent_parctice_id) < 0)
					{
						a_practice_involved.push(i_parent_parctice_id);
					}
					if(a_practice_involved.indexOf(i_sub_parctice_id) < 0)
					{
						a_practice_involved.push(i_sub_parctice_id);
					}
				}
			}
			
			var a_recipient_mail_id = '';
			var a_filters_search_practice = [['internalid','anyof',a_practice_involved]
									  ];
									  
			var a_columns_practice = new Array();
			a_columns_practice[0] = new nlobjSearchColumn('email', 'custrecord_practicehead');
			
			var a_parctice_srch = nlapiSearchRecord('department', null, a_filters_search_practice, a_columns_practice);
			if (a_parctice_srch)
			{
				for(var i_practice_index = 0; i_practice_index < a_parctice_srch.length; i_practice_index++)
				{
					a_recipient_mail_id += a_parctice_srch[i_practice_index].getValue('email', 'custrecord_practicehead') + ':';
				}
			}
			response.write(a_recipient_mail_id);
		}
		else if(i_mode == 3)
		{
			var i_project_executing_practice = request.getParameter('i_project_executing_practice');
			
			var s_executing_practice_head_details = '';
			var i_executing_practice_head = nlapiLookupField('department',i_project_executing_practice,'custrecord_practicehead');
			var s_executing_practice_head = nlapiLookupField('department',i_project_executing_practice,'custrecord_practicehead',true);
		
			s_executing_practice_head_details += i_executing_practice_head +':';
			s_executing_practice_head_details += s_executing_practice_head;
			
			response.write(s_executing_practice_head_details);
		}
		else if(i_mode == 4)
		{
			var i_executing_practice_head = request.getParameter('i_executing_practice_head');
			var s_executing_practice_head_mail = nlapiLookupField('employee',i_executing_practice_head,'email');
			
			response.write(s_executing_practice_head_mail);
		}
		else if(i_mode == 5)
		{
			var i_project_id = request.getParameter('i_proj_id');
			var i_revenue_share_id = request.getParameter('i_revenue_share_id');
			nlapiLogExecution('audit','project id:- '+i_project_id,'revenue share id:- '+i_revenue_share_id);
			var a_approval_mail_recipient = 'success';
			
			/*var a_projectData = nlapiLookupField('job', i_project_id, [
				        'custentity_projectmanager', 'custentity_deliverymanager',
				        'customer', 'custentity_clientpartner']);
						
			var s_recipient_mail_pm = nlapiLookupField('employee',a_projectData.custentity_projectmanager,'email');
			var s_recipient_mail_dm = nlapiLookupField('employee',a_projectData.custentity_deliverymanager,'email');
			var s_recipient_mail_cp = nlapiLookupField('employee',a_projectData.custentity_clientpartner,'email');
			var i_client_cp = nlapiLookupField('customer',a_projectData.customer,'custentity_clientpartner');
			var s_recipient_mail_acp = nlapiLookupField('employee',i_client_cp,'email');
			
			a_approval_mail_recipient += s_recipient_mail_pm +':';
			a_approval_mail_recipient += s_recipient_mail_dm +':';
			a_approval_mail_recipient += s_recipient_mail_cp +':';
			a_approval_mail_recipient += s_recipient_mail_acp;*/
			
			// call schedule script to create month end effort entries
			var params=new Array();
			params['custscript_proj_id_to_send_mail'] = i_project_id;
			params['custscript_revenue_share_id_to_process'] = i_revenue_share_id;
		 
			var status = nlapiScheduleScript('customscript_sch_fp_rev_rec_create_effrt',null,params);
			nlapiLogExecution('audit','schuduled script triggered');
			response.write(a_approval_mail_recipient);
		}
		else if(i_mode == 6)
		{
			var i_project_id = request.getParameter('i_proj_id');
			
			var a_approval_mail_recipient = '';
			
			var a_projectData = nlapiLookupField('job', i_project_id, [
				        'custentity_projectmanager', 'custentity_deliverymanager',
				        'customer', 'custentity_clientpartner']);
						
			var s_recipient_mail_pm = nlapiLookupField('employee',a_projectData.custentity_projectmanager,'email');
			var s_recipient_mail_dm = nlapiLookupField('employee',a_projectData.custentity_deliverymanager,'email');
			var s_recipient_mail_cp = nlapiLookupField('employee',a_projectData.custentity_clientpartner,'email');
			var i_client_cp = nlapiLookupField('customer',a_projectData.customer,'custentity_clientpartner');
			var s_recipient_mail_acp = nlapiLookupField('employee',i_client_cp,'email');
			
			a_approval_mail_recipient += s_recipient_mail_pm +':';
			a_approval_mail_recipient += s_recipient_mail_dm +':';
			a_approval_mail_recipient += s_recipient_mail_cp +':';
			a_approval_mail_recipient += s_recipient_mail_acp;
			
			response.write(a_approval_mail_recipient);
		}
		else if(i_mode == 7)
		{
			var i_region_id = request.getParameter('i_region_id');
			var i_region_head = nlapiLookupField('customrecord_region',i_region_id,'custrecord_region_head');
			var s_region_head_mail = nlapiLookupField('employee',i_region_head,'email');
			
			response.write(s_region_head_mail);
		}
	}
	catch(err)
	{
		nlapiLogExecution('ERROR','validatePractice','ERROR MESSAGE :- '+err);
	}
}
