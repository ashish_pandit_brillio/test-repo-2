function update_JE()
{
	try
	{
		var current_date = nlapiDateToString(new Date());
		//Log for current date
		nlapiLogExecution('DEBUG', 'Execution Started Current Date', 'current_date...' + current_date);

		var timestp = timestamp();
		nlapiLogExecution('DEBUG', 'Execution Started Current Date', 'timestp...' + timestp);
		var counter ='';
		var context = nlapiGetContext();
		
		var je_list = new Array();
		var filter = new Array();
		var a_results_je = searchRecord('transaction', 'customsearch3232', null, null);
		//var a_results_je = searchRecord('transaction', 'customsearch2914', null, null);
		if (_logValidation(a_results_je))
		{	
			for(var counter = 0; counter<a_results_je.length ; counter++)
			{
				nlapiLogExecution('audit','j:-- ',counter);
				nlapiLogExecution('audit','a_results_je:-- ',a_results_je.length);
				
					nlapiLogExecution('audit','a_results_je[i].getId():-- ',a_results_je[counter].getId());
					
					if(je_list.indexOf(a_results_je[counter].getId())>=0)
					{
						
					}
					else
					{
						var o_je_rcrd = nlapiLoadRecord('expensereport',a_results_je[counter].getId());//332853
						var i_line_count = o_je_rcrd.getLineItemCount('expense');
						for (var i = 1; i <= i_line_count; i++)
						{
							var usageEnd = context.getRemainingUsage();
							if (usageEnd < 1000) 
							{
								yieldScript(context);
							}	
					
							var emp_type = '';
							var emp_fusion_id = '';
							var person_type = '';
							var onsite_offsite = '';
							var s_employee_name_split = '';
							var emp_frst_name = '';
							var emp_middl_name = '';
							var emp_lst_name = '';
							var emp_full_name = '';
							var emp_blnk_flag = 0;
							var emp_practice = '';
							var emp_practice_text ='';
							var emp_location = '';
							var employee_with_id = '';
							
							var proj_name = '';
							var proj_entity_id = '';
							var proj_billing_type = '';
							var s_proj_desc_split = '';
							var region_id = '';
							var proj_category = '';
							var proj_vertical= '';
							var proj_full_name_with_id = '';
							var proj_practice = '';
							var project_id = '';
							var i_project_service = '';
							var proj_category_val='';
							var core_practice = '';
							var cust_name = '';
							var cust_entity_id = '';
							var cust_id = '';
							var cust_internal_id = '';
							var cust_territory = '';
							var customer_id = '';
							var customerObj = '';
							var customer_region = '';
							var cust_name_with_id = '';
							var s_cust_id_split = ''; 
							var  misc_practice = '';
							var parent_practice = '';
							var project_region = '';
							var isinactive_Practice_mis = '';
							
							
							//Customer
							var cust_name_id = o_je_rcrd.getLineItemValue('expense','custcolcustcol_temp_customer',i);
							var cust_id = o_je_rcrd.getLineItemValue('expense','custcol_customer_entityid',i);

							var existing_practice = o_je_rcrd.getLineItemValue('expense', 'department',i);

							var proj_desc = o_je_rcrd.getLineItemValue('expense','custcolprj_name',i);
							var proj_desc_id = o_je_rcrd.getLineItemValue('expense','custcol_project_entity_id',i);
							if(proj_desc_id){
							s_proj_desc_split = proj_desc_id;
							nlapiLogExecution('audit','s_proj_desc_split',s_proj_desc_split);
							}
							else if(proj_desc){
								s_proj_desc_id = proj_desc.substr(0,9);
								s_proj_desc_split = s_proj_desc_id;
								nlapiLogExecution('audit','proj_desc',proj_desc);
							}
							
							if (proj_desc || proj_desc_id )
							{
								//var s_proj_desc_id = proj_desc.split(' ');
								//var s_proj_desc_split = s_proj_desc_id[0];
								
								//var s_proj_desc_id = proj_desc.substr(0,9);
								//var s_proj_desc_split = s_proj_desc_id;
								s_proj_desc_split = s_proj_desc_split.trim();
								var filters_proj = new Array();
								filters_proj[0] = new nlobjSearchFilter('entityid', null, 'startswith', s_proj_desc_split);
								var column_proj = new Array();
								column_proj[0] = new nlobjSearchColumn('jobbillingtype');
								column_proj[1] = new nlobjSearchColumn('customer');
								column_proj[2] = new nlobjSearchColumn('custentity_region');
								column_proj[3] = new nlobjSearchColumn('entityid');
								column_proj[4] = new nlobjSearchColumn('altname');
								column_proj[5] = new nlobjSearchColumn('custentity_project_allocation_category');
								column_proj[6] = new nlobjSearchColumn('companyname','customer');
								column_proj[7] = new nlobjSearchColumn('entityid','customer');
								column_proj[8] = new nlobjSearchColumn('custentity_vertical');
								column_proj[9] = new nlobjSearchColumn('territory','customer');
								column_proj[10] = new nlobjSearchColumn('custentity_practice');
								column_proj[11] = new nlobjSearchColumn('internalid','customer');
								column_proj[12] = new nlobjSearchColumn('internalid');
								column_proj[13] = new nlobjSearchColumn('custentity_project_services');
								column_proj[14] = new nlobjSearchColumn('custrecord_parent_practice','custentity_practice');
								
								
								var a_results_proj = nlapiSearchRecord('job', null, filters_proj, column_proj);
								if (a_results_proj)
								{
									project_id = a_results_proj[0].getValue('internalid');
									proj_billing_type = a_results_proj[0].getText('jobbillingtype');
									customer_id = a_results_proj[0].getValue('internalid','customer');
									nlapiLogExecution('debug','customer_id',customer_id +'At expense:'+i);
									region_id = a_results_proj[0].getText('custentity_region');
									nlapiLogExecution('debug','region_id',region_id);
									var proj_rcrd = nlapiLoadRecord('job',parseInt(project_id));
									proj_name = proj_rcrd.getFieldValue('altname');
									proj_entity_id = a_results_proj[0].getValue('entityid');
									proj_category = a_results_proj[0].getText('custentity_project_allocation_category');
									proj_vertical = a_results_proj[0].getValue('custentity_vertical');
									i_project_service = a_results_proj[0].getValue('custentity_project_services');
									proj_category_val = a_results_proj[0].getValue('custentity_project_allocation_category');
									parent_practice = a_results_proj[0].getText('custrecord_parent_practice','custentity_practice');
									
									
									
								}
							}
							
							

							//
							
							o_je_rcrd.setLineItemValue('expense', 'custcol_proj_category_on_a_click', i, proj_category);
							
							
						}
						
						o_je_rcrd.setFieldValue('custbody_is_je_updated_for_emp_type','T');
						var je_submit_id = nlapiSubmitRecord(o_je_rcrd, {disabletriggers : true, enablesourcing : true});
						nlapiLogExecution('audit','je_submit_id:-- ',je_submit_id);
						je_list.push(je_submit_id);
					}
					
					yieldScript(context);
			}
			nlapiLogExecution('DEBUG', 'usageEnd =' + usageEnd + '-->j-->' + counter);
			
		}
			var timestp_E = timestamp();
			nlapiLogExecution('DEBUG', 'Execution Ended Current Date', 'timestp_E...' + timestp_E);
	}
	catch(err)
	{
		nlapiLogExecution('ERROR','ERROR MESSAGE:-- ',err);
	}
}

function yieldScript(currentContext) {

		nlapiLogExecution('AUDIT', 'API Limit Exceeded');
		var state = nlapiYieldScript();

		if (state.status == "FAILURE") {
			nlapiLogExecution('ERROR', 'Yield Script Failed', 'Reason : '
			        + state.reason + ' / Size : ' + state.size);
			return false;
		} else if (state.status == "RESUME") {
			nlapiLogExecution('AUDIT', 'Script Resumed');
		}
}

function Schedulescriptafterusageexceeded()
{
    try
    {
        var params = new Array();
        var startDate = new Date();
        params['startdate'] = startDate.toUTCString();
        var status = nlapiScheduleScript(nlapiGetContext().getScriptId(), nlapiGetContext().getDeploymentId(), params);
        //nlapiLogExecution('DEBUG', 'After Scheduling', 'Script scheduled status=' + status);
    } 
    catch (e) 
    {
        nlapiLogExecution('DEBUG', 'In Scheduled Catch', 'e : ' + e.message);
    }
}

function _logValidation(value){
    if (value != null && value != '' && value != undefined && value.toString() != 'NaN' && value != NaN) {
        return true;
    }
    else {
        return false;
    }
}

function searchRecord(recordType, savedSearch, arrFilters, arrColumns,
        filterExpression)
{

	try {
		var search = null;

		// if a saved search is provided, load it and add the filters and
		// columns
		if (isNotEmpty(savedSearch)) {
			search = nlapiLoadSearch(recordType, savedSearch);

			if (isArrayNotEmpty(arrFilters)) {
				search.addFilters(arrFilters);
			}

			if (isArrayNotEmpty(arrColumns)) {
				search.addColumns(arrColumns);
			}

			if (isArrayNotEmpty(filterExpression)) {
				search.setFilterExpression(filterExpression);
			}
		}
		// create a new search
		else {
			search = nlapiCreateSearch(recordType, arrFilters, arrColumns);
		}

		// run search
		var resultSet = search.runSearch();

		// iterate through the search and get all data 1000 at a time
		var searchResultCount = 0;
		var resultSlice = null;
		var searchResult = [];

		do {
			resultSlice = resultSet.getResults(searchResultCount,
			        searchResultCount + 1000);

			if (resultSlice) {

				resultSlice.forEach(function(result) {

					searchResult.push(result);
					searchResultCount++;
				});
			}
		} while (isArrayNotEmpty(resultSlice) && resultSlice.length >= 1000);

		return searchResult;
	} catch (err) {
		nlapiLogExecution('ERROR', 'searchRecord', err);
		throw err;
	}
}

function isEmpty(value) {

	return value == null || value == "" || typeof (value) == undefined;
}

function isNotEmpty(value) {

	return !isEmpty(value);
}

function isArrayEmpty(argArray) {

	return !isArrayNotEmpty(argArray);
}

function isArrayNotEmpty(argArray) {

	return (isNotEmpty(argArray) && argArray.length > 0);
}
function timestamp() {
var str = "";

var currentTime = new Date();
var hours = currentTime.getHours();
var minutes = currentTime.getMinutes();
var seconds = currentTime.getSeconds();
var meridian = "";
if (hours > 12) {
    meridian += "pm";
} else {
    meridian += "am";
}
if (hours > 12) {

    hours = hours - 12;
}
if (minutes < 10) {
    minutes = "0" + minutes;
}
if (seconds < 10) {
    seconds = "0" + seconds;
}
str += hours + ":" + minutes + ":" + seconds + " ";

return str + meridian;
}