/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       02 Dec 2014     amol.sahijwani
 *
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType
 *   
 * @param {String} type Operation types: create, edit, view, copy, print, email
 * @param {nlobjForm} form Current form
 * @param {nlobjRequest} request Request object
 * @returns {Void}
 */
function userEventBeforeLoad(type, form, request){
	var d_start_date = new Date(nlapiGetFieldValue('startdate'));
	
	var d_today = new Date();
	
	var days = ['monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday'];
	
	for(var i_day_indx = 0; i_day_indx < 7; i_day_indx++)
		{
			if(nlapiAddDays(d_today, 7) > nlapiAddDays(d_start_date, i_day_indx))
			{
				nlapiDisableLineItemField('timegrid', days[i_day_indx], false);
			}
		}
	
}
