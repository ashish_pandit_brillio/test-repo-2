/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       22 Apr 2020     deepak.srinivas
 *
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType
 * 
 * @param {String} type Operation types: create, edit, delete, xedit
 *                      approve, reject, cancel (SO, ER, Time Bill, PO & RMA only)
 *                      pack, ship (IF)
 *                      markcomplete (Call, Task)
 *                      reassign (Case)
 *                      editforecast (Opp, Estimate)
 * @returns {Void}
 */
function userEventBeforeSubmit(type) {

    try {
        nlapiLogExecution('debug', 'type', type);
        var currentContext = nlapiGetContext();
        nlapiLogExecution('debug', 'Context', currentContext.getExecutionContext());
        var dataRow = [];
        if (type == 'delete') {
            dataRow.push(nlapiGetRecordId());
            var url = "https://onthegoactivitylog.azurewebsites.net/onthego-v2-payrollmanagement/api/timesheet?apitype=1003";
            var method = "POST";
            var headers = {
                "x-api-key": "615679DD-ABEA-48CE-B916-28974A309F77",
                "x-api-customer-key": "98F78573-3172-4A8D-A521-3DC619AF2BE1",
                "x-api-token-key": "8D660F3B-7CA9-47D0-848A-E8D7BE1B2786",
                "x.api.user": "113056",
                "content-type": "application/json",
                "Accept": "application/json"

            };
            var JSONBody = JSON.stringify(dataRow, replacer);
            var responseObj = nlapiRequestURL(url, JSONBody, headers, null, method);
            nlapiLogExecution("DEBUG", "Response Body :", (responseObj.body)); //JSON.stringify

            if (!(parseInt(JSON.parse(responseObj.getCode())) == 200)) {
                // send Email to fuel
                // send Email to fuel
                var file = nlapiCreateFile("payload.json", "PLAINTEXT", JSONBody);
                var emailSub = "Error in TS Updation #" + nlapiGetRecordId(); //onthego
                var emailBoody = "Team," + "\r\n" + "Error while updating the TS details into OTG server #" + nlapiGetRecordId() + "\r\n" + "\r\n" + 'Employee :' + emp + 'and the week start date: ' + weekStart + "\r\n" + responseObj.body + "\r\n" + "\r\n" + "Please find attached payLoad." + "\r\n";
                nlapiSendEmail(442, "onthego@BRILLIO.COM", emailSub, emailBoody, 'onthego@BRILLIO.COM', null, null, file);
            }
        }
    } catch (e) {
        nlapiLogExecution('DEBUG', 'TS Notification Error - OTG', e);
    }
}

function replacer(key, value) {
    if (typeof value == "number" && !isFinite(value)) {
        return String(value);
    }
    return value;
}