function scheduledScript(request,response) {

    try {
        var invoiceArray = new Array();
		
		//var context = nlapiGetContext();
		//var invData = context.getSetting("SCRIPT", "custscript_invoice_array");
		//invoiceArray = invData.toString();
		//var i_invoice = invoiceArray.split(',');
		var i_invoice = nlapiSearchRecord('transaction', 'customsearch4048', null, null);
		nlapiLogExecution('debug', ' SALES INVOICE', 'Execution Start Here: ' + 'Array Length :' + i_invoice.length);
		if(i_invoice)
		{
			for(var invAry = 0; invAry<i_invoice.length;invAry++)
			{
				var invRecObj = nlapiLoadRecord('invoice',i_invoice[invAry].getId());
				nlapiLogExecution('debug','i_invoice[invAry].getId() ',i_invoice[invAry].getId());
				var i_proj_internal_id = invRecObj.getFieldValue('job');
				var projectType = nlapiLookupField('job', i_proj_internal_id,'jobbillingtype');
				nlapiLogExecution('debug', 'project type', projectType);
				var i_Location = invRecObj.getFieldValue('location');
				if (projectType == 'TM') {

					var subsidiary = invRecObj.getFieldValue('subsidiary');
					var taxCode = invRecObj.getFieldValue('custbody_inv_tax_code');
					var doApplyTaxCode = (projectType == 'TM') &&
						(subsidiary == constant.Subsidiary.IN) &&
						isNotEmpty(taxCode);
					var taxCodeAlreadySet = false;
					
					
					// Billing From and To
					var billingFrom = invRecObj.getFieldValue('custbody_billfrom');
					var billingTo = invRecObj.getFieldValue('custbody_billto');

					// check if both the fields have values
					if (isNotEmpty(billingFrom) && isNotEmpty(billingTo)) {
						billFrom = nlapiStringToDate(billingFrom);
						billTo = nlapiStringToDate(billingTo);
						taxCodeAlreadySet = doApplyTaxCode; // setting it to true so we
						// don't need to set tax code again

						var b_is_multi_loc_invoice = invRecObj.getFieldValue('custbody_is_multi_loc_invoice');
						if (b_is_multi_loc_invoice == 'T') {
							var a_resource_allocations = new Array();
							var a_emp_list = new Array();

							var filters_search_allocation = new Array();
							filters_search_allocation[0] = new nlobjSearchFilter('project', null, 'anyof', i_proj_internal_id);
							filters_search_allocation[1] = new nlobjSearchFilter('startdate', null, 'onorbefore', billTo);
							filters_search_allocation[2] = new nlobjSearchFilter('enddate', null, 'onorafter', billFrom);
							var columns = new Array();
							columns[0] = new nlobjSearchColumn('resource');
							columns[1] = new nlobjSearchColumn('enddate');
							columns[2] = new nlobjSearchColumn('custevent_gstworklocation');

							var project_allocation_result = nlapiSearchRecord('resourceallocation', null, filters_search_allocation, columns);
							if (project_allocation_result) {
								for (var i_search_indx = 0; i_search_indx < project_allocation_result.length; i_search_indx++) {
									var i_employee_id = project_allocation_result[i_search_indx].getValue('resource');
									var s_end_date = project_allocation_result[i_search_indx].getValue('enddate');
									var i_gst_state = project_allocation_result[i_search_indx].getValue('custevent_gstworklocation');

									a_resource_allocations[i_search_indx] = {
										'i_employee_id': i_employee_id,
										's_end_date': s_end_date,
										'i_gst_state': i_gst_state
									};
									a_emp_list.push(i_employee_id);
								}
							}

							var i_gst_place_of_spply = invRecObj.getFieldValue('custbody_iit_pos_invoice');
						}
						var lineCountItem = invRecObj.getLineItemCount('item');
						nlapiLogExecution('debug', 'lineCountItem  ', lineCountItem);
						for (var itemLine = lineCountItem; itemLine > 0; itemLine--) {
							nlapiLogExecution('debug','remove line ',itemLine);
							invRecObj.removeLineItem('item', itemLine);
						}
						var lineCount = invRecObj.getLineItemCount('time');

						for (var linenum = 1; linenum <= lineCount; linenum++) {
							var billedDate = nlapiStringToDate(invRecObj.getLineItemValue('time', 'billeddate', linenum));

							if (billFrom <= billedDate && billTo >= billedDate) {
								
								if (b_is_multi_loc_invoice == 'T') {
									var i_lineLevel_emp = invRecObj.getLineItemValue('time', 'employee', linenum);
									var i_existing_emp_index = a_emp_list.indexOf(i_lineLevel_emp);
									if (i_existing_emp_index >= 0) {
										var s_emp_allo_end_date = a_resource_allocations[i_existing_emp_index].s_end_date;
										s_emp_allo_end_date = nlapiStringToDate(s_emp_allo_end_date);

										if (s_emp_allo_end_date < billedDate) {
											for (var i_loop_emp = i_existing_emp_index; i_loop_emp < a_emp_list.length; i_loop_emp++) {
												if (a_emp_list[i_loop_emp] == i_lineLevel_emp) {
													var s_emp_allo_end_date = a_resource_allocations[i_loop_emp].s_end_date;
													s_emp_allo_end_date = nlapiStringToDate(s_emp_allo_end_date);

													if (s_emp_allo_end_date < billedDate) {

													} else {
														var i_gst_state_final_allo = a_resource_allocations[i_loop_emp].i_gst_state;

														if (i_gst_state_final_allo == i_gst_place_of_spply) {
															invRecObj.setLineItemValue('time', 'apply', linenum, 'T');
															invRecObj.setLineItemValue('time', 'location',linenum,'');
															if (doApplyTaxCode) {
																invRecObj.setLineItemValue('time', 'taxcode', linenum, taxCode);
															}
														} else {
															invRecObj.setLineItemValue('time', 'apply', linenum, 'F');
														}
													}
												} else {
													invRecObj.setLineItemValue('time', 'apply', linenum, 'F');
												}
											}
										} else {
											var i_gst_state_final_allo = a_resource_allocations[i_existing_emp_index].i_gst_state;

											if (i_gst_state_final_allo == i_gst_place_of_spply) {
												nlapiLogExecution('audit', 'check timesheet');
												invRecObj.setLineItemValue('time', 'apply', linenum, 'T');
												invRecObj.setLineItemValue('time', 'location',linenum,'');
												if (doApplyTaxCode) {
													invRecObj.setLineItemValue('time', 'taxcode', linenum, taxCode);
												}
											} else {
												nlapiLogExecution('audit', 'uncheck timesheet');
												invRecObj.setLineItemValue('time', 'apply', linenum, 'F');
											}
										}
									} else {
										invRecObj.setLineItemValue('time', 'apply', linenum, 'F');
									}
								} else {
									invRecObj.setLineItemValue('time', 'apply', linenum, 'T');
									invRecObj.setLineItemValue('time', 'location',linenum,''); // Need to check with uncomment
									if (doApplyTaxCode) {
										invRecObj.setLineItemValue('time', 'taxcode', linenum, taxCode);
									}
								}
							} else {
								invRecObj.setLineItemValue('time', 'apply', linenum, 'F');
							}
						}
						nlapiLogExecution('debug', 'auto check done');
					} else {
						throw "Billing From and To are empty";
					}

					invRecObj.setFieldValue('custbody_auto_check_timesheet', 'F');
					

					if (doApplyTaxCode && !taxCodeAlreadySet) {
						nlapiLogExecution('debug', 'setting tax code', 2);
						var lineCount = invRecObj.getLineItemCount('time');
						nlapiLogExecution('debug', 'lineCount ', lineCount);
						var billingTo = invRecObj.getFieldValue('custbody_billto');
						billTo = nlapiStringToDate(billingTo);

						for (var linenum = 1; linenum <= lineCount; linenum++) {
							var isApplied = invRecObj.getLineItemValue('time', 'apply', linenum);

							if (isTrue(isApplied)) {
								invRecObj.setLineItemValue('time', 'taxcode', linenum, taxCode);
								nlapiLogExecution('debug', 'set', 'set');
							}
						}
					}
				}
				var recId = nlapiSubmitRecord(invRecObj);
				nlapiLogExecution('debug','recId  ',recId);
			}
		}
    } catch (error) {
        nlapiLogExecution('ERROR', 'SALES INVOICE', 'Error Details:' + error);
    }
    nlapiLogExecution('debug', 'SALES INVOICE', 'Execution End Here: ');
}