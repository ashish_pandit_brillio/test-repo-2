//Prabhat Gupta
function userEventBeforeSubmit(type) {

    try {
        nlapiLogExecution('debug', 'type', type);
        var currentContext = nlapiGetContext();
        nlapiLogExecution('debug', 'Context', currentContext.getExecutionContext());
        var dataRow = [];
        if (type == 'delete') {
            dataRow.push(nlapiGetRecordId());
            
            var commonHeader = {
            		"Content-Type": "application/json",
           		    "accept": "application/json"
            };
            //************************Token Generation steps*****************************//
            var tokenURL = 'https://onthegoactivitylog.azurewebsites.net/onthego-v2-payrollmanagement/authenticate';
            var tokenMethod = 'POST';
            var tokenBody = {
            		 "username":"ankit",
            		 "password":"ankit" 

            };
            var tokenResponse = nlapiRequestURL(tokenURL, JSON.stringify(tokenBody), commonHeader, null, tokenMethod);
            nlapiLogExecution("DEBUG", "tokenResponse Body :", (tokenResponse.body)); //JSON.stringify
            var tken = JSON.parse(tokenResponse.body);
          //  nlapiLogExecution("DEBUG", "tokenResponse token :", (tken.token)); //JSON.stringify
            //********************END***********************************//
            
            var url = "https://onthegoactivitylog.azurewebsites.net/onthego-v2-payrollmanagement/api/expense?apitype=1003";
            var method = "POST";
            var headers = {
                	"Authorization": "Bearer "+tken.token,
                    "x-api-key": "615679DD-ABEA-48CE-B916-28974A309F77",
                    "x-api-customer-key": "98F78573-3172-4A8D-A521-3DC619AF2BE1",
                    "x-api-token-key": "8D660F3B-7CA9-47D0-848A-E8D7BE1B2786",
                    "x.api.user": "113056",
                    "content-type": "application/json",
                    "Accept": "application/json"

                };
            var JSONBody = JSON.stringify(dataRow, replacer);
            var responseObj = nlapiRequestURL(url, JSONBody, headers, null, method);
            nlapiLogExecution("DEBUG", "Response Body :", (responseObj.body)); //JSON.stringify

            if (!(parseInt(JSON.parse(responseObj.getCode())) == 200)) {
            	var a_emp_attachment = new Array();
        		a_emp_attachment['entity'] = 83134;
                // send Email to fuel
                // send Email to fuel
                var file = nlapiCreateFile("payload.json", "PLAINTEXT", JSONBody);
                var emailSub = "Error in Expense Deletion #" + nlapiGetRecordId(); //onthego
                var emailBoody = "Team," + "\r\n" + "Error while deleting the expense details into OTG server Timesheet #" + nlapiGetRecordId() + "\r\n";
                nlapiSendEmail(442, "onthego@BRILLIO.COM", emailSub, emailBoody, 'information.systems@brillio.com', null, a_emp_attachment, file);
            }
        }
    }catch (err) {
    	var a_emp_attachment = new Array();
		a_emp_attachment['entity'] = 83134;
		 var emailSub = "Error in Expense - NS Script Error #" + nlapiGetRecordId(); //onthego
		 var emailBoody = "Team," + "\r\n" + "Error while deleting the expense details into OTG server #" + nlapiGetRecordId() + "\r\n" + "Please find below NS error." + err + "\r\n";
       // nlapiSendEmail(442, "onthego@BRILLIO.COM", emailSub, emailBoody, 'information.systems@brillio.com', null, a_emp_attachment);
        nlapiLogExecution('DEBUG', 'TS Notification Error - OTG', err);
    }
}


function otg_expense_notification(type) {
   
    try {
    	 var currentContext = nlapiGetContext();
    	 nlapiLogExecution('Debug', 'type ', type);
    	 nlapiLogExecution('debug', 'Context', currentContext.getExecutionContext());
       
        //Dont trigger if type is delete
        if(type != 'delete')
		{
        var recordObject = nlapiLoadRecord(nlapiGetRecordType(), nlapiGetRecordId());
        var status = recordObject.getFieldValue('statusRef');
        nlapiLogExecution('debug', 'Expense status', status);
           if((currentContext.getExecutionContext() == 'userinterface' || currentContext.getExecutionContext() == 'userevent') )
			/*&& (status == 'inProgress' || status == 'pendingSupApproval' ||
        		status == 'rejectedBySup' || status == 'pendingAcctApproval')) */
        {
            var expense = {};
            //old record object
            var oldRecord	=	nlapiGetOldRecord();
            
            var purpose = recordObject.getFieldValue('memo');
            var trans_no = recordObject.getFieldValue('transactionnumber');
            var exp_internail_id = nlapiGetRecordId();

            var created_date = recordObject.getFieldValue('createddate');

            var emp_email = nlapiLookupField('employee', recordObject.getFieldValue('entity'), 'email');

            var currency = recordObject.getFieldText('basecurrency');

            var exp_status = recordObject.getFieldValue('status');

            var total_amount = recordObject.getFieldValue('total');

            var total_non_reimbursable = recordObject.getFieldValue('nonreimbursable');

            var total_due = recordObject.getFieldValue('amount');

            var internal_id = recordObject.getId();

            expense.project = null;
            expense.purpose = purpose;
            expense.expId = trans_no;
            expense.complete = true;
            expense.emailId = emp_email;
            expense.currency = currency;
            expense.status = exp_status;
            expense.total = total_amount;
            expense.totalNonReimbursable = total_non_reimbursable;
            expense.totalDue = total_due;
            expense.internalId = internal_id;
            expense.netsuiteCreatedDate = created_date;



            var lineItemCount = recordObject.getLineItemCount('expense');
            var getItem = [];
            

            for (var line = 1; line <= lineItemCount; line++) {


                var img = '';
                var data = '';
                var file = '';
                var data_str = '';
                var file_name = '';
                var file_type = '';
                var isReimbursable = '';
                var projectId = '';
                var memo = '';
                var billable = '';
                img = recordObject.getLineItemValue('expense', 'expmediaitem', line);
                if (img) {
                    file = nlapiLoadFile(img);
                    file_name = file.name;
                    file_type = file.type;
                    data = file.getValue();
                    data_str = JSON.stringify(data);
                    nlapiLogExecution('debug', 'Img data', 'line:' + line + ',' + 'data:' + data);
                }

                isReimbursable = recordObject.getLineItemValue('expense',
                    'isnonreimbursable', line);
                projectId = recordObject.getLineItemValue('expense',
                    'customer', line); //project internal id

                var amount_line_f = recordObject.getLineItemValue('expense',
                    'foreignamount', line);
                amount_line_f = parseFloat(amount_line_f).toFixed(1);
                billable = recordObject.getLineItemValue(
                        'expense', 'isbillable', line),

                billable = (billable == 'T') ? true : false;
                isReimbursable = (isReimbursable == 'T') ? true : false;
                // if (isFalse(isReimbursable)) {



                getItem.push({

                    "refNo": recordObject.getLineItemValue('expense',
                        'refnumber', line),
                    "refId": file_name || '',
                    "amount": recordObject.getLineItemValue('expense',
                        'foreignamount', line) || "",
                    "date": recordObject.getLineItemValue('expense',
                        'expensedate', line) || "",
                    "memo": recordObject.getLineItemValue('expense',
                        'memo', line) || "",
                    "currency": recordObject.getLineItemText('expense',
                        'currency', line) || "",
                    "projectId": projectId || "",
                    "project": recordObject.getLineItemText('expense',
                        'customer', line) || "",
                    "category": recordObject.getLineItemValue('expense',
                        'category_display', line) || "",


                    "imgType": file_type || '',

                    "imgData": data || '',

                    "isBillable": billable,
                    "isReimbursable": isReimbursable,
                    "isEdit": false,
                    "isDelete": false,
                    "isNew": true

                });

            }
            
            expense.Items = getItem;


            //**************Authentication steps******************//
            var commonHeader = {
            		"Content-Type": "application/json",
           		    "accept": "application/json"
            };
            // **************************************************//
           /* var registerURL = 'https://onthegoactivitylog.azurewebsites.net/onthego-v2-payrollmanagement/register';
            var registerMethod = 'POST';
            var registerBody = {
            		 "userName":"ankit", 
            		 "email":"ankit@brillio.com", 
            		 "password":"ankit" 
            };
            var registerResponse = nlapiRequestURL(registerURL, JSON.stringify(registerBody), commonHeader, null, registerMethod);
            nlapiLogExecution("DEBUG", "registerResponse Body :", (registerResponse.body)); //JSON.stringify
*/           
            //************************Token Generation steps*****************************//
            var tokenURL = 'https://onthegoactivitylog.azurewebsites.net/onthego-v2-payrollmanagement/authenticate';
            var tokenMethod = 'POST';
            var tokenBody = {
            		 "username":"ankit",
            		 "password":"ankit" 

            };
            var tokenResponse = nlapiRequestURL(tokenURL, JSON.stringify(tokenBody), commonHeader, null, tokenMethod);
            nlapiLogExecution("DEBUG", "tokenResponse Body :", (tokenResponse.body)); //JSON.stringify
            var tken = JSON.parse(tokenResponse.body);
          //  nlapiLogExecution("DEBUG", "tokenResponse token :", (tken.token)); //JSON.stringify
            //********************END***********************************//
           



            //**************Authentication********************************//
            var dataRow = expense;
            nlapiLogExecution("DEBUG", "body create Mode: ", JSON.stringify(dataRow));
            var method = "POST";
            if(_logValidation(oldRecord.getFieldValue('status'))){
            	nlapiLogExecution("DEBUG", "Update: ", 'update');
            	var url = "https://onthegoactivitylog.azurewebsites.net/onthego-v2-payrollmanagement/api/expense?apitype=1002"; //Update
            }
            else{
            	nlapiLogExecution("DEBUG", "Create: ", 'Create');
            var url = "https://onthegoactivitylog.azurewebsites.net/onthego-v2-payrollmanagement/api/expense?apitype=1001";	//Create
            }
            var headers = {
                	"Authorization": "Bearer "+tken.token,
                    "x-api-key": "615679DD-ABEA-48CE-B916-28974A309F77",
                    "x-api-customer-key": "98F78573-3172-4A8D-A521-3DC619AF2BE1",
                    "x-api-token-key": "8D660F3B-7CA9-47D0-848A-E8D7BE1B2786",
                    "x.api.user": "113056",
                    "content-type": "application/json",
                    "Accept": "application/json"

                };

            var JSONBody = JSON.stringify(dataRow, replacer);

            var response = nlapiRequestURL(url, JSONBody, headers, method);
            nlapiLogExecution("DEBUG", "response Body ", JSON.stringify(response.body));

            //************************END*****************************// 	



            if (!(parseInt(JSON.parse(response.getCode())) == 200)) {
                var a_emp_attachment = new Array();
                a_emp_attachment['entity'] = 83134;
                // send Email to fuel
                // send Email to fuel
                var file = nlapiCreateFile("payload.json", "PLAINTEXT", JSONBody);
                var emailSub = "Error in Expense Updation #" + nlapiGetRecordId(); //onthego
                var emailBoody = "Team," + "\r\n" + "Error while updating the expense details into OTG server #" + nlapiGetRecordId() + "\r\n" + "\r\n" + 'Employee :' + recordObject.getFieldValue('entity') + "\r\n" + "\r\n" + response.body + "\r\n" + "\r\n" + "Please find attached payLoad." + "\r\n";
                nlapiSendEmail(442, "onthego@BRILLIO.COM", emailSub, emailBoody, 'information.systems@brillio.com', null, a_emp_attachment, file);
            }
        }
        }
    } catch (err) {
    	var a_emp_attachment = new Array();
		a_emp_attachment['entity'] = 83134;
		 var emailSub = "Error in Expense - NS Script Error #" + nlapiGetRecordId(); //onthego
		 var emailBoody = "Team," + "\r\n" + "Error while sending the expense details into OTG server #" + nlapiGetRecordId() + "\r\n" + "\r\n" +  'Employee :'+recordObject.getFieldValue('entity')  + "\r\n" + "\r\n" + "Please find below NS error." + err + "\r\n";
        nlapiSendEmail(442, "onthego@BRILLIO.COM", emailSub, emailBoody, 'information.systems@brillio.com', null, a_emp_attachment);
        nlapiLogExecution('DEBUG', 'Expense Notification Error - OTG', err);
    }
}

function replacer(key, value) {
    if (typeof value == "number" && !isFinite(value)) {
        return String(value);
    }
    return value;
}

function _logValidation(value) {
    if (value != null && value.toString() != null && value != '' && value != undefined && value.toString() != undefined && value != 'undefined' && value.toString() != 'undefined' && value.toString() != 'NaN' && value != NaN) {
        return true;
    } else {
        return false;
    }
}