/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       23 Nov 2016     deepak.srinivas
 *
 */

/**
 * @param {String} type Context Types: scheduled, ondemand, userinterface, aborted, skipped
 * @returns {Void}
 */
function scheduled_Project_Trigger(type) {
try{
	//GET Current Date
	var today = nlapiDateToString(new Date());
	var intID = Array();
	intID[0] = '58897';
	intID[1] = '56074';
	var filters = Array();
	filters.push(new nlobjSearchFilter('status', null, 'is', 4)); //pending
	filters.push(new nlobjSearchFilter('jobtype', null, 'is', 2));
	filters.push(new nlobjSearchFilter('internalid', null, 'anyof', intID));
	filters.push(new nlobjSearchFilter('enddate', null, 'notbefore', today));
	

	var cols = Array();
	//cols.push(new nlobjSearchColumn('resource','resourceallocation')); 
	cols.push(new nlobjSearchColumn('entityid')); //ID
	cols.push(new nlobjSearchColumn('companyname'));  //Name
	cols.push(new nlobjSearchColumn('startdate')); 
	cols.push(new nlobjSearchColumn('enddate')); 
	cols.push(new nlobjSearchColumn('datecreated')); 
	cols.push(new nlobjSearchColumn('status')); 
	cols.push(new nlobjSearchColumn('custentity_clientpartner')); 
	cols.push(new nlobjSearchColumn('custentity_projectmanager')); 
	cols.push(new nlobjSearchColumn('custentity_deliverymanager')); 
	cols.push(new nlobjSearchColumn('custentity_region')); 
	cols.push(new nlobjSearchColumn('customer'));

	var projectSearchObj = nlapiSearchRecord('job',null,filters,cols);
	var JsonCategory = {};
	var dataRows = [];
	if(projectSearchObj){
		nlapiLogExecution('DEBUG','projectSearchObj',projectSearchObj.length);
		for(var i=0;i<projectSearchObj.length;i++){
			JsonCategory ={
					ID 	 : projectSearchObj[i].getValue('entityid'),
					Name : projectSearchObj[i].getValue('companyname'),
					StartDate : projectSearchObj[i].getValue('startdate'),
					EndDate : projectSearchObj[i].getValue('enddate'),
					CreatedDate : projectSearchObj[i].getValue('datecreated'),
					Status : projectSearchObj[i].getText('status'),
					CP : projectSearchObj[i].getValue('custentity_clientpartner'),
					PM : projectSearchObj[i].getValue('custentity_projectmanager'),
					DM : projectSearchObj[i].getValue('custentity_deliverymanager'),
					Region : projectSearchObj[i].getValue('custentity_region'),
					Customer : projectSearchObj[i].getValue('customer')
				
					}
			dataRows.push(JsonCategory);
			
		}
		sendEmail(dataRows);
	}
	
}
catch(e){
	nlapiLogExecution('DEBUG','Scheduled pending project trigger Error',e);
}
}

function sendEmail(dataRows){
	try{
		var boOps_Person = '';
		var manager_name = '';
		var dataRow = '';
		var pm_ = '';
		var dm_ = '';
		var cp_ = '';
		if(dataRows){
			for(var j=0 ; j<dataRows.length ; j++){
				//Reset Variable Values
				var boOps_Person = '';
				var manager_name = '';
				var dataRow = '';
				var pm_ = '';
				var dm_ = '';
				var cp_ = '';
				var pm_email_id = '';
				var pm_name = '';
				var dm_email_id =  '';
				var dm_name = '';
				var cp_email_id = '';
				var projectID = '';
				var projectName = '';
				var stDate = '';
				var endDate = '';
				var CreatedDate =  '';
				var Status = '';
				var Region = '';
				var buOPS_Email = '';
				var recipient = '';
				var subject = '';
				var body = ''; 
				var cust_lookup = '';
				var cust_name = '';
				var cust_id = '';
				
				dataRow = dataRows[j];
				
				 pm_ = dataRow.PM;
				 dm_ = dataRow.DM;
				 cp_ = dataRow.CP;
				 cust_id = dataRow.Customer;
				 if(cust_id){
					 cust_lookup = nlapiLookupField('customer',parseInt(cust_id),['altname']);
					 cust_name = cust_lookup.altname;
				 }
				if(pm_){
					pm_email_id = getEmployeeEmailID(pm_);
					pm_name = getEmployeeName(pm_);
				}
				if(dm_){
					dm_email_id = getEmployeeEmailID(dm_);
					dm_name = getEmployeeName(dm_);
				}
				if(cp_){
					cp_email_id = getEmployeeEmailID(cp_);
				}
				if(dm_name == pm_name){
					manager_name = dm_name;
				}
				else{
				 manager_name = pm_name + '/' + dm_name;
				}
				projectID = dataRow.ID;
				projectName = dataRow.Name;
				stDate = dataRow.StartDate;
				endDate = dataRow.EndDate;
				CreatedDate = dataRow.CreatedDate;
				Status = dataRow.Status;
				Region = dataRow.Region;
				
				if(Region){
				buOPS_Email = getEmailsBO(Region);
				}
				//Mail Content
				
				// Table for onsite unbilled bifurcation only
				var project_trigger_body = '';
				project_trigger_body += '<html>';
				project_trigger_body += '<body>';
				
				project_trigger_body += '<p>Hi ' + manager_name + ',</p>';
				
				project_trigger_body += "<p>This project is in pending status in NetSuite due to any or all of the following Issues:<br/> "
					                    +"1.Executed original SOW and/or PO not sent to BO<br/>"
					                    +"2.Executed copy of SOW amendment/CR not sent to BO.<br/></p>";
				
				project_trigger_body += "<p>Request you to kindly share the pending document with Business Operations team at the earliest. Thanks!</p>";

				//project_trigger_body += '<table width="100%" border="1">';
				var project_trigger_body_table = '';
				project_trigger_body_table = '<table width="100%" border="1">';
				project_trigger_body_table += '	<tr>';
				project_trigger_body_table += ' <td width="16%" font-size="11" align="center" bgcolor="#404040"><font color="#ffffff">Project ID</font></td>';
				project_trigger_body_table += ' <td width="26%" font-size="11" align="center" bgcolor="#404040"><font color="#ffffff">Project Name</font></td>';
				project_trigger_body_table += ' <td width="10%" font-size="11" align="center" bgcolor="#404040"><font color="#ffffff">Project Start Date</font></td>';
				project_trigger_body_table += ' <td width="10%" font-size="11" align="center" bgcolor="#404040"><font color="#ffffff">Project End Date</font></td>';
				project_trigger_body_table += ' <td width="10%" font-size="11" align="center" bgcolor="#404040"><font color="#ffffff">Project Status</font></td>';
				project_trigger_body_table += ' <td width="16%" font-size="11" align="center" bgcolor="#404040"><font color="#ffffff">Project creation Date</font></td>';
				
				project_trigger_body_table += '	</tr>';
				
				
						
				project_trigger_body_table += '	<tr>';
				project_trigger_body_table += ' <td width="16%" font-size="11" align="center" >'+ projectID +'</td>';
				project_trigger_body_table += ' <td width="26%" font-size="11" align="center">'+ projectName +'</td>';
				project_trigger_body_table += ' <td width="10%" font-size="11" align="center">'+ stDate +'</td>';
				project_trigger_body_table += ' <td width="10%" font-size="11" align="center">'+ endDate +'</td>';
				project_trigger_body_table += ' <td width="10%" font-size="11" align="center">'+ Status +'</td>';
				project_trigger_body_table += ' <td width="16%" font-size="11" align="center">'+ CreatedDate +'</td>';
				
				project_trigger_body_table += '	</tr>';
						
					
			
				project_trigger_body_table += '</table>';
				project_trigger_body += project_trigger_body_table;
				//project_trigger_body += '<p>**For any discrepancy in data, please reach out to business.ops@brillio.com or chetan.barot@brillio.com </p>';	
				project_trigger_body += '<p>Regards,<br/>';
				project_trigger_body += 'Information Systems</p>';
				project_trigger_body += '</body>';
				project_trigger_body += '</html>';
				
				//Emails List
				var author = 442;
				var recipient_list = new Array();
				recipient_list[0] = 'manikandan.v@brillio.com';
				//recipient_list[1] = pm_email_id;
				//recipient_list[2] = dm_email_id;
				nlapiLogExecution('debug','Mail Sent to User','cust_name ->'+cust_name);
				nlapiLogExecution('debug','Mail Sent to User','projectID ->'+projectID);
				nlapiLogExecution('debug','Mail Sent to User','projectName ->'+projectName);
				subject = cust_name +' - '+projectID +' - '+ projectName +' - '+'Pending Status';
				body = project_trigger_body;
				var  cc = ['information.systems@brillio.com','manikandan.v@brillio.com'];
				var cc_list = new Array();
				
				
				cc_list[0] = 'information.systems@brillio.com';
				//cc_list[1] = buOPS_Email;
				//cc_list[2] = cp_email_id;
				
				//Sends Email with Data
				nlapiSendEmail(author, recipient_list, subject, body, cc_list, 'deepak.srinivas@brillio.com', null);
				nlapiLogExecution('debug','Mail Sent to User','buOPS_Email ->'+buOPS_Email);
				nlapiLogExecution('debug','Mail Sent to User','Region ->'+Region);
				nlapiLogExecution('debug','Mail Sent to User','USER ->'+recipient);
			}
		}
	}
	catch(e){
		nlapiLogExecution('DEBUG','Project Trigger Send Email Error',e);
	}
}
function getEmployeeEmailID(id){
	try{
		var emp_lookup = nlapiLookupField('employee',parseInt(id),['email'])
		var emailID = emp_lookup.email;
		
		return emailID;
	}
	catch(e){
		nlapiLogExecution('DEBUG','Lookup Error',e);
	}
}

function getEmployeeName(id){
	try{
		var emp_lookup = nlapiLookupField('employee',parseInt(id),['firstname'])
		var firstname = emp_lookup.firstname;
		
		return firstname;
	}
	catch(e){
		nlapiLogExecution('DEBUG','Lookup Error',e);
	}
}

function getEmailsBO(region){
	try{
		var filters = Array();
		filters.push(new nlobjSearchFilter('custrecord_project_trigger_region', null, 'anyof', region))
		
		var cols = Array();
		cols.push(new nlobjSearchColumn('custrecord_project_trigger_email'));
		
		var searchObj = nlapiSearchRecord('customrecord_project_trigger_email',null,filters,cols);
		
		var email = searchObj[0].getValue('custrecord_project_trigger_email');
		
		return email;
		//customrecord_project_trigger_email
	}
	catch(e){
		nlapiLogExecution('DEBUG','Project Trigger Email Region Search Error',e);
	}
}
