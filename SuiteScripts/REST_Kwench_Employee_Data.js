function postRestlet(dataIn)
{
	try
	{
		var requestType = 'GET'; //Request Type hardcoded in script itself
		
		var response = new Response()
		nlapiLogExecution('DEBUG','Request',requestType);
		
		switch (requestType) {

			case M_Constants.Request.Get:
				
				response.Data = getActiveEmployeeDetails(); //Function Call to get the employee data
				response.Status = true;
				break;
		}	
	}
	
	catch (err)
	{
		nlapiLogExecution('Audit','Error', err);
		response.Status = false;
		response.Data = err
	}
	
	return response;
}

function getActiveEmployeeDetails()
{
	try
	{
		var EmployeeJSONData = []; //For Employee data
		var PracticeJSON = []; //For Department data
		var PracticeList = [];//For Parent Practice Head
		var AllocationData = []; //For getting project id, customer and customer region
		var AllocationList = []; //For indexing
		var practice_head_email = '';
		
		//Parent department search
		var departmentSearch = nlapiSearchRecord("department",null,
		[
			["isinactive","is","F"], 
			"AND", 
			["formulatext: CASE WHEN {name} = {custrecord_parent_practice}  THEN '1' ELSE '0' END","is","1"]
		], 
		[
			new nlobjSearchColumn("name").setSort(false), 
			new nlobjSearchColumn("email","CUSTRECORD_PRACTICEHEAD",null)
		]
		);
		
		for(var prc_count = 0 ; prc_count < departmentSearch.length; prc_count++)
		{
			PracticeJSON.push({
				ID : departmentSearch[prc_count].getId(),
				Name : departmentSearch[prc_count].getValue("name"),
				Practice_Head_Email : departmentSearch[prc_count].getValue("email","CUSTRECORD_PRACTICEHEAD")
			})
			
			PracticeList.push(departmentSearch[prc_count].getId())
		}
		
		
		var resourceallocationSearch = searchRecord("resourceallocation",null,
		[
			["startdate","notafter","today"], 
			"AND", 
			["enddate","notbefore","today"], 
			"AND", 
			["max(percentoftime)","isnotempty",""]
		], 
		[
			new nlobjSearchColumn("resource",null,"GROUP").setSort(false),
			new nlobjSearchColumn("customer",null,"GROUP"), 
			new nlobjSearchColumn("custentity_region","customer","GROUP"),
			new nlobjSearchColumn("company",null,"MAX")
		
		]
		);
		
		for(var allo_count = 0 ; allo_count < resourceallocationSearch.length; allo_count++)
		{
			AllocationData.push({
				customer : resourceallocationSearch[allo_count].getText("customer",null,"GROUP"),
				project : resourceallocationSearch[allo_count].getValue("company",null,"MAX"),
				region : resourceallocationSearch[allo_count].getText("custentity_region","customer","GROUP")
			})
			
			AllocationList.push(resourceallocationSearch[allo_count].getValue("resource",null,"GROUP"))
		}
		
		
		//Employee search
		var employeeSearch = searchRecord("employee",null,
		[
			["custentity_employee_inactive","is","F"], 
			"AND", 
			["custentity_implementationteam","is","F"], 
			"AND", 
			["custentity_employeetype","noneof","3"]
		], 
		
		[
			new nlobjSearchColumn("firstname"), 
			new nlobjSearchColumn("lastname"), 
			new nlobjSearchColumn("email"), 
			new nlobjSearchColumn("custentity_personalemailid"), 
			new nlobjSearchColumn("mobilephone"), 
			new nlobjSearchColumn("custentity_fusion_empid"), 
			new nlobjSearchColumn("title"), 
			new nlobjSearchColumn("departmentnohierarchy"), 
			new nlobjSearchColumn("gender"), 
			new nlobjSearchColumn("birthdate"), 
			new nlobjSearchColumn("custentity_actual_hire_date"), 
			new nlobjSearchColumn("custentity_fusion_empid","CUSTENTITY_REPORTINGMANAGER",null), 
			new nlobjSearchColumn("locationnohierarchy"), 
			new nlobjSearchColumn("custrecord_parent_practice","department",null), 
			new nlobjSearchColumn("subsidiarynohierarchy"),
			new nlobjSearchColumn("internalid"),
			new nlobjSearchColumn("custentity_employeetype")
		]
		);
		
		if(_logValidation(employeeSearch))
		{
			for(var counter = 0; counter < employeeSearch.length; counter++)
			{
				var parent_practice_id = employeeSearch[counter].getValue('custrecord_parent_practice','department');
				
				var practice_index = PracticeList.indexOf(parent_practice_id);
				
				var resource_index = AllocationList.indexOf(employeeSearch[counter].getId());
				
				EmployeeJSONData.push({
					first_name : employeeSearch[counter].getValue('firstname'),
					last_name : employeeSearch[counter].getValue('lastname'),
					email_id : employeeSearch[counter].getValue('email'),
					//mobile_number : employeeSearch[counter].getValue('mobilephone'),
					employee_id : employeeSearch[counter].getValue('custentity_fusion_empid'),
					designation : employeeSearch[counter].getValue('title'),
					department : employeeSearch[counter].getText('departmentnohierarchy'),
					date_of_birth : employeeSearch[counter].getValue('birthdate'),
					date_of_joining : employeeSearch[counter].getValue('custentity_actual_hire_date'),
					reporting_manager_emp_id : employeeSearch[counter].getValue('custentity_fusion_empid','CUSTENTITY_REPORTINGMANAGER'),
					location : employeeSearch[counter].getText('locationnohierarchy'),
					employee_type : employeeSearch[counter].getText('custentity_employeetype'),
					parent_practice_email: (practice_index >= 0) ? PracticeJSON[practice_index].Practice_Head_Email : '',//updated the logic index should start from zero also by shravan
					parent_practice : (practice_index >= 0) ? PracticeJSON[practice_index].Name : '',//updated the logic index should start from zero also by shravan
					legal_entity : employeeSearch[counter].getText('subsidiarynohierarchy'),
					Project : (resource_index >= 0) ? AllocationData[resource_index].project : '',
					//Customer : (resource_index > 0) ? AllocationData[resource_index].customer : '',
					Region : (resource_index >= 0) ? AllocationData[resource_index].region : '',
					
					
				})
				//nlapiLogExecution('Debug','Count',counter);
			}
			
			nlapiLogExecution('Debug','JSON Response',JSON.stringify(EmployeeJSONData));
			
			//MasterJSON = {
			//	EmployeeData : EmployeeJSONData,
			//	PracticeData : PracticeJSON
			//};
			
			return EmployeeJSONData;
		}
	}
	
	catch (err)
	{
		nlapiLogExecution('Audit','Error in fetching employee data',err);
		throw err;
	}
}

function _logValidation(value) {
    if (value != null && value.toString() != null && value != '' && value != undefined && value.toString() != undefined && value != 'undefined' && value.toString() != 'undefined' && value.toString() != 'NaN' && value != NaN) {
        return true;
    } else {
        return false;
    }
}