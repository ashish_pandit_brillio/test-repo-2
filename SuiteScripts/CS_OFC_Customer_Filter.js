/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       14 Sep 2016     deepak.srinivas
 *
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType
 * 
 * @param {String} type Sublist internal id
 * @param {String} name Field internal id
 * @param {Number} linenum Optional line item number, starts from 1
 * @returns {Void}
 */
function fieldChanged(type, name)
{
	try{
	if (name == 'custrecord_customer_ofc')
	{
		//nlapiSetFieldValue('custrecord_project_ofc','', false, true);
		var customer = nlapiGetFieldValues('custrecord_customer_ofc');
		nlapiLogExecution('DEBUG','Customer Val',customer);
		
		var filters = Array();
		filters.push(new nlobjSearchFilter('customer',null,'anyof',customer));
		filters.push(new nlobjSearchFilter('status',null,'anyof',['2','4']));
		
		var cols =  Array();
		cols.push(new nlobjSearchColumn('internalid'));
		cols.push(new nlobjSearchColumn('entityid'));
		cols.push(new nlobjSearchColumn('altname'));
		
		var dataObj = {};
		var dataRow = [];
		var projectArray = [];
		var searchRes = nlapiSearchRecord('job',null,filters,cols);
		nlapiLogExecution('DEBUG','Customer searchRes',searchRes.length);
		if(searchRes){
			for(var j=0;j<searchRes.length;j++){
				var id = searchRes[j].getValue('internalid');
				var entityid = searchRes[j].getValue('entityid');
				var name = searchRes[j].getValue('altname');
				
				dataObj = {
						ID: id,
						ProjectID: entityid,
						ProjectName: name
				};
				dataRow.push(dataObj);
			}
		}
		if(dataRow){
		for(var k=0;k<dataRow.length;k++){
			projectDetails = dataRow[k];
			projectArray.push(projectDetails.ID);
		}
		}
		nlapiSetFieldValues('custrecord_project_ofc',projectArray);
		return true;
		
		
	}
}
	catch(e){
		nlapiLogExecution('DEBUG','Client Script Error',e);
	}
}
