/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       13 Aug 2015     amol.sahijwani
 *
 */

/**
 * @param {nlobjRequest} request Request object
 * @param {nlobjResponse} response Response object
 * @returns {Void} Any output is written via response object
 */

function suitelet(request, response) {

	var method = request.getMethod();

	var s_step_1 = 'step-1';
	var s_step_2 = 'step-2';
	var s_step_3 = 'step-3';
	var s_step_approve = 'approve';

	// Get logged-in user
	var objUser = nlapiGetContext();

	var i_employee_id = objUser.getUser();

	var s_role = getRoleName(i_employee_id, objUser.getRole());

	var s_listing_page_url = nlapiResolveURL('SUITELET', 'customscript_sut_tr_approver_screen', 'customdeploy1');

	var s_new_request_url = nlapiResolveURL('SUITELET', 'customscript_sut_travel_req_details_form', 'customdeploy1');

	var o_employee_data = nlapiLookupField('employee', i_employee_id, ['firstname', 'lastname', 'employeestatus', 'subsidiary']);
	var o_employee_data_subsidiary = nlapiLookupField('employee', i_employee_id, 'subsidiary', true);
	var status = 1;	// Used to display success or failure at top message box
	var message = request.getParameter('custparam_message'); // Display the message at the top

	var i_tr_id = request.getParameter('custparam_tr_id');	// TR id

	var i_tri_id = request.getParameter('custparam_tri_id'); // Travel Segment ID(for edit mode)

	var i_file_id = request.getParameter('custparam_file_id');

	var mode = request.getParameter('custparam_mode'); // Edit mode for editing the segment or travel details

	var s_next_step = request.getParameter('custparam_next_step');

	var s_current_step = s_next_step == null ? s_step_1 : s_next_step;

	if (mode == s_step_approve) {
		s_current_step = s_step_3;
	}
	else if (mode == 'edit_header') {
		s_current_step = s_step_1;
	}
	else if (mode == 'edit') {
		s_current_step = s_step_2;
	}

	var values = new Object();

	values['username'] = o_employee_data.firstname + ' ' + o_employee_data.lastname;
	values['emp_subsidiary'] = o_employee_data_subsidiary;
	values['role'] = s_role;
	values['new_request_url'] = s_new_request_url;
	values['my_requests_url'] = s_listing_page_url + '&mode=my-requests';
	values['approve_requests_url'] = s_listing_page_url;
	values['step_1'] = s_step_1;
	values['step_2'] = s_step_2;
	values['step_3'] = s_step_3;

	values['enter-tr'] = 'hide';
	values['edit-tr'] = 'hide';
	values['preview_and_submit'] = 'hide';
	values['display-travel-summary'] = 'none';
	values['travel-segments-preview-show-hide'] = 'hide';
	values['approval-log-show-hide'] = 'hide';
	values['attachments-show-hide'] = 'hide';
	values['approval-show-hide'] = 'hide';

	// Travel Segment Display Options
	if (mode == 'edit') {
		values['tri-id'] = i_tri_id;
		values['add-edit-segment-heading'] = '<span class="glyphicon glyphicon-pencil"></span> Edit';
		values['add-edit-segment'] = 'Update';
		values['show-hide-cancel-button'] = '';
	}
	else {
		values['tri-id'] = '';
		values['add-edit-segment-heading'] = '<span class="glyphicon glyphicon-plus"></span> Add';
		values['add-edit-segment'] = 'Save';
		values['show-hide-cancel-button'] = 'hide';
	}

	var view_mode = false; // Allow editing by the user who entered the travel request

	var i_status = null;
	var i_approver_id = null;
	var i_travel_type = null;
	var i_visa_type = null;
	var i_tr_employee_id = null;

	if (i_tr_id != null && i_tr_id != '') {
		// Check the status(if editing is allowed)
		var trData = nlapiLookupField('customrecord_travel_request', i_tr_id, ['custrecord_tr_status', 'custrecord_tr_delivery_manager', 'custrecord_tr_employee', 'custrecord_tr_approving_manager_id', 'custrecord_tr_travel_type', 'custrecord_tr_visa_type']);

		i_travel_type = trData.custrecord_tr_travel_type == 1 ? K_DOMESTIC_INTERNATIONAL.DOMESTIC : K_DOMESTIC_INTERNATIONAL.INTERNATIONAL;
		i_visa_type = trData.custrecord_tr_visa_type;
		i_status = trData.custrecord_tr_status;
		i_approver_id = trData.custrecord_tr_approving_manager_id;
		i_tr_employee_id = trData.custrecord_tr_employee;

		view_mode = getViewMode(i_status, i_employee_id, i_tr_employee_id);

	}
	else {
		s_current_step = s_step_1;

		view_mode = false;

		mode = 'new_tr';

		//values['enter-tr']	=	'show';
		//values['edit-tr']	=	'hide';
	}

	if (method == 'POST') {
		// Check which button was clicked
		var s_button_clicked = '';

		if (request.getParameter('add_segment') != null && view_mode == false) {
			s_button_clicked = 'ADD_SEGMENT';
		}
		else if (request.getParameter('submit_travel_request') != null && view_mode == false) {
			s_button_clicked = 'SUBMIT_TRAVEL_REQUEST';
		}
		else if (request.getParameter('approve') != null) {
			s_button_clicked = 'APPROVE_TRAVEL_REQUEST';
		}
		else if (request.getParameter('reject') != null) {
			s_button_clicked = 'REJECT_TRAVEL_REQUEST';
		}
		else if (request.getParameter('save_tr') != null) {
			s_button_clicked = 'SAVE_TRAVEL_REQUEST';
		}
		else if (request.getParameter('edit_tr') != null && view_mode == false) {
			s_button_clicked = 'EDIT_TRAVEL_REQUEST';
		}
		else if (request.getParameter('file_upload') != null && i_status == K_STATUS.TRAVEL) {
			s_button_clicked = 'UPLOAD_ADDITIONAL_FILES';
		}
		else if (request.getParameter('previous_step') != null) {
			s_button_clicked = 'PREVIOUS_STEP';

			var s_previous_step = s_step_1;

			if (s_current_step == s_step_2) {
				s_previous_step = s_step_1;
			}

			//var s_url = nlapiSetRedirectURL('SUITELET', 'customscript_sut_travel_req_details_form', 'customdeploy1', false, {'custparam_tr_id':i_tr_id, 'custparam_next_step':s_previous_step});
			response.sendRedirect('SUITELET', 'customscript_sut_travel_req_details_form', 'customdeploy1', false, { 'custparam_tr_id': i_tr_id, 'custparam_next_step': s_previous_step });
			nlapiRequestURL('https://' + request.getHeader('Host') + '/' + s_url);
		}
		else {
			s_button_clicked = 'NO_PERMISSION';
		}

		var o_status = saveTravelRequestDetails(request, s_button_clicked, i_status, i_approver_id, i_tr_id, i_travel_type, i_visa_type);

		nlapiLogExecution('AUDIT', 'Button Clicked', s_button_clicked);

		if (o_status.status == 1) {
			message = o_status.message;

			if (s_button_clicked == 'SAVE_TRAVEL_REQUEST') {
				i_tr_id = o_status.tr_id;

				mode = '';

				s_current_step = s_step_2;

				response.sendRedirect('SUITELET', 'customscript_sut_travel_req_details_form', 'customdeploy1', false, { 'custparam_tr_id': i_tr_id, 'custparam_next_step': s_current_step, 'custparam_message': o_status.message });
			}
			else if (s_button_clicked == 'EDIT_TRAVEL_REQUEST') {
				mode = '';

				s_current_step = s_step_2;
			}
		}
		else {
			message = o_status.message;

			//values['edit-tr']	=	'hide';
			//values['enter-tr']	=	'show';
		}

		status = o_status.status;

	}
	else {
		var i_approver_role = getApproverDetails(i_status, i_approver_id);

		// Delete travel segment
		if (mode == 'delete' && view_mode == false) {
			o_status = deleteTravelRequestDetails(i_tri_id);

			message = o_status.message;
			status = o_status.status;
		}
		else if (mode == 'delete-file') {
			if (view_mode == false || i_approver_role == K_TRAVEL_APPROVER_ROLE.TRAVEL) {
				o_status = deleteTravelRequestFile(i_file_id, i_tr_id);

				message = o_status.message;
				status = o_status.status;
			}
			else {
				message = 'Permission Denied';
				status = -1;
			}
		}
	}

	var file = nlapiLoadFile(57170); //load the HTML file
	var contents = file.getValue(); //get the contents

	if (message == null) {
		values['message-display'] = 'display:none;';
	}
	else {
		values['message'] = message;
		if (status == 1) {
			values['message-status'] = 'success';
		}
		else {
			values['message-status'] = 'danger';

			nlapiLogExecution('ERROR', 'Error: ', message);
		}
	}

	var recTR = null;

	if (mode != 'new_tr') {
		// Load the data and populate it
		recTR = nlapiLoadRecord('customrecord_travel_request', i_tr_id);

		var o_json = getCustomerProjectJSON();

		values['customer-project-json'] = JSON.stringify(o_json);

		// Display Input form
		values['header-data-input'] = displayHeaderForm(mode, recTR, i_tr_id, s_step_2, o_json);

		i_status = recTR.getFieldValue('custrecord_tr_status');

		view_mode = getViewMode(i_status, i_employee_id, recTR.getFieldValue('custrecord_tr_employee'));

		var i_approver_role = getApproverDetails(i_status, i_approver_id);

		if (view_mode == true) {
			values['display'] = 'none';

			if (i_approver_role != K_TRAVEL_APPROVER_ROLE.TRAVEL) {
				values['allow-file-delete'] = 'none';
			}
			else {
				values['allow-file-delete'] = '';
			}
		}

		if (i_approver_role != -1) {
			values['approval-show-hide'] = 'show';

			if (i_status == K_STATUS.TRAVEL) {
				values['display-additional-file-upload'] = '';
			}
			else {
				values['display-additional-file-upload'] = 'none';
			}
		}
		else {
			values['approval-show-hide'] = 'hide';

			values['display-additional-file-upload'] = 'none';
		}

		// Attachments
		var strAttachmentTableHtml = '<thead><tr><th>View</th><th>Filename</th><th>Description</th><th>File Type</th><th class="file-delete-mode">Delete</th></tr></thead>';

		var a_attachment_list = new Array();

		var attachment_filters = new Array();
		attachment_filters[0] = new nlobjSearchFilter('internalid', null, 'anyof', i_tr_id);
		attachment_filters[1] = new nlobjSearchFilter('internalidnumber', 'file', 'isnotempty');

		var attachment_columns = new Array();
		attachment_columns[0] = new nlobjSearchColumn('description', 'file');
		attachment_columns[1] = new nlobjSearchColumn('filetype', 'file');
		attachment_columns[2] = new nlobjSearchColumn('internalid', 'file');
		attachment_columns[3] = new nlobjSearchColumn('url', 'file');
		attachment_columns[4] = new nlobjSearchColumn('name', 'file');
		attachment_columns[5] = new nlobjSearchColumn('internalid', 'file');

		var search_attachments = nlapiSearchRecord('customrecord_travel_request', null, attachment_filters, attachment_columns);

		var s_select_files_for_email = '';

		for (var i = 0; search_attachments != null && i < search_attachments.length; i++) {
			var i_file_id = search_attachments[i].getValue(attachment_columns[2]);
			var s_url = search_attachments[i].getValue(attachment_columns[3]);

			a_attachment_list.push({ 'display': search_attachments[i].getValue(attachment_columns[4]), 'value': search_attachments[i].getValue(attachment_columns[5]) });

			strAttachmentTableHtml += '<tr>';
			strAttachmentTableHtml += '<td><a href="' + s_url + '" target="_blank">View</a></td>';
			strAttachmentTableHtml += '<td>' + search_attachments[i].getValue(attachment_columns[4]) + '</td>';
			strAttachmentTableHtml += '<td>' + search_attachments[i].getValue(attachment_columns[0]) + '</td>';
			strAttachmentTableHtml += '<td>' + search_attachments[i].getValue(attachment_columns[1]) + '</td>';
			strAttachmentTableHtml += '<td class="file-delete-mode">' + '<a href="' + nlapiResolveURL('SUITELET', 'customscript_sut_travel_req_details_form', 'customdeploy1') + '&custparam_tr_id=' + i_tr_id + '&custparam_file_id=' + i_file_id + '&custparam_next_step=' + s_current_step + '&custparam_mode=delete-file">Delete</a>';
			strAttachmentTableHtml += '</tr>';
		}

		values['attachments'] = search_attachments == null ? '<tr><td>No files attached.</td></tr>' : strAttachmentTableHtml;
		values['send-attachment-list'] = getChecklistToSendAttachment(a_attachment_list);

		if (i_status != K_STATUS.OPEN) {
			var s_read_only = (i_approver_role == 1) ? 'editable' : 'read-only';
			values['approver-checklist'] = getTravelRequestChecklist(recTR, s_read_only);

			if (s_read_only == 'read-only') {

				if (i_status != K_STATUS.PENDING_APPROVAL) {
					values['billable-items-show-hide'] = 'show';
				}
				else {
					values['billable-items-show-hide'] = 'hide';
				}

				values['billable-items-input-show-hide'] = 'hide';

				values['gt-required-fields'] = '';

				values['show-gt-fields'] = 'none';

			}
			else {

				values['billable-items-show-hide'] = 'hide';
				values['billable-items-input-show-hide'] = 'show';

				if (recTR.getFieldValue('custrecord_tr_travel_type') == K_DOMESTIC_INTERNATIONAL.INTERNATIONAL && recTR.getFieldValue('custrecord_tr_visa_type') == K_VISA_TYPE.WORK) {
					values['gt_timesheet_approver'] = getEmployeeList();
					values['gt_expense_approver'] = getEmployeeList();//ListValues('employee', null, '--Please Select--');

					values['gt-required-fields'] = 'required';

					values['show-gt-fields'] = '';
				}
				else {
					values['gt-required-fields'] = '';

					values['show-gt-fields'] = 'none';
				}

			}
		}
		else {
			values['billable-items-show-hide'] = 'hide';
			values['billable-items-input-show-hide'] = 'hide';

			values['gt-required-fields'] = '';

			values['show-gt-fields'] = 'none';
		}

		var o_approver = nlapiLookupField('employee', recTR.getFieldValue('custrecord_tr_approving_manager_id'), ['firstname', 'lastname']);

		var o_tr_employee_details = nlapiLookupField('employee', recTR.getFieldValue('custrecord_tr_employee'), ['employeestatus'], true);

		// TR Basic Details
		values['script-id'] = 659;
		values['tr-name'] = recTR.getFieldValue('name');
		values['status'] = recTR.getFieldText('custrecord_tr_status');
		values['employee-name'] = recTR.getFieldText('custrecord_tr_employee');
		values['project-name'] = recTR.getFieldText('custrecord_tr_project');
		values['domestic-international'] = recTR.getFieldText('custrecord_tr_travel_type');
		values['domestic-country'] = recTR.getFieldText('custrecord_tr_country');
		values['mobile-number'] = recTR.getFieldValue('custrecord_tr_mobile_number');
		values['request-type'] = recTR.getFieldText('custrecord_tr_request_type');
		values['purpose-of-travel'] = recTR.getFieldText('custrecord_tr_purpose_of_travel');
		values['travel-short-description'] = recTR.getFieldValue('custrecord_tr_short_description');
		values['work-address'] = recTR.getFieldValue('custrecord_tr_work_address');
		values['zip-code'] = recTR.getFieldValue('custrecord_tr_zip_code');
		values['onsite-contact-number'] = recTR.getFieldValue('custrecord_tr_onsite_contact_number');
		values['travel-advance-currency'] = recTR.getFieldText('custrecord_tr_advance_currency');
		values['visa-type'] = recTR.getFieldText('custrecord_tr_visa_type');
		values['visa-category'] = recTR.getFieldText('custrecord_tr_visa_category');
		values['forex-or-advance'] = recTR.getFieldValue('custrecord_tr_forex_or_advance');
		values['per-diem'] = recTR.getFieldText('custrecord_tr_per_diem');
		values['remarks'] = recTR.getFieldValue('custrecord_tr_remarks');
		values['delivery-manager'] = recTR.getFieldText('custrecord_tr_delivery_manager');
		values['sub-practice-head'] = recTR.getFieldText('custrecord_tr_sub_department_head');
		values['client-partner'] = nlapiLookupField('job', recTR.getFieldValue('custrecord_tr_project'), 'custentity_clientpartner', true);
		values['next-approver'] = o_approver.firstname + ' ' + o_approver.lastname;
		values['onsite-duration'] = recTR.getFieldValue('custrecord_tr_onsite_duration');
		values['employee-level'] = o_tr_employee_details.employeestatus;
		values['employee-subsidiary'] = nlapiLookupField('employee', recTR.getFieldValue('custrecord_tr_employee'), 'subsidiary', true);

		// GT fields
		values['gt-bill-rate'] = recTR.getFieldValue('custrecord_tr_gt_bill_rate');
		values['gt-allocation-start-date'] = recTR.getFieldValue('custrecord_tr_gt_allocation_start_date');
		values['gt-allocation-end-date'] = recTR.getFieldValue('custrecord_tr_gt_allocation_end_date');
		values['gt-timesheet-approver'] = recTR.getFieldText('custrecord_tr_gt_timesheet_approver');
		values['gt-expense-approver'] = recTR.getFieldText('custrecord_tr_gt_expense_approver');

		// Hide international fields if domestic
		if (recTR.getFieldValue('custrecord_tr_travel_type') == K_DOMESTIC_INTERNATIONAL.INTERNATIONAL) {
			values['show-international-fields'] = '';
		}
		else {
			values['show-international-fields'] = 'none';
		}

		// Travel Details
		var i_travel_details_count = recTR.getLineItemCount('recmachcustrecord_trli_travel_request');

		values['preview_link_disable'] = i_travel_details_count > 0 ? '' : 'not-active';  // used for validation

		var o_travel_details_heading = { 'highlight': '', 'edit': '<span class="glyphicon glyphicon-pencil"></span>', 'date': 'Departure Date', 'origin': 'Origin', 'destination': 'Destination', 'mode': 'Mode Of Travel', 'meal_seat': 'Meal/Seat Preference', 'hotel': 'Hotel Required', 'delete': '<span class="glyphicon glyphicon-trash"></span>' };

		var a_travel_details = new Array();

		for (var i = 1; i <= i_travel_details_count; i++) {
			a_travel_details[i - 1] = new Object();

			if (i_tri_id == recTR.getLineItemValue('recmachcustrecord_trli_travel_request', 'id', i)) {
				a_travel_details[i - 1]['highlight'] = 'info';
			}
			else {
				a_travel_details[i - 1]['highlight'] = '';
			}

			a_travel_details[i - 1]['edit'] = '<a href="' + nlapiResolveURL('SUITELET', 'customscript_sut_travel_req_details_form', 'customdeploy1') + '&custparam_tr_id=' + i_tr_id + '&custparam_tri_id=' + recTR.getLineItemValue('recmachcustrecord_trli_travel_request', 'id', i) + '&custparam_mode=edit&custparam_next_step=">Edit</a>';
			a_travel_details[i - 1]['date'] = convertToDateFormat1(recTR.getLineItemValue('recmachcustrecord_trli_travel_request', 'custrecord_trli_departure_date', i));
			a_travel_details[i - 1]['origin'] = nlapiLookupField('customrecord_tr_city_list', recTR.getLineItemValue('recmachcustrecord_trli_travel_request', 'custrecord_trli_origin_city', i), 'custrecord_trc_name');
			a_travel_details[i - 1]['destination'] = nlapiLookupField('customrecord_tr_city_list', recTR.getLineItemValue('recmachcustrecord_trli_travel_request', 'custrecord_trli_destination_city', i), 'custrecord_trc_name');
			a_travel_details[i - 1]['mode'] = recTR.getLineItemText('recmachcustrecord_trli_travel_request', 'custrecord_trli_mode_of_travel', i);
			a_travel_details[i - 1]['meal_seat'] = recTR.getLineItemValue('recmachcustrecord_trli_travel_request', 'custrecord_trli_meal_seat_preference', i);
			a_travel_details[i - 1]['hotel'] = recTR.getLineItemText('recmachcustrecord_trli_travel_request', 'custrecord_trli_hotel_required', i);
			a_travel_details[i - 1]['delete'] = '<a href="' + nlapiResolveURL('SUITELET', 'customscript_sut_travel_req_details_form', 'customdeploy1') + '&custparam_tr_id=' + i_tr_id + '&custparam_tri_id=' + recTR.getLineItemValue('recmachcustrecord_trli_travel_request', 'id', i) + '&custparam_mode=delete&custparam_next_step=' + s_current_step + '">Delete</a>';
		}

		var strTableHtml = displayTable(o_travel_details_heading, a_travel_details, 'datetime'); // '<thead><tr><th class="edit-mode">Edit</th><th>Departure Date</th><th>Origin</th><th>Destinatin</th><th>Mode of Travel</th><th>Meal/Seat Preference</th><th>Hotel Required</th><th class="edit-mode">Delete</th></tr></thead>';

		values['list'] = i_travel_details_count == 0 ? '<tr><td>Please enter travel segments</td></tr>' : strTableHtml;

		// Travel Approval Details
		var o_approval_status = new Object();
		if (i_status > 0) {
			o_approval_status['0'] = '1';
		}

		var i_travel_approval_count = recTR.getLineItemCount('recmachcustrecord_tral_id');

		for (var i = 1; i <= i_travel_approval_count; i++) {
			o_approval_status[recTR.getLineItemValue('recmachcustrecord_tral_id', 'custrecord_tral_approver_role', i)] = recTR.getLineItemValue('recmachcustrecord_tral_id', 'custrecord_tral_approver_action', i);
		}

		// Create Approval Log data array
		var a_approval_log = new Array();

		for (var i = 1; i <= i_travel_approval_count; i++) {
			a_approval_log[i - 1] = new Object();
			a_approval_log[i - 1]['date'] = nlapiStringToDate(recTR.getLineItemValue('recmachcustrecord_tral_id', 'custrecord_tral_date', i), 'datetimetz');
			a_approval_log[i - 1]['approved_by'] = recTR.getLineItemText('recmachcustrecord_tral_id', 'custrecord_tral_approved_by', i);
			a_approval_log[i - 1]['approver_role'] = recTR.getLineItemText('recmachcustrecord_tral_id', 'custrecord_tral_approver_role', i);
			a_approval_log[i - 1]['approver_action'] = recTR.getLineItemText('recmachcustrecord_tral_id', 'custrecord_tral_approver_action', i);
			a_approval_log[i - 1]['remarks'] = recTR.getLineItemValue('recmachcustrecord_tral_id', 'custrecord_tral_remarks', i);
		}

		// Display Approval Log table
		var o_headings = { 'date': 'Date', 'approved_by': 'User', 'approver_role': 'Role', 'approver_action': 'Action', 'remarks': 'Remarks' };

		a_approval_log.sort(dynamicSort('date'));

		var strTableHtml = displayTable(o_headings, a_approval_log, 'datetimetz');//'<thead><tr><th>Date</th><th>User</th><th>Role</th><th>Action</th><th>Remarks</th></tr></thead>';

		values['approval-log-show-hide'] = i_travel_approval_count == 0 ? 'hide' : 'show';
		values['approval-log'] = i_travel_approval_count == 0 ? '<tr><td>No approvals yet</td></tr>' : strTableHtml;

		// Get travel type
		var i_domestic_international = recTR.getFieldValue('custrecord_tr_travel_type') == 1 ? K_DOMESTIC_INTERNATIONAL.DOMESTIC : K_DOMESTIC_INTERNATIONAL.INTERNATIONAL;

		// Display Progress Tracker
		values['progress-bar'] = (i_status > 1) ? displayProgressBar(o_approval_status, i_status, i_domestic_international) : '';

		// Display Segment Form
		var entered_departure_date = '';
		var selected_mode_of_travel = '';
		var selected_hotel_required = '';
		var selected_origin = '';
		var selected_destination = '';
		var entered_meal_seat_preference = '';

		if (mode == 'edit' && i_tri_id != null) {
			var recTR_Item = nlapiLoadRecord('customrecord_tr_travel_request_line_item', i_tri_id);
			entered_departure_date = recTR_Item.getFieldValue('custrecord_trli_departure_date');
			selected_mode_of_travel = recTR_Item.getFieldValue('custrecord_trli_mode_of_travel');
			selected_hotel_required = recTR_Item.getFieldValue('custrecord_trli_hotel_required');
			selected_origin = recTR_Item.getFieldValue('custrecord_trli_origin_city');
			selected_destination = recTR_Item.getFieldValue('custrecord_trli_destination_city');
			entered_meal_seat_preference = recTR_Item.getFieldValue('custrecord_trli_meal_seat_preference');
		}

		// Add Segment Form Drop-downs
		values['mode-of-travel'] = getListValues('customlist_tr_mode_of_travel', selected_mode_of_travel, '', true);
		values['departure-date-value'] = convertToDateFormat1(entered_departure_date);
		values['hotel-required'] = getListValues('customlist_tr_hotel_required', selected_hotel_required, '--Please Select--');
		values['origin'] = getCities(recTR.getFieldValue('custrecord_tr_country'), selected_origin, i_domestic_international);
		values['destination'] = getCities(recTR.getFieldValue('custrecord_tr_country'), selected_destination, i_domestic_international);
		values['meal-seat-preference-value'] = entered_meal_seat_preference;
		values['tr-id'] = i_tr_id;
	}
	else {

		var o_json = getCustomerProjectJSON();
		//nlapiLogExecution('Debug', 'o_json', JSON.stringify(o_json));
		values['customer-project-json'] = JSON.stringify(o_json);

		// Display Input form
		values['header-data-input'] = displayHeaderForm(mode, recTR, i_tr_id, s_step_2, o_json);

	}

	// Display Sections based on Current Step

	if (s_current_step == s_step_1) {
		values['enter-tr'] = 'show';
	}
	else if (s_current_step == s_step_2) {
		values['edit-tr'] = 'show';
	}
	else if (s_current_step == s_step_3 || mode == 'approve') {
		values['preview_and_submit'] = 'show';
		values['display-travel-summary'] = '';
		values['travel-segments-preview-show-hide'] = 'show';
		values['approval-log-show-hide'] = 'show';
		values['attachments-show-hide'] = 'show';
	}

	contents = replaceValues(contents, values);
	response.write(contents); //render it on the suitelet

	var context = nlapiGetContext();
	nlapiLogExecution('AUDIT', 'Page: ' + s_current_step + ', Mode: ' + mode + ', Remaining Usage: ', context.getRemainingUsage());

}

function getViewMode(i_status, i_logged_in_user, i_tr_employee_id) {
	if (i_status == 1 && i_logged_in_user == i_tr_employee_id) {
		return false;
	}

	return true;
}

function getApproverDetails(i_status, i_approving_manager) {
	var i_approver_role = null;

	// Get logged-in user
	var objUser = nlapiGetContext();

	var i_employee_id = objUser.getUser();

	var i_role = objUser.getRoleId();

	if (i_status == 2 && i_employee_id == i_approving_manager) {
		i_approver_role = 1;
	}
	else if (i_status == 3 && K_ROLES.IMMIGRATION.indexOf(i_employee_id) != -1) {
		i_approver_role = 2;
	}
	else if ((i_status == 2 || i_status == 4) && K_ROLES.TRAVEL.indexOf(i_employee_id) != -1) {
		i_approver_role = 3;
	}
	else {
		i_approver_role = -1;
	}

	return i_approver_role;
}

function deleteTravelRequestFile(i_file_id, i_rec_id) {
	try {
		//nlapiDetachRecord('file', i_file_id, 'customrecord_travel_request', i_rec_id);

		nlapiDeleteFile(i_file_id);

		return ({ 'status': 1, 'message': 'File Deleted', 'file_id': i_file_id });
	}
	catch (e) {
		return ({ 'status': -1, 'message': 'Error: ' + e.message, 'tr_id': null });
	}
}

function saveTravelRequestDetails(request, s_button, i_status, i_approving_manager, i_tr_id, i_travel_type, i_visa_type) {
	if (s_button == 'SUBMIT_TRAVEL_REQUEST') {
		try {
			// Load the record
			var recTR = nlapiLoadRecord('customrecord_travel_request', i_tr_id);

			// Change the status to Submitted
			recTR.setFieldValue('custrecord_tr_status', 2);

			// Get Checklist Options
			var a_checklist = getListValuesArray('customlist_tr_manager_checklist');

			if (recTR.getLineItemCount('recmachcustrecord_trmc_travel_request') == 0) {
				// Add the checklist line items
				for (var i = 0; i < a_checklist.length; i++) {
					recTR.selectNewLineItem('recmachcustrecord_trmc_travel_request');
					recTR.setCurrentLineItemValue('recmachcustrecord_trmc_travel_request', 'custrecord_trmc_item', a_checklist[i].value);
					recTR.commitLineItem('recmachcustrecord_trmc_travel_request');
				}
			}

			nlapiSubmitRecord(recTR);

			sendEmail('TR_CREATED', i_tr_id, recTR);

			return ({ 'status': 1, 'message': 'Travel Request Submitted', 'tr_id': i_tr_id });
		}
		catch (e) {
			return ({ 'status': -1, 'message': 'Error: ' + e.message, 'tr_id': null });
		}

	}
	else if (s_button == 'ADD_SEGMENT') {
		try {
			var o_travel_data = new Object();

			var s_mode = '';

			o_travel_data['departure_date'] = convertToDateFormat(request.getParameter('departure_date'));
			o_travel_data['mode_of_travel'] = request.getParameter('mode_of_travel');
			o_travel_data['origin'] = request.getParameter('origin');
			o_travel_data['destination'] = request.getParameter('destination');
			o_travel_data['meal_seat_preference'] = request.getParameter('meal_seat_preference');
			o_travel_data['hotel_required'] = request.getParameter('hotel_required');
			o_travel_data['tr_id'] = request.getParameter('tr_id');
			o_travel_data['tri_id'] = request.getParameter('tri_id');

			var recTR = null;
			if (o_travel_data.tri_id != 'null' && o_travel_data.tri_id != '') {
				recTR = nlapiLoadRecord('customrecord_tr_travel_request_line_item', o_travel_data.tri_id);
				s_mode = 'Updated';
			}
			else {
				recTR = nlapiCreateRecord('customrecord_tr_travel_request_line_item');
				s_mode = 'Added';
			}

			recTR.setFieldValue('custrecord_trli_departure_date', o_travel_data.departure_date);
			recTR.setFieldValue('custrecord_trli_mode_of_travel', o_travel_data.mode_of_travel);
			recTR.setFieldValue('custrecord_trli_origin_city', o_travel_data.origin);
			recTR.setFieldValue('custrecord_trli_destination_city', o_travel_data.destination);
			recTR.setFieldValue('custrecord_trli_meal_seat_preference', o_travel_data.meal_seat_preference);
			recTR.setFieldValue('custrecord_trli_hotel_required', o_travel_data.hotel_required);
			recTR.setFieldValue('custrecord_trli_travel_request', o_travel_data.tr_id);

			var i_rec_id = nlapiSubmitRecord(recTR, true);

			var s_current_departure_date = nlapiLookupField('customrecord_travel_request', o_travel_data.tr_id, 'custrecord_tr_departure_date');

			var d_current_departure_date = nlapiStringToDate(s_current_departure_date, 'date');

			if (s_current_departure_date == '' || nlapiStringToDate(o_travel_data.departure_date, 'date') < d_current_departure_date) {
				nlapiSubmitField('customrecord_travel_request', o_travel_data.tr_id, 'custrecord_tr_departure_date', o_travel_data.departure_date);
			}

			return ({ 'status': 1, 'message': 'Travel Segment ' + s_mode, 'tr_id': i_rec_id });
		}
		catch (e) {
			return ({ 'status': -1, 'message': 'Error: ' + e.message, 'tr_id': null });
		}
	}
	else if (s_button == 'APPROVE_TRAVEL_REQUEST') {
		var i_approver_role = getApproverDetails(i_status, i_approving_manager);

		// Get logged-in user
		var objUser = nlapiGetContext();

		var i_employee_id = objUser.getUser();

		if (i_approver_role != -1) {
			try {
				var o_travel_data = new Object();
				o_travel_data['tr_id'] = request.getParameter('tr_id');
				o_travel_data['approver_remarks'] = request.getParameter('approver_remarks');

				// Update the checklist
				if (i_status == K_STATUS.PENDING_APPROVAL) {
					var a_checklist = getListValuesArray('customlist_tr_manager_checklist');

					var a_selected_checklist = new Array();

					for (var i = 0; i < a_checklist.length; i++) {

						// Check if checkbox ticked
						var isCheckboxTicked = 'F';
						if (request.getParameter('manager_checklist_' + a_checklist[i].value) != null) {
							isCheckboxTicked = 'T';
						}

						a_selected_checklist.push({ 'id': a_checklist[i].value, 'checked': isCheckboxTicked });

					}

					updateChecklist(a_selected_checklist, o_travel_data.tr_id);
				}


				// Add to approval log
				var recTRApproverAction = nlapiCreateRecord('customrecord_tr_approval_log');

				recTRApproverAction.setFieldValue('custrecord_tral_id', o_travel_data.tr_id);
				recTRApproverAction.setFieldValue('custrecord_tral_remarks', o_travel_data.approver_remarks);
				recTRApproverAction.setFieldValue('custrecord_tral_approver_action', 1);
				recTRApproverAction.setFieldValue('custrecord_tral_approved_by', i_employee_id);
				recTRApproverAction.setFieldValue('custrecord_tral_approver_role', i_approver_role);

				var i_rec_id = nlapiSubmitRecord(recTRApproverAction);

				var i_next_status = null;

				switch (i_status) {
					case K_STATUS.OPEN:
						i_next_status = K_STATUS.PENDING_APPROVAL;
						break;
					case K_STATUS.PENDING_APPROVAL:
						if (i_travel_type == K_DOMESTIC_INTERNATIONAL.DOMESTIC) {
							i_next_status = K_STATUS.TRAVEL;
						}
						else {
							i_next_status = K_STATUS.IMMIGRATION;
						}
						break;
					case K_STATUS.IMMIGRATION:
						i_next_status = K_STATUS.TRAVEL;
						break;
					case K_STATUS.TRAVEL:
						i_next_status = K_STATUS.APPROVED;
						break;
					default:
						i_next_status = -1;
				}

				var a_field_names = ['custrecord_tr_status'];
				var a_field_values = [i_next_status];

				// GT Case
				if (i_travel_type == K_DOMESTIC_INTERNATIONAL.INTERNATIONAL && i_visa_type == K_VISA_TYPE.WORK && i_status == K_STATUS.PENDING_APPROVAL) {
					a_field_names.push('custrecord_tr_gt_allocation_start_date');
					a_field_values.push(convertToDateFormat(request.getParameter('gt_allocation_start_date')));

					a_field_names.push('custrecord_tr_gt_allocation_end_date');
					a_field_values.push(convertToDateFormat(request.getParameter('gt_allocation_end_date')));

					a_field_names.push('custrecord_tr_gt_timesheet_approver');
					a_field_values.push(request.getParameter('gt_timesheet_approver'));

					a_field_names.push('custrecord_tr_gt_expense_approver');
					a_field_values.push(request.getParameter('gt_expense_approver'));

					a_field_names.push('custrecord_tr_gt_bill_rate');
					a_field_values.push(request.getParameter('gt_bill_rate'));

					//values['show-gt-fields']	=	'';
				}
				else {
					//values['show-gt-fields']	=	'none';
				}

				nlapiSubmitField('customrecord_travel_request', i_tr_id, a_field_names, a_field_values);

				// Get Attachment List To Send

				var a_attachment_ids = new Array();

				var a_request_parameters = request.getAllParameters();

				for (param in a_request_parameters) {
					if (param.indexOf('upload_attachment_') != -1) {
						a_attachment_ids.push(a_request_parameters[param]);
					}
				}

				nlapiLogExecution('AUDIT', 'Attachment IDs', a_attachment_ids.toString());

				sendEmail('TR_APPROVED', i_tr_id, null, i_rec_id, a_attachment_ids);

				return ({ 'status': 1, 'message': 'Travel Request Approved', 'tr_id': i_rec_id });
			}
			catch (e) {
				return ({ 'status': -1, 'message': 'Error: ' + e.message, 'tr_id': null });
			}
		}
		else {
			return ({ 'status': -1, 'message': 'Permission Denied', 'tr_id': null });
		}
	}
	else if (s_button == 'REJECT_TRAVEL_REQUEST') {
		var i_approver_role = getApproverDetails(i_status, i_approving_manager);

		// Get logged-in user
		var objUser = nlapiGetContext();

		var i_employee_id = objUser.getUser();

		if (i_approver_role != -1) {
			try {
				var o_travel_data = new Object();
				o_travel_data['tr_id'] = request.getParameter('tr_id');
				o_travel_data['approver_remarks'] = request.getParameter('approver_remarks');

				// Update the checklist
				var a_checklist = getListValuesArray('customlist_tr_manager_checklist');

				var a_selected_checklist = new Array();

				for (var i = 0; i < a_checklist.length; i++) {

					// Check if checkbox ticked
					var isCheckboxTicked = 'F';
					if (request.getParameter('manager_checklist_' + a_checklist[i].value) != null) {
						isCheckboxTicked = 'T';
					}

					a_selected_checklist.push({ 'id': a_checklist[i].value, 'checked': isCheckboxTicked });

				}

				updateChecklist(a_selected_checklist, o_travel_data.tr_id);

				// Add to approval log
				var recTRApproverAction = nlapiCreateRecord('customrecord_tr_approval_log');

				recTRApproverAction.setFieldValue('custrecord_tral_id', o_travel_data.tr_id);
				recTRApproverAction.setFieldValue('custrecord_tral_remarks', o_travel_data.approver_remarks);
				recTRApproverAction.setFieldValue('custrecord_tral_approver_action', 2);
				recTRApproverAction.setFieldValue('custrecord_tral_approved_by', i_employee_id);
				recTRApproverAction.setFieldValue('custrecord_tral_approver_role', i_approver_role);

				var i_rec_id = nlapiSubmitRecord(recTRApproverAction);

				nlapiSubmitField('customrecord_travel_request', i_tr_id, 'custrecord_tr_status', 1); // 1 = Open Status

				sendEmail('TR_REJECTED', i_tr_id, null, i_rec_id);

				return ({ 'status': -1, 'message': 'Travel Request Rejected', 'tr_id': i_rec_id });
			}
			catch (e) {
				return ({ 'status': -1, 'message': 'Error: ' + e.message, 'tr_id': null });
			}
		}
		else {
			return ({ 'status': -1, 'message': 'Permission Denied', 'tr_id': null });
		}
	}
	else if (s_button == 'SAVE_TRAVEL_REQUEST' || s_button == 'EDIT_TRAVEL_REQUEST') {
		var o_travel_new = new Object();
		o_travel_new['i_tr_id'] = i_tr_id;
		o_travel_new['i_domestic_country'] = request.getParameter('domestic_country');
		o_travel_new['s_mobile_number'] = request.getParameter('mobile_number');
		o_travel_new['i_project'] = request.getParameter('project');
		o_travel_new['s_travel_description'] = request.getParameter('travel_description');
		o_travel_new['i_request_type'] = request.getParameter('request_type');
		o_travel_new['i_purpose_of_travel'] = request.getParameter('purpose_of_travel');
		o_travel_new['i_domestic_international'] = request.getParameter('travel_type');
		o_travel_new['s_work_address'] = request.getParameter('work_address');
		o_travel_new['s_zip_code'] = request.getParameter('zip_code');
		o_travel_new['s_onsite_duration'] = request.getParameter('onsite_duration');
		o_travel_new['s_onsite_contact_number'] = request.getParameter('onsite_contact_number');
		o_travel_new['i_travel_advance_currency'] = request.getParameter('travel_advance_currency');
		o_travel_new['s_forex_or_advance'] = request.getParameter('forex_or_advance');
		o_travel_new['i_per_diem'] = request.getParameter('per_diem');
		o_travel_new['s_remarks'] = request.getParameter('enter_remarks');
		o_travel_new['f_passport_scan'] = request.getFile('passport_upload');
		o_travel_new['i_visa_category'] = request.getParameter('visa_category');

		var s_save_tr_mode = s_button == 'SAVE_TRAVEL_REQUEST' ? 'NEW' : 'EDIT';

		var o_status = saveTravelRequest(o_travel_new, s_save_tr_mode);

		return o_status;
	}
	else if (s_button == 'UPLOAD_ADDITIONAL_FILES') {
		try {
			if (i_status == K_STATUS.TRAVEL) {
				var o_additional_file = request.getFile('additional_files_upload');
				var s_file_description = request.getParameter('file_description');

				if (o_additional_file != null) {
					o_additional_file.setFolder(45936);
					o_additional_file.setDescription(s_file_description);
					o_additional_file.setName('TR' + i_tr_id + '_' + o_additional_file.getName());

					var i_file_id = nlapiSubmitFile(o_additional_file);

					nlapiAttachRecord('file', i_file_id, 'customrecord_travel_request', i_tr_id);

					return ({ 'status': 1, 'message': 'File Uploaded', 'tr_id': i_tr_id });

				}
			}

			return ({ 'status': 1, 'message': 'No File Uploaded', 'tr_id': i_tr_id });
		}
		catch (e) {
			return ({ 'status': -1, 'message': 'File Upload Error(' + e.message + ')', 'tr_id': i_tr_id });
		}

	}
	else if (s_button == 'NO_PERMISSION') {
		return ({ 'status': -1, 'message': 'Permission Denied', 'tr_id': null });
	}
}

// Delete Travel Request line item
function deleteTravelRequestDetails(i_tri_id) {
	try {
		nlapiDeleteRecord('customrecord_tr_travel_request_line_item', i_tri_id);

		return ({ 'status': 1, 'message': 'Travel Segment Deleted', 'tr_id': i_tri_id });
	}
	catch (e) {
		return ({ 'status': -1, 'message': 'Error: ' + e.message, 'tr_id': null });
	}
}

// Used to display the html, by replacing the placeholders
function replaceValues(content, oValues) {
	for (param in oValues) {
		// Replace null values with blank
		var s_value = (oValues[param] == null) ? '' : oValues[param];

		// Replace content
		content = content.replace(new RegExp('{{' + param + '}}', 'gi'), s_value);
	}

	return content;
}

// Get Options for select dropdown
// a_data: Array of options to be added
// i_selected_value: The option to be selected
// s_placeholder: Text to be displayed for empty value
// removeBlankOptions: If no empty value required
function getOptions(a_data, i_selected_value, s_placeholder, removeBlankOptions) {
	var strOptions = '';

	if (removeBlankOptions == true) {

	}
	else {
		strOptions = '<option value = "">' + (s_placeholder == undefined ? '' : s_placeholder) + '</option>';
	}

	var strSelected = '';

	for (var i = 0; i < a_data.length; i++) {
		if (i_selected_value == a_data[i].value) {
			strSelected = 'selected';
		}
		else {
			strSelected = '';
		}
		strOptions += '<option value = "' + a_data[i].value + '" ' + strSelected + ' >' + a_data[i].display + '</option>';
	}
	//nlapiLogExecution('Debug', 'stringsss', strOptions);
	return strOptions;
}

/**
 * @param o_attachment_list Display the attachment list for sending email
 * @returns The HTML for displaying the checklist
 */
function getChecklistToSendAttachment(o_attachment_list) {
	var s_checklist = '<div class="row">';

	var i_show_per_line = 3;

	for (var i = 0; i < o_attachment_list.length; i++) {
		if (i % i_show_per_line == 0 && i > 0) {
			s_checklist += '</div><div class="row">';
		}

		s_checklist += '<div class="col-sm-' + 12 / i_show_per_line + '"><input type="checkbox" name="upload_attachment_' + o_attachment_list[i].value + '" value="' + o_attachment_list[i].value + '" >' + o_attachment_list[i].display + '</div>';
	}

	return (s_checklist + '</div>');
}

/** Generate the billable checklist
 * @param recTR - Travel Request Record
 * @param s_mode - read-only: For non-approvers
 * @returns The HTML for the billable checklist
 */
function getTravelRequestChecklist(recTR, s_mode) {
	var i_tr_checklist_count = recTR.getLineItemCount('recmachcustrecord_trmc_travel_request');

	var list_values = new Array();

	for (var i = 1; i <= i_tr_checklist_count; i++) {
		list_values.push({ 'display': recTR.getLineItemText('recmachcustrecord_trmc_travel_request', 'custrecord_trmc_item', i), 'value': recTR.getLineItemValue('recmachcustrecord_trmc_travel_request', 'custrecord_trmc_item', i), 'checked': recTR.getLineItemValue('recmachcustrecord_trmc_travel_request', 'custrecord_trmc_value', i) });
	}

	var s_html = '<div class="row">';

	if (s_mode == 'read-only') {
		var i_show_per_line = 6;

		s_html += '<div class="col-md-12"><label class="control-label">Billable Items</label></div></div><div class="row">';

		var j = 0;

		for (var i = 0; i < list_values.length; i++) {
			if (j % i_show_per_line == 0 && j > 0) {
				s_html += '</div><div class="row">';
			}

			if (list_values[i].checked == 'T') {
				s_html += '<div class="col-sm-' + 12 / i_show_per_line + '"><span class="glyphicon glyphicon-ok"></span> ' + list_values[i].display + '</div>';

				j++;
			}
		}

		if (j == 0) {
			s_html += '<div class="col-md-12">No billable items</div>';
		}

		s_html += '</div><div class="row">';

		s_html += '<div class="col-md-12"><br /><label class="control-label">Non-Billable Items</label></div></div><div class="row">';

		j = 0;

		for (var i = 0; i < list_values.length; i++) {
			if (j % i_show_per_line == 0 && j > 0) {
				s_html += '</div><div class="row">';
			}

			if (list_values[i].checked != 'T') {
				s_html += '<div class="col-sm-' + 12 / i_show_per_line + '"><span class="glyphicon glyphicon-minus"></span> ' + list_values[i].display + '</div>';

				j++;
			}
		}

		if (j == 0) {
			s_html += '<div class="col-md-12">No non-billable items</div>';
		}
	}
	else {
		var s_disabled = s_mode == 'read-only' ? 'disabled' : '';

		var i_show_per_line = 3;

		for (var i = 0; i < list_values.length; i++) {
			if (i % i_show_per_line == 0 && i > 0) {
				s_html += '</div><div class="row">';
			}

			s_html += '<div class="col-sm-' + 12 / i_show_per_line + '"><input type="checkbox" name="manager_checklist_' + list_values[i].value + '" value="' + list_values[i].value + '" ' + ((list_values[i].checked == 'T') ? 'checked' : '') + ' ' + s_disabled + '>' + list_values[i].display + '</div>';
		}
	}


	s_html += '</div>';

	return s_html;

}

/**
 * @param a_data - data to generate the radio buttons
 * @param s_control_name - name and id for the generated control
 * @param s_selected_value - the selected option
 * @returns - the HTML for the radio buttons
 */
function getCheckBoxOptions(a_data, s_control_name, s_selected_value) {
	var strOptions = '';

	for (var i = 0; i < a_data.length; i++) {
		var isSelected = '';
		if (a_data[i].value == s_selected_value) {
			isSelected = 'checked';
		}
		strOptions += ' <input type="radio" ' + isSelected + ' id="' + s_control_name + '" name="' + s_control_name + '" value = "' + a_data[i].value + '"><span> ' + a_data[i].display + '</span>';
	}

	return strOptions;
}
function getProjects(i_employee_id) {
	var columns = new Array();
	columns[0] = new nlobjSearchColumn('company');

	var resource_allocations = nlapiSearchRecord('resourceallocation', null, [new nlobjSearchFilter('resource', null, 'anyof', i_employee_id)], columns);

	var project_list = new Array();

	for (var i = 0; resource_allocations != null && i < resource_allocations.length; i++) {
		project_list.push({ 'display': resource_allocations[i].getText(columns[0]), 'value': resource_allocations[i].getValue(columns[0]) });
	}

	return project_list;
}

function getCustomerProjectList() {
	var filters = new Array();
	filters[0] = new nlobjSearchFilter('status', null, 'noneof', 1);
//	------------------------------------------------------------------------------------------------
//this filter is used to disable the whole customer(26/10/2021-Koushalya)
//	filters[1] = new nlobjSearchFilter('customer', null, 'noneof', K_DISABLE_CUSTOMERS['CUSTOMER']);
//----------------------------------------------------------------------------------------------------

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('internalid');
	columns[1] = new nlobjSearchColumn('jobname');
	columns[2] = new nlobjSearchColumn('internalid', 'customer');
	columns[3] = new nlobjSearchColumn('altname', 'customer');
	columns[4] = new nlobjSearchColumn('entityid');

	var searchResults = searchRecord('job', null, filters, columns);
	var a_data = new Array();

	for (var i = 0; i < searchResults.length; i++) {
		//-------------------------------------------------------------------------------------
		//this condition used to disable the particular project using library files(26/10/2021-Koushalya)
		if (K_DISABLE_PROJECTS['PROJECT'].indexOf(searchResults[i].getValue(columns[0])) == -1) {
		//-----------------------------------------------------------------------------------------
			var s_project_id = searchResults[i].getValue(columns[0]);
			var s_project_name = searchResults[i].getValue(columns[1]);
			var s_customer_id = searchResults[i].getValue(columns[2]);
			var s_customer_name = searchResults[i].getValue(columns[3]);
			var s_project_entityid = searchResults[i].getValue(columns[4]);
			
			a_data.push({ 'project_id': s_project_id, 'project_name': s_project_name, 'customer_id': s_customer_id, 'customer_name': s_customer_name, 'entityid': s_project_entityid });
		}
	}
	return a_data;
}

function getCustomerDropdown(o_json, i_project_id) {
	var list = new Array();

	var i_selected_value = '';

	for (var x in o_json) {
		list.push({ 'display': o_json[x].name, 'value': x });

		for (var i = 0; i < o_json[x].list.length; i++) {
			if (o_json[x].list[i].id == i_project_id) {
				i_selected_value = x;
			}
		}

	}

	return getOptions(list, i_selected_value);
}

function getProjectDropdown(o_json, i_project_id) {

	var list = new Array();

	var i_customer_id = null;

	for (var x in o_json) {
		for (var i = 0; i < o_json[x].list.length; i++) {
			if (o_json[x].list[i].id == i_project_id) {
				i_customer_id = x;
			}
		}
	}

	if (i_customer_id != null) {
		var display_project = new Array();
		for (i = 0; i < o_json[i_customer_id].list.length; i++) {
			nlapiLogExecution('Debug', 'getProjectDropdown', JSON.stringify(o_json));
			display_project[i] = o_json[i_customer_id].list[i].entityid + " : " + o_json[i_customer_id].list[i].value;
			//nlapiLogExecution('Audit', 'projects', display_project[i]);
			list.push({ 'display': display_project[i], 'value': o_json[i_customer_id].list[i].id });
		}
	}

	return getOptions(list, i_project_id);
}

function getCustomerProjectJSON() {
	var a_data = getCustomerProjectList();

	var o_json = new Object();

	for (var i = 0; i < a_data.length; i++) {

		var o_project = { 'id': a_data[i].project_id, 'value': a_data[i].project_name, 'entityid': a_data[i].entityid };

		if (o_json[a_data[i].customer_id] != undefined) {
			o_json[a_data[i].customer_id].list.push(o_project);
		}
		else {
			o_json[a_data[i].customer_id] = { 'name': a_data[i].customer_name, 'list': [o_project] };
		}
	}

	return o_json;

}

function getCities(i_country, i_selected_city, i_domestic_international) {
	var filters = new Array();
	if (i_domestic_international != K_DOMESTIC_INTERNATIONAL.INTERNATIONAL) {
		filters[0] = new nlobjSearchFilter('custrecord_trc_country', null, 'anyof', i_country);
	}

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_trc_name').setSort();
	columns[1] = new nlobjSearchColumn('custrecord_trc_country');

	var a_search_results = searchRecord('customrecord_tr_city_list', null, filters, columns);

	var list_values = new Array();

	for (var i = 0; a_search_results != null && i < a_search_results.length; i++) {
		list_values.push({ 'display': a_search_results[i].getValue('custrecord_trc_name'), 'value': a_search_results[i].id });
	}

	return getOptions(list_values, i_selected_city);
}

// Get Employee List
function getEmployeeList() {
	var filters = new Array();
	filters[0] = new nlobjSearchFilter('isinactive', null, 'is', 'F');

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('entityid').setSort();
	//columns[1]	=	new nlobjSearchColumn('custrecord_trc_country');

	var a_search_results = searchRecord('employee', null, filters, columns);

	var list_values = new Array();

	for (var i = 0; a_search_results != null && i < a_search_results.length; i++) {
		list_values.push({ 'display': a_search_results[i].getValue('entityid'), 'value': a_search_results[i].id });
	}

	return getOptions(list_values, null, '--Please Select--');
}

function getListValues(s_list_name, strSelectedValue, s_placeholder, removeBlankOption) {
	var columns = new Array();
	columns[0] = new nlobjSearchColumn('name');

	var a_search_results = searchRecord(s_list_name, null, null, columns);

	var list_values = new Array();

	for (var i = 0; a_search_results != null && i < a_search_results.length; i++) {
		list_values.push({ 'display': a_search_results[i].getValue('name'), 'value': a_search_results[i].id });
	}

	return getOptions(list_values, strSelectedValue, s_placeholder, removeBlankOption);
}

function getListValuesArray(s_list_name) {
	var columns = new Array();
	columns[0] = new nlobjSearchColumn('name');

	var a_search_results = searchRecord(s_list_name, null, null, columns);

	var list_values = new Array();

	for (var i = 0; a_search_results != null && i < a_search_results.length; i++) {
		list_values.push({ 'display': a_search_results[i].getValue('name'), 'value': a_search_results[i].id });
	}

	return list_values;
}

function getCheckBoxes(s_list_name, s_control_name, s_selected_value) {
	var columns = new Array();
	columns[0] = new nlobjSearchColumn('name');

	var a_search_results = searchRecord(s_list_name, null, null, columns);

	var list_values = new Array();

	for (var i = 0; a_search_results != null && i < a_search_results.length; i++) {
		list_values.push({ 'display': a_search_results[i].getValue('name'), 'value': a_search_results[i].id });
	}

	return getCheckBoxOptions(list_values, s_control_name, s_selected_value);
}

function updateChecklist(a_list, i_tr_id) {
	var recTR = nlapiLoadRecord('customrecord_travel_request', i_tr_id);

	var i_checklist_count = recTR.getLineItemCount('recmachcustrecord_trmc_travel_request');

	for (var i = 0; i < a_list.length; i++) {
		// Loop through checklist
		for (var j = 1; j <= i_checklist_count; j++) {
			if (recTR.getLineItemValue('recmachcustrecord_trmc_travel_request', 'custrecord_trmc_item', j) == a_list[i].id) {
				// Update Checklist Record
				recTR.setLineItemValue('recmachcustrecord_trmc_travel_request', 'custrecord_trmc_value', j, a_list[i].checked);
			}
		}
	}

	nlapiSubmitRecord(recTR);
}

function displayHeaderForm(edit_mode, recTR, i_tr_id, s_next_step, o_json) {
	var file = nlapiLoadFile(57171); //load the HTML file
	var contents = file.getValue(); //get the contents

	var o_values = new Object();

	if (edit_mode == 'edit_header' && recTR != null) {
		o_values.i_tr_id = i_tr_id;
		o_values.travel_type = recTR.getFieldValue('custrecord_tr_travel_type');
		o_values.request_type = recTR.getFieldValue('custrecord_tr_request_type');
		o_values.travel_purpose = recTR.getFieldValue('custrecord_tr_purpose_of_travel');
		o_values.country = recTR.getFieldValue('custrecord_tr_country');
		o_values.short_desc = recTR.getFieldValue('custrecord_tr_short_description');
		o_values.project = recTR.getFieldValue('custrecord_tr_project');
		o_values.work_address = recTR.getFieldValue('custrecord_tr_work_address');
		o_values.zip_code = recTR.getFieldValue('custrecord_tr_zip_code');
		o_values.onsite_duration = recTR.getFieldValue('custrecord_tr_onsite_duration');
		o_values.onsite_contact_number = recTR.getFieldValue('custrecord_tr_onsite_contact_number');
		o_values.per_diem = recTR.getFieldValue('custrecord_tr_per_diem');
		o_values.travel_advance_currency = recTR.getFieldValue('custrecord_tr_advance_currency');
		o_values.forex_or_advance = recTR.getFieldValue('custrecord_tr_forex_or_advance');
		o_values.tr_remarks = recTR.getFieldValue('custrecord_tr_remarks');
		o_values.visa_category = recTR.getFieldValue('custrecord_tr_visa_category');
		o_values.mobile_number = recTR.getFieldValue('custrecord_tr_mobile_number');
		o_values.subsidiary = recTR.getFieldText('custrecord_tr_subsidiary');
	}
	else {
		o_values.travel_type = 1;
		o_values.request_type = 1;
		// o_values.travel_advance_currency	=	1;
	}

	if (edit_mode == 'edit_header') {
		o_values.button_name = 'edit_tr';
	}
	else {
		o_values.button_name = 'save_tr';
		s_next_step = 'step-2';
	}

	// Get logged-in user
	var objUser = nlapiGetContext();

	var i_employee_id = objUser.getUser();

	var o_employee_data = nlapiLookupField('employee', i_employee_id, ['firstname', 'lastname', 'Subsidiary']);
	var o_employee_data_subsidiary = nlapiLookupField('employee', i_employee_id, 'subsidiary', true);
	//var a_project_data = getProjects(i_employee_id);

	var strCustomerOptions = getCustomerDropdown(o_json, o_values.project);


	var strProjectOptions = getProjectDropdown(o_json, o_values.project);//getOptions(a_project_data, o_values.project);
	//nlapiLogExecution('Debug', 'proj list', JSON.stringify(strProjectOptions));
	//nlapiLogExecution('Audit', 'proj list', strProjectOptions);

	var values = new Object();
	values['script-id'] = 659;
	values['tr-id'] = o_values.i_tr_id;
	values['next-step'] = s_next_step;
	values['button_name'] = o_values.button_name;

	values['customer_options'] = strCustomerOptions;
	values['project_options'] = strProjectOptions;
	values['per_diem'] = getListValues('customlist_tr_per_diem', o_values.per_diem, '--Please Select--');
	values['country_list'] = getListValues('customlist_country_list', o_values.country, '--Please Select--');
	values['domestic_international'] = getCheckBoxes('customlist_tr_travel_type', 'travel_type', o_values.travel_type);
	values['request_type'] = getListValues('customlist_tr_request_type', o_values.request_type, '--Please Select--', true);
	values['purpose_of_travel'] = getListValues('customlist_tr_travel_purpose', o_values.travel_purpose, '--Please Select--');

	values['travel_description'] = o_values.short_desc;
	values['work_address'] = o_values.work_address;
	values['zip_code'] = o_values.zip_code;
	values['mobile_number'] = o_values.mobile_number;
	values['onsite_duration'] = o_values.onsite_duration;
	values['onsite_contact_number'] = o_values.onsite_contact_number;
	values['forex_or_advance'] = o_values.forex_or_advance;
	values['enter_remarks'] = o_values.tr_remarks;

	values['visa_category'] = getListValues('customrecord_tr_visa_categories', o_values.visa_category, '--Please Select--');
	values['travel-advance-currency'] = getListValues('currency', o_values.travel_advance_currency, '-Currency-');

	contents = replaceValues(contents, values);

	return contents;
}

function saveTravelRequest(o_travel, mode) {
	try {
		// Get logged-in user
		var objUser = nlapiGetContext();

		var i_employee_id = objUser.getUser();
		var i_employee_subsidiary = nlapiLookupField('employee', i_employee_id, 'subsidiary', true);

		var recTR = null;

		if (mode == 'NEW') {
			recTR = nlapiCreateRecord('customrecord_travel_request');
		}
		else {
			recTR = nlapiLoadRecord('customrecord_travel_request', o_travel.i_tr_id);
		}

		recTR.setFieldValue('custrecord_tr_travel_type', o_travel.i_domestic_international);
		recTR.setFieldValue('custrecord_tr_country', o_travel.i_domestic_country);
		recTR.setFieldValue('custrecord_tr_mobile_number', o_travel.s_mobile_number);
		recTR.setFieldValue('custrecord_tr_request_type', o_travel.i_request_type);
		recTR.setFieldValue('custrecord_tr_employee', i_employee_id);
		recTR.setFieldText('custrecord_tr_subsidiary', i_employee_subsidiary);
		recTR.setFieldValue('custrecord_tr_project', o_travel.i_project);
		recTR.setFieldValue('custrecord_tr_short_description', o_travel.s_travel_description);
		recTR.setFieldValue('custrecord_tr_purpose_of_travel', o_travel.i_purpose_of_travel);
		recTR.setFieldValue('custrecord_tr_work_address', o_travel.s_work_address);
		recTR.setFieldValue('custrecord_tr_zip_code', o_travel.s_zip_code);
		recTR.setFieldValue('custrecord_tr_onsite_duration', o_travel.s_onsite_duration);
		recTR.setFieldValue('custrecord_tr_onsite_contact_number', o_travel.s_onsite_contact_number);
		recTR.setFieldValue('custrecord_tr_advance_currency', o_travel.i_travel_advance_currency);
		recTR.setFieldValue('custrecord_tr_forex_or_advance', o_travel.s_forex_or_advance);
		recTR.setFieldValue('custrecord_tr_per_diem', o_travel.i_per_diem);
		recTR.setFieldValue('custrecord_tr_remarks', o_travel.s_remarks);
		recTR.setFieldValue('custrecord_tr_visa_category', o_travel.i_visa_category);

		var i_rec_id = nlapiSubmitRecord(recTR, true);
		if (o_travel.f_passport_scan != null) {
			o_travel.f_passport_scan.setFolder(42299);
			o_travel.f_passport_scan.setDescription('Passport Scan');
			o_travel.f_passport_scan.setName('TR' + i_rec_id + '_' + o_travel.f_passport_scan.getName());

			var i_file_id = nlapiSubmitFile(o_travel.f_passport_scan);

			nlapiAttachRecord('file', i_file_id, 'customrecord_travel_request', i_rec_id);
		}

		return ({ 'status': 1, 'message': 'Travel Request Saved', 'tr_id': i_rec_id });
	}
	catch (e) {
		return ({ 'status': -1, 'message': 'Error: ' + e.message, 'tr_id': null });
	}
}

// Display Progress Bar
function displayProgressBar(o_approval_status, i_status, i_domestic_international) {
	var i_corresponding_status = 0;
	switch (i_status) {
		case '1':
			i_corresponding_status = -1;
			break;
		case '2':
			i_corresponding_status = 0;
			break;
		case '3':
			i_corresponding_status = 1;
			break;
		case '4':
			i_corresponding_status = 2;
			break;
		case '5':
			i_corresponding_status = 3;
			break;
	}

	var step_count = 3;

	var steps = new Object();
	steps['0'] = 'Request Submitted';
	steps['1'] = 'Approver';
	if (i_domestic_international == K_DOMESTIC_INTERNATIONAL.INTERNATIONAL) {
		steps['2'] = 'Immigration';
		step_count++;
	}
	steps['3'] = 'Travel';

	var strHtml = '<div style="text-align:center;" class="row"><ol class="progtrckr" data-progtrckr-steps="' + step_count.toString() + '">';

	for (var x in steps) {
		var s_approved = 'progtrckr-todo';

		if (parseInt(x) <= parseInt(i_corresponding_status) && o_approval_status[x] != undefined) {
			if (o_approval_status[x] == '1') {
				s_approved = 'progtrckr-done';
			}
			else if (o_approval_status[x] == '2') {
				s_approved = 'progtrckr-cancel';
			}
		}

		strHtml += '<li class="' + s_approved + '">' + steps[x] + '</li>';
	}

	strHtml += '</ol></div><br />';

	return strHtml;
}

// Display Table
function displayTable(o_headings, a_data, date_format) {
	var strHtml = '';

	var enableHighlight = false;

	var s_highlight = '';

	// Display Header
	strHtml += '<thead><tr>';
	for (var s_field in o_headings) {
		if (s_field != 'highlight') {
			strHtml += '<th class="' + ((s_field == 'edit' || s_field == 'delete') ? 'edit-mode' : '') + '" style="' + ((s_field == 'edit' || s_field == 'delete') ? 'text-align:center;' : '') + '">' + o_headings[s_field] + '</th>';
		}
		else {
			enableHighlight = true;
		}
	}
	strHtml += '</tr></thead>';

	for (var i = 0; i < a_data.length; i++) {

		if (enableHighlight == true) {
			s_highlight = a_data[i]['highlight'];
		}
		else {
			s_highlight = '';
		}

		strHtml += '<tr class="' + s_highlight + '">';
		for (var s_field in o_headings) {
			if (s_field != 'highlight') {
				var s_value = a_data[i][s_field];
				if (s_value == null) {
					s_value = '';
				}
				else if (isDate(s_value)) {
					s_value = nlapiDateToString(s_value, date_format);
				}

				strHtml += '<td class="' + ((s_field == 'edit' || s_field == 'delete') ? 'edit-mode' : '') + '" style="' + ((s_field == 'edit' || s_field == 'delete') ? 'text-align:center;' : '') + '">' + s_value + '</td>';
			}
		}
		strHtml += '</tr>';
	}

	return strHtml;
}

// Sort Data
function compare(a, b) {
	if (a.sort_value < b.sort_value)
		return -1;
	if (a.sort_value > b.sort_value)
		return 1;
	return 0;
}

// Dynamic sort
function dynamicSort(property) {
	var sortOrder = 1;
	if (property[0] === "-") {
		sortOrder = -1;
		property = property.substr(1);
	}
	return function (a, b) {
		var result = (a[property] < b[property]) ? -1 : (a[property] > b[property]) ? 1 : 0;
		return result * sortOrder;
	};
}

// Change from dd-MMM-yyyy to mm/dd/yyyy format
function convertToDateFormat(input) {
	var pattern = /(.*?)-(.*?)-(.*?)$/;
	var result = input.replace(pattern, function (match, p1, p2, p3) {
		var months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
		return (months.indexOf(p2) + 1) + "/" + (p1 < 10 ? p1.substring(1) : p1) + "/" + p3;
	});

	return result;
}

// Change from mm/dd/yyyy format to dd-MMM-yyyy
function convertToDateFormat1(input) {
	var pattern = /(.*?)\/(.*?)\/(.*?)$/;
	var result = input.replace(pattern, function (match, p1, p2, p3) {
		var months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
		return (p2 < 10 ? "0" + p2 : p2) + "-" + months[(p1 - 1)] + "-" + p3;
	});

	return result;
}

// Format Date
function GetFormattedDate(d_date) {
	var todayTime = d_date;
	var month = format(todayTime.getMonth() + 1);
	var day = format(todayTime.getDate());
	var year = format(todayTime.getFullYear());
	return month + "/" + day + "/" + year;
}

// Check if Date object
function isDate(myDate) {
	return myDate.constructor.toString().indexOf("Date") > -1;
}

// Send Email
function sendEmail(mode, i_tr_id, recTR, i_log_rec_id, a_attachment_list) {
	var i_template = null;	// Template to load

	var s_to = null;
	var a_cc = new Array();

	var values = new Object();

	var a_attachment_files = null;

	var i_domestic_international;

	var i_destination;

	var o_values = nlapiLookupField('customrecord_travel_request', i_tr_id, ['custrecord_tr_employee', 'custrecord_tr_status', 'custrecord_tr_delivery_manager', 'custrecord_tr_approving_manager_id', 'custrecord_tr_visa_type', 'custrecord_tr_travel_type', 'custrecord_tr_gt_bill_rate']);

	var search_destination = nlapiSearchRecord('customrecord_tr_travel_request_line_item', null,
		['custrecord_trli_travel_request.internalid', 'is', i_tr_id],
		[new nlobjSearchColumn('custrecord_trli_destination_city')]);

	var o_requestor = nlapiLookupField('employee', o_values.custrecord_tr_employee, ['internalid', 'email']);

	var sendSeperateEmailToRequestor = false;

	try {
		switch (mode) {
			case 'TR_CREATED':
				s_to = recTR.getFieldValue('custrecord_tr_approving_manager_id');

				i_domestic_international = recTR.getFieldValue('custrecord_tr_travel_type');

				var o_approver = nlapiLookupField('employee', s_to, ['firstname', 'lastname']);

				values['approver_firstname'] = o_approver.firstname;
				values['approver_lastname'] = o_approver.lastname;

				a_cc = a_cc.concat(o_requestor.email);
				a_cc = a_cc.concat(email_configuration.TRAVEL);
				a_cc = a_cc.concat('waseem.pasha@brillio.com');
				a_cc = a_cc.concat('aditi.sood@BRILLIO.COM');

				if (i_domestic_international == K_DOMESTIC_INTERNATIONAL.INTERNATIONAL) {
					a_cc = a_cc.concat(email_configuration.IMMIGRATION);
					a_cc = a_cc.concat(email_configuration.INDIA_HR);
				}
				else {

				}

				for (var i = 0; i < search_destination.length; i++) {
					i_destination = search_destination[i].getValue('custrecord_trli_destination_city');
					if (i_destination == 314 || i_destination == 'PUNE' || i_destination == 'Pune') {
						a_cc = a_cc.concat('Jayarajan.C@brillio.com');
					}
				}
				i_template = K_EMAIL_TEMPLATES.SUBMITTED;
				var reqstr = recTR.getFieldValue('custrecord_tr_employee');
				var req_emp = nlapiLookupField('employee', reqstr, ['firstname', 'email', 'custentity_otg_app']);
				if (req_emp.custentity_otg_app != 'T') {
					var htmltext = "";
					htmltext += "<p> Dear " + req_emp.firstname + ",<p>";
					htmltext += "<p>A change in continents shouldn’t stand in the way of your travels. Expenses can be filed now with just a click.<p>";
					htmltext += "<p>Download On the Go today!<p>";
					htmltext += "<p>Click on the link: https://onthegoprod.blob.core.windows.net/onthego/index.html<p>";
					htmltext += "<p>Thanks & Regards,<p>";
					htmltext += "<p>Information Systems<p>";
					nlapiSendEmail(442, req_emp.email, 'Filling expenses OnTheGO.', htmltext, null, null, null, null);
				}
				break;
			case 'TR_APPROVED':

				i_domestic_international = o_values.custrecord_tr_travel_type;

				var o_approval_log = nlapiLoadRecord('customrecord_tr_approval_log', i_log_rec_id);

				var o_approver = nlapiLookupField('employee', o_approval_log.getFieldValue('custrecord_tral_approved_by'), ['firstname', 'lastname', 'email']); // The one who approved

				var o_approving_manager = nlapiLookupField('employee', o_values.custrecord_tr_approving_manager_id, ['email']); // The approving manager

				//var o_requestor	=	nlapiLookupField('employee', o_values.custrecord_tr_employee, ['internalid', 'email']);

				values['role'] = o_approval_log.getFieldText('custrecord_tral_approver_role');
				values['action'] = o_approval_log.getFieldValue('custrecord_tral_approver_action');
				values['remarks'] = o_approval_log.getFieldValue('custrecord_tral_remarks');
				values['remarks'] = (values['remarks'] == null) ? '' : '<br /><br /><b>' + values['role'] + ' Remarks</b><br />' + values['remarks'] + '<br />';

				var i_approver_role = o_approval_log.getFieldValue('custrecord_tral_approver_role'); // Role of the approver

				if (i_approver_role == K_TRAVEL_APPROVER_ROLE.APPROVER) // Approved by Approver
				{
					if (i_domestic_international == K_DOMESTIC_INTERNATIONAL.DOMESTIC) {
						s_to = email_configuration.TRAVEL;

						//a_cc = a_cc.concat(o_requestor.email);
						a_cc = a_cc.concat(o_approver.email);
						a_cc = a_cc.concat(o_approving_manager.email);

						/*for(var i = 0 ; i < search_destination.length; i++)
						{
							i_destination = search_destination[i].getValue('custrecord_trli_destination_city');
							if(i_destination == 314 || i_destination == 'PUNE' || i_destination =='Pune')
							{
								//a_cc = a_cc.concat('Jayarajan.C@brillio.com');
							}
						}*/
					}
					else {
						s_to = email_configuration.IMMIGRATION;

						sendSeperateEmailToRequestor = true;

						//a_cc = a_cc.concat(o_requestor.email);
						a_cc = a_cc.concat(o_approver.email);
						a_cc = a_cc.concat(o_approving_manager.email);
						a_cc = a_cc.concat(email_configuration.TRAVEL);
						//a_cc = a_cc.concat(email_configuration.HR);
						a_cc = a_cc.concat(email_configuration.FINANCE);

						/*if(o_values.custrecord_tr_visa_type	==	K_VISA_TYPE.WORK)
							{
							
								// values['gt_bill_rate']	=	o_values.custrecord_tr_gt_bill_rate;
							
								a_cc	=	a_cc.concat(email_configuration.OPS);
								a_cc	=	a_cc.concat(email_configuration.HR_US);
								a_cc	=	a_cc.concat(email_configuration.TA);
								a_cc	=	a_cc.concat(email_configuration.INDIA_HR);
								a_cc	=	a_cc.concat(email_configuration.INDIA_COMP_TEAM);
								a_cc	=	a_cc.concat(email_configuration.RMG_US);
								a_cc	=	a_cc.concat(email_configuration.RMG_INDIA);
							}*/
					}

					values['approver_firstname'] = o_approver.firstname;
					values['approver_lastname'] = o_approver.lastname;
					//a_cc.push('immigration@brillio.com');
					i_template = 24;
				}
				else if (i_approver_role == K_TRAVEL_APPROVER_ROLE.IMMIGRATION) // Approved by Immigration
				{
					if (i_domestic_international == K_DOMESTIC_INTERNATIONAL.INTERNATIONAL) {
						s_to = email_configuration.TRAVEL;
					}

					//var o_delivery_manager	=	nlapiLookupField('employee', o_approval_log.getFieldValue('custrecord_tral_approved_by'), ['firstname', 'lastname']);
					values['approver_firstname'] = o_approver.firstname;
					values['approver_lastname'] = o_approver.lastname;
					a_cc = a_cc.concat(email_configuration.IMMIGRATION);
					//a_cc.push(nlapiLookupField('employee', o_values.custrecord_tr_approving_manager_id, 'email'));
					//a_cc.push('team.indiahr@brillio.com');
					a_cc = a_cc.concat(email_configuration.OPS);
					//a_cc.push('traveldesk@brillio.com');
					i_template = K_EMAIL_TEMPLATES.IMMIGRATION_APPROVED;
				}
				else if (i_approver_role == K_TRAVEL_APPROVER_ROLE.TRAVEL) // Approved by Travel
				{
					s_to = o_requestor.internalid;
					values['approver_firstname'] = o_approver.firstname;
					values['approver_lastname'] = o_approver.lastname;

					if (i_domestic_international == K_DOMESTIC_INTERNATIONAL.INTERNATIONAL) {
						// Send seperate email to requestor, without the gt rate
						sendSeperateEmailToRequestor = true;

						a_cc = a_cc.concat(email_configuration.FINANCE);
						a_cc = a_cc.concat(email_configuration.IMMIGRATION);
						a_cc = a_cc.concat(email_configuration.OPS);
						//a_cc = a_cc.concat(email_configuration.FMG);

						// If Work Visa, send to HR
						if (o_values.custrecord_tr_visa_type == K_VISA_TYPE.WORK) {
							a_cc = a_cc.concat(email_configuration.INDIA_HR);
						}
					}

					if (a_attachment_list.length > 0) {
						a_attachment_files = new Array();
					}

					for (var i = 0; i < a_attachment_list.length; i++) {
						a_attachment_files.push(nlapiLoadFile(a_attachment_list[i]));
					}

					i_template = K_EMAIL_TEMPLATES.TRAVEL_APPROVED;
				}
				try {
					var req_details = nlapiLookupField('customrecord_travel_request', i_tr_id, ['custrecord_tr_forex_or_advance']);
					var currency = req_details.custrecord_tr_forex_or_advance;
					nlapiLogExecution('AUDIT', 'currency value', currency);
					if (parseInt(currency) > 0) {
						a_cc = a_cc.concat('santosh.banadawar@brillio.com');
					}
				} catch (e) {
					// TODO: handle exception
				}

				break;
			case 'TR_REJECTED':

				var o_approval_log = nlapiLoadRecord('customrecord_tr_approval_log', i_log_rec_id);

				s_to = o_requestor.internalid;
				a_cc = a_cc.concat(email_configuration.TRAVEL);
				var o_approver = nlapiLookupField('employee', o_approval_log.getFieldValue('custrecord_tral_approved_by'), ['firstname', 'lastname']);
				values['delivery_manager_firstname'] = o_approver.firstname;
				values['delivery_manager_lastname'] = o_approver.lastname;
				values['role'] = o_approval_log.getFieldText('custrecord_tral_approver_role').toUpperCase();
				values['action'] = o_approval_log.getFieldValue('custrecord_tral_approver_action');
				values['remarks'] = (values['remarks'] == null) ? '' : '<br /><br /><b>' + values['role'] + ' Remarks</b><br />' + values['remarks'] + '<br />';

				i_template = K_EMAIL_TEMPLATES.REJECTED;

				break;
		}

		var emailMerger = nlapiCreateEmailMerger(i_template); // Initiate Email Merger

		emailMerger.setCustomRecord('customrecord_travel_request', i_tr_id); // Set the ID of the transaction where you are going to fetch the values to populate the variables on the template

		var mergeResult = emailMerger.merge(); // Merge the template with the email

		var emailSubject = mergeResult.getSubject(); // Get the subject for the email

		var emailBody = mergeResult.getBody(); // Get the body for the email

		emailSubject = replaceValues(emailSubject, values);

		var emailBodyRequestor = '';

		if (sendSeperateEmailToRequestor == true) {
			values['gt_bill_rate'] = '';
			emailBodyRequestor = replaceValues(emailBody, values);
		}

		values['gt_bill_rate'] = o_values.custrecord_tr_gt_bill_rate;

		emailBody = replaceValues(emailBody, values);

		//s_to	=	9122;
		//a_cc	=	['amol.sahijwani@brillio.com'];
		//a_cc	=	a_cc.concat('waseem.pasha@brillio.com');

		try {
			var orh = recTR.getLineItemValue('recmachcustrecord_trli_travel_request', 'custrecord_trli_origin_city', 1);
			if (orh == 429) {
				a_cc = a_cc.concat('gouri.v@brillio.com');
				a_cc = a_cc.concat('lincy.n@brillio.com');
			}

			var project_id = recTR.getFieldValue('custrecord_tr_project');
			var jobSearch = nlapiSearchRecord("job", null,
				[
					["internalid", "anyof", project_id]
				],
				[
					new nlobjSearchColumn("email", "CUSTENTITY_DELIVERYMANAGER", null),
					new nlobjSearchColumn("email", "CUSTENTITY_PROJECTMANAGER", null)
				]
			);
			if (jobSearch) {
				for (var js = 0; js < jobSearch.length; js++) {
					a_cc = a_cc.concat(jobSearch[0].getValue("email", "CUSTENTITY_PROJECTMANAGER", null));
					a_cc = a_cc.concat(jobSearch[0].getValue("email", "CUSTENTITY_DELIVERYMANAGER", null));
				}
			}

			if (project_id == 161385) {
				a_cc = a_cc.concat('kaushal.thakar@BRILLIO.COM');
				a_cc = a_cc.concat('govind.kharayat@brillio.com');
				a_cc = a_cc.concat('sridhar.v@brillio.com');
			}

		} catch (e) {
			// TODO: handle exception


		}
		var email_attach = [];
		email_attach['entity'] = 97260;
		nlapiSendEmail(442, s_to, emailSubject, emailBody, a_cc, null, null, a_attachment_files, email_attach); // Send the email with attachment

		if (sendSeperateEmailToRequestor == true) {
			nlapiSendEmail(442, o_requestor.email, emailSubject, emailBodyRequestor, null, null, null, a_attachment_files, email_attach);
		}

	}
	catch (e) {
		nlapiLogExecution('ERROR', 'Error Send Email: ', e.message);
	}
}

// Get Role Name
function getRoleName(i_user_id, i_role_id) {
	var a_search_results = nlapiSearchRecord('employee', null, [new nlobjSearchFilter('internalid', null, 'anyof', i_user_id), new nlobjSearchFilter('role', null, 'anyof', i_role_id)], new nlobjSearchColumn('role'));

	if (a_search_results != null && a_search_results.length == 1) {
		return a_search_results[0].getText('role');
	}
	else {
		return '';
	}
}
