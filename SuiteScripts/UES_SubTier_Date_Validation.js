/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       02 Jan 2017     deepak.srinivas
 *
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType
 *   
 * @returns {Boolean} True to continue save, false to abort save
 */
function validateDates_Subtier(type){
try{
	if(type == 'create'){  //|| type == 'edit') && type != 'delete'
		
	/*var oldRec = nlapiGetOldRecord();
	var s_startDate = oldRec.getFieldValue('custrecord_stvd_start_date');
	var d_startDate = nlapiStringToDate(s_startDate);
	var s_endDate = oldRec.getFieldValue('custrecord_stvd_end_date');
	var d_endDate = nlapiStringToDate(s_endDate);*/
	
	var stDate = nlapiGetFieldValue('custrecord_stvd_start_date');
	var endDate = nlapiGetFieldValue('custrecord_stvd_end_date');
	var contractor = nlapiGetFieldValue('custrecord_stvd_contractor');
	var vendortype=nlapiGetFieldValue('custrecord_stvd_vendor_type');
	stDate = nlapiStringToDate(stDate);
	endDate = nlapiStringToDate(endDate);
	nlapiLogExecution('DEBUG','enddate',endDate);
	/*if(stDate == d_startDate && d_endDate == endDate){
		return;
	}*/
	if(endDate <= stDate){
		throw 'End date cannot be prior to start date';
		return false;
	}
			
	//Search For the entry
	var filterExpression = Array();
	//Define search filter expression
	var filterExpression  = [
	            		
	            			[['custrecord_stvd_start_date', 'onorbefore', endDate],
	            			'AND' ,
                          ['custrecord_stvd_end_date', 'onorafter', stDate],
                        'AND',
                        ['custrecord_stvd_vendor_type','is',vendortype]],
						'AND',
	            		['isinactive', 'is', 'F'],
	            		'AND',
	            		['custrecord_stvd_contractor', 'anyof', contractor]
      //,
					//	'AND',
						//['custrecord_stvd_vendor_type','anyof',vendortype]
	            ];
	/*var filterExpression =   [ [ 'custrecord_stvd_start_date', 'within',stDate,endDate],
	               'or',
	               [ 'custrecord_stvd_end_date', 'within',stDate,endDate],
	               'and'
	               ['custrecord_stvd_contractor', 'is',contractor]
	               ] ;*/
	//filters.push(new nlobjSearchFilter('custrecord_stvd_start_date',null,'within',stDate,endDate));
	//filters.push(new nlobjSearchFilter('custrecord_stvd_contractor',null,'anyof',contractor));
	
	var cols =  Array();
	cols.push(new nlobjSearchColumn('internalid'));
	cols.push(new nlobjSearchColumn('custrecord_stvd_start_date'));
	cols.push(new nlobjSearchColumn('custrecord_stvd_end_date'));
	cols.push(new nlobjSearchColumn('custrecord_stvd_contractor'));
	cols.push(new nlobjSearchColumn('custrecord_stvd_vendor'));
	cols.push(new nlobjSearchColumn('custrecord_stvd_subsidiary'));
     cols.push(new nlobjSearchColumn('custrecord_stvd_vendor_type')); 
	
	var dataObj = {};
	var dataRow = [];
	var projectArray = [];
	var searchRes = nlapiSearchRecord('customrecord_subtier_vendor_data',null,filterExpression,cols);
	
	if(searchRes){
		nlapiLogExecution('DEBUG','Subtier searchRes',searchRes.length);
		for(var i=0;i<searchRes.length;i++){
			var id = searchRes[i].getValue('internalid');
			nlapiLogExecution('DEBUG','Subtier For Line',i);
			nlapiLogExecution('DEBUG','Subtier Rec ID',id);
			var st_date = searchRes[i].getValue('custrecord_stvd_start_date');
			nlapiLogExecution('DEBUG','Subtier st_date',st_date);
			var end_date = searchRes[i].getValue('custrecord_stvd_end_date');
			nlapiLogExecution('DEBUG','Subtier end_date',end_date);
			var s_contractor = searchRes[i].getText('custrecord_stvd_contractor');
   			nlapiLogExecution('DEBUG','Subtier s_contractor',s_contractor);
          var s_ref = searchRes[i].getText('custrecord_stvd_vendor_type');
   			nlapiLogExecution('DEBUG','vendor type',s_ref);
			var s_vendor = searchRes[i].getText('custrecord_stvd_vendor');
			nlapiLogExecution('DEBUG','Subtier s_vendor',s_vendor);
			
		}

		throw 'Please check the dates. There is already a record with same dates !!';
		return false;
	}
	//Search For the entry
		var filterExp = Array();
			//Define search filter expression
		var filterExp  = [
	            		['isinactive', 'is', 'F'],
	            		'AND',
	            		['custrecord_stvd_contractor', 'anyof', contractor]
	            ];
		var colm =  Array();
		colm.push(new nlobjSearchColumn('internalid'));
		colm.push(new nlobjSearchColumn('custrecord_stvd_end_date').setSort(false));
		var searchResult = nlapiSearchRecord('customrecord_subtier_vendor_data',null,filterExp,colm);
	
		if(searchResult){
			nlapiLogExecution('DEBUG','Subtier searchRes',searchResult.length);
			for(var i=0;i<searchResult.length;i++){
			nlapiLogExecution('DEBUG','Subtier For Line',i);			
			var d_end_date = searchResult[i].getValue('custrecord_stvd_end_date');	
			d_end_date = nlapiStringToDate(d_end_date);
			if(i==searchResult.length-1)
			{
				var gap=((stDate-d_end_date)/(1000 * 60 * 60 * 24));
					if(parseInt(gap)	> parseInt(1))
					{
						throw 'Gap Between Allocation is not Allowed';
						return false;
					}
			}
			}
		 }
	}
	else if(type == 'edit'){
		var recid=nlapiGetRecordId();
		var stDate = nlapiGetFieldValue('custrecord_stvd_start_date');
		var endDate = nlapiGetFieldValue('custrecord_stvd_end_date');
		var contractor = nlapiGetFieldValue('custrecord_stvd_contractor');
		var internalid=nlapiGetFieldValue('internalid');
		var vendor_type=nlapiGetFieldText('custrecord_stvd_vendor_type');
		var i_vendor_type=nlapiGetFieldValue('custrecord_stvd_vendor_type');
		stDate = nlapiStringToDate(stDate);
		endDate = nlapiStringToDate(endDate); 
		nlapiLogExecution('DEBUG','Mode',type);
		nlapiLogExecution('DEBUG','recordid',recid);
		nlapiLogExecution('DEBUG','vendor',vendor_type);
		if(endDate <=stDate)
		{
			throw 'endate should be not as start date';
			return false;
		}
	var inactive=nlapiGetFieldValue('isinactive');
	if(inactive == 'T')
	{
	nlapiLogExecution('debug', 'record deleted');
	return true;
	
	}
	else
	{
	
	//i_vendor_type
	
	nlapiLogExecution('debug', 'i_vendor_type',i_vendor_type);
	nlapiLogExecution('debug', 'contractor',contractor);	
		//Search For the entry
		var filterExpressions = Array();
			//Define search filter expression
		var filterExpressions  = [
	          
	            		['internalid','noneof',recid],
						'AND',
	            		['isinactive', 'is', 'F'],
	            		'AND',
	            		['custrecord_stvd_contractor', 'anyof', contractor],
						'AND',
						['custrecord_stvd_vendor_type','anyof',i_vendor_type]
	            ];
	

		var cols =  Array();
		cols.push(new nlobjSearchColumn('internalid'));
		cols.push(new nlobjSearchColumn('custrecord_stvd_start_date'));
		cols.push(new nlobjSearchColumn('custrecord_stvd_end_date'));
		cols.push(new nlobjSearchColumn('custrecord_stvd_contractor'));
		cols.push(new nlobjSearchColumn('custrecord_stvd_vendor'));
		cols.push(new nlobjSearchColumn('custrecord_stvd_subsidiary'));
		var searchResu = nlapiSearchRecord('customrecord_subtier_vendor_data',null,filterExpressions,cols);
	
		if(searchResu){
			nlapiLogExecution('DEBUG','Subtier searchRes',searchResu.length);
			for(var i=0;i<searchResu.length;i++){
			var id = searchResu[i].getValue('internalid');
			nlapiLogExecution('DEBUG','internalid',id);
			nlapiLogExecution('DEBUG','Subtier For Line',i);			
			var st_date = searchResu[i].getValue('custrecord_stvd_start_date');	
			var end_date = searchResu[i].getValue('custrecord_stvd_end_date');	
			var s_contractor = searchResu[i].getText('custrecord_stvd_contractor');
			var s_vendor = searchResu[i].getText('custrecord_stvd_vendor');
			st_date = nlapiStringToDate(st_date);
			end_date = nlapiStringToDate(end_date);
			//nlapiLogExecution('debug', 'date',st_date);
			//nlapiLogExecution('debug', 'stdate',stDate);
		 	if(stDate<st_date)
			{
				throw 'Please check the record. Another vendor is tagged to the resource !!';
				return false;
			}
			if(stDate<=end_date)
			{
				throw 'Please Check the dates. Another Record exist in between these dates.!!!';
				return false;
			}
			}
		}	
		
	}
	nlapiLogExecution('debug', 'record updated',recid);
	return true;
	}   
}
catch(e){
	nlapiLogExecution('DEBUG','Validation Error',e);
	throw e;
}
}

/*function fieldChange_Trigger(type, name, linenum)
{
try{
if(name == 'custrecord_stvd_end_date'){
var s_endDate = nlapiGetFieldValue('custrecord_stvd_end_date');
var d_endDate = nlapiStringToDate(s_endDate);
var s_startDate = nlapiGetFieldValue('custrecord_stvd_start_date');
var d_startDate = nlapiStringToDate(s_startDate);
if(d_endDate <= d_startDate && nlapiGetFieldValue('custrecord_stvd_end_date') != ''){
alert('End Date date cannot be lessthan Start Date');
nlapiSetFieldValue('custrecord_stvd_end_date', '')

}
}
return true;
}
catch(e){
nlapiLogExecution('DEBUG','Field Change Error',e);
	return false;
}
}*/
//Added functionality for UK VAT field - 26-FEB-19
function beforeLoad(type, form)
{
try{
	var i_employee = nlapiGetFieldValue('custrecord_stvd_contractor');
	var lookUp_subsdiary_fld = nlapiLookupField('employee',parseInt(i_employee),'subsidiary');
	if(parseInt(lookUp_subsdiary_fld) != parseInt(7)){
 form.getField('custrecord_uk_vat_applicable').setDisplayType('hidden');
 }
 if(parseInt(lookUp_subsdiary_fld) == parseInt(7)){
 form.getField('custrecord_uk_vat_applicable').setDisplayType('normal').setMandatory(true);
 }
 }
 catch(e){
nlapiLogExecution('DEBUG','Before Load Error',e);
	
}
}