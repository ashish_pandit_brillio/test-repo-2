/**
 * Module Description
 * Report to get a summary of the hours entered and not entered by an employee
 * Version    Date            Author           Remarks
 * 1.00       09 Feb 2015     amol.sahijwani
 *
 */

/**
 * @param {nlobjRequest} request Request object
 * @param {nlobjResponse} response Response object
 * @returns {Void} Any output is written via response object
 */
function suitelet(request, response){
	var form = nlapiCreateForm('Hours Summary Report');
	
	// Add From Date Field
	var fld_from_date = form.addField('custpage_from_date', 'date', 'From Date').setMandatory(true);
	
	// Add To Date Field
	var fld_to_date = form.addField('custpage_to_date', 'date', 'To Date').setMandatory(true);
	
	// Add Department Field
	var fld_department = form.addField('custpage_department', 'select', 'Department', 'department');
	
	// Add Total Working Days Field
	var fld_total_working_days = form.addField('custpage_total_working_days', 'text', 'Working Days').setDisplayType('inline');
	
	// Add Total Working Days Field
	var fld_total_working_hours = form.addField('custpage_total_working_hours', 'text', 'Working Hours').setDisplayType('inline');
	
	form.addSubmitButton('Load Data');
	
	if(request.getMethod() == 'POST')
	{
		var s_from_date = request.getParameter('custpage_from_date');
		var d_from_date = nlapiStringToDate(s_from_date, 'datetimetz');
		fld_from_date.setDefaultValue(s_from_date);
		//d_from_date = new Date(d_from_date.setHours(0,0,0));
		
		var s_to_date = request.getParameter('custpage_to_date');
		var d_to_date = nlapiStringToDate(s_to_date, 'datetimetz');
		fld_to_date.setDefaultValue(s_to_date);
		//d_to_date = new Date(d_to_date.setHours(0,0,0));
		
		var i_department = request.getParameter('custpage_department');
		var a_department = null;
		if(i_department != '')
			{
				a_department = searchSubDepartments(i_department);
			}
		fld_department.setDefaultValue(i_department);
		var i_working_days = parseInt(calcBusinessDays(d_from_date, d_to_date));
		
		fld_total_working_days.setDefaultValue(i_working_days.toString());
		fld_total_working_hours.setDefaultValue((i_working_days * 8).toString());
		var o_holidays	=	getHolidays(d_from_date, d_to_date);
		// Resource Allocation Data
		var a_resource_allocation = getResourceAllocations(d_from_date, d_to_date, a_department, o_holidays);
		
		var i_current_employee = null;
		
		var a_res_alloc_for_each_employee = new Object();
		
		var a_exception_employees = new Array();
		
		var a_billable_employees = new Array();
		
		for(var i_res_alloc_indx = 0; a_resource_allocation != null && i_res_alloc_indx < a_resource_allocation.length; i_res_alloc_indx++)
			{
				var o_resource_allocation = a_resource_allocation[i_res_alloc_indx];
				var i_employee_id = o_resource_allocation.employee;
				if(a_res_alloc_for_each_employee[i_employee_id] == undefined)
					{
						a_res_alloc_for_each_employee[i_employee_id] = [o_resource_allocation];
						
						a_billable_employees.push(i_employee_id);
					}
				else
					{
						a_res_alloc_for_each_employee[i_employee_id].push(o_resource_allocation);
						
						a_exception_employees.push(i_employee_id);
					}
				
				if(o_resource_allocation.start_date > d_from_date  || o_resource_allocation.end_date < d_to_date)
					{
						a_exception_employees.push(i_employee_id);
					}
			}
		
		var a_search_data = getData(d_from_date, d_to_date, a_billable_employees, o_holidays);
		
		// Add Sublist
		var o_sublist = form.addSubList('custpage_time_entries', 'list', '');
		
		// Add Fields
		o_sublist.addField('custpage_employee', 'text', 'Employee');
		o_sublist.addField('custpage_employee_department', 'text', 'Department');
		o_sublist.addField('custpage_billed_hours', 'text', 'Billed Hours');
		o_sublist.addField('custpage_billed_amount', 'text', 'Billed Amount');
		o_sublist.addField('custpage_approved_hours', 'text', 'Approved Hours');
		o_sublist.addField('custpage_approved_amount', 'text', 'Approved Amount');
		o_sublist.addField('custpage_submitted_hours', 'text', 'Submitted Hours');
		o_sublist.addField('custpage_submitted_amount', 'text', 'Submitted Amount');
		o_sublist.addField('custpage_not_submitted_hours', 'text', 'Not Submitted Hours');
		o_sublist.addField('custpage_not_submitted_amount', 'text', 'Not Submitted Amount');
		o_sublist.addField('custpage_total_amount', 'float', 'Total Amount');
		o_sublist.addField('custpage_leave_hours', 'text', 'Leave Hours');
		o_sublist.addField('custpage_holiday_hours', 'text', 'Holiday Hours');
		o_sublist.addField('custpage_rate', 'text', 'Billable Rate');
		
		var a_data = new Array();
		for(var i_list_indx = 0; a_billable_employees != null && i_list_indx < a_billable_employees.length; i_list_indx++)
			{
				var i_employee_id = a_billable_employees[i_list_indx];
				
				var a_row_data = new Object();
				var o_search_result = a_search_data[i_employee_id];
				
				if(a_exception_employees.indexOf(i_employee_id) == -1)
					{
						a_row_data['custpage_employee'] = a_res_alloc_for_each_employee[i_employee_id][0].employee_name;
						a_row_data['custpage_employee_department'] = a_res_alloc_for_each_employee[i_employee_id][0].department;
						a_row_data['custpage_holiday_hours'] = a_res_alloc_for_each_employee[i_employee_id][0].holidays.length;
						if(o_search_result)
							{
								a_row_data['custpage_billed_hours'] = o_search_result.billed_hours;
								a_row_data['custpage_approved_hours'] = o_search_result.approved_hours;
								a_row_data['custpage_submitted_hours'] = o_search_result.submitted_hours;
								a_row_data['custpage_not_submitted_hours'] = ((i_working_days - a_row_data['custpage_holiday_hours'] - o_search_result.working_days_entered) * 8).toFixed(2);
								a_row_data['custpage_leave_hours'] = o_search_result.leave_hours;
								//a_row_data['custpage_holiday_hours'] = o_search_result.holiday_hours;
							}
						else
							{
								a_row_data['custpage_billed_hours'] = '0';
								a_row_data['custpage_approved_hours'] = '0';
								a_row_data['custpage_submitted_hours'] = '0';
								a_row_data['custpage_not_submitted_hours'] = ((i_working_days) * 8).toString();
								a_row_data['custpage_leave_hours'] = '0';
								//a_row_data['custpage_holiday_hours'] = '0';
							}
						a_row_data['custpage_rate'] = a_res_alloc_for_each_employee[i_employee_id][0].st_rate;
						a_row_data['custpage_billed_amount'] = parseFloat(parseFloat(a_row_data['custpage_billed_hours']) * parseFloat(a_row_data['custpage_rate'])).toFixed(2);
						a_row_data['custpage_approved_amount'] = parseFloat(parseFloat(a_row_data['custpage_approved_hours']) * parseFloat(a_row_data['custpage_rate'])).toFixed(2);
						a_row_data['custpage_submitted_amount'] = parseFloat(parseFloat(a_row_data['custpage_submitted_hours']) * parseFloat(a_row_data['custpage_rate'])).toFixed(2);
						a_row_data['custpage_not_submitted_amount'] = parseFloat(parseFloat(a_row_data['custpage_not_submitted_hours']) * parseFloat(a_row_data['custpage_rate'])).toFixed(2);
						a_row_data['custpage_total_amount'] = parseFloat(a_row_data['custpage_billed_amount']) + parseFloat(a_row_data['custpage_approved_amount']) + parseFloat(a_row_data['custpage_submitted_amount']) + parseFloat(a_row_data['custpage_not_submitted_amount']);
						a_data[i_list_indx] = a_row_data;
					}
				else if(a_exception_employees.indexOf(i_employee_id) != -1)
					{
						a_row_data['custpage_employee'] = a_res_alloc_for_each_employee[i_employee_id][0].employee_name;
						a_row_data['custpage_employee_department'] = a_res_alloc_for_each_employee[i_employee_id][0].department;
						a_row_data['custpage_holiday_hours'] = a_res_alloc_for_each_employee[i_employee_id][0].holidays.length;
						if(a_res_alloc_for_each_employee[i_employee_id].length == 1)
							{
								var d_rec_alloc_start_date = a_res_alloc_for_each_employee[i_employee_id][0].start_date;
								if(d_rec_alloc_start_date < d_from_date){d_rec_alloc_start_date = d_from_date;}
								var d_rec_alloc_end_date   = a_res_alloc_for_each_employee[i_employee_id][0].end_date;
								if(d_rec_alloc_end_date > d_to_date){d_rec_alloc_end_date = d_to_date;}
								
								var i_rec_alloc_working_days = calcBusinessDays(d_rec_alloc_start_date, d_rec_alloc_end_date);
								
								if(o_search_result)
									{
										a_row_data['custpage_billed_hours'] = o_search_result.billed_hours;
										a_row_data['custpage_approved_hours'] = o_search_result.approved_hours;
										a_row_data['custpage_submitted_hours'] = o_search_result.submitted_hours;
										a_row_data['custpage_not_submitted_hours'] = ((i_rec_alloc_working_days - a_row_data['custpage_holiday_hours'] - o_search_result.working_days_entered) * 8).toFixed(2);
										a_row_data['custpage_leave_hours'] = o_search_result.leave_hours;
										//a_row_data['custpage_holiday_hours'] = o_search_result.holiday_hours;
									}
								else
									{
										a_row_data['custpage_billed_hours'] = '0';
										a_row_data['custpage_approved_hours'] = '0';
										a_row_data['custpage_submitted_hours'] = '0';
										a_row_data['custpage_not_submitted_hours'] = ((i_rec_alloc_working_days) * 8).toFixed(2);
										a_row_data['custpage_leave_hours'] = '0';
										//a_row_data['custpage_holiday_hours'] = '0';
									}
							}
						else
							{
								/*for(var i = 0; i < a_res_alloc_for_each_employee[i_employee_id].length; i++)
									{
										var d_rec_alloc_start_date = a_res_alloc_for_each_employee[i_employee_id][i].start_date;
										if(d_rec_alloc_start_date < d_from_date){a_res_alloc_for_each_employee[i_employee_id][i].start_date = d_from_date;}
										var d_rec_alloc_end_date   = a_res_alloc_for_each_employee[i_employee_id][i].end_date;
										if(d_rec_alloc_end_date > d_to_date){a_res_alloc_for_each_employee[i_employee_id][i].end_date = d_to_date;}									
									}
								
								var total_percentage = 0;*/
								var isException = false;
								/*// Check for overlapping resource allocations
								o_first_res_alloc_for_this_employee = a_res_alloc_for_each_employee[i_employee_id][0];
								for(var i = 0; i < a_res_alloc_for_each_employee[i_employee_id].length; i++)
									{
										o_res_alloc_for_this_employee = a_res_alloc_for_each_employee[i_employee_id][i];
										
										if(o_res_alloc_for_this_employee.start_date != o_first_res_alloc_for_this_employee.start_date)
											{
												isException = true;
											}
										if(o_res_alloc_for_this_employee.end_date != o_first_res_alloc_for_this_employee.end_date)
											{
												isException = true;
											}
										if(o_res_alloc_for_this_employee.st_rate != o_first_res_alloc_for_this_employee.st_rate)
											{
												isException = true;
											}
										total_percentage += parseFloat(o_res_alloc_for_this_employee.percentoftime);
									}
								if(total_percentage != 100)
									{
										isException = true;
									}
								*/
								// Check for multiple full allocations
								/*if(isException == true)
									{*/
										isException = false;
										var i_total_working_days_for_this_employee = 0;
										var f_rate = a_res_alloc_for_each_employee[i_employee_id][0].st_rate;
										for(var i = 0; i < a_res_alloc_for_each_employee[i_employee_id].length; i++)
											{
												o_res_alloc_for_this_employee = a_res_alloc_for_each_employee[i_employee_id][i];
												if(parseFloat(o_res_alloc_for_this_employee.percentoftime) == 100.0)
													{
														i_total_working_days_for_this_employee += calcBusinessDays(o_res_alloc_for_this_employee.start_date, o_res_alloc_for_this_employee.end_date);
													}
												else
													{
														isException = true;
													}
												if(o_res_alloc_for_this_employee.st_rate != f_rate)
												{
													isException = true;
												}
											}
									
								
										if(i_total_working_days_for_this_employee != i_working_days)
										{
											isException = true;
										}
									/*}*/
								
								if(isException == false)
									{
									
										var d_rec_alloc_start_date = a_res_alloc_for_each_employee[i_employee_id][0].start_date;
										if(d_rec_alloc_start_date < d_from_date){d_rec_alloc_start_date = d_from_date;}
										var d_rec_alloc_end_date   = a_res_alloc_for_each_employee[i_employee_id][0].end_date;
										if(d_rec_alloc_end_date > d_to_date){d_rec_alloc_end_date = d_to_date;}
									
										var i_rec_alloc_working_days = calcBusinessDays(d_rec_alloc_start_date, d_rec_alloc_end_date);
									
										if(o_search_result)
										{
											a_row_data['custpage_billed_hours'] = o_search_result.billed_hours;
											a_row_data['custpage_approved_hours'] = o_search_result.approved_hours;
											a_row_data['custpage_submitted_hours'] = o_search_result.submitted_hours;
											a_row_data['custpage_not_submitted_hours'] = ((i_rec_alloc_working_days - a_row_data['custpage_holiday_hours'] - o_search_result.working_days_entered) * 8).toFixed(2);
											a_row_data['custpage_leave_hours'] = o_search_result.leave_hours;
											//a_row_data['custpage_holiday_hours'] = o_search_result.holiday_hours;
										}
									else
										{
											a_row_data['custpage_billed_hours'] = '0';
											a_row_data['custpage_approved_hours'] = '0';
											a_row_data['custpage_submitted_hours'] = '0';
											a_row_data['custpage_not_submitted_hours'] = ((i_rec_alloc_working_days) * 8).toFixed(2);
											a_row_data['custpage_leave_hours'] = '0';
											//a_row_data['custpage_holiday_hours'] = '0';
										}
									}
								else
									{
										if(o_search_result)
										{
											a_row_data['custpage_billed_hours'] = o_search_result.billed_hours;
											a_row_data['custpage_approved_hours'] = o_search_result.approved_hours;
											a_row_data['custpage_submitted_hours'] = o_search_result.submitted_hours;
											a_row_data['custpage_not_submitted_hours'] = '0';//((i_working_days - a_row_data['custpage_holiday_hours'] - o_search_result.working_days_entered) * 8).toString();
											a_row_data['custpage_leave_hours'] = o_search_result.leave_hours;
											//a_row_data['custpage_holiday_hours'] = o_search_result.holiday_hours;
										}
										else
										{
											a_row_data['custpage_billed_hours'] = '0';
											a_row_data['custpage_approved_hours'] = '0';
											a_row_data['custpage_submitted_hours'] = '0';
											a_row_data['custpage_not_submitted_hours'] = '0';//((i_working_days) * 8).toString();
											a_row_data['custpage_leave_hours'] = '0';
											//a_row_data['custpage_holiday_hours'] = '0';
										}
										/*a_row_data['custpage_billed_hours'] = 'Exception: ' + a_res_alloc_for_each_employee[i_employee_id].length;
										a_row_data['custpage_approved_hours'] = '0';
										a_row_data['custpage_submitted_hours'] = '0';
										a_row_data['custpage_not_submitted_hours'] = ((i_working_days) * 8).toString();
										a_row_data['custpage_leave_hours'] = '0';*/
										//a_row_data['custpage_holiday_hours'] = '0';
									}
							}
						
						a_row_data['custpage_rate'] = a_res_alloc_for_each_employee[i_employee_id][0].st_rate;
						a_row_data['custpage_billed_amount'] = parseFloat(parseFloat(a_row_data['custpage_billed_hours']) * parseFloat(a_row_data['custpage_rate'])).toFixed(2);
						a_row_data['custpage_approved_amount'] = parseFloat(parseFloat(a_row_data['custpage_approved_hours']) * parseFloat(a_row_data['custpage_rate'])).toFixed(2);
						a_row_data['custpage_submitted_amount'] = parseFloat(parseFloat(a_row_data['custpage_submitted_hours']) * parseFloat(a_row_data['custpage_rate'])).toFixed(2);
						a_row_data['custpage_not_submitted_amount'] = parseFloat(parseFloat(a_row_data['custpage_not_submitted_hours']) * parseFloat(a_row_data['custpage_rate'])).toFixed(2);
						a_row_data['custpage_total_amount'] = parseFloat(a_row_data['custpage_billed_amount']) + parseFloat(a_row_data['custpage_approved_amount']) + parseFloat(a_row_data['custpage_submitted_amount']) + parseFloat(a_row_data['custpage_not_submitted_amount']);
						a_data[i_list_indx] = a_row_data;
					}
			}
		o_sublist.setLineItemValues(a_data);
		o_sublist.setLabel('Billable Employee Count: ' + (o_sublist.getLineItemCount() == -1?'0':o_sublist.getLineItemCount()));
	}response.writePage(form);
}
// Get Active Resource Allocations
function getResourceAllocations(d_start_date, d_end_date, a_department, o_holidays)
{
	// Store resource allocations for project and employee
	var a_resource_allocations = new Array();
	
	// Get Resource allocations for this week
	var filters = new Array();
	//filters[0]	=	new nlobjSearchFilter('resource', null, 'anyof', i_employee_id);
	//filters[1]	=	new nlobjSearchFilter('project', null, 'anyof', i_project_id);
	filters[0]	=	new nlobjSearchFilter('custeventbstartdate', null, 'onorbefore', d_end_date);
	filters[1]	=	new nlobjSearchFilter('custeventbenddate', null, 'onorafter', d_start_date);
	filters[2]	=	new nlobjSearchFilter('custeventrbillable', null, 'is', 'T');
	if(a_department != null && a_department.length != 0)
		{
			filters[3]	=	new nlobjSearchFilter('department', 'employee', 'anyof', a_department);
		}

	var columns = new Array();
	columns[0]	= new nlobjSearchColumn('internalid', 'employee', 'group');
	columns[1]	= new nlobjSearchColumn('formuladate', null, 'group');
	columns[1].setFormula('CASE WHEN {custeventbstartdate} < TO_DATE(\'' + d_start_date.toJSON().substring(0,10) + '\', \'YYYY-MM-DD\') THEN TO_DATE(\'' + d_start_date.toJSON().substring(0,10) + '\', \'YYYY-MM-DD\') ELSE {custeventbstartdate} END'); 
	columns[2]	= new nlobjSearchColumn('formuladate', null, 'group');
	columns[2].setFormula('CASE WHEN {custeventbenddate} < TO_DATE(\'' + d_end_date.toJSON().substring(0,10) + '\', \'YYYY-MM-DD\') THEN TO_DATE(\'' + d_end_date.toJSON().substring(0,10) + '\', \'YYYY-MM-DD\') ELSE {custeventbenddate} END');
	columns[3]	= new nlobjSearchColumn('custeventrbillable', null, 'group');
	columns[4]  = new nlobjSearchColumn('custevent3', null, 'group'); // st rate
	//columns[5]	= new nlobjSearchColumn('custevent_otrate');
	//columns[6]  = new nlobjSearchColumn('project');
	columns[5]	= new nlobjSearchColumn('resource', null, 'group');
	columns[6]	= new nlobjSearchColumn('percentoftime', null, 'sum');
	columns[7]	= new nlobjSearchColumn('departmentnohierarchy', 'employee', 'group');
	columns[8]	= new nlobjSearchColumn('subsidiary', 'employee', 'group');
	columns[0].setSort();
	columns[1].setSort();
	
	var search_results = searchRecord('resourceallocation', null, filters, columns);
	
	if(search_results != null && search_results.length > 0)
	{
		for(var i_search_indx = 0; i_search_indx < search_results.length; i_search_indx++)
			{
				//var i_project_id = search_results[i_search_indx].getValue('project');
				var resource_allocation_start_date = nlapiStringToDate(search_results[i_search_indx].getValue(columns[1]), 'datetimetz');
				var resource_allocation_end_date   = nlapiStringToDate(search_results[i_search_indx].getValue(columns[2]), 'datetimetz');
				var is_resource_billable	= search_results[i_search_indx].getValue(columns[3]);
				var stRate					= search_results[i_search_indx].getValue(columns[4]);
				//var otRate					= search_results[i_search_indx].getValue('custevent_otrate');
				var i_employee				= search_results[i_search_indx].getValue(columns[0]);
				var s_employee_name			= search_results[i_search_indx].getText(columns[5]);
				var i_percent_of_time		= search_results[i_search_indx].getValue(columns[6]);
				var s_department			= search_results[i_search_indx].getValue(columns[7]);
				var i_subsidiary			= search_results[i_search_indx].getValue(columns[8]);
				a_resource_allocations[i_search_indx] = {
															//project_id: i_project_id,
															start_date:resource_allocation_start_date,
															end_date:resource_allocation_end_date,
															is_billable: is_resource_billable,
															st_rate: stRate,
															//ot_rate: otRate,
															'employee': i_employee,
															'employee_name': s_employee_name,
															'percentoftime': i_percent_of_time,
															'department': s_department,
															'holidays': (o_holidays[i_subsidiary] == undefined?[]:o_holidays[i_subsidiary])
														};
			}
	}
	else
	{
		a_resource_allocations = null;
	}
	
	return a_resource_allocations;
}
// Return data from the search
function getData(d_start_date, d_end_date, a_billable_employees, o_holidays)
{
	var a_holidays = [];
	var strExcludeHolidayEntries = '';
	if(o_holidays != null)
		{
			for(var i_subsidiary in o_holidays)
				{
					for(var i = 0; i < o_holidays[i_subsidiary].length; i++)
						{
							var s_date	=	o_holidays[i_subsidiary][i].date.toJSON().substring(0, 10);
							strExcludeHolidayEntries += ' OR ({employee.subsidiary.id} = TO_NUMBER(\'' + i_subsidiary + '\') AND {date} = TO_DATE(\''+s_date+'\', \'YYYY-MM-DD\'))';
						}
				}
		}
	var filters = new Array();
	filters[0] = new nlobjSearchFilter('date', null, 'within', d_start_date, d_end_date);
	if(a_billable_employees != null && a_billable_employees.length != 0)
		{
			filters[1] = new nlobjSearchFilter('employee', null, 'anyof', a_billable_employees);
		}
	var columns = new Array();
	columns[0] = new nlobjSearchColumn('formuladate', null, 'count');
	columns[0].setFormula('CASE WHEN TO_CHAR({date}, \'D\') = 1 OR TO_CHAR({date},\'D\') = 7 ' + strExcludeHolidayEntries + ' THEN NULL ELSE {date} END')
	
	// Search Records
	var search_results = searchRecord('timebill', 'customsearch_revenue_report_2', filters, columns);
	
	var a_data = new Object();
	
	for(var i = 0; i < search_results.length; i++)
		{
			var o_data = new Object();
			
			columns = search_results[i].getAllColumns();
			
			o_data.employee_id = search_results[i].getValue(columns[0]);
			o_data.employee = search_results[i].getText(columns[0]);
			o_data.billed_hours = search_results[i].getValue(columns[1]);
			o_data.approved_hours = search_results[i].getValue(columns[2]);
			o_data.submitted_hours = search_results[i].getValue(columns[3]);
			o_data.leave_hours = search_results[i].getValue(columns[4]);
			o_data.holiday_hours = search_results[i].getValue(columns[5]);
			o_data.working_days_entered = parseInt(search_results[i].getValue(columns[6]));
			a_data[o_data.employee_id] = o_data;
		}
	
	return a_data;
}

function searchRecord (recordType, savedSearch, arrFilters, arrColumns) {

	try {
		var search = null;

		// if a saved search is provided, load it and add the filters and
		// columns
		if (isNotEmpty(savedSearch)) {
			search = nlapiLoadSearch(recordType, savedSearch);
			search.addFilters(arrFilters);
			search.addColumns(arrColumns);
		}
		// create a new search
		else {
			search = nlapiCreateSearch(recordType, arrFilters, arrColumns);
		}

		// run search
		var resultSet = search.runSearch();

		// iterate through the search and get all data 1000 at a time
		var searchResultCount = 0;
		var resultSlice = null;
		var searchResult = [];

		do {
			resultSlice = resultSet.getResults(searchResultCount, searchResultCount + 1000);

			if (resultSlice) {

				resultSlice.forEach(function (result) {

					searchResult.push(result);
					searchResultCount++;
				});
			}
		}
		while (isArrayNotEmpty(resultSlice) && resultSlice.length >= 1000);

		return searchResult;
	}
	catch (err) {
		nlapiLogExecution('ERROR', 'searchRecord', err);
		throw err;
	}
}

function isArrayEmpty (argArray) {

	return !isArrayNotEmpty(argArray);
}

function isArrayNotEmpty (argArray) {

	return (isNotEmpty(argArray) && argArray.length > 0);
}

function isEmpty (value) {

	return value == null || value == "" || typeof (value) == undefined;
}

function isNotEmpty (value) {

	return !isEmpty(value);
}

function isTrue (value) {

	return value == 'T';
}

function isFalse (value) {

	return value != 'T';
}

function calcBusinessDays(d_startDate, d_endDate) { // input given as Date objects
    var startDate	=	new Date(d_startDate.getTime());
    var endDate		=	new Date(d_endDate.getTime());
	// Validate input
    if (endDate < startDate)
        return 0;
    
    // Calculate days between dates
    var millisecondsPerDay = 86400 * 1000; // Day in milliseconds
    startDate.setHours(0,0,0,1);  // Start just after midnight
    endDate.setHours(23,59,59,999);  // End just before midnight
    var diff = endDate - startDate;  // Milliseconds between datetime objects    
    var days = Math.ceil(diff / millisecondsPerDay);
    
    // Subtract two weekend days for every week in between
    var weeks = Math.floor(days / 7);
    var days = days - (weeks * 2);

    // Handle special cases
    var startDay = startDate.getDay();
    var endDay = endDate.getDay();
    
    // Remove weekend not previously removed.   
    if (startDay - endDay > 1)         
        days = days - 2;      
    
    // Remove start day if span starts on Sunday but ends before Saturday
    if (startDay == 0 && endDay != 6)
        days = days - 1  
            
    // Remove end day if span ends on Saturday but starts after Sunday
    if (endDay == 6 && startDay != 0)
        days = days - 1  
    
    return days;
     
    }

function searchSubDepartments(i_department_id)
{
	
	var s_department_name	=	nlapiLookupField('department', i_department_id, 'name');
		
	var filters = new Array();
	if(s_department_name.indexOf(' : ') == -1)
		{
			filters[0] = new nlobjSearchFilter('formulatext', null, 'is', s_department_name);
			filters[0].setFormula('TRIM(CASE WHEN INSTR({name} , \' : \', 1) > 0 THEN SUBSTR({name}, 1 , INSTR({name} , \' : \', 1)) ELSE {name} END)');
		}
	else
		{
			filters[0] = new nlobjSearchFilter('internalid', null, 'anyof', i_department_id);
		}
	var columns = new Array();
	columns[0] = new nlobjSearchColumn('internalid');
	
	var search_sub_departments = nlapiSearchRecord('department', null, filters, columns);
	
	var a_sub_departments = new Array();
	
	for(var i = 0; i < search_sub_departments.length; i++)
		{
			a_sub_departments.push(search_sub_departments[i].getValue('internalid'));
		}
	
	return a_sub_departments;
}

function getHolidays(d_start_date, d_end_date)
{
	var filters = new Array();
	filters[0]	=	new nlobjSearchFilter('custrecord_date', null, 'within', d_start_date, d_end_date);
	
	var columns = new Array();
	columns[0]	=	new nlobjSearchColumn('custrecord_date');
	columns[1]	=	new nlobjSearchColumn('custrecordsubsidiary');
	
	var search_holidays = nlapiSearchRecord('customrecord_holiday', null, filters, columns);
	
	var a_holidays = new Object();
	
	for(var i = 0; search_holidays != null && i < search_holidays.length; i++)
		{
			var i_subsidiary = search_holidays[i].getValue('custrecordsubsidiary');
			var o_holiday = {'date': nlapiStringToDate(search_holidays[i].getValue('custrecord_date'))};
			if(a_holidays[i_subsidiary] == undefined)
				{
					a_holidays[i_subsidiary] = [o_holiday];
				}
			else
				{
					a_holidays[i_subsidiary].push(o_holiday);
				}
		}
	
	return a_holidays;
}