/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       09 Feb 2022     shravan.k			Sets TS to Auto Approve 
 *
 */

/**
 * @returns {Void} Any or no return value
 */
function Set_Auto_Approve() 
{
	try
	{
		nlapiLogExecution('DEBUG', 'nlapiGetRecordId()==', nlapiGetRecordId());
		var s_Status = nlapiGetFieldValue('approvalstatus')
		nlapiLogExecution('DEBUG', 's_Status()==', s_Status);
		var i_Employee = nlapiGetFieldValue('employee');
		
		if(s_Status == 2 &&  i_Employee == 5706)
			{
			var objTSSearch = nlapiSearchRecord("timesheet", null,
		            [	
						["internalid", "anyof", nlapiGetRecordId()]
		            ],
		            [
		                new nlobjSearchColumn("internalid", "timeBill", null)
		            ]
		        );
		        for (var j = 0;objTSSearch!=null && objTSSearch.length > j; j++) 
		        {
		            var Obj_timebill_Rec = nlapiSubmitField('timebill', objTSSearch[j].getValue("internalid", "timeBill"), ['approvalstatus'], ['3']);
		            nlapiLogExecution('DEBUG', 'Obj_timebill_Rec-Submit', Obj_timebill_Rec);
			   } //// for (var j = 0;objTSSearch!=null && objTSSearch.length > j; j++)
			} //// if(s_Status == 2 &&  i_Employee == 5706)
		
	} /// End of try
	catch(s_Exp)
	{
		nlapiLogExecution('DEBUG', 's_Exp==', s_Exp)
	} /// End of catch
		
} //// End of Function function Set_Auto_Approve() 
