function PL_curencyexchange_rate(b_recentRecord,obj_rev_currencymapping) {
	var obj_rev_currencymapping=obj_rev_currencymapping;
	if(!obj_rev_currencymapping){
    var array_filters = [];
    array_filters.push(["custrecord_revenue_sourcetype", "anyof", "4"]);
	obj_rev_currencymapping = Search_revenue_Location_subsdidary(array_filters);
    }
	
	
    var a_dataVal = {};
    var column = new Array();
    column[0] = new nlobjSearchColumn('custrecord_pl_currency_exchange_revnue_r');
    column[1] = new nlobjSearchColumn('custrecord_pl_currency_exchange_cost');
    column[2] = new nlobjSearchColumn('custrecord_pl_currency_exchange_year');
    column[3] = new nlobjSearchColumn('custrecord_pl_currency_exchange_month');
    //ADD Conversion fields internal ids columns
    for (key in obj_rev_currencymapping["PLfldids"]) {
        column[column.length] = new nlobjSearchColumn(obj_rev_currencymapping["PLfldids"][key]);
    }

    if (b_recentRecord == 'T') {
        column[column.length] = new nlobjSearchColumn('internalid').setSort(true);
    }

    var currencySearch = nlapiSearchRecord('customrecord_pl_currency_exchange_rates', null, null, column);
    if (currencySearch) {

        //Fetch Recent P&L currency exchange rate table
        if (b_recentRecord == 'T') {
            var i_indx = 0;
            a_dataVal = {
                s_month: currencySearch[i_indx].getText('custrecord_pl_currency_exchange_month'),
                i_year: currencySearch[i_indx].getText('custrecord_pl_currency_exchange_year'),
            }
            a_dataVal['revenue'] = 1/parseFloat(currencySearch[i_indx].getValue('custrecord_pl_currency_exchange_revnue_r'));

            for (key in obj_rev_currencymapping.forecastExchangerates) {
                a_dataVal[key] = currencySearch[i_indx].getValue(obj_rev_currencymapping['forecastExchangerates'][key]);
                a_dataVal[key] = 1 / (parseFloat(a_dataVal[key]));
            }

            nlapiLogExecution("DEBUG", "a_dataVal", JSON.stringify(a_dataVal));

        } else {
            //Fetch all P&L currency exchange rate table data
            for (var i_indx = 0; i_indx < currencySearch.length; i_indx++) {
                var indexvalue = currencySearch[i_indx].getText('custrecord_pl_currency_exchange_month') + "_" + currencySearch[i_indx].getText('custrecord_pl_currency_exchange_year');
                a_dataVal[indexvalue] = {
                    s_month: currencySearch[i_indx].getText('custrecord_pl_currency_exchange_month'),
                    i_year: currencySearch[i_indx].getText('custrecord_pl_currency_exchange_year'),
                    'plcost': {},
                    'plrevenue': {}
                }

                for (var key in obj_rev_currencymapping.plcost) {

                    a_dataVal[indexvalue]['plcost'][key] = currencySearch[i_indx].getValue(obj_rev_currencymapping['plcost'][key]);
                }

                for (var key in obj_rev_currencymapping['plrevenue']) {
                    a_dataVal[indexvalue]['plrevenue'][key] = currencySearch[i_indx].getValue(obj_rev_currencymapping['plrevenue'][key]);
                }
				
				a_dataVal[indexvalue]['revenue'] = parseFloat(currencySearch[i_indx].getValue('custrecord_pl_currency_exchange_revnue_r'));

				for (key in obj_rev_currencymapping.forecastExchangerates) {
					
					a_dataVal[indexvalue][key] = currencySearch[i_indx].getValue(obj_rev_currencymapping['forecastExchangerates'][key]);
					a_dataVal[indexvalue][key] = (parseFloat(a_dataVal[indexvalue][key]));
				}

            }
        }
    }
    return a_dataVal;
}

//Calculate the P&L Cost rate based on currency and for INR check for the cost, revenue also.
function _Currency_exchangeRate(s_proj_currency, o_currencyexchange_rates, category) {
    var i_master_currency = 1;

    i_master_currency = o_currencyexchange_rates[s_proj_currency] ? o_currencyexchange_rates[s_proj_currency] : i_master_currency;
    if (s_proj_currency == 'INR' && category == "Revenue"){
      i_master_currency = o_currencyexchange_rates.revenue;
    }
        
	
    return i_master_currency;

}

function _conversionrateIntoUSD(books_id, trans_Accounting_line_Amount, category, exchangerate, pl_currencyexchangerate) {
    var trans_Accounting_line_Amount = trans_Accounting_line_Amount * exchangerate;

    var total_USD = 0;
	//Check for revenue exchange rate for the INDIA Books
    if ((category == "Revenue" || category == "Discount") && (pl_currencyexchangerate["plrevenue"][books_id])) {
        total_USD = parseFloat(trans_Accounting_line_Amount) / parseFloat(pl_currencyexchangerate["plrevenue"][books_id]);
        total_USD = parseFloat(total_USD.toFixed(3));
    } else if (pl_currencyexchangerate["plcost"][books_id])//Check for Others books exchange rates and India cost
	{
        total_USD = parseFloat(trans_Accounting_line_Amount) / parseFloat(pl_currencyexchangerate["plcost"][books_id]);
        total_USD = parseFloat(total_USD.toFixed(3));
    } else { 
        total_USD = trans_Accounting_line_Amount;
    }

return total_USD;
}

function Search_revenue_Location_subsdidary(array_filters) {
    var filters = [];
    filters.push(["isinactive", "is", "F"]);

    if (array_filters) {
        filters.push("And");
        filters.push(array_filters);
    }

    var obj_rev_loc_subSrch = nlapiSearchRecord("customrecord_revenue_fields_mapping", null,
        filters,
        [
            new nlobjSearchColumn("custrecord_revenue_sourcetype", null, "GROUP").setSort(false),
            new nlobjSearchColumn("custrecord_source_location", null, "GROUP"),
            new nlobjSearchColumn("custrecord_revenue_location", null, "GROUP"),
            new nlobjSearchColumn("custrecord_subsidairy", null, "GROUP"),
            new nlobjSearchColumn("custrecord_revenue_subsidiary", null, "GROUP"),
            new nlobjSearchColumn("custrecord_cost_location", null, "GROUP"),
            new nlobjSearchColumn("custrecord_cost_in_dollor", null, "GROUP"),
            new nlobjSearchColumn("custrecord_offsite_onsite", null, "GROUP"),
            new nlobjSearchColumn("custrecord_currency_symbol", null, "GROUP"),
            new nlobjSearchColumn("custrecord_plcurrency", null, "GROUP"),
            new nlobjSearchColumn("custrecord_pl_rate_mapping", null, "GROUP"),
            new nlobjSearchColumn("custrecord_pl_revenue_mapping", null, "GROUP")
        ]
    );

    var o_rev_loc_sub = {
        "Location": {},
        "Subsidairy": {},
        "Cost Location": {},
        "Cost Reference": {},
        "Onsite/Offsite": {},
        'CurrencySymbol': {},
        'forecastExchangerates': {},
        'plcost': {},
        'plrevenue': {},
        'PLfldids': {}
    };
	
	if(obj_rev_loc_subSrch){
		
    for (var i = 0; obj_rev_loc_subSrch.length > i; i++) {
        var i_sourcetype = obj_rev_loc_subSrch[i].getText("custrecord_revenue_sourcetype", null, "GROUP");
        switch (i_sourcetype) {
            case "Location":
                o_rev_loc_sub.Location[obj_rev_loc_subSrch[i].getValue('custrecord_source_location', null, "GROUP")] = obj_rev_loc_subSrch[i].getValue('custrecord_revenue_location', null, "GROUP");
                break;

            case "Subsidairy":
                o_rev_loc_sub["Subsidairy"]
                    [obj_rev_loc_subSrch[i].getValue('custrecord_subsidairy', null, "GROUP")] = obj_rev_loc_subSrch[i].getValue('custrecord_revenue_subsidiary', null, "GROUP");
                o_rev_loc_sub["Onsite/Offsite"]
                    [obj_rev_loc_subSrch[i].getValue('custrecord_subsidairy', null, "GROUP")] = obj_rev_loc_subSrch[i].getValue('custrecord_offsite_onsite', null, "GROUP");
                break;

            case "Cost Location":
                o_rev_loc_sub["Cost Location"][obj_rev_loc_subSrch[i].getValue('custrecord_source_location', null, "GROUP")] = obj_rev_loc_subSrch[i].getValue('custrecord_cost_location', null, "GROUP");
                o_rev_loc_sub["Cost Reference"][obj_rev_loc_subSrch[i].getValue('custrecord_source_location', null, "GROUP")] = obj_rev_loc_subSrch[i].getValue('custrecord_cost_in_dollor', null, "GROUP");
                break;
            case "Currency":

                var i_map_subsidairy = obj_rev_loc_subSrch[i].getValue('custrecord_revenue_subsidiary', null, "GROUP");
                var s_pl_revenue_map = obj_rev_loc_subSrch[i].getValue('custrecord_pl_revenue_mapping', null, "GROUP");
                var s_pl_cost_map = obj_rev_loc_subSrch[i].getValue('custrecord_pl_rate_mapping', null, "GROUP");
                var s_currencey = obj_rev_loc_subSrch[i].getText('custrecord_plcurrency', null, "GROUP");
                o_rev_loc_sub["forecastExchangerates"][s_currencey] = s_pl_cost_map;
                o_rev_loc_sub["CurrencySymbol"][s_currencey] = obj_rev_loc_subSrch[i].getValue('custrecord_currency_symbol', null, "GROUP");
                if (i_map_subsidairy) {
                    o_rev_loc_sub["plcost"][i_map_subsidairy] = s_pl_cost_map;
                }
                if (_logValidation(s_pl_revenue_map)) {
                    o_rev_loc_sub["plrevenue"][i_map_subsidairy] = s_pl_revenue_map;
                    o_rev_loc_sub["PLfldids"][s_pl_revenue_map] = s_pl_revenue_map;
                }

                if (!o_rev_loc_sub["PLfldids"][s_pl_cost_map] && _logValidation(s_pl_cost_map)) {
                    o_rev_loc_sub["PLfldids"][s_pl_cost_map] = s_pl_cost_map;
                }

                break;
        }
    }
	}
    return o_rev_loc_sub;
}



function _logValidation(value) {
    if (value != null && value!= '- None -' && value.toString() != null && value != '' && value != undefined && value.toString() != undefined && value != 'undefined' && value.toString() != 'undefined' && value.toString() != 'NaN' && value != NaN) {
        return true;
    } else {
        return false;
    }
}