function field_change(type,name)
{//fun start
if(type != 'delete')
	{//delete if start
	if(name == 'custrecord_fpr_per_completed')
		{//1 if start
				var i_input_rev_rec_id=nlapiGetRecordId();
				var i_project_id=nlapiGetFieldValue('custrecord_fpr_parent');
				var milestone=nlapiGetFieldValue('custrecord_fpr_ano_milestone');
				var month=nlapiGetFieldValue('custrecord_fpr_month');
				
				var i_sum_of_per=0;
			    if((i_project_id == '' || i_project_id == null || i_project_id == 'undefined') || (month == '' || month == null || month == 'undefined'))
			    {
			    	alert('Please select Month and Miletsone and Project Value');
			    }
			    else
			    	{
					var revenue_rec_filter=new Array();
					revenue_rec_filter[0]=new nlobjSearchFilter('custrecord_fpr_parent', null, 'is',i_project_id);
					revenue_rec_filter[1]=new nlobjSearchFilter('custrecord_fpr_ano_milestone', null, 'is',milestone);
					if(_logValidation(i_input_rev_rec_id))
						{
					revenue_rec_filter[2]=new nlobjSearchFilter('internalid', null, 'noneof',i_input_rev_rec_id);
						}
					//revenue_rec_filter[2]=new nlobjSearchFilter('custrecord_fpr_month', null, 'is',month);
				
					var revenue_rec_column=new Array();
					revenue_rec_column[0]=new nlobjSearchColumn('custrecord_fpr_per_completed');
				
					var search_Revenue_Rec=nlapiSearchRecord('customrecord_fpr_input_revenue_rec', null, revenue_rec_filter, revenue_rec_column);
					//nlapiLogExecution('DEBUG','search_Revenue_Rec',search_Revenue_Rec.length);
					//alert('search_Revenue_Rec'+search_Revenue_Rec.length);
					
					if(search_Revenue_Rec != null)
					{
						for(var jj=0 ; jj<search_Revenue_Rec.length ; jj++)
						{
				  	  
							var per_completed=parseFloat(search_Revenue_Rec[jj].getValue('custrecord_fpr_per_completed'));
							//alert('per_completed'+per_completed);
							// nlapiLogExecution('DEBUG','per_completed',per_completed);
					  
							i_sum_of_per=i_sum_of_per+per_completed;
							//alert('i_sum_of_per'+i_sum_of_per);
						}
					}
					
					var per_completed_new=parseFloat(nlapiGetFieldValue('custrecord_fpr_per_completed'));
					//alert('per_completed_new'+per_completed_new);
				
					i_sum_of_per=i_sum_of_per+per_completed_new;
				  	
				  	var effort_remain=100-i_sum_of_per;
					//alert('effort_remain'+effort_remain);
				  	
				  	//--------------------------validation-------------------------------
				  	var i_sum=i_sum_of_per+effort_remain;
				  //	alert('i_sum_of_per==========>'+i_sum_of_per);
				  	//alert('effort_remain==========>'+effort_remain);
				   if(parseFloat(i_sum_of_per) <= '100')//if(i_sum_of_per <= '100' && effort_remain >= '0')
				  		{
				  			//alert('in if')
					        nlapiSetFieldValue('custrecord_fpr_effort_expended',i_sum_of_per);
				  			nlapiSetFieldValue('custrecord_fpr_effort_remaining',effort_remain);
				  		}
				  	else
				  		{
				  		        if(i_sum_of_per > '100')
				  		        	{
				  		        	alert('Percentage Can Not Be Greater Than 100');
				  		        	nlapiSetFieldValue('custrecord_fpr_per_completed','');
				  		        	}
				  		        else
				  		        	{
				  				alert('Effort Expended And Effort Remaining Can Not Be Greater Than 100');
				  				nlapiSetFieldValue('custrecord_fpr_effort_expended','');
				  				nlapiSetFieldValue('custrecord_fpr_effort_remaining','');
				  		        	}
				  			
				  		}
				  	//-------------------------------------------------------------------
				   //if(per_completed_new == '100')
						 //  {
							   /* var revenue_rec_filter1=new Array();
								revenue_rec_filter1[0]=new nlobjSearchFilter('custrecord_fpr_parent', null, 'is',i_project_id);
								revenue_rec_filter1[1]=new nlobjSearchFilter('custrecord_fpr_ano_milestone', null, 'is',milestone);
								//revenue_rec_filter1[2]=new nlobjSearchFilter('custrecord_fpr_per_completed', null, 'is','100');
								//revenue_rec_filter[2]=new nlobjSearchFilter('custrecord_fpr_month', null, 'is',month);
							
								var revenue_rec_column1=new Array();
								revenue_rec_column1[0]=new nlobjSearchColumn('custrecord_fpr_per_completed');//customrecord_fpr_input_revenue_rec
							
								var search_Revenue_Rec1=nlapiSearchRecord('customrecord_fpr_input_revenue_rec',423, revenue_rec_filter1, null);
									//alert(search_Revenue_Rec1.length);
								if(search_Revenue_Rec1 != null)
									{
									
									  //var per_completed=search_Revenue_Rec1[0].getValue('custrecord_fpr_per_completed');
									  //if(per_completed > '100')
										//{
											alert('Milestone Has Completed 100%');
											nlapiSetFieldValue('custrecord_fpr_per_completed','',false);
											nlapiSetFieldValue('custrecord_fpr_effort_expended','');
							  				nlapiSetFieldValue('custrecord_fpr_effort_remaining','');
										//}
									}*/
						   //	}
			    }
			  	
		}//1 if close

//--------------------------------------------------------------------------
	if(name == 'custrecord_fpr_ano_milestone')
		{//if start
			//alert('project_task');
		var project_task=nlapiGetFieldValue('custrecord_fpr_ano_milestone');
		//alert('project_task'+project_task);
		var month=nlapiGetFieldValue('custrecord_fpr_month');
		var year=nlapiGetFieldValue('custrecord_fpr_year');
		var project_id=nlapiGetFieldValue('custrecord_fpr_parent');
		//------------------------------------------------------------
		if(project_task != null || project_task != '' || project_task != 'undefined')
			{//if start
				var project_task_load=nlapiLoadRecord('projectTask', project_task);
				var is_milestone=project_task_load.getFieldValue('ismilestone');
				//alert('is_milestone'+is_milestone);
				if(is_milestone == 'F')
				{
					alert('Please Select Only Milestone');
					nlapiSetFieldValue('custrecord_fpr_ano_milestone','',false);
				}
			}//if close
		//-----------------------------------------------------------
		if(month != null || month != '')
		{
			var miles_filter=new Array();
			miles_filter[0]=new nlobjSearchFilter('custrecord_fpr_ano_milestone', null,'is', project_task);
			miles_filter[1]=new nlobjSearchFilter('custrecord_fpr_month', null,'is', month);
			var search_milestone=nlapiSearchRecord('customrecord_fpr_input_revenue_rec', null, miles_filter, null);
			//alert('search_milestone.length'+search_milestone.length);
			if(search_milestone != null)
				{
					alert('Duplicate Milestone Entry');
					nlapiSetFieldValue('custrecord_fpr_ano_milestone','',false);
				}
		}
		//----------------------added by swati to add validation on 29-021-2015--------------------------------------
		
		var context = nlapiGetContext();

		var date_format = context.getPreference('dateformat');
		//alert('date_format'+date_format);
		if(year == 2)
		{
		year='2015'
		}
		else if(year == 3)
		{
		year='2016'
		}
		else if(year == 4)
		{
		year='2017'
		}
		if (date_format == 'YYYY-MM-DD')
			{
			 start_date=year+'/'+month+'/'+'1';
			 end_date=year+'/'+month+'/'+'31';
			}
		if (date_format == 'DD/MM/YYYY')
			{
			 start_date='1'+'/'+month+'/'+year;
			 end_date='31'+'/'+month+'/'+year;
			}
		if (date_format == 'MM/DD/YYYY')	
			{
			 start_date=month+'/'+'1'+'/'+year;
			 end_date=month+'/'+'31'+'/'+year;
			}
		//alert('start_date'+start_date);
		//alert('end_date'+end_date);
		//alert('project_task'+project_task);
		
		var miles_rec_filter=new Array();
		miles_rec_filter[0]=new nlobjSearchFilter('custrecord_fpr_milestone', null,'is', project_task);
		miles_rec_filter[1]=new nlobjSearchFilter('custrecordfpr_projparent', null,'is', project_id);//month
		//miles_rec_filter[2]=new nlobjSearchFilter('custrecord_fpr_date',null,'within','1/4/2015','31/4/2015');//
		//miles_rec_filter[2]=new nlobjSearchFilter('custrecord_fpr_date',null,'within',start_date,end_date);//
		
		var search_milestone=nlapiSearchRecord('customrecord_fpr_milestone_wi', null, miles_rec_filter, null);
		//alert('search_milestone.length'+search_milestone.length);
		if(search_milestone == null)
			{
				alert('Atleast One Milestone Entry Define For This Record');
				nlapiSetFieldValue('custrecord_fpr_ano_milestone','',false);
			}
		//-----------------------------------------------------------------------------------------------------------
			
		
		
		}//if close
	//--------------------------------------------------------------------------------
	}//delete if close
}//fun close
///////////////////////////////////////////////////////////////////////////////////
function save_rev_rec(type)
{//fun start
	var rec_id=nlapiGetRecordId();
	if(_logValidation(rec_id))
   {//create if start
   }//create if close
   else
		{//create else start
	var milestone=nlapiGetFieldValue('custrecord_fpr_ano_milestone');
	var d_month=nlapiGetFieldValue('custrecord_fpr_month');
	var d_year=nlapiGetFieldValue('custrecord_fpr_year');//
	var project_id=nlapiGetFieldValue('custrecord_fpr_parent');
	if((milestone == '' || milestone == null || milestone == 'undefined') && (month == '' || month == null || month == 'undefined') && (year == '' || year == null || year == 'undefined'))
		{//if start
		 return true;
		}
	else
		{
		 var rev_rec_filter=new Array();
		 rev_rec_filter[0]=new nlobjSearchFilter('custrecord_fpr_parent', null, 'is', project_id);
		 rev_rec_filter[1]=new nlobjSearchFilter('custrecord_fpr_month', null, 'is', d_month);
		 rev_rec_filter[2]=new nlobjSearchFilter('custrecord_fpr_year', null, 'is', d_year);
		 rev_rec_filter[3]=new nlobjSearchFilter('custrecord_fpr_ano_milestone', null, 'is', milestone);
		 
		 var rev_rec_column=new Array();
		 rev_rec_column[0]=new nlobjSearchColumn('custrecord_fpr_per_completed');
		 rev_rec_column[1]=new nlobjSearchColumn('custrecord_fpr_ano_milestone');
		 
		 var rev_rec_search=nlapiSearchRecord('customrecord_fpr_input_revenue_rec', null, rev_rec_filter,rev_rec_column);
		 if(rev_rec_search != null)
			 {//if start
			 	alert('You Can Not Create Revenue Record for same milestone and same month');
			 	return false;
			 
			 
			 }//if close
		 else
			 {
			 return true;
			 }
		}//if close
		 }//create else close
		 return true;
}//fun close

//------------------------------------------------------------
function _logValidation(value) 
{
 if(value!='null' && value != null && value != null && value != '' && value != undefined && value != undefined && value != 'undefined' && value!= 'undefined'&& value!= 'NaN' && value != NaN) 
 {
  return true;
 }
 else 
 { 
  return false;
 }
}