/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       31 Dec 2015     nitish.mishra
 *
 */

/**
 * @param {String}
 *            type Context Types: scheduled, ondemand, userinterface, aborted,
 *            skipped
 * @returns {Void}
 */
function scheduled(type) {
	try {
		var context = nlapiGetContext();

		var allocationList = [];
		var firstEndDate = "12/20/2015";
		var secondStart = "12/21/2015";
		var secondEnd = "12/31/2015";
		var thirdStart = "1/1/2016";
		var thirdEnd = "";

		for (var i = 0; i < allocationList.length; i++) {
			try {
				yieldScript(context);

				// end first allocation
				var allocationRecord = nlapiLoadRecord('resourceallocation',
				        allocationList[i], {
					        recordmode : 'dynamic'
				        });

				thirdEnd = allocationRecord.getFieldValue('enddate');

				if (allocationList[i] == 16554) {
					thirdEnd = "3/31/2016";
				}

				if (allocationList[i] == 20004) {
					thirdEnd = "3/31/2016";
				}

				nlapiLogExecution('debug', 'new end date', thirdEnd);

				allocationRecord.setFieldValue('enddate', firstEndDate);
				allocationRecord.setFieldValue('custeventbenddate',
				        firstEndDate);
				allocationRecord.setFieldValue('custevent_allocation_status',
				        '5');
				nlapiSubmitRecord(allocationRecord, false, true);
				nlapiLogExecution('audit', 'allocation ended', allocationRecord
				        .getId());

				// create non-billable allocation
				createNonBillableAllocation(allocationRecord, secondStart,
				        secondEnd);

				// create billable allocation
				createBillableAllocation(allocationRecord, thirdStart, thirdEnd);

			} catch (e) {
				nlapiLogExecution('error', 'allocation ' + allocationList[i], e);
			}
		}

	} catch (err) {
		nlapiLogExecution('ERROR', 'main', err);
	}
}

function createNonBillableAllocation(currentAllocation, secondStart, secondEnd)
{
	try {
		var allocationRecord = nlapiCreateRecord('resourceallocation');
		allocationRecord.setFieldValue('allocationresource', currentAllocation
		        .getFieldValue('allocationresource'));
		allocationRecord.setFieldValue('project', currentAllocation
		        .getFieldValue('project'));
		allocationRecord.setFieldValue('startdate', secondStart);
		allocationRecord.setFieldValue('enddate', secondEnd);
		allocationRecord.setFieldValue('custeventrbillable', 'F');
		allocationRecord.setFieldValue('custevent_allocation_status', '2');
		allocationRecord.setFieldValue('custevent3', 0);
		allocationRecord.setFieldValue('custevent2', '0.0');
		allocationRecord.setFieldValue('custevent4', currentAllocation
		        .getFieldValue('custevent4'));
		allocationRecord.setFieldValue('custevent1', currentAllocation
		        .getFieldValue('custevent1'));
		allocationRecord.setFieldValue('custeventwlocation', currentAllocation
		        .getFieldValue('custeventwlocation'));
		allocationRecord.setFieldValue('allocationamount', '100');
		allocationRecord.setFieldValue('allocationunit', 'P');
		var id = nlapiSubmitRecord(allocationRecord, true, true);
		nlapiLogExecution('audit', 'unbill allocation created',
		        currentAllocation.getFieldText('allocationresource') + " " + id);
	} catch (err) {
		nlapiLogExecution('error', 'unbill allocation failed',
		        currentAllocation.getFieldText('allocationresource')
		                + "  error : " + err);
		throw err;
	}
}

function createBillableAllocation(currentAllocation, thirdStart, thirdEnd) {
	try {
		var allocationRecord = nlapiCreateRecord('resourceallocation');
		allocationRecord.setFieldValue('allocationresource', currentAllocation
		        .getFieldValue('allocationresource'));
		allocationRecord.setFieldValue('project', currentAllocation
		        .getFieldValue('project'));
		allocationRecord.setFieldValue('startdate', thirdStart);
		allocationRecord.setFieldValue('enddate', thirdEnd);
		allocationRecord.setFieldValue('custeventbstartdate', thirdStart);
		allocationRecord.setFieldValue('custeventbenddate', thirdEnd);
		allocationRecord.setFieldValue('custeventrbillable', 'T');
		allocationRecord.setFieldValue('custevent_allocation_status', '1');
		allocationRecord.setFieldValue('custevent3', currentAllocation
		        .getFieldValue('custevent3'));
		allocationRecord.setFieldValue('custevent2', '0.0');
		allocationRecord.setFieldValue('custevent4', currentAllocation
		        .getFieldValue('custevent4'));
		allocationRecord.setFieldValue('custevent1', currentAllocation
		        .getFieldValue('custevent1'));
		allocationRecord.setFieldValue('custeventwlocation', currentAllocation
		        .getFieldValue('custeventwlocation'));
		allocationRecord.setFieldValue('allocationamount', '100');
		allocationRecord.setFieldValue('allocationunit', 'P');
		var id = nlapiSubmitRecord(allocationRecord, true, true);
		nlapiLogExecution('audit', 'bill allocation created', currentAllocation
		        .getFieldText('allocationresource')
		        + " " + id);
	} catch (err) {
		nlapiLogExecution('error', 'bill allocation failed', currentAllocation
		        .getFieldText('allocationresource')
		        + "  error : " + err);
		throw err;
	}
}

function yieldScript(currentContext) {

	if (currentContext.getRemainingUsage() <= 1000) {
		nlapiLogExecution('AUDIT', 'API Limit Exceeded');
		var state = nlapiYieldScript();

		if (state.status == "FAILURE") {
			nlapiLogExecution('ERROR', 'Yield Script Failed', 'Reason : '
			        + state.reason + ' / Size : ' + state.size);
			return false;
		} else if (state.status == "RESUME") {
			nlapiLogExecution('AUDIT', 'Script Resumed');
		}
	}
}
