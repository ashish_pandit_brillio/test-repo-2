 /**
 * Module Description
 * User Event script to handle employee record creation and updation from the XML file generated from Fusion
 * 
 * Version               Date                      Author                           Remarks
 * 1.00             13th October, 2015         Manikandan           This script saves the form data into 30 days questionnaire custom record from 30 days * questionnaire.html 
 *
 */


function feedbackSuitelet(request, response)
{
  var method=request.getMethod();
  if (method == 'GET' )
  {
    var file = nlapiLoadFile(70205);   //load the HTML file
    var contents = file.getValue();    //get the contents
    response.write(contents);          //render it on  suitlet
  
  }
  else
  {
  //var form = nlapiCreateForm("Suitelet - POST call");
  
  var feedback = new Object();
  feedback['Fusion_ID'] = request.getParameter('TextBox1');
  feedback['Name'] = request.getParameter('TextBox3');
  feedback['Email_ID'] = request.getParameter('TextBox2');
  feedback['Designation'] = request.getParameter('TextBox4');
  
  feedback['Response1'] = request.getParameter('OptionsGroup1');
  feedback['Comments1'] = request.getParameter('txtComments1');
  
  feedback['Response2'] = request.getParameter('OptionsGroup2');
  feedback['Comments2'] = request.getParameter('txtComments2');
  
  feedback['Response3'] = request.getParameter('OptionsGroup3');
  feedback['Comments3'] = request.getParameter('txtComments3');
  
  feedback['Response4'] = request.getParameter('OptionsGroup4');
  feedback['Response5'] = request.getParameter('OptionsGroup5');
  feedback['Response6'] = request.getParameter('OptionsGroup6');
  feedback['Response7'] = request.getParameter('OptionsGroup7');
  feedback['Response8'] = request.getParameter('OptionsGroup8');
  feedback['Response9'] = request.getParameter('OptionsGroup9');
  feedback['Response10'] = request.getParameter('OptionsGroup10');
  feedback['Response11'] = request.getParameter('OptionsGroup11');
  feedback['Response12'] = request.getParameter('OptionsGroup12');
  feedback['Response13'] = request.getParameter('OptionsGroup13');
  feedback['Response14'] = request.getParameter('OptionsGroup14');
  feedback['Response15'] = request.getParameter('OptionsGroup15');
  feedback['Response16'] = request.getParameter('OptionsGroup16');
  feedback['Response17'] = request.getParameter('OptionsGroup17');
  feedback['Response18'] = request.getParameter('OptionsGroup18');
  feedback['Response19'] = request.getParameter('OptionsGroup19');
 
  var status = saveRequest(feedback);
   
    var thanks_note = nlapiLoadFile(63444);   //load the HTML file
    var thanks_contents = thanks_note.getValue();    //get the contents
    response.write(thanks_contents);     //render it on  suitlet
   }

function saveRequest(feedback)
{
	try
	{
               var feedback_form = nlapiCreateRecord('customrecord_30_days_response_us');
               feedback_form.setFieldValue('custrecord_30_day_fusion_id', feedback.Fusion_ID);
	        feedback_form.setFieldValue('custrecord_30_day_name', feedback.Name);
               feedback_form.setFieldValue('custrecord_30_day_email', feedback.Email_ID);
	       feedback_form.setFieldValue('custrecord_30_day_designation', feedback.Designation);

			   
               feedback_form.setFieldValue('custrecord_q_1',"How was your hiring process at Brillio has been?");
               feedback_form.setFieldValue('custrecord_q_2', "How was your HR Onboarding experience?");
               feedback_form.setFieldValue('custrecord_q_3', "Did you receive timely support from all the teams like IS, IT, HR, Operations, travel, Finance, payroll etc. during your initial    days?");
			   feedback_form.setFieldValue('custrecord_q_4',"Checklist Status:Activities");
			   feedback_form.setFieldValue('custrecord_q_5',"Checklist Status:Welcome note");
			  feedback_form.setFieldValue('custrecord_q_6',"Checklist Status:Benefits Information(Insurance,401K, and FSA)");
			  feedback_form.setFieldValue('custrecord_q_7',"Checklist Status:Brillio email ID and access to various systems like Netsuite, yammer, Fusion, webmail etc");
			  feedback_form.setFieldValue('custrecord_q_8',"Checklist Status:Payroll Calendar and Holiday list");
			  feedback_form.setFieldValue('custrecord_q_9',"Checklist Status:Onboarding deck with information on Org structure, leadership and Processes and HR contact points");
			  feedback_form.setFieldValue('custrecord_q_10',"Checklist Status:You know your Brillio reporting manager");
			  feedback_form.setFieldValue('custrecord_q_11',"Checklist Status:You know your hierarchy and placement in the org structure");
			  feedback_form.setFieldValue('custrecord_q_12',"Checklist Status:Goal Setting completion");
			  feedback_form.setFieldValue('custrecord_q_13',"Checklist Status:Access to ADP payroll portal");
			  feedback_form.setFieldValue('custrecord_q_14',"Checklist Status:Access to 401K Portal");
			  feedback_form.setFieldValue('custrecord_q_15',"Checklist Status:Access to Benefits portal");
			  feedback_form.setFieldValue('custrecord_q_16',"Checklist Status:Have you enrolled or declined in the benefits?(You are required to do this within 30 days of your joining)");
			  feedback_form.setFieldValue('custrecord_q_17',"Checklist Status:Set up in Travel related portal(Egencia, Uber etc)");
			  feedback_form.setFieldValue('custrecord_q_18',"Checklist Status:I am aware of the employee engagement activities at my location");
			  feedback_form.setFieldValue('custrecord_q_19',"Checklist Status:Did you nominate for any training under Brillio Learning Academy");
              

                           feedback_form.setFieldValue('custrecord_answer_1', feedback.Response1);
                           feedback_form.setFieldValue('custrecord_answer_2', feedback.Response2);
                           feedback_form.setFieldValue('custrecord_answer_3', feedback.Response3);
			   feedback_form.setFieldValue('custrecord_answer_4', feedback.Response4);
			   feedback_form.setFieldValue('custrecord_answer_5', feedback.Response5);
			   feedback_form.setFieldValue('custrecord_answer_6', feedback.Response6);
			   feedback_form.setFieldValue('custrecord_answer_7', feedback.Response7);
			   feedback_form.setFieldValue('custrecord_answer_8', feedback.Response8);
			   feedback_form.setFieldValue('custrecord_answer_9', feedback.Response9);
			   feedback_form.setFieldValue('custrecord_answer_10', feedback.Response10);
			   feedback_form.setFieldValue('custrecord_answer_11', feedback.Response11);
			   feedback_form.setFieldValue('custrecord_answer_12', feedback.Response12);
			   feedback_form.setFieldValue('custrecord_answer_13', feedback.Response13);
			   feedback_form.setFieldValue('custrecord_answer_14', feedback.Response14);
			   feedback_form.setFieldValue('custrecord_answer_15', feedback.Response15);
			   feedback_form.setFieldValue('custrecord_answer_16', feedback.Response16);
			   feedback_form.setFieldValue('custrecord_answer_17', feedback.Response17);
			   feedback_form.setFieldValue('custrecord_answer_18', feedback.Response18);
			   feedback_form.setFieldValue('custrecord_answer_19', feedback.Response19);
               
                           feedback_form.setFieldValue('custrecord_recommendation_1', feedback.Comments1);
			   feedback_form.setFieldValue('custrecord_recommendation_2', feedback.Comments2);
			   feedback_form.setFieldValue('custrecord_recommendation_3', feedback.Comments3);
              
               
                         feedback_form.setFieldValue('custrecord_survey_classification',1);
     
              
                         var id = nlapiSubmitRecord(feedback_form, false,true);
                         nlapiLogExecution('debug', 'Record Saved', id);
        }
catch(err)
{
nlapiLogExecution('error', 'Record not saved', err);
feedback_form.setFieldValue('custrecord_error_log','');
}
}
 }