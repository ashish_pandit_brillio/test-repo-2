/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       07 Dec 2017     deepak.srinivas
 *
 */

/**
 * @param {Object} dataIn Parameter object
 * @returns {Object} Output object
 */


/*
//Production
 function getAccessToken() {
	try {
		var header = new Object();
	  	var clientId = '3MVG9A2kN3Bn17hs0GGObrz9z5wzHxHwrux8rii1yo0H71sigaPrDjfeHgY2xSpkr8LxpiWnPKww8aHBTQyCV';
        var clientSecret = '8986018429788350001';
        var username= 'netsuiteintegration@brillio.com';
        var password='Brillio@123in6luoqu4Tb0qaLsSPRr5LZRo';
        
        var param_header = 'grant_type=password&client_id='+clientId+'&client_secret='+clientSecret+'&username='+username+'&password='+password;
        
        var URL = 'https://login.salesforce.com/services/oauth2/token?'+param_header;
        return URL;
	} catch (err) {
		nlapiLogExecution('ERROR', 'getUserUsingEmailId', err);
		throw err;
	}
}
*/


//SANDBOX 
function getAccessToken() {
	try {
		var header = new Object();
	  	var clientId = '3MVG9jBOyAOWY5bW2ldeqQWvO4AOkvfetQmSW7pWelT4S_u0bbYKT5Bal4TQhNbwEZRsGpP.N.deERsAYQq5E';
        var clientSecret = 'FCE51B3B92237ABAFCF383C33415F1162C6CB3A8131878D8FC2E16C25352D123';
        var username= 'netsuiteintegration@brillio.com.prodimage';
        var password='Brillio@1234x4ulxL7vz0S5tZfMHxDRk0FFh';
        var param_header = 'grant_type=password&client_id='+clientId+'&client_secret='+clientSecret+'&username='+username+'&password='+password;
        
        var URL = 'https://test.salesforce.com/services/oauth2/token?'+param_header;
        return URL;
	} catch (err) {
		//nlapiLogExecution('ERROR', 'getUserUsingEmailId', err);
		throw err;
	}
}

function Staging_getAccessToken() {
	try {
		var header = new Object();
	  	var clientId = '3MVG9jBOyAOWY5bW2ldeqQWvO4AOkvfetQmSW7pWelT4S_u0bbYKT5Bal4TQhNbwEZRsGpP.N.deERsAYQq5E';
        var clientSecret = 'FCE51B3B92237ABAFCF383C33415F1162C6CB3A8131878D8FC2E16C25352D123';
        var username= 'netsuiteintegration@brillio.com.prodimage';
        //var password='passw0rd95qMSA7dGKGJuiWsMSD6RM96t';
        //var password='Brillio@1234';
        var password='Brillio@1234x4ulxL7vz0S5tZfMHxDRk0FFh';
        var param_header = 'grant_type=password&client_id='+clientId+'&client_secret='+clientSecret+'&username='+username+'&password='+password;
        
        var URL = 'https://test.salesforce.com/services/oauth2/token?'+param_header;
        return URL;
	} catch (err) {
		//nlapiLogExecution('ERROR', 'getUserUsingEmailId', err);
		throw err;
	}
}

//https://test.salesforce.com/services/oauth2/token?grant_type=password&client_id=3MVG9zZht._ZaMukJ1RbFjWMQq.7MEQ6ab9PY8RSovByBZdRONSNOaYv6.0ikUvBJlqdtmoMXULeF2CZ.bmOk&client_secret=8983827852806654161&username=netsuiteuser@brillio.com.netsuite&password=brill!123


