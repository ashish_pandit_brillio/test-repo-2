/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       23 Nov 2017     deepak.srinivas
 *
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType
 * 
 * @param {String} type Operation types: create, edit, delete, xedit,
 *                      approve, cancel, reject (SO, ER, Time Bill, PO & RMA only)
 *                      pack, ship (IF only)
 *                      dropship, specialorder, orderitems (PO only) 
 *                      paybills (vendor payments)
 * @returns {Void}
 */
function userEventAfterSubmit(type){
	
	try{
		if(type == 'create' || type == 'edit'){
          var JSON_OBJ  = {};
		var dataRow = [];
		var rec_Obj = nlapiLoadRecord(nlapiGetRecordType(),nlapiGetRecordId());
		var check_sfdc_update =  rec_Obj.getFieldValue('custrecord_actual_transaction_complete');
		
		if(check_sfdc_update == 'T'){
			
			
				JSON_OBJ = {
					'Month': rec_Obj.getFieldText('custrecord_actual_transaction_month'),
					'Year': rec_Obj.getFieldText('custrecord_actual_transaction_year'),
					'Month_End': 'Closed'
					
			};
				dataRow.push(JSON_OBJ);
				nlapiLogExecution('DEBUG','Response',JSON.stringify(dataRow));
			var temp = nlapiSubmitField('customrecord_actual_transaction_posting',nlapiGetRecordId(),'custrecord_actual_sfdc_update','T');	
			//Calling SFDC
		var status =	call_sfdc(dataRow,check_sfdc_update );
		
		
		
		}
		
		}    
	       
	       
			//End of SuiteScript  
	}
	
	catch(e){
		
		nlapiLogExecution('DEBUG','Process Error',e);
		throw e;
	}
  
}

function replacer(key, value){
    if (typeof value == "number" && !isFinite(value)){
        return String(value);
    }
    return value;
}

function objToString(obj, ndeep) {
	  if(obj == null){ return String(obj); }
	  switch(typeof obj){
	    case "string": return '"'+obj+'"';
	    case "function": return obj.name || obj.toString();
	    case "object":
	      var indent = Array(ndeep||1).join('\t'), isArray = Array.isArray(obj);
	      return '{['[+isArray] + Object.keys(obj).map(function(key){
	           return '\n\t' + indent + key + ': ' + objToString(obj[key], (ndeep||1)+1);
	         }).join(',') + '\n' + indent + '}]'[+isArray];
	    default: return obj.toString();
	  }
	}

function call_sfdc(dataRows,check_sfdc_update){
	try{
	//Start of SuiteScript
	
	var accessURL = getAccessToken();
	
    var method = 'POST';
    
 
    var response = nlapiRequestURL(accessURL,null,null,method);
    var temp = response.body;
    nlapiLogExecution('DEBUG','JSON',temp);
    var data = JSON.parse(response.body);
    var access_token_SFDC = data.access_token;
    var instance_URL_Sfdc = data.instance_url;
    nlapiLogExecution('DEBUG','JSON',data);
    
	
	//SFDC Rest URL
	var dynamic_URL = instance_URL_Sfdc+'/services/apexrest/MonthEndClosure/v1.0/';
		     
    
    var headers = {"Authorization": "Bearer "+access_token_SFDC,
    		 "Content-Type": "application/json",
    		 "accept": "application/json"};
    //
    var method_ = 'POST';
    var response_ = nlapiRequestURL(dynamic_URL,JSON.stringify(dataRows),headers,method_);
    var data = JSON.parse(response_.body);
    var message = data[0].message;
    nlapiLogExecution('DEBUG','JSON',message); 
    nlapiLogExecution('DEBUG','JSON',JSON.stringify(dataRows)); 
	}
	catch(e){
		nlapiLogExecution('DEBUG','SFDC Call error',e);
		
	}
}
function searchSOW(pro_id,pro_stDate,pro_endDate){
	try{
		var sow_id = '';
		var filters = [];
		var s_account_customer = '"'+pro_id+'"';
		nlapiLogExecution('DEBUG','Customer Filter ',s_account_customer);
		filters.push(new nlobjSearchFilter('internalid','job','anyof',pro_id));
		//if(pro_stDate)
		//filters.push(new nlobjSearchFilter('custbody_projectstartdate',null,'onorafter',pro_stDate));
	//	if(pro_endDate)
		//	filters.push(new nlobjSearchFilter('custbody_projectenddate',null,'onorbefore',pro_endDate));
		
		var cols = [];
		cols.push(new nlobjSearchColumn('internalid'));
		cols.push(new nlobjSearchColumn('tranid').setSort());
		
		
		var searchRecord = nlapiSearchRecord('salesorder',null,filters,cols);
		if(searchRecord)
			sow_id = searchRecord[0].getValue('tranid');
		nlapiLogExecution('DEBUG','SOW Search ',sow_id);
		
		return sow_id;
	}
	catch(e){
		nlapiLogExecution('DEBUG','Process Error',e);
		throw e;
	}
}

function credentials(){
    this.email = "deepak.srinivas@brillio.com";
    this.account = "3883006";
    this.role = "3";
    this.password = "Welcome@1992";
}
 
function replacer(key, value){
    if (typeof value == "number" && !isFinite(value)){
        return String(value);
    }
    return value;
}
//validate blank entries
function _logValidation(value) 
{
 if(value != null && value.toString() != null && value != '' && value != undefined && value.toString() != undefined && value != 'undefined' && value.toString() != 'undefined'&& value.toString() != 'NaN' && value != NaN) 
 {
  return true;
 }
 else 
 { 
  return false;
 }
}
