/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       25 Oct 2016     deepak.srinivas
 *
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType
 *   
 * @param {String} type Operation types: create, edit, view, copy, print, email
 * @param {nlobjForm} form Current form
 * @param {nlobjRequest} request Request object
 * @returns {Void}
 */
function userEventBeforeLoad(type, form, request){
	nlapiLogExecution('debug', 'Running');
	try{
		var file_type = nlapiGetFieldValue('custrecord_fi_emp_phy_addr_type');
		if(file_type == 5){
			try{
			var xmlData = nlapiStringToXML(nlapiGetFieldValue('custrecord_fi_xml_data'));
			var fusionID = nlapiSelectValue(xmlData,'//G_1/EMPLOYEE_ID');
			
			var emp_search = nlapiSearchRecord(
			        'employee',
			        null,
			        [
			                new nlobjSearchFilter(
			                        'custentity_fusion_empid',
			                        null, 'is',
			                        fusionID),
			                new nlobjSearchFilter(
			                        'custentity_employee_inactive',
			                        null, 'is', 'F') ],
			        [ new nlobjSearchColumn('internalid') ]);
			if(emp_search){
			var emp_id = emp_search[0].getValue('internalid');
			}
			if(emp_id){
				nlapiSetFieldValue('custrecord_fi_emp_phy_addr_employee',emp_id);
			
			
			
			
				var address1 ;
				var ADDRESS_LINE_1 = nlapiSelectValue(xmlData,'//G_1/ADDRESS_LINE_1');
				var ADDRESS_LINE_2 = nlapiSelectValue(xmlData,'//G_1/ADDRESS_LINE_2');
				var COUNTRY = nlapiSelectValue(xmlData,'//G_1/COUNTRY');
				var CITY = nlapiSelectValue(xmlData,'//G_1/CITY');
				var STATE = nlapiSelectValue(xmlData,'//G_1/STATE');
				var POSTAL_CODE = nlapiSelectValue(xmlData,'//G_1/POSTAL_CODE');
				var PHYSICAL_ADD = nlapiSelectValue(xmlData,'//G_1/PHYSICAL_ADD');
				var EFFECTIVE_START_DATE = nlapiSelectValue(xmlData,'//G_1/EFFECTIVE_START_DATE');
				var assignmentEffStartDate = nlapiStringToDate(EFFECTIVE_START_DATE);
				if(ADDRESS_LINE_1){
					address1 = PHYSICAL_ADD + ' , ' +ADDRESS_LINE_1;
				}
				else{
					address1 = PHYSICAL_ADD ;
				}
				var filters = Array();
				filters[0] = new nlobjSearchFilter('custrecord_physical_employee',null, 'anyof', emp_id);
				var cols = Array();
				cols[0] = new nlobjSearchColumn('custrecord_emp_phy_add_end_date').setSort(true);
				//cols[0] = new nlobjSearchColumn('custrecord_emp_phy_add_end_date');
				cols[1] = new nlobjSearchColumn('custrecord_emp_phy_add_st_date');
				cols[2] = new nlobjSearchColumn('custrecord_physical_address_1');
				
				cols[0].setSort(true);
				
				var search = nlapiSearchRecord('customrecord_empployee_physical_address',
				        null, filters ,cols);
				if(search){
					var phyAddressID = search[0].getId();
					var Phy_AddressStartDate = search[0].getValue('custrecord_emp_phy_add_st_date');
					var Phy_address_1 = search[0].getValue('custrecord_physical_address_1');
					nlapiLogExecution('DEBUG','EFFECTIVE_START_DATE','EFFECTIVE_START_DATE:'+EFFECTIVE_START_DATE+','+'Phy_AddressStartDate'+Phy_AddressStartDate);
					var no_of_days = getDatediffIndays(Phy_AddressStartDate,EFFECTIVE_START_DATE);
					EFFECTIVE_START_DATE = nlapiStringToDate(EFFECTIVE_START_DATE);
					Phy_AddressStartDate = nlapiStringToDate(Phy_AddressStartDate);
					if( parseInt(no_of_days) == 0 || address1 == Phy_address_1 ){
						return ;
					}
					Phy_AddressStartDate = nlapiStringToDate(Phy_AddressStartDate);
					var end_DATE = nlapiAddDays(nlapiStringToDate(EFFECTIVE_START_DATE), -1);
					if( Phy_AddressStartDate > end_DATE ){
						end_DATE = Phy_AddressStartDate;
					}
					nlapiSubmitField('customrecord_empployee_physical_address', phyAddressID,
					        'custrecord_emp_phy_add_end_date', nlapiDateToString(end_DATE));
					//nlapiSubmitField('customrecord_brillio_work_address_e', functionUpdateID,
					//        'custrecord_old_value_fu_update',Function_OLD );
				}
				//endActivePhysicalAddress(emp_id,startDatE);
				
				var createPhysicalAddress_rec = nlapiCreateRecord('customrecord_empployee_physical_address');
				createPhysicalAddress_rec.setFieldValue('custrecord_physical_address_1',address1);
				createPhysicalAddress_rec.setFieldValue('custrecord_physical_employee',emp_id);
				createPhysicalAddress_rec.setFieldValue('custrecord_physical_address_2',ADDRESS_LINE_2);
				createPhysicalAddress_rec.setFieldValue('custrecord_physical_city',CITY);
				createPhysicalAddress_rec.setFieldValue('custrecord_physical_state',STATE);
				createPhysicalAddress_rec.setFieldValue('custrecord_physical_country',COUNTRY);
				createPhysicalAddress_rec.setFieldValue('custrecord_physical_pincode',POSTAL_CODE);
				createPhysicalAddress_rec.setFieldValue('custrecord_emp_phy_add_st_date',EFFECTIVE_START_DATE);
				
				
				var physicalAdd_ID = nlapiSubmitRecord(createPhysicalAddress_rec);
				nlapiLogExecution('debug', 'Physical Address creation', 'physicalAdd_ID:'+physicalAdd_ID);
				
			
			
			}
			}
			catch(e){
				nlapiSetFieldValue('custrecord_fi_emp_phy_addr_log',e.message);
				return;
			}
		}
	}
	catch(e){
		nlapiLogExecution('DEBUG','Physical Address Error',e);
	}
}
function getDatediffIndays(startDate, endDate)
{
    var one_day=1000*60*60*24;
    var fromDate = startDate;
    var toDate=endDate

	var date1 = nlapiStringToDate(fromDate);

    var date2 = nlapiStringToDate(toDate);

    var date3=Math.round((date2-date1)/one_day);

    return date3;
}

