/**
 * List all the milestones of the project
 * 
 * Version Date Author Remarks 1.00 26 Jul 2016 Nitish Mishra
 * 
 */

function suitelet(request, response) {
	try {
		var projectId = request.getParameter('pi');
		var billingScheduleId = request.getParameter('bs');

		if (projectId && billingScheduleId) {
			var projectDetails = nlapiLookupField('job', projectId, [
			        'billingschedule', 'altname', 'entityid' ]);

			if (projectDetails.billingschedule == billingScheduleId) {
				var billingScheduleRec = nlapiLoadRecord('billingschedule',
				        billingScheduleId);

				var milestoneList = [];
				totalMilestoneCount = billingScheduleRec
				        .getLineItemCount('milestone');

				var milestoneListHtml = "";
				var cssFileUrl = "https://system.na1.netsuite.com/core/media/media.nl?id=219990&c=3883006&h=cb39138a6ff301175cc3&mv=ir30u32w&_xt=.css&whence=";

				milestoneListHtml += "<link href='" + cssFileUrl
				        + "' rel='stylesheet'/>";

				milestoneListHtml += "<div style='text-align:center; width:100%;'>";
				milestoneListHtml += "<table class='pd-data-table milestone-table'>";
				milestoneListHtml += "<tr class='pd-data-table-header'>";
				milestoneListHtml += "<td>Milestone</td>";
				milestoneListHtml += "<td>Amount</td>";
				milestoneListHtml += "<td>Estimated Completion Date</td>";
				milestoneListHtml += "<td>Actual Completion Date</td>";
				milestoneListHtml += "<td>Is Completed</td>";
				milestoneListHtml += "</tr>";

				for (var line = 1; line <= totalMilestoneCount; line++) {

					if (billingScheduleRec.getLineItemValue('milestone',
					        'milestonecompleted', line) == 'T') {
						milestoneListHtml += "<tr class='pd-data-table-row completed'>";
					} else {
						milestoneListHtml += "<tr class='pd-data-table-row in-progress'>";
					}

					milestoneListHtml += "<td>"
					        + billingScheduleRec.getLineItemText('milestone',
					                'projecttask', line) + "</td>";
					milestoneListHtml += "<td>"
					        + billingScheduleRec.getLineItemValue('milestone',
					                'comments', line) + "</td>";
					milestoneListHtml += "<td>"
					        + billingScheduleRec.getLineItemValue('milestone',
					                'milestonedate', line) + "</td>";
					milestoneListHtml += "<td>"
					        + billingScheduleRec.getLineItemValue('milestone',
					                'milestoneactualcompletiondate', line)
					        + "</td>";
					milestoneListHtml += "<td>"
					        + (billingScheduleRec.getLineItemValue('milestone',
					                'milestonecompleted', line) == 'T' ? "Yes"
					                : "No") + "</td>";
					milestoneListHtml += "</tr>";
				}

				milestoneListHtml += "</table>";
				milestoneListHtml += "</div>";

				var form = nlapiCreateForm(projectDetails.entityid + " "
				        + projectDetails.altname);
				form.addField('custpage_1', 'inlinehtml').setDefaultValue(
				        milestoneListHtml);
				response.writePage(form);
			} else {
				throw "Access not allowed";
			}
		} else {
			throw "No Project Id Provided";
		}
	} catch (err) {
		nlapiLogExecution("ERROR", 'Suitelet', err);
	}
}
