/**
 * Menu RESTlet
 * 
 * Version    Date            Author           Remarks
 * 1.00       25 Nov 2015     nitish.mishra
 * 1.1 Modified by Shravan on 20 March 2021
 */

/**
 * @param {Object}
 *            dataIn Parameter object
 * @returns {Object} Output object
 */

function postRESTlet(dataIn) {
	var response = new Response();
	var current_date = nlapiDateToString(new Date());
	nlapiLogExecution('debug', 'dataIn', JSON.stringify(dataIn));
	nlapiLogExecution('debug', 'Email id', dataIn.EmailId);
	try {
		var employeeId = getUserUsingEmailId(dataIn.EmailId);
		nlapiLogExecution('DEBUG', 'employeeId==', employeeId);
	
		if (employeeId) 
		{
			var d_Emp_Hire_Date = nlapiLookupField('employee', employeeId,'hiredate');
			d_Emp_Hire_Date = nlapiStringToDate(d_Emp_Hire_Date);
			nlapiLogExecution('DEBUG', 'd_Emp_Hire_Date==', d_Emp_Hire_Date);
		
			
			var b_Exp_Approver = false;
			var obj_Emp_Search_One = nlapiSearchRecord("employee",null,
					[
					   ["approver","anyof",employeeId]
					], 
					[
					   new nlobjSearchColumn("entityid").setSort(false)
					]
					);
			if(obj_Emp_Search_One)
				{
					b_Exp_Approver = true;
				} //// if(obj_Emp_Search_One)
			
			var b_Time_Approver = false;
			
			var obj_Emp_Search_Two = nlapiSearchRecord("employee",null,
					[
					   ["timeapprover","anyof",employeeId]
					], 
					[
					   new nlobjSearchColumn("entityid").setSort(false)
					]
					);
			if(obj_Emp_Search_Two)
			{
				b_Time_Approver = true;
			} //// if(obj_Emp_Search_Two)
			
			var b_PR_Approver = false;
			var obj_Proj_Search = nlapiSearchRecord("job",null,
					[
					   [["custentity_deliverymanager","anyof",employeeId]], 
					   "OR", 
					   [["custentity_practice.custrecord_practicehead","anyof",employeeId]],
					   "AND", 
					   ["status","anyof","2","4"]  /// Pending or active
					], 
					[
					   new nlobjSearchColumn("entityid").setSort(false)
					]
					);
			if(obj_Proj_Search)
				{
					b_PR_Approver = true;
				} ///if(obj_Proj_Search)
			var b_Tr_Approver = false;
			var obj_Practice_Search  = nlapiSearchRecord("department",null,
					[
					   ["isinactive","is","F"], 
					   "AND", 
					   ["custrecord_practicehead","anyof",employeeId]
					], 
					[
					   new nlobjSearchColumn("name").setSort(false)
					]
					);
			if(obj_Practice_Search)
				{ 
					b_Tr_Approver = true;
				} ///////if(obj_Practice_Search)
			
			var curr = new Date;
			var d_First_Day_Week  = new Date(curr.setDate(curr.getDate() - curr.getDay()));
			d_First_Day_Week = nlapiDateToString(d_First_Day_Week);
			nlapiLogExecution('DEBUG', 'd_First_Day_Week==', d_First_Day_Week);
			var d_last_Day_Week  = new Date(curr.setDate(curr.getDate() - curr.getDay()+6));
			d_last_Day_Week = nlapiDateToString(d_last_Day_Week);
			nlapiLogExecution('DEBUG', 'd_last_Day_Week==', d_last_Day_Week);
			var d_Sixth_Week_First_Day = nlapiDateToString(nlapiAddDays(nlapiStringToDate(d_last_Day_Week), -42), 'date');
			nlapiLogExecution('DEBUG', 'd_Sixth_Week_First_Day==', d_Sixth_Week_First_Day);
			var d_Eight_Week_First_Day = nlapiDateToString(nlapiAddDays(nlapiStringToDate(d_last_Day_Week), -55), 'date');
			nlapiLogExecution('DEBUG', 'd_Eight_Week_First_Day==', d_Eight_Week_First_Day);
			//var d_Sixth_Week_First_Day = nlapiDateToString(nlapiAddDays(nlapiStringToDate(d_First_Day_Week, 'mm/dd/yyyy'), -42), 'date');
			//nlapiLogExecution('DEBUG', 'd_Sixth_Week_First_Day==', d_Sixth_Week_First_Day);
			var i_Reference_Weeks ;
						d_last_Day_Week = nlapiStringToDate(d_last_Day_Week)
					var Difference_In_Time = d_Emp_Hire_Date.getTime() - d_last_Day_Week.getTime();
					var Difference_In_Days = Difference_In_Time / (1000 * 3600 * 24);
					
					if(parseFloat(Difference_In_Days) < 0 )
						{
							Difference_In_Days = parseFloat(Difference_In_Days)  * -1;
						}
					nlapiLogExecution('DEBUG', 'Difference_In_Days==', Difference_In_Days);
					i_Reference_Weeks = Math.ceil(Difference_In_Days/7);
					if(i_Reference_Weeks > parseInt(6)  )
						{
							i_Reference_Weeks = 6;
						}
					
			var lastday = new Date(curr.setDate(curr.getDate() - curr.getDay()+6));
			
						var obj_TS_Search = nlapiSearchRecord("timesheet",null,
					[
					   ["approvalstatusicon","anyof","2"], 
					   "AND", 
					   ["custrecord_ts_time_approver","anyof",employeeId], 
					   "AND", 
					   ["employee.custentity_employee_inactive","is","F"], 
					   "AND", 
					  // ["timesheetdate","onorafter","01/01/2020"]
					  ["timesheetdate","within",d_Eight_Week_First_Day,d_First_Day_Week]
					], 
					[
					   new nlobjSearchColumn("id").setSort(false), 
					   new nlobjSearchColumn("employee"), 
					   new nlobjSearchColumn("startdate"), 
					   new nlobjSearchColumn("totalhours"), 
					   new nlobjSearchColumn("approvalstatus")
					]
					);
			var i_Pending_TS_Count_For_Approval = 0;
			if(obj_TS_Search)
				{
					i_Pending_TS_Count_For_Approval = obj_TS_Search.length;
				} ///// if(obj_TS_Search)
			nlapiLogExecution('DEBUG', 'i_Pending_TS_Count_For_Approval==', i_Pending_TS_Count_For_Approval);
			
			
			var obj_TS_Search_Employee_Level_Rejected = nlapiSearchRecord("timesheet",null,
					[
					   ["approvalstatusicon","anyof","4"],  /// Rejected
					   "AND", 
					   ["timesheetdate","within",d_Sixth_Week_First_Day,d_First_Day_Week], 
					   "AND", 
					   ["employee","anyof",employeeId]
					], 
					[
					   new nlobjSearchColumn("id").setSort(false), 
					   new nlobjSearchColumn("employee"), 
					   new nlobjSearchColumn("startdate"), 
					   new nlobjSearchColumn("totalhours"), 
					   new nlobjSearchColumn("approvalstatus")
					]
					);
			
			var i_Pending_TS_Rejected = 0;
			if(obj_TS_Search_Employee_Level_Rejected)
				{
					i_Pending_TS_Rejected = obj_TS_Search_Employee_Level_Rejected.length;
				} ///// if(obj_TS_Search)
			nlapiLogExecution('DEBUG', 'i_Pending_TS_Rejected==', i_Pending_TS_Rejected);
			
			var obj_TS_Srch_Open = nlapiSearchRecord("timesheet",null,
					[
					   ["approvalstatusicon","anyof","1"], 
					   "AND", 
					   ["timesheetdate","within",d_Sixth_Week_First_Day,d_First_Day_Week], 
					   "AND", 
					   ["employee","anyof",employeeId]
					], 
					[
					   new nlobjSearchColumn("id").setSort(false), 
					   new nlobjSearchColumn("employee"), 
					   new nlobjSearchColumn("startdate"), 
					   new nlobjSearchColumn("totalhours"), 
					   new nlobjSearchColumn("approvalstatus")
					]
					);
			var i_Pending_TS_Open = 0;
			if(obj_TS_Srch_Open)
				{
				i_Pending_TS_Open = obj_TS_Srch_Open.length;
				} ///// if(obj_TS_Search)
			nlapiLogExecution('DEBUG', 'i_Pending_TS_Open==', i_Pending_TS_Open);
			
			var obj_TS_Srch_Submitted = nlapiSearchRecord("timesheet",null,
					[
					   ["approvalstatusicon","anyof","3","2"],  /// Approved and Submitted
					   "AND", 
					   ["timesheetdate","within",d_Sixth_Week_First_Day,d_First_Day_Week], 
					   "AND", 
					   ["employee","anyof",employeeId]
					], 
					[
					   new nlobjSearchColumn("id").setSort(false), 
					   new nlobjSearchColumn("employee"), 
					   new nlobjSearchColumn("startdate"), 
					   new nlobjSearchColumn("totalhours"), 
					   new nlobjSearchColumn("approvalstatus")
					]
					);
			var i_Pending_TS_Submitted = 0;
			if(obj_TS_Srch_Submitted)
				{
					i_Pending_TS_Submitted = obj_TS_Srch_Submitted.length;
				} ///// if(obj_TS_Search)
			nlapiLogExecution('DEBUG', 'i_Pending_TS_Submitted==', i_Pending_TS_Submitted);
			nlapiLogExecution('DEBUG', 'i_Reference_Weeks==', i_Reference_Weeks);
			var i_TS_Total_Pending = i_Reference_Weeks - ( parseInt(i_Pending_TS_Submitted));
			nlapiLogExecution('DEBUG', 'i_TS_Total_Pending==', i_TS_Total_Pending);
			
			var obj_Exp_Search = nlapiSearchRecord("expensereport",null,
					[
					   ["type","anyof","ExpRept"], 
					   "AND", 
					   ["status","anyof","ExpRept:A"], 
					   "AND", 
					   ["mainline","is","T"], 
					   "AND", 
					   ["employee","anyof",employeeId]
					], 
					[
					   new nlobjSearchColumn("tranid"), 
					   new nlobjSearchColumn("trandate").setSort(false), 
					   new nlobjSearchColumn("entityid","jobMain",null), 
					   new nlobjSearchColumn("companyname","jobMain",null), 
					   new nlobjSearchColumn("customer","jobMain",null), 
					   new nlobjSearchColumn("custcol_employeenamecolumn"), 
					   new nlobjSearchColumn("amount"), 
					   new nlobjSearchColumn("subsidiary"), 
					   new nlobjSearchColumn("statusref")
					]
					);
			
			var i_Employee_Exp_Rej = 0;
			if(obj_Exp_Search)
				{
				i_Employee_Exp_Rej = obj_Exp_Search.length;
				} ///// if(obj_TS_Search)
			nlapiLogExecution('DEBUG', 'i_Employee_Exp_Rej==', i_Employee_Exp_Rej);
			
			// Expenses
			var pendingExpenseSearch = nlapiSearchRecord('expensereport',
			        'customsearch_m_pending_expenses', [ new nlobjSearchFilter(
			                'custbody1stlevelapprover', null, 'anyof',
			                employeeId) ]);
		

			var pendingExpenseCount = 0;
			var totalExpensePendingAmount = 0;
			if (pendingExpenseSearch) 
			{
				flag_projectManager = true;
				pendingExpenseCount = pendingExpenseSearch[0].getValue(
				        'internalid', null, 'count');
				totalExpensePendingAmount = pendingExpenseSearch[0].getValue(
				        'netamount', null, 'sum');
			} ///// if (pendingExpenseSearch) 

			// PR Item
			
			  var pendingPRSearch = nlapiSearchRecord('customrecord_pritem',
			  null, [ [ [ 'custrecord_prastatus',
			  'anyof', '2' ], 'and', [ 'custrecord_subpracticehead', 'anyof',
			  employeeId ] ], 'or', [ [ 'custrecord_prastatus', 'anyof', '30' ],
			  'and', [ 'custrecord_pri_delivery_manager', 'anyof', employeeId ] ]
			  ],[new nlobjSearchColumn('internalid',null,'count')]);
			  
			  var pendingPrCount = 0; if (pendingPRSearch) { pendingPrCount =
			  pendingPRSearch[0].getValue('internalid', null, 'count'); }
			 nlapiLogExecution('DEBUG','pendingPrCount',pendingPrCount);
			//Enter Expense
			var enterExpenseCount = '';
			
		/*	var emp_lookUP = nlapiLookupField('employee',employeeId,['subsidiary']);
			var subsidiary_V = emp_lookUP.subsidiary;
			nlapiLogExecution('debug','subsidiary',subsidiary_V);
			if(parseInt(subsidiary_V) == 2){
			enterExpenseCount = '$';
			}
			else if(parseInt(subsidiary_V) == 3){
			enterExpenseCount = 'â‚¹';
			}
			nlapiLogExecution('debug','symbol',enterExpenseCount);*/

			// travel
			var pendingTravelSearch = nlapiSearchRecord(
			        'customrecord_travel_request',
			        'customsearch_m_pending_travel', [ new nlobjSearchFilter(
			                'custrecord_tr_approving_manager_id', null, 'is',
			                employeeId) ]);
			var pendingTravelCount = 0;
			if (pendingTravelSearch) {
				pendingTravelCount = pendingTravelSearch[0].getValue(
				        'internalid', null, 'count');
			}
			
			var obj_Emp_Search = nlapiSearchRecord("employee",null,
					[
					   ["hiredate","onorafter","lastmonthtodate"]
					], 
					[
					   new nlobjSearchColumn("entityid").setSort(false)
					]
					);   
			var i_New_joinee_Count = 0;
			if(obj_Emp_Search)
				{
				i_New_joinee_Count = obj_Emp_Search.length;
				} ///// if(obj_TS_Search)
			nlapiLogExecution('DEBUG', 'i_New_joinee_Count==', i_New_joinee_Count);

		
			pendingExpenseCount = parseInt(pendingExpenseCount);
			pendingTravelCount = parseInt(pendingTravelCount);
			pendingPrCount = parseInt(pendingPrCount);
			
			var obj_Final_Response_Json = {
					"count": {
					      "newJoineeCount":i_New_joinee_Count ,
					      "pendingTimesheetCount": i_TS_Total_Pending,   
					      "rejectedTimesheetCount": i_Pending_TS_Rejected,   
					      "rejectedExpenseCount": i_Employee_Exp_Rej ,    
					      "timesheetApprovalCount": i_Pending_TS_Count_For_Approval,   
					      "expenseApprovalCount": pendingExpenseCount ,    
					      "travelApprovalCount": pendingTravelCount,			
					      "prApprovalCount": pendingPrCount			
					    //  "hireApprovalCount": 0, 		
					    },
					    "approver": {
					      "isTimeSheetApprover": b_Time_Approver,
					      "isExpenseApprover": b_Exp_Approver,
					      "isTravelApprover": b_Tr_Approver,
					      "isPRApprover": b_PR_Approver
					     // "isHireApprover": false			//// hold
					    }		
			};
			
			response.Data = [obj_Final_Response_Json];
			nlapiLogExecution('DEBUG','response.Data',JSON.stringify(response.Data));
			response.Status = true;
		} else {
			throw "No Employee Id Provided";
		}
	} catch (err) {
		nlapiLogExecution('ERROR', 'postRESTlet', err);
		response.Data = err;
	}
	return response;
}

var sort_by = function(field, reverse, primer){

	   var key = primer ? 
	       function(x) {return primer(x[field])} : 
	       function(x) {return x[field]};

	   reverse = !reverse ? 1 : -1;

	   return function (a, b) {
	       return a = key(a), b = key(b), reverse * ((a > b) - (b > a));
	     } 
	}

function predicateBy(prop){
	   return function(a,b){
	      if( a[prop] > b[prop]){
	          return 1;
	      }else if( a[prop] < b[prop] ){
	          return -1;
	      }
	      return 0;
	   }
	}
//Function to remove duplicates
function removearrayduplicate(array) {
    var newArray = new Array();
    label: for (var i = 0; i < array.length; i++) {
          for (var j = 0; j < array.length; j++) {
                if (newArray[j] == array[i])
                      continue label;
          }
          newArray[newArray.length] = array[i];
    }
    return newArray;
}

function _logValidation(value) {
    if (value != null && value.toString() != null && value != '' && value != undefined && value.toString() != undefined && value != 'undefined' && value.toString() != 'undefined' && value.toString() != 'NaN' && value != NaN) {
        return true;
    } else {
        return false;
    }
}