function update_VB() {
    try {
        /*----------Added by Koushalya 28/12/2021---------*/
        var filters_customrecord_revenue_location_subsidiarySearch=["custrecord_offsite_onsite","anyof","2"];
        var obj_rev_loc_subSrch= Search_revenue_Location_subsdidary(filters_customrecord_revenue_location_subsidiarySearch);
        var obj_offsite_onsite=obj_rev_loc_subSrch["Onsite/Offsite"];
        /*-------------------------------------------------*/

        var current_date = nlapiDateToString(new Date());
        //Log for current date
        nlapiLogExecution('DEBUG', 'Execution Started Current Date', 'current_date...' + current_date);

        var timestp = timestamp();
        nlapiLogExecution('DEBUG', 'Execution Started Current Date', 'timestp...' + timestp);
        var counter = '';
        var context = nlapiGetContext();

        var je_list = new Array();
        var filter = new Array();
        var a_results_je = searchRecord('transaction', 'customsearch1773', null, null); //customsearch1773
        //var a_results_je = searchRecord('transaction', 'customsearch2914', null, null);	
		
		var excel_file_obj = '';	
        var err_row_excel = '';	
        var strVar_excel = '';	
        strVar_excel += '<table>';	
        strVar_excel += '	<tr>';	
        strVar_excel += ' <td width="100%">';	
        strVar_excel += '<table width="100%" border="1">';	
        strVar_excel += '	<tr>';	
        strVar_excel += ' <td width="6%" font-size="11" align="center">Transaction Type</td>';	
        strVar_excel += ' <td width="6%" font-size="11" align="center">Transaction Number</td>';	
        strVar_excel += ' <td width="6%" font-size="11" align="center">Transaction Internal ID</td>';	
        strVar_excel += ' <td width="6%" font-size="11" align="center">Error Message</td>';	
        strVar_excel += '	</tr>';
		
        if (_logValidation(a_results_je)) {
            for (var counter = 0; counter < a_results_je.length; counter++) {
				try{
                nlapiLogExecution('audit', 'j:-- ', counter);
                nlapiLogExecution('audit', 'a_results_je:-- ', a_results_je.length);

                nlapiLogExecution('audit', 'a_results_je[i].getId():-- ', a_results_je[counter].getId());

                if (je_list.indexOf(a_results_je[counter].getId()) >= 0) {

                } else {
                    var o_je_rcrd = nlapiLoadRecord('vendorbill', a_results_je[counter].getId()); //332853
					var transactionNum = o_je_rcrd.getFieldValue('tranid');	
                    var transaction_internal = a_results_je[counter].getId();
                    var i_expense_count = o_je_rcrd.getLineItemCount('expense');
                    var i_item_count = o_je_rcrd.getLineItemCount('item');
                    //Items
					
					var misPracticeFlagItem = true;	
					var prevMisPractice = '';	
					var misCounter = 0;
                    for (var i = 1; i <= i_item_count; i++) {
                        var usageEnd = context.getRemainingUsage();
                        if (usageEnd < 1000) {
                            yieldScript(context);
                        }

                        var emp_type = '';
                        var emp_fusion_id = '';
                        var person_type = '';
                        var onsite_offsite = '';
                        var s_employee_name_split = '';
                        var emp_frst_name = '';
                        var emp_middl_name = '';
                        var emp_lst_name = '';
                        var emp_full_name = '';
                        var emp_blnk_flag = 0;
                        var emp_practice = '';
                        var emp_practice_text = '';
                        var emp_location = '';
                        var employee_with_id = '';

                        var proj_name = '';
                        var proj_entity_id = '';
                        var proj_billing_type = '';
                        var s_proj_desc_split = '';
                        var region_id = '';
                        var proj_category = '';
                        var proj_vertical = '';
                        var proj_full_name_with_id = '';
                        var proj_practice = '';
                        var project_id = '';
                        var proj_category_val = '';
                        var core_practice = '';
                        var cust_name = '';
                        var cust_entity_id = '';
                        var cust_id = '';
                        var cust_internal_id = '';
                        var cust_territory = '';
                        var customer_id = '';
                        var customerObj = '';
                        var customer_region = '';
                        var cust_name_with_id = '';
                        var s_cust_id_split = '';
                        var misc_practice = '';
                        var parent_practice = '';
                        var s_employee_name = o_je_rcrd.getLineItemValue('item', 'custcol_employeenamecolumn', i);

                        var s_employee_id = o_je_rcrd.getLineItemValue('item', 'custcol_employee_entity_id', i);
						if (misCounter > 0 && !_logValidation(prevMisPractice)) {	
                                misPracticeFlagItem = false;	
                           }
                        if (s_employee_id) {
                            s_employee_name_split = s_employee_id;
                            nlapiLogExecution('audit', 's_employee_name_split', s_employee_name_split);
                        } else if (s_employee_name) {
                            var s_employee_name_id = s_employee_name.split('-');
                            s_employee_name_split = s_employee_name_id[0];
                            nlapiLogExecution('audit', 's_employee_name', s_employee_name);
                        }
                        if (s_employee_name || s_employee_id) {
                            //var s_employee_name_id = s_employee_name.split('-');
                            //var s_employee_name_split = s_employee_name_id[0];

                            //if(s_employee_name_id[1])
                            {
                                var filters_emp = new Array();
                                filters_emp[0] = new nlobjSearchFilter('entityid', null, 'startswith', s_employee_name_split);
                                var column_emp = new Array();
                                column_emp[0] = new nlobjSearchColumn('custentity_persontype');
                                column_emp[1] = new nlobjSearchColumn('employeetype');
                                column_emp[2] = new nlobjSearchColumn('subsidiary');
                                column_emp[3] = new nlobjSearchColumn('custentity_fusion_empid');
                                column_emp[4] = new nlobjSearchColumn('firstname');
                                column_emp[5] = new nlobjSearchColumn('middlename');
                                column_emp[6] = new nlobjSearchColumn('lastname');
                                column_emp[7] = new nlobjSearchColumn('department');
                                column_emp[8] = new nlobjSearchColumn('location');
                                column_emp[9] = new nlobjSearchColumn('custentity_legal_entity_fusion');
                                var a_results_emp_id = nlapiSearchRecord('employee', null, filters_emp, column_emp);
                                if (_logValidation(a_results_emp_id)) {
                                    var emp_id = a_results_emp_id[0].getId();
                                    emp_type = a_results_emp_id[0].getText('employeetype');
                                    person_type = a_results_emp_id[0].getText('custentity_persontype');
                                    emp_fusion_id = a_results_emp_id[0].getValue('custentity_fusion_empid');
                                    emp_frst_name = a_results_emp_id[0].getValue('firstname');
                                    emp_middl_name = a_results_emp_id[0].getValue('middlename');
                                    emp_lst_name = a_results_emp_id[0].getValue('lastname');
                                    if (emp_frst_name)
                                        emp_full_name = emp_frst_name;

                                    if (emp_middl_name)
                                        emp_full_name = emp_full_name + ' ' + emp_middl_name;

                                    if (emp_lst_name)
                                        emp_full_name = emp_full_name + ' ' + emp_lst_name;

                                    var emp_subsidiary = a_results_emp_id[0].getValue('subsidiary');
                                    if (emp_subsidiary) {
                                        if (emp_subsidiary == 3 && a_results_emp_id[0].getValue('custentity_legal_entity_fusion') == 'Brillio Technologies Private Limited UK') {
                                            onsite_offsite = 'Onsite';
                                        } 
                                        else{
                                            onsite_offsite = obj_offsite_onsite[emp_subsidiary]?'Offshore':'Onsite';    
                                        }
                                        /*else if (emp_subsidiary == 3 || emp_subsidiary == 9 || emp_subsidiary == 12) {
                                            onsite_offsite = 'Offsite';
                                        } else {
                                            onsite_offsite = 'Onsite';
                                        }*/
                                    }

                                    emp_practice = a_results_emp_id[0].getValue('department');
                                    emp_practice_text = a_results_emp_id[0].getText('department');
                                    if (emp_practice) {
                                        var is_practice_active_e = nlapiLookupField('department', parseInt(emp_practice), ['isinactive', 'custrecord_is_delivery_practice']);
                                        var isinactive_Practice_e = is_practice_active_e.isinactive;
                                        nlapiLogExecution('debug', 'isinactive_Practice_e', isinactive_Practice_e);
                                        core_practice = is_practice_active_e.custrecord_is_delivery_practice;
                                        nlapiLogExecution('debug', 'core_practice', core_practice);
                                    }
                                    emp_location = a_results_emp_id[0].getValue('location');
                                }
                            }
                        } else
                            emp_blnk_flag = 1;
                        //Customer
                        var cust_name_id = o_je_rcrd.getLineItemValue('item', 'custcolcustcol_temp_customer', i);
                        var cust_id = o_je_rcrd.getLineItemValue('item', 'custcol_customer_entityid', i);

                        var proj_desc = o_je_rcrd.getLineItemValue('item', 'custcolprj_name', i);
                        var proj_desc_id = o_je_rcrd.getLineItemValue('item', 'custcol_project_entity_id', i);
                        if (proj_desc_id) {
                            s_proj_desc_split = proj_desc_id;
                            nlapiLogExecution('audit', 's_proj_desc_split', s_proj_desc_split);
                        } else if (proj_desc) {
                            s_proj_desc_id = proj_desc.substr(0, 9);
                            s_proj_desc_split = s_proj_desc_id;
                            nlapiLogExecution('audit', 'proj_desc', proj_desc);
                        }

                        if (proj_desc || proj_desc_id) {
                            //var s_proj_desc_id = proj_desc.split(' ');
                            //var s_proj_desc_split = s_proj_desc_id[0];

                            //var s_proj_desc_id = proj_desc.substr(0,9);
                            //var s_proj_desc_split = s_proj_desc_id;
                            s_proj_desc_split = s_proj_desc_split.trim();
                            var filters_proj = new Array();
                            filters_proj[0] = new nlobjSearchFilter('entityid', null, 'startswith', s_proj_desc_split);
                            var column_proj = new Array();
                            column_proj[0] = new nlobjSearchColumn('jobbillingtype');
                            column_proj[1] = new nlobjSearchColumn('customer');
                            column_proj[2] = new nlobjSearchColumn('custentity_region');
                            column_proj[3] = new nlobjSearchColumn('entityid');
                            column_proj[4] = new nlobjSearchColumn('altname');
                            column_proj[5] = new nlobjSearchColumn('custentity_project_allocation_category');
                            column_proj[6] = new nlobjSearchColumn('companyname', 'customer');
                            column_proj[7] = new nlobjSearchColumn('entityid', 'customer');
                            column_proj[8] = new nlobjSearchColumn('custentity_vertical');
                            column_proj[9] = new nlobjSearchColumn('territory', 'customer');
                            column_proj[10] = new nlobjSearchColumn('custentity_practice');
                            column_proj[11] = new nlobjSearchColumn('internalid', 'customer');
                            column_proj[12] = new nlobjSearchColumn('internalid');
                            column_proj[13] = new nlobjSearchColumn('custentity_project_services');
                            column_proj[14] = new nlobjSearchColumn('custrecord_parent_practice', 'custentity_practice');

                            var a_results_proj = nlapiSearchRecord('job', null, filters_proj, column_proj);
                            if (a_results_proj) {
                                project_id = a_results_proj[0].getValue('internalid');
                                proj_billing_type = a_results_proj[0].getText('jobbillingtype');
                                customer_id = a_results_proj[0].getValue('internalid', 'customer');
                                nlapiLogExecution('debug', 'customer_id', customer_id + 'At expense:' + i);
                                region_id = a_results_proj[0].getText('custentity_region');
                                nlapiLogExecution('debug', 'region_id', region_id);
                                var proj_rcrd = nlapiLoadRecord('job', parseInt(project_id));
                                proj_name = proj_rcrd.getFieldValue('altname');
                                proj_entity_id = a_results_proj[0].getValue('entityid');
                                proj_category = a_results_proj[0].getText('custentity_project_allocation_category');
                                proj_vertical = a_results_proj[0].getValue('custentity_vertical');
                                proj_category_val = a_results_proj[0].getValue('custentity_project_allocation_category');
                                parent_practice = a_results_proj[0].getText('custrecord_parent_practice', 'custentity_practice');
                                if (customer_id) {
                                    customerObj = nlapiLoadRecord('customer', parseInt(customer_id));
                                    customer_region = customerObj.getFieldValue('custentity_region');
                                    nlapiLogExecution('debug', 'customer_region', customer_region);
                                }

                                //if(region_id == '' || region_id == null){
                                //customerObj = nlapiLookupField('customer',parseInt(customer_id),['custentity_region']);
                                //customer_region = customerObj.custentity_region;
                                //nlapiLogExecution('debug','customer_region',customer_region);
                                //}
                                if (emp_blnk_flag == 1)
                                    proj_practice = a_results_proj[0].getValue('custentity_practice');

                                if (proj_practice) {
                                    var is_practice_active = nlapiLookupField('department', parseInt(proj_practice), ['isinactive']);
                                    var isinactive_Practice = is_practice_active.isinactive;
                                    nlapiLogExecution('debug', 'isinactive_Practice', isinactive_Practice);
                                }

                                cust_name = a_results_proj[0].getValue('companyname', 'customer');
                                cust_entity_id = a_results_proj[0].getValue('entityid', 'customer');
                                cust_territory = a_results_proj[0].getValue('territory', 'customer');
                            }
                        }
                        if (!_logValidation(proj_desc) && !_logValidation(proj_desc_id)) {
                            if (_logValidation(cust_name_id)) {
                                var s_cust_id = cust_name_id.split(' ');
                                s_cust_id_split = s_cust_id[0];
                            } else if (_logValidation(cust_id)) {
                                s_cust_id_split = cust_id;
                            }
                            if (_logValidation(s_cust_id_split)) {
                                var filters_cust = new Array();
                                filters_cust[0] = new nlobjSearchFilter('entityid', null, 'startswith', s_cust_id_split);
                                var column_customer = new Array();

                                column_customer[0] = new nlobjSearchColumn('companyname');
                                column_customer[1] = new nlobjSearchColumn('entityid');

                                column_customer[2] = new nlobjSearchColumn('territory');

                                column_customer[3] = new nlobjSearchColumn('internalid');


                                var a_results_customer = nlapiSearchRecord('customer', null, filters_cust, column_customer);
                                if (a_results_customer) {
                                    cust_name = a_results_customer[0].getValue('companyname');
                                    cust_entity_id = a_results_customer[0].getValue('entityid');
                                    cust_territory = a_results_customer[0].getValue('territory');
                                }
                            }
                        }
                        o_je_rcrd.setLineItemValue('item', 'custcol_onsite_offsite', i, onsite_offsite);
                        o_je_rcrd.setLineItemValue('item', 'custcol_billing_type', i, proj_billing_type);
                        o_je_rcrd.setLineItemValue('item', 'custcol_employee_type', i, emp_type);
                        o_je_rcrd.setLineItemValue('item', 'custcol_person_type', i, person_type);
                        //Added for project region and Parent Practice of Executing Practice
                        o_je_rcrd.setLineItemValue('item', 'custcol_parent_executing_practice', i, parent_practice);
                        o_je_rcrd.setLineItemValue('item', 'custcol_project_region', i, region_id);
                        //
                        o_je_rcrd.setLineItemValue('item', 'custcol_customer_entityid', i, cust_entity_id);
                        var s_cust_name = o_je_rcrd.getLineItemValue('item', 'custcol_cust_name_on_a_click_report', i);
                        if (s_cust_name) {} else {
                            o_je_rcrd.setLineItemValue('item', 'custcol_cust_name_on_a_click_report', i, cust_name);
                        }
                        o_je_rcrd.setLineItemValue('item', 'custcol_territory', i, cust_territory);

                        if (cust_entity_id)
                            cust_name_with_id = cust_entity_id + ' ' + cust_name;

                        o_je_rcrd.setLineItemValue('item', 'custcolcustcol_temp_customer', i, cust_name_with_id);

                        o_je_rcrd.setLineItemValue('item', 'custcol_project_entity_id', i, proj_entity_id);
                        var s_proj_name = o_je_rcrd.getLineItemValue('item', 'custcol_proj_name_on_a_click_report', i);
                        if (s_proj_name) {} else {
                            o_je_rcrd.setLineItemValue('item', 'custcol_proj_name_on_a_click_report', i, proj_name);
                        }
                        //if(region_id){
                        //o_je_rcrd.setLineItemValue('item', 'custcol_region_master_setup', i, region_id);
                        //}
                        //else if(customer_region){

                        //}
                        o_je_rcrd.setLineItemValue('item', 'custcol_region_master_setup', i, customer_region);
                        o_je_rcrd.setLineItemValue('item', 'custcol_proj_category_on_a_click', i, proj_category);
                        o_je_rcrd.setLineItemValue('item', 'class', i, proj_vertical);

                        if (proj_entity_id)
                            proj_full_name_with_id = proj_entity_id + ' ' + proj_name;


                        o_je_rcrd.setLineItemValue('item', 'custcolprj_name', i, proj_full_name_with_id);

                        o_je_rcrd.setLineItemValue('item', 'custcol_employee_entity_id', i, emp_fusion_id);
                        var s_emp_name = o_je_rcrd.getLineItemValue('item', 'custcol_emp_name_on_a_click_report', i);
                        if (s_emp_name) {} else {
                            o_je_rcrd.setLineItemValue('item', 'custcol_emp_name_on_a_click_report', i, emp_full_name);
                        }
                        if (emp_fusion_id)
                            employee_with_id = emp_fusion_id + '-' + emp_full_name;

                        o_je_rcrd.setLineItemValue('item', 'custcol_employeenamecolumn', i, employee_with_id);
                        //
                        if (proj_category_val) {
                            if ((parseInt(proj_category_val) == parseInt(1)) && (core_practice == 'T')) {
                                misc_practice = emp_practice;
                                var is_practice_active = nlapiLookupField('department', parseInt(misc_practice), ['isinactive']);
                                isinactive_Practice_e = is_practice_active.isinactive;
                                misc_practice = emp_practice_text;
                            } else {
                                misc_practice = a_results_proj[0].getValue('custentity_practice');
                                var is_practice_active = nlapiLookupField('department', parseInt(misc_practice), ['isinactive']);
                                isinactive_Practice_e = is_practice_active.isinactive;
                                misc_practice = a_results_proj[0].getText('custentity_practice');
                            }
                        }
                        nlapiLogExecution('audit', 'Pratice', 'misc_practice:' + misc_practice);
                        if (misc_practice && isinactive_Practice_e == 'F') {
                            o_je_rcrd.setLineItemValue('item', 'custcol_mis_practice', i, misc_practice);
                        }
						
						misCounter++;	
                        prevMisPractice = o_je_rcrd.getLineItemValue('item', 'custcol_mis_practice', i);
						
                        //
                        var existing_practice = o_je_rcrd.getLineItemValue('item', 'department', i);
                        if (existing_practice) {

                        } else if (emp_practice && isinactive_Practice_e == 'F') {
                            o_je_rcrd.setLineItemValue('item', 'department', i, emp_practice);
                            nlapiLogExecution('audit', 'emp Name and emp Pratice', 'emp_practice:' + emp_practice + 'emp_fusion_id:' + emp_fusion_id);
                        } else if (proj_practice && isinactive_Practice == 'F') {
                            o_je_rcrd.setLineItemValue('item', 'department', i, proj_practice);
                            nlapiLogExecution('audit', 'Proj ID and Proj Pratice', 'proj_practice:' + proj_practice + 's_proj_desc_split:' + s_proj_desc_split);
                        }
						
						 

                        //o_je_rcrd.setLineItemValue('item', 'location', i, emp_location);
                    }
                    //End of Items
					var misPracticeFlagBill = true;	
                    prevMisPractice = '';	
                    misCounter = 0;
					
                    for (var i = 1; i <= i_expense_count; i++) {
                        var usageEnd = context.getRemainingUsage();
                        if (usageEnd < 1000) {
                            yieldScript(context);
                        }

                        var emp_type = '';
                        var emp_fusion_id = '';
                        var person_type = '';
                        var onsite_offsite = '';
                        var s_employee_name_split = '';
                        var emp_frst_name = '';
                        var emp_middl_name = '';
                        var emp_lst_name = '';
                        var emp_full_name = '';
                        var emp_blnk_flag = 0;
                        var emp_practice = '';
                        var emp_practice_text = '';
                        var emp_location = '';
                        var employee_with_id = '';

                        var proj_name = '';
                        var proj_entity_id = '';
                        var proj_billing_type = '';
                        var s_proj_desc_split = '';
                        var region_id = '';
                        var proj_category = '';
                        var proj_vertical = '';
                        var proj_full_name_with_id = '';
                        var proj_practice = '';
                        var project_id = '';
                        var proj_category_val = '';
                        var core_practice = '';
                        var cust_name = '';
                        var cust_entity_id = '';
                        var cust_id = '';
                        var cust_internal_id = '';
                        var cust_territory = '';
                        var customer_id = '';
                        var customerObj = '';
                        var customer_region = '';
                        var cust_name_with_id = '';
                        var s_cust_id_split = '';
                        var misc_practice = '';
                        var parent_practice = '';
                        var s_employee_name = o_je_rcrd.getLineItemValue('expense', 'custcol_employeenamecolumn', i);

                        var s_employee_id = o_je_rcrd.getLineItemValue('expense', 'custcol_employee_entity_id', i);
						
						if (misCounter > 0 && !_logValidation(prevMisPractice)) {	
							misPracticeFlagBill = false;	
						}
                        if (s_employee_id) {
                            s_employee_name_split = s_employee_id;
                            nlapiLogExecution('audit', 's_employee_name_split', s_employee_name_split);
                        } else if (s_employee_name) {
                            var s_employee_name_id = s_employee_name.split('-');
                            s_employee_name_split = s_employee_name_id[0];
                            nlapiLogExecution('audit', 's_employee_name', s_employee_name);
                        }
                        if (s_employee_name || s_employee_id) {
                            //var s_employee_name_id = s_employee_name.split('-');
                            //var s_employee_name_split = s_employee_name_id[0];

                            //if(s_employee_name_id[1])
                            {
                                var filters_emp = new Array();
                                filters_emp[0] = new nlobjSearchFilter('entityid', null, 'startswith', s_employee_name_split);
                                var column_emp = new Array();
                                column_emp[0] = new nlobjSearchColumn('custentity_persontype');
                                column_emp[1] = new nlobjSearchColumn('employeetype');
                                column_emp[2] = new nlobjSearchColumn('subsidiary');
                                column_emp[3] = new nlobjSearchColumn('custentity_fusion_empid');
                                column_emp[4] = new nlobjSearchColumn('firstname');
                                column_emp[5] = new nlobjSearchColumn('middlename');
                                column_emp[6] = new nlobjSearchColumn('lastname');
                                column_emp[7] = new nlobjSearchColumn('department');
                                column_emp[8] = new nlobjSearchColumn('location');
                                column_emp[9] = new nlobjSearchColumn('custentity_legal_entity_fusion');
                                var a_results_emp_id = nlapiSearchRecord('employee', null, filters_emp, column_emp);
                                if (_logValidation(a_results_emp_id)) {
                                    var emp_id = a_results_emp_id[0].getId();
                                    emp_type = a_results_emp_id[0].getText('employeetype');
                                    person_type = a_results_emp_id[0].getText('custentity_persontype');
                                    emp_fusion_id = a_results_emp_id[0].getValue('custentity_fusion_empid');
                                    emp_frst_name = a_results_emp_id[0].getValue('firstname');
                                    emp_middl_name = a_results_emp_id[0].getValue('middlename');
                                    emp_lst_name = a_results_emp_id[0].getValue('lastname');
                                    if (emp_frst_name)
                                        emp_full_name = emp_frst_name;

                                    if (emp_middl_name)
                                        emp_full_name = emp_full_name + ' ' + emp_middl_name;

                                    if (emp_lst_name)
                                        emp_full_name = emp_full_name + ' ' + emp_lst_name;

                                    var emp_subsidiary = a_results_emp_id[0].getValue('subsidiary');
                                    if (emp_subsidiary) {
                                        if (emp_subsidiary == 3 && a_results_emp_id[0].getValue('custentity_legal_entity_fusion') == 'Brillio Technologies Private Limited UK') {
                                            onsite_offsite = 'Onsite';
                                        } 
                                        else{
                                            onsite_offsite = obj_offsite_onsite[emp_subsidiary]?'Offshore':'Onsite';    
                                        }
                                        /*else if (emp_subsidiary == 3 || emp_subsidiary == 9 || emp_subsidiary == 12) {
                                            onsite_offsite = 'Offsite';
                                        } else {
                                            onsite_offsite = 'Onsite';
                                        }*/
                                    }

                                    emp_practice = a_results_emp_id[0].getValue('department');
                                    emp_practice_text = a_results_emp_id[0].getText('department');
                                    if (emp_practice) {
                                        var is_practice_active_e = nlapiLookupField('department', parseInt(emp_practice), ['isinactive', 'custrecord_is_delivery_practice']);
                                        var isinactive_Practice_e = is_practice_active_e.isinactive;
                                        nlapiLogExecution('debug', 'isinactive_Practice_e', isinactive_Practice_e);
                                        core_practice = is_practice_active_e.custrecord_is_delivery_practice;
                                        nlapiLogExecution('debug', 'core_practice', core_practice);
                                    }
                                    emp_location = a_results_emp_id[0].getValue('location');
                                }
                            }
                        } else
                            emp_blnk_flag = 1;
                        //Customer
                        var cust_name_id = o_je_rcrd.getLineItemValue('expense', 'custcolcustcol_temp_customer', i);
                        var cust_id = o_je_rcrd.getLineItemValue('expense', 'custcol_customer_entityid', i);

                        var proj_desc = o_je_rcrd.getLineItemValue('expense', 'custcolprj_name', i);
                        var proj_desc_id = o_je_rcrd.getLineItemValue('expense', 'custcol_project_entity_id', i);
                        if (proj_desc_id) {
                            s_proj_desc_split = proj_desc_id;
                            nlapiLogExecution('audit', 's_proj_desc_split', s_proj_desc_split);
                        } else if (proj_desc) {
                            s_proj_desc_id = proj_desc.substr(0, 9);
                            s_proj_desc_split = s_proj_desc_id;
                            nlapiLogExecution('audit', 'proj_desc', proj_desc);
                        }

                        if (proj_desc || proj_desc_id) {
                            //var s_proj_desc_id = proj_desc.split(' ');
                            //var s_proj_desc_split = s_proj_desc_id[0];

                            //var s_proj_desc_id = proj_desc.substr(0,9);
                            //var s_proj_desc_split = s_proj_desc_id;
                            s_proj_desc_split = s_proj_desc_split.trim();
                            var filters_proj = new Array();
                            filters_proj[0] = new nlobjSearchFilter('entityid', null, 'startswith', s_proj_desc_split);
                            var column_proj = new Array();
                            column_proj[0] = new nlobjSearchColumn('jobbillingtype');
                            column_proj[1] = new nlobjSearchColumn('customer');
                            column_proj[2] = new nlobjSearchColumn('custentity_region');
                            column_proj[3] = new nlobjSearchColumn('entityid');
                            column_proj[4] = new nlobjSearchColumn('altname');
                            column_proj[5] = new nlobjSearchColumn('custentity_project_allocation_category');
                            column_proj[6] = new nlobjSearchColumn('companyname', 'customer');
                            column_proj[7] = new nlobjSearchColumn('entityid', 'customer');
                            column_proj[8] = new nlobjSearchColumn('custentity_vertical');
                            column_proj[9] = new nlobjSearchColumn('territory', 'customer');
                            column_proj[10] = new nlobjSearchColumn('custentity_practice');
                            column_proj[11] = new nlobjSearchColumn('internalid', 'customer');
                            column_proj[12] = new nlobjSearchColumn('internalid');
                            column_proj[13] = new nlobjSearchColumn('custentity_project_services');
                            column_proj[14] = new nlobjSearchColumn('custrecord_parent_practice', 'custentity_practice');
                            var a_results_proj = nlapiSearchRecord('job', null, filters_proj, column_proj);
                            if (a_results_proj) {
                                project_id = a_results_proj[0].getValue('internalid');
                                proj_billing_type = a_results_proj[0].getText('jobbillingtype');
                                customer_id = a_results_proj[0].getValue('internalid', 'customer');
                                nlapiLogExecution('debug', 'customer_id', customer_id + 'At expense:' + i);
                                region_id = a_results_proj[0].getText('custentity_region');
                                nlapiLogExecution('debug', 'region_id', region_id);
                                var proj_rcrd = nlapiLoadRecord('job', parseInt(project_id));
                                proj_name = proj_rcrd.getFieldValue('altname');
                                proj_entity_id = a_results_proj[0].getValue('entityid');
                                proj_category = a_results_proj[0].getText('custentity_project_allocation_category');
                                proj_vertical = a_results_proj[0].getValue('custentity_vertical');
                                proj_category_val = a_results_proj[0].getValue('custentity_project_allocation_category');
                                parent_practice = a_results_proj[0].getText('custrecord_parent_practice', 'custentity_practice');
                                if (customer_id) {
                                    customerObj = nlapiLoadRecord('customer', parseInt(customer_id));
                                    customer_region = customerObj.getFieldValue('custentity_region');
                                    nlapiLogExecution('debug', 'customer_region', customer_region);
                                }

                                //if(region_id == '' || region_id == null){
                                //customerObj = nlapiLookupField('customer',parseInt(customer_id),['custentity_region']);
                                //customer_region = customerObj.custentity_region;
                                //nlapiLogExecution('debug','customer_region',customer_region);
                                //}
                                if (emp_blnk_flag == 1)
                                    proj_practice = a_results_proj[0].getValue('custentity_practice');

                                if (proj_practice) {
                                    var is_practice_active = nlapiLookupField('department', parseInt(proj_practice), ['isinactive']);
                                    var isinactive_Practice = is_practice_active.isinactive;
                                    nlapiLogExecution('debug', 'isinactive_Practice', isinactive_Practice);
                                }

                                cust_name = a_results_proj[0].getValue('companyname', 'customer');
                                cust_entity_id = a_results_proj[0].getValue('entityid', 'customer');
                                cust_territory = a_results_proj[0].getValue('territory', 'customer');
                            }
                        }
                        if (!_logValidation(proj_desc) && !_logValidation(proj_desc_id)) {
                            if (_logValidation(cust_name_id)) {
                                var s_cust_id = cust_name_id.split(' ');
                                s_cust_id_split = s_cust_id[0];
                            } else if (_logValidation(cust_id)) {
                                s_cust_id_split = cust_id;
                            }
                            if (_logValidation(s_cust_id_split)) {
                                var filters_cust = new Array();
                                filters_cust[0] = new nlobjSearchFilter('entityid', null, 'startswith', s_cust_id_split);
                                var column_customer = new Array();

                                column_customer[0] = new nlobjSearchColumn('companyname');
                                column_customer[1] = new nlobjSearchColumn('entityid');

                                column_customer[2] = new nlobjSearchColumn('territory');

                                column_customer[3] = new nlobjSearchColumn('internalid');


                                var a_results_customer = nlapiSearchRecord('customer', null, filters_cust, column_customer);
                                if (a_results_customer) {
                                    cust_name = a_results_customer[0].getValue('companyname');
                                    cust_entity_id = a_results_customer[0].getValue('entityid');
                                    cust_territory = a_results_customer[0].getValue('territory');
                                }
                            }
                        }
                        o_je_rcrd.setLineItemValue('expense', 'custcol_onsite_offsite', i, onsite_offsite);
                        o_je_rcrd.setLineItemValue('expense', 'custcol_billing_type', i, proj_billing_type);
                        o_je_rcrd.setLineItemValue('expense', 'custcol_employee_type', i, emp_type);
                        o_je_rcrd.setLineItemValue('expense', 'custcol_person_type', i, person_type);
                        //Added for project region and Parent Practice of Executing Practice
                        o_je_rcrd.setLineItemValue('expense', 'custcol_parent_executing_practice', i, parent_practice);
                        o_je_rcrd.setLineItemValue('expense', 'custcol_project_region', i, region_id);
                        //
                        o_je_rcrd.setLineItemValue('expense', 'custcol_customer_entityid', i, cust_entity_id);
                        var s_cust_name = o_je_rcrd.getLineItemValue('expense', 'custcol_cust_name_on_a_click_report', i);
                        if (s_cust_name) {} else {
                            o_je_rcrd.setLineItemValue('expense', 'custcol_cust_name_on_a_click_report', i, cust_name);
                        }
                        o_je_rcrd.setLineItemValue('expense', 'custcol_territory', i, cust_territory);

                        if (cust_entity_id)
                            cust_name_with_id = cust_entity_id + ' ' + cust_name;

                        o_je_rcrd.setLineItemValue('expense', 'custcolcustcol_temp_customer', i, cust_name_with_id);

                        o_je_rcrd.setLineItemValue('expense', 'custcol_project_entity_id', i, proj_entity_id);
                        var s_proj_name = o_je_rcrd.getLineItemValue('expense', 'custcol_proj_name_on_a_click_report', i);
                        if (s_proj_name) {} else {
                            o_je_rcrd.setLineItemValue('expense', 'custcol_proj_name_on_a_click_report', i, proj_name);
                        }
                        //if(region_id){
                        //o_je_rcrd.setLineItemValue('expense', 'custcol_region_master_setup', i, region_id);
                        //}
                        //else if(customer_region){

                        //}
                        o_je_rcrd.setLineItemValue('expense', 'custcol_region_master_setup', i, customer_region);
                        o_je_rcrd.setLineItemValue('expense', 'custcol_proj_category_on_a_click', i, proj_category);
                        o_je_rcrd.setLineItemValue('expense', 'class', i, proj_vertical);

                        if (proj_entity_id)
                            proj_full_name_with_id = proj_entity_id + ' ' + proj_name;


                        o_je_rcrd.setLineItemValue('expense', 'custcolprj_name', i, proj_full_name_with_id);

                        o_je_rcrd.setLineItemValue('expense', 'custcol_employee_entity_id', i, emp_fusion_id);
                        var s_emp_name = o_je_rcrd.getLineItemValue('expense', 'custcol_emp_name_on_a_click_report', i);
                        if (s_emp_name) {} else {
                            o_je_rcrd.setLineItemValue('expense', 'custcol_emp_name_on_a_click_report', i, emp_full_name);
                        }
                        if (emp_fusion_id)
                            employee_with_id = emp_fusion_id + '-' + emp_full_name;

                        o_je_rcrd.setLineItemValue('expense', 'custcol_employeenamecolumn', i, employee_with_id);
                        //
                        if (proj_category_val) {
                            if ((parseInt(proj_category_val) == parseInt(1)) && (core_practice == 'T')) {
                                misc_practice = emp_practice;
                                var is_practice_active = nlapiLookupField('department', parseInt(misc_practice), ['isinactive']);
                                isinactive_Practice_e = is_practice_active.isinactive;
                                misc_practice = emp_practice_text;
                            } else {
                                misc_practice = a_results_proj[0].getValue('custentity_practice');
                                var is_practice_active = nlapiLookupField('department', parseInt(misc_practice), ['isinactive']);
                                isinactive_Practice_e = is_practice_active.isinactive;
                                misc_practice = a_results_proj[0].getText('custentity_practice');
                            }
                        }
                        nlapiLogExecution('audit', 'Pratice', 'misc_practice:' + misc_practice);
                        if (misc_practice && isinactive_Practice_e == 'F') {
                            o_je_rcrd.setLineItemValue('expense', 'custcol_mis_practice', i, misc_practice);
                        }
						
						prevMisPractice = o_je_rcrd.getLineItemValue('expense', 'custcol_mis_practice', i);	
                        misCounter++;
						
						
                        //
                        var existing_practice = o_je_rcrd.getLineItemValue('expense', 'department', i);
                        if (existing_practice) {

                        } else if (emp_practice && isinactive_Practice_e == 'F') {
                            o_je_rcrd.setLineItemValue('expense', 'department', i, emp_practice);
                            nlapiLogExecution('audit', 'emp Name and emp Pratice', 'emp_practice:' + emp_practice + 'emp_fusion_id:' + emp_fusion_id);
                        } else if (proj_practice && isinactive_Practice == 'F') {
                            o_je_rcrd.setLineItemValue('expense', 'department', i, proj_practice);
                            nlapiLogExecution('audit', 'Proj ID and Proj Pratice', 'proj_practice:' + proj_practice + 's_proj_desc_split:' + s_proj_desc_split);
                        }

                        //o_je_rcrd.setLineItemValue('expense', 'location', i, emp_location);
                    }
					
					if (misPracticeFlagBill == false || misPracticeFlagItem == false) {	
                            err_row_excel += '	<tr>';	
                            err_row_excel += ' <td width="6%" font-size="11" align="center">' + 'vendorbill' + '</td>';	
                            err_row_excel += ' <td width="6%" font-size="11" align="center">' + transactionNum + '</td>';	
                            err_row_excel += ' <td width="6%" font-size="11" align="center">' + transaction_internal + '</td>';	
                            err_row_excel += ' <td width="6%" font-size="11" align="center">' + 'MIS practice is blank' + '</td>';	
                            err_row_excel += '	</tr>';	
                        }

                    o_je_rcrd.setFieldValue('custbody_is_je_updated_for_emp_type', 'T');
                    var je_submit_id = nlapiSubmitRecord(o_je_rcrd, {
                        disabletriggers: true,
                        enablesourcing: true
                    });
                    nlapiLogExecution('DEBUG', 'submitted_id', je_submit_id);
                    je_list.push(je_submit_id);
                }

                //yieldScript(context);

                //		if (context.getRemainingUsage() < 1000) 
                {
                    yieldScript(context);
                }
				} catch (err) {	
                    //Added try-catch logic by Sitaram 06/08/2021	
                    nlapiLogExecution('DEBUG', 'err', err);	
                    err_row_excel += '	<tr>';	
                    err_row_excel += ' <td width="6%" font-size="11" align="center">' + 'vendorbill' + '</td>';	
                    err_row_excel += ' <td width="6%" font-size="11" align="center">' + transactionNum + '</td>';	
                    err_row_excel += ' <td width="6%" font-size="11" align="center">' + transaction_internal + '</td>';	
                    err_row_excel += ' <td width="6%" font-size="11" align="center">' + err.message + '</td>';	
                    err_row_excel += '	</tr>';	
                    //continue;	
                }
            }
          if (_logValidation(err_row_excel)) {	
                var tailMail = '';	
                tailMail += '</table>';	
                tailMail += ' </td>';	
                tailMail += '</tr>';	
                tailMail += '</table>';	
                strVar_excel = strVar_excel + err_row_excel + tailMail	
                //excel_file_obj = generate_excel(strVar_excel);	
                var mailTemplate = "";	
                mailTemplate += '<html>';	
                mailTemplate += '<body>';	
                mailTemplate += "<p> This is to inform that following transactions are having Errors and therefore, system is not able to set the project/MIS mandatory fields</p>";	
                mailTemplate += "<p> Please make sure, transaction is updated with proper details else may not able to see the project/customer/employee details at Income Statement level.</p>";	
                mailTemplate += "<p><b> Script Id:- " + context.getScriptId() + "</b></p>";	
                mailTemplate += "<p><b> Script Deployment Id:- " + context.getDeploymentId() + "</b></p>";	
                mailTemplate += "<br/>"	
                mailTemplate += "<br/>"	
                mailTemplate += strVar_excel;	
                mailTemplate += "<br/>"	
                mailTemplate += "<p>Regards, <br/> Information Systems</p>";	
                mailTemplate += '</body>';	
                mailTemplate += '</html>';	
                nlapiSendEmail(442, 'ap@brillio.com', 'Issue with updating the MIS data for the transactions', mailTemplate, 'netsuite.support@brillio.com', null, null, null);	
            }
            nlapiLogExecution('DEBUG', 'usageEnd =' + usageEnd + '-->j-->' + counter);

        }
        var timestp_E = timestamp();
        nlapiLogExecution('DEBUG', 'Execution Ended Current Date', 'timestp_E...' + timestp_E);
    } catch (err) {
        nlapiLogExecution('ERROR', 'ERROR MESSAGE:-- ', err);
    }
}

function yieldScript(currentContext) {

    nlapiLogExecution('AUDIT', 'API Limit Exceeded');
    var state = nlapiYieldScript();

    if (state.status == "FAILURE") {
        nlapiLogExecution('ERROR', 'Yield Script Failed', 'Reason : ' +
            state.reason + ' / Size : ' + state.size);
        return false;
    } else if (state.status == "RESUME") {
        nlapiLogExecution('AUDIT', 'Script Resumed');
    }
}

function Schedulescriptafterusageexceeded() {
    try {
        var params = new Array();
        var startDate = new Date();
        params['startdate'] = startDate.toUTCString();
        var status = nlapiScheduleScript(nlapiGetContext().getScriptId(), nlapiGetContext().getDeploymentId(), params);
        //nlapiLogExecution('DEBUG', 'After Scheduling', 'Script scheduled status=' + status);
    } catch (e) {
        nlapiLogExecution('DEBUG', 'In Scheduled Catch', 'e : ' + e.message);
    }
}

function _logValidation(value) {
    if (value != null && value != '' && value != undefined && value.toString() != 'NaN' && value != NaN) {
        return true;
    } else {
        return false;
    }
}

function searchRecord(recordType, savedSearch, arrFilters, arrColumns,
    filterExpression) {

    try {
        var search = null;

        // if a saved search is provided, load it and add the filters and
        // columns
        if (isNotEmpty(savedSearch)) {
            search = nlapiLoadSearch(recordType, savedSearch);

            if (isArrayNotEmpty(arrFilters)) {
                search.addFilters(arrFilters);
            }

            if (isArrayNotEmpty(arrColumns)) {
                search.addColumns(arrColumns);
            }

            if (isArrayNotEmpty(filterExpression)) {
                search.setFilterExpression(filterExpression);
            }
        }
        // create a new search
        else {
            search = nlapiCreateSearch(recordType, arrFilters, arrColumns);
        }

        // run search
        var resultSet = search.runSearch();

        // iterate through the search and get all data 1000 at a time
        var searchResultCount = 0;
        var resultSlice = null;
        var searchResult = [];

        do {
            resultSlice = resultSet.getResults(searchResultCount,
                searchResultCount + 1000);

            if (resultSlice) {

                resultSlice.forEach(function(result) {

                    searchResult.push(result);
                    searchResultCount++;
                });
            }
        } while (isArrayNotEmpty(resultSlice) && resultSlice.length >= 1000);

        return searchResult;
    } catch (err) {
        nlapiLogExecution('ERROR', 'searchRecord', err);
        throw err;
    }
}

function isEmpty(value) {

    return value == null || value == "" || typeof(value) == undefined;
}

function isNotEmpty(value) {

    return !isEmpty(value);
}

function isArrayEmpty(argArray) {

    return !isArrayNotEmpty(argArray);
}

function isArrayNotEmpty(argArray) {

    return (isNotEmpty(argArray) && argArray.length > 0);
}

function timestamp() {
    var str = "";

    var currentTime = new Date();
    var hours = currentTime.getHours();
    var minutes = currentTime.getMinutes();
    var seconds = currentTime.getSeconds();
    var meridian = "";
    if (hours > 12) {
        meridian += "pm";
    } else {
        meridian += "am";
    }
    if (hours > 12) {

        hours = hours - 12;
    }
    if (minutes < 10) {
        minutes = "0" + minutes;
    }
    if (seconds < 10) {
        seconds = "0" + seconds;
    }
    str += hours + ":" + minutes + ":" + seconds + " ";

    return str + meridian;
}