/**
 * @NApiVersion 2.x
 * @NScriptType Suitelet
 */

var PAGE_SIZE = 1000;
var SEARCH_ID = 'customsearch3226';
var CLIENT_SCRIPT_FILE_ID = 2346293;

define(['N/ui/serverWidget', 'N/search', 'N/redirect', 'N/file', 'N/encode'],
		function (serverWidget, search, redirect, file, encode) {
	function onRequest(context) {
		if (context.request.method == 'GET') {
			var form = serverWidget.createForm({
				title: 'PO Balance Report',
				hideNavBar: false
			});

			form.clientScriptFileId = CLIENT_SCRIPT_FILE_ID;

			// Get parameters
			var pageId = parseInt(context.request.parameters.page);
			var scriptId = context.request.parameters.script;
			var deploymentId = context.request.parameters.deploy;
			var startDate = context.request.parameters.startdate;
			var endDate = context.request.parameters.endDate;
			var sub = context.request.parameters.subsidiary;
			var PrjStatus = context.request.parameters.jobstatus;
			var custoFilt = context.request.parameters.customer;
			var jobFilt = context.request.parameters.job;
			// Add UI objects 
			// form.addButton({
			//     id : 'exportexcelreport',
			//     label : 'Export Excel',
			//     functionName: "ExcelExport()"
			// });
			form.addSubmitButton({
				id: 'exportexcelreport',
				label: 'Export Excel'
			});
			form.addButton({
				id: 'filter',
				label: 'Search',
				functionName: 'Filteroptions(' + scriptId + ', ' + deploymentId + ')'
			});
			var stDate = form.addField({
				id: 'custpage_st_date',
				type: serverWidget.FieldType.DATE,
				label: 'Start Date'
			});
			var edDate =  form.addField({
				id: 'custpage_end_date',
				type: serverWidget.FieldType.DATE,
				label: 'End Date'
			});
			var customer =  form.addField({
				id: 'custpage_customer',
				type: serverWidget.FieldType.SELECT,
				label: 'Customer',
				source: 'customer'
			});
			var project =  form.addField({
				id: 'custpage_job',
				type: serverWidget.FieldType.SELECT,
				label: 'Project',
				source: 'job'
			});
			var subsid =  form.addField({
				id: 'custpage_subsidiary',
				type: serverWidget.FieldType.SELECT,
				label: 'Subsidiary',
				source: 'subsidiary'
			});
			var jobstatus =  form.addField({
				id: 'custpage_jobstatus',
				type: serverWidget.FieldType.SELECT,
				label: 'Project Status' //,
					//   source: 'jobstatus'
			});
			var JobOptions = searchJobStatus();
			for(var ro = 0 ; ro<JobOptions.length;ro++)
			{
				jobstatus.addSelectOption({
					value: JobOptions[ro].internalId,
					text: JobOptions[ro].name 
				});
			}
			if(PrjStatus)
			{
				jobstatus.defaultValue =PrjStatus;
			}
			else
			{
				jobstatus.defaultValue =  "2";
			}
			if(sub)
			{
				subsid.defaultValue = sub;
			}
			if(custoFilt)
			{
				customer.defaultValue = custoFilt;
			}
			if(jobFilt)
			{
				project.defaultValue = jobFilt;
			}
			
			if(startDate && endDate)
			{
				stDate.defaultValue = startDate;
				edDate.defaultValue = endDate;
			}
			// Add sublist that will show results
			var sublist = form.addSublist({
				id: 'sublist',
				type: serverWidget.SublistType.LIST,//,INLINEEDITOR
				label: 'Report'
			});

			// Add columns to be shown on Page

			sublist.addField({
				id: 'nsid',
				label: 'Internal ID',
				type: serverWidget.FieldType.TEXT
			});

			sublist.addField({
				id: 'customer',
				label: 'Customer',
				type: serverWidget.FieldType.TEXT
			});
			sublist.addField({
				id: 'project_id',
				label: 'Project Id',
				type: serverWidget.FieldType.TEXT
			});
			sublist.addField({
				id: 'po_check_number',
				label: 'PO/Check Number',
				type: serverWidget.FieldType.TEXT
			});
			sublist.addField({
				id: 'companyname',
				label: 'Project Name',
				type: serverWidget.FieldType.TEXT
			});
			sublist.addField({
				id: 'tranid',
				label: 'Document Number',
				type: serverWidget.FieldType.TEXT
			});
			sublist.addField({
				id: 'type',
				label: 'Record Type',
				type: serverWidget.FieldType.TEXT
			});
			sublist.addField({
				id: 'formulacurrency',
				label: 'PO Amount',
				type: serverWidget.FieldType.CURRENCY
			});
			sublist.addField({
				id: 'inv_formulacurrency',
				label: 'Invoice Amount',
				type: serverWidget.FieldType.CURRENCY
			});
			sublist.addField({
				id: 'open_po_amount',
				label: 'Open PO Amount',
				type: serverWidget.FieldType.CURRENCY
			});
			sublist.addField({
				id: 'trandate',
				label: 'Date',
				type: serverWidget.FieldType.DATE
			});
			// Run search and determine page count
			var PO_ID = '';

			if ((startDate && endDate) || sub || PrjStatus || custoFilt || jobFilt) 
			{
				log.debug("Calling with Filters");
				PO_ID = runSearch(SEARCH_ID, startDate, endDate, sub ,PrjStatus ,custoFilt,jobFilt);
			}
			else 
			{
				log.debug("Calling First time without Filters");
				var Prj = "2";
				PO_ID = runSearch(SEARCH_ID ,null,null,null, Prj);
			}


			var s = 0;
			var Excel_Data = [];
			for (var key in PO_ID) {
				var groupResultbyPO = PO_ID[key].reduce(function (r, a) {
					r[a.po_check_number] = r[a.po_check_number] || [];
					r[a.po_check_number].push(a);
					return r;
				}, Object.create(null));

				for (var key in groupResultbyPO) {
					var temp_amt = 0;
					var CR_SO_amt = 0;
					var aa = new Array();
					for (var i = 0; i < groupResultbyPO[key].length; i++) {

						if (groupResultbyPO[key][i].recordtype == "salesorder" || groupResultbyPO[key][i].recordtype == "creditmemo") {
							temp_amt = Number(temp_amt) + Number(groupResultbyPO[key][i].formulacurrency);
							CR_SO_amt = temp_amt;
						}
					}
					for (var j = 0; j < groupResultbyPO[key].length; j++) {
						if (groupResultbyPO[key][j].recordtype == "invoice") {
							temp_amt = Number(temp_amt) + Number(groupResultbyPO[key][j].formulacurrency);

							sublist.setSublistValue({
								id: 'nsid',
								line: s,
								value: groupResultbyPO[key][j].id
							});

							sublist.setSublistValue({
								id: 'customer',
								line: s,
								value: groupResultbyPO[key][j].customer
							});
							sublist.setSublistValue({
								id: 'project_id',
								line: s,
								value: groupResultbyPO[key][j].project_id
							});

							sublist.setSublistValue({
								id: 'po_check_number',
								line: s,
								value: groupResultbyPO[key][j].po_check_number
							});
							sublist.setSublistValue({
								id: 'companyname',
								line: s,
								value: groupResultbyPO[key][j].companyname
							});
							sublist.setSublistValue({
								id: 'tranid',
								line: s,
								value: groupResultbyPO[key][j].tranid
							});
							sublist.setSublistValue({
								id: 'type',
								line: s,
								value: groupResultbyPO[key][j].recordtype
							});
							sublist.setSublistValue({
								id: 'inv_formulacurrency',
								line: s,
								value: groupResultbyPO[key][j].formulacurrency
							});
							sublist.setSublistValue({
								id: 'open_po_amount',
								line: s,
								value: temp_amt
							});
							sublist.setSublistValue({
								id: 'trandate',
								line: s,
								value: groupResultbyPO[key][j].trandate
							});

							s++;
						}
						if (groupResultbyPO[key][j].recordtype == "salesorder" || groupResultbyPO[key][j].recordtype == "creditmemo") {

							sublist.setSublistValue({
								id: 'nsid',
								line: s,
								value: groupResultbyPO[key][j].id
							});

							sublist.setSublistValue({
								id: 'customer',
								line: s,
								value: groupResultbyPO[key][j].customer
							});
							sublist.setSublistValue({
								id: 'project_id',
								line: s,
								value: groupResultbyPO[key][j].project_id
							});
							sublist.setSublistValue({
								id: 'po_check_number',
								line: s,
								value: groupResultbyPO[key][j].po_check_number
							});
							sublist.setSublistValue({
								id: 'companyname',
								line: s,
								value: groupResultbyPO[key][j].companyname
							});
							sublist.setSublistValue({
								id: 'tranid',
								line: s,
								value: groupResultbyPO[key][j].tranid
							});
							sublist.setSublistValue({
								id: 'type',
								line: s,
								value: groupResultbyPO[key][j].recordtype
							});
							sublist.setSublistValue({
								id: 'formulacurrency',
								line: s,
								value: groupResultbyPO[key][j].formulacurrency
							});
							sublist.setSublistValue({
								id: 'open_po_amount',
								line: s,
								value: CR_SO_amt//groupResultbyPO[key][j].formulacurrency
							});
							sublist.setSublistValue({
								id: 'trandate',
								line: s,
								value: groupResultbyPO[key][j].trandate
							});
							s++;
						}
					}
				}
			}
			// Set data returned to columns
			//  var j = 0;
			// addResults.forEach(function (result[j]) 
			// {
			//     sublist.setSublistValue({
			//         id : 'id',
			//         line : j,
			//         value : result.id
			//     });

			//     sublist.setSublistValue({
			//         id : 'customer',
			//         line : j,
			//         value : result.customer
			//     });
			//     sublist.setSublistValue({
			//         id : 'po_check_number',
			//         line : j,
			//         value : result.po_check_number
			//     });
			//     sublist.setSublistValue({
			//         id : 'companyname',
			//         line : j,
			//         value : result.companyname
			//     });
			//     sublist.setSublistValue({
			//         id : 'tranid',
			//         line : j,
			//         value : result.tranid
			//     });
			//     sublist.setSublistValue({
			//         id : 'formulacurrency',
			//         line : j,
			//         value : result.formulacurrency
			//     });
			//     j++
			// });

			context.response.writePage(form);
		}
		else {
			var delimiter1 = /\u0001/;
			var delimiter = /\u0002/;
			var SO_ID = [];
			var sublistData = context.request.parameters.sublistdata.split(delimiter);
			log.debug('length', sublistData.length);
			var hardHeaders = ["INTERNAL ID ", "CUSTOMER","PROJECT ID" ,"PO/CHECK NUMBER", "PROJECT NAME", "DOCUMENT NUMBER", "RECORD TYPE", " PO AMOUNT", " INVOICE AMOUNT", " OPEN PO AMOUNT", "DATE"]
			var CSVData = '<?xml version="1.0"?><?mso-application progid="Excel.Sheet"?>';
			CSVData += '<Workbook xmlns="urn:schemas-microsoft-com:office:spreadsheet" ';
			CSVData += 'xmlns:o="urn:schemas-microsoft-com:office:office" ';
			CSVData += 'xmlns:x="urn:schemas-microsoft-com:office:excel" ';
			CSVData += 'xmlns:ss="urn:schemas-microsoft-com:office:spreadsheet" ';
			CSVData += 'xmlns:html="http://www.w3.org/TR/REC-html40">';

			CSVData += '<Styles>';

			CSVData += '<Style ss:ID="s1">';
			CSVData += '<Font ss:Bold="1" ss:Underline="Single"/>';
			CSVData += '<Interior ss:Color="#CCFFFF" ss:Pattern="Solid"/>';
			CSVData += ' <Borders>';
			CSVData += ' <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>';
			CSVData += '</Borders>';
			CSVData += '</Style>';

			CSVData += '</Styles>';

			CSVData += '<Worksheet ss:Name="Report">';
			CSVData += '<Table><Row>';
			for (var k = 0; k < hardHeaders.length; k++) {
				CSVData += '<Cell ss:StyleID="s1"><Data ss:Type="String">' + hardHeaders[k] + '</Data></Cell>';
			}
			CSVData += '</Row>';
			for (var i = 0; i < sublistData.length; i++) {
				var sublistDataDetails = sublistData[i].split(delimiter1);
				CSVData += '<Row>';
				CSVData += '<Cell><Data ss:Type="String">' + sublistDataDetails[0] + '</Data></Cell>';
				CSVData += '<Cell><Data ss:Type="String">' + sublistDataDetails[1] + '</Data></Cell>';
				CSVData += '<Cell><Data ss:Type="String">' + sublistDataDetails[2] + '</Data></Cell>';
				CSVData += '<Cell><Data ss:Type="String">' + sublistDataDetails[3] + '</Data></Cell>';
				CSVData += '<Cell><Data ss:Type="String">' + sublistDataDetails[4] + '</Data></Cell>';
				CSVData += '<Cell><Data ss:Type="String">' + sublistDataDetails[5] + '</Data></Cell>';
				CSVData += '<Cell><Data ss:Type="String">' + sublistDataDetails[6] + '</Data></Cell>';
				CSVData += '<Cell><Data ss:Type="String">' + sublistDataDetails[7] + '</Data></Cell>';
				CSVData += '<Cell><Data ss:Type="String">' + sublistDataDetails[8] + '</Data></Cell>';
				CSVData += '<Cell><Data ss:Type="String">' + sublistDataDetails[9] + '</Data></Cell>';
				CSVData += '<Cell><Data ss:Type="String">' + sublistDataDetails[10] + '</Data></Cell>';
				CSVData += '</Row>';
			}
			CSVData += '</Table></Worksheet></Workbook>';
			var strXmlEncoded = encode.convert({
				string: CSVData,
				inputEncoding: encode.Encoding.UTF_8,
				outputEncoding: encode.Encoding.BASE_64
			});

			var objXlsFile = file.create({
				name: 'PO Open Balance.xls',
				fileType: file.Type.EXCEL,
				contents: strXmlEncoded
			});
			// Optional: you can choose to save it to file cabinet
			// objXlsFile.folder = -14;
			// var intFileId = objXlsFile.save();

			context.response.writeFile({
				file: objXlsFile
			});

		}
	}

	return {
		onRequest: onRequest
	};

	function runSearch(searchId, startDate, endDate ,sub , prj ,custoFilt,jobFilt) {
		var searchObj = search.load({
			id: searchId
		});
		if (startDate && endDate)
		{
			searchObj.filters.push(search.createFilter({
				name : 'trandate',
				operator : search.Operator.WITHIN,
				values : [startDate,endDate]
			}));
		}
		if(custoFilt)
		{
			searchObj.filters.push(search.createFilter({
				name : 'customer',
				join : 'jobMain',
				operator : search.Operator.ANYOF,
				values : custoFilt
			})); 
		}
		if(jobFilt)
		{
			searchObj.filters.push(search.createFilter({
				name : 'internalid',
				join : 'jobMain',
				operator : search.Operator.ANYOF,
				values : jobFilt
			})); 
		}
		if(sub)
		{
			searchObj.filters.push(search.createFilter({
				name : 'subsidiary',
				operator : search.Operator.IS,
				values : sub
			})); 
		}
		log.debug('prj', prj);
		if(prj)
		{
			searchObj.filters.push(search.createFilter({
				name : 'status',
				join : 'jobMain',
				operator : search.Operator.ANYOF,
				values : prj
			})); 
			if(prj == "1")
			{
				searchObj.filters.push(search.createFilter({
					name : 'startdate',
					join : 'jobMain',
					operator : search.Operator.WITHIN,
					values : "lastyeartodate"
				})); 
			}
		}
		var results = [];
		var count = 0;
		var pageSize = 1000;
		var start = 0;
		do {
			var subresults = searchObj.run().getRange({
				start: start,
				end: start + pageSize
			});

			results = results.concat(subresults);
			count = subresults.length;
			start += pageSize;
		} while (count == pageSize);
		log.debug('resultsresultsresults', results.length);

		var resultsofData = new Array();
		for (var res = 0; res < results.length; res++) {
			var internalId = results[res].getValue({
				name: "internalid",
				join: "jobMain",
				summary: "GROUP",
				label: "Internal ID"
			});
			var customer = results[res].getText({
				name: "customer",
				join: "jobMain",
				summary: "GROUP",
				label: "Customer"
			});
			var companyname = results[res].getValue({
				 name: "companyname",
				join: "jobMain",
				summary: "GROUP",
				label: "Project Name"
			});
			var formulacurrency = results[res].getValue({
				name: "formulacurrency",
				summary: "GROUP",
				formula: "(CASE WHEN {recordtype}='salesorder' THEN {fxamount} ELSE 0 END) + (CASE WHEN {recordtype}='invoice' THEN -{fxamount} ELSE 0 END) + (CASE WHEN {recordtype}='creditmemo' THEN -{fxamount} ELSE 0 END)",
				label: "Formula (Currency)"
			});
			var otherrefnum = results[res].getValue({
				name: "otherrefnum",
				summary: "GROUP",
				label: "PO/Check Number"
			});
			var tranid = results[res].getValue({
				name: "tranid",
				summary: "GROUP",
				label: "Document Number"
			});
			var recordtype = results[res].getValue({
				name: "recordtype",
				summary: "GROUP",
				label: "Record Type"
			});
			var trandate = results[res].getValue({
				name: "trandate",
				summary: "GROUP",
				label: "Date"
			});
			var project_id = results[res].getValue({
				name: "entityid",
				join: "jobMain",
				summary: "GROUP",
				label: "ID"
			});

			resultsofData.push({
				"id": internalId,
				"customer": customer,
				"po_check_number": otherrefnum,
				"companyname": companyname,
				"tranid": tranid,
				"recordtype": recordtype,
				"formulacurrency": formulacurrency,
				"trandate": trandate,
				"project_id":project_id
			});
		}
		log.debug('All resultsofData is :', resultsofData.length);
		var groupResult_id = resultsofData.reduce(function (r, a) {
			r[a.id] = r[a.id] || [];
			r[a.id].push(a);
			return r;
		}, Object.create(null));

		return groupResult_id;

	}
	function searchJobStatus()
	{
		var jobstatusSearchObj = search.create({
			type: "jobstatus",
			filters:
				[
					["isinactive","is","F"]
					],
					columns:
						[
							search.createColumn({
								name: "name",
								sort: search.Sort.ASC,
								label: "Name"
							}),
							search.createColumn({name: "description", label: "Description"}),
							search.createColumn({name: "internalid"})
							]
		});
		var searchResultCount =  jobstatusSearchObj.run().getRange(0, 1000);
		//	log.debug("jobstatusSearchObj result count",searchResultCount);
		var dataArr = [];
		for (var rr = 0 ; rr<searchResultCount.length ; rr++)
		{
			var internalId = searchResultCount[rr].getValue({name:"internalid"});
			var Name = searchResultCount[rr].getValue({name: "name"});
			dataArr.push({"internalId":internalId,"name":Name});
		}
		return dataArr;
	}
});