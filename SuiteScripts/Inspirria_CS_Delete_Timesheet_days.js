/**
 * @NApiVersion 2.x
 * @NScriptType ClientScript
 */
define(['N/url','N/currentRecord','N/format','N/ui/dialog'],
		function (url,currentRecord,format,dialog) {
	function fieldChanged(context) {}

	function LoadData()
	{
		var currentRecordObj = currentRecord.get(); 
		var weekstartdate =  currentRecordObj.getValue('custpage_date');  
		var Customer = currentRecordObj.getValue('custpage_text')
		if(Customer == "" || Customer == null || Customer == 'undefided')
		{
			var options = {
					title: "Alert",
					message: "Select Employee."
			};
			dialog.alert(options).then(success).catch(failure);
			return false;
		}


		if(weekstartdate == "" || weekstartdate == null || weekstartdate == 'undefided')
		{
			var options = {
					title: "Alert",
					message: "Enter Week Start Date."
			};
			dialog.alert(options).then(success).catch(failure);
			return false;
		}

		/*var subsidiary =  currentRecordObj.getValue('custpage_subsidiary');
		jobstatus =  currentRecordObj.getValue('custpage_jobstatus');
		var custpage_customer =  currentRecordObj.getValue('custpage_customer');
		var custpage_job =  currentRecordObj.getValue('custpage_job');*/
		var weekstartdate =  DateFormat(currentRecordObj.getValue('custpage_date'));  
		document.location = url.resolveScript({
			scriptId : 'customscript_weekly_timesheet_delete',
			deploymentId :'customdeploy_weekly_timesheet_delete',
			params : {
				'weekstartdate' : weekstartdate,
				'Customer':Customer,
				'disp':'yes'
			}
		});
	}
	function DateFormat(initialFormattedDateString)
	{
		var parsedDateStringAsRawDateObject = format.parse({
			value: initialFormattedDateString,
			type: format.Type.DATE
		});
		return format.format({
			value: parsedDateStringAsRawDateObject,
			type: format.Type.DATE
		});
	}
	function updateData(context)
	{
		var records = currentRecord.get();
		var lineCount = records.getLineCount({
			sublistId: 'sublist'
		});
		var obj = [];
		for(var r = 0;r<lineCount;r++)
		{
			var lineNum = records.selectLine({
				sublistId: 'sublist',
				line: r
			});
			var data = records.getCurrentSublistValue({sublistId: 'sublist',fieldId: 'mark'})
			if(data == "false" ||data == false)
			{
				continue;
			}
			obj.push({
				"rate":records.getCurrentSublistValue({sublistId: 'sublist',fieldId: 'rate'}),
				"approvalstatus":records.getCurrentSublistValue({sublistId: 'sublist',fieldId: 'status'}),
				"durationdecimal":records.getCurrentSublistValue({sublistId: 'sublist',fieldId: 'durationdecimal'}),
				"billed_unbilled":records.getCurrentSublistValue({sublistId: 'sublist',fieldId: 'billed_unbilled'}),
				"timetrack_id":records.getCurrentSublistValue({sublistId: 'sublist',fieldId: 'internal_id'}),
				"timesheet_id":records.getCurrentSublistValue({sublistId: 'sublist',fieldId: 'timesheet_id'})
			});

		}
		if(obj.length == 0)
		{
			var options = {
					title: "Alert",
					message: "Select atleast one line for update."
			};
			dialog.alert(options).then(success).catch(failure);
			return false;
		}
		else
		{
			document.location = url.resolveScript({
				scriptId : 'customscript_weekly_timesheet_delete',
				deploymentId :'customdeploy_weekly_timesheet_delete',
				params : {
					'obj' : JSON.stringify(obj)
				}
			});
		}
	}
	function reset()
	{
		document.location = url.resolveScript({
			scriptId : 'customscript_weekly_timesheet_delete',
			deploymentId :'customdeploy_weekly_timesheet_delete',
		});
	}
	function saveRecord()
	{
		var records = currentRecord.get();
		var lineCount = records.getLineCount({
			sublistId: 'sublist'
		});
		var flag = true;
		for(var r = 0;r<lineCount;r++)
		{
			var lineNum = records.selectLine({
				sublistId: 'sublist',
				line: r
			});
			var data = records.getCurrentSublistValue({sublistId: 'sublist',fieldId: 'mark'})
			if(data == "true" ||data == true)
			{
				flag = false;
			}
		}
		if(flag)
		{
			var options = {
					title: "Alert",
					message: "Select atleast one line for delete."
			};
			dialog.alert(options);
			return false;
		}
		else
		{
			return true;
		}
	}
	return {
		fieldChanged : fieldChanged,
		LoadData:LoadData,
		updateData:updateData,
		reset:reset,
		saveRecord:saveRecord
	};

});
