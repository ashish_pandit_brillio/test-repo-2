function bankBalancePortlet(portlet, column) {
	portlet.setTitle('Brillio LLC');

	var IdCurrentTotal = 'customsearch_this_month';
	var IdPreviousTotal = 'customsearch_till_last_month';
	var IdCurrentSales = 'customsearch2239';
	var IdPreviousSales = 'customsearch2241';
	var IdCurrentExpense = 'customsearch2242';
	var IdPreviousExpense = 'customsearch2243';
	// Get Balance amount
	var currentAmount = GetBalance(IdCurrentTotal);
	if(currentAmount){
	var currentAmounts = GetNumberWithCommas(currentAmount);
	var previousAmount = GetBalance(IdPreviousTotal);
	var previousAmounts = GetNumberWithCommas(previousAmount);
	var balanceChange = GetChange(currentAmount, previousAmount);
	balanceChange = GetNumberWithCommas(balanceChange);
	// Get Sales amount
	var currentSales = GetBalance(IdCurrentSales);
	var currentSaless = GetNumberWithCommas(currentSales);
	var previousSales = GetBalance(IdPreviousSales);
	var previousSaless = GetNumberWithCommas(previousSales);
	var salesChange = GetChange(currentSales, previousSales);
	salesChange = GetNumberWithCommas(salesChange);
	// Get Expense Amount
	var currentExpense = GetBalance(IdCurrentExpense);
	var currentExpenses = GetNumberWithCommas(currentExpense);
	var previousExpense = GetBalance(IdPreviousExpense);
	var previousExpenses = GetNumberWithCommas(previousExpense);
	var expenseChange = GetChange(currentExpense, previousExpense);

	var content = "<table align=center border=1 cellpadding=3 cellspacing=0 width=100%> <tr><td>INDICATOR</td><td>CURRENT</td><td>PREVIOUS</td><td>CHANGE % </td></tr>";
	content += "<tr><td>Total Bank Balance</td><td>" + "$" + currentAmounts
			+ "</td><td>" + "$" + previousAmounts + "</td><td>" + balanceChange
			+ "%" + "</td></tr>" + "<tr><td>Sales By Customer</td><td>" + "$"
			+ currentSaless + "</td><td>" + "$" + previousSaless + "</td><td>"
			+ salesChange + "%" + "</td></tr>" + "<tr><td>Expenses</td><td>"
			+ "$" + currentExpenses + "</td><td>" + "$" + previousExpenses
			+ "</td><td>" + expenseChange + "%" + "</td></tr>" + "</table>";
	content = '<td><span>' + content;
	portlet.setHtml(content);
	}
	else{
		return;
	}
}
function GetBalance(Id) {
	var results = nlapiLoadSearch(null, Id);
	var searchResult = results.runSearch();
	var resultSet = [];
	// nlapiLogExecution('DEBUG', '3', '3');
	if (searchResult != null) {
		resultSet = searchResult.getResults(0, 1);
	}
	var amount = resultSet[0].getValue('amount', null, 'sum');
	return amount;
}
function GetChange(currentIdAmount, previousIdAmount) {
	// var diff = parseFloat(currentIdAmount)- parseFloat(previousIdAmount);
	var diff = parseFloat((currentIdAmount) - (previousIdAmount));
	nlapiLogExecution('DEBUG', 'diff', diff);
	var changes = (diff / previousIdAmount) * 100;
	changes = changes.toFixed(1);
	nlapiLogExecution('DEBUG', 'Changes in per', changes);
	nlapiLogExecution('DEBUG', 'currentAmount', currentIdAmount);
	nlapiLogExecution('DEBUG', 'previousAmount', previousIdAmount);
	return changes;
}
function GetNumberWithCommas(amount) {
	return amount.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}
function GetNaN(change) {
	if (change !== change)
		return "true"
}