// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
/*
   	Script Name:CLI_Validate_adjustAdvance_pay_form.js
	Author:		Nikhil jain
	Company:	Aashna Cloudtech Pvt. Ltd.
	Date:		1 Dec 2015
    Description:validate the adjust advance payment Form


	Script Modification Log:

	-- Date --			-- Modified By --				--Requested By--				-- Description --




	Below is a summary of the process controls enforced by this script file.  The control logic is described
	more fully, below, in the appropriate function headers and code blocks.

     PAGE INIT
		- pageInit(type)


     SAVE RECORD
		- saveRecord()


     VALIDATE FIELD
		- validateField(type, name, linenum)


     FIELD CHANGED
		- fieldChanged(type, name, linenum)


     POST SOURCING
		- postSourcing(type, name)


	LINE INIT
		- lineInit(type)


     VALIDATE LINE
		- validateLine()


     RECALC
		- reCalc()


     SUB-FUNCTIONS
		- The following sub-functions are called by the above core functions in order to maintain code
            modularization:





*/
}
// END SCRIPT DESCRIPTION BLOCK  ====================================



// BEGIN SCRIPT UPDATION BLOCK  ====================================
/*


*/
// END SCRIPT UPDATION BLOCK  ====================================




// BEGIN GLOBAL VARIABLE BLOCK  =====================================
{

	//  Initialize any Global Variables, in particular, debugging variables...


}
// END GLOBAL VARIABLE BLOCK  =======================================





// BEGIN PAGE INIT ==================================================

function pageInit(type)
{
    /*  On page init:

	- PURPOSE

		FIELDS USED:

		--Field Name--				--ID--			--Line Item Name--

    */

    //  LOCAL VARIABLES


    //  PAGE INIT CODE BODY


}

// END PAGE INIT ====================================================





// BEGIN SAVE RECORD ================================================

function saveRecord_validate_advance_amt(){
	/*  On save record:
	 - PURPOSE
	 FIELDS USED:
	 --Field Name--			--ID--		--Line Item Name--
	 */
	//  LOCAL VARIABLES
	
	
	
	//  SAVE RECORD CODE BODY
	
	var i_billremainingamt = nlapiGetFieldValue('custpage_fxremainingamt');
	
	var i_bill_credit_amt = nlapiGetFieldValue('custpage_billcreditamt');
	
	
	if (parseFloat(i_bill_credit_amt) > parseFloat(i_billremainingamt)) 
	{
		alert('Bill Credit amount should not be more than Remaining bill amount')
		return false;
	}
	
	
	return true;
}

// END SAVE RECORD ==================================================





// BEGIN VALIDATE FIELD =============================================

function validateField(type, name, linenum)
{

	/*  On validate field:

          - EXPLAIN THE PURPOSE OF THIS FUNCTION


          FIELDS USED:

          --Field Name--				--ID--


    */


	//  LOCAL VARIABLES



	//  VALIDATE FIELD CODE BODY


	return true;

}

// END VALIDATE FIELD ===============================================





// BEGIN FIELD CHANGED ==============================================

function fieldChanged_apply_advance(type, name, linenum){


	/*  On field changed:
	 - PURPOSE
	 FIELDS USED:
	 --Field Name--				--ID--
	 */
	//  LOCAL VARIABLES
	
	
	//  FIELD CHANGED CODE BODY
	
	//alert('Field Name='+name+'\nLine Item Type='+type+'\nField Value='+nlapiGetFieldValue(name)+'\nLine Item Value='+nlapiGetLineItemValue(type, name, 1));
	if (name == 'applyadvance') 
	{

		var apply = nlapiGetLineItemValue('advance_line', 'applyadvance', linenum)
		
		var totalamount = nlapiGetFieldValue('custpage_billcreditamt')
		
		var totalbaseamt = nlapiGetFieldValue('custpage_total_baseamt')
		
		if (apply == 'T') 
		{
			var amount = nlapiGetLineItemValue('advance_line', 'billcreditamount',linenum)
			
			totalamount = (parseFloat(totalamount) + parseFloat(amount))
			
			var baseamount = nlapiGetLineItemValue('advance_line', 'baseamount',linenum)
			
			if (baseamount != null && baseamount != '' && baseamount != undefined) {
				totalbaseamt = (parseFloat(totalbaseamt) + parseFloat(baseamount))
			}
			nlapiSetFieldValue('custpage_billcreditamt', totalamount.toFixed(2));
			
			if (nullValidation(totalbaseamt)) 
			{
				totalbaseamt = parseFloat(totalbaseamt)
				nlapiSetFieldValue('custpage_total_baseamt', totalbaseamt.toFixed(2));
			}
		}
		else 
		{
			var amount = nlapiGetLineItemValue('advance_line', 'billcreditamount', linenum)
			
			totalamount = (parseFloat(totalamount) - parseFloat(amount))
			
			var baseamount = nlapiGetLineItemValue('advance_line', 'baseamount', linenum)
			
			if (baseamount != null && baseamount != '' && baseamount != undefined) 
			{
				totalbaseamt = (parseFloat(totalbaseamt) - parseFloat(baseamount))
			}
			nlapiSetFieldValue('custpage_billcreditamt', totalamount.toFixed(2));
			
				if (nullValidation(totalbaseamt)) 
				{
					totalbaseamt = parseFloat(totalbaseamt)
					nlapiSetFieldValue('custpage_total_baseamt', totalbaseamt.toFixed(2));
				}
		}
	}
}

// END FIELD CHANGED ================================================





// BEGIN POST SOURCING ==============================================

function postSourcing(type, name)
{

    /*  On post sourcing:

	- PURPOSE

          FIELDS USED:

		--Field Name--			--ID--		--Line Item Name--


    */

    //  LOCAL VARIABLES


    //  POST SOURCING CODE BODY



}

// END POST SOURCING ================================================



// BEGIN LINE INIT ==============================================

function lineInit(type)
{

    /*  On Line Init:

	- PURPOSE

          FIELDS USED:

		--Field Name--			--ID--		--Line Item Name--


    */

    //  LOCAL VARIABLES


    //  LINE INIT CODE BODY



}

// END LINE INIT ================================================


// BEGIN VALIDATE LINE ==============================================

function validateLine(type)
{

	/*  On validate line:

          - EXPLAIN THE PURPOSE OF THIS FUNCTION


          FIELDS USED:

          --Field Name--				--ID--


    */


	//  LOCAL VARIABLES


	//  VALIDATE LINE CODE BODY


	return true;

}

// END VALIDATE LINE ================================================




// BEGIN RECALC =====================================================

function recalc(type)
{

	/*  On recalc:

          - EXPLAIN THE PURPOSE OF THIS FUNCTION


          FIELDS USED:

          --Field Name--				--ID--


    */


	//  LOCAL VARIABLES


	//  RECALC CODE BODY


}

// END RECALC =======================================================




// BEGIN FUNCTION ===================================================
function adjustAdvancemarkall()
{
	var totalamount = parseFloat(0);
	
	var totalbaseamt = parseFloat(0);
	
	var i_linecount = nlapiGetLineItemCount('advance_line');
	
	for (var i = 1; i <= i_linecount; i++) 
	{
		nlapiSetLineItemValue('advance_line', 'applyadvance', i, 'T')
		
		var amount = nlapiGetLineItemValue('advance_line', 'billcreditamount', i)
		
		totalamount = (parseFloat(totalamount) + parseFloat(amount))
		
		var baseamount = nlapiGetLineItemValue('advance_line', 'baseamount', i)
		
		if (baseamount != null && baseamount != '' && baseamount != undefined) 
		{
			totalbaseamt = (parseFloat(totalbaseamt) + parseFloat(baseamount))
		}
	}
	nlapiSetFieldValue('custpage_billcreditamt', totalamount.toFixed(2));
	
	if (nullValidation(totalbaseamt)) 
	{
		nlapiSetFieldValue('custpage_total_baseamt', totalbaseamt.toFixed(2));
	}
}

function adjustAdvanceunmarkall()
{
	var i_linecount = nlapiGetLineItemCount('advance_line');
	
	for(var i = 1;i<=i_linecount ;i++)
	{
			nlapiSetLineItemValue('advance_line', 'applyadvance', i,'F')
	}
	nlapiSetFieldValue('custpage_billcreditamt',0);
	
	nlapiSetFieldValue('custpage_total_baseamt',0);
}
function nullValidation(value)
{
	if(value != '' && value != null && value != undefined)
	{
		return true;
	}
	else
	{
		return false;
	}
		
}


// END FUNCTION =====================================================
