/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       09 Jun 2017     deepak.srinivas
 *
 */

/**
 * @param {Object} dataIn Parameter object
 * @returns {Object} Output object
 */
function postRESTlet(dataIn) {
	var response = new Response();
	try {
		var emp_department = '';
		nlapiLogExecution('debug', 'dataIn', JSON.stringify(dataIn));
		var employeeId = getUserUsingEmailId(dataIn.EmailId);
		//var employeeId = 45292;
		nlapiLogExecution('debug', 'employeeId', employeeId);
		if(employeeId){
			var emp_Rec = nlapiLookupField('employee',employeeId,['department']);
			emp_department = emp_Rec.department;
		//	nlapiLogExecution('debug', 'emp_department', emp_department);
		}
		
		var requestType = dataIn.RequestType;
		var idExists = dataIn.Data ? dataIn.Data.ID : '';
	/*	var requestType = 'GET';
		var idExists = 57146;*/
       nlapiLogExecution('debug', 'idExists', idExists);
		//var isReportingManager = dataIn.Data ? dataIn.Data.isRM : '';
		
		switch (requestType) {

		case M_Constants.Request.Get:
			if (employeeId && idExists) {
				response.Data = getOneEmployeeDetails(idExists, employeeId);
				response.Status = true;
			}
			else if (employeeId ) {
				response.Data = getEmployee_PracticeWise(employeeId,emp_department);
				response.Status = true;
			}
			break;
		case M_Constants.Request.GetAll:
			if (employeeId) {
				response.Data = getAllActiveEmployeeName();
				response.Status = true;
			}
		}
		
		
	}
 catch (err) {
	nlapiLogExecution('ERROR', 'postRESTlet', err);
	response.Data = err;
}
return response;
}

function getEmployee_PracticeWise(empid,emp_department){
	try{
		var cols = [];
		var emp_full_name = '';
		cols.push(new nlobjSearchColumn('firstname'));
		cols.push(new nlobjSearchColumn('middlename'));
		cols.push(new nlobjSearchColumn('lastname'));
		cols.push(new nlobjSearchColumn('custentity_fusion_empid'));
		cols.push(new nlobjSearchColumn('department'));
		cols.push(new nlobjSearchColumn('title'));
		cols.push(new nlobjSearchColumn('custentity_employeetype'));
		cols.push(new nlobjSearchColumn('custentity_persontype'));
		cols.push(new nlobjSearchColumn('email')); //gender
		cols.push(new nlobjSearchColumn('gender'));
		
		var employeeSearch = searchRecord('employee', null, [new nlobjSearchFilter('isinactive', null, 'is', 'F'),
		                                                     new nlobjSearchFilter('internalid', null, 'noneof', empid),
		                                                     new nlobjSearchFilter('custentity_employee_inactive', null, 'is', 'F'),
		                                 			        new nlobjSearchFilter('department', null,
		                                 			                'anyof', emp_department) ], cols);
		
		var dataRow = [];
		var JSON = {};
		if (employeeSearch) {
				for(var i=0;i<employeeSearch.length;i++){
				   var emp_id = employeeSearch[i].getId();
					var emp_type = employeeSearch[i].getText('custentity_employeetype');
				    var person_type = employeeSearch[i].getText('custentity_persontype');
					var emp_fusion_id = employeeSearch[i].getValue('custentity_fusion_empid');
					var emp_frst_name = employeeSearch[i].getValue('firstname');
					var emp_middl_name = employeeSearch[i].getValue('middlename');
					var emp_lst_name = employeeSearch[i].getValue('lastname');
					if(emp_frst_name)
						emp_full_name = emp_frst_name;
						
					if(emp_middl_name)
						emp_full_name = emp_full_name + ' ' + emp_middl_name;
						
					if(emp_lst_name)
						emp_full_name = emp_full_name + ' ' + emp_lst_name;
					
					JSON = {
							ID: emp_id,
							Initial:'',
							EmployeeID:emp_fusion_id,
							Name: emp_full_name,
							EmployeeType: emp_type,
							PersonType: person_type,
							Designation: employeeSearch[i].getValue('title'),
							Department: employeeSearch[i].getText('department'),
							Email: employeeSearch[i].getValue('email'),
							Gender: employeeSearch[i].getValue('gender')
					
							
					};
					dataRow.push(JSON);
					
				}
				
			
		}
		return dataRow;
	}
	catch(e){
		nlapiLogExecution('ERROR', 'postRESTlet', e);
	}
}



function getOneEmployeeDetails(empid,id) {
	try{
		var main = [];
		var emp_full_name = '';
		var emp_full_name_ = '';
		var JSON = {};
		var dataRow = [];
		var reporting_flag = '';
		
		//Employee Hirerachy search
		var emp_hirerachy_flag = getEmployeeHirerachyDetails(empid,id);
		nlapiLogExecution('DEBUG','emp_hirerachy_flag',emp_hirerachy_flag);
		//Check for If RM or Not
		
		var cols = [];
		cols.push(new nlobjSearchColumn('firstname'));
		cols.push(new nlobjSearchColumn('middlename'));
		cols.push(new nlobjSearchColumn('lastname'));
		cols.push(new nlobjSearchColumn('custentity_fusion_empid'));
		cols.push(new nlobjSearchColumn('department'));
		cols.push(new nlobjSearchColumn('title'));
		cols.push(new nlobjSearchColumn('custentity_employeetype'));
		cols.push(new nlobjSearchColumn('custentity_persontype'));
		cols.push(new nlobjSearchColumn('email'));
		cols.push(new nlobjSearchColumn('gender'));
		
		var employeeSearch = searchRecord('employee', null, [new nlobjSearchFilter('custentity_employee_inactive', null, 'is', 'F'),
		                                 			        new nlobjSearchFilter('custentity_reportingmanager', null,
		                                 			                'anyof', empid) ], cols);
		
		//Get Employee Details
		if(_logValidation(employeeSearch)){
			reporting_flag = 'Yes';
		}
		else{
			reporting_flag = 'No';
		}
		
		
		var JSON = {};
		var dataRow = [];
		var cols = [];
		cols.push(new nlobjSearchColumn('firstname'));
		cols.push(new nlobjSearchColumn('middlename'));
		cols.push(new nlobjSearchColumn('lastname'));
		cols.push(new nlobjSearchColumn('custentity_fusion_empid'));
		cols.push(new nlobjSearchColumn('department'));
		cols.push(new nlobjSearchColumn('title'));
		cols.push(new nlobjSearchColumn('custentity_employeetype'));
		cols.push(new nlobjSearchColumn('custentity_persontype'));
		cols.push(new nlobjSearchColumn('phone'));
		cols.push(new nlobjSearchColumn('location'));
		cols.push(new nlobjSearchColumn('email'));
		cols.push(new nlobjSearchColumn('employeestatus'));
		cols.push(new nlobjSearchColumn('custentity_reportingmanager'));
		cols.push(new nlobjSearchColumn('approver'));
		cols.push(new nlobjSearchColumn('timeapprover'));
		cols.push(new nlobjSearchColumn('timeapprover'));
		cols.push(new nlobjSearchColumn('title','custentity_reportingmanager'));
		cols.push(new nlobjSearchColumn('gender'));
		
		var employeeObj = searchRecord('employee', null, [new nlobjSearchFilter('internalid', null, 'anyof', empid)], cols);
		
		
		if(_logValidation(employeeObj)){
			for(var i=0;i<employeeObj.length;i++){
				
				var emp_id = employeeObj[i].getId();
				var emp_type = employeeObj[i].getText('custentity_employeetype');
				var  person_type = employeeObj[i].getText('custentity_persontype');
				var emp_fusion_id = employeeObj[i].getValue('custentity_fusion_empid');
				var emp_frst_name = employeeObj[i].getValue('firstname');
				var emp_middl_name = employeeObj[i].getValue('middlename');
				var emp_lst_name = employeeObj[i].getValue('lastname');
				
				if(emp_frst_name)
					emp_full_name = emp_frst_name;
					
				if(emp_middl_name)
					emp_full_name = emp_full_name + ' ' + emp_middl_name;
					
				if(emp_lst_name)
					emp_full_name = emp_full_name + ' ' + emp_lst_name;
				
				JSON = {
						ID: emp_id,
						Initial:'',
						EmployeeID:emp_fusion_id,
						Name: emp_full_name,
						EmployeeType: emp_type,
						PersonType: person_type,
						Designation: employeeObj[i].getValue('title'),
						Department: employeeObj[i].getText('department'),
						Email: employeeObj[i].getValue('email'),
						Gender: employeeObj[i].getValue('gender'),
						Phone: employeeObj[i].getValue('phone'),
						Location: employeeObj[i].getText('location'),
						Level: employeeObj[i].getText('employeestatus'),
						RMID: employeeObj[i].getValue('custentity_reportingmanager'),
						RM: employeeObj[i].getText('custentity_reportingmanager'),
						RM_Designation: employeeObj[i].getValue('title','custentity_reportingmanager'),
						ExpenseApprover: employeeObj[i].getText('approver'),
						TimeApprover: employeeObj[i].getText('timeapprover'),
						isRM: reporting_flag,
						Attendance: emp_hirerachy_flag
				
						
				};
				dataRow.push(JSON);
			
			}
			
		}
		main.push(dataRow);
		/*var employeeObj = nlapiLoadRecord('employee',empid);
		
				var rm_title = '';
				var	emp_frst_name = employeeObj.getFieldValue('firstname');
				var	emp_middl_name = employeeObj.getFieldValue('middlename');
				var	emp_lst_name = employeeObj.getFieldValue('lastname');
				var rm = employeeObj.getFieldValue('custentity_reportingmanager');
				if(rm){
				var rm_Obj = nlapiLookupField('employee',rm,['title']);
				rm_title = rm_Obj.title;
				}
					if(emp_frst_name)
						emp_full_name = emp_frst_name;
						
					if(emp_middl_name)
						emp_full_name = emp_full_name + ' ' + emp_middl_name;
						
					if(emp_lst_name)
						emp_full_name = emp_full_name + ' ' + emp_lst_name;
					
					JSON = {
							EmployeeID:employeeObj.getFieldValue('custentity_fusion_empid'),
							Name: emp_full_name,
							Initial:'',
							EmployeeType: employeeObj.getFieldText('custentity_employeetype'),
							PersonType: employeeObj.getFieldText('custentity_persontype'),
							Designation: employeeObj.getFieldValue('title'),
							Department: employeeObj.getFieldText('department'),
							Email: employeeObj.getFieldValue('email'),
							Phone: employeeObj.getFieldValue('phone'),
							Location: employeeObj.getFieldText('location'),
							Level: employeeObj.getFieldText('employeestatus'),
							RM: employeeObj.getFieldText('custentity_reportingmanager'),
							RM_Designation: rm_title,
							ExpenseApprover: employeeObj.getFieldText('approver'),
							TimeApprover: employeeObj.getFieldText('timeapprover'),
							isRM: reporting_flag
					
							
					};
					dataRow.push(JSON);
					main.push(dataRow);*/
					
					//Check for If RM or Not
					var emp_JSON = {};
					var dataRows = [];
					var cols = [];
					cols.push(new nlobjSearchColumn('firstname'));
					cols.push(new nlobjSearchColumn('middlename'));
					cols.push(new nlobjSearchColumn('lastname'));
					cols.push(new nlobjSearchColumn('custentity_fusion_empid'));
					cols.push(new nlobjSearchColumn('department'));
					cols.push(new nlobjSearchColumn('title'));
					cols.push(new nlobjSearchColumn('custentity_employeetype'));
					cols.push(new nlobjSearchColumn('custentity_persontype'));
					cols.push(new nlobjSearchColumn('email'));
					cols.push(new nlobjSearchColumn('gender'));
					
					var employeeSearch = searchRecord('employee', null, [new nlobjSearchFilter('custentity_employee_inactive', null, 'is', 'F'),
					                                 			        new nlobjSearchFilter('custentity_reportingmanager', null,
					                                 			                'anyof', empid) ], cols);
					
					
					if(_logValidation(employeeSearch)){
						for(var i=0;i<employeeSearch.length;i++){
							
							var emp_id_ = employeeSearch[i].getId();
							var emp_type_ = employeeSearch[i].getText('custentity_employeetype');
							var  person_type_ = employeeSearch[i].getText('custentity_persontype');
							var emp_fusion_id_ = employeeSearch[i].getValue('custentity_fusion_empid');
							var emp_frst_name_ = employeeSearch[i].getValue('firstname');
							var emp_middl_name_ = employeeSearch[i].getValue('middlename');
							var emp_lst_name_ = employeeSearch[i].getValue('lastname');
							if(emp_frst_name_)
								emp_full_name_ = emp_frst_name_;
								
							if(emp_middl_name_)
								emp_full_name_ = emp_full_name_ + ' ' + emp_middl_name_;
								
							if(emp_lst_name_)
								emp_full_name_ = emp_full_name_ + ' ' + emp_lst_name_;
							
							emp_JSON = {
									ID: emp_id_,
									Initial:'',
									EmployeeID:emp_fusion_id_,
									Name: emp_full_name_,
									EmployeeType: emp_type_,
									PersonType: person_type_,
									Designation: employeeSearch[i].getValue('title'),
									Department: employeeSearch[i].getText('department'),
									Email: employeeSearch[i].getValue('email'),
									Gender: employeeSearch[i].getValue('gender')
							
									
							};
							dataRows.push(emp_JSON);
						}
						
					}
					/*else{
						
						//var employeeObj_ = nlapiLoadRecord('employee',empid);
						emp_JSON ={
								ReportingManager: employeeObj.getFieldText('custentity_reportingmanager')
						};
						dataRows.push(emp_JSON);
					}*/
					main.push(dataRows);
					
					return main;
				}
				
			
		
		

	catch(e){
		nlapiLogExecution('ERROR', 'postRESTlet', e);
		throw e;
	}
}

function getEmployeeHirerachyDetails(id, empid) {
    try {
        var main = [];
        var emp_full_name = '';
        var emp_full_name_ = '';
        var JSON = {};
        var dataRow = [];
        var reporting_flag_ = false;
        var s_source_level_sub_str = '';
        var s_target_level_sub_str = '';
        //Check For RAJ // Duplicate Records
        if(parseInt(empid) == parseInt(5706))
        	empid = 8021;
        //Check the levels
        var source_emp = nlapiLookupField('employee', parseInt(empid), 'employeestatus',true);
        if (_logValidation(source_emp)) {
            source_emp = source_emp.toString();
            s_source_level_sub_str = source_emp.substr(0, 1);
        }
        var tar_emp = nlapiLookupField('employee', parseInt(id), 'employeestatus',true);
        if (_logValidation(tar_emp)) {
            tar_emp = tar_emp.toString();
            s_target_level_sub_str = tar_emp.substr(0, 1);
        }

        if (parseInt(s_target_level_sub_str) < parseInt(s_source_level_sub_str)) {
            return reporting_flag_ = false;
            
        }
        //check if same employee wants to see attendance
        if (parseInt(empid) == parseInt(id)) {
            return reporting_flag_ = true;
            
        }
        //Check for If RM or Not
        var cols = [];
        cols.push(new nlobjSearchColumn('firstname'));
        cols.push(new nlobjSearchColumn('middlename'));
        cols.push(new nlobjSearchColumn('lastname'));
        cols.push(new nlobjSearchColumn('custentity_fusion_empid'));
        cols.push(new nlobjSearchColumn('department'));
        cols.push(new nlobjSearchColumn('title'));
        cols.push(new nlobjSearchColumn('custentity_employeetype'));
        cols.push(new nlobjSearchColumn('custentity_persontype'));
        cols.push(new nlobjSearchColumn('email'));
        cols.push(new nlobjSearchColumn('custentity_reportingmanager'));


        var employeeSearch = searchRecord('employee', null, [new nlobjSearchFilter('custentity_employee_inactive', null, 'is', 'F'),
            new nlobjSearchFilter('internalid', null,
                'anyof', parseInt(id))
        ], [new nlobjSearchColumn('custentity_reportingmanager')]);



        if (_logValidation(employeeSearch)) {
            var reporting_manager = employeeSearch[0].getValue('custentity_reportingmanager');
            if (parseInt(reporting_manager) == parseInt(empid)) {
                return reporting_flag_ = true;
				
            } else if (parseInt(reporting_manager) == parseInt(8021)) {
               return reporting_flag_ = false;
			   
            } else if (parseInt(reporting_manager) != parseInt(8021)) { //CEO Check
                var employeeSearch_1 = searchRecord('employee', null, [new nlobjSearchFilter('custentity_employee_inactive', null, 'is', 'F'),
                    new nlobjSearchFilter('internalid', null,
                        'anyof', parseInt(reporting_manager))
                ], [new nlobjSearchColumn('custentity_reportingmanager')]);
                if (_logValidation(employeeSearch_1)) {
                    var reporting_manager_1 = employeeSearch_1[0].getValue('custentity_reportingmanager');
                    if (parseInt(reporting_manager_1) == parseInt(empid)) {
                       return reporting_flag_ = true;
					   
                    } else if (parseInt(reporting_manager_1) == parseInt(8021)) {
                       return reporting_flag_ = false;
					   
                    } else if (parseInt(reporting_manager_1) != parseInt(8021)) { //CEO Check
                        var employeeSearch_2 = searchRecord('employee', null, [new nlobjSearchFilter('custentity_employee_inactive', null, 'is', 'F'),
                            new nlobjSearchFilter('internalid', null,
                                'anyof', parseInt(reporting_manager_1))
                        ], [new nlobjSearchColumn('custentity_reportingmanager')]);
                        if (_logValidation(employeeSearch_2)) {
                            var reporting_manager_2 = employeeSearch_2[0].getValue('custentity_reportingmanager');
                            if (parseInt(reporting_manager_2) == parseInt(empid)) {
                               return reporting_flag_ = true;
							   
                            } else if (parseInt(reporting_manager_2) == parseInt(8021)) {
                               return reporting_flag_ = false;
							   ;
                            } else if (parseInt(reporting_manager_2) != parseInt(8021)) { //CEO Check
                                var employeeSearch_3 = searchRecord('employee', null, [new nlobjSearchFilter('custentity_employee_inactive', null, 'is', 'F'),
                                    new nlobjSearchFilter('internalid', null,
                                        'anyof', parseInt(reporting_manager_2))
                                ], [new nlobjSearchColumn('custentity_reportingmanager')]);
                                if (_logValidation(employeeSearch_3)) {
                                    var reporting_manager_3 = employeeSearch_3[0].getValue('custentity_reportingmanager');
                                    if (parseInt(reporting_manager_3) == parseInt(empid)) {
                                      return reporting_flag_ = true;
									  
                                    } else if (parseInt(reporting_manager_3) == parseInt(8021)) {
                                        return reporting_flag_ = false;
										
                                    } else if (parseInt(reporting_manager_3) != parseInt(8021)) { //CEO Check
                                        var employeeSearch_4 = searchRecord('employee', null, [new nlobjSearchFilter('custentity_employee_inactive', null, 'is', 'F'),
                                            new nlobjSearchFilter('internalid', null,
                                                'anyof', parseInt(reporting_manager_3))
                                        ], [new nlobjSearchColumn('custentity_reportingmanager')]);
                                        if (_logValidation(employeeSearch_4)) {
                                            var reporting_manager_4 = employeeSearch_4[0].getValue('custentity_reportingmanager');
                                            if (parseInt(reporting_manager_4) == parseInt(empid)) {
                                               return reporting_flag_ = true;
											   
                                            } else if (parseInt(reporting_manager_4) == parseInt(8021)) {
                                            	return reporting_flag_ = false;
                                            } else if (parseInt(reporting_manager_4) != parseInt(8021)) { //CEO Check
                                                var employeeSearch_5 = searchRecord('employee', null, [new nlobjSearchFilter('custentity_employee_inactive', null, 'is', 'F'),
                                                    new nlobjSearchFilter('internalid', null,
                                                        'anyof', parseInt(reporting_manager_4))
                                                ], [new nlobjSearchColumn('custentity_reportingmanager')]);
                                                if (_logValidation(employeeSearch_5)) {
                                                    var reporting_manager_5 = employeeSearch_5[0].getValue('custentity_reportingmanager');
                                                    if (parseInt(reporting_manager_5) == parseInt(empid)) {
                                                       return reporting_flag_ = true;
													   
                                                    } else if (parseInt(reporting_manager_5) == parseInt(8021)) {
                                                        return reporting_flag_ = false;
														
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }

                            }
                        }
                    }
                }
            }
            }
        return reporting_flag_;
    }
        catch (e) {
                nlapiLogExecution('ERROR', 'postRESTlet', e);
                throw e;
            }
        }
		
function getAllActiveEmployeeName() {
	try {
		var employeeList = [];
		var emp_id ='';
		var emp_type ='';
		var person_type ='';
		var emp_fusion_id ='';
		var emp_frst_name ='';
		var emp_middl_name ='';
		var emp_lst_name ='';
		var emp_full_name ='';
		var emp_id ='';
		var emp_id ='';
		
		var cols = [];
		cols.push(new nlobjSearchColumn('firstname'));
		cols.push(new nlobjSearchColumn('middlename'));
		cols.push(new nlobjSearchColumn('lastname'));
		cols.push(new nlobjSearchColumn('custentity_fusion_empid'));
		cols.push(new nlobjSearchColumn('department'));
		cols.push(new nlobjSearchColumn('title'));
		cols.push(new nlobjSearchColumn('custentity_employeetype'));
		cols.push(new nlobjSearchColumn('custentity_persontype'));
		cols.push(new nlobjSearchColumn('email'));
		cols.push(new nlobjSearchColumn('gender'));
		
		var employeeSearch = searchRecord('employee', null, [
		        new nlobjSearchFilter('custentity_employee_inactive', null,
		                'is', 'F'),
				new nlobjSearchFilter('custentity_implementationteam', null,
		                'is', 'F'),		
		        new nlobjSearchFilter('isinactive', null, 'is', 'F')
		        ], cols);
		var dataRow = [];
		var JSON = {};
		if(_logValidation(employeeSearch)) {
				for(var i=0;i<employeeSearch.length;i++){
				    emp_id = employeeSearch[i].getId();
					emp_type = employeeSearch[i].getText('custentity_employeetype');
				    person_type = employeeSearch[i].getText('custentity_persontype');
					emp_fusion_id = employeeSearch[i].getValue('custentity_fusion_empid');
					emp_frst_name = employeeSearch[i].getValue('firstname');
					emp_middl_name = employeeSearch[i].getValue('middlename');
					emp_lst_name = employeeSearch[i].getValue('lastname');
					if(emp_frst_name)
						emp_full_name = emp_frst_name;
						
					if(emp_middl_name)
						emp_full_name = emp_full_name + ' ' + emp_middl_name;
						
					if(emp_lst_name)
						emp_full_name = emp_full_name + ' ' + emp_lst_name;
					
					JSON = {
							ID: emp_id,
							EmployeeID:emp_fusion_id,
							Initial:'',
							Name: emp_full_name,
							EmployeeType: emp_type,
							PersonType: person_type,
							Designation: employeeSearch[i].getValue('title'),
							Department: employeeSearch[i].getText('department'),
							Email: employeeSearch[i].getValue('email'),
							Gender: employeeSearch[i].getValue('gender')
					
							
					};
					dataRow.push(JSON);
					
				}
				
			
		}

		return dataRow;
	} catch (err) {
		nlapiLogExecution('error', 'getAllActiveEmployeeName', err);
		throw err;
	}
}

function _logValidation(value) 
{
 if(value != null && value.toString() != null && value != '' && value != undefined && value.toString() != undefined && value != 'undefined' && value.toString() != 'undefined'&& value.toString() != 'NaN' && value != NaN) 
 {
  return true;
 }
 else 
 { 
  return false;
 }
}