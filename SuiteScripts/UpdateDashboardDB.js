/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       17 Dec 2018     Aazamali Khan	   update the changes made to SFDC opportunity record into dashboard DB 	
 *
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType
 * 
 * @param {String} type Operation types: create, edit, delete, xedit,
 *                      approve, cancel, reject (SO, ER, Time Bill, PO & RMA only)
 *                      pack, ship (IF only)
 *                      dropship, specialorder, orderitems (PO only) 
 *                      paybills (vendor payments)
 * @returns {Void}
 */
function CreateUpdateDashboardDB(type){
	// on create of SFDC Opportunity following code execute to create fulfillment dashboard data records.
	if (type == 'create') 
    {

		var o_sfdcRecord = nlapiLoadRecord(nlapiGetRecordType(),nlapiGetRecordId());

		var i_project = o_sfdcRecord.getFieldValue('custrecord_project_internal_id_sfdc');
		var s_account = o_sfdcRecord.getFieldValue('custrecord_customer_internal_id_sfdc');
		var s_revenue_status_con = o_sfdcRecord.getFieldValue('custrecord_confidence_sfdc');//custrecord_confidence_sfdc
		var s_revenue_status_projection = o_sfdcRecord.getFieldText('custrecord_projection_status_sfdc');//custrecord_confidence_sfdc
		var i_practice = o_sfdcRecord.getFieldValue('custrecord_practice_internal_id_sfdc');
		var s_manager = "";
		if (i_project) {
			var s_projectManager = nlapiLookupField('job', i_project, 'custentity_projectmanager', true);
			s_manager =  GetManagerName(s_projectManager);
		}
		var d_startdate = o_sfdcRecord.getFieldValue('custrecord_start_date_sfdc');
		var d_enddate = o_sfdcRecord.getFieldValue('custrecord_end_date_sfdc');

		// Create fulfillement dashboard data database record
		var recObj = nlapiCreateRecord('customrecord_fulfillment_dashboard_data');
		recObj.setFieldValue('custrecord_fulfill_dashboard_opp_id', nlapiGetRecordId());
		recObj.setFieldValue('custrecord_fulfill_dashboard_project', i_project);
		recObj.setFieldValue('custrecord_fulfill_dashboard_account', s_account);
		recObj.setFieldValue('custrecord_fulfill_dashboard_reve_confid', s_revenue_status_con);
		recObj.setFieldValue('custrecord_fulfill_dashboard_rev_sta_pro', s_revenue_status_projection);
		recObj.setFieldValue('custrecord_fulfill_dashboard_manager', s_manager);
		recObj.setFieldValue('custrecord_fulfill_dashboard_practice',i_practice);
		recObj.setFieldValue('custrecord_fulfill_dashboard_start_date',d_startdate);
		recObj.setFieldValue('custrecord_fulfill_dashboard_end_date',d_enddate);

		if (i_project) {
			//nlapiLogExecution('DEBUG', 'coming here ', "1");
			//nlapiLogExecution('DEBUG', 'team size', GetTeamSize(i_project));
			recObj.setFieldValue('custrecord_fulfill_dashboard_pro_team',GetTeamSize(i_project));
			var s_teamexiting = GetEmployeeExist(i_project);
			//nlapiLogExecution('DEBUG', 's_teamexiting ', s_teamexiting);
			if(s_teamexiting){
				s_teamexiting = s_teamexiting.toString();
			}
			recObj.setFieldValue('custrecord_fulfill_dashboard_exiting',s_teamexiting);
		}else{
			recObj.setFieldValue('custrecord_fulfill_dashboard_pro_team',"0");
			recObj.setFieldValue('custrecord_fulfill_dashboard_exiting',"0");
		}
		recObj.setFieldValue('custrecord_fulfill_dashboard_rampup',"");
		recObj.setFieldValue('custrecord_fulfill_dashboard_frf',"0");
		recObj.setFieldValue('custrecord_fulfill_dashboard_rrf',"0");
		var idGenerated = nlapiSubmitRecord(recObj);
		nlapiLogExecution('AUDIT', 'fulfillment dashboard ID:', idGenerated);
	}
	if (type=='edit') {

		var o_sfdcRecord = nlapiLoadRecord(nlapiGetRecordType(),nlapiGetRecordId());
		var i_project = o_sfdcRecord.getFieldValue('custrecord_project_internal_id_sfdc');
		var s_account = o_sfdcRecord.getFieldValue('custrecord_customer_internal_id_sfdc');
		var s_revenue_status_con = o_sfdcRecord.getFieldValue('custrecord_confidence_sfdc');//custrecord_confidence_sfdc
		var s_revenue_status_projection = o_sfdcRecord.getFieldText('custrecord_projection_status_sfdc');//custrecord_confidence_sfdc
		var i_practice = o_sfdcRecord.getFieldValue('custrecord_practice_internal_id_sfdc');
        var i_stage = o_sfdcRecord.getFieldValue('custrecord_stage_sfdc');
		var s_manager = "";
		var s_team  = "0";
		var s_teamexiting = "0" ; 
		var d_enddate = o_sfdcRecord.getFieldValue('custrecord_end_date_sfdc');
		var d_startdate = o_sfdcRecord.getFieldValue('custrecord_start_date_sfdc');
		if (i_project) {
			var s_projectManager = nlapiLookupField('job', i_project, 'custentity_projectmanager', true);
			s_manager = GetManagerName(s_projectManager);
			s_team = GetTeamSize(i_project);
			s_teamexiting = GetEmployeeExist(i_project);
		}
		if(s_team){
			s_team = s_team.toString();
		}
		if(s_teamexiting){
			s_teamexiting = s_teamexiting.toString();
		}
		// Create dashboard database
		var searchFulfillmentDB = GetFulfillmentID(nlapiGetRecordId());
		if (_logValidation(searchFulfillmentDB)) {
			var recObj = nlapiLoadRecord('customrecord_fulfillment_dashboard_data',searchFulfillmentDB[0].getId());
			//recObj.setFieldValue('custrecord_fulfill_dashboard_opp_id', nlapiGetRecordId());
			
			recObj.setFieldValue('custrecord_fulfill_dashboard_project', i_project);
			recObj.setFieldValue('custrecord_fulfill_dashboard_account', s_account);
			recObj.setFieldValue('custrecord_fulfill_dashboard_reve_confid', s_revenue_status_con);
			recObj.setFieldText('custrecord_fulfill_dashboard_rev_sta_pro', s_revenue_status_projection);
			recObj.setFieldText('custrecord_fulfill_dashboard_status', s_revenue_status_projection);
			recObj.setFieldValue('custrecord_fulfill_dashboard_manager', s_manager);
			recObj.setFieldValue('custrecord_fulfill_dashboard_practice',i_practice);
			recObj.setFieldValue('custrecord_fulfill_dashboard_pro_team',s_team);
			recObj.setFieldValue('custrecord_fulfill_dashboard_exiting',s_teamexiting);
			//recObj.setFieldValue('custrecord_fulfill_dashboard_frf',"0");
			//recObj.setFieldValue('custrecord_fulfill_dashboard_rrf',"0");
			recObj.setFieldValue('custrecord_fulfill_dashboard_start_date',d_startdate);
		    recObj.setFieldValue('custrecord_fulfill_dashboard_end_date',d_enddate);
          	if(i_stage == "8" || i_stage == "9"){
                recObj.setFieldValue('custrecord_fulfillment_dash_excluded',"T");
            }else{
                recObj.setFieldValue('custrecord_fulfillment_dash_excluded',"F");
            }	
          
			var idGenerated = nlapiSubmitRecord(recObj);
			nlapiLogExecution('AUDIT', 'edit fulfillment dashboard ID:', idGenerated);
		}
	}
}
function fieldWasChanged(field,rcdOld,record){

	if(rcdOld.getFieldValue(field) != record.getFieldValue(field)) { // compare field values 

		return true;

	} else {

		return false;

	}; 
};

function GetTeamSize(i_project) {
	var jobSearch = nlapiSearchRecord("job",null,
			[
			 ["internalid","anyof",i_project],
			 "AND", 
			 ["resourceallocation.startdate","notafter","today"], 
			 "AND", 
			 ["resourceallocation.enddate","notbefore","today"]
			 ], 
			 [
			  new nlobjSearchColumn("resource","resourceAllocation",null)
			  ]
	);
	if (_logValidation(jobSearch)) {
		var resourceCheck = jobSearch[0].getValue("resource","resourceAllocation",null);
		nlapiLogExecution('AUDIT', 'resourceCheck', resourceCheck);
		if (_logValidation(resourceCheck)) {
			return jobSearch.length;
			nlapiLogExecution('DEBUG', 'Team Size', jobSearch.length);
		}
		else{
			return "0";
		}
	}
}
function GetEmployeeExist(i_project) {
	Date.prototype.addDays = function(days) {
		var date = new Date(this.valueOf());
		date.setDate(date.getDate() + days);
		return date;
	}
	var date = new Date();
	var nextDate = date.addDays(30);
	nlapiLogExecution("AUDIT", "exiting date", date);
	var resourceallocationSearch = nlapiSearchRecord("resourceallocation",null,
			[
			 ["job.internalid","anyof",i_project], //5515 project internal ID
			 "AND", 
			 ["employee.custentity_lwd","within",date,nextDate] // 01/05/2019 calculated dynamically using current date and adding 30 days to it. 
			 ], 
			 [
			  new nlobjSearchColumn("resource",null,'GROUP')
			  ]
	);
	if (resourceallocationSearch) {
		return resourceallocationSearch.length;
	}else{
		return "0";
	}
}
function GetManagerName(tempString) {
	var s_manager = "";
	//var tempString = searchResult[int].getText("custentity_projectmanager","custrecord_project_internal_id_sfdc",null);
	temp = tempString.indexOf("-");
	if(temp>0)
	{
		var s_manager = tempString.split("-")[1];
	}
	else{
		var s_manager = tempString;
	}
	return s_manager;
}
function GetFulfillmentID(i_fulfillmentDB) {
	var customrecord_fulfillment_dashboard_dataSearch = nlapiSearchRecord("customrecord_fulfillment_dashboard_data",null,
			[
			 ["custrecord_fulfill_dashboard_opp_id","anyof",i_fulfillmentDB]
			 ], 
			 [

			  ]
	);
	return customrecord_fulfillment_dashboard_dataSearch;
}
function _logValidation(value){
	if (value != null && value != '' && value != undefined && value != 'NaN') {
		return true;
	}
	else {
		return false;
	}
}