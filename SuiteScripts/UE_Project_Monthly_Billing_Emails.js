/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       25 May 2015     nitish.mishra
 *
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your
 * script deployment.
 * 
 * @appliedtorecord recordType
 * 
 * @param {String}
 *            type Operation types: create, edit, delete, xedit, approve,
 *            cancel, reject (SO, ER, Time Bill, PO & RMA only) pack, ship (IF
 *            only) dropship, specialorder, orderitems (PO only) paybills
 *            (vendor payments)
 * @returns {Void}
 */
function userEventAfterSubmit(type) {
	try {
		// run for only create or edit triggers
		if (type == 'create') {

			// check if it a T&M project of BTPL subsidiary
			var billing_type = nlapiGetFieldValue('jobbillingtype');
			var subsidiary = nlapiGetFieldValue('subsidiary');

			if (billing_type == 'TM' && subsidiary == '3') {
				sendEmailToFinance();
			}
		} else if (type == 'edit') {

			var oldRecord = nlapiGetOldRecord();
			var old_is_monthly = oldRecord
					.getFieldValue('custentity_t_and_m_monthly');
			var new_billing_type = nlapiGetFieldValue('jobbillingtype');
			var new_subsidiary = nlapiGetFieldValue('subsidiary');
			var new_is_monthly = nlapiGetFieldValue('custentity_t_and_m_monthly');

			if (new_billing_type == 'TM' && new_subsidiary == '3'
					&& old_is_monthly != new_is_monthly
					&& isTrue(new_is_monthly)) {
				sendEmailToOps();
			}
		}
	} catch (err) {
		nlapiLogExecution('ERROR', 'userEventAfterSubmit', err);
	}
}

function sendEmailToFinance() {
	try {

		var i_raghavendra_herle = 1615;
		var i_jayakrishanan_sl = 2563;
		var recipient_list = [ i_raghavendra_herle, i_jayakrishanan_sl ];

		var projectId = nlapiGetRecordId();
		var projectDetails = nlapiLookupField('job', projectId, [ 'entityid',
				'companyname', 'startdate', 'enddate' ]);
		var projectDetails_text = nlapiLookupField('job', projectId, [
				'jobtype', 'custentity_vertical', 'customer',
				'custentity_projectmanager', 'custentity_deliverymanager',
				'custentity_practice', 'jobbillingtype' ], true);

		projectDetails.jobtype = projectDetails_text.jobtype;
		projectDetails.custentity_vertical = projectDetails_text.custentity_vertical;
		projectDetails.customer = projectDetails_text.customer;
		projectDetails.custentity_projectmanager = projectDetails_text.custentity_projectmanager;
		projectDetails.custentity_deliverymanager = projectDetails_text.custentity_deliverymanager;
		projectDetails.custentity_practice = projectDetails_text.custentity_practice;
		projectDetails.jobbillingtype = projectDetails_text.jobbillingtype;

		recipient_list.forEach(function(empId) {

			var emp_details = nlapiLookupField('employee', empId, [
					'firstname', 'email' ]);

			var mailData = getFinanceMailTemplate(emp_details.firstname,
					projectDetails);

			nlapiSendEmail('442', emp_details.email, mailData.MailSubject,
					mailData.MailBody, null, null, {
						entity : empId
					});
		});
	} catch (err) {
		nlapiLogExecution('ERROR', 'sendEmailToFinance', err);
		throw err;
	}
}

function getFinanceMailTemplate(firstName, projectDetails) {
	var htmltext = '';

	htmltext += '<table border="0" width="100%"><tr>';
	htmltext += '<td colspan="4" valign="top">';
	htmltext += '<p>Hi ' + firstName + ',</p>';

	htmltext += '<p>A new project has been created in NetSuite : </p>';

	if (projectDetails.customer)
		htmltext += '<b>Customer Name </b> : ' + projectDetails.customer
				+ "<br/>";

	if (projectDetails.entityid)
		htmltext += '<b>Project Id </b> : ' + projectDetails.entityid + "<br/>";

	if (projectDetails.companyname)
		htmltext += '<b>Project Name </b> : ' + projectDetails.companyname
				+ "<br/>";

	if (projectDetails.jobtype)
		htmltext += '<b>Type </b> : ' + projectDetails.jobtype + "<br/>";

	if (projectDetails.custentity_vertical)
		htmltext += '<b>Vertical </b> : ' + projectDetails.custentity_vertical
				+ "<br/>";

	if (projectDetails.custentity_practice)
		htmltext += '<b>Executing Practice </b> : '
				+ projectDetails.custentity_practice + "<br/>";

	if (projectDetails.startdate)
		htmltext += '<b>Start Date </b> : ' + projectDetails.startdate
				+ "<br/>";

	if (projectDetails.enddate)
		htmltext += '<b>End Date </b> : ' + projectDetails.enddate + "<br/>";

	if (projectDetails.custentity_projectmanager)
		htmltext += '<b>Project Manager </b> : '
				+ projectDetails.custentity_projectmanager + "<br/>";

	if (projectDetails.custentity_deliverymanager)
		htmltext += '<b>Delivery Manager </b> : '
				+ projectDetails.custentity_deliverymanager + "<br/>";

	if (projectDetails.jobbillingtype)
		htmltext += '<b>Billing Type </b> : ' + projectDetails.jobbillingtype
				+ "<br/>";

	htmltext += '<p>Regards,<br/>';
	htmltext += 'Information Systems</p>';

	htmltext += '</td></tr>';
	htmltext += '</table>';
	htmltext += '<hr width="100%" size="1" noshade color="#CCCCCC">';
	htmltext += '<table width="100%" border="0" cellspacing="0" cellpadding="0">';
	htmltext += '<tr>';
	htmltext += '<td align="right">';
	htmltext += '<font style="font-size:9px; font-family:Verdana,Arial,Helvetica,sans-serif; color:#999999;">Brillio is powered by <a href="http://www.netsuite.com/" style="color:#999999;">NetSuite</a> &#151; One System. No Limits.</font>';
	htmltext += '</td>';
	htmltext += '</tr>';
	htmltext += '</table>';
	htmltext += '</body>';
	htmltext += '</html>';

	return {
		MailBody : htmltext,
		MailSubject : "New T&M Project Created"
	};
}

function sendEmailToOps() {
	try {

		var recipient_mail_list = [ 'business.ops@brillio.com' ];

		var projectId = nlapiGetRecordId();
		var projectDetails = nlapiLookupField('job', projectId, [ 'entityid',
				'companyname', 'startdate', 'enddate' ]);
		var projectDetails_text = nlapiLookupField('job', projectId, [
				'jobtype', 'custentity_vertical', 'customer',
				'custentity_projectmanager', 'custentity_deliverymanager',
				'custentity_practice', 'jobbillingtype' ], true);

		projectDetails.jobtype = projectDetails_text.jobtype;
		projectDetails.custentity_vertical = projectDetails_text.custentity_vertical;
		projectDetails.customer = projectDetails_text.customer;
		projectDetails.custentity_projectmanager = projectDetails_text.custentity_projectmanager;
		projectDetails.custentity_deliverymanager = projectDetails_text.custentity_deliverymanager;
		projectDetails.custentity_practice = projectDetails_text.custentity_practice;
		projectDetails.jobbillingtype = projectDetails_text.jobbillingtype
				+ " Monthly";

		// var emp_details = nlapiLookupField('employee', empId, [
		// 'firstname', 'email' ]);

		var mailData = getOpsMailTemplate('All', projectDetails);

		nlapiSendEmail('442', recipient_mail_list[0], mailData.MailSubject,
				mailData.MailBody, null, null);
		// });
	} catch (err) {
		nlapiLogExecution('ERROR', 'sendEmailToOps', err);
		throw err;
	}
}

function getOpsMailTemplate(firstName, projectDetails) {
	var htmltext = '';

	htmltext += '<table border="0" width="100%"><tr>';
	htmltext += '<td colspan="4" valign="top">';
	htmltext += '<p>Hi ' + firstName + ',</p>';

	htmltext += '<p>A T&M Project has been updated for Monthly Billing : </p>';

	htmltext += '<b>Customer Name </b> : ' + projectDetails.customer + "<br/>";
	htmltext += '<b>Project Id </b> : ' + projectDetails.entityid + "<br/>";
	htmltext += '<b>Project Name </b> : ' + projectDetails.companyname
			+ "<br/>";
	htmltext += '<b>Type </b> : ' + projectDetails.jobtype + "<br/>";
	htmltext += '<b>Vertical </b> : ' + projectDetails.custentity_vertical
			+ "<br/>";
	htmltext += '<b>Executing Practice </b> : '
			+ projectDetails.custentity_practice + "<br/>";
	htmltext += '<b>Start Date </b> : ' + projectDetails.startdate + "<br/>";
	htmltext += '<b>End Date </b> : ' + projectDetails.enddate + "<br/>";
	htmltext += '<b>Project Manager </b> : '
			+ projectDetails.custentity_projectmanager + "<br/>";
	htmltext += '<b>Delivery Manager </b> : '
			+ projectDetails.custentity_deliverymanager + "<br/>";
	htmltext += '<b>Billing Type </b> : ' + projectDetails.jobbillingtype
			+ "<br/>";

	htmltext += '<p>Regards,<br/>';
	htmltext += 'Information Systems</p>';

	htmltext += '</td></tr>';
	htmltext += '</table>';
	htmltext += '<hr width="100%" size="1" noshade color="#CCCCCC">';
	htmltext += '<table width="100%" border="0" cellspacing="0" cellpadding="0">';
	htmltext += '<tr>';
	htmltext += '<td align="right">';
	htmltext += '<font style="font-size:9px; font-family:Verdana,Arial,Helvetica,sans-serif; color:#999999;">Brillio is powered by <a href="http://www.netsuite.com/" style="color:#999999;">NetSuite</a> &#151; One System. No Limits.</font>';
	htmltext += '</td>';
	htmltext += '</tr>';
	htmltext += '</table>';
	htmltext += '</body>';
	htmltext += '</html>';

	return {
		MailBody : htmltext,
		MailSubject : "Project Updated As Monthly"
	};
}
