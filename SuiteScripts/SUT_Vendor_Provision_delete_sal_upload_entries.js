// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
/*
   	Script Name : SUT_Vendor_Provision_delete_sal_upload_entries.js
	Author      : Jayesh Dinde
	Date        : 21 April 2016
	Description : Delete vendor provision salary upload record entries.


	Script Modification Log:

	-- Date --			-- Modified By --				--Requested By--				-- Description --													  
                                                                                         																						  

Below is a summary of the process controls enforced by this script file.  The control logic is described
more fully, below, in the appropriate function headers and code blocks.


     SUITELET
		- suiteletFunction(request, response)


     SUB-FUNCTIONS
		- The following sub-functions are called by the above core functions in order to maintain code
            modularization:

               - NOT USED

*/
}
// END SCRIPT DESCRIPTION BLOCK  ====================================



// BEGIN GLOBAL VARIABLE BLOCK  =====================================
{
	//  Initialize any Global Variables, in particular, debugging variables...




}
// END GLOBAL VARIABLE BLOCK  =======================================

// BEGIN SUITELET FUNCTION==================================================

function delete_sal_upload_entries(request, response)
{
	try
	{
		if (request.getMethod() == 'GET')
		{
			var vendor_provision_month = request.getParameter('custscript_vendor_provision_month');
			var vendor_provision_year = request.getParameter('custscript_vendor_provision_year');
			var rcrd_id = request.getParameter('rcrd_id');
			rcrd_id = rcrd_id.trim();
			var rate_type = request.getParameter('rate_type');
			rate_type = rate_type.trim();
			var vendor_provision_subsidiary = request.getParameter('vendor_provision_subsidiary');
			
			var form_obj = nlapiCreateForm("Delete Vendor Provision Salary Upload Entries");
			
			var subsidiary = form_obj.addField('custpage_subsidiary', 'text', 'subsidiary').setDisplayType('hidden');
			subsidiary.setDefaultValue(vendor_provision_subsidiary);
			
			var month = form_obj.addField('custpage_month', 'text', 'Month').setDisplayType('inline');
			month.setDefaultValue(vendor_provision_month);
			
			var year = form_obj.addField('custpage_year', 'text', 'Year').setDisplayType('inline');
			year.setDefaultValue(vendor_provision_year);
			
			var record_id = form_obj.addField('custpage_rcrd_id', 'text', 'Record ID').setDisplayType('hidden');
			record_id.setDefaultValue(rcrd_id);
			
			var rate_type_dynamic = form_obj.addField('custpage_rate_type', 'text', 'Rate Type').setDisplayType('hidden');
			rate_type_dynamic.setDefaultValue(rate_type);
			
			if(rate_type == 2)
			{
				var filters_sal_upload = new Array();
				filters_sal_upload[0] = new nlobjSearchFilter('custrecord_month_sal_up_ven',null,'is',vendor_provision_month.trim());
				filters_sal_upload[1] = new nlobjSearchFilter('custrecord_year_sal_up_ven',null,'is',vendor_provision_year.trim());
				filters_sal_upload[2] = new nlobjSearchFilter('custrecord_rate_type_sal_upload', null,'is',2);
				filters_sal_upload[3] = new nlobjSearchFilter('custrecord_subsidiary_sal_up_ven', null,'is',parseInt(vendor_provision_subsidiary));
				
				var sal_upload_search_results = nlapiSearchRecord('customrecord_salary_upload_file', null, filters_sal_upload, null);
				
				if (_logValidation(sal_upload_search_results))
				{
					var delete_record_count = form_obj.addField('custpage_rcrd_count', 'text', 'Total records to be deleted').setDisplayType('inline');
					delete_record_count.setDefaultValue(sal_upload_search_results.length);
				}
			}
			else
			{
				var filters_sal_upload = new Array();
				filters_sal_upload[0] = new nlobjSearchFilter('custrecord_month_sal_up_ven',null,'is',vendor_provision_month.trim());
				filters_sal_upload[1] = new nlobjSearchFilter('custrecord_year_sal_up_ven',null,'is',vendor_provision_year.trim());
				filters_sal_upload[2] = new nlobjSearchFilter('custrecord_rate_type_sal_upload', null,'is',3);
				filters_sal_upload[3] = new nlobjSearchFilter('custrecord_subsidiary_sal_up_ven', null,'is',parseInt(vendor_provision_subsidiary));
				
				var sal_upload_search_results = nlapiSearchRecord('customrecord_salary_upload_file', null, filters_sal_upload, null);
				
				if (_logValidation(sal_upload_search_results))
				{
					var delete_record_count = form_obj.addField('custpage_rcrd_count', 'text', 'Total records to be deleted').setDisplayType('inline');
					delete_record_count.setDefaultValue(sal_upload_search_results.length);
				}
			}
			
			form_obj.addSubmitButton('Delete Record');
			response.writePage(form_obj);
			
		}
		else
		{
			var vendor_provision_month = request.getParameter('custpage_month');
			var vendor_provision_year = request.getParameter('custpage_year');
			var record_id = request.getParameter('custpage_rcrd_id');
			var rate_type = request.getParameter('custpage_rate_type');
			var vendor_provision_subsidiary = request.getParameter('custpage_subsidiary');
			vendor_provision_subsidiary = parseInt(vendor_provision_subsidiary);
			
			if(rate_type == 2)
			{
				var params=new Array();
				params['custscript_ven_pro_rcrd_id'] = record_id;
				params['custscript_sal_upload_month'] = vendor_provision_month;
				params['custscript_sal_upload_year'] = vendor_provision_year;
				params['custscript_subsidiary_to_del_monthly'] = vendor_provision_subsidiary;
				
				var status = nlapiScheduleScript('customscript_sch_del_ven_pro_data',null,params);
				var vendor_pro_rcrd = nlapiLoadRecord('customrecord_salary_upload_process_btpl',record_id);
				if(status == 'QUEUED')
				{
					vendor_pro_rcrd.setFieldValue('custrecord_status_btpl',9);
					vendor_pro_rcrd.setFieldValue('custrecord_completion_status','Not Started');
					vendor_pro_rcrd.setFieldValue('custrecord_notes_btpl','');		
					vendor_pro_rcrd.setFieldValue('custrecord_prcnt_complete','');
					vendor_pro_rcrd.setFieldValue('custrecord_contractor_count','');
					vendor_pro_rcrd.setFieldValue('custrecord_total_processed','');
					vendor_pro_rcrd.setFieldValue('custrecord_not_processed','');
					vendor_pro_rcrd.setFieldValue('custrecord_which_bttn_clicked','DELETE_JE');		
				}
				else
				{
					vendor_pro_rcrd.setFieldValue('custrecord_status_btpl',9);
					vendor_pro_rcrd.setFieldValue('custrecord_completion_status','');
					vendor_pro_rcrd.setFieldValue('custrecord_notes_btpl','');	
					vendor_pro_rcrd.setFieldValue('custrecord_prcnt_complete','');
					vendor_pro_rcrd.setFieldValue('custrecord_contractor_count','');
					vendor_pro_rcrd.setFieldValue('custrecord_total_processed','');
					vendor_pro_rcrd.setFieldValue('custrecord_not_processed','');
					vendor_pro_rcrd.setFieldValue('custrecord_which_bttn_clicked','DELETE_JE');
				}
			}
			else
			{
				var params=new Array();
				params['custscriptprovision_rcrd_id'] = record_id;
				params['custscript_month_to_del'] = vendor_provision_month;
				params['custscript_year_to_del'] = vendor_provision_year;
				params['custscript_subsidiary_to_del'] = vendor_provision_subsidiary;
				
				var status = nlapiScheduleScript('customscript_del_ven_pro_data_hourly',null,params);
				var vendor_pro_rcrd = nlapiLoadRecord('customrecord_salary_upload_process_btpl',record_id);
				if(status == 'QUEUED')
				{
					vendor_pro_rcrd.setFieldValue('custrecord_status_btpl',9);
					vendor_pro_rcrd.setFieldValue('custrecord_completion_status','Not Started');
					vendor_pro_rcrd.setFieldValue('custrecord_notes_btpl','');		
					vendor_pro_rcrd.setFieldValue('custrecord_prcnt_complete','');
					vendor_pro_rcrd.setFieldValue('custrecord_contractor_count','');
					vendor_pro_rcrd.setFieldValue('custrecord_total_processed','');
					vendor_pro_rcrd.setFieldValue('custrecord_not_processed','');
					vendor_pro_rcrd.setFieldValue('custrecord_which_bttn_clicked','DELETE_JE_HOURLY');		
				}
				else
				{
					vendor_pro_rcrd.setFieldValue('custrecord_status_btpl',9);
					vendor_pro_rcrd.setFieldValue('custrecord_completion_status','');
					vendor_pro_rcrd.setFieldValue('custrecord_notes_btpl','');	
					vendor_pro_rcrd.setFieldValue('custrecord_prcnt_complete','');
					vendor_pro_rcrd.setFieldValue('custrecord_contractor_count','');
					vendor_pro_rcrd.setFieldValue('custrecord_total_processed','');
					vendor_pro_rcrd.setFieldValue('custrecord_not_processed','');
					vendor_pro_rcrd.setFieldValue('custrecord_which_bttn_clicked','DELETE_JE_HOURLY');
				}
			}
			
			
			nlapiSubmitRecord(vendor_pro_rcrd,true,true);
			nlapiSetRedirectURL('RECORD', 'customrecord_salary_upload_process_btpl', record_id, false);
			
			/*var filters_sal_upload = new Array();
			filters_sal_upload[0] = new nlobjSearchFilter('custrecord_month_sal_up_ven',null,'is',vendor_provision_month.trim());
			filters_sal_upload[1] = new nlobjSearchFilter('custrecord_year_sal_up_ven',null,'is',vendor_provision_year.trim());
			//filters_sal_upload[2] = new nlobjSearchFilter('custrecord_is_processed',null,'is','F');
			
			var sal_upload_search_results = nlapiSearchRecord('customrecord_salary_upload_file', null, filters_sal_upload, null);
			
			if(_logValidation(sal_upload_search_results))
			{
				for(var i=0; i< sal_upload_search_results.length; i++)
					nlapiDeleteRecord('customrecord_salary_upload_file',sal_upload_search_results[i].getId());
			}
			
			var o_recordOBJ = nlapiLoadRecord('customrecord_salary_upload_process_btpl', record_id);
			o_recordOBJ.setFieldValue('custrecord_status_btpl', 4);
			o_recordOBJ.setFieldValue('custrecord_notes_btpl', '');		
			var i_submitID = nlapiSubmitRecord(o_recordOBJ, true, true);
			
			//nlapiSubmitField('customrecord_salary_upload_process_btpl',record_id,'custrecord_status_btpl',4);				
			//nlapiSubmitField('customrecord_salary_upload_process_btpl',record_id,'custrecord_notes_btpl','');
			nlapiSetRedirectURL('RECORD', 'customrecord_salary_upload_process_btpl', record_id, false);*/
		}
	}
	catch(err)
	{
		nlapiLogExecution('ERROR','ERROR MESSAGE:- ',err);
	}
}

// END SUITELET FUNCTION==================================================

function _logValidation(value) 
{
 if(value != null && value.toString() != null && value != '' && value != undefined && value.toString() != undefined && value != 'undefined' && value.toString() != 'undefined'&& value.toString() != 'NaN' && value != NaN) 
 {
  return true;
 }
 else 
 { 
  return false;
 }
}