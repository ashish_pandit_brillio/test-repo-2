/**
 * Module Description
 * 
 * Version Date Author Remarks 1.00 09 Mar 2015 nitish.mishra
 * 
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your
 * script deployment.
 * 
 * @appliedtorecord recordType
 * 
 * @param {String}
 *        type Operation types: create, edit, delete, xedit approve, reject,
 *        cancel (SO, ER, Time Bill, PO & RMA only) pack, ship (IF) markcomplete
 *        (Call, Task) reassign (Case) editforecast (Opp, Estimate)
 * @returns {Void}
 */
function userEventBeforeSubmit(type) {

	try {
		nlapiLogExecution('debug', 'started');

		var a_layout_type = [
			'1', // BLLC T&M
			'2', // BLLC T&M Group
			'12', // BLLC T&M Group - Discount
			'13' // BLLC T&M Group - Move
		];

		var record = nlapiLoadRecord(nlapiGetRecordType(), nlapiGetRecordId());
		var i_current_layout = record.getFieldValue('custbody_layout_type');
		nlapiLogExecution('debug', 'i_current_layout', i_current_layout);

		// if its a T&M project
		if (_.indexOf(a_layout_type, i_current_layout) != -1) {

			// check all items and get a list of all discount items
			var i_item_count = record.getLineItemCount('item');
			var a_discount_lines = [];
			var a_discount_account = [
				constant.Account.TradeDiscount
			];
			// get all discount items
			var a_discount_item_search = nlapiSearchRecord('discountitem', null, [
				new nlobjSearchFilter('account', null, 'anyof', a_discount_account)
			]);
			var a_discounted_items = []; // list of discount items
			a_discount_item_search.forEach(function(item) {

				a_discounted_items.push(item.getId());
			});

			for (var line_num = 1; line_num <= i_item_count; line_num++) {
				var i_service_item = record.getLineItemValue('item', 'item', line_num);
				var i_item_account = record.getLineItemValue('item', 'account', line_num);
				nlapiLogExecution('debug', 'i_item_account', i_item_account);
				nlapiLogExecution('debug', '_.indexOf(a_discounted_items, i_service_item) != -1', _
						.indexOf(a_discounted_items, i_service_item));

				if (_.indexOf(a_discounted_items, i_service_item) != -1) {
					a_discount_lines.push({
						LineNumber : line_num,
						ServiceItem : i_service_item,
						PriceLevel : record.getLineItemValue('item', 'pricelevels', line_num),
						Customer : record.getLineItemValue('item', 'custcolcustcol_temp_customer',
								line_num),
						Project : record.getLineItemValue('item', 'custcolprj_name', line_num),
						Employee : record.getLineItemValue('item', 'custcol_employeenamecolumn',
								line_num),
						Rate : record.getLineItemValue('item', 'rate', line_num)
					});
				}
			}

			// get the customer vertical
			var i_project_vertical =
					nlapiLookupField('customer', record.getFieldValue('entity'),
							'custentity_vertical');

			nlapiLogExecution('debug', 'original item count', i_item_count);
			nlapiLogExecution('debug', 'no. of discount items', a_discount_lines.length);

			// get all the employees from time
			var i_time_count = record.getLineItemCount('time');
			var a_emp_list = [] , a_emp_id_list = [];
			var f_total = 0;

			nlapiLogExecution('debug', 'i_time_count', i_time_count);

			for (var line_num = 1; line_num <= i_time_count; line_num++) {
				var b_is_applied = record.getLineItemValue('time', 'apply', line_num) == 'T';
				// nlapiLogExecution('debug', 'b_is_applied', b_is_applied);
				var i_emp_id = record.getLineItemValue('time', 'employee', line_num);

				if (b_is_applied) {

					var pos = -1;
					for (var i = 0; i < a_emp_id_list.length; i++) {
						if (a_emp_id_list[i] == i_emp_id) {
							pos = i;
							break;
						}
					}

					var amount = parseFloat(record.getLineItemValue('time', 'amount', line_num));

					nlapiLogExecution('debug', 'amount A', amount);

					// a_emp_id_list
					if (pos == -1) {
						a_emp_list.push({
							Id : i_emp_id,
							Name : record.getLineItemValue('time', 'employee', line_num),
							Practice : record.getLineItemValue('time', 'department', line_num),
							Amount : amount
						});

						a_emp_id_list.push(i_emp_id);
					}
					else {
						nlapiLogExecution('debug', 'amount B', JSON.stringify(a_emp_list[pos]));
						nlapiLogExecution('debug', 'amount C', a_emp_list[pos].Amount);
						a_emp_list[pos].Amount += amount;
						nlapiLogExecution('debug', 'amount D', a_emp_list[pos].Amount);
					}

					f_total += parseFloat(record.getLineItemValue('time', 'amount', line_num));
				}
			}

			nlapiLogExecution('debug', 'employee list before', JSON.stringify(a_emp_list));
			mapEmployeeDetails(a_emp_list, a_emp_id_list, f_total);
			nlapiLogExecution('debug', 'employee list after', JSON.stringify(a_emp_list));
			// a_emp_list.push(a_emp_list[0]);
			// a_emp_list.push(a_emp_list[0]);

			var i_employee_count = a_emp_list.length;
			nlapiLogExecution('debug', 'no. of employees', i_employee_count);

			var b_do_remove = false;
			var count = 0;

			// add new lines for each employee and discount item
			a_discount_lines.forEach(function(discount_details) {

				var i_total_amount = discount_details.Rate;
				var i_new_amount = i_total_amount / i_employee_count;
				b_do_remove = false;
				nlapiLogExecution('debug', 'i_total_amount', i_total_amount);
				nlapiLogExecution('debug', 'i_new_amount', i_new_amount);

				a_emp_list.forEach(function(employee) {

					nlapiLogExecution('debug', 'employee', JSON.stringify(employee));
					var discount = (i_total_amount * employee.DiscountRatio);
					nlapiLogExecution('debug', 'discount', discount);
					count++;
					record.selectNewLineItem('item');
					record.setCurrentLineItemValue('item', 'item', discount_details.ServiceItem);
					record.setCurrentLineItemValue('item', 'pricelevels',
							discount_details.PriceLevel);
					record.setCurrentLineItemValue('item', 'rate', discount);
					record.setCurrentLineItemValue('item', 'amount', discount);
					record.setCurrentLineItemValue('item', 'custcolcustcol_temp_customer',
							discount_details.Customer);
					record.setCurrentLineItemValue('item', 'custcolprj_name',
							discount_details.Project);
					record.setCurrentLineItemValue('item', 'department', employee.Practice);
					record.setCurrentLineItemValue('item', 'class', i_project_vertical);
					record.setCurrentLineItemValue('item', 'custcol_employeenamecolumn',
							employee.Name);
					record.commitLineItem('item');
					nlapiLogExecution('debug', 'item added', count);
					b_do_remove = true;
				});

				// remove the original discount line
				if (b_do_remove) {
					record.removeLineItem('item', discount_details.LineNumber);
					// nlapiCommitLineItem('item');
					nlapiLogExecution('debug', 'item remove', discount_details.LineNumber);
					// nlapiRemoveLineItem('item', discount_details.LineNumber);
				}
			});

			nlapiSubmitRecord(record, false, true);
		}
	}
	catch (err) {
		nlapiLogExecution('ERROR', 'userEventBeforeSubmit', err);
	}
}

function mapEmployeeDetails(a_emp_list, a_emp_id_list, total) {

	nlapiSearchRecord('employee', null, [
		new nlobjSearchFilter('internalid', null, 'anyof', a_emp_id_list)
	], [
		new nlobjSearchColumn('department'), new nlobjSearchColumn('entityid')
	]).forEach(function(emp) {

		for (var i = 0; i < a_emp_list.length; i++) {

			if (a_emp_list[i].Id == emp.getId()) {
				a_emp_list[i].Name = emp.getValue('entityid');
				a_emp_list[i].Practice = emp.getValue('department');
				a_emp_list[i].DiscountRatio = a_emp_list[i].Amount / total;
				break;
			}
		}
	});

	nlapiLogExecution('debug', 'employee list A', JSON.stringify(a_emp_list));
}
