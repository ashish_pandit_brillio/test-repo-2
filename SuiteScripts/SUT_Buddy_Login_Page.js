/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       18 Aug 2016     shruthi.l
 *
 */

/**
 * @param {nlobjRequest} request Request object
 * @param {nlobjResponse} response Response object
 * @returns {Void} Any output is written via response object
 */
function suitelet(request, response){
	var method=request.getMethod();
	if (method == 'GET' )
	{
		var buddy_login = nlapiLoadFile(232676);   //load the HTML file
		var buddy_login_contents = buddy_login.getValue();    //get the contents
		response.write(buddy_login_contents);
		
	}
	else{
		var username = request.getParameter('custparam_uname');
		
		nlapiLogExecution('Debug', 'suitelet', username);
		
		var filters = new Array();
		filters[0]  = new nlobjSearchFilter('email',null,'is',username);
		filters[1]  = new nlobjSearchFilter('custentity_isbuddy',null,'is','T');
		
		var columns   = new Array();
		columns[0]    = new nlobjSearchColumn('firstname');
		
		var emp_search = nlapiSearchRecord('employee',null,filters,columns);
		
		nlapiLogExecution('Debug', 'suitelet', emp_search);
		
		if(isNotEmpty(emp_search)){
			var buddy_form = nlapiLoadFile(234219);   //load the HTML file
			var buddy_form_contents = buddy_form.getValue();    //get the contents
			response.write(buddy_form_contents);
		}else{
			throw 'You are not a buddy';
		}
		
		
		
		
		nlapiLogExecution('Debug', 'suitelet', username);
		//nlapiLogExecution('debug', 'user', request.getAllParameters());
		
		
		
	}
}
