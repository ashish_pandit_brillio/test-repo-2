


function suiteletFunction_Vendor_Pro(request, response)
{
	try
	{
		var vendor_provision_month =  request.getParameter('vendor_provision_month');
		var vendor_provision_year =  request.getParameter('vendor_provision_year');
		var vendor_provision_subsidiary =  request.getParameter('vendor_provision_subsidiary');
		var vendor_subsidiary_sub_id =  request.getParameter('vendor_subsidiary_sub_id');
		var rate_type = request.getParameter('rate_type');
		nlapiLogExecution('audit','rate_type:- ',rate_type);
		
		var filters_error_logs = new Array();
		filters_error_logs[0] = new nlobjSearchFilter('custrecord_month_ven_pro', null, 'is', vendor_provision_month.trim());
		filters_error_logs[1] = new nlobjSearchFilter('custrecord_year_ven_pro', null, 'is', vendor_provision_year.trim());
		filters_error_logs[2] = new nlobjSearchFilter('custrecord_rate_type_error_log', null, 'is', parseInt(rate_type));
		filters_error_logs[3] = new nlobjSearchFilter('custrecord_subsidiary_ven_pro', null, 'is', parseInt(vendor_subsidiary_sub_id));
		
		var column_error_logs = new Array();
		column_error_logs[0]  = new nlobjSearchColumn('custrecord_error_details');
		column_error_logs[1]  = new nlobjSearchColumn('custrecord_employee_id_ven_pro');
		column_error_logs[2]  = new nlobjSearchColumn('custrecord_subsidiary_ven_pro');
		column_error_logs[3]  = new nlobjSearchColumn('custrecord_month_ven_pro');
		column_error_logs[4]  = new nlobjSearchColumn('custrecord_year_ven_pro');
				
		var down_excel_error_logs = nlapiSearchRecord('customrecord_error_logs_vendor_pro', null, filters_error_logs, column_error_logs);
		if (_logValidation(down_excel_error_logs))
		{
			down_excel_function(down_excel_error_logs, vendor_provision_month, vendor_provision_year);
		}
	}
	catch(err)
	{
		nlapiLogExecution('ERROR','ERROR MESSAGE:- ',err);
	}
}

function down_excel_function(down_excel_error_logs,vendor_provision_month,vendor_provision_year)
{
	try
	{
		var strVar1= '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:ss="urn:schemas-microsoft-com:office:spreadsheet" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40">'+
		'<head>'+
		'<meta http-equiv=Content-Type content="text/html; charset=windows-1252"/>'+
        '<meta name=ProgId content=Excel.Sheet/>'+
        '<meta name=Generator content="Microsoft Excel 11"/>'+
        '<!--[if gte mso 9]><xml>'+
        '<x:excelworkbook>'+
        '<x:excelworksheets>'+
        '<x:excelworksheet=sheet1>'+
        '<x:name>** ESTIMATE FILE**</x:name>'+
        '<x:worksheetoptions>'+
        '<x:selected></x:selected>'+
        '<x:freezepanes></x:freezepanes>'+
        '<x:frozennosplit></x:frozennosplit>'+
        '<x:splithorizontal>** FROZEN ROWS + 1 **</x:splithorizontal>'+
        '<x:toprowbottompane>** FROZEN ROWS + 1 **</x:toprowbottompane>'+
        '<x:splitvertical>** FROZEN COLUMNS + 1 **</x:splitvertical>'+
        '<x:leftcolumnrightpane>** FROZEN COLUMNS + 1**</x:leftcolumnrightpane>'+
		    '<x:activepane>0</x:activepane>'+// 0
		    '<x:panes>'+
			'<x:pane>'+
			'<x:number>3</x:number>'+
			'</x:pane>'+
			'<x:pane>'+
		    '<x:number>1</x:number>'+
		    '</x:pane>'+
		    '<x:pane>'+
			'<x:number>2</x:number>'+
        '</x:pane>'+
        '<x:pane>'+
        '<x:number>0</x:number>'+//1
        '</x:pane>'+
        '</x:panes>'+
        '<x:protectcontents>False</x:protectcontents>'+
        '<x:protectobjects>False</x:protectobjects>'+
        '<x:protectscenarios>False</x:protectscenarios>'+
        '</x:worksheetoptions>'+
        '</x:excelworksheet>'+
		'</x:excelworksheets>'+
        '<x:protectstructure>False</x:protectstructure>'+
        '<x:protectwindows>False</x:protectwindows>'+
		'</x:excelworkbook>'+
        
        // '<style>'+
			
			//-------------------------------------
        '</x:excelworkbook>'+
        '</xml><![endif]-->'+
         '<style>'+
		'p.MsoFooter, li.MsoFooter, div.MsoFooter'+
		'{ margin:0in; margin-bottom:.0001pt; mso-pagination:widow-orphan; tab-stops:center 3.0in right 6.0in; font-size:12.0pt;}'+
		'<style>'+

		'<!-- /* Style Definitions */'+

		//'@page Section1'+
		//'{ size:8.5in 11.0in; margin:1.9cm 1.27cm 1.9cm 1.27cm; mso-header-margin:.5in; mso-footer-margin:.5in; mso-title-page:yes; mso-header: h1; mso-footer: f1; mso-first-header: fh1; mso-first-footer: ff1; mso-paper-source:0;}'+

		'div.Section1'+
		'{ page:Section1;}'+

		'table#hrdftrtbl'+
		'{ margin:0in 0in 0in 900in; width:1px; height:1px; overflow:hidden; font-family:Arial;}-->'+

		'</style>'+
		
		
		'</head>'+
		

		'<body>'+
		
		'<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">'
		
		var strVar2 = '';    
		strVar2 += "<table width=\"100%\">";
		strVar2 += "<tr>";
		strVar2 += "<td Width=\"10%\" style='text-align:left;font-weight:bold;' font-size=\"10\" \>Month</td>";
		strVar2 += "<td Width=\"10%\" style='text-align:left;font-weight:bold;' font-size=\"10\" \>Year</td>";
		strVar2 += "<td Width=\"20%\" style='text-align:left;font-weight:bold;' font-size=\"10\" \>Contractor Name</td>";
		strVar2 += "<td Width=\"10%\" style='text-align:left;font-weight:bold;' font-size=\"10\" \>Subsidiary</td>";
		strVar2 += "<td Width=\"50%\" style='text-align:left;font-weight:bold;' font-size=\"10\" \>Error Detail</td>";
		strVar2 += "<\/tr>";
		
		if (_logValidation(down_excel_error_logs)) //
		{
			for (var i = 0; i < down_excel_error_logs.length; i++) //
			{
				var Emp_name = down_excel_error_logs[i].getValue('custrecord_employee_id_ven_pro');
				var Emp_Subsidiary = down_excel_error_logs[i].getText('custrecord_subsidiary_ven_pro');
				var Err_detail = down_excel_error_logs[i].getValue('custrecord_error_details');
				
				strVar2 += "<tr>";
				strVar2 += "<td Width=\"10%\" style='text-align:left;' font-size=\"10\" \>" + vendor_provision_month + "</td>";
				strVar2 += "<td Width=\"10%\" style='text-align:left;' font-size=\"10\" \>" + vendor_provision_year + "</td>";
				strVar2 += "<td Width=\"20%\" style='text-align:left;' font-size=\"10\" \>" + Emp_name + "</td>";
				strVar2 += "<td Width=\"10%\" style='text-align:left;' font-size=\"10\" \>" + Emp_Subsidiary + "</td>";
				strVar2 += "<td Width=\"50%\" style='text-align:left;' font-size=\"10\" \>" + Err_detail + "</td>";
				strVar2 += "<\/tr>";
			}
		}
		else
		{
			//NO RESULTS FOUND
		}
					
		strVar2 += "<\/table>";			
		strVar1 = strVar1 + strVar2;
		var file = nlapiCreateFile('Error Logs.xls', 'XMLDOC', strVar1);
		nlapiLogExecution('debug','file:- ',file);
		response.setContentType('XMLDOC','Error Logs.xls');
		response.write( file.getValue() );	 
		
	}
	catch(err)
	{
		nlapiLogExecution('ERROR','ERROR MESSAGE:- ',err);
	}
}

function _logValidation(value) 
{
 if(value != null && value.toString() != null && value != '' && value != undefined && value.toString() != undefined && value != 'undefined' && value.toString() != 'undefined'&& value.toString() != 'NaN' && value != NaN) 
 {
  return true;
 }
 else 
 { 
  return false;
 }
}
