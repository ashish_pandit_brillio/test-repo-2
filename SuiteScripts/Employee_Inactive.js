/**
 * Module Description
 * 
 * Version Date Author Remarks 1.00 13 Mar 2015 nitish.mishra
 * 
 */

function onPageSave() {
	try {
		var employeeId = nlapiGetRecordId();

		var current_inactive_status = nlapiGetFieldValue('custentity_employee_inactive');

		// check if the employee is currently inactive
		if (isTrue(current_inactive_status)) {
			var last_inactive_status = nlapiLookupField('employee', employeeId,
			        'custentity_employee_inactive');

			// check if the inactive status has changed
			if (isFalse(last_inactive_status)) {

				// check if any employee or projects are dependent
				if (getRelatedEmployees(employeeId).DependentEmployee.length > 0
				        && getRelatedProjects(employeeId).DependentProjectList) {
					nlapiLogExecution('debug', 'see done',
					        constant.Suitelet.Script.Employee_Inactive);
					var url = nlapiResolveURL('SUITELET',
					        constant.Suitelet.Script.Employee_Inactive,
					        constant.Suitelet.Deployement.Employee_Inactive);
					nlapiLogExecution('debug', 'url', url);
					window.location = url + "&empid=" + employeeId
					        + "&unlayered=T";
				}
			}
		}
	} catch (err) {
		nlapiLogExecution('ERROR', 'onPageSave', err.message);
	}

	return true;
}

function getRelatedEmployees(employeeId) {
	try {
		var a_dependend_employee = [];
		var a_expense_approver_list = [];
		var a_time_approver_list = [];
		var a_reporting_manager_list = [];

		searchRecord(
		        'employee',
		        null,
		        [ [
		                [ 'custentity_employee_inactive', 'is', 'F' ],
		                'and',
		                [ [ 'approver', 'anyof', employeeId ], 'or',
		                // [ 'purchaseorderapprover', 'anyof', employeeId ],
		                // 'or',
		                [ 'custentity_reportingmanager', 'anyof', employeeId ],
		                        'or', [ 'timeapprover', 'anyof', employeeId ] ] ] ],
		        [ new nlobjSearchColumn('entityid'),
		                new nlobjSearchColumn('approver'),
		                new nlobjSearchColumn('purchaseorderapprover'),
		                new nlobjSearchColumn('timeapprover'),
		                new nlobjSearchColumn('custentity_reportingmanager') ])
		        .forEach(
		                function(emp) {

			                if (emp.getValue('approver') == employeeId) {
				                a_expense_approver_list
				                        .push({
				                            employee : emp.getId(),
				                            name : emp.getValue('entityid'),
				                            expenseapprover : '',
				                            reportingmanager : '',
				                            timeapprover : emp
				                                    .getValue('timeapprover') == employeeId ? '-'
				                                    : emp
				                                            .getText('timeapprover')
				                        });
			                }

			                if (emp.getValue('timeapprover') == employeeId) {
				                a_time_approver_list
				                        .push({
				                            employee : emp.getId(),
				                            name : emp.getValue('entityid'),
				                            timeapprover : '',
				                            reportingmanager : '',
				                            expenseapprover : emp
				                                    .getValue('approver') == employeeId ? ''
				                                    : emp.getValue('approver')
				                        });
			                }

			                if (emp.getValue('custentity_reportingmanager') == employeeId) {
				                a_reporting_manager_list
				                        .push({
				                            employee : emp.getId(),
				                            name : emp.getValue('entityid'),
				                            timeapprover : '',
				                            expenseapprover : '',
				                            reportingmanager : emp
				                                    .getValue('custentity_reportingmanager') == employeeId ? ''
				                                    : emp
				                                            .getValue('custentity_reportingmanager')
				                        });
			                }

			                /*
							 * a_dependend_employee.push({ employee :
							 * emp.getId(), name : emp.getValue('entityid'),
							 * expenseapprover : emp.getValue('approver'),
							 * timeapprover : emp
							 * .getValue('purchaseorderapprover') });
							 */
		                });

		return {
		    ExpenseApproverList : a_expense_approver_list,
		    TimeApproverList : a_time_approver_list,
		    ReportingManagerList : a_reporting_manager_list,
		    DependentEmployee : a_dependend_employee
		};
	} catch (err) {
		nlapiLogExecution('ERROR', 'getRelatedEmployees', err);
		throw err;
	}
}

function getRelatedProjects(employeeId) {
	try {
		var a_dependend_project = [];
		var a_project_manager_list = [];
		var a_delivery_manager_list = [];
		var a_client_partner_list = [];
			var filters = [
	        [ [ 'custentity_projectmanager', 'anyof', employeeId ], 'or',
	                [ 'custentity_deliverymanager', 'anyof', employeeId ],
	                'or', [ 'custentity_clientpartner', 'anyof', employeeId ] ],
	        'and',
	        [ [ 'status', 'anyof', '2' ], 'or', [ 'status', 'anyof', '4' ] ]]; 
		searchRecord(
		        'job',
		        null,
		        filters,
		        [ new nlobjSearchColumn('custentity_deliverymanager'),
		                new nlobjSearchColumn('customer'),
		                new nlobjSearchColumn('custentity_clientpartner'),
		                new nlobjSearchColumn('custentity_projectmanager'),
		                new nlobjSearchColumn('companyname'),
		                new nlobjSearchColumn('entityid'),
						new nlobjSearchColumn('status')])
		        .forEach(
		                function(projectDetails) {

			                if (projectDetails
			                        .getValue('custentity_projectmanager') == employeeId) {
				                a_project_manager_list.push({
				                    project : projectDetails.getId(),
				                    name : projectDetails.getValue('entityid')
				                            + " "
				                            + projectDetails
				                                    .getValue('companyname'),
				                    projectmanager : '',
				                    customer : projectDetails
				                            .getValue('customer')
				                });
			                }

			                if (projectDetails
			                        .getValue('custentity_deliverymanager') == employeeId) {
				                a_delivery_manager_list.push({
				                    project : projectDetails.getId(),
				                    name : projectDetails.getValue('entityid')
				                            + " "
				                            + projectDetails
				                                    .getValue('companyname'),
				                    deliverymanager : '',
				                    customer : projectDetails
				                            .getValue('customer')
				                });
			                }

			                if (projectDetails
			                        .getValue('custentity_clientpartner') == employeeId) {
				                a_client_partner_list.push({
				                    project : projectDetails.getId(),
				                    name : projectDetails.getValue('entityid')
				                            + " "
				                            + projectDetails
				                                    .getValue('companyname'),
				                    deliverymanager : '',
				                    customer : projectDetails
				                            .getValue('customer')
				                });
			                }

			                a_dependend_project
			                        .push({
			                            project : projectDetails.getId(),
			                            name : projectDetails
			                                    .getValue('companyname'),
			                            deliverymanager : projectDetails
			                                    .getValue('custentity_deliverymanager'),
			                            projectmanager : projectDetails
			                                    .getValue('custentity_projectmanager'),
			                            clientpartner : projectDetails
			                                    .getValue('custentity_clientpartner'),
			                            customer : projectDetails
			                                    .getValue('customer')
			                        });
		                });

		return {
		    DependentProjectList : a_dependend_project,
		    ProjectManagerList : a_project_manager_list,
		    DeliveryManagerList : a_delivery_manager_list,
		    ClientPartnerList : a_client_partner_list
		};
	} catch (err) {
		nlapiLogExecution('ERROR', 'getRelatedProjects', err);
		throw err;
	}
}
