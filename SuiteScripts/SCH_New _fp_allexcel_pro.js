/**
 * @author Jayesh//fp oter
 */

var f_current_month_actual_revenue_total_proj_level_f = 0;
var a_recognized_revenue_total_proj_level_f = new Array();
var f_total_cost_overall_prac_f = new Array();
var flag_counter_f = 0;
var f_revenue_arr_f = new Array();
var flag_counter_arr_f = 0;
var fp_xml = '';
var tm='';
var milestone_xml = '';
//var user = 1542;
var temp_fp_othr ='';
function sch_fp_rev_project_report() {
try {
		var context = nlapiGetContext();
		var excel_data_res = [];
		var date_proj = new Date();
		var sr_num = 0;
		var a_practice_list = new Array();
		//var user = nlapiGetUser();
		if (user < 0) {
			user = 7905;
		}
		if (user = 30484) {
			user = 1542;
		}
		
		//var user = 1542;
		var user =nlapiGetUser();
		var email = nlapiLookupField('employee', user, 'email');
		
		nlapiLogExecution('DEBUG', 'USER', email);
		nlapiLogExecution('DEBUG', 'USER', user);
		
		var pract_col = new Array();
		//
		var pract_fil = [
		            				[
		            						[
		            								'custrecord_practicehead',
		            								'anyof', user ],
		            						'or',
		            						[
		            								'custrecord_practice_head_2',
		            								'anyof', user ],
		            						'and',
		            						[
		            								'isinactive',
		            								'is', 'F' ]]];
		
		pract_col[0] = new nlobjSearchColumn('internalid');
		var pract_search = nlapiSearchRecord('department', null, pract_fil,
				pract_col);
		if (pract_search) {
			for (var i_prac = 0; i_prac < pract_search.length; i_prac++) {
				a_practice_list.push(pract_search[i_prac]
						.getValue('internalid'));
			}
		}
	
		
		var a_get_logged_in_user_exsiting_revenue_cap = '';
		var a_project_search_results = '';
		if(_logValidation(a_practice_list)){
		var a_revenue_cap_filter = [
				[
						[
								'custrecord_fp_rev_rec_others_projec.custentity_projectmanager',
								'anyof', user ],
						'or',
						[
								'custrecord_fp_rev_rec_others_projec.custentity_deliverymanager',
								'anyof', user ],
						'or',
						[
								'custrecord_fp_rev_rec_others_projec.custentity_clientpartner',
								'anyof', user ],
						'or',
						[
								'custrecord_fp_rev_rec_others_projec.custentity_practice',
								'anyof', a_practice_list ],
						'or',
						[
								'custrecord_fp_rev_rec_others_customer.custentity_clientpartner',
								'anyof', user ] ], 'and',
				[ 'custrecord_revenue_other_approval_status', 'anyof', 2 ] ];}

		var a_columns_existing_cap_srch = new Array();
		a_columns_existing_cap_srch[0] = new nlobjSearchColumn(
				'custrecord_fp_rev_rec_others_projec');
		a_columns_existing_cap_srch[1] = new nlobjSearchColumn(
				'custrecord_fp_rev_rec_others_total_rev');
		a_columns_existing_cap_srch[2] = new nlobjSearchColumn('created')
				.setSort(true);
		a_columns_existing_cap_srch[3] = new nlobjSearchColumn(
				'custrecord_revenue_other_approval_status');
		if (parseInt(user) == parseInt(41571)
				|| parseInt(user) == parseInt(62082)
				|| parseInt(user) == parseInt(7905)
				|| parseInt(user) == parseInt(35819)
				|| parseInt(user) == parseInt(90037)
				|| parseInt(user) == parseInt(30484)
				|| parseInt(user) == parseInt(72055)
				|| parseInt(user) == parseInt(2301)) {
			nlapiYieldScript();
			var a_project_cap_filter = [
					'custrecord_revenue_other_approval_status', 'anyof', 2 ];

			a_get_logged_in_user_exsiting_revenue_cap = searchRecord(
					'customrecord_fp_rev_rec_others_parent', null,
					a_project_cap_filter, a_columns_existing_cap_srch);
		} else
			a_get_logged_in_user_exsiting_revenue_cap = searchRecord(
					'customrecord_fp_rev_rec_others_parent', null,
					a_revenue_cap_filter, a_columns_existing_cap_srch);
		if (_logValidation(a_get_logged_in_user_exsiting_revenue_cap)) {
			for (var i = 0; i < a_get_logged_in_user_exsiting_revenue_cap.length; i++)
			//for(var i=0;i< 1;i++)
			{
				var pid = a_get_logged_in_user_exsiting_revenue_cap[i]
						.getValue('custrecord_fp_rev_rec_others_projec');

				if (pid) {
					if(_logValidation(a_practice_list)){

					var a_project_filter = [
												[
														[ 'custentity_projectmanager', 'anyof',
																user ],
														'or',
														[ 'custentity_deliverymanager', 'anyof',
																user ],
														'or',
														[ 'custentity_clientpartner', 'anyof', user ],
														'or',
														[
																'custentity_practice.custrecord_practicehead',
																'anyof', user ],
														'or',
														['custentity_practice.custrecord_practice_head_2','anyof', user],'or',
														[
															'custentity_practice',
															'anyof', a_practice_list ],
													'or',
														[ 'customer.custentity_clientpartner',
																'anyof', user ] ], 'and',
												[ 'status', 'noneof', 1 ], 'and',
												[ 'internalid', 'anyof', pid ], 'and',
												[ 'enddate', 'onorafter', date_proj ], 'and',
												[ 'jobtype', 'anyof', 2 ], 'and',
												[ 'jobbillingtype', 'anyof', 'FBM' ], 'and',
												[ 'custentity_fp_rev_rec_type', 'anyof', [ 2, 4 ] ] ];}
					//['custentity_fp_rev_rec_type', 'noneof', '@NONE@']
					var a_columns_proj_srch = new Array();
					a_columns_proj_srch[0] = new nlobjSearchColumn(
							'custentity_projectvalue');
					a_columns_proj_srch[1] = new nlobjSearchColumn('customer');
					a_columns_proj_srch[2] = new nlobjSearchColumn(
							'custentity_fp_rev_rec_type');
					a_columns_proj_srch[3] = new nlobjSearchColumn(
							'custentity_ytd_rev_recognized');
					a_columns_proj_srch[4] = new nlobjSearchColumn('startdate');
					a_columns_proj_srch[5] = new nlobjSearchColumn(
							'custentity_rev_rec_new_strt_date');
					a_columns_proj_srch[6] = new nlobjSearchColumn('enddate');
					a_columns_proj_srch[7] = new nlobjSearchColumn(
							'custentity_project_currency');
					a_columns_proj_srch[8] = new nlobjSearchColumn('internalid');
					a_columns_proj_srch[9] = new nlobjSearchColumn(
							'companyname');
					a_columns_proj_srch[10] = new nlobjSearchColumn('entityid');
					if (parseInt(user) == parseInt(41571)
							|| parseInt(user) == parseInt(7905)
							|| parseInt(user) == parseInt(35819)
							|| parseInt(user) == parseInt(90037)
							|| parseInt(user) == parseInt(30484)
							|| parseInt(user) == parseInt(72055)
							|| parseInt(user) == parseInt(2301)) {
						var a_project_filter = [ [ 'status', 'noneof', 1 ],
								'and', [ 'jobtype', 'anyof', 2 ], 'and',
								[ 'jobbillingtype', 'anyof', 'FBM' ], 'and',
								[ 'custentity_fp_rev_rec_type', 'anyof', 4 ],
								'and', [ 'enddate', 'onorafter', date_proj ],
								'and', [ 'internalid', 'anyof', pid ] ];
						nlapiYieldScript();

						a_project_search_results = searchRecord('job', null,
								a_project_filter, a_columns_proj_srch);
					} else {
						a_project_search_results = searchRecord('job', null,
								a_project_filter, a_columns_proj_srch);
					}
					nlapiYieldScript();
					for (var pm = 0; pm < a_project_search_results.length; pm++) {
						f_current_month_actual_revenue_total_proj_level_f = 0;
						a_recognized_revenue_total_proj_level_f = new Array();
						flag_counter_f = 0;
						f_revenue_arr_f = new Array();
						flag_counter_arr_f = 0;
						f_total_cost_overall_prac_f = new Array();
						var excel_dat = {
							'excel_data' : []
						};
						var data = '';
						var internalid = a_project_search_results[pm]
								.getValue('internalid');
						var proj_name = a_project_search_results[pm]
								.getValue('companyname');
						var d_proj_start_date = a_project_search_results[pm]
								.getValue('startdate');
						var d_proj_strt_date_old_proj = a_project_search_results[pm]
								.getValue('custentity_rev_rec_new_strt_date');
						var d_proj_end_date = a_project_search_results[pm]
								.getValue('enddate');
						var s_proj_currency = a_project_search_results[pm]
								.getText('custentity_project_currency');
						var s_proj_customer = a_project_search_results[pm]
						.getText('customer');
						var s_proj_type = a_project_search_results[pm]
						.getText('custentity_fp_rev_rec_type');
						nlapiLogExecution('DEBUG', 'internalid', internalid);
						var proj_id = a_project_search_results[pm]
								.getValue('entityid');
						if (d_proj_strt_date_old_proj) {
							d_proj_start_date = d_proj_strt_date_old_proj;
						}

						var d_pro_strtDate = nlapiStringToDate(d_proj_start_date);
						var i_year_project = d_pro_strtDate.getFullYear();
						var s_pro_endDate = nlapiStringToDate(d_proj_end_date);
						var i_year_project_end = s_pro_endDate.getFullYear();
						var i_project_mnth = d_pro_strtDate.getMonth();
						var i_prject_end_mnth = s_pro_endDate.getMonth();
						var s_currency_symbol_proj = s_proj_currency;
						var s_currency_symbol_usd = getCurrency_Symbol('USD');
						// get previous mnth effrt for true up and true down scenario

						data = getMargin_f(internalid, i_year_project,
								s_currency_symbol_proj, d_pro_strtDate,
								s_pro_endDate, i_project_mnth,
								s_currency_symbol_usd, i_prject_end_mnth,
								i_year_project_end);
						for (var dataln = 0; dataln < data.length; dataln++) {

							excel_dat['excel_data'].push({
								'proj' : proj_id,
								'project_id' : proj_name,
								'currency' : s_currency_symbol_proj,
								'project_name' : data[dataln].project_id,
								'practice' : data[dataln].project_name,
								'customer' : s_proj_customer,
								'type' : s_proj_type,
								'st_date' : data[dataln].month,
								'actual_rev' : data[dataln].actual_rev,

							});
							//sr_num++;

						}
						excel_data_res.push(excel_dat);
						//sr_num++;
					}

				}
			}
			temp_fp_othr = excel_data_res;
			down_excel_function_f(excel_data_res, email);
			nlapiLogExecution('debug', 'te', fp_xml);
		}
	} catch (err) {
		nlapiLogExecution('ERROR', 'SCh_fp_rev_rec_report error',
				'ERROR MESSAGE :- ' + err);
		throw err;
	}

	//milestone code starts
	try {
		var context = nlapiGetContext();
		var excel_data_res = [];
		var a_practice_list = new Array();
		var date_proj = new Date();
		var sr_num = 0;
		var user = nlapiGetUser();
		//var user = nlapiGetUser();
		
		var email = nlapiLookupField('employee', user, 'email');
		
		var str_user = parseInt(user).toString();
	
		var pract_fil = [
	         				[
	         						[
	         								'custrecord_practicehead',
	         								'anyof', user ],
	         						'or',
	         						[
	         								'custrecord_practice_head_2',
	         								'anyof', user ],
	         						'and',
	         						[
	         								'isinactive',
	         								'is', 'F' ]]];
		
		var pract_col = new Array();
		pract_col[0] = new nlobjSearchColumn('internalid');
		var pract_search = nlapiSearchRecord('department', null, pract_fil,
				pract_col);
		if (pract_search) {
			for (var i_prac = 0; i_prac < pract_search.length; i_prac++) {
				a_practice_list.push(pract_search[i_prac]
						.getValue('internalid'));
			}
		}
		
		
		var a_get_logged_in_user_exsiting_revenue_cap = '';
		var a_project_search_results = '';
		if(_logValidation(a_practice_list)){
		var a_revenue_cap_filter = [
				[
						[
								'custrecord_revenue_share_project.custentity_projectmanager',
								'anyof', user ],
						'or',
						[
								'custrecord_revenue_share_project.custentity_deliverymanager',
								'anyof', user ],
						'or',
						[
								'custrecord_revenue_share_project.custentity_clientpartner',
								'anyof', user ],
						'or',
						[
								'custrecord_revenue_share_project.custentity_practice',
								'anyof', a_practice_list ],
						'or',
						[
								'custrecord_revenue_share_cust.custentity_clientpartner',
								'anyof', user ] ], 'and',
				[ 'custrecord_revenue_share_approval_status', 'anyof', 3 ] ];}

		var a_columns_existing_cap_srch = new Array();
		a_columns_existing_cap_srch[0] = new nlobjSearchColumn(
				'custrecord_revenue_share_project');
		a_columns_existing_cap_srch[1] = new nlobjSearchColumn(
				'custrecord_revenue_share_total');
		a_columns_existing_cap_srch[2] = new nlobjSearchColumn('created')
				.setSort(true);
		a_columns_existing_cap_srch[3] = new nlobjSearchColumn(
				'custrecord_revenue_share_approval_status');
		if (parseInt(user) == parseInt(41571)
				|| parseInt(user) == parseInt(7905)
				|| parseInt(user) == parseInt(35819)
				|| parseInt(user) == parseInt(90037)
				|| parseInt(user) == parseInt(30484)
				|| parseInt(user) == parseInt(72055)
				|| parseInt(user) == parseInt(2301)) {
			//var a_project_cap_filter =[ ['custrecord_revenue_share_approval_status','anyof',3],'and',
			//                            ['internalid','anyof',75]];
			var a_project_cap_filter = [ [
					'custrecord_revenue_share_approval_status', 'anyof', 3 ] ];
			nlapiYieldScript();

			a_get_logged_in_user_exsiting_revenue_cap = searchRecord(
					'customrecord_revenue_share', null, a_project_cap_filter,
					a_columns_existing_cap_srch);
		} else
			a_get_logged_in_user_exsiting_revenue_cap = searchRecord(
					'customrecord_revenue_share', null, a_revenue_cap_filter,
					a_columns_existing_cap_srch);
		if (a_get_logged_in_user_exsiting_revenue_cap) {
			for (var i = 0; i < a_get_logged_in_user_exsiting_revenue_cap.length; i++)
			//for(var i=0;i< 1;i++)
			{
				var pid = a_get_logged_in_user_exsiting_revenue_cap[i]
						.getValue('custrecord_revenue_share_project');

				if (pid) {
					if(_logValidation(a_practice_list)){
					var a_project_filter = [[['custentity_projectmanager', 'anyof', user], 'or',
												['custentity_deliverymanager', 'anyof', user], 'or',
												['custentity_clientpartner', 'anyof', user], 'or',
												['custentity_practice.custrecord_practicehead','anyof', user],'or',
												['custentity_practice.custrecord_practice_head_2','anyof', user],'or',
												['custentity_practice','anyof', a_practice_list],'or',
												['customer.custentity_clientpartner', 'anyof', user]], 'and',
												['status', 'noneof', 1], 'and',
												['internalid', 'anyof',pid], 'and',
												['jobtype', 'anyof', 2], 'and',
				                                ['enddate','onorafter',date_proj],'and',
												['jobbillingtype', 'anyof', 'FBM'], 'and',
												['custentity_fp_rev_rec_type', 'anyof', 1]];}
					//['custentity_fp_rev_rec_type', 'noneof', '@NONE@']
					var a_columns_proj_srch = new Array();
					a_columns_proj_srch[0] = new nlobjSearchColumn(
							'custentity_projectvalue');
					a_columns_proj_srch[1] = new nlobjSearchColumn('customer');
					a_columns_proj_srch[2] = new nlobjSearchColumn(
							'custentity_fp_rev_rec_type');
					a_columns_proj_srch[3] = new nlobjSearchColumn(
							'custentity_ytd_rev_recognized');
					a_columns_proj_srch[4] = new nlobjSearchColumn('startdate');
					a_columns_proj_srch[5] = new nlobjSearchColumn(
							'custentity_rev_rec_new_strt_date');
					a_columns_proj_srch[6] = new nlobjSearchColumn('enddate');
					a_columns_proj_srch[7] = new nlobjSearchColumn(
							'custentity_project_currency');
					a_columns_proj_srch[8] = new nlobjSearchColumn('internalid');
					a_columns_proj_srch[9] = new nlobjSearchColumn(
							'companyname');
					a_columns_proj_srch[10] = new nlobjSearchColumn('entityid');
					if (parseInt(user) == parseInt(41571)
							|| parseInt(user) == parseInt(7905)
							|| parseInt(user) == parseInt(35819)
							|| parseInt(user) == parseInt(90037)
							|| parseInt(user) == parseInt(30484)
							|| parseInt(user) == parseInt(72055)
							|| parseInt(user) == parseInt(2301)) {
						var a_project_filter = [ [ 'status', 'noneof', 1 ],
								'and', [ 'jobtype', 'anyof', 2 ], 'and',
								[ 'jobbillingtype', 'anyof', 'FBM' ], 'and',
								[ 'custentity_fp_rev_rec_type', 'anyof', 1 ],
								'and', [ 'enddate', 'onorafter', date_proj ],
								'and', [ 'internalid', 'anyof', pid ] ];
						nlapiYieldScript();

						a_project_search_results = searchRecord('job', null,
								a_project_filter, a_columns_proj_srch);
					} else {
						a_project_search_results = searchRecord('job', null,
								a_project_filter, a_columns_proj_srch);
					}

					for (var pm = 0; pm < a_project_search_results.length; pm++) {
						f_current_month_actual_revenue_total_proj_level = 0;
						a_recognized_revenue_total_proj_level = new Array();
						var excel_dat = {
							'excel_data' : []
						};
						var data = '';
						var internalid = a_project_search_results[pm]
								.getValue('internalid');
						var proj_name = a_project_search_results[pm]
								.getValue('companyname');
						var d_proj_start_date = a_project_search_results[pm]
								.getValue('startdate');
						var d_proj_strt_date_old_proj = a_project_search_results[pm]
								.getValue('custentity_rev_rec_new_strt_date');
						var d_proj_end_date = a_project_search_results[pm]
								.getValue('enddate');
						var s_proj_currency = a_project_search_results[pm]
								.getText('custentity_project_currency');
						var proj_id = a_project_search_results[pm]
								.getValue('entityid');
						var s_proj_customer = a_project_search_results[pm]
						.getText('customer');
						var s_proj_type = a_project_search_results[pm]
						.getText('custentity_fp_rev_rec_type');
						//nlapiLogExecution('DEBUG', 'internalid', internalid);
						if (d_proj_strt_date_old_proj) {
							d_proj_start_date = d_proj_strt_date_old_proj;
						}

						var d_pro_strtDate = nlapiStringToDate(d_proj_start_date);
						var i_year_project = d_pro_strtDate.getFullYear();
						var s_pro_endDate = nlapiStringToDate(d_proj_end_date);
						var i_year_project_end = s_pro_endDate.getFullYear();
						var i_project_mnth = d_pro_strtDate.getMonth();
						var i_prject_end_mnth = s_pro_endDate.getMonth();
						var s_currency_symbol_proj = s_proj_currency;
						var s_currency_symbol_usd = getCurrency_Symbol('USD');
						// get previous mnth effrt for true up and true down scenario

						data = getMargin(internalid, i_year_project,
								s_currency_symbol_proj, d_pro_strtDate,
								s_pro_endDate, i_project_mnth,
								s_currency_symbol_usd, i_prject_end_mnth,
								i_year_project_end);
						for (var dataln = 0; dataln < data.length; dataln++) {

							excel_dat['excel_data']
									.push({
										'proj' : proj_id,
										'project_id' : proj_name,
										'project_name' : data[dataln].project_id,
										'practice' : data[dataln].project_name,
										'currency' : s_currency_symbol_proj,
										'st_date' : data[dataln].month,
										'actual_rev' : data[dataln].actual_rev,
										'cumm_rev' : data[dataln].cumm_rev,
										'monthlyrevenue' : data[dataln].monthlyrevenue,
										'rev_rec_permnth' : data[dataln].rev_rec_permnth,
										'act_rev_rec_mnth' : data[dataln].act_rev_rec_mnth,
										'cummulative_mnth' : data[dataln].cummulative_mnth,
										'type' : s_proj_type,
										'customer' : s_proj_customer
									
									});
							//sr_num++;

						}
						excel_data_res.push(excel_dat);
						//sr_num++;
					}

				}
			}
			temp_fp_othr = temp_fp_othr + excel_data_res;
			//xml(temp_fp_othr);
			if (excel_data_res != 0)
				down_excel_function(excel_data_res, email);
				

		}
	} catch (err) {
		nlapiLogExecution('ERROR', 'SCh_fp_rev_rec_report error',
				'ERROR MESSAGE :- ' + err);
		throw err;
	}
	
	//tm starts
	
    var d_today = new Date();
   //var d_today = '2/1/2018';
//   d_today = nlapiStringToDate(d_today);

    var error = false;
    var dataRow = [];
    //Conversion Rates table
    var inr_to_usd = 0;
    var gbp_to_usd = 0;
    var eur_to_usd = 0;
    var proj ='';
    var a_conversion_rate_table = nlapiSearchRecord('customrecord_conversion_rate_sfdc',null,null,[new nlobjSearchColumn('custrecord_rate_inr_to_usd'),
                                                                  new nlobjSearchColumn('custrecord_rate_gbp_to_usd'),
                                                                  new nlobjSearchColumn('custrecord_rate_eur_to_usd'),
                                                                  new nlobjSearchColumn('internalid').setSort(true)]);
    if(a_conversion_rate_table)
          inr_to_usd = 1 / parseFloat(a_conversion_rate_table[0].getValue('custrecord_rate_inr_to_usd'));
          gbp_to_usd = a_conversion_rate_table[0].getValue('custrecord_rate_gbp_to_usd');
          eur_to_usd = a_conversion_rate_table[0].getValue('custrecord_rate_eur_to_usd');
          var o_total_array = [];
          var dte_array = [];
          //
          var date = new Date();
          var year = date.getFullYear();
          var startDate = '1/1/'+year;
          startDate = nlapiStringToDate(startDate);
          var o_total_curnt = [];   
          var SkipMnt =false;
   for (var i = 0; i <= 24; i++) {
          nlapiYieldScript();
          
          
          var d_day = nlapiAddMonths(startDate, i);
          /*for (var i = -1; i < 24; i++) {
              nlapiYieldScript();
              var d_day = nlapiAddMonths(d_today, i);*/
              
          
          var d_month_start = nlapiAddDays(d_day, -1 * (d_day.getDate() - 1));
          var s_month_start = d_month_start.getMonth() + 1 + '/'
                  + d_month_start.getDate() + '/' + d_month_start.getFullYear();
          var d_month_end = nlapiAddDays(nlapiAddMonths(d_month_start, 1), -1);
          var s_month_end = d_month_end.getMonth() + 1 + '/'
                  + d_month_end.getDate() + '/' + d_month_end.getFullYear();
          dte_array.push(d_month_end);
       
          try {
                var JSON_Obj = {};
                var JSON_Obj2 = {};
               
               var a_data = getRevenueData(s_month_start, s_month_end,
                        d_month_start, d_month_end,inr_to_usd,gbp_to_usd,eur_to_usd);
              
              // a_data.sort(dynamicSort("custpage_employee_department"));
                
                var projects_list = new Array();
                for(var i2 in a_data){
                	projects_list.push(a_data[i2].custpage_project.split(' ')[0]+":"+a_data[i2].custpage_employee_department);
                }
                projects_list = removearrayduplicate(projects_list);
                for(var i1 in projects_list){
                	var total =0;
                	 
                	for(var j in  a_data){
                		if( projects_list[i1]== a_data[j].custpage_project.split(' ')[0]+":"+a_data[j].custpage_employee_department){
                			total = total + parseInt(a_data[j].custpage_total_amount);
                			JSON_Obj = {
                					'ProjectID': a_data[j].custpage_project.split(' ')[0],
                					'ProjectIDsbdp': a_data[j].custpage_project.split(' ')[0]+":"+a_data[j].custpage_employee_department,
                					'ProjectName': a_data[j].custpage_project,
                					'ProjectCurrency': a_data[j].custpage_currency,
                                    'Practice': a_data[j].custpage_employee_parent_department,
                                    'Sub_Practice': a_data[j].custpage_employee_department,
                                    'Rev_Rec_Type': 'T&M',
                                    'Customer': a_data[j].custpage_customer,
                                    'blank': '',
                                    'month_amount':total ,
                                    'end_date':d_month_end
                                    
                        };
                			
                		}
                	}
                	//o_total_curnt.push(JSON_Obj)
                	o_total_array.push(JSON_Obj);
                }
                nlapiLogExecution('debug', 'response', o_total_array.length);
         
               
                }
                
               
          
           catch (e) {
                nlapiLogExecution('ERROR', 'Error: ', e.message);
                error = true;
          }
    }
    projects_list =[];
  

  //  var strVar2 = '';    
for(var i in o_total_array){
  projects_list.push(o_total_array[i].ProjectIDsbdp);
}
projects_list = removearrayduplicate(projects_list);
createXml(projects_list, o_total_array, dte_array); //
//createXml(projects_list, o_total_curnt, dte_array);


	//tm ends
milestone_xml = fp_xml + milestone_xml + tm;
var strVar2 ='';
strVar2 += "<table width=\"100%\">";
strVar2 += '<table width="100%" border="1">';
strVar2 += milestone_xml;
strVar2 += "<\/table>";

	var filetm = nlapiCreateFile('Project Revenue Report FP other,milestone and T&M other.xls',
			'XMLDOC', strVar2);
	
	
	//var rec = email;
	var a_emp_attachment = new Array();
	a_emp_attachment['entity'] = 62082;
	var email = 'bidhi.raj@brillio.com';
	nlapiSendEmail(7905, email, 'Project Revenue Recognized Report',
			'Please find the attachement of project rev rec report', null,
			'bidhi.raj@brillio.com', a_emp_attachment, filetm, true, null,
			null);
	
	
	
}
function _logValidation(value) 
{
 if(value != null && value.toString() != null && value != '' && value != undefined && value.toString() != undefined && value != 'undefined' && value.toString() != 'undefined'&& value.toString() != 'NaN' && value != NaN) 
 {
  return true;
 }
 else 
 { 
  return false;
 }
}
function getRevenueData(s_from_date, s_to_date, d_from_date,
        d_to_date,inr_to_usd,gbp_to_usd,eur_to_usd)
{

      //var o_holidays = getHolidays(d_from_date, d_to_date);
      //var i_working_days = parseInt(calcBusinessDays(d_from_date, d_to_date));
      var chk_show_ot_hours = 'T';
      
      // Get Tagged Projects
      var dataRow_Proj_list = [];
      var searchProjects = nlapiSearchRecord('job','customsearch2339',null,[new nlobjSearchColumn('internalid')]);
      for(var k=0;k<searchProjects.length;k++){
    	  dataRow_Proj_list.push(searchProjects[k].getValue('internalid'));
      }
      if(parseInt(nlapiGetUser()) == parseInt(30484)){
    	  var dataRow_Proj_list = [];
          var searchProjects = nlapiSearchRecord('job','customsearch2348',null,[new nlobjSearchColumn('internalid')]);
          for(var k=0;k<searchProjects.length;k++){
        	  dataRow_Proj_list.push(searchProjects[k].getValue('internalid'));
          }
      }
      // Resource Allocation Data
      var a_resource_allocation = getResourceAllocations(d_from_date, d_to_date,dataRow_Proj_list);

      nlapiYieldScript();
      var i_current_employee = null;

      var a_res_alloc_for_each_employee = new Object();

      var a_exception_employees = new Array();

      var a_billable_employees = new Array();

      var a_currency_rate = new Object();

      for (var i_res_alloc_indx = 0; a_resource_allocation != null
              && i_res_alloc_indx < a_resource_allocation.length; i_res_alloc_indx++) {
            var o_resource_allocation = a_resource_allocation[i_res_alloc_indx];
            var i_employee_id = o_resource_allocation.employee;
            var i_project_id = o_resource_allocation.project_id.toString();
            if (a_res_alloc_for_each_employee[i_employee_id] == undefined) {
                  a_res_alloc_for_each_employee[i_employee_id] = new Object();
                  a_res_alloc_for_each_employee[i_employee_id][i_project_id] = o_resource_allocation;
                  a_billable_employees.push(i_employee_id);
            } else {
                  a_res_alloc_for_each_employee[i_employee_id][i_project_id] = o_resource_allocation;

                  // a_exception_employees.push(i_employee_id);
            }

            /*
            * if(o_resource_allocation.start_date > d_from_date ||
            * o_resource_allocation.end_date < d_to_date) {
            * a_exception_employees.push(i_employee_id); }
            */
      }

      /*var a_search_data = getData(d_from_date, d_to_date, a_billable_employees
              );*/
      var a_data = new Array();
      for (var i_list_indx = 0; a_billable_employees != null
              && i_list_indx < a_billable_employees.length; i_list_indx++) {
            var i_employee_id = a_billable_employees[i_list_indx];

            if (a_exception_employees.indexOf(i_employee_id) == -1) {
                  var a_res_allocations_for_this_employee = a_res_alloc_for_each_employee[i_employee_id];
                  for ( var i_project_id in a_res_allocations_for_this_employee) {
                        var a_row_data = new Object();

                        //var o_search_result = null;

                  //    if (a_search_data[i_employee_id] != undefined) {
                  //          o_search_result = a_search_data[i_employee_id][i_project_id];
                  //    }

                        var o_resource_allocation = a_res_alloc_for_each_employee[i_employee_id][i_project_id];
                        var s_conversion_rate_date = s_from_date;
                        var d_today = new Date();
                        if (d_today < d_from_date) {
                              s_conversion_rate_date = null;
                        }
                        if (a_currency_rate[o_resource_allocation.currency] == undefined) {
                              a_currency_rate[o_resource_allocation.currency] = nlapiExchangeRate(
                                      o_resource_allocation.currency, 'USD',
                                      s_conversion_rate_date);
                        }
                        //Conversion factor
                        var f_currency_conversion = 1;
                        if(o_resource_allocation.currency =='USD')
                              f_currency_conversion = 1;
                        if(o_resource_allocation.currency =='INR')
                              f_currency_conversion = inr_to_usd;
                        if(o_resource_allocation.currency =='EUR')
                              f_currency_conversion.currency = eur_to_usd;
                        if(o_resource_allocation.currency =='GBP')
                              f_currency_conversion = gbp_to_usd;
                              
                  //    var f_currency_conversion = parseFloat(a_currency_rate[o_resource_allocation.currency]);
                        a_row_data['custpage_vertical'] = o_resource_allocation.vertical;
                        a_row_data['custpage_vertical_id'] = o_resource_allocation.vertical_id;
                        a_row_data['custpage_employee'] = o_resource_allocation.employee_name;
                        a_row_data['custpage_employee_id'] = o_resource_allocation.employee;
                        a_row_data['custpage_employee_department'] = o_resource_allocation.department;
                        a_row_data['custpage_employee_department_id'] = o_resource_allocation.department_id;
                        a_row_data['custpage_employee_parent_department'] = o_resource_allocation.parent_department;
                        a_row_data['custpage_project'] = o_resource_allocation.project_name;
                        a_row_data['custpage_project_id'] = o_resource_allocation.project_id;
                        a_row_data['custpage_customer'] = o_resource_allocation.customer;
                        a_row_data['custpage_customer_id'] = o_resource_allocation.customer_id;
                        a_row_data['custpage_holiday_hours'] = o_resource_allocation.holidays;
                        a_row_data['start_date'] = o_resource_allocation.start_date;
                        a_row_data['end_date'] = o_resource_allocation.end_date;
                        a_row_data['custpage_rate'] = (o_resource_allocation.monthly_billing == 'T') ? (o_resource_allocation.monthly_rate / (8.0 * i_working_days))
                                : o_resource_allocation.st_rate;
                  {
                              a_row_data['custpage_billed_hours'] = '0';
                              a_row_data['custpage_approved_hours'] = '0';
                              a_row_data['custpage_submitted_hours'] = '0';
                              a_row_data['custpage_billed_hours_ot'] = '0';
                              a_row_data['custpage_approved_hours_ot'] = '0';
                              a_row_data['custpage_submitted_hours_ot'] = '0';
                              a_row_data['custpage_not_submitted_hours'] = (((parseFloat(o_resource_allocation.workingdays)) -  parseFloat(o_resource_allocation.holidays))
                                      * 8
                                      * parseFloat(o_resource_allocation.percentoftime) / 100.0)
                                      .toString();
                              a_row_data['custpage_leave_hours'] = '0';
                              // a_row_data['custpage_holiday_hours'] = '0';

                              a_row_data['custpage_billed_amount'] = parseFloat(0.0)
                                      .toFixed(2);// *
                              // parseFloat(a_row_data['custpage_rate'])).toFixed(2);
                              a_row_data['custpage_billed_amount_ot'] = parseFloat(0.0)
                                      .toFixed(2);
                              a_row_data['custpage_approved_amount'] = parseFloat(0.0)
                                      .toFixed(2);// *
                              // parseFloat(a_row_data['custpage_rate'])).toFixed(2);
                              a_row_data['custpage_approved_amount_ot'] = parseFloat(0.0)
                                      .toFixed(2);
                              a_row_data['custpage_submitted_amount'] = parseFloat(0.0)
                                      .toFixed(2);// *
                              // parseFloat(a_row_data['custpage_rate'])).toFixed(2);
                              a_row_data['custpage_submitted_amount_ot'] = parseFloat(0.0)
                                      .toFixed(2);
                        }
                        a_row_data['custpage_count'] = parseFloat(o_resource_allocation.count);
                        a_row_data['custpage_allocated_days'] = parseFloat(o_resource_allocation.workingdays)- parseFloat(o_resource_allocation.holidays);
                        a_row_data['custpage_percent'] = o_resource_allocation.percentoftime
                                .toString();
                        a_row_data['custpage_not_submitted_amount'] = parseFloat(
                                parseFloat(a_row_data['custpage_not_submitted_hours'])
                                        * parseFloat(a_row_data['custpage_rate'])
                                    //    * (parseFloat(a_row_data['custpage_percent']) / 100.0)
                                        * f_currency_conversion).toFixed(2);
                        a_row_data['custpage_total_amount'] = parseFloat(a_row_data['custpage_billed_amount'])
                                + parseFloat(a_row_data['custpage_approved_amount'])
                                + parseFloat(a_row_data['custpage_submitted_amount'])
                                + parseFloat(a_row_data['custpage_not_submitted_amount']);
                        if (chk_show_ot_hours == 'T') {
                              a_row_data['custpage_total_amount'] += parseFloat(a_row_data['custpage_billed_amount_ot'])
                                      + parseFloat(a_row_data['custpage_approved_amount_ot'])
                                      + parseFloat(a_row_data['custpage_submitted_amount_ot']);
                        }
                        a_row_data['custpage_total_amount'] = a_row_data['custpage_total_amount']
                                .toFixed(2);
                        a_row_data['custpage_currency'] = o_resource_allocation.currency;
                        a_row_data['custpage_t_and_m_monthly'] = o_resource_allocation.monthly_billing == 'T' ? 'T & M Monthly'
                                : 'T & M';
                        a_row_data['custpage_end_customer'] = o_resource_allocation.end_customer;
                        a_data.push(a_row_data);// [i_list_indx] = a_row_data;

                  }

            }
      }
      nlapiLogExecution('AUDIT', 'Number of Records: ', a_data.length);
      return a_data;
}
function createXml(projects_list,o_total_array,dte_array){
	  var strVar1= '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:ss="urn:schemas-microsoft-com:office:spreadsheet" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40">'+
    '<head>'+
    '<meta http-equiv=Content-Type content="text/html; charset=windows-1252"/>'+
'<meta name=ProgId content=Excel.Sheet/>'+
'<meta name=Generator content="Microsoft Excel 11"/>'+
'<!--[if gte mso 9]><xml>'+
'<x:excelworkbook>'+
'<x:excelworksheets>'+
'<x:excelworksheet=sheet1>'+
'<x:name>** ESTIMATE FILE**</x:name>'+
'<x:worksheetoptions>'+
'<x:selected></x:selected>'+
'<x:freezepanes></x:freezepanes>'+
'<x:frozennosplit></x:frozennosplit>'+
'<x:splithorizontal>** FROZEN ROWS + 1 **</x:splithorizontal>'+
'<x:toprowbottompane>** FROZEN ROWS + 1 **</x:toprowbottompane>'+
'<x:splitvertical>** FROZEN COLUMNS + 1 **</x:splitvertical>'+
'<x:leftcolumnrightpane>** FROZEN COLUMNS + 1**</x:leftcolumnrightpane>'+
        '<x:activepane>0</x:activepane>'+// 0
        '<x:panes>'+
                    '<x:pane>'+
                    '<x:number>3</x:number>'+
                    '</x:pane>'+
                    '<x:pane>'+
        '<x:number>1</x:number>'+
        '</x:pane>'+
        '<x:pane>'+
                    '<x:number>2</x:number>'+
'</x:pane>'+
'<x:pane>'+
'<x:number>0</x:number>'+//1
'</x:pane>'+
'</x:panes>'+
'<x:protectcontents>False</x:protectcontents>'+
'<x:protectobjects>False</x:protectobjects>'+
'<x:protectscenarios>False</x:protectscenarios>'+
'</x:worksheetoptions>'+
'</x:excelworksheet>'+
    '</x:excelworksheets>'+
'<x:protectstructure>False</x:protectstructure>'+
'<x:protectwindows>False</x:protectwindows>'+
    '</x:excelworkbook>'+


                    
              
'</x:excelworkbook>'+
'</xml><![endif]-->'+
'<style>'+
    'p.MsoFooter, li.MsoFooter, div.MsoFooter'+
    '{ margin:0in; margin-bottom:.0001pt; mso-pagination:widow-orphan; tab-stops:center 3.0in right 6.0in; font-size:12.0pt;}'+
    '<style>'+

    '<!-- /* Style Definitions */'+

   

    'div.Section1'+
    '{ page:Section1;}'+

    'table#hrdftrtbl'+
    '{ margin:0in 0in 0in 900in; width:1px; height:1px; overflow:hidden; font-family:Arial;}-->'+

    '</style>'+
    
    
    '</head>'+


    '<body>'+
    
    '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">'
    
    nlapiYieldScript();
    var strVar2 = '';   
	  var p =''; var flg =0; var flg2 =0;
	  var curncy = 'USD';
   /* strVar2 += "<table width=\"100%\">";
		strVar2+= '<table width="100%" border="1">';*/
	  
	 for(var k in projects_list){
		 var total=0;
 	  strVar2+="<tr>";
 	// strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \>" +projects_list[k].split("\\:", 2)[0]+ "</td>";
 			 
 				 for(var d in dte_array){
 					
 					 
 					 if(d!=0 &&  flg !=1){
 						 strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \>"+p+"</td>"; 
 					 }
 					 
 					flg =0;
 					 for(var k1 in o_total_array){
 						
 			  if(o_total_array[k1].ProjectIDsbdp ==projects_list[k] && dte_array[d] == o_total_array[k1].end_date){
 				
 				if(k == flg2 ){
 					var blank = 'Revenue to be reconized/month';
 					strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \>" +o_total_array[k1].ProjectID+ "</td>";
 					strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \>" +o_total_array[k1].ProjectName+ "</td>";
 					strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \>" +curncy+ "</td>";
 					strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \>" +o_total_array[k1].Practice+ "</td>";
 					strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \>" +o_total_array[k1].Sub_Practice+ "</td>";
 					strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \>" +o_total_array[k1].Rev_Rec_Type+ "</td>";
 					strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \>" +o_total_array[k1].Customer+ "</td>";
 					strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \>" +blank+ "</td>";
 					flg2++;
 				}
 				flg = 1;
 				  strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \>" +o_total_array[k1].month_amount+ "</td>";
 				 total =total+parseInt(o_total_array[k1].month_amount);
 				 break;
 			  }
 			  if(flg ==0 && o_total_array[k1].end_date >dte_array[d]&&o_total_array[k1].ProjectIDsbdp ==projects_list[k] ){
 				  var present = false;
 					for(var date in o_total_array){
 						//if(o_total_array[date].end_date == dte_array[d] && o_total_array[date].ProjectID == o_total_array[k1].ProjectID){
 						if(o_total_array[date].end_date == dte_array[d] && o_total_array[date].ProjectIDsbdp == projects_list[k]){
 							present =true;
 							 break;
 						}
 					}
 					if(present == false){
 					if(k == flg2 ){
 				 var blank = 'Revenue to be reconized/month';
 				   strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \>" +o_total_array[k1].ProjectID+ "</td>";
					strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \>" +o_total_array[k1].ProjectName+ "</td>";
					strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \>" +curncy+ "</td>";
					strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \>" +o_total_array[k1].Practice+ "</td>";
					strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \>" +o_total_array[k1].Sub_Practice+ "</td>";
					strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \>" +o_total_array[k1].Rev_Rec_Type+ "</td>";
					strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \>" +o_total_array[k1].Customer+ "</td>";
					strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \>" +blank+ "</td>";
					flg2++;
 						}
					flg = 1;
					var noval = '0';
	 				  strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \>" +noval+ "</td>";
	 				// strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \>" +o_total_array[k1].month_amount+ "</td>";
	 				 total =total+0;
 					}
 					break;
 			  }
 			  
 			 
 			  
 		  }
 		  
 	  }
 	//strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \>" +p+ "</td>";
 	strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \>" +total+ "</td>";

 	   strVar2 += "<\/tr>";
   }
   
  // strVar2 += "<\/table>";                                 
  // strVar1 = strVar1 + strVar2;
   tm=strVar2;
	}
function removearrayduplicate(cp) {
	var newArray = new Array();
	label: for (var i = 0; i < cp.length; i++) {
		for (var j = 0; j < cp.length; j++) {
			if (newArray[j] == cp[i])
				continue label;
		}
		newArray[newArray.length] = cp[i];
	}
	return newArray;
}
function getResourceAllocations(d_start_date, d_end_date,proList)
{

      nlapiLogExecution('AUDIT', 'getResourceAllocation Parameters', d_start_date
              + ' - ' + d_end_date );

     
      var a_resource_allocations = new Array();

      // Get Resource allocations for this month
     // var  user =3169;//taking pro under hema
      var  user = nlapiGetUser();
     nlapiYieldScript();
     var a_practice_list = new Array();
     var pract_col = new Array();
		//
		/*var pract_fil = [
		            				[
		            						[
		            								'custrecord_practicehead',
		            								'anyof', user ],
		            						'or',
		            						[
		            								'custrecord_practice_head_2',
		            								'anyof', user ],
		            						'and',
		            						[
		            								'isinactive',
		            								'is', 'F' ]]];
		if(parseInt(user) == parseInt(30484)){
			var pract_fil = [	[	'isinactive','is', 'F' ]];
		}
		
		pract_col[0] = new nlobjSearchColumn('internalid');
		var pract_search = nlapiSearchRecord('department', null, pract_fil,
				pract_col);
		if (pract_search) {
			for (var i_prac = 0; i_prac < pract_search.length; i_prac++) {
				a_practice_list.push(pract_search[i_prac]
						.getValue('internalid'));
			}
		}*/
	
      
      var filters = new Array();
      //
      filters[0]  = new nlobjSearchFilter('formuladate', null, 'notafter',d_end_date);
      filters[0].setFormula('{custeventbstartdate}');
      filters[1] = new nlobjSearchFilter('formuladate', null, 'notbefore',d_start_date);
      filters[1].setFormula('{custeventbenddate}');
      filters[2] = new nlobjSearchFilter('custeventrbillable', null, 'is', 'T');
      filters[3] = new nlobjSearchFilter('jobbillingtype', 'job', 'anyof', 'TM');
      filters[4] = new nlobjSearchFilter('custentity_exclude_rev_forecast', 'job', 'is', 'F');
      filters[5] = new nlobjSearchFilter('project', null, 'anyof', proList);
    
      var columns = new Array();
      columns[0] = new nlobjSearchColumn('percentoftime', null, 'avg');
      columns[1] = new nlobjSearchColumn('custeventbstartdate', null, 'group');
      columns[2] = new nlobjSearchColumn('custeventbenddate', null, 'group');
      columns[3] = new nlobjSearchColumn('resource', null, 'group');
      columns[4] = new nlobjSearchColumn('project', null, 'group');
      columns[5] = new nlobjSearchColumn('custevent3', null, 'avg');
      columns[6] = new nlobjSearchColumn('custevent_monthly_rate', null, 'avg');
      columns[7] = new nlobjSearchColumn('custeventrbillable', null, 'group');
      columns[8] = new nlobjSearchColumn('departmentnohierarchy', 'employee',
    'group');
      columns[9] = new nlobjSearchColumn('subsidiary', 'employee', 'group');
      columns[10] = new nlobjSearchColumn('customer', 'job', 'group');
      columns[11] = new nlobjSearchColumn('custentity_project_currency', 'job',
    'group');
      columns[12] = new nlobjSearchColumn('custentity_vertical', 'job', 'group');
      columns[13] = new nlobjSearchColumn('custentity_t_and_m_monthly', 'job',
    'group');
      columns[14] = new nlobjSearchColumn('custevent_monthly_rate', null, 'group');
      
      columns[15] = new nlobjSearchColumn('formulatext', null, 'group');
      columns[15]
              .setFormula('CASE WHEN INSTR({employee.department} , \' : \', 1) > 0 THEN SUBSTR({employee.department}, 1 , INSTR({employee.department} , \' : \', 1)) ELSE {employee.department} END');
      columns[16] = new nlobjSearchColumn('custentity_endcustomer', 'job',
              'group');
      columns[17] = new nlobjSearchColumn('internalid', null, 'count');
     
      try {
    	  nlapiYieldScript();
            var search_results = searchRecord('resourceallocation', null, filters,
                    columns);
    	//  nlapiYieldScript();
    	 /* var search_results = searchRecord('resourceallocation', null, filters,
                  columns);*/
      } catch (e) {
            nlapiLogExecution('ERROR', 'Error', e.message);
      }
      nlapiYieldScript();
      if (search_results != null && search_results.length > 0) {
    	//  nlapiYieldScript();
            for (var i_search_indx = 0; i_search_indx < search_results.length; i_search_indx++) {
                  var i_project_id = search_results[i_search_indx]
                          .getValue(columns[4]);
                  var s_project_name = search_results[i_search_indx]
                          .getText(columns[4]);
                  
                  var s_project_strdte= search_results[i_search_indx]
                  .getText(columns[1]);
                  var s_project_enddate= search_results[i_search_indx]
                  .getText(columns[2]);
                  
                  var is_resource_billable = search_results[i_search_indx]
                          .getValue(columns[7]);
                  var stRate = search_results[i_search_indx].getValue(columns[5]);
                  stRate = stRate ? parseFloat(stRate) : 0;
                  var i_employee = search_results[i_search_indx].getValue(columns[3]);
                  var s_employee_name = search_results[i_search_indx]
                          .getText(columns[3]);
                  var i_percent_of_time = search_results[i_search_indx]
                          .getValue(columns[0]);
                  var i_department_id = search_results[i_search_indx]
                          .getValue(columns[8]);
                  var s_department = search_results[i_search_indx]
                          .getValue(columns[8]);
                  var i_subsidiary = search_results[i_search_indx]
                          .getValue(columns[9]);
            //    var i_working_days = search_results[i_search_indx].getValue(columns[8]);
                  var allocationStartDate = search_results[i_search_indx].getValue(columns[1]);
                  var allocationEndDate = search_results[i_search_indx].getValue(columns[2]);
                  
                  //var i_working_days = parseInt(calcBusinessDays(d_start_date, d_end_date));;
                  
                  var i_customer_id = search_results[i_search_indx]
                          .getValue(columns[10]);
                  var s_customer = search_results[i_search_indx].getText(columns[10]);
                  var i_count = search_results[i_search_indx].getValue(columns[17]);
                  var i_currency = search_results[i_search_indx].getText(columns[11]);
                  var i_vertical_id = search_results[i_search_indx]
                          .getValue(columns[12]);
                  var s_vertical = search_results[i_search_indx].getText(columns[12]);
                  var is_T_and_M_monthly = search_results[i_search_indx]
                          .getValue(columns[13]);
                  var f_monthly_rate = search_results[i_search_indx]
                          .getValue(columns[14]);
                  f_monthly_rate = f_monthly_rate ? parseFloat(f_monthly_rate) : 0;
                  var s_parent_department = search_results[i_search_indx]
                          .getValue(columns[15]);
                  var s_end_customer = search_results[i_search_indx]
                          .getText(columns[16]);
                  
                  // days calculation
                  
                  
                  var d_allocationStartDate = nlapiStringToDate(allocationStartDate);
                  var d_allocationEndDate = nlapiStringToDate(allocationEndDate);
                  var d_provisionStartDate = d_start_date;
                  var d_provisionEndDate = d_end_date;
                  var provisionStartDate = nlapiDateToString(d_start_date);
                  var provisionEndDate = nlapiDateToString(d_end_date);

                  var provisionMonthStartDate = nlapiStringToDate((d_provisionStartDate
                          .getMonth() + 1)
                          + '/1/' + d_provisionStartDate.getFullYear());
                  var nextProvisionMonthStartDate = nlapiAddMonths(
                          provisionMonthStartDate, 1);
                  var provisionMonthEndDate = nlapiAddDays(
                          nextProvisionMonthStartDate, -1);
                  // nlapiLogExecution('debug', 'provisionMonthStartDate',
                  // provisionMonthStartDate);
                  // nlapiLogExecution('debug', 'provisionMonthEndDate',
                  // provisionMonthEndDate);

                  var startDate = d_allocationStartDate > d_provisionStartDate ? allocationStartDate
                          : provisionStartDate;
                  var endDate = d_allocationEndDate < d_provisionEndDate ? allocationEndDate
                          : provisionEndDate;
                  
                  // days calculation
                  var workingDays = parseFloat(getWorkingDays(
                              startDate, endDate));
                  var holidayDetailsInMonth = get_holidays(startDate,
                              endDate, i_employee, i_project_id, i_customer_id);
                  var holidayCountInMonth = holidayDetailsInMonth.length;

                  
                  
                  a_resource_allocations[i_search_indx] = {
                      project_id : i_project_id,
                      'project_name' : s_project_name,
                      is_billable : is_resource_billable,
                      st_rate : stRate,
                      'employee' : i_employee,
                      'employee_name' : s_employee_name,
                      'percentoftime' : i_percent_of_time,
                      'department_id' : i_department_id,
                      'department' : s_department,
                      'holidays' : holidayCountInMonth,
                      'workingdays' : workingDays,
                      'customer_id' : i_customer_id,
                      'customer' : s_customer,
                      'count' : i_count,
                      'currency' : i_currency,
                      'vertical_id' : i_vertical_id,
                      'vertical' : s_vertical,
                      'monthly_billing' : is_T_and_M_monthly,
                      'monthly_rate' : f_monthly_rate,
                      'end_date':endDate,
                      'start_date':startDate,
                      'parent_department' : s_parent_department,
                      'end_customer' : s_end_customer == '- None -' ? ''
                              : s_end_customer
                  };
            }
      }  else {
            a_resource_allocations = null;
      }

      return a_resource_allocations;
}
function calcBusinessDays(d_startDate, d_endDate) { // input given as Date

    // objects
    var startDate = new Date(d_startDate.getTime());
    var endDate = new Date(d_endDate.getTime());
    // Validate input
    if (endDate < startDate)
          return 0;

    // Calculate days between dates
    var millisecondsPerDay = 86400 * 1000; // Day in milliseconds
    startDate.setHours(0, 0, 0, 1); // Start just after midnight
    endDate.setHours(23, 59, 59, 999); // End just before midnight
    var diff = endDate - startDate; // Milliseconds between datetime objects
    var days = Math.ceil(diff / millisecondsPerDay);

    // Subtract two weekend days for every week in between
    var weeks = Math.floor(days / 7);
    var days = days - (weeks * 2);

    // Handle special cases
    var startDay = startDate.getDay();
    var endDay = endDate.getDay();

    // Remove weekend not previously removed.
    if (startDay - endDay > 1)
          days = days - 2;

    // Remove start day if span starts on Sunday but ends before Saturday
    if (startDay == 0 && endDay != 6)
          days = days - 1

          // Remove end day if span ends on Saturday but starts after Sunday
    if (endDay == 6 && startDay != 0)
          days = days - 1

    return days;

}
function get_holidays(start_date, end_date, employee, project, customer) {

    var project_holiday = nlapiLookupField('job', project,
            'custentityproject_holiday');
    var emp_subsidiary = nlapiLookupField('employee', employee, 'subsidiary');

    if (project_holiday == 1) {
          return get_company_holidays(start_date, end_date, emp_subsidiary);
    } else {
          return get_customer_holidays(start_date, end_date, emp_subsidiary,
                  customer);
    }
}
function get_holidays(start_date, end_date, employee, project, customer) {

    var project_holiday = nlapiLookupField('job', project,
            'custentityproject_holiday');
    var emp_subsidiary = nlapiLookupField('employee', employee, 'subsidiary');

    if (project_holiday == 1) {
          return get_company_holidays(start_date, end_date, emp_subsidiary);
    } else {
          return get_customer_holidays(start_date, end_date, emp_subsidiary,
                  customer);
    }
}

function get_company_holidays(start_date, end_date, subsidiary) {
    var holiday_list = [];

    var search_company_holiday = nlapiSearchRecord('customrecord_holiday',
            'customsearch_company_holiday_search', [
                    new nlobjSearchFilter('custrecord_date', null, 'within',
                            start_date, end_date),
                    new nlobjSearchFilter('custrecordsubsidiary', null,
                            'anyof', subsidiary) ], [ new nlobjSearchColumn(
                    'custrecord_date') ]);

    if (search_company_holiday) {

          for (var i = 0; i < search_company_holiday.length; i++) {
                holiday_list.push(search_company_holiday[i]
                        .getValue('custrecord_date'));
          }
    }

    return holiday_list;
}
function get_customer_holidays(start_date, end_date, subsidiary, customer) {
    var holiday_list = [];

    //start_date = nlapiDateToString(start_date, 'date');
    //end_date = nlapiDateToString(end_date, 'date');

    nlapiLogExecution('debug', 'start_date', start_date);
    nlapiLogExecution('debug', 'end_date', end_date);
    nlapiLogExecution('debug', 'subsidiary', subsidiary);
    nlapiLogExecution('debug', 'customer', customer);

    var search_customer_holiday = nlapiSearchRecord(
            'customrecordcustomerholiday', 'customsearch_customer_holiday', [
                    new nlobjSearchFilter('custrecordholidaydate', null,
                            'within', start_date, end_date),
                    new nlobjSearchFilter('custrecordcustomersubsidiary', null,
                            'anyof', subsidiary),
                    new nlobjSearchFilter('custrecord13', null, 'anyof',
                            customer) ], [ new nlobjSearchColumn(
                    'custrecordholidaydate', null, 'group') ]);

    if (search_customer_holiday) {

          for (var i = 0; i < search_customer_holiday.length; i++) {
                holiday_list.push(search_customer_holiday[i].getValue(
                        'custrecordholidaydate', null, 'group'));
          }
    }

    return holiday_list;
}

function getWorkingDays(startDate, endDate) {
    try {
          var d_startDate = nlapiStringToDate(startDate);
          var d_endDate = nlapiStringToDate(endDate);
          var numberOfWorkingDays = 0;

          for (var i = 0;; i++) {
                var currentDate = nlapiAddDays(d_startDate, i);

                if (currentDate > d_endDate) {
                      break;
                }

                if (currentDate.getDay() != 0 && currentDate.getDay() != 6) {
                      numberOfWorkingDays += 1;
                }
          }

          return numberOfWorkingDays;
    } catch (err) {
          nlapiLogExecution('error', 'getWorkingDays', err);
          throw err;
    }
}

function down_excel_function_fm(bill_data_arr,email){
	
}
//mile function down_excel_function(bill_data_arr,email)//milestone
//mile function down_excel_function(bill_data_arr,email)//milestone
function down_excel_function(bill_data_arr,email)

{
	try {
		

		var strVar2 = '';
		/*strVar2 += "<table width=\"100%\">";
		strVar2 += '<table width="100%" border="1">';*/
		var current_date = new Date();
		var i_year = current_date.getFullYear();
		var excel_mnths = MonthNames(i_year);
//convertion to usd
		 var inr_to_usd = 0;
         var gbp_to_usd = 0;
         var eur_to_usd = 0;
         var a_conversion_rate_table = nlapiSearchRecord('customrecord_conversion_rate_sfdc',null,null,[new nlobjSearchColumn('custrecord_rate_inr_to_usd'),
                                                                       new nlobjSearchColumn('custrecord_rate_gbp_to_usd'),
                                                                       new nlobjSearchColumn('custrecord_rate_eur_to_usd'),
                                                                       new nlobjSearchColumn('internalid').setSort(true)]);
         if(a_conversion_rate_table)
               inr_to_usd = 1 / parseFloat(a_conversion_rate_table[0].getValue('custrecord_rate_inr_to_usd'));
               gbp_to_usd = a_conversion_rate_table[0].getValue('custrecord_rate_gbp_to_usd');
               eur_to_usd = a_conversion_rate_table[0].getValue('custrecord_rate_eur_to_usd');
		
			
		
		
//		
		if (bill_data_arr) {

			strVar2 += "<tr>";
			for (var bill_data_arr_len = 0; bill_data_arr_len < bill_data_arr.length; bill_data_arr_len++) {
				var bill_arr = bill_data_arr[bill_data_arr_len].excel_data;
				
				for (var jk = 0; jk < bill_arr.length; jk++) {
                var currncy ='USD';
					var act = bill_data_arr[bill_data_arr_len].excel_data[jk].actual_rev;
					strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \>"
							+ bill_data_arr[bill_data_arr_len].excel_data[jk].proj
							+ "</td>";
					strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \>"
							+ bill_data_arr[bill_data_arr_len].excel_data[jk].project_id
							+ "</td>";
					strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \>"
							+ currncy
							+ "</td>";
					strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \>"
							+ bill_data_arr[bill_data_arr_len].excel_data[jk].project_name
							+ "</td>";
					strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \>"
							+ bill_data_arr[bill_data_arr_len].excel_data[jk].practice
							+ "</td>";
					strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \>"
						+ bill_data_arr[bill_data_arr_len].excel_data[jk].type
						+ "</td>";
					strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \>"
						+ bill_data_arr[bill_data_arr_len].excel_data[jk].customer
						+ "</td>";
					var sum =0;
					
					//strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \></td>";
					for (var act_len = 0; act_len < act.length; act_len++) {
						if (act_len == 0) {
							/*strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \>"
									+ bill_data_arr[bill_data_arr_len].excel_data[jk].actual_rev[act_len]
									+ "</td>";*/
							var status= 'Revenue to be reconized/month';
							strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \>"
								+status
								+ "</td>";
							
						}
						if(act_len != act.length-1 && act_len != 0){
							var i_master_currency =1; 
							if(bill_data_arr[bill_data_arr_len].excel_data[jk].currency =='USD')
		                         i_master_currency = 1;
		                   if(bill_data_arr[bill_data_arr_len].excel_data[jk].currency =='INR')
		                         i_master_currency = inr_to_usd;
		                   if(bill_data_arr[bill_data_arr_len].excel_data[jk].currency =='EUR')
		                         i_master_currency = eur_to_usd;
		                   if(bill_data_arr[bill_data_arr_len].excel_data[jk].currency =='GBP')
		                         i_master_currency = gbp_to_usd;
							
					        var str2 = bill_data_arr[bill_data_arr_len].excel_data[jk].actual_rev[act_len];
					        str2 =	str2.replace( /,/g, "" );
							sum  = sum + parseInt(str2)*i_master_currency;
							
						}
						
						
						var mn_len = act_len - 1;
						var mn = bill_data_arr[bill_data_arr_len].excel_data[jk].st_date[mn_len];
						var st_d = bill_data_arr[bill_data_arr_len].excel_data[jk].st_date;
						var valdt = bill_data_arr[bill_data_arr_len].excel_data[jk].st_date;
						var len = bill_data_arr[bill_data_arr_len].excel_data[jk].actual_rev
						valdt = valdt.slice(0, len.length - 2);
						mn = valdt[mn_len ];
						var mon = excel_mnths.indexOf(mn);
						
						
						if (mon >= 0) {
							if ((mon > 0) && (act_len == 1)) {
								for (var prev_mn = 0; prev_mn < mon; prev_mn++) {
									strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \></td>";
								}
							}
							if (mn == excel_mnths[mon])
								var i_master_currency =1; 
								if(bill_data_arr[bill_data_arr_len].excel_data[jk].currency =='USD')
			                         i_master_currency = 1;
			                   if(bill_data_arr[bill_data_arr_len].excel_data[jk].currency =='INR')
			                         i_master_currency = inr_to_usd;
			                   if(bill_data_arr[bill_data_arr_len].excel_data[jk].currency =='EUR')
			                         i_master_currency = eur_to_usd;
			                   if(bill_data_arr[bill_data_arr_len].excel_data[jk].currency =='GBP')
			                         i_master_currency = gbp_to_usd;
			                   var prs = parseInt(bill_data_arr[bill_data_arr_len].excel_data[jk].actual_rev[act_len]);
			                   var prs = bill_data_arr[bill_data_arr_len].excel_data[jk].actual_rev[act_len];
			                   prs =	prs.replace( /,/g, "" );
			                   var cnvtd = parseInt(prs)*i_master_currency;
						        
								strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \>"
										+ cnvtd
										+ "</td>";

						}
						
						if (act_len == act.length - 1) {
							var mn_len = act_len - 2;
							var mn = bill_data_arr[bill_data_arr_len].excel_data[jk].st_date[mn_len];
							var mon = excel_mnths.indexOf(mn);
							for (var mnth_indx = 1; mnth_indx < excel_mnths.length- mon; mnth_indx++) {
								strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \></td>";
							}
							
							/*strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \>"
									+ bill_data_arr[bill_data_arr_len].excel_data[jk].actual_rev[act_len]
									+ "</td>";*/
							strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \>"
								+ sum
								+ "</td>";
						}
						

					}
					strVar2 += "<\/tr>";

				}
				//strVar2+="<tr></tr>";
			}
		} else {

		}
		//////////strVar2 += "<\/table>";
		milestone_xml = strVar2;
		
	
	 
	} catch (err) {
		nlapiLogExecution('ERROR', 'ERROR MESSAGE:- ', err);
	}


	
}


//function down_excel_function(bill_data_arr,email) ends
function getMargin(i_projectId,i_year_project,s_currency_symbol_proj,d_proj_start_date,d_proj_end_date,i_project_mnth,s_currency_symbol_usd,i_prject_end_mnth,i_year_project_end)
{
	var sr_no=0;
	var bill_data_arr = new Array();
	nlapiYieldScript();
	try
	{
	var a_revenue_cap_filters = [['custrecord_revenue_share_project', 'anyof', parseInt(i_projectId)]];
					
				var a_column = new Array();
				a_column[0] = new nlobjSearchColumn('created').setSort(true);
				a_column[1] = new nlobjSearchColumn('custrecord_auto_number_counter');
				a_column[2] = new nlobjSearchColumn('custrecord_revenue_share_approval_status');
				
				var a_get_logged_in_user_exsiting_revenue_cap = nlapiSearchRecord('customrecord_revenue_share', null, a_revenue_cap_filters, a_column);
				if (a_get_logged_in_user_exsiting_revenue_cap)
				{
					var i_revenue_share_id = a_get_logged_in_user_exsiting_revenue_cap[0].getId();
					var i_revenue_share_stat = a_get_logged_in_user_exsiting_revenue_cap[0].getValue('custrecord_revenue_share_approval_status');
				}
	
			
		{
				var a_effort_activity_mnth_end_filter = [['custrecord_revenue_share_parent_json', 'anyof', parseInt(i_revenue_share_id)]];
			
					var a_columns_mnth_end_effort_activity_srch = new Array();
					a_columns_mnth_end_effort_activity_srch[0] = new nlobjSearchColumn('created').setSort(true);
					
					var a_get_mnth_end_effrt_activity = nlapiSearchRecord('customrecord_fp_rev_rec_mnth_end_json', null, a_effort_activity_mnth_end_filter, a_columns_mnth_end_effort_activity_srch);
					if (a_get_mnth_end_effrt_activity)
					{
						var i_mnth_end_json_id = a_get_mnth_end_effrt_activity[0].getId();
					}			
					
				var a_recipient_mail_id = new Array();
				var a_practice_involved = new Array();
				var a_revenue_cap_filter = [['custrecord_revenue_share_per_practice_ca.internalid', 'anyof', parseInt(i_revenue_share_id)]];
				var a_columns_existing_cap_srch = new Array();
				a_columns_existing_cap_srch[0] = new nlobjSearchColumn('custrecord_revenue_share_per_practice_pr');
				a_columns_existing_cap_srch[1] = new nlobjSearchColumn('custrecord_revenue_share_per_practice_su');
				var a_exsiting_revenue_cap = nlapiSearchRecord('customrecord_revenue_share_per_practice', null, a_revenue_cap_filter, a_columns_existing_cap_srch);
				if (a_exsiting_revenue_cap)
				{
					for(var i_revenue_index = 0; i_revenue_index < a_exsiting_revenue_cap.length; i_revenue_index++)
					{
						var i_parent_parctice_id = a_exsiting_revenue_cap[i_revenue_index].getValue(	'custrecord_revenue_share_per_practice_pr');
						var i_sub_parctice_id = a_exsiting_revenue_cap[i_revenue_index].getValue('custrecord_revenue_share_per_practice_su');
					
						if(a_practice_involved.indexOf(i_parent_parctice_id) < 0)
						{
							a_practice_involved.push(i_parent_parctice_id);
						}
						if(a_practice_involved.indexOf(i_sub_parctice_id) < 0)
						{
							a_practice_involved.push(i_sub_parctice_id);
						}
					}
				}	
			
					var a_filters_search_practice = [['internalid','anyof',a_practice_involved]
									  ];						  
					var a_columns_practice = new Array();
					a_columns_practice[0] = new nlobjSearchColumn('email', 'custrecord_practicehead');
					var a_parctice_srch = nlapiSearchRecord('department', null, a_filters_search_practice, a_columns_practice);
					if (a_parctice_srch)
					{
						for(var i_practice_index = 0; i_practice_index < a_parctice_srch.length; i_practice_index++)
						{
						a_recipient_mail_id.push(a_parctice_srch[i_practice_index].getValue('email', 'custrecord_practicehead'));
						}
					}			
		}			
		var projectWiseRevenue = [];
		var a_revenue_recognized_for_project = new Array();
		var a_filter_get_ytd_revenue_recognized = [['custrecord_project_to_recognize_amount', 'anyof', i_projectId]
													];
							
		var a_columns_get_ytd_revenue_recognized = new Array();
		a_columns_get_ytd_revenue_recognized[0] = new nlobjSearchColumn('custrecord_project_to_recognize_amount');
		a_columns_get_ytd_revenue_recognized[1] = new nlobjSearchColumn('custrecord_practice_to_recognize_amount');
		a_columns_get_ytd_revenue_recognized[2] = new nlobjSearchColumn('custrecord_subparctice_to_recognize_amnt');
		a_columns_get_ytd_revenue_recognized[3] = new nlobjSearchColumn('custrecord_role_to_recognize_amount');
		a_columns_get_ytd_revenue_recognized[4] = new nlobjSearchColumn('custrecord_level_to_recognize_amount');
		a_columns_get_ytd_revenue_recognized[5] = new nlobjSearchColumn('custrecord_revenue_recognized');
		a_columns_get_ytd_revenue_recognized[6] = new nlobjSearchColumn('custrecord_month_to_recognize_amount');
		a_columns_get_ytd_revenue_recognized[7] = new nlobjSearchColumn('custrecord_year_to_recognize_amount');
		
		var a_get_ytd_revenue = nlapiSearchRecord('customrecord_fp_rev_rec_recognized_amnt', null, a_filter_get_ytd_revenue_recognized, a_columns_get_ytd_revenue_recognized);
		if (a_get_ytd_revenue)
		{
			//nlapiLogExecution('audit','recognized revenue for project found');
			for(var i_revenue_index=0; i_revenue_index<a_get_ytd_revenue.length; i_revenue_index++)
			{
				//nlapiLogExecution('audit','recognized amnt:- ',a_get_ytd_revenue[i_revenue_index].getValue('custrecord_month_to_recognize_amount'));
				a_revenue_recognized_for_project.push({
												'practice_revenue_recognized': a_get_ytd_revenue[i_revenue_index].getValue('custrecord_practice_to_recognize_amount'),
												'subpractice_revenue_recognized': a_get_ytd_revenue[i_revenue_index].getValue('custrecord_subparctice_to_recognize_amnt'),
												'role_revenue_recognized': a_get_ytd_revenue[i_revenue_index].getValue('custrecord_role_to_recognize_amount'),
												'level_revenue_recognized': a_get_ytd_revenue[i_revenue_index].getValue('custrecord_level_to_recognize_amount'),
												'amount_revenue_recognized': a_get_ytd_revenue[i_revenue_index].getValue('custrecord_revenue_recognized'),
												'month_revenue_recognized': a_get_ytd_revenue[i_revenue_index].getValue('custrecord_month_to_recognize_amount'),
												'year_revenue_recognized': a_get_ytd_revenue[i_revenue_index].getValue('custrecord_year_to_recognize_amount')
												});
			}
		}
		
		
	var d_today_date = new Date();
							var i_current_mnth = d_today_date.getMonth();
							var i_current_year = d_today_date.getFullYear();
							var i_prev_month = parseFloat(i_current_mnth) - parseFloat(1);
	
		
		var i_prev_month = parseFloat(i_current_mnth) - parseFloat(1);
		
		var a_filter_get_prev_mnth_effrt = [['custrecord_revenue_share_parent_json', 'anyof', parseInt(i_revenue_share_id)], 'and',
											['custrecord_current_mnth_no', 'equalto', parseInt(i_prev_month)]];
													
		var a_columns_get_prev_mnth_effrt = new Array();
		a_columns_get_prev_mnth_effrt[0] = new nlobjSearchColumn('created').setSort(true);
			
		var a_get_prev_month_effrt = nlapiSearchRecord('customrecord_fp_rev_rec_mnth_end_json', null, a_filter_get_prev_mnth_effrt, a_columns_get_prev_mnth_effrt);
		if (a_get_prev_month_effrt)
		{
			//nlapiLogExecution('debug','prev mnth effrt json id:- '+a_get_prev_month_effrt[0].getId());
			
			var projectWiseRevenue_previous_effrt = [];
			
			var a_prev_subprac_searched_once = new Array();
			
			var o_mnth_end_effrt_prev_mnth = nlapiLoadRecord('customrecord_fp_rev_rec_mnth_end_json', a_get_prev_month_effrt[0].getId());
			
			var s_effrt_json_1_prev_mnth = o_mnth_end_effrt_prev_mnth.getFieldValue('custrecord_json_mnth_end_effrt');
			if(s_effrt_json_1_prev_mnth)
			{
				//generate_previous_effrt_revenue(s_effrt_json_1_prev_mnth,projectWiseRevenue_previous_effrt,a_revenue_recognized_for_project,i_year_project);
				
				{
					var i_practice_previous = 0;
					var f_total_revenue_for_tenure = 0;
					//for (var i_mnth_end_plan = 0; i_mnth_end_plan < a_get_mnth_end_effrt_plan.length; i_mnth_end_plan++)
					var s_entire_json_clubed = JSON.parse(s_effrt_json_1_prev_mnth);
					for (var i_mnth_end_plan = 0; i_mnth_end_plan < s_entire_json_clubed.length; i_mnth_end_plan++)
					{
						var a_row_json_data = JSON.parse(JSON.stringify(s_entire_json_clubed[i_mnth_end_plan]));
						for (var i_row_json_index = 0; i_row_json_index < a_row_json_data.length; i_row_json_index++)
						{
							var i_practice = a_row_json_data[i_row_json_index].prac;
							var s_practice = a_row_json_data[i_row_json_index].prac_text;
							var i_sub_practice = a_row_json_data[i_row_json_index].subprac;
							var s_sub_practice = a_row_json_data[i_row_json_index].subprac_text;
							var i_role = a_row_json_data[i_row_json_index].role;
							var s_role = a_row_json_data[i_row_json_index].role_text;
							var i_level = a_row_json_data[i_row_json_index].level;
							var s_level = a_row_json_data[i_row_json_index].level_text;
							var i_location = a_row_json_data[i_row_json_index].loc;
							var s_location = a_row_json_data[i_row_json_index].loc_text;
							var f_revenue = a_row_json_data[i_row_json_index].cost;
							if (!f_revenue) 
								f_revenue = 0;
							
							var f_revenue_share = a_row_json_data[i_row_json_index].share;
							if (!f_revenue_share) 
								f_revenue_share = 0;
								
							var i_no_of_resources = a_row_json_data[i_row_json_index].allo;
							var s_mnth_strt_date = '1/31/2017';
							var s_mnth_end_date = '31/31/2017';
							var s_mnth = a_row_json_data[i_row_json_index].mnth;
							var s_year = a_row_json_data[i_row_json_index].year;
							
							var f_revenue_recognized = find_recognized_revenue_prev(a_revenue_recognized_for_project,i_practice,i_sub_practice,i_role,i_level,s_mnth,s_year,a_prev_subprac_searched_once);
							
							if (!f_revenue_recognized) 
								f_revenue_recognized = 0;
							
							//nlapiLogExecution('audit','f_revenue_recognized:- '+f_revenue_recognized);
							
							var s_month_name = s_mnth + '_' + s_year;
							
							var total_revenue = parseFloat(i_no_of_resources) * parseFloat(f_revenue);
							total_revenue = parseFloat(total_revenue).toFixed(2);
							
							if (i_practice_previous == 0) {
								i_practice_previous = a_row_json_data[i_row_json_index].prac_row;
							}
							
							if (i_practice_previous != a_row_json_data[i_row_json_index].prac_row) {
								f_total_revenue_for_tenure = 0;
								i_practice_previous = a_row_json_data[i_row_json_index].prac_row;
							}
						
							f_total_revenue_for_tenure = parseFloat(f_total_revenue_for_tenure) + parseFloat(total_revenue);
							f_total_revenue_for_tenure = parseFloat(f_total_revenue_for_tenure).toFixed(2);
							
							var i_prac_subPrac_role_level = i_practice + '_' + i_sub_practice + '_' + i_role + '_' + i_level + '_' + i_location;
							
							if (!projectWiseRevenue_previous_effrt[i_prac_subPrac_role_level]) {
								projectWiseRevenue_previous_effrt[i_prac_subPrac_role_level] = {
									practice_name: s_practice,
									sub_prac_name: s_sub_practice,
									sub_practice: i_sub_practice,
									role_name: s_role,
									level_name: s_level,
									location_name: s_location,
									total_revenue_for_tenure: f_total_revenue_for_tenure,
									revenue_share: f_revenue_share,
									RevenueData: {}
								};
								
								projectWiseRevenue_previous_effrt[i_prac_subPrac_role_level].RevenueData[i_practice] = new yearData(i_year_project);
								
								projectWiseRevenue_previous_effrt[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].Amount = total_revenue;
								projectWiseRevenue_previous_effrt[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].prcnt_allocated = i_no_of_resources;
								projectWiseRevenue_previous_effrt[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].recognized_revenue = f_revenue_recognized;
								projectWiseRevenue_previous_effrt[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].mnth_strt = s_mnth_strt_date;
								projectWiseRevenue_previous_effrt[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].mnth_end = s_mnth_end_date;
							}
							else {
								projectWiseRevenue_previous_effrt[i_prac_subPrac_role_level].total_revenue_for_tenure = f_total_revenue_for_tenure;
								
								projectWiseRevenue_previous_effrt[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].Amount = total_revenue;
								projectWiseRevenue_previous_effrt[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].prcnt_allocated = i_no_of_resources;
								projectWiseRevenue_previous_effrt[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].recognized_revenue = f_revenue_recognized;
								projectWiseRevenue_previous_effrt[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].mnth_strt = s_mnth_strt_date;
								projectWiseRevenue_previous_effrt[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].mnth_end = s_mnth_end_date;
							}
						}
					}
				}
			}
			
			var s_effrt_json_2_prev_mnth = o_mnth_end_effrt_prev_mnth.getFieldValue('custrecord_json_mnth_end_effrt2');
			if(s_effrt_json_2_prev_mnth)
			{
				//generate_previous_effrt_revenue(s_effrt_json_2_prev_mnth,projectWiseRevenue_previous_effrt,a_revenue_recognized_for_project,i_year_project);
			
				{
					var i_practice_previous = 0;
					var f_total_revenue_for_tenure = 0;
					//for (var i_mnth_end_plan = 0; i_mnth_end_plan < a_get_mnth_end_effrt_plan.length; i_mnth_end_plan++)
					var s_entire_json_clubed = JSON.parse(s_effrt_json_2_prev_mnth);
					for (var i_mnth_end_plan = 0; i_mnth_end_plan < s_entire_json_clubed.length; i_mnth_end_plan++)
					{
						var a_row_json_data = JSON.parse(JSON.stringify(s_entire_json_clubed[i_mnth_end_plan]));
						for (var i_row_json_index = 0; i_row_json_index < a_row_json_data.length; i_row_json_index++)
						{
							var i_practice = a_row_json_data[i_row_json_index].prac;
							var s_practice = a_row_json_data[i_row_json_index].prac_text;
							var i_sub_practice = a_row_json_data[i_row_json_index].subprac;
							var s_sub_practice = a_row_json_data[i_row_json_index].subprac_text;
							var i_role = a_row_json_data[i_row_json_index].role;
							var s_role = a_row_json_data[i_row_json_index].role_text;
							var i_level = a_row_json_data[i_row_json_index].level;
							var s_level = a_row_json_data[i_row_json_index].level_text;
							var i_location = a_row_json_data[i_row_json_index].loc;
							var s_location = a_row_json_data[i_row_json_index].loc_text;
							var f_revenue = a_row_json_data[i_row_json_index].cost;
							if (!f_revenue) 
								f_revenue = 0;
							
							var f_revenue_share = a_row_json_data[i_row_json_index].share;
							if (!f_revenue_share) 
								f_revenue_share = 0;
								
							var i_no_of_resources = a_row_json_data[i_row_json_index].allo;
							var s_mnth_strt_date = '1/31/2017';
							var s_mnth_end_date = '31/31/2017';
							var s_mnth = a_row_json_data[i_row_json_index].mnth;
							var s_year = a_row_json_data[i_row_json_index].year;
							
							var f_revenue_recognized = find_recognized_revenue_prev(a_revenue_recognized_for_project,i_practice,i_sub_practice,i_role,i_level,s_mnth,s_year,a_prev_subprac_searched_once);
							
							if (!f_revenue_recognized) 
								f_revenue_recognized = 0;
								
							//nlapiLogExecution('audit','f_revenue_recognized:- '+f_revenue_recognized);
							
							var s_month_name = s_mnth + '_' + s_year;
							
							var total_revenue = parseFloat(i_no_of_resources) * parseFloat(f_revenue);
							total_revenue = parseFloat(total_revenue).toFixed(2);
							
							if (i_practice_previous == 0) {
								i_practice_previous = a_row_json_data[i_row_json_index].prac_row;
							}
							
							if (i_practice_previous != a_row_json_data[i_row_json_index].prac_row) {
								f_total_revenue_for_tenure = 0;
								i_practice_previous = a_row_json_data[i_row_json_index].prac_row;
							}
						
							f_total_revenue_for_tenure = parseFloat(f_total_revenue_for_tenure) + parseFloat(total_revenue);
							f_total_revenue_for_tenure = parseFloat(f_total_revenue_for_tenure).toFixed(2);
							
							var i_prac_subPrac_role_level = i_practice + '_' + i_sub_practice + '_' + i_role + '_' + i_level + '_' + i_location;
							
							if (!projectWiseRevenue_previous_effrt[i_prac_subPrac_role_level]) {
								projectWiseRevenue_previous_effrt[i_prac_subPrac_role_level] = {
									practice_name: s_practice,
									sub_prac_name: s_sub_practice,
									sub_practice: i_sub_practice,
									role_name: s_role,
									level_name: s_level,
									location_name: s_location,
									total_revenue_for_tenure: f_total_revenue_for_tenure,
									revenue_share: f_revenue_share,
									RevenueData: {}
								};
								
								projectWiseRevenue_previous_effrt[i_prac_subPrac_role_level].RevenueData[i_practice] = new yearData(i_year_project);
								
								projectWiseRevenue_previous_effrt[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].Amount = total_revenue;
								projectWiseRevenue_previous_effrt[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].prcnt_allocated = i_no_of_resources;
								projectWiseRevenue_previous_effrt[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].recognized_revenue = f_revenue_recognized;
								projectWiseRevenue_previous_effrt[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].mnth_strt = s_mnth_strt_date;
								projectWiseRevenue_previous_effrt[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].mnth_end = s_mnth_end_date;
							}
							else {
								projectWiseRevenue_previous_effrt[i_prac_subPrac_role_level].total_revenue_for_tenure = f_total_revenue_for_tenure;
								
								projectWiseRevenue_previous_effrt[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].Amount = total_revenue;
								projectWiseRevenue_previous_effrt[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].prcnt_allocated = i_no_of_resources;
								projectWiseRevenue_previous_effrt[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].recognized_revenue = f_revenue_recognized;
								projectWiseRevenue_previous_effrt[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].mnth_strt = s_mnth_strt_date;
								projectWiseRevenue_previous_effrt[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].mnth_end = s_mnth_end_date;
							}
						}
					}
				}
			}
			
			var s_effrt_json_3_prev_mnth = o_mnth_end_effrt_prev_mnth.getFieldValue('custrecord_json_mnth_end_effrt3');
			if(s_effrt_json_3_prev_mnth)
			{
				//generate_previous_effrt_revenue(s_effrt_json_3_prev_mnth,projectWiseRevenue_previous_effrt,a_revenue_recognized_for_project,i_year_project);
			
				{
					var i_practice_previous = 0;
					var f_total_revenue_for_tenure = 0;
					//for (var i_mnth_end_plan = 0; i_mnth_end_plan < a_get_mnth_end_effrt_plan.length; i_mnth_end_plan++)
					var s_entire_json_clubed = JSON.parse(s_effrt_json_3_prev_mnth);
					for (var i_mnth_end_plan = 0; i_mnth_end_plan < s_entire_json_clubed.length; i_mnth_end_plan++)
					{
						var a_row_json_data = JSON.parse(JSON.stringify(s_entire_json_clubed[i_mnth_end_plan]));
						for (var i_row_json_index = 0; i_row_json_index < a_row_json_data.length; i_row_json_index++)
						{
							var i_practice = a_row_json_data[i_row_json_index].prac;
							var s_practice = a_row_json_data[i_row_json_index].prac_text;
							var i_sub_practice = a_row_json_data[i_row_json_index].subprac;
							var s_sub_practice = a_row_json_data[i_row_json_index].subprac_text;
							var i_role = a_row_json_data[i_row_json_index].role;
							var s_role = a_row_json_data[i_row_json_index].role_text;
							var i_level = a_row_json_data[i_row_json_index].level;
							var s_level = a_row_json_data[i_row_json_index].level_text;
							var i_location = a_row_json_data[i_row_json_index].loc;
							var s_location = a_row_json_data[i_row_json_index].loc_text;
							var f_revenue = a_row_json_data[i_row_json_index].cost;
							if (!f_revenue) 
								f_revenue = 0;
							
							var f_revenue_share = a_row_json_data[i_row_json_index].share;
							if (!f_revenue_share) 
								f_revenue_share = 0;
								
							var i_no_of_resources = a_row_json_data[i_row_json_index].allo;
							var s_mnth_strt_date = '1/31/2017';
							var s_mnth_end_date = '31/31/2017';
							var s_mnth = a_row_json_data[i_row_json_index].mnth;
							var s_year = a_row_json_data[i_row_json_index].year;
							
							var f_revenue_recognized = find_recognized_revenue_prev(a_revenue_recognized_for_project,i_practice,i_sub_practice,i_role,i_level,s_mnth,s_year,a_prev_subprac_searched_once);
							
							if (!f_revenue_recognized) 
								f_revenue_recognized = 0;
							
							//nlapiLogExecution('audit','f_revenue_recognized:- '+f_revenue_recognized);
							
							var s_month_name = s_mnth + '_' + s_year;
							
							var total_revenue = parseFloat(i_no_of_resources) * parseFloat(f_revenue);
							total_revenue = parseFloat(total_revenue).toFixed(2);
							
							if (i_practice_previous == 0) {
								i_practice_previous = a_row_json_data[i_row_json_index].prac_row;
							}
							
							if (i_practice_previous != a_row_json_data[i_row_json_index].prac_row) {
								f_total_revenue_for_tenure = 0;
								i_practice_previous = a_row_json_data[i_row_json_index].prac_row;
							}
						
							f_total_revenue_for_tenure = parseFloat(f_total_revenue_for_tenure) + parseFloat(total_revenue);
							f_total_revenue_for_tenure = parseFloat(f_total_revenue_for_tenure).toFixed(2);
							
							var i_prac_subPrac_role_level = i_practice + '_' + i_sub_practice + '_' + i_role + '_' + i_level + '_' + i_location;
							
							if (!projectWiseRevenue_previous_effrt[i_prac_subPrac_role_level]) {
								projectWiseRevenue_previous_effrt[i_prac_subPrac_role_level] = {
									practice_name: s_practice,
									sub_prac_name: s_sub_practice,
									sub_practice: i_sub_practice,
									role_name: s_role,
									level_name: s_level,
									location_name: s_location,
									total_revenue_for_tenure: f_total_revenue_for_tenure,
									revenue_share: f_revenue_share,
									RevenueData: {}
								};
								
								projectWiseRevenue_previous_effrt[i_prac_subPrac_role_level].RevenueData[i_practice] = new yearData(i_year_project);
								
								projectWiseRevenue_previous_effrt[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].Amount = total_revenue;
								projectWiseRevenue_previous_effrt[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].prcnt_allocated = i_no_of_resources;
								projectWiseRevenue_previous_effrt[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].recognized_revenue = f_revenue_recognized;
								projectWiseRevenue_previous_effrt[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].mnth_strt = s_mnth_strt_date;
								projectWiseRevenue_previous_effrt[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].mnth_end = s_mnth_end_date;
							}
							else {
								projectWiseRevenue_previous_effrt[i_prac_subPrac_role_level].total_revenue_for_tenure = f_total_revenue_for_tenure;
								
								projectWiseRevenue_previous_effrt[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].Amount = total_revenue;
								projectWiseRevenue_previous_effrt[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].prcnt_allocated = i_no_of_resources;
								projectWiseRevenue_previous_effrt[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].recognized_revenue = f_revenue_recognized;
								projectWiseRevenue_previous_effrt[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].mnth_strt = s_mnth_strt_date;
								projectWiseRevenue_previous_effrt[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].mnth_end = s_mnth_end_date;
							}
						}
					}
				}
			}
			
			var s_effrt_json_4_prev_mnth = o_mnth_end_effrt_prev_mnth.getFieldValue('custrecord_json_mnth_end_effrt4');
			if(s_effrt_json_4_prev_mnth)
			{
				//generate_previous_effrt_revenue(s_effrt_json_4_prev_mnth,projectWiseRevenue_previous_effrt,a_revenue_recognized_for_project,i_year_project);
			
				{
					var i_practice_previous = 0;
					var f_total_revenue_for_tenure = 0;
					//for (var i_mnth_end_plan = 0; i_mnth_end_plan < a_get_mnth_end_effrt_plan.length; i_mnth_end_plan++)
					var s_entire_json_clubed = JSON.parse(s_effrt_json_4_prev_mnth);
					for (var i_mnth_end_plan = 0; i_mnth_end_plan < s_entire_json_clubed.length; i_mnth_end_plan++)
					{
						var a_row_json_data = JSON.parse(JSON.stringify(s_entire_json_clubed[i_mnth_end_plan]));
						for (var i_row_json_index = 0; i_row_json_index < a_row_json_data.length; i_row_json_index++)
						{
							var i_practice = a_row_json_data[i_row_json_index].prac;
							var s_practice = a_row_json_data[i_row_json_index].prac_text;
							var i_sub_practice = a_row_json_data[i_row_json_index].subprac;
							var s_sub_practice = a_row_json_data[i_row_json_index].subprac_text;
							var i_role = a_row_json_data[i_row_json_index].role;
							var s_role = a_row_json_data[i_row_json_index].role_text;
							var i_level = a_row_json_data[i_row_json_index].level;
							var s_level = a_row_json_data[i_row_json_index].level_text;
							var i_location = a_row_json_data[i_row_json_index].loc;
							var s_location = a_row_json_data[i_row_json_index].loc_text;
							var f_revenue = a_row_json_data[i_row_json_index].cost;
							if (!f_revenue) 
								f_revenue = 0;
							
							var f_revenue_share = a_row_json_data[i_row_json_index].share;
							if (!f_revenue_share) 
								f_revenue_share = 0;
								
							var i_no_of_resources = a_row_json_data[i_row_json_index].allo;
							var s_mnth_strt_date = '1/31/2017';
							var s_mnth_end_date = '31/31/2017';
							var s_mnth = a_row_json_data[i_row_json_index].mnth;
							var s_year = a_row_json_data[i_row_json_index].year;
							
							var f_revenue_recognized = find_recognized_revenue_prev(a_revenue_recognized_for_project,i_practice,i_sub_practice,i_role,i_level,s_mnth,s_year,a_prev_subprac_searched_once);
							
							if (!f_revenue_recognized) 
								f_revenue_recognized = 0;
							
							//nlapiLogExecution('audit','f_revenue_recognized:- '+f_revenue_recognized);
							
							var s_month_name = s_mnth + '_' + s_year;
							
							var total_revenue = parseFloat(i_no_of_resources) * parseFloat(f_revenue);
							total_revenue = parseFloat(total_revenue).toFixed(2);
							
							if (i_practice_previous == 0) {
								i_practice_previous = a_row_json_data[i_row_json_index].prac_row;
							}
							
							if (i_practice_previous != a_row_json_data[i_row_json_index].prac_row) {
								f_total_revenue_for_tenure = 0;
								i_practice_previous = a_row_json_data[i_row_json_index].prac_row;
							}
						
							f_total_revenue_for_tenure = parseFloat(f_total_revenue_for_tenure) + parseFloat(total_revenue);
							f_total_revenue_for_tenure = parseFloat(f_total_revenue_for_tenure).toFixed(2);
							
							var i_prac_subPrac_role_level = i_practice + '_' + i_sub_practice + '_' + i_role + '_' + i_level + '_' + i_location;
							
							if (!projectWiseRevenue_previous_effrt[i_prac_subPrac_role_level]) {
								projectWiseRevenue_previous_effrt[i_prac_subPrac_role_level] = {
									practice_name: s_practice,
									sub_prac_name: s_sub_practice,
									sub_practice: i_sub_practice,
									role_name: s_role,
									level_name: s_level,
									location_name: s_location,
									total_revenue_for_tenure: f_total_revenue_for_tenure,
									revenue_share: f_revenue_share,
									RevenueData: {}
								};
								
								projectWiseRevenue_previous_effrt[i_prac_subPrac_role_level].RevenueData[i_practice] = new yearData(i_year_project);
								
								projectWiseRevenue_previous_effrt[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].Amount = total_revenue;
								projectWiseRevenue_previous_effrt[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].prcnt_allocated = i_no_of_resources;
								projectWiseRevenue_previous_effrt[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].recognized_revenue = f_revenue_recognized;
								projectWiseRevenue_previous_effrt[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].mnth_strt = s_mnth_strt_date;
								projectWiseRevenue_previous_effrt[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].mnth_end = s_mnth_end_date;
							}
							else {
								projectWiseRevenue_previous_effrt[i_prac_subPrac_role_level].total_revenue_for_tenure = f_total_revenue_for_tenure;
								
								projectWiseRevenue_previous_effrt[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].Amount = total_revenue;
								projectWiseRevenue_previous_effrt[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].prcnt_allocated = i_no_of_resources;
								projectWiseRevenue_previous_effrt[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].recognized_revenue = f_revenue_recognized;
								projectWiseRevenue_previous_effrt[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].mnth_strt = s_mnth_strt_date;
								projectWiseRevenue_previous_effrt[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].mnth_end = s_mnth_end_date;
							}
						}
					}
				}
			}
		}
		
		
		{
			var a_total_effort_json = [];
			if(i_mnth_end_json_id){
			var o_mnth_end_effrt = nlapiLoadRecord('customrecord_fp_rev_rec_mnth_end_json',i_mnth_end_json_id);
			
			var s_effrt_json = o_mnth_end_effrt.getFieldValue('custrecord_json_mnth_end_effrt');
			//s_effrt_json = JSON.stringify(s_effrt_json);
			var s_effrt_json_2 = o_mnth_end_effrt.getFieldValue('custrecord_json_mnth_end_effrt2');
			//s_effrt_json_2 = JSON.stringify(s_effrt_json_2);
			var s_effrt_json_3 = o_mnth_end_effrt.getFieldValue('custrecord_json_mnth_end_effrt3');
			var s_effrt_json_4 = o_mnth_end_effrt.getFieldValue('custrecord_json_mnth_end_effrt4');
			
			var a_duplicate_sub_prac_count = new Array();
			var a_unique_list_practice_sublist = new Array();
			
			var a_subprac_searched_once = new Array();
			var a_subprac_searched_once_month = new Array();
			var a_subprac_searched_once_year = new Array();
			}
			if(s_effrt_json)
			{
				a_total_effort_json.push(s_effrt_json);
				
				var i_practice_previous = 0;
				var f_total_revenue_for_tenure = 0;
				//for (var i_mnth_end_plan = 0; i_mnth_end_plan < a_get_mnth_end_effrt_plan.length; i_mnth_end_plan++)
				var s_entire_json_clubed = JSON.parse(s_effrt_json);
				for (var i_mnth_end_plan = 0; i_mnth_end_plan < s_entire_json_clubed.length; i_mnth_end_plan++)
				{
					var a_row_json_data = JSON.parse(JSON.stringify(s_entire_json_clubed[i_mnth_end_plan]));
					for (var i_row_json_index = 0; i_row_json_index < a_row_json_data.length; i_row_json_index++)
					{
						var i_practice = a_row_json_data[i_row_json_index].prac;
						var s_practice = a_row_json_data[i_row_json_index].prac_text;
						var i_sub_practice = a_row_json_data[i_row_json_index].subprac;
						var s_sub_practice = a_row_json_data[i_row_json_index].subprac_text;
						var i_role = a_row_json_data[i_row_json_index].role;
						var s_role = a_row_json_data[i_row_json_index].role_text;
						var i_level = a_row_json_data[i_row_json_index].level;
						var s_level = a_row_json_data[i_row_json_index].level_text;
						var i_location = a_row_json_data[i_row_json_index].loc;
						var s_location = a_row_json_data[i_row_json_index].loc_text;
						var f_revenue = a_row_json_data[i_row_json_index].cost;
						if (!f_revenue) 
							f_revenue = 0;
						
						var f_revenue_share = a_row_json_data[i_row_json_index].share;
						if (!f_revenue_share) 
							f_revenue_share = 0;
							
						var i_no_of_resources = a_row_json_data[i_row_json_index].allo;
						var s_mnth_strt_date = '1/31/2017';
						var s_mnth_end_date = '31/31/2017';
						var s_mnth = a_row_json_data[i_row_json_index].mnth;
						var s_year = a_row_json_data[i_row_json_index].year;
						
						var f_revenue_recognized = 0;
						
						if(a_unique_list_practice_sublist.indexOf(a_row_json_data[i_row_json_index].subprac)<0)
						{
							a_unique_list_practice_sublist.push(a_row_json_data[i_row_json_index].subprac);
							a_duplicate_sub_prac_count.push({
														'sub_prac': a_row_json_data[i_row_json_index].subprac,
														'sub_prac_name': a_row_json_data[i_row_json_index].subprac_text,
														'revenue_share': 50000,
														'no_count': 0
														});
														
						}
						
						f_revenue_recognized = find_recognized_revenue(a_revenue_recognized_for_project,i_practice,i_sub_practice,i_role,i_level,s_mnth,s_year,a_subprac_searched_once,a_subprac_searched_once_month,a_subprac_searched_once_year);
						
						if (!f_revenue_recognized) 
							f_revenue_recognized = 0;
							
					
						
						var s_month_name = s_mnth + '_' + s_year;
						
						var total_revenue = parseFloat(i_no_of_resources) * parseFloat(f_revenue);
						total_revenue = parseFloat(total_revenue).toFixed(2);
						
						
						
						if (i_practice_previous == 0) {
							i_practice_previous = a_row_json_data[i_row_json_index].prac_row;
						}
						
						if (i_practice_previous != a_row_json_data[i_row_json_index].prac_row) {
							f_total_revenue_for_tenure = 0;
							i_practice_previous = a_row_json_data[i_row_json_index].prac_row;
						}
					
						f_total_revenue_for_tenure = parseFloat(f_total_revenue_for_tenure) + parseFloat(total_revenue);
						f_total_revenue_for_tenure = parseFloat(f_total_revenue_for_tenure).toFixed(2);
						
						var i_prac_subPrac_role_level = i_practice + '_' + i_sub_practice + '_' + i_role + '_' + i_level + '_' + i_location;
						
						if (!projectWiseRevenue[i_prac_subPrac_role_level]) {
							projectWiseRevenue[i_prac_subPrac_role_level] = {
								practice_name: s_practice,
								sub_prac_name: s_sub_practice,
								sub_practice: i_sub_practice,
								role_name: s_role,
								level_name: s_level,
								location_name: s_location,
								total_revenue_for_tenure: f_total_revenue_for_tenure,
								revenue_share: f_revenue_share,
								RevenueData: {}
							};
							
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice] = new yearData(i_year_project);
							
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].Amount = total_revenue;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].prcnt_allocated = i_no_of_resources;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].recognized_revenue = f_revenue_recognized;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].mnth_strt = s_mnth_strt_date;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].mnth_end = s_mnth_end_date;
						}
						else {
							projectWiseRevenue[i_prac_subPrac_role_level].total_revenue_for_tenure = f_total_revenue_for_tenure;
							
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].Amount = total_revenue;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].prcnt_allocated = i_no_of_resources;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].recognized_revenue = f_revenue_recognized;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].mnth_strt = s_mnth_strt_date;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].mnth_end = s_mnth_end_date;
						}
					}
				}
			}
			
			if(s_effrt_json_2)
			{
				var i_practice_previous = 0;
				var f_total_revenue_for_tenure = 0;
				//for (var i_mnth_end_plan = 0; i_mnth_end_plan < a_get_mnth_end_effrt_plan.length; i_mnth_end_plan++)
				var s_entire_json_clubed = JSON.parse(s_effrt_json_2);
				for (var i_mnth_end_plan = 0; i_mnth_end_plan < s_entire_json_clubed.length; i_mnth_end_plan++)
				{
					var a_row_json_data = JSON.parse(JSON.stringify(s_entire_json_clubed[i_mnth_end_plan]));
					for (var i_row_json_index = 0; i_row_json_index < a_row_json_data.length; i_row_json_index++)
					{
						var i_practice = a_row_json_data[i_row_json_index].prac;
						var s_practice = a_row_json_data[i_row_json_index].prac_text;
						var i_sub_practice = a_row_json_data[i_row_json_index].subprac;
						var s_sub_practice = a_row_json_data[i_row_json_index].subprac_text;
						var i_role = a_row_json_data[i_row_json_index].role;
						var s_role = a_row_json_data[i_row_json_index].role_text;
						var i_level = a_row_json_data[i_row_json_index].level;
						var s_level = a_row_json_data[i_row_json_index].level_text;
						var i_location = a_row_json_data[i_row_json_index].loc;
						var s_location = a_row_json_data[i_row_json_index].loc_text;
						var f_revenue = a_row_json_data[i_row_json_index].cost;
						if (!f_revenue) 
							f_revenue = 0;
						
						var f_revenue_share = a_row_json_data[i_row_json_index].share;
						if (!f_revenue_share) 
							f_revenue_share = 0;
							
						var i_no_of_resources = a_row_json_data[i_row_json_index].allo;
						var s_mnth_strt_date = '1/31/2017';
						var s_mnth_end_date = '31/31/2017';
						var s_mnth = a_row_json_data[i_row_json_index].mnth;
						var s_year = a_row_json_data[i_row_json_index].year;
						
						var f_revenue_recognized = 0;
						
						if(a_unique_list_practice_sublist.indexOf(a_row_json_data[i_row_json_index].subprac)<0)
						{
							a_unique_list_practice_sublist.push(a_row_json_data[i_row_json_index].subprac);
							a_duplicate_sub_prac_count.push({
														'sub_prac': a_row_json_data[i_row_json_index].subprac,
														'sub_prac_name': a_row_json_data[i_row_json_index].subprac_text,
														'revenue_share': 50000,
														'no_count': 0
														});
														
						}
						
						f_revenue_recognized = find_recognized_revenue(a_revenue_recognized_for_project,i_practice,i_sub_practice,i_role,i_level,s_mnth,s_year,a_subprac_searched_once,a_subprac_searched_once_month,a_subprac_searched_once_year);
						
						if (!f_revenue_recognized) 
							f_revenue_recognized = 0;
						
						
						
						var s_month_name = s_mnth + '_' + s_year;
						
						var total_revenue = parseFloat(i_no_of_resources) * parseFloat(f_revenue);
						total_revenue = parseFloat(total_revenue).toFixed(2);
						
						
						
						if (i_practice_previous == 0) {
							i_practice_previous = a_row_json_data[i_row_json_index].prac_row;
						}
						
						if (i_practice_previous != a_row_json_data[i_row_json_index].prac_row) {
							f_total_revenue_for_tenure = 0;
							i_practice_previous = a_row_json_data[i_row_json_index].prac_row;
						}
					
						f_total_revenue_for_tenure = parseFloat(f_total_revenue_for_tenure) + parseFloat(total_revenue);
						f_total_revenue_for_tenure = parseFloat(f_total_revenue_for_tenure).toFixed(2);
						
						var i_prac_subPrac_role_level = i_practice + '_' + i_sub_practice + '_' + i_role + '_' + i_level + '_' + i_location;
						
						if (!projectWiseRevenue[i_prac_subPrac_role_level]) {
							projectWiseRevenue[i_prac_subPrac_role_level] = {
								practice_name: s_practice,
								sub_prac_name: s_sub_practice,
								sub_practice: i_sub_practice,
								role_name: s_role,
								level_name: s_level,
								location_name: s_location,
								total_revenue_for_tenure: f_total_revenue_for_tenure,
								revenue_share: f_revenue_share,
								RevenueData: {}
							};
							
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice] = new yearData(i_year_project);
							
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].Amount = total_revenue;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].prcnt_allocated = i_no_of_resources;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].recognized_revenue = f_revenue_recognized;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].mnth_strt = s_mnth_strt_date;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].mnth_end = s_mnth_end_date;
						}
						else {
							projectWiseRevenue[i_prac_subPrac_role_level].total_revenue_for_tenure = f_total_revenue_for_tenure;
							
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].Amount = total_revenue;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].prcnt_allocated = i_no_of_resources;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].recognized_revenue = f_revenue_recognized;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].mnth_strt = s_mnth_strt_date;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].mnth_end = s_mnth_end_date;
						}
					}
				}
			}
			
			if(s_effrt_json_3)
			{
				var i_practice_previous = 0;
				var f_total_revenue_for_tenure = 0;
				//for (var i_mnth_end_plan = 0; i_mnth_end_plan < a_get_mnth_end_effrt_plan.length; i_mnth_end_plan++)
				var s_entire_json_clubed = JSON.parse(s_effrt_json_3);
				for (var i_mnth_end_plan = 0; i_mnth_end_plan < s_entire_json_clubed.length; i_mnth_end_plan++)
				{
					var a_row_json_data = JSON.parse(JSON.stringify(s_entire_json_clubed[i_mnth_end_plan]));
					for (var i_row_json_index = 0; i_row_json_index < a_row_json_data.length; i_row_json_index++)
					{
						var i_practice = a_row_json_data[i_row_json_index].prac;
						var s_practice = a_row_json_data[i_row_json_index].prac_text;
						var i_sub_practice = a_row_json_data[i_row_json_index].subprac;
						var s_sub_practice = a_row_json_data[i_row_json_index].subprac_text;
						var i_role = a_row_json_data[i_row_json_index].role;
						var s_role = a_row_json_data[i_row_json_index].role_text;
						var i_level = a_row_json_data[i_row_json_index].level;
						var s_level = a_row_json_data[i_row_json_index].level_text;
						var i_location = a_row_json_data[i_row_json_index].loc;
						var s_location = a_row_json_data[i_row_json_index].loc_text;
						var f_revenue = a_row_json_data[i_row_json_index].cost;
						if (!f_revenue) 
							f_revenue = 0;
						
						var f_revenue_share = a_row_json_data[i_row_json_index].share;
						if (!f_revenue_share) 
							f_revenue_share = 0;
							
						var i_no_of_resources = a_row_json_data[i_row_json_index].allo;
						var s_mnth_strt_date = '1/31/2017';
						var s_mnth_end_date = '31/31/2017';
						var s_mnth = a_row_json_data[i_row_json_index].mnth;
						var s_year = a_row_json_data[i_row_json_index].year;
						
						var f_revenue_recognized = 0;
						
						if(a_unique_list_practice_sublist.indexOf(a_row_json_data[i_row_json_index].subprac)<0)
						{
							a_unique_list_practice_sublist.push(a_row_json_data[i_row_json_index].subprac);
							a_duplicate_sub_prac_count.push({
														'sub_prac': a_row_json_data[i_row_json_index].subprac,
														'sub_prac_name': a_row_json_data[i_row_json_index].subprac_text,
														'revenue_share': 50000,
														'no_count': 0
														});
											
						}
						
						f_revenue_recognized = find_recognized_revenue(a_revenue_recognized_for_project,i_practice,i_sub_practice,i_role,i_level,s_mnth,s_year,a_subprac_searched_once,a_subprac_searched_once_month,a_subprac_searched_once_year);
						
						if (!f_revenue_recognized) 
							f_revenue_recognized = 0;
						
						
						
						var s_month_name = s_mnth + '_' + s_year;
						
						var total_revenue = parseFloat(i_no_of_resources) * parseFloat(f_revenue);
						total_revenue = parseFloat(total_revenue).toFixed(2);
						
						
						
						if (i_practice_previous == 0) {
							i_practice_previous = a_row_json_data[i_row_json_index].prac_row;
						}
						
						if (i_practice_previous != a_row_json_data[i_row_json_index].prac_row) {
							f_total_revenue_for_tenure = 0;
							i_practice_previous = a_row_json_data[i_row_json_index].prac_row;
						}
					
						f_total_revenue_for_tenure = parseFloat(f_total_revenue_for_tenure) + parseFloat(total_revenue);
						f_total_revenue_for_tenure = parseFloat(f_total_revenue_for_tenure).toFixed(2);
						
						var i_prac_subPrac_role_level = i_practice + '_' + i_sub_practice + '_' + i_role + '_' + i_level + '_' + i_location;
						
						if (!projectWiseRevenue[i_prac_subPrac_role_level]) {
							projectWiseRevenue[i_prac_subPrac_role_level] = {
								practice_name: s_practice,
								sub_prac_name: s_sub_practice,
								sub_practice: i_sub_practice,
								role_name: s_role,
								level_name: s_level,
								location_name: s_location,
								total_revenue_for_tenure: f_total_revenue_for_tenure,
								revenue_share: f_revenue_share,
								RevenueData: {}
							};
							
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice] = new yearData(i_year_project);
							
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].Amount = total_revenue;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].prcnt_allocated = i_no_of_resources;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].recognized_revenue = f_revenue_recognized;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].mnth_strt = s_mnth_strt_date;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].mnth_end = s_mnth_end_date;
						}
						else {
							projectWiseRevenue[i_prac_subPrac_role_level].total_revenue_for_tenure = f_total_revenue_for_tenure;
							
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].Amount = total_revenue;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].prcnt_allocated = i_no_of_resources;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].recognized_revenue = f_revenue_recognized;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].mnth_strt = s_mnth_strt_date;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].mnth_end = s_mnth_end_date;
						}
					}
				}
			}
			
			if(s_effrt_json_4)
			{
				var i_practice_previous = 0;
				var f_total_revenue_for_tenure = 0;
				//for (var i_mnth_end_plan = 0; i_mnth_end_plan < a_get_mnth_end_effrt_plan.length; i_mnth_end_plan++)
				var s_entire_json_clubed = JSON.parse(s_effrt_json_4);
				for (var i_mnth_end_plan = 0; i_mnth_end_plan < s_entire_json_clubed.length; i_mnth_end_plan++)
				{
					var a_row_json_data = JSON.parse(JSON.stringify(s_entire_json_clubed[i_mnth_end_plan]));
					for (var i_row_json_index = 0; i_row_json_index < a_row_json_data.length; i_row_json_index++)
					{
						var i_practice = a_row_json_data[i_row_json_index].prac;
						var s_practice = a_row_json_data[i_row_json_index].prac_text;
						var i_sub_practice = a_row_json_data[i_row_json_index].subprac;
						var s_sub_practice = a_row_json_data[i_row_json_index].subprac_text;
						var i_role = a_row_json_data[i_row_json_index].role;
						var s_role = a_row_json_data[i_row_json_index].role_text;
						var i_level = a_row_json_data[i_row_json_index].level;
						var s_level = a_row_json_data[i_row_json_index].level_text;
						var i_location = a_row_json_data[i_row_json_index].loc;
						var s_location = a_row_json_data[i_row_json_index].loc_text;
						var f_revenue = a_row_json_data[i_row_json_index].cost;
						if (!f_revenue) 
							f_revenue = 0;
						
						var f_revenue_share = a_row_json_data[i_row_json_index].share;
						if (!f_revenue_share) 
							f_revenue_share = 0;
							
						var i_no_of_resources = a_row_json_data[i_row_json_index].allo;
						var s_mnth_strt_date = '1/31/2017';
						var s_mnth_end_date = '31/31/2017';
						var s_mnth = a_row_json_data[i_row_json_index].mnth;
						var s_year = a_row_json_data[i_row_json_index].year;
						
						var f_revenue_recognized = 0;
						
						if(a_unique_list_practice_sublist.indexOf(a_row_json_data[i_row_json_index].subprac)<0)
						{
							a_unique_list_practice_sublist.push(a_row_json_data[i_row_json_index].subprac);
							a_duplicate_sub_prac_count.push({
														'sub_prac': a_row_json_data[i_row_json_index].subprac,
														'sub_prac_name': a_row_json_data[i_row_json_index].subprac_text,
														'revenue_share': 50000,
														'no_count': 0
														});
									
						}
						
						f_revenue_recognized = find_recognized_revenue(a_revenue_recognized_for_project,i_practice,i_sub_practice,i_role,i_level,s_mnth,s_year,a_subprac_searched_once,a_subprac_searched_once_month,a_subprac_searched_once_year);
						
						if (!f_revenue_recognized) 
							f_revenue_recognized = 0;
						
						var s_month_name = s_mnth + '_' + s_year;
						
						var total_revenue = parseFloat(i_no_of_resources) * parseFloat(f_revenue);
						total_revenue = parseFloat(total_revenue).toFixed(2);
												
						if (i_practice_previous == 0) {
							i_practice_previous = a_row_json_data[i_row_json_index].prac_row;
						}
						
						if (i_practice_previous != a_row_json_data[i_row_json_index].prac_row) {
							f_total_revenue_for_tenure = 0;
							i_practice_previous = a_row_json_data[i_row_json_index].prac_row;
						}
					
						f_total_revenue_for_tenure = parseFloat(f_total_revenue_for_tenure) + parseFloat(total_revenue);
						f_total_revenue_for_tenure = parseFloat(f_total_revenue_for_tenure).toFixed(2);
						
						var i_prac_subPrac_role_level = i_practice + '_' + i_sub_practice + '_' + i_role + '_' + i_level + '_' + i_location;
						
						if (!projectWiseRevenue[i_prac_subPrac_role_level]) {
							projectWiseRevenue[i_prac_subPrac_role_level] = {
								practice_name: s_practice,
								sub_prac_name: s_sub_practice,
								sub_practice: i_sub_practice,
								role_name: s_role,
								level_name: s_level,
								location_name: s_location,
								total_revenue_for_tenure: f_total_revenue_for_tenure,
								revenue_share: f_revenue_share,
								RevenueData: {}
							};
							
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice] = new yearData(i_year_project);
							
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].Amount = total_revenue;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].prcnt_allocated = i_no_of_resources;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].recognized_revenue = f_revenue_recognized;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].mnth_strt = s_mnth_strt_date;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].mnth_end = s_mnth_end_date;
						}
						else {
							projectWiseRevenue[i_prac_subPrac_role_level].total_revenue_for_tenure = f_total_revenue_for_tenure;
							
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].Amount = total_revenue;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].prcnt_allocated = i_no_of_resources;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].recognized_revenue = f_revenue_recognized;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].mnth_strt = s_mnth_strt_date;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].mnth_end = s_mnth_end_date;
						}
					}
				}
			}
			var monthBreakUp = getMonthsBreakup(d_proj_start_date,d_proj_end_date);
			var excel_cost_data=[];		
			// Revenue share table
			var h_tableHtml_revenue_share = generate_table_effort_view(projectWiseRevenue,monthBreakUp,i_year_project,i_project_mnth,5,s_currency_symbol_proj,s_currency_symbol_usd,i_year_project_end);					
			// Effort View table
			for (var i_dupli = 0; i_dupli < a_duplicate_sub_prac_count.length; i_dupli++)
			{
				//var f_prev_mnth_cost = generate_cost_view_prev_mnth(projectWiseRevenue_previous_effrt,monthBreakUp,i_year_project,i_project_mnth,2,a_duplicate_sub_prac_count[i_dupli].sub_prac,s_currency_symbol_proj,s_currency_symbol_usd,i_year_project_end,i_prject_end_mnth);
				//nlapiLogExecution('audit','cost for prev mnth:- '+f_prev_mnth_cost,'sub prac:- '+a_duplicate_sub_prac_count[i_dupli].sub_prac);
				var h_tableHtml_effort_view = generate_cost_view(projectWiseRevenue,monthBreakUp,i_year_project,i_project_mnth,2,a_duplicate_sub_prac_count[i_dupli].sub_prac,s_currency_symbol_proj,s_currency_symbol_usd,i_year_project_end,i_prject_end_mnth);
				excel_cost_data.push(h_tableHtml_effort_view);
			}
			// Total revenue table testing for now
			var h_tableHtml_total_view = generate_total_project_view(projectWiseRevenue,monthBreakUp,i_year_project,i_project_mnth,4,s_currency_symbol_proj,s_currency_symbol_usd,i_year_project_end,i_prject_end_mnth);
			//if(_logValidation(h_tableHtml_effort_view)&& )
			for(var excel_len=0;excel_len< excel_cost_data.length;excel_len++)
				{
						var rev_len=excel_cost_data[excel_len].month;
					//	for(var reven=0;reven<h_tableHtml_effort_view.length;reven++)
						if(excel_len!=excel_cost_data.length-1)
						{
							bill_data_arr[sr_no] = {
                                    'project_id': excel_cost_data[excel_len].practice,
                                    'project_name': excel_cost_data[excel_len].subpractice,
									'month' :excel_cost_data[excel_len].month,
									'actual_rev':excel_cost_data[excel_len].revenue,
									'cumm_rev' :excel_cost_data[excel_len].cummulative									
								};
						}
						else
						{
								bill_data_arr[sr_no] = {
                                    'project_id': excel_cost_data[excel_len].practice,
                                    'project_name': excel_cost_data[excel_len].subpractice,
									'month' :excel_cost_data[excel_len].month,
									'actual_rev':excel_cost_data[excel_len].revenue,
									'cumm_rev' :excel_cost_data[excel_len].cummulative,
									'monthlyrevenue':h_tableHtml_total_view.monthlyrevenue,
                                    'rev_rec_permnth': h_tableHtml_total_view.rev_rec_permnth,
                                    'act_rev_rec_mnth': h_tableHtml_total_view.act_rev_rec_mnth,
									'cummulative_mnth': h_tableHtml_total_view.cummulative_mnth
                                                             
                                   };
						}
							sr_no++;
							nlapiLogExecution('debug','Data',bill_data_arr);
				}
		}
		
		nlapiLogExecution('audit','remaining usage:- ',nlapiGetContext().getRemainingUsage());
	}
	catch(err)
	{
		nlapiLogExecution('ERROR','suitelet_month_end_confirmation','ERROR MESSAGE:- '+err);
		throw err;
	}
	return bill_data_arr;
}
function generate_total_project_view(projectWiseRevenue, monthBreakUp,
		i_year_project, i_project_mnth, mode, s_currency_symbol_proj,
		s_currency_symbol_usd, i_year_project_end, i_prject_end_mnth) {
	var css_file_url = "";

	var context = nlapiGetContext();

	var html = {
		'month' : [],
		'monthlyrevenue' : [],
		'rev_rec_permnth' : [],
		'act_rev_rec_mnth' : [],
		'cummulative_mnth' : [],
	};

	var today = new Date();
	var currentMonthName = getMonthName(nlapiDateToString(today));

	for (var j = 0; j < monthBreakUp.length; j++) {
		var months = monthBreakUp[j];
		var s_month_name = getMonthName(months.Start); // get month name
		var s_year = nlapiStringToDate(months.Start)
		s_year = s_year.getFullYear();
		s_month_name = s_month_name + '_' + s_year;

		html['month'].push(s_month_name);

	}

	//html += "Total";

	var a_sub_prac_already_displayed = new Array();
	var i_subpractice_id = 0;
	var a_amount = new Array();
	var a_prcnt_arr = new Array();
	var i_frst_row = 0;
	var i_diplay_frst_column = 0;
	var i_total_row_revenue = 0;
	var total_prcnt_aloocated = new Array();
	var b_first_line_copied = 0;
	var f_total_allocated_row = 0;
	var i_total_row_revenue = 0;
	var f_total_prcnt_complt = 0;
	var a_sub_practice_arr = new Array();

	for ( var emp_internal_id in projectWiseRevenue) {
		for ( var project_internal_id in projectWiseRevenue[emp_internal_id].RevenueData) {
			var i_sub_practice_internal_id = 0;
			var i_amt = 0;

			if (a_sub_practice_arr
					.indexOf(projectWiseRevenue[emp_internal_id].sub_practice) < 0) {
				for ( var month in projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id]) {
					var a_MonthNames = MonthNames(i_year_project);
					var currentMonthPos = a_MonthNames.indexOf(month);
					var s_current_month = a_MonthNames[currentMonthPos];
					var s_current_mnth_yr = s_current_month.substr(4, 8);

					var i_nxt_yr_flg = 0;
					if (currentMonthPos >= 12) {
						currentMonthPos = currentMonthPos - 12;
						i_nxt_yr_flg = 1;
					}

					var i_curr_yr_dis_flag = 0;
					if (parseInt(s_current_mnth_yr) < i_year_project_end
							&& i_prject_end_mnth < currentMonthPos) {
						i_curr_yr_dis_flag = 1;
					}

					if (((i_project_mnth <= currentMonthPos
							&& ((i_prject_end_mnth >= currentMonthPos || i_curr_yr_dis_flag == 1) && i_nxt_yr_flg == 0) && parseInt(s_current_mnth_yr) <= i_year_project) && parseInt(s_current_mnth_yr) <= i_year_project_end)
							|| (i_prject_end_mnth >= currentMonthPos
									&& parseInt(s_current_mnth_yr) <= i_year_project_end && i_nxt_yr_flg == 1)) {
						var total_revenue_format = parseFloat(projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id][month].total_revenue);
						if (!total_revenue_format)
							total_revenue_format = 0;

						var f_perctn_complete = parseFloat(projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id][month].prcent_complt);
						if (!f_perctn_complete)
							f_perctn_complete = 0;

						if (i_frst_row == 0) {
							a_amount.push(total_revenue_format);
							a_prcnt_arr.push(f_perctn_complete);
						} else {
							var amnt_mnth = a_amount[i_amt];

							var f_revenue_amount = projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id][month].total_revenue;
							if (!f_revenue_amount)
								f_revenue_amount = 0;

							amnt_mnth = parseFloat(amnt_mnth)
									+ parseFloat(f_revenue_amount);
							a_amount[i_amt] = amnt_mnth;
							//i_amt++;

							var f_prcnt_present = a_prcnt_arr[i_amt];

							var f_prcnt_updated = projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id][month].prcent_complt;
							if (!f_prcnt_updated)
								f_prcnt_updated = 0;

							f_prcnt_present = parseFloat(f_prcnt_present)
									+ parseFloat(f_prcnt_updated);
							a_prcnt_arr[i_amt] = f_prcnt_present;
							i_amt++;
						}

						a_sub_practice_arr
								.push(projectWiseRevenue[emp_internal_id].sub_practice);

					}
				}
			}
			i_frst_row = 1;

			var f_revenue_share = projectWiseRevenue[emp_internal_id].revenue_share;
		}
	}

	//prcnt completed for project

	html['monthlyrevenue'].push('Monthly Percent Complete');

	for (var i_amt = 0; i_amt < a_amount.length; i_amt++) {
		i_total_row_revenue = parseFloat(i_total_row_revenue)
				+ parseFloat(a_amount[i_amt]);
	}

	for (var i_amt = 0; i_amt < a_amount.length; i_amt++) {
		var f_prcnt_to_diaplay = parseFloat(a_amount[i_amt])
				/ parseFloat(i_total_row_revenue);
		f_prcnt_to_diaplay = parseFloat(f_prcnt_to_diaplay) * 100;

		html['monthlyrevenue'].push(parseFloat(f_prcnt_to_diaplay).toFixed(
				1)
				+ ' %');

		f_total_prcnt_complt = parseFloat(f_total_prcnt_complt)
				+ parseFloat(f_prcnt_to_diaplay);

	}

	//                	html += "<td class='projected-amount'>";
	html['monthlyrevenue'].push(parseFloat(f_total_prcnt_complt).toFixed(1)
			+ ' %');
	//html += "</td>";

	//html += "</tr>";

	// total revenue recognized project level

	html['rev_rec_permnth'].push('Revenue to be reconized/month'); //Total Project Revenue as per Plan

	for (var i_amt = 0; i_amt < a_amount.length; i_amt++) {

		html['rev_rec_permnth'].push(format2(a_amount[i_amt]));

	}

	html['rev_rec_permnth'].push(format2(i_total_row_revenue));

	// actual revenue that will be recognized

	html['act_rev_rec_mnth']
			.push('Actual revenue recognized/Revenue forecast'); //Actual Revenue to Recognize

	var d_today_date = new Date();
	var i_current_month = d_today_date.getMonth();
	var i_current_year = d_today_date.getFullYear();
	//nlapiLogExecution('audit','proj level len:- ',a_recognized_revenue_total_proj_level.length);

	var f_total_revenue_proj_level = 0;
	if (parseInt(i_year_project) != parseInt(i_current_year)) {
		var i_total_project_tenure = 11 - parseInt(i_project_mnth);
		i_current_month = i_current_month + i_total_project_tenure + 1;

		for (var i_revenue_index = 0; i_revenue_index < i_current_month; i_revenue_index++) {
			//i_total_revenue_recognized_ytd = parseFloat(i_total_revenue_recognized_ytd) + parseFloat(a_recognized_revenue[i_revenue_index]);

			html['act_rev_rec_mnth']
					.push(format2(a_recognized_revenue_total_proj_level[i_revenue_index]));
			f_total_revenue_proj_level = parseFloat(f_total_revenue_proj_level)
					+ parseFloat(a_recognized_revenue_total_proj_level[i_revenue_index]);
		}
	} else {
		i_current_month = parseFloat(i_current_month)
				- parseFloat(i_project_mnth);
		for (var i_revenue_index = 0; i_revenue_index <= i_current_month - 1; i_revenue_index++) {
			//i_total_revenue_recognized_ytd = parseFloat(i_total_revenue_recognized_ytd) + parseFloat(a_recognized_revenue[i_revenue_index]);

			html['act_rev_rec_mnth']
					.push(format2(a_recognized_revenue_total_proj_level[i_revenue_index]));

			f_total_revenue_proj_level = parseFloat(f_total_revenue_proj_level)
					+ parseFloat(a_recognized_revenue_total_proj_level[i_revenue_index]);
		}
	}

	html['act_rev_rec_mnth']
			.push(format2(f_current_month_actual_revenue_total_proj_level));

	f_total_revenue_proj_level = parseFloat(f_total_revenue_proj_level)
			+ parseFloat(f_current_month_actual_revenue_total_proj_level);

	for (var i_amt = i_current_month + 1; i_amt < a_amount.length; i_amt++) {

		html['act_rev_rec_mnth'].push(format2(a_amount[i_amt]));

		f_total_revenue_proj_level = parseFloat(f_total_revenue_proj_level)
				+ parseFloat(a_amount[i_amt]);
	}

	html['act_rev_rec_mnth'].push(format2(f_total_revenue_proj_level));

	// cumulative total project level

	html['cummulative_mnth'].push('Cumulative Revenue');

	var f_project_level_cumulative = 0;
	for (var i_revenue_index = 0; i_revenue_index <= i_current_month - 1; i_revenue_index++) {
		//i_total_revenue_recognized_ytd = parseFloat(i_total_revenue_recognized_ytd) + parseFloat(a_recognized_revenue[i_revenue_index]);
		f_project_level_cumulative = parseFloat(f_project_level_cumulative)
				+ parseFloat(a_recognized_revenue_total_proj_level[i_revenue_index]);
		html['cummulative_mnth'].push(format2(f_project_level_cumulative));

	}

	f_project_level_cumulative = parseFloat(f_project_level_cumulative)
			+ parseFloat(f_current_month_actual_revenue_total_proj_level);

	html['cummulative_mnth'].push(format2(f_project_level_cumulative));

	for (var i_amt = i_current_month + 1; i_amt < a_amount.length; i_amt++) {
		f_project_level_cumulative = parseFloat(f_project_level_cumulative)
				+ parseFloat(a_amount[i_amt]);
		html['cummulative_mnth'].push(format2(f_project_level_cumulative));
	}

	html['cummulative_mnth'].push(format2(f_total_revenue_proj_level));
	nlapiLogExecution('debug', 'Value', JSON.stringify(html));

	return html;
}
function generate_cost_view(projectWiseRevenue, monthBreakUp,
		i_year_project, i_project_mnth, mode, sub_prac,
		s_currency_symbol_proj, s_currency_symbol_usd, i_year_project_end,
		i_prject_end_mnth) {
	var css_file_url = "";

	var context = nlapiGetContext();
	var html_data = [];
	var html_val = {};

	var html = {
		'month' : [],
		'practice' : [],
		'subpractice' : [],
		'revenue' : [],
		'actual_rec' : [],
		'cummulative' : [],
		'html_amount' : []
	};

	var today = new Date();
	var currentMonthName = getMonthName(nlapiDateToString(today));
	//html['currency'].push(s_currency_symbol_proj);
	for (var j = 0; j < monthBreakUp.length; j++) {
		var months = monthBreakUp[j];
		var s_month_name = getMonthName(months.Start); // get month name
		var s_year = nlapiStringToDate(months.Start)
		s_year = s_year.getFullYear();
		s_month_name = s_month_name + '_' + s_year;
		html['month'].push(s_month_name);
	}

	var a_sub_prac_already_displayed = new Array();
	var i_subpractice_id = 0;
	var a_amount = new Array();
	var a_recognized_revenue = new Array();
	var i_frst_row = 0;
	var i_diplay_frst_column = 0;
	var i_total_row_revenue = 0;
	var i_total_per_row = 0;
	var total_prcnt_aloocated = new Array();
	var b_first_line_copied = 0;
	var f_total_allocated_row = 0;

	for ( var emp_internal_id in projectWiseRevenue) {
		if (projectWiseRevenue[emp_internal_id].sub_practice == sub_prac) {
			f_total_allocated_row = 0;
			for ( var project_internal_id in projectWiseRevenue[emp_internal_id].RevenueData) {

				if (i_diplay_frst_column == 0) {
					//	html += "<td class='label-name'>";
					//	html['revenue'].push('Effort View');
					//	html += "</td>";
				} else {
					//html += "<td class='label-name'>";
					//	html += '<b>';
					//html += "</td>";
				}

				projectWiseRevenue[emp_internal_id].practice_name;
				projectWiseRevenue[emp_internal_id].sub_prac_name;
				projectWiseRevenue[emp_internal_id].role_name;
				projectWiseRevenue[emp_internal_id].level_name;
				projectWiseRevenue[emp_internal_id].location_name;

				var i_index_mnth = 0;
				for ( var month in projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id]) {
					var a_MonthNames = MonthNames(i_year_project);
					var currentMonthPos = a_MonthNames.indexOf(month);
					var s_current_month = a_MonthNames[currentMonthPos];
					var s_current_mnth_yr = s_current_month.substr(4, 8);

					var i_nxt_yr_flg = 0;
					if (currentMonthPos >= 12) {
						currentMonthPos = currentMonthPos - 12;
						i_nxt_yr_flg = 1;
					}

					var i_curr_yr_dis_flag = 0;
					if (parseInt(s_current_mnth_yr) < i_year_project_end
							&& i_prject_end_mnth < currentMonthPos) {
						i_curr_yr_dis_flag = 1;
					}

					if (((i_project_mnth <= currentMonthPos
							&& ((i_prject_end_mnth >= currentMonthPos || i_curr_yr_dis_flag == 1) && i_nxt_yr_flg == 0) && parseInt(s_current_mnth_yr) <= i_year_project) && parseInt(s_current_mnth_yr) <= i_year_project_end)
							|| (i_prject_end_mnth >= currentMonthPos
									&& parseInt(s_current_mnth_yr) <= i_year_project_end && i_nxt_yr_flg == 1)) {
						//nlapiLogExecution('audit','currentMonthPos:- ',+currentMonthPos);
						if (b_first_line_copied == 1) {
							var f_allocated_prcnt = total_prcnt_aloocated[i_index_mnth];
							f_allocated_prcnt = parseFloat(f_allocated_prcnt)
									+ parseFloat(projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id][month].prcnt_allocated);
							total_prcnt_aloocated[i_index_mnth] = f_allocated_prcnt;
							i_index_mnth++;
						} else {
							total_prcnt_aloocated
									.push(projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id][month].prcnt_allocated);
						}

						f_total_allocated_row = parseFloat(projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id][month].prcnt_allocated)
								+ parseFloat(f_total_allocated_row);

						parseFloat(
								projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id][month].prcnt_allocated)
								.toFixed(2);

					}
				}
			}

			b_first_line_copied = 1;

			parseFloat(f_total_allocated_row).toFixed(2);

			var s_sub_prac_name = projectWiseRevenue[emp_internal_id].sub_prac_name;
			var s_practice_name = projectWiseRevenue[emp_internal_id].practice_name;

			i_diplay_frst_column = 1;
		}
	}
	i_diplay_frst_column = 0;

	var f_total_row_allocation = 0;
	for (var i_index_total_allocated = 0; i_index_total_allocated < total_prcnt_aloocated.length; i_index_total_allocated++) {
		
		f_total_row_allocation = parseFloat(f_total_row_allocation)
				+ parseFloat(total_prcnt_aloocated[i_index_total_allocated]);
	}


	for ( var emp_internal_id in projectWiseRevenue) {

		for ( var project_internal_id in projectWiseRevenue[emp_internal_id].RevenueData) {

			if (projectWiseRevenue[emp_internal_id].sub_practice == sub_prac) {
				projectWiseRevenue[emp_internal_id].practice_name;

				projectWiseRevenue[emp_internal_id].sub_prac_name;

				projectWiseRevenue[emp_internal_id].role_name;
				projectWiseRevenue[emp_internal_id].level_name;
				projectWiseRevenue[emp_internal_id].location_name;
				var i_sub_practice_internal_id = 0;
				var i_amt = 0;
				for ( var month in projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id]) {
					var a_MonthNames = MonthNames(i_year_project);
					var currentMonthPos = a_MonthNames.indexOf(month);
					var s_current_month = a_MonthNames[currentMonthPos];
					var s_current_mnth_yr = s_current_month.substr(4, 8);

					var i_nxt_yr_flg = 0;
					if (currentMonthPos >= 12) {
						currentMonthPos = currentMonthPos - 12;
						i_nxt_yr_flg = 1;
					}

					var i_curr_yr_dis_flag = 0;
					if (parseInt(s_current_mnth_yr) < i_year_project_end
							&& i_prject_end_mnth < currentMonthPos) {
						i_curr_yr_dis_flag = 1;
					}

					if (((i_project_mnth <= currentMonthPos
							&& ((i_prject_end_mnth >= currentMonthPos || i_curr_yr_dis_flag == 1) && i_nxt_yr_flg == 0) && parseInt(s_current_mnth_yr) <= i_year_project) && parseInt(s_current_mnth_yr) <= i_year_project_end)
							|| (i_prject_end_mnth >= currentMonthPos
									&& parseInt(s_current_mnth_yr) <= i_year_project_end && i_nxt_yr_flg == 1)) {
						if (mode == 2) {
							var total_revenue_format = parseFloat(
									projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id][month].Amount)
									.toFixed(1);
							if (i_frst_row == 0) {
								//nlapiLogExecution('audit','inside push recog amnt:- '+projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id][month].recognized_revenue);
								a_amount.push(total_revenue_format);
								a_recognized_revenue
										.push(projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id][month].recognized_revenue);
							} else {
								var amnt_mnth = a_amount[i_amt];
								amnt_mnth = parseFloat(amnt_mnth)
										+ parseFloat(projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id][month].Amount);

								a_amount[i_amt] = amnt_mnth;

								var i_existing_recognised_amount = a_recognized_revenue[i_amt];
								/*nlapiLogExecution('audit', 'month name:- '
										+ month);*/
/*								nlapiLogExecution(
										'audit',
										'index:- '
												+ i_amt
												+ '::existing rev:- '
												+ i_existing_recognised_amount,
										'Rev REcg:- '
												+ projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id][month].recognized_revenue);*/
								i_existing_recognised_amount = parseFloat(i_existing_recognised_amount)
										+ parseFloat(projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id][month].recognized_revenue);

								a_recognized_revenue[i_amt] = i_existing_recognised_amount;

								i_amt++;
							}
							i_total_row_revenue = parseFloat(i_total_row_revenue)
									+ parseFloat(projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id][month].Amount);
							i_total_per_row = parseFloat(i_total_per_row)
									+ parseFloat(projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id][month].Amount);
						}

						//html += "</td>";
					}
				}

				if (mode == 2) {
					i_total_per_row = 0;
				}

				i_frst_row = 1;

				i_diplay_frst_column = 1;

				var f_revenue_share = projectWiseRevenue[emp_internal_id].revenue_share;
			}
		}
	}

	var s_sub_prac_name = '';
	for ( var emp_internal_id in projectWiseRevenue) {
		if (projectWiseRevenue[emp_internal_id].sub_practice == sub_prac) {
			s_sub_prac_name = projectWiseRevenue[emp_internal_id].sub_prac_name;
			s_practice_name = projectWiseRevenue[emp_internal_id].practice_name;

		}
	}
	var f_total_prcnt = 0;
	for (var i_amt = 0; i_amt < a_amount.length; i_amt++) {
		var f_prcnt_revenue = parseFloat(a_amount[i_amt])
				/ parseFloat(i_total_row_revenue);
		f_prcnt_revenue = f_prcnt_revenue * 100;
		if (!f_prcnt_revenue)
			f_prcnt_revenue = 0;

		f_total_prcnt = parseFloat(f_prcnt_revenue)
				+ parseFloat(f_total_prcnt);

	}

	var d_today_date = new Date();
	var i_current_month = d_today_date.getMonth();
	var i_current_year = d_today_date.getFullYear();

	var f_total_revenue = 0;
	var f_revenue_amount_till_current_month = 0;
	for (var i_amt = 0; i_amt < a_amount.length; i_amt++) {
		var f_prcnt_revenue = parseFloat(a_amount[i_amt])
				/ parseFloat(i_total_row_revenue);
		f_prcnt_revenue = f_prcnt_revenue * 100;

		var f_revenue_amount = (parseFloat(f_prcnt_revenue) * parseFloat(f_revenue_share)) / 100;
		if (!f_revenue_amount)
			f_revenue_amount = 0;

		if (parseInt(i_year_project) != parseInt(i_current_year)) {
			var i_total_project_tenure = 11 + parseInt(i_current_month);
			var f_total_prev_mnths = parseFloat(i_total_project_tenure)
					- parseFloat(i_project_mnth);
			f_total_prev_mnths = parseFloat(f_total_prev_mnths)
					+ parseFloat(1);
		} else {
			var f_total_prev_mnths = parseFloat(i_current_month)
					- parseFloat(i_project_mnth);
		}

		if (i_amt <= f_total_prev_mnths) {
			//nlapiLogExecution('audit','f_revenue_amount:- '+f_revenue_amount,'a_amount.length:- '+a_amount.length);
			f_revenue_amount_till_current_month = parseFloat(f_revenue_amount)
					+ parseFloat(f_revenue_amount_till_current_month);
		}

		f_total_revenue = parseFloat(f_revenue_amount)
				+ parseFloat(f_total_revenue);

	}

	for ( var emp_internal_id in projectWiseRevenue) {
		for ( var project_internal_id in projectWiseRevenue[emp_internal_id].RevenueData) {
			var i_amount_map = 0;
			if (projectWiseRevenue[emp_internal_id].sub_practice == sub_prac) {
				for ( var month in projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id]) {
					var a_MonthNames = MonthNames(i_year_project);
					var currentMonthPos = a_MonthNames.indexOf(month);
					var s_current_month = a_MonthNames[currentMonthPos];
					var s_current_mnth_yr = s_current_month.substr(4, 8);

					var i_nxt_yr_flg = 0;
					if (currentMonthPos >= 12) {
						currentMonthPos = currentMonthPos - 12;
						i_nxt_yr_flg = 1;
					}

					var i_curr_yr_dis_flag = 0;
					if (parseInt(s_current_mnth_yr) < i_year_project_end
							&& i_prject_end_mnth < currentMonthPos) {
						i_curr_yr_dis_flag = 1;
					}

					if (((i_project_mnth <= currentMonthPos
							&& ((i_prject_end_mnth >= currentMonthPos || i_curr_yr_dis_flag == 1) && i_nxt_yr_flg == 0) && parseInt(s_current_mnth_yr) <= i_year_project) && parseInt(s_current_mnth_yr) <= i_year_project_end)
							|| (i_prject_end_mnth >= currentMonthPos
									&& parseInt(s_current_mnth_yr) <= i_year_project_end && i_nxt_yr_flg == 1)) {
						var f_prcnt_revenue = parseFloat(a_amount[i_amount_map])
								/ parseFloat(i_total_row_revenue);
						f_prcnt_revenue = f_prcnt_revenue * 100;

						var f_revenue_amount = (parseFloat(f_prcnt_revenue) * parseFloat(f_revenue_share)) / 100;

						projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id][month].total_revenue = f_revenue_amount;
						projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id][month].prcent_complt = f_prcnt_revenue;
						i_amount_map++;
					}
				}
			}
		}
	}

	var today = new Date();
	var currentMonthName = getMonthName(nlapiDateToString(today));

	for (var j = 0; j < monthBreakUp.length; j++) {
		var months = monthBreakUp[j];
		var s_month_name = getMonthName(months.Start); // get month name
		var s_year = nlapiStringToDate(months.Start)
		s_year = s_year.getFullYear();
		s_month_name = s_month_name + '_' + s_year;

	}

	
	var i_total_revenue_recognized_ytd = 0;
	for (var i_revenue_index = 0; i_revenue_index < a_recognized_revenue.length; i_revenue_index++) {
		//nlapiLogExecution('audit','prev mnth recog rev:- '+a_recognized_revenue[i_revenue_index]);
		i_total_revenue_recognized_ytd = parseFloat(i_total_revenue_recognized_ytd)
				+ parseFloat(a_recognized_revenue[i_revenue_index]);

	}

	html['revenue'].push('Actual revenue recognized/Revenue forecast');
	html['practice'].push(s_practice_name);
	html['subpractice'].push(s_sub_prac_name);

	if (parseInt(i_year_project) != parseInt(i_current_year)) {
		var i_total_project_tenure = 11 + parseInt(i_current_month);
		var f_total_prev_mnths = parseFloat(i_total_project_tenure)
				- parseFloat(i_project_mnth);
		i_current_month = parseFloat(f_total_prev_mnths) + parseFloat(1);

		for (var i_revenue_index = 0; i_revenue_index < i_current_month; i_revenue_index++) {

			html['revenue']
					.push(format2(a_recognized_revenue[i_revenue_index]));

		}
	} else {
		i_current_month = parseFloat(i_current_month)
				- parseFloat(i_project_mnth);
		nlapiLogExecution('audit', 'i_current_month:- ' + i_current_month,
				a_recognized_revenue[i_current_month - 1]);
		for (var i_revenue_index = 0; i_revenue_index <= i_current_month - 1; i_revenue_index++) {
			html['revenue']
					.push(format2(a_recognized_revenue[i_revenue_index]));

		}
	}

	nlapiLogExecution('audit', 'f_revenue_amount_till_current_month:- '
			+ f_revenue_amount_till_current_month,
			'i_total_revenue_recognized_ytd:- '
					+ i_total_revenue_recognized_ytd);
	var f_current_month_actual_revenue = parseFloat(f_revenue_amount_till_current_month)
			- parseFloat(i_total_revenue_recognized_ytd);
	//var i_total_revenue_recognized_ytd = 0;

	f_current_month_actual_revenue_total_proj_level = parseFloat(f_current_month_actual_revenue_total_proj_level)
			+ parseFloat(f_current_month_actual_revenue);

	html['revenue'].push(format2(f_current_month_actual_revenue));

	var i_total_actual_revenue_recognized = parseFloat(i_total_revenue_recognized_ytd)
			+ parseFloat(f_current_month_actual_revenue);

	for (var i_amt = i_current_month + 1; i_amt < a_amount.length; i_amt++) {
		var f_prcnt_revenue = parseFloat(a_amount[i_amt])
				/ parseFloat(i_total_row_revenue);
		f_prcnt_revenue = f_prcnt_revenue * 100;

		var f_revenue_amount = (parseFloat(f_prcnt_revenue) * parseFloat(f_revenue_share)) / 100;
		if (!f_revenue_amount)
			f_revenue_amount = 0;

		if (i_amt < i_current_month) {
			//nlapiLogExecution('audit','i_current_month:- '+i_current_month,'a_amount.length:- '+a_amount.length);
			f_revenue_amount_till_current_month = parseFloat(f_revenue_amount)
					+ parseFloat(f_revenue_amount_till_current_month);
			if (!f_revenue_amount_till_current_month)
				f_revenue_amount_till_current_month = 0;
		}

		f_total_revenue = parseFloat(f_revenue_amount)
				+ parseFloat(f_total_revenue);

		html['revenue'].push(format2(f_revenue_amount));

		i_total_actual_revenue_recognized = parseFloat(i_total_actual_revenue_recognized)
				+ parseFloat(f_revenue_amount);
	}

	html['revenue'].push(format2(i_total_actual_revenue_recognized));

	html['cummulative'].push('Cumulative Revenue');
	

	var f_cumulative_rev = 0;
	for (var i_revenue_index = 0; i_revenue_index <= i_current_month - 1; i_revenue_index++) {
		var f_amount_rounded_off = parseFloat(
				a_recognized_revenue[i_revenue_index]).toFixed(1);

		f_cumulative_rev = parseFloat(f_cumulative_rev)
				+ parseFloat(f_amount_rounded_off);
		//	html += "<td class='monthly-amount'>";
		html['cummulative'].push(format2(f_cumulative_rev));
		//html += "</td>";
	}

	f_amount_rounded_off = parseFloat(f_current_month_actual_revenue)
			.toFixed(1);
	f_cumulative_rev = parseFloat(f_cumulative_rev)
			+ parseFloat(f_amount_rounded_off);
	//html += "<td class='monthly-amount'>";
	html['cummulative'].push(format2(f_cumulative_rev));
	//html += "</td>";

	for (var i_future_index = i_current_month + 1; i_future_index < a_amount.length; i_future_index++) {
		var f_prcnt_revenue = parseFloat(a_amount[i_future_index])
				/ parseFloat(i_total_row_revenue);
		f_prcnt_revenue = f_prcnt_revenue * 100;

		var f_revenue_amount = (parseFloat(f_prcnt_revenue) * parseFloat(f_revenue_share)) / 100;
		if (!f_revenue_amount)
			f_revenue_amount = 0;

		f_cumulative_rev = parseFloat(f_cumulative_rev)
				+ parseFloat(f_revenue_amount);
		//	html += "<td class='monthly-amount'>";
		html['cummulative'].push(format2(f_cumulative_rev));
		//	html += "</td>";
	}

	//if(i_total_revenue_recognized_ytd == 0)
	i_total_revenue_recognized_ytd = f_revenue_share;

	//html += "<td class='monthly-amount'>";
	html['cummulative'].push(format2(i_total_revenue_recognized_ytd));
	

	for (var i_exist_index = 0; i_exist_index <= a_recognized_revenue.length; i_exist_index++) {
		if (a_recognized_revenue_total_proj_level[i_exist_index]) {
			var f_existing_cost_in_arr = parseFloat(a_recognized_revenue_total_proj_level[i_exist_index]);
		} else {
			var f_existing_cost_in_arr = 0;
		}

		var f_existing_cost = a_recognized_revenue[i_exist_index];
		f_existing_cost = parseFloat(f_existing_cost)
				+ parseFloat(f_existing_cost_in_arr);

		a_recognized_revenue_total_proj_level[i_exist_index] = f_existing_cost;
	}
	//a_recognized_revenue_total_proj_level = a_recognized_revenue;
	html_val = {
		pract : s_practice_name,
		sub_pract : s_sub_prac_name,
	};
	html_data.push(html);
	nlapiLogExecution('DEBUG', 'HTML', JSON.stringify(html));

	return html;
}

function generate_table_effort_view(projectWiseRevenue, monthBreakUp,
		i_year_project, i_project_mnth, mode, s_currency_symbol_proj,
		s_currency_symbol_usd, i_year_project_end) {
	var css_file_url = "";

	var context = nlapiGetContext();
	if (context.getEnvironment() == "SANDBOX") {
		css_file_url = "https://system.sandbox.netsuite.com/core/media/media.nl?id=199133&c=3883006&h=64fa194295cd4ab64d58&mv=iq3kjr6g&_xt=.css&whence=";
	} else if (context.getEnvironment() == "PRODUCTION") {
		css_file_url = "https://system.na1.netsuite.com/core/media/media.nl?id=199133&c=3883006&h=64fa194295cd4ab64d58&mv=iq3kjr6g&_xt=.css&whence=";
	}

	var html = "";
	html += "<link href='" + css_file_url + "'  rel='stylesheet'>";

	html += "<table class='projection-table'>";

	// header
	html += "<tr class='header-row'>";

	html += "<td>";
	html += "Practice";
	html += "</td>";

	html += "<td>";
	html += "Sub Practice";
	html += "</td>";

	if (mode != 5) {
		html += "<td>";
		html += "Role";
		html += "</td>";

		if (mode == 1 || mode == 2 || mode == 6) {
			html += "<td>";
			html += "Level";
			html += "</td>";

			if (mode == 6) {
				html += "<td>";
				html += "No. of resources";
				html += "</td>";

				html += "<td>";
				html += "Allocation start date";
				html += "</td>";

				html += "<td>";
				html += "Allocation end date";
				html += "</td>";

				html += "<td>";
				html += "Percent allocated";
				html += "</td>";
			}

			html += "<td>";
			html += "Location";
			html += "</td>";
		}
	} else {
		html += "<td>";
		html += "Revenue Share";
		html += "</td>";
	}

	if (mode != 5 && mode != 6) {
		var today = new Date();
		var currentMonthName = getMonthName(nlapiDateToString(today));

		for (var j = 0; j < monthBreakUp.length; j++) {
			var months = monthBreakUp[j];
			var s_month_name = getMonthName(months.Start); // get month name
			var s_year = nlapiStringToDate(months.Start)
			s_year = s_year.getFullYear();
			s_month_name = s_month_name + '_' + s_year;

			html += "<td>";
			html += s_month_name;
			html += "</td>";
		}

		if (mode == 2) {
			html += "<td>";
			html += "Total";
			html += "</td>";
		}
	}

	html += "</tr>";

	var a_sub_prac_already_displayed = new Array();
	var i_subpractice_id = 0;
	for ( var emp_internal_id in projectWiseRevenue) {

		for ( var project_internal_id in projectWiseRevenue[emp_internal_id].RevenueData) {

			var b_display_reveue_share = 'F';
			if (a_sub_prac_already_displayed
					.indexOf(projectWiseRevenue[emp_internal_id].sub_practice) < 0
					&& mode == 5) {
				html += "<tr>";
				html += "<td class='label-name'>";
				html += projectWiseRevenue[emp_internal_id].practice_name;
				html += "</td>";

				html += "<td class='label-name'>";
				html += projectWiseRevenue[emp_internal_id].sub_prac_name;
				html += "</td>";

				b_display_reveue_share = 'T';
				a_sub_prac_already_displayed
						.push(projectWiseRevenue[emp_internal_id].sub_practice);
			}

			if (mode != 5) {
				html += "<tr>";
				html += "<td class='label-name'>";
				html += projectWiseRevenue[emp_internal_id].practice_name;
				html += "</td>";

				html += "<td class='label-name'>";
				html += projectWiseRevenue[emp_internal_id].sub_prac_name;
				html += "</td>";
			}

			if (mode != 5) {
				html += "<td class='label-name'>";
				html += projectWiseRevenue[emp_internal_id].role_name;
				html += "</td>";

				if (mode == 1 || mode == 2 || mode == 6) {
					html += "<td class='label-name'>";
					html += projectWiseRevenue[emp_internal_id].level_name;
					html += "</td>";

					if (mode == 6) {
						html += "<td class='label-name'>";
						html += projectWiseRevenue[emp_internal_id].no_of_resources;
						html += "</td>";

						html += "<td class='label-name'>";
						html += projectWiseRevenue[emp_internal_id].allocation_strt_date;
						html += "</td>";

						html += "<td class='label-name'>";
						html += projectWiseRevenue[emp_internal_id].allocation_end_date;
						html += "</td>";

						html += "<td class='label-name'>";
						html += projectWiseRevenue[emp_internal_id].total_allocation;
						html += "</td>";
					}

					html += "<td class='label-name'>";
					html += projectWiseRevenue[emp_internal_id].location_name;
					html += "</td>";
				}
			} else {
				if (b_display_reveue_share == 'T') {
					html += "<td class='label-name'>";
					html += ''
							+ s_currency_symbol_proj
							+ ' '
							+ format2(projectWiseRevenue[emp_internal_id].revenue_share);
					html += "</td>";
				}
			}

			if (mode != 5 && mode != 6) {
				var i_sub_practice_internal_id = 0;
				for ( var month in projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id]) {
					var a_MonthNames = MonthNames(i_year_project);
					var currentMonthPos = a_MonthNames.indexOf(month);
					var s_current_month = a_MonthNames[currentMonthPos];
					var s_current_mnth_yr = s_current_month.substr(4, 8);

					var i_nxt_yr_flg = 0;
					if (currentMonthPos >= 12) {
						currentMonthPos = currentMonthPos - 12;
						i_nxt_yr_flg = 1;
					}

					var i_curr_yr_dis_flag = 0;
					if (parseInt(s_current_mnth_yr) < i_year_project_end
							&& i_prject_end_mnth < currentMonthPos) {
						i_curr_yr_dis_flag = 1;
					}

					if (((i_project_mnth <= currentMonthPos
							&& ((i_prject_end_mnth >= currentMonthPos || i_curr_yr_dis_flag == 1) && i_nxt_yr_flg == 0) && parseInt(s_current_mnth_yr) <= i_year_project) && parseInt(s_current_mnth_yr) <= i_year_project_end)
							|| (i_prject_end_mnth >= currentMonthPos
									&& parseInt(s_current_mnth_yr) <= i_year_project_end && i_nxt_yr_flg == 1)) {

						if (mode == 2) {
							html += "<td class='monthly-amount'>";
							var total_revenue_format = parseFloat(
									projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id][month].Amount)
									.toFixed(1);
							html += format2(total_revenue_format);
						} else if (mode == 1) {
							html += "<td class='monthly-amount'>";
							html += parseFloat(
									projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id][month].prcnt_allocated)
									.toFixed(1);

							if (i_sub_practice_internal_id = 0) {
								i_sub_practice_internal_id = projectWiseRevenue[emp_internal_id].sub_practice;
							}
						} else if (mode == 3 || mode == 4) {
							var f_amount = projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id][month].Amount;
							var f_total_amount = projectWiseRevenue[emp_internal_id].total_revenue_for_tenure;

							var f_prcnt_cost = parseFloat(f_amount)
									/ parseFloat(f_total_amount);
							var f_prcnt_cost_ = parseFloat(f_prcnt_cost)
									* parseFloat(100);

							if (mode == 3) {
								f_prcnt_cost_ = parseFloat(f_prcnt_cost_)
										.toFixed(1);
								html += "<td class='monthly-amount'>";
								html += f_prcnt_cost_ + ' %';
							} else {
								html += "<td class='projected-amount'>";
								var f_revenue_share = projectWiseRevenue[emp_internal_id].revenue_share;
								if (projectWiseRevenue[emp_internal_id].sub_practice == 316)
									nlapiLogExecution('audit', 'prcnt:- '
											+ f_prcnt_cost, f_revenue_share);

								f_revenue_share = (parseFloat(f_prcnt_cost_) * parseFloat(f_revenue_share)) / 100;
								html += format2(f_revenue_share);
							}
						}

						html += "</td>";
					}
				}
			}

			if (mode == 2) {
				html += "<td class='monthly-amount'>";
				html += format2(projectWiseRevenue[emp_internal_id].total_revenue_for_tenure);
				html += "</td>";
			}

			html += "</tr>";
		}
	}

	html += "</table>";

	return html;
}

//find_recognized_revenue ->starts

function find_recognized_revenue(a_revenue_recognized_for_project,
practice, subpractice, role, level, month, year,
a_subprac_searched_once, a_subprac_searched_once_month,
a_subprac_searched_once_year) {
var f_recognized_amount = 0;


/*function func_search_prac_processed_fr_mnth(a_subprac_searched_once,
		subpractice, month, year) {*/
	var i_return_var = -1;
	for (var i_loop = 0; i_loop < a_subprac_searched_once.length; i_loop++) {
		if (a_subprac_searched_once[i_loop].subpractice == subpractice) {
			if (a_subprac_searched_once[i_loop].month == month) {
				if (a_subprac_searched_once[i_loop].year == year) {
					i_return_var = i_loop;
					break;
				}
			}
		}
	}

	//return i_return_var;
//}

/*if (func_search_prac_processed_fr_mnth(a_subprac_searched_once,
	subpractice, month, year) < 0) {*/
	if (i_return_var < 0) {
for (var i_index_find = 0; i_index_find < a_revenue_recognized_for_project.length; i_index_find++) {
	if (practice == a_revenue_recognized_for_project[i_index_find].practice_revenue_recognized) {
		if (subpractice == a_revenue_recognized_for_project[i_index_find].subpractice_revenue_recognized) {
			//if(role == a_revenue_recognized_for_project[i_index_find].role_revenue_recognized)
			{
				//if(level == a_revenue_recognized_for_project[i_index_find].level_revenue_recognized)
				{
					if (month == a_revenue_recognized_for_project[i_index_find].month_revenue_recognized) {
						if (parseInt(year) == parseInt(a_revenue_recognized_for_project[i_index_find].year_revenue_recognized)) {
							f_recognized_amount = a_revenue_recognized_for_project[i_index_find].amount_revenue_recognized;
							//nlapiLogExecution('audit','f_recognized_amount inside look for prev mnth amnt once pr sub prac:- ',f_recognized_amount);
							a_subprac_searched_once.push({
								'subpractice' : subpractice,
								'month' : month,
								'year' : year
							});
							//a_subprac_searched_once_month.push(month);
							//a_subprac_searched_once_year.push(year);

							if (subpractice == 325) {
								nlapiLogExecution('audit',
										'month:- ' + month,
										'year:- ' + year);
								nlapiLogExecution(
										'audit',
										'f_recognized_amount:- '
												+ f_recognized_amount);
							}
						}
					}
				}
			}
		}
	}
}

return f_recognized_amount;
} else {
return 0;
}
}








//find_recognized_revenue->ends

function find_recognized_revenue_prev(a_revenue_recognized_for_project,practice,subpractice,role,level,month,year,a_prev_subprac_searched_once)
{
	var f_recognized_amount = 0;
	
	if(a_prev_subprac_searched_once.indexOf(subpractice) < 0)
	{
		for(var i_index_find=0; i_index_find<a_revenue_recognized_for_project.length; i_index_find++)
		{
			if(practice == a_revenue_recognized_for_project[i_index_find].practice_revenue_recognized)
			{
				if(subpractice == a_revenue_recognized_for_project[i_index_find].subpractice_revenue_recognized)
				{
					//if(role == a_revenue_recognized_for_project[i_index_find].role_revenue_recognized)
					{
						//if(level == a_revenue_recognized_for_project[i_index_find].level_revenue_recognized)
						{
							if(month == a_revenue_recognized_for_project[i_index_find].month_revenue_recognized)
							{
								if(year == a_revenue_recognized_for_project[i_index_find].year_revenue_recognized)
								{
									f_recognized_amount = a_revenue_recognized_for_project[i_index_find].amount_revenue_recognized;
									//nlapiLogExecution('audit','f_recognized_amount:- '+f_recognized_amount);
									a_prev_subprac_searched_once.push(subpractice);
								}
							}
						}
					}
				}
			}
		}
		
		return f_recognized_amount;
	}
	else
	{
		return 0;
	}
}
function getMargin_f(i_projectId, i_year_project, s_currency_symbol_proj,
		d_proj_start_date, d_proj_end_date, i_project_mnth,
		s_currency_symbol_usd, i_prject_end_mnth, i_year_project_end) {
	var sr_no = 0;
	var bill_data_arr = new Array();
	nlapiYieldScript();
	try {
		var a_revenue_cap_filters = [ [ 'custrecord_fp_rev_rec_others_projec',
				'anyof', parseInt(i_projectId) ] ];

		var a_column = new Array();
		a_column[0] = new nlobjSearchColumn('created').setSort(true);
		a_column[1] = new nlobjSearchColumn(
				'custrecord_fp_rev_rec_others_auto_numb');
		a_column[2] = new nlobjSearchColumn(
				'custrecord_revenue_other_approval_status');

		var a_get_logged_in_user_exsiting_revenue_cap = nlapiSearchRecord(
				'customrecord_fp_rev_rec_others_parent', null,
				a_revenue_cap_filters, a_column);
		if (a_get_logged_in_user_exsiting_revenue_cap) {
			var i_revenue_share_id = a_get_logged_in_user_exsiting_revenue_cap[0]
					.getId();
			var i_revenue_share_stat = a_get_logged_in_user_exsiting_revenue_cap[0]
					.getValue('custrecord_revenue_other_approval_status');
		}

		var a_effort_activity_mnth_end_filter = [ [
				'custrecord_fp_others_mnth_end_fp_parent', 'anyof',
				parseInt(i_revenue_share_id) ] ];

		var a_columns_mnth_end_effort_activity_srch = new Array();
		a_columns_mnth_end_effort_activity_srch[0] = new nlobjSearchColumn(
				'created').setSort(true);

		var a_get_mnth_end_effrt_activity = nlapiSearchRecord(
				'customrecord_fp_others_mnth_end_json', null,
				a_effort_activity_mnth_end_filter,
				a_columns_mnth_end_effort_activity_srch);
		if (a_get_mnth_end_effrt_activity) {
			var i_mnth_end_json_id = a_get_mnth_end_effrt_activity[0].getId();
		}

		var projectWiseRevenue = [];

		var a_revenue_recognized_for_project = new Array();
		var a_filter_get_ytd_revenue_recognized = [ [
				'custrecord_project_to_recognize_amount', 'anyof', i_projectId ] ];

		var a_columns_get_ytd_revenue_recognized = new Array();
		a_columns_get_ytd_revenue_recognized[0] = new nlobjSearchColumn(
				'custrecord_project_to_recognize_amount');
		a_columns_get_ytd_revenue_recognized[1] = new nlobjSearchColumn(
				'custrecord_practice_to_recognize_amount');
		a_columns_get_ytd_revenue_recognized[2] = new nlobjSearchColumn(
				'custrecord_subparctice_to_recognize_amnt');
		a_columns_get_ytd_revenue_recognized[3] = new nlobjSearchColumn(
				'custrecord_role_to_recognize_amount');
		a_columns_get_ytd_revenue_recognized[4] = new nlobjSearchColumn(
				'custrecord_level_to_recognize_amount');
		a_columns_get_ytd_revenue_recognized[5] = new nlobjSearchColumn(
				'custrecord_revenue_recognized');
		a_columns_get_ytd_revenue_recognized[6] = new nlobjSearchColumn(
				'custrecord_month_to_recognize_amount');
		a_columns_get_ytd_revenue_recognized[7] = new nlobjSearchColumn(
				'custrecord_year_to_recognize_amount');

		var a_get_ytd_revenue = nlapiSearchRecord(
				'customrecord_fp_rev_rec_recognized_amnt', null,
				a_filter_get_ytd_revenue_recognized,
				a_columns_get_ytd_revenue_recognized);
		if (a_get_ytd_revenue) {
			nlapiLogExecution('audit', 'recognized revenue for project found');
			for (var i_revenue_index = 0; i_revenue_index < a_get_ytd_revenue.length; i_revenue_index++) {
				//nlapiLogExecution('audit','recognized amnt:- ',a_get_ytd_revenue[i_revenue_index].getValue('custrecord_month_to_recognize_amount'));
				a_revenue_recognized_for_project
						.push({
							'practice_revenue_recognized' : a_get_ytd_revenue[i_revenue_index]
									.getValue('custrecord_practice_to_recognize_amount'),
							'subpractice_revenue_recognized' : a_get_ytd_revenue[i_revenue_index]
									.getValue('custrecord_subparctice_to_recognize_amnt'),
							'role_revenue_recognized' : a_get_ytd_revenue[i_revenue_index]
									.getValue('custrecord_role_to_recognize_amount'),
							'level_revenue_recognized' : a_get_ytd_revenue[i_revenue_index]
									.getValue('custrecord_level_to_recognize_amount'),
							'amount_revenue_recognized' : a_get_ytd_revenue[i_revenue_index]
									.getValue('custrecord_revenue_recognized'),
							'month_revenue_recognized' : a_get_ytd_revenue[i_revenue_index]
									.getValue('custrecord_month_to_recognize_amount'),
							'year_revenue_recognized' : a_get_ytd_revenue[i_revenue_index]
									.getValue('custrecord_year_to_recognize_amount')
						});
			}
		}

		i_projectId = i_projectId.trim();

		var o_project_object = nlapiLoadRecord('job', i_projectId); // load project record object
		// get necessary information about project from project record
		var s_project_region = o_project_object
				.getFieldValue('custentity_region');
		var i_customer_name = o_project_object.getFieldValue('parent');
		var s_project_name = o_project_object.getFieldValue('companyname');
		var d_proj_start_date = o_project_object.getFieldValue('startdate');
		var d_proj_strt_date_old_proj = o_project_object
				.getFieldValue('custentity_rev_rec_new_strt_date');
		var d_proj_end_date = o_project_object.getFieldValue('enddate');
		var i_proj_executing_practice = o_project_object
				.getFieldValue('custentity_practice');
		var i_proj_manager = o_project_object
				.getFieldValue('custentity_projectmanager');
		var i_proj_manager_practice = nlapiLookupField('employee',
				i_proj_manager, 'department');
		var i_project_sow_value = o_project_object
				.getFieldValue('custentity_projectvalue');
		var i_proj_revenue_rec_type = o_project_object
				.getFieldValue('custentity_fp_rev_rec_type');
		var s_proj_currency = o_project_object
				.getFieldText('custentity_project_currency');

		if (d_proj_strt_date_old_proj) {
			d_proj_start_date = d_proj_strt_date_old_proj;
		}

		var monthBreakUp = getMonthsBreakup(
				nlapiStringToDate(d_proj_start_date),
				nlapiStringToDate(d_proj_end_date));

		var d_pro_strtDate = nlapiStringToDate(d_proj_start_date);
		var i_year_project = d_pro_strtDate.getFullYear();

		var s_pro_endDate = nlapiStringToDate(d_proj_end_date);
		var i_year_project_end = s_pro_endDate.getFullYear();

		var i_project_mnth = d_pro_strtDate.getMonth();
		var i_prject_end_mnth = s_pro_endDate.getMonth();

		var s_currency_symbol_proj = getCurrency_Symbol(s_proj_currency);
		var s_currency_symbol_usd = getCurrency_Symbol('USD');

		// get previous mnth effrt for true up and true down scenario
		var d_today_date = new Date();
		var i_current_mnth = d_today_date.getMonth();
		var i_current_year = d_today_date.getFullYear();

		var i_prev_month = parseFloat(i_current_mnth) - parseFloat(1);

		var a_filter_get_prev_mnth_effrt = [
				[ 'custrecord_fp_others_mnth_end_fp_parent', 'anyof',
						parseInt(i_revenue_share_id) ],
				'and',
				[ 'custrecord_other_current_mnth_no', 'equalto',
						parseInt(i_prev_month) ] ];

		var a_columns_get_prev_mnth_effrt = new Array();
		a_columns_get_prev_mnth_effrt[0] = new nlobjSearchColumn('created')
				.setSort(true);

		var a_get_prev_month_effrt = nlapiSearchRecord(
				'customrecord_fp_others_mnth_end_json', null,
				a_filter_get_prev_mnth_effrt, a_columns_get_prev_mnth_effrt);
		if (a_get_prev_month_effrt) {
			nlapiLogExecution('debug', 'prev mnth effrt json id:- '
					+ a_get_prev_month_effrt[0].getId());

			var projectWiseRevenue_previous_effrt = [];

			var a_prev_subprac_searched_once = new Array();

			var o_mnth_end_effrt_prev_mnth = nlapiLoadRecord(
					'customrecord_fp_others_mnth_end_json',
					a_get_prev_month_effrt[0].getId());

			var s_effrt_json_1_prev_mnth = o_mnth_end_effrt_prev_mnth
					.getFieldValue('custrecord_fp_others_mnth_end_fld_json1');
			if (s_effrt_json_1_prev_mnth) {
				//generate_previous_effrt_revenue_f(s_effrt_json_1_prev_mnth,projectWiseRevenue_previous_effrt,a_revenue_recognized_for_project,i_year_project);

				{
					var i_practice_previous = 0;
					var f_total_revenue_for_tenure = 0;
					//for (var i_mnth_end_plan = 0; i_mnth_end_plan < a_get_mnth_end_effrt_plan.length; i_mnth_end_plan++)
					var s_entire_json_clubed = JSON
							.parse(s_effrt_json_1_prev_mnth);
					for (var i_mnth_end_plan = 0; i_mnth_end_plan < s_entire_json_clubed.length; i_mnth_end_plan++) {
						var a_row_json_data = JSON
								.parse(JSON
										.stringify(s_entire_json_clubed[i_mnth_end_plan]));
						for (var i_row_json_index = 0; i_row_json_index < a_row_json_data.length; i_row_json_index++) {
							var i_practice = a_row_json_data[i_row_json_index].prac;
							var s_practice = a_row_json_data[i_row_json_index].prac_text;
							var i_sub_practice = a_row_json_data[i_row_json_index].subprac;
							var s_sub_practice = a_row_json_data[i_row_json_index].subprac_text;
							var i_role = a_row_json_data[i_row_json_index].role;
							var s_role = a_row_json_data[i_row_json_index].role_text;
							var i_level = a_row_json_data[i_row_json_index].level;
							var s_level = a_row_json_data[i_row_json_index].level_text;
							var i_location = a_row_json_data[i_row_json_index].loc;
							var s_location = a_row_json_data[i_row_json_index].loc_text;
							var f_revenue = a_row_json_data[i_row_json_index].cost;
							if (!f_revenue)
								f_revenue = 0;

							var f_revenue_share = a_row_json_data[i_row_json_index].share;
							if (!f_revenue_share)
								f_revenue_share = 0;

							var i_no_of_resources = a_row_json_data[i_row_json_index].allo;
							var s_mnth_strt_date = '1/31/2017';
							var s_mnth_end_date = '31/31/2017';
							var s_mnth = a_row_json_data[i_row_json_index].mnth;
							var s_year = a_row_json_data[i_row_json_index].year;

							var f_revenue_recognized = find_recognized_revenue_prev_f(
									a_revenue_recognized_for_project,
									i_practice, i_sub_practice, i_role,
									i_level, s_mnth, s_year,
									a_prev_subprac_searched_once);

							if (!f_revenue_recognized)
								f_revenue_recognized = 0;

							//nlapiLogExecution('audit','f_revenue_recognized:- '+f_revenue_recognized);

							var s_month_name = s_mnth + '_' + s_year;

							var total_revenue = parseFloat(i_no_of_resources)
									* parseFloat(f_revenue);
							total_revenue = parseFloat(total_revenue)
									.toFixed(2);

							if (i_practice_previous == 0) {
								i_practice_previous = a_row_json_data[i_row_json_index].prac_row;
							}

							if (i_practice_previous != a_row_json_data[i_row_json_index].prac_row) {
								f_total_revenue_for_tenure = 0;
								i_practice_previous = a_row_json_data[i_row_json_index].prac_row;
							}

							f_total_revenue_for_tenure = parseFloat(f_total_revenue_for_tenure)
									+ parseFloat(total_revenue);
							f_total_revenue_for_tenure = parseFloat(
									f_total_revenue_for_tenure).toFixed(2);

							var i_prac_subPrac_role_level = i_practice + '_'
									+ i_sub_practice + '_' + i_role + '_'
									+ i_level + '_' + i_location;

							if (!projectWiseRevenue_previous_effrt[i_prac_subPrac_role_level]) {
								projectWiseRevenue_previous_effrt[i_prac_subPrac_role_level] = {
									practice_name : s_practice,
									sub_prac_name : s_sub_practice,
									sub_practice : i_sub_practice,
									role_name : s_role,
									level_name : s_level,
									location_name : s_location,
									total_revenue_for_tenure : f_total_revenue_for_tenure,
									revenue_share : f_revenue_share,
									RevenueData : {}
								};

								projectWiseRevenue_previous_effrt[i_prac_subPrac_role_level].RevenueData[i_practice] = new yearData(
										i_year_project);

								projectWiseRevenue_previous_effrt[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].Amount = total_revenue;
								projectWiseRevenue_previous_effrt[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].prcnt_allocated = i_no_of_resources;
								projectWiseRevenue_previous_effrt[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].recognized_revenue = f_revenue_recognized;
								projectWiseRevenue_previous_effrt[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].mnth_strt = s_mnth_strt_date;
								projectWiseRevenue_previous_effrt[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].mnth_end = s_mnth_end_date;
							} else {
								projectWiseRevenue_previous_effrt[i_prac_subPrac_role_level].total_revenue_for_tenure = f_total_revenue_for_tenure;

								projectWiseRevenue_previous_effrt[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].Amount = total_revenue;
								projectWiseRevenue_previous_effrt[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].prcnt_allocated = i_no_of_resources;
								projectWiseRevenue_previous_effrt[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].recognized_revenue = f_revenue_recognized;
								projectWiseRevenue_previous_effrt[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].mnth_strt = s_mnth_strt_date;
								projectWiseRevenue_previous_effrt[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].mnth_end = s_mnth_end_date;
							}
						}
					}
				}
			}

			var s_effrt_json_2_prev_mnth = o_mnth_end_effrt_prev_mnth
					.getFieldValue('custrecord_fp_others_mnth_end_fld_json2');
			if (s_effrt_json_2_prev_mnth) {
				//generate_previous_effrt_revenue_f(s_effrt_json_2_prev_mnth,projectWiseRevenue_previous_effrt,a_revenue_recognized_for_project,i_year_project);

				{
					var i_practice_previous = 0;
					var f_total_revenue_for_tenure = 0;
					//for (var i_mnth_end_plan = 0; i_mnth_end_plan < a_get_mnth_end_effrt_plan.length; i_mnth_end_plan++)
					var s_entire_json_clubed = JSON
							.parse(s_effrt_json_2_prev_mnth);
					for (var i_mnth_end_plan = 0; i_mnth_end_plan < s_entire_json_clubed.length; i_mnth_end_plan++) {
						var a_row_json_data = JSON
								.parse(JSON
										.stringify(s_entire_json_clubed[i_mnth_end_plan]));
						for (var i_row_json_index = 0; i_row_json_index < a_row_json_data.length; i_row_json_index++) {
							var i_practice = a_row_json_data[i_row_json_index].prac;
							var s_practice = a_row_json_data[i_row_json_index].prac_text;
							var i_sub_practice = a_row_json_data[i_row_json_index].subprac;
							var s_sub_practice = a_row_json_data[i_row_json_index].subprac_text;
							var i_role = a_row_json_data[i_row_json_index].role;
							var s_role = a_row_json_data[i_row_json_index].role_text;
							var i_level = a_row_json_data[i_row_json_index].level;
							var s_level = a_row_json_data[i_row_json_index].level_text;
							var i_location = a_row_json_data[i_row_json_index].loc;
							var s_location = a_row_json_data[i_row_json_index].loc_text;
							var f_revenue = a_row_json_data[i_row_json_index].cost;
							if (!f_revenue)
								f_revenue = 0;

							var f_revenue_share = a_row_json_data[i_row_json_index].share;
							if (!f_revenue_share)
								f_revenue_share = 0;

							var i_no_of_resources = a_row_json_data[i_row_json_index].allo;
							var s_mnth_strt_date = '1/31/2017';
							var s_mnth_end_date = '31/31/2017';
							var s_mnth = a_row_json_data[i_row_json_index].mnth;
							var s_year = a_row_json_data[i_row_json_index].year;

							var f_revenue_recognized = find_recognized_revenue_prev_f(
									a_revenue_recognized_for_project,
									i_practice, i_sub_practice, i_role,
									i_level, s_mnth, s_year,
									a_prev_subprac_searched_once);

							if (!f_revenue_recognized)
								f_revenue_recognized = 0;

							//nlapiLogExecution('audit','f_revenue_recognized:- '+f_revenue_recognized);

							var s_month_name = s_mnth + '_' + s_year;

							var total_revenue = parseFloat(i_no_of_resources)
									* parseFloat(f_revenue);
							total_revenue = parseFloat(total_revenue)
									.toFixed(2);

							if (i_practice_previous == 0) {
								i_practice_previous = a_row_json_data[i_row_json_index].prac_row;
							}

							if (i_practice_previous != a_row_json_data[i_row_json_index].prac_row) {
								f_total_revenue_for_tenure = 0;
								i_practice_previous = a_row_json_data[i_row_json_index].prac_row;
							}

							f_total_revenue_for_tenure = parseFloat(f_total_revenue_for_tenure)
									+ parseFloat(total_revenue);
							f_total_revenue_for_tenure = parseFloat(
									f_total_revenue_for_tenure).toFixed(2);

							var i_prac_subPrac_role_level = i_practice + '_'
									+ i_sub_practice + '_' + i_role + '_'
									+ i_level + '_' + i_location;

							if (!projectWiseRevenue_previous_effrt[i_prac_subPrac_role_level]) {
								projectWiseRevenue_previous_effrt[i_prac_subPrac_role_level] = {
									practice_name : s_practice,
									sub_prac_name : s_sub_practice,
									sub_practice : i_sub_practice,
									role_name : s_role,
									level_name : s_level,
									location_name : s_location,
									total_revenue_for_tenure : f_total_revenue_for_tenure,
									revenue_share : f_revenue_share,
									RevenueData : {}
								};

								projectWiseRevenue_previous_effrt[i_prac_subPrac_role_level].RevenueData[i_practice] = new yearData(
										i_year_project);

								projectWiseRevenue_previous_effrt[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].Amount = total_revenue;
								projectWiseRevenue_previous_effrt[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].prcnt_allocated = i_no_of_resources;
								projectWiseRevenue_previous_effrt[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].recognized_revenue = f_revenue_recognized;
								projectWiseRevenue_previous_effrt[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].mnth_strt = s_mnth_strt_date;
								projectWiseRevenue_previous_effrt[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].mnth_end = s_mnth_end_date;
							} else {
								projectWiseRevenue_previous_effrt[i_prac_subPrac_role_level].total_revenue_for_tenure = f_total_revenue_for_tenure;

								projectWiseRevenue_previous_effrt[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].Amount = total_revenue;
								projectWiseRevenue_previous_effrt[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].prcnt_allocated = i_no_of_resources;
								projectWiseRevenue_previous_effrt[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].recognized_revenue = f_revenue_recognized;
								projectWiseRevenue_previous_effrt[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].mnth_strt = s_mnth_strt_date;
								projectWiseRevenue_previous_effrt[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].mnth_end = s_mnth_end_date;
							}
						}
					}
				}
			}

			var s_effrt_json_3_prev_mnth = o_mnth_end_effrt_prev_mnth
					.getFieldValue('custrecord_fp_others_mnth_end_fld_json3');
			if (s_effrt_json_3_prev_mnth) {
				//generate_previous_effrt_revenue_f(s_effrt_json_3_prev_mnth,projectWiseRevenue_previous_effrt,a_revenue_recognized_for_project,i_year_project);

				{
					var i_practice_previous = 0;
					var f_total_revenue_for_tenure = 0;
					//for (var i_mnth_end_plan = 0; i_mnth_end_plan < a_get_mnth_end_effrt_plan.length; i_mnth_end_plan++)
					var s_entire_json_clubed = JSON
							.parse(s_effrt_json_3_prev_mnth);
					for (var i_mnth_end_plan = 0; i_mnth_end_plan < s_entire_json_clubed.length; i_mnth_end_plan++) {
						var a_row_json_data = JSON
								.parse(JSON
										.stringify(s_entire_json_clubed[i_mnth_end_plan]));
						for (var i_row_json_index = 0; i_row_json_index < a_row_json_data.length; i_row_json_index++) {
							var i_practice = a_row_json_data[i_row_json_index].prac;
							var s_practice = a_row_json_data[i_row_json_index].prac_text;
							var i_sub_practice = a_row_json_data[i_row_json_index].subprac;
							var s_sub_practice = a_row_json_data[i_row_json_index].subprac_text;
							var i_role = a_row_json_data[i_row_json_index].role;
							var s_role = a_row_json_data[i_row_json_index].role_text;
							var i_level = a_row_json_data[i_row_json_index].level;
							var s_level = a_row_json_data[i_row_json_index].level_text;
							var i_location = a_row_json_data[i_row_json_index].loc;
							var s_location = a_row_json_data[i_row_json_index].loc_text;
							var f_revenue = a_row_json_data[i_row_json_index].cost;
							if (!f_revenue)
								f_revenue = 0;

							var f_revenue_share = a_row_json_data[i_row_json_index].share;
							if (!f_revenue_share)
								f_revenue_share = 0;

							var i_no_of_resources = a_row_json_data[i_row_json_index].allo;
							var s_mnth_strt_date = '1/31/2017';
							var s_mnth_end_date = '31/31/2017';
							var s_mnth = a_row_json_data[i_row_json_index].mnth;
							var s_year = a_row_json_data[i_row_json_index].year;

							var f_revenue_recognized = find_recognized_revenue_prev_f(
									a_revenue_recognized_for_project,
									i_practice, i_sub_practice, i_role,
									i_level, s_mnth, s_year,
									a_prev_subprac_searched_once);

							if (!f_revenue_recognized)
								f_revenue_recognized = 0;

							//nlapiLogExecution('audit','f_revenue_recognized:- '+f_revenue_recognized);

							var s_month_name = s_mnth + '_' + s_year;

							var total_revenue = parseFloat(i_no_of_resources)
									* parseFloat(f_revenue);
							total_revenue = parseFloat(total_revenue)
									.toFixed(2);

							if (i_practice_previous == 0) {
								i_practice_previous = a_row_json_data[i_row_json_index].prac_row;
							}

							if (i_practice_previous != a_row_json_data[i_row_json_index].prac_row) {
								f_total_revenue_for_tenure = 0;
								i_practice_previous = a_row_json_data[i_row_json_index].prac_row;
							}

							f_total_revenue_for_tenure = parseFloat(f_total_revenue_for_tenure)
									+ parseFloat(total_revenue);
							f_total_revenue_for_tenure = parseFloat(
									f_total_revenue_for_tenure).toFixed(2);

							var i_prac_subPrac_role_level = i_practice + '_'
									+ i_sub_practice + '_' + i_role + '_'
									+ i_level + '_' + i_location;

							if (!projectWiseRevenue_previous_effrt[i_prac_subPrac_role_level]) {
								projectWiseRevenue_previous_effrt[i_prac_subPrac_role_level] = {
									practice_name : s_practice,
									sub_prac_name : s_sub_practice,
									sub_practice : i_sub_practice,
									role_name : s_role,
									level_name : s_level,
									location_name : s_location,
									total_revenue_for_tenure : f_total_revenue_for_tenure,
									revenue_share : f_revenue_share,
									RevenueData : {}
								};

								projectWiseRevenue_previous_effrt[i_prac_subPrac_role_level].RevenueData[i_practice] = new yearData(
										i_year_project);

								projectWiseRevenue_previous_effrt[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].Amount = total_revenue;
								projectWiseRevenue_previous_effrt[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].prcnt_allocated = i_no_of_resources;
								projectWiseRevenue_previous_effrt[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].recognized_revenue = f_revenue_recognized;
								projectWiseRevenue_previous_effrt[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].mnth_strt = s_mnth_strt_date;
								projectWiseRevenue_previous_effrt[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].mnth_end = s_mnth_end_date;
							} else {
								projectWiseRevenue_previous_effrt[i_prac_subPrac_role_level].total_revenue_for_tenure = f_total_revenue_for_tenure;

								projectWiseRevenue_previous_effrt[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].Amount = total_revenue;
								projectWiseRevenue_previous_effrt[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].prcnt_allocated = i_no_of_resources;
								projectWiseRevenue_previous_effrt[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].recognized_revenue = f_revenue_recognized;
								projectWiseRevenue_previous_effrt[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].mnth_strt = s_mnth_strt_date;
								projectWiseRevenue_previous_effrt[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].mnth_end = s_mnth_end_date;
							}
						}
					}
				}
			}

			var s_effrt_json_4_prev_mnth = o_mnth_end_effrt_prev_mnth
					.getFieldValue('custrecord_fp_others_mnth_end_fld_json4');
			if (s_effrt_json_4_prev_mnth) {
				//generate_previous_effrt_revenue_f(s_effrt_json_4_prev_mnth,projectWiseRevenue_previous_effrt,a_revenue_recognized_for_project,i_year_project);

				{
					var i_practice_previous = 0;
					var f_total_revenue_for_tenure = 0;
					//for (var i_mnth_end_plan = 0; i_mnth_end_plan < a_get_mnth_end_effrt_plan.length; i_mnth_end_plan++)
					var s_entire_json_clubed = JSON
							.parse(s_effrt_json_4_prev_mnth);
					for (var i_mnth_end_plan = 0; i_mnth_end_plan < s_entire_json_clubed.length; i_mnth_end_plan++) {
						var a_row_json_data = JSON
								.parse(JSON
										.stringify(s_entire_json_clubed[i_mnth_end_plan]));
						for (var i_row_json_index = 0; i_row_json_index < a_row_json_data.length; i_row_json_index++) {
							var i_practice = a_row_json_data[i_row_json_index].prac;
							var s_practice = a_row_json_data[i_row_json_index].prac_text;
							var i_sub_practice = a_row_json_data[i_row_json_index].subprac;
							var s_sub_practice = a_row_json_data[i_row_json_index].subprac_text;
							var i_role = a_row_json_data[i_row_json_index].role;
							var s_role = a_row_json_data[i_row_json_index].role_text;
							var i_level = a_row_json_data[i_row_json_index].level;
							var s_level = a_row_json_data[i_row_json_index].level_text;
							var i_location = a_row_json_data[i_row_json_index].loc;
							var s_location = a_row_json_data[i_row_json_index].loc_text;
							var f_revenue = a_row_json_data[i_row_json_index].cost;
							if (!f_revenue)
								f_revenue = 0;

							var f_revenue_share = a_row_json_data[i_row_json_index].share;
							if (!f_revenue_share)
								f_revenue_share = 0;

							var i_no_of_resources = a_row_json_data[i_row_json_index].allo;
							var s_mnth_strt_date = '1/31/2017';
							var s_mnth_end_date = '31/31/2017';
							var s_mnth = a_row_json_data[i_row_json_index].mnth;
							var s_year = a_row_json_data[i_row_json_index].year;

							var f_revenue_recognized = find_recognized_revenue_prev_f(
									a_revenue_recognized_for_project,
									i_practice, i_sub_practice, i_role,
									i_level, s_mnth, s_year,
									a_prev_subprac_searched_once);

							if (!f_revenue_recognized)
								f_revenue_recognized = 0;

							//nlapiLogExecution('audit','f_revenue_recognized:- '+f_revenue_recognized);

							var s_month_name = s_mnth + '_' + s_year;

							var total_revenue = parseFloat(i_no_of_resources)
									* parseFloat(f_revenue);
							total_revenue = parseFloat(total_revenue)
									.toFixed(2);

							if (i_practice_previous == 0) {
								i_practice_previous = a_row_json_data[i_row_json_index].prac_row;
							}

							if (i_practice_previous != a_row_json_data[i_row_json_index].prac_row) {
								f_total_revenue_for_tenure = 0;
								i_practice_previous = a_row_json_data[i_row_json_index].prac_row;
							}

							f_total_revenue_for_tenure = parseFloat(f_total_revenue_for_tenure)
									+ parseFloat(total_revenue);
							f_total_revenue_for_tenure = parseFloat(
									f_total_revenue_for_tenure).toFixed(2);

							var i_prac_subPrac_role_level = i_practice + '_'
									+ i_sub_practice + '_' + i_role + '_'
									+ i_level + '_' + i_location;

							if (!projectWiseRevenue_previous_effrt[i_prac_subPrac_role_level]) {
								projectWiseRevenue_previous_effrt[i_prac_subPrac_role_level] = {
									practice_name : s_practice,
									sub_prac_name : s_sub_practice,
									sub_practice : i_sub_practice,
									role_name : s_role,
									level_name : s_level,
									location_name : s_location,
									total_revenue_for_tenure : f_total_revenue_for_tenure,
									revenue_share : f_revenue_share,
									RevenueData : {}
								};

								projectWiseRevenue_previous_effrt[i_prac_subPrac_role_level].RevenueData[i_practice] = new yearData(
										i_year_project);

								projectWiseRevenue_previous_effrt[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].Amount = total_revenue;
								projectWiseRevenue_previous_effrt[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].prcnt_allocated = i_no_of_resources;
								projectWiseRevenue_previous_effrt[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].recognized_revenue = f_revenue_recognized;
								projectWiseRevenue_previous_effrt[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].mnth_strt = s_mnth_strt_date;
								projectWiseRevenue_previous_effrt[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].mnth_end = s_mnth_end_date;
							} else {
								projectWiseRevenue_previous_effrt[i_prac_subPrac_role_level].total_revenue_for_tenure = f_total_revenue_for_tenure;

								projectWiseRevenue_previous_effrt[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].Amount = total_revenue;
								projectWiseRevenue_previous_effrt[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].prcnt_allocated = i_no_of_resources;
								projectWiseRevenue_previous_effrt[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].recognized_revenue = f_revenue_recognized;
								projectWiseRevenue_previous_effrt[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].mnth_strt = s_mnth_strt_date;
								projectWiseRevenue_previous_effrt[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].mnth_end = s_mnth_end_date;
							}
						}
					}
				}
			}
		}

		if (i_mnth_end_json_id) {
			var a_total_effort_json = [];

			var o_mnth_end_effrt = nlapiLoadRecord(
					'customrecord_fp_others_mnth_end_json', i_mnth_end_json_id);

			var s_effrt_json = o_mnth_end_effrt
					.getFieldValue('custrecord_fp_others_mnth_end_fld_json1');
			//s_effrt_json = JSON.stringify(s_effrt_json);
			var s_effrt_json_2 = o_mnth_end_effrt
					.getFieldValue('custrecord_fp_others_mnth_end_fld_json2');
			//s_effrt_json_2 = JSON.stringify(s_effrt_json_2);
			var s_effrt_json_3 = o_mnth_end_effrt
					.getFieldValue('custrecord_fp_others_mnth_end_fld_json3');
			var s_effrt_json_4 = o_mnth_end_effrt
					.getFieldValue('custrecord_fp_others_mnth_end_fld_json4');

			var a_duplicate_sub_prac_count = new Array();
			var a_unique_list_practice_sublist = new Array();

			var a_subprac_searched_once = new Array();
			var a_subprac_searched_once_month = new Array();
			var a_subprac_searched_once_year = new Array();

			if (s_effrt_json) {
				a_total_effort_json.push(s_effrt_json);

				var i_practice_previous = 0;
				var f_total_revenue_for_tenure = 0;
				//for (var i_mnth_end_plan = 0; i_mnth_end_plan < a_get_mnth_end_effrt_plan.length; i_mnth_end_plan++)
				var s_entire_json_clubed = JSON.parse(s_effrt_json);
				for (var i_mnth_end_plan = 0; i_mnth_end_plan < s_entire_json_clubed.length; i_mnth_end_plan++) {
					var a_row_json_data = JSON.parse(JSON
							.stringify(s_entire_json_clubed[i_mnth_end_plan]));
					for (var i_row_json_index = 0; i_row_json_index < a_row_json_data.length; i_row_json_index++) {
						var i_practice = a_row_json_data[i_row_json_index].prac;
						var s_practice = a_row_json_data[i_row_json_index].prac_text;
						var i_sub_practice = a_row_json_data[i_row_json_index].subprac;
						var s_sub_practice = a_row_json_data[i_row_json_index].subprac_text;
						var i_role = a_row_json_data[i_row_json_index].role;
						var s_role = a_row_json_data[i_row_json_index].role_text;
						var i_level = a_row_json_data[i_row_json_index].level;
						var s_level = a_row_json_data[i_row_json_index].level_text;
						var i_location = a_row_json_data[i_row_json_index].loc;
						var s_location = a_row_json_data[i_row_json_index].loc_text;
						var f_revenue = a_row_json_data[i_row_json_index].cost;
						if (!f_revenue)
							f_revenue = 0;

						var f_revenue_share = a_row_json_data[i_row_json_index].share;
						if (!f_revenue_share)
							f_revenue_share = 0;

						var i_no_of_resources = a_row_json_data[i_row_json_index].allo;
						var s_mnth_strt_date = '1/31/2017';
						var s_mnth_end_date = '31/31/2017';
						var s_mnth = a_row_json_data[i_row_json_index].mnth;
						var s_year = a_row_json_data[i_row_json_index].year;

						var f_revenue_recognized = 0;

						if (a_unique_list_practice_sublist
								.indexOf(a_row_json_data[i_row_json_index].subprac) < 0) {
							a_unique_list_practice_sublist
									.push(a_row_json_data[i_row_json_index].subprac);
							a_duplicate_sub_prac_count
									.push({
										'sub_prac' : a_row_json_data[i_row_json_index].subprac,
										'sub_prac_name' : a_row_json_data[i_row_json_index].subprac_text,
										'revenue_share' : 50000,
										'no_count' : 0
									});

						}

						f_revenue_recognized = find_recognized_revenue_f(
								a_revenue_recognized_for_project, i_practice,
								i_sub_practice, i_role, i_level, s_mnth,
								s_year, a_subprac_searched_once,
								a_subprac_searched_once_month,
								a_subprac_searched_once_year);

						if (!f_revenue_recognized)
							f_revenue_recognized = 0;

						var s_month_name = s_mnth + '_' + s_year;

						var total_revenue = parseFloat(i_no_of_resources)
								* parseFloat(f_revenue);
						total_revenue = parseFloat(total_revenue).toFixed(2);


						if (i_practice_previous == 0) {
							i_practice_previous = a_row_json_data[i_row_json_index].prac_row;
						}

						if (i_practice_previous != a_row_json_data[i_row_json_index].prac_row) {
							f_total_revenue_for_tenure = 0;
							i_practice_previous = a_row_json_data[i_row_json_index].prac_row;
						}

						f_total_revenue_for_tenure = parseFloat(f_total_revenue_for_tenure)
								+ parseFloat(total_revenue);
						f_total_revenue_for_tenure = parseFloat(
								f_total_revenue_for_tenure).toFixed(2);

						var i_prac_subPrac_role_level = i_practice + '_'
								+ i_sub_practice + '_' + i_role + '_' + i_level
								+ '_' + i_location;

						if (!projectWiseRevenue[i_prac_subPrac_role_level]) {
							projectWiseRevenue[i_prac_subPrac_role_level] = {
								practice_name : s_practice,
								sub_prac_name : s_sub_practice,
								sub_practice : i_sub_practice,
								role_name : s_role,
								level_name : s_level,
								location_name : s_location,
								total_revenue_for_tenure : f_total_revenue_for_tenure,
								//revenue_share: f_revenue_share,
								RevenueData : {}
							};

							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice] = new yearData(
									i_year_project);

							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].Amount = total_revenue;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].prcnt_allocated = i_no_of_resources;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].recognized_revenue = f_revenue_recognized;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].mnth_strt = s_mnth_strt_date;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].mnth_end = s_mnth_end_date;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].revenue_share = f_revenue_share;
						} else {
							projectWiseRevenue[i_prac_subPrac_role_level].total_revenue_for_tenure = f_total_revenue_for_tenure;

							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].Amount = total_revenue;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].prcnt_allocated = i_no_of_resources;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].recognized_revenue = f_revenue_recognized;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].mnth_strt = s_mnth_strt_date;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].mnth_end = s_mnth_end_date;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].revenue_share = f_revenue_share;
						}
					}
				}
			}

			if (s_effrt_json_2) {
				var i_practice_previous = 0;
				var f_total_revenue_for_tenure = 0;
				//for (var i_mnth_end_plan = 0; i_mnth_end_plan < a_get_mnth_end_effrt_plan.length; i_mnth_end_plan++)
				var s_entire_json_clubed = JSON.parse(s_effrt_json_2);
				for (var i_mnth_end_plan = 0; i_mnth_end_plan < s_entire_json_clubed.length; i_mnth_end_plan++) {
					var a_row_json_data = JSON.parse(JSON
							.stringify(s_entire_json_clubed[i_mnth_end_plan]));
					for (var i_row_json_index = 0; i_row_json_index < a_row_json_data.length; i_row_json_index++) {
						var i_practice = a_row_json_data[i_row_json_index].prac;
						var s_practice = a_row_json_data[i_row_json_index].prac_text;
						var i_sub_practice = a_row_json_data[i_row_json_index].subprac;
						var s_sub_practice = a_row_json_data[i_row_json_index].subprac_text;
						var i_role = a_row_json_data[i_row_json_index].role;
						var s_role = a_row_json_data[i_row_json_index].role_text;
						var i_level = a_row_json_data[i_row_json_index].level;
						var s_level = a_row_json_data[i_row_json_index].level_text;
						var i_location = a_row_json_data[i_row_json_index].loc;
						var s_location = a_row_json_data[i_row_json_index].loc_text;
						var f_revenue = a_row_json_data[i_row_json_index].cost;
						if (!f_revenue)
							f_revenue = 0;

						var f_revenue_share = a_row_json_data[i_row_json_index].share;
						if (!f_revenue_share)
							f_revenue_share = 0;

						var i_no_of_resources = a_row_json_data[i_row_json_index].allo;
						var s_mnth_strt_date = '1/31/2017';
						var s_mnth_end_date = '31/31/2017';
						var s_mnth = a_row_json_data[i_row_json_index].mnth;
						var s_year = a_row_json_data[i_row_json_index].year;

						var f_revenue_recognized = 0;

						if (a_unique_list_practice_sublist
								.indexOf(a_row_json_data[i_row_json_index].subprac) < 0) {
							a_unique_list_practice_sublist
									.push(a_row_json_data[i_row_json_index].subprac);
							a_duplicate_sub_prac_count
									.push({
										'sub_prac' : a_row_json_data[i_row_json_index].subprac,
										'sub_prac_name' : a_row_json_data[i_row_json_index].subprac_text,
										'revenue_share' : 50000,
										'no_count' : 0
									});

						}

						f_revenue_recognized = find_recognized_revenue_f(
								a_revenue_recognized_for_project, i_practice,
								i_sub_practice, i_role, i_level, s_mnth,
								s_year, a_subprac_searched_once,
								a_subprac_searched_once_month,
								a_subprac_searched_once_year);

						if (!f_revenue_recognized)
							f_revenue_recognized = 0;

						var s_month_name = s_mnth + '_' + s_year;

						var total_revenue = parseFloat(i_no_of_resources)
								* parseFloat(f_revenue);
						total_revenue = parseFloat(total_revenue).toFixed(2);

						if (i_practice_previous == 0) {
							i_practice_previous = a_row_json_data[i_row_json_index].prac_row;
						}

						if (i_practice_previous != a_row_json_data[i_row_json_index].prac_row) {
							f_total_revenue_for_tenure = 0;
							i_practice_previous = a_row_json_data[i_row_json_index].prac_row;
						}

						f_total_revenue_for_tenure = parseFloat(f_total_revenue_for_tenure)
								+ parseFloat(total_revenue);
						f_total_revenue_for_tenure = parseFloat(
								f_total_revenue_for_tenure).toFixed(2);

						var i_prac_subPrac_role_level = i_practice + '_'
								+ i_sub_practice + '_' + i_role + '_' + i_level
								+ '_' + i_location;

						if (!projectWiseRevenue[i_prac_subPrac_role_level]) {
							projectWiseRevenue[i_prac_subPrac_role_level] = {
								practice_name : s_practice,
								sub_prac_name : s_sub_practice,
								sub_practice : i_sub_practice,
								role_name : s_role,
								level_name : s_level,
								location_name : s_location,
								total_revenue_for_tenure : f_total_revenue_for_tenure,
								//revenue_share: f_revenue_share,
								RevenueData : {}
							};

							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice] = new yearData(
									i_year_project);

							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].Amount = total_revenue;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].prcnt_allocated = i_no_of_resources;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].recognized_revenue = f_revenue_recognized;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].mnth_strt = s_mnth_strt_date;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].mnth_end = s_mnth_end_date;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].revenue_share = f_revenue_share;
						} else {
							projectWiseRevenue[i_prac_subPrac_role_level].total_revenue_for_tenure = f_total_revenue_for_tenure;

							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].Amount = total_revenue;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].prcnt_allocated = i_no_of_resources;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].recognized_revenue = f_revenue_recognized;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].mnth_strt = s_mnth_strt_date;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].mnth_end = s_mnth_end_date;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].revenue_share = f_revenue_share;
						}
					}
				}
			}

			if (s_effrt_json_3) {
				var i_practice_previous = 0;
				var f_total_revenue_for_tenure = 0;
				//for (var i_mnth_end_plan = 0; i_mnth_end_plan < a_get_mnth_end_effrt_plan.length; i_mnth_end_plan++)
				var s_entire_json_clubed = JSON.parse(s_effrt_json_3);
				for (var i_mnth_end_plan = 0; i_mnth_end_plan < s_entire_json_clubed.length; i_mnth_end_plan++) {
					var a_row_json_data = JSON.parse(JSON
							.stringify(s_entire_json_clubed[i_mnth_end_plan]));
					for (var i_row_json_index = 0; i_row_json_index < a_row_json_data.length; i_row_json_index++) {
						var i_practice = a_row_json_data[i_row_json_index].prac;
						var s_practice = a_row_json_data[i_row_json_index].prac_text;
						var i_sub_practice = a_row_json_data[i_row_json_index].subprac;
						var s_sub_practice = a_row_json_data[i_row_json_index].subprac_text;
						var i_role = a_row_json_data[i_row_json_index].role;
						var s_role = a_row_json_data[i_row_json_index].role_text;
						var i_level = a_row_json_data[i_row_json_index].level;
						var s_level = a_row_json_data[i_row_json_index].level_text;
						var i_location = a_row_json_data[i_row_json_index].loc;
						var s_location = a_row_json_data[i_row_json_index].loc_text;
						var f_revenue = a_row_json_data[i_row_json_index].cost;
						if (!f_revenue)
							f_revenue = 0;

						var f_revenue_share = a_row_json_data[i_row_json_index].share;
						if (!f_revenue_share)
							f_revenue_share = 0;

						var i_no_of_resources = a_row_json_data[i_row_json_index].allo;
						var s_mnth_strt_date = '1/31/2017';
						var s_mnth_end_date = '31/31/2017';
						var s_mnth = a_row_json_data[i_row_json_index].mnth;
						var s_year = a_row_json_data[i_row_json_index].year;

						var f_revenue_recognized = 0;

						if (a_unique_list_practice_sublist
								.indexOf(a_row_json_data[i_row_json_index].subprac) < 0) {
							a_unique_list_practice_sublist
									.push(a_row_json_data[i_row_json_index].subprac);
							a_duplicate_sub_prac_count
									.push({
										'sub_prac' : a_row_json_data[i_row_json_index].subprac,
										'sub_prac_name' : a_row_json_data[i_row_json_index].subprac_text,
										'revenue_share' : 50000,
										'no_count' : 0
									});

						}

						f_revenue_recognized = find_recognized_revenue_f(
								a_revenue_recognized_for_project, i_practice,
								i_sub_practice, i_role, i_level, s_mnth,
								s_year, a_subprac_searched_once,
								a_subprac_searched_once_month,
								a_subprac_searched_once_year);

						if (!f_revenue_recognized)
							f_revenue_recognized = 0;

						var s_month_name = s_mnth + '_' + s_year;

						var total_revenue = parseFloat(i_no_of_resources)
								* parseFloat(f_revenue);
						total_revenue = parseFloat(total_revenue).toFixed(2);

						

						if (i_practice_previous == 0) {
							i_practice_previous = a_row_json_data[i_row_json_index].prac_row;
						}

						if (i_practice_previous != a_row_json_data[i_row_json_index].prac_row) {
							f_total_revenue_for_tenure = 0;
							i_practice_previous = a_row_json_data[i_row_json_index].prac_row;
						}

						f_total_revenue_for_tenure = parseFloat(f_total_revenue_for_tenure)
								+ parseFloat(total_revenue);
						f_total_revenue_for_tenure = parseFloat(
								f_total_revenue_for_tenure).toFixed(2);

						var i_prac_subPrac_role_level = i_practice + '_'
								+ i_sub_practice + '_' + i_role + '_' + i_level
								+ '_' + i_location;

						if (!projectWiseRevenue[i_prac_subPrac_role_level]) {
							projectWiseRevenue[i_prac_subPrac_role_level] = {
								practice_name : s_practice,
								sub_prac_name : s_sub_practice,
								sub_practice : i_sub_practice,
								role_name : s_role,
								level_name : s_level,
								location_name : s_location,
								total_revenue_for_tenure : f_total_revenue_for_tenure,
								//revenue_share: f_revenue_share,
								RevenueData : {}
							};

							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice] = new yearData(
									i_year_project);

							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].Amount = total_revenue;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].prcnt_allocated = i_no_of_resources;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].recognized_revenue = f_revenue_recognized;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].mnth_strt = s_mnth_strt_date;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].mnth_end = s_mnth_end_date;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].revenue_share = f_revenue_share;
						} else {
							projectWiseRevenue[i_prac_subPrac_role_level].total_revenue_for_tenure = f_total_revenue_for_tenure;

							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].Amount = total_revenue;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].prcnt_allocated = i_no_of_resources;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].recognized_revenue = f_revenue_recognized;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].mnth_strt = s_mnth_strt_date;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].mnth_end = s_mnth_end_date;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].revenue_share = f_revenue_share;
						}
					}
				}
			}

			if (s_effrt_json_4) {
				var i_practice_previous = 0;
				var f_total_revenue_for_tenure = 0;
				//for (var i_mnth_end_plan = 0; i_mnth_end_plan < a_get_mnth_end_effrt_plan.length; i_mnth_end_plan++)
				var s_entire_json_clubed = JSON.parse(s_effrt_json_4);
				for (var i_mnth_end_plan = 0; i_mnth_end_plan < s_entire_json_clubed.length; i_mnth_end_plan++) {
					var a_row_json_data = JSON.parse(JSON
							.stringify(s_entire_json_clubed[i_mnth_end_plan]));
					for (var i_row_json_index = 0; i_row_json_index < a_row_json_data.length; i_row_json_index++) {
						var i_practice = a_row_json_data[i_row_json_index].prac;
						var s_practice = a_row_json_data[i_row_json_index].prac_text;
						var i_sub_practice = a_row_json_data[i_row_json_index].subprac;
						var s_sub_practice = a_row_json_data[i_row_json_index].subprac_text;
						var i_role = a_row_json_data[i_row_json_index].role;
						var s_role = a_row_json_data[i_row_json_index].role_text;
						var i_level = a_row_json_data[i_row_json_index].level;
						var s_level = a_row_json_data[i_row_json_index].level_text;
						var i_location = a_row_json_data[i_row_json_index].loc;
						var s_location = a_row_json_data[i_row_json_index].loc_text;
						var f_revenue = a_row_json_data[i_row_json_index].cost;
						if (!f_revenue)
							f_revenue = 0;

						var f_revenue_share = a_row_json_data[i_row_json_index].share;
						if (!f_revenue_share)
							f_revenue_share = 0;

						var i_no_of_resources = a_row_json_data[i_row_json_index].allo;
						var s_mnth_strt_date = '1/31/2017';
						var s_mnth_end_date = '31/31/2017';
						var s_mnth = a_row_json_data[i_row_json_index].mnth;
						var s_year = a_row_json_data[i_row_json_index].year;

						var f_revenue_recognized = 0;

						if (a_unique_list_practice_sublist
								.indexOf(a_row_json_data[i_row_json_index].subprac) < 0) {
							a_unique_list_practice_sublist
									.push(a_row_json_data[i_row_json_index].subprac);
							a_duplicate_sub_prac_count
									.push({
										'sub_prac' : a_row_json_data[i_row_json_index].subprac,
										'sub_prac_name' : a_row_json_data[i_row_json_index].subprac_text,
										'revenue_share' : 50000,
										'no_count' : 0
									});

						}

						f_revenue_recognized = find_recognized_revenue_f(
								a_revenue_recognized_for_project, i_practice,
								i_sub_practice, i_role, i_level, s_mnth,
								s_year, a_subprac_searched_once,
								a_subprac_searched_once_month,
								a_subprac_searched_once_year);

						if (!f_revenue_recognized)
							f_revenue_recognized = 0;

						var s_month_name = s_mnth + '_' + s_year;

						var total_revenue = parseFloat(i_no_of_resources)
								* parseFloat(f_revenue);
						total_revenue = parseFloat(total_revenue).toFixed(2);

						if (i_practice_previous == 0) {
							i_practice_previous = a_row_json_data[i_row_json_index].prac_row;
						}

						if (i_practice_previous != a_row_json_data[i_row_json_index].prac_row) {
							f_total_revenue_for_tenure = 0;
							i_practice_previous = a_row_json_data[i_row_json_index].prac_row;
						}

						f_total_revenue_for_tenure = parseFloat(f_total_revenue_for_tenure)
								+ parseFloat(total_revenue);
						f_total_revenue_for_tenure = parseFloat(
								f_total_revenue_for_tenure).toFixed(2);

						var i_prac_subPrac_role_level = i_practice + '_'
								+ i_sub_practice + '_' + i_role + '_' + i_level
								+ '_' + i_location;

						if (!projectWiseRevenue[i_prac_subPrac_role_level]) {
							projectWiseRevenue[i_prac_subPrac_role_level] = {
								practice_name : s_practice,
								sub_prac_name : s_sub_practice,
								sub_practice : i_sub_practice,
								role_name : s_role,
								level_name : s_level,
								location_name : s_location,
								total_revenue_for_tenure : f_total_revenue_for_tenure,
								//revenue_share: f_revenue_share,
								RevenueData : {}
							};

							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice] = new yearData(
									i_year_project);

							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].Amount = total_revenue;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].prcnt_allocated = i_no_of_resources;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].recognized_revenue = f_revenue_recognized;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].mnth_strt = s_mnth_strt_date;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].mnth_end = s_mnth_end_date;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].revenue_share = f_revenue_share;
						} else {
							projectWiseRevenue[i_prac_subPrac_role_level].total_revenue_for_tenure = f_total_revenue_for_tenure;

							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].Amount = total_revenue;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].prcnt_allocated = i_no_of_resources;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].recognized_revenue = f_revenue_recognized;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].mnth_strt = s_mnth_strt_date;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].mnth_end = s_mnth_end_date;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].revenue_share = f_revenue_share;
						}
					}
				}
			}

			for (var i_dupli = 0; i_dupli < a_duplicate_sub_prac_count.length; i_dupli++) {
				var f_prev_mnth_cost = generate_cost_view_prev_mnth_f(
						projectWiseRevenue_previous_effrt, monthBreakUp,
						i_year_project, i_project_mnth, 2,
						a_duplicate_sub_prac_count[i_dupli].sub_prac,
						s_currency_symbol_proj, s_currency_symbol_usd,
						i_year_project_end, i_prject_end_mnth);
				nlapiLogExecution('audit', 'cost for prev mnth:- '
						+ f_prev_mnth_cost, 'sub prac:- '
						+ a_duplicate_sub_prac_count[i_dupli].sub_prac);
				var f_total_cost_breakUp = generate_total_cost_f(
						projectWiseRevenue, monthBreakUp, i_year_project,
						i_project_mnth, 2,
						a_duplicate_sub_prac_count[i_dupli].sub_prac,
						s_currency_symbol_proj, s_currency_symbol_usd,
						i_year_project_end, i_prject_end_mnth, f_prev_mnth_cost);

			}
			f_current_month_actual_revenue_total_proj_level_f = 0;
			a_recognized_revenue_total_proj_level_f = new Array();
			var excel_cost_data = [];
			// Effort View table
			for (var i_dupli = 0; i_dupli < a_duplicate_sub_prac_count.length; i_dupli++) {
				var f_prev_mnth_cost = generate_cost_view_prev_mnth_f(
						projectWiseRevenue_previous_effrt, monthBreakUp,
						i_year_project, i_project_mnth, 2,
						a_duplicate_sub_prac_count[i_dupli].sub_prac,
						s_currency_symbol_proj, s_currency_symbol_usd,
						i_year_project_end, i_prject_end_mnth);
				nlapiLogExecution('audit', 'cost for prev mnth:- '
						+ f_prev_mnth_cost, 'sub prac:- '
						+ a_duplicate_sub_prac_count[i_dupli].sub_prac);
				var h_tableHtml_effort_view = generate_cost_view_f(
						projectWiseRevenue, monthBreakUp, i_year_project,
						i_project_mnth, 2,
						a_duplicate_sub_prac_count[i_dupli].sub_prac,
						s_currency_symbol_proj, s_currency_symbol_usd,
						i_year_project_end, i_prject_end_mnth,
						f_prev_mnth_cost, f_total_cost_breakUp);
				excel_cost_data.push(h_tableHtml_effort_view);
			}

			// Total revenue table
			var h_tableHtml_total_view = generate_total_project_view_f(
					projectWiseRevenue, monthBreakUp, i_year_project,
					i_project_mnth, 4, s_currency_symbol_proj,
					s_currency_symbol_usd, i_year_project_end,
					i_prject_end_mnth);
			for (var excel_len = 0; excel_len < excel_cost_data.length; excel_len++) {
				var rev_len = excel_cost_data[excel_len].month;
				bill_data_arr[sr_no] = {
					'project_id' : excel_cost_data[excel_len].practice,
					'project_name' : excel_cost_data[excel_len].subpractice,
					'month' : excel_cost_data[excel_len].month,
					'actual_rev' : excel_cost_data[excel_len].revenue,

				};
				sr_no++;
				nlapiLogExecution('debug', 'Data', bill_data_arr);
			}
		}

		nlapiLogExecution('audit', 'remaining usage:- ', nlapiGetContext()
				.getRemainingUsage());
	} catch (err) {
		nlapiLogExecution('ERROR', 'suitelet_month_end_confirmation',
				'ERROR MESSAGE:- ' + err);
	}
	return bill_data_arr;
}

function generate_cost_view_prev_mnth_f(projectWiseRevenue_previous_effrt,
		monthBreakUp, i_year_project, i_project_mnth, mode, sub_prac,
		s_currency_symbol_proj, s_currency_symbol_usd, i_year_project_end,
		i_prject_end_mnth) {

	var today = new Date();
	var currentMonthName = getMonthName(nlapiDateToString(today));

	var a_sub_prac_already_displayed = new Array();
	var i_subpractice_id = 0;
	var a_amount = new Array();
	var a_recognized_revenue = new Array();
	var i_frst_row = 0;
	var i_diplay_frst_column = 0;
	var i_total_row_revenue = 0;
	var i_total_per_row = 0;
	var total_prcnt_aloocated = new Array();
	var b_first_line_copied = 0;
	var f_total_allocated_row = 0;

	for ( var emp_internal_id in projectWiseRevenue_previous_effrt) {
		if (projectWiseRevenue_previous_effrt[emp_internal_id].sub_practice == sub_prac) {
			f_total_allocated_row = 0;
			for ( var project_internal_id in projectWiseRevenue_previous_effrt[emp_internal_id].RevenueData) {
				var i_index_mnth = 0;
				for ( var month in projectWiseRevenue_previous_effrt[emp_internal_id].RevenueData[project_internal_id]) {
					var a_MonthNames = MonthNames(i_year_project);
					var currentMonthPos = a_MonthNames.indexOf(month);
					var s_current_month = a_MonthNames[currentMonthPos];
					var s_current_mnth_yr = s_current_month.substr(4, 8);

					var i_nxt_yr_flg = 0;
					if (currentMonthPos >= 12) {
						currentMonthPos = currentMonthPos - 12;
						i_nxt_yr_flg = 1;
					}

					var i_curr_yr_dis_flag = 0;
					if (parseInt(s_current_mnth_yr) < i_year_project_end
							&& i_prject_end_mnth < currentMonthPos) {
						i_curr_yr_dis_flag = 1;
					}

					if (((i_project_mnth <= currentMonthPos
							&& ((i_prject_end_mnth >= currentMonthPos || i_curr_yr_dis_flag == 1) && i_nxt_yr_flg == 0) && parseInt(s_current_mnth_yr) <= i_year_project) && parseInt(s_current_mnth_yr) <= i_year_project_end)
							|| (i_prject_end_mnth >= currentMonthPos
									&& parseInt(s_current_mnth_yr) <= i_year_project_end && i_nxt_yr_flg == 1)) {
						//nlapiLogExecution('audit','currentMonthPos:- ',+currentMonthPos);
						if (b_first_line_copied == 1) {
							var f_allocated_prcnt = total_prcnt_aloocated[i_index_mnth];
							f_allocated_prcnt = parseFloat(f_allocated_prcnt)
									+ parseFloat(projectWiseRevenue_previous_effrt[emp_internal_id].RevenueData[project_internal_id][month].prcnt_allocated);
							total_prcnt_aloocated[i_index_mnth] = f_allocated_prcnt;
							i_index_mnth++;
						} else {
							total_prcnt_aloocated
									.push(projectWiseRevenue_previous_effrt[emp_internal_id].RevenueData[project_internal_id][month].prcnt_allocated);
						}

						f_total_allocated_row = parseFloat(projectWiseRevenue_previous_effrt[emp_internal_id].RevenueData[project_internal_id][month].prcnt_allocated)
								+ parseFloat(f_total_allocated_row);

					}
				}
			}

			b_first_line_copied = 1;

			var s_sub_prac_name = projectWiseRevenue_previous_effrt[emp_internal_id].sub_prac_name;
			var s_practice_name = projectWiseRevenue_previous_effrt[emp_internal_id].practice_name;

			i_diplay_frst_column = 1;
		}
	}
	i_diplay_frst_column = 0;

	var f_total_row_allocation = 0;
	for (var i_index_total_allocated = 0; i_index_total_allocated < total_prcnt_aloocated.length; i_index_total_allocated++) {
		f_total_row_allocation = parseFloat(f_total_row_allocation)
				+ parseFloat(total_prcnt_aloocated[i_index_total_allocated]);
	}

	for ( var emp_internal_id in projectWiseRevenue_previous_effrt) {

		for ( var project_internal_id in projectWiseRevenue_previous_effrt[emp_internal_id].RevenueData) {

			if (projectWiseRevenue_previous_effrt[emp_internal_id].sub_practice == sub_prac) {

				var i_sub_practice_internal_id = 0;
				var i_amt = 0;
				for ( var month in projectWiseRevenue_previous_effrt[emp_internal_id].RevenueData[project_internal_id]) {
					var a_MonthNames = MonthNames(i_year_project);
					var currentMonthPos = a_MonthNames.indexOf(month);
					var s_current_month = a_MonthNames[currentMonthPos];
					var s_current_mnth_yr = s_current_month.substr(4, 8);

					var i_nxt_yr_flg = 0;
					if (currentMonthPos >= 12) {
						currentMonthPos = currentMonthPos - 12;
						i_nxt_yr_flg = 1;
					}

					var i_curr_yr_dis_flag = 0;
					if (parseInt(s_current_mnth_yr) < i_year_project_end
							&& i_prject_end_mnth < currentMonthPos) {
						i_curr_yr_dis_flag = 1;
					}

					if (((i_project_mnth <= currentMonthPos
							&& ((i_prject_end_mnth >= currentMonthPos || i_curr_yr_dis_flag == 1) && i_nxt_yr_flg == 0) && parseInt(s_current_mnth_yr) <= i_year_project) && parseInt(s_current_mnth_yr) <= i_year_project_end)
							|| (i_prject_end_mnth >= currentMonthPos
									&& parseInt(s_current_mnth_yr) <= i_year_project_end && i_nxt_yr_flg == 1)) {
						if (mode == 2) {

							var total_revenue_format = parseFloat(
									projectWiseRevenue_previous_effrt[emp_internal_id].RevenueData[project_internal_id][month].Amount)
									.toFixed(1);

							if (i_frst_row == 0) {
								//nlapiLogExecution('audit','inside push amnt');
								a_amount.push(total_revenue_format);
								a_recognized_revenue
										.push(projectWiseRevenue_previous_effrt[emp_internal_id].RevenueData[project_internal_id][month].recognized_revenue);
							} else {
								var amnt_mnth = a_amount[i_amt];
								amnt_mnth = parseFloat(amnt_mnth)
										+ parseFloat(projectWiseRevenue_previous_effrt[emp_internal_id].RevenueData[project_internal_id][month].Amount);

								a_amount[i_amt] = amnt_mnth;

								var i_existing_recognised_amount = a_recognized_revenue[i_amt];
								i_existing_recognised_amount = parseFloat(i_existing_recognised_amount)
										+ parseFloat(projectWiseRevenue_previous_effrt[emp_internal_id].RevenueData[project_internal_id][month].recognized_revenue);

								a_recognized_revenue[i_amt] = i_existing_recognised_amount;

								i_amt++;
							}
							i_total_row_revenue = parseFloat(i_total_row_revenue)
									+ parseFloat(projectWiseRevenue_previous_effrt[emp_internal_id].RevenueData[project_internal_id][month].Amount);
							i_total_per_row = parseFloat(i_total_per_row)
									+ parseFloat(projectWiseRevenue_previous_effrt[emp_internal_id].RevenueData[project_internal_id][month].Amount);
						}

					}
				}

				if (mode == 2) {
					i_total_per_row = 0;
					//i_total_row_revenue = parseFloat(i_total_row_revenue) + parseFloat(projectWiseRevenue[emp_internal_id].total_revenue_for_tenure);
				}

				i_frst_row = 1;

				i_diplay_frst_column = 1;

				var f_revenue_share = projectWiseRevenue_previous_effrt[emp_internal_id].revenue_share;
			}
		}
	}

	var s_sub_prac_name = '';
	for ( var emp_internal_id in projectWiseRevenue_previous_effrt) {
		if (projectWiseRevenue_previous_effrt[emp_internal_id].sub_practice == sub_prac) {
			s_sub_prac_name = projectWiseRevenue_previous_effrt[emp_internal_id].sub_prac_name;
			s_practice_name = projectWiseRevenue_previous_effrt[emp_internal_id].practice_name;
			//projectWiseRevenue[emp_internal_id].revenue_share_revised = i_total_row_revenue;
			//projectWiseRevenue[emp_internal_id].total_revenue_for_tenure = i_total_row_revenue;
		}
	}

	for (var i_amt = 0; i_amt < a_amount.length; i_amt++) {

	}

	// Percent row

	var f_total_prcnt = 0;
	for (var i_amt = 0; i_amt < a_amount.length; i_amt++) {
		var f_prcnt_revenue = parseFloat(a_amount[i_amt])
				/ parseFloat(i_total_row_revenue);
		f_prcnt_revenue = f_prcnt_revenue * 100;
		if (!f_prcnt_revenue)
			f_prcnt_revenue = 0;

		f_total_prcnt = parseFloat(f_prcnt_revenue) + parseFloat(f_total_prcnt);

	}

	//Total reveue recognized per month row

	var d_today_date = new Date();
	var i_current_month = d_today_date.getMonth();
	var i_current_year = d_today_date.getFullYear();

	var f_total_revenue = 0;
	var f_revenue_amount_till_current_month = 0;
	for (var i_amt = 0; i_amt < a_amount.length; i_amt++) {
		var f_prcnt_revenue = parseFloat(a_amount[i_amt])
				/ parseFloat(i_total_row_revenue);
		f_prcnt_revenue = f_prcnt_revenue * 100;

		var f_revenue_amount = (parseFloat(f_prcnt_revenue) * parseFloat(f_revenue_share)) / 100;
		if (!f_revenue_amount)
			f_revenue_amount = 0;

		if (i_amt <= i_current_month) {
			//nlapiLogExecution('audit','i_current_month:- '+i_current_month,'a_amount.length:- '+a_amount.length);
			f_revenue_amount_till_current_month = parseFloat(f_revenue_amount)
					+ parseFloat(f_revenue_amount_till_current_month);
			if (!f_revenue_amount_till_current_month)
				f_revenue_amount_till_current_month = 0;
		}

		f_total_revenue = parseFloat(f_revenue_amount)
				+ parseFloat(f_total_revenue);

	}

	for ( var emp_internal_id in projectWiseRevenue_previous_effrt) {
		for ( var project_internal_id in projectWiseRevenue_previous_effrt[emp_internal_id].RevenueData) {
			var i_amount_map = 0;
			if (projectWiseRevenue_previous_effrt[emp_internal_id].sub_practice == sub_prac) {
				for ( var month in projectWiseRevenue_previous_effrt[emp_internal_id].RevenueData[project_internal_id]) {
					var a_MonthNames = MonthNames(i_year_project);
					var currentMonthPos = a_MonthNames.indexOf(month);
					var s_current_month = a_MonthNames[currentMonthPos];
					var s_current_mnth_yr = s_current_month.substr(4, 8);

					var i_nxt_yr_flg = 0;
					if (currentMonthPos >= 12) {
						currentMonthPos = currentMonthPos - 12;
						i_nxt_yr_flg = 1;
					}

					var i_curr_yr_dis_flag = 0;
					if (parseInt(s_current_mnth_yr) < i_year_project_end
							&& i_prject_end_mnth < currentMonthPos) {
						i_curr_yr_dis_flag = 1;
					}

					if (((i_project_mnth <= currentMonthPos
							&& ((i_prject_end_mnth >= currentMonthPos || i_curr_yr_dis_flag == 1) && i_nxt_yr_flg == 0) && parseInt(s_current_mnth_yr) <= i_year_project) && parseInt(s_current_mnth_yr) <= i_year_project_end)
							|| (i_prject_end_mnth >= currentMonthPos
									&& parseInt(s_current_mnth_yr) <= i_year_project_end && i_nxt_yr_flg == 1)) {
						var f_prcnt_revenue = parseFloat(a_amount[i_amount_map])
								/ parseFloat(i_total_row_revenue);
						f_prcnt_revenue = f_prcnt_revenue * 100;

						var f_revenue_amount = (parseFloat(f_prcnt_revenue) * parseFloat(f_revenue_share)) / 100;

						projectWiseRevenue_previous_effrt[emp_internal_id].RevenueData[project_internal_id][month].total_revenue = f_revenue_amount;
						projectWiseRevenue_previous_effrt[emp_internal_id].RevenueData[project_internal_id][month].prcent_complt = f_prcnt_revenue;
						i_amount_map++;
					}
				}
			}
		}
	}

	var today = new Date();
	var currentMonthName = getMonthName(nlapiDateToString(today));

	var i_total_revenue_recognized_ytd = 0;
	for (var i_revenue_index = 0; i_revenue_index < a_recognized_revenue.length; i_revenue_index++) {
		i_total_revenue_recognized_ytd = parseFloat(i_total_revenue_recognized_ytd)
				+ parseFloat(a_recognized_revenue[i_revenue_index]);

	}

	//Actual revenue to be recognized 

	if (parseInt(i_year_project) != parseInt(i_current_year)) {
		var i_total_project_tenure = 11 + parseInt(i_current_month);
		var f_total_prev_mnths = parseFloat(i_total_project_tenure)
				- parseFloat(i_project_mnth);
		i_current_month = parseFloat(f_total_prev_mnths) + parseFloat(1);
	} else {
		i_current_month = parseFloat(i_current_month)
				- parseFloat(i_project_mnth);
	}

	var f_current_month_actual_revenue = parseFloat(f_revenue_amount_till_current_month)
			- parseFloat(i_total_revenue_recognized_ytd);
	//var i_total_revenue_recognized_ytd = 0;

	//nlapiLogExecution('audit','i_current_month:- '+i_current_month);
	for (var i_revenue_index = 0; i_revenue_index <= i_current_month - 1; i_revenue_index++) {
		//nlapiLogExecution('audit','f_current_month_actual_revenue:- '+f_current_month_actual_revenue,'f_revenue_amount_till_current_month:- '+f_revenue_amount_till_current_month);
		//i_total_revenue_recognized_ytd = parseFloat(i_total_revenue_recognized_ytd) + parseFloat(a_recognized_revenue[i_revenue_index]);

	}


	var i_total_actual_revenue_recognized = parseFloat(i_total_revenue_recognized_ytd)
			+ parseFloat(f_current_month_actual_revenue);

	

	{
		var f_prcnt_revenue = parseFloat(a_amount[i_current_month])
				/ parseFloat(i_total_row_revenue);
		f_prcnt_revenue = f_prcnt_revenue * 100;

		var f_revenue_amount = (parseFloat(f_prcnt_revenue) * parseFloat(f_revenue_share)) / 100;
		if (!f_revenue_amount)
			f_revenue_amount = 0;

		return f_revenue_amount;

		if (i_amt < i_current_month) {
			//nlapiLogExecution('audit','i_current_month:- '+i_current_month,'a_amount.length:- '+a_amount.length);
			f_revenue_amount_till_current_month = parseFloat(f_revenue_amount)
					+ parseFloat(f_revenue_amount_till_current_month);
			if (!f_revenue_amount_till_current_month)
				f_revenue_amount_till_current_month = 0;
		}

		f_total_revenue = parseFloat(f_revenue_amount)
				+ parseFloat(f_total_revenue);

		i_total_actual_revenue_recognized = parseFloat(i_total_actual_revenue_recognized)
				+ parseFloat(f_revenue_amount);
	}

	//Total revenue recognized for previous months

	for (var i_exist_index = 0; i_exist_index <= a_recognized_revenue.length; i_exist_index++) {
		if (a_recognized_revenue_total_proj_level_f[i_exist_index]) {
			var f_existing_cost_in_arr = parseFloat(a_recognized_revenue_total_proj_level_f[i_exist_index]);
		} else {
			var f_existing_cost_in_arr = 0;
		}

		var f_existing_cost = a_recognized_revenue[i_exist_index];
		f_existing_cost = parseFloat(f_existing_cost)
				+ parseFloat(f_existing_cost_in_arr);

		a_recognized_revenue_total_proj_level_f[i_exist_index] = f_existing_cost;
	}
	//a_recognized_revenue_total_proj_level_f = a_recognized_revenue;

	//return html;
}

function generate_cost_view_f(projectWiseRevenue, monthBreakUp, i_year_project,
		i_project_mnth, mode, sub_prac, s_currency_symbol_proj,
		s_currency_symbol_usd, i_year_project_end, i_prject_end_mnth,
		f_prev_mnth_cost, f_total_cost_breakUp) {
	var css_file_url = "";
	var html_data = [];
	var html_val = {};
	var html = {
		'month' : [],
		'practice' : [],
		'subpractice' : [],
		'revenue' : [],
		'actual_rec' : [],
		'cummulative' : [],
		'html_amount' : [],
	};

	// header

	var today = new Date();
	var currentMonthName = getMonthName(nlapiDateToString(today));

	for (var j = 0; j < monthBreakUp.length; j++) {
		var months = monthBreakUp[j];
		var s_month_name = getMonthName(months.Start); // get month name
		var s_year = nlapiStringToDate(months.Start)
		s_year = s_year.getFullYear();
		s_month_name = s_month_name + '_' + s_year;
		html['month'].push(s_month_name);
	}

	var a_sub_prac_already_displayed = new Array();
	var i_subpractice_id = 0;
	var a_amount = new Array();
	var a_recognized_revenue = new Array();
	var i_frst_row = 0;
	var i_diplay_frst_column = 0;
	var i_total_row_revenue = 0;
	var i_total_per_row = 0;
	var total_prcnt_aloocated = new Array();
	var b_first_line_copied = 0;
	var f_total_allocated_row = 0;
	var revenue_share_arr = new Array();

	for ( var emp_internal_id in projectWiseRevenue) {
		if (projectWiseRevenue[emp_internal_id].sub_practice == sub_prac) {
			f_total_allocated_row = 0;
			for ( var project_internal_id in projectWiseRevenue[emp_internal_id].RevenueData) {

				if (i_diplay_frst_column == 0) {

				} else {

				}

				var i_index_mnth = 0;
				for ( var month in projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id]) {
					var a_MonthNames = MonthNames(i_year_project);
					var currentMonthPos = a_MonthNames.indexOf(month);
					var s_current_month = a_MonthNames[currentMonthPos];
					var s_current_mnth_yr = s_current_month.substr(4, 8);

					var i_nxt_yr_flg = 0;
					if (currentMonthPos >= 12) {
						currentMonthPos = currentMonthPos - 12;
						i_nxt_yr_flg = 1;
					}

					var i_curr_yr_dis_flag = 0;
					if (parseInt(s_current_mnth_yr) < i_year_project_end
							&& i_prject_end_mnth < currentMonthPos) {
						i_curr_yr_dis_flag = 1;
					}

					if (((i_project_mnth <= currentMonthPos
							&& ((i_prject_end_mnth >= currentMonthPos || i_curr_yr_dis_flag == 1) && i_nxt_yr_flg == 0) && parseInt(s_current_mnth_yr) <= i_year_project) && parseInt(s_current_mnth_yr) <= i_year_project_end)
							|| (i_prject_end_mnth >= currentMonthPos
									&& parseInt(s_current_mnth_yr) <= i_year_project_end && i_nxt_yr_flg == 1)) {
						//nlapiLogExecution('audit','currentMonthPos:- ',+currentMonthPos);
						if (b_first_line_copied == 1) {
							var f_allocated_prcnt = total_prcnt_aloocated[i_index_mnth];
							f_allocated_prcnt = parseFloat(f_allocated_prcnt)
									+ parseFloat(projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id][month].prcnt_allocated);
							total_prcnt_aloocated[i_index_mnth] = f_allocated_prcnt;
							i_index_mnth++;
						} else {
							total_prcnt_aloocated
									.push(projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id][month].prcnt_allocated);
						}

						f_total_allocated_row = parseFloat(projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id][month].prcnt_allocated)
								+ parseFloat(f_total_allocated_row);

					}
				}
			}

			b_first_line_copied = 1;

			var s_sub_prac_name = projectWiseRevenue[emp_internal_id].sub_prac_name;
			var s_practice_name = projectWiseRevenue[emp_internal_id].practice_name;

			i_diplay_frst_column = 1;
		}
	}
	i_diplay_frst_column = 0;

	var f_total_row_allocation = 0;
	for (var i_index_total_allocated = 0; i_index_total_allocated < total_prcnt_aloocated.length; i_index_total_allocated++) {

		f_total_row_allocation = parseFloat(f_total_row_allocation)
				+ parseFloat(total_prcnt_aloocated[i_index_total_allocated]);
	}

	for ( var emp_internal_id in projectWiseRevenue) {

		for ( var project_internal_id in projectWiseRevenue[emp_internal_id].RevenueData) {

			if (projectWiseRevenue[emp_internal_id].sub_practice == sub_prac) {

				if (i_diplay_frst_column == 0) {

				} else {

				}

				var i_sub_practice_internal_id = 0;
				var i_amt = 0;
				for ( var month in projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id]) {
					var a_MonthNames = MonthNames(i_year_project);
					var currentMonthPos = a_MonthNames.indexOf(month);
					var s_current_month = a_MonthNames[currentMonthPos];
					var s_current_mnth_yr = s_current_month.substr(4, 8);

					var i_nxt_yr_flg = 0;
					if (currentMonthPos >= 12) {
						currentMonthPos = currentMonthPos - 12;
						i_nxt_yr_flg = 1;
					}

					var i_curr_yr_dis_flag = 0;
					if (parseInt(s_current_mnth_yr) < i_year_project_end
							&& i_prject_end_mnth < currentMonthPos) {
						i_curr_yr_dis_flag = 1;
					}

					if (((i_project_mnth <= currentMonthPos
							&& ((i_prject_end_mnth >= currentMonthPos || i_curr_yr_dis_flag == 1) && i_nxt_yr_flg == 0) && parseInt(s_current_mnth_yr) <= i_year_project) && parseInt(s_current_mnth_yr) <= i_year_project_end)
							|| (i_prject_end_mnth >= currentMonthPos
									&& parseInt(s_current_mnth_yr) <= i_year_project_end && i_nxt_yr_flg == 1)) {
						if (mode == 2) {

							var total_revenue_format = parseFloat(
									projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id][month].Amount)
									.toFixed(1);

							if (i_frst_row == 0) {
								//nlapiLogExecution('audit','inside push recog amnt:- '+projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id][month].recognized_revenue);
								a_amount.push(total_revenue_format);
								a_recognized_revenue
										.push(projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id][month].recognized_revenue);
								revenue_share_arr
										.push(parseFloat(projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id][month].revenue_share));
							} else {
								var amnt_mnth = a_amount[i_amt];
								amnt_mnth = parseFloat(amnt_mnth)
										+ parseFloat(projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id][month].Amount);

								a_amount[i_amt] = amnt_mnth;
								revenue_share_arr[i_amt] = parseFloat(projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id][month].revenue_share);
								var i_existing_recognised_amount = a_recognized_revenue[i_amt];
								//nlapiLogExecution('audit','month name:- '+month);
								//nlapiLogExecution('audit','index:- '+i_amt+'::existing rev:- '+i_existing_recognised_amount,'Rev REcg:- '+projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id][month].recognized_revenue);
								i_existing_recognised_amount = parseFloat(i_existing_recognised_amount)
										+ parseFloat(projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id][month].recognized_revenue);

								a_recognized_revenue[i_amt] = i_existing_recognised_amount;

								i_amt++;
							}
							i_total_row_revenue = parseFloat(i_total_row_revenue)
									+ parseFloat(projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id][month].Amount);
							i_total_per_row = parseFloat(i_total_per_row)
									+ parseFloat(projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id][month].Amount);
						}

					}
				}

				if (mode == 2) {

					//html += ''+s_currency_symbol_usd+' '+format2(projectWiseRevenue[emp_internal_id].total_revenue_for_tenure);
					//html += ''+s_currency_symbol_proj+' '+format2(i_total_per_row);

					i_total_per_row = 0;
					//i_total_row_revenue = parseFloat(i_total_row_revenue) + parseFloat(projectWiseRevenue[emp_internal_id].total_revenue_for_tenure);
				}

				i_frst_row = 1;
				i_diplay_frst_column = 1;

				var f_revenue_share = projectWiseRevenue[emp_internal_id].revenue_share;
			}
		}
	}

	var s_sub_prac_name = '';
	for ( var emp_internal_id in projectWiseRevenue) {
		if (projectWiseRevenue[emp_internal_id].sub_practice == sub_prac) {
			s_sub_prac_name = projectWiseRevenue[emp_internal_id].sub_prac_name;
			s_practice_name = projectWiseRevenue[emp_internal_id].practice_name;
			//projectWiseRevenue[emp_internal_id].revenue_share_revised = i_total_row_revenue;
			//projectWiseRevenue[emp_internal_id].total_revenue_for_tenure = i_total_row_revenue;
		}
	}

	// Percent row

	//Total reveue recognized per month row

	html['revenue'].push('Revenue to be reconized/month');

	html['practice'].push(s_practice_name);
	html['subpractice'].push(s_sub_prac_name);

	var d_today_date = new Date();
	var i_current_month = d_today_date.getMonth();
	var i_current_year = d_today_date.getFullYear();

	var f_total_revenue = 0;
	var f_revenue_amount_till_current_month = 0;
	if (flag_counter_arr_f == 0) {
		for (var i_amt = 0; i_amt < a_amount.length; i_amt++) {
			f_revenue_arr_f[i_amt] = 0;

		}
	}
	for (var i_amt = 0; i_amt < a_amount.length; i_amt++) {
		var f_prcnt_revenue = parseFloat(a_amount[i_amt])
				/ parseFloat(f_total_cost_breakUp[i_amt]);
		f_prcnt_revenue = f_prcnt_revenue * 100;
		//var f_revenue_amount = (parseFloat(f_prcnt_revenue) * parseFloat(100000)) / 100

		var f_revenue_amount = (parseFloat(f_prcnt_revenue) * parseFloat(revenue_share_arr[i_amt])) / 100;
		if (!f_revenue_amount)
			f_revenue_amount = 0;
		html['revenue'].push(format2(f_revenue_amount));
		if (parseInt(i_year_project) != parseInt(i_current_year)) {
			var i_total_project_tenure = 11 + parseInt(i_current_month);
			var f_total_prev_mnths = parseFloat(i_total_project_tenure)
					- parseFloat(i_project_mnth);
			f_total_prev_mnths = parseFloat(f_total_prev_mnths) + parseFloat(1);
		} else {
			var f_total_prev_mnths = parseFloat(i_current_month)
					- parseFloat(i_project_mnth);
		}

		if (i_amt <= f_total_prev_mnths) {
			//nlapiLogExecution('audit','f_revenue_amount:- '+f_revenue_amount,'a_amount.length:- '+a_amount.length);
			f_revenue_amount_till_current_month = parseFloat(f_revenue_amount)
					+ parseFloat(f_revenue_amount_till_current_month);
		}

		f_total_revenue = parseFloat(f_revenue_amount)
				+ parseFloat(f_total_revenue);

		f_revenue_arr_f[i_amt] = parseFloat(f_revenue_arr_f[i_amt])
				+ parseFloat(f_revenue_amount);
		flag_counter_arr_f = 1;
	}

	for ( var emp_internal_id in projectWiseRevenue) {
		for ( var project_internal_id in projectWiseRevenue[emp_internal_id].RevenueData) {
			var i_amount_map = 0;
			if (projectWiseRevenue[emp_internal_id].sub_practice == sub_prac) {
				for ( var month in projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id]) {
					var a_MonthNames = MonthNames(i_year_project);
					var currentMonthPos = a_MonthNames.indexOf(month);
					var s_current_month = a_MonthNames[currentMonthPos];
					var s_current_mnth_yr = s_current_month.substr(4, 8);

					var i_nxt_yr_flg = 0;
					if (currentMonthPos >= 12) {
						currentMonthPos = currentMonthPos - 12;
						i_nxt_yr_flg = 1;
					}

					var i_curr_yr_dis_flag = 0;
					if (parseInt(s_current_mnth_yr) < i_year_project_end
							&& i_prject_end_mnth < currentMonthPos) {
						i_curr_yr_dis_flag = 1;
					}

					if (((i_project_mnth <= currentMonthPos
							&& ((i_prject_end_mnth >= currentMonthPos || i_curr_yr_dis_flag == 1) && i_nxt_yr_flg == 0) && parseInt(s_current_mnth_yr) <= i_year_project) && parseInt(s_current_mnth_yr) <= i_year_project_end)
							|| (i_prject_end_mnth >= currentMonthPos
									&& parseInt(s_current_mnth_yr) <= i_year_project_end && i_nxt_yr_flg == 1)) {
						var f_prcnt_revenue = parseFloat(a_amount[i_amount_map])
								/ parseFloat(f_total_cost_breakUp[i_amount_map]);
						f_prcnt_revenue = f_prcnt_revenue * 100;

						var f_revenue_amount = (parseFloat(f_prcnt_revenue) * parseFloat(revenue_share_arr[i_amount_map])) / 100;

						projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id][month].total_revenue = f_revenue_amount;
						projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id][month].prcent_complt = f_prcnt_revenue;
						i_amount_map++;
					}
				}
			}
		}
	}

	html['revenue'].push(format2(f_total_revenue));

	for (var i_exist_index = 0; i_exist_index <= a_recognized_revenue.length; i_exist_index++) {
		if (a_recognized_revenue_total_proj_level_f[i_exist_index]) {
			var f_existing_cost_in_arr = parseFloat(a_recognized_revenue_total_proj_level_f[i_exist_index]);
		} else {
			var f_existing_cost_in_arr = 0;
		}

		var f_existing_cost = a_recognized_revenue[i_exist_index];
		f_existing_cost = parseFloat(f_existing_cost)
				+ parseFloat(f_existing_cost_in_arr);

		a_recognized_revenue_total_proj_level_f[i_exist_index] = f_existing_cost;
	}
	//a_recognized_revenue_total_proj_level_f = a_recognized_revenue;
	html_val = {
		pract : s_practice_name,
		sub_pract : s_sub_prac_name,
	};
	html_data.push(html);
	nlapiLogExecution('DEBUG', 'HTML', JSON.stringify(html));
	return html;
}

function generate_total_project_view_f(projectWiseRevenue, monthBreakUp,
		i_year_project, i_project_mnth, mode, s_currency_symbol_proj,
		s_currency_symbol_usd, i_year_project_end, i_prject_end_mnth) {
	var css_file_url = "";

	var context = nlapiGetContext();
	if (context.getEnvironment() == "SANDBOX") {
		css_file_url = "https://system.sandbox.netsuite.com/core/media/media.nl?id=199133&c=3883006&h=64fa194295cd4ab64d58&mv=iq3kjr6g&_xt=.css&whence=";
	} else if (context.getEnvironment() == "PRODUCTION") {
		css_file_url = "https://system.na1.netsuite.com/core/media/media.nl?id=199133&c=3883006&h=64fa194295cd4ab64d58&mv=iq3kjr6g&_xt=.css&whence=";
	}

	var html = "";
	html += "<link href='" + css_file_url + "'  rel='stylesheet'>";

	html += "<table class='projection-table'>";

	// header
	html += "<tr class='header-row'>";

	html += "<td>";
	html += "";
	html += "</td>";

	var today = new Date();
	var currentMonthName = getMonthName(nlapiDateToString(today));

	for (var j = 0; j < monthBreakUp.length; j++) {
		var months = monthBreakUp[j];
		var s_month_name = getMonthName(months.Start); // get month name
		var s_year = nlapiStringToDate(months.Start)
		s_year = s_year.getFullYear();
		s_month_name = s_month_name + '_' + s_year;

		html += "<td>";
		html += s_month_name;
		html += "</td>";
	}

	html += "<td>";
	html += "Total";
	html += "</td>";

	html += "</tr>";

	var a_sub_prac_already_displayed = new Array();
	var i_subpractice_id = 0;
	var a_amount = new Array();
	var a_prcnt_arr = new Array();
	var i_frst_row = 0;
	var i_diplay_frst_column = 0;
	var i_total_row_revenue = 0;
	var total_prcnt_aloocated = new Array();
	var b_first_line_copied = 0;
	var f_total_allocated_row = 0;
	var i_total_row_revenue_rec = 0;
	var f_total_prcnt_complt = 0;
	var a_sub_practice_arr = new Array();

	for ( var emp_internal_id in projectWiseRevenue) {
		for ( var project_internal_id in projectWiseRevenue[emp_internal_id].RevenueData) {
			var i_sub_practice_internal_id = 0;
			var i_amt = 0;

			if (a_sub_practice_arr
					.indexOf(projectWiseRevenue[emp_internal_id].sub_practice) < 0) {
				for ( var month in projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id]) {
					var a_MonthNames = MonthNames(i_year_project);
					var currentMonthPos = a_MonthNames.indexOf(month);
					var s_current_month = a_MonthNames[currentMonthPos];
					var s_current_mnth_yr = s_current_month.substr(4, 8);

					var i_nxt_yr_flg = 0;
					if (currentMonthPos >= 12) {
						currentMonthPos = currentMonthPos - 12;
						i_nxt_yr_flg = 1;
					}

					var i_curr_yr_dis_flag = 0;
					if (parseInt(s_current_mnth_yr) < i_year_project_end
							&& i_prject_end_mnth < currentMonthPos) {
						i_curr_yr_dis_flag = 1;
					}

					if (((i_project_mnth <= currentMonthPos
							&& ((i_prject_end_mnth >= currentMonthPos || i_curr_yr_dis_flag == 1) && i_nxt_yr_flg == 0) && parseInt(s_current_mnth_yr) <= i_year_project) && parseInt(s_current_mnth_yr) <= i_year_project_end)
							|| (i_prject_end_mnth >= currentMonthPos
									&& parseInt(s_current_mnth_yr) <= i_year_project_end && i_nxt_yr_flg == 1)) {
						var total_revenue_format = parseFloat(projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id][month].total_revenue);
						if (!total_revenue_format)
							total_revenue_format = 0;

						var f_perctn_complete = parseFloat(projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id][month].prcent_complt);
						if (!f_perctn_complete)
							f_perctn_complete = 0;

						if (i_frst_row == 0) {
							a_amount.push(total_revenue_format);
							a_prcnt_arr.push(f_perctn_complete);
						} else {
							var amnt_mnth = a_amount[i_amt];

							var f_revenue_amount = projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id][month].total_revenue;
							if (!f_revenue_amount)
								f_revenue_amount = 0;

							amnt_mnth = parseFloat(amnt_mnth)
									+ parseFloat(f_revenue_amount);
							a_amount[i_amt] = amnt_mnth;
							//i_amt++;

							var f_prcnt_present = a_prcnt_arr[i_amt];

							var f_prcnt_updated = projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id][month].prcent_complt;
							if (!f_prcnt_updated)
								f_prcnt_updated = 0;

							f_prcnt_present = parseFloat(f_prcnt_present)
									+ parseFloat(f_prcnt_updated);
							a_prcnt_arr[i_amt] = f_prcnt_present;
							i_amt++;
						}

						a_sub_practice_arr
								.push(projectWiseRevenue[emp_internal_id].sub_practice);

					}
				}
			}
			i_frst_row = 1;

			var f_revenue_share = projectWiseRevenue[emp_internal_id].revenue_share;
		}
	}

	//prcnt completed for project
	html += "<tr>";

	html += "<td class='label-name'>";
	html += '<b>Monthly Percent Complete';
	html += "</td>";

	for (var i_amt = 0; i_amt < a_amount.length; i_amt++) {
		i_total_row_revenue = parseFloat(i_total_row_revenue)
				+ parseFloat(a_amount[i_amt]);
	}

	for (var i_amt = 0; i_amt < a_amount.length; i_amt++) {
		var f_prcnt_to_diaplay = parseFloat(a_amount[i_amt])
				/ parseFloat(i_total_row_revenue);
		f_prcnt_to_diaplay = parseFloat(f_prcnt_to_diaplay) * 100;

		html += "<td class='projected-amount'>";
		html += '<b>' + parseFloat(f_prcnt_to_diaplay).toFixed(1) + ' %';
		html += "</td>";

		f_total_prcnt_complt = parseFloat(f_total_prcnt_complt)
				+ parseFloat(f_prcnt_to_diaplay);

	}

	html += "<td class='projected-amount'>";
	html += '<b>' + parseFloat(f_total_prcnt_complt).toFixed(1) + ' %';
	html += "</td>";

	html += "</tr>";

	// total revenue recognized project level
	html += "<tr>";

	html += "<td class='label-name'>";
	html += '<b>Revenue to be reconized/month'; //Total Project Revenue as per Plan
	html += "</td>";

	for (var i_amt = 0; i_amt < f_revenue_arr_f.length; i_amt++) {
		html += "<td class='projected-amount'>";
		html += '<b>' + s_currency_symbol_proj + ' '
				+ format2(f_revenue_arr_f[i_amt]);
		html += "</td>";
		i_total_row_revenue_rec = (i_total_row_revenue_rec)
				+ (f_revenue_arr_f[i_amt]);
	}

	html += "<td class='projected-amount'>";
	html += '<b>' + s_currency_symbol_proj + ' '
			+ format2(i_total_row_revenue_rec);
	html += "</td>";

	html += "</tr>";

	// actual revenue that will be recognized
	/*html += "<tr>";
				
	html += "<td class='label-name'>";
	html += '<b>Actual revenue recognized/Revenue forecast'; //Actual Revenue to Recognize
	html += "</td>";
	
	var d_today_date = new Date();
	var i_current_month = d_today_date.getMonth();
	var i_current_year = d_today_date.getFullYear();
	//nlapiLogExecution('audit','proj level len:- ',a_recognized_revenue_total_proj_level_f.length);
	
	var f_total_revenue_proj_level = 0;
	if(parseInt(i_year_project) != parseInt(i_current_year))
	{
		var i_total_project_tenure = 11 - parseInt(i_project_mnth);
		i_current_month = i_current_month + i_total_project_tenure + 1;
		
		for(var i_revenue_index=0; i_revenue_index<i_current_month; i_revenue_index++)
		{
			//i_total_revenue_recognized_ytd = parseFloat(i_total_revenue_recognized_ytd) + parseFloat(a_recognized_revenue[i_revenue_index]);
			
			html += "<td class='projected-amount'>";
			html += '<b>'+s_currency_symbol_proj+' '+format2(a_recognized_revenue_total_proj_level_f[i_revenue_index]);
			html += "</td>";
			
			f_total_revenue_proj_level = parseFloat(f_total_revenue_proj_level) + parseFloat(a_recognized_revenue_total_proj_level_f[i_revenue_index]);
		}
	}
	else
	{
		i_current_month = parseFloat(i_current_month) - parseFloat(i_project_mnth);
		for(var i_revenue_index=0; i_revenue_index<=i_current_month-1; i_revenue_index++)
		{
			//i_total_revenue_recognized_ytd = parseFloat(i_total_revenue_recognized_ytd) + parseFloat(a_recognized_revenue[i_revenue_index]);
			
			html += "<td class='projected-amount'>";
			html += '<b>'+s_currency_symbol_proj+' '+format2(a_recognized_revenue_total_proj_level_f[i_revenue_index]);
			html += "</td>";
			
			f_total_revenue_proj_level = parseFloat(f_total_revenue_proj_level) + parseFloat(a_recognized_revenue_total_proj_level_f[i_revenue_index]);
		}
	}
	
	html += "<td class='projected-amount'>";
	html += '<b>'+s_currency_symbol_proj+' '+format2(f_current_month_actual_revenue_total_proj_level_f);
	html += "</td>";
	
	f_total_revenue_proj_level = parseFloat(f_total_revenue_proj_level) + parseFloat(f_current_month_actual_revenue_total_proj_level_f);
	
	for (var i_amt = i_current_month+1; i_amt < a_amount.length; i_amt++)
	{
		html += "<td class='projected-amount'>";
		html += '<b>'+s_currency_symbol_proj+' '+format2(a_amount[i_amt]);
		html += "</td>";
		
		f_total_revenue_proj_level = parseFloat(f_total_revenue_proj_level) + parseFloat(a_amount[i_amt]);
	}
	
	html += "<td class='projected-amount'>";
	html += '<b>'+s_currency_symbol_proj+' '+format2(f_total_revenue_proj_level);
	html += "</td>";
	
	html += "</tr>";
	
	// cumulative total project level
	html += "<tr>";
	
	html += "<td class='label-name'>";
	html += '<b>Cumulative Revenue';
	html += "</td>";
	
	var f_project_level_cumulative = 0;
	for(var i_revenue_index=0; i_revenue_index<=i_current_month-1; i_revenue_index++)
	{
		//i_total_revenue_recognized_ytd = parseFloat(i_total_revenue_recognized_ytd) + parseFloat(a_recognized_revenue[i_revenue_index]);
		f_project_level_cumulative = parseFloat(f_project_level_cumulative) + parseFloat(a_recognized_revenue_total_proj_level_f[i_revenue_index]);
		html += "<td class='monthly-amount'>";
		html += '<b>'+s_currency_symbol_proj+' '+format2(f_project_level_cumulative);
		html += "</td>";
	}
	
	f_project_level_cumulative = parseFloat(f_project_level_cumulative) + parseFloat(f_current_month_actual_revenue_total_proj_level_f);
	html += "<td class='monthly-amount'>";
	html += '<b>'+s_currency_symbol_proj+' '+format2(f_project_level_cumulative);
	html += "</td>";
	
	for (var i_amt = i_current_month+1; i_amt < a_amount.length; i_amt++)
	{
		f_project_level_cumulative = parseFloat(f_project_level_cumulative) + parseFloat(a_amount[i_amt]);
		html += "<td class='monthly-amount'>";
		html += '<b>'+s_currency_symbol_proj+' '+format2(f_project_level_cumulative);
		html += "</td>";
		
	}
	
	html += "<td class='monthly-amount'>";
	html += '<b>'+s_currency_symbol_proj+' '+format2(f_total_revenue_proj_level);
	html += "</td>";
	
	html += "</tr>";*/

	html += "</table>";

	return html;
}

function generate_table_effort_view_f(projectWiseRevenue, monthBreakUp,
		i_year_project, i_project_mnth, mode, s_currency_symbol_proj,
		s_currency_symbol_usd, i_year_project_end) {
	var css_file_url = "";

	var context = nlapiGetContext();
	if (context.getEnvironment() == "SANDBOX") {
		css_file_url = "https://system.sandbox.netsuite.com/core/media/media.nl?id=199133&c=3883006&h=64fa194295cd4ab64d58&mv=iq3kjr6g&_xt=.css&whence=";
	} else if (context.getEnvironment() == "PRODUCTION") {
		css_file_url = "https://system.na1.netsuite.com/core/media/media.nl?id=199133&c=3883006&h=64fa194295cd4ab64d58&mv=iq3kjr6g&_xt=.css&whence=";
	}

	var html = "";
	html += "<link href='" + css_file_url + "'  rel='stylesheet'>";

	html += "<table class='projection-table'>";

	// header
	html += "<tr class='header-row'>";

	html += "<td>";
	html += "Practice";
	html += "</td>";

	html += "<td>";
	html += "Sub Practice";
	html += "</td>";

	if (mode != 5) {
		html += "<td>";
		html += "Role";
		html += "</td>";

		if (mode == 1 || mode == 2 || mode == 6) {
			html += "<td>";
			html += "Level";
			html += "</td>";

			if (mode == 6) {
				html += "<td>";
				html += "No. of resources";
				html += "</td>";

				html += "<td>";
				html += "Allocation start date";
				html += "</td>";

				html += "<td>";
				html += "Allocation end date";
				html += "</td>";

				html += "<td>";
				html += "Percent allocated";
				html += "</td>";
			}

			html += "<td>";
			html += "Location";
			html += "</td>";
		}
	} else {
		html += "<td>";
		html += "Revenue Share";
		html += "</td>";
	}

	if (mode != 5 && mode != 6) {
		var today = new Date();
		var currentMonthName = getMonthName(nlapiDateToString(today));

		for (var j = 0; j < monthBreakUp.length; j++) {
			var months = monthBreakUp[j];
			var s_month_name = getMonthName(months.Start); // get month name
			var s_year = nlapiStringToDate(months.Start)
			s_year = s_year.getFullYear();
			s_month_name = s_month_name + '_' + s_year;

			html += "<td>";
			html += s_month_name;
			html += "</td>";
		}

		if (mode == 2) {
			html += "<td>";
			html += "Total";
			html += "</td>";
		}
	}

	html += "</tr>";

	var a_sub_prac_already_displayed = new Array();
	var i_subpractice_id = 0;
	for ( var emp_internal_id in projectWiseRevenue) {

		for ( var project_internal_id in projectWiseRevenue[emp_internal_id].RevenueData) {

			var b_display_reveue_share = 'F';
			if (a_sub_prac_already_displayed
					.indexOf(projectWiseRevenue[emp_internal_id].sub_practice) < 0
					&& mode == 5) {
				html += "<tr>";
				html += "<td class='label-name'>";
				html += projectWiseRevenue[emp_internal_id].practice_name;
				html += "</td>";

				html += "<td class='label-name'>";
				html += projectWiseRevenue[emp_internal_id].sub_prac_name;
				html += "</td>";

				b_display_reveue_share = 'T';
				a_sub_prac_already_displayed
						.push(projectWiseRevenue[emp_internal_id].sub_practice);
			}

			if (mode != 5) {
				html += "<tr>";
				html += "<td class='label-name'>";
				html += projectWiseRevenue[emp_internal_id].practice_name;
				html += "</td>";

				html += "<td class='label-name'>";
				html += projectWiseRevenue[emp_internal_id].sub_prac_name;
				html += "</td>";
			}

			if (mode != 5) {
				html += "<td class='label-name'>";
				html += projectWiseRevenue[emp_internal_id].role_name;
				html += "</td>";

				if (mode == 1 || mode == 2 || mode == 6) {
					html += "<td class='label-name'>";
					html += projectWiseRevenue[emp_internal_id].level_name;
					html += "</td>";

					if (mode == 6) {
						html += "<td class='label-name'>";
						html += projectWiseRevenue[emp_internal_id].no_of_resources;
						html += "</td>";

						html += "<td class='label-name'>";
						html += projectWiseRevenue[emp_internal_id].allocation_strt_date;
						html += "</td>";

						html += "<td class='label-name'>";
						html += projectWiseRevenue[emp_internal_id].allocation_end_date;
						html += "</td>";

						html += "<td class='label-name'>";
						html += projectWiseRevenue[emp_internal_id].total_allocation;
						html += "</td>";
					}

					html += "<td class='label-name'>";
					html += projectWiseRevenue[emp_internal_id].location_name;
					html += "</td>";
				}
			} else {
				if (b_display_reveue_share == 'T') {
					html += "<td class='label-name'>";
					html += ''
							+ s_currency_symbol_proj
							+ ' '
							+ format2(projectWiseRevenue[emp_internal_id].revenue_share);
					html += "</td>";
				}
			}

			if (mode != 5 && mode != 6) {
				var i_sub_practice_internal_id = 0;
				for ( var month in projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id]) {
					var a_MonthNames = MonthNames(i_year_project);
					var currentMonthPos = a_MonthNames.indexOf(month);
					var s_current_month = a_MonthNames[currentMonthPos];
					var s_current_mnth_yr = s_current_month.substr(4, 8);

					var i_nxt_yr_flg = 0;
					if (currentMonthPos >= 12) {
						currentMonthPos = currentMonthPos - 12;
						i_nxt_yr_flg = 1;
					}

					var i_curr_yr_dis_flag = 0;
					if (parseInt(s_current_mnth_yr) < i_year_project_end
							&& i_prject_end_mnth < currentMonthPos) {
						i_curr_yr_dis_flag = 1;
					}

					if (((i_project_mnth <= currentMonthPos
							&& ((i_prject_end_mnth >= currentMonthPos || i_curr_yr_dis_flag == 1) && i_nxt_yr_flg == 0) && parseInt(s_current_mnth_yr) <= i_year_project) && parseInt(s_current_mnth_yr) <= i_year_project_end)
							|| (i_prject_end_mnth >= currentMonthPos
									&& parseInt(s_current_mnth_yr) <= i_year_project_end && i_nxt_yr_flg == 1)) {

						if (mode == 2) {
							html += "<td class='monthly-amount'>";
							var total_revenue_format = parseFloat(
									projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id][month].Amount)
									.toFixed(1);
							html += format2(total_revenue_format);
						} else if (mode == 1) {
							html += "<td class='monthly-amount'>";
							html += parseFloat(
									projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id][month].prcnt_allocated)
									.toFixed(1);

							if (i_sub_practice_internal_id = 0) {
								i_sub_practice_internal_id = projectWiseRevenue[emp_internal_id].sub_practice;
							}
						} else if (mode == 3 || mode == 4) {
							var f_amount = projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id][month].Amount;
							var f_total_amount = projectWiseRevenue[emp_internal_id].total_revenue_for_tenure;

							var f_prcnt_cost = parseFloat(f_amount)
									/ parseFloat(f_total_amount);
							var f_prcnt_cost_ = parseFloat(f_prcnt_cost)
									* parseFloat(100);

							if (mode == 3) {
								f_prcnt_cost_ = parseFloat(f_prcnt_cost_)
										.toFixed(1);
								html += "<td class='monthly-amount'>";
								html += f_prcnt_cost_ + ' %';
							} else {
								html += "<td class='projected-amount'>";
								var f_revenue_share = projectWiseRevenue[emp_internal_id].revenue_share;
								if (projectWiseRevenue[emp_internal_id].sub_practice == 316)
									nlapiLogExecution('audit', 'prcnt:- '
											+ f_prcnt_cost, f_revenue_share);

								f_revenue_share = (parseFloat(f_prcnt_cost_) * parseFloat(f_revenue_share)) / 100;
								html += format2(f_revenue_share);
							}
						}

						html += "</td>";
					}
				}
			}

			if (mode == 2) {
				html += "<td class='monthly-amount'>";
				html += format2(projectWiseRevenue[emp_internal_id].total_revenue_for_tenure);
				html += "</td>";
			}

			html += "</tr>";
		}
	}

	html += "</table>";

	return html;
}

function format2(n) {
	n = parseFloat(n).toFixed(1).replace(/(\d)(?=(\d{3})+\.)/g, "$1,");
	n = n.toString().split('.');
	return n[0];
}

function getCurrency_Symbol(s_proj_currency) {
	var s_currency_symbols = {
		'USD' : '$', // US Dollar
		'EUR' : 'â‚¬', // Euro
		'GBP' : 'Â£', // British Pound Sterling
		'INR' : 'â‚¹', // Indian Rupee
	};

	if (s_currency_symbols[s_proj_currency] !== undefined) {
		return s_currency_symbols[s_proj_currency];
	}
}

function getMonthsBreakup(d_startDate, d_endDate) {

	try {
		var dateBreakUp = [];
		var new_start_date = new Date(d_startDate.getFullYear(), d_startDate
				.getMonth(), 1);

		var nxt_mnth_strt_date = new Date(d_startDate.getFullYear(),
				d_startDate.getMonth() + 1, 1);

		var endDate = new Date(nxt_mnth_strt_date - 1);

		//var endDate = nlapiAddDays(nlapiAddMonths(d_startDate, 1), -1);
		endDate = nlapiDateToString(endDate, 'date');

		dateBreakUp.push({
			Start : nlapiDateToString(new_start_date, 'date'),
			End : endDate
		});

		for (var i = 1;; i++) {
			var new_date = nlapiAddMonths(new_start_date, i);

			if (new_date > d_endDate) {
				break;
			}

			endDate = nlapiAddDays(nlapiAddMonths(new_date, 1), -1);
			endDate = nlapiDateToString(endDate, 'date');

			dateBreakUp.push({
				Start : nlapiDateToString(new_date, 'date'),
				End : endDate
			});
		}
	} catch (err) {
		nlapiLogExecution('ERROR', 'getMonthsBreakup', 'ERROR MESSAGE :- '
				+ err);
	}

	return dateBreakUp;
}

function getMonthName(currentDate) {
	currentDate = nlapiStringToDate(currentDate)
	var monthNames = [ "JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JUL", "AUG",
			"SEP", "OCT", "NOV", "DEC" ];
	return monthNames[currentDate.getMonth()];
}

function MonthNames(yr) {
	var yr1 = Number(yr) + 1;

	var monthNames = [ "JAN_" + yr, "FEB_" + yr, "MAR_" + yr, "APR_" + yr,
			"MAY_" + yr, "JUN_" + yr, "JUL_" + yr, "AUG_" + yr, "SEP_" + yr,
			"OCT_" + yr, "NOV_" + yr, "DEC_" + yr ];
	var month_nxt_yr = [ "JAN_" + yr1, "FEB_" + yr1, "MAR_" + yr1,
			"APR_" + yr1, "MAY_" + yr1, "JUN_" + yr1, "JUL_" + yr1,
			"AUG_" + yr1, "SEP_" + yr1, "OCT_" + yr1, "NOV_" + yr1,
			"DEC_" + yr1 ];
	monthNames = monthNames.concat(month_nxt_yr);

	return monthNames;
}

function yearData(yr) {
	var yr1 = Number(yr) + 1;

	var monthNames = [ "JAN_" + yr, "FEB_" + yr, "MAR_" + yr, "APR_" + yr,
			"MAY_" + yr, "JUN_" + yr, "JUL_" + yr, "AUG_" + yr, "SEP_" + yr,
			"OCT_" + yr, "NOV_" + yr, "DEC_" + yr ];
	var month_nxt_yr = [ "JAN_" + yr1, "FEB_" + yr1, "MAR_" + yr1,
			"APR_" + yr1, "MAY_" + yr1, "JUN_" + yr1, "JUL_" + yr1,
			"AUG_" + yr1, "SEP_" + yr1, "OCT_" + yr1, "NOV_" + yr1,
			"DEC_" + yr1 ];
	monthNames = monthNames.concat(month_nxt_yr);

	for (var i = 0; i < monthNames.length; i++) {
		this[monthNames[i]] = {
			Amount : 0,
			prcnt_allocated : 0,
			total_revenue : 0,
			recognized_revenue : 0,
			actual_revenue : 0,
			mnth_strt : 0,
			mnth_end : 0,
			prcent_complt : 0
		};
	}
}

function generate_previous_effrt_revenue_f(s_effrt_json,
		projectWiseRevenue_previous_effrt, a_revenue_recognized_for_project,
		i_year_project) {
	if (s_effrt_json) {
		var i_practice_previous = 0;
		var f_total_revenue_for_tenure = 0;
		//for (var i_mnth_end_plan = 0; i_mnth_end_plan < a_get_mnth_end_effrt_plan.length; i_mnth_end_plan++)
		var s_entire_json_clubed = JSON.parse(s_effrt_json);
		for (var i_mnth_end_plan = 0; i_mnth_end_plan < s_entire_json_clubed.length; i_mnth_end_plan++) {
			var a_row_json_data = JSON.parse(JSON
					.stringify(s_entire_json_clubed[i_mnth_end_plan]));
			for (var i_row_json_index = 0; i_row_json_index < a_row_json_data.length; i_row_json_index++) {
				var i_practice = a_row_json_data[i_row_json_index].prac;
				var s_practice = a_row_json_data[i_row_json_index].prac_text;
				var i_sub_practice = a_row_json_data[i_row_json_index].subprac;
				var s_sub_practice = a_row_json_data[i_row_json_index].subprac_text;
				var i_role = a_row_json_data[i_row_json_index].role;
				var s_role = a_row_json_data[i_row_json_index].role_text;
				var i_level = a_row_json_data[i_row_json_index].level;
				var s_level = a_row_json_data[i_row_json_index].level_text;
				var i_location = a_row_json_data[i_row_json_index].loc;
				var s_location = a_row_json_data[i_row_json_index].loc_text;
				var f_revenue = a_row_json_data[i_row_json_index].cost;
				if (!f_revenue)
					f_revenue = 0;

				var f_revenue_share = a_row_json_data[i_row_json_index].share;
				if (!f_revenue_share)
					f_revenue_share = 0;

				var i_no_of_resources = a_row_json_data[i_row_json_index].allo;
				var s_mnth_strt_date = '1/31/2017';
				var s_mnth_end_date = '31/31/2017';
				var s_mnth = a_row_json_data[i_row_json_index].mnth;
				var s_year = a_row_json_data[i_row_json_index].year;

				var f_revenue_recognized = find_recognized_revenue_prev_f(
						a_revenue_recognized_for_project, i_practice,
						i_sub_practice, i_role, i_level, s_mnth, s_year,
						a_prev_subprac_searched_once);

				if (!f_revenue_recognized)
					f_revenue_recognized = 0;

				//nlapiLogExecution('audit','f_revenue_recognized:- '+f_revenue_recognized);

				var s_month_name = s_mnth + '_' + s_year;

				var total_revenue = parseFloat(i_no_of_resources)
						* parseFloat(f_revenue);
				total_revenue = parseFloat(total_revenue).toFixed(2);

				if (i_practice_previous == 0) {
					i_practice_previous = a_row_json_data[i_row_json_index].prac_row;
				}

				if (i_practice_previous != a_row_json_data[i_row_json_index].prac_row) {
					f_total_revenue_for_tenure = 0;
					i_practice_previous = a_row_json_data[i_row_json_index].prac_row;
				}

				f_total_revenue_for_tenure = parseFloat(f_total_revenue_for_tenure)
						+ parseFloat(total_revenue);
				f_total_revenue_for_tenure = parseFloat(
						f_total_revenue_for_tenure).toFixed(2);

				var i_prac_subPrac_role_level = i_practice + '_'
						+ i_sub_practice + '_' + i_role + '_' + i_level + '_'
						+ i_location;

				if (!projectWiseRevenue_previous_effrt[i_prac_subPrac_role_level]) {
					projectWiseRevenue_previous_effrt[i_prac_subPrac_role_level] = {
						practice_name : s_practice,
						sub_prac_name : s_sub_practice,
						sub_practice : i_sub_practice,
						role_name : s_role,
						level_name : s_level,
						location_name : s_location,
						total_revenue_for_tenure : f_total_revenue_for_tenure,
						revenue_share : f_revenue_share,
						RevenueData : {}
					};

					projectWiseRevenue_previous_effrt[i_prac_subPrac_role_level].RevenueData[i_practice] = new yearData(
							i_year_project);

					projectWiseRevenue_previous_effrt[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].Amount = total_revenue;
					projectWiseRevenue_previous_effrt[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].prcnt_allocated = i_no_of_resources;
					projectWiseRevenue_previous_effrt[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].recognized_revenue = f_revenue_recognized;
					projectWiseRevenue_previous_effrt[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].mnth_strt = s_mnth_strt_date;
					projectWiseRevenue_previous_effrt[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].mnth_end = s_mnth_end_date;
				} else {
					projectWiseRevenue_previous_effrt[i_prac_subPrac_role_level].total_revenue_for_tenure = f_total_revenue_for_tenure;

					projectWiseRevenue_previous_effrt[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].Amount = total_revenue;
					projectWiseRevenue_previous_effrt[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].prcnt_allocated = i_no_of_resources;
					projectWiseRevenue_previous_effrt[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].recognized_revenue = f_revenue_recognized;
					projectWiseRevenue_previous_effrt[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].mnth_strt = s_mnth_strt_date;
					projectWiseRevenue_previous_effrt[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].mnth_end = s_mnth_end_date;
				}
			}
		}
	}

	//return projectWiseRevenue_previous_effrt;
}

function find_recognized_revenue_prev_f(a_revenue_recognized_for_project,
		practice, subpractice, role, level, month, year,
		a_prev_subprac_searched_once) {
	var f_recognized_amount = 0;

	if (a_prev_subprac_searched_once.indexOf(subpractice) < 0) {
		for (var i_index_find = 0; i_index_find < a_revenue_recognized_for_project.length; i_index_find++) {
			if (practice == a_revenue_recognized_for_project[i_index_find].practice_revenue_recognized) {
				if (subpractice == a_revenue_recognized_for_project[i_index_find].subpractice_revenue_recognized) {
					//if(role == a_revenue_recognized_for_project[i_index_find].role_revenue_recognized)
					{
						//if(level == a_revenue_recognized_for_project[i_index_find].level_revenue_recognized)
						{
							if (month == a_revenue_recognized_for_project[i_index_find].month_revenue_recognized) {
								if (year == a_revenue_recognized_for_project[i_index_find].year_revenue_recognized) {
									f_recognized_amount = a_revenue_recognized_for_project[i_index_find].amount_revenue_recognized;
									//nlapiLogExecution('audit','f_recognized_amount:- '+f_recognized_amount);
									a_prev_subprac_searched_once
											.push(subpractice);
								}
							}
						}
					}
				}
			}
		}

		return f_recognized_amount;
	} else {
		return 0;
	}
}

function find_recognized_revenue_f(a_revenue_recognized_for_project, practice,
		subpractice, role, level, month, year, a_subprac_searched_once,
		a_subprac_searched_once_month, a_subprac_searched_once_year) {
	var f_recognized_amount = 0;

	if (func_search_prac_processed_fr_mnth_f(a_subprac_searched_once,
			subpractice, month, year) < 0) {
		for (var i_index_find = 0; i_index_find < a_revenue_recognized_for_project.length; i_index_find++) {
			if (practice == a_revenue_recognized_for_project[i_index_find].practice_revenue_recognized) {
				if (subpractice == a_revenue_recognized_for_project[i_index_find].subpractice_revenue_recognized) {
					//if(role == a_revenue_recognized_for_project[i_index_find].role_revenue_recognized)
					{
						//if(level == a_revenue_recognized_for_project[i_index_find].level_revenue_recognized)
						{
							if (month == a_revenue_recognized_for_project[i_index_find].month_revenue_recognized) {
								if (parseInt(year) == parseInt(a_revenue_recognized_for_project[i_index_find].year_revenue_recognized)) {
									f_recognized_amount = a_revenue_recognized_for_project[i_index_find].amount_revenue_recognized;
									//nlapiLogExecution('audit','f_recognized_amount inside look for prev mnth amnt once pr sub prac:- ',f_recognized_amount);
									a_subprac_searched_once.push({
										'subpractice' : subpractice,
										'month' : month,
										'year' : year
									});
									//a_subprac_searched_once_month.push(month);
									//a_subprac_searched_once_year.push(year);

									if (subpractice == 325) {
										nlapiLogExecution('audit', 'month:- '
												+ month, 'year:- ' + year);
										nlapiLogExecution('audit',
												'f_recognized_amount:- '
														+ f_recognized_amount);
									}
								}
							}
						}
					}
				}
			}
		}

		return f_recognized_amount;
	} else {
		return 0;
	}
}

function func_search_prac_processed_fr_mnth_f(a_subprac_searched_once,
		subpractice, month, year) {
	var i_return_var = -1;
	for (var i_loop = 0; i_loop < a_subprac_searched_once.length; i_loop++) {
		if (a_subprac_searched_once[i_loop].subpractice == subpractice) {
			if (a_subprac_searched_once[i_loop].month == month) {
				if (a_subprac_searched_once[i_loop].year == year) {
					i_return_var = i_loop;
					break;
				}
			}
		}
	}

	return i_return_var;
}
function generate_total_cost_f(projectWiseRevenue, monthBreakUp,
		i_year_project, i_project_mnth, mode, sub_prac, s_currency_symbol_proj,
		s_currency_symbol_usd, i_year_project_end, i_prject_end_mnth,
		f_prev_mnth_cost) {
	var css_file_url = "";

	var context = nlapiGetContext();
	if (context.getEnvironment() == "SANDBOX") {
		css_file_url = "https://system.sandbox.netsuite.com/core/media/media.nl?id=199133&c=3883006&h=64fa194295cd4ab64d58&mv=iq3kjr6g&_xt=.css&whence=";
	} else if (context.getEnvironment() == "PRODUCTION") {
		css_file_url = "https://system.na1.netsuite.com/core/media/media.nl?id=199133&c=3883006&h=64fa194295cd4ab64d58&mv=iq3kjr6g&_xt=.css&whence=";
	}

	var html = "";
	html += "<link href='" + css_file_url + "'  rel='stylesheet'>";

	html += "<table class='projection-table'>";

	// header
	html += "<tr class='header-row'>";

	html += "<td>";
	html += "";
	html += "</td>";

	html += "<td width:15px>";
	html += "Practice";
	html += "</td>";

	html += "<td width:30px>";
	html += "Sub Practice";
	html += "</td>";

	html += "<td width=\"10%\">";
	html += "Role";
	html += "</td>";

	html += "<td width=\"10%\">";
	html += "Level";
	html += "</td>";

	html += "<td>";
	html += "Location";
	html += "</td>";

	var today = new Date();
	var currentMonthName = getMonthName(nlapiDateToString(today));

	for (var j = 0; j < monthBreakUp.length; j++) {
		var months = monthBreakUp[j];
		var s_month_name = getMonthName(months.Start); // get month name
		var s_year = nlapiStringToDate(months.Start)
		s_year = s_year.getFullYear();
		s_month_name = s_month_name + '_' + s_year;

		html += "<td>";
		html += s_month_name;
		html += "</td>";
	}

	html += "<td>";
	html += "Total";
	html += "</td>";

	html += "</tr>";

	var a_sub_prac_already_displayed = new Array();
	var i_subpractice_id = 0;
	var a_amount = new Array();
	var a_recognized_revenue = new Array();
	var i_frst_row = 0;
	var i_diplay_frst_column = 0;
	var i_total_row_revenue = 0;
	var i_total_per_row = 0;
	var total_prcnt_aloocated = new Array();
	var b_first_line_copied = 0;
	var f_total_allocated_row = 0;

	for ( var emp_internal_id in projectWiseRevenue) {
		if (projectWiseRevenue[emp_internal_id].sub_practice == sub_prac) {
			f_total_allocated_row = 0;
			for ( var project_internal_id in projectWiseRevenue[emp_internal_id].RevenueData) {
				html += "<tr>";

				if (i_diplay_frst_column == 0) {
					html += "<td class='label-name'>";
					html += '<b>Effort View';
					html += "</td>";
				} else {
					html += "<td class='label-name'>";
					html += '<b>';
					html += "</td>";
				}

				html += "<td class='label-name' width:15px>";
				html += projectWiseRevenue[emp_internal_id].practice_name;
				html += "</td>";

				html += "<td class='label-name' width:30px>";
				html += projectWiseRevenue[emp_internal_id].sub_prac_name;
				html += "</td>";

				html += "<td class='label-name' width=\"10%\">";
				html += projectWiseRevenue[emp_internal_id].role_name;
				html += "</td>";

				html += "<td class='label-name' width=\"10%\">";
				html += projectWiseRevenue[emp_internal_id].level_name;
				html += "</td>";

				html += "<td class='label-name'>";
				html += projectWiseRevenue[emp_internal_id].location_name;
				html += "</td>";

				var i_index_mnth = 0;
				for ( var month in projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id]) {
					var a_MonthNames = MonthNames(i_year_project);
					var currentMonthPos = a_MonthNames.indexOf(month);
					var s_current_month = a_MonthNames[currentMonthPos];
					var s_current_mnth_yr = s_current_month.substr(4, 8);

					var i_nxt_yr_flg = 0;
					if (currentMonthPos >= 12) {
						currentMonthPos = currentMonthPos - 12;
						i_nxt_yr_flg = 1;
					}

					var i_curr_yr_dis_flag = 0;
					if (parseInt(s_current_mnth_yr) < i_year_project_end
							&& i_prject_end_mnth < currentMonthPos) {
						i_curr_yr_dis_flag = 1;
					}

					if (((i_project_mnth <= currentMonthPos
							&& ((i_prject_end_mnth >= currentMonthPos || i_curr_yr_dis_flag == 1) && i_nxt_yr_flg == 0) && parseInt(s_current_mnth_yr) <= i_year_project) && parseInt(s_current_mnth_yr) <= i_year_project_end)
							|| (i_prject_end_mnth >= currentMonthPos
									&& parseInt(s_current_mnth_yr) <= i_year_project_end && i_nxt_yr_flg == 1)) {
						//nlapiLogExecution('audit','currentMonthPos:- ',+currentMonthPos);
						if (b_first_line_copied == 1) {
							var f_allocated_prcnt = total_prcnt_aloocated[i_index_mnth];
							f_allocated_prcnt = parseFloat(f_allocated_prcnt)
									+ parseFloat(projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id][month].prcnt_allocated);
							total_prcnt_aloocated[i_index_mnth] = f_allocated_prcnt;
							i_index_mnth++;
						} else {
							total_prcnt_aloocated
									.push(projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id][month].prcnt_allocated);
						}

						f_total_allocated_row = parseFloat(projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id][month].prcnt_allocated)
								+ parseFloat(f_total_allocated_row);

						html += "<td class='monthly-amount'>";
						html += parseFloat(
								projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id][month].prcnt_allocated)
								.toFixed(2);
						html += "</td>";
					}
				}
			}

			b_first_line_copied = 1;

			html += "<td class='monthly-amount'>";
			html += parseFloat(f_total_allocated_row).toFixed(2);
			html += "</td>";

			html += "</tr>";

			var s_sub_prac_name = projectWiseRevenue[emp_internal_id].sub_prac_name;
			var s_practice_name = projectWiseRevenue[emp_internal_id].practice_name;

			i_diplay_frst_column = 1;
		}
	}
	i_diplay_frst_column = 0;

	html += "<tr>";
	html += "<td class='label-name'>";
	html += '<b>Sub Total';
	html += "</td>";

	html += "<td class='label-name'>";
	html += '<b>' + s_practice_name;
	html += "</td>";

	html += "<td class='label-name'>";
	html += '<b>' + s_sub_prac_name;
	html += "</td>";

	html += "<td class='label-name'>";
	html += '';
	html += "</td>";

	html += "<td class='label-name'>";
	html += '';
	html += "</td>";

	html += "<td class='label-name'>";
	html += '';
	html += "</td>";

	var f_total_row_allocation = 0;
	for (var i_index_total_allocated = 0; i_index_total_allocated < total_prcnt_aloocated.length; i_index_total_allocated++) {
		html += "<td class='monthly-amount'>";
		html += '<b>'
				+ parseFloat(total_prcnt_aloocated[i_index_total_allocated])
						.toFixed(2);
		html += "</td>";

		f_total_row_allocation = parseFloat(f_total_row_allocation)
				+ parseFloat(total_prcnt_aloocated[i_index_total_allocated]);
	}

	html += "<td class='monthly-amount'>";
	html += '<b>' + parseFloat(f_total_row_allocation).toFixed(2);
	html += "</td>";

	html += "</tr>";

	for ( var emp_internal_id in projectWiseRevenue) {

		for ( var project_internal_id in projectWiseRevenue[emp_internal_id].RevenueData) {

			if (projectWiseRevenue[emp_internal_id].sub_practice == sub_prac) {
				html += "<tr>";

				if (i_diplay_frst_column == 0) {
					html += "<td class='label-name'>";
					html += '<b>Cost View';
					html += "</td>";
				} else {
					html += "<td class='label-name'>";
					html += '<b>';
					html += "</td>";
				}

				html += "<td class='label-name'>";
				html += projectWiseRevenue[emp_internal_id].practice_name;
				html += "</td>";

				html += "<td class='label-name'>";
				html += projectWiseRevenue[emp_internal_id].sub_prac_name;
				html += "</td>";

				html += "<td class='label-name'>";
				html += projectWiseRevenue[emp_internal_id].role_name;
				html += "</td>";

				html += "<td class='label-name'>";
				html += projectWiseRevenue[emp_internal_id].level_name;
				html += "</td>";

				html += "<td class='label-name'>";
				html += projectWiseRevenue[emp_internal_id].location_name;
				html += "</td>";

				var i_sub_practice_internal_id = 0;
				var i_amt = 0;
				for ( var month in projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id]) {
					var a_MonthNames = MonthNames(i_year_project);
					var currentMonthPos = a_MonthNames.indexOf(month);
					var s_current_month = a_MonthNames[currentMonthPos];
					var s_current_mnth_yr = s_current_month.substr(4, 8);

					var i_nxt_yr_flg = 0;
					if (currentMonthPos >= 12) {
						currentMonthPos = currentMonthPos - 12;
						i_nxt_yr_flg = 1;
					}

					var i_curr_yr_dis_flag = 0;
					if (parseInt(s_current_mnth_yr) < i_year_project_end
							&& i_prject_end_mnth < currentMonthPos) {
						i_curr_yr_dis_flag = 1;
					}

					if (((i_project_mnth <= currentMonthPos
							&& ((i_prject_end_mnth >= currentMonthPos || i_curr_yr_dis_flag == 1) && i_nxt_yr_flg == 0) && parseInt(s_current_mnth_yr) <= i_year_project) && parseInt(s_current_mnth_yr) <= i_year_project_end)
							|| (i_prject_end_mnth >= currentMonthPos
									&& parseInt(s_current_mnth_yr) <= i_year_project_end && i_nxt_yr_flg == 1)) {
						if (mode == 2) {

							html += "<td class='monthly-amount'>";
							var total_revenue_format = parseFloat(
									projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id][month].Amount)
									.toFixed(1);
							html += '' + s_currency_symbol_proj + ' '
									+ format2(total_revenue_format);

							if (i_frst_row == 0) {
								//nlapiLogExecution('audit','inside push recog amnt:- '+projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id][month].recognized_revenue);
								a_amount.push(total_revenue_format);
								a_recognized_revenue
										.push(projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id][month].recognized_revenue);
							} else {
								var amnt_mnth = a_amount[i_amt];
								amnt_mnth = parseFloat(amnt_mnth)
										+ parseFloat(projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id][month].Amount);

								a_amount[i_amt] = amnt_mnth;

								var i_existing_recognised_amount = a_recognized_revenue[i_amt];
								//nlapiLogExecution('audit','month name:- '+month);
								//nlapiLogExecution('audit','index:- '+i_amt+'::existing rev:- '+i_existing_recognised_amount,'Rev REcg:- '+projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id][month].recognized_revenue);
								i_existing_recognised_amount = parseFloat(i_existing_recognised_amount)
										+ parseFloat(projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id][month].recognized_revenue);

								a_recognized_revenue[i_amt] = i_existing_recognised_amount;

								i_amt++;
							}
							i_total_row_revenue = parseFloat(i_total_row_revenue)
									+ parseFloat(projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id][month].Amount);
							i_total_per_row = parseFloat(i_total_per_row)
									+ parseFloat(projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id][month].Amount);
						}

						html += "</td>";
					}
				}

				if (mode == 2) {
					html += "<td class='monthly-amount'>";
					//html += ''+s_currency_symbol_usd+' '+format2(projectWiseRevenue[emp_internal_id].total_revenue_for_tenure);
					html += '' + s_currency_symbol_proj + ' '
							+ format2(i_total_per_row);
					html += "</td>";
					i_total_per_row = 0;
					//i_total_row_revenue = parseFloat(i_total_row_revenue) + parseFloat(projectWiseRevenue[emp_internal_id].total_revenue_for_tenure);
				}

				i_frst_row = 1;

				html += "</tr>";

				i_diplay_frst_column = 1;

				var f_revenue_share = projectWiseRevenue[emp_internal_id].revenue_share;
			}
		}
	}

	var s_sub_prac_name = '';
	for ( var emp_internal_id in projectWiseRevenue) {
		if (projectWiseRevenue[emp_internal_id].sub_practice == sub_prac) {
			s_sub_prac_name = projectWiseRevenue[emp_internal_id].sub_prac_name;
			s_practice_name = projectWiseRevenue[emp_internal_id].practice_name;
			//projectWiseRevenue[emp_internal_id].revenue_share_revised = i_total_row_revenue;
			//projectWiseRevenue[emp_internal_id].total_revenue_for_tenure = i_total_row_revenue;
		}
	}

	html += "</tr>";

	html += "<td class='label-name'>";
	html += '<b>Sub total';
	html += "</td>";

	html += "<td class='label-name'>";
	html += '<b>' + s_practice_name;
	html += "</td>";

	html += "<td class='label-name'>";
	html += '<b>' + s_sub_prac_name;
	html += "</td>";

	html += "<td class='label-name'>";
	html += '';
	html += "</td>";

	html += "<td class='label-name'>";
	html += '';
	html += "</td>";

	html += "<td class='label-name'>";
	html += '';
	html += "</td>";
	if (flag_counter_f == 0) {
		for (var i_amt = 0; i_amt < a_amount.length; i_amt++) {
			f_total_cost_overall_prac_f[i_amt] = 0;

		}
	}
	for (var i_amt = 0; i_amt < a_amount.length; i_amt++) {
		html += "<td class='projected-amount'>";
		html += '<b>' + s_currency_symbol_proj + ' ' + format2(a_amount[i_amt]);
		html += "</td>";
		f_total_cost_overall_prac_f[i_amt] = parseFloat(a_amount[i_amt])
				+ parseFloat(f_total_cost_overall_prac_f[i_amt]);
		flag_counter_f = 1;
	}

	html += "<td class='projected-amount'>";
	html += '<b>' + s_currency_symbol_proj + ' ' + format2(i_total_row_revenue);
	html += "</td>";

	html += "</tr>";

	// Percent row
	html += "</tr>";

	html += "<td class='label-name'>";
	html += '<b>Monthly % Cost Completion';
	html += "</td>";

	html += "<td class='label-name'>";
	html += '<b>' + s_practice_name;
	html += "</td>";

	html += "<td class='label-name'>";
	html += '<b>' + s_sub_prac_name;
	html += "</td>";

	html += "<td class='label-name'>";
	html += '';
	html += "</td>";

	html += "<td class='label-name'>";
	html += '';
	html += "</td>";

	html += "<td class='label-name'>";
	html += '';
	html += "</td>";

	var f_total_prcnt = 0;
	for (var i_amt = 0; i_amt < a_amount.length; i_amt++) {
		var f_prcnt_revenue = parseFloat(a_amount[i_amt])
				/ parseFloat(i_total_row_revenue);
		f_prcnt_revenue = f_prcnt_revenue * 100;
		if (!f_prcnt_revenue)
			f_prcnt_revenue = 0;

		f_total_prcnt = parseFloat(f_prcnt_revenue) + parseFloat(f_total_prcnt);

		html += "<td class='projected-amount'>";
		html += '<b>' + parseFloat(f_prcnt_revenue).toFixed(1) + ' %';
		html += "</td>";
	}

	html += "<td class='projected-amount'>";
	html += '<b>' + parseFloat(f_total_prcnt).toFixed(1) + ' %';
	html += "</td>";

	html += "</tr>";

	//Total reveue recognized per month row
	html += "<tr>";

	html += "<td class='label-name'>";
	html += '<b>Revenue to be reconized/month';
	html += "</td>";

	html += "<td class='label-name'>";
	html += '<b>' + s_practice_name;
	html += "</td>";

	html += "<td class='label-name'>";
	html += '<b>' + s_sub_prac_name;
	html += "</td>";

	html += "<td class='label-name'>";
	html += '';
	html += "</td>";

	html += "<td class='label-name'>";
	html += '';
	html += "</td>";

	html += "<td class='label-name'>";
	html += '';
	html += "</td>";

	var d_today_date = new Date();
	var i_current_month = d_today_date.getMonth();
	var i_current_year = d_today_date.getFullYear();

	var f_total_revenue = 0;
	var f_revenue_amount_till_current_month = 0;
	for (var i_amt = 0; i_amt < a_amount.length; i_amt++) {
		var f_prcnt_revenue = parseFloat(a_amount[i_amt])
				/ parseFloat(i_total_row_revenue);
		f_prcnt_revenue = f_prcnt_revenue * 100;

		var f_revenue_amount = (parseFloat(f_prcnt_revenue) * parseFloat(f_revenue_share)) / 100;
		if (!f_revenue_amount)
			f_revenue_amount = 0;

		if (parseInt(i_year_project) != parseInt(i_current_year)) {
			var i_total_project_tenure = 11 + parseInt(i_current_month);
			var f_total_prev_mnths = parseFloat(i_total_project_tenure)
					- parseFloat(i_project_mnth);
			f_total_prev_mnths = parseFloat(f_total_prev_mnths) + parseFloat(1);
		} else {
			var f_total_prev_mnths = parseFloat(i_current_month)
					- parseFloat(i_project_mnth);
		}

		if (i_amt <= f_total_prev_mnths) {
			//nlapiLogExecution('audit','f_revenue_amount:- '+f_revenue_amount,'a_amount.length:- '+a_amount.length);
			f_revenue_amount_till_current_month = parseFloat(f_revenue_amount)
					+ parseFloat(f_revenue_amount_till_current_month);
		}

		f_total_revenue = parseFloat(f_revenue_amount)
				+ parseFloat(f_total_revenue);

		html += "<td class='projected-amount'>";
		html += '<b>' + s_currency_symbol_proj + ' '
				+ format2(f_revenue_amount);
		html += "</td>";
	}

	html += "</tr>";

	html += "</table>";

	html += "<table class='projection-table'>";

	html += "<tr class='header-row'>";

	html += "<td>";
	html += "";
	html += "</td>";

	html += "<td width:15px>";
	html += "Practice";
	html += "</td>";

	html += "<td width:30px>";
	html += "Sub Practice";
	html += "</td>";

	var today = new Date();
	var currentMonthName = getMonthName(nlapiDateToString(today));

	for (var i_amt = i_current_month + 1; i_amt < a_amount.length; i_amt++) {
		var f_prcnt_revenue = parseFloat(a_amount[i_amt])
				/ parseFloat(i_total_row_revenue);
		f_prcnt_revenue = f_prcnt_revenue * 100;

		var f_revenue_amount = (parseFloat(f_prcnt_revenue) * parseFloat(f_revenue_share)) / 100;
		if (!f_revenue_amount)
			f_revenue_amount = 0;

		if (i_amt < i_current_month) {
			//nlapiLogExecution('audit','i_current_month:- '+i_current_month,'a_amount.length:- '+a_amount.length);
			//	f_revenue_amount_till_current_month = parseFloat(f_revenue_amount) + parseFloat(f_revenue_amount_till_current_month);
			if (!f_revenue_amount_till_current_month)
				f_revenue_amount_till_current_month = 0;
		}

		f_total_revenue = parseFloat(f_revenue_amount)
				+ parseFloat(f_total_revenue);

		html += "<td class='projected-amount'>";
		html += '<b>' + s_currency_symbol_proj + ' '
				+ format2(f_revenue_amount);
		html += "</td>";

		//i_total_actual_revenue_recognized = parseFloat(i_total_actual_revenue_recognized) + parseFloat(f_revenue_amount);
	}

	html += "<td class='projected-amount'>";
	//html += '<b>'+s_currency_symbol_proj+' '+format2(i_total_actual_revenue_recognized);;
	html += "</td>";

	html += "</tr>";

	//if(i_total_revenue_recognized_ytd == 0)
	i_total_revenue_recognized_ytd = f_revenue_share;

	html += "<td class='monthly-amount'>";
	html += s_currency_symbol_proj + ' '
			+ format2(i_total_revenue_recognized_ytd);
	html += "</td>";

	html += "</tr>";

	html += "</table>";

	//a_recognized_revenue_total_proj_level_f = a_recognized_revenue;

	return f_total_cost_overall_prac_f;
}
function down_excel_function_f(bill_data_arr, email) {
	try {
		var strVar1 = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:ss="urn:schemas-microsoft-com:office:spreadsheet" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40">'
				+ '<head>'
				+ '<meta http-equiv=Content-Type content="text/html; charset=windows-1252"/>'
				+ '<meta name=ProgId content=Excel.Sheet/>'
				+ '<meta name=Generator content="Microsoft Excel 11"/>'
				+ '<!--[if gte mso 9]><xml>'
				+ '<x:excelworkbook>'
				+ '<x:excelworksheets>'
				+ '<x:excelworksheet=sheet1>'
				+ '<x:name>** ESTIMATE FILE**</x:name>'
				+ '<x:worksheetoptions>'
				+ '<x:selected></x:selected>'
				+ '<x:freezepanes></x:freezepanes>'
				+ '<x:frozennosplit></x:frozennosplit>'
				+ '<x:splithorizontal>** FROZEN ROWS + 1 **</x:splithorizontal>'
				+ '<x:toprowbottompane>** FROZEN ROWS + 1 **</x:toprowbottompane>'
				+ '<x:splitvertical>** FROZEN COLUMNS + 1 **</x:splitvertical>'
				+ '<x:leftcolumnrightpane>** FROZEN COLUMNS + 1**</x:leftcolumnrightpane>'
				+ '<x:activepane>0</x:activepane>'
				+ // 0
				'<x:panes>'
				+ '<x:pane>'
				+ '<x:number>3</x:number>'
				+ '</x:pane>'
				+ '<x:pane>'
				+ '<x:number>1</x:number>'
				+ '</x:pane>'
				+ '<x:pane>'
				+ '<x:number>2</x:number>'
				+ '</x:pane>'
				+ '<x:pane>'
				+ '<x:number>0</x:number>'
				+ //1
				'</x:pane>'
				+ '</x:panes>'
				+ '<x:protectcontents>False</x:protectcontents>'
				+ '<x:protectobjects>False</x:protectobjects>'
				+ '<x:protectscenarios>False</x:protectscenarios>'
				+ '</x:worksheetoptions>'
				+ '</x:excelworksheet>'
				+ '</x:excelworksheets>'
				+ '<x:protectstructure>False</x:protectstructure>'
				+ '<x:protectwindows>False</x:protectwindows>'
				+ '</x:excelworkbook>'
				+

				'</x:excelworkbook>'
				+ '</xml><![endif]-->'
				+ '<style>'
				+ 'p.MsoFooter, li.MsoFooter, div.MsoFooter'
				+ '{ margin:0in; margin-bottom:.0001pt; mso-pagination:widow-orphan; tab-stops:center 3.0in right 6.0in; font-size:12.0pt;}'
				+ '<style>'
				+

				'<!-- /* Style Definitions */'
				+

				'div.Section1'
				+ '{ page:Section1;}'
				+

				'table#hrdftrtbl'
				+ '{ margin:0in 0in 0in 900in; width:1px; height:1px; overflow:hidden; font-family:Arial;}-->'
				+

				'</style>' +

				'</head>' +

				'<body>' +

				'<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">'

		var strVar2 = '';
		/*strVar2 += "<table width=\"100%\">";
		strVar2 += '<table width="100%" border="1">';*/
		var current_date = new Date();
		//current_date = nlapiStringToDate(current_date);
		var i_year = current_date.getFullYear();
		var excel_mnths = MonthNames(i_year);
		
		//convertion to usd
		 var inr_to_usd = 0;
        var gbp_to_usd = 0;
        var eur_to_usd = 0;
        var a_conversion_rate_table = nlapiSearchRecord('customrecord_conversion_rate_sfdc',null,null,[new nlobjSearchColumn('custrecord_rate_inr_to_usd'),
                                                                      new nlobjSearchColumn('custrecord_rate_gbp_to_usd'),
                                                                      new nlobjSearchColumn('custrecord_rate_eur_to_usd'),
                                                                      new nlobjSearchColumn('internalid').setSort(true)]);
        if(a_conversion_rate_table)
              inr_to_usd = 1 / parseFloat(a_conversion_rate_table[0].getValue('custrecord_rate_inr_to_usd'));
              gbp_to_usd = a_conversion_rate_table[0].getValue('custrecord_rate_gbp_to_usd');
              eur_to_usd = a_conversion_rate_table[0].getValue('custrecord_rate_eur_to_usd');
		
		
//		
		if (bill_data_arr) {

			strVar2 += "<tr>";
			strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" style='background-color:#999999'\>Project ID</td>";
			strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" style='background-color:#999999'\>Project</td>";
			strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" style='background-color:#999999'\>Currency</td>";
			strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" style='background-color:#999999' \>Practice</td>";
			strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" style='background-color:#999999' \>Sub-Practice</td>";
			strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" style='background-color:#999999' \>Rev TEc Type</td>";
			strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" style='background-color:#999999' \>Customer</td>";
			strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" style='background-color:#999999' \>   </td>";
			
			nlapiLogExecution('audit',
					'down_excel_function_f transcation length:- ',
					bill_data_arr.length);
			for (mnth_ind = 0; mnth_ind < excel_mnths.length; mnth_ind++) {
				strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" style='background-color:#999999' \>"
						+ excel_mnths[mnth_ind] + "</td>";
			}
			strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" style='background-color:#999999' \>Total</td>";
			strVar2 += "<\/tr>";
			strVar2 += "<tr>";
			for (var bill_data_arr_len = 0; bill_data_arr_len < bill_data_arr.length; bill_data_arr_len++) {
				var bill_arr = bill_data_arr[bill_data_arr_len].excel_data;
				/*for (var i = 0; i < bill_arr.length; i++)
				{
					if(i==0)
					{
						var  month_len=bill_data_arr[bill_data_arr_len].excel_data[i].st_date;
					for(var mn=0;mn< month_len.length;mn++)
						strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" style='background-color:#999999' \>"+bill_data_arr[bill_data_arr_len].excel_data[i].st_date[mn]+"</td>";
					}
					   
				}*/
				for (var jk = 0; jk < bill_arr.length; jk++) {

					var act = bill_data_arr[bill_data_arr_len].excel_data[jk].actual_rev;
					
					if(jk == 0){
						var temp_act= act.slice(0, -1);
					}
					var currncy='USD';
					strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \>"
							+ bill_data_arr[bill_data_arr_len].excel_data[jk].proj
							+ "</td>";
					strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \>"
							+ bill_data_arr[bill_data_arr_len].excel_data[jk].project_id
							+ "</td>";
					/*strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \>"
							+ bill_data_arr[bill_data_arr_len].excel_data[jk].currency
							+ "</td>";*/
					strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \>"
						+ currncy
						+ "</td>";
					strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \>"
							+ bill_data_arr[bill_data_arr_len].excel_data[jk].project_name
							+ "</td>";
					strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \>"
							+ bill_data_arr[bill_data_arr_len].excel_data[jk].practice
							+ "</td>";
					strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \>"
						+ bill_data_arr[bill_data_arr_len].excel_data[jk].type
						+ "</td>";
					strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \>"
						+ bill_data_arr[bill_data_arr_len].excel_data[jk].customer
						+ "</td>";
					var sum =0;
					//strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \></td>";
					for (var act_len = 0; act_len < act.length; act_len++) {
						if(act_len == act.length-1){
							nlapiLogExecution('debug', '', sum);
						}
						if (act_len == 0) {
							strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \>"
									+ bill_data_arr[bill_data_arr_len].excel_data[jk].actual_rev[act_len]
									+ "</td>";
						}
						if(act_len != act.length-1 && act_len != 0){
							var i_master_currency =1; 
							if(bill_data_arr[bill_data_arr_len].excel_data[jk].currency =='USD')
		                         i_master_currency = 1;
		                   if(bill_data_arr[bill_data_arr_len].excel_data[jk].currency =='INR')
		                         i_master_currency = inr_to_usd;
		                   if(bill_data_arr[bill_data_arr_len].excel_data[jk].currency =='EUR')
		                         i_master_currency = eur_to_usd;
		                   if(bill_data_arr[bill_data_arr_len].excel_data[jk].currency =='GBP')
		                         i_master_currency = gbp_to_usd;
					        var str2 = bill_data_arr[bill_data_arr_len].excel_data[jk].actual_rev[act_len];
					        str2 =	str2.replace( /,/g, "" );
					        sum  = sum + parseInt(str2)*i_master_currency;
							
						}
						
						var mn_len = act_len - 1;
						var mn = bill_data_arr[bill_data_arr_len].excel_data[jk].st_date[mn_len];
						var st_d = bill_data_arr[bill_data_arr_len].excel_data[jk].st_date;
						var valdt = bill_data_arr[bill_data_arr_len].excel_data[jk].st_date;
						var len = bill_data_arr[bill_data_arr_len].excel_data[jk].actual_rev
						valdt = valdt.slice(0, len.length - 2);
						mn = valdt[mn_len ];
						var mon = excel_mnths.indexOf(mn);
						
						if (mon >= 0) {
							if ((mon > 0) && (act_len == 1)) {
								for (var prev_mn = 0; prev_mn < mon; prev_mn++) {
									strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \></td>";
								}
							}
							
							if (mn == excel_mnths[mon] )
								var i_master_currency =1; 
								if(bill_data_arr[bill_data_arr_len].excel_data[jk].currency =='USD')
			                         i_master_currency = 1;
			                   if(bill_data_arr[bill_data_arr_len].excel_data[jk].currency =='INR')
			                         i_master_currency = inr_to_usd;
			                   if(bill_data_arr[bill_data_arr_len].excel_data[jk].currency =='EUR')
			                         i_master_currency = eur_to_usd;
			                   if(bill_data_arr[bill_data_arr_len].excel_data[jk].currency =='GBP')
			                         i_master_currency = gbp_to_usd;
			                  
			                   var prs = parseInt(bill_data_arr[bill_data_arr_len].excel_data[jk].actual_rev[act_len]);
			                   var prs = bill_data_arr[bill_data_arr_len].excel_data[jk].actual_rev[act_len];
			                   prs =	prs.replace( /,/g, "" );
			                   var cnvtd = parseInt(prs)*i_master_currency;
						        
								strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \>"
										+ cnvtd
										+ "</td>";

						}
						if (act_len == act.length - 1) {
							var mn_len = act_len - 2;
							var mn = bill_data_arr[bill_data_arr_len].excel_data[jk].st_date[mn_len];
							var mon = excel_mnths.indexOf(mn);
							
							for (var mnth_indx = 1; mnth_indx < excel_mnths.length- mon; mnth_indx++) {
								strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \></td>";
							}
							
							/*strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \>"
									+ bill_data_arr[bill_data_arr_len].excel_data[jk].actual_rev[act_len]
									+ "</td>";*/
							strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \>"
								+ sum
								+ "</td>";
						}
						

					}
					strVar2 += "<\/tr>";

				}
				//strVar2+="<tr></tr>";
			}
		} else {

		}

		/////////////strVar2 += "<\/table>";
		//strVar1 = strVar1 + strVar2;
		fp_xml = strVar2;
	  
	} catch (err) {
		nlapiLogExecution('ERROR', 'ERROR MESSAGE:- ', err);
	}


	
	
	
}
	
	
	
	
	function generate_excel(h_tableHtml_effort_view) {
		var rev_len = h_tableHtml_effort_view.month;
		//	for(var reven=0;reven<h_tableHtml_effort_view.length;reven++)
		{
			bill_data_arr[sr_no] = {
				'project_id' : h_tableHtml_effort_view.practice,
				'project_name' : h_tableHtml_effort_view.subpractice,
				'month' : h_tableHtml_effort_view.month,
				'actual_rev' : h_tableHtml_effort_view.revenue,
				'cumm_rev' : h_tableHtml_effort_view.cummulative,
				'monthlyrevenue' : h_tableHtml_total_view.monthlyrevenue,
				'rev_rec_permnth' : h_tableHtml_total_view.rev_rec_permnth,
				'act_rev_rec_mnth' : h_tableHtml_total_view.act_rev_rec_mnth,
				'cummulative_mnth' : h_tableHtml_total_view.cummulative_mnth
			/*  'client_partnr': client_partnr,
			  'sold_margin': sold_margin,
			'expe': expe,
			'rev': rev,
			  'actual_margin': margin,
			   'account_name': s_pro_account  */
			};
			sr_no++;
			nlapiLogExecution('debug', 'Data', bill_data_arr);
		}

	}
	function generate_cost_view_prev_mnth(projectWiseRevenue_previous_effrt,
			monthBreakUp, i_year_project, i_project_mnth, mode, sub_prac,
			s_currency_symbol_proj, s_currency_symbol_usd, i_year_project_end,
			i_prject_end_mnth) {

		var today = new Date();
		var currentMonthName = getMonthName(nlapiDateToString(today));

		var a_sub_prac_already_displayed = new Array();
		var i_subpractice_id = 0;
		var a_amount = new Array();
		var a_recognized_revenue = new Array();
		var i_frst_row = 0;
		var i_diplay_frst_column = 0;
		var i_total_row_revenue = 0;
		var i_total_per_row = 0;
		var total_prcnt_aloocated = new Array();
		var b_first_line_copied = 0;
		var f_total_allocated_row = 0;

		for ( var emp_internal_id in projectWiseRevenue_previous_effrt) {
			if (projectWiseRevenue_previous_effrt[emp_internal_id].sub_practice == sub_prac) {
				f_total_allocated_row = 0;
				for ( var project_internal_id in projectWiseRevenue_previous_effrt[emp_internal_id].RevenueData) {
					var i_index_mnth = 0;
					for ( var month in projectWiseRevenue_previous_effrt[emp_internal_id].RevenueData[project_internal_id]) {
						var a_MonthNames = MonthNames(i_year_project);
						var currentMonthPos = a_MonthNames.indexOf(month);
						var s_current_month = a_MonthNames[currentMonthPos];
						var s_current_mnth_yr = s_current_month.substr(4, 8);

						var i_nxt_yr_flg = 0;
						if (currentMonthPos >= 12) {
							currentMonthPos = currentMonthPos - 12;
							i_nxt_yr_flg = 1;
						}

						var i_curr_yr_dis_flag = 0;
						if (parseInt(s_current_mnth_yr) < i_year_project_end
								&& i_prject_end_mnth < currentMonthPos) {
							i_curr_yr_dis_flag = 1;
						}

						if (((i_project_mnth <= currentMonthPos
								&& ((i_prject_end_mnth >= currentMonthPos || i_curr_yr_dis_flag == 1) && i_nxt_yr_flg == 0) && parseInt(s_current_mnth_yr) <= i_year_project) && parseInt(s_current_mnth_yr) <= i_year_project_end)
								|| (i_prject_end_mnth >= currentMonthPos
										&& parseInt(s_current_mnth_yr) <= i_year_project_end && i_nxt_yr_flg == 1)) {
							//nlapiLogExecution('audit','currentMonthPos:- ',+currentMonthPos);
							if (b_first_line_copied == 1) {
								var f_allocated_prcnt = total_prcnt_aloocated[i_index_mnth];
								f_allocated_prcnt = parseFloat(f_allocated_prcnt)
										+ parseFloat(projectWiseRevenue_previous_effrt[emp_internal_id].RevenueData[project_internal_id][month].prcnt_allocated);
								total_prcnt_aloocated[i_index_mnth] = f_allocated_prcnt;
								i_index_mnth++;
							} else {
								total_prcnt_aloocated
										.push(projectWiseRevenue_previous_effrt[emp_internal_id].RevenueData[project_internal_id][month].prcnt_allocated);
							}

							f_total_allocated_row = parseFloat(projectWiseRevenue_previous_effrt[emp_internal_id].RevenueData[project_internal_id][month].prcnt_allocated)
									+ parseFloat(f_total_allocated_row);

						}
					}
				}

				b_first_line_copied = 1;

				var s_sub_prac_name = projectWiseRevenue_previous_effrt[emp_internal_id].sub_prac_name;
				var s_practice_name = projectWiseRevenue_previous_effrt[emp_internal_id].practice_name;

				i_diplay_frst_column = 1;
			}
		}
		i_diplay_frst_column = 0;

		var f_total_row_allocation = 0;
		for (var i_index_total_allocated = 0; i_index_total_allocated < total_prcnt_aloocated.length; i_index_total_allocated++) {
			f_total_row_allocation = parseFloat(f_total_row_allocation)
					+ parseFloat(total_prcnt_aloocated[i_index_total_allocated]);
		}

		for ( var emp_internal_id in projectWiseRevenue_previous_effrt) {

			for ( var project_internal_id in projectWiseRevenue_previous_effrt[emp_internal_id].RevenueData) {

				if (projectWiseRevenue_previous_effrt[emp_internal_id].sub_practice == sub_prac) {

					var i_sub_practice_internal_id = 0;
					var i_amt = 0;
					for ( var month in projectWiseRevenue_previous_effrt[emp_internal_id].RevenueData[project_internal_id]) {
						var a_MonthNames = MonthNames(i_year_project);
						var currentMonthPos = a_MonthNames.indexOf(month);
						var s_current_month = a_MonthNames[currentMonthPos];
						var s_current_mnth_yr = s_current_month.substr(4, 8);

						var i_nxt_yr_flg = 0;
						if (currentMonthPos >= 12) {
							currentMonthPos = currentMonthPos - 12;
							i_nxt_yr_flg = 1;
						}

						var i_curr_yr_dis_flag = 0;
						if (parseInt(s_current_mnth_yr) < i_year_project_end
								&& i_prject_end_mnth < currentMonthPos) {
							i_curr_yr_dis_flag = 1;
						}

						if (((i_project_mnth <= currentMonthPos
								&& ((i_prject_end_mnth >= currentMonthPos || i_curr_yr_dis_flag == 1) && i_nxt_yr_flg == 0) && parseInt(s_current_mnth_yr) <= i_year_project) && parseInt(s_current_mnth_yr) <= i_year_project_end)
								|| (i_prject_end_mnth >= currentMonthPos
										&& parseInt(s_current_mnth_yr) <= i_year_project_end && i_nxt_yr_flg == 1)) {
							if (mode == 2) {

								var total_revenue_format = parseFloat(
										projectWiseRevenue_previous_effrt[emp_internal_id].RevenueData[project_internal_id][month].Amount)
										.toFixed(1);

								if (i_frst_row == 0) {
									//nlapiLogExecution('audit','inside push amnt');
									a_amount.push(total_revenue_format);
									a_recognized_revenue
											.push(projectWiseRevenue_previous_effrt[emp_internal_id].RevenueData[project_internal_id][month].recognized_revenue);
								} else {
									var amnt_mnth = a_amount[i_amt];
									amnt_mnth = parseFloat(amnt_mnth)
											+ parseFloat(projectWiseRevenue_previous_effrt[emp_internal_id].RevenueData[project_internal_id][month].Amount);

									a_amount[i_amt] = amnt_mnth;

									var i_existing_recognised_amount = a_recognized_revenue[i_amt];
									i_existing_recognised_amount = parseFloat(i_existing_recognised_amount)
											+ parseFloat(projectWiseRevenue_previous_effrt[emp_internal_id].RevenueData[project_internal_id][month].recognized_revenue);

									a_recognized_revenue[i_amt] = i_existing_recognised_amount;

									i_amt++;
								}
								i_total_row_revenue = parseFloat(i_total_row_revenue)
										+ parseFloat(projectWiseRevenue_previous_effrt[emp_internal_id].RevenueData[project_internal_id][month].Amount);
								i_total_per_row = parseFloat(i_total_per_row)
										+ parseFloat(projectWiseRevenue_previous_effrt[emp_internal_id].RevenueData[project_internal_id][month].Amount);
							}

						}
					}

					if (mode == 2) {
						i_total_per_row = 0;
						//i_total_row_revenue = parseFloat(i_total_row_revenue) + parseFloat(projectWiseRevenue[emp_internal_id].total_revenue_for_tenure);
					}

					i_frst_row = 1;

					i_diplay_frst_column = 1;

					var f_revenue_share = projectWiseRevenue_previous_effrt[emp_internal_id].revenue_share;
				}
			}
		}

		var s_sub_prac_name = '';
		for ( var emp_internal_id in projectWiseRevenue_previous_effrt) {
			if (projectWiseRevenue_previous_effrt[emp_internal_id].sub_practice == sub_prac) {
				s_sub_prac_name = projectWiseRevenue_previous_effrt[emp_internal_id].sub_prac_name;
				s_practice_name = projectWiseRevenue_previous_effrt[emp_internal_id].practice_name;
				//projectWiseRevenue[emp_internal_id].revenue_share_revised = i_total_row_revenue;
				//projectWiseRevenue[emp_internal_id].total_revenue_for_tenure = i_total_row_revenue;
			}
		}

		for (var i_amt = 0; i_amt < a_amount.length; i_amt++) {

		}

		// Percent row

		var f_total_prcnt = 0;
		for (var i_amt = 0; i_amt < a_amount.length; i_amt++) {
			var f_prcnt_revenue = parseFloat(a_amount[i_amt])
					/ parseFloat(i_total_row_revenue);
			f_prcnt_revenue = f_prcnt_revenue * 100;
			if (!f_prcnt_revenue)
				f_prcnt_revenue = 0;

			f_total_prcnt = parseFloat(f_prcnt_revenue)
					+ parseFloat(f_total_prcnt);

		}

		//Total reveue recognized per month row

		var d_today_date = new Date();
		var i_current_month = d_today_date.getMonth();
		var i_current_year = d_today_date.getFullYear();

		var f_total_revenue = 0;
		var f_revenue_amount_till_current_month = 0;
		for (var i_amt = 0; i_amt < a_amount.length; i_amt++) {
			var f_prcnt_revenue = parseFloat(a_amount[i_amt])
					/ parseFloat(i_total_row_revenue);
			f_prcnt_revenue = f_prcnt_revenue * 100;

			var f_revenue_amount = (parseFloat(f_prcnt_revenue) * parseFloat(f_revenue_share)) / 100;
			if (!f_revenue_amount)
				f_revenue_amount = 0;

			if (i_amt <= i_current_month) {
				//nlapiLogExecution('audit','i_current_month:- '+i_current_month,'a_amount.length:- '+a_amount.length);
				f_revenue_amount_till_current_month = parseFloat(f_revenue_amount)
						+ parseFloat(f_revenue_amount_till_current_month);
				if (!f_revenue_amount_till_current_month)
					f_revenue_amount_till_current_month = 0;
			}

			f_total_revenue = parseFloat(f_revenue_amount)
					+ parseFloat(f_total_revenue);

		}

		for ( var emp_internal_id in projectWiseRevenue_previous_effrt) {
			for ( var project_internal_id in projectWiseRevenue_previous_effrt[emp_internal_id].RevenueData) {
				var i_amount_map = 0;
				if (projectWiseRevenue_previous_effrt[emp_internal_id].sub_practice == sub_prac) {
					for ( var month in projectWiseRevenue_previous_effrt[emp_internal_id].RevenueData[project_internal_id]) {
						var a_MonthNames = MonthNames(i_year_project);
						var currentMonthPos = a_MonthNames.indexOf(month);
						var s_current_month = a_MonthNames[currentMonthPos];
						var s_current_mnth_yr = s_current_month.substr(4, 8);

						var i_nxt_yr_flg = 0;
						if (currentMonthPos >= 12) {
							currentMonthPos = currentMonthPos - 12;
							i_nxt_yr_flg = 1;
						}

						var i_curr_yr_dis_flag = 0;
						if (parseInt(s_current_mnth_yr) < i_year_project_end
								&& i_prject_end_mnth < currentMonthPos) {
							i_curr_yr_dis_flag = 1;
						}

						if (((i_project_mnth <= currentMonthPos
								&& ((i_prject_end_mnth >= currentMonthPos || i_curr_yr_dis_flag == 1) && i_nxt_yr_flg == 0) && parseInt(s_current_mnth_yr) <= i_year_project) && parseInt(s_current_mnth_yr) <= i_year_project_end)
								|| (i_prject_end_mnth >= currentMonthPos
										&& parseInt(s_current_mnth_yr) <= i_year_project_end && i_nxt_yr_flg == 1)) {
							var f_prcnt_revenue = parseFloat(a_amount[i_amount_map])
									/ parseFloat(i_total_row_revenue);
							f_prcnt_revenue = f_prcnt_revenue * 100;

							var f_revenue_amount = (parseFloat(f_prcnt_revenue) * parseFloat(f_revenue_share)) / 100;

							projectWiseRevenue_previous_effrt[emp_internal_id].RevenueData[project_internal_id][month].total_revenue = f_revenue_amount;
							projectWiseRevenue_previous_effrt[emp_internal_id].RevenueData[project_internal_id][month].prcent_complt = f_prcnt_revenue;
							i_amount_map++;
						}
					}
				}
			}
		}

		var today = new Date();
		var currentMonthName = getMonthName(nlapiDateToString(today));

		var i_total_revenue_recognized_ytd = 0;
		for (var i_revenue_index = 0; i_revenue_index < a_recognized_revenue.length; i_revenue_index++) {
			i_total_revenue_recognized_ytd = parseFloat(i_total_revenue_recognized_ytd)
					+ parseFloat(a_recognized_revenue[i_revenue_index]);

		}

		//Actual revenue to be recognized 

		if (parseInt(i_year_project) != parseInt(i_current_year)) {
			var i_total_project_tenure = 11 + parseInt(i_current_month);
			var f_total_prev_mnths = parseFloat(i_total_project_tenure)
					- parseFloat(i_project_mnth);
			i_current_month = parseFloat(f_total_prev_mnths) + parseFloat(1);
		} else {
			i_current_month = parseFloat(i_current_month)
					- parseFloat(i_project_mnth);
		}

		var f_current_month_actual_revenue = parseFloat(f_revenue_amount_till_current_month)
				- parseFloat(i_total_revenue_recognized_ytd);
		//var i_total_revenue_recognized_ytd = 0;

		//nlapiLogExecution('audit','i_current_month:- '+i_current_month);
		for (var i_revenue_index = 0; i_revenue_index <= i_current_month - 1; i_revenue_index++) {

		}

		var i_total_actual_revenue_recognized = parseFloat(i_total_revenue_recognized_ytd)
				+ parseFloat(f_current_month_actual_revenue);

		{
			var f_prcnt_revenue = parseFloat(a_amount[i_current_month])
					/ parseFloat(i_total_row_revenue);
			f_prcnt_revenue = f_prcnt_revenue * 100;

			var f_revenue_amount = (parseFloat(f_prcnt_revenue) * parseFloat(f_revenue_share)) / 100;
			if (!f_revenue_amount)
				f_revenue_amount = 0;

			return f_revenue_amount;

			if (i_amt < i_current_month) {
				//nlapiLogExecution('audit','i_current_month:- '+i_current_month,'a_amount.length:- '+a_amount.length);
				f_revenue_amount_till_current_month = parseFloat(f_revenue_amount)
						+ parseFloat(f_revenue_amount_till_current_month);
				if (!f_revenue_amount_till_current_month)
					f_revenue_amount_till_current_month = 0;
			}

			f_total_revenue = parseFloat(f_revenue_amount)
					+ parseFloat(f_total_revenue);

			i_total_actual_revenue_recognized = parseFloat(i_total_actual_revenue_recognized)
					+ parseFloat(f_revenue_amount);
		}

		//Total revenue recognized for previous months

		for (var i_exist_index = 0; i_exist_index <= a_recognized_revenue.length; i_exist_index++) {
			if (a_recognized_revenue_total_proj_level[i_exist_index]) {
				var f_existing_cost_in_arr = parseFloat(a_recognized_revenue_total_proj_level[i_exist_index]);
			} else {
				var f_existing_cost_in_arr = 0;
			}

			var f_existing_cost = a_recognized_revenue[i_exist_index];
			f_existing_cost = parseFloat(f_existing_cost)
					+ parseFloat(f_existing_cost_in_arr);

			a_recognized_revenue_total_proj_level[i_exist_index] = f_existing_cost;
		}
		//a_recognized_revenue_total_proj_level = a_recognized_revenue;

		//return html;
	}





function generate_previous_effrt_revenue(s_effrt_json,
			projectWiseRevenue_previous_effrt,
			a_revenue_recognized_for_project, i_year_project) {
		if (s_effrt_json) {
			var i_practice_previous = 0;
			var f_total_revenue_for_tenure = 0;
			//for (var i_mnth_end_plan = 0; i_mnth_end_plan < a_get_mnth_end_effrt_plan.length; i_mnth_end_plan++)
			var s_entire_json_clubed = JSON.parse(s_effrt_json);
			for (var i_mnth_end_plan = 0; i_mnth_end_plan < s_entire_json_clubed.length; i_mnth_end_plan++) {
				var a_row_json_data = JSON.parse(JSON
						.stringify(s_entire_json_clubed[i_mnth_end_plan]));
				for (var i_row_json_index = 0; i_row_json_index < a_row_json_data.length; i_row_json_index++) {
					var i_practice = a_row_json_data[i_row_json_index].prac;
					var s_practice = a_row_json_data[i_row_json_index].prac_text;
					var i_sub_practice = a_row_json_data[i_row_json_index].subprac;
					var s_sub_practice = a_row_json_data[i_row_json_index].subprac_text;
					var i_role = a_row_json_data[i_row_json_index].role;
					var s_role = a_row_json_data[i_row_json_index].role_text;
					var i_level = a_row_json_data[i_row_json_index].level;
					var s_level = a_row_json_data[i_row_json_index].level_text;
					var i_location = a_row_json_data[i_row_json_index].loc;
					var s_location = a_row_json_data[i_row_json_index].loc_text;
					var f_revenue = a_row_json_data[i_row_json_index].cost;
					if (!f_revenue)
						f_revenue = 0;

					var f_revenue_share = a_row_json_data[i_row_json_index].share;
					if (!f_revenue_share)
						f_revenue_share = 0;

					var i_no_of_resources = a_row_json_data[i_row_json_index].allo;
					var s_mnth_strt_date = '1/31/2017';
					var s_mnth_end_date = '31/31/2017';
					var s_mnth = a_row_json_data[i_row_json_index].mnth;
					var s_year = a_row_json_data[i_row_json_index].year;

					var f_revenue_recognized = find_recognized_revenue_prev(
							a_revenue_recognized_for_project, i_practice,
							i_sub_practice, i_role, i_level, s_mnth, s_year,
							a_prev_subprac_searched_once);

					if (!f_revenue_recognized)
						f_revenue_recognized = 0;

					//nlapiLogExecution('audit','f_revenue_recognized:- '+f_revenue_recognized);

					var s_month_name = s_mnth + '_' + s_year;

					var total_revenue = parseFloat(i_no_of_resources)
							* parseFloat(f_revenue);
					total_revenue = parseFloat(total_revenue).toFixed(2);

					if (i_practice_previous == 0) {
						i_practice_previous = a_row_json_data[i_row_json_index].prac_row;
					}

					if (i_practice_previous != a_row_json_data[i_row_json_index].prac_row) {
						f_total_revenue_for_tenure = 0;
						i_practice_previous = a_row_json_data[i_row_json_index].prac_row;
					}

					f_total_revenue_for_tenure = parseFloat(f_total_revenue_for_tenure)
							+ parseFloat(total_revenue);
					f_total_revenue_for_tenure = parseFloat(
							f_total_revenue_for_tenure).toFixed(2);

					var i_prac_subPrac_role_level = i_practice + '_'
							+ i_sub_practice + '_' + i_role + '_' + i_level
							+ '_' + i_location;

					if (!projectWiseRevenue_previous_effrt[i_prac_subPrac_role_level]) {
						projectWiseRevenue_previous_effrt[i_prac_subPrac_role_level] = {
							practice_name : s_practice,
							sub_prac_name : s_sub_practice,
							sub_practice : i_sub_practice,
							role_name : s_role,
							level_name : s_level,
							location_name : s_location,
							total_revenue_for_tenure : f_total_revenue_for_tenure,
							revenue_share : f_revenue_share,
							RevenueData : {}
						};

						projectWiseRevenue_previous_effrt[i_prac_subPrac_role_level].RevenueData[i_practice] = new yearData(
								i_year_project);

						projectWiseRevenue_previous_effrt[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].Amount = total_revenue;
						projectWiseRevenue_previous_effrt[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].prcnt_allocated = i_no_of_resources;
						projectWiseRevenue_previous_effrt[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].recognized_revenue = f_revenue_recognized;
						projectWiseRevenue_previous_effrt[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].mnth_strt = s_mnth_strt_date;
						projectWiseRevenue_previous_effrt[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].mnth_end = s_mnth_end_date;
					} else {
						projectWiseRevenue_previous_effrt[i_prac_subPrac_role_level].total_revenue_for_tenure = f_total_revenue_for_tenure;

						projectWiseRevenue_previous_effrt[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].Amount = total_revenue;
						projectWiseRevenue_previous_effrt[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].prcnt_allocated = i_no_of_resources;
						projectWiseRevenue_previous_effrt[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].recognized_revenue = f_revenue_recognized;
						projectWiseRevenue_previous_effrt[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].mnth_strt = s_mnth_strt_date;
						projectWiseRevenue_previous_effrt[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].mnth_end = s_mnth_end_date;
					}
				}
			}
		}

		//return projectWiseRevenue_previous_effrt;
	}

function find_recognized_revenue_prev(a_revenue_recognized_for_project,
			practice, subpractice, role, level, month, year,
			a_prev_subprac_searched_once) {
		var f_recognized_amount = 0;

		if (a_prev_subprac_searched_once.indexOf(subpractice) < 0) {
			for (var i_index_find = 0; i_index_find < a_revenue_recognized_for_project.length; i_index_find++) {
				if (practice == a_revenue_recognized_for_project[i_index_find].practice_revenue_recognized) {
					if (subpractice == a_revenue_recognized_for_project[i_index_find].subpractice_revenue_recognized) {
						//if(role == a_revenue_recognized_for_project[i_index_find].role_revenue_recognized)
						{
							//if(level == a_revenue_recognized_for_project[i_index_find].level_revenue_recognized)
							{
								if (month == a_revenue_recognized_for_project[i_index_find].month_revenue_recognized) {
									if (year == a_revenue_recognized_for_project[i_index_find].year_revenue_recognized) {
										f_recognized_amount = a_revenue_recognized_for_project[i_index_find].amount_revenue_recognized;
										//nlapiLogExecution('audit','f_recognized_amount:- '+f_recognized_amount);
										a_prev_subprac_searched_once
												.push(subpractice);
									}
								}
							}
						}
					}
				}
			}

			return f_recognized_amount;
		} else {
			return 0;
		}
	}

/*function find_recognized_revenue(a_revenue_recognized_for_project,
			practice, subpractice, role, level, month, year,
			a_subprac_searched_once, a_subprac_searched_once_month,
			a_subprac_searched_once_year) {
		var f_recognized_amount = 0;

		if (func_search_prac_processed_fr_mnth(a_subprac_searched_once,
				subpractice, month, year) < 0) {
			for (var i_index_find = 0; i_index_find < a_revenue_recognized_for_project.length; i_index_find++) {
				if (practice == a_revenue_recognized_for_project[i_index_find].practice_revenue_recognized) {
					if (subpractice == a_revenue_recognized_for_project[i_index_find].subpractice_revenue_recognized) {
						//if(role == a_revenue_recognized_for_project[i_index_find].role_revenue_recognized)
						{
							//if(level == a_revenue_recognized_for_project[i_index_find].level_revenue_recognized)
							{
								if (month == a_revenue_recognized_for_project[i_index_find].month_revenue_recognized) {
									if (parseInt(year) == parseInt(a_revenue_recognized_for_project[i_index_find].year_revenue_recognized)) {
										f_recognized_amount = a_revenue_recognized_for_project[i_index_find].amount_revenue_recognized;
										//nlapiLogExecution('audit','f_recognized_amount inside look for prev mnth amnt once pr sub prac:- ',f_recognized_amount);
										a_subprac_searched_once.push({
											'subpractice' : subpractice,
											'month' : month,
											'year' : year
										});
										//a_subprac_searched_once_month.push(month);
										//a_subprac_searched_once_year.push(year);

										if (subpractice == 325) {
											nlapiLogExecution('audit',
													'month:- ' + month,
													'year:- ' + year);
											nlapiLogExecution(
													'audit',
													'f_recognized_amount:- '
															+ f_recognized_amount);
										}
									}
								}
							}
						}
					}
				}
			}

			return f_recognized_amount;
		} else {
			return 0;
		}
	}*/
	/*function func_search_prac_processed_fr_mnth(a_subprac_searched_once,
			subpractice, month, year) {
		var i_return_var = -1;
		for (var i_loop = 0; i_loop < a_subprac_searched_once.length; i_loop++) {
			if (a_subprac_searched_once[i_loop].subpractice == subpractice) {
				if (a_subprac_searched_once[i_loop].month == month) {
					if (a_subprac_searched_once[i_loop].year == year) {
						i_return_var = i_loop;
						break;
					}
				}
			}
		}

		return i_return_var;
	}*/

	function _logValidation(value) {
		if (value != null && value.toString() != null && value != ''
				&& value != undefined && value.toString() != undefined
				&& value != 'undefined' && value.toString() != 'undefined'
				&& value.toString() != 'NaN' && value != NaN) {
			return true;
		} else {
			return false;
		}
	}

	function getMonthCompleteName(month) {
		var s_mont_complt_name = '';
		if (month == 'Jan')
			s_mont_complt_name = 'January';
		if (month == 'Feb')
			s_mont_complt_name = 'February';
		if (month == 'Mar')
			s_mont_complt_name = 'March';
		if (month == 'Apr')
			s_mont_complt_name = 'April';
		if (month == 'May')
			s_mont_complt_name = 'May';
		if (month == 'Jun')
			s_mont_complt_name = 'June';
		if (month == 'Jul')
			s_mont_complt_name = 'July';
		if (month == 'Aug')
			s_mont_complt_name = 'August';
		if (month == 'Sep')
			s_mont_complt_name = 'September';
		if (month == 'Oct')
			s_mont_complt_name = 'October';
		if (month == 'Nov')
			s_mont_complt_name = 'November';
		if (month == 'Dec')
			s_mont_complt_name = 'December';

		return s_mont_complt_name;
	}
	function dynamicSort(property) {
	    var sortOrder = 1;
	    if(property[0] === "-") {
	        sortOrder = -1;
	        property = property.substr(1);
	    }
	    return function (a,b) {
	        var result = (a[property] < b[property]) ? -1 : (a[property] > b[property]) ? 1 : 0;
	        return result * sortOrder;
	    }
	}

