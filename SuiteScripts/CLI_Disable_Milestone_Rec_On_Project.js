// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
/*
   	Script Name:CLI_Milestone_Validation
	Author:Swati Kurariya
	Company:Brillio
	Date:20-08-2014
    Description:Validate milestone rec.


	Script Modification Log:

	-- Date --			-- Modified By --				--Requested By--				-- Description --




	Below is a summary of the process controls enforced by this script file.  The control logic is described
	more fully, below, in the appropriate function headers and code blocks.

     PAGE INIT
		- pageInit(type)


     SAVE RECORD
		- saveRecord()


     VALIDATE FIELD
		- validateField(type, name, linenum)


     FIELD CHANGED
		- fieldChanged(type, name, linenum)


     POST SOURCING
		- postSourcing(type, name)


	LINE INIT
		- lineInit(type)


     VALIDATE LINE
		- validateLine()


     RECALC
		- reCalc()


     SUB-FUNCTIONS
		- The following sub-functions are called by the above core functions in order to maintain code
            modularization:





*/
}
// END SCRIPT DESCRIPTION BLOCK  ====================================



// BEGIN SCRIPT UPDATION BLOCK  ====================================
/*


*/
// END SCRIPT UPDATION BLOCK  ====================================




// BEGIN GLOBAL VARIABLE BLOCK  =====================================
{

	//  Initialize any Global Variables, in particular, debugging variables...


}
// END GLOBAL VARIABLE BLOCK  =======================================





// BEGIN PAGE INIT ==================================================

function pageInit(type)
{
	
	//alert('type'+type);
	if(type == 'edit' || type == 'view')
	{
		var billing_type=nlapiGetFieldValue('jobbillingtype');
		//alert('billing_type'+billing_type);
		if(billing_type == 'CB' || billing_type == 'FBI' || billing_type == 'FBM')
		{
			document.getElementById('recmachcustrecordfpr_projparent_main_form').style.display='block';
			document.getElementById('recmachcustrecord_fpr_parent_main_form').style.display='block';
			document.getElementById('recmachcustrecord_parent_main_form').style.display='block';
		}
	else
		{
			document.getElementById('recmachcustrecordfpr_projparent_main_form').style.display='none';
			document.getElementById('recmachcustrecord_fpr_parent_main_form').style.display='none';
			document.getElementById('recmachcustrecord_parent_main_form').style.display='none';
			alert('Milestone Entry Allowed Only Charge-Based,Fixed Bid Milestone,Fixed Bid Interval Billing Type');
			
		
		}
	}
    /*  On page init:

	- PURPOSE

		FIELDS USED:
recmachcustrecord_fpr_parent_form
		--Field Name--				--ID--			--Line Item Name--

    */

    //  LOCAL VARIABLES


    //  PAGE INIT CODE BODY


}

// END PAGE INIT ====================================================





// BEGIN SAVE RECORD ================================================

function saveRecord()
{
    /*  On save record:

	- PURPOSE



	  FIELDS USED:

		--Field Name--			--ID--		--Line Item Name--


    */


    //  LOCAL VARIABLES



    //  SAVE RECORD CODE BODY


	return true;

}

// END SAVE RECORD ==================================================





// BEGIN VALIDATE FIELD =============================================

function validateField(type, name, linenum)
{

	/*  On validate field:

          - EXPLAIN THE PURPOSE OF THIS FUNCTION


          FIELDS USED:

          --Field Name--				--ID--


    */


	//  LOCAL VARIABLES



	//  VALIDATE FIELD CODE BODY


	return true;

}

// END VALIDATE FIELD ===============================================





// BEGIN FIELD CHANGED ==============================================

function fieldChanged(type, name, linenum)
{
	//alert('type'+type);
	var id=nlapiGetRecordId();
	//alert('id'+id);
	//===============================================================================================
	if(id == '' || id == null || id == 'undefined' || id == 'null')
	{//1 if start
	}//1 if close
	else
	{//else start
		if(name == 'jobbillingtype')
		{
			var billing_type=nlapiGetFieldValue('jobbillingtype');
			//alert('billing_type'+billing_type);
			if(billing_type == 'CB' || billing_type == 'FBI' || billing_type == 'FBM')
			{
				document.getElementById('recmachcustrecordfpr_projparent_main_form').style.display='block';
				document.getElementById('recmachcustrecord_fpr_parent_main_form').style.display='block';
				document.getElementById('recmachcustrecord_parent_main_form').style.display='block';
			}
		else
			{
				document.getElementById('recmachcustrecordfpr_projparent_main_form').style.display='none';
				document.getElementById('recmachcustrecord_fpr_parent_main_form').style.display='none';
				document.getElementById('recmachcustrecord_parent_main_form').style.display='none';
				alert('Milestone Entry Allowed Only Charge-Based,Fixed Bid Milestone,Fixed Bid Interval Billing Type');
				
			
			}
			
		}
	}//else close
    /*  On field changed:

          - PURPOSE


          FIELDS USED:

			--Field Name--				--ID--


    */


    //  LOCAL VARIABLES


    //  FIELD CHANGED CODE BODY

	//alert('Field Name='+name+'\nLine Item Type='+type+'\nField Value='+nlapiGetFieldValue(name)+'\nLine Item Value='+nlapiGetLineItemValue(type, name, 1));

}

// END FIELD CHANGED ================================================





// BEGIN POST SOURCING ==============================================

function postSourcing(type, name)
{

    /*  On post sourcing:

	- PURPOSE

          FIELDS USED:

		--Field Name--			--ID--		--Line Item Name--


    */

    //  LOCAL VARIABLES


    //  POST SOURCING CODE BODY



}

// END POST SOURCING ================================================



// BEGIN LINE INIT ==============================================

function lineInit(type)
{

    /*  On Line Init:

	- PURPOSE

          FIELDS USED:

		--Field Name--			--ID--		--Line Item Name--


    */

    //  LOCAL VARIABLES


    //  LINE INIT CODE BODY



}

// END LINE INIT ================================================


// BEGIN VALIDATE LINE ==============================================

function validateLine_Milestone(type)
{//validate fun start

	//alert('type'+type);
	/*  On validate line:

          - EXPLAIN THE PURPOSE OF THIS FUNCTION
check revenue record tab ,effort expended and effort remaining field is not more than 100 or less than 100.

          FIELDS USED:

          --Field Name--				--ID--


    */


	return true;
}//validate fun close

// END VALIDATE LINE ================================================





// BEGIN RECALC =====================================================

function recalc(type)
{

	/*  On recalc:

          - EXPLAIN THE PURPOSE OF THIS FUNCTION


          FIELDS USED:

          --Field Name--				--ID--


    */


	//  LOCAL VARIABLES


	//  RECALC CODE BODY


}

// END RECALC =======================================================





// BEGIN FUNCTION ===================================================




// END FUNCTION =====================================================
