/**
 * @NApiVersion 2.x
 * @NScriptType Restlet
 * @NModuleScope SameAccount
 */
/*
 *   Name of Developer : Shravan kulkarni
 *   Date : 29-10-2021
 *   Use : Script is used to fetch the Employee Data as record master
 */
define(['N/record', 'N/search','N/runtime'],
function(obj_Record, obj_Search,obj_Runtime)
{
    function Send_Emp_data(obj_Request) 
    {
    	
    	try
    	{
    		log.debug('requestBody ==',JSON.stringify(obj_Request));
    		var obj_Usage_Context = obj_Runtime.getCurrentScript();
    		var s_Request_Type = obj_Request.RequestType;
    		log.debug('s_Request_Type ==',s_Request_Type);
    		var s_Request_Data = obj_Request.RequestData;
    		log.debug('s_Request_Data ==',s_Request_Data);
    		var obj_Response_Return = {};
    		if(s_Request_Data == 'EMPLOYEE')
			{
    			obj_Response_Return = get_Employee_Data(obj_Search);
				
			} //// if(s_Request_Data == 'Employee')
    		
    		
    	} /// End of try
    	catch(s_Exception)
    	{
    		log.debug('s_Exception==',s_Exception);
    	} //// 
    	  log.debug('obj_Response_Return==', JSON.stringify(obj_Response_Return));
    		log.debug('Final Remaining Usage  ==',obj_Usage_Context.getRemainingUsage());
    	return obj_Response_Return;
    } ///// Function Send_Emp_data
  
	
    return {
        post: Send_Emp_data
    };
    
}); //// End of Main Function
/****************** Supporting Functions ***********************************/
function get_Employee_Data(obj_Search)
{
	try
	{
		var obj_Emp_JSon_Response = {};
		var obj_Employee_Search = obj_Search.create({
			type: "employee",
			filters: [
				["custentity_implementationteam", "is", "F"]
			],
			columns: [
			     obj_Search.createColumn({name: "internalid", label: "Internal ID"}),
				obj_Search.createColumn({name: "entityid",sort: obj_Search.Sort.ASC,label: "Name"}),
				obj_Search.createColumn({name: "custentity_fusion_empid",label: "Employee ID"}),
				obj_Search.createColumn({name: "firstname",label: "Employee Name"}),
				obj_Search.createColumn({name: "middlename",label: "Middle Name"}),
				obj_Search.createColumn({name: "lastname",label: "Last Name"}),
				obj_Search.createColumn({name: "custentity_employee_inactive",label: "Employee Inactive"}),
				obj_Search.createColumn({name: "internalid",join: "department",label: "Employee Practice"}),
				obj_Search.createColumn({name: "custrecord_parent_practice",join: "department",label: "Employee Parent practice"}),
				obj_Search.createColumn({name: "employeetype",label: "Salaried/Hourly"}),
				obj_Search.createColumn({name: "internalid",join: "CUSTENTITY_REPORTINGMANAGER",label: "Reporting Manager"}),
				obj_Search.createColumn({name: "timeapprover",label: "Time Approver"}),
				obj_Search.createColumn({name: "email",label: "Email"}),
				obj_Search.createColumn({name: "altemail",label: "Alt. Email"}),
				obj_Search.createColumn({name: "internalid",join: "subsidiary",label: "Subsidiary"}),
				obj_Search.createColumn({name: "custentity_persontype",label: "Person Type"}),
				obj_Search.createColumn({name: "custentity_employeetype",label: "Employee Type"}),
				obj_Search.createColumn({name: "employeestatus",label: "Employee Status"}),
				obj_Search.createColumn({name: "title",label: "Job Title"}),
				obj_Search.createColumn({name: "custentity_actual_hire_date",label: "Actual Hire Date"}),
				obj_Search.createColumn({name: "custentity_emp_function",label: "Function"}),
				obj_Search.createColumn({name: "internalid",join: "location",label: "Employee Location"}),
				obj_Search.createColumn({name: "custentity_lwd",label: "Last Working Date"}),
				obj_Search.createColumn({name: "custentity_emp_category",label: "Employment Category"})
			]
		});
		var i_Search_Count_Employee = obj_Employee_Search.runPaged().count;
		log.debug("i_Search_Count_Employee==", i_Search_Count_Employee);
		if(i_Search_Count_Employee >= 1000)
		{
			/****** Below code is taken from SFDC script which has access to more than 1000 resut set ***/
			var results = [];
			var searchid = 0;
			do {
			var resultslice = obj_Employee_Search.run().getRange({
				start: searchid,
				end: searchid + 1000
			});
			for (var rs in resultslice) 
			{
				results.push(resultslice[rs]);
				searchid++;
			}
		} while (resultslice.length >= 1000);
		/****** Above code is taken from SFDC script which has access to more than 1000 result set ***/
		} ///// if(i_Initial_Search_Count > 1000)
	else
		{
			var obj_Results = obj_Employee_Search.run();
			 var results =  obj_Results.getRange({
		        start: 0,
		        end: parseInt(i_Initial_Search_Count)
		    });
			 
		} //// else of ///// if(i_Initial_Search_Count > 1000)
		var i_Loop_Total_Count = results.length;
		log.debug('i_Loop_Total_Count==',i_Loop_Total_Count);	
		var arr_Emp_json= [];
		var obj_json_Container = {}
		for(var i_Index_loop = 0 ;i_Index_loop < i_Loop_Total_Count ;i_Index_loop++)
			{
			obj_json_Container = {
				'Internal_ID' : results[i_Index_loop].getValue({name: "internalid", label: "Internal ID"}),
				'Name': results[i_Index_loop].getValue({name: "entityid",sort: obj_Search.Sort.ASC,label: "Name"}),
				'Employee_ID' : results[i_Index_loop].getValue({name: "custentity_fusion_empid",label: "Employee ID"}),
				'Employee_Name' : results[i_Index_loop].getValue({name: "firstname",label: "Employee Name"}),
				'Middle_Name': results[i_Index_loop].getValue({name: "middlename",label: "Middle Name"}),
				'Last_Name':results[i_Index_loop].getValue({name: "lastname",label: "Last Name"}),
				'Employee_Inactive':results[i_Index_loop].getValue({name: "custentity_employee_inactive",label: "Employee Inactive"}),
				'Employee_Practice':results[i_Index_loop].getValue({name: "internalid",join: "department",label: "Employee Practice"}),
				'Employee_Parent_practice':results[i_Index_loop].getValue({name: "custrecord_parent_practice",join: "department",label: "Employee Parent practice"}),
				'Salaried_Hourly':results[i_Index_loop].getValue({name: "employeetype",label: "Salaried/Hourly"}),
				'Reporting_Manager':results[i_Index_loop].getValue({name: "internalid",join: "CUSTENTITY_REPORTINGMANAGER",label: "Reporting Manager"}),
				'Time_Approver':results[i_Index_loop].getValue({name: "timeapprover",label: "Time Approver"}),
				'Email':results[i_Index_loop].getValue({name: "email",label: "Email"}),
				'Alt_Email':results[i_Index_loop].getValue({name: "altemail",label: "Alt. Email"}),
				'Subsidiary':results[i_Index_loop].getValue({name: "internalid",join: "subsidiary",label: "Subsidiary"}),
				'Person_Type':results[i_Index_loop].getValue({name: "custentity_persontype",label: "Person Type"}),
				'Employee_Type':results[i_Index_loop].getValue({name: "custentity_employeetype",label: "Employee Type"}),
				'Employee_Status':results[i_Index_loop].getValue({name: "employeestatus",label: "Employee Status"}),
				'Job_Title':results[i_Index_loop].getValue({name: "title",label: "Job Title"}),
				'Actual_Hire_Date':results[i_Index_loop].getValue({name: "custentity_actual_hire_date",label: "Actual Hire Date"}),
				'Function':results[i_Index_loop].getValue({name: "custentity_emp_function",label: "Function"}),
				'Employee_Location':results[i_Index_loop].getValue({name: "internalid",join: "location",label: "Employee Location"}),
				'Last_Working_Date':results[i_Index_loop].getValue({name: "custentity_lwd",label: "Last Working Date"}),
				'Employment_Category':results[i_Index_loop].getValue({name: "custentity_emp_category",label: "Employment Category"})			
				};
			arr_Emp_json.push(obj_json_Container);	
			} //////for(var i_Index_loop = 0 ;i_Index_loop < i_Loop_Total_Count ;i_Index_loop++)
return arr_Emp_json;
	} //// End of try
	catch(s_Exception)
	{
		log.debug('get_Employee_Data s_Exception== ',s_Exception);
	} //// End of catch 
} ///// End of function get_Employee_Data()