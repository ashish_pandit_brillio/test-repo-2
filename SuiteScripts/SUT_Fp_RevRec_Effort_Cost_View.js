/**
 * @author Jayesh
 */

function suiteletFunction_Gnerate_View(request,response)
{
	try
	{
		var o_context = nlapiGetContext();
		var i_user_logegdIn_id = o_context.getUser();
		
		var i_projectId = request.getParameter('proj_id');
		if(i_projectId){
		var i_project_executing_practice_ = nlapiLookupField('job',i_projectId,'custentity_practice');
		var i_executing_practice_head_ = nlapiLookupField('department',i_project_executing_practice_,'custrecord_practicehead');
		nlapiLogExecution('DEBUG','nlapiGetRole',nlapiGetRole());
		//Giving permission to FP&A team to approve budget
		if(nlapiGetRole() == 1064 || i_user_logegdIn_id==62082 ||i_user_logegdIn_id== 5748 || i_user_logegdIn_id==278398 ||i_user_logegdIn_id==283840){ //Manager FPA role
		i_user_logegdIn_id = i_executing_practice_head_;
		}
          
		}
		var s_request_mode = request.getParameter('mode');
		
		var i_revenue_share_id = request.getParameter('existing_rcrd_id');
		if(i_revenue_share_id)
			i_revenue_share_id = i_revenue_share_id.trim();
			
		var i_revenue_share_status = request.getParameter('revenue_rcrd_status');
		if(i_revenue_share_status)
			i_revenue_share_status = i_revenue_share_status.trim();
			
		var b_revenue_rcrd_updated = request.getParameter('revenue_rcrd_updated');
			
		if(request.getMethod() == 'POST')
		{
         
			var i_projectId = request.getParameter('proj_selected');
				
			var s_request_type = request.getParameter('share_type');
				
			if(s_request_type == 'View' || s_request_type == 'Submit')
			{
				var a_proj_participating_practice = new Array();
				var a_revenue_cap_filter = [['custrecord_revenue_share_project', 'anyof', parseInt(i_projectId)]];
				
				var a_column = new Array();
				a_column[0] = new nlobjSearchColumn('created').setSort(true);
				a_column[1] = new nlobjSearchColumn('custrecord_auto_number_counter');
				a_column[2] = new nlobjSearchColumn('custrecord_revenue_share_approval_status');
				
				var a_get_logged_in_user_exsiting_revenue_cap = nlapiSearchRecord('customrecord_revenue_share', null, a_revenue_cap_filter, a_column);
				if (a_get_logged_in_user_exsiting_revenue_cap)
				{
					var i_revenue_share_id = a_get_logged_in_user_exsiting_revenue_cap[0].getId();
					var i_revenue_share_status = a_get_logged_in_user_exsiting_revenue_cap[0].getValue('custrecord_revenue_share_approval_status');
				}
				
				var a_url_parameters = new Array();
				a_url_parameters['proj_id'] = i_projectId;
				a_url_parameters['proj_id_'] = i_projectId;
				if(s_request_type == 'View')
				{
					a_url_parameters['mode'] = 'Submit';
					nlapiSubmitField('customrecord_revenue_share',i_revenue_share_id,'custrecord_revenue_share_approval_status',1);
				}
					
				if(s_request_type == 'Submit')
				{
					a_url_parameters['mode'] = 'PendingApproval';
					nlapiSubmitField('customrecord_revenue_share',i_revenue_share_id,'custrecord_revenue_share_approval_status',2);
				}
					
				a_url_parameters['existing_rcrd_id'] = i_revenue_share_id;
				
				nlapiSetRedirectURL('SUITELET', '1144', 'customdeploy_sut_fp_revrec_effrtcst_view', null, a_url_parameters);
				//nlapiSetRedirectURL('SUITELET', '1044', 'customdeploy_sut_fp_rev_rec_proj_details', null, a_url_parameters);
			}
		}
		
		if(!i_revenue_share_id)
			return;
			
			
		if (i_projectId) // check if project id is present
		{
			i_projectId = i_projectId.trim();
			
			var o_project_object = nlapiLoadRecord('job', i_projectId); // load project record object
			// get necessary information about project from project record
			var s_project_region = o_project_object.getFieldValue('custentity_region');
			var i_customer_name = o_project_object.getFieldValue('parent');
			var s_project_name = o_project_object.getFieldValue('companyname');
			var d_proj_start_date = o_project_object.getFieldValue('startdate');
			var d_proj_strt_date_old_proj = o_project_object.getFieldValue('custentity_rev_rec_new_strt_date');
			var d_proj_end_date = o_project_object.getFieldValue('enddate');
			var i_proj_executing_practice = o_project_object.getFieldValue('custentity_practice');
			var i_proj_manager = o_project_object.getFieldValue('custentity_projectmanager');
			var i_proj_manager_practice = nlapiLookupField('employee', i_proj_manager, 'department');
			var i_project_sow_value = o_project_object.getFieldValue('custentity_projectvalue');
			var i_proj_revenue_rec_type = o_project_object.getFieldValue('custentity_fp_rev_rec_type');
			var s_proj_currency = o_project_object.getFieldText('custentity_project_currency');
			var array_filters=[];
            array_filters.push(["custrecord_revenue_sourcetype", "anyof", "4"]);
            array_filters.push("AND"),
            array_filters.push(["custrecord_plcurrency", "anyof",o_project_object.getFieldValue('custentity_project_currency')]);
            var s_currency_symbol_proj=Search_revenue_Location_subsdidary(array_filters);
            s_currency_symbol_proj=s_currency_symbol_proj["CurrencySymbol"][s_proj_currency]?s_currency_symbol_proj["CurrencySymbol"][s_proj_currency]:'$';
        }
		//d_proj_end_date = '3/31/2018';
		var a_effort_plan_filter = [['custrecord_effort_plan_revenue_cap', 'anyof', parseInt(i_revenue_share_id)]];
		
		var a_columns_effort_plan = new Array();
		a_columns_effort_plan[0] = new nlobjSearchColumn('custrecord_effort_plan_practice');
		a_columns_effort_plan[1] = new nlobjSearchColumn('custrecord_effort_plan_sub_practice').setSort(false);
		a_columns_effort_plan[2] = new nlobjSearchColumn('custrecord_effort_plan_role');
		a_columns_effort_plan[3] = new nlobjSearchColumn('custrecord_effort_plan_level');
		a_columns_effort_plan[4] = new nlobjSearchColumn('custrecord_efort_plan_no_of_resources');
		a_columns_effort_plan[5] = new nlobjSearchColumn('custrecord_effort_plan_allo_strt_date');
		a_columns_effort_plan[6] = new nlobjSearchColumn('custrecord_effort_plan_allo_end_date');
		a_columns_effort_plan[7] = new nlobjSearchColumn('custrecord_effort_plan_percent_allocated');
		a_columns_effort_plan[8] = new nlobjSearchColumn('custrecord_cost_for_resource');
		a_columns_effort_plan[9] = new nlobjSearchColumn('custrecord_proj_strt_date_revenue_cap','custrecord_effort_plan_revenue_cap');
		a_columns_effort_plan[10] = new nlobjSearchColumn('custrecord_proj_end_date_revenue_cap','custrecord_effort_plan_revenue_cap');
		a_columns_effort_plan[11] = new nlobjSearchColumn('custrecord_effort_plan_location');
		a_columns_effort_plan[12] = new nlobjSearchColumn('custrecord_revenue_share_effort_plan');
		
		var a_get_proj_effort_plan = nlapiSearchRecord('customrecord_fp_rev_rec_effort_plan', null, a_effort_plan_filter, a_columns_effort_plan);
		if (a_get_proj_effort_plan)
		{
			
			var a_effort_plan_duplicate_filter = [['custrecord_effort_plan_revenue_cap', 'anyof', parseInt(i_revenue_share_id)]];
		
			var a_columns_effort_plan_duplicate = new Array();
			a_columns_effort_plan_duplicate[0] = new nlobjSearchColumn('custrecord_effort_plan_sub_practice',null,'group').setSort(false);
			a_columns_effort_plan_duplicate[1] = new nlobjSearchColumn('custrecord_revenue_share_effort_plan',null,'group');
			a_columns_effort_plan_duplicate[2] = new nlobjSearchColumn('internalid',null,'count');
			
			var a_srch_duplicate_sub_practice = nlapiSearchRecord('customrecord_fp_rev_rec_effort_plan', null, a_effort_plan_duplicate_filter, a_columns_effort_plan_duplicate);
			if(a_srch_duplicate_sub_practice)
			{
				var a_duplicate_sub_prac_count = new Array();
				var sr_no = 0;
				for(var i_dupli_index = 0; i_dupli_index < a_srch_duplicate_sub_practice.length; i_dupli_index++)
				{
					a_duplicate_sub_prac_count[sr_no] = {
													'sub_prac': a_srch_duplicate_sub_practice[i_dupli_index].getValue('custrecord_effort_plan_sub_practice',null,'group'),
													'sub_prac_name': a_srch_duplicate_sub_practice[i_dupli_index].getText('custrecord_effort_plan_sub_practice',null,'group'),
													'revenue_share': a_srch_duplicate_sub_practice[i_dupli_index].getValue('custrecord_revenue_share_effort_plan',null,'group'),
													'no_count': a_srch_duplicate_sub_practice[i_dupli_index].getValue('internalid',null,'count')
													};
					sr_no++;
				}
			}
			
			var previous_year = '';
			var projectWiseRevenue = {};
			var projectWiseEffortPlan = {};
			
			if(d_proj_strt_date_old_proj)
			{
				d_proj_start_date = d_proj_strt_date_old_proj;
			}
			
			var pro_strtDate = d_proj_start_date; //a_get_proj_effort_plan[0].getValue('custrecord_proj_strt_date_revenue_cap','custrecord_effort_plan_revenue_cap')
			pro_strtDate = nlapiStringToDate(pro_strtDate);
			var i_year_project = pro_strtDate.getFullYear();
			
			var pro_endDate = a_get_proj_effort_plan[0].getValue('custrecord_proj_end_date_revenue_cap','custrecord_effort_plan_revenue_cap');
			pro_endDate = nlapiStringToDate(pro_endDate);
			var i_year_project_end = pro_endDate.getFullYear();
			
			var i_project_mnth = pro_strtDate.getMonth();
			var i_prject_end_mnth = pro_endDate.getMonth();
			//nlapiLogExecution('audit','i_prject_end_mnth:::: '+i_prject_end_mnth,pro_endDate);
			pro_strtDate = nlapiDateToString(pro_strtDate, 'date');
			
			var pro_endDate = a_get_proj_effort_plan[0].getValue('custrecord_proj_end_date_revenue_cap','custrecord_effort_plan_revenue_cap');
			pro_endDate = nlapiStringToDate(pro_endDate);
			pro_endDate = nlapiDateToString(pro_endDate, 'date');
			
			var monthBreakUp = getMonthsBreakup(
			        nlapiStringToDate(pro_strtDate),
			        nlapiStringToDate(pro_endDate));
			//nlapiLogExecution('audit','month brckup len:- '+monthBreakUp.length);
			var i_prev_subprac = '';
			var i_prev_role = '';
			var i_prev_level = '';
				
			for(var i_effort_index = 0; i_effort_index < a_get_proj_effort_plan.length; i_effort_index++)
			{
				var i_practice = a_get_proj_effort_plan[i_effort_index].getValue('custrecord_effort_plan_practice');
				var s_practice = a_get_proj_effort_plan[i_effort_index].getText('custrecord_effort_plan_practice');
				var i_sub_practice = a_get_proj_effort_plan[i_effort_index].getValue('custrecord_effort_plan_sub_practice');
				var s_sub_practice = a_get_proj_effort_plan[i_effort_index].getText('custrecord_effort_plan_sub_practice');
				var i_role = a_get_proj_effort_plan[i_effort_index].getValue('custrecord_effort_plan_role');
				var s_role = a_get_proj_effort_plan[i_effort_index].getText('custrecord_effort_plan_role');
				var i_level = a_get_proj_effort_plan[i_effort_index].getValue('custrecord_effort_plan_level');
				var s_level = a_get_proj_effort_plan[i_effort_index].getText('custrecord_effort_plan_level');
				var i_location = a_get_proj_effort_plan[i_effort_index].getValue('custrecord_effort_plan_location');
				var s_location = a_get_proj_effort_plan[i_effort_index].getText('custrecord_effort_plan_location');
				var f_revenue = a_get_proj_effort_plan[i_effort_index].getValue('custrecord_cost_for_resource');
				var i_total_allocation = a_get_proj_effort_plan[i_effort_index].getValue('custrecord_effort_plan_percent_allocated');
				var s_allocation_strt_date = a_get_proj_effort_plan[i_effort_index].getValue('custrecord_effort_plan_allo_strt_date');
				var s_allocation_end_date = a_get_proj_effort_plan[i_effort_index].getValue('custrecord_effort_plan_allo_end_date');
				var f_revenue_share = a_get_proj_effort_plan[i_effort_index].getValue('custrecord_revenue_share_effort_plan');
				var i_no_of_resources = a_get_proj_effort_plan[i_effort_index].getValue('custrecord_efort_plan_no_of_resources');
				
				if(i_location == 2)
				{
					s_location = 'UK/Europe/Nordics';
				}
				
				if(i_prev_subprac == '')
				{
					var f_total_revenue_for_tenure = 0;
					
					i_prev_subprac = i_sub_practice;
					i_prev_role = i_role;
					i_prev_level = i_level;
				}
				
				if(i_prev_subprac != i_sub_practice)
				{
					var f_total_revenue_for_tenure = 0;
					
					i_prev_subprac = i_sub_practice;
					i_prev_role = i_role;
					i_prev_level = i_level;
				}
				else
				{
					if(i_prev_role != i_role)
					{
						var f_total_revenue_for_tenure = 0;
						
						i_prev_subprac = i_sub_practice;
						i_prev_role = i_role;
						i_prev_level = i_level;
					}
					else if(i_prev_level != i_level)
					{
						var f_total_revenue_for_tenure = 0;
						
						i_prev_subprac = i_sub_practice;
						i_prev_role = i_role;
						i_prev_level = i_level;
					}
				}
				
				
				for (var j = 0; j < monthBreakUp.length; j++)
				{
					var months = monthBreakUp[j];
					
					var s_month_name = getMonthName(months.Start); // get month name
					
					var month_strt = nlapiStringToDate(months.Start);
					var month_end = nlapiStringToDate(months.End);
					var i_month = month_strt.getMonth();
					var i_year = month_strt.getFullYear();
					s_month_name = s_month_name+'_'+i_year;
					
					var i_total_days_in_mnth = new Date(i_year, i_month+1, 0).getDate();
					
					if(!f_revenue_share)
						f_revenue_share = 0;
						
					if(!f_revenue)
						f_revenue = 0;
						
					i_total_allocation = i_total_allocation.toString();
					i_total_allocation = i_total_allocation.split('%');
					var i_total_allo_resource_no = parseFloat(i_total_allocation[0]) * parseFloat(i_no_of_resources);
					
					var final_prcnt_allocation = parseFloat(i_total_allo_resource_no) / parseFloat(100);
					final_prcnt_allocation = parseFloat(final_prcnt_allocation);
					
					var d_allocation_strt_date = nlapiStringToDate(s_allocation_strt_date);
					var d_allocation_end_date = nlapiStringToDate(s_allocation_end_date);
					
					//nlapiLogExecution('audit','month_strt:- '+month_strt,month_end);
					
					var i_days_diff = 0;
					if(month_strt >= d_allocation_strt_date || month_end >= d_allocation_strt_date)
					{
						i_days_diff = 0;
						if(d_allocation_strt_date <= month_strt && d_allocation_end_date >= month_strt)
						{
							d_allocation_strt_date = month_strt;
						}
						else if(d_allocation_end_date < month_strt)
							d_allocation_strt_date = '';
						
						if(d_allocation_end_date >= month_end && d_allocation_end_date >= month_strt)
						{
							d_allocation_end_date = month_end;
						}
						else if(d_allocation_end_date < month_strt)
						{
							d_allocation_end_date = '';
						}
						
						
						if(d_allocation_strt_date != '')
						{
							if(d_allocation_end_date != '')
							{
								//nlapiLogExecution('audit','allo strt date inside:- '+d_allocation_strt_date,d_allocation_end_date);
								i_days_diff = getDatediffIndays(d_allocation_strt_date,d_allocation_end_date);
							}
						}
					}
					
					//nlapiLogExecution('audit','allo strt date:- '+d_allocation_strt_date,d_allocation_end_date);
					//nlapiLogExecution('audit','days diff:- '+i_days_diff,i_total_days_in_mnth);
					
					var f_prcnt_mnth_allo = parseFloat(i_days_diff)/parseFloat(i_total_days_in_mnth);
					f_prcnt_mnth_allo = parseFloat(f_prcnt_mnth_allo) * parseFloat(final_prcnt_allocation);
					f_prcnt_mnth_allo = parseFloat(f_prcnt_mnth_allo).toFixed(2);
					
					var total_revenue = parseFloat(f_prcnt_mnth_allo) * parseFloat(f_revenue);
					total_revenue = parseFloat(total_revenue).toFixed(2);
					f_total_revenue_for_tenure = parseFloat(f_total_revenue_for_tenure) + parseFloat(total_revenue);
					f_total_revenue_for_tenure = parseFloat(f_total_revenue_for_tenure).toFixed(2);
					
					var i_prac_subPrac_role_level = i_practice + '_' + i_sub_practice + '_' + i_role + '_' + i_level + '_' + i_location;
					
					projectWiseEffortPlan[i_effort_index] = {
							practice_name: s_practice,
							sub_prac_name: s_sub_practice,
							sub_practice: i_sub_practice,
							role_name: s_role,
							level_name: s_level,
							location_name: s_location,
							total_revenue_for_tenure: f_total_revenue_for_tenure,
							revenue_share: f_revenue_share,
							revenue_share_revised: 0,
							allocation_strt_date: s_allocation_strt_date,
							allocation_end_date: s_allocation_end_date,
							total_allocation: i_total_allocation[0],
							no_of_resources: i_no_of_resources,
							RevenueData: {}
						};
						
						projectWiseEffortPlan[i_effort_index].RevenueData[i_practice] = new yearData(i_year);
						
						projectWiseEffortPlan[i_effort_index].RevenueData[i_practice][s_month_name].Amount = total_revenue;
						projectWiseEffortPlan[i_effort_index].RevenueData[i_practice][s_month_name].prcnt_allocated = f_prcnt_mnth_allo;
					
					if (!projectWiseRevenue[i_prac_subPrac_role_level]) {
						projectWiseRevenue[i_prac_subPrac_role_level] = {
							practice_name: s_practice,
							sub_prac_name: s_sub_practice,
							sub_practice: i_sub_practice,
							role_name: s_role,
							level_name: s_level,
							location_name: s_location,
							total_revenue_for_tenure: f_total_revenue_for_tenure,
							revenue_share: f_revenue_share,
							revenue_share_revised: 0,
							allocation_strt_date: s_allocation_strt_date,
							allocation_end_date: s_allocation_end_date,
							total_allocation: i_total_allocation[0],
							no_of_resources: i_no_of_resources,
							RevenueData: {}
						};
						
						projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice] = new yearData(i_year);
						
						projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].Amount = total_revenue;
						projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].prcnt_allocated = f_prcnt_mnth_allo;
					//  nlapiLogExecution('DEBUG','Revenue data',JSON.stringify(projectWiseRevenue));	
					}
                
					else {
						//  nlapiLogExecution('DEBUG','Revenue data',JSON.stringify(projectWiseRevenue));
						var f_earlier_tenure_cost = projectWiseRevenue[i_prac_subPrac_role_level].total_revenue_for_tenure;
						//nlapiLogExecution('audit','i_prac_subPrac_role_level:- '+i_prac_subPrac_role_level,'f_earlier_tenure_cost:- '+f_earlier_tenure_cost);
						f_earlier_tenure_cost = parseFloat(f_earlier_tenure_cost) + parseFloat(f_total_revenue_for_tenure);
						
						var f_earlier_monthly_cost = projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].Amount;
						f_earlier_monthly_cost = parseFloat(f_earlier_monthly_cost) + parseFloat(total_revenue);
						
						var f_earlier_prcnt_allocated = projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].prcnt_allocated;
						f_earlier_prcnt_allocated = parseFloat(f_earlier_prcnt_allocated) + parseFloat(f_prcnt_mnth_allo);
						
						projectWiseRevenue[i_prac_subPrac_role_level].total_revenue_for_tenure = f_total_revenue_for_tenure;
						
						projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].Amount = f_earlier_monthly_cost;
						projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].prcnt_allocated = f_earlier_prcnt_allocated;
					}
				}
			}
			
			var o_form_obj = nlapiCreateForm('Project Budget Plan');
			
			// call client script to perform validation check
			o_form_obj.setScript('customscript_cli_fp_revrec_revenue_vali');
			
			o_form_obj.addFieldGroup('custpage_proj_info', 'Project Information');
			
			// create region field
			var fld_region_field = o_form_obj.addField('proj_region', 'select','Region','customrecord_region','custpage_proj_info').setDisplayType('inline');
			fld_region_field.setDefaultValue(s_project_region);
			
			// create customer field
			var fld_customer_field = o_form_obj.addField('customer_selected', 'select','Customer','customer','custpage_proj_info').setDisplayType('inline');
			fld_customer_field.setDefaultValue(i_customer_name);
			
			// create project field
			var fld_proj_field = o_form_obj.addField('proj_selected', 'select','Project','job','custpage_proj_info').setDisplayType('inline');
			fld_proj_field.setDefaultValue(i_projectId);
			
			// create project sow value field
			var fld_proj_sow_value = o_form_obj.addField('proj_sow_value', 'text','Project SOW value',null,'custpage_proj_info').setDisplayType('inline');
			fld_proj_sow_value.setDefaultValue(i_project_sow_value);
			
			// create field for project start date
			var fld_proj_start_date = o_form_obj.addField('proj_strt_date', 'date','Project Start Date',null,'custpage_proj_info').setDisplayType('inline');
			fld_proj_start_date.setDefaultValue(d_proj_start_date);
			
			// create field for projet end date
			var fld_proj_end_date = o_form_obj.addField('proj_end_date', 'date','Project End Date',null,'custpage_proj_info').setDisplayType('inline');
			fld_proj_end_date.setDefaultValue(d_proj_end_date);
			
			// create field for projet end date
			var fld_proj_end_date = o_form_obj.addField('proj_currency', 'text','Project Currency',null,'custpage_proj_info').setDisplayType('inline');
			fld_proj_end_date.setDefaultValue(s_proj_currency);
			
			// create field for project practice
			var fld_proj_practice = o_form_obj.addField('proj_practice', 'select','Project Executing Practice','department','custpage_proj_info').setDisplayType('inline');
			fld_proj_practice.setDefaultValue(i_proj_executing_practice);
			
			var i_project_executing_practice = nlapiLookupField('job',i_projectId,'custentity_practice');
			var i_executing_practice_head = nlapiLookupField('department',i_project_executing_practice,'custrecord_practicehead');
			
			// create field for approver
			var fld_plan_approver = o_form_obj.addField('budget_plan_approver', 'select','Approver','employee','custpage_proj_info').setDisplayType('inline');
			fld_plan_approver.setDefaultValue(i_executing_practice_head);
			
			// create field for poject manager
			var fld_proj_manager = o_form_obj.addField('proj_manager', 'select','Project Manager','employee','custpage_proj_info').setDisplayType('inline');
			fld_proj_manager.setDefaultValue(i_proj_manager);
			
			// create field for poject manager practice field
			var fld_proj_manager_practice = o_form_obj.addField('proj_manager_practice', 'select','Project Manager Practice','department','custpage_proj_info').setDisplayType('inline');
			fld_proj_manager_practice.setDefaultValue(i_proj_manager_practice);
			
			// create field for project revenue rec type
			var fld_proj_revRec_type = o_form_obj.addField('rev_rec_type', 'select','Revenue Recognition Type','customlist_fp_rev_rec_project_type','custpage_proj_info').setDisplayType('inline');
			fld_proj_revRec_type.setDefaultValue(i_proj_revenue_rec_type);
			
			// create field for request type i.e create or update
			var fld_existing_revenue_share_status = o_form_obj.addField('share_type', 'text','Request Type',null,'custpage_proj_info').setDisplayType('inline');
			fld_existing_revenue_share_status.setDefaultValue(s_request_mode);
			
			//var s_currency_symbol_proj = getCurrency_Symbol(s_proj_currency);
			var s_currency_symbol_usd = getCurrency_Symbol('USD');
				
			// Revenue share table
			var h_tableHtml_revenue_share = generate_table_effort_view(projectWiseRevenue,monthBreakUp,i_year_project,i_project_mnth,5,s_currency_symbol_proj,s_currency_symbol_usd,i_prject_end_mnth);
			o_form_obj.addFieldGroup('custpage_revenue_share', 'Revenue Share');
			o_form_obj.addField('revenue_share', 'inlinehtml', null, null, 'custpage_revenue_share').setDefaultValue(h_tableHtml_revenue_share);
			
			// Effort plan table
			var h_tableHtml_effort_plan = generate_table_effort_view(projectWiseEffortPlan,monthBreakUp,i_year_project,i_project_mnth,6,s_currency_symbol_proj,s_currency_symbol_usd,i_prject_end_mnth);
			o_form_obj.addFieldGroup('custpage_effrt_plan', 'Project Effort Plan');
			o_form_obj.addField('effort_plan', 'inlinehtml', null, null, 'custpage_effrt_plan').setDefaultValue(h_tableHtml_effort_plan);
			
			// Effort view table
			/*var h_tableHtml_effort_view = generate_table_effort_view(projectWiseRevenue,monthBreakUp,i_year_project,i_project_mnth,1);
			o_form_obj.addFieldGroup('custpage_effrt_view', 'Effort View');
			o_form_obj.addField('effort_view', 'inlinehtml', null, null, 'custpage_effrt_view').setDefaultValue(h_tableHtml_effort_view);
			*/
			
			// Cost view table
			o_form_obj.addFieldGroup('custpage_cost_view', 'Sub Practice Details');
			var temp_field = '<html>';
			o_form_obj.addField('temp_field', 'inlinehtml', null, null, 'custpage_cost_view').setDefaultValue(temp_field);
			
			for(var i_dupli = 0; i_dupli<a_duplicate_sub_prac_count.length; i_dupli++)
			{
				o_form_obj.addFieldGroup('custpage_cost_view_'+i_dupli, ''+a_duplicate_sub_prac_count[i_dupli].sub_prac_name);
				var h_tableHtml_cost_view = generate_cost_view(projectWiseRevenue,monthBreakUp,i_year_project,i_project_mnth,2,a_duplicate_sub_prac_count[i_dupli].sub_prac,s_currency_symbol_proj,s_currency_symbol_usd,i_year_project_end,i_prject_end_mnth);
				o_form_obj.addField('cost_view_'+i_dupli, 'inlinehtml', null, null, 'custpage_cost_view_'+i_dupli).setDefaultValue(h_tableHtml_cost_view);
			}
			
			// Monthly % cost table
			/*var h_tableHtml_monthly_cost = generate_table_effort_view(projectWiseRevenue,monthBreakUp,i_year_project,i_project_mnth,3);
			o_form_obj.addFieldGroup('custpage_monthly_cost', 'Monthly % cost completion');
			o_form_obj.addField('monthly_cost', 'inlinehtml', null, null, 'custpage_monthly_cost').setDefaultValue(h_tableHtml_monthly_cost);
			*/
			
			// Total revenue table
			var h_tableHtml_total_view = generate_total_project_view(projectWiseRevenue,monthBreakUp,i_year_project,i_project_mnth,4,s_currency_symbol_proj,s_currency_symbol_usd,i_year_project_end,i_prject_end_mnth);
			o_form_obj.addFieldGroup('custpage_total_view', 'Total revenue recognized / month');
			o_form_obj.addField('total_view', 'inlinehtml', null, null, 'custpage_total_view').setDefaultValue(h_tableHtml_total_view);
			
			
			if(s_request_mode == 'View' && i_revenue_share_status != 2 && i_revenue_share_status != 1) //&& b_revenue_rcrd_updated == 'T'
			{
				o_form_obj.addSubmitButton('Submit for review');
				//o_form_obj.addButton('back_button','Back','custom_back()');
			}
			else if(s_request_mode == 'Submit' && i_revenue_share_status != 1)
			{
				o_form_obj.addSubmitButton('Submit for approval');
			}
			else if(s_request_mode == 'View' && i_revenue_share_status == '')
			{
				o_form_obj.addSubmitButton('Submit for review');
				//o_form_obj.addButton('back_button','Back','custom_back()');
			}
			else if(i_revenue_share_status == 1 && b_revenue_rcrd_updated != 'T' && s_request_mode != 'Submit')
			{
				s_request_mode = 'Submit';
				fld_existing_revenue_share_status.setDefaultValue(s_request_mode);
				o_form_obj.addSubmitButton('Submit for approval');
			}
			else if(s_request_mode == 'PendingApproval' && i_revenue_share_status == 2)
			{
				//nlapiLogExecution('audit','inside aprove reject');
				// if logged in user is executing practice head display approve & reject button
								
				if(i_executing_practice_head == i_user_logegdIn_id)
				{
					//o_form_obj.setScript('customscript_sut_fp_rev_rec_proj_details');
					var fld_rejection_reason = o_form_obj.addField('rejection_reason', 'text','Rejection Reason',null,'custpage_proj_info');
					o_form_obj.addButton('approve_button', 'Approve', 'custom_Approve(\'  ' +i_revenue_share_id+ '  \')');
					o_form_obj.addButton('reject_button', 'Reject', 'custom_Reject(\'  ' +i_revenue_share_id+ '  \')');
				}
			}
			else if(s_request_mode == 'View' && i_revenue_share_status == 2)
			{
				
			}
			
			response.writePage(o_form_obj);
		}
	}
	catch(err)
	{
		nlapiLogExecution('ERROR','suiteletFunction_Gnerate_View','ERROR MESSAGE :- '+err);
	}
}

function format2(n) {
    n = parseFloat(n).toFixed(1).replace(/(\d)(?=(\d{3})+\.)/g, "$1,");
	n = n.toString().split('.');
	return n[0];
}

function generate_cost_view(projectWiseRevenue,monthBreakUp,i_year_project,i_project_mnth,mode,sub_prac,s_currency_symbol_proj,s_currency_symbol_usd,i_year_project_end,i_prject_end_mnth)
{
	var css_file_url = "";

	var context = nlapiGetContext();
	if (context.getEnvironment() == "SANDBOX") {
		css_file_url = "https://system.sandbox.netsuite.com/core/media/media.nl?id=199133&c=3883006&h=64fa194295cd4ab64d58&mv=iq3kjr6g&_xt=.css&whence=";
	} else if (context.getEnvironment() == "PRODUCTION") {
		css_file_url = "https://3883006.app.netsuite.com/core/media/media.nl?id=199133&c=3883006&h=64fa194295cd4ab64d58&_xt=.css";
	}

	var html = "";
	html += "<link href='" + css_file_url + "'  rel='stylesheet'>";

	html += "<table class='projection-table'>";

	// header
	html += "<tr class='header-row'>";
	
	html += "<td>";
	html += "";
	html += "</td>";
	
	html += "<td>";
	html += "Practice";
	html += "</td>";
	
	html += "<td>";
	html += "Sub Practice";
	html += "</td>";
	
	html += "<td>";
	html += "Role";
	html += "</td>";
	
	html += "<td>";
	html += "Level";
	html += "</td>";
	
	html += "<td>";
	html += "Location";
	html += "</td>";
		
	
	var today = new Date();
	var currentMonthName = getMonthName(nlapiDateToString(today));
	
	for (var j = 0; j < monthBreakUp.length; j++)
	{
		var months = monthBreakUp[j];
		var s_month_name = getMonthName(months.Start); // get month name
		var s_year = nlapiStringToDate(months.Start)
		s_year = s_year.getFullYear();
		s_month_name = s_month_name+'_'+s_year;
				
		html += "<td>";
		html += s_month_name;
		html += "</td>";
	}
		
	html += "<td>";
	html += "Total";
	html += "</td>";
			
	html += "</tr>";
	
	var a_sub_prac_already_displayed = new Array();
	var i_subpractice_id = 0;
	var a_amount = new Array();
	var i_frst_row = 0;
	var i_diplay_frst_column = 0;
	var i_total_row_revenue = 0;
	var total_prcnt_aloocated = new Array();
	var b_first_line_copied = 0;
	var f_total_allocated_row = 0;
	var f_total_per_row_cost = 0;
	
	for (var emp_internal_id in projectWiseRevenue)
	{
		if (projectWiseRevenue[emp_internal_id].sub_practice == sub_prac)
		{
			f_total_allocated_row = 0;
			for (var project_internal_id in projectWiseRevenue[emp_internal_id].RevenueData)
			{
				html += "<tr>";
				
				if(i_diplay_frst_column == 0)
				{
					html += "<td class='label-name'>";
					html += '<b>Effort View';
					html += "</td>";
				}
				else
				{
					html += "<td class='label-name'>";
					html += '<b>';
					html += "</td>";
				}
				
				html += "<td class='label-name'>";
				html += projectWiseRevenue[emp_internal_id].practice_name;
				html += "</td>";
						
				html += "<td class='label-name'>";
				html += projectWiseRevenue[emp_internal_id].sub_prac_name;
				html += "</td>";
			
				html += "<td class='label-name'>";
				html += projectWiseRevenue[emp_internal_id].role_name;
				html += "</td>";
					
				html += "<td class='label-name'>";
				html += projectWiseRevenue[emp_internal_id].level_name;
				html += "</td>";
				
				html += "<td class='label-name'>";
				html += projectWiseRevenue[emp_internal_id].location_name;
				html += "</td>";
				
				var i_index_mnth = 0;
				//nlapiLogExecution('audit','project year :- ',i_year_project);
				//nlapiLogExecution('audit','project month :- ',i_project_mnth);
				for (var month in projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id])
				{
					var a_MonthNames = MonthNames(i_year_project);
					var currentMonthPos = a_MonthNames.indexOf(month);
					var s_current_month = a_MonthNames[currentMonthPos];
					var s_current_mnth_yr = s_current_month.substr(4, 8);
					
					var i_nxt_yr_flg = 0;
					if(currentMonthPos >=12)
					{
						currentMonthPos = currentMonthPos - 12;
						i_nxt_yr_flg = 1;
					}
					
					var i_curr_yr_dis_flag = 0;
					if(parseInt(s_current_mnth_yr) < i_year_project_end && i_prject_end_mnth < currentMonthPos)
					{
						i_curr_yr_dis_flag = 1;
					}
					
					//nlapiLogExecution('audit','currentMonthPos::::'+currentMonthPos,'i_project_mnth:- '+i_project_mnth+':: i_prject_end_mnth:- '+i_prject_end_mnth);
					if (((i_project_mnth <= currentMonthPos && ((i_prject_end_mnth >= currentMonthPos || i_curr_yr_dis_flag == 1) && i_nxt_yr_flg == 0) && parseInt(s_current_mnth_yr) <= i_year_project) && parseInt(s_current_mnth_yr) <= i_year_project_end) || (i_prject_end_mnth >= currentMonthPos && parseInt(s_current_mnth_yr) <= i_year_project_end && i_nxt_yr_flg == 1))
					{
						//nlapiLogExecution('audit','i_project_mnth:- '+i_project_mnth+':::i_prject_end_mnth:- '+i_prject_end_mnth+'::::currentMonthPos:- '+currentMonthPos,s_current_mnth_yr);
						if(b_first_line_copied == 1)
						{
							var f_allocated_prcnt = total_prcnt_aloocated[i_index_mnth];
							f_allocated_prcnt = parseFloat(f_allocated_prcnt) + parseFloat(projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id][month].prcnt_allocated);
							total_prcnt_aloocated[i_index_mnth] = f_allocated_prcnt;
							i_index_mnth++;							
						}
						else
						{
							total_prcnt_aloocated.push(projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id][month].prcnt_allocated);
						}
						
						f_total_allocated_row = parseFloat(projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id][month].prcnt_allocated) + parseFloat(f_total_allocated_row);
						
						html += "<td class='monthly-amount'>";
						html += parseFloat(projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id][month].prcnt_allocated).toFixed(2);
						html += "</td>";
					}
				}
			}
			
			b_first_line_copied = 1;
			
			html += "<td class='monthly-amount'>";
			html += parseFloat(f_total_allocated_row).toFixed(2);
			html += "</td>";
			
			html += "</tr>";
			
			var s_sub_prac_name = projectWiseRevenue[emp_internal_id].sub_prac_name;
			var s_practice_name = projectWiseRevenue[emp_internal_id].practice_name;
			
			i_diplay_frst_column = 1;
		}
	}
	i_diplay_frst_column = 0;
	
	html += "<tr>";
	html += "<td class='label-name'>";
	html += '<b>Sub Total';
	html += "</td>";
	
	html += "<td class='label-name'>";
	html += '<b>'+s_practice_name;
	html += "</td>";
			
	html += "<td class='label-name'>";
	html += '<b>'+s_sub_prac_name;
	html += "</td>";

	html += "<td class='label-name'>";
	html += '';
	html += "</td>";
		
	html += "<td class='label-name'>";
	html += '';
	html += "</td>";
	
	html += "<td class='label-name'>";
	html += '';
	html += "</td>";
	
	var f_total_row_allocation = 0;
	for(var i_index_total_allocated=0; i_index_total_allocated<total_prcnt_aloocated.length; i_index_total_allocated++)
	{
		html += "<td class='monthly-amount'>";
		html += '<b>'+parseFloat(total_prcnt_aloocated[i_index_total_allocated]).toFixed(2);
		html += "</td>";
		
		f_total_row_allocation = parseFloat(f_total_row_allocation) + parseFloat(total_prcnt_aloocated[i_index_total_allocated]);
	}
	
	html += "<td class='monthly-amount'>";
	html += '<b>'+parseFloat(f_total_row_allocation).toFixed(2);
	html += "</td>";
		
	html += "</tr>";

	
	for ( var emp_internal_id in projectWiseRevenue) {
		f_total_per_row_cost = 0;
		for ( var project_internal_id in projectWiseRevenue[emp_internal_id].RevenueData) {
			
			if(projectWiseRevenue[emp_internal_id].sub_practice == sub_prac)
			{
				html += "<tr>";
				
				if (i_diplay_frst_column == 0)
				{
					html += "<td class='label-name'>";
					html += '<b>Cost View';
					html += "</td>";
				}
				else
				{
					html += "<td class='label-name'>";
					html += '<b>';
					html += "</td>";
				}
				
				html += "<td class='label-name'>";
				html += projectWiseRevenue[emp_internal_id].practice_name;
				html += "</td>";
						
				html += "<td class='label-name'>";
				html += projectWiseRevenue[emp_internal_id].sub_prac_name;
				html += "</td>";
			
				html += "<td class='label-name'>";
				html += projectWiseRevenue[emp_internal_id].role_name;
				html += "</td>";
					
				html += "<td class='label-name'>";
				html += projectWiseRevenue[emp_internal_id].level_name;
				html += "</td>";
				
				html += "<td class='label-name'>";
				html += projectWiseRevenue[emp_internal_id].location_name;
				html += "</td>";
					
				var i_sub_practice_internal_id = 0;
				var i_amt = 0;
				for (var month in projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id])
				{
					var a_MonthNames = MonthNames(i_year_project);
					var currentMonthPos = a_MonthNames.indexOf(month);
					var s_current_month = a_MonthNames[currentMonthPos];
					var s_current_mnth_yr = s_current_month.substr(4, 8);
					var i_nxt_yr_flg = 0;
					
					if(currentMonthPos >=12)
					{
						i_nxt_yr_flg = 1;
						currentMonthPos = currentMonthPos - 12;
					}
					
					var i_curr_yr_dis_flag = 0;
					if(parseInt(s_current_mnth_yr) < i_year_project_end && i_prject_end_mnth < currentMonthPos)
					{
						i_curr_yr_dis_flag = 1;
					}
					
					if (((i_project_mnth <= currentMonthPos && ((i_prject_end_mnth >= currentMonthPos || i_curr_yr_dis_flag == 1)&& i_nxt_yr_flg == 0) && parseInt(s_current_mnth_yr) <= i_year_project) && parseInt(s_current_mnth_yr) <= i_year_project_end) || (i_prject_end_mnth >= currentMonthPos && parseInt(s_current_mnth_yr) <= i_year_project_end && i_nxt_yr_flg == 1))
					{
						
						if (mode == 2)
						{
							
							html += "<td class='monthly-amount'>";
							var total_revenue_format = parseFloat(projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id][month].Amount).toFixed(1);
							html += '$ '+format2(total_revenue_format);
							
							f_total_per_row_cost = parseFloat(projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id][month].Amount) + parseFloat(f_total_per_row_cost);
							
							if(i_frst_row == 0)
							{
								a_amount.push(total_revenue_format);
							}
							else
							{
								var amnt_mnth = a_amount[i_amt];
								amnt_mnth = parseFloat(amnt_mnth) + parseFloat(projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id][month].Amount);
								
								a_amount[i_amt] = amnt_mnth;
								i_amt++;
							}
						}
						
						html += "</td>";
					}
				}
				
				if (mode == 2)
				{
					html += "<td class='monthly-amount'>";
					//html += ''+s_currency_symbol_proj+' '+format2(projectWiseRevenue[emp_internal_id].total_revenue_for_tenure);
					html += '$'+format2(f_total_per_row_cost);
					html += "</td>";
					
					//i_total_row_revenue = parseFloat(i_total_row_revenue) + parseFloat(projectWiseRevenue[emp_internal_id].total_revenue_for_tenure);
					i_total_row_revenue = parseFloat(i_total_row_revenue) + parseFloat(f_total_per_row_cost);
				}
				
				i_frst_row = 1;
	
				html += "</tr>";
				
				i_diplay_frst_column = 1;
				
				var f_revenue_share = projectWiseRevenue[emp_internal_id].revenue_share;
			}			
		}
	}
	
	var s_sub_prac_name = '';
	for (var emp_internal_id in projectWiseRevenue)
	{
		if (projectWiseRevenue[emp_internal_id].sub_practice == sub_prac)
		{
			s_sub_prac_name = projectWiseRevenue[emp_internal_id].sub_prac_name;
			s_practice_name = projectWiseRevenue[emp_internal_id].practice_name;
			projectWiseRevenue[emp_internal_id].revenue_share_revised = i_total_row_revenue;
			projectWiseRevenue[emp_internal_id].total_revenue_for_tenure = i_total_row_revenue;
		}
	}
	
	html += "</tr>";
	
	html += "<td class='label-name'>";
	html += '<b>Sub total';
	html += "</td>";
				
	html += "<td class='label-name'>";
	html += '<b>'+s_practice_name;
	html += "</td>";
			
	html += "<td class='label-name'>";
	html += '<b>'+s_sub_prac_name;
	html += "</td>";

	html += "<td class='label-name'>";
	html += '';
	html += "</td>";
		
	html += "<td class='label-name'>";
	html += '';
	html += "</td>";
	
	html += "<td class='label-name'>";
	html += '';
	html += "</td>";
	
	for (var i_amt = 0; i_amt < a_amount.length; i_amt++)
	{
		html += "<td class='projected-amount'>";
		html += '<b>$'+format2(a_amount[i_amt]);
		html += "</td>";
	}
	
	html += "<td class='projected-amount'>";
	html += '<b>$'+format2(i_total_row_revenue);
	html += "</td>";
	
	html += "</tr>";
	
	// Percent row
	html += "</tr>";
	
	html += "<td class='label-name'>";
	html += '<b>Monthly % Cost Completion';
	html += "</td>";
	
	html += "<td class='label-name'>";
	html += '<b>'+s_practice_name;
	html += "</td>";
			
	html += "<td class='label-name'>";
	html += '<b>'+s_sub_prac_name;
	html += "</td>";

	html += "<td class='label-name'>";
	html += '';
	html += "</td>";
		
	html += "<td class='label-name'>";
	html += '';
	html += "</td>";
	
	html += "<td class='label-name'>";
	html += '';
	html += "</td>";
	
	var f_total_prcnt = 0;
	for (var i_amt = 0; i_amt < a_amount.length; i_amt++)
	{
		var f_prcnt_revenue = parseFloat(a_amount[i_amt]) / parseFloat(i_total_row_revenue);
		f_prcnt_revenue = f_prcnt_revenue * 100;
		f_total_prcnt = parseFloat(f_prcnt_revenue) + parseFloat(f_total_prcnt);
		
		html += "<td class='projected-amount'>";
		html += '<b>'+parseFloat(f_prcnt_revenue).toFixed(1)+' %';
		html += "</td>";
	}
	
	html += "<td class='projected-amount'>";
	html += '<b>'+parseFloat(f_total_prcnt).toFixed(1)+' %';
	html += "</td>";
	
	html += "</tr>";
	
	//Total reveue recognized per month row
	html += "<tr>";
	
	html += "<td class='label-name'>";
	html += '<b>Revenue to be reconized/month';
	html += "</td>";
	
	html += "<td class='label-name'>";
	html += '<b>'+s_practice_name;
	html += "</td>";
			
	html += "<td class='label-name'>";
	html += '<b>'+s_sub_prac_name;
	html += "</td>";

	html += "<td class='label-name'>";
	html += '';
	html += "</td>";
		
	html += "<td class='label-name'>";
	html += '';
	html += "</td>";
	
	html += "<td class='label-name'>";
	html += '';
	html += "</td>";
	
	var f_total_revenue = 0;
	for (var i_amt = 0; i_amt < a_amount.length; i_amt++)
	{
		var f_prcnt_revenue = parseFloat(a_amount[i_amt]) / parseFloat(i_total_row_revenue);
		f_prcnt_revenue = f_prcnt_revenue * 100;
		
		var f_revenue_amount = (parseFloat(f_prcnt_revenue) * parseFloat(f_revenue_share)) / 100;
		
		f_total_revenue = parseFloat(f_revenue_amount) + parseFloat(f_total_revenue);
		
		html += "<td class='projected-amount'>";
		html += '<b>'+s_currency_symbol_proj+' '+format2(f_revenue_amount);
		html += "</td>";
	}
	
	for (var emp_internal_id in projectWiseRevenue)
	{
		for (var project_internal_id in projectWiseRevenue[emp_internal_id].RevenueData)
		{
			var i_amount_map = 0;
			if (projectWiseRevenue[emp_internal_id].sub_practice == sub_prac)
			{
				for (var month in projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id])
				{
					var a_MonthNames = MonthNames(i_year_project);
					var currentMonthPos = a_MonthNames.indexOf(month);
					var s_current_month = a_MonthNames[currentMonthPos];
					var s_current_mnth_yr = s_current_month.substr(4, 8);
					var i_nxt_yr_flg = 0;
					
					if(currentMonthPos >=12)
					{
						i_nxt_yr_flg = 1;
						currentMonthPos = currentMonthPos - 12;
					}
					
					var i_curr_yr_dis_flag = 0;
					if(parseInt(s_current_mnth_yr) < i_year_project_end && i_prject_end_mnth < currentMonthPos)
					{
						i_curr_yr_dis_flag = 1;
					}
					
					if (((i_project_mnth <= currentMonthPos && ((i_prject_end_mnth >= currentMonthPos || i_curr_yr_dis_flag == 1) && i_nxt_yr_flg == 0) && parseInt(s_current_mnth_yr) <= i_year_project) && parseInt(s_current_mnth_yr) <= i_year_project_end) || (i_prject_end_mnth >= currentMonthPos && parseInt(s_current_mnth_yr) <= i_year_project_end && i_nxt_yr_flg == 1))
					{
						var f_prcnt_revenue = parseFloat(a_amount[i_amount_map]) / parseFloat(i_total_row_revenue);
						f_prcnt_revenue = f_prcnt_revenue * 100;
						
						var f_revenue_amount = (parseFloat(f_prcnt_revenue) * parseFloat(f_revenue_share)) / 100;
		
						projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id][month].total_revenue = f_revenue_amount;
						i_amount_map++;
					}
				}	
			}
		}
	}
	
	
	
	html += "<td class='projected-amount'>";
	html += '<b>'+s_currency_symbol_proj+' '+format2(f_total_revenue);
	html += "</td>";
	
	html += "</tr>";
	
	html += "</table>";
	
	return html;
}

function generate_total_project_view(projectWiseRevenue,monthBreakUp,i_year_project,i_project_mnth,mode,s_currency_symbol_proj,s_currency_symbol_usd,i_year_project_end,i_prject_end_mnth)
{
	var css_file_url = "";

	var context = nlapiGetContext();
	if (context.getEnvironment() == "SANDBOX") {
		css_file_url = "https://system.sandbox.netsuite.com/core/media/media.nl?id=199133&c=3883006&h=64fa194295cd4ab64d58&mv=iq3kjr6g&_xt=.css&whence=";
	} else if (context.getEnvironment() == "PRODUCTION") {
		css_file_url = "https://3883006.app.netsuite.com/core/media/media.nl?id=199133&c=3883006&h=64fa194295cd4ab64d58&_xt=.css";
	}

	var html = "";
	html += "<link href='" + css_file_url + "'  rel='stylesheet'>";

	html += "<table class='projection-table'>";

	// header
	html += "<tr class='header-row'>";
	
	html += "<td>";
	html += "";
	html += "</td>";
	
	var today = new Date();
	var currentMonthName = getMonthName(nlapiDateToString(today));
	
	for (var j = 0; j < monthBreakUp.length; j++)
	{
		var months = monthBreakUp[j];
		var s_month_name = getMonthName(months.Start); // get month name
		var s_year = nlapiStringToDate(months.Start)
		s_year = s_year.getFullYear();
		s_month_name = s_month_name+'_'+s_year;
				
		html += "<td>";
		html += s_month_name;
		html += "</td>";
	}
		
	html += "<td>";
	html += "Total";
	html += "</td>";
			
	html += "</tr>";
	
	var a_sub_prac_already_displayed = new Array();
	var i_subpractice_id = 0;
	var a_amount = new Array();
	var i_frst_row = 0;
	var i_diplay_frst_column = 0;
	var i_total_row_revenue = 0;
	var total_prcnt_aloocated = new Array();
	var b_first_line_copied = 0;
	var f_total_allocated_row = 0;
	var i_total_row_revenue = 0;
	var a_sub_practice_arr = new Array();
	
	for ( var emp_internal_id in projectWiseRevenue)
	{
		for ( var project_internal_id in projectWiseRevenue[emp_internal_id].RevenueData)
		{
			var i_sub_practice_internal_id = 0;
			var i_amt = 0;
			
			if (a_sub_practice_arr.indexOf(projectWiseRevenue[emp_internal_id].sub_practice) < 0)
			{
				for (var month in projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id])
				{
					var a_MonthNames = MonthNames(i_year_project);
					var currentMonthPos = a_MonthNames.indexOf(month);
					var s_current_month = a_MonthNames[currentMonthPos];
					var s_current_mnth_yr = s_current_month.substr(4, 8);
					var i_nxt_yr_flg = 0;
					
					if(currentMonthPos >=12)
					{
						i_nxt_yr_flg = 1;
						currentMonthPos = currentMonthPos - 12;
					}
					
					var i_curr_yr_dis_flag = 0;
					if(parseInt(s_current_mnth_yr) < i_year_project_end && i_prject_end_mnth < currentMonthPos)
					{
						i_curr_yr_dis_flag = 1;
					}
					
					if (((i_project_mnth <= currentMonthPos && ((i_prject_end_mnth >= currentMonthPos || i_curr_yr_dis_flag == 1) && i_nxt_yr_flg == 0) && parseInt(s_current_mnth_yr) <= i_year_project) && parseInt(s_current_mnth_yr) <= i_year_project_end) || (i_prject_end_mnth >= currentMonthPos && parseInt(s_current_mnth_yr) <= i_year_project_end && i_nxt_yr_flg == 1))
					{
						var total_revenue_format = parseFloat(projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id][month].total_revenue);
						
						if (i_frst_row == 0)
						{
							a_amount.push(total_revenue_format);
						}
						else
						{
							var amnt_mnth = a_amount[i_amt];
							amnt_mnth = parseFloat(amnt_mnth) + parseFloat(projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id][month].total_revenue);
							a_amount[i_amt] = amnt_mnth;
							i_amt++;
						}
						
						a_sub_practice_arr.push(projectWiseRevenue[emp_internal_id].sub_practice);
						
					}
				}
			}
			i_frst_row = 1;
	
			var f_revenue_share = projectWiseRevenue[emp_internal_id].revenue_share;			
		}
	}
	
	html += "<tr>";
				
	html += "<td class='label-name'>";
	html += '<b>Revenue to be reconized/month';
	html += "</td>";
	
	for (var i_amt = 0; i_amt < a_amount.length; i_amt++)
	{
		html += "<td class='projected-amount'>";
		html += '<b>'+s_currency_symbol_proj+' '+format2(a_amount[i_amt]);
		html += "</td>";
		
		i_total_row_revenue = parseFloat(i_total_row_revenue) + parseFloat(a_amount[i_amt]);
		
	}
	
	html += "<td class='projected-amount'>";
	html += '<b>'+s_currency_symbol_proj+' '+format2(i_total_row_revenue);
	html += "</td>";
	
	html += "</tr>";
	
	html += "</table>";
	
	return html;
}

function generate_table_effort_view(projectWiseRevenue,monthBreakUp,i_year_project,i_project_mnth,mode,s_currency_symbol_proj,s_currency_symbol_usd,i_year_project_end,i_prject_end_mnth)
{
	var css_file_url = "";

	var context = nlapiGetContext();
	if (context.getEnvironment() == "SANDBOX") {
		css_file_url = "https://system.na1.netsuite.com/core/media/media.nl?id=199133&c=3883006&h=64fa194295cd4ab64d58&mv=iq3kjr6g&_xt=.css&whence=";
	} else if (context.getEnvironment() == "PRODUCTION") {
		css_file_url = "https://3883006.app.netsuite.com/core/media/media.nl?id=199133&c=3883006&h=64fa194295cd4ab64d58&_xt=.css";
	}

	var html = "";
	html += "<link href='" + css_file_url + "'  rel='stylesheet'>";

	html += "<table class='projection-table'>";

	// header
	html += "<tr class='header-row'>";
	
	html += "<td>";
	html += "Practice";
	html += "</td>";
	
	html += "<td>";
	html += "Sub Practice";
	html += "</td>";

	if(mode != 5)
	{	
		html += "<td>";
		html += "Role";
		html += "</td>";
	
		if(mode == 1 || mode == 2 || mode == 6)
		{
			html += "<td>";
			html += "Level";
			html += "</td>";
			
			if(mode == 6)
			{
				html += "<td>";
				html += "No. of resources";
				html += "</td>";
				
				html += "<td>";
				html += "Allocation start date";
				html += "</td>";
				
				html += "<td>";
				html += "Allocation end date";
				html += "</td>";
				
				html += "<td>";
				html += "Percent allocated";
				html += "</td>";
			}
			
			html += "<td>";
			html += "Location";
			html += "</td>";
		}
	}
	else
	{
		html += "<td>";
		html += "Revenue Share";
		html += "</td>";
	}
	
	if(mode != 5 && mode != 6)
	{
		var today = new Date();
		var currentMonthName = getMonthName(nlapiDateToString(today));
		
		for (var j = 0; j < monthBreakUp.length; j++)
		{
			var months = monthBreakUp[j];
			var s_month_name = getMonthName(months.Start); // get month name
			var s_year = nlapiStringToDate(months.Start)
			s_year = s_year.getFullYear();
			s_month_name = s_month_name+'_'+s_year;
					
			html += "<td>";
			html += s_month_name;
			html += "</td>";
		}
		
		if (mode == 2)
		{
			html += "<td>";
			html += "Total";
			html += "</td>";
		}
	}
		
	html += "</tr>";
	
	var a_sub_prac_already_displayed = new Array();
	var i_subpractice_id = 0;
	for ( var emp_internal_id in projectWiseRevenue) {
		
		for ( var project_internal_id in projectWiseRevenue[emp_internal_id].RevenueData) {
			
			var b_display_reveue_share = 'F';
			if(a_sub_prac_already_displayed.indexOf(projectWiseRevenue[emp_internal_id].sub_practice) < 0 && mode == 5)
			{
				html += "<tr>";
				html += "<td class='label-name'>";
				html += projectWiseRevenue[emp_internal_id].practice_name;
				html += "</td>";
					
				html += "<td class='label-name'>";
				html += projectWiseRevenue[emp_internal_id].sub_prac_name;
				html += "</td>";
				
				b_display_reveue_share = 'T';
				a_sub_prac_already_displayed.push(projectWiseRevenue[emp_internal_id].sub_practice);
			}
			
			if(mode != 5)
			{
				html += "<tr>";
				html += "<td class='label-name'>";
				html += projectWiseRevenue[emp_internal_id].practice_name;
				html += "</td>";
					
				html += "<td class='label-name'>";
				html += projectWiseRevenue[emp_internal_id].sub_prac_name;
				html += "</td>";
			}
			
			if (mode != 5)
			{
				html += "<td class='label-name'>";
				html += projectWiseRevenue[emp_internal_id].role_name;
				html += "</td>";
				
				if (mode == 1 || mode == 2 || mode == 6)
				{
					html += "<td class='label-name'>";
					html += projectWiseRevenue[emp_internal_id].level_name;
					html += "</td>";
					
					if(mode == 6)
					{
						html += "<td class='label-name'>";
						html += projectWiseRevenue[emp_internal_id].no_of_resources;
						html += "</td>";
						
						html += "<td class='label-name'>";
						html += projectWiseRevenue[emp_internal_id].allocation_strt_date;
						html += "</td>";
						
						html += "<td class='label-name'>";
						html += projectWiseRevenue[emp_internal_id].allocation_end_date;
						html += "</td>";
						
						html += "<td class='label-name'>";
						html += projectWiseRevenue[emp_internal_id].total_allocation;
						html += "</td>";
					}
					
					html += "<td class='label-name'>";
					html += projectWiseRevenue[emp_internal_id].location_name;
					html += "</td>";
				}
			}
			else
			{
				if (b_display_reveue_share == 'T')
				{
					html += "<td class='label-name'>";
					html += ''+s_currency_symbol_proj+' '+format2(projectWiseRevenue[emp_internal_id].revenue_share);
					html += "</td>";
				}
			}
			
			if (mode != 5 && mode != 6)
			{
				var i_sub_practice_internal_id = 0;
				for (var month in projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id])
				{
					var a_MonthNames = MonthNames(i_year_project);
					var currentMonthPos = a_MonthNames.indexOf(month);
					var s_current_month = a_MonthNames[currentMonthPos];
					var s_current_mnth_yr = s_current_month.substr(4, 8);
					var i_nxt_yr_flg = 0;
					
					if(currentMonthPos >=12)
					{
						i_nxt_yr_flg = 1;
						currentMonthPos = currentMonthPos - 12;
					}
					
					var i_curr_yr_dis_flag = 0;
					if(parseInt(s_current_mnth_yr) < i_year_project_end && i_prject_end_mnth < currentMonthPos)
					{
						i_curr_yr_dis_flag = 1;
					}
					
					if (((i_project_mnth <= currentMonthPos && ((i_prject_end_mnth >= currentMonthPos || i_curr_yr_dis_flag == 1)&& i_nxt_yr_flg == 0) && parseInt(s_current_mnth_yr) <= i_year_project) && parseInt(s_current_mnth_yr) <= i_year_project_end) || (i_prject_end_mnth >= currentMonthPos && parseInt(s_current_mnth_yr) <= i_year_project_end && i_nxt_yr_flg == 1))
					{
						
						if (mode == 2)
						{
							html += "<td class='monthly-amount'>";
							var total_revenue_format = parseFloat(projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id][month].Amount).toFixed(1);
							html += format2(total_revenue_format);
						}
						else if (mode == 1)
						{
							html += "<td class='monthly-amount'>";
							html += parseFloat(projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id][month].prcnt_allocated).toFixed(2);
							
							if(i_sub_practice_internal_id = 0)
							{
								i_sub_practice_internal_id = projectWiseRevenue[emp_internal_id].sub_practice;
							}
						}
						else if (mode == 3 || mode == 4)
						{
							var f_amount = projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id][month].Amount;
							var f_total_amount = projectWiseRevenue[emp_internal_id].total_revenue_for_tenure;
							
							var f_prcnt_cost = parseFloat(f_amount) / parseFloat(f_total_amount);
							var f_prcnt_cost_ = parseFloat(f_prcnt_cost) * parseFloat(100);
							
							if (mode == 3)
							{
								f_prcnt_cost_ = parseFloat(f_prcnt_cost_).toFixed(1);
								html += "<td class='monthly-amount'>";
								html += f_prcnt_cost_+' %';
							}
							else
							{
								html += "<td class='projected-amount'>";
								var f_revenue_share = projectWiseRevenue[emp_internal_id].revenue_share;
								if(projectWiseRevenue[emp_internal_id].sub_practice == 316)
									nlapiLogExecution('audit','prcnt:- '+f_prcnt_cost,f_revenue_share);
									
								f_revenue_share = (parseFloat(f_prcnt_cost_) * parseFloat(f_revenue_share)) / 100;
								html += format2(f_revenue_share);
							}
						}
						
						html += "</td>";
					}
				}
			}
			
			if (mode == 2)
			{
				html += "<td class='monthly-amount'>";
				html += format2(projectWiseRevenue[emp_internal_id].total_revenue_for_tenure);
				html += "</td>";
			}
			
			html += "</tr>";			
		}
	}

	html += "</table>";
	
	return html;
}

function getCurrency_Symbol(s_proj_currency)
{
	var s_currency_symbols = {
    'USD': '$', // US Dollar
    'EUR': '€', // Euro
    'GBP': '£', // British Pound Sterling
    'INR': '₹', // Indian Rupee
	'CAD': 'c$', // Indian Rupee
	'RON': 'lei', // Indian Rupee
	};
	
	if(s_currency_symbols[s_proj_currency]!==undefined) {
	    return s_currency_symbols[s_proj_currency];
	}
}

function getDatediffIndays(startDate, endDate)
{
    var one_day=1000*60*60*24;
    var fromDate = startDate;
    var toDate=endDate

	//var date1 = nlapiStringToDate(fromDate);

   //var date2 = nlapiStringToDate(toDate);
	
	var date1 = fromDate;
	var date2 = toDate;
	
    var date3=Math.round((date2-date1)/one_day);

    return (date3+1);
}

function getMonthsBreakup(d_startDate, d_endDate) {
	
	try
	{
		var dateBreakUp = [];
		var new_start_date = new Date(d_startDate.getFullYear(), d_startDate
		        .getMonth(), 1);
				
		var nxt_mnth_strt_date = new Date(d_startDate.getFullYear(), d_startDate
		        .getMonth()+1, 1);
		
		var endDate = new Date(nxt_mnth_strt_date - 1);	
		//var endDate = nlapiAddDays(nlapiAddMonths(d_startDate, 1), -1);
		endDate = nlapiDateToString(endDate, 'date');
	
		dateBreakUp.push({
		    Start : nlapiDateToString(new_start_date, 'date'),
		    End : endDate
		});
	
		for (var i = 1;; i++) {
			var new_date = nlapiAddMonths(new_start_date, i);
	
			if (new_date > d_endDate) {
				break;
			}
			
			endDate = nlapiAddDays(nlapiAddMonths(new_date, 1), -1);
			endDate = nlapiDateToString(endDate, 'date');
					
			dateBreakUp.push({
			    Start : nlapiDateToString(new_date, 'date'),
			    End : endDate
			});
		}
	}
	catch(err)
	{
		nlapiLogExecution('ERROR','getMonthsBreakup','ERROR MESSAGE :- '+err);
	}

	return dateBreakUp;
}

function getMonthName(currentDate) {
	currentDate = nlapiStringToDate(currentDate)
	var monthNames = [ "JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JUL", "AUG",
	        "SEP", "OCT", "NOV", "DEC" ];
	return monthNames[currentDate.getMonth()];
}

function yearData(yr) {
	var yr1 = Number(yr) + 1;
	
	var monthNames = [ "JAN_"+yr, "FEB_"+yr, "MAR_"+yr, "APR_"+yr, "MAY_"+yr, "JUN_"+yr, "JUL_"+yr, "AUG_"+yr,
	        "SEP_"+yr, "OCT_"+yr, "NOV_"+yr, "DEC_"+yr ];
	var month_nxt_yr = ["JAN_"+yr1, "FEB_"+yr1, "MAR_"+yr1, "APR_"+yr1, "MAY_"+yr1, "JUN_"+yr1, "JUL_"+yr1, "AUG_"+yr1,
	        "SEP_"+yr1, "OCT_"+yr1, "NOV_"+yr1, "DEC_"+yr1 ];
	monthNames = monthNames.concat(month_nxt_yr);
	
	for (var i = 0; i < monthNames.length; i++) {
		this[monthNames[i]] = {
		    Amount : 0,
			prcnt_allocated : 0,
			total_revenue : 0
		};
	}
}

function MonthNames(yr) {
	var yr1 = Number(yr) + 1;
	
	var monthNames = [ "JAN_"+yr, "FEB_"+yr, "MAR_"+yr, "APR_"+yr, "MAY_"+yr, "JUN_"+yr, "JUL_"+yr, "AUG_"+yr,
	        "SEP_"+yr, "OCT_"+yr, "NOV_"+yr, "DEC_"+yr ];
	var month_nxt_yr = ["JAN_"+yr1, "FEB_"+yr1, "MAR_"+yr1, "APR_"+yr1, "MAY_"+yr1, "JUN_"+yr1, "JUL_"+yr1, "AUG_"+yr1,
	        "SEP_"+yr1, "OCT_"+yr1, "NOV_"+yr1, "DEC_"+yr1 ];
	monthNames = monthNames.concat(month_nxt_yr);
	
	return monthNames;
}