function scheduled(type) {
    var result = "";
    try {
		
		var i_context = nlapiGetContext();
		
		var proj_selected = i_context.getSetting('SCRIPT', 'custscript_proj_selected');
		proj_selected = 23694;
		var bill_from = i_context.getSetting('SCRIPT', 'custscript_bill_from');
		var bill_to = i_context.getSetting('SCRIPT', 'custscript_bill_to');
		var csv_file_id = i_context.getSetting('SCRIPT', 'custscript_csv_file_id');
				
		 var csvFileId = parseInt(csv_file_id);
		 
        if (csvFileId) {
			var submit_flag = 0;
            var csvFile = nlapiLoadFile(csvFileId);
            var csvContent = csvFile.getValue();
           	var rows_data = csvContent.split('\r\n');
			var total_rows = parseInt(rows_data.length) - parseInt(1);
			nlapiLogExecution('debug', 'rows_data len', rows_data.length);
           // var employeeMappingList = getEmployeeMappingList();

			/*var invoiceRec = nlapiCreateRecord('invoice', { recordmode: 'dynamic', 'entity': 5766 });
            invoiceRec.setFieldValue('job', parseInt(proj_selected));*/
			
			var invoiceRec = nlapiLoadRecord('invoice',305642);
			
            if (location) {
               // invoiceRec.setFieldValue('location', location);
            }

            if (pdfLayout) {
                invoiceRec.setFieldValue('custbody_layout_type', pdfLayout);
            }
			
			bill_from = new Date(bill_from);
			bill_to = new Date(bill_to);
			
			//bill_from = bill_from.toUTCString();
			bill_from = nlapiDateToString(bill_from);
			//bill_to = bill_to.toUTCString();
			bill_to = nlapiDateToString(bill_to);
			
			//nlapiLogExecution('debug', 'bill_from', bill_from);
			//nlapiLogExecution('debug', 'bill_to', bill_to);
				
			var location = 18;
              var pdfLayout = 1;
				
            for (var i = 1; i < total_rows; i++)
			{
				var total_hours = 0;
				var total_checkd_trigger = 0;
				
                var columns = rows_data[i].toString().split(',');;
				//nlapiLogExecution('debug', 'columns len', columns.length);
				
				var timesheet_strt_date_csv = columns[2];
				var timesheet_end_date_csv = columns[3];
				var client_duration = columns[4];
				//nlapiLogExecution('audit','duration by clinet:- ',client_duration);
				
				if(parseInt(client_duration) == 32)
				{
					var d_fromDate = new Date(timesheet_strt_date_csv);
					//d_fromDate = d_fromDate.setHours(0,0,0,0);
					d_fromDate = nlapiDateToString(new Date(d_fromDate));
					//nlapiLogExecution('debug', 'd_fromDate', d_fromDate);
					
					var d_toDate = new Date(timesheet_end_date_csv);
					//d_toDate = d_toDate.setHours(0,0,0,0);
					d_toDate = nlapiDateToString(new Date(d_toDate));
					
					//nlapiLogExecution('debug', 'd_fromDate', d_fromDate);
					//nlapiLogExecution('debug', 'd_toDate', d_toDate);
					
	                var timeLineCount = invoiceRec.getLineItemCount('time');
					//nlapiLogExecution('debug', 'timeLineCount', timeLineCount);
					nlapiLogExecution('audit','strt date:- ',d_fromDate);
	                for (var line = 1; line <= timeLineCount; line++)
					{
	                    invoiceRec.selectLineItem('time', line);
	
	                    var lineDate = invoiceRec.getLineItemValue('time', 'billeddate',line);
	                    //var d_lineDate = nlapiStringToDate(lineDate);
	                    var lineEmployee =invoiceRec.getLineItemValue('time', 'employeedisp',line)
						var emp_hours =invoiceRec.getLineItemValue('time','qty',line);
						
						lineDate = new Date(lineDate);
						//lineDate = lineDate.setHours(0,0,0,0);
						lineDate = nlapiDateToString(new Date(lineDate));
						
	                    if (lineDate >= d_fromDate && lineDate <= d_toDate)
						{
	                        //var line_emp_name = nlapiLookupField('employee',lineEmployee,'entityid');
	                        var s_employee_name_id = lineEmployee.split('-');
							s_employee_name_line = s_employee_name_id[0];
				
							var employee_name_csv = columns[0];
							var s_employee_name_csv = employee_name_csv.split('-');
							s_employee_name_csv = s_employee_name_csv[0];
							
							//var bill_rate_csv = columns[1];
							
							//var billable_hours_csv = columns[4];
							
	                        if(s_employee_name_line == s_employee_name_csv)
							{
								nlapiLogExecution('debug', 's_employee_name_line:- '+s_employee_name_line,'s_employee_name_csv:- '+s_employee_name_csv);
	                          //  nlapiLogExecution('debug', 'apply', lineDate);
							  	//nlapiLogExecution('debug', 'd_lineDate inside emp match', lineDate);
	                            invoiceRec.setCurrentLineItemValue('time', 'apply', 'T');
	                            invoiceRec.commitLineItem('time');
								total_hours = parseFloat(total_hours) + parseFloat(emp_hours);
								total_checkd_trigger++;
								submit_flag = 1;
	                        }
	                            
	                    }
					
						if(parseInt(total_hours) >= parseInt(client_duration))
						{
							break;
						}
                	}
				}	
            }
			
			if(submit_flag == 1)
			{
				var invoiceId = null;
				//nlapiLogExecution('audit','inside invoice submit:-- ');
				invoiceId = nlapiSubmitRecord(invoiceRec, true, true);
            	nlapiLogExecution('audit','invoice id:-- ',invoiceId);
			}
        }
        else {
            result = "No CSV File Provided";
        }
    }
    catch(err)
	{
        nlapiLogExecution('ERROR', 'scheduled', err);
    }
}

function getProjectData(projectName) {
    try {
        var projectCode = (projectName.split(':')[0]).trim();
        var projectSearch = nlapiSearchRecord('job', null, [
            new nlobjSearchFilter('entityid', null, 'is', projectCode),
            new nlobjSearchFilter('isinactive', null, 'is', 'F')
        ], [new nlobjSearchColumn('customer')]);

        if (projectSearch) {
            return {
                ID: projectSearch[0].getId(),
                Customer: projectSearch[0].getValue('customer')
            }
        }
        else {
            throw "Project Not Found";
        }
    }
    catch (err) {
        nlapiLogExecution('ERROR', 'getProjectData', err);
        throw err;
    }
}

function getEmployeeMappingList() {
    try {
        var employeeSearch = searchRecord('employee', null, [
        new nlobjSearchFilter('isinactive', null, 'is', 'F')], [new nlobjSearchColumn('entityid')]);
        var employeeData = {};

        if (employeeSearch) {
            employeeSearch.forEach(function (employee) {
                employeeData[employee.getValue('entityid')] = employee.getId();
            });
        }
        else {
            throw "No Employee Found";
        }

        return employeeData;
    }

    catch (err) {
        nlapiLogExecution('ERROR', 'getEmployeeMappingList', err);
        throw err;
    }
}