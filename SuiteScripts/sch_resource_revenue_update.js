function resoureRevenue() {
	try {
	
		var jobidcol = new Array();
		jobidcol[0]  =new nlobjSearchColumn('internalid');	
		jobidcol[1]  =new nlobjSearchColumn('subsidiary');
	
		var search = nlapiSearchRecord('job', 'customsearch_tm_project_search'
	, null,jobidcol);
    
   
		nlapiLogExecution('AUDIT', 'Info', 'search result'
				        + search.length);
		//for(var id=0;id<search.length;id++)
		for(var id=0;id<search.length;id++)
		{
			var pid=search[id].getValue('internalid');
			var subsidiary=search[id].getValue('subsidiary');
			nlapiLogExecution('AUDIT', 'Info', 'pid#'
					        + pid);
				var rev_cost=0;
			var col=new Array();
			col[0]=new nlobjSearchColumn('resource');
			col[1]=new nlobjSearchColumn('project');
			col[2]=new nlobjSearchColumn('custeventbstartdate');
			col[3]=new nlobjSearchColumn('custeventbenddate');
			col[4]=new nlobjSearchColumn('custevent3');
			col[5]=new nlobjSearchColumn('percentoftime');
		
			var resFilter=new Array();
			resFilter[0]=new nlobjSearchFilter('project',null,'anyOf',pid);
			var s=searchRecord('resourceallocation',null,resFilter
					,col);
			nlapiLogExecution('AUDIT', 'Info', 'rsearch#'
					        + s.length);			
			for(var res_count=0;s!= null && res_count< s.length;res_count++)
			{
				var empid=s[res_count].getValue('resource');
				var new_bill_st_date=s[res_count].getValue('custeventbstartdate');
				var new_bill_end_date=s[res_count].getValue('custeventbenddate');
				var project=s[res_count].getValue('project');
				var bill_rate=s[res_count].getValue('custevent3');
				var percentage_of_time=s[res_count].getValue('percentoftime');
				if(_logValidation(new_bill_st_date)&&_logValidation(new_bill_end_date))
				{
				//	nlapiLogExecution('DEBUG', 'new_bill_st_date', new_bill_st_date);
					var cost=getRevCost(new_bill_st_date,new_bill_end_date,pid,bill_rate,subsidiary,percentage_of_time);
					rev_cost=parseFloat(rev_cost)+parseFloat(cost);
					//nlapiLogExecution('DEBUG', 'bill_type', bill_type);
				}
			
			}
			nlapiSubmitField('job',pid,'custentity_resource_revenue_cost',rev_cost);
		}
	}
	catch (err) {
			nlapiLogExecution('ERROR', 'beforeLoad', err);
		}
	
}

function getWeekend(startDate, endDate) 
{
                          
                var startDate_1 = startDate
                var endDate_1 = endDate
                var weekends=0;
               startDate = nlapiStringToDate(startDate);
               endDate = nlapiStringToDate(endDate);
                 
              
              //  nlapiLogExecution('DEBUG', 'Check Date', 'endDate : ' + endDate);
                
                
            
                var sat = new Array(); //Declaring array for inserting Saturdays
                var sun = new Array(); //Declaring array for inserting Sundays
				while(startDate<endDate)
				{
						
					if(startDate.getDay() == 0 ||startDate.getDay() == 6)
					{
					weekends++;
					}
					 startDate=nlapiAddDays(startDate,1);
				
				}
                       
                return weekends;
}
function checkDateFormat()
{
    var context = nlapiGetContext();

                var dateFormatPref = context.getPreference('dateformat');

    return dateFormatPref;
}
function getDatediffIndays(startDate, endDate)
{
    var one_day=1000*60*60*24;
    var fromDate = startDate;
    var toDate=endDate

                var date1 = nlapiStringToDate(fromDate);

    var date2 = nlapiStringToDate(toDate);

    var date3=Math.round((date2-date1)/one_day);

    return (date3+1);
}
function _logValidation(value) 
{
 if(value!='null' && value != null && value != null && value != '' && value != undefined && value != undefined && value != 'undefined' && value!= 'undefined'&& value!= 'NaN' && value != NaN) 
 {
  return true;
 }
 else 
 { 
  return false;
 }
}
function getRevCost(new_bill_st_date,new_bill_end_date,project,bill_rate,subsidiary,percentage_of_time)
{
	//var pro_rev_cost=pro_fields.custentity_resource_revenue_cost;
	var allocated_days=getDatediffIndays(new_bill_st_date,new_bill_end_date);
	var sun_sat=getWeekend(new_bill_st_date, new_bill_end_date);
	var st_Date = nlapiStringToDate(new_bill_st_date);
    var end_Date = nlapiStringToDate(new_bill_end_date);
	nlapiLogExecution('DEBUG','st_date',st_Date);
	var hol_fil=[];
	hol_fil[0]=new nlobjSearchFilter('custrecordsubsidiary',null,'anyof',subsidiary);
	hol_fil[1]=new nlobjSearchFilter('custrecord_date',null,'within',[st_Date,end_Date]);
	var holiday_search=nlapiSearchRecord('customrecord_holiday',null,hol_fil,null);
	if(_logValidation(holiday_search)){
	var tot_holday=holiday_search.length;
	var total_days=allocated_days-sun_sat-tot_holday;
	}
	else{
	var total_days=allocated_days-sun_sat;
	}
	var percentage_oftime=parseFloat(percentage_of_time)/100;
	var rev_cost=parseFloat(total_days)*parseFloat(bill_rate)*8*parseFloat(percentage_oftime);
		//	nlapiLogExecution('DEBUG', 'tot_holday', tot_holday);
		//	nlapiLogExecution('DEBUG', 'allocated_days', allocated_days);
		//	nlapiLogExecution('DEBUG', 'sun_sat', sun_sat);
		//	nlapiLogExecution('DEBUG', 'tot_days', total_days);
		return rev_cost;	
		//	nlapiSubmitField('job',project,'custentity_resource_revenue_cost',parseFloat(rev_cost));
			
			
}