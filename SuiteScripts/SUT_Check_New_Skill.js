function CheckNewSkill()
{
	try
	{
		var o_context = nlapiGetContext();
		
		var o_form_obj = nlapiCreateForm("Skill Check");
		o_form_obj.setScript('customscript_validate_new_skill');
		response.writePage(o_form_obj);
	}
	
	catch(err)
	{
		nlapiLogExecution('DEBUG','Error in New Skill',err);
		throw err;
	}
}