//BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
	/*
   	Script Name : REST_mainDashboard.js
	Author      : ASHISH PANDIT
	Date        : 14 MARCH 2019
    Description : RESTlet to show main dashboard 


	Script Modification Log:

	-- Date --			-- Modified By --				--Requested By--				-- Description --



Below is a summary of the process controls enforced by this script file.  The control logic is described
more fully, below, in the appropriate function headers and code blocks.


     SCHEDULED FUNCTION
		- scheduledFunction(type)


     SUB-FUNCTIONS
		- The following sub-functions are called by the above core functions in order to maintain code
            modularization:

               - NOT USED

	 */
}
//END SCRIPT DESCRIPTION BLOCK  ====================================


//Main function 
function CreateDashboard(dataIn)
{
	try
	{
		var values = {};
	//	nlapiLogExecution('debug', 'dataIn', JSON.stringify(dataIn));
		var i_userEmail = dataIn.user;
		var i_user = getUserUsingEmailId(i_userEmail);
		/***For testing Region Head Persona 05-08-2019***/
		//if(i_user == 94862)
		//i_user = 148781;
		/*************************************************/
		var adminUser = GetAdmin(i_user);
		var spocSearch = GetPracticeSPOC(i_user);
		var region = GetRegion(i_user);
		var deliveryAnchore = GetDeliveryAnchore(i_user);
		//nlapiLogExecution('AUDIT', 'spocSearch', spocSearch);

		if(adminUser)
		{
			// Create JSON for dashboard table when logged in user in practice SPOC
			var o_headerTileData = GetDashBoardDataPractice(null,null,true,null,null);
			var o_utilisation = getUtilisation(true,null,null);
			var o_projectData = GetProjectDetails(null,null,true,null,null);
			var o_opportunityData = GetOpportunityDetails(null,null,true,null,null);
			var o_pipeLineProData = GetpipeLineProjectDetails(null,null,true,null,null);
			//nlapiLogExecution('AUDIT', 'o_headerTileData adminUser', o_headerTileData);

			values.HeaderTileData = o_headerTileData;
			values.utilisation = o_utilisation;
			values.projectData = o_projectData;
			values.opportunityData = o_opportunityData;
			values.pipelineProjectData = o_pipeLineProData;
		}
		else if(spocSearch.length>0) 
		{
			//nlapiLogExecution('AUDIT', 'In spoc', i_practice);
			var i_practice = spocSearch;//[0].getValue('custrecord_practice');
			//nlapiLogExecution('AUDIT', 'i_practice', i_practice);
			var a_subPractices = getSubPractices(i_practice);
			// Create JSON for dashboard table when logged in user in practice SPOC
			var o_headerTileData = GetDashBoardDataPractice(null,a_subPractices,null,null,null);
			var o_utilisation = getUtilisation(null,a_subPractices,null);
			var o_projectData = GetProjectDetails(null,a_subPractices,null,null,null);
			var o_opportunityData = GetOpportunityDetails(null,a_subPractices,null,null,null);
			var o_pipeLineProData = GetpipeLineProjectDetails(null,a_subPractices,null,null,null);

			values.HeaderTileData = o_headerTileData;
			values.utilisation = o_utilisation;
			values.projectData = o_projectData;
			values.opportunityData = o_opportunityData;
			values.pipelineProjectData = o_pipeLineProData;
		}
		else if(region)
		{
			// Create JSON for dashboard table when logged in user in practice SPOC
			var o_headerTileData = GetDashBoardDataPractice(null,null,null,region,null);
			var o_utilisation = getUtilisation(true,null,null);
			var o_projectData = GetProjectDetails(null,null,null,region,null);
			var o_opportunityData = GetOpportunityDetails(null,null,null,region,null);
			var o_pipeLineProData = GetpipeLineProjectDetails(null,null,null,region,null);

			values.HeaderTileData = o_headerTileData;
			values.utilisation = o_utilisation;
			values.projectData = o_projectData;
			values.opportunityData = o_opportunityData;
			values.pipelineProjectData = o_pipeLineProData;
		}else if(deliveryAnchore.length>0)
		{
			// Create JSON for dashboard table when logged in user in practice SPOC
			var o_headerTileData = GetDashBoardDataPractice(null,null,null,null,deliveryAnchore);
			var o_utilisation = getUtilisation(true,null,null);
			var o_projectData = GetProjectDetails(null,null,null,null,deliveryAnchore);
			var o_opportunityData = GetOpportunityDetails(null,null,null,null,deliveryAnchore);
			var o_pipeLineProData = GetpipeLineProjectDetails(null,null,null,null,deliveryAnchore);

			values.HeaderTileData = o_headerTileData;
			values.utilisation = o_utilisation;
			values.projectData = o_projectData;
			values.opportunityData = o_opportunityData;
			values.pipelineProjectData = o_pipeLineProData;
		}
		else if(i_user)
		{

			var o_headerTileData = GetDashBoardDataPractice(i_user,null,null,null,null);
			var o_utilisation = getUtilisation(null,null,i_user);
			var o_projectData = GetProjectDetails(i_user,null,null,null,null,null);
			var o_opportunityData = GetOpportunityDetails(i_user,null,null,null,null);
			var o_pipeLineProData = GetpipeLineProjectDetails(i_user,null,null,null,null);

			values.HeaderTileData = o_headerTileData;
			values.utilisation = o_utilisation;
			values.projectData = o_projectData;
			values.opportunityData = o_opportunityData;
			values.pipelineProjectData = o_pipeLineProData;
		}
		//nlapiLogExecution('AUDIT','values ',JSON.Stringify(values));
		return values;
	}
	catch(exc)
	{
		nlapiLogExecution('Debug','Exception ',exc);
		return {
			"code": "Error",
			"message": exc
		}
	}

}

function GetAdmin(user) 
{
	//nlapiLogExecution('AUDIT', 'user GetAdmin ', user);
	var adminSearch = nlapiSearchRecord("customrecord_fuel_leadership",null,
			[
				["custrecord_fuel_leadership_employee","anyof",user],
				"AND",
				["isinactive","is","F"]
				], 
				[
					new nlobjSearchColumn("custrecord_fuel_leadership_employee")
					]
	);
	return adminSearch;
}
function GetPracticeSPOC(user) 
{
	var practiceArray = new Array();
	//nlapiLogExecution('AUDIT', 'user', user);
	var spocSearch = nlapiSearchRecord("customrecord_practice_spoc",null,
			[
				["custrecord_spoc","anyof",user],
				"AND",
				["isinactive","is","F"]
				], 
				[
					new nlobjSearchColumn("custrecord_practice")
					]
	);
	if(spocSearch)
	{
		for(var i=0; i<spocSearch.length; i++)
		{
			practiceArray.push(spocSearch[i].getValue('custrecord_practice'));
		}
	}
	return practiceArray;
}
var onHold = GetOnholdStatusOfAllRRF();
var reduceOnHoldID =  meargeDate(onHold);
nlapiLogExecution('debug', 'reduceOnHoldID is',JSON.stringify(reduceOnHoldID));
function GetDashBoardDataPractice(i_user, practiceId, isAdmin,i_region,deliveryAnchore)
{
	try
	{
		//nlapiLogExecution('Debug','In Function getFrfDetailspracticeId ',practiceId);
		var filters = new Array();
		var columns = new Array();
		if(isAdmin == true)
		{
			filters = [["custrecord_frf_details_status","is","F"],
				"AND",
				["isinactive","is","F"],
				"AND",
				["custrecord_frf_details_opp_id.custrecord_stage_sfdc","noneof","9","8"]] 
		}
		else if(practiceId)
		{
			filters = [
				/*["custrecord_frf_details_opp_id.custrecord_practice_internal_id_sfdc","anyof",practiceId],
				   "OR",
				   ["custrecord_frf_details_project.custentity_practice","anyof",practiceId],
				   "OR",*/
				["custrecord_frf_details_res_practice","anyof",practiceId],
				"AND",
				["isinactive","is","F"],
				"AND",
				["custrecord_frf_details_status","is","F"],
				"AND",
				["custrecord_frf_details_opp_id.custrecord_stage_sfdc","noneof","9","8"]]

		}
		else if(i_region)
		{

			filters = [
				//["custrecord_frf_details_region","anyof",i_region],
				["custrecord_frf_details_account.custentity_region","anyof",i_region],
				 "AND",
				 [["isinactive","is","F"],
					"OR",
					["custrecord_frf_details_opp_id.custrecord_status_sfdc","anyof","1"]],
					"AND",
					["custrecord_frf_details_status","is","F"],
					"AND",
					["custrecord_frf_details_opp_id.custrecord_stage_sfdc","noneof","9","8"]]
		}
		else if(deliveryAnchore)
		{

			filters = [
				//["custrecord_frf_details_region","anyof",i_region],
				["custrecord_frf_details_account","anyof",deliveryAnchore],
				 "AND",
				 [["isinactive","is","F"],
					"OR",
					["custrecord_frf_details_opp_id.custrecord_status_sfdc","anyof","1"]],
					"AND",
					["custrecord_frf_details_status","is","F"],
					"AND",
					["custrecord_frf_details_opp_id.custrecord_stage_sfdc","noneof","9","8"]]
		}
		else if(i_user)
		{
			filters = [
				[[["custrecord_frf_details_project.custentity_projectmanager","anyof",i_user],
					"OR",
					["custrecord_frf_details_project.custentity_deliverymanager","anyof",i_user],
					"OR",
					["custrecord_frf_details_project.custentity_clientpartner","anyof",i_user]],
					"AND",
					["custrecord_frf_details_opp_id","anyof","@NONE@"],
					"AND",
					["custrecord_frf_details_status","is","F"],
					"AND",
					["custrecord_frf_details_opp_id.custrecord_stage_sfdc","noneof","9","8"]],
					"OR",
					[[["custrecord_frf_details_opp_id.custrecord_sfdc_opp_pm","anyof",i_user],
						"OR",
						["custrecord_frf_details_opp_id.custrecord_sfdc_opportunity_dm","anyof",i_user],
						"OR",
						["custrecord_frf_details_opp_id.custrecord_sfdc_client_partner","anyof",i_user]],
						"AND",
						["custrecord_frf_details_project","anyof","@NONE@"],
						"AND",
						["custrecord_frf_details_status","is","F"],
						"AND",
						["custrecord_frf_details_opp_id.custrecord_stage_sfdc","noneof","9","8"]]
				]
		}

		columns = [
			new nlobjSearchColumn("custrecord_frf_details_opp_id"), 
			new nlobjSearchColumn("custrecord_frf_details_res_location"), 
			new nlobjSearchColumn("custrecord_frf_details_res_practice"), 
			//new nlobjSearchColumn("custrecord_frf_details_emp_level"), 
			new nlobjSearchColumn("custrecord_frf_details_external_hire"), 
			new nlobjSearchColumn("custrecord_frf_details_account"), 
			new nlobjSearchColumn("custrecord_frf_details_role"), 
			new nlobjSearchColumn("custrecord_frf_details_bill_rate"), 
			new nlobjSearchColumn("custrecord_frf_details_critical_role"), 
			//new nlobjSearchColumn("custrecord_frf_details_skill_family"), 
			new nlobjSearchColumn("custrecord_frf_details_start_date"), 
			new nlobjSearchColumn("custrecord_frf_details_end_date"), 
			new nlobjSearchColumn("custrecord_frf_details_selected_emp"), 
			//new nlobjSearchColumn("custrecord_frf_details_internal_fulfill"), 
			new nlobjSearchColumn("custrecord_frf_details_primary_skills"), 
			new nlobjSearchColumn("custrecord_frf_details_frf_number"), 
			new nlobjSearchColumn("custrecord_frf_details_project"), 
			new nlobjSearchColumn("custrecord_frf_type"), 
			new nlobjSearchColumn("custrecord_frf_emp_notice_rotation"), 
			new nlobjSearchColumn("custrecord_frf_details_billiable"), 
			new nlobjSearchColumn("custrecord_frf_details_source"), 
			new nlobjSearchColumn("custrecord_frf_details_created_by"), 
			new nlobjSearchColumn("custrecord_frf_details_parent"), 
			new nlobjSearchColumn("custrecord_frf_details_status_flag"),
			new nlobjSearchColumn("custrecord_frf_details_allocated_date"),
			new nlobjSearchColumn("custrecord_frf_details_availabledate"),
			new nlobjSearchColumn("custrecord_frf_details_sales_activity"),
			new nlobjSearchColumn("custentity_region","CUSTRECORD_FRF_DETAILS_ACCOUNT",null), 
			new nlobjSearchColumn("custrecord_opportunity_name_sfdc","custrecord_frf_details_opp_id",null), 
			new nlobjSearchColumn("custrecord_projection_status_sfdc","custrecord_frf_details_opp_id",null), // Nihal
			new nlobjSearchColumn("custrecord_taleo_ext_hire_status","CUSTRECORD_FRF_DETAILS_RRF_NUMBER",null),
     	     new nlobjSearchColumn("CUSTRECORD_FRF_DETAILS_RRF_NUMBER"),// Nihal
			new nlobjSearchColumn("custrecord_frf_type",null), // Nihal
			new nlobjSearchColumn("custentity_region","CUSTRECORD_FRF_DETAILS_PROJECT",null),
			new nlobjSearchColumn("type","CUSTRECORD_FRF_DETAILS_PROJECT",null),
			new nlobjSearchColumn("custrecord_frf_details_fitment_type"), //prabhat gupta 04/09/2020 NIS-1723
			new nlobjSearchColumn("custrecord_frf_details_level") //prabhat gupta 07/10/2020 NIS-1755
			]
		var customrecord_frf_detailsSearch = searchRecord("customrecord_frf_details", null, filters, columns,null)	
		if(customrecord_frf_detailsSearch) 
		{
			var dataArray = new Array();
			//nlapiLogExecution('Debug','customrecord_frf_detailsSearch Length ',customrecord_frf_detailsSearch.length);
			for(var i = 0; i<customrecord_frf_detailsSearch.length; i++)
			{
				var data = {};
				data.frfstatus = {"name":customrecord_frf_detailsSearch[i].getText("custrecord_frf_details_status_flag"),"id":customrecord_frf_detailsSearch[i].getValue("custrecord_frf_details_status_flag")};
				if(customrecord_frf_detailsSearch[i].getText("custrecord_frf_details_project")){
					data.project = {"name":customrecord_frf_detailsSearch[i].getText("custrecord_frf_details_project"),"id":customrecord_frf_detailsSearch[i].getValue("custrecord_frf_details_project")};
					data.projectType = {"name": customrecord_frf_detailsSearch[i].getText("type","custrecord_frf_details_project")};
				}
				else 
				{
					data.project = {"name":customrecord_frf_detailsSearch[i].getValue("custrecord_opportunity_name_sfdc","custrecord_frf_details_opp_id",null),"id":customrecord_frf_detailsSearch[i].getValue("custrecord_frf_details_opp_id")};
					data.projectType = {"name": "External"};
				}
				data.account = {"name":customrecord_frf_detailsSearch[i].getText("custrecord_frf_details_account"),"id":customrecord_frf_detailsSearch[i].getValue("custrecord_frf_details_account")};
				data.frfnumber = {"name":customrecord_frf_detailsSearch[i].getValue("custrecord_frf_details_frf_number")};
				data.practice = {"name":customrecord_frf_detailsSearch[i].getText("custrecord_frf_details_res_practice"),"id":customrecord_frf_detailsSearch[i].getValue("custrecord_frf_details_res_practice")};
				data.location = {"name":customrecord_frf_detailsSearch[i].getText("custrecord_frf_details_res_location"),"id":customrecord_frf_detailsSearch[i].getValue("custrecord_frf_details_res_location")};
				data.startDate = {"name":customrecord_frf_detailsSearch[i].getValue("custrecord_frf_details_start_date")};
				data.availabledate = {"name":customrecord_frf_detailsSearch[i].getValue("custrecord_frf_details_availabledate")};
				data.allocationdate = {"name":customrecord_frf_detailsSearch[i].getValue("custrecord_frf_details_allocated_date")};
				data.salesactivity = {"name":customrecord_frf_detailsSearch[i].getValue("custrecord_frf_details_sales_activity")};
				data.endDate = {"name":customrecord_frf_detailsSearch[i].getValue("custrecord_frf_details_end_date")};
				data.billrate = {"name":customrecord_frf_detailsSearch[i].getValue("custrecord_frf_details_bill_rate")};
				data.softlockresource = {"name":customrecord_frf_detailsSearch[i].getText("custrecord_frf_details_selected_emp"),"id":customrecord_frf_detailsSearch[i].getValue("custrecord_frf_details_selected_emp")};

				if(customrecord_frf_detailsSearch[i].getValue("custrecord_frf_details_billiable")=='T')
				{
					data.billable =  {"name":true};
				}
				else
				{
					data.billable =  {"name":false};
				}
				if(customrecord_frf_detailsSearch[i].getValue("custrecord_frf_details_external_hire")=='T')
				{
					data.externalhire =  {"name":true};
				}
				else
				{
					data.externalhire =  {"name":false};
				}
				if(customrecord_frf_detailsSearch[i].getValue("custentity_region","CUSTRECORD_FRF_DETAILS_ACCOUNT",null))
				{
					data.region = {"name":customrecord_frf_detailsSearch[i].getText("custentity_region","CUSTRECORD_FRF_DETAILS_ACCOUNT",null),"id":customrecord_frf_detailsSearch[i].getValue("custentity_region","CUSTRECORD_FRF_DETAILS_ACCOUNT",null)};
				}
				else if(customrecord_frf_detailsSearch[i].getValue("custentity_region","CUSTRECORD_FRF_DETAILS_PROJECT",null))
				{
					data.region = {"name":customrecord_frf_detailsSearch[i].getText("custentity_region","CUSTRECORD_FRF_DETAILS_PROJECT",null),"id":customrecord_frf_detailsSearch[i].getValue("custentity_region","CUSTRECORD_FRF_DETAILS_PROJECT",null)};
				}
				else
				{
					data.region = {"name":"","id":""};
				}
				
				data.type = {"name":customrecord_frf_detailsSearch[i].getText("custrecord_frf_type"),"id":customrecord_frf_detailsSearch[i].getValue("custrecord_frf_type")};
				
		//--------------------------------------------------------------------------------------------------------
			
			//prabhat gupta 08/09/2020 NIS-1723
			
			data.fitmentType = {"name":customrecord_frf_detailsSearch[i].getText("custrecord_frf_details_fitment_type"),"id":customrecord_frf_detailsSearch[i].getValue("custrecord_frf_details_fitment_type")};
			
			
	  //---------------------------------------------------------------------------------------------------------
	  
	  //--------------------------------------------------------------------------------------------------------
			
			//prabhat gupta 07/10/2020 NIS-1755
			
			data.level = {"name":customrecord_frf_detailsSearch[i].getText("custrecord_frf_details_level"),"id":customrecord_frf_detailsSearch[i].getValue("custrecord_frf_details_level")};
			
			
	  //---------------------------------------------------------------------------------------------------------
				
				if(customrecord_frf_detailsSearch[i].getValue("CUSTRECORD_FRF_DETAILS_RRF_NUMBER")){
					data.taleoStatus = customrecord_frf_detailsSearch[i].getValue("custrecord_taleo_ext_hire_status","CUSTRECORD_FRF_DETAILS_RRF_NUMBER",null);
					var rrfNumber = customrecord_frf_detailsSearch[i].getValue("CUSTRECORD_FRF_DETAILS_RRF_NUMBER");
					data.OnHoldDetails = reduceOnHoldID[rrfNumber]?reduceOnHoldID[rrfNumber]:"";
					
				}
				else
					{
					 data.taleoStatus = "";
                      data.OnHoldDetails ="";
					}
				if(customrecord_frf_detailsSearch[i].getValue("custrecord_frf_details_opp_id")){
					data.projectionstatus  = {"name":customrecord_frf_detailsSearch[i].getText("custrecord_projection_status_sfdc","custrecord_frf_details_opp_id",null),"id":customrecord_frf_detailsSearch[i].getValue("custrecord_projection_status_sfdc","custrecord_frf_details_opp_id",null)};
				}
				else
					{
						data.projectionstatus = {"name":"Booked","id": "3"};
					}
				dataArray.push(data);
			}

			return dataArray;
		}
		else
		{
			var dataArray = [];
			return dataArray;
		}
	}
	catch(error)
	{
		return error;
	}
}
function GetOnholdStatusOfAllRRF()
{
    // prabhat gupta 27/8/2020 replaced nlapiSearchRecord with custom function searchRecord() because it's throwing error
	var customrecord_taleo_external_hireSearch = searchRecord("customrecord_taleo_external_hire",null,
[
   [["systemnotes.newvalue","is","On hold"],"OR",["systemnotes.oldvalue","is","On hold"]], 
   "AND", 
   ["isinactive","is","F"]
], 
[
   new nlobjSearchColumn("oldvalue","systemNotes",null), 
   new nlobjSearchColumn("newvalue","systemNotes",null), 
   new nlobjSearchColumn("date","systemNotes",null)
]
);
/*
//prabhat gupta 27/8/2020 commented this code because of production issue
var completeResultSet = customrecord_taleo_external_hireSearch;
	
		while(customrecord_taleo_external_hireSearch.length == 1000)
		{
			var lastId = customrecord_taleo_external_hireSearch[999].getId();
			filter.push(new nlobjSearchFilter('internalidnumber',null,'greaterthan',lastId));
			customrecord_taleo_external_hireSearch = nlapiSearchRecord("customrecord_frf_details",null,filter,column);
			completeResultSet = completeResultSet.concat(customrecord_taleo_external_hireSearch); 
		} 
		customrecord_taleo_external_hireSearch = completeResultSet;
	*/	

	var returnData = [];
	if(customrecord_taleo_external_hireSearch){
		for(var t = 0;t<customrecord_taleo_external_hireSearch.length;t++)
		{
			returnData.push({
				"internalId": customrecord_taleo_external_hireSearch[t].getId(),
				"oldStatus":customrecord_taleo_external_hireSearch[t].getValue("oldvalue","systemNotes",null),
				"newStatus":customrecord_taleo_external_hireSearch[t].getValue("newvalue","systemNotes",null),
				"Date":customrecord_taleo_external_hireSearch[t].getValue("date","systemNotes",null)
			});
		}
	}
	return returnData;
	
}
function meargeDate(all_data)
{
return groupResult = all_data.reduce(function (r, a) {
		r[a.internalId] = r[a.internalId] || [];
		r[a.internalId].push(a);
		return r;
	}, Object.create(null));
}
/****Function to get Project details****/
function GetProjectDetails(i_user,i_practice,isAdmin,i_region,deliveryAnchore)
{
	try
	{
	//	nlapiLogExecution('audit','i_region ',i_region);
	//	nlapiLogExecution('audit','i_practice34534 ',i_practice);

		var filters = new Array();
		var columns = new Array();
		if(isAdmin == true)
		{
			filters = [[["custrecord_fulfill_dashboard_opp_id.custrecord_projection_status_sfdc","anyof","3"],
				"AND",
				["isinactive","is","F"],
				"AND",
				["custrecord_fulfill_dashboard_opp_id.custrecord_project_internal_id_sfdc","noneof","@NONE@"]], 
				"OR", 
				[["isinactive","is","F"],
					"AND",
					["custrecord_fulfill_dashboard_project","noneof","@NONE@"],
					"AND",
					["custrecord_fulfill_dashboard_opp_id","anyof","@NONE@"],
					"AND",
					["custrecord_fulfill_dashboard_project.status","noneof","1"]]]

			/*filters = [
				["isinactive","is","F"],
				 "AND", 
				[["custrecord_fulfill_dashboard_project","noneof","@NONE@"],
				"AND",
				["custrecord_fulfill_dashboard_project.status","noneof","1"],
				"AND",
				["custrecord_fulfill_dashboard_opp_id","anyof","@NONE@"]],
				"OR",
				[["custrecord_fulfill_dashboard_opp_id.custrecord_projection_status_sfdc","anyof","3"],
				"AND",
				["custrecord_fulfill_dashboard_opp_id.custrecord_project_internal_id_sfdc","noneof","@NONE@"]]]*/
		}
		else if(i_practice)
		{

			filters = [
				["custrecord_fulfill_dashboard_project.custentity_practice","anyof",i_practice],
				"AND",
				["isinactive","is","F"],
				"AND", 
				[["custrecord_fulfill_dashboard_project","noneof","@NONE@"],
					"AND",
					["custrecord_fulfill_dashboard_project.status","noneof","1"],
					"AND",
					["custrecord_fulfill_dashboard_opp_id","anyof","@NONE@"]],
					"OR",
					[["custrecord_fulfill_dashboard_opp_id.custrecord_projection_status_sfdc","anyof","3"],
						"AND",
						["custrecord_fulfill_dashboard_opp_id.custrecord_project_internal_id_sfdc","noneof","@NONE@"]]]

		}
		else if(i_region)
		{

			 filters = [
				["custrecord_fulfill_dashboard_project.custentity_region","anyof",i_region],
				"AND",
				 ["isinactive","is","F"],
				"AND",
				[["custrecord_fulfill_dashboard_project.status","noneof","1"],
					"AND",
					["custrecord_fulfill_dashboard_opp_id.custrecord_status_sfdc","is","1"],
					"AND",
					["custrecord_fulfill_dashboard_opp_id","anyof","@NONE@"]],
					"OR",
					[["custrecord_fulfill_dashboard_opp_id.custrecord_projection_status_sfdc","anyof","3"],
						"AND",
						["custrecord_fulfill_dashboard_opp_id.custrecord_project_internal_id_sfdc","noneof","@NONE@"]]]
		}
		else if(deliveryAnchore)
		{
				filters = [[
						["custrecord_fulfill_dashboard_opp_id.custrecord_projection_status_sfdc","anyof","3"],
						"AND",
						["custrecord_fulfill_dashboard_account","anyof",deliveryAnchore],
						"AND",
						["isinactive","is","F"],
						"AND",
						["custrecord_fulfill_dashboard_opp_id.custrecord_project_internal_id_sfdc","noneof","@NONE@"]], 
						"OR", 
						[["isinactive","is","F"],
						"AND",
						["custrecord_fulfill_dashboard_account","anyof",deliveryAnchore],
						"AND",
						["custrecord_fulfill_dashboard_project","noneof","@NONE@"],
						"AND",
						["custrecord_fulfill_dashboard_opp_id","anyof","@NONE@"],
						"AND",
						["custrecord_fulfill_dashboard_project.status","noneof","1"]]]
			
			
			
			
			 /*filters = [
				["custrecord_fulfill_dashboard_account","anyof",deliveryAnchore],
				"AND",
				 ["isinactive","is","F"],
				"AND",
				[["custrecord_fulfill_dashboard_project.status","noneof","1"],
					"AND",
					["custrecord_fulfill_dashboard_opp_id.custrecord_status_sfdc","is","1"],
					"AND",
					["custrecord_fulfill_dashboard_opp_id","anyof","@NONE@"]],
					"OR",
					[["custrecord_fulfill_dashboard_opp_id.custrecord_projection_status_sfdc","anyof","3"],
						"AND",
						["custrecord_fulfill_dashboard_opp_id.custrecord_project_internal_id_sfdc","noneof","@NONE@"]]]*/
		}
		else if(i_user)
		{
			filters = [[
				["custrecord_fulfill_dashboard_project.custentity_projectmanager","anyof",i_user],
				"OR",
				["custrecord_fulfill_dashboard_project.custentity_deliverymanager","anyof",i_user],
				"OR",
				["custrecord_fulfill_dashboard_project.custentity_clientpartner","anyof",i_user]],
				"AND",
				["isinactive","is","F"],
				"AND", 
				[["custrecord_fulfill_dashboard_project","noneof","@NONE@"],
					"AND",
					["custrecord_fulfill_dashboard_project.status","noneof","1"],
					"AND",
					["custrecord_fulfill_dashboard_opp_id","anyof","@NONE@"]],
					"OR",
					[["custrecord_fulfill_dashboard_opp_id.custrecord_projection_status_sfdc","anyof","3"],
						"AND",
						["custrecord_fulfill_dashboard_opp_id.custrecord_project_internal_id_sfdc","noneof","@NONE@"]]]
		}
	//	nlapiLogExecution('audit','filters GetProjectDetails',filters);

		columns = [
			// new nlobjSearchColumn("custrecord_fulfill_dashboard_status"), 
			new nlobjSearchColumn("custrecord_fulfill_dashboard_project"), 
			new nlobjSearchColumn("custrecord_fulfill_dashboard_account"), 
			new nlobjSearchColumn("custrecord_fulfill_dashboard_practice"), 
			new nlobjSearchColumn("custrecord_fulfill_dashboard_start_date"),
			new nlobjSearchColumn("custrecord_fulfill_dashboard_end_date"),
			new nlobjSearchColumn("custrecord_fulfill_dashboard_opp_id"),
			new nlobjSearchColumn("custrecord_fulfill_dashboard_risk"),
			new nlobjSearchColumn("custrecord_fulfill_dashboard_rev_sta_pro"),
			new nlobjSearchColumn("custrecord_opp_fulfilment_status_sfdc","CUSTRECORD_FULFILL_DASHBOARD_OPP_ID",null), 
			new nlobjSearchColumn("custentity_region","CUSTRECORD_FULFILL_DASHBOARD_PROJECT",null),
			new nlobjSearchColumn("custrecord_projection_status_sfdc","CUSTRECORD_FULFILL_DASHBOARD_OPP_ID",null),
			new nlobjSearchColumn("custrecord_project_internal_id_sfdc","CUSTRECORD_FULFILL_DASHBOARD_OPP_ID",null),
			new nlobjSearchColumn("custrecord_status_sfdc","CUSTRECORD_FULFILL_DASHBOARD_OPP_ID",null),
			new nlobjSearchColumn("custentity_practice","custrecord_fulfill_dashboard_project",null),
			new nlobjSearchColumn("entityid","custrecord_fulfill_dashboard_project",null),
			new nlobjSearchColumn("custrecord_practice_internal_id_sfdc","CUSTRECORD_FULFILL_DASHBOARD_OPP_ID",null)
			]
			
		var customrecord_fulfillment_dashboard_dataSearch = searchRecord("customrecord_fulfillment_dashboard_data",null, filters,columns,null);
		
		if(customrecord_fulfillment_dashboard_dataSearch)
		{
		//	nlapiLogExecution('debug','customrecord_fulfillment_dashboard_dataSearch length %%% ',customrecord_fulfillment_dashboard_dataSearch.length);
			var dataArray = new Array();
			for(var i=0; i<customrecord_fulfillment_dashboard_dataSearch.length;i++)
			{
				var data = {};
				data.project = {"name":customrecord_fulfillment_dashboard_dataSearch[i].getText("custrecord_fulfill_dashboard_project"),"id":customrecord_fulfillment_dashboard_dataSearch[i].getValue("custrecord_fulfill_dashboard_project")};
				data.account = {"name":customrecord_fulfillment_dashboard_dataSearch[i].getText("custrecord_fulfill_dashboard_account"),"id":customrecord_fulfillment_dashboard_dataSearch[i].getValue("custrecord_fulfill_dashboard_account")};
				data.projectID = {"name":customrecord_fulfillment_dashboard_dataSearch[i].getValue("entityid","custrecord_fulfill_dashboard_project",null)};

				//data.account = {"name":customrecord_fulfillment_dashboard_dataSearch[i].getValue("custrecord_fulfill_dashboard_account")};
				if(customrecord_fulfillment_dashboard_dataSearch[i].getValue('custrecord_fulfill_dashboard_project'))
					data.practice = {"name":customrecord_fulfillment_dashboard_dataSearch[i].getText("custentity_practice","custrecord_fulfill_dashboard_project",null),"id":customrecord_fulfillment_dashboard_dataSearch[i].getValue("custentity_practice","custrecord_fulfill_dashboard_project",null)};
				else
					data.practice = {"name":customrecord_fulfillment_dashboard_dataSearch[i].getText("custrecord_practice_internal_id_sfdc","CUSTRECORD_FULFILL_DASHBOARD_OPP_ID",null),"id":customrecord_fulfillment_dashboard_dataSearch[i].getValue("custrecord_practice_internal_id_sfdc","CUSTRECORD_FULFILL_DASHBOARD_OPP_ID",null)};

				data.startDate = {"name":customrecord_fulfillment_dashboard_dataSearch[i].getValue("custrecord_fulfill_dashboard_start_date")};
				data.endDate = {"name":customrecord_fulfillment_dashboard_dataSearch[i].getValue("custrecord_fulfill_dashboard_end_date")};
				data.projectionstatus = {"name":customrecord_fulfillment_dashboard_dataSearch[i].getValue("custrecord_fulfill_dashboard_rev_sta_pro")};
				data.oppStatus = {"name":customrecord_fulfillment_dashboard_dataSearch[i].getValue("custrecord_opp_fulfilment_status_sfdc","CUSTRECORD_FULFILL_DASHBOARD_OPP_ID",null)};
				if(customrecord_fulfillment_dashboard_dataSearch[i].getValue("custentity_region","CUSTRECORD_FULFILL_DASHBOARD_PROJECT",null))
				{
					data.region = {"name":customrecord_fulfillment_dashboard_dataSearch[i].getText("custentity_region","CUSTRECORD_FULFILL_DASHBOARD_PROJECT",null),"id":customrecord_fulfillment_dashboard_dataSearch[i].getValue("custentity_region","CUSTRECORD_FULFILL_DASHBOARD_PROJECT",null)};
				}
				else 
				{
					data.region = {"name":"","id":""};
				}
				//data.region = {"name":customrecord_fulfillment_dashboard_dataSearch[i].getText("custentity_region","CUSTRECORD_FULFILL_DASHBOARD_PROJECT",null),"id":customrecord_fulfillment_dashboard_dataSearch[i].getValue("custentity_region","CUSTRECORD_FULFILL_DASHBOARD_PROJECT",null)};
				data.salesactivity = {"name":customrecord_fulfillment_dashboard_dataSearch[i].getText("custrecord_projection_status_sfdc","CUSTRECORD_FULFILL_DASHBOARD_OPP_ID",null)};
				data.frfstatus = {"name":customrecord_fulfillment_dashboard_dataSearch[i].getText("custrecord_status_sfdc","CUSTRECORD_FULFILL_DASHBOARD_OPP_ID",null),"id":customrecord_fulfillment_dashboard_dataSearch[i].getValue("custrecord_status_sfdc","CUSTRECORD_FULFILL_DASHBOARD_OPP_ID",null)};
				data.risktotal = {"name":customrecord_fulfillment_dashboard_dataSearch[i].getValue("custrecord_fulfill_dashboard_risk")};
				dataArray.push(data);
				
				var i_project = customrecord_fulfillment_dashboard_dataSearch[i].getValue("custrecord_project_internal_id_sfdc","CUSTRECORD_FULFILL_DASHBOARD_OPP_ID",null);
				var i_opportunity = customrecord_fulfillment_dashboard_dataSearch[i].getValue('custrecord_fulfill_dashboard_opp_id');
				var s_projStatus = customrecord_fulfillment_dashboard_dataSearch[i].getText("custrecord_projection_status_sfdc","CUSTRECORD_FULFILL_DASHBOARD_OPP_ID",null);
				//nlapiLogExecution('debug','i_project '+i_project,'i_opportunity '+i_opportunity);
				//nlapiLogExecution('debug','s_projStatus ',s_projStatus);

				if(_logValidation(i_project) && s_projStatus == "Booked" && _logValidation(i_opportunity))
				{
					dataArray.pop();
				}
			}
		//	nlapiLogExecution('Debug','o_projectData Project Details ',JSON.stringify(dataArray));
			return dataArray;

		}
		else
		{
			var dataArray = [];
			return dataArray;
		}
	}
	catch(error)
	{
		return error;
	}
}

/****Function to get Opportunity details****/
function GetOpportunityDetails(i_user,i_practice,isAdmin,i_region,deliveryAnchore)
{
	try
	{
		//nlapiLogExecution('Debug','o_projectData ',i_user);
		var filters = [];
		if(isAdmin==true)
		{
			filters = [[["isinactive","is","F"],
				"AND", 
				["custrecord_fulfill_dashboard_opp_id.custrecord_stage_sfdc","noneof","9","8"],
				"AND", 
				["custrecord_fulfill_dashboard_opp_id.custrecord_projection_status_sfdc","anyof","2"]],
				"OR",
				[["custrecord_fulfill_dashboard_opp_id.custrecord_projection_status_sfdc","anyof","3"],
					"AND",
					["custrecord_fulfill_dashboard_project","anyof","@NONE@"],
					"AND",
					["isinactive","is","F"],
					"AND", 
					["custrecord_fulfill_dashboard_opp_id.custrecord_stage_sfdc","noneof","9","8"]]] 
		}
		else if(i_practice)
		{
			filters = [[[
				["custrecord_fulfill_dashboard_opp_id.custrecord_practice_internal_id_sfdc","anyof",i_practice]],
				"AND",
				["isinactive","is","F"],
				"AND", 
				["custrecord_fulfill_dashboard_opp_id.custrecord_stage_sfdc","noneof","9","8"],
				"AND", 
				["custrecord_fulfill_dashboard_opp_id.custrecord_projection_status_sfdc","anyof","2"]],
				"OR",
				[["custrecord_fulfill_dashboard_opp_id.custrecord_projection_status_sfdc","anyof","3"],
					"AND",
					["custrecord_fulfill_dashboard_project","anyof","@NONE@"],
					"AND",
					["custrecord_fulfill_dashboard_opp_id.custrecord_practice_internal_id_sfdc","anyof",i_practice],
					"AND",
					["isinactive","is","F"],
					"AND", 
					["custrecord_fulfill_dashboard_opp_id.custrecord_stage_sfdc","noneof","9","8"]]]
		}
		else if(i_region)
		{

			 filters = [[
				["custrecord_fulfill_dashboard_opp_id.custrecord_sfdc_opp_account_region","anyof",i_region],
				"AND",
				 ["isinactive","is","F"],
				"AND",
				["custrecord_fulfill_dashboard_opp_id.custrecord_stage_sfdc","noneof","9","8"],
				"AND", 
				["custrecord_fulfill_dashboard_opp_id.custrecord_projection_status_sfdc","anyof","2"]],
				"OR",
				[["custrecord_fulfill_dashboard_opp_id.custrecord_projection_status_sfdc","anyof","3"],
					"AND",
					["custrecord_fulfill_dashboard_project","anyof","@NONE@"],
					"AND",
					["custrecord_fulfill_dashboard_opp_id.custrecord_sfdc_opp_account_region","anyof",i_region],
					"AND",
					 ["isinactive","is","F"],
					"AND",
					["custrecord_fulfill_dashboard_opp_id.custrecord_stage_sfdc","noneof","9","8"]]]
		}
		else if(deliveryAnchore)
		{

			 filters = [
				["custrecord_fulfill_dashboard_account","anyof",deliveryAnchore],
				"AND",
				 ["isinactive","is","F"],
				"AND",
				["custrecord_fulfill_dashboard_opp_id.custrecord_stage_sfdc","noneof","9","8"],
				"AND", 
				["custrecord_fulfill_dashboard_opp_id.custrecord_projection_status_sfdc","anyof","2"]];
		}
		else if(i_user)
		{
			filters = [[[
				["custrecord_fulfill_dashboard_opp_id.custrecord_sfdc_opp_pm","anyof",i_user],
				"OR",
				["custrecord_fulfill_dashboard_opp_id.custrecord_sfdc_opportunity_dm","anyof",i_user],
				"OR",
				["custrecord_fulfill_dashboard_opp_id.custrecord_sfdc_client_partner","anyof",i_user]],
				"AND",
				["isinactive","is","F"],
				"AND", 
				["custrecord_fulfill_dashboard_opp_id.custrecord_stage_sfdc","noneof","9","8"],
				"AND", 
				["custrecord_fulfill_dashboard_opp_id.custrecord_projection_status_sfdc","anyof","2"]],
				"OR",
				[[["custrecord_fulfill_dashboard_opp_id.custrecord_projection_status_sfdc","anyof","3"],
					"AND",
					["custrecord_fulfill_dashboard_project","anyof","@NONE@"],
					"AND",
					["custrecord_fulfill_dashboard_opp_id.custrecord_sfdc_opp_pm","anyof",i_user],
					"OR",
					["custrecord_fulfill_dashboard_opp_id.custrecord_sfdc_opportunity_dm","anyof",i_user],
					"OR",
					["custrecord_fulfill_dashboard_opp_id.custrecord_sfdc_client_partner","anyof",i_user]],
					"AND",
					["isinactive","is","F"],
					"AND", 
					["custrecord_fulfill_dashboard_opp_id.custrecord_stage_sfdc","noneof","9","8"]]]
		}
		//nlapiLogExecution('Debug','filters GetOpportunityDetails',filters);
		var customrecord_fulfillment_dashboard_dataSearch = nlapiSearchRecord("customrecord_fulfillment_dashboard_data",null, filters,
				[
					//new nlobjSearchColumn("custrecord_fulfill_dashboard_status"), 
					new nlobjSearchColumn("custrecord_fulfill_dashboard_project"), 
					new nlobjSearchColumn("custrecord_fulfill_dashboard_total_pos"), 
					new nlobjSearchColumn("custrecord_fulfill_dashboard_account"), 
					new nlobjSearchColumn("custrecord_fulfill_dashboard_practice"), 
					new nlobjSearchColumn("custrecord_fulfill_dashboard_opp_id"), 
					new nlobjSearchColumn("custrecord_fulfill_dashboard_start_date"),
					new nlobjSearchColumn("custrecord_fulfill_dashboard_end_date"),
					new nlobjSearchColumn("custrecord_fulfill_dashboard_opp_id"),
					new nlobjSearchColumn("custrecord_fulfill_dashboard_risk"),	   
					new nlobjSearchColumn("custrecord_opp_fulfilment_status_sfdc","CUSTRECORD_FULFILL_DASHBOARD_OPP_ID",null),
					new nlobjSearchColumn("custrecord_confidence_sfdc","CUSTRECORD_FULFILL_DASHBOARD_OPP_ID",null), 	
					new nlobjSearchColumn("custrecord_opp_type_sfdc","CUSTRECORD_FULFILL_DASHBOARD_OPP_ID",null), 
					new nlobjSearchColumn("custrecord_opportunity_name_sfdc","custrecord_fulfill_dashboard_opp_id",null), 
					new nlobjSearchColumn("custrecord_sfdc_opp_account_region","CUSTRECORD_FULFILL_DASHBOARD_OPP_ID",null), 
					new nlobjSearchColumn("custentity_region","CUSTRECORD_FULFILL_DASHBOARD_PROJECT",null),
					new nlobjSearchColumn("custrecord_projection_status_sfdc","CUSTRECORD_FULFILL_DASHBOARD_OPP_ID",null),
					new nlobjSearchColumn("custrecord_status_sfdc","CUSTRECORD_FULFILL_DASHBOARD_OPP_ID",null),
					new nlobjSearchColumn("custrecord_account_type_sfdc","CUSTRECORD_FULFILL_DASHBOARD_OPP_ID",null),
					new nlobjSearchColumn("custrecord_tcv_sfdc","CUSTRECORD_FULFILL_DASHBOARD_OPP_ID",null),
					new nlobjSearchColumn("custrecord_practice_internal_id_sfdc","CUSTRECORD_FULFILL_DASHBOARD_OPP_ID",null), 
					new nlobjSearchColumn("custrecord_opportunity_id_sfdc","CUSTRECORD_FULFILL_DASHBOARD_OPP_ID",null),
					new nlobjSearchColumn("custrecord_set_owner","CUSTRECORD_FULFILL_DASHBOARD_OPP_ID",null)
					]
		);
		if(customrecord_fulfillment_dashboard_dataSearch)
		{
		//	nlapiLogExecution('Debug','customrecord_fulfillment_dashboard_dataSearch ',customrecord_fulfillment_dashboard_dataSearch.length);
			var dataArray = new Array();
			for(var i=0; i<customrecord_fulfillment_dashboard_dataSearch.length;i++)
			{
				var data = {};
				data.opportunity = {"name":customrecord_fulfillment_dashboard_dataSearch[i].getValue("custrecord_opportunity_name_sfdc","custrecord_fulfill_dashboard_opp_id",null),"id":customrecord_fulfillment_dashboard_dataSearch[i].getValue("custrecord_fulfill_dashboard_opp_id")};
				data.account = {"name":customrecord_fulfillment_dashboard_dataSearch[i].getText("custrecord_fulfill_dashboard_account"),"id":customrecord_fulfillment_dashboard_dataSearch[i].getValue("custrecord_fulfill_dashboard_account")};
				data.OppID = {"name": customrecord_fulfillment_dashboard_dataSearch[i].getValue("custrecord_opportunity_id_sfdc","custrecord_fulfill_dashboard_opp_id",null)};
				//data.account = {"name":customrecord_fulfillment_dashboard_dataSearch[i].getValue("custrecord_fulfill_dashboard_account")};

				data.practice = {"name":customrecord_fulfillment_dashboard_dataSearch[i].getText("custrecord_practice_internal_id_sfdc","CUSTRECORD_FULFILL_DASHBOARD_OPP_ID",null),"id":customrecord_fulfillment_dashboard_dataSearch[i].getValue("custrecord_practice_internal_id_sfdc","CUSTRECORD_FULFILL_DASHBOARD_OPP_ID",null)};
				//data.practice = {"name":customrecord_fulfillment_dashboard_dataSearch[i].getText("custrecord_fulfill_dashboard_practice"),"id":customrecord_fulfillment_dashboard_dataSearch[i].getValue("custrecord_fulfill_dashboard_practice")};
				data.startDate = {"name":customrecord_fulfillment_dashboard_dataSearch[i].getValue("custrecord_fulfill_dashboard_start_date")};
				data.endDate = {"name":customrecord_fulfillment_dashboard_dataSearch[i].getValue("custrecord_fulfill_dashboard_end_date")};
				data.oppStatus = {"name":customrecord_fulfillment_dashboard_dataSearch[i].getValue("custrecord_opp_fulfilment_status_sfdc","CUSTRECORD_FULFILL_DASHBOARD_OPP_ID",null)};
				data.positions = {"name":customrecord_fulfillment_dashboard_dataSearch[i].getValue("custrecord_fulfill_dashboard_total_pos")};
				if(customrecord_fulfillment_dashboard_dataSearch[i].getValue("custentity_region","CUSTRECORD_FULFILL_DASHBOARD_PROJECT",null))
				{
					data.region = {"name":customrecord_fulfillment_dashboard_dataSearch[i].getText("custentity_region","CUSTRECORD_FULFILL_DASHBOARD_PROJECT",null),"id":customrecord_fulfillment_dashboard_dataSearch[i].getValue("custentity_region","CUSTRECORD_FULFILL_DASHBOARD_PROJECT",null)};
				}
				else if(customrecord_fulfillment_dashboard_dataSearch[i].getValue("custrecord_sfdc_opp_account_region","CUSTRECORD_FULFILL_DASHBOARD_OPP_ID",null))
				{
					data.region = {"name":customrecord_fulfillment_dashboard_dataSearch[i].getText("custrecord_sfdc_opp_account_region","CUSTRECORD_FULFILL_DASHBOARD_OPP_ID",null),"id":customrecord_fulfillment_dashboard_dataSearch[i].getValue("custrecord_sfdc_opp_account_region","CUSTRECORD_FULFILL_DASHBOARD_OPP_ID",null)};
				}
				else 
				{
					data.region = {"name":"","id":""};
				}
				//data.region = {"name":customrecord_fulfillment_dashboard_dataSearch[i].getText("custentity_region","CUSTRECORD_FULFILL_DASHBOARD_PROJECT",null),"id":customrecord_fulfillment_dashboard_dataSearch[i].getValue("custentity_region","CUSTRECORD_FULFILL_DASHBOARD_PROJECT",null)};
				data.salesactivity = {"name":customrecord_fulfillment_dashboard_dataSearch[i].getText("custrecord_projection_status_sfdc","CUSTRECORD_FULFILL_DASHBOARD_OPP_ID",null)};
				data.frfstatus = {"name":customrecord_fulfillment_dashboard_dataSearch[i].getText("custrecord_status_sfdc","CUSTRECORD_FULFILL_DASHBOARD_OPP_ID",null),"id":customrecord_fulfillment_dashboard_dataSearch[i].getValue("custrecord_status_sfdc","CUSTRECORD_FULFILL_DASHBOARD_OPP_ID",null)};
				data.frfCount = getFrfCount(customrecord_fulfillment_dashboard_dataSearch[i].getValue("custrecord_fulfill_dashboard_opp_id"));
				data.risktotal = {"name":customrecord_fulfillment_dashboard_dataSearch[i].getValue("custrecord_fulfill_dashboard_risk")};
				data.participatingpractice = {"name" : getOpportunityDetails(customrecord_fulfillment_dashboard_dataSearch[i].getValue("custrecord_fulfill_dashboard_opp_id"),customrecord_fulfillment_dashboard_dataSearch[i].getText("custrecord_fulfill_dashboard_practice"))};
				data.confidence = {"name" : customrecord_fulfillment_dashboard_dataSearch[i].getValue("custrecord_confidence_sfdc","CUSTRECORD_FULFILL_DASHBOARD_OPP_ID",null)};
				data.stage = {"name" : customrecord_fulfillment_dashboard_dataSearch[i].getValue("custrecord_account_type_sfdc","CUSTRECORD_FULFILL_DASHBOARD_OPP_ID",null)};
				data.tcv = {"name" : customrecord_fulfillment_dashboard_dataSearch[i].getValue("custrecord_tcv_sfdc","CUSTRECORD_FULFILL_DASHBOARD_OPP_ID",null)};
				data.opportunityType = {"name" : customrecord_fulfillment_dashboard_dataSearch[i].getValue("custrecord_opp_type_sfdc","CUSTRECORD_FULFILL_DASHBOARD_OPP_ID",null)};
				data.setOwner = {"name" : customrecord_fulfillment_dashboard_dataSearch[i].getText("custrecord_set_owner","CUSTRECORD_FULFILL_DASHBOARD_OPP_ID",null),"id": customrecord_fulfillment_dashboard_dataSearch[i].getValue("custrecord_set_owner","CUSTRECORD_FULFILL_DASHBOARD_OPP_ID",null)};
				
				dataArray.push(data);
			}

		//	nlapiLogExecution('Debug','o_projectData Opportunity Details ',JSON.stringify(dataArray));
			return dataArray;

		}
		else
		{   var dataArray = [];
		return dataArray;
		}
	}
	catch(error)
	{
		return error;
	}
}

function getFrfCount(i_oppId)
{
	//nlapiLogExecution('debug','i_oppId ',i_oppId);
	if(i_oppId)
	{
		var customrecord_frf_detailsSearch = nlapiSearchRecord("customrecord_frf_details",null,
				[
					["custrecord_frf_details_opp_id","anyof",i_oppId]
					], 
					[
						new nlobjSearchColumn("internalid",null,"COUNT")
						]
		);
		if(customrecord_frf_detailsSearch)
		{
			var i_frfCount = customrecord_frf_detailsSearch[0].getValue("internalid",null,"COUNT");
			return i_frfCount;
		}
	}
	else
	{
		return 0;
	}
}

function GetpipeLineProjectDetails(i_user,i_practice,isAdmin,i_region,deliveryAnchore)
{
	//nlapiLogExecution('Debug','o_projectData ',i_user);
	var filters = [];
	if(isAdmin==true)
	{
		filters = [["isinactive","is","F"],
			"AND", 
			["custrecord_fulfill_dashboard_opp_id.custrecord_stage_sfdc","noneof","9","8"],
			"AND", 
			["custrecord_fulfill_dashboard_opp_id.custrecord_projection_status_sfdc","anyof","1"]]  // need to add closed filter
	}
	else if(i_practice)
	{
		filters = [
			["custrecord_fulfill_dashboard_opp_id.custrecord_practice_internal_id_sfdc","anyof",i_practice],
			"AND",
			["isinactive","is","F"],
			"AND", 
			["custrecord_fulfill_dashboard_opp_id.custrecord_stage_sfdc","noneof","9","8"],
			"AND", 
			["custrecord_fulfill_dashboard_opp_id.custrecord_projection_status_sfdc","anyof","1"]]
	}
	else if(i_region)
	{

		 filters = [
			["custrecord_fulfill_dashboard_opp_id.custrecord_sfdc_opp_account_region","anyof",i_region],
			"AND",
			["isinactive","is","F"],
			"AND",
			["custrecord_fulfill_dashboard_opp_id.custrecord_stage_sfdc","noneof","9","8"],
			"AND", 
			["custrecord_fulfill_dashboard_opp_id.custrecord_projection_status_sfdc","anyof","1"]]
	}
	else if(deliveryAnchore.length>0)
	{
		 filters = [
			["custrecord_fulfill_dashboard_account","anyof",deliveryAnchore],
			"AND",
			["isinactive","is","F"],
			"AND",
			["custrecord_fulfill_dashboard_opp_id.custrecord_stage_sfdc","noneof","9","8"],
			"AND", 
			["custrecord_fulfill_dashboard_opp_id.custrecord_projection_status_sfdc","anyof","1"]]
	}
	else if(i_user)
	{
		filters = [[
			["custrecord_fulfill_dashboard_opp_id.custrecord_sfdc_opp_pm","anyof",i_user],
			"OR",
			["custrecord_fulfill_dashboard_opp_id.custrecord_sfdc_opportunity_dm","anyof",i_user],
			"OR",
			["custrecord_fulfill_dashboard_opp_id.custrecord_sfdc_client_partner","anyof",i_user]],
			"AND",
			["isinactive","is","F"],
			"AND", 
			["custrecord_fulfill_dashboard_opp_id.custrecord_stage_sfdc","noneof","9","8"],
			"AND", 
			["custrecord_fulfill_dashboard_opp_id.custrecord_projection_status_sfdc","anyof","1"]]
	}
//	nlapiLogExecution('Debug','filters GetpipeLineProjectDetails',filters);
	var customrecord_fulfillment_dashboard_dataSearch = nlapiSearchRecord("customrecord_fulfillment_dashboard_data",null, filters,
			[
				//new nlobjSearchColumn("custrecord_fulfill_dashboard_status"), 
				new nlobjSearchColumn("custrecord_fulfill_dashboard_project"), 
				new nlobjSearchColumn("custrecord_fulfill_dashboard_account"), 
				new nlobjSearchColumn("custrecord_fulfill_dashboard_total_pos"), 
				new nlobjSearchColumn("custrecord_fulfill_dashboard_practice"), 
				new nlobjSearchColumn("custrecord_fulfill_dashboard_opp_id"), 
				new nlobjSearchColumn("custrecord_fulfill_dashboard_start_date"),
				new nlobjSearchColumn("custrecord_fulfill_dashboard_end_date"),	
				new nlobjSearchColumn("custrecord_fulfill_dashboard_risk"),
				new nlobjSearchColumn("custrecord_opp_fulfilment_status_sfdc","CUSTRECORD_FULFILL_DASHBOARD_OPP_ID",null), 
				new nlobjSearchColumn("custrecord_opportunity_name_sfdc","CUSTRECORD_FULFILL_DASHBOARD_OPP_ID",null), 
				new nlobjSearchColumn("custrecord_sfdc_opp_account_region","CUSTRECORD_FULFILL_DASHBOARD_OPP_ID",null), 
				new nlobjSearchColumn("custentity_region","CUSTRECORD_FULFILL_DASHBOARD_PROJECT",null),
				new nlobjSearchColumn("custrecord_projection_status_sfdc","CUSTRECORD_FULFILL_DASHBOARD_OPP_ID",null),
				new nlobjSearchColumn("custrecord_status_sfdc","CUSTRECORD_FULFILL_DASHBOARD_OPP_ID",null),
				new nlobjSearchColumn("custrecord_account_type_sfdc","CUSTRECORD_FULFILL_DASHBOARD_OPP_ID",null),
				new nlobjSearchColumn("custrecord_tcv_sfdc","CUSTRECORD_FULFILL_DASHBOARD_OPP_ID",null),
				new nlobjSearchColumn("custrecord_opp_type_sfdc","CUSTRECORD_FULFILL_DASHBOARD_OPP_ID",null), 
				new nlobjSearchColumn("custrecord_confidence_sfdc","CUSTRECORD_FULFILL_DASHBOARD_OPP_ID",null),
				new nlobjSearchColumn("custrecord_practice_internal_id_sfdc","CUSTRECORD_FULFILL_DASHBOARD_OPP_ID",null),
				new nlobjSearchColumn("custrecord_opportunity_id_sfdc","CUSTRECORD_FULFILL_DASHBOARD_OPP_ID",null),
				new nlobjSearchColumn("custrecord_set_owner","CUSTRECORD_FULFILL_DASHBOARD_OPP_ID",null)
				]
	);
	if(customrecord_fulfillment_dashboard_dataSearch)
	{
		var dataArray = new Array();
		for(var i=0; i<customrecord_fulfillment_dashboard_dataSearch.length;i++)
		{
			//nlapiLogExecution('Debug','o_projectData Pipeline Details '+i,dataArray);
			var data = {};
			data.project = {"name":customrecord_fulfillment_dashboard_dataSearch[i].getValue("custrecord_opportunity_name_sfdc","custrecord_fulfill_dashboard_opp_id",null),"id":customrecord_fulfillment_dashboard_dataSearch[i].getValue("custrecord_fulfill_dashboard_opp_id")};
			data.account = {"name":customrecord_fulfillment_dashboard_dataSearch[i].getText("custrecord_fulfill_dashboard_account"),"id":customrecord_fulfillment_dashboard_dataSearch[i].getValue("custrecord_fulfill_dashboard_account")};
			data.OppID = {"name": customrecord_fulfillment_dashboard_dataSearch[i].getValue("custrecord_opportunity_id_sfdc","custrecord_fulfill_dashboard_opp_id",null)};	
			//data.account = {"name":customrecord_fulfillment_dashboard_dataSearch[i].getValue("custrecord_fulfill_dashboard_account")};
			data.practice = {"name":customrecord_fulfillment_dashboard_dataSearch[i].getText("custrecord_practice_internal_id_sfdc","CUSTRECORD_FULFILL_DASHBOARD_OPP_ID",null),"id":customrecord_fulfillment_dashboard_dataSearch[i].getValue("custrecord_practice_internal_id_sfdc","CUSTRECORD_FULFILL_DASHBOARD_OPP_ID",null)};
			//data.practice = {"name":customrecord_fulfillment_dashboard_dataSearch[i].getText("custrecord_fulfill_dashboard_practice"),"id":customrecord_fulfillment_dashboard_dataSearch[i].getValue("custrecord_fulfill_dashboard_practice")};
			data.startDate = {"name":customrecord_fulfillment_dashboard_dataSearch[i].getValue("custrecord_fulfill_dashboard_start_date")};
			data.endDate = {"name":customrecord_fulfillment_dashboard_dataSearch[i].getValue("custrecord_fulfill_dashboard_end_date")};
			data.oppStatus = {"name":customrecord_fulfillment_dashboard_dataSearch[i].getValue("custrecord_opp_fulfilment_status_sfdc","CUSTRECORD_FULFILL_DASHBOARD_OPP_ID",null)};
			data.positions = {"name":customrecord_fulfillment_dashboard_dataSearch[i].getValue("custrecord_fulfill_dashboard_total_pos")};
			if(customrecord_fulfillment_dashboard_dataSearch[i].getValue("custentity_region","CUSTRECORD_FULFILL_DASHBOARD_PROJECT",null))
			{
				data.region = {"name":customrecord_fulfillment_dashboard_dataSearch[i].getText("custentity_region","CUSTRECORD_FULFILL_DASHBOARD_PROJECT",null),"id":customrecord_fulfillment_dashboard_dataSearch[i].getValue("custentity_region","CUSTRECORD_FULFILL_DASHBOARD_PROJECT",null)};
			}
			else if(customrecord_fulfillment_dashboard_dataSearch[i].getValue("custrecord_sfdc_opp_account_region","CUSTRECORD_FULFILL_DASHBOARD_OPP_ID",null))
			{
				data.region = {"name":customrecord_fulfillment_dashboard_dataSearch[i].getText("custrecord_sfdc_opp_account_region","CUSTRECORD_FULFILL_DASHBOARD_OPP_ID",null),"id":customrecord_fulfillment_dashboard_dataSearch[i].getValue("custrecord_sfdc_opp_account_region","CUSTRECORD_FULFILL_DASHBOARD_OPP_ID",null)};
			}
			else 
			{
				data.region = {"name":"","id":""};
			}
			data.salesactivity = {"name":customrecord_fulfillment_dashboard_dataSearch[i].getText("custrecord_projection_status_sfdc","CUSTRECORD_FULFILL_DASHBOARD_OPP_ID",null)};
			data.frfstatus = {"name":customrecord_fulfillment_dashboard_dataSearch[i].getText("custrecord_status_sfdc","CUSTRECORD_FULFILL_DASHBOARD_OPP_ID",null),"id":customrecord_fulfillment_dashboard_dataSearch[i].getValue("custrecord_status_sfdc","CUSTRECORD_FULFILL_DASHBOARD_OPP_ID",null)};
			data.risktotal = {"name":customrecord_fulfillment_dashboard_dataSearch[i].getValue("custrecord_fulfill_dashboard_risk")};
			data.frfCount = getFrfCount(customrecord_fulfillment_dashboard_dataSearch[i].getValue("custrecord_fulfill_dashboard_opp_id"));
			data.participatingpractice = {"name" : getOpportunityDetails(customrecord_fulfillment_dashboard_dataSearch[i].getValue("custrecord_fulfill_dashboard_opp_id"),customrecord_fulfillment_dashboard_dataSearch[i].getText("custrecord_fulfill_dashboard_practice"))};
			data.confidence = {"name" : customrecord_fulfillment_dashboard_dataSearch[i].getValue("custrecord_confidence_sfdc","CUSTRECORD_FULFILL_DASHBOARD_OPP_ID",null)};
			data.stage = {"name" : customrecord_fulfillment_dashboard_dataSearch[i].getValue("custrecord_account_type_sfdc","CUSTRECORD_FULFILL_DASHBOARD_OPP_ID",null)};
			data.tcv = {"name" : customrecord_fulfillment_dashboard_dataSearch[i].getValue("custrecord_tcv_sfdc","CUSTRECORD_FULFILL_DASHBOARD_OPP_ID",null)};
			data.opportunityType = {"name" : customrecord_fulfillment_dashboard_dataSearch[i].getValue("custrecord_opp_type_sfdc","CUSTRECORD_FULFILL_DASHBOARD_OPP_ID",null)};
			data.setOwner = {"name" : customrecord_fulfillment_dashboard_dataSearch[i].getText("custrecord_set_owner","CUSTRECORD_FULFILL_DASHBOARD_OPP_ID",null),"id": customrecord_fulfillment_dashboard_dataSearch[i].getValue("custrecord_set_owner","CUSTRECORD_FULFILL_DASHBOARD_OPP_ID",null)};
			
			dataArray.push(data);
		}
	//	nlapiLogExecution('Debug','o_projectData Pipeline Details ',JSON.stringify(dataArray));
		return dataArray;

	}
	else
	{
		var dataArray = [];
		return dataArray;
	}
}



/*********************************************Function to get Search Results**********************************************/
function searchRecord(recordType, savedSearch, arrFilters, arrColumns,
		filterExpression)
{

	try {
		var search = null;

		// if a saved search is provided, load it and add the filters and
		// columns
		if (isNotEmpty(savedSearch)) {
			search = nlapiLoadSearch(recordType, savedSearch);

			if (isArrayNotEmpty(arrFilters)) {
				search.addFilters(arrFilters);
			}

			if (isArrayNotEmpty(arrColumns)) {
				search.addColumns(arrColumns);
			}

			if (isArrayNotEmpty(filterExpression)) {
				search.setFilterExpression(filterExpression);
			}
		}
		// create a new search
		else {
			search = nlapiCreateSearch(recordType, arrFilters, arrColumns);
		}

		// run search
		var resultSet = search.runSearch();

		// iterate through the search and get all data 1000 at a time
		var searchResultCount = 0;
		var resultSlice = null;
		var searchResult = [];

		do {
			resultSlice = resultSet.getResults(searchResultCount,
					searchResultCount + 1000);

			if (resultSlice) {

				resultSlice.forEach(function(result) {

					searchResult.push(result);
					searchResultCount++;
				});
			}
		} while (isArrayNotEmpty(resultSlice) && resultSlice.length >= 1000);

		return searchResult;
	} catch (err) {
		nlapiLogExecution('ERROR', 'searchRecord', err);
		throw err;
	}
}

function isEmpty(value) {

	return value == null || value == "" || typeof (value) == undefined;
}

function isNotEmpty(value) {

	return !isEmpty(value);
}

function isArrayEmpty(argArray) {

	return !isArrayNotEmpty(argArray);
}

function isArrayNotEmpty(argArray) {

	return (isNotEmpty(argArray) && argArray.length > 0);
}

//*******************GET REGION***************************//

function GetRegion(user) 
{
	try
	{
		//nlapiLogExecution('AUDIT', 'user', user);
		var regionSearch = nlapiSearchRecord("customrecord_region",null,
				[
					[["custrecord_region_head","anyof",user],"OR",["custrecord_region_head_2","anyof",user]],
					"AND",
					["isinactive","is","F"]
					], 
					[
						new nlobjSearchColumn("name")
						]
		);
		if(regionSearch)
		{
			var region = regionSearch[0].getId();
			//nlapiLogExecution('Debug','region ',region);
			return region;
		}
		else
		{
			return '';
		}

	}
	catch(e)
	{
		return e; 
	}

}
function GetDeliveryAnchore(user)
{
	var temp = [];
	var customrecord_fuel_delivery_anchorSearch = nlapiSearchRecord("customrecord_fuel_delivery_anchor",null,
			[
				["custrecord_fuel_anchor_employee","anyof",user]
				], 
				[
					new nlobjSearchColumn("custrecord_fuel_anchor_customer")
					]
	);	
	if(customrecord_fuel_delivery_anchorSearch){
		for(var i = 0 ; i < customrecord_fuel_delivery_anchorSearch.length ;i++){
			temp.push(customrecord_fuel_delivery_anchorSearch[i].getValue('custrecord_fuel_anchor_customer'));
		}
		return temp;
	}else{
		return false;
	}
}
//function to get the subpractice tagged to parent 
function getSubPractices(i_practice){
	var temp = [];
	var departmentSearch = nlapiSearchRecord("department",null,
			[
				["isinactive","is","F"], 
				"AND", 
				["custrecord_parent_practice","anyof",i_practice]
				], 
				[
					]
	);
	if(departmentSearch){
		for(var i = 0 ; i < departmentSearch.length ;i++){
			temp.push(departmentSearch[i].getId());
		}
		return temp;
	}else{
		return i_practice
	}
}


function getUtilisation(isAdmin,practice,user)
{
	var filters = new Array();
	var columns = new Array();
	if(isAdmin==true)
	{
		var practiceArray = getAdminPractice();
		filters =  [["startdate","notonorafter","today"], 
			"AND", 
			["enddate","notonorbefore","today"], 
			"AND", 
			["employee.custentity_employee_inactive","is","F"], 
			"AND", 
			["employee.custentity_implementationteam","is","F"],
			"AND",
			["employee.department","anyof",practiceArray]]
	}
	else if(practice)
	{
		filters = [["startdate","notonorafter","today"], 
			"AND", 
			["enddate","notonorbefore","today"], 
			"AND", 
			["employee.custentity_employee_inactive","is","F"], 
			"AND", 
			["employee.custentity_implementationteam","is","F"], 
			"AND", 
			["employee.department","anyof",practice]]
	}
	else if(user)
	{
		var practice = nlapiLookupField('employee',user,'department');
		var parentPractice = nlapiLookupField("department",practice,"custrecord_parent_practice");
		var a_subPractices = getSubPractices(parentPractice);
		filters = [["startdate","notonorafter","today"], 
			"AND", 
			["enddate","notonorbefore","today"], 
			"AND", 
			["employee.custentity_employee_inactive","is","F"], 
			"AND", 
			["employee.custentity_implementationteam","is","F"], 
			"AND", 
			["employee.department","anyof",a_subPractices]]

	}  
	var resourceallocationSearch = nlapiSearchRecord("resourceallocation",null,filters,
			[
				new nlobjSearchColumn("custeventrbillable",null,"GROUP").setSort(false), 
				new nlobjSearchColumn("internalid",null,"COUNT")
				]
	);
//	nlapiLogExecution('debug','filters $$$$ ',filters);
	if(resourceallocationSearch)
	{
		var dataArray = new Array();
		var data = {};
		var flg  = false;
		var flg_ = false;
		for(var i=0;i<resourceallocationSearch.length;i++)
		{
			
			var isBillable = resourceallocationSearch[i].getValue("custeventrbillable",null,"GROUP");
			var count = resourceallocationSearch[i].getValue("internalid",null,"COUNT");
			nlapiLogExecution('Debug','isBillable '+isBillable, 'count '+count);
			if(_logValidation(isBillable) && isBillable == 'F' && parseInt(count) > parseInt(1) && flg == false)
			{
				data.nonBillable = {"name":count};
				flg = true;
			}
			if(_logValidation(isBillable) && isBillable == 'T' && flg_ == false)
			{
				data.billable = {"name":count};
				flg_ =  true;
			}
			dataArray.push(data);
		}
		//nlapiLogExecution('Debug','dataArray Utilisation ',dataArray);
		return dataArray;
	}
}

function getAdminPractice()
{
	var departmentSearch = nlapiSearchRecord("department",null,
			[
				["isinactive","is","F"]/*, 
				"AND", 
				["custrecord_is_delivery_practice","is","T"] */
				], 
				[
					new nlobjSearchColumn("internalid")
					]
	);
	if(departmentSearch)
	{
		var practiceArray = new Array();
		for(var i=0; i<departmentSearch.length;i++)
		{
			practiceArray.push(departmentSearch[i].getValue('internalid'));
		}
		//nlapiLogExecution('Debug','practiceArray ',practiceArray);
		return practiceArray;		
	}
}

function _logValidation(value) {
	if (value != null && value != '' && value != undefined && value != 'NaN') {
		return true;
	} else {
		return false;
	}
}
function getOpportunityDetails(oppId,practiceName) {
	var tempArray = [];
	var customrecord_sfdc_opportunity_recordSearch = nlapiSearchRecord("customrecord_sfdc_opportunity_record", null,
			[
				["internalidnumber", "equalto", oppId]
				],
				[
					new nlobjSearchColumn("custrecord_fulfill_plan_practice", "CUSTRECORD_FULFILL_PLAN_OPP_ID", null)
					]
	);
	if (customrecord_sfdc_opportunity_recordSearch) {
		for (var i = 0; i < customrecord_sfdc_opportunity_recordSearch.length; i++) {
			var s_practice = customrecord_sfdc_opportunity_recordSearch[i].getValue("custrecord_fulfill_plan_practice", "CUSTRECORD_FULFILL_PLAN_OPP_ID", null);
			if(practiceName != s_practice){
				if(s_practice != ""){
					tempArray.push(s_practice); 
				}	
			}

		}
	}
	return tempArray;
}

//----------------------------------------------------------------------------------------------------------------
//prabhat gupta 27/8/2020 doing this because of production issue
function searchRecord(recordType, savedSearch, arrFilters, arrColumns,
		filterExpression) {

	try {
		var search = null;

		// if a saved search is provided, load it and add the filters and
		// columns
		if (savedSearch) {
			search = nlapiLoadSearch(recordType, savedSearch);

			if (isArrayNotEmpty(arrFilters)) {
				search.addFilters(arrFilters);
			}

			if (isArrayNotEmpty(arrColumns)) {
				search.addColumns(arrColumns);
			}

			if (isArrayNotEmpty(filterExpression)) {
				search.setFilterExpression(filterExpression);
			}
		}
		// create a new search
		else {
			search = nlapiCreateSearch(recordType, arrFilters, arrColumns);
		}

		// run search
		var resultSet = search.runSearch();

		// iterate through the search and get all data 1000 at a time
		var searchResultCount = 0;
		var resultSlice = null;
		var searchResult = [];

		do {
			resultSlice = resultSet.getResults(searchResultCount,
					searchResultCount + 1000);

			if (resultSlice) {

				resultSlice.forEach(function(result) {

					searchResult.push(result);
					searchResultCount++;
				});
			}
		} while (isArrayNotEmpty(resultSlice) && resultSlice.length >= 1000);

		return searchResult;
	} catch (err) {
		nlapiLogExecution('ERROR', 'searchRecord', err);
		throw err;
	}
}
//----------------------------------------------------------------------------------------------------------------