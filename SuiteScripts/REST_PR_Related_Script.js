/**
 * Module Description
 * 
 * Version    Date            		Author           Remarks
 * 1.00       04 Nov 2020     	shravan.k		 Creation of PR Record
 *
 */

/**
 * @param {Object} dataIn Parameter object
 * @returns {Object} Output object
 */
function postRESTlet_PR_Operations(dataIn) 
{
	var obj_Response = new Response();
	try
	{
		var obj_Usage_Context = nlapiGetContext();
		if ( (!dataIn) || ( !(dataIn.Data) ) )
			{
					obj_Response.Data ='Please Enter Request Body';
					obj_Response.Status = false;
					return obj_Response
			}// End of if ( (!dataIn) || ( !(dataIn.Data) ) )
		nlapiLogExecution('DEBUG', 'Begining Usage  ==',obj_Usage_Context.getRemainingUsage());
		nlapiLogExecution('DEBUG', 'Input JSON is ==', JSON.stringify(dataIn));
		var s_Request_Type = dataIn.RequestType;
		nlapiLogExecution('DEBUG', 's_Request_Type==',s_Request_Type); 
		var s_Email_Id = dataIn.Data.EmailId;
		nlapiLogExecution('DEBUG', 's_Email_Id==',s_Email_Id); 
		if(  (s_Request_Type == 'GET' ) ||   (s_Request_Type == 'CREATE' ) || (s_Request_Type == 'GETALL' ) )  /// if request has no email id
		{
			if(!s_Email_Id)
				{
					obj_Response.Data ='Please Enter Email ID';
					obj_Response.Status = false;
				} ////End of if(!s_Email_Id)
			else
			{
				var i_Employee_Internal_Id = getUserUsingEmailId(s_Email_Id)
				nlapiLogExecution('DEBUG', 'i_Employee_Internal_Id==', i_Employee_Internal_Id);
			}///End of else of if(!s_Email_Id)
		} //// End of (  ( (s_Request_Type == 'GET' ) ||   (s_Request_Type == 'CREATE' ) )  && (!s_Email_Id))

		var i_Pr_Num = dataIn.Data.PRNumber;
		nlapiLogExecution('DEBUG', 'i_Pr_Num==',i_Pr_Num); 
		
		switch(s_Request_Type)
		{
				case M_Constants.Request.Get:
							obj_Response.Data = exportSupportingData(i_Employee_Internal_Id);  /// This function returns the JSON for Supporting of creation of PR i.e Project , Item and location
							obj_Response.Status = true;
				break;   //// End of case For Get
				
				case M_Constants.Request.GetOne:
					if(i_Pr_Num)
						{
							obj_Response.Data = getStatusofOnePR(i_Pr_Num);  /// This function returns the JSON of status of all items of One PR
							obj_Response.Status = true;
						} /// End of if(i_Pr_Num)
					else
						{
							obj_Response.Data ='Please Enter PR Number';
							obj_Response.Status = false;
						} /// End of else of if(i_Pr_Num)
				break;   //// End of case For Getone
					
				case M_Constants.Request.GetAll:
							obj_Response.Data = getStatusofAllPR(i_Employee_Internal_Id);  /// This function returns the JSON for All the PRs of Employee Requested
							obj_Response.Status = true;
				break;   //// End of case For GetAll
				
				case M_Constants.Request.Create:
					obj_Response.Data = createPRRequest(dataIn,i_Employee_Internal_Id);  /// This function returns the JSON for All the PRs of Employee Requested
					obj_Response.Status = true;
					break; /// End of case For Create
					
					default:
						obj_Response.Data = 'Improper Request , Please check the Request Type';
						obj_Response.Status = false;
						break; /// /// End of case Default
				
		} /// End of  Switch
	} /// End of try for main post restlet Function
	catch(e)
	{
		nlapiLogExecution('DEBUG', 'Exception in main function ==', e);
		obj_Response.Data = e;
		obj_Response.Status = false;
	} /// End of catch for main post restlet Function
	nlapiLogExecution('DEBUG', 'Output JSON is ==', JSON.stringify(obj_Response));
	nlapiLogExecution('DEBUG', 'Final Remaining Usage  ==',obj_Usage_Context.getRemainingUsage());
	return obj_Response;
}/// End of Main Function of Post


/////// Function for Exporting Support Data -------  BEGIN
function exportSupportingData(i_Employee_Internal_Id)
{
	try
	{
		nlapiLogExecution('DEBUG', 'exportSupportingData==', 'exportSupportingData');
		var obj_Item_Search = nlapiSearchRecord("item",null,
				[
				   ["isinactive","is","F"], 
				   "AND", 
				   ["custitem1","is","T"], 
				   "AND", 
				   ["type","anyof","NonInvtPart","Service"]
				], 
				[
				   new nlobjSearchColumn("itemid").setSort(false), 
				   new nlobjSearchColumn("internalid")
				]
				); /// end of item search
		if(obj_Item_Search)
			{
				var s_Item_Name,i_Item_InternalId;
				var arr_Data_Items = [];
				var obj_Data_Items_Json = {};
			    for(var i_Index_item = 0; i_Index_item <  obj_Item_Search.length ; i_Index_item++)
			    	{
			    			s_Item_Name = obj_Item_Search[i_Index_item].getValue('itemid');
			    			i_Item_InternalId =obj_Item_Search[i_Index_item].getValue('internalid');
			    			obj_Data_Items_Json = {
			    					Name : s_Item_Name,
			    					Id : i_Item_InternalId
			    			} 
			    			arr_Data_Items.push(obj_Data_Items_Json);
			    	} /// End of for(var i_Index_item = 0; i_Index_item <  obj_Item_Search.length ; i_Index_item++)
			} // End of if(obj_Item_Search)
			
		var arr_Filter_Project  = [
		   ["isinactive","is","F"], 
		   "AND", 
		   ["status","anyof","2","4"], 
		   "AND", 
		   ["custentity_practice.isinactive","is","F"]
		]; 
		
		var arr_Columns_Project = [];
		arr_Columns_Project.push(new nlobjSearchColumn("internalid"));
		arr_Columns_Project.push( new nlobjSearchColumn("companyname"));
		arr_Columns_Project.push( new nlobjSearchColumn("entityid"));
		
		var obj_Project_Search = searchRecord('job',null,arr_Filter_Project,arr_Columns_Project); /// Custom Search function stored in Library of utility.js, used in case of 1000 or more results
		
		if(obj_Project_Search)
			{
					var s_Project_Name,s_Project_Id,i_Project_InternalId,s_Return_Name;
					var arr_Data_Project = [];
					var obj_Data_Project_Json = {};
					for(var i_Project_Index = 0; i_Project_Index < obj_Project_Search.length ;  i_Project_Index++ )
						{
								s_Project_Name = obj_Project_Search[i_Project_Index].getValue('companyname');
								s_Project_Id = obj_Project_Search[i_Project_Index].getValue('entityid');
								i_Project_InternalId = obj_Project_Search[i_Project_Index].getValue('internalid');
								s_Return_Name =   s_Project_Id + ' : ' + s_Project_Name ;
								obj_Data_Project_Json = {
										Name : s_Return_Name,
										Id: i_Project_InternalId
								}
								arr_Data_Project.push(obj_Data_Project_Json);
						} ///  End of for(var i_Project_Index = 0; i_Project_Index < obj_Project_Search.length ;  i_Project_Index++ )
			}// End of if(obj_Project_Search)
		var i_Employee_Subsidiary = nlapiLookupField('employee', i_Employee_Internal_Id, 'subsidiary');
	//	nlapiLogExecution('DEBUG', 'i_Employee_Subsidiary==', i_Employee_Subsidiary);
		i_Employee_Subsidiary = parseInt(i_Employee_Subsidiary);
		var obj_Location_Search = nlapiSearchRecord("location",null,
				[
				   ["isinactive","is","F"], 
				   "AND", 
				   ["subsidiary","anyof",i_Employee_Subsidiary]
				], 
				[
				   new nlobjSearchColumn("name").setSort(false), 
				   new nlobjSearchColumn("internalid")
				]
				);
		var arr_Data_Location = [];
		var obj_Data_Location_Json = {};
		if(obj_Location_Search)
			{
					var s_Location_Name,i_Location_Id;
					for(var i_Location_Index = 0; i_Location_Index < obj_Location_Search.length ; i_Location_Index++ )
						{
									s_Location_Name = obj_Location_Search[i_Location_Index].getValue('name');
									i_Location_Id= obj_Location_Search[i_Location_Index].getValue('internalid');
									obj_Data_Location_Json = 
										{
											Name:s_Location_Name,
											Id : i_Location_Id
										}
									arr_Data_Location.push(obj_Data_Location_Json);
						}// end of for(var i_Location_Index = 0; i_Location_Index < obj_Location_Search.length , i_Location_Index++ )
			}//// End of if(obj_Location_Search)
		
		return {
			Projects : arr_Data_Project,
			Items : arr_Data_Items,
			Locations:arr_Data_Location
		}
	}//// End of try for exportSupportingData
	catch(e)
	{
		nlapiLogExecution('DEBUG', 'Exception in  exportSupportingData==', e);
		return e;
	}/// End of catch for exportSupportingData
}/// End of exportSupportingData
/////// Function for Exporting Support Data -------    END

//////// Function for Getting Status of one PR  ---- BEGIN
function getStatusofOnePR(i_Pr_Num)
{
	try
	{
		nlapiLogExecution('DEBUG', 'getStatusofOnePR==', i_Pr_Num);
	var obj_getStatusofOnePR_Response = {};
	var obj_PR_Search_One_Record = nlapiSearchRecord("customrecord__prpurchaserequest",null,
			[
			   ["custrecord_prno","is",i_Pr_Num], 
			   "AND", 
			   ["isinactive","is","F"]
			], 
			[
			   new nlobjSearchColumn("custrecord_prno"), 
			   new nlobjSearchColumn("custrecord_requisitiondate"), 
			   new nlobjSearchColumn("custrecord_requisitionname"), 
			   new nlobjSearchColumn("custrecord_requistionerremarks"), 
			   new nlobjSearchColumn("custrecord_prmatgrpcategory","CUSTRECORD_PURCHASEREQUEST",null), 
			   new nlobjSearchColumn("custrecord_prastatus","CUSTRECORD_PURCHASEREQUEST",null), 
			   new nlobjSearchColumn("custrecord_totalvalue","CUSTRECORD_PURCHASEREQUEST",null), 
			   new nlobjSearchColumn("custrecord_quantity","CUSTRECORD_PURCHASEREQUEST",null), 
			   new nlobjSearchColumn("custrecord_prastatus","CUSTRECORD_PURCHASEREQUEST",null),
			   new nlobjSearchColumn("custrecord_prmatldescription","CUSTRECORD_PURCHASEREQUEST",null),
			   new nlobjSearchColumn("custrecord_unit","CUSTRECORD_PURCHASEREQUEST",null),
			   new nlobjSearchColumn("custrecord_project","CUSTRECORD_PURCHASEREQUEST",null),
			   new nlobjSearchColumn("custrecord_deliverylocation","CUSTRECORD_PURCHASEREQUEST",null),
			   new nlobjSearchColumn("custrecord_currencyforpr","CUSTRECORD_PURCHASEREQUEST",null)
			]
			);
	if(obj_PR_Search_One_Record)
		{
			var obj_PR_Json_Body = {};
			var obj_PR_JSON_Line = {};
			var arr_PR_Lines = [];
			var s_PR_Remarks,d_PR_Date;
			var s_PR_Line_Item, i_PR_Line_Quantity,f_PR_Line_Amount,s_PR_Line_Description,s_PR_Lines_Status,s_Currency,s_Unit,s_Location,s_Project,s_Customer;
			
			for(var i_PROne_Index = 0 ;  i_PROne_Index < obj_PR_Search_One_Record.length ; i_PROne_Index++)
				{
					if(i_PROne_Index == 0)
						{
								s_PR_Remarks = obj_PR_Search_One_Record[i_PROne_Index].getValue('custrecord_requistionerremarks');
								d_PR_Date = obj_PR_Search_One_Record[i_PROne_Index].getValue('custrecord_requisitiondate');
								obj_PR_Json_Body ={
										PRNumber : i_Pr_Num,
										Date : d_PR_Date,
										Remarks : s_PR_Remarks
								}
						} //// End of if(i_PROne_Index == 0)
					s_PR_Line_Item = obj_PR_Search_One_Record[i_PROne_Index].getText("custrecord_prmatgrpcategory","CUSTRECORD_PURCHASEREQUEST",null);
					i_PR_Line_Quantity = obj_PR_Search_One_Record[i_PROne_Index].getValue("custrecord_quantity","CUSTRECORD_PURCHASEREQUEST",null);
					f_PR_Line_Amount = obj_PR_Search_One_Record[i_PROne_Index].getValue("custrecord_totalvalue","CUSTRECORD_PURCHASEREQUEST",null);
					s_PR_Line_Description = obj_PR_Search_One_Record[i_PROne_Index].getValue("custrecord_prmatldescription","CUSTRECORD_PURCHASEREQUEST",null);
					s_PR_Lines_Status =  obj_PR_Search_One_Record[i_PROne_Index].getText("custrecord_prastatus","CUSTRECORD_PURCHASEREQUEST",null);
					s_Currency=obj_PR_Search_One_Record[i_PROne_Index].getText("custrecord_currencyforpr","CUSTRECORD_PURCHASEREQUEST",null);
					s_Unit = obj_PR_Search_One_Record[i_PROne_Index].getText("custrecord_unit","CUSTRECORD_PURCHASEREQUEST",null);
					s_Location =  obj_PR_Search_One_Record[i_PROne_Index].getText("custrecord_deliverylocation","CUSTRECORD_PURCHASEREQUEST",null);
					s_Project = obj_PR_Search_One_Record[i_PROne_Index].getText("custrecord_project","CUSTRECORD_PURCHASEREQUEST",null);
					obj_PR_JSON_Line = {
							Item : s_PR_Line_Item,
							Description : s_PR_Line_Description,
							Quantity : i_PR_Line_Quantity,
							Unit : s_Unit,
							Currency : s_Currency,
							Amount : f_PR_Line_Amount,
							Project : s_Project,
							Customer : s_Customer,
							Location : s_Location,
							Status : s_PR_Lines_Status
					}
					arr_PR_Lines.push(obj_PR_JSON_Line);
				}/// End of for(var i_PROne_Index = 0 ;  i_PROne_Index < obj_PR_Search_One_Record.length ; i_PROne_Index++)
			obj_getStatusofOnePR_Response = {
					Body : obj_PR_Json_Body,
					Lines : arr_PR_Lines
			};
			return obj_getStatusofOnePR_Response;
		} /// End of if(obj_PR_Search_One_Record)
	else
		{
			return 'Please Enter Proper PR Number';
		} /// /// End of Else	if(obj_PR_Search_One_Record)
	} ////  End of try function getStatusofOnePR(i_Pr_Num)
	catch(e)
	{
		nlapiLogExecution('DEBUG', 'Exception in  getStatusofOnePR==', e);
		return e;
	} /// End of catch function getStatusofOnePR(i_Pr_Num)
}/// End of function getStatusofOnePR(i_Pr_Num)
////////Function for Getting Status of one PR ------ END

////// Function for Getting Status of All PR ---- BEGIN
function getStatusofAllPR(i_Employee_Internal_Id)
{
	try
	{
		nlapiLogExecution('DEBUG', 'getStatusofAllPR==',i_Employee_Internal_Id);
		var arr_Filters_PR_Search = 	[
		              				   ["isinactive","is","F"], 
		            				   "AND", 
		            				   ["custrecord_requisitionname","anyof",parseInt(i_Employee_Internal_Id)]
		            				];
		var arr_Columns_PR_Search = [
		                             	new nlobjSearchColumn("created").setSort(true),
									   new nlobjSearchColumn("custrecord_prno"), 
									   new nlobjSearchColumn("custrecord_requisitiondate"), 
									   new nlobjSearchColumn("custrecord_requisitionname"), 
									   new nlobjSearchColumn("custrecord_requistionerremarks"), 
									   new nlobjSearchColumn("custrecord_prmatgrpcategory","CUSTRECORD_PURCHASEREQUEST",null), 
									   new nlobjSearchColumn("custrecord_prastatus","CUSTRECORD_PURCHASEREQUEST",null), 
									   new nlobjSearchColumn("custrecord_totalvalue","CUSTRECORD_PURCHASEREQUEST",null), 
									   new nlobjSearchColumn("custrecord_quantity","CUSTRECORD_PURCHASEREQUEST",null), 
									   new nlobjSearchColumn("custrecord_prastatus","CUSTRECORD_PURCHASEREQUEST",null),
									   new nlobjSearchColumn("custrecord_prmatldescription","CUSTRECORD_PURCHASEREQUEST",null),
									   new nlobjSearchColumn("custrecord_unit","CUSTRECORD_PURCHASEREQUEST",null),
									   new nlobjSearchColumn("custrecord_project","CUSTRECORD_PURCHASEREQUEST",null),
									   new nlobjSearchColumn("custrecord_deliverylocation","CUSTRECORD_PURCHASEREQUEST",null),
									   new nlobjSearchColumn("custrecord_currencyforpr","CUSTRECORD_PURCHASEREQUEST",null)
									];
	
		
		var obj_PR_Search_EmployeeWise = searchRecord('customrecord__prpurchaserequest',null,arr_Filters_PR_Search,arr_Columns_PR_Search);
			if(obj_PR_Search_EmployeeWise)
				{
						//nlapiLogExecution('DEBUG',' obj_PR_Search_EmployeeWise.length==',  obj_PR_Search_EmployeeWise.length );
						var i_PR_Number ;
						var s_PR_Remarks,d_PR_Date;
						var s_PR_Line_Item, i_PR_Line_Quantity,f_PR_Line_Amount,s_PR_Line_Description,s_PR_Lines_Status,s_Currency,s_Unit,s_Location,s_Project,s_Customer;
						var arr_PR_All_Data = [];
						var obj_PR_Json_Body = {};
						var obj_PR_JSON_Line = {};
						var arr_PR_Lines = [];
						var i_Measure_Counter = 0;
						for(var i_PR_Emp_Index = 0 ; i_PR_Emp_Index <  obj_PR_Search_EmployeeWise.length ; i_PR_Emp_Index++) /// main search for getting PRs of all employee
							{
									if(i_PR_Emp_Index == 0)
										{		
												//i_Measure_Counter++;
												s_PR_Remarks = obj_PR_Search_EmployeeWise[i_PR_Emp_Index].getValue('custrecord_requistionerremarks');
												d_PR_Date = obj_PR_Search_EmployeeWise[i_PR_Emp_Index].getValue('custrecord_requisitiondate');
												i_PR_Number = obj_PR_Search_EmployeeWise[i_PR_Emp_Index].getValue('custrecord_prno');
												obj_PR_Json_Body = {
												PRNumber : i_PR_Number,
												Date : d_PR_Date,
												Remarks : s_PR_Remarks}
												s_PR_Line_Item = obj_PR_Search_EmployeeWise[i_PR_Emp_Index].getText("custrecord_prmatgrpcategory","CUSTRECORD_PURCHASEREQUEST",null);
												i_PR_Line_Quantity = obj_PR_Search_EmployeeWise[i_PR_Emp_Index].getValue("custrecord_quantity","CUSTRECORD_PURCHASEREQUEST",null);
												f_PR_Line_Amount = obj_PR_Search_EmployeeWise[i_PR_Emp_Index].getValue("custrecord_totalvalue","CUSTRECORD_PURCHASEREQUEST",null);
												s_PR_Line_Description = obj_PR_Search_EmployeeWise[i_PR_Emp_Index].getValue("custrecord_prmatldescription","CUSTRECORD_PURCHASEREQUEST",null);
												s_PR_Lines_Status =  obj_PR_Search_EmployeeWise[i_PR_Emp_Index].getText("custrecord_prastatus","CUSTRECORD_PURCHASEREQUEST",null);
												s_Currency=obj_PR_Search_EmployeeWise[i_PR_Emp_Index].getText("custrecord_currencyforpr","CUSTRECORD_PURCHASEREQUEST",null);
												s_Unit = obj_PR_Search_EmployeeWise[i_PR_Emp_Index].getText("custrecord_unit","CUSTRECORD_PURCHASEREQUEST",null);
												s_Location =  obj_PR_Search_EmployeeWise[i_PR_Emp_Index].getText("custrecord_deliverylocation","CUSTRECORD_PURCHASEREQUEST",null);
												s_Project = obj_PR_Search_EmployeeWise[i_PR_Emp_Index].getText("custrecord_project","CUSTRECORD_PURCHASEREQUEST",null);
													obj_PR_JSON_Line = {
															Item : s_PR_Line_Item,
															Description : s_PR_Line_Description,
															Quantity : i_PR_Line_Quantity,
															Unit : s_Unit,
															Currency : s_Currency,
															Amount : f_PR_Line_Amount,
															Project : s_Project,
															Customer : s_Customer,
															Location : s_Location,
															Status : s_PR_Lines_Status}
												arr_PR_Lines.push(obj_PR_JSON_Line);
										} /// End of if(i_PR_Emp_Index == 0)
									else
										{
												var i_Current_PR_Number  = obj_PR_Search_EmployeeWise[i_PR_Emp_Index].getValue('custrecord_prno');
												var i_Previous_PR_Number = obj_PR_Search_EmployeeWise[i_PR_Emp_Index - 1].getValue('custrecord_prno');
											//	nlapiLogExecution('DEBUG',' i_Current_PR_Number==',  i_Current_PR_Number );
											//	nlapiLogExecution('DEBUG',' i_Previous_PR_Number==',  i_Previous_PR_Number );
		
												if( i_Current_PR_Number == i_Previous_PR_Number) /// Fetch only line level items
													{
													
													s_PR_Line_Item = obj_PR_Search_EmployeeWise[i_PR_Emp_Index].getText("custrecord_prmatgrpcategory","CUSTRECORD_PURCHASEREQUEST",null);
													i_PR_Line_Quantity = obj_PR_Search_EmployeeWise[i_PR_Emp_Index].getValue("custrecord_quantity","CUSTRECORD_PURCHASEREQUEST",null);
													f_PR_Line_Amount = obj_PR_Search_EmployeeWise[i_PR_Emp_Index].getValue("custrecord_totalvalue","CUSTRECORD_PURCHASEREQUEST",null);
													s_PR_Line_Description = obj_PR_Search_EmployeeWise[i_PR_Emp_Index].getValue("custrecord_prmatldescription","CUSTRECORD_PURCHASEREQUEST",null);
													s_PR_Lines_Status =  obj_PR_Search_EmployeeWise[i_PR_Emp_Index].getText("custrecord_prastatus","CUSTRECORD_PURCHASEREQUEST",null);
													s_Currency=obj_PR_Search_EmployeeWise[i_PR_Emp_Index].getText("custrecord_currencyforpr","CUSTRECORD_PURCHASEREQUEST",null);
													s_Unit = obj_PR_Search_EmployeeWise[i_PR_Emp_Index].getText("custrecord_unit","CUSTRECORD_PURCHASEREQUEST",null);
													s_Location =  obj_PR_Search_EmployeeWise[i_PR_Emp_Index].getText("custrecord_deliverylocation","CUSTRECORD_PURCHASEREQUEST",null);
													s_Project = obj_PR_Search_EmployeeWise[i_PR_Emp_Index].getText("custrecord_project","CUSTRECORD_PURCHASEREQUEST",null);
														obj_PR_JSON_Line = {
																Item : s_PR_Line_Item,
																Description : s_PR_Line_Description,
																Quantity : i_PR_Line_Quantity,
																Unit : s_Unit,
																Currency : s_Currency,
																Amount : f_PR_Line_Amount,
																Project : s_Project,
																Customer : s_Customer,
																Location : s_Location,
																Status : s_PR_Lines_Status}
															arr_PR_Lines.push(obj_PR_JSON_Line);
														
													} //// End of if( i_Current_PR_Number == i_Previous_PR_Number)
												else
													{
															i_Measure_Counter++;
														//	nlapiLogExecution('DEBUG',' i_Measure_Counter==',  i_Measure_Counter );
															obj_getStatusofOnePR_Response = {
																	Body : obj_PR_Json_Body,
																	Lines : arr_PR_Lines
															};	
															arr_PR_All_Data.push(obj_getStatusofOnePR_Response)	;
															
															if(i_Measure_Counter >= 10)
																{
																	break;
																} ////if(i_Measure_Counter >= 10)
															arr_PR_Lines = [];
															obj_PR_Json_Body = {}
															obj_PR_JSON_Line = {}
															/// refreshing the values.
															
															s_PR_Remarks = obj_PR_Search_EmployeeWise[i_PR_Emp_Index].getValue('custrecord_requistionerremarks');
															d_PR_Date = obj_PR_Search_EmployeeWise[i_PR_Emp_Index].getValue('custrecord_requisitiondate');
															i_PR_Number = obj_PR_Search_EmployeeWise[i_PR_Emp_Index].getValue('custrecord_prno');
															obj_PR_Json_Body ={
															PRNumber : i_PR_Number,
															Date : d_PR_Date,
															Remarks : s_PR_Remarks}
															s_PR_Line_Item = obj_PR_Search_EmployeeWise[i_PR_Emp_Index].getText("custrecord_prmatgrpcategory","CUSTRECORD_PURCHASEREQUEST",null);
															i_PR_Line_Quantity = obj_PR_Search_EmployeeWise[i_PR_Emp_Index].getValue("custrecord_quantity","CUSTRECORD_PURCHASEREQUEST",null);
															f_PR_Line_Amount = obj_PR_Search_EmployeeWise[i_PR_Emp_Index].getValue("custrecord_totalvalue","CUSTRECORD_PURCHASEREQUEST",null);
															s_PR_Line_Description = obj_PR_Search_EmployeeWise[i_PR_Emp_Index].getValue("custrecord_prmatldescription","CUSTRECORD_PURCHASEREQUEST",null);
															s_PR_Lines_Status =  obj_PR_Search_EmployeeWise[i_PR_Emp_Index].getText("custrecord_prastatus","CUSTRECORD_PURCHASEREQUEST",null);
															s_Currency=obj_PR_Search_EmployeeWise[i_PR_Emp_Index].getText("custrecord_currencyforpr","CUSTRECORD_PURCHASEREQUEST",null);
															s_Unit = obj_PR_Search_EmployeeWise[i_PR_Emp_Index].getText("custrecord_unit","CUSTRECORD_PURCHASEREQUEST",null);
															s_Location =  obj_PR_Search_EmployeeWise[i_PR_Emp_Index].getText("custrecord_deliverylocation","CUSTRECORD_PURCHASEREQUEST",null);
															s_Project = obj_PR_Search_EmployeeWise[i_PR_Emp_Index].getText("custrecord_project","CUSTRECORD_PURCHASEREQUEST",null);
																obj_PR_JSON_Line = {
																		Item : s_PR_Line_Item,
																		Description : s_PR_Line_Description,
																		Quantity : i_PR_Line_Quantity,
																		Unit : s_Unit,
																		Currency : s_Currency,
																		Amount : f_PR_Line_Amount,
																		Project : s_Project,
																		Customer : s_Customer,
																		Location : s_Location,
																		Status : s_PR_Lines_Status}
																arr_PR_Lines.push(obj_PR_JSON_Line);
													} /// End of  else of if( i_Current_PR_Number == i_Previous_PR_Number)
										} //// End of else if(i_PR_Emp_Index == 0)
							} /// End of for(var i_PR_Emp_Index = 0 ; i_PR_Emp_Index < 10 ; i_PR_Emp_Index++)
				} /// End of if(obj_PR_Search_EmployeeWise)
			else
				{
					return 'Employee has no previous PR Requests';
				}// End of  else if(obj_PR_Search_EmployeeWise)
		return arr_PR_All_Data;
	} /// End of try for function getStatusofAllPR
	catch(e)
	{
		nlapiLogExecution('DEBUG', 'Exception in  getStatusofAllPR==', e);
		return e;
	} /// End of catch for function getStatusofAllPR
}///End of function getStatusofAllPR(s_Email_Id)
//////////// Function for Getting Status of All PR ---- END

////Function to create the PR ----- BEGIN
function createPRRequest(dataIn,i_Employee_Internal_Id)
{
	var s_Return_Message;
	try
	{
		nlapiLogExecution('DEBUG','createPRRequest', 'createPRRequest');
		//nlapiLogExecution('DEBUG', 'i_Employee_Internal_Id==', i_Employee_Internal_Id);
		var d_Date_Creation = dataIn.Data.Body.Date;
	//	nlapiLogExecution('DEBUG', 'd_Date_Creation==', d_Date_Creation);
		var s_PR_Body_Remarks = dataIn.Data.Body.Remarks;
		//nlapiLogExecution('DEBUG', 's_PR_Body_Remarks==', s_PR_Body_Remarks);
		var s_Employee_Level = nlapiLookupField('employee',i_Employee_Internal_Id, 'employeestatus',true);
	//	nlapiLogExecution('DEBUG', 's_Employee_Level==', s_Employee_Level);
		var arr_Levels_Employee = ['0','1','1A','1B','2','2A','2B','3','3A','3B','4','4A','4B','5','5A','5B' ,'6','6A','6B' ]
		var i_Index_Employee = arr_Levels_Employee.indexOf(s_Employee_Level);
		nlapiLogExecution('DEBUG', 'i_Index_Employee==', i_Index_Employee);
		if(i_Index_Employee == -1 )
			{
					s_Return_Message = 'You are not Authorized for PR Creation as your level is above 6';
					return s_Return_Message;
			}///End of if(i_Employee_Level > 6)
		
		
		var obj_PR_Record_Create = nlapiCreateRecord('customrecord__prpurchaserequest');   ///// Creation of record
//------------------------------------------------------------------------------------------------------------------------------///		
		///////// Setting the body level Fields ----- BEGIN
				obj_PR_Record_Create.setFieldValue('custrecord_requisitiondate',d_Date_Creation); /// Date
				obj_PR_Record_Create.setFieldValue('custrecord_requisitionname',i_Employee_Internal_Id ); // Created by
				if(s_PR_Body_Remarks)
					{
						obj_PR_Record_Create.setFieldValue('custrecord_requistionerremarks', s_PR_Body_Remarks); //// Body level Remarks
					}/// End of if(s_PR_Body_Remarks)
		///////// Setting the body level Fields ----- END
//------------------------------------------------------------------------------------------------------------------------------///					
				var arr_Lines_Item = dataIn.Data.Lines
				var i_PR_Item_Length = arr_Lines_Item.length; /// getting length of line items length
				i_PR_Item_Length = parseInt(i_PR_Item_Length);
				nlapiLogExecution('DEBUG', 'i_PR_Item_Length==', i_PR_Item_Length);
				if(i_PR_Item_Length == 0)
					{
						s_Return_Message = 'Please Enter atleast a one line item';
						return s_Return_Message;
					} ///if(i_PR_Item_Length == 0)
				for(var i_Line_Item_Index = 0 ; i_Line_Item_Index < i_PR_Item_Length ; i_Line_Item_Index++ )
					{
						nlapiLogExecution('DEBUG','i_Line_Item_Index==', i_Line_Item_Index);
					  //-----------------------------------------------//
					/// Validating the line level inputs ----- Begin
							var i_Item_PR = dataIn.Data.Lines[i_Line_Item_Index].Item;
							nlapiLogExecution('DEBUG','i_Item_PR==', i_Item_PR);
							if(!i_Item_PR)
								{
									s_Return_Message = 'Please Enter Item For Request No : '  + (i_Line_Item_Index + 1);
									return s_Return_Message;
								} //// if(!i_Item_PR)
							
							var s_Item_Description = dataIn.Data.Lines[i_Line_Item_Index].Description;
							nlapiLogExecution('DEBUG','s_Item_Description==', s_Item_Description);
							if(!s_Item_Description)
								{
									s_Return_Message = 'Please Enter Item Description For Request No : '  + (i_Line_Item_Index + 1);
									return s_Return_Message;
								} //// if(!s_Item_Description)	
							
							var i_Item_Quantity = dataIn.Data.Lines[i_Line_Item_Index].Quantity;
							nlapiLogExecution('DEBUG','i_Item_Quantity==', i_Item_Quantity);
							if ( (!i_Item_Quantity) || (i_Item_Quantity <= 0))
								{
									s_Return_Message = 'Please Enter Item Quantity or check Number entered For Request No : '  + (i_Line_Item_Index + 1);
									return s_Return_Message;
								} ////if ( (!i_Item_Quantity) || (i_Item_Quantity <= 0))
						
							var i_Unit_Item = dataIn.Data.Lines[i_Line_Item_Index].Unit;
							nlapiLogExecution('DEBUG','i_Unit_Item==', i_Unit_Item);
							if(!i_Unit_Item)
								{
									s_Return_Message = 'Please Enter Item Unit For Request No : '  + (i_Line_Item_Index + 1);
									return s_Return_Message;
								} //// if(!i_Unit_Item)	
							
							var i_Currency = dataIn.Data.Lines[i_Line_Item_Index].Currency;
							nlapiLogExecution('DEBUG','i_Currency==', i_Currency);
							if(!i_Currency)
								{
									s_Return_Message = 'Please Enter Item Currency For Request No : '  + (i_Line_Item_Index + 1);
									return s_Return_Message;
								} //// if(!i_Currency)	
							 
							
							var f_Estimated_Value = dataIn.Data.Lines[i_Line_Item_Index].Estimated_Value;
							nlapiLogExecution('DEBUG','f_Estimated_Value==', f_Estimated_Value);
							if ( (!f_Estimated_Value) || (f_Estimated_Value <= 0))
							{
								s_Return_Message = 'Please Enter Item Estimated Value or check Value entered For Request No : '  + (i_Line_Item_Index + 1);
								return s_Return_Message;
							} ////if ( (!f_Estimated_Value) || (f_Estimated_Value <= 0))
							
							var i_Project_Item = dataIn.Data.Lines[i_Line_Item_Index].Project;
							nlapiLogExecution('DEBUG','i_Project_Item==', i_Project_Item);
							if(!i_Project_Item)
								{
									s_Return_Message = 'Please Enter Project For Request No : '  + (i_Line_Item_Index + 1);
									return s_Return_Message;
								} //// if(!i_Project_Item)	
							
							var cb_Check_Billable = dataIn.Data.Lines[i_Line_Item_Index].Billable;
							nlapiLogExecution('DEBUG','cb_Check_Billable==', cb_Check_Billable);
							
							
							var i_Delivery_Location = dataIn.Data.Lines[i_Line_Item_Index].Delivery_Location;
							nlapiLogExecution('DEBUG','i_Delivery_Location==', i_Delivery_Location);
							if(!i_Delivery_Location)
								{
									s_Return_Message = 'Please Enter Delivery Location For Request No : '  + (i_Line_Item_Index + 1);
									return s_Return_Message;
								} //// if(!i_Delivery_Location)	
							
							/// Validating the line level inputs ---- End
							  //-----------------------------------------------//
							
							///------- Internally calculated values ---------  Begin
							var i_Item_Category = nlapiLookupField('item',i_Item_PR,'custitem_itemcategory');
							nlapiLogExecution('DEBUG','i_Item_Category==', i_Item_Category);
							var i_Sr_No = parseInt(i_Line_Item_Index)+1;
							var f_Total_Estimated_Value = parseInt(i_Item_Quantity) * parseFloat(f_Estimated_Value);
							f_Total_Estimated_Value = parseFloat(f_Total_Estimated_Value).toFixed(2);
							nlapiLogExecution('DEBUG','f_Total_Estimated_Value==', f_Total_Estimated_Value);

							var obj_Project_Search = nlapiSearchRecord("job",null,
									[
									   ["internalid","anyof",i_Project_Item]
									], 
									[
									   new nlobjSearchColumn("customer"), 
									   new nlobjSearchColumn("jobtype"), 
									   new nlobjSearchColumn("custentity_practice")
									]
									);
							if(obj_Project_Search)
								{
										var s_Customer_Name = obj_Project_Search[0].getValue('customer');
										var i_Project_Type = obj_Project_Search[0].getValue('jobtype');
										nlapiLogExecution('DEBUG','i_Project_Type==', i_Project_Type);
										var i_Project_Practice = obj_Project_Search[0].getValue('custentity_practice');
										var i_Practice_Head = nlapiLookupField('department',i_Project_Practice, 'custrecord_practicehead');
										nlapiLogExecution('DEBUG','i_Practice_Head==', i_Practice_Head);
										var i_Pr_Item_Status ;
										if(i_Project_Type == 2) /// If Project is External
											{
												i_Pr_Item_Status = 30; /// Pending Delivery Manager Approval
											} // End of if(i_Project_Type == 2) /// If Project is External
										else // Project is Internal
											{
													i_Pr_Item_Status = 2; /// Pending for Sub practice Head Approval
											} /// End of else of if if(i_Project_Type == 2) /// If Project is External
								} /////if(obj_Project_Search)
							///------- Internally calculated values ---------  		End
								//// Setting the line level items ------------------------------ BEGIN
							   // obj_PR_Record_Create.selectLineItem('recmachcustrecord_purchaserequest', i_Sr_No)
								obj_PR_Record_Create.selectNewLineItem('recmachcustrecord_purchaserequest');
								obj_PR_Record_Create.setCurrentLineItemValue('recmachcustrecord_purchaserequest', 'custrecord_employee',i_Employee_Internal_Id );
							//	obj_PR_Record_Create.setCurrentLineItemValue('recmachcustrecord_purchaserequest', 'custrecord73',i_Employee_Internal_Id );
								obj_PR_Record_Create.setCurrentLineItemValue('recmachcustrecord_purchaserequest', 'custrecord_prlineitemno',i_Sr_No ); // Sr No
								obj_PR_Record_Create.setCurrentLineItemValue('recmachcustrecord_purchaserequest', 'custrecord_prmatgrpcategory', i_Item_PR); /// Item
								obj_PR_Record_Create.setCurrentLineItemValue('recmachcustrecord_purchaserequest', 'custrecord_prmatldescription',s_Item_Description ); // Description
								obj_PR_Record_Create.setCurrentLineItemValue('recmachcustrecord_purchaserequest', 'custrecord_itemcategory',i_Item_Category ); /// Item Category
								obj_PR_Record_Create.setCurrentLineItemValue('recmachcustrecord_purchaserequest', 'custrecord_quantity', i_Item_Quantity);// Quantity
								obj_PR_Record_Create.setCurrentLineItemValue('recmachcustrecord_purchaserequest', 'custrecord_unit',i_Unit_Item ); // Unit
								obj_PR_Record_Create.setCurrentLineItemValue('recmachcustrecord_purchaserequest', 'custrecord_currencyforpr',i_Currency );//Currency
								obj_PR_Record_Create.setCurrentLineItemValue('recmachcustrecord_purchaserequest', 'custrecord_valueperunit', f_Estimated_Value); /// Estimated Value
								obj_PR_Record_Create.setCurrentLineItemValue('recmachcustrecord_purchaserequest', 'custrecord_project', i_Project_Item); /// Project
								obj_PR_Record_Create.setCurrentLineItemValue('recmachcustrecord_purchaserequest', 'custrecord_billabletocustomer', cb_Check_Billable);//Billable to Customer
								obj_PR_Record_Create.setCurrentLineItemValue('recmachcustrecord_purchaserequest', 'custrecord_deliverylocation', i_Delivery_Location); /// Delivery location
								obj_PR_Record_Create.setCurrentLineItemValue('recmachcustrecord_purchaserequest', 'custrecord_customerforpritem',s_Customer_Name ); /// Customer
								obj_PR_Record_Create.setCurrentLineItemValue('recmachcustrecord_purchaserequest', 'custrecord_prastatus', i_Pr_Item_Status); // PR item Status
								obj_PR_Record_Create.setCurrentLineItemValue('recmachcustrecord_purchaserequest', 'custrecord_subpracticehead',i_Practice_Head ); /// PR item Sub Pr head
								obj_PR_Record_Create.setCurrentLineItemValue('recmachcustrecord_purchaserequest', 'custrecord_totalvalue', f_Total_Estimated_Value); /// Total estimated value
							    obj_PR_Record_Create.commitLineItem('recmachcustrecord_purchaserequest');
							  
					/*	obj_PR_Record_Create.setLineItemValue('recmachcustrecord_purchaserequest', 'custrecord_prlineitemno', i_Sr_No,i_Sr_No ); // Sr No
						obj_PR_Record_Create.setLineItemValue('recmachcustrecord_purchaserequest', 'custrecord_prmatgrpcategory', i_Sr_No, i_Item_PR); /// Item
						obj_PR_Record_Create.setLineItemValue('recmachcustrecord_purchaserequest', 'custrecord_prmatldescription', i_Sr_No,s_Item_Description ); // Description
						obj_PR_Record_Create.setLineItemValue('recmachcustrecord_purchaserequest', 'custrecord_itemcategory', i_Sr_No,i_Item_Category ); /// Item Category
						obj_PR_Record_Create.setLineItemValue('recmachcustrecord_purchaserequest', 'custrecord_quantity', i_Sr_No, i_Item_Quantity);// Quantity
						obj_PR_Record_Create.setLineItemValue('recmachcustrecord_purchaserequest', 'custrecord_unit', i_Sr_No,i_Unit_Item ); // Unit
						obj_PR_Record_Create.setLineItemValue('recmachcustrecord_purchaserequest', 'custrecord_currencyforpr', i_Sr_No,i_Currency );//Currency
						obj_PR_Record_Create.setLineItemValue('recmachcustrecord_purchaserequest', 'custrecord_valueperunit', i_Sr_No, f_Estimated_Value); /// Estimated Value
						obj_PR_Record_Create.setLineItemValue('recmachcustrecord_purchaserequest', 'custrecord_project', i_Sr_No, i_Project_Item); /// Project
						obj_PR_Record_Create.setLineItemValue('recmachcustrecord_purchaserequest', 'custrecord_billabletocustomer', i_Sr_No, cb_Check_Billable);//Billable to Customer
						obj_PR_Record_Create.setLineItemValue('recmachcustrecord_purchaserequest', 'custrecord_deliverylocation', i_Sr_No, i_Delivery_Location); /// Delivery location
						obj_PR_Record_Create.setLineItemValue('recmachcustrecord_purchaserequest', 'custrecord_customerforpritem', i_Sr_No,s_Customer_Name ); /// Customer
						obj_PR_Record_Create.setLineItemValue('recmachcustrecord_purchaserequest', 'custrecord_prastatus', i_Sr_No, i_Pr_Item_Status); // PR item Status
						obj_PR_Record_Create.setLineItemValue('recmachcustrecord_purchaserequest', 'custrecord_subpracticehead', i_Sr_No,i_Practice_Head ); /// PR item Sub Pr head
						obj_PR_Record_Create.setLineItemValue('recmachcustrecord_purchaserequest', 'custrecord_totalvalue', i_Sr_No, f_Total_Estimated_Value); /// Total estimated value*/
					
							//// Setting the line level items ------------------------------ END
					} /// End of for(var i_Line_Item_Index = 0 ; i_Line_Item_Index < i_PR_Item_Length ; i_Line_Item_Index++ )
				
				var i_PR_Internal_Id = nlapiSubmitRecord(obj_PR_Record_Create, true,false);
			//	nlapiLogExecution('DEBUG','i_PR_Internal_Id==', i_PR_Internal_Id);
				var i_Response_PR_Number = nlapiLookupField('customrecord__prpurchaserequest',i_PR_Internal_Id ,'custrecord_prno');
				nlapiLogExecution('DEBUG','i_Response_PR_Number==', i_Response_PR_Number);		
				return {
					PRNumber : i_Response_PR_Number
				};
	} /// End of try for function createPRRequest
	catch(e)
	{
		nlapiLogExecution('DEBUG', 'Exception in  createPRRequest==', e);
		return e;
	} //// End of catch createPRRequest
}///// End of function createPRRequest
////Function to create the PR ------ END