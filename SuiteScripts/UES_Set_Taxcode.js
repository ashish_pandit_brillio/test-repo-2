//For setting the tax code of Canada Subsidiary

function beforeSubmit()
{
	try
	{
		if(type == 'create' || type == 'view')
		{
			var empSubsidiary = nlapiLookupField('employee',nlapiGetFieldValue('entity'),'subsidiary');
		
			if(empSubsidiary == parseInt(10))
			{
				var line = nlapiGetLineItemCount('expense');
			
				for(var i = 1; i <= line; i++)
				{
					nlapiSetLineItemValue('expense', 'taxcode', i, 2901);
				}
			}	
		}
	}
	
	catch (e)
	{
		nlapiLogExecution('Debug','Error',e);
		throw e;
	}
}