/**
 * Module Description
 * 
 * Version Date Author Remarks 1.00 Apr 17 2015 Nitish Mishra
 * 
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your
 * script deployment.
 * 
 * @appliedtorecord recordType
 * 
 * @param {String}
 *        type Operation types: create, edit, delete, xedit, approve, cancel,
 *        reject (SO, ER, Time Bill, PO & RMA only) pack, ship (IF only)
 *        dropship, specialorder, orderitems (PO only) paybills (vendor
 *        payments)
 * @returns {Void}
 */
function scheduled() {

    try {
        var cols = [];
        cols.push(new nlobjSearchColumn('internalid'));
        var loadSearchRes = nlapiSearchRecord('employee', 'customsearch2500', null, cols);

        if (loadSearchRes) {

            for (var i = 0; i < loadSearchRes.length; i++) {

                // get the person type
                //var employeeId = loadSearchRes[i].getValue('internalid');
                var mailContent = null;
                var employeeDetails = nlapiLookupField('employee', loadSearchRes[i].getValue('internalid'), [
                    'firstname', 'email', 'custentity_persontype', 'department'
                ]);

                // if email id contains "@brillio.com" send the salaried mail,
                // else send the contigent worker mail

                // get mail content as per the person type
              //  if (doesEmailContainsBrillio(employeeDetails.email)) {
                    mailContent = getSalariedMailTemplate(employeeDetails);
              //  }

                // send email to the new joinee
                if (isNotEmpty(mailContent)) {
                    var file_obj = nlapiLoadFile('857976');
                    nlapiSendEmail(129799, employeeDetails.email,mailContent.Subject, mailContent.Body, null, null,{entity: loadSearchRes[i].getValue('internalid')}, file_obj);
                    //sendNewJoineeEmail(employeeId);
                }
				/*else {
                    throw "Invalid Person Type";
                }*/
            }
        }
    } catch (err) {
        nlapiLogExecution('error', 'userEventAfterSubmit', err);
        throw err;
    }
}

function doesEmailContainsBrillio(email) {

    email = email.toLowerCase();
    return email.indexOf("@brillio.com") > -1;
}

function getSalariedMailTemplate(employeeDetails) {
    
    var htmlText = '<style> p,li {font-size : 11; font-family : "verdana"}</style>';
    htmlText += '<p>Hi ' + employeeDetails.firstname + ',<p>';
    htmlText +=
        
        '<p>Please visit the below link to update your Skills and certification.</p>' +
        '<a href=https://system.na1.netsuite.com/app/site/hosting/scriptlet.nl?script=1657&deploy=1>Skills and certification</a>' +
        '<br/><p>Thanks and All The Best,<br/>' + 'Fulfillment Team</p>';

    return {
        Subject: 'Skill Update',
        Body: addMailTemplate(htmlText)
    };
}