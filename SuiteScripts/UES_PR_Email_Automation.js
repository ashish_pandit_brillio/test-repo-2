/**
 * @author Shweta
 */
// BEGIN SCRIPT DESCRIPTION BLOCK ==================================
{
	/*
	 * Script Name: UES_PR_Email_Automation.js Author : Shweta Chopde Date : 20
	 * Aug 2014 Description: Trigger an email on create on PR
	 * 
	 * 
	 * Script Modification Log:
	 *  -- Date -- -- Modified By -- --Requested By-- -- Description --
	 * 
	 * 
	 * 
	 * Below is a summary of the process controls enforced by this script file.
	 * The control logic is described more fully, below, in the appropriate
	 * function headers and code blocks.
	 * 
	 * 
	 * BEFORE LOAD - beforeLoadRecord(type)
	 * 
	 * 
	 * 
	 * BEFORE SUBMIT - beforeSubmitRecord(type)
	 * 
	 * 
	 * AFTER SUBMIT - afterSubmitRecord(type)
	 * 
	 * 
	 * 
	 * SUB-FUNCTIONS - The following sub-functions are called by the above core
	 * functions in order to maintain code modularization:
	 *  - NOT USED
	 * 
	 */
}
// END SCRIPT DESCRIPTION BLOCK ====================================

// BEGIN GLOBAL VARIABLE BLOCK =====================================
{
	// Initialize any Global Variables, in particular, debugging variables...

}
// END GLOBAL VARIABLE BLOCK =======================================

// BEGIN BEFORE LOAD ==================================================

function beforeLoadRecord(type) {

	/*
	 * On before load:
	 *  - EXPLAIN THE PURPOSE OF THIS FUNCTION
	 *  -
	 * 
	 * 
	 * FIELDS USED:
	 * 
	 * --Field Name-- --ID--
	 * 
	 * 
	 */

	// LOCAL VARIABLES
	// BEFORE LOAD CODE BODY

	return true;

}

// END BEFORE LOAD ====================================================

// BEGIN BEFORE SUBMIT ================================================

function beforeSubmitRecord(type) {
	/*
	 * On before submit:
	 *  - PURPOSE -
	 * 
	 * FIELDS USED:
	 * 
	 * --Field Name-- --ID--
	 * 
	 * 
	 */

	// LOCAL VARIABLES

	// BEFORE SUBMIT CODE BODY

	return true;

}

// END BEFORE SUBMIT ==================================================

// BEGIN AFTER SUBMIT =============================================

function afterSubmit_email_trigger(type) {

	var i_practice_head_email;
	var i_practice_head_name;

	if (type == 'create') {
		try {
			var a_practice_head_array = new Array();
			var i_recordID = nlapiGetRecordId();
			var s_record_type = nlapiGetRecordType();
			var i_user = nlapiGetUser();

			if (_logValidation(i_recordID) && _logValidation(s_record_type)) {
				var o_recordOBJ = nlapiLoadRecord(s_record_type, i_recordID);

				if (_logValidation(o_recordOBJ)) {
					var i_PR_no = o_recordOBJ.getFieldValue('custrecord_prno');

					var i_requestor = o_recordOBJ
					        .getFieldText('custrecord_requisitionname');

					var i_line_count = o_recordOBJ
					        .getLineItemCount('recmachcustrecord_purchaserequest');
					nlapiLogExecution('DEBUG', 'afterSubmit_email_trigger',
					        ' Line Count -->' + i_line_count);

					nlapiLogExecution('DEBUG', 'afterSubmit_email_trigger',
					        ' Requestor -->' + i_requestor);

					nlapiLogExecution('DEBUG', 'afterSubmit_email_trigger',
					        ' PR # -->' + i_PR_no);

					if (_logValidation(i_line_count)) {
						for (var i = 1; i <= i_line_count; i++) {
							var i_practice_head = o_recordOBJ.getLineItemValue(
							        'recmachcustrecord_purchaserequest',
							        'custrecord_subpracticehead', i)
							nlapiLogExecution('DEBUG',
							        'afterSubmit_email_trigger',
							        ' Practice Head -->' + i_practice_head);

							a_practice_head_array.push(i_practice_head);

						}// Loop
					}// Line Count

					a_practice_head_array = removearrayduplicate(a_practice_head_array);
					nlapiLogExecution('DEBUG', 'afterSubmit_email_trigger',
					        ' Practice Head Array -->' + a_practice_head_array);

					for (var b = 0; b < a_practice_head_array.length; b++) {
						var a_split_array = new Array();
						var a_employee_array = get_employee_details(a_practice_head_array[b])

						if (_logValidation(a_employee_array)) {
							a_split_array = a_employee_array[0].split('####')

							i_practice_head_email = a_split_array[0]

							i_practice_head_name = a_split_array[1]

						}// Employee Array

						nlapiLogExecution('DEBUG', 'afterSubmit_email_trigger',
						        ' Practice Head Email -->'
						                + i_practice_head_email);

						var i_author = 442

						var i_recipient = i_practice_head_email

						nlapiLogExecution('DEBUG', 'afterSubmit_email_trigger',
						        'i_recipient...' + i_recipient);
						nlapiLogExecution('DEBUG', 'afterSubmit_email_trigger',
						        'i_author...' + i_author);

						var s_email_body = 'Hi  ' + i_practice_head_name
						        + ',\n\n'
						s_email_body += '	Purchase request '
						        + i_PR_no
						        + ' has been raised by '
						        + i_requestor
						        + '  and is pending for your approval. Please login to NetSuite to approve /reject.\n\n'
						// s_email_body+='PR item request pending for practice
						// head approval to approve / reject the PR.\n'
						// s_email_body+='PR # : '+i_PR_no+'\n'
						// s_email_body+='Requestor : '+i_requestor+'\n\n'

						s_email_body += 'Regards ,\n'
						s_email_body += 'Information Systems'

						var s_email_subject = ' Purchase request raised for your approval'

						var records = new Object();
						records['transaction'] = i_recordID

						if (_logValidation(i_author)
						        && _logValidation(i_recipient)) {
							nlapiLogExecution('DEBUG',
							        'afterSubmit_email_trigger', 'cccccccccc');

							nlapiSendEmail(i_author, i_recipient,
							        s_email_subject, s_email_body, null, null,
							        null, null);
							nlapiLogExecution('DEBUG',
							        'afterSubmit_email_trigger',
							        ' Email Sent ......');

						}// Author / Recipient

					}

				}// Record OBJ

			}// Record ID & Record type

		}// TRY
		catch (exception) {
			nlapiLogExecution('DEBUG', 'ERROR', ' Exception Caught -->'
			        + exception);
		}// CATCH

	}// CREATE

	return true;

}

// END AFTER SUBMIT ===============================================

// BEGIN FUNCTION ===================================================
function _logValidation(value) {
	if (value != null && value.toString() != null && value != ''
	        && value != undefined && value.toString() != undefined
	        && value != 'undefined' && value.toString() != 'undefined'
	        && value.toString() != 'NaN' && value != NaN) {
		return true;
	} else {
		return false;
	}
}

function get_employee_details(i_employee) {
	var s_email;
	var s_name;
	var a_return_array = new Array();

	if (_logValidation(i_employee)) {
		var o_employeeOBJ = nlapiLoadRecord('employee', i_employee)

		if (_logValidation(o_employeeOBJ)) {
			s_email = o_employeeOBJ.getFieldValue('email')
			nlapiLogExecution('DEBUG', 'get_employee_email', ' Email -->'
			        + s_email);

			var s_first_name = o_employeeOBJ.getFieldValue('firstname')
			nlapiLogExecution('DEBUG', 'get_employee_email', ' First Name -->'
			        + s_first_name);

			var s_middle_name = o_employeeOBJ.getFieldValue('middlename')
			nlapiLogExecution('DEBUG', 'get_employee_email', ' Middle Name -->'
			        + s_middle_name);

			var s_last_name = o_employeeOBJ.getFieldValue('lastname')
			nlapiLogExecution('DEBUG', 'get_employee_email', ' Last Name -->'
			        + s_last_name);

			if (!_logValidation(s_first_name)) {
				s_first_name = ''
			}
			if (!_logValidation(s_middle_name)) {
				s_middle_name = ''
			}
			if (!_logValidation(s_last_name)) {
				s_last_name = ''
			}

			s_name = s_first_name + ' ' + s_middle_name + ' ' + s_last_name

		}// Employee OBJ

	}// Employee

	a_return_array[0] = s_email + '####' + s_name

	return a_return_array;

}// Recipient Email

function removearrayduplicate(array) {
	var newArray = new Array();
	label: for (var i = 0; i < array.length; i++) {
		for (var j = 0; j < array.length; j++) {
			if (newArray[j] == array[i])
				continue label;
		}
		newArray[newArray.length] = array[i];
	}
	return newArray;
}

// END FUNCTION =====================================================
