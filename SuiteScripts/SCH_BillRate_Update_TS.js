/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       18 Aug 2016     deepak.srinivas
 *
 */

/**
 * @param {String} type Context Types: scheduled, ondemand, userinterface, aborted, skipped
 * @returns {Void}
 */
var days = ['sunday','monday','tuesday','wednesday','thursday','friday','saturday'];


function searchTimeSheets(){
	try{
		var billRate = '';
		// new nlobjSearchFilter('billingstatus', 'timeentry', 'is', 'F')
		
		/*var filter = Array();
		
		filter.push(new nlobjSearchFilter('employee', null, 'anyof', employeeID));
		filter.push(new nlobjSearchFilter('startdate', null, 'onorbefore', enDate ));//startdate
		filter.push(new nlobjSearchFilter('endate', null, 'onorafter', stDate ));*/ 
		
		var col = Array();
		col.push(new nlobjSearchColumn('internalid'));
	//	col.push(new nlobjSearchColumn('startdate'));
		//col.push(new nlobjSearchColumn('enddate'));
		
		var timeSheet_Res = nlapiSearchRecord('timesheet',2818,null,col);
		if(timeSheet_Res){
			for(var k=0;k<timeSheet_Res.length;k++){
				var timeSheet_ID = timeSheet_Res[k].getValue('internalid');
			//	var st_Date_T = timeSheet_Res[k].getValue('startdate');
			//	var end_Date_T = timeSheet_Res[k].getValue('enddate');
			//	st_Date_T = nlapiStringToDate(st_Date_T);
			//	end_Date_T = nlapiStringToDate(end_Date_T);
				//Compare
			//	var st_date_R = nlapiStringToDate(st_date);
			//	var en_date_R = nlapiStringToDate(end_date);
				
				//
			//	if(st_date_R >= st_Date_T && en_date_R >= end_Date_T  ){
				nlapiLogExecution('debug','TimeSheet Edit' ,timeSheet_ID );
				var loadRecord = nlapiLoadRecord('timesheet',timeSheet_ID);
				
				var lineCount = loadRecord.getLineItemCount('timegrid');
				for(var m=1;m<=lineCount;m++){
					
					loadRecord.selectLineItem('timegrid',m);
					
					
					for(var n=0;n<7;n++){
						
						var sub_day = days[n];
						var o_sub_record_view = loadRecord.viewCurrentLineItemSubrecord('timegrid', sub_day);
						if(o_sub_record_view){
						var i_item_id = o_sub_record_view.getFieldValue('item');
						if(i_item_id == '2222'){
						var sub_rec = loadRecord.editCurrentLineItemSubrecord('timegrid',sub_day);
						if(sub_rec){
							//nlapiLogExecution('debug','Bill Rate ',billRate);
						sub_rec.setFieldValue('isbillable','F');
						sub_rec.commit();
						}
					}
					}
				}
					loadRecord.commitLineItem('timegrid');
				}
				
				var id = nlapiSubmitRecord(loadRecord);
				nlapiLogExecution('debug','Submitted ID',id);
			//}
		}
	}
		
		
	}
	catch(e){
		
		nlapiLogExecution('debug','Process Error',e);
	}
}