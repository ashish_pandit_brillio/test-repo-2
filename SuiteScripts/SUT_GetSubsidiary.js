function getSubsidiary(request,response)
{
	try
	{
		if(request.getMethod() == 'GET')
		{	
			var i_employee = request.getParameter('i_employee');
			nlapiLogExecution('Debug','i_employee ',i_employee);
			var subsidiary_emp =  nlapiLookupField('employee', i_employee, 'subsidiary');
			nlapiLogExecution('Debug','s_subsidiary ',subsidiary_emp);
			response.write(subsidiary_emp);
		}
	}
	catch(err)
	{
		nlapiLogExecution('ERROR','ERROR MESSAGE :- ',err);
	}
}