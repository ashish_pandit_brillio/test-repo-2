function TimeSheet_Extract()
{
	nlapiLogExecution('DEBUG', '1', '1');
	
	var cols = new Array();
	cols[0] = new nlobjSearchColumn('employee', null, 'group');
	cols[1] = new nlobjSearchColumn('date', null, 'group');
	cols[2] = new nlobjSearchColumn('title', 'projecttask', 'group');
	cols[3] = new nlobjSearchColumn('durationdecimal', null, 'sum');
	cols[0].setSort();
	cols[1].setSort();
	
	var filters = new Array();
	filters[filters.length] = new nlobjSearchFilter('subsidiary', null, 'anyof', 2);
	filters[filters.length] = new nlobjSearchFilter('date', null, 'within', '7/14/2014', '7/27/2014');
	filters[filters.length] = new nlobjSearchFilter('type', null, 'is', 'A');
	
	
	var searchresult = nlapiSearchRecord('timebill', null, filters, cols);
	nlapiLogExecution('DEBUG', 'COUNT', searchresult.length);
	
	if(searchresult != null)
	{
		for(var i=0; i< Math.min(10,searchresult.length); i++)
		{
			nlapiLogExecution('DEBUG', 'Employee', searchresult[i].getText('employee', null, 'group'));
			nlapiLogExecution('DEBUG', 'Date', searchresult[i].getValue('date', null, 'group'));
			nlapiLogExecution('DEBUG', 'Task', searchresult[i].getValue('title', 'projecttask', 'group'));
			nlapiLogExecution('DEBUG', 'Hours', searchresult[i].getValue('durationdecimal', null, 'sum'));
		
		}
	
	}
	else
	{
	
	}
}


function Block_Timesheet(request, response)
{
	if(request.getMethod() == 'GET')
	{
		var TimeSheetForm = nlapiCreateForm("Block TimeSheet");
		TimeSheetForm.setTitle("Block TimeSheet");
		
		var BlockTimeSheet = TimeSheetForm.addField('blockts', 'select', 'Block TimeSheet').setLayoutType('normal', 'startrow');
		BlockTimeSheet.addSelectOption('0','No');
		BlockTimeSheet.addSelectOption('1','Yes');
		
		TimeSheetForm.addField('ts_helptext', 'label', '<b>Note - Timesheet blocking feature when enabled will not allow employees of LLC subsidiary to submit timesheet</b>').setLayoutType('startrow');
		
		var cols = new Array();
		cols[cols.length] = new nlobjSearchColumn('custrecord_block_timesheet');
		
		var searchresult = nlapiSearchRecord('customrecord_feature_blocking', null, null, cols);
		if(searchresult != null)
		{
			var RecId = TimeSheetForm.addField('recordid', 'integer', 'Record Id');
			RecId.setDisplayType('hidden');
			
			for(var i = 0; i < searchresult.length; i++)
			{
				RecId.setDefaultValue(searchresult[i].getId());
				if(searchresult[i].getValue('custrecord_block_timesheet') == 'T')
				{
					BlockTimeSheet.setDefaultValue('1');
				}
				else
				{
					BlockTimeSheet.setDefaultValue('0');
				}
			}
		}
		else
		{
			BlockTimeSheet.setDefaultValue('0');
		}
		
		TimeSheetForm.addSubmitButton('Submit');
		response.writePage(TimeSheetForm);
	}
	else
	{
		if(request.getParameter('recordid') != null && request.getParameter('recordid') > 0)
		{
			//Record exists
			var record = nlapiLoadRecord('customrecord_feature_blocking', request.getParameter('recordid'));
		}
		else
		{
			//No Record exists
			var record = nlapiCreateRecord('customrecord_feature_blocking');
		}

		if(request.getParameter('blockts') == 0)
		{
			record.setFieldValue('custrecord_block_timesheet', 'F');
		}
		else
		{
			record.setFieldValue('custrecord_block_timesheet', 'T');
		}
		var RecId = nlapiSubmitRecord(record);
		nlapiSetRedirectURL('TASKLINK', 'CARD_-29');
	}
}