/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       17 Sept 2018    shamanth.k
 *
 */

/**
 * @param {String} type Context Types: scheduled, ondemand, userinterface, aborted, skipped
 * @returns {Void}
 */
function scheduled(type) {
    try {
        var context = nlapiGetContext();
        var current_date = new Date();
        var a_data = searchTransactions(current_date);
        
        var excel_result = createExcel(a_data);
       
	   //Returns File ID
        var newAttachment = nlapiLoadFile(excel_result);
        var a_emp_attachment = new Array();
		var emp_email = '';
		emp_email = ['swetha.b@brillio.com','swapnil.g@brillio.com','bharath.s@brillio.com','monisa.p1@brillio.com','subas.panda@BRILLIO.COM','prabhakaran.g@BRILLIO.COM','sandeep.sayal@BRILLIO.COM','shrijith.nair@BRILLIO.COM','sanjay.talreja@BRILLIO.COM','Jayanth.Selvappullai@brillio.com','Yogesh.sikotara@BRILLIO.COM','yogesh.gokhale@brillio.com','Rupali.Jain@brillio.com','archana.r@BRILLIO.COM','neha.rohira1@brillio.com','nagaveni.h@brillio.com','benjamin.l@brillio.com','dave.bernstein@brillio.com','balaji.pr@brillio.com','Jatin.Valecha@brillio.com','anitha.ramesh@brillio.com','manigandan.b@brillio.com'];
        
		var day = current_date.getDay();
		if(day == 0)
		{
			emp_email = ['swetha.b@brillio.com','swapnil.g@brillio.com','bharath.s@brillio.com','Ajay.Deshmukh@brillio.com','monisa.p1@brillio.com','subas.panda@BRILLIO.COM','prabhakaran.g@BRILLIO.COM','sandeep.sayal@BRILLIO.COM','shrijith.nair@BRILLIO.COM','sunil.chakraborty@brillio.com','lavanya.p@brillio.com','sanjay.talreja@BRILLIO.COM','yogesh.gokhale@brillio.com','Rupali.Jain@brillio.com','archana.r@BRILLIO.COM','neha.rohira1@brillio.com','nagaveni.h@brillio.com','manigandan.b@brillio.com'];
		}
		nlapiSendEmail(442, emp_email , 'Daily HC Report as on ' + nlapiDateToString(new Date(), 'date') , 'Please find the attachement of head count report', null, ["sridhar.v@brillio.com"],null, newAttachment);
        nlapiLogExecution('DEBUG', 'Mail Sent', emp_email);
    } 
	catch (err) {
        nlapiLogExecution('DEBUG', 'Process Error - Main', err);
        Send_Exception_Mail(err);    
    }
}
//Create Excel logic
function createExcel(a_results) {
    try {
       
		var current_date = nlapiDateToString(new Date());
        var strVar_excel = '';
        var html = "";

        html += "Function,";
        html += "Subsidiary,";
        html += "Email,";
       // html += "Solution ID,";
        //html += "INC ID,";
        html += "Resource Name,";
        //html += "OTG App,";
        html += "Fusion ID,";
        html += "Department,";
        html += "Sub Department,";
        html += "Level,";
		html += "Family,";
        html += "Primary Skill,";
        html += "Secondary Skill,";
		html += "Skill Approval Status,";
        html += "Designation,";
       // html += "Customer Vertical,";
        html += "Customer Name,";
        //html += "Customer Territory,";
       // html += "End Customer,";
        html += "Project ID,";
        html += "Project Name,";
        html += "Project Type,";
       // html += "OLD Project ID,";
        html += "Executing Practice,";
        html += "Resource Billable,";
        html += "Shadow Resource,";
        html += "Onsite/Offsite,";
        html += "Allocation Start Date,";
        html += "Allocation End Date,";
       // html += "Numbers of Hours,";
        html += "Percentage Time,";
        html += "Billing Start Date,"; //
        html += "Billing End Date,";
        html += "Billing Type,";
       // html += "IS T&M Monthly,";
        html += "Work State,";
		html += "Work City,";
		html += "Employee Location,";
		//html += "Visa Type,";
		html += "Project Manager,";
		html += "Delivery Manager,";
		html += "Reporting Manager,";
		//html += "Salaried/Hourly,";
		html += "Person Type,";
		html += "Employee Type,";
		//html += "Employement Category,";
		html += "Hire Date,";
		html += "Expense Approver,";
		html += "Time Approver,";
		//html += "Bill Rate,";
		//html += "OT Rate,";
		//html += "Monthly Rate,";
		//html += "Notes,";
		//html += "Allocation Status,";
		//html += "Last Modified By,";
		//html += "Last Modified Date,";
		html += "ALT. Email,";
		html += "Other Email,";
		html += "Personal Email,";
		//html += "Employee Inactive,";
		//html += "Last Working Day,";
		html += "Project Allocation Category,";
		//html += "Project Allocation Region,";
		//html += "Monthly Rate,";
		//html += "Employee Internal ID,";
		//html += "Gender,";
		//html += "GST Location,";
		//html += "Region,";
		//html += "Billing Type,";
		html += "Date Created,";
		html += "Primary Skill Old,";
		html += "Secondary Skill Old,";
		html += "Certificate Start Date,";
		html += "Certificate End Date,";
		html += "Certificate,";
        html += "\r\n";
		
		for (var k = 0; k < a_results.length; k++) {
            var temp = a_results[k];
            html += temp.Function;
            html += ",";
            html += _logValidation(temp.subsidiary)== false ? '' : temp.subsidiary.replace(/[,.\t\n\r\f\v]/g, '');
            html += ",";
			html += temp.email;
            html += ",";
           // html += temp.solution_id;
           // html += ",";
           // html += temp.inc_id;
           // html += ",";
            html += _logValidation(temp.Name) == false ? '' : temp.Name.replace(/[,.\t\n\r\f\v]/g, '');
            html += ",";
           // html += temp.OTG_APP; //replace(/[^a-zA-Z0-9_]/g, ' '); 
           // html += ",";
            html +=  temp.FusionID;
            html += ",";
            html += _logValidation(temp.Department) == false ? ' ' : temp.Department;
            html += ",";
            html += _logValidation(temp.Sub_Department) == false ? ' ' : temp.Sub_Department;
            html += ",";
            html += _logValidation(temp.Level) == false ? ' ' : temp.Level;
            html += ",";
			html += _logValidation(temp.family) == false ? ' ' : temp.family.replace(/[,.\t\n\r\f\v]/g, '');
            html += ",";
            html += _logValidation(temp.primary_skill) == false ? ' ' : temp.primary_skill.replace(/[,.\t\n\r\f\v]/g, '');
            html += ",";
            html += _logValidation(temp.secondary_skills) == false ? ' ' : temp.secondary_skills.replace(/[,.\t\n\r\f\v]/g, '');
            html += ",";
			html += _logValidation(temp.Skill_status) == false ? ' ' : temp.Skill_status.replace(/[,.\t\n\r\f\v]/g, '');
			html += ",";
            html += _logValidation(temp.Designation) == false ? ' ' : temp.Designation.replace(/[,.\t\n\r\f\v]/g, '');
            html += ",";
           // html += _logValidation(temp.Customer_verical) == false ? ' ' : temp.Customer_verical;
           // html += ",";
            html += _logValidation(temp.Customer_name) == false ? ' ' : temp.Customer_name.replace(/[,.\t\n\r\f\v]/g, '');
            html += ",";
           // html += _logValidation(temp.Customer_terriorty) == false ? ' ' : temp.Customer_terriorty;
           // html += ",";
           // html += _logValidation(temp.End_Customer) == false ? ' ' : temp.End_Customer.replace(/[,.\t\n\r\f\v]/g, '');
           // html += ",";
            html += _logValidation(temp.Project_ID) == false ? ' ' : temp.Project_ID;
            html += ",";
            html += _logValidation(temp.Project_Name) == false ? ' ' : temp.Project_Name.replace(/[,.\t\n\r\f\v]/g, '');
            html += ",";
            html += _logValidation(temp.Project_type) == false ? ' ' : temp.Project_type;
            html += ",";
            //html += _logValidation(temp.old_projectID) == false ? ' ' : temp.old_projectID;
            //html += ",";
            html += _logValidation(temp.Executing_Practice) == false ? ' ' : temp.Executing_Practice.replace(/[^a-zA-Z0-9_]/g, ' '); //.replace(/[ \t\n\r\f\v]/g, ' ');
            html += ",";
            html += _logValidation(temp.Resource_Billable) == false ? ' ' : temp.Resource_Billable;
            html += ",";
            html += _logValidation(temp.Shadow_Resource) == false ? ' ' : temp.Shadow_Resource;
            html += ",";
            html += _logValidation(temp.onsite_offsite) == false ? ' ' : temp.onsite_offsite;
            html += ",";
            html += _logValidation(temp.Allocation_Start_Date) == false ? ' ' : temp.Allocation_Start_Date;
            html += ",";
            html += _logValidation(temp.Allocation_End_Date) == false ? ' ' : temp.Allocation_End_Date;
            html += ",";
            html += _logValidation(temp.percentage_time) == false ? ' ' : temp.percentage_time;
            html += ",";
            html += _logValidation(temp.billing_start_date) == false ? ' ' : temp.billing_start_date;
            html += ",";
            html += _logValidation(temp.billing_end_date) == false ? ' ' : temp.billing_end_date;
            html += ",";
            html += _logValidation(temp.billing_type) == false ? ' ' : temp.billing_type.replace(/[^a-zA-Z0-9_]/g, ' ');
            html += ",";
            html += _logValidation(temp.work_state) == false ? ' ' : temp.work_state;
            html += ",";
            html += _logValidation(temp.work_city) == false ? ' ' : temp.work_city;
            html += ",";
            html += _logValidation(temp.location) == false ? ' ' : temp.location.replace(/[^a-zA-Z0-9_]/g, ' ');
            html += ",";
			html += _logValidation(temp.Project_manager) == false ? ' ' : temp.Project_manager;
			html += ",";
			html += _logValidation(temp.Delivery_manager) == false ? ' ' : temp.Delivery_manager;
			html += ",";
			html += _logValidation(temp.Reporting_manager) == false ? ' ' : temp.Reporting_manager;
			html += ",";
			html += _logValidation(temp.Person_type) == false ? ' ' : temp.Person_type;
			html += ",";
			html += _logValidation(temp.Employee_type) == false ? ' ' : temp.Employee_type;
			html += ",";
			html += _logValidation(temp.Hire_Date) == false ? ' ' : temp.Hire_Date;
			html += ",";
			html += _logValidation(temp.Expense_approver) == false ? ' ' : temp.Expense_approver;
			html += ",";
			html += _logValidation(temp.Time_approver) == false ? ' ' : temp.Time_approver;
			html += ",";
			html += _logValidation(temp.ALT_email) == false ? ' ' : temp.ALT_email;
			html += ",";
			html += _logValidation(temp.Other_email) == false ? ' ' : temp.Other_email;
			html += ",";
			html += _logValidation(temp.Peronal_email) == false ? ' ' : temp.Peronal_email;
			html += ",";
			html += _logValidation(temp.Project_Allocation_category) == false ? ' ' : temp.Project_Allocation_category;
			html += ",";
			html += _logValidation(temp.Date_Created) == false ? ' ' : temp.Date_Created;
			html += ",";
			html += _logValidation(temp.Primary) == false ? ' ' : temp.Primary.replace(/[^a-zA-Z0-9_]/g, ' ');
			html += ",";
			html += _logValidation(temp.Secondary) == false ? ' ' : temp.Secondary.replace(/[^a-zA-Z0-9_]/g, ' ');
			html += ",";
			html += _logValidation(temp.Certificate_st_date) == false ? ' ' : temp.Certificate_st_date.replace(/[^a-zA-Z0-9_]/g, ' ');
			html += ",";
			html += _logValidation(temp.Certificate_end_date) == false ? ' ' : temp.Certificate_end_date.replace(/[^a-zA-Z0-9_]/g, ' ');
			html += ",";
			html += _logValidation(temp.Certificate) == false ? ' ' : temp.Certificate.replace(/[^a-zA-Z0-9_]/g, ' ');
            html += "\r\n";
        }
		var current_date = nlapiDateToString(new Date());
		//var _date = nlapiStringToDate(current_date);
        var fileName = 'Daily Head Count Report '+ nlapiDateToString(new Date())  +' .csv';

        var file = nlapiCreateFile(fileName, 'CSV', html);
        file.setFolder('354237'); //Prod
        var fileID = nlapiSubmitFile(file);
        nlapiLogExecution('debug', 'File ID', fileID);

        return fileID;
    } catch (err) {
        nlapiLogExecution('DEBUG', 'Excel creation error', err);
    }
}
//Search for transactions
function searchTransactions(d_date) {

    try {

		
        var context = nlapiGetContext();
		var search_ = nlapiLoadSearch('resourceallocation','customsearch2704');
			var filters = search_.getFilters();
			var columns = search_.getColumns();

    var o_data = new Array();
		//var columns = new Array();
		
	
        var search_results = searchRecord('resourceallocation', null, filters, [
            columns[0], columns[1], columns[2], columns[3], columns[4], columns[5],
            columns[6], columns[7], columns[8], columns[9], columns[10], columns[11], columns[12], columns[13], columns[14], columns[15],
            columns[16], columns[17], columns[18], columns[19], columns[20], columns[21], columns[22], columns[23], columns[24], columns[25],
            columns[26], columns[27], columns[28], columns[29], columns[30], columns[31], columns[32], columns[33], columns[34], columns[35], columns[36],
            columns[37], columns[38], columns[39], columns[40], columns[41], columns[42], columns[43], columns[44], columns[45], columns[46],
            columns[47], columns[48], columns[49], columns[50], columns[51], columns[52], columns[53], columns[54], columns[55], columns[56], columns[57], columns[58],
            columns[59], columns[60], columns[61], columns[62], columns[63], columns[64], columns[65], columns[66],columns[67]
			]);

        if (search_results) {
			
			
            for (var i = 0; i < search_results.length; i++) { //search_results.length
				
                var employee = search_results[i].getValue(columns[5]);
				
				nlapiLogExecution('DEBUG','Employee Name',search_results[i].getText(columns[5]));
				
			
                var skills_search = searchSkills(employee);
				
				
				
				var fam = _logValidation(skills_search) ? skills_search[0].getText('custrecord_family_selected') : ' ';
				
				var sec_skills = _logValidation(skills_search) ? skills_search[0].getValue('custrecord_secondry_updated') : ' ';
				
				var date_created = _logValidation(skills_search) ? skills_search[0].getValue('created') : ' ';
				
				var skill_status = _logValidation(skills_search) ? skills_search[0].getText('custrecord_skill_status') : ' ';
				
				var a_primarySkill = '';
				
						
				if(_logValidation(skills_search))
				{
					//var temp = skills_search[0].getText('custrecord_primary_updated');
					var a_primarySkill = skills_search[0].getText('custrecord_primary_updated');
 
				}
				
				//var certificate_search = '',
				var certifiacte_details = new Array();
				var certificate_st_date = new Array();
				var certificate_end_date = new Array();
				//var id = skills_search[0].getValue('internalid');
				if(_logValidation(skills_search))
				{
					var id = skills_search[0].getValue('internalid');
					nlapiLogExecution('DEBUG','Skill Search ID',id);
					var certificate_search = certificateDetails(id);
				
					if(_logValidation(certificate_search))
					{
						for(var index_certificate = 0; index_certificate < certificate_search.length; index_certificate++)
						{
						
							var certificate = certificate_search[index_certificate].getValue('custrecord_certifcate_det');
							certifiacte_details.push(certificate);
							
							var certificate_Start_Date = certificate_search[index_certificate].getValue('custrecord_certificate_st_date');
							certificate_st_date.push(certificate_Start_Date);
							
							var certificate_End_Date = certificate_search[index_certificate].getValue('custrecord_certificate_enddate');
							certificate_end_date.push(certificate_End_Date);
							
							nlapiLogExecution('DEBUG','Certificate Search ID',certificate_search[index_certificate].getValue('internalid'));
						}
					}
					else
					{
						certifiacte_details.push(' ');
						certificate_st_date.push(' ');
						certificate_end_date.push(' ');
					}
				}
				else
				{
					certifiacte_details.push(' ');
					certificate_st_date.push(' ');
					certificate_end_date.push(' ');
				}
				
                o_data.push({
                    'Function': search_results[i].getText(columns[0]), //1
                    'subsidiary': search_results[i].getText(columns[1]), //2
                    'email': search_results[i].getValue(columns[2]), //3
                   // 'solution_id': search_results[i].getValue(columns[3]), //4
                    //'inc_id': search_results[i].getValue(columns[4]), //6
                    'Name': search_results[i].getText(columns[5]), //7
                   // 'OTG_APP': search_results[i].getValue(columns[6]), //8
                    'FusionID':search_results[i].getValue(columns[7]), //9
                    'Department': search_results[i].getValue(columns[8]), //10
                    'Sub_Department': search_results[i].getText(columns[9]), //11
                    'Level':search_results[i].getText(columns[10]), //12
                    'family': fam,
					'primary_skill': a_primarySkill,
					'secondary_skills': sec_skills,
					'Skill_status' : skill_status,
                    'Designation': search_results[i].getValue(columns[11]), //13
                   // 'Customer_verical': search_results[i].getText(columns[12]), //14
                    'Customer_name': search_results[i].getValue(columns[13]), //15
                   // 'Customer_terriorty': search_results[i].getText(columns[14]), //16
                   // 'End_Customer': search_results[i].getValue(columns[15]), //17
                    'Project_ID': search_results[i].getValue(columns[16]), //18
                    'Project_Name': search_results[i].getValue(columns[17]), //19
                    'Project_type': search_results[i].getText(columns[18]), //20
                   // 'old_projectID': search_results[i].getValue(columns[19]), //21
                    'Executing_Practice': search_results[i].getText(columns[20]), //22
                    'Resource_Billable': search_results[i].getValue(columns[21]), //23
                   'Shadow_Resource': search_results[i].getValue(columns[22]), //24
                    'onsite_offsite': search_results[i].getText(columns[23]), //25
                    'Allocation_Start_Date': search_results[i].getValue(columns[24]), //26
                    'Allocation_End_Date': search_results[i].getValue(columns[25]), //27
                   // 'No_of_hours': search_results[i].getValue(columns[26]), //28
                   'percentage_time': search_results[i].getValue(columns[27]), //29
                    'billing_start_date': search_results[i].getValue(columns[28]), //30
                    'billing_end_date': search_results[i].getValue(columns[29]), //31
                    'billing_type': search_results[i].getText(columns[30]), //32
                   // 'IS_TM_Monthly': search_results[i].getValue(columns[31]),
                    'work_state': search_results[i].getText(columns[32]),
                    'work_city':  search_results[i].getValue(columns[33]),
                    'location': search_results[i].getText(columns[34]),
                  //  'visa_type': search_results[i].getValue(columns[35]),
                    'Project_manager': search_results[i].getText(columns[36]),
					'Delivery_manager': search_results[i].getText(columns[37]),
					'Reporting_manager': search_results[i].getText(columns[38]),
					//'Salaried_hourly': search_results[i].getText(columns[39]),
					'Person_type': search_results[i].getText(columns[40]),
					'Employee_type': search_results[i].getText(columns[41]),
					//'Employemnet_category': search_results[i].getText(columns[42]),
					'Hire_Date': search_results[i].getValue(columns[43]),
					'Expense_approver': search_results[i].getText(columns[44]),
					'Time_approver': search_results[i].getText(columns[45]),
					//'Bill_rate': search_results[i].getValue(columns[46]),
					//'OT_rate': search_results[i].getValue(columns[47]),
					//'Monthly_rate': search_results[i].getValue(columns[48]),
					//'Notes': search_results[i].getValue(columns[49]),
					//'Allocation_status': search_results[i].getText(columns[50]),
					//'Last_modified_by': search_results[i].getText(columns[51]),
					//'Last_modified_date': search_results[i].getValue(columns[52]),
					'ALT_email': search_results[i].getValue(columns[53]),
					'Other_email': search_results[i].getValue(columns[54]),
					'Peronal_email': search_results[i].getValue(columns[55]),
					//'Employee_inactive': search_results[i].getValue(columns[56]),
					//'Last_working_day': search_results[i].getValue(columns[57]),
					'Project_Allocation_category': search_results[i].getText(columns[59]),
				
					'Date_Created': date_created,
					'Primary' : search_results[i].getValue(columns[61]),
					'Secondary' : search_results[i].getValue(columns[62]),
					'Certificate_st_date' : JSON.stringify(certificate_st_date),
					'Certificate_end_date' : JSON.stringify(certificate_end_date),
					'Certificate' : JSON.stringify(certifiacte_details),

                });
            }
        }
		

        return o_data;
    } catch (ex) {
        nlapiLogExecution('DEBUG', 'Summary Error', ex);

    }
}


//Blank Values
function _logValidation(value) {
    if (value != 'null' && value != null && value != '- None -' && value != null && value != '' && value != undefined && value != undefined && value != 'undefined' && value != 'undefined' && value != 'NaN' && value != NaN) {
        return true;
    } else {
        return false;
    }
}


//Employee Level Search
function searchSkills(emp) {
    try {
		var context = nlapiGetContext();
		//nlapiYieldScript();
		
var customrecord_employee_master_skill_dataSearch = nlapiSearchRecord("customrecord_employee_master_skill_data",null,
[
   ["custrecord_employee_skill_updated","anyof",emp]
], 
[
   new nlobjSearchColumn("internalid"),
   new nlobjSearchColumn("custrecord_primary_updated"), 
   new nlobjSearchColumn("custrecord_secondry_updated"), 
   new nlobjSearchColumn("custrecord_family_selected"),
   new nlobjSearchColumn("custrecord_skill_status"),
   new nlobjSearchColumn("created").setSort(true)
]
);
        return customrecord_employee_master_skill_dataSearch;
    } catch (err) {
        nlapiLogExecution('DEBUG', 'Skills Search Error', err);
    }
}

//Certificate Search
function certificateDetails(id_) {
	try
	{
		//nlapiYieldScript();
		var certificate_result = nlapiSearchRecord("customrecord_emp_certificate_detail", null,
			["custrecord_emp_skill_parent","anyof",id_],
		[
			new nlobjSearchColumn("internalid"),
			new nlobjSearchColumn("custrecord_certifcate_det"),
			new nlobjSearchColumn("custrecord_certificate_st_date"),
			new nlobjSearchColumn("custrecord_certificate_enddate")
		]
		);
		
		return certificate_result;
	}
	catch (err)
	{
		nlapiLogExecution('DEBUG','Certificate Details error', err)
	}
}


function Send_Exception_Mail(err)
{​​​​​
    try
    {​​​​​   
        var context = nlapiGetContext();
        var s_DeploymentID =context.getDeploymentId();
        var s_ScriptID =context.getScriptId();
        var s_Subject ='Issue on Daily HC Report';
        
        var s_Body = 'This is to inform that Daily HC Report is having an issue and System is not able to send the details.';
        s_Body += '<br/><b>Issue: Code:</b> '+err.code+' Message: '+err.message;
        s_Body += '<br/><b>Script: </b>'+s_ScriptID;
        s_Body += '<br/><b>Deployment:</b> '+s_DeploymentID;
        s_Body += '<br/>Please have a look and do the needfull.';
        s_Body += '<br/><br/>Note: This is system genarated email incase of any failure while sending the notification to employees';

        s_Body += '<br/><br/>Regards,';
        s_Body += '<br/>Digital Office';
        nlapiSendEmail(442,"netsuite.support@brillio.com",s_Subject,s_Body,null, null, null, null);
    }​​​​​
    catch(err)
    {​​​​​
        nlapiLogExecution('error',' Send_Exception_Mail', err.message); 
		
    }​​​​​
}​​​​​
