function update_resource_allo() {
	try {
		var context = nlapiGetContext();
		var src_rslt = nlapiSearchRecord('resourceallocation', 'customsearch1571');
		
		if (src_rslt) {
			for (var srch_index = 0; srch_index < src_rslt.length; srch_index++) {
				allocate_assignee(src_rslt[srch_index].getId());

				var usageEnd = context.getRemainingUsage();
				if (usageEnd < 2000) {
					nlapiYieldScript();
				}
			}
		}
	}
	catch (err) {
		nlapiLogExecution('ERROR', 'ERROR MESSAGE:- ', err);
		Send_Exeception_Mail(err);
	}
}

function allocate_assignee(i_recordID) {
	try //
	{
		if (_logValidation(i_recordID)) //
		{
			var o_recordOBJ = nlapiLoadRecord('resourceallocation', i_recordID);

			if (_logValidation(o_recordOBJ)) //
			{
				var i_project = o_recordOBJ.getFieldValue('project')
				//nlapiLogExecution('DEBUG', 'afterSubmit_allocate_assignee', 'Project -->'+i_project);	

				var i_resource = o_recordOBJ.getFieldValue('allocationresource')
				var resource_name = o_recordOBJ.getFieldText('allocationresource')
				//nlapiLogExecution('DEBUG', 'afterSubmit_allocate_assignee', 'Resource -->'+i_resource);	

				var i_service_item = o_recordOBJ.getFieldValue('custevent1')
				//nlapiLogExecution('DEBUG', 'afterSubmit_allocate_assignee', 'Service Item -->'+i_service_item);	

				var i_service_item_ID = get_item_ID(i_service_item)
				//nlapiLogExecution('DEBUG', 'afterSubmit_allocate_assignee', 'Service Item ID -->'+i_service_item_ID);	

				var i_allocated_time = o_recordOBJ.getFieldValue('allocationamount')
				//nlapiLogExecution('DEBUG', 'afterSubmit_allocate_assignee', 'Allocated Time -->'+i_allocated_time);	

				var i_unit_cost = o_recordOBJ.getFieldValue('custevent2')
				//nlapiLogExecution('DEBUG', 'afterSubmit_allocate_assignee', 'Unit Cost -->'+i_unit_cost);	

				var i_bill_rate = o_recordOBJ.getFieldValue('custevent3')
				//nlapiLogExecution('DEBUG', 'afterSubmit_allocate_assignee', ' Bill Rate -->'+i_bill_rate);	

				//Added by Vinod
				var i_ot_billable = o_recordOBJ.getFieldValue('custevent_otbillable')
				//nlapiLogExecution('DEBUG', 'afterSubmit_allocate_assignee', 'OT Billable -->' + i_ot_billable);

				var i_ot_payable = o_recordOBJ.getFieldValue('custevent_otpayable')
				//nlapiLogExecution('DEBUG', 'afterSubmit_allocate_assignee', ' OT Payable -->' + i_ot_payable);

				var i_service_otitem = o_recordOBJ.getFieldValue('custevent_otserviceitem')
				//nlapiLogExecution('DEBUG', 'afterSubmit_allocate_assignee', ' Service Item -->' + i_service_otitem);

				var i_service_otrate = o_recordOBJ.getFieldValue('custevent_otrate')
				//nlapiLogExecution('DEBUG', 'afterSubmit_allocate_assignee', ' OT Rate -->' + i_service_otrate);

				// Added by Vikrant
				var i_Holiday_Item_ID = o_recordOBJ.getFieldValue('custevent_holiday_service_item');
				//nlapiLogExecution('DEBUG', 'afterSubmit_allocate_assignee', 'i_Holiday_Item_ID -->'+i_Holiday_Item_ID);

				var i_Leave_Item_ID = o_recordOBJ.getFieldValue('custevent_leave_service_item');
				//nlapiLogExecution('DEBUG', 'afterSubmit_allocate_assignee', 'i_Leave_Item_ID -->'+i_Leave_Item_ID);

				// End of Added by Vikrant


				//Added by Vinod

				if (_logValidation(i_project)) //
				{
					var filter = new Array();
					filter[0] = new nlobjSearchFilter('company', null, 'is', i_project);

					// added by Nitish
					filter[1] = new nlobjSearchFilter('ismilestone', null, 'is', 'F');

					var columns = new Array();
					columns[0] = new nlobjSearchColumn('internalid');
					columns[1] = new nlobjSearchColumn('title');//Added by Vinod
					var a_search_results = nlapiSearchRecord('projectTask', null, filter, columns);

					if (_logValidation(a_search_results)) //
					{
						//nlapiLogExecution('DEBUG', 'afterSubmit_allocate_assignee', ' Search Results Length -->' + a_search_results.length);

						for (var yt = 0; yt < a_search_results.length; yt++) //
						{
							var is_assignee_present = false;
							//Added by Vinod
							var AllocateTask = 'F';

							if (a_search_results[yt].getValue('title') == 'OT' && i_ot_billable == 'F' && i_ot_payable == 'F') //
							{
								AllocateTask = 'F';
							}
							else //
							{
								AllocateTask = 'T';
							}

							if (a_search_results[yt].getValue('title') == 'Floating Holiday') // if Task is floating holiday then do not allocate employee to it
							{
								AllocateTask = 'F';
							}

							//Added by Vinod

							//nlapiLogExecution('DEBUG', 'Allocate Task', AllocateTask);
							if (AllocateTask == 'T') //Added by Vinod
							{
								var i_internalID = a_search_results[yt].getValue('internalid')
								//nlapiLogExecution('DEBUG', 'afterSubmit_allocate_assignee', ' Internal ID *************-->' + i_internalID);

								if (_logValidation(i_internalID)) //
								{
									var o_project_taskOBJ = nlapiLoadRecord('projectTask', i_internalID)

									if (_logValidation(o_project_taskOBJ)) //
									{
										var i_estimated_work = o_project_taskOBJ.getFieldValue('estimatedwork')

										//nlapiLogExecution('DEBUG', 'afterSubmit_allocate_assignee', ' Estimated Work-->' + i_estimated_work);

										var i_line_count_assignee = o_project_taskOBJ.getLineItemCount('assignee')

										if (_logValidation(i_line_count_assignee)) //
										{
											for (var ht = 1; ht <= i_line_count_assignee; ht++) //
											{
												var i_resource_pr = o_project_taskOBJ.getLineItemValue('assignee', 'resource', ht)

												if (i_resource_pr == i_resource) //
												{
													is_assignee_present = true;
													break;
												}
											}//Loop							
										}//Line Count Assignee
										if (_logValidation(i_estimated_work) && i_estimated_work != 0) //
										{
											if (is_assignee_present == false) //
											{
												nlapiLogExecution('audit', 'assignee created for resource :: ' + resource_name);
												o_project_taskOBJ.selectNewLineItem('assignee')
												o_project_taskOBJ.setCurrentLineItemValue('assignee', 'resource', i_resource);
												o_project_taskOBJ.setCurrentLineItemValue('assignee', 'unitcost', i_unit_cost);
												//Added by Vinod
												if (o_project_taskOBJ.getFieldValue('title') == 'OT') //
												{
													o_project_taskOBJ.setCurrentLineItemValue('assignee', 'serviceitem', i_service_otitem);
													o_project_taskOBJ.setCurrentLineItemValue('assignee', 'unitprice', i_service_otrate);
												}
												else //
												{
													o_project_taskOBJ.setCurrentLineItemValue('assignee', 'serviceitem', i_service_item_ID);
													o_project_taskOBJ.setCurrentLineItemValue('assignee', 'unitprice', i_bill_rate);
												}

												if (o_project_taskOBJ.getFieldValue('title') == 'Holiday') //
												{
													o_project_taskOBJ.setCurrentLineItemValue('assignee', 'serviceitem', i_Holiday_Item_ID);
													o_project_taskOBJ.setCurrentLineItemValue('assignee', 'unitprice', 0);
												}

												if (o_project_taskOBJ.getFieldValue('title') == 'Leave') //
												{
													o_project_taskOBJ.setCurrentLineItemValue('assignee', 'serviceitem', i_Leave_Item_ID);
													o_project_taskOBJ.setCurrentLineItemValue('assignee', 'unitprice', 0);
												}

												//Added by Vinod
												o_project_taskOBJ.setCurrentLineItemValue('assignee', 'estimatedwork', i_allocated_time);
												o_project_taskOBJ.commitLineItem('assignee');
											}//False

											if (is_assignee_present == true) //
											{
												if (_logValidation(i_line_count_assignee)) //
												{
													for (var ht = 1; ht <= i_line_count_assignee; ht++) //
													{
														var i_resource_pr = o_project_taskOBJ.getLineItemValue('assignee', 'resource', ht)

														if (i_resource_pr == i_resource) //
														{
															o_project_taskOBJ.setLineItemValue('assignee', 'estimatedwork', ht, i_allocated_time)
															o_project_taskOBJ.setLineItemValue('assignee', 'unitcost', ht, i_unit_cost);
															//Added by Vinod
															if (o_project_taskOBJ.getFieldValue('title') == 'OT') //
															{
																o_project_taskOBJ.setLineItemValue('assignee', 'unitprice', ht, i_service_otrate)
															}
															else //
															{
																o_project_taskOBJ.setLineItemValue('assignee', 'unitprice', ht, i_bill_rate)
															}
															//Added by Vinod
														}
													}//Loop							
												}//Line Count Assignee
											}
										}

										var i_submitID = nlapiSubmitRecord(o_project_taskOBJ, true, true);
										//nlapiLogExecution('DEBUG', 'afterSubmit_allocate_assignee', ' ***************** Project Task Submit ID *************** -->' + i_submitID);

									}//Project TAsk OBJ
								}//Project Task ID
							}
							else //
							{
								//Added by Vinod
								//Remove assignee from OT Task
								var is_assignee_present = false;

								var i_internalID = a_search_results[yt].getValue('internalid')
								//nlapiLogExecution('DEBUG', 'afterSubmit_allocate_assignee', ' Internal ID *************-->' + i_internalID);

								var o_project_taskOBJ = nlapiLoadRecord('projectTask', i_internalID)
								for (var ht = 1; ht <= i_line_count_assignee; ht++) //
								{
									var i_resource_pr = o_project_taskOBJ.getLineItemValue('assignee', 'resource', ht)
									if (i_resource_pr == i_resource) //
									{
										is_assignee_present = true;
										break;
									}
								}//Loop							
								if (is_assignee_present == true) //
								{
									for (var ht = 1; ht <= i_line_count_assignee; ht++) //
									{
										var i_resource_pr = o_project_taskOBJ.getLineItemValue('assignee', 'resource', ht)

										if (i_resource_pr == i_resource) //
										{
											if (o_project_taskOBJ.getFieldValue('title') == 'OT') //
											{
												//Remove the assignee from OT task
												o_project_taskOBJ.removeLineItem('assignee', ht);
											}
										}
									}
									var i_submitID = nlapiSubmitRecord(o_project_taskOBJ, true, true);
									//nlapiLogExecution('DEBUG', 'afterSubmit_allocate_assignee', ' ***************** Project Task Submit ID *************** -->' + i_submitID);
								}
								//Added by Vinod
							}
						}//Results
					}//Search Results
				}//Employee
			}//Record OBJ		
		}//Record ID & Record Type		
	}
	catch (exception) //
	{
		nlapiLogExecution('ERROR', 'ERROR', 'Exception -->' + exception);
	}

}

function _logValidation(value) {
	if (value != null && value.toString() != null && value != '' && value != undefined && value.toString() != undefined && value != 'undefined' && value.toString() != 'undefined' && value.toString() != 'NaN' && value != NaN) {
		return true;
	}
	else {
		return false;
	}
}

function get_item_ID(i_service_item) {

	//nlapiLogExecution('DEBUG', 'get_item_ID', 'i_service_item-->' + i_service_item);

	var i_item_ID;
	if (_logValidation(i_service_item)) {
		var filter = new Array();
		filter[0] = new nlobjSearchFilter('itemid', null, 'is', i_service_item);

		var columns = new Array();
		columns[0] = new nlobjSearchColumn('internalid');

		var a_search_results = nlapiSearchRecord('item', null, filter, columns);

		if (_logValidation(a_search_results)) {
			i_item_ID = a_search_results[0].getValue('internalid');
			//nlapiLogExecution('DEBUG', 'get_item_ID', 'i_item_ID-->' + i_item_ID);

		}

	}
	return i_item_ID;
}
function Send_Exeception_Mail(err)
{​​​​​
    try
    {​​​​​   
        var context = nlapiGetContext();
        var s_DeploymentID =context.getDeploymentId();
        var s_ScriptID =context.getScriptId();
        var s_Subject ='Failure Notification on Update Resource Allocation';
        
        var s_Body = 'This is to inform that Update Resource Allocation is having an issue and System is not able to send the email notification to employees.';
        s_Body += '<br/>Issue: Code: '+err.code+' Message: '+err.message;
        s_Body += '<br/>Script: '+s_ScriptID;
        s_Body += '<br/>Deployment: '+s_DeploymentID;
        s_Body += '<br/>Please have a look and do the needfull.';
        s_Body += '<br/><br/>Note: This is system genarated email incase of any failure while sending the notification to employees';

        s_Body += '<br/><br/>Regards,';
        s_Body += '<br/>Digital Office';
        nlapiSendEmail(442,15279,s_Subject,s_Body,null, null, null, null);
    }​​​​​
    catch(err)
    {​​​​​
        nlapiLogExecution('error',' Send_Exeception_Mail', err.message); 
		
    }​​​​​
}​​​​​
