/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       16 March 2020     Prabhat Gupta
 *
 */

/**
 * @param {Object} dataIn Parameter object
 * @returns {Object} Output object
 */
 function getRESTlet(dataIn) {

	return {};
}

/**
 * @param {Object} dataIn Parameter object
 * @returns {Object} Output object
 */
function postRESTlet(dataIn) {

	try {
		
		//dataIn = {"lastUpdatedTimestamp":""}
        var response = new Response();
        //nlapiLogExecution('debug', 'dataIn', JSON.stringify(dataIn));
        var currentDateAndTime = sysDate();



		var receivedDate = dataIn.lastUpdatedTimestamp;
		//nlapiLogExecution('DEBUG', 'Current Date', 'receivedDate...' + receivedDate);
		//nlapiLogExecution('DEBUG', 'Current Date', 'currentDateAndTime...' + currentDateAndTime);

		//var receivedDate = '7/12/2019 1:00 am';
		//Check if the timestamp is empty
		if (!_logValidation(receivedDate)) {          // for empty timestanp in this we will give whole data as a response
			var filters = new Array();
			filters = [
				
				  ["isinactive","is","F"]			 
				
				];
		//	nlapiLogExecution('debug', 'filters ', filters);
		} else {
			var filters = new Array();                             // this filter will provide the result within the current date and given date     
			filters = [
				
				["lastmodifieddate", "within", receivedDate, currentDateAndTime]		//prabhat gupta NIS-1350 28/05/2020		
				];
		//	nlapiLogExecution('debug', 'filters ', filters);
		}
		
		// created search by grouping 
		var searchResults = searchRecord("customer",null,
				filters, 
				[
				      
					  
					   new nlobjSearchColumn("stage"), 
					   new nlobjSearchColumn("altname"),    
                       new nlobjSearchColumn("formulatext").setFormula("CONCAT({entityid},CONCAT(' ',{altname}))"),    
					   new nlobjSearchColumn("custentity_region"), 
					   new nlobjSearchColumn("custentity_clientpartner"), 
					   new nlobjSearchColumn("email","CUSTENTITY_CLIENTPARTNER",null), 
					   new nlobjSearchColumn("internalid","CUSTENTITY_CLIENTPARTNER",null),
					   new nlobjSearchColumn("isinactive"),
                       new nlobjSearchColumn("custentity_sfdc_account_id"),
										
				]
				);	


						
		
		
		var customerData = [];
        
		if (searchResults) {
			
			
			for(var i=0; i<searchResults.length; i++){    
				
		    var customer = {};
                   
			var customerInternalId = searchResults[i].getId();
			var type = searchResults[i].getValue("stage");
			var name = searchResults[i].getValue("altname");
            var custIdName = searchResults[i].getValue("formulatext");
			var regionId = searchResults[i].getValue("custentity_region");
			var regionName =  searchResults[i].getText("custentity_region");
			var clientPartnerId =  searchResults[i].getValue("custentity_clientpartner");
			var clientPartnerName =  searchResults[i].getText("custentity_clientpartner");
			var clientPartnerEmail =  searchResults[i].getValue("email","CUSTENTITY_CLIENTPARTNER",null);
			var inactiveStatus = searchResults[i].getValue("isinactive");
			var sfdcId = searchResults[i].getValue("custentity_sfdc_account_id");
           
			
			
			
			customer.customerInternalId = customerInternalId;
			customer.type = type;
			customer.name = name;
            customer.custIdName = custIdName;
			customer.regionId = regionId;
			customer.regionName = regionName;
			customer.clientPartnerId = clientPartnerId;
			customer.clientPartnerName = clientPartnerName;
			customer.clientPartnerEmail = clientPartnerEmail;
			customer.inactiveStatus = inactiveStatus;
			customer.accountId = sfdcId;
			
			
				
				
			customerData.push(customer);
			
		    }
		}
		

		response.timeStamp = currentDateAndTime;
		response.data = customerData;
		//response.data = dataRow;
		response.status = true;
	//	nlapiLogExecution('debug', 'response ',JSON.stringify(response));
	} catch (err) {
		nlapiLogExecution('ERROR', 'postRESTlet', err);
		response.timeStamp = '';
		response.data = err;
		response.status = false;
	}
	return response;

}
//validate blank entries
function _logValidation(value) {
	if (value != null && value.toString() != null && value != '' && value != undefined && value.toString() != undefined && value != 'undefined' && value.toString() != 'undefined' && value.toString() != 'NaN' && value != NaN) {
		return true;
	} else {
		return false;
	}
}
//Get current date

//Returns timestamp
function timestamp() {
	var str = "";

	var currentTime = new Date();
	var hours = currentTime.getHours();
	var minutes = currentTime.getMinutes();
	var seconds = currentTime.getSeconds();
	var meridian = "";
	if (hours > 12) {
		meridian += "pm";
	} else {
		meridian += "am";
	}
	if (hours > 12) {

		hours = hours - 12;
	}
	if (minutes < 10) {
		minutes = "0" + minutes;
	}
	if (seconds < 10) {
		seconds = "0" + seconds;
	}
	//str += hours + ":" + minutes + ":" + seconds + " ";
	str += hours + ":" + minutes + " ";
	return str + meridian;
}

function Response() {
	this.status = "";
	this.timeStamp = "";
	this.data = "";

}

//Search function for results more than 1000
function searchRecord(recordType, savedSearch, arrFilters, arrColumns,
		filterExpression) {

	try {
		var search = null;

		// if a saved search is provided, load it and add the filters and
		// columns
		if (savedSearch) {
			search = nlapiLoadSearch(recordType, savedSearch);

			if (isArrayNotEmpty(arrFilters)) {
				search.addFilters(arrFilters);
			}

			if (isArrayNotEmpty(arrColumns)) {
				search.addColumns(arrColumns);
			}

			if (isArrayNotEmpty(filterExpression)) {
				search.setFilterExpression(filterExpression);
			}
		}
		// create a new search
		else {
			search = nlapiCreateSearch(recordType, arrFilters, arrColumns);
		}

		// run search
		var resultSet = search.runSearch();

		// iterate through the search and get all data 1000 at a time
		var searchResultCount = 0;
		var resultSlice = null;
		var searchResult = [];

		do {
			resultSlice = resultSet.getResults(searchResultCount,
					searchResultCount + 1000);

			if (resultSlice) {

				resultSlice.forEach(function(result) {

					searchResult.push(result);
					searchResultCount++;
				});
			}
		} while (isArrayNotEmpty(resultSlice) && resultSlice.length >= 1000);

		return searchResult;
	} catch (err) {
		nlapiLogExecution('ERROR', 'searchRecord', err);
		throw err;
	}
}



function isEmpty(value) {

	return value == null || value == "" || typeof(value) == undefined;
}

function isNotEmpty(value) {

	return !isEmpty(value);
}

function isArrayEmpty(argArray) {

	return !isArrayNotEmpty(argArray);
}

function isArrayNotEmpty(argArray) {

	return (isNotEmpty(argArray) && argArray.length > 0);
}
//function to format date object into NetSuite's mm/dd/yyyy format.
function formatNSDate(dateObj) {
	if (dateObj) {
		var nsFormatDate = dateObj.getFullYear() + '-' + addZeros(dateObj.getMonth() + 1, 2) + '-' + addZeros(dateObj.getDate(), 2);
		return nsFormatDate;
	}
	return null;
}

//function to add leading zeros on date parts.
function addZeros(num, len) {
	var str = num.toString();
	while (str.length < len) {
		str = '0' + str;
	}
	return str;
}