function SkillUpdateDashboard(request ,response)
{
	try
	{
		
		//var o_context = nlapiGetContext();
		var o_form_obj = nlapiCreateForm("Skill Update");
		if(request.getMethod() == 'GET')
		{
			nlapiLogExecution('Debug','GET method start');
			
			o_form_obj.addFieldGroup('days','Duration');
            var startdate = o_form_obj.addField('fromdate','Date','From').setMandatory(true);
            var enddate = o_form_obj.addField('todate','Date','To').setMandatory(true);
			nlapiLogExecution('debug','Start date',startdate);
			nlapiLogExecution('debug','End date',enddate);
            o_form_obj.addSubmitButton('Submit');
		}
		else
	{
		//var o_form_obj = nlapiCreateForm("Skill Update Dashboard");
		
		var start_date = request.getParameter('fromdate');
		var st_date = nlapiStringToDate(start_date);
		var end_date = request.getParameter('todate');
		var en_date = nlapiStringToDate(end_date);
		var emp_first_trigger_list = new Array(); //No. of new joiners got the 1st trigger
		var new_joiner_update_skill_list = new Array();//No. of new joiners updated skill
		var emp_last_trigger_list = new Array();//No. of resources pending after 3 triggers
		var emp_update_skill_list = new Array();//No. of resources skills updated
		var pending_rm_first_list = new Array();//No. of resources skills validation pending with managers 
		var pending_rm_list = new Array();//No. of resources skills validation pending with managers after 3 triggers
		
		var day = nlapiAddDays(st_date , -5);
		
		//Check new joiners 1st trigger
		
		var emp_new_joiners = nlapiSearchRecord("employee",null,[
							["custentity_employee_inactive","is","F"],"AND", 
							["custentity_implementationteam","is","F"], "AND", 
							["isinactive","is","F"], "AND", 
							//["custentity_skill_updated","is","F"], "AND", 
							["systemnotes.type","is","T"],"AND", 
							["custentity_actual_hire_date","notbefore",st_date],'AND',
							["custentity_actual_hire_date","notafter",en_date],'AND',
							[["department","anyof","485","191"],"OR",
							["department.custrecord_parent_practice","anyof","492"]]], 
							
								[new nlobjSearchColumn("entityid").setSort(false),
								new nlobjSearchColumn("email"), 
								new nlobjSearchColumn("custentity_empid"),
								new nlobjSearchColumn("custentity_fusion_empid"),		
								new nlobjSearchColumn("custentity_reportingmanager"), 
								new nlobjSearchColumn('firstname'),
								new nlobjSearchColumn('email','custentity_reportingmanager'),
								new nlobjSearchColumn('department'),
								new nlobjSearchColumn("custentity_actual_hire_date")]);
		//nlapiLogExecution('Debug','First Trigger Length' , emp_new_joiners.length);		

		/*var emp_first_update = nlapiSearchRecord("employee",null,[
							["custentity_employee_inactive","is","F"],"AND", 
							["custentity_implementationteam","is","F"], "AND", 
							["isinactive","is","F"], "AND", 
							["custentity_skill_updated","is","T"], "AND", 
							["systemnotes.type","is","T"],"AND", 
							["systemnotes.date","notbefore",st_date],'AND',
							["systemnotes.date","notafter",en_date],'AND',
							[["department","anyof","485","191"],"OR",
							["department.custrecord_parent_practice","anyof","492"]]], 
							
								[new nlobjSearchColumn("entityid").setSort(false),
								new nlobjSearchColumn("email"), 
								new nlobjSearchColumn("custentity_empid"),
								new nlobjSearchColumn("custentity_fusion_empid"),		
								new nlobjSearchColumn("custentity_reportingmanager"), 
								new nlobjSearchColumn('firstname'),
								new nlobjSearchColumn('email','custentity_reportingmanager'),
								new nlobjSearchColumn('department'),
								new nlobjSearchColumn("custentity_actual_hire_date")]);		
								*/
		var emp_final_trigger = nlapiSearchRecord("employee",null,[
							["custentity_employee_inactive","is","F"],"AND", 
							["custentity_implementationteam","is","F"], "AND", 
							["isinactive","is","F"], "AND", 
							["custentity_skill_updated","is","F"], "AND", 
							["systemnotes.type","is","T"],"AND", 
							["custentity_actual_hire_date","notbefore",st_date],'AND',
							["custentity_actual_hire_date","notafter",en_date],'AND',
							[["department","anyof","485","191"],"OR",
							["department.custrecord_parent_practice","anyof","492"]]], 
								[new nlobjSearchColumn("entityid").setSort(false),
								new nlobjSearchColumn("email"), 
								new nlobjSearchColumn("custentity_empid"), 
								new nlobjSearchColumn("custentity_fusion_empid"),
								new nlobjSearchColumn("custentity_reportingmanager"), 
								new nlobjSearchColumn('firstname'),
								new nlobjSearchColumn('email','custentity_reportingmanager'),
								new nlobjSearchColumn('department'),
								new nlobjSearchColumn("custentity_actual_hire_date")]);	
								
		//nlapiLogExecution('Debug','Final Trigger Length' , emp_final_trigger.length);	
		
		var emp_skill_updated = nlapiSearchRecord("employee",null,[
							["custentity_employee_inactive","is","F"],"AND", 
							["custentity_implementationteam","is","F"], "AND", 
							["isinactive","is","F"], "AND", 
							["custentity_skill_updated","is","T"], "AND", 
							["custentity_actual_hire_date","notbefore",st_date],'AND',
							["custentity_actual_hire_date","notafter",en_date],'AND',
							[["department","anyof","485","191"],"OR",
							["department.custrecord_parent_practice","anyof","492"]]], 
								[new nlobjSearchColumn("entityid").setSort(false),
								new nlobjSearchColumn("email"), 
								new nlobjSearchColumn("custentity_empid"), 
								new nlobjSearchColumn("custentity_fusion_empid"),
								new nlobjSearchColumn("custentity_reportingmanager"), 
								new nlobjSearchColumn('firstname'),
								new nlobjSearchColumn('email','custentity_reportingmanager'),
								new nlobjSearchColumn('department'),
								new nlobjSearchColumn("custentity_actual_hire_date")]);
		
		//nlapiLogExecution('Debug','Employee Skill Update Length' , emp_skill_updated.length);	
								
		/*var pending_manager_approval = nlapiSearchRecord("customrecord_employee_master_skill_data",null,[
							["systemnotes.type","is","T"], 
							"AND", 
							["custrecord_skill_status","anyof","1"]], 
							[new nlobjSearchColumn("entityid","custrecord_employee_skill_updated"), 
							new nlobjSearchColumn("internalid"),
							new nlobjSearchColumn("email","custrecord_employee_approver"), 
							new nlobjSearchColumn("entityid","custrecord_employee_approver"),
							new nlobjSearchColumn("custentity_reportingmanager","CUSTRECORD_EMPLOYEE_APPROVER",null)]);	
		//nlapiLogExecution('Debug','Pending Manager Approval Length' , pending_manager_approval.length);	*/					
		
		var pending_manager_approval_3 = nlapiSearchRecord("customrecord_employee_master_skill_data",null,[
							["systemnotes.type","is","T"], 
							"AND", 
							["custrecord_employee_skill_updated.custentity_actual_hire_date","notbefore",st_date],  
							"AND", 
							["custrecord_employee_skill_updated.custentity_actual_hire_date","notafter",en_date], 
							"AND", 
							["custrecord_skill_status","anyof","1"]], 
							[new nlobjSearchColumn("entityid","custrecord_employee_skill_updated"), 
							new nlobjSearchColumn("internalid"),
							new nlobjSearchColumn("email","custrecord_employee_approver"), 
							new nlobjSearchColumn("entityid","custrecord_employee_approver"),
							new nlobjSearchColumn("custentity_reportingmanager","CUSTRECORD_EMPLOYEE_APPROVER",null)]);	
			
		//nlapiLogExecution('Debug','Pending Manager Approval 3 Length' , pending_manager_approval_3.length);	
		
	    if(emp_new_joiners)
		{
			var first_trigger_json = {};
			
			for(var i_first_trigger = 0 ; i_first_trigger < emp_new_joiners.length ; i_first_trigger++)
			{
				first_trigger_json = {
					resource_first : emp_new_joiners[i_first_trigger].getValue('entityid'),
					email_first : emp_new_joiners[i_first_trigger].getValue('email'),
					empid_first : emp_new_joiners[i_first_trigger].getValue('custentity_fusion_empid'),
					reportingmanager_first : emp_new_joiners[i_first_trigger].getText('custentity_reportingmanager'),
					reptmngrmail_first :  emp_new_joiners[i_first_trigger].getValue('email','custentity_reportingmanager'),
					department_first : emp_new_joiners[i_first_trigger].getText('department'),
					hire_date_first : emp_new_joiners[i_first_trigger].getValue('custentity_actual_hire_date')
				}
			emp_first_trigger_list.push(first_trigger_json);
			nlapiLogExecution('DEBUG', 'Search Result Employee First Trigger : ', JSON.stringify(emp_first_trigger_list));
			}
			
			//Create sublist
			var new_joiner_1_sublist = o_form_obj.addSubList('first_trigger_sublist','list','New Joiners List');
			new_joiner_1_sublist.addField('resource_first','text','Resource').setDisplayType('inline');
			new_joiner_1_sublist.addField('email_first','text','Email').setDisplayType('inline');
			new_joiner_1_sublist.addField('empid_first','text','Fusion ID').setDisplayType('inline');
			new_joiner_1_sublist.addField('reportingmanager_first','text','Reporting Mangaer').setDisplayType('inline');
			new_joiner_1_sublist.addField('reptmngrmail_first','text','Reporting Manager Mail').setDisplayType('inline');
			new_joiner_1_sublist.addField('department_first','text','Practice').setDisplayType('inline');
			new_joiner_1_sublist.addField('hire_date_first','text','Hire Date').setDisplayType('inline');
			new_joiner_1_sublist.setLineItemValues(emp_first_trigger_list);
		}	
		
		
		/*if(emp_first_update)
		{
			var first_trigger_skill_json = {};
			
			for(var i_first_trigger_skill = 0 ; i_first_trigger_skill < emp_new_joiners.length ; i_first_trigger_skill++)
			{
				first_trigger_skill_json = {
					resource_first_update : emp_new_joiners[i_first_trigger_skill].getValue('entityid'),
					email_first_update : emp_new_joiners[i_first_trigger_skill].getValue('email'),
					empid_first_update : emp_new_joiners[i_first_trigger_skill].getValue('custentity_fusion_empid'),
					reportingmanager_first_update : emp_new_joiners[i_first_trigger_skill].getText('custentity_reportingmanager'),
					reptmngrmail_first_update :  emp_new_joiners[i_first_trigger_skill].getValue('email','custentity_reportingmanager'),
					department_first_update : emp_new_joiners[i_first_trigger_skill].getText('department'),
					hire_date_first_update : emp_new_joiners[i_first_trigger_skill].getValue('custentity_actual_hire_date')
				}
			new_joiner_update_skill_list.push(first_trigger_json);
			nlapiLogExecution('DEBUG', 'Search Result Employee First Trigger Skill uploaded : ', JSON.stringify(new_joiner_update_skill_list));
			}
			
			//Create sublist
			var new_joiner_skill_sublist = o_form_obj.addSubList('first_employee_skill_sublist','list','New Joiners Skill Uploaded');
			new_joiner_skill_sublist.addField('resource_first_update','text','Resource').setDisplayType('inline');
			new_joiner_skill_sublist.addField('email_first_update','text','Email').setDisplayType('inline');
			new_joiner_skill_sublist.addField('empid_first_update','text','Fusion ID').setDisplayType('inline');
			new_joiner_skill_sublist.addField('reportingmanager_first_update','text','Reporting Mangaer').setDisplayType('inline');
			new_joiner_skill_sublist.addField('reptmngrmail_first_update','text','Reporting Manager Mail').setDisplayType('inline');
			new_joiner_skill_sublist.addField('department_first_update','text','Practice').setDisplayType('inline');
			new_joiner_skill_sublist.addField('hire_date_first_update','text','Hire Date').setDisplayType('inline');
			new_joiner_skill_sublist.setLineItemValues(new_joiner_update_skill_list);
		}
		*/
	    if(emp_final_trigger)
		{
			var final_trigger_json = {};
			
			for(var i_final_trigger = 0 ; i_final_trigger < emp_final_trigger.length ; i_final_trigger++)
			{
				final_trigger_json = {
					resource_final : emp_final_trigger[i_final_trigger].getValue('entityid'),
					email_final : emp_final_trigger[i_final_trigger].getValue('email'),
					empid_final : emp_final_trigger[i_final_trigger].getValue('custentity_fusion_empid'),
					reportingmanager_final : emp_final_trigger[i_final_trigger].getText('custentity_reportingmanager'),
					reptmngrmail_final :  emp_final_trigger[i_final_trigger].getValue('email','custentity_reportingmanager'),
					department_final : emp_final_trigger[i_final_trigger].getText('department'),
					hire_date_final : emp_final_trigger[i_final_trigger].getValue('custentity_actual_hire_date')
				}
			emp_last_trigger_list.push(final_trigger_json);
			nlapiLogExecution('DEBUG', 'Search Result Employee Final Trigger: ', JSON.stringify(emp_last_trigger_list));
			}
			
			
			//Create Sublist
			var emp_final_sublist = o_form_obj.addSubList('final_trigger_sublist','list','Skill Pending');
			emp_final_sublist.addField('resource_final','text','Resource').setDisplayType('inline');
			emp_final_sublist.addField('email_final','text','Email').setDisplayType('inline');
			emp_final_sublist.addField('empid_final','text','Fusion ID').setDisplayType('inline');
			emp_final_sublist.addField('reportingmanager_final','text','Reporting Mangaer').setDisplayType('inline');
			emp_final_sublist.addField('reptmngrmail_final','text','Reporting Manager Mail').setDisplayType('inline');
			emp_final_sublist.addField('department_final','text','Practice').setDisplayType('inline');
			emp_final_sublist.addField('hire_date_final','text','Hire Date').setDisplayType('inline');
			emp_final_sublist.setLineItemValues(emp_last_trigger_list);
		}
		
		
		
		if(emp_skill_updated)
		{
			var skill_updated_json = {};
			
			for(var i_skill_update = 0 ; i_skill_update < emp_skill_updated.length ; i_skill_update++)
			{
				skill_updated_json = {
					resource_update : emp_skill_updated[i_skill_update].getValue('entityid'),
					email_update : emp_skill_updated[i_skill_update].getValue('email'),
					empid_update : emp_skill_updated[i_skill_update].getValue('custentity_fusion_empid'),
					reportingmanager_update : emp_skill_updated[i_skill_update].getText('custentity_reportingmanager'),
					reptmngrmail_update :  emp_skill_updated[i_skill_update].getValue('email','custentity_reportingmanager'),
					department_update : emp_skill_updated[i_skill_update].getText('department'),
					hire_date_update : emp_skill_updated[i_skill_update].getValue('custentity_actual_hire_date')
				}
			emp_update_skill_list.push(skill_updated_json);
			nlapiLogExecution('DEBUG', 'Search Result Skill Updated : ', JSON.stringify(emp_update_skill_list));
			}
			
			
			//Create Sublist
			var resource_pending_sublist = o_form_obj.addSubList('skill_updated_sublist','list','Skill Updated');
			resource_pending_sublist.addField('resource_update','text','Resource').setDisplayType('inline');
			resource_pending_sublist.addField('email_update','text','Email').setDisplayType('inline');
			resource_pending_sublist.addField('empid_update','text','Fusion ID').setDisplayType('inline');
			resource_pending_sublist.addField('reportingmanager_update','text','Reporting Mangaer').setDisplayType('inline');
			resource_pending_sublist.addField('reptmngrmail_update','text','Reporting Manager Mail').setDisplayType('inline');
			resource_pending_sublist.addField('department_update','text','Practice').setDisplayType('inline');
			resource_pending_sublist.addField('hire_date_update','text','Hire Date').setDisplayType('inline');
			resource_pending_sublist.setLineItemValues(emp_update_skill_list);
		}
		
		/*if(pending_manager_approval)
		{
			var pending_manager_json = {};
			
			for(var i_pending_mngr = 0 ; i_pending_mngr < pending_manager_approval.length ; i_pending_mngr++)
			{
				pending_manager_json = {
					emp_name : pending_manager_approval[i_pending_mngr].getValue("entityid","custrecord_employee_skill_updated"),
					manager_email : pending_manager_approval[i_pending_mngr].getValue("email","custrecord_employee_approver"),
					manager_name : pending_manager_approval[i_pending_mngr].getValue("entityid","CUSTRECORD_EMPLOYEE_APPROVER",null)
				}
			pending_rm_first_list.push(pending_manager_json);
			nlapiLogExecution('DEBUG', 'Search Result Pending RM : ', JSON.stringify(pending_rm_first_list));	
			}
		var pending_manager_sublist = o_form_obj.addSubList('pending_manager_list','list','Skill Pending Manager Approval');
		pending_manager_sublist.addField('emp_name','text','Resource Name').setDisplayType('inline');
		pending_manager_sublist.addField('manager_email','text','Manager Email').setDisplayType('inline');
		pending_manager_sublist.addField('manager_name','text','Manager Name').setDisplayType('inline');
        pending_manager_sublist.setLineItemValues(pending_rm_first_list);		
		}*/
		
		if(pending_manager_approval_3)
		{
			var pending_manager_json = {};
			
			for(var i_pending_mngr_3 = 0 ; i_pending_mngr_3 < pending_manager_approval_3.length ; i_pending_mngr_3++)
			{
				pending_manager_json = {
					emp_name_3 : pending_manager_approval_3[i_pending_mngr_3].getValue("entityid","custrecord_employee_skill_updated"),
					manager_email_3 : pending_manager_approval_3[i_pending_mngr_3].getValue("email","custrecord_employee_approver"),
					manager_name_3 : pending_manager_approval_3[i_pending_mngr_3].getValue("entityid","CUSTRECORD_EMPLOYEE_APPROVER",null)
				}
			pending_rm_list.push(pending_manager_json);
			nlapiLogExecution('DEBUG', 'Search Result Pending RM3: ', JSON.stringify(pending_rm_list));	
			}
		var pending_manager_sublist_3 = o_form_obj.addSubList('pending_manager_list_3','list','Pending Manager Approval');
		pending_manager_sublist_3.addField('emp_name_3','text', 'Resource Name').setDisplayType('inline');
		pending_manager_sublist_3.addField('manager_email_3','text','Manager Email').setDisplayType('inline');
		pending_manager_sublist_3.addField('manager_name_3','text','Manager Name').setDisplayType('inline');	
		pending_manager_sublist_3.setLineItemValues(pending_rm_list);
		}
		
	}
	response.writePage(o_form_obj);
	
	
}
	catch(err)
	{
		nlapiLogExecution('Debug','Error in Script',err);
	}
}