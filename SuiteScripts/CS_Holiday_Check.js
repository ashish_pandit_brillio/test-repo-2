/**
 * Check if the week has a holiday, then show user a prompt in case he has not
 * applied it
 * 
 * In case the user applies a leave, show him a message to update the same in
 * fusion
 * 
 * Version Date Author Remarks 1.00 29 Sep 2015 nitish.mishra
 * Version Date Author Remarks 2.00 16 Mar 2020 Praveena Madem Changed iternalids of time sheet sublist ids
 */

function holidayCheckPageInit(type) {

	if (type == "edit" || type == "create") {
		if (nlapiGetFieldValue('custpage_week_has_holiday') == 'T') {
        /*  var main_div = document.getElementsByClassName("uir-page-title")[0];
			var newDiv = document.createElement("div");
			newDiv.innerHTML = "<span style='color:red;font-size:15px;float:right;'><b>The week has a holiday, please update 'Holiday' manually </b></span>";
			main_div.appendChild(newDiv);
            */
          
         
			alert("The week has a holiday, please update 'Holiday' manually");

		
          
		}
	}
}

function holidayCheckSaveRecord() {

	if (nlapiGetFieldValue('custpage_week_has_holiday') == 'T') {
		var holidayList = JSON
		        .parse(nlapiGetFieldValue('custpage_holiday_dates'));

		var holidayAppliedList = nlapiFindLineItemValue('timitem', 'item',
		        '2480');

		if (holidayAppliedList == -1) {
			return confirm("This week has holiday. Are you sure you worked on Holiday and want to submit ?");
		}
	}

	// check if the employee has applied for leave
	var hasAppliedLeave = nlapiFindLineItemValue('timitem', 'item', '2479');

	if (hasAppliedLeave != -1) {
		alert("Kindly update the leave in Fusion as applicable.");
		var newTab = window
		        .open("https://sts.brillio.com/adfs/ls/IdpInitiatedSignOn.aspx");

		if (newTab) {

		} else {
			alert("Please allow pop-up for this site");
		}
	}

	return true;
}