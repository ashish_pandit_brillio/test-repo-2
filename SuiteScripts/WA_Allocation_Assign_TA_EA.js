/**
 * Assign / Update the employee records expense and time approver as the newly
 * allocated project's DM and PM
 * 
 * Version Date Author Remarks 1.00 14 Sep 2015 Nitish Mishra
 * 
 */

function workflowAction() {
	try {
		var s_allocationEndDate = nlapiGetFieldValue('enddate');

		if (s_allocationEndDate) 
		{
			var d_allocationEndDate = nlapiStringToDate(s_allocationEndDate);
			var currentDate = nlapiStringToDate(nlapiDateToString(new Date(),
			        'date'));

			if (d_allocationEndDate >= currentDate) 
			{

				// get the employee
				var employeeId = nlapiGetFieldValue('allocationresource');

				// check if TA / EA field is blank
				var employeeDetails = nlapiLookupField('employee', employeeId,
				        [ 'timeapprover', 'approver',
				                'custentity_reportingmanager' ]);

				// get the project
				var projectId = nlapiGetFieldValue('project');

				// get the PM and DM of the project
				var project_details = nlapiLookupField('job', projectId, [
				        'custentity_projectmanager',
				        'custentity_deliverymanager', 'jobtype' ]);
				var i_Reporting_Manager = employeeDetails.custentity_reportingmanager	
				if(	i_Reporting_Manager != 5706) // Direct Reporters to Raj
				{
					if (project_details.jobtype == 1) 
					{ // Internal
						nlapiSubmitField('employee', employeeId, [ 'timeapprover',
					        'approver' ], [
					        employeeDetails.custentity_reportingmanager,
					        employeeDetails.custentity_reportingmanager ]);
						nlapiLogExecution('AUDIT', 'Updated', 'Internal#'
					        + employeeId);
					} else 
					{ // External
					nlapiSubmitField('employee', employeeId, [ 'timeapprover',
					        'approver' ], [
					        project_details.custentity_projectmanager,
					        project_details.custentity_deliverymanager ]);
					nlapiLogExecution('AUDIT', 'Updated', 'External#'
					        + employeeId);
					}
				}  ////// if(	i_Reporting_Manager != 5706)
					else
					{
						if (project_details.jobtype == 1) 
						{ // Internal
						nlapiSubmitField('employee', employeeId, [ 'timeapprover'], 
						[ employeeDetails.custentity_reportingmanager ]);
						nlapiLogExecution('AUDIT', 'Updated', 'Internal#'
					        + employeeId);
						} else 
						{ // External
							nlapiSubmitField('employee', employeeId, [ 'timeapprover'], 
							[project_details.custentity_projectmanager]);
							nlapiLogExecution('AUDIT', 'Updated', 'External#'
					        + employeeId);
						}
					} //// Else of if ////// if(	i_Reporting_Manager != 5706)
					
				
			} else {
				nlapiLogExecution('DEBUG', 'TA / EA Not Updated',
				        'Past Allocation');
			}

		} else {
			nlapiLogExecution('DEBUG', 'TA / EA Not Updated',
			        'End Date Missing');
		}
	} catch (err) {
		nlapiLogExecution('error', 'workflowAction', err);
	}
}