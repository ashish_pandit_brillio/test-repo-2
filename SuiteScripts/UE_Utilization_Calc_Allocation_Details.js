/**
 * Populate the entry fields based on the allocation
 * 
 * Version Date Author Remarks 1.00 18 Apr 2016 Nitish Mishra
 * 
 */

function userEventBeforeSubmit(type) {
	try {

		if (type == 'create' || type == 'edit' || type == 'xedit') {

			var startDate = nlapiGetFieldValue('custrecord_urt_start_date');
			var endDate = nlapiGetFieldValue('custrecord_urt_end_date');
			var project = nlapiGetFieldValue('custrecord_urt_project');

			if (startDate && endDate && project) {
				var projectDetails = nlapiLookupField('job', project, [
				        'customer', 'custentity_practice' ]);
				nlapiSetFieldValue('custrecord_urt_customer',
				        projectDetails.customer);
				nlapiSetFieldValue('custrecord_urt_executing_practice',
				        projectDetails.custentity_practice);
				var region = nlapiLookupField('customer',
				        projectDetails.customer, 'territory');
				nlapiSetFieldValue('custrecord_urt_region', region);

				var offsiteBilled = 0, offsiteUnbilled = 0, onsiteBilled = 0, onsiteUnbilled = 0;

				var allocationSearch1 = nlapiSearchRecord('resourceallocation',
				        null, [
				                new nlobjSearchFilter('formuladate', null,
				                        'notafter', endDate)
				                        .setFormula('{startdate}'),
				                new nlobjSearchFilter('formuladate', null,
				                        'notbefore', startDate)
				                        .setFormula('{enddate}'),
				                new nlobjSearchFilter('project', null, 'anyof',
				                        project) ], [
				                new nlobjSearchColumn('resource'),
				                new nlobjSearchColumn('startdate'),
				                new nlobjSearchColumn('enddate'),
				                new nlobjSearchColumn('custeventrbillable'),
				                new nlobjSearchColumn('custevent4'),
				                new nlobjSearchColumn('custentity_persontype',
				                        'employee'),
				                new nlobjSearchColumn('percentoftime') ]);

				var totalDays = getDayDiff(startDate, endDate);
				var contractCount = 0, salariedCount = 0;

				if (allocationSearch1) {

					allocationSearch1
					        .forEach(function(allocation) {
						        var personType = allocation.getValue(
						                'custentity_persontype', 'employee');
						        var isOnsite = allocation
						                .getValue('custevent4') == '1';
						        var isBillable = allocation
						                .getValue('custeventrbillable') == 'T';
						        var percentageAllocation = allocation
						                .getValue('percentoftime');

						        var personMonth = getPersonUtilization(
						                allocation.getValue('startdate'),
						                allocation.getValue('enddate'),
						                percentageAllocation, startDate,
						                endDate, totalDays, 8);

						        nlapiLogExecution('debug', allocation
						                .getText('resource'), personMonth);

						        if (isOnsite) {

							        if (isBillable) {
								        onsiteBilled += personMonth;
							        } else {
								        onsiteUnbilled += personMonth;
							        }
						        } else {

							        if (isBillable) {
								        offsiteBilled += personMonth;
							        } else {
								        offsiteUnbilled += personMonth;
							        }
						        }

						        // Contract
						        if (personType == 1) {
							        contractCount += personMonth;
						        } // Employee
						        else if (personType == 2) {
							        salariedCount += personMonth;
						        }
					        });
				}

				nlapiSetFieldValue('custrecord_urt_onsite_billed', onsiteBilled);
				nlapiSetFieldValue('custrecord_urt_onsite_unbilled',
				        onsiteUnbilled);
				nlapiSetFieldValue('custrecord_urt_offsite_billed',
				        offsiteBilled);
				nlapiSetFieldValue('custrecord_urt_offsite_unbilled',
				        offsiteUnbilled);

				var total = onsiteBilled + onsiteUnbilled + offsiteBilled
				        + offsiteUnbilled;

				nlapiSetFieldValue('custrecord_urt_onsite_ratio',
				        ((onsiteBilled + onsiteUnbilled) * 100 / total)
				                .toFixed(2));
				nlapiSetFieldValue('custrecord_urt_offsite_ratio',
				        ((offsiteBilled + offsiteUnbilled) * 100 / total)
				                .toFixed(2));
				nlapiSetFieldValue('custrecord_urt_shadow_percent',
				        ((offsiteUnbilled + onsiteUnbilled) * 100 / total)
				                .toFixed(2));
				nlapiSetFieldValue('custrecord_urt_utilization_percent',
				        ((onsiteBilled + offsiteBilled) * 100 / total)
				                .toFixed(2));

				// Salaried / Contract
				nlapiSetFieldValue('custrecord_urt_contract_count',
				        contractCount);
				nlapiSetFieldValue('custrecord_urt_salaried_count',
				        salariedCount);

				nlapiSetFieldValue('custrecord_urt_contractor_percent',
				        contractCount * 100 / (contractCount + salariedCount));
			} else {
				throw 'Mandatory fields missing';
			}
		}
	} catch (err) {
		nlapiLogExecution('ERROR', 'userEventAfterSubmit', err);
	}
}