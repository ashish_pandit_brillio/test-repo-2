/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       05 May 2015     amol.sahijwani
 * Disable department for all except admin
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType
 *   
 * @param {String} type Operation types: create, edit, view, copy, print, email
 * @param {nlobjForm} form Current form
 * @param {nlobjRequest} request Request object
 * @returns {Void}
 */
function userEventBeforeLoad(type, form, request){
	var first_approver = form.getField('department');
		first_approver.setDisplayType('inline');
}
