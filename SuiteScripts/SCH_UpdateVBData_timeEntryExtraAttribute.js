//BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
	/*
     Script Name	: SCH_JE_Upload_Validate_Process
     Author			: Ashish Pandit
     Company		: Inspirria CloudTech Pvt Ltd
     Date			: 06/08/2018
     
	 
	 Script Modification Log:
     
	 -- Date --               -- Modified By --                  --Requested By--                   -- Description --
     

	 Below is a summary of the process controls enforced by this script file.  The control logic is described
     more fully, below, in the appropriate function headers and code blocks.
     SCHEDULED FUNCTION
     - RFQbody_SCH_main()
     SUB-FUNCTIONS
     - The following sub-functions are called by the above core functions in order to maintain code
     modularization:
     - NOT USED
	 */
}

//END SCRIPT DESCRIPTION BLOCK  ====================================

function scheduled_UpdateRecord_main() 
{
	try
	{
		var context = nlapiGetContext();
		var Rec_Id = 1;//context.getSetting('script', 'custscript_import_rec_id');
		var Rec_Type = 'customrecord561';//context.getSetting('script', 'custscript_record_type');
		
		
		// Call Scheduled script for JE creation
		if(_logValidation(Rec_Id)&&_logValidation(Rec_Type))
		{
			var o_RecObj = nlapiLoadRecord(Rec_Type,Rec_Id);
			var fileId = o_RecObj.getFieldValue('custrecord_file');
			if(_logValidation(fileId))
			{
				var status = updateRecord(fileId,Rec_Type,Rec_Id); //call CSV file read and create records
				nlapiLogExecution('Debug','status ',status);
			}
		}
	
	}
	catch(e)
	{
		nlapiLogExecution('Debug','Exception ',e.message);
	}
}
//================================VALIDATE CSV================================================================


//================================CREATE JE FUNCTION===========================================================
function updateRecord(i_rec_id,Rec_Type,Rec_Id)
{
	try
	{
		if(_logValidation(i_rec_id))
		{
			var primaryFile = nlapiLoadFile(i_rec_id);	
			var fileContent = primaryFile.getValue();
			
			var splitLine = fileContent.split("\n");
			
			nlapiLogExecution('Debug','splitLine.length ',splitLine.length);
			for(var i=1; i<splitLine.length -1; i++)
			{
				var word = (splitLine[i]).split(",");
				
				var i_transactionNumber  = word[0]; 
				var d_billingFrom =  word[1];
				var d_billingTo =  word[2];
				var s_employeeID = word[3];
				
				try
				{
					var d_billingFromNew = d_billingFrom;
					var d_billingToNew = d_billingTo;
					nlapiLogExecution('Audit','d_billingFromNew ',d_billingFromNew);
					nlapiLogExecution('Audit','d_billingToNew ',d_billingToNew);
					var employeeSearch = nlapiSearchRecord("employee",null,
					[
					   ["entityid","is",regExReplacer(s_employeeID)]
					], 
					[
					   new nlobjSearchColumn("internalid")
					]
					);
					if(employeeSearch)
					{
						var empId = employeeSearch[0].getId();
					}
					nlapiLogExecution('debug','s_employeeID: ',s_employeeID);
				    nlapiLogExecution('debug','empId ',empId);
					nlapiLogExecution('debug','i  : ',i);
					if(_logValidation(empId)&&_logValidation(d_billingFromNew)&&_logValidation(d_billingToNew))
					{
						var timeentrySearch = nlapiSearchRecord("timeentry",null,
						[
						   ["employee","anyof",empId], 
						   "AND", 
						   ["date","within",regExReplacer(d_billingFromNew),regExReplacer(d_billingToNew)]
						], 
						[
						   new nlobjSearchColumn("internalid")
						]
						);
						
						if(timeentrySearch)
						{
							nlapiLogExecution('Debug','timeentrySearch Length ',timeentrySearch.length);
							for(var temp =0;temp<timeentrySearch.length; temp++)
							{
								var timeEntryId = timeentrySearch[temp].getId();
								nlapiLogExecution('Audit','timeEntryId ',timeEntryId);
								var customrecord_ts_extra_attributesSearch = nlapiSearchRecord("customrecord_ts_extra_attributes",null,
								[
								   ["custrecord_time_entry_id","anyof",timeEntryId]
								], 
								[
								   new nlobjSearchColumn("custrecord_time_entry_id"), 
								   new nlobjSearchColumn("custrecord_vendor_bill"), 
								   new nlobjSearchColumn("custrecord_provision_created")
								]
								);
								if(customrecord_ts_extra_attributesSearch)
								{
									nlapiLogExecution('Debug','customrecord_ts_extra_attributesSearch ',customrecord_ts_extra_attributesSearch.length);
									var timeEntryAttriId = customrecord_ts_extra_attributesSearch[0].getId();
									var vendorBill = customrecord_ts_extra_attributesSearch[0].getValue('custrecord_vendor_bill');
									if(vendorBill)
									{
										nlapiLogExecution('Debug','Provision Created');
										nlapiSubmitField('customrecord_ts_extra_attributes',timeEntryAttriId,'custrecord_provision_created','T');
									}
									else
									{
										nlapiSubmitField('customrecord_ts_extra_attributes',timeEntryAttriId,'custrecord_provision_created','T');
										var vendorbillSearch = nlapiSearchRecord("vendorbill",null,
										[
										   ["type","anyof","VendBill"], 
										   "AND", 
										   ["transactionnumber","is",i_transactionNumber], 
										   "AND", 
										   ["mainline","is","T"]
										], 
										[
										   new nlobjSearchColumn("internalid")
										]
										);
										if(vendorbillSearch)
										{
											nlapiLogExecution('Debug','Bill Updated');
											nlapiSubmitField('customrecord_ts_extra_attributes',timeEntryAttriId,'custrecord_vendor_bill',vendorbillSearch[0].getId());
										}
									}
									
									nlapiLogExecution('Debug','timeEntryAttriId ',timeEntryAttriId);
								}
								else
								{
									nlapiLogExecution('Debug','i_transactionNumber ',i_transactionNumber);
									var vendorbillSearch = nlapiSearchRecord("vendorbill",null,
										[
										   ["type","anyof","VendBill"], 
										   "AND", 
										   ["transactionnumbertext","is",i_transactionNumber], 
										   "AND", 
										   ["mainline","is","T"]
										], 
										[
										   new nlobjSearchColumn("internalid")
										]
										);
									nlapiLogExecution('Audit','No Time Extra Attribute Record',customrecord_ts_extra_attributesSearch);
									var timeEntryRecNew = nlapiCreateRecord('customrecord_ts_extra_attributes');
									timeEntryRecNew.setFieldValue('custrecord_time_entry_id',timeEntryId);
									if(vendorbillSearch)
										timeEntryRecNew.setFieldValue('custrecord_vendor_bill',vendorbillSearch[0].getId());
									timeEntryRecNew.setFieldValue('custrecord_provision_created','T');
									var recNew = nlapiSubmitRecord(timeEntryRecNew);
									nlapiLogExecution('Audit',' rec New ',recNew);
								}
							}
						}
					}
					
				}
				catch(e)
				{
					nlapiLogExecution('Debug','Exception ',e);
				}
				var context = nlapiGetContext();
				var i_usage_end = context.getRemainingUsage();
				nlapiLogExecution('Debug','i_usage_end ',i_usage_end);
				if (i_usage_end < 500) 
					{
						var rescheduledScript = nlapiYieldScript();
					}
			}
		
		}
	}
	catch(e)
	{
		nlapiLogExecution('Debug','Exception', e);
	}
	
	return 'SUCCESS';
}

function _logValidation(value) 
{
 if(value!='null' && value != null && value != null && value != '' && value != undefined && value != undefined && value != 'undefined' && value!= 'undefined'&& value!= 'NaN' && value != NaN) 
 {
  return true;
 }
 else 
 { 
  return false;
 }
}


function regExReplacer(str)
{
	return str.replace(/\n|\r/g, "");
}