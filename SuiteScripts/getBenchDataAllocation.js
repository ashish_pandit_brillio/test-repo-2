/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       01 Apr 2019     Aazamali Khan
 *
 */

/**
 * @param {Object} dataIn Parameter object
 * @returns {Object} Output object
 */
function postRESTlet(dataIn) {
    try {
        var s_certificate = [];
        var tempAllocatedResources = [];
        var tempBenchResources = [];
        var employeeList = [];

        // Get the user role and search the data accordingly 
        var s_user = dataIn.user;
        var i_user = getUserUsingEmailId(s_user);
		nlapiLogExecution("DEBUG","i_user",i_user);
        // Get Logged In user's executing practice
        var spocSearch = GetPracticeSPOC(i_user);
        var regionSearch = GetRegion(i_user);
        var adminUser = GetAdmin(i_user)//true;
		var delivery_practice = getDeliveryPractices();
		/************************************************/
        // Get the Index for pagination
        //nlapiLogExecution("ERROR","mode",mode);
        var mode = 0;
        var firstIndex , secondIndex ;  
        if (mode != "0") {
            firstIndex = parseInt(mode) * 10;
            secondIndex = firstIndex + 10
        } else {
            firstIndex = 0;
            secondIndex = (firstIndex + 10)// - index;
        }
        /************************************************************************************/
        if (adminUser) {
            var searchResultBench = null;//GetBenchResoucesProjection(null, null, true, null); // Get the resources on bench for 3 months
            var searchResult = GetBenchProjection(null, null, true, null,delivery_practice); // Allocated resource bench projection
        } else if (spocSearch) {
            var i_practice = spocSearch;
			var practiceArray = getChildPractices(i_practice);
			nlapiLogExecution("AUDIT","practiceArray : ",JSON.stringify(practiceArray))
            var searchResultBench = null;//GetBenchResoucesProjection(i_user, practiceArray, null, null); // Get the resources on bench for 3 months
            var searchResult = GetBenchProjection(i_user, practiceArray, null, null,delivery_practice); // Allocated resource bench projection
        } else if (regionSearch) {
            var i_region = regionSearch;
            var searchResultBench = null;//GetBenchResoucesProjection(i_user, null, null, i_region); // Get the resources on bench for 3 months
            var searchResult = GetBenchProjection(i_user, null, null, i_region,delivery_practice); // Allocated resource bench projection
        } else {
            var searchResultBench = null;//GetBenchResoucesProjection(i_user, null, null, null); // Get the resources on bench for 3 months
            var searchResult = GetBenchProjection(i_user, null, null, null,delivery_practice); // Allocated resource bench projection
        }
        // From Allocation 
        if (searchResult) {
            for (var i = 0/*firstIndex*/; i < searchResult.length/*secondIndex*/; i++) {
                var jsonObj = {};
                var empId = searchResult[i].getValue("resource", null, "GROUP");
                jsonObj.avaliable = searchResult[i].getValue("percentoftime", null, "GROUP");
                jsonObj.employeename = searchResult[i].getText("resource", null, "GROUP");
                jsonObj.employeeId = searchResult[i].getValue("resource", null, "GROUP");
                jsonObj.designation = searchResult[i].getValue("title", "employee", "GROUP")//nlapiLookupField("employee", empId, "title");
                jsonObj.practice = searchResult[i].getText("department", "employee", "GROUP");
                jsonObj.location = searchResult[i].getText("location", "employee", "GROUP");
                jsonObj.bench = searchResult[i].getValue("enddate", null, "MAX");
                jsonObj.days = "NA";
                tempAllocatedResources.push(jsonObj);
                employeeList.push(searchResult[i].getValue("resource", null, "GROUP"));
            }
        }
        // From Bench 
        if (searchResultBench) {
            for (var i = 0/*firstIndex*/; i < searchResultBench.length/*secondIndex*/; i++) {
                var jsonObj = {};
                var empId = searchResultBench[i].getValue("resource", null, "GROUP");
                jsonObj.avaliable = searchResultBench[i].getValue("percentoftime", null, "GROUP");
                jsonObj.employeename = searchResultBench[i].getText("resource", null, "GROUP");
                jsonObj.employeeId = searchResultBench[i].getValue("resource", null, "GROUP");
                jsonObj.designation = nlapiLookupField("employee", empId, "title");
                jsonObj.practice = nlapiLookupField("employee", empId, "department", true);
                jsonObj.location = nlapiLookupField("employee", empId, "location", true);
                var enddate = searchResultBench[i].getValue("enddate", null, "MAX");
                enddate = nlapiStringToDate(enddate);
                var date = new Date();
                var milliseconds = date - enddate;
                var days = Math.round(milliseconds / (1000 * 60 * 60 * 24));
                jsonObj.bench = nlapiDateToString(enddate, "mm/dd/yyyy");
                jsonObj.days = Math.abs(days);
                tempBenchResources.push(jsonObj);
                employeeList.push(searchResultBench[i].getValue("resource", null, "GROUP"));
            }
        }
		var skillArray = [];
		if(employeeList){
        var skillSearchResult = getFamilySkill(employeeList); // Get the employees skills 
        if (skillSearchResult) {
            for (var j = 0; j < skillSearchResult.length; j++) {
                var jsonObj = {};
                jsonObj.skills = skillSearchResult[j].getText("custrecord_primary_updated") + "," + skillSearchResult[j].getValue("custrecord_secondry_updated");
                jsonObj.employeeId = skillSearchResult[j].getValue("custrecord_employee_skill_updated");
                jsonObj.certificates = skillSearchResult[j].getValue("custrecord_certifcate_det", "CUSTRECORD_EMP_SKILL_PARENT", null);
                skillArray.push(jsonObj);
            }
        }
		}
        var frfSearchResult = GetFRFSoftLock(employeeList); // Get the employees who are softlocked.
        var frfResult = [];
        if (frfSearchResult) {
            for (var j = 0; j < frfSearchResult.length; j++) {
                nlapiLogExecution("ERROR", "frfSearchResult.length", frfSearchResult.length)
                var jsonObj = {};
                jsonObj.status = frfSearchResult[j].getValue("custrecord_frf_details_frf_number");
                jsonObj.employeeId = frfSearchResult[j].getValue("custrecord_frf_details_selected_emp");
                frfResult.push(jsonObj);
            }
        }
        var trainingResult = GetEmployeeTrainings(employeeList); // Get the training assigned to employee.
        var trainingArray = [];
        if (trainingResult) {
            for (var i = 0; i < trainingResult.length; i++) {
                var jsonObj = {};
                jsonObj.trainingassigned = trainingResult[i].getValue("internalid", "CUSTRECORD_TRAINING_EMPLOYEE", "COUNT");
                jsonObj.employeeId = trainingResult[i].getValue("internalid", null, "GROUP");
                trainingArray.push(jsonObj);
            }
        }
        var utilizationSearchResult = GetUtilizationPercent(employeeList); // Get the utilization percent for this year.
        var utilizationArray = [];
        if (utilizationSearchResult) {
            for (var j = 0; j < utilizationSearchResult.length; j++) {
                var jsonObj = {};
                var resourcecEndDate = utilizationSearchResult[j].getValue("enddate", null, "MAX");
                var resourcecStartDate = utilizationSearchResult[j].getValue("startdate", null, "GROUP");
                var resource = utilizationSearchResult[j].getValue("resource", null, "GROUP");
                jsonObj.resourceenddate = resourcecEndDate;
                jsonObj.resourcestartdate = resourcecStartDate;
                jsonObj.employeeId = resource;
                //jsonObj.days = Math.ceil((current - previous + 1));
                jsonObj.percentofTime = utilizationSearchResult[j].getValue("percentoftime", null, "GROUP");
                //jsonObj.utilizationpercent = GetPercentOfUtilization(resourcecStartDate, resourcecEndDate); // calculate the percent of utilizatio on the basis of allocation
                utilizationArray.push(jsonObj);
            }

        }
        var visaDetailsResult = GetVisaDetails(employeeList); // Get the visa details 
        var visaDetailsArray = [];
        if (visaDetailsResult) {
            for (var i = 0; i < visaDetailsResult.length; i++) {
                var jsonObj = {};
                jsonObj.visatype = visaDetailsResult[i].getText("custrecord_visa", "CUSTRECORD_VISAID", null);
                jsonObj.visavalidfrom = visaDetailsResult[i].getValue("custrecord27", "CUSTRECORD_VISAID", null);
                jsonObj.visavalidto = visaDetailsResult[i].getValue("custrecord_validtill", "CUSTRECORD_VISAID", null);
                jsonObj.employeeId = visaDetailsResult[i].getId();
                visaDetailsArray.push(jsonObj);
            }
        }
        return {
            "dataOut": {
                "allocatedresources": tempAllocatedResources,
                "benchresources": tempBenchResources,
                "skills": skillArray,
                "frfstatus": frfResult,
                "trainings": trainingArray,
                "utilization": utilizationArray,
                "visadetails": visaDetailsArray
            }
        }
    } catch (e) {
        return {
            "code": "error",
            "message": e
        }
    }
}

function GetPercentOfUtilization(resourcecStartDate, resourcecEndDate) {

    var todaysDate = GetTodaysDate();
    var startOfYear = getEndDate();//"01/01/" + new Date().getFullYear();
    //nlapiLogExecution("ERROR","startOfYear : ",startOfYear);
    var diff1 = nlapiStringToDate(resourcecStartDate) - nlapiStringToDate(startOfYear);
    var diff1days = Math.round(diff1 / (1000 * 60 * 60 * 24));
    if (diff1days < 0) {
        diff1days = 0;
    }
    var diff2 = 0;
    var diff2days = 0;
    if (nlapiStringToDate(todaysDate) > nlapiStringToDate(resourcecEndDate)) {
        var diff2 = nlapiStringToDate(resourcecEndDate) - nlapiStringToDate(todaysDate);
        var diff2days = Math.round(diff1 / (1000 * 60 * 60 * 24));
        if (diff2days < 0) {
            diff2days = 0;
        }
    }
    // total days different 
    var totalDiffDays = parseInt(diff1days) + parseInt(diff2days);
    var denomnator = Math.round((nlapiStringToDate(todaysDate) - nlapiStringToDate(startOfYear)) / (1000 * 60 * 60 * 24))
   // nlapiLogExecution("ERROR", "denomnator : ", denomnator);
    var utilizationPercent = (totalDiffDays / denomnator) * 100;
    return utilizationPercent;
}

function GetBenchProjection(i_user, i_practice, isAdmin, i_region,delivery_practice) {
    var todaysDate = GetTodaysDate();
    var nextDate = GetNextDate();
    var filters = [];
    var columns = [];
    if (isAdmin == true) {
        filters = [
           ["enddate", "within", todaysDate, nextDate],"AND",
		   ["job.custentity_project_allocation_category", "noneof", "4"],"AND",
		   ["employee.department","anyof",delivery_practice]
        ] // Need to add closed filter
    } else if (i_user && i_practice) {
		//i_practice = getSkillIds(i_practice);
		i_practice = JSON.stringify(i_practice);
        filters = [
            [
                ["enddate", "within", todaysDate, nextDate],
                "AND",
                ["job.custentity_project_allocation_category", "noneof", "4"],
				"AND",
				["employee.department","anyof",delivery_practice]
            ],
            "AND", [
                ["job.custentity_projectmanager", "anyof", i_user],
                "OR",
                ["resource.custentity_executingpractice", "anyof", i_practice],
                "OR",
                ["job.custentity_deliverymanager", "anyof", i_user]
            ]
        ]
    } else if (i_user) {
        
             filters = [
            [
                ["enddate", "within", todaysDate, nextDate],
                "AND",
                ["job.custentity_project_allocation_category", "noneof", "4"],
				"AND",
				["employee.department","anyof",delivery_practice]
            ], "AND",
            [
                ["job.custentity_projectmanager", "anyof", i_user],
                "OR",
                ["job.custentity_deliverymanager", "anyof", i_user],
                "OR",
                ["job.custentity_clientpartner", "anyof", i_user]
            ]
			]
        
    } else if (i_region) {
         filters = [
            ["enddate", "within", todaysDate, nextDate],
            "AND",
            ["job.custentity_project_allocation_category", "noneof", "4"],
			"AND",
			["employee.department","anyof",delivery_practice]
        ];
    }
    columns = [
        new nlobjSearchColumn("resource", null, "GROUP"),
        new nlobjSearchColumn("enddate", null, "MAX"),
        new nlobjSearchColumn("percentoftime", null, "GROUP"),
        new nlobjSearchColumn("custentity_project_allocation_category", "job", "GROUP"),
		new nlobjSearchColumn("department", "employee", "GROUP"),
		new nlobjSearchColumn("title", "employee", "GROUP"),
		new nlobjSearchColumn("location", "employee", "GROUP")
    ]
    var resourceallocationSearch = searchRecord("resourceallocation", null, filters, columns, null);
	nlapiLogExecution("AUDIT","resourceallocationSearch length",resourceallocationSearch.length);
    return resourceallocationSearch;
}

function GetFRFSoftLock(empIds) {
    var customrecord_frf_detailsSearch = nlapiSearchRecord("customrecord_frf_details", null,
        [
            ["custrecord_frf_details_selected_emp", "anyof", empIds],"AND",
			["custrecord_frf_details_status_flag","anyof","2"]
        ],
        [
            new nlobjSearchColumn("custrecord_frf_details_frf_number"),
            new nlobjSearchColumn("custrecord_frf_details_selected_emp")
        ]
    );
    return customrecord_frf_detailsSearch;
}

function GetBenchResoucesProjection(i_user, i_practice, isAdmin, i_region) {
    var todaysDate = GetTodaysDate();
	nlapiLogExecution("AUDIT","todaysDate : ",todaysDate);
    var nextDate = GetNextDate();
	nlapiLogExecution("AUDIT","nextDate : ",nextDate);
    var columns = [];
    var filters = [];
    if (isAdmin == true) {
        filters = [
            ["enddate", "within", todaysDate, nextDate],
            "AND",
            ["job.custentity_project_allocation_category", "anyof", "4"]
        ];
    } else if (i_user && i_practice) {
		//i_practice = getSkillIds(i_practice);
		i_practice = JSON.stringify(i_practice);
        filters = [
            [
                ["enddate", "within", todaysDate, nextDate],
                "AND",
                ["job.custentity_project_allocation_category", "anyof", "4"]
            ],
            "AND", [
                ["job.custentity_projectmanager", "anyof", i_user],
                "OR",
                ["resource.custentity_executingpractice", "anyof", i_practice],
                "OR",
                ["job.custentity_deliverymanager", "anyof", i_user]
            ]
        ];
    } else if (i_user) {
        filters = [
            [
                ["enddate", "within", todaysDate, nextDate],
                "AND",
                ["job.custentity_project_allocation_category", "anyof", "4"]
            ], "AND",
            [
                ["job.custentity_projectmanager", "anyof", i_user],
                "OR",
                ["job.custentity_deliverymanager", "anyof", i_user],
                "OR",
                ["job.custentity_clientpartner", "anyof", i_user]
            ]
        ];
    } else if (i_region) {
        filters = [
            ["enddate", "within", todaysDate, nextDate],
            "AND",
            ["job.custentity_project_allocation_category", "anyof", "4"]
        ];
    }
    var columns = [
        new nlobjSearchColumn("resource", null, "GROUP"),
        new nlobjSearchColumn("enddate", null, "MAX"),
        new nlobjSearchColumn("percentoftime", null, "GROUP")
    ];
    var resourceSearch = searchRecord("resourceallocation", null, filters, columns, null);
	nlapiLogExecution("AUDIT","Bench result count :",resourceSearch.length);
    return resourceSearch;
}

function getFamilySkill(employeeIds) {
    var customrecord_employee_master_skill_dataSearch = nlapiSearchRecord("customrecord_employee_master_skill_data", null,

        [

            ["custrecord_employee_skill_updated", "anyof", employeeIds]

        ],
        [
            new nlobjSearchColumn("internalid"),
            new nlobjSearchColumn("custrecord_primary_updated"),
            new nlobjSearchColumn("custrecord_secondry_updated"),
            new nlobjSearchColumn("custrecord_family_selected"),
            new nlobjSearchColumn("custrecord_employee_skill_updated"),
            new nlobjSearchColumn("custrecord_certifcate_det", "CUSTRECORD_EMP_SKILL_PARENT", null),
            new nlobjSearchColumn("created").setSort(true)
        ]

    );

    return customrecord_employee_master_skill_dataSearch;
}

function GetTodaysDate() {
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth() + 1;
    var yyyy = today.getFullYear();
    return mm + "/" + dd + "/" + yyyy;
}

function GetNextDate() {
    var todaysDate = GetTodaysDate();
    var date = nlapiStringToDate(todaysDate, "mm/dd/yyyy");
    var nextDate = nlapiAddDays(date, 120)
    return nlapiDateToString(nextDate, "mm/dd/yyyy");
}

function GetEmployeeTrainings(empIds) {
    var employeeSearch = nlapiSearchRecord("employee", null,
        [
            ["internalid", "anyof", empIds]
        ],
        [
            new nlobjSearchColumn("internalid", "CUSTRECORD_TRAINING_EMPLOYEE", "COUNT").setSort(false),
            new nlobjSearchColumn("internalid", null, "GROUP")
        ]
    );
    return employeeSearch;
}
function GetUtilizationPercent(empIds) {
    var startofyear = "01/02/" + new Date().getFullYear();
    var endofyear = "12/31/" + new Date().getFullYear();
    var resourceallocationSearch = nlapiSearchRecord("resourceallocation", null,
        [
            ["enddate", "within", startofyear, endofyear],
            "AND",
            ["resource", "anyof", empIds],
            "AND",
            ["job.custentity_project_allocation_category", "noneof", "4"]
        ],
        [
            new nlobjSearchColumn("resource", null, "GROUP"),
            new nlobjSearchColumn("enddate", null, "MAX"),
            new nlobjSearchColumn("percentoftime", null, "GROUP"),
            new nlobjSearchColumn("startdate", null, "GROUP")
        ]
    );
    return resourceallocationSearch
}

function GetVisaDetails(empIds) {
    var employeeSearch = nlapiSearchRecord("employee", null,
        [
            ["internalid", "anyof", empIds]
        ],
        [
            new nlobjSearchColumn("custrecord_visa", "CUSTRECORD_VISAID", null),
            new nlobjSearchColumn("custrecord27", "CUSTRECORD_VISAID", null),
            new nlobjSearchColumn("custrecord_validtill", "CUSTRECORD_VISAID", null)
        ]
    );
    return employeeSearch;
}
function getEndDate() {
    var date = new Date();
    var endDate = new Date(date.getFullYear(), date.getMonth() - 12, 0);
    return nlapiDateToString(endDate);
}
function getChildPractices(praticeId){
	var departmentSearch = nlapiSearchRecord("department",null,
[
   ["isinactive","is","F"], 
   "AND", 
   ["custrecord_parent_practice","anyof",praticeId]
], 
[
   new nlobjSearchColumn("name").setSort(false)
]
);
if(departmentSearch){
	var arr = [];
	for(var i = 0 ; i < departmentSearch.length ; i++){
		arr.push(departmentSearch[i].getId());
	}
	return arr;
}else{
	return praticeId;
}
}
//Get Delivery Practices

function getDeliveryPractices(){
	var arr_list = [];
	var departmentSearch = nlapiSearchRecord("department",null,
[
   ["isinactive","is","F"], 
   "AND", 
   ["custrecord_is_delivery_practice","is",'T']
], 
[
   new nlobjSearchColumn("name").setSort(false)
]
);
if(departmentSearch){
	
	for(var i = 0 ; i < departmentSearch.length ; i++){
		arr_list.push(departmentSearch[i].getId());
	}
	
}
nlapiLogExecution('DEBUG','Delivery Practice',arr_list);
nlapiLogExecution('ERROR','type of arr_list',typeof(arr_list));
return arr_list;
}
