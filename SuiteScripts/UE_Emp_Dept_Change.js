/**
 * When an employees department is changed, update all the active and future allocation department as well
 * 
 * Version    Date            Author           Remarks
 * 1.00       02 Jun 2015     Nitish Mishra
 *
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your
 * script deployment.
 * 
 * @appliedtorecord recordType
 * 
 * @param {String}
 *            type Operation types: create, edit, delete, xedit approve, reject,
 *            cancel (SO, ER, Time Bill, PO & RMA only) pack, ship (IF)
 *            markcomplete (Call, Task) reassign (Case) editforecast (Opp,
 *            Estimate)
 * @returns {Void}
 */
function userEventBeforeSubmit(type) {
	try {
		if (type == 'edit') {
			var recOldRecord = nlapiGetOldRecord(), oldDept = recOldRecord
					.getFieldValue('department'), newDept = nlapiGetFieldValue('department');

			if (newDept != oldDept) {
				// get all active or future allocation for the employee
				var resourceAllocationSearch = nlapiSearchRecord(
						'resourceallocation', null, [
								new nlobjSearchFilter('internalid', 'employee',
										'anyof', nlapiGetRecordId()),
								new nlobjSearchFilter('enddate', null,
										'notbefore', 'today') ]);

				if (isArrayNotEmpty(resourceAllocationSearch)) {

					for (var index = 0; index < resourceAllocationSearch.length; index++) {
						var rescAllocRec = nlapiLoadRecord(
								'resourceallocation',
								resourceAllocationSearch[index].getId(), {
									recordmode : 'dynamic'
								});
						rescAllocRec.setFieldValue('custevent_practice',
								newDept);
						rescAllocRec.setFieldValue(
								'custevent_allocation_status', '20');
						rescAllocRec.setFieldValue('notes',
								'Employee Department Updated');
						rescAllocRec
								.setFieldValue('custevent_ra_last_modified_by',
										nlapiGetUser());
						rescAllocRec.setFieldValue(
								'custevent_ra_last_modified_date',
								nlapiDateToString(new Date(), 'datetimetz'));
						nlapiSubmitRecord(rescAllocRec, false, true);
					}
				}
			}
		}
	} catch (err) {
		nlapiLogExecution('ERROR', 'Before Submit', err);
		throw err;
	}
}
