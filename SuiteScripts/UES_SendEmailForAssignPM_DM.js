							
// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
/*
   	Script Name : UES_SendEmailForAssignPM_DM.js
	Author      : Ashish Pandit
	Date        : 08 May 2018
    Description : User Event to send email to Assign PM and DM  


	Script Modification Log:

	-- Date --			-- Modified By --				--Requested By--				-- Description --


Below is a summary of the process controls enforced by this script file.  The control logic is described
more fully, below, in the appropriate function headers and code blocks.


     BEFORE LOAD
		- beforeLoadRecord(type)



     BEFORE SUBMIT
		- beforeSubmitRecord(type)


     AFTER SUBMIT
		- afterSubmitRecord(type)



     SUB-FUNCTIONS
		- The following sub-functions are called by the above core functions in order to maintain code
            modularization:

               - NOT USED

*/
}
// END SCRIPT DESCRIPTION BLOCK  ====================================



// BEGIN GLOBAL VARIABLE BLOCK  =====================================
{
	//  Initialize any Global Variables, in particular, debugging variables...

}
// END GLOBAL VARIABLE BLOCK  =======================================



// BEGIN BEFORE LOAD ==================================================

function afterSubmitSendEmail(type)
{
	if(type == 'create')
	{
		try
		{
			var recId = nlapiGetRecordId();
			var recType = nlapiGetRecordType();
			var recordObject = nlapiLoadRecord(recType,recId);
			var i_pm = recordObject.getFieldValue('custrecord_sfdc_opp_pm');
			var i_dm = recordObject.getFieldValue('custrecord_sfdc_opportunity_dm');
			var s_oppName = recordObject.getFieldValue('custrecord_opportunity_name_sfdc');
			var i_practice = recordObject.getFieldValue('custrecord_practice_internal_id_sfdc');
			var s_account = recordObject.getFieldText('custrecord_customer_internal_id_sfdc');
			var i_account = recordObject.getFieldValue('custrecord_customer_internal_id_sfdc');
			if(i_practice)
			{
				
				var i_pracHead = nlapiLookupField('department',i_practice,'custrecord_practicehead');
				var SendTo = [i_pracHead];
				var fulfillmentSpocs = getAllFulfillmentSpocs(i_practice);
				SendTo = SendTo.concat(fulfillmentSpocs);
				if((!i_pm || !i_dm) && i_pracHead)
				{
					var records = new Array();
					var userName = nlapiLookupField('employee',i_pracHead,'firstname');
					var ccList = getCCList(i_practice,i_account);
					var emailSubject = 'FUEL Notification: Assign DM/PM'
					var emailBody = 'Dear '+userName+'\n Opportunity: '+s_oppName+' has been created for Account : '+s_account+'.\n Please assign DM/PM for fulfillment planning.\n\nBrillio-FUEL\nThis email was sent from a notification only address.\nIf you need further assistance, please write to fuel.support@brillio.com';
					records['entity'] = [94862,41571];
					nlapiLogExecution('Debug','ccList '+ccList,'i_pracHead '+i_pracHead);
					nlapiLogExecution('Debug','userName '+userName,'emailBody '+emailBody);
					nlapiLogExecution('Debug','s_account '+s_account,'s_oppName '+s_oppName);
					nlapiSendEmail(154256, SendTo, emailSubject,emailBody, ccList, null, records);
				}
			}
		}
		catch(error)
		{
			nlapiLogExecution('Debug','Error ',error);
		}
	}
}

// END BEFORE LOAD ====================================================


function _logValidation(value) 
{
 if(value != null && value!= 'null' && value != '' && value != undefined && value != undefined && value != 'undefined' && value.toString() != 'NaN' && value != NaN) 
 {
  return true;
 }
 else 
 { 
  return false;
 }
}
// END FUNCTION =====================================================
function getCCList(i_practice,i_account)
{
	var ccArray = new Array();
	if(i_practice && i_account)
	{
		var customrecord_fuel_delivery_anchorSearch = nlapiSearchRecord("customrecord_fuel_delivery_anchor",null,
		[
		   ["custrecord_fuel_anchor_customer","anyof",i_account], 
		   "AND", 
		   ["custrecord_fuel_anchor_practice","anyof",i_practice]
		], 
		[
		   new nlobjSearchColumn("custrecord_fuel_anchor_employee")
		]
		);
		if(customrecord_fuel_delivery_anchorSearch)
		{
			for(var i=0;i<customrecord_fuel_delivery_anchorSearch.length;i++)
			{
				ccArray.push(customrecord_fuel_delivery_anchorSearch[i].getValue('custrecord_fuel_anchor_employee'));
			}
		}
	}
	if(i_practice)
	{
		var customrecord_practice_spocSearch = nlapiSearchRecord("customrecord_practice_spoc",null,
		[
		   ["custrecord_practice","anyof",i_practice]
		], 
		[
		   new nlobjSearchColumn("custrecord_spoc") 
		]
		);
		if(customrecord_practice_spocSearch)
		{
			for(var i=0;i<customrecord_practice_spocSearch.length;i++)
			{
				ccArray.push(customrecord_practice_spocSearch[i].getValue('custrecord_spoc'));
			}
		}
	}
	return ccArray;
}
function getAllFulfillmentSpocs(practice)
{
	var filter = new Array();
	var all_data= new Array();

	filter.push(new nlobjSearchFilter('custrecord_resource_practice', null, 'is',practice)); 
	filter.push(new nlobjSearchFilter('isinactive', null, 'is','F'));

	var columns = new Array();
	columns.push(new nlobjSearchColumn('email','custrecord_fulfillment_team_members'));


	var results= nlapiSearchRecord('customrecord_fulfillment_spocs', null, filter, columns);
	for(var r = 0; r<results.length;r++)
	{
		all_data.push(results[r].getValue('email','custrecord_fulfillment_team_members'));
	}
	return all_data;
}