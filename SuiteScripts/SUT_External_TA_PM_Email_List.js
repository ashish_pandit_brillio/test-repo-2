/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       04 Aug 2015     nitish.mishra
 *
 */

/**
 * @param {nlobjRequest}
 *            request Request object
 * @param {nlobjResponse}
 *            response Response object
 * @returns {Void} Any output is written via response object
 */
function suitelet(request, response) {
	try {
		var employeeList = [];

		// get TA list
		employeeList = getAllExternalAllocatedTAList();
		nlapiLogExecution('debug', 'TA count', employeeList.length);

		// PM list
		var pmList = getAllExternalProjectManagerList();
		nlapiLogExecution('debug', 'PM count', pmList.length);

		// consolidate the list
		for (var i = 0; i < pmList.length; i++) {
			employeeList.push(pmList[i]);
		}
		nlapiLogExecution('debug', 'consolidate count', employeeList.length);

		// get the email IDs using search on employee record
		var employee_search = searchRecord('employee', null,
				[
						new nlobjSearchFilter('internalid', null, 'anyof',
								employeeList),
						new nlobjSearchFilter('custentity_employee_inactive',
								null, 'is', 'F'),
						new nlobjSearchFilter('custentity_implementationteam',
								null, 'is', 'F') ], [ new nlobjSearchColumn(
						'email') ]);
		nlapiLogExecution('debug', 'email count', employee_search.length);

		// create the list of email id in text file format
		var email_list = '';
		employee_search.forEach(function(emp) {
			email_list += emp.getValue('email');
			email_list += ";";
		});

		// show the file as response
		var file = nlapiCreateFile('TA PM Email List.txt', 'PLAINTEXT',
				email_list);
		response.write(file.getValue());
	} catch (err) {
		nlapiLogExecution('ERROR', 'suitelet', err);
	}
}

function getAllExternalAllocatedTAList() {
	try {
		var list = [];

		searchRecord(null, 948).forEach(
				function(allocation) {
					list.push(allocation.getValue('timeapprover', 'employee',
							'group'));
				});

		return list;
	} catch (err) {
		nlapiLogExecution('ERROR', 'getAllExternalAllocatedTAList', err);
		throw err;
	}
}

function getAllExternalProjectManagerList() {
	try {
		var list = [];

		searchRecord(null, 1013).forEach(
				function(allocation) {
					list.push(allocation.getValue('custentity_projectmanager',
							null, 'group'));
				});

		return list;
	} catch (err) {
		nlapiLogExecution('ERROR', 'getAllExternalProjectManagerList', err);
		throw err;
	}
}