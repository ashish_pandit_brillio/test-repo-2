/**
 * Module Description
 * 
 * Version      Date                         Author                             Remarks  
 * 1.00         4th September 2018          Shamanth k              Employee Learning Trigger Analytics
 */

function Main(){
try{

var columns = new Array();
columns[0]=new nlobjSearchColumn('firstname');
columns[1]=new nlobjSearchColumn('email');
//columns[2]=new nlobjSearchColumn('email','custentity_reportingmanager');
//columns[3]=new nlobjSearchColumn('custrecord_hrbusinesspartner','department');

// search for employee those who are in bench

//var emp_search = nlapiSearchRecord('employee', 'customsearch_learning_trigger_analytics', null,columns);

var emp_search = nlapiSearchRecord('employee', 'customsearch_single_trigger_employee', null,columns);

//nlapiLogExecution('debug', 'emp_search.length',emp_search.length);

if(emp_search){
	
	for(var i=0;i<emp_search.length;i++){
		
		// get the HR BP email id
		//var hr_bp_id = emp_search[i].getValue('custrecord_hrbusinesspartner','department');
		//var hr_bp_email = null;
		
		//if(isNotEmpty(hr_bp_id)){
			//hr_bp_email = nlapiLookupField('employee',hr_bp_id, 'email');
			//hr_bp_firstname = nlapiLookupField('employee',hr_bp_id, 'firstname');
			//hr_bp_lastname = nlapiLookupField('employee',hr_bp_id, 'lastname');
		//}
		
		sendServiceMails(
			emp_search[i].getValue('firstname'),
			emp_search[i].getValue('email'),
			emp_search[i].getId());
	}
}

}
catch(err){
nlapiLogExecution('error', 'Main', err);
}
}

function sendServiceMails(firstName,email,emp_id){
	
	try{
		var mailTemplate = serviceTemplate(firstName,emp_id);		
		nlapiLogExecution('debug', 'chekpoint',email);
      //var employe=nlapiLoadRecord('employee',emp_id);
		//employe.setFieldValue('custentity10','T');
		//nlapiSubmitRecord(employe);
		nlapiSendEmail('16808', email, mailTemplate.MailSubject, mailTemplate.MailBody,['neha.roy@BRILLIO.COM','swetha.b@brillio.com','naveen.alex@brillio.com','rashi.vishnoi@BRILLIO.COM'],null, {entity: emp_id});
	}
	catch(err){
nlapiLogExecution('error', 'sendServiceMails', err);
throw err;
     }
}

function serviceTemplate(firstName,emp_id) {
	//nlapiLogExecution('audit','emp_id',emp_id);  
//var feedbackFormUrl='https://forms.na1.netsuite.com/app/site/hosting/scriptlet.nl?script=953&deploy=1&compid=3883006&h=9e2d89672706b82c5b0c';
	//feedbackFormUrl +='&custparam_employee_id='+emp_id;
	
    var htmltext = '';
    
    htmltext += '<table border="0" width="100%"><tr>';
    htmltext += '<td colspan="4" valign="top">';
    //htmltext += '<p>Hi ' + firstName + ',</p>';  
htmltext += '<p>Dear ' + firstName + ',</p><br>';
//htmltext += '&nbsp';
 
htmltext += '<p>In an effort to keep you abreast with current trends in technology and future demands of the organization, we have launched a training portal which will give you access to multiple courses keeping in mind the latest skills trending in the industry globally</p>';
//htmltext += &nbsp;
htmltext += '<p>These courses would greatly enhance upskilling of your skills in the relevant areas. We encourage you to take the plunge and keep learning to constantly stay ahead of the curve.</p><br>';

htmltext += '<p>L&D will be tracking the progress basis the target end date you choose for yourself. Completion of every course is followed by assessment or a mock live project to gain the practical experience.</p>';
htmltext += '<p>Please find the link to the training portal <a href ="https://brillioonline.sharepoint.com/learningacademy/Lists/AnalyticsUpskilling/Item/newifs.aspx?List=cc5de282-b398-4762-8e0b-3e389eccde06&Source=https%3A%2F%2Fbrillioonline%2Esharepoint%2Ecom%2Flearningacademy%2FLists%2FAnalyticsUpskilling%2FSubmitted_Me%2Easpx&RootFolder=&Web=8d9b86a9-7209-4cb2-a1ba-ace3a264bc3c">  Refresh. Reboot. Reconnect </a></p>';
htmltext += '<p> Happy learning!!!!</p><br>'

//htmltext += '<p>You can reach out to'+" "+ hr_bp_firstname+" "+hr_bp_lastname +" " +'at'+ hr_bp_email+'</p>';
//htmltext += '<p>Here’s wishing you a very successful year ahead and all the best for your future endeavors and contributions to Brillio Technologies Pvt. Ltd.</p>';

htmltext += '<p>Thanks & Regards,</p>';
htmltext += '<p>Learning and Development</p>';
 
 
    htmltext += '</td></tr>';
    htmltext += '</table>';
    htmltext += '<hr width="100%" size="1" noshade color="#CCCCCC">';
    htmltext += '<table width="100%" border="0" cellspacing="0" cellpadding="0">';
    htmltext += '<tr>';
    htmltext += '<td align="right">';
    htmltext += '<font style="font-size:9px; font-family:Verdana,Arial,Helvetica,sans-serif; color:#999999;">Brillio is powered by <a href="http://www.netsuite.com/" style="color:#999999;">NetSuite</a> One System. No Limits.</font>';
    htmltext += '</td>';
    htmltext += '</tr>';
    htmltext += '</table>';
    htmltext += '</body>';
    htmltext += '</html>';
 
    return {
        MailBody : htmltext,
        MailSubject : "Refresh. Reboot. Reconnect"      
    };
}

