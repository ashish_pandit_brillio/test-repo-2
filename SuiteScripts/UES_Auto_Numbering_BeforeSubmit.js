function beforeSubmitRecord_auto_numbering(type)
{
	if(type == 'create')
	{
	var i_record = nlapiGetRecordId();
	var s_recordtype=nlapiGetRecordType();
	nlapiLogExecution('DEBUG', 'afterSubmit_auto_numbering',' Record ID -->' + i_record);
	nlapiLogExecution('DEBUG', 'afterSubmit_auto_numbering',' Record Type -->' + s_recordtype);
	//var record_obj = nlapiLoadRecord(s_recordtype,i_record);
	var customer = nlapiGetFieldText('parent');
	nlapiLogExecution('DEBUG', 'afterSubmit_auto_numbering', ' customer -->' + customer);
	
	if(customer != 'COIN-01 Collabera Inc')
	{	
	if (customer != null && customer != undefined && customer != '') 
	{
		
		var a_split_array = new Array()
		var a_split_array = customer.split('-');
		var cust_prefix = a_split_array[0];
		if (cust_prefix != null && cust_prefix != undefined && cust_prefix != "") 
		{
			//search and load the serial number generation record object
				var o_serialrecord_obj = search_serial_number();
				if (o_serialrecord_obj != null && o_serialrecord_obj != '' && o_serialrecord_obj != undefined) 
				{
					var sequence_no = o_serialrecord_obj.getFieldValue('custrecord_current_sequence_number');
					
					if (sequence_no != null && sequence_no != undefined && sequence_no != '')
					{
						nlapiLogExecution('DEBUG', 'afterSubmit_auto_numbering', ' sequence no -->' + sequence_no);
						var i_next_sequence_no = parseInt(sequence_no) + 1;
						if (i_next_sequence_no.toString().length == 1) 
						{
							i_zero_number = '0000'
						}
						if (i_next_sequence_no.toString().length == 2) 
						{
							i_zero_number = '000'
						}
						if (i_next_sequence_no.toString().length == 3) 
						{
							i_zero_number = '00'
						}
						if (i_next_sequence_no.toString().length == 4) 
						{
							i_zero_number = 0
						}
						if (i_next_sequence_no.toString().length == 5) 
						{
							i_zero_number = ''
						}
						o_serialrecord_obj.setFieldValue('custrecord_current_sequence_number', i_next_sequence_no);
						
						var new_serialnumber = cust_prefix + i_zero_number + i_next_sequence_no;
						nlapiLogExecution('DEBUG', 'beforeSubmit_auto_numbering', ' new serial number -->' + new_serialnumber);
						// set the new serial number on the serial number genration record
						o_serialrecord_obj.setFieldValue('custrecord_current_serial_number', new_serialnumber);
						
					//	nlapiSubmitRecord(o_serialrecord_obj,true,true);
						
						//set the project id of project record
						nlapiSetFieldValue('entityid', new_serialnumber);
						nlapiSetFieldValue('jobtype',2);
						
					}//Sequence No.
				}//Serial OBJ
			
		}//Customer Prefix
		
	}//Customer
	
	}//Collebra Customer
		
	}//CREATE
	
  
	
  return true;
}



function search_serial_number(){
		var filters = new Array();
		var columns = new Array();
		filters[0] = new nlobjSearchFilter('custrecord_purchase_request_type', null, 'is', 'Project');
		columns[0] = new nlobjSearchColumn('internalid');
		var searchresult = nlapiSearchRecord('customrecord_serial_number_generation', null, filters, columns);
		{
			if (searchresult != null && searchresult != '' && searchresult != undefined) {
				var i_recordID = searchresult[0].getValue('internalid');
				nlapiLogExecution('DEBUG', 'afterSubmit_auto_numbering',' Record ID -->' + i_recordID);
				var o_serialrecord = nlapiLoadRecord('customrecord_serial_number_generation', i_recordID);
				return o_serialrecord;
			}
		}
}


