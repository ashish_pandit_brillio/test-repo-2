/**
 * Module Description
 * 
 * Version Date Author Remarks 1.00 13 Mar 2015 nitish.mishra
 * 
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your
 * script deployment.
 * 
 * @appliedtorecord recordType
 * 
 * @param {String}
 *            type Operation types: create, edit, delete, xedit, approve,
 *            cancel, reject (SO, ER, Time Bill, PO & RMA only) pack, ship (IF
 *            only) dropship, specialorder, orderitems (PO only) paybills
 *            (vendor payments)
 * @returns {Void}
 */
function userEventAfterSubmit(type) {

	nlapiLogExecution('debug', 'type', type);

	if (type == 'edit' || type == 'xedit') {

		var previous_expense_approver = nlapiGetOldRecord().getFieldValue(
		        'approver');
		var new_expense_approver = nlapiGetFieldValue('approver');

		if (new_expense_approver && new_expense_approver
		        && previous_expense_approver != new_expense_approver) {
			var expensesList = [];
			nlapiLogExecution('debug', 'nlapiGetRecordId()', nlapiGetRecordId());

			getPendingExpenses(nlapiGetRecordId()).forEach(
			        function(expense) {

				        nlapiSubmitField('expensereport', expense.getValue(
				                'internalid', null, 'group'),
				                'custbody1stlevelapprover',
				                new_expense_approver);
				        expensesList.push({
					        ExpenseName : expense.getValue('tranid', null,
					                'group')
				        });
			        });

			if (new_expense_approver && expensesList.length > 0) {
				notifyNewExpenseApprover(new_expense_approver, expensesList,
				        nlapiLookupField('employee', nlapiGetRecordId(),
				                'entityid'));
			}
		}
	}
}

function notifyNewExpenseApprover(new_expense_approver, expensesList,
        employeeName)
{

	var new_expense_approver_details = nlapiLookupField('employee',
	        new_expense_approver, [ 'firstname', 'email' ]);

	var mailContent = '<table border="0" width="100%">';
	mailContent += '<tr>';
	mailContent += '<td colspan="4" valign="top">';

	mailContent += "<p>Hi "
	        + new_expense_approver_details.firstname
	        + ",</p>"
	        + "<p>This is to inform you that you have been assigned as the expense approver of the employee "
	        + employeeName + ".</p>";

	if (isArrayNotEmpty(expensesList)) {
		mailContent += "<p>Below given expenses raised by "
		        + employeeName
		        + " have been routed to your approval queue. Please take necessary actions. </p>";

		mailContent += "<ul>";
		expensesList.forEach(function(expense) {

			mailContent += "<li>";
			mailContent += expense.ExpenseName;
			mailContent += "</li>";
		});
		mailContent += "</ul>";
	}

	mailContent += "<p>Regards,<br/>";
	mailContent += "Information Systems</p>";

	mailContent += '</td></tr>';
	mailContent += '</table>';
	mailContent += '<hr width="100%" size="1" noshade color="#CCCCCC">';
	mailContent += '<table width="100%" border="0" cellspacing="0" cellpadding="0">';
	mailContent += '<tr>';
	mailContent += '<td align="right">';
	mailContent += '<font style="font-size:9px; font-family:Verdana,Arial,Helvetica,sans-serif; color:#999999;">Brillio is powered by <a href="http://www.netsuite.com/" style="color:#999999;">NetSuite</a> &#151; One System. No Limits.</font>';
	mailContent += '</td>';
	mailContent += '</tr>';
	mailContent += '</table>';

	nlapiSendEmail(constant.Mail_Author.InformationSystem,
	        new_expense_approver_details.email, 'Expense Approver Changed',
	        mailContent, nlapiGetFieldValue('email'), null, {
		        entity : new_expense_approver
	        });
}

function getPendingExpenses(a_employee_list) {

	return searchRecord('expensereport', null, [
	        new nlobjSearchFilter('entity', null, 'anyof', a_employee_list),
	        new nlobjSearchFilter('status', null, 'anyof', [ 'ExpRept:B' ]) ],
	        [ new nlobjSearchColumn('internalid', null, 'group').setSort(true),
	                new nlobjSearchColumn('tranid', null, 'group'),
	                new nlobjSearchColumn('entityid', 'employee', 'group') ]);
}