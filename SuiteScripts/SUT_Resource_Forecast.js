/**
 * Backlog No. :
 * 
 * Report for Resource Forecast Purpose : This file contains code for generating
 * a forecast of allocation The logic used is for every allocation, person month
 * is calculated and displayed The report is designed to show only future
 * allocations.
 * 
 * Backlog Version Date Author Remarks # 1.00 15 Mar 2016 Nitish Mishra
 * 
 */

var file_constants = {
    Default_Forecast_Month : 6,
    Max_Forecast_Month : 9, // Limited to 9 to manage performance
    Email_Recipient : [ 9673, 1525 ], // List of employees to send email
    CSV_File_Name : 'Resource Forecast',
    Report_Name : 'Resource Forecast'
};

function suitelet(request, response) {
	try {
		var user = nlapiGetUser();

		if (user == 41571) {
			user = 113195;
		}

		// get the list of projects the user is tagged to
		var projectList = getTaggedProjectList(user);

		// if no project found, throw an error
		if (projectList.length == 0) {
			throw "You dont have access to this report";
		}

		// create a list of project ID for the filter in the search
		var allProjectId = [];

		// get the project selected by the user
		var selectedProject = request
		        .getParameterValues('custpage_selected_project');
		nlapiLogExecution('debug', 'selected project', JSON
		        .stringify(selectedProject));
		nlapiLogExecution('debug', 'projectList', JSON.stringify(projectList));

		// if a project is selected by the user, pass only that project to the
		// filter
		if (selectedProject && selectedProject != -1
		        && selectedProject.length > 0) {

			// check if the selected project is in the tagged list
			var isProjectTagged = true;

			for (var k = 0; k < selectedProject.length; k++) {
				var projectFound = false;

				projectList.forEach(function(project) {

					if (parseFloat(selectedProject[k]) == parseFloat(project
					        .getId())) {
						allProjectId.push(selectedProject[k]);
						nlapiLogExecution('debug', 'project found');
						projectFound = true;
					}
				});

				if (!projectFound) {
					nlapiLogExecution('debug', 'project not tagged');
					isProjectTagged = false;
					break;
				}
			}

			nlapiLogExecution('debug', 'projectFound', projectFound);

			if (!isProjectTagged) {
				throw "You dont have access to this project";
			}
		} else {
			// else push all the tagged project to the filter array
			projectList.forEach(function(project) {
				allProjectId.push(project.getId());
			});

			if (user == 9673) {
				allProjectId = [];
			}

			// set the selected project as "All" for select option in field
			selectedProject = -1;
		}

		// get the date selected by the user
		var selectedDate = request.getParameter('custpage_selected_date');

		// if no date is selected, take the current date as selected
		if (!selectedDate) {
			selectedDate = nlapiDateToString(new Date(), 'date');
		}

		// get the no. of months entered by the user
		var noOfMonths = request.getParameter('custpage_forecast_month');

		// if not entered, take the default value
		if (!noOfMonths) {
			noOfMonths = file_constants.Default_Forecast_Month;
		}

		// create a list of the months from the selected date to n forecast
		// months
		var arrDates = getForecastMonths(selectedDate, noOfMonths);

		// create a list of dates that will be used to pull the allocations and
		// perform calculations
		var mainObject = getAllocationDetails(arrDates, allProjectId);

		// check the request mode
		var mode = request.getParameter('mode');

		// if mode is CSV, generate a CSV file
		if (mode == 'CSV') {
			var file = nlapiCreateFile(file_constants.CSV_File_Name + '.csv',
			        'CSV', generateCsvFileForForecast(mainObject, arrDates));
			response.setContentType('CSV', file_constants.CSV_File_Name
			        + '.csv');
			response.write(file.getValue());
		} else {
			// else display data on the screen
			var form = nlapiCreateForm(file_constants.Report_Name);
			form.setScript('customscript_cs_sut_resource_forecast');

			form.addFieldGroup('custpage_grp_1', 'Select Options');

			// add project field
			var projectField = form.addField('custpage_selected_project',
			        'multiselect', 'Project', null, 'custpage_grp_1')
			        .setBreakType('startcol');
			// projectField.addSelectOption(-1, 'All');
			projectList.forEach(function(project) {
				projectField.addSelectOption(project.getId(), project
				        .getValue('entityid')
				        + " " + project.getValue('altname'));
			});
			projectField.setBreakType('startcol');
			projectField.setDefaultValue(selectedProject);

			// add data field
			form.addField('custpage_selected_date', 'date', 'From Date', null,
			        'custpage_grp_1').setBreakType('startcol').setDefaultValue(
			        selectedDate);
			form.addField('custpage_forecast_month', 'integer',
			        'No. of Months', null, 'custpage_grp_1').setBreakType(
			        'startcol').setDefaultValue(noOfMonths);

			// generate CSV download link
			var context = nlapiGetContext();
			var url = nlapiResolveURL('SUITELET', context.getScriptId(),
			        context.getDeploymentId())
			        + "&custpage_selected_project="
			        + selectedProject
			        + "&custpage_selected_date="
			        + selectedDate
			        + "&custpage_forecast_month=" + noOfMonths + "&mode=CSV";
			var link = "<a href='"
			        + url
			        + "' target='_blank'>"
			        + "<img style='height: 40px; margin-top: 20px;' src='https://system.na1.netsuite.com/core/media/media.nl?id=145877&c=3883006&h=db69121d869e7d0ba460'></a>";
			form.addField('custpage_csv_link', 'inlinehtml', '', null,
			        'custpage_grp_1').setBreakType('startcol').setDefaultValue(
			        link);

			// generate the table HTML and add it
			form.addFieldGroup('custpage_grp_2', 'Forecast Data');
			form.addField('custpage_1', 'inlinehtml', '', null,
			        'custpage_grp_2').setDefaultValue(
			        generateHtmlScreen(mainObject, arrDates));

			form.addSubmitButton("Refresh");

			// render the form
			response.writePage(form);
		}
	} catch (err) {
		nlapiLogExecution('ERROR', 'suitelet', err);
		displayErrorForm(err, file_constants.Report_Name);
	}
}

function getMonthEndDate(currentDate) {
	return nlapiDateToString(nlapiAddDays(nlapiAddMonths(currentDate, 1), -1),
	        'date');
}

function getMonthStartDate(currentDate) {
	var month = currentDate.getMonth() + 1;
	var year = currentDate.getFullYear();
	var s_startDate = month + "/1/" + year;
	var d_startDate = nlapiStringToDate(s_startDate);
	return d_startDate;
}

function getPersonUtilization(allocationStartDate, allocationEndDate,
        percentageAllocation, entryStartDate, entryEndDate, totalDays,
        hourPerDay)
{
	try {
		var percentageNumeric = (parseFloat(percentageAllocation.split("%")[0]) / 100);

		var d_allocationStartDate = nlapiStringToDate(allocationStartDate);
		var d_allocationEndDate = nlapiStringToDate(allocationEndDate);
		var d_entryStartDate = nlapiStringToDate(entryStartDate);
		var d_entryEndDate = nlapiStringToDate(entryEndDate);

		var entryMonthStartDate = nlapiStringToDate((d_entryStartDate
		        .getMonth() + 1)
		        + '/1/' + d_entryStartDate.getFullYear());
		var nextEntryMonthStartDate = nlapiAddMonths(entryMonthStartDate, 1);
		var entryMonthEndDate = nlapiAddDays(nextEntryMonthStartDate, -1);
		var startDate = d_allocationStartDate > d_entryStartDate ? allocationStartDate
		        : entryStartDate;
		var endDate = d_allocationEndDate < d_entryEndDate ? allocationEndDate
		        : entryEndDate;

		var noOfAllocatedDays = getDayDiff(startDate, endDate);
		var noOfAllocatedHours = noOfAllocatedDays * hourPerDay
		        * percentageNumeric;

		var personMonth = noOfAllocatedHours / (totalDays * hourPerDay);
		return parseFloat(personMonth.toFixed(2));
	} catch (err) {
		nlapiLogExecution('ERROR', 'getPersonUtilization', err);
		throw err;
	}
}

function getDayDiff(fromDate, toDate) {
	fromDate = nlapiStringToDate(fromDate);
	toDate = nlapiStringToDate(toDate);

	if (fromDate > toDate) {
		return -999;
	}

	var dayDiff = 1;
	for (;; dayDiff++) {
		var newDate = nlapiAddDays(fromDate, dayDiff);

		if (newDate > toDate) {
			break;
		}
	}

	return dayDiff;
}

function getForecastMonths(selectedDate, noOfForecastMonths) {
	try {
		var currentDate = nlapiStringToDate(selectedDate);

		// get start date of the current month
		var d_firstStartDate = getMonthStartDate(currentDate);
		var arrDates = [];

		for (var i = 0; i < noOfForecastMonths; i++) {
			var newDate = nlapiAddMonths(d_firstStartDate, i);
			var s_startDate = nlapiDateToString(newDate, 'date');
			var s_endDate = getMonthEndDate(newDate);

			arrDates.push({
			    StartDate : s_startDate,
			    EndDate : s_endDate,
			    TotalDays : getDayDiff(s_startDate, s_endDate),
			    MonthName : getMonthName(newDate)
			});
		}

		return arrDates;
	} catch (err) {
		nlapiLogExecution('ERROR', 'getForecastMonths', err);
		throw err;
	}
}

function getMonthName(currentDate) {
	var monthNames = [ "January", "February", "March", "April", "May", "June",
	        "July", "August", "September", "October", "November", "December" ];
	return monthNames[currentDate.getMonth()].substring(0, 3) + " "
	        + currentDate.getFullYear();
}

function removeComma(text) {
	return text.replace(/,/g, " ");
}

function getAllocationDetails(arrDates, filterProjectList) {
	var mainObject = {};

	for (var i = 0; i < arrDates.length; i++) {
		var monthName = arrDates[i].MonthName;

		var arrFilters = [
		        new nlobjSearchFilter('jobtype', 'job', 'anyof', '2'),
		        new nlobjSearchFilter('formuladate', null, 'notafter',
		                arrDates[i].EndDate).setFormula('{startdate}'),
		        new nlobjSearchFilter('formuladate', null, 'notbefore',
		                arrDates[i].StartDate).setFormula('{enddate}') ];

		if (filterProjectList && filterProjectList.length > 0) {
			arrFilters.push(new nlobjSearchFilter('project', null, 'anyof',
			        filterProjectList));
		}

		// search the allocation active during the period
		var allocationSearch = searchRecord('resourceallocation', null,
		        arrFilters, [
		                new nlobjSearchColumn('territory', 'customer'),
		                new nlobjSearchColumn('customer'),

		                new nlobjSearchColumn('company'),
		                new nlobjSearchColumn('startdate', 'job'),
		                new nlobjSearchColumn('enddate', 'job'),
		                new nlobjSearchColumn('custentity_practice', 'job'),

		                new nlobjSearchColumn('startdate'),
		                new nlobjSearchColumn('enddate'),
		                new nlobjSearchColumn('percentoftime'),
		                new nlobjSearchColumn('custevent4'),
		                new nlobjSearchColumn('custentity_practice', 'job'),
		                new nlobjSearchColumn('jobbillingtype', 'job'),
		                new nlobjSearchColumn('custentity_t_and_m_monthly',
		                        'job'),
		                new nlobjSearchColumn('department', 'employee') ]);

		for (var j = 0; j < allocationSearch.length; j++) {
			// get the person month
			var personMonth = getPersonUtilization(allocationSearch[j]
			        .getValue('startdate'), allocationSearch[j]
			        .getValue('enddate'), allocationSearch[j]
			        .getValue('percentoftime'), arrDates[i].StartDate,
			        arrDates[i].EndDate, arrDates[i].TotalDays, 8);

			// check the site
			var site = allocationSearch[j].getValue('custevent4') == 1 ? 'Onsite'
			        : 'Offsite';

			var employeePractice = removeComma(allocationSearch[j].getText(
			        'department', 'employee'));
			var projectName = removeComma(allocationSearch[j]
			        .getText('company'));
			var projectBillingType = removeComma(allocationSearch[j].getText(
			        'jobbillingtype', 'job'));

			if (allocationSearch[j].getValue('custentity_t_and_m_monthly',
			        'job') == 'T') {
				projectBillingType += " Monthly";
			}

			if (mainObject[projectName]) {

				if (mainObject[projectName].Allocation[employeePractice]) {

				} else {
					mainObject[projectName].Allocation[employeePractice] = {};

					// add all the months
					for (var k = 0; k < arrDates.length; k++) {
						mainObject[projectName].Allocation[employeePractice][arrDates[k].MonthName] = {
						    Onsite : 0,
						    Offsite : 0
						};
					}
				}
			} else {
				mainObject[projectName] = {};
				mainObject[projectName].Other = {
				    Customer : removeComma(allocationSearch[j]
				            .getText('customer')),
				    Region : removeComma(allocationSearch[j].getText(
				            'territory', 'customer')),
				    ProjectStartDate : allocationSearch[j].getValue(
				            'startdate', 'job'),
				    ProjectEndDate : allocationSearch[j].getValue('enddate',
				            'job'),
				    ExecutingPractice : removeComma(allocationSearch[j]
				            .getText('custentity_practice', 'job')),
				    BillingType : projectBillingType
				};
				mainObject[projectName].Allocation = {};
				mainObject[projectName].Allocation[employeePractice] = {};

				for (var k = 0; k < arrDates.length; k++) {
					mainObject[projectName].Allocation[employeePractice][arrDates[k].MonthName] = {
					    Onsite : 0,
					    Offsite : 0
					};
				}
			}

			mainObject[projectName].Allocation[employeePractice][monthName][site] += personMonth;
			mainObject[projectName].Allocation[employeePractice][monthName][site] = parseFloat(mainObject[projectName].Allocation[employeePractice][monthName][site]
			        .toFixed(2));
		}
	}

	return mainObject;
}
function generateHtmlScreen(mainObject, arrDates) {
	var table = "";

	var context = nlapiGetContext();
	if (context.getEnvironment() == "SANDBOX") {
		css_file_url = "https://system.na1.netsuite.com/core/media/media.nl?id=63885&c=3883006&h=1ab6e33882afec4c1d2b&_xt=.css";
	} else if (context.getEnvironment() == "PRODUCTION") {
		css_file_url = "https://system.na1.netsuite.com/core/media/media.nl?id=63885&c=3883006&h=1ab6e33882afec4c1d2b&_xt=.css";
	}

	table += "<link href='" + css_file_url + "'  rel='stylesheet'>";
	table += "<style>";
	table += ".timesheet-master-table { margin-left: 0px; !important}";
	table += ".timesheet-master-table td {min-width : 20px !important}";
	table += "</style>";

	table += "<table class='timesheet-master-table'>";

	// add the header
	table += "<tr class='header-row'>";
	table += "<td rowspan='2'>";
	table += "Region";
	table += "</td>";
	table += "<td rowspan='2'>";
	table += "Account";
	table += "</td>";
	table += "<td rowspan='2'>";
	table += "Project";
	table += "</td>";
	table += "<td rowspan='2'>";
	table += "Practice";
	table += "</td>";
	for (var i = 0; i < arrDates.length; i++) {
		table += "<td colspan='2'>";
		table += arrDates[i].MonthName;
		table += "</td>";
	}
	table += "<td rowspan='2'>";
	table += "Billing Type";
	table += "</td>";
	table += "<td rowspan='2'>";
	table += "Project Start Date";
	table += "</td>";
	table += "<td rowspan='2'>";
	table += "Project End Date";
	table += "</td>";
	table += "<td rowspan='2'>";
	table += "Executing Practice";
	table += "</td>";

	table += "</tr>";

	table += "<tr class='header-row'>";
	for (var i = 0; i < arrDates.length; i++) {
		table += "<td>";
		table += "Onsite";
		table += "</td>";
		table += "<td>";
		table += "Offsite";
		table += "</td>";
	}

	table += "</tr>";

	var count = 0;
	// var newProject = false;

	// add data rows
	for ( var project in mainObject) {
		// newProject = true;

		for ( var employeePractice in mainObject[project].Allocation) {
			count++;

			var color1 = "";
			var color2 = "";

			if (count % 2 == 0) {
				color1 = "#e6e6ff";
				color2 = "#CCFFCC";
			} else {
				color1 = "#ccccff";
				color2 = "#7DCC96";
			}

			table += "<tr style='background-color:" + color1 + "'>";

			table += "<td>";
			table += mainObject[project].Other.Region;
			table += "</td>";

			table += "<td>";
			table += mainObject[project].Other.Customer;
			table += "</td>";

			table += "<td>";
			table += project;
			table += "</td>";

			// Employee Practice
			table += "<td>";
			table += employeePractice;
			table += "</td>";

			// loop and add column for each month & site
			for ( var month in mainObject[project].Allocation[employeePractice]) {
				table += "<td style='background-color:" + color2 + "'>";
				table += mainObject[project].Allocation[employeePractice][month].Onsite;
				table += "</td>";
				table += "<td style='background-color:" + color2 + "'>";
				table += mainObject[project].Allocation[employeePractice][month].Offsite;
				table += "</td>";
			}

			table += "<td>";
			table += mainObject[project].Other.BillingType;
			table += "</td>";

			table += "<td>";
			table += mainObject[project].Other.ProjectStartDate;
			table += "</td>";

			table += "<td>";
			table += mainObject[project].Other.ProjectEndDate;
			table += "</td>";

			table += "<td>";
			table += mainObject[project].Other.ExecutingPractice;
			table += "</td>";

			table += "</tr>";
		}
	}

	table += "</table>";
	return table;
}
function generateCsvFileForForecast(mainObject, arrDates) {
	var table = "";

	// add the header
	table += "Region,";
	table += "Account,";
	table += "Project,";
	table += "Billing Type,";
	table += "Project Start Date,";
	table += "Project End Date,";
	table += "Executing Practice,";
	table += "Practice,";
	for (var i = 0; i < arrDates.length; i++) {
		table += arrDates[i].MonthName;
		table += ",,";
	}
	table += "\r\n";

	table += ",";
	table += ",";
	table += ",";
	table += ",";
	table += ",";
	table += ",";
	table += ",";
	table += ",";
	for (var i = 0; i < arrDates.length; i++) {
		table += "Onsite,";
		table += "Offsite,";
	}
	table += "\r\n";

	// add data rows
	for ( var project in mainObject) {

		for ( var employeePractice in mainObject[project].Allocation) {
			table += mainObject[project].Other.Region + ",";
			table += mainObject[project].Other.Customer + ",";
			table += project + ",";
			table += mainObject[project].Other.BillingType + ",";
			table += mainObject[project].Other.ProjectStartDate + ",";
			table += mainObject[project].Other.ProjectEndDate + ",";
			table += mainObject[project].Other.ExecutingPractice + ",";
			table += employeePractice + ",";

			// loop and add column for each month & site
			for ( var month in mainObject[project].Allocation[employeePractice]) {
				table += mainObject[project].Allocation[employeePractice][month].Onsite
				        + ",";
				table += mainObject[project].Allocation[employeePractice][month].Offsite
				        + ",";
			}

			table += "\r\n";
		}
	}
	return table;
}

// Client Script
function fieldChange(type, name, linenum) {

	if (name == 'custpage_forecast_month') {
		var noOfMonths = nlapiGetFieldValue(name);

		if (noOfMonths) {
			if (noOfMonths > 0
			        && noOfMonths <= file_constants.Max_Forecast_Month) {
			} else {
				alert("Please enter a number between 1 to "
				        + file_constants.Max_Forecast_Month);
				nlapiSetFieldValue(name, '');
			}
		}
	} else if (name == 'custpage_selected_date') {
		var selectedDate = nlapiGetFieldValue(name);

		if (selectedDate) {
			var d_selectedDate = nlapiStringToDate(selectedDate);
			if (d_selectedDate < new Date()) {
				alert("Cannot select past dates");
				nlapiSetFieldValue(name, '');
			}
		}
	}
}

// Scheduled script
function scheduled(type) {
	try {
		var currentDate = nlapiDateToString(new Date(), 'date');
		var arrDates = getForecastMonths(currentDate,
		        file_constants.Default_Forecast_Month);
		var mainObject = getAllocationDetails(arrDates);
		var file = nlapiCreateFile(file_constants.CSV_File_Name + '.csv',
		        'CSV', generateCsvFileForForecast(mainObject, arrDates));
		file.setName(file_constants.CSV_File_Name + '.csv');

		var emailTemplate = getEmailTemplate();

		nlapiSendEmail(constant.Mail_Author.InformationSystem,
		        file_constants.Email_Recipient, emailTemplate.Subject,
		        emailTemplate.Body, null, null, null, file);
	} catch (err) {
		nlapiLogExecution('ERROR', 'scheduled', err);
		Send_Exception_Mail(err);
	}
}

function getEmailTemplate() {
	var htmltext = '<p>Hi,</p>';
	htmltext += '<p>Please find the resource forecast report in the attachment.</p>';
	htmltext += '<p></p>';
	htmltext += '<p>Regards,<br/>';
	htmltext += 'Information Systems</p>';

	return {
	    Subject : 'Resource Forecast Report',
	    Body : addMailTemplate(htmltext)
	};
}
function Send_Exception_Mail(err)
{​​​​​
    try
    {​​​​​   
        var context = nlapiGetContext();
        var s_DeploymentID =context.getDeploymentId();
        var s_ScriptID =context.getScriptId();
        var s_Subject ='Issue Resource Forecast Report';
        
        var s_Body = 'This is to inform that resource forecat email is having an issue and System is not able to send the email notification to employees.';
        s_Body += '<br/>Issue: Code: '+err.code+' Message: '+err.message;
        s_Body += '<br/>Script: '+s_ScriptID;
        s_Body += '<br/>Deployment: '+s_DeploymentID;
        s_Body += '<br/>Please have a look and do the needfull.';
        s_Body += '<br/><br/>Note: This is system genarated email incase of any failure while sending the notification to employees';

        s_Body += '<br/><br/>Regards,';
        s_Body += '<br/>Digital Office';
        nlapiSendEmail(442,"netsuite.support@brillio.com",s_Subject,s_Body,null, null, null, null);
    }​​​​​
    catch(err)
    {​​​​​
        nlapiLogExecution('error',' Send_Exception_Mail', err.message); 
		
    }​​​​​
}​​​​​
