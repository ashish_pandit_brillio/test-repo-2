function massUpdate(recType, recordId) {
	try {

		var rec = nlapiLoadRecord(recType, recordId);

		if (rec.getLineItemCount('recmachcustrecord_purchaserequest') == 1) {
			var prItemId = rec.getLineItemValue(
			        'recmachcustrecord_purchaserequest', 'id', 1);
			var qaId = rec.getLineItemValue('recmachcustrecord_prquote', 'id',
			        1);

			nlapiSubmitField('customrecord_quotationanalysis', qaId,
			        'custrecord_qa_pr_item', prItemId);

			updateQAStatus(prId);
		}
	} catch (err) {
		nlapiLogExecution('error', 'massUpdate', err);
	}
}

function updateQAStatus(prId) {
	try {
		var qaSearch = nlapiSearchRecord('customrecord_quotationanalysis',
		        null, [
		                new nlobjSearchFilter('custrecord_qa_pr_item', null,
		                        'anyof', prId),
		                new nlobjSearchFilter('isinactive', null, 'is', 'F') ]);

		var prItemStatus = nlapiGetFieldValue('custrecord_prastatus');

		if (qaSearch) {
			var qaStatus = "";
			nlapiLogExecution('debug', 'prItemStatus', prItemStatus);

			if (prItemStatus) {
				prItemStatus = parseInt(prItemStatus);
			}

			switch (prItemStatus) {

				// PR approved pending for Quotation analysis
				case 7:
					qaStatus = 5;
				break;

				// QA Pending for Practice Head Approval
				case 24:
					qaStatus = 6;
				break;

				// Pending PO Creation
				case 27:
					qaStatus = 7;
				break;

				// PO created
				case 23:
					qaStatus = 8;
				break;

				// Closed
				// Closed, Issued from Stock
				case 5:
				case 29:
					qaStatus = 10;
				break;

				case 6:
					qaStatus = 4;
				break;
			}

			if (qaStatus) {
				nlapiSubmitField('customrecord_quotationanalysis', qaSearch[0]
				        .getId(), 'custrecord_approvalstatusforquotation',
				        qaStatus);
				nlapiLogExecution('debug', 'quotation record updated', qaStatus);
			}
		} else {
			nlapiLogExecution('debug', 'no qa found');
		}
	} catch (err) {
		nlapiLogExecution('ERROR', 'updateQAStatus', err);
	}
}