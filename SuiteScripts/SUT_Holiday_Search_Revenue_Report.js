function searchHolidays(request, response){
	try{
	// Get the list of Time Entries
	var strRequest = request.getBody();
	
	var a_data = new Array(); // Store Time Entry data
	
	var o_request	=	JSON.parse(strRequest);
	var holiday_list = [];
	var a_list = o_request.data;
	//var strSelectedValues = JSON.stringify({'s_startDate':startDate, 's_endDate':endDate, 'project':i_project_id, 'customer': i_customer_id, 'employee': i_employee});
	holiday_list = get_holidays(o_request.s_startDate, o_request.s_endDate,o_request.employee,o_request.project,o_request.customer );
	var o_response = new Object();
	if(holiday_list)
	o_response.list = holiday_list.length;
	else
	o_response.list = holiday_list;
	
	var context = nlapiGetContext();
	var usageRemaining = context.getRemainingUsage();
	nlapiLogExecution('AUDIT', 'Remaining Usage: ', usageRemaining);
	
	response.write(JSON.stringify(o_response));
	}
	catch(e)
    {
      nlapiLogExecution('DEBUG','Error occured',e);
    }
	}
	
	
function get_holidays(start_date, end_date, employee, project, customer) {

	var project_holiday = nlapiLookupField('job', project,
	        'custentityproject_holiday');
	var emp_subsidiary = nlapiLookupField('employee', employee, 'subsidiary');

	if (project_holiday == 1) {
		return get_company_holidays(start_date, end_date, emp_subsidiary);
	} else {
		return get_customer_holidays(start_date, end_date, emp_subsidiary,
		        customer);
	}
}

function get_company_holidays(start_date, end_date, subsidiary) {
	var holiday_list = [];

	//start_date = nlapiDateToString(start_date, 'date');
	//end_date = nlapiDateToString(end_date, 'date');

	// nlapiLogExecution('debug', 'start_date', start_date);
	// nlapiLogExecution('debug', 'end_date', end_date);
//	 nlapiLogExecution('debug', 'subsidiary', subsidiary);

	var search_company_holiday = nlapiSearchRecord('customrecord_holiday',
	        'customsearch_company_holiday_search', [
	                new nlobjSearchFilter('custrecord_date', null, 'within',
	                        start_date, end_date),
	                new nlobjSearchFilter('custrecordsubsidiary', null,
	                        'anyof', subsidiary) ], [ new nlobjSearchColumn(
	                'custrecord_date') ]);

	if (search_company_holiday) {

		for (var i = 0; i < search_company_holiday.length; i++) {
			holiday_list.push(search_company_holiday[i]
			        .getValue('custrecord_date'));
		}
	}

	return holiday_list;
}

function get_customer_holidays(start_date, end_date, subsidiary, customer) {
	var holiday_list = [];

	//start_date = nlapiDateToString(start_date, 'date');
	//end_date = nlapiDateToString(end_date, 'date');

	 nlapiLogExecution('debug', 'start_date', start_date);
	 nlapiLogExecution('debug', 'end_date', end_date);
	 nlapiLogExecution('debug', 'subsidiary', subsidiary);
	 nlapiLogExecution('debug', 'customer', customer);

	var search_customer_holiday = nlapiSearchRecord(
	        'customrecordcustomerholiday', 'customsearch_customer_holiday', [
	                new nlobjSearchFilter('custrecordholidaydate', null,
	                        'within', start_date, end_date),
	                new nlobjSearchFilter('custrecordcustomersubsidiary', null,
	                        'anyof', subsidiary),
	                new nlobjSearchFilter('custrecord13', null, 'anyof',
	                        customer) ], [ new nlobjSearchColumn(
	                'custrecordholidaydate', null, 'group') ]);

	if (search_customer_holiday) {

		for (var i = 0; i < search_customer_holiday.length; i++) {
			holiday_list.push(search_customer_holiday[i].getValue(
			        'custrecordholidaydate', null, 'group'));
		}
	}

	return holiday_list;
}