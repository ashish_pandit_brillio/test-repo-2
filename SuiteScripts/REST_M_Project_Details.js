/**
 * RESTlet for Project Details
 * 
 * Version Date Author Remarks 1.00 11 Apr 2016 Nitish Mishra
 * 
 */

function postRESTlet(dataIn) {

	if (nlapiGetUser() == 9673) {
		dataIn = {
		    EmailId : 'vinod.subramanyam@brillio.com',
		    Data : {
		        ProjectId : '32599',
		        AllocationId : 25570
		    },
		    RequestType : M_Constants.Request.Get
		};
	}

	var response = new Response();
	try {
		nlapiLogExecution('debug', 'dataIn', JSON.stringify(dataIn));
		var employeeId = getUserUsingEmailId(dataIn.EmailId);
		var projectId = dataIn.Data ? dataIn.Data.ProjectId : '';
		var allocationId = dataIn.Data ? dataIn.Data.AllocationId : '';
		var requestType = dataIn.RequestType;

		switch (requestType) {

			case M_Constants.Request.Get:

				if (allocationId && projectId) {
					response.Data = getAllocationDetails(employeeId, projectId,
					        allocationId);
					response.Status = true;
				} else if (projectId) {
					response.Data = getProjectDetails(employeeId, projectId);
					response.Status = true;
				} else {
					response.Data = getAllProjectList(employeeId);
					response.Status = true;
				}
			break;

		}

	} catch (err) {
		nlapiLogExecution('ERROR', 'postRESTlet', err);
		response.Data = err;
	}

	if (nlapiGetUser() == 9673) {
		return JSON.stringify(response);
	} else {
		return response;
	}
}

function getAllProjectList(employeeId) {
	try {
		nlapiLogExecution('debug', 'employee id', employeeId);

		// get the active project list
		var projectSearch = nlapiSearchRecord('job', null,
		        [
		                [ 'isinactive', 'is', 'F' ],
		                'and',
		                [ [ 'status', 'anyof', 2 ], 'or',
		                        [ 'status', 'anyof', 4 ] ],
		                'and',
		                [
		                        [ 'custentity_projectmanager', 'anyof',
		                                employeeId ],
		                        'or',
		                        [ 'custentity_deliverymanager', 'anyof',
		                                employeeId ] ] ], [
		                new nlobjSearchColumn('entityid'),
		                new nlobjSearchColumn('altname'),
		                new nlobjSearchColumn('jobtype'),
		                new nlobjSearchColumn('status') ]);

		var projectList = [];

		if (projectSearch) {

			projectSearch.forEach(function(project) {
				projectList.push({
				    Id : project.getId(),
				    Name : project.getValue('entityid') + ":"
				            + project.getValue('altname'),
				    Type : project.getText('jobtype'),
				    Status : project.getText('status')
				});
			});
		}
		return projectList;

	} catch (err) {
		nlapiLogExecution('error', 'getAllProjectList', err);
		throw err;
	}
}

function getProjectDetails(employeeId, projectId) {
	try {
		var record = nlapiLoadRecord('job', projectId);

		if (record.getFieldValue('custentity_projectmanager') == employeeId
		        || record.getFieldValue('custentity_deliverymanager') == employeeId) {
			var projectDetails = {
			    Name : record.getFieldValue('entityid') + ":"
			            + record.getFieldValue('altname'),
			    Customer : record.getFieldText('parent'),
			    Type : record.getFieldText('jobtype'),
			    BillingType : record.getFieldText('jobbillingtype'),
			    Status : record.getFieldText('entitystatus'),
			    ProjectManager : record
			            .getFieldText('custentity_projectmanager'),
			    DeliveryManager : record
			            .getFieldText('custentity_deliverymanager'),
			    ClientPartner : record.getFieldText('custentity_clientpartner'),
			    StartDate : record.getFieldValue('startdate'),
			    EndDate : record.getFieldValue('enddate'),
			    Subsidiary : record.getFieldText('subsidiary'),
			    Vertical : record.getFieldText('custentity_vertical'),
			    ExecutingPractice : record.getFieldText('custentity_practice'),
			    Region : record.getFieldText('custentity_entitygeo'),
			    CustomerSector : record.getFieldText('custentity_customerlob'),
			    AllocationList : []
			};

			var allocationSearch = nlapiSearchRecord('resourceallocation',
			        null, [
			                new nlobjSearchFilter('startdate', null,
			                        'notafter', 'today'),
			                new nlobjSearchFilter('enddate', null, 'notbefore',
			                        'today'),
			                new nlobjSearchFilter('project', null, 'anyof',
			                        projectId) ], [
			                new nlobjSearchColumn('resource'),
			                new nlobjSearchColumn('startdate'),
			                new nlobjSearchColumn('enddate'),
			                new nlobjSearchColumn('custeventrbillable'),
			                new nlobjSearchColumn('percentoftime') ]);

			if (allocationSearch) {

				allocationSearch.forEach(function(allocation) {
					projectDetails.AllocationList
					        .push({
					            Id : allocation.getId(),
					            Employee : allocation.getText('resource'),
					            StartDate : allocation.getValue('startdate'),
					            EndDate : allocation.getValue('enddate'),
					            IsBillable : allocation
					                    .getValue('custeventrbillable') == 'T',
					            Percent : allocation.getValue('percentoftime')
					        });
				});
			}

			return projectDetails;
		} else {
			throw "You are not authorized to access this record";
		}
	} catch (err) {
		nlapiLogExecution('error', 'getProjectDetails', err);
		throw err;
	}
}

function getAllocationDetails(employeeId, projectId, allocationId) {
	try {
		var projectDetails = nlapiLookupField('job', projectId, [
		        'custentity_projectmanager', 'custentity_deliverymanager' ]);

		if (projectDetails.custentity_projectmanager == employeeId
		        || projectDetails.custentity_deliverymanager == employeeId) {

			var allocationSearch = nlapiSearchRecord(
			        'resourceallocation',
			        null,
			        [ new nlobjSearchFilter('internalid', null, 'anyof',
			                allocationId) ],
			        [
			                new nlobjSearchColumn('resource'),
			                new nlobjSearchColumn('project'),
			                new nlobjSearchColumn('startdate'),
			                new nlobjSearchColumn('enddate'),
			                new nlobjSearchColumn('custeventrbillable'),
			                new nlobjSearchColumn('percentoftime'),
			                new nlobjSearchColumn('custeventbstartdate'),
			                new nlobjSearchColumn('custeventbenddate'),
			                new nlobjSearchColumn('custeventwlocation'),
			                new nlobjSearchColumn('custevent3'),
			                new nlobjSearchColumn('custevent_practice'),
			                new nlobjSearchColumn('custevent4'),
			                new nlobjSearchColumn('custevent_allocation_status'),
			                new nlobjSearchColumn('approver', 'employee'),
			                new nlobjSearchColumn(
			                        'custevent_ra_last_modified_date')
			                        .setSort(true),
			                new nlobjSearchColumn('timeapprover', 'employee') ]);

			var allocationDetails = {};

			if (allocationSearch) {
				var allocation = allocationSearch[0];

				allocationDetails = {
				    Id : allocation.getId(),
				    Resource : allocation.getText('resource'),
				    StartDate : allocation.getValue('startdate'),
				    EndDate : allocation.getValue('enddate'),
				    IsBillable : allocation.getValue('custeventrbillable') == 'T',
				    BillRate : allocation.getValue('custevent3'),
				    Percent : allocation.getValue('percentoftime'),
				    Location : allocation.getText('custeventwlocation'),
				    Practice : allocation.getText('custevent_practice'),
				    TimeApprover : allocation.getText('timeapprover',
				            'employee'),
				    ExpenseApprover : allocation
				            .getText('approver', 'employee'),
				    Site : allocation.getText('custevent4')
				};
			}

			return allocationDetails;

		} else {
			throw "You are not authorized to access this record";
		}
	} catch (err) {
		nlapiLogExecution('error', 'getProjectDetails', err);
		throw err;
	}
}