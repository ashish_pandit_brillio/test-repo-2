/**
 * @author Jayesh
 */

function schedule_JE_create()
{
	var vendorList = {};	
    var default_vendor_mappingSearch = nlapiSearchRecord("customrecord_default_vendor_mapping", null,	
        [],	
        [	
            new nlobjSearchColumn("custrecord_subsidiary_je"),	
            new nlobjSearchColumn("custrecord_default_vendor")	
        ]	
    );	
    for (var i_index = 0; i_index < default_vendor_mappingSearch.length; i_index++) {	
        var subsidiary_je = default_vendor_mappingSearch[i_index].getValue('custrecord_subsidiary_je');	
        var default_vendor_je = default_vendor_mappingSearch[i_index].getValue('custrecord_default_vendor');	
        default_vendor_je = default_vendor_je.toString();	
        vendorList[subsidiary_je] = default_vendor_je;	
    }	

	
	
	
	try
	{
		var o_context = nlapiGetContext();
	var i_fp_revrec_id = o_context.getSetting('SCRIPT','custscript_rev_rec_id');
    //  var i_fp_revrec_id =parseInt(42);
		nlapiLogExecution('audit', 'i_fp_revrec_id:- ' + i_fp_revrec_id);
		if(!i_fp_revrec_id){
			return;
        }
			
		var o_rev_rec_rcrd = nlapiLoadRecord('customrecord_fp_rev_rec_je_creation',i_fp_revrec_id);
				
		var a_ignore_proj = o_rev_rec_rcrd.getFieldValues('custrecord_rev_rec_ignore_proj');
		
		var a_fp_revrec_rcrd_details = nlapiLookupField('customrecord_fp_rev_rec_je_creation',i_fp_revrec_id,
		['custrecord_debit_gl_fp_rev_rec', 'custrecord_credit_gl_fp_rev_rec', 'custrecord_fp_revrec_subsidiary']);
		
		var s_currentMonthName = o_rev_rec_rcrd.getFieldText('custrecord_month_fp_rev_rec');
		s_currentMonthName = s_currentMonthName.substr(0,3);
		s_currentMonthName = s_currentMonthName.toUpperCase();
		var s_currentYear = o_rev_rec_rcrd.getFieldText('custrecord_year_fp_rev_rec');
		
		var s_mnth_start_date = nlapiStringToDate(get_previous_month_start_date(s_currentMonthName,s_currentYear));
		var s_mnth_end_date = nlapiStringToDate(get_previous_month_end_date(s_currentMonthName,s_currentYear));
		
		var i_days_in_mnth = getDatediffIndays(nlapiDateToString(s_mnth_start_date),nlapiDateToString(s_mnth_end_date));
		var cur_sub=parseInt(a_fp_revrec_rcrd_details.custrecord_fp_revrec_subsidiary);
		var sub_currency = nlapiLookupField('subsidiary',cur_sub,'currency');
		var currency = sub_currency;
		/*if(cur_sub== parseInt(2))
		{
			var currency='USD';
		}
		else
		{
			var currency='INR';
		}*/
		var a_project_filter = new Array();
		a_project_filter[0] = new nlobjSearchFilter('status', null, 'noneof', 1);
		a_project_filter[1] = new nlobjSearchFilter('jobtype', null, 'anyof', 2);
		a_project_filter[2] = new nlobjSearchFilter('jobbillingtype', null, 'anyof', 'FBM');
		a_project_filter[3] = new nlobjSearchFilter('custentity_fp_rev_rec_type', null, 'anyof', [1,2,4]);
		a_project_filter[4] = new nlobjSearchFilter('subsidiary', null, 'anyof', parseInt(a_fp_revrec_rcrd_details.custrecord_fp_revrec_subsidiary));
		//a_project_filter[6] = new nlobjSearchFilter('internalid', null, 'anyof', 13632);
		var a_project_col=new Array();
		a_project_col[0]=new nlobjSearchColumn('custentity_fp_rev_rec_type');
		a_project_col[1]=new nlobjSearchColumn('custentity_project_currency');
		
		if(a_ignore_proj)
		{
		a_project_filter[5] = new nlobjSearchFilter('internalid', null, 'noneof', a_ignore_proj);
			//a_project_filter[5] = new nlobjSearchFilter('internalid', null, 'anyof', a_ignore_proj);
		}
		
		var a_project_search_results = nlapiSearchRecord('job', null, a_project_filter, a_project_col);
		
		if (a_project_search_results)
		{
			
			var o_je_creation = nlapiCreateRecord('journalentry');
			o_je_creation.setFieldValue('trandate', s_mnth_end_date);
			o_je_creation.setFieldValue('reversaldate', '');
			o_je_creation.setFieldValue('subsidiary', parseInt(a_fp_revrec_rcrd_details.custrecord_fp_revrec_subsidiary));
			
			var cur_sub = parseInt(a_fp_revrec_rcrd_details.custrecord_fp_revrec_subsidiary);
          	var o_rev_loc_sub=Search_revenue_Location_subsdidary();	
			var obj_resource_location=o_rev_loc_sub["Location"];
			var f_total_debit_amount = 0;
			var i_line_sr_no = 1;
			
			nlapiLogExecution('audit', 'proj len:- ' + a_project_search_results.length);
			for (var i_proj_index = 0; i_proj_index < a_project_search_results.length; i_proj_index++)
			{
				var rev_rec_type=a_project_search_results[i_proj_index].getValue('custentity_fp_rev_rec_type');
				var pro_currency=a_project_search_results[i_proj_index].getText('custentity_project_currency');
				nlapiLogExecution('Debug','currency',pro_currency);
				nlapiLogExecution('audit', 'proj id:- ' + a_project_search_results[i_proj_index].getId());
				if(o_context.getRemainingUsage() <= 2000)
				{
					nlapiYieldScript();
				}
				
				var a_allocation_details = new Array();
				
				var filters_allocation = [
									['startdate', 'onorbefore', s_mnth_end_date], 'and',
									['enddate', 'onorafter', s_mnth_start_date], 'and',
									['project', 'anyof', a_project_search_results[i_proj_index].getId()]
								  ];
						
				var columns = new Array();
				columns[0] 	= new nlobjSearchColumn('company');
				columns[1] = new nlobjSearchColumn('resource');
				columns[2] = new nlobjSearchColumn('percentoftime');
				columns[3] = new nlobjSearchColumn('custevent_practice');
				columns[4] = new nlobjSearchColumn('custrecord_parent_practice', 'custevent_practice');
				columns[5] = new nlobjSearchColumn('employeestatus', 'employee');
				columns[6] = new nlobjSearchColumn('customer');
				columns[7] = new nlobjSearchColumn('startdate');
				columns[8] = new nlobjSearchColumn('enddate');
				columns[9] = new nlobjSearchColumn('location','employee');
				
				var a_allocation_result = nlapiSearchRecord('resourceallocation', null, filters_allocation, columns);
				if (a_allocation_result)
				{
					//nlapiLogExecution('audit', 'a_allocation_result len:- ' + a_allocation_result.length);
					for(var i_allocation_index=0; i_allocation_index<a_allocation_result.length; i_allocation_index++)
					{
						if(o_context.getRemainingUsage() <= 1000)
						{
							nlapiYieldScript();
						}
				
						var s_allocation_percent = a_allocation_result[i_allocation_index].getValue('percentoftime');
						s_allocation_percent = s_allocation_percent.toString();
						s_allocation_percent = s_allocation_percent.split('.');
						s_allocation_percent = s_allocation_percent[0].toString().split('%');
						
						var d_allocation_strt_date = a_allocation_result[i_allocation_index].getValue('startdate');
						var d_allocation_end_date = a_allocation_result[i_allocation_index].getValue('enddate');
						d_allocation_strt_date = nlapiStringToDate(d_allocation_strt_date);
						d_allocation_end_date = nlapiStringToDate(d_allocation_end_date);
						
						if(d_allocation_strt_date < s_mnth_start_date)
						{
							d_allocation_strt_date = s_mnth_start_date;
						}
						
						if(d_allocation_end_date > s_mnth_end_date)
						{
							d_allocation_end_date = s_mnth_end_date;
						}
						
						var i_days_diff = getDatediffIndays(nlapiDateToString(d_allocation_strt_date),nlapiDateToString(d_allocation_end_date));
						
						if(i_days_diff >= i_days_in_mnth)
						{
							var f_final_prcnt_allocation = parseFloat(s_allocation_percent) / parseFloat(100);
						}
						else
						{
							var i_total_allocated_days = parseFloat(i_days_diff) / parseFloat(i_days_in_mnth);
							s_allocation_percent = parseFloat(s_allocation_percent) * parseFloat(i_total_allocated_days);
							var f_final_prcnt_allocation = parseFloat(s_allocation_percent) / parseFloat(100);
						}
						f_final_prcnt_allocation = parseFloat(f_final_prcnt_allocation).toFixed(2);
						
						var i_resource_location = a_allocation_result[i_allocation_index].getValue('location','employee');
						i_resource_location=obj_resource_location[i_resource_location]?parseInt(obj_resource_location[i_resource_location]):parseInt(i_resource_location);
                      
						var f_resource_cost = 0;
						//Adding for Fp others
						if(parseInt(rev_rec_type)==parseInt(4)|| parseInt(rev_rec_type)==parseInt(2))
						{
								var emp_level=a_allocation_result[i_allocation_index].getText('employeestatus', 'employee');
								emp_level = emp_level.toString();
								var s_emp_level_sub_str = emp_level.substr(0,1);
								if(!isNaN(s_emp_level_sub_str))
								{
									emp_level = s_emp_level_sub_str;
								}
								else
								{
									emp_level=a_allocation_result[i_allocation_index].getValue('employeestatus', 'employee');
								}
								var a_revenue_cap_filter = [['custrecord_fp_rev_rec_others_projec', 'anyof', a_project_search_results[i_proj_index].getId()]];

								var a_columns_existing_cap_srch = new Array();
								a_columns_existing_cap_srch[0] = new nlobjSearchColumn('created').setSort(true);
								var a_get_logged_in_user_exsiting_revenue_cap = nlapiSearchRecord('customrecord_fp_rev_rec_others_parent', null, a_revenue_cap_filter, a_columns_existing_cap_srch);
								//nlapiLogExecution('audit', 'a_get_logged_in_user_exsiting_revenue_cap len:- ' + a_get_logged_in_user_exsiting_revenue_cap.length);	
								if (a_get_logged_in_user_exsiting_revenue_cap)
								{
								var i_revenue_share_id = a_get_logged_in_user_exsiting_revenue_cap[0].getId();
								
								var a_mnth_end_filter = [['custrecord_fp_rev_rec_month_end_parent_l', 'anyof', i_revenue_share_id]];

								var a_columns_mnth_end = new Array();
								a_columns_mnth_end[0] = new nlobjSearchColumn('created').setSort(true);
								var a_mnth_end_rcrd_srch = nlapiSearchRecord('customrecord_fp_rev_rec_others_mont_end', null, a_mnth_end_filter, a_columns_mnth_end);
								if (a_mnth_end_rcrd_srch)
								{
									var a_mnth_end_effrt = [['custrecord_fp_others_mnth_act_parent', 'anyof', a_mnth_end_rcrd_srch[0].getId()], 'and',
															['custrecordfp_other_mnth_end_level', 'anyof', emp_level], 'and',
															['custrecordfp_othr_month_end_sub_practice', 'anyof', a_allocation_result[i_allocation_index].getValue('custevent_practice')], 'and',
															['custrecord_fp_others__location_mnth_end', 'anyof', i_resource_location]];
									var a_columns_mnth_end_effrt = new Array();
									a_columns_mnth_end_effrt[0] = new nlobjSearchColumn('created').setSort(true);
									a_columns_mnth_end_effrt[1] = new nlobjSearchColumn('custrecord_fp_others__resource_cost');
									
									var a_mnth_end_effrt_srch = nlapiSearchRecord('customrecord_fp_rev_rec_othr_mnth_end_ef', null, a_mnth_end_effrt, a_columns_mnth_end_effrt);
									//nlapiLogExecution('audit', 'a_mnth_end_effrt_srch len:- ' + a_mnth_end_effrt_srch.length);	
									if(a_mnth_end_effrt_srch)
									{
										f_resource_cost = a_mnth_end_effrt_srch[0].getValue('custrecord_fp_others__resource_cost');
									
										f_resource_cost = parseFloat(f_resource_cost) * parseFloat(f_final_prcnt_allocation);
										f_resource_cost = parseFloat(f_resource_cost).toFixed(2);
									}
								}
							}
						}
						else
						{
							var emp_level=a_allocation_result[i_allocation_index].getText('employeestatus', 'employee');
							emp_level = emp_level.toString();
							var s_emp_level_sub_str = emp_level.substr(0,1);
							if(!isNaN(s_emp_level_sub_str))
							{
								emp_level = s_emp_level_sub_str;
							}
							else
							{
								emp_level=a_allocation_result[i_allocation_index].getValue('employeestatus', 'employee');
							}
						var a_revenue_cap_filter = [['custrecord_revenue_share_project', 'anyof', a_project_search_results[i_proj_index].getId()]];

						var a_columns_existing_cap_srch = new Array();
						a_columns_existing_cap_srch[0] = new nlobjSearchColumn('created').setSort(true);
						var a_get_logged_in_user_exsiting_revenue_cap = nlapiSearchRecord('customrecord_revenue_share', null, a_revenue_cap_filter, a_columns_existing_cap_srch);
						//nlapiLogExecution('audit', 'a_get_logged_in_user_exsiting_revenue_cap len:- ' + a_get_logged_in_user_exsiting_revenue_cap.length);	
                      if (a_get_logged_in_user_exsiting_revenue_cap)
						{
							var i_revenue_share_id = a_get_logged_in_user_exsiting_revenue_cap[0].getId();
							
							var a_mnth_end_filter = [['custrecord_revenue_share_parent', 'anyof', i_revenue_share_id]];

							var a_columns_mnth_end = new Array();
							a_columns_mnth_end[0] = new nlobjSearchColumn('created').setSort(true);
							var a_mnth_end_rcrd_srch = nlapiSearchRecord('customrecord_fp_revrec_month_end_process', null, a_mnth_end_filter, a_columns_mnth_end);
							if (a_mnth_end_rcrd_srch)
							{
								var a_mnth_end_effrt = [['custrecord_month_end_parent', 'anyof', a_mnth_end_rcrd_srch[0].getId()], 'and',
														['custrecord_mnth_end_level', 'anyof', emp_level], 'and',
														['custrecord_month_end_sub_practice', 'anyof', a_allocation_result[i_allocation_index].getValue('custevent_practice')], 'and',
														['custrecord_location_mnth_end', 'anyof', i_resource_location]];
								var a_columns_mnth_end_effrt = new Array();
								a_columns_mnth_end_effrt[0] = new nlobjSearchColumn('created').setSort(true);
								a_columns_mnth_end_effrt[1] = new nlobjSearchColumn('custrecord_resource_cost');
								
								var a_mnth_end_effrt_srch = nlapiSearchRecord('customrecord_fp_revrec_month_end_effort', null, a_mnth_end_effrt, a_columns_mnth_end_effrt);
								//nlapiLogExecution('audit', 'a_mnth_end_effrt_srch len:- ' + a_mnth_end_effrt_srch.length);	
								if(a_mnth_end_effrt_srch)
								{
									f_resource_cost = a_mnth_end_effrt_srch[0].getValue('custrecord_resource_cost');
								
									f_resource_cost = parseFloat(f_resource_cost) * parseFloat(f_final_prcnt_allocation);
									f_resource_cost = parseFloat(f_resource_cost).toFixed(2);
								}
							}
						}
						}	
						a_allocation_details.push({
												'i_project_allo': a_allocation_result[i_allocation_index].getText('company'),
												'i_project': a_allocation_result[i_allocation_index].getValue('company'),
												'i_resource_allo': a_allocation_result[i_allocation_index].getValue('resource'),
												'i_resource_allo_text': a_allocation_result[i_allocation_index].getText('resource'),
												'f_percent_allo': f_final_prcnt_allocation,
												'i_subpractce_allo': a_allocation_result[i_allocation_index].getValue('custevent_practice'),
												'i_practice_allo': a_allocation_result[i_allocation_index].getValue('custrecord_parent_practice', 'custevent_practice'),
												'i_emp_level_allo': a_allocation_result[i_allocation_index].getValue('employeestatus', 'employee'),
												'i_customer_allo': a_allocation_result[i_allocation_index].getText('customer'),
												'i_resource_location': i_resource_location,
												'f_resource_cost': f_resource_cost
												});
					}
				}
				//nlapiLogExecution('audit', 'a_allocation_details len:- ' + a_allocation_details.length);	
				//nlapiLogExecution('audit','s_currentMonthName:- '+s_currentMonthName,'s_currentYear:- '+s_currentYear);
              //nlapiSendEmail ( 200580 , 'praveena@inspirria.com' , "TEST ALLOCATIONS JE DATA" , JSON.stringify(a_allocation_details) , null , null , null , null );
              //Added filter to exclude Record created from offline revenue
				var a_filter_get_ytd_revenue_recognized = [['custrecord_project_to_recognize_amount', 'anyof', a_project_search_results[i_proj_index].getId()],'and',
															['custrecord_month_to_recognize_amount', 'contains', s_currentMonthName], 'and',
															['custrecord_year_to_recognize_amount', 'contains', s_currentYear],'and',
                                                          ["custrecord_je_intenalid","isempty",""]];
					
				var a_columns_get_ytd_revenue_recognized = new Array();
				a_columns_get_ytd_revenue_recognized[0] = new nlobjSearchColumn('custrecord_project_to_recognize_amount');
				a_columns_get_ytd_revenue_recognized[1] = new nlobjSearchColumn('custrecord_practice_to_recognize_amount');
				a_columns_get_ytd_revenue_recognized[2] = new nlobjSearchColumn('custrecord_subparctice_to_recognize_amnt');
				a_columns_get_ytd_revenue_recognized[3] = new nlobjSearchColumn('custrecord_role_to_recognize_amount');
				a_columns_get_ytd_revenue_recognized[4] = new nlobjSearchColumn('custrecord_level_to_recognize_amount');
				a_columns_get_ytd_revenue_recognized[5] = new nlobjSearchColumn('custrecord_revenue_recognized');
				a_columns_get_ytd_revenue_recognized[6] = new nlobjSearchColumn('custrecord_month_to_recognize_amount');
				a_columns_get_ytd_revenue_recognized[7] = new nlobjSearchColumn('custrecord_year_to_recognize_amount');
				//a_columns_get_ytd_revenue_recognized[8] = new nlobjSearchColumn('custrecord_subtotal_amount');
				a_columns_get_ytd_revenue_recognized[8] = new nlobjSearchColumn('customer','custrecord_project_to_recognize_amount');
				a_columns_get_ytd_revenue_recognized[9] = new nlobjSearchColumn('custrecord_fp_rev_rec_type');
				var a_get_ytd_revenue = nlapiSearchRecord('customrecord_fp_rev_rec_recognized_amnt', null, a_filter_get_ytd_revenue_recognized, a_columns_get_ytd_revenue_recognized);
				//nlapiLogExecution('audit', 'a_get_ytd_revenue len:- ' + a_get_ytd_revenue.length);	
				if (a_get_ytd_revenue)
				{
					nlapiLogExecution('audit','a_get_ytd_revenue len:- '+a_get_ytd_revenue.length);
					for(var i_ytd_index=0; i_ytd_index<a_get_ytd_revenue.length; i_ytd_index++)
					{
						if(o_context.getRemainingUsage() <= 1000)
						{
							nlapiYieldScript();
						}
				
						var i_ytd_proj = a_get_ytd_revenue[i_ytd_index].getValue('custrecord_project_to_recognize_amount');
						var s_ytd_proj_text = a_get_ytd_revenue[i_ytd_index].getText('custrecord_project_to_recognize_amount');
						var s_ytd_cust_text = a_get_ytd_revenue[i_ytd_index].getText('customer','custrecord_project_to_recognize_amount');
						var i_ytd_practice = a_get_ytd_revenue[i_ytd_index].getValue('custrecord_practice_to_recognize_amount');
						var i_ytd_sub_practice = a_get_ytd_revenue[i_ytd_index].getValue('custrecord_subparctice_to_recognize_amnt');
						var i_ytd_level_selected = a_get_ytd_revenue[i_ytd_index].getValue('custrecord_level_to_recognize_amount');
						var i_ytd_revenue_to_recog = a_get_ytd_revenue[i_ytd_index].getValue('custrecord_revenue_recognized');
					//	var i_ytd_resource_subtotal = a_get_ytd_revenue[i_ytd_index].getValue('custrecord_subtotal_amount');
						
						var i_sub_practice_prcessed = 0;
						var f_emp_cost_sub_practice = search_cost_subprac(a_allocation_details,i_ytd_sub_practice);
						nlapiLogExecution('audit', 'f_emp_cost_sub_practice:- ', f_emp_cost_sub_practice);
							
						if (parseFloat(i_ytd_revenue_to_recog) != parseFloat(0))
						{
							for (var i_allo_index = 0; i_allo_index < a_allocation_details.length; i_allo_index++)
							{
								if (a_allocation_details[i_allo_index].i_project == i_ytd_proj)
								{
									if (a_allocation_details[i_allo_index].i_subpractce_allo == i_ytd_sub_practice)
									{
										var f_resource_cost = a_allocation_details[i_allo_index].f_resource_cost;
										nlapiLogExecution('audit', 'resource cost from allocation:- ', f_resource_cost);
										//f_resource_cost = parseFloat(f_resource_cost).toFixed(1);
										//nlapiLogExecution('audit', 'final f_resource_cost 1:- ', f_resource_cost);
										//f_resource_cost = parseInt(f_resource_cost);
										//nlapiLogExecution('audit', 'final f_resource_cost:- ', f_resource_cost);
										
										if(f_resource_cost > 0)
										{
											var f_credit_amount = (parseFloat(f_resource_cost) / parseFloat(f_emp_cost_sub_practice)) * parseFloat(i_ytd_revenue_to_recog);
											var rate=nlapiExchangeRate(pro_currency,currency);
											f_credit_amount=parseFloat(f_credit_amount)*parseFloat(rate);
											f_credit_amount = parseFloat(f_credit_amount).toFixed(2);
											
											if (f_credit_amount != 0)
											{
												nlapiLogExecution('audit', 'allocated cost for resource:- ' + f_credit_amount);
												//je line entry
												o_je_creation.selectNewLineItem('line');
												o_je_creation.setCurrentLineItemValue('line', 'account', a_fp_revrec_rcrd_details.custrecord_credit_gl_fp_rev_rec);
												o_je_creation.setCurrentLineItemValue('line', 'custcol_srnumber', parseInt(i_line_sr_no));
												i_line_sr_no++;
												o_je_creation.setCurrentLineItemValue('line', 'credit', parseFloat(f_credit_amount));
												o_je_creation.setCurrentLineItemValue('line', 'department', i_ytd_sub_practice);
												o_je_creation.setCurrentLineItemValue('line', 'custcolprj_name', a_allocation_details[i_allo_index].i_project_allo);
												o_je_creation.setCurrentLineItemValue('line', 'custcol_sow_project', a_project_search_results[i_proj_index].getId());
												o_je_creation.setCurrentLineItemValue('line', 'custcolcustcol_temp_customer', a_allocation_details[i_allo_index].i_customer_allo);
												o_je_creation.setCurrentLineItemValue('line', 'custcol_employeenamecolumn', a_allocation_details[i_allo_index].i_resource_allo_text);
												o_je_creation.setCurrentLineItemValue('line', 'memo', 'Revenue Recognized for month of ' + o_rev_rec_rcrd.getFieldText('custrecord_month_fp_rev_rec') + ' ' + o_rev_rec_rcrd.getFieldText('custrecord_year_fp_rev_rec'));
												//Added by Sitaram	
                                                if (_logValidation(vendorList[parseInt(a_fp_revrec_rcrd_details.custrecord_fp_revrec_subsidiary)])) {	
                                                    o_je_creation.setCurrentLineItemValue('line', 'entity', vendorList[parseInt(a_fp_revrec_rcrd_details.custrecord_fp_revrec_subsidiary)]);	
                                                }
												o_je_creation.commitLineItem('line');
												
												f_total_debit_amount = parseFloat(f_total_debit_amount) + parseFloat(f_credit_amount);
												//f_total_debit_amount = parseFloat(f_total_debit_amount).toFixed(2);
												i_sub_practice_prcessed = 1;
											}
										}
									}
								}
							}
							
							if (i_sub_practice_prcessed == 0)
							{
								var rate=nlapiExchangeRate(pro_currency, currency);
								nlapiLogExecution('DEBUG','rate'+rate,'revenue'+i_ytd_revenue_to_recog);
								i_ytd_revenue_to_recog=parseFloat(i_ytd_revenue_to_recog)*parseFloat(rate);
								i_ytd_revenue_to_recog = parseFloat(i_ytd_revenue_to_recog).toFixed(2);
								
								
								nlapiLogExecution('audit', 'unallocated sub prac cost:- ' + i_ytd_revenue_to_recog);
								
								//je entry for not processed sub practice
								o_je_creation.selectNewLineItem('line');
								o_je_creation.setCurrentLineItemValue('line', 'account', a_fp_revrec_rcrd_details.custrecord_credit_gl_fp_rev_rec);
								o_je_creation.setCurrentLineItemValue('line', 'custcol_srnumber', parseInt(i_line_sr_no));
								i_line_sr_no++;
								o_je_creation.setCurrentLineItemValue('line', 'credit', parseFloat(i_ytd_revenue_to_recog));
								//o_je_creation.setCurrentLineItemValue('line', 'department', a_practice_nt_processed[i_dupli]);
								o_je_creation.setCurrentLineItemValue('line', 'department', i_ytd_sub_practice);
								o_je_creation.setCurrentLineItemValue('line', 'custcolprj_name', s_ytd_proj_text);
								o_je_creation.setCurrentLineItemValue('line', 'custcol_sow_project', i_ytd_proj);
								o_je_creation.setCurrentLineItemValue('line', 'custcolcustcol_temp_customer', s_ytd_cust_text);
								o_je_creation.setCurrentLineItemValue('line', 'memo', 'Revenue Recognized for month of ' + o_rev_rec_rcrd.getFieldText('custrecord_month_fp_rev_rec') + ' ' + o_rev_rec_rcrd.getFieldText('custrecord_year_fp_rev_rec'));
								
								//Added by Sitaram
                                if (_logValidation(vendorList[parseInt(a_fp_revrec_rcrd_details.custrecord_fp_revrec_subsidiary)])) {
                                    o_je_creation.setCurrentLineItemValue('line', 'entity', vendorList[parseInt(a_fp_revrec_rcrd_details.custrecord_fp_revrec_subsidiary)]);
                                }
								
								o_je_creation.commitLineItem('line');
								
								f_total_debit_amount = parseFloat(f_total_debit_amount) + parseFloat(i_ytd_revenue_to_recog);
							}
						}
					}
				}
			}
			
			if(f_total_debit_amount)
			{
				if(o_context.getRemainingUsage() <= 400)
				{
					nlapiYieldScript();
				}
				
				nlapiLogExecution('audit','f_total_debit_amount:- '+f_total_debit_amount);
				o_je_creation.selectNewLineItem('line');
				o_je_creation.setCurrentLineItemValue('line', 'custcol_srnumber', parseInt(i_line_sr_no));
				o_je_creation.setCurrentLineItemValue('line', 'account', a_fp_revrec_rcrd_details.custrecord_debit_gl_fp_rev_rec);
				
				 //Added by Sitaram	
                if (_logValidation(vendorList[parseInt(a_fp_revrec_rcrd_details.custrecord_fp_revrec_subsidiary)])) {	
                    o_je_creation.setCurrentLineItemValue('line', 'entity', vendorList[parseInt(a_fp_revrec_rcrd_details.custrecord_fp_revrec_subsidiary)]);	
                }
				
				o_je_creation.setCurrentLineItemValue('line', 'debit', parseFloat(f_total_debit_amount).toFixed(2));
				o_je_creation.commitLineItem('line');
				var i_je_created_id = nlapiSubmitRecord(o_je_creation,true,true);
				nlapiLogExecution('audit','je created sucessfully:- '+i_je_created_id);
				
				var a_fld_arr = new Array();
				a_fld_arr[0] = 'custrecord_journal_entries_fp_rev_rec';
				a_fld_arr[1] = 'custrecord_status_fp_rev_rce';
				a_fld_arr[2] = 'custrecord_data_validated';
				
				var a_fld_values = new Array();
				a_fld_values[0] = i_je_created_id;//i_je_created_id
				a_fld_values[1] = 5;
				a_fld_values[2] = 'F';
				
				nlapiSubmitField('customrecord_fp_rev_rec_je_creation',i_fp_revrec_id,a_fld_arr,a_fld_values);
			}
		}
		
		if(a_ignore_proj)
		{
			nlapiLogExecution('audit','inside delete ignore proj entry');
			var a_filter_ignore_proj_rev = [['custrecord_project_to_recognize_amount', 'anyof', a_ignore_proj],'and',
											['custrecord_month_to_recognize_amount', 'contains', s_currentMonthName], 'and',
											['custrecord_year_to_recognize_amount', 'contains', s_currentYear],'and',
                  ["custrecord_je_intenalid","isempty",""]];
			
			var a_get_ytd_revenue_ignore_proj = nlapiSearchRecord('customrecord_fp_rev_rec_recognized_amnt', null, a_filter_ignore_proj_rev, null);
			if (a_get_ytd_revenue_ignore_proj)
			{
				for(var i_igonre=0; i_igonre<a_get_ytd_revenue_ignore_proj.length; i_igonre++)
					nlapiDeleteRecord('customrecord_fp_rev_rec_recognized_amnt',a_get_ytd_revenue_ignore_proj[i_igonre].getId());
			}
		}
	}
	catch(err)
	{
		nlapiLogExecution('ERROR','ERROR MESSAGE:- '+err);
	}
}

function search_cost_subprac(a_allocation_details,i_ytd_sub_practice)
{
	var f_total_resource_cost_sub_prac = 0;
	for(var i_index_allo_srch=0; i_index_allo_srch<a_allocation_details.length; i_index_allo_srch++)
	{
		if(parseInt(a_allocation_details[i_index_allo_srch].i_subpractce_allo) == parseInt(i_ytd_sub_practice))
		{
			f_total_resource_cost_sub_prac = parseFloat(f_total_resource_cost_sub_prac) + parseFloat(a_allocation_details[i_index_allo_srch].f_resource_cost);
		}
	}
	
	return f_total_resource_cost_sub_prac;
	
}

function getMonthName(currentDate) {
	currentDate = nlapiStringToDate(currentDate)
	var monthNames = [ "JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JUL", "AUG",
	        "SEP", "OCT", "NOV", "DEC" ];
	return monthNames[currentDate.getMonth()];
}

function getDatediffIndays(startDate, endDate)
{
    var one_day=1000*60*60*24;
    var fromDate = startDate;
    var toDate=endDate

	var date1 = nlapiStringToDate(fromDate);

    var date2 = nlapiStringToDate(toDate);

    var date3=Math.round((date2-date1)/one_day);

    return (date3+1);
}

function get_previous_month_start_date(i_month,i_year)
{
   var d_start_date = '';
   var d_end_date = '';
	
   var date_format = checkDateFormat();	 
	
 if (_logValidation(i_month) && _logValidation(i_year)) 
 {
 	  if(i_month == 'Jan' || i_month == 'January' || i_month == 'JAN' || i_month == 'JANUARY')
	  {	  	
		 if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 1 + '-' + 1;		
			d_end_date = i_year + '-' + 1 + '-' + 31;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 1 + '/' + i_year;
			d_end_date = 31 + '/' + 1 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 1 + '/' + 1 + '/' + i_year;
			d_end_date = 1 + '/' + 31 + '/' + i_year;
		 }	 
				
	  }	
	  else if(i_month == 'Feb' || i_month == 'February' || i_month == 'FEB' || i_month == 'FEBRUARY')
	  {
	  	if((i_year%4) == 0)
		{
			i_day = 29;
		}
		else
		{
			i_day = 28;
		}
			
		 if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 2 + '-' + 1;		
			d_end_date = i_year + '-' + 2 + '-' + i_day;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 2 + '/' + i_year;
			d_end_date = i_day + '/' + 2 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 2 + '/' + 1 + '/' + i_year;
			d_end_date = 2 + '/' + i_day + '/' + i_year;
		 }	 
			
	  }		
	  else if(i_month == 'Mar' || i_month == 'March' || i_month == 'MAR' || i_month == 'MARCH')
	  {	  
	     if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 3 + '-' + 1;		
			d_end_date = i_year + '-' + 3 + '-' + 31;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 3 + '/' + i_year;
			d_end_date = 31 + '/' + 3 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 3 + '/' + 1 + '/' + i_year;
			d_end_date = 3 + '/' + 31 + '/' + i_year;
		 }	 
	
	  }	
	  else if(i_month == 'Apr' || i_month == 'April' || i_month == 'APR' || i_month == 'APRIL')
	  {
	  	if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 4 + '-' + 1;		
			d_end_date = i_year + '-' + 4 + '-' + 30;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 4 + '/' + i_year;
			d_end_date = 30 + '/' + 4 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 4 + '/' + 1 + '/' + i_year;
			d_end_date = 4 + '/' + 30 + '/' + i_year;
	     }	 
	 
	
	  }	
	  else if(i_month == 'May' || i_month == 'MAY')
	  {	  
		 if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 5 + '-' + 1;		
			d_end_date = i_year + '-' + 5 + '-' + 31;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 5 + '/' + i_year;
			d_end_date = 31 + '/' + 5 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 5 + '/' + 1 + '/' + i_year;
			d_end_date = 5 + '/' + 31 + '/' + i_year;
	     }	 
	
	  }	  
	  else if(i_month == 'June' || i_month == 'Jun' || i_month == 'JUN' || i_month == 'JUNE')
	  {	 
		 if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 6 + '-' + 1;		
			d_end_date = i_year + '-' + 6 + '-' + 30;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 6 + '/' + i_year;
			d_end_date = 30 + '/' + 6 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 6 + '/' + 1 + '/' + i_year;
			d_end_date = 6 + '/' + 30 + '/' + i_year;
	     }	 
			
	  }	
	  else if(i_month == 'Jul' || i_month == 'July' || i_month == 'JUL' || i_month == 'JULY')
	  {	  
		 if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 7 + '-' + 1;		
			d_end_date = i_year + '-' + 7 + '-' + 31;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 7 + '/' + i_year;
			d_end_date = 31 + '/' + 7 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 7 + '/' + 1 + '/' + i_year;
			d_end_date = 7 + '/' + 31 + '/' + i_year;
	     }	 
		
	  }	
	  else if(i_month == 'August' || i_month == 'Aug' || i_month == 'AUG' || i_month == 'AUGUST')
	  {	  
		 if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 8 + '-' + 1;		
			d_end_date = i_year + '-' + 8 + '-' + 31;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 8 + '/' + i_year;
			d_end_date = 31 + '/' + 8 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 8 + '/' + 1 + '/' + i_year;
			d_end_date = 8 + '/' + 31 + '/' + i_year;
	     }	 
		
	  }  
	  else if(i_month == 'Sep' || i_month == 'September' || i_month == 'SEP' || i_month == 'SEPTEMBER')
	  {  	
	     if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 9 + '-' + 1;		
			d_end_date = i_year + '-' + 9 + '-' + 30;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 9 + '/' + i_year;
			d_end_date = 30 + '/' + 9 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 9 + '/' + 1 + '/' + i_year;
			d_end_date = 9 + '/' + 30 + '/' + i_year;
	     }	 
	
	  }	
	  else if(i_month == 'Oct' || i_month == 'October' || i_month == 'OCT' || i_month == 'OCTOBER')
	  {	 	
		if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 10 + '-' + 1;		
			d_end_date = i_year + '-' + 10 + '-' + 31;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 10 + '/' + i_year;
			d_end_date = 31 + '/' + 10 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 10 + '/' + 1 + '/' + i_year;
			d_end_date = 10 + '/' + 31 + '/' + i_year;
	     }		
	  }	
	  else if(i_month == 'Nov' || i_month == 'November' || i_month == 'NOV' || i_month == 'NOVEMBER')
	  {	  
		if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 11 + '-' + 1;		
			d_end_date = i_year + '-' + 11 + '-' + 30;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 11 + '/' + i_year;
			d_end_date = 30 + '/' + 11 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 11 + '/' + 1 + '/' + i_year;
			d_end_date = 11 + '/' + 30 + '/' + i_year;
	     }	 
			
	  }	
	  else if(i_month == 'Dec' || i_month == 'December' || i_month == 'DEC' || i_month == 'DECEMBER')
	  {	 
		if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 12 + '-' + 1;		
			d_end_date = i_year + '-' + 12 + '-' + 31;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 12 + '/' + i_year;
			d_end_date = 31 + '/' + 12 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 12 + '/' + 1 + '/' + i_year;
			d_end_date = 12 + '/' + 31 + '/' + i_year;
	     }	 
			
	  }	
 }//Month & Year
 return d_start_date;
}

function checkDateFormat()
{
    var context = nlapiGetContext();

	var dateFormatPref = context.getPreference('dateformat');

    return dateFormatPref;
}

function get_previous_month_end_date(i_month,i_year)
{
   var d_start_date = '';
   var d_end_date = '';
	
   var date_format = checkDateFormat();	 
	
 if (_logValidation(i_month) && _logValidation(i_year)) 
 {
 	  if(i_month == 'Jan' || i_month == 'January' || i_month == 'JAN' || i_month == 'JANUARY')
	  {	  	
		 if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 1 + '-' + 1;		
			d_end_date = i_year + '-' + 1 + '-' + 31;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 1 + '/' + i_year;
			d_end_date = 31 + '/' + 1 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 1 + '/' + 1 + '/' + i_year;
			d_end_date = 1 + '/' + 31 + '/' + i_year;
		 }	 
				
	  }	
	  else if(i_month == 'Feb' || i_month == 'February' || i_month == 'FEB' || i_month == 'FEBRUARY')
	  {
	  	if((i_year%4) == 0)
		{
			i_day = 29;
		}
		else
		{
			i_day = 28;
		}
			
		 if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 2 + '-' + 1;		
			d_end_date = i_year + '-' + 2 + '-' + i_day;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 2 + '/' + i_year;
			d_end_date = i_day + '/' + 2 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 2 + '/' + 1 + '/' + i_year;
			d_end_date = 2 + '/' + i_day + '/' + i_year;
		 }	 
			
	  }		
	  else if(i_month == 'Mar' || i_month == 'March' || i_month == 'MAR' || i_month == 'MARCH')
	  {	  
	     if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 3 + '-' + 1;		
			d_end_date = i_year + '-' + 3 + '-' + 31;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 3 + '/' + i_year;
			d_end_date = 31 + '/' + 3 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 3 + '/' + 1 + '/' + i_year;
			d_end_date = 3 + '/' + 31 + '/' + i_year;
		 }	 
	
	  }	
	  else if(i_month == 'Apr' || i_month == 'April' || i_month == 'APR' || i_month == 'APRIL')
	  {
	  	if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 4 + '-' + 1;		
			d_end_date = i_year + '-' + 4 + '-' + 30;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 4 + '/' + i_year;
			d_end_date = 30 + '/' + 4 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 4 + '/' + 1 + '/' + i_year;
			d_end_date = 4 + '/' + 30 + '/' + i_year;
	     }	 
	 
	
	  }	
	  else if(i_month == 'May' || i_month == 'MAY')
	  {	  
		 if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 5 + '-' + 1;		
			d_end_date = i_year + '-' + 5 + '-' + 31;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 5 + '/' + i_year;
			d_end_date = 31 + '/' + 5 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 5 + '/' + 1 + '/' + i_year;
			d_end_date = 5 + '/' + 31 + '/' + i_year;
	     }	 
	
	  }	  
	  else if(i_month == 'June' || i_month == 'Jun' || i_month == 'JUN' || i_month == 'JUNE')
	  {	 
		 if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 6 + '-' + 1;		
			d_end_date = i_year + '-' + 6 + '-' + 30;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 6 + '/' + i_year;
			d_end_date = 30 + '/' + 6 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 6 + '/' + 1 + '/' + i_year;
			d_end_date = 6 + '/' + 30 + '/' + i_year;
	     }	 
			
	  }	
	  else if(i_month == 'Jul' || i_month == 'July' || i_month == 'JUL' || i_month == 'JULY')
	  {	  
		 if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 7 + '-' + 1;		
			d_end_date = i_year + '-' + 7 + '-' + 31;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 7 + '/' + i_year;
			d_end_date = 31 + '/' + 7 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 7 + '/' + 1 + '/' + i_year;
			d_end_date = 7 + '/' + 31 + '/' + i_year;
	     }	 
		
	  }	
	  else if(i_month == 'August' || i_month == 'Aug' || i_month == 'AUG' || i_month == 'AUGUST')
	  {	  
		 if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 8 + '-' + 1;		
			d_end_date = i_year + '-' + 8 + '-' + 31;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 8 + '/' + i_year;
			d_end_date = 31 + '/' + 8 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 8 + '/' + 1 + '/' + i_year;
			d_end_date = 8 + '/' + 31 + '/' + i_year;
	     }	 
		
	  }  
	  else if(i_month == 'Sep' || i_month == 'September' || i_month == 'SEP' || i_month == 'SEPTEMBER')
	  {  	
	     if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 9 + '-' + 1;		
			d_end_date = i_year + '-' + 9 + '-' + 30;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 9 + '/' + i_year;
			d_end_date = 30 + '/' + 9 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 9 + '/' + 1 + '/' + i_year;
			d_end_date = 9 + '/' + 30 + '/' + i_year;
	     }	 
	
	  }	
	  else if(i_month == 'Oct' || i_month == 'October' || i_month == 'OCT' || i_month == 'OCTOBER')
	  {	 	
		if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 10 + '-' + 1;		
			d_end_date = i_year + '-' + 10 + '-' + 31;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 10 + '/' + i_year;
			d_end_date = 31 + '/' + 10 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 10 + '/' + 1 + '/' + i_year;
			d_end_date = 10 + '/' + 31 + '/' + i_year;
	     }		
	  }	
	  else if(i_month == 'Nov' || i_month == 'November' || i_month == 'NOV' || i_month == 'NOVEMBER')
	  {	  
		if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 11 + '-' + 1;		
			d_end_date = i_year + '-' + 11 + '-' + 30;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 11 + '/' + i_year;
			d_end_date = 30 + '/' + 11 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 11 + '/' + 1 + '/' + i_year;
			d_end_date = 11 + '/' + 30 + '/' + i_year;
	     }	 
			
	  }	
	  else if(i_month == 'Dec' || i_month == 'December' || i_month == 'DEC' || i_month == 'DECEMBER')
	  {	 
		if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 12 + '-' + 1;		
			d_end_date = i_year + '-' + 12 + '-' + 31;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 12 + '/' + i_year;
			d_end_date = 31 + '/' + 12 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 12 + '/' + 1 + '/' + i_year;
			d_end_date = 12 + '/' + 31 + '/' + i_year;
	     }	 
			
	  }	
 }//Month & Year
 return d_end_date;
}

function get_previous_month(i_month)
{
	if (i_month) 
 	{
 	  if(i_month == 'Jan' || i_month == 'January' || i_month == 'JAN' || i_month == 'JANUARY')
	  {	  	
	  		i_month = 'Dec';				
	  }	
	  else if(i_month == 'Feb' || i_month == 'February' || i_month == 'FEB' || i_month == 'FEBRUARY')
	  {
	  	i_month = 'Jan';
			
	  }		
	  else if(i_month == 'Mar' || i_month == 'March' || i_month == 'MAR' || i_month == 'MARCH')
	  {	  
	     i_month = 'Feb';
	
	  }	
	  else if(i_month == 'Apr' || i_month == 'April' || i_month == 'APR' || i_month == 'APRIL')
	  {
	  	i_month = 'Mar';
	
	  }	
	  else if(i_month == 'May' || i_month == 'MAY')
	  {	  
		  i_month = 'Apr';
	
	  }	  
	  else if(i_month == 'June' || i_month == 'Jun' || i_month == 'JUN' || i_month == 'JUNE')
	  {	 
		 i_month = 'May';	 
			
	  }	
	  else if(i_month == 'Jul' || i_month == 'July' || i_month == 'JUL' || i_month == 'JULY')
	  {	  
		i_month = 'Jun';
	  }	
	  else if(i_month == 'August' || i_month == 'Aug' || i_month == 'AUG' || i_month == 'AUGUST')
	  {	  
		 i_month = 'Jul';
	  }  
	  else if(i_month == 'Sep' || i_month == 'September' || i_month == 'SEP' || i_month == 'SEPTEMBER')
	  {  	
	      i_month = 'Aug';
	
	  }	
	  else if(i_month == 'Oct' || i_month == 'October' || i_month == 'OCT' || i_month == 'OCTOBER')
	  {	 	
			i_month = 'Sep';	
	  }	
	  else if(i_month == 'Nov' || i_month == 'November' || i_month == 'NOV' || i_month == 'NOVEMBER')
	  {	  
		 i_month = 'Oct';
			
	  }	
	  else if(i_month == 'Dec' || i_month == 'December' || i_month == 'DEC' || i_month == 'DECEMBER')
	  {	 
		 i_month = 'Nov';
			
	  }	
 }//Month & Year
	
	return i_month;
}

function _logValidation(value) 
{
 if(value!='null' && value != null && value != null && value != '' && value!= '- None -' && value != undefined && value != undefined && value != 'undefined' && value!= 'undefined'&& value!= 'NaN' && value != NaN) 
 {
  return true;
 }
 else 
 { 
  return false;
 }
}