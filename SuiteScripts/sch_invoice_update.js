function main() {

	try {

		var invoices =
				[
					83240, 74571, 74570, 75116, 76241, 76233, 77388, 76852, 76961, 77373, 78605,
					76246, 76953, 77270, 76244, 77503, 77491, 77500, 76959, 77385, 77509, 77272,
					77375, 76248, 76958, 76955, 77508, 77494, 77169, 76247, 77068, 77065, 78614,
					78616, 78611, 78621, 78617, 78613, 78607, 78620, 78618, 78619, 78615, 80521,
					80519, 80520, 80523, 80515, 80526, 80412, 80517, 80531, 80528, 80522, 81656,
					81657, 81664, 81654, 81661, 81662, 81653, 81660, 81663, 81655, 81652, 81658,
					81665, 81736, 81718, 81723, 81748, 81741, 81763, 81752, 81758, 82354, 82356,
					82361, 82351, 82330, 82331, 82368, 82357, 82336, 82365, 82353, 82363, 82328,
					82359, 82364, 82366, 82349, 83229, 83236, 83233, 83246, 83226, 83242, 83234,
					83228, 83240, 83245, 83336, 83309, 83389, 83393, 83341, 83396, 83395, 83397
				];

		nlapiLogExecution('debug', 'total records', invoices.length);
		var count = 0;

		var context = nlapiGetContext();

		for (var i = 0; i < invoices.length; i++) {
			yieldScript(context);
			try {
				userEventBeforeSubmit(invoices[i]);
				nlapiLogExecution('debug', 'record id', invoices[i]);
				count++;
				// break;
			}
			catch (er) {
				nlapiLogExecution('error', 'failed record id : ' + invoices[i], er);
			}

		}
		nlapiLogExecution('debug', 'updated records', count);

	}
	catch (err) {
		nlapiLogExecution('error', 'some error', err);
	}
}

function yieldScript(currentContext) {

	if (currentContext.getRemainingUsage() <= 100) {
		nlapiLogExecution('AUDIT', 'API Limit Exceeded');
		var state = nlapiYieldScript();

		if (state.status == "FAILURE") {
			nlapiLogExecution('ERROR', 'Yield Script Failed', 'Reason : ' + state.reason
					+ ' / Size : ' + state.size);
			return false;
		}
		else if (state.status == "RESUME") {
			nlapiLogExecution('AUDIT', 'Script Resumed');
		}
	}
}

function userEventBeforeSubmit(recordid) {

	try {
		nlapiLogExecution('debug', 'started');

		var a_layout_type = [
			'1', // BLLC T&M
			'2', // BLLC T&M Group
			'12', // BLLC T&M Group - Discount
			'13' // BLLC T&M Group - Move
		];

		var record = nlapiLoadRecord('invoice', recordid);
		var i_current_layout = record.getFieldValue('custbody_layout_type');
		nlapiLogExecution('debug', 'i_current_layout', i_current_layout);

		// if its a T&M project
		if (_.indexOf(a_layout_type, i_current_layout) != -1) {

			// check all items and get a list of all discount items
			var i_item_count = record.getLineItemCount('item');
			var a_discount_lines = [];
			var a_discount_account = [
				constant.Account.TradeDiscount
			];
			// get all discount items
			var a_discount_item_search = nlapiSearchRecord('discountitem', null, [
				new nlobjSearchFilter('account', null, 'anyof', a_discount_account)
			]);
			var a_discounted_items = []; // list of discount items
			a_discount_item_search.forEach(function(item) {

				a_discounted_items.push(item.getId());
			});

			for (var line_num = 1; line_num <= i_item_count; line_num++) {
				var i_service_item = record.getLineItemValue('item', 'item', line_num);
				var i_item_account = record.getLineItemValue('item', 'account', line_num);
				nlapiLogExecution('debug', 'i_item_account', i_item_account);
				nlapiLogExecution('debug', '_.indexOf(a_discounted_items, i_service_item) != -1', _
						.indexOf(a_discounted_items, i_service_item));

				if (_.indexOf(a_discounted_items, i_service_item) != -1) {
					a_discount_lines.push({
						LineNumber : line_num,
						ServiceItem : i_service_item,
						PriceLevel : record.getLineItemValue('item', 'pricelevels', line_num),
						Customer : record.getLineItemValue('item', 'custcolcustcol_temp_customer',
								line_num),
						Project : record.getLineItemValue('item', 'custcolprj_name', line_num),
						Employee : record.getLineItemValue('item', 'custcol_employeenamecolumn',
								line_num),
						Rate : record.getLineItemValue('item', 'rate', line_num)
					});
				}
			}

			// get the customer vertical
			var i_project_vertical =
					nlapiLookupField('customer', record.getFieldValue('entity'),
							'custentity_vertical');

			nlapiLogExecution('debug', 'original item count', i_item_count);
			nlapiLogExecution('debug', 'no. of discount items', a_discount_lines.length);

			// get all the employees from time
			var i_time_count = record.getLineItemCount('time');
			var a_emp_list = [] , a_emp_id_list = [];
			var f_total = 0;

			nlapiLogExecution('debug', 'i_time_count', i_time_count);

			for (var line_num = 1; line_num <= i_time_count; line_num++) {
				var b_is_applied = record.getLineItemValue('time', 'apply', line_num) == 'T';
				// nlapiLogExecution('debug', 'b_is_applied', b_is_applied);
				var i_emp_id = record.getLineItemValue('time', 'employee', line_num);

				if (b_is_applied) {

					var pos = -1;
					for (var i = 0; i < a_emp_id_list.length; i++) {
						if (a_emp_id_list[i] == i_emp_id) {
							pos = i;
							break;
						}
					}

					var amount = parseFloat(record.getLineItemValue('time', 'amount', line_num));

					nlapiLogExecution('debug', 'amount A', amount);

					// a_emp_id_list
					if (pos == -1) {
						a_emp_list.push({
							Id : i_emp_id,
							Name : record.getLineItemValue('time', 'employee', line_num),
							Practice : record.getLineItemValue('time', 'department', line_num),
							Amount : amount
						});

						a_emp_id_list.push(i_emp_id);
					}
					else {
						nlapiLogExecution('debug', 'amount B', JSON.stringify(a_emp_list[pos]));
						nlapiLogExecution('debug', 'amount C', a_emp_list[pos].Amount);
						a_emp_list[pos].Amount += amount;
						nlapiLogExecution('debug', 'amount D', a_emp_list[pos].Amount);
					}

					f_total += parseFloat(record.getLineItemValue('time', 'amount', line_num));
				}
			}

			nlapiLogExecution('debug', 'employee list before', JSON.stringify(a_emp_list));
			mapEmployeeDetails(a_emp_list, a_emp_id_list, f_total);
			nlapiLogExecution('debug', 'employee list after', JSON.stringify(a_emp_list));
			// a_emp_list.push(a_emp_list[0]);
			// a_emp_list.push(a_emp_list[0]);

			var i_employee_count = a_emp_list.length;
			nlapiLogExecution('debug', 'no. of employees', i_employee_count);

			if (i_employee_count > 1) {
				nlapiLogExecution('audit', 'greater than 1', recordid);
				throw "greater than 1 " + recordid;
			}
			else {
				nlapiLogExecution('audit', '1', recordid);
			}

			nlapiLogExecution('debug', 'total', f_total);

			var b_do_remove = false;
			var count = 0;

			// add new lines for each employee and discount item
			a_discount_lines.forEach(function(discount_details) {

				var i_total_amount = discount_details.Rate;
				var i_new_amount = i_total_amount / i_employee_count;
				b_do_remove = false;
				nlapiLogExecution('debug', 'i_total_amount', i_total_amount);
				nlapiLogExecution('debug', 'i_new_amount', i_new_amount);

				a_emp_list.forEach(function(employee) {

					nlapiLogExecution('debug', 'employee', JSON.stringify(employee));
					var discount = (i_total_amount * employee.DiscountRatio);
					nlapiLogExecution('debug', 'discount', discount);
					count++;
					record.selectNewLineItem('item');
					record.setCurrentLineItemValue('item', 'item', discount_details.ServiceItem);
					record.setCurrentLineItemValue('item', 'pricelevels',
							discount_details.PriceLevel);
					record.setCurrentLineItemValue('item', 'rate', discount);
					record.setCurrentLineItemValue('item', 'amount', discount);
					record.setCurrentLineItemValue('item', 'custcolcustcol_temp_customer',
							discount_details.Customer);
					record.setCurrentLineItemValue('item', 'custcolprj_name',
							discount_details.Project);
					record.setCurrentLineItemValue('item', 'department', employee.Practice);
					record.setCurrentLineItemValue('item', 'class', i_project_vertical);
					record.setCurrentLineItemValue('item', 'custcol_employeenamecolumn',
							employee.Name);
					record.commitLineItem('item');
					nlapiLogExecution('debug', 'item added', count);
					b_do_remove = true;
				});

				// remove the original discount line
				if (b_do_remove) {
					record.removeLineItem('item', discount_details.LineNumber);
					// nlapiCommitLineItem('item');
					nlapiLogExecution('debug', 'item remove', discount_details.LineNumber);
					// nlapiRemoveLineItem('item', discount_details.LineNumber);
				}
			});

			nlapiSubmitRecord(record, false, true);
		}
	}
	catch (err) {
		nlapiLogExecution('ERROR', 'userEventBeforeSubmit', err);
	}
}

function mapEmployeeDetails(a_emp_list, a_emp_id_list, total) {

	nlapiSearchRecord('employee', null, [
		new nlobjSearchFilter('internalid', null, 'anyof', a_emp_id_list)
	], [
		new nlobjSearchColumn('department'), new nlobjSearchColumn('entityid')
	]).forEach(function(emp) {

		for (var i = 0; i < a_emp_list.length; i++) {

			if (a_emp_list[i].Id == emp.getId()) {
				a_emp_list[i].Name = emp.getValue('entityid');
				a_emp_list[i].Practice = emp.getValue('department');
				a_emp_list[i].DiscountRatio = a_emp_list[i].Amount / total;
				break;
			}
		}
	});

	nlapiLogExecution('debug', 'employee list A', JSON.stringify(a_emp_list));
}
