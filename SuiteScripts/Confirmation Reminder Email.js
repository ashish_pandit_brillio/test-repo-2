/**
 * Module Description
 * 
 * Version      Date                          Author                          Remarks  
 * 1.00         24th  Feb 2016             Manikandan v          
 */
 
function Main()
{
  try

{
var filters = new Array();
filters[0]  =new nlobjSearchFilter('custrecord_new_question_6',null,'isempty');
var columns = new Array();
columns[0]  =new nlobjSearchColumn('firstname','custrecord_njcp_employee_name');
columns[1]  =new nlobjSearchColumn('email','custrecord_reporting_manager');
columns[2]  =new nlobjSearchColumn('firstname', 'custrecord_reporting_manager');
columns[3]  =new nlobjSearchColumn('internalid','custrecord_njcp_employee_name');
columns[4]  =new nlobjSearchColumn('custentity_probationenddate','custrecord_njcp_employee_name');
columns[5]  =new nlobjSearchColumn('internalid');

// search for employee's reporting Manager

var emp_search = nlapiSearchRecord('customrecord_confirmation_process', null,filters,columns);

if(isNotEmpty(emp_search))

{
	
	nlapiLogExecution('debug', 'emp_search.length',emp_search.length);
	
	for(var i=0;i<emp_search.length;i++)
	{
     sendServiceMails(
			emp_search[i].getValue('firstname','custrecord_njcp_employee_name'),
			emp_search[i].getValue('email','custrecord_reporting_manager'),
			emp_search[i].getValue('firstname', 'custrecord_reporting_manager'),
			emp_search[i].getValue('internalid','custrecord_njcp_employee_name'),
			emp_search[i].getValue('custentity_probationenddate','custrecord_njcp_employee_name'),
			emp_search[i].getValue('internalid'));
	}

}

}
catch(err){
          nlapiLogExecution('error', 'Main', err);
}
}

function sendServiceMails(firstName, reporting_mng_email, reporting_mng_first_name, employee_internal_id, probation_end_date,feedback_record_id)
{
	
	try{
                //var AttachmentID=32680;
                //var fileObj = nlapiLoadFile();
		var mailTemplate = serviceTemplate(firstName, reporting_mng_email, reporting_mng_first_name, employee_internal_id, probation_end_date, feedback_record_id);		
		nlapiLogExecution('debug', 'chekpoint',reporting_mng_email);
		nlapiSendEmail(10730, reporting_mng_email, mailTemplate.MailSubject, mailTemplate.MailBody,['waseem.pasha@brillio.com'],null,null,null,null);
		
	}
catch(err){
          nlapiLogExecution('error', 'sendServiceMails', err);
          throw err;
     }
}

function serviceTemplate(firstName, reporting_mng_email, reporting_mng_first_name, employee_internal_id, probation_end_date, feedback_record_id) 
{
    
	var feedbackFormUrl = 'https://forms.na1.netsuite.com/app/site/hosting/scriptlet.nl?script=768&deploy=1&compid=3883006&h=7aef7678ce733227f374';
	feedbackFormUrl +='&custparam_employee_id='+employee_internal_id+'&feedback_record_id=' + feedback_record_id;
	
	     
	var htmltext = '';
    
    htmltext += '<table border="0" width="100%"><tr>';
    htmltext += '<td colspan="4" valign="top">';
    //htmltext += '<p>Hi ' + firstName + ',</p>';  
    htmltext += '<p>Hi  ' + reporting_mng_first_name + ',</p>';
   //htmltext += '&nbsp';
   //htmltext += &nbsp;
 
 
   htmltext += '<p>This is just a Gentle Reminder.</p>';
 
   htmltext += '<p> This is to bring to your notice that '+firstName+' is due for confirmation on '+probation_end_date+'.Request you to kindly provide us your feedback by clciking here <a href  ="'+feedbackFormUrl+'" >Confirmation</a> after having a detailed discussion with '+firstName+' within 2 business days from the date of receipt of this reminder mail.Please respond to Waseem.pasha@brillio.com </p>';
   htmltext +='<p>Please Note: This is a system generated email, so please don’t respond to it.In case of any clarification, do raise a ticket at ‘HR Hub’</p>';

   htmltext += '<p>Thanks & Regards,</p>';
   htmltext += '<p>Team HR</p>';
 
 
    htmltext += '</td></tr>';
    htmltext += '</table>';
    htmltext += '<hr width="100%" size="1" noshade color="#CCCCCC">';
    htmltext += '<table width="100%" border="0" cellspacing="0" cellpadding="0">';
    htmltext += '<tr>';
    htmltext += '<td align="right">';
    htmltext += '<font style="font-size:9px; font-family:Verdana,Arial,Helvetica,sans-serif; color:#999999;">Brillio is powered by <a href="http://www.netsuite.com/" style="color:#999999;">NetSuite</a> One System. No Limits.</font>';
    htmltext += '</td>';
    htmltext += '</tr>';
    htmltext += '</table>';
    htmltext += '</body>';
    htmltext += '</html>';
 
    return {
        MailBody : htmltext,
        MailSubject : "Reminder -Confirmation feedback initiation"      
    };
}
































