// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
/*
   	Script Name: CLI_Approve_Reject
	Author: Vikrant S. Adhav	
	Company: Aashna CloudTech
	Date: 23-04-2014
    Description: Initial draft for sending Invoice for approval or rejection. 


	Script Modification Log:

	-- Date --			-- Modified By --				--Requested By--				-- Description --




	Below is a summary of the process controls enforced by this script file.  The control logic is described
	more fully, below, in the appropriate function headers and code blocks.

     PAGE INIT
		- pageInit(type)


     SAVE RECORD
		- saveRecord()


     VALIDATE FIELD
		- validateField(type, name, linenum)


     FIELD CHANGED
		- fieldChanged(type, name, linenum)


     POST SOURCING
		- postSourcing(type, name)


	LINE INIT
		- lineInit(type)


     VALIDATE LINE
		- validateLine()


     RECALC
		- reCalc()


     SUB-FUNCTIONS
		- The following sub-functions are called by the above core functions in order to maintain code
            modularization:





*/
}
// END SCRIPT DESCRIPTION BLOCK  ====================================



// BEGIN SCRIPT UPDATION BLOCK  ====================================
/*


*/
// END SCRIPT UPDATION BLOCK  ====================================




// BEGIN GLOBAL VARIABLE BLOCK  =====================================
{

	//  Initialize any Global Variables, in particular, debugging variables...


}
// END GLOBAL VARIABLE BLOCK  =======================================

function btn_final_invoice_call(invoice_ID)//
{

}

function btn_reject_call(invoice_ID)//
{
    var rec_invoice = nlapiLoadRecord('invoice', invoice_ID);
    
    rec_invoice.setFieldValue('custbody_invoicestatus', 2);
    
    nlapiSubmitRecord(rec_invoice, true);
}



function btn_approve_call(invoice_ID)//
{
    try //
    {
        var rec_invoice = nlapiLoadRecord('invoice', invoice_ID);
        
        var filters = new Array();
        var columns = new Array();
        
        filters[0] = new nlobjSearchFilter('name', null, 'is', invoice_ID);
        
        columns[0] = new nlobjSearchColumn('name');
        columns[1] = new nlobjSearchColumn('custrecord_invoice_item');
        columns[2] = new nlobjSearchColumn('custrecord_invoice_qty');
        columns[3] = new nlobjSearchColumn('custrecord_invoice_item_amount');
        columns[4] = new nlobjSearchColumn('custrecord_invoice_id');
        columns[5] = new nlobjSearchColumn('custrecord_invoice_line_number');
        columns[6] = new nlobjSearchColumn('custrecord_invoice_list_type');
        columns[7] = new nlobjSearchColumn('custrecord_invoice_item_rate');
        
        var searchResult = nlapiSearchRecord('customrecord_invoice_line_items', null, filters, columns);
        
        if (searchResult != null && searchResult != 'undefined' && searchResult != '') //
        {
            //alert('searchResult.length : ' + searchResult.length);
            for (var i = 0; i < searchResult.length; i++) //
            {
                var line_type = searchResult[i].getValue('custrecord_invoice_list_type');
                nlapiLogExecution('DEBUG', 'ApproveButton', 'line_type : ' + line_type);
                //alert('line_type : ' + line_type);
                
                var rate = searchResult[i].getValue('custrecord_invoice_item_rate');
                nlapiLogExecution('DEBUG', 'ApproveButton', 'rate : ' + rate);
                //alert('rate : ' + rate);
                
                var line_no = searchResult[i].getValue('custrecord_invoice_line_number');
                nlapiLogExecution('DEBUG', 'ApproveButton', 'line_no : ' + line_no);
                //alert('line_no : ' + line_no);
                
                var amount = searchResult[i].getValue('custrecord_invoice_item_amount');
                nlapiLogExecution('DEBUG', 'ApproveButton', 'amount : ' + amount);
                //alert('amount : ' + amount);
                
                
                if (line_type == 'time') //
                {
                    rec_invoice.setLineItemValue('time', 'rate', parseInt(line_no), rate);
                    //alert('into time');
                }
                else //
                {
                    rec_invoice.setLineItemValue('item', 'amount', parseInt(line_no), amount);
                    //alert('into item');
                }
            }
        }
        rec_invoice.setFieldValue('custbody_invoicestatus', 1);
		nlapiSetFieldValue('custbody_invoicestatus', 1);
		
		var int_id = get_Invoice_ID();
        //alert('int_id ' + int_id);
		
        rec_invoice.setFieldValue('custbody_invoice_id', int_id);
		
        var rec_id = nlapiSubmitRecord(rec_invoice, true, true);
		//alert('rec_id : ' + rec_id);
		
        
    } 
    catch (e) //
    {
        nlapiLogExecution('DEBUG', 'ERROR', 'ERROR : ' + e);
		alert('error '+e.getCode());
    }
    location.reload();
    return true;
    
}


function get_Invoice_ID() //
{
	var r_cust_rec = nlapiLoadRecord('customrecord_invoice_auto_numbering','1');	
    
    //if (type == 'create') //
    {
        var int_id = r_cust_rec.getFieldValue('custrecord_approved_invoice_id');
        int_id = parseInt(int_id) + 1;
        r_cust_rec.setFieldValue('custrecord_approved_invoice_id', int_id);
        nlapiSubmitRecord(r_cust_rec, true);
		
        int_id = 'INV-' + int_id;
		
		return int_id;
    }
}



// BEGIN PAGE INIT ==================================================

function pageInit(type)
{
    /*  On page init:

	- PURPOSE

		FIELDS USED:

		--Field Name--				--ID--			--Line Item Name--

    */

    //  LOCAL VARIABLES


    //  PAGE INIT CODE BODY


}

// END PAGE INIT ====================================================





// BEGIN SAVE RECORD ================================================

function saveRecord()
{
    /*  On save record:

	- PURPOSE



	  FIELDS USED:

		--Field Name--			--ID--		--Line Item Name--


    */


    //  LOCAL VARIABLES



    //  SAVE RECORD CODE BODY


	return true;

}

// END SAVE RECORD ==================================================





// BEGIN VALIDATE FIELD =============================================

function validateField(type, name, linenum)
{

	/*  On validate field:

          - EXPLAIN THE PURPOSE OF THIS FUNCTION


          FIELDS USED:

          --Field Name--				--ID--


    */


	//  LOCAL VARIABLES



	//  VALIDATE FIELD CODE BODY


	return true;

}

// END VALIDATE FIELD ===============================================





// BEGIN FIELD CHANGED ==============================================

function fieldChanged(type, name, linenum)
{
    /*  On field changed:

          - PURPOSE


          FIELDS USED:

			--Field Name--				--ID--


    */


    //  LOCAL VARIABLES


    //  FIELD CHANGED CODE BODY

	//alert('Field Name='+name+'\nLine Item Type='+type+'\nField Value='+nlapiGetFieldValue(name)+'\nLine Item Value='+nlapiGetLineItemValue(type, name, 1));

}

// END FIELD CHANGED ================================================





// BEGIN POST SOURCING ==============================================

function postSourcing(type, name)
{

    /*  On post sourcing:

	- PURPOSE

          FIELDS USED:

		--Field Name--			--ID--		--Line Item Name--


    */

    //  LOCAL VARIABLES


    //  POST SOURCING CODE BODY



}

// END POST SOURCING ================================================



// BEGIN LINE INIT ==============================================

function lineInit(type)
{

    /*  On Line Init:

	- PURPOSE

          FIELDS USED:

		--Field Name--			--ID--		--Line Item Name--


    */

    //  LOCAL VARIABLES


    //  LINE INIT CODE BODY



}

// END LINE INIT ================================================


// BEGIN VALIDATE LINE ==============================================

function validateLine(type)
{

	/*  On validate line:

          - EXPLAIN THE PURPOSE OF THIS FUNCTION


          FIELDS USED:

          --Field Name--				--ID--


    */


	//  LOCAL VARIABLES


	//  VALIDATE LINE CODE BODY


	return true;

}

// END VALIDATE LINE ================================================





// BEGIN RECALC =====================================================

function recalc(type)
{

	/*  On recalc:

          - EXPLAIN THE PURPOSE OF THIS FUNCTION


          FIELDS USED:

          --Field Name--				--ID--


    */


	//  LOCAL VARIABLES


	//  RECALC CODE BODY


}

// END RECALC =======================================================





// BEGIN FUNCTION ===================================================




// END FUNCTION =====================================================
